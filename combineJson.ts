const fs = require('fs');
const path = require('path');

const getMultipleLanguage = () => {
  try {
    const pathReadDirI18 = path.resolve(__dirname, 'public', 'i18n');
    const readDirI18 = fs.readdirSync(pathReadDirI18, 'utf8');

    for (const folder of readDirI18) {
      let data = {};
      const pathReadFolderLangs = path.resolve(pathReadDirI18, folder);
      const readDirFolderLangs = fs.readdirSync(pathReadFolderLangs, 'utf8');
      for (const file of readDirFolderLangs) {
        if (file !== 'translation.json') {
          const pathReadFileLang = path.resolve(pathReadFolderLangs, file);
          const readDirLangs = JSON.parse(
            fs.readFileSync(pathReadFileLang, 'utf8')
          );
          data = { ...data, ...readDirLangs };
        }
      }
      const pathWriteFile = path.resolve(
        pathReadFolderLangs,
        'translation.json'
      );
      fs.writeFileSync(pathWriteFile, JSON.stringify(data));
    }
  } catch (error) {
    console.log(error);
  }
};

// Combine DOF config
// Folder: public/dof
// Output: public/dof/dof.json
// Format: {fields: [], views: []}
// Condition: put all config file into [fields and views] folder ONLY!!!
const combineDOFConfig = () => {
  try {
    const content = { fields: [], views: [] };
    const output_filename = 'dof.json';

    const dir = path.resolve(__dirname, 'public', 'dof');
    const readDirDOF = fs.readdirSync(dir, 'utf8');

    for (const folder of readDirDOF) {
      const folderPath = `${dir}/${folder}`;
      // Check is folder
      const isFolder = fs.statSync(folderPath).isDirectory();
      if (isFolder) {
        const readFolder = fs.readdirSync(folderPath, 'utf8');

        for (const file of readFolder) {
          const pathReadFile = path.resolve(folderPath, file);
          const readFile = JSON.parse(fs.readFileSync(pathReadFile, 'utf8'));
          // Merge to content
          readFile.fields &&
            (content.fields = [...content.fields, ...readFile?.fields]);
          readFile.views &&
            (content.views = [...content.views, ...readFile?.views]);
        }
      }
    }
    // Write to dof.json file
    fs.writeFileSync(
      path.resolve(dir, output_filename),
      JSON.stringify(content)
    );
  } catch (error) {
    console.log(error);
  }
};

// Run
getMultipleLanguage();
combineDOFConfig();
