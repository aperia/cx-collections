import { controlRegistry } from 'app/_libraries/_dof/core';

// controls
import { HeadingControlProps } from 'app/components/_dof_controls/controls/HeadingControl';
import { GroupTextControlProps } from 'app/components/_dof_controls/controls/GroupTextControl';
import { DividerControlProps } from 'app/components/_dof_controls/controls/DividerControl';
import { TextBoxControlProps } from 'app/components/_dof_controls/controls/TextBoxControl';
import { CheckBoxControlProps } from 'app/components/_dof_controls/controls/CheckBoxControl';
import { ComboBoxControlProps } from 'app/components/_dof_controls/controls/ComboBoxControl';
import { DropDownControlProps } from 'app/components/_dof_controls/controls/DropDownControl';
import { DatePickerControlProps } from 'app/components/_dof_controls/controls/DatePickerControl';
import { NumericControlProps } from 'app/components/_dof_controls/controls/NumericControl';
import { MaskedTextBoxControlProps } from 'app/components/_dof_controls/controls/MaskedTextBoxControl';
import { SimpleGridControlProps } from 'app/components/_dof_controls/controls/SimpleGridControl';
import { TextAreaControlProps } from 'app/components/_dof_controls/controls/TextAreaControl';
import { AlertMessageControlProps } from 'app/components/_dof_controls/controls/AlertMessageControl';
import { DateTypeRadioControlProps } from 'app/components/_dof_controls/controls/DateTypeRadioControl';
import { DateRangePickerControlProps } from 'app/components/_dof_controls/controls/DateRangePickerControl';
import { GroupRadioControlProps } from 'app/components/_dof_controls/controls/GroupRadioControl';
import { GroupRadioControlProps_V2 } from 'app/components/_dof_controls/controls/GroupRadioControl_V2';
import { InlineMessageProps } from 'app/components/_dof_controls/controls/InlineMessage';
import { DynamicGroupRadioControlProps } from 'app/components/_dof_controls/controls/DynamicGroupRadioControl';
import { RelatedAccountControlProps } from 'app/components/_dof_controls/controls/RelatedAccount';
import { DangerousHtmlControlProps } from 'app/components/_dof_controls/controls/DangerousHtmlControl';


/**
 * Onchange funcs registries
 */
import 'app/components/_dof_controls/custom-functions';

/**
 * Custom Validations registries
 */
import 'app/components/_dof_controls/custom-validations';
import { LinkAccountsControlProps } from 'app/components/_dof_controls/controls/LinkAccounts';
import { BubbleControlProps, NumericWithCheckControlProps } from 'app/components/_dof_controls/controls';

/**
 * Layout Registries
 */

/**
 * Control registries
 */
controlRegistry.registerControl<HeadingControlProps>('HeadingControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/HeadingControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<GroupTextControlProps>('GroupTextControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/GroupTextControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<DividerControlProps>('DividerControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/DividerControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<DatePickerControlProps>('DatePickerControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/DatePickerControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<ComboBoxControlProps>('ComboBoxControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/ComboBoxControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<DropDownControlProps>('DropDownControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/DropDownControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<CheckBoxControlProps>('CheckBoxControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/CheckBoxControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<TextBoxControlProps>('TextBoxControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/TextBoxControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<NumericControlProps>('NumericControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/NumericControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<MaskedTextBoxControlProps>(
  'MaskedTextBoxControl',
  {
    promise: () =>
      import('app/components/_dof_controls/controls/MaskedTextBoxControl').then(
        imports => imports.default
      )
  }
);

controlRegistry.registerControl<SimpleGridControlProps>('SimpleGridControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/SimpleGridControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<TextAreaControlProps>('TextAreaControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/TextAreaControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<AlertMessageControlProps>(
  'AlertMessageControl',
  {
    promise: () =>
      import('app/components/_dof_controls/controls/AlertMessageControl').then(
        imports => imports.default
      )
  }
);

controlRegistry.registerControl<DateTypeRadioControlProps>(
  'DateTypeRadioControl',
  {
    promise: () =>
      import('app/components/_dof_controls/controls/DateTypeRadioControl').then(
        imports => imports.default
      )
  }
);

controlRegistry.registerControl<DateRangePickerControlProps>(
  'DateRangePickerControl',
  {
    promise: () =>
      import(
        'app/components/_dof_controls/controls/DateRangePickerControl'
      ).then(imports => imports.default)
  }
);

controlRegistry.registerControl<GroupRadioControlProps_V2>(
  'GroupRadioControl_V2',
  {
    promise: () =>
      import('app/components/_dof_controls/controls/GroupRadioControl_V2').then(
        imports => imports.default
      )
  }
);

controlRegistry.registerControl<GroupRadioControlProps>('GroupRadioControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/GroupRadioControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<DynamicGroupRadioControlProps>(
  'DynamicGroupRadioControl',
  {
    promise: () =>
      import(
        'app/components/_dof_controls/controls/DynamicGroupRadioControl'
      ).then(imports => imports.default)
  }
);

controlRegistry.registerControl<InlineMessageProps>('InlineMessage', {
  promise: () =>
    import('app/components/_dof_controls/controls/InlineMessage').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<RelatedAccountControlProps>('RelatedAccount', {
  promise: () =>
    import('app/components/_dof_controls/controls/RelatedAccount').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<LinkAccountsControlProps>('LinkAccounts', {
  promise: () =>
    import('app/components/_dof_controls/controls/LinkAccounts').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<NumericWithCheckControlProps>(
  'NumberWithCheck',
  {
    promise: () =>
      import('app/components/_dof_controls/controls/NumericWithCheck').then(
        imports => imports.default
      )
  }
);

controlRegistry.registerControl<DangerousHtmlControlProps>(
  'DangerousHtmlControl',
  {
    promise: () =>
      import('app/components/_dof_controls/controls/DangerousHtmlControl').then(
        imports => imports.default
      )
  }
);

controlRegistry.registerControl<BubbleControlProps>(
  'BubbleControl',
  {
    promise: () =>
      import('app/components/_dof_controls/controls/BubbleControl').then(
        imports => imports.default
      )
  }
);
