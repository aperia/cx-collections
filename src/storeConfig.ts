import { combineReducers } from 'redux';
import { caches } from 'app/_libraries/_dof/core/redux/reducers';
import { reducer as form } from 'redux-form';

// accountDetail
import { reducer as accountCollection } from 'pages/AccountDetails/AccountCollection/_redux';
import { reducer as cardholderMaintenance } from 'pages/AccountDetails/CardHolderMaintenance/_redux';
import { reducer as accountInformation } from 'pages/AccountDetails/AccountInformation/_redux';
import { accountDetailSnapshot } from 'pages/AccountDetails/AccountDetailSnapshot/_redux';
import { reducer as clientContactInfo } from 'pages/AccountDetails/ClientContactInfo/_redux';
import { relatedAccount } from 'pages/AccountDetails/RelatedAccount/_redux/reducer';
import { accountDetail } from 'pages/AccountDetails/_redux/reducer';

// account Search
import { reducer as accountSearchList } from 'pages/AccountSearch/AccountSearchList/_redux/reducers';

// common
import { reducer as tab } from 'pages/__commons/TabBar/_redux';
import { reducer as mapping } from 'pages/__commons/MappingProvider/_redux/reducers';
import { reducer as toastNotifications } from 'pages/__commons/ToastNotifications/_redux';
import { confirmModal } from 'pages/__commons/ConfirmModal/_redux';
import { reducer as i18next } from 'pages/__commons/I18nextProvider/_redux/reducers';
import { reducer as accountDetailTabs } from 'pages/__commons/AccountDetailTabs/_redux';
import { reducer as infoBar } from 'pages/__commons/InfoBar/_redux';
import { renewToken } from 'pages/__commons/RenewToken/_redux/reducer';
import { accessToken } from 'pages/__commons/AccessToken/_redux/reducer';
import { accessConfig } from 'pages/__commons/AccessConfig/_redux/reducer';
import { appConfig } from 'pages/__commons/ESSConfig/_redux/reducers';
import { reducer as sharedInfo } from 'pages/__commons/SharedInfo/_redux/reducer';

import { entitlement } from 'pages/__commons/Entitlement/_redux/reducers';

// Memos
import { reducer as memo } from 'pages/Memos/_redux/reducers';
import { reducer as memoChronicle } from 'pages/Memos/Chronicle/_redux/reducers';
import { reducer as cisMemo } from 'pages/Memos/CIS/_redux/reducers';

// StatementsAndTransactions
import { reducer as pendingAuthorization } from 'pages/StatementsAndTransactions/PendingAuthorizationTransaction/__redux/reducers';
import { reducer as statementTransactionList } from 'pages/StatementsAndTransactions/StatementTransaction/__redux/reducers';

// transaction Adjustment
import { reducer as transactionAdjustment } from 'pages/TransactionAdjustment/RequestList/_redux/reducer';

// Payment
import { reducer as paymentsList } from 'pages/Payment/PaymentList/_redux/reducers';
import { reducer as paymentHistory } from 'pages/Payment/PaymentHistory/__redux/reducers';
import { reducer as payment } from 'pages/Payment/__redux/reducers';

// MakePayment
import { reducer as schedulePayment } from 'pages/MakePayment/SchedulePayment/_redux/reducers';
import { reducer as demandACHPayment } from 'pages/MakePayment/ACHPayment/_redux/reducers';
import { reducer as paymentConfirm } from 'pages/MakePayment/ModalConfirm/_redux/reducers';

// Letters
import { reducer as sendLetter } from 'pages/Letters/SendLetter/_redux/reducers';
import { reducer as letterList } from 'pages/Letters/LetterList/_redux/reducers';
import { reducer as pendingLetter } from 'pages/Letters/PendingLetterList/_redux/reducers';
import { reducer as letter } from 'pages/Letters/_redux/reducers';

// collection Form
import { reducer as collectionForm } from 'pages/CollectionForm/_redux/reducers';
import { reducer as company } from 'pages/CCCS/CompanyList/_redux/reducers';
import { reducer as bankruptcy } from 'pages/Bankruptcy/_redux/reducers';
import { reducer as deceased } from 'pages/Deceased/_redux/reducers';
import { reducer as hardship } from 'pages/HardShip/_redux/reducers';
import { reducer as promiseToPay } from 'pages/PromiseToPay/_redux/reducers';
import { reducer as cccs } from 'pages/CCCS/_redux/reducers';
import { reducer as generalInformation } from 'pages/CollectionForm/CollectionActivities/GeneralInformation/_redux/reducers';
import { reducer as incarceration } from 'pages/Incarceration/_redux/reducers';
import { reducer as longTermMedical } from 'pages/LongTermMedical/_redux/reducers';
import { reducer as settlement } from 'pages/Settlement/_redux/reducers';

import { reducer as timerWorking } from 'pages/TimerWorking/_redux/reducers';

// Reference data
import { reducer as refData } from 'pages/AccountSearch/Home/_redux/reducers';

import { reducer as nextActions } from 'pages/AccountDetails/NextActions/_redux/reducers';
// Caller Authentication
import { reducer as callerAuthentication } from 'pages/CallerAuthenticate/_redux/reducers';

// client configuration
import { reducer as clientConfiguration } from 'pages/ClientConfiguration/_redux/reducers';

import { reducer as adjustmentTransactionConfig } from 'pages/ClientConfiguration/AdjustmentTransaction/_redux/reducers';

import { reducer as clientConfigurationDebtCompanies } from 'pages/ClientConfiguration/DebtManageCompanies/_redux/reducer';

import { reducer as authenticationConfig } from 'pages/ClientConfiguration/Authentication/_redux/reducers';

import { reducer as promiseToPayConfig } from 'pages/ClientConfiguration/PromiseToPay/_redux/reducers';

import { promiseToPayHistory } from 'pages/AccountDetails/PromiseToPayHistory/_redux/reducers';

import { accountExternalStatus } from 'pages/ClientConfiguration/AccountExternalStatus/_redux/reducers';

import { codeToText } from 'pages/ClientConfiguration/CodeToText/_redux/reducers';

import { reference } from 'pages/ClientConfiguration/References/_redux/reducers';

import { reducer as clientConfigCallResult } from 'pages/ClientConfiguration/CallResult/_redux/reducers';

import { reducer as clientConfigurationTalkingPoint } from 'pages/ClientConfiguration/TalkingPoint/_redux/reducers';

import { reducer as letterConfig } from 'pages/ClientConfiguration/Letter/_redux/reducers';

import { reducer as clientConfigClientContactInformation } from 'pages/ClientConfiguration/ClientContactInformation/_redux/reducers';

import { reducer as clientConfigTimer } from 'pages/ClientConfiguration/Timer/_redux/reducers';
import { reducer as paybyPhoneConfig } from 'pages/ClientConfiguration/PayByPhone/_redux/reducers';
import { reducer as settlementPaymentConfig } from 'pages/ClientConfiguration/SettlementPayment/_redux/reducers';
import { reducer as clientConfigClusterToQueueMapping } from 'pages/ClientConfiguration/ClusterToQueueMapping/_redux/reducers';

import { reducer as sendLetterConfig } from 'pages/ClientConfiguration/SendLetter/_redux/reducers';

import { reducer as clientConfigQueueAndRoleMapping } from 'pages/ClientConfiguration/QueueAndRoleMapping/_redux/reducers';

import { reducer as memosConfiguration } from 'pages/ClientConfiguration/Memos/_redux/reducers';

// Configuration Entitlements

import { configurationUserRoleEntitlement } from 'pages/ConfigurationEntitlement/UserRoleEntitlements/_redux/reducer';

import { configurationContactAccountEntitlement } from 'pages/ConfigurationEntitlement/ContactAndAccountEntitlement/_redux/reducer';

import { reducer as changeHistory } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

// dashboard
import { dashboard } from 'pages/AccountSearch/Home/Dashboard/_redux/reducers';

// reporting
import { reducer as reportingTabs } from 'pages/Reporting/ReportingTabs/_redux/reducers';
import { reducer as promisesPaymentsStatistics } from 'pages/Reporting/PromisesPaymentsStatistics/_redux/reducers';
import { reducer as collectorProductivity } from 'pages/Reporting/CollectorProductivity/_redux/reducers';
import { reducer as collectorsTeamsComboBox } from 'pages/Reporting/CollectorsTeamsComboBox/_redux/reducers';

// queueList
import { queueList } from 'pages/QueueList/_redux/reducers';

import { workQueueList } from 'pages/Collector/WorkQueue/_redux/reducers';

import { collectorList } from 'pages/Collector/CollectorList/_redux/reducers';

// function rule mapping
import { reducer as functionRuleMapping } from 'pages/ClientConfiguration/FunctionRuleMapping/_redux/reducers';

// ApiErrorNotification
import { reducer as apiErrorNotification } from 'pages/ApiErrorNotification/_redux/reducers';

import { reducer as userSessionWarning } from 'pages/UserSessionWarning/_redux/reducer';

// exception handling
import { reducer as cspa } from 'pages/ExceptionHandling/CSPAList/_redux/reducers';
import { reducer as caseDetailSearch } from 'pages/ExceptionHandling/CaseDetailSearch/_redux/reducers';
import { reducer as paymentSummary } from 'pages/MakePayment/PaymentSummary/_redux/reducer';
import { reducer as liveVoxReducer } from 'app/livevox-dialer/_redux/reducers';

// Queue Detail
import { reducer as queueAccounts } from 'pages/AccountSearch/Home/Dashboard/QueueDetail/_redux';


export const reducerMappingList = {
  infoBar,
  accountDetailTabs,
  accountCollection,
  accountInformation,
  tab,
  i18next,
  mapping,
  accountSearchList,
  accountDetailSnapshot,
  cisMemo,
  refData,
  toastNotifications,
  memoChronicle,
  memo,
  pendingAuthorization,
  statementTransactionList,
  transactionAdjustment,
  confirmModal,
  schedulePayment,
  payment,
  paymentsList,
  demandACHPayment,
  paymentConfirm,
  cardholderMaintenance,
  paymentHistory,
  clientContactInfo,
  letterList,
  pendingLetter,
  sendLetter,
  letter,
  company,
  form,
  collectionForm,
  bankruptcy,
  deceased,
  hardship,
  promiseToPay,
  promiseToPayHistory,
  cccs,
  renewToken,
  accessToken,
  generalInformation,
  timerWorking,
  nextActions,
  callerAuthentication,
  relatedAccount,
  clientConfiguration,
  adjustmentTransactionConfig,
  authenticationConfig,
  promiseToPayConfig,
  clientConfigTimer,
  longTermMedical,
  accountDetail,
  accountExternalStatus,
  incarceration,
  sharedInfo,
  entitlement,
  codeToText,
  reference,
  clientConfigCallResult,
  clientConfigurationTalkingPoint,
  clientConfigClientContactInformation,
  letterConfig,
  dashboard,
  reportingTabs,
  configurationUserRoleEntitlement,
  configurationContactAccountEntitlement,
  clientConfigurationDebtCompanies,
  queueList,
  workQueueList,
  collectorList,
  promisesPaymentsStatistics,
  collectorProductivity,
  collectorsTeamsComboBox,
  paybyPhoneConfig,
  settlementPaymentConfig,
  clientConfigClusterToQueueMapping,
  changeHistory,
  functionRuleMapping,
  sendLetterConfig,
  clientConfigQueueAndRoleMapping,
  apiErrorNotification,
  userSessionWarning,
  cspa,
  caseDetailSearch,
  memosConfiguration,
  settlement,
  appConfig,
  accessConfig,
  paymentSummary,
  liveVoxReducer,
  queueAccounts
};

export const rootReducer = combineReducers(reducerMappingList);
export type AppState = ReturnType<typeof rootReducer>;

// DOF AppState
export const dofRootReducer = combineReducers({
  ...reducerMappingList,
  caches
});
export type DOFAppState = ReturnType<typeof dofRootReducer>;
