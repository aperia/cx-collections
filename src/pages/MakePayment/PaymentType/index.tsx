import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';

// components
import { Radio } from 'app/_libraries/_dls/components';

// Types
import { PaymentTypeList } from '../types';

// Constants
import { PAYMENT_TYPE } from '../constants';

// Hooks
import { useAccountDetail } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';
import classNames from 'classnames';

export interface PaymentTypeProps {
  onChangePaymentType: (value: string) => void;
}

export const getTypes = (storeId: string): Array<PaymentTypeList> => {
  const types = [];

  if (checkPermission(PERMISSIONS.MAKE_PAYMENT_SCHEDULE_PAYMENT, storeId)) {
    types.push({
      label: 'txt_schedule_payment',
      value: PAYMENT_TYPE.SCHEDULE
    });
  }

  if (checkPermission(PERMISSIONS.MAKE_PAYMENT_MAKE_ON_DEMAND_ACH, storeId)) {
    types.push({
      label: 'txt_on_demand_ach_payment',
      value: PAYMENT_TYPE.DEMAND_ACH
    });
  }
  return types;
};

const PaymentType: React.FC<PaymentTypeProps> = ({ onChangePaymentType }) => {
  const { t } = useTranslation();
  const [type, setType] = useState<string>(PAYMENT_TYPE.SCHEDULE);
  const { storeId } = useAccountDetail();
  const testId = 'account-details_make-payment-modal_payment-type';

  const types = useMemo(() => getTypes(storeId), [storeId]);

  const keepRef = useRef({ onChangePaymentType });
  keepRef.current.onChangePaymentType = onChangePaymentType;

  const handleSelectType = useCallback((value: string) => {
    setType(value);
    keepRef.current.onChangePaymentType(value);
  }, []);

  useEffect(() => {
    if (types.length === 0) return;

    handleSelectType(types[0].value);
  }, [handleSelectType, types]);

  return (
    <div className={classNames(types.length >= 1 && 'p-24 border-bottom bg-light-l20')}>
      {types.length === 1 && <h5>{t(types[0].label)}</h5>}
      {types.length >= 2 && (
        <div className="row mr-lg-46">
          {types.map(({ value, label }) => {
            return (
              <div className="col-xl-3 col-lg-4 col-md-5" key={value}>
                <div
                  className="card card-radio cursor-pointer br-radius-8 p-16"
                  onClick={() => handleSelectType(value)}
                  data-testid={genAmtId(value, testId, '')}
                >
                  <div className="d-flex justify-content-between">
                    <span data-testid={genAmtId(`${value}_${testId}`, 'label', '')}>
                      {t(label)}
                    </span>
                    <Radio dataTestId={genAmtId(`${value}_${testId}`, 'radio', '')}>
                      <Radio.Input
                        value={value}
                        id={`${storeId}-${value}`}
                        checked={type === value}
                        onChange={() => handleSelectType(value)}
                        className="checked-style"
                        name={`payment-type-${storeId}`}
                      />
                    </Radio>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      )}
    </div>
  );
};

export default PaymentType;
