import React from 'react';
import PaymentType, { getTypes } from './';
import { screen } from '@testing-library/react';
import { renderMockStoreId } from 'app/test-utils';
import * as entitlementsHelpers from 'app/entitlements/helpers';
import { PERMISSIONS } from 'app/entitlements/constants';

const onChangeMock = jest.fn();

describe('Test Payment type component', () => {

  beforeEach(() => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
  });

  it('should render without error', () => {
    const { wrapper } = renderMockStoreId(
      <PaymentType onChangePaymentType={onChangeMock} />,
      {}
    );
    expect(wrapper.baseElement).toBeTruthy();
  });

  it('should change type', () => {
    renderMockStoreId(<PaymentType onChangePaymentType={onChangeMock} />, {});

    screen.getAllByRole('radio')[0].click();

    expect(onChangeMock).toBeCalledWith(getTypes('')[0].value);
  });

  it('should select type', () => {
    renderMockStoreId(<PaymentType onChangePaymentType={onChangeMock} />, {});

    screen.getAllByRole('radio')[1].click();

    expect(onChangeMock).toBeCalledWith(getTypes('')[1].value);
  });

  it('should render empty', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(false);
    renderMockStoreId(<PaymentType onChangePaymentType={onChangeMock} />, {});

    expect(screen.queryByText('txt_schedule_payment')).toEqual(null);
  });

  it('should render only Schedule', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission')
      .mockImplementation((permission) => {
        if (permission === PERMISSIONS.MAKE_PAYMENT_MAKE_ON_DEMAND_ACH) return false;

        return true;
      });
    renderMockStoreId(<PaymentType onChangePaymentType={onChangeMock} />, {});

    expect(screen.getByText('txt_schedule_payment')).toBeInTheDocument();
  });

  it('should render only ACHPayment', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission')
      .mockImplementation((permission) => {
        if (permission === PERMISSIONS.MAKE_PAYMENT_SCHEDULE_PAYMENT) return false;

        return true;
      });
    renderMockStoreId(<PaymentType onChangePaymentType={onChangeMock} />, {});

    expect(screen.getByText('txt_on_demand_ach_payment')).toBeInTheDocument();
  });
});
