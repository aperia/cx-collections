import React, { useEffect } from 'react';

// Hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useDispatch } from 'react-redux';

// Redux
import { schedulePaymentActions } from '../_redux/reducers';
import { takeBankAccData } from '../_redux/selectors';

// Component
import PaymentAccounts from '../PaymentAccounts';
import { Button } from 'app/_libraries/_dls/components';

// Type
import { BankAccount } from 'pages/MakePayment/SchedulePayment/types';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

export interface PaymentMethodContextProps {
  onAddAcc?: () => void;
}
export const PaymentMethodContext =
  React.createContext<PaymentMethodContextProps>({});

export interface PaymentMethodProps {
  onAddAccount?: () => void;
}

const PaymentMethod: React.FC<PaymentMethodProps> = ({ onAddAccount }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId, accEValue } = useAccountDetail();

  const bankAccts = useStoreIdSelector<Array<BankAccount>>(takeBankAccData);

  const testId = 'account-details_make-payment-modal_schedule-payment_payment-method';

  const showAddMethod = bankAccts.length !== 0
    && checkPermission(PERMISSIONS.MAKE_PAYMENT_ADD_BANK_REGISTRATION, storeId);

  useEffect(() => {
    dispatch(
      schedulePaymentActions.getBankAccounts({
        storeId,
        postData: {
          accountId: accEValue
        }
      })
    );
  }, [dispatch, storeId, accEValue]);

  return (
    <PaymentMethodContext.Provider
      value={{
        onAddAcc: onAddAccount
      }}
    >
      <>
        <div className="d-flex align-items-center justify-content-between mb-n8">
          <h5 data-testid={genAmtId(testId, 'title', '')}>
            {t(I18N_COMMON_TEXT.PAYMENT_METHOD)}
          </h5>
          {showAddMethod && (
            <Button
              size="sm"
              className="mr-n8"
              variant="outline-primary"
              onClick={onAddAccount}
              dataTestId={genAmtId(testId, 'add-btn', '')}
            >
              {t(I18N_COMMON_TEXT.ADD_PAYMENT_METHOD)}
            </Button>
          )}
        </div>
        <PaymentAccounts data={bankAccts} dataTestId={genAmtId(testId, 'payment-accounts', '')} />
      </>
    </PaymentMethodContext.Provider>
  );
};

export default PaymentMethod;
