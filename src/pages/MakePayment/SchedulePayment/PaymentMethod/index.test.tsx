import React from 'react';

// components
import PaymentMethod from './index';

// helpers
import { AccountDetailProvider } from 'app/hooks';
import { renderMockStore, storeId } from 'app/test-utils';
import { screen } from '@testing-library/react';
import * as entitlementsHelpers from 'app/entitlements/helpers';
import { PERMISSIONS } from 'app/entitlements/constants';

jest.mock('react-redux', () => ({
  ...(jest.requireActual('react-redux') as object),
  useDispatch: () => jest.fn()
}));

const onAddAccount = jest.fn();

const initialState: Partial<RootState> = {
  schedulePayment: {
    [storeId]: {
      bankAccts: [
        {
          accNbr: { eValue: '12', maskedValue: '12' },
          accType: { description: 'unknown description' },
          moreInfo: {
            bankRoutingNbr: 'unknown string',
            bankName: 'unknown',
            bankCity: 'unknown',
            bankState: 'unknown'
          }
        }
      ]
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <PaymentMethod onAddAccount={onAddAccount} />
    </AccountDetailProvider>,
    { initialState }
  );
};
let spy: jest.SpyInstance;

afterEach(() => {
  spy.mockReset();
  spy.mockRestore();
});

describe('render', () => {
  it('Render UI', () => {
    spy = jest
      .spyOn(entitlementsHelpers, 'checkPermission')
      .mockImplementation(permission => {
        if (
          permission === PERMISSIONS.MAKE_PAYMENT_ADD_BANK_REGISTRATION ||
          permission === PERMISSIONS.MAKE_PAYMENT_UPDATE_SAVINGS_ACCOUNT
        )
          return true;
        return false;
      });
    renderWrapper(initialState);

    expect(screen.getByText('txt_payment_method')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('onAddAccount', () => {
    spy = jest
      .spyOn(entitlementsHelpers, 'checkPermission')
      .mockReturnValue(true);
    renderWrapper(initialState);

    screen.getByRole('button').click();

    expect(onAddAccount).toBeCalled();
  });
});
