import schedulePaymentService from './schedulePaymentService';

// utils
import { accEValue, mockApiServices, storeId } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import {
  GetCheckPaymentListPendingStatusRequestBody,
  GetBankAccountsRequestBody,
  DeleteRegisteredAccount,
  BankAccountDetailPayload,
  CancelPendingPaymentPostData,
  ValidatePaymentRequest,
  IMakeAdjustmentTransactionRequestBody
} from './types';

describe('schedulePaymentService', () => {
  describe('checkPaymentListPendingStatus', () => {
    const params: GetCheckPaymentListPendingStatusRequestBody = {
      common: { accountId: storeId, app: 'app' },
      keyType: 'keyType',
      payeeID: 'payeeID',
      registrationID: 'registrationID'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      schedulePaymentService.checkPaymentListPendingStatus(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      schedulePaymentService.checkPaymentListPendingStatus(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.checkPaymentListPendingStatus,
        params
      );
    });
  });

  describe('getBankAccounts', () => {
    const params: GetBankAccountsRequestBody = {
      common: { accountId: storeId, app: 'app' },
      lookupReference: 'lookupReference'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      schedulePaymentService.getBankAccounts(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      schedulePaymentService.getBankAccounts(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.getBankAccounts,
        params
      );
    });
  });

  describe('deleteRegisteredAccount', () => {
    const params: DeleteRegisteredAccount = {
      common: { accountId: storeId, app: 'app' },
      action: 'action',
      lookupReference: 'lookupReference',
      registrationID: 'registrationID'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      schedulePaymentService.deleteRegisteredAccount(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      schedulePaymentService.deleteRegisteredAccount(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.deleteRegisteredAccount,
        params
      );
    });
  });

  describe('submitScheduleMakePayment', () => {
    const params: MagicKeyValue = {
      common: { accountId: storeId }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      schedulePaymentService.submitScheduleMakePayment(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      schedulePaymentService.submitScheduleMakePayment(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.submitScheduleMakePayment,
        params
      );
    });
  });

  describe('getDetailAccountPaymentInfo', () => {
    const params: BankAccountDetailPayload = {
      accountId: storeId,
      eValue: 'eValue'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      schedulePaymentService.getDetailAccountPaymentInfo(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      schedulePaymentService.getDetailAccountPaymentInfo(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.getDetailAccountPaymentInfo,
        params
      );
    });
  });

  describe('getDetailPaymentInfo', () => {
    const params: CommonAccountIdRequestBody = {
      common: { accountId: storeId }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      schedulePaymentService.getDetailPaymentInfo(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      schedulePaymentService.getDetailPaymentInfo(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.getDetailPaymentInfo,
        params
      );
    });
  });

  describe('registerBankAccount', () => {
    const params: MagicKeyValue = {
      accountId: storeId
    };

    it('when data is undefined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      schedulePaymentService.registerBankAccount(
        undefined as unknown as MagicKeyValue
      );

      expect(mockService).not.toBeCalled();
    });

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      schedulePaymentService.registerBankAccount(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      schedulePaymentService.registerBankAccount(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.registerBankAccount,
        params
      );
    });
  });

  describe('validateBankAccountInfo', () => {
    const params: ValidatePaymentRequest = {
      routingNumber: '5765'
    };

    it('when data is undefined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      schedulePaymentService.validateBankAccountInfo(
        undefined as unknown as ValidatePaymentRequest
      );

      expect(mockService).not.toBeCalled();
    });

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      schedulePaymentService.validateBankAccountInfo(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      schedulePaymentService.validateBankAccountInfo(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.validateBankAccountInfo,
        params
      );
    });
  });

  describe('cancelPendingPayments', () => {
    const params: CancelPendingPaymentPostData = {
      accountId: storeId,
      paymentIds: ['id']
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      schedulePaymentService.cancelPendingPayments(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      schedulePaymentService.cancelPendingPayments(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.cancelPendingPayments,
        params
      );
    });
  });

  describe('getAddressInfo', () => {
    const params: MagicKeyValue = {
      accountId: storeId,
      paymentIds: ['id']
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      schedulePaymentService.getAddressInfo(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      schedulePaymentService.getAddressInfo(params);

      expect(mockService).toBeCalledWith(
        apiUrl.cardholdersMaintenance.getAddressInfo,
        params
      );
    });
  });

  describe('makeAdjustmentPayment', () => {
    const requestBody: IMakeAdjustmentTransactionRequestBody = {
      common: {
        accountId: accEValue
      },
      batchType: '05',
      batchPrefix: 'A2',
      transactionCode: '253',
      transactionAmount: '1.00',
      transactionPostingDate: '111621'
    };

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      schedulePaymentService.makeAdjustmentPayment(requestBody);

      expect(mockService).toBeCalledWith(
        apiUrl.transactionAdjustmentRequest.adjustTransaction,
        requestBody
      );
    });
  });
});
