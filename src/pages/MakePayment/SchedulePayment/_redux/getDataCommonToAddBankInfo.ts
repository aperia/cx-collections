import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { MakePaymentResult } from '../types';

// Helper
import accountDetailService from 'pages/AccountDetails/Overview/accountDetailService';

interface Payload {
  data: {
    customerInquiry?: {
      firstName?: string;
      lastName?: string;
      primaryCustomerName?: string;
    }[];
    cardholderInfo?: { zipCode: string }[];
  };
}

interface Args {
  storeId: string;
  postData: {
    accountId: string;
    memberSequenceIdentifier?: string;
  };
}

export const getDataCommonToAddBankInfo = createAsyncThunk<
  Payload,
  Args,
  ThunkAPIConfig
>('schedulePayment/getDataCommonToAddBankInfo', async (args, thunkAPI) => {
  const { accountId, memberSequenceIdentifier } = args.postData;
  const { data } = await accountDetailService.getAccountDetails({
    common: { accountId },
    selectFields: [
      'customerInquiry.firstName',
      'customerInquiry.lastName',
      'cardholderInfo.zipCode',
      'customerInquiry.primaryCustomerName'
    ],
    inquiryFields: { memberSequenceIdentifier }
  });

  return { data };
});

export const splitName = (fullName?: string) => {
  if (typeof fullName !== 'string') return { first: '', last: '', full: '' };
  const [lastName, firstName] = fullName.split(','); // full name: "Smith,Adele"
  const first = firstName.trim();
  const last = lastName.trim();
  return { first, last, full: `${first} ${last}` };
};

export const getDataCommonToAddBankInfoBuilder = (
  builder: ActionReducerMapBuilder<MakePaymentResult>
) => {
  builder
    .addCase(getDataCommonToAddBankInfo.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        bankAccts: [],
        isLoadingCommonDataToAdd: true
      };
    })
    .addCase(getDataCommonToAddBankInfo.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      let commonDataToAdd = {};
      if (data.customerInquiry && data.customerInquiry.length > 0) {
        const { firstName, lastName, primaryCustomerName } =
          data.customerInquiry[0];
        const { first, last, full } = splitName(primaryCustomerName);
        commonDataToAdd = {
          firstName: firstName?.trim() || first,
          lastName: lastName?.trim() || last,
          fullName: full
        };
      }
      if (data.cardholderInfo && data.cardholderInfo.length > 0) {
        commonDataToAdd = { ...commonDataToAdd, ...data.cardholderInfo[0] };
      }

      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingCommonDataToAdd: false,
        commonDataToAdd
      };
    })
    .addCase(getDataCommonToAddBankInfo.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingCommonDataToAdd: false,
        commonDataToAdd: {}
      };
    });
};
