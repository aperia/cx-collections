import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import isEmpty from 'lodash.isempty';

// Type
import { BankAccount, MakePaymentResult } from '../types';

// Service
import schedulePaymentService from '../schedulePaymentService';

// Const
import { EMPTY_STRING } from 'app/constants';

const CAN_DELETED = 'Payment_Pending';

interface Payload {
  data: boolean;
}

export interface Args extends StoreIdPayload {
  postData: {
    accountId: string;
    registrationID: string;
    bankAccount: BankAccount;
  };
}
export const getCheckPaymentListPendingStatus = createAsyncThunk<
  Payload,
  Args,
  ThunkAPIConfig
>(
  'schedulePayment/getCheckPaymentListPendingStatus',
  async (args, thunkAPI) => {
    const { accountId, registrationID } = args.postData;

    const { commonConfig } = window.appConfig || {};

    const { data } = await schedulePaymentService.checkPaymentListPendingStatus(
      {
        common: {
          app: commonConfig?.appCxCollections || EMPTY_STRING,
          accountId
        },
        keyType: 'Site',
        payeeID: accountId,
        registrationID
      }
    );
    
    const canDelete =
      isEmpty(data?.payments) ||
    (data?.payments).every(
        (item: MagicKeyValue) => item.transactionResultCode !== CAN_DELETED
      );

    return { data: canDelete };
  }
);

export const getCheckPaymentListPendingStatusBuilder = (
  builder: ActionReducerMapBuilder<MakePaymentResult>
) => {
  builder
    .addCase(getCheckPaymentListPendingStatus.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingCheckStatus: true,
        bankAccountWithDelete: {},
        canDelete: undefined
      };
    })
    .addCase(
      getCheckPaymentListPendingStatus.fulfilled,
      (draftState, action) => {
        const {
          storeId,
          postData: { bankAccount }
        } = action.meta.arg;
        const { data } = action.payload;

        draftState[storeId] = {
          ...draftState[storeId],
          isLoadingCheckStatus: false,
          canDelete: data,
          bankAccountWithDelete: bankAccount
        };
      }
    )
    .addCase(
      getCheckPaymentListPendingStatus.rejected,
      (draftState, action) => {
        const { storeId } = action.meta.arg;

        draftState[storeId] = {
          ...draftState[storeId],
          isLoadingCheckStatus: false
        };
      }
    );
};
