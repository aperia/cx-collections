import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Service
import schedulePaymentService from '../schedulePaymentService';

// types
import { GetAddressInfoArgs, MakePaymentResult } from '../types';

// constants
import { ADDRESS_CODE, ADDRESS_TYPE_BLL, ADDRESS_TYPE_LETTER, MASSACHUSETTS, RHODE_ISLAND } from '../constants';

// helpers
import forEach from 'lodash.foreach';


export const getAddressInfo = createAsyncThunk<
  MagicKeyValue,
  GetAddressInfoArgs,
  ThunkAPIConfig
>('schedulePayment/getAddressInfo', async (args, thunkAPI) => {
  try {
    const { accountId } = args;
    const requestData = {
      common: {
        accountId
      },
      selectFields: [
        "addressCategoryCode",
        "addressTypeCode",
        "addressSubdivisionOneText"
      ]
    };

    const { data } = await schedulePaymentService.getAddressInfo(requestData);

    return {
      primary: data['primary'] || [{}],
      secondary: data['secondary'] || [{}]
    }
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const getAddressInfoBuilder = (
  builder: ActionReducerMapBuilder<MakePaymentResult>
) => {
  builder
    .addCase(getAddressInfo.pending, (draftState, action) => {})
    .addCase(getAddressInfo.fulfilled, (draftState, action) => {
      const { primary, secondary } = action.payload;
      const { storeId } = action.meta.arg;

      let isDisableDate = false;

      forEach(primary, (item) => {
        const checkPrimary = item?.addressSubdivisionOneText?.trim() === RHODE_ISLAND || item?.addressSubdivisionOneText?.trim() === MASSACHUSETTS;
        const checkLetterPrimary = item.addressCategoryCode === ADDRESS_CODE && item?.addressTypeCode === ADDRESS_TYPE_LETTER;
        const checkBLL1Primary = item.addressCategoryCode === ADDRESS_CODE && item.addressTypeCode === ADDRESS_TYPE_BLL;
        if (checkPrimary && (checkLetterPrimary || checkBLL1Primary)) {
          isDisableDate = true
        }
      })

      forEach(secondary, (item) => {
        const checkSecondary = item?.addressSubdivisionOneText?.trim() === RHODE_ISLAND || item?.addressSubdivisionOneText?.trim() === MASSACHUSETTS;
        const checkLetterSecondary = item.addressCategoryCode === ADDRESS_CODE && item?.addressTypeCode === ADDRESS_TYPE_LETTER;
        const checkBLL1Secondary = item.addressCategoryCode === ADDRESS_CODE && item.addressTypeCode === ADDRESS_TYPE_BLL;
        if (checkSecondary && (checkLetterSecondary || checkBLL1Secondary)) {
          isDisableDate = true
        }
      })

      draftState[storeId] = {
        ...draftState[storeId],
        isDisableDate
      }
    })
    .addCase(getAddressInfo.rejected, (draftState, action) => {});
};
