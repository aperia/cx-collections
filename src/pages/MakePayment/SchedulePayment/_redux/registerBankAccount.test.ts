import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { responseDefault, storeId, mockActionCreator } from 'app/test-utils';
import {
  ValidatePaymentMethodRequest,
  PAYMENT_CARD_TYPE
} from 'pages/MakePayment/SchedulePayment/types';
import { registerBankAccount } from './registerBankAccount';
import { CodeErrorType } from 'app/constants/enums';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

import schedulePaymentService from '../schedulePaymentService';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

const requestData: ValidatePaymentMethodRequest = {
  storeId,
  accountId: storeId,
  bankAccountNumber: '22222222222222222',
  bankAccountType: {
    code: PAYMENT_CARD_TYPE.CHECKING,
    description: PAYMENT_CARD_TYPE.CHECKING
  },
  bankRoutingNumber: '111111111',
  onSuccess: () => undefined,
  onFailure: () => undefined
};

describe('redux-store > registerBankAccount', () => {
  const mockToastSpy = mockActionCreator(actionsToast);
  const apiErrorNotificationActionSpy = mockActionCreator(
    apiErrorNotificationAction
  );
  const memoActionSpy = mockActionCreator(memoActions);

  it('it should be throw error', async () => {
    const store = createStore(rootReducer, {
      schedulePayment: { [storeId]: {} }
    });

    jest
      .spyOn(schedulePaymentService, 'registerBankAccount')
      .mockResolvedValue({
        ...responseDefault,
        data: { message: '', returnCode: 'fail' }
      });

    const data = await registerBankAccount({
      ...requestData
    })(store.dispatch, store.getState, {});

    expect(data.type).toEqual('schedulePayment/addCardPayment/rejected');
  });

  it('success with action memo is ADD BANK', async () => {
    jest
      .spyOn(schedulePaymentService, 'registerBankAccount')
      .mockResolvedValue({
        ...responseDefault,
        data: { message: '', returnCode: 'success' }
      });

    const onSuccess = jest.fn();

    const addToastSpy = mockToastSpy('addToast');
    const clearApiErrorDataSpy =
      apiErrorNotificationActionSpy('clearApiErrorData');
    const memoPostSpy = memoActionSpy('postMemo');

    const store = createStore(rootReducer, {
      schedulePayment: { [storeId]: {} }
    });

    const data = await registerBankAccount({
      ...requestData,
      onSuccess
    })(store.dispatch, store.getState, {});

    expect(data.type).toEqual('schedulePayment/addCardPayment/fulfilled');
    expect(memoPostSpy).toHaveBeenCalled();
    expect(addToastSpy).toBeCalledWith({
      message: 'txt_payment_method_added',
      show: true,
      type: 'success'
    });
    expect(clearApiErrorDataSpy).toBeCalledWith({
      storeId,
      forSection: 'inAddAndEditPaymentMethodModal'
    });
    expect(onSuccess).toBeCalled();
  });

  it('success with action memo is ADD CARD', async () => {
    jest
      .spyOn(schedulePaymentService, 'registerBankAccount')
      .mockResolvedValue({
        ...responseDefault,
        data: { message: '', returnCode: 'success' }
      });

    const onSuccess = jest.fn();

    const addToastSpy = mockToastSpy('addToast');
    const clearApiErrorDataSpy =
      apiErrorNotificationActionSpy('clearApiErrorData');
    const memoPostSpy = memoActionSpy('postMemo');

    const store = createStore(rootReducer, {
      schedulePayment: { [storeId]: {} }
    });

    const customRequestData = { ...requestData };
    customRequestData.bankAccountType = { code: '', description: '' };

    const data = await registerBankAccount({
      ...customRequestData,
      onSuccess
    })(store.dispatch, store.getState, {});

    expect(data.type).toEqual('schedulePayment/addCardPayment/fulfilled');
    expect(memoPostSpy).toHaveBeenCalled();
    expect(addToastSpy).toBeCalledWith({
      message: 'txt_payment_method_added',
      show: true,
      type: 'success'
    });
    expect(clearApiErrorDataSpy).toBeCalledWith({
      storeId,
      forSection: 'inAddAndEditPaymentMethodModal'
    });
    expect(onSuccess).toBeCalled();
  });

  it('error > error 453', async () => {
    // mock
    const mockOnFailure = jest.fn();
    const updateCollectionForm = jest.fn().mockRejectedValue({
      data: {
        coreResponse: {
          message: 'error',
          errorFields: ['field'],
          responseCode: CodeErrorType.ValidationException
        }
      }
    });

    jest
      .spyOn(schedulePaymentService, 'registerBankAccount')
      .mockImplementation(updateCollectionForm);

    const mockAddToast = jest.fn().mockImplementation(() => ({ type: 'type' }));
    jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);

    // store
    const store = createStore(rootReducer, {
      schedulePayment: {
        [storeId]: {}
      }
    });

    // invoke
    await registerBankAccount({ ...requestData, onFailure: mockOnFailure })(
      store.dispatch,
      store.getState,
      {}
    );

    // expect
    expect(mockOnFailure).toBeCalled();
    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: 'txt_payment_method_failed_to_add'
    });
  });

  it('error > other errors SAVING case', async () => {
    const updateCollectionForm = jest.fn().mockRejectedValue({
      data: {
        coreResponse: {
          message: 'this is error',
          errorFields: ['field'],
          responseCode: CodeErrorType.ODSErrorException
        }
      }
    });

    jest
      .spyOn(schedulePaymentService, 'registerBankAccount')
      .mockImplementation(updateCollectionForm);

    const mockAddToast = jest.fn().mockImplementation(() => ({ type: 'type' }));
    jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);

    const store = createStore(rootReducer, {
      schedulePayment: {
        [storeId]: {}
      }
    });

    await registerBankAccount({
      ...requestData,
      bankAccountType: {
        code: PAYMENT_CARD_TYPE.SAVING,
        description: PAYMENT_CARD_TYPE.SAVING
      }
    })(store.dispatch, store.getState, {});

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: 'txt_payment_method_failed_to_add'
    });
  });

  it('error > other errors DEBIT case', async () => {
    const updateCollectionForm = jest.fn().mockRejectedValue({
      data: {
        coreResponse: {
          message: 'this is error',
          errorFields: ['field'],
          responseCode: CodeErrorType.ODSErrorException
        }
      }
    });

    jest
      .spyOn(schedulePaymentService, 'registerBankAccount')
      .mockImplementation(updateCollectionForm);

    const mockAddToast = jest.fn().mockImplementation(() => ({ type: 'type' }));
    jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);

    const store = createStore(rootReducer, {
      schedulePayment: {
        [storeId]: {}
      }
    });

    await registerBankAccount({
      ...requestData,
      bankAccountType: {
        code: PAYMENT_CARD_TYPE.DEBIT,
        description: PAYMENT_CARD_TYPE.DEBIT
      }
    })(store.dispatch, store.getState, {
      paymentService: {
        validateBankAccountInfo: jest.fn().mockRejectedValue({
          data: {
            coreResponse: {
              message: 'this is error',
              errorFields: ['field'],
              responseCode: CodeErrorType.ODSErrorException
            }
          }
        })
      }
    });

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: 'txt_payment_method_failed_to_add'
    });
  });

  it('error > other errors default case', async () => {
    const updateCollectionForm = jest.fn().mockRejectedValue({
      data: {
        coreResponse: {
          message: 'this is error',
          errorFields: ['field'],
          responseCode: CodeErrorType.ODSErrorException
        }
      }
    });

    jest
      .spyOn(schedulePaymentService, 'registerBankAccount')
      .mockImplementation(updateCollectionForm);

    const store = createStore(rootReducer, {
      schedulePayment: {
        [storeId]: {}
      }
    });
    await registerBankAccount({
      ...requestData,
      bankAccountType: {
        code: null as unknown as PAYMENT_CARD_TYPE,
        description: PAYMENT_CARD_TYPE.DEBIT
      }
    })(store.dispatch, store.getState, {
      paymentService: {
        validateBankAccountInfo: jest.fn().mockRejectedValue({
          data: {}
        })
      }
    });
  });
});
