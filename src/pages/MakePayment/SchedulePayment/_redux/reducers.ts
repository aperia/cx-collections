import { MAKE_PAYMENT_FROM } from './../types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Types
import {
  AddPaymentCardProps,
  AllCheckedListPayload,
  MakePaymentResult,
  SelectedAccountPaymentPayload,
  SelectedPaymentInfoPayload,
  SelectedPaymentMethodPayload,
  SelectedPaymentPayload,
  PaymentAmountView
} from '../types';

// Thunk
import { getBankAccountsBuilder, getBankAccounts } from './getBankAccount';
import {
  getBankAccountInfo,
  getBankAccountInfoBuilder
} from './getBankAccountInfo';
import {
  getDetailPaymentInfo,
  getDetailPaymentInfoBuilder
} from './getDetailPaymentInfo';

import {
  deleteRegisteredAccount,
  deleteRegisteredAccountBuilder,
  triggerDeleteRegisteredAccount
} from './deleteRegisteredAccount';

import {
  getVerifyCardPayment,
  getVerifyCardPaymentBuilder
} from './getVerifyCardPayment';

import {
  registerBankAccount,
  registerBankAccountBuilder
} from './registerBankAccount';

import {
  submitMakePayment,
  submitMakePaymentBuilder
} from './submitMakePayment';

import {
  getDataCommonToAddBankInfo,
  getDataCommonToAddBankInfoBuilder
} from './getDataCommonToAddBankInfo';

import {
  getCheckPaymentListPendingStatus,
  getCheckPaymentListPendingStatusBuilder
} from './getCheckPaymentListPendingStatus';

import { getAddressInfo, getAddressInfoBuilder } from './getAddressInformation';

import { AddAccountBankStep } from '../constants';

export const defaultAddPaymentState: AddPaymentCardProps = {
  loading: false,
  errorMessage: '',
  bankInfo: null,
  errorFields: [],
  step: AddAccountBankStep.INIT_INFO
};

const initialState: MakePaymentResult = {};

const { actions, reducer } = createSlice({
  name: 'schedulePayment',
  initialState,
  reducers: {
    clearData: (draffState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      draffState[storeId] = {};
    },
    clearDataSelected: (draffState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      draffState[storeId] = {
        ...draffState[storeId],
        selectedAmount: undefined,
        selectedDate: undefined,
        canDelete: undefined,
        bankAccountWithDelete: {},
        checkedList: [],
        allCheckedList: []
      };
    },
    setAllCheckedList: (
      draffState,
      action: PayloadAction<AllCheckedListPayload>
    ) => {
      const { storeId, allCheckedList = [] } = action.payload;

      const [prevCheckedList] = draffState[storeId].checkedList || [];
      const [firstCheckedList] = allCheckedList;

      const newCheckedList = allCheckedList.includes(prevCheckedList)
        ? prevCheckedList
        : firstCheckedList;

      draffState[storeId] = {
        ...draffState[storeId],
        allCheckedList,
        checkedList: [newCheckedList]
      };
    },
    setCvv2: (
      draffState,
      action: PayloadAction<{ storeId: string; cvv2: string }>
    ) => {
      const { storeId, cvv2 } = action.payload;

      draffState[storeId] = {
        ...draffState[storeId],
        cvv2
      };
    },
    setSelectedPaymentMethod: (
      draffState,
      action: PayloadAction<SelectedPaymentMethodPayload>
    ) => {
      const { storeId, checkedList, method } = action.payload;

      draffState[storeId] = { ...draffState[storeId], checkedList, method };
    },
    setSelectedType: (
      draffState,
      action: PayloadAction<SelectedPaymentPayload>
    ) => {
      const { storeId, id } = action.payload;
      draffState[storeId] = {
        ...draffState[storeId],
        selectedType: id
      };
    },
    setStateAddAccount: (draffState, action: PayloadAction<MagicKeyValue>) => {
      const { storeId, data } = action.payload;
      draffState[storeId] = {
        ...draffState[storeId],
        addAccount: {
          ...draffState[storeId].addAccount,
          ...data
        }
      };
    },
    setSelectedAcc: (
      draffState,
      action: PayloadAction<SelectedAccountPaymentPayload>
    ) => {
      const { storeId, data } = action.payload;
      draffState[storeId] = {
        ...draffState[storeId],
        selectedAcc: data
      };
    },
    toggleModal: (
      draffState,
      action: PayloadAction<{ storeId: string; from?: MAKE_PAYMENT_FROM }>
    ) => {
      const { storeId, from } = action.payload;

      const status = draffState[storeId]?.openModal || false;

      draffState[storeId] = {
        ...draffState[storeId],
        openModal: !status,
        from
      };
    },
    selectedAmount: (
      draffState,
      action: PayloadAction<{ storeId: string; data: PaymentAmountView }>
    ) => {
      const { storeId, data } = action.payload;

      draffState[storeId] = {
        ...draffState[storeId],
        selectedAmount: data
      };
    },
    selectedDate: (
      draffState,
      action: PayloadAction<SelectedPaymentInfoPayload>
    ) => {
      const { storeId, data } = action.payload;

      draffState[storeId] = {
        ...draffState[storeId],
        selectedDate: data
      };
    },
    setErrorBusinessMessage: (
      draftState,
      action: PayloadAction<IInlineMessage>
    ) => {
      const { storeId, message } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        addAccount: {
          ...draftState[storeId]?.addAccount,
          errorMessage: message
        }
      };
    },
    setErrorFields: (draftState, action) => {
      const { storeId, errorFields } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        addAccount: {
          ...draftState[storeId]?.addAccount,
          errorFields: errorFields
        }
      };
    },
    clearError: (draftState, action) => {
      const { storeId } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        addAccount: {
          ...draftState[storeId]?.addAccount,
          errorMessage: defaultAddPaymentState.errorMessage,
          errorFields: defaultAddPaymentState.errorFields
        }
      };
    },
    resetState: (draftState, action) => {
      const { storeId } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        addAccount: {
          ...draftState[storeId]?.addAccount,
          errorMessage: defaultAddPaymentState.errorMessage,
          loading: defaultAddPaymentState.loading,
          errorFields: defaultAddPaymentState.errorFields,
          bankInfo: defaultAddPaymentState.bankInfo
        }
      };
    },
    setIsInvalidSchedule: (draftState, action) => {
      const { storeId, value } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        isInvalidSchedule: value
      };
    },
    setAccountBankStep: (draftState, action) => {
      const { storeId, step } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        addAccount: {
          ...draftState[storeId]?.addAccount,
          step: step
        }
      };
    },
    setAcceptedBalanceAmount: (
      draffState,
      action: PayloadAction<StoreIdPayload & { acceptedBalanceAmount?: string }>
    ) => {
      const { storeId, acceptedBalanceAmount } = action.payload;

      draffState[storeId] = {
        ...draffState[storeId],
        acceptedBalanceAmount: acceptedBalanceAmount
      };
    }
  },
  extraReducers: builder => {
    getCheckPaymentListPendingStatusBuilder(builder);
    getBankAccountsBuilder(builder);
    getBankAccountInfoBuilder(builder);
    getDetailPaymentInfoBuilder(builder);
    deleteRegisteredAccountBuilder(builder);
    getVerifyCardPaymentBuilder(builder);
    registerBankAccountBuilder(builder);
    submitMakePaymentBuilder(builder);
    getDataCommonToAddBankInfoBuilder(builder);
    getAddressInfoBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getCheckPaymentListPendingStatus,
  getDataCommonToAddBankInfo,
  submitMakePayment,
  getBankAccounts,
  getBankAccountInfo,
  getDetailPaymentInfo,
  deleteRegisteredAccount,
  registerBankAccount,
  getVerifyCardPayment,
  triggerDeleteRegisteredAccount,
  getAddressInfo
};

export { allActions as schedulePaymentActions, reducer };
