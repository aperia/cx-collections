import './reducers';
import {
  getCheckPaymentListPendingStatus,
  Args
} from './getCheckPaymentListPendingStatus';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { responseDefault, storeId } from 'app/test-utils';

import schedulePaymentService from '../schedulePaymentService';

const exampleResponseData: Args = {
  storeId,
  postData: { accountId: storeId, bankAccount: {}, registrationID: '213' }
};

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('test getCheckPaymentListPendingStatus', () => {
  const store = createStore(rootReducer, {
    schedulePayment: { [storeId]: {} }
  });
  const state = store.getState();

  it('it should be work for fullFilled', () => {
    const response = getCheckPaymentListPendingStatus.fulfilled(
      {
        data: true
      },
      '',
      exampleResponseData
    );

    expect(response.type).toEqual(
      'schedulePayment/getCheckPaymentListPendingStatus/fulfilled'
    );
    const fullFilled = rootReducer(state, response).schedulePayment;
    expect(fullFilled).toEqual({
      '123456789': {
        bankAccountWithDelete: {},
        canDelete: true,
        isLoadingCheckStatus: false
      }
    });
  });

  it('it should be work for pending', () => {
    const response = getCheckPaymentListPendingStatus.pending('', exampleResponseData);

    expect(response.type).toEqual(
      'schedulePayment/getCheckPaymentListPendingStatus/pending'
    );

    const { schedulePayment } = rootReducer(state, response);
    expect(schedulePayment).toEqual({
      '123456789': {
        bankAccountWithDelete: {},
        canDelete: undefined,
        isLoadingCheckStatus: true
      }
    });
  });

    it('it should be work for rejected', () => {
      const response = getCheckPaymentListPendingStatus.rejected(null,'', exampleResponseData);

      expect(response.type).toEqual(
        'schedulePayment/getCheckPaymentListPendingStatus/rejected'
      );

      const { schedulePayment } = rootReducer(state, response);
      expect(schedulePayment).toEqual({
        '123456789': {
            isLoadingCheckStatus: false
        }
      });
    });

    it('it should be work for schedulePaymentService service correctly', async () => {
      spy = jest
        .spyOn(schedulePaymentService, 'checkPaymentListPendingStatus')
        .mockResolvedValue({
          ...responseDefault,
          data: {
              payments: [{transactionResultCode: 'Payment_Pending'}],
              data: []
          }
        });
      const response = await getCheckPaymentListPendingStatus(exampleResponseData)(
        store.dispatch,
        store.getState,
        {}
      );
      expect(response.type).toEqual(
        'schedulePayment/getCheckPaymentListPendingStatus/fulfilled'
      );
    })
});
