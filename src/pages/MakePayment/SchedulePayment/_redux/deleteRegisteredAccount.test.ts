import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import {
  deleteRegisteredAccount,
  triggerDeleteRegisteredAccount
} from './deleteRegisteredAccount';
import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault,
  storeId
} from 'app/test-utils';
import { DeleteRegisteredAccountArgs } from '../types';

import * as backAccountActions from './getBankAccount';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

import schedulePaymentService from '../schedulePaymentService';
import { isRejected } from '@reduxjs/toolkit';

const requestData: DeleteRegisteredAccountArgs = {
  storeId,
  postData: {
    accountId: storeId,
    bankAccount: {}
  }
};

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

const backAccountActionsSpy = mockActionCreator(backAccountActions);
const actionsToastSpy = mockActionCreator(actionsToast);

describe('deleteRegisteredAccount', () => {
  it('deleteRegisteredAccount fulfilled', async () => {
    spy = jest
      .spyOn(schedulePaymentService, 'deleteRegisteredAccount')
      .mockResolvedValue({
        ...responseDefault,
        data: { data: { message: 'success' } }
      });

    const store = createStore(rootReducer, {
      schedulePayment: {
        [storeId]: {}
      }
    });

    const response = await deleteRegisteredAccount(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ data: { message: 'success' } });
  });

  it('deleteRegisteredAccount rejected', async () => {
    spy = jest
      .spyOn(schedulePaymentService, 'deleteRegisteredAccount')
      .mockRejectedValue({ data: { message: 'failed' } });

    const store = createStore(rootReducer, {
      schedulePayment: {
        [storeId]: {}
      }
    });
    const response = await deleteRegisteredAccount(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(isRejected(response)).toBeTruthy;
  });

  it('triggerDelete success', async () => {
    const mockGetBankAccounts = backAccountActionsSpy('getBankAccounts');
    const mockAddToast = actionsToastSpy('addToast');

    const store = createStore(rootReducer, {
      schedulePayment: {
        [storeId]: {
          bankAccountWithDelete: {
            id: 'id',
            registrationID: '123',
            accType: {
              code: 'CheckCard' as any,
              description: 'string'
            }
          }
        }
      }
    });
    await triggerDeleteRegisteredAccount(requestData)(
      byPassFulfilled(deleteRegisteredAccount.fulfilled.type, {
        data: { message: 'success' }
      }),
      store.getState,
      {
        storeId,
        accountId: storeId,
        registrationID: '123'
      }
    );

    expect(mockGetBankAccounts).toHaveReturnedWith({ type: 'type' });
    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: 'txt_deactivate_success'
    });
  });

  it('triggerDelete success', async () => {
    const mockGetBankAccounts = backAccountActionsSpy('getBankAccounts');
    const mockAddToast = actionsToastSpy('addToast');

    const store = createStore(rootReducer, {
      schedulePayment: {
        [storeId]: {
          bankAccountWithDelete: {
            id: 'id',
            registrationID: '123',
            accType: {
              code: 'echeck' as any,
              description: 'string'
            }
          }
        }
      }
    });
    await triggerDeleteRegisteredAccount(requestData)(
      byPassFulfilled(deleteRegisteredAccount.fulfilled.type, {
        data: { message: 'success' }
      }),
      store.getState,
      {
        storeId,
        accountId: storeId,
        registrationID: '123'
      }
    );

    expect(mockGetBankAccounts).toHaveReturnedWith({ type: 'type' });
    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: 'txt_deactivate_success'
    });
  });

  it('triggerDelete fail', async () => {
    const mockAddToast = actionsToastSpy('addToast');

    const store = createStore(rootReducer, {
      schedulePayment: {
        [storeId]: {
          bankAccountWithDelete: {
            id: 'id',
            registrationID: '123'
          }
        }
      }
    });
    await triggerDeleteRegisteredAccount(requestData)(
      byPassRejected(deleteRegisteredAccount.rejected.type, {
        data: { message: 'success' }
      }),
      store.getState,
      {
        storeId,
        accountId: storeId,
        registrationID: '123'
      }
    );

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: 'txt_deactivate_failed'
    });
  });
});
