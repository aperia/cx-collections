import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// types
import {
  DeleteRegisteredAccountArgs,
  DeleteRegisteredAccountPayload,
  MakePaymentResult,
  PAYMENT_CARD_TYPE
} from '../types';

// Redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { getBankAccounts } from './getBankAccount';

// Service
import schedulePaymentService from '../schedulePaymentService';

// Const
import { EMPTY_STRING } from 'app/constants';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { SCHEDULE_ACTIONS } from 'pages/Memos/constants';
import { formatMaskValueToPost } from '../helpers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { CHRONICLE_PAYMENT_KEY } from 'pages/ClientConfiguration/Memos/constants';

export const deleteRegisteredAccount = createAsyncThunk<
  DeleteRegisteredAccountPayload,
  DeleteRegisteredAccountArgs,
  ThunkAPIConfig
>(
  'schedulePayment/deleteRegisteredAccount',
  async (args, { rejectWithValue }) => {
    const {
      postData: { accountId, registrationID }
    } = args;
    const { commonConfig } = window.appConfig || {};
    try {
      const { data } = await schedulePaymentService.deleteRegisteredAccount({
        common: {
          app: commonConfig?.appCxCollections || EMPTY_STRING,
          accountId
        },
        action: 'disable',
        lookupReference: accountId,
        registrationID: registrationID!
      });
      return data;
    } catch (error) {
      return rejectWithValue({ response: error });
    }
  }
);

export const triggerDeleteRegisteredAccount = createAsyncThunk<
  void,
  DeleteRegisteredAccountArgs,
  ThunkAPIConfig
>('schedulePayment/triggerDelete', async (args, { dispatch, getState }) => {
  const { storeId, postData } = args;
  const { accountId } = postData;
  const states = getState();
  const bankAccount = states.schedulePayment[storeId]?.bankAccountWithDelete;

  const response = await dispatch(
    deleteRegisteredAccount({
      storeId,
      postData: {
        registrationID: bankAccount?.registrationID,
        bankAccount,
        accountId
      }
    })
  );
  if (isFulfilled(response)) {
    batch(() => {
      dispatch(getBankAccounts({ storeId, postData: { accountId } }));
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'txt_deactivate_success'
        })
      );
      // insert memo deactivate
      const type =
        bankAccount?.accType?.code === PAYMENT_CARD_TYPE.CARD_INFO
          ? SCHEDULE_ACTIONS.DEACTIVATED
          : SCHEDULE_ACTIONS.DEACTIVATED_BANK;
          
      const paymentMethod =
        bankAccount?.accType?.code === PAYMENT_CARD_TYPE.CARD_INFO
          ? bankAccount?.accType?.code
          : bankAccount?.accType?.description;
      dispatch(
        memoActions.postMemo(type, {
          storeId,
          accEValue: accountId,
          chronicleKey: CHRONICLE_PAYMENT_KEY,
          makePayment: {
            paymentMethod,
            bankNumber: formatMaskValueToPost(bankAccount?.accNbr?.maskedValue),
            registrationId: bankAccount?.registrationID,
            routingNumber: bankAccount?.moreInfo?.bankRoutingNbr
          }
        })
      );

      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inModalBody'
        })
      );
    });
  }
  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: 'txt_deactivate_failed'
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          apiResponse: response.payload?.response,
          forSection: 'inModalBody'
        })
      );
    });
  }
});

export const deleteRegisteredAccountBuilder = (
  builder: ActionReducerMapBuilder<MakePaymentResult>
) => {
  builder
    .addCase(deleteRegisteredAccount.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        deleteRegisteredAccount: { loading: true }
      };
    })
    .addCase(deleteRegisteredAccount.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].deleteRegisteredAccount!.loading = false;
    })
    .addCase(deleteRegisteredAccount.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].deleteRegisteredAccount!.loading = false;
    });
};
