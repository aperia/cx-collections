import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { mockActionCreator, responseDefault, storeId } from 'app/test-utils';
import {
  PAYMENT_CARD_TYPE,
  VerifyCardPaymentArgs
} from 'pages/MakePayment/SchedulePayment/types';
import { getVerifyCardPayment } from './getVerifyCardPayment';
import { CodeErrorType } from 'app/constants/enums';

import schedulePaymentService from '../schedulePaymentService';

import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

const requestData: VerifyCardPaymentArgs = {
  storeId,
  accountId: storeId,
  bankAccountNumber: '22222222222222222',
  bankRoutingNumber: '111111111'
};

const data = {
  bankRoutingNumber: '',
  bankAccountNumber: '',
  bankAccountType: PAYMENT_CARD_TYPE.DEBIT,
  financialPaymentInfo: {}
};

const toastSpy = mockActionCreator(actionsToast);

describe('redux-store > getVerifyCardPayment', () => {
  const apiErrorNotificationActionSpy = mockActionCreator(
    apiErrorNotificationAction
  );
  const memoActionSpy = mockActionCreator(memoActions);
  it('success', async () => {
    // mock
    const clearApiErrorDataSpy =
      apiErrorNotificationActionSpy('clearApiErrorData');
    const memoPostSpy = memoActionSpy('postMemo');
    jest
      .spyOn(schedulePaymentService, 'validateBankAccountInfo')
      .mockResolvedValue({
        ...responseDefault,
        data
      });

    // store
    const store = createStore(rootReducer, {
      schedulePayment: {
        [storeId]: {}
      }
    });

    // invoke
    const response = await getVerifyCardPayment(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    // expect
    expect(response.payload).toEqual(data.financialPaymentInfo);
    expect(memoPostSpy).toBeCalledWith('VERIFIED_BANK', {
      accEValue: storeId,
      storeId,
      chronicleKey: 'payment',
      makePayment: {
        bankName: undefined,
        bankNumber: '****2222',
        paymentMedium: 'eCheck',
        routingNumber: undefined
      }
    });
    expect(clearApiErrorDataSpy).toBeCalledWith({
      storeId,
      forSection: 'inAddAndEditPaymentMethodModal'
    });
  });

  it('another error', async () => {
    // mock
    const mockAction = toastSpy('addToast');
    jest
      .spyOn(schedulePaymentService, 'validateBankAccountInfo')
      .mockRejectedValue({
        ...responseDefault,
        data: {}
      });

    // store
    const store = createStore(rootReducer, {
      schedulePayment: {
        [storeId]: {}
      }
    });

    // invoke
    const response = await getVerifyCardPayment(requestData)(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig
    );
    // expect
    expect(response.payload).toBeUndefined();
    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'error',
      message: 'txt_verify_failed'
    });
  });

  it('error 453', async () => {
    // mock
    jest
      .spyOn(schedulePaymentService, 'validateBankAccountInfo')
      .mockRejectedValue({
        ...responseDefault,
        data: {
          coreResponse: { responseCode: CodeErrorType.ValidationException }
        }
      });

    // store
    const store = createStore(rootReducer, {
      schedulePayment: {
        [storeId]: {}
      }
    });

    // invoke
    const response = await getVerifyCardPayment(requestData)(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig
    );

    // expect
    expect(response.payload).toBeUndefined();
  });
});
