import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import {
  BankAccountInfoPayload,
  BankAccountInfoArgs,
  MakePaymentResult
} from '../types';

// Helper
import { mappingDataFromObj } from 'app/helpers';

import schedulePaymentService from '../schedulePaymentService';

export const getBankAccountInfo = createAsyncThunk<
  BankAccountInfoPayload,
  BankAccountInfoArgs,
  ThunkAPIConfig
>('schedulePayment/getBankAccountInfo', async (args, thunkAPI) => {
  const { postData } = args;
  const { data } = await schedulePaymentService.getDetailAccountPaymentInfo(
    postData
  );
  const state = thunkAPI.getState();
  const bankAccountMapping = state.mapping?.data?.bankAccountInfo || {};
  const mappingData = mappingDataFromObj(data, bankAccountMapping);

  return {
    data: mappingData
  };
});

export const getBankAccountInfoBuilder = (
  builder: ActionReducerMapBuilder<MakePaymentResult>
) => {
  builder
    .addCase(getBankAccountInfo.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isBankAcctInfoLoading: true
      };
    })
    .addCase(getBankAccountInfo.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isBankAcctInfoLoading: false,
        bankAcctInfo: data
      };
    })
    .addCase(getBankAccountInfo.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isBankAcctInfoLoading: false
      };
    });
};
