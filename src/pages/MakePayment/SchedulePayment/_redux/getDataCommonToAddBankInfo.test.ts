import './reducers';
import { getDataCommonToAddBankInfo } from './getDataCommonToAddBankInfo';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { PaymentInfoArgs } from '../types';
import { responseDefault, storeId } from 'app/test-utils';

import accountDetailService from 'pages/AccountDetails/Overview/accountDetailService';

const requestData: PaymentInfoArgs = {
  storeId,
  postData: { accountId: storeId, memberSequenceIdentifier: 'test' }
};

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('test getDataCommonToAddBankInfoBuilder', () => {
  const store = createStore(rootReducer, {
    schedulePayment: { [storeId]: {} }
  });
  const state = store.getState();

  it('it should be work for fullFilled', () => {
    const response = getDataCommonToAddBankInfo.fulfilled(
      {
        data: {
          customerInquiry: [
            {
              firstName: 'test',
              lastName: 'super',
              primaryCustomerName: 'super,test'
            }
          ],
          cardholderInfo: [{ zipCode: '1000' }]
        }
      },
      '',
      { storeId, postData: { accountId: storeId } }
    );

    expect(response.type).toEqual(
      'schedulePayment/getDataCommonToAddBankInfo/fulfilled'
    );
    const fullFilled = rootReducer(state, response).schedulePayment;
    expect(fullFilled).toEqual({
      '123456789': {
        isLoadingCommonDataToAdd: false,
        commonDataToAdd: {
          firstName: 'test',
          lastName: 'super',
          fullName: 'test super',
          zipCode: '1000'
        }
      }
    });
    const responseEmptyData = getDataCommonToAddBankInfo.fulfilled(
      {
        data: {
          customerInquiry: [],
          cardholderInfo: []
        }
      },
      '',
      { storeId, postData: { accountId: storeId } }
    );

    const emptyFulfilled = rootReducer(
      state,
      responseEmptyData
    ).schedulePayment;
    expect(emptyFulfilled).toEqual({
      '123456789': {
        isLoadingCommonDataToAdd: false,
        commonDataToAdd: {}
      }
    });
  });

  it('it should be work for fullFilled without firstName and lastName', () => {
    const response = getDataCommonToAddBankInfo.fulfilled(
      {
        data: {
          customerInquiry: [
            {
              firstName: '      ',
              lastName: '      ',
              primaryCustomerName: 'super,test'
            }
          ],
          cardholderInfo: [{ zipCode: '1000' }]
        }
      },
      '',
      { storeId, postData: { accountId: storeId } }
    );

    expect(response.type).toEqual(
      'schedulePayment/getDataCommonToAddBankInfo/fulfilled'
    );
    const fullFilled = rootReducer(state, response).schedulePayment;
    expect(fullFilled).toEqual({
      '123456789': {
        isLoadingCommonDataToAdd: false,
        commonDataToAdd: {
          firstName: 'test',
          lastName: 'super',
          fullName: 'test super',
          zipCode: '1000'
        }
      }
    });
    const responseEmptyData = getDataCommonToAddBankInfo.fulfilled(
      {
        data: {
          customerInquiry: [],
          cardholderInfo: []
        }
      },
      '',
      { storeId, postData: { accountId: storeId } }
    );

    const emptyFulfilled = rootReducer(
      state,
      responseEmptyData
    ).schedulePayment;
    expect(emptyFulfilled).toEqual({
      '123456789': {
        isLoadingCommonDataToAdd: false,
        commonDataToAdd: {}
      }
    });
  });

  it('it should be work for pending', () => {
    const response = getDataCommonToAddBankInfo.pending('', {
      storeId,
      postData: { accountId: storeId }
    });

    expect(response.type).toEqual(
      'schedulePayment/getDataCommonToAddBankInfo/pending'
    );

    const { schedulePayment } = rootReducer(state, response);
    expect(schedulePayment).toEqual({
      '123456789': {
        isLoadingCommonDataToAdd: true,
        bankAccts: []
      }
    });
  });

  it('it should be work for rejected', () => {
    const response = getDataCommonToAddBankInfo.rejected(null, '', {
      storeId,
      postData: { accountId: storeId }
    });

    expect(response.type).toEqual(
      'schedulePayment/getDataCommonToAddBankInfo/rejected'
    );

    const { schedulePayment } = rootReducer(state, response);
    expect(schedulePayment).toEqual({
      '123456789': {
        isLoadingCommonDataToAdd: false,
        commonDataToAdd: {}
      }
    });
  });

  it('it should be work for getDataCommonToAddBankInfo service correctly', async () => {
    spy = jest
      .spyOn(accountDetailService, 'getAccountDetails')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          customerInquiry: [
            {
              firstName: 'test',
              lastName: 'super'
            }
          ]
        }
      });
    const response = await getDataCommonToAddBankInfo(requestData)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'schedulePayment/getDataCommonToAddBankInfo/fulfilled'
    );
  });
});
