import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import isEmpty from 'lodash.isempty';

// Type
import {
  PAYMENT_CARD_TYPE,
  BankAccountPayload,
  BankAccountArgs,
  MakePaymentResult
} from '../types';

// types
import isUndefined from 'lodash.isundefined';

// Helper
import { mappingDataFromObj, parseRawData } from 'app/helpers';

// Service
import schedulePaymentService from '../schedulePaymentService';

// Const
import { EMPTY_STRING } from 'app/constants';

export const mapRawDataToView = (
  data: MagicKeyValue[],
  bankAccountMapping: MagicKeyValue,
  cardInfoModalMapping: MagicKeyValue
) => {
  return data
    .filter(
      item =>
        !(
          item.registration.registrationFlags === 'Disabled' ||
          (isEmpty(item.financialPaymentInfoResponse) &&
            item.registration.paymentInfo.paymentMedium === 'eCheck')
        )
    )
    .map((item, index) => {
      const { paymentMedium, paymentInfoCC, paymentInfoEFT } =
        item.registration.paymentInfo;
      const id = item.registration.registrationID;
      let result = {};

      switch (paymentMedium) {
        case PAYMENT_CARD_TYPE.CARD_INFO: {
          const temp = { paymentMedium, ...paymentInfoCC };
          result = {
            ...item,
            id: `${id}-${index}`,
            registration: {
              ...item.registration,
              paymentInfo: parseRawData(temp)
            }
          };
          result = mappingDataFromObj(result, cardInfoModalMapping);
          break;
        }

        case PAYMENT_CARD_TYPE.BANK_INFO:
        default: {
          const temp = { paymentMedium, ...paymentInfoEFT };
          result = {
            ...item,
            id: `${id}-${index}`,
            registration: {
              ...item.registration,
              paymentInfo: parseRawData(temp)
            }
          };
          result = mappingDataFromObj(result, bankAccountMapping);
        }
      }
      return result;
    });
};

export const getBankAccounts = createAsyncThunk<
  BankAccountPayload,
  BankAccountArgs,
  ThunkAPIConfig
>('schedulePayment/getBankAccounts', async (args, thunkAPI) => {
  const { accountId } = args.postData;
  const { commonConfig } = window.appConfig || {};

  const { data } = await schedulePaymentService.getBankAccounts({
    common: { app: commonConfig?.appCxCollections || EMPTY_STRING, accountId },
    lookupReference: accountId
  });
  const state = thunkAPI.getState();
  const bankAccountMapping = state.mapping?.data?.bankAccount || {};
  const cardInfoModalMapping = state.mapping?.data?.cardInfoModal || {};

  const result = mapRawDataToView(
    data.cxRegistrations,
    bankAccountMapping,
    cardInfoModalMapping
  );

  return { data: result };
});

export const getBankAccountsBuilder = (
  builder: ActionReducerMapBuilder<MakePaymentResult>
) => {
  builder
    .addCase(getBankAccounts.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      if (isUndefined(draftState[storeId])) {
        draftState[storeId] = {
          bankAccts: [],
          isBankAcctsLoading: true
        };
      } else {
        draftState[storeId].bankAccts = [];
        draftState[storeId].isBankAcctsLoading = true;
      }
    })
    .addCase(getBankAccounts.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data = [] } = action.payload;

      const sortData = data.reverse();

      draftState[storeId] = {
        ...draftState[storeId],
        selectedAcc: sortData[0] || {},
        bankAccts: sortData,
        checkedList: [],
        isBankAcctsLoading: false
      };
    })
    .addCase(getBankAccounts.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].isBankAcctsLoading = false;
    });
};
