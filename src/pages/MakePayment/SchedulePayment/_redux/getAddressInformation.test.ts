import { responseDefault, storeId } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { ADDRESS_CODE, ADDRESS_TYPE_BLL, MASSACHUSETTS } from '../constants';
import schedulePaymentService from '../schedulePaymentService';
import { getAddressInfo } from './getAddressInformation';

let spy: jest.SpyInstance;

afterEach(() => {
  spy.mockReset();
  spy.mockRestore();
});

describe('test getAddressInfo', () => {
  it('should fulfilled', async () => {
    spy = jest
      .spyOn(schedulePaymentService, 'getAddressInfo')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          responseStatus: 'success',
          primary: [
            {
              addressSubdivisionOneText: MASSACHUSETTS,
              addressCategoryCode: ADDRESS_CODE,
              addressTypeCode: ADDRESS_TYPE_BLL
            }
          ],
          secondary: [
            {
              addressSubdivisionOneText: MASSACHUSETTS,
              addressCategoryCode: ADDRESS_CODE,
              addressTypeCode: ADDRESS_TYPE_BLL
            }
          ]
        }
      });
    const store = createStore(rootReducer, {});
    const response = await getAddressInfo({
      storeId,
      accountId: '12345'
    })(store.dispatch, store.getState, {});
    expect(response.type).toEqual('schedulePayment/getAddressInfo/fulfilled');
    expect(response?.payload).toEqual({
      primary: [
        {
          addressSubdivisionOneText: MASSACHUSETTS,
          addressCategoryCode: ADDRESS_CODE,
          addressTypeCode: ADDRESS_TYPE_BLL
        }
      ],
      secondary: [
        {
          addressSubdivisionOneText: MASSACHUSETTS,
          addressCategoryCode: ADDRESS_CODE,
          addressTypeCode: ADDRESS_TYPE_BLL
        }
      ]
    });
  });

  it('should fulfilled empty data', async () => {
    spy = jest
      .spyOn(schedulePaymentService, 'getAddressInfo')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          responseStatus: 'success'
        }
      });
    const store = createStore(rootReducer, {});
    const response = await getAddressInfo({
      storeId,
      accountId: '12345'
    })(store.dispatch, store.getState, {});
    expect(response.type).toEqual('schedulePayment/getAddressInfo/fulfilled');
    expect(response?.payload).toEqual({
      primary: [{}],
      secondary: [{}]
    });
  });

  it('should rejected', async () => {
    spy = jest
      .spyOn(schedulePaymentService, 'getAddressInfo')
      .mockRejectedValue({});
    const store = createStore(rootReducer, {});
    const response = await getAddressInfo({
      storeId,
      accountId: '12345'
    })(store.dispatch, store.getState, {});
    expect(response.type).toEqual('schedulePayment/getAddressInfo/rejected');
    expect(response?.payload).toEqual({});
  });
});
