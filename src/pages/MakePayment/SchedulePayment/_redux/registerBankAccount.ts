import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import dateTime from 'date-and-time';
import isFunction from 'lodash.isfunction';

// Type
import { MakePaymentResult, PAYMENT_CARD_TYPE } from '../types';

// Redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// Service
import schedulePaymentService from '../schedulePaymentService';
import { batch } from 'react-redux';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { SCHEDULE_ACTIONS } from 'pages/Memos/constants';
import { formatMaskValueToPost } from '../helpers';
import { formatTime } from 'app/helpers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { CHRONICLE_PAYMENT_KEY } from 'pages/ClientConfiguration/Memos/constants';

const mapDataToRequest = (
  args: MagicKeyValue,
  commonDataToAdd: MagicKeyValue = {},
  commonConfig?: CommonConfig
) => {
  const {
    accountId,
    // data for Bank info
    bankRoutingNumber,
    bankAccountNumber,
    bankAccountType,

    // data for Card info
    cardDebitNumber
  } = args;
  const { firstName, lastName, zipCode: zip, fullName } = commonDataToAdd;
  const nameFull = fullName;

  let postData: MagicKeyValue = {
    common: {
      app: commonConfig?.appCxCollections,
      x500id: commonConfig?.x500id,
      accountId: accountId
    },
    lookupReference: accountId,
    action: 'create'
  };

  // create post data for bank info
  if (bankAccountType && bankAccountType?.code) {
    postData = {
      ...postData,
      paymentInfo: {
        paymentMedium: PAYMENT_CARD_TYPE.BANK_INFO,
        paymentInfoEFT: {
          accountType: bankAccountType?.code,
          bankRoutingNumber,
          bankAccountNumber,
          // hard code
          authorizationMedium: 'Voice',

          addressBilling: { nameFirst: firstName, nameLast: lastName, nameFull }
        }
      }
    };
  } else {
    const expirationDate = dateTime.format(
      new Date(args.expirationDate),
      'MMYY'
    );
    postData = {
      ...postData,
      paymentInfo: {
        paymentMedium: PAYMENT_CARD_TYPE.CARD_INFO,
        paymentInfoCC: {
          cardType: 'MC',
          cardNumber: cardDebitNumber,
          expirationDate,
          billingAddress: { nameFull, zip }
        }
      }
    };
  }
  return postData;
};

export const registerBankAccount = createAsyncThunk<
  unknown,
  MagicKeyValue,
  ThunkAPIConfig
>('schedulePayment/addCardPayment', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;
  const { storeId, onSuccess, accountId } = args;
  const states = getState();
  const currentState = states.schedulePayment[storeId];

  try {
    const { commonConfig } = window.appConfig || {};

    const postData = mapDataToRequest(
      args,
      currentState?.commonDataToAdd,
      commonConfig
    );
    const response = await schedulePaymentService.registerBankAccount(postData);

    if (response?.data?.returnCode?.toLowerCase() !== 'success') {
      throw response;
    }
    const isAddBank =
      postData.paymentInfo.paymentMedium === PAYMENT_CARD_TYPE.BANK_INFO;
    const actionMemo = isAddBank
      ? SCHEDULE_ACTIONS.ADD_BANK
      : SCHEDULE_ACTIONS.ADD_CARD;
    batch(() => {
      dispatch(
        memoActions.postMemo(actionMemo, {
          storeId,
          accEValue: accountId,
          chronicleKey: CHRONICLE_PAYMENT_KEY,
          makePayment: isAddBank
            ? {
                routingNumber: args.bankRoutingNumber,
                paymentMedium: postData.paymentInfo.paymentMedium,
                typeMethod: args?.bankAccountType?.description,
                methodAcc: formatMaskValueToPost(args?.bankAccountNumber),
                bankName: args.bankName,
                registrationId: response?.data?.registrationID
              }
            : {
                cardNumber: postData.paymentInfo.paymentInfoCC.cardNumber,
                expirationDate: formatTime(args.expirationDate)
                  .shortMonthShortYear,
                paymentMedium: postData.paymentInfo.paymentMedium
              }
        })
      );
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'txt_payment_method_added'
        })
      );
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inAddAndEditPaymentMethodModal'
        })
      );

      onSuccess && onSuccess();
    });
  } catch (error) {
    const { onFailure } = args;
    batch(() => {
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inAddAndEditPaymentMethodModal',
          apiResponse: error
        })
      );
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: 'txt_payment_method_failed_to_add'
        })
      );
    });
    isFunction(onFailure) && onFailure();
    throw new Error(error);
  }
});

export const registerBankAccountBuilder = (
  builder: ActionReducerMapBuilder<MakePaymentResult>
) => {
  builder.addCase(registerBankAccount.pending, (draftState, action) => {
    const { storeId } = action.meta.arg;

    draftState[storeId] = {
      ...draftState[storeId],
      addAccount: {
        ...draftState[storeId]?.addAccount,
        loading: true
      }
    };
  });
  builder.addCase(registerBankAccount.fulfilled, (draftState, action) => {
    const { storeId } = action.meta.arg;

    draftState[storeId] = {
      ...draftState[storeId],
      addAccount: {
        ...draftState[storeId]?.addAccount,
        loading: false
      }
    };
  });
  builder.addCase(registerBankAccount.rejected, (draftState, action) => {
    const { storeId } = action.meta.arg;

    draftState[storeId] = {
      ...draftState[storeId],
      addAccount: {
        ...draftState[storeId]?.addAccount,
        loading: false
      }
    };
  });
};
