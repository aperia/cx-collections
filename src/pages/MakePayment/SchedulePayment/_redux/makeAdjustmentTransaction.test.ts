import { FormatTime } from 'app/constants/enums';
import { formatTimeDefault } from 'app/helpers';
import { accEValue, responseDefault, storeId } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import schedulePaymentService from '../schedulePaymentService';
import { IMakeAdjustmentTransactionRequestBody } from '../types';
import { makeAdjustmentTransaction } from './makeAdjustmentTransaction';

describe('makeAdjustmentTransaction', () => {
  const store = createStore(rootReducer, {
    schedulePayment: { [storeId]: {} }
  });
  it('should be called', async () => {
    const spy = jest
      .spyOn(schedulePaymentService, 'makeAdjustmentPayment')
      .mockResolvedValue({
        ...responseDefault
      });

    const requestBody: IMakeAdjustmentTransactionRequestBody = {
      common: {
        accountId: accEValue
      },
      batchType: '05',
      batchPrefix: 'PZ',
      transactionCode: '253',
      transactionAmount: '1.00',
      transactionPostingDate: formatTimeDefault(
        new Date(),
        FormatTime.DefaultShortPostDate
      )!
    };

    await makeAdjustmentTransaction({
      accountId: accEValue,
      feeAmount: '1.00'
    })(store.dispatch, store.getState, {});

    expect(spy).toBeCalledWith(requestBody);

    spy.mockReset();
    spy.mockRestore();
  });
});
