import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// types
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import {
  PAYMENT_CARD_TYPE,
  VerifyCardPaymentArgs
} from 'pages/MakePayment/SchedulePayment/types';
import {
  MakePaymentResult,
  AddPaymentCardProps,
  ValidatePaymentResponse
} from '../types';

// constants
import { CodeErrorType } from 'app/constants/enums';
import { AddAccountBankStep } from '../constants';

// Service
import schedulePaymentService from '../schedulePaymentService';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { SCHEDULE_ACTIONS } from 'pages/Memos/constants';
import { formatMaskValueToPost } from '../helpers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { CHRONICLE_PAYMENT_KEY } from 'pages/ClientConfiguration/Memos/constants';

const defaultAddPaymentState: AddPaymentCardProps = {
  loading: false,
  errorMessage: '',
  bankInfo: null,
  errorFields: [],
  step: AddAccountBankStep.INIT_INFO
};

const mapRawDataToView = (data: MagicKeyValue) => {
  const {
    accountType,
    bankAccountType,
    bankAccountNumber,
    bankRoutingNumber,
    institutionCity: bankCity,
    institutionNameAbbreviation: bankName,
    institutionState: bankState
  } = data;

  return {
    bankAccountType,
    accountType,
    bankAccountNumber,
    bankRoutingNumber,
    bankCity,
    bankName,
    bankState
  };
};

export const getVerifyCardPayment = createAsyncThunk<
  ValidatePaymentResponse,
  VerifyCardPaymentArgs,
  ThunkAPIConfig
>(
  'schedulePayment/getVerifyCardPayment',
  async (args: VerifyCardPaymentArgs, thunkAPI) => {
    const { storeId, accountId } = args;
    const { dispatch } = thunkAPI;

    try {
      const postData = {
        routingNumber: args.bankRoutingNumber
      };
      const response = await schedulePaymentService.validateBankAccountInfo(
        postData
      );

      batch(() => {
        dispatch(
          apiErrorNotificationAction.clearApiErrorData({
            storeId,
            forSection: 'inAddAndEditPaymentMethodModal'
          })
        );
        dispatch(
          memoActions.postMemo(SCHEDULE_ACTIONS.VERIFIED_BANK, {
            storeId,
            accEValue: accountId,
            chronicleKey: CHRONICLE_PAYMENT_KEY,
            makePayment: {
              bankName:
                response?.data?.financialPaymentInfo?.institutionFullName,
              routingNumber:
                response?.data?.financialPaymentInfo?.routingNumber,
              bankNumber: formatMaskValueToPost(args.bankAccountNumber),
              paymentMedium: PAYMENT_CARD_TYPE.BANK_INFO
            }
          })
        );
      });

      return response?.data?.financialPaymentInfo;
    } catch (error) {
      const responseCode = error?.data?.coreResponse?.responseCode;

      if (responseCode !== CodeErrorType.ValidationException) {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'error',
            message: 'txt_verify_failed'
          })
        );
      }

      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inAddAndEditPaymentMethodModal',
          apiResponse: error
        })
      );

      throw error;
    }
  }
);

export const getVerifyCardPaymentBuilder = (
  builder: ActionReducerMapBuilder<MakePaymentResult>
) => {
  builder.addCase(getVerifyCardPayment.pending, (draftState, action) => {
    const { storeId } = action.meta.arg;

    draftState[storeId] = {
      ...draftState[storeId],
      addAccount: {
        ...draftState[storeId]?.addAccount,
        loading: true
      }
    };
  });
  builder.addCase(getVerifyCardPayment.fulfilled, (draftState, action) => {
    const { storeId } = action.meta.arg;
    // clear error
    draftState[storeId] = {
      ...draftState[storeId],
      addAccount: {
        ...draftState[storeId]?.addAccount,
        errorMessage: defaultAddPaymentState.errorMessage,
        errorFields: defaultAddPaymentState.errorFields
      }
    };

    draftState[storeId] = {
      ...draftState[storeId],
      addAccount: {
        ...draftState[storeId]?.addAccount,
        loading: false,
        step: AddAccountBankStep.VERIFY_INFO,
        bankInfo: mapRawDataToView({
          ...action.payload,
          ...action.meta.arg
        })
      }
    };
  });
  builder.addCase(getVerifyCardPayment.rejected, (draftState, action) => {
    const { storeId } = action.meta.arg;
    draftState[storeId] = {
      ...draftState[storeId],
      addAccount: {
        ...draftState[storeId]?.addAccount,
        step: AddAccountBankStep.FILL_INFO,
        loading: false
      }
    };
  });
};
