import {
  schedulePaymentActions,
  reducer,
  defaultAddPaymentState
} from './reducers';
import { MakePaymentResult } from '../types';
import { storeId } from 'app/test-utils';
import { PAYMENT_CARD_TYPE } from 'pages/MakePayment/SchedulePayment/types';

describe('redux-store > make-payment', () => {
  let initState: MakePaymentResult;
  beforeEach(() => {
    initState = { [storeId]: {} };
  });

  it('clearData', () => {
    const action = schedulePaymentActions.clearData({ storeId });
    const nextState = reducer(initState, action);
    expect(nextState[storeId]).toEqual({});
  });

  it('clearDataSelected', () => {
    const action = schedulePaymentActions.clearDataSelected({ storeId });
    const nextState = reducer(initState, action);
    expect(nextState[storeId]).toEqual({
      bankAccountWithDelete: {},
      canDelete: undefined,
      selectedAmount: undefined,
      selectedDate: undefined,
      checkedList: [],
      allCheckedList: []
    });
  });

  describe('setAllCheckedList', () => {
    it('with state', () => {
      const action = schedulePaymentActions.setAllCheckedList({
        storeId,
        allCheckedList: ['row_1', 'row_2']
      });
      const nextState = reducer(
        { [storeId]: { checkedList: ['row_1'] } },
        action
      );
      expect(nextState[storeId]).toEqual({
        checkedList: ['row_1'],
        allCheckedList: ['row_1', 'row_2']
      });
    });

    it('with empty state', () => {
      const action = schedulePaymentActions.setAllCheckedList({
        storeId,
        allCheckedList: ['row_1', 'row_2']
      });
      const nextState = reducer({ [storeId]: {} }, action);
      expect(nextState[storeId]).toEqual({
        checkedList: ['row_1'],
        allCheckedList: ['row_1', 'row_2']
      });
    });

    it('without allCheckedList', () => {
      const action = schedulePaymentActions.setAllCheckedList({
        storeId,
        allCheckedList: undefined as unknown as string[]
      });
      const nextState = reducer({ [storeId]: {} }, action);
      expect(nextState[storeId]).toEqual({
        checkedList: [undefined],
        allCheckedList: []
      });
    });
  });

  it('setSelectedPaymentMethod', () => {
    const action = schedulePaymentActions.setSelectedPaymentMethod({
      checkedList: ['row_1'],
      storeId
    });
    const nextState = reducer(initState, action);
    expect(nextState[storeId]).toEqual({ checkedList: ['row_1'] });
  });

  it('setFee', () => {
    const data = { fee: 1 };
    const action = schedulePaymentActions.setStateAddAccount({
      storeId,
      data
    });
    const nextState = reducer(initState, action);
    expect(nextState[storeId]).toEqual({ addAccount: { ...data } });
  });

  it('setSelectedType', () => {
    const action = schedulePaymentActions.setSelectedType({
      storeId,
      id: 'id'
    });
    const nextState = reducer(initState, action);
    expect(nextState[storeId].selectedType).toEqual('id');
  });

  it('setSelectedAcc', () => {
    const action = schedulePaymentActions.setSelectedAcc({
      storeId,
      data: {
        accType: { description: 'accType', code: PAYMENT_CARD_TYPE.CHECKING }
      }
    });
    const nextState = reducer(initState, action);
    expect(nextState[storeId].selectedAcc).toEqual({
      accType: { description: 'accType', code: PAYMENT_CARD_TYPE.CHECKING }
    });
  });

  it('toggleModal', () => {
    const action = schedulePaymentActions.toggleModal({
      storeId
    });
    const nextState = reducer(initState, action);
    expect(nextState[storeId].openModal).toEqual(true);
  });

  it('toggleModal has default value', () => {
    initState = {
      [storeId]: {
        openModal: true
      }
    };
    const action = schedulePaymentActions.toggleModal({
      storeId
    });
    const nextState = reducer(initState, action);
    expect(nextState[storeId].openModal).toEqual(false);
  });

  it('selectedAmount', () => {
    const action = schedulePaymentActions.selectedAmount({
      storeId,
      data: {
        id: 'id',
        value: 'value'
      }
    });
    const nextState = reducer(initState, action);
    expect(nextState[storeId].selectedAmount).toEqual({
      id: 'id',
      value: 'value'
    });
  });

  it('selectedDate', () => {
    const value = new Date();
    const action = schedulePaymentActions.selectedDate({
      storeId,
      data: {
        id: 'id',
        value
      }
    });
    const nextState = reducer(initState, action);
    expect(nextState[storeId].selectedDate).toEqual({ id: 'id', value });
  });

  it('setErrorBusinessMessage', () => {
    const action = schedulePaymentActions.setErrorBusinessMessage({
      storeId,
      message: 'message'
    });
    const nextState = reducer(initState, action);
    expect(nextState[storeId]).toEqual({
      addAccount: { errorMessage: 'message' }
    });
  });

  it('setErrorFields', () => {
    const action = schedulePaymentActions.setErrorFields({
      storeId,
      errorFields: 'errorFields'
    });
    const nextState = reducer(initState, action);
    expect(nextState[storeId]).toEqual({
      addAccount: { errorFields: 'errorFields' }
    });
  });

  it('clearError', () => {
    const action = schedulePaymentActions.clearError({ storeId });
    const nextState = reducer(initState, action);
    expect(nextState[storeId]).toEqual({
      addAccount: {
        errorMessage: defaultAddPaymentState.errorMessage,
        errorFields: defaultAddPaymentState.errorFields
      }
    });
  });

  it('resetState', () => {
    const action = schedulePaymentActions.resetState({ storeId });
    const nextState = reducer(initState, action);
    expect(nextState[storeId]).toEqual({
      addAccount: {
        errorMessage: defaultAddPaymentState.errorMessage,
        loading: defaultAddPaymentState.loading,
        errorFields: defaultAddPaymentState.errorFields,
        bankInfo: defaultAddPaymentState.bankInfo
      }
    });
  });

  it('setAccountBankStep', () => {
    const action = schedulePaymentActions.setAccountBankStep({
      storeId,
      step: '2'
    });
    const nextState = reducer(initState, action);
    expect(nextState[storeId]).toEqual({ addAccount: { step: '2' } });
  });

  it('setCvv2', () => {
    const cvv2 = '000';
    const action = schedulePaymentActions.setCvv2({ storeId, cvv2 });
    const nextState = reducer(initState, action);
    expect(nextState[storeId]).toEqual({ cvv2 });
  });

  it('it should be call setIsInvalidSchedule correctly', () => {
    const action = schedulePaymentActions.setIsInvalidSchedule({
      storeId,
      value: true
    });
    const nextState = reducer(initState, action);
    expect(nextState[storeId]).toEqual({ isInvalidSchedule: true });
  });

  it('it should be call setAcceptedBalanceAmount', () => {
    const acceptedBalanceAmount = 'acceptedBalanceAmount';
    const action = schedulePaymentActions.setAcceptedBalanceAmount({
      storeId,
      acceptedBalanceAmount
    });
    const nextState = reducer(initState, action);
    expect(nextState[storeId].acceptedBalanceAmount).toEqual(
      acceptedBalanceAmount
    );
  });
});
