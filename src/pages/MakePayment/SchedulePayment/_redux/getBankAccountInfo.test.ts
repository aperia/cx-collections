import './reducers';
import { getBankAccountInfo } from './getBankAccountInfo';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { BankAccountInfoArgs } from '../types';
import { responseDefault, storeId } from 'app/test-utils';

import schedulePaymentService from '../schedulePaymentService';

const requestData: BankAccountInfoArgs = {
  storeId,
  postData: {
    eValue: 'eValue',
    accountId: storeId
  }
};

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('redux-store > getBankAccountInfo', () => {
  it('success', async () => {
    spy = jest
      .spyOn(schedulePaymentService, 'getDetailAccountPaymentInfo')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    const store = createStore(rootReducer, {
      mapping: {
        data: {
          bankAccount: {}
        }
      },
      schedulePayment: {
        [storeId]: {}
      }
    });

    const response = await getBankAccountInfo(requestData)(
      store.dispatch,
      store.getState,
      (undefined as unknown) as ThunkAPIConfig['extra']
    );

    expect(response.payload).toEqual({ data: {} });
  });

  it('error', async () => {
    spy = jest
      .spyOn(schedulePaymentService, 'getDetailAccountPaymentInfo')
      .mockRejectedValue({
        ...responseDefault,
        data: {}
      });

    const store = createStore(rootReducer, {
      mapping: {
        data: {
          bankAccount: {}
        }
      },
      schedulePayment: {
        [storeId]: {}
      }
    });

    const response = await getBankAccountInfo(requestData)(
      store.dispatch,
      store.getState,
      (undefined as unknown) as ThunkAPIConfig['extra']
    );

    expect(response.payload).toBeUndefined();
  });
});
