import './reducers';
import { getBankAccounts, mapRawDataToView } from './getBankAccount';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { BankAccountArgs, PAYMENT_CARD_TYPE } from '../types';
import { reducer } from '../_redux/reducers';
import { responseDefault, storeId } from 'app/test-utils';
import schedulePaymentService from '../schedulePaymentService';

const requestData: BankAccountArgs = {
  storeId,
  postData: { accountId: storeId }
};

let spy: jest.SpyInstance;

const cxRegistrations = [
  {
    registration: {
      registrationID: '',
      registrationFlags: 'Enable',
      paymentInfo: {
        paymentMedium: PAYMENT_CARD_TYPE.CARD_INFO,
        paymentInfoCC: '123',
        paymentInfoEFT: 'testEft'
      }
    },
    registrationID: '123'
  },
  {
    registration: {
      registrationID: '',
      registrationFlags: 'Enabled',
      paymentInfo: {
        paymentMedium: PAYMENT_CARD_TYPE.BANK_INFO,
        paymentInfoCC: '123',
        paymentInfoEFT: 'testEft'
      }
    },
    financialPaymentInfoResponse: {
      test: '123'
    },
    registrationID: '1234'
  }
];

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('test function mapRawDataToView', () => {
  it('should be return data {}', () => {
    const bankAccountMapping = {};
    const cardInfoModalMapping = {};
    const data = mapRawDataToView(
      cxRegistrations,
      bankAccountMapping,
      cardInfoModalMapping
    );
    expect(data).toEqual([{}, {}]);
  });

  it('should be return data with have cardInfoModalMapping', () => {
    const bankAccountMapping = { value: '123' };
    const cardInfoModalMapping = { value: '123' };
    const data = mapRawDataToView(
      cxRegistrations,
      bankAccountMapping,
      cardInfoModalMapping
    );
    expect(data).toEqual([{ value: undefined }, { value: undefined }]);
  });
});

describe('redux-store > getBankAccounts', () => {
  it('async thunk', async () => {
    spy = jest
      .spyOn(schedulePaymentService, 'getBankAccounts')
      .mockResolvedValue({
        ...responseDefault,
        data: { cxRegistrations: cxRegistrations }
      });

    const store = createStore(rootReducer, {
      mapping: { data: { bankAccount: {} } },
      schedulePayment: { [storeId]: {} }
    });
    const response = await getBankAccounts(requestData)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.payload).toEqual({ data: [{}, {}] });
  });

  it('async thunk empty storeId', async () => {
    spy = jest
      .spyOn(schedulePaymentService, 'getBankAccounts')
      .mockResolvedValue({ ...responseDefault, data: { cxRegistrations: [] } });

    const store = createStore(rootReducer, { mapping: { data: {} } });

    const response = await getBankAccounts(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ data: [] });
  });

  it('reject', () => {
    const action = getBankAccounts.rejected(
      null,
      'getBankAccounts',
      requestData
    );

    const nextState = reducer({ [storeId]: {} }, action);

    expect(nextState[storeId].isBankAcctsLoading).toEqual(false);
  });

  it('fulfilled', () => {
    const action = getBankAccounts.fulfilled(
      { } as any,
      'getBankAccounts',
      requestData
    );

    const nextState = reducer({ [storeId]: {} }, action);

    expect(nextState[storeId].isBankAcctsLoading).toEqual(false);
    expect(nextState[storeId].bankAccts).toEqual([]);
  });
});
