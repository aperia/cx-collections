import './reducers';
import { getDetailPaymentInfo } from './getDetailPaymentInfo';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { PaymentInfoArgs } from '../types';
import { reducer } from '../_redux/reducers';
import { responseDefault, storeId } from 'app/test-utils';

import schedulePaymentService from '../schedulePaymentService';

const requestData: PaymentInfoArgs = {
  storeId,
  postData: { accountId: storeId }
};

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('redux-store > getDetailPaymentInfo', () => {
  it('success with paymentDueDateView = undefined', async () => {
    const dataResponse = {
      customerInquiry: [
        {
          lastStatementBalanceAmount: '0',
          currentBalanceAmount: '0',
          cycleToDateUnpaidBilledPaymentDueAmount: '0',
          cycleToDateNonDelinquentMinimumPaymentDueAmount: '0',
          paymentDueDate: undefined
        }
      ]
    };
    // mock
    spy = jest
      .spyOn(schedulePaymentService, 'getDetailPaymentInfo')
      .mockReturnValue({ ...responseDefault, data: dataResponse });

    const store = createStore(rootReducer, {
      schedulePayment: { [storeId]: {} },
      paymentSummary: {
        [storeId]: {
          data: {
            minimumPaymentAmount: '0000000000000218.96',
            pastDueAmount: '0000000000000109.48'
          }
        }
      }
    });

    const response = await getDetailPaymentInfo(requestData)(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig['extra']
    );

    expect(response.payload).toBeTruthy();
  });

  it('success with data = undefined', async () => {
    const dataResponse = {
      customerInquiry: [
        {
          lastStatementBalanceAmount: undefined,
          currentBalanceAmount: undefined,
          cycleToDateUnpaidBilledPaymentDueAmount: undefined,
          cycleToDateNonDelinquentMinimumPaymentDueAmount: undefined,
          paymentDueDate: undefined
        }
      ]
    };
    // mock
    spy = jest
      .spyOn(schedulePaymentService, 'getDetailPaymentInfo')
      .mockReturnValue({ ...responseDefault, data: dataResponse });

    const store = createStore(rootReducer, {
      schedulePayment: { [storeId]: {} },
      paymentSummary: {
        [storeId]: {
          data: {
            minimumPaymentAmount: undefined,
            pastDueAmount: undefined
          }
        }
      }
    });

    const response = await getDetailPaymentInfo(requestData)(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig['extra']
    );

    expect(response.payload).toBeTruthy();
  });

  it('success', async () => {
    const dataResponse = {
      customerInquiry: [
        {
          lastStatementBalanceAmount: '0',
          currentBalanceAmount: '0',
          cycleToDateUnpaidBilledPaymentDueAmount: '0',
          cycleToDateNonDelinquentMinimumPaymentDueAmount: '0',
          paymentDueDate: '09-2020'
        }
      ]
    };
    // mock
    spy = jest
      .spyOn(schedulePaymentService, 'getDetailPaymentInfo')
      .mockReturnValue({ ...responseDefault, data: dataResponse });

    const store = createStore(rootReducer, {
      schedulePayment: { [storeId]: {} },
      paymentSummary: {
        [storeId]: {
          data: {
            minimumPaymentAmount: '0000000000000218.96',
            pastDueAmount: '0000000000000109.48'
          }
        }
      }
    });

    const response = await getDetailPaymentInfo(requestData)(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig['extra']
    );

    expect(response.payload).toBeTruthy();
  });

  it('success with empty data', async () => {
    const dataResponse = { customerInquiry: {} };
    // mock
    jest
      .spyOn(schedulePaymentService, 'getDetailPaymentInfo')
      .mockReturnValue({ ...responseDefault, data: dataResponse });

    const store = createStore(rootReducer, {
      schedulePayment: { [storeId]: {} },
      paymentSummary: {
        [storeId]: {
          data: {
            minimumPaymentAmount: '0000000000000218.96',
            pastDueAmount: '0000000000000109.48'
          }
        }
      }
    });

    const response = await getDetailPaymentInfo(requestData)(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig['extra']
    );
    expect(response.payload).toBeFalsy();
  });

  it('success with data array no data', async () => {
    const dataResponse = { customerInquiry: [] };
    // mock
    jest
      .spyOn(schedulePaymentService, 'getDetailPaymentInfo')
      .mockReturnValue({ ...responseDefault, data: dataResponse });

    const store = createStore(rootReducer, {
      schedulePayment: { [storeId]: {} },
      paymentSummary: {
        [storeId]: {
          data: {
            minimumPaymentAmount: '0000000000000218.96',
            pastDueAmount: '0000000000000109.48'
          }
        }
      }
    });

    const response = await getDetailPaymentInfo(requestData)(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig['extra']
    );
    expect(response.payload).toBeFalsy();
  });

  it('reject', () => {
    const action = getDetailPaymentInfo.rejected(
      null,
      'getDetailPaymentInfo',
      requestData
    );
    const nextState = reducer({ [storeId]: {} }, action);
    expect(nextState[storeId].isLoadingPaymentInfo).toEqual(false);
  });
});
