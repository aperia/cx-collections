import { createAsyncThunk } from '@reduxjs/toolkit';
import { FormatTime } from 'app/constants/enums';
import { formatTimeDefault } from 'app/helpers';
import { MAKE_ADJUSTMENT_TRANSACTION } from '../constants';
import schedulePaymentService from '../schedulePaymentService';
import {
  IMakeAdjustmentTransactionArgs,
  IMakeAdjustmentTransactionRequestBody
} from '../types';

export const makeAdjustmentTransaction = createAsyncThunk<
  unknown,
  IMakeAdjustmentTransactionArgs,
  ThunkAPIConfig
>(MAKE_ADJUSTMENT_TRANSACTION, async args => {
  const requestBody: IMakeAdjustmentTransactionRequestBody = {
    common: {
      accountId: args.accountId
    },
    batchType: '05',
    batchPrefix: 'PZ',
    transactionCode: '253',
    transactionAmount: args.feeAmount,
    transactionPostingDate: formatTimeDefault(
      new Date(),
      FormatTime.DefaultShortPostDate
    )!
  };

  await schedulePaymentService.makeAdjustmentPayment({
    ...requestBody
  });
});
