import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import {
  CustomerInquiry,
  MakePaymentResult,
  PaymentAmount,
  PaymentAmountView,
  PaymentInfoArgs,
  PaymentInfoPayload
} from '../types';

import { formatCommon, convertAPIDateToView } from 'app/helpers';

// Helper

import {
  OTHER_AMOUNT,
  getAmountID,
  ID_OTHER_AMOUNT,
  OTHER_DATE
} from '../../constants';

import schedulePaymentService from '../schedulePaymentService';
import { StatementOverviewData } from 'pages/StatementsAndTransactions/StatementTransaction/types';

export const DETAIL_PAYMENT_INFO_FIELD = [
  'customerInquiry.lastStatementBalanceAmount',
  'customerInquiry.currentBalanceAmount',
  'customerInquiry.cycleToDateUnpaidBilledPaymentDueAmount',
  'customerInquiry.cycleToDateNonDelinquentMinimumPaymentDueAmount',
  'customerInquiry.nextPaymentDueDate'
];

export const getDetailPaymentInfo = createAsyncThunk<
  PaymentInfoPayload,
  PaymentInfoArgs,
  ThunkAPIConfig
>('schedulePayment/getDetailPaymentInfo', async (args, thunkAPI) => {
  const { storeId } = args;
  const { accountId, memberSequenceIdentifier } = args.postData;

  const { pastDueAmount, minimumPaymentAmount } = thunkAPI.getState()
    ?.paymentSummary[storeId]?.data as StatementOverviewData;

  const { data } = await schedulePaymentService.getDetailPaymentInfo({
    common: { accountId },
    selectFields: DETAIL_PAYMENT_INFO_FIELD,
    inquiryFields: { memberSequenceIdentifier }
  });

  const {
    lastStatementBalanceAmount,
    currentBalanceAmount,
    nextPaymentDueDate
  } = data.customerInquiry[0];

  return {
    data: {
      customerInquiry: [
        {
          lastStatementBalanceAmount,
          currentBalanceAmount,
          nextPaymentDueDate,
          cycleToDateUnpaidBilledPaymentDueAmount: pastDueAmount,
          cycleToDateNonDelinquentMinimumPaymentDueAmount: minimumPaymentAmount
        }
      ]
    }
  };
});

const getDefaultAmount = (amounts: PaymentAmountView[]) => {
  return amounts.find(amount => !amount.disabled);
};

const mapAmountsToView = (amounts: PaymentAmount[]) => {
  const result: PaymentAmountView[] = amounts.map(({ label, value }) => {
    const disabled = parseFloat(value + '') <= 0;

    return {
      id: getAmountID({ label, value }),
      value,
      disabled,
      title: label,
      label
    };
  });
  result.push({
    ...OTHER_AMOUNT,
    id: ID_OTHER_AMOUNT,
    disabled: false
  });

  return result;
};

const mapDatesToView = (paymentDueDate: string) => {
  const formatPaymentDueDate = convertAPIDateToView(paymentDueDate);

  const currentDate = new Date();

  const result = [
    {
      label: 'txt_currentDate',
      value: formatCommon(currentDate).time.date,
      id: `date-${1}`
    },
    {
      label: 'txt_paymentDueDate',
      value: formatPaymentDueDate,
      id: `date-${2}`
    },
    OTHER_DATE
  ];
  return result;
};

const mapRawDataToView = (rawData: CustomerInquiry[]) => {
  const {
    lastStatementBalanceAmount = '',
    currentBalanceAmount = '',
    cycleToDateUnpaidBilledPaymentDueAmount = '',
    cycleToDateNonDelinquentMinimumPaymentDueAmount = '',
    nextPaymentDueDate = ''
  } = rawData[0];

  const lastStatementBalanceAmountView = parseFloat(
    lastStatementBalanceAmount.trim()
  );

  const currentBalanceAmountView = parseFloat(currentBalanceAmount.trim());
  const cycleToDateUnpaidBilledPaymentDueAmountView = parseFloat(
    cycleToDateUnpaidBilledPaymentDueAmount.trim()
  );
  const cycleToDateNonDelinquentMinimumPaymentDueAmountView = parseFloat(
    cycleToDateNonDelinquentMinimumPaymentDueAmount.trim()
  );
  const paymentDueDateView = nextPaymentDueDate.trim();

  const amountRaw = [
    {
      value:
        cycleToDateUnpaidBilledPaymentDueAmountView +
        cycleToDateNonDelinquentMinimumPaymentDueAmountView,
      label: 'txt_totalDueAmount'
    },
    {
      value: lastStatementBalanceAmountView,
      label: 'txt_lastStatementBalanceAmount'
    },
    {
      value: currentBalanceAmountView,
      label: 'txt_currentBalanceAmount'
    },
    {
      value: cycleToDateUnpaidBilledPaymentDueAmount,
      label: 'txt_cycleToDateUnpaidBilledPaymentDueAmount'
    },
    {
      value: cycleToDateNonDelinquentMinimumPaymentDueAmount,
      label: 'txt_cycleToDateNonDelinquentMinimumPaymentDueAmount'
    }
  ];

  return {
    currentBalanceAmount: parseFloat(currentBalanceAmount || ''),
    amounts: mapAmountsToView(amountRaw),
    paymentDueDate: paymentDueDateView,
    dates: mapDatesToView(paymentDueDateView)
  };
};

export const getDetailPaymentInfoBuilder = (
  builder: ActionReducerMapBuilder<MakePaymentResult>
) => {
  builder
    .addCase(getDetailPaymentInfo.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingPaymentInfo: true
      };
    })
    .addCase(getDetailPaymentInfo.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const {
        data: { customerInquiry: rawData }
      } = action.payload;

      const { amounts, dates, paymentDueDate, currentBalanceAmount } =
        mapRawDataToView(rawData);
      const [defaultSelectedDate] = dates;
      const defaultAmount = getDefaultAmount(amounts);

      draftState[storeId] = {
        ...draftState[storeId],
        selectedAmount: defaultAmount,
        isLoadingPaymentInfo: false,
        selectedDate: defaultSelectedDate,
        paymentInfo: {
          dates,
          paymentDueDate,
          balanceAmount: currentBalanceAmount,
          amountInfo: amounts
        }
      };
    })
    .addCase(getDetailPaymentInfo.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingPaymentInfo: false
      };
    });
};
