import { createSelector } from '@reduxjs/toolkit';
import { MAKE_PAYMENT_FROM, PAYMENT_TYPE } from '../types';
import { SubmitMakePaymentData } from 'pages/MakePayment/SchedulePayment/types';
import { defaultAddPaymentState } from './reducers';
import { getBankAccountByCheckedList } from '../helpers';
import { METHOD } from 'pages/MakePayment/ACHPayment/constants';

// Get data checkedList
export const getAllCheckedList = (state: RootState, storeId: string) =>
  state?.schedulePayment[storeId]?.allCheckedList || [];

export const allCheckedListData = createSelector(
  [getAllCheckedList],
  data => data
);

// Get data checkedList
export const getCvv2 = (state: RootState, storeId: string) =>
  state?.schedulePayment[storeId]?.cvv2 || '';

export const selectCvv2 = createSelector([getCvv2], (data: string) => data);

// Get data checkedList
export const getCheckedList = (state: RootState, storeId: string) =>
  state?.schedulePayment[storeId]?.checkedList || [];

export const checkedListData = createSelector([getCheckedList], data => data);

// Get data post
export const getDataPost = createSelector(
  (states: RootState, storeId: string) => {
    const schedule = states.schedulePayment[storeId] || {};
    const {
      bankAccts = [],
      checkedList = [],
      selectedAmount = {},
      selectedDate = {},
      method
    } = schedule;
    const [eValue] = checkedList;
    const bankAccount = getBankAccountByCheckedList(bankAccts, checkedList);
    const accountNumberMaskedValue = bankAccount?.accNbr?.maskedValue || '';
    const paymentMedium = bankAccount?.accType?.code;

    return {
      registrationID: bankAccount?.registrationID,
      crdNbrEValue: eValue,
      accountNumberMaskedValue,
      paymentMedium,
      amount: selectedAmount,
      schedulePaymentDate: selectedDate.value,
      method: method || METHOD.bank
    };
  },
  data => data as SubmitMakePaymentData
);

// Get data bank accts
export const getBankAccData = (state: RootState, storeId: string) =>
  state.schedulePayment[storeId]?.bankAccts || [];

export const takeBankAccData = createSelector([getBankAccData], data => data);

// Get selected type
export const getSelectedType = (state: RootState, storeId: string) =>
  state.schedulePayment[storeId]?.selectedType || PAYMENT_TYPE.SCHEDULE;

export const takeSelectedType = createSelector([getSelectedType], data => data);

// Get selected acc
export const getSelectedAcc = (state: RootState, storeId: string) =>
  state.schedulePayment[storeId]?.selectedAcc;

export const takeSelectedAcc = createSelector([getSelectedAcc], data => data);

// Get make payment modal status
export const getModalStatus = (state: RootState, storeId: string) =>
  state.schedulePayment[storeId]?.openModal;

export const takeModalStatus = createSelector([getModalStatus], data => data);

// Get payment loading status
export const getLoadingBankAcctsStatus = (state: RootState, storeId: string) =>
  state.schedulePayment[storeId]?.isBankAcctsLoading;

export const getLoadingDeleteRegisteredAccount = (
  states: RootState,
  storeId: string
) => states.schedulePayment[storeId]?.deleteRegisteredAccount?.loading;

export const takeLoadingBankAcctsStatus = createSelector(
  [getLoadingBankAcctsStatus],
  data => data
);

// Get payment account info
export const getPaymentAccInfo = (state: RootState, storeId: string) =>
  state.schedulePayment[storeId]?.bankAcctInfo;

export const takePaymentAccInfo = createSelector(
  [getPaymentAccInfo],
  data => data
);

// Get loading payment account info status
export const getLoadingBankAcctInfoStatus = (
  state: RootState,
  storeId: string
) => state.schedulePayment[storeId]?.isBankAcctInfoLoading;

export const takeLoadingBankAcctInfoStatus = createSelector(
  [getLoadingBankAcctInfoStatus],
  data => data
);

// Get payment amounts
export const getAmounts = (state: RootState, storeId: string) =>
  state.schedulePayment[storeId]?.paymentInfo?.amountInfo || [];

export const dataAmounts = createSelector([getAmounts], data => data);

export const selectBalanceAmount = (states: RootState, storeId: string) =>
  states.schedulePayment[storeId]?.paymentInfo?.balanceAmount;

// Get payment info
export const getPaymentInfo = (state: RootState, storeId: string) =>
  state.schedulePayment[storeId]?.paymentInfo || {};

export const takePaymentInfo = createSelector([getPaymentInfo], data => data);

// Get selected date
export const getDate = (state: RootState, storeId: string) =>
  state.schedulePayment[storeId]?.selectedDate || {};

export const selectDate = createSelector([getDate], data => data);

// Get dates
export const getDates = (state: RootState, storeId: string) =>
  state.schedulePayment[storeId]?.paymentInfo?.dates || [];

export const dataDates = createSelector([getDates], data => data);

// Get payment info loading status
export const getPaymentLoadingStatus = (state: RootState, storeId: string) =>
  state.schedulePayment[storeId]?.isLoadingPaymentInfo;

export const takePaymentLoadingStatus = createSelector(
  [getPaymentLoadingStatus],
  data => data
);

export const getIsLoadingSubmit = (state: RootState, storeId: string) =>
  state.schedulePayment[storeId]?.submitMakePayment?.loading || false;

// Loading submit make payment
export const getIsLoadingMakePayment = createSelector(
  [getIsLoadingSubmit],
  data => data
);

export const selectLoadingCommonDataToAdd = (
  states: RootState,
  storeId: string
) => states.schedulePayment[storeId]?.isLoadingCommonDataToAdd;

export const selectLoadingCheckStatus = (states: RootState, storeId: string) =>
  states.schedulePayment[storeId]?.isLoadingCheckStatus;

export const selectCanDelete = (states: RootState, storeId: string) =>
  states.schedulePayment[storeId]?.canDelete;

// Loading payment info and bank account status
export const takeLoadingMakePayment = createSelector(
  (states: RootState, storeId: string) =>
    states.schedulePayment[storeId]?.isBankAcctsLoading ||
    states.schedulePayment[storeId]?.isLoadingPaymentInfo ||
    states.schedulePayment[storeId]?.isBankAcctInfoLoading ||
    states.schedulePayment[storeId]?.isLoadingCommonDataToAdd,
  data => data
);

// Payment amount
export const getSelectAmount = (state: RootState, storeId: string) =>
  state.schedulePayment[storeId]?.selectedAmount || {};

export const selectAmount = createSelector([getSelectAmount], data => data);

// Payment date
export const getPaymentDate = (state: RootState, storeId: string) =>
  state.schedulePayment[storeId]?.selectedDate;

export const takePaymentDate = createSelector([getPaymentDate], data => data);

export const selectLoading = createSelector(
  (states: RootState, storeId: string) =>
    states.schedulePayment[storeId]?.isLoadingCommonDataToAdd ||
    states.schedulePayment[storeId]?.addAccount?.loading ||
    false,
  (loading: boolean) => loading || defaultAddPaymentState.loading
);

export const selectErrorMessage = createSelector(
  (states: RootState, storeId: string) =>
    states.schedulePayment[storeId]?.addAccount?.errorMessage || '',
  (errorMessage: string) => errorMessage || defaultAddPaymentState.errorMessage
);

export const selectErrorFields = createSelector(
  (states: RootState, storeId: string) =>
    states.schedulePayment[storeId]?.addAccount?.errorFields || [],
  (errorFields: Array<string>) => errorFields
);

export const selectBankInfo = createSelector(
  (states: RootState, storeId: string) =>
    states.schedulePayment[storeId].addAccount?.bankInfo,
  bankInfo => bankInfo
);

export const selectStep = createSelector(
  (states: RootState, storeId: string) =>
    states.schedulePayment[storeId]?.addAccount?.step,
  step => step || (defaultAddPaymentState.step as number)
);

export const selectIsDisableDate = createSelector(
  (states: RootState, storeId: string) =>
    states.schedulePayment[storeId]?.isDisableDate,
  isDisableDate => isDisableDate
);

export const selectForm = (viewName: string) =>
  createSelector(
    (states: RootState, storeId: string) =>
      states.form[`${storeId}-${viewName}`],
    form => form
  );

export const getAcceptedBalanceAmount = (state: RootState, storeId: string) =>
  state.schedulePayment[storeId]?.acceptedBalanceAmount;

export const getMakePaymentFrom = (
  state: RootState,
  storeId: string
): MAKE_PAYMENT_FROM | undefined => state.schedulePayment[storeId]?.from;

export const selectIsMakePaymentFromSettlement = createSelector(
  getMakePaymentFrom,
  (data?: MAKE_PAYMENT_FROM) => data === MAKE_PAYMENT_FROM.SETTLEMENT
);