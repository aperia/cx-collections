import * as makeSelector from './selectors';
import { useSelectorMock } from 'app/test-utils/useSelectorMock';
import { selectorWrapper, storeId } from 'app/test-utils';
import { PAYMENT_CARD_TYPE } from 'pages/MakePayment/SchedulePayment/types';
import { PAYMENT_TYPE } from 'pages/MakePayment/constants';
import { bankAccountInformationCheckingView } from '../constants';

const storeEmptyData: Partial<RootState> = {
  form: {},
  schedulePayment: {
    [storeId]: {}
  }
};

const store: Partial<RootState> = {
  form: {
    [`${storeId}-${bankAccountInformationCheckingView}`]: {
      registeredFields: []
    }
  },
  schedulePayment: {
    [storeId]: {
      acceptedBalanceAmount: '100',
      bankAcctInfo: {},
      paymentInfo: {
        amountInfo: [],
        paymentDueDate: new Date().toISOString(),
        dates: [],
        balanceAmount: 0
      },
      isDisableDate: true,
      isBankAcctInfoLoading: false,
      isLoadingPaymentInfo: false,
      isBankAcctsLoading: false,
      isLoadingCheckStatus: false,
      canDelete: false,
      deleteRegisteredAccount: {
        loading: false
      },
      openModal: false,
      addAccount: {
        loading: true,
        bankInfo: {
          bankRoutingNumber: ''
        },
        errorFields: ['amount'],
        errorMessage: 'error message',
        step: 1
      },
      selectedDate: {
        id: '1',
        label: 'label',
        value: new Date()
      },
      selectedAmount: {
        id: '1',
        title: 'title',
        label: 'label',
        value: '1'
      },
      selectedAcc: {
        accNbr: { eValue: '1', maskedValue: '1' },
        accType: {
          code: PAYMENT_CARD_TYPE.CHECKING,
          description: '1'
        }
      },
      selectedType: PAYMENT_TYPE.DEMAND_ACH,
      bankAccts: [
        {
          id: 'ky-1',
          accNbr: { eValue: 'ky-1', maskedValue: '1' },
          accType: {
            code: PAYMENT_CARD_TYPE.CHECKING,
            description: '1'
          }
        }
      ],
      checkedList: ['ky-1'],
      allCheckedList: ['ky-1'],
      submitMakePayment: { loading: false },
      cvv2: '000'
    }
  }
};

describe('redux-store > make-payment > selector', () => {
  describe('allCheckedListData', () => {
    it('it should be getLoadingDeleteRegisteredAccount correctly', () => {
      const data = useSelectorMock(
        makeSelector.getLoadingDeleteRegisteredAccount,
        store,
        storeId
      );
      expect(data).toEqual(false);
    });

    it('it should be selectBalanceAmount correctly', () => {
      const data = useSelectorMock(
        makeSelector.selectBalanceAmount,
        store,
        storeId
      );
      expect(data).toEqual(0);
    });

    it('it should be get selectLoadingCheckStatus correctly', () => {
      const data = useSelectorMock(
        makeSelector.selectLoadingCheckStatus,
        store,
        storeId
      );
      expect(data).toEqual(false);
    });

    it('it should be canDelete correctly', () => {
      const data = useSelectorMock(
        makeSelector.selectCanDelete,
        store,
        storeId
      );
      expect(data).toEqual(false);
    });

    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.allCheckedListData,
        store,
        storeId
      );
      const { allCheckedList } = store.schedulePayment![storeId];

      expect(data).toEqual(allCheckedList);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.allCheckedListData,
        storeEmptyData,
        storeId
      );

      expect(data).toEqual([]);
    });
  });

  describe('checkedListData', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.checkedListData,
        store,
        storeId
      );
      const { checkedList } = store.schedulePayment![storeId];

      expect(data).toEqual(checkedList);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.checkedListData,
        storeEmptyData,
        storeId
      );

      expect(data).toEqual([]);
    });
  });

  describe('getDataPost', () => {
    it('with data', () => {
      const data = useSelectorMock(makeSelector.getDataPost, store, storeId);
      const schedule = store.schedulePayment![storeId];

      const {
        bankAccts = [],
        checkedList = [],
        selectedAmount = {},
        selectedDate = {}
      } = schedule;

      expect(data).toEqual({
        accountNumberMaskedValue: bankAccts[0].accNbr?.maskedValue,
        amount: selectedAmount,
        crdNbrEValue: checkedList[0],
        paymentMedium: 'C',
        registrationID: undefined,
        schedulePaymentDate: selectedDate.value,
        method: 'Bank'
      });
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.getDataPost,
        { schedulePayment: {} },
        storeId
      );

      expect(data).toEqual({
        crdNbrEValue: undefined,
        accountNumberMaskedValue: '',
        amount: {},
        schedulePaymentDate: undefined,
        method: 'Bank',
        paymentMedium: undefined,
        registrationID: undefined
      });
    });
  });

  describe('takeBankAccData', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.takeBankAccData,
        store,
        storeId
      );

      const { bankAccts } = store.schedulePayment![storeId];

      expect(data).toEqual(bankAccts);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.takeBankAccData,
        storeEmptyData,
        storeId
      );

      expect(data).toEqual([]);
    });
  });

  describe('takeSelectedType', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.takeSelectedType,
        store,
        storeId
      );

      const { selectedType } = store.schedulePayment![storeId];

      expect(data).toEqual(selectedType);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.takeSelectedType,
        storeEmptyData,
        storeId
      );

      expect(data).toEqual(PAYMENT_TYPE.SCHEDULE);
    });
  });

  describe('takeSelectedAcc', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.takeSelectedAcc,
        store,
        storeId
      );

      const { selectedAcc } = store.schedulePayment![storeId];

      expect(data).toEqual(selectedAcc);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.takeSelectedAcc,
        storeEmptyData,
        storeId
      );
      expect(data).toBeUndefined();
    });
  });

  describe('takeModalStatus', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.takeModalStatus,
        store,
        storeId
      );

      const { openModal } = store.schedulePayment![storeId];

      expect(data).toEqual(openModal);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.takeModalStatus,
        storeEmptyData,
        storeId
      );
      expect(data).toBeUndefined();
    });
  });

  describe('takeLoadingBankAcctsStatus', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.takeLoadingBankAcctsStatus,
        store,
        storeId
      );

      const { isBankAcctsLoading } = store.schedulePayment![storeId];

      expect(data).toEqual(isBankAcctsLoading);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.takeLoadingBankAcctsStatus,
        storeEmptyData,
        storeId
      );
      expect(data).toBeUndefined();
    });
  });

  describe('takePaymentAccInfo', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.takePaymentAccInfo,
        store,
        storeId
      );

      const { bankAcctInfo } = store.schedulePayment![storeId];

      expect(data).toEqual(bankAcctInfo);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.takePaymentAccInfo,
        storeEmptyData,
        storeId
      );
      expect(data).toBeUndefined();
    });
  });

  describe('takeLoadingBankAcctInfoStatus', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.takeLoadingBankAcctInfoStatus,
        store,
        storeId
      );

      const { isBankAcctInfoLoading } = store.schedulePayment![storeId];

      expect(data).toEqual(isBankAcctInfoLoading);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.takeLoadingBankAcctInfoStatus,
        storeEmptyData,
        storeId
      );
      expect(data).toBeUndefined();
    });
  });

  describe('getAmounts', () => {
    it('with data', () => {
      const data = useSelectorMock(makeSelector.getAmounts, store, storeId);
      const { paymentInfo } = store.schedulePayment![storeId];

      expect(data).toEqual(paymentInfo?.amountInfo);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.getAmounts,
        storeEmptyData,
        storeId
      );
      expect(data).toEqual([]);
    });
  });

  describe('dataAmounts', () => {
    it('with data', () => {
      const data = useSelectorMock(makeSelector.dataAmounts, store, storeId);
      const { paymentInfo } = store.schedulePayment![storeId];

      expect(data).toEqual(paymentInfo?.amountInfo);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.dataAmounts,
        storeEmptyData,
        storeId
      );

      expect(data).toEqual([]);
    });
  });

  describe('getDate', () => {
    it('with data', () => {
      const data = useSelectorMock(makeSelector.getDate, store, storeId);
      const { selectedDate } = store.schedulePayment![storeId];

      expect(data).toEqual(selectedDate);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.getDate,
        storeEmptyData,
        storeId
      );

      expect(data).toEqual({});
    });
  });

  describe('selectDate', () => {
    it('with data', () => {
      const data = useSelectorMock(makeSelector.selectDate, store, storeId);
      const { selectedDate } = store.schedulePayment![storeId];

      expect(data).toEqual(selectedDate);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.selectDate,
        storeEmptyData,
        storeId
      );

      expect(data).toEqual({});
    });
  });

  describe('getDates', () => {
    it('with data', () => {
      const data = useSelectorMock(makeSelector.getDates, store, storeId);
      const { dates } = store.schedulePayment![storeId].paymentInfo || {};

      expect(data).toEqual(dates);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.getDates,
        storeEmptyData,
        storeId
      );

      expect(data).toEqual([]);
    });
  });

  describe('dataDates', () => {
    it('with data', () => {
      const data = useSelectorMock(makeSelector.dataDates, store, storeId);
      const { dates } = store.schedulePayment![storeId].paymentInfo || {};

      expect(data).toEqual(dates);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.dataDates,
        storeEmptyData,
        storeId
      );

      expect(data).toEqual([]);
    });
  });

  describe('takePaymentInfo', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.takePaymentInfo,
        store,
        storeId
      );
      const { paymentInfo } = store.schedulePayment![storeId];

      expect(data).toEqual(paymentInfo);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.takePaymentInfo,
        storeEmptyData,
        storeId
      );

      expect(data).toEqual({});
    });
  });

  describe('takePaymentLoadingStatus', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.takePaymentLoadingStatus,
        store,
        storeId
      );

      const { isLoadingPaymentInfo } = store.schedulePayment![storeId];

      expect(data).toEqual(isLoadingPaymentInfo);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.takePaymentLoadingStatus,
        storeEmptyData,
        storeId
      );
      expect(data).toBeUndefined();
    });
  });

  describe('getIsLoadingSubmit', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.getIsLoadingSubmit,
        store,
        storeId
      );

      const { loading } =
        store.schedulePayment![storeId].submitMakePayment || {};

      expect(data).toEqual(loading);
    });
    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.getIsLoadingSubmit,
        storeEmptyData,
        storeId
      );

      expect(data).toBeFalsy();
    });
  });

  describe('getIsLoadingMakePayment', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.getIsLoadingMakePayment,
        store,
        storeId
      );

      const { loading } =
        store.schedulePayment![storeId].submitMakePayment || {};

      expect(data).toEqual(loading);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.getIsLoadingMakePayment,
        storeEmptyData,
        storeId
      );

      expect(data).toBeFalsy();
    });
  });

  describe('takePaymentDate', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.takePaymentDate,
        store,
        storeId
      );

      const { selectedDate } = store.schedulePayment![storeId];

      expect(data).toEqual(selectedDate);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.takePaymentDate,
        storeEmptyData,
        storeId
      );
      expect(data).toBeUndefined();
    });
  });

  describe('takeLoadingMakePayment', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.takeLoadingMakePayment,
        store,
        storeId
      );

      const {
        isBankAcctsLoading,
        isLoadingPaymentInfo,
        isBankAcctInfoLoading,
        isLoadingCommonDataToAdd
      } = store.schedulePayment![storeId];

      expect(data).toEqual(
        isBankAcctsLoading ||
          isBankAcctInfoLoading ||
          isLoadingPaymentInfo ||
          isLoadingCommonDataToAdd
      );

      // Just for coverage
      const dataIsLoadingCommonDataToAdd = useSelectorMock(
        makeSelector.selectLoadingCommonDataToAdd,
        store,
        storeId
      );

      expect(dataIsLoadingCommonDataToAdd).toEqual(undefined);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.takeLoadingMakePayment,
        storeEmptyData,
        storeId
      );
      expect(data).toBeUndefined();
    });
  });

  describe('getSelectAmount', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.getSelectAmount,
        store,
        storeId
      );

      const { selectedAmount } = store.schedulePayment![storeId];

      expect(data).toEqual(selectedAmount);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.getSelectAmount,
        storeEmptyData,
        storeId
      );

      expect(data).toEqual({});
    });
  });

  describe('selectAmount', () => {
    it('with data', () => {
      const data = useSelectorMock(makeSelector.selectAmount, store, storeId);

      const { selectedAmount } = store.schedulePayment![storeId];

      expect(data).toEqual(selectedAmount);
    });
    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.selectAmount,
        storeEmptyData,
        storeId
      );

      expect(data).toEqual({});
    });
  });

  describe('selectLoading', () => {
    it('with data', () => {
      const data = useSelectorMock(makeSelector.selectLoading, store, storeId);

      const { loading } = store.schedulePayment![storeId].addAccount!;

      expect(data).toEqual(loading);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.selectLoading,
        storeEmptyData,
        storeId
      );

      expect(data).toBeFalsy();
    });
  });

  describe('selectErrorMessage', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.selectErrorMessage,
        store,
        storeId
      );

      const { errorMessage } = store.schedulePayment![storeId].addAccount!;

      expect(data).toEqual(errorMessage);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.selectErrorMessage,
        storeEmptyData,
        storeId
      );
      expect(data).toEqual('');
    });
  });

  describe('selectErrorFields', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.selectErrorFields,
        store,
        storeId
      );

      const { errorFields } = store.schedulePayment![storeId].addAccount!;

      expect(data).toEqual(errorFields);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.selectErrorFields,
        storeEmptyData,
        storeId
      );
      expect(data).toEqual([]);
    });
  });

  describe('selectBankInfo', () => {
    it('with data', () => {
      const data = useSelectorMock(makeSelector.selectBankInfo, store, storeId);

      const { bankInfo } = store.schedulePayment![storeId].addAccount!;

      expect(data).toEqual(bankInfo);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.selectBankInfo,
        storeEmptyData,
        storeId
      );

      expect(data).toBeUndefined();
    });
  });

  describe('selectStep', () => {
    it('with data', () => {
      const data = useSelectorMock(makeSelector.selectStep, store, storeId);
      expect(data).toEqual(1);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.selectStep,
        storeEmptyData,
        storeId
      );
      expect(data).toEqual(0);
    });
  });

  describe('selectIsDisableDate', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.selectIsDisableDate,
        store,
        storeId
      );
      expect(data).toEqual(true);
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.selectIsDisableDate,
        storeEmptyData,
        storeId
      );
      expect(data).toBeUndefined();
    });
  });

  describe('selectForm', () => {
    it('with data', () => {
      const data = useSelectorMock(
        makeSelector.selectForm(bankAccountInformationCheckingView),
        store,
        storeId
      );
      expect(data).toEqual({ registeredFields: [] });
    });

    it('without data', () => {
      const data = useSelectorMock(
        makeSelector.selectForm(bankAccountInformationCheckingView),
        storeEmptyData,
        storeId
      );
      expect(data).toBeUndefined();
    });
  });

  it('getAcceptedBalanceAmount', () => {
    const { data, emptyData } = selectorWrapper(store)(
      makeSelector.getAcceptedBalanceAmount
    );

    expect(data).toEqual(
      store.schedulePayment?.[storeId].acceptedBalanceAmount
    );
    expect(emptyData).toEqual(undefined);
  });

  it('selectCvv2', () => {
    const { data, emptyData } = selectorWrapper(store)(makeSelector.selectCvv2);

    expect(data).toEqual(store.schedulePayment?.[storeId].cvv2);
    expect(emptyData).toEqual('');
  });
});
