import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { responseDefault, storeId, mockActionCreator } from 'app/test-utils';
import {
  PAYMENT_CARD_TYPE,
  ValidateBankAccountDebitInfoPayload,
  SubmitMakePaymentRequest
} from 'pages/MakePayment/SchedulePayment/types';
import { submitMakePayment, mapDataToPost } from './submitMakePayment';
import { CodeErrorType, FormatTime } from 'app/constants/enums';
import schedulePaymentService from '../schedulePaymentService';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { SCHEDULE_ACTIONS } from 'pages/Memos/constants';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { formatTimeDefault } from 'app/helpers';

const requestData: SubmitMakePaymentRequest = {
  storeId,
  data: {
    schedulePaymentDate: '11/11/2021',
    amount: { value: 100 },
    registrationID: '123',
    method: '',
    cvv2: '000'
  },
  accountId: storeId,
  onSuccess: () => undefined,
  onFailure: () => undefined
};

const data: ValidateBankAccountDebitInfoPayload = {
  bankRoutingNumber: '',
  bankAccountNumber: '',
  bankAccountType: PAYMENT_CARD_TYPE.DEBIT
};

const apiErrorNotificationActionSpy = mockActionCreator(
  apiErrorNotificationAction
);
const memoSpy = mockActionCreator(memoActions);
const toastActionSpy = mockActionCreator(actionsToast);

describe('test func mapDataToPost', () => {
  it('it should be not change schedulePaymentDate', () => {
    const { commonConfig } = window.appConfig || {};
    const dataPost = mapDataToPost(
      requestData.storeId,
      requestData.data,
      commonConfig
    );
    expect(dataPost).toEqual({
      common: {
        accountId: '123456789',
        app: undefined,
        x500id: undefined
      },
      payeeID: '123456789',
      paymentAmount: '100.00',
      paymentDate: '11/11/2021',
      reference: '123456789 11/11/2021',
      registrationID: '123',
      paymentInfo: {
        paymentMedium: 'CheckCard',
        paymentInfoCC: {
          cvv2: '000'
        }
      }
    });
  });

  it('should ', () => {
    const { commonConfig } = window.appConfig || {};
    const withSchedulePaymentDateTypeDate = mapDataToPost(
      requestData.storeId,
      { ...requestData.data, schedulePaymentDate: new Date('July 10 2021') },
      commonConfig
    );

    expect(withSchedulePaymentDateTypeDate.paymentDate).toEqual('07/10/2021');
  });
});

describe('redux-store > submitMakePayment', () => {
  it('success submit card info', async () => {
    const clearApiErrorDataSpy =
      apiErrorNotificationActionSpy('clearApiErrorData');
    const postMemoSpy = memoSpy('postMemo');

    const mockOnSuccess = jest.fn();
    jest
      .spyOn(schedulePaymentService, 'submitScheduleMakePayment')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          ...data,
          confirmationNumber: '123'
        }
      });
    const store = createStore(rootReducer, {
      schedulePayment: {
        [storeId]: {}
      },
      paymentConfirm: {
        [storeId]: {
          responseConvenienceFee: {
            convenienceFee: 10
          }
        }
      }
    });
    const result = await submitMakePayment({
      ...requestData,
      data: {
        ...requestData.data,
        schedulePaymentDate: formatTimeDefault(new Date(), FormatTime.Date)
      },
      onSuccess: mockOnSuccess
    })(store.dispatch, store.getState, undefined as unknown as ThunkAPIConfig);

    expect(mockOnSuccess).toBeCalled();
    expect(result.type).toEqual(submitMakePayment.fulfilled.type);
    expect(clearApiErrorDataSpy).toBeCalledWith({
      forSection: 'inModalBody',
      storeId
    });

    const paramMemoSpy = {
      accEValue: storeId,
      chronicleKey: 'payment',
      makePayment: {
        confirmationNumber: '123',
        feeAmount: '$10.00',
        maskedNumber: '****',
        paymentAmt: '$100.00',
        registrationId: '123',
        scheduledDate: formatTimeDefault(new Date(), FormatTime.Date)
      },
      storeId
    };

    expect(postMemoSpy).toBeCalledWith(
      SCHEDULE_ACTIONS.SUBMIT_CARD,
      paramMemoSpy
    );

    await submitMakePayment({
      ...requestData,
      data: {
        schedulePaymentDate: formatTimeDefault(new Date(), FormatTime.Date),
        amount: { value: 100 },
        registrationID: '123',
        paymentMedium: 'eCheck',
        method: '',
        cvv2: '000'
      },
      onSuccess: mockOnSuccess
    })(store.dispatch, store.getState, {});

    expect(postMemoSpy).toHaveBeenCalled();
  });

  it('success submit bank info', async () => {
    const clearApiErrorDataSpy =
      apiErrorNotificationActionSpy('clearApiErrorData');
    const postMemoSpy = memoSpy('postMemo');

    const mockOnSuccess = jest.fn();
    jest
      .spyOn(schedulePaymentService, 'submitScheduleMakePayment')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          ...data,
          confirmationNumber: '123'
        }
      });
    const store = createStore(rootReducer, {
      schedulePayment: {
        [storeId]: {}
      },
      paymentConfirm: {
        [storeId]: {
          responseConvenienceFee: {
            convenienceFee: 10
          }
        }
      }
    });
    const result = await submitMakePayment({
      storeId,
      data: {
        paymentMedium: 'eCheck',
        schedulePaymentDate: '11/11/2021',
        amount: { value: 100 },
        registrationID: '123',
        method: '',
        cvv2: '000'
      },
      accountId: storeId,
      onFailure: () => undefined,
      onSuccess: mockOnSuccess
    })(store.dispatch, store.getState, undefined as unknown as ThunkAPIConfig);

    expect(mockOnSuccess).toBeCalled();
    expect(result.type).toEqual(submitMakePayment.fulfilled.type);
    expect(clearApiErrorDataSpy).toBeCalledWith({
      forSection: 'inModalBody',
      storeId
    });

    const paramMemoSpy = {
      accEValue: storeId,
      chronicleKey: 'payment',
      makePayment: {
        confirmationNumber: '123',
        feeAmount: '$10.00',
        maskedNumber: '****',
        paymentAmt: '$100.00',
        registrationId: '123',
        scheduledDate: '11/11/2021'
      },
      storeId
    };

    expect(postMemoSpy).toBeCalledWith(
      SCHEDULE_ACTIONS.SUBMIT_BANK,
      paramMemoSpy
    );

    await submitMakePayment({
      ...requestData,
      data: {
        schedulePaymentDate: '11/11/2021',
        amount: { value: 100 },
        registrationID: '123',
        paymentMedium: 'eCheck',
        method: '',
        cvv2: '000'
      },
      onSuccess: mockOnSuccess
    })(store.dispatch, store.getState, {});

    expect(postMemoSpy).toHaveBeenCalled();
  });

  it('submit error when empty confirmationNumber', async () => {
    const mockOnFailure = jest.fn();
    const mockOnSuccess = jest.fn();
    jest
      .spyOn(schedulePaymentService, 'submitScheduleMakePayment')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          ...data,
          confirmationNumber: ''
        }
      });

    const store = createStore(rootReducer, {
      schedulePayment: {
        [storeId]: {}
      },
      paymentConfirm: {
        [storeId]: {
          responseConvenienceFee: {
            convenienceFee: 10
          }
        }
      }
    });

    await submitMakePayment({
      storeId,
      data: {
        paymentMedium: 'eCheck',
        schedulePaymentDate: '11/11/2021',
        amount: { value: 100 },
        registrationID: '123',
        method: '',
        cvv2: '000'
      },
      accountId: storeId,
      onFailure: mockOnFailure,
      onSuccess: mockOnSuccess
    })(store.dispatch, store.getState, undefined as unknown as ThunkAPIConfig);

    expect(mockOnFailure).toBeCalled();
  });

  it('another error', async () => {
    // mock
    const mockOnFailure = jest.fn();
    jest
      .spyOn(schedulePaymentService, 'submitScheduleMakePayment')
      .mockRejectedValue({
        ...responseDefault,
        data: undefined
      });

    // store
    const store = createStore(rootReducer, {
      schedulePayment: {
        [storeId]: {}
      },
      paymentConfirm: {
        [storeId]: {
          responseConvenienceFee: {
            convenienceFee: 123
          }
        }
      }
    });

    // invoke
    await submitMakePayment({
      ...requestData,
      data: {
        ...requestData.data,
        schedulePaymentDate: undefined
      },
      onFailure: mockOnFailure
    })(store.dispatch, store.getState, {});

    // expect
    expect(mockOnFailure).toBeCalled();
  });

  it('error 453', async () => {
    // mock
    const mockOnFailure = jest.fn();
    const addToastSpy = toastActionSpy('addToast');
    jest
      .spyOn(schedulePaymentService, 'submitScheduleMakePayment')
      .mockRejectedValue({
        ...responseDefault,
        data: {
          coreResponse: {
            responseCode: CodeErrorType.ValidationException,
            message: 'Error'
          }
        }
      });

    // store
    const store = createStore(rootReducer, {
      schedulePayment: {
        [storeId]: {}
      },
      paymentConfirm: {
        [storeId]: {
          responseConvenienceFee: {
            convenienceFee: 123
          }
        }
      }
    });

    // invoke
    await submitMakePayment({ ...requestData, onFailure: mockOnFailure })(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig
    );

    // expect
    expect(mockOnFailure).toBeCalled();
    expect(addToastSpy).toBeCalledWith({
      message: 'Error',
      show: true,
      type: 'error'
    });
  });
});
