import { batch } from 'react-redux';
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import dateTime from 'date-and-time';
import isString from 'lodash.isstring';

// types
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import {
  MakePaymentResult,
  PAYMENT_CARD_TYPE,
  SubmitMakePaymentRequest
} from '../types';

// constants
import { CodeErrorType } from 'app/constants/enums';

// Service
import schedulePaymentService from '../schedulePaymentService';

// Helper
import { memoActions } from 'pages/Memos/_redux/reducers';
import { SCHEDULE_ACTIONS } from 'pages/Memos/constants';
import { formatCommon } from 'app/helpers';
import { formatMaskValueToPost } from '../helpers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { CHRONICLE_PAYMENT_KEY } from 'pages/ClientConfiguration/Memos/constants';

export const mapDataToPost = (
  accountId: string,
  args: SubmitMakePaymentRequest['data'],
  commonConfig?: CommonConfig
) => {
  const { schedulePaymentDate, amount, registrationID, paymentMedium, cvv2 } =
    args;
  const paymentDate = isString(schedulePaymentDate)
    ? schedulePaymentDate
    : schedulePaymentDate && dateTime.format(schedulePaymentDate, 'MM/DD/YYYY');

  const isBank = paymentMedium === PAYMENT_CARD_TYPE.BANK_INFO;

  return {
    common: {
      app: commonConfig?.appCxCollections,
      x500id: commonConfig?.x500id,
      accountId
    },
    payeeID: accountId,
    reference: `${accountId} ${schedulePaymentDate}`,
    paymentDate,
    registrationID,
    paymentAmount: parseFloat(amount?.value as string).toFixed(2),
    paymentInfo: {
      paymentMedium: isBank
        ? PAYMENT_CARD_TYPE.BANK_INFO
        : PAYMENT_CARD_TYPE.CARD_INFO,
      ...(!isBank && {
        paymentInfoCC: {
          cvv2
        }
      })
    }
  };
};

export const submitMakePayment = createAsyncThunk<
  void,
  SubmitMakePaymentRequest,
  ThunkAPIConfig
>(
  'schedulePayment/submitMakePayment',
  async (args: SubmitMakePaymentRequest, { dispatch, getState }) => {
    try {
      const { accountId, data, onSuccess, storeId } = args;

      const { commonConfig } = window.appConfig || {};
      const states = getState();
      const fee =
        states.paymentConfirm[storeId].responseConvenienceFee?.convenienceFee;

      const postData = mapDataToPost(accountId, data, commonConfig);

      const response = await schedulePaymentService.submitScheduleMakePayment(
        postData
      );

      const isBank = data.paymentMedium === 'eCheck';
      const memoActionType = isBank
        ? SCHEDULE_ACTIONS.SUBMIT_BANK
        : SCHEDULE_ACTIONS.SUBMIT_CARD;

      const { returnCode, confirmationNumber } = response.data;

      if (returnCode === 'Error' || confirmationNumber === '') throw response;

      batch(() => {
        dispatch(
          apiErrorNotificationAction.clearApiErrorData({
            forSection: 'inModalBody',
            storeId
          })
        );
        dispatch(
          memoActions.postMemo(memoActionType, {
            storeId,
            accEValue: accountId,
            chronicleKey: CHRONICLE_PAYMENT_KEY,
            makePayment: {
              maskedNumber: formatMaskValueToPost(
                data.accountNumberMaskedValue
              ),
              confirmationNumber: response.data.confirmationNumber,
              scheduledDate: formatCommon(`${data.schedulePaymentDate}`).time
                .date,
              paymentAmt: formatCommon(`${data.amount?.value}`).currency(2),
              feeAmount: formatCommon(`${fee}`).currency(2),
              registrationId: data.registrationID
            }
          })
        );
      });

      onSuccess && onSuccess(response);
    } catch (error) {
      const { onFailure } = args;

      const responseCode = error?.data?.coreResponse?.responseCode;

      if (responseCode === CodeErrorType.ValidationException) {
        const message = error?.data?.coreResponse?.message;

        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'error',
            message: message
          })
        );
      }

      dispatch(
        apiErrorNotificationAction.updateApiError({
          forSection: 'inModalBody',
          storeId: args.storeId,
          apiResponse: error
        })
      );
      onFailure && onFailure(error);

      throw error;
    }
  }
);

export const submitMakePaymentBuilder = (
  builder: ActionReducerMapBuilder<MakePaymentResult>
) => {
  builder.addCase(submitMakePayment.pending, (draftState, action) => {
    const { storeId } = action.meta.arg;

    draftState[storeId] = {
      ...draftState[storeId],
      submitMakePayment: {
        ...draftState[storeId]?.addAccount,
        loading: true
      }
    };
  });
  builder.addCase(submitMakePayment.fulfilled, (draftState, action) => {
    const { storeId } = action.meta.arg;

    draftState[storeId] = {
      ...draftState[storeId],
      submitMakePayment: {
        ...draftState[storeId]?.submitMakePayment,
        loading: false,
        error: false
      }
    };
  });
  builder.addCase(submitMakePayment.rejected, (draftState, action) => {
    const { storeId } = action.meta.arg;

    draftState[storeId] = {
      ...draftState[storeId],
      submitMakePayment: {
        ...draftState[storeId]?.submitMakePayment,
        loading: false
      }
    };
  });
};
