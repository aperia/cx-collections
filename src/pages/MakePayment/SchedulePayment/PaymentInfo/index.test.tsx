import React from 'react';
import { screen } from '@testing-library/react';

// hooks
import { AccountDetailProvider } from 'app/hooks';

// test-utils
import { mockActionCreator, renderMockStore, storeId } from 'app/test-utils';

// components
import PaymentInfo from './index';
import { schedulePaymentActions as actions } from '../_redux/reducers';

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId: storeId, accEValue: storeId }}>
      <PaymentInfo />
    </AccountDetailProvider>,
    { initialState }
  );
};

const schedulePaymentSpy = mockActionCreator(actions);

describe('Make Payment > PaymentInfo', () => {
  it('render component has loading', async () => {
    renderWrapper({
      schedulePayment: {
        [storeId]: {
          isBankAcctsLoading: true,
          isLoadingPaymentInfo: true
        }
      }
    });

    expect(screen.queryByText('txt_paymentInformation')).toBeNull();
  });

  it('render component full data', async () => {
    const mockAction = schedulePaymentSpy('getDetailPaymentInfo');

    renderWrapper({
      schedulePayment: {
        [storeId]: {
          paymentInfo: {
            dates: [],
            amountInfo: [
              { id: '0', value: '25', label: 'Minimum Payment Due' },
              { id: '1', value: '0', label: 'Last Statement Balance' },
              { id: '2', value: '0', label: 'Current Balance' },
              { id: '3', value: '0', label: 'Past Due Amount' }
            ],
            paymentDueDate: new Date().toISOString(),
            balanceAmount: 0
          },
          isBankAcctsLoading: false,
          isLoadingPaymentInfo: false,
          isBankAcctInfoLoading: false,
          isLoadingCommonDataToAdd: false
        }
      }
    });

    expect(screen.getByText('txt_paymentInformation')).toBeInTheDocument();
    expect(mockAction).toBeCalledWith({
      storeId,
      postData: {
        accountId: storeId,
        memberSequenceIdentifier: undefined
      }
    });
  });
});
