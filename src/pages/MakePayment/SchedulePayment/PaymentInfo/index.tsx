import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

// Hook
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// Redux
import { schedulePaymentActions as actions } from '../_redux/reducers';
import { takeLoadingMakePayment } from '../_redux/selectors';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Component
import Amount from './Amount';
import Date from './Date';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface PaymentInfoProps {}

const PaymentDetail: React.FC<PaymentInfoProps> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId, accEValue, memberSequenceIdentifier } = useAccountDetail();

  const isLoading = useStoreIdSelector<boolean>(takeLoadingMakePayment);

  const testId =
    'account-details_make-payment-modal_schedule-payment_payment-detail';

  useEffect(() => {
    // Custom field data for Amount Component
    const postData = {
      accountId: accEValue,
      memberSequenceIdentifier
    };
    dispatch(actions.getDetailPaymentInfo({ storeId, postData }));
    dispatch(actions.getAddressInfo({ storeId, accountId: accEValue }));
  }, [accEValue, dispatch, memberSequenceIdentifier, storeId]);

  if (isLoading) return null;

  return (
    <>
      <h5 data-testid={genAmtId(testId, 'title', '')}>
        {t('txt_paymentInformation')}
      </h5>
      <Amount />
      <hr />
      <Date />
    </>
  );
};

export default PaymentDetail;
