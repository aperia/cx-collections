import React, { useEffect, useMemo, useRef } from 'react';

// components
import {
  DatePicker,
  DatePickerChangeEvent,
  Radio,
  Tooltip
} from 'app/_libraries/_dls/components';

// Hook
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useDispatch, useSelector } from 'react-redux';

// Redux
import {
  dataDates,
  selectDate,
  selectIsDisableDate
} from '../_redux/selectors';
import { schedulePaymentActions } from '../_redux/reducers';

// Type
import { PaymentDateView } from 'pages/MakePayment/SchedulePayment/types';
import { ID_OTHER_DATE, OTHER_DATE } from '../../constants';

// utils
import isNull from 'lodash.isnull';
import isUndefined from 'lodash.isundefined';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { clientConfigPayByPhoneActions } from 'pages/ClientConfiguration/PayByPhone/_redux/reducers';
import { selectCurrentConfig } from 'pages/ClientConfiguration/PayByPhone/_redux/selectors';
import { PayByPhoneItem } from 'pages/ClientConfiguration/PayByPhone/types';

const PaymentDetail: React.FC = () => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();

  const minDateRef = useRef<Date | undefined>();
  const dates = useStoreIdSelector<PaymentDateView[]>(dataDates);
  const selectedDate = useStoreIdSelector<PaymentDateView>(selectDate);
  const isDisableDate = useStoreIdSelector<boolean>(selectIsDisableDate);
  const payByPhoneConfig: PayByPhoneItem = useSelector(selectCurrentConfig);
  const otherMaxDay = useMemo(() => new Date(), []);

  const { id: selectedID, value: selectedValue } = selectedDate;
  const testId =
    'account-details_make-payment-modal_schedule-payment_payment-detail_date';

  const handleSelectDate = (date: PaymentDateView) => {
    dispatch(schedulePaymentActions.selectedDate({ storeId, data: date }));
  };

  const handleChangeOtherDate = ({ value }: DatePickerChangeEvent) => {
    handleSelectDate({ ...OTHER_DATE, value });
  };

  const handleBlurOtherDate = () => {
    if (selectedValue !== undefined) return;

    handleSelectDate({ ...OTHER_DATE, value: null });
  };

  const handleDateTooltip = (date: Date) => {
    if (
      currentDateDate?.value &&
      new Date(date) < new Date(currentDateDate?.value)
    ) {
      return <Tooltip element={t('txt_payment_date_must_not_be_past_date')} />;
    }
  };

  useEffect(() => {
    dispatch(clientConfigPayByPhoneActions.getPayByPhoneData({ storeId }));
  }, [dispatch, storeId]);

  useEffect(() => {
    if (payByPhoneConfig.maxDay) {
      otherMaxDay.setDate(otherMaxDay.getDate() + payByPhoneConfig.maxDay);
    }
  }, [otherMaxDay, payByPhoneConfig.maxDay]);

  const showOtherDate = selectedID === ID_OTHER_DATE;

  const [currentDateDate] = dates;
  minDateRef.current =
    isNull(currentDateDate?.value) || isUndefined(currentDateDate?.value)
      ? undefined
      : new Date(currentDateDate?.value);

  return (
    <div data-testid={genAmtId(testId, 'container', '')}>
      <h6 className="color-grey" data-testid={genAmtId(testId, 'title', '')}>
        {t('txt_date')}
      </h6>
      {dates.map((date, index: number) => {
        const { id, label, value = '' } = date;
        const isOtherDate = id === ID_OTHER_DATE;
        const labelView = `${t(label)}${isOtherDate ? '' : ':'} ${value}`;
        const disableText = t('txt_make_payment_disable_radio');

        return (
          <div key={id} className="d-flex mt-16 cursor-pointer">
            {isDisableDate &&
            (isOtherDate || label === 'txt_paymentDueDate') ? (
              <Tooltip
                element={disableText}
                variant="default"
                placement="top"
                triggerClassName="d-block"
              >
                <Radio
                  id={`makePayment-amount-${storeId}-${index}`}
                  dataTestId={genAmtId(`${label}_${testId}`, 'option', '')}
                >
                  <Radio.Input
                    disabled
                    value={id}
                    checked={selectedID === id}
                    name={`make-payment-date-${storeId}`}
                  />
                  <Radio.Label id={`make-payment-date-${storeId}`}>
                    {labelView}
                  </Radio.Label>
                </Radio>
              </Tooltip>
            ) : (
              <Radio
                id={`makePayment-amount-${storeId}-${index}`}
                dataTestId={genAmtId(`${label}_${testId}`, 'option', '')}
              >
                <Radio.Input
                  value={id}
                  checked={selectedID === id}
                  onChange={() => handleSelectDate(date)}
                  name={`make-payment-date-${storeId}`}
                />
                <Radio.Label
                  id={`make-payment-date-${storeId}`}
                  title={labelView}
                >
                  {labelView}
                </Radio.Label>
              </Radio>
            )}
          </div>
        );
      })}
      {showOtherDate && (
        <div className="pl-24 mt-16">
          <DatePicker
            required
            id={`paymentDate-${storeId}`}
            name="paymentDate"
            label={t('txt_payment_date')}
            error={{
              status: selectedDate.value === null,
              message: t('txt_payment_date_is_required')
            }}
            minDate={minDateRef.current}
            maxDate={otherMaxDay}
            dateTooltip={handleDateTooltip}
            value={isNull(selectedValue) ? undefined : selectedValue}
            onBlur={handleBlurOtherDate}
            onChange={handleChangeOtherDate}
            dataTestId={genAmtId(testId, 'paymentDate', '')}
          />
        </div>
      )}
    </div>
  );
};

export default PaymentDetail;
