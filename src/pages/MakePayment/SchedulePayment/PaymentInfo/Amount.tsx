import React, { useCallback, useEffect, useState } from 'react';

// components
import {
  NumericTextBox,
  OnChangeNumericEvent,
  Radio
} from 'app/_libraries/_dls/components';

// Redux
import { useDispatch } from 'react-redux';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import {
  selectAmount,
  dataAmounts,
  selectBalanceAmount
} from '../_redux/selectors';
import { schedulePaymentActions as actions } from '../_redux/reducers';

// Helper
import { getLabelView } from 'pages/MakePayment/helper';
import { ID_OTHER_AMOUNT, OTHER_AMOUNT } from '../../constants';

// Type
import { PaymentAmountView } from '../types';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

const Amount: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();

  const amounts = useStoreIdSelector<PaymentAmountView[]>(dataAmounts);
  const balanceAmount =
    useStoreIdSelector<number | undefined>(selectBalanceAmount);
  const selectedAmount = useStoreIdSelector<PaymentAmountView>(selectAmount);
  const [statusOtherAmount, setStatusOtherAmount] =
    useState<{ status?: boolean; message: string }>();

  const { id: selectedID, value: selectedValue } = selectedAmount;

  const showOtherAmount = selectedID === ID_OTHER_AMOUNT;
  const testId =
    'account-details_make-payment-modal_schedule-payment_payment-detail_amount';

  const handleSelectAmount = (amount: PaymentAmountView) => {
    const value = amount.id === ID_OTHER_AMOUNT ? undefined : amount.value;
    dispatch(
      actions.selectedAmount({
        storeId,
        data: { ...amount, value }
      })
    );
  };

  const handleSetOtherAmount = ({ value }: OnChangeNumericEvent) => {
    const data = { ...OTHER_AMOUNT, value, id: ID_OTHER_AMOUNT };
    dispatch(actions.selectedAmount({ storeId, data }));
  };

  const handleBlur = () => {
    if (selectedValue !== undefined) return;

    const data = { ...OTHER_AMOUNT, id: ID_OTHER_AMOUNT, value: '' };

    dispatch(actions.selectedAmount({ storeId, data }));
  };

  const getStatus = useCallback(() => {
    const is0 = `${Number(selectedValue)}` === '0';
    const isEmptyValue = selectedValue === '';
    const isUnderBalanceAmount =
      selectedValue !== undefined &&
      balanceAmount !== undefined &&
      selectedValue > balanceAmount;
    const isInvalid = isEmptyValue || is0 || undefined || isUnderBalanceAmount;

    let message = '';

    if (isEmptyValue) {
      message = 'txt_payment_amount_required';
    } else if (is0) {
      message = 'txt_payment_amount_greater_0';
    } else if (isUnderBalanceAmount) {
      message = 'txt_other_amount_error';
    }

    return {
      status: isInvalid,
      message: t(message)
    };
  }, [balanceAmount, selectedValue, t]);

  useEffect(() => {
    if (!showOtherAmount) return;

    const newStatus = getStatus();

    setStatusOtherAmount(newStatus);
    dispatch(
      actions.setIsInvalidSchedule({ storeId, value: newStatus.status })
    );
  }, [dispatch, getStatus, showOtherAmount, storeId]);

  return (
    <div data-testid={genAmtId(testId, 'container', '')}>
      <h6
        className="color-grey mt-24"
        data-testid={genAmtId(testId, 'title', '')}
      >
        {t('txt_amount')}
      </h6>
      {amounts.map(amount => {
        const { label, value, id, disabled } = amount;
        const labelView = getLabelView(t, label, value, id);

        return (
          <div key={id} className="d-flex mt-16">
            <Radio
              id={`makePayment-amount-${storeId}-${label}`}
              dataTestId={genAmtId(`${label}_${testId}`, 'option', '')}
            >
              <Radio.Input
                disabled={disabled}
                value={value}
                checked={selectedID === id}
                onChange={() => handleSelectAmount(amount)}
                name={`makePayment-amount-${storeId}`}
              />
              <Radio.Label
                title={t(labelView)}
                id={`make-payment-amount-${storeId}`}
              >
                {labelView}
              </Radio.Label>
            </Radio>
          </div>
        );
      })}
      {showOtherAmount && (
        <div className="pl-24 mt-16">
          <NumericTextBox
            id={`paymentMount-${storeId}`}
            label={t('txt_payment_amount')}
            placeholder="0.00"
            format="c2"
            required
            error={statusOtherAmount}
            onBlur={handleBlur}
            value={selectedValue}
            onChange={handleSetOtherAmount}
            dataTestId={genAmtId(testId, 'paymentMount', '')}
          />
        </div>
      )}
    </div>
  );
};

export default Amount;
