import React from 'react';
import { fireEvent, screen } from '@testing-library/react';

// hooks
import { AccountDetailProvider } from 'app/hooks';

// test-utils
import { renderMockStore, storeId } from 'app/test-utils';

// components
import Amount from './Amount';
import { NumericProps } from 'app/_libraries/_dls/components';
import { InputProps } from 'app/_libraries/_dls/components/Radio/Input';

// redux
import { schedulePaymentActions } from '../_redux/reducers';
import { ID_OTHER_AMOUNT, OTHER_AMOUNT } from 'pages/MakePayment/constants';

jest.mock('app/_libraries/_dls/components', () => {
  return {
    ...(jest.requireActual('app/_libraries/_dls/components') as object),
    NumericTextBox: ({ onChange, onBlur }: NumericProps) => {
      return (
        <>
          <input
            data-testid="NumericTextBox-onChange"
            onChange={e => onChange && onChange(e.target)}
          />
          <button
            data-testid="NumericTextBox-onBlur"
            onClick={e => onBlur && onBlur(e)}
          />
        </>
      );
    }
  };
});

jest.mock(
  'app/_libraries/_dls/components/Radio/Input',
  () =>
    ({ onChange }: InputProps) => {
      return (
        <button
          data-testid="Radio.Input-onChange"
          onClick={e => onChange && onChange(e)}
        />
      );
    }
);

const initialState: Partial<RootState> = {
  schedulePayment: {
    [storeId]: {
      paymentInfo: {
        dates: [],
        amountInfo: [
          { id: '1', value: '25', label: 'Minimum Payment Due' },
          { id: ID_OTHER_AMOUNT, value: '0', label: 'Last Statement Balance' },
          { id: '2', value: '0', label: 'Current Balance' },
          { id: '3', value: '0', label: 'Past Due Amount' }
        ],
        paymentDueDate: new Date().toISOString(),
        balanceAmount: -2
      },

      selectedAmount: { value: '-1', id: `otherAmount` },
      isBankAcctsLoading: false,
      isLoadingPaymentInfo: false
    }
  }
};

const mockImp = () => ({ type: 'type' });
let spy1: jest.SpyInstance;

afterEach(() => {
  if (spy1) {
    spy1.mockReset();
    spy1.mockRestore();
  }
});

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId: storeId, accEValue: storeId }}>
      <Amount />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper(initialState);

    expect(screen.getByText(/Past Due Amount/)).toBeInTheDocument();
  });

  it('Render UI => amount is 0', () => {
    renderWrapper({
      schedulePayment: {
        [storeId]: {
          selectedAmount: { value: '0', id: `otherAmount` }
        }
      }
    });

    expect(screen.getByText('txt_amount')).toBeInTheDocument();
  });

  it('Render UI => amount is empty', () => {
    renderWrapper({
      schedulePayment: {
        [storeId]: {
          selectedAmount: { value: '', id: `otherAmount` }
        }
      }
    });

    expect(screen.getByText('txt_amount')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleSelectAmount', () => {
    const mockAction = jest.fn().mockImplementation(mockImp);
    spy1 = jest
      .spyOn(schedulePaymentActions, 'selectedAmount')
      .mockImplementation(mockAction);

    renderWrapper(initialState);

    const radios = screen.getAllByTestId('Radio.Input-onChange');
    fireEvent.click(radios[0]);
    fireEvent.click(radios[1]);

    expect(mockAction).toBeCalledWith({
      storeId,
      data: initialState.schedulePayment![storeId].paymentInfo?.amountInfo[0]
    });
  });

  it('handleSetOtherAmount', () => {
    const mockAction = jest.fn().mockImplementation(mockImp);
    spy1 = jest
      .spyOn(schedulePaymentActions, 'selectedAmount')
      .mockImplementation(mockAction);

    renderWrapper(initialState);

    fireEvent.change(screen.getByTestId('NumericTextBox-onChange'), {
      target: { id: ID_OTHER_AMOUNT, value: 10 }
    });

    expect(mockAction).toBeCalledWith({
      storeId,
      data: { ...OTHER_AMOUNT, id: ID_OTHER_AMOUNT, value: '10' }
    });
  });

  it('handleBlur', () => {
    const mockAction = jest.fn().mockImplementation(mockImp);
    spy1 = jest
      .spyOn(schedulePaymentActions, 'selectedAmount')
      .mockImplementation(mockAction);

    renderWrapper(initialState);

    fireEvent.click(screen.getByTestId('NumericTextBox-onBlur'));

    expect(mockAction).not.toBeCalled();
  });

  it('handleBlur > selected value is null', () => {
    const state: Partial<RootState> = {
      schedulePayment: {
        [storeId]: {
          paymentInfo: {
            dates: [],
            amountInfo: [
              { id: '1', value: '25', label: 'Minimum Payment Due' },
              { id: '2', value: '0', label: 'Last Statement Balance' },
              { id: '3', value: '0', label: 'Current Balance' },
              { id: '4', value: '0', label: 'Past Due Amount' }
            ],
            paymentDueDate: new Date().toISOString(),
            balanceAmount: -5
          },

          selectedAmount: { id: `otherAmount` },
          isBankAcctsLoading: false,
          isLoadingPaymentInfo: false
        }
      }
    };
    const mockAction = jest.fn().mockImplementation(mockImp);
    spy1 = jest
      .spyOn(schedulePaymentActions, 'selectedAmount')
      .mockImplementation(mockAction);

    renderWrapper(state);

    fireEvent.click(screen.getByTestId('NumericTextBox-onBlur'));

    expect(mockAction).toBeCalledWith({
      storeId,
      data: { ...OTHER_AMOUNT, id: ID_OTHER_AMOUNT, value: '' }
    });
  });
});
