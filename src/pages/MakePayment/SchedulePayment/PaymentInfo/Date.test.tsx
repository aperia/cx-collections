import React from 'react';
import datetime from 'date-and-time';

// hooks
import { AccountDetailProvider } from 'app/hooks';

// test-utils
import { renderMockStore, storeId } from 'app/test-utils';

// components
import DateComponent from './Date';
import { fireEvent, screen } from '@testing-library/react';

// Redux
import { schedulePaymentActions } from '../_redux/reducers';
import { DatePickerProps } from 'app/_libraries/_dls/components';
import { OTHER_DATE, ID_OTHER_DATE } from 'pages/MakePayment/constants';

// utils

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');
  const { DatePicker } = dlsComponents;
  return {
    ...dlsComponents,
    DatePicker: (props: DatePickerProps) => {
      const { onBlur, onChange, dateTooltip } = props;
      return (
        <>
          <input
            data-testid="DatePicker-onChange"
            onChange={(e: any) => onChange && onChange(e.target)}
          />
          <button
            data-testid="DatePicker-onBlur"
            onClick={(e: any) => onBlur && onBlur(e)}
          />
          <input
            data-testid="DatePicker_dateTooltip"
            onChange={(e: any) => dateTooltip && dateTooltip(e.target.value)}
          />
          <DatePicker {...props} />
        </>
      );
    }
  };
});

const newDate = new Date();

const initialState: Partial<RootState> = {
  schedulePayment: {
    [storeId]: {
      paymentInfo: {
        dates: [{ id: 'date_id', value: newDate }],
        balanceAmount: 10,
        paymentDueDate: '12/12/2021',
        amountInfo: [
          { value: '25', label: 'Minimum Payment Due' },
          { value: '0', label: 'Last Statement Balance' },
          { value: '0', label: 'Current Balance' },
          { value: '0', label: 'Past Due Amount' }
        ]
      },
      selectedDate: {
        value: newDate,
        id: `paymentDueOtherDate`
      },
      isBankAcctsLoading: false,
      isLoadingPaymentInfo: false
    }
  }
};

const stateWithoutDate: Partial<RootState> = {
  schedulePayment: {
    [storeId]: {
      paymentInfo: {
        dates: [],
        balanceAmount: 10,
        paymentDueDate: '12/12/2021',
        amountInfo: [
          { value: '25', label: 'Minimum Payment Due' },
          { value: '0', label: 'Last Statement Balance' },
          { value: '0', label: 'Current Balance' },
          { value: '0', label: 'Past Due Amount' }
        ]
      },
      selectedDate: {
        value: newDate,
        id: `paymentDueOtherDate`
      },
      isBankAcctsLoading: false,
      isLoadingPaymentInfo: false
    }
  }
};

let spy1: jest.SpyInstance;

afterEach(() => {
  if (spy1) {
    spy1.mockReset();
    spy1.mockRestore();
  }
});

const renderWrapper = (defaultState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId: storeId, accEValue: storeId }}>
      <DateComponent />
    </AccountDetailProvider>,
    { initialState: defaultState }
  );
};

describe('Render', () => {
  it('Render UI with label view paymentDueOtherDate', () => {
    renderWrapper({
      paybyPhoneConfig: {
        currentValue: { maxDay: 'maxDay' }
      },
      schedulePayment: {
        [storeId]: {
          paymentInfo: {
            dates: [{ id: ID_OTHER_DATE, value: null }],
            paymentDueDate: '12/12/2021',
            amountInfo: [],
            balanceAmount: 10
          }
        }
      }
    } as MagicKeyValue);

    expect(screen.getByText('txt_date')).toBeInTheDocument();
  });

  it('Render UI with label view paymentDueDate', () => {
    renderWrapper({
      schedulePayment: {
        [storeId]: {
          paymentInfo: {
            dates: [
              { id: 'date_id', value: newDate, label: 'txt_paymentDueDate' }
            ],
            paymentDueDate: '12/12/2021',
            amountInfo: [],
            balanceAmount: 10
          },
          isDisableDate: true
        }
      }
    });

    expect(screen.getByText('txt_date')).toBeInTheDocument();
  });

  it('Render UI > empty state', () => {
    renderWrapper({
      schedulePayment: {
        [storeId]: {
          paymentInfo: {
            dates: [{ id: 'date_id', value: null }],
            paymentDueDate: '12/12/2021',
            amountInfo: [],
            balanceAmount: 10
          },
          selectedDate: {
            value: null,
            id: `paymentDueOtherDate`
          }
        }
      }
    });

    expect(screen.getByText('txt_date')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleSelectDate', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy1 = jest
      .spyOn(schedulePaymentActions, 'selectedDate')
      .mockImplementation(mockAction);
    renderWrapper(initialState);

    screen.getByRole('radio').click();

    expect(mockAction).toBeCalledWith({
      storeId,
      data: initialState.schedulePayment![storeId].paymentInfo?.dates[0]
    });
  });

  it('handleChangeOtherDate', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy1 = jest
      .spyOn(schedulePaymentActions, 'selectedDate')
      .mockImplementation(mockAction);
    renderWrapper(initialState);

    const date = '2021/01/05';

    fireEvent.change(screen.getByTestId('DatePicker-onChange'), {
      target: { value: date }
    });

    expect(mockAction).toBeCalledWith({
      storeId,
      data: {
        ...OTHER_DATE,
        value: date
      }
    });
  });

  it('handleChangeOtherDate', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy1 = jest
      .spyOn(schedulePaymentActions, 'selectedDate')
      .mockImplementation(mockAction);

    renderWrapper(initialState);

    fireEvent.click(screen.getByTestId('DatePicker-onBlur'));

    expect(mockAction).not.toBeCalled();
  });

  describe('handleDateTooltip', () => {
    it('with date', () => {
      const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
      spy1 = jest
        .spyOn(schedulePaymentActions, 'selectedDate')
        .mockImplementation(mockAction);

      renderWrapper(initialState);

      fireEvent.change(screen.getByTestId('DatePicker_dateTooltip'), {
        target: { value: datetime.addDays(new Date(), -1) }
      });

      expect(mockAction).not.toBeCalled();
    });

    it('with empty', () => {
      const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
      spy1 = jest
        .spyOn(schedulePaymentActions, 'selectedDate')
        .mockImplementation(mockAction);

      renderWrapper(stateWithoutDate);

      fireEvent.change(screen.getByTestId('DatePicker_dateTooltip'), {
        target: { value: datetime.addDays(new Date(), -1) }
      });

      expect(mockAction).not.toBeCalled();
    });
  });

  it('handleChangeOtherDate > selected value is null', () => {
    const state: Partial<RootState> = {
      schedulePayment: {
        [storeId]: {
          paymentInfo: {
            dates: [{ id: 'date_id' }],
            paymentDueDate: '12/12/2021',
            amountInfo: [
              { value: '25', label: 'Minimum Payment Due' },
              { value: '0', label: 'Last Statement Balance' },
              { value: '0', label: 'Current Balance' },
              { value: '0', label: 'Past Due Amount' }
            ],
            balanceAmount: 10
          },
          selectedDate: {
            id: `paymentDueOtherDate`
          },
          isBankAcctsLoading: false,
          isLoadingPaymentInfo: false
        }
      }
    };

    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy1 = jest
      .spyOn(schedulePaymentActions, 'selectedDate')
      .mockImplementation(mockAction);

    renderWrapper(state);

    fireEvent.click(screen.getByTestId('DatePicker-onBlur'));

    expect(mockAction).toBeCalledWith({
      storeId,
      data: {
        ...OTHER_DATE,
        value: null
      }
    });
  });
});
