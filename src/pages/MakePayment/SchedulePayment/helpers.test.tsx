import * as helper from './helpers';
import { BankAccount as ScheduleBankAccount } from 'pages/MakePayment/SchedulePayment/types';
import get from 'lodash.get';
import { GRID_ID as GRID_ID_SCHEDULE } from './PaymentAccounts';

describe('functions', () => {
  it('getDataViewWithCode with params', () => {
    // no params
    const codes = ['code'];
    const data: MagicKeyValue[] = [{ accNbr: { eValue: codes[0] } }];
    const pathCode = 'accNbr.eValue';

    const result = helper.getDataViewWithCode(data, codes, pathCode);
    expect(result).toEqual(data);
  });

  it('getBankAccountByCheckedList without params', () => {
    // no params
    const result = helper.getBankAccountByCheckedList();
    expect(result).toEqual({});
  });

  it('getBankAccountByCheckedList with params', () => {
    // with checkList param
    const checkList: string[] = ['ky-1'];

    let result = helper.getBankAccountByCheckedList([], checkList);
    expect(result).toEqual({});

    // width all params
    const bankAccounts: ScheduleBankAccount[] = [
      { accNbr: { eValue: checkList[0] } }
    ];

    result = helper.getBankAccountByCheckedList(bankAccounts, checkList);
    expect(result).toEqual(
      bankAccounts.find(
        bankAccount => get(bankAccount, GRID_ID_SCHEDULE) === checkList[0]
      ) || {}
    );
  });
});
