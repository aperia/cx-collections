import React, { useCallback, useLayoutEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';

// Components
import PaymentMethod from './PaymentMethod';
import PaymentDetail from './PaymentInfo';
import AddCard from './AddCard';

// Redux
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { schedulePaymentActions } from './_redux/reducers';
import {
  getLoadingDeleteRegisteredAccount,
  selectLoadingCheckStatus,
  takeLoadingMakePayment,
  selectIsMakePaymentFromSettlement
} from './_redux/selectors';

// helper
import classNames from 'classnames';
import { AddAccountBankStep } from './constants';
import { SimpleBar } from 'app/_libraries/_dls/components';
import SettlementPaymentInfo from './SettlementPaymentInfo';

const MakePayment: React.FC<{}> = () => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const divRef = useRef<HTMLDivElement | null>(null);
  const divRightRef = useRef<HTMLDivElement | null>(null);
  const isLoading = useStoreIdSelector<boolean>(takeLoadingMakePayment);
  const isLoadingDelete = useStoreIdSelector<boolean>(
    getLoadingDeleteRegisteredAccount
  );
  const isLoadingCheckStatus = useStoreIdSelector<boolean>(
    selectLoadingCheckStatus
  );

  const isMakePaymentFromSettlement = useStoreIdSelector<boolean>(
    selectIsMakePaymentFromSettlement
  );

  const loading = isLoading || isLoadingCheckStatus || isLoadingDelete;

  const handleAddAccount = useCallback(() => {
    dispatch(
      schedulePaymentActions.setAccountBankStep({
        storeId,
        step: AddAccountBankStep.FILL_INFO
      })
    );
  }, [dispatch, storeId]);

  useLayoutEffect(() => {
    divRightRef.current && (divRightRef.current.style.width = '356px');
    divRef.current && (divRef.current.style.height = `calc(100% - 103px)`);
  }, []);

  return (
    <div className="d-flex" ref={divRef}>
      <div
        className={classNames('border-right flex-1 ', {
          loading
        })}
      >
        <SimpleBar>
          <div className="p-24">
            <AddCard />
            <PaymentMethod onAddAccount={handleAddAccount} />
          </div>
        </SimpleBar>
      </div>

      <div className={classNames({ loading })} ref={divRightRef}>
        <SimpleBar>
          <div className="p-24">
            {isMakePaymentFromSettlement ? (
              <SettlementPaymentInfo />
            ) : (
              <PaymentDetail />
            )}
          </div>
        </SimpleBar>
      </div>
    </div>
  );
};

export default MakePayment;
