import React, { useEffect } from 'react';
import { batch, useDispatch } from 'react-redux';

// Hook
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// Redux
import { schedulePaymentActions as actions } from '../_redux/reducers';
import {
  getAcceptedBalanceAmount,
  takeLoadingMakePayment
} from '../_redux/selectors';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Component

import { genAmtId } from 'app/_libraries/_dls/utils';
import { formatCommon } from 'app/helpers';
import { PaymentAmountView, PaymentDateView } from '../types';
import {
  ID_OTHER_AMOUNT,
  OTHER_AMOUNT,
  OTHER_DATE
} from 'pages/MakePayment/constants';

export interface SettlementPaymentInfoProps {}

const SettlementPaymentInfo: React.FC<SettlementPaymentInfoProps> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();

  const isLoading = useStoreIdSelector<boolean>(takeLoadingMakePayment);
  const acceptedBalanceAmount = useStoreIdSelector<string | undefined>(
    getAcceptedBalanceAmount
  );
  const testId =
    'account-details_make-payment-modal_schedule-payment_payment-detail';

  useEffect(() => {
    const amountPayment: PaymentAmountView = {
      ...OTHER_AMOUNT,
      value: acceptedBalanceAmount,
      id: ID_OTHER_AMOUNT
    };
    
    const paymentDate = {
      ...OTHER_DATE,
      value: formatCommon(new Date()).time.date
    } as PaymentDateView;

    batch(() => {
      dispatch(
        actions.selectedAmount({
          storeId,
          data: amountPayment
        })
      );

      dispatch(actions.selectedDate({ storeId, data: paymentDate }));
    });
  }, [dispatch, storeId, acceptedBalanceAmount]);

  if (isLoading) return null;

  return (
    <>
      <h5 data-testid={genAmtId(testId, 'title', '')}>
        {t('txt_paymentInformation')}
      </h5>
      <div>
        <h6
          className="color-grey mt-24"
          data-testid={genAmtId(testId, 'title', '')}
        >
          {t('txt_amount')}
        </h6>
        <div className="d-flex mt-16">
          <p className="fs-14 color-grey-d20">
            {t('txt_accepted_balance_amount')}:
          </p>
          <p className="ml-4 fs-14 fw-500 color-grey-d20">
            {formatCommon(acceptedBalanceAmount!).currency(2)}
          </p>
        </div>
      </div>
      <hr />
      <div>
        <h6 className="color-grey" data-testid={genAmtId(testId, 'title', '')}>
          {t('txt_date')}
        </h6>
        <div className="d-flex mt-16">
          <p className="fs-14 color-grey-d20">{t('txt_currentDate')}:</p>
          <p className="ml-4 fs-14 fw-500 color-grey-d20">
            {formatCommon(new Date()).time.date}
          </p>
        </div>
      </div>
    </>
  );
};

export default SettlementPaymentInfo;
