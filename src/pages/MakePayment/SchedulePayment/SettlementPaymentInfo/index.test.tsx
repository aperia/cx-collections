import { screen } from '@testing-library/react';
import { AccountDetailProvider } from 'app/hooks';
import { accEValue, renderMockStore, storeId } from 'app/test-utils';
import React from 'react';
import SettlementPaymentInfo from './index';

const initialState: Partial<RootState> = {
  schedulePayment: {
    [storeId]: {
      isBankAcctsLoading: false,
      isLoadingPaymentInfo: false,
      isBankAcctInfoLoading: false,
      isLoadingCommonDataToAdd: false
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <SettlementPaymentInfo />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('Render', () => {
  it('should render null if isLoading', () => {
    renderWrapper({
      ...initialState,
      schedulePayment: {
        [storeId]: {
          ...[storeId],
          isBankAcctsLoading: true
        }
      }
    });

    expect(screen.queryByText('txt_paymentInformation')).toBeNull();
  });

  it('should render UI', () => {
    renderWrapper({});
    screen.getByText('txt_paymentInformation');
  });
});
