import get from 'lodash.get';
import { BankAccount as ScheduleBankAccount } from './types';
import { GRID_ID as GRID_ID_SCHEDULE } from './PaymentAccounts';

export const getDataViewWithCode = (
  data: MagicKeyValue[],
  codes: string[],
  pathCode: string
) => {
  return data.filter(item => codes.includes(get(item, pathCode)));
};

export const getBankAccountByCheckedList = (
  bankAccounts: ScheduleBankAccount[] = [],
  checkList: string[] = []
) => {
  const [eValue] = checkList;
  
  if (!eValue) return {};

  return (
    bankAccounts.find(
      bankAccount => get(bankAccount, GRID_ID_SCHEDULE) === eValue
    ) || {}
  );
};

export const formatMaskValueToPost = (value = '', character = 4) => {
  return '****' + value.trim().substr(-character);
};
