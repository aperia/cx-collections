import React from 'react';

// Hooks
import { AccountDetailProvider } from 'app/hooks';

// Helper
import { renderMockStore, storeId } from 'app/test-utils';
import BankInfo from './index';

// Type
import { BankAccount, PAYMENT_CARD_TYPE } from '../types';
import { screen } from '@testing-library/react';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

jest.mock('app/_libraries/_dof/core/View', () => () => (
  <div data-testid="dof-view" />
));

const data: BankAccount = {
  accNbr: {
    maskedValue:
      '15e2b0d3c33891ebb0f1ef609ec419420c20e320ce94c65fbc8c3312448eb225s',
    eValue: '•••••6789'
  },
  accType: {
    description: 'Checking',
    code: PAYMENT_CARD_TYPE.CHECKING
  },
  moreInfo: {
    bankRoutingNbr: '•••••2903',
    bankName: 'Morissette - Braun',
    bankCity: 'East Alvah',
    bankState: 'TX - Texas'
  },
  expirationDate: '01/2021',
  isPendingStatus: true
};

const initialState: Partial<RootState> = {
  schedulePayment: {
    [storeId]: {
      openModal: true
    }
  }
};

const handleOnChange = jest.fn();

const renderWrapper = (initialState: Partial<RootState>, data: BankAccount) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <BankInfo data={data} onClose={handleOnChange} />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('Testing BankInfo', () => {
  it('Render UI with empty data', () => {
    renderWrapper(initialState, {});

    expect(
      screen.getByText(I18N_COMMON_TEXT.BANK_INFORMATION)
    ).toBeInTheDocument();
  });

  it('Render UI', () => {
    renderWrapper(initialState, data);

    expect(
      screen.getByText(I18N_COMMON_TEXT.BANK_INFORMATION)
    ).toBeInTheDocument();
  });
});
