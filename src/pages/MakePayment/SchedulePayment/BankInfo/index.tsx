import React, { useEffect } from 'react';

// components
import {
  Icon,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { View } from 'app/_libraries/_dof/core';

// Hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useDispatch } from 'react-redux';

// Type
import {
  BankAccount,
  BankAccountInfo
} from 'pages/MakePayment/SchedulePayment/types';

// Redux
import { schedulePaymentActions } from '../_redux/reducers';
import {
  takeLoadingBankAcctInfoStatus,
  takePaymentAccInfo
} from '../_redux/selectors';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface BankInfoProps {
  onClose: () => void;
  data: BankAccount;
}

const BankInfo: React.FC<BankInfoProps> = ({ onClose, data }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId, accEValue } = useAccountDetail();

  const dataInfo = useStoreIdSelector<BankAccountInfo>(takePaymentAccInfo);
  const isLoading = useStoreIdSelector<boolean>(takeLoadingBankAcctInfoStatus);

  const testId = 'make-payment-modal_schedule-payment_bank-info';

  useEffect(() => {
    dispatch(
      schedulePaymentActions.getBankAccountInfo({
        storeId,
        postData: {
          accountId: accEValue,
          eValue: data?.accNbr?.eValue || ''
        }
      })
    );
  }, [dispatch, storeId, data, accEValue]);

  return (
    <Modal show sm loading={isLoading} dataTestId={testId}>
      <ModalHeader
        border
        closeButton
        onHide={onClose}
        dataTestId={`${testId}-header`}
      >
        <ModalTitle dataTestId={`${testId}-title`}>
          {t(I18N_COMMON_TEXT.BANK_INFORMATION)}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div className="text-center">
          <Icon
            color="green"
            name={'success'}
            className="fs-60"
            dataTestId={`${testId}-success-icon`}
          />
          <p
            className="color-green mt-8"
            data-testid={genAmtId(testId, 'method-information-verified', '')}
          >
            {t('txt_method_information_verified')}
          </p>
        </div>
        <div className="border bg-light-l20 br-light-l04 br-radius-8 px-16 pb-16 mt-24">
          <View
            descriptor="bankInfoView"
            id={`bankInfoView-${storeId}`}
            formKey={`bankInfoView-${storeId}`}
            value={dataInfo}
          />
        </div>
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_close')}
        onCancel={onClose}
        dataTestId={`${testId}-footer`}
      />
    </Modal>
  );
};

export default BankInfo;
