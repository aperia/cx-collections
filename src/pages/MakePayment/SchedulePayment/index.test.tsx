import { screen, waitFor } from '@testing-library/react';
import { AccountDetailProvider } from 'app/hooks';
import { renderMockStore, storeId, accEValue } from 'app/test-utils';
import React from 'react';
import { AddAccountBankStep } from './constants';

import SchedulePayment from './index';
import { PaymentMethodProps } from './PaymentMethod';
import { MAKE_PAYMENT_FROM } from './types';

// Redux
import { schedulePaymentActions } from './_redux/reducers';

jest.mock('./AddCard', () => () => <div>Payment AddCard</div>);
jest.mock('./PaymentInfo', () => () => <div>Payment Information</div>);

jest.mock('./PaymentMethod', () => (props: PaymentMethodProps) => {
  const { onAddAccount = () => undefined } = props;

  return (
    <>
      Payment Method
      <button data-testid="PaymentMethod_onAddAccount" onClick={onAddAccount} />
    </>
  );
});

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <SchedulePayment />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper({});
    expect(screen.getByText(/Payment Method/)).toBeInTheDocument();
    expect(screen.getByText(/Payment Information/)).toBeInTheDocument();
    expect(screen.getByText(/Payment AddCard/)).toBeInTheDocument();
  });

  it('should render PaymentDetail if acceptedBalanceAmount === undefined', () => {
    renderWrapper({
      schedulePayment: {
        [storeId]: {}
      }
    });

    expect(screen.getByText(/Payment Information/)).toBeInTheDocument();
  });

  it('should render SettlementPaymentInfo if acceptedBalanceAmount has value', () => {
    renderWrapper({
      schedulePayment: {
        [storeId]: {
          acceptedBalanceAmount: '100'
        }
      }
    });

    waitFor(() =>
      expect(
        screen.findByText('txt_accepted_balance_amount')
      ).toBeInTheDocument()
    );
  });
});

describe('Actions', () => {
  it('onAddAccount', () => {
    // mock
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    jest
      .spyOn(schedulePaymentActions, 'setAccountBankStep')
      .mockImplementation(mockAction);

    // render
    renderWrapper({
      schedulePayment: {
        [storeId]: {
          from: MAKE_PAYMENT_FROM.SETTLEMENT
        }
      }
    });

    // simulate
    screen.getByTestId('PaymentMethod_onAddAccount').click();

    // expect
    expect(mockAction).toBeCalledWith({
      storeId,
      step: AddAccountBankStep.FILL_INFO
    });
  });
});
