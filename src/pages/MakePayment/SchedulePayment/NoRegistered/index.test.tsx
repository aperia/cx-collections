import React from 'react';

// Hooks
import { AccountDetailProvider } from 'app/hooks';

// Helper
import { renderMockStore, storeId } from 'app/test-utils';
import NoRegistered from './index';

// Type
import { screen } from '@testing-library/react';

import * as entitlementsHelpers from 'app/entitlements/helpers';

let spy: jest.SpyInstance;

beforeEach(() => {
  spy = jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
});

afterEach(() => {
  spy.mockReset();
  spy.mockRestore();
});

describe('Render', () => {
  it('Render UI', () => {
    renderMockStore(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <NoRegistered />
      </AccountDetailProvider>,
      { initialState: {} },
      true
    );

    expect(screen.getByText('txt_no_register_payment')).toBeInTheDocument();
  });
});
