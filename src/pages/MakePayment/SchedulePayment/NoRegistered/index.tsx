import React, { useContext } from 'react';

// components
import { Button, Icon } from 'app/_libraries/_dls/components';

// Context
import { PaymentMethodContext } from '../PaymentMethod';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';
import { useAccountDetail } from 'app/hooks';

export const NoRegistered: React.FC<any> = ({ dataTestId }) => {
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const { onAddAcc } = useContext(PaymentMethodContext);

  const showAddMethod = checkPermission(PERMISSIONS.MAKE_PAYMENT_ADD_BANK_REGISTRATION, storeId);

  return (
    <div className="mt-80 text-center" data-testid={genAmtId(dataTestId!, 'container', '')}>
      <Icon name="card-collection" className="fs-80 color-light-l12" />
      <p
        className="mt-20 mb-24"
        data-testid={genAmtId(dataTestId!, 'message', '')}
      >{t('txt_no_register_payment')}</p>
      {showAddMethod && (
        <Button
          size="sm"
          variant="outline-primary"
          onClick={onAddAcc}
          dataTestId={genAmtId(dataTestId!, 'add-payment-method-btn', '')}
        >
          {t(I18N_COMMON_TEXT.ADD_PAYMENT_METHOD)}
        </Button>
      )}
    </div>
  );
};

export default NoRegistered;
