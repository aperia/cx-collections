import { AxiosResponse } from 'axios';

export interface ValidateBankAccountDebitInfoPayload {
  bankRoutingNumber: string;
  bankAccountNumber: string;
  bankAccountType: PAYMENT_CARD_TYPE.DEBIT;
}

export interface ValidateBankAccountCreditInfoPayload {
  bankRoutingNumber: string;
  bankAccountNumber: string;
  bankAccountType: PAYMENT_CARD_TYPE.CREDIT;
}

export interface ValidateBankAccountInfoPayload {
  bankAccountType: PAYMENT_CARD_TYPE.CREDIT;
  bankRoutingNumber: string;
  bankAccountNumber: string;
  bankName: string;
  bankCity: string;
  bankState: string;
}

export interface ValidatePaymentView {
  bankRoutingNumber?: string;
  checkingAccountNumber?: string;
  savingsAccountNumber?: string;
  bankName?: string;
  bankCity?: string;
  bankState?: string;
}

export interface ValidatePaymentResponse {
  branchCode?: string;
  fileKey?: string;
  institutionCity?: string;
  institutionFullName?: string;
  institutionNameAbbreviation?: string;
  institutionState?: string;
  institutionTypeCode?: string;
  routingNumber?: string;
}
export interface SubmitMakePaymentData {
  transactionType?: string;
  accountId?: string;
  crdNbrEValue?: string;
  accountNumberMaskedValue?: string;
  amount?: PaymentAmountView;
  schedulePaymentDate?: Date | string;
  fee?: ReactText;
  paymentMedium?: string;
  registrationID?: string;
  method: string;
  cvv2?: string;
}

export interface PaymentInfo {
  amountInfo: PaymentAmountView[];
  paymentDueDate: string;
  balanceAmount: number;
  dates: PaymentDateView[];
}

export interface BankAccountInfo extends Omit<BankAccount, 'accNbr'> {
  accNbr?: string;
}

export interface BankAccount {
  id?: string;
  registrationID?: string;
  accNbr?: {
    maskedValue?: string;
    eValue?: string;
  };
  accType?: {
    description?: string;
    code?: PAYMENT_CARD_TYPE;
  };
  moreInfo?: {
    bankRoutingNbr?: string;
    bankName?: string;
    bankCity?: string;
    bankState?: string;
  };
  expirationDate?: string;
  isPendingStatus?: boolean;
}

export interface PaymentAmount {
  value?: ReactText;
  label?: string;
}

export interface PaymentAmountView {
  id?: string;
  title?: string;
  value?: ReactText;
  label?: string;
  disabled?: boolean;
}

export interface PaymentDateView {
  id?: string;
  value?: Date | null;
  label?: string;
}

export interface AddPaymentCardProps {
  loading?: boolean;
  errorMessage?: string;
  bankInfo?: ValidatePaymentView | null;
  errorFields?: Array<string>;
  step?: number;
  isInvalid?: boolean;
}

export interface SubmitMakePayment {
  loading?: boolean;
  error?: boolean;
}

export interface SubmitMakePaymentRequest {
  storeId: string;
  accountId: string;
  data: SubmitMakePaymentData;

  onSuccess?: (response: AxiosResponse<MagicKeyValue>) => void;
  onFailure?: (error: MagicKeyValue) => void;
}

export interface MakePaymentState {
  allCheckedList?: string[];
  checkedList?: string[];
  selectedType?: string;
  selectedAcc?: BankAccount;
  bankAccts?: Array<BankAccount>;
  bankAcctInfo?: BankAccountInfo;
  paymentInfo?: PaymentInfo;
  isBankAcctsLoading?: boolean;
  isBankAcctInfoLoading?: boolean;
  isLoadingPaymentInfo?: boolean;
  openModal?: boolean;
  selectedAmount?: PaymentAmountView;
  selectedDate?: PaymentDateView;
  deleteRegisteredAccount?: DeleteRegisterAccount;
  addAccount?: AddPaymentCardProps;
  submitMakePayment?: SubmitMakePayment;
  isInvalidSchedule?: boolean;

  isLoadingCheckStatus?: boolean;
  canDelete?: boolean;
  bankAccountWithDelete?: BankAccount;

  isLoadingCommonDataToAdd?: boolean;
  commonDataToAdd?: {
    firstName?: string;
    lastName?: string;
    zipCode?: string;
  };
  isDisableDate?: boolean;
  acceptedBalanceAmount?: string;
  method?: string;
  cvv2?: string;
  makePaymentSummary?: MagicKeyValue;
  from?: MAKE_PAYMENT_FROM;
}

export enum MAKE_PAYMENT_FROM {
  QUICK_ACTION = 'QUICK_ACTION',
  SETTLEMENT = 'SETTLEMENT'
}

export interface MakePaymentResult extends Record<string, MakePaymentState> {}

export interface AllCheckedListPayload {
  storeId: string;
  allCheckedList: string[];
}

export interface SelectedPaymentMethodPayload {
  storeId: string;
  checkedList: string[];
  method?: string;
}

export interface SelectedPaymentPayload {
  id: string;
  storeId: string;
}

export interface SelectedPaymentInfoPayload {
  data: PaymentDateView;
  storeId: string;
}

export interface SelectedAccountPaymentPayload {
  data: BankAccount;
  storeId: string;
}

export interface BankAccountArgs {
  storeId: string;
  postData: {
    accountId: string;
  };
}

export interface BankAccountPayload {
  data: any;
}

// Bank acct detail

export interface BankAccountInfoArgs {
  storeId: string;
  postData: {
    accountId: string;
    eValue: string;
  };
}

export interface BankAccountInfoPayload {
  data: BankAccountInfo;
}

// Payment info

export interface PaymentInfoArgs {
  storeId: string;
  postData: {
    accountId: string;
    memberSequenceIdentifier?: string;
  };
}

export interface CustomerInquiry {
  cycleToDateUnpaidBilledPaymentDueAmount: string;
  cycleToDateNonDelinquentMinimumPaymentDueAmount: string;
  lastStatementBalanceAmount: string;
  currentBalanceAmount: string;
  nextPaymentDueDate: string;
  pastDueAmount: string;
  minimumPaymentAmount: string;
}

export interface DataResponse {
  customerInquiry: CustomerInquiry[];
}

export interface PaymentInfoPayload {
  data: any;
}

export interface DeleteRegisterAccount {
  loading: boolean;
}

export interface DeleteRegisterAccountArgs {}

export interface DeleteRegisteredAccountArgs {
  storeId: string;
  postData: {
    registrationID?: string;
    accountId: string;
    bankAccount?: BankAccount;
  };
}

export interface DeleteRegisteredAccountPayload {
  message?: string;
}

export type RegisterPaymentMethodRequest = ValidateAddBankAccountInfoArgs &
  RegisterBankAccount;

export interface RegisterBankAccount {
  accountId: string;
  storeId: string;
  onSuccess?: Function;
  onFailure?: Function;
}

export interface ValidateBankAccountDebitInfoArgs {
  cardNumber: string;
  expirationDate: string;
  bankAccountType: BankAccountType;
}

export interface ValidateBankAccountCreditInfoArgs {
  cardNumber: string;
  expirationDate: string;
  bankAccountType: BankAccountType;
}

export interface BankAccountType {
  code:
    | PAYMENT_CARD_TYPE.SAVING
    | PAYMENT_CARD_TYPE.BANK_INFO
    | PAYMENT_CARD_TYPE.CARD_INFO
    | PAYMENT_CARD_TYPE.CHECKING
    | PAYMENT_CARD_TYPE.DEBIT
    | PAYMENT_CARD_TYPE.CREDIT;
  description: string;
}

export interface ValidateBankAccountInfoArgs {
  bankRoutingNumber: string;
  bankAccountNumber: string;
  bankAccountType: BankAccountType;
}

// ------------------------------------
import { ReactText } from 'react';

export enum PAYMENT_CARD_TYPE {
  BANK_INFO = 'eCheck',
  CARD_INFO = 'CheckCard',
  CHECKING = 'C',
  SAVING = 'S',
  DEBIT = 'D',
  CREDIT = 'CR'
}

export interface SubmitMakePaymentData {
  transactionType?: string;
  accountId?: string;
  crdNbrEValue?: string;
  accountNumberMaskedValue?: string;
  amount?: PaymentAmountView;
  schedulePaymentDate?: Date | string;
  fee?: ReactText;
}

export interface SubmitMakePaymentPostData {
  transactionType?: string;
  accountId?: string;
  crdNbrEValue?: string;
  accountNumberMaskedValue?: string;
  amount?: ReactText;
  schedulePaymentDate?: Date | string;
  fee?: ReactText;
}

export interface BankAccountDetailPayload {
  accountId: string;
  eValue: string;
}

export enum PaymentAction {
  VALIDATE_BANK_ACCOUNT_INFO = 'validateBankAccountInfo'
}

export interface RegisterBankAccount {
  accountId: string;
  storeId: string;
  onSuccess?: Function;
  onFailure?: Function;
}

export interface BankAccountType {
  code:
    | PAYMENT_CARD_TYPE.BANK_INFO
    | PAYMENT_CARD_TYPE.CARD_INFO
    | PAYMENT_CARD_TYPE.SAVING
    | PAYMENT_CARD_TYPE.CHECKING
    | PAYMENT_CARD_TYPE.DEBIT
    | PAYMENT_CARD_TYPE.CREDIT;
  description: string;
}

export interface ValidateBankAccountCreditInfoArgs {
  cardNumber: string;
  expirationDate: string;
  bankAccountType: BankAccountType;
}

export interface ValidateBankAccountDebitInfoArgs {
  cardNumber: string;
  expirationDate: string;
  bankAccountType: BankAccountType;
}

export interface ValidateBankAccountInfoArgs {
  bankRoutingNumber: string;
  bankAccountNumber: string;
  bankAccountType: BankAccountType;
}

export interface ValidateBankAccountInfoPayload {
  bankAccountType: PAYMENT_CARD_TYPE.CREDIT;
  bankRoutingNumber: string;
  bankAccountNumber: string;
  bankName: string;
  bankCity: string;
  bankState: string;
}

export interface ValidateBankAccountCreditInfoPayload {
  bankRoutingNumber: string;
  bankAccountNumber: string;
  bankAccountType: PAYMENT_CARD_TYPE.CREDIT;
}
export interface ValidateBankAccountDebitInfoPayload {
  bankRoutingNumber: string;
  bankAccountNumber: string;
  bankAccountType: PAYMENT_CARD_TYPE.DEBIT;
}

export interface PaymentState {
  validateBankAccountInfo:
    | ValidateBankAccountDebitInfoPayload
    | ValidateBankAccountCreditInfoPayload
    | ValidateBankAccountInfoPayload;
}

export type ValidateBankAddAccountInfoPayload =
  PaymentState['validateBankAccountInfo'];

export type ValidateAddBankAccountInfoArgs =
  | ValidateBankAccountDebitInfoArgs
  | ValidateBankAccountCreditInfoArgs
  | ValidateBankAccountInfoArgs;

export type ValidatePaymentMethodRequest = ValidateAddBankAccountInfoArgs &
  RegisterBankAccount;

export interface ValidatePaymentRequest {
  routingNumber?: string;
}

export interface VerifyCardPaymentArgs {
  storeId: string;
  accountId?: string;
  bankAccountNumber?: string;
  bankAccountType?: {
    code?: string;
    description?: string;
  };
  bankRoutingNumber?: string;
  accountType?: {
    code?: string;
    description?: string;
  };
}

export type RegisterPaymentMethodResponse = {
  message: string;
  returnCode?: string;
};

export interface BankAccount {
  accNbr?: {
    maskedValue?: string;
    eValue?: string;
  };
  accType?: {
    description?: string;
    code?: PAYMENT_CARD_TYPE;
  };
  moreInfo?: {
    bankRoutingNbr?: string;
    bankName?: string;
    bankCity?: string;
    bankState?: string;
  };
  expirationDate?: string;
  isPendingStatus?: boolean;
}

export interface PaymentAmount {
  value?: ReactText;
  label?: string;
}

export interface PaymentInfo {
  amountInfo: PaymentAmountView[];
  paymentDueDate: string;
  dates: PaymentDateView[];
}

export interface BankAccountInfo extends Omit<BankAccount, 'accNbr'> {
  accNbr?: string;
}

export interface PaymentAmountView {
  id?: string;
  title?: string;
  value?: ReactText;
  label?: string;
}

export interface PaymentAccountInfoSelected {
  id?: string;
  value?: string;
  label?: string;
}

export interface DeleteRegisteredAccount {
  common: {
    app: string;
    accountId: string;
  };
  action: string;
  lookupReference: string;
  registrationID: string;
}

export interface DeleteRegisteredAccountPayload {
  message?: string;
}

export interface CancelPendingPaymentsArgs {
  storeId: string;
  postData: {
    accountId: string;
    paymentIds: string[];
  };
}

export interface CancelPendingPaymentPostData {
  accountId: string;
  paymentIds: string[];
}

export interface CancelPendingPaymentsPayload {
  payments: MagicKeyValue[];
  data: MagicKeyValue;
}

export interface AccountTypeTemplateProps {
  imageSrc: string;
  title: string;
  id: string;
  selected?: boolean;
  onSelect?: (id: string) => void;
  dataTestId?: string;
}

export enum PAYMENT_TYPE {
  SCHEDULE = 'Schedule',
  DEMAND_ACH = 'ACH'
}

// GetBankAccountsRequestBody

export interface GetBankAccountsRequestBody {
  common: {
    app: string;
    accountId: string;
  };
  lookupReference: string;
}

export interface GetCheckPaymentListPendingStatusRequestBody {
  common: {
    app: string;
    accountId: string;
  };
  keyType: string;
  payeeID: string;
  registrationID: string;
}

export interface GetAddressInfoArgs {
  storeId: string;
  accountId: string;
}

export interface IMakeAdjustmentTransactionArgs {
  accountId: string;
  feeAmount: string;
}

export interface IMakeAdjustmentTransactionRequestBody {
  common: {
    accountId: string;
    user?: string;
    cycle?: string;
  };
  batchType: string;
  batchPrefix: string;
  transactionCode: string;
  transactionAmount: string;
  transactionPostingDate: string;
}
