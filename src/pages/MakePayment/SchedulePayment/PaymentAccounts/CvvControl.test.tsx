import React from 'react';
import { act } from 'react-dom/test-utils';
import { fireEvent, render, screen } from '@testing-library/react';

// //components
import CvvControl from './CvvControl';

// mock store
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';

jest.mock('react-i18next', () => {
  return {
    __esModule: true,
    ...jest.requireActual('react-i18next'),
    useTranslation: () => ({ t: (text: string) => text })
  };
});

const store = createStore(rootReducer, {} as RootState);

describe('CvvControl', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterEach(() => {
    jest.useRealTimers();
    jest.restoreAllMocks();
  });

  it('render', () => {
    // Given
    act(() => {
      render(
        <Provider store={store}>
          <CvvControl numberOnly={true} enable={true} />
        </Provider>
      );
    });

    screen.getByPlaceholderText('txt_cvv2_placeholder');
  });

  it('onChange number', async () => {
    // Given
    act(() => {
      render(
        <Provider store={store}>
          <CvvControl numberOnly={true} enable={true} />
        </Provider>
      );
    });

    const input = screen.getByPlaceholderText(
      'txt_cvv2_placeholder'
    ) as HTMLInputElement;

    act(() => {
      fireEvent.change(input, {
        target: { value: '123' }
      });
      jest.runAllTimers();
    });

    expect(input.value).toEqual('123');
  });

  it('onChange text', async () => {
    // Given
    act(() => {
      render(
        <Provider store={store}>
          <CvvControl numberOnly={true} enable={true} />
        </Provider>
      );
    });

    const input = screen.getByPlaceholderText(
      'txt_cvv2_placeholder'
    ) as HTMLInputElement;

    act(() => {
      fireEvent.change(input, {
        target: { value: 'abc' }
      });
      jest.runAllTimers();
    });

    expect(input.value).toEqual('');
  });

  it('onBlur number with enable=true', async () => {
    // Given
    act(() => {
      render(
        <Provider store={store}>
          <CvvControl numberOnly={true} enable={true} />
        </Provider>
      );
    });

    const input = screen.getByPlaceholderText(
      'txt_cvv2_placeholder'
    ) as HTMLInputElement;

    act(() => {
      fireEvent.change(input, {
        target: { value: '123' }
      });
      jest.runAllTimers();
    });

    fireEvent.blur(input!);
    expect(input.value).toEqual('123');
  });

  it('onBlur number with enable=true, invalidValue', async () => {
    // Given
    act(() => {
      render(
        <Provider store={store}>
          <CvvControl numberOnly={true} enable={true} />
        </Provider>
      );
    });

    const input = screen.getByPlaceholderText(
      'txt_cvv2_placeholder'
    ) as HTMLInputElement;

    act(() => {
      fireEvent.change(input, {
        target: { value: '12' }
      });
      jest.runAllTimers();
    });

    fireEvent.blur(input!);
    expect(input.value).toEqual('12');
  });

  it('onBlur number with enable=true, emptyValue', async () => {
    // Given
    act(() => {
      render(
        <Provider store={store}>
          <CvvControl numberOnly={true} enable={true} />
        </Provider>
      );
    });

    const input = screen.getByPlaceholderText(
      'txt_cvv2_placeholder'
    ) as HTMLInputElement;

    fireEvent.blur(input!);
    expect(input.value).toEqual('');
  });

  it('onBlur number with enable=false', async () => {
    // Given
    act(() => {
      render(
        <Provider store={store}>
          <CvvControl numberOnly={true} enable={false} />
        </Provider>
      );
    });

    const input = screen.getByPlaceholderText(
      'txt_cvv2_placeholder'
    ) as HTMLInputElement;

    act(() => {
      fireEvent.change(input, {
        target: { value: '123' }
      });
      jest.runAllTimers();
    });

    fireEvent.blur(input!);
    expect(input.value).toEqual('123');
  });

  it('onBlur number with readOnly=true', async () => {
    // Given
    act(() => {
      render(
        <Provider store={store}>
          <CvvControl numberOnly={true} enable={true} readOnly={true} />
        </Provider>
      );
    });

    const input = screen.getByPlaceholderText(
      'txt_cvv2_placeholder'
    ) as HTMLInputElement;

    act(() => {
      fireEvent.change(input, {
        target: { value: '123' }
      });
      jest.runAllTimers();
    });

    fireEvent.blur(input!);
    expect(input.value).toEqual('123');
  });
});
