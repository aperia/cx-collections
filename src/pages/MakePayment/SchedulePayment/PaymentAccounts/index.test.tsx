import React from 'react';
import { act, fireEvent, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as entitlementsHelpers from 'app/entitlements/helpers';
// Hooks
import { AccountDetailProvider } from 'app/hooks';
// Helper
import { mockActionCreator, renderMockStore, storeId } from 'app/test-utils';
// Components
import { ButtonProps, GridProps } from 'app/_libraries/_dls/components';
// Type-Constants
import {
  BankAccount,
  PAYMENT_CARD_TYPE
} from 'pages/MakePayment/SchedulePayment/types';
import ConfirmModal from 'pages/__commons/ConfirmModal';
import { ModalData } from 'pages/__commons/ConfirmModal/types';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import PaymentAccounts from '../PaymentAccounts';
// redux
import { schedulePaymentActions } from '../_redux/reducers';

jest.mock('react-redux', () => {
  return {
    ...(jest.requireActual('react-redux') as object),
    useDispatch: () => (x: Function) => jest.fn().mockImplementation(() => x())
  };
});

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');
  const { Grid } = dlsComponents;

  return {
    ...dlsComponents,
    Grid: (props: GridProps) => {
      return (
        <>
          <input
            data-testid="grid-onCheck"
            onChange={e => props.onCheck!([e.target.value])}
          />
          <button
            data-testid="grid-onCheck-empty"
            onClick={e => props.onCheck!([])}
          />
          <Grid {...props} />
        </>
      );
    },
    Button: (props: ButtonProps) => {
      return <button data-testid="dls-tooltip" {...props} disabled={false} />;
    }
  };
});

const defaultData = [
  {
    registrationID: '1',
    accNbr: {
      maskedValue: '•••••6789',
      eValue: '15e2b0d3c33891ebb0f1ef609ec419420c20e320ce94c65fbc8c3312448eb225'
    },
    accType: {
      description: 'Checking',
      code: PAYMENT_CARD_TYPE.CARD_INFO
    },
    moreInfo: {
      bankRoutingNbr: '•••••2903',
      bankName: 'Morissette - Braun',
      bankCity: 'East Alvah',
      bankState: 'TX - Texas'
    },
    expirationDate: '01/2021',
    isPendingStatus: false
  },
  {
    registrationID: '2',
    accNbr: { maskedValue: '•••••6789' },
    accType: {
      description: 'Checking',
      code: PAYMENT_CARD_TYPE.CHECKING
    },
    moreInfo: {
      bankRoutingNbr: '•••••2903',
      bankName: 'Morissette - Braun',
      bankCity: 'East Alvah',
      bankState: 'TX - Texas'
    },
    expirationDate: '01/2021',
    isPendingStatus: false
  },
  {
    registrationID: '3',
    accNbr: {
      maskedValue: '•••••6789',
      eValue: '15e2b0d3c33891ebb0f1ef609ec419420c20e320ce94c65fbc8c3312448eb225'
    },
    accType: {
      description: 'Checking',
      code: PAYMENT_CARD_TYPE.CHECKING
    },
    moreInfo: {
      bankRoutingNbr: '•••••2903',
      bankName: 'Morissette - Braun',
      bankCity: 'East Alvah',
      bankState: 'TX - Texas'
    },
    expirationDate: '01/2021',
    isPendingStatus: true
  }
];

const initialState: Partial<RootState> = {
  schedulePayment: {
    [storeId]: {
      openModal: true,
      isBankAcctsLoading: false,
      isLoadingPaymentInfo: false,
      canDelete: false
    }
  }
};

const renderWrapper = (
  data: BankAccount[] = [],
  defaultState: Partial<RootState> = initialState
) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <PaymentAccounts data={data} />
    </AccountDetailProvider>,
    { initialState: defaultState }
  );
};

const confirmModalSpy = mockActionCreator(confirmModalActions);
const schedulePaymentSpy = mockActionCreator(schedulePaymentActions);

let spy: jest.SpyInstance;

beforeEach(() => {
  spy = jest
    .spyOn(entitlementsHelpers, 'checkPermission')
    .mockReturnValue(true);
});

afterEach(() => {
  spy.mockReset();
  spy.mockRestore();
});

describe('Testing PaymentAccounts', () => {
  it('Should render NoRegistered with mock store', () => {
    spy = jest
      .spyOn(entitlementsHelpers, 'checkPermission')
      .mockReturnValue(false);
    renderWrapper();

    expect(screen.getByText(/txt_no_register_payment/)).toBeInTheDocument();
  });

  it('Should render PaymentAccounts with mock store', () => {
    renderWrapper(defaultData, {
      schedulePayment: {
        [storeId]: {
          allCheckedList: ['test', 'test 1'],
          checkedList: ['test']
        }
      }
    });

    expect(
      screen.getAllByText(/txt_select_method_to_make_payment/).length
    ).toBeGreaterThan(0);
  });

  it('Should render PaymentAccounts with mock store', () => {
    renderWrapper(defaultData, {
      schedulePayment: {
        [storeId]: {
          allCheckedList: ['test', 'test 1'],
          checkedList: ['test']
        }
      }
    });

    expect(
      screen.getAllByText(/txt_select_method_to_make_payment/).length
    ).toBeGreaterThan(0);
  });
});

describe('Actions', () => {
  it('handleDelete', () => {
    confirmModalSpy('open', ({ confirmCallback }: ModalData) => {
      confirmCallback!();
      return { type: 'type' };
    });
    const mockAction = schedulePaymentSpy('getCheckPaymentListPendingStatus');
    jest.useFakeTimers();
    renderWrapper(defaultData, {
      schedulePayment: {
        [storeId]: {
          openModal: true,
          isBankAcctsLoading: false,
          isLoadingPaymentInfo: false,
          canDelete: true
        }
      }
    });
    jest.runAllTimers();
    const buttonDelete = screen.getByRole('button', { name: /txt_deactivate/ });

    act(() => {
      userEvent.click(buttonDelete);
    });
    const bankAccount = defaultData[0];

    expect(mockAction).toBeCalledWith({
      storeId,
      postData: {
        accountId: storeId,
        registrationID: bankAccount.registrationID,
        bankAccount
      }
    });
  });

  it('handleCheck', () => {
    const checkedList = ['ky-1'];
    const mockAction = jest
      .fn()
      .mockImplementation((payload: ModalData) => ({ type: 'type' }));
    jest
      .spyOn(schedulePaymentActions, 'setSelectedPaymentMethod')
      .mockImplementation(mockAction);
    renderWrapper(defaultData);
    fireEvent.change(screen.getByTestId('grid-onCheck'), {
      target: { value: checkedList[0] }
    });
    expect(mockAction).toBeCalledWith({ storeId, checkedList, method: 'Card' });
  });

  it('handleCheck => empty list', () => {
    const mockAction = jest
      .fn()
      .mockImplementation((payload: ModalData) => ({ type: 'type' }));
    jest
      .spyOn(schedulePaymentActions, 'setSelectedPaymentMethod')
      .mockImplementation(mockAction);
    renderWrapper(defaultData);
    fireEvent.click(screen.getByTestId('grid-onCheck-empty'));
    expect(mockAction).toBeCalledWith({
      storeId,
      checkedList: [''],
      method: 'Card'
    });
  });

  it('it should call confirmModalActions should work correctly', () => {
    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });
    jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);
    jest.useFakeTimers();
    const openConfirmSpy = confirmModalSpy('open');
    renderWrapper(defaultData, {
      schedulePayment: {
        [storeId]: {
          openModal: true,
          isBankAcctsLoading: false,
          isLoadingPaymentInfo: false,
          canDelete: true
        }
      }
    });
    jest.runAllTimers();
    expect(openConfirmSpy).toBeCalledWith(
      expect.objectContaining({
        body: 'txt_content_confirm_deactivate',
        confirmText: 'txt_deactivate',
        title: 'txt_title_confirm_deactivate'
      })
    );
  });

  it('it should call triggerDeleteRegisteredAccount should work correctly', () => {
    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });
    jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);

    jest.useFakeTimers();

    const triggerDeleteRegisteredAccountSpy = schedulePaymentSpy(
      'triggerDeleteRegisteredAccount'
    );

    renderMockStore(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <PaymentAccounts data={defaultData} />
        <ConfirmModal />
      </AccountDetailProvider>,
      {
        initialState: {
          schedulePayment: {
            [storeId]: {
              openModal: true,
              isBankAcctsLoading: false,
              isLoadingPaymentInfo: false,
              canDelete: true
            }
          },
          confirmModal: {
            open: true
          }
        }
      }
    );
    jest.runAllTimers();

    const btnConfirmDeActive = screen.getAllByTestId('dls-tooltip')[1];
    act(() => {
      userEvent.click(btnConfirmDeActive);
    });

    expect(triggerDeleteRegisteredAccountSpy).toBeCalledWith({
      storeId,
      postData: { accountId: storeId }
    });
  });

  it('it should close Modal correctly', () => {
    jest.useFakeTimers();
    renderWrapper(defaultData, {
      schedulePayment: {
        [storeId]: {
          openModal: true,
          isBankAcctsLoading: false,
          isLoadingPaymentInfo: false,
          canDelete: false
        }
      }
    });
    jest.runAllTimers();

    expect(
      screen.getByText(/txt_content_cannot_deactivate/)
    ).toBeInTheDocument();

    jest.useFakeTimers();
    renderWrapper(defaultData, {
      schedulePayment: {
        [storeId]: {
          openModal: true,
          isBankAcctsLoading: false,
          isLoadingPaymentInfo: false,
          canDelete: true
        }
      }
    });
    jest.runAllTimers();

    const buttonClose = screen.getByRole('button', { name: /txt_close/ });
    act(() => {
      userEvent.click(buttonClose);
    });
    jest.useFakeTimers();
    renderWrapper(defaultData, {
      schedulePayment: {
        [storeId]: {
          openModal: true,
          isBankAcctsLoading: false,
          isLoadingPaymentInfo: false,
          canDelete: true
        }
      }
    });
    jest.runAllTimers();
    const buttonDeActive = screen.getAllByRole('button', {
      name: /txt_deactivate/
    });
    expect(buttonDeActive).toHaveLength(2);
  });
});
