import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';

// redux
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail } from 'app/hooks';
import { schedulePaymentActions } from '../_redux/reducers';

// components
import { TextBox, TextBoxRef } from 'app/_libraries/_dls/components';

// Helper
import { isEmpty } from 'lodash';
import { stringValidate } from 'app/helpers';

export interface CvvControlProps {
  readOnly?: boolean;
  enable?: boolean;
  numberOnly?: boolean;
}

const CvvControl: React.FC<CvvControlProps> = ({
  readOnly,
  enable,
  numberOnly
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { storeId } = useAccountDetail();

  const [value, setValue] = useState('');
  const [error, setError] = useState<string>('');

  const refTextBox = useRef<TextBoxRef | null>(null);

  useLayoutEffect(() => {
    refTextBox.current &&
      refTextBox.current.containerElement &&
      (refTextBox.current.containerElement.style.width = '93px') &&
      refTextBox.current.inputElement &&
      (refTextBox.current.inputElement.style.width = '93px');
  }, []);

  useEffect(() => {
    if (!enable) {
      setError('');
      setValue('');

      dispatch(
        schedulePaymentActions.setCvv2({
          storeId,
          cvv2: ''
        })
      );
    }
  }, [dispatch, enable, storeId]);

  const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;

    if (numberOnly && !stringValidate(value).isNumber()) {
      return;
    }

    setValue(value);
  };

  const handleOnBlur = useCallback(
    (event: React.FocusEvent<HTMLInputElement>) => {
      if (!enable || readOnly) return;

      const { value: newValue } = event.target;      

      dispatch(
        schedulePaymentActions.setCvv2({
          storeId,
          cvv2: newValue
        })
      );

      if (isEmpty(newValue)) {
        setError('txt_cvv2_required');
        return;
      }

      if (newValue.length !== 3) {
        setError('txt_cvv2_invalid_format');
        return;
      }

      setError('');
    },
    [dispatch, enable, readOnly, storeId]
  );

  return (
    <TextBox
      ref={refTextBox}
      size="sm"
      disabled={!enable}
      value={value}
      type={'password'}
      required={true}
      onChange={handleOnChange}
      onBlur={handleOnBlur}
      maxLength={3}
      minLength={3}
      placeholder={t('txt_cvv2_placeholder')}
      error={
        error
          ? {
              message: t(error) as string,
              status: true
            }
          : undefined
      }
    />
  );
};

export default CvvControl;
