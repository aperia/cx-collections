import React, { useCallback, useEffect, useMemo, useState } from 'react';

// Hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
// Components
import {
  Button,
  // Button,
  ColumnType,
  InlineMessage,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

// helper
import get from 'lodash.get';
import isUndefined from 'lodash.isundefined';
import { getMethodByCardType } from 'pages/MakePayment/helper';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

// types
import {
  BankAccount,
  PAYMENT_CARD_TYPE
} from 'pages/MakePayment/SchedulePayment/types';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';

// Redux
import { useDispatch } from 'react-redux';
import { getDataViewWithCode } from '../helpers';
import NoRegistered from '../NoRegistered';
import { schedulePaymentActions } from '../_redux/reducers';
import {
  allCheckedListData,
  checkedListData,
  selectCanDelete,
  takeLoadingMakePayment
} from '../_redux/selectors';
import { GRID_ID, GROUP, KEY_GRID_FIXED_LEFT, PATH_CODE } from './constants';
import CvvControl from './CvvControl';

import PaymentAccount from './PaymentAccount';

export interface AccountInfo {
  name?: string;
  data?: BankAccount;
}

export interface PaymentMethodProps {
  data: BankAccount[];
  dataTestId?: string;
}

const PaymentAccounts: React.FC<PaymentMethodProps> = ({
  data,
  dataTestId
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { accEValue: accountId, storeId } = useAccountDetail();

  const isLoading = useStoreIdSelector<boolean>(takeLoadingMakePayment);
  const canDelete = useStoreIdSelector<boolean>(selectCanDelete);
  const checkList = useStoreIdSelector<string[]>(checkedListData);
  const allCheckedList = useStoreIdSelector<string[]>(allCheckedListData);

  const [showModalCannotDelete, setShowModalCannotDelete] = useState(false);

  const handleDelete = useCallback(
    (bankAccount: BankAccount) => {
      dispatch(
        schedulePaymentActions.getCheckPaymentListPendingStatus({
          storeId,
          postData: {
            accountId,
            registrationID: bankAccount.registrationID!,
            bankAccount
          }
        })
      );
    },
    [dispatch, storeId, accountId]
  );

  const handleCheck = (checklist: typeof checkList, cardType: string) => {
    const newValue = checklist.pop() || '';

    dispatch(
      schedulePaymentActions.setSelectedPaymentMethod({
        storeId,
        checkedList: [newValue],
        method: getMethodByCardType(cardType)
      })
    );
  };

  useEffect(() => {
    if (isUndefined(canDelete)) return;

    if (canDelete) {
      const confirmCallback = () => {
        dispatch(
          schedulePaymentActions.triggerDeleteRegisteredAccount({
            storeId,
            postData: { accountId }
          })
        );
      };
      dispatch(
        confirmModalActions.open({
          title: 'txt_title_confirm_deactivate',
          body: 'txt_content_confirm_deactivate',
          confirmCallback,
          confirmText: 'txt_deactivate'
        })
      );
      return;
    }
    setShowModalCannotDelete(true);
  }, [accountId, canDelete, dispatch, storeId, t]);

  // Update list checked & recheck current checked is exist
  // if not exist will update checked to first item in allCheckedList
  useEffect(() => {
    if (!data || data.length === 0) return;

    // get checkedList the same render View
    const newAllCheckedList = GROUP.reduce<any>((prev, { codes }) => {
      const dataView = getDataViewWithCode(data, codes, PATH_CODE);
      const ids = dataView.map(value => get(value, GRID_ID));

      return [...prev, ...ids];
    }, []);

    dispatch(
      schedulePaymentActions.setAllCheckedList({
        storeId,
        allCheckedList: newAllCheckedList
      })
    );
  }, [data, storeId, dispatch]);

  const views = useMemo(() => {
    return GROUP.map(({ codes, columns, header, ...rest }) => {
      const _columns = (columns as MagicKeyValue[]).map(
        ({ Header, id, ...restCol }) =>
          ({
            ...restCol,
            id,
            Header: t(Header)
          } as ColumnType)
      );

      if (
        checkPermission(
          PERMISSIONS.MAKE_PAYMENT_DELETE_BANK_REGISTRATION,
          storeId
        )
      ) {
        _columns.push({
          id: 'action',
          Header: t('txt_action'),
          accessor: (item: BankAccount, index: number) => (
            <Button
              size="sm"
              className="m-auto"
              variant="outline-danger"
              onClick={() => handleDelete(item)}
              dataTestId={genAmtId(dataTestId!, `deactivate-${index}`, '')}
            >
              {t('txt_deactivate')}
            </Button>
          ),
          width: 125,
          cellProps: { className: 'text-center' },
          cellBodyProps: { className: 'td-sm' }
        });
      }

      return {
        data: getDataViewWithCode(data, codes, PATH_CODE),
        header: t(header),
        columns: [..._columns],
        ...rest
      };
    });
  }, [data, dataTestId, handleDelete, storeId, t]);

  const onCloseModal = () => {
    setShowModalCannotDelete(false);
  };

  if (data.length === 0 && !isLoading)
    return (
      <NoRegistered dataTestId={genAmtId(dataTestId!, 'no-registerd', '')} />
    );

  return (
    <>
      {!isLoading &&
        views.map(({ header, cardType, ...props }) => {
          const { data: dataProp } = props;
          const [itemSelected] = checkList;
          const checkedList = allCheckedList.includes(itemSelected)
            ? checkList
            : [];

          const isEmptyValue = !dataProp || dataProp.length === 0;

          const columns =
            cardType === PAYMENT_CARD_TYPE.BANK_INFO
              ? props.columns
              : [
                  ...props.columns.slice(0, 2),
                  {
                    id: 'cvv2',
                    Header: t('txt_cvv2_header'),
                    accessor: (item: BankAccount) => {
                      return (
                        <CvvControl
                          numberOnly={true}
                          enable={checkList.includes(item?.id || '')}
                        />
                      );
                    },
                    width: 253,
                    autoWidth: true
                  },
                  ...props.columns.slice(2, props.columns.length)
                ];

          return (
            <div key={cardType} className="mt-24">
              {KEY_GRID_FIXED_LEFT.includes(cardType) && (
                <>
                  <p
                    className="my-16"
                    data-testid={genAmtId(
                      `${cardType}_${dataTestId || ''}`,
                      'select-payment-method-title',
                      ''
                    )}
                  >
                    {t('txt_select_method_to_make_payment')}
                  </p>

                  <InlineMessage
                    variant="warning"
                    dataTestId={genAmtId(
                      `${cardType}_${dataTestId || ''}`,
                      'select-payment-method-warning',
                      ''
                    )}
                  >
                    {t('txt_make_payment_warning')}
                  </InlineMessage>
                </>
              )}

              <p
                className="fw-500"
                data-testid={genAmtId(
                  `${cardType}_${dataTestId || ''}`,
                  'select-payment-method-header',
                  ''
                )}
              >
                {header}
              </p>

              {isEmptyValue ? (
                <p
                  className="color-grey mt-16"
                  data-testid={genAmtId(
                    `${cardType}_${dataTestId || ''}`,
                    'no-data',
                    ''
                  )}
                >
                  {t('txt_no_accounts_to_display')}
                </p>
              ) : (
                <PaymentAccount
                  cardType={cardType}
                  className="grid-check-style mt-16"
                  variant={{
                    type: 'radio',
                    id: `${storeId}-${cardType}-grid-schedule`,
                    // NOTES
                    // isFixedLeft alway fixed, can change
                    // isFixedLeft is required
                    isFixedLeft: true,
                    width: 63
                  }}
                  dataItemKey={GRID_ID}
                  {...props}
                  columns={columns}
                  checkedList={checkedList}
                  onCheck={(dataKeyList: string[]) =>
                    handleCheck(dataKeyList, cardType)
                  }
                  dataTestId={genAmtId(
                    `${cardType}_${dataTestId || ''}`,
                    'payment-accounts',
                    ''
                  )}
                />
              )}
            </div>
          );
        })}
      <Modal sm show={showModalCannotDelete} dataTestId={dataTestId}>
        <ModalHeader
          border
          closeButton
          onHide={onCloseModal}
          dataTestId={genAmtId(dataTestId!, 'header', '')}
        >
          <ModalTitle dataTestId={genAmtId(dataTestId!, 'header-title', '')}>
            {t('txt_title_cannot_deactivate')}
          </ModalTitle>
        </ModalHeader>
        <ModalBody dataTestId={genAmtId(dataTestId!, 'body', '')}>
          {t('txt_content_cannot_deactivate')}
        </ModalBody>
        <ModalFooter
          cancelButtonText={t('txt_close')}
          onCancel={onCloseModal}
          dataTestId={genAmtId(dataTestId!, 'footer', '')}
        />
      </Modal>
    </>
  );
};

export default React.memo(PaymentAccounts);
export { GRID_ID };
