import dateTime from 'date-and-time';
import { renderMaskedValue } from 'app/helpers';
import { BankAccount, PAYMENT_CARD_TYPE } from '../types';

export const GRID_ID = 'id';
export const PATH_CODE = 'accType.code';

export const CONFIG_BANK_INFORMATION = [
  {
    id: 'accountNumber',
    Header: 'txt_acc_nbr',
    accessor: (item: BankAccount) => {
      const maskedValue = item?.accNbr?.maskedValue || '';
      return renderMaskedValue(maskedValue);
    },
    width: 170
  },
  {
    id: 'type',
    Header: 'txt_type',
    accessor: (item: BankAccount) => {
      const description = item.accType?.description || '';
      return description;
    },
    width: 120
  },
  {
    id: 'routingNumber',
    Header: 'txt_routing_number',
    accessor: (item: BankAccount) => {
      return item.moreInfo?.bankRoutingNbr || '';
    },
    width: 170
  },
  {
    id: 'bankName',
    Header: 'txt_bank_name',
    accessor: (item: BankAccount) => {
      const bankName = item.moreInfo?.bankName || '';
      return bankName;
    },
    width: 200,
    autoWidth: true
  },
  {
    id: 'bankCityState',
    Header: 'txt_bank_city_state',
    accessor: (item: BankAccount) => {
      const { bankCity, bankState } = item.moreInfo || {};
      return bankCity && bankState
        ? `${bankCity}, ${bankState}`
        : bankCity || bankState;
    },
    width: 250,
    autoWidth: true
  }
];

export const CONFIG_CARD_INFO = [
  {
    id: 'accountNumber',
    Header: 'txt_card_number',
    accessor: (item: BankAccount) => {
      const { maskedValue = '' } = item.accNbr || {};
      const maskedValueView = maskedValue.includes('*')
        ? maskedValue
        : '*****' + maskedValue;
      return renderMaskedValue(maskedValueView);
    },
    width: 160,
    autoWidth: true
  },
  {
    id: 'expirationDate',
    Header: 'txt_expiration_date',
    accessor: ({ expirationDate }: BankAccount) =>
      (dateTime as any).transform(expirationDate, 'M/Y', 'MM/YY'),
    width: 160,
    autoWidth: true
  }
];

export const KEY_GRID_FIXED_LEFT = [PAYMENT_CARD_TYPE.BANK_INFO];

export const GROUP = [
  {
    codes: [PAYMENT_CARD_TYPE.BANK_INFO],
    cardType: PAYMENT_CARD_TYPE.BANK_INFO,
    header: 'txt_bank_information',
    columns: CONFIG_BANK_INFORMATION
  },
  {
    codes: [PAYMENT_CARD_TYPE.CARD_INFO],
    cardType: PAYMENT_CARD_TYPE.CARD_INFO,
    header: 'txt_card_information',
    columns: CONFIG_CARD_INFO
  }
];
