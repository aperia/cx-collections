import React from 'react';
import { screen } from '@testing-library/react';
import { renderMockStoreId, queryByClass } from 'app/test-utils';
import PaymentAccount from './PaymentAccount';

import { PAYMENT_CARD_TYPE } from '../types';

const renderData = (num: number) => {
  const data = [];
  for (let i = 0; i < num; i += 1) {
    data.push({ id: `test-${i}` });
  }
  return data;
};

describe('Test UI', () => {
  it('it should be render correctly with data is empty', () => {
    renderMockStoreId(
      <PaymentAccount
        columns={[]}
        data={[]}
        cardType={PAYMENT_CARD_TYPE.CHECKING}
      />,
      {}
    );
    expect(screen.getByText('txt_no_accounts_to_display')).toBeInTheDocument();
  });

  it('it should be render correctly with data is undefined', () => {
    renderMockStoreId(
      <PaymentAccount columns={[]} cardType={PAYMENT_CARD_TYPE.CHECKING} />,
      {}
    );
    expect(screen.getByText('txt_no_accounts_to_display')).toBeInTheDocument();
  });

  it('it should be render correctly with data is undefined', () => {
    renderMockStoreId(
      <PaymentAccount columns={[]} cardType={PAYMENT_CARD_TYPE.CARD_INFO} />,
      {}
    );
    expect(screen.getByText('txt_no_accounts_to_display')).toBeInTheDocument();
  });

  it('it should be render correctly with pagination', () => {
    const { wrapper } = renderMockStoreId(
      <PaymentAccount
        columns={[]}
        data={renderData(20)}
        cardType={PAYMENT_CARD_TYPE.CHECKING}
      />,
      {}
    );
    expect(queryByClass(wrapper.container, 'pagination')).toBeInTheDocument();
  });
});
