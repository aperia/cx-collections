import React, { Fragment } from 'react';

// Hooks
import { usePagination } from 'app/hooks';

// Components
import { Grid, Pagination, GridProps } from 'app/_libraries/_dls/components';

// helper
import { GRID_ID } from './constants';

// types
import {
  BankAccount,
  PAYMENT_CARD_TYPE
} from 'pages/MakePayment/SchedulePayment/types';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { EMPTY_ARRAY } from 'app/constants';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface Props extends Omit<GridProps, 'data'> {
  data?: BankAccount[];
  header?: string;
  cardType: PAYMENT_CARD_TYPE;
  handleCheck?: any;
}

const PaymentAccount: React.FC<Props> = ({
  data: dataProp = EMPTY_ARRAY,
  header,
  cardType,
  dataTestId,
  ...rest
}) => {
  const { t } = useTranslation();

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(dataProp);

  const showPagination = total > pageSize[0];

  const isEmpty = dataProp.length === 0;

  return (
    <div className="mt-24">
      <p
        className="fw-500"
        data-testid={genAmtId(dataTestId!, 'header', '')}
      >{header}</p>

      {isEmpty ? (
        <p
          className="color-grey mt-16"
          data-testid={genAmtId(dataTestId!, 'no-data', '')}
        >{t('txt_no_accounts_to_display')}</p>
      ) : (
        <Fragment>
          <Grid {...rest} data={gridData} dataTestId={genAmtId(dataTestId!, 'grid', '')} />

          {showPagination && (
            <div className="pt-16">
              <Pagination
                totalItem={total}
                pageSize={pageSize}
                pageNumber={currentPage}
                pageSizeValue={currentPageSize}
                compact
                onChangePage={onPageChange}
                onChangePageSize={onPageSizeChange}
                dataTestId={genAmtId(dataTestId!, 'pagination', '')}
              />
            </div>
          )}
        </Fragment>
      )}
    </div>
  );
};

export default React.memo(PaymentAccount);
export { GRID_ID };
