import { apiService } from 'app/utils/api.service';
import {
  GetCheckPaymentListPendingStatusRequestBody,
  GetBankAccountsRequestBody,
  DeleteRegisteredAccount,
  BankAccountInfo,
  BankAccountDetailPayload,
  CancelPendingPaymentPostData,
  CancelPendingPaymentsPayload,
  ValidatePaymentRequest,
  IMakeAdjustmentTransactionRequestBody
} from './types';

const schedulePaymentService = {
  checkPaymentListPendingStatus(
    data: GetCheckPaymentListPendingStatusRequestBody
  ) {
    const url =
      window.appConfig?.api?.payment?.checkPaymentListPendingStatus || '';
    return apiService.post<CancelPendingPaymentsPayload>(url, data);
  },

  getBankAccounts(data: GetBankAccountsRequestBody) {
    const url = window.appConfig?.api?.payment?.getBankAccounts || '';
    return apiService.post(url, data);
  },
  deleteRegisteredAccount(data: DeleteRegisteredAccount) {
    const url = window.appConfig?.api?.payment?.deleteRegisteredAccount || '';
    return apiService.post(url, data);
  },
  submitScheduleMakePayment(postData: MagicKeyValue) {
    const url = window.appConfig?.api?.payment?.submitScheduleMakePayment || '';
    return apiService.post(url, { ...postData });
  },

  getDetailAccountPaymentInfo(data: BankAccountDetailPayload) {
    const url =
      window.appConfig?.api?.payment?.getDetailAccountPaymentInfo || '';
    return apiService.post<BankAccountInfo>(url, data);
  },

  getDetailPaymentInfo(data: CommonAccountIdRequestBody) {
    const url = window.appConfig?.api?.payment?.getDetailPaymentInfo || '';
    return apiService.post(url, data);
  },

  registerBankAccount(data: Omit<MagicKeyValue, 'storeId'>) {
    if (!data) return;
    const url = window.appConfig?.api?.payment?.registerBankAccount || '';
    return apiService.post<MagicKeyValue>(url, data);
  },

  validateBankAccountInfo(data: ValidatePaymentRequest) {
    if (!data) return;
    const url = window.appConfig?.api?.payment?.validateBankAccountInfo || '';
    return apiService.post(url, data);
  },

  cancelPendingPayments(data: CancelPendingPaymentPostData) {
    const url = window.appConfig?.api?.payment?.cancelPendingPayments || '';
    return apiService.post<CancelPendingPaymentsPayload>(url, data);
  },

  getAddressInfo: (postData: MagicKeyValue) => {
    const url =
      window.appConfig?.api?.cardholdersMaintenance?.getAddressInfo || '';
    return apiService.post<MagicKeyValue>(url, postData);
  },

  makeAdjustmentPayment: (data: IMakeAdjustmentTransactionRequestBody) => {
    const url =
      window.appConfig?.api?.transactionAdjustmentRequest?.adjustTransaction;
    return apiService.post<IMakeAdjustmentTransactionRequestBody>(url!, data);
  }
};
export default schedulePaymentService;
