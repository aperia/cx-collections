// PAYMENT TYPE

export const bankAccountInformationCheckingView =
  'bankAccountInformationCheckingView';
export const bankAccountInformationSavingView =
  'bankAccountInformationSavingView';
export const addCardInfoView = 'addCardInfoView';

export const mappingViewConfirm = {
  bankAccountInformationCheckingView: 'confirmAddBankAccountView',
  bankAccountInformationSavingView: 'confirmAddBankAccountView',
  addCardInfoView: 'confirmAddDebitAccountView'
};

export const TypeAccountTemplateID = {
  BANK_ACCOUNT: 'bankAccount',
  CARD_INFORMATION: 'cardInfo'
};

export const AddAccountBankStep = {
  INIT_INFO: 0,
  FILL_INFO: 1,
  VERIFY_INFO: 2
};

export const ADDRESS_CODE = 'P';
export const ADDRESS_TYPE_LETTER = 'LTTR';
export const ADDRESS_TYPE_BLL = 'BLL1';
export const RHODE_ISLAND = 'RI';
export const MASSACHUSETTS = 'MA';

export const MAKE_ADJUSTMENT_TRANSACTION =
  'schedulePayment/makeAdjustmentTransaction';
