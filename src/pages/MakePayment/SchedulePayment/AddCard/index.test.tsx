import React from 'react';

// hooks
import { AccountDetailProvider } from 'app/hooks';

// test-utils
import {
  queryByClass,
  renderMockStore,
  storeId,
  accEValue
} from 'app/test-utils';
import { schedulePaymentActions } from '../_redux/reducers';

// components
import AddCard from './index';
import { AccountTypeTemplateProps } from '../types';

// constants
import {
  AddAccountBankStep,
  addCardInfoView,
  TypeAccountTemplateID
} from '../constants';
import { act, fireEvent, screen } from '@testing-library/react';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import userEvent from '@testing-library/user-event';
import ApiError from 'app/components/ApiError';
import * as entitlementsHelpers from 'app/entitlements/helpers';

jest.mock('./AccountType', () => ({ onSelect }: AccountTypeTemplateProps) => {
  return (
    <input data-testid="onSelect" onChange={e => onSelect!(e.target.value)} />
  );
});

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockViewChangeValues')
);

const stateFillInfo: any = {
  schedulePayment: {
    [storeId]: {
      addAccount: {
        errorFields: ['bankRoutingNumberTextBox'],
        errorMessage: 'messageError ',
        loading: false,
        bankInfo: undefined,
        step: AddAccountBankStep.FILL_INFO
      },
      selectedAmount: {
        value: '-1',
        id: `amount-${-1}`
      },
      isBankAcctsLoading: false,
      isLoadingPaymentInfo: false
    }
  }
};

const stateVerify: Partial<RootState> = {
  schedulePayment: {
    [storeId]: {
      addAccount: {
        errorFields: ['bankRoutingNumberTextBox'],
        errorMessage: 'messageError ',
        loading: false,
        bankInfo: undefined,
        step: AddAccountBankStep.VERIFY_INFO
      },
      selectedAmount: {
        value: '-1',
        id: `amount-${-1}`
      },
      isBankAcctsLoading: false,
      isLoadingPaymentInfo: false
    }
  }
};

const stateWithApiError: Partial<RootState> = {
  accountDetail: {
    [storeId]: {
      apiErrorSection: {
        errorDetail: {
          addAndEditPaymentMethod: {
            response: 'Test error',
            request: 'Test error'
          }
        }
      }
    } as any
  },
  schedulePayment: {
    [storeId]: {
      addAccount: {
        errorFields: ['bankRoutingNumberTextBox'],
        errorMessage: 'messageError ',
        loading: false,
        bankInfo: undefined,
        step: AddAccountBankStep.FILL_INFO
      },
      selectedAmount: {
        value: '-1',
        id: `amount-${-1}`
      },
      isBankAcctsLoading: false,
      isLoadingPaymentInfo: false
    }
  }
};

const mockImp = () => ({ type: 'type' });

let spy1: jest.SpyInstance;
let spy2: jest.SpyInstance;

afterEach(() => {
  spy1?.mockReset();
  spy1?.mockRestore();

  spy2?.mockReset();
  spy2?.mockRestore();
});

const renderWrapper = (
  initialState: Partial<RootState>,
  onClose?: () => void
) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId: storeId, accEValue }}>
      <AddCard onClose={onClose} />
      <ApiError onClickViewErrorDetail={jest.fn} className="mb-24" />
    </AccountDetailProvider>,
    { initialState }
  );
};

beforeEach(() => {
  jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
});
describe('Render', () => {
  it('Render UI > DEBIT', () => {
    renderWrapper(stateFillInfo);
    fireEvent.change(screen.getByTestId('onSelect'), {
      target: { value: TypeAccountTemplateID.CARD_INFORMATION }
    });
    // expect
    expect(screen.getByText(/txt_card_information/)).toBeInTheDocument();
    expect(screen.getByRole('dialog')).toBeInTheDocument();
  });
  it('Render UI > BANK_ACCOUNT', () => {
    renderWrapper(stateFillInfo);

    expect(screen.getByText(/txt_bank_information/)).toBeInTheDocument();
    expect(screen.getByRole('dialog')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleBackToEntryForm', () => {
    renderWrapper(stateVerify);
    screen.getByText(I18N_COMMON_TEXT.BACK_TO_ENTRY_FORM).click();
    expect(true).toBeTruthy();
  });

  it('onClose', () => {
    const handleClose = jest.fn();
    renderWrapper(stateFillInfo, handleClose);

    screen.getByText('txt_cancel').click();

    expect(handleClose).toHaveBeenCalled();
  });

  it('handleVerify', async () => {
    // mock
    const mockAction = jest.fn().mockImplementation(mockImp);
    spy1 = jest
      .spyOn(schedulePaymentActions, 'getVerifyCardPayment')
      .mockImplementation(mockAction);

    renderWrapper(stateFillInfo);
    screen.getByText(/txt_verify/).click();

    // expect
    expect(mockAction).toBeCalledWith({
      storeId,
      accountId: accEValue,
      bankAccountType: { code: 'Checking', description: 'txt_checking' }
    });
  });

  it('handleVerify > not call getVerifyCardPayment', () => {
    //
    const mockAction = jest.fn().mockImplementation(mockImp);
    spy1 = jest
      .spyOn(schedulePaymentActions, 'getVerifyCardPayment')
      .mockImplementation(mockAction);

    renderWrapper(stateFillInfo);
    const selectAccountType = screen.getByTestId('onSelect');
    act(() => {
      fireEvent.change(selectAccountType, {
        target: { value: TypeAccountTemplateID.CARD_INFORMATION }
      });
    });

    screen.getByText(/txt_verify/).click();
    expect(mockAction).not.toBeCalled();
  });

  it('handleVerify > fill card info', () => {
    const mockAction = jest.fn().mockImplementation(mockImp);
    spy1 = jest
      .spyOn(schedulePaymentActions, 'getVerifyCardPayment')
      .mockImplementation(mockAction);
    const registerBankAccount = jest
      .spyOn(schedulePaymentActions, 'registerBankAccount')
      .mockImplementation(({ onSuccess }: any) => {
        onSuccess();

        return { type: 'type ' } as any;
      });

    renderWrapper(stateFillInfo);
    const selectAccountType = screen.getByTestId('onSelect');
    act(() => {
      fireEvent.change(selectAccountType, {
        target: { value: TypeAccountTemplateID.CARD_INFORMATION }
      });
    });

    act(() => {
      fireEvent.change(screen.getByTestId(addCardInfoView + '_onChange'), {
        target: {
          value: 'value',
          values: { cardDebitNumber: '4242424242424242' },
          previousValues: { cardDebitNumber: '' }
        }
      });
    });

    screen.getByText('txt_verify').click();
    expect(mockAction).not.toBeCalled();

    const addBtn = screen.getByText('txt_add');
    expect(addBtn).toBeDisabled();

    userEvent.click(screen.getByText('txt_confirm_payment_method_information'));

    expect(addBtn).not.toBeDisabled();
    userEvent.click(addBtn);

    expect(registerBankAccount).toBeCalled();
  });

  it('handleVerify > BankAccount', async () => {
    // mock
    const mockAction = jest.fn().mockImplementation(mockImp);
    spy1 = jest
      .spyOn(schedulePaymentActions, 'getVerifyCardPayment')
      .mockImplementation(mockAction);

    renderWrapper(stateFillInfo);

    const radioChecking = screen.getByRole('radio', { name: 'txt_checking' });
    userEvent.click(radioChecking);
    screen.getByText(/txt_verify/).click();
    expect(mockAction).toBeCalledWith({
      storeId,
      accountId: accEValue,
      bankAccountType: { code: 'Checking', description: 'txt_checking' }
    });

    const radioSaving = screen.getByRole('radio', { name: 'txt_savings' });
    userEvent.click(radioSaving);
    screen.getByText(/txt_verify/).click();
    expect(mockAction).toBeCalledWith({
      storeId,
      accountId: accEValue,
      bankAccountType: { code: 'Savings', description: 'txt_savings' }
    });
  });

  it('it should be show api error correctly', async () => {
    renderWrapper(stateWithApiError);
    expect(screen.getByText('txt_api_error')).toBeInTheDocument();
  });

  it('handCancel', () => {
    const mockAction = jest.fn().mockImplementation(mockImp);
    jest
      .spyOn(schedulePaymentActions, 'setAccountBankStep')
      .mockImplementation(mockAction);

    renderWrapper(stateVerify);

    screen.getByText('txt_cancel').click();

    expect(mockAction).toBeCalledWith({
      storeId,
      step: AddAccountBankStep.INIT_INFO
    });
  });

  it('onHide', () => {
    const mockAction = jest.fn().mockImplementation(mockImp);
    jest
      .spyOn(schedulePaymentActions, 'setAccountBankStep')
      .mockImplementation(mockAction);

    const { baseElement } = renderWrapper(stateVerify);

    queryByClass(baseElement, /icon-close/)!.click();

    expect(mockAction).toBeCalledWith({
      storeId,
      step: AddAccountBankStep.INIT_INFO
    });
  });
});
