import React from 'react';

// hooks
import { AccountDetailProvider } from 'app/hooks';

// test-utils
import { renderMockStore, storeId, accEValue } from 'app/test-utils';

// components
import BankAccount from './BankAccount';
import { screen, fireEvent } from '@testing-library/react';

import { storeIdViewNotFound } from 'app/test-utils/mocks/MockView';

// constants
import { bankAccountInformationSavingView } from '../constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import * as entitlementsHelpers from 'app/entitlements/helpers';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const generateState = (id = storeId, errorFields?: string[]) => {
  const initialState: Partial<RootState> = {
    schedulePayment: {
      [storeId]: {
        addAccount: {
          errorFields,
          errorMessage: 'messageError ',
          loading: false,
          bankInfo: undefined,
          step: 0
        },
        selectedAmount: {
          value: '-1',
          id: `amount-${-1}`
        },
        isBankAcctsLoading: false,
        isLoadingPaymentInfo: false
      }
    }
  };
  return initialState;
};

const renderWrapper = (
  initialState: Partial<RootState>,
  onChange?: (viewName: string) => void,
  id = storeId
) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId: id, accEValue }}>
      <BankAccount ref={{ current: {} }} onChange={onChange} />
    </AccountDetailProvider>,
    { initialState }
  );
};


let spy: jest.SpyInstance;

beforeEach(() => {
  spy = jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
});

afterEach(() => {
  spy.mockReset();
  spy.mockRestore();
});

describe('Render', () => {
  it('Render UI', () => {
    spy = jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(false);
    renderWrapper(generateState());

    expect(
      screen.getByText(I18N_COMMON_TEXT.BANK_INFORMATION)
    ).toBeInTheDocument();
  });

  it('Render UI > error', () => {
    renderWrapper(generateState(storeId, ['lastFieldValidatorKeys']));

    expect(
      screen.getByText(I18N_COMMON_TEXT.BANK_INFORMATION)
    ).toBeInTheDocument();
  });

  it('Render UI > error is not array', () => {
    renderWrapper(generateState(storeId, {} as unknown as string[]));

    expect(
      screen.getByText(I18N_COMMON_TEXT.BANK_INFORMATION)
    ).toBeInTheDocument();
  });

  it('Render UI > onFind not found', () => {
    renderWrapper(
      generateState(storeIdViewNotFound),
      jest.fn(),
      storeIdViewNotFound
    );

    expect(
      screen.getByText(I18N_COMMON_TEXT.BANK_INFORMATION)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('onInit', () => {
    const onChange = jest.fn();

    renderWrapper(generateState(), onChange);

    fireEvent.click(screen.getByLabelText('txt_savings'));

    expect(onChange).toBeCalledWith(bankAccountInformationSavingView);
  });
});
