import React from 'react';
import classNames from 'classnames';
import isFunction from 'lodash.isfunction';
import { AccountTypeTemplateProps } from '../types';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { TypeAccountTemplateID } from '../constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';

interface AddAccountProps {
  onSelect?: (id: string) => void;
  selectedAccount?: string;
}

const AccountTypeTemplate: React.FC<AccountTypeTemplateProps> = ({
  imageSrc,
  title,
  id,
  dataTestId,
  selected,
  onSelect
}) => {
  return (
    <div
      id={id}
      data-testid={genAmtId(dataTestId!, 'account-type', '')}
      className={classNames(
        'd-flex align-items-center card card-blue',
        selected && 'active'
      )}
      onClick={() => {
        isFunction(onSelect) && onSelect(id);
      }}
    >
      <img
        width="60"
        src={imageSrc}
        alt=""
        data-testid={genAmtId(dataTestId!, 'account-type_img', '')}
      />
      <p
        className="fw-500 mt-8"
        data-testid={genAmtId(dataTestId!, 'account-type_title', '')}
      >{title}</p>
    </div>
  );
};

const AccountType: React.FC<AddAccountProps> = ({
  onSelect,
  selectedAccount
}) => {
  const { t } = useTranslation();
  const testId = 'account-details_make-payment-modal_schedule-payment_add-card';
  const handleSelect = (id: string) => {
    isFunction(onSelect) && onSelect(id);
  };

  return (
    <div className="row">
      <div className="col-6">
        <AccountTypeTemplate
          id={TypeAccountTemplateID.BANK_ACCOUNT}
          imageSrc="images/bankAccountIcon.svg"
          title={t(I18N_COMMON_TEXT.BANK_INFORMATION)}
          selected={TypeAccountTemplateID.BANK_ACCOUNT === selectedAccount}
          onSelect={handleSelect}
          dataTestId={testId + TypeAccountTemplateID.BANK_ACCOUNT}
        />
      </div>
      <div className="col-6">
        <AccountTypeTemplate
          id={TypeAccountTemplateID.CARD_INFORMATION}
          imageSrc="images/debitCardIcon.svg"
          title={t('txt_card_information')}
          selected={TypeAccountTemplateID.CARD_INFORMATION === selectedAccount}
          onSelect={handleSelect}
          dataTestId={testId + TypeAccountTemplateID.CARD_INFORMATION}
        />
      </div>
    </div>
  );
};

export default AccountType;
