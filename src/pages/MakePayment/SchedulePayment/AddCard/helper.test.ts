import {luhn_checksum, luhn_caclulate, luhn_validate} from './helper'

describe('test helper luhn_checksum', () => {
    it('it should be return nan if params is not valid', ()=>{
        const params = 'test';
        const result = luhn_checksum(params)
        expect(result).toEqual(NaN)
    })
    it('it should be return code if params is valid', ()=>{
        const params = '9464222222';
        const result = luhn_checksum(params)
        expect(result).toEqual(8)
    })
})

describe('test helper luhn_caclulate', () => {
    it('it should be return 10 - luhn_checksum ', ()=>{
        const params = '9464222222';
        const result = luhn_caclulate(params)
        expect(result).toEqual(1)
    })

    it('it should be return 0', ()=>{
        const params = '000000000';
        const result = luhn_caclulate(params)
        expect(result).toEqual(0)
    })
})

describe('test helper luhn_validate', () => {
    it('it should be return false', ()=>{
        const params = '000000000';
        const result = luhn_validate(params)
        expect(result).toEqual(true)
    })

    it('it should be return 0', ()=>{
        const params = '9464222222';
        const result = luhn_validate(params)
        expect(result).toEqual(false)
    })
})