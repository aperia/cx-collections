import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';

// components
import {
  CheckBox,
  Icon,
  Button,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  ModalBody
} from 'app/_libraries/_dls/components';
import { View } from 'app/_libraries/_dof/core';
import AccountType from './AccountType';
import CardInfo from './CardInfo';
import BankAccount from './BankAccount';

// redux-store
import { batch, useDispatch } from 'react-redux';
import {
  selectBankInfo,
  selectErrorMessage,
  selectLoading,
  selectForm,
  selectStep
} from '../_redux/selectors';
import { schedulePaymentActions as actions } from '../_redux/reducers';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// types
import { PAYMENT_CARD_TYPE, ValidateAddBankAccountInfoArgs } from '../types';

// helper
import classNames from 'classnames';
import isFunction from 'lodash.isfunction';
import isEmpty from 'lodash.isempty';
import { luhn_validate } from './helper';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import {
  AddAccountBankStep,
  bankAccountInformationCheckingView,
  mappingViewConfirm,
  TypeAccountTemplateID
} from '../constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { SCHEDULE_ACTIONS } from 'pages/Memos/constants';
import { formatCommon } from 'app/helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { useApiErrorNotification } from 'pages/ApiErrorNotification/hooks/useApiErrorNotification';
import { CHRONICLE_PAYMENT_KEY } from 'pages/ClientConfiguration/Memos/constants';

interface AddCardProps {
  onClose?: () => void;
}

const AddCard: React.FC<AddCardProps> = ({ onClose }) => {
  const containerRef = useRef<any>(null);

  const {
    storeId,
    accEValue: accountId,
    memberSequenceIdentifier
  } = useAccountDetail();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const [viewName, setViewName] = useState(bankAccountInformationCheckingView);
  const [typeAccount, setTypeAccount] = useState(
    TypeAccountTemplateID.BANK_ACCOUNT
  );
  const [isShow, setIsShow] = useState(false);
  const [confirmInformationCheckbox, setConfirmInformationCheckbox] =
    useState(false);

  const valuesForm: MagicKeyValue = useStoreIdSelector(selectForm(viewName));
  const isFormValid = isEmpty(valuesForm?.syncErrors);

  const isLoading: boolean = useStoreIdSelector(selectLoading);
  const messageError: string = useStoreIdSelector(selectErrorMessage);
  const step = useStoreIdSelector(selectStep);
  const valueConfirmAddAccount: ValidateAddBankAccountInfoArgs =
    useStoreIdSelector(selectBankInfo);

  const ApiErrorDetail = useApiErrorNotification({
    storeId,
    forSection: 'inAddAndEditPaymentMethodModal'
  });

  const isFill = step === AddAccountBankStep.FILL_INFO;
  const isVerify = step === AddAccountBankStep.VERIFY_INFO;
  const testId = 'account-details_make-payment-modal_schedule-payment_add-card';

  useEffect(() => {
    setIsShow(
      step === AddAccountBankStep.FILL_INFO ||
        step === AddAccountBankStep.VERIFY_INFO
    );
  }, [step]);

  useEffect(() => {
    dispatch(
      actions.setErrorBusinessMessage({ message: messageError, storeId })
    );
  }, [dispatch, messageError, storeId]);

  useEffect(() => {
    // reset viewName to default
    if (step === AddAccountBankStep.INIT_INFO) {
      setTypeAccount(TypeAccountTemplateID.BANK_ACCOUNT);
    }
  }, [step]);

  useEffect(() => {
    const args = {
      storeId,
      postData: {
        accountId,
        memberSequenceIdentifier
      }
    };
    dispatch(actions.getDataCommonToAddBankInfo(args));
  }, [accountId, dispatch, memberSequenceIdentifier, storeId]);

  const handleSelectType = (id: string) => {
    setTypeAccount(id);
  };

  const handleChangeView = useCallback(
    (selectedViewName: string) => {
      batch(() => {
        dispatch(actions.resetState({ storeId }));
        // reset API Error when changing step
        dispatch(
          apiErrorNotificationAction.clearApiErrorData({
            storeId,
            forSection: 'inAddAndEditPaymentMethodModal'
          })
        );
      });
      setViewName(selectedViewName);
    },
    [dispatch, storeId]
  );

  const handleVerify = () => {
    const values = containerRef.current?.element?.values;
    const isBankInfo = typeAccount === TypeAccountTemplateID.BANK_ACCOUNT;

    if (isBankInfo) {
      const { type } = containerRef.current;

      let bankAccountType = {};
      if (type.radioChecking) {
        bankAccountType = { code: 'Checking', description: 'txt_checking' };
      } else {
        bankAccountType = { code: 'Savings', description: 'txt_savings' };
      }
      const dataPost = { ...values, storeId, accountId, bankAccountType };

      dispatch(actions.getVerifyCardPayment(dataPost));
      return;
    }

    const valid =
      values?.cardDebitNumber && luhn_validate(values.cardDebitNumber);
    if (valid) {
      setConfirmInformationCheckbox(false);
      const data = {
        loading: false,
        step: AddAccountBankStep.VERIFY_INFO,
        bankInfo: values
      };

      batch(() => {
        dispatch(
          memoActions.postMemo(SCHEDULE_ACTIONS.VERIFIED_CARD, {
            storeId,
            accEValue: accountId,
            chronicleKey: CHRONICLE_PAYMENT_KEY,
            makePayment: {
              cardNumber: values.cardDebitNumber,
              scheduledDate: formatCommon(values.expirationDate).time
                .shortMonthShortYear,
              paymentMedium: PAYMENT_CARD_TYPE.CARD_INFO
            }
          })
        );
        dispatch(actions.setStateAddAccount({ storeId, data }));
      });
    } else {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: 'txt_verify_failed'
        })
      );
    }
  };

  const handleSetStep = (
    selectedStep: number = AddAccountBankStep.FILL_INFO
  ) => {
    dispatch(actions.setAccountBankStep({ storeId, step: selectedStep }));
  };

  const handCloseModal = () => {
    setIsShow(false);
    // uncheck confirm add card
    setConfirmInformationCheckbox(false);
    batch(() => {
      handleSetStep(AddAccountBankStep.INIT_INFO);
      dispatch(actions.resetState({ storeId }));
      isFunction(onClose) && onClose && onClose();
    });
  };

  const handleAddAccount = () => {
    const handleSuccess = () => {
      batch(() => {
        dispatch(actions.getBankAccounts({ storeId, postData: { accountId } }));
        handCloseModal();
      });
    };
    dispatch(
      actions.registerBankAccount({
        ...valueConfirmAddAccount,
        storeId,
        accountId,
        onSuccess: handleSuccess
      })
    );
  };

  const view = useMemo(() => {
    switch (typeAccount) {
      case TypeAccountTemplateID.CARD_INFORMATION:
        return <CardInfo ref={containerRef} onInit={handleChangeView} />;
      case TypeAccountTemplateID.BANK_ACCOUNT:
      default:
        return <BankAccount ref={containerRef} onChange={handleChangeView} />;
    }
  }, [handleChangeView, typeAccount]);

  const renderHeader = () => {
    return (
      <ModalHeader
        border
        closeButton
        onHide={handCloseModal}
        dataTestId={genAmtId(testId, 'header', '')}
      >
        <ModalTitle dataTestId={genAmtId(testId, 'header-title', '')}>
          {t(I18N_COMMON_TEXT.ADD_PAYMENT_METHOD)}
        </ModalTitle>
      </ModalHeader>
    );
  };

  const renderBody = () => {
    const viewNameVerify =
      mappingViewConfirm[viewName as keyof typeof mappingViewConfirm];

    return (
      <ModalBody dataTestId={genAmtId(testId, 'body', '')}>
        <ApiErrorDetail
          className="mb-24"
          dataTestId={genAmtId(testId, 'body-api-error', '')}
        />
        <div className={classNames({ 'd-none': !isFill })}>
          <AccountType
            onSelect={handleSelectType}
            selectedAccount={typeAccount}
          />
          {view}
        </div>
        {!!containerRef.current?.element?.values && (
          <div className={classNames({ 'd-none': !isVerify })}>
            <div
              className="text-center"
              data-testid={genAmtId(testId, 'body_infomation-verified', '')}
            >
              <Icon color="green" name="success" className="fs-60" />
              <p className="color-green mt-8">
                {t('txt_method_information_verified')}
              </p>
            </div>
            <div className="border bg-light-l20 br-light-l04 br-radius-8 px-16 pb-16 mt-24">
              <View
                id={`${storeId}-${viewNameVerify}`}
                formKey={`${storeId}-${viewNameVerify}`}
                descriptor={viewNameVerify}
                value={valueConfirmAddAccount}
              />
            </div>
            <CheckBox
              className="mt-16"
              dataTestId={genAmtId(
                testId,
                'body_confirmInformationCheckbox',
                ''
              )}
            >
              <CheckBox.Input
                id="confirmInformationCheckbox"
                checked={confirmInformationCheckbox}
                onChange={event =>
                  setConfirmInformationCheckbox(event.target.checked)
                }
              />
              <CheckBox.Label htmlFor="confirmInformationCheckbox">
                {t('txt_confirm_payment_method_information')}
              </CheckBox.Label>
            </CheckBox>
          </div>
        )}
      </ModalBody>
    );
  };

  const renderFooter = () => {
    let props: MagicKeyValue = {};

    switch (step) {
      case AddAccountBankStep.VERIFY_INFO:
        props = {
          okButtonText: 'txt_add',
          cancelButtonText: 'txt_cancel',
          onCancel: handCloseModal,
          onOk: handleAddAccount,
          disabledOk: !confirmInformationCheckbox
        };
        break;
      case AddAccountBankStep.FILL_INFO:
      default:
        props = {
          okButtonText: 'txt_verify',
          cancelButtonText: 'txt_cancel',
          disabledOk: !isFormValid,
          onOk: handleVerify,
          onCancel: handCloseModal
        };
        break;
    }

    const handleBackToEntryForm = () => {
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inAddAndEditPaymentMethodModal'
        })
      );
      setConfirmInformationCheckbox(false);
      handleSetStep();
    };

    const {
      okButtonText,
      cancelButtonText,
      onCancel,
      onOk,
      disabledOk,
      ...rest
    } = props;

    return (
      <ModalFooter {...rest} dataTestId={genAmtId(testId, 'footer', '')}>
        {isVerify && (
          <Button
            variant="outline-primary"
            className="mr-auto ml-n8"
            onClick={handleBackToEntryForm}
            dataTestId={genAmtId(testId, 'footer_back-to-entry-form-btn', '')}
          >
            {t(I18N_COMMON_TEXT.BACK_TO_ENTRY_FORM)}
          </Button>
        )}
        <Button
          variant="secondary"
          onClick={onCancel}
          dataTestId={genAmtId(testId, 'footer_cancel-btn', '')}
        >
          {t(cancelButtonText)}
        </Button>
        <Button
          onClick={onOk}
          disabled={disabledOk}
          dataTestId={genAmtId(testId, 'footer_ok-btn', '')}
        >
          {t(okButtonText)}
        </Button>
      </ModalFooter>
    );
  };

  //  clear API ERROR when close modal
  useEffect(() => {
    if (!isShow) return;
    return () => {
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inAddAndEditPaymentMethodModal'
        })
      );
    };
  }, [dispatch, isShow, storeId]);

  if (!isShow) return null;

  return (
    <Modal show sm loading={isLoading} dataTestId={testId}>
      {renderHeader()}
      {renderBody()}
      {renderFooter()}
    </Modal>
  );
};

export default AddCard;
