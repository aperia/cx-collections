import React, {
  forwardRef,
  RefForwardingComponent,
  useEffect,
  useImperativeHandle,
  useRef
} from 'react';

// components
import { InlineMessage } from 'app/_libraries/_dls/components';
import { View } from 'app/_libraries/_dof/core';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// constants
import { addCardInfoView } from '../constants';

// redux-store
import { selectErrorFields, selectErrorMessage } from '../_redux/selectors';

// utils
import isArray from 'lodash.isarray';
import isFunction from 'lodash.isfunction';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

interface DebitCardProps {
  onInit?: (viewName: string) => void;
}

const CardInfo: RefForwardingComponent<MagicKeyValue, DebitCardProps> = (
  { onInit },
  ref
) => {
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const containerRef = useRef<any>(null);
  useImperativeHandle(ref, () => ({
    element: containerRef.current!
  }));

  const messageError: string = useStoreIdSelector(selectErrorMessage);
  const errorFields: string = useStoreIdSelector(selectErrorFields);

  const testId =
    'account-details_make-payment-modal_schedule-payment_add-card_card-info';

  useEffect(() => {
    isFunction(onInit) && onInit(addCardInfoView);
  }, [onInit, storeId]);

  useEffect(() => {
    const { current } = containerRef;
    if (!isArray(errorFields) || !current) return;

    const listFields = current?.ref?.current
      ?.lastFieldValidatorKeys as Array<string>;

    listFields.forEach(field => {
      const fieldForm = current?.ref?.current?.props?.onFind(field);
      if (!fieldForm) return;

      const error = errorFields.find(item => item === field) ? true : false;

      fieldForm.props?.props?.setOthers((prev: MagicKeyValue) => {
        fieldForm.props?.props?.setOthers({
          ...prev,
          options: { ...prev.options, isError: error }
        });
      });
    });
  }, [errorFields]);

  return (
    <div className="mt-24">
      <h5 data-testid={genAmtId(testId, 'title', '')}>
        {t('txt_card_information')}
      </h5>
      {messageError && (
        <InlineMessage
          className="mt-16 mb-0"
          variant="danger"
          dataTestId={genAmtId(testId, 'error_message', '')}
        >
          {messageError}
        </InlineMessage>
      )}
      <View
        id={`${storeId}-${addCardInfoView}`}
        formKey={`${storeId}-${addCardInfoView}`}
        descriptor={addCardInfoView}
        ref={containerRef}
      />
    </div>
  );
};

export default forwardRef<MagicKeyValue, DebitCardProps>(CardInfo);
