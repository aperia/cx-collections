import React from 'react';

// hooks
import { AccountDetailProvider } from 'app/hooks';

// test-utils
import { renderMockStore, storeId, accEValue } from 'app/test-utils';

// components
import CardInfo from './CardInfo';
import { screen } from '@testing-library/react';

import { storeIdViewNotFound } from 'app/test-utils/mocks/MockView';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const generateState = (id = storeId, errorFields?: string[]) => {
  const initialState: Partial<RootState> = {
    schedulePayment: {
      [id]: {
        addAccount: {
          errorFields,
          errorMessage: 'messageError ',
          loading: false,
          bankInfo: undefined,
          step: 0
        },
        selectedAmount: {
          value: '-1',
          id: `amount-${-1}`
        },
        isBankAcctsLoading: false,
        isLoadingPaymentInfo: false
      }
    }
  };

  return initialState;
};

const renderWrapper = (
  initialState: Partial<RootState>,
  onInit?: (viewName: string) => void,
  id = storeId
) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId: id, accEValue }}>
      <CardInfo ref={{ current: {} }} onInit={onInit} />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper(generateState());

    expect(screen.getByText('txt_card_information')).toBeInTheDocument();
  });

  it('Render UI > error', () => {
    renderWrapper(generateState(storeId, ['lastFieldValidatorKeys']));

    expect(screen.getByText('txt_card_information')).toBeInTheDocument();
  });

  it('Render UI > error is not array', () => {
    renderWrapper(generateState(storeId, {} as unknown as string[]));

    expect(screen.getByText('txt_card_information')).toBeInTheDocument();
  });

  it('Render UI > onFind not found', () => {
    renderWrapper(
      generateState(storeIdViewNotFound),
      jest.fn(),
      storeIdViewNotFound
    );

    expect(screen.getByText('txt_card_information')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('onInit', () => {
    const onInit = jest.fn();

    renderWrapper(generateState(), onInit);

    expect(onInit).toBeCalledWith('addCardInfoView');
  });
});
