import React, {
  forwardRef,
  RefForwardingComponent,
  useEffect,
  useImperativeHandle,
  useMemo,
  useRef,
  useState
} from 'react';
import { InlineMessage, Radio } from 'app/_libraries/_dls/components';
import { View } from 'app/_libraries/_dof/core';
import isArray from 'lodash.isarray';
import isFunction from 'lodash.isfunction';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import {
  bankAccountInformationCheckingView,
  bankAccountInformationSavingView
} from '../constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// redux-store
import { selectErrorFields, selectErrorMessage } from '../_redux/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';

interface BankAccountProps {
  onChange?: (viewName: string) => void;
}

const BankAccount: RefForwardingComponent<MagicKeyValue, BankAccountProps> = (
  { onChange },
  ref
) => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const containerRef = useRef<any>(null);

  const [viewName, setViewName] = useState(bankAccountInformationCheckingView);
  const [checked, setChecked] = useState({
    radioChecking: true,
    radioSaving: false
  });

  const messageError: string = useStoreIdSelector(selectErrorMessage);
  const errorFields: string = useStoreIdSelector(selectErrorFields);

  const testId = 'account-details_make-payment-modal_schedule-payment_add-card_bank-account';

  useEffect(() => {
    isFunction(onChange) && onChange(viewName);
  }, [onChange, viewName]);

  useEffect(() => {
    const { current } = containerRef;
    if (!isArray(errorFields) || !current) return;

    // reset error
    const listFields = current?.ref?.current
      ?.lastFieldValidatorKeys as Array<string>;

    listFields.forEach(field => {
      const fieldForm = current?.ref?.current?.props?.onFind(field);
      if (!fieldForm) return;

      const error = errorFields.find(item => item === field) ? true : false;

      fieldForm.props?.props?.setOthers((prev: MagicKeyValue) => {
        fieldForm.props?.props?.setOthers({
          ...prev,
          options: { ...prev.options, isError: error }
        });
      });
    });
  }, [errorFields]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const target = event.target.id as 'radioChecking' | 'radioSaving';

    setChecked(state => ({
      radioChecking: false,
      radioSaving: false,
      [target]: !state[target]
    }));
  };

  const inputAccountView = useMemo(() => {
    const view = checked.radioChecking ? (
      <View
        id={`${storeId}-${bankAccountInformationCheckingView}`}
        formKey={`${storeId}-${bankAccountInformationCheckingView}`}
        descriptor={bankAccountInformationCheckingView}
        ref={containerRef}
      />
    ) : (
      <View
        id={`${storeId}-${bankAccountInformationSavingView}`}
        formKey={`${storeId}-${bankAccountInformationSavingView}`}
        descriptor={bankAccountInformationSavingView}
        ref={containerRef}
      />
    );
    setViewName(
      checked.radioChecking
        ? bankAccountInformationCheckingView
        : bankAccountInformationSavingView
    );
    return view;
  }, [checked.radioChecking, storeId]);

  useImperativeHandle(ref, () => ({
    element: containerRef.current!,
    type: checked
  }));

  return (
    <div className="mt-24">
      <h5 data-testid={genAmtId(testId, 'title', '')}>
        {t(I18N_COMMON_TEXT.BANK_INFORMATION)}
      </h5>
      <div className="d-flex mt-16">
        <span
          className="color-grey fw-500"
          data-testid={genAmtId(testId, 'type-label', '')}
        >{t('txt_type')}:</span>
        <div className="d-flex ml-24">
          <Radio dataTestId={genAmtId(testId, 'checking-radio', '')}>
            <Radio.Input
              id="radioChecking"
              checked={checked.radioChecking}
              onChange={handleChange}
            />
            <Radio.Label>{t('txt_checking')}</Radio.Label>
          </Radio>
          <Radio className="ml-22" dataTestId={genAmtId(testId, 'savings-radio', '')}>
            <Radio.Input
              id="radioSaving"
              checked={checked.radioSaving}
              onChange={handleChange}
            />
            <Radio.Label>{t('txt_savings')}</Radio.Label>
          </Radio>
        </div>
      </div>
      {messageError && (
        <InlineMessage
          className="fw-400 mt-16 mb-0"
          variant="danger"
          dataTestId={genAmtId(testId, 'error-message', '')}
        >
          {messageError}
        </InlineMessage>
      )}
      <div>{inputAccountView}</div>
    </div>
  );
};
export default forwardRef<MagicKeyValue, BankAccountProps>(BankAccount);
