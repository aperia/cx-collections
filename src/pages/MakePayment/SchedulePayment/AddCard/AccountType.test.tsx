import React from 'react';
import { render } from '@testing-library/react';

import AccountType from './AccountType';
import { TypeAccountTemplateID } from '../constants';
import { queryById } from 'app/test-utils';

jest.mock('', () => {});

const renderWrapper = (handleSelect?: jest.Mock) => {
  return render(
    <AccountType
      selectedAccount={TypeAccountTemplateID.BANK_ACCOUNT}
      onSelect={handleSelect}
    />
  );
};

describe('Actions', () => {
  it('handleSelect', () => {
    const handleSelect = jest.fn();

    const baseElement = renderWrapper(handleSelect).baseElement as HTMLElement;

    queryById(baseElement, TypeAccountTemplateID.BANK_ACCOUNT)!.click();

    expect(handleSelect).toBeCalledWith(TypeAccountTemplateID.BANK_ACCOUNT);
  });
});
