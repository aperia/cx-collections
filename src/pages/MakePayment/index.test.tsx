import React from 'react';

// components
import MakePayment from './index';

// helpers
import { AccountDetailProvider } from 'app/hooks';
import {
  renderMockStore,
  storeId,
  accEValue,
  mockActionCreator
} from 'app/test-utils';
import { fireEvent, screen } from '@testing-library/react';
import { PaymentTypeProps } from './PaymentType';
import { ACHPaymentInitialState } from 'pages/MakePayment/ACHPayment/types';
import { ID_OTHER_AMOUNT, PAYMENT_TYPE } from './constants';
import { TYPE_CARD, OTHER_CODE } from './ACHPayment/constants';

// Redux
import { schedulePaymentActions } from './SchedulePayment/_redux/reducers';
import { demandACHPaymentAction } from 'pages/MakePayment/ACHPayment/_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { actions as paymentActions } from 'pages/Payment/__redux/reducers';
import { actions as accDetailTabsActions } from 'pages/__commons/AccountDetailTabs/_redux';
import { actions as paymentsListAction } from 'pages/Payment/PaymentList/_redux/reducers';
import {
  MAKE_PAYMENT_FROM,
  PAYMENT_CARD_TYPE,
  SubmitMakePaymentRequest
} from './SchedulePayment/types';
import { AxiosResponse } from 'axios';
import { TAB_IDS } from 'pages/Payment/constants';
import * as entitlementsHelpers from 'app/entitlements/helpers';
import { KEYS } from 'pages/__commons/AccountDetailTabs';
import { PERMISSIONS } from 'app/entitlements/constants';
import * as ScheduleSelector from './SchedulePayment/_redux/selectors';

const mockDate = new Date('10/10/2020');

const schedulePaymentData = {
  [storeId]: {
    openModal: true,
    checkedList: ['100'],
    from: MAKE_PAYMENT_FROM.SETTLEMENT,
    selectedDate: {
      value: new Date()
    },
    selectedAmount: {
      id: '',
      value: '100'
    }
  }
};

const demandACHPaymentData: ACHPaymentInitialState = {
  [storeId]: {
    checkedList: ['row_1'],
    data: {
      bankAccounts: [
        {
          accountNumber: { eValue: '123456' },
          accountType: { code: 'row_1' }
        }
      ]
    },
    dataInfo: { data: { fee: 1 } },
    amount: { id: OTHER_CODE, value: '100.0' },
    transactionCode: '123',
    achInformation: {
      typeChecked: TYPE_CARD.CHECKING,
      gridData: [
        {
          code: TYPE_CARD.CHECKING,
          typeCard: TYPE_CARD.CHECKING,
          accountNumber: '123'
        }
      ]
    }
  }
};

const accDetailTabsActionsSpy = mockActionCreator(accDetailTabsActions);
const actionsToastSpy = mockActionCreator(actionsToast);
const paymentActionSpy = mockActionCreator(paymentActions);
const paymentsListActionSpy = mockActionCreator(paymentsListAction);

jest.mock('./SchedulePayment', () => () => <div>SchedulePayment</div>);
jest.mock('./ACHPayment', () => () => <div>ACHPayment</div>);

jest.mock(
  './PaymentType',
  () =>
    ({ onChangePaymentType }: PaymentTypeProps) => {
      return (
        <input
          data-testid="PaymentType_onChangePaymentType"
          onChange={e =>
            onChangePaymentType && onChangePaymentType(e.target.value)
          }
        />
      );
    }
);

const initialState: Partial<RootState> = {
  schedulePayment: {
    [storeId]: {
      openModal: true,
      checkedList: [PAYMENT_CARD_TYPE.CHECKING],
      bankAccts: [
        {
          accType: { code: PAYMENT_CARD_TYPE.CHECKING }
        }
      ],
      selectedAmount: { id: ID_OTHER_AMOUNT },
      selectedDate: { value: new Date() },
      isInvalidSchedule: true,
      cvv2: '12'
    }
  },
  demandACHPayment: {
    [storeId]: {
      checkedList: ['row_1'],
      data: {
        bankAccounts: [
          {
            accountNumber: { eValue: '123456' },
            accountType: { code: 'row_1' }
          }
        ]
      },
      dataInfo: { data: { fee: 1 } },
      amount: { id: ID_OTHER_AMOUNT, value: '100' },
      transactionType: ''
    }
  }
};

const responseDefault: AxiosResponse<any> = {
  data: null,
  status: 200,
  statusText: '',
  headers: {},
  config: {}
};

const mockImp = () => ({ type: 'type' });
let spy: jest.SpyInstance;
let spy2: jest.SpyInstance;
let spy3: jest.SpyInstance;
let spy4: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();

  spy2?.mockReset();
  spy2?.mockRestore();

  spy3?.mockReset();
  spy3?.mockRestore();

  spy4?.mockReset();
  spy4?.mockRestore();
});

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <MakePayment />
    </AccountDetailProvider>,
    { initialState }
  );
};

beforeEach(() => {
  jest
    .spyOn(global, 'Date')
    .mockImplementation(() => mockDate as unknown as string);
  global.Date.now = jest.fn(() => mockDate.getTime());

  jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
});

describe('render', () => {
  it(`Render UI > don't open modal`, () => {
    renderWrapper({});
    expect(screen.queryByRole('dialog')).toBeNull();
  });

  it('Render UI > no methos', () => {
    renderWrapper(initialState);
    expect(screen.queryByText('SchedulePayment')).toEqual(null);
    expect(screen.queryByText('ACHPayment')).toEqual(null);
  });

  it('Render UI > Schedule Bank', () => {
    const schedulePayment = {
      [storeId]: {
        openModal: true,
        checkedList: [PAYMENT_CARD_TYPE.CHECKING],
        bankAccts: [
          {
            accType: { code: PAYMENT_CARD_TYPE.CHECKING }
          }
        ],
        isInvalidSchedule: true,
        selectedAmount: { id: ID_OTHER_AMOUNT, value: 'test' },
        selectedDate: { value: new Date() }
      }
    };
    renderWrapper({ ...initialState, schedulePayment });

    fireEvent.change(screen.getByTestId('PaymentType_onChangePaymentType'), {
      target: { value: PAYMENT_TYPE.SCHEDULE }
    });
    expect(screen.getByText('SchedulePayment')).toBeInTheDocument();
  });

  it('Render UI > Schedule Card invalid', () => {
    const schedulePayment = {
      [storeId]: {
        openModal: true,
        checkedList: [PAYMENT_CARD_TYPE.CARD_INFO],
        bankAccts: [
          {
            accType: { code: PAYMENT_CARD_TYPE.CARD_INFO }
          }
        ],
        isInvalidSchedule: true,

        selectedAmount: { id: ID_OTHER_AMOUNT, value: 'test' },
        selectedDate: { value: new Date() }
      }
    };

    jest.spyOn(ScheduleSelector, 'getDataPost').mockReturnValue({
      registrationID: '123',
      crdNbrEValue: '100',
      accountNumberMaskedValue: '',
      paymentMedium: PAYMENT_CARD_TYPE.CARD_INFO,
      amount: { id: ID_OTHER_AMOUNT, value: 'test' },
      schedulePaymentDate: new Date(),
      method: 'Card'
    });

    renderWrapper({ ...initialState, schedulePayment });

    fireEvent.change(screen.getByTestId('PaymentType_onChangePaymentType'), {
      target: { value: PAYMENT_TYPE.SCHEDULE }
    });
    expect(screen.getByText('SchedulePayment')).toBeInTheDocument();

    jest.restoreAllMocks();
  });

  it('Render UI > ACHPayment', () => {
    renderWrapper(initialState);
    fireEvent.change(screen.getByTestId('PaymentType_onChangePaymentType'), {
      target: { value: PAYMENT_TYPE.DEMAND_ACH }
    });
    expect(screen.getByText('ACHPayment')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  describe('handleChangePaymentType', () => {
    it('Schedule', () => {
      const mockAction = jest.fn().mockImplementation(mockImp);
      spy = jest
        .spyOn(demandACHPaymentAction, 'removeStore')
        .mockImplementation(mockAction);

      renderWrapper(initialState);

      fireEvent.change(screen.getByTestId('PaymentType_onChangePaymentType'), {
        target: { value: PAYMENT_TYPE.SCHEDULE }
      });

      expect(mockAction).toBeCalledWith({ storeId });
    });

    it('Schedule without common check', () => {
      const schedulePayment = {
        [storeId]: {
          openModal: true,
          checkedList: [PAYMENT_CARD_TYPE.CHECKING],
          bankAccts: [
            {
              accType: { code: PAYMENT_CARD_TYPE.CHECKING }
            }
          ],
          isInvalidSchedule: true,
          selectedAmount: { id: ID_OTHER_AMOUNT, value: 'test' },
          selectedDate: { value: new Date() }
        }
      };
      // mock
      const mockAction = jest.fn().mockImplementation(mockImp);
      spy = jest
        .spyOn(schedulePaymentActions, 'clearDataSelected')
        .mockImplementation(mockAction);
      renderWrapper({ ...initialState, schedulePayment });
      fireEvent.change(screen.getByTestId('PaymentType_onChangePaymentType'), {
        target: { value: PAYMENT_TYPE.DEMAND_ACH }
      });

      expect(mockAction).toBeCalledWith({ storeId });
    });

    it('ACHPayment', () => {
      // mock
      const mockAction = jest.fn().mockImplementation(mockImp);
      spy = jest
        .spyOn(schedulePaymentActions, 'clearDataSelected')
        .mockImplementation(mockAction);

      // render
      renderWrapper(initialState);

      // simulate
      fireEvent.change(screen.getByTestId('PaymentType_onChangePaymentType'), {
        target: { value: PAYMENT_TYPE.DEMAND_ACH }
      });

      // assertion
      expect(mockAction).toBeCalledWith({ storeId });
    });

    it('ACHPayment without disable button', () => {
      const demandACHPayment = {
        [storeId]: {
          checkedList: ['row_1'],
          data: {
            bankAccounts: [
              {
                accountNumber: { eValue: '123456' },
                accountType: { code: 'row_1' }
              }
            ]
          },
          dataInfo: { data: { fee: 1 } },
          amount: { id: OTHER_CODE, value: '100.0' },
          transactionCode: '123',
          achInformation: {
            gridData: [
              {
                code: TYPE_CARD.CHECKING,
                typeCard: TYPE_CARD.CHECKING,
                accountNumber: '123'
              }
            ]
          }
        }
      };
      const mockAction = jest.fn().mockImplementation(mockImp);
      spy = jest
        .spyOn(schedulePaymentActions, 'clearDataSelected')
        .mockImplementation(mockAction);

      // render
      renderWrapper({ ...initialState, demandACHPayment });

      // simulate
      fireEvent.change(screen.getByTestId('PaymentType_onChangePaymentType'), {
        target: { value: PAYMENT_TYPE.DEMAND_ACH }
      });

      // assertion
      expect(mockAction).toBeCalledWith({ storeId });
    });
  });

  describe('handleConfirm', () => {
    it('Schedule', () => {
      // mock
      const mockAction = jest.fn().mockImplementation(mockImp);
      spy = jest
        .spyOn(schedulePaymentActions, 'submitMakePayment')
        .mockImplementation(mockAction);

      renderWrapper({ schedulePayment: schedulePaymentData });

      fireEvent.change(screen.getByTestId('PaymentType_onChangePaymentType'), {
        target: { value: PAYMENT_TYPE.SCHEDULE }
      });

      const btnMakePayment = screen.getByRole('button', {
        name: /txt_make_payment/
      });

      fireEvent.click(btnMakePayment);

      const btnConfirm = screen.getByRole('button', {
        name: /txt_confirm/
      });

      fireEvent.click(btnConfirm);

      const expectedData = {
        transactionType: undefined,
        crdNbrEValue: '100',
        accountNumberMaskedValue: '',
        amount: {
          id: '',
          value: '100'
        },
        method: 'Bank',
        schedulePaymentDate: schedulePaymentData[storeId].selectedDate.value
      };

      expect(mockAction).toBeCalledWith(
        expect.objectContaining({
          accountId: accEValue,
          storeId,
          data: expectedData
        })
      );
    });

    it('ACHPayment', () => {
      const mockAction = jest.fn().mockImplementation(mockImp);
      spy = jest
        .spyOn(demandACHPaymentAction, 'submitMakePayment')
        .mockImplementation(mockAction);

      // render
      renderWrapper({
        demandACHPayment: demandACHPaymentData,
        schedulePayment: { [storeId]: { openModal: true } }
      });

      fireEvent.change(screen.getByTestId('PaymentType_onChangePaymentType'), {
        target: { value: PAYMENT_TYPE.DEMAND_ACH }
      });

      const btnMakePayment = screen.getByRole('button', {
        name: /txt_make_payment/
      });

      fireEvent.click(btnMakePayment);

      const btnConfirm = screen.getByRole('button', {
        name: /txt_confirm/
      });

      fireEvent.click(btnConfirm);

      const expectedData = {
        amount: {
          id: OTHER_CODE,
          value: '100.0'
        },
        method: 'Bank',
        transactionType: '123',
        schedulePaymentDate: mockDate,
        accountNumberMaskedValue: '123'
      };

      expect(mockAction).toBeCalledWith(
        expect.objectContaining({
          accountId: accEValue,
          storeId,
          data: expectedData
        })
      );
    });
  });
});
describe('onSuccess', () => {
  it('Navigate to Tabs Payments', () => {
    // mock
    const mockSubmitMakePayment = jest
      .fn()
      .mockImplementation((props: SubmitMakePaymentRequest) => {
        const { onSuccess = () => undefined } = props;
        onSuccess(responseDefault);

        return mockImp();
      });
    spy = jest
      .spyOn(schedulePaymentActions, 'submitMakePayment')
      .mockImplementation(mockSubmitMakePayment);

    const mockSetActiveTab = jest.fn().mockImplementation(mockImp);
    spy2 = jest
      .spyOn(accDetailTabsActions, 'setActiveTab')
      .mockImplementation(mockSetActiveTab);

    const mockClearAndReset = jest.fn().mockImplementation(mockImp);
    spy3 = jest
      .spyOn(paymentsListAction, 'clearAndReset')
      .mockImplementation(mockClearAndReset);

    const mockAddToast = jest.fn().mockImplementation(mockImp);

    spy4 = jest
      .spyOn(actionsToast, 'addToast')
      .mockImplementation(mockAddToast);

    // render
    renderWrapper({
      schedulePayment: schedulePaymentData
    });

    // simulate
    fireEvent.change(screen.getByTestId('PaymentType_onChangePaymentType'), {
      target: { value: PAYMENT_TYPE.SCHEDULE }
    });

    const btnMakePayment = screen.getByRole('button', {
      name: /txt_make_payment/
    });
    fireEvent.click(btnMakePayment);
    const btnConfirm = screen.getByRole('button', {
      name: /txt_confirm/
    });

    fireEvent.click(btnConfirm);

    // assertion
    expect(mockSetActiveTab).toBeCalledWith({
      storeId,
      activeKey: KEYS.payments
    });
    expect(mockAddToast).toBeCalledWith(
      expect.objectContaining({
        show: true,
        type: 'success'
      })
    );
  });

  it('should not navigate to Tabs Payments when do not has view payment list permission', () => {
    jest
      .spyOn(entitlementsHelpers, 'checkPermission')
      .mockImplementation(permission => {
        return PERMISSIONS.PAYMENT_LIST_VIEW !== permission;
      });
    // mock
    const mockSubmitMakePayment = jest
      .fn()
      .mockImplementation((props: SubmitMakePaymentRequest) => {
        const { onSuccess = () => undefined } = props;
        onSuccess(responseDefault);

        return mockImp();
      });
    spy = jest
      .spyOn(schedulePaymentActions, 'submitMakePayment')
      .mockImplementation(mockSubmitMakePayment);

    const mockSetActiveTab = jest.fn().mockImplementation(mockImp);
    spy2 = jest
      .spyOn(accDetailTabsActions, 'setActiveTab')
      .mockImplementation(mockSetActiveTab);

    const mockClearAndReset = jest.fn().mockImplementation(mockImp);
    spy3 = jest
      .spyOn(paymentsListAction, 'clearAndReset')
      .mockImplementation(mockClearAndReset);

    const mockAddToast = jest.fn().mockImplementation(mockImp);

    spy4 = jest
      .spyOn(actionsToast, 'addToast')
      .mockImplementation(mockAddToast);

    // render
    renderWrapper({
      schedulePayment: schedulePaymentData
    });

    // simulate
    fireEvent.change(screen.getByTestId('PaymentType_onChangePaymentType'), {
      target: { value: PAYMENT_TYPE.SCHEDULE }
    });

    const btnMakePayment = screen.getByRole('button', {
      name: /txt_make_payment/
    });
    fireEvent.click(btnMakePayment);
    const btnConfirm = screen.getByRole('button', {
      name: /txt_confirm/
    });

    fireEvent.click(btnConfirm);

    // assertion
    expect(mockSetActiveTab).not.toBeCalledWith({
      storeId,
      activeKey: KEYS.payments
    });
    expect(mockAddToast).toBeCalledWith(
      expect.objectContaining({
        show: true,
        type: 'success'
      })
    );
  });

  it('Active tab Payment list in Payment with different key', () => {
    const setActiveTabSpy = accDetailTabsActionsSpy('setActiveTab');
    const mockSubmitMakePayment = jest
      .fn()
      .mockImplementation((props: SubmitMakePaymentRequest) => {
        const { onSuccess = () => undefined } = props;
        onSuccess(responseDefault);

        return mockImp();
      });
    jest
      .spyOn(schedulePaymentActions, 'submitMakePayment')
      .mockImplementation(mockSubmitMakePayment);

    renderWrapper({
      schedulePayment: schedulePaymentData
    });

    fireEvent.change(screen.getByTestId('PaymentType_onChangePaymentType'), {
      target: { value: PAYMENT_TYPE.SCHEDULE }
    });
    const btnMakePayment = screen.getByRole('button', {
      name: /txt_make_payment/
    });
    fireEvent.click(btnMakePayment);

    const btnConfirm = screen.getByRole('button', {
      name: /txt_confirm/
    });

    fireEvent.click(btnConfirm);
    // assertion
    expect(setActiveTabSpy).toBeCalledWith({
      storeId,
      activeKey: 'payments'
    });
  });

  it('Active tab Payment list in Payment with same key', () => {
    const mockSubmitMakePayment = jest
      .fn()
      .mockImplementation((props: SubmitMakePaymentRequest) => {
        const { onSuccess = () => undefined } = props;
        onSuccess(responseDefault);
        return mockImp();
      });
    spy = jest
      .spyOn(schedulePaymentActions, 'submitMakePayment')
      .mockImplementation(mockSubmitMakePayment);
    const setActiveKeySpy = paymentActionSpy('setActiveKey');
    const getPaymentsSpy = paymentsListActionSpy('getPayments');
    const clearAndResetSpy = paymentsListActionSpy('clearAndReset');
    renderWrapper({
      schedulePayment: schedulePaymentData,
      accountDetailTabs: {
        [storeId]: {
          activeKey: 'payments'
        }
      }
    });

    fireEvent.change(screen.getByTestId('PaymentType_onChangePaymentType'), {
      target: { value: PAYMENT_TYPE.SCHEDULE }
    });
    const btnMakePayment = screen.getByRole('button', {
      name: /txt_make_payment/
    });
    fireEvent.click(btnMakePayment);

    const btnConfirm = screen.getByRole('button', {
      name: /txt_confirm/
    });

    fireEvent.click(btnConfirm);
    expect(setActiveKeySpy).toBeCalledWith({
      storeId,
      activeKey: TAB_IDS.paymentList
    });
    expect(getPaymentsSpy).toBeCalledWith({
      storeId,
      postData: { accountId: accEValue }
    });
    expect(clearAndResetSpy).toBeCalledWith({
      storeId
    });
  });

  it('With ACH', async () => {
    const addToastSpy = actionsToastSpy('addToast');
    const mockSubmitMakePayment = jest
      .fn()
      .mockImplementation((props: SubmitMakePaymentRequest) => {
        const { onSuccess = () => undefined } = props;
        onSuccess(responseDefault);
        return mockImp();
      });
    jest
      .spyOn(demandACHPaymentAction, 'submitMakePayment')
      .mockImplementation(mockSubmitMakePayment);

    renderWrapper({
      schedulePayment: {
        [storeId]: {
          acceptedBalanceAmount: '100',
          openModal: true,
          from: MAKE_PAYMENT_FROM.SETTLEMENT
        }
      },
      demandACHPayment: demandACHPaymentData
    });

    fireEvent.change(screen.getByTestId('PaymentType_onChangePaymentType'), {
      target: { value: PAYMENT_TYPE.DEMAND_ACH }
    });
    const btnMakePayment = screen.getByRole('button', {
      name: /txt_make_payment/
    });
    fireEvent.click(btnMakePayment);

    const btnConfirm = screen.getByRole('button', {
      name: /txt_confirm/
    });
    fireEvent.click(btnConfirm);
    expect(addToastSpy).toBeCalledWith({
      show: true,
      type: 'success',
      message: 'txt_on_demand_ach_payment_scheduled'
    });
  });
});

describe('onFailure', () => {
  it('onFailure', () => {
    // mock
    const addToastSpy = actionsToastSpy('addToast');
    const mockSubmitMakePayment = jest
      .fn()
      .mockImplementation((props: SubmitMakePaymentRequest) => {
        const { onFailure = () => undefined } = props;
        onFailure(responseDefault);
        return mockImp();
      });
    jest
      .spyOn(schedulePaymentActions, 'submitMakePayment')
      .mockImplementation(mockSubmitMakePayment);
    renderWrapper({
      schedulePayment: schedulePaymentData
    });

    fireEvent.change(screen.getByTestId('PaymentType_onChangePaymentType'), {
      target: { value: PAYMENT_TYPE.SCHEDULE }
    });
    const btnMakePayment = screen.getByRole('button', {
      name: /txt_make_payment/
    });
    fireEvent.click(btnMakePayment);

    const btnConfirm = screen.getByRole('button', {
      name: /txt_confirm/
    });
    fireEvent.click(btnConfirm);
    expect(addToastSpy).toBeCalledWith({
      show: true,
      type: 'error',
      message: 'txt_make_payment_failed'
    });
  });
});
