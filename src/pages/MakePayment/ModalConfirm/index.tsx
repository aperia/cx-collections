import React, { useEffect } from 'react';

// components
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

// helpers
import { formatCommon, formatTimeDefault } from 'app/helpers';
import { PAYMENT_TYPE } from '../constants';
import { PaymentAmountView } from '../SchedulePayment/types';

import { actions } from './_redux/reducers';
import { useDispatch } from 'react-redux';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { selectConvenienceFee } from './_redux/selectors';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { FormatTime } from 'app/constants/enums';

export interface DataType {
  amount: PaymentAmountView;
  accountNumber: string;
  date: Date | string;
  paymentMedium?: string;
}
export interface ConfirmModalProps {
  handleCloseModal: () => void;
  handleMakePayment: () => void;
  openConfirm: boolean;
  loading: boolean;
  data: DataType;
  paymentType: string;
}

const ConfirmModal: React.FC<ConfirmModalProps> = ({
  handleCloseModal,
  handleMakePayment,
  paymentType,
  openConfirm,
  loading,
  data
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId, accEValue: accountId } = useAccountDetail();
  const fee = useStoreIdSelector(selectConvenienceFee);

  const { amount, accountNumber, date } = data;
  const { value: amountValue } = amount;
  const total =
    paymentType === PAYMENT_TYPE.DEMAND_ACH
      ? Number(amountValue)
      : Number(fee) + Number(amountValue);

  const amountView = formatCommon(Number(amountValue)).currency();
  const feeView = formatCommon(Number(fee)).currency();
  const dateView = formatCommon(date).time.date;
  const totalView = formatCommon(total).currency();
  const testId = 'makePayment_modalConfirm';

  useEffect(() => {
    if (
      !openConfirm ||
      !data ||
      paymentType === PAYMENT_TYPE.DEMAND_ACH ||
      Number(fee) > 0
    )
      return;
    const { paymentMedium, amount, date } = data;
    const paymentDate = formatTimeDefault(date, FormatTime.Date);
    dispatch(
      actions.getConvenienceFee({
        storeId,
        params: {
          accountId,
          paymentAmount: amount?.value,
          paymentMedium,
          paymentDate
        }
      })
    );
  }, [accountId, data, dispatch, fee, openConfirm, paymentType, storeId]);

  const renderBodyByType = () => {
    if (paymentType === PAYMENT_TYPE.DEMAND_ACH) {
      return (
        <>
          <p
            className="pb-16"
            data-testid={genAmtId(testId, 'information', '')}
          >
            <span>{t('txt_modal_content_1')}</span>
            <span className="fw-500">{` ${totalView} `}</span>
            <span> {t('txt_modal_content_2')} </span>
            <span className="custom-account-number">
              <span className="fw-500 hide-account-icon">
                {` ${accountNumber.replace(/\*/g, '•')} `}
              </span>
            </span>
            <span>{t('txt_modal_ach_content_3')}</span>
            <span className="fw-500"> {dateView}.</span>
          </p>
          <p data-testid={genAmtId(testId, 'confirm', '')}>
            {t('txt_can_you_please_confirm_your_authorization_to_proceed')}
          </p>
        </>
      );
    }
    return (
      <div>
        <>
          <p
            className="pb-16"
            data-testid={genAmtId(testId, 'information', '')}
          >
            <span>{t('txt_modal_content_1')}</span>
            <span className="fw-500">{` ${amountView} `}</span>
            <span> {t('txt_modal_content_2')} </span>
            <span className="custom-account-number">
              <span className="fw-500 hide-account-icon">
                {` ${`*****${accountNumber.replace(/\*/g, '')}`.replace(
                  /\*/g,
                  '•'
                )} `}
              </span>
            </span>
            <span>{t('txt_modal_content_3')}</span>
            <span className="fw-500">{` ${feeView} `}</span>
            <span>{t('txt_modal_content_4')} </span>
            <span className="fw-500"> {dateView}.</span>
          </p>
          <p data-testid={genAmtId(testId, 'confirm', '')}>
            {t('txt_can_you_please_confirm_your_authorization_to_proceed')}
          </p>
        </>
      </div>
    );
  };

  return (
    <Modal sm show={openConfirm} loading={loading} dataTestId={testId}>
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId={`${testId}-header`}
      >
        <ModalTitle dataTestId={`${testId}-title`}>
          {t('txt_confirm_make_payment')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>{renderBodyByType()}</ModalBody>
      <ModalFooter
        okButtonText={t('txt_confirm')}
        cancelButtonText={t('txt_cancel')}
        onCancel={handleCloseModal}
        onOk={handleMakePayment}
        dataTestId={`${testId}-footer`}
      />
    </Modal>
  );
};

export default ConfirmModal;
