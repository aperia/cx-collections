import confirmMakePaymentServices from './confirmMakePaymentServices';
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

describe('confirmMakePaymentServices', () => {
  const params = {
    common: {
      app: 'app',
      accountId: 'accountId'
    },
    paymentAmount: 'paymentAmount',
    paymentMedium: 'paymentMedium',
    paymentDate: 'paymentDate'
  };
  it('getConvenienceFee when url was defined', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    confirmMakePaymentServices.getConvenienceFee(params);

    expect(mockService).toBeCalledWith('', params);
  });

  it('getConvenienceFee', () => {
    mockAppConfigApi.setup();

    const mockService = mockApiServices.post();

    confirmMakePaymentServices.getConvenienceFee(params);

    expect(mockService).toBeCalledWith(
      apiUrl.payment.getConvenienceFee,
      params
    );
  });
});
