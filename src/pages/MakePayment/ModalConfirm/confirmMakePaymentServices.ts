import { apiService } from 'app/utils/api.service';
import { ReactText } from 'react';

interface GetConvenienceFeeRequest {
  common: {
    app: string;
    accountId: string;
  };
  paymentAmount?: ReactText;
  paymentMedium?: string;
  paymentDate?: string;
}

const confirmMakePaymentServices = {
  getConvenienceFee(data: GetConvenienceFeeRequest) {
    const url = window.appConfig?.api?.payment?.getConvenienceFee || '';
    return apiService.post(url, data);
  }
};
export default confirmMakePaymentServices;
