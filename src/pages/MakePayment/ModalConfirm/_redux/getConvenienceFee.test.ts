import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';

import { responseDefault, storeId } from 'app/test-utils';

import { getConvenienceFee, Args } from './getConvenienceFee';
import { reducer } from './reducers';
import services from '../confirmMakePaymentServices';

const requestParams: Args = {
  storeId,
  params: {
    accountId: storeId
  }
};

const initialState = {};

describe('test getConvenienceFee', () => {
  it('It should be full filled', async () => {
    const store = createStore(rootReducer, {});
    jest
      .spyOn(services, 'getConvenienceFee')
      .mockResolvedValue({ ...responseDefault, data: {} });
    const response = await getConvenienceFee(requestParams)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual('paymentConfirm/getConvenienceFee/fulfilled');
  });

  it('It should be rejected', async () => {
    const store = createStore(rootReducer, {});
    jest
      .spyOn(services, 'getConvenienceFee')
      .mockRejectedValue({ ...responseDefault, data: {} });
    const response = await getConvenienceFee(requestParams)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual('paymentConfirm/getConvenienceFee/rejected');
  });

  it('It should be pending', async () => {
    const action = getConvenienceFee.pending(
      getConvenienceFee.pending.type,
      requestParams
    );
    jest
      .spyOn(services, 'getConvenienceFee')
      .mockResolvedValue({ ...responseDefault, data: {} });
    const nextState = reducer(initialState, action);
    expect(nextState[storeId].loading).toEqual(true);
  });
});
