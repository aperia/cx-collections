import { ReactText } from 'react';
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Service
import confirmMakePaymentServices from '../confirmMakePaymentServices';

// Type
import { ResponseConvenienceFee, Result } from '../types';
import { EMPTY_STRING } from 'app/constants';

export interface Payload {
  data: ResponseConvenienceFee;
}

export interface Args {
  storeId: string;
  params: {
    accountId: string;
    paymentAmount?: ReactText;
    paymentMedium?: string;
    paymentDate?: string;
  };
}

export const getConvenienceFee = createAsyncThunk<
  Payload,
  Args,
  ThunkAPIConfig
>('paymentConfirm/getConvenienceFee', async (args, thunkAPI) => {
  const {
    params: { accountId, paymentAmount, paymentMedium, paymentDate }
  } = args;

  const { commonConfig } = window.appConfig || {};

  return confirmMakePaymentServices.getConvenienceFee({
    common: {
      app: commonConfig?.appCxCollections || EMPTY_STRING,
      accountId
    },
    paymentAmount,
    paymentMedium,
    paymentDate
  });
});

export const getConvenienceFeeBuilder = (
  builder: ActionReducerMapBuilder<Result>
) => {
  builder
    .addCase(getConvenienceFee.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: true
      };
    })
    .addCase(getConvenienceFee.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: false,
        responseConvenienceFee: data
      };
    })
    .addCase(getConvenienceFee.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: false
      };
    });
};
