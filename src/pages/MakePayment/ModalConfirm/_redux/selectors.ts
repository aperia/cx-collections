import { ReactText } from 'react';
import { createSelector } from '@reduxjs/toolkit';

export const selectLoading = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentConfirm[storeId]?.loading || false,
  (loading: boolean) => loading
);

export const selectConvenienceFee = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentConfirm[storeId]?.responseConvenienceFee?.convenienceFee || 0,
  (convenienceFee: ReactText | undefined) => convenienceFee
);
