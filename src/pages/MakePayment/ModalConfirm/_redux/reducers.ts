import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Types
import { Result } from '../types';

// Thunk
import {
  getConvenienceFee,
  getConvenienceFeeBuilder
} from './getConvenienceFee';

const initialState: Result = {};

const { actions: _actions, reducer } = createSlice({
  name: 'paymentConfirm',
  initialState,
  reducers: {
    clearData: (draffState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draffState[storeId];
    }
  },
  extraReducers: builder => {
    getConvenienceFeeBuilder(builder);
  }
});

const actions = {
  ..._actions,
  getConvenienceFee
};

export { actions, reducer };
