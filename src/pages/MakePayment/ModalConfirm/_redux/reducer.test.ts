import { storeId } from 'app/test-utils';
import {reducer , actions} from './reducers'

const {clearData} = actions

describe('test reducer',() => {
    it('clear data work correctly', () => {
        const state = reducer({[storeId] : {loading: true}}, clearData({ storeId }));
       expect(state).toEqual({})
    })
})