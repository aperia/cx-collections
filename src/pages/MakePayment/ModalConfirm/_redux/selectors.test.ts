import { storeId } from 'app/test-utils';
import { useSelectorMock } from 'app/test-utils/useSelectorMock';
import { selectConvenienceFee, selectLoading } from './selectors';

const store: Partial<RootState> = {
  paymentConfirm: {
    [storeId]: {
      loading: false,
      responseConvenienceFee: {
        convenienceFee: 10
      }
    }
  }
};

describe('should test modal confirm selectors', () => {
  it('selectLoading', () => {
    const data = useSelectorMock(selectLoading, store, storeId);

    expect(data).toEqual(false);
  });
  it('selectConvenienceFee', () => {
    const data = useSelectorMock(selectConvenienceFee, store, storeId);

    expect(data).toEqual(10);
  });
});
