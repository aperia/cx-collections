import { ReactText } from 'react';

export interface ResponseConvenienceFee {
  convenienceFee: ReactText;
  paymentAmount: ReactText;
  totalAmount: ReactText;
}

export interface State {
  loading?: boolean;
  responseConvenienceFee?: {
    convenienceFee?: ReactText;
    paymentAmount?: ReactText;
    resultMessage?: string;
    returnCode?: string;
    timeoutLimit?: ReactText;
    totalAmount?: ReactText;
  };
}

export interface Result extends Record<string, State> {}
