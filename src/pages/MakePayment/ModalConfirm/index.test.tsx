import React from 'react';

// Component
import ModalConfirm, { DataType } from './index';

// Helper
import { renderMockStoreId } from 'app/test-utils';

// Hooks
import { screen } from '@testing-library/react';

// Types
import { PAYMENT_TYPE } from '../constants';
import { ModalFooterProps } from 'app/_libraries/_dls/components';

jest.mock('app/_libraries/_dls/components', () => ({
  ...jest.requireActual('app/_libraries/_dls/components'),
  ModalFooter: ({ onCancel, onOk }: ModalFooterProps) => (
    <>
      <button data-testid="onCancel" onClick={onCancel} />
      <button data-testid="onOk" onClick={onOk} />
    </>
  )
}));

const dataDefault: DataType = {
  amount: { value: '1' },
  accountNumber: '12345',
  date: new Date()
};

const renderWrapper = (data: DataType, paymentType: string) => {
  const { wrapper } = renderMockStoreId(
    <ModalConfirm
      handleCloseModal={jest.fn()}
      handleMakePayment={jest.fn()}
      openConfirm
      loading={false}
      data={data}
      paymentType={paymentType}
    />,
    { initialState: {} }
  );

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement
  };
};

describe('Test ModalConfirm component', () => {
  it('Render Schedule', () => {
    renderWrapper(dataDefault, PAYMENT_TYPE.SCHEDULE);
    expect(screen.queryByText('txt_confirm_make_payment')).toBeInTheDocument();
  });

  it('Render with ACH', () => {
    renderWrapper(dataDefault, PAYMENT_TYPE.DEMAND_ACH);
    expect(screen.queryByText('txt_confirm_make_payment')).toBeInTheDocument();
  });
});
