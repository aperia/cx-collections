import { ReactText } from 'react';

export interface PaymentDateView {
  id?: string;
  value?: Date | null;
  label?: string;
}

export interface PaymentAmountView {
  id?: string;
  title?: string;
  value?: ReactText;
  label?: string;
}

export interface PaymentAmount {
  value?: ReactText;
  label?: string;
}

export const getAmountID = (amount: PaymentAmount) => {
  const { label = '', value } = amount;
  if (!label && !value) return;

  return `${label.replace(' ', '_')}-${value}`;
};

export const OTHER_AMOUNT: PaymentAmountView = {
  label: 'txt_otherAmount',
  title: 'txt_otherAmount',
  value: undefined
};

export const ID_OTHER_AMOUNT = 'otherAmount';

export const ID_OTHER_DATE = 'paymentDueOtherDate';

export const OTHER_DATE: PaymentDateView = {
  label: 'txt_otherDate',
  value: undefined,
  id: ID_OTHER_DATE
};

// PAYMENT TYPE
export enum PAYMENT_TYPE {
  SCHEDULE = 'Schedule',
  DEMAND_ACH = 'ACH'
}
