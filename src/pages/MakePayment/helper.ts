import { PAYMENT_CARD_TYPE } from 'pages/MakePayment/SchedulePayment/types';
import { formatAPICurrency } from 'app/helpers';
import isUndefined from 'lodash.isundefined';
import { ReactText } from 'react';
import { METHOD, OTHER_CODE } from 'pages/MakePayment/ACHPayment/constants';
import { ID_OTHER_AMOUNT } from './constants';

export const getLabelView = (
  t: any,
  label = '',
  value?: ReactText,
  id?: string
) => {
  const isOtherAmount = id === ID_OTHER_AMOUNT || id === OTHER_CODE;

  return `${t(label)}${isOtherAmount ? '' : ':'} ${
    isUndefined(value) ? '' : formatAPICurrency(`${value}`)
  }`;
};

export const getMethodByCardType = (cardType: string) => {
  return cardType === PAYMENT_CARD_TYPE.CARD_INFO ? METHOD.card : METHOD.bank;
};
