// Payment Type
export interface PaymentTypeList {
  label: React.ReactNode;
  value: string;
}
