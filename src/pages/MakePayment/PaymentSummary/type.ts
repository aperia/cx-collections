export interface PaymentSummaryState {
  data: MagicKeyValue;
  isLoading: boolean;
}

export interface PaymentSummary extends Record<string, PaymentSummaryState> { }