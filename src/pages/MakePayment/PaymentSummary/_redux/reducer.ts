import { createSlice } from '@reduxjs/toolkit';

// Type
import { PaymentSummary } from '../type';

// helper
import { getPaymentSummary, getPaymentSummaryBuilder } from '../../PaymentSummary/_redux/getPaymentSummary';


const initialState: PaymentSummary = {};

const { actions, reducer } = createSlice({
  name: 'paymentSummary',
  initialState,
  reducers: {},
  extraReducers: builder => {
    getPaymentSummaryBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getPaymentSummary
};

export { allActions as paymentSummaryActions, reducer };
