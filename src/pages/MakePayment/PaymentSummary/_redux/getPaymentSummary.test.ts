import { responseDefault, storeId } from 'app/test-utils';
import { statementTransactionServices } from 'pages/StatementsAndTransactions/StatementTransaction/api-services';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import { getPaymentSummary } from './getPaymentSummary'

const requestData = {
  storeId,
  postData: {
    accountId: storeId,
    statementDate: ''
  }
};

describe('getPaymentSummary', () => {
  it('should be error', async () => {
    jest
      .spyOn(statementTransactionServices, 'getStatementOverview')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    const store = createStore(rootReducer, {
      paymentSummary: {
        [storeId]: {
          data: {
            minimumPaymentAmount: "0000000000000218.96",
            pastDueAmount: "0000000000000109.48"
          }
        }
      }
    }, applyMiddleware(thunk));

    const response = await getPaymentSummary(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'paymentSummary/getPaymentSummary/rejected'
    );
  });

  it('should be success', async () => {
    jest
      .spyOn(statementTransactionServices, 'getStatementOverview')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    const store = createStore(rootReducer, {
      paymentSummary: {
        [storeId]: {
          data: {
            minimumPaymentAmount: "0000000000000218.96",
            pastDueAmount: "0000000000000109.48"
          }
        }
      },
      statementTransactionList: {
        [storeId]: {
          detail: {
            data: {}
          },
          adjust: {}
        }
      },
      form: {
        [`${storeId}_transaction-adjustment`]: {
          values: {
            type: {
              adjustmentBatchCode: '123',
              adjustmentTranCode: '123',
              adjustmentDescription: '1',
              amount: '123'
            }
          }
        }
      },
      mapping: {
        data: {}
      }
    }, applyMiddleware(thunk));

    const response = await getPaymentSummary(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'paymentSummary/getPaymentSummary/fulfilled'
    );
  });
})

