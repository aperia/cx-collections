// Redux
import { AppState } from 'storeConfig';
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Helpers
import { mappingDataFromObj } from 'app/helpers';
import {
  StatementOverviewArg,
  StatementTransactionStateResult
} from 'pages/StatementsAndTransactions/StatementTransaction/types';

// Services
import { statementTransactionServices } from 'pages/StatementsAndTransactions/StatementTransaction/api-services';
import { PaymentSummary } from '../type';

interface getPaymentSummaryPayLoad extends StatementTransactionStateResult { }
interface getPaymentSummaryArgs extends StatementOverviewArg { }

export const getPaymentSummary = createAsyncThunk<
  getPaymentSummaryPayLoad,
  getPaymentSummaryArgs,
  ThunkAPIConfig
>('paymentSummary/getPaymentSummary', async (args, thunkAPI) => {
  try {
    const { accountId, statementDate } = args.postData;

    const { data } = await statementTransactionServices.getStatementOverview({
      accountId,
      statementDate
    });

    const state = thunkAPI.getState() as AppState;
    const mapping = state.mapping.data.statementSummary || {};

    const mappedData = mappingDataFromObj(data, mapping);

    return { data: mappedData } as getPaymentSummaryPayLoad;
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const getPaymentSummaryBuilder = (
  builder: ActionReducerMapBuilder<PaymentSummary>
) => {
  builder
    .addCase(getPaymentSummary.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true
      };
    })
    .addCase(getPaymentSummary.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;


      draftState[storeId] = {
        ...draftState[storeId],
        data,
        isLoading: false,
      };
    })
    .addCase(getPaymentSummary.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
      }
    });
};
