import { METHOD, OTHER_CODE } from './ACHPayment/constants';
import { ID_OTHER_AMOUNT } from './constants';
import { getLabelView, getMethodByCardType } from './helper';
import { PAYMENT_CARD_TYPE } from './SchedulePayment/types';

const t = (value: string) => value;

describe('test getLabelView', () => {
  it('return correct value without label, value and id', () => {
    const data = getLabelView(t);
    expect(data).toEqual(': ');
  });

  it('return correct value with id is equal ID_OTHER_AMOUNT or OTHER_CODE', () => {
    const dataWithIdOtherAmount = getLabelView(t, 'Test', '', ID_OTHER_AMOUNT);
    expect(dataWithIdOtherAmount).toEqual('Test ');

    const dataWithOtherCode = getLabelView(t, 'Test', '', OTHER_CODE);
    expect(dataWithOtherCode).toEqual('Test ');
  });

  it('return correct value with id is not equal ID_OTHER_AMOUNT or OTHER_CODE', () => {
    const data = getLabelView(t, 'Test', '', 'No code');
    expect(data).toEqual('Test: ');
  });

  it('return correct value with full params', () => {
    const data = getLabelView(t, 'Test', '123', 'No code');
    expect(data).toEqual('Test: $123.00');
  });
});

describe('getMethodByCardType', () => {
  it.each`
    cardType                       | expectedResult
    ${"''"}                        | ${METHOD.bank}
    ${PAYMENT_CARD_TYPE.BANK_INFO} | ${METHOD.bank}
    ${PAYMENT_CARD_TYPE.CHECKING}  | ${METHOD.bank}
    ${PAYMENT_CARD_TYPE.CREDIT}    | ${METHOD.bank}
    ${PAYMENT_CARD_TYPE.DEBIT}     | ${METHOD.bank}
    ${PAYMENT_CARD_TYPE.SAVING}    | ${METHOD.bank}
    ${PAYMENT_CARD_TYPE.CARD_INFO} | ${METHOD.card}
  `(
    'returns $expectedResult when cardType is $cardType',
    ({ cardType, expectedResult }) => {
      expect(getMethodByCardType(cardType)).toEqual(expectedResult);
    }
  );
});
