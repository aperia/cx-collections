import { METHOD, ModeType, TYPE_CARD, TYPE_TRANSACTION } from '../constants';
import { createSelector } from '@reduxjs/toolkit';
import { SubmitMakePaymentData } from 'pages/MakePayment/SchedulePayment/types';
import {
  ACHPaymentData,
  VerifyInfo,
  BankAccount,
  AmountData,
  TransactionTypeData
} from '../types';

// Get data post
export const getDataPost = createSelector(
  (states: RootState, storeId: string) => {
    const ach = states.demandACHPayment[storeId] || {};
    const { amount = {}, transactionCode } = ach;
    const typeChecked =
      states.demandACHPayment[storeId]?.achInformation?.typeChecked ||
      TYPE_CARD.CHECKING.value;
    const gridData =
      states.demandACHPayment[storeId]?.achInformation?.gridData || [];
    const checked = gridData.filter(item => item.code === typeChecked);
    return {
      amount,
      transactionType: transactionCode,
      accountNumberMaskedValue: checked[0]?.accountNumber,
      schedulePaymentDate: new Date(),
      method: METHOD.bank
    };
  },
  (data: SubmitMakePaymentData) => data
);

export const getIsLoadingMakePayment = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.submitMakePayment?.loading || false,
  (loading: boolean) => loading
);

export const selectAllCheckedList = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.allCheckedList || [],
  (data: string[]) => data
);

export const selectCheckedList = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.checkedList || [],
  (data: string[]) => data
);

export const selectBankAccounts = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.data?.bankAccounts || [],
  (data: BankAccount[]) => data
);

export const selectNoRegister = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.noRegister || false,
  (noRegister: boolean) => noRegister
);

export const selectOpenModal = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.openModal || false,
  (openModal: boolean) => openModal
);

export const selectVerifyModal = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.isVerify || false,
  (isVerify: boolean) => isVerify
);

export const selectVerifyInfo = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.verifyInfo || {},
  (verifyInfo: VerifyInfo) => verifyInfo
);

export const selectReload = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.reload || false,
  (reload: boolean) => reload
);

export const selectACHData = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.data || {},
  (data: ACHPaymentData) => data
);

export const selectMode = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.mode || '',
  (mode: string) => mode
);

export const selectBankInfo = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.data || {},
  (data: ACHPaymentData) => {
    if (!data?.bankInformation) return {};
    const { bankCity, bankName, bankState, routingNumber } =
      data?.bankInformation;
    return {
      bankCity,
      bankName,
      bankState,
      bankRoutingNumber: routingNumber?.maskValue
    };
  }
);

export const selectAmount = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.amount || {},
  (amount: AmountData) => amount
);

export const selectAmountInfo = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.dataInfo?.data?.amountInfo || [],
  (dataInfo: AmountData[]) => dataInfo
);

export const selectBalanceAmount = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.dataInfo?.data?.currentBalanceAmount,
  dataInfo => dataInfo
);

export const selectTransaction = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.achInformation?.typeChecked ||
    TYPE_CARD.CHECKING.value,
  (checked: string) => {
    if (checked === TYPE_CARD.CHECKING.value) return TYPE_TRANSACTION.CHECKING;
    return TYPE_TRANSACTION.SAVINGS;
  }
);

export const selectTypeTransaction = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.transactionType,
  type => type
);

export const selectTransactions = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.dataInfo?.data?.transactionType || [],
  (transactionType: TransactionTypeData[]) => transactionType
);

export const selectFeeInfo = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.dataInfo?.data?.fee || 0,
  (fee: number) => fee
);

export const selectLoadingDelete = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.delete?.loading || false,
  (loading: boolean) => loading
);

export const selectLoadingForm = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.loadingForm || false,
  (loading: boolean) => loading
);

// integrate ACH
export const selectRoutingNumber = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.achInformation?.bankInfo?.data
      ?.transitRoutingIdentifier || '',
  (routingNumber: string) => routingNumber
);

export const selectViewDataACH = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.achInformation?.cardInfo?.data,
  cardInfo => cardInfo
);

export const selectBankInfoData = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.achInformation?.bankInfo?.data,
  bankInfo => bankInfo
);

export const selectGridData = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.achInformation?.gridData || [],
  gridData => gridData
);

export const selectTypeCard = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.achInformation?.typeChecked,
  typeCard => typeCard
);

export const selectBankName = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.achInformation?.cardInfo?.data
      ?.institutionFullName || '',
  bankName => bankName
);

export const selectDataEdit = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.mode || '',
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.achInformation?.bankInfo?.data
      ?.transitRoutingIdentifier || '',
  (mode: string, routingNumber: string) => {
    if (mode === ModeType.add) return {};
    return {
      bankRoutingNumber: routingNumber,
      checkingAccountNumber: '',
      savingsAccountNumber: ''
    };
  }
);

export const selectLoading = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.achInformation?.bankInfo
      ?.loadingBankInfo || false,
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.achInformation?.cardInfo
      ?.loadingCardInfo || false,
  (loading: boolean, loadingCard: boolean) => loading || loadingCard
);

export const selectIsInvalidSchedule = createSelector(
  (states: RootState, storeId: string) =>
    states.schedulePayment[storeId]?.isInvalidSchedule,
  isInvalidSchedule => isInvalidSchedule
);

export const selectIsInvalidACH = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.isInvalidACH,
  isInvalidACH => isInvalidACH
);

export const selectIsErrorACH = createSelector(
  (states: RootState, storeId: string) =>
    states.demandACHPayment[storeId]?.achInformation?.isError,
  isError => isError
);
