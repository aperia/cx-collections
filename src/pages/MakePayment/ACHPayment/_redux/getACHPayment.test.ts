import { getACHPayment } from './getACHPayment';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { ACHPaymentArg } from '../types';
import { responseDefault, storeId } from 'app/test-utils';

import paymentService from '../paymentService';

const requestData: ACHPaymentArg = {
  storeId,
  postData: {
    common: {
      accountId: storeId
    }
  }
};

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('redux-store > getACHPayment', () => {
  it('async thunk', async () => {
    spy = jest
      .spyOn(paymentService, 'getACHPayment')
      .mockResolvedValue({ ...responseDefault, data: [] });

    const store = createStore(rootReducer, {});
    const response = await getACHPayment(requestData)(
      store.dispatch,
      store.getState,
      (undefined as unknown) as ThunkAPIConfig['extra']
    );

    expect(response.payload).toEqual({ ...responseDefault, data: [] });
  });

  it('reject', async () => {
    spy = jest.spyOn(paymentService, 'getACHPayment').mockRejectedValue({});

    const store = createStore(rootReducer, {});
    const response = await getACHPayment(requestData)(
      store.dispatch,
      store.getState,
      (undefined as unknown) as ThunkAPIConfig['extra']
    );

    expect(response.payload).toBeUndefined();
  });
});
