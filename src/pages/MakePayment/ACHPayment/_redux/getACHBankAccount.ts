import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// types
import {
  ACHPaymentInitialState,
  ACHBankAccountArg,
  DataEditPayload
} from '../types';

import paymentService from '../paymentService';

const SELECT_FIELDS = [
  'customerInquiry.cycleToDateUnpaidBilledPaymentDueAmount',
  'customerInquiry.cycleToDateNonDelinquentMinimumPaymentDueAmount',
  'customerInquiry.lastStatementBalanceAmount',
  'customerInquiry.currentBalanceAmount',
  'customerInquiry.nextPaymentDueDate'
];

export const getACHBankAccount = createAsyncThunk<
  DataEditPayload,
  ACHBankAccountArg,
  ThunkAPIConfig
>('payment/getACHBankAccount', async (args, thunkAPI) => {
  const { postData } = args;
  const data = await paymentService.getACHBankAccount({
    common: {
      accountId: postData.accountId
    },
    selectFields: SELECT_FIELDS,
    inquiryFields: {
      memberSequenceIdentifier: postData.memberSequenceIdentifier
    }
  });

  return data;
});

export const getACHBankAccountBuilder = (
  builder: ActionReducerMapBuilder<ACHPaymentInitialState>
) => {
  builder
    .addCase(getACHBankAccount.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId]
      };
    })
    .addCase(getACHBankAccount.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        loading: false,
        dataEdit: data
      };
    })
    .addCase(getACHBankAccount.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId]
      };
    });
};
