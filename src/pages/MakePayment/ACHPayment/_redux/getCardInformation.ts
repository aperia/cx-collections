import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// types
import {
  ACHPaymentInitialState,
  CardInfoACHArg,
  CardInfoACHPayload
} from '../types';

// Service
import paymentService from '../paymentService';

export const getCardInfoACH = createAsyncThunk<
  CardInfoACHPayload,
  CardInfoACHArg,
  ThunkAPIConfig
>('getCardInfoACH', async (args, thunkAPI) => {
  const { routingNumber } = args;
  const data = await paymentService.getCardInformationACH({
    routingNumber
  });
  return data;
});

export const getCardInfoACHBuilder = (
  builder: ActionReducerMapBuilder<ACHPaymentInitialState>
) => {
  builder
    .addCase(getCardInfoACH.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        achInformation: {
          ...draftState[storeId]?.achInformation,
          cardInfo: {
            loadingCardInfo: true
          }
        }
      };
    })
    .addCase(getCardInfoACH.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        achInformation: {
          ...draftState[storeId]?.achInformation,
          cardInfo: {
            loadingCardInfo: false,
            data: data?.financialPaymentInfo
          }
        }
      };
    })
    .addCase(getCardInfoACH.rejected, (draftState, action) => {
      const { storeId, routingNumber } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        achInformation: {
          ...draftState[storeId]?.achInformation,
          cardInfo: {
            data: {
              routingNumber
            },
            loadingCardInfo: false
          }
        }
      };
    });
};
