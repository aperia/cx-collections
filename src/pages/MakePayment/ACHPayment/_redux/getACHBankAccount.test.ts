import { getACHBankAccount } from './getACHBankAccount';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { ACHBankAccountArg } from '../types';
import { responseDefault, storeId } from 'app/test-utils';

import paymentService from '../paymentService';

const requestData: ACHBankAccountArg = {
  storeId,
  postData: {
    eValues: {
      bankRoutingNumber: 'bankRoutingNumber',
      checkingAccountNumber: 'checkingAccountNumber',
      savingsAccountNumber: 'savingsAccountNumber'
    }
  }
};

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('redux-store > getACHBankAccount', () => {
  it('async thunk', async () => {
    spy = jest
      .spyOn(paymentService, 'getACHBankAccount')
      .mockResolvedValue({ ...responseDefault, data: [] });

    const store = createStore(rootReducer, {});
    const response = await getACHBankAccount(requestData)(
      store.dispatch,
      store.getState,
      (undefined as unknown) as ThunkAPIConfig['extra']
    );

    expect(response.payload).toEqual({ ...responseDefault, data: [] });
  });

  it('reject', async () => {
    spy = jest.spyOn(paymentService, 'getACHBankAccount').mockRejectedValue({});

    const store = createStore(rootReducer, {});
    const response = await getACHBankAccount(requestData)(
      store.dispatch,
      store.getState,
      (undefined as unknown) as ThunkAPIConfig['extra']
    );

    expect(response.payload).toBeUndefined();
  });
});
