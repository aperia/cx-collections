import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// types
import {
  ACHPaymentInitialState,
  BankInfoACHArg,
  BankInfoACHPayload,
  GridDataType
} from '../types';

import paymentService from '../paymentService';
import { TYPE_CARD } from '../constants';
import isEmpty from 'lodash.isempty';
import { parseJSONString } from 'app/helpers';

const selectFields = [
  'customerInquiry.cycleToDateUnpaidBilledPaymentDueAmount',
  'customerInquiry.cycleToDateNonDelinquentMinimumPaymentDueAmount',
  'customerInquiry.lastStatementBalanceAmount',
  'customerInquiry.currentBalanceAmount',
  'customerInquiry.cycleToDateUnpaidBilledPaymentDueAmount',
  'customerInquiry.cycleToDateNonDelinquentMinimumPaymentDueAmount',
  'customerInquiry.nextPaymentDueDate',
  'customerInquiry.transitRoutingIdentifier',
  'customerInquiry.institutionNameAbbreviation',
  'customerInquiry.institutionCity',
  'customerInquiry.institutionState',
  'customerInquiry.presentationInstrumentIdentifier',
  'customerInquiry.transitRoutingIdentifier',
  'customerInquiry.demandDepositAccountIdentifier',
  'customerInquiry.savingsAccountIdentifier',
  'cardholderInfo.savingsAccountNumber'
];

export const getBankInformationACH = createAsyncThunk<
  BankInfoACHPayload,
  BankInfoACHArg,
  ThunkAPIConfig
>('getBankInfoACH', async (args, thunkAPI) => {
  const { memberSequenceIdentifier, accountId } = args;
  const data = await paymentService.getBankInformationACH({
    common: {
      accountId
    },
    inquiryFields: {
      memberSequenceIdentifier
    },
    selectFields
  });
  return data;
});

export const getBankInformationACHBuilder = (
  builder: ActionReducerMapBuilder<ACHPaymentInitialState>
) => {
  builder
    .addCase(getBankInformationACH.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        achInformation: {
          ...draftState[storeId]?.achInformation,
          bankInfo: {
            loadingBankInfo: true
          }
        }
      };
    })
    .addCase(getBankInformationACH.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      const saving = parseJSONString(
        data.cardholderInfo[0]?.savingsAccountNumber
      );
      const checking = parseJSONString(
        data.customerInquiry[0]?.demandDepositAccountIdentifier
      );
      let gridData: GridDataType[] = [];

      if (!isEmpty(checking)) {
        gridData = [
          ...gridData,
          {
            code: TYPE_CARD.CHECKING.value,
            typeCard: TYPE_CARD.CHECKING.description,
            accountNumber: checking?.maskedValue
          }
        ];
      }

      if (!isEmpty(saving)) {
        gridData = [
          ...gridData,
          {
            code: TYPE_CARD.SAVINGS.value,
            typeCard: TYPE_CARD.SAVINGS.description,
            accountNumber: saving?.maskedValue
          }
        ];
      }

      draftState[storeId] = {
        ...draftState[storeId],
        noRegister: gridData.length < 1,
        achInformation: {
          ...draftState[storeId]?.achInformation,
          isError: false,
          typeChecked: gridData[0]?.typeCard,
          gridData,
          bankInfo: {
            loadingBankInfo: false,
            data: data?.customerInquiry[0]
          }
        }
      };
    })
    .addCase(getBankInformationACH.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        achInformation: {
          ...draftState[storeId]?.achInformation,
          isError: true,
          bankInfo: {
            loadingBankInfo: false
          }
        }
      };
    });
};
