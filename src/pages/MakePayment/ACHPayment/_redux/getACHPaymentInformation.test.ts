import './reducers';

import {
  getACHPaymentInformation,
  getAmountID,
  mapAmountsToView
} from './getACHPaymentInformation';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { ACHPaymentArg, CustomerInquiry, AmountData } from '../types';
import { storeId, responseDefault } from 'app/test-utils';

import paymentService from '../paymentService';
import { OTHER_CODE } from '../constants';

const requestData: ACHPaymentArg = {
  storeId,
  postData: { common: { accountId: storeId } }
};

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

let store: Store<RootState>;
let state: RootState;

const initialState: Partial<RootState> = {
  demandACHPayment: {
    [storeId]: {
      achInformation: {
        gridData: [{ code: 'Checking', accountNumber: '2564', typeCard: 'S' }]
      }
    }
  },
  paymentSummary: {
    [storeId]: {
      data: {
        minimumPaymentAmount: '0000000000000218.96',
        pastDueAmount: '0000000000000109.48'
      }
    }
  }
};

const emptyDataState: Partial<RootState> = {
  demandACHPayment: {
    [storeId]: {}
  },
  paymentSummary: {
    [storeId]: {
      data: {}
    }
  }
};
beforeEach(() => {
  store = createStore(rootReducer, initialState);
  state = store.getState();
});

afterEach(() => {
  jest.clearAllMocks();
});

describe('redux-store > getACHPaymentInformation', () => {
  const data = {
    customerInquiry: [{}] as CustomerInquiry[],
    amountInfo: [{ label: 'label', code: OTHER_CODE }, { value: 'value' }]
  };
  const dataEmpty = {
    customerInquiry: [],
    amountInfo: [{ label: 'label', code: OTHER_CODE }, { value: 'value' }]
  };

  it('with initialData', async () => {
    spy = jest
      .spyOn(paymentService, 'getACHPaymentInformation')
      .mockResolvedValue({ ...responseDefault, data });

    const store = createStore(rootReducer, emptyDataState);
    const response = await getACHPaymentInformation(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('payment/getACHPaymentInformation/fulfilled');
  });

  it('success with empty data', async () => {
    // mock
    jest.spyOn(paymentService, 'getACHPaymentInformation').mockResolvedValue({
      ...responseDefault,
      data: dataEmpty
    });

    const store = createStore(rootReducer, emptyDataState);
    const response = await getACHPaymentInformation(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toBeUndefined();
  });

  it('should response getACHPaymentInformation pending', () => {
    const pendingAction = getACHPaymentInformation.pending(
      getACHPaymentInformation.pending.type,
      requestData
    );
    const actual = rootReducer(state, pendingAction);

    expect(actual.demandACHPayment[storeId].dataInfo!.loading).toBe(true);
  });

  it('reject', async () => {
    spy = jest
      .spyOn(paymentService, 'getACHPaymentInformation')
      .mockRejectedValue({});

    const store = createStore(rootReducer, {});
    const response = await getACHPaymentInformation(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toBeUndefined();
  });
});

describe('functions', () => {
  describe('getAmountID', () => {
    it('with parms value', () => {
      const amount: AmountData = { value: 'value' };
      const result = getAmountID(amount);

      expect(result).toEqual('-value');
    });

    it('with parms label and value', () => {
      const amount: AmountData = { label: 'label', value: 'value' };
      const result = getAmountID(amount);

      expect(result).toEqual('label-value');
    });

    it('without params', () => {
      const amount: AmountData = {};
      const result = getAmountID(amount);

      expect(result).toBeUndefined();
    });
  });

  describe('mapAmountsToView', () => {
    const defaultResult = {
      disabled: false,
      label: 'txt_otherAmount',
      title: 'txt_otherAmount',
      value: undefined,
      id: OTHER_CODE
    };

    it('without parms', () => {
      const result = mapAmountsToView();

      expect(result).toEqual([defaultResult]);
    });

    it('with parms value', () => {
      const amounts = [
        { label: 'label_1' },
        { label: 'label_2', value: 'value_2' }
      ];
      const result = mapAmountsToView(amounts);

      expect(result).toEqual([
        ...amounts.map(({ label, value = '' }) => ({
          disabled: false,
          id: getAmountID({ label, value }),
          value,
          title: label,
          label
        })),
        defaultResult
      ]);
    });
  });
});
