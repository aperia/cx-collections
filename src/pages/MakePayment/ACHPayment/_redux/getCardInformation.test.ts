import './reducers';

import { getCardInfoACH } from './getCardInformation';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { CardInfoACHArg, CardInfoACHData, CustomerInquiry } from '../types';
import { storeId, responseDefault } from 'app/test-utils';

import paymentService from '../paymentService';
import { OTHER_CODE } from '../constants';

const requestData: CardInfoACHArg = { storeId, routingNumber: '5263' };

const demandACHPayment = { [storeId]: {} };

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

let store: Store<RootState>;
let state: RootState;

const initialState: Partial<RootState> = {
  demandACHPayment: {
    [storeId]: {}
  }
};

beforeEach(() => {
  store = createStore(rootReducer, initialState);
  state = store.getState();
});

describe('redux-store > getCardInfoACH', () => {
  const data = {
    data: {
      customerInquiry: [{}] as CustomerInquiry[],
      amountInfo: [{ label: 'label', code: OTHER_CODE }, { value: 'value' }]
    }
  };

  it('success', async () => {
    spy = jest
      .spyOn(paymentService, 'getCardInformationACH')
      .mockResolvedValue({ ...responseDefault, data });

    const store = createStore(rootReducer, { demandACHPayment });
    const response = await getCardInfoACH(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ ...responseDefault, data });
  });

  it('success with empty data', async () => {
    // mock
    jest.spyOn(paymentService, 'getCardInformationACH').mockResolvedValue({
      ...responseDefault,
      data: {}
    });

    const store = createStore(rootReducer, { demandACHPayment });
    const response = await getCardInfoACH(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ ...responseDefault, data: {} });
  });

  it('should response getCardInfoACH pending', () => {
    const pendingAction = getCardInfoACH.pending(
      getCardInfoACH.pending.type,
      requestData
    );
    const actual = rootReducer(state, pendingAction);

    expect(
      actual.demandACHPayment[storeId].achInformation?.cardInfo?.loadingCardInfo
    ).toBe(true);
  });

  it('should response getCardInfoACH fulfilled', () => {
    const data: CardInfoACHData = {
      fileKe: 'fileKe',
      institutionTypeCode: 'institutionTypeCode',
      branchCode: 'branchCode',
      routingNumber: 12,
      institutionFullName: 'institutionFullName',
      institutionNameAbbreviation: 'institutionNameAbbreviation',
      institutionCity: 'institutionCity',
      institutionState: 'institutionState'
    };

    const fulfilled = getCardInfoACH.fulfilled(
      { data: { financialPaymentInfo: data } },
      getCardInfoACH.fulfilled.type,
      requestData
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual.demandACHPayment[storeId].achInformation).toEqual({
      cardInfo: { data, loadingCardInfo: false }
    });
  });

  it('reject', async () => {
    spy = jest
      .spyOn(paymentService, 'getCardInformationACH')
      .mockRejectedValue({});

    const store = createStore(rootReducer, {});
    const response = await getCardInfoACH(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toBeUndefined();
  });
});
