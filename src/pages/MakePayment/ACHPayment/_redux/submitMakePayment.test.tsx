import {
  createStoreWithDefaultMiddleWare,
  responseDefault,
  storeId
} from 'app/test-utils';
import {
  PAYMENT_CARD_TYPE,
  ValidateBankAccountDebitInfoPayload,
  SubmitMakePaymentRequest
} from 'pages/MakePayment/SchedulePayment/types';
import { submitMakePayment } from './submitMakePayment';
import { CodeErrorType } from 'app/constants/enums';

import schedulePaymentService from '../paymentService';

const requestData: SubmitMakePaymentRequest = {
  storeId,
  data: {},
  accountId: storeId,
  onSuccess: () => undefined,
  onFailure: () => undefined
};

const data: ValidateBankAccountDebitInfoPayload = {
  bankRoutingNumber: '',
  bankAccountNumber: '',
  bankAccountType: PAYMENT_CARD_TYPE.DEBIT
};

describe('redux-store > submitMakePayment', () => {
  it('success', async () => {
    // mock
    const mockOnSuccess = jest.fn();
    jest
      .spyOn(schedulePaymentService, 'submitACHMakePayment')
      .mockResolvedValue({
        ...responseDefault,
        data
      });

    // store
    const store = createStoreWithDefaultMiddleWare({
      schedulePayment: {
        [storeId]: {}
      }
    });

    // invoke
    await submitMakePayment({ ...requestData, onSuccess: mockOnSuccess })(
      store.dispatch,
      store.getState,
      {}
    );

    // expect
    expect(mockOnSuccess).toBeCalled();
  });

  it('another error', async () => {
    // mock
    const mockOnFailure = jest.fn();
    jest
      .spyOn(schedulePaymentService, 'submitACHMakePayment')
      .mockRejectedValue({
        ...responseDefault,
        data: {}
      });

    // store
    const store = createStoreWithDefaultMiddleWare({
      schedulePayment: {
        [storeId]: {}
      }
    });

    // invoke
    await submitMakePayment({ ...requestData, onFailure: mockOnFailure })(
      store.dispatch,
      store.getState,
      {}
    );

    // expect
    expect(mockOnFailure).toBeCalled();
  });

  it('error 453', async () => {
    // mock
    const mockOnFailure = jest.fn();
    jest
      .spyOn(schedulePaymentService, 'submitACHMakePayment')
      .mockRejectedValue({
        ...responseDefault,
        data: {
          coreResponse: { responseCode: CodeErrorType.ValidationException }
        }
      });

    // store
    const store = createStoreWithDefaultMiddleWare({
      schedulePayment: {
        [storeId]: {}
      }
    });

    // invoke
    await submitMakePayment({ ...requestData, onFailure: mockOnFailure })(
      store.dispatch,
      store.getState,
      {}
    );

    // expect
    expect(mockOnFailure).toBeCalled();
  });
});
