import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// helpers
import isEmpty from 'lodash.isempty';

// types
import {
  ACHPaymentInitialState,
  ACHPaymentArg,
  ACHPaymentPayload
} from '../types';

import paymentService from '../paymentService';

export const getACHPayment = createAsyncThunk<
  ACHPaymentPayload,
  ACHPaymentArg,
  ThunkAPIConfig
>('payment/getACHPayment', async (args, thunkAPI) => {
  const { postData } = args;
  const data = await paymentService.getACHPayment(postData);
  return data;
});

export const getACHPaymentBuilder = (
  builder: ActionReducerMapBuilder<ACHPaymentInitialState>
) => {
  builder
    .addCase(getACHPayment.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: true
      };
    })
    .addCase(getACHPayment.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        loading: false,
        data,
        noRegister: isEmpty(data)
      };
    })
    .addCase(getACHPayment.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: false
      };
    });
};
