import { isRejected, isFulfilled } from '@reduxjs/toolkit';

import { demandACHPaymentAction } from 'pages/MakePayment/ACHPayment/_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { VerifyACHArg } from 'pages/MakePayment/ACHPayment/types';
import { responseDefault, storeId, mockActionCreator } from 'app/test-utils';
import { Dispatch } from 'react';
import { memoActions } from 'pages/Memos/_redux/reducers';
import paymentService from '../paymentService';

const requestData: VerifyACHArg = {
  storeId,
  postData: {
    accountId: storeId,
    checkingAccountNumber: 'checkingAccountNumber', 
    savingsAccountNumber: 'savingsAccountNumber' 
  }
};

const memoSpy = mockActionCreator(memoActions);

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

jest.mock('pages/__commons/ToastNotifications/_redux', () => ({
  ...jest.requireActual('pages/__commons/ToastNotifications/_redux'),
  actionsToast: {
    addToast: jest.fn()
  }
}));

jest.mock('@reduxjs/toolkit', () => ({
  ...jest.requireActual('@reduxjs/toolkit'),
  isRejected: jest.fn(),
  isFulfilled: jest.fn()
}));

describe('redux-store > updateACHPayment', () => {

  it('full filled without not call memoActions', async () => {
    (isFulfilled as unknown as jest.Mock).mockImplementation(() => true);
    jest
      .spyOn(paymentService, 'addACHBankAccount')
      .mockResolvedValue({ ...responseDefault, data: [] });
    const postMemoSpy = memoSpy('postMemo');
    const store = createStore(rootReducer, {});
    await demandACHPaymentAction.triggerUpdateACHPayment({
      storeId,
      postData: {
        accountId: storeId
      }
    })((() => null as unknown) as Dispatch<object>, store.getState, {});

    expect(postMemoSpy).not.toBeCalled();
  });

  it('async thunk', async () => {
    spy = jest
      .spyOn(paymentService, 'updateACHBankAccount')
      .mockResolvedValue({ ...responseDefault, data: [] });

    const store = createStore(rootReducer, {});
    const response = await demandACHPaymentAction.updateACHPayment(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ ...responseDefault, data: [] });
  });

  it('reject', async () => {
    spy = jest
      .spyOn(paymentService, 'updateACHBankAccount')
      .mockRejectedValue({});

    const store = createStore(rootReducer, {});
    const response = await demandACHPaymentAction.updateACHPayment(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(isRejected(response)).toBeTruthy;
  });
});

describe('redux-store > triggerUpdateACHPayment', () => {
  it('async thunk', async () => {
    const store = createStore(rootReducer, {});
    await demandACHPaymentAction.triggerUpdateACHPayment(requestData)(
      () => null as unknown as Dispatch<object>,
      store.getState,
      {
        paymentService: {
          addACHBankAccount: jest.fn().mockResolvedValue({ data: [] })
        }
      }
    );

    expect(actionsToast.addToast).not.toBeCalled();
  });

  it('Coverages', async () => {
    (isFulfilled as unknown as jest.Mock).mockImplementation(() => true);
    (isRejected as unknown as jest.Mock).mockImplementation(() => true);

    expect(actionsToast.addToast).not.toBeCalled();

    const store = createStore(rootReducer, {});
    await demandACHPaymentAction.triggerUpdateACHPayment(requestData)(
      () => null as unknown as Dispatch<object>,
      store.getState,
      {
        paymentService: {
          addACHBankAccount: jest.fn().mockResolvedValue({ data: [] })
        }
      }
    );

    expect(actionsToast.addToast).toBeCalled();
  });
});
