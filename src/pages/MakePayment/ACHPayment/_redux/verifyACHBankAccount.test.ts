import { demandACHPaymentAction } from './reducers';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { VerifyACHArg } from 'pages/MakePayment/ACHPayment/types';
import {
  createStoreWithDefaultMiddleWare,
  responseDefault,
  storeId
} from 'app/test-utils';

import paymentService from '../paymentService';
import { isRejected } from '@reduxjs/toolkit';
import { CodeErrorType } from 'app/constants/enums';

const requestData: VerifyACHArg = {
  storeId,
  postData: {
    accountId: storeId
  }
};

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('redux-store > verifyACHPayment', () => {
  it('async thunk', async () => {
    spy = jest
      .spyOn(paymentService, 'verifyACHBankAccount')
      .mockResolvedValue({ ...responseDefault, data: [] });

    const store = createStoreWithDefaultMiddleWare({});
    const response = await demandACHPaymentAction.verifyACHPayment(requestData)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.payload).toEqual({ ...responseDefault, data: [] });
  });

  it('reject', async () => {
    spy = jest
      .spyOn(paymentService, 'verifyACHBankAccount')
      .mockRejectedValue({});

    const store = createStore(rootReducer, {});
    const response = await demandACHPaymentAction.verifyACHPayment(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(isRejected(response)).toBeTruthy;
  });
  it('error 453', async () => {
    // mock
    jest
      .spyOn(paymentService, 'verifyACHBankAccount')
      .mockRejectedValue({
        ...responseDefault,
        data: {
          coreResponse: { responseCode: CodeErrorType.ValidationException }
        }
      });

    // store
    const store = createStore(rootReducer, {
      schedulePayment: {
        [storeId]: {}
      }
    });

    // invoke
    const response = await demandACHPaymentAction.verifyACHPayment(requestData)(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig
    );

    // expect
    expect(response.payload).toBeTruthy();
  });
});
