import { isRejected, isFulfilled } from '@reduxjs/toolkit';

import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { demandACHPaymentAction } from './reducers';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { VerifyACHArg } from '../types';
import { mockActionCreator, responseDefault, storeId } from 'app/test-utils';
import { Dispatch } from 'react';
import { memoActions } from 'pages/Memos/_redux/reducers';

import paymentService from '../paymentService';

const requestData: VerifyACHArg = {
  storeId,
  postData: {
    accountId: storeId,
    checkingAccountNumber: 'checkingAccountNumber',
    savingsAccountNumber: 'savingsAccountNumber'
  }
};

let spy: jest.SpyInstance;
let spy2: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();

  spy2?.mockReset();
  spy2?.mockRestore();
});

jest.mock('@reduxjs/toolkit', () => ({
  ...jest.requireActual('@reduxjs/toolkit'),
  isRejected: jest.fn(),
  isFulfilled: jest.fn()
}));

const memoSpy = mockActionCreator(memoActions);

describe('redux-store > addACHPayment', () => {
  it('async thunk', async () => {
    spy = jest
      .spyOn(paymentService, 'addACHBankAccount')
      .mockResolvedValue({ ...responseDefault, data: [] });

    const store = createStore(rootReducer, {});
    const response = await demandACHPaymentAction.addACHPayment(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ ...responseDefault, data: [] });
  });

  it('reject', async () => {
    spy = jest.spyOn(paymentService, 'addACHBankAccount').mockRejectedValue({});

    const store = createStore(rootReducer, {});
    const response = await demandACHPaymentAction.addACHPayment(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(isRejected(response)).toBeTruthy;
  });
});

describe('redux-store > triggerAddACHPayment', () => {
  it('full filled without not call memoActions', async () => {
    (isFulfilled as unknown as jest.Mock).mockImplementation(() => true);
    jest
      .spyOn(paymentService, 'addACHBankAccount')
      .mockResolvedValue({ ...responseDefault, data: [] });
    const postMemoSpy = memoSpy('postMemo');
    const store = createStore(rootReducer, {});
    await demandACHPaymentAction.triggerAddACHPayment({
      storeId,
      postData: {
        accountId: storeId
      }
    })((() => null as unknown) as Dispatch<object>, store.getState, {});

    expect(postMemoSpy).not.toBeCalled();
  });

  it('async thunk', async () => {
    const mockAddToast = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy = jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);
    spy2 = jest
      .spyOn(paymentService, 'addACHBankAccount')
      .mockResolvedValue({ ...responseDefault, data: [] });

    const store = createStore(rootReducer, {});
    await demandACHPaymentAction.triggerAddACHPayment(requestData)(
      (() => null as unknown) as Dispatch<object>,
      store.getState,
      {}
    );

    expect(mockAddToast).not.toBeCalled();
  });

  it('Coverages', async () => {
    // mocks
    (isFulfilled as unknown as jest.Mock).mockImplementation(() => true);
    (isRejected as unknown as jest.Mock).mockImplementation(() => true);
    const mockAddToast = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy = jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);
    spy2 = jest
      .spyOn(paymentService, 'addACHBankAccount')
      .mockResolvedValue({ ...responseDefault, data: [] });

    expect(mockAddToast).not.toBeCalled();

    const store = createStore(rootReducer, {});
    await demandACHPaymentAction.triggerAddACHPayment(requestData)(
      (() => null as unknown) as Dispatch<object>,
      store.getState,
      {}
    );

    expect(mockAddToast).toBeCalled();
  });
});
