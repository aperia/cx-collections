import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// types
import {
  ACHPaymentInitialState,
  VerifyACHArg,
  VerifyACHPayload
} from '../types';

// Service
import paymentService from '../paymentService';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { CodeErrorType } from 'app/constants/enums';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

export const verifyACHPayment = createAsyncThunk<
  VerifyACHPayload,
  VerifyACHArg,
  ThunkAPIConfig
>('verifyACHBankAccount', async (args, thunkAPI) => {
  const { postData, storeId } = args;
  const { dispatch } = thunkAPI;
  try {
    const data = await paymentService.verifyACHBankAccount({
      routingNumber: postData.bankRoutingNumber
    });

    dispatch(
      apiErrorNotificationAction.clearApiErrorData({
        storeId,
        forSection: 'inAddAndEditPaymentMethodModal'
      })
    );
    return data;
  } catch (error) {
    const responseCode = error?.data?.coreResponse?.responseCode;

      if (responseCode !== CodeErrorType.ValidationException) {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'error',
            message: 'txt_verify_failed'
          })
        );
      }
    dispatch(
      apiErrorNotificationAction.updateApiError({
        storeId,
        forSection: 'inAddAndEditPaymentMethodModal',
        apiResponse: error
      })
    );
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const verifyACHPaymentBuilder = (
  builder: ActionReducerMapBuilder<ACHPaymentInitialState>
) => {
  builder
    .addCase(verifyACHPayment.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loadingForm: true
      };
    })
    .addCase(verifyACHPayment.fulfilled, (draftState, action) => {
      const { storeId, postData } = action.meta.arg;
      const { financialPaymentInfo: resData } = action.payload.data;
      draftState[storeId] = {
        ...draftState[storeId],
        loadingForm: false,
        verifyInfo: { ...postData, ...resData },
        isVerify: true
      };
    })
    .addCase(verifyACHPayment.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loadingForm: false
      };
    });
};
