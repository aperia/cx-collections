import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { OTHER_CODE } from '../constants';

// types
import {
  ACHPaymentArg,
  ACHPaymentInitialState,
  ACHPaymentInfoPayload,
  AmountData,
  CustomerInquiry,
  PaymentAmountView
} from '../types';

import paymentService from '../paymentService';
import { StatementOverviewData } from 'pages/StatementsAndTransactions/StatementTransaction/types';

export const getAmountID = (amount: AmountData) => {
  const { label = '', value } = amount;
  if (!label && !value) return;

  return `${label.replace(' ', '_')}-${value}`;
};

export const mapAmountsToView = (amounts: AmountData[] = []) => {
  const result: PaymentAmountView[] = amounts.map(({ label, value = '' }) => {
    const disabled = parseFloat(value + '') <= 0;

    return {
      id: getAmountID({ label, value }),
      value,
      title: label,
      disabled,
      label
    };
  });

  result.push({
    label: 'txt_otherAmount',
    title: 'txt_otherAmount',
    value: undefined,
    disabled: false,
    id: OTHER_CODE
  });

  return result;
};

export const mapRawDataToView = (rawData: CustomerInquiry[]) => {
  const {
    lastStatementBalanceAmount = '',
    currentBalanceAmount = '',
    cycleToDateUnpaidBilledPaymentDueAmount = '',
    cycleToDateNonDelinquentMinimumPaymentDueAmount = ''
  } = rawData[0];

  const lastStatementBalanceAmountView = parseFloat(
    lastStatementBalanceAmount.trim()
  );
  const currentBalanceAmountView = parseFloat(currentBalanceAmount.trim());
  const cycleToDateUnpaidBilledPaymentDueAmountView = parseFloat(
    cycleToDateUnpaidBilledPaymentDueAmount.trim()
  );
  const cycleToDateNonDelinquentMinimumPaymentDueAmountView = parseFloat(
    cycleToDateNonDelinquentMinimumPaymentDueAmount.trim()
  );

  const amountRaw = [
    {
      value:
        cycleToDateUnpaidBilledPaymentDueAmountView +
        cycleToDateNonDelinquentMinimumPaymentDueAmountView,
      label: 'txt_totalDueAmount'
    },
    {
      value: lastStatementBalanceAmountView,
      label: 'txt_lastStatementBalanceAmount'
    },
    {
      value: currentBalanceAmountView,
      label: 'txt_currentBalanceAmount'
    },
    {
      value: cycleToDateUnpaidBilledPaymentDueAmountView,
      label: 'txt_cycleToDateUnpaidBilledPaymentDueAmount'
    },
    {
      value: cycleToDateNonDelinquentMinimumPaymentDueAmountView,
      label: 'txt_cycleToDateNonDelinquentMinimumPaymentDueAmount'
    }
  ];

  return {
    amounts: mapAmountsToView(amountRaw),
    currentBalanceAmount: currentBalanceAmountView
  };
};

const ACH_PAYMENT_INFO_FIELD = [
  'customerInquiry.lastStatementBalanceAmount',
  'customerInquiry.currentBalanceAmount',
  'customerInquiry.cycleToDateUnpaidBilledPaymentDueAmount',
  'customerInquiry.cycleToDateNonDelinquentMinimumPaymentDueAmount'
];

export const getACHPaymentInformation = createAsyncThunk<
  ACHPaymentInfoPayload,
  ACHPaymentArg,
  ThunkAPIConfig
>('payment/getACHPaymentInformation', async (args, thunkAPI) => {
  const { storeId, postData } = args;
  const { pastDueAmount, minimumPaymentAmount } = thunkAPI.getState()
    ?.paymentSummary[storeId]?.data as StatementOverviewData;

  const data = await paymentService.getACHPaymentInformation({
    ...postData,
    selectFields: ACH_PAYMENT_INFO_FIELD
  });

  const {
    lastStatementBalanceAmount,
    currentBalanceAmount,
    nextPaymentDueDate
  } = data.data.customerInquiry[0];

  return {
    data: {
      customerInquiry: [
        {
          cycleToDateUnpaidBilledPaymentDueAmount: pastDueAmount,
          cycleToDateNonDelinquentMinimumPaymentDueAmount: minimumPaymentAmount,
          lastStatementBalanceAmount,
          currentBalanceAmount,
          nextPaymentDueDate
        } as CustomerInquiry
      ]
    }
  };
});

export const getACHPaymentInformationBuilder = (
  builder: ActionReducerMapBuilder<ACHPaymentInitialState>
) => {
  builder
    .addCase(getACHPaymentInformation.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        dataInfo: {
          loading: true
        }
      };
    })
    .addCase(getACHPaymentInformation.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      const { amounts, currentBalanceAmount } = mapRawDataToView(
        data?.customerInquiry
      );
      const amount = amounts.filter(amountItem => amountItem.value !== 0)[0];
      draftState[storeId] = {
        ...draftState[storeId],
        achInformation: {
          ...draftState[storeId]?.achInformation
        },
        amount,
        dataInfo: {
          data: {
            ...data?.customerInquiry,
            amountInfo: amounts,
            currentBalanceAmount
          },
          loading: false
        }
      };
    })
    .addCase(getACHPaymentInformation.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        dataInfo: {
          loading: false
        }
      };
    });
};
