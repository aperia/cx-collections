import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isRejected,
  isFulfilled
} from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// types
import { ACHPaymentInitialState, VerifyACHArg } from '../types';

import paymentService from '../paymentService';
import { batch } from 'react-redux';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { ACH_ACTION } from 'pages/Memos/constants';
import { formatTime } from 'app/helpers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { CHRONICLE_PAYMENT_KEY } from 'pages/ClientConfiguration/Memos/constants';

export const updateACHPayment = createAsyncThunk<
  MagicKeyValue,
  VerifyACHArg,
  ThunkAPIConfig
>('updateACH', async (args, thunkApi) => {
  const { postData } = args;
  const postDataFormat = {
    common: {
      cycle: 'M',
      accountId: postData?.accountId
    },
    demandDepositAccount: postData?.checkingAccountNumber,
    transitRoutingAccount: postData?.bankRoutingNumber,
    savingsAccount: postData?.savingsAccountNumber
  };
  try {
    return await paymentService.updateACHBankAccount({ ...postDataFormat });
  } catch (error) {
    return thunkApi.rejectWithValue({ response: error });
  }
});

export const triggerUpdateACHPayment = createAsyncThunk<
  void,
  VerifyACHArg,
  ThunkAPIConfig
>('triggerUpdateACH', async (args, { dispatch, getState }) => {
  const response = await dispatch(updateACHPayment(args));
  const { storeId, postData } = args;
  const states = getState();
  const bankNameACH =
    states.demandACHPayment[storeId]?.verifyInfo?.institutionFullName;
  // edit ACH success
  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_COMMON_TEXT.BANK_ACCOUNT_UPDATED
        })
      );
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId: args.storeId,
          forSection: 'inAddAndEditPaymentMethodModal'
        })
      );
      // Post memo
      const time = new Date().toString();
      if (postData?.checkingAccountNumber) {
        dispatch(
          memoActions.postMemo(ACH_ACTION.UPDATE_CHECKING, {
            storeId,
            accEValue: postData?.accountId,
            chronicleKey: CHRONICLE_PAYMENT_KEY,
            timeACH: formatTime(time).date,
            routingNumberACH: postData?.bankRoutingNumber,
            checkingACH: postData?.checkingAccountNumber,
            bankNameACH
          })
        );
      }
      if (postData?.savingsAccountNumber) {
        dispatch(
          memoActions.postMemo(ACH_ACTION.UPDATE_SAVING, {
            storeId,
            accEValue: postData?.accountId,
            chronicleKey: CHRONICLE_PAYMENT_KEY,
            timeACH: formatTime(time).date,
            routingNumberACH: postData?.bankRoutingNumber,
            bankNameACH,
            savingACH: postData?.savingsAccountNumber
          })
        );
      }
    });
  }

  // edit ACH error
  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_COMMON_TEXT.BANK_ACCOUNT_FAILED_TO_UPDATE
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId: args.storeId,
          forSection: 'inAddAndEditPaymentMethodModal',
          apiResponse: response.payload?.response
        })
      );
    });
  }
});

export const updateACHPaymentBuilder = (
  builder: ActionReducerMapBuilder<ACHPaymentInitialState>
) => {
  builder
    .addCase(updateACHPayment.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loadingForm: true
      };
    })
    .addCase(updateACHPayment.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loadingForm: false,
        isVerify: false,
        openModal: false,
        reload: true
      };
    })
    .addCase(updateACHPayment.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loadingForm: false
      };
    });
};
