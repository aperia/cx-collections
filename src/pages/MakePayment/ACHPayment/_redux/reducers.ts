import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// types
import {
  ACHPaymentInitialState,
  AmountData,
  UpdateChecked,
  UpdateOpenModal,
  UpdateReload,
  UpdateVerifyModal
} from '../types';

// async reducer
import { getACHPayment, getACHPaymentBuilder } from './getACHPayment';
import {
  verifyACHPayment,
  verifyACHPaymentBuilder
} from './verifyACHBankAccount';
import {
  addACHPayment,
  addACHPaymentBuilder,
  triggerAddACHPayment
} from './addACHBankAccount';
import {
  getACHBankAccountBuilder,
  getACHBankAccount
} from './getACHBankAccount';
import {
  updateACHPayment,
  updateACHPaymentBuilder,
  triggerUpdateACHPayment
} from './updateACHBankAccount';
import {
  getACHPaymentInformation,
  getACHPaymentInformationBuilder
} from './getACHPaymentInformation';

import {
  submitMakePayment,
  submitMakePaymentBuilder
} from './submitMakePayment';

// integrate ACH
import {
  getBankInformationACH,
  getBankInformationACHBuilder
} from './getBankInformation';
import { getCardInfoACH, getCardInfoACHBuilder } from './getCardInformation';

const { actions, reducer } = createSlice({
  name: 'demandACHPayment',
  initialState: {} as ACHPaymentInitialState,
  reducers: {
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    clearDataSelected: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        allCheckedList: [],
        transactionType: undefined,
        checkedList: [],
        amount: undefined
      };
    },
    setTransactionType: (
      draftState,
      action: PayloadAction<{ storeId: string; data: string; code: string }>
    ) => {
      const { storeId, data, code } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        transactionType: data,
        transactionCode: code
      };
    },
    setAmount: (
      draftState,
      action: PayloadAction<{ storeId: string; amount: AmountData }>
    ) => {
      const { storeId, amount } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        amount
      };
    },
    setIsInvalidACH: (
      draftState,
      action: PayloadAction<{ storeId: string; value: boolean }>
    ) => {
      const { storeId, value } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        isInvalidACH: value
      };
    },
    updateOpenModal: (draftState, action: PayloadAction<UpdateOpenModal>) => {
      const { storeId, openModal, mode } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        openModal,
        mode
      };
    },
    updateVerifyModal: (
      draftState,
      action: PayloadAction<UpdateVerifyModal>
    ) => {
      const { storeId, isVerify } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isVerify
      };
    },
    updateReload: (draftState, action: PayloadAction<UpdateReload>) => {
      const { storeId, reload } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        reload
      };
    },
    updateChecked: (draftState, action: PayloadAction<UpdateChecked>) => {
      const { storeId, typeChecked } = action.payload;
      const checked = draftState[storeId]?.achInformation?.gridData?.filter(
        item => item.code === typeChecked
      );
      draftState[storeId] = {
        ...draftState[storeId],
        transactionCode: undefined,
        transactionType: undefined,
        achInformation: {
          ...draftState[storeId]?.achInformation,
          typeChecked,
          checked
        }
      };
    }
  },
  extraReducers: builder => {
    getACHPaymentBuilder(builder);
    verifyACHPaymentBuilder(builder);
    addACHPaymentBuilder(builder);
    getACHBankAccountBuilder(builder);
    updateACHPaymentBuilder(builder);
    getACHPaymentInformationBuilder(builder);
    submitMakePaymentBuilder(builder);
    getBankInformationACHBuilder(builder);
    getCardInfoACHBuilder(builder);
  }
});

const demandACHPaymentAction = {
  ...actions,
  submitMakePayment,
  getACHPaymentInformation,
  getACHPayment,
  verifyACHPayment,
  addACHPayment,
  triggerAddACHPayment,
  getACHBankAccount,
  updateACHPayment,
  triggerUpdateACHPayment,
  getBankInformationACH,
  getCardInfoACH
};

export { demandACHPaymentAction, reducer };
