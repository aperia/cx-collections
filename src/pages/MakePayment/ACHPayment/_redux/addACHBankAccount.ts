import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isRejected,
  isFulfilled
} from '@reduxjs/toolkit';
import { batch } from 'react-redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// types
import { ACHPaymentInitialState, VerifyACHArg } from '../types';
import paymentService from '../paymentService';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { ACH_ACTION } from 'pages/Memos/constants';
import { formatTime } from 'app/helpers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { CHRONICLE_PAYMENT_KEY } from 'pages/ClientConfiguration/Memos/constants';

export const addACHPayment = createAsyncThunk<
  MagicKeyValue,
  VerifyACHArg,
  ThunkAPIConfig
>('addACHPayment', async (args, thunkAPI) => {
  const { postData } = args;

  try {
    const data = await paymentService.addACHBankAccount({
      common: {
        cycle: 'M',
        accountId: postData.accountId
      },
      transitRoutingAccount: postData.bankRoutingNumber,
      demandDepositAccount: postData.checkingAccountNumber,
      savingsAccount: postData.savingsAccountNumber
    });
    return data;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerAddACHPayment = createAsyncThunk<
  void,
  VerifyACHArg,
  ThunkAPIConfig
>('triggerAddACHPayment', async (args, { dispatch, getState }) => {
  const response = await dispatch(addACHPayment(args));
  const { storeId, postData } = args;
  const states = getState();
  const bankNameACH =
    states.demandACHPayment[storeId]?.verifyInfo?.institutionFullName;
  // add ACH success
  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'Payment method added.'
        })
      );
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId: args.storeId,
          forSection: 'inAddAndEditPaymentMethodModal'
        })
      );
      // Post memo
      const time = new Date().toString();
      if (postData?.checkingAccountNumber) {
        dispatch(
          memoActions.postMemo(ACH_ACTION.ADD_CHECKING, {
            storeId,
            accEValue: postData?.accountId,
            chronicleKey: CHRONICLE_PAYMENT_KEY,
            timeACH: formatTime(time).date,
            routingNumberACH: postData?.bankRoutingNumber,
            checkingACH: postData?.checkingAccountNumber,
            bankNameACH
          })
        );
      }
      if (postData?.savingsAccountNumber) {
        dispatch(
          memoActions.postMemo(ACH_ACTION.ADD_SAVING, {
            storeId,
            accEValue: postData?.accountId,
            chronicleKey: CHRONICLE_PAYMENT_KEY,
            timeACH: formatTime(time).date,
            routingNumberACH: postData?.bankRoutingNumber,
            bankNameACH,
            savingACH: postData?.savingsAccountNumber
          })
        );
      }
    });
  }

  // add ACH error
  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_COMMON_TEXT.BANK_ACCOUNT_FAILED_TO_ADD
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId: args.storeId,
          forSection: 'inAddAndEditPaymentMethodModal',
          apiResponse: response.payload?.response
        })
      );
    });
  }
});

export const addACHPaymentBuilder = (
  builder: ActionReducerMapBuilder<ACHPaymentInitialState>
) => {
  builder
    .addCase(addACHPayment.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loadingForm: true
      };
    })
    .addCase(addACHPayment.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loadingForm: false,
        isVerify: false,
        openModal: false,
        reload: true
      };
    })
    .addCase(addACHPayment.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loadingForm: false
      };
    });
};
