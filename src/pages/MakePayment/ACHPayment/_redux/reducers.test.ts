import { storeId } from 'app/test-utils';
import { TYPE_CARD } from '../constants';
import { AmountData } from '../types';
import { demandACHPaymentAction, reducer } from './reducers';

const {
  removeStore,
  clearDataSelected,
  setTransactionType,
  setAmount,
  updateOpenModal,
  updateVerifyModal,
  updateReload,
  updateChecked,
  setIsInvalidACH
} = demandACHPaymentAction;

const demandACHPayment = {
  [storeId]: {
    isInvalidACH: false,
    achInformation: {
      gridData: [{ code: 'code', accountNumber: '2564', typeCard: 'S' }]
    }
  }
};

describe('Test reducer', () => {
  it('removeStore action', () => {
    const state = reducer(
      { [storeId]: { checkedList: [] } },
      removeStore({ storeId })
    );

    expect(state).toEqual({});
  });

  it('clearDataSelected action', () => {
    const state = reducer({ [storeId]: {} }, clearDataSelected({ storeId }));

    expect(state[storeId]).toEqual({
      allCheckedList: [],
      transactionType: undefined,
      checkedList: [],
      amount: undefined
    });
  });

  it('setTransactionType action', () => {
    const data = 'transactionType';
    const state = reducer(
      { [storeId]: {} },
      setTransactionType({ storeId, data, code: 'code' })
    );

    expect(state[storeId]).toEqual({
      transactionCode: 'code',
      transactionType: data
    });
  });

  it('setAmount action', () => {
    const amount: AmountData = { label: 'label', value: 'value' };
    const state = reducer({ [storeId]: {} }, setAmount({ storeId, amount }));

    expect(state[storeId]).toEqual({ amount });
  });

  it('updateOpenModal action', () => {
    const state = reducer({}, updateOpenModal({ storeId, openModal: true }));

    expect(state).toEqual({ [storeId]: { openModal: true } });
  });

  it('updateVerifyModal action', () => {
    const state = reducer({}, updateVerifyModal({ storeId, isVerify: true }));

    expect(state).toEqual({ [storeId]: { isVerify: true } });
  });

  it('updateReload action', () => {
    const state = reducer({}, updateReload({ storeId, reload: true }));

    expect(state).toEqual({ [storeId]: { reload: true } });
  });

  describe('updateChecked action', () => {
    it('checking card', () => {
      const typeChecked = TYPE_CARD.CHECKING;
      const state = reducer(
        demandACHPayment,
        updateChecked({ storeId, typeChecked })
      );

      expect(state).toEqual({
        [storeId]: {
          ...demandACHPayment[storeId],
          achInformation: {
            ...demandACHPayment[storeId].achInformation,
            typeChecked: TYPE_CARD.CHECKING,
            checked: demandACHPayment[storeId].achInformation.gridData?.filter(
              item => item.code === typeChecked
            )
          }
        }
      });
    });

    it('saving card', () => {
      const typeChecked = TYPE_CARD.SAVINGS;
      const state = reducer(
        demandACHPayment,
        updateChecked({ storeId, typeChecked: TYPE_CARD.SAVINGS })
      );

      expect(state).toEqual({
        [storeId]: {
          ...demandACHPayment[storeId],
          achInformation: {
            ...demandACHPayment[storeId].achInformation,
            typeChecked: TYPE_CARD.SAVINGS,
            checked: demandACHPayment[storeId].achInformation.gridData?.filter(
              item => item.code === typeChecked
            )
          }
        }
      });
    });

    it('setIsInvalidACH', () => {
      const state = reducer(
        demandACHPayment,
        setIsInvalidACH({ storeId, value: true })
      );

      expect(state[storeId].isInvalidACH).toBe(true);
      expect(state).toEqual({
        [storeId]: {
          ...demandACHPayment[storeId],
          isInvalidACH: true
        }
      });
    });
  });
});
