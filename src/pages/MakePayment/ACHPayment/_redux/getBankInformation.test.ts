import './reducers';

import { getBankInformationACH } from './getBankInformation';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { BankInfoACHArg } from '../types';
import { storeId, responseDefault } from 'app/test-utils';

import paymentService from '../paymentService';

const requestData: BankInfoACHArg = {
  storeId,
  accountId: storeId,
  memberSequenceIdentifier: '6859'
};

const demandACHPayment = { [storeId]: {} };

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

const data = {
  customerInquiry: [
    {
      currentBalanceAmount: '5643',
      customerRoleTypeCode: 'S',
      savingsAccountIdentifier: '12345',
      demandDepositAccountIdentifier: '{"maskedValue":"123445"}'
    }
  ],
  cardholderInfo: [
    {
      savingsAccountNumber: '{"mockField":"123445"}'
    }
  ]
};

let store: Store<RootState>;
let state: RootState;

const initialState: Partial<RootState> = {
  demandACHPayment: {
    [storeId]: {}
  }
};

beforeEach(() => {
  store = createStore(rootReducer, initialState);
  state = store.getState();
});

describe('redux-store > getBankInformationACH', () => {
  it('with initialState', async () => {
    spy = jest
      .spyOn(paymentService, 'getBankInformationACH')
      .mockResolvedValue({ ...responseDefault, data });

    const store = createStore(rootReducer, initialState);
    const response = await getBankInformationACH(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ ...responseDefault, data });
  });

  it('success', async () => {
    spy = jest
      .spyOn(paymentService, 'getBankInformationACH')
      .mockResolvedValue({ ...responseDefault, data });

    const store = createStore(rootReducer, { demandACHPayment });
    const response = await getBankInformationACH(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ ...responseDefault, data });
  });

  it('success with empty data', async () => {
    // mock
    jest.spyOn(paymentService, 'getBankInformationACH').mockResolvedValue({
      ...responseDefault,
      data: { customerInquiry: [], cardholderInfo: [] }
    });

    const store = createStore(rootReducer, { demandACHPayment });
    const response = await getBankInformationACH(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      ...responseDefault,
      data: { customerInquiry: [], cardholderInfo: [] }
    });
  });

  it('should response getBankInformationACH pending', () => {
    const pendingAction = getBankInformationACH.pending(
      getBankInformationACH.pending.type,
      requestData
    );
    const actual = rootReducer(state, pendingAction);

    expect(
      actual.demandACHPayment[storeId].achInformation?.bankInfo?.loadingBankInfo
    ).toBe(true);
  });

  it('reject', async () => {
    spy = jest
      .spyOn(paymentService, 'getBankInformationACH')
      .mockRejectedValue({});

    const store = createStore(rootReducer, {});
    const response = await getBankInformationACH(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toBeUndefined();
  });
});
