import { storeId } from 'app/test-utils';
import { useSelectorMock } from 'app/test-utils/useSelectorMock';
import * as makeSelector from 'pages/MakePayment/ACHPayment/_redux/selectors';
import { TYPE_CARD, TYPE_TRANSACTION } from '../constants';

const initialState: Partial<RootState> = {
  schedulePayment: {
    [storeId]: {
      isInvalidSchedule: true
    }
  },
  demandACHPayment: {
    [storeId]: {
      isInvalidACH: false,
      noRegister: false,
      loading: false,
      openModal: false,
      isVerify: false,
      loadingForm: false,
      verifyInfo: {},
      reload: false,
      dataEdit: { bankRoutingNumber: 'string' },
      mode: 'Add',
      allCheckedList: ['ky-1'],
      checkedList: ['ky-1'],
      data: {
        bankAccounts: [{ accountNumber: {}, accountType: {} }],
        bankInformation: {}
      },
      dataInfo: {
        data: {
          fee: 10,
          amountInfo: [],
          transactionType: []
        }
      },
      delete: {
        loading: true,
        data: {
          accountId: storeId,
          accountTypeCode: 'code'
        }
      },
      transactionType: 'type',
      amount: {},
      achInformation: {
        isError: true,
        typeChecked: TYPE_CARD.SAVINGS,
        bankInfo: {
          data: { transitRoutingIdentifier: 'transitRoutingIdentifier' }
        },
        cardInfo: {
          data: {
            fileKe: 'fileKe',
            institutionTypeCode: 'institutionTypeCode',
            branchCode: 'branchCode',
            routingNumber: 12,
            institutionFullName: 'institutionFullName',
            institutionNameAbbreviation: 'institutionNameAbbreviation',
            institutionCity: 'institutionCity',
            institutionState: 'institutionState'
          }
        },
        gridData: [{ code: 'code', typeCard: 'S', accountNumber: '5265' }]
      }
    }
  }
};

const generateSelectorMock = () => (action: Function) => {
  const data = useSelectorMock(action, initialState, storeId);
  const noData = useSelectorMock(
    action,
    { demandACHPayment: {}, schedulePayment: {} },
    storeId
  );

  return { data, noData };
};

describe('Test selector state', () => {
  it('getDataPost', () => {
    const schedulePaymentDate = new Date();
    jest.spyOn(global, 'Date').mockImplementation(() => schedulePaymentDate);

    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.getDataPost);

    expect(noData).toEqual({
      accountNumberMaskedValue: undefined,
      amount: {},
      crdNbrEValue: undefined,
      fee: undefined,
      method: 'Bank',
      transactionType: undefined,
      schedulePaymentDate: new Date()
    });

    expect(data).toEqual({
      schedulePaymentDate: new Date(),
      accountNumberMaskedValue: undefined,
      method: 'Bank',
      amount: {},
      transactionType: undefined
    });
  });

  it('getIsLoadingMakePayment', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.getIsLoadingMakePayment);

    expect(noData).toBeFalsy();
    const { loading } = initialState.demandACHPayment![storeId];
    expect(data).toEqual(loading);
  });
  it('selectAllCheckedList', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectAllCheckedList);

    expect(noData).toEqual([]);
    expect(data).toEqual(
      initialState.demandACHPayment![storeId].allCheckedList
    );
  });
  it('selectCheckedList', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectCheckedList);

    expect(noData).toEqual([]);
    expect(data).toEqual(initialState.demandACHPayment![storeId].checkedList);
  });
  it('selectBankAccounts', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectBankAccounts);

    expect(noData).toEqual([]);
    expect(data).toEqual(
      initialState.demandACHPayment![storeId].data!.bankAccounts
    );
  });

  it('selectNoRegister', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectNoRegister);

    expect(noData).toBeFalsy();
    expect(data).toEqual(initialState.demandACHPayment![storeId].noRegister);
  });

  it('selectLoading', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectLoading);

    expect(noData).toBeFalsy();
    expect(data).toEqual(initialState.demandACHPayment![storeId].loading);
  });

  it('selectOpenModal', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectOpenModal);

    expect(noData).toBeFalsy();
    expect(data).toEqual(initialState.demandACHPayment![storeId].openModal);
  });

  it('selectVerifyModal', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectVerifyModal);

    expect(noData).toBeFalsy();
    expect(data).toEqual(initialState.demandACHPayment![storeId].isVerify);
  });

  it('selectVerifyInfo', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectVerifyInfo);

    expect(noData).toEqual({});
    expect(data).toEqual(initialState.demandACHPayment![storeId].verifyInfo);
  });

  it('selectReload', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectReload);

    expect(noData).toBeFalsy();
    expect(data).toEqual(initialState.demandACHPayment![storeId].reload);
  });

  it('selectACHData', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectACHData);

    expect(noData).toEqual({});
    expect(data).toEqual(initialState.demandACHPayment![storeId].data);
  });

  it('selectDataEdit', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectDataEdit);

    expect(noData).toEqual({
      bankRoutingNumber: '',
      checkingAccountNumber: '',
      savingsAccountNumber: ''
    });
    expect(data).toEqual({});
  });

  it('selectMode', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectMode);

    expect(noData).toEqual('');
    expect(data).toEqual(initialState.demandACHPayment![storeId].mode);
  });

  it('selectBankInfo', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectBankInfo);

    expect(noData).toEqual({});
    expect(data).toEqual(
      initialState.demandACHPayment![storeId].data!.bankInformation
    );
  });

  it('selectAmount', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectAmount);

    expect(noData).toEqual({});
    const { amount } = initialState.demandACHPayment![storeId];
    expect(data).toEqual(amount);
  });

  it('selectAmountInfo', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectAmountInfo);

    expect(noData).toEqual([]);
    const { amountInfo } =
      initialState.demandACHPayment![storeId].dataInfo?.data || {};

    expect(data).toEqual(amountInfo);
  });

  it('selectTransaction', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectTransaction);

    expect(noData).toEqual(TYPE_TRANSACTION.CHECKING);
    expect(data).toEqual(TYPE_TRANSACTION.SAVINGS);
  });

  it('selectTypeTransaction', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectTypeTransaction);

    expect(noData).toBeUndefined();
    expect(data).toEqual(
      initialState.demandACHPayment![storeId].transactionType
    );
  });

  it('selectTransactions', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectTransactions);

    expect(noData).toEqual([]);
    const { transactionType } =
      initialState.demandACHPayment![storeId].dataInfo?.data || {};

    expect(data).toEqual(transactionType);
  });

  it('selectFeeInfo', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectFeeInfo);

    expect(noData).toEqual(0);
    const { fee } =
      initialState.demandACHPayment![storeId].dataInfo?.data || {};
    expect(data).toEqual(fee);
  });

  it('selectLoadingDelete', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectLoadingDelete);

    expect(noData).toBeFalsy();
    expect(data).toEqual(
      initialState.demandACHPayment![storeId].delete!.loading
    );
  });

  it('selectLoadingForm', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectLoadingForm);

    expect(noData).toBeFalsy();
    expect(data).toEqual(initialState.demandACHPayment![storeId].loadingForm);
  });

  it('selectRoutingNumber', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectRoutingNumber);

    expect(noData).toBeFalsy();
    expect(data).toEqual(
      initialState.demandACHPayment![storeId].achInformation!.bankInfo!.data!
        .transitRoutingIdentifier
    );
  });

  it('selectViewDataACH', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectViewDataACH);

    expect(noData).toBeFalsy();
    expect(data).toEqual(
      initialState.demandACHPayment![storeId].achInformation!.cardInfo?.data
    );
  });

  it('selectBankInfoData', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectBankInfoData);

    expect(noData).toBeFalsy();
    expect(data).toEqual(
      initialState.demandACHPayment![storeId].achInformation!.bankInfo?.data
    );
  });

  it('selectGridData', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectGridData);

    expect(noData).toEqual([]);
    expect(data).toEqual(
      initialState.demandACHPayment![storeId].achInformation!.gridData
    );
  });

  it('selectTypeCard', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectTypeCard);

    expect(noData).toBeFalsy();
    expect(data).toEqual(
      initialState.demandACHPayment![storeId].achInformation!.typeChecked
    );
  });

  it('selectBankName', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectBankName);
    expect(noData).toBeFalsy();
    expect(data).toEqual(
      initialState.demandACHPayment![storeId].achInformation?.cardInfo?.data
        ?.institutionFullName
    );
  });

  it('selectBalanceAmount ', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectBalanceAmount);
    expect(noData).toBeFalsy();
    expect(data).toEqual(
      initialState.demandACHPayment![storeId].dataInfo?.data
        ?.currentBalanceAmount
    );
  });

  it('selectIsInvalidSchedule', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectIsInvalidSchedule);
    expect(noData).toBeFalsy();
    expect(data).toEqual(
      initialState.schedulePayment![storeId].isInvalidSchedule
    );
  });

  it('selectIsInvalidACH', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectIsInvalidACH);
    expect(noData).toBeFalsy();
    expect(data).toEqual(initialState.demandACHPayment![storeId].isInvalidACH!);
  });

  it('selectIsErrorACH', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectIsErrorACH);
    expect(noData).toBeFalsy();
    expect(data).toEqual(
      initialState.demandACHPayment![storeId].achInformation!.isError!
    );
  });
});
