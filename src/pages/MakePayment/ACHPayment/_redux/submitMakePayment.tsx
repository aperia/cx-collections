import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';
import { CodeErrorType } from 'app/constants/enums';
import { ACHPaymentInitialState } from '../types';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// types
import { SubmitMakePaymentArg } from '../types';
import paymentService from '../paymentService';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { ACH_ACTION } from 'pages/Memos/constants';
import { formatCommon } from 'app/helpers';
import { formatMaskValueToPost } from 'pages/MakePayment/SchedulePayment/helpers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { CHRONICLE_PAYMENT_KEY } from 'pages/ClientConfiguration/Memos/constants';

export const submitMakePayment = createAsyncThunk<
  void,
  SubmitMakePaymentArg,
  ThunkAPIConfig
>(
  'achPayment/submitMakePayment',
  async (args: SubmitMakePaymentArg, { dispatch, getState }) => {
    try {
      const { accountId, data, onSuccess, storeId } = args;
      const { amount = { value: 0 }, transactionType } = data;
      const states = getState();

      const achInfo = states.demandACHPayment[storeId]?.achInformation;
      const bankNameACH =
        states.demandACHPayment[storeId]?.achInformation?.cardInfo?.data
          ?.institutionFullName;
      const routingNumberACH =
        achInfo?.cardInfo?.data?.routingNumber ||
        achInfo?.bankInfo?.data?.transitRoutingIdentifier;

      const postData = {
        common: {
          user: 'string',
          cycle: 'M',
          debug: false,
          org: 'string',
          processor: false,
          contentType: 'string',
          retry: 0,
          timeout: 0,
          cid: 'string',
          eventOrigin: 'string',
          eventOriginInputName: 'string',
          externalCustomerId: 'string',
          presentationId: 'string',
          accountId
        },
        paymentAmount: parseFloat(amount?.value as string).toFixed(2),
        withdrawalType: transactionType
      };
      const response = await paymentService.submitACHMakePayment(postData);
      batch(() => {
        dispatch(
          apiErrorNotificationAction.clearApiErrorData({
            storeId: storeId,
            forSection: 'inModalBody'
          })
        );
        dispatch(
          memoActions.postMemo(ACH_ACTION.SUBMIT, {
            storeId,
            accEValue: accountId,
            chronicleKey: CHRONICLE_PAYMENT_KEY,
            routingNumberACH,
            bankNameACH,
            paymentAmtACH: formatCommon(`${data.amount?.value}`).currency(2),
            acctACH: formatMaskValueToPost(data.accountNumberMaskedValue),
            paypointACH: '_'
          })
        );
      });

      onSuccess && onSuccess(response);
    } catch (error) {
      const { onFailure } = args;

      const responseCode = error?.data?.coreResponse?.responseCode;

      if (responseCode === CodeErrorType.ValidationException) {
        const message = error?.data?.coreResponse?.message;
        batch(() => {
          dispatch(
            actionsToast.addToast({
              show: true,
              type: 'error',
              message: message
            })
          );
        });
      }
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId: args.storeId,
          forSection: 'inModalBody',
          apiResponse: error
        })
      );
      onFailure && onFailure(error);

      throw error;
    }
  }
);

export const submitMakePaymentBuilder = (
  builder: ActionReducerMapBuilder<ACHPaymentInitialState>
) => {
  builder.addCase(submitMakePayment.pending, (draftState, action) => {
    const { storeId } = action.meta.arg;

    draftState[storeId] = {
      ...draftState[storeId],
      submitMakePayment: {
        ...draftState[storeId]?.submitMakePayment,
        loading: true
      }
    };
  });
  builder.addCase(submitMakePayment.fulfilled, (draftState, action) => {
    const { storeId } = action.meta.arg;

    draftState[storeId] = {
      ...draftState[storeId],
      submitMakePayment: {
        ...draftState[storeId]?.submitMakePayment,
        loading: false,
        error: false
      }
    };
  });
  builder.addCase(submitMakePayment.rejected, (draftState, action) => {
    const { storeId } = action.meta.arg;

    draftState[storeId] = {
      ...draftState[storeId],
      submitMakePayment: {
        ...draftState[storeId]?.submitMakePayment,
        loading: false
      }
    };
  });
};
