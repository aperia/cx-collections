export const ACH_PAYMENT_FORM = {
  bankRoutingNumber: {
    name: 'bankRoutingNumber',
    label: 'txt_bank_routing_number',
    minLength: 9,
    maxLength: 9
  },
  checkingAccountNumber: {
    name: 'checkingAccountNumber',
    label: 'txt_checking_account_number',
    minLength: 5,
    maxLength: 17
  },
  savingsAccountNumber: {
    name: 'savingsAccountNumber',
    label: 'txt_savings_account_number',
    minLength: 5,
    maxLength: 17
  }
};

export const MESSAGE_FORM = {
  bankRoutingNumber: {
    required: 'txt_bank_routing_number_is_required',
    length: 'Bank Routing Number must be 9 digits.'
  },
  checkingAccountNumber: {
    required: 'txt_checking_account_number_is_required',
    length: 'Checking Account Number must be at least 5 digits.'
  },
  savingsAccountNumber: {
    required: 'txt_saving_account_number_is_required',
    length: 'Savings Account Number must be at least 5 digits.'
  }
};

export const AccountType = {
  checking: 'C',
  savings: 'S'
};

export const ModeType = {
  edit: 'Edit',
  add: 'Add'
};

export const METHOD = {
  bank: 'Bank',
  card: 'Card'
};

export const OTHER_CODE = 'OA';

// METHOD CONSTANTS

import { renderMaskedValue } from 'app/helpers';
import { PAYMENT_CARD_TYPE } from './types';
import { BankAccount } from 'pages/MakePayment/ACHPayment/types';

export const GRID_ID_ACH = 'accountType.code';
export const PATH_CODE = 'accountType.code';

export const CONFIG_BANK_INFORMATION = [
  {
    id: 'type',
    Header: 'TYPE',
    accessor: (item: BankAccount) => {
      const { code } = item.accountType || {};
      switch (code) {
        case PAYMENT_CARD_TYPE.CHECKING:
          return 'Checking';
        case PAYMENT_CARD_TYPE.SAVING:
        default:
          return 'Savings';
      }
    },
    width: 130
  },
  {
    id: 'accountNumber',
    Header: 'ACCOUNT NUMBER',
    accessor: (item: BankAccount) => {
      const { maskedValue } = item.accountNumber || {};
      return renderMaskedValue(maskedValue || '');
    },
    width: 160,
    autoWidth: true
  }
];

export const GROUP = [
  {
    codes: [PAYMENT_CARD_TYPE.CHECKING, PAYMENT_CARD_TYPE.SAVING] as string[],
    key: [PAYMENT_CARD_TYPE.CHECKING, PAYMENT_CARD_TYPE.SAVING].join('-'),
    header: '',
    columns: CONFIG_BANK_INFORMATION
  }
];

export const TYPE_CARD = {
  CHECKING: {
    value: 'Checking',
    description: 'txt_checking'
  },
  SAVINGS: {
    value: 'Savings',
    description: 'txt_savings'
  }
};

export const TYPE_TRANSACTION = {
  CHECKING: [
    {
      code: '1',
      text: 'txt_personal_checking'
    },
    {
      code: '3',
      text: 'txt_business_checking'
    }
  ],
  SAVINGS: [
    {
      code: '2',
      text: 'txt_personal_savings'
    },
    {
      code: '4',
      text: 'txt_business_savings'
    }
  ]
};
