import { AxiosResponse } from 'axios';
import { ReactText } from 'react';

export interface BankAccount {
  accountNumber?: {
    maskedValue?: string;
    eValue?: string;
    value?: string | null;
  };
  accountType?: {
    code?: string;
    description?: string;
  };
}

export interface ACHPaymentData {
  bankInformation?: {
    routingNumber?: {
      maskValue?: string;
      eValue?: string;
      value?: string | null;
    };
    bankName?: string;
    bankCity?: string;
    bankState?: string;
  };
  bankAccounts?: BankAccount[];
}

export interface PaymentAmountView {
  id?: string;
  title?: string;
  value?: ReactText;
  label?: string;
  disabled?: boolean;
}

export interface VerifyInfo {
  bankRoutingNumber?: string | number;
  checkingAccountNumber?: string | number;
  savingsAccountNumber?: string | number;
  bankCity?: string;
  bankName?: string;
  bankState?: string;
  institutionFullName?: React.ReactText;
}

export interface DataEditType {
  bankRoutingNumber: string;
  checkingAccountNumber?: string;
  savingsAccountNumber?: string;
}

export interface TransactionTypeData {
  code?: string;
  text?: string;
}
export interface ACHPaymentInfoData {
  currentBalanceAmount?: number;
  amountInfo?: PaymentAmountView[];
  transactionType?: TransactionTypeData[];
  fee?: number;
}

export interface ACHPaymentInfoResponse {
  customerInquiry: CustomerInquiry[];
}

export interface CustomerInquiry {
  cycleToDateUnpaidBilledPaymentDueAmount: string;
  cycleToDateNonDelinquentMinimumPaymentDueAmount: string;
  lastStatementBalanceAmount: string;
  currentBalanceAmount: string;
  nextPaymentDueDate: string;
  pastDueAmount: string;
  minimumPaymentAmount: string;
  savingsAccountIdentifier?: string;
}

// delete ACH

export interface DeleteACHPaymentPostData {
  accountId: string;
  accountTypeCode: string;
}

export interface DeleteACHArg {
  storeId: string;
  postData: DeleteACHPaymentPostData;
}

// verify account
export interface VerifyACHArg {
  storeId: string;
  postData: ACHPostData;
}

export interface ACHPostData {
  accountId: string;
  bankRoutingNumber?: string;
  checkingAccountNumber?: string;
  savingsAccountNumber?: string;
  bankName?: string;
}

export interface VerifyACHPostData {
  routingNumber?: ReactText;
}
export interface AddACHPostData {
  common: {
    cycle: string;
    accountId: string;
  };
  demandDepositAccount?: ReactText;
  transitRoutingAccount?: ReactText;
  savingsAccount?: ReactText;
}

export interface GetACHBankPostData extends CommonAccountIdRequestBody { }

export interface ACHBankAccountArg {
  storeId: string;
  postData: {
    accountId: string;
    memberSequenceIdentifier?: string;
  };
}

export interface DataEditPayload {
  data: DataEditType;
}

export interface ACHPaymentArg {
  storeId: string;
  postData: CommonAccountIdRequestBody;
}

export interface ACHPaymentPayload {
  data: ACHPaymentData;
}

export interface ACHPaymentInfoPayload {
  data: ACHPaymentInfoResponse;
}

export interface AmountData {
  id?: string;
  label?: string;
  title?: string;
  value?: ReactText;
}

export interface AllCheckedListPayload {
  storeId: string;
  allCheckedList: string[];
}

export interface CheckedListPayload {
  storeId: string;
  checkedList: string[];
}

//
export interface UpdateOpenModal {
  storeId: string;
  openModal: boolean;
  mode?: string;
}

export interface UpdateReload {
  storeId: string;
  reload: boolean;
}

export interface UpdateVerifyModal {
  storeId: string;
  isVerify: boolean;
}

export interface SubmitACHMakePaymentPostRequest {
  transactionType?: string;
  accountId?: string;
  crdNbrEValue?: string;
  accountNumberMaskedValue?: string;
  amount?: ReactText;
  schedulePaymentDate?: Date;
  fee?: ReactText;
}
export interface SubmitACHMakePaymentData {
  transactionType?: string;
  accountId?: string;
  crdNbrEValue?: string;
  accountNumberMaskedValue?: string;
  amount?: PaymentAmountView;
  schedulePaymentDate?: Date | string;
  fee?: ReactText;
}

export interface SubmitMakePaymentArg {
  storeId: string;
  accountId: string;
  data: SubmitACHMakePaymentData;
  onSuccess?: (response: AxiosResponse<MagicKeyValue>) => void;
  onFailure?: (error: MagicKeyValue) => void;
}

export interface VerifyACHResponseData {
  financialPaymentInfo: {
    branchCode?: ReactText;
    fileKey?: ReactText;
    institutionCity?: ReactText;
    institutionFullName?: ReactText;
    institutionNameAbbreviation?: ReactText;
    institutionState?: ReactText;
    institutionTypeCode?: ReactText;
    routingNumber?: ReactText;
  };
}
export interface VerifyACHPayload {
  data: VerifyACHResponseData;
}

export enum PAYMENT_CARD_TYPE {
  CHECKING = 'C',
  SAVING = 'S',
  DEBIT = 'D',
  CREDIT = 'CR'
}

// integrate API ach

export interface CardInfoACHArg {
  storeId: string;
  routingNumber: string;
}

export interface CardInfoACHPayload {
  data: {
    financialPaymentInfo: CardInfoACHData;
  };
}

export interface BankInfoACHArg {
  storeId: string;
  accountId: string;
  memberSequenceIdentifier?: string;
}

export interface BankInfoACHPayload {
  data: {
    customerInquiry: BankInfoACHData[];
    cardholderInfo: { savingsAccountNumber: string }[]
  };
}
export interface BankInfoACHData {
  currentBalanceAmount?: string;
  cycleToDateNonDelinquentMinimumPaymentDueAmount?: string;
  cycleToDateUnpaidBilledPaymentDueAmount?: string;
  customerExternalIdentifier?: string;
  customerRoleTypeCode?: string;
  demandDepositAccountIdentifier?: string;
  lastStatementBalanceAmount?: string;
  memberSequenceIdentifier?: string;
  nextPaymentDueDate?: string;
  presentationInstrumentIdentifier?: string;
  presentationInstrumentReplacementSequenceNumber?: string;
  presentationInstrumentTypeCode?: string;
  plasticReplacementIndicator?: string;
  savingsAccountIdentifier?: string;
  transitRoutingIdentifier?: string;
}

export interface CardInfoACHData {
  fileKey?: string;
  institutionTypeCode?: string;
  branchCode?: string;
  routingNumber?: string;
  institutionFullName?: string;
  institutionNameAbbreviation?: string;
  institutionCity?: string;
  institutionState?: string;
}

export interface ACHPayment {
  isInvalidACH?: boolean;
  amount?: PaymentAmountView;
  transactionType?: string;
  transactionCode?: string;
  allCheckedList?: string[];
  checkedList?: string[];
  loading?: boolean;
  loadingForm?: boolean;
  data?: ACHPaymentData;
  openModal?: boolean;
  noRegister?: boolean;
  isVerify?: boolean;
  verifyInfo?: VerifyInfo;
  reload?: boolean;
  dataEdit?: DataEditType;
  mode?: string;
  dataInfo?: {
    loading?: boolean;
    data?: ACHPaymentInfoData;
  };
  submitMakePayment?: {
    loading?: boolean;
    error?: boolean;
  };
  delete?: {
    loading?: boolean;
    data?: DeleteACHPaymentPostData;
    openModal?: boolean;
  };
  achInformation?: {
    isError?: boolean;
    gridData?: GridDataType[];
    typeChecked?: string;
    checked?: GridDataType[];
    bankInfo?: {
      loadingBankInfo?: boolean;
      data?: BankInfoACHData;
    };
    cardInfo?: {
      loadingCardInfo?: boolean;
      data?: CardInfoACHData;
    };
  };
}

export interface GridDataType {
  code: string;
  typeCard: string;
  accountNumber: string;
}

export interface UpdateChecked {
  storeId: string;
  typeChecked: string;
}

export interface ACHPaymentInitialState {
  [storeId: string]: ACHPayment;
}
