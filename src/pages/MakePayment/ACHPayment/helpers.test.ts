import { validation, disabledButton, formatFormData } from './helpers';

import { ACH_PAYMENT_FORM, MESSAGE_FORM, ModeType } from './constants';

describe('helpers', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('validation', () => {
    let result;
    let base;

    // ACH_PAYMENT_FORM.bankRoutingNumber
    base = ACH_PAYMENT_FORM.bankRoutingNumber;

    result = validation(base.name, '', true, true);
    expect(result).toEqual({
      [base.name]: MESSAGE_FORM.bankRoutingNumber.required
    });

    result = validation(base.name, base.name.slice(0, base.minLength - 1), true, true);
    expect(result).toEqual({
      [base.name]: MESSAGE_FORM.bankRoutingNumber.length
    });

    result = validation(base.name, `${base.name}_string`, true, true);
    expect(result).toEqual({ [base.name]: undefined });

    // ACH_PAYMENT_FORM.checkingAccountNumber
    base = ACH_PAYMENT_FORM.checkingAccountNumber;
    result = validation(base.name, base.name.slice(0, base.minLength - 1), true, true);
    expect(result).toEqual({
      [base.name]: MESSAGE_FORM.checkingAccountNumber.length
    });

    result = validation(base.name, '', true, false);
    expect(result).toEqual({ [base.name]: MESSAGE_FORM.checkingAccountNumber.required });

    result = validation(base.name, `${base.name}_string`, true, true);
    expect(result).toEqual({ [base.name]: undefined });

    // ACH_PAYMENT_FORM.savingsAccountNumber
    base = ACH_PAYMENT_FORM.savingsAccountNumber;
    result = validation(base.name, base.name.slice(0, base.minLength - 1), true, true);
    expect(result).toEqual({
      [base.name]: MESSAGE_FORM.savingsAccountNumber.length
    });

    result = validation(base.name, '', false, true);
    expect(result).toEqual({ [base.name]: MESSAGE_FORM.savingsAccountNumber.required });

    result = validation(base.name, `${base.name}_string`, true, true);
    expect(result).toEqual({ [base.name]: undefined });

    // default
    result = validation('default', '', true, true);
    expect(result).toBeUndefined();
  });

  it('disabledButton', () => {
    let isDisabled = disabledButton({}, ModeType.edit);
    expect(isDisabled).toBeFalsy();

    isDisabled = disabledButton({}, ModeType.add);
    expect(isDisabled).toBeTruthy();

    isDisabled = disabledButton({ ky: 'value' }, ModeType.add);
    expect(isDisabled).toBeTruthy();

    isDisabled = disabledButton({ ky: undefined }, ModeType.add);
    expect(isDisabled).toBeFalsy();
  });

  it('formatFormData', () => {
    const data = {
      bankRoutingNumber: '123',
      checkingAccountNumber: '123',
      savingsAccountNumber: '123',
      test: ''
    };

    const formData = formatFormData(data);
    expect(formData).toEqual({
      bankRoutingNumber: '123',
      checkingAccountNumber: '123',
      savingsAccountNumber: '123'
    });
  });

  it('formatFormData with empty Date', () => {
    const data = {};

    const formData = formatFormData(data);
    expect(formData).toEqual({});
  });
});
