import isEmpty from 'lodash.isempty';
import { DataFormType } from './ModalACH/Form';
import { ACH_PAYMENT_FORM, MESSAGE_FORM, ModeType } from './constants';

export const validation = (name: string, value: string, showChecking: boolean, showSavings: boolean) => {
  switch (name) {
    case ACH_PAYMENT_FORM.bankRoutingNumber.name:
      const bank = ACH_PAYMENT_FORM.bankRoutingNumber.name;

      if (!value) return { [bank]: MESSAGE_FORM.bankRoutingNumber.required };

      if (value && value.length < ACH_PAYMENT_FORM.bankRoutingNumber.minLength)
        return { [bank]: MESSAGE_FORM.bankRoutingNumber.length };

      return { [bank]: undefined };
    case ACH_PAYMENT_FORM.checkingAccountNumber.name:
      const checking = ACH_PAYMENT_FORM.checkingAccountNumber.name;

      if (!value && !showSavings) return { [checking]: MESSAGE_FORM.checkingAccountNumber.required };

      if (
        value &&
        value.length < ACH_PAYMENT_FORM.checkingAccountNumber.minLength
      )
        return { [checking]: MESSAGE_FORM.checkingAccountNumber.length };

      return { [checking]: undefined };
    case ACH_PAYMENT_FORM.savingsAccountNumber.name:
      const saving = ACH_PAYMENT_FORM.savingsAccountNumber.name;

      if (!value && !showChecking) return { [saving]: MESSAGE_FORM.savingsAccountNumber.required };

      if (
        value &&
        value.length < ACH_PAYMENT_FORM.savingsAccountNumber.minLength
      )
        return { [saving]: MESSAGE_FORM.savingsAccountNumber.length };

      return { [saving]: undefined };
    default:
      return;
  }
};

export const disabledButton = (message: MagicKeyValue, mode: string) => {
  if (isEmpty(message) && mode === ModeType.edit) return false;
  if (isEmpty(message)) return true;
  let disable = false;
  for (const key in message) {
    if (message[key] !== undefined) disable = true;
  }
  return disable;
};

export const formatFormData = (formData: DataFormType) => {
  let postData = {};
  for (const [key, value] of Object.entries(formData)) {
    if (!isEmpty(value)) {
      postData = { ...postData, [key]: value };
    }
  }
  return postData;
};
