import paymentService from './paymentService';

// utils
import { mockApiServices, storeId } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import {
  DeleteACHPaymentPostData,
  GetACHBankPostData,
  AddACHPostData,
  VerifyACHPostData
} from './types';

describe('paymentService', () => {
  describe('submitACHMakePayment', () => {
    const params: MagicKeyValue = {
      common: { accountId: storeId }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      paymentService.submitACHMakePayment(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      paymentService.submitACHMakePayment(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.submitACHMakePayment,
        params
      );
    });
  });

  describe('deleteACHBankAccount', () => {
    const params: DeleteACHPaymentPostData = {
      accountId: storeId,
      accountTypeCode: ''
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      paymentService.deleteACHBankAccount(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      paymentService.deleteACHBankAccount(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.deleteACHBankAccount,
        params
      );
    });
  });

  describe('verifyACHBankAccount', () => {
    const params: VerifyACHPostData = {
      routingNumber: 123
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      paymentService.verifyACHBankAccount(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      paymentService.verifyACHBankAccount(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.verifyACHBankAccount,
        params
      );
    });
  });

  describe('addACHBankAccount', () => {
    const params: AddACHPostData = {
      common: { cycle: 'cycle', accountId: storeId }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      paymentService.addACHBankAccount(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      paymentService.addACHBankAccount(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.addACHBankAccount,
        params
      );
    });
  });

  describe('getACHBankAccount', () => {
    const params: GetACHBankPostData = {
      common: { accountId: storeId }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      paymentService.getACHBankAccount(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      paymentService.getACHBankAccount(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.getACHBankAccount,
        params
      );
    });
  });

  describe('updateACHBankAccount', () => {
    const params: MagicKeyValue = {
      common: { accountId: storeId }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      paymentService.updateACHBankAccount(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      paymentService.updateACHBankAccount(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.updateACHBankAccount,
        params
      );
    });
  });

  describe('getACHPaymentInformation', () => {
    const params: CommonAccountIdRequestBody = {
      common: { accountId: storeId }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      paymentService.getACHPaymentInformation(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      paymentService.getACHPaymentInformation(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.getDetailPaymentInfo,
        params
      );
    });
  });

  describe('getACHPayment', () => {
    const params: CommonAccountIdRequestBody = {
      common: { accountId: storeId }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      paymentService.getACHPayment(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      paymentService.getACHPayment(params);

      expect(mockService).toBeCalledWith(apiUrl.payment.getACHPayment, params);
    });
  });

  describe('getCardInformationACH', () => {
    const params: CommonAccountIdRequestBody = {
      common: { accountId: storeId }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      paymentService.getCardInformationACH(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      paymentService.getCardInformationACH(params);

      expect(mockService).toBeCalledWith(apiUrl.payment.getCardInfoACH, params);
    });
  });

  describe('getBankInformationACH', () => {
    const params: CommonAccountIdRequestBody = {
      common: { accountId: storeId }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      paymentService.getBankInformationACH(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      paymentService.getBankInformationACH(params);

      expect(mockService).toBeCalledWith(apiUrl.payment.getBankInfoACH, params);
    });
  });
});
