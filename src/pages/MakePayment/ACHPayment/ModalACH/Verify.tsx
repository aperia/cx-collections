import React from 'react';
import { View } from 'app/_libraries/_dof/core';

// components
import { Icon } from 'app/_libraries/_dls/components';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// selector
import { selectVerifyInfo } from 'pages/MakePayment/ACHPayment/_redux/selectors';

// types
import { VerifyInfo } from 'pages/MakePayment/ACHPayment/types';
import { genAmtId } from 'app/_libraries/_dls/utils';

const Verify: React.FC = () => {
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const testId = 'account-details_make-payment-modal_ACH-payment_demand-modal_body';

  const verifyInfo = useStoreIdSelector<VerifyInfo>(selectVerifyInfo);
  return (
    <div className="px-24">
      <div
        className="text-center pt-24"
        data-testid={genAmtId(testId, 'method_information_verified', '')}
      >
        <Icon color="green" name={'success'} className="fs-60" />
        <p className="color-green mt-8">
          {t('txt_method_information_verified')}
        </p>
      </div>
      <div className="border bg-light-l20 br-light-l04 br-radius-8 px-16 pb-16 mt-24">
        <View
          descriptor="demandPaymentInformationView"
          id={`demandPaymentInformationView-${storeId}`}
          formKey={`demandPaymentInformationView-${storeId}`}
          value={verifyInfo}
        />
      </div>
    </div>
  );
};

export default Verify;
