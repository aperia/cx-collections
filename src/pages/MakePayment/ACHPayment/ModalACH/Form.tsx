import React, { useState } from 'react';

// components
import {
  Button,
  Icon,
  InlineMessage,
  Popover,
  TextBox
} from 'app/_libraries/_dls/components';

// constants
import { ACH_PAYMENT_FORM } from '../constants';

// helper
import classNames from 'classnames';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import pickBy from 'lodash.pickby';
import isUndefined from 'lodash.isundefined';
import isEmpty from 'lodash.isempty';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';
import { useAccountDetail } from 'app/hooks';

export interface DataFormType {
  bankRoutingNumber?: number | string;
  checkingAccountNumber?: number | string;
  savingsAccountNumber?: number | string;
}

export interface MessageErrorType {
  bankRoutingNumber?: string | undefined;
  checkingAccountNumber?: string | undefined;
  savingsAccountNumber?: string | undefined;
}

export interface FormProps {
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleBlur: (event: React.FocusEvent<HTMLInputElement>) => void;
  errorMessage: MessageErrorType;
  dataForm: DataFormType;
  isErrorVerify: boolean;
}

const Form: React.FC<FormProps> = ({
  handleChange,
  handleBlur,
  errorMessage,
  dataForm,
  isErrorVerify
}) => {
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const [active, setActive] = useState<boolean>(false);
  const testId =
    'account-details_make-payment-modal_ACH-payment_demand-modal_body_form';

  const showCheckingAccount = checkPermission(
    PERMISSIONS.MAKE_PAYMENT_UPDATE_CHECKING_ACCOUNT,
    storeId
  );
  const showSavingsAccount = checkPermission(
    PERMISSIONS.MAKE_PAYMENT_UPDATE_SAVINGS_ACCOUNT,
    storeId
  );

  return (
    <>
      <div className="px-24 pt-16">
        <div className="d-flex align-items-center">
          <h5 className="mr-8" data-testid={genAmtId(testId, 'title', '')}>
            {t('txt_make_payment_bank_info')}
          </h5>
          <Popover
            placement="right"
            size="md"
            onVisibilityChange={opened => setActive(opened)}
            element={
              <div data-testid={genAmtId(testId, 'info-popup', '')}>
                <img
                  height="151"
                  className="w-100"
                  src="images/bankach.png"
                  alt="Bank ACH"
                  data-testid={genAmtId(testId, 'info-popup-img', '')}
                />
                <p
                  className="mt-16 fw-500"
                  data-testid={genAmtId(testId, 'info-popup-message', '')}
                >
                  {t('txt_make_payment_ach_bank_aba')}
                </p>
                <p
                  className="mt-8"
                  data-testid={genAmtId(testId, 'info-popup-desc', '')}
                >
                  {t('txt_make_payment_ach_desc')}
                </p>
              </div>
            }
          >
            <Button
              className={classNames({ active })}
              variant="icon-secondary"
              size="sm"
              dataTestId={genAmtId(testId, 'info-popup-info-btn', '')}
            >
              <Icon name="information"></Icon>
            </Button>
          </Popover>
        </div>
        <div className="row mt-16">
          <div className="col-6">
            <TextBox
              name={ACH_PAYMENT_FORM.bankRoutingNumber.name}
              label={t(ACH_PAYMENT_FORM.bankRoutingNumber.label)}
              maxLength={ACH_PAYMENT_FORM.bankRoutingNumber.maxLength}
              minLength={ACH_PAYMENT_FORM.bankRoutingNumber.minLength}
              value={dataForm.bankRoutingNumber}
              required
              onChange={handleChange}
              onBlur={handleBlur}
              error={{
                message: errorMessage.bankRoutingNumber as string,
                status: !!errorMessage.bankRoutingNumber
              }}
              dataTestId={genAmtId(
                testId,
                ACH_PAYMENT_FORM.bankRoutingNumber.name,
                ''
              )}
            />
          </div>
        </div>
      </div>
      <div className="py-16 px-24 bg-light-l20 border-top border-bottom mt-16">
        <p data-testid={genAmtId(testId, 'ach_checking-label', '')}>
          {showCheckingAccount &&
            showSavingsAccount &&
            t('txt_make_payment_ach_checking_and_savings')}
          {showCheckingAccount &&
            !showSavingsAccount &&
            t('txt_make_payment_ach_checking')}
          {!showCheckingAccount &&
            showSavingsAccount &&
            t('txt_make_payment_ach_savings')}
        </p>
        {isErrorVerify && (
          <InlineMessage
            className="mb-0 mt-16"
            variant="danger"
            dataTestId={genAmtId(testId, 'ach_checking-warning', '')}
          >
            {t('txt_enter_at_least_one_field')}
          </InlineMessage>
        )}
        <div className="row mt-16">
          {showCheckingAccount && (
            <div className="col-6">
              <TextBox
                name={ACH_PAYMENT_FORM.checkingAccountNumber.name}
                label={t(ACH_PAYMENT_FORM.checkingAccountNumber.label)}
                maxLength={ACH_PAYMENT_FORM.checkingAccountNumber.maxLength}
                minLength={ACH_PAYMENT_FORM.checkingAccountNumber.minLength}
                value={dataForm.checkingAccountNumber}
                required={!showSavingsAccount}
                onChange={handleChange}
                onBlur={handleBlur}
                error={{
                  message: errorMessage.checkingAccountNumber || '',
                  status:
                    isErrorVerify ||
                    !isEmpty(errorMessage.checkingAccountNumber)
                }}
                errorTooltipProps={
                  isErrorVerify && isEmpty(errorMessage.checkingAccountNumber)
                    ? pickBy(
                        {
                          opened: false
                        },
                        item => !isUndefined(item)
                      )
                    : undefined
                }
                dataTestId={genAmtId(
                  testId,
                  ACH_PAYMENT_FORM.checkingAccountNumber.name,
                  ''
                )}
              />
            </div>
          )}

          {showSavingsAccount && (
            <div className="col-6">
              <TextBox
                name={ACH_PAYMENT_FORM.savingsAccountNumber.name}
                label={t(ACH_PAYMENT_FORM.savingsAccountNumber.label)}
                maxLength={ACH_PAYMENT_FORM.savingsAccountNumber.maxLength}
                minLength={ACH_PAYMENT_FORM.savingsAccountNumber.minLength}
                value={dataForm.savingsAccountNumber}
                required={!showCheckingAccount}
                onChange={handleChange}
                onBlur={handleBlur}
                error={{
                  message: errorMessage.savingsAccountNumber || '',
                  status:
                    isErrorVerify || !isEmpty(errorMessage.savingsAccountNumber)
                }}
                errorTooltipProps={
                  isErrorVerify && isEmpty(errorMessage.savingsAccountNumber)
                    ? pickBy(
                        {
                          opened: false
                        },
                        item => !isUndefined(item)
                      )
                    : undefined
                }
                dataTestId={genAmtId(
                  testId,
                  ACH_PAYMENT_FORM.savingsAccountNumber.name,
                  ''
                )}
              />
            </div>
          )}
        </div>
      </div>
    </>
  );
};

export default Form;
