import React from 'react';

// components
import Form from './Form';
import Verify from './Verify';

// types
import { DataFormType, MessageErrorType } from './Form';

// hooks
import { useStoreIdSelector } from 'app/hooks';

// selector
import { selectVerifyModal } from 'pages/MakePayment/ACHPayment/_redux/selectors';

export interface ModalBodyProps {
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  handleBlur: (event: React.FocusEvent<HTMLInputElement>) => void;
  errorMessage: MessageErrorType;
  dataForm: DataFormType;
  isErrorVerify: boolean;
}

const ModalBody: React.FC<ModalBodyProps> = ({
  handleChange,
  handleBlur,
  errorMessage,
  dataForm,
  isErrorVerify
}) => {
  const isVerify = useStoreIdSelector<boolean>(selectVerifyModal);
  return (
    <>
      {isVerify ? (
        <Verify />
      ) : (
        <Form
          handleChange={handleChange}
          handleBlur={handleBlur}
          errorMessage={errorMessage}
          dataForm={dataForm}
          isErrorVerify={isErrorVerify}
        />
      )}
    </>
  );
};

export default ModalBody;
