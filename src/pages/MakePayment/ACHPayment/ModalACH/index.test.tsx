import React from 'react';

// redux
import { demandACHPaymentAction as actions } from '../_redux/reducers';

// Component
import DemandPaymentForm from './index';

// Helper
import { renderMockStore, storeId, mockActionCreator } from 'app/test-utils';

// Hooks
import { AccountDetailProvider } from 'app/hooks';
import { act, fireEvent, screen } from '@testing-library/react';
import { ModalBodyProps } from './ModalBody';
import {
  ModalFooterProps,
  ModalHeaderProps
} from 'app/_libraries/_dls/components';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import * as entitlementsHelpers from 'app/entitlements/helpers';
import { PERMISSIONS } from 'app/entitlements/constants';

const actionSpy = mockActionCreator(actions);
const apiErrorNotificationActionSpy = mockActionCreator(
  apiErrorNotificationAction
);

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');
  const { ModalHeader, ModalFooter } = dlsComponents;

  return {
    ...dlsComponents,
    ModalHeader: (props: ModalHeaderProps) => {
      const { onHide } = props;
      return (
        <>
          <div data-testid="onHide" onClick={() => onHide && onHide()} />
          <ModalHeader {...props} />
        </>
      );
    },
    ModalFooter: (props: ModalFooterProps) => {
      const { onOk } = props;
      return (
        <>
          <div data-testid="onOk" onClick={() => onOk && onOk()} />
          <ModalFooter {...props} />
        </>
      );
    }
  };
});

jest.mock('./ModalBody', () => (props: ModalBodyProps) => {
  return (
    <>
      <input
        name="savingsAccountNumber"
        data-testid="handleBlur"
        onBlur={props.handleBlur}
      />
      <input
        name="savingsAccountNumber"
        data-testid="handleChange"
        onChange={props.handleChange}
      />
    </>
  );
});

const initialState = {
  demandACHPayment: {
    [storeId]: {
      isVerify: true,
      dataEdit: { bankRoutingNumber: '1' },
      openModal: true
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <DemandPaymentForm />
    </AccountDetailProvider>,
    { initialState }
  );
};

beforeEach(() => {
  jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
});
describe('Render', () => {
  it('Render UI', () => {
    const expectText = I18N_COMMON_TEXT.ADD_PAYMENT_METHOD;

    renderWrapper(initialState);

    expect(screen.getByText(expectText)).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('Actions handleCloseModal with Verify', () => {
    const mockUpdateVerifyModal = jest
      .fn()
      .mockImplementation(() => ({ type: 'type' }));
    jest
      .spyOn(actions, 'updateVerifyModal')
      .mockImplementation(mockUpdateVerifyModal);

    renderWrapper(initialState);

    // trigger hide modal
    screen.getByTestId('onHide').click();
    expect(mockUpdateVerifyModal).toBeCalledWith({ storeId, isVerify: false });
  });

  it('Actions handleCloseModal without Verify', () => {
    const mockUpdateVerifyModal = jest
      .fn()
      .mockImplementation(() => ({ type: 'type' }));
    jest
      .spyOn(actions, 'updateVerifyModal')
      .mockImplementation(mockUpdateVerifyModal);
    const mockUpdateOpenModal = jest
      .fn()
      .mockImplementation(() => ({ type: 'type' }));
    jest
      .spyOn(actions, 'updateOpenModal')
      .mockImplementation(mockUpdateOpenModal);
    renderWrapper({
      demandACHPayment: {
        [storeId]: {
          dataEdit: { bankRoutingNumber: '1' },
          isVerify: false,
          openModal: true
        }
      }
    });

    // trigger hide modal
    screen.getByTestId('onHide').click();
    expect(mockUpdateVerifyModal).toBeCalledWith({ storeId, isVerify: false });
    expect(mockUpdateOpenModal).toBeCalledWith({ storeId, openModal: false });
  });

  it('Actions handleEntryForm', () => {
    const mockUpdateVerifyModal = jest
      .fn()
      .mockImplementation(() => ({ type: 'type' }));
    jest
      .spyOn(actions, 'updateVerifyModal')
      .mockImplementation(mockUpdateVerifyModal);
    const expectAction = { storeId, isVerify: false };
    renderWrapper({
      demandACHPayment: {
        [storeId]: {
          isVerify: true,
          dataEdit: { bankRoutingNumber: '1' },
          openModal: true
        }
      }
    });
    expect(mockUpdateVerifyModal).not.toBeCalled();

    // check [Confirm Bank Account Information]
    screen.getByLabelText('txt_confirm_payment_method_information').click();

    // handleEntryForm
    screen.getByText(I18N_COMMON_TEXT.BACK_TO_ENTRY_FORM).click();

    expect(mockUpdateVerifyModal).toBeCalledWith(expectAction);
  });

  it('disable button ok for modal', () => {
    renderWrapper({
      demandACHPayment: {
        [storeId]: {
          mode: 'Edit',
          openModal: true
        }
      }
    });
    let buttonVerify = screen.getByRole('button', { name: 'txt_verify' });
    expect(buttonVerify).toBeDisabled();
    renderWrapper({
      demandACHPayment: {
        [storeId]: {
          mode: 'Add',
          openModal: true
        }
      }
    });
    buttonVerify = screen.getByRole('button', { name: 'txt_verify' });
    expect(buttonVerify).toBeDisabled();
  });

  it('show error from api correctly', () => {
    const openApiDetailErrorModalSpy = apiErrorNotificationActionSpy(
      'openApiDetailErrorModal'
    );
    renderWrapper({
      demandACHPayment: {
        [storeId]: {
          mode: 'Edit',
          openModal: true
        }
      },
      apiErrorNotification: {
        [storeId]: {
          errorDetail: {
            inAddAndEditPaymentMethodModal: {
              response: 'test',
              request: 'test'
            }
          }
        }
      }
    });
    expect(screen.getByText('txt_api_error')).toBeInTheDocument();

    const linkElement = screen.getByText('txt_view_detail');
    fireEvent.click(linkElement);

    expect(openApiDetailErrorModalSpy).toBeCalledWith({
      forSection: 'inAddAndEditPaymentMethodModal',
      storeId
    });
  });

  it('form data work correctly', () => {
    renderWrapper({
      demandACHPayment: {
        [storeId]: {
          mode: 'Edit',
          openModal: true
        }
      }
    });
    const inputOnChange = screen.getByTestId('handleChange');
    const inputOnBlur = screen.getByTestId('handleBlur');

    act(() => {
      fireEvent.change(inputOnChange, {
        target: { name: 'form', value: '123' }
      });
      fireEvent.blur(inputOnBlur, {
        target: { name: 'form', value: 'test-error' }
      });
    });
    expect(inputOnChange).toHaveValue('123');
    expect(inputOnBlur).toHaveValue('test-error');
    act(() => {
      fireEvent.change(inputOnChange, {
        target: { name: 'form', value: 'abc' }
      });
    });
    expect(inputOnChange).toHaveValue('abc');
  });

  it('submit form with verifyACHPayment when required checkingAccountNumber', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockImplementation((permission) => {
      return PERMISSIONS.MAKE_PAYMENT_UPDATE_SAVINGS_ACCOUNT !== permission;
    });

    const verifyACHPaymentSpy = actionSpy('verifyACHPayment');
    renderWrapper({
      demandACHPayment: {
        [storeId]: {
          mode: 'Edit',
          openModal: true,
          isVerify: false,
          achInformation: {
            bankInfo: {
              data: {
                transitRoutingIdentifier: '123'
              }
            }
          }
        }
      }
    });

    const inputOnChange = screen.getByTestId('handleChange');
    act(() => {
      fireEvent.change(inputOnChange, {
        target: { name: 'name', value: '123' }
      });
    });
    const btnVerify = screen.getByRole('button', { name: 'txt_verify' });
    fireEvent.click(btnVerify);
    expect(verifyACHPaymentSpy).not.toBeCalled();
  });

  it('submit form with verifyACHPayment when required savingsAccountNumber', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockImplementation((permission) => {
      return PERMISSIONS.MAKE_PAYMENT_UPDATE_CHECKING_ACCOUNT !== permission;
    });

    const verifyACHPaymentSpy = actionSpy('verifyACHPayment');
    renderWrapper({
      demandACHPayment: {
        [storeId]: {
          mode: 'Edit',
          openModal: true,
          isVerify: false,
          achInformation: {
            bankInfo: {
              data: {
                transitRoutingIdentifier: '123'
              }
            }
          }
        }
      }
    });

    const inputOnChange = screen.getByTestId('handleChange');
    act(() => {
      fireEvent.change(inputOnChange, {
        target: { name: 'name', value: '123' }
      });
    });
    const btnVerify = screen.getByRole('button', { name: 'txt_verify' });
    fireEvent.click(btnVerify);
    expect(verifyACHPaymentSpy).not.toBeCalled();
  });

  it('submit form with verifyACHPayment with savingsAccountNumber', () => {
    const verifyACHPaymentSpy = actionSpy('verifyACHPayment');
    renderWrapper({
      demandACHPayment: {
        [storeId]: {
          mode: 'Edit',
          openModal: true,
          isVerify: false,
          achInformation: {
            bankInfo: {
              data: {
                transitRoutingIdentifier: '123'
              }
            }
          }
        }
      }
    });

    const inputOnChange = screen.getByTestId('handleChange');
    act(() => {
      fireEvent.change(inputOnChange, {
        target: { name: 'name', value: '123' }
      });
    });
    const btnVerify = screen.getByRole('button', { name: 'txt_verify' });
    fireEvent.click(btnVerify);
    expect(verifyACHPaymentSpy).not.toBeCalled();
  });

  it('submit form with verifyACHPayment work correctly', () => {
    const verifyACHPaymentSpy = actionSpy('verifyACHPayment');
    renderWrapper({
      demandACHPayment: {
        [storeId]: {
          mode: 'Edit',
          openModal: true,
          isVerify: false,
          achInformation: {
            bankInfo: {
              data: {
                transitRoutingIdentifier: '123'
              }
            }
          }
        }
      }
    });

    const inputOnChange = screen.getByTestId('handleChange');
    act(() => {
      fireEvent.change(inputOnChange, {
        target: { name: 'savingsAccountNumber', value: '123' }
      });
    });
    const btnVerify = screen.getByRole('button', { name: 'txt_verify' });
    fireEvent.click(btnVerify);

    expect(verifyACHPaymentSpy).toBeCalledWith({
      postData: {
        accountId: storeId,
        bankName: '',
        bankRoutingNumber: '123',
        savingsAccountNumber: '123'
      },
      storeId
    });
  });

  it('submit form with triggerAddACHPayment work correctly', () => {
    const triggerAddACHPaymentSpy = actionSpy('triggerAddACHPayment');
    renderWrapper({
      demandACHPayment: {
        [storeId]: {
          mode: 'Add',
          openModal: true,
          isVerify: true,
          achInformation: {
            bankInfo: {
              data: {
                transitRoutingIdentifier: '123'
              }
            }
          }
        }
      }
    });
    const inputOnChange = screen.getByTestId('handleChange');
    const confirmInput = screen.getByRole('checkbox', {
      name: 'txt_confirm_payment_method_information'
    });
    act(() => {
      fireEvent.change(inputOnChange, {
        target: { name: 'form', value: '123' }
      });
      fireEvent.click(confirmInput);
    });
    const btnAdd = screen.getByRole('button', { name: 'txt_add' });
    fireEvent.click(btnAdd);
    expect(triggerAddACHPaymentSpy).toBeCalledWith({
      postData: {
        accountId: storeId,
        bankName: '',
        form: '123'
      },
      storeId
    });
  });

  it('submit form with triggerUpdateACHPayment work correctly', () => {
    const triggerUpdateACHPaymentSpy = actionSpy('triggerUpdateACHPayment');
    renderWrapper({
      demandACHPayment: {
        [storeId]: {
          mode: 'Edit',
          openModal: true,
          isVerify: true,
          achInformation: {
            bankInfo: {
              data: {
                transitRoutingIdentifier: '123'
              }
            }
          }
        }
      }
    });
    const inputOnChange = screen.getByTestId('handleChange');
    const confirmInput = screen.getByRole('checkbox', {
      name: 'txt_confirm_payment_method_information'
    });
    act(() => {
      fireEvent.change(inputOnChange, {
        target: { name: 'form', value: '123' }
      });
      fireEvent.click(confirmInput);
    });
    const btnSave = screen.getByRole('button', { name: 'txt_save' });
    fireEvent.click(btnSave);
    expect(triggerUpdateACHPaymentSpy).toBeCalledWith({
      postData: {
        accountId: storeId,
        bankName: '',
        form: '123',
        bankRoutingNumber: '123'
      },
      storeId
    });
  });
});
