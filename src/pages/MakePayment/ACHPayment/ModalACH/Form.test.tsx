import React from 'react';

// Component
import Form from './Form';

// Helper
import { queryByClass, renderMockStore, storeId } from 'app/test-utils';

// Hooks
import { AccountDetailProvider } from 'app/hooks';
import { screen } from '@testing-library/react';
import { PopoverProps } from 'app/_libraries/_dls/components';
import * as entitlementsHelpers from 'app/entitlements/helpers';
import { PERMISSIONS } from 'app/entitlements/constants';

HTMLCanvasElement.prototype.getContext = jest.fn()

jest.mock('app/_libraries/_dls/components', () => {
  const { Popover } = jest.requireActual('app/_libraries/_dls/components');
  return {
    ...jest.requireActual('app/_libraries/_dls/components'),
    Popover: (props: PopoverProps) => (
      <>
        <div
          data-testid="onVisibilityChange"
          onClick={() => props.onVisibilityChange!(true)}
        />
        <Popover opened {...props} />
      </>
    )
  };
});

const renderWrapper = (isErrorVerify = false) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <Form
        handleChange={() => { }}
        handleBlur={() => { }}
        errorMessage={{}}
        dataForm={{ bankRoutingNumber: 0 }}
        isErrorVerify={isErrorVerify}
      />
    </AccountDetailProvider>,
    { initialState: {} }
  );
};

beforeEach(() => {
  jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
});
describe('Render UI', () => {
  it('Render UI', () => {
    renderWrapper();
    expect(screen.getByText('txt_make_payment_bank_info')).toBeInTheDocument();
  });
  it('Render UI with only checking acount field', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockImplementation((permission) => {
      return PERMISSIONS.MAKE_PAYMENT_UPDATE_SAVINGS_ACCOUNT !== permission;
    });
    renderWrapper();
    expect(screen.getByText('txt_make_payment_ach_checking')).toBeInTheDocument();
  });
  it('Render UI with only savings acount field', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockImplementation((permission) => {
      return PERMISSIONS.MAKE_PAYMENT_UPDATE_CHECKING_ACCOUNT !== permission;
    });
    renderWrapper();
    expect(screen.getByText('txt_make_payment_ach_savings')).toBeInTheDocument();
  });
  it('Render UI with error verify', () => {
    renderWrapper(true);
    expect(screen.getByText('txt_enter_at_least_one_field')).toBeInTheDocument();
  });
});
describe('Actions', () => {
  it('onVisibilityChange', () => {
    const { baseElement } = renderWrapper();

    // trigger onVisibilityChange(true)
    screen.getByTestId('onVisibilityChange').click();

    expect(queryByClass(baseElement, /active/)).toBeInTheDocument();
  });
});
