import React from 'react';

// Component
import ModalBody from './ModalBody';

// Helper
import { renderMockStore, storeId } from 'app/test-utils';

// Hooks
import { AccountDetailProvider } from 'app/hooks';
import { screen } from '@testing-library/react';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

HTMLCanvasElement.prototype.getContext = jest.fn();

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <ModalBody
        handleChange={() => {}}
        handleBlur={() => {}}
        errorMessage={{}}
        dataForm={{ bankRoutingNumber: 0 }}
      />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('Render UI', () => {
  it('Render UI With Form', () => {
    renderWrapper({});

    expect(screen.getByText('txt_make_payment_bank_info')).toBeInTheDocument();
  });

  it('Render UI With Verify', () => {
    renderWrapper({
      demandACHPayment: {
        [storeId]: {
          isVerify: true
        }
      }
    });

    expect(
      screen.getByText('txt_method_information_verified')
    ).toBeInTheDocument();
  });
});
