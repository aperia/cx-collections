import React, { useState } from 'react';

// components
import {
  Button,
  CheckBox,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import ModalBodyACH from './ModalBody';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// helpers
import { disabledButton, validation, formatFormData } from '../helpers';
import { stringValidate } from 'app/helpers';

// redux store
import { useDispatch } from 'react-redux';
import { demandACHPaymentAction as actions } from 'pages/MakePayment/ACHPayment/_redux/reducers';

// selector
import {
  selectBankName,
  selectDataEdit,
  selectLoadingForm,
  selectMode,
  selectOpenModal,
  selectVerifyModal
} from 'pages/MakePayment/ACHPayment/_redux/selectors';

// types
import { DataFormType, MessageErrorType } from './Form';
import { DataEditType } from 'pages/MakePayment/ACHPayment/types';

// constants
import { ModeType } from '../constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import isEmpty from 'lodash.isempty';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { useApiErrorNotification } from 'pages/ApiErrorNotification/hooks/useApiErrorNotification';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

export interface DemandPaymentFormProps {}

const DemandPaymentForm: React.FC<DemandPaymentFormProps> = () => {
  const dispatch = useDispatch();
  const { storeId, accEValue } = useAccountDetail();
  const { t } = useTranslation();
  const testId = 'account-details_make-payment-modal_ACH-payment_demand-modal';

  const openModal = useStoreIdSelector<boolean>(selectOpenModal);
  const isVerify = useStoreIdSelector<boolean>(selectVerifyModal);
  const loading = useStoreIdSelector<boolean>(selectLoadingForm);
  const dataDefault = useStoreIdSelector<DataEditType>(selectDataEdit);
  const modeType = useStoreIdSelector<string>(selectMode);
  const bankName = useStoreIdSelector<string>(selectBankName);

  const ApiErrorDetail = useApiErrorNotification({
    storeId,
    forSection: 'inAddAndEditPaymentMethodModal'
  });

  const [dataForm, setDataForm] = useState<DataFormType>(dataDefault);
  const [errorMessage, setErrorMessage] = useState<MessageErrorType>({});
  const [confirmInfo, setConfirmInfo] = useState<boolean>(false);
  const [isErrorVerify, setIsErrorVerify] = useState<boolean>(false);

  const showCheckingAccount = checkPermission(
    PERMISSIONS.MAKE_PAYMENT_UPDATE_CHECKING_ACCOUNT,
    storeId
  );
  const showSavingsAccount = checkPermission(
    PERMISSIONS.MAKE_PAYMENT_UPDATE_SAVINGS_ACCOUNT,
    storeId
  );

  const disabledOk =
    disabledButton(errorMessage, modeType) ||
    !dataForm.bankRoutingNumber ||
    (!showCheckingAccount && !dataForm.savingsAccountNumber) ||
    (!showSavingsAccount && !dataForm.checkingAccountNumber);

  const disabled = isVerify ? !confirmInfo : disabledOk;

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    if (!stringValidate(value).isNumber()) {
      return;
    }
    setDataForm({ ...dataForm, [name]: value.toString() });
  };

  const handleBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    const { name, value } = event.target;
    const error = validation(
      name,
      value,
      showCheckingAccount,
      showSavingsAccount
    );
    setErrorMessage({ ...errorMessage, ...error });
  };

  const handleCloseModal = () => {
    handleEntryForm();
    dispatch(actions.updateOpenModal({ storeId, openModal: false }));
  };

  const submitForm = () => {
    const postData = formatFormData(dataForm);

    const data = {
      storeId,
      postData: { accountId: accEValue, bankName, ...postData }
    };

    if (!isVerify) {
      if (
        showCheckingAccount &&
        showSavingsAccount &&
        isEmpty(dataForm?.checkingAccountNumber) &&
        isEmpty(dataForm?.savingsAccountNumber)
      ) {
        setIsErrorVerify(true);
        return;
      }
      setIsErrorVerify(false);
      dispatch(actions.verifyACHPayment(data));
      return;
    }

    if (modeType === ModeType.add) {
      dispatch(actions.triggerAddACHPayment(data));
      return;
    }

    dispatch(actions.triggerUpdateACHPayment(data));
  };

  const handleEntryForm = () => {
    dispatch(
      apiErrorNotificationAction.clearApiErrorData({
        storeId,
        forSection: 'inAddAndEditPaymentMethodModal'
      })
    );
    setConfirmInfo(false);
    dispatch(actions.updateVerifyModal({ storeId, isVerify: false }));
  };

  const textButton = modeType === ModeType.edit ? t('txt_save') : t('txt_add');

  return (
    <Modal sm show={openModal} loading={loading} dataTestId={testId}>
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId={genAmtId(testId, 'header', '')}
      >
        <ModalTitle dataTestId={genAmtId(testId, 'header-title', '')}>
          {modeType === ModeType.edit
            ? t('txt_edit_payment_method')
            : t(I18N_COMMON_TEXT.ADD_PAYMENT_METHOD)}
        </ModalTitle>
      </ModalHeader>
      <ModalBody noPadding>
        <ApiErrorDetail
          className="mt-24 ml-24"
          dataTestId={genAmtId(testId, 'body-api-error', '')}
        />
        <ModalBodyACH
          handleChange={handleChange}
          handleBlur={handleBlur}
          errorMessage={errorMessage}
          dataForm={dataForm}
          isErrorVerify={isErrorVerify}
        />
        {isVerify && (
          <div className="pl-24 pr-24">
            <CheckBox
              className="mt-16"
              dataTestId={genAmtId(testId, 'body-confirmInfoACH', '')}
            >
              <CheckBox.Input
                id="confirmInfoACH"
                checked={confirmInfo}
                onChange={event => setConfirmInfo(event.target.checked)}
              />
              <CheckBox.Label htmlFor="confirmInfoACH">
                {t('txt_confirm_payment_method_information')}
              </CheckBox.Label>
            </CheckBox>
          </div>
        )}
      </ModalBody>
      <ModalFooter
        okButtonText={isVerify ? textButton : t('txt_verify')}
        cancelButtonText={t('txt_cancel')}
        onCancel={handleCloseModal}
        disabledOk={disabled}
        onOk={submitForm}
        dataTestId={genAmtId(testId, 'footer', '')}
      >
        {isVerify && (
          <div className="flex-1 d-flex">
            <Button
              variant="outline-primary"
              className="ml-n8"
              onClick={handleEntryForm}
              dataTestId={genAmtId(testId, 'footer_back-to-entry-btn', '')}
            >
              {t(I18N_COMMON_TEXT.BACK_TO_ENTRY_FORM)}
            </Button>
          </div>
        )}
      </ModalFooter>
    </Modal>
  );
};

export default DemandPaymentForm;
