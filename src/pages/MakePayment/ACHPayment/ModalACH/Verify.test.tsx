import React from 'react';

// Component
import Verify from './Verify';

// Helper
import { renderMockStore, storeId } from 'app/test-utils';

// Hooks
import { AccountDetailProvider } from 'app/hooks';
import { screen } from '@testing-library/react';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

describe('Render UI', () => {
  it('Render UI', () => {
    renderMockStore(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <Verify />
      </AccountDetailProvider>
    );

    expect(
      screen.getByTestId('demandPaymentInformationView')
    ).toBeInTheDocument();
  });
});
