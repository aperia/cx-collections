import React from 'react';
import { act, fireEvent, screen } from '@testing-library/react';

import ACHPayment from '.';
import { mockActionCreator, renderMockStore, storeId } from 'app/test-utils';
import { AccountDetailProvider } from 'app/hooks';

// redux
import { demandACHPaymentAction } from './_redux/reducers';

jest.mock('./Method', () => () => <div>Payment Method</div>);
jest.mock('./Information', () => () => <div>Payment Information</div>);

const demandACHPaymentSpy = mockActionCreator(demandACHPaymentAction);

const initialState: Partial<RootState> = {
  demandACHPayment: {
    [storeId]: {
      // reload: true,
      achInformation: {
        bankInfo: { data: { transitRoutingIdentifier: '5642' } }
      }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>, id?: string) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <ACHPayment />
    </AccountDetailProvider>,
    { initialState }
  ).wrapper;
};

describe('Render', () => {
  it('Render UI with empty data', () => {
    const mockGetBankInformationACH = demandACHPaymentSpy(
      'getBankInformationACH'
    );

    renderWrapper({});

    expect(screen.getByText(/Payment Method/)).toBeInTheDocument();
    expect(screen.getByText(/Payment Information/)).toBeInTheDocument();
    expect(mockGetBankInformationACH).toBeCalledWith({
      storeId,
      accountId: storeId
    });
  });

  it('Render UI', () => {
    const mockGetCardInfoACH = demandACHPaymentSpy('getCardInfoACH');

    renderWrapper(initialState);

    expect(screen.getByText(/Payment Method/)).toBeInTheDocument();
    expect(screen.getByText(/Payment Information/)).toBeInTheDocument();
    expect(mockGetCardInfoACH).toBeCalledWith({
      storeId,
      routingNumber:
        initialState.demandACHPayment![storeId].achInformation?.bankInfo?.data
          ?.transitRoutingIdentifier
    });
  });

  it('Render UI when reload', () => {
    const mockGetBankInformationACH = demandACHPaymentSpy(
      'getBankInformationACH'
    );
    const mockGetCardInfoACH = demandACHPaymentSpy('getCardInfoACH');
    const mockUpdateReload = demandACHPaymentSpy('updateReload');

    renderWrapper({
      demandACHPayment: {
        [storeId]: {
          reload: true,
          achInformation: {
            bankInfo: { data: { transitRoutingIdentifier: '5642' } }
          }
        }
      }
    });

    expect(screen.getByText(/Payment Method/)).toBeInTheDocument();
    expect(screen.getByText(/Payment Information/)).toBeInTheDocument();
    expect(mockGetBankInformationACH).toBeCalledWith({
      storeId,
      accountId: storeId
    });
    expect(mockGetCardInfoACH).toBeCalledWith({
      storeId,
      routingNumber:
        initialState.demandACHPayment![storeId].achInformation?.bankInfo?.data
          ?.transitRoutingIdentifier
    });
    expect(mockUpdateReload).toBeCalledWith({ storeId, reload: false });
  });


  it('Render UI when error', () => {
    const mockGetBankInformationACH = demandACHPaymentSpy(
      'getBankInformationACH'
    );
    renderWrapper({
      demandACHPayment: {
        [storeId]: {
          reload: true,
          achInformation: {
            isError: true
          }
        }
      }
    });

    const btnReload = screen.getByRole('button', {name: /txt_reload/})

    act(() => {
      fireEvent.click(btnReload)
    })
    expect(mockGetBankInformationACH).toBeCalledWith({
      storeId,
      accountId: storeId
    });
  });

});
