import { apiService } from 'app/utils/api.service';

import {
  DeleteACHPaymentPostData,
  GetACHBankPostData,
  AddACHPostData,
  VerifyACHPostData,
} from './types';

const paymentService = {
  submitACHMakePayment(postData: MagicKeyValue) {
    const url = window.appConfig?.api?.payment?.submitACHMakePayment || '';
    return apiService.post(url, { ...postData });
  },
  deleteACHBankAccount(postData: DeleteACHPaymentPostData) {
    const url = window.appConfig?.api?.payment?.deleteACHBankAccount || '';
    return apiService.post(url, { ...postData });
  },
  verifyACHBankAccount(postData: VerifyACHPostData) {
    const url = window.appConfig?.api?.payment?.verifyACHBankAccount || '';
    return apiService.post(url, { ...postData });
  },
  addACHBankAccount(postData: AddACHPostData) {
    const url = window.appConfig?.api?.payment?.addACHBankAccount || '';
    return apiService.post(url, { ...postData });
  },
  getACHBankAccount(postData: GetACHBankPostData) {
    const url = window.appConfig?.api?.payment?.getACHBankAccount || '';
    return apiService.post(url, { ...postData });
  },
  updateACHBankAccount(postData: MagicKeyValue) {
    const url = window.appConfig?.api?.payment?.updateACHBankAccount || '';
    return apiService.post(url, { ...postData });
  },
  getACHPaymentInformation(postData: CommonAccountIdRequestBody) {
    const url = window.appConfig?.api?.payment?.getDetailPaymentInfo || '';
    return apiService.post(url, { ...postData });
  },
  getACHPayment(postData: CommonAccountIdRequestBody) {
    const url = window.appConfig?.api?.payment?.getACHPayment || '';
    return apiService.post(url, { ...postData });
  },
  getCardInformationACH(postData: MagicKeyValue) {
    const url = window.appConfig?.api?.payment?.getCardInfoACH || '';
    return apiService.post(url, { ...postData });
  },
  getBankInformationACH(postData: MagicKeyValue) {
    const url = window.appConfig?.api?.payment?.getBankInfoACH || '';
    return apiService.post(url, { ...postData });
  }
};

export default paymentService;
