import React, { useEffect, useLayoutEffect, useRef } from 'react';

// components
import Information from './Information';
import Method from './Method';

// redux
import { batch, useDispatch } from 'react-redux';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { demandACHPaymentAction as actions } from './_redux/reducers';
import {
  selectBankInfoData,
  selectIsErrorACH,
  selectReload,
  selectRoutingNumber
} from './_redux/selectors';

// helpers
import isEmpty from 'lodash.isempty';
import { FailedApiReload } from 'app/components';
import { genAmtId } from 'app/_libraries/_dls/utils';

interface ACHPaymentProps {}

const ACHPayment: React.FC<ACHPaymentProps> = () => {
  const dispatch = useDispatch();
  const { storeId, accEValue, memberSequenceIdentifier } = useAccountDetail();
  const divRef = useRef<HTMLDivElement | null>(null);

  const routingNumber = useStoreIdSelector<string>(selectRoutingNumber);
  const dataBankInfo = useStoreIdSelector(selectBankInfoData);
  const reload = useStoreIdSelector<boolean>(selectReload);
  const isError = useStoreIdSelector<boolean>(selectIsErrorACH);

  const testId = 'account-details_make-payment-modal_ACH-payment';

  // get bank information
  useEffect(() => {
    if (!isEmpty(dataBankInfo)) return;
    dispatch(
      actions.getBankInformationACH({
        storeId,
        accountId: accEValue,
        memberSequenceIdentifier
      })
    );
  }, [accEValue, dataBankInfo, dispatch, memberSequenceIdentifier, storeId]);

  // get card information
  useEffect(() => {
    if (isEmpty(routingNumber)) return;
    dispatch(
      actions.getCardInfoACH({
        storeId,
        routingNumber
      })
    );
  }, [dispatch, routingNumber, storeId]);

  // reload
  useEffect(() => {
    if (!reload) return;
    batch(() => {
      dispatch(
        actions.getBankInformationACH({
          storeId,
          accountId: accEValue,
          memberSequenceIdentifier
        })
      );
      dispatch(
        actions.updateReload({
          storeId,
          reload: false
        })
      );
    });
  }, [accEValue, dispatch, memberSequenceIdentifier, reload, storeId]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100% - 103px)`);
  }, []);

  const handleReload = () => {
    dispatch(
      actions.getBankInformationACH({
        storeId,
        accountId: accEValue,
        memberSequenceIdentifier
      })
    );
  };

  if (isError)
    return (
      <>
        <FailedApiReload
          id="ach_make_payment"
          className="mt-80"
          onReload={handleReload}
          dataTestId={genAmtId(testId, 'api-reload-failed', '')}
        />
      </>
    );

  return (
    <div className="d-flex" ref={divRef}>
      <Method />
      <Information />
    </div>
  );
};

export default ACHPayment;
