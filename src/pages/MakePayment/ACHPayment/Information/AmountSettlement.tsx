import React, { useEffect } from 'react';

// component
import {
  DropdownList,
  DropdownListItem,
  DropdownBaseChangeEvent
} from 'app/_libraries/_dls/components';

// redux
import { useDispatch } from 'react-redux';
import { selectTransaction, selectTypeTransaction } from '../_redux/selectors';
import { useStoreIdSelector, useAccountDetail } from 'app/hooks';
import { demandACHPaymentAction as actions } from '../_redux/reducers';

// type

import { PaymentAmountView } from '../types';

// helper
import { useTranslation } from 'app/_libraries/_dls/hooks';

import { getAcceptedBalanceAmount } from 'pages/MakePayment/SchedulePayment/_redux/selectors';
import { ID_OTHER_AMOUNT, OTHER_AMOUNT } from 'pages/MakePayment/constants';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { formatCommon } from 'app/helpers';

const AmountSettlement: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const { storeId } = useAccountDetail();

  const acceptedBalanceAmount = useStoreIdSelector<string | undefined>(
    getAcceptedBalanceAmount
  );

  // transactionType
  const transactionTypes = useStoreIdSelector<{ code: string; text: string }[]>(
    selectTransaction
  ).map(item => ({
    ...item,
    text: `${item.code} - ${t(item.text)}`
  }));
  const type = useStoreIdSelector(selectTypeTransaction);

  const testId = 'account-details_make-payment-modal_ACH-payment_info-amount';

  const handleChangeTransactionType = (event: DropdownBaseChangeEvent) => {
    const { value } = event.target;
    const code = transactionTypes.filter(item => item.text === value)[0]?.code;
    dispatch(actions.setTransactionType({ storeId, data: value, code }));
  };

  const handleBlurTransactionType = () => {
    if (type !== undefined) return;

    dispatch(
      actions.setTransactionType({
        storeId,
        data: '',
        code: ''
      })
    );
  };

  useEffect(() => {
    const amountPayment: PaymentAmountView = {
      ...OTHER_AMOUNT,
      value: acceptedBalanceAmount,
      id: ID_OTHER_AMOUNT
    };

    dispatch(
      actions.setAmount({
        storeId,
        amount: amountPayment
      })
    );
  }, [dispatch, storeId, acceptedBalanceAmount]);
  // useEffect(() => {
  //   if (!showEditAmount) return;
  //   const newStatus = getNewState();
  //   setStatusOtherAmount(newStatus);
  //   dispatch(actions.setIsInvalidACH({ storeId, value: !!newStatus.status }));
  // }, [dispatch, getNewState, selectedValue, showEditAmount, storeId]);

  return (
    <div className="mt-24">
      <h6 className="color-grey" data-testid={genAmtId(testId, 'title', '')}>
        {t('txt_amount')}
      </h6>
      <div className="d-flex mt-16">
        <p className="fs-14 color-grey-d20">
          {t('txt_accepted_balance_amount')}:
        </p>
        <p className="ml-4 fs-14 fw-500 color-grey-d20">
          {formatCommon(acceptedBalanceAmount!).currency(2)}
        </p>
      </div>
      <div className="row ">
        <div className="col-12 mt-16">
          <DropdownList
            textField="transactionType"
            name="transactionType"
            label={t('txt_transaction_type')}
            required
            error={{
              status: type === '',
              message: t('txt_transaction_type_is_required')
            }}
            id="transaction-type"
            value={type}
            onChange={handleChangeTransactionType}
            onBlur={handleBlurTransactionType}
            dataTestId={genAmtId(testId, 'transactionType', '')}
            noResult={t('txt_no_results_found')}
          >
            {transactionTypes.map(({ code, text }) => (
              <DropdownListItem key={code} label={text} value={text} />
            ))}
          </DropdownList>
        </div>
      </div>
    </div>
  );
};

export default AmountSettlement;
