import React, { useCallback, useEffect, useState } from 'react';

// component
import {
  Radio,
  DropdownList,
  DropdownListItem,
  NumericTextBox,
  DropdownBaseChangeEvent,
  OnChangeNumericEvent
} from 'app/_libraries/_dls/components';

// redux
import { useDispatch } from 'react-redux';
import {
  selectAmountInfo,
  selectAmount,
  selectTransaction,
  selectBalanceAmount,
  selectTypeTransaction
} from '../_redux/selectors';
import { useStoreIdSelector, useAccountDetail } from 'app/hooks';
import { demandACHPaymentAction as actions } from '../_redux/reducers';

// type

import { PaymentAmountView } from '../types';

// helper
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { getLabelView } from 'pages/MakePayment/helper';
import { OTHER_CODE } from '../constants';
import { genAmtId } from 'app/_libraries/_dls/utils';

const Amount: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const { storeId } = useAccountDetail();
  // amount
  const amounts = useStoreIdSelector<PaymentAmountView[]>(selectAmountInfo);
  const balanceAmount =
    useStoreIdSelector<number | undefined>(selectBalanceAmount);
  const selectedAmount = useStoreIdSelector<PaymentAmountView>(selectAmount);
  // transactionType
  const transactionTypes = useStoreIdSelector<{ code: string; text: string }[]>(
    selectTransaction
  ).map(item => ({
    ...item,
    text: `${item.code} - ${t(item.text)}`
  }));
  const type = useStoreIdSelector(selectTypeTransaction);

  const [statusOtherAmount, setStatusOtherAmount] =
    useState<{ status?: boolean; message: string }>();

  const { id: selectedCode, value: selectedValue, disabled } = selectedAmount;
  const testId = 'account-details_make-payment-modal_ACH-payment_info-amount';

  const handleSelectAmount = (amount: PaymentAmountView) => {
    const newAmount = { ...amount };

    if (amount.id === OTHER_CODE) {
      newAmount.value = undefined;
    }

    dispatch(actions.setAmount({ storeId, amount: newAmount }));
  };

  const handleOtherAmount = (event: OnChangeNumericEvent) => {
    const { value } = event.target;
    dispatch(
      actions.setAmount({ storeId, amount: { ...selectedAmount, value } })
    );
  };

  const handleChangeTransactionType = (event: DropdownBaseChangeEvent) => {
    const { value } = event.target;
    const code = transactionTypes.filter(item => item.text === value)[0]?.code;
    dispatch(actions.setTransactionType({ storeId, data: value, code }));
  };

  const handleBlurOtherAmount = () => {
    if (selectedValue !== undefined) return;

    dispatch(
      actions.setAmount({ storeId, amount: { ...selectedAmount, value: '' } })
    );
  };

  const handleBlurTransactionType = () => {
    if (type !== undefined) return;

    dispatch(
      actions.setTransactionType({
        storeId,
        data: '',
        code: ''
      })
    );
  };

  const showEditAmount = selectedCode === OTHER_CODE && !disabled;

  const getNewState = useCallback(() => {
    const isAmountEmpty = selectedValue === '';
    const is0 = `${Number(selectedValue)}` === '0';
    const isUnderAmountBalance =
      selectedValue !== undefined &&
      balanceAmount !== undefined &&
      selectedValue > balanceAmount;

    let message = '';

    if (isAmountEmpty) {
      message = 'txt_payment_amount_required';
    } else if (is0) {
      message = 'txt_payment_amount_greater_0';
    } else if (isUnderAmountBalance) {
      message = 'txt_other_amount_error';
    }

    return {
      status: isAmountEmpty || is0 || undefined || isUnderAmountBalance,
      message: t(message)
    };
  }, [balanceAmount, selectedValue, t]);

  useEffect(() => {
    if (!showEditAmount) return;
    const newStatus = getNewState();
    setStatusOtherAmount(newStatus);
    dispatch(actions.setIsInvalidACH({ storeId, value: !!newStatus.status }));
  }, [dispatch, getNewState, selectedValue, showEditAmount, storeId]);

  return (
    <div className="mt-24">
      <h6 className="color-grey" data-testid={genAmtId(testId, 'title', '')}>
        {t('txt_amount')}
      </h6>
      {amounts?.map((item, index) => {
        const { value, id, label, disabled } = item;
        const keyId = `ach-amount-${storeId}-${id}-${index}`;
        const labelView = getLabelView(t, label, value, id);
        return (
          <div className="d-flex mt-16" key={keyId}>
            <Radio
              id={keyId}
              dataTestId={genAmtId(`${label}_${testId}`, 'option', '')}
            >
              <Radio.Input
                name={`ach-amount-${storeId}`}
                disabled={disabled}
                value={value}
                checked={id === selectedCode && !disabled}
                onChange={() => handleSelectAmount(item)}
              />
              <Radio.Label title={labelView} htmlFor={id}>
                {labelView}
              </Radio.Label>
            </Radio>
          </div>
        );
      })}
      {showEditAmount && (
        <div className="pl-24 mt-16">
          <NumericTextBox
            required
            format="c2"
            maxLength={16}
            id={`paymentMount-${storeId}`}
            label={t('txt_payment_amount')}
            placeholder="0.00"
            error={statusOtherAmount}
            value={selectedValue}
            onChange={handleOtherAmount}
            onBlur={handleBlurOtherAmount}
            dataTestId={genAmtId(testId, 'paymentMount', '')}
          />
        </div>
      )}
      <div className="row ">
        <div className="col-12 mt-16">
          <DropdownList
            textField="transactionType"
            name="transactionType"
            label={t('txt_transaction_type')}
            required
            error={{
              status: type === '',
              message: t('txt_transaction_type_is_required')
            }}
            id="transaction-type"
            value={type}
            onChange={handleChangeTransactionType}
            onBlur={handleBlurTransactionType}
            dataTestId={genAmtId(testId, 'transactionType', '')}
            noResult={t('txt_no_results_found')}
          >
            {transactionTypes.map(({ code, text }) => (
              <DropdownListItem key={code} label={text} value={text} />
            ))}
          </DropdownList>
        </div>
      </div>
    </div>
  );
};

export default Amount;
