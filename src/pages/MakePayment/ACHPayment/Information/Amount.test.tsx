import React from 'react';
import { fireEvent, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

// Component
import Amount from './Amount';
import {
  DropdownBaseChangeEvent,
  DropdownListProps,
  NumericProps
} from 'app/_libraries/_dls/components';

// Helper
import { renderMockStore, storeId } from 'app/test-utils';

// Hooks
import { AccountDetailProvider } from 'app/hooks';
import { demandACHPaymentAction } from '../_redux/reducers';

const mockImp = () => ({ type: 'type' });

const dataInfo = {
  data: {
    amountInfo: [
      { id: 'OA', title: 'label', label: 'label' },
      { id: 'IA', title: 'label IA', label: 'label IA' }
    ],
    transactionType: [{ code: 'OA', text: 'transactionType' }],
    currentBalanceAmount: 1
  }
};

const initialState: Partial<RootState> = {
  demandACHPayment: { [storeId]: { dataInfo } }
};

const stateWithAmount: Partial<RootState> = {
  demandACHPayment: { [storeId]: { amount: { id: 'OA' }, dataInfo } }
};

const stateWithIsAmountEmpty: Partial<RootState> = {
  demandACHPayment: { [storeId]: { amount: { id: 'OA', value: '' }, dataInfo } }
};

const stateWithIs0: Partial<RootState> = {
  demandACHPayment: { [storeId]: { amount: { id: 'OA', value: '0' }, dataInfo } }
};

const stateWithIsUnderAmountBalance: Partial<RootState> = {
  demandACHPayment: { [storeId]: { amount: { id: 'OA', value: '123' }, dataInfo } }
};

jest.mock('app/_libraries/_dls/components', () => {
  return {
    ...(jest.requireActual('app/_libraries/_dls/components') as object),
    NumericTextBox: ({ onChange, onBlur, error }: NumericProps) => {
      return (
        <>
          <input
            name="savingsAccountNumber"
            data-testid="handleOtherAmount"
            onChange={onChange}
          />
          <input
            name="savingsAccountNumber"
            data-testid="handleBlurOtherAmount"
            onBlur={onBlur}
          />
          <p>{error?.status}</p>
          <p>{error?.message}</p>
        </>
      );
    },
    DropdownList: (props: DropdownListProps) => {
      const {
        onChange = (event: DropdownBaseChangeEvent) => undefined,
        onBlur = () => undefined
      } = props;
      return (
        <>
          <input data-testid="DropdownList_onChange" onChange={onChange} />
          <button data-testid="DropdownList_onBlur" onBlur={onBlur} />
        </>
      );
    }
  };
});

jest.mock('app/_libraries/_dls/components/DropdownBase/Item.tsx', () => () => {
  return <div></div>;
});

let spySubmitMakePayment: jest.SpyInstance;
let spySetAmount: jest.SpyInstance;
let spySetTransactionType: jest.SpyInstance;

afterEach(() => {
  spySubmitMakePayment?.mockReset();
  spySubmitMakePayment?.mockRestore();

  spySetAmount?.mockReset();
  spySetAmount?.mockRestore();

  spySetTransactionType?.mockReset();
  spySetTransactionType?.mockRestore();
});

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <Amount />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper(initialState);
    expect(screen.getByText('txt_amount')).toBeInTheDocument();
  });

  it('Render with empty amount', () => {
    renderWrapper(stateWithIsAmountEmpty);
    expect(screen.getByText('txt_payment_amount_required')).toBeInTheDocument();
  })

  it('Render with amount is 0', () => {
    renderWrapper(stateWithIs0);
    expect(screen.getByText('txt_payment_amount_greater_0')).toBeInTheDocument();
  })

  it('Render with amount with error', () => {
    renderWrapper(stateWithIsUnderAmountBalance);
    expect(screen.getByText('txt_other_amount_error')).toBeInTheDocument();
  })
});

describe('Actions', () => {
  it('handleSelectAmount', () => {
    const mockSetAmount = jest.fn().mockImplementation(mockImp);
    spySetAmount = jest
      .spyOn(demandACHPaymentAction, 'setAmount')
      .mockImplementation(mockSetAmount);

    renderWrapper(initialState);

    // check radio
    userEvent.click(screen.getAllByRole('radio')[1]);
    userEvent.click(screen.getAllByRole('radio')[0]);

    expect(mockSetAmount).toBeCalledWith({
      storeId,
      amount:
        initialState.demandACHPayment![storeId].dataInfo!.data!.amountInfo![0]
    });
  });

  it('handleOtherAmount', () => {
    const mockSetAmount = jest.fn().mockImplementation(mockImp);
    spySetAmount = jest
      .spyOn(demandACHPaymentAction, 'setAmount')
      .mockImplementation(mockSetAmount);

    renderWrapper(stateWithAmount);

    // typing
    userEvent.type(screen.getAllByTestId('handleOtherAmount')[0], '1');

    expect(mockSetAmount).toBeCalledWith({
      storeId,
      amount: { id: 'OA', value: '1' }
    });
  });

  it('handleBlurOtherAmount with amount value', () => {
    const mockSetAmount = jest.fn().mockImplementation(mockImp);
    spySetAmount = jest
      .spyOn(demandACHPaymentAction, 'setAmount')
      .mockImplementation(mockSetAmount);

    renderWrapper({
      demandACHPayment: {
        [storeId]: {
          amount: { id: 'OA', value: '1' },
          dataInfo: {
            data: {
              amountInfo: [
                { id: 'OA', title: 'label', label: 'label' },
                { id: 'IA', title: 'label IA', label: 'label IA' }
              ],
              transactionType: [{ code: 'OA', text: 'transactionType' }]
            }
          }
        }
      }
    });

    // typing
    fireEvent.blur(screen.getAllByTestId('handleBlurOtherAmount')[0]);

    expect(mockSetAmount).not.toBeCalled();
  });

  it('handleBlurOtherAmount', () => {
    const mockSetAmount = jest.fn().mockImplementation(mockImp);
    spySetAmount = jest
      .spyOn(demandACHPaymentAction, 'setAmount')
      .mockImplementation(mockSetAmount);

    renderWrapper(stateWithAmount);

    // typing
    fireEvent.blur(screen.getAllByTestId('handleBlurOtherAmount')[0]);

    expect(mockSetAmount).toBeCalledWith({
      storeId,
      amount: { id: 'OA', value: '' }
    });
  });

  it('handleChangeTransactionType', () => {
    const mockSetTransactionType = jest.fn().mockImplementation(mockImp);
    spySetTransactionType = jest
      .spyOn(demandACHPaymentAction, 'setTransactionType')
      .mockImplementation(mockSetTransactionType);

    renderWrapper(stateWithAmount);

    // click to dropdown item
    fireEvent.change(screen.getByTestId('DropdownList_onChange'), {
      target: { value: 'transactionType' }
    });

    fireEvent.blur(screen.getByTestId('DropdownList_onBlur'));

    expect(mockSetTransactionType).toBeCalledWith({
      storeId,
      data: 'transactionType'
    });
  });

  describe('handleBlurTransactionType', () => {
    it('with transactionType', () => {
      const mockSetTransactionType = jest.fn().mockImplementation(mockImp);
      spySetTransactionType = jest
        .spyOn(demandACHPaymentAction, 'setTransactionType')
        .mockImplementation(mockSetTransactionType);

      renderWrapper(stateWithAmount);

      // click to dropdown item
      fireEvent.change(screen.getByTestId('DropdownList_onChange'), {
        target: { value: 'transactionType' }
      });

      fireEvent.blur(screen.getByTestId('DropdownList_onBlur'));

      expect(mockSetTransactionType).toBeCalledWith({
        storeId,
        data: 'transactionType'
      });
    });

    it('without transactionType', () => {
      const mockSetTransactionType = jest.fn().mockImplementation(mockImp);
      spySetTransactionType = jest
        .spyOn(demandACHPaymentAction, 'setTransactionType')
        .mockImplementation(mockSetTransactionType);

      renderWrapper({
        demandACHPayment: { [storeId]: { transactionType: 'type' } }
      });

      fireEvent.blur(screen.getByTestId('DropdownList_onBlur'));

      expect(mockSetTransactionType).not.toBeCalled();
    });
  });
});
