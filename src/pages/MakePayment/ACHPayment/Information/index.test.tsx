import { screen, waitFor } from '@testing-library/react';
// Hooks
import { AccountDetailProvider } from 'app/hooks';
// Helper
import { renderMockStore, storeId } from 'app/test-utils';
import React from 'react';
// Component
import ACHInformation from './index';

HTMLCanvasElement.prototype.getContext = jest.fn();

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <ACHInformation />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('Render', () => {
  it('should render Amount if acceptedBalanceAmount === undefined', () => {
    renderWrapper({
      schedulePayment: {
        [storeId]: {}
      }
    });

    expect(screen.getByText('txt_paymentInformation')).toBeInTheDocument();
  });

  it('should render AmountSettlement if acceptedBalanceAmount has value', () => {
    renderWrapper({
      schedulePayment: {
        [storeId]: {
          acceptedBalanceAmount: '100'
        }
      }
    });

    waitFor(() =>
      expect(
        screen.findByText('txt_accepted_balance_amount')
      ).toBeInTheDocument()
    );
  });
});
