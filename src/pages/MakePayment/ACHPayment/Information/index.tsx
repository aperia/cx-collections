import React, { useEffect, useLayoutEffect, useRef } from 'react';

// components
import Amount from './Amount';

// redux store
import { demandACHPaymentAction as actions } from '../_redux/reducers';
import { selectLoading } from '../_redux/selectors';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helper
import classNames from 'classnames';
import { SimpleBar } from 'app/_libraries/_dls/components';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { useDispatch } from 'react-redux';
import { getAcceptedBalanceAmount } from 'pages/MakePayment/SchedulePayment/_redux/selectors';
import AmountSettlement from './AmountSettlement';

export interface ACHInformationProps {}
const ACHInformation: React.FC<ACHInformationProps> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const divRef = useRef<HTMLDivElement | null>(null);

  const { accEValue, storeId, memberSequenceIdentifier } = useAccountDetail();

  const loading = useStoreIdSelector<boolean>(selectLoading);
  const acceptedBalanceAmount = useStoreIdSelector<string | undefined>(
    getAcceptedBalanceAmount
  );
  const testId = 'account-details_make-payment-modal_ACH-payment_info';

  useEffect(() => {
    if (acceptedBalanceAmount) return;
    const postData = {
      common: { accountId: accEValue },
      inquiryFields: { memberSequenceIdentifier },
    };

    dispatch(actions.getACHPaymentInformation({ storeId, postData }));
  }, [
    accEValue,
    dispatch,
    storeId,
    memberSequenceIdentifier,
    acceptedBalanceAmount
  ]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.width = '356px');
  }, []);

  return (
    <div className={classNames({ loading })} ref={divRef}>
      <SimpleBar>
        <div className="p-24">
          <h5 data-testid={genAmtId(testId, 'title', '')}>
            {t('txt_paymentInformation')}
          </h5>
          {acceptedBalanceAmount === undefined ? (
            <Amount />
          ) : (
            <AmountSettlement />
          )}
        </div>
      </SimpleBar>
    </div>
  );
};

export default ACHInformation;
