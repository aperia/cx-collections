import { fireEvent, screen } from '@testing-library/react';
// Hooks
import { AccountDetailProvider } from 'app/hooks';
// Helper
import { renderMockStore, storeId } from 'app/test-utils';
import {
  DropdownBaseChangeEvent,
  DropdownListProps
} from 'app/_libraries/_dls/components';
import React from 'react';
import { demandACHPaymentAction } from '../_redux/reducers';
import AmountSettlement from './AmountSettlement';

const mockImp = () => ({ type: 'type' });

jest.mock('app/_libraries/_dls/components', () => {
  return {
    ...(jest.requireActual('app/_libraries/_dls/components') as object),
    DropdownList: (props: DropdownListProps) => {
      const {
        onChange = (event: DropdownBaseChangeEvent) => undefined,
        onBlur = () => undefined
      } = props;
      return (
        <>
          <input data-testid="DropdownList_onChange" onChange={onChange} />
          <button data-testid="DropdownList_onBlur" onBlur={onBlur} />
        </>
      );
    }
  };
});

jest.mock('app/_libraries/_dls/components/DropdownBase/Item.tsx', () => () => {
  return <div></div>;
});

let spySubmitMakePayment: jest.SpyInstance;
let spySetAmount: jest.SpyInstance;
let spySetTransactionType: jest.SpyInstance;

afterEach(() => {
  spySubmitMakePayment?.mockReset();
  spySubmitMakePayment?.mockRestore();

  spySetAmount?.mockReset();
  spySetAmount?.mockRestore();

  spySetTransactionType?.mockReset();
  spySetTransactionType?.mockRestore();
});

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <AmountSettlement />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper({
      schedulePayment: {
        [storeId]: {
          acceptedBalanceAmount: '100'
        }
      }
    });
    expect(screen.getByText('txt_amount')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleChangeTransactionType', () => {
    const mockSetTransactionType = jest.fn().mockImplementation(mockImp);
    spySetTransactionType = jest
      .spyOn(demandACHPaymentAction, 'setTransactionType')
      .mockImplementation(mockSetTransactionType);

    renderWrapper({
      schedulePayment: {
        [storeId]: {
          acceptedBalanceAmount: '100'
        }
      }
    });

    // click to dropdown item
    fireEvent.change(screen.getByTestId('DropdownList_onChange'), {
      target: { value: 'transactionType' }
    });

    fireEvent.blur(screen.getByTestId('DropdownList_onBlur'));

    expect(mockSetTransactionType).toBeCalledWith({
      storeId,
      data: 'transactionType'
    });
  });

  describe('handleBlurTransactionType', () => {
    it('with transactionType', () => {
      const mockSetTransactionType = jest.fn().mockImplementation(mockImp);
      spySetTransactionType = jest
        .spyOn(demandACHPaymentAction, 'setTransactionType')
        .mockImplementation(mockSetTransactionType);

      renderWrapper({
        schedulePayment: {
          [storeId]: {
            acceptedBalanceAmount: '100'
          }
        }
      });

      // click to dropdown item
      fireEvent.change(screen.getByTestId('DropdownList_onChange'), {
        target: { value: 'transactionType' }
      });

      fireEvent.blur(screen.getByTestId('DropdownList_onBlur'));

      expect(mockSetTransactionType).toBeCalledWith({
        storeId,
        data: 'transactionType'
      });
    });

    it('without transactionType', () => {
      const mockSetTransactionType = jest.fn().mockImplementation(mockImp);
      spySetTransactionType = jest
        .spyOn(demandACHPaymentAction, 'setTransactionType')
        .mockImplementation(mockSetTransactionType);

      renderWrapper({
        demandACHPayment: { [storeId]: { transactionType: 'type' } }
      });

      fireEvent.blur(screen.getByTestId('DropdownList_onBlur'));

      expect(mockSetTransactionType).not.toBeCalled();
    });
  });
});
