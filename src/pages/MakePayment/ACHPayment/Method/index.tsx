import React from 'react';

// components
import { Button, SimpleBar } from 'app/_libraries/_dls/components';
import NoRegisterAccount from './NoRegisterAccount';
import PaymentMethodDetail from './PaymentMethodDetail';
import DemandPaymentForm from '../ModalACH';

// redux store
import { useDispatch } from 'react-redux';
import { demandACHPaymentAction } from '../_redux/reducers';
// selector
import {
  selectLoading,
  selectNoRegister,
  selectOpenModal
} from '../_redux/selectors';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// helpers
import classNames from 'classnames';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { ModeType } from '../constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

export interface ACHMethodProps { }

const ACHMethod: React.FC<ACHMethodProps> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const { storeId } = useAccountDetail();
  const testId = 'account-details_make-payment-modal_ACH-payment_method';

  const noRegister = useStoreIdSelector<boolean>(selectNoRegister);
  const openModal = useStoreIdSelector<boolean>(selectOpenModal);
  const loading = useStoreIdSelector<boolean>(selectLoading);

  const showCheckingAccount = checkPermission(PERMISSIONS.MAKE_PAYMENT_UPDATE_CHECKING_ACCOUNT, storeId);
  const showSavingsAccount = checkPermission(PERMISSIONS.MAKE_PAYMENT_UPDATE_SAVINGS_ACCOUNT, storeId);

  const handleOpenModal = () => {
    dispatch(
      demandACHPaymentAction.updateOpenModal({
        storeId,
        openModal: true,
        mode: ModeType.edit
      })
    );
  };

  return (
    <div className={classNames({ loading }, 'border-right flex-1')}>
      <SimpleBar>
        <div className="p-24">
          <div className="d-flex align-items-center justify-content-between">
            <h5 data-testid={genAmtId(testId, 'title', '')}>
              {t(I18N_COMMON_TEXT.PAYMENT_METHOD)}
            </h5>
            {(!noRegister && (showCheckingAccount || showSavingsAccount)) && (
              <Button
                variant="outline-primary"
                size="sm"
                className="mr-n8"
                onClick={handleOpenModal}
                dataTestId={genAmtId(testId, 'edit-payment-method-btn', '')}
              >
                {t('txt_edit_payment_method')}
              </Button>
            )}
          </div>
          {noRegister ? <NoRegisterAccount /> : <PaymentMethodDetail />}
        </div>
      </SimpleBar>
      {openModal && <DemandPaymentForm />}
    </div>
  );
};

export default ACHMethod;
