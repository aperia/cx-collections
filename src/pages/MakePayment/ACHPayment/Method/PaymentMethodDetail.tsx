import React from 'react';

// components
import { Card } from 'react-bootstrap';
import { View } from 'app/_libraries/_dof/core';
import { InlineMessage } from 'app/_libraries/_dls/components';
import GridCardInfo from './GridCardInfo';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// redux
import { selectViewDataACH } from '../_redux/selectors';
// helper
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface PaymentMethodDetailProps { }

const PaymentMethodDetail: React.FC<PaymentMethodDetailProps> = () => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const testId = 'account-details_make-payment-modal_ACH-payment_method-detail';

  const dataBankInfo = useStoreIdSelector(selectViewDataACH);

  return (
    <div>
      <p className="my-16" data-testid={genAmtId(testId, 'title', '')}>
        {t('txt_select_method_to_make_payment')}
      </p>
      <InlineMessage variant="warning" dataTestId={genAmtId(testId, 'warning', '')}>
        {t('txt_make_payment_warning')}
      </InlineMessage>

      <Card className="br-light-l12 br-radius-8 mt-16">
        <Card.Header className="br-top-left-radius-8 br-top-right-radius-8 p-16">
          <strong data-testid={genAmtId(testId, 'bank-info-title', '')}>
            {t(I18N_COMMON_TEXT.BANK_INFORMATION)}
          </strong>
          <View
            descriptor="achPaymentView"
            id={`achPaymentView-${storeId}`}
            formKey={`achPaymentView-${storeId}`}
            value={dataBankInfo}
          />
        </Card.Header>
        <Card.Body>
          <GridCardInfo />
        </Card.Body>
      </Card>
    </div>
  );
};

export default PaymentMethodDetail;
