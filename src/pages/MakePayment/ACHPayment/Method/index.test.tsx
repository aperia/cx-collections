import React from 'react';
import { screen } from '@testing-library/react';

// Component
import ACHMethod from './index';

// Helper
import { renderMockStoreId, storeId, mockActionCreator } from 'app/test-utils';
import * as entitlementsHelpers from 'app/entitlements/helpers';

// Hooks
import { AccountDetailProvider } from 'app/hooks';

// redux store
import { demandACHPaymentAction } from '../_redux/reducers';
import { PERMISSIONS } from 'app/entitlements/constants';

const initialState: Partial<RootState> = {
  demandACHPayment: {
    [storeId]: {
      noRegister: false,
      openModal: false,
      achInformation: {
        bankInfo: {
          loadingBankInfo: false
        },
        cardInfo: {
          loadingCardInfo: false
        }
      }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <ACHMethod />
    </AccountDetailProvider>,
    { initialState }
  );
};

const demandACHPaymentActionSpy = mockActionCreator(demandACHPaymentAction);

beforeEach(() => {
  jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
});
describe('Render', () => {
  it('Render UI', () => {
    renderWrapper(initialState);
    expect(
      screen.getByText('txt_select_method_to_make_payment')
    ).toBeInTheDocument();
  });

  it('Render UI with NoRegisterAccount', () => {
    const state = JSON.parse(JSON.stringify(initialState));
    state.demandACHPayment[storeId].noRegister = true;
    renderWrapper(state);
    expect(screen.getByText('txt_no_register_payment')).toBeInTheDocument();
  });

  it('Render UI with DemandPaymentForm', () => {
    const state = JSON.parse(JSON.stringify(initialState));
    state.demandACHPayment[storeId].openModal = true;
    renderWrapper(state);
    expect(screen.getByText('txt_add_method_payment')).toBeInTheDocument();
  });

  it('Render UI without Edit button when have no permission to update checking account or savings account', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockImplementation((permission) => {
      return PERMISSIONS.MAKE_PAYMENT_UPDATE_CHECKING_ACCOUNT !== permission
        && PERMISSIONS.MAKE_PAYMENT_UPDATE_SAVINGS_ACCOUNT !== permission;
    });

    const state = JSON.parse(JSON.stringify(initialState));
    renderWrapper(state);
    expect(screen.queryByText('txt_edit_payment_method')).toEqual(null);
  });

  it('handleOpenModal', () => {
    const mockAction = demandACHPaymentActionSpy('updateOpenModal');
    renderWrapper(initialState);
    screen.getByText('txt_edit_payment_method').click();
    expect(mockAction).toBeCalledWith({
      storeId,
      openModal: true,
      mode: 'Edit'
    });
  });
});
