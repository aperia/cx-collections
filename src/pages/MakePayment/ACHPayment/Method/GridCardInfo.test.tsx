import React from 'react';
import { fireEvent, screen } from '@testing-library/react';

// Component
import GridCardInfo from './GridCardInfo';

// Helper
import { renderMockStore, storeId } from 'app/test-utils';

// Redux
import { demandACHPaymentAction as actions } from 'pages/MakePayment/ACHPayment/_redux/reducers';

// Hooks
import { AccountDetailProvider } from 'app/hooks';

// types
import { GridProps } from 'app/_libraries/_dls/components';

import * as entitlementsHelpers from 'app/entitlements/helpers';

jest.mock('app/_libraries/_dof/core/View', () => () => (
  <div data-testid="dof-view" />
));

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');
  const { Grid } = dlsComponents;

  return {
    ...dlsComponents,
    Grid: (props: GridProps) => (
      <>
        Grid
        <input
          data-testid="Grid_onCheck"
          onChange={(e: any) => props.onCheck!(e.target.checkedList)}
        />
        <Grid {...props} />
      </>
    )
  };
});

const data = {
  bankAccounts: [
    {
      accountType: { code: 'C' },
      accountNumber: { eValue: 'eValue', maskedValue: '' }
    },
    {
      accountType: { code: 'S' },
      accountNumber: { eValue: 'eValue', maskedValue: '' }
    }
  ]
};

const initialState: Partial<RootState> = {
  demandACHPayment: {
    [storeId]: {
      data,
      delete: {},
      achInformation: {
        gridData: [
          {
            code: 'C',
            typeCard: 'mock card',
            accountNumber: '022009001600989'
          }
        ]
      }
    }
  }
};

const checkedState: Partial<RootState> = {
  demandACHPayment: { [storeId]: { data, checkedList: ['S'] } }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <GridCardInfo />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('Render', () => {
  beforeEach(() => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
  });

  it('Render UI', () => {
    renderWrapper(initialState);

    expect(screen.getByText('Grid')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  describe('handleCheck', () => {
    it('with checkedList', () => {
      const mockAction = jest.fn().mockImplementation(() => {
        return { type: 'type' };
      });

      const spySetEValueBankInfo = jest
        .spyOn(actions, 'updateChecked')
        .mockImplementation(mockAction);

      expect(mockAction).not.toBeCalled();

      renderWrapper(checkedState);

      fireEvent.change(screen.getByTestId('Grid_onCheck'), {
        target: { value: 'undefined', checkedList: ['row_1'] }
      });

      expect(mockAction).toBeCalledWith({
        storeId,
        typeChecked: 'row_1'
      });

      spySetEValueBankInfo.mockReset();
      spySetEValueBankInfo.mockRestore();
    });
  });
});
