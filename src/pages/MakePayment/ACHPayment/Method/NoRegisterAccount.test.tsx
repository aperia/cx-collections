import React from 'react';
import { screen } from '@testing-library/react';

// Component
import NoRegisterAccount from './NoRegisterAccount';

// Helper
import { renderMockStoreId, storeId, mockActionCreator } from 'app/test-utils';

// Hooks
import { AccountDetailProvider } from 'app/hooks';

// redux store
import { demandACHPaymentAction } from '../_redux/reducers';
import * as entitlementsHelpers from 'app/entitlements/helpers';
import { PERMISSIONS } from 'app/entitlements/constants';

const initialState: Partial<RootState> = {
  demandACHPayment: {
    [storeId]: {}
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <NoRegisterAccount />
    </AccountDetailProvider>,
    { initialState }
  );
};

const demandACHPaymentActionSpy = mockActionCreator(demandACHPaymentAction);

let spy: jest.SpyInstance;

afterEach(() => {
  spy.mockReset();
  spy.mockRestore();
});

describe('Render', () => {
  beforeEach(() => {
    spy = jest
      .spyOn(entitlementsHelpers, 'checkPermission')
      .mockReturnValue(true);
  });

  it('Render UI', () => {
    spy = jest
      .spyOn(entitlementsHelpers, 'checkPermission')
      .mockImplementation(permission => {
        if (
          permission === PERMISSIONS.MAKE_PAYMENT_ADD_BANK_REGISTRATION ||
          permission === PERMISSIONS.MAKE_PAYMENT_UPDATE_SAVINGS_ACCOUNT
        )
          return true;
        return false;
      });
    renderWrapper(initialState);
    expect(screen.getByText('txt_no_register_payment')).toBeInTheDocument();
  });

  it('handleOpenModal', () => {
    const mockAction = demandACHPaymentActionSpy('updateOpenModal');
    renderWrapper({});
    screen.getByText('txt_add_method_payment').click();
    expect(mockAction).toBeCalledWith({
      storeId,
      openModal: true,
      mode: 'Add'
    });
  });
});
