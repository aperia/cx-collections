import React from 'react';

// components
import { Button, Icon } from 'app/_libraries/_dls/components';

// hooks
import { useAccountDetail } from 'app/hooks';

// redux store
import { useDispatch } from 'react-redux';
import { demandACHPaymentAction } from '../_redux/reducers';

// constants
import { ModeType } from '../constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

export interface NoRegisterAccountProps { }

const NoRegisterAccount: React.FC<NoRegisterAccountProps> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const { storeId } = useAccountDetail();
  const testId = 'account-details_make-payment-modal_ACH-payment_no-register';
  
  const showCheckingAccount = checkPermission(PERMISSIONS.MAKE_PAYMENT_UPDATE_CHECKING_ACCOUNT, storeId);
  const showSavingsAccount = checkPermission(PERMISSIONS.MAKE_PAYMENT_UPDATE_SAVINGS_ACCOUNT, storeId);

  const handleOpenModal = () => {
    dispatch(
      demandACHPaymentAction.updateOpenModal({
        storeId,
        openModal: true,
        mode: ModeType.add
      })
    );
  };

  return (
    <div className="text-center mt-80">
      <Icon name="card-collection" className="fs-80 color-light-l12" />
      <p
        className="mt-20 mb-24"
        data-testid={genAmtId(testId, 'message', '')}
      >{t('txt_no_register_payment')}</p>
      {(showCheckingAccount || showSavingsAccount) && (
        <Button
          size="sm"
          variant="outline-primary"
          onClick={handleOpenModal}
          dataTestId={genAmtId(testId, 'add-payment-method-btn', '')}
        >
          {t(I18N_COMMON_TEXT.ADD_PAYMENT_METHOD)}
        </Button>
      )}
    </div>
  );
};

export default NoRegisterAccount;
