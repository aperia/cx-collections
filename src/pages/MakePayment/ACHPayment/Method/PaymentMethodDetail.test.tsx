import React from 'react';
import { screen } from '@testing-library/react';

// Component
import PaymentMethodDetail from './PaymentMethodDetail';

// Helper
import { renderMockStoreId, storeId } from 'app/test-utils';

// Hooks
import { AccountDetailProvider } from 'app/hooks';



const initialState: Partial<RootState> = {
  demandACHPayment: {
    [storeId]: {}
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <PaymentMethodDetail />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper(initialState);
    expect(screen.getByText('txt_select_method_to_make_payment')).toBeInTheDocument()
  });
});