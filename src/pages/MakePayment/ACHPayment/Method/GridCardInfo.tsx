import React, { useMemo } from 'react';

// components
import { Button, ColumnType, Grid } from 'app/_libraries/_dls/components';

// redux
import { useDispatch } from 'react-redux';
import { demandACHPaymentAction } from '../_redux/reducers';
import { selectGridData, selectTypeCard } from '../_redux/selectors';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { formatMaskNumber } from 'app/helpers/formatMask';
import { PERMISSIONS } from 'app/entitlements/constants';
import { checkPermission } from 'app/entitlements';

const GridRadio: React.FC = () => {
  const { t } = useTranslation();

  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const testId =
    'account-details_make-payment-modal_ACH-payment_method-detaild';

  const gridData = useStoreIdSelector<ColumnType[]>(selectGridData);
  const checked = useStoreIdSelector<string>(selectTypeCard);
  const isAllowDeleteBankRegistration = checkPermission(
    PERMISSIONS.MAKE_PAYMENT_DELETE_BANK_REGISTRATION,
    storeId
  );

  const columns = useMemo(() => {
    const _columns: ColumnType[] = [
      {
        id: 'typeCard',
        Header: t('txt_type'),
        accessor: (data: MagicKeyValue) => t(data['typeCard']),
        width: 130
      },
      {
        id: 'accountNumber',
        Header: t('txt_account_number'),
        accessor: (data: any) => {
          const { firstText, lastText, maskText } = formatMaskNumber(
            data?.accountNumber
          ) as MagicKeyValue;
          return (
            <span className="custom-account-number">
              {firstText}
              <span className="hide-account-icon">{maskText}</span>
              {lastText}
            </span>
          );
        },
        width: 190,
        autoWidth: true
      }
    ];
    if (isAllowDeleteBankRegistration) {
      _columns.push({
        id: 'action',
        Header: t('txt_action'),
        accessor: () => (
          <Button size="sm" variant="outline-danger" disabled>
            {t('txt_deactivate')}
          </Button>
        ),
        width: 125,
        cellProps: { className: 'text-center' },
        cellBodyProps: { className: 'td-sm ' }
      });
    }
    return _columns;
  }, [isAllowDeleteBankRegistration, t]);

  const handleCheck = (dataKeyList: string[]) => {
    dispatch(
      demandACHPaymentAction.updateChecked({
        storeId,
        typeChecked: dataKeyList[0]
      })
    );
  };

  return (
    <div>
      <div>
        <Grid
          columns={columns}
          data={gridData}
          variant={{
            id: 'checkbox_id',
            type: 'radio',
            width: 68,
            isFixedLeft: true
          }}
          dataItemKey="code"
          checkedList={[checked]}
          onCheck={handleCheck}
          dataTestId={genAmtId(testId, 'grid', '')}
        />
      </div>
    </div>
  );
};

export default GridRadio;
