import React, { useEffect, useMemo, useState } from 'react';
import { batch, useDispatch } from 'react-redux';

// Components
import {
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

import PaymentType from './PaymentType';
import SchedulePayment from './SchedulePayment';
import ACHPayment from './ACHPayment';

import ConfirmModal, { DataType } from './ModalConfirm';
import TimerWorking from 'pages/TimerWorking';
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';

// Hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Redux
import { schedulePaymentActions } from './SchedulePayment/_redux/reducers';

import { demandACHPaymentAction } from 'pages/MakePayment/ACHPayment/_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import {
  actions as accDetailTabsActions,
  selectActiveKey
} from 'pages/__commons/AccountDetailTabs/_redux';
import { actions as paymentActions } from 'pages/Payment/__redux/reducers';
import { actions as paymentsListAction } from 'pages/Payment/PaymentList/_redux/reducers';
import { actions as paymentConfirmActions } from 'pages/MakePayment/ModalConfirm/_redux/reducers';
import { settlementActions } from 'pages/Settlement/_redux/reducers';

// selectors
import {
  getAcceptedBalanceAmount,
  getDataPost as getDataPostSchedule,
  getIsLoadingMakePayment as getIsLoadingScheduleMakePayment,
  selectCvv2,
  selectIsMakePaymentFromSettlement,
  takeModalStatus
} from './SchedulePayment/_redux/selectors';
import {
  getDataPost as getDataPostACH,
  getIsLoadingMakePayment as getIsLoadingACHMakePayment,
  selectIsInvalidACH,
  selectIsInvalidSchedule
} from 'pages/MakePayment/ACHPayment/_redux/selectors';
import { selectLoading as selectLoadingConvenienceFee } from './ModalConfirm/_redux/selectors';

// helper
import { AxiosResponse } from 'axios';
import isEmpty from 'lodash.isempty';

// constants
import { PAYMENT_TYPE, ID_OTHER_AMOUNT } from './constants';
import { KEYS } from 'pages/__commons/AccountDetailTabs';

// types
import {
  PAYMENT_CARD_TYPE,
  SubmitMakePaymentData as DataConfirm
} from './SchedulePayment/types';
import { TAB_IDS } from 'pages/Payment/constants';
import { OTHER_CODE } from './ACHPayment/constants';
import isNaN from 'lodash.isnan';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';
import { paymentSummaryActions } from './PaymentSummary/_redux/reducer';
import { PromiseToPaySingleData } from 'pages/PromiseToPay/types';

const DEFAULT_TYPE = '';

const DEFAULT_DATA_CONFIRM: DataType = {
  accountNumber: '',
  amount: {},
  date: ''
};

const MakePayment: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId, accEValue } = useAccountDetail();

  const currentActiveKey = useStoreIdSelector<string>(selectActiveKey);
  const isOpen = useStoreIdSelector<boolean>(takeModalStatus);
  const isInvalidOtherAmountSchedule = useStoreIdSelector<boolean>(
    selectIsInvalidSchedule
  );
  const isInvalidOtherAmountACH =
    useStoreIdSelector<boolean>(selectIsInvalidACH);
  const dataPostSchedule = useStoreIdSelector<DataConfirm>(getDataPostSchedule);
  const cvv2 = useStoreIdSelector<string>(selectCvv2);

  const dataPostACH = useStoreIdSelector<DataConfirm>(getDataPostACH);
  // is loading modal confirm
  const isLoadingSchedule = useStoreIdSelector<boolean>(
    getIsLoadingScheduleMakePayment
  );
  const isLoadingConvenienceFee = useStoreIdSelector<boolean>(
    selectLoadingConvenienceFee
  );

  const isLoadingACH = useStoreIdSelector<boolean>(getIsLoadingACHMakePayment);

  const acceptedBalanceAmount = useStoreIdSelector<string | undefined>(
    getAcceptedBalanceAmount
  );

  const isMakePaymentFromSettlement = useStoreIdSelector<boolean>(
    selectIsMakePaymentFromSettlement
  );

  const [paymentType, setPaymentType] = useState<string>(DEFAULT_TYPE);
  const [openConfirm, setOpenConfirm] = useState<boolean>(false);
  const [dataConfirm, setDataConfirm] =
    useState<DataType>(DEFAULT_DATA_CONFIRM);

  const isLoadingModalConfirm =
    isLoadingACH || isLoadingSchedule || isLoadingConvenienceFee;

  const testId = 'account-details_make-payment-modal';

  const dataPost = useMemo(() => {
    switch (paymentType) {
      case PAYMENT_TYPE.SCHEDULE:
        const { paymentMedium = PAYMENT_CARD_TYPE.BANK_INFO } =
          dataPostSchedule;

        if (paymentMedium === PAYMENT_CARD_TYPE.CARD_INFO) {
          return {
            ...dataPostSchedule,
            cvv2
          };
        }

        return dataPostSchedule;
      case PAYMENT_TYPE.DEMAND_ACH:
      default:
        return dataPostACH;
    }
  }, [dataPostACH, dataPostSchedule, paymentType, cvv2]);

  const disabledOk = useMemo(() => {
    const {
      accountNumberMaskedValue,
      crdNbrEValue,
      schedulePaymentDate,
      amount,
      transactionType,
      paymentMedium = PAYMENT_CARD_TYPE.BANK_INFO
    } = dataPost;
    const { id, value } = amount!;
    const isOtherAmountSchedule = id === ID_OTHER_AMOUNT;
    const isOtherAmountACH = id === OTHER_CODE;
    const isErrorAmount =
      (isOtherAmountSchedule &&
        (parseFloat((value || '').toString()) === 0 || value === '')) ||
      value === undefined;

    const commonCheck = !crdNbrEValue || !schedulePaymentDate || isErrorAmount;

    switch (paymentType) {
      case PAYMENT_TYPE.SCHEDULE:
        let isInValidCvv = false;
        if (paymentMedium === PAYMENT_CARD_TYPE.CARD_INFO && cvv2.length < 3) {
          isInValidCvv = true;
        }

        return (
          commonCheck ||
          isInValidCvv ||
          (isOtherAmountSchedule && isInvalidOtherAmountSchedule)
        );
      case PAYMENT_TYPE.DEMAND_ACH:
      default:
        return (
          isEmpty(accountNumberMaskedValue) ||
          isEmpty(transactionType) ||
          parseFloat(`${amount?.value}`) <= 0 ||
          isNaN(parseFloat(`${amount?.value}`)) ||
          (isOtherAmountACH && isInvalidOtherAmountACH)
        );
    }
  }, [
    dataPost,
    paymentType,
    cvv2,
    isInvalidOtherAmountSchedule,
    isInvalidOtherAmountACH
  ]);

  const handleClose = () => {
    // reset payment type
    setPaymentType(DEFAULT_TYPE);

    batch(() => {
      handleCloseConfirm();
      // remove data Schedule Payment
      dispatch(schedulePaymentActions.clearData({ storeId }));
      // remove data ACH Payment
      dispatch(demandACHPaymentAction.removeStore({ storeId }));
    });
  };

  // handle Confirm Payment
  const handleCloseConfirm = () => {
    setOpenConfirm(false);
    dispatch(paymentConfirmActions.clearData({ storeId }));
  };

  const handleOpenConfirm = () => {
    setDataConfirm({
      paymentMedium: dataPost?.paymentMedium,
      accountNumber: dataPost?.accountNumberMaskedValue || '',
      amount: dataPost.amount!,
      date: (dataPost?.schedulePaymentDate as any)!
    });
    setOpenConfirm(true);
  };

  const handleConfirm = () => {
    const data = {
      accountId: accEValue,
      storeId,
      data: dataPost,
      onSuccess: (response: AxiosResponse<MagicKeyValue>) => {
        const activeKey = KEYS.payments;
        let message = 'txt_on_demand_ach_payment_scheduled';

        if (paymentType === PAYMENT_TYPE.SCHEDULE) {
          const { confirmationNumber } = response?.data || {};
          message = t('txt_payment_scheduled_with_confirmation_number', {
            count: confirmationNumber
          });

          if (checkPermission(PERMISSIONS.PAYMENT_LIST_VIEW, storeId)) {
            batch(() => {
              // navigate to Tabs Payments
              if (currentActiveKey !== activeKey) {
                dispatch(
                  accDetailTabsActions.setActiveTab({ storeId, activeKey })
                );
              } else {
                // active tab Payment list in Payment
                dispatch(
                  paymentActions.setActiveKey({
                    storeId,
                    activeKey: TAB_IDS.paymentList
                  })
                );
                // reload payment list
                dispatch(
                  paymentsListAction.getPayments({
                    storeId,
                    postData: { accountId: accEValue }
                  })
                );
                // clear search sort pagination param
                dispatch(paymentsListAction.clearAndReset({ storeId }));
              }
            });
          }
        }

        // show suggest toast
        dispatch(
          actionsToast.addToast({ show: true, type: 'success', message })
        );

        // when has acceptedBalanceAmount should be trigger settlement to submit call result
        acceptedBalanceAmount !== undefined &&
          dispatch(
            settlementActions.setPaymentSuccess({
              storeId,
              isSuccess: true,
              confirmationNumber: response?.data?.confirmationNumber || '',
              method: dataPost?.method,
              type: paymentType
            })
          );

        handleClose();
      },

      onFailure: () => {
        const message = 'txt_make_payment_failed';
        dispatch(actionsToast.addToast({ show: true, type: 'error', message }));
        handleCloseConfirm();
      }
    };

    switch (paymentType) {
      case PAYMENT_TYPE.SCHEDULE: {
        dispatch(schedulePaymentActions.submitMakePayment(data));
        break;
      }

      case PAYMENT_TYPE.DEMAND_ACH:
      default: {
        dispatch(demandACHPaymentAction.submitMakePayment(data));
        break;
      }
    }

    if (isMakePaymentFromSettlement) {
      const {
        data: { amount, method, schedulePaymentDate, paymentMedium }
      } = data;

      dispatch(
        settlementActions.triggerUpdateSettlement({
          storeId,
          postData: {
            common: { accountId: accEValue },
            data: {
              settlementType: 'MakePayment',
              settlementTypeSubmit:
                paymentType === PAYMENT_TYPE.SCHEDULE
                  ? 'PAY_POINT_PAYMENT'
                  : 'HOST_PAYMENT',
              paymentMedium,
              acceptedBalanceAmount,
              method,
              promiseToPayData: {
                amount: parseFloat(amount?.value as string).toFixed(2),
                method: { value: method, description: method },
                paymentDate: schedulePaymentDate
              } as PromiseToPaySingleData
            }
          }
        })
      );
    }
  };

  const handleChangePaymentType = (selectedPayment: string) => {
    setPaymentType(selectedPayment);

    // clear all data have choose
    switch (selectedPayment) {
      case PAYMENT_TYPE.SCHEDULE:
        dispatch(demandACHPaymentAction.removeStore({ storeId }));
        break;
      case PAYMENT_TYPE.DEMAND_ACH:
      default:
        dispatch(schedulePaymentActions.clearDataSelected({ storeId }));
        break;
    }
  };

  useEffect(() => {
    dispatch(
      paymentSummaryActions.getPaymentSummary({
        storeId: storeId,
        postData: { statementDate: '', accountId: accEValue }
      })
    );
  }, [accEValue, dispatch, storeId]);

  const body = useMemo(() => {
    if (!paymentType) return;

    if (paymentType === PAYMENT_TYPE.SCHEDULE) {
      return <SchedulePayment />;
    }
    return <ACHPayment />;
  }, [paymentType]);

  if (!isOpen) return null;

  return (
    <>
      <Modal full show dataTestId={testId}>
        <ModalHeader
          border
          closeButton
          onHide={handleClose}
          dataTestId={genAmtId(testId, 'header', '')}
        >
          <ModalTitle
            className="d-flex align-items-center"
            dataTestId={genAmtId(testId, 'header-title', '')}
          >
            {t('txt_make_payment')}{' '}
            <TimerWorking
              className="ml-16"
              dataTestId={genAmtId(testId, 'header-timmer', '')}
            />
          </ModalTitle>
        </ModalHeader>
        <ModalBodyWithApiError
          noPadding
          className="d-flex flex-column overflow-hidden"
          apiErrorClassName="pt-24 pl-24 bg-light-l20"
          storeId={storeId}
          dataTestId={genAmtId(testId, 'body', '')}
        >
          <PaymentType onChangePaymentType={handleChangePaymentType} />
          {body}
        </ModalBodyWithApiError>
        <ModalFooter
          border
          cancelButtonText={t('txt_cancel')}
          okButtonText={t('txt_make_payment')}
          disabledOk={disabledOk}
          onCancel={handleClose}
          onOk={handleOpenConfirm}
          dataTestId={genAmtId(testId, 'footer', '')}
        />
      </Modal>
      {openConfirm && (
        <ConfirmModal
          openConfirm={openConfirm}
          loading={isLoadingModalConfirm}
          handleMakePayment={handleConfirm}
          handleCloseModal={handleCloseConfirm}
          data={dataConfirm}
          paymentType={paymentType}
        />
      )}
    </>
  );
};

export default MakePayment;
