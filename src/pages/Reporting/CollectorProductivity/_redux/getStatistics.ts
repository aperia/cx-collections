import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { FormatTime } from 'app/constants/enums';
import { getEndOfDay, getStartOfDay, mappingDataFromObj } from 'app/helpers';
import { SettingFilter } from 'pages/Reporting/types';
import { collectorProductivityServices } from '../collectorProductivityServices';
import { TIME_PERIOD_VALUE } from '../constants';
import {
  CollectorProductivityState,
  GetStatisticsPayload,
  ProductivityStatistics
} from '../types';

export const getStatistics = createAsyncThunk<
  GetStatisticsPayload,
  SettingFilter,
  ThunkAPIConfig
>('collectorProductivity/getStatistics', async (args, thunkAPI) => {
  const state = thunkAPI.getState();

  const queues = state?.queueList?.data?.queueDetails || [];
  const { collector } = args;

  const queueIds = queues.map((item: any) => {
    return item?.queueId;
  });

  let startDateTime = '',
    endDateTime = '';
  const currentDate = new Date();
  switch (args.timePeriod) {
    case TIME_PERIOD_VALUE.TODAY:
      startDateTime = getStartOfDay(currentDate, FormatTime.UTCAllDatetime);
      endDateTime = getEndOfDay(currentDate, FormatTime.UTCAllDatetime);
      break;
    case TIME_PERIOD_VALUE.CURRENT_MONTH:
      const startMonthDate = new Date(
        currentDate.getFullYear(),
        currentDate.getMonth(),
        1
      );
      const endMonthDate = new Date(
        currentDate.getFullYear(),
        currentDate.getMonth() + 1,
        0
      );
      startDateTime = getStartOfDay(startMonthDate, FormatTime.UTCAllDatetime);
      endDateTime = getEndOfDay(endMonthDate, FormatTime.UTCAllDatetime);
      break;
    case TIME_PERIOD_VALUE.DATE_RANGE:
      startDateTime = getStartOfDay(
        args.rangeDatePicker?.start,
        FormatTime.UTCAllDatetime
      );
      endDateTime = getEndOfDay(
        args.rangeDatePicker?.end,
        FormatTime.UTCAllDatetime
      );
      break;
  }

  const mappingModel = state.mapping?.data?.productivityStatistic;

  const response = await collectorProductivityServices.getStatistics({
    userName: collector?.userId,
    queues: queueIds,
    startDateTime,
    endDateTime
  });

  if (response.data?.status?.toLowerCase() !== 'success') {
    throw new Error('Status is not success');
  }

  const productivityStatisticData = mappingDataFromObj<ProductivityStatistics>(
    response.data,
    mappingModel!
  );

  productivityStatisticData.percentPromisesVsWorked =
    (productivityStatisticData.percentPromisesVsWorked || 0) * 100;
  productivityStatisticData.percentPromisesKept =
    (productivityStatisticData.percentPromisesKept || 0) * 100;

  // TODO: in API getCollectorStats, missing response data for goals, breaks and breakOverview
  return {
    productivityStatistics: productivityStatisticData,
    goals: [],
    breaks: [],
    breakOverview: undefined
  };
});

export const getStatisticsBuilder = (
  builder: ActionReducerMapBuilder<CollectorProductivityState>
) => {
  builder
    .addCase(getStatistics.pending, (draftState, action) => {
      draftState.loading = true;
    })
    .addCase(getStatistics.fulfilled, (draftState, action) => {
      draftState.reports = action.payload;
      draftState.loading = false;
    })
    .addCase(getStatistics.rejected, (draftState, action) => {
      draftState.loading = false;
    });
};
