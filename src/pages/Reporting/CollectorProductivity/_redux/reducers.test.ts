import { DEFAULT_FILTER } from '../constants';
import { CollectorProductivityState } from '../types';
import { collectorProductivityActions as actions, reducer } from './reducers';

const initialState: CollectorProductivityState = {
  filters: DEFAULT_FILTER,
  isCollapsed: false,
  isMounted: false
};

describe('collector productivity reducers', () => {
  it('should test updateFilter reducer', () => {
    const state = reducer(initialState, actions.updateFilter({}));

    expect(state.filters).toEqual({});
    expect(state.isMounted).toEqual(true);
  });

  it('should test setToggleSettingsSection reducer', () => {
    const state = reducer(initialState, actions.setToggleSettingsSection(true));

    expect(state.isCollapsed).toEqual(true);
  });

  it('should test cleanup reducer', () => {
    const state = reducer(initialState, actions.cleanup());

    expect(state).toEqual(initialState);
  });
});
