import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { SettingFilter } from 'pages/Reporting/types';
import { DEFAULT_FILTER } from '../constants';
import { CollectorProductivityState, Reports } from '../types';
import { getFilterRefData, getFilterRefDataBuilder } from './getFilterRefData';
import { getStatistics, getStatisticsBuilder } from './getStatistics';

// Export initial state for unit test
export const initialState: CollectorProductivityState = {
  filters: DEFAULT_FILTER,
  isCollapsed: false,
  isMounted: false
};

const { actions, reducer } = createSlice({
  name: 'collectorProductivity',
  initialState: initialState,
  reducers: {
    updateFilter: (draftState, action: PayloadAction<SettingFilter>) => {
      draftState.filters = action.payload;
      draftState.isMounted = true;
    },
    setReports: (draftState, action: PayloadAction<Reports>) => {
      draftState.reports = action.payload;
    },
    setToggleSettingsSection: (draftState, action: PayloadAction<boolean>) => {
      draftState.isCollapsed = action.payload;
    },
    cleanup: () => initialState
  },
  extraReducers: builder => {
    getFilterRefDataBuilder(builder);
    getStatisticsBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getFilterRefData,
  getStatistics
};

export { allActions as collectorProductivityActions, reducer };
