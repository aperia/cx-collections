import { selectorWrapper } from 'app/test-utils';
import { REPORT_TYPE_VALUE } from 'pages/Reporting/constants';
import { SettingFilter } from 'pages/Reporting/types';
import { DEFAULT_FILTER, TIME_PERIOD_VALUE } from '../constants';
import { FilterRefData, Reports } from '../types';
import * as selector from './selectors';

const mockFilterData: SettingFilter = {
  timePeriod: TIME_PERIOD_VALUE.CURRENT_MONTH,
  reportType: REPORT_TYPE_VALUE.ROLL_UP
};

const mockFilterRefData: FilterRefData = {
  timePeriods: [],
  reportTypes: []
};

const mockReportData: Reports = {
  productivityStatistics: {},
  breakOverview: {},
  goals: [],
  breaks: []
};

describe('should test collector productivity selectors', () => {
  const states: Partial<RootState> = {
    collectorProductivity: {
      filters: mockFilterData,
      filterRefData: mockFilterRefData,
      reports: mockReportData
    }
  };

  it('should test filter selector', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectedFilter
    );

    expect(data).toEqual(mockFilterData);
    expect(emptyData).toEqual(DEFAULT_FILTER);
  });

  it('should test filter ref data selector', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectedFilterRefData
    );

    expect(data).toEqual(mockFilterRefData);
    expect(emptyData).toEqual({});
  });

  it('should test reports data selector', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectedReport
    );

    expect(data).toEqual(mockReportData);
    expect(emptyData).toEqual({});
  });

  it('should test productivity statistics data selector', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectedProductivityStatistics
    );

    expect(data).toEqual({});
    expect(emptyData).toEqual({});
  });

  it('should test goals data selector', () => {
    const { data, emptyData } = selectorWrapper(states)(selector.selectedGoals);

    expect(data).toEqual([]);
    expect(emptyData).toEqual([]);
  });

  it('should test breaks data selector', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectedBreaks
    );

    expect(data).toEqual([]);
    expect(emptyData).toEqual([]);
  });
});
