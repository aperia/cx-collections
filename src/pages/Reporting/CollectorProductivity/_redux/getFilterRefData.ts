import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { collectorProductivityServices } from '../collectorProductivityServices';
import { CollectorProductivityState, GetFilterDataPayload } from '../types';

export const getFilterRefData = createAsyncThunk<
  GetFilterDataPayload,
  undefined,
  ThunkAPIConfig
>('collectorProductivity/getFilterRefData', async _ => {
  const [timePeriodsResponse, reportTypesResponse] = await Promise.all([
    collectorProductivityServices.getTimePeriods(),
    collectorProductivityServices.getReportTypes()
  ]);

  return {
    timePeriods: timePeriodsResponse?.data,
    reportTypes: reportTypesResponse?.data
  };
});

export const getFilterRefDataBuilder = (
  builder: ActionReducerMapBuilder<CollectorProductivityState>
) => {
  builder
    .addCase(getFilterRefData.pending, (draftState, action) => {
      draftState.loading = true;
    })
    .addCase(getFilterRefData.fulfilled, (draftState, action) => {
      draftState.loading = false;
      draftState.filterRefData = action.payload;
    })
    .addCase(getFilterRefData.rejected, (draftState, action) => {
      draftState.loading = false;
    });
};
