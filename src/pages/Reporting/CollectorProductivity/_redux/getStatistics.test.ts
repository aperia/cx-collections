import { responseDefault } from 'app/test-utils';
import { SettingFilter } from 'pages/Reporting/types';
import { MappingStateData } from 'pages/__commons/MappingProvider/types';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { collectorProductivityServices } from '../collectorProductivityServices';
import { DEFAULT_FILTER, TIME_PERIOD_VALUE } from '../constants';
import { GetStatisticsPayload } from '../types';
import { getStatistics } from './getStatistics';

const mockArgs: SettingFilter = DEFAULT_FILTER;

const productivityStatisticMapping = {
  callsTaken: 'callsTaken',
  accountsWorked: 'accountsWorked',
  pendingPromises: 'pendingPromises',
  keptPromises: 'keptPromises',
  brokenPromises: 'brokenPromises',
  totalPromises: 'totalPromises',
  pendingAmount: 'pendingAmount',
  keptAmount: 'keptAmount',
  brokenAmount: 'brokenAmount',
  totalAmount: 'totalAmount',
  percentPromisesVsWorked: 'promisesWorkedPcnt',
  promiseBalance: 'promiseBalance',
  amountCollected: 'amountCollected',
  percentPromisesKept: 'promiseKeptPcnt'
};

describe('should test get reports', () => {
  let spy: jest.SpyInstance;
  let store: Store<RootState>;
  let state: RootState;

  beforeEach(() => {
    store = createStore(rootReducer, {
      collectorProductivity: {}
    });
    state = store.getState();
  });

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('should return data with time period today', async () => {
    spy = jest
      .spyOn(collectorProductivityServices, 'getStatistics')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          status: "success",
        }
      });

    const reports = await getStatistics(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(reports.payload).toEqual({
      productivityStatistics: {
        percentPromisesKept: 0,
        percentPromisesVsWorked: 0
      },
      breakOverview: undefined,
      goals: [],
      breaks: []
    });
    expect(reports.type).toEqual(
      'collectorProductivity/getStatistics/fulfilled'
    );
  });

  it('should return data with time period current month', async () => {
    spy = jest
      .spyOn(collectorProductivityServices, 'getStatistics')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          status: "success",
          callsTaken: '10',
          promisesWorkedPcnt: '0.50',
          promiseKeptPcnt: '0.70'
        }
      });

    const reports = await getStatistics({
      ...mockArgs,
      timePeriod: TIME_PERIOD_VALUE.CURRENT_MONTH
    })(
      store.dispatch,
      () => {
        return {
          queueList: {
            data: {
              queueDetails: [{ queueId: 'queueId1' }]
            }
          } as any,
          mapping: {
            data: { productivityStatistic: productivityStatisticMapping }
          } as MappingStateData
        } as RootState;
      },
      {}
    );

    const result = reports.payload as Record<string, any>;
    expect(result.productivityStatistics.percentPromisesKept).toEqual(70);
    expect(result.productivityStatistics.percentPromisesVsWorked).toEqual(50);
    expect(result.productivityStatistics.callsTaken).toEqual('10');
  });

  it('should return data with time period date range', async () => {
    spy = jest
      .spyOn(collectorProductivityServices, 'getStatistics')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          status: "success",
          accountsWorked: '16',
          keptAmount: '100.00'
        }
      });

    const reports = await getStatistics({
      ...mockArgs,
      timePeriod: TIME_PERIOD_VALUE.DATE_RANGE
    })(
      store.dispatch,
      () => {
        return {
          mapping: {
            data: { productivityStatistic: productivityStatisticMapping }
          } as MappingStateData
        } as RootState;
      },
      {}
    );

    const result = reports.payload as Record<string, any>;
    expect(result.productivityStatistics.accountsWorked).toEqual("16");
    expect(result.productivityStatistics.keptAmount).toEqual("100.00");
  });

  it('should throw error when status is not "success"', async () => {
    spy = jest
      .spyOn(collectorProductivityServices, 'getStatistics')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          status: "failed"
        }
      });

    const reports = await getStatistics({
      ...mockArgs,
      timePeriod: TIME_PERIOD_VALUE.DATE_RANGE
    })(
      store.dispatch,
      () => {
        return {
          mapping: {
            data: { productivityStatistic: productivityStatisticMapping }
          } as MappingStateData
        } as RootState;
      },
      {}
    ) as Record<string, any>;

    expect(reports.error?.message).toEqual("Status is not success");
  });

  const payload: GetStatisticsPayload = {
    productivityStatistics: {},
    breakOverview: {},
    goals: [],
    breaks: []
  };

  it('should be pending', () => {
    const pending = getStatistics.pending(getStatistics.pending.type, mockArgs);
    const actual = rootReducer(state, pending);

    expect(actual.collectorProductivity.loading).toEqual(true);
  });

  it('should be fulfilled', () => {
    const fulfilled = getStatistics.fulfilled(
      payload,
      getStatistics.fulfilled.type,
      mockArgs
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual.collectorProductivity.reports).toEqual({
      productivityStatistics: {},
      breakOverview: {},
      goals: [],
      breaks: []
    });
    expect(actual.collectorProductivity.loading).toEqual(false);
  });

  it('should be rejected', () => {
    const rejected = getStatistics.rejected(
      { name: 'Error', message: 'Error' },
      getStatistics.rejected.type,
      mockArgs
    );
    const actual = rootReducer(state, rejected);

    expect(actual.collectorProductivity.reports).toBeUndefined();
    expect(actual.collectorProductivity.loading).toEqual(false);
  });
});
