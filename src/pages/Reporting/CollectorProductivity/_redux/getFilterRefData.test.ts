import { responseDefault } from 'app/test-utils';
import { ADMIN_ROLE } from 'pages/__commons/UserRole/constants';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { collectorProductivityServices } from '../collectorProductivityServices';
import { FilterItem, GetFilterDataArgs } from '../types';
import { getFilterRefData } from './getFilterRefData';

const mockArgs: GetFilterDataArgs = {
  userRole: ADMIN_ROLE
};

describe('should test get filter ref data', () => {
  let spyGetTimePeriods: jest.SpyInstance;
  let spyGetReportTypes: jest.SpyInstance;

  let store: Store<RootState>;
  let state: RootState;

  beforeEach(() => {
    store = createStore(rootReducer, {
      collectorProductivity: {}
    });
    state = store.getState();
  });

  afterEach(() => {
    spyGetTimePeriods?.mockReset();
    spyGetTimePeriods?.mockRestore();

    spyGetReportTypes?.mockReset();
    spyGetReportTypes?.mockRestore();
  });

  it('should fetch data', async () => {
    spyGetTimePeriods = jest
      .spyOn(collectorProductivityServices, 'getTimePeriods')
      .mockResolvedValue({ ...responseDefault });
    spyGetReportTypes = jest
      .spyOn(collectorProductivityServices, 'getReportTypes')
      .mockResolvedValue({ ...responseDefault });

    const response = await getFilterRefData()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      timePeriods: null,
      reportTypes: null
    });
  });

  it('should response fulfilled', () => {
    const timePeriods: FilterItem[] = [],
      reportTypes: FilterItem[] = [];

    const fulfilled = getFilterRefData.fulfilled(
      { timePeriods, reportTypes },
      getFilterRefData.fulfilled.type,
      mockArgs
    );

    const actual = rootReducer(state, fulfilled);

    expect(actual.collectorProductivity.loading).toEqual(false);
    expect(actual.collectorProductivity.filterRefData?.timePeriods).toEqual([]);
    expect(actual.collectorProductivity.filterRefData?.reportTypes).toEqual([]);
  });
});
