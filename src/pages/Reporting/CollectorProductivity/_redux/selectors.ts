import { createSelector } from 'reselect';
import { DEFAULT_FILTER } from '../constants';
import { CollectorProductivityState, Reports } from '../types';

const getCollectorProductivity = (states: RootState) =>
  states.collectorProductivity;

const getReports = (states: RootState) =>
  states.collectorProductivity?.reports || {};

export const selectedFilter = createSelector(
  getCollectorProductivity,
  (data: CollectorProductivityState) => data?.filters || DEFAULT_FILTER
);

export const selectedFilterRefData = createSelector(
  getCollectorProductivity,
  (data: CollectorProductivityState) => data?.filterRefData || {}
);

export const selectedReport = createSelector(
  getReports,
  (data: Reports) => data
);

export const selectedProductivityStatistics = createSelector(
  getReports,
  (data: Reports) => data?.productivityStatistics || {}
);

export const selectedGoals = createSelector(
  getReports,
  (data: Reports) => data?.goals || []
);

export const selectedBreaks = createSelector(
  getReports,
  (data: Reports) => data?.breaks || []
);

export const selectedBreakOverview = createSelector(
  getReports,
  (data: Reports) => data?.breakOverview || {}
);

export const selectedCollapsedStatus = createSelector(
  getCollectorProductivity,
  (data: CollectorProductivityState) => data?.isCollapsed
);

export const selectedLoading = createSelector(
  getCollectorProductivity,
  (data: CollectorProductivityState) => data?.loading
);

export const selectedMountedStatus = createSelector(
  getCollectorProductivity,
  (data: CollectorProductivityState) => data?.isMounted
);
