import { apiService } from 'app/utils/api.service';
import { GetStatisticsArgs } from './types';

export const collectorProductivityServices = {
  getTimePeriods() {
    const { isAdminRole, isSupervisor } = window.appConfig?.commonConfig || {};

    if (!isAdminRole && !isSupervisor) {
      const url =
        window.appConfig?.api?.collectorProductivity.getTimePeriods || '';
      return apiService.get(url);
    } else {
      const url =
        window.appConfig?.api?.collectorProductivity.getAdminTimePeriods || '';
      return apiService.get(url);
    }
  },

  getReportTypes() {
    const { isAdminRole, isSupervisor } = window.appConfig?.commonConfig || {};
    if (!isAdminRole && !isSupervisor) return;
    const url =
      window.appConfig?.api?.collectorProductivity.getReportTypes || '';
    return apiService.get(url);
  },

  getStatistics(args: GetStatisticsArgs) {
    const url =
      window.appConfig?.api?.collectorProductivity.getStatistics || '';
    return apiService.post(url, args);
  }
};
