// components
import React, { useCallback, useLayoutEffect, useMemo, useRef } from 'react';
import { ToggleButton } from 'app/components';
import {
  Button,
  DateRangePicker,
  Radio,
  SimpleBar
} from 'app/_libraries/_dls/components';
import { genAmtId } from 'app/_libraries/_dls/utils';
import classNames from 'classnames';
import { FormikProps, FormikProvider } from 'formik';
import { ReportingField } from '../ReportingField';
import { CollectorsTeamsComboBox } from '../CollectorsTeamsComboBox';

// constants
import {
  I18N_COLLECTOR_PRODUCTIVITY,
  I18N_COMMON_TEXT,
  I18N_PROMISES_PAYMENTS_STATISTICS
} from 'app/constants/i18n';

import { DEFAULT_FILTER, TIME_PERIOD_VALUE } from './constants';
import { DateRangePickerError, FilterRefData } from './types';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import isString from 'lodash.isstring';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import { collectorsTeamsComboBoxActions } from '../CollectorsTeamsComboBox/_redux/reducers';
import { collectorProductivityActions } from './_redux/reducers';
import {
  selectedCollapsedStatus,
  selectedFilterRefData
} from './_redux/selectors';
import { FORM_NAMES } from '../constants';
import { SettingFilter } from '../types';

const FilterFormDetail = (props: FormikProps<SettingFilter>) => {
  const {
    values,
    errors,
    isValid,
    touched,
    setValues,
    setTouched,
    submitForm,
    handleChange,
    handleBlur,
    validateForm,
    validateField,
    resetForm
  } = props;

  const dispatch = useDispatch();
  const { t } = useTranslation();
  const leftRef = useRef<HTMLDivElement | null>(null);

  const collapsed = useSelector(selectedCollapsedStatus);
  const { timePeriods, reportTypes }: FilterRefData = useSelector(
    selectedFilterRefData
  );
  const { isAdminRole: isAdmin, isSupervisor } =
    window.appConfig?.commonConfig || {};
  const testId = 'collectorProductivity-Filter';

  useLayoutEffect(() => {
    leftRef.current && (leftRef.current.style.height = `calc(100vh - 382px)`);
  }, []);

  const handleToggleButtonEvent = useCallback(() => {
    dispatch(collectorProductivityActions.setToggleSettingsSection(!collapsed));
  }, [dispatch, collapsed]);

  const handleTimePeriodChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      setValues({
        ...values,
        rangeDatePicker: undefined
      });
      setTouched({
        ...touched,
        [FORM_NAMES.RANGE_DATE_PICKER]: false
      });
      handleChange(e);
      process.nextTick(() => {
        validateField(FORM_NAMES.RANGE_DATE_PICKER);
      });
    },
    [handleChange, setTouched, setValues, touched, validateField, values]
  );

  const handleResetForm = useCallback(() => {
    resetForm({
      values: DEFAULT_FILTER
    });
    validateForm(DEFAULT_FILTER);
    dispatch(collectorProductivityActions.updateFilter(DEFAULT_FILTER));
    dispatch(collectorsTeamsComboBoxActions.reset());
  }, [dispatch, resetForm, validateForm]);

  const handleShowDateRangePickerError = useCallback(
    (
      dateRangePickerError: DateRangePickerError
    ): { status: boolean; message: string } | undefined => {
      if (dateRangePickerError?.start) {
        return {
          status: isString(dateRangePickerError.start),
          message: t(dateRangePickerError.start)
        };
      } else if (dateRangePickerError?.end) {
        return {
          status: isString(dateRangePickerError.end),
          message: t(dateRangePickerError.end)
        };
      }
      return undefined;
    },
    [t]
  );

  const timePeriodRadio = useMemo(
    () => (
      <ReportingField
        title={t(I18N_COLLECTOR_PRODUCTIVITY.TIME_PERIOD)}
        dataTestId={genAmtId(testId, 'timePeriod', '')}
      >
        <div className="custom-group-radio col-12">
          {timePeriods?.map((element, index) => (
            <Radio
              key={`${index}-${element.id}`}
              className="mt-16 mr-24"
              dataTestId={genAmtId(testId, 'timePeriod-radio', '')}
            >
              <Radio.Input
                id={element.id}
                name={FORM_NAMES.TIME_PERIOD}
                checked={values?.timePeriod === element.value}
                value={element.value}
                onChange={handleTimePeriodChange}
              ></Radio.Input>
              <Radio.Label>{t(element.label)}</Radio.Label>
            </Radio>
          ))}
        </div>
        {values?.timePeriod === TIME_PERIOD_VALUE.DATE_RANGE && (
          <div className="col-12 mt-16">
            <DateRangePicker
              required
              id={FORM_NAMES.RANGE_DATE_PICKER}
              name={FORM_NAMES.RANGE_DATE_PICKER}
              label={t(I18N_COLLECTOR_PRODUCTIVITY.DATE_RANGE)}
              value={values?.rangeDatePicker}
              onChange={handleChange}
              onBlur={handleBlur}
              error={
                touched?.rangeDatePicker
                  ? handleShowDateRangePickerError(
                      errors?.rangeDatePicker as DateRangePickerError
                    )
                  : undefined
              }
              dataTestId={genAmtId(testId, 'date-range', '')}
            />
          </div>
        )}
      </ReportingField>
    ),
    [
      errors?.rangeDatePicker,
      handleBlur,
      handleChange,
      handleShowDateRangePickerError,
      handleTimePeriodChange,
      t,
      timePeriods,
      touched?.rangeDatePicker,
      values?.rangeDatePicker,
      values?.timePeriod
    ]
  );

  const handleChangeReportType = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      batch(() => {
        setValues({
          ...values,
          [FORM_NAMES.COLLECTOR_COMBO_BOX]: undefined,
          [FORM_NAMES.TEAM_COMBO_BOX]: undefined
        });
        setTouched({
          ...touched,
          [FORM_NAMES.COLLECTOR_COMBO_BOX]: false,
          [FORM_NAMES.TEAM_COMBO_BOX]: false
        });
        handleChange(e);
        dispatch(collectorsTeamsComboBoxActions.resetComboBoxData());
        dispatch(collectorsTeamsComboBoxActions.setCollectors([]));
        dispatch(collectorsTeamsComboBoxActions.setTeams([]));
      });
    },
    [dispatch, handleChange, setTouched, setValues, touched, values]
  );

  const reportTypeRadio = useMemo(
    () => (
      <ReportingField
        title={t(I18N_PROMISES_PAYMENTS_STATISTICS.REPORT_TYPE)}
        dataTestId={genAmtId(testId, 'reportType', '')}
      >
        {(isSupervisor || isAdmin) && (
          <div className="custom-group-radio col-12">
            {reportTypes?.map((element, index) => (
              <Radio
                key={`${index}-${element.id}`}
                className="mt-16 mr-24"
                dataTestId={genAmtId(testId, 'report-type', '')}
              >
                <Radio.Input
                  id={element.id}
                  name={FORM_NAMES.REPORT_TYPE}
                  checked={values?.reportType === element.value}
                  value={element.value}
                  onChange={handleChangeReportType}
                />
                <Radio.Label>{t(element.label)}</Radio.Label>
              </Radio>
            ))}
          </div>
        )}
      </ReportingField>
    ),
    [
      t,
      isSupervisor,
      isAdmin,
      reportTypes,
      values?.reportType,
      handleChangeReportType
    ]
  );

  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        submitForm();
      }}
      className={classNames('reporting-section', {
        collapsed
      })}
    >
      <ToggleButton
        collapseMessage={t(I18N_COMMON_TEXT.EXPAND)}
        expandMessage={t(I18N_COMMON_TEXT.COLLAPSE)}
        collapsed={!collapsed}
        onToggleCollapsed={handleToggleButtonEvent}
        dataTestId={genAmtId(testId, 'expand-btn', '')}
      />
      <div ref={leftRef}>
        <SimpleBar>
          <div className="px-24 pb-24 pt-16">
            <h5 className="mb-n8" data-testid={genAmtId(testId, 'setting', '')}>
              {t(I18N_COLLECTOR_PRODUCTIVITY.SETTINGS)}
            </h5>

            {timePeriodRadio}
            {reportTypeRadio}
            <FormikProvider value={props}>
              <CollectorsTeamsComboBox />
            </FormikProvider>
          </div>
        </SimpleBar>
      </div>

      <div className="d-flex justify-content-end py-16 px-24 border-top">
        <Button
          variant="secondary"
          onClick={handleResetForm}
          dataTestId={genAmtId(testId, 'reset-btn', '')}
        >
          {t(I18N_PROMISES_PAYMENTS_STATISTICS.RESET_TO_DEFAULT)}
        </Button>
        <Button
          type="submit"
          variant="primary"
          disabled={!isValid}
          dataTestId={genAmtId(testId, 'submit-btn', '')}
        >
          {t(I18N_PROMISES_PAYMENTS_STATISTICS.RUN)}
        </Button>
      </div>
    </form>
  );
};

export default FilterFormDetail;
