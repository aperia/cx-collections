import { BreakGridView } from './BreakGridView';
import { renderMockStore } from 'app/test-utils';
import React from 'react';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponent = jest.requireActual('app/_libraries/_dls/components');
  const { MockGrid } = jest.requireActual('app/test-utils/mocks/MockGrid');

  return {
    ...dlsComponent,
    Grid: MockGrid
  };
});

describe('Test Break Gridview component', () => {
  beforeEach(() => {});

  afterEach(() => {});

  const initStateData = {
    collectorProductivity: {
      reports: {
        breaks: [
          {
            code: 1,
            activity: 'mock activity'
          }
        ],
        breakOverview: {}
      }
    }
  };

  const renderBreakGrid = (storeConfig?: Record<string, any>) => {
    return renderMockStore(<BreakGridView />, {
      initialState: { ...initStateData, ...storeConfig }
    });
  };

  it('should render Break Grid UI with break data', () => {
    const { wrapper } = renderBreakGrid();

    expect(wrapper.getByText('txt_breaks')).toBeInTheDocument();
    expect(
      wrapper.baseElement.querySelector('.dls-grid-body')?.childElementCount
    ).toEqual(1);
  });

  it('should render Break Grid UI without break data', () => {
    const updateData = { ...initStateData };
    updateData.collectorProductivity.reports.breaks.length = 0;

    const { wrapper } = renderBreakGrid(updateData);

    expect(wrapper.getByText('txt_no_data')).toBeInTheDocument();
  });
});
