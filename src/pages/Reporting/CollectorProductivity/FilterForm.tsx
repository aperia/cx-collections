import { Formik } from 'formik';
import React from 'react';
import { useSelector } from 'react-redux';
import { collectorProductivityFormValidationSchema } from '../helpers';
import { SettingFilter } from '../types';
import FilterFormDetail from './FilterFormDetail';
import { selectedFilter, selectedMountedStatus } from './_redux/selectors';

interface FilterFormProps {
  onSubmit: (updatedValues: SettingFilter) => void;
}

export const FilterForm: React.FC<FilterFormProps> = ({ onSubmit }) => {
  const filters = useSelector(selectedFilter);
  const isMounted = useSelector(selectedMountedStatus);

  const handleSubmit = (updatedValues: SettingFilter) => {
    return onSubmit(updatedValues);
  };

  return (
    <Formik
      initialValues={filters}
      validationSchema={collectorProductivityFormValidationSchema}
      validateOnChange={true}
      validateOnBlur={true}
      validateOnMount={!isMounted}
      onSubmit={handleSubmit}
    >
      {props => <FilterFormDetail {...props} />}
    </Formik>
  );
};
