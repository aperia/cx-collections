import { fireEvent, screen, waitFor } from '@testing-library/dom';
import { I18N_PROMISES_PAYMENTS_STATISTICS } from 'app/constants/i18n';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { FormikProps } from 'formik';
import React from 'react';
import { collectorsTeamsComboBoxActions } from '../CollectorsTeamsComboBox/_redux/reducers';
import { SettingFilter } from '../types';
import { DEFAULT_FILTER, TIME_PERIOD_VALUE } from './constants';
import FilterFormDetail from './FilterFormDetail';
import { DateRangePickerError, FilterItem } from './types';
import { collectorProductivityActions } from './_redux/reducers';
import { ToggleButtonProps } from 'app/components/ToggleButton';
import { DateRangePickerProps } from 'app/_libraries/_dls/components';
import 'app/_libraries/_dls/test-utils/mocks/mockCanvas';

jest.mock(
  'app/components/ToggleButton',
  () =>
    ({ onToggleCollapsed, collapsed }: ToggleButtonProps) =>
      (
        <button
          onClick={(e: any) => {
            if (onToggleCollapsed) {
              onToggleCollapsed(e);
            }
          }}
        >
          Collapse Button
        </button>
      )
);

jest.mock(
  'app/_libraries/_dls/components/DateRangePicker',
  () =>
    ({ onBlur, error }: DateRangePickerProps) =>
      (
        <>
          <div>{error?.message}</div>
          <input type="text" placeholder="startDate" onBlur={onBlur} />
          <input type="text" placeholder="endDate" onBlur={onBlur} />
        </>
      )
);

const mockCollectorProductivityActions = mockActionCreator(
  collectorProductivityActions
);

const mockCollectorsTeamsComboBoxActions = mockActionCreator(
  collectorsTeamsComboBoxActions
);

const mockTimePeriods: FilterItem[] = [
  {
    id: 'today',
    value: 'Today',
    label: 'txt_today'
  },
  {
    id: 'currentMonth',
    value: 'Current Month',
    label: 'txt_current_month'
  },
  {
    id: 'dateRange',
    value: 'Date Range',
    label: 'txt_date_range'
  }
];

const mockReportTypes: FilterItem[] = [
  {
    id: 'individual',
    value: 'Individual',
    label: 'txt_individual'
  },
  {
    id: 'roll_up',
    value: 'Roll Up',
    label: 'txt_roll_up'
  }
];

const adminConfig = () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      isAdminRole: true,
      isSupervisor: true
    } as CommonConfig
  };
};

const collectorConfig = () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      isCollector: true
    } as CommonConfig
  };
};

describe('should test filter form detail', () => {
  const filterFormState: Partial<RootState> = {
    collectorProductivity: {
      filterRefData: {
        timePeriods: mockTimePeriods,
        reportTypes: mockReportTypes
      }
    }
  };

  const mockFilterFormProps = {
    values: {
      timePeriod: TIME_PERIOD_VALUE.DATE_RANGE
    },
    errors: {},
    isValid: true,
    touched: {
      rangeDatePicker: true,
      collector: true
    },
    setValues: value => {},
    setTouched: value => {},
    submitForm: () => {},
    handleChange: (value: any) => {},
    handleBlur: (value: any) => {},
    validateForm: value => {},
    validateField: value => {},
    resetForm: value => {}
  } as FormikProps<SettingFilter>;

  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<FilterFormDetail {...mockFilterFormProps} />, {
      initialState
    });
  };

  it('should ', async () => {
    jest.useFakeTimers();

    renderWrapper(filterFormState);

    jest.runAllTimers();
  });

  describe('should test toggle button', () => {
    it('should handle toggle button event', () => {
      jest.useFakeTimers();

      const mockToggleCollapse = mockCollectorProductivityActions(
        'setToggleSettingsSection'
      );

      renderWrapper(filterFormState);

      jest.runAllTimers();

      const toggleButton = screen.getByRole('button', {
        name: 'Collapse Button'
      });

      toggleButton.click();

      expect(mockToggleCollapse).toBeCalledWith(
        !filterFormState.collectorProductivity?.isCollapsed
      );
    });
  });

  describe('should test formik', () => {
    beforeAll(() => {
      adminConfig();
    });

    it('should test handling time period', async () => {
      jest.useFakeTimers();

      renderWrapper(filterFormState);

      jest.runAllTimers();

      const dateRangeRadio = screen.getByRole('radio', {
        name: 'txt_today'
      });

      await waitFor(() => {
        fireEvent.click(dateRangeRadio);
      });
    });

    it('should test handling report type', async () => {
      jest.useFakeTimers();

      const mockSetReportTypeComboBox = mockCollectorsTeamsComboBoxActions(
        'setReportTypeComboBox'
      );

      renderWrapper(filterFormState);

      jest.runAllTimers();

      const rollUp = screen.getByRole('radio', {
        name: 'txt_roll_up'
      });

      await waitFor(() => {
        fireEvent.click(rollUp);
      });

      expect(mockSetReportTypeComboBox).not.toBeCalled();
    });

    it('should test submit button event', async () => {
      jest.useFakeTimers();

      renderWrapper(filterFormState);

      jest.runAllTimers();

      const submitButton = screen.getByRole('button', {
        name: I18N_PROMISES_PAYMENTS_STATISTICS.RUN
      });

      expect(submitButton).toBeInTheDocument();

      submitButton.click();
    });

    it('should test reset previous button event', () => {
      jest.useFakeTimers();

      const mockUpdateFilter = mockCollectorProductivityActions('updateFilter');

      renderWrapper(filterFormState);

      jest.runAllTimers();

      const resetButton = screen.getByRole('button', {
        name: I18N_PROMISES_PAYMENTS_STATISTICS.RESET_TO_DEFAULT
      });

      resetButton.click();

      expect(mockUpdateFilter).toBeCalledWith(DEFAULT_FILTER);
    });

    it('should handling role undefined', async () => {
      jest.useFakeTimers();

      collectorConfig();

      const mockProps = {
        ...mockFilterFormProps,
        touched: {
          rangeDatePicker: false
        }
      } as FormikProps<SettingFilter>;

      renderMockStore(<FilterFormDetail {...mockProps} />, {
        initialState: {
          ...filterFormState
        }
      });

      jest.runAllTimers();

      expect(
        screen.queryByRole('radio', {
          name: 'txt_roll_up'
        })
      ).toBeNull();
    });
  });

  describe('should handle date range picker error', () => {
    it('should handle without start date', () => {
      jest.useFakeTimers();

      const mockProps = {
        ...mockFilterFormProps,
        errors: {
          rangeDatePicker: {
            start: 'error'
          } as DateRangePickerError
        }
      } as FormikProps<SettingFilter>;

      renderMockStore(<FilterFormDetail {...mockProps} />, {
        initialState: filterFormState
      });

      jest.runAllTimers();
    });
    it('should handle without end date', () => {
      jest.useFakeTimers();

      const mockProps = {
        ...mockFilterFormProps,
        errors: {
          rangeDatePicker: {
            end: 'error'
          } as DateRangePickerError
        }
      } as FormikProps<SettingFilter>;

      renderMockStore(<FilterFormDetail {...mockProps} />, {
        initialState: filterFormState
      });

      jest.runAllTimers();
    });
  });
});
