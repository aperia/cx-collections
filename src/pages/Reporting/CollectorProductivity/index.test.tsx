import React from 'react';
import {
  mockActionCreator,
  queryByClass,
  renderMockStore
} from 'app/test-utils';
import CollectorProductivity from '.';
import { DEFAULT_FILTER, TIME_PERIOD_VALUE } from './constants';
import { fireEvent, screen } from '@testing-library/react';
import { collectorProductivityActions } from './_redux/reducers';
import * as FilterForm from './FilterForm';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

const mockCollectorProductivityActions = mockActionCreator(
  collectorProductivityActions
);

describe('should test collector productivity', () => {
  const withCollectorProductivityStates: Partial<RootState> = {
    collectorProductivity: {
      filters: {
        ...DEFAULT_FILTER,
        timePeriod: TIME_PERIOD_VALUE.DATE_RANGE
      },
      filterRefData: {
        timePeriods: [],
        reportTypes: []
      },
      collectors: [],
      teams: []
    }
  };

  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<CollectorProductivity />, {
      initialState
    });
  };

  describe('should render UI', () => {
    it('should test get filter ref data', () => {
      jest.useFakeTimers();

      const mockGetFilterRefDataAction =
        mockCollectorProductivityActions('getFilterRefData');

      renderWrapper(withCollectorProductivityStates);

      jest.runAllTimers();

      expect(mockGetFilterRefDataAction).toBeCalled();
    });

    it('should test submit form', () => {
      const spy = jest
        .spyOn(FilterForm, 'FilterForm')
        .mockImplementation(({ onSubmit }) => (
          <div>
            <button data-testid="submitBtn" onClick={() => onSubmit({} as any)}>
              run
            </button>
          </div>
        ));

      const mockUpdateFilter = mockCollectorProductivityActions('updateFilter');

      jest.useFakeTimers();

      renderWrapper(withCollectorProductivityStates);

      jest.runAllTimers();

      const submitBtn = screen.getByTestId('submitBtn');
      submitBtn.click();
      expect(mockUpdateFilter).toBeCalled();
      spy.mockReset();
      spy.mockRestore();
    });
  });

  describe('should test handling scroll event', () => {
    it('scroll top 0', () => {
      jest.useFakeTimers();

      const { baseElement } = renderWrapper(withCollectorProductivityStates);

      jest.runAllTimers();

      fireEvent.scroll(window, {
        target: {
          scrollTop: 0
        }
      });

      expect(queryByClass(baseElement, /br-white/)).toBeInTheDocument();
    });

    it('scroll more than 0', () => {
      jest.useFakeTimers();

      const { baseElement } = renderWrapper(withCollectorProductivityStates);

      jest.runAllTimers();

      fireEvent.scroll(window, {
        target: {
          scrollTop: 10
        }
      });

      expect(queryByClass(baseElement, /br-white/)).not.toBeInTheDocument();
    });
  });
});
