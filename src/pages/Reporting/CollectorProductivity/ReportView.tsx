import React, { useCallback, useEffect } from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';
import { GoalGridView } from './GoalGridView';
import { BreakGridView } from './BreakGridView';

// components
import { Button, Icon } from 'app/_libraries/_dls/components';
import { View } from 'app/_libraries/_dof/core';

// redux
import { reportingTabsActions } from '../ReportingTabs/_redux/reducers';
import { selectedFilter, selectedReport } from './_redux/selectors';
import { collectorProductivityActions } from './_redux/reducers';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_COLLECTOR_PRODUCTIVITY } from 'app/constants/i18n';
import { TAB_IDS } from '../ReportingTabs/types';
import { VIEW_NAME } from './constants';
import { promisesPaymentsStatisticsActions } from '../PromisesPaymentsStatistics/_redux/reducers';
import { collectorsTeamsComboBoxActions } from '../CollectorsTeamsComboBox/_redux/reducers';
import { genAmtId } from 'app/_libraries/_dls/utils';

export const ReportView: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const filter = useSelector(selectedFilter);
  const { productivityStatistics, goals, breaks } = useSelector(selectedReport);

  const handleViewPromisesPaymentsStatistics = useCallback(() => {
    batch(() => {
      dispatch(
        reportingTabsActions.changeTab(TAB_IDS.PROMISES_PAYMENTS_STATISTICS)
      );
      dispatch(
        promisesPaymentsStatisticsActions.setInitialFilter({
          reportType: filter?.reportType,
          collector: filter?.collector,
          team: filter?.team
        })
      );
      dispatch(collectorsTeamsComboBoxActions.reset());
    });
  }, [dispatch, filter?.collector, filter?.reportType, filter?.team]);

  useEffect(() => {
    if (!filter?.collector && !filter?.team) {
      dispatch(collectorProductivityActions.setReports({}));
      return;
    }
    dispatch(collectorProductivityActions.getStatistics(filter));
  }, [dispatch, filter]);

  if (!productivityStatistics && !goals && !breaks) {
    return (
      <div className="my-80 text-center">
        <Icon name="file" className="fs-80 color-light-l12" />
        <p className="color-grey mt-20">
          {t(I18N_COLLECTOR_PRODUCTIVITY.NO_DATA)}
        </p>
      </div>
    );
  }

  return (
    <div className="px-24 pb-24 pt-16">
      <div className="d-flex align-items-center justify-content-between">
        <h5 data-testid={genAmtId('collectorProductivity-report', 'title', '')}>
          {t(I18N_COLLECTOR_PRODUCTIVITY.REPORT)}
        </h5>
        <Button
          className="mr-n8"
          size="sm"
          variant="outline-primary"
          onClick={handleViewPromisesPaymentsStatistics}
          dataTestId={genAmtId('collectorProductivity-report', 'view-btn', '')}
        >
          {t(I18N_COLLECTOR_PRODUCTIVITY.VIEW_PROMISES_PAYMENTS_STATISTICS)}
        </Button>
      </div>
      <View
        id={VIEW_NAME.statistics}
        formKey={VIEW_NAME.statistics}
        descriptor={VIEW_NAME.statistics}
        value={productivityStatistics}
      />
      <GoalGridView />
      <BreakGridView />
    </div>
  );
};
