import React from 'react';
import * as formik from 'formik';
import { DEFAULT_FILTER } from './constants';
import { renderMockStore } from 'app/test-utils';
import { FilterForm } from './FilterForm';
import { screen } from '@testing-library/dom';

const mockSubmit = jest.fn();

const mockFormik =
  (values: Record<string, any> | undefined) =>
  ({ onSubmit }: any) => {
    return (
      <>
        <button data-testid="submitBtn" onClick={() => onSubmit(values)}>
          run
        </button>
      </>
    );
  };

describe('should test filter form', () => {
  it('should test submit event', () => {
    jest.spyOn(formik, 'Formik').mockImplementation(
      mockFormik({
        ...DEFAULT_FILTER
      })
    );
    renderMockStore(<FilterForm onSubmit={mockSubmit} />, {
      initialState: {
        collectorProductivity: {
          filters: DEFAULT_FILTER
        }
      }
    });

    const submitBtn = screen.getByTestId('submitBtn');
    submitBtn.click();
    expect(mockSubmit).toBeCalled();
  });
});
