import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
  useState
} from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// components
import { ReportView } from './ReportView';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { selectedLoading } from './_redux/selectors';
import { collectorProductivityActions } from './_redux/reducers';
import { collectorsTeamsComboBoxActions } from '../CollectorsTeamsComboBox/_redux/reducers';

// constants
import { I18N_COLLECTOR_PRODUCTIVITY } from 'app/constants/i18n';
import { SimpleBar } from 'app/_libraries/_dls/components';

// helpers
import debounce from 'lodash.debounce';
import classNames from 'classnames';
import { SettingFilter } from '../types';
import { FilterForm } from './FilterForm';
import { genAmtId } from 'app/_libraries/_dls/utils';

const CollectorProductivity: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const rightRef = useRef<HTMLDivElement | null>(null);
  const [isShowBorder, setIsShowBorder] = useState(false);

  const loading = useSelector(selectedLoading);

  useEffect(() => {
    dispatch(collectorsTeamsComboBoxActions.resetComboBoxData());
    dispatch(collectorProductivityActions.getFilterRefData());
    return () => {
      dispatch(collectorsTeamsComboBoxActions.resetComboBoxData());
    };
  }, [dispatch]);

  useLayoutEffect(() => {
    rightRef.current && (rightRef.current.style.height = `calc(100vh - 313px)`);
  }, []);

  useEffect(() => {
    const handleWindowScroll = (event: Event) => {
      const target = event.target as HTMLElement;

      if (target?.scrollTop > 0) {
        setIsShowBorder(true);
      } else {
        setIsShowBorder(false);
      }
    };

    const handleWindowScrollDebounced = debounce(handleWindowScroll, 50, {
      leading: true,
      trailing: true
    });

    window.addEventListener('scroll', handleWindowScrollDebounced, true);
    return () => {
      window.removeEventListener('scroll', handleWindowScrollDebounced, true);
    };
  }, [isShowBorder]);

  const handleSubmit = useCallback(
    (updatedValues: SettingFilter) => {
      dispatch(collectorProductivityActions.updateFilter(updatedValues));
    },
    [dispatch]
  );

  return (
    <div className={classNames('bg-white', { loading })}>
      <h4
        className={classNames(
          'p-24 border-bottom',
          isShowBorder ? '' : 'br-white'
        )}
        data-testid={genAmtId('collectorProductivity', 'title', '')}
      >
        {t(I18N_COLLECTOR_PRODUCTIVITY.COLLECTOR_PRODUCTIVITY)}
      </h4>
      <div className="d-flex">
        <FilterForm onSubmit={handleSubmit} />
        <div ref={rightRef} className="flex-1">
          <SimpleBar>
            <ReportView />
          </SimpleBar>
        </div>
      </div>
    </div>
  );
};

export default CollectorProductivity;
