import { REPORT_TYPE_VALUE } from '../constants';
import { SettingFilter } from '../types';

export enum TIME_PERIOD_VALUE {
  TODAY = 'Today',
  CURRENT_MONTH = 'Current Month',
  DATE_RANGE = 'Date Range'
}

export const DEFAULT_FILTER: SettingFilter = {
  timePeriod: TIME_PERIOD_VALUE.TODAY,
  rangeDatePicker: undefined,
  reportType: REPORT_TYPE_VALUE.INDIVIDUAL
};

export const VIEW_NAME = {
  statistics: 'productivityStatisticsView',
  breaks: 'breakStatisticsView'
};
