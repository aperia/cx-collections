import { I18N_COLLECTOR_PRODUCTIVITY } from 'app/constants/i18n';
import { ColumnType, Grid } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { View } from 'app/_libraries/_dof/core';
import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { VIEW_NAME } from './constants';
import { Break } from './types';
import { selectedBreakOverview, selectedBreaks } from './_redux/selectors';

export const BreakGridView: React.FC = () => {
  const { t } = useTranslation();
  const breaks = useSelector(selectedBreaks);
  const breakOverview = useSelector(selectedBreakOverview);

  const BREAK_GRID_COLUMNS: ColumnType<Break>[] = useMemo(
    () => [
      {
        id: 'breakCode',
        Header: t(I18N_COLLECTOR_PRODUCTIVITY.CODE),
        isSort: false,
        width: 80,
        accessor: 'code'
      },
      {
        id: 'breakActivity',
        Header: t(I18N_COLLECTOR_PRODUCTIVITY.ACTIVITY),
        isSort: false,
        width: 250,
        accessor: 'activity'
      },
      {
        id: 'breakTotalTime',
        Header: t(I18N_COLLECTOR_PRODUCTIVITY.TOTAL_TIME),
        isSort: false,
        width: 160,
        accessor: 'totalTime'
      },
      {
        id: 'breakTotalNumber',
        Header: t(I18N_COLLECTOR_PRODUCTIVITY.TOTAL_NUMBER),
        isSort: false,
        width: 160,
        accessor: 'totalNumber'
      },
      {
        id: 'breakAverageTime',
        Header: t(I18N_COLLECTOR_PRODUCTIVITY.AVERAGE_TIME),
        isSort: false,
        width: 160,
        accessor: 'averageTime'
      }
    ],
    [t]
  );

  return (
    <div className="mt-16 pt-16 border-top">
      <h6
        className="color-grey"
        data-testid={genAmtId('breakGridView', 'title', '')}
      >
        {t(I18N_COLLECTOR_PRODUCTIVITY.BREAKS)}
      </h6>
      {breaks?.length > 0 ? (
        <>
          <View
            id={VIEW_NAME.breaks}
            formKey={VIEW_NAME.breaks}
            descriptor={VIEW_NAME.breaks}
            value={breakOverview}
          />
          <Grid
            className="mt-16"
            columns={BREAK_GRID_COLUMNS}
            data={breaks}
            dataTestId="breakGridView"
          />
        </>
      ) : (
        <p
          className="mt-16 color-grey"
          data-testid={genAmtId('breakGridView', 'no-data', '')}
        >
          {t(I18N_COLLECTOR_PRODUCTIVITY.NO_DATA)}
        </p>
      )}
    </div>
  );
};
