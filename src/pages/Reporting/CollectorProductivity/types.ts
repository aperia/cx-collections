import { Collector, Team } from '../CollectorsTeamsComboBox/types';
import { SettingFilter } from '../types';

export interface CollectorProductivityState {
  loading?: boolean;
  filters?: SettingFilter;
  filterRefData?: FilterRefData;
  reportTypeComboBox?: ReportTypeComboBox;
  collectors?: Collector[];
  teams?: Team[];
  reports?: Reports;
  isCollapsed?: boolean;
  isMounted?: boolean;
}

export interface FilterItem {
  id: string;
  label?: string;
  value?: string;
  name?: string;
}

export interface FilterRefData {
  timePeriods?: FilterItem[];
  reportTypes?: FilterItem[];
}

export interface GetFilterDataPayload {
  timePeriods?: FilterItem[];
  reportTypes?: FilterItem[];
}

export interface GetFilterDataArgs {}

export interface ReportTypeComboBox {
  filter?: string;
  itemsLazyLoad?: JSX.Element[];
  opened?: boolean;
}

export interface ProductivityStatistics {
  callsTaken?: number;
  accountsWorked?: number;
  pendingPromises?: number;
  keptPromises?: number;
  brokenPromises?: number;
  totalPromises?: number;
  pendingAmount?: number;
  keptAmount?: number;
  brokenAmount?: number;
  totalAmount?: number;
  percentPromisesVsWorked?: number;
  promiseBalance?: number;
  amountCollected?: number;
  percentPromisesKept?: number;
}

export interface Goal {
  id: string;
  description?: string;
  goal?: number;
  actual?: number;
}

export interface Break {
  code: number;
  activity?: string;
  totalTime?: string;
  totalNumber?: number;
  averageTime?: string;
}

export interface Reports {
  productivityStatistics?: ProductivityStatistics;
  goals?: Goal[];
  breaks?: Break[];
  breakOverview?: BreakOverview;
}

export interface BreakOverview {
  totalBreaks?: number;
  totalBreakTime?: string;
  averageSecondPerCall?: string;
  totalSystemTime?: string;
}

export interface GetStatisticsPayload extends Reports {}
export interface GetStatisticsArgs {
  userName?: string;
  queues?: any[];
  startDateTime?: string;
  endDateTime?: string;
}

export interface DateRangePickerError {
  start?: string;
  end?: string;
}
