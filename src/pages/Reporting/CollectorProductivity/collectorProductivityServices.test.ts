import { mockApiServices } from 'app/test-utils';
import { apiUrl, mockAppConfigApi } from 'app/test-utils/mocks/mockProperty';
import { collectorProductivityServices } from './collectorProductivityServices';

const adminConfig = () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      isAdminRole: true,
      isSupervisor: true
    } as CommonConfig
  };
};

const collectorConfig = () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      isCollector: true
    } as CommonConfig
  };
};

describe('get time periods', () => {
  it('when url was not defined with admin role', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.get();

    adminConfig();

    collectorProductivityServices.getTimePeriods();

    expect(mockService).toBeCalledWith('');
  });

  it('when url was not defined with collector role', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.get();

    collectorConfig();

    collectorProductivityServices.getTimePeriods();

    expect(mockService).toBeCalledWith('');
  });

  it('when url was defined with admin role', () => {
    mockAppConfigApi.setup();

    const mockService = mockApiServices.get();

    adminConfig();

    collectorProductivityServices.getTimePeriods();

    expect(mockService).toBeCalledWith(
      apiUrl.collectorProductivity.getAdminTimePeriods
    );
  });

  it('when url was defined with collector role', () => {
    mockAppConfigApi.setup();

    const mockService = mockApiServices.get();

    collectorConfig();

    collectorProductivityServices.getTimePeriods();

    expect(mockService).toBeCalledWith(
      apiUrl.collectorProductivity.getTimePeriods
    );
  });
});
describe('get report types', () => {
  it('when url was not defined', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.get();

    adminConfig();

    collectorProductivityServices.getReportTypes();

    expect(mockService).toBeCalledWith('');
  });

  it('when url was defined with admin role', () => {
    mockAppConfigApi.setup();

    const mockService = mockApiServices.get();

    adminConfig();

    collectorProductivityServices.getReportTypes();

    expect(mockService).toBeCalledWith(
      apiUrl.collectorProductivity.getReportTypes
    );
  });

  it('when url was defined with collector role', () => {
    mockAppConfigApi.setup();

    const mockService = mockApiServices.get();

    collectorConfig();

    collectorProductivityServices.getReportTypes();

    expect(mockService).not.toBeCalled();
  });
});
