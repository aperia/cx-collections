import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { ReportView } from './ReportView';
import { screen } from '@testing-library/dom';
import { I18N_COLLECTOR_PRODUCTIVITY } from 'app/constants/i18n';
import { DEFAULT_FILTER } from './constants';
import { collectorProductivityActions } from './_redux/reducers';
import { reportingTabsActions } from '../ReportingTabs/_redux/reducers';
import { TAB_IDS } from '../ReportingTabs/types';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

const mockCollectorProductivityActions = mockActionCreator(
  collectorProductivityActions
);

const mockReportingTabsActions = mockActionCreator(reportingTabsActions);

const reportViewStates: Partial<RootState> = {
  collectorProductivity: {
    filters: {
      ...DEFAULT_FILTER,
      collector: { id: '1', name: '1' },
      team: { id: '1', name: '1' }
    },
    reports: {
      productivityStatistics: {
        callsTaken: 10
      },
      goals: [],
      breaks: []
    }
  }
};

describe('should test report view', () => {
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<ReportView />, {
      initialState
    });
  };

  it('should render UI', () => {
    jest.useFakeTimers();

    const mockGetStatistics = mockCollectorProductivityActions('getStatistics');

    renderWrapper(reportViewStates);

    jest.runAllTimers();

    expect(mockGetStatistics).toBeCalledWith(
      reportViewStates.collectorProductivity?.filters
    );

    expect(
      screen.getByText(I18N_COLLECTOR_PRODUCTIVITY.REPORT)
    ).toBeInTheDocument();
  });

  it('should handle view promises payments statistics button', () => {
    jest.useFakeTimers();

    const mockChangeTabAction = mockReportingTabsActions('changeTab');

    renderWrapper(reportViewStates);

    jest.runAllTimers();

    const viewButton = screen.getByRole('button', {
      name: I18N_COLLECTOR_PRODUCTIVITY.VIEW_PROMISES_PAYMENTS_STATISTICS
    });

    viewButton.click();

    expect(mockChangeTabAction).toBeCalledWith(
      TAB_IDS.PROMISES_PAYMENTS_STATISTICS
    );
  });
});
