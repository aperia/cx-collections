import { I18N_COLLECTOR_PRODUCTIVITY } from 'app/constants/i18n';
import { ColumnType, Grid } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import React, { useMemo } from 'react';
import { useSelector } from 'react-redux';
import { Goal } from './types';
import { selectedGoals } from './_redux/selectors';

export const GoalGridView: React.FC = () => {
  const { t } = useTranslation();
  const goals = useSelector(selectedGoals);

  const GOAL_GRID_COLUMNS: ColumnType<Goal>[] = useMemo(
    () => [
      {
        id: 'goalId',
        Header: t(I18N_COLLECTOR_PRODUCTIVITY.GOAL_ID),
        isSort: false,
        width: 170,
        accessor: 'id'
      },
      {
        id: 'goldDescription',
        Header: t(I18N_COLLECTOR_PRODUCTIVITY.DESCRIPTION),
        isSort: false,
        width: 250,
        accessor: 'description'
      },
      {
        id: 'goalNumber',
        Header: t(I18N_COLLECTOR_PRODUCTIVITY.GOAL),
        isSort: false,
        width: 200,
        accessor: 'goal'
      },
      {
        id: 'goalActual',
        Header: t(I18N_COLLECTOR_PRODUCTIVITY.ACTUAL),
        isSort: false,
        width: 200,
        accessor: 'actual'
      }
    ],
    [t]
  );

  return (
    <div className="mt-16 pt-16 border-top">
      <h6
        className="color-grey"
        data-testid={genAmtId('goalGridView', 'title', '')}
      >
        {t(I18N_COLLECTOR_PRODUCTIVITY.GOALS)}
      </h6>
      {goals?.length > 0 ? (
        <Grid
          className="mt-16"
          columns={GOAL_GRID_COLUMNS}
          data={goals}
          dataTestId={genAmtId('goalGridView', 'grid', '')}
        />
      ) : (
        <p className="mt-16 color-grey">
          {t(I18N_COLLECTOR_PRODUCTIVITY.NO_DATA)}
        </p>
      )}
    </div>
  );
};
