import { GoalGridView } from './GoalGridView';
import { renderMockStore } from 'app/test-utils';
import React from 'react';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponent = jest.requireActual('app/_libraries/_dls/components');
  const { MockGrid } = jest.requireActual('app/test-utils/mocks/MockGrid');

  return {
    ...dlsComponent,
    Grid: MockGrid
  };
});

describe('Test Goals Gridview component', () => {
  const initStateData = {
    collectorProductivity: {
      reports: {
        goals: [
          {
            id: '1',
            goals: 2
          }
        ]
      }
    }
  };

  const renderGoalGrid = (storeConfig?: Record<string, any>) => {
    return renderMockStore(<GoalGridView />, {
      initialState: { ...initStateData, ...storeConfig }
    });
  };

  it('should render Goal Grid UI with goals data', () => {
    const { wrapper } = renderGoalGrid();

    expect(wrapper.getByText('txt_goals')).toBeInTheDocument();
    expect(
      wrapper.baseElement.querySelector('.dls-grid-body')?.childElementCount
    ).toEqual(1);
  });

  it('should render Goal Grid UI without goals data', () => {
    const updateData = { ...initStateData };
    updateData.collectorProductivity.reports.goals.length = 0;

    const { wrapper } = renderGoalGrid(updateData);

    expect(wrapper.getByText('txt_no_data')).toBeInTheDocument();
  });
});
