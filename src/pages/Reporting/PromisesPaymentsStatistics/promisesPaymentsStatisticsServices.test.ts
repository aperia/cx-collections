import { promisesPaymentsStatisticsServices } from './promisesPaymentsStatisticsServices';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import { GetReportsArgs } from './types';

const adminConfig = () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      isAdminRole: true,
      isSupervisor: true
    } as CommonConfig
  };
};

const collectorConfig = () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      isCollector: true
    } as CommonConfig
  };
};

describe('promisesPaymentsStatisticsServices', () => {
  describe('getReports', () => {
    const params: GetReportsArgs = {
      fromAmount: '324',
      toAmount: '575'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      promisesPaymentsStatisticsServices.getReports(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      promisesPaymentsStatisticsServices.getReports(params);

      expect(mockService).toBeCalledWith(
        apiUrl.promisesPaymentsStatistics.getReports,
        params
      );
    });
  });

  describe('getTimePeriods', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      promisesPaymentsStatisticsServices.getTimePeriods();

      expect(mockService).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      promisesPaymentsStatisticsServices.getTimePeriods();

      expect(mockService).toBeCalledWith(
        apiUrl.promisesPaymentsStatistics.getTimePeriods
      );
    });
  });

  describe('getPromiseStatuses', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      promisesPaymentsStatisticsServices.getPromiseStatuses();

      expect(mockService).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      promisesPaymentsStatisticsServices.getPromiseStatuses();

      expect(mockService).toBeCalledWith(
        apiUrl.promisesPaymentsStatistics.getPromiseStatuses
      );
    });
  });

  describe('getReportTypes', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      adminConfig();

      const mockService = mockApiServices.get();

      promisesPaymentsStatisticsServices.getReportTypes();

      expect(mockService).toBeCalledWith('');
    });

    it('when url was defined with admin role', () => {
      mockAppConfigApi.setup();

      adminConfig();

      const mockService = mockApiServices.get();

      promisesPaymentsStatisticsServices.getReportTypes();

      expect(mockService).toBeCalledWith(
        apiUrl.promisesPaymentsStatistics.getReportTypes
      );
    });

    it('when url was defined with collector role', () => {
      mockAppConfigApi.setup();

      collectorConfig();

      const mockService = mockApiServices.get();

      promisesPaymentsStatisticsServices.getReportTypes();

      expect(mockService).not.toBeCalled();
    });

    it('when role is undefined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      promisesPaymentsStatisticsServices.getReportTypes();

      expect(mockService).not.toBeCalled();
    });
  });

  describe('getPromiseTypes', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      adminConfig();

      const mockService = mockApiServices.get();

      promisesPaymentsStatisticsServices.getPromiseTypes();

      expect(mockService).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      adminConfig();

      const mockService = mockApiServices.get();

      promisesPaymentsStatisticsServices.getPromiseTypes();

      expect(mockService).toBeCalledWith(
        apiUrl.promisesPaymentsStatistics.getPromiseTypes
      );
    });

    it('when url was defined with collector role', () => {
      mockAppConfigApi.setup();

      collectorConfig();

      const mockService = mockApiServices.get();

      promisesPaymentsStatisticsServices.getPromiseTypes();

      expect(mockService).not.toBeCalled();
    });

    it('when role is undefined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      promisesPaymentsStatisticsServices.getPromiseTypes();

      expect(mockService).not.toBeCalled();
    });
  });
});
