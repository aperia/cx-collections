import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
  useState
} from 'react';
import { debounce } from 'lodash';
import classNames from 'classnames';

// components
import { SimpleBar } from 'app/_libraries/_dls/components';
import ReportList from './ReportList';
import { ToggleButton } from 'app/components';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { FilterForm } from './FilterForm';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { promisesPaymentsStatisticsActions } from './_redux/reducers';
import { selectedCollapsedStatus, selectedLoading } from './_redux/selectors';
import { collectorsTeamsComboBoxActions } from '../CollectorsTeamsComboBox/_redux/reducers';

// constants
import { I18N_REPORTING_TABS } from '../ReportingTabs/constants';

// Type
import { SettingFilter } from '../types';

const PromisesPaymentsStatistics: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const rightRef = useRef<HTMLDivElement | null>(null);
  const [isShowBorder, setIsShowBorder] = useState(false);

  const collapsed = useSelector(selectedCollapsedStatus) || false;
  const loading = useSelector(selectedLoading);

  useEffect(() => {
    dispatch(collectorsTeamsComboBoxActions.resetComboBoxData());
    dispatch(promisesPaymentsStatisticsActions.getFilterRefData());
    return () => {
      dispatch(collectorsTeamsComboBoxActions.resetComboBoxData());
      dispatch(promisesPaymentsStatisticsActions.setMountStatus(false));
    };
  }, [dispatch]);

  const handleToggleButtonEvent = useCallback(() => {
    dispatch(
      promisesPaymentsStatisticsActions.setToggleSettingsSection(!collapsed)
    );
  }, [dispatch, collapsed]);

  useLayoutEffect(() => {
    rightRef.current && (rightRef.current.style.height = `calc(100vh - 313px)`);
  }, []);

  useEffect(() => {
    const handleWindowScroll = (event: Event) => {
      const target = event.target as HTMLElement;

      if (target?.scrollTop > 0) {
        setIsShowBorder(true);
      } else {
        setIsShowBorder(false);
      }
    };

    const handleWindowScrollDebounced = debounce(handleWindowScroll, 50, {
      leading: true,
      trailing: true
    });

    window.addEventListener('scroll', handleWindowScrollDebounced, true);
    return () => {
      window.removeEventListener('scroll', handleWindowScrollDebounced, true);
    };
  }, [isShowBorder]);

  const handleSubmitForm = useCallback(
    (values: SettingFilter) => {
      dispatch(promisesPaymentsStatisticsActions.updateFilter(values));
    },
    [dispatch]
  );

  return (
    <div className={classNames('bg-white', { loading })}>
      <h4
        className={classNames(
          'p-24 border-bottom',
          isShowBorder ? '' : 'br-white'
        )}
      >
        {t(I18N_REPORTING_TABS.PROMISES_PAYMENTS_STATISTICS)}
      </h4>
      <div className="d-flex">
        <div
          className={classNames('reporting-section', {
            collapsed
          })}
        >
          <ToggleButton
            collapseMessage={t(I18N_COMMON_TEXT.EXPAND)}
            expandMessage={t(I18N_COMMON_TEXT.COLLAPSE)}
            collapsed={!collapsed}
            onToggleCollapsed={handleToggleButtonEvent}
            dataTestId="togglePromisesPaymentsStatistics"
          />
          <FilterForm onSubmit={handleSubmitForm} />
        </div>
        <div ref={rightRef} className="flex-1">
          <SimpleBar>
            <ReportList />
          </SimpleBar>
        </div>
      </div>
    </div>
  );
};

export default PromisesPaymentsStatistics;
