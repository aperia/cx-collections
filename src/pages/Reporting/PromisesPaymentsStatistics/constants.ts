import { REPORT_TYPE_VALUE } from '../constants';
import { SettingFilter } from '../types';

export const FORM_IDS = {
  PROMISES_PAYMENTS_STATISTICS_SETTINGS_CORE: 'promisesPaymentsStatisticsCore',
  PROMISES_PAYMENTS_STATISTICS_SETTINGS_CORE_PLUS:
    'promisesPaymentsStatisticsCorePlus'
};

export const REPORT_STATUS = {
  KEPT: 'kept',
  BROKEN: 'broken',
  PENDING: 'pending'
};

export enum TIME_PERIOD_VALUE {
  MOST_300_RECENT_RECORDS = 'Most 300 recent records',
  ENTERED_PROMISE_DATE = 'Entered promise date',
  DUE_PROMISE_DATE = 'Due promise date'
}

export const DEFAULT_FILTER: SettingFilter = {
  timePeriod: TIME_PERIOD_VALUE.MOST_300_RECENT_RECORDS,
  ppsDatePicker: undefined,
  reportType: REPORT_TYPE_VALUE.INDIVIDUAL,
  collector: undefined,
  team: undefined,
  ppsNonRecurringMultiplePromise: false,
  ppsPromiseToPay: false,
  ppsRecurringMultiplePromise: false,
  ppsKeptPromise: false,
  ppsBrokenPromise: false,
  ppsPendingPromise: false,
  ppsPromiseAmountFrom: '',
  ppsPromiseAmountTo: ''
};
