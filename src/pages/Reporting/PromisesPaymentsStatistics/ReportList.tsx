import React, { useEffect, useMemo } from 'react';

// components
import {
  Badge,
  ColumnType,
  Grid,
  Icon,
  Pagination
} from 'app/_libraries/_dls/components';

// constants
import { REPORT_STATUS } from './constants';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { usePagination } from 'app/hooks';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { selectedReports, selectedFilters } from './_redux/selectors';
import { promisesPaymentsStatisticsActions } from './_redux/reducers';

// helpers
import isEmpty from 'lodash.isempty';
import isUndefined from 'lodash.isundefined';
import { formatCommon, formatTime } from 'app/helpers';
import { Report } from './types';
import { I18N_PROMISES_PAYMENTS_STATISTICS } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';

const ReportList: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const reports = useSelector(selectedReports);
  const filters = useSelector(selectedFilters);
  const testId = 'promisesPaymentsStatistics-reportList';

  useEffect(() => {
    if (
      isUndefined(filters) ||
      (isEmpty(filters?.collector) && isEmpty(filters?.team))
    ) {
      dispatch(promisesPaymentsStatisticsActions.setReports([]));
      return;
    }
    dispatch(promisesPaymentsStatisticsActions.getReports(filters));
  }, [dispatch, filters]);

  const REPORT_LIST_COLUMNS: ColumnType<Report>[] = useMemo(
    () => [
      {
        id: 'accountNumber',
        Header: t(I18N_PROMISES_PAYMENTS_STATISTICS.ACCOUNT_NUMBER),
        isSort: false,
        width: 160,
        accessor: 'accountNumber'
      },
      {
        id: 'type',
        Header: t(I18N_PROMISES_PAYMENTS_STATISTICS.TYPE),
        isSort: false,
        width: 200,
        accessor: 'type'
      },
      {
        id: 'method',
        Header: t(I18N_PROMISES_PAYMENTS_STATISTICS.METHOD),
        isSort: false,
        width: 140,
        accessor: 'method'
      },
      {
        id: 'promiseAmount',
        Header: t(I18N_PROMISES_PAYMENTS_STATISTICS.PROMISE_AMOUNT),
        isSort: false,
        width: 150,
        accessor: function accessor(data) {
          return formatCommon(data.promiseAmount || 0).currency();
        },
        cellProps: { className: 'text-right' }
      },
      {
        id: 'paidAmount',
        Header: t(I18N_PROMISES_PAYMENTS_STATISTICS.PAID_AMOUNT),
        isSort: false,
        width: 150,
        accessor: function accessor(data) {
          return formatCommon(data.paidAmount || 0).currency();
        },
        cellProps: { className: 'text-right' }
      },
      {
        id: 'status',
        Header: t(I18N_PROMISES_PAYMENTS_STATISTICS.STATUS),
        isSort: false,
        width: 140,
        accessor: function accessor(data) {
          if (isUndefined(data.status)) return '';
          switch (data.status.toLowerCase()) {
            case REPORT_STATUS.KEPT:
              return <Badge color="green">{data.status}</Badge>;
            case REPORT_STATUS.BROKEN:
              return <Badge color="red">{data.status}</Badge>;
            case REPORT_STATUS.PENDING:
              return <Badge color="orange">{data.status}</Badge>;
            default:
              return <Badge color="grey">{data.status}</Badge>;
          }
        }
      },
      {
        id: 'promiseDate',
        Header: t(I18N_PROMISES_PAYMENTS_STATISTICS.PROMISE_DATE),
        width: 140,
        accessor: data =>
          data.promiseDate ? formatTime(data.promiseDate).date : ''
      },
      {
        id: 'enteredDate',
        Header: t(I18N_PROMISES_PAYMENTS_STATISTICS.ENTERED_DATE),
        width: 140,
        accessor: data =>
          data.enteredDate ? formatTime(data.enteredDate).date : ''
      },
      {
        id: 'updatedDate',
        Header: t(I18N_PROMISES_PAYMENTS_STATISTICS.UPDATED_DATE),
        width: 140,
        accessor: data =>
          data.updatedDate ? formatTime(data.updatedDate).date : ''
      }
    ],
    [t]
  );

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange,
    setCurrentPage
  } = usePagination(reports);
  const showPagination = total > pageSize[0];

  useEffect(() => {
    setCurrentPage(1);
  }, [setCurrentPage, reports]);

  if (isEmpty(gridData)) {
    return (
      <div className="my-80 text-center">
        <Icon
          name="file"
          className="fs-80 color-light-l12"
          dataTestId={testId}
        />
        <p
          className="color-grey mt-20"
          data-testid={genAmtId(testId, 'no-date', '')}
        >
          {t(I18N_PROMISES_PAYMENTS_STATISTICS.NO_DATA)}
        </p>
      </div>
    );
  }

  return (
    <div className="px-24 pb-24 pt-16">
      <h5 data-testid={genAmtId(testId, 'title', '')}>
        {t(I18N_PROMISES_PAYMENTS_STATISTICS.REPORT)}
      </h5>
      <div className="mt-16">
        <Grid
          columns={REPORT_LIST_COLUMNS}
          data={gridData}
          dataTestId={testId}
        />
        {showPagination && (
          <div className="pt-16">
            <Pagination
              totalItem={total}
              pageSize={pageSize}
              pageNumber={currentPage}
              pageSizeValue={currentPageSize}
              compact
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
              dataTestId={testId}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default ReportList;
