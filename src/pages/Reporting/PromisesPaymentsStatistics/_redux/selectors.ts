import { createSelector } from '@reduxjs/toolkit';
import { DEFAULT_FILTER } from '../constants';
import { PromisesPaymentsStatisticsState } from '../types';

const getPromisesPaymentsStatistics = (states: RootState) =>
  states.promisesPaymentsStatistics || {};

export const selectedCollectors = createSelector(
  getPromisesPaymentsStatistics,
  (data: PromisesPaymentsStatisticsState) => data.collectors
);

export const selectedTeams = createSelector(
  getPromisesPaymentsStatistics,
  (data: PromisesPaymentsStatisticsState) => data.teams
);

export const selectedReports = createSelector(
  getPromisesPaymentsStatistics,
  (data: PromisesPaymentsStatisticsState) => data.reports || []
);

export const selectedFilters = createSelector(
  getPromisesPaymentsStatistics,
  (data: PromisesPaymentsStatisticsState) => data.filters || DEFAULT_FILTER
);

export const selectedLoading = createSelector(
  getPromisesPaymentsStatistics,
  (data: PromisesPaymentsStatisticsState) => data.loading
);

export const selectedFilterRefData = createSelector(
  getPromisesPaymentsStatistics,
  (data: PromisesPaymentsStatisticsState) => data.filterRefData
);

export const selectedCollapsedStatus = createSelector(
  getPromisesPaymentsStatistics,
  (data: PromisesPaymentsStatisticsState) => data.isCollapsed
);

export const selectedMountedStatus = createSelector(
  getPromisesPaymentsStatistics,
  (data: PromisesPaymentsStatisticsState) => data.isMounted
);

export const selectedDatePickerLabel = createSelector(
  getPromisesPaymentsStatistics,
  (data: PromisesPaymentsStatisticsState) => data.datePickerLabel
);
