import * as selector from './selectors';
import { selectorWrapper } from 'app/test-utils';
import { DEFAULT_FILTER } from '../constants';

const mockCollectorData = [
  { id: '1', name: 'collector 1' },
  { id: '2', name: 'collector 2' }
];

const mockTeamData = [
  { id: '1', name: 'team 1' },
  { id: '2', name: 'team 2' }
];

describe('should test promises payments statistics selectors', () => {
  const states: Partial<RootState> = {
    promisesPaymentsStatistics: {
      collectors: mockCollectorData,
      teams: mockTeamData
    }
  };

  it('should test selectedCollectors', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectedCollectors
    );

    expect(data).toEqual(mockCollectorData);

    expect(emptyData).toBeUndefined();
  });

  it('should test selectedTeams', () => {
    const { data, emptyData } = selectorWrapper(states)(selector.selectedTeams);

    expect(data).toEqual(mockTeamData);

    expect(emptyData).toBeUndefined();
  });

  it('should test selectedFilters', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectedFilters
    );

    expect(data).toEqual(DEFAULT_FILTER);

    expect(emptyData).toEqual(DEFAULT_FILTER);
  });
});
