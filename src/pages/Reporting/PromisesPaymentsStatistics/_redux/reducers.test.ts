import { DEFAULT_FILTER } from '../constants';
import { PromisesPaymentsStatisticsState } from '../types';
import {
  promisesPaymentsStatisticsActions as actions,
  reducer
} from './reducers';

const initialState: PromisesPaymentsStatisticsState = {
  reports: [],
  filters: DEFAULT_FILTER,
  isCollapsed: false,
  isMounted: false
};

describe('promises payments statistics reducers', () => {
  it('should test setToggleSettingsSection reducer', () => {
    const state = reducer(initialState, actions.setToggleSettingsSection(true));

    expect(state.isCollapsed).toEqual(true);
  });

  it('should test setInitialFilter reducer', () => {
    const state = reducer(
      initialState,
      actions.setInitialFilter({
        ...DEFAULT_FILTER,
        timePeriod: '1'
      })
    );

    expect(state.filters).toEqual({ ...DEFAULT_FILTER, timePeriod: '1' });
  });

  it('should test setDatePickerLabel reducer', () => {
    const state = reducer(initialState, actions.setDatePickerLabel('label'));

    expect(state.datePickerLabel).toEqual('label');
  });

  it('should test updateFilter reducer', () => {
    const state = reducer(initialState, actions.updateFilter({}));

    expect(state.filters).toEqual({});
    expect(state.isMounted).toEqual(true);
  });

  it('should test resetFilter reducer', () => {
    const state = reducer(initialState, actions.resetFilter());

    expect(state.filters).toEqual(DEFAULT_FILTER);
    expect(state.reports).toEqual([]);
  });

  it('should test setReports reducer', () => {
    const state = reducer(initialState, actions.setReports([]));

    expect(state.reports).toEqual([]);
  });

  it('should test setReports reducer', () => {
    const state = reducer(initialState, actions.cleanup());

    expect(state).toEqual(initialState);
  });
});
