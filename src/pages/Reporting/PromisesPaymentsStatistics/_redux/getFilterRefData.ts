import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { promisesPaymentsStatisticsServices } from '../promisesPaymentsStatisticsServices';
import {
  GetFilterDataPayload,
  PromisesPaymentsStatisticsState
} from '../types';

export const getFilterRefData = createAsyncThunk<
  GetFilterDataPayload,
  undefined,
  ThunkAPIConfig
>('promisesPaymentsStatistics/getFilterRefData', async _ => {
  const [
    getTimePeriodsResponse,
    getReportTypesResponse,
    getPromiseTypesResponse,
    getPromiseStatusesResponse
  ] = await Promise.all([
    promisesPaymentsStatisticsServices.getTimePeriods(),
    promisesPaymentsStatisticsServices.getReportTypes(),
    promisesPaymentsStatisticsServices.getPromiseTypes(),
    promisesPaymentsStatisticsServices.getPromiseStatuses()
  ]);

  return {
    timePeriods: getTimePeriodsResponse?.data,
    reportTypes: getReportTypesResponse?.data,
    promiseTypes: getPromiseTypesResponse?.data,
    promiseStatuses: getPromiseStatusesResponse?.data
  };
});

export const getFilterRefDataBuilder = (
  builder: ActionReducerMapBuilder<PromisesPaymentsStatisticsState>
) => {
  builder
    .addCase(getFilterRefData.pending, (draftState, action) => {
      draftState.loading = true;
    })
    .addCase(getFilterRefData.fulfilled, (draftState, action) => {
      draftState.filterRefData = action.payload;
      draftState.loading = false;
    })
    .addCase(getFilterRefData.rejected, (draftState, action) => {
      draftState.loading = false;
    });
};
