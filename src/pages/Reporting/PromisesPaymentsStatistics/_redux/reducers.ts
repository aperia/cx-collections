import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { getReports, getReportsBuilder } from './getReports';
import { getFilterRefData, getFilterRefDataBuilder } from './getFilterRefData';

// constants
import { DEFAULT_FILTER } from '../constants';
import { PromisesPaymentsStatisticsState, Report } from '../types';
import { SettingFilter } from 'pages/Reporting/types';

// Export initial state for unit test
export const initialState: PromisesPaymentsStatisticsState = {
  reports: [],
  filters: DEFAULT_FILTER,
  isCollapsed: false,
  isMounted: false
};

const { actions, reducer } = createSlice({
  name: 'promisesPaymentsStatistics',
  initialState: initialState,
  reducers: {
    updateFilter: (draftState, action: PayloadAction<SettingFilter>) => {
      draftState.filters = action.payload;
      draftState.isMounted = true;
    },
    resetFilter: draftState => {
      draftState.filters = DEFAULT_FILTER;
      draftState.reports = [];
    },
    setReports: (draftState, action: PayloadAction<Report[]>) => {
      draftState.reports = action.payload;
    },
    setToggleSettingsSection: (draftState, action: PayloadAction<boolean>) => {
      draftState.isCollapsed = action.payload;
    },
    setInitialFilter: (draftState, action: PayloadAction<SettingFilter>) => {
      draftState.filters = {
        ...DEFAULT_FILTER,
        ...action.payload
      };
    },
    setDatePickerLabel: (draftState, action: PayloadAction<string>) => {
      draftState.datePickerLabel = action.payload;
    },
    setMountStatus: (draftState, action: PayloadAction<boolean>) => {
      draftState.isMounted = action.payload;
    },
    cleanup: () => initialState
  },
  extraReducers: builder => {
    getReportsBuilder(builder);
    getFilterRefDataBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getReports,
  getFilterRefData
};

export { allActions as promisesPaymentsStatisticsActions, reducer };
