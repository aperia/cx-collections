import { responseDefault } from 'app/test-utils';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { promisesPaymentsStatisticsServices } from '../promisesPaymentsStatisticsServices';
import { FilterItem } from '../types';
import { getFilterRefData } from './getFilterRefData';

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    isAdminRole: true,
    isSupervisor: true
  } as CommonConfig
};

describe('should test get filter ref data', () => {
  let spyGetTimePeriods: jest.SpyInstance;
  let spyGetReportTypes: jest.SpyInstance;
  let spyGetPromiseTypes: jest.SpyInstance;
  let spyGetPromiseStatuses: jest.SpyInstance;

  let store: Store<RootState>;
  let state: RootState;

  beforeEach(() => {
    store = createStore(rootReducer, {
      promisesPaymentsStatistics: {}
    });
    state = store.getState();
  });

  afterEach(() => {
    spyGetTimePeriods?.mockReset();
    spyGetTimePeriods?.mockRestore();

    spyGetReportTypes?.mockReset();
    spyGetReportTypes?.mockRestore();

    spyGetPromiseTypes?.mockReset();
    spyGetPromiseTypes?.mockRestore();

    spyGetPromiseStatuses?.mockReset();
    spyGetPromiseStatuses?.mockRestore();
  });

  it('should fetch data', async () => {
    spyGetTimePeriods = jest
      .spyOn(promisesPaymentsStatisticsServices, 'getTimePeriods')
      .mockResolvedValue({ ...responseDefault });
    spyGetReportTypes = jest
      .spyOn(promisesPaymentsStatisticsServices, 'getReportTypes')
      .mockResolvedValue({ ...responseDefault });
    spyGetPromiseTypes = jest
      .spyOn(promisesPaymentsStatisticsServices, 'getPromiseTypes')
      .mockResolvedValue({ ...responseDefault });
    spyGetPromiseStatuses = jest
      .spyOn(promisesPaymentsStatisticsServices, 'getPromiseStatuses')
      .mockResolvedValue({ ...responseDefault });

    const response = await getFilterRefData()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      timePeriods: null,
      reportTypes: null,
      promiseTypes: null,
      promiseStatuses: null
    });
  });

  it('should response fulfilled', () => {
    const timePeriods: FilterItem[] = [],
      reportTypes: FilterItem[] = [],
      promiseStatuses: FilterItem[] = [],
      promiseTypes: FilterItem[] = [];

    const fulfilled = getFilterRefData.fulfilled(
      { timePeriods, reportTypes, promiseTypes, promiseStatuses },
      getFilterRefData.fulfilled.type,
      undefined
    );

    const actual = rootReducer(state, fulfilled);

    expect(actual.promisesPaymentsStatistics.loading).toEqual(false);
    expect(
      actual.promisesPaymentsStatistics.filterRefData?.timePeriods
    ).toEqual([]);
    expect(
      actual.promisesPaymentsStatistics.filterRefData?.reportTypes
    ).toEqual([]);
    expect(
      actual.promisesPaymentsStatistics.filterRefData?.promiseTypes
    ).toEqual([]);
    expect(
      actual.promisesPaymentsStatistics.filterRefData?.promiseStatuses
    ).toEqual([]);
  });
});
