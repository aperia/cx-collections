import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { FormatTime } from 'app/constants/enums';
import {
  PAYMENT_METHOD,
  PAYMENT_STATUS,
  PROMISE_TYPE
} from 'app/constants/mapping';
import { getStartOfDay, mappingDataFromArray } from 'app/helpers';
import { SettingFilter } from 'pages/Reporting/types';
import { TIME_PERIOD_VALUE } from '../constants';
import { promisesPaymentsStatisticsServices } from '../promisesPaymentsStatisticsServices';
import {
  GetReportsArgs,
  PromisesPaymentsStatisticsState,
  Report
} from '../types';

export interface GetReportsPayload {
  reports?: Report[];
}

export const getReports = createAsyncThunk<
  GetReportsPayload,
  SettingFilter,
  ThunkAPIConfig
>('promisesPaymentsStatistics/getReports', async (args, thunkAPI) => {
  const state = thunkAPI.getState();
  const { collector } = args;

  const requestBody = {
    collectorId: args.collector?.id
  } as GetReportsArgs;

  requestBody.userName = collector?.userId || '';

  requestBody.promiseTypes = [];
  if (args.ppsNonRecurringMultiplePromise) requestBody.promiseTypes.push('PM');
  if (args.ppsPromiseToPay) requestBody.promiseTypes.push('PP');
  if (args.ppsRecurringMultiplePromise) requestBody.promiseTypes.push('PR');
  if (requestBody.promiseTypes.length === 0)
    requestBody.promiseTypes = undefined;

  requestBody.promiseStatuses = [];
  if (args.ppsKeptPromise) requestBody.promiseStatuses.push('KEPT');
  if (args.ppsBrokenPromise) requestBody.promiseStatuses.push('BROKEN');
  if (args.ppsPendingPromise) requestBody.promiseStatuses.push('PENDING');
  if (requestBody.promiseStatuses.length === 0)
    requestBody.promiseStatuses = undefined;

  switch (args.timePeriod) {
    case TIME_PERIOD_VALUE.ENTERED_PROMISE_DATE:
      requestBody.enteredPromiseDate = getStartOfDay(
        args.ppsDatePicker,
        FormatTime.UTCDate
      );
      break;
    case TIME_PERIOD_VALUE.DUE_PROMISE_DATE:
      requestBody.lastPromiseDate = getStartOfDay(
        args.ppsDatePicker,
        FormatTime.UTCDate
      );
      break;
    case TIME_PERIOD_VALUE.MOST_300_RECENT_RECORDS:
      requestBody.last300Records = true;
      break;
  }

  if (args.ppsPromiseAmountFrom)
    requestBody.fromAmount = args.ppsPromiseAmountFrom;
  if (args.ppsPromiseAmountTo) requestBody.toAmount = args.ppsPromiseAmountTo;

  const mappingModel = state.mapping?.data?.promisesPaymentsStatistic;

  const response = await promisesPaymentsStatisticsServices.getReports(
    requestBody
  );

  if (response.data?.status?.toLowerCase() !== 'success') {
    throw new Error('Status is not success');
  }

  const promisesPaymentsStatisticData = mappingDataFromArray<Report>(
    response.data?.promises,
    mappingModel!
  );

  const mappingData = promisesPaymentsStatisticData?.map(d => ({
    ...d,
    status: PAYMENT_STATUS[d.status!] || d.status,
    method: PAYMENT_METHOD[d.method!] || d.method,
    type: PROMISE_TYPE[d.promiseType!] || d.promiseType
  }));

  return {
    reports: mappingData
  };
});

export const getReportsBuilder = (
  builder: ActionReducerMapBuilder<PromisesPaymentsStatisticsState>
) => {
  builder
    .addCase(getReports.pending, (draftState, action) => {
      draftState.loading = true;
    })
    .addCase(getReports.fulfilled, (draftState, action) => {
      draftState.reports = action.payload.reports;
      draftState.loading = false;
    })
    .addCase(getReports.rejected, (draftState, action) => {
      draftState.reports = [];
      draftState.loading = false;
    });
};
