import { FormatTime } from 'app/constants/enums';
import { getStartOfDay } from 'app/helpers';
import { responseDefault } from 'app/test-utils';
import { SettingFilter } from 'pages/Reporting/types';
import { MappingStateData } from 'pages/__commons/MappingProvider/types';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { DEFAULT_FILTER, TIME_PERIOD_VALUE } from '../constants';
import { promisesPaymentsStatisticsServices } from '../promisesPaymentsStatisticsServices';
import { getReports, GetReportsPayload } from './getReports';

const mockArgs: SettingFilter = {
  ...DEFAULT_FILTER,
  ppsRecurringMultiplePromise: true,
  ppsNonRecurringMultiplePromise: true,
  ppsPromiseToPay: true,
  ppsKeptPromise: true,
  ppsBrokenPromise: true,
  ppsPendingPromise: true,
  ppsDatePicker: new Date()
};

const promisesPaymentsStatisticMapping = {
  accountNumber: 'accountId',
  type: 'type',
  method: 'paymentMethod',
  promiseAmount: 'promiseAmount',
  paidAmount: 'paymentAmount',
  status: 'paymentStatus',
  promiseDate: 'paymentDate',
  enteredDate: 'createdDate',
  updatedDate: 'updatedDate'
};

const promisesData = [
  {
    promiseAmount: '100.00',
    paymentAmount: '100.00',
    paymentMethod: 'CHECK',
    paymentStatus: 'KEPT',
    paymentDate: '12-03-2021',
    userName: ''
  },
  {
    promiseAmount: '100.00',
    paymentAmount: '0.00',
    paymentMethod: 'CHECK',
    paymentStatus: 'PENDING',
    paymentDate: '12-03-2021',
    userName: ''
  }
];

describe('should test get reports', () => {
  let spy: jest.SpyInstance;
  let store: Store<RootState>;
  let state: RootState;

  beforeEach(() => {
    store = createStore(rootReducer, {
      promisesPaymentsStatistics: {}
    });
    state = store.getState();
  });

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  const getStateWithMapping = () => {
    return {
      mapping: {
        data: { promisesPaymentsStatistic: promisesPaymentsStatisticMapping }
      } as MappingStateData
    } as RootState;
  };

  it('should return data with most 300 recent records', async () => {
    spy = jest
      .spyOn(promisesPaymentsStatisticsServices, 'getReports')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          status: 'success',
          promises: promisesData
        }
      });

    const reports = await getReports({
      ...mockArgs,
      timePeriod: TIME_PERIOD_VALUE.MOST_300_RECENT_RECORDS
    })(store.dispatch, getStateWithMapping, {});

    const result = reports.payload as Record<string, any>;

    expect(result.reports.length).toEqual(2);
    expect(reports.type).toEqual(
      'promisesPaymentsStatistics/getReports/fulfilled'
    );
  });

  it('should return data with due promise date', async () => {
    spy = jest
      .spyOn(promisesPaymentsStatisticsServices, 'getReports')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          status: 'success',
          promises: promisesData.map(p => ({
            ...p,
            paymentMethod: undefined,
            paymentStatus: undefined
          }))
        }
      });

    const reports = await getReports({
      ...mockArgs,
      timePeriod: TIME_PERIOD_VALUE.DUE_PROMISE_DATE
    })(store.dispatch, getStateWithMapping, {});

    const result = reports.payload as Record<string, any>;

    expect(result.reports.length).toEqual(2);
    expect(reports.type).toEqual(
      'promisesPaymentsStatistics/getReports/fulfilled'
    );
  });

  it('should return data with entered promise date', async () => {
    spy = jest
      .spyOn(promisesPaymentsStatisticsServices, 'getReports')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          status: 'success',
          promises: promisesData
        }
      });

    const mockDate = new Date('2022-01-01');

    const reports = await getReports({
      ...mockArgs,
      ppsRecurringMultiplePromise: false,
      ppsNonRecurringMultiplePromise: false,
      ppsPromiseToPay: false,
      ppsKeptPromise: false,
      ppsBrokenPromise: false,
      ppsPendingPromise: false,
      ppsPromiseAmountFrom: '1.00',
      ppsPromiseAmountTo: '2.99',
      collector: {
        id: '1',
        name: 'collector 1'
      },
      timePeriod: TIME_PERIOD_VALUE.ENTERED_PROMISE_DATE,
      ppsDatePicker: mockDate
    })(store.dispatch, getStateWithMapping, {});

    const result = reports.payload as Record<string, any>;

    expect(result.reports.length).toEqual(2);
    expect(promisesPaymentsStatisticsServices.getReports).toHaveBeenCalledWith({
      userName: '',
      collectorId: '1',
      fromAmount: '1.00',
      toAmount: '2.99',
      promiseTypes: undefined,
      promiseStatuses: undefined,
      enteredPromiseDate: getStartOfDay(mockDate, FormatTime.UTCDate),
      userName: ''
    });
  });

  it('should throw error when status is not "success"', async () => {
    spy = jest
      .spyOn(promisesPaymentsStatisticsServices, 'getReports')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          status: 'failed'
        }
      });

    const reports = (await getReports(mockArgs)(
      store.dispatch,
      getStateWithMapping,
      {}
    )) as Record<string, any>;

    expect(reports.error?.message).toEqual('Status is not success');
  });

  const payload: GetReportsPayload = {
    reports: [
      {
        id: '1',
        accountNumber: 'report 1'
      }
    ]
  };

  it('should be pending', () => {
    const pending = getReports.pending(getReports.pending.type, mockArgs);
    const actual = rootReducer(state, pending);

    expect(actual.promisesPaymentsStatistics.loading).toEqual(true);
  });

  it('should be fulfilled', () => {
    const fulfilled = getReports.fulfilled(
      payload,
      getReports.fulfilled.type,
      mockArgs
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual.promisesPaymentsStatistics.loading).toEqual(false);
    expect(actual.promisesPaymentsStatistics.reports).toEqual([
      {
        id: '1',
        accountNumber: 'report 1'
      }
    ]);
  });

  it('should be rejected', () => {
    const rejected = getReports.rejected(
      { name: 'Error', message: 'Error' },
      getReports.rejected.type,
      mockArgs
    );
    const actual = rootReducer(state, rejected);

    expect(actual.promisesPaymentsStatistics.loading).toEqual(false);
    expect(actual.promisesPaymentsStatistics.reports).toEqual([]);
  });
});
