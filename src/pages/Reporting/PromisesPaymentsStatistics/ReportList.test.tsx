import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import ReportList from './ReportList';
import { promisesPaymentsStatisticsActions } from './_redux/reducers';
import { DEFAULT_FILTER, REPORT_STATUS } from './constants';
import { Report } from './types';

const mockPromisesPaymentsStatisticsAction = mockActionCreator(
  promisesPaymentsStatisticsActions
);

const mockReportData: Report[] = [];

describe('should test report list', () => {
  for (let i = 0; i < 11; i++) {
    mockReportData.push({
      id: '1',
      accountNumber: '1',
      type: '1',
      method: '1',
      promiseAmount: 500,
      paidAmount: 100,
      promiseDate: '1',
      enteredDate: '1',
      updatedDate: '1'
    });
  }

  const withReportListState: Partial<RootState> = {
    promisesPaymentsStatistics: {
      filters: {
        ...DEFAULT_FILTER,
        collector: { id: '1', name: 'collector 1' }
      },
      collectors: [
        { id: '1', name: 'collector 1' },
        { id: '2', name: 'collector 2' }
      ],
      teams: [
        { id: '1', name: 'team 1' },
        { id: '2', name: 'team 2' }
      ],
      reports: mockReportData
    }
  };

  const withReportListEmptyFiltersState: Partial<RootState> = {
    promisesPaymentsStatistics: {
      filters: DEFAULT_FILTER
    }
  };

  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<ReportList />, { initialState });
  };

  describe('should test get report data', () => {
    it('should test with filter data', () => {
      jest.useFakeTimers();
      const mockGetReports = mockPromisesPaymentsStatisticsAction('getReports');
      renderWrapper(withReportListState);
      jest.runAllTimers();

      expect(mockGetReports).toBeCalledWith(
        withReportListState.promisesPaymentsStatistics?.filters || {}
      );
    });

    it('should test with empty filter data', () => {
      jest.useFakeTimers();
      const mockSetReports = mockPromisesPaymentsStatisticsAction('setReports');
      renderWrapper(withReportListEmptyFiltersState);
      jest.runAllTimers();

      expect(mockSetReports).toBeCalledWith([]);
    });
  });

  describe('should test show badge', () => {
    it('should show broken status', () => {
      jest.useFakeTimers();
      const { wrapper } = renderWrapper({
        promisesPaymentsStatistics: {
          ...withReportListState.promisesPaymentsStatistics,
          reports: [
            {
              ...mockReportData,
              status: REPORT_STATUS.KEPT
            }
          ]
        }
      });
      jest.runAllTimers();
      const badgeElement = wrapper.container.querySelector(
        'span[class="badge badge-green"]'
      );
      expect(badgeElement).toBeInTheDocument();
    });
    it('should show broken status', () => {
      jest.useFakeTimers();
      const { wrapper } = renderWrapper({
        promisesPaymentsStatistics: {
          ...withReportListState.promisesPaymentsStatistics,
          reports: [
            {
              ...mockReportData,
              status: REPORT_STATUS.BROKEN
            }
          ]
        }
      });
      jest.runAllTimers();
      const badgeElement = wrapper.container.querySelector(
        'span[class="badge badge-red"]'
      );
      expect(badgeElement).toBeInTheDocument();
    });
    it('should show pending status', () => {
      jest.useFakeTimers();
      const { wrapper } = renderWrapper({
        promisesPaymentsStatistics: {
          ...withReportListState.promisesPaymentsStatistics,
          reports: [
            {
              ...mockReportData,
              status: REPORT_STATUS.PENDING
            }
          ]
        }
      });
      jest.runAllTimers();
      const badgeElement = wrapper.container.querySelector(
        'span[class="badge badge-orange"]'
      );
      expect(badgeElement).toBeInTheDocument();
    });
    it('should show other status', () => {
      jest.useFakeTimers();
      const { wrapper } = renderWrapper({
        promisesPaymentsStatistics: {
          ...withReportListState.promisesPaymentsStatistics,
          reports: [
            {
              ...mockReportData,
              status: 'other'
            }
          ]
        }
      });
      jest.runAllTimers();
      const badgeElement = wrapper.container.querySelector(
        'span[class="badge badge-grey"]'
      );
      expect(badgeElement).toBeInTheDocument();
    });
  });
});
