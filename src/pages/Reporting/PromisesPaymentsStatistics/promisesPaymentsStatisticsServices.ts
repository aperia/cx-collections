import { apiService } from 'app/utils/api.service';

import { GetReportsArgs } from './types';

export const promisesPaymentsStatisticsServices = {
  getReports(args: GetReportsArgs) {
    const url =
      window.appConfig?.api?.promisesPaymentsStatistics.getReports || '';

    return apiService.post(url, args);
  },

  getTimePeriods() {
    const url =
      window.appConfig?.api?.promisesPaymentsStatistics.getTimePeriods || '';
    return apiService.get(url);
  },

  getReportTypes() {
    const { isAdminRole, isSupervisor } = window.appConfig?.commonConfig || {};
    if (!isAdminRole && !isSupervisor) return;
    const url =
      window.appConfig?.api?.promisesPaymentsStatistics.getReportTypes || '';
    return apiService.get(url);
  },

  getPromiseTypes() {
    const { isAdminRole, isSupervisor } = window.appConfig?.commonConfig || {};
    if (!isAdminRole && !isSupervisor) return;
    const url =
      window.appConfig?.api?.promisesPaymentsStatistics.getPromiseTypes || '';
    return apiService.get(url);
  },

  getPromiseStatuses() {
    const url =
      window.appConfig?.api?.promisesPaymentsStatistics.getPromiseStatuses ||
      '';
    return apiService.get(url);
  }
};
