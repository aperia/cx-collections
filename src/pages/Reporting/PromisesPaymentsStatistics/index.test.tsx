import React from 'react';
import {
  mockActionCreator,
  queryByClass,
  renderMockStore
} from 'app/test-utils';
import PromisesPaymentsStatistics from '.';
import { promisesPaymentsStatisticsActions } from './_redux/reducers';
import { ToggleButtonProps } from 'app/components/ToggleButton';
import { fireEvent, queryByAttribute, screen } from '@testing-library/dom';
import { I18N_PROMISES_PAYMENTS_STATISTICS } from 'app/constants/i18n';
import { DEFAULT_FILTER } from './constants';
import { SettingFilter } from '../types';
import { collectorsTeamsComboBoxActions } from '../CollectorsTeamsComboBox/_redux/reducers';
import * as FilterForm from './FilterForm';

jest.mock(
  'app/components/ToggleButton',
  () =>
    ({ onToggleCollapsed, collapsed }: ToggleButtonProps) =>
      (
        <button
          onClick={(e: any) => {
            if (onToggleCollapsed) {
              onToggleCollapsed(e);
            }
          }}
        >
          Collapse Button
        </button>
      )
);

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockFilterData: SettingFilter = {
  ...DEFAULT_FILTER,
  ppsDatePicker: new Date(),
  timePeriod: 'Due promise date'
};

const mockPromisesPaymentsStatisticsActions = mockActionCreator(
  promisesPaymentsStatisticsActions
);

const mockCollectorsTeamsComboBoxActions = mockActionCreator(
  collectorsTeamsComboBoxActions
);

const mockTimePeriodData = [
  {
    id: 'most300RecentRecords',
    value: 'Most 300 recent records',
    label: 'txt_most_300_recent_records'
  },
  {
    id: 'enteredPromiseDate',
    value: 'Entered promise date',
    label: 'txt_entered_promise_date'
  },
  {
    id: 'duePromiseDate',
    value: 'Due promise date',
    label: 'txt_due_promise_date'
  }
];

const mockTimePeriodDataWithoutLabel = [
  {
    id: 'most300RecentRecords',
    value: 'Most 300 recent records'
  },
  {
    id: 'enteredPromiseDate',
    value: 'Entered promise date'
  },
  {
    id: 'duePromiseDate',
    value: 'Due promise date'
  }
];

const supervisorConfig = () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      isSupervisor: true
    } as CommonConfig
  };
};

const collectorConfig = () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      isCollector: true
    } as CommonConfig
  };
};

describe('should test payments promises statistics', () => {
  beforeEach(() => {
    supervisorConfig();
  });

  const withStatisticsCollectorState: Partial<RootState> = {
    promisesPaymentsStatistics: {
      filters: mockFilterData,
      isCollapsed: false,
      isMounted: false,
      filterRefData: {
        timePeriods: mockTimePeriodData,
        reportTypes: [
          {
            id: 'individual',
            value: 'Individual',
            label: 'txt_individual'
          },
          {
            id: 'roll_up',
            value: 'Roll Up',
            label: 'txt_roll_up'
          }
        ],
        promiseTypes: [
          {
            id: 'ppsNonRecurringMultiplePromise',
            name: 'ppsNonRecurringMultiplePromise',
            label: 'txt_pm_non_recurring_multiple_promise'
          },
          {
            id: 'ppsPromiseToPay',
            name: 'ppsPromiseToPay',
            label: 'txt_pp_promise_to_pay'
          },
          {
            id: 'ppsRecurringMultiplePromise',
            name: 'ppsRecurringMultiplePromise',
            label: 'txt_pr_recurring_multiple_promise'
          }
        ],
        promiseStatuses: [
          {
            id: 'ppsKeptPromise',
            name: 'ppsKeptPromise',
            label: 'txt_kept_promise'
          },
          {
            id: 'ppsBrokenPromise',
            name: 'ppsBrokenPromise',
            label: 'txt_broken_promise'
          },
          {
            id: 'ppsPendingPromise',
            name: 'ppsPendingPromise',
            label: 'txt_pending_promise'
          }
        ]
      },
      datePickerLabel: 'Date Picker Label'
    },
    collectorsTeamsComboBox: {
      lazyLoadComboBox: {
        filter: '',
        itemsLazyLoad: [],
        opened: false
      }
    }
  };

  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<PromisesPaymentsStatistics />, {
      initialState
    });
  };

  describe('should render UI', () => {
    it('should get filter data', () => {
      const mockGetFilterData =
        mockPromisesPaymentsStatisticsActions('getFilterRefData');

      renderWrapper(withStatisticsCollectorState);

      expect(mockGetFilterData).toBeCalled();
    });

    it('should render without filter ref data', () => {
      const { wrapper } = renderWrapper({
        ...withStatisticsCollectorState,
        promisesPaymentsStatistics: {
          ...withStatisticsCollectorState.promisesPaymentsStatistics,
          filterRefData: undefined
        }
      });

      expect(
        wrapper.queryByText(
          I18N_PROMISES_PAYMENTS_STATISTICS.MOST_300_RECENT_RECORDS
        )
      ).not.toBeInTheDocument();
    });

    it('should render time period without label', () => {
      const mockSetDatePickerLabel =
        mockPromisesPaymentsStatisticsActions('setDatePickerLabel');

      const { wrapper } = renderWrapper({
        ...withStatisticsCollectorState,
        promisesPaymentsStatistics: {
          ...withStatisticsCollectorState.promisesPaymentsStatistics,
          filterRefData: {
            timePeriods: mockTimePeriodDataWithoutLabel
          }
        }
      });

      const getById = queryByAttribute.bind(null, 'id');

      const radio = getById(wrapper.container, 'enteredPromiseDate')!;

      fireEvent.click(radio);

      expect(mockSetDatePickerLabel).toBeCalledWith('');
    });

    it('should hide promise type with collector role', () => {
      collectorConfig();

      renderWrapper(withStatisticsCollectorState);

      expect(
        screen.queryByText(I18N_PROMISES_PAYMENTS_STATISTICS.PROMISE_TYPE)
      ).not.toBeInTheDocument();
    });

    describe('should test handling scroll event', () => {
      it('scroll top 0', () => {
        const { baseElement } = renderWrapper(withStatisticsCollectorState);

        fireEvent.scroll(window, {
          target: {
            scrollTop: 0
          }
        });

        expect(queryByClass(baseElement, /br-white/)).toBeInTheDocument();
      });
      it('scroll more than 0', () => {
        const { baseElement } = renderWrapper(withStatisticsCollectorState);

        fireEvent.scroll(window, {
          target: {
            scrollTop: 10
          }
        });

        expect(queryByClass(baseElement, /br-white/)).not.toBeInTheDocument();
      });
    });
  });

  describe('should test toggle button', () => {
    it('should handle toggle button event', () => {
      const mockToggleCollapse = mockPromisesPaymentsStatisticsActions(
        'setToggleSettingsSection'
      );

      renderWrapper(withStatisticsCollectorState);

      const toggleButton = screen.getByRole('button', {
        name: 'Collapse Button'
      });

      toggleButton.click();

      expect(mockToggleCollapse).toBeCalledWith(
        !withStatisticsCollectorState.promisesPaymentsStatistics?.isCollapsed
      );
    });
  });

  describe('should test form', () => {
    it('should test handling time period', () => {
      jest.useFakeTimers();

      const mockSetDatePickerLabel =
        mockPromisesPaymentsStatisticsActions('setDatePickerLabel');

      renderWrapper(withStatisticsCollectorState);

      const enteredPromiseDate = screen.getByRole('radio', {
        name: I18N_PROMISES_PAYMENTS_STATISTICS.ENTERED_PROMISE_DATE
      });

      fireEvent.click(enteredPromiseDate);

      jest.runAllTimers();

      expect(mockSetDatePickerLabel).toBeCalled();
    });

    it('should test handling report type', () => {
      const mockSetReportTypeComboBox = mockCollectorsTeamsComboBoxActions(
        'setReportTypeComboBox'
      );

      renderWrapper(withStatisticsCollectorState);

      const rollUp = screen.getByRole('radio', {
        name: I18N_PROMISES_PAYMENTS_STATISTICS.ROLL_UP
      });

      fireEvent.click(rollUp);

      expect(mockSetReportTypeComboBox).not.toBeCalled();
    });

    it('should test blur event date picker', () => {
      const { wrapper } = renderWrapper(withStatisticsCollectorState);

      const datePicker = wrapper.getByPlaceholderText('mm/dd/yyyy');

      fireEvent.blur(datePicker);

      expect(datePicker).toBeInTheDocument();
    });

    it('should test submit form', () => {
      const spy = jest
        .spyOn(FilterForm, 'FilterForm')
        .mockImplementation(({ onSubmit }) => (
          <div>
            <button data-testid="submitBtn" onClick={() => onSubmit({} as any)}>
              run
            </button>
          </div>
        ));

      const mockUpdateFilter =
        mockPromisesPaymentsStatisticsActions('updateFilter');

      jest.useFakeTimers();

      renderWrapper(withStatisticsCollectorState);

      jest.runAllTimers();

      const submitBtn = screen.getByTestId('submitBtn');
      submitBtn.click();
      expect(mockUpdateFilter).toBeCalled();
      spy.mockReset();
      spy.mockRestore();
    });

    it('should test reset previous button event', () => {
      const mockUpdateFilter =
        mockPromisesPaymentsStatisticsActions('resetFilter');

      renderWrapper(withStatisticsCollectorState);

      const resetButton = screen.getByRole('button', {
        name: I18N_PROMISES_PAYMENTS_STATISTICS.RESET_TO_DEFAULT
      });

      resetButton.click();

      expect(mockUpdateFilter).toBeCalled();
    });
  });
});
