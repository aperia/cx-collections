import React, { useLayoutEffect, useRef } from 'react';

// components
import { ReportingField } from '../ReportingField';
import {
  Button,
  CheckBox,
  DatePicker,
  NumericTextBox,
  Radio,
  SimpleBar
} from 'app/_libraries/_dls/components';
import { Form, Formik, FormikProvider } from 'formik';
import { CollectorsTeamsComboBox } from '../CollectorsTeamsComboBox';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { promisesPaymentsStatisticsActions } from './_redux/reducers';
import {
  selectedDatePickerLabel,
  selectedFilterRefData,
  selectedFilters,
  selectedMountedStatus
} from './_redux/selectors';
import { collectorsTeamsComboBoxActions } from '../CollectorsTeamsComboBox/_redux/reducers';

// helpers
import { promisesPaymentsStatisticsFormValidationSchema } from '../helpers';
import isString from 'lodash.isstring';

// constants
import { SettingFilter } from '../types';
import { I18N_PROMISES_PAYMENTS_STATISTICS } from 'app/constants/i18n';
import { DEFAULT_FILTER, TIME_PERIOD_VALUE } from './constants';
import { FORM_NAMES } from '../constants';
import { FilterFormProps, FilterRefData } from './types';

import { genAmtId } from 'app/_libraries/_dls/utils';

export const FilterForm: React.FC<FilterFormProps> = ({ onSubmit }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const leftRef = useRef<HTMLDivElement | null>(null);

  const filterValue: SettingFilter = useSelector(selectedFilters);
  const isMounted = useSelector(selectedMountedStatus);
  const {
    timePeriods,
    reportTypes,
    promiseTypes,
    promiseStatuses
  }: FilterRefData = useSelector(selectedFilterRefData) ?? {};
  const datePickerLabel = useSelector(selectedDatePickerLabel);
  const { isAdminRole: isAdmin, isSupervisor } =
    window.appConfig?.commonConfig || {};
  const testId = 'promisesPaymentsStatistics-FilterForm';

  useLayoutEffect(() => {
    if (!leftRef.current) return;
    leftRef.current.style.height = `calc(100vh - 382px)`;
  }, []);

  const handleSubmit = (updatedValues: SettingFilter) => {
    return onSubmit(updatedValues);
  };

  return (
    <Formik
      initialValues={filterValue}
      validationSchema={promisesPaymentsStatisticsFormValidationSchema}
      validateOnChange={true}
      validateOnBlur={true}
      validateOnMount={!isMounted}
      onSubmit={handleSubmit}
    >
      {formikProps => {
        const {
          values,
          touched,
          errors,
          isValid,
          setValues,
          setTouched,
          handleChange,
          handleBlur,
          resetForm,
          validateField,
          validateForm
        } = formikProps;

        const handleTimePeriodChange = (
          e: React.ChangeEvent<HTMLInputElement>
        ) => {
          setValues({
            ...values,
            ppsDatePicker: undefined
          });

          setTouched({
            ...touched,
            [FORM_NAMES.DATE_PICKER]: false
          });
          handleChange(e);
          dispatch(
            promisesPaymentsStatisticsActions.setDatePickerLabel(
              e.target.dataset.label || ''
            )
          );
          process.nextTick(() => {
            validateField(FORM_NAMES.DATE_PICKER);
          });
        };

        const handleChangeReportType = (
          e: React.ChangeEvent<HTMLInputElement>
        ) => {
          setValues({
            ...values,
            [FORM_NAMES.COLLECTOR_COMBO_BOX]: undefined,
            [FORM_NAMES.TEAM_COMBO_BOX]: undefined
          });
          setTouched({
            ...touched,
            [FORM_NAMES.COLLECTOR_COMBO_BOX]: false,
            [FORM_NAMES.TEAM_COMBO_BOX]: false
          });
          handleChange(e);
          dispatch(collectorsTeamsComboBoxActions.resetComboBoxData());
        };

        const handleResetForm = () => {
          resetForm({
            values: DEFAULT_FILTER
          });
          validateForm(DEFAULT_FILTER);
          dispatch(promisesPaymentsStatisticsActions.resetFilter());
          dispatch(collectorsTeamsComboBoxActions.reset());
        };

        return (
          <Form>
            <div ref={leftRef}>
              <SimpleBar>
                <div className="px-24 pb-24 pt-16">
                  <h5
                    className="mb-n8"
                    data-testid={genAmtId(testId, 'setting', '')}
                  >
                    {t(I18N_PROMISES_PAYMENTS_STATISTICS.SETTINGS)}
                  </h5>
                  <ReportingField
                    title={t(I18N_PROMISES_PAYMENTS_STATISTICS.TIME_PERIOD)}
                    dataTestId={genAmtId(testId, 'time-period', '')}
                  >
                    <div className="custom-group-radio col-12">
                      {timePeriods?.map((element, index) => (
                        <Radio
                          key={`${index}-${element.id}`}
                          className="mt-16 mr-24"
                          dataTestId={genAmtId(
                            testId,
                            `timePeriod-radio-${element.label}`,
                            ''
                          )}
                        >
                          <Radio.Input
                            id={element.id}
                            name={FORM_NAMES.TIME_PERIOD}
                            checked={values?.timePeriod === element.value}
                            value={element.value}
                            data-label={element.label}
                            onChange={handleTimePeriodChange}
                          ></Radio.Input>
                          <Radio.Label>{t(element.label)}</Radio.Label>
                        </Radio>
                      ))}
                    </div>
                    {values?.timePeriod !==
                      TIME_PERIOD_VALUE.MOST_300_RECENT_RECORDS && (
                      <div className="col-12 mt-16">
                        <DatePicker
                          required
                          id={FORM_NAMES.PROMISE_DATE_PICKER}
                          name={FORM_NAMES.PROMISE_DATE_PICKER}
                          label={t(datePickerLabel)}
                          onChange={handleChange}
                          onBlur={handleBlur}
                          value={
                            values?.ppsDatePicker
                              ? new Date(values?.ppsDatePicker)
                              : undefined
                          }
                          error={
                            touched?.ppsDatePicker
                              ? {
                                  status: isString(errors?.ppsDatePicker),
                                  message: t(errors?.ppsDatePicker)
                                }
                              : undefined
                          }
                          dataTestId={genAmtId(testId, 'promise-date', '')}
                        />
                      </div>
                    )}
                  </ReportingField>
                  <ReportingField
                    title={t(I18N_PROMISES_PAYMENTS_STATISTICS.REPORT_TYPE)}
                    dataTestId={genAmtId(testId, 'report-type', '')}
                  >
                    {(isAdmin || isSupervisor) && (
                      <div className="custom-group-radio col-12">
                        {reportTypes?.map((element, index) => (
                          <Radio
                            key={`${index}-${element.id}`}
                            className="mt-16 mr-24"
                            dataTestId={genAmtId(
                              testId,
                              `report-type-${element.value}`,
                              ''
                            )}
                          >
                            <Radio.Input
                              id={element.id}
                              name={FORM_NAMES.REPORT_TYPE}
                              checked={values?.reportType === element.value}
                              value={element.value}
                              onChange={handleChangeReportType}
                            />
                            <Radio.Label>{t(element.label)}</Radio.Label>
                          </Radio>
                        ))}
                      </div>
                    )}
                  </ReportingField>
                  <FormikProvider value={formikProps}>
                    <CollectorsTeamsComboBox />
                  </FormikProvider>
                  <ReportingField
                    title={t(I18N_PROMISES_PAYMENTS_STATISTICS.PROMISE_TYPE)}
                    disabled={!isSupervisor && !isAdmin}
                    dataTestId={genAmtId(testId, 'promise-type', '')}
                  >
                    {promiseTypes?.map((element, index) => (
                      <div key={`${index}-${element.id}`} className="col-12">
                        <CheckBox
                          className="mt-16"
                          dataTestId={genAmtId(
                            testId,
                            `promise-type-${element.label}`,
                            ''
                          )}
                        >
                          <CheckBox.Input
                            id={element.id}
                            name={element.name}
                            checked={values && (values as any)[element.name!]}
                            onChange={handleChange}
                          />
                          <CheckBox.Label htmlFor={element.label}>
                            {t(element.label)}
                          </CheckBox.Label>
                        </CheckBox>
                      </div>
                    ))}
                  </ReportingField>
                  <ReportingField
                    title={t(I18N_PROMISES_PAYMENTS_STATISTICS.PROMISE_STATUS)}
                    dataTestId={genAmtId(testId, 'promise-status', '')}
                  >
                    {promiseStatuses?.map((element, index) => (
                      <div key={`${index}-${element.id}`} className="col-md-6">
                        <CheckBox
                          className="mt-16"
                          dataTestId={genAmtId(
                            testId,
                            `promise-status-${element.label}`,
                            ''
                          )}
                        >
                          <CheckBox.Input
                            id={element.id}
                            name={element.name}
                            checked={values && (values as any)[element.name!]}
                            onChange={handleChange}
                          />
                          <CheckBox.Label htmlFor={element.label}>
                            {t(element.label)}
                          </CheckBox.Label>
                        </CheckBox>
                      </div>
                    ))}
                  </ReportingField>
                  <ReportingField
                    title={t(I18N_PROMISES_PAYMENTS_STATISTICS.PROMISE_AMOUNT)}
                    dataTestId={genAmtId(testId, 'promise-amount', '')}
                  >
                    <div className="col-6 mt-16">
                      <NumericTextBox
                        id={FORM_NAMES.PROMISE_AMOUNT_FROM}
                        name={FORM_NAMES.PROMISE_AMOUNT_FROM}
                        label={t(
                          I18N_PROMISES_PAYMENTS_STATISTICS.PROMISE_AMOUNT_FROM
                        )}
                        value={values?.ppsPromiseAmountFrom}
                        format="c2"
                        onChange={handleChange}
                        dataTestId={genAmtId(testId, 'promise-amount-from', '')}
                      />
                    </div>
                    <div className="col-6 mt-16">
                      <NumericTextBox
                        id={FORM_NAMES.PROMISE_AMOUNT_TO}
                        name={FORM_NAMES.PROMISE_AMOUNT_TO}
                        label={t(
                          I18N_PROMISES_PAYMENTS_STATISTICS.PROMISE_AMOUNT_TO
                        )}
                        value={values?.ppsPromiseAmountTo}
                        format="c2"
                        onChange={handleChange}
                        dataTestId={genAmtId(testId, 'promise-amount-to', '')}
                      />
                    </div>
                  </ReportingField>
                </div>
              </SimpleBar>
            </div>
            <div className="d-flex justify-content-end py-16 px-24 border-top">
              <Button
                variant="secondary"
                onClick={handleResetForm}
                dataTestId={genAmtId(testId, 'reset-btn', '')}
              >
                {t(I18N_PROMISES_PAYMENTS_STATISTICS.RESET_TO_DEFAULT)}
              </Button>
              <Button
                type="submit"
                variant="primary"
                disabled={!isValid}
                dataTestId={genAmtId(testId, 'run-btn', '')}
              >
                {t(I18N_PROMISES_PAYMENTS_STATISTICS.RUN)}
              </Button>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};
