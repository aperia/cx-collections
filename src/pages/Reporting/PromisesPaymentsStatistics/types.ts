import { Collector, Team } from '../CollectorsTeamsComboBox/types';
import { SettingFilter } from '../types';

export interface PromisesPaymentsStatisticsState {
  loading?: boolean;
  collectors?: Collector[];
  reports?: Report[];
  filters?: SettingFilter;
  teams?: Team[];
  filterRefData?: FilterRefData;
  isCollapsed?: boolean;
  isMounted?: boolean;
  datePickerLabel?: string;
}

export interface Report {
  id?: string;
  accountNumber?: string;
  type?: string;
  promiseType?: string;
  method?: string;
  promiseAmount?: number;
  paidAmount?: number;
  status?: string;
  promiseDate?: string;
  enteredDate?: string;
  updatedDate?: string;
  team?: string;
}

export interface GetReportsArgs {
  userName: string;
  enteredPromiseDate?: string;
  lastPromiseDate?: string;
  last300Records?: boolean;
  promiseType?: string;
  promiseTypes?: string[];
  promiseStatus?: string;
  promiseStatuses?: string[];
  fromAmount?: string;
  toAmount?: string;
  collectorId?: string;
}

export interface FilterItem {
  id: string;
  label?: string;
  value?: string;
  name?: string;
}

export interface GetFilterDataPayload {
  timePeriods?: FilterItem[];
  reportTypes?: FilterItem[];
  promiseTypes?: FilterItem[];
  promiseStatuses?: FilterItem[];
}

export interface GetFilterDataArgs {
  userRole?: string;
}

export interface FilterRefData extends GetFilterDataPayload {}

export interface FilterFormProps {
  onSubmit: (values: SettingFilter) => void;
}
