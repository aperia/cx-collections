import { FORM_NAMES, REPORT_TYPE_VALUE } from './constants';
import * as Yup from 'yup';
import {
  I18N_COLLECTOR_PRODUCTIVITY,
  I18N_PROMISES_PAYMENTS_STATISTICS
} from 'app/constants/i18n';
import { TIME_PERIOD_VALUE as STATISTICS_TIME_PERIOD_VALUE } from './PromisesPaymentsStatistics/constants';
import { TIME_PERIOD_VALUE as PRODUCTIVITY_TIME_PERIOD_VALUE } from './CollectorProductivity/constants';

export const collectorProductivityFormValidationSchema = () => {
  return Yup.object().shape({
    [FORM_NAMES.COLLECTOR_COMBO_BOX]: Yup.object().when(
      FORM_NAMES.REPORT_TYPE,
      {
        is: REPORT_TYPE_VALUE.INDIVIDUAL,
        then: Yup.object().required(
          I18N_PROMISES_PAYMENTS_STATISTICS.COLLECTOR_IS_REQUIRED
        ),
        otherwise: Yup.object().optional()
      }
    ),
    [FORM_NAMES.TEAM_COMBO_BOX]: Yup.object().when(FORM_NAMES.REPORT_TYPE, {
      is: REPORT_TYPE_VALUE.ROLL_UP,
      then: Yup.object().required(
        I18N_PROMISES_PAYMENTS_STATISTICS.TEAM_IS_REQUIRED
      ),
      otherwise: Yup.object().optional()
    }),
    [FORM_NAMES.RANGE_DATE_PICKER]: Yup.object().when(FORM_NAMES.TIME_PERIOD, {
      is: PRODUCTIVITY_TIME_PERIOD_VALUE.DATE_RANGE,
      then: Yup.object({
        start: Yup.date().required(
          I18N_COLLECTOR_PRODUCTIVITY.DATE_RANGE_IS_REQUIRED
        ),
        end: Yup.date().required(
          I18N_COLLECTOR_PRODUCTIVITY.DATE_RANGE_IS_REQUIRED
        )
      }).required(I18N_COLLECTOR_PRODUCTIVITY.DATE_RANGE_IS_REQUIRED),
      otherwise: Yup.object().optional()
    })
  });
};

export const promisesPaymentsStatisticsFormValidationSchema = () => {
  return Yup.object().shape({
    [FORM_NAMES.DATE_PICKER]: Yup.date()
      .when(FORM_NAMES.TIME_PERIOD, {
        is: STATISTICS_TIME_PERIOD_VALUE.ENTERED_PROMISE_DATE,
        then: Yup.date().required(
          I18N_PROMISES_PAYMENTS_STATISTICS.ENTERED_PROMISE_DATE_IS_REQUIRED
        ),
        otherwise: Yup.date().optional()
      })
      .when(FORM_NAMES.TIME_PERIOD, {
        is: STATISTICS_TIME_PERIOD_VALUE.DUE_PROMISE_DATE,
        then: Yup.date().required(
          I18N_PROMISES_PAYMENTS_STATISTICS.DUE_PROMISE_DATE_IS_REQUIRED
        ),
        otherwise: Yup.date().optional()
      }),
    [FORM_NAMES.COLLECTOR_COMBO_BOX]: Yup.object().when(
      FORM_NAMES.REPORT_TYPE,
      {
        is: REPORT_TYPE_VALUE.INDIVIDUAL,
        then: Yup.object().required(
          I18N_PROMISES_PAYMENTS_STATISTICS.COLLECTOR_IS_REQUIRED
        ),
        otherwise: Yup.object().optional()
      }
    ),
    [FORM_NAMES.TEAM_COMBO_BOX]: Yup.object().when(FORM_NAMES.REPORT_TYPE, {
      is: REPORT_TYPE_VALUE.ROLL_UP,
      then: Yup.object().required(
        I18N_PROMISES_PAYMENTS_STATISTICS.TEAM_IS_REQUIRED
      ),
      otherwise: Yup.object().optional()
    })
  });
};
