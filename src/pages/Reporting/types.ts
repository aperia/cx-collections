import { DateRangePickerValue } from 'app/_libraries/_dls/components';
import { Collector, Team } from './CollectorsTeamsComboBox/types';
import { REPORT_TYPE_VALUE } from './constants';

export interface FormFieldProps {
  title?: string;
  children?: any;
  disabled?: boolean;
  dataTestId?: string;
}

export interface SettingFilter {
  timePeriod?: string;
  ppsDatePicker?: Date;
  rangeDatePicker?: DateRangePickerValue;
  reportType: REPORT_TYPE_VALUE;
  collector?: Collector;
  team?: Team;
  ppsNonRecurringMultiplePromise?: boolean;
  ppsPromiseToPay?: boolean;
  ppsRecurringMultiplePromise?: boolean;
  ppsKeptPromise?: boolean;
  ppsBrokenPromise?: boolean;
  ppsPendingPromise?: boolean;
  ppsPromiseAmountFrom?: string;
  ppsPromiseAmountTo?: string;
}

export interface ComboBoxData {
  label?: string;
  placeholder?: string;
  name?: string;
}
