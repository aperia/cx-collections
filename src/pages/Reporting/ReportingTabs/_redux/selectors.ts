import { createSelector } from 'reselect';
import { ReportingTabsStates } from '../types';

const getReportingTabs = (states: RootState) => states.reportingTabs;

export const selectedActiveTabId = createSelector(
  getReportingTabs,
  (data: ReportingTabsStates) => data.activeTab
);
