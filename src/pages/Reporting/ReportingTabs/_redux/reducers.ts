import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { TAB_IDS, ReportingTabsStates } from '../types';

// For unit test
export const initialState: ReportingTabsStates = {
  activeTab: TAB_IDS.COLLECTOR_PRODUCTIVITY
};

const { actions, reducer } = createSlice({
  name: 'reportingTabs',
  initialState: initialState,
  reducers: {
    changeTab: (draftState, action: PayloadAction<TAB_IDS>) => {
      draftState.activeTab = action.payload;
    },
    reset: () => initialState
  }
});

export { actions as reportingTabsActions, reducer };
