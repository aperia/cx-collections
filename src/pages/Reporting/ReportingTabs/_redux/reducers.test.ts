import { ReportingTabsStates, TAB_IDS } from '../types';
import { reportingTabsActions as actions, reducer } from './reducers';

const initialState: ReportingTabsStates = {
  activeTab: TAB_IDS.COLLECTOR_PRODUCTIVITY
};

describe('should test reporting tab reducers', () => {
  it('should test change tab reducer', () => {
    const state = reducer(
      initialState,
      actions.changeTab(TAB_IDS.PROMISES_PAYMENTS_STATISTICS)
    );

    expect(state.activeTab).toEqual(TAB_IDS.PROMISES_PAYMENTS_STATISTICS);
  });

  it('should test reset reducer', () => {
    const state = reducer(initialState, actions.reset());

    expect(state).toEqual(initialState);
  });
});
