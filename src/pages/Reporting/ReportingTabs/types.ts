export interface ReportingTabsStates {
  activeTab: TAB_IDS;
}

export enum TAB_IDS {
  COLLECTOR_PRODUCTIVITY = 'collectorProductivity',
  PROMISES_PAYMENTS_STATISTICS = 'promisesPaymentsStatistics'
}
