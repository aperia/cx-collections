import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { TAB_IDS } from './types';
import ReportingTabs from '.';
import { screen } from '@testing-library/dom';
import { I18N_REPORTING_TABS } from './constants';
import { reportingTabsActions } from './_redux/reducers';

const mockReportingTabsActions = mockActionCreator(reportingTabsActions);

const { getComputedStyle } = global.window;
window.getComputedStyle = (elm, select) => getComputedStyle(elm, select);

describe('should test reporting tabs', () => {
  const withReportingTabsState: Partial<RootState> = {
    reportingTabs: {
      activeTab: TAB_IDS.COLLECTOR_PRODUCTIVITY
    }
  };

  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<ReportingTabs />, { initialState });
  };

  it('should render UI', () => {
    jest.useFakeTimers();

    const mockChangeTab = mockReportingTabsActions('changeTab');

    renderWrapper(withReportingTabsState);

    jest.runAllTimers();

    const promisesPaymentsStatisticsTab = screen.getByRole('tab', {
      name: I18N_REPORTING_TABS.PROMISES_PAYMENTS_STATISTICS
    });

    promisesPaymentsStatisticsTab.click();

    expect(mockChangeTab).toBeCalled();
  });
});
