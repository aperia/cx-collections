import React, { useCallback, useEffect } from 'react';

// components
import { HorizontalTabs } from 'app/_libraries/_dls/components';
import CollectorProductivity from '../CollectorProductivity';
import PromisesPaymentsStatistics from '../PromisesPaymentsStatistics';

// constants
import { I18N_REPORTING_TABS } from './constants';
import { I18N_REPORTING } from '../constants';
import { TAB_IDS } from './types';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { selectedActiveTabId } from './_redux/selectors';
import { reportingTabsActions } from './_redux/reducers';
import { removeStoreRedux } from 'app/helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { queueListActions } from 'pages/QueueList/_redux/reducers';

const ReportingTabs: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const activeTabId = useSelector(selectedActiveTabId);
  const testId = 'reportingTabs';

  const TABS = [
    {
      title: t(I18N_REPORTING_TABS.COLLECTOR_PRODUCTIVITY),
      component: CollectorProductivity,
      id: TAB_IDS.COLLECTOR_PRODUCTIVITY
    },
    {
      title: t(I18N_REPORTING_TABS.PROMISES_PAYMENTS_STATISTICS),
      component: PromisesPaymentsStatistics,
      id: TAB_IDS.PROMISES_PAYMENTS_STATISTICS
    }
  ];

  useEffect(() => {
    dispatch(queueListActions.getQueueList());
  }, [dispatch]);

  useEffect(() => {
    return () => {
      dispatch(removeStoreRedux('reporting'));
    };
  }, [dispatch]);

  const handleChangeTab = useCallback(
    tab => {
      dispatch(reportingTabsActions.changeTab(tab));
    },
    [dispatch]
  );

  return (
    <div className="bg-light-l20">
      <h3 className="p-24" data-testid={genAmtId(testId, 'title', '')}>
        {t(I18N_REPORTING.REPORTING)}
      </h3>
      <HorizontalTabs
        activeKey={activeTabId}
        onSelect={handleChangeTab}
        dataTestId={genAmtId(testId, 'tab', '')}
      >
        {TABS.map(({ title, id, component: Component }) => (
          <HorizontalTabs.Tab key={id} eventKey={id} title={title}>
            <Component />
          </HorizontalTabs.Tab>
        ))}
      </HorizontalTabs>
    </div>
  );
};

export default ReportingTabs;
