import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';
import ReportingNavigation from '.';
import { I18N_REPORTING } from './constants';

const mockTabBarActions = mockActionCreator(actionsTab);

describe('should test reporting navigation', () => {
  it('Render UI + click function', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        isAdminRole: true
      } as CommonConfig
    };

    const spy = mockTabBarActions('addTab');
    const { wrapper } = renderMockStore(<ReportingNavigation />, {
      initialState: {}
    });

    const button = wrapper.getByText('txt_reporting');
    button.click();

    expect(spy).toHaveBeenCalledWith({
      id: 'reporting',
      title: I18N_REPORTING.REPORTING,
      storeId: 'reporting',
      tabType: 'reporting',
      iconName: 'file'
    });
  });

  it('Render empty with collector role', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        isCollector: true
      } as CommonConfig
    };

    const { wrapper } = renderMockStore(<ReportingNavigation />, {
      initialState: {}
    });

    expect(wrapper.queryByText('txt_reporting')).not.toBeInTheDocument();
  });

  it('Render empty with undefined role', () => {
    window.appConfig = {} as AppConfiguration;

    const { wrapper } = renderMockStore(<ReportingNavigation />, {
      initialState: {}
    });

    expect(wrapper.queryByText('txt_reporting')).not.toBeInTheDocument();
  });
});
