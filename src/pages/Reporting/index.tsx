import React from 'react';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Constants
import { I18N_REPORTING } from './constants';

// Redux
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';
import { useDispatch } from 'react-redux';
import { genAmtId } from 'app/_libraries/_dls/utils';

const ReportingNavigation: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { isAdminRole, isSupervisor } = window.appConfig?.commonConfig || {};
  const isAccess = isSupervisor || isAdminRole;

  const handleReportingTab = () => {
    dispatch(
      actionsTab.addTab({
        id: 'reporting',
        title: I18N_REPORTING.REPORTING,
        storeId: 'reporting',
        tabType: 'reporting',
        iconName: 'file'
      })
    );
  };

  return (
    <>
      {isAccess && (
        <span
          className="link-header"
          onClick={handleReportingTab}
          data-testid={genAmtId('header_reporting', 'title', '')}
        >
          {t(I18N_REPORTING.REPORTING)}
        </span>
      )}
    </>
  );
};

export default ReportingNavigation;
