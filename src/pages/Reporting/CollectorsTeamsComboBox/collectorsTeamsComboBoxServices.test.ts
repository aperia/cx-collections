import { collectorsTeamsComboBoxServices } from './collectorsTeamsComboBoxServices';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

describe('collectorsTeamsComboBoxServices', () => {
  it('getCollectors', () => {
    const params = {
      common: {
        privileges: ['role'],
        app: 'app',
        org: 'org',
        userName: 'userName'
      }
    };
    mockAppConfigApi.setup();

    const mockService = mockApiServices.post();

    collectorsTeamsComboBoxServices.getCollectors(params);

    expect(mockService).toBeCalledWith(
      apiUrl.promisesPaymentsStatistics.getCollectors,
      params
    );
  });

  it('getTeams', () => {
    const params = {
      teamName: 'teamName'
    };

    mockAppConfigApi.setup();

    const mockService = mockApiServices.post();

    collectorsTeamsComboBoxServices.getTeams(params);

    expect(mockService).toBeCalledWith(
      apiUrl.promisesPaymentsStatistics.getTeams,
      params
    );
  });
});
