export interface CollectorsTeamsComboBoxState {
  collectors?: Collector[];
  teams?: Team[];
  lazyLoadComboBox: LazyLoadComboBox;
}

export interface Collector {
  id: string;
  name: string;
  userId: string;
  teamId?: string;
  team?: string;
}

export interface Team {
  id: string;
  name: string;
}

export interface LazyLoadComboBox {
  filter?: string;
  itemsLazyLoad?: JSX.Element[];
  opened?: boolean;
}

export interface GetCollectorsArgs {
  collectorName?: string;
}

export interface GetCollectorsBodyRequest {
  token?: string;
}

export interface GetTeamsArgs {
  teamName?: string;
}

export interface GetCollectorsPayload {
  collectors?: Collector[];
}

export interface GetTeamsPayload {
  teams?: Team[];
}
