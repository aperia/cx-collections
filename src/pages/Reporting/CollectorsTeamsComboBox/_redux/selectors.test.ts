import { selectorWrapper } from 'app/test-utils';
import * as selector from './selectors';

describe('should test collector teams combobox selectors', () => {
  const states: Partial<RootState> = {
    collectorsTeamsComboBox: {
      collectors: [
        {
          id: '1',
          name: 'mockName',
          userId: '001',
          teamId: 'team01',
          team: 'ABC'
        },
        {
          id: '2',
          name: 'mockName2',
          userId: '002',
          teamId: 'team02',
          team: 'ABC2'
        }
      ],
      teams: [
        {
          id: 'team01',
          name: 'ABC'
        },
        {
          id: 'team02',
          name: 'ABC2'
        }
      ],
      lazyLoadComboBox: {
        filter: '',
        opened: false
      }
    } as any
  };

  it('should test selectedCollectors selector', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectedCollectors
    );

    expect(data).toEqual([
      {
        id: '1',
        name: 'mockName',
        team: 'ABC',
        teamId: 'team01',
        userId: '001'
      },
      {
        id: '2',
        name: 'mockName2',
        team: 'ABC2',
        teamId: 'team02',
        userId: '002'
      }
    ]);
    expect(emptyData).toEqual(undefined);
  });

  it('should test selectedTeams selector', () => {
    const { data, emptyData } = selectorWrapper(states)(selector.selectedTeams);

    expect(data).toEqual([
      {
        id: 'team01',
        name: 'ABC'
      },
      {
        id: 'team02',
        name: 'ABC2'
      }
    ]);
    expect(emptyData).toEqual(undefined);
  });

  it('should test selectedLazyLoadComboBox selector', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectedLazyLoadComboBox
    );

    expect(data).toEqual({ filter: '', opened: false });
    expect(emptyData).toEqual(undefined);
  });
});
