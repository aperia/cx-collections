import { CollectorsTeamsComboBoxState } from '../types';
import { collectorsTeamsComboBoxActions as actions, reducer } from './reducers';

const initialState: CollectorsTeamsComboBoxState = {
  collectors: [],
  teams: [],
  lazyLoadComboBox: {
    filter: '',
    itemsLazyLoad: [],
    opened: false
  }
};

describe('collectors teams combo box reducers', () => {
  it('should test set collectors reducer', () => {
    const state = reducer(
      initialState,
      actions.setCollectors([{ id: '1', name: 'collector 1' }])
    );

    expect(state.collectors).toEqual([{ id: '1', name: 'collector 1' }]);
  });

  it('should test set teams reducer', () => {
    const state = reducer(
      initialState,
      actions.setTeams([{ id: '1', name: 'team 1' }])
    );

    expect(state.teams).toEqual([{ id: '1', name: 'team 1' }]);
  });

  it('should have setReportTypeComboBox', () => {
    const params = {
      filter: '',
      itemsLazyLoad: [],
      opened: false
    };
    const state = reducer(initialState, actions.setReportTypeComboBox(params));

    expect(state.lazyLoadComboBox).toEqual(params);
  });

  it('should have setReportTypeComboBox', () => {
    const params = {
      filter: '',
      itemsLazyLoad: [],
      opened: false
    };
    const state = reducer(initialState, actions.setReportTypeComboBox(params));

    expect(state.lazyLoadComboBox).toEqual(params);
  });

  it('should have reset', () => {
    const params = {
      filter: '',
      itemsLazyLoad: [],
      opened: false
    };
    const state = reducer(initialState, actions.reset());

    expect(state.lazyLoadComboBox).toEqual(params);
    expect(state.collectors).toEqual([]);
    expect(state.teams).toEqual([]);
  });
});
