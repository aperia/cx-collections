import { responseDefault } from 'app/test-utils';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { collectorsTeamsComboBoxServices } from '../collectorsTeamsComboBoxServices';
import { GetTeamsArgs, GetTeamsPayload } from '../types';
import { getTeams } from './getTeams';

const mockArgs: GetTeamsArgs = {
  teamName: 'team'
};

describe('should test get teams', () => {
  let spy: jest.SpyInstance;
  let store: Store<RootState>;
  let state: RootState;

  beforeEach(() => {
    store = createStore(rootReducer, {
      collectorsTeamsComboBox: {}
    });
    state = store.getState();
  });

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('should return data', async () => {
    spy = jest
      .spyOn(collectorsTeamsComboBoxServices, 'getTeams')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          teams: []
        }
      });

    const response = await getTeams(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      teams: []
    });
  });

  const payload: GetTeamsPayload = {
    teams: []
  };

  it('should be fulfilled', () => {
    const fulfilled = getTeams.fulfilled(
      payload,
      getTeams.fulfilled.type,
      mockArgs
    );

    const actual = rootReducer(state, fulfilled);

    expect(actual.collectorsTeamsComboBox.teams).toEqual([]);
  });
});
