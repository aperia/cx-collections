import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { collectorsTeamsComboBoxServices } from '../collectorsTeamsComboBoxServices';
import {
  CollectorsTeamsComboBoxState,
  GetTeamsArgs,
  GetTeamsPayload
} from '../types';

export const getTeams = createAsyncThunk<
  GetTeamsPayload,
  GetTeamsArgs,
  ThunkAPIConfig
>('collectorsTeamsComboBox/getTeams', async args => {
  const response = await collectorsTeamsComboBoxServices.getTeams(args);

  return {
    teams: response?.data?.teams
  };
});

export const getTeamsBuilder = (
  builder: ActionReducerMapBuilder<CollectorsTeamsComboBoxState>
) => {
  builder.addCase(getTeams.fulfilled, (draftState, action) => {
    draftState.teams = action.payload.teams;
  });
};
