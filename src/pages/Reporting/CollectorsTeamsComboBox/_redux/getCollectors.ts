import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { USER_TOKEN_ID } from 'app/constants';
import { mappingDataFromArray } from 'app/helpers';
import { collectorsTeamsComboBoxServices } from '../collectorsTeamsComboBoxServices';
import {
  Collector,
  CollectorsTeamsComboBoxState,
  GetCollectorsBodyRequest,
  GetCollectorsPayload
} from '../types';

export const getCollectorList = createAsyncThunk<
  GetCollectorsPayload,
  undefined,
  ThunkAPIConfig
>('collectorsTeamsComboBox/getCollectors', async (args, { getState }) => {
  const { mapping, renewToken } = getState();
  const { getCollectors = {} } = mapping.data;

  const body: GetCollectorsBodyRequest = {
    token: renewToken[USER_TOKEN_ID]?.token || ''
  };
  const { data } = await collectorsTeamsComboBoxServices.getCollectors(body);
  const mappingData = mappingDataFromArray<Collector>(
    data?.userList,
    getCollectors
  );
  return {
    collectors: mappingData
  };
});

export const getCollectorsBuilder = (
  builder: ActionReducerMapBuilder<CollectorsTeamsComboBoxState>
) => {
  builder.addCase(getCollectorList.fulfilled, (draftState, action) => {
    draftState.collectors = action.payload.collectors;
  });
};
