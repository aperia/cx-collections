import { createSelector } from 'reselect';
import { CollectorsTeamsComboBoxState } from '../types';

const getCollectorsTeamsComboBox = (states: RootState) =>
  states?.collectorsTeamsComboBox;

export const selectedCollectors = createSelector(
  getCollectorsTeamsComboBox,
  (data: CollectorsTeamsComboBoxState) => data?.collectors
);

export const selectedTeams = createSelector(
  getCollectorsTeamsComboBox,
  (data: CollectorsTeamsComboBoxState) => data?.teams
);

export const selectedLazyLoadComboBox = createSelector(
  getCollectorsTeamsComboBox,
  (data: CollectorsTeamsComboBoxState) => data?.lazyLoadComboBox
);
