import { responseDefault } from 'app/test-utils';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { collectorsTeamsComboBoxServices } from '../collectorsTeamsComboBoxServices';
import { GetCollectorsArgs, GetCollectorsPayload } from '../types';
import { getCollectorList } from './getCollectors';

const mockArgs: GetCollectorsArgs = {
  collectorName: 'collector'
};

describe('should test get collectors', () => {
  beforeEach(() => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        org: 'org',
        app: 'app'
      }
    };
  });

  let spy: jest.SpyInstance;

  const store: Store<RootState> = createStore(rootReducer, {
    collectorsTeamsComboBox: {},
    mapping: {
      data: {
        getCollectors: {
          id: 'collectorCode',
          name: '{userFirstName}, {userLastName}',
          team: 'collectorCode'
        }
      }
    }
  });
  const state: RootState = store.getState();

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('should return data', async () => {
    spy = jest
      .spyOn(collectorsTeamsComboBoxServices, 'getCollectors')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          collectors: []
        }
      });

    const response = await getCollectorList(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      collectors: []
    });
  });

  it('should have undefined common config', async () => {
    window.appConfig = {} as AppConfiguration;

    spy = jest
      .spyOn(collectorsTeamsComboBoxServices, 'getCollectors')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          collectors: []
        }
      });

    const response = await getCollectorList(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      collectors: []
    });
  });

  it('should have undefined mapping.data', async () => {
    window.appConfig = {} as AppConfiguration;
    const store2: Store<RootState> = createStore(rootReducer, {
      collectorsTeamsComboBox: {},
      mapping: {
        data: {
          getCollectors: undefined
        }
      }
    });
    spy = jest
      .spyOn(collectorsTeamsComboBoxServices, 'getCollectors')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          collectors: []
        }
      });

    const response = await getCollectorList(mockArgs)(
      store2.dispatch,
      store2.getState,
      {}
    );

    expect(response.payload).toEqual({
      collectors: []
    });
  });

  it('should have collectorName is undefined', async () => {
    spy = jest
      .spyOn(collectorsTeamsComboBoxServices, 'getCollectors')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          collectors: []
        }
      });

    const response = await getCollectorList({ collectorName: undefined })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.meta.arg.collectorName).toEqual(undefined);
  });

  const payload: GetCollectorsPayload = {
    collectors: []
  };

  it('should be fulfilled', () => {
    const fulfilled = getCollectorList.fulfilled(
      payload,
      getCollectorList.fulfilled.type,
      mockArgs
    );

    const actual = rootReducer(state, fulfilled);

    expect(actual.collectorsTeamsComboBox.collectors).toEqual([]);
  });
});
