import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  Collector,
  CollectorsTeamsComboBoxState,
  LazyLoadComboBox,
  Team
} from '../types';
import { getCollectorList, getCollectorsBuilder } from './getCollectors';
import { getTeams, getTeamsBuilder } from './getTeams';

const initialState: CollectorsTeamsComboBoxState = {
  lazyLoadComboBox: {
    filter: '',
    opened: false
  }
};

const { actions, reducer } = createSlice({
  name: 'collectorsTeamsComboBox',
  initialState,
  reducers: {
    setReportTypeComboBox: (
      draftState,
      action: PayloadAction<LazyLoadComboBox>
    ) => {
      draftState.lazyLoadComboBox = {
        ...draftState.lazyLoadComboBox,
        ...action.payload
      };
    },
    setCollectors: (draftState, action: PayloadAction<Collector[]>) => {
      draftState.collectors = action.payload;
    },
    setTeams: (draftState, action: PayloadAction<Team[]>) => {
      draftState.teams = action.payload;
    },
    reset: draftState => {
      draftState.lazyLoadComboBox = {
        ...draftState.lazyLoadComboBox,
        filter: '',
        opened: false
      };
    },
    resetComboBoxData: draftState => {
      draftState.lazyLoadComboBox = {
        ...draftState.lazyLoadComboBox,
        filter: '',
        opened: false,
        itemsLazyLoad: []
      };
    },
    removeStore: () => initialState
  },
  extraReducers: builder => {
    getCollectorsBuilder(builder);
    getTeamsBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getCollectorList,
  getTeams
};

export { allActions as collectorsTeamsComboBoxActions, reducer };
