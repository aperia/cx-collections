import React, { useContext, useEffect, useMemo } from 'react';

// components
import { ComboBox } from 'app/_libraries/_dls/components';
import { FormikContext, FormikProps } from 'formik';

// constants
import { COMBO_BOX_DATA, REPORT_TYPE_VALUE } from '../constants';
import { SettingFilter } from '../types';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { selectedCollectors, selectedTeams } from './_redux/selectors';
import { collectorsTeamsComboBoxActions } from './_redux/reducers';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import isString from 'lodash.isstring';

export const CollectorsTeamsComboBox: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const collectors = useSelector(selectedCollectors);
  const teams = useSelector(selectedTeams);
  const formikContext = useContext<FormikProps<SettingFilter>>(FormikContext);
  const { values, errors, handleChange, handleBlur, touched } = formikContext;

  const comboBoxArgs = useMemo(
    () => COMBO_BOX_DATA[values.reportType],
    [values.reportType]
  );

  const getComboBoxValue = useMemo(() => {
    if (values.collector) {
      return values?.collector;
    }
    if (values.team) {
      return values?.team;
    }
  }, [values]);

  useEffect(() => {
    dispatch(
      values.reportType === REPORT_TYPE_VALUE.INDIVIDUAL
        ? collectorsTeamsComboBoxActions.getCollectorList()
        : collectorsTeamsComboBoxActions.getTeams({
            teamName: ''
          })
    );
  }, [dispatch, values.reportType]);

  const comboBoxItems = useMemo(
    () =>
      (values.reportType === REPORT_TYPE_VALUE.INDIVIDUAL
        ? collectors?.map((item, index) => (
            <ComboBox.Item
              key={`${index}-${item.id}`}
              label={item.name}
              value={item}
              variant="text"
              textProps={{ children: item.id }}
            />
          ))
        : teams?.map((item, index) => (
            <ComboBox.Item
              key={`${index}-${item.id}`}
              label={item.name}
              value={item}
              variant="text"
            />
          ))) || [],
    [collectors, teams, values.reportType]
  );

  return (
    <div className="row mt-16">
      <div className="col-12">
        <ComboBox
          value={getComboBoxValue}
          required
          textField="name"
          label={t(comboBoxArgs?.label)}
          name={t(comboBoxArgs?.name)}
          onChange={handleChange}
          onBlur={handleBlur}
          error={
            touched?.collector || touched?.team
              ? {
                  status: isString(errors?.collector) || isString(errors?.team),
                  message: t(errors?.collector) || t(errors?.team)
                }
              : undefined
          }
          dataTestId="collectorsTeamsComboBox"
          noResult={t('txt_no_results_found')}
          checkAllLabel={t('txt_all')}
        >
          {comboBoxItems}
        </ComboBox>
      </div>
    </div>
  );
};
