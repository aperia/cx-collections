import { apiService } from 'app/utils/api.service';
import { GetCollectorsBodyRequest, GetTeamsArgs } from './types';

export const collectorsTeamsComboBoxServices = {
  getCollectors(data: GetCollectorsBodyRequest) {
    const url = window.appConfig?.api?.promisesPaymentsStatistics.getCollectors;

    return apiService.post(url!, data);
  },

  getTeams(args: GetTeamsArgs) {
    const url = window.appConfig?.api?.promisesPaymentsStatistics.getTeams;

    return apiService.post(url!, args);
  }
};
