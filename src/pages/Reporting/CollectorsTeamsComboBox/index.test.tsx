import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { CollectorsTeamsComboBox } from '.';
import { fireEvent, screen } from '@testing-library/react';
import {
  FormikContextType,
  FormikProvider,
  FormikTouched,
  FormikValues
} from 'formik';
import { REPORT_TYPE_VALUE } from '../constants';
import { collectorsTeamsComboBoxActions } from './_redux/reducers';
import { SettingFilter } from '../types';
import userEvent from '@testing-library/user-event';
import { ComboBox } from 'app/_libraries/_dls/components';

jest.mock('lodash.debounce', () => {
  const entries = [{ contentRect: {} }];

  return (fn: Function) => () => fn(entries);
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockCollectorsTeamsComboBoxActions = mockActionCreator(
  collectorsTeamsComboBoxActions
);

const mockCollectorData = [{ id: '1', name: 'collector 1' }];
const mockTeamData = [{ id: '1', name: 'team 1' }];

describe('should test collectors teams combo box', () => {
  describe('should test with report type individual', () => {
    const withCollectorsTeamsState: Partial<RootState> = {
      collectorsTeamsComboBox: {
        collectors: mockCollectorData,
        teams: mockTeamData,
        lazyLoadComboBox: {
          filter: '',
          itemsLazyLoad: [
            <ComboBox.Item
              key={`1`}
              label={'test'}
              value={'test'}
              variant="text"
            />
          ],
          opened: false
        }
      }
    };

    const mockFormikValues: FormikValues = {
      reportType: REPORT_TYPE_VALUE.INDIVIDUAL,
      collector: 'c'
    };

    const mockFormikValuesWithEmptyData: FormikValues = {
      reportType: REPORT_TYPE_VALUE.INDIVIDUAL
    };

    const renderWrapper = (
      initialState: Partial<RootState>,
      formikValues: FormikValues
    ) => {
      return renderMockStore(
        <FormikProvider
          value={
            {
              values: formikValues,
              touched: {
                collector: true
              } as FormikTouched<any>,
              setValues: (values: SettingFilter) => {}
            } as FormikContextType<any>
          }
        >
          <CollectorsTeamsComboBox />
        </FormikProvider>,
        { initialState }
      );
    };

    it('handle focus', () => {
      const mockSetReportTypeComboBox = mockCollectorsTeamsComboBoxActions(
        'setReportTypeComboBox'
      );

      const { baseElement } = renderWrapper(
        withCollectorsTeamsState,
        mockFormikValues
      );

      const containerElement = baseElement.querySelector(
        '.text-field-container input'
      )!;
      fireEvent.focus(containerElement);

      expect(mockSetReportTypeComboBox).not.toBeCalled();
    });

    it('handle change > type less than 3 characters', () => {
      jest.useFakeTimers();

      const mockSetReportTypeComboBox = mockCollectorsTeamsComboBoxActions(
        'setReportTypeComboBox'
      );
      const mockGetCollectors =
        mockCollectorsTeamsComboBoxActions('getCollectorList');

      renderWrapper(withCollectorsTeamsState, mockFormikValuesWithEmptyData);

      jest.runAllTimers();

      const comboBoxInput = screen.getByTestId(
        'collectorsTeamsComboBox_dls-combobox'
      );

      userEvent.type(comboBoxInput, '1');

      expect(mockSetReportTypeComboBox).not.toBeCalled();
      expect(mockGetCollectors).toBeCalled();
    });

    it('handle change > type more than 3 characters', () => {
      jest.useFakeTimers();

      const mockSetReportTypeComboBox = mockCollectorsTeamsComboBoxActions(
        'setReportTypeComboBox'
      );
      const mockGetCollectors =
        mockCollectorsTeamsComboBoxActions('getCollectorList');

      renderWrapper(withCollectorsTeamsState, mockFormikValuesWithEmptyData);

      const comboBoxInput = screen.getByTestId(
        'collectorsTeamsComboBox_dls-combobox'
      );

      userEvent.type(comboBoxInput, '1111');

      expect(mockSetReportTypeComboBox).not.toBeCalled();
      expect(mockGetCollectors).toBeCalled();
    });

    it('handle change > type after select', () => {
      jest.useFakeTimers();

      const mockSetReportTypeComboBox = mockCollectorsTeamsComboBoxActions(
        'setReportTypeComboBox'
      );

      renderWrapper(withCollectorsTeamsState, mockFormikValues);

      const comboBoxInput = screen.getByTestId(
        'collectorsTeamsComboBox_dls-combobox'
      );

      userEvent.type(comboBoxInput, '1');

      expect(mockSetReportTypeComboBox).not.toBeCalled();
    });

    it('handle focus, blur without available data', () => {
      const mockSetReportTypeComboBox = mockCollectorsTeamsComboBoxActions(
        'setReportTypeComboBox'
      );

      const { baseElement } = renderWrapper(
        withCollectorsTeamsState,
        mockFormikValuesWithEmptyData
      );
      const containerElement = baseElement.querySelector(
        '.text-field-container input'
      )!;
      fireEvent.focus(containerElement);

      expect(mockSetReportTypeComboBox).not.toBeCalled();

      const comboBoxInput = screen.getByTestId(
        'collectorsTeamsComboBox_dls-combobox'
      );

      fireEvent.blur(comboBoxInput);

      expect(mockSetReportTypeComboBox).not.toBeCalled();
    });
  });

  describe('should test with report type roll up', () => {
    const withCollectorsTeamsState: Partial<RootState> = {
      collectorsTeamsComboBox: {
        collectors: mockCollectorData,
        teams: mockTeamData,
        lazyLoadComboBox: {
          filter: 'team',
          itemsLazyLoad: [],
          opened: false
        }
      }
    };

    const mockFormikValues: FormikValues = {
      reportType: REPORT_TYPE_VALUE.ROLL_UP,
      team: 'Team'
    };

    const mockFormikValuesWithEmptyData: FormikValues = {
      reportType: REPORT_TYPE_VALUE.ROLL_UP
    };

    const renderWrapper = (
      initialState: Partial<RootState>,
      formikValues: FormikValues
    ) => {
      return renderMockStore(
        <FormikProvider
          value={
            {
              values: formikValues,
              touched: {} as FormikTouched<any>,
              setValues: (values: SettingFilter) => {}
            } as FormikContextType<any>
          }
        >
          <CollectorsTeamsComboBox />
        </FormikProvider>,
        { initialState }
      );
    };

    it('event control', () => {
      const mockSetReportTypeComboBox = mockCollectorsTeamsComboBoxActions(
        'setReportTypeComboBox'
      );

      renderWrapper(withCollectorsTeamsState, mockFormikValues);

      const comboBoxInput = screen.getByTestId(
        'collectorsTeamsComboBox_dls-combobox'
      );

      userEvent.type(comboBoxInput, '1111');

      expect(mockSetReportTypeComboBox).not.toBeCalled();
    });

    it('event control without current data', () => {
      const mockSetReportTypeComboBox = mockCollectorsTeamsComboBoxActions(
        'setReportTypeComboBox'
      );

      renderWrapper(withCollectorsTeamsState, mockFormikValuesWithEmptyData);

      const comboBoxInput = screen.getByTestId(
        'collectorsTeamsComboBox_dls-combobox'
      );

      fireEvent.blur(comboBoxInput);

      expect(mockSetReportTypeComboBox).not.toBeCalled();
    });

    it('event control without fetch data', () => {
      const mockSetReportTypeComboBox = mockCollectorsTeamsComboBoxActions(
        'setReportTypeComboBox'
      );

      renderWrapper(
        {
          collectorsTeamsComboBox: {
            collectors: mockCollectorData,
            teams: undefined,
            lazyLoadComboBox: {
              filter: 'team',
              itemsLazyLoad: [],
              opened: false
            }
          }
        },
        mockFormikValuesWithEmptyData
      );

      const comboBoxInput = screen.getByTestId(
        'collectorsTeamsComboBox_dls-combobox'
      );

      fireEvent.blur(comboBoxInput);

      expect(mockSetReportTypeComboBox).not.toBeCalled();
    });
  });
});
