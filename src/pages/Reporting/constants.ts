import { I18N_PROMISES_PAYMENTS_STATISTICS } from 'app/constants/i18n';
import { ComboBoxData } from './types';

export const I18N_REPORTING = {
  REPORTING: 'txt_reporting'
};

export enum REPORT_TYPE_VALUE {
  INDIVIDUAL = 'Individual',
  ROLL_UP = 'Roll Up'
}

export const FORM_NAMES = {
  TIME_PERIOD: 'timePeriod',
  PROMISE_DATE_PICKER: 'ppsDatePicker',
  RANGE_DATE_PICKER: 'rangeDatePicker',
  COLLECTOR_COMBO_BOX: 'collector',
  TEAM_COMBO_BOX: 'team',
  REPORT_TYPE: 'reportType',
  PROMISE_TYPE: 'promiseType',
  PROMISE_STATUS: 'promiseStatus',
  PROMISE_AMOUNT_FROM: 'ppsPromiseAmountFrom',
  PROMISE_AMOUNT_TO: 'ppsPromiseAmountTo',
  DATE_PICKER: 'ppsDatePicker'
};

export const COMBO_BOX_DATA: Record<REPORT_TYPE_VALUE, ComboBoxData> = {
  [REPORT_TYPE_VALUE.INDIVIDUAL]: {
    label: I18N_PROMISES_PAYMENTS_STATISTICS.COLLECTOR_NAME,
    placeholder: I18N_PROMISES_PAYMENTS_STATISTICS.TYPE_AT_LEAST_3_CHARACTERS,
    name: FORM_NAMES.COLLECTOR_COMBO_BOX
  },
  [REPORT_TYPE_VALUE.ROLL_UP]: {
    label: I18N_PROMISES_PAYMENTS_STATISTICS.TEAM_NAME,
    placeholder: I18N_PROMISES_PAYMENTS_STATISTICS.TYPE_AT_LEAST_3_CHARACTERS,
    name: FORM_NAMES.TEAM_COMBO_BOX
  }
};
