import { genAmtId } from 'app/_libraries/_dls/utils';
import React from 'react';
import { FormFieldProps } from './types';

export const ReportingField = ({
  title,
  children,
  disabled,
  dataTestId
}: FormFieldProps) => {
  if (disabled) return null;

  return (
    <div className="row mt-24">
      <div className="col-12">
        {title && (
          <h6
            className="color-grey"
            data-testid={genAmtId(dataTestId, 'reporting-field-title', '')}
          >
            {title}
          </h6>
        )}
      </div>
      {children}
    </div>
  );
};
