export interface LongTermMedicalState {
  [storeId: string]: LongTermMedicalData;
}

export interface GetLongTermMedicalArg {
  storeId: string;
}

export interface GetLongMedicalRequestBody extends CommonAccountIdRequestBody {
  callingApplication: string;
  operatorCode: string;
}

export interface LongTermMedicalData {
  callResultType?: string;
  data?: LongTermMedicalPayload;
  isOpenModal?: boolean;
  isLoadingModal?: boolean;
  isLoading?: boolean;
}

export interface LongTermMedicalPayload {
  longTermMedicalDetails?: string;
  longTermMedicalStatus?: string;
}

export interface UpdateDataAction extends StoreIdPayload {
  data: LongTermMedicalPayload;
}

export interface UpdateDataFromLastFollowUpPayloadAction {
  storeId: string;
  callResultType?: string;
  data?: LongTermMedicalPayload;
}

export enum TRIGGER_TYPE {
  CREATE = 'create',
  EDIT = 'edit'
}

export interface UpdateLongTermMedicalRequest
  extends CommonAccountIdRequestBody {
  medicalDetails: string;
  callingApplication: string;
  operatorCode: string;
  cardholderContactedName?: string;
  cardholderContactedPhoneNumber?: string;
}
export interface TriggerUpdateLongTermMedicalArg extends StoreIdPayload {
  postData: UpdateLongTermMedicalRequest;
  type?: TRIGGER_TYPE;
}
