import { isEqualPostDataAndSavedData } from './helpers';

describe('Helpers', () => {
  it('isEqualPostDataAndSavedData', () => {
    const postData = {
      test: '123'
    };
    const saveData = {
      longTermMedicalDetails: '123'
    };
    const result = isEqualPostDataAndSavedData(postData, saveData);
    expect(result).toBeFalsy();
  });
});
