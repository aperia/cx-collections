import isEqual from 'lodash.isequal';

export const isEqualPostDataAndSavedData = (
  postData: MagicKeyValue,
  savedData: MagicKeyValue
) => {
  const compareData = {
    medicalDetails: savedData?.longTermMedicalDetails
  };

  return isEqual(postData, compareData);
};
