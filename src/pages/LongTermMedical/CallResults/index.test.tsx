import React from 'react';
import { screen } from '@testing-library/react';

// utils
import { renderMockStoreId, storeId } from 'app/test-utils';

// components
import CallResults from '.';

// redux
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';

jest.mock('./ModalRemoveStatus', () => () => (
  <div>txt_confirm_remove_long_term_medical</div>
));

const initialState: Partial<RootState> = {
  longTermMedical: {
    [storeId]: {
      isLoading: true,
      data: { longTermMedicalDetails: 'impactDetail' }
    }
  }
};

describe('Render', () => {
  it('should render', () => {
    renderMockStoreId(<CallResults />, { initialState });
    expect(
      screen.getByText(I18N_COLLECTION_FORM.LONG_TEAM_MEDICAL_INFORMATION)
    ).toBeInTheDocument();
  });
});
