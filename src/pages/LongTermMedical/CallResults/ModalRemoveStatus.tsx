import React from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useStoreIdSelector } from 'app/hooks';
import { useAccountDetail } from 'app/hooks';

// redux
import { useDispatch } from 'react-redux';
import { selectLoadingModal, selectOpenModal } from '../_redux/selectors';
import { longTermMedicalActions } from '../_redux/reducers';

// helper
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { genAmtId } from 'app/_libraries/_dls/utils';

const ModalRemoveStatus: React.FC = () => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const loading: boolean = useStoreIdSelector(selectLoadingModal);
  const isOpen: boolean = useStoreIdSelector(selectOpenModal);
  const { t } = useTranslation();
  const testId = 'longTermMedical-removeModal';

  const handleRemove = () => {
    dispatch(
      longTermMedicalActions.removeLongTermMedical({
        storeId
      })
    );
  };

  const handleCloseModal = () => {
    dispatch(
      longTermMedicalActions.onChangeOpenModal({
        storeId
      })
    );
  };

  return (
    <Modal
      sm
      show={isOpen}
      loading={loading}
      className="window-lg-full"
      dataTestId={testId}
    >
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId={`${testId}-header`}
      >
        <ModalTitle dataTestId={`${testId}-title`}>
          {t('txt_confirm_remove_long_term_medical')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <p data-testid={genAmtId(testId, 'remove-description', '')}>
          {t('txt_are_you_sure_remove_long_term_medical')}
        </p>
      </ModalBody>
      <ModalFooter>
        <Button
          type="submit"
          variant="secondary"
          onClick={handleCloseModal}
          dataTestId={`${testId}_submit-btn`}
        >
          {t('txt_cancel')}
        </Button>
        <Button
          type="submit"
          onClick={handleRemove}
          variant="danger"
          dataTestId={`${testId}_remove-btn`}
        >
          {t('txt_remove')}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ModalRemoveStatus;
