import React from 'react';
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import { longTermMedicalActions } from '../_redux/reducers';
import ModalRemoveStatus from './ModalRemoveStatus';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

const longTermMedicalActionsMock = mockActionCreator(longTermMedicalActions);
describe('ModalRemoveStatus', () => {
  const initialState = {
    longTermMedical: {
      [storeId]: { isOpenModal: true }
    }
  };
  it('render UI ', () => {
    renderMockStoreId(<ModalRemoveStatus />, {
      initialState
    });

    expect(
      screen.getByText('txt_are_you_sure_remove_long_term_medical')
    ).toBeInTheDocument();
  });
  it(' remove action ', () => {
    const removeLongTermMedical = longTermMedicalActionsMock(
      'removeLongTermMedical'
    );
    renderMockStoreId(<ModalRemoveStatus />, {
      initialState
    });
    userEvent.click(screen.getByText('txt_remove'));
    expect(removeLongTermMedical).toBeCalledWith({
      storeId
    });
  });

  it(' handleCloseModal action ', () => {
    const onChangeOpenModal = longTermMedicalActionsMock('onChangeOpenModal');
    renderMockStoreId(<ModalRemoveStatus />, {
      initialState
    });
    userEvent.click(screen.getByText('txt_cancel'));
    expect(onChangeOpenModal).toBeCalledWith({
      storeId
    });
  });
});
