import React, { useEffect } from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// redux
import { selectData } from '../_redux/selectors';

// helper
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import ModalRemoveStatus from './ModalRemoveStatus';
import { longTermMedicalActions } from '../_redux/reducers';
import { useDispatch } from 'react-redux';
import GroupTextControl from 'app/components/_dof_controls/controls/GroupTextControl';
import { GroupTextFormat } from 'app/constants/enums';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface CallResultsProps {}

const CallResults: React.FC<CallResultsProps> = () => {
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();

  const longTermMedicalData = useStoreIdSelector<string>(selectData);

  const testId = 'longTermMedical-callResults';

  useEffect(() => {
    dispatch(
      longTermMedicalActions.getLongTermMedical({
        storeId
      })
    );
  }, [dispatch, storeId]);

  useEffect(() => {
    return () => {
      dispatch(longTermMedicalActions.removeStore({ storeId }));
    };
  }, [dispatch, storeId]);

  return (
    <>
      <h6
        className="color-grey mt-8"
        data-testid={genAmtId(testId, 'title', '')}
      >
        {t(I18N_COLLECTION_FORM.LONG_TEAM_MEDICAL_INFORMATION)}
      </h6>

      <GroupTextControl
        id={t(I18N_COLLECTION_FORM.LONG_TEAM_MEDICAL_IMPACT)}
        input={{ value: longTermMedicalData } as any}
        meta={{} as any}
        label={t(I18N_COLLECTION_FORM.LONG_TEAM_MEDICAL_IMPACT)}
        options={
          { format: GroupTextFormat.TruncateLessMore, truncateLines: 2 } as any
        }
        dataTestId={genAmtId(testId, 'content', 'GroupTextControl')}
      />
      <ModalRemoveStatus />
    </>
  );
};

export default CallResults;
