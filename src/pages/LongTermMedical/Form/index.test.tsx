import React from 'react';
import { fireEvent, waitFor } from '@testing-library/react';

// utils
import { renderMockStoreId, storeId, accEValue } from 'app/test-utils';

// components
import Form from '.';
import { mockActionCreator } from 'app/test-utils';

// redux
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { longTermMedicalActions } from '../_redux/reducers';
import { TRIGGER_TYPE } from '../types';
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';
import { CARD_HOLDER_CONTACT_NUMBER } from 'app/constants';
import { AccountDetailProvider } from 'app/hooks';
import { act } from 'react-dom/test-utils';

const textErea101 =
  '1232132132132131232131231232132132132131231232132131232131232132132132132132132132131231232132131312321321321321312321312321321321312313121312';

const initialState: Partial<RootState> = {
  longTermMedical: {
    [storeId]: {
      isLoading: true,
      data: { longTermMedicalDetails: 'impactDetail' }
    }
  },
  collectionForm: { [storeId]: { method: TRIGGER_TYPE.EDIT, uploadFiles: {} } },
  accountDetail: {
    [storeId]: {
      selectedAuthenticationData: {
        customerNameForCreateWorkflow: '123'
      }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <Form />
    </AccountDetailProvider>,
    {
      initialState
    }
  );
};

const collectionFormSpy = mockActionCreator(collectionFormActions);
const longTermMedicalSpy = mockActionCreator(longTermMedicalActions);

describe('Render', () => {
  it('should render', () => {
    jest.useFakeTimers();
    const { wrapper } = renderWrapper(initialState);
    jest.runAllTimers();
    expect(wrapper.getByText('txt_long_term_medical')).toBeInTheDocument();
  });

  it('handleCancel', () => {
    const spy = collectionFormSpy('setCurrentView');
    const spy2 = collectionFormSpy('selectCallResult');
    const spy3 = collectionFormSpy('changeMethod');
    jest.useFakeTimers();
    const { wrapper } = renderWrapper(initialState);
    jest.runAllTimers();
    const button = wrapper.getByText('txt_cancel')!;
    button.click();

    expect(spy).toBeCalledWith({
      storeId
    });
    expect(spy2).toBeCalledWith({
      storeId,
      item: undefined
    });
    expect(spy3).toBeCalledWith({
      storeId,
      method: COLLECTION_METHOD.CREATE
    });
  });

  it('submitForm', () => {
    waitFor(() => {
      jest.useFakeTimers();
      const spy = longTermMedicalSpy('triggerUpdateLongTermMedical');
      const { wrapper } = renderWrapper({
        longTermMedical: {
          [storeId]: {
            isLoading: false,
            data: { longTermMedicalDetails: 'impactDetail' }
          }
        },
        collectionForm: {
          [storeId]: { method: TRIGGER_TYPE.CREATE, uploadFiles: {} }
        },
        accountDetail: {
          [storeId]: {
            selectedAuthenticationData: {
              customerNameForCreateWorkflow: '123'
            }
          }
        }
      });
      act(() => {
        const textElement = wrapper.container.querySelector('textarea');
        fireEvent.change(textElement!, {
          target: { value: textErea101 }
        });
        jest.runAllTimers();
      });
      const button = wrapper.getByText('txt_submit')!;

      button.click();

      expect(spy).toBeCalledWith({
        storeId,
        postData: {
          common: {
            app: {},
            org: {},
            accountId: accEValue
          },
          callingApplication: '',
          operatorCode: '',
          cardholderContactedName: 'contactName',
          cardholderContactedPhoneNumber: CARD_HOLDER_CONTACT_NUMBER,
          medicalDetails: 'values.textArea'
        },
        type: TRIGGER_TYPE.CREATE
      });
      jest.resetAllMocks();
    });
  });

  it('submitForm with edit form', () => {
    waitFor(() => {
      const spy = longTermMedicalSpy('triggerUpdateLongTermMedical');
      jest.useFakeTimers();

      const { wrapper } = renderWrapper({
        longTermMedical: {
          [storeId]: {
            isLoading: true,
            data: {
              longTermMedicalDetails: 'longTermMedicalDetails'
            }
          }
        },
        collectionForm: {
          [storeId]: { method: TRIGGER_TYPE.EDIT, uploadFiles: {} }
        },
        accountDetail: {
          [storeId]: {
            selectedAuthenticationData: {
              customerNameForCreateWorkflow: '123'
            }
          }
        }
      });
      act(() => {
        const textElement = wrapper.container.querySelector('textarea');
        fireEvent.change(textElement!, {
          target: { value: 'textarea change' }
        });
        jest.runAllTimers();
      });
      const button = wrapper.getByText('txt_save')!;
      button.click();

      expect(spy).toBeCalledWith({
        storeId,
        postData: {
          common: {
            app: {},
            org: {},
            accountId: accEValue
          },
          callingApplication: '',
          operatorCode: '',
          cardholderContactedName: 'contactName',
          cardholderContactedPhoneNumber: CARD_HOLDER_CONTACT_NUMBER,
          medicalDetails: 'values.textArea'
        },
        type: TRIGGER_TYPE.CREATE
      });
      jest.resetAllMocks();
    });
  });
});
