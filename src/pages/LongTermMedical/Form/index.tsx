import React, {
  Fragment,
  useEffect,
  useLayoutEffect,
  useRef,
  useState
} from 'react';
import classNames from 'classnames';

// components
import { Button, Icon, TextArea } from 'app/_libraries/_dls/components';

// redux
import { batch, useDispatch } from 'react-redux';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { getCollectionFormMethod } from 'pages/CollectionForm/_redux/selectors';
import { longTermMedicalActions } from '../_redux/reducers';
import { selectCardholderContactName, selectData } from '../_redux/selectors';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useFormik } from 'formik';

// helper
import { I18N_COLLECTION_FORM, I18N_COMMON_TEXT } from 'app/constants/i18n';

// Const
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';
import { TRIGGER_TYPE } from '../types';
import { EMPTY_STRING } from 'app/constants';
import SimpleBarWithApiError from 'pages/CollectionForm/SimpleBarWithApiError';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface LongTermMedicalProps {}

const LongTermMedical: React.FC<LongTermMedicalProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const maxLength = 100;
  const { storeId, accEValue } = useAccountDetail();
  const [restCharacter, setRestCharacter] = useState<number>(maxLength);
  const divRef = useRef<HTMLDivElement | null>(null);

  const impactData = useStoreIdSelector<string>(selectData);
  const method = useStoreIdSelector<string>(getCollectionFormMethod);

  const contactName = useStoreIdSelector<string>(selectCardholderContactName);
  const isEdit = method === TRIGGER_TYPE.EDIT;
  const { commonConfig } = window.appConfig || {};
  const testId = 'longTermMedical';

  useEffect(() => {
    document.body.classList.add('hide-float-btn');
    return () => {
      document.body.classList.remove('hide-float-btn');
    };
  }, []);

  const {
    values,
    handleChange,
    handleBlur,
    submitForm,
    dirty,
    validateForm,
    errors,
    touched
  } = useFormik({
    initialValues: {
      textArea: isEdit ? impactData : ''
    },
    onSubmit: values => {
      dispatch(
        longTermMedicalActions.triggerUpdateLongTermMedical({
          storeId,
          postData: {
            common: {
              app: commonConfig?.app || EMPTY_STRING,
              org: commonConfig?.org || EMPTY_STRING,
              accountId: accEValue
            },
            callingApplication: commonConfig?.callingApplicationApr01 || '',
            operatorCode: commonConfig?.operatorID || EMPTY_STRING,
            cardholderContactedName: contactName,
            medicalDetails: values.textArea
          },
          type: isEdit ? TRIGGER_TYPE.EDIT : TRIGGER_TYPE.CREATE
        })
      );
    },
    validateOnBlur: true,
    validateOnChange: false,
    validate: ({ textArea }) => {
      const formErrors: Record<string, string> | undefined = {};
      if (!textArea?.trim()) {
        formErrors['textArea'] =
          I18N_COLLECTION_FORM.LONG_TEAM_MEDICAL_REQUIRED;
      }
      return formErrors;
    }
  });

  useEffect(() => {
    validateForm();
  }, [validateForm]);

  useEffect(() => {
    values && setRestCharacter(maxLength - values?.textArea?.length);
  }, [values]);

  const formatCharacterLeft = (restChar: number) => {
    if (restChar < 0)
      return t('txt_character_over_limit', { count: Math.abs(restChar) });
    return t('txt_character_left', { count: restChar });
  };

  const handleCancel = () => {
    batch(() => {
      dispatch(collectionFormActions.setCurrentView({ storeId }));
      dispatch(
        collectionFormActions.selectCallResult({ storeId, item: undefined })
      );
      dispatch(
        collectionFormActions.changeMethod({
          storeId,
          method: COLLECTION_METHOD.CREATE
        })
      );
    });
  };

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100vh - 258px)`);
  }, []);

  useEffect(() => {
    if (isEdit) {
      dispatch(
        longTermMedicalActions.getLongTermMedical({
          storeId
        })
      );
    }

    return () => {
      dispatch(longTermMedicalActions.removeStore({ storeId }));
    };
  }, [dispatch, storeId, isEdit]);

  return (
    <Fragment>
      <div ref={divRef}>
        <SimpleBarWithApiError className="ml-24 mt-24">
          <div className="pt-24 px-24">
            <h5
              className="mr-auto"
              data-testid={genAmtId(testId, 'call-result', '')}
            >
              {t(I18N_COLLECTION_FORM.CALL_RESULT)}
            </h5>
            <div className="mt-16 p-8 bg-light-l16 d-flex align-items-center rounded-lg">
              <div className="bubble-icon">
                <Icon name="megaphone" />
              </div>

              <p className="ml-12" data-testid={genAmtId(testId, 'title', '')}>
                {t(I18N_COLLECTION_FORM.LONG_TEAM_MEDICAL)}
              </p>
            </div>
            <h6
              className="color-grey mt-24"
              data-testid={genAmtId(testId, 'information', '')}
            >
              {t(I18N_COLLECTION_FORM.LONG_TEAM_MEDICAL_INFORMATION)}
            </h6>
            <p
              className="fw-500 color-grey mt-16 mb-8"
              data-testid={genAmtId(testId, 'impact', '')}
            >
              {t(I18N_COLLECTION_FORM.LONG_TEAM_MEDICAL_IMPACT)}
              <span className="color-red ml-4">*</span>
            </p>
            <TextArea
              id="123"
              name="textArea"
              placeholder={t(
                I18N_COLLECTION_FORM.LONG_TEAM_MEDICAL_ENTER_IMPACT
              )}
              className="resize-none h-100"
              required
              maxLength={100}
              value={values.textArea}
              onChange={handleChange}
              onBlur={handleBlur}
              error={{
                status: Boolean(errors.textArea && touched.textArea),
                message: t(errors.textArea) as string
              }}
              dataTestId={genAmtId(testId, 'impact-input', '')}
            />
            <p
              className={classNames(
                'color-grey-l24 fs-12 mt-8',
                restCharacter < 0 && 'color-danger'
              )}
              data-testid={genAmtId(testId, 'count-character', '')}
            >
              {formatCharacterLeft(restCharacter)}
            </p>
          </div>
        </SimpleBarWithApiError>
      </div>
      <div className="group-button-footer d-flex justify-content-end">
        <Button
          variant="secondary"
          onClick={handleCancel}
          dataTestId={`${testId}_cancel-btn`}
        >
          {t(I18N_COMMON_TEXT.CANCEL)}
        </Button>
        <Button
          disabled={(values?.textArea?.trim()?.length || 0) === 0 || !dirty}
          onClick={submitForm}
          dataTestId={`${testId}_submit-btn`}
        >
          {isEdit ? t(I18N_COMMON_TEXT.SAVE) : t(I18N_COMMON_TEXT.SUBMIT)}
        </Button>
      </div>
    </Fragment>
  );
};

export default LongTermMedical;
