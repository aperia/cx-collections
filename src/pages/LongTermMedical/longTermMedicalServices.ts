import { apiService } from 'app/utils/api.service';
import {
  GetLongMedicalRequestBody,
  UpdateLongTermMedicalRequest
} from './types';

export const longTermMedicalService = {
  getLongTermMedical(requestData: GetLongMedicalRequestBody) {
    const url = window.appConfig?.api?.collectionForm.getLongTermMedical || '';
    return apiService.post(url, requestData);
  },

  removeLongTermMedical(requestData: GetLongMedicalRequestBody) {
    const url = window.appConfig?.api?.collectionForm.removeLongTermMedical || '';
    return apiService.post(url, requestData);
  },

  updateLongTermMedical(requestData: UpdateLongTermMedicalRequest) {
    const url =
      window.appConfig?.api?.collectionForm.updateLongTermMedical || '';
    return apiService.post(url, requestData);
  }
};
