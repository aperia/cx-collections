import { storeId } from 'app/test-utils';
import { LongTermMedicalState } from '../types';
import { longTermMedicalActions, reducer } from './reducers';

const { removeStore, onChangeOpenModal } = longTermMedicalActions;

const initialState: LongTermMedicalState = {};

describe('LongTermMedical reducer', () => {
  it('should run removeStore action', () => {
    const params = { storeId };
    const state = reducer(initialState, removeStore(params));
    expect(state[storeId]).toBeUndefined();
  });

  it('should run updateOpenModal action', () => {
    const params = { storeId };
    const state = reducer(initialState, onChangeOpenModal(params));
    expect(state[storeId]).toEqual(
      expect.objectContaining({ isOpenModal: true })
    );
  });
});
