import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import './reducers';
import { applyMiddleware, createStore } from '@reduxjs/toolkit';

import { longTermMedicalService } from '../longTermMedicalServices';
import {
  accEValue,
  mockActionCreator,
  responseDefault,
  storeId
} from 'app/test-utils';
import { rootReducer } from 'storeConfig';

// redux
import thunk from 'redux-thunk';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { triggerUpdateLongTermMedical } from './updateLongTermMedicalData';
import { TRIGGER_TYPE } from '../types';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import './reducers';

jest.mock('pages/CollectionForm/helpers', () => ({
  getDelayProgressRawData: jest.fn,
  dispatchDelayProgress: jest.fn,
  setDelayProgress: jest.fn
}));

const sessionStorageMock = (() => {
  let store: MagicKeyValue = {};
  return {
    getItem: function (key: string) {
      return store[key] || undefined;
    },
    setItem: function (key: string, value: any) {
      store[key] = value ? value.toString() : value + '';
    },
    clear: function () {
      store = {};
    }
  };
})();

Object.defineProperty(window, 'sessionStorage', {
  value: sessionStorageMock
});

const collectionFormActionSpy = mockActionCreator(collectionFormActions);
const apiSpy = mockActionCreator(apiErrorNotificationAction);

const toastSpy = mockActionCreator(actionsToast);

describe('triggerUpdateLongTermMedical', () => {
  const mockArgs = {
    storeId,
    postData: {
      accountId: accEValue,
      common: {
        accountId: accEValue
      }
    },
    type: TRIGGER_TYPE.EDIT
  } as unknown;

  const mockArgsCreate = {
    storeId,
    postData: {
      accountId: accEValue
    },
    type: TRIGGER_TYPE.CREATE
  };

  const store = createStore(
    rootReducer,
    {
      collectionForm: {
        [storeId]: {
          selectedCallResult: {
            code: CALL_RESULT_CODE.BANKRUPTCY,
            description: 'des'
          }
        }
      }
    },
    applyMiddleware(thunk)
  );

  it('triggerUpdateLongTermMedical fulfilled', async () => {
    const setCurrentViewSpy = collectionFormActionSpy('setCurrentView');
    const selectCallResultSpy = collectionFormActionSpy('selectCallResult');
    const addToastSpy = toastSpy('addToast');
    sessionStorage.setItem(
      'collectionInProgressStatus',
      btoa(JSON.stringify({ storeData: { data: '123' } }))
    );

    jest
      .spyOn(longTermMedicalService, 'updateLongTermMedical')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          value: '123'
        }
      });
    await triggerUpdateLongTermMedical(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(addToastSpy).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATED
    });
    expect(setCurrentViewSpy).toBeCalledWith({
      storeId
    });
    expect(selectCallResultSpy).toBeCalledWith({ storeId, item: undefined });
  });

  it('triggerUpdateLongTermMedical fulfilled empty data', async () => {
    const setCurrentViewSpy = collectionFormActionSpy('setCurrentView');
    const selectCallResultSpy = collectionFormActionSpy('selectCallResult');
    const addToastSpy = toastSpy('addToast');
    sessionStorage.setItem(
      'collectionInProgressStatus',
      btoa(JSON.stringify({ storeData: { data: '123' } }))
    );

    jest
      .spyOn(longTermMedicalService, 'updateLongTermMedical')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          value: '123'
        }
      });
    await triggerUpdateLongTermMedical(mockArgsCreate)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(addToastSpy).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
    });
    expect(setCurrentViewSpy).toBeCalledWith({
      storeId
    });
    expect(selectCallResultSpy).toBeCalledWith({ storeId, item: undefined });
  });

  it('triggerUpdateLongTermMedical rejected when empty args', async () => {
    jest
      .spyOn(longTermMedicalService, 'updateLongTermMedical')
      .mockRejectedValue({
        ...responseDefault,
        type: 'longTermMedical/updateLongTermMedical/rejected',
        meta: { requestId: 'id', requestStatus: 'rejected' }
      });

    apiSpy('updateApiError');

    const response = await triggerUpdateLongTermMedical({})(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'longTermMedical/triggerUpdateLongTermMedical/rejected'
    );
    expect(response.payload).toEqual(undefined);
  });

  it('triggerUpdateLongTermMedical rejected when edit', async () => {
    jest
      .spyOn(longTermMedicalService, 'updateLongTermMedical')
      .mockRejectedValue({
        ...responseDefault,
        type: 'longTermMedical/updateLongTermMedical/rejected',
        meta: { requestId: 'id', requestStatus: 'rejected' }
      });

    apiSpy('updateApiError');

    const response = await triggerUpdateLongTermMedical(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'longTermMedical/triggerUpdateLongTermMedical/fulfilled'
    );
    expect(response.payload).toEqual(undefined);
  });

  it('triggerUpdateLongTermMedical rejected when create', async () => {
    jest
      .spyOn(longTermMedicalService, 'updateLongTermMedical')
      .mockRejectedValue({
        ...responseDefault,
        type: 'longTermMedical/updateLongTermMedical/rejected',
        meta: { requestId: 'id', requestStatus: 'rejected' }
      });

    apiSpy('updateApiError');

    const response = await triggerUpdateLongTermMedical(mockArgsCreate)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'longTermMedical/triggerUpdateLongTermMedical/fulfilled'
    );
    expect(response.payload).toEqual(undefined);
  });
});
