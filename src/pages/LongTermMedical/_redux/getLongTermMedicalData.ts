import { orderBy } from 'app/_libraries/_dls/lodash';
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { EMPTY_STRING } from 'app/constants';
import isEqual from 'lodash.isequal';
import isEmpty from 'lodash.isempty';
import { dispatchDestroyDelayProgress } from 'pages/CollectionForm/helpers';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';

// Service
import { longTermMedicalService } from '../longTermMedicalServices';

// Type
import { GetLongTermMedicalArg, LongTermMedicalState } from '../types';

export const getLongTermMedical = createAsyncThunk<
  MagicKeyValue,
  GetLongTermMedicalArg,
  ThunkAPIConfig
>(
  'longTermMedical/getLongTermMedical',
  async (args, { dispatch, getState, rejectWithValue }) => {
    const { accountDetail } = getState();
    const { storeId } = args;

    const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;

    const { commonConfig } = window.appConfig || {};

    const { inProgressInfo = {}, isInProgress } =
      getState().collectionForm[storeId] || {};

    try {
      const requestBody = {
        common: {
          org: commonConfig?.org || EMPTY_STRING,
          app: commonConfig?.app || EMPTY_STRING,
          accountId: eValueAccountId
        },
        callingApplication: commonConfig?.callingApplicationApr01 || '',
        operatorCode: commonConfig?.operatorID || EMPTY_STRING,
        requestType: 'GET_STATUS'
      };
      const { data } = await longTermMedicalService.getLongTermMedical(
        requestBody
      );

      const latestLongTermMedicalDetails = orderBy(
        data?.latestLongTermMedicalDetails?.medicalChangeHistoryList,
        ['actionDateTime'],
        'desc'
      )?.[0];

      const longTermMedicalStatus = data?.latestLongTermMedicalStatus;
      const longTermMedicalDetails =
        latestLongTermMedicalDetails?.medicalDetails;

      if (
        isEqual(inProgressInfo, { longTermMedicalDetails }) &&
        !isEmpty(inProgressInfo) &&
        isInProgress
      ) {
        dispatch(
          collectionFormActions.toggleCallResultInProgressStatus({
            storeId,
            info: {}
          })
        );
        dispatchDestroyDelayProgress(storeId);
      }

      if (longTermMedicalStatus === 'INACTIVE') {
        dispatch(collectionFormActions.changeLastFollowUpData({ storeId }));
      }
      return { longTermMedicalStatus, longTermMedicalDetails };
    } catch (error) {
      return rejectWithValue(error);
    }
  },
  {
    // Condition of redux toolkit prevent call api if this condition return false
    condition: (args, thunkAPI) => {
      const { storeId } = args;
      const loading = thunkAPI?.getState().longTermMedical[storeId]?.isLoading;
      if (loading) return false;
    }
  }
);

export const getLongTermMedicalBuilder = (
  builder: ActionReducerMapBuilder<LongTermMedicalState>
) => {
  builder
    .addCase(getLongTermMedical.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true
      };
    })
    .addCase(getLongTermMedical.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const data = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        data: data
      };
    })
    .addCase(getLongTermMedical.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false
      };
    });
};
