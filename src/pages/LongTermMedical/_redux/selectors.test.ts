// selectors
import * as selectors from './selectors';

import { storeId } from 'app/test-utils/renderComponentWithMockStore';

// helpers
import { selectorWrapper } from 'app/test-utils';

const mockData: Partial<RootState> = {
  longTermMedical: {
    [storeId]: {
      isLoading: true,
      data: { longTermMedicalDetails: 'impactDetail' }
    }
  }
};

const selectorTrigger = selectorWrapper(mockData);

describe('LongTermMedical > selectors', () => {
  it('selectData', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectData);

    expect(data).toEqual(
      mockData.longTermMedical![storeId].data!.longTermMedicalDetails
    );
    expect(emptyData).toBeUndefined();
  });

  it('selectLoading', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectLoading);

    expect(data).toEqual(mockData.longTermMedical![storeId].isLoading);
    expect(emptyData).toBeUndefined();
  });
});
