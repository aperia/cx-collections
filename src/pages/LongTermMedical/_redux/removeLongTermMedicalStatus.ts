import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { EMPTY_STRING } from 'app/constants';
import { isEmpty } from 'lodash';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';

import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// Service
import { longTermMedicalService } from '../longTermMedicalServices';

// Type
import { GetLongTermMedicalArg, LongTermMedicalState } from '../types';
import { longTermMedicalActions } from './reducers';
import { selectCardholderContactName } from './selectors';

export const removeLongTermMedical = createAsyncThunk<
  undefined,
  GetLongTermMedicalArg,
  ThunkAPIConfig
>(
  'longTermMedical/removeLongTermMedical',
  async (args, { dispatch, getState, rejectWithValue }) => {
    const { collectionForm, accountDetail } = getState();
    const { commonConfig } = window.appConfig || {};
    const { storeId } = args;
    const selectedCallResult = collectionForm[storeId].selectedCallResult;
    const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;

    const cardholderContactedName = selectCardholderContactName(
      getState(),
      storeId
    ) as string;

    try {
      const requestBody = {
        common: {
          org: commonConfig?.org || EMPTY_STRING,
          app: commonConfig?.app || EMPTY_STRING,
          accountId: eValueAccountId
        },
        callingApplication: commonConfig?.callingApplicationApr01 || '',
        operatorCode: commonConfig?.operatorID || EMPTY_STRING,
        cardholderContactedName: cardholderContactedName,
        cardholderContactedPhoneNumber:
          accountDetail[storeId]?.numberPhoneAuthenticated
      };

      await longTermMedicalService.removeLongTermMedical(requestBody);

      if (!isEmpty(selectedCallResult)) {
        dispatch(
          collectionFormActions.selectCallResult({ item: undefined, storeId })
        );
      }

      dispatch(
        longTermMedicalActions.getLongTermMedical({
          storeId
        })
      );

      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'txt_long_term_removed'
        })
      );

      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inCollectionFormFlyOut'
        })
      );
    } catch (error) {
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inCollectionFormFlyOut',
          apiResponse: error
        })
      );
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: 'txt_long_term_remove_failed'
        })
      );
      return rejectWithValue(error);
    }
  }
);

export const removeLongTermMedicalBuilder = (
  builder: ActionReducerMapBuilder<LongTermMedicalState>
) => {
  const { pending, fulfilled, rejected } = removeLongTermMedical;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingModal: true
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isOpenModal: false,
        isLoadingModal: false
      };
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingModal: false,
        isOpenModal: false
      };
    });
};
