import { createSelector } from '@reduxjs/toolkit';

const getLongTermMedicalData = (state: RootState, storeId: string) => {
  return state.longTermMedical[storeId];
};

export const selectData = createSelector(
  getLongTermMedicalData,
  longTermMedical => longTermMedical?.data?.longTermMedicalDetails
);

export const selectLoading = createSelector(
  getLongTermMedicalData,
  loading => loading?.isLoading
);

export const selectLoadingModal = createSelector(
  (state: RootState, storeId: string) =>
    state.longTermMedical[storeId]?.isLoadingModal,
  (data?: boolean) => data
);

export const selectOpenModal = createSelector(
  (state: RootState, storeId: string) =>
    state.longTermMedical[storeId]?.isOpenModal,
  (data?: boolean) => data
);

export const selectCardholderContactName = (
  state: RootState,
  storeId: string
) => {
  return state.accountDetail[storeId]?.selectedAuthenticationData
    ?.customerNameForCreateWorkflow;
};
