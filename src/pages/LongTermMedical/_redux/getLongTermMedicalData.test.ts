import { longTermMedicalService } from './../longTermMedicalServices';
import {
  responseDefault,
  storeId
} from './../../../app/test-utils/renderComponentWithMockStore';
import { getLongTermMedical } from './getLongTermMedicalData';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';

describe('getLongTermMedical', () => {
  const store = createStore(rootReducer, {
    collectionForm: {
      [storeId]: {
        inProgressInfo: {
          longTermMedicalDetails: 'value'
        },
        isInProgress: true
      }
    }
  });

  const emptyStore = createStore(rootReducer, {
    collectionForm: {}
  });

  const conditionStore = createStore(rootReducer, {
    collectionForm: {},
    longTermMedical: {
      [storeId]: {
        isLoading: true
      }
    }
  });

  it('should call successful action', async () => {
    const mockData = {
      storeId,
      postData: {
        accountId: 'accountId'
      }
    };

    jest.spyOn(longTermMedicalService, 'getLongTermMedical').mockResolvedValue({
      ...responseDefault,
      data: {
        latestLongTermMedicalDetails: {
          medicalChangeHistoryList: [
            {
              actionDateTime: '2021-08-26T00:10:13.326+0000',
              actionType: 'UPDATE',
              medicalDetails: 'value'
            }
          ]
        },
        latestLongTermMedicalStatus: 'INACTIVE'
      }
    });

    const response = await getLongTermMedical(mockData)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'longTermMedical/getLongTermMedical/fulfilled'
    );
    expect(response.payload).toEqual({
      longTermMedicalDetails: 'value',
      longTermMedicalStatus: 'INACTIVE'
    });
  });

  it('should call successful action, status active', async () => {
    const mockData = {
      storeId,
      postData: {
        accountId: 'accountId'
      }
    };

    jest.spyOn(longTermMedicalService, 'getLongTermMedical').mockResolvedValue({
      ...responseDefault,
      data: {
        latestLongTermMedicalDetails: {
          medicalChangeHistoryList: [
            {
              actionDateTime: '2021-08-26T00:10:13.326+0000',
              actionType: 'UPDATE',
              medicalDetails: 'value'
            }
          ]
        },
        latestLongTermMedicalStatus: 'ACTIVE'
      }
    });

    const response = await getLongTermMedical(mockData)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'longTermMedical/getLongTermMedical/fulfilled'
    );
    expect(response.payload).toEqual({
      longTermMedicalDetails: 'value',
      longTermMedicalStatus: 'ACTIVE'
    });
  });

  it('should call successful action, empty data', async () => {
    const mockData = {
      storeId,
      postData: {
        accountId: 'accountId'
      }
    };

    jest.spyOn(longTermMedicalService, 'getLongTermMedical').mockResolvedValue({
      ...responseDefault,
      data: {
        latestLongTermMedicalDetails: {
          medicalChangeHistoryList: [
            {
              actionDateTime: '2021-08-26T00:10:13.326+0000',
              actionType: 'UPDATE',
              medicalDetails: 'value'
            }
          ]
        },
        latestLongTermMedicalStatus: 'INACTIVE'
      }
    });

    const response = await getLongTermMedical(mockData)(
      emptyStore.dispatch,
      emptyStore.getState,
      {}
    );
    expect(response.type).toEqual(
      'longTermMedical/getLongTermMedical/fulfilled'
    );
    expect(response.payload).toEqual({
      longTermMedicalDetails: 'value',
      longTermMedicalStatus: 'INACTIVE'
    });
  });

  it('should call have condition', async () => {
    const mockData = {
      storeId,
      postData: {
        accountId: 'accountId'
      }
    };

    jest.spyOn(longTermMedicalService, 'getLongTermMedical').mockResolvedValue({
      ...responseDefault,
      data: {
        longTermMedicalDetails: {
          value: 'value'
        },
        longTermMedicalStatus: 'INACTIVE'
      }
    });

    const response = await getLongTermMedical(mockData)(
      conditionStore.dispatch,
      conditionStore.getState,
      {}
    );
    expect(response.type).toEqual(
      'longTermMedical/getLongTermMedical/rejected'
    );
    expect(response.payload).toEqual(undefined);
  });

  it('should call rejected action', async () => {
    const mockData = {
      storeId,
      postData: {
        accountId: ''
      }
    };

    const response = await getLongTermMedical(mockData)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'longTermMedical/getLongTermMedical/rejected'
    );
  });
});
