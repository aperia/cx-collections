import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import {
  CALL_RESULT_CODE,
  COLLECTION_METHOD
} from 'pages/CollectionForm/constants';
import {
  dispatchDelayProgress,
  setDelayProgress
} from 'pages/CollectionForm/helpers';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import { longTermMedicalService } from '../longTermMedicalServices';
import {
  TriggerUpdateLongTermMedicalArg,
  TRIGGER_TYPE,
  LongTermMedicalState
} from '../types';
import { longTermMedicalActions } from './reducers';

export const updateLongTermMedical = createAsyncThunk<
  MagicKeyValue,
  TriggerUpdateLongTermMedicalArg,
  ThunkAPIConfig
>('longTermMedical/updateLongTermMedical', async (args, thunkAPI) => {
  try {
    const { data } = await longTermMedicalService.updateLongTermMedical(
      args.postData
    );

    return data;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerUpdateLongTermMedical = createAsyncThunk<
  unknown,
  TriggerUpdateLongTermMedicalArg,
  ThunkAPIConfig
>('longTermMedical/triggerUpdateLongTermMedical', async (args, thunkAPI) => {
  const { storeId, postData, type } = args;

  const { dispatch } = thunkAPI;

  const { accountDetail } = thunkAPI.getState();

  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;

  const response = await dispatch(
    updateLongTermMedical({
      ...args,
      postData: {
        ...args.postData,
        common: { ...args.postData.common, accountId: eValueAccountId },
        cardholderContactedPhoneNumber:
          accountDetail[storeId]?.numberPhoneAuthenticated
      }
    })
  );

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message:
            type === TRIGGER_TYPE.EDIT
              ? I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATED
              : I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
        })
      );

      dispatch(collectionFormActions.setCurrentView({ storeId }));

      dispatch(
        collectionFormActions.selectCallResult({ storeId, item: undefined })
      );

      dispatch(
        collectionFormActions.changeMethod({
          storeId,
          method: COLLECTION_METHOD.CREATE
        })
      );

      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inCollectionFormFlyOut'
        })
      );

      dispatch(
        collectionFormActions.toggleCallResultInProgressStatus({
          storeId,
          info: { longTermMedicalDetails: postData.medicalDetails },
          lastDelayCallResult: CALL_RESULT_CODE.LONG_TERM_MEDICAL
        })
      );

      dispatchDelayProgress(
        longTermMedicalActions.getLongTermMedical({
          storeId
        }),
        storeId
      );

      setDelayProgress(storeId, CALL_RESULT_CODE.LONG_TERM_MEDICAL, {
        longTermMedicalDetails: postData.medicalDetails
      });
    });
  }

  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message:
            type === TRIGGER_TYPE.EDIT
              ? I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATE_FAILED
              : I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMIT_FAILED
        })
      );

      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inCollectionFormFlyOut',
          apiResponse: response.payload?.response
        })
      );
    });
  }
});

export const triggerUpdateLongTermMedicalBuilder = (
  builder: ActionReducerMapBuilder<LongTermMedicalState>
) => {
  builder
    .addCase(triggerUpdateLongTermMedical.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true
      };
    })
    .addCase(triggerUpdateLongTermMedical.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false
      };
    })
    .addCase(triggerUpdateLongTermMedical.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false
      };
    });
};
