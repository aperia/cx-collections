import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { LongTermMedicalState } from '../types';

import {
  triggerUpdateLongTermMedical,
  triggerUpdateLongTermMedicalBuilder
} from './updateLongTermMedicalData';

import {
  getLongTermMedical,
  getLongTermMedicalBuilder
} from './getLongTermMedicalData';

import {
  removeLongTermMedicalBuilder,
  removeLongTermMedical
} from './removeLongTermMedicalStatus';

const { actions, reducer } = createSlice({
  name: 'longTermMedical',
  initialState: {} as LongTermMedicalState,
  reducers: {
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    onChangeOpenModal: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isOpenModal: !draftState[storeId]?.isOpenModal
      };
    }
  },
  extraReducers: builder => {
    getLongTermMedicalBuilder(builder);
    triggerUpdateLongTermMedicalBuilder(builder);
    removeLongTermMedicalBuilder(builder);
  }
});

const longTermMedicalActions = {
  ...actions,
  removeLongTermMedical,
  getLongTermMedical,
  triggerUpdateLongTermMedical
};

export { longTermMedicalActions, reducer };
