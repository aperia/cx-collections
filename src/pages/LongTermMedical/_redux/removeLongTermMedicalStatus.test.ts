import './reducers';
import { applyMiddleware, createStore } from '@reduxjs/toolkit';
import { removeLongTermMedical } from './removeLongTermMedicalStatus';

import { longTermMedicalService } from '../longTermMedicalServices';
import {
  accEValue,
  mockActionCreator,
  responseDefault,
  storeId
} from 'app/test-utils';
import { rootReducer } from 'storeConfig';

// redux
import thunk from 'redux-thunk';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { longTermMedicalActions } from './reducers';

jest.mock('pages/CollectionForm/helpers', () => ({
  getDelayProgressRawData: () => jest.fn()
}));

const collectionFormActionSpy = mockActionCreator(collectionFormActions);
const longTermMedicalActionSpy = mockActionCreator(longTermMedicalActions);
const apiSpy = mockActionCreator(apiErrorNotificationAction);
const toastSpy = mockActionCreator(actionsToast);

describe('removeLongTermMedical', () => {
  const mockArgs = {
    storeId,
    postData: {
      accountId: accEValue
    }
  };

  const store = createStore(
    rootReducer,
    {
      collectionForm: {
        [storeId]: {
          selectedCallResult: {
            code: CALL_RESULT_CODE.BANKRUPTCY,
            description: 'des'
          }
        }
      }
    },
    applyMiddleware(thunk)
  );

  const storeEmptySelectedCallResult = createStore(
    rootReducer,
    {
      collectionForm: {
        [storeId]: {
          selectedCallResult: undefined
        }
      }
    },
    applyMiddleware(thunk)
  );

  it('updateLongTermMedical fulfilled', async () => {
    const spy = longTermMedicalActionSpy('getLongTermMedical');
    const spyApi = apiSpy('clearApiErrorData');
    const spyToast = toastSpy('addToast');
    jest
      .spyOn(longTermMedicalService, 'removeLongTermMedical')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          value: '123'
        }
      });
    const response = await removeLongTermMedical(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'longTermMedical/removeLongTermMedical/fulfilled'
    );
    expect(spy).toBeCalledWith({
      storeId
    });
    expect(spyApi).toBeCalledWith({
      storeId,
      forSection: 'inCollectionFormFlyOut'
    });
    expect(spyToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: 'txt_long_term_removed'
    });
  });

  it('updateLongTermMedical rejected', async () => {
    const spyApi = apiSpy('updateApiError');
    const spyToast = toastSpy('addToast');
    jest
      .spyOn(longTermMedicalService, 'removeLongTermMedical')
      .mockRejectedValue({
        ...responseDefault,
        data: null
      });

    const response = await removeLongTermMedical(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'longTermMedical/removeLongTermMedical/rejected'
    );
    expect(spyApi).toBeCalledWith({
      storeId,
      forSection: 'inCollectionFormFlyOut',
      apiResponse: {
        config: {},
        data: null,
        headers: {},
        status: 200,
        statusText: ''
      }
    });
    expect(spyToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: 'txt_long_term_remove_failed'
    });
  });

  it('selectedCallResult should not be called', async () => {
    const spy = collectionFormActionSpy('selectCallResult');
    jest
      .spyOn(longTermMedicalService, 'removeLongTermMedical')
      .mockResolvedValue(responseDefault);
    await removeLongTermMedical(mockArgs)(
      storeEmptySelectedCallResult.dispatch,
      storeEmptySelectedCallResult.getState,
      {}
    );
    expect(spy).not.toBeCalledWith({ item: undefined, storeId });
  });
});
