import { longTermMedicalService } from './longTermMedicalServices';

// utils
import { mockApiServices, storeId } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import {
  GetLongMedicalRequestBody,
  UpdateLongTermMedicalRequest
} from './types';

describe('longTermMedicalService', () => {
  describe('getLongTermMedical', () => {
    const params: GetLongMedicalRequestBody = {
      common: { accountId: storeId },
      callingApplication: 'app',
      operatorCode: 'code'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      longTermMedicalService.getLongTermMedical(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      longTermMedicalService.getLongTermMedical(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.getLongTermMedical,
        params
      );
    });
  });

  describe('updateLongTermMedical', () => {
    const params: UpdateLongTermMedicalRequest = {
      common: { accountId: storeId },
      medicalDetails: 'medicalDetails',
      callingApplication: '',
      operatorCode: '',
      cardholderContactedName: ''
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      longTermMedicalService.updateLongTermMedical(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      longTermMedicalService.updateLongTermMedical(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.updateLongTermMedical,
        params
      );
    });
  });

  describe('removeLongTermMedical', () => {
    const params: GetLongMedicalRequestBody = {
      callingApplication: '123',
      operatorCode: '123'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      longTermMedicalService.removeLongTermMedical(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      longTermMedicalService.removeLongTermMedical(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.removeLongTermMedical,
        params
      );
    });
  });
});
