import React from 'react';

// redux
import { useDispatch } from 'react-redux';

// actions
import { tabActions } from 'pages/__commons/TabBar/_redux';

// Componenet
import CoreView from 'pages/AccountSearch/Home/Dashboard/Collector/CoreView';

// constants
import { EMPTY_STRING } from 'app/constants';
import { QUEUE_LIST } from 'pages/AccountDetails/NextActions/constants';

// type
import { CollectorDetail } from 'pages/AccountSearch/Home/Dashboard/types';

export interface CoreViewListProps {
  data?: CollectorDetail[];
}

const CoreViewList: React.FC<CoreViewListProps> = ({ data }) => {
  const dispatch = useDispatch();

  const handleOpenWorkQueue = (dataItem?: CollectorDetail) => {
    const collectorId = dataItem?.collectorId || EMPTY_STRING;

    if (!collectorId) return;

    const tabTitle = QUEUE_LIST;
    const eValue = `${collectorId}`;

    dispatch(
      tabActions.addTab({
        title: tabTitle,
        storeId: 'workQueueCollector',
        accEValue: eValue,
        tabType: 'workQueueCore',
        iconName: 'file',
        props: {
          collectorId
        }
      })
    );
  };

  return (
    <CoreView data={data} title="" handleOpenWorkQueue={handleOpenWorkQueue} />
  );
};

export default CoreViewList;
