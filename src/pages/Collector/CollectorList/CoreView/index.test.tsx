import React from 'react';

//test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

//components
import CoreView from '.';

//actions
import { tabActions } from 'pages/__commons/TabBar/_redux';

//constants
import {
  I18N_DASH_BOARD,
  USER_ROLE
} from 'pages/AccountSearch/Home/Dashboard/constants';
import { CollectorDetail } from 'pages/AccountSearch/Home/Dashboard/types';

const initialState: Partial<RootState> = {
  userRole: { role: USER_ROLE.ADMIN1 }
};

const spyTab = mockActionCreator(tabActions);

const data: CollectorDetail[] = [
  { collectorId: 'collector_id_1', workQueue: 34543 }
];

describe('Render', () => {
  it('Should render', () => {
    renderMockStore(<CoreView data={data} />, { initialState });

    expect(
      screen.getByText(new RegExp(I18N_DASH_BOARD.WORK_QUEUE))
    ).toBeInTheDocument();
  });

  it('Should render with empty state', () => {
    renderMockStore(<CoreView data={data} />, { initialState: {} });

    expect(
      screen.getByText(new RegExp(I18N_DASH_BOARD.WORK_QUEUE))
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  describe('handleOpenWorkQueue', () => {
    it('when data has collectorId', () => {
      const mockAction = spyTab('addTab');

      renderMockStore(<CoreView data={data} />, {
        initialState
      });

      userEvent.click(screen.getByText(new RegExp(I18N_DASH_BOARD.WORK_QUEUE)));

      expect(mockAction).toBeCalledWith({
        accEValue: 'collector_id_1',
        iconName: 'file',
        props: {
          collectorId: 'collector_id_1'
        },
        storeId: 'workQueueCollector',
        tabType: 'workQueueCore',
        title: 'txt_queue_list'
      });
    });

    it('when data do not has collectorId', () => {
      const mockAction = spyTab('addTab');

      renderMockStore(
        <CoreView data={[{ accountWorked: 5, workQueue: 345431 }]} />,
        {
          initialState
        }
      );

      userEvent.click(screen.getByText(new RegExp(I18N_DASH_BOARD.WORK_QUEUE)));

      expect(mockAction).not.toBeCalled();
    });
  });
});
