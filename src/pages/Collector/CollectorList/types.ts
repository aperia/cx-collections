import { CollectorDetail } from 'pages/AccountSearch/Home/Dashboard/types';

export interface CollectorListState {
  data?: CollectorDetail[];
  loading?: boolean;
  error?: string;
  selectedPeriod?: RefDataValue;
}

export interface GetCollectorListPayload {
  data?: CollectorDetail[];
}

export interface GeCollectorListArgs {
  period: string;
}
