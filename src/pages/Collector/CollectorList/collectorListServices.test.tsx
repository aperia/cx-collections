// services
import { collectorListServices } from './collectorListServices';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

describe('collectorListServices', () => {
  describe('getCodeToTextDropdownList', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      collectorListServices.getCollectorList();

      expect(mockService).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      collectorListServices.getCollectorList();

      expect(mockService).toBeCalledWith(apiUrl.collector.getCollectorList);
    });
  });
});
