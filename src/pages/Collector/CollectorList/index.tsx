import React, { Fragment, useEffect } from 'react';

//libs
import classnames from 'classnames';

// redux
import { useDispatch, useSelector } from 'react-redux';

// actions
import { collectorListActions as actions } from './_redux/reducers';

// selector
import {
  selectedCollectorList,
  selectedCollectorListError,
  selectedCollectorListLoading,
  selectSelectedPeriod
} from './_redux/selectors';

// component
import CoreView from './CoreView';
import CorePlusView from './CorePlusView';
import DashboardDropdownList from 'pages/AccountSearch/Home/Dashboard/common/DashboardDropdownList';
import { SimpleBar, Pagination } from 'app/_libraries/_dls/components';
import { FailedApiReload, NoDataGrid } from 'app/components';

// constants

import {
  DASHBOARD_COLLECTOR,
  I18N_DASH_BOARD
} from 'pages/AccountSearch/Home/Dashboard/constants';

//language
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { usePagination } from 'app/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface CollectorListProps {}

const CollectorList: React.FC<CollectorListProps> = () => {
  const dispatch = useDispatch();

  const { t } = useTranslation();

  const { isCollector, isAdminRole } = window.appConfig.commonConfig;
  const isCoreView = isCollector || isAdminRole;

  const isLoading = useSelector(selectedCollectorListLoading);

  const isError = useSelector(selectedCollectorListError);

  const data = useSelector(selectedCollectorList);

  const period: RefDataValue = useSelector(selectSelectedPeriod);

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(data);

  const showPagination = total > pageSize[0];

  const handleReload = () => {
    dispatch(actions.getCollectorList({ period: period.value }));
  };

  const handleBlur = React.useCallback(
    event => {
      dispatch(actions.setSelectedPeriod({ value: event.value }));
    },
    [dispatch]
  );

  const testId = 'collectorList';

  useEffect(() => {
    dispatch(actions.getCollectorList({ period: period.value }));
  }, [dispatch, period]);

  if (isError) {
    return (
      <div>
        <FailedApiReload
          id="dashboard-work-queue-failed"
          className="mt-80"
          onReload={handleReload}
        />
      </div>
    );
  }

  const renderLayout = () => {
    if (!gridData.length && !isLoading) {
      return (
        <NoDataGrid text={t('txt_no_collector_data')} dataTestId={testId} />
      );
    }
    return (
      <Fragment>
        {isCoreView ? (
          <CoreView data={gridData} />
        ) : (
          <CorePlusView data={gridData} />
        )}
        {showPagination && (
          <div className="pt-16">
            <Pagination
              totalItem={total}
              pageSize={pageSize}
              pageNumber={currentPage}
              pageSizeValue={currentPageSize}
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
              dataTestId="collectorList-pagination"
            />
          </div>
        )}
      </Fragment>
    );
  };

  return (
    <div className={classnames('h-100 bg-light-l20', { loading: isLoading })}>
      <SimpleBar>
        <div className="p-24">
          <div className="dash-board-content">
            <div className="d-flex align-items-center justify-content-between">
              <h3 data-testid={genAmtId('collectorList', 'title', '')}>
                {t(DASHBOARD_COLLECTOR.COLLECTOR_LIST)}
              </h3>
              <div className="mr-n8">
                <span
                  className="color-grey fw-500 mr-4"
                  data-testid={genAmtId('collectorList', 'period', '')}
                >
                  {t(I18N_DASH_BOARD.PERIOD)}
                </span>
                <DashboardDropdownList
                  onBlur={handleBlur}
                  dataTestId="collectorList-dropdown"
                />
              </div>
            </div>
            <div className="mt-16">{renderLayout()}</div>
          </div>
        </div>
      </SimpleBar>
    </div>
  );
};

export default CollectorList;
