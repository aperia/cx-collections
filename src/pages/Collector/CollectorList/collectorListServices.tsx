import { apiService } from 'app/utils/api.service';

export const collectorListServices = {
  getCollectorList() {
    const url = window.appConfig?.api?.collector?.getCollectorList || '';
    return apiService.get(url);
  }
};
