import { createSelector } from '@reduxjs/toolkit';
import { DASHBOARD_PERIOD } from 'pages/AccountSearch/Home/Dashboard/constants';

const getCollectorList = (states: RootState) => states.collectorList;

const getSelectedPeriod = (states: RootState) => {
  return states.collectorList?.selectedPeriod || DASHBOARD_PERIOD[0];
};

export const selectSelectedPeriod = createSelector(
  getSelectedPeriod,
  (data: RefDataValue) => data
);

export const selectedCollectorListLoading = createSelector(
  getCollectorList,
  data => data?.loading
);

export const selectedCollectorListError = createSelector(
  getCollectorList,
  data => data?.error
);

export const selectedCollectorList = createSelector(
  getCollectorList,
  data => data?.data || []
);
