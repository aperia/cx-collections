import {
  collectorListActions as actions,
  collectorList as reducer
} from './reducers';

import { CollectorListState } from '../types';

const state: CollectorListState = {};

describe('Payment > reducers', () => {
  it('setSelectedPeriod', () => {
    const result = reducer(state, actions.setSelectedPeriod({ value: '10' }));

    expect(result.selectedPeriod).toEqual('10');
  });
});
