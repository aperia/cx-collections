import './reducers';

import { responseDefault } from 'app/test-utils';
import { getCollectorList } from './getCollectorList';

import { collectorListServices } from '../collectorListServices';

import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { GeCollectorListArgs } from '../types';

const mockArgs: GeCollectorListArgs = {
  period: 'periodId'
};

describe('ClientConfiguration > addClientConfiguration', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {});
  });

  it('success', async () => {
    jest.spyOn(collectorListServices, 'getCollectorList').mockResolvedValue({
      ...responseDefault,
      data: { CXCOL1: { periodId: 'periodId' } }
    });

    const response = await getCollectorList(mockArgs)(
      store.dispatch,
      store.getState,
      undefined
    );
    expect(response.payload).toEqual({ data: mockArgs.period });
  });

  it('fail', async () => {
    jest.spyOn(collectorListServices, 'getCollectorList').mockRejectedValue({
      errorMessage: 'errorMessage'
    });

    const response = await getCollectorList(mockArgs)(
      store.dispatch,
      store.getState,
      undefined
    );
    expect(response.meta.requestStatus).toEqual('rejected');
  });
});
