import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// types
import { CollectorListState } from '../types';

// thunks
import { getCollectorList, getCollectorListBuilder } from './getCollectorList';

const { actions, reducer } = createSlice({
  name: 'collectorList',
  initialState: {
    data: undefined,
    loading: false,
    error: ''
  } as CollectorListState,
  reducers: {
    setSelectedPeriod: (
      draftState,
      action: PayloadAction<{ value: RefDataValue }>
    ) => {
      const { value } = action.payload;

      draftState.selectedPeriod = value;
    }
  },
  extraReducers: builder => {
    getCollectorListBuilder(builder);
  }
});

const collectorListActions = {
  ...actions,
  getCollectorList
};

export { collectorListActions, reducer as collectorList };
