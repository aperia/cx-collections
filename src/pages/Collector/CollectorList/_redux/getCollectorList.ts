import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Type
import {
  GetCollectorListPayload,
  GeCollectorListArgs,
  CollectorListState
} from '../types';

// Service
import { collectorListServices } from './../collectorListServices';

export const getCollectorList = createAsyncThunk<
  GetCollectorListPayload,
  GeCollectorListArgs,
  ThunkAPIConfig
>('collector/getCollectorList', async (args, thunkAPI) => {
  const { period } = args;
  const { data } = await collectorListServices.getCollectorList();
  return { data: data['CXCOL1'][period] };
});

export const getCollectorListBuilder = (
  builder: ActionReducerMapBuilder<CollectorListState>
) => {
  builder.addCase(getCollectorList.pending, (draftState, action) => {
    draftState.loading = true;
  });
  builder.addCase(getCollectorList.fulfilled, (draftState, action) => {
    const payload = action.payload;

    draftState.data = payload?.data;
    draftState.loading = false;
  });
  builder.addCase(getCollectorList.rejected, (draftState, action) => {
    draftState.loading = false;
    draftState.error = action.payload?.errorMessage;
  });
};
