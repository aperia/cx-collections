import { selectorWrapper } from 'app/test-utils';

import * as selector from './selectors';

import { DASHBOARD_DROPDOWN_LIST } from 'pages/AccountSearch/Home/Dashboard/constants';

const states: Partial<RootState> = {
  collectorList: {
    selectedPeriod: 'selectedPeriod' as any,
    loading: true,
    error: 'error',
    data: [{ collectorId: 'collectorId_1' }, { collectorId: 'collectorId_2' }]
  }
};

const selectorTrigger = selectorWrapper(states);

describe('CollectorList selectors', () => {
  it('getSelectedPeriod', () => {
    const { data, emptyData } = selectorTrigger(selector.selectSelectedPeriod);
    expect(data).toEqual(states.collectorList!.selectedPeriod);
    expect(emptyData?.value).toEqual(DASHBOARD_DROPDOWN_LIST[0].toLowerCase());
  });

  it('selectedCollectorListLoading', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectedCollectorListLoading
    );
    expect(data).toEqual(states.collectorList!.loading);
    expect(emptyData).toEqual(false);
  });

  it('selectedCollectorListError', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectedCollectorListError
    );
    expect(data).toEqual(states.collectorList!.error);
    expect(emptyData).toEqual('');
  });

  it('selectedCollectorList', () => {
    const { data, emptyData } = selectorTrigger(selector.selectedCollectorList);
    expect(data).toEqual([
      { collectorId: 'collectorId_1' },
      { collectorId: 'collectorId_2' }
    ]);
    expect(emptyData).toEqual([]);
  });
});
