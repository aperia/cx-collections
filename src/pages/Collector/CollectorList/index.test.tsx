import React from 'react';

//test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { fireEvent, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

//components
import CollectorList from '.';

//actions
import { collectorListActions } from './_redux/reducers';

//constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { I18N_DASH_BOARD } from 'pages/AccountSearch/Home/Dashboard/constants';
import { PaginationWrapperProps } from 'app/_libraries/_dls/components';

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');

  return {
    ...dlsComponents,
    Pagination: (props: PaginationWrapperProps) => {
      const {
        onChangePage = () => undefined,
        onChangePageSize = () => undefined
      } = props;

      return (
        <div>
          <input
            data-testid="Pagination_onChangePage"
            onChange={(e: any) => onChangePage(e.target.value)}
          />
          <input
            data-testid="Pagination_onChangePageSize"
            onChange={(e: any) => onChangePageSize(e.target)}
          />
        </div>
      );
    }
  };
});

jest.mock('pages/Collector/CollectorList/CoreView', () => () => {
  return <div>Core View</div>;
});

jest.mock('pages/Collector/CollectorList/CorePlusView', () => () => {
  return <div>Core View Plus</div>;
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const initialState: Partial<RootState> = {
  collectorList: {
    data: [
      { collectorId: 'collector_id_1' },
      { collectorId: 'collector_id_2' },
      { collectorId: 'collector_id_3' },
      { collectorId: 'collector_id_4' },
      { collectorId: 'collector_id_5' },
      { collectorId: 'collector_id_6' },
      { collectorId: 'collector_id_7' },
      { collectorId: 'collector_id_8' },
      { collectorId: 'collector_id_9' },
      { collectorId: 'collector_id_10' },
      { collectorId: 'collector_id_11' }
    ]
  }
};

const mockCollectorList = mockActionCreator(collectorListActions);

describe('Render', () => {
  it('Should render CoreView with isCollector', () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: { commonConfig: { isCollector: true, org: 'org', app: 'app' } }
    });

    renderMockStore(<CollectorList />, { initialState });

    expect(screen.getByText('Core View')).toBeInTheDocument();
  });

  it('Should render CoreView with isAdminRole', () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: { commonConfig: { isAdminRole: true, org: 'org', app: 'app' } }
    });

    renderMockStore(<CollectorList />, { initialState });

    expect(screen.getByText('Core View')).toBeInTheDocument();
  });

  it('Should render CoreViewPlus', () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: { commonConfig: { org: 'org', app: 'app' } }
    });

    renderMockStore(<CollectorList />, { initialState });

    expect(screen.getByText('Core View Plus')).toBeInTheDocument();
  });

  it('Should render with empty state', () => {
    renderMockStore(<CollectorList />, { initialState: {} });

    expect(screen.getByText(I18N_DASH_BOARD.PERIOD)).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleReload', () => {
    const mockAction = mockCollectorList('getCollectorList');

    renderMockStore(<CollectorList />, {
      initialState: {
        collectorList: { error: 'error' }
      }
    });

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.RELOAD));

    expect(mockAction).toBeCalledWith({ period: 'today' });
  });

  it('handleChangePage', () => {
    // render
    renderMockStore(<CollectorList />, { initialState });

    // simulate
    const onChangePage = fireEvent.change(
      screen.getByTestId('Pagination_onChangePage'),
      {
        target: { value: 5 }
      }
    );

    expect(onChangePage).toBeTruthy();
  });

  it('handleChangePageSize', () => {
    // render
    renderMockStore(<CollectorList />, { initialState });

    // simulate
    const onChangePageSize = fireEvent.change(
      screen.getByTestId('Pagination_onChangePageSize'),
      {
        target: { value: 'undefined', size: 50 }
      }
    );

    expect(onChangePageSize).toBeTruthy();
  });

  it('handleBlur', () => {
    // mock
    const mockAction = mockCollectorList('setSelectedPeriod');

    // render
    const { container } = renderMockStore(<CollectorList />, { initialState });

    // simulate
    userEvent.click(container.querySelector('.text-field-container')!);
    userEvent.click(document.body);

    // assert
    expect(mockAction).toBeCalledWith({
      value: { value: 'today', description: I18N_COMMON_TEXT.TODAY }
    });
  });

  it('should render with empty data ', () => {
    const initialState: Partial<RootState> = {
      collectorList: {
        data: []
      }
    };
    renderMockStore(<CollectorList />, {
      initialState
    });

    expect(screen.queryByText('txt_no_collector_data')).toBeNull();
  });
});
