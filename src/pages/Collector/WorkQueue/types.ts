import {
  CollectorPromises,
  OwnerModel
} from 'pages/AccountSearch/Home/Dashboard/types';

export interface WorkQueueProps {
  collectorId: string;
  owner: OwnerModel;
  collector: OwnerModel[];
  workQueue?: WorkQueueDetailProps[];
}

export interface WorkQueueDetailProps {
  queueId: string;
  queueName: string;
  description: string;
  primaryName: string[];
  accountWorked?: number;
  totalAccountWorked?: number;
  amountCollected?: number;
  totalAmountCollected?: number;
  promises?: CollectorPromises[];
}

export interface WorkQueueListState {
  workQueue: {
    data?: WorkQueueProps;
    loading?: boolean;
    error?: string;
    page?: number;
    pageSize?: number;
    collectorValue?: string;
  };
  userList: {
    data?: OwnerModel[];
    loading?: boolean;
    error?: string;
    page?: number;
    pageSize?: number;
  };
}

export interface GetWorkQueueListPayload {
  data?: WorkQueueProps;
}

export interface GetWorkQueueListArgs {
  collectorId: string;
  userName: string;
}

export interface GetUserListPayload {
  data?: OwnerModel[];
}

export interface GetUserListArgs {
  collectorId: string;
}
