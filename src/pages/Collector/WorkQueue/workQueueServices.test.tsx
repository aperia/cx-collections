// services
import { workQueueServices } from './workQueueServices';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

describe('workQueueServices', () => {
  describe('getWorkQueueList', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      workQueueServices.getWorkQueueList();

      expect(mockService).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      workQueueServices.getWorkQueueList();

      expect(mockService).toBeCalledWith(
        apiUrl.collector.getCollectorWorkQueue
      );
    });
  });

  describe('getUserList', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      workQueueServices.getUserList();

      expect(mockService).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      workQueueServices.getUserList();

      expect(mockService).toBeCalledWith(apiUrl.collector.getUserList);
    });
  });
});
