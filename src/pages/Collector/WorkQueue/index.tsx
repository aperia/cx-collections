import { OwnerModel } from 'pages/AccountSearch/Home/Dashboard/types';
import React from 'react';

// componenet
import WorkQueueItem from './WorkQueueItem';

export interface WorkQueueCollectorProps {
  collectorId: string;
  selectedOwner?: OwnerModel;
}

const WorkQueueCollector: React.FC<WorkQueueCollectorProps> = ({
  collectorId,
  selectedOwner
}) => {
  return (
    <React.Fragment>
      <WorkQueueItem collectorId={collectorId} selectedOwner={selectedOwner} />
    </React.Fragment>
  );
};

export default WorkQueueCollector;
