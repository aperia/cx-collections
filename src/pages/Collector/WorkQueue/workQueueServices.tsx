import { apiService } from 'app/utils/api.service';

export const workQueueServices = {
  getWorkQueueList() {
    const url = window.appConfig?.api?.collector?.getCollectorWorkQueue || '';
    return apiService.get(url);
  },
  getUserList() {
    const url = window.appConfig?.api?.collector?.getUserList || '';
    return apiService.get(url);
  }
};
