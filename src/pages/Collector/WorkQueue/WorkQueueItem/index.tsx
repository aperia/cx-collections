import React, { useCallback, useEffect } from 'react';

//libs
import classnames from 'classnames';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';

// selectors
import { workQueueListActions as actions } from '../_redux/reducers';
import {
  selectedWorkQueueList,
  selectedWorkQueueDetail,
  selectedTotalItem,
  selectedCollectorList,
  selectedWorkQueueListError,
  selectedWorkQueueListLoading
} from '../_redux/selectors';

// types
import { WorkQueueDetailProps } from '../types';
import { OwnerModel } from 'pages/AccountSearch/Home/Dashboard/types';
import { DropdownBaseChangeEvent } from 'app/_libraries/_dls/components/DropdownBase';

// const
import { DASHBOARD_COLLECTOR } from 'pages/AccountSearch/Home/Dashboard/constants';
import { QUEUE_LIST } from 'pages/AccountDetails/NextActions/constants';

// language
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// Component
import {
  DropdownList,
  Pagination,
  SimpleBar,
  Button
} from 'app/_libraries/_dls/components';
import Detail from 'pages/AccountSearch/Home/Dashboard/Collector/CoreView/Detail';
import { FailedApiReload, NoDataGrid } from 'app/components';

// hooks
import { usePagination } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface WorkQueueItemProps {
  collectorId: string;
  selectedOwner?: OwnerModel;
}

const WorkQueueItem: React.FC<WorkQueueItemProps> = ({
  collectorId,
  selectedOwner
}) => {
  const dispatch = useDispatch();

  const [collectorValue, setCollectorValue] = React.useState<OwnerModel>();

  const { t } = useTranslation();

  const { isCollector, isAdminRole } = window.appConfig.commonConfig || {};
  const isCoreView = isCollector || isAdminRole;

  const isLoading = useSelector(selectedWorkQueueListLoading);

  const isError = useSelector(selectedWorkQueueListError);

  const dataDetail = useSelector(selectedWorkQueueDetail);

  const data = useSelector(selectedWorkQueueList);

  const collectors = useSelector(selectedCollectorList);

  const totalItem = useSelector(selectedTotalItem);

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(dataDetail);

  const handleLoadData = useCallback(
    (userName: string) => {
      batch(() => {
        dispatch(
          actions.getWorkQueueList({
            collectorId,
            userName
          })
        );
        dispatch(actions.getUserList({ collectorId }));
      });
    },
    [collectorId, dispatch]
  );

  useEffect(() => {
    const owner =
      collectors?.find(item => item.userName === data?.owner?.userName) ||
      selectedOwner;
    setCollectorValue(owner);
    dispatch(
      actions.selectedCollectorValue(
        (owner?.userName !== 'All' ? owner?.userName : '') || ''
      )
    );
  }, [collectors, data, dispatch, selectedOwner]);

  useEffect(() => {
    handleLoadData('');
  }, [handleLoadData]);

  const handleChange = (event: DropdownBaseChangeEvent) => {
    dispatch(
      actions.selectedCollectorValue(
        (event.value?.userName !== 'All' ? event.value?.userName : '') || ''
      )
    );
    setCollectorValue(event.value);
  };

  const renderWorkQueueList = (
    dataItem?: WorkQueueDetailProps
  ): JSX.Element => {
    return (
      <div className="col-xl-6 col-md-12 mt-16">
        <div className="bg-white rounded-lg border br-light-l04 px-24 py-18">
          <div
            className={`d-flex align-items-center${
              !isCoreView ? ' mb-12' : ''
            }`}
          >
            <h5 className="flex-1 pr-1">
              {dataItem?.queueName} - {dataItem?.description}
            </h5>
            <div className="ml-auto mr-n8">
              <Button size="sm" variant="outline-primary">
                {t(I18N_COMMON_TEXT.VIEW_DETAILS)}
              </Button>
            </div>
          </div>
          {!isCoreView && <Detail dataItem={dataItem} />}
        </div>
      </div>
    );
  };

  const dropdownItem = useCallback(
    (item: OwnerModel, index: number): JSX.Element => {
      return (
        <DropdownList.Item key={index} label={item?.fullName} value={item} />
      );
    },
    []
  );

  if (isError) {
    return (
      <div>
        <FailedApiReload
          id="dashboard-work-queue-failed"
          className="mt-80"
          onReload={() =>
            handleLoadData(
              (collectorValue?.userName !== 'All'
                ? collectorValue?.userName
                : '') || ''
            )
          }
        />
      </div>
    );
  }

  return (
    <div className={classnames('bg-light-l20 h-100', { loading: isLoading })}>
      <SimpleBar>
        <div className="p-24">
          <div className="dash-board-content">
            <h3>{t(QUEUE_LIST)}</h3>
            <div className="mt-16">
              <span className="color-grey fw-500 mr-4">
                {t(DASHBOARD_COLLECTOR.COLLECTOR)}:
              </span>
              {collectors?.length && (
                <DropdownList
                  name="queueListCollector"
                  variant="no-border"
                  value={collectorValue}
                  textField="fullName"
                  onChange={handleChange}
                  noResult={t('txt_no_results_found')}
                >
                  {collectors.map((item: OwnerModel, index: number) => {
                    return index === 0 ? (
                      <DropdownList.Group key={index}>
                        {dropdownItem(item, index)}
                      </DropdownList.Group>
                    ) : (
                      dropdownItem(item, index)
                    );
                  })}
                </DropdownList>
              )}
            </div>
            <div>
              <div className="row">
                {dataDetail?.length ? (
                  (gridData || [])?.map(
                    (item: WorkQueueDetailProps, index: number) => (
                      <React.Fragment key={index}>
                        {renderWorkQueueList(item)}
                      </React.Fragment>
                    )
                  )
                ) : (
                  <div className="col-12">
                    <NoDataGrid text={t('txt_no_queue_data')} />
                  </div>
                )}
              </div>
              {totalItem > 10 && (
                <div className="pt-16">
                  <Pagination
                    totalItem={total}
                    pageSize={pageSize}
                    pageNumber={currentPage}
                    pageSizeValue={currentPageSize}
                    compact
                    onChangePage={onPageChange}
                    onChangePageSize={onPageSizeChange}
                  />
                </div>
              )}
            </div>
          </div>
        </div>
      </SimpleBar>
    </div>
  );
};

export default WorkQueueItem;
