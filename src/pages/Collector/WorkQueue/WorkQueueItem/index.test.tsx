import React from 'react';
import { mockActionCreator, renderMockStore, storeId } from 'app/test-utils';

import WorkQueueItem from '.';

import { fireEvent, screen } from '@testing-library/react';

import { workQueueListActions } from '../_redux/reducers';

import * as usePagination from 'app/hooks/usePagination';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import userEvent from '@testing-library/user-event';
import { QUEUE_LIST } from 'pages/AccountDetails/NextActions/constants';
import { WorkQueueListState } from '../types';
import { act } from 'react-dom/test-utils';
import { DropdownListProps } from 'app/_libraries/_dls/components';
import {
  DropdownBaseChangeEvent,
  DropdownBaseGroupProps
} from 'app/_libraries/_dls/components/DropdownBase/types';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls/components/DropdownList', () => {
  jest.requireActual('app/_libraries/_dls/components/DropdownList');

  const DropdownList = ({ onChange }: DropdownListProps) => {
    return (
      <input
        data-testid="DropdownList_onChange"
        onChange={e =>
          onChange!({
            value: { userName: e.target.value }
          } as unknown as DropdownBaseChangeEvent)
        }
      />
    );
  };

  DropdownList.Group = ({ children }: DropdownBaseGroupProps) => (
    <div>{children}</div>
  );
  DropdownList.Item = () => <div />;

  return {
    __esModule: true,
    default: DropdownList
  };
});

const workQueueList: WorkQueueListState = {
  workQueue: {
    error: '',
    loading: false,
    data: {
      collectorId: 'collectorId',
      owner: { userId: 'owner_id' },
      workQueue: [
        {
          queueId: 'queue_id_1',
          description: 'description 1',
          queueName: 'queue name 1',
          primaryName: ['primary name 1']
        },
        {
          queueId: 'queue_id_2',
          description: 'description 2',
          queueName: 'queue name 2',
          primaryName: ['primary name 2']
        },
        {
          queueId: 'queue_id_3',
          description: 'description 3',
          queueName: 'queue name 3',
          primaryName: ['primary name 3']
        },
        {
          queueId: 'queue_id_4',
          description: 'description 4',
          queueName: 'queue name 4',
          primaryName: ['primary name 4']
        },
        {
          queueId: 'queue_id_5',
          description: 'description 5',
          queueName: 'queue name 5',
          primaryName: ['primary name 5']
        },
        {
          queueId: 'queue_id_6',
          description: 'description 6',
          queueName: 'queue name 6',
          primaryName: ['primary name 6']
        },
        {
          queueId: 'queue_id_7',
          description: 'description 7',
          queueName: 'queue name 7',
          primaryName: ['primary name 7']
        },
        {
          queueId: 'queue_id_8',
          description: 'description 8',
          queueName: 'queue name 8',
          primaryName: ['primary name 8']
        },
        {
          queueId: 'queue_id_9',
          description: 'description 9',
          queueName: 'queue name 9',
          primaryName: ['primary name 9']
        },
        {
          queueId: 'queue_id_10',
          description: 'description 10',
          queueName: 'queue name 10',
          primaryName: ['primary name 10']
        },
        {
          queueId: 'queue_id_11',
          description: 'description 11',
          queueName: 'queue name 11',
          primaryName: ['primary name 11']
        }
      ],
      collector: [
        {
          userId: 'collector_id_1',
          fullName: 'collector fullName 1',
          userName: 'collector userName 1'
        },
        {
          userId: 'collector_id_2',
          fullName: 'collector fullName 2',
          userName: 'collector userName 2'
        }
      ]
    }
  },
  userList: {
    data: [
      { userId: 'user_1', fullName: 'fullName 1', userName: 'All' },
      { userId: 'user_2', fullName: 'fullName 2', userName: 'userName 2' },
      { userId: 'user_3', fullName: 'fullName 3', userName: 'userName 3' },
      { userId: 'user_4', fullName: 'fullName 4', userName: 'userName 4' },
      { userId: 'user_5', fullName: 'fullName 5', userName: 'userName 5' },
      { userId: 'user_6', fullName: 'fullName 6', userName: 'userName 6' },
      { userId: 'user_7', fullName: 'fullName 7', userName: 'userName 7' },
      { userId: 'user_8', fullName: 'fullName 8', userName: 'userName 8' },
      { userId: 'user_9', fullName: 'fullName 9', userName: 'userName 9' },
      { userId: 'user_10', fullName: 'fullName 10', userName: 'userName 10' }
    ]
  }
};

const initialState: Partial<RootState> = {
  workQueueList
};

const spyWorkQueueList = mockActionCreator(workQueueListActions);

beforeEach(() => {
  spyWorkQueueList('getWorkQueueList');
  spyWorkQueueList('selectedCollectorValue');

  jest.spyOn(usePagination, 'usePagination').mockImplementation(
    data =>
      ({
        total: 200,
        currentPage: 1,
        pageSize: [10, 25, 50],
        currentPageSize: 10,
        gridData: data,
        onPageChange: () => {},
        onPageSizeChange: () => {}
      } as any)
  );
});

describe('Render', () => {
  beforeEach(() => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app',
        isCollector: true,
        isAdminRole: true
      }
    };
  });

  it('Should render', () => {
    const mockAction = spyWorkQueueList('getWorkQueueList');

    renderMockStore(<WorkQueueItem collectorId={storeId} />, { initialState });

    expect(mockAction).toBeCalled();
    expect(screen.getByText(QUEUE_LIST)).toBeInTheDocument();
  });

  it('Should render without commonConfig', () => {
    window.appConfig = {} as any;

    const mockAction = spyWorkQueueList('getWorkQueueList');

    renderMockStore(<WorkQueueItem collectorId={storeId} />, { initialState });

    expect(mockAction).toBeCalled();
    expect(screen.getByText(QUEUE_LIST)).toBeInTheDocument();
  });

  it('Should render with isAdminRole', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app',
        isCollector: false,
        isAdminRole: true
      }
    };

    const mockAction = spyWorkQueueList('getWorkQueueList');

    renderMockStore(<WorkQueueItem collectorId={storeId} />, { initialState });

    expect(mockAction).toBeCalled();
    expect(screen.getByText(QUEUE_LIST)).toBeInTheDocument();
  });

  it('Should render without role', () => {
    const mockAction = spyWorkQueueList('getWorkQueueList');

    renderMockStore(<WorkQueueItem collectorId={storeId} />, {
      initialState: { workQueueList }
    });

    expect(mockAction).toBeCalled();
    expect(screen.getByText(QUEUE_LIST)).toBeInTheDocument();
  });

  it('Should render with error', () => {
    renderMockStore(<WorkQueueItem collectorId={storeId} />, {
      initialState: {
        workQueueList: { workQueue: { error: 'error' }, userList: {} }
      }
    });

    expect(screen.getByText(I18N_COMMON_TEXT.RELOAD)).toBeInTheDocument();
    expect(screen.queryByText(QUEUE_LIST)).toBeNull();
  });

  it('Should render with empty grid data', () => {
    jest.spyOn(usePagination, 'usePagination').mockImplementation(
      () =>
        ({
          total: 200,
          currentPage: 1,
          pageSize: [10, 25, 50],
          currentPageSize: 10,
          gridData: null,
          onPageChange: () => {},
          onPageSizeChange: () => {}
        } as any)
    );

    renderMockStore(<WorkQueueItem collectorId={storeId} />, {
      initialState
    });

    expect(screen.getByText(QUEUE_LIST)).toBeInTheDocument();
  });

  it('Should render when empty state', () => {
    const mockAction = spyWorkQueueList('getWorkQueueList');

    renderMockStore(<WorkQueueItem collectorId={storeId} />, {
      initialState: {}
    });

    expect(mockAction).toBeCalled();
  });
});

describe('Actions', () => {
  it('handleLoadData', () => {
    const mockAction = spyWorkQueueList('getWorkQueueList');

    renderMockStore(<WorkQueueItem collectorId={storeId} />, {
      initialState: {
        workQueueList: {
          workQueue: {
            error: 'error'
          },
          userList: {}
        }
      }
    });

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.RELOAD));

    expect(mockAction).toBeCalled();
  });

  it('handleLoadData with collector All', () => {
    const mockAction = spyWorkQueueList('getWorkQueueList');

    renderMockStore(<WorkQueueItem collectorId={storeId} />, {
      initialState: {
        workQueueList: {
          workQueue: {
            error: 'error',
            data: {
              collectorId: 'collectorId',
              owner: { userId: 'owner_id', userName: 'All' },
              collector: []
            }
          },
          userList: {
            data: [
              { userId: 'user_1', fullName: 'fullName 1', userName: 'All' }
            ]
          }
        }
      }
    });

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.RELOAD));

    expect(mockAction).toBeCalled();
  });

  it('handleChange', () => {
    const mockAction = spyWorkQueueList('selectedCollectorValue');

    renderMockStore(<WorkQueueItem collectorId={storeId} />, {
      initialState
    });

    act(() => {
      fireEvent.change(screen.getByTestId('DropdownList_onChange'), {
        target: { value: 'value' }
      });
    });

    expect(mockAction).toBeCalled();
  });

  it('handleChange with collector All', () => {
    const mockAction = spyWorkQueueList('selectedCollectorValue');

    renderMockStore(<WorkQueueItem collectorId={storeId} />, {
      initialState
    });

    act(() => {
      fireEvent.change(screen.getByTestId('DropdownList_onChange'), {
        target: { value: 'All' }
      });
    });

    expect(mockAction).toBeCalled();
  });
});
