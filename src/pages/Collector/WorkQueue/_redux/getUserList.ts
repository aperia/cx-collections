import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Type
import {
  GetUserListPayload,
  GetUserListArgs,
  WorkQueueListState,
} from '../types';

// Service
import { workQueueServices } from './../workQueueServices';


export const getUserList = createAsyncThunk<
  GetUserListPayload,
  GetUserListArgs,
  ThunkAPIConfig
>('collector/getUserList', async (args, thunkAPI) => {
  const data = await workQueueServices.getUserList();
  return data;
});

export const getUserListBuilder = (
  builder: ActionReducerMapBuilder<WorkQueueListState>
) => {
  builder.addCase(getUserList.pending, (draftState, action) => {
    draftState.userList = {
      ...draftState?.userList,
      loading: true
    }
  });
  builder.addCase(getUserList.fulfilled, (draftState, action) => {
    const payload = action.payload;

    draftState.userList = {
      ...draftState?.userList,
      data: payload?.data,
      loading: false
    }
  });
  builder.addCase(getUserList.rejected, (draftState, action) => {
    draftState.userList = {
      ...draftState?.userList,
      loading: false,
      error: action.payload?.errorMessage
    }
  });
};
