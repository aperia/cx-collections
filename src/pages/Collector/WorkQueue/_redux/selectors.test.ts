import { selectorWrapper } from 'app/test-utils';
import * as selectors from './selectors';

const workQueue = {
  loading: true,
  error: 'error',
  pageSize: 10,
  page: 10,
  data: {
    collectorId: 'collectorId',
    owner: { userId: 'owner_id' },
    workQueue: [
      {
        queueId: 'queue_id_1',
        description: 'description 1',
        queueName: 'queue name 1',
        primaryName: ['primary name 1']
      },
      {
        queueId: 'queue_id_2',
        description: 'description 2',
        queueName: 'queue name 2',
        primaryName: ['primary name 2']
      },
      {
        queueId: 'queue_id_3',
        description: 'description 3',
        queueName: 'queue name 3',
        primaryName: ['primary name 3']
      },
      {
        queueId: 'queue_id_4',
        description: 'description 4',
        queueName: 'queue name 4',
        primaryName: ['primary name 4']
      },
      {
        queueId: 'queue_id_5',
        description: 'description 5',
        queueName: 'queue name 5',
        primaryName: ['primary name 5']
      },
      {
        queueId: 'queue_id_6',
        description: 'description 6',
        queueName: 'queue name 6',
        primaryName: ['primary name 6']
      },
      {
        queueId: 'queue_id_7',
        description: 'description 7',
        queueName: 'queue name 7',
        primaryName: ['primary name 7']
      },
      {
        queueId: 'queue_id_8',
        description: 'description 8',
        queueName: 'queue name 8',
        primaryName: ['primary name 8']
      },
      {
        queueId: 'queue_id_9',
        description: 'description 9',
        queueName: 'queue name 9',
        primaryName: ['primary name 9']
      },
      {
        queueId: 'queue_id_10',
        description: 'description 10',
        queueName: 'queue name 10',
        primaryName: ['primary name 10']
      },
      {
        queueId: 'queue_id_11',
        description: 'description 11',
        queueName: 'queue name 11',
        primaryName: ['primary name 11']
      }
    ],
    collector: [
      {
        userId: 'collector_id_1',
        fullName: 'collector fullName 1',
        userName: 'collector userName 1'
      },
      {
        userId: 'collector_id_2',
        fullName: 'collector fullName 2',
        userName: 'collector userName 2'
      }
    ]
  }
};

const userList = {
  loading: true,
  error: 'error',
  data: [
    { userId: 'user_1', fullName: 'fullName 1', userName: 'All' },
    { userId: 'user_2', fullName: 'fullName 2', userName: 'userName 2' },
    { userId: 'user_3', fullName: 'fullName 3', userName: 'userName 3' },
    { userId: 'user_4', fullName: 'fullName 4', userName: 'userName 4' },
    { userId: 'user_5', fullName: 'fullName 5', userName: 'userName 5' },
    { userId: 'user_6', fullName: 'fullName 6', userName: 'userName 6' },
    { userId: 'user_7', fullName: 'fullName 7', userName: 'userName 7' },
    { userId: 'user_8', fullName: 'fullName 8', userName: 'userName 8' },
    { userId: 'user_9', fullName: 'fullName 9', userName: 'userName 9' },
    { userId: 'user_10', fullName: 'fullName 10', userName: 'userName 10' }
  ]
};

const store: Partial<RootState> = {
  workQueueList: {
    workQueue,
    userList
  }
};

const selectorTrigger = selectorWrapper(store);

describe('Work Queue Selectors', () => {
  it('selectedWorkQueueListLoading', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectedWorkQueueListLoading
    );

    expect(data).toEqual(store.workQueueList!.workQueue.loading);
    expect(emptyData).toEqual(false);
  });

  it('selectedWorkQueueListError', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectedWorkQueueListError
    );

    expect(data).toEqual(store.workQueueList!.workQueue.error);
    expect(emptyData).toEqual('');
  });

  it('selectedWorkQueueList', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectedWorkQueueList
    );

    expect(data).toEqual(store.workQueueList!.workQueue.data);
    expect(emptyData).toEqual(undefined);
  });

  describe('selectedWorkQueueDetail', () => {
    it('with no collector value', () => {
      const newStore = {
        workQueueList: {
          workQueue: {
            ...workQueue,
            pageSize: undefined,
            page: undefined,
            collectorValue: undefined
          },
          userList
        }
      };

      const { data, emptyData } = selectorWrapper(newStore)(
        selectors.selectedWorkQueueDetail
      );

      expect(data).toEqual(
        store.workQueueList!.workQueue.data!.workQueue!.slice(0, 10)
      );
      expect(emptyData).toEqual([]);
    });

    it('with collector value', () => {
      const newStore = {
        workQueueList: {
          workQueue: {
            ...workQueue,
            pageSize: undefined,
            page: undefined,
            collectorValue: 'value'
          },
          userList
        }
      };

      const { data, emptyData } = selectorWrapper(newStore)(
        selectors.selectedWorkQueueDetail
      );

      expect(data).toEqual([]);
      expect(emptyData).toEqual([]);
    });

    it('with empty work queue', () => {
      const newStore = {
        workQueueList: {
          userList,
          workQueue: {
            data: {
              owner: { userId: 'owner_id', userName: 'test' }
            }
          }
        }
      } as any;

      const { data, emptyData } = selectorWrapper(newStore)(
        selectors.selectedWorkQueueDetail
      );

      expect(data).toEqual([]);
      expect(emptyData).toEqual([]);
    });
  });

  it('selectedSize', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectedSize);

    expect(data).toEqual(store.workQueueList!.workQueue.pageSize);
    expect(emptyData).toEqual(10);
  });

  it('selectedPage', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectedPage);

    expect(data).toEqual(store.workQueueList!.workQueue.page);
    expect(emptyData).toEqual(1);
  });

  it('selectedTotalItem', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectedTotalItem);

    expect(data).toEqual(
      store.workQueueList!.workQueue.data!.workQueue!.length
    );
    expect(emptyData).toEqual(0);
  });

  it('selectedCollectorListLoading', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectedCollectorListLoading
    );

    expect(data).toEqual(store.workQueueList!.userList.loading);
    expect(emptyData).toEqual(false);
  });

  it('selectedCollectorListError', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectedCollectorListError
    );

    expect(data).toEqual(store.workQueueList!.userList.error);
    expect(emptyData).toEqual('');
  });

  it('selectedCollectorList', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectedCollectorList
    );

    expect(data).toEqual(store.workQueueList!.userList.data);
    expect(emptyData).toEqual([]);
  });
});
