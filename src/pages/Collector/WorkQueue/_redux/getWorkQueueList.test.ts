import './reducers';
import {
  createStoreWithDefaultMiddleWare,
  responseDefault
} from 'app/test-utils';

import { getWorkQueueList } from './getWorkQueueList';
import { workQueueServices } from '../workQueueServices';
import { GetWorkQueueListArgs } from '../types';

const requestBody: GetWorkQueueListArgs = {
  collectorId: 'collectorId',
  userName: 'userName'
};

describe('workQueueServices', () => {
  it('success', async () => {
    jest.spyOn(workQueueServices, 'getWorkQueueList').mockResolvedValue({
      ...responseDefault,
      data: { CXCOL1: [{ collectorId: 'collectorId' }] }
    });

    const store = createStoreWithDefaultMiddleWare({});

    const response = await getWorkQueueList(requestBody)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ data: { collectorId: 'collectorId' } });
  });

  it('failure', async () => {
    jest.spyOn(workQueueServices, 'getWorkQueueList').mockRejectedValue({});

    const store = createStoreWithDefaultMiddleWare({});

    const response = await getWorkQueueList(requestBody)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });
});
