import {
  workQueueListActions as actions,
  workQueueList as reducer
} from './reducers';

import { WorkQueueListState } from '../types';

const initialState: WorkQueueListState = {
  workQueue: {
    data: {
      collectorId: 'collectorId',
      owner: { userId: 'userId' },
      collector: []
    },
    loading: false,
    error: '',
    page: 1,
    pageSize: 10,
    collectorValue: ''
  },
  userList: {
    data: [],
    loading: false,
    error: '',
    page: 1,
    pageSize: 10
  }
};

describe('WorkQueue reducer', () => {
  it('should run selectedCollectorValue', () => {
    const state = reducer(
      initialState,
      actions.selectedCollectorValue('value')
    );
    expect(state.workQueue.collectorValue).toEqual('value');
  });
});
