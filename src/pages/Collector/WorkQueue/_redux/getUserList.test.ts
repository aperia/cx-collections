import './reducers';
import {
  createStoreWithDefaultMiddleWare,
  responseDefault
} from 'app/test-utils';

import { getUserList } from './getUserList';
import { workQueueServices } from '../workQueueServices';
import { GetWorkQueueListArgs } from '../types';

const requestBody: GetWorkQueueListArgs = {
  collectorId: 'collectorId',
  userType: 'userType',
  userName: 'userName'
};

describe('workQueueServices', () => {
  it('success', async () => {
    jest.spyOn(workQueueServices, 'getUserList').mockResolvedValue({
      ...responseDefault,
      data: { userType: [{ collectorId: 'collectorId' }] }
    });
    const store = createStoreWithDefaultMiddleWare({});

    const response = await getUserList(requestBody)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      ...responseDefault,
      data: { userType: [{ collectorId: 'collectorId' }] }
    });
  });

  it('failure', async () => {
    jest.spyOn(workQueueServices, 'getUserList').mockRejectedValue({});

    const store = createStoreWithDefaultMiddleWare({});

    const response = await getUserList(requestBody)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });
});
