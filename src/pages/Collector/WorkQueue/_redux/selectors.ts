import { createSelector } from '@reduxjs/toolkit';

//constants
import { QUEUE_LIST } from './../constants';

const getWorkQueueList = (states: RootState) => states.workQueueList.workQueue;

const getUserList = (states: RootState) => states.workQueueList.userList;

const getCollectorValue = (states: RootState) =>
  states.workQueueList?.workQueue?.collectorValue;

const getPage = (states: RootState) => {
  return states.workQueueList?.workQueue?.page;
};

const getPageSize = (states: RootState) => {
  return states.workQueueList?.workQueue?.pageSize;
};

export const selectedWorkQueueListLoading = createSelector(
  getWorkQueueList,
  data => data?.loading
);

export const selectedWorkQueueListError = createSelector(
  getWorkQueueList,
  data => data?.error
);

export const selectedWorkQueueList = createSelector(
  [getWorkQueueList],
  data => {
    return data?.data;
  }
);

export const selectedWorkQueueDetail = createSelector(
  [getWorkQueueList, getCollectorValue, getPage, getPageSize],
  (
    data,
    collectorValue,
    page = QUEUE_LIST.DEFAULT_PAGE_NUMBER,
    size = QUEUE_LIST.DEFAULT_PAGE_SIZE
  ) => {
    const start = size * (page - 1);
    const end = size * page;
    if (
      data?.data?.owner?.userName === collectorValue ||
      collectorValue === ''
    ) {
      return data?.data?.workQueue?.slice(start, end) || [];
    } else {
      return (
        data?.data?.workQueue
          ?.filter(
            item =>
              collectorValue && item?.primaryName?.indexOf(collectorValue) > -1
          )
          .slice(start, end) || []
      );
    }
  }
);

export const selectedSize = createSelector(
  getWorkQueueList,
  data => data?.pageSize
);

export const selectedPage = createSelector(
  getWorkQueueList,
  data => data?.page
);

export const selectedTotalItem = createSelector(
  getWorkQueueList,
  data => data?.data?.workQueue?.length || 0
);

export const selectedCollectorListLoading = createSelector(
  getUserList,
  data => data?.loading
);

export const selectedCollectorListError = createSelector(
  getUserList,
  data => data?.error
);

export const selectedCollectorList = createSelector([getUserList], data => {
  return data?.data;
});
