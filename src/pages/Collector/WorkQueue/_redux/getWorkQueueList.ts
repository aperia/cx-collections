import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Type
import {
  GetWorkQueueListArgs,
  GetWorkQueueListPayload,
  WorkQueueListState,
  WorkQueueProps
} from '../types';

// Service
import { workQueueServices } from './../workQueueServices';

export const getWorkQueueList = createAsyncThunk<
  GetWorkQueueListPayload,
  GetWorkQueueListArgs,
  ThunkAPIConfig
>('collector/getCollectorWorkQueue', async (args, thunkAPI) => {
  const { collectorId } = args;

  const { data } = await workQueueServices.getWorkQueueList();

  const dataByUser = data['CXCOL1'].find(
    (item: WorkQueueProps) => `${item.collectorId}` === `${collectorId}`
  );

  return { data: dataByUser };
});

export const getWorkQueueListBuilder = (
  builder: ActionReducerMapBuilder<WorkQueueListState>
) => {
  builder.addCase(getWorkQueueList.pending, (draftState, action) => {
    draftState.workQueue = {
      ...draftState?.workQueue,
      loading: true
    };
  });
  builder.addCase(getWorkQueueList.fulfilled, (draftState, action) => {
    const payload = action.payload;

    draftState.workQueue = {
      ...draftState?.workQueue,
      data: payload?.data,
      loading: false
    };
  });
  builder.addCase(getWorkQueueList.rejected, (draftState, action) => {
    draftState.workQueue = {
      ...draftState?.workQueue,
      loading: false,
      error: action.payload?.errorMessage
    };
  });
};
