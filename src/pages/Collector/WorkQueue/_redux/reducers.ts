import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// types
import { WorkQueueListState } from '../types';

// thunks
import { getWorkQueueList, getWorkQueueListBuilder } from './getWorkQueueList';
import { getUserList, getUserListBuilder } from './getUserList';

const { actions, reducer } = createSlice({
  name: 'workQueueList',
  initialState: {
    workQueue: {
      data: undefined,
      loading: false,
      error: '',
      page: 1,
      pageSize: 10,
      collectorValue: ''
    },
    userList: {
      data: [],
      loading: false,
      error: '',
      page: 1,
      pageSize: 10
    }
  } as WorkQueueListState,
  reducers: {
    selectedCollectorValue: (draftState, action: PayloadAction<string>) => {
      draftState.workQueue = {
        ...draftState.workQueue,
        collectorValue: action.payload
      };
    },
  },

  extraReducers: builder => {
    getWorkQueueListBuilder(builder);
    getUserListBuilder(builder);
  }
});

const workQueueListActions = {
  ...actions,
  getWorkQueueList,
  getUserList
};

export { workQueueListActions, reducer as workQueueList };
