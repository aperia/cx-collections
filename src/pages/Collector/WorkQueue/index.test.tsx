import React from 'react';
import { render, screen } from '@testing-library/react';

import { storeId } from 'app/test-utils';

import WorkQueueCollector from '.';

jest.mock('./WorkQueueItem', () => () => <div>WorkQueueItem</div>);

describe('Render', () => {
  it('Should render', () => {
    render(<WorkQueueCollector collectorId={storeId} />);

    expect(screen.getByText('WorkQueueItem')).toBeInTheDocument();
  });
});
