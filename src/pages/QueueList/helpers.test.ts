import { filterAndOrderQueueList, randomDateFromToDay } from './helpers';

describe('Test helpers Queue list', () => {
  it('randomDateFromToDay', () => {
    const result = randomDateFromToDay(new Date(2020, 0, 1));
    expect(typeof result).toEqual('string');
  });

  it('filterAndOrderQueueList', () => {
    const mockData = {
      data: [],
      sort: 'lastWorkDate',
      order: 'desc',
      searchBy: [],
      queueType: { pull: true, push: true }
    };
    const result = filterAndOrderQueueList(
      mockData.data,
      mockData.sort,
      mockData.order,
      mockData.searchBy,
      mockData.queueType
    );
    expect(result).toEqual([]);
  });

  it('filterAndOrderQueueList > case 2', () => {
    const mockData = {
      data: [
        { queueId: '123', queueName: 'abc' },
        { queueId: '456', queueName: 'xyz' }
      ],
      sort: 'lastWorkDate',
      order: 'desc',
      searchBy: [{ key: 'queueId', value: '123' }],
      queueType: { pull: true, push: true }
    };
    const result = filterAndOrderQueueList(
      mockData.data,
      mockData.sort,
      mockData.order,
      mockData.searchBy,
      mockData.queueType
    );
    expect(result).toEqual([
      {
        queueId: '123',
        queueName: 'abc'
      }
    ]);
  });

  it('filterAndOrderQueueList > case 3', () => {
    const mockData = {
      data: [
        { queueId: '123', queueName: 'abc' },
        { queueId: '456', queueName: 'xyz' }
      ],
      sort: 'lastWorkDate',
      order: 'desc',
      searchBy: [{ key: 'queueName', value: 'abc' }],
      queueType: { pull: true, push: false }
    };
    const result = filterAndOrderQueueList(
      mockData.data,
      mockData.sort,
      mockData.order,
      mockData.searchBy,
      mockData.queueType
    );
    expect(result).toEqual([
      {
        queueId: '123',
        queueName: 'abc'
      }
    ]);
  });

  it('filterAndOrderQueueList > case 4', () => {
    const mockData = {
      data: [
        { queueId: '123', queueName: 'abc' },
        { queueId: '456', queueName: 'xyz' }
      ],
      sort: 'lastWorkDate',
      order: 'desc',
      searchBy: [{ key: 'queueName', value: 'xxx' }],
      queueType: { pull: false, push: false }
    };
    const result = filterAndOrderQueueList(
      mockData.data,
      mockData.sort,
      mockData.order,
      mockData.searchBy,
      mockData.queueType
    );
    expect(result).toEqual([]);
  });

  it('filterAndOrderQueueList > case 5', () => {
    const mockData = {
      data: [
        { queueId: '123', queueName: 'abc', queueType: 'Pull' },
        { queueId: '456', queueName: 'xyz', queueType: 'Push' }
      ],
      sort: 'lastWorkDate',
      order: 'desc',
      searchBy: [],
      queueType: { pull: false, push: false }
    };
    const result = filterAndOrderQueueList(
      mockData.data,
      mockData.sort,
      mockData.order,
      mockData.searchBy,
      mockData.queueType
    );
    expect(result).toEqual([]);
  });

  it('filterAndOrderQueueList > case 6', () => {
    const mockData = {
      data: [
        { queueId: '123', queueName: 'abc', queueType: 'Pull' },
        { queueId: '456', queueName: 'xyz', queueType: 'Push' }
      ],
      sort: 'lastWorkDate',
      order: 'desc',
      searchBy: [],
      queueType: { pull: true, push: false }
    };
    const result = filterAndOrderQueueList(
      mockData.data,
      mockData.sort,
      mockData.order,
      mockData.searchBy,
      mockData.queueType
    );
    expect(result).toEqual([
      {
        queueId: '123',
        queueName: 'abc',
        queueType: 'Pull'
      }
    ]);
  });

  it('filterAndOrderQueueList > case 7', () => {
    const mockData = {
      data: [
        { queueId: '123', queueName: 'abc', queueType: 'Pull' },
        { queueId: '456', queueName: 'xyz', queueType: 'Push' }
      ],
      sort: 'lastWorkDate',
      order: 'desc',
      searchBy: [],
      queueType: { pull: false, push: true }
    };
    const result = filterAndOrderQueueList(
      mockData.data,
      mockData.sort,
      mockData.order,
      mockData.searchBy,
      mockData.queueType
    );
    expect(result).toEqual([
      {
        queueId: '456',
        queueName: 'xyz',
        queueType: 'Push'
      }
    ]);
  });

  it('filterAndOrderQueueList > case 8', () => {
    const mockData = {
      data: [
        { queueId: '123', queueName: 'abc', queueType: 'Pull' },
        { queueId: '456', queueName: 'xyz', queueType: 'Push' }
      ],
      sort: 'lastWorkDate',
      order: 'desc',
      searchBy: [],
      queueType: { pull: true, push: true }
    };
    const result = filterAndOrderQueueList(
      mockData.data,
      mockData.sort,
      mockData.order,
      mockData.searchBy,
      mockData.queueType
    );
    expect(result).toEqual([
      {
        queueId: '123',
        queueName: 'abc',
        queueType: 'Pull'
      },
      {
        queueId: '456',
        queueName: 'xyz',
        queueType: 'Push'
      }
    ]);
  });
});
