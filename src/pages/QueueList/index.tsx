import React, { useEffect, useMemo, useState } from 'react';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import {
  queueListActions as actions,
  queueListActions
} from './_redux/reducers';
import {
  selectedQueueList,
  selectedQueueListError,
  selectedQueueListLoading,
  selectedQueueType
} from './_redux/selectors';

// components
import CardQueue from 'pages/AccountSearch/Home/Dashboard/DashboardRecentWorkqueue/CardQueue';
import {
  AttributeSearch,
  AttributeSearchEvent,
  AttributeSearchValue,
  Button,
  CheckBox,
  DropdownList,
  Pagination,
  SimpleBar
} from 'app/_libraries/_dls/components';
import { FailedApiReload } from 'app/components';

// hooks
import { usePagination } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// const
import { QUEUE_LIST } from 'pages/AccountDetails/NextActions/constants';

// helpers
import classnames from 'classnames';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { QueueListSortBy } from './QueueListSortBy';
import { QueueListOrderBy } from './QueueListOrderBy';
import { getAttrSearchQueueList } from './attributeSearch';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { attrValidations } from './searchValidation';
import every from 'lodash.every';

import {
  FIELD_QUEUE_NOT_CONSTRAINT,
  NO_COMBINE_FIELDS_QUEUE
} from './constants';
import { DROPDOWN_COLLECTOR } from 'app/constants';
import { SearchNodata } from 'app/components';

const QueueList: React.FC = () => {
  const dispatch = useDispatch();

  const { t } = useTranslation();

  const { isSupervisor } = window.appConfig?.commonConfig || {};

  const queueDetails = useSelector(selectedQueueList);

  const isError = useSelector(selectedQueueListError);

  const isLoading = useSelector(selectedQueueListLoading);

  const queueType = useSelector(selectedQueueType);

  const [filterValue, setFilterValue] = useState<AttributeSearchValue[]>([]);

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(queueDetails);

  const isShowPagination = total > pageSize[0];

  const testId = 'queueList';

  const { data } = useMemo(() => getAttrSearchQueueList(t), [t]);

  const noCombineFields = React.useMemo(() => {
    const isEnableNoCombineFeature = every(filterValue, ({ key }) => {
      return FIELD_QUEUE_NOT_CONSTRAINT.includes(key);
    });

    const translationFields = NO_COMBINE_FIELDS_QUEUE.map(r => ({
      ...r,
      filterMessage: t(r.filterMessage)
    }));

    return isEnableNoCombineFeature ? translationFields : [];
  }, [filterValue, t]);

  useEffect(() => {
    dispatch(actions.getQueueList());
  }, [dispatch]);

  const handleReload = () => dispatch(actions.getQueueList());

  const handleChange = (event: AttributeSearchEvent) => {
    const { value } = event;
    setFilterValue(value);
  };

  const handleOnClear = () => {
    setFilterValue([]);
  };

  const handleClearAndResetSearch = () => {
    setFilterValue([]);
    dispatch(actions.resetPreviousQueueList());
  };

  const handleSearch = (
    event: AttributeSearchEvent,
    error?: Record<string, string>
  ) => {
    if (!isEmpty(error) || !event.value.length) return;
    batch(() => {
      dispatch(queueListActions.onChangeSearchValue(event.value));
    });
  };

  const handleChangePullType = () => {
    dispatch(queueListActions.onChangeQueueTypePull());
  };
  const handleChangePushType = () => {
    dispatch(queueListActions.onChangeQueueTypePush());
  };
  if (isError) {
    return (
      <FailedApiReload
        id="queue-list__failed"
        className="mt-80"
        onReload={handleReload}
        dataTestId={testId}
      />
    );
  }

  const renderQueueList = () =>
    gridData.map((item: any, index: number) => (
      <CardQueue
        id={`dashboard__work-queue__card-queue__${item.queueName}`}
        dataTestId={genAmtId(testId, `card-${item.queueName}`, '')}
        key={index}
        queueName={item.queueName}
        queueId={item.queueId}
        userId={item.userId}
        agentId={item.agentId}
        collectorQueueStats={item.collectorQueueStats}
        collectorPromiseStats={item.collectorPromiseStats}
        queueType={item.queueType}
        lastWorkDate={item.lastWorkDate}
        assignedTo={item.assignedTo}
        totalNumbersOfAccount={item.totalNumbersOfAccount}
        totalOutstandingBalanceAmount={item.totalOutstandingBalanceAmount}
        fromDashboard={false}
      />
    ));

  return (
    <div className={classnames('h-100', { loading: isLoading })}>
      <SimpleBar id="queue-list__simple-bar">
        <div className="p-24">
          <div className="dash-board-content">
            <div className="ml-auto mb-16 d-flex justify-content-between">
              <h3 data-testid={genAmtId(testId, 'title', '')}>
                {t(QUEUE_LIST)}
              </h3>
              <AttributeSearch
                id={`queueList__attributeSearch`}
                dataTestId={`${testId}_queueListSearch`}
                data={data}
                value={filterValue}
                orderBy="asc"
                small
                onChange={handleChange}
                onSearch={handleSearch}
                onClear={handleOnClear}
                validator={(value: Record<string, any>) =>
                  attrValidations(value, t)
                }
                noCombineFields={noCombineFields}
                clearTooltipProps={{
                  placement: 'top',
                  containerClassName: 'tooltip-clear',
                  element: t('txt_clear_all_criteria')
                }}
                placeholder={t('txt_type_to_add_parameter')}
                noFilterResultText={t('txt_no_other_parameters_available')}
              />
            </div>

            <div className="d-flex justify-content-between">
              <div className="d-flex">
                {isSupervisor && (
                  <div className="d-flex align-items-center mr-24">
                    <p className="fw-500 color-grey mr-4">
                      {t('txt_collector')}:
                    </p>
                    <DropdownList
                      id={'DropdownSortByQueueList_Collector'}
                      textField="description"
                      variant={'no-border'}
                      value={'All'}
                      noResult={t('txt_no_results_found')}
                    >
                      {DROPDOWN_COLLECTOR.map(item => (
                        <DropdownList.Item
                          key={item.value}
                          label={t(item.description)}
                          value={item}
                        />
                      ))}
                    </DropdownList>
                  </div>
                )}

                <div className="d-flex align-items-center">
                  <h5 className="text-capitalize fs-14">
                    {t('txt_queue_type')}:
                  </h5>
                  <div className="d-flex justify-content-between w-10">
                    <CheckBox className="ml-24">
                      <CheckBox.Input
                        checked={queueType['pull']}
                        onChange={handleChangePullType}
                      />
                      <CheckBox.Label>{t('txt_pull')}</CheckBox.Label>
                    </CheckBox>
                    <CheckBox className="ml-24">
                      <CheckBox.Input
                        checked={queueType['push']}
                        onChange={handleChangePushType}
                      />
                      <CheckBox.Label>{t('txt_push')}</CheckBox.Label>
                    </CheckBox>
                  </div>
                </div>
              </div>

              <div className="d-none d-lg-block">
                <div className="d-flex align-items-center">
                  <QueueListSortBy />
                  <QueueListOrderBy />
                </div>
              </div>
            </div>
            {!isEmpty(filterValue) && !isEmpty(gridData) && (
              <div className="text-right mt-16 mr-n8 mr-4">
                <Button
                  size="sm"
                  variant="outline-primary"
                  onClick={handleClearAndResetSearch}
                  dataTestId={`${testId}__clearAndResetSearch`}
                >
                  {t('txt_clear_and_reset')}
                </Button>
              </div>
            )}

            <>
              {isEmpty(gridData) && !isLoading ? (
                <div className="p-24">
                  <SearchNodata
                    text={t('txt_no_search_or_filter_results_found')}
                    onClearSearch={handleClearAndResetSearch}
                  />
                </div>
              ) : (
                renderQueueList()
              )}

              {isShowPagination && (
                <div className="pt-16">
                  <Pagination
                    totalItem={total}
                    pageSize={pageSize}
                    pageNumber={currentPage}
                    pageSizeValue={currentPageSize}
                    compact
                    onChangePage={onPageChange}
                    onChangePageSize={onPageSizeChange}
                    dataTestId={testId}
                  />
                </div>
              )}
            </>
          </div>
        </div>
      </SimpleBar>
    </div>
  );
};

export default QueueList;
