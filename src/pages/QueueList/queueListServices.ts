import { apiService } from 'app/utils/api.service';
import { QueueListRequest } from './types';

export const queueListServices = {
  getQueueList() {
    const url = window.appConfig?.api?.queueList?.getQueueList || '';
    return apiService.get(url);
  },
  getCollectorQueueList(bodyRequest: QueueListRequest) {
    const url = window.appConfig?.api?.queueList?.getCollectorQueueList || '';
    return apiService.post(url, bodyRequest);
  }
};
