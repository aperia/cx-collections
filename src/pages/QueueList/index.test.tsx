import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import QueueList from '.';
import { queueListActions } from './_redux/reducers';
import {
  AttributeSearchEvent,
  AttributeSearchProps
} from 'app/_libraries/_dls/components';

jest.mock('app/_libraries/_dls/components/AttributeSearch', () => {
  return (props: AttributeSearchProps) => (
    <>
      <div
        data-testid="queueList__test_onClear"
        onClick={() => props.onClear!()}
      >
        onClear
      </div>
      <div
        onClick={() => props.onChange!({ value: {} } as AttributeSearchEvent)}
        data-testid="queueList__test_onChange"
      >
        onChange
      </div>
      <div
        onClick={() =>
          props.onChange!({
            value: [{ key: 'queueName', value: 'abcabc' }]
          } as AttributeSearchEvent)
        }
        data-testid="queueList__test_onChangeValue"
      >
        onChangeValue
      </div>

      <div
        onClick={() =>
          props.onChange!({
            value: [{ key: 'xxx', value: 'abcabc' }]
          } as AttributeSearchEvent)
        }
        data-testid="queueList__test_onChangeValue2"
      >
        onChangeValue
      </div>

      <div
        onClick={() =>
          props.onChange!({
            value: [{ key: 'queueId', value: '001' }]
          } as AttributeSearchEvent)
        }
        data-testid="queueList__test_onChangeValueQueueId"
      >
        onChangeValueQueueId
      </div>
      <div
        onClick={() =>
          props.onSearch!({
            value: [{ key: 'queueId', value: '123123' }]
          } as AttributeSearchEvent)
        }
        data-testid="queueList__test_onSearch"
      >
        onSearch
      </div>
      <div
        onClick={() =>
          props.onSearch!({
            value: {}
          } as AttributeSearchEvent)
        }
        data-testid="queueList__test_onSearchError"
      >
        onSearchError
      </div>
      <div
        onClick={() =>
          props.validator!({
            value: {}
          } as AttributeSearchEvent)
        }
        data-testid="queueList__test_onValidator"
      >
        onValidator
      </div>
    </>
  );
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockQueueList = [
  {
    queueName: 'Queue 004 - Bankruptcy',
    collectorQueueStats: {
      totalAccounts: '680',
      accountsWorked: '180',
      totalAmount: '59092.00',
      amountCollected: '24032.00'
    },
    collectorPromiseStats: {
      pendingPromises: '304',
      pendingAmount: '35900.00',
      keptPromises: '220',
      keptAmount: '20840.00',
      brokenPromises: '156',
      brokenAmount: '2352.00',
      totalPromises: '680',
      totalAmount: '59092.00'
    }
  },
  {
    queueName: 'Queue 004 - Bankruptcy',
    collectorQueueStats: {
      totalAccounts: '680',
      accountsWorked: '180',
      totalAmount: '59092.00',
      amountCollected: '24032.00'
    },
    collectorPromiseStats: {
      pendingPromises: '304',
      pendingAmount: '35900.00',
      keptPromises: '220',
      keptAmount: '20840.00',
      brokenPromises: '156',
      brokenAmount: '2352.00',
      totalPromises: '680',
      totalAmount: '59092.00'
    }
  }
];

const mock12QueueList = [
  {
    queueName: 'Queue 001 - Bankruptcy',
    queueId: '001',
    collectorQueueStats: {
      totalAccounts: '680',
      accountsWorked: '180',
      totalAmount: '59092.00',
      amountCollected: '24032.00'
    },
    collectorPromiseStats: {
      pendingPromises: '304',
      pendingAmount: '35900.00',
      keptPromises: '220',
      keptAmount: '20840.00',
      brokenPromises: '156',
      brokenAmount: '2352.00',
      totalPromises: '680',
      totalAmount: '59092.00'
    }
  },
  {
    queueName: 'Queue 002 - Bankruptcy',
    queueId: '002',
    collectorQueueStats: {
      totalAccounts: '680',
      accountsWorked: '180',
      totalAmount: '59092.00',
      amountCollected: '24032.00'
    },
    collectorPromiseStats: {
      pendingPromises: '304',
      pendingAmount: '35900.00',
      keptPromises: '220',
      keptAmount: '20840.00',
      brokenPromises: '156',
      brokenAmount: '2352.00',
      totalPromises: '680',
      totalAmount: '59092.00'
    }
  },
  {
    queueName: 'Queue 003 - Bankruptcy',
    queueId: '003',
    collectorQueueStats: {
      totalAccounts: '680',
      accountsWorked: '180',
      totalAmount: '59092.00',
      amountCollected: '24032.00'
    },
    collectorPromiseStats: {
      pendingPromises: '304',
      pendingAmount: '35900.00',
      keptPromises: '220',
      keptAmount: '20840.00',
      brokenPromises: '156',
      brokenAmount: '2352.00',
      totalPromises: '680',
      totalAmount: '59092.00'
    }
  },
  {
    queueName: 'Queue 004 - Bankruptcy',
    queueId: '004',
    collectorQueueStats: {
      totalAccounts: '680',
      accountsWorked: '180',
      totalAmount: '59092.00',
      amountCollected: '24032.00'
    },
    collectorPromiseStats: {
      pendingPromises: '304',
      pendingAmount: '35900.00',
      keptPromises: '220',
      keptAmount: '20840.00',
      brokenPromises: '156',
      brokenAmount: '2352.00',
      totalPromises: '680',
      totalAmount: '59092.00'
    }
  },
  {
    queueName: 'Queue 005 - Bankruptcy',
    queueId: '005',
    collectorQueueStats: {
      totalAccounts: '680',
      accountsWorked: '180',
      totalAmount: '59092.00',
      amountCollected: '24032.00'
    },
    collectorPromiseStats: {
      pendingPromises: '304',
      pendingAmount: '35900.00',
      keptPromises: '220',
      keptAmount: '20840.00',
      brokenPromises: '156',
      brokenAmount: '2352.00',
      totalPromises: '680',
      totalAmount: '59092.00'
    }
  },
  {
    queueName: 'Queue 006 - Bankruptcy',
    queueId: '006',
    collectorQueueStats: {
      totalAccounts: '680',
      accountsWorked: '180',
      totalAmount: '59092.00',
      amountCollected: '24032.00'
    },
    collectorPromiseStats: {
      pendingPromises: '304',
      pendingAmount: '35900.00',
      keptPromises: '220',
      keptAmount: '20840.00',
      brokenPromises: '156',
      brokenAmount: '2352.00',
      totalPromises: '680',
      totalAmount: '59092.00'
    }
  },
  {
    queueName: 'Queue 007 - Bankruptcy',
    queueId: '007',
    collectorQueueStats: {
      totalAccounts: '680',
      accountsWorked: '180',
      totalAmount: '59092.00',
      amountCollected: '24032.00'
    },
    collectorPromiseStats: {
      pendingPromises: '304',
      pendingAmount: '35900.00',
      keptPromises: '220',
      keptAmount: '20840.00',
      brokenPromises: '156',
      brokenAmount: '2352.00',
      totalPromises: '680',
      totalAmount: '59092.00'
    }
  },
  {
    queueName: 'Queue 008 - Bankruptcy',
    queueId: '008',
    collectorQueueStats: {
      totalAccounts: '680',
      accountsWorked: '180',
      totalAmount: '59092.00',
      amountCollected: '24032.00'
    },
    collectorPromiseStats: {
      pendingPromises: '304',
      pendingAmount: '35900.00',
      keptPromises: '220',
      keptAmount: '20840.00',
      brokenPromises: '156',
      brokenAmount: '2352.00',
      totalPromises: '680',
      totalAmount: '59092.00'
    }
  },
  {
    queueName: 'Queue 009 - Bankruptcy',
    queueId: '009',
    collectorQueueStats: {
      totalAccounts: '680',
      accountsWorked: '180',
      totalAmount: '59092.00',
      amountCollected: '24032.00'
    },
    collectorPromiseStats: {
      pendingPromises: '304',
      pendingAmount: '35900.00',
      keptPromises: '220',
      keptAmount: '20840.00',
      brokenPromises: '156',
      brokenAmount: '2352.00',
      totalPromises: '680',
      totalAmount: '59092.00'
    }
  },
  {
    queueName: 'Queue 010 - Bankruptcy',
    queueId: '001',
    collectorQueueStats: {
      totalAccounts: '680',
      accountsWorked: '180',
      totalAmount: '59092.00',
      amountCollected: '24032.00'
    },
    collectorPromiseStats: {
      pendingPromises: '304',
      pendingAmount: '35900.00',
      keptPromises: '220',
      keptAmount: '20840.00',
      brokenPromises: '156',
      brokenAmount: '2352.00',
      totalPromises: '680',
      totalAmount: '59092.00'
    }
  },
  {
    queueName: 'Queue 011 - Bankruptcy',
    queueId: '011',
    collectorQueueStats: {
      totalAccounts: '680',
      accountsWorked: '180',
      totalAmount: '59092.00',
      amountCollected: '24032.00'
    },
    collectorPromiseStats: {
      pendingPromises: '304',
      pendingAmount: '35900.00',
      keptPromises: '220',
      keptAmount: '20840.00',
      brokenPromises: '156',
      brokenAmount: '2352.00',
      totalPromises: '680',
      totalAmount: '59092.00'
    }
  },
  {
    queueName: 'Queue 012 - Bankruptcy',
    queueId: '012',
    collectorQueueStats: {
      totalAccounts: '680',
      accountsWorked: '180',
      totalAmount: '59092.00',
      amountCollected: '24032.00'
    },
    collectorPromiseStats: {
      pendingPromises: '304',
      pendingAmount: '35900.00',
      keptPromises: '220',
      keptAmount: '20840.00',
      brokenPromises: '156',
      brokenAmount: '2352.00',
      totalPromises: '680',
      totalAmount: '59092.00'
    }
  }
];

const queueListActionsMock = mockActionCreator(queueListActions);

beforeEach(() => {
  queueListActionsMock('getQueueList');
});

describe('Queue List', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isSupervisor: true
    }
  };
  it('should not render pagination ', () => {
    renderMockStoreId(<QueueList />, {
      initialState: {
        queueList: {
          data: { queueDetails: mockQueueList },
          loading: false,
          error: '',
          sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
          orderBy: { description: 'Descending', value: 'desc' },
          searchBy: [],
          queueType: { pull: true, push: true }
        }
      }
    });
    userEvent.click(screen.getByTestId('queueList__test_onValidator'));
    expect(screen.getByText('txt_queue_list')).toBeInTheDocument();
  });

  it('should render with empty data ', () => {
    renderMockStoreId(<QueueList />, {
      initialState: {
        queueList: {
          data: { queueDetails: [] },
          loading: false,
          error: '',
          sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
          orderBy: { description: 'Descending', value: 'desc' },
          searchBy: [],
          queueType: { pull: true, push: true }
        }
      }
    });
    expect(screen.queryByText('txt_queue_list')).toBeInTheDocument();
  });
  it('should have pagination ', () => {
    const { wrapper } = renderMockStoreId(<QueueList />, {
      initialState: {
        queueList: {
          data: { queueDetails: mock12QueueList },
          loading: false,
          error: '',
          sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
          orderBy: { description: 'Descending', value: 'desc' },
          searchBy: [],
          queueType: { pull: true, push: true }
        }
      }
    });
    expect(
      queryByClass(wrapper.container, /pagination-wrapper/)
    ).toBeInTheDocument();
  });

  it('render error ', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: undefined
    } as never;
    renderMockStoreId(<QueueList />, {
      initialState: {
        queueList: {
          data: { queueDetails: mock12QueueList },
          error: 'having error',
          loading: false,
          sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
          orderBy: { description: 'Descending', value: 'desc' },
          searchBy: [],
          queueType: { pull: true, push: true }
        }
      }
    });
    expect(screen.getByText('txt_reload')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleReload', () => {
    const mockAction = queueListActionsMock('getQueueList');

    renderMockStoreId(<QueueList />, {
      initialState: {
        queueList: {
          data: { queueDetails: mock12QueueList },
          error: 'having error',
          loading: false,
          sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
          orderBy: { description: 'Descending', value: 'desc' },
          searchBy: [],
          queueType: { pull: true, push: true }
        }
      }
    });

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.RELOAD));

    expect(mockAction).toBeCalled();
  });

  it('handleSearch', () => {
    const mockAction = queueListActionsMock('getQueueList');

    renderMockStoreId(<QueueList />, {
      initialState: {
        queueList: {
          data: { queueDetails: mock12QueueList },
          error: '',
          loading: false,
          sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
          orderBy: { description: 'Descending', value: 'desc' },
          searchBy: [{ key: 'Queue Id', value: 'queueId' }],
          queueType: { pull: true, push: true }
        }
      }
    });

    userEvent.click(screen.getByTestId('queueList__test_onSearch'));

    expect(mockAction).toBeCalled();
  });

  it('handleSearch > error', () => {
    jest.mock('pages/QueueList/searchValidation', () => ({
      attrValidations: jest.fn()
    }));

    const mockAction = queueListActionsMock('getQueueList');

    renderMockStoreId(<QueueList />, {
      initialState: {
        queueList: {
          data: { queueDetails: mock12QueueList },
          error: '',
          loading: false,
          sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
          orderBy: { description: 'Descending', value: 'desc' },
          searchBy: [{ key: 'Queue Id', value: 'queueId' }],
          queueType: { pull: true, push: true }
        }
      }
    });

    userEvent.click(screen.getByTestId('queueList__test_onSearchError'));

    expect(mockAction).toBeCalled();
  });

  it('handleChange', () => {
    renderMockStoreId(<QueueList />, {
      initialState: {
        queueList: {
          data: { queueDetails: mock12QueueList },
          error: '',
          loading: false,
          sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
          orderBy: { description: 'Descending', value: 'desc' },
          searchBy: [{ key: 'queueId', value: 'queueId' }],
          queueType: { pull: true, push: true }
        }
      }
    });

    const input = screen.getByTestId('queueList__test_onChangeValue');

    userEvent.click(input);
  });

  it('handleChange > case 2', () => {
    renderMockStoreId(<QueueList />, {
      initialState: {
        queueList: {
          data: { queueDetails: mock12QueueList },
          error: '',
          loading: false,
          sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
          orderBy: { description: 'Descending', value: 'desc' },
          searchBy: [{ key: 'xxx', value: '123123' }],
          queueType: { pull: true, push: true }
        }
      }
    });

    const input = screen.getByTestId('queueList__test_onChangeValue2');

    userEvent.click(input);
  });

  it('handleOnClear', () => {
    renderMockStoreId(<QueueList />, {
      initialState: {
        queueList: {
          data: { queueDetails: mock12QueueList },
          error: '',
          loading: false,
          sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
          orderBy: { description: 'Descending', value: 'desc' },
          searchBy: [{ key: 'queueId', value: 'queueId' }],
          queueType: { pull: true, push: true }
        }
      }
    });

    const input = screen.getByTestId('queueList__test_onClear');

    userEvent.click(input);
  });

  it('handleChangePullType', () => {
    const mockAction = queueListActionsMock('getQueueList');

    renderMockStoreId(<QueueList />, {
      initialState: {
        queueList: {
          data: { queueDetails: mock12QueueList },
          error: '',
          loading: false,
          sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
          orderBy: { description: 'Descending', value: 'desc' },
          searchBy: [{ key: 'Queue Id', value: 'queueId' }],
          queueType: { pull: true, push: true }
        }
      }
    });

    userEvent.click(screen.getByText('txt_pull'));

    expect(mockAction).toBeCalled();
  });

  it('handleChangePushType', () => {
    const mockAction = queueListActionsMock('getQueueList');

    renderMockStoreId(<QueueList />, {
      initialState: {
        queueList: {
          data: { queueDetails: mock12QueueList },
          error: '',
          loading: false,
          sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
          orderBy: { description: 'Descending', value: 'desc' },
          searchBy: [{ key: 'Queue Id', value: 'queueId' }],
          queueType: { pull: true, push: true }
        }
      }
    });

    userEvent.click(screen.getByText('txt_push'));

    expect(mockAction).toBeCalled();
  });

  it('handleClearAndResetSearch', () => {
    renderMockStoreId(<QueueList />, {
      initialState: {
        queueList: {
          data: { queueDetails: mock12QueueList },
          error: '',
          loading: false,
          sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
          orderBy: { description: 'Descending', value: 'desc' },
          searchBy: [{ key: 'queueId', value: '001' }],
          queueType: { pull: true, push: true }
        }
      }
    });

    const input = screen.getByTestId('queueList__test_onChangeValueQueueId');

    userEvent.click(input);

    const btnReset = screen.getByTestId(
      'queueList__clearAndResetSearch_dls-button'
    );
    userEvent.click(btnReset);
  });
});
