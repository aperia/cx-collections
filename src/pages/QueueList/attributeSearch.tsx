import React from 'react';

import { I18N_COMMON_TEXT } from "app/constants/i18n"
import { AttributeSearchData } from "app/_libraries/_dls/components"
import { ATTRIBUTE_QUEUE_SEARCH_FIELD } from "./constants"
import InputControl from 'app/components/InputControl';

export const getAttrSearchQueueList = (t : any) => {
    const queueIdField: AttributeSearchData = {
        key: ATTRIBUTE_QUEUE_SEARCH_FIELD.QUEUE_ID,
        name: t(I18N_COMMON_TEXT.QUEUE_ID),
        description: t('txt_queue_id_description'),
        component: props => (
          <InputControl
            placeholder={I18N_COMMON_TEXT.ENTER_KEYWORD}
            maxLength={12}
            mode="number"
            {...props}
          />
        ),
        groupName: t(I18N_COMMON_TEXT.SEARCH_PARAMETERS)
      };
      const queueNameField: AttributeSearchData = {
        key: ATTRIBUTE_QUEUE_SEARCH_FIELD.QUEUE_NAME,
        name: t(I18N_COMMON_TEXT.QUEUE_NAME),
        description: t('txt_queue_name_description'),
        component: props => (
          <InputControl
            placeholder={I18N_COMMON_TEXT.ENTER_KEYWORD}
            maxLength={50}
            mode="text"
            {...props}
          />
        ),
        groupName: t(I18N_COMMON_TEXT.SEARCH_PARAMETERS)
      };
      return {
        data: [
          queueIdField,
          queueNameField
        ]
      }
}