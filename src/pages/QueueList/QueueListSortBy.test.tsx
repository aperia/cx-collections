import React from 'react';

import { fireEvent, screen } from '@testing-library/react';
import {
  mockActionCreator,
  queryByClass,
  renderMockStoreId
} from 'app/test-utils';
import { queueListActions } from './_redux/reducers';

import { QueueListSortBy } from './QueueListSortBy';
import userEvent from '@testing-library/user-event';
import { queueListSortByTestId } from './types';

HTMLCanvasElement.prototype.getContext = jest.fn();

const queueListActionsMock = mockActionCreator(queueListActions);

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(
    <div>
      <QueueListSortBy />
    </div>,
    { initialState }
  );
};

const initialState: Partial<RootState> = {
  queueList: {
    sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
    SortBy: { description: 'Descending', value: 'desc' }
  }
} as any;

beforeEach(() => {
  queueListActionsMock('getQueueList');
});

describe('Actions', () => {
  it('handleChangeDropDown', () => {
    const { wrapper } = renderWrapper({
      ...initialState
    });
    const button = queryByClass(wrapper.container, /text-field-container/);

    userEvent.click(button!);
    const el = screen.queryByTestId(
      `txt_last_work_date-${queueListSortByTestId}__DropdownSortByQueueList_dropdown-base-item_text`
    );
    fireEvent.click(el!);
  });
});
