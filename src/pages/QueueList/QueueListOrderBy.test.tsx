import React from 'react';

import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import {
  mockActionCreator,
  queryByClass,
  renderMockStoreId
} from 'app/test-utils';
import { QueueListOrderBy } from './QueueListOrderBy';
import { QueueListOrderByTestId } from './types';
import { queueListActions } from './_redux/reducers';

HTMLCanvasElement.prototype.getContext = jest.fn();

const queueListActionsMock = mockActionCreator(queueListActions);

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(
    <div>
      <QueueListOrderBy />
    </div>,
    { initialState }
  );
};

const initialState: Partial<RootState> = {
  queueList: {
    sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
    orderBy: { description: 'Descending', value: 'desc' }
  }
} as any;

beforeEach(() => {
  queueListActionsMock('getQueueList');
});

describe('Actions', () => {
  it('handleChangeDropDown', () => {
    const { wrapper } = renderWrapper({
      ...initialState
    });
    const button = queryByClass(wrapper.container, /text-field-container/);

    userEvent.click(button!);
    screen.debug();
    const el = screen.queryByTestId(
      `txt_ascending-${QueueListOrderByTestId}__DropdownOrderByQueueList_dropdown-base-item_text`
    );
    userEvent.click(el!);
  });
});
