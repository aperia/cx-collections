import React, { useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// components
import {
  DropdownBaseChangeEvent,
  DropdownList
} from 'app/_libraries/_dls/components';

// redux
import { queueListActions } from './_redux/reducers';
import { selectedQueueListOrderBy } from './_redux/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants & types
import { ORDER_DROPDOWN } from 'app/constants';
import { QueueListOrderByTestId } from './types';

export interface QueueListOrderByProps {}

export const QueueListOrderBy: React.FC<QueueListOrderByProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const value: RefDataValue = useSelector(selectedQueueListOrderBy);

  const valueDropdown = useMemo(
    () => ORDER_DROPDOWN.find(item => item.value === value?.value),
    [value]
  );

  const handleChange = (e: DropdownBaseChangeEvent) => {
    dispatch(queueListActions.onChangeOrderQueueList(e.value));
  };

  return (
    <div className="d-flex align-items-center">
      <p className="fw-500 color-grey mr-4">{t('txt_order_by')}:</p>
      <DropdownList
        id="DropdownOrderByQueueList"
        textField="description"
        variant={'no-border'}
        onChange={handleChange}
        value={valueDropdown}
        name="orderBy"
        dataTestId={`${QueueListOrderByTestId}__DropdownOrderByQueueList`}
        noResult={t('txt_no_results_found')}
      >
        {ORDER_DROPDOWN.map(item => (
          <DropdownList.Item
            key={item.value}
            label={t(item.description)}
            value={item}
          />
        ))}
      </DropdownList>
    </div>
  );
};
