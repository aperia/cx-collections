import * as searchValidator from './searchValidation';

const translateFn = (text: string) => text;

describe('attrValidations', () => {
  it('should run validateQueueId without value', () => {
    const result = searchValidator.validateQueueId(
      {
        queueId: { value: '' }
      },
      translateFn
    );
    expect(result).toEqual({ queueId: 'txt_queue_id_is_required' });
  });

  it('should run validateQueueName with value', () => {
    const result = searchValidator.validateQueueName(
      {
        queueName: { value: '' }
      },
      translateFn
    );
    expect(result).toEqual({ queueName: 'txt_queue_name_is_required' });
  });

  it('should run attrValidations with value', () => {
    const result = searchValidator.attrValidations(
      {
        queueName: { value: 'smith' }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });
  it('should run attrValidations with value > case 2', () => {
    const result = searchValidator.attrValidations(
      {
        queueName: { value: '' }
      },
      translateFn
    );
    expect(result).toEqual({ queueName: 'txt_queue_name_is_required' });
  });
});
