import { NoCombineField } from "app/_libraries/_dls/components";

export enum QUEUE_LIST {
  DEFAULT_PAGE_NUMBER = 1,
  DEFAULT_PAGE_SIZE = 10
}

export const PAGE_SIZE = [10, 25, 50];

export const ENHANCE_QUEUE_LIST_DATA = {
  lastWorkDate: '01/22/2022',
  assignedTo: 'Nate Nash',
  totalNumbersOfAccount: '4',
  totalOutstandingBalanceAmount: '9000000',
  collectorPromiseStats: {
    brokenAmount: '0.00',
    brokenPromises: '0',
    keptAmount: '70.00',
    keptPromises: '3',
    pendingAmount: '12.00',
    pendingPromises: '1',
    totalAmount: '82.00',
    totalPromises: '4'
  },
  collectorQueueStats: {
    accountsWorked: '2',
    amountCollected: '100.00',
    totalAccounts: '5',
    totalAmount: '200'
  }
};

export const ATTRIBUTE_QUEUE_SEARCH_FIELD = {
  QUEUE_ID: 'queueId',
  QUEUE_NAME: 'queueName'
};

export const FIELD_QUEUE_NOT_CONSTRAINT: string[] = [
  ATTRIBUTE_QUEUE_SEARCH_FIELD.QUEUE_ID,
  ATTRIBUTE_QUEUE_SEARCH_FIELD.QUEUE_NAME
];

export const NO_COMBINE_FIELDS_QUEUE: NoCombineField[] = [
  {
    field: ATTRIBUTE_QUEUE_SEARCH_FIELD.QUEUE_ID,
    filterMessage: "Queue ID cannot be combined with other parameters."
  },
  {
    field: ATTRIBUTE_QUEUE_SEARCH_FIELD.QUEUE_NAME,
    filterMessage: "Queue Name cannot be combined with other parameters."
  },
];