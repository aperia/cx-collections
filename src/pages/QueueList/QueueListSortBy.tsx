import React, { useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// components
import {
  DropdownBaseChangeEvent,
  DropdownList
} from 'app/_libraries/_dls/components';

// redux
import { queueListActions } from './_redux/reducers';
import { selectedQueueListSortBy } from './_redux/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants & types
import { DROPDOWN_SORT_QUEUE_LIST } from 'app/constants';
import { queueListSortByTestId } from './types';

export interface QueueListSortByProps {}

export const QueueListSortBy: React.FC<QueueListSortByProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const value: RefDataValue = useSelector(selectedQueueListSortBy);

  const handleChange = (e: DropdownBaseChangeEvent) => {
    dispatch(queueListActions.onChangeSortQueueList(e.value));
  };

  const valueDropdown = useMemo(
    () => DROPDOWN_SORT_QUEUE_LIST.find(item => item.value === value?.value),
    [value]
  );

  return (
    <div className="d-flex align-items-center mr-24">
      <p className="fw-500 color-grey mr-4">{t('txt_sort_by')}:</p>
      <DropdownList
        id="DropdownSortByQueueList"
        dataTestId={`${queueListSortByTestId}__DropdownSortByQueueList`}
        textField="description"
        variant={'no-border'}
        value={valueDropdown}
        onChange={handleChange}
        noResult={t('txt_no_results_found')}
      >
        {DROPDOWN_SORT_QUEUE_LIST.map(item => (
          <DropdownList.Item
            key={item.value}
            label={t(item.description)}
            value={item}
          />
        ))}
      </DropdownList>
    </div>
  );
};
