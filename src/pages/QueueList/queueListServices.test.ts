import { queueListServices } from './queueListServices';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';
import { QueueListRequest } from './types';

describe('queueListServices', () => {
  describe('getQueueList', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      queueListServices.getQueueList();

      expect(mockService).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      queueListServices.getQueueList();

      expect(mockService).toBeCalledWith(apiUrl.queueList.getQueueList);
    });
  });

  describe('getCollectorQueueList', () => {
    const params: QueueListRequest = {
      common: {
        agent: '$ALL',
        clientNumber: '$ALL',
        system: '$ALL',
        prin: '$ALL',
        privileges: ['CXCOL1'],
        userPrivileges:{
          cspa:["8997-$ALL-$ALL-$ALL"],
          groups:["admin"]
        },
        app: 'cx',
      },  
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      queueListServices.getCollectorQueueList(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      queueListServices.getCollectorQueueList(params);

      expect(mockService).toBeCalledWith(
        apiUrl.queueList.getCollectorQueueList,
        params
      );
    });
  });
});
