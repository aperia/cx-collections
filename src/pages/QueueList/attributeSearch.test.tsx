
import React from 'react';
import * as attributeSearch from './attributeSearch';
import { render, screen } from '@testing-library/react';


HTMLCanvasElement.prototype.getContext = jest.fn();

describe('getAttrSearchQueueList', () => {
    it('should run queueIdField success', () => {
        const result = attributeSearch.getAttrSearchQueueList(jest.fn());
        const Component = result.data[0].component;
        render(<Component />);
        expect(screen.getByRole('textbox')).toBeInTheDocument();
      });

      it('should run queueNameField success', () => {
        const result = attributeSearch.getAttrSearchQueueList(jest.fn());
        const Component = result.data[1].component;
        render(<Component />);
        expect(screen.getByRole('textbox')).toBeInTheDocument();
      });
});
