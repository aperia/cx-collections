import { ATTRIBUTE_QUEUE_SEARCH_FIELD } from './constants';
import { AttributeSearchQueueListModel } from './types';

export const validateQueueId = (
  data: AttributeSearchQueueListModel,
  translateFn: Function
) => {
  if (data.queueId && !data.queueId?.value) {
    return {
      [ATTRIBUTE_QUEUE_SEARCH_FIELD.QUEUE_ID]: translateFn(
        'txt_queue_id_is_required'
      )
    };
  }
};

export const validateQueueName = (
  data: AttributeSearchQueueListModel,
  translateFn: Function
) => {
  if (data.queueName && !data.queueName.value) {
    return {
      [ATTRIBUTE_QUEUE_SEARCH_FIELD.QUEUE_NAME]: translateFn(
        'txt_queue_name_is_required'
      )
    };
  }
};

export const attrValidations = (
  data: AttributeSearchQueueListModel,
  translateFn: Function
) => {
  const validateList = [validateQueueId, validateQueueName];
  const runAllValidates = validateList.map(fn => {
    return fn(data, translateFn);
  }, []);

  const errors: Record<string, string> = runAllValidates.reduce(
    (accumulator, current) => {
      if (!current) return accumulator;
      return {
        ...accumulator,
        ...current
      };
    },
    {}
  ) as Record<string, string>;
  return Object.keys(errors).length ? errors : undefined;
};
