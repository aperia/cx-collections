import { AttributeSearchValue } from "app/_libraries/_dls/components";
import { AttrCommonModel } from "pages/AccountSearch/types";

export interface QueueListState {
  data?: MagicKeyValue;
  loading?: boolean;
  error?: string;
  page?: number;
  pageSize?: number;
  sortBy: RefDataValue;
  orderBy: RefDataValue;
  searchBy: AttributeSearchValue[];
  queueType: QueueType;
  prevQueueData?: MagicKeyValue;
}

export interface GetQueueListPayload {
  data?: MagicKeyValue;
}

export interface QueueProps {
  queueName: string;
  collectorQueueStats: MagicKeyValue;
  collectorPromiseStats: MagicKeyValue;
}

export interface userPrivileges {
  cspa: string[],
  groups: string[]
}

export interface QueueListRequest {
  common: {
    agent: string;
    clientNumber: string;
    system: string;
    prin: string;
    privileges: string[];
    userPrivileges: userPrivileges;
    app: string;
  };
}

// export type AttrCommonModel = FieldType<string>;

export interface AttributeSearchQueueListModel {
  queueId?: AttrCommonModel;
  queueName?: AttrCommonModel;
}
export interface QueueType {
  pull: boolean;
  push: boolean;
}

export const QueueListOrderByTestId = 'queueListOrderBy';

export const queueListSortByTestId = 'queueListSortBy';