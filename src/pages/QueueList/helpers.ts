import { isEmpty } from 'lodash';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { orderBy } from 'lodash';
import { QueueType } from './types';
export const filterAndOrderQueueList = (
  data: MagicKeyValue,
  sort: string,
  order: string,
  searchBy: AttributeSearchValue[],
  queueType: QueueType
) => {
  let searchData = [];
  if (isEmpty(data) || !data.length) return [];
  if (searchBy?.length) {
    const { key, value } = searchBy[0];
    searchData = data.filter((r: MagicKeyValue) => {
      if (key === 'queueId') return r.queueId === value;
      return r.queueName.includes(value);
    });
    if (searchData.length)
      return orderBy(searchData, sort, order as 'asc' | 'desc');
    return [];
  }
  const filterQueue = data.filter((r: MagicKeyValue) => {
    if (queueType.pull && queueType.push) return true;
    if (queueType.pull) return r.queueType === 'Pull';
    if (queueType.push) return r.queueType === 'Push';
    return false;
  });

  return orderBy(filterQueue, sort, order as 'asc' | 'desc');
};

export const randomDateFromToDay = (start: Date) => {
  const today = new Date();
  return new Date(
    start.getTime() + Math.random() * (today.getTime() - start.getTime())
  ).toString();
};
