import { createSelector } from '@reduxjs/toolkit';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { filterAndOrderQueueList } from '../helpers';
import { QueueType } from '../types';

const getQueueList = (states: RootState) => states.queueList;

const getSortBy = (states: RootState) => states.queueList?.sortBy;

const getOrderBy = (states: RootState) => states.queueList?.orderBy;

const getSearchBy = (states: RootState) => states.queueList?.searchBy;

const getQueueType = (states: RootState) => states.queueList?.queueType;

export const selectedQueueListLoading = createSelector(
  getQueueList,
  data => data?.loading
);

export const selectedQueueListError = createSelector(
  getQueueList,
  data => data?.error
);

export const selectedQueueList = createSelector(
  getQueueList,
  getSortBy,
  getOrderBy,
  getSearchBy,
  getQueueType,
  (data: MagicKeyValue, sortBy: RefDataValue, orderBy: RefDataValue, searchBy: AttributeSearchValue[], queueType: QueueType) =>
    {
      return filterAndOrderQueueList(
        data?.data?.queueDetails,
        sortBy?.value,
        orderBy?.value,
        searchBy,
        queueType
      )
    }
);

export const selectedQueueListSortBy = createSelector(
  getQueueList,
  data => data?.sortBy
);

export const selectedQueueListOrderBy = createSelector(
  getQueueList,
  data => data?.orderBy
);

export const selectedQueueType = createSelector(
  getQueueList,
  data => data?.queueType
);

export const selectedPrevQueueList = createSelector(
  getQueueList,
  data => data?.prevQueueData
);