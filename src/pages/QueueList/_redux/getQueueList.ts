import { mappingDataFromObj } from './../../../app/helpers/mappingData';
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Type
import {
  GetQueueListPayload,
  QueueListRequest,
  QueueListState
} from '../types';

// Service
import { queueListServices } from './../queueListServices';
import { ENHANCE_QUEUE_LIST_DATA } from '../constants';
import { randomDateFromToDay } from '../helpers';
import { formatTime } from 'app/helpers';
import { randomQueueType } from 'pages/AccountSearch/Home/Dashboard/helpers';

// constants
import queueList from '../__mock__/queueList.json';

export const getQueueList = createAsyncThunk<
  GetQueueListPayload,
  undefined,
  ThunkAPIConfig
>('queueList/getQueueList', async (args, thunkAPI) => {
  const mockQueueList = JSON.parse(JSON.stringify(queueList));

  const { mapping, accessConfig } = thunkAPI.getState();

  const { app } = window?.appConfig?.commonConfig;

  const mappingData = mapping.data.dashboardQueueList;
  const { privileges, isSupervisor } = window.appConfig?.commonConfig;
  const [clientNumber = ''] =
    accessConfig?.cspaList?.[0]?.cspa?.split('-') || [];

  const isCXCOL5 = privileges?.[0] === 'CXCOL5';

  const requestBody: QueueListRequest = {
    common: {
      clientNumber,
      agent: '$ALL',
      system: '$ALL',
      prin: '$ALL',
      privileges,
      userPrivileges: {
        cspa: [`${clientNumber}-$ALL-$ALL-$ALL`],
        groups: ['admin']
      },
      app
    }
  };

  if (isSupervisor) {
    const { data: corePlusData } = await queueListServices.getQueueList();
    const formatCorePlusData = mappingDataFromObj(
      corePlusData,
      mappingData || {}
    );
    // Combine mock data
    const combineData = formatCorePlusData.queueDetails.map((item: any) => ({
      ...item,
      ...ENHANCE_QUEUE_LIST_DATA,
      queueType: randomQueueType(Math.round(Math.random())),
      lastWorkDate: formatTime(randomDateFromToDay(new Date(2021, 0, 1))).date,
      totalOutstandingBalanceAmount: Math.random() * 1000000
    }));
    formatCorePlusData.queueDetails = [...combineData];

    return { data: formatCorePlusData };
  }

  // CXCOL5 => Mock data from json to show core plus view
  if (isCXCOL5) {
    const combineData = mockQueueList.queueDetails.map((item: any) => ({
      ...item,
      ...ENHANCE_QUEUE_LIST_DATA,
      queueType: randomQueueType(Math.round(Math.random())),
      lastWorkDate: formatTime(randomDateFromToDay(new Date(2021, 0, 1))).date,
      totalOutstandingBalanceAmount: Math.random() * 1000000
    }));
    mockQueueList.queueDetails = [...combineData];
    return {
      data: mockQueueList
    };
  }

  const { data: coreData } = await queueListServices.getCollectorQueueList(
    requestBody
  );

  const formatCoreData = mappingDataFromObj(coreData, mappingData || {});
  // Combine mock data
  const combineData = formatCoreData.queueDetails.map((item: any) => ({
    ...item,
    ...ENHANCE_QUEUE_LIST_DATA,
    queueType: randomQueueType(Math.round(Math.random())),
    lastWorkDate: formatTime(randomDateFromToDay(new Date(2021, 0, 1))).date,
    totalOutstandingBalanceAmount: Math.random() * 1000000
  }));
  formatCoreData.queueDetails = [...combineData];

  return { data: formatCoreData };
});

export const getQueueListBuilder = (
  builder: ActionReducerMapBuilder<QueueListState>
) => {
  builder.addCase(getQueueList.pending, (draftState, action) => {
    draftState.loading = true;
  });
  builder.addCase(getQueueList.fulfilled, (draftState, action) => {
    const payload = action.payload;

    draftState.data = payload?.data;
    draftState.prevQueueData = payload?.data;

    draftState.loading = false;
  });
  builder.addCase(getQueueList.rejected, (draftState, action) => {
    draftState.loading = false;
    draftState.error = action.payload?.errorMessage;
  });
};
