import { createStore, Store } from '@reduxjs/toolkit';

import { getQueueList } from './getQueueList';
import { responseDefault } from 'app/test-utils';
import { queueListServices } from '../queueListServices';

import { rootReducer } from 'storeConfig';

let spy: jest.SpyInstance;
let store: Store<RootState>;

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    org: 'org',
    app: 'app'
  }
};

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

const mockQueueList = [
  {
    accessLevel: 'UPDATE',
    agentId: '$ALL',
    ascendingOrder: false,
    assignedTo: 'Nate Nash',
    autoUserAssign: false,
    clientId: '8997',
    collectorPromiseStats: {
      brokenAmount: '0.00',
      brokenPromises: '0',
      keptAmount: '70.00',
      keptPromises: '3',
      pendingAmount: '12.00',
      pendingPromises: '1',
      totalAmount: '82.00',
      totalPromises: '4'
    },
    collectorQueueStats: {
      accountsWorked: '2',
      amountCollected: '100.00',
      totalAccounts: '5',
      totalAmount: '200'
    },
    isAscendingOrder: false,
    lastWorkDate: '07/27/2021',
    principleId: '$ALL',
    queueGroupIdOwner: 'FD - ADMIN',
    queueId: '163299977456',
    queueName: 'NonVerifiedDeceased',
    queueType: 'Pull',
    sortWith: 'CURR_BALANCE',
    systemId: '$ALL',
    totalNumbersOfAccount: '4',
    totalOutstandingBalanceAmount: 455761.58749992633,
    userId: 'NSA1'
  }
];

describe('should have test getQueueList', () => {
  it('success response', async () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: { commonConfig: { org: 'org', app: 'app' } }
    });

    jest.spyOn(queueListServices, 'getQueueList').mockResolvedValue({
      ...responseDefault,
      data: {
        queueIds: mockQueueList,
        status: 'success',
        success: true
      }
    });
    jest.spyOn(queueListServices, 'getCollectorQueueList').mockResolvedValue({
      ...responseDefault,
      data: { queueIds: mockQueueList, status: 'success', success: true }
    });

    store = createStore(rootReducer, {
      mapping: {
        data: {
          dashboardQueueList: {
            queueDetails: 'queueIds'
          }
        },
        loading: false
      }
    } as any);

    await getQueueList()(store.dispatch, store.getState, {});

    expect(store.getState().queueList.data).not.toEqual([]);
  });

  it('success response > case 2', async () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: { commonConfig: { org: 'org', app: 'app' } }
    });

    jest.spyOn(queueListServices, 'getQueueList').mockResolvedValue({
      ...responseDefault,
      data: {
        queueIds: mockQueueList,
        status: 'success',
        success: true
      }
    });
    jest.spyOn(queueListServices, 'getCollectorQueueList').mockResolvedValue({
      ...responseDefault,
      data: { queueIds: mockQueueList, status: 'success', success: true }
    });

    store = createStore(rootReducer, {
      mapping: {
        data: {
          dashboardQueueList: undefined
        },
        loading: false
      }
    } as any);

    await getQueueList()(store.dispatch, store.getState, {});

    expect(store.getState().queueList.data).toEqual({
      queueDetails: []
    });
  });

  it('success response with isSupervisor', async () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: { commonConfig: { isSupervisor: true, org: 'org', app: 'app' } }
    });

    const spyGetQueueList = jest
      .spyOn(queueListServices, 'getQueueList')
      .mockResolvedValue({
        ...responseDefault,
        data: { queueIds: mockQueueList, status: 'success', success: true }
      });

    store = createStore(rootReducer, {
      mapping: {
        data: {
          dashboardQueueList: {
            queueDetails: 'queueIds'
          }
        },
        loading: false
      }
    } as any);

    await getQueueList()(store.dispatch, store.getState, {});

    expect(spyGetQueueList).toHaveBeenCalled();
  });

  it('success response with isSupervisor > case 2', async () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: { commonConfig: { isSupervisor: true, org: 'org', app: 'app' } }
    });

    const spyGetQueueList = jest
      .spyOn(queueListServices, 'getQueueList')
      .mockResolvedValue({
        ...responseDefault,
        data: { queueIds: mockQueueList, status: 'success', success: true }
      });

    store = createStore(rootReducer, {
      mapping: {
        data: {
          dashboardQueueList: undefined
        },
        loading: false
      }
    } as any);

    await getQueueList()(store.dispatch, store.getState, {});

    expect(spyGetQueueList).toHaveBeenCalled();
  });

  it('success response with CXCOL5', async () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: {
        commonConfig: {
          isSupervisor: false,
          org: 'org',
          app: 'app',
          privileges: ['CXCOL5']
        }
      }
    });

    const spyGetQueueList = jest
      .spyOn(queueListServices, 'getQueueList')
      .mockResolvedValue({
        ...responseDefault,
        data: { queueIds: mockQueueList, status: 'success', success: true }
      });

    store = createStore(rootReducer, {
      mapping: {
        data: {
          dashboardQueueList: {
            queueDetails: 'queueIds'
          }
        },
        loading: false
      }
    } as any);

    await getQueueList()(store.dispatch, store.getState, {});

    expect(spyGetQueueList).toBeCalledTimes(0);
  });

  it('failed response', async () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: { commonConfig: { org: 'org', app: 'app' } }
    });

    jest.spyOn(queueListServices, 'getQueueList').mockRejectedValue({});
    jest
      .spyOn(queueListServices, 'getCollectorQueueList')
      .mockRejectedValue({});
    store = createStore(rootReducer, {
      mapping: {
        data: {
          dashboardQueueList: {
            queueDetails: 'queueIds'
          }
        },
        loading: false
      }
    } as any);
    await getQueueList()(store.dispatch, store.getState, {});
    expect(store.getState().queueList.loading).toEqual(false);
  });
});
