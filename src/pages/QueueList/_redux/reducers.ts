import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';

// types
import { QueueListState } from '../types';

// thunks
import { getQueueList, getQueueListBuilder } from './getQueueList';

const { actions, reducer } = createSlice({
  name: 'queueList',
  initialState: {
    data: {
      queueDetails: []
    },
    prevQueueData: {
      queueDetails: []
    },
    loading: false,
    error: '',
    sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
    orderBy: { description: 'Descending', value: 'desc' },
    searchBy: [],
    queueType: { pull: true, push: true }
  } as QueueListState,
  reducers: {
    toggleLoading: draftState => {
      draftState.loading = !draftState.loading;
    },
    onChangeSortQueueList: (
      draftState,
      action: PayloadAction<RefDataValue>
    ) => {
      draftState.sortBy = action.payload;
    },
    onChangeOrderQueueList: (
      draftState,
      action: PayloadAction<RefDataValue>
    ) => {
      draftState.orderBy = action.payload;
    },
    onChangeSearchValue: (
      draftState,
      action: PayloadAction<AttributeSearchValue[]>
    ) => {
      draftState.searchBy = action.payload;
    },
    onChangeQueueTypePull: draftState => {
      draftState.queueType['pull'] = !draftState.queueType['pull'];
    },
    onChangeQueueTypePush: draftState => {
      draftState.queueType['push'] = !draftState.queueType['push'];
    },
    resetPreviousQueueList: draftState => {
      draftState.data = draftState.prevQueueData;
      draftState.loading = false;
      draftState.error = '';
      draftState.sortBy = {
        description: 'Last Work Date',
        value: 'lastWorkDate'
      };
      draftState.orderBy = { description: 'Descending', value: 'desc' };
      draftState.searchBy = [];
      draftState.queueType = { pull: true, push: true };
    }
  },

  extraReducers: builder => {
    getQueueListBuilder(builder);
  }
});

const queueListActions = {
  ...actions,
  getQueueList
};

export { queueListActions, reducer as queueList };
