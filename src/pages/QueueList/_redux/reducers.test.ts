import { convertToExpectedKeys } from 'app/test-utils';
import { AnyAction } from 'redux';
import { QueueListState } from '../types';
import { queueList as reducer, queueListActions } from './reducers';

const mockQueueList = [
  {
    lastWorkDate: '07/27/2021',
    queueId: '163299977456',
    queueName: 'NonVerifiedDeceased',
    queueType: 'Pull'
  }
];

const initialState = {
  data: {
    queueDetails: mockQueueList
  },
  prevQueueData: {
    queueDetails: mockQueueList
  },
  loading: false,
  error: '',
  sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
  orderBy: { description: 'Descending', value: 'desc' },
  searchBy: [],
  queueType: { pull: true, push: true }
} as QueueListState;

const testCases = [
  {
    reducerName: 'toggleLoading',
    payload: undefined,
    expected: {
      toExpectedKeys: {},
      removeKeys: []
    }
  },
  {
    reducerName: 'getQueueList',
    payload: undefined,
    expected: {
      data: {
        queueDetails: ['queueId1', 'queueId2']
      },
      toExpectedKeys: {},
      removeKeys: []
    }
  },
  {
    reducerName: 'onChangeQueueTypePull',
    payload: undefined,
    expected: {}
  },
  {
    reducerName: 'onChangeQueueTypePush',
    payload: undefined,
    expected: {
      toExpectedKeys: {},
      removeKeys: []
    }
  }
];

describe.each(testCases)(
  'Test queueList',
  ({ reducerName, payload, expected }) => {
    it(`${reducerName}`, () => {
      // Given
      const actionCreator =
        queueListActions[reducerName as keyof typeof queueListActions];
      const action = actionCreator() as AnyAction;

      // When
      const state = reducer(initialState, action);

      // Then
      if (!expected) return expect(state).toEqual(expected);

      const { toExpectedKeys = {}, removeKeys = [] } = expected;

      const expectedResult = convertToExpectedKeys({
        payload,
        mappingRule: toExpectedKeys,
        removeKeys
      });

      expect(state?.data || state).toEqual(
        expect.objectContaining(expectedResult)
      );
    });
  }
);

describe('test reducers', () => {
  it('onChangeSortQueueList', () => {
    const state = reducer(
      initialState,
      queueListActions.onChangeSortQueueList({
        value: 'lastWorkDate',
        description: 'Last Work Date'
      })
    );

    expect(state.sortBy).toEqual({
      description: 'Last Work Date',
      value: 'lastWorkDate'
    });
  });

  it('onChangeOrderQueueList', () => {
    const state = reducer(
      initialState,
      queueListActions.onChangeOrderQueueList({
        value: 'asc',
        description: 'Ascending'
      })
    );

    expect(state.orderBy).toEqual({
      description: 'Ascending',
      value: 'asc'
    });
  });

  it('onChangeSearchValue', () => {
    const state = reducer(
      initialState,
      queueListActions.onChangeSearchValue([
        {
          key: 'queueId',
          value: '123'
        }
      ])
    );

    expect(state.searchBy).toEqual([{ key: 'queueId', value: '123' }]);
  });

  it('resetPreviousQueueList', () => {
    const state = reducer(
      initialState,
      queueListActions.resetPreviousQueueList()
    );

    expect(state.data).not.toEqual([]);
  });
});
