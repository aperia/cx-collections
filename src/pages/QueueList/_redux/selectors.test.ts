import { selectorWrapper } from 'app/test-utils';
import * as selector from './selectors';

const mockData = [
  {
    queueId: '004',
    description: 'bankruptcy',
    primaryName: 'SMITH,ADELE               ',
    memberSequenceIdentifier: '001',
    socialSecurityIdentifier:
      '{"maskedValue":"***-**-1111","eValue":"VOL(bGl2RGN1bmskIzlSLiMo)"}',
    presentationInstrumentIdentifier:
      '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
    accountWorked: '180',
    totalAccountWorked: '680',
    amountCollected: '24032',
    totalAmountCollected: '59092',
    promiseStatistics: [
      {
        name: 'pending promises',
        promises: '304',
        amount: '35900'
      },
      {
        name: 'kept promises',
        promises: '220',
        amount: '20840'
      },
      {
        name: 'broken promises',
        promises: '159',
        amount: '2352'
      },
      {
        name: 'total promises',
        promises: '680',
        amount: '59092'
      }
    ]
  }
];

describe('redux-store > queue-list > selector', () => {
  const store: Partial<RootState> = {
    queueList: {
      data: { queueDetails: mockData },
      prevQueueData: { queueDetails: mockData },
      error: 'queueList_error_msg',
      loading: false,
      sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
      orderBy: { description: 'Descending', value: 'desc' },
      searchBy: [],
      queueType: { pull: true, push: true }
    }
  };

  it('selectedQueueList', () => {
    const { data } = selectorWrapper(store)(selector.selectedQueueList);

    expect(data).toEqual([
      {
        accountWorked: '180',
        amountCollected: '24032',
        description: 'bankruptcy',
        memberSequenceIdentifier: '001',
        presentationInstrumentIdentifier:
          '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
        primaryName: 'SMITH,ADELE               ',
        promiseStatistics: [
          {
            amount: '35900',
            name: 'pending promises',
            promises: '304'
          },
          {
            amount: '20840',
            name: 'kept promises',
            promises: '220'
          },
          {
            amount: '2352',
            name: 'broken promises',
            promises: '159'
          },
          {
            amount: '59092',
            name: 'total promises',
            promises: '680'
          }
        ],
        queueId: '004',
        socialSecurityIdentifier:
          '{"maskedValue":"***-**-1111","eValue":"VOL(bGl2RGN1bmskIzlSLiMo)"}',
        totalAccountWorked: '680',
        totalAmountCollected: '59092'
      }
    ]);
  });

  it('selectedQueueListError', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selector.selectedQueueListError
    );

    expect(data).toEqual('queueList_error_msg');
    expect(emptyData).toBeUndefined();
  });

  it('selectedQueueListLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selector.selectedQueueListLoading
    );

    expect(data).toEqual(false);
    expect(emptyData).toBeUndefined();
  });

  it('selectedQueueListSortBy', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selector.selectedQueueListSortBy
    );

    expect(data).toEqual({
      description: 'Last Work Date',
      value: 'lastWorkDate'
    });
    expect(emptyData).toBeUndefined();
  });

  it('selectedQueueListOrderBy', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selector.selectedQueueListOrderBy
    );

    expect(data).toEqual({ description: 'Descending', value: 'desc' });
    expect(emptyData).toBeUndefined();
  });

  it('selectedQueueType', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selector.selectedQueueType
    );

    expect(data).toEqual({ pull: true, push: true });
    expect(emptyData).toBeUndefined();
  });

  it('selectedPrevQueueList', () => {
    const { data } = selectorWrapper(store)(selector.selectedPrevQueueList);

    expect(data?.queueDetails).toEqual([
      {
        accountWorked: '180',
        amountCollected: '24032',
        description: 'bankruptcy',
        memberSequenceIdentifier: '001',
        presentationInstrumentIdentifier:
          '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
        primaryName: 'SMITH,ADELE               ',
        promiseStatistics: [
          {
            amount: '35900',
            name: 'pending promises',
            promises: '304'
          },
          {
            amount: '20840',
            name: 'kept promises',
            promises: '220'
          },
          {
            amount: '2352',
            name: 'broken promises',
            promises: '159'
          },
          {
            amount: '59092',
            name: 'total promises',
            promises: '680'
          }
        ],
        queueId: '004',
        socialSecurityIdentifier:
          '{"maskedValue":"***-**-1111","eValue":"VOL(bGl2RGN1bmskIzlSLiMo)"}',
        totalAccountWorked: '680',
        totalAmountCollected: '59092'
      }
    ]);
  });
});
