import React from 'react';
import { screen } from '@testing-library/react';

// components
import ViewResult from './ViewResult';

// utils
import { storeId, renderMockStoreId } from 'app/test-utils';
import { HARDSHIP_TYPE_ID } from '../constants';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);
jest.mock('./LongTermHardship', () => () => (
  <div>LongTermHardShipViewResult</div>
));
jest.mock('./MilitaryDeploymentResult', () => () => (
  <div>MilitaryDeploymentResult</div>
));
jest.mock('./NaturalDisaster', () => () => <div>NaturalDisaster</div>);

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<ViewResult />, { initialState });
};

describe('Render', () => {
  it('Render when ewmpty state', () => {
    renderWrapper({});

    expect(screen.getByTestId('hardshipDetailViews')).toBeInTheDocument();
  });

  it('Render when typeHardship is longTerm', () => {
    renderWrapper({
      hardship: {
        [storeId]: { data: { typeCode: HARDSHIP_TYPE_ID.longTerm } } as any
      }
    });

    expect(screen.getByText('LongTermHardShipViewResult')).toBeInTheDocument();
  });

  it('Render when typeHardship is militaryDeployment', () => {
    renderWrapper({
      hardship: {
        [storeId]: {
          data: { typeCode: HARDSHIP_TYPE_ID.militaryDeployment }
        } as any
      }
    });

    expect(screen.getByText('MilitaryDeploymentResult')).toBeInTheDocument();
  });

  it('Render when typeHardship is hardship', () => {
    renderWrapper({
      hardship: {
        [storeId]: { data: { typeCode: HARDSHIP_TYPE_ID.hardship } } as any
      }
    });

    expect(screen.getByTestId(`hardshipDetailViews`)).toBeInTheDocument();
  });

  it('Render when typeHardship is naturalDisaster', () => {
    renderWrapper({
      hardship: {
        [storeId]: {
          data: { typeCode: HARDSHIP_TYPE_ID.naturalDisaster }
        } as any
      }
    });

    expect(screen.getByText(`NaturalDisaster`)).toBeInTheDocument();
  });
});
