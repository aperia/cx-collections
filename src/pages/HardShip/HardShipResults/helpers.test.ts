import {
  mapDataToLongTermHardshipViewResult,
  mapDataToMilitaryHardshipViewResult
} from './helpers';

describe('helper', () => {
  it('mapDataToLongTermHardshipViewResult', () => {
    const data = {
      ctdPaymentAmount: 123,
      reAgePaymentPeriod: 'reAgePaymentPeriod'
    };
    const mapping = {
      cycleToDatePaymentsAmount: 'ctdPaymentAmount',
      newFixedPaymentAmount: 'newFixedPaymentAmount',
      nonDelinquentMpdAmount: 'nonDelinquentMinimumPaymentDueAmount',
      reagePaymentPeriod: 'reAgePaymentPeriod.description',
      fromPaymentDate: 'startDate',
      toPaymentDate: 'endDate',
      processStatus: 'processStatus',
      incomeAmount: 'longTermHardship.incomeAmount',
      incomeFrequency: 'longTermHardship.incomeFrequency',
      outgoingInfo: 'longTermHardship.outgoingInfo',
      paymentMethod: 'longTermHardship.paymentMethod',
      totalOutgoing: 'longTermHardship.totalOutgoing'
    };
    const result = mapDataToLongTermHardshipViewResult(data, mapping);

    expect(result.reagePaymentPeriod.description).toEqual(
      `${data.reAgePaymentPeriod} Months`
    );

    expect(result?.cycleToDatePaymentsAmount).toEqual(data.ctdPaymentAmount);
  });

  it('mapDataToMilitaryHardshipViewResult', () => {
    const data = {
      newInterestRate: 12,
      ctdPaymentAmount: 123,
      reAgePaymentPeriod: 'reAgePaymentPeriod'
    };
    const mapping = {
      newInterestRate: 'newInterestRate',
      militaryDeploymentHardship: 'militaryDeploymentHardship',
      deploymentStartDate: 'militaryDeploymentHardship.deploymentStartDate',
      deploymentEndDate: 'militaryDeploymentHardship.deploymentEndDate',
      countryOfDeployment: 'militaryDeploymentHardship.deploymentCountry',
      cycleToDatePaymentsAmount: 'ctdPaymentAmount',
      newFixedPaymentAmount: 'newFixedPaymentAmount',
      nonDelinquentMpdAmount: 'nonDelinquentMinimumPaymentDueAmount',
      'reagePaymentPeriod.value': 'reAgePaymentPeriod',
      fromPaymentDate: 'startDate',
      toPaymentDate: 'endDate',
      processStatus: 'processStatus'
    };
    let result = mapDataToMilitaryHardshipViewResult(data, mapping);

    expect(result.reagePaymentPeriod.description).toEqual(
      `${data.reAgePaymentPeriod} Months`
    );
    expect(result?.cycleToDatePaymentsAmount).toEqual(data.ctdPaymentAmount);
    expect(result?.newInterestRate).toEqual(data.newInterestRate);

    result = mapDataToMilitaryHardshipViewResult(
      { ...data, newInterestRate: undefined },
      mapping
    );

    expect(result?.newInterestRate).toEqual(6);
  });
});
