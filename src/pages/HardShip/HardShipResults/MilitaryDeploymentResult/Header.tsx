import React, { Fragment } from 'react';

// Hooks
import { useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// i18n/constant
import { selectHardshipInfo } from 'pages/HardShip/_redux/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';

const CallResultMilitaryHeader: React.FC = () => {
  const { t } = useTranslation();
  const mainInfo = useStoreIdSelector<MagicKeyValue>(selectHardshipInfo);
  const testId = 'callResultMilitaryHeader';

  return (
    <Fragment>
      <div className="d-flex justify-content-between align-items-center my-16">
        <p
          className=" fs-14 fw-500 color-grey"
          data-testid={genAmtId(testId, 'main-information', '')}
        >
          {t('txt_main_information')}
        </p>
      </div>
      <div className="form-group-static">
        <span
          className="form-group-static__label"
          data-testid={genAmtId(testId, 'type-label', '')}
        >
          {t('txt_type')}
        </span>
        <span
          className="form-group-static__text"
          data-testid={genAmtId(testId, 'type-value', '')}
        >
          {mainInfo?.typeHardship}
        </span>
      </div>
      <div className="divider-dashed mt-24 mb-16"></div>
    </Fragment>
  );
};

export default CallResultMilitaryHeader;
