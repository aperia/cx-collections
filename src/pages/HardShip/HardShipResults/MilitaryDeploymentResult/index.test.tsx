import React from 'react';
import { screen } from '@testing-library/react';

// components
import MilitaryDeploymentResult from './';

// utils
import { storeId, mockActionCreator, renderMockStoreId } from 'app/test-utils';
import { HARDSHIP_TYPE } from '../../constants';
import { hardshipActions } from '../../_redux/reducers';
import { DISABLED_VALUE } from 'app/components/_dof_controls/custom-functions/hardshipGroupCountryOfDeploymentChange';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import userEvent from '@testing-library/user-event';

jest.mock('app/_libraries/_dls/hooks', () => {
  const hooks = jest.requireActual('app/_libraries/_dls/hooks');

  return {
    ...hooks,
    useTranslation: () => ({ t: (text: string) => text })
  };
});

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const hardshipActionsSpy = mockActionCreator(hardshipActions);

const generateState = (
  data?: MagicKeyValue,
  groupCheckboxCountryDeployment?: MagicKeyValue[],
  loading?: boolean,
  others?: Partial<RootState>
) => {
  const state: Partial<RootState> = {
    collectionForm: {
      [storeId]: {
        method: 'edit',
        collectionCurrentData: {
          dateNoticeReceivedMainSecond: 'dateNoticeReceivedMainSecond'
        }
      } as any
    },
    hardship: {
      [storeId]: {
        data,
        typeForm: HARDSHIP_TYPE[2],
        commonData: {
          cycleToDatePaymentsAmount: '3',
          newFixedPaymentAmount: '2',
          nonDelinquentMpdAmount: '1',
          minimumNewFixedPaymentAmount: '1'
        },
        militaryDeployment: {
          groupCheckboxCountryDeployment,
          loading
        }
      } as any
    },
    ...others
  };
  return state;
};

const renderWrapper = (initialState?: Partial<RootState>) => {
  return renderMockStoreId(<MilitaryDeploymentResult />, { initialState });
};

describe('Test MilitaryDeploymentResult component', () => {
  it('Render UI DISABLED_VALUE', async () => {
    const getGroupCheckboxCountryDeployment = hardshipActionsSpy(
      'getGroupCheckboxCountryDeployment'
    );

    const mainInfoDisabled = {
      groupCountryOfDeployment: DISABLED_VALUE,
      countryOfDeployment: {}
    };

    renderWrapper(generateState(mainInfoDisabled));

    expect(getGroupCheckboxCountryDeployment).toHaveBeenCalled();
    expect(screen.getByText('txt_countryOfDeployment')).toBeInTheDocument();
  });

  it('Render UI', async () => {
    const getGroupCheckboxCountryDeployment = hardshipActionsSpy(
      'getGroupCheckboxCountryDeployment'
    );
    const groupCheckboxCountryDeployment = [
      {
        label: 'label',
        value: 'value'
      }
    ];
    renderWrapper(generateState(undefined, groupCheckboxCountryDeployment));

    expect(getGroupCheckboxCountryDeployment).toHaveBeenCalled();
    expect(screen.getByText('txt_countryOfDeployment')).toBeInTheDocument();
  });

  it('Render UI not DISABLED_VALUE', async () => {
    const getGroupCheckboxCountryDeployment = hardshipActionsSpy(
      'getGroupCheckboxCountryDeployment'
    );
    const mainInfoNotDisabled = {
      groupCountryOfDeployment: 'other',
      countryOfDeployment: { description: 'description' }
    };

    renderWrapper(generateState(mainInfoNotDisabled, undefined, true));

    expect(getGroupCheckboxCountryDeployment).toHaveBeenCalled();
    expect(
      screen.queryByText('txt_countryOfDeployment')
    ).not.toBeInTheDocument();
  });

  it('Render hasPermissionUpload and switch tab', async () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        isCollector: true
      } as CommonConfig
    };

    const getGroupCheckboxCountryDeployment = hardshipActionsSpy(
      'getGroupCheckboxCountryDeployment'
    );

    window.getComputedStyle = () =>
      ({
        width: '20px'
      } as any);
    const mainInfoNotDisabled = {
      groupCountryOfDeployment: 'other',
      countryOfDeployment: 'test'
    };

    const { wrapper } = renderWrapper(
      generateState(mainInfoNotDisabled, undefined, false, {})
    );

    const fileIcon = queryByClass(wrapper.container, /icon icon-file/);
    userEvent.click(fileIcon!);
    expect(getGroupCheckboxCountryDeployment).toHaveBeenCalled();
  });
});
