import { createSelector } from 'reselect';

export const getMilitaryTab = (state: RootState, storeId: string) =>
  state.hardship[storeId]?.resultHardship?.selectedMilitaryTab;

export const getHardshipDocuments = (state: RootState, storeId: string) =>
  state.collectionForm[storeId]?.caseDocumentFiles?.hardShip;

export const selectHardshipFiles = createSelector(
  getHardshipDocuments,
  data => data?.files
);
export const selectHardshipFilesLoading = createSelector(
  getHardshipDocuments,
  data => data?.loading
);

export const selectHardshipFilesError = createSelector(
  getHardshipDocuments,
  data => data?.error
);
