import React from 'react';
import { renderMockStoreId, storeId } from 'app/test-utils';
import HardshipAttachment from './HardshipAttachment';
import { screen } from '@testing-library/dom';
import { mockUseTranslate } from 'app/test-utils/mockUtils';
import { queryByClass } from 'app/_libraries/_dls/test-utils/queryHelpers';
import userEvent from '@testing-library/user-event';

mockUseTranslate();

const renderComponent = () => {
  return renderMockStoreId(<HardshipAttachment />, {
    initialState: {
      collectionForm: {
        [storeId]: {
          caseDocumentFiles: {
            hardShip: {
              files: [
                {
                  documentName: 'searchAccount.png',
                  documentObjectId: '123-456-789'
                }
              ]
            }
          }
        } as any
      }
    }
  });
};

describe('HardshipAttachment', () => {
  it('render UI', () => {
    const { wrapper } = renderComponent();
    userEvent.click(queryByClass(wrapper.container, /icon icon-add-file/)!);
    userEvent.click(queryByClass(wrapper.container, /icon icon-download/)!);

    expect(screen.getByText('txt_order_of_deployment')).toBeInTheDocument();
  });
});
