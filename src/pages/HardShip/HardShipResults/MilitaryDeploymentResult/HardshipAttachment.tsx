import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { collectionFormActions as actions } from 'pages/CollectionForm/_redux/reducers';
import { selectHardshipFiles, selectHardshipFilesLoading } from './selectors';
import classnames from 'classnames';
import DocumentList, { FileDetail } from 'app/components/FileList';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { Button, Icon, Tooltip } from 'app/_libraries/_dls/components';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface Props {}

const HardshipAttachment: React.FC<Props> = ({}) => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();

  const files = useStoreIdSelector<FileDetail[]>(selectHardshipFiles);
  const loading = useStoreIdSelector<boolean>(selectHardshipFilesLoading);
  const testId = 'hardshipAttachment';

  const handleDownloadFile = (file: FileDetail) => {
    dispatch(
      actions.downloadCaseDocumentFile({
        storeId,
        postData: {
          fileName: file?.documentName,
          uuid: file?.documentObjectId
        }
      })
    );
  };

  const handleUpLoadFile = () => {
    dispatch(actions.openUploadFileModal({ storeId, type: 'hardShip' }));
  };

  useEffect(() => {
    dispatch(actions.getListCaseDocument({ storeId, type: 'hardShip' }));
  }, [dispatch, storeId]);

  return (
    <div className={classnames('dls-upload mt-16', { loading })}>
      <div className="d-flex justify-content-between align-items-center my-16">
        <p
          className=" fs-14 fw-500 color-grey"
          data-testid={genAmtId(testId, 'order-of-deployment', '')}
        >
          {t('txt_order_of_deployment')}
        </p>
        <Tooltip
          element={t('txt_upload_order_of_deployment')}
          placement="top"
          variant="primary"
          triggerClassName="mr-n4"
          dataTestId={`${testId}_add-file-tooltip`}
        >
          <Button
            variant="icon-secondary"
            onClick={handleUpLoadFile}
            dataTestId={`${testId}_add-file-btn`}
          >
            <Icon name="add-file" />
          </Button>
        </Tooltip>
      </div>
      <DocumentList
        files={files}
        onDownload={handleDownloadFile}
        dataTestId={`${testId}-list`}
      />
    </div>
  );
};

export default HardshipAttachment;
