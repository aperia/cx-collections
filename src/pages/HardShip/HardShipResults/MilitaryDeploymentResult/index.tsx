import React, { Fragment, useEffect, useMemo } from 'react';

// components
import { View } from 'app/_libraries/_dof/core';
import HardshipAttachment from './HardshipAttachment';
import CallResultMilitaryHeader from './Header';
import {
  HorizontalTabs,
  Icon,
  Tooltip,
  TruncateText
} from 'app/_libraries/_dls/components';

// redux
import { useDispatch } from 'react-redux';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { hardshipActions as actions } from '../../_redux/reducers';
import {
  selectGroupCheckboxCountryDeployment,
  selectMilitaryDeploymentLoading
} from '../../MilitaryDeploymentHardship/selectors';

// helper
import isEmpty from 'lodash.isempty';
import { DISABLED_VALUE } from 'app/components/_dof_controls/custom-functions/hardshipGroupCountryOfDeploymentChange';
import { GroupCheckboxCountryDeployment } from '../../MilitaryDeploymentHardship/types';
import { HardshipResultTab } from './constants';
import { getMilitaryTab } from './selectors';
import { selectHardshipInfo } from 'pages/HardShip/_redux/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface Props {}

const LongTermHardShipViewResult: React.FC<Props> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId, accEValue: accountId } = useAccountDetail();
  const isLoading = useStoreIdSelector<boolean>(
    selectMilitaryDeploymentLoading
  );
  const { isAdminRole, isCollector } = window.appConfig?.commonConfig || {};
  const hasPermissionUpload = isAdminRole || isCollector;
  const listCheckbox = useStoreIdSelector<GroupCheckboxCountryDeployment[]>(
    selectGroupCheckboxCountryDeployment
  );
  const selectedTab = useStoreIdSelector<string>(getMilitaryTab);
  const mainInfo = useStoreIdSelector<MagicKeyValue>(selectHardshipInfo);
  const testId = 'militaryDeploymentResult';

  useEffect(() => {
    dispatch(actions.getGroupCheckboxCountryDeployment({ storeId }));
  }, [accountId, dispatch, storeId]);

  const content = useMemo(() => {
    const { groupCountryOfDeployment, countryOfDeployment } = mainInfo || {};
    const id = groupCountryOfDeployment;
    const label = 'txt_countryOfDeployment';

    let value = '';
    if (
      groupCountryOfDeployment !== DISABLED_VALUE &&
      !isEmpty(countryOfDeployment)
    ) {
      value = countryOfDeployment;
    } else {
      value =
        (listCheckbox || []).find(
          item => item?.value === groupCountryOfDeployment
        )?.label || groupCountryOfDeployment;
    }

    return (
      <div key={value} className="form-group-static mt-16 col-16" id={id}>
        <span id={`${id}__label`} className="form-group-static__label">
          <TruncateText
            id={`${id}__label_text`}
            title={t(label)}
            dataTestId={`${testId}_countryOfDeployment-label`}
          >
            {t(label)}
          </TruncateText>
        </span>
        <div
          id={`${id}__text`}
          className="form-group-static__text"
          data-testid={genAmtId(testId, 'countryOfDeployment-value', '')}
        >
          {t(value)}
        </div>
      </div>
    );
  }, [listCheckbox, mainInfo, t]);

  const formName = `${storeId}-militaryDeploymentHardshipFormDetail`;
  const formNameBasic = `${storeId}-militaryDeploymentHardshipFormDetail1`;

  const handleSelect = (key: any) => {
    dispatch(actions.setActiveTabMilitary({ storeId, value: key }));
  };

  const contentMainInformation = useMemo(() => {
    return (
      <Fragment>
        <CallResultMilitaryHeader />
        <View
          id={formName}
          formKey={formName}
          descriptor="militaryDeploymentHardshipFormDetail"
          value={mainInfo}
        />
        {content}
        <View
          id={formNameBasic}
          formKey={formNameBasic}
          descriptor="militaryDeploymentHardshipFormDetail1"
          value={mainInfo}
        />
      </Fragment>
    );
  }, [content, formName, formNameBasic, mainInfo]);

  if (isLoading) return null;

  if (!hasPermissionUpload) {
    return contentMainInformation;
  }

  return (
    <Fragment>
      <HorizontalTabs
        level2
        unmountOnExit={false}
        onSelect={handleSelect}
        activeKey={selectedTab}
        dataTestId={`${testId}_tab`}
      >
        <HorizontalTabs.Tab
          title={
            <Tooltip
              element={t('txt_main_information')}
              placement="top"
              variant="primary"
            >
              <Icon name="file" size="6x" />
            </Tooltip>
          }
          key={HardshipResultTab.info}
          eventKey={HardshipResultTab.info}
        >
          {contentMainInformation}
        </HorizontalTabs.Tab>
        <HorizontalTabs.Tab
          title={
            <Tooltip
              element={t('txt_order_of_deployment')}
              placement="top"
              variant="primary"
            >
              <Icon name="attachment" size="6x" />
            </Tooltip>
          }
          key={HardshipResultTab.files}
          eventKey={HardshipResultTab.files}
        >
          <HardshipAttachment />
        </HorizontalTabs.Tab>
      </HorizontalTabs>
    </Fragment>
  );
};

export default LongTermHardShipViewResult;
