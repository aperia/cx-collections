import * as selector from './selectors';

// utils
import { selectorWrapper, storeId } from 'app/test-utils';

const states: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      caseDocumentFiles: {
        hardShip: {
          loading: true,
          error: true,
          files: [{ id: 123 }]
        }
      }
    }
  },
  hardship: {
    [storeId]: {
      resultHardship: {
        selectedMilitaryTab: 0
      }
    }
  }
};

const selectorTrigger = selectorWrapper(states);

describe('HardShip > selector', () => {
  it('getMilitaryTab', () => {
    const { data, emptyData } = selectorTrigger(selector.getMilitaryTab);

    expect(data).toEqual(
      states.hardship![storeId].resultHardship?.selectedMilitaryTab
    );
    expect(emptyData).toEqual(undefined);
  });

  it('getHardshipDocuments', () => {
    const { data, emptyData } = selectorTrigger(selector.getHardshipDocuments);

    expect(data).toEqual(
      states.collectionForm![storeId].caseDocumentFiles?.hardShip
    );
    expect(emptyData).toEqual(undefined);
  });

  it('selectHardshipFiles', () => {
    const { data, emptyData } = selectorTrigger(selector.selectHardshipFiles);

    expect(data).toEqual(
      states.collectionForm![storeId].caseDocumentFiles?.hardShip?.files
    );
    expect(emptyData).toEqual(undefined);
  });

  it('selectHardshipFilesLoading', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectHardshipFilesLoading
    );

    expect(data).toEqual(
      states.collectionForm![storeId].caseDocumentFiles?.hardShip?.loading
    );
    expect(emptyData).toEqual(undefined);
  });

  it('selectHardshipFilesError', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectHardshipFilesError
    );

    expect(data).toEqual(
      states.collectionForm![storeId].caseDocumentFiles?.hardShip?.error
    );
    expect(emptyData).toEqual(undefined);
  });
});
