import React from 'react';
import { renderMockStoreId, storeId } from 'app/test-utils';
import CallResultMilitaryHeader from './Header';
import { mockUseTranslate } from 'app/test-utils/mockUtils';
import { screen } from '@testing-library/dom';
import { HARDSHIP_TYPE_DESC } from 'pages/HardShip/constants';

mockUseTranslate();

describe('CallResultMilitaryHeader', () => {
  it('render ui', () => {
    renderMockStoreId(<CallResultMilitaryHeader />, {
      initialState: {
        hardship: {
          [storeId]: {
            data: {
              typeHardship: HARDSHIP_TYPE_DESC.MD
            }
          } as any
        }
      }
    });

    expect(screen.getByText('txt_type')).toBeInTheDocument();
    expect(screen.getByText('Military Deployment')).toBeInTheDocument();
  });
});
