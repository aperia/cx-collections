import React from 'react';
import { screen } from '@testing-library/react';

// components
import HardShip from './HardShip';

// hooks
import { AccountDetailProvider } from 'app/hooks';

// utils
import { renderMockStore, storeId, accEValue } from 'app/test-utils';

jest.mock('app/_libraries/_dof/core/View', () => () => {
  return <div data-testid="View" />;
});

const initialState: Partial<RootState> = {
  hardship: { [storeId]: {} }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <HardShip />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('Render', () => {
  it('Render UI', async () => {
    renderWrapper(initialState);

    expect(screen.getByTestId('View')).toBeInTheDocument();
  });
});
