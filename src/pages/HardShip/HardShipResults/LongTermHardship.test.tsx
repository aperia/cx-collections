import React from 'react';

// components
import LongTermHardship from './LongTermHardship';

// utils
import { storeId, renderMockStoreId } from 'app/test-utils';
import { HARDSHIP_TYPE } from '../constants';
import { screen } from '@testing-library/react';
import { hardshipActions } from '../_redux/reducers';

jest.mock('app/_libraries/_dls/hooks', () => {
  const hooks = jest.requireActual('app/_libraries/_dls/hooks');

  return {
    ...hooks,
    useTranslation: () => ({ t: (text: string) => text })
  };
});

const defaultInitialState: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      method: 'edit',
      collectionCurrentData: {
        dateNoticeReceivedMainSecond: 'dateNoticeReceivedMainSecond'
      }
    } as any
  },
  hardship: {
    [storeId]: {
      data: {},
      typeForm: HARDSHIP_TYPE[2],
      commonData: {
        cycleToDatePaymentsAmount: '3',
        newFixedPaymentAmount: '2',
        nonDelinquentMpdAmount: '1',
        minimumNewFixedPaymentAmount: '1'
      },
      longTerm: {
        incomeFrequenciesLoading: false,
        monthlyOutgoItemsLoading: false,
        monthlyOutgoItems: [
          { value: 'key01', description: 'value 01' },
          { value: 'key02', description: 'value 02' }
        ]
      }
    } as any
  }
};

const renderWrapper = (
  mainInfo: MagicKeyValue,
  initialState?: Partial<RootState>,
  id?: string
) => {
  return renderMockStoreId(<LongTermHardship mainInfo={mainInfo} />, {
    initialState: defaultInitialState
  });
};

let spy: jest.SpyInstance;

describe('Test LongTermHardship component', () => {
  afterEach(() => {
    if (spy) {
      spy.mockReset();
      spy.mockRestore();
    }
  });

  it('Render with no data', async () => {
    // render no data
    const mockData = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy = jest.spyOn(hardshipActions, 'getMonthlyOutgoInformation').mockImplementation(mockData);
    const { wrapper } = renderWrapper({
      outgoMultiSelectItems: [],
      outgoInputItems: {}
    });
    expect(wrapper.container).toBeInTheDocument();
  });

  it('Render with data', async () => {
    const mainInfoInput = {
      outgoingInfo: {
        key01: '120',
        key02: '100'
      }
    };
    const mockData = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy = jest.spyOn(hardshipActions, 'getMonthlyOutgoInformation').mockImplementation(mockData);
    renderWrapper(mainInfoInput);

    expect(screen.queryByText('$100.00')).toBeInTheDocument();
  });
});
