import React from 'react';

// components
import { View } from 'app/_libraries/_dof/core';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// redux
import { selectHardshipInfo } from '../_redux/selectors';

// helpers
import isEmpty from 'lodash.isempty';
import { formatCommon } from 'app/helpers/formatCommon';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface NaturalDisasterProps {}

const NaturalDisaster: React.FC<NaturalDisasterProps> = () => {
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();

  const mainInfo = useStoreIdSelector<MagicKeyValue>(selectHardshipInfo);
  return (
    <>
      <View
        id={`${storeId}-hardshipNaturalDetail`}
        formKey={`${storeId}-hardshipNaturalDetail`}
        descriptor="hardshipNaturalDetail"
        value={mainInfo}
      />
      <div className="divider-dashed my-24"></div>
      <View
        id={`${storeId}-hardshipNaturalDetail1`}
        formKey={`${storeId}-hardshipNaturalDetail1`}
        descriptor="hardshipNaturalDetail1"
        value={mainInfo}
      />
      {!isEmpty(mainInfo?.reagePaymentPeriod?.value) && (
        <div className="form-group-static">
          <span
            className="form-group-static__label"
            data-testid={genAmtId('naturalDisaster', 'reage-label', '')}
          >
            {t('txt_hardship_reage_title')}
          </span>
          <span
            className="form-group-static__text"
            data-testid={genAmtId('naturalDisaster', 'reage-value', '')}
          >
            {mainInfo?.reagePaymentPeriod?.description}
          </span>
        </div>
      )}
      {!isEmpty(mainInfo?.manualReage) && (
        <div className="form-group-static">
          <span
            className="form-group-static__label"
            data-testid={genAmtId('naturalDisaster', 'manual-label', '')}
          >
            {t('txt_hardship_manual_title')}
          </span>
          <span
            className="form-group-static__text"
            data-testid={genAmtId('naturalDisaster', 'manual-value', '')}
          >
            {formatCommon(mainInfo?.manualReage).time.date}
          </span>
        </div>
      )}
    </>
  );
};

export default NaturalDisaster;
