import { mappingDataFromObj } from 'app/helpers';

export const mapDataToLongTermHardshipViewResult = (
  rawData: MagicKeyValue,
  mapping: MagicKeyValue
) => {
  const formatData = mappingDataFromObj<MagicKeyValue>(rawData, mapping);
  return {
    ...formatData,
    reagePaymentPeriod: {
      ...formatData?.reagePaymentPeriod,
      description: `${rawData?.reAgePaymentPeriod} Months`
    }
  };
};

export const mapDataToMilitaryHardshipViewResult = (
  rawData: MagicKeyValue,
  mapping: MagicKeyValue
) => {
  const formatData = mappingDataFromObj<MagicKeyValue>(rawData, mapping);
  return {
    ...formatData,
    newInterestRate: formatData.newInterestRate || 6,
    reagePaymentPeriod: {
      ...formatData?.reagePaymentPeriod,
      description: `${rawData?.reAgePaymentPeriod} Months`
    }
  };
};
