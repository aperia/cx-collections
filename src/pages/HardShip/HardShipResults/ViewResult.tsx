import React, { Fragment } from 'react';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// components
import { View } from 'app/_libraries/_dof/core';

// type
import { HARDSHIP_TYPE_ID } from '../constants';

// redux
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { selectHardshipInfo } from '../_redux/selectors';
import LongTermHardShipViewResult from './LongTermHardship';
import MilitaryDeploymentResult from './MilitaryDeploymentResult/';
import Natural from './NaturalDisaster';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface Props {}

const HardShipViewResult: React.FC<Props> = () => {
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();

  const mainInfo = useStoreIdSelector<MagicKeyValue>(selectHardshipInfo);

  switch (mainInfo?.typeCode) {
    case HARDSHIP_TYPE_ID.naturalDisaster:
      return (
        <Fragment>
          <div className="form-group-static">
            <span
              className="form-group-static__label"
              data-testid={genAmtId('hardShipViewResult', 'label', '')}
            >
              {t('txt_type')}
            </span>
            <span
              className="form-group-static__text"
              data-testid={genAmtId('hardShipViewResult', 'text', '')}
            >
              {mainInfo?.typeHardship}
            </span>
          </div>
          <div className="divider-dashed my-24"></div>
          <Natural />
        </Fragment>
      );
    case HARDSHIP_TYPE_ID.longTerm:
      return (
        <Fragment>
          <div className="form-group-static">
            <span
              className="form-group-static__label"
              data-testid={genAmtId('hardShipViewResult', 'label', '')}
            >
              {t('txt_type')}
            </span>
            <span
              className="form-group-static__text"
              data-testid={genAmtId('hardShipViewResult', 'text', '')}
            >
              {mainInfo?.typeHardship}
            </span>
          </div>
          <div className="divider-dashed my-24"></div>
          <LongTermHardShipViewResult mainInfo={mainInfo} />
        </Fragment>
      );
    case HARDSHIP_TYPE_ID.militaryDeployment:
      return <MilitaryDeploymentResult />;
    case HARDSHIP_TYPE_ID.hardship:
    default:
      return (
        <Fragment>
          <div className="form-group-static">
            <span
              className="form-group-static__label"
              data-testid={genAmtId('hardShipViewResult', 'label', '')}
            >
              {t('txt_type')}
            </span>
            <span
              className="form-group-static__text"
              data-testid={genAmtId('hardShipViewResult', 'text', '')}
            >
              {mainInfo?.typeHardship}
            </span>
          </div>
          <div className="divider-dashed my-24"></div>
          <View
            id={`${storeId}-hardshipDetailViews`}
            formKey={`${storeId}-hardshipDetailViews`}
            descriptor="hardshipDetailViews"
            value={mainInfo}
          />
        </Fragment>
      );
  }
};

export default HardShipViewResult;
