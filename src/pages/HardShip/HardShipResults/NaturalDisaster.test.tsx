import React from 'react';
import { screen } from '@testing-library/react';
import {
  renderMockStoreId,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';

import NaturalDisaster from './NaturalDisaster';

describe('Natural Disaster Test', () => {
  it('render with Data', () => {
    renderMockStoreId(<NaturalDisaster />);
    expect(
      screen.queryByText('txt_hardship_manual_title')
    ).not.toBeInTheDocument();
  });

  it('render with Data', () => {
    renderMockStoreId(<NaturalDisaster />, {
      initialState: {
        hardship: {
          [storeId]: {
            data: {
              reagePaymentPeriod: {
                value: 'value01',
                description: 'description 01'
              },
              manualReage: {
                value: 'June 12 2020'
              }
            }
          } as any
        }
      }
    });

    expect(screen.getByText('txt_hardship_manual_title')).toBeInTheDocument();
  });
});
