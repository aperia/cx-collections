import React, { useEffect, useMemo } from 'react';

// components
import { View } from 'app/_libraries/_dof/core';

// redux
import { useDispatch } from 'react-redux';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { mapDataToView } from '../LongTermHardship/helper';
import { TruncateText } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import DividerControl, {
  DividerControlProps
} from 'app/components/_dof_controls/controls/DividerControl';
import { formatCommon } from 'app/helpers';
import { hardshipActions as actions } from '../_redux/reducers';
import { MonthlyOutgoItem } from '../LongTermHardship/types';
import { selectMonthlyOutgoItems } from '../LongTermHardship/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface Props {
  mainInfo: MagicKeyValue;
}

const LongTermHardShipViewResult: React.FC<Props> = ({ mainInfo }) => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const monthlyOutgoItems = useStoreIdSelector<MonthlyOutgoItem[]>(
    selectMonthlyOutgoItems
  );
  const testId = 'longTermHardShipViewResult';

  const content = useMemo(() => {
    const { fields, totalValue } = mapDataToView(mainInfo, monthlyOutgoItems);

    if (fields.length === 0) return null;
    const propsDivider = { dashed: true } as DividerControlProps;
    const totalValueView = formatCommon(totalValue).currency();

    return (
      <>
        <div className="my-24">
          <DividerControl {...propsDivider} />
        </div>
        <div className="color-grey fs-11 text-uppercase fw-500">
          {t('txt_monthlyOutgoInformation')}
        </div>
        {fields.map(({ value, description }, index) => {
          const id = value;
          const valueView = formatCommon(value).currency();
          return (
            <div key={value} className="form-group-static mt-16 col-16" id={id}>
              <span id={`${id}__label`} className="form-group-static__label">
                <TruncateText
                  id={`${id}__label_text`}
                  title={t(description)}
                  dataTestId={genAmtId(
                    testId,
                    `monthlyOutgoInformation-label-${index}`,
                    ''
                  )}
                >
                  {t(description)}
                </TruncateText>
              </span>
              <div
                id={`${id}__text`}
                className="form-group-static__text"
                data-testid={genAmtId(
                  testId,
                  `monthlyOutgoInformation-value-${index}`,
                  ''
                )}
              >
                {valueView}
              </div>
            </div>
          );
        })}
        <div
          className="fw-500 color-grey-d20 mt-16 fs-14"
          data-testid={genAmtId(testId, 'totalOutgo', '')}
        >
          {t('txt_totalOutgo')}: {totalValueView}
        </div>
      </>
    );
  }, [mainInfo, monthlyOutgoItems, t]);

  useEffect(() => {
    dispatch(actions.getMonthlyOutgoInformation({ storeId }));
  }, [dispatch, storeId]);

  return (
    <>
      <View
        id={`${storeId}-longTermHardshipDetailViews`}
        formKey={`${storeId}-longTermHardshipDetailViews`}
        descriptor="longTermHardshipDetailViews"
        value={mainInfo}
      />
      {content}
      <View
        id={`${storeId}-longTermHardshipBasicDetail`}
        formKey={`${storeId}-longTermHardshipBasicDetail`}
        descriptor="longTermHardshipBasicDetail"
        value={mainInfo}
      />
    </>
  );
};

export default LongTermHardShipViewResult;
