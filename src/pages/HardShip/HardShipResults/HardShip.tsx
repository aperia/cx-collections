import React, { useEffect } from 'react';

// components
import ViewResults from './ViewResult';

// helpers
import classNames from 'classnames';

// Hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// redux
import { selectIsLoadingView } from '../_redux/selectors';
import { useDispatch } from 'react-redux';
import { hardshipActions } from '../_redux/reducers';

export interface Props {}

const Hardship: React.FC<Props> = () => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const loading = useStoreIdSelector<boolean>(selectIsLoadingView);

  useEffect(() => {
    dispatch(
      hardshipActions.getHardshipDebtManagementDetails({
        postData: {
          storeId
        }
      })
    );
  }, [dispatch, storeId]);

  return (
    <div className={classNames({ loading }, 'mt-n8')}>
      <ViewResults />
    </div>
  );
};

export default Hardship;
