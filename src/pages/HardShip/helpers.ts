import { Field } from 'redux-form';
import { ExtraFieldProps } from 'app/_libraries/_dof/core';
import { isEmpty, isUndefined } from 'app/_libraries/_dls/lodash';

// isLastDayMonth
export const isLastDayMonth = (day: number, month: number, year: number) => {
  const lastDay =
    month === 2
      ? year & 3 || (!(year % 25) && year & 15)
        ? 28
        : 29
      : 30 + ((month + (month >> 3)) & 1);

  return lastDay === day;
};

// only cover reage payment period
// field ["item 1", "item 2"]
// data form
export const lengthMonth = (
  data: MagicKeyValue,
  field: string[],
  type?: string
) => {
  if (isEmpty(data)) return 0;
  const from = data[field[0]];
  const to = data[field[1]];

  if (isUndefined(to) || isUndefined(from)) return 0;

  const fromDate = new Date(`${from}`).getDate();
  const fromMonth = new Date(`${from}`).getMonth();
  const fromYear = new Date(`${from}`).getFullYear();
  const toDate = new Date(`${to}`).getDate();
  const toMonth = new Date(`${to}`).getMonth();
  const toYear = new Date(`${to}`).getFullYear();

  if (type === 'longTermHardship') {
    // the toDate minus fromDate is less than one month
    const calMonthByDate = fromDate > toDate ? 1 : 0;

    const totalMonth =
      (toYear - fromYear) * 12 + toMonth - fromMonth - calMonthByDate;

    return Math.floor(totalMonth / 12) - 1;
  }

  if (toMonth === fromMonth && toDate === fromDate) return 12;

  const lastDayFrom = isLastDayMonth(fromDate, fromMonth + 1, fromYear);
  const lastDayTo = isLastDayMonth(toDate, toMonth + 1, toYear);

  // fromMonth > toMonth =>  toYear > fromYear
  if (fromMonth > toMonth) {
    if (lastDayFrom && lastDayTo) {
      return toMonth + 12 - fromMonth - 3;
    }
    return fromDate > toDate
      ? toMonth + 12 - fromMonth - 1 - 3
      : toMonth + 12 - fromMonth - 3;
  }

  if (fromMonth === toMonth) {
    return toMonth + 12 - fromMonth - 1 - 3;
  }


  // fromMonth < toMonth
  // toYear === fromYear
  if (lastDayFrom && lastDayTo) {
    return toMonth - fromMonth - 3;
  }
  return fromDate > toDate
    ? toMonth - fromMonth - 1 - 3
    : toMonth - fromMonth - 3;
};

export const setStatusNFPAInit = (
  field: Field<ExtraFieldProps>,
  data: MagicKeyValue
) => {
  const { minimumNewFixedPaymentAmount, newFixedPaymentAmount } = data;
  const valueBalance = parseFloat(minimumNewFixedPaymentAmount);
  const valueNFPA = parseFloat(newFixedPaymentAmount);

  const isError = valueNFPA > valueBalance;

  field?.props?.props?.setOthers((prevOthers: MagicKeyValue) => ({
    ...prevOthers,
    autoResetForceInvalid: isError
  }));
};

export const setFieldValidationRules = (
  field: Field<ExtraFieldProps>,
  rules: any[]
) => {
  field?.props?.props?.setOthers((pre: MagicKeyValue) => {
    // const { validationRules: currentRules } = pre;

    return {
      ...pre,
      validationRules: [
        // ...(Array.isArray(currentRules) ? currentRules : []),
        ...rules
      ]
    };
  });
};
