// mocks
import { mockAxiosResolve } from 'app/test-utils/mocks/mockAxiosResolve';

// services
import { hardshipService } from './hardshipService';

// utils
import { mockApiServices, storeId } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';
import { RemoveHardshipPayload } from './types';

describe('hardshipService success', () => {
  it('getGroupCheckboxCountryDeployment', () => {
    hardshipService.getGroupCheckboxCountryDeployment();

    expect(mockAxiosResolve).toBeCalledWith({
      url: 'refData/checkboxCountryDeployment.json',
      method: 'GET'
    });
  });

  it('getMonthlyOutgoInformation', () => {
    hardshipService.getMonthlyOutgoInformation();

    expect(mockAxiosResolve).toBeCalledWith({
      url: 'refData/monthlyOutgoInformation.json',
      method: 'GET'
    });
  });

  it('getIncomeFrequency', () => {
    hardshipService.getIncomeFrequency();

    expect(mockAxiosResolve).toBeCalledWith({
      url: 'refData/incomeFrequency.json',
      method: 'GET'
    });
  });

  describe('uploadMilitaryFile', () => {
    const params = { accountId: storeId, file: '', onUploadProgress: () => {} };

    const formData = new FormData();
    formData.append('file', params.file);
    formData.append('accountId', params.accountId);
    formData.append('uploadType', 'HARDSHIP');

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      hardshipService.uploadMilitaryFile(
        params.accountId,
        params.file,
        params.onUploadProgress
      );

      expect(mockService).toBeCalledWith('', formData, {
        headers: { 'Content-Type': 'application/json' },
        onUploadProgress: params.onUploadProgress
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      hardshipService.uploadMilitaryFile(
        params.accountId,
        params.file,
        params.onUploadProgress
      );

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.uploadMilitaryFiles,
        formData,
        {
          headers: { 'Content-Type': 'application/json' },
          onUploadProgress: params.onUploadProgress
        }
      );
    });
  });

  describe('updateHardship', () => {
    const params = { accountId: storeId };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      hardshipService.updateHardship(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      hardshipService.updateHardship(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.setupHardship,
        params
      );
    });
  });

  describe('removeHardship', () => {
    const params: RemoveHardshipPayload = {
      common: { accountId: storeId },
      callResultType: 'type'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      hardshipService.removeHardship(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      hardshipService.removeHardship(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.removeHardship,
        params
      );
    });
  });

  describe('getDetailsHardship', () => {
    const params = { accountId: storeId };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      hardshipService.getDetailsHardship(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      hardshipService.getDetailsHardship(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.getDetailsHardship,
        params
      );
    });
  });
});
