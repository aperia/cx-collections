// constants & i18n
import { I18N_COLLECTION_FORM, I18N_COMMON_TEXT } from 'app/constants/i18n';
// hooks
import {
  useAccountDetail,
  useIsDirtyForm,
  useStoreIdSelector
} from 'app/hooks';
// components
import {
  Button,
  DropdownBaseChangeEvent,
  DropdownList,
  Icon
} from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';
import SimpleBarWithApiError from 'pages/CollectionForm/SimpleBarWithApiError';
// actions
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
// selectors
import { selectCollectionFormMethod } from 'pages/CollectionForm/_redux/selectors';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import React, { useEffect, useLayoutEffect, useMemo, useRef } from 'react';
// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import { isInvalid } from 'redux-form';
import {
  HARDSHIP_TYPE,
  HARDSHIP_TYPE_ID,
  HARD_SHIP_FORM_VIEWS,
  NATURAL_DISASTER
} from '../constants';
import { VIEWS as LONG_TERM_VIEWS } from '../LongTermHardship/helper';
import { VIEWS as MILITARY_DEPLOYMENT_VIEWS } from '../MilitaryDeploymentHardship/constants';
import { TRIGGER_TYPE } from '../types';
import { hardshipActions } from '../_redux/reducers';
import {
  selectHardshipDetailsData,
  selectIsDirtyLongTermHardship,
  selectIsValidLongTermHardship,
  selectTypeFormHardship
} from '../_redux/selectors';
import HardShipViewForm from './ViewForm';

import * as liveVoxSelectors from 'app/livevox-dialer/_redux/selectors';
import { actionsLiveVox } from 'app/livevox-dialer/_redux/reducers';
import { LiveVoxCallResultArgs } from 'app/livevox-dialer/types';
import { CallResult } from 'pages/CollectionForm/types';
import { takeSelectedCallResult } from 'pages/CollectionForm/_redux/selectors';
import { isEmpty } from 'app/_libraries/_dls/lodash';

export interface Props {}

const HardShip: React.FC<Props> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const hardshipView = `${storeId}-${HARD_SHIP_FORM_VIEWS}`;
  const naturalDisasterView = `${storeId}-${NATURAL_DISASTER}`;
  const longTermFormName = `${storeId}-${LONG_TERM_VIEWS}`;
  const militaryDeploymentFormName = `${storeId}-${MILITARY_DEPLOYMENT_VIEWS}`;
  const divRef = useRef<HTMLDivElement | null>(null);

  const isHardship = useSelector<RootState, boolean>(isInvalid(hardshipView));
  const isNaturalDisaster = useSelector<RootState, boolean>(
    isInvalid(naturalDisasterView)
  );
  const isMilitaryDeployment = useSelector<RootState, boolean>(
    isInvalid(militaryDeploymentFormName)
  );
  const isBasicLongTermHardshipFormValid = useStoreIdSelector<
    boolean | undefined
  >(selectIsValidLongTermHardship);

  const isBasicLongTermHardshipFormDirty = useStoreIdSelector<
    boolean | undefined
  >(selectIsDirtyLongTermHardship);

  const isLongTermFormInvalid = useSelector<RootState, boolean>(
    isInvalid(longTermFormName)
  );

  const method = useStoreIdSelector<string>(selectCollectionFormMethod);
  const selectedType = useStoreIdSelector<{ id: string; value: string }>(
    selectTypeFormHardship
  );
  const typeForm = useMemo(
    () => ({
      ...selectedType,
      value: t(selectedType.value)
    }),
    [selectedType, t]
  );
  const dataDetails = useStoreIdSelector<MagicKeyValue>(
    selectHardshipDetailsData
  );

  const longTermFormDirty = useIsDirtyForm([longTermFormName]);

  const isNaturalDisasterDirty = useIsDirtyForm([naturalDisasterView]);

  const isMilitaryDeploymentDirty = useIsDirtyForm([
    militaryDeploymentFormName
  ]);

  const isEdit = method === COLLECTION_METHOD.EDIT;

  // LiveVox
  const liveVoxDialerSessionId = useSelector(
    liveVoxSelectors.getLiveVoxDialerSessionId
  );
  const liveVoxDialerLineNumber = useSelector(
    liveVoxSelectors.getLiveVoxDialerLineNumber
  );
  const liveVoxDialerLineActiveAndReadyData = useSelector(
    liveVoxSelectors.getLiveVoxDialerLineActiveAndReadyData
  );
  const callResultSelected = useStoreIdSelector<CallResult>(
    takeSelectedCallResult
  );

  const handleCancel = () => {
    const isLongTermDirty =
      isBasicLongTermHardshipFormDirty || longTermFormDirty;

    if (
      (typeForm.id === HARDSHIP_TYPE_ID.longTerm && isLongTermDirty) ||
      (typeForm.id === HARDSHIP_TYPE_ID.militaryDeployment &&
        isMilitaryDeploymentDirty) ||
      (typeForm.id === HARDSHIP_TYPE_ID.naturalDisaster &&
        isNaturalDisasterDirty)
    ) {
      // form dirty
      return dispatch(
        confirmModalActions.open({
          title: 'txt_unsaved_changes',
          body: 'txt_discard_change_body',
          cancelText: 'txt_continue_editing',
          confirmText: 'txt_discard_change',
          confirmCallback: handleProcessCancel,
          variant: 'danger'
        })
      );
    }
    handleProcessCancel();
  };

  const handleProcessCancel = () => {
    batch(() => {
      dispatch(collectionFormActions.setCurrentView({ storeId }));
      dispatch(
        collectionFormActions.selectCallResult({ storeId, item: undefined })
      );
    });
  };

  const handleSubmit = () => {
    let viewName: string[] | undefined;
    dispatch(hardshipActions.setIsSubmit({ storeId, value: true }));
    switch (typeForm.id) {
      case HARDSHIP_TYPE_ID.naturalDisaster:
        viewName = [naturalDisasterView];
        break;
      case HARDSHIP_TYPE_ID.longTerm:
        viewName = [longTermFormName];
        break;
      case HARDSHIP_TYPE_ID.militaryDeployment:
        viewName = [militaryDeploymentFormName];
        break;
      case HARDSHIP_TYPE_ID.hardship:
      default:
        viewName = [hardshipView];
        break;
    }

    if (
      liveVoxDialerLineActiveAndReadyData &&
      !isEmpty(liveVoxDialerLineActiveAndReadyData.accountNumber)
    ) {
      const liveVoxTermCodesArgs: LiveVoxCallResultArgs = {
        liveVoxDialerSessionId: liveVoxDialerSessionId,
        liveVoxDialerLineNumber: liveVoxDialerLineNumber,
        selectedCallResult: callResultSelected,
        serviceId: liveVoxDialerLineActiveAndReadyData.serviceId,
        callTransactionId:
          liveVoxDialerLineActiveAndReadyData.callTransactionId,
        callSessionId: liveVoxDialerLineActiveAndReadyData.callSessionId,
        account: liveVoxDialerLineActiveAndReadyData.accountNumber,
        phoneDialed: liveVoxDialerLineActiveAndReadyData.phoneNumber,
        moveAgentToNotReady: false
      };
      dispatch(
        actionsLiveVox.liveVoxDialerTermCodesThunk(liveVoxTermCodesArgs)
      );
    }

    dispatch(
      hardshipActions.triggerUpdateHardship({
        storeId,
        postData: {
          viewName,
          typeForm: typeForm.id,
          type: isEdit ? TRIGGER_TYPE.UPDATE : TRIGGER_TYPE.CREATE,
          dataDetails
        }
      })
    );
  };

  const handleChangeType = (event: DropdownBaseChangeEvent) => {
    const doChange = () => {
      const newType = event.value;
      batch(() => {
        dispatch(
          hardshipActions.updateTypeFormHardship({
            storeId,
            typeForm: newType
          })
        );
        dispatch(hardshipActions.setDataFormHardship({ storeId }));
        // Clear API Error when change type
        dispatch(
          apiErrorNotificationAction.clearApiErrorData({
            storeId,
            forSection: 'inCollectionFormFlyOut'
          })
        );
      });
    };
    const isLongTermDirty =
      isBasicLongTermHardshipFormDirty || longTermFormDirty;

    if (
      (typeForm.id === HARDSHIP_TYPE_ID.longTerm && isLongTermDirty) ||
      (typeForm.id === HARDSHIP_TYPE_ID.militaryDeployment &&
        isMilitaryDeploymentDirty) ||
      (typeForm.id === HARDSHIP_TYPE_ID.naturalDisaster &&
        isNaturalDisasterDirty)
    ) {
      // form dirty
      return dispatch(
        confirmModalActions.open({
          title: 'txt_unsaved_changes',
          body: 'txt_discard_change_body',
          cancelText: 'txt_continue_editing',
          confirmText: 'txt_discard_change',
          confirmCallback: doChange,
          variant: 'danger'
        })
      );
    }
    doChange();
  };

  // get hardship data
  useEffect(() => {
    dispatch(
      hardshipActions.getDetailsHardship({
        postData: { storeId, hardshipType: typeForm.id }
      })
    );
  }, [dispatch, storeId, typeForm.id]);

  // Hide next actions - float dropdown button
  useEffect(() => {
    document.body.classList.add('hide-float-btn');
    return () => {
      document.body.classList.remove('hide-float-btn');
    };
  }, []);

  let isDisabledSubmit = false;
  switch (typeForm.id) {
    case HARDSHIP_TYPE_ID.naturalDisaster:
      isDisabledSubmit = isNaturalDisaster;
      break;
    case HARDSHIP_TYPE_ID.longTerm:
      isDisabledSubmit = Boolean(
        isLongTermFormInvalid || !isBasicLongTermHardshipFormValid
      );
      break;
    case HARDSHIP_TYPE_ID.militaryDeployment:
      isDisabledSubmit = Boolean(isMilitaryDeployment);
      break;
    case HARDSHIP_TYPE_ID.hardship:
    default:
      isDisabledSubmit = isHardship;
  }

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100vh - 258px)`);
  }, []);

  return (
    <>
      <div ref={divRef}>
        <SimpleBarWithApiError className="ml-24 mt-24">
          <div className="p-24">
            <h5
              className="mr-auto"
              data-testid={genAmtId('hardShip', 'call-result', '')}
            >
              {t(I18N_COLLECTION_FORM.CALL_RESULT)}
            </h5>
            <div className="mt-16 p-8 bg-light-l16 d-flex align-items-center rounded-lg">
              <div className="bubble-icon">
                <Icon name="megaphone" />
              </div>
              <p
                className="ml-12"
                data-testid={genAmtId('hardShip', 'title', '')}
              >
                {t(I18N_COMMON_TEXT.HARDSHIP)}
              </p>
            </div>
            <div className="pt-16">
              <DropdownList
                dataTestId="hardShip-type"
                textField="value"
                onChange={handleChangeType}
                label={t('txt_type')}
                value={typeForm}
                placeholder={t('txt_type')}
                popupBaseProps={{
                  popupBaseClassName: 'inside-infobar'
                }}
                noResult={t('txt_no_results_found')}
              >
                {HARDSHIP_TYPE.map(item => {
                  item = {
                    ...item,
                    value: t(item.value)
                  };
                  return (
                    <DropdownList.Item
                      key={item.id}
                      label={item.value}
                      value={item}
                    />
                  );
                })}
              </DropdownList>
            </div>
            <hr />
            <HardShipViewForm type={typeForm.id} />
          </div>
        </SimpleBarWithApiError>
      </div>
      <div className="group-button-footer d-flex justify-content-end">
        <Button
          variant="secondary"
          onClick={handleCancel}
          dataTestId="hardShip-cancel-btn"
        >
          {t(I18N_COMMON_TEXT.CANCEL)}
        </Button>
        <Button
          disabled={isDisabledSubmit}
          onClick={handleSubmit}
          dataTestId="hardShip-submit-btn"
        >
          {isEdit ? t(I18N_COMMON_TEXT.SAVE) : t(I18N_COMMON_TEXT.SUBMIT)}
        </Button>
      </div>
    </>
  );
};

export default HardShip;
