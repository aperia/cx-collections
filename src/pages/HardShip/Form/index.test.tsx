import React from 'react';
import HardShipForm from '.';
import {
  renderMockStoreId,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';
import { fireEvent, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { mockActionCreator } from 'app/test-utils';
import { HARDSHIP_TYPE_ID, NATURAL_DISASTER } from '../constants';
import { hardshipActions } from '../_redux/reducers';
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';
import { VIEWS as MilitaryDeploymentViews } from '../MilitaryDeploymentHardship/constants';
import { CollectionForm } from 'pages/CollectionForm/types';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { actionsLiveVox } from 'app/livevox-dialer/_redux/reducers';

jest.mock('app/_libraries/_dls/components/DropdownList', () =>
  jest.requireActual('app/test-utils/mocks/MockDropdownList')
);

function MockHardShipViewForm({ type }: any) {
  return <span>{type}</span>;
}

jest.mock('./ViewForm', () => (props: any) => MockHardShipViewForm(props));

const renderHardShipForm = (initialState: Partial<RootState>) => {
  renderMockStoreId(<HardShipForm />, { initialState });
};

HTMLCanvasElement.prototype.getContext = jest.fn();

const basicHardShipState = {
  isLoading: false,
  data: {
    currentBalance: 1110,
    cycleToDatePaymentsAmount: 0,
    nonDelinquentMpdAmount: 100,
    minimumNewFixedPaymentAmount: 1110,
    newFixedPaymentAmount: 111
  },
  commonData: {
    currentBalance: 1110,
    cycleToDatePaymentsAmount: 0,
    nonDelinquentMpdAmount: 100,
    minimumNewFixedPaymentAmount: 1110,
    newFixedPaymentAmount: 111
  },
  dataDetails: {
    status: 'success',
    caseId: '7990231062101291',
    debtMgmtCode: 'PS',
    debtMgmtCodeValue: '3CAP',
    pricingStrategyLockStatus: 'U',
    ctdPaymentAmount: '0000000000000000.00',
    newFixedPaymentAmount: '111.00',
    nonDelinquentMinimumPaymentDueAmount: '0000000000000100.00',
    activeHardshipExists: false,
    cardholderContactedName: 'SMITH,ADELE               ',
    currentBalanceAmount: '0000000001110.00',
    miscellaneousField1Identifier: '    ',
    workoutLastDate: '00000000'
  }
};

const confirmActions = mockActionCreator(confirmModalActions);
const mockCollectionFormActions = mockActionCreator(collectionFormActions);
const mockHardShipAction = mockActionCreator(hardshipActions);
const mockLiveVoxAction = mockActionCreator(actionsLiveVox);
const mockApiErrorNotificationAction = mockActionCreator(
  apiErrorNotificationAction
);

describe('handle Cancel Action', () => {
  it('execute handleCancel when click cancel button without changing anything', () => {
    const openAction = confirmActions('open');
    const setCurrentViewAction = mockCollectionFormActions('setCurrentView');
    renderHardShipForm({
      hardship: {
        [storeId]: {
          ...basicHardShipState,
          typeForm: {
            id: HARDSHIP_TYPE_ID.hardship,
            value: HARDSHIP_TYPE_ID.hardship
          }
        }
      }
    });
    const cancelButton = screen.queryByText('txt_cancel');
    userEvent.click(cancelButton!);
    expect(openAction).not.toHaveBeenCalled();
    expect(setCurrentViewAction).toHaveBeenCalledWith({ storeId });
  });

  it('execute handleCancel when click cancel button with changing Value', () => {
    const openAction = confirmActions('open');
    renderHardShipForm({
      hardship: {
        [storeId]: {
          ...basicHardShipState,
          typeForm: {
            id: HARDSHIP_TYPE_ID.longTerm,
            value: HARDSHIP_TYPE_ID.longTerm
          },
          longTerm: { isDirty: true }
        }
      }
    });
    const cancelButton = screen.queryByText('txt_cancel');
    userEvent.click(cancelButton!);
    expect(openAction).toHaveBeenCalled();
  });
  it('execute handleCancel -> isMilitaryDeploymentDirty ', () => {
    const openAction = confirmActions('open');
    renderHardShipForm({
      hardship: {
        [storeId]: {
          ...basicHardShipState,
          typeForm: {
            id: HARDSHIP_TYPE_ID.militaryDeployment,
            value: HARDSHIP_TYPE_ID.militaryDeployment
          }
        }
      },
      form: {
        [`${storeId}-${MilitaryDeploymentViews}`]: {
          fields: {
            militaryDeployment: {
              isDirty: true
            }
          },
          values: {
            militaryDeployment: ''
          }
        }
      } as any
    });
    const cancelButton = screen.queryByText('txt_cancel');
    userEvent.click(cancelButton!);
    expect(openAction).toHaveBeenCalled();
  });

  it('execute handleCancel -> isNaturalDisasterDirty ', () => {
    const openAction = confirmActions('open');
    renderHardShipForm({
      hardship: {
        [storeId]: {
          ...basicHardShipState,
          typeForm: {
            id: HARDSHIP_TYPE_ID.naturalDisaster,
            value: HARDSHIP_TYPE_ID.naturalDisaster
          }
        }
      },
      form: {
        [`${storeId}-${NATURAL_DISASTER}`]: {
          fields: {
            naturalDisasterImpact: {
              touched: true
            }
          },
          values: {
            naturalDisasterImpact: ''
          }
        }
      } as any
    });
    const cancelButton = screen.queryByText('txt_cancel');
    userEvent.click(cancelButton!);
    expect(openAction).toHaveBeenCalled();
  });
});

describe('handle Submit', () => {
  it('hardship form', () => {
    const setIsSubmitMock = mockHardShipAction('setIsSubmit');
    const triggerUpdateHardshiptMock = mockHardShipAction(
      'triggerUpdateHardship'
    );
    const mockLiveVoxDialerTermCodesThunk = mockLiveVoxAction(
      'liveVoxDialerTermCodesThunk'
    );

    renderHardShipForm({
      liveVoxReducer: {
        sessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
        isAgentAuthenticated: true,
        lineNumber: 'ACD',
        accountNumber: '2345',
        lastInCallAccountNumber: '2345',
        inCallAccountNumber: '2345',
        call: {
          accountNumber: '5166480500018901',
          accountNumberRequired: true,
          callCenterId: 75,
          callRecordingStarted: true,
          callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
          callTransactionId: '126650403531',
          phoneNumber: '1234567890',
          serviceId: 1094568
        }
      },
      collectionForm: { [storeId]: {
        selectedCallResult: {
          code: 'PP',
          description: 'Promise to Pay'
        }
        }
      },
      hardship: {
        [storeId]: {
          ...basicHardShipState,
          typeForm: {
            id: HARDSHIP_TYPE_ID.hardship,
            value: HARDSHIP_TYPE_ID.hardship
          }
        }
      }
    });

    const submitButton = screen.queryByText('txt_submit');
    userEvent.click(submitButton!);
    expect(setIsSubmitMock).toBeCalledWith({ storeId, value: true });
    expect(triggerUpdateHardshiptMock).toHaveBeenCalled();
    expect(mockLiveVoxDialerTermCodesThunk).toBeCalledWith(
      {
        liveVoxDialerSessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
        liveVoxDialerLineNumber: 'ACD',
        selectedCallResult: {
          code: 'PP',
          description: 'Promise to Pay'
        },
        serviceId: 1094568,
        callTransactionId: '126650403531',
        callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
        account: '5166480500018901',
        phoneDialed: '1234567890',
        moveAgentToNotReady: false
      }
    );
    expect(triggerUpdateHardshiptMock).toHaveBeenCalled();
  });

  it('hardship form should not dispatch DialerTermCodes when no accountNumber', () => {
    const setIsSubmitMock = mockHardShipAction('setIsSubmit');
    const triggerUpdateHardshiptMock = mockHardShipAction(
      'triggerUpdateHardship'
    );
    const mockLiveVoxDialerTermCodesThunk = mockLiveVoxAction(
      'liveVoxDialerTermCodesThunk'
    );

    renderHardShipForm({
      liveVoxReducer: {
        sessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
        isAgentAuthenticated: true,
        lineNumber: 'ACD',
        accountNumber: '',
        lastInCallAccountNumber: '',
        inCallAccountNumber: '',
        call: {
          accountNumber: '',
          accountNumberRequired: true,
          callCenterId: 75,
          callRecordingStarted: true,
          callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
          callTransactionId: '126650403531',
          phoneNumber: '1234567890',
          serviceId: 1094568
        }
      },
      collectionForm: { [storeId]: {
        selectedCallResult: {
          code: 'PP',
          description: 'Promise to Pay'
        }
        }
      },
      hardship: {
        [storeId]: {
          ...basicHardShipState,
          typeForm: {
            id: HARDSHIP_TYPE_ID.hardship,
            value: HARDSHIP_TYPE_ID.hardship
          }
        }
      }
    });

    const submitButton = screen.queryByText('txt_submit');
    userEvent.click(submitButton!);
    expect(setIsSubmitMock).toBeCalledWith({ storeId, value: true });
    expect(triggerUpdateHardshiptMock).toHaveBeenCalled();
    expect(mockLiveVoxDialerTermCodesThunk).not.toBeCalled();
    expect(triggerUpdateHardshiptMock).toHaveBeenCalled();
  });

  it('naturalDisaster form and Edit Method', () => {
    const setIsSubmitMock = mockHardShipAction('setIsSubmit');
    const triggerUpdateHardshiptMock = mockHardShipAction(
      'triggerUpdateHardship'
    );
    renderHardShipForm({
      hardship: {
        [storeId]: {
          ...basicHardShipState,
          typeForm: {
            id: HARDSHIP_TYPE_ID.naturalDisaster,
            value: HARDSHIP_TYPE_ID.naturalDisaster
          }
        }
      },
      collectionForm: {
        [storeId]: {
          method: COLLECTION_METHOD.EDIT
        } as CollectionForm
      },
      form: {
        [`${storeId}-${NATURAL_DISASTER}`]: {
          syncErrors: {}
        }
      } as any
    });
    const saveButton = screen.queryByText('txt_save');
    userEvent.click(saveButton!);
    expect(triggerUpdateHardshiptMock).toHaveBeenCalled();
    expect(setIsSubmitMock).toBeCalledWith({ storeId, value: true });
  });

  it('naturalDisaster form and Submit Method', () => {
    mockHardShipAction('setIsSubmit');
    const triggerUpdateHardshiptMock = mockHardShipAction(
      'triggerUpdateHardship'
    );
    renderHardShipForm({
      hardship: {
        [storeId]: {
          ...basicHardShipState,
          typeForm: {
            id: HARDSHIP_TYPE_ID.longTerm,
            value: HARDSHIP_TYPE_ID.longTerm
          },
          longTerm: {
            isValid: true,
            isDirty: true
          }
        }
      }
    });
    const submitButton = screen.queryByText('txt_submit');
    userEvent.click(submitButton!);
    expect(triggerUpdateHardshiptMock).toHaveBeenCalled();
  });

  it('naturalDisaster form and Submit Method', () => {
    mockHardShipAction('setIsSubmit');
    const triggerUpdateHardshiptMock = mockHardShipAction(
      'triggerUpdateHardship'
    );
    renderHardShipForm({
      hardship: {
        [storeId]: {
          ...basicHardShipState,
          typeForm: {
            id: HARDSHIP_TYPE_ID.militaryDeployment,
            value: HARDSHIP_TYPE_ID.militaryDeployment
          }
        }
      },
      form: {
        [`${storeId}-${MilitaryDeploymentViews}`]: {
          syncErrors: {}
        }
      } as any
    });
    const submitButton = screen.queryByText('txt_submit');
    userEvent.click(submitButton!);
    expect(triggerUpdateHardshiptMock).toHaveBeenCalled();
  });
});

describe('handleChangeType', () => {
  it('select Long-Term Hardship', () => {
    const updateTypeFormHardship = mockHardShipAction('updateTypeFormHardship');
    const clearApiErrorData =
      mockApiErrorNotificationAction('clearApiErrorData');
    renderHardShipForm({
      hardship: {
        [storeId]: {
          ...basicHardShipState,
          typeForm: {
            id: HARDSHIP_TYPE_ID.hardship,
            value: HARDSHIP_TYPE_ID.hardship
          }
        }
      }
    });
    const dropdownList = screen.queryByText(HARDSHIP_TYPE_ID.hardship);
    userEvent.click(dropdownList!);

    const longTerm = screen.getByTestId('DropdownList.onChange');

    fireEvent.change(longTerm!, {
      target: {
        value: { id: HARDSHIP_TYPE_ID.longTerm, value: 'Long-Term Hardship' }
      }
    });
    expect(updateTypeFormHardship).toHaveBeenCalled();
    expect(clearApiErrorData).toHaveBeenCalled();
  });
  it('select Long-Term Hardship ', () => {
    const updateTypeFormHardship = mockHardShipAction('updateTypeFormHardship');
    const clearApiErrorData =
      mockApiErrorNotificationAction('clearApiErrorData');
    const openAction = confirmActions('open');
    renderHardShipForm({
      hardship: {
        [storeId]: {
          ...basicHardShipState,
          typeForm: {
            id: HARDSHIP_TYPE_ID.longTerm,
            value: HARDSHIP_TYPE_ID.longTerm
          },
          longTerm: {
            isDirty: true
          }
        }
      }
    });

    const dropdownList = screen.queryByText(HARDSHIP_TYPE_ID.longTerm);
    userEvent.click(dropdownList!);

    const longTerm = screen.getByTestId('DropdownList.onChange');

    fireEvent.change(longTerm!, {
      target: {
        value: { id: HARDSHIP_TYPE_ID.longTerm, value: 'Long-Term Hardship' }
      }
    });
    expect(updateTypeFormHardship).not.toHaveBeenCalled();
    expect(clearApiErrorData).not.toHaveBeenCalled();
    expect(openAction).toHaveBeenCalled();
  });
  it('select militaryDeployment  ', () => {
    const updateTypeFormHardship = mockHardShipAction('updateTypeFormHardship');
    const clearApiErrorData =
      mockApiErrorNotificationAction('clearApiErrorData');
    const openAction = confirmActions('open');
    renderHardShipForm({
      hardship: {
        [storeId]: {
          ...basicHardShipState,
          typeForm: {
            id: HARDSHIP_TYPE_ID.militaryDeployment,
            value: HARDSHIP_TYPE_ID.militaryDeployment
          }
        }
      }
    });

    const dropdownList = screen.queryByText(
      HARDSHIP_TYPE_ID.militaryDeployment
    );
    userEvent.click(dropdownList!);

    const longTerm = screen.getByTestId('DropdownList.onChange');

    fireEvent.change(longTerm!, {
      target: {
        value: { id: HARDSHIP_TYPE_ID.longTerm, value: 'Long-Term Hardship' }
      }
    });
    expect(updateTypeFormHardship).toHaveBeenCalled();
    expect(clearApiErrorData).toHaveBeenCalled();
    expect(openAction).not.toHaveBeenCalled();
  });

  it('select militaryDeployment  ', () => {
    const updateTypeFormHardship = mockHardShipAction('updateTypeFormHardship');
    const clearApiErrorData =
      mockApiErrorNotificationAction('clearApiErrorData');
    const openAction = confirmActions('open');
    renderHardShipForm({
      hardship: {
        [storeId]: {
          ...basicHardShipState,
          typeForm: {
            id: HARDSHIP_TYPE_ID.naturalDisaster,
            value: HARDSHIP_TYPE_ID.naturalDisaster
          }
        }
      },
      form: {
        [`${storeId}-${NATURAL_DISASTER}`]: {
          fields: {
            naturalDisasterImpact: {
              touched: true
            }
          },
          values: {
            naturalDisasterImpact: ''
          }
        }
      } as any
    });

    const dropdownList = screen.queryByText(HARDSHIP_TYPE_ID.naturalDisaster);
    userEvent.click(dropdownList!);

    const longTerm = screen.getByTestId('DropdownList.onChange');

    fireEvent.change(longTerm!, {
      target: {
        value: { id: HARDSHIP_TYPE_ID.longTerm, value: 'Long-Term Hardship' }
      }
    });
    expect(updateTypeFormHardship).not.toHaveBeenCalled();
    expect(clearApiErrorData).not.toHaveBeenCalled();
    expect(openAction).toHaveBeenCalled();
  });
});
