import React from 'react';
import { render, screen } from '@testing-library/react';

// components
import ViewForm from './ViewForm';
import { HARDSHIP_TYPE_ID } from '../constants';

// components
jest.mock('../HardshipForm', () => () => <div>Hardship</div>);
jest.mock('../LongTermHardship', () => () => <div>LongTerm</div>);
jest.mock('../MilitaryDeploymentHardship', () => () => (
  <div>MilitaryDeployment</div>
));
jest.mock('../NaturalDisaster', () => () => <div>NaturalDisaster</div>);

describe('Render', () => {
  it('Render UI', async () => {
    const { rerender } = render(<ViewForm type={HARDSHIP_TYPE_ID.hardship} />);
    expect(screen.getByText('Hardship')).toBeInTheDocument();

    rerender(<ViewForm type={HARDSHIP_TYPE_ID.longTerm} />);
    expect(screen.getByText('LongTerm')).toBeInTheDocument();

    rerender(<ViewForm type={HARDSHIP_TYPE_ID.militaryDeployment} />);
    expect(screen.getByText('MilitaryDeployment')).toBeInTheDocument();

    rerender(<ViewForm type={HARDSHIP_TYPE_ID.naturalDisaster} />);
    expect(screen.getByText('NaturalDisaster')).toBeInTheDocument();
  });
});
