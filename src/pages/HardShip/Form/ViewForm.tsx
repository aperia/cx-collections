import React from 'react';

// components
import Hardship from '../HardshipForm';
import LongTermHardShipForm from '../LongTermHardship';
import MilitaryDeploymentHardshipForm from '../MilitaryDeploymentHardship';

// type
import { HARDSHIP_TYPE_ID } from '../constants';
import NaturalDisasterForm from '../NaturalDisaster';

export interface Props {
  type: string;
}

const HardShipViewForm: React.FC<Props> = ({ type }) => {
  switch (type) {
    case HARDSHIP_TYPE_ID.naturalDisaster:
      return <NaturalDisasterForm />;
    case HARDSHIP_TYPE_ID.longTerm:
      return <LongTermHardShipForm />;
    // Military Deployment
    case HARDSHIP_TYPE_ID.militaryDeployment:
      return <MilitaryDeploymentHardshipForm />;
    case HARDSHIP_TYPE_ID.hardship:
    default:
      return <Hardship />;
  }
};

export default HardShipViewForm;
