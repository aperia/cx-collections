import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { hardshipService } from '../hardshipService';
import { HardshipState } from '../types';

export const getMonthlyOutgoInformation = createAsyncThunk<
  { value: string; description: string }[],
  StoreIdPayload,
  ThunkAPIConfig
>('hardship/getMonthlyOutgoInformation', async (_, thunkAPI) => {
  return await hardshipService.getMonthlyOutgoInformation();
});

export const getMonthlyOutgoInformationBuilder = (
  builder: ActionReducerMapBuilder<HardshipState>
) => {
  builder
    .addCase(getMonthlyOutgoInformation.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        longTerm: {
          ...draftState[storeId]?.longTerm,
          monthlyOutgoItemsLoading: true
        }
      };
    })
    .addCase(getMonthlyOutgoInformation.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const data = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        longTerm: {
          ...draftState[storeId].longTerm,
          monthlyOutgoItemsLoading: false,
          monthlyOutgoItems: data.map((item, index) => ({ ...item, index }))
        }
      };
    })
    .addCase(getMonthlyOutgoInformation.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        longTerm: {
          ...draftState[storeId].longTerm,
          monthlyOutgoItems: [],
          monthlyOutgoItemsLoading: false
        }
      };
    });
};
