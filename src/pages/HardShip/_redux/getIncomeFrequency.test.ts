import { hardshipActions } from './reducers';
import {
  createStoreWithDefaultMiddleWare,
  storeId,
  responseDefault
} from 'app/test-utils';
import { hardshipService } from '../hardshipService';
import { isFulfilled, isRejected } from '@reduxjs/toolkit';

const dataResponse = {
  status: 'success',
  data: {}
};

let spy: jest.SpyInstance;

describe('Name of the group', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('for success case', async () => {
    spy = jest.spyOn(hardshipService, 'getIncomeFrequency').mockResolvedValue({
      ...responseDefault,
      data: dataResponse
    });
    const store = createStoreWithDefaultMiddleWare({});
    const response = await hardshipActions.getIncomeFrequency({
      storeId
    })(store.dispatch, store.getState, {});
    expect(isFulfilled(response)).toBeTruthy;
  });

  it('for failure case', async () => {
    spy = jest
      .spyOn(hardshipService, 'getIncomeFrequency')
      .mockRejectedValue({});
    const store = createStoreWithDefaultMiddleWare({});
    const response = await hardshipActions.getIncomeFrequency({
      storeId
    })(store.dispatch, store.getState, {});
    expect(isRejected(response)).toBeTruthy;
  });
});
