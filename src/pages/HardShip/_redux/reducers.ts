import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Actions
import {
  HardshipState,
  SetFormLongTermPayload,
  SetReasonHardshipPayloadAction,
  ToggleConfirmModalPayloadAction,
  UpdateDataFromLastFollowUpPayloadAction,
  UpdateDataPayloadAction
} from '../types';
import {
  triggerUpdateHardship,
  triggerUpdateHardshipBuilder
} from './updateHardship';
import {
  triggerRemoveHardshipStatusRequest,
  removeHardshipStatusRequest,
  removeStatusCallResultBuilder
} from './removeHardshipStatus';
import {
  getGroupCheckboxCountryDeployment,
  getGroupCheckboxCountryDeploymentBuilder
} from './getGroupCheckboxCountryDeployment';
import pick from 'lodash.pick';
import { COMMON_FIELDS, HARDSHIP_TYPE, HARDSHIP_TYPE_ID } from '../constants';

import {
  getIncomeFrequencyBuilder,
  getIncomeFrequency
} from './getIncomeFrequency';

import {
  getMonthlyOutgoInformationBuilder,
  getMonthlyOutgoInformation
} from './getMonthlyOutgoInformation';

import { SetBooleanValue } from 'app/global-types/setValue';

// integrate api
import {
  getDetailsHardship,
  getDetailsHardshipBuilder
} from './getDetailsHardship';

import {
  getHardshipDebtManagementDetails,
  getHardshipDebtManagementDetailsBuilder
} from './hardshipDebtManagementDetails';

const { actions, reducer } = createSlice({
  name: 'hardship',
  initialState: {} as HardshipState,
  reducers: {
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    removeStoreForm: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        militaryDeployment: {},
        longTerm: {}
      };
    },
    updateData: (
      draftState,
      action: PayloadAction<UpdateDataPayloadAction>
    ) => {
      const { storeId, data } = action.payload;

      const commonData = pick(data, COMMON_FIELDS);
      const type = data?.typeHardship || HARDSHIP_TYPE_ID.hardship;

      const typeForm = HARDSHIP_TYPE.filter(item => item.id === type)[0];

      draftState[storeId] = {
        ...draftState[storeId],
        data: {
          ...data,
          minimumNewFixedPaymentAmount: data.currentBalance
        },
        commonData: {
          ...commonData,
          minimumNewFixedPaymentAmount: data.currentBalance
        },
        typeForm
      };
    },
    updateDataFromLastFollowUp: (
      draftState,
      action: PayloadAction<UpdateDataFromLastFollowUpPayloadAction>
    ) => {
      const { storeId, data } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        callResultType: data?.callResultType,
        data: data?.data || {}
      };
    },
    setReasonHardship: (
      draftState,
      action: PayloadAction<SetReasonHardshipPayloadAction>
    ) => {
      const { storeId, value, reason } = action.payload;
      draftState[storeId] = { ...draftState[storeId], reasonHardship: value, reason };
    },
    updateTypeFormHardship: (
      draftState,
      action: PayloadAction<{
        storeId: string;
        typeForm: {
          id: string;
          value: string;
        };
      }>
    ) => {
      const { storeId, typeForm } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        typeForm
      };
    },
    setDataFormHardship: (
      draftState,
      action: PayloadAction<StoreIdPayload>
    ) => {
      const { storeId } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        data: draftState[storeId]?.commonData as MagicKeyValue
      };
    },
    setFormLongIsValid: (
      draftState,
      action: PayloadAction<SetFormLongTermPayload>
    ) => {
      const { storeId, isValid } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        longTerm: {
          ...draftState[storeId].longTerm,
          isValid
        }
      };
    },

    setFormLongIsDirty: (
      draftState,
      action: PayloadAction<SetFormLongTermPayload>
    ) => {
      const { storeId, isDirty } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        longTerm: {
          ...draftState[storeId].longTerm,
          isDirty
        }
      };
    },
    setIsSubmit: (draftState, action: PayloadAction<SetBooleanValue>) => {
      const { storeId, value } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isSubmit: value
      };
    },
    setFormLongTermValues: (
      draftState,
      action: PayloadAction<SetFormLongTermPayload>
    ) => {
      const { storeId, values } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        longTerm: {
          ...draftState[storeId].longTerm,
          values
        }
      };
    },
    setActiveTabMilitary: (draftState, action) => {
      const { storeId, value } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        resultHardship: {
          ...draftState[storeId].resultHardship,
          selectedMilitaryTab: value
        }
      };
    },
    toggleConfirmModal: (
      draftState,
      action: PayloadAction<ToggleConfirmModalPayloadAction>
    ) => {
      const { storeId, opened } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        showConfirmModal: opened
      };
    }
  },
  extraReducers: builder => {
    getGroupCheckboxCountryDeploymentBuilder(builder);
    removeStatusCallResultBuilder(builder);
    getIncomeFrequencyBuilder(builder);
    getMonthlyOutgoInformationBuilder(builder);
    triggerUpdateHardshipBuilder(builder);
    getDetailsHardshipBuilder(builder);
    getHardshipDebtManagementDetailsBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getGroupCheckboxCountryDeployment,
  getMonthlyOutgoInformation,
  getIncomeFrequency,
  triggerUpdateHardship,
  triggerRemoveHardshipStatusRequest,
  removeHardshipStatusRequest,
  getDetailsHardship,
  getHardshipDebtManagementDetails
};

export { allActions as hardshipActions, reducer };
