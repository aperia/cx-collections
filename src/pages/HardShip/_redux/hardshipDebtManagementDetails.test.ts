import { hardshipActions } from './reducers';
import {
  createStoreWithDefaultMiddleWare,
  responseDefault,
  storeId
} from 'app/test-utils';
import { hardshipService } from '../hardshipService';
import { isFulfilled, isRejected } from '@reduxjs/toolkit';
import { VIEW_TYPE } from '../constants';

let spy: jest.SpyInstance;

describe('getHardshipDebtManagementDetails', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('for success case', async () => {
    spy = jest
      .spyOn(hardshipService, 'getHardshipDebtManagementDetails')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          hardshipType: VIEW_TYPE.BASIC,
          processStatus: 'COMPLETE'
        }
      });
    const store = createStoreWithDefaultMiddleWare({
      collectionForm: {
        [storeId]: {
          inProgressInfo: { processStatus: 'COMPLETE' },
          isInProgress: true,
          lastDelayCallResult: "HS"
        }
      }
    });
    const response = await hardshipActions.getHardshipDebtManagementDetails({
      postData: {
        storeId,
        accountId: storeId
      }
    })(store.dispatch, store.getState, {});
    expect(isFulfilled(response)).toBeTruthy;
  });

  it('for success case long term harship', async () => {
    spy = jest
      .spyOn(hardshipService, 'getHardshipDebtManagementDetails')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          hardshipType: VIEW_TYPE.LONG_TERM,
          processStatus: 'processStatus'
        }
      });
    const store = createStoreWithDefaultMiddleWare({
      collectionForm: {
        [storeId]: {
          inProgressInfo: {},
          isInProgress: true
        }
      }
    });
    const response = await hardshipActions.getHardshipDebtManagementDetails({
      postData: {
        storeId,
        accountId: storeId
      }
    })(store.dispatch, store.getState, {});
    expect(isFulfilled(response)).toBeTruthy;
  });

  it('for success case long term harship', async () => {
    spy = jest
      .spyOn(hardshipService, 'getHardshipDebtManagementDetails')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          hardshipType: VIEW_TYPE.MILITARY_DEPLOYMENT,
          processStatus: 'processStatus'
        }
      });
    const store = createStoreWithDefaultMiddleWare({
      collectionForm: {
        [storeId]: {
          inProgressInfo: {},
          isInProgress: true
        }
      }
    });
    const response = await hardshipActions.getHardshipDebtManagementDetails({
      postData: {
        storeId,
        accountId: storeId
      }
    })(store.dispatch, store.getState, {});
    expect(isFulfilled(response)).toBeTruthy;
  });
  it('for success case long term harship', async () => {
    spy = jest
      .spyOn(hardshipService, 'getHardshipDebtManagementDetails')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          hardshipType: VIEW_TYPE.ND,
          processStatus: 'processStatus'
        }
      });
    const store = createStoreWithDefaultMiddleWare({
      collectionForm: {
        [storeId]: {
          inProgressInfo: {},
          isInProgress: true
        }
      }
    });
    const response = await hardshipActions.getHardshipDebtManagementDetails({
      postData: {
        storeId,
        accountId: storeId
      }
    })(store.dispatch, store.getState, {});
    expect(isFulfilled(response)).toBeTruthy;
  });

  it('for success case long term harship', async () => {
    spy = jest
      .spyOn(hardshipService, 'getHardshipDebtManagementDetails')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });
    const store = createStoreWithDefaultMiddleWare({});
    const response = await hardshipActions.getHardshipDebtManagementDetails({
      postData: {
        storeId,
        accountId: storeId
      }
    })(store.dispatch, store.getState, {});
    expect(isFulfilled(response)).toBeTruthy;
  });

  it('for failure case', async () => {
    spy = jest
      .spyOn(hardshipService, 'getHardshipDebtManagementDetails')
      .mockRejectedValue({});
    const store = createStoreWithDefaultMiddleWare({});
    const response = await hardshipActions.getHardshipDebtManagementDetails({
      postData: {
        storeId,
        accountId: storeId
      }
    })(store.dispatch, store.getState, {});
    expect(isRejected(response)).toBeTruthy;
  });
});
