import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { EMPTY_STRING, INVALID_DATE } from 'app/constants';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { formatTime, mappingDataFromObj } from 'app/helpers';
import isEmpty from 'lodash.isempty';
import pick from 'lodash.pick';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import {
  dispatchDelayProgress,
  setDelayProgress
} from 'pages/CollectionForm/helpers';
import { getLatestActionRequest } from 'pages/CollectionForm/_redux/getLatestAction';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import {
  HARDSHIP_IN_PROGRESS,
  HARDSHIP_TYPE_ID,
  POST_FIELDS,
  POST_LONG_TERM_FIELDS,
  POST_MILITARY_DEPLOYMENT_FIELDS,
  POST_NATURAL_FIELDS,
  STATUS_HARDSHIP
} from '../constants';
import { hardshipService } from '../hardshipService';
import { mapDataToPost as mapDataLongTermToPost } from '../LongTermHardship/helper';
import { mapDataToPost as mapDataMilitaryToPost } from '../MilitaryDeploymentHardship/helper';
import {
  HardshipFormData,
  HardshipState,
  SetupHardshipArgs,
  TriggerUpdateHardshipArg,
  TRIGGER_TYPE,
  UpdateHardshipRequestPayload
} from '../types';
import { hardshipActions } from './reducers';

export const updateHardshipRequest = createAsyncThunk<
  UpdateHardshipRequestPayload,
  SetupHardshipArgs,
  ThunkAPIConfig
>('hardship/updateHardshipRequest', async (args, thunkAPI) => {
  const requestData = args.postData;

  try {
    const response = await hardshipService.updateHardship(requestData);

    if (response.data?.status !== STATUS_HARDSHIP.SUCCESS) {
      throw response;
    }
    return response.data;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerUpdateHardship = createAsyncThunk<
  unknown,
  TriggerUpdateHardshipArg,
  ThunkAPIConfig
>('hardship/triggerUpdateHardship', async (args, thunkAPI) => {
  const {
    storeId,
    postData: { viewName = [], type, typeForm, dataDetails }
  } = args;
  const { dispatch, getState } = thunkAPI;
  const { form, hardship, accountDetail } = getState();
  const state = thunkAPI.getState();

  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;

  const { org, app, callingApplicationAPP1, operatorID } =
    window.appConfig?.commonConfig || {};

  const common = {
    org: org || EMPTY_STRING,
    app: app || EMPTY_STRING,
    accountId: eValueAccountId
  };

  let data = {} as MagicKeyValue;
  switch (typeForm) {
    case HARDSHIP_TYPE_ID.naturalDisaster:
      const dataNatural = form[viewName[0]]?.values;
      const dataRes = pick(dataNatural, POST_NATURAL_FIELDS);
      const mapping = state.mapping?.data?.naturalHardship || {};
      const dataMapObj = mappingDataFromObj(dataRes, mapping);
      const naturalDisaster = isEmpty(dataRes?.reagePaymentPeriod)
        ? {
            naturalDisasterHardship: {
              naturalDisasterType: dataRes?.naturalDisasterType,
              naturalDisasterImpact: dataRes?.naturalDisasterImpact,
              naturalDisasterManualReAgeDate: formatTime(
                dataRes?.manualReage || EMPTY_STRING
              )
                .date?.split('/')
                ?.join('-')
            }
          }
        : {
            naturalDisasterHardship: {
              naturalDisasterType: dataRes?.naturalDisasterType,
              naturalDisasterImpact: dataRes?.naturalDisasterImpact
            },
            reAgePaymentPeriod: dataRes?.reagePaymentPeriod?.value
          };
      data = {
        ...dataMapObj,
        ...naturalDisaster,
        ctdPaymentAmount: `${dataMapObj?.ctdPaymentAmount}`,
        newFixedPaymentAmount: `${dataMapObj?.newFixedPaymentAmount}`,
        nonDelinquentMinimumPaymentDueAmount: `${dataMapObj?.nonDelinquentMinimumPaymentDueAmount}`,

        startDate: formatTime(dataMapObj?.startDate || EMPTY_STRING)
          .date?.split('/')
          ?.join('-'),
        endDate: formatTime(dataMapObj?.endDate || EMPTY_STRING)
          .date?.split('/')
          ?.join('-'),
        hardshipType: 'ND'
      };
      break;
    case HARDSHIP_TYPE_ID.longTerm:
      data = {
        ...pick(form[viewName[0]]?.values, POST_LONG_TERM_FIELDS),
        ...hardship[storeId]?.longTerm?.values
      };
      const mappingLongTerm = state.mapping?.data?.longTermHardship || {};
      data = mapDataLongTermToPost(data, mappingLongTerm);

      break;
    case HARDSHIP_TYPE_ID.militaryDeployment:
      const mappingMilitary = state.mapping?.data?.militaryHardship || {};
      data = {
        ...pick(form[viewName[0]]?.values, POST_MILITARY_DEPLOYMENT_FIELDS)
      };
      data = mapDataMilitaryToPost(data, mappingMilitary);
      break;
    case HARDSHIP_TYPE_ID.hardship:
    default:
      const formData = pick(form[viewName[0]]?.values, POST_FIELDS);
      const mappingModel = state.mapping?.data?.hardshipForm || {};
      const dataMapping = mappingDataFromObj<HardshipFormData>(
        formData,
        mappingModel
      );

      data = {
        ...dataMapping,
        ctdPaymentAmount: `${dataMapping?.ctdPaymentAmount}`,
        newFixedPaymentAmount: `${dataMapping?.newFixedPaymentAmount}`,
        nonDelinquentMinimumPaymentDueAmount: `${dataMapping?.nonDelinquentMinimumPaymentDueAmount}`,
        startDate: formatTime(dataMapping?.startDate || EMPTY_STRING)
          .date?.split('/')
          ?.join('-'),
        endDate: formatTime(dataMapping?.endDate || EMPTY_STRING)
          .date?.split('/')
          ?.join('-'),
        hardshipType: 'BH'
      };
  }

  const requestBody = {
    common,
    ...data,
    callingApplication: callingApplicationAPP1 || EMPTY_STRING,
    operatorCode: operatorID || EMPTY_STRING,
    cardholderContactedName: dataDetails?.cardholderContactedName,
    workoutLastDate: !INVALID_DATE.includes(dataDetails?.workoutLastDate)
      ? formatTime(dataDetails?.workoutLastDate).fullYearMonthDay
      : dataDetails?.workoutLastDate,
    debtMgmtCode: dataDetails?.debtMgmtCode,
    debtMgmtCodeValue: dataDetails?.debtMgmtCodeValue,
    pricingStrategyLockStatus: dataDetails?.pricingStrategyLockStatus,
    currentBalanceAmount: dataDetails?.currentBalanceAmount,
    phoneCalledNumber: accountDetail[storeId]?.numberPhoneAuthenticated
  };

  const response = await dispatch(
    updateHardshipRequest({
      storeId,
      postData: requestBody
    })
  );

  if (isFulfilled(response)) {
    batch(() => {
      const { isAdminRole, isCollector } = window.appConfig?.commonConfig || {};
      // open modal Upload file
      if (
        typeForm === HARDSHIP_TYPE_ID.militaryDeployment &&
        (isAdminRole || isCollector)
      ) {
        dispatch(
          collectionFormActions.openUploadFileModal({
            storeId,
            type: 'hardShip'
          })
        );
      }
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message:
            type === TRIGGER_TYPE.UPDATE
              ? I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATED
              : I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
        })
      );

      // clear data in detail hardship
      dispatch(hardshipActions.removeStoreForm({ storeId }));
      dispatch(collectionFormActions.setCurrentView({ storeId }));
      dispatch(
        collectionFormActions.selectCallResult({ storeId, item: undefined })
      );

      // delay message
      dispatch(
        collectionFormActions.toggleCallResultInProgressStatus({
          storeId,
          info: {
            processStatus: HARDSHIP_IN_PROGRESS
          },
          lastDelayCallResult: CALL_RESULT_CODE.HARDSHIP
        })
      );

      dispatchDelayProgress(
        hardshipActions.getHardshipDebtManagementDetails({
          postData: {
            storeId
          }
        }),
        storeId
      );

      setDelayProgress(storeId, CALL_RESULT_CODE.HARDSHIP, {
        processStatus: HARDSHIP_IN_PROGRESS
      });

      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inCollectionFormFlyOut'
        })
      );

      dispatch(
        getLatestActionRequest({
          storeId
        })
      );
    });
  }

  if (isRejected(response)) {
    batch(() => {
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          apiResponse: response.payload?.response,
          forSection: 'inCollectionFormFlyOut'
        })
      );
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message:
            type === TRIGGER_TYPE.UPDATE
              ? I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATE_FAILED
              : I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMIT_FAILED
        })
      );
    });
  }
});

export const triggerUpdateHardshipBuilder = (
  builder: ActionReducerMapBuilder<HardshipState>
) => {
  builder
    .addCase(triggerUpdateHardship.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true
      };
    })
    .addCase(triggerUpdateHardship.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isSubmit: false
      };
    })
    .addCase(triggerUpdateHardship.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isSubmit: false
      };
    });
};
