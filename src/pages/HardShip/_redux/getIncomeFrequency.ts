import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { HardshipState } from '../types';

import { hardshipService } from '../hardshipService';

export const getIncomeFrequency = createAsyncThunk<
  MagicKeyValue[],
  StoreIdPayload,
  ThunkAPIConfig
>('hardship/getIncomeFrequency', async (_, thunkAPI) => {
  return await hardshipService.getIncomeFrequency();
});

export const getIncomeFrequencyBuilder = (
  builder: ActionReducerMapBuilder<HardshipState>
) => {
  builder
    .addCase(getIncomeFrequency.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        longTerm: {
          ...draftState[storeId]?.longTerm,
          incomeFrequenciesLoading: true
        }
      };
    })
    .addCase(getIncomeFrequency.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const data = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        longTerm: {
          ...draftState[storeId].longTerm,
          incomeFrequenciesLoading: false,
          incomeFrequencies: data
        }
      };
    })
    .addCase(getIncomeFrequency.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        longTerm: {
          ...draftState[storeId].longTerm,
          incomeFrequenciesLoading: false,
          incomeFrequencies: []
        }
      };
    });
};
