import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { mappingDataFromObj } from 'app/helpers';

// Service
import { hardshipService } from '../hardshipService';

// Type
import { GetDetailHardshipArgs, HardshipState } from '../types';

export const getDetailsHardship = createAsyncThunk<
  MagicKeyValue,
  GetDetailHardshipArgs,
  ThunkAPIConfig
>('getDetailsHardship', async (args, thunkAPI) => {
  const { hardshipType, storeId } = args.postData;
  const state = thunkAPI.getState();

  const { accountDetail } = state;

  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;

  const {
    user,
    org = '',
    app = '',
    callingApplicationAPP1 = '',
    operatorID: operatorCode
  } = window.appConfig?.commonConfig || {};

  try {
    const requestBody = {
      common: {
        user: user,
        org: org,
        app: app,
        accountId: eValueAccountId
      },
      callingApplication: callingApplicationAPP1,
      operatorCode,
      hardshipType
    };
    const { data } = await hardshipService.getDetailsHardship(requestBody);

    const mappingModel = state.mapping?.data?.hardshipDetails || {};
    const hardshipCommonData = mappingDataFromObj<MagicKeyValue>(
      data,
      mappingModel
    );
    const commonData = {
      currentBalance: parseFloat(hardshipCommonData?.currentBalance),
      cycleToDatePaymentsAmount: parseFloat(
        hardshipCommonData?.cycleToDatePaymentsAmount
      ),
      nonDelinquentMpdAmount: parseFloat(
        hardshipCommonData?.nonDelinquentMpdAmount
      ),
      minimumNewFixedPaymentAmount: parseFloat(
        hardshipCommonData?.currentBalance
      ),
      newFixedPaymentAmount: parseFloat(
        hardshipCommonData?.newFixedPaymentAmount
      )
    };
    return {
      dataDetails: data,
      commonData
    };
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const getDetailsHardshipBuilder = (
  builder: ActionReducerMapBuilder<HardshipState>
) => {
  builder
    .addCase(getDetailsHardship.pending, (draftState, action) => {
      const { postData } = action.meta.arg;
      const { storeId } = postData;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true
      };
    })
    .addCase(getDetailsHardship.fulfilled, (draftState, action) => {
      const { postData } = action.meta.arg;
      const { storeId } = postData;
      const { commonData, dataDetails } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        commonData: commonData,
        dataDetails
      };
    })
    .addCase(getDetailsHardship.rejected, (draftState, action) => {
      const { postData } = action.meta.arg;
      const { storeId } = postData;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false
      };
    });
};
