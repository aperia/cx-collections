import { hardshipActions } from './reducers';
import { createStoreWithDefaultMiddleWare, storeId } from 'app/test-utils';
import { hardshipService } from '../hardshipService';
import { isFulfilled, isRejected } from '@reduxjs/toolkit';

const dataResponse = [{}];

let spy: jest.SpyInstance;

describe('Name of the group', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('for success case', async () => {
    spy = jest
      .spyOn(hardshipService, 'getMonthlyOutgoInformation')
      .mockResolvedValue(dataResponse);
    const store = createStoreWithDefaultMiddleWare({});
    const response = await hardshipActions.getMonthlyOutgoInformation({
      storeId
    })(store.dispatch, store.getState, {});
    expect(isFulfilled(response)).toBeTruthy;
  });

  it('for failure case', async () => {
    spy = jest
      .spyOn(hardshipService, 'getMonthlyOutgoInformation')
      .mockRejectedValue({});
    const store = createStoreWithDefaultMiddleWare({});
    const response = await hardshipActions.getMonthlyOutgoInformation({
      storeId
    })(store.dispatch, store.getState, {});
    expect(isRejected(response)).toBeTruthy;
  });
});
