import { hardshipActions } from './reducers';
import {
  createStoreWithDefaultMiddleWare,
  storeId,
  responseDefault
} from 'app/test-utils';
import { hardshipService } from '../hardshipService';
import { isFulfilled, isRejected } from '@reduxjs/toolkit';

const requestBody = {
  postData: {
    storeId,
    accountId: storeId
  }
};

const dataResponse = {
  status: 'success',
  debtMgmtCode: 'PS',
  debtMgmtCodeValue: '3CAP',
  pricingStrategyLockStatus: 'U',
  ctdPaymentAmount: '0000000000000000.00',
  newFixedPaymentAmount: '111.00',
  nonDelinquentMinimumPaymentDueAmount: '0000000000000100.00',
  activeHardshipExists: false,
  cardholderContactedName: 'SMITH,ADELE               ',
  currentBalanceAmount: '0000000001110.00',
  miscellaneousField1Identifier: '    ',
  workoutLastDate: '00000000'
};

let spy: jest.SpyInstance;

describe('Name of the group', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('for success case', async () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        user: 'user',
        org: 'org',
        app: 'app',
        callingApplicationAPP1: 'test',
        operatorCode: 'code'
      } as CommonConfig
    };
    spy = jest.spyOn(hardshipService, 'getDetailsHardship').mockResolvedValue({
      ...responseDefault,
      data: dataResponse
    });
    const store = createStoreWithDefaultMiddleWare({});
    const response = await hardshipActions.getDetailsHardship(requestBody)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(isFulfilled(response)).toBeTruthy;
  });

  it('for failure case', async () => {
    window.appConfig = {} as AppConfiguration;
    spy = jest
      .spyOn(hardshipService, 'getDetailsHardship')
      .mockRejectedValue({});
    const store = createStoreWithDefaultMiddleWare({});
    const response = await hardshipActions.getDetailsHardship(requestBody)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(isRejected(response)).toBeTruthy;
  });
});
