import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { HardshipState, RemoveStatusArgs } from '../types';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { hardshipService } from '../hardshipService';
import { EMPTY_STRING } from 'app/constants';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import {
  dispatchDelayProgress,
  setDelayProgress
} from 'pages/CollectionForm/helpers';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { hardshipActions } from './reducers';
import { HARDSHIP_COMPLETE, STATUS_HARDSHIP } from '../constants';
import { batch } from 'react-redux';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export const removeHardshipStatusRequest = createAsyncThunk<
  unknown,
  RemoveStatusArgs,
  ThunkAPIConfig
>('hardship/removeHardshipStatusRequest', async (args, thunkAPI) => {
  try {
    const state = thunkAPI.getState();
    const { storeId } = args.postData;
    const { commonConfig } = window.appConfig || {};
    const { accountDetail } = state;

    const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;

    const hardshipStatusRemovalReason = state.hardship[storeId]?.reason;

    const requestBody = {
      common: {
        org: commonConfig?.org || EMPTY_STRING,
        app: commonConfig?.app || EMPTY_STRING,
        accountId: eValueAccountId
      },
      callingApplication: commonConfig?.callingApplicationAPP1 || EMPTY_STRING,
      operatorCode: commonConfig?.operatorID || EMPTY_STRING,
      hardshipStatusRemovalReason
    };

    const response = await hardshipService.removeHardship(requestBody);

    if (response.data?.status !== STATUS_HARDSHIP.SUCCESS) {
      throw response;
    }
    return { data: response.data };
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerRemoveHardshipStatusRequest = createAsyncThunk<
  unknown,
  RemoveStatusArgs,
  ThunkAPIConfig
>('hardship/triggerRemoveHardshipStatusRequest', async (args, thunkAPI) => {
  const { dispatch } = thunkAPI;
  const { storeId } = args.postData;

  const response = await dispatch(removeHardshipStatusRequest(args));

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_COLLECTION_FORM.REMOVE_HARDSHIP_SUCCESS
        })
      );
      // delay message
      dispatch(
        collectionFormActions.toggleCallResultInProgressStatus({
          storeId,
          info: {
            processStatus: HARDSHIP_COMPLETE
          },
          lastDelayCallResult: CALL_RESULT_CODE.NOT_HARDSHIP
        })
      );

      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inCollectionFormFlyOut'
        })
      );

      dispatchDelayProgress(
        hardshipActions.getHardshipDebtManagementDetails({
          postData: {
            storeId
          }
        }),
        storeId
      );

      setDelayProgress(storeId, CALL_RESULT_CODE.NOT_HARDSHIP, {
        processStatus: undefined
      });
    });
  }
  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_COLLECTION_FORM.REMOVE_HARDSHIP_FAILURE
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inCollectionFormFlyOut',
          apiResponse: response.payload?.response
        })
      );
    });
  }
});

export const removeStatusCallResultBuilder = (
  builder: ActionReducerMapBuilder<HardshipState>
) => {
  builder
    .addCase(
      removeHardshipStatusRequest.pending,
      (draftState, action) => undefined
    )
    .addCase(
      removeHardshipStatusRequest.fulfilled,
      (draftState, action) => undefined
    )
    .addCase(
      removeHardshipStatusRequest.rejected,
      (draftState, action) => undefined
    );
};
