import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { HardshipState } from '../types';

import { hardshipService } from '../hardshipService';
import { GroupCheckboxCountryDeployment } from '../MilitaryDeploymentHardship/types';

export const getGroupCheckboxCountryDeployment = createAsyncThunk<
  GroupCheckboxCountryDeployment[],
  StoreIdPayload,
  ThunkAPIConfig
>('hardship/getGroupCheckboxCountryDeployment', async (_, thunkAPI) => {
  return await hardshipService.getGroupCheckboxCountryDeployment();
});

export const getGroupCheckboxCountryDeploymentBuilder = (
  builder: ActionReducerMapBuilder<HardshipState>
) => {
  builder
    .addCase(
      getGroupCheckboxCountryDeployment.pending,
      (draftState, action) => {
        const { storeId } = action.meta.arg;

        draftState[storeId] = {
          ...draftState[storeId],
          militaryDeployment: {
            ...draftState[storeId]?.militaryDeployment,
            loading: true
          }
        };
      }
    )
    .addCase(
      getGroupCheckboxCountryDeployment.fulfilled,
      (draftState, action) => {
        const { storeId } = action.meta.arg;
        const data = action.payload;

        draftState[storeId] = {
          ...draftState[storeId],
          militaryDeployment: {
            ...draftState[storeId].militaryDeployment,
            groupCheckboxCountryDeployment: data,
            loading: false
          }
        };
      }
    )
    .addCase(
      getGroupCheckboxCountryDeployment.rejected,
      (draftState, action) => {
        const { storeId } = action.meta.arg;
        draftState[storeId] = {
          ...draftState[storeId],
          militaryDeployment: {
            ...draftState[storeId].militaryDeployment,
            loading: false,
            groupCheckboxCountryDeployment: []
          }
        };
      }
    );
};
