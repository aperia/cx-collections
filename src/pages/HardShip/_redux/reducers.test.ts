import { hardshipActions, reducer } from './reducers';
import { storeId } from 'app/test-utils';
import { HardshipState } from '../types';
import { HARDSHIP_TYPE } from '../constants';

const {
  removeStore,
  removeStoreForm,
  updateData,
  updateDataFromLastFollowUp,
  setReasonHardship,
  updateTypeFormHardship,
  setFormLongIsValid,
  setFormLongIsDirty,
  setIsSubmit,
  setFormLongTermValues,
  setDataFormHardship,
  toggleConfirmModal,
  setActiveTabMilitary
} = hardshipActions;

const initialState: HardshipState = {
  [storeId]: {
    typeForm: HARDSHIP_TYPE[1],
    commonData: {},
    reasonHardship: {},
    data: {},
    dataDetails: {},
    longTerm: {},
    militaryDeployment: {},
    resultHardship: {}
  }
};

describe('Hardship > reducer', () => {
  it('removeStore', () => {
    const params = { storeId };
    const state = reducer(initialState, removeStore(params));
    const result = state[storeId];
    expect(result).toEqual(undefined);
  });

  it('removeStoreForm', () => {
    const params = { storeId };
    const state = reducer(initialState, removeStoreForm(params));
    const result = state[storeId];
    expect(result.militaryDeployment).toEqual({});
    expect(result.longTerm).toEqual({});
  });

  it('updateData', () => {
    const params = {
      storeId,
      data: {
        currentBalance: '12',
        other: 'other'
      }
    };
    const state = reducer(initialState, updateData(params));
    const result = state[storeId];
    expect(result.data.minimumNewFixedPaymentAmount).toEqual(
      params.data.currentBalance
    );
    expect(result?.commonData?.minimumNewFixedPaymentAmount).toEqual(
      params.data.currentBalance
    );
  });

  describe('updateDataFromLastFollowUp', () => {
    it('empty data', () => {
      const params = { storeId };
      const state = reducer(initialState, updateDataFromLastFollowUp(params));
      const result = state[storeId];
      expect(result.data).toEqual({});
    });
    it('data', () => {
      const params = { storeId, data: { data: { data1: 'data1' } } };
      const state = reducer(initialState, updateDataFromLastFollowUp(params));
      const result = state[storeId];
      expect(result.data).toEqual(params.data.data);
    });
  });

  it('setReasonHardship', () => {
    // invoke
    const params = { storeId, value: 'reasonHardship' };
    const state = reducer(initialState, setReasonHardship(params));

    // expect
    expect(state[storeId].reasonHardship).toEqual('reasonHardship');
  });

  it('updateTypeFormHardship', () => {
    // invoke
    const params = { storeId, typeForm: HARDSHIP_TYPE[0] };
    const state = reducer(initialState, updateTypeFormHardship(params));

    // expect
    expect(state[storeId].typeForm).toEqual(HARDSHIP_TYPE[0]);
  });

  it('setDataFormHardship', () => {
    // invoke
    const params = { storeId };
    const commonData = {};
    const state = reducer(
      { [storeId]: { commonData } },
      setDataFormHardship(params)
    );

    // expect
    expect(state[storeId]).toEqual({ commonData, data: commonData });
  });

  it('setFormLongIsValid', () => {
    const params = { storeId, isValid: true };
    const state = reducer(initialState, setFormLongIsValid(params));
    const result = state[storeId];
    expect(result?.longTerm?.isValid).toEqual(params.isValid);
  });

  it('setFormLongIsDirty', () => {
    const params = { storeId, isDirty: true };
    const state = reducer(initialState, setFormLongIsDirty(params));
    const result = state[storeId];
    expect(result?.longTerm?.isDirty).toEqual(params.isDirty);
  });

  it('setIsSubmit', () => {
    const params = { storeId, value: true };
    const state = reducer(initialState, setIsSubmit(params));
    const result = state[storeId];
    expect(result?.isSubmit).toEqual(params.value);
  });

  it('setFormLongTermValues', () => {
    const params = {
      storeId,
      values: {
        outgoMultiSelectItems: [],
        outgoInputItems: {},
        incomeAmount: '12'
      }
    };
    const state = reducer(initialState, setFormLongTermValues(params));
    const result = state[storeId];
    expect(result.longTerm?.values).toEqual(params.values);
  });

  it('toggleConfirmModal', () => {
    const params = { storeId, opened: true };
    const state = reducer(initialState, toggleConfirmModal(params));
    const result = state[storeId];
    expect(result.showConfirmModal).toEqual(params.opened);
  });

  it('setActiveTabMilitary', () => {
    const params = { storeId, value: 'MD' };
    const state = reducer(initialState, setActiveTabMilitary(params));
    const result = state[storeId];
    expect(result.resultHardship?.selectedMilitaryTab).toEqual(params.value);
  });
});
