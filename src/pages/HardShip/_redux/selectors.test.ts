import * as selector from './selectors';

// utils
import {
  HARDSHIP_IN_PROGRESS,
  HARDSHIP_TYPE,
  REASON_REMOVE_HARDSHIP
} from '../constants';
import { selectorWrapper, storeId } from 'app/test-utils';

const states: Partial<RootState> = {
  hardship: {
    [storeId]: {
      isLoadingView: true,
      isSubmit: true,
      isLoading: true,
      showConfirmModal: true,
      data: { ky: 'value', processStatus: HARDSHIP_IN_PROGRESS },
      commonData: {},
      reasonHardship: REASON_REMOVE_HARDSHIP[0],
      typeForm: { id: 'typeForm', value: 'value' },
      status: 'true'
    } as any
  }
};

const selectorTrigger = selectorWrapper(states);

describe('HardShip > selector', () => {
  it('selectHardshipInfo', () => {
    const { data, emptyData } = selectorTrigger(selector.selectHardshipInfo);
    expect(data).toEqual(states.hardship![storeId].data);
    expect(emptyData).toEqual(undefined);
  });

  it('selectHardshipDataForm', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectHardshipDataForm
    );
    expect(data).toEqual(states.hardship![storeId].commonData);
    expect(emptyData).toEqual(undefined);
  });

  it('selectHardshipDetailsData', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectHardshipDetailsData
    );
    expect(data).toEqual(states.hardship![storeId].dataDetails);
    expect(emptyData).toEqual(undefined);
  });

  it('selectReasonRemove', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectReasonRemove(storeId)
    );

    expect(data).toEqual(states.hardship![storeId].reasonHardship);
    expect(emptyData).toEqual(REASON_REMOVE_HARDSHIP[0]);
  });

  it('selectTypeFormHardship', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectTypeFormHardship
    );

    expect(data).toEqual(states.hardship![storeId].typeForm);
    expect(emptyData).toEqual(HARDSHIP_TYPE[0]);
  });

  it('selectIsValidLongTermHardship', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectIsValidLongTermHardship
    );
    expect(data).toEqual(states.hardship![storeId]?.longTerm?.isValid);
    expect(emptyData).toEqual(undefined);
  });

  it('selectIsDirtyLongTermHardship', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectIsDirtyLongTermHardship
    );
    expect(data).toEqual(states.hardship![storeId]?.longTerm?.isDirty);
    expect(emptyData).toEqual(undefined);
  });

  it('selectIsSubmit', () => {
    const { data, emptyData } = selectorTrigger(selector.selectIsSubmit);
    expect(data).toEqual(states.hardship![storeId]?.isSubmit);
    expect(emptyData).toEqual(undefined);
  });

  it('selectIsLoading', () => {
    const { data, emptyData } = selectorTrigger(selector.selectIsLoading);
    expect(data).toEqual(states.hardship![storeId]?.isLoading);
    expect(emptyData).toEqual(undefined);
  });

  it('selectIsLoadingView', () => {
    const { data, emptyData } = selectorTrigger(selector.selectIsLoadingView);
    expect(data).toEqual(states.hardship![storeId]?.isLoadingView);
    expect(emptyData).toEqual(false);
  });

  it('selectIsOpenConfirmModal', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectIsOpenConfirmModal
    );
    expect(data).toEqual(states.hardship![storeId]?.showConfirmModal);
    expect(emptyData).toEqual(false);
  });
  it('selectIsDisableRemove', () => {
    const { data, emptyData } = selectorTrigger(selector.selectIsDisableRemove);
    expect(data).toEqual(states.hardship![storeId]?.showConfirmModal);
    expect(emptyData).toEqual(false);
  });
  it('selectStatus', () => {
    const { data, emptyData } = selectorTrigger(selector.selectStatus);
    expect(data).toEqual(states.hardship![storeId]?.status);
    expect(emptyData).toEqual(undefined);
  });
});
