import './reducers';
import { updateHardshipRequest, triggerUpdateHardship } from './updateHardship';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import {
  byPassFulfilled,
  createStoreWithDefaultMiddleWare,
  mockActionCreator,
  responseDefault,
  storeId
} from 'app/test-utils';

import { hardshipService } from '../hardshipService';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { TRIGGER_TYPE } from '../types';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { HARDSHIP_TYPE_ID, STATUS_HARDSHIP } from '../constants';
import { isRejected } from '@reduxjs/toolkit';

let spy: jest.SpyInstance;
let spy2: jest.SpyInstance;
let spy3: jest.SpyInstance;
let spy4: jest.SpyInstance;

const runMockSuccessUpdate = (status: string) =>
  jest.spyOn(hardshipService, 'updateHardship').mockResolvedValue({
    ...responseDefault,
    data: { status, data: [] }
  });

const runMockFailUpdate = (status: string) =>
  jest.spyOn(hardshipService, 'updateHardship').mockRejectedValue({
    ...responseDefault,
    data: { status, data: [] }
  });

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();

  spy2?.mockReset();
  spy2?.mockRestore();

  spy3?.mockReset();
  spy3?.mockRestore();

  spy4?.mockReset();
  spy4?.mockRestore();
});

let state: RootState;

const postData = {
  common: { accountId: storeId },
  data: {}
};

const postDataTriggerCreate = {
  type: TRIGGER_TYPE.CREATE,
  accountId: storeId,
  typeForm: HARDSHIP_TYPE_ID.naturalDisaster
};
const postDataTriggerUpdate = {
  type: TRIGGER_TYPE.UPDATE,
  accountId: storeId,
  typeForm: HARDSHIP_TYPE_ID.hardship
};

const toastSpy = mockActionCreator(actionsToast);

describe('updateHardshipRequest', () => {
  it('async thunk success', async () => {
    spy = jest.spyOn(hardshipService, 'updateHardship').mockResolvedValue({
      ...responseDefault,
      data: { status: STATUS_HARDSHIP.SUCCESS, data: [] }
    });

    const store = createStore(rootReducer, {});
    const response = await updateHardshipRequest({ storeId, postData })(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.payload).toEqual({
      status: STATUS_HARDSHIP.SUCCESS,
      data: []
    });
  });

  it('reject with status hardship fail', async () => {
    spy = jest.spyOn(hardshipService, 'updateHardship').mockResolvedValue({
      ...responseDefault,
      data: { status: STATUS_HARDSHIP.FAIL }
    });

    const store = createStore(rootReducer, {});
    const response = await updateHardshipRequest({ storeId, postData })(
      store.dispatch,
      store.getState,
      {}
    );
    expect(isRejected(response)).toBeTruthy;
  });

  it('reject', async () => {
    spy = jest.spyOn(hardshipService, 'updateHardship').mockRejectedValue({});

    const store = createStore(rootReducer, {});
    const response = await updateHardshipRequest({ storeId, postData })(
      store.dispatch,
      store.getState,
      {}
    );
    expect(isRejected(response)).toBeTruthy;
  });
});

describe('triggerUpdateHardship', () => {
  describe('async thunk', () => {
    afterEach(() => {
      window.appConfig = {} as AppConfiguration;
    });

    describe('isFulfilled', () => {
      window.appConfig = {
        ...window.appConfig,
        commonConfig: {
          isAdminRole: true
        } as CommonConfig
      };
      it('type Update with success status', async () => {
        const initialState: Partial<RootState> = {};
        const mockAddToast = toastSpy('addToast');
        spy = runMockSuccessUpdate(STATUS_HARDSHIP.SUCCESS);
        const store = createStoreWithDefaultMiddleWare(initialState);
        await triggerUpdateHardship({
          storeId,
          postData: {
            ...postDataTriggerUpdate,
            typeForm: HARDSHIP_TYPE_ID.militaryDeployment
          }
        })(store.dispatch, store.getState, {});

        expect(mockAddToast).toBeCalledWith({
          show: true,
          type: 'success',
          message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATED
        });
      });

      describe('type Create', () => {
        it('typeform naturalDisaster with success status', async () => {
          const initialState: Partial<RootState> = {
            form: {
              ['form1']: {
                values: {
                  ['toPaymentDate']: '2021-08-10',
                  ['reagePaymentPeriod']: {
                    value: 'reagePaymentPeriod'
                  }
                }
              }
            }
          };
          const mockAddToast = toastSpy('addToast');

          const store = createStore(rootReducer, initialState);
          await triggerUpdateHardship({
            storeId,
            postData: {
              ...postDataTriggerCreate,
              typeForm: HARDSHIP_TYPE_ID.naturalDisaster,
              viewName: ['form1']
            }
          })(
            byPassFulfilled(triggerUpdateHardship.fulfilled.type, {
              status: STATUS_HARDSHIP.SUCCESS
            }),
            store.getState,
            {}
          );

          expect(mockAddToast).toBeCalledWith({
            show: true,
            type: 'success',
            message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
          });
        });

        it('typeform longTerm with success status', async () => {
          const mockAddToast = toastSpy('addToast');
          spy = runMockSuccessUpdate(STATUS_HARDSHIP.SUCCESS);
          const store = createStore(rootReducer, {});
          await triggerUpdateHardship({
            storeId,
            postData: {
              ...postDataTriggerCreate,
              typeForm: HARDSHIP_TYPE_ID.longTerm
            }
          })(
            byPassFulfilled(triggerUpdateHardship.fulfilled.type, {
              status: STATUS_HARDSHIP.SUCCESS
            }),
            store.getState,
            {}
          );

          expect(mockAddToast).toBeCalledWith({
            show: true,
            type: 'success',
            message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
          });
        });

        it('typeform militaryDeployment', async () => {
          const mockAddToast = toastSpy('addToast');
          const store = createStore(rootReducer, {});
          await triggerUpdateHardship({
            storeId,
            postData: {
              ...postDataTriggerCreate,
              typeForm: HARDSHIP_TYPE_ID.militaryDeployment
            }
          })(
            byPassFulfilled(triggerUpdateHardship.fulfilled.type, {
              status: STATUS_HARDSHIP.SUCCESS
            }),
            store.getState,
            {}
          );
          expect(mockAddToast).toBeCalledWith({
            show: true,
            type: 'success',
            message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
          });
        });

        it('typeform hardship', async () => {
          const mockAddToast = toastSpy('addToast');

          const store = createStore(rootReducer, {});
          await triggerUpdateHardship({
            storeId,
            postData: {
              ...postDataTriggerCreate,
              typeForm: HARDSHIP_TYPE_ID.hardship,
              dataDetails: {
                workoutLastDate: '00000000'
              }
            }
          })(
            byPassFulfilled(triggerUpdateHardship.fulfilled.type, {
              status: STATUS_HARDSHIP.SUCCESS
            }),
            store.getState,
            {}
          );

          expect(mockAddToast).toBeCalledWith({
            show: true,
            type: 'success',
            message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
          });
        });
      });
    });

    it('isRejected', async () => {
      const mockAddToast = toastSpy('addToast');
      spy = runMockFailUpdate(STATUS_HARDSHIP.SUCCESS);
      const store = createStoreWithDefaultMiddleWare({});
      await triggerUpdateHardship({
        storeId,
        postData: postDataTriggerUpdate
      })(store.dispatch, store.getState, {});

      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'error',
        message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATE_FAILED
      });
    });

    it('isRejected', async () => {
      const mockAddToast = toastSpy('addToast');
      spy = runMockFailUpdate(STATUS_HARDSHIP.SUCCESS);
      const store = createStoreWithDefaultMiddleWare({});
      await triggerUpdateHardship({
        storeId,
        postData: postDataTriggerCreate
      })(store.dispatch, store.getState, {});

      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'error',
        message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMIT_FAILED
      });
    });

    describe('should be triggerUpdateHardshipBuilder', () => {
      it('should response triggerUpdateHardship pending', () => {
        const pendingAction = triggerUpdateHardship.pending(
          triggerUpdateHardship.pending.type,
          { storeId, postData: postDataTriggerCreate }
        );
        const actual = rootReducer(state, pendingAction);

        expect(actual.hardship[storeId]?.isLoading).toEqual(true);
      });

      it('should response triggerUpdateHardship fulfilled', () => {
        const fulfilled = triggerUpdateHardship.fulfilled(
          null,
          triggerUpdateHardship.pending.type,
          { storeId, postData: postDataTriggerCreate }
        );
        const actual = rootReducer(state, fulfilled);

        expect(actual.hardship[storeId]?.isLoading).toEqual(false);
        expect(actual.hardship[storeId]?.isSubmit).toEqual(false);
      });

      it('should response triggerUpdateHardship rejected', () => {
        const rejected = triggerUpdateHardship.rejected(
          null,
          triggerUpdateHardship.rejected.type,
          { storeId, postData: postDataTriggerCreate }
        );
        const actual = rootReducer(state, rejected);

        expect(actual.hardship[storeId]?.isLoading).toEqual(false);
        expect(actual.hardship[storeId]?.isSubmit).toEqual(false);
      });

      it('should response triggerUpdateHardship rejected', () => {
        const rejected = triggerUpdateHardship.rejected(
          null,
          triggerUpdateHardship.rejected.type,
          { storeId, postData: postDataTriggerCreate }
        );
        const actual = rootReducer(state, rejected);

        expect(actual.hardship[storeId]?.isLoading).toEqual(false);
        expect(actual.hardship[storeId]?.isSubmit).toEqual(false);
      });
    });
  });
});
