import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { EMPTY_STRING } from 'app/constants';
import { mappingDataFromObj } from 'app/helpers';
import { isEmpty, isEqual } from 'lodash';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { dispatchDestroyDelayProgress } from 'pages/CollectionForm/helpers';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import {
  HARDSHIP_COMPLETE,
  HARDSHIP_IN_PROGRESS,
  HARDSHIP_TYPE_DESC,
  VIEW_TYPE
} from '../constants';
import {
  mapDataToLongTermHardshipViewResult,
  mapDataToMilitaryHardshipViewResult
} from '../HardShipResults/helpers';

// Service
import { hardshipService } from '../hardshipService';

// Type
import { GetDetailHardshipArgs, HardshipState } from '../types';

export const getHardshipDebtManagementDetails = createAsyncThunk<
  MagicKeyValue,
  GetDetailHardshipArgs,
  ThunkAPIConfig
>('hardshipDebtManagementDetails', async (args, thunkAPI) => {
  const { storeId } = args.postData;
  const { commonConfig } = window.appConfig || {};
  const { getState, dispatch } = thunkAPI;
  const { mapping, collectionForm, accountDetail } = getState();
  const {
    inProgressInfo = {},
    isInProgress,
    lastDelayCallResult
  } = collectionForm[storeId] || {};

  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;

  try {
    const requestBody = {
      common: {
        org: commonConfig?.org || EMPTY_STRING,
        app: commonConfig?.app || EMPTY_STRING,
        accountId: eValueAccountId
      },
      callingApplication: commonConfig?.callingApplicationAPP1 || EMPTY_STRING,
      operatorCode: commonConfig?.operatorID || EMPTY_STRING
    };
    const { data } = await hardshipService.getHardshipDebtManagementDetails(
      requestBody
    );

    let viewData: MagicKeyValue = {};

    if (data?.hardshipType) {
      switch (data?.hardshipType) {
        case VIEW_TYPE.LONG_TERM:
          const mappingViewLT = mapping?.data?.viewHardshipLongTerm || {};
          viewData = mapDataToLongTermHardshipViewResult(data, mappingViewLT);
          viewData.typeHardship = HARDSHIP_TYPE_DESC[data?.hardshipType];
          break;
        case VIEW_TYPE.MILITARY_DEPLOYMENT:
          const mappingViewMD = mapping?.data?.viewHardshipMilitary || {};
          viewData = mapDataToMilitaryHardshipViewResult(data, mappingViewMD);
          viewData.typeHardship = HARDSHIP_TYPE_DESC[data?.hardshipType];
          break;
        case VIEW_TYPE.ND:
        case VIEW_TYPE.BASIC:
        default:
          const mappingModelBH = mapping?.data?.viewHardshipBasic || {};
          const formatData = mappingDataFromObj<MagicKeyValue>(
            data,
            mappingModelBH
          );
          viewData = {
            ...formatData,
            reagePaymentPeriod: {
              ...formatData?.reagePaymentPeriod,
              description: `${data?.reAgePaymentPeriod} Months`
            },
            typeHardship: HARDSHIP_TYPE_DESC[data?.hardshipType]
          };
      }
      viewData = { ...viewData, typeCode: data?.hardshipType };
    }

    // delay messages
    const processStatus = data?.processStatus;
    const isSetup = lastDelayCallResult === CALL_RESULT_CODE.HARDSHIP;
    const isSetupDone =
      processStatus === HARDSHIP_COMPLETE ||
      processStatus === HARDSHIP_IN_PROGRESS;
    const isDelay = isSetup
      ? isSetupDone
      : isEqual({ processStatus }, inProgressInfo);

    if (isDelay && !isEmpty(inProgressInfo) && isInProgress) {
      dispatch(
        collectionFormActions.toggleCallResultInProgressStatus({
          storeId
        })
      );
      dispatchDestroyDelayProgress(storeId);
    }

    return {
      data: viewData,
      status: processStatus
    };
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const getHardshipDebtManagementDetailsBuilder = (
  builder: ActionReducerMapBuilder<HardshipState>
) => {
  builder
    .addCase(getHardshipDebtManagementDetails.pending, (draftState, action) => {
      const { postData } = action.meta.arg;
      const { storeId } = postData;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingView: true
      };
    })
    .addCase(
      getHardshipDebtManagementDetails.fulfilled,
      (draftState, action) => {
        const { postData } = action.meta.arg;
        const { storeId } = postData;
        const { data, status } = action.payload;
        draftState[storeId] = {
          ...draftState[storeId],
          isLoadingView: false,
          data,
          status
        };
      }
    )
    .addCase(
      getHardshipDebtManagementDetails.rejected,
      (draftState, action) => {
        const { postData } = action.meta.arg;
        const { storeId } = postData;
        draftState[storeId] = {
          ...draftState[storeId],
          isLoadingView: false
        };
      }
    );
};
