import { hardshipActions } from './reducers';
import {
  storeId,
  mockActionCreator,
  responseDefault,
  createStoreWithDefaultMiddleWare
} from 'app/test-utils';

import { hardshipService } from '../hardshipService';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { STATUS_HARDSHIP } from '../constants';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

const toastSpy = mockActionCreator(actionsToast);
const mockCollectionFormActions = mockActionCreator(collectionFormActions);
const apiErrorNotificationActionMock = mockActionCreator(
  apiErrorNotificationAction
);
const mockHardshipActions = mockActionCreator(hardshipActions);

describe('triggerRemoveHardshipStatusRequest', () => {
  it('with success', async () => {
    const addToast = toastSpy('addToast');
    const toggleCallResultInProgressStatus = mockCollectionFormActions(
      'toggleCallResultInProgressStatus'
    );
    const clearApiErrorData =
      apiErrorNotificationActionMock('clearApiErrorData');
    const getHardshipDebtManagementDetails = mockHardshipActions(
      'getHardshipDebtManagementDetails'
    );
    spy = jest.spyOn(hardshipService, 'removeHardship').mockResolvedValue({
      ...responseDefault,
      data: {
        status: STATUS_HARDSHIP.SUCCESS
      }
    });

    const store = createStoreWithDefaultMiddleWare({});

    await hardshipActions.triggerRemoveHardshipStatusRequest({
      postData: {
        storeId,
        accountId: storeId
      }
    })(store.dispatch, store.getState, {});

    expect(addToast).toHaveBeenCalled();
    expect(toggleCallResultInProgressStatus).toHaveBeenCalled();
    expect(clearApiErrorData).toHaveBeenCalled();
    expect(getHardshipDebtManagementDetails).toHaveBeenCalled();
  });

  it('with failure with fail status', async () => {
    const addToast = toastSpy('addToast');

    const updateApiError = apiErrorNotificationActionMock('updateApiError');

    spy = jest.spyOn(hardshipService, 'removeHardship').mockResolvedValue({
      ...responseDefault,
      data: {
        status: STATUS_HARDSHIP.FAIL
      }
    });

    const store = createStoreWithDefaultMiddleWare({});

    await hardshipActions.triggerRemoveHardshipStatusRequest({
      postData: {
        storeId,
        accountId: storeId
      }
    })(store.dispatch, store.getState, {});
    expect(addToast).toHaveBeenCalled();
    expect(updateApiError).toHaveBeenCalled();
  });
});
