import { createSelector } from '@reduxjs/toolkit';
import { HARDSHIP_IN_PROGRESS, HARDSHIP_TYPE, REASON_REMOVE_HARDSHIP } from '../constants';

export const selectHardshipInfo = createSelector(
  (states: RootState, storeId: string) => {
    const data = states.hardship[storeId]?.data;
    return data;
  },
  data => data
);

export const selectStatus = createSelector(
  (states: RootState, storeId: string) => {
    const status = states.hardship[storeId]?.status;
    return status;
  },
  status => status
);

export const selectHardshipDataForm = createSelector(
  (states: RootState, storeId: string) => {
    const commonData = states.hardship[storeId]?.commonData;
    return commonData;
  },
  commonData => commonData
);

export const selectHardshipDetailsData = createSelector(
  (states: RootState, storeId: string) => {
    const dataDetails = states.hardship[storeId]?.dataDetails;
    return dataDetails;
  },
  dataDetails => dataDetails
);

export const selectReasonRemove = (storeId: string) => {
  return createSelector(
    (states: RootState) =>
      states.hardship[storeId]?.reasonHardship || REASON_REMOVE_HARDSHIP[0],
    data => data
  );
};

export const selectTypeFormHardship = createSelector(
  (states: RootState, storeId: string) => {
    return states.hardship[storeId]?.typeForm || HARDSHIP_TYPE[0];
  },
  type => type
);

export const selectIsValidLongTermHardship = createSelector(
  (states: RootState, storeId: string) =>
    states.hardship[storeId]?.longTerm?.isValid,
  (isValid?: boolean) => isValid
);

export const selectIsDirtyLongTermHardship = createSelector(
  (states: RootState, storeId: string) =>
    states.hardship[storeId]?.longTerm?.isDirty,
  (isDirty?: boolean) => isDirty
);

export const selectIsSubmit = createSelector(
  (states: RootState, storeId: string) => states.hardship[storeId]?.isSubmit,
  (isSubmit?: boolean) => isSubmit
);

export const selectIsLoading = createSelector(
  (states: RootState, storeId: string) => states.hardship[storeId]?.isLoading,
  (isLoading?: boolean) => isLoading
);

export const selectIsLoadingView = createSelector(
  (states: RootState, storeId: string) =>
    states.hardship[storeId]?.isLoadingView || false,
  (isLoading?: boolean) => isLoading
);

export const selectIsOpenConfirmModal = createSelector(
  (states: RootState, storeId: string) =>
    states.hardship[storeId]?.showConfirmModal || false,
  (data: boolean) => data
);

export const selectIsDisableRemove = createSelector(
  (states: RootState, storeId: string) =>
    states.hardship[storeId]?.data?.processStatus ,
  (processStatus: string) => processStatus === HARDSHIP_IN_PROGRESS
);
