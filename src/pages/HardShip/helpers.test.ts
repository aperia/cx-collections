import { isLastDayMonth, lengthMonth, setStatusNFPAInit } from './helpers';

const leapYear = 2000;
const normalYear = 2001;
const febMonth = 2;
const marchMonth = 3;
describe('test is last day of Month', () => {
  it('when month is Feb for leap year', () => {
    const result = isLastDayMonth(28, febMonth, leapYear);
    expect(result).toBeFalsy;

    const result2 = isLastDayMonth(29, febMonth, leapYear);
    expect(result2).toBeTruthy;
  });

  it('when month is Feb for normal year year', () => {
    const result = isLastDayMonth(28, febMonth, normalYear);
    expect(result).toBeTruthy;

    const result2 = isLastDayMonth(22, febMonth, normalYear);
    expect(result2).toBeFalsy;
  });

  it('when month not Feb for normal year year and leap year', () => {
    const result = isLastDayMonth(30, marchMonth, normalYear);
    expect(result).toBeTruthy;

    const result2 = isLastDayMonth(22, marchMonth, normalYear);
    expect(result2).toBeFalsy;
  });
});

describe('test for lengthMonth function', () => {
  const defaultData = {
    fromPaymentDate: '2021-07-07T17:00:00.000Z',
    toPaymentDate: '2021-10-30T17:00:00.000Z'
  };
  const defaultFields = ['fromPaymentDate', 'toPaymentDate'];
  const longTermHardshipType = 'longTermHardship';
  it('empty case data', () => {
    const result = lengthMonth({}, []);
    expect(result).toEqual(0);
  });

  it('undefined field case data', () => {
    const field = ['fieldDoesNotExist ', 'toPaymentDate'];
    const result = lengthMonth(defaultData, field);
    expect(result).toEqual(0);
  });

  it('longTermHardship case data', () => {
    const result = lengthMonth(
      defaultData,
      defaultFields,
      longTermHardshipType
    );
    expect(result).toEqual(-1);
  });

  it('longTermHardship with from date < to date ', () => {
    const result = lengthMonth(
      {
        fromPaymentDate: '2021-07-25T17:00:00.000Z',
        toPaymentDate: '2021-10-10T17:00:00.000Z'
      },
      defaultFields,
      longTermHardshipType
    );

    expect(result).toEqual(-1);
  });

  it('month and day equally', () => {
    const result = lengthMonth(
      {
        fromPaymentDate: '2021-07-07T17:00:00.000Z',
        toPaymentDate: '2022-07-07T17:00:00.000Z'
      },
      defaultFields
    );
    expect(result).toEqual(12);
  });

  it('not same day and months, from Month > toMonth and both are lastDay of Month', () => {
    const result = lengthMonth(
      {
        fromPaymentDate: '2021-08-31T00:00:00.000Z',
        toPaymentDate: '2022-07-31T00:00:00.000Z'
      },
      defaultFields
    );
    expect(result).toEqual(8);
  });

  it('not same day and months, from Month > toMonth and both are not lastDay of Month', () => {
    const result = lengthMonth(
      {
        fromPaymentDate: '2021-08-26T00:00:00.000Z',
        toPaymentDate: '2022-07-23T00:00:00.000Z'
      },
      defaultFields
    );
    expect(result).toEqual(7);
  });

  it('not same day and months, from Month > toMonth and fromDate < toDate', () => {
    const result = lengthMonth(
      {
        fromPaymentDate: '2021-08-15T00:00:00.000Z',
        toPaymentDate: '2022-07-23T00:00:00.000Z'
      },
      defaultFields
    );
    expect(result).toEqual(8);
  });

  it('not same day and months, from Month = toMonth', () => {
    const result = lengthMonth(
      {
        fromPaymentDate: '2021-08-15T00:00:00.000Z',
        toPaymentDate: '2022-08-14T00:00:00.000Z'
      },
      defaultFields
    );
    expect(result).toEqual(8);
  });

  it('not same day and months, from Month > toMonth and toDaY is Last Day', () => {
    const result = lengthMonth(
      {
        fromPaymentDate: '2021-08-31T10:00:00.000Z',
        toPaymentDate: '2021-07-31T10:00:00.000Z'
      },
      defaultFields
    );
    expect(result).toEqual(8);
  });

  it('not same day and months, from Month < toMonth and both are lastDay of Month', () => {
    const result = lengthMonth(
      {
        fromPaymentDate: '2021-07-31T00:00:00.000Z',
        toPaymentDate: '2022-08-31T00:00:00.000Z'
      },
      defaultFields
    );
    expect(result).toEqual(-2);
  });

  it('not same day and months, from Month < toMonth and fromDate > toDate', () => {
    const result = lengthMonth(
      {
        fromPaymentDate: '2021-07-31T00:00:00.000Z',
        toPaymentDate: '2022-08-23T00:00:00.000Z'
      },
      defaultFields
    );
    expect(result).toEqual(-3);
  });
  it('not same day and months, from Month < toMonth and fromDate < toDate', () => {
    const result = lengthMonth(
      {
        fromPaymentDate: '2021-07-20T00:00:00.000Z',
        toPaymentDate: '2022-08-25T00:00:00.000Z'
      },
      defaultFields
    );
    expect(result).toEqual(-2);
  });

  it('not same day and months, from Month < toMonth and toYear === fromYear ', () => {
    const result = lengthMonth(
      {
        fromPaymentDate: '2021-03-31T10:00:00.000Z',
        toPaymentDate: '2021-08-31T10:00:00.000Z'
      },
      defaultFields
    );
    expect(result).toEqual(2);
  });
});

describe('setStatusNFPAInit', () => {
  const callBackFn = jest.fn();
  const field = {
    props: {
      props: {
        setOthers: (previous: any) => {
          previous();
          callBackFn();
        }
      }
    }
  } as any;
  const data = {
    currentBalance: 1110,
    cycleToDatePaymentsAmount: 0,
    nonDelinquentMpdAmount: 100,
    minimumNewFixedPaymentAmount: 1110,
    newFixedPaymentAmount: 111
  };
  it('should combine mockApi from config with url ', () => {
    setStatusNFPAInit(field, data);
    expect(callBackFn).toHaveBeenCalled();
  });
});
