import { createSelector } from '@reduxjs/toolkit';
import { GroupCheckboxCountryDeployment } from './types';

export const selectGroupCheckboxCountryDeployment = createSelector(
  (states: RootState, storeId: string) =>
    states.hardship[storeId]?.militaryDeployment
      ?.groupCheckboxCountryDeployment,
  (monthlyOutgoItems?: GroupCheckboxCountryDeployment[]) => monthlyOutgoItems
);

export const selectMilitaryDeploymentLoading = createSelector(
  (states: RootState, storeId: string) =>
    states.hardship[storeId]?.militaryDeployment?.loading,
  (incomeFrequencies?: boolean) => incomeFrequencies
);
