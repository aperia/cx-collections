import { mapDataToPost, mapObjectData } from './helper';

const mapping = {
  ctdPaymentAmount: 'cycleToDatePaymentsAmount',
  newFixedPaymentAmount: 'newFixedPaymentAmount',
  nonDelinquentMinimumPaymentDueAmount: 'nonDelinquentMpdAmount',
  reAgePaymentPeriod: 'reagePaymentPeriod.value',
  startDate: 'fromPaymentDate',
  endDate: 'toPaymentDate',
  'militaryDeploymentHardship.deploymentStartDate': 'deploymentStartDate',
  'militaryDeploymentHardship.deploymentEndDate': 'deploymentEndDate',
  'militaryDeploymentHardship.groupCountryOfDeployment':
    'groupCountryOfDeployment',
  'militaryDeploymentHardship.deploymentCountry':
    'countryOfDeployment.fieldValue'
};

describe('helper', () => {
  describe('mapDataToPost', () => {
    it('with data', () => {
      const data = {
        cycleToDatePaymentsAmount: '1',
        newFixedPaymentAmount: '1',
        nonDelinquentMpdAmount: '1',
        reagePaymentPeriod: {
          value: 'reagePaymentPeriod'
        },
        fromPaymentDate: new Date('11/11/2021'),
        toPaymentDate: new Date('11/11/2021'),

        deploymentStartDate: new Date('11/11/2021'),
        deploymentEndDate: new Date('11/11/2021'),
        groupCountryOfDeployment: 'disclosed',
        countryOfDeployment: {
          fieldValue: 'USA'
        }
      };
      const result = mapDataToPost(data, mapping);

      expect(result).toEqual({
        ctdPaymentAmount: '1',
        endDate: '11-11-2021',
        hardshipType: 'MD',
        militaryDeploymentHardship: {
          deploymentCountry: 'USA',
          deploymentEndDate: '11-11-2021',
          deploymentStartDate: '11-11-2021'
        },
        newFixedPaymentAmount: '1',
        nonDelinquentMinimumPaymentDueAmount: '1',
        reAgePaymentPeriod: 'reagePaymentPeriod',
        startDate: '11-11-2021'
      });
    });

    it('with empty data', () => {
      const data = {
        cycleToDatePaymentsAmount: '1',
        newFixedPaymentAmount: '1',
        nonDelinquentMpdAmount: '1',
        reagePaymentPeriod: {
          value: 'reagePaymentPeriod'
        },
        fromPaymentDate: new Date('11/11/2021'),
        toPaymentDate: new Date('11/11/2021'),

        deploymentStartDate: new Date('11/11/2021'),
        deploymentEndDate: new Date('11/11/2021'),
        groupCountryOfDeployment: 'disclosed',
        countryOfDeployment: {
          fieldValue: 'USA'
        }
      };
      const result = mapDataToPost(data, mapping);

      expect(result).toEqual({
        ctdPaymentAmount: '1',
        endDate: '11-11-2021',
        hardshipType: 'MD',
        militaryDeploymentHardship: {
          deploymentCountry: 'USA',
          deploymentEndDate: '11-11-2021',
          deploymentStartDate: '11-11-2021'
        },
        newFixedPaymentAmount: '1',
        nonDelinquentMinimumPaymentDueAmount: '1',
        reAgePaymentPeriod: 'reagePaymentPeriod',
        startDate: '11-11-2021'
      });
    });
  });

  describe('mapObjectData', () => {
    it('with data', () => {
      const data = {
        deploymentStartDate: new Date('11/11/2021'),
        deploymentEndDate: new Date('11/11/2021'),
        groupCountryOfDeployment: 'other',
        deploymentCountry: 'deploymentCountry'
      };
      const result = mapObjectData(data);

      expect(result).toEqual({
        deploymentStartDate: '11-11-2021',
        deploymentEndDate: '11-11-2021'
      });
    });
  });

  it('empty data', () => {
    const data = {};
    const result = mapObjectData(data);

    expect(result).toEqual({});
  });
});
