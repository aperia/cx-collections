import React from 'react';
import { renderMockStoreId, storeId } from 'app/test-utils';
import MilitaryDeploymentHardshipForm from './index';
import { HARDSHIP_TYPE } from '../constants';
import { screen } from '@testing-library/dom';
import { DISABLED_VALUE } from 'app/components/_dof_controls/custom-functions/hardshipGroupCountryOfDeploymentChange';
import { act } from '@testing-library/react';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('Test MilitaryDeploymentHardship component', () => {
  it('Render UI ', async () => {
    jest.useFakeTimers();
    await act(async () => {
      renderMockStoreId(<MilitaryDeploymentHardshipForm />, {
        initialState: {
          hardship: {
            [storeId]: {
              data: {
                groupCountryOfDeployment: 'abcxyz',
                newInterestRate: 6
              },
              typeForm: HARDSHIP_TYPE[2],
              commonData: {
                cycleToDatePaymentsAmount: '3',
                newFixedPaymentAmount: '2',
                nonDelinquentMpdAmount: '1',
                minimumNewFixedPaymentAmount: '1',
                groupCountryOfDeployment: DISABLED_VALUE
              },
              militaryDeployment: {}
            }
          } as MagicKeyValue
        }
      });
    });
    jest.runAllImmediates();
    expect(
      screen.getByTestId('militaryDeploymentHardshipFormViews')
    ).toBeInTheDocument();
  });

  it('Render with empty value UI ', () => {
    jest.useFakeTimers();
    renderMockStoreId(<MilitaryDeploymentHardshipForm />, {
      initialState: {
        hardship: {
          [storeId]: {
            typeForm: HARDSHIP_TYPE[2],
            commonData: {},
            militaryDeployment: {}
          }
        } as MagicKeyValue
      }
    });
    jest.runAllImmediates();
    expect(
      screen.getByTestId('militaryDeploymentHardshipFormViews')
    ).toBeInTheDocument();
  });

  it('Render UI ', async () => {
    jest.useFakeTimers();
    await act(async () => {
      renderMockStoreId(<MilitaryDeploymentHardshipForm />, {
        initialState: {
          hardship: {
            [storeId]: {
              data: { groupCountryOfDeployment: 'abcxyz' },
              typeForm: HARDSHIP_TYPE[2],
              commonData: {
                cycleToDatePaymentsAmount: '3',
                newFixedPaymentAmount: '2',
                nonDelinquentMpdAmount: '1',
                minimumNewFixedPaymentAmount: '1'
              },
              militaryDeployment: {}
            }
          } as MagicKeyValue
        }
      });
    });
    jest.runAllImmediates();
    expect(
      screen.getByTestId('militaryDeploymentHardshipFormViews')
    ).toBeInTheDocument();
  });
});
