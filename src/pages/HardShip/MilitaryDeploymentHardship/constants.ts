export const VIEWS = 'militaryDeploymentHardshipFormViews';

export const SUPPORTED_FILE_TYPES = [
  'doc',
  'docx',
  'pdf',
  'jpg',
  'png',
  'tiff'
];

export const MAX_SIZE_FILE = 5 * 1024 * 1024;
