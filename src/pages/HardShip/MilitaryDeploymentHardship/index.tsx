import React, { Fragment, useEffect, useRef, useState } from 'react';

// components
import { ExtraFieldProps, View } from 'app/_libraries/_dof/core';
// redux
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { selectHardshipDataForm } from '../_redux/selectors';

// helper
import isEmpty from 'lodash.isempty';
import { Field } from 'redux-form';
import { DISABLED_VALUE } from 'app/components/_dof_controls/custom-functions/hardshipGroupCountryOfDeploymentChange';
import { setStatusNFPAInit } from '../helpers';
import { VIEWS } from './constants';
import { NFPA } from '../constants';
import { useAddHardShipValidation } from '../hooks/useAddHardshipValidation';

export interface Props {}

const MilitaryDeploymentHardshipForm: React.FC<Props> = () => {
  const { storeId } = useAccountDetail();
  const ref = useRef<any>(null);

  const data = useStoreIdSelector<MagicKeyValue>(selectHardshipDataForm);

  const [formValues, setFormValues] = useState(data);
  const formName = `${storeId}-${VIEWS}`;

  useAddHardShipValidation(data, ref);

  // set error for NFPA after load data
  useEffect(() => {
    if (isEmpty(data) || !ref.current) return;
    const immediateId = setImmediate(() => {
      // init value New Interest Rate
      setFormValues({ ...data, newInterestRate: data.newInterestRate ?? 6 });

      // enable or disabled when groupCountryOfDeployment check 'confidential'
      const { groupCountryOfDeployment } = data;
      if (groupCountryOfDeployment !== DISABLED_VALUE) return;
      const countryOfDeploymentRef = ref.current?.props?.onFind(
        'countryOfDeployment'
      );
      countryOfDeploymentRef?.props?.props?.setEnable(false);

      // set status error for NFPA
      const field: Field<ExtraFieldProps> = ref.current?.props?.onFind(NFPA);
      setStatusNFPAInit(field, data);
    });

    return () => {
      clearImmediate(immediateId);
    };
  }, [data]);

  return (
    <Fragment>
      <View
        ref={ref}
        id={formName}
        formKey={formName}
        descriptor={VIEWS}
        value={formValues}
      />
    </Fragment>
  );
};

export default MilitaryDeploymentHardshipForm;
