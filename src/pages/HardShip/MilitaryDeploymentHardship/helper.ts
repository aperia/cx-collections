import dateTime from 'date-and-time';
import isEmpty from 'lodash.isempty';
import { mappingDataFromObj } from 'app/helpers';
import { FormatTime } from 'app/constants/enums';

export const mapObjectData = (value: MagicKeyValue) => {
  const {
    deploymentStartDate,
    deploymentEndDate,
    groupCountryOfDeployment,
    deploymentCountry
  } = value;

  if (isEmpty(value)) return {};
  const result = {
    deploymentStartDate: dateTime.format(
      deploymentStartDate,
      FormatTime.DateWithHyphen
    ),
    deploymentEndDate: dateTime.format(
      deploymentEndDate,
      FormatTime.DateWithHyphen
    )
  };

  /**
   * IF Collector selects Disclosed radio button,
   * THEN map Country of Deployment combo box with deploymentCountry
   */
  // default value groupCountryOfDeployment = disclosed
  const hasCountry =
    groupCountryOfDeployment === undefined ||
    groupCountryOfDeployment === 'disclosed';

  return hasCountry ? { deploymentCountry, ...result } : result;
};

export const mapDataToPost = (
  rawData: MagicKeyValue,
  mapping: MagicKeyValue
) => {
  const data = mappingDataFromObj(rawData, mapping);
  const dataPost: MagicKeyValue = Object.keys(data).reduce((prev, key) => {
    const value = data[key];
    let newData = value;

    switch (key) {
      case 'startDate':
      case 'endDate':
        newData = dateTime.format(value, FormatTime.DateWithHyphen);
        break;
      case 'ctdPaymentAmount':
      case 'incomeAmount':
      case 'newFixedPaymentAmount':
      case 'nonDelinquentMinimumPaymentDueAmount':
        newData = value;
        break;
      case 'militaryDeploymentHardship':
        return { ...prev, [key]: mapObjectData(value) };
      default:
        break;
    }
    return {
      ...prev,
      [key]: newData
    };
  }, {});

  return {
    ...dataPost,
    hardshipType: 'MD'
  };
};
