import * as selector from './selectors';

// utils
import { HARDSHIP_TYPE, REASON_REMOVE_HARDSHIP } from '../constants';
import { selectorWrapper, storeId } from 'app/test-utils';

const states: Partial<RootState> = {
  hardship: {
    [storeId]: {
      data: [{ ky: 'value' }],
      commonData: {},
      reasonHardship: REASON_REMOVE_HARDSHIP[0],
      typeForm: HARDSHIP_TYPE[2],
      militaryDeployment: {
        groupCheckboxCountryDeployment: [],
        loading: false
      }
    }
  }
};

const selectorTrigger = selectorWrapper(states);

describe('MilitaryDeploymentHardship > selector', () => {
  it('selectGroupCheckboxCountryDeployment', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectGroupCheckboxCountryDeployment
    );

    expect(data).toEqual(
      states.hardship![storeId].militaryDeployment
        ?.groupCheckboxCountryDeployment
    );
    expect(emptyData).toBeUndefined();
  });
  it('selectMilitaryDeploymentLoading ', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectMilitaryDeploymentLoading
    );

    expect(data).toEqual(states.hardship![storeId].militaryDeployment?.loading);
    expect(emptyData).toBeUndefined();
  });
});
