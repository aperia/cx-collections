export interface GroupCheckboxCountryDeployment {
  value: string;
  label: string;
}

export interface MilitaryDeploymentHardship {
  callResultType?: string;
  commonData?: any;
  reasonHardship?: any;
  groupCheckboxCountryDeployment?: GroupCheckboxCountryDeployment[];
  loading?: boolean;
}
