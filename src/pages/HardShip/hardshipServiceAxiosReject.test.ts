// mocks
import { mockAxiosReject } from 'app/test-utils/mocks/mockAxiosReject';

// services
import { hardshipService } from './hardshipService';

describe('hardshipService success', () => {
  it('getGroupCheckboxCountryDeployment', () => {
    hardshipService.getGroupCheckboxCountryDeployment();

    expect(mockAxiosReject).toBeCalledWith({
      url: 'refData/checkboxCountryDeployment.json',
      method: 'GET'
    });
  });

  it('getMonthlyOutgoInformation', () => {
    hardshipService.getMonthlyOutgoInformation();

    expect(mockAxiosReject).toBeCalledWith({
      url: 'refData/monthlyOutgoInformation.json',
      method: 'GET'
    });
  });

  it('getIncomeFrequency', () => {
    hardshipService.getIncomeFrequency();

    expect(mockAxiosReject).toBeCalledWith({
      url: 'refData/incomeFrequency.json',
      method: 'GET'
    });
  });
});
