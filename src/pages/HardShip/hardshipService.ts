import axios from 'axios';
import { apiService } from 'app/utils/api.service';

export const hardshipService = {
  async getGroupCheckboxCountryDeployment() {
    try {
      const { data } = await axios({
        url: 'refData/checkboxCountryDeployment.json',
        method: 'GET'
      });

      return data;
    } catch (error) {
      console.log(error);
    }
  },
  async getMonthlyOutgoInformation() {
    try {
      const { data } = await axios({
        url: 'refData/monthlyOutgoInformation.json',
        method: 'GET'
      });

      return data;
    } catch (error) {
      console.log(error);
    }
  },
  getIncomeFrequency: async () => {
    try {
      const { data } = await axios({
        url: 'refData/incomeFrequency.json',
        method: 'GET'
      });

      return data;
    } catch (error) {
      console.log(error);
    }
  },
  uploadMilitaryFile(
    accountId: string,
    file: string | Blob,
    onUploadProgress: (event: ProgressEvent<EventTarget>) => void
  ) {
    const formData = new FormData();

    formData.append('file', file);
    formData.append('accountId', accountId);
    formData.append('uploadType', 'HARDSHIP');

    return apiService.post(
      window.appConfig?.api?.collectionForm?.uploadMilitaryFiles || '',
      formData,
      {
        headers: { 'Content-Type': 'application/json' },
        onUploadProgress
      }
    );
  },

  updateHardship(data: MagicKeyValue) {
    const url = window.appConfig?.api?.collectionForm?.setupHardship || '';
    return apiService.post(url, data);
  },
  removeHardship(data: MagicKeyValue) {
    const url = window.appConfig?.api?.collectionForm?.removeHardship || '';
    return apiService.post(url, data);
  },
  getDetailsHardship(data: MagicKeyValue) {
    const url = window.appConfig?.api?.collectionForm?.getDetailsHardship || '';
    return apiService.post(url, data);
  },
  getHardshipDebtManagementDetails(data: MagicKeyValue) {
    const url =
      window.appConfig?.api?.collectionForm?.getHardshipDebtManagementDetails ||
      '';
    return apiService.post(url, data);
  }
};
