import React from 'react';
import NaturalDisasterForm from './index';
import {
  renderMockStoreId,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';
import { screen } from '@testing-library/react';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

describe('NaturalDisasterForm', () => {
  it('should render with empty Data ', () => {
    renderMockStoreId(<NaturalDisasterForm />);
    expect(
      screen.queryByText('txt_natural_disaster_title')
    ).toBeInTheDocument();
  });

  it('should render with data ', () => {
    jest.useFakeTimers();
    renderMockStoreId(<NaturalDisasterForm />, {
      initialState: {
        hardship: {
          [storeId]: {
            commonData: {
              naturalDisasterType: 'naturalDisasterType'
            }
          } as any
        }
      }
    });
    jest.runAllImmediates();
    expect(
      screen.queryByText('{"naturalDisasterType":"naturalDisasterType"}')
    ).toBeInTheDocument();
  });
});
