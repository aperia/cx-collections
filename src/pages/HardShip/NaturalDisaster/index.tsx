import React, { useEffect, useRef } from 'react';

// components
import { ExtraFieldProps, View } from 'app/_libraries/_dof/core';

// helpers
import { Field } from 'redux-form';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { setStatusNFPAInit } from '../helpers';

// constants
import { NATURAL_DISASTER, NFPA } from '../constants';

// redux
import isEmpty from 'lodash.isempty';
import { selectHardshipDataForm } from '../_redux/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { useAddHardShipValidation } from '../hooks/useAddHardshipValidation';

export interface Props {}

const NaturalDisasterForm: React.FC<Props> = () => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const ref = useRef<any>(null);

  const formName = `${storeId}-${NATURAL_DISASTER}`;

  const data = useStoreIdSelector<MagicKeyValue>(selectHardshipDataForm);

  useAddHardShipValidation(data, ref);

  // set error for NFPA after load data
  useEffect(() => {
    if (isEmpty(data) || !ref.current) return;

    const immediateId = setImmediate(() => {
      const field: Field<ExtraFieldProps> = ref.current?.props?.onFind(NFPA);
      setStatusNFPAInit(field, data);
    });

    return () => {
      clearImmediate(immediateId);
    };
  }, [data]);

  return (
    <>
      <h6
        className="color-grey"
        data-testid={genAmtId('naturalDisasterForm', 'title', '')}
      >
        {t(I18N_COLLECTION_FORM.NATURAL_DISASTER_TITLE)}
      </h6>
      <View
        ref={ref}
        id={formName}
        formKey={formName}
        descriptor={NATURAL_DISASTER}
        value={data}
      />
    </>
  );
};

export default NaturalDisasterForm;
