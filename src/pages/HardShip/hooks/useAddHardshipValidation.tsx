import { formatCommon } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { ExtraFieldProps } from 'app/_libraries/_dof/core';
import { useEffect } from 'react';
import { Field } from 'redux-form';
import { I18N_HARDSHIP } from '../constants';
import { setFieldValidationRules } from '../helpers';

export const useAddHardShipValidation = (data: any, ref: any) => {
  const { t } = useTranslation();

  useEffect(() => {
    if (isEmpty(data) || !ref.current) return;

    const immediateId = setImmediate(() => {
      const newFixedPaymentAmountErrMsg = t(
        I18N_HARDSHIP.FIXED_PAYMENT_AMOUNT_ERR_MSG,
        {
          newFixedPaymentAmount: formatCommon(
            data?.newFixedPaymentAmount || '0'
          ).currency(2),
          currentBalance: formatCommon(data?.currentBalance || '0').currency(2)
        }
      );

      const nonDelinquentMpdAmountErrMsg = t(
        I18N_HARDSHIP.NON_DELINQUENT_MPD_AMOUNT_ERR_MSG,
        {
          nonDelinquentMpdAmount: formatCommon(
            data?.nonDelinquentMpdAmount || '0'
          ).currency(2)
        }
      );

      const newFixedPaymentAmountField: Field<ExtraFieldProps> =
        ref.current?.props?.onFind('newFixedPaymentAmount');

      const nonDelinquentMpdAmountField: Field<ExtraFieldProps> =
        ref.current?.props?.onFind('nonDelinquentMpdAmount');

      setFieldValidationRules(newFixedPaymentAmountField, [
        {
          ruleName: 'GreaterThanOrEqual',
          errorMsg: newFixedPaymentAmountErrMsg,
          ruleOptions: {
            otherValue: data?.newFixedPaymentAmount
          }
        },
        {
          ruleName: 'LessThanOrEqual',
          errorMsg: newFixedPaymentAmountErrMsg,
          ruleOptions: {
            otherValue: data?.currentBalance
          }
        }
      ]);

      setFieldValidationRules(nonDelinquentMpdAmountField, [
        {
          ruleName: 'LessThanOrEqual',
          errorMsg: nonDelinquentMpdAmountErrMsg,
          ruleOptions: {
            otherValue: data?.nonDelinquentMpdAmount
          }
        }
      ]);
    });

    return () => {
      clearImmediate(immediateId);
    };
  }, [data, ref, t]);

  return;
};
