import { ReactText } from 'react';
import { ResultHardship } from './HardShipResults/type';
import { LongTermHardship, ValuesProps } from './LongTermHardship/types';
import { MilitaryDeploymentHardship } from './MilitaryDeploymentHardship/types';

export interface CommonPayload {
  common: {
    accountId: string;
  };
}

export interface HardshipData {
  cycleToDatePaymentsAmount?: ReactText;
  nonDelinquentMpdAmount?: ReactText;
  newFixedPaymentAmount?: ReactText;
  hardshipProgramPeriod?: string;
  ragePaymentPeriod?: string;
  minimumNewFixedPaymentAmount?: ReactText;
}

export enum TRIGGER_TYPE {
  CREATE = 'CREATE',
  UPDATE = 'UPDATE'
}

export interface TriggerUpdateHardshipArg extends StoreIdPayload {
  postData: {
    viewName?: Array<string>;
    type?: TRIGGER_TYPE;
    typeForm: string;
    dataDetails?: MagicKeyValue;
  };
}

export interface UpdateHardshipRequestPayload {
  errorMessage: string;
  status: string;
  statusCode: string;
  statusMessage: string;
}

export interface UpdateHardshipRequestArg extends StoreIdPayload {
  postData: UpdatePromiseToPayPayload;
}

export interface UpdatePromiseToPayPayload extends CommonPayload {
  callResultType?: string;
  data: MagicKeyValue;
}
export interface RemoveHardshipPayload {
  common: { accountId: string };
  callResultType: string;
}

export interface SetReasonHardshipPayloadAction {
  storeId: string;
  value?: string;
  reason: string;
}

export interface UpdateDataFromLastFollowUpPayloadAction {
  storeId: string;
  data?: MagicKeyValue;
}

export interface UpdateDataFromLastFollowUpPayloadData extends MagicKeyValue {
  cycleToDatePaymentsAmount?: number | string;
  hardshipProgramPeriod?: { description?: string; value?: number | string };
  newFixedPaymentAmount?: string;
  nonDelinquentMpdAmount?: string;
  ragePaymentPeriod?: { description?: string; value?: number | string };
}

// enhance hardship
export interface UpdateHardshipPayload extends CommonPayload {
  callResultType?: string;
  data: MagicKeyValue;
}

export interface HardshipState {
  [storeId: string]: Hardship;
}

export interface Hardship {
  callResultType?: string;
  commonData?: CommonDataHardship;
  reasonHardship?: any;
  reason?: string;
  data: MagicKeyValue;
  dataDetails: MagicKeyValue;
  typeForm: {
    id: string;
    value: string;
  };
  isLoading: boolean;
  isSubmit?: boolean;
  isLoadingView?: boolean;
  longTerm?: LongTermHardship;
  militaryDeployment?: MilitaryDeploymentHardship;
  showConfirmModal?: boolean;
  resultHardship?: ResultHardship;
  status?: string;
}

export interface UpdateDataPayloadAction {
  storeId: string;
  data: MagicKeyValue;
}

export interface CommonDataHardship {
  cycleToDatePaymentsAmount?: string | number;
  newFixedPaymentAmount?: string | number;
  nonDelinquentMpdAmount?: string | number;
  minimumNewFixedPaymentAmount?: string | number;
}

export interface SetFormLongTermPayload extends StoreIdPayload {
  isValid?: boolean;
  values?: ValuesProps;
  isDirty?: boolean;
}

export interface RemoveHardshipFileArgs extends StoreIdPayload, MagicKeyValue {
  accountId: string;
}

export interface GetHardshipFileArgs extends StoreIdPayload, MagicKeyValue {
  accountId: string;
}
export interface GetHardshipFilePayload extends StoreIdPayload, MagicKeyValue {
  files: File[];
}

export interface GetDetailHardshipArgs {
  postData: {
    storeId: string;
    hardshipType?: string;
  };
}

export interface SetupHardshipArgs {
  storeId: string;
  postData: MagicKeyValue;
}

export interface HardshipFormData {
  ctdPaymentAmount: number;
  newFixedPaymentAmount: number;
  nonDelinquentMinimumPaymentDueAmount: number;
  reAgePaymentPeriod: string;
  startDate: string;
  endDate: string;
}

export interface RemoveStatusArgs {
  postData: {
    storeId: string;
  };
}

export interface ToggleConfirmModalPayloadAction {
  storeId: string;
  opened?: boolean;
}
