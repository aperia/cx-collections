import React, { useEffect, useRef } from 'react';

// components
import { ExtraFieldProps, View } from 'app/_libraries/_dof/core';
import BasicForm from './BasicForm';

// redux
import { useDispatch, batch } from 'react-redux';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { hardshipActions as actions } from '../_redux/reducers';
import { selectHardshipDataForm } from '../_redux/selectors';
import { selectLongTermHardshipLoading } from './selectors';

// helper
import isEmpty from 'lodash.isempty';
import { Field } from 'redux-form';
import { setStatusNFPAInit } from '../helpers';
import { VIEWS } from './helper';
import { NFPA } from '../constants';
import { ValuesProps } from './types';
import { useAddHardShipValidation } from '../hooks/useAddHardshipValidation';

export interface Props {}

const LongTermHardShipForm: React.FC<Props> = () => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();

  const ref = useRef<any>(null);

  const data = useStoreIdSelector<ValuesProps>(selectHardshipDataForm);
  const loading = useStoreIdSelector<boolean>(selectLongTermHardshipLoading);

  useAddHardShipValidation(data, ref);

  // init common data
  useEffect(() => {
    batch(() => {
      dispatch(actions.getMonthlyOutgoInformation({ storeId }));
      dispatch(actions.getIncomeFrequency({ storeId }));
    });
  }, [dispatch, storeId]);

  // set error for NFPA after load data
  useEffect(() => {
    if (loading || isEmpty(data) || !ref.current) return;

    const immediateId = setImmediate(() => {
      const field: Field<ExtraFieldProps> = ref.current?.props?.onFind(NFPA);
      setStatusNFPAInit(field, data);
    });

    return () => {
      clearImmediate(immediateId);
    };
  }, [data, loading]);

  if (loading) return null;

  const formName = `${storeId}-${VIEWS}`;

  return (
    <>
      <BasicForm data={data} />
      <View
        ref={ref}
        id={formName}
        formKey={formName}
        descriptor={VIEWS}
        value={data}
      />
    </>
  );
};

export default LongTermHardShipForm;
