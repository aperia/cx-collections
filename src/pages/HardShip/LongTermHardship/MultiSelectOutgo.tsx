import React, { useEffect, useMemo, useState } from 'react';

// components
import MultiSelect, {
  MultiSelectProps
} from 'app/_libraries/_dls/components/MultiSelect';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { DropdownBaseChangeEvent } from 'app/_libraries/_dls/components';
import { getGroupFormatInputText } from 'app/helpers/formatMultiSelectText';

export interface MultiSelectFormProps
  extends Omit<MultiSelectProps, 'onChange' | 'onBlur' | 'name' | 'children'> {
  listItems: MagicKeyValue[];
  name: string;
  onChange: (name: string, newValue: any) => void;
  onBlur: (e: React.FocusEvent<any>) => void;
}
const MultiSelectForm: React.FC<MultiSelectFormProps> = ({
  onChange,
  onBlur,
  listItems,
  name,
  value: valueProp,
  ...props
}) => {
  const { t } = useTranslation();
  const [value, setValue] = useState<typeof valueProp>(valueProp);

  const handleChange = (event: DropdownBaseChangeEvent) => {
    const { value } = event.target;
    setValue(value);
  };

  const handleBlur = (event: React.FocusEvent<Element>) => {
    onChange(name, value);
    onBlur(event);
  };

  useEffect(() => setValue(valueProp), [valueProp]);

  const dropdownListItem = useMemo(() => {
    return (listItems || []).map(item => (
      <MultiSelect.Item
        key={item.value + item.index}
        label={t(item.description)}
        value={item}
        variant="checkbox"
      />
    ));
  }, [listItems, t]);

  return (
    <MultiSelect
      name={name}
      value={value}
      onBlur={handleBlur}
      onChange={handleChange}
      variant="group"
      textField="description"
      popupBaseProps={{ popupBaseClassName: 'inside-infobar' }}
      noResult={t('txt_no_results_found')}
      searchBarPlaceholder={t('txt_type_to_search')}
      checkAllLabel={t('txt_all')}
      groupFormatInputText={(...args) =>
        t(getGroupFormatInputText(...args), {
          selected: args[0],
          items: args[1]
        })
      }
      {...props}
    >
      {dropdownListItem}
    </MultiSelect>
  );
};

export default MultiSelectForm;
