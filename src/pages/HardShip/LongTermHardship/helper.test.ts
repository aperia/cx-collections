import {
  mapDataToView,
  mapOutgoingToPost,
  mapLongTermHardship,
  mapDataToPost
} from './helper';

describe('helper', () => {
  it('mapDataToView with data', () => {
    const outgoMultiSelectItems = [0, 1].map(index => ({
      index,
      value: `item${index}`,
      description: `${index}`
    }));
    const outgoingInfo = {
      item0: 0,
      item1: 1
    };
    const data = { outgoMultiSelectItems, outgoingInfo };
    const result = mapDataToView(data);

    expect(result.totalValue).toEqual(0);
  });

  it('mapDataToView with no data', () => {
    const monthlyOutgoItems = [0, 1].map(index => ({
      index,
      value: `item${index}`,
      description: `${index}`
    }));

    const result = mapDataToView({}, monthlyOutgoItems);
    expect(result.totalValue).toEqual(0);
  });

  it('mapDataToView with no data', () => {
    const monthlyOutgoItems = [0, 1].map(index => ({
      index,
      value: `item${index}`,
      description: `${index}`
    }));

    const outgoingInfo = {
      item0: 0,
      item1: 'not a number'
    };

    const result = mapDataToView({ outgoingInfo }, monthlyOutgoItems);
    expect(result.totalValue).toEqual(0);
  });
});
describe('mapOutgoingToPost', () => {
  it('mapOutgoingToPost with empty Data', () => {
    const output = mapOutgoingToPost({});
    expect(output).toEqual({
      outgoingInfo: {},
      totalOutgoing: undefined
    });
  });
  it('mapOutgoingToPost with Data', () => {
    const output = mapOutgoingToPost({ amount: '100', other: '' });
    expect(output.totalOutgoing).toEqual(100);
  });
});

describe('mapLongTermHardship', () => {
  it('mapLongTermHardship with empty data', () => {
    const output = mapLongTermHardship({});
    expect(output).toEqual({});
  });
  it('mapLongTermHardship with default Data', () => {
    const output = mapLongTermHardship({ default: 'default' });
    expect(output).toEqual({ default: 'default' });
  });
  it('mapLongTermHardship with incomeAmount Data', () => {
    const output = mapLongTermHardship({ incomeAmount: '1200', other: '' });
    expect(output).toEqual({ incomeAmount: '1200' });
  });
  it('mapLongTermHardship with outgoingParams Data', () => {
    const output = mapLongTermHardship({ outgoingInfo: { amount: '100' } });
    expect(output).toEqual({
      outgoingInfo: { amount: '100' },
      totalOutgoing: 100
    });
  });
});

describe('mapDataToPost', () => {
  it('mapDataToPost with default Data', () => {
    const rawData = {
      ctdPaymentAmount: '1000',
      other: null
    };
    const modelMapping = {
      ctdPaymentAmount: 'ctdPaymentAmount',
      other: 'other'
    };
    const output = mapDataToPost(rawData, modelMapping);
    expect(output).toEqual({
      ctdPaymentAmount: '1000',
      hardshipType: 'LT',
      other: null
    });
  });
  it('mapDataToPost with startDate Data', () => {
    const startDate = new Date('11/11/2021');
    const rawData = {
      startDate: startDate
    };
    const modelMapping = { startDate: 'startDate' };
    const output = mapDataToPost(rawData, modelMapping);
    expect(output).toEqual({ startDate: '11-11-2021', hardshipType: 'LT' });
  });
  it('mapDataToPost with longTermHardship Data', () => {
    const rawData = {
      longTermHardship: { incomeAmount: '100' }
    };
    const modelMapping = { longTermHardship: 'longTermHardship' };
    const output = mapDataToPost(rawData, modelMapping);
    expect(output).toEqual({
      longTermHardship: { incomeAmount: '100' },
      hardshipType: 'LT'
    });
  });

  it('mapDataToPost with default Data', () => {
    const rawData = {
      default: '1000'
    };
    const modelMapping = { default: 'default' };
    const output = mapDataToPost(rawData, modelMapping);
    expect(output).toEqual({
      default: '1000',
      hardshipType: 'LT'
    });
  });
});
