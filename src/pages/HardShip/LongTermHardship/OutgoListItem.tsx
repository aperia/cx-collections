import React from 'react';

// components
import OutgoItem from './OutgoItem';

// hooks
import { useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { selectMonthlyOutgoItems } from './selectors';

// helper
import { FormikTouched } from 'formik';
import isEmpty from 'lodash.isempty';
import isEqual from 'lodash.isequal';
import { formatCommon } from 'app/helpers';
import { MonthlyOutgoItem, ValuesProps } from './types';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface OutgoListItemProps {
  nameKey: string;
  onRemove: (item: MonthlyOutgoItem) => void;

  outgoMultiSelectItems: (MonthlyOutgoItem | undefined)[];
  outgoInputItems: MagicKeyValue;

  onChange: (name: string, value: any) => void;
  onBlur: {
    (e: React.FocusEvent<any>): void;
    <T = any>(fieldOrEvent: T): T extends string ? (e: any) => void : void;
  };
  touched: FormikTouched<ValuesProps>;
}

const OutgoListItem: React.FC<OutgoListItemProps> = ({
  nameKey,
  onRemove,

  touched,
  outgoMultiSelectItems,
  outgoInputItems: valueObj,

  onBlur,
  onChange
}) => {
  const { t } = useTranslation();

  const monthlyOutgoItems = useStoreIdSelector<MonthlyOutgoItem[]>(
    selectMonthlyOutgoItems
  );

  const total = Object.keys(valueObj).reduce((prev: number, key) => {
    const current = valueObj[key];
    if (isEmpty(current)) return prev;

    return prev + parseFloat(current);
  }, 0);

  const totalView = formatCommon(total).currency();

  if (!outgoMultiSelectItems || outgoMultiSelectItems.length === 0) return null;

  return (
    <>
      {(monthlyOutgoItems || []).map(item => {
        const { value: key, description } = item;
        const isHidden = isEmpty(
          outgoMultiSelectItems.find(_item => isEqual(item, _item))
        );
        if (isHidden) return null;

        const itemName = `${nameKey}.${key}`;

        const touch = (touched as MagicKeyValue)[nameKey];
        const isTouched = touch && touch[key];

        // ex "Auto Insurance Amount"
        const label = t(`${description} Amount`);
        const messageError = t(`${description} Amount Required`);

        return (
          <OutgoItem
            key={key}
            label={label}
            name={itemName}
            touched={isTouched}
            itemInList={item}
            value={valueObj[key]}
            messageError={messageError}
            onBlur={onBlur}
            onChange={onChange}
            onRemove={onRemove}
          />
        );
      })}

      <div className="mt-16 fs-14 ">
        <span
          className="color-grey-d20 fw-500"
          data-testid={genAmtId('outgo', 'total-label', '')}
        >
          {t('txt_totalOutgo')}
        </span>
        : <span data-testid={genAmtId('outgo', 'total', '')}>{totalView}</span>
      </div>
    </>
  );
};

export default OutgoListItem;
