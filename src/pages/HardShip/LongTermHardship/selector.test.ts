import * as selector from './selectors';

// utils
import { HARDSHIP_TYPE, REASON_REMOVE_HARDSHIP } from '../constants';
import { selectorWrapper, storeId } from 'app/test-utils';

const states: Partial<RootState> = {
  hardship: {
    [storeId]: {
      data: [{ ky: 'value' }],
      commonData: {},
      reasonHardship: REASON_REMOVE_HARDSHIP[0],
      typeForm: HARDSHIP_TYPE[2],
      longTerm: {
        incomeFrequenciesLoading: false,
        monthlyOutgoItemsLoading: false,
        incomeFrequencies: [],
        monthlyOutgoItems: []
      }
    }
  }
};

const selectorTrigger = selectorWrapper(states);

describe('HardShip > selector', () => {
  it('selectMonthlyOutgoItems ', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectMonthlyOutgoItems
    );

    expect(data).toEqual(states.hardship![storeId].longTerm?.monthlyOutgoItems);
    expect(emptyData).toBeUndefined();
  });
  it('selectIncomeFrequencies', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectIncomeFrequencies
    );

    expect(data).toEqual(states.hardship![storeId].longTerm?.incomeFrequencies);
    expect(emptyData).toBeUndefined();
  });
  it('selectLongTermHardshipLoading ', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectLongTermHardshipLoading
    );

    expect(data).toEqual(
      states.hardship![storeId].longTerm?.monthlyOutgoItemsLoading ||
        states.hardship![storeId].longTerm?.incomeFrequenciesLoading
    );
    expect(emptyData).toBeUndefined();
  });
});
