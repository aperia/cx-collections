import React from 'react';
import MultiSelectOutgo from './MultiSelectOutgo';
import { fireEvent, render, screen } from '@testing-library/react';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

jest.mock('app/_libraries/_dls/components/MultiSelect', () =>
  jest.requireActual('app/test-utils/mocks/MockMultiSelect')
);

describe('Test MultiSelectOutgo component', () => {
  it('Render UI', () => {
    const onChange = jest.fn();
    const onBlur = jest.fn();
    const listItems = [
      { value: 'value', index: 0, description: 'description' }
    ];
    const name = 'name';
    const value = { value: 'value', index: 0, description: 'description' };

    const { rerender } = render(
      <MultiSelectOutgo
        onChange={onChange}
        onBlur={onBlur}
        listItems={listItems}
        name={name}
        value={value}
      />
    );

    screen.getByTestId('MultiSelect_groupFormatInputText').click();
    const multiselect = screen.getByTestId('MultiSelect.Input_onChange');
    fireEvent.change(multiselect, { target: { value: {} } });

    fireEvent.blur(multiselect, { target: { value: {} } });
    expect(onChange).toHaveBeenCalled();
    expect(onBlur).toHaveBeenCalled();
    const noData = undefined;

    rerender(
      <MultiSelectOutgo
        onChange={onChange}
        onBlur={onBlur}
        listItems={noData as any}
        name={name}
        value={value}
      />
    );
  });
});
