import dateTime from 'date-and-time';
import isEmpty from 'lodash.isempty';
import isNaN from 'lodash.isnan';
import { mappingDataFromObj } from 'app/helpers';
import { FormatTime } from 'app/constants/enums';
import { MonthlyOutgoItem } from './types';

export const VIEWS = 'longTermHardshipFormViews';

export const mapDataToView = (
  data: MagicKeyValue,
  monthlyOutgoItems: MonthlyOutgoItem[] = []
) => {
  const { outgoingInfo = {} } = data;
  let totalValue = 0;

  const _outgoingInfo = Object.keys(outgoingInfo).reduce((prev, key) => {
    return { ...prev, [key.toLocaleLowerCase()]: outgoingInfo[key] };
  }, {}) as MagicKeyValue;

  const result = monthlyOutgoItems.reduce((prev: MagicKeyValue[], item) => {
    const { value, description } = item;
    const keyValue = value.toLocaleLowerCase();
    const valueView = _outgoingInfo[keyValue];

    if (valueView !== 0 && isEmpty(valueView)) return prev;

    const labelView = `${description} Amount`;
    totalValue += isNaN(parseFloat(valueView)) ? 0 : parseFloat(valueView);
    return [
      ...prev,
      {
        value: valueView,
        description: labelView
      }
    ];
  }, []);

  return { fields: result, totalValue };
};

export const mapOutgoingToPost = (value: MagicKeyValue) => {
  if (isEmpty(value))
    return {
      outgoingInfo: {},
      totalOutgoing: undefined
    };
  let totalOutgoing = 0;

  const outgoingInfo = Object.keys(value).reduce((prev, key) => {
    // remove value undefined
    if (isEmpty(value[key])) return prev;

    const newValue = parseFloat(value[key]);
    totalOutgoing += newValue;

    return {
      ...prev,
      [key]: value[key]
    };
  }, {});

  return { outgoingInfo, totalOutgoing };
};

export const mapLongTermHardship = (data: MagicKeyValue) => {
  if (isEmpty(data)) return {};
  return Object.keys(data).reduce((prev, key) => {
    const value = data[key];
    if (value !== 0 && isEmpty(value)) return prev;

    switch (key) {
      case 'incomeAmount':
        return { ...prev, [key]: value };
      case 'outgoingInfo':
        return { ...prev, ...mapOutgoingToPost(value) };
      default:
        return { ...prev, [key]: value };
    }
  }, {});
};

export const mapDataToPost = (
  rawData: MagicKeyValue,
  mappingModel: MagicKeyValue
) => {
  const data = mappingDataFromObj(rawData, mappingModel);

  const nextNewData = Object.keys(data).reduce((prev, key) => {
    const value = data[key];

    let collector;
    switch (key) {
      case 'ctdPaymentAmount':
      case 'incomeAmount':
      case 'newFixedPaymentAmount':
      case 'nonDelinquentMinimumPaymentDueAmount':
        collector = value;
        break;
      case 'startDate':
      case 'endDate':
        collector = dateTime.format(value, FormatTime.DateWithHyphen);
        break;
      case 'longTermHardship':
        return { ...prev, [key]: mapLongTermHardship(value) };
      default:
        collector = value;
    }

    return { ...prev, [key]: collector };
  }, {});

  return {
    ...nextNewData,
    hardshipType: 'LT'
  };
};
