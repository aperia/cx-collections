import React from 'react';
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import LongTermHardShipForm from './index';
import { hardshipActions } from '../_redux/reducers';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

jest.mock('./BasicForm', () => () => <div>Basic form</div>);

HTMLCanvasElement.prototype.getContext = jest.fn();
const hardshipActionsSpy = mockActionCreator(hardshipActions);

describe('Test LongTermHardship component', () => {
  it('Render UI', () => {
    const getMonthlyOutgoInformationSpy = hardshipActionsSpy(
      'getMonthlyOutgoInformation'
    );
    const getIncomeFrequencySpy = hardshipActionsSpy('getIncomeFrequency');

    renderMockStoreId(<LongTermHardShipForm />, {
      initialState: {
        hardship: {
          [storeId]: {
            commonData: {},
            longTerm: {
              incomeFrequenciesLoading: false,
              monthlyOutgoItemsLoading: false
            },
            data: {},
            typeForm: { id: '', value: '' }
          } as any
        }
      }
    });
    expect(getMonthlyOutgoInformationSpy).toBeCalledWith({ storeId });
    expect(getIncomeFrequencySpy).toBeCalledWith({ storeId });
  });

  it('Render UI when loading', () => {
    // mock action
    hardshipActionsSpy('getMonthlyOutgoInformation');
    const getIncomeFrequencySpy = hardshipActionsSpy('getIncomeFrequency');
    renderMockStoreId(<LongTermHardShipForm />, {
      initialState: {
        hardship: {
          [storeId]: {
            commonData: {},
            longTerm: {
              incomeFrequenciesLoading: true,
              monthlyOutgoItemsLoading: true
            },
            data: {},
            typeForm: { id: '', value: '' }
          } as any
        }
      }
    });

    expect(getIncomeFrequencySpy).toBeCalledWith({ storeId });
  });

  it('Render UI when loading', () => {
    jest.useFakeTimers();
    // mock action
    hardshipActionsSpy('getMonthlyOutgoInformation');
    const getIncomeFrequencySpy = hardshipActionsSpy('getIncomeFrequency');
    renderMockStoreId(<LongTermHardShipForm />, {
      initialState: {
        hardship: {
          [storeId]: {
            commonData: { value: 'value' },
            longTerm: {},
            data: {},
            typeForm: { id: '', value: '' }
          } as any
        }
      }
    });
    jest.runAllImmediates();
    expect(getIncomeFrequencySpy).toBeCalledWith({ storeId });
  });
});
