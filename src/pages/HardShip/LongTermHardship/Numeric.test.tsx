import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

// components
import NumericForm from './Numeric';
import { OnChangeNumericEvent } from 'app/_libraries/_dls/components';

describe('Test NumericForm component', () => {
  it('Render UI', async () => {
    const handleChange = jest.fn();
    const handleBlur = jest.fn();
    const value = '1';

    render(
      <NumericForm
        name="name"
        value={value}
        onChange={handleChange}
        onBlur={handleBlur}
      />
    );
    expect(screen.getByRole('textbox')).toBeInTheDocument();

    const eventChange = { target: { value: '2' } } as OnChangeNumericEvent;
    fireEvent.change(screen.getByRole('textbox'), eventChange);
    const eventBlur = {
      target: { value: '2' }
    } as React.FocusEvent<HTMLInputElement>;
    fireEvent.blur(screen.getByRole('textbox'), eventBlur);
    expect(handleChange).toHaveBeenCalled();
    expect(handleBlur).toHaveBeenCalled();
  });
});
