import React from 'react';
import { fireEvent, screen, waitFor } from '@testing-library/react';
// components
import BasicForm, { Props } from './BasicForm';

// utils
import { storeId, renderMockStoreId, queryById } from 'app/test-utils';
import userEvent from '@testing-library/user-event';
import { queryAllByClass, queryByClass } from 'app/_libraries/_dls/test-utils';

jest.mock('app/_libraries/_dls/components/MultiSelect', () =>
  jest.requireActual('app/test-utils/mocks/MockMultiSelect')
);

HTMLCanvasElement.prototype.getContext = jest.fn();

const generateState = () => {
  const state: Partial<RootState> = {
    collectionForm: {
      [storeId]: {
        method: 'edit',
        collectionCurrentData: {
          dateNoticeReceivedMainSecond: 'dateNoticeReceivedMainSecond'
        }
      } as any
    },
    hardship: {
      [storeId]: {
        isLoadingView: false,
        data: {},
        isLoading: false,
        commonData: {
          currentBalance: '1110',
          cycleToDatePaymentsAmount: 0,
          nonDelinquentMpdAmount: 100,
          minimumNewFixedPaymentAmount: 1110,
          newFixedPaymentAmount: 555
        },
        dataDetails: {
          status: 'success',
          caseId: '2289044432884688',
          debtMgmtCode: 'SF8',
          debtMgmtCodeValue: 'D',
          pricingStrategyLockStatus: 'U',
          ctdPaymentAmount: '0000000000000000.00',
          newFixedPaymentAmount: '555.00',
          nonDelinquentMinimumPaymentDueAmount: '0000000000000100.00',
          activeHardshipExists: false,
          cardholderContactedName: 'SMITH,ADELE               ',
          currentBalanceAmount: '0000000001110.00',
          miscellaneousField1Identifier: '    ',
          workoutLastDate: '00000000'
        },
        typeForm: {
          id: 'LT',
          value: 'Long-Term Hardship'
        },
        longTerm: {
          isValid: false,
          isDirty: true,
          values: {
            outgoInputItems: {},
            outgoMultiSelectItems: []
          },
          monthlyOutgoItemsLoading: false,
          incomeFrequenciesLoading: false,
          monthlyOutgoItems: [
            {
              value: 'Mortgage_or_Rent_Payment_Amount',
              description: 'Mortgage or Rent Payment',
              index: 0
            },
            {
              value: 'Property_Taxes_Amount',
              description: 'Property Taxes',
              index: 1
            },
            {
              value: 'Home_Insurance_Amount',
              description: 'Home Insurance',
              index: 2
            }
          ],
          incomeFrequencies: [
            {
              value: 'Weekly',
              description: 'Weekly'
            },
            {
              value: 'Bi-Weekly',
              description: 'Bi-Weekly'
            },
            {
              value: 'Monthly',
              description: 'Monthly'
            }
          ]
        }
      }
    } as MagicKeyValue
  };
  return state;
};

const renderWrapper = (props: Props, initialState?: Partial<RootState>) => {
  return renderMockStoreId(<BasicForm {...props} />, {
    initialState
  });
};

describe('Test BasicForm component', () => {
  it('Should render UI', () => {
    const props = {
      data: {
        outgoMultiSelectItems: [
          {
            value: 'Mortgage_or_Rent_Payment_Amount',
            description: 'Mortgage or Rent Payment',
            index: 0
          }
        ],
        outgoInputItems: { value: '' }
      }
    };
    renderWrapper(props, generateState());
    screen.getByTestId('MultiSelect_groupFormatInputText').click();
    expect(
      screen.queryByText('Mortgage or Rent Payment Amount')
    ).toBeInTheDocument();
  });

  it('Should render UI without state longterm data', () => {
    const props = {
      data: {
        outgoMultiSelectItems: [
          {
            value: 'Mortgage_or_Rent_Payment_Amount',
            description: 'Mortgage or Rent Payment',
            index: 0
          }
        ],
        outgoInputItems: { value: '' }
      }
    };
    const initialState: Partial<RootState> = {
      hardship: {
        [storeId]: {
          longTerm: {
            monthlyOutgoItems: []
          }
        }
      } as MagicKeyValue
    };
    const { wrapper } = renderWrapper(props, initialState);
    expect(
      wrapper.queryByText('Mortgage or Rent Payment Amount')
    ).not.toBeInTheDocument();
  });

  it('Should render UI without props outgoMultiSelectItems', () => {
    const props = {
      data: {
        outgoMultiSelectItems: [
          {
            value: 'Mortgage_or_Rent_Payment_Amount',
            description: 'Mortgage or Rent Payment',
            index: 0
          }
        ]
      }
    } as any;
    const { wrapper } = renderWrapper(props, generateState());

    expect(
      wrapper.queryByText('Mortgage or Rent Payment Amount')
    ).toBeInTheDocument();
  });

  it('Should render UI without props outgoInputItems', () => {
    const props = {
      data: {
        outgoInputItems: {}
      }
    } as any;
    const { wrapper } = renderWrapper(props, generateState());

    expect(
      wrapper.queryByText('Mortgage or Rent Payment Amount')
    ).not.toBeInTheDocument();
  });
});

describe('Action', () => {
  it('handleRemove', () => {
    const props = {
      data: {
        outgoMultiSelectItems: [
          {
            value: 'Mortgage_or_Rent_Payment_Amount',
            description: 'Mortgage or Rent Payment',
            index: 0
          }
        ],
        outgoInputItems: { value: '' }
      }
    };

    const {
      wrapper: { container }
    } = renderWrapper(props, generateState());

    const removeIcon = queryByClass(container, /icon icon-close/);
    waitFor(() => {
      userEvent.click(removeIcon!);
    }).then(() => {
      expect(
        screen.queryByText('Mortgage or Rent Payment Amount')
      ).not.toBeInTheDocument();
    });
  });

  it('handleChangeNumeric', () => {
    // renders
    const props = {
      data: {
        outgoMultiSelectItems: [
          {
            value: 'Mortgage_or_Rent_Payment_Amount',
            description: 'Mortgage or Rent Payment',
            index: 0
          }
        ],
        outgoInputItems: { value: '' }
      }
    };

    const {
      wrapper: { container }
    } = renderWrapper(props, generateState());
    const inputNumber = container.querySelector(
      '.dls-numreric-container input'
    )!;
    waitFor(() => {
      fireEvent.focus(inputNumber);
      fireEvent.change(inputNumber, {
        target: {
          id: 'Test_Input',
          name: 'Numeric',
          value: '12'
        }
      });
      fireEvent.blur(inputNumber);
    });
    expect(queryById(container, 'Test_Input')).toBeInTheDocument();
  });

  it('validate is not error', () => {
    const props = {
      data: {
        outgoMultiSelectItems: [
          {
            value: 'Mortgage_or_Rent_Payment_Amount',
            description: 'Mortgage or Rent Payment',
            index: 0
          }
        ],
        outgoInputItems: { Mortgage_or_Rent_Payment_Amount: '123' }
      }
    };

    const {
      wrapper: { container }
    } = renderWrapper(props, generateState());
    const inputNumber = container.querySelector(
      '.dls-numreric-container input'
    )!;
    waitFor(() => {
      fireEvent.focus(inputNumber);
      fireEvent.change(inputNumber, {
        target: {
          id: 'Test_Input',
          name: 'Numeric',
          value: '12'
        }
      });
      fireEvent.blur(inputNumber);
    });
    expect(queryById(container, 'Test_Input')).toBeInTheDocument();
  });

  it('handleChangeMultiSelect with outgoInputItems empty', () => {
    const props = {
      data: {
        outgoMultiSelectItems: [
          {
            value: 'Mortgage_or_Rent_Payment_Amount',
            description: 'Mortgage or Rent Payment',
            index: 0
          },
          {
            value: 'Home_Insurance_Amount',
            description: 'Home Insurance',
            index: 2
          }
        ],
        incomeAmount: '123',
        outgoInputItems: 0
      } as any
    };
    const { wrapper } = renderWrapper(props, generateState());
    const multiselect = wrapper.getByTestId('MultiSelect.Input_onChange');
    waitFor(() => {
      fireEvent.change(multiselect, {
        target: {
          id: '2',
          name: 'Property Taxes',
          value: {
            value: 'Property_Taxes_Amount',
            description: 'Property Taxes',
            index: 1
          }
        }
      });
    }).then(() => {
      fireEvent.blur(multiselect);
      expect(wrapper.getByText(/Property Taxes/)).toBeInTheDocument();
    });
  });

  it('handleChangeMultiSelect have touched', async () => {
    const props = {
      data: {
        outgoMultiSelectItems: [
          {
            value: 'Mortgage_or_Rent_Payment_Amount',
            description: 'Mortgage or Rent Payment',
            index: 0
          },
          {
            value: 'Home_Insurance_Amount',
            description: 'Home Insurance',
            index: 2
          }
        ],
        incomeAmount: '123'
        // outgoInputItems: 0
      } as any
    };
    const { wrapper } = renderWrapper(props, generateState());
    const removeIcon = queryAllByClass(wrapper.container, /icon icon-close/);
    await waitFor(() => {
      userEvent.click(removeIcon[0]!);
    });
    const multiselect = wrapper.getByTestId('MultiSelect.Input_onChange');
    waitFor(() => {
      fireEvent.change(multiselect, {
        target: {
          id: '2',
          name: 'Property Taxes',
          value: {
            value: 'Property_Taxes_Amount',
            description: 'Property Taxes',
            index: 1
          }
        }
      });
    }).then(() => {
      fireEvent.blur(multiselect);
      expect(wrapper.getByText(/Property Taxes/)).toBeInTheDocument();
    });
  });
});
