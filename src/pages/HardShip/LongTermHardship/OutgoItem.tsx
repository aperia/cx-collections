import React from 'react';

// components
import { Button, Icon, Tooltip } from 'app/_libraries/_dls/components';
import NumericForm from './Numeric';

// helper
import isEmpty from 'lodash.isempty';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { MonthlyOutgoItem } from './types';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface OutgoItemProps {
  name: string;
  itemInList: MonthlyOutgoItem;
  value: any;
  touched?: boolean;
  label: string;
  messageError: string;
  onRemove: (item: MonthlyOutgoItem) => void;
  onChange: (name: string, value: any) => void;
  onBlur: {
    (e: React.FocusEvent<any>): void;
    <T = any>(fieldOrEvent: T): T extends string ? (e: any) => void : void;
  };
}

const OutgoItem: React.FC<OutgoItemProps> = ({
  name,
  itemInList,
  value,
  label,
  touched,
  messageError: message,
  onRemove,

  onBlur,
  onChange
}) => {
  const { t } = useTranslation();
  const isError = isEmpty(value) && value !== '0';
  const status = Boolean(isError && touched);
  const testId = genAmtId('outgoItem', label, '');

  return (
    <div className="mt-16 d-flex align-items-center">
      <NumericForm
        dataTestId={testId}
        error={{ status, message }}
        format="c2"
        required
        name={name}
        label={label}
        value={value}
        onBlur={onBlur}
        onChange={onChange}
        errorTooltipProps={{ triggerClassName: 'd-block flex-fill' }}
      />
      <Tooltip
        placement="top"
        variant="primary"
        element={t('txt_remove')}
        dataTestId={`${testId}-remove`}
      >
        <Button
          variant="icon-secondary"
          onClick={() => onRemove(itemInList)}
          dataTestId={`${testId}-remove-btn`}
        >
          <Icon name="close" color="red" />
        </Button>
      </Tooltip>
    </div>
  );
};

export default OutgoItem;
