import { HardshipData } from '../types';

export interface CommonPayload {
  common: {
    accountId: string;
  };
}

export interface ValuesProps {
  outgoMultiSelectItems: MonthlyOutgoItem[];
  outgoInputItems: MagicKeyValue;
  incomeAmount?: string;
  incomeFrequency?: MagicKeyValue;
}
export interface MonthlyOutgoItem {
  index: number;
  value: string;
  description: string;
}

export interface LongTermHardship extends HardshipData {
  callResultType?: string;
  commonData?: any;
  reasonHardship?: any;
  monthlyOutgoItems?: MonthlyOutgoItem[];
  monthlyOutgoItemsLoading?: boolean;
  incomeFrequencies?: MagicKeyValue[];
  incomeFrequenciesLoading?: boolean;
  isValid?: boolean;
  isDirty?: boolean;
  values?: ValuesProps;
}
