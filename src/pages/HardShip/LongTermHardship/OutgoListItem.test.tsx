import React from 'react';
import { screen } from '@testing-library/react';

// components
import OutgoListItem from './OutgoListItem';
import { renderMockStore, storeId, accEValue } from 'app/test-utils';
import { AccountDetailProvider } from 'app/hooks';
import { formatCommon } from 'app/helpers';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

jest.mock('app/_libraries/_dls/hooks', () => {
  const hooks = jest.requireActual('app/_libraries/_dls/hooks');

  return {
    ...hooks,
    useTranslation: () => ({ t: (text: string) => text })
  };
});

const initialState: Partial<RootState> = {
  hardship: {
    [storeId]: {
      longTerm: {
        monthlyOutgoItems: [
          {
            index: 0,
            value: `item${0}`,
            description: `description${0}`
          },
          {
            index: 1,
            value: `item${1}`,
            description: `description${1}`
          },
          {
            index: 2,
            value: `item${2}`,
            description: `description${2}`
          },
          {
            index: 3,
            value: `item${3}`,
            description: `description${3}`
          }
        ]
      }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  const handleChange = jest.fn();
  const handleBlur = jest.fn();
  const handleRemove = jest.fn();

  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <OutgoListItem
        nameKey="outgoInputItems"
        outgoMultiSelectItems={[0, 1, 2].map(i => ({
          index: i,
          value: `item${i}`,
          description: `description${i}`
        }))}
        outgoInputItems={{
          item0: '1',
          item1: '',
          item2: '3'
        }}
        touched={{ outgoInputItems: {}, outgoMultiSelectItems: [] }}
        onBlur={handleBlur}
        onChange={handleChange}
        onRemove={handleRemove}
      />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('Test OutgoListItem component', () => {
  it('Render UI', async () => {
    const { rerender } = renderWrapper(initialState);
    expect(screen.getByText('txt_totalOutgo')).toBeInTheDocument();

    const totalElm = screen.getByText(formatCommon(4).currency());
    expect(totalElm).toBeInTheDocument();

    const result = rerender(
      <OutgoListItem
        nameKey="outgoInputItems"
        outgoMultiSelectItems={[]}
        outgoInputItems={{}}
        touched={{ outgoInputItems: {}, outgoMultiSelectItems: [] }}
        onBlur={jest.fn()}
        onChange={jest.fn()}
        onRemove={jest.fn()}
      />
    );

    expect(result).toEqual(undefined);
  });

  it('Render UI empty data', () => {
    renderWrapper({});
    expect(screen.getByText('txt_totalOutgo')).toBeInTheDocument();
  });
});
