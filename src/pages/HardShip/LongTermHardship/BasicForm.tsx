import React, { useEffect, useMemo } from 'react';

// components
import { DropdownList } from 'app/_libraries/_dls/components';
import NumericForm from './Numeric';
import OutgoListItem from './OutgoListItem';
import MultiSelectForm from './MultiSelectOutgo';

// hooks
import { useDispatch } from 'react-redux';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { hardshipActions as actions } from '../_redux/reducers';
import { selectIncomeFrequencies, selectMonthlyOutgoItems } from './selectors';

// helper
import { useFormik } from 'formik';
import isEmpty from 'lodash.isempty';
import isEqual from 'lodash.isequal';
import { MonthlyOutgoItem, ValuesProps } from './types';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { getGroupFormatInputText } from 'app/helpers/formatMultiSelectText';

export interface Props {
  data: ValuesProps;
  dataTestId?: string;
}

const BasicForm: React.FC<Props> = ({ data, dataTestId }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();

  const monthlyOutgoItems = useStoreIdSelector<MonthlyOutgoItem[]>(
    selectMonthlyOutgoItems
  );
  const incomeFrequencies = useStoreIdSelector<MagicKeyValue[]>(
    selectIncomeFrequencies
  );

  const {
    values,
    handleChange,
    handleBlur,
    setFieldValue,
    setValues,
    resetForm,
    setTouched,
    touched,
    isValid,
    dirty
  } = useFormik<ValuesProps>({
    initialValues: {
      outgoMultiSelectItems: [],
      outgoInputItems: {}
    },

    validateOnBlur: true,
    validate: ({ outgoInputItems, outgoMultiSelectItems }: ValuesProps) => {
      const errors: MagicKeyValue = {};
      if (outgoMultiSelectItems && outgoMultiSelectItems.length > 0) {
        const isError = outgoMultiSelectItems.some(item =>
          isEmpty(outgoInputItems[item.value])
        );

        if (isError) {
          errors.outgoInputItems = true;
        }
      }
      return errors;
    }
  } as any);

  const {
    incomeFrequency,
    incomeAmount,
    outgoMultiSelectItems,
    outgoInputItems
  } = values;

  const initialData = useMemo(() => {
    if (isEmpty(data) || isEmpty(monthlyOutgoItems)) {
      return {
        outgoMultiSelectItems: [],
        outgoInputItems: {}
      };
    }

    const { outgoMultiSelectItems = [], ...rest } = data;
    const newOutgo = outgoMultiSelectItems.map(item => {
      return monthlyOutgoItems.find(itemInList =>
        isEqual(item.value, itemInList.value)
      );
    }) as MonthlyOutgoItem[];

    return {
      ...rest,
      outgoMultiSelectItems: newOutgo
    };
  }, [data, monthlyOutgoItems]);

  // update isValid
  useEffect(() => {
    dispatch(actions.setFormLongIsValid({ storeId, isValid }));
  }, [dispatch, isValid, storeId]);

  // update is dirty
  useEffect(() => {
    dispatch(actions.setFormLongIsDirty({ storeId, isDirty: dirty }));
  }, [dispatch, dirty, storeId]);

  // update values to submit
  useEffect(() => {
    dispatch(actions.setFormLongTermValues({ storeId, values }));
  }, [dispatch, values, storeId]);

  // set initial Data
  useEffect(() => {
    const {
      outgoInputItems = {},
      incomeAmount,
      incomeFrequency,
      outgoMultiSelectItems
    } = initialData;

    resetForm({
      values: {
        outgoInputItems,
        incomeAmount,
        incomeFrequency,
        outgoMultiSelectItems
      }
    });
  }, [initialData, resetForm]);

  const handleRemove = (item: MonthlyOutgoItem) => {
    const { value: key } = item;

    const newOutgoMultiSelectItems = outgoMultiSelectItems.filter(
      outgoMultiSelectItem => !isEqual(outgoMultiSelectItem, item)
    );

    setValues({
      ...values,
      outgoMultiSelectItems: newOutgoMultiSelectItems,
      outgoInputItems: {
        ...values.outgoInputItems,
        [key]: undefined
      }
    }).then(() => {
      setTouched({
        ...touched,
        outgoInputItems: { ...touched?.outgoInputItems, [key]: false }
      });
    });
  };

  const handleChangeMultiSelect = (_: string, newValue: MonthlyOutgoItem[]) => {
    const newOutgoInputItems = newValue.reduce((prev, { value: key }) => {
      const isEmptyValue =
        !values.outgoInputItems &&
        !values.outgoInputItems[key] &&
        values.outgoInputItems[key] !== 0 &&
        values.outgoInputItems[key] !== '0';

      if (isEmptyValue) return prev;

      return {
        ...prev,
        [key]: values.outgoInputItems[key]
      };
    }, {});
    const newValues = {
      ...values,
      outgoMultiSelectItems: newValue,
      outgoInputItems: newOutgoInputItems
    };
    setValues(newValues).then(() => {
      const newTouchedOutgoInputItems = newValue.reduce(
        (prev, { value: key }) => {
          if (!touched.outgoInputItems) return prev;
          return {
            ...prev,
            [key]: touched.outgoInputItems[key]
          };
        },
        {}
      );
      const newTouched = {
        ...touched,
        outgoInputItems: newTouchedOutgoInputItems
      };

      setTouched(newTouched);
    });
  };

  const handleChangeNumeric = (name: string, value: any) => {
    setFieldValue(name, value, true);
  };

  return (
    <>
      <div
        className="col-12 color-grey fs-11 text-uppercase fw-500 pl-0"
        data-testid={genAmtId(dataTestId, 'incomeInformation', '')}
      >
        {t('txt_incomeInformation')}
      </div>
      <div className="pt-16">
        <DropdownList
          id="incomeFrequency"
          dataTestId={genAmtId(dataTestId, 'incomeFrequency', '')}
          name="incomeFrequency"
          textField="description"
          label={t('txt_incomeFrequency')}
          onChange={handleChange}
          onBlur={handleBlur}
          value={incomeFrequency}
          popupBaseProps={{ popupBaseClassName: 'inside-infobar' }}
          noResult={t('txt_no_results_found')}
        >
          {(incomeFrequencies || []).map((item, index) => {
            const { description: label, value } = item;
            const key = `${value}-${index}`;

            return (
              <DropdownList.Item value={item} label={t(label)} key={key} />
            );
          })}
        </DropdownList>
      </div>

      <div className="pt-16">
        <NumericForm
          format="c2"
          id="incomeAmount"
          dataTestId={genAmtId(dataTestId, 'incomeAmount', '')}
          name="incomeAmount"
          label={t('txt_incomeAmount')}
          value={incomeAmount}
          onChange={handleChangeNumeric}
          onBlur={handleBlur}
        />
      </div>

      <div
        className="col-12 color-grey fs-11 text-uppercase fw-500 pl-0 pt-24"
        data-testid={genAmtId(dataTestId, 'monthlyOutgoInformation', '')}
      >
        {t('txt_monthlyOutgoInformation')}
      </div>
      <div className="pt-16">
        <MultiSelectForm
          id="outgoMultiSelectItems"
          dataTestId={genAmtId(dataTestId, 'outgoMultiSelectItems', '')}
          name="outgoMultiSelectItems"
          label={t('txt_outgoItems')}
          listItems={monthlyOutgoItems}
          value={outgoMultiSelectItems}
          onBlur={handleBlur}
          onChange={handleChangeMultiSelect}
          checkAllLabel={t('txt_all')}
          groupFormatInputText={(...args) =>
            t(getGroupFormatInputText(...args), {
              selected: args[0],
              items: args[1]
            })
          }
        />
      </div>

      <div>
        <OutgoListItem
          nameKey="outgoInputItems"
          onBlur={handleBlur}
          onChange={handleChangeNumeric}
          touched={touched}
          outgoMultiSelectItems={outgoMultiSelectItems}
          outgoInputItems={outgoInputItems}
          onRemove={handleRemove}
        />
      </div>
    </>
  );
};

export default BasicForm;
