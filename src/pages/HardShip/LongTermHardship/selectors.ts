import { createSelector } from '@reduxjs/toolkit';

export const selectMonthlyOutgoItems = createSelector(
  (states: RootState, storeId: string) =>
    states.hardship[storeId]?.longTerm?.monthlyOutgoItems,
  (monthlyOutgoItems?: MagicKeyValue[]) => monthlyOutgoItems
);

export const selectIncomeFrequencies = createSelector(
  (states: RootState, storeId: string) =>
    states.hardship[storeId]?.longTerm?.incomeFrequencies,
  (incomeFrequencies?: MagicKeyValue[]) => incomeFrequencies
);

export const selectLongTermHardshipLoading = createSelector(
  (states: RootState, storeId: string) =>
    states.hardship[storeId]?.longTerm?.incomeFrequenciesLoading ||
    states.hardship[storeId]?.longTerm?.monthlyOutgoItemsLoading,
  (loading?: boolean) => loading
);
