import React, { useEffect, useState } from 'react';
import {
  NumericProps,
  NumericTextBox,
  OnChangeNumericEvent
} from 'app/_libraries/_dls/components';
export interface NumericFormProps
  extends Omit<NumericProps, 'onChange' | 'onBlur' | 'setFieldValue' | 'name'> {
  name: string;
  onChange: (name: string, value: any) => void;
  onBlur: (e: React.FocusEvent<any>) => void;
}
const NumericForm: React.FC<NumericFormProps> = ({
  onChange,
  onBlur,
  name,
  value: valueProp,
  ...props
}) => {
  const [value, setValue] = useState<typeof valueProp>(valueProp);

  const handleOnChange = (event: OnChangeNumericEvent) => {
    const { value } = event.target;
    setValue(value);
  };

  const handleOnBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    onBlur && onBlur(event);
    onChange(name, value);
  };

  useEffect(() => setValue(valueProp), [valueProp]);

  return (
    <NumericTextBox
      name={name}
      value={value}
      onChange={handleOnChange}
      onBlur={handleOnBlur}
      {...props}
    />
  );
};

export default NumericForm;
