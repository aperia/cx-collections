import React from 'react';
import { fireEvent, render, screen } from '@testing-library/react';

// components
import OutgoItem from './OutgoItem';
import { OnChangeNumericEvent } from 'app/_libraries/_dls/components';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});


describe('Test OutgoItem component', () => {
  it('Render UI', async () => {
    const handleChange = jest.fn();
    const handleBlur = jest.fn();
    const handleRemove = jest.fn();
    const value = '1';

    render(
      <OutgoItem
        name="name"
        itemInList={{ value: 'value1', description: 'description', index: 0 }}
        touched
        label="label"
        messageError="error"
        value={value}
        onBlur={handleBlur}
        onChange={handleChange}
        onRemove={handleRemove}
      />
    );
    expect(screen.getByRole('textbox')).toBeInTheDocument();

    const eventChange = { target: { value: '2' } } as OnChangeNumericEvent;
    fireEvent.change(screen.getByRole('textbox'), eventChange);
    const eventBlur = {
      target: { value: '2' }
    } as React.FocusEvent<HTMLInputElement>;
    fireEvent.blur(screen.getByRole('textbox'), eventBlur);
    expect(handleChange).toHaveBeenCalled();
    expect(handleBlur).toHaveBeenCalled();
  });

  it('remove item', () => {
    const handleChange = jest.fn();
    const handleBlur = jest.fn();
    const handleRemove = jest.fn();
    const value = undefined;

    render(
      <OutgoItem
        name="name"
        itemInList={{ value: 'value1', description: 'description', index: 0 }}
        touched
        label="label"
        messageError="error"
        value={value}
        onBlur={handleBlur}
        onChange={handleChange}
        onRemove={handleRemove}
      />
    );

    screen.getByRole('button').click();
    expect(handleRemove).toHaveBeenCalled();
  });
});
