export const NFPA = 'newFixedPaymentAmount';

export enum HARDSHIP_AMOUNT {
  TOTAL_AMOUNT = 'cycleToDatePaymentsAmount',
  NON_DELINQUENT_AMOUNT = 'nonDelinquentMpdAmount',
  NEW_FIXED_PAYMENT_MIN = 'minimumNewFixedPaymentAmount',
  NEW_FIXED_PAYMENT_AMOUNT = 'newFixedPaymentAmount'
}

export const POST_MILITARY_DEPLOYMENT_FIELDS = [
  'deploymentStartDate',
  'deploymentEndDate',
  'groupCountryOfDeployment',
  'countryOfDeployment',
  'newInterestRate',
  'cycleToDatePaymentsAmount',
  'nonDelinquentMpdAmount',
  NFPA,
  'fromPaymentDate',
  'toPaymentDate',
  'reagePaymentPeriod'
];

export const POST_LONG_TERM_FIELDS = [
  'cycleToDatePaymentsAmount',
  'nonDelinquentMpdAmount',
  'paymentMethod',
  NFPA,
  'fromPaymentDate',
  'toPaymentDate',
  'reagePaymentPeriod'
];

export const POST_NATURAL_FIELDS = [
  'cycleToDatePaymentsAmount',
  'nonDelinquentMpdAmount',
  NFPA,
  'fromPaymentDate',
  'toPaymentDate',
  'naturalDisasterImpact',
  'naturalDisasterType',
  'reagePaymentPeriod',
  'manualReage'
];

export const POST_FIELDS = [
  'cycleToDatePaymentsAmount',
  'nonDelinquentMpdAmount',
  NFPA,
  'fromPaymentDate',
  'toPaymentDate',
  'reagePaymentPeriod'
];

export const COMMON_FIELDS = [
  'cycleToDatePaymentsAmount',
  'nonDelinquentMpdAmount',
  NFPA
];

export const HARD_SHIP_FORM_VIEWS = 'hardshipFormViews';
export const NATURAL_DISASTER = 'hardshipNaturalDisasterViews';
export const REASON_REMOVE_HARDSHIP = ['0', '1'];

export const HARDSHIP_TYPE_ID = {
  hardship: 'BH',
  longTerm: 'LT',
  militaryDeployment: 'MD',
  naturalDisaster: 'ND'
};

export const HARDSHIP_TYPE = [
  { id: HARDSHIP_TYPE_ID.hardship, value: 'txt_hardship' },
  { id: HARDSHIP_TYPE_ID.longTerm, value: 'txt_long_term_hardship' },
  { id: HARDSHIP_TYPE_ID.militaryDeployment, value: 'txt_military_deployment' },
  { id: HARDSHIP_TYPE_ID.naturalDisaster, value: 'txt_natural_disaster' }
];

export const HARDSHIP_TYPE_DESC = {
  BH: 'Hardship',
  LT: 'Long-Term Hardship',
  MD: 'Military Deployment',
  ND: 'Natural Disaster Type'
} as MagicKeyValue;

export const NATURAL_DISASTER_TYPE = {
  manual: 'Manual Reage',
  reage: 'Reage Payment Period'
};

export const STATUS_HARDSHIP = {
  FAIL: 'failure',
  SUCCESS: 'success'
};

export const HARDSHIP_COMPLETE = 'COMPLETE';
export const HARDSHIP_IN_PROGRESS = 'IN_PROGRESS';

export const VIEW_TYPE = {
  BASIC: 'BH',
  LONG_TERM: 'LT',
  MILITARY_DEPLOYMENT: 'MD',
  ND: 'ND'
};

export const I18N_HARDSHIP = {
  FIXED_PAYMENT_AMOUNT_ERR_MSG: 'txt_hardship_fixed_validation_error',
  NON_DELINQUENT_MPD_AMOUNT_ERR_MSG: 'txt_hardship_mpd_payment_validation_error'
};
