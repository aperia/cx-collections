import React from 'react';
import { screen } from '@testing-library/react';

// components
import HashShip from './index';

// constants
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';

// utils
import { storeId, renderMockStoreId } from 'app/test-utils';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const generateState = (commonData?: MagicKeyValue) => {
  const state: Partial<RootState> = {
    collectionForm: {
      [storeId]: {
        method: 'edit',
        collectionCurrentData: {
          dateNoticeReceivedMainSecond: 'dateNoticeReceivedMainSecond'
        }
      } as any
    },
    hardship: {
      [storeId]: {
        ragePaymentPeriod: undefined,
        hardshipProgramPeriod: undefined,
        commonData
      } as any
    }
  };
  return state;
};

const renderWrapper = (initialState?: Partial<RootState>) => {
  return renderMockStoreId(<HashShip />, {
    initialState
  });
};

describe('Render', () => {
  it('Render UI with data', async () => {
    jest.useFakeTimers();
    const commonData = { value: 'value' };
    renderWrapper(generateState(commonData));
    jest.runAllImmediates();
    expect(
      screen.getByText(I18N_COLLECTION_FORM.HARDSHIP_TITLE)
    ).toBeInTheDocument();
  });

  it('Render UI with empty Data', async () => {
    renderWrapper(generateState());
    expect(
      screen.getByText(I18N_COLLECTION_FORM.HARDSHIP_TITLE)
    ).toBeInTheDocument();
  });
});
