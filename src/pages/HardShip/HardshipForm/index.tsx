import React, { useEffect, useRef } from 'react';

// components
import { ExtraFieldProps, View } from 'app/_libraries/_dof/core';

// helpers
import isEmpty from 'lodash.isempty';
import { Field } from 'redux-form';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { setStatusNFPAInit } from '../helpers';

// constants
import { HARD_SHIP_FORM_VIEWS, NFPA } from '../constants';

// redux
import { selectHardshipDataForm } from '../_redux/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { useAddHardShipValidation } from '../hooks/useAddHardshipValidation';

export interface Props {}

const HardShipForm: React.FC<Props> = () => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();

  const ref = useRef<any>(null);
  const formName = `${storeId}-${HARD_SHIP_FORM_VIEWS}`;

  const data = useStoreIdSelector<MagicKeyValue>(selectHardshipDataForm);

  useAddHardShipValidation(data, ref);

  // set error for NFPA after load data
  useEffect(() => {
    if (isEmpty(data) || !ref.current) return;

    const immediateId = setImmediate(() => {
      const field: Field<ExtraFieldProps> = ref.current?.props?.onFind(NFPA);
      setStatusNFPAInit(field, data);
    });

    return () => {
      clearImmediate(immediateId);
    };
  }, [data]);

  return (
    <>
      <h6
        className="color-grey"
        data-testid={genAmtId('HardShipForm', 'title', '')}
      >
        {t(I18N_COLLECTION_FORM.HARDSHIP_TITLE)}
      </h6>
      <View
        ref={ref}
        id={formName}
        formKey={formName}
        descriptor={HARD_SHIP_FORM_VIEWS}
        value={data}
      />
    </>
  );
};

export default HardShipForm;
