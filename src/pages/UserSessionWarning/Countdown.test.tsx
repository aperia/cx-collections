import { render } from '@testing-library/react';
import React from 'react';

import { Countdown } from './Countdown';

describe('test Countdown component', () => {
  it('should be working correctly', () => {
    const { getByText } = render(<Countdown minutes="03" seconds="04" />);
    expect(getByText('03')).toBeInTheDocument();
    expect(getByText('04')).toBeInTheDocument();
  });
});
