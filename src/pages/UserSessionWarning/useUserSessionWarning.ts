import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { IDLE_TIME_TO_LOGOUT, SHOW_MODAL_TIME } from './constants';
import { userSessionWarningActions } from './_redux/reducer';
import {
  selectDisableBeforeUnload,
  selectEndSessionTime
} from './_redux/selector';

const zeroPad = (num: number, size: number) => String(num).padStart(size, '0');

const getHumanTime = (diff: number) => {
  const seconds = Math.floor((diff / 1000) % 60);
  const minutes = Math.floor((diff / (1000 * 60)) % 60);
  return { seconds: zeroPad(seconds, 2), minutes: zeroPad(minutes, 2) };
};

const useUserSessionWarning = () => {
  const dispatch = useDispatch();
  const expirationTime = useSelector(selectEndSessionTime);
  const disableBeforeUnload = useSelector(selectDisableBeforeUnload);
  const [isOpenCountdown, setOpenCountdown] = useState(false);
  const [isShowBackToLogin, setShowBackToLogin] = useState(false);
  const [countdownTime, setCountdownTime] = useState(0);

  const { seconds, minutes } = getHumanTime(countdownTime);

  const onContinue = () => {
    setOpenCountdown(false);
    dispatch(userSessionWarningActions.setEndSessionTime());
  };

  const onBackToLogin = () => {
    sessionStorage.clear();
    location.replace(window.appConfig?.commonConfig?.loginURL);
  };

  useEffect(() => {
    const workingTimeId = setInterval(() => {
      const diff = expirationTime - Date.now();
      setOpenCountdown(diff <= SHOW_MODAL_TIME * 1000 && diff > 1000);
      setShowBackToLogin(diff < 1000);
      if (diff < parseInt(`-${IDLE_TIME_TO_LOGOUT * 1000}`, 10)) {
        clearInterval(workingTimeId);
        onBackToLogin();
        return;
      }
      setCountdownTime(diff);
    }, 1000);

    return () => {
      clearInterval(workingTimeId);
    };
  }, [expirationTime]);

  // handle reload page confirm modal --> Default of Browser
  useEffect(() => {
    const handleBeforeReload = async (event: BeforeUnloadEvent) => {
      if (process.env.NODE_ENV === 'development') return;
      event.preventDefault();
      event.returnValue = 'it must have a string return';
    };
    !isShowBackToLogin &&
      !disableBeforeUnload &&
      window.addEventListener('beforeunload', handleBeforeReload);
    return () => {
      window.removeEventListener('beforeunload', handleBeforeReload);
    };
  }, [disableBeforeUnload, isShowBackToLogin]);

  return {
    isOpenCountdown,
    isShowBackToLogin,
    onContinue,
    onBackToLogin,
    seconds,
    minutes
  };
};

export { useUserSessionWarning };
