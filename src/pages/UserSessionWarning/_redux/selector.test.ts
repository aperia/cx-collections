import { selectorWrapper } from 'app/test-utils';
import { selectDisableBeforeUnload, selectEndSessionTime } from './selector';

const mEndSessionTime = 1000;

const states: Partial<RootState> = {
  userSessionWarning: {
    endSessionTime: mEndSessionTime,
    disableBeforeUnload: false
  }
};

const selectorTrigger = selectorWrapper(states);

describe('test user session warning selector', () => {
  it('should be return end session time', () => {
    const { data } = selectorTrigger(selectEndSessionTime);
    expect(data).toEqual(mEndSessionTime);
  });

  it('should be return disableBeforeUnload', () => {
    const { data } = selectorTrigger(selectDisableBeforeUnload);
    expect(data).toEqual(false);
  });
});
