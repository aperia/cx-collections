import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { EXPIRATION_TIME } from '../constants';
import { UserSessionWarningState } from '../types';

const now = Date.now();

const initialState: UserSessionWarningState = {
  endSessionTime: now + EXPIRATION_TIME * 1000,
  disableBeforeUnload: false
};

const { actions, reducer } = createSlice({
  name: 'userSessionWarning',
  initialState,
  reducers: {
    setEndSessionTime: draftState => {
      draftState.endSessionTime = Date.now() + EXPIRATION_TIME * 1000;
    },
    setDisableBeforeUnload: (draftState, action: PayloadAction<boolean>) => {
      draftState.disableBeforeUnload = action.payload;
    }
  },
  extraReducers: builder => {}
});

const allActions = {
  ...actions
};

export { allActions as userSessionWarningActions, reducer };
