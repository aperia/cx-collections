import { EXPIRATION_TIME } from '../constants';
import { userSessionWarningActions, reducer } from './reducer';

describe('Test reducer', () => {
  const mockDateNow = 1619600237447;
  it('reset Warning message', () => {
    const oldDateNow = global.Date.now;
    global.Date.now = () => mockDateNow;
    const state = reducer(
      { endSessionTime: 0, disableBeforeUnload: false },
      userSessionWarningActions.setEndSessionTime()
    );
    expect(state.endSessionTime).toEqual(mockDateNow + EXPIRATION_TIME * 1000);
    global.Date.now = oldDateNow;
  });

  it('reset Warning message', () => {
    const state = reducer(
      { endSessionTime: 0, disableBeforeUnload: false },
      userSessionWarningActions.setDisableBeforeUnload(true)
    );
    expect(state.disableBeforeUnload).toEqual(true);
  });
});
