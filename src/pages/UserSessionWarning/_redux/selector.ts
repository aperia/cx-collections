import { createSelector } from '@reduxjs/toolkit';

export const selectEndSessionTime = createSelector(
  (states: RootState) => {
    const data = states.userSessionWarning.endSessionTime;
    return data;
  },
  data => data
);

export const selectDisableBeforeUnload = createSelector(
  (states: RootState) => {
    const data = states.userSessionWarning.disableBeforeUnload;
    return data;
  },
  data => data
);
