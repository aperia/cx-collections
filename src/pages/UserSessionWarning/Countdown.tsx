import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import React from 'react';

interface CountdownProps {
  seconds: string;
  minutes: string;
}

const Countdown: React.FC<CountdownProps> = ({ seconds, minutes }) => {
  const { t } = useTranslation();
  const testId = 'countdown-timer';
  return (
    <div className="count-down">
      <div className="count-down-box">
        <span
          className="count-down-time"
          data-testid={genAmtId(testId, 'minutes', '')}
        >
          {minutes}
        </span>
        <span className="count-down-title">{t('txt_minutes')}</span>
      </div>
      <div className="count-down-box">
        <span
          className="count-down-time"
          data-testid={genAmtId(testId, 'seconds', '')}
        >
          {seconds}
        </span>
        <span className="count-down-title">{t('txt_seconds')}</span>
      </div>
    </div>
  );
};

export { Countdown };
