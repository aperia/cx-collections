import { render } from '@testing-library/react';
import React from 'react';

import UserSessionWarning from './index';
import * as hook from './useUserSessionWarning';

jest.mock('./Countdown', () => ({ Countdown: () => <div>Countdown</div> }));

describe('test user session warning component', () => {
  it('should be render counting modal', () => {
    jest.spyOn(hook, 'useUserSessionWarning').mockReturnValue({
      isOpenCountdown: true,
      isShowBackToLogin: false,
      onContinue: jest.fn(),
      onBackToLogin: jest.fn(),
      minutes: '03',
      seconds: '03'
    });
    const { getByText } = render(<UserSessionWarning />);
    expect(getByText('Countdown')).toBeInTheDocument();
  });

  it('should be render expired modal', () => {
    jest.spyOn(hook, 'useUserSessionWarning').mockReturnValue({
      isOpenCountdown: false,
      isShowBackToLogin: true,
      onContinue: jest.fn(),
      onBackToLogin: jest.fn(),
      minutes: '03',
      seconds: '03'
    });
    const { getByText } = render(<UserSessionWarning />);
    expect(getByText('txt_session_expired')).toBeInTheDocument();
  });
});
