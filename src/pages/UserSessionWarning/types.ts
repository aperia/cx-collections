export interface UserSessionWarningState {
  endSessionTime: number;
  disableBeforeUnload: boolean;
}
