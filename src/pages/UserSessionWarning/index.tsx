import React from 'react';
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

import { useUserSessionWarning } from './useUserSessionWarning';
import { Countdown } from './Countdown';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { EXPIRATION_TIME } from './constants';
import { genAmtId } from 'app/_libraries/_dls/utils';

const UserSessionWarning = () => {
  const { t } = useTranslation();
  const {
    isOpenCountdown,
    isShowBackToLogin,
    onContinue,
    onBackToLogin,
    minutes,
    seconds
  } = useUserSessionWarning();

  const testId = 'user-session-warning';

  return (
    <>
      <Modal
        xs
        show={isOpenCountdown}
        dataTestId={genAmtId(testId, 'countdown-container', '')}
      >
        <ModalHeader
          border
          dataTestId={genAmtId(testId, 'countdown-header', '')}
        >
          <ModalTitle>{t('txt_session_expiring')}</ModalTitle>
        </ModalHeader>
        <ModalBody dataTestId={genAmtId(testId, 'countdown-body', '')}>
          <Countdown minutes={minutes} seconds={seconds} />
          <h5 className="text-center mt-24 mb-16 color-orange-d16">
            {t('txt_this_session_will_expire_soon')}
          </h5>
          <p className="text-center">
            {t('txt_this_session_will_expire_after', {
              count: Math.floor(EXPIRATION_TIME / 60)
            })}
          </p>
        </ModalBody>
        <ModalFooter
          okButtonText={t('txt_continue')}
          onOk={onContinue}
          dataTestId={genAmtId(testId, 'countdown-footer', '')}
        />
      </Modal>

      <Modal
        xs
        show={isShowBackToLogin}
        dataTestId={genAmtId(testId, 'back-to-login-container', '')}
      >
        <ModalHeader
          border
          dataTestId={genAmtId(testId, 'back-to-login-header', '')}
        >
          <ModalTitle>{t('txt_session_expired')}</ModalTitle>
        </ModalHeader>
        <ModalBody dataTestId={genAmtId(testId, 'back-to-login-body', '')}>
          {t('txt_your_session_has_expired')}
        </ModalBody>
        <ModalFooter
          okButtonText={t('txt_back_to_login')}
          onOk={onBackToLogin}
          dataTestId={genAmtId(testId, 'back-to-login-footer', '')}
        />
      </Modal>
    </>
  );
};

export default UserSessionWarning;
