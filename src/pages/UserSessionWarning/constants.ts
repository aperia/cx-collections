export const EXPIRATION_TIME = 900; // 900 second = 15 minutes
export const SHOW_MODAL_TIME = 300; // 300 second = 5 minutes
export const IDLE_TIME_TO_LOGOUT = 300; // 5 minutes
