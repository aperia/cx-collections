import { mockActionCreator, renderHookWithStore } from 'app/test-utils';
import { act } from 'react-dom/test-utils';
import { IDLE_TIME_TO_LOGOUT } from './constants';
import { useUserSessionWarning } from './useUserSessionWarning';
import { userSessionWarningActions } from './_redux/reducer';

const mSessionWarningActions = mockActionCreator(userSessionWarningActions);

describe('test useUserSessionWarning hook', () => {
  it('should be working correctly', () => {
    const now = Date.now();
    jest.useFakeTimers();
    act(() => {
      const { result } = renderHookWithStore(() => useUserSessionWarning(), {
        initialState: {
          userSessionWarning: {
            endSessionTime: now + 2000,
            disableBeforeUnload: false
          }
        }
      });
      jest.advanceTimersByTime(1000);
      expect(result.current.isOpenCountdown).toEqual(true);
    });
  });

  it('should be show back to login', () => {
    const now = Date.now();
    jest.useFakeTimers();
    act(() => {
      const { result } = renderHookWithStore(() => useUserSessionWarning(), {
        initialState: {
          userSessionWarning: {
            endSessionTime: now + 900,
            disableBeforeUnload: false
          }
        }
      });
      jest.advanceTimersByTime(1000);
      expect(result.current.isOpenCountdown).toEqual(false);
      expect(result.current.isShowBackToLogin).toEqual(true);
    });
  });

  it('should be redirect to login', () => {
    const now = Date.now();
    const originalLocation = window.location;
    const mockLocationReplace = jest.fn();
    const mockLocation = new URL('https://google.com');
    (mockLocation as any).replace = mockLocationReplace;
    delete (window as any).location;
    (window as any).location = {
      replace: mockLocationReplace,
      assign: jest.fn()
    };
    jest.useFakeTimers();
    act(() => {
      renderHookWithStore(() => useUserSessionWarning(), {
        initialState: {
          userSessionWarning: {
            endSessionTime: now - IDLE_TIME_TO_LOGOUT * 1000,
            disableBeforeUnload: false
          }
        }
      });
      process.env = Object.assign(process.env, {
        NODE_ENV: 'production'
      });

      jest.advanceTimersByTime(1000);
      expect(mockLocationReplace).toBeCalled();
      window.location = originalLocation;
    });
  });

  it('should be working correctly when trigger onContinue', () => {
    const mSetEndSessionTime = mSessionWarningActions('setEndSessionTime');
    const now = Date.now();
    const { result } = renderHookWithStore(() => useUserSessionWarning(), {
      initialState: {
        userSessionWarning: {
          endSessionTime: now + 900,
          disableBeforeUnload: false
        }
      }
    });
    result.current.onContinue();
    expect(result.current.isOpenCountdown).toEqual(false);
    expect(mSetEndSessionTime).toBeCalled();
  });

  it('should be working correctly when trigger onContinue', () => {
    const now = Date.now();
    const originalLocation = window.location;
    const mockLocationReplace = jest.fn();
    const mockLocation = new URL('https://google.com');
    (mockLocation as any).replace = mockLocationReplace;
    delete (window as any).location;
    (window as any).location = {
      replace: mockLocationReplace,
      assign: jest.fn()
    };
    const { result } = renderHookWithStore(() => useUserSessionWarning(), {
      initialState: {
        userSessionWarning: {
          endSessionTime: now + 900,
          disableBeforeUnload: false
        }
      }
    });

    process.env = Object.assign(process.env, {
      NODE_ENV: 'production'
    });

    window.dispatchEvent(new Event('beforeunload'));
    result.current.onBackToLogin();
    expect(result.current.isOpenCountdown).toEqual(false);
    expect(mockLocationReplace).toBeCalled();
    window.location = originalLocation;
  });

  it('should be working correctly in development mode', () => {
    const now = Date.now();
    const originalLocation = window.location;
    const mockLocationReplace = jest.fn();
    const mockLocation = new URL('https://google.com');
    (mockLocation as any).replace = mockLocationReplace;
    delete (window as any).location;
    (window as any).location = {
      replace: mockLocationReplace,
      assign: jest.fn()
    };
    const { result } = renderHookWithStore(() => useUserSessionWarning(), {
      initialState: {
        userSessionWarning: {
          endSessionTime: now + 900,
          disableBeforeUnload: false
        }
      }
    });
    process.env = Object.assign(process.env, {
      NODE_ENV: 'development'
    });

    window.dispatchEvent(new Event('beforeunload'));
    result.current.onBackToLogin();
    expect(result.current.isOpenCountdown).toEqual(false);
    window.location = originalLocation;
  });
});
