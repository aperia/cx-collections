import React from 'react';

import Payment from '.';
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import { actions } from './__redux/reducers';
import * as entitlements from 'app/entitlements/helpers';
import { PERMISSIONS } from 'app/entitlements/constants';

const spyActions = mockActionCreator(actions);

const { getComputedStyle } = global.window;
window.getComputedStyle = (elm, select) => getComputedStyle(elm, select);

describe('Payment', () => {
  afterAll(() => {
    jest.clearAllMocks();
  });
  it('Render UI > PERMISSIONS.PAYMENT_LIST_VIEW', () => {
    jest
      .spyOn(entitlements, 'checkPermission')
      .mockImplementation(permission => {
        if (permission === PERMISSIONS.PAYMENT_LIST_VIEW) return true;
        return false;
      });
    const { wrapper } = renderMockStoreId(<Payment />, {
      initialState: {
        payment: {
          [storeId]: {
            activeKey: 'key'
          }
        }
      }
    });

    expect(wrapper.getByText('txt_payments')).toBeInTheDocument();
  });

  it('Render UI > PERMISSIONS.PAYMENT_HISTORY_VIEW', () => {
    jest
      .spyOn(entitlements, 'checkPermission')
      .mockImplementation(permission => {
        if (permission === PERMISSIONS.PAYMENT_LIST_VIEW) return false;
        return false;
      });

    const { wrapper } = renderMockStoreId(<Payment />, {
      initialState: {
        payment: {
          [storeId]: {
            activeKey: 'key'
          }
        }
      }
    });

    expect(wrapper.getByText('txt_payments')).toBeInTheDocument();
  });

  it('handleSelect with empty key', () => {
    jest.spyOn(entitlements, 'checkPermission').mockImplementation(() => true);
    const spy = spyActions('setActiveKey');

    const { wrapper } = renderMockStoreId(<Payment />, {
      initialState: {
        payment: {
          [storeId]: {
            activeKey: 'key'
          }
        }
      }
    });
    const button = wrapper.getByText('txt_payment_list')!;
    button.click();
    expect(spy).toHaveBeenCalledWith({
      storeId,
      activeKey: 'PaymentList'
    });
  });

  it('handleSelect with correct key', () => {
    jest.spyOn(entitlements, 'checkPermission').mockImplementation(() => true);
    const spy = spyActions('setActiveKey');

    const { wrapper } = renderMockStoreId(<Payment />, {
      initialState: {
        payment: {
          [storeId]: {
            activeKey: 'PaymentList'
          }
        }
      }
    });
    expect(wrapper.getByText('txt_payments')).toBeInTheDocument();
    const button = wrapper.getAllByText('txt_payment_list')!;
    button[0].click();
    expect(spy).not.toBeCalled();
  });
});
