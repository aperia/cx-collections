import React from 'react';
import { screen } from '@testing-library/react';

// components
import PaymentHistory from 'pages/Payment/PaymentHistory';

// hooks
import { AccountDetailProvider } from 'app/hooks';

// helpers
import { mockActionCreator, renderMockStore, storeId } from 'app/test-utils';

// redux-store
import { paymentHistoryActions } from './__redux/reducers';

// constants
import { I18N_PAYMENT_TEXT } from '../constants';
import { ID_GRID } from './constants';

const paymentHistory = [
  {
    cycleDate: '2021-03-30',
    delinquentAmount: '0000000000000000.00',
    daysDelinquent: '00',
    dueDate: '000000',
    effectiveDate: '2021-02-24',
    minimumPayable: '0000000000000000.00',
    id: '1',
    postingDate: '2021-02-24',
    amountPaid: '120',
    description: 'description'
  },
  {
    cycleDate: '2021-02-25',
    delinquentAmount: '0000000000000000.00',
    daysDelinquent: 'A00',
    dueDate: '000000',
    effectiveDate: '2021-02-24',
    minimumPayable: '0000000000000000.00',
    id: '2',
    postingDate: '2021-02-24',
    amountPaid: '120',
    description: 'description'
  }
];

const initialState: Partial<RootState> = {
  paymentHistory: {
    [storeId]: {
      paymentHistory,
      expandedALl: true,
      loading: false,
      sortBy: { id: ID_GRID.CYCLE_DATE, order: 'asc' }
    }
  }
};

const spyPaymentHistory = mockActionCreator(paymentHistoryActions);

const renderWrapper = (state: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <PaymentHistory />
    </AccountDetailProvider>,
    { initialState: state }
  );
};

beforeEach(() => {
  spyPaymentHistory('getPaymentHistory');
});

describe('Render', () => {
  it('Render UI with empty state', () => {
    renderWrapper({
      paymentHistory: { [storeId]: { loading: false, paymentHistory: [] } }
    });

    expect(
      screen.getByText(I18N_PAYMENT_TEXT.PAYMENT_NO_PAYMENT_HISTORY)
    ).toBeInTheDocument();
  });

  it('Render UI with Data', () => {
    renderWrapper({
      paymentHistory: { [storeId]: { paymentHistory, loading: false } }
    });

    expect(
      screen.getByText(I18N_PAYMENT_TEXT.PAYMENT_HISTORY)
    ).toBeInTheDocument();
  });

  it('Render UI with loading', () => {
    renderWrapper({
      paymentHistory: { [storeId]: { paymentHistory, loading: true } }
    });

    expect(
      screen.getByText(I18N_PAYMENT_TEXT.PAYMENT_HISTORY)
    ).toBeInTheDocument();
  });

  it('Render UI with error', () => {
    renderWrapper({
      paymentHistory: { [storeId]: { paymentHistory, error: true } }
    });

    expect(
      screen.getByText(I18N_PAYMENT_TEXT.PAYMENT_HISTORY)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleSort', () => {
    const mockAction = spyPaymentHistory('updateSortBy');

    renderWrapper(initialState);

    screen.getByText(I18N_PAYMENT_TEXT.CYCLE_DATE).click();

    expect(mockAction).toBeCalledWith({
      storeId,
      sortBy: { id: ID_GRID.CYCLE_DATE, order: 'desc' }
    });
  });

  it('handleExpandAll', () => {
    const mockAction = spyPaymentHistory('updateExpandAll');

    renderWrapper(initialState);

    screen.getByText(I18N_PAYMENT_TEXT.COLLAPSE_ALL_PAYMENTS).click();

    expect(mockAction).toBeCalledWith({ storeId });
  });

  it('handleOnExpand', () => {
    const mockAction = spyPaymentHistory('updateExpand');

    renderWrapper(initialState);

    screen
      .getByTestId(
        'account-detail-payment-tab_payment-history_grid_dls-grid_row-0_expand-btn_dls-button'
      )
      .click();

    expect(mockAction).toBeCalledWith({ storeId, dataKeyList: ['2'] });
  });
});
