import { SortType } from 'app/_libraries/_dls/components';

export const ID_GRID = {
  CYCLE_DATE: 'cycleDate',
  DUE_DATE: 'dueDate',
  MINIMUM_PAYMENT_DUE: 'minimumPayable',
  DELQ_AMOUNT: 'delinquentAmount',
  DAYS_DELQ: 'daysDelinquent'
};

export const DEFAULT_SORT: SortType = { id: ID_GRID.CYCLE_DATE, order: 'desc' };

export const REQUEST_CONFIG = {
  selectFields: [
    'cycleDate',
    'dueDate',
    'minimumPayable',
    'delinquentAmount',
    'daysDelinquent',
    'postDate',
    'effectiveDate',
    'transactionAmount',
    'transactionCode'
  ],
  dateOperator: 'LESS_THAN_OR_EQUAL'
};
