import { SortType } from 'app/_libraries/_dls/components';
import { ID_GRID } from './constants';
import { sortGridData } from './helpers';
import { PaymentHistoryItem } from './types';

const mockGridData: PaymentHistoryItem[] = [
  {
    cycleDate: '2021-02-25',
    delinquentAmount: '0000000000000000.00',
    daysDelinquent: '00',
    dueDate: '2021-01-15',
    effectiveDate: '2021-02-24',
    minimumPayable: '0000000000000000.00',
    id: '2',
    postingDate: '2021-02-24',
    amountPaid: '120',
    description: 'description'
  },
  {
    cycleDate: '2021-02-30',
    delinquentAmount: '0000000000000000.00',
    daysDelinquent: '18',
    dueDate: '2021-01-16',
    effectiveDate: '2021-02-24',
    minimumPayable: '0000000000000012.00',
    id: '1',
    postingDate: '2021-02-24',
    amountPaid: '120',
    description: 'description'
  },
  {
    cycleDate: '2021-01-30',
    delinquentAmount: '0000000000000000.00',
    daysDelinquent: '12',
    dueDate: '2020-01-14',
    effectiveDate: '2021-02-24',
    minimumPayable: '0000000000000015.00',
    id: '1',
    postingDate: '2021-02-24',
    amountPaid: '120',
    description: 'description'
  }
];

describe('Payment > helpers', () => {
  describe('sortGridData', () => {
    it('empty sortBy will use default sort cycledate', () => {
      const result = sortGridData(mockGridData, {} as unknown as SortType);
      expect(result[0].cycleDate).toEqual(mockGridData[1].cycleDate);
    });

    it('desc', () => {
      const result = sortGridData(mockGridData, {
        id: ID_GRID.CYCLE_DATE,
        order: 'desc'
      });

      expect(result[0].cycleDate).toEqual(mockGridData[1].cycleDate);
    });

    it('asc', () => {
      const result = sortGridData(mockGridData, {
        id: ID_GRID.CYCLE_DATE,
        order: 'asc'
      });
      expect(result[0].cycleDate).toEqual(mockGridData[2].cycleDate);
    });

    it('sortBy DueDate desc', () => {
      const result = sortGridData(mockGridData, {
        id: ID_GRID.DUE_DATE,
        order: 'desc'
      });
      expect(result[0].dueDate).toEqual(mockGridData[1].dueDate);
    });

    it('sortBy Minimum paydue desc', () => {
      const result = sortGridData(mockGridData, {
        id: ID_GRID.MINIMUM_PAYMENT_DUE,
        order: 'desc'
      });
      expect(result[0].minimumPayable).toEqual(mockGridData[2].minimumPayable);
    });

    it('sortBy Deliquence Amount desc', () => {
      const result = sortGridData(mockGridData, {
        id: ID_GRID.DELQ_AMOUNT,
        order: 'desc'
      });
      expect(result[0].delinquentAmount).toEqual(
        mockGridData[1].delinquentAmount
      );
    });

    it('sortBy Date Delinquent desc', () => {
      const result = sortGridData(mockGridData, {
        id: ID_GRID.DAYS_DELQ,
        order: 'desc'
      });
      expect(result[0].minimumPayable).toEqual(mockGridData[1].minimumPayable);
    });
  });
});
