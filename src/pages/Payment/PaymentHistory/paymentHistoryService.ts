import { apiService } from 'app/utils/api.service';
import { PaymentHistoryRequest } from './types';

class PaymentHistoryServices {
  getPaymentHistory(data: PaymentHistoryRequest) {
    const url = window.appConfig?.api?.payment?.getPaymentHistory || '';
    return apiService.post(url, data);
  }
}

export const paymentHistoryServices = new PaymentHistoryServices();
