import { responseDefault, storeId } from 'app/test-utils';
import { getPaymentHistory } from './getPaymentList';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';

import { paymentHistoryServices } from '../paymentHistoryService';
import { RECORD_NOT_FOUND_MESSAGE } from 'app/constants';

jest.mock('app/helpers/commons', () => ({
  ...jest.requireActual('app/helpers/commons'),
  getFeatureConfig: () => ({
    common: {
      app: 'cxCollections',
      x500id: 'AAAA1114'
    },
    selectFields: [
      'cycleDate',
      'dueDate',
      'minimumPayable',
      'delinquentAmount',
      'daysDelinquent',
      'postDate',
      'effectiveDate',
      'transactionAmount',
      'transactionCode'
    ],
    dateOperator: 'LESS_THAN_OR_EQUAL'
  })
}));

const requestActionData = { storeId, accountId: storeId };

const initialState: Partial<RootState> = {
  mapping: {
    loading: false,
    data: {
      paymentHistory: {
        cycleDate: 'cycleDate'
      }
    }
  }
};

describe('Payment > getPaymentList', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, initialState);
  });

  const apiData = {
    paymentHistory: [{ cycleDate: '01/01/2021' }, { cycleDate: '02/01/2021' }]
  };
  const mappedData = {
    paymentHistory: [
      { cycleDate: '01/01/2021', id: '0' },
      { cycleDate: '02/01/2021', id: '1' }
    ]
  };

  it('success', async () => {
    jest
      .spyOn(paymentHistoryServices, 'getPaymentHistory')
      .mockResolvedValue({ ...responseDefault, data: apiData });

    const response = await getPaymentHistory(requestActionData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(JSON.stringify(response.payload)).toEqual(
      JSON.stringify(mappedData)
    );
  });

  it('success without mapping state', async () => {
    jest
      .spyOn(paymentHistoryServices, 'getPaymentHistory')
      .mockResolvedValue({ ...responseDefault, data: apiData });

    const newStore = createStore(rootReducer, {});

    const response = await getPaymentHistory(requestActionData)(
      newStore.dispatch,
      newStore.getState,
      {}
    );

    expect(JSON.stringify(response.payload)).toEqual(
      JSON.stringify({ paymentHistory: [] })
    );
  });

  it('error', async () => {
    jest
      .spyOn(paymentHistoryServices, 'getPaymentHistory')
      .mockRejectedValue({ ...responseDefault, data: {} });

    const response = await getPaymentHistory(requestActionData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(JSON.stringify(response.payload)).toEqual(JSON.stringify({}));
  });

  it('error not found', async () => {
    jest.spyOn(paymentHistoryServices, 'getPaymentHistory').mockRejectedValue({
      ...responseDefault,
      data: { message: RECORD_NOT_FOUND_MESSAGE }
    });

    const response = await getPaymentHistory(requestActionData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(JSON.stringify(response.payload)).toEqual(
      JSON.stringify({ paymentHistory: [] })
    );
  });
});
