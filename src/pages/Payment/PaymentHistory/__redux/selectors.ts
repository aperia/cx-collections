import { createSelector } from '@reduxjs/toolkit';
import { sortGridData } from '../helpers';
import { SortBy, PaymentHistoryItem } from '../types';

export const selectLoading = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentHistory[storeId]?.loading || false,
  (loading: boolean) => loading
);

export const selectError = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentHistory[storeId]?.error || false,
  (error: boolean) => error
);

export const selectSortBy: CustomStoreIdSelector<SortBy | undefined> =
  createSelector(
    (states: RootState, storeId: string) =>
      states.paymentHistory[storeId]?.sortBy,
    sortBy => sortBy
  );

export const selectExpandedAll = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentHistory[storeId]?.expandedALl || false,
  (expandedAll: boolean) => expandedAll
);

export const selectPaymentHistory: CustomStoreIdSelector<PaymentHistoryItem[]> =
  createSelector(
    [
      (states: RootState, storeId: string) =>
        states.paymentHistory[storeId]?.paymentHistory,
      selectSortBy
    ],
    (paymentHistory, sortBy) => sortGridData(paymentHistory, sortBy)
  );

export const selectExpandedList = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentHistory[storeId]?.expandedList,
  expandedList => expandedList as string[]
);

export const selectTotalPaymentHistory = createSelector(
  selectPaymentHistory,
  paymentHistory => paymentHistory?.length || 0
);
