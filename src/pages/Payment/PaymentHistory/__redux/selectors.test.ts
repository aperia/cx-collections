import { useSelectorMock } from 'app/test-utils/useSelectorMock';
import * as makeSelector from './selectors';
import { mockActionCreator, storeId } from 'app/test-utils';
import * as helpers from '../helpers';

const state: Partial<RootState> = {
  paymentHistory: {
    [storeId]: {
      loading: true,
      error: true,
      sortBy: { id: 'id', order: 'desc' },
      expandedALl: true,
      paymentHistory: [
        {
          cycleDate: '2021-02-25',
          delinquentAmount: '0000000000000000.00',
          daysDelinquent: '00',
          dueDate: '000000',
          effectiveDate: '2021-02-24',
          minimumPayable: '0000000000000000.00',
          id: '1',
          postingDate: '2021-02-24',
          amountPaid: '120',
          description: 'description'
        },
        {
          cycleDate: '2021-02-25',
          delinquentAmount: '0000000000000000.00',
          daysDelinquent: '00',
          dueDate: '000000',
          effectiveDate: '2021-02-24',
          minimumPayable: '0000000000000000.00',
          id: '2',
          postingDate: '2021-02-24',
          amountPaid: '120',
          description: 'description'
        }
      ],
      expandedList: ['row_1', 'row_2']
    }
  }
};

const emptyState: Partial<RootState> = { paymentHistory: { [storeId]: {} } };

const helpersSpy = mockActionCreator(helpers);

const generateSelectorMock = () => (action: Function) => {
  const data = useSelectorMock(action, state, storeId);
  const noData = useSelectorMock(action, emptyState, storeId);

  return { data, noData };
};

describe('Payment > selectors', () => {
  it('selectLoading', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectLoading);

    expect(data).toEqual(state.paymentHistory![storeId].loading);
    expect(noData).toBeFalsy();
  });

  it('selectError', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectError);

    expect(data).toEqual(state.paymentHistory![storeId].error);
    expect(noData).toBeFalsy();
  });

  it('selectSortBy', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectSortBy);

    expect(data).toEqual(state.paymentHistory![storeId].sortBy);
    expect(noData).toBeUndefined();
  });

  it('selectExpandedAll', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectExpandedAll);

    expect(data).toEqual(state.paymentHistory![storeId].expandedALl);
    expect(noData).toBeFalsy();
  });

  it('selectPaymentHistory', () => {
    helpersSpy('sortGridData', (a1: MagicKeyValue[]) => a1);

    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectPaymentHistory);

    expect(data).toEqual(state.paymentHistory![storeId].paymentHistory);
    expect(noData).toBeUndefined();
  });

  it('selectExpandedList', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectExpandedList);

    expect(data).toEqual(state.paymentHistory![storeId].expandedList);
    expect(noData).toBeUndefined();
  });

  it('selectTotalPaymentHistory', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(
      makeSelector.selectTotalPaymentHistory
    );

    expect(data).toEqual(state.paymentHistory![storeId].paymentHistory!.length);
    expect(noData).toEqual(0);
  });
});
