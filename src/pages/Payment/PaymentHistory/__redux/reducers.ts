import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  UpdateExpand,
  UpdateExpandAll,
  UpdateSortBy,
  PaymentHistoryState
} from '../types';
import difference from 'lodash.difference';
import isEmpty from 'lodash.isempty';

import { getPaymentHistory, getPaymentHistoryBuilder } from './getPaymentList';

const { actions, reducer } = createSlice({
  name: 'paymentHistory',
  initialState: {} as PaymentHistoryState,
  reducers: {
    updateSortBy: (draftState, action: PayloadAction<UpdateSortBy>) => {
      const { storeId, sortBy } = action.payload;

      draftState[storeId].sortBy = sortBy;
    },
    updateExpandAll: (draftState, action: PayloadAction<UpdateExpandAll>) => {
      const { storeId } = action.payload;

      const isExpandedAll = draftState[storeId].expandedALl;

      draftState[storeId].expandedALl = !isExpandedAll;
      draftState[storeId].expandedList = isExpandedAll
        ? []
        : draftState[storeId].paymentHistory!.map(item => item.id);
    },
    updateExpand: (draftState, action: PayloadAction<UpdateExpand>) => {
      const { storeId, dataKeyList = [] } = action.payload;

      draftState[storeId].expandedList = dataKeyList;

      // collapse all
      if (dataKeyList.length === 0) draftState[storeId].expandedALl = false;

      const listId = draftState[storeId].paymentHistory!.map(item => item.id);
      // expand all
      if (isEmpty(difference(listId, dataKeyList))) {
        draftState[storeId].expandedALl = true;
      }
    }
  },

  extraReducers: builder => {
    getPaymentHistoryBuilder(builder);
  }
});

const paymentHistoryActions = {
  ...actions,
  getPaymentHistory
};

export { paymentHistoryActions, reducer };
