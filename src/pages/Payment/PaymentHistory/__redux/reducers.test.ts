import { paymentHistoryActions, reducer } from './reducers';
import { PaymentHistoryState } from '../types';
import { storeId } from 'app/test-utils';
import { SortType } from 'app/_libraries/_dls/components';

// const dataGrid = [
//   {
//     amount: 9,
//     confirmationNumber: '123',
//     paymentAccountNumber: {},
//     paymentId: '123',
//     schedulePaymentDate: '2020, 01, 27',
//     paymentStatus: { value: 'Settled' },
//     paymentType: {}
//   },
//   {
//     amount: 8,
//     confirmationNumber: '123',
//     paymentAccountNumber: {},
//     paymentId: '123',
//     schedulePaymentDate: '2020, 01, 26',
//     paymentStatus: { value: 'Canceled' },
//     paymentType: {}
//   },
//   {
//     amount: 10,
//     confirmationNumber: '123',
//     paymentAccountNumber: {},
//     paymentId: '123',
//     schedulePaymentDate: '2020, 01, 28',
//     paymentStatus: { value: 'Pending' },
//     paymentType: {}
//   },
//   {
//     amount: 7,
//     confirmationNumber: '123',
//     paymentAccountNumber: {},
//     paymentId: '123',
//     schedulePaymentDate: '2020, 01, 25',
//     paymentStatus: {},
//     paymentType: {}
//   }
// ];

const state: PaymentHistoryState = {
  [storeId]: {
    expandedALl: false,
    paymentHistory: [{ id: 'row_1' }]
  }
};

describe('Payment > reducers', () => {
  it('updateSortBy', () => {
    const sortBy: SortType = { id: 'id', order: 'desc' };
    const result = reducer(
      state,
      paymentHistoryActions.updateSortBy({ storeId, sortBy })
    );

    expect(result[storeId].sortBy).toEqual(sortBy);
  });

  describe('updateExpandAll', () => {
    it('collapse all', () => {
      const result = reducer(
        state,
        paymentHistoryActions.updateExpandAll({ storeId })
      );

      expect(result[storeId].expandedALl).toEqual(!state[storeId].expandedALl);
      expect(result[storeId].expandedList).toEqual(
        state[storeId].paymentHistory!.map(item => item.id)
      );
    });

    it('expand all', () => {
      const result = reducer(
        { [storeId]: { expandedALl: true } },
        paymentHistoryActions.updateExpandAll({ storeId })
      );

      expect(result[storeId].expandedALl).toBeFalsy();
      expect(result[storeId].expandedList).toEqual([]);
    });
  });

  describe('updateExpand', () => {
    it('collapse all', () => {
      const result = reducer(
        state,
        paymentHistoryActions.updateExpand({ storeId, dataKeyList: ['row_1'] })
      );

      expect(result[storeId].expandedALl).toBeTruthy();
    });

    it('expand all', () => {
      const result = reducer(
        state,
        paymentHistoryActions.updateExpand({ storeId })
      );

      expect(result[storeId].expandedALl).toBeFalsy();
    });
  });
});
