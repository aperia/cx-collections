import dateTime from 'date-and-time';
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Type
import {
  PaymentHistoryState,
  PaymentHistoryItem,
  PaymentHistoryPayload,
  PaymentHistoryArgs,
  PaymentHistoryRequest
} from '../types';

// Service
import { paymentHistoryServices } from '../paymentHistoryService';

// Helper
import { mappingDataFromArray } from 'app/helpers';

// Const
import { RECORD_NOT_FOUND_MESSAGE } from 'app/constants';
import { FormatTime } from 'app/constants/enums';
import { REQUEST_CONFIG } from '../constants';

export const getPaymentHistory = createAsyncThunk<
  PaymentHistoryPayload,
  PaymentHistoryArgs,
  ThunkAPIConfig
>('paymentHistory/getPaymentHistory', async (args, thunkAPI) => {
  const { accountId } = args;
  const state = thunkAPI.getState();
  const today = dateTime.format(new Date(), FormatTime.FullYearMonthDay);

  const { commonConfig } = window.appConfig || {};

  const requestBody: PaymentHistoryRequest = {
    ...REQUEST_CONFIG,
    common: {
      app: commonConfig?.appCxCollections,
      x500id: commonConfig?.x500id,
      accountId
    },
    date: today
  };

  try {
    const { data } = await paymentHistoryServices.getPaymentHistory(
      requestBody
    );

    const mappingModel = state.mapping?.data?.paymentHistory || {};
    const mappedData = mappingDataFromArray<PaymentHistoryItem>(
      data?.paymentHistory,
      mappingModel
    );

    return {
      paymentHistory: mappedData.map((item, index) => ({
        ...item,
        id: `${index}`
      }))
    };
  } catch (error) {
    const message = error?.data?.message;
    if (message === RECORD_NOT_FOUND_MESSAGE) {
      return {
        paymentHistory: []
      };
    }
    return thunkAPI.rejectWithValue({ errorMessage: error?.message });
  }
});

export const getPaymentHistoryBuilder = (
  builder: ActionReducerMapBuilder<PaymentHistoryState>
) => {
  builder
    .addCase(getPaymentHistory.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: true,
        // set expanded list to empty, will be removed when integrate real api
        expandedList: [],
        expandedALl: false
      };
    })
    .addCase(getPaymentHistory.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { paymentHistory } = action.payload;

      draftState[storeId].paymentHistory = [...paymentHistory].sort((a, b) => {
        return (
          new Date(b.cycleDate).getTime() - new Date(a.cycleDate).getTime()
        );
      });
      draftState[storeId].loading = false;
      draftState[storeId].error = false;
    })
    .addCase(getPaymentHistory.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId].loading = false;
      draftState[storeId].error = true;
    });
};
