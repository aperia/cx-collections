import { ID_GRID, DEFAULT_SORT } from './constants';
import { SortType } from 'app/_libraries/_dls/components';
import { PaymentHistoryItem } from './types';
import orderBy from 'lodash.orderby';

export const sortGridData = (
  gridData: PaymentHistoryItem[] | undefined,
  sortBy?: SortType
) => {
  if (!gridData) return [];
  if (!sortBy?.order) {
    sortBy = DEFAULT_SORT;
  }

  const orderByWrapper = (
    orderFn: (item: PaymentHistoryItem) => string | number | Date
  ) => {
    return orderBy(gridData, [orderFn], [sortBy!.order!]);
  };

  switch (sortBy?.id) {
    case ID_GRID.CYCLE_DATE:
      return orderByWrapper(item => new Date(item.cycleDate));
    case ID_GRID.DUE_DATE:
      return orderByWrapper(item => new Date(item.dueDate));
    case ID_GRID.MINIMUM_PAYMENT_DUE:
      return orderByWrapper(item => parseFloat(item.minimumPayable));
    case ID_GRID.DELQ_AMOUNT:
      return orderByWrapper(item => parseFloat(item.delinquentAmount));
    case ID_GRID.DAYS_DELQ:
      return orderByWrapper(item => parseFloat(item.daysDelinquent));

    default:
      return orderByWrapper(item => new Date(item.cycleDate));
  }
};
