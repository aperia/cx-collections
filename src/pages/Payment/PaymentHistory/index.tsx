import React, {
  FC,
  useCallback,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef
} from 'react';
import classNames from 'classnames';

// components
import { Button, Grid, Icon, SortType } from 'app/_libraries/_dls/components';
import FailedApiReload from 'app/components/FailedApiReload';
import SubRow from './SubRow';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useDispatch } from 'react-redux';
import {
  selectError,
  selectExpandedAll,
  selectExpandedList,
  selectPaymentHistory,
  selectSortBy,
  selectTotalPaymentHistory,
  selectLoading
} from './__redux/selectors';
import { paymentHistoryActions } from './__redux/reducers';

// helpers
import { formatCommon } from 'app/helpers';
import { ID_GRID } from './constants';

// I18N
import { I18N_PAYMENT_TEXT } from '../constants';

import { PaymentHistoryItem } from './types';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface PaymentHistoryProps {}

const PaymentHistory: FC<PaymentHistoryProps> = () => {
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const { storeId, accEValue } = useAccountDetail();
  const divRef = useRef<HTMLDivElement | null>(null);

  const expandAll = useStoreIdSelector<boolean>(selectExpandedAll);
  const expandedList = useStoreIdSelector<string[]>(selectExpandedList);
  const paymentHistory =
    useStoreIdSelector<PaymentHistoryItem[]>(selectPaymentHistory);
  const totalPaymentHistory = useStoreIdSelector<number>(
    selectTotalPaymentHistory
  );
  const isError = useStoreIdSelector<Record<string, string>[]>(selectError);
  const loading = useStoreIdSelector<Record<string, string>[]>(selectLoading);
  const sortBy = useStoreIdSelector<SortType>(selectSortBy);

  const testId = 'account-detail-payment-tab_payment-history';

  const columns = useMemo(
    () => [
      {
        id: ID_GRID.CYCLE_DATE,
        Header: t(I18N_PAYMENT_TEXT.CYCLE_DATE),
        accessor: (row: PaymentHistoryItem) => {
          return formatCommon(row.cycleDate).time.date;
        },
        width: 120,
        isSort: true
      },
      {
        id: ID_GRID.DUE_DATE,
        Header: t(I18N_PAYMENT_TEXT.DUE_DATE),
        accessor: (row: PaymentHistoryItem) => {
          return formatCommon(row.dueDate).time.date;
        },
        width: 120,
        isSort: true
      },
      {
        id: ID_GRID.MINIMUM_PAYMENT_DUE,
        Header: t(I18N_PAYMENT_TEXT.MINIMUM_PAYMENT_DUE),
        accessor: (row: PaymentHistoryItem) => {
          return formatCommon(row.minimumPayable).currency(2);
        },
        width: 184,
        cellProps: { className: 'text-right' },
        isSort: true
      },
      {
        id: ID_GRID.DELQ_AMOUNT,
        Header: t(I18N_PAYMENT_TEXT.DELQ_AMOUNT),
        accessor: (row: PaymentHistoryItem) => {
          return formatCommon(row.delinquentAmount).currency(2);
        },
        width: 170,
        cellProps: { className: 'text-right' },
        isSort: true
      },
      {
        id: ID_GRID.DAYS_DELQ,
        Header: t(I18N_PAYMENT_TEXT.DAYS_DELQ),
        accessor: (row: PaymentHistoryItem) => {
          const parsedValue = parseInt(row.daysDelinquent);
          return isNaN(parsedValue) ? '' : parsedValue;
        },
        cellProps: { className: 'text-right' },
        width: 170,
        isSort: true
      }
    ],
    [t]
  );

  const handleExpandAll = () => {
    dispatch(
      paymentHistoryActions.updateExpandAll({
        storeId
      })
    );
  };

  const handleOnExpand = useCallback(
    (dataKeyList: string[]) => {
      dispatch(
        paymentHistoryActions.updateExpand({
          storeId,
          dataKeyList
        })
      );
    },
    [dispatch, storeId]
  );

  const handleGetPaymentHistory = useCallback(() => {
    dispatch(
      paymentHistoryActions.getPaymentHistory({
        storeId,
        accountId: accEValue
      })
    );
  }, [dispatch, accEValue, storeId]);

  useEffect(() => {
    handleGetPaymentHistory();
  }, [handleGetPaymentHistory]);

  const handleSort = useCallback(
    (e: SortType) => {
      dispatch(paymentHistoryActions.updateSortBy({ storeId, sortBy: e }));
    },
    [dispatch, storeId]
  );

  const renderBody = useMemo(() => {
    if (loading) return null;
    if (isError)
      return (
        <FailedApiReload
          id="payment-history-failed"
          className="mt-80"
          onReload={handleGetPaymentHistory}
          dataTestId={`${testId}_api-reload-failed`}
        />
      );
    if (totalPaymentHistory === 0)
      return (
        <div
          className="text-center my-80"
          data-testid={genAmtId(testId, 'no-payments-history', '')}
        >
          <Icon
            name="file"
            className="fs-80 color-light-l12"
            dataTestId={testId}
          />
          <p data-testid={genAmtId(testId, 'no-payment', '')}>
            {t(I18N_PAYMENT_TEXT.PAYMENT_NO_PAYMENT_HISTORY)}
          </p>
        </div>
      );

    return (
      <Grid
        expandedText={t('txt_expand')}
        collapsedText={t('txt_collapse')}
        columns={columns}
        data={paymentHistory}
        subRow={SubRow}
        expand={{ id: 'expand' }}
        expandedItemKey={'id'}
        expandedList={expandedList}
        onExpand={handleOnExpand}
        sortBy={sortBy ? [sortBy] : []}
        onSortChange={handleSort}
        dataTestId={`${testId}_grid`}
      />
    );
  }, [
    columns,
    expandedList,
    handleGetPaymentHistory,
    handleOnExpand,
    handleSort,
    isError,
    loading,
    paymentHistory,
    sortBy,
    t,
    totalPaymentHistory
  ]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.minHeight = '250px');
  }, []);

  return (
    <div className={classNames({ loading })} ref={divRef}>
      <div className="d-flex align-items-center justify-content-between pt-24 pb-16 mr-n8">
        <h5 data-testid={genAmtId(testId, 'title', '')}>
          {t(I18N_PAYMENT_TEXT.PAYMENT_HISTORY)}
        </h5>
        {totalPaymentHistory !== 0 && !isError && (
          <Button
            size="sm"
            variant="outline-primary"
            onClick={handleExpandAll}
            dataTestId={`${testId}_expand-collapse-all-btn`}
          >
            {t(
              expandAll
                ? I18N_PAYMENT_TEXT.COLLAPSE_ALL_PAYMENTS
                : I18N_PAYMENT_TEXT.EXPAND_ALL_PAYMENTS
            )}
          </Button>
        )}
      </div>
      {renderBody}
    </div>
  );
};

export default PaymentHistory;
