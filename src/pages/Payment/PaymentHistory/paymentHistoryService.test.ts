import { paymentHistoryServices } from './paymentHistoryService';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import { PaymentHistoryRequest } from './types';

describe('paymentHistoryServices', () => {
  describe('getPaymentHistory', () => {
    const params: PaymentHistoryRequest = {
      date: '23/01/2021',
      dateOperator: 'dateOperator'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      paymentHistoryServices.getPaymentHistory(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      paymentHistoryServices.getPaymentHistory(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.getPaymentHistory,
        params
      );
    });
  });
});
