export interface PaymentHistoryState {
  [storeId: string]: {
    paymentHistory?: PaymentHistoryItem[];
    loading?: boolean;
    error?: boolean;
    sortBy?: SortBy;
    expandedALl?: boolean;
    expandedList?: string[];
  };
}

export interface SortBy {
  id: string;
  order?: 'asc' | 'desc';
}

export interface UpdateSortBy {
  storeId: string;
  sortBy: SortBy;
}

export interface UpdateExpandAll {
  storeId: string;
}

export interface UpdateExpand {
  storeId: string;
  dataKeyList?: string[];
}

export interface PaymentHistoryRequest extends CommonAccountIdRequestBody {
  date: string;
  dateOperator: string;
}

export interface PaymentHistoryArgs {
  storeId: string;
  accountId: string;
}

export interface PaymentHistoryPayload {
  paymentHistory: PaymentHistoryItem[];
}

export interface PaymentHistoryItem {
  id: string;
  cycleDate: string;
  daysDelinquent: string;
  delinquentAmount: string;
  dueDate: string;
  effectiveDate: string;
  minimumPayable: string;
  postingDate: string;
  amountPaid: string;
  description: string;
}
