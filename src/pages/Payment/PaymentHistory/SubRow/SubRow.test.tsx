import React from 'react';

// components
import SubRow from 'pages/Payment/PaymentHistory/SubRow';

// hooks
import { AccountDetailProvider } from 'app/hooks';

// helpers
import { renderMockStore, storeId } from 'app/test-utils';
import { screen } from '@testing-library/react';

// redux-store
jest.mock('app/_libraries/_dof/core/View', () => () => (
  <div data-testid="dof-view" />
));

const original = {
  amountPaid: '-0000000000118.57',
  cycleDate: '2021-03-30',
  daysDelinquent: '00',
  delinquentAmount: '0000000000000000.00',
  description: '271',
  dueDate: '000000',
  effectiveDate: '2021-03-30',
  id: '5',
  minimumPayable: '0000000000000000.00',
  postingDate: '2021-03-30'
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <SubRow original={original} />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('Render', () => {
  it('Render UI with empty state', () => {
    renderWrapper({});

    expect(screen.getAllByTestId('dof-view').length).toEqual(1);
  });
});
