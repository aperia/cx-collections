import React from 'react';
import { useAccountDetail } from 'app/hooks';
import { View } from 'app/_libraries/_dof/core';
import { PaymentHistoryItem } from '../types';

export interface SubGridProps {
  original: PaymentHistoryItem;
}

const SubGrid: React.FC<SubGridProps> = ({ original }) => {
  const { storeId } = useAccountDetail();

  return (
    <div className="px-20 py-4 pb-20">
      <View
        id={`${storeId}-${original.id}-paymentHistoryDetailView`}
        formKey={`${storeId}-${original.id}-paymentHistoryDetailView`}
        descriptor="paymentHistoryDetailView"
        value={original}
      />
    </div>
  );
};

export default SubGrid;
