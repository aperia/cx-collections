export const I18N_PAYMENT_TEXT = {
  PAYMENT_LIST: 'txt_payment_list',
  PAYMENT_HISTORY: 'txt_payment_history',
  EXPAND_ALL_PAYMENTS: 'txt_expand_all_payments',
  COLLAPSE_ALL_PAYMENTS: 'txt_collapse_all_payments',
  PAYMENT_NO_PAYMENT_HISTORY: 'txt_payment_no_payment_history',
  CYCLE_DATE: 'txt_payment_cycle_date',
  DUE_DATE: 'txt_payment_due_date',
  MINIMUM_PAYMENT_DUE: 'txt_payment_minimum_payment_due',
  DELQ_AMOUNT: 'txt_payment_delq_amount',
  DAYS_DELQ: 'txt_payment_days_delq'
};

export const TAB_IDS = {
  paymentList: 'PaymentList',
  paymentHistory: 'PaymentHistory'
};
