import React from 'react';

// components
import PaymentHistory from './PaymentHistory';
import PaymentList from './PaymentList';
import { HorizontalTabs, TabsProps } from 'app/_libraries/_dls/components';

// i18n
import { I18N_PAYMENT_TEXT, TAB_IDS } from './constants';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useDispatch } from 'react-redux';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { selectActiveKey } from './__redux/selectors';
import { actions } from './__redux/reducers';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

const Payments = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const activeKey = useStoreIdSelector(selectActiveKey);
  const testId = 'account-detail-payment-tab';

  const [data, setData] = React.useState<MagicKeyValue>([
    {
      title: t(I18N_PAYMENT_TEXT.PAYMENT_LIST),
      component: PaymentList,
      id: TAB_IDS.paymentList
    },
    {
      title: t(I18N_PAYMENT_TEXT.PAYMENT_HISTORY),
      component: PaymentHistory,
      id: TAB_IDS.paymentHistory
    }
  ]);

  React.useEffect(() => {
    if (!checkPermission(PERMISSIONS.PAYMENT_LIST_VIEW, storeId)) {
      return setData([
        {
          title: t(I18N_PAYMENT_TEXT.PAYMENT_HISTORY),
          component: PaymentHistory,
          id: TAB_IDS.paymentHistory
        }
      ]);
    }

    if (!checkPermission(PERMISSIONS.PAYMENT_HISTORY_VIEW, storeId)) {
      return setData([
        {
          title: t(I18N_PAYMENT_TEXT.PAYMENT_LIST),
          component: PaymentList,
          id: TAB_IDS.paymentList
        }
      ]);
    }
  }, [setData, t, storeId]);

  const handleSelect = (key: string | null) => {
    if (key === activeKey || key === null) return;

    dispatch(actions.setActiveKey({ storeId, activeKey: key }));
  };

  const FinalComponent = data[0]?.component;

  const renderInfo = () => {
    if (data.length === 1) return <FinalComponent />;
    return (
      <HorizontalTabs
        defaultActiveKey={TAB_IDS.paymentList}
        activeKey={activeKey as TabsProps['activeKey']}
        onSelect={handleSelect}
        level2
        mountOnEnter
        unmountOnExit={false}
        dataTestId={testId}
      >
        {data.map(({ id, title, component: Component }: MagicKeyValue) => (
          <HorizontalTabs.Tab key={id} eventKey={id} title={title}>
            <Component />
          </HorizontalTabs.Tab>
        ))}
      </HorizontalTabs>
    );
  };

  return (
    <div className="p-24 mb-64">
      <h4 className="mb-16" data-testid={genAmtId(testId, 'title', '')}>
        {t('txt_payments')}
      </h4>
      {renderInfo()}
    </div>
  );
};

export default Payments;
