import { createSelector } from '@reduxjs/toolkit';

export const selectActiveKey = createSelector(
  (states: RootState, storeId: string) => states.payment[storeId]?.activeKey,
  (activeKey?: string) => activeKey
);
