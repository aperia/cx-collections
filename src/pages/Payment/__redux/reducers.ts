import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface State {
  [storeId: string]: {
    activeKey?: string;
  };
}
const { actions: _actions, reducer } = createSlice({
  name: 'payment',
  initialState: {} as State,
  reducers: {
    setActiveKey: (
      draftState,
      action: PayloadAction<{
        storeId: string;
        activeKey: string;
      }>
    ) => {
      const { storeId, activeKey } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        activeKey
      };
    },
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    }
  },

  extraReducers: builder => {}
});

const actions = {
  ..._actions
};

export { actions, reducer };
