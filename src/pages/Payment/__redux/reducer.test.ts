import { actions, reducer, State } from './reducers';
import { storeId } from 'app/test-utils';

const initialState: State = {};

describe('Incarceration > reducer', () => {
  it('removeStore', () => {
    // invoke
    const params = { storeId };
    const state = reducer(initialState, actions.removeStore(params));

    // expect
    expect(state[storeId]).toBeUndefined();
  });

  it('setActiveKey', () => {
    // invoke
    const params = { storeId, activeKey: 'activeKey' };
    const state = reducer(initialState, actions.setActiveKey(params));

    // expect
    expect(state[storeId].activeKey).toEqual(params.activeKey);
  });
});
