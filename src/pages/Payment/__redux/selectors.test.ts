import { selectorWrapper, storeId } from 'app/test-utils';
import * as selectors from './selectors';

describe('Payment Selectors', () => {
  const store: Partial<RootState> = {
    payment: {
      [storeId]: { activeKey: 'activeKy' }
    }
  };

  it('selectActiveKey', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectActiveKey
    );

    expect(data).toEqual(store.payment![storeId]!.activeKey);
    expect(emptyData).toEqual(undefined);
  });
});
