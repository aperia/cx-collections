import { genAmtId } from 'app/_libraries/_dls/utils';
import React from 'react';

// types
import { PaymentFormatData } from '../types';

export interface Props {
  paymentData: PaymentFormatData;
  dataTestId?: string;
}

const AccountNumberGrid: React.FC<Props> = ({ paymentData, dataTestId }) => {
  const mask = paymentData?.paymentAccountNumber || '';

  if (!mask) return <span className="form-group-static__text" />;

  return (
    <div className="data-value">
      <span
        className="custom-account-number"
        data-testid={genAmtId(dataTestId, 'account-number', '')}
      >
        <span className="hide-account-icon">·····</span>
        {mask}
      </span>
    </div>
  );
};

export default AccountNumberGrid;
