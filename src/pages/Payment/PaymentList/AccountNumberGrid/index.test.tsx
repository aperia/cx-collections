import React from 'react';
import { render } from '@testing-library/react';

import AccountNumberGrid from '.';
import { queryByClass } from 'app/test-utils';

describe('Render', () => {
  it('Render UI > with mask', () => {
    const { baseElement } = render(<AccountNumberGrid paymentData={{}} />);

    expect(
      queryByClass(baseElement as HTMLElement, 'form-group-static__text')
    ).toBeInTheDocument();
  });

  it('Render UI > without mask', () => {
    const { baseElement } = render(
      <AccountNumberGrid paymentData={{ paymentAccountNumber: '123456' }} />
    );

    expect(
      queryByClass(baseElement as HTMLElement, 'data-value')
    ).toBeInTheDocument();
  });
});
