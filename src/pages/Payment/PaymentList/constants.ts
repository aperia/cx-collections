// types
import { SortType } from 'app/_libraries/_dls/components';

export enum ATTRIBUTE_SEARCH_PAYMENT {
  ACCOUNT_NUMBER = 'last4AccountNumber',
  BANK = 'bankRoutingNumber',
  CONFIRMATION = 'confirmationNumber',
  AMOUNT = 'paymentAmount',
  END_DATE = 'endDate',
  START_DATE = 'startDate'
}

export enum STATUS_PAYMENT {
  PENDING = 'txt_pending',
  SETTLED = 'txt_success',
  CANCELED = 'txt_canceled'
}

export enum STATUS_CODE {
  PENDING = 3,
  SETTLED = 2,
  CANCELED = 1
}

export enum ID_SORT_GRID {
  AMOUNT = 'amount',
  STATUS = 'paymentStatus',
  DATE = 'schedulePaymentDate',
  TYPE = 'paymentType',
  ACCOUNT = 'paymentAccountNumber',
  CONFIRM = 'confirmationNumber'
}

export const PAGE = 1;

export const SORT_PAYMENT: SortType = { id: ID_SORT_GRID.DATE };

export const MESSAGE = {
  INVALID_FORMAT: 'txt_invalid_format_dot',
  ACCOUNT_NUMBER_REQUIRED: 'txt_account_number_is_required',
  BANK_REQUIRED: 'txt_bank_routing_number_is_required',
  CONFIRM_REQUIRED: 'txt_confirmation_number_is_required',
  AMOUNT_REQUIRED: 'txt_payment_amount_required',
  END_DATE_INVALID: 'txt_end_date_invalid',
  START_DATE_INVALID: 'txt_start_date_invalid',
  START_DATE_REQUIRED: 'txt_start_date_required',
  END_DATE_REQUIRED: 'txt_end_date_required'
};

export enum DESCRIPTION {
  ACCOUNT_NUMBER = 'txt_attribute_search_account_number_description',
  BANK = 'txt_nine_digits',
  CONFIRMATION = 'txt_attribute_search_confirmation_description',
  AMOUNT = 'txt_attribute_search_amount_description',
  END_DATE = 'mm/dd/yyyy',
  START_DATE = 'mm/dd/yyyy'
}

export enum NAME {
  ACCOUNT_NUMBER = 'txt_account_number',
  BANK = 'txt_bank_routing_number',
  CONFIRMATION = 'txt_confirmation_number',
  AMOUNT = 'txt_payment_amount',
  END_DATE = 'txt_end_date',
  START_DATE = 'txt_start_date'
}

export enum COMMON_COLOR {
  ORANGE = 'orange',
  GREEN = 'green',
  GREY = 'grey',
  RED = 'red'
}

export const mappingPaymentStatus: MagicKeyValue = {
  Unknown: COMMON_COLOR.GREY,
  Success: COMMON_COLOR.GREEN,
  Payment_Success: COMMON_COLOR.GREEN,
  Cancel_Success: COMMON_COLOR.GREEN,
  Error: COMMON_COLOR.RED,
  Declined: COMMON_COLOR.RED,
  Verification_Failed: COMMON_COLOR.RED,
  Communication_Error: COMMON_COLOR.RED,
  Settled: COMMON_COLOR.GREEN,
  Settlement_Error: COMMON_COLOR.RED,
  Network_Error: COMMON_COLOR.RED,
  Processor_Mismatch: COMMON_COLOR.ORANGE,
  CreditCards_Disabled: COMMON_COLOR.GREY,
  Unaccepted_Card_Type: COMMON_COLOR.ORANGE,
  Payment_Exceeds_System_Limit: COMMON_COLOR.ORANGE,
  Payment_Exceeds_Allowable_Limit: COMMON_COLOR.ORANGE,
  Possible_Duplicate_Payment: COMMON_COLOR.ORANGE,
  Unresolved_Cancellation: COMMON_COLOR.ORANGE,
  Undefined_Item: COMMON_COLOR.GREY,
  Chargeback: COMMON_COLOR.ORANGE,
  Chargeback_Reversal: COMMON_COLOR.ORANGE,
  Settlement_Incomplete: COMMON_COLOR.ORANGE,
  Partial_Settlement: COMMON_COLOR.ORANGE,
  Settlement_Pending: COMMON_COLOR.ORANGE,
  eChecks_Disabled: COMMON_COLOR.GREY,
  Missing_Identification: COMMON_COLOR.ORANGE,
  Waiting_On_PreNote: COMMON_COLOR.ORANGE,
  PreNote_Failed: COMMON_COLOR.RED,
  Stop_Payment_Issued: COMMON_COLOR.RED,
  Non_Sufficient_Funds: COMMON_COLOR.ORANGE,
  Final_Non_Sufficient_Funds: COMMON_COLOR.ORANGE,
  Account_Invalid: COMMON_COLOR.RED,
  Payment_Pending: COMMON_COLOR.ORANGE,
  Post_Date_Too_Large: COMMON_COLOR.ORANGE,
  Refund_Settlement_Pending: COMMON_COLOR.ORANGE,
  Pre_Auth_Success: COMMON_COLOR.GREEN,
  Eligible: COMMON_COLOR.GREEN,
  Not_Eligible: COMMON_COLOR.RED,
  PINLessDebit_Disabled: COMMON_COLOR.GREY,
  PINDebit_Disabled: COMMON_COLOR.GREY,
  OverPayment: COMMON_COLOR.ORANGE,
  Payment_Failed: COMMON_COLOR.RED,
  Refund_Auth_Success: COMMON_COLOR.GREEN
};

const keyCanCheck = 'Payment_Pending';

export const getStatusDisabled = () => {
  return Object.keys(mappingPaymentStatus).filter(key => key !== keyCanCheck);
};

export enum PAYMENT_LIST_GRID {
  DEFAULT_PAGE_NUMBER = 1,
  DEFAULT_PAGE_SIZE = 10
}
