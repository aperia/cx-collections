import React from 'react';

// components
import { Badge } from 'app/_libraries/_dls/components';

// constant
import { mappingPaymentStatus } from '../constants';

// types
import { PaymentFormatData } from '../types';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface Props {
  paymentData: PaymentFormatData;
  dataTestId?: string;
}

const StatusGrid: React.FC<Props> = ({ paymentData, dataTestId }) => {
  const status = paymentData?.paymentStatus || '';
  const color = mappingPaymentStatus[status];
  const text = status.replace('_', ' ');

  return (
    <Badge color={color} dataTestId={genAmtId(dataTestId, 'status', '')}>
      {text}
    </Badge>
  );
};

export default StatusGrid;
