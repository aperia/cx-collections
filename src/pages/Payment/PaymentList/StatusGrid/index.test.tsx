import { render, screen } from '@testing-library/react';
import React from 'react';
import StatusGrid from '.';
import { queryByClass } from 'app/test-utils';

describe('Render', () => {
  it('Render UI', () => {
    render(<StatusGrid paymentData={{ paymentStatus: 'Pending' }} />);

    expect(screen.getByText('Pending')).toBeInTheDocument();
  });

  it('Render UI => empty data', () => {
    const { baseElement } = render(<StatusGrid paymentData={{}} />);

    expect(
      queryByClass(baseElement as HTMLElement, /badge-undefined/)
    ).toBeInTheDocument();
  });
});
