import { paymentListServices } from './api-services';

// utils
import { mockApiServices, storeId } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import {
  CancelPendingPaymentRequest,
  PaymentPost,
  PaymentViewMorePost
} from './types';

describe('paymentListServices', () => {
  describe('cancelPendingPayments', () => {
    const params: CancelPendingPaymentRequest = {
      common: { accountId: storeId, app: 'app' },
      confirmationNumbers: ['8768']
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      paymentListServices.cancelPendingPayments(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      paymentListServices.cancelPendingPayments(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.cancelPendingPayments,
        params
      );
    });
  });

  describe('getPayments', () => {
    const params: PaymentPost = {
      common: { accountId: storeId, app: 'app' },
      keyType: 'keyType',
      payeeID: 'payeeID'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      paymentListServices.getPayments(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      paymentListServices.getPayments(params);

      expect(mockService).toBeCalledWith(apiUrl.payment.getPayments, params);
    });
  });

  describe('getPaymentInfo', () => {
    const params: PaymentViewMorePost = {
      routingNumber: '8779'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      paymentListServices.getPaymentInfo(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      paymentListServices.getPaymentInfo(params);

      expect(mockService).toBeCalledWith(
        apiUrl.payment.getPaymentViewMore,
        params
      );
    });
  });
});
