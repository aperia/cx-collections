import React, { useEffect, useMemo, useState } from 'react';

// components
import { Button, Popover } from 'app/_libraries/_dls/components';
import PaymentViewMore from '../ViewPaymentInfo';

// helpers
import classNames from 'classnames';
import { DataViewMore, PaymentFormatData } from '../types';
import { useDispatch } from 'react-redux';
import { actions } from '../_redux/reducers';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { selectPaymentViewMore } from '../_redux/selectors';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface RenderViewInfoProps {
  paymentData: PaymentFormatData;
  dataTestId?: string;
}

const RenderViewInfo: React.FC<RenderViewInfoProps> = ({
  paymentData,
  dataTestId
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const [active, setActive] = useState(false);
  const infoData = useStoreIdSelector<DataViewMore>(selectPaymentViewMore);
  const { routingNumber = '' } = paymentData;

  const handleActive = () => {
    setActive(!active);
  };

  useEffect(() => {
    if (!active) return;

    dispatch(actions.getPaymentInfo({ storeId, postData: { routingNumber } }));
  }, [active, dispatch, routingNumber, storeId]);

  const paymentInfo = useMemo(() => {
    if (!infoData) return {};

    return { ...paymentData, ...infoData };
  }, [infoData, paymentData]);

  return (
    <Popover
      placement="top"
      size="md"
      element={
        <PaymentViewMore
          data={paymentInfo}
          dataTestId={genAmtId(dataTestId!, 'payment', '')}
        />
      }
      triggerClassName="d-inline-block"
      containerClassName="inside-infobar"
      onVisibilityChange={handleActive}
    >
      <Button
        className={classNames({ active })}
        size="sm"
        variant="outline-primary"
        dataTestId={genAmtId(dataTestId!, 'payment_view-btn', '')}
      >
        {t('txt_view')}
      </Button>
    </Popover>
  );
};

export default RenderViewInfo;
