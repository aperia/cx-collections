import React from 'react';
import { screen } from '@testing-library/react';

// Component
import ViewInformation from 'pages/Payment/PaymentList/ViewInformation';

// Helper
import { renderMockStoreId, storeId } from 'app/test-utils';

// Types
import { PaymentFormatData } from '../types';
import { PopoverProps } from 'app/_libraries/_dls/components';

const paymentData: PaymentFormatData = {
  paymentId: '06eaec22-f5e4-4649-8a54-4f523882c6dd',
  amount: '638.17',
  schedulePaymentDate: 'Mon Jan 11 2021 22:08:29 GMT+0700 (Indochina Time)',
  paymentAccountNumber:
    '57ddeca7ecec45b7f6dfa649f61c7f62ad43e57b709b04a34a3e5184b7a6de27',
  confirmationNumber: 'HDKSL-348KDS',
  paymentStatus: 'Settled',
  paymentType: 'D'
};

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');
  const { Popover } = dlsComponents;

  return {
    ...dlsComponents,
    Popover: (props: PopoverProps) => (
      <>
        <div
          data-testid="onVisibilityChange"
          onClick={() => props.onVisibilityChange!(true)}
        />
        <Popover opened {...props} />
      </>
    )
  };
});

const renderWrapper = (
  initialState: any,
  paymentDataInit?: PaymentFormatData
) => {
  return renderMockStoreId(
    <ViewInformation
      paymentData={paymentDataInit ? paymentDataInit : paymentData}
    />,
    initialState
  );
};

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper({
      initialState: {
        paymentsList: {
          [storeId]: {
            dataViewMore: { fee: '1' }
          }
        }
      }
    });

    expect(screen.getByText('txt_view')).toBeInTheDocument();
  });
  it('Render UI => empty data', () => {
    renderWrapper({});
    expect(screen.getByText('txt_view')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('onVisibilityChange', () => {
    jest.useFakeTimers();
    renderWrapper({
      initialState: { paymentsList: { [storeId]: {} } }
    });

    const divElement = screen.getByTestId('onVisibilityChange');

    divElement.click();

    expect(screen.getByText('txt_view')).toBeInTheDocument();
    jest.runAllTimers();
  });
});
