import React from 'react';

// components
import {
  AmountControl,
  AttributeSearchData,
  DatePickerControl,
  InputControl
} from 'app/_libraries/_dls/components';

// constants
import { ATTRIBUTE_SEARCH_PAYMENT, DESCRIPTION, NAME } from './constants';

export const amountPayment: AttributeSearchData = {
  key: ATTRIBUTE_SEARCH_PAYMENT.AMOUNT,
  name: NAME.AMOUNT,
  description: DESCRIPTION.AMOUNT,
  component: props => (
    <AmountControl
      isAllShowNumber
      maxLength={14}
      {...props}
      placeholder="txt_enter_amount"
    />
  ),
  groupName: 'payment'
};

export const startDatePayment: AttributeSearchData = {
  key: ATTRIBUTE_SEARCH_PAYMENT.START_DATE,
  name: NAME.START_DATE,
  description: DESCRIPTION.START_DATE,
  component: props => (
    <DatePickerControl
      popupBaseProps={{ placement: 'bottom-end' }}
      {...props}
    />
  ),
  groupName: 'payment'
};

export const endDatePayment: AttributeSearchData = {
  key: ATTRIBUTE_SEARCH_PAYMENT.END_DATE,
  name: NAME.END_DATE,
  description: DESCRIPTION.END_DATE,
  component: props => (
    <DatePickerControl
      popupBaseProps={{ placement: 'bottom-end' }}
      {...props}
    />
  ),
  groupName: 'payment'
};

export const accountNumberPayment: AttributeSearchData = {
  key: ATTRIBUTE_SEARCH_PAYMENT.ACCOUNT_NUMBER,
  name: NAME.ACCOUNT_NUMBER,
  description: DESCRIPTION.ACCOUNT_NUMBER,
  component: props => (
    <InputControl
      placeholder="txt_enter_keyword"
      maxLength={4}
      required
      mode="number"
      {...props}
    />
  ),
  groupName: 'payment'
};

export const bankNumberPayment: AttributeSearchData = {
  key: ATTRIBUTE_SEARCH_PAYMENT.BANK,
  name: NAME.BANK,
  description: DESCRIPTION.BANK,
  component: props => (
    <InputControl
      placeholder="txt_enter_keyword"
      maxLength={9}
      mode="number"
      {...props}
    />
  ),
  groupName: 'payment'
};

export const confirmNumberPayment: AttributeSearchData = {
  key: ATTRIBUTE_SEARCH_PAYMENT.CONFIRMATION,
  name: NAME.CONFIRMATION,
  description: DESCRIPTION.CONFIRMATION,
  component: props => (
    <InputControl
      placeholder="txt_enter_keyword"
      maxLength={14}
      mode="text"
      {...props}
    />
  ),
  groupName: 'payment'
};

export const initialAttrData = [
  accountNumberPayment,
  bankNumberPayment,
  confirmNumberPayment,
  amountPayment,
  startDatePayment,
  endDatePayment
];

export const getAttributeSearchData = (
  translateFn: Function
): AttributeSearchData[] => [
  {
    ...accountNumberPayment,
    name: translateFn(accountNumberPayment.name)
  },
  {
    ...bankNumberPayment,
    name: translateFn(bankNumberPayment.name)
  },
  {
    ...confirmNumberPayment,
    name: translateFn(confirmNumberPayment.name)
  },
  {
    ...amountPayment,
    name: translateFn(amountPayment.name)
  },
  {
    ...startDatePayment,
    name: translateFn(startDatePayment.name)
  },
  {
    ...endDatePayment,
    name: translateFn(endDatePayment.name)
  }
];
