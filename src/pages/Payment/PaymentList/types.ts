import {
  MainSubConditionValue,
  AttributeSearchValue,
  SortType
} from 'app/_libraries/_dls/components';
import { ReactText } from 'react';

export type FieldType<T> = {
  value?: T;
};

export type AttrMainSubConditionModel = FieldType<MainSubConditionValue>;

export type AttrCommonModel = FieldType<string>;

export interface AttributeSearchFieldModel {
  bankRoutingNumber?: AttrCommonModel;
  last4AccountNumber?: AttrCommonModel;
  confirmationNumber?: AttrCommonModel;
  paymentAmount?: AttrCommonModel;
  endDate?: AttrCommonModel;
  startDate?: AttrCommonModel;
}

export interface PaymentPost {
  common: {
    app: string;
    accountId: string;
  };
  keyType: string;
  payeeID: string;
}

export interface PaymentPostData {
  accountId: string;
}

export interface PaymentArg {
  storeId: string;
  postData: PaymentPostData;
}

export interface UpdateSortBy {
  storeId: string;
  sort: SortType;
}

export interface UpdateCheckList {
  storeId: string;
  dataKeyList: string[];
}

export interface PaymentData {
  amount?: number;
  confirmationNumber: string;
  paymentAccountNumber?: {
    eValue?: string;
    maskedValue?: string;
  };
  paymentId?: string;
  paymentStatus?: {
    description?: string;
    value?: string;
  };

  paymentType?: {
    code?: string;
    description?: string;
  };
  schedulePaymentDate?: string;

  // detail
  paymentChannel?: string;
  paymentTimestamp?: string;
  bankRouting?: string;
  institutionNameAbbreviation?: string;
  institutionCity?: string;
  institutionState?: string;
  fee?: string;
}

export interface ChangePage {
  storeId: string;
  page: number;
}

export interface ChangePageSize {
  storeId: string;
  size: number;
}

export interface UpdateSearch {
  storeId: string;
  filterValue: AttributeSearchValue[];
}

export interface UpdateIsSearch {
  storeId: string;
  isSearch: boolean;
}

export interface UpdateIsRefresh {
  storeId: string;
  isRefresh: boolean;
}

export interface PaymentFormatData {
  amount?: string;
  confirmationNumber?: string;
  paymentAccountNumber?: string;
  paymentId?: string;
  paymentStatus?: string;
  paymentType?: string;
  schedulePaymentDate?: string;
  routingNumber?: string;

  paymentChannel?: string;
  paymentTimestamp?: string;
  bankRouting?: string;
  institutionNameAbbreviation?: string;
  institutionCity?: string;
  institutionState?: string;
  fee?: string;
}
export interface PaymentPayload {
  payments: RawData[];
  minDate?: string;
  maxDate?: string;
}

export interface PaymentState {
  loading?: boolean;
  error?: boolean;
  isFirstCall?: boolean;
  noPayment?: boolean;
  noResult?: boolean;
  page?: number;
  sortBy?: SortType;
  pageSize?: number;
  data?: DataFormat[];
  viewMoreLoading?: boolean;
  dataViewMore?: DataViewMore;
  dataGrid?: MagicKeyValue[];
  allCheckedList?: string[];
  filterValue?: AttributeSearchValue[];
  isSearch?: boolean;
  isRefresh?: boolean;
  clearAndReset?: boolean;
  cancelPendingPayments?: CancelPendingPendingPayments;
  minDate?: string;
  maxDate?: string;
}

export interface PaymentInitialState {
  [storeId: string]: PaymentState;
}

export interface CancelPendingPendingPayments {
  loading: boolean;
}

export interface CancelPendingPaymentsArgs {
  storeId: string;
  postData: {
    accountId: string;
    confirmationNumbers: string[];
  };
}

export interface CancelPendingPaymentsPayload {
  data: MagicKeyValue;
}

export interface CancelPendingPaymentPostData {
  accountId: string;
  confirmationNumbers: string[];
}

export interface CancelPendingPaymentRequest {
  common: {
    app: string;
    accountId: string;
  };
  confirmationNumbers: string[];
}

export interface RawData {
  paymentID: string;
  paymentAmount: number;
  paymentMedium: string;
  eftAccountType: string;
  paymentPostDate: string;

  transactionID?: string;
  appID?: string;
  transactionResultCode?: string;
  paymentResultCode?: string;
  paymentTimestamp?: string;
  originalTransactionAmount?: number;
  netTransactionAmount?: number;
  customReference?: string;
  payeeID?: string;
  fullName?: string;
  firstName?: string;
  lastName?: string;
  accountNumber?: string;
  cardType?: string;
  paymentChannel?: string;
  lastPaymentStatus?: string;
  userID?: string;
  confirmationNumber?: string;
  routingNumber?: string;
  paymentCode?: string;
  fee?: string;
}

export interface DataFormat extends Omit<RawData & GridView, 'accountNumber'> {
  accountNumber?: {
    maskedValue: string;
    eValue: string;
  };
  schedulePaymentDate?: string;
  paymentId: string;
  amount?: ReactText;
  paymentAccountNumber?: ReactText;
  paymentStatus?: string;
  paymentType?: string;
  bankRouting?: ReactText;
}

export interface GridView {
  paymentId: string;
  schedulePaymentDate?: ReactText;
  amount?: ReactText;
  confirmationNumber?: ReactText;
  paymentAccountNumber?: ReactText;
  paymentStatus?: ReactText;
  paymentType?: ReactText;

  // detail
  paymentChannel?: ReactText;
  paymentTimestamp?: ReactText;
  bankRouting?: ReactText;
  institutionNameAbbreviation?: ReactText;
  institutionCity?: ReactText;
  institutionState?: ReactText;
  fee?: ReactText;
}

// payment view more

export interface PaymentViewMorePayload {
  data: {
    financialPaymentInfo: {
      fileKey: string;
      institutionTypeCode: string;
      branchCode: string;
      routingNumber: string;
      institutionFullName: string;
      institutionNameAbbreviation: string;
      institutionCity: string;
      institutionState: string;
    };
  };
}

export interface PaymentViewMoreArg {
  storeId: string;
  postData: {
    routingNumber: string;
  };
}
export interface PaymentViewMorePost {
  routingNumber: string;
}

export interface DataViewMore {
  paymentChannel?: string;
  paymentTimestamp?: string;
  bankRouting?: string;
  institutionNameAbbreviation?: string;
  institutionCity?: string;
  institutionState?: string;
  fee?: string;
}
