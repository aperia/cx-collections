import React from 'react';

import {
  accountNumberPayment,
  amountPayment,
  bankNumberPayment,
  confirmNumberPayment,
  startDatePayment,
  endDatePayment
} from 'pages/Payment/PaymentList/attributeSearch';
import { renderMockStoreId } from 'app/test-utils';

HTMLCanvasElement.prototype.getContext = jest.fn();
const { getComputedStyle } = global.window;
window.getComputedStyle = (elm, select) => getComputedStyle(elm, select);

describe('attributeSearch', () => {
  it('startDatePayment', () => {
    const { component: Component } = startDatePayment;
    const { wrapper } = renderMockStoreId(<Component />);
    const el = wrapper.baseElement.querySelector('.date-picker-control');

    expect(el).toBeInTheDocument();

    wrapper.unmount();
  });

  it('endDatePayment', () => {
    const { component: Component } = endDatePayment;
    const { wrapper } = renderMockStoreId(<Component />);
    const el = wrapper.baseElement.querySelector('.date-picker-control');

    expect(el).toBeInTheDocument();

    wrapper.unmount();
  });

  it('amountPayment', () => {
    const { component: Component } = amountPayment;

    const { wrapper } = renderMockStoreId(<Component />);
    const el = wrapper.baseElement.querySelector('.input-control');
    expect(el).toBeInTheDocument();

    wrapper.unmount();
  });

  it('accountNumberPayment', () => {
    const { component: Component } = accountNumberPayment;
    const { wrapper } = renderMockStoreId(<Component />);
    const el = wrapper.baseElement.querySelector('.input-control');
    expect(el).toBeInTheDocument();

    wrapper.unmount();
  });

  it('bankNumberPayment', () => {
    const { component: Component } = bankNumberPayment;
    const { wrapper } = renderMockStoreId(<Component />);
    const el = wrapper.baseElement.querySelector('.input-control');
    expect(el).toBeInTheDocument();

    wrapper.unmount();
  });

  it('confirmNumberPayment', () => {
    const { component: Component } = confirmNumberPayment;
    const { wrapper } = renderMockStoreId(<Component />);
    const el = wrapper.baseElement.querySelector('.input-control');
    expect(el).toBeInTheDocument();

    wrapper.unmount();
  });
});
