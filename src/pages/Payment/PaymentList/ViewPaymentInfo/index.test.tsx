import React from 'react';
import { screen } from '@testing-library/react';

// Component
import ViewPaymentInfo from 'pages/Payment/PaymentList/ViewPaymentInfo';

// Helper
import { renderMockStoreId, setupWrapper, storeId } from 'app/test-utils';

// Hooks
import { AccountDetailProvider } from 'app/hooks';
import { PaymentFormatData } from '../types';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

jest.mock('lodash.isequal', () => {
  return {
    __esModule: true,
    default: () => false
  };
});
const paymentData = {
  paymentId: '123',
  paymentChannel: 'Channel',
  paymentTimestamp: '12/12/2020',
  bankRouting: 'ABC',
  institutionNameAbbreviation: '',
  institutionCity: '',
  institutionState: '',
  fee: ''
};

const renderWrapper = (paymentData: PaymentFormatData) => {
  return renderMockStoreId(<ViewPaymentInfo data={paymentData} />, {});
};

describe('Render', () => {
  it('Render UI', () => {
    const { wrapper } = renderWrapper(paymentData);

    // rerender with new data
    const paymentData1 = { ...paymentData, fee: '1' };
    const { wrapper: preWrapper } = setupWrapper(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <ViewPaymentInfo data={paymentData1} />
      </AccountDetailProvider>,
      {}
    );

    wrapper.rerender(preWrapper);

    expect(screen.getByTestId('paymentDetailView')).toBeInTheDocument();
  });

  it('Render UI and compare data', () => {
    renderWrapper(paymentData);

    expect(screen.getByTestId('paymentDetailView')).toBeInTheDocument();
  });

  it('Render UI and data is null', () => {
    renderWrapper(null as unknown as PaymentFormatData);

    expect(screen.getByTestId('paymentDetailView')).toBeInTheDocument();
  });
});
