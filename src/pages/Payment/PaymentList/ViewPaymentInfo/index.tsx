import React, { useEffect, useLayoutEffect, useRef, useState } from 'react';

// components
import { View } from 'app/_libraries/_dof/core';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { selectPaymentViewMoreLoading } from '../_redux/selectors';

// helpers
import isEqual from 'lodash.isequal';
import classNames from 'classnames';

// types
import { PaymentFormatData } from '../types';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface Props {
  data: PaymentFormatData;
  dataTestId?: string;
}

const ViewInfoPayment: React.FC<Props> = ({ data, dataTestId }) => {
  const { storeId } = useAccountDetail();
  const divRef = useRef<HTMLDivElement | null>(null);

  const [viewData, setViewData] = useState(data || {});
  const loading = useStoreIdSelector(selectPaymentViewMoreLoading);

  const prevViewData = useRef(viewData);
  prevViewData.current = viewData;

  useEffect(() => {
    !isEqual(prevViewData.current, data) && setViewData(data);
  }, [data]);

  const id = `${storeId}_${data?.paymentId || ''}`;

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.minHeight = '180px');
  }, []);

  return (
    <div
      className={classNames({ loading })}
      ref={divRef}
      data-testid={genAmtId(dataTestId!, 'view-payment-info', '')}
    >
      <View
        id={id}
        formKey={`${id}_view`}
        descriptor="paymentDetailView"
        value={viewData}
        dataTestId={genAmtId(dataTestId!, 'view-payment-info_details', '')}
      />
    </div>
  );
};

export default ViewInfoPayment;
