// types
import { AttributeSearchFieldModel, DataFormat } from './types';

// constant
import { MESSAGE, ATTRIBUTE_SEARCH_PAYMENT } from './constants';

// helpers
import filter from 'lodash.filter';
import isEmpty from 'lodash.isempty';
import pick from 'lodash.pick';
import { stringValidate } from 'app/helpers';

// import { PaymentData } from './types';
import { SortType } from 'app/_libraries/_dls/components';
import { ID_SORT_GRID } from 'pages/Payment/PaymentList/constants';

export const validateAmount = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  if (data.paymentAmount && !data.paymentAmount?.value) {
    return {
      [ATTRIBUTE_SEARCH_PAYMENT.AMOUNT]: translateFn(MESSAGE.AMOUNT_REQUIRED)
    };
  }
};

export const validateStartDate = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  if (data.startDate && !data.startDate?.value) {
    return {
      [ATTRIBUTE_SEARCH_PAYMENT.START_DATE]: translateFn(
        MESSAGE.START_DATE_REQUIRED
      )
    };
  }

  if (
    data.startDate &&
    data.endDate &&
    new Date(data.startDate.value!).getTime() >
      new Date(data.endDate.value!).getTime()
  ) {
    return {
      [ATTRIBUTE_SEARCH_PAYMENT.START_DATE]: translateFn(
        MESSAGE.START_DATE_INVALID
      )
    };
  }
};

export const validateEndDate = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  if (data.endDate && !data.endDate?.value) {
    return {
      [ATTRIBUTE_SEARCH_PAYMENT.END_DATE]: translateFn(
        MESSAGE.END_DATE_REQUIRED
      )
    };
  }
  if (
    data.startDate &&
    data.endDate &&
    new Date(data.startDate.value!).getTime() >
      new Date(data.endDate.value!).getTime()
  ) {
    return {
      [ATTRIBUTE_SEARCH_PAYMENT.START_DATE]: translateFn(
        MESSAGE.END_DATE_INVALID
      )
    };
  }
};

export const validateAccountNumber = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  const accountNumberValue = data.last4AccountNumber?.value;

  if (data.last4AccountNumber && !accountNumberValue) {
    return {
      [ATTRIBUTE_SEARCH_PAYMENT.ACCOUNT_NUMBER]: translateFn(
        MESSAGE.ACCOUNT_NUMBER_REQUIRED
      )
    };
  }

  if (data.last4AccountNumber) {
    const methodValid = stringValidate(accountNumberValue);
    const isRequire = methodValid.isRequire();
    const isValidMinLength = accountNumberValue && methodValid.minLength(4);

    if (!isRequire || !isValidMinLength) {
      return {
        [ATTRIBUTE_SEARCH_PAYMENT.ACCOUNT_NUMBER]: translateFn(
          MESSAGE.INVALID_FORMAT
        )
      };
    }
  }
};

export const validateBankNumberPayment = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  const bankNumber = data.bankRoutingNumber?.value;

  if (data.bankRoutingNumber && !bankNumber) {
    return {
      [ATTRIBUTE_SEARCH_PAYMENT.BANK]: translateFn(MESSAGE.BANK_REQUIRED)
    };
  }

  if (data.bankRoutingNumber) {
    const methodValid = stringValidate(bankNumber);
    const isRequire = methodValid.isRequire();
    const isValidMinLength = bankNumber && methodValid.minLength(9);

    if (!isRequire || !isValidMinLength) {
      return {
        [ATTRIBUTE_SEARCH_PAYMENT.BANK]: translateFn(MESSAGE.INVALID_FORMAT)
      };
    }
  }
};

export const validateConfirmPayment = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  const confirmNumberValue = data.confirmationNumber?.value;
  if (data.confirmationNumber && !data.confirmationNumber?.value) {
    return {
      [ATTRIBUTE_SEARCH_PAYMENT.CONFIRMATION]: translateFn(
        MESSAGE.CONFIRM_REQUIRED
      )
    };
  }

  if (data.confirmationNumber) {
    const methodValid = stringValidate(confirmNumberValue);
    const isRequire = methodValid.isRequire();
    const isValidMinLength = confirmNumberValue && methodValid.minLength(14);

    if (!isRequire || !isValidMinLength) {
      return {
        [ATTRIBUTE_SEARCH_PAYMENT.CONFIRMATION]: translateFn(
          MESSAGE.INVALID_FORMAT
        )
      };
    }
  }
};

export const attrValidations = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  const validateList = [
    validateAccountNumber,
    validateBankNumberPayment,
    validateConfirmPayment,
    validateEndDate,
    validateStartDate
  ];

  const runAllValidates = validateList.map(fn => {
    return fn(data, translateFn);
  });

  const errors: Record<string, string> = runAllValidates.reduce(
    (accumulator, current) => {
      return { ...accumulator, ...current };
    },
    {}
  );
  return Object.keys(errors).length ? errors : undefined;
};

export const sortByGrid = (sortType: SortType, data: DataFormat[]) => {
  const gridData = [...data];

  if (!sortType.order) return gridData;

  switch (sortType.id) {
    case ID_SORT_GRID.AMOUNT:
      return gridData.sort((currentItem, nextItem) => {
        if (sortType.order === 'desc') {
          return nextItem.paymentAmount - currentItem.paymentAmount;
        }
        return currentItem.paymentAmount - nextItem.paymentAmount;
      });
    case ID_SORT_GRID.DATE:
      return gridData.sort((currentItem, nextItem) => {
        if (sortType.order === 'desc') {
          return (
            new Date(nextItem?.paymentPostDate || '').getTime() -
            new Date(currentItem?.paymentPostDate || '').getTime()
          );
        }
        return (
          new Date(currentItem?.paymentPostDate || '').getTime() -
          new Date(nextItem?.paymentPostDate || '').getTime()
        );
      });
    case ID_SORT_GRID.TYPE:
      return [...gridData].sort((a, b) => {
        const textA = (a.paymentType || '').toLocaleLowerCase();
        const textB = (b.paymentType || '').toLocaleLowerCase();
        if (sortType.order === 'desc') {
          return textA > textB ? -1 : 1;
        }
        return textA > textB ? 1 : -1;
      });
    case ID_SORT_GRID.STATUS:
      return [...gridData].sort((a, b) => {
        const textA = (a.paymentStatus || '').toLocaleLowerCase();
        const textB = (b.paymentStatus || '').toLocaleLowerCase();

        if (sortType.order === 'desc') {
          return textA > textB ? -1 : 1;
        }
        return textA > textB ? 1 : -1;
      });
    default:
      return data;
  }
};

export const parseJSON = (input = '') => {
  if (isEmpty(input)) return {};
  try {
    return JSON.parse(input);
  } catch (error) {
    return {};
  }
};

export const getFilterValue = (searchValue: MagicKeyValue[] = []) => {
  return searchValue
    ?.map((item: MagicKeyValue) => pick(item, ['key', 'value']))
    .reduce((searchValues: MagicKeyValue, current: MagicKeyValue) => {
      return {
        ...searchValues,
        [current.key]: current.value
      };
    }, {});
};

export const addDays = (baseDate: Date, days: number) =>
  new Date(baseDate.getTime() + days * 24 * 60 * 60 * 1000);

export const getTimeValueOfStartDate = (
  date: string,
  addMoreDate?: number
): number => {
  if (!date) return 0;
  const dateTime = new Date(date);
  const startOfDay = new Date(
    dateTime.getFullYear(),
    dateTime.getMonth(),
    dateTime.getDate()
  );
  if (addMoreDate) return addDays(startOfDay, 1).valueOf();
  return startOfDay.valueOf();
};

export const isDateBetween = (
  compareDate = '',
  startDate = '',
  endDate = ''
): boolean => {
  if (!compareDate) return false;

  const _compareDate = new Date(compareDate).valueOf();
  const _startDate = getTimeValueOfStartDate(startDate);
  const _endDate = getTimeValueOfStartDate(endDate);

  return _compareDate >= _startDate && _compareDate <= _endDate;
};

export const filterPayments = (
  payments: DataFormat[] = [],
  param: any = {},
  minDate?: string,
  maxDate?: string
) => {
  const {
    last4AccountNumber = '',
    bankRoutingNumber = '',
    confirmationNumber = '',
    paymentAmount,
    endDate,
    startDate
  } = param;

  if (isEmpty(param)) return payments;

  return filter(payments, item => {
    let isValid = true;
    if (last4AccountNumber) {
      isValid = item?.paymentAccountNumber == last4AccountNumber && isValid;
    }
    if (bankRoutingNumber) {
      isValid = item?.routingNumber == bankRoutingNumber && isValid;
    }
    if (confirmationNumber) {
      isValid = item?.confirmationNumber == confirmationNumber && isValid;
    }
    if (paymentAmount) isValid = item.amount == paymentAmount && isValid;

    if (startDate || endDate) {
      isValid =
        isDateBetween(
          item.schedulePaymentDate,
          startDate || minDate,
          endDate || maxDate
        ) && isValid;
    }

    return isValid;
  });
};
