import { createSelector } from '@reduxjs/toolkit';

// helpers
import isEmpty from 'lodash.isempty';
import uniq from 'lodash.uniq';
import { filterPayments, getFilterValue, sortByGrid } from '../helpers';

// types
import { DataViewMore, GridView } from '../types';
import { AttributeSearchValue, SortType } from 'app/_libraries/_dls/components';

// constant
import {
  getStatusDisabled,
  PAYMENT_LIST_GRID,
  SORT_PAYMENT
} from 'pages/Payment/PaymentList/constants';

const getAllChecked = (states: RootState, storeId: string) => {
  return states.paymentsList[storeId]?.allCheckedList || [];
};

const getPage = (states: RootState, storeId: string) => {
  return states.paymentsList[storeId]?.page;
};

const getPageSize = (states: RootState, storeId: string) => {
  return states.paymentsList[storeId]?.pageSize;
};

export const selectIsSearch = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentsList[storeId]?.isSearch || false,
  (isSearch: boolean) => isSearch
);

export const totalFilteredPaymentData = createSelector(
  (states: RootState, storeId: string) => {
    const paymentListState = states.paymentsList[storeId] || {};
    const {
      data = [],
      sortBy = SORT_PAYMENT,
      filterValue = [],
      minDate,
      maxDate
    } = paymentListState;

    const param = getFilterValue(filterValue);

    const filteredData = filterPayments(data, param, minDate, maxDate);

    return sortByGrid(sortBy, filteredData);
  },
  (data: GridView[]) => data
);

export const selectPaymentData = createSelector(
  [totalFilteredPaymentData, getPage, getPageSize],
  (
    filteredData,
    page = PAYMENT_LIST_GRID.DEFAULT_PAGE_NUMBER,
    size = PAYMENT_LIST_GRID.DEFAULT_PAGE_SIZE
  ) => {
    const start = size * (page - 1);
    const end = size * page;
    return [...filteredData.slice(start, end)];
  }
);

export const selectSize = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentsList[storeId]?.pageSize ||
    PAYMENT_LIST_GRID.DEFAULT_PAGE_SIZE,
  (pageSize: number) => pageSize
);

export const selectPage = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentsList[storeId]?.page || PAYMENT_LIST_GRID.DEFAULT_PAGE_NUMBER,
  (page: number) => page
);

export const selectSortBy = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentsList[storeId]?.sortBy || SORT_PAYMENT,
  (sortBy: SortType) => sortBy
);

export const selectCheckedList = createSelector(
  totalFilteredPaymentData,
  getAllChecked,
  (data, allChecked) => {
    const checkedList = data.filter(item => {
      return allChecked.indexOf(item?.paymentId) !== -1;
    });

    return checkedList.map(item => item.paymentId);
  }
);

export const selectTotalChecked = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentsList[storeId]?.allCheckedList?.length || 0,
  (totalChecked: number) => totalChecked
);

export const selectIsRefresh = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentsList[storeId]?.isRefresh || false,
  (isRefresh: boolean) => isRefresh
);

export const selectSearchValue = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentsList[storeId]?.filterValue,
  (value?: AttributeSearchValue[]) => value
);

export const selectLoadingPayment = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentsList[storeId]?.loading ||
    states.paymentsList[storeId]?.cancelPendingPayments?.loading,
  (loading?: boolean) => loading
);

export const selectError = createSelector(
  (states: RootState, storeId: string) => states.paymentsList[storeId]?.error,
  (error?: boolean) => error
);

export const selectNoPayment = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentsList[storeId]?.noPayment,
  (noPayment?: boolean) => noPayment
);

export const selectNoResult = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentsList[storeId]?.noResult,
  (noResult?: boolean) => noResult
);

export const selectClearAndReset = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentsList[storeId]?.clearAndReset,
  (clearAndReset?: boolean) => clearAndReset
);

export const selectAllConfirmNumberChecked = createSelector(
  (states: RootState, storeId: string) => {
    const ids = states.paymentsList[storeId]?.allCheckedList || [];
    const data = states.paymentsList[storeId]?.data || [];
    return uniq(
      ids.map(
        id => data.find(item => item.paymentId === id)?.confirmationNumber || ''
      )
    );
  },
  (confirmNumberChecked: string[]) => confirmNumberChecked
);

export const selectTotalPayment = createSelector(
  totalFilteredPaymentData,
  (data: MagicKeyValue[]) => data.length
);

export const selectAllCheckList = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentsList[storeId]?.allCheckedList || [],
  (allCheckedList: string[]) => allCheckedList
);

export const selectIsDisableCancelPayments = createSelector(
  selectAllCheckList,
  (selectFlatCheckList: string[]) => isEmpty(selectFlatCheckList)
);

export const selectorReadOnlyData = createSelector(
  totalFilteredPaymentData,
  (data: MagicKeyValue[]) => {
    const statusDisabled = getStatusDisabled();

    return data
      .filter(item => statusDisabled.indexOf(item?.paymentStatus) >= 0)
      .map(item => item.paymentId);
  }
);

// Payment view more
export const selectPaymentViewMore = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentsList[storeId]?.dataViewMore,
  (dataViewMore?: DataViewMore) => dataViewMore
);

export const selectPaymentViewMoreLoading = createSelector(
  (states: RootState, storeId: string) =>
    states.paymentsList[storeId]?.viewMoreLoading || false,
  (loading: boolean) => loading
);
