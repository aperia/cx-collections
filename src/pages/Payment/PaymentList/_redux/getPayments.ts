import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import isEmpty from 'lodash.isempty';
import sortBy from 'lodash.sortby';

// types
import {
  PaymentInitialState,
  PaymentArg,
  // PaymentPayload,
  RawData,
  DataFormat
} from '../types';

// Service
import { paymentListServices } from '../api-services';

// Helper
import { parseJSON, sortByGrid } from '../helpers';
import { convertAPIDateToView } from 'app/helpers';

// Const
import { EMPTY_STRING } from 'app/constants';
import { ID_SORT_GRID } from '../constants';

const mapType = (eftAccountType: string, paymentMedium: string) => {
  if (eftAccountType === 'Checking') {
    return 'txt_checking_account';
  }
  if (eftAccountType === 'Unknown' && paymentMedium === 'CheckCard') {
    return 'txt_debit_account';
  }
  if (eftAccountType === 'Savings') {
    return 'txt_savings_account';
  }
  return '';
};

const mapDataToView = (data: RawData[]) => {
  return data.map(item => {
    const {
      accountNumber,
      paymentPostDate,
      paymentAmount,
      payeeID,
      eftAccountType,
      paymentMedium,
      paymentID,
      transactionResultCode,
      fee,
      ...rest
    } = item;
    const objAcc = parseJSON(accountNumber);
    const objPay = parseJSON(payeeID);

    return {
      ...rest,
      paymentAmount,
      paymentPostDate,
      paymentMedium,
      eftAccountType,
      schedulePaymentDate: convertAPIDateToView(paymentPostDate),
      paymentId: paymentID,
      amount: paymentAmount,
      paymentAccountNumber: objAcc?.maskedValue,
      paymentStatus: transactionResultCode,

      paymentType: mapType(eftAccountType, paymentMedium),
      accountNumber: objAcc,
      fee,
      payeeID: objPay
    };
  });
};

export const getPayments = createAsyncThunk<any, PaymentArg, ThunkAPIConfig>(
  'payment/getPayments',
  async args => {
    const {
      postData: { accountId }
    } = args;
    const { commonConfig } = window.appConfig || {};

    const postData = {
      common: {
        app: commonConfig?.appCxCollections || EMPTY_STRING,
        accountId
      },
      keyType: 'Site',
      payeeID: accountId
    };

    const { data } = await paymentListServices.getPayments(postData);

    // move Fee to paymentCode == "Primary"
    const primaryPaymentList = (data?.payments || []) as RawData[];

    const payments = primaryPaymentList.map(item => {
      const { paymentCode, paymentAmount } = item;
      return paymentCode !== 'Primary' ? { ...item, fee: paymentAmount } : item;
    });

    // get max and min day from the list for filter Front End
    const sortedList = sortBy(payments, [
      item => new Date(item?.paymentPostDate)
    ]);

    const minDate = sortedList[0]?.paymentPostDate;
    const maxDate = sortedList[sortedList.length - 1]?.paymentPostDate;

    return {
      payments: sortByGrid(
        { id: ID_SORT_GRID.DATE, order: 'desc' },
        payments as any
      ),
      minDate,
      maxDate
    };
  }
);

export const getPaymentsBuilder = (
  builder: ActionReducerMapBuilder<PaymentInitialState>
) => {
  builder
    .addCase(getPayments.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: true
      };
    })
    .addCase(getPayments.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { payments = [], maxDate, minDate } = action.payload;
      const firstCall = draftState[storeId]?.isFirstCall;
      if (!firstCall) {
        draftState[storeId] = {
          ...draftState[storeId],
          isFirstCall: true,
          noPayment: isEmpty(payments),
          error: false
        };
      }
      if (firstCall) {
        draftState[storeId] = {
          ...draftState[storeId],
          noResult: isEmpty(payments),
          error: false
        };
      }

      draftState[storeId] = {
        ...draftState[storeId],
        loading: false,
        error: false,
        data: mapDataToView(payments) as DataFormat[],
        maxDate,
        minDate
      };
    })
    .addCase(getPayments.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: false,
        error: true,
        noPayment: false,
        noResult: false
      };
    });
};
