import { createStore, Store } from '@reduxjs/toolkit';

import { getPaymentInfo } from './getPaymentInfo';

import { paymentListServices } from '../api-services';

import { rootReducer } from 'storeConfig';

import { responseDefault, storeId } from 'app/test-utils';

let store: Store<RootState>;
let state: RootState;

beforeEach(() => {
  store = createStore(rootReducer, {});
  state = store.getState();
});

let spy1: jest.SpyInstance;

const mockArgs = {
  storeId,
  postData: {
    routingNumber: 'routingNumber'
  }
};

const mockPayload = {
  data: {
    financialPaymentInfo: {
      fileKey: 'string',
      institutionTypeCode: 'string',
      branchCode: 'string',
      routingNumber: 'string',
      institutionFullName: 'string',
      institutionNameAbbreviation: 'string',
      institutionCity: 'string',
      institutionState: 'string'
    }
  }
};

afterEach(() => {
  spy1?.mockReset();
  spy1?.mockRestore();
});

describe('should have test getPaymentInfo', () => {
  it('Should return data', async () => {
    spy1 = jest.spyOn(paymentListServices, 'getPaymentInfo').mockResolvedValue({
      ...responseDefault,
      data: {
        financialPaymentInfo: {
          fileKey: 'string',
          institutionTypeCode: 'string',
          branchCode: 'string',
          routingNumber: 'string',
          institutionFullName: 'string',
          institutionNameAbbreviation: 'string',
          institutionCity: 'string',
          institutionState: 'string'
        }
      }
    });

    const response = await getPaymentInfo(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toBeDefined();
  });

  it('should response getPaymentInfo pending', () => {
    const pendingAction = getPaymentInfo.pending(
      getPaymentInfo.pending.type,
      mockArgs
    );
    const actual = rootReducer(state, pendingAction);

    expect(actual?.paymentsList[storeId]?.viewMoreLoading).toEqual(true);
  });

  it('should response getPaymentInfo fulfilled', () => {
    const fulfilled = getPaymentInfo.fulfilled(
      mockPayload,
      getPaymentInfo.fulfilled.type,
      mockArgs
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual?.paymentsList[storeId]?.viewMoreLoading).toEqual(false);
    expect(actual?.paymentsList[storeId]?.dataViewMore).toEqual({
      fileKey: 'string',
      institutionTypeCode: 'string',
      branchCode: 'string',
      routingNumber: 'string',
      institutionFullName: 'string',
      institutionNameAbbreviation: 'string',
      institutionCity: 'string',
      institutionState: 'string'
    });
  });

  it('should response getPaymentInfo fulfilled with undefined', () => {
    const emptyData = {
      data: {
        financialPaymentInfo: null as unknown as any
      }
    };
    const fulfilled = getPaymentInfo.fulfilled(
      emptyData,
      getPaymentInfo.fulfilled.type,
      mockArgs
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual?.paymentsList[storeId]?.viewMoreLoading).toEqual(false);
    expect(actual?.paymentsList[storeId]?.dataViewMore).toEqual({});
  });

  it('should response getPaymentInfo rejected', () => {
    const rejected = getPaymentInfo.rejected(
      null,
      getPaymentInfo.rejected.type,
      mockArgs
    );
    const actual = rootReducer(state, rejected);

    expect(actual?.paymentsList[storeId]?.viewMoreLoading).toEqual(false);
  });
});
