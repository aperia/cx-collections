import { actions, reducer } from './reducers';
import { PaymentInitialState } from '../types';
import { storeId } from 'app/test-utils';

const dataGrid = [
  {
    amount: 9,
    confirmationNumber: '123',
    paymentAccountNumber: {},
    paymentId: '123',
    schedulePaymentDate: '2020, 01, 27',
    paymentStatus: { value: 'Settled' },
    paymentType: {}
  },
  {
    amount: 8,
    confirmationNumber: '123',
    paymentAccountNumber: {},
    paymentId: '123',
    schedulePaymentDate: '2020, 01, 26',
    paymentStatus: { value: 'Canceled' },
    paymentType: {}
  },
  {
    amount: 10,
    confirmationNumber: '123',
    paymentAccountNumber: {},
    paymentId: '123',
    schedulePaymentDate: '2020, 01, 28',
    paymentStatus: { value: 'Pending' },
    paymentType: {}
  },
  {
    amount: 7,
    confirmationNumber: '123',
    paymentAccountNumber: {},
    paymentId: '123',
    schedulePaymentDate: '2020, 01, 25',
    paymentStatus: {},
    paymentType: {}
  }
];

const initStateData: PaymentInitialState = {
  [storeId]: {
    cancelPendingPayments: { loading: true },
    loading: false,
    isFirstCall: true,
    noPayment: false,
    noResult: false,
    page: 1,
    dataGrid,
    data: dataGrid
  }
};

describe('Test reducer', () => {
  it('removeStore action', () => {
    const state = reducer(initStateData, actions.removeStore({ storeId }));

    expect(state).toEqual({});
  });

  describe('changePage action', () => {
    it('with data', () => {
      const page = 2;

      // case default pageSize
      const newData = { [storeId]: {} };
      newData[storeId] = { ...initStateData[storeId], pageSize: 10 };

      const state = reducer(newData, actions.changePage({ storeId, page }));

      expect(state[storeId].page).toEqual(page);

      const stateUpdated = reducer(
        initStateData,
        actions.changePage({ storeId, page })
      );

      expect(stateUpdated[storeId].page).toEqual(page);
    });

    it('without data', () => {
      const page = 2;

      // case default pageSize
      const newData = { [storeId]: {} };
      newData[storeId] = { ...initStateData[storeId], data: null };

      const state = reducer(newData, actions.changePage({ storeId, page }));

      expect(state[storeId].page).toEqual(page);

      const stateUpdated = reducer(
        initStateData,
        actions.changePage({ storeId, page })
      );

      expect(stateUpdated[storeId].page).toEqual(page);
    });
  });

  describe('changePageSize action', () => {
    it('with data', () => {
      const size = 50;

      const state = reducer(
        initStateData,
        actions.changePageSize({ storeId, size })
      );

      expect(state[storeId].pageSize).toEqual(size);
    });

    it('without data', () => {
      const size = 50;

      // case default pageSize
      const newData = { [storeId]: {} };
      newData[storeId] = { ...initStateData[storeId], data: null };

      const state = reducer(newData, actions.changePageSize({ storeId, size }));

      expect(state[storeId].pageSize).toEqual(size);
    });
  });

  it('updateSortBy action', () => {
    const sort = { id: 'amount' };

    const state = reducer(
      initStateData,
      actions.updateSortBy({ storeId, sort })
    );

    expect(state[storeId].sortBy).toEqual(sort);
  });

  it('updateCheckList action', () => {
    const dataKeyList = ['amount'];

    const state = reducer(
      initStateData,
      actions.updateCheckList({ storeId, dataKeyList })
    );

    expect(state[storeId].allCheckedList).toEqual(dataKeyList);
  });

  it('updateCheckList action with allCheckedList', () => {
    const stateData = { ...initStateData };
    stateData[storeId].allCheckedList = ['row_1'];

    const state = reducer(
      stateData,
      actions.updateCheckList({ storeId, dataKeyList: ['row_2'] })
    );

    expect(state[storeId].allCheckedList).toEqual(['row_1', 'row_2']);
  });

  it('updateSearch action', () => {
    const filterValue = [{ key: 'key', value: '1' }];

    const state = reducer(
      initStateData,
      actions.updateSearch({ storeId, filterValue })
    );

    expect(state[storeId].filterValue).toEqual(filterValue);
  });

  it('updateIsSearch action', () => {
    const isSearch = true;

    const state = reducer(
      initStateData,
      actions.updateIsSearch({ storeId, isSearch })
    );

    expect(state[storeId].isSearch).toEqual(isSearch);
  });

  it('updateIsSearch action', () => {
    const isSearch = true;

    const state = reducer(
      initStateData,
      actions.updateIsSearch({ storeId, isSearch })
    );

    expect(state[storeId].isSearch).toEqual(isSearch);
  });

  it('updateIsRefresh action', () => {
    const isRefresh = true;

    const state = reducer(
      initStateData,
      actions.updateIsRefresh({ storeId, isRefresh })
    );

    expect(state[storeId].isRefresh).toEqual(isRefresh);
  });

  it('clearAndReset action', () => {
    const clearAndReset = true;

    const state = reducer(initStateData, actions.clearAndReset({ storeId }));

    expect(state[storeId].clearAndReset).toEqual(clearAndReset);
  });
});
