import { responseDefault, storeId } from 'app/test-utils';
import {
  cancelPendingPayments,
  triggerCancelPendingPayments
} from './cancelPendingPayment';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';

import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

import { paymentListServices } from '../api-services';
import { isRejected } from '@reduxjs/toolkit';

const requestActionData = {
  storeId,
  postData: {
    accountId: storeId,
    paymentIds: ['123']
  }
};

let spy: jest.SpyInstance;
let state: RootState;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('PaymentList > cancelPendingPayments', () => {
  let store: Store<RootState>;

  const initialState: Partial<RootState> = {
    paymentsList: {
      [storeId]: {
        cancelPendingPayments: { loading: true },
        loading: false,
        isFirstCall: true,
        noPayment: false,
        noResult: false,
        page: 1,
        data: []
      }
    }
  };
  beforeEach(() => {
    store = createStore(rootReducer, initialState);
    state = store.getState();
  });

  it('error', async () => {
    spy = jest
      .spyOn(paymentListServices, 'cancelPendingPayments')
      .mockRejectedValue({
        ...responseDefault,
        data: { data: { message: 'failed' } }
      });

    const response = await cancelPendingPayments(requestActionData)(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig['extra']
    );

    expect(response.payload).toEqual({
      response: {
        config: {},
        data: { data: { message: 'failed' } },
        headers: {},
        status: 200,
        statusText: ''
      }
    });
  });
});

describe('PaymentList > triggerCancelPendingPayments', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {});
  });

  describe('success', () => {
    it('fulfilled', async () => {
      spy = jest
        .spyOn(paymentListServices, 'cancelPendingPayments')
        .mockResolvedValue({
          ...responseDefault,
          data: { data: { message: 'success' } }
        });

      const mockAddToast = jest
        .fn()
        .mockImplementation(() => ({ type: 'type' }));
      jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);

      await triggerCancelPendingPayments(requestActionData)(
        jest.fn().mockResolvedValue({
          meta: {
            arg: {
              storeId: storeId
            },
            requestStatus: 'fulfilled',
            requestId: 'mockRequestID'
          },
          type: 'makePayment/triggerCancelPendingPayments/fulfilled',
          payload: null
        }),
        store.getState,
        {}
      );

      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: 'txt_payment_cancel_success'
      });
    });

    it('rejected', async () => {
      spy = jest
        .spyOn(paymentListServices, 'cancelPendingPayments')
        .mockResolvedValue({
          ...responseDefault,
          data: { data: { message: 'success' } }
        });

      const mockAddToast = jest
        .fn()
        .mockImplementation(() => ({ type: 'type' }));
      jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);

      await triggerCancelPendingPayments(requestActionData)(
        jest.fn().mockResolvedValue({
          meta: {
            arg: {
              storeId: storeId
            },
            requestStatus: 'rejected',
            requestId: 'mockRequestID'
          },
          type: 'makePayment/triggerCancelPendingPayments/fulfilled',
          payload: null
        }),
        store.getState,
        undefined as unknown as ThunkAPIConfig['extra']
      );

      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'error',
        message: 'txt_payment_cancel_failure'
      });
    });
  });

  it('reject', async () => {
    spy = jest
      .spyOn(paymentListServices, 'cancelPendingPayments')
      .mockRejectedValue({});

    const response = await cancelPendingPayments(requestActionData)(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig['extra']
    );

    expect(isRejected(response)).toBeTruthy;
  });

  describe('cancelPendingPayment Builder', () => {
    it('should response cancelPendingPayment pending', () => {
      const pendingAction = cancelPendingPayments.pending(
        cancelPendingPayments.pending.type,
        { storeId, postData: { accountId: '', confirmationNumbers: [''] } }
      );
      const actual = rootReducer(state, pendingAction);
      expect(
        actual?.paymentsList[storeId].cancelPendingPayments?.loading
      ).toEqual(true);
    });

    it('should response cancelPendingPayments fulfilled', () => {
      const fulfilled = cancelPendingPayments.fulfilled(
        { data: [] },
        cancelPendingPayments.fulfilled.type,
        { storeId, postData: { accountId: '', confirmationNumbers: [''] } }
      );
      const actual = rootReducer(state, fulfilled);

      expect(actual?.paymentsList[storeId].isSearch).toEqual(true);
      expect(actual?.paymentsList[storeId].allCheckedList).toEqual([]);
    });

    it('should response cancelPendingPayments rejected', () => {
      const rejected = cancelPendingPayments.rejected(
        null,
        cancelPendingPayments.rejected.type,
        { storeId, postData: { accountId: '', confirmationNumbers: [''] } }
      );
      const actual = rootReducer(state, rejected);

      expect(
        actual?.paymentsList[storeId].cancelPendingPayments?.loading
      ).toEqual(false);
    });
  });
});
