import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// types
import {
  PaymentInitialState,
  PaymentViewMorePayload,
  PaymentViewMoreArg
} from '../types';

// Service
import { paymentListServices } from '../api-services';

export const getPaymentInfo = createAsyncThunk<
  PaymentViewMorePayload,
  PaymentViewMoreArg,
  ThunkAPIConfig
>('payment/getPaymentInfo', async (args, thunkAPI) => {
  const data = await paymentListServices.getPaymentInfo({
    routingNumber: args.postData?.routingNumber
  });

  return data;
});

export const getPaymentInfoBuilder = (
  builder: ActionReducerMapBuilder<PaymentInitialState>
) => {
  builder
    .addCase(getPaymentInfo.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        viewMoreLoading: true
      };
    })
    .addCase(getPaymentInfo.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        viewMoreLoading: false,
        dataViewMore: action.payload.data?.financialPaymentInfo || {}
      };
    })
    .addCase(getPaymentInfo.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        dataViewMore: {},
        viewMoreLoading: false
      };
    });
};
