import { useSelectorMock } from 'app/test-utils/useSelectorMock';
import * as makeSelector from './selectors';
import { PaymentInitialState } from '../types';
import { getStatusDisabled, SORT_PAYMENT } from '../constants';
import { storeId } from 'app/test-utils';

const expectDataGrid: MagicKeyValue[] = [
  {
    amount: undefined,
    bankRouting: 'bankRouting2',
    confirmationNumber: '2',
    fee: undefined,
    institutionCity: undefined,
    institutionNameAbbreviation: undefined,
    institutionState: undefined,
    paymentAccountNumber: undefined,
    paymentChannel: '',
    paymentId: 'paymentId',
    paymentStatus: 'Success',
    paymentTimestamp: '',
    paymentType: undefined,
    schedulePaymentDate: undefined
  },
  {
    amount: undefined,
    bankRouting: 'bankRouting3',
    confirmationNumber: '3',
    fee: undefined,
    institutionCity: undefined,
    institutionNameAbbreviation: undefined,
    institutionState: undefined,
    paymentAccountNumber: undefined,
    paymentChannel: '',
    paymentId: 'paymentId2',
    paymentStatus: 'Canceled',
    paymentTimestamp: '',
    paymentType: undefined,
    schedulePaymentDate: undefined
  }
];

const mockDataGrid: MagicKeyValue[] = [
  {
    paymentID: 'paymentId',
    amount: 1,
    // confirmationNumber: '2',
    paymentAccountNumber: {
      eValue: 'eValue',
      maskedValue: 'maskedValue'
    },
    paymentStatus: {
      description: 'description',
      value: 'Success'
    },
    schedulePaymentDate: 'null',
    paymentType: {
      code: 'code',
      description: 'description'
    }
  },
  {
    // paymentId: 'paymentId2',
    // amount: 2,
    // confirmationNumber: '3',
    paymentAccountNumber: {
      eValue: 'eValue',
      maskedValue: 'maskedValue'
    },
    paymentStatus: {
      description: 'description',
      value: 'Canceled'
    },
    // schedulePaymentDate: 'null',
    paymentType: {
      code: 'code',
      description: 'description'
    }
  }
];

const mockRawData = [
  {
    schedulePaymentDate: 'schedulePaymentDate1',
    amount: 'amount1',
    paymentAccountNumber: 'paymentAccountNumber1',
    paymentStatus: 'ABC',
    paymentType: 'paymentType1',
    bankRouting: 'bankRouting1',
    institutionNameAbbreviation: 'institutionNameAbbreviation1',
    institutionCity: 'institutionCity1',
    institutionState: 'institutionState1',
    fee: 'fee1',

    paymentId: 'paymentId',
    paymentAmount: '',
    paymentMedium: '',
    eftAccountType: 'Checking',
    paymentPostDate: '0000000',

    transactionID: '',
    appID: '',
    transactionResultCode: 'Success',
    paymentResultCode: '',
    paymentTimestamp: '',
    originalTransactionAmount: 0,
    netTransactionAmount: 0,
    customReference: '',
    payeeID: '',
    fullName: '',
    firstName: '',
    lastName: '',
    accountNumber: '',
    cardType: '',
    paymentChannel: '',
    lastPaymentStatus: '',
    userID: '',
    // confirmationNumber: '2',
    routingNumber: ''
  },
  {
    schedulePaymentDate: 'schedulePaymentDate2',
    amount: 'amount2',
    paymentAccountNumber: 'paymentAccountNumber2',
    paymentStatus: 'Success',
    paymentType: 'paymentType2',
    bankRouting: 'bankRouting2',
    institutionNameAbbreviation: 'institutionNameAbbreviation2',
    institutionCity: 'institutionCity2',
    institutionState: 'institutionState2',
    fee: 'fee2',

    paymentId: 'paymentId2',
    paymentAmount: '',
    paymentMedium: '',
    eftAccountType: 'Checking',
    paymentPostDate: '0000000',

    transactionID: '',
    appID: '',
    transactionResultCode: 'Canceled',
    paymentResultCode: '',
    paymentTimestamp: '',
    originalTransactionAmount: 0,
    netTransactionAmount: 0,
    customReference: '',
    payeeID: '',
    fullName: '',
    firstName: '',
    lastName: '',
    accountNumber: '',
    cardType: '',
    paymentChannel: '',
    lastPaymentStatus: '',
    userID: '',
    // confirmationNumber: '3',
    routingNumber: ''
  }
];

const mockData = {
  dataGrid: mockDataGrid,
  data: mockRawData,
  pageSize: 20,
  page: 1,
  sortBy: { id: 'amount' },
  checkedList: { page_1: ['paymentId'] },
  moreDetails: {
    paymentDetail: { source: 'source' },
    loading: true
  },
  error: true,
  isSearch: true,
  isRefresh: true,
  filterValue: [],
  loading: true,
  noPayment: true,
  noResult: true,
  clearAndReset: true,
  allCheckedList: ['paymentId'],
  viewMoreLoading: true,
  dataViewMore: { fee: '10' }
};

const undefinedData = {
  dataGrid: mockDataGrid,
  data: undefined,
  pageSize: 20,
  page: 1,
  sortBy: { id: 'amount' },
  checkedList: { page_1: ['paymentId'] },
  moreDetails: {
    paymentDetail: { source: 'source' },
    loading: true
  },
  error: true,
  isSearch: true,
  isRefresh: true,
  filterValue: [],
  loading: true,
  noPayment: true,
  noResult: true,
  clearAndReset: true,
  allCheckedList: undefined
};

const initData: PaymentInitialState = {
  [storeId]: mockData
};

const initUndefinedData: PaymentInitialState = {
  [storeId]: undefinedData
};

const initNoData: PaymentInitialState = {};

const generateSelectorMock = () => (action: Function) => {
  const data = useSelectorMock(action, { paymentsList: initData }, storeId);
  const noData = useSelectorMock(action, { paymentsList: initNoData }, storeId);
  const undefinedData = useSelectorMock(
    action,
    { paymentsList: initUndefinedData },
    storeId
  );

  const haveFilter = useSelectorMock(
    action,
    {
      paymentsList: {
        [storeId]: {
          ...mockData,
          filterValue: [
            {
              key: 'confirmationNumber',
              value: '3'
            }
          ]
        }
      }
    },
    storeId
  );

  return { data, noData, haveFilter, undefinedData };
};

describe('Test selector state', () => {
  it('select payment data', () => {
    const selectorMock = generateSelectorMock();
    const { noData } = selectorMock(makeSelector.selectPaymentData);

    expect(noData).toEqual([]);
  });

  it('select total payment', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectTotalPayment);

    expect(noData).toEqual(0);
    expect(data).toEqual(expectDataGrid.length);
  });

  it('select size', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectSize);

    expect(noData).toEqual(10);
    expect(data).toEqual(mockData.pageSize);
  });

  it('select page', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectPage);

    expect(noData).toEqual(1);
    expect(data).toEqual(mockData.page);
  });

  it('select SortBy', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectSortBy);

    expect(noData).toEqual(SORT_PAYMENT);
    expect(data).toEqual(mockData.sortBy);
  });

  it('selectCheckedList ', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectCheckedList);

    expect(noData).toEqual([]);
    expect(data).toEqual(mockData.allCheckedList);
  });

  it('select total checked', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectTotalChecked);

    expect(noData).toEqual(0);
    expect(data).toEqual(mockData.checkedList.page_1.length);
  });

  it('select isRefresh', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectIsRefresh);

    expect(noData).toEqual(false);
    expect(data).toEqual(mockData.isRefresh);
  });

  it('select select value', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectSearchValue);

    expect(noData).toEqual(undefined);
    expect(data).toEqual(mockData.filterValue);
  });

  it('select loading payment', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectLoadingPayment);

    expect(noData).toEqual(undefined);
    expect(data).toEqual(mockData.loading);
  });

  it('select error', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectError);

    expect(noData).toEqual(undefined);
    expect(data).toEqual(mockData.error);
  });

  it('select no payment', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectNoPayment);

    expect(noData).toEqual(undefined);
    expect(data).toEqual(mockData.noPayment);
  });

  it('select no result', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectNoResult);

    expect(noData).toEqual(undefined);
    expect(data).toEqual(mockData.noResult);
  });

  it('select clear and reset', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectClearAndReset);

    expect(noData).toEqual(undefined);
    expect(data).toEqual(mockData.clearAndReset);
  });

  it('selectIsDisableCancelPayments', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(
      makeSelector.selectIsDisableCancelPayments
    );

    expect(noData).toEqual(true);
    expect(data).toEqual(false);
  });

  it('selectorReadOnlyData', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectorReadOnlyData);

    const statusDisabled = getStatusDisabled();
    expect(noData).toEqual([]);
    expect(data).toEqual(
      mockRawData
        .filter(item => statusDisabled.indexOf(item?.paymentStatus) >= 0)
        .map(item => item.paymentId)
    );
  });

  it('selectIsSearch', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData } = selectorMock(makeSelector.selectIsSearch);

    expect(noData).toEqual(false);
    expect(data).toEqual(true);
  });

  it('selectAllConfirmNumberChecked', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData, undefinedData } = selectorMock(
      makeSelector.selectAllConfirmNumberChecked
    );

    expect(noData).toEqual([]);
    expect(undefinedData).toEqual([]);
    expect(data).toEqual(['']);
  });

  it('selectPaymentViewMore', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData, undefinedData } = selectorMock(
      makeSelector.selectPaymentViewMore
    );

    expect(noData).toEqual(undefined);
    expect(undefinedData).toEqual(undefined);
    expect(data).toEqual({ fee: '10' });
  });

  it('selectPaymentViewMoreLoading', () => {
    const selectorMock = generateSelectorMock();
    const { data, noData, undefinedData } = selectorMock(
      makeSelector.selectPaymentViewMoreLoading
    );

    expect(noData).toEqual(false);
    expect(undefinedData).toEqual(false);
    expect(data).toEqual(true);
  });
});
