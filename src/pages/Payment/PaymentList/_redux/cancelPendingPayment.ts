import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// Redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { actions } from './reducers';

// types
import {
  PaymentInitialState,
  CancelPendingPaymentsArgs,
  CancelPendingPaymentsPayload
} from '../types';

// Service
import { paymentListServices } from '../api-services';

// Const
import { EMPTY_STRING } from 'app/constants';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export const cancelPendingPayments = createAsyncThunk<
  CancelPendingPaymentsPayload,
  CancelPendingPaymentsArgs,
  ThunkAPIConfig
>('makePayment/cancelPendingPayments', async (args, thunkApi) => {
  const {
    postData: { accountId, confirmationNumbers }
  } = args;

  const { commonConfig } = window.appConfig || {};
  try {
    return await paymentListServices.cancelPendingPayments({
      common: {
        app: commonConfig?.appCxCollections || EMPTY_STRING,
        accountId
      },
      confirmationNumbers
    });
  } catch (error) {
    return thunkApi.rejectWithValue({ response: error });
  }
});

export const triggerCancelPendingPayments = createAsyncThunk<
  void,
  CancelPendingPaymentsArgs,
  ThunkAPIConfig
>('makePayment/triggerCancelPendingPayments', async (args, { dispatch }) => {
  const response = await dispatch(cancelPendingPayments(args));
  const { storeId } = args;

  // delete success
  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'txt_payment_cancel_success'
        })
      );
      dispatch(
        actions.getPayments({
          storeId: args.storeId,
          postData: {
            accountId: args.postData.accountId
          }
        })
      );

      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inSnapshot'
        })
      );
    });
  }

  // delete error
  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: 'txt_payment_cancel_failure'
        })
      );

      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inSnapshot',
          apiResponse: response.payload?.response
        })
      );
    });
  }
});

export const cancelPendingPaymentsBuilder = (
  builder: ActionReducerMapBuilder<PaymentInitialState>
) => {
  builder
    .addCase(cancelPendingPayments.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        cancelPendingPayments: {
          loading: true
        }
      };
    })
    .addCase(cancelPendingPayments.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].cancelPendingPayments!.loading = false;
      draftState[storeId].allCheckedList = [];
      draftState[storeId].isSearch = true;
    })
    .addCase(cancelPendingPayments.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].cancelPendingPayments!.loading = false;
    });
};
