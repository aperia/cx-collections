import { RawData } from './../types';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';

import { getPayments } from './getPayments';
import { PaymentArg } from '../types';
import { responseDefault, storeId } from 'app/test-utils';

import { paymentListServices } from '../api-services';

const requestData: PaymentArg = {
  storeId,
  postData: { accountId: 'accountId' }
};

let spy: jest.SpyInstance;
let state: RootState;
let store: Store<RootState>;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

beforeEach(() => {
  store = createStore(rootReducer, {});
  state = store.getState();
});

describe('redux-store > getPayments', () => {
  it('reject', async () => {
    spy = jest.spyOn(paymentListServices, 'getPayments').mockRejectedValue({});

    const response = await getPayments(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toBeUndefined();
  });

  it('Should return with empty payments', async () => {
    spy = jest.spyOn(paymentListServices, 'getPayments').mockResolvedValue({
      ...responseDefault,
      data: {
        payments: undefined as unknown as RawData[]
      }
    });

    const response = await getPayments(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toBeDefined();
  });

  it('Should return with payments', async () => {
    spy = jest.spyOn(paymentListServices, 'getPayments').mockResolvedValue({
      ...responseDefault,
      data: {
        payments: [
          {
            paymentID: '1',
            paymentAmount: 15,
            paymentMedium: '1',
            eftAccountType: 'Checking',
            paymentPostDate: '',
            paymentCode: 'Primary'
          },
          {
            paymentID: '2',
            paymentAmount: 10,
            paymentMedium: 'CheckCard',
            eftAccountType: 'Unknown',
            paymentPostDate: '2',
            paymentCode: 'Primary'
          },
          {
            paymentID: '2',
            paymentAmount: 10,
            paymentMedium: '2',
            eftAccountType: 'Savings',
            paymentPostDate: '2',
            paymentCode: 'Secondary'
          },
          {
            paymentID: '2',
            paymentAmount: 10,
            paymentMedium: 'CheckCard',
            eftAccountType: 'Unknown',
            paymentPostDate: '2',
            paymentCode: 'Secondary'
          },
          {
            paymentID: '2',
            paymentAmount: 10,
            paymentMedium: '',
            eftAccountType: 'Unknown',
            paymentPostDate: '2',
            paymentCode: 'Secondary'
          }
        ]
      }
    });

    const response = await getPayments(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toBeDefined();
  });

  it('Should return with payments has eftAccountType = Checking', async () => {
    spy = jest.spyOn(paymentListServices, 'getPayments').mockResolvedValue({
      ...responseDefault,
      data: {
        payments: [
          {
            paymentID: '2',
            paymentAmount: 10,
            paymentMedium: '2',
            eftAccountType: 'Savings',
            paymentPostDate: '2',
            paymentCode: 'Secondary'
          }
        ]
      }
    });

    const response = await getPayments(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toBeDefined();
  });

  it('Should return with payments has eftAccountType = Savings', async () => {
    spy = jest.spyOn(paymentListServices, 'getPayments').mockResolvedValue({
      ...responseDefault,
      data: {
        payments: [
          {
            paymentID: '1',
            paymentAmount: 15,
            paymentMedium: '1',
            eftAccountType: 'Checking',
            paymentPostDate: '',
            paymentCode: 'Primary'
          }
        ]
      }
    });

    const response = await getPayments(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toBeDefined();
  });
});

describe('getPayments Builder', () => {
  it('should response getPayments pending', () => {
    const pendingAction = getPayments.pending(getPayments.pending.type, {
      storeId,
      postData: { accountId: 'accountId' }
    });
    const actual = rootReducer(state, pendingAction);
    expect(actual?.paymentsList[storeId]?.loading).toEqual(true);
  });

  it('should response getPayments fulfilled', () => {
    const fulfilled = getPayments.fulfilled(
      { payments: [] },
      getPayments.fulfilled.type,
      {
        storeId,
        postData: { accountId: 'accountId' }
      }
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual?.paymentsList[storeId].isFirstCall).toEqual(true);
  });

  it('should response getPayments fulfilled with isFirst call true', () => {
    const fulfilled = getPayments.fulfilled(
      {
        payments: undefined as unknown as RawData[]
      },
      getPayments.fulfilled.type,
      {
        storeId,
        postData: { accountId: 'accountId' }
      }
    );
    const actual = rootReducer(
      {
        ...state,
        paymentsList: { [storeId]: { ...[storeId], isFirstCall: true } }
      },
      fulfilled
    );

    expect(actual?.paymentsList[storeId].isFirstCall).toEqual(true);
  });

  it('should response getPayments rejected', () => {
    const rejected = getPayments.rejected(null, getPayments.rejected.type, {
      storeId,
      postData: { accountId: 'accountId' }
    });
    const actual = rootReducer(state, rejected);

    expect(actual?.paymentsList[storeId]?.loading).toEqual(false);
  });
});
