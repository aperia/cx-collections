import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { PAGE, SORT_PAYMENT } from '../constants';
import {
  ChangePage,
  ChangePageSize,
  PaymentInitialState,
  UpdateCheckList,
  UpdateIsRefresh,
  UpdateIsSearch,
  UpdateSearch,
  UpdateSortBy
} from '../types';
import {
  cancelPendingPaymentsBuilder,
  cancelPendingPayments,
  triggerCancelPendingPayments
} from './cancelPendingPayment';
import { getPayments, getPaymentsBuilder } from './getPayments';
import { getPaymentInfo, getPaymentInfoBuilder } from './getPaymentInfo';
import { PAGE_SIZE_COMMON } from 'app/constants';

const { actions: _actions, reducer } = createSlice({
  name: 'paymentList',
  initialState: {} as PaymentInitialState,
  reducers: {
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    changePage: (draftState, action: PayloadAction<ChangePage>) => {
      const { storeId, page } = action.payload;
      const size = draftState[storeId]?.pageSize || 10;
      const data = draftState[storeId]?.data || [];
      const start = size * (page - 1);
      const end = size * page;
      const dataGrid = data.slice(start, end);
      draftState[storeId] = {
        ...draftState[storeId],
        page,
        dataGrid,
        isRefresh: true
      };
    },
    changePageSize: (draftState, action: PayloadAction<ChangePageSize>) => {
      const { storeId, size } = action.payload;
      const data = draftState[storeId]?.data || [];
      const dataGrid = data.slice(0, size);
      draftState[storeId] = {
        ...draftState[storeId],
        pageSize: size,
        page: PAGE,
        dataGrid,
        isRefresh: true
      };
    },
    updateSortBy: (draftState, action: PayloadAction<UpdateSortBy>) => {
      const { storeId, sort } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        sortBy: sort,
        isRefresh: true
      };
    },
    updateCheckList: (draftState, action: PayloadAction<UpdateCheckList>) => {
      const { storeId, dataKeyList } = action.payload;
      const allChecked = draftState[storeId]?.allCheckedList || [];
      const idDataGrid = draftState[storeId]?.data!.map(item => item.paymentId);
      const checked = allChecked.filter(
        item => idDataGrid.indexOf(item) === -1
      );
      draftState[storeId] = {
        ...draftState[storeId],
        allCheckedList: [...checked, ...dataKeyList]
      };
    },
    updateSearch: (draftState, action: PayloadAction<UpdateSearch>) => {
      const { storeId, filterValue } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        filterValue,
        isSearch: true,
        page: PAGE,
        pageSize: PAGE_SIZE_COMMON[0],
        sortBy: SORT_PAYMENT,
        allCheckedList: []
      };
    },
    updateIsSearch: (draftState, action: PayloadAction<UpdateIsSearch>) => {
      const { storeId, isSearch } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isSearch
      };
    },
    updateIsRefresh: (draftState, action: PayloadAction<UpdateIsRefresh>) => {
      const { storeId, isRefresh } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isRefresh,
        clearAndReset: isRefresh
      };
    },
    clearAndReset: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        filterValue: [],
        isSearch: true,
        page: PAGE,
        pageSize: PAGE_SIZE_COMMON[0],
        sortBy: SORT_PAYMENT,
        clearAndReset: true,
        allCheckedList: []
      };
    }
  },
  extraReducers: builder => {
    getPaymentsBuilder(builder);
    cancelPendingPaymentsBuilder(builder);
    getPaymentInfoBuilder(builder);
  }
});

const actions = {
  ..._actions,
  getPayments,
  getPaymentInfo,
  cancelPendingPayments,
  triggerCancelPendingPayments
};

export { actions, reducer };
