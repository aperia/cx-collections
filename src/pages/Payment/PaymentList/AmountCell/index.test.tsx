import React from 'react';
import { render, screen } from '@testing-library/react';
import AmountCell from '.';

describe('Render', () => {
  it('Render UI', () => {
    render(<AmountCell paymentData={{ amount: '100' }} />);

    expect(screen.getByText('$100.00')).toBeInTheDocument();
  });

  it('Render UI => empty data', () => {
    const { baseElement } = render(<AmountCell paymentData={{}} />);

    expect(baseElement.querySelector('span')).toBeInTheDocument();
  });
});
