import { formatAPICurrency } from 'app/helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';
import React from 'react';

// types
import { PaymentFormatData } from '../types';

export interface Props {
  paymentData: PaymentFormatData;
  dataTestId?: string;
}

const AmountCell: React.FC<Props> = ({ paymentData, dataTestId }) => {
  const { amount } = paymentData;
  return (
    <span data-testid={genAmtId(dataTestId, 'amount', '')}>
      {formatAPICurrency(amount ? amount + '' : '')}
    </span>
  );
};

export default AmountCell;
