import React from 'react';
import { AccountDetailProvider } from 'app/hooks';
import { screen } from '@testing-library/react';

// utils
import { renderMockStore, storeId } from 'app/test-utils';

// components
import PaymentList from 'pages/Payment/PaymentList';

// redux store
import { actions } from './_redux/reducers';

// constants
import { I18N_PAYMENT_TEXT } from '../constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

jest.mock('./GridPayment', () => () => <div>GridPayment</div>);
HTMLCanvasElement.prototype.getContext = jest.fn();

const dataGrid: MagicKeyValue[] = [
  {
    confirmationNumber: '1',
    schedulePaymentDate: '2020, 01, 27',
    amount: 1,
    paymentAccountNumber: {
      maskedValue: 'maskedValue'
    },
    paymentStatus: {
      value: 'Settled',
      description: 'Settled'
    },
    paymentType: {
      description: 'description'
    }
  },
  {
    confirmationNumber: '2',
    schedulePaymentDate: '2020, 01, 28',
    amount: 2,
    paymentAccountNumber: {
      maskedValue: 'maskedValue'
    },
    paymentStatus: {
      value: 'Canceled',
      description: 'Canceled'
    },
    paymentType: {
      description: 'description'
    }
  },
  {
    confirmationNumber: '3',
    schedulePaymentDate: '2020, 01, 29',
    amount: 3,
    paymentAccountNumber: {
      maskedValue: 'maskedValue'
    },
    paymentStatus: {
      value: 'Pending',
      description: 'Pending'
    },
    paymentType: {
      description: 'description'
    }
  }
];

const stateError: Partial<RootState> = {
  paymentsList: {
    [storeId]: {
      error: true,
      noPayment: true,
      noResult: true,
      clearAndReset: false,
      page: 2,
      isSearch: true,
      loading: false,
      filterValue: [{ key: 'amount', value: '1' }],
      dataGrid,
      data: dataGrid
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <PaymentList />
    </AccountDetailProvider>,
    { initialState }
  );
};

const renderWrapperWithoutStoreId = (initialState: Partial<RootState>) => {
  return renderMockStore(<PaymentList />, { initialState });
};

let spy: jest.SpyInstance;

afterEach(() => {
  if (spy) {
    spy.mockReset();
    spy.mockRestore();
  }
});

describe('Render', () => {
  it('Render UI', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy = jest.spyOn(actions, 'getPayments').mockImplementation(mockAction);

    renderWrapper({
      paymentsList: {
        [storeId]: {
          error: false,
          noPayment: false,
          noResult: false,
          page: 1,
          loading: false,
          dataGrid,
          data: dataGrid
        }
      }
    });

    expect(
      screen.getByText(I18N_PAYMENT_TEXT.PAYMENT_LIST)
    ).toBeInTheDocument();
    expect(mockAction).toBeCalledWith({
      storeId,
      postData: { accountId: storeId }
    });
  });

  it('Render UI with error', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy = jest.spyOn(actions, 'getPayments').mockImplementation(mockAction);

    renderWrapper(stateError);

    expect(
      screen.getByText(I18N_PAYMENT_TEXT.PAYMENT_LIST)
    ).toBeInTheDocument();
    expect(mockAction).toBeCalledWith({
      storeId,
      postData: { accountId: storeId }
    });
  });

  it('Render UI without storeId', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy = jest.spyOn(actions, 'getPayments').mockImplementation(mockAction);

    renderWrapperWithoutStoreId(stateError);

    expect(
      screen.getByText(I18N_PAYMENT_TEXT.PAYMENT_LIST)
    ).toBeInTheDocument();
    expect(mockAction).not.toBeCalled();
  });

  it('Render UI with no payment', () => {
    const initialState: Partial<RootState> = {
      paymentsList: {
        [storeId]: {
          error: false,
          noPayment: true,
          noResult: false,
          loading: false,
          dataGrid
        }
      }
    };

    renderWrapper(initialState);

    expect(screen.getByText('txt_no_payments_to_display')).toBeInTheDocument();
  });

  it('Render UI with no result', () => {
    const initialState: Partial<RootState> = {
      paymentsList: {
        [storeId]: {
          error: false,
          noPayment: false,
          noResult: true,
          loading: false,
          dataGrid
        }
      }
    };
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy = jest.spyOn(actions, 'getPayments').mockImplementation(mockAction);

    renderWrapper(initialState);
    expect(screen.getByText('txt_memos_no_results_found')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleClearAndReset', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy = jest.spyOn(actions, 'clearAndReset').mockImplementation(mockAction);

    renderWrapper({
      paymentsList: {
        [storeId]: {
          error: false,
          noPayment: false,
          noResult: true,
          clearAndReset: false,
          page: 2,
          isSearch: true,
          loading: false,
          dataGrid
        }
      }
    });

    screen.getByText(I18N_COMMON_TEXT.CLEAR_AND_RESET).click();

    expect(mockAction).toBeCalledWith({ storeId });
  });

  it('handleReload', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy = jest.spyOn(actions, 'getPayments').mockImplementation(mockAction);

    renderWrapper(stateError);

    screen.getByText(I18N_COMMON_TEXT.RELOAD).click();
    expect(mockAction).toBeCalledWith({
      storeId,
      postData: { accountId: storeId }
    });
  });
});
