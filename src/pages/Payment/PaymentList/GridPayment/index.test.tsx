import React from 'react';

import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import GridPayment from 'pages/Payment/PaymentList/GridPayment';

import { fireEvent, screen } from '@testing-library/react';

// redux store
import { actions as paymentsListAction } from '../_redux/reducers';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import {
  GridProps,
  PaginationWrapperProps
} from 'app/_libraries/_dls/components';
import * as entitlementsHelpers from 'app/entitlements/helpers';

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');
  const { Grid } = dlsComponents;

  return {
    ...dlsComponents,
    Grid: (props: GridProps) => (
      <>
        <input
          data-testid="Grid_handleOnCheck"
          onChange={(e: any) => props.onCheck!(e.target.value)}
        />
        <input
          data-testid="Grid_handleSortBy"
          onChange={(e: any) => props.onSortChange!(e.target.value)}
        />
        <Grid {...props} />
      </>
    ),
    Pagination: (props: PaginationWrapperProps) => (
      <>
        <input
          data-testid="Pagination_handleChangePage"
          onChange={(e: any) => props.onChangePage!(e.target.value)}
        />
        <input
          data-testid="Pagination_handleChangePageSize"
          onChange={(e: any) => props.onChangePageSize!(e.target.value)}
        />
      </>
    )
  };
});

const newAllCheckedList = ['row_1', 'row_2'];

const dataGrid: any = [
  {
    confirmationNumber: '1',
    schedulePaymentDate: '2020, 01, 27',
    amount: 1,
    paymentAccountNumber: {
      maskedValue: 'maskedValue'
    },
    paymentStatus: {
      value: 'Settled',
      description: 'Settled'
    },
    paymentType: {
      description: 'description'
    }
  },
  {
    confirmationNumber: '2',
    schedulePaymentDate: '2020, 01, 28',
    amount: 2,
    paymentAccountNumber: {
      maskedValue: 'maskedValue'
    },
    paymentStatus: {
      value: 'Canceled',
      description: 'Canceled'
    },
    paymentType: {
      description: 'description'
    }
  },
  {
    confirmationNumber: '3',
    schedulePaymentDate: '2020, 01, 29',
    amount: 3,
    paymentAccountNumber: {
      maskedValue: 'maskedValue'
    },
    paymentStatus: {
      value: 'Pending',
      description: 'Pending'
    },
    paymentType: {
      description: 'description'
    }
  }
];

const pageData: Partial<RootState> = {
  paymentsList: {
    [storeId]: {
      dataGrid,
      allCheckedList: ['row_1'],
      clearAndReset: false,
      page: 2,
      pageSize: 1,
      filterValue: [{ key: 'amount', value: '1' }],
      data: [
        {
          paymentID: 'example',
          paymentAmount: 1,
          paymentId: 'paymentId',
          paymentMedium: 'medium',
          paymentPostDate: 'date example',
          eftAccountType: 'aaa'
        },
        {
          paymentID: 'example2',
          paymentAmount: 1,
          paymentId: 'paymentId2',
          paymentMedium: 'medium',
          paymentPostDate: 'date example',
          eftAccountType: 'aaa'
        },
        {
          paymentID: 'example3',
          paymentAmount: 1,
          paymentId: 'paymentId3',
          paymentMedium: 'medium',
          paymentPostDate: 'date example',
          eftAccountType: 'aaa'
        }
      ]
    }
  }
};

const renderWrapper = (state: Partial<RootState>) => {
  return renderMockStoreId(<GridPayment />, { initialState: state });
};

const initialState: Partial<RootState> = {
  paymentsList: {
    [storeId]: {
      dataGrid,
      allCheckedList: ['row_1'],
      clearAndReset: false,
      page: 2,
      pageSize: 10,
      filterValue: [{ key: 'amount', value: '1' }],
      data: []
    }
  }
};

let spy: jest.SpyInstance;
let spy2: jest.SpyInstance;

afterEach(() => {
  if (spy) {
    spy.mockReset();
    spy.mockRestore();
  }
  if (spy2) {
    spy2.mockReset();
    spy2.mockRestore();
  }
});

const mockAction = mockActionCreator(paymentsListAction);

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper(initialState);

    expect(screen.getByText('txt_selected_payments')).toBeInTheDocument();
  });
});

describe('Test GridPayment functions', () => {
  beforeEach(() => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
  });
  it('handleClearAndReset', () => {
    const mockClearAndReset = mockAction('clearAndReset');

    renderWrapper(initialState);

    screen.getByText('txt_clear_and_reset').click();

    expect(mockClearAndReset).toBeCalledWith({ storeId });
  });

  it('handleCancelPayment', () => {
    const mockTriggerCancelPendingPayments = mockAction(
      'triggerCancelPendingPayments'
    );
    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });

    spy = jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);

    renderWrapper(initialState);

    screen.getByText('txt_cancel_payments').click();

    expect(openConfirmModalAction).toBeCalledWith(
      expect.objectContaining({
        title: 'txt_confirm_cancel_payment_title',
        body: 'txt_confirm_cancel_payment_body',
        confirmText: 'txt_confirm_cancel_payment_button'
      })
    );

    expect(mockTriggerCancelPendingPayments).toBeCalledWith({
      storeId,
      postData: {
        accountId: 'accEValue',
        confirmationNumbers: ['']
      }
    });
  });

  it('handleOnCheck', () => {
    const mockUpdateCheckList = mockAction('updateCheckList');

    renderWrapper(initialState);

    fireEvent.change(screen.getByTestId('Grid_handleOnCheck'), {
      target: { value: [newAllCheckedList[1]] }
    });

    screen.getByText('txt_cancel_payments').click();

    expect(mockUpdateCheckList).toBeCalledWith({
      storeId,
      dataKeyList: newAllCheckedList[1]
    });
  });

  it('handleChangePage', () => {
    const mockChangePage = mockAction('changePage');

    renderWrapper(pageData);

    fireEvent.change(screen.getByTestId('Pagination_handleChangePage'), {
      target: { value: 1 }
    });

    expect(mockChangePage).toBeCalledWith({ storeId, page: '1' });
  });

  it('handleChangePageSize', () => {
    const mockChangePage = mockAction('changePageSize');

    renderWrapper(pageData);

    fireEvent.change(screen.getByTestId('Pagination_handleChangePageSize'), {
      target: { value: { size: 10 } }
    });

    expect(mockChangePage).toBeCalledWith(expect.objectContaining({ storeId }));
  });

  it('handleSortBy', () => {
    const mockUpdateSortBy = mockAction('updateSortBy');

    const mockChangePage = mockAction('changePage');

    renderWrapper(initialState);

    fireEvent.change(screen.getByTestId('Grid_handleSortBy'), {
      target: { value: 'desc' }
    });

    expect(mockUpdateSortBy).toBeCalledWith({
      sort: 'desc',
      storeId
    });

    expect(mockChangePage).toBeCalledWith({
      page: 1,
      storeId
    });
  });
});
