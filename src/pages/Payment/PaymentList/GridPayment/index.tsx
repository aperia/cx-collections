import React, { useCallback, useMemo } from 'react';

// components
import {
  Button,
  Grid,
  pageSizeInfo,
  Pagination,
  SortType,
  AttributeSearchValue
} from 'app/_libraries/_dls/components';

// redux store
import { batch, useDispatch } from 'react-redux';
import { actions } from '../_redux/reducers';

// helpers
import isEmpty from 'lodash.isempty';

// constant
import { ID_SORT_GRID, PAYMENT_LIST_GRID } from '../constants';

// selector
import {
  selectPaymentData,
  selectTotalPayment,
  selectPage,
  selectSize,
  selectSortBy,
  selectCheckedList,
  selectTotalChecked,
  selectClearAndReset,
  selectSearchValue,
  selectAllCheckList,
  selectIsDisableCancelPayments,
  selectorReadOnlyData,
  selectAllConfirmNumberChecked
} from '../_redux/selectors';

// hooks
import { useStoreIdSelector, useAccountDetail } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// type
import { PaymentFormatData } from '../types';

// redux store
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { PAGE_SIZE_COMMON } from 'app/constants';
import StatusGrid from '../StatusGrid';
import AmountCell from '../AmountCell';
import AccountNumberGrid from '../AccountNumberGrid';
import RenderViewInfo from '../ViewInformation';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

const GridPayment: React.FC = () => {
  const dispatch = useDispatch();
  const { storeId, accEValue: accountId } = useAccountDetail();

  const gridData = useStoreIdSelector<PaymentFormatData[]>(selectPaymentData);
  const totalPayment = useStoreIdSelector<number>(selectTotalPayment);
  const page = useStoreIdSelector<number>(selectPage);
  const size = useStoreIdSelector<number>(selectSize);
  const sortBy = useStoreIdSelector<SortType>(selectSortBy);
  const checkList = useStoreIdSelector<string[]>(selectCheckedList);
  const totalChecked = useStoreIdSelector<number>(selectTotalChecked);
  const searchValue =
    useStoreIdSelector<AttributeSearchValue[]>(selectSearchValue);
  const clearAndReset = useStoreIdSelector<boolean>(selectClearAndReset);
  const allCheckList = useStoreIdSelector<string[]>(selectAllCheckList);
  const confirmationNumbers = useStoreIdSelector<string[]>(
    selectAllConfirmNumberChecked
  );
  const isDisableCancelPayments = useStoreIdSelector<boolean>(
    selectIsDisableCancelPayments
  );
  const readOnlyCheckbox = useStoreIdSelector<string[]>(selectorReadOnlyData);
  const testId = 'account-detail-payment-tab_payment-list_grid';

  const { t } = useTranslation();

  // clear and reset
  const handleClearAndReset = () => {
    dispatch(actions.clearAndReset({ storeId }));
  };

  // pagination
  const handleChangePage = (page: number) => {
    dispatch(actions.changePage({ storeId, page }));
  };
  const handleChangePageSize = (pageInfo: pageSizeInfo) => {
    dispatch(actions.changePageSize({ storeId, size: pageInfo.size }));
  };

  // sort by Grid
  const handleSortBy = useCallback(
    (sort: SortType) => {
      batch(() => {
        dispatch(actions.updateSortBy({ storeId, sort }));
        dispatch(
          actions.changePage({
            storeId,
            page: PAYMENT_LIST_GRID.DEFAULT_PAGE_NUMBER
          })
        );
      });
    },

    [dispatch, storeId]
  );

  const handleOnCheck = (dataKeyList: string[]) => {
    dispatch(actions.updateCheckList({ storeId, dataKeyList }));
  };

  const handleConfirmCancelPayment = () => {
    dispatch(
      actions.triggerCancelPendingPayments({
        storeId,
        postData: { accountId, confirmationNumbers }
      })
    );
  };

  const handleCancelPayment = () => {
    dispatch(
      confirmModalActions.open({
        title: 'txt_confirm_cancel_payment_title',
        body: 'txt_confirm_cancel_payment_body',
        confirmCallback: handleConfirmCancelPayment,
        confirmText: 'txt_confirm_cancel_payment_button',
        count: allCheckList.length
      })
    );
  };

  const sortByView = useMemo(() => [sortBy], [sortBy]);
  const isDisplayPagination =
    totalPayment > PAYMENT_LIST_GRID.DEFAULT_PAGE_SIZE || totalPayment > size;

  const COLUMNS = useMemo(
    () => [
      {
        id: ID_SORT_GRID.DATE,
        Header: t('txt_scheduled_date'),
        accessor: ID_SORT_GRID.DATE,
        isSort: true,
        width: 142
      },
      {
        id: ID_SORT_GRID.STATUS,
        Header: t('txt_status'),
        isSort: true,
        accessor: (paymentData: PaymentFormatData, index: number) => (
          <StatusGrid paymentData={paymentData} dataTestId={`${index}`} />
        ),
        width: 120
      },
      {
        id: ID_SORT_GRID.AMOUNT,
        Header: t('txt_amount'),
        accessor: (paymentData: PaymentFormatData, index: number) => (
          <AmountCell paymentData={paymentData} dataTestId={`${index}`} />
        ),
        isSort: true,
        width: 142,
        cellProps: { className: 'text-right' }
      },
      {
        id: ID_SORT_GRID.ACCOUNT,
        Header: t('txt_account_number'),
        accessor: (paymentData: PaymentFormatData, index: number) => (
          <AccountNumberGrid
            paymentData={paymentData}
            dataTestId={`${index}`}
          />
        ),
        width: 152
      },
      {
        id: ID_SORT_GRID.CONFIRM,
        Header: t('txt_confirmation_number'),
        accessor: 'confirmationNumber',
        autoWidth: true,
        width: 184
      },
      {
        id: ID_SORT_GRID.TYPE,
        Header: t('txt_type'),
        isSort: true,
        accessor: (paymentData: PaymentFormatData) =>
          t(paymentData?.paymentType),
        width: 177
      },
      {
        id: 'view',
        Header: t('txt_action'),
        accessor: (paymentData: PaymentFormatData, index: number) => (
          <RenderViewInfo paymentData={paymentData} dataTestId={`${index}`} />
        ),
        cellProps: { className: 'text-center' },
        cellBodyProps: { className: 'td-sm ' },
        width: 87
      }
    ],
    [t]
  );

  return (
    <div>
      <div className="d-flex align-items-center justify-content-between pb-16 mr-n8">
        <p className="fs-14">
          <span
            className="color-grey fw-500"
            data-testid={genAmtId(testId, 'total-label', '')}
          >
            {t('txt_selected_payments')}{' '}
          </span>
          <span data-testid={genAmtId(testId, 'total-value', '')}>
            {totalChecked}
          </span>
        </p>
        <div>
          {checkPermission(
            PERMISSIONS.PAYMENT_LIST_CANCEL_PENDING_PAYMENT,
            storeId
          ) && (
            <Button
              size="sm"
              variant="outline-danger"
              onClick={handleCancelPayment}
              disabled={isDisableCancelPayments}
              dataTestId={`${testId}_cancel-payments-btn`}
            >
              {t('txt_cancel_payments')}
            </Button>
          )}
          {!isEmpty(searchValue) && !clearAndReset && (
            <Button
              className="ml-16"
              size="sm"
              variant="outline-primary"
              onClick={handleClearAndReset}
              dataTestId={`${testId}_clear-and-reset-btn`}
            >
              {t('txt_clear_and_reset', { count: allCheckList.length })}
            </Button>
          )}
        </div>
      </div>
      <Grid
        columns={COLUMNS}
        data={gridData}
        variant={{
          type: 'checkbox',
          id: `${storeId}-grid-payments`,
          isFixedLeft: true,
          width: 58
        }}
        dataItemKey="paymentId"
        sortBy={sortByView}
        checkedList={checkList}
        readOnlyCheckbox={readOnlyCheckbox}
        onSortChange={handleSortBy}
        onCheck={handleOnCheck}
        dataTestId={`${testId}_grid`}
      />
      {isDisplayPagination && (
        <div className="pt-16">
          <Pagination
            totalItem={totalPayment}
            pageSize={PAGE_SIZE_COMMON}
            pageNumber={page}
            pageSizeValue={size}
            compact
            onChangePage={handleChangePage}
            onChangePageSize={handleChangePageSize}
            dataTestId={`${testId}_grid-pagination`}
          />
        </div>
      )}
    </div>
  );
};

export default GridPayment;
