import { apiService } from 'app/utils/api.service';
import {
  CancelPendingPaymentRequest,
  CancelPendingPaymentsPayload,
  PaymentPost,
  PaymentViewMorePost
} from './types';

class PaymentListServices {
  cancelPendingPayments(data: CancelPendingPaymentRequest) {
    const url = window.appConfig?.api?.payment?.cancelPendingPayments || '';
    return apiService.post<CancelPendingPaymentsPayload>(url, data);
  }

  getPayments(postData: PaymentPost) {
    const url = window.appConfig?.api?.payment?.getPayments || '';

    return apiService.post(url, { ...postData });
  }

  getPaymentInfo(postData: PaymentViewMorePost) {
    const url = window.appConfig?.api?.payment?.getPaymentViewMore || '';

    return apiService.post(url, { ...postData });
  }
}

export const paymentListServices = new PaymentListServices();
