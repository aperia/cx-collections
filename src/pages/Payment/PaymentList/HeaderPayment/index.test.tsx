import React from 'react';
import { fireEvent, screen } from '@testing-library/react';

// Component
import HeaderPayment from '.';

// Helper
import { renderMockStore, storeId } from 'app/test-utils';

// Hooks
import { AccountDetailProvider } from 'app/hooks';

// Types
import {
  AttributeSearchEvent,
  AttributeSearchProps
} from 'app/_libraries/_dls/components';
import { mockActionCreator } from 'app/test-utils';

// redux store
import { actions } from '../_redux/reducers';
import { I18N_PAYMENT_TEXT } from 'pages/Payment/constants';

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');

  return {
    ...dlsComponents,
    AttributeSearch: (props: AttributeSearchProps) => (
      <>
        <div data-testid="AttributeSearch_value">
          {JSON.stringify(props.value)}
        </div>
        <button
          data-testid="AttributeSearch_onClear"
          onClick={() => props.onClear!()}
        />
        <button
          data-testid="AttributeSearch_onValidator"
          onClick={() =>
            props.validator!({
              value: {}
            } as AttributeSearchEvent)
          }
        />
        <input
          data-testid="AttributeSearch_onSearch"
          onChange={(e: any) => props.onSearch!(e.target.event, e.target.error)}
        />
        <input
          data-testid="AttributeSearch_onChange"
          onChange={(e: any) => props.onChange!(e.target.event)}
        />
      </>
    )
  };
});

const initialState: Partial<RootState> = {
  paymentsList: {
    [storeId]: {
      clearAndReset: true,
      filterValue: [{ key: 'id', value: 'value' }]
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <HeaderPayment />
    </AccountDetailProvider>,
    {
      initialState
    }
  );
};

const paymentSpy = mockActionCreator(actions);

describe('Render', () => {
  it('Render UI', () => {
    // mock
    const mockAction = paymentSpy('updateIsRefresh');

    // render
    renderWrapper(initialState);

    // expect
    expect(mockAction).toBeCalledWith({ storeId, isRefresh: false });
    expect(
      screen.getByText(I18N_PAYMENT_TEXT.PAYMENT_LIST)
    ).toBeInTheDocument();
  });

  it('Render UI => empty state', () => {
    // render
    renderWrapper({});

    // expect
    expect(
      screen.getByText(I18N_PAYMENT_TEXT.PAYMENT_LIST)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleOnClear', () => {
    // render
    renderWrapper(initialState);

    screen.getByTestId('AttributeSearch_onValidator').click();
    screen.getByTestId('AttributeSearch_onClear').click();

    const expected = JSON.parse(
      screen.getByTestId('AttributeSearch_value').textContent!
    );
    expect(expected).toEqual([]);
  });

  describe('handleSearch', () => {
    it('without error', () => {
      // mock
      const mockAction = paymentSpy('updateSearch');

      // render
      renderWrapper(initialState);

      fireEvent.change(screen.getByTestId('AttributeSearch_onSearch'), {
        target: { value: 'undefined', event: { value: 'value' } }
      });

      expect(mockAction).toBeCalledWith({ storeId, filterValue: 'value' });
    });

    it('with error', () => {
      // mock
      const mockAction = paymentSpy('updateSearch');

      // render
      renderWrapper(initialState);

      fireEvent.change(screen.getByTestId('AttributeSearch_onSearch'), {
        target: {
          value: 'undefined',
          event: { value: 'value' },
          error: { message: 'error' }
        }
      });

      expect(mockAction).not.toBeCalled();
    });
  });

  it('handleChange', () => {
    // render
    renderWrapper(initialState);

    fireEvent.change(screen.getByTestId('AttributeSearch_onChange'), {
      target: {
        value: 'undefined',
        event: { value: 'value' }
      }
    });
    const expected = JSON.parse(
      screen.getByTestId('AttributeSearch_value').textContent!
    );
    expect(expected).toEqual('value');
  });
});
