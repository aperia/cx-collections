import React, { useEffect, useState } from 'react';

// components
import {
  AttributeSearch,
  AttributeSearchEvent,
  AttributeSearchValue
} from 'app/_libraries/_dls/components';
import { getAttributeSearchData } from '../attributeSearch';

// hooks
import { useStoreIdSelector, useAccountDetail } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import cloneDeep from 'lodash.clonedeep';
import isEmpty from 'lodash.isempty';
import { attrValidations } from '../helpers';

// redux store
import { useDispatch } from 'react-redux';
import { actions } from '../_redux/reducers';

// selector
import {
  selectSearchValue,
  selectIsRefresh,
  selectNoPayment,
  selectClearAndReset
} from '../_redux/selectors';
import { I18N_PAYMENT_TEXT } from '../../constants';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface HeaderPaymentProps {}

const HeaderPayment: React.FC<HeaderPaymentProps> = () => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const [filterValue, setFilterValue] = useState<AttributeSearchValue[]>([]);

  const searchValue =
    useStoreIdSelector<AttributeSearchValue[]>(selectSearchValue);
  const isRefresh = useStoreIdSelector<boolean>(selectIsRefresh);
  const noPayment = useStoreIdSelector<boolean>(selectNoPayment);
  const clearAndReset = useStoreIdSelector<boolean>(selectClearAndReset);
  const testId = 'account-detail-payment-tab_payment-list_header';

  const handleChange = (event: AttributeSearchEvent) => {
    const { value } = event;
    setFilterValue(value);
  };

  const { t } = useTranslation();

  const handleSearch = (
    event: AttributeSearchEvent,
    error?: Record<string, string>
  ) => {
    const { value } = event;

    if (isEmpty(value) || error) return;

    dispatch(actions.updateSearch({ storeId, filterValue: value }));
  };

  const handleOnClear = () => {
    setFilterValue([]);
  };

  useEffect(() => {
    if ((!isEmpty(searchValue) && isRefresh) || clearAndReset) {
      setFilterValue(searchValue);
      dispatch(actions.updateIsRefresh({ storeId, isRefresh: false }));
    }
  }, [clearAndReset, dispatch, isRefresh, searchValue, storeId]);

  return (
    <div className="d-flex align-items-center flex-wrap">
      <h5 className="mb-16 mr-24" data-testid={genAmtId(testId, 'title', '')}>
        {t(I18N_PAYMENT_TEXT.PAYMENT_LIST)}
      </h5>
      {!noPayment && (
        <div className="ml-auto mb-16">
          <AttributeSearch
            id={`payment__attributeSearch`}
            dataTestId={`${testId}_paymentSearch`}
            data={cloneDeep(getAttributeSearchData(t))}
            value={filterValue}
            orderBy="asc"
            small
            onChange={handleChange}
            onSearch={handleSearch}
            onClear={handleOnClear}
            validator={(value: Record<string, any>) =>
              attrValidations(value, t)
            }
            clearTooltipProps={{
              placement: 'top',
              containerClassName: 'tooltip-clear',
              element: t('txt_clear_all_criteria')
            }}
            placeholder={t('txt_type_to_add_parameter')}
            noFilterResultText={t('txt_no_other_parameters_available')}
          />
        </div>
      )}
    </div>
  );
};

export default HeaderPayment;
