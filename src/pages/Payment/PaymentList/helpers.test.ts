import { sortByGrid } from './helpers';
import { ID_SORT_GRID } from './constants';
import { DataFormat } from './types';

import {
  parseJSON,
  getFilterValue,
  addDays,
  getTimeValueOfStartDate,
  isDateBetween,
  filterPayments,
  validateAmount,
  validateAccountNumber,
  validateBankNumberPayment,
  validateConfirmPayment,
  attrValidations,
  validateStartDate,
  validateEndDate
} from 'pages/Payment/PaymentList/helpers';
import {
  MESSAGE,
  ATTRIBUTE_SEARCH_PAYMENT
} from 'pages/Payment/PaymentList/constants';

const translateFn = (text: string) => text;

describe(`PaymentList Helpers`, () => {
  it('validateAmount', () => {
    // error required
    const require = validateAmount({ paymentAmount: {} }, translateFn);
    expect(require![ATTRIBUTE_SEARCH_PAYMENT.AMOUNT]).toEqual(
      MESSAGE.AMOUNT_REQUIRED
    );

    // no error
    const valid = validateAmount(
      { paymentAmount: { value: '123' } },
      translateFn
    );
    expect(valid).toBeUndefined();
  });

  it('validateStartDate', () => {
    // error invalid
    const invalid = validateStartDate(
      {
        startDate: { value: '2020, 02, 02' },
        endDate: { value: '2020, 01, 01' }
      },
      translateFn
    );
    expect(invalid![ATTRIBUTE_SEARCH_PAYMENT.START_DATE]).toEqual(
      MESSAGE.START_DATE_INVALID
    );

    // error require
    const require = validateStartDate({ startDate: {} }, translateFn);
    expect(require![ATTRIBUTE_SEARCH_PAYMENT.START_DATE]).toEqual(
      MESSAGE.START_DATE_REQUIRED
    );

    // no error
    const valid = validateAccountNumber(
      {
        startDate: { value: '2020, 01, 01' }
      },
      translateFn
    );
    expect(valid).toBeUndefined();
  });

  it('validateEndDate', () => {
    // error invalid
    const invalid = validateEndDate(
      {
        startDate: { value: '2020, 02, 02' },
        endDate: { value: '2020, 01, 01' }
      },
      translateFn
    );
    expect(invalid![ATTRIBUTE_SEARCH_PAYMENT.START_DATE]).toEqual(
      MESSAGE.END_DATE_INVALID
    );

    // error require
    const require = validateEndDate({ endDate: {} }, translateFn);
    expect(require![ATTRIBUTE_SEARCH_PAYMENT.END_DATE]).toEqual(
      MESSAGE.END_DATE_REQUIRED
    );

    // no error
    const valid = validateAccountNumber(
      {
        endDate: { value: '2020, 01, 01' }
      },
      translateFn
    );
    expect(valid).toBeUndefined();
  });

  it('validateAccountNumber', () => {
    // error required
    const require = validateAccountNumber(
      {
        last4AccountNumber: {
          value: undefined
        }
      },
      translateFn
    );
    expect(require?.[ATTRIBUTE_SEARCH_PAYMENT.ACCOUNT_NUMBER]).toEqual(
      MESSAGE.ACCOUNT_NUMBER_REQUIRED
    );

    // error invalid format
    const invalidFormat = validateAccountNumber(
      {
        last4AccountNumber: { value: '012' }
      },
      translateFn
    );
    expect(invalidFormat?.[ATTRIBUTE_SEARCH_PAYMENT.ACCOUNT_NUMBER]).toEqual(
      MESSAGE.INVALID_FORMAT
    );

    // no error
    const valid = validateAccountNumber(
      {
        last4AccountNumber: { value: '01234567891234' }
      },
      translateFn
    );
    expect(valid).toBeUndefined();
  });

  it('validateBankNumberPayment', () => {
    // error required
    const require = validateBankNumberPayment(
      { bankRoutingNumber: {} },
      translateFn
    );
    expect(require![ATTRIBUTE_SEARCH_PAYMENT.BANK]).toEqual(
      MESSAGE.BANK_REQUIRED
    );

    // error invalid format
    const invalidFormat = validateBankNumberPayment(
      {
        bankRoutingNumber: { value: '1' }
      },
      translateFn
    );
    expect(invalidFormat![ATTRIBUTE_SEARCH_PAYMENT.BANK]).toEqual(
      MESSAGE.INVALID_FORMAT
    );

    // no error
    const valid = validateBankNumberPayment(
      {
        bankRoutingNumber: { value: '123456789' }
      },
      translateFn
    );
    expect(valid).toBeUndefined();
  });

  it('validateConfirmPayment', () => {
    // error invalid format
    const invalid = validateConfirmPayment(
      {
        confirmationNumber: { value: '123456789' }
      },
      translateFn
    );
    expect(invalid![ATTRIBUTE_SEARCH_PAYMENT.CONFIRMATION]).toEqual(
      MESSAGE.INVALID_FORMAT
    );

    // error required
    const require = validateConfirmPayment(
      { confirmationNumber: {} },
      translateFn
    );
    expect(require![ATTRIBUTE_SEARCH_PAYMENT.CONFIRMATION]).toEqual(
      MESSAGE.CONFIRM_REQUIRED
    );

    // no error
    const valid = validateConfirmPayment(
      {
        confirmationNumber: { value: '12345678901234' }
      },
      translateFn
    );
    expect(valid).toBeUndefined();
  });

  it('attrValidations with error', () => {
    const validate = attrValidations(
      {
        last4AccountNumber: {
          value: undefined
        }
      },
      translateFn
    );
    expect(validate).toEqual({
      last4AccountNumber: 'txt_account_number_is_required'
    });
  });

  it('attrValidations no error', () => {
    const valid = attrValidations(
      { paymentAmount: { value: '123' } },
      translateFn
    );
    expect(valid).toBeUndefined();
  });
});

const requiredFields: DataFormat = {
  eftAccountType: '',
  paymentID: '',
  paymentMedium: '',
  paymentId: '',
  paymentAmount: 0,
  paymentPostDate: ''
};

describe('Test sortByGrid function', () => {
  it('sort data grid for sortType incorrect', () => {
    const data = sortByGrid({ id: '111' }, [requiredFields]);
    expect(data).toEqual([requiredFields]);
  });

  it('sort data gird for amount', () => {
    const dataSort: DataFormat[] = [
      { ...requiredFields, paymentAmount: 1 },
      { ...requiredFields, paymentAmount: 2 }
    ];

    const resultDesc = [
      { ...requiredFields, paymentAmount: 2 },
      { ...requiredFields, paymentAmount: 1 }
    ];
    const data = sortByGrid({ id: ID_SORT_GRID.AMOUNT }, dataSort);
    const data1 = sortByGrid(
      { id: ID_SORT_GRID.AMOUNT, order: 'desc' },
      dataSort
    );
    expect(data).toEqual(data);
    expect(data1).toEqual(resultDesc);

    const data2 = sortByGrid(
      { id: ID_SORT_GRID.AMOUNT, order: 'asc' },
      dataSort
    );
    const resultAsc = [
      { ...requiredFields, paymentAmount: 1 },
      { ...requiredFields, paymentAmount: 2 }
    ];
    expect(data2).toEqual(resultAsc);
  });

  it('sort data gird for date', () => {
    const dataSort: DataFormat[] = [
      { ...requiredFields, paymentPostDate: '12/12/2020' },
      { ...requiredFields, paymentPostDate: '' },
      { ...requiredFields, paymentPostDate: '11/11/2020' }
    ];

    const resultDesc = [
      { ...requiredFields, paymentPostDate: '12/12/2020' },
      { ...requiredFields, paymentPostDate: '' },
      { ...requiredFields, paymentPostDate: '11/11/2020' }
    ];
    const resultAsc = [
      { ...requiredFields, paymentPostDate: '12/12/2020' },
      { ...requiredFields, paymentPostDate: '' },
      { ...requiredFields, paymentPostDate: '11/11/2020' }
    ];

    const data = sortByGrid({ id: ID_SORT_GRID.DATE }, dataSort);
    const dataDesc = sortByGrid(
      { id: ID_SORT_GRID.DATE, order: 'desc' },
      dataSort
    );
    const dataAsc = sortByGrid(
      { id: ID_SORT_GRID.DATE, order: 'asc' },
      dataSort
    );

    expect(data).toEqual(data);
    expect(dataDesc).toEqual(resultDesc);
    expect(dataAsc).toEqual(resultAsc);
  });

  it('sort data gird for type', () => {
    const dataSort: DataFormat[] = [
      { ...requiredFields, paymentType: 'type1' },
      { ...requiredFields, paymentType: 'type2' },
      { ...requiredFields, paymentType: undefined },
      { ...requiredFields, paymentType: undefined }
    ];
    const resultAsc = [
      { ...requiredFields, paymentType: undefined },
      { ...requiredFields, paymentType: undefined },
      { ...requiredFields, paymentType: 'type1' },
      { ...requiredFields, paymentType: 'type2' }
    ];

    const resultDesc = [
      { ...requiredFields, paymentType: 'type2' },
      { ...requiredFields, paymentType: 'type1' },
      { ...requiredFields, paymentType: undefined },
      { ...requiredFields, paymentType: undefined }
    ];

    const data = sortByGrid({ id: ID_SORT_GRID.TYPE }, dataSort);
    const dataDesc = sortByGrid(
      { id: ID_SORT_GRID.TYPE, order: 'desc' },
      dataSort
    );
    const dataAsc = sortByGrid(
      { id: ID_SORT_GRID.TYPE, order: 'asc' },
      dataSort
    );

    expect(data).toEqual(data);
    expect(dataDesc).toEqual(resultDesc);
    expect(dataAsc).toEqual(resultAsc);
  });

  it('sort data gird for status', () => {
    const dataSort: DataFormat[] = [
      { ...requiredFields, paymentStatus: 'status1' },
      { ...requiredFields, paymentStatus: 'status2' },
      { ...requiredFields, paymentStatus: undefined },
      { ...requiredFields, paymentStatus: undefined }
    ];

    const resultAsc = [
      { ...requiredFields, paymentStatus: 'status2' },
      { ...requiredFields, paymentStatus: 'status1' },
      { ...requiredFields, paymentStatus: undefined },
      { ...requiredFields, paymentStatus: undefined }
    ];
    const resultDesc = [
      { ...requiredFields, paymentStatus: undefined },
      { ...requiredFields, paymentStatus: undefined },
      { ...requiredFields, paymentStatus: 'status1' },
      { ...requiredFields, paymentStatus: 'status2' }
    ];

    const data = sortByGrid({ id: ID_SORT_GRID.STATUS }, dataSort);
    const dataAsc = sortByGrid(
      { id: ID_SORT_GRID.STATUS, order: 'asc' },
      dataSort
    );
    const dataDesc = sortByGrid(
      { id: ID_SORT_GRID.STATUS, order: 'desc' },
      dataSort
    );

    expect(data).toEqual(data);
    expect(dataDesc).toEqual(resultAsc);
    expect(dataAsc).toEqual(resultDesc);
  });

  it('sort data gird for other', () => {
    const dataSort: DataFormat[] = [
      { ...requiredFields, paymentStatus: 'status1' },
      { ...requiredFields, paymentStatus: 'status2' },
      { ...requiredFields, paymentStatus: undefined },
      { ...requiredFields, paymentStatus: undefined }
    ];

    const data = sortByGrid({ id: 'other', order: 'desc' }, dataSort);

    expect(data).toEqual(data);
  });
});

describe('parseJSON', () => {
  it('correct format json', () => {
    const data = { id: 1 };
    const result = parseJSON(JSON.stringify(data));

    expect(result).toEqual(data);
  });

  it('empty input', () => {
    const result = parseJSON();

    expect(result).toEqual({});
  });

  it('incorrect format json', () => {
    const result = parseJSON('{');

    expect(result).toEqual({});
  });
});

describe('getFilterValue', () => {
  it('empty input', () => {
    const result = getFilterValue();

    expect(result).toEqual({});
  });
  it('correct input', () => {
    const result = getFilterValue([{ key: 'key1', value: 'value1' }]);
    expect(result).toEqual({ key1: 'value1' });
  });
});

it('addDays', () => {
  const date = new Date('11/08/1995');
  const result = addDays(date, 1);

  expect(result).toEqual(new Date('11/09/1995'));
});

describe('getTimeValueOfStartDate ', () => {
  it('empty value', () => {
    const date = '';
    const result = getTimeValueOfStartDate(date);

    expect(result).toEqual(0);
  });

  it('valid value', () => {
    const date = '11/09/1995';
    const result = getTimeValueOfStartDate(date);

    const dateTime = new Date(date);
    const startOfDay = new Date(
      dateTime.getFullYear(),
      dateTime.getMonth(),
      dateTime.getDate()
    );

    expect(result).toEqual(startOfDay.valueOf());
  });

  it('valid value & add date', () => {
    const date = '11/09/1995';
    const result = getTimeValueOfStartDate(date, 1);

    const dateTime = new Date(date);
    const startOfDay = new Date(
      dateTime.getFullYear(),
      dateTime.getMonth(),
      dateTime.getDate() + 1
    );

    expect(result).toEqual(startOfDay.valueOf());
  });
});

describe('isDateBetween', () => {
  it('empty input', () => {
    const result = isDateBetween();

    expect(result).toBeFalsy();
  });

  it('is not between start - end date', () => {
    const compareDate = '11/09/1995';
    const result = isDateBetween(compareDate);

    expect(result).toBeFalsy();
  });

  it('is between start - end date', () => {
    const compareDate = '11/09/1995';
    const startDate = '10/9/1995';
    const endDate = '12/9/1995';
    const result = isDateBetween(compareDate, startDate, endDate);

    expect(result).toBeTruthy();
  });
});

describe('filterPayments', () => {
  it('empty input', () => {
    const payments = undefined;
    const result = filterPayments(payments);

    expect(result).toEqual([]);
  });

  it('valid input', () => {
    const payments = [
      {
        ...requiredFields,
        paymentAccountNumber: '1',
        routingNumber: '2',
        confirmationNumber: '3',
        amount: 4,
        schedulePaymentDate: '11/09/1995'
      }
    ];
    const param = {
      last4AccountNumber: '1',
      bankRoutingNumber: '2',
      confirmationNumber: '3',
      paymentAmount: 4,
      endDate: '11/20/1995',
      startDate: '11/01/1995'
    };
    const result1 = filterPayments(payments, param);
    const result2 = filterPayments(
      payments,
      { ...param, startDate: undefined },
      param.startDate
    );
    const result3 = filterPayments(
      payments,
      { ...param, endDate: undefined },
      param.startDate,
      param.endDate
    );
    expect(result1).toBeTruthy();
    expect(result2).toBeTruthy();
    expect(result3).toBeTruthy();
  });

  it('valid input with empty params', () => {
    const payments = [
      {
        ...requiredFields,
        paymentAccountNumber: '1',
        routingNumber: '2',
        confirmationNumber: '3',
        amount: 4,
        schedulePaymentDate: '11/09/1995'
      }
    ];
    const param = { id: 'id' };
    const result1 = filterPayments(payments, param);
    expect(result1).toBeTruthy();
  });
});
