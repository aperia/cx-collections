import React, {
  useEffect,
  useCallback,
  useMemo,
  useRef,
  useLayoutEffect
} from 'react';

// components
import GridPayment from './GridPayment';
import HeaderPayment from './HeaderPayment';
import FailedApiReload from 'app/components/FailedApiReload';
import ClearAndReset from 'pages/__commons/ClearAndReset';

// helpers
import classNames from 'classnames';

// redux store
import { actions } from 'pages/Payment/PaymentList/_redux/reducers';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// selector
import {
  selectLoadingPayment as selectLoading,
  selectNoResult,
  selectNoPayment,
  selectError,
  selectPaymentData
} from 'pages/Payment/PaymentList/_redux/selectors';

// types
import { PaymentFormatData } from './types';
import { Icon } from 'app/_libraries/_dls/components';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface PaymentListProps {}

export interface ItemSearch {
  key: string;
  value: string;
}

const PaymentList: React.FC<PaymentListProps> = () => {
  const dispatch = useDispatch();
  const { storeId, accEValue: accountId } = useAccountDetail();
  const { t } = useTranslation();
  const divRef = useRef<HTMLDivElement | null>(null);

  const loading = useStoreIdSelector<boolean>(selectLoading);
  const noResult = useStoreIdSelector<boolean>(selectNoResult);
  const noPayment = useStoreIdSelector<boolean>(selectNoPayment);
  const isError = useStoreIdSelector<boolean>(selectError);
  const gridData = useStoreIdSelector<PaymentFormatData[]>(selectPaymentData);
  const isNoDataDisplay = gridData && gridData.length === 0;
  const testId = 'account-detail-payment-tab_payment-list';

  const handleClearAndReset = useCallback(() => {
    dispatch(actions.clearAndReset({ storeId }));
  }, [dispatch, storeId]);

  const handleReload = useCallback(() => {
    dispatch(actions.getPayments({ storeId, postData: { accountId } }));
  }, [dispatch, accountId, storeId]);

  useEffect(() => {
    if (!storeId) return;

    handleReload();
  }, [handleReload, storeId]);

  const renderGrid = useMemo(() => {
    if (isError)
      return (
        <FailedApiReload
          id="account-detail-overview-fail"
          className="mt-80"
          onReload={handleReload}
          dataTestId={`${testId}_api-reload-failed`}
        />
      );

    if (noPayment)
      return (
        <div
          className="text-center my-80"
          data-testid={genAmtId(testId, 'no-payments', '')}
        >
          <Icon
            name="file"
            className="fs-80 color-light-l12"
            dataTestId={testId}
          />
          <p
            className="mt-20 text-secondary"
            data-testid={genAmtId(testId, 'no-payments-to-display', '')}
          >
            {t('txt_no_payments_to_display')}
          </p>
        </div>
      );

    if (!noPayment && (noResult || isNoDataDisplay))
      return (
        <ClearAndReset
          className="my-80"
          onClearAndReset={handleClearAndReset}
          testId={testId}
        />
      );

    return <GridPayment />;
  }, [
    handleClearAndReset,
    handleReload,
    isError,
    isNoDataDisplay,
    noPayment,
    noResult,
    t
  ]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.minHeight = '250px');
  }, []);

  return (
    <div className={classNames({ loading }, 'pt-24')} ref={divRef}>
      <HeaderPayment />
      {renderGrid}
    </div>
  );
};

export default PaymentList;
