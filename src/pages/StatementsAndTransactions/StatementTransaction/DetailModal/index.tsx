import React, { useMemo } from 'react';

// components
import { View } from 'app/_libraries/_dof/core';
import {
  Modal,
  ModalHeader,
  ModalTitle,
  ModalBody,
  ModalFooter
} from 'app/_libraries/_dls/components';

// constants
import { INFO, CONTENT } from '../constants';

// redux store
import { useDispatch } from 'react-redux';
import { statementTransactionAction as action } from '../__redux/reducers';
import { selectDetailData, selectDetailIsOpen } from '../__redux/selectors';

// hooks
import isEmpty from 'lodash.isempty';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

const DetailModal: React.FC = () => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const data = useStoreIdSelector(selectDetailData);
  const isOpenModal = useStoreIdSelector<boolean>(selectDetailIsOpen);

  const handleClose = () => dispatch(action.toggleDetailModal({ storeId }));

  const infoView = useMemo(() => {
    if (isEmpty(data)) return null;
    return (
      <View
        id={`${storeId}_${INFO}`}
        formKey={`${storeId}_${INFO}_view`}
        descriptor={INFO}
        value={data}
      />
    );
  }, [data, storeId]);

  const contentView = useMemo(() => {
    if (isEmpty(data)) return null;
    return (
      <View
        id={`${storeId}_${CONTENT}`}
        formKey={`${storeId}_${CONTENT}_view`}
        descriptor={CONTENT}
        value={data}
      />
    );
  }, [data, storeId]);

  return (
    <Modal
      lg
      show={isOpenModal}
      className="window-lg-full"
      dataTestId="transactionDetails-modal"
    >
      <ModalHeader
        dataTestId="transactionDetails-header"
        border
        closeButton
        onHide={handleClose}
      >
        <ModalTitle dataTestId="transactionDetails-header-title">
          {t('txt_transaction_details')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody className="p-0">
        <div className="px-24 pb-24 pt-8 border-bottom bg-light-l20">
          {infoView}
        </div>
        <div className="px-24 pt-24">{contentView}</div>
      </ModalBody>
      <ModalFooter
        dataTestId="transactionDetails-footer"
        cancelButtonText={t('txt_close')}
        onCancel={handleClose}
      />
    </Modal>
  );
};

export default DetailModal;
