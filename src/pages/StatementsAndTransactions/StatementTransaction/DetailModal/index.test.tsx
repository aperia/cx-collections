import React from 'react';
// components
import DetailModal from './index';

import { AccountDetailProvider } from 'app/hooks';
import { queryAllByClass, renderMockStoreId, storeId } from 'app/test-utils';
import { fireEvent, screen } from '@testing-library/react';

// constants
import { INFO, CONTENT } from '../constants';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const renderComponentWithMockProvider = (initialState: object) => {
  return renderMockStoreId(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <DetailModal />
    </AccountDetailProvider>,
    { initialState },
    true
  );
};

describe('Test GridViewHistory component', () => {
  it('Render UI', () => {
    const initialState: object = {
      statementTransactionList: {
        [storeId]: {
          detail: {
            isOpen: true,
            loading: false,
            data: {
              agentNumber: '0000',
              aggregateTransactionAmount: '00000000000012.89',
              authorizationNumber: '',
              authorizationSourceCode: '',
              automatedTellerMachineAtmFlag: '',
              businessIdentification: '00000000',
              currencyCode: '840',
              dailyStatementSequenceNumber: '0',
              debitProductCode: '',
              descriptionOfTransaction:
                ' F04120010000RM032 RETURN MAIL FEE          OMAHA        NE           ',
              detailIndustryTransactionIdentifier: '032103956056358',
              dualityFlag: '',
              feeAttribute: '000',
              flapIdentifier: '0000000',
              mailPhoneIndicator: '',
              merchantAccount: '041200******0448',
              merchantCategoryCode: '00000',
              merchantCity: 'OMAHA',
              merchantName: 'RETURN MAIL FEE          ',
              merchantState: 'NE',
              originalPostDate: '000000',
              originalReferenceNumber: '',
              pointOfSaleEntryMode: '',
              postDate: '2021-02-01',
              presentationInstrumentTypeCode: '',
              promotionIdentifier: '',
              riskIdentificationServiceIndicator: '',
              sourceTransactionIdentifier: '020',
              transactionAccountNumber: '041200******7541',
              transactionAmount: '0000000000000000.00',
              transactionCode: '253',
              transactionDate: '2021-02-01',
              visaNetInterchangeCenterProcessingDate: ''
            }
          }
        }
      }
    };

    const { wrapper } = renderComponentWithMockProvider(initialState);

    expect(
      queryAllByClass(wrapper.baseElement as HTMLElement, /dls-modal-title/)
        .length
    ).toEqual(1);

    expect(screen.getByText('txt_transaction_details')).toBeInTheDocument();

    // render Info View
    expect(screen.queryByTestId(INFO)).toBeInTheDocument();

    // render Content View
    expect(screen.queryByTestId(CONTENT)).toBeInTheDocument();
  });

  it('Should close when trigger cancel button without callback', () => {
    const initialState = {
      statementTransactionList: {
        [storeId]: { detail: { isOpen: true, loading: false } }
      }
    };
    const { wrapper } = renderComponentWithMockProvider(initialState);

    expect(screen.getByText('txt_transaction_details')).toBeInTheDocument();

    jest.useFakeTimers();

    expect(screen.getAllByText('txt_close').length).toBe(1);

    fireEvent.click(screen.getByText('txt_close')!);

    jest.runAllTimers();

    expect(
      queryAllByClass(wrapper.baseElement as HTMLElement, /dls-modal-title/)
        .length
    ).toEqual(0);
  });
});
