import React, { useState } from 'react';

// components
import {
  Modal,
  ModalHeader,
  ModalTitle,
  ModalBody,
  ModalFooter,
  Button
} from 'app/_libraries/_dls/components';
import GridViewHistory from '../GridViewHistory';

// hooks
import { useAccountDetail } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

const AccountStatementHistory: React.FC = () => {
  const { t } = useTranslation();
  const [isViewHistory, setIsViewHistory] = useState<boolean>(false);
  const { storeId } = useAccountDetail();

  const handleViewHistory = () => setIsViewHistory(true);

  const handleCancelViewHistory = () => setIsViewHistory(false);

  return (
    <>
      {checkPermission(PERMISSIONS.STATEMENT_HISTORY_VIEW, storeId) && (
        <Button
          dataTestId={genAmtId('accountStatementHistory', 'view-btn', '')}
          size="sm"
          id={`${storeId}-external-status`}
          className="mr-n8"
          variant="outline-primary"
          onClick={handleViewHistory}
        >
          {t('txt_view_history')}
        </Button>
      )}
      <Modal
        full
        show={isViewHistory}
        dataTestId={genAmtId('accountStatementHistory', 'modal', '')}
      >
        <ModalHeader
          dataTestId={genAmtId('accountStatementHistory', 'header', '')}
          border
          closeButton
          onHide={handleCancelViewHistory}
        >
          <ModalTitle
            dataTestId={genAmtId('accountStatementHistory', 'header-title', '')}
          >
            {t('txt_statement_history')}
          </ModalTitle>
        </ModalHeader>
        <ModalBody>
          <GridViewHistory />
        </ModalBody>
        <ModalFooter
          dataTestId={genAmtId('accountStatementHistory', 'close-btn', '')}
          border
          cancelButtonText={t('txt_close')}
          onCancel={handleCancelViewHistory}
        />
      </Modal>
    </>
  );
};

export default AccountStatementHistory;
