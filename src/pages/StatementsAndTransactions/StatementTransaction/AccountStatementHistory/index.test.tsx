import React from 'react';
import { AccountDetailProvider } from 'app/hooks';

// components
import AccountStatementHistory from './index';

// test utils
import { fireEvent } from '@testing-library/dom';
import { queryByClass, queryById } from 'app/_libraries/_dls/test-utils';
import { renderMockStoreId } from 'app/test-utils';
import { screen } from '@testing-library/react';
import * as entitlementsHelpers from 'app/entitlements/helpers';

const storeId = '123456789';
const renderComponentWithMockProvider = (initialState: object) => {
  return renderMockStoreId(
    <AccountDetailProvider
      value={{
        storeId,
        accEValue: storeId
      }}
    >
      <AccountStatementHistory />
    </AccountDetailProvider>,
    {
      initialState
    },
    true
  );
};

describe('Test AccountStatementHistory component', () => {
  it('Render UI', () => {
    const initialState: object = {
      statementTransactionList: {
        [storeId]: {}
      }
    };
    jest.useFakeTimers();
    const spy = jest.spyOn(entitlementsHelpers, 'checkPermission').mockImplementation(() => true);
    const { wrapper } = renderComponentWithMockProvider(initialState);

    expect(screen.getByText('txt_view_history')).toBeInTheDocument();
    // open Statement History modal when click View History
    fireEvent.click(
      queryById(
        wrapper.baseElement as HTMLElement,
        `${storeId}-external-status`
      )!
    );
    jest.runAllTimers();

    expect(screen.getByText('txt_statement_history')).toBeInTheDocument();
    expect(
      queryByClass(wrapper.baseElement as HTMLElement, /modal-content/)
    ).toBeInTheDocument();
    // close modal Statement History when click Close
    fireEvent.click(screen.getByText('txt_close'));

    jest.runAllTimers();
    expect(
      queryByClass(wrapper.baseElement as HTMLElement, /modal-content/)
    ).not.toBeInTheDocument();

    spy.mockReset();
    spy.mockRestore();
  });
});
