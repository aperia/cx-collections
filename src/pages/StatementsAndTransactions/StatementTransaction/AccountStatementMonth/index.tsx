import React, { useEffect, useRef, useState } from 'react';

// components
import {
  DropdownBaseChangeEvent,
  DropdownList
} from 'app/_libraries/_dls/components';
import Overview from '../Overview';

// helpers
import debounce from 'lodash.debounce';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// redux store
import { batch, useDispatch } from 'react-redux';
import { selectDataAccountStatementMonth } from '../__redux/selectors';
import { statementTransactionAction } from '../__redux/reducers';

// types
import { StatementMonthListData } from '../types';

// constant
import { PAGINATION_TRANSACTION, STATEMENT_MONTH_DEFAULT } from '../constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface AccountStatementMonthProps {}

const AccountStatementMonth: React.FC<AccountStatementMonthProps> = () => {
  const { storeId, accEValue } = useAccountDetail();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const accountStatementMonth = useStoreIdSelector<StatementMonthListData[]>(
    selectDataAccountStatementMonth
  );
  const data = accountStatementMonth.map(item => ({
    ...item,
    text: t(item.text)
  }));

  const [value, setValue] = useState<StatementMonthListData>({
    ...STATEMENT_MONTH_DEFAULT,
    text: t(STATEMENT_MONTH_DEFAULT.text)
  });

  const handleChangeDebounceRef = useRef(
    debounce(
      (value: StatementMonthListData) => {
        batch(() => {
          dispatch(
            statementTransactionAction.updatePostData({
              storeId,
              postData: {
                statementDate: value.code,
                startSequence: PAGINATION_TRANSACTION.START_SEQUENCE,
                endSequence: PAGINATION_TRANSACTION.END_SEQUENCE_FIFTY
              }
            })
          );
          dispatch(statementTransactionAction.updateIsReset({ storeId }));
        });
      },
      250,
      { leading: true, trailing: true }
    )
  );

  const handleChange = (event: DropdownBaseChangeEvent) => {
    const { value } = event.target;
    setValue(value);
  };

  useEffect(() => {
    dispatch(
      statementTransactionAction.getStatementMonthList({
        storeId,
        postData: { accId: accEValue }
      })
    );
  }, [dispatch, storeId, accEValue]);

  useEffect(() => {
    handleChangeDebounceRef.current(value);
  }, [value]);

  return (
    <div className="mt-16">
      <div className="d-flex align-items-center">
        <span
          className="fw-500 color-grey mr-4"
          data-testid={genAmtId(
            'statementTransaction',
            'statement',
            'AccountStatementMonth'
          )}
        >
          {t('txt_statement')}:
        </span>
        <DropdownList
          dataTestId={genAmtId(
            'statementTransaction',
            'date',
            'AccountStatementMonth'
          )}
          textField="text"
          value={value}
          onChange={handleChange}
          variant="no-border"
          noResult={t('txt_no_results_found')}
        >
          {data.map(item => (
            <DropdownList.Item key={item.code} label={item.text} value={item} />
          ))}
        </DropdownList>
      </div>
      <Overview statementDate={value.code} />
    </div>
  );
};

export default AccountStatementMonth;
