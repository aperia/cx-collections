import React from 'react';

// test utils
import { screen } from '@testing-library/react';
import { renderMockStoreId, mockActionCreator } from 'app/test-utils';
import { queryByClass } from 'app/_libraries/_dls/test-utils/queryHelpers';
import userEvent from '@testing-library/user-event';

// component
import AccountStatementMonth from './index';

// redux
import { statementTransactionAction } from '../__redux/reducers';

HTMLCanvasElement.prototype.getContext = jest.fn();

const storeId = '123456789';
const initialState: object = {
  statementTransactionList: {
    [storeId]: {
      monthList: {
        data: [
          { code: '', text: 'txt_cycle_to_date' },
          { code: '20210620', text: '06/20/2021' },
          { code: '20210520', text: '05/20/2021' }
        ],
        isLoading: false
      }
    }
  }
};
const renderComponentWithMockProvider = (initialState: object) => {
  return renderMockStoreId(
    <AccountStatementMonth />,
    {
      initialState
    },
    true
  );
};

let spy: jest.SpyInstance;
afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

const statementActionMock = mockActionCreator(statementTransactionAction);

describe('Test AccountStatementMonth component', () => {
  it('Render UI', () => {
    renderComponentWithMockProvider(initialState);
    expect(screen.getByText('txt_cycle_to_date')).toBeInTheDocument();
  });
  it('handleOnChange', () => {
    const getStatementMonthListMock = statementActionMock(
      'getStatementMonthList'
    );
    const { wrapper } = renderComponentWithMockProvider(initialState);

    expect(getStatementMonthListMock).toBeCalled();

    expect(
      queryByClass(wrapper.baseElement as HTMLElement, /dls-dropdown-list/)
    ).toBeInTheDocument();

    expect(screen.getByText('txt_cycle_to_date')).toBeInTheDocument();
    expect(screen.queryByText('05/20/2021')).not.toBeInTheDocument();

    // open dropdown
    const { container, baseElement } = wrapper;
    const iconDropdownListElement = container.querySelector(
      '.icon.icon-chevron-down'
    );
    userEvent.click(iconDropdownListElement!);

    const popup = baseElement.querySelector('.dls-popup');

    expect(popup).toBeInTheDocument();
    // select dropdown item
    jest.useFakeTimers();

    userEvent.click(screen.getByText('05/20/2021')!);

    // use runOnlyPendingTimers for lodash debounce
    jest.runOnlyPendingTimers();

    expect(popup).not.toBeInTheDocument();
    expect(screen.getByText('05/20/2021')).toBeInTheDocument();
    expect(screen.queryByText('txt_cycle_to_date')).not.toBeInTheDocument();
  });
});
