import { ReactText } from 'react';
import { SortType } from 'app/_libraries/_dls/components';

export interface StatementMonthListArg {
  storeId: string;
  postData: { accId: string };
}

export interface StatementMonthListData {
  code: string;
  text: string;
}

export interface StatementMonthListDataBE {
  statementDate: string;
  statementType: string;
}

export interface StatementMonthListPayload {
  data?: {
    statement?: StatementMonthListDataBE[];
  };
}

export interface StatementMonthListState {
  isLoading?: boolean;
  data?: StatementMonthListData[];
}

export interface AccountStatementHistoryData {
  totalCreditAmount: ReactText;
  statementDate: ReactText;
  statementBalanceAmount: ReactText;
  minimumPaymentDue: ReactText;
  minimumPaymentDueDate: ReactText;
  pastDueAmount: ReactText;
  balanceCashAdvances: ReactText;
  availableCredit: ReactText;
  creditLimit: ReactText;
  purchaseCount: ReactText;
  totalPurchaseAmount: ReactText;
  creditCount: ReactText;
  cashAdvanceCount: ReactText;
  totalCashAdvanceAmount: ReactText;
  paymentsCount: ReactText;
  totalPaymentAmount: ReactText;
  adjustmentsCount: ReactText;
  totalAdjustmentsAmount: ReactText;
  annualPercentageRateCashAdvances: ReactText;
  annualPercentageRateMerchandise: ReactText;
  lateFees: ReactText;
  overlimitFees: ReactText;
  financeChargesPurchases: ReactText;
  financeChargeCashAdvances: ReactText;
  averageDailyBalanceCashAdvances: ReactText;
  averageDailyBalancePurchases: ReactText;
  behaviorScorecardIdentifier: ReactText;
  beaconScore: ReactText;
}

export interface AccountStatementHistoryFormatData {
  statementDate: string;
  newBalance: string;
  minPayDue: string;
  dueDate: string;
  pastDue: string;
  cashBalance: string;
  availAbleCredit: string;
  creditLimit: string;
  purchaseCount: number;
  purchaseAmount: string;
  creditCount: number;
  creditAmount: string;
  cashCount: string;
  cashAmount: string;
  paymentCount: string;
  paymentAmount: string;
  adjustmentCount: string;
  adjustmentAmount: string;
  aprCash: string;
  aPRMerchant: string;
  lateFees: string;
  overLimitFees: string;
  financeChargeP: string;
  financeChargeC: string;
  cashBalanceADB: string;
  purchaseBalanceADB: string;
  behavior: number;
  bureauScore: number;
}

export interface AccountStatementHistoryArg {
  storeId: string;
  postData: {
    accId: string;
  };
}

export interface AccountStatementHistoryPayload {
  data?: AccountStatementHistoryData[];
}

export interface AccountStatementHistoryState {
  isLoading?: boolean;
  data?: AccountStatementHistoryFormatData[];
  noData?: boolean;
}
// Overview
export interface StatementOverviewData {
  statementCreditLineAmount?: ReactText;
  availableCreditRemainingAmount?: ReactText;
  daysInBillingCycleNumber?: ReactText;
  statementDate?: ReactText;
  paymentDueDate?: string;
  minimumPaymentAmount?: string;
  pastDueAmount?: string;
  mpdPayoffYears?: ReactText;
  totalCostMfd?: ReactText;
  totalYearsMonths?: ReactText;
  payment36Month?: ReactText;
  totalCost36?: ReactText;
  costSavings?: ReactText;
  previousMonthBalance?: ReactText;
  paymentAndCredit?: ReactText;
  purchase?: ReactText;
  periodicFinanceCharge?: ReactText;
  newAccountBalance?: ReactText;
  otherFees?: ReactText;
  heldStatementDestinationCode?: string;

  purAdb?: ReactText;
  purDailyPeriodicRate?: ReactText;
  purCorrespondingApr?: ReactText;
  purApr?: ReactText;
  cashAdb?: ReactText;
  cashDailyPeriodicRate?: ReactText;
  cashCorrespondingApr?: ReactText;
  cashApr?: ReactText;
}
export interface StatementOverviewArg {
  storeId: string;
  postData: {
    accountId: string;
    statementDate: string;
  };
}

export interface StatementOverviewPayload {
  data: StatementOverviewData;
}

export interface StatementOverviewRequestBody {
  accountId: string;
  statementDate: string;
}

export interface StatementOverviewState {
  overview: {
    isLoading?: boolean;
    data?: StatementOverviewData;
  };
}

// End Overview

// Detail
export interface DataDetail {
  authorizationDate?: Date | string;
  transactedAccountNumber?: ReactText;
  accountIdentifier?: ReactText;
  authorizationAmount?: ReactText;
  internalStatusCode?: ReactText;
  externalStatusCode?: ReactText;

  transactionDate?: Date | string;
  postDate?: Date | string;
  aggregateTransactionAmount?: ReactText;
  originalReferenceNumber?: ReactText;
  descriptionOfTransaction?: ReactText;

  originalPostDate?: Date | string;
  presentationInstrumentTypeCode?: ReactText;
  transactionAccountNumber?: ReactText;
  transactionCode?: ReactText;

  merchantName?: ReactText;
  businessIdentification?: ReactText;
  merchantCategoryCode?: ReactText;
  merchantAccount?: ReactText;
  riskIdentificationServiceIndicator?: ReactText;
  merchantCity?: ReactText;
  merchantState?: ReactText;

  currencyCode?: ReactText;
  transactionAmount?: ReactText;

  authorizationNumber?: ReactText;
  authorizationSourceCode?: ReactText;

  agentNumber?: ReactText;
  dailyStatementSequenceNumber?: ReactText;

  automatedTellerMachineAtmFlag?: ReactText;
  debitProductCode?: ReactText;

  sourceTransactionIdentifier?: ReactText;
  promotionIdentifier?: ReactText;
  detailIndustryTransactionIdentifier?: ReactText;
  flapIdentifier?: ReactText;
  feeAttribute?: ReactText;
  mailPhoneIndicator?: ReactText;
  dualityFlag?: ReactText;
  visaNetInterchangeCenterProcessingDate?: Date | string;
  pointOfSaleEntryMode?: ReactText;
}

export interface DetailModalArg {
  storeId: string;
  postData: {
    accId?: string;
    transactionCode?: ReactText;
  };
}

export interface DetailModalPayload {
  data?: DataDetail;
}
export interface DetailModal extends StoreIdPayload {
  transactionCode?: React.ReactText;
}
export interface DataDetailModal extends StoreIdPayload {
  data?: DataDetail;
}
// End Detail
export interface StatementTransactionArg {
  storeId: string;
  postData: {
    accId: string;
    startSequence: string;
    endSequence: string;
    isCycleToDate: boolean;
    statementDate: string;
  };
}

export interface RawStatementTransactionData {
  aggregateTransactionAmount?: string;
  descriptionOfTransaction?: string;
  postDate?: string;
  transactionDate?: string;
  transactionCode?: string;

  originalReferenceNumber?: string;
  originalPostDate?: string;
  transactionAccountNumber?: string;
  presentationInstrumentTypeCode?: string;
  merchantDescription?: string;
  businessIdentification?: string;
  merchantCategoryCode?: string;
  merchantAccount?: string;
  riskIdentificationServiceMerchantIndicator?: string;
  merchantCity?: string;
  merchantState?: string;
  currencyCode?: string;
  transactionAmount?: string;
  authorizationNumber?: string;
  authorizationSourceCode?: string;
  agentNumber?: string;
  dailyStatementSequenceNumber?: string;
  automatedTellerMachineAtmFlag?: string;
  debitProductCode?: string;
  sourceTransactionIdentifier?: string;
  promotionIdentifier?: string;
  dualityFlag?: string;
  detailIndustryTransactionIdentifier?: string;
  flapIdentifier?: string;
  visaNetInterchangeCenterProcessingDate?: string;
  feeAttribute?: string;
  mailPhoneIndicator?: string;
  pointOfSaleEntryMode?: string;
}

export interface StatementTransactionData {
  aggregateTransactionAmount?: number;
  descriptionOfTransaction?: string;
  postDate?: string;
  transactionDate?: string;
  transactionCode?: string;

  detail?: DataDetail;
}

export interface SortByTransactionPayload {
  storeId: string;
  sortType: SortType;
}

export interface StatementTransactionPayload {
  data: StatementTransactionData[];
}

export interface StatementTransactionRequestBody {
  accId: string;
  startSequence: string;
  endSequence: string;
  isCycleToDate: boolean;
  statementDate: string;
}

export interface PostData {
  startSequence: string;
  endSequence: string;
  isCycleToDate: boolean;
  statementDate: string;
}

export interface UpdatePostData {
  storeId: string;
  postData: {
    startSequence?: string;
    endSequence?: string;
    isCycleToDate?: boolean;
    statementDate?: string;
  };
}

export interface StatementTransactionState {
  error?: boolean;
  isLoading?: boolean;
  isLoadMore?: boolean;
  sortType?: SortType;
  data?: StatementTransactionData[];
  isFirstCall?: boolean;
  noTransaction?: boolean;
  isReset?: boolean;
  postData?: PostData;
  monthList?: StatementMonthListState;
  statementHistory?: AccountStatementHistoryState;
  overview?: {
    isLoading?: boolean;
    data?: StatementOverviewData;
  };
  detail?: {
    data?: DataDetail;
    isOpen?: boolean;
    transactionCode?: ReactText;
  };
  adjust?: {
    data?: DataDetail;
    isOpen?: boolean;
    isLoading?: boolean;
    isLoadingModal?: boolean;
    transactionCode?: ReactText;
    adjustmentCodes?: IAdjustmentCode[];
  };
}

export interface StatementTransactionStateResult
  extends Record<string, StatementTransactionState> {}

export interface StatementTransactionInitialState {
  [storeId: string]: StatementTransactionState;
}

export interface StatementOverviewPayload {
  accountId: React.ReactText;
  statementDate: string;
}

export interface StatementTransactionPayload {
  accId: string;
  startSequence: string;
  endSequence: string;
  isCycleToDate: boolean;
  statementDate: string;
}

export interface ResponseStatementOverviewTransaction {
  customers: Array<StatementOverviewData>;
}

export interface TransactionDetailPayload {
  accId: string;
  transactionCode?: React.ReactText;
}

export interface PaginationTransactionType {
  START_SEQUENCE: string;
  END_SEQUENCE_FIFTY: string;
  NEXT_SEQUENCE: number;
}

export interface IDSortGridType {
  AMOUNT: string;
  DATE_TIME: string;
  TRANSACTION_DATE: string;
  POST_DATE: string;
}

export interface AdjustTransactionRequestArgs {
  storeId: string;
  accEValue: string;
  accNbr: string;
}

export interface IAdjustmentCode {
  active: boolean;
  adjustmentBatchCode: string;
  adjustmentDescription: string;
  adjustmentTranCode: string;
  originalTranCode: string;
}

export interface IPayloadGetAdjustmentCode {
  adjustmentCodes: IAdjustmentCode[];
}

export interface IDataRequestAdjustmentTransaction {
  common: {
    user: 'string';
    cycle: 'M';
    accountId: string;
  };
  batchType: string;
  batchPrefix: string;
  transactionCode: string;
  transactionAmount: string;
  adjustmentAmount: string;
  transactionPostingDate: string;
  monetaryRejectOverrideIndicator: string;
}
