import React from 'react';
import StatementTransaction from 'pages/StatementsAndTransactions/StatementTransaction';
import { renderMockStoreId } from 'app/test-utils';
import { screen } from '@testing-library/react';

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('Test StatementTransaction/index.test.ts', () => {
  it('Render UI', () => {
    jest.useFakeTimers();
    renderMockStoreId(<StatementTransaction />);
    jest.runAllTimers();
    expect(screen.getByText('txt_statement')).toBeInTheDocument();
  });
});
