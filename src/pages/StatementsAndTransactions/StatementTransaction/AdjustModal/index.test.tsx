import React from 'react';
import { screen } from '@testing-library/react';

// mocks
import 'app/test-utils/mocks/MockView';

// hooks
import { AccountDetailProvider } from 'app/hooks';

// components
import AdjustModal, {
  ADJUST_TRANSACTION_VIEW_DESCRIPTOR,
  DETAIL_TRANSACTION_VIEW_DESCRIPTOR
} from './index';

// actions
import { statementTransactionAction as action } from '../__redux/reducers';

// utils
import {
  mockActionCreator,
  renderMockStore,
  storeId,
  accEValue,
  accNbr
} from 'app/test-utils';
import { storeIdViewNotFound } from 'app/test-utils/mocks/MockView';
import { I18N_ADJUST_TRANSACTION } from './contants';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const mockStoreIdHasDetail = 'hasDataDetail';
const mockStoreIdNotShowModal = 'notShowModal';
const mockStoreIdDetailReversal = 'mockStoreIdDetailReversal';

const generateState = () => {
  const state: Partial<RootState> = {
    statementTransactionList: {
      [storeId]: {
        adjust: {
          isOpen: true,
          adjustmentCodes: [
            {
              active: true,
              adjustmentBatchCode: '1',
              adjustmentDescription: '1',
              adjustmentTranCode: '1',
              originalTranCode: '1'
            }
          ]
        },
        detail: {
          data: {
            transactionCode: '1'
          }
        }
      },
      [mockStoreIdHasDetail]: {
        adjust: {
          isOpen: true
        },
        detail: {
          data: {
            postDate: '2021-04-05',
            aggregateTransactionAmount: '32.05'
          }
        }
      },
      [mockStoreIdNotShowModal]: {
        adjust: {
          isOpen: false
        },
        detail: {
          data: { postDate: '2021-04-05' }
        }
      },
      [storeIdViewNotFound]: {
        adjust: {
          isOpen: true
        },
        detail: {
          data: { postDate: '2021-04-05', aggregateTransactionAmount: '32.05' }
        }
      },
      [mockStoreIdDetailReversal]: {
        adjust: {
          isOpen: true
        },
        detail: {
          data: {
            postDate: '2021-04-05',
            aggregateTransactionAmount: '32.05',
            descriptionOfTransaction: 'reversal'
          }
        }
      }
    }
  };
  return state;
};

const renderWrapper = (initialState?: Partial<RootState>, id?: string) => {
  return renderMockStore(
    <AccountDetailProvider
      value={{ storeId: id ? id : storeId, accEValue, accNbr }}
    >
      <AdjustModal />
    </AccountDetailProvider>,
    { initialState: initialState ? initialState : generateState() }
  );
};

const actionSpy = mockActionCreator(action);

describe('Render', () => {
  it('not show modal', () => {
    renderWrapper(generateState(), mockStoreIdNotShowModal);
    expect(
      screen.queryByText(I18N_ADJUST_TRANSACTION.ADJUST_TRANSACTION)
    ).not.toBeInTheDocument();
  });

  it('render correctly', () => {
    jest.useFakeTimers();
    renderWrapper();
    jest.runAllTimers();
    expect(
      screen.getByText(I18N_ADJUST_TRANSACTION.ADJUST_TRANSACTION)
    ).toBeInTheDocument();
    expect(
      screen.getByTestId(ADJUST_TRANSACTION_VIEW_DESCRIPTOR)
    ).toBeInTheDocument();
  });

  it('render infoView', () => {
    jest.useFakeTimers();

    renderWrapper(generateState(), mockStoreIdHasDetail);

    jest.runAllTimers();

    expect(
      screen.getByText(I18N_ADJUST_TRANSACTION.ADJUST_TRANSACTION)
    ).toBeInTheDocument();
    expect(
      screen.getByTestId(DETAIL_TRANSACTION_VIEW_DESCRIPTOR)
    ).toBeInTheDocument();
  });

  it('cover reversal', () => {
    jest.useFakeTimers();

    renderWrapper(generateState(), mockStoreIdDetailReversal);

    jest.runAllTimers();

    expect(
      screen.getByText(I18N_ADJUST_TRANSACTION.ADJUST_TRANSACTION)
    ).toBeInTheDocument();
    expect(
      screen.getByTestId(DETAIL_TRANSACTION_VIEW_DESCRIPTOR)
    ).toBeInTheDocument();
  });

  it('cover not amountField', async () => {
    jest.useFakeTimers();

    renderWrapper(generateState(), storeIdViewNotFound);

    jest.runAllTimers();

    expect(
      screen.getByText(I18N_ADJUST_TRANSACTION.ADJUST_TRANSACTION)
    ).toBeInTheDocument();
    expect(
      screen.getByTestId(DETAIL_TRANSACTION_VIEW_DESCRIPTOR)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleClose', () => {
    const mockToggleAdjustModal = actionSpy('toggleAdjustModal');
    renderWrapper();
    const cancelButton = screen.getByText(I18N_ADJUST_TRANSACTION.CANCELED);
    cancelButton.click();
    expect(mockToggleAdjustModal).toBeCalled();
  });

  it('handleSubmit', () => {
    const mockAdjustTransactionRequest = actionSpy('adjustTransactionRequest');
    renderWrapper();
    const submitButton = screen.getByText(I18N_ADJUST_TRANSACTION.SUBMIT);
    submitButton.click();
    expect(mockAdjustTransactionRequest).toBeCalled();
  });
});
