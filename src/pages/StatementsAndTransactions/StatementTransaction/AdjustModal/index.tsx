import React, { useEffect, useMemo, useRef } from 'react';
import { isInvalid } from 'redux-form';

// components
import {
  Modal,
  ModalHeader,
  ModalTitle,
  ModalFooter
} from 'app/_libraries/_dls/components';

// constants

// redux store
import { useDispatch, useSelector } from 'react-redux';
import { statementTransactionAction as action } from '../__redux/reducers';
import {
  selectAdjustLoading as selectLoading,
  selectAdjustmentCodes,
  selectAdjustModalOpen as selectIsOpen,
  selectDetailData
} from '../__redux/selectors';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helper
import isEmpty from 'lodash.isempty';

// types & constants
import { I18N_ADJUST_TRANSACTION } from './contants';
import { INFO } from '../constants';
import { DataDetail, IAdjustmentCode } from '../types';
import { handleWithRefOnFind, isArrayHasValue } from 'app/helpers';
import { View } from 'app/_libraries/_dof/core';
import { genAmtId } from 'app/_libraries/_dls/utils';
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';

export const ADJUST_TRANSACTION_VIEW_DESCRIPTOR = 'transactionAdjustment';
export const DETAIL_TRANSACTION_VIEW_DESCRIPTOR = INFO;

export interface ModalProps {}

const DetailModal: React.FC<ModalProps> = () => {
  const viewRef = useRef<any>(null); // view ref DOF has any type
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId, accEValue, accNbr } = useAccountDetail();

  const formName = `${storeId}_transaction-adjustment`;

  const loading = useStoreIdSelector<boolean>(selectLoading);
  const isOpenModal = useStoreIdSelector<boolean>(selectIsOpen);
  const transactionDetail = useStoreIdSelector<DataDetail>(selectDetailData);
  const adjustmentCodes = useStoreIdSelector<IAdjustmentCode[]>(
    selectAdjustmentCodes
  );

  const isInvalidForm = useSelector(isInvalid(formName));

  const originalTransactionCode = transactionDetail?.transactionCode;

  const adjustmentType = useMemo(() => {
    const filteredAdjustmentCodes = adjustmentCodes.filter(
      item => item.originalTranCode === originalTransactionCode
    );

    return filteredAdjustmentCodes;
  }, [adjustmentCodes, originalTransactionCode]);

  const handleClose = () => dispatch(action.toggleAdjustModal({ storeId }));

  useEffect(() => {
    if (!isOpenModal) return undefined;

    dispatch(action.getAdjustTransactionType({ storeId }));
  }, [dispatch, storeId, isOpenModal]);

  useEffect(() => {
    if (!isArrayHasValue(adjustmentType)) {
      setImmediate(() => {
        const adjustView = handleWithRefOnFind(viewRef, 'type');
        adjustView?.props.setValue(undefined);
      });
      return undefined;
    }
    setImmediate(() => {
      const adjustView = handleWithRefOnFind(viewRef, 'type');
      adjustView?.props.setData(adjustmentType);
    });
  }, [adjustmentType]);

  const handleSubmit = () => {
    dispatch(
      action.adjustTransactionRequest({
        storeId,
        accEValue,
        accNbr: accNbr!
      })
    );
  };

  const infoView = useMemo(() => {
    if (isEmpty(transactionDetail) || !isOpenModal) return null;
    return (
      <View
        id={`${storeId}_${DETAIL_TRANSACTION_VIEW_DESCRIPTOR}`}
        formKey={`${storeId}_${DETAIL_TRANSACTION_VIEW_DESCRIPTOR}_view`}
        descriptor={DETAIL_TRANSACTION_VIEW_DESCRIPTOR}
        value={transactionDetail}
      />
    );
  }, [transactionDetail, isOpenModal, storeId]);

  useEffect(() => {
    const originalAmount = transactionDetail?.aggregateTransactionAmount;
    const description = transactionDetail?.descriptionOfTransaction;

    if (!originalAmount || !isOpenModal) return;

    setImmediate(() => {
      const defaultAmount = Number(originalAmount).toString();

      const amountField = handleWithRefOnFind(viewRef, 'amount');

      if (!amountField) return;

      amountField.props.setValue(defaultAmount);

      const originalField = handleWithRefOnFind(viewRef, 'originalAmount');
      originalField.props.setValue(defaultAmount);

      // [business] set amount readonly when description contains reversal
      if (
        description &&
        description.toString().toLowerCase().includes('reversal')
      ) {
        amountField.props.setReadOnly(true);
      }
    });
  }, [transactionDetail, isOpenModal]);

  return (
    <Modal
      md
      loading={loading}
      show={isOpenModal}
      className="window-lg-full"
      dataTestId="adjustTransaction-modal"
    >
      <ModalHeader
        dataTestId="adjustTransaction-header"
        border
        closeButton
        onHide={handleClose}
      >
        <ModalTitle dataTestId="adjustTransaction-header-title">
          {t(I18N_ADJUST_TRANSACTION.ADJUST_TRANSACTION)}
        </ModalTitle>
      </ModalHeader>
      <ModalBodyWithApiError
        className="p-0"
        storeId={storeId}
        apiErrorClassName="pl-24 pt-24 bg-light-l20"
        dataTestId={genAmtId('adjustTransaction', 'body', '')}
      >
        <div className="p-24 border-bottom bg-light-l20">
          <h5
            data-testid={genAmtId(
              'adjustTransaction',
              'current-transaction',
              'AdjustModal'
            )}
          >
            {t(I18N_ADJUST_TRANSACTION.CURRENT_TRANSACTION)}
          </h5>
          {infoView}
        </div>
        <div className="pt-24 px-24">
          <View
            id={formName}
            formKey={formName}
            descriptor={ADJUST_TRANSACTION_VIEW_DESCRIPTOR}
            ref={viewRef}
          />
        </div>
      </ModalBodyWithApiError>
      <ModalFooter
        dataTestId="adjustTransaction-footer"
        okButtonText={t(I18N_ADJUST_TRANSACTION.SUBMIT)}
        onOk={handleSubmit}
        cancelButtonText={t(I18N_ADJUST_TRANSACTION.CANCELED)}
        onCancel={handleClose}
        disabledOk={isInvalidForm || (adjustmentType?.length || 0) === 0}
      />
    </Modal>
  );
};

export default DetailModal;
