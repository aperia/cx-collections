export const I18N_ADJUST_TRANSACTION = {
  ADJUST_TRANSACTION: 'txt_adjust_transaction',
  CURRENT_TRANSACTION: 'txt_current_transaction',
  ADJUSTMENT: 'txt_adjustment',
  TRANS_DATE: 'txt_trans_date',
  POSTING_DATE: 'txt_posting_date',
  AMOUNT: 'txt_amount',
  REFERENCE_NUMBER: 'txt_reference_number',
  DESCRIPTION: 'txt_description',
  TYPE: 'txt_type',
  REASON_FOR_ADJUSTMENT: 'txt_reason_for_adjustment',
  PROMO_ID: 'txt_promo_id',
  CANCELED: 'txt_cancel',
  SUBMIT: 'txt_submit',
  AMOUNT_ERROR_MSG: 'txt_adjust_transaction_amount_msg',
  ADJUSTMENT_REQUEST_FAILED: 'txt_adjustment_request_failed',
  ADJUSTMENT_REQUEST_SUCCESS: 'txt_adjustment_request_success'
};
