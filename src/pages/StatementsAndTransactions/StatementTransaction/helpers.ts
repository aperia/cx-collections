// Types
import { SortType } from 'app/_libraries/_dls/components';
import {
  AccountStatementHistoryFormatData,
  AccountStatementHistoryData,
  StatementTransactionData
} from './types';

// Constants
import { ROW_HEADER_STATEMENT_HISTORY } from './constants';
import { ID_SORT_GRID } from './constants';

// Helpers
import { formatCommon } from 'app/helpers';

export const formatDataGrid = (
  data: AccountStatementHistoryFormatData[],
  t: Function
) => {
  const dataBody: Record<string, string>[] = [];

  Object.entries(ROW_HEADER_STATEMENT_HISTORY).forEach(([key, val]) => {
    dataBody.push(
      data
        .map((item: MagicKeyValue) => item[key])
        .reduce(
          (result, item, index) => ({
            ...result,
            [`month_${index + 1}`]: item
          }),
          { statementDate: t(val) }
        )
    );
  });

  return [...dataBody];
};

export const sortByGrid = (
  sortType: SortType,
  data: StatementTransactionData[]
) => {
  const gridData = [...data];
  switch (sortType.id) {
    case ID_SORT_GRID.AMOUNT:
      if (!sortType.order || sortType.order === 'desc') {
        return gridData.sort(
          (currentItem, nextItem) =>
            nextItem.aggregateTransactionAmount! -
            currentItem.aggregateTransactionAmount!
        );
      }
      return gridData.sort(
        (currentItem, nextItem) =>
          currentItem.aggregateTransactionAmount! -
          nextItem.aggregateTransactionAmount!
      );
    case ID_SORT_GRID.TRANSACTION_DATE:
    case ID_SORT_GRID.POST_DATE:
      if (!sortType.order || sortType.order === 'desc') {
        return gridData.sort(
          (currentItem, nextItem) =>
            new Date(nextItem?.transactionDate || '').getTime() -
            new Date(currentItem?.transactionDate || '').getTime()
        );
      }
      return gridData.sort(
        (currentItem, nextItem) =>
          new Date(currentItem?.transactionDate || '').getTime() -
          new Date(nextItem?.transactionDate || '').getTime()
      );
    default:
      return data;
  }
};

export const formatStatementHistoryData = (
  data: AccountStatementHistoryData[]
) => {
  return data.map(item => {
    const {
      statementDate,
      statementBalanceAmount,
      creditCount,
      totalCreditAmount,
      minimumPaymentDue,
      minimumPaymentDueDate,
      pastDueAmount,
      balanceCashAdvances,
      availableCredit,
      creditLimit,
      purchaseCount,
      totalPurchaseAmount,
      cashAdvanceCount,
      totalCashAdvanceAmount,
      paymentsCount,
      totalPaymentAmount,
      adjustmentsCount,
      totalAdjustmentsAmount,
      annualPercentageRateCashAdvances,
      annualPercentageRateMerchandise,
      lateFees,
      overlimitFees,
      financeChargesPurchases,
      financeChargeCashAdvances,
      averageDailyBalanceCashAdvances,
      averageDailyBalancePurchases,
      behaviorScorecardIdentifier,
      beaconScore
    } = item;
    return {
      statementDate: formatCommon(statementDate).time.date,
      newBalance: formatCommon(statementBalanceAmount).currency(),
      creditCount: formatCommon(creditCount).number,
      minPayDue: formatCommon(minimumPaymentDue).currency(),
      dueDate: formatCommon(minimumPaymentDueDate).time.date,
      pastDue: formatCommon(pastDueAmount).currency(),
      cashBalance: formatCommon(balanceCashAdvances).currency(),
      availAbleCredit: formatCommon(availableCredit).currency(),
      creditLimit: formatCommon(creditLimit).currency(),
      purchaseCount: formatCommon(purchaseCount).number,
      purchaseAmount: formatCommon(totalPurchaseAmount).currency(),
      creditAmount: formatCommon(totalCreditAmount).currency(),
      cashCount: formatCommon(cashAdvanceCount).number,
      cashAmount: formatCommon(totalCashAdvanceAmount).currency(),
      paymentCount: formatCommon(paymentsCount).number,
      paymentAmount: formatCommon(totalPaymentAmount).currency(),
      adjustmentCount: formatCommon(adjustmentsCount).number,
      adjustmentAmount: formatCommon(totalAdjustmentsAmount).currency(),
      aprCash: formatCommon(annualPercentageRateCashAdvances).percent,
      aPRMerchant: formatCommon(annualPercentageRateMerchandise).percent,
      lateFees: formatCommon(lateFees).currency(),
      overLimitFees: formatCommon(overlimitFees).currency(),
      financeChargeP: formatCommon(financeChargesPurchases).currency(),
      financeChargeC: formatCommon(financeChargeCashAdvances).currency(),
      cashBalanceADB: formatCommon(averageDailyBalanceCashAdvances).currency(),
      purchaseBalanceADB: formatCommon(averageDailyBalancePurchases).currency(),
      behavior: formatCommon(behaviorScorecardIdentifier).number,
      bureauScore: formatCommon(beaconScore).number
    };
  });
};
