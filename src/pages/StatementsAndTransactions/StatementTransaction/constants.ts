import { I18N_REF_DATA } from 'app/constants/i18n';
import { SortType } from 'app/_libraries/_dls/components';
import {
  PaginationTransactionType,
  StatementMonthListData,
  IDSortGridType
} from './types';

export const SORT_STATEMENT_TRANSACTION: SortType[] = [
  { id: 'transactionDate' }
];
export const STATEMENT_MONTH_DEFAULT: StatementMonthListData = {
  code: '',
  text: I18N_REF_DATA.CYCLE_TO_DATE
};
export const PAGINATION_TRANSACTION: PaginationTransactionType = {
  START_SEQUENCE: '1',
  END_SEQUENCE_FIFTY: '25',
  NEXT_SEQUENCE: 25
};
export const ROW_HEADER_STATEMENT_HISTORY: Record<string, string> = {
  newBalance: 'txt_new_balance',
  creditCount: 'txt_credit_count',
  minPayDue: 'txt_min_pay_due',
  dueDate: 'txt_payment_due_date',
  pastDue: 'txt_past_due_amount',
  cashBalance: 'txt_cash_balance',
  availAbleCredit: 'txt_available_credit',
  creditLimit: 'txt_credit_limit',
  purchaseCount: 'txt_purchase_count',
  purchaseAmount: 'txt_purchase_amount',
  creditAmount: 'txt_credit_amount',
  cashCount: 'txt_cash_count',
  cashAmount: 'txt_cash_amount',
  paymentCount: 'txt_payment_count',
  paymentAmount: 'txt_payment_amount',
  adjustmentCount: 'txt_adjustment_count',
  adjustmentAmount: 'txt_adjustment_amount',
  aprCash: 'txt_apr_cash',
  aPRMerchant: 'txt_apr_merchant',
  lateFees: 'txt_late_fees',
  overLimitFees: 'txt_over_limit_fees',
  financeChargeP: 'txt_finance_charge_(p)',
  financeChargeC: 'txt_finance_charge_(c)',
  cashBalanceADB: 'txt_cash_balance_adb',
  purchaseBalanceADB: 'txt_purchase_balance_adb',
  behavior: 'txt_behavior',
  bureauScore: 'txt_bureau_score'
};
export const TRANSACTION_ACTION = {
  VIEW: 'txt_view',
  ADJUST: 'txt_adjust'
};
export const INFO = 'accountDetailStatementDetailInfo';
export const CONTENT = 'accountDetailStatementDetailContent';
export const OVERVIEW = 'accountDetailStatementOverview';
export const POST_DATA_DEFAULT = {
  startSequence: PAGINATION_TRANSACTION.START_SEQUENCE,
  endSequence: PAGINATION_TRANSACTION.END_SEQUENCE_FIFTY,
  isCycleToDate: true,
  statementDate: ''
};

export const ID_SORT_GRID: IDSortGridType = {
  AMOUNT: 'amount',
  DATE_TIME: 'dateTime',
  TRANSACTION_DATE: 'transactionDate',
  POST_DATE: 'postDate'
};

export const SELECT_FIELDS = {
  OVERVIEW: [
    'statementCreditLineAmount',
    'availableCreditRemaining',
    'daysInBillingCycle',
    'statementDate',
    'paymentDueDate',
    'minimumPaymentAmount',
    'delinquentAmount',
    'minimumPaymentDuePayoffTotalYearCount',
    'totalMinimumPaymentDuePayoffAmount',
    'monthYearPayoffCount',
    'monthlyPaymentFor36MonthAmount',
    'totalPaymentFor36MonthAmount',
    'totalSavingsAmount',
    'statementSummaryLine1Field1',
    'statementSummaryLine1Field2',
    'statementSummaryLine1Field3',
    'statementSummaryLine1Field4',
    'statementSummaryLine1Field7',
    'statementSummaryLine1Field8',
    'statementSummaryLine1Field6',
    'heldStatementDestinationCode',
    'statementSummaryLine2Field5',
    'statementSummaryLine2Field2',
    'statementSummaryLine2Field3',
    'statementSummaryLine2Field1',
    'statementSummaryLine3Field5',
    'statementSummaryLine3Field2',
    'statementSummaryLine3Field3',
    'statementSummaryLine3Field1'
  ],
  ALL: ['*'],
  LIST: [
    'transactionDate',
    'postDate',
    'aggregateTransactionAmount',
    'descriptionOfTransaction'
  ],
  DETAIL: [
    'transactionDate',
    'postDate',
    'aggregateTransactionAmount',
    'descriptionOfTransaction',

    'originalPostDate',
    'originalReferenceNumber',
    'presentationInstrumentTypeCode',
    'transactionAccountNumber',
    'transactionCode',

    'merchantDescription',
    'businessIdentification',
    'merchantCategoryCode',
    'merchantAccount',
    'riskIdentificationServiceMerchantIndicator',
    'merchantCity',
    'merchantState',

    'currencyCode',
    'transactionAmount',

    'authorizationNumber',
    'authorizationSourceCode',

    'agentNumber',
    'dailyStatementSequenceNumber',

    'automatedTellerMachineAtmFlag',
    'debitProductCode',

    'sourceTransactionIdentifier',
    'promotionIdentifier',
    'industryTransactionIdentifier',
    'flapIdentifier',
    'feeAttribute',
    'mailPhoneIndicator',
    'dualityFlag',
    'visaNetInterchangeCenterProcessingDate',
    'pointOfSaleEntryMode'
  ]
};

export const ADJUST_ACCEPTABLE_TRANSACTION_CODE_LIST = [
  '253',
  '254',
  '255',
  '271',
  '900',
  '961',
  '962',
  '963',
  '986',
  '987',
  '988',
  '990',
  '991'
];
