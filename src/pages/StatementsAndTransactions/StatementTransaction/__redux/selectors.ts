import { createSelector } from '@reduxjs/toolkit';
import {
  SORT_STATEMENT_TRANSACTION,
  STATEMENT_MONTH_DEFAULT
} from '../constants';
import {
  AccountStatementHistoryFormatData,
  PostData,
  StatementMonthListData,
  StatementTransactionData,
  DataDetail,
  IAdjustmentCode
} from '../types';
import { ReactText } from 'react';
import { SortType } from 'app/_libraries/_dls/components';

export const selectDataAccountStatementMonth = createSelector(
  (states: RootState, storeId: string) =>
    states.statementTransactionList[storeId]?.monthList?.data,
  (data?: StatementMonthListData[]) => data ?? [
    STATEMENT_MONTH_DEFAULT
  ]
);

export const selectNodataStatementHistory = createSelector(
  (states: RootState, storeId: string) =>
    states.statementTransactionList[storeId]?.statementHistory?.noData ?? false,
  (noData: boolean) => noData
);

export const selectDataStatementHistory = createSelector(
  (states: RootState, storeId: string) =>
    states.statementTransactionList[storeId]?.statementHistory?.data,
  (data?: AccountStatementHistoryFormatData[]) => data ?? []
);

export const selectIsLoadingStatementHistory = createSelector(
  (states: RootState, storeId: string) =>
    states.statementTransactionList[storeId]?.statementHistory?.isLoading ??
    false,
  (loading: boolean) => loading
);

export const selectDataStatementTransaction = createSelector(
  (states: RootState, storeId: string) =>
    states.statementTransactionList[storeId]?.data,
  (data?: StatementTransactionData[]) => data ?? []
);

export const selectLoadingStatementTransaction = createSelector(
  (states: RootState, storeId: string) =>
    states.statementTransactionList[storeId]?.isLoading ?? false,
  (loading: boolean) => loading
);

export const selectLoadMoreStatementTransaction = createSelector(
  (states: RootState, storeId: string) =>
    states.statementTransactionList[storeId]?.isLoadMore ?? false,
  (isLoadMore: boolean) => isLoadMore
);

export const selectIsResetStatementTransaction = createSelector(
  (states: RootState, storeId: string) =>
    states.statementTransactionList[storeId]?.isReset ?? false,
  (isReset: boolean) => isReset
);

export const selectPostDataStatementTransaction = createSelector(
  (states: RootState, storeId: string) =>
    states.statementTransactionList[storeId]?.postData,
  (postData?: PostData) => postData
);

export const selectSortByStatementTransaction = createSelector(
  (states: RootState, storeId: string) =>
    states.statementTransactionList[storeId]?.sortType ??
    SORT_STATEMENT_TRANSACTION[0],
  (sortBy: SortType) => sortBy
);

export const selectDetailData = createSelector(
  (states: RootState, storeId: string) =>
    states.statementTransactionList[storeId]?.detail?.data || {},
  (data: DataDetail) => data
);

export const selectDetailIsOpen = createSelector(
  (states: RootState, storeId: string) =>
    states.statementTransactionList[storeId]?.detail?.isOpen || false,
  (isOpen: boolean) => isOpen
);

export const selectDetailTransactionCode = createSelector(
  (states: RootState, storeId: string) =>
    states.statementTransactionList[storeId]?.detail?.transactionCode || '',
  (isOpen: ReactText) => isOpen
);

export const selectNoTransaction = createSelector(
  (states: RootState, storeId: string) =>
    states.statementTransactionList[storeId]?.noTransaction || false,
  (noTransaction: boolean) => noTransaction
);

export const selectIsError = createSelector(
  (states: RootState, storeId: string) =>
    states.statementTransactionList[storeId]?.error || false,
  (error: boolean) => error
);

export const getAdjust = (states: RootState, storeId: string) => {
  return states.statementTransactionList[storeId]?.adjust ?? {};
};

export const selectAdjustData = createSelector(getAdjust, data => data.data);

export const selectAdjustModalOpen = createSelector(
  getAdjust,
  data => data.isOpen
);

export const selectAdjustLoading = createSelector(
  getAdjust,
  data => data.isLoading
);

export const selectAdjustmentCodes = createSelector(
  (states: RootState, storeId: string) =>
    states.statementTransactionList[storeId]?.adjust?.adjustmentCodes || [],
  (data: IAdjustmentCode[]) => data
);
