// Redux
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Helpers
import { formatTimeDefault } from 'app/helpers';

// Types
import {
  StatementMonthListArg,
  StatementMonthListPayload,
  StatementTransactionStateResult
} from '../types';

// Constants
import { STATEMENT_MONTH_DEFAULT } from '../constants';

// Services
import { statementTransactionServices } from '../api-services';
import { FormatTime } from 'app/constants/enums';

export const getStatementMonthList = createAsyncThunk<
  StatementMonthListPayload,
  StatementMonthListArg,
  ThunkAPIConfig
>('statementTransaction/getStatementMonthList', async (args, thunkAPI) => {
  try {
    const { accId } = args.postData;
    const data = await statementTransactionServices.getStatementMonthList({
      accId
    });
    return data;
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const getStatementMonthListBuilder = (
  builder: ActionReducerMapBuilder<StatementTransactionStateResult>
) => {
  builder
    .addCase(getStatementMonthList.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        monthList: {
          isLoading: true
        }
      };
    })
    .addCase(getStatementMonthList.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data = {} } = action.payload;
      const monthList = (data.statement || []).map(({ statementDate }) => {
        return {
          code: statementDate,
          text:
            formatTimeDefault(statementDate, FormatTime.Date, 'YYYYMMDD') || ''
        };
      });
      draftState[storeId] = {
        ...draftState[storeId],
        monthList: {
          isLoading: false,
          data: [STATEMENT_MONTH_DEFAULT, ...monthList]
        }
      };
    })
    .addCase(getStatementMonthList.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        monthList: {
          isLoading: false
        }
      };
    });
};
