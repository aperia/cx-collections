import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  getAccountStatementHistory,
  getAccountStatementHistoryBuilder
} from './getAccountStatementHistory';
import { getOverview, getOverviewBuilder } from './getOverview';
import {
  getStatementMonthList,
  getStatementMonthListBuilder
} from './getStatementMonthList';

import {
  getStatementListTransaction,
  getStatementListTransactionBuilder
} from './getStatementTransaction';

import {
  adjustTransactionRequest,
  adjustTransactionRequestBuilder
} from './adjustTransaction';

import {
  getAdjustTransactionType,
  getAdjustTransactionTypeBuilder
} from './getAdjustTransactionType';

// helpers
import { sortByGrid } from '../helpers';

// types
import {
  DetailModal,
  StatementTransactionInitialState,
  SortByTransactionPayload,
  UpdatePostData,
  DataDetailModal
} from '../types';

const { actions, reducer } = createSlice({
  name: 'statementTransactionList',
  initialState: {} as StatementTransactionInitialState,
  reducers: {
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    sortByTransaction: (
      draftState,
      action: PayloadAction<SortByTransactionPayload>
    ) => {
      const { storeId, sortType } = action.payload;
      const gridData = sortByGrid(sortType, draftState[storeId].data!);
      draftState[storeId] = {
        ...draftState[storeId],
        data: gridData,
        sortType
      };
      return;
    },
    setDataDetail: (draftState, action: PayloadAction<DataDetailModal>) => {
      const { storeId, data } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        detail: { ...draftState[storeId].detail, data }
      };
    },
    toggleDetailModal: (draftState, action: PayloadAction<DetailModal>) => {
      const { storeId } = action.payload;
      const isOpenModal = !!draftState[storeId]?.detail?.isOpen;

      draftState[storeId] = {
        ...draftState[storeId],
        detail: {
          ...draftState[storeId].detail,
          isOpen: !isOpenModal
        }
      };
    },
    toggleAdjustModal: (draftState, action: PayloadAction<DetailModal>) => {
      const { storeId, transactionCode } = action.payload;
      const isOpenModal = !!draftState[storeId]?.adjust?.isOpen;
      const prevTransactionCode = draftState[storeId]?.adjust?.transactionCode;

      draftState[storeId] = {
        ...draftState[storeId],
        adjust: {
          ...draftState[storeId].adjust,
          isOpen: !isOpenModal,
          transactionCode: transactionCode || prevTransactionCode
        }
      };
    },
    updatePostData: (draftState, action: PayloadAction<UpdatePostData>) => {
      const { storeId, postData } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        postData: { ...draftState[storeId].postData!, ...postData }
      };
      return;
    },
    updateIsReset: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isReset: true,
        isFirstCall: false
      };
      return;
    }
  },
  extraReducers: builder => {
    getAdjustTransactionTypeBuilder(builder);
    getOverviewBuilder(builder);
    getStatementListTransactionBuilder(builder);
    getStatementMonthListBuilder(builder);
    getAccountStatementHistoryBuilder(builder);
    adjustTransactionRequestBuilder(builder);
  }
});

const statementTransactionAction = {
  ...actions,
  getAdjustTransactionType,
  getStatementMonthList,
  getStatementListTransaction,
  getOverview,
  getAccountStatementHistory,
  adjustTransactionRequest
};

export { statementTransactionAction, reducer };
