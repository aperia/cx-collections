import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';
import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';
// actions
// types
import {
  IAdjustmentCode,
  IPayloadGetAdjustmentCode,
  StatementTransactionInitialState
} from '../types';

export const getAdjustTransactionType = createAsyncThunk<
  IPayloadGetAdjustmentCode,
  StoreIdPayload,
  ThunkAPIConfig
>('transaction/getAdjustTransactionType', async (args, thunkAPI) => {
  const { dispatch, rejectWithValue } = thunkAPI;
  const { storeId } = args;

  const response = await dispatch(
    accountDetailActions.getClientInfoData({ storeId, dataType: 'adjustments' })
  );
  if (isFulfilled(response)) {
    const adjustmentCodes = response.payload.adjustments
      ?.adjustmentCodes as IAdjustmentCode[];
    return { adjustmentCodes };
  }
  return rejectWithValue({});
});

export const getAdjustTransactionTypeBuilder = (
  builder: ActionReducerMapBuilder<StatementTransactionInitialState>
) => {
  builder
    .addCase(getAdjustTransactionType.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        adjust: {
          ...draftState[storeId].adjust,
          isLoading: true
        }
      };
    })
    .addCase(getAdjustTransactionType.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { adjustmentCodes } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        adjust: {
          ...draftState[storeId].adjust,
          isLoading: false,
          adjustmentCodes
        }
      };
    })
    .addCase(getAdjustTransactionType.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        adjust: {
          ...draftState[storeId].adjust,
          isLoading: false
        }
      };
    });
};
