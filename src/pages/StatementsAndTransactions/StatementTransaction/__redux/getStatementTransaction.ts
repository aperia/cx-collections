// Redux
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Helpers
import { sortByGrid } from '../helpers';
import isEmpty from 'lodash.isempty';
import isUndefined from 'lodash.isundefined';

// Types
import {
  StatementTransactionStateResult,
  StatementTransactionArg,
  StatementTransactionPayload
} from '../types';

// Constants
import {
  PAGINATION_TRANSACTION,
  SORT_STATEMENT_TRANSACTION
} from '../constants';

// Services
import { statementTransactionServices } from '../api-services';

export const getStatementListTransaction = createAsyncThunk<
  StatementTransactionPayload,
  StatementTransactionArg,
  ThunkAPIConfig
>('statementTransaction/getTransactionList', async (args, thunkAPI) => {
  try {
    const { postData } = args;

    const response = await statementTransactionServices.getTransactionList(
      postData
    );
    return {
      data: response?.data?.transaction || []
    } as StatementTransactionPayload;
  } catch (error) {
    return thunkAPI.rejectWithValue({ errorMessage: `${error?.status || error?.response?.status}` });
  }
});

export const getStatementListTransactionBuilder = (
  builder: ActionReducerMapBuilder<StatementTransactionStateResult>
) => {
  builder
    .addCase(getStatementListTransaction.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true
      };
    })
    .addCase(getStatementListTransaction.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;
      const isReset = draftState[storeId]?.isReset;
      const isFirstCall = draftState[storeId]?.isFirstCall;

      if (!isFirstCall) {
        draftState[storeId] = {
          ...draftState[storeId],
          isFirstCall: true,
          noTransaction: isEmpty(data),
          error: false
        };
      }

      if (isReset) {
        const sortedData = sortByGrid(SORT_STATEMENT_TRANSACTION[0], data);
        draftState[storeId] = {
          ...draftState[storeId],
          isLoading: false,
          sortType: SORT_STATEMENT_TRANSACTION[0],
          data: sortedData,
          isLoadMore: data.length === 25 ? true : false,
          isReset: false,
          error: false
        };
        return;
      }

      const dataGrid = draftState[storeId]?.data
        ? [...data, ...draftState[storeId].data!]
        : data;

      if (isUndefined(draftState[storeId].sortType)) {
        const newSortedData = sortByGrid(
          SORT_STATEMENT_TRANSACTION[0],
          dataGrid
        );
        draftState[storeId] = {
          ...draftState[storeId],
          isLoading: false,
          data: newSortedData,
          isLoadMore: data.length === 25 ? true : false,
          error: false
        };
        return;
      }

      const dataSort = sortByGrid(draftState[storeId].sortType!, dataGrid);

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        data: dataSort,
        isLoadMore: data.length === 25 ? true : false,
        error: false
      };
    })
    .addCase(getStatementListTransaction.rejected, (draftState, action) => {
      const status = action?.payload?.errorMessage;
      const { storeId, postData } = action.meta.arg;
      const noData =
        status === '455' &&
        postData?.startSequence === PAGINATION_TRANSACTION.START_SEQUENCE;
      draftState[storeId] = {
        ...draftState[storeId],
        noTransaction: noData,
        isLoadMore: false,
        isLoading: false,
        error: status !== '455'
      };
    });
};
