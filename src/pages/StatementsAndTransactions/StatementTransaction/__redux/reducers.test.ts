import { reducer, statementTransactionAction } from './reducers';
import { StatementTransactionInitialState } from '../types';
import { ID_SORT_GRID } from '../constants';

const storeId = '123456789';

const initStateData: StatementTransactionInitialState = {
  [storeId]: {
    isFirstCall: true,
    isLoading: false,
    isLoadMore: true,
    isReset: false,
    noTransaction: false,
    postData: {
      endSequence: '25',
      isCycleToDate: true,
      startSequence: '0',
      statementDate: 'cyc'
    },
    data: [
      {
        aggregateTransactionAmount: 529,
        descriptionOfTransaction: 'LIONS EYE HOSPITAL',
        postDate: '2020-08-28T16:58:51.000Z',
        transactionDate: '2020-08-28T16:58:51.000Z'
      },
      {
        aggregateTransactionAmount: 226,
        descriptionOfTransaction: 'WANDER TRAILS SERVICES',
        postDate: '2020-08-28T12:17:12.000Z',
        transactionDate: '2020-08-28T12:17:12.000Z'
      }
    ],
    detail: {
      isOpen: false,
      transactionCode: '1234'
    },
    adjust: {
      isOpen: false,
      transactionCode: '1234'
    }
  }
};

describe('Test reducer transaction statement', () => {
  it('remove store action', () => {
    const state = reducer(
      initStateData,
      statementTransactionAction.removeStore({ storeId })
    );

    expect(state).toEqual({});
  });

  it('sort by transaction action', () => {
    const state = reducer(
      initStateData,
      statementTransactionAction.sortByTransaction({
        storeId,
        sortType: { id: ID_SORT_GRID.AMOUNT, order: 'asc' }
      })
    );

    expect(state[storeId].sortType).toEqual({
      id: ID_SORT_GRID.AMOUNT,
      order: 'asc'
    });
  });

  it('update post data action', () => {
    const state = reducer(
      initStateData,
      statementTransactionAction.updatePostData({
        storeId,
        postData: {
          startSequence: '25',
          endSequence: '50',
          isCycleToDate: true,
          statementDate: 'jan'
        }
      })
    );

    expect(state[storeId].postData).toEqual({
      startSequence: '25',
      endSequence: '50',
      isCycleToDate: true,
      statementDate: 'jan'
    });
  });

  it('update is reset action', () => {
    const state = reducer(
      initStateData,
      statementTransactionAction.updateIsReset({ storeId })
    );

    expect(state[storeId].isReset).toEqual(true);
  });

  it('toggle detail modal', () => {
    const state = reducer(
      initStateData,
      statementTransactionAction.toggleDetailModal({ storeId })
    );

    expect(state[storeId].detail).toEqual({
      isOpen: true,
      transactionCode: '1234'
    });
  });

  it('setDataDetail', () => {
    const state = reducer(
      initStateData,
      statementTransactionAction.setDataDetail({
        storeId,
        data: { authorizationDate: 'true' }
      })
    );

    expect(state[storeId].detail?.data).toEqual({ authorizationDate: 'true' });
  });
  it('toggleAdjustModal', () => {
    const state = reducer(
      initStateData,
      statementTransactionAction.toggleAdjustModal({
        storeId
      })
    );

    expect(state[storeId].adjust).toEqual({
      isOpen: true,
      transactionCode: '1234'
    });
  });
});
