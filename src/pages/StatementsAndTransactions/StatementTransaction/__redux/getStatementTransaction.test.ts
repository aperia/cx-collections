import { POST_DATA_DEFAULT } from './../constants';
import { accEValue, responseDefault, storeId } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { statementTransactionServices } from '../api-services';
import { getStatementListTransaction } from './getStatementTransaction';

describe('Test getStatementListTransaction async action', () => {
  it('fulfilled with first call', async () => {
    const store = createStore(rootReducer, {
      statementTransactionList: {
        [storeId]: {
          isFirstCall: true,
          data: [],
          detail: {
            data: {}
          },
          adjust: {}
        }
      },
      form: {
        [`${storeId}_transaction-adjustment`]: {
          values: {
            type: {
              adjustmentBatchCode: '123',
              adjustmentTranCode: '123',
              adjustmentDescription: '1',
              amount: '123'
            }
          }
        }
      },
      mapping: {
        data: {}
      }
    });

    jest
      .spyOn(statementTransactionServices, 'getTransactionList')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          transaction: [
            1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3,
            4, 5
          ]
        }
      });

    const args = {
      storeId,
      postData: {
        accId: accEValue,
        ...POST_DATA_DEFAULT
      }
    };
    const result = await getStatementListTransaction(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'statementTransaction/getTransactionList/fulfilled'
    );
  });

  it('fulfilled with first call with no data', async () => {
    const store = createStore(rootReducer, {
      statementTransactionList: {
        [storeId]: {
          isFirstCall: true,
          detail: {
            data: {}
          },
          adjust: {}
        }
      },
      mapping: {
        data: {}
      }
    });

    jest
      .spyOn(statementTransactionServices, 'getTransactionList')
      .mockResolvedValue({
        ...responseDefault,
        data: []
      });

    const args = {
      storeId,
      postData: {
        accId: accEValue,
        ...POST_DATA_DEFAULT
      }
    };
    const result = await getStatementListTransaction(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'statementTransaction/getTransactionList/fulfilled'
    );
  });

  it('fulfilled with second call, reset', async () => {
    const store = createStore(rootReducer, {
      statementTransactionList: {
        [storeId]: {
          isFirstCall: false,
          isReset: true,
          detail: {
            data: {}
          },
          adjust: {}
        }
      }
    });

    jest
      .spyOn(statementTransactionServices, 'getTransactionList')
      .mockResolvedValue({
        ...responseDefault,
        data: undefined
      });

    const args = {
      storeId,
      postData: {
        accId: accEValue,
        ...POST_DATA_DEFAULT
      }
    };
    const result = await getStatementListTransaction(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'statementTransaction/getTransactionList/fulfilled'
    );
  });

  it('fulfilled with case reset with data === 25 records', async () => {
    const store = createStore(rootReducer, {
      statementTransactionList: {
        [storeId]: {
          isFirstCall: false,
          isReset: true,
          detail: {
            data: {}
          },
          adjust: {}
        }
      }
    });

    jest
      .spyOn(statementTransactionServices, 'getTransactionList')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          transaction: [
            1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3,
            4, 5
          ]
        }
      });

    const args = {
      storeId,
      postData: {
        accId: accEValue,
        ...POST_DATA_DEFAULT
      }
    };
    const result = await getStatementListTransaction(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'statementTransaction/getTransactionList/fulfilled'
    );
  });

  it('fulfilled with first call with sort type', async () => {
    const store = createStore(rootReducer, {
      statementTransactionList: {
        [storeId]: {
          isFirstCall: true,
          data: [],
          sortType: {
            id: '1',
            order: 'desc'
          },
          detail: {
            data: {}
          },
          adjust: {}
        }
      },

      mapping: {
        data: {}
      }
    });

    jest
      .spyOn(statementTransactionServices, 'getTransactionList')
      .mockResolvedValue({
        ...responseDefault,
        data: []
      });

    const args = {
      storeId,
      postData: {
        accId: accEValue,
        ...POST_DATA_DEFAULT
      }
    };
    const result = await getStatementListTransaction(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'statementTransaction/getTransactionList/fulfilled'
    );
  });

  it('fulfilled with first call with sort type with 25 records data', async () => {
    const store = createStore(rootReducer, {
      statementTransactionList: {
        [storeId]: {
          isFirstCall: true,
          data: [],
          sortType: {
            id: '1',
            order: 'desc'
          },
          detail: {
            data: {}
          },
          adjust: {}
        }
      },

      mapping: {
        data: {}
      }
    });

    jest
      .spyOn(statementTransactionServices, 'getTransactionList')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          transaction: [
            1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3,
            4, 5
          ]
        }
      });

    const args = {
      storeId,
      postData: {
        accId: accEValue,
        ...POST_DATA_DEFAULT
      }
    };
    const result = await getStatementListTransaction(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'statementTransaction/getTransactionList/fulfilled'
    );
  });

  it('rejected', async () => {
    const store = createStore(rootReducer, {
      statementTransactionList: {
        [storeId]: {
          detail: {
            data: {}
          },
          adjust: {}
        }
      },

      mapping: {
        data: {}
      }
    });

    jest
      .spyOn(statementTransactionServices, 'getTransactionList')
      .mockRejectedValue({
        ...responseDefault,
        status: '455'
      });

    const args = {
      storeId,
      postData: {
        accId: accEValue,
        ...POST_DATA_DEFAULT
      }
    };
    const result = await getStatementListTransaction(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'statementTransaction/getTransactionList/rejected'
    );
  });
});
