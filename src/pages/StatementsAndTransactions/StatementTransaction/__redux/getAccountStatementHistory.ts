// Redux
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Helpers
import { formatStatementHistoryData } from '../helpers';

// Types
import {
  AccountStatementHistoryArg,
  AccountStatementHistoryPayload,
  StatementTransactionInitialState
} from '../types';

// Services
import { statementTransactionServices } from '../api-services';

export const getAccountStatementHistory = createAsyncThunk<
  AccountStatementHistoryPayload,
  AccountStatementHistoryArg,
  ThunkAPIConfig
>('statementTransaction/getAccountStatementHistory', async (args, thunkAPI) => {
  try {
    const { accId } = args.postData;
    const data = await statementTransactionServices.getAccountStatementHistory({
      accId
    });
    return { data: data.data?.cardholderStatementHistory };
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const getAccountStatementHistoryBuilder = (
  builder: ActionReducerMapBuilder<StatementTransactionInitialState>
) => {
  builder
    .addCase(getAccountStatementHistory.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        statementHistory: {
          isLoading: true
        }
      };
    })
    .addCase(getAccountStatementHistory.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;
      const dataGrid = formatStatementHistoryData(data!) as any;

      draftState[storeId] = {
        ...draftState[storeId],
        statementHistory: {
          isLoading: false,
          data: dataGrid
        }
      };
    })
    .addCase(getAccountStatementHistory.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const status = (action?.payload as MagicKeyValue)?.status + '';
      const noData = status == '455';
      draftState[storeId] = {
        ...draftState[storeId],
        statementHistory: {
          isLoading: false,
          noData
        }
      };
    });
};
