import { rootReducer } from 'storeConfig';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';

import { getAdjustTransactionType } from './getAdjustTransactionType';

import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';
import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  storeId
} from 'app/test-utils';

const mockData = { storeId, postData: { accId: 'accountId' } };

const initialState: Partial<RootState> = {
  mapping: { loading: false, data: { transactionDetail: {} } },
  statementTransactionList: {
    [storeId]: {
      adjust: {}
    }
  }
};

const store = createStore(rootReducer, initialState, applyMiddleware(thunk));

const mockAccountDetail = mockActionCreator(accountDetailActions);

describe('getAdjustTransactionType', () => {
  it('isFulfilled', async () => {
    const mockAction = mockAccountDetail('getClientInfoData');

    const response = await getAdjustTransactionType(mockData)(
      byPassFulfilled(getAdjustTransactionType.fulfilled.type, {
        adjustments: { adjustmentCodes: [{ active: true }] }
      }),
      store.getState,
      {}
    );

    expect(mockAction).toBeCalledWith({ storeId, dataType: 'adjustments' });
    expect(response.payload).toEqual({ adjustmentCodes: [{ active: true }] });
  });

  it('isRejected', async () => {
    const mockAction = mockAccountDetail('getClientInfoData');

    const response = await getAdjustTransactionType(mockData)(
      byPassRejected(getAdjustTransactionType.fulfilled.type),
      store.getState,
      {}
    );

    expect(mockAction).toBeCalledWith({ storeId, dataType: 'adjustments' });
    expect(response.payload).toEqual({});
  });

  it('pending', async () => {
    const action = getAdjustTransactionType.pending('', { storeId });
    const state = rootReducer(store.getState(), action)
      .statementTransactionList[storeId];

    expect(state.adjust!.isLoading).toEqual(true);
  });

  it('fulfilled', async () => {
    const action = getAdjustTransactionType.fulfilled(
      {
        adjustmentCodes: [
          {
            active: true,
            adjustmentBatchCode: 'adjustmentBatchCode',
            adjustmentDescription: 'adjustmentDescription',
            adjustmentTranCode: 'adjustmentTranCode',
            originalTranCode: 'originalTranCode'
          }
        ]
      },
      '',
      { storeId }
    );
    const state = rootReducer(store.getState(), action)
      .statementTransactionList[storeId];

    expect(state.adjust!.isLoading).toEqual(false);
    expect(state.adjust!.adjustmentCodes).toEqual([
      {
        active: true,
        adjustmentBatchCode: 'adjustmentBatchCode',
        adjustmentDescription: 'adjustmentDescription',
        adjustmentTranCode: 'adjustmentTranCode',
        originalTranCode: 'originalTranCode'
      }
    ]);
  });

  it('rejected', async () => {
    const action = getAdjustTransactionType.rejected(
      { name: 'name', message: 'message' },
      '',
      { storeId }
    );
    const state = rootReducer(store.getState(), action)
      .statementTransactionList[storeId];

    expect(state.adjust!.isLoading).toEqual(false);
  });
});
