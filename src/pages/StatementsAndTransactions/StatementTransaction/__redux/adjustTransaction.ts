import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';
// actions
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { memoActions } from 'pages/Memos/_redux/reducers';
// services
import { statementTransactionServices } from '../api-services';
// helpers
import { formatCommon, formatTimeDefault } from 'app/helpers';
// types
import { MONETARY_ADJUSTMENT_ACTIONS } from 'pages/Memos/constants';
import {
  StatementTransactionInitialState,
  AdjustTransactionRequestArgs,
  IDataRequestAdjustmentTransaction
} from '../types';
import { FormatTime } from 'app/constants/enums';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { CHRONICLE_TRANSACTION_ADJUSTMENT_KEY } from 'pages/ClientConfiguration/Memos/constants';

export const adjustTransactionRequest = createAsyncThunk<
  unknown,
  AdjustTransactionRequestArgs,
  ThunkAPIConfig
>('transaction/adjustTransactionRequest', async (args, thunkAPI) => {
  const { operatorID = '' } = window.appConfig?.commonConfig || {};
  const { dispatch, getState } = thunkAPI;
  const { form, statementTransactionList, accountDetail } = getState();
  const { storeId, accEValue } = args;
  try {
    const { values } = form[`${storeId}_transaction-adjustment`];
    const originalTransaction = statementTransactionList[storeId].detail?.data;
    const postingDate =
      statementTransactionList[storeId].detail?.data!.postDate;
    const transactionDate =
      statementTransactionList[storeId].detail?.data!.transactionDate;
    const descriptionOfTransaction = statementTransactionList[storeId].detail
      ?.data?.descriptionOfTransaction as string;
    const requestData = {
      common: {
        user: 'string',
        cycle: 'M',
        accountId: accEValue
      },
      batchType: '05',
      batchPrefix: values!.type.adjustmentBatchCode,
      transactionCode: values?.type?.adjustmentTranCode,
      transactionAmount:
        originalTransaction?.aggregateTransactionAmount &&
        Number(originalTransaction?.aggregateTransactionAmount).toFixed(2),
      adjustmentAmount: Number(values!.amount).toFixed(2),
      transactionPostingDate: formatTimeDefault(
        postingDate!,
        FormatTime.DefaultShortPostDate
      ),
      monetaryRejectOverrideIndicator: 'Y'
    } as IDataRequestAdjustmentTransaction;

    await statementTransactionServices.adjustTransaction({
      ...requestData
    });
    const type = `${values!.type.adjustmentTranCode} - ${
      values!.type.adjustmentDescription
    }`;
    const amount = formatCommon(values!.amount).currency(2);
    const transactionCode = originalTransaction?.transactionCode || '';
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'txt_adjust_transaction_success_alert',
          msgVariables: {
            type,
            amount,
            transactionCode
          }
        })
      );
      dispatch(
        memoActions.postMemo(MONETARY_ADJUSTMENT_ACTIONS.ADJUST_TRANSACTION, {
          storeId,
          accEValue,
          chronicleKey: CHRONICLE_TRANSACTION_ADJUSTMENT_KEY,
          cardholderContactedName: accountDetail[storeId]?.data?.primaryName,
          operatorId: operatorID,
          dollarAmount1: formatCommon(
            originalTransaction?.aggregateTransactionAmount || ''
          ).currency(2),
          dollarAmount2: amount,
          batchCd: values!.type.adjustmentBatchCode,
          transactionCode,
          adjTranCd: type,
          reason: values?.reason?.description || '',
          transactionDate,
          postDate: postingDate,
          descriptionOfTransaction: descriptionOfTransaction?.trim()
        })
      );

      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inModalBody'
        })
      );
    });
  } catch (error) {
    const { values } = form[`${storeId}_transaction-adjustment`];
    const postingDate =
      statementTransactionList[storeId].detail?.data!.postDate;
    const transactionDate =
      statementTransactionList[storeId].detail?.data?.transactionDate;
    const descriptionOfTransaction =
      statementTransactionList[
        storeId
      ].detail?.data?.descriptionOfTransaction?.toString() || '';
    const type = `${values!.type.adjustmentTranCode} - ${
      values!.type.adjustmentDescription
    }`;
    const amount = formatCommon(values!.amount).currency(2);
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: 'txt_adjust_transaction_failed_alert'
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inModalBody',
          apiResponse: error
        })
      );
      dispatch(
        memoActions.postMemo(
          MONETARY_ADJUSTMENT_ACTIONS.ADJUST_FAIL_TRANSACTION,
          {
            storeId,
            accEValue,
            chronicleKey: CHRONICLE_TRANSACTION_ADJUSTMENT_KEY,
            cardholderContactedName: accountDetail[storeId]?.data?.primaryName,
            operatorId: operatorID,
            dollarAmount2: amount,
            batchCd: values?.type?.adjustmentBatchCode,
            transactionCode: values?.type?.adjustmentTranCode,
            adjTranCd: type,
            reason: values?.reason?.description || '',
            transactionDate: transactionDate,
            postDate: postingDate,
            descriptionOfTransaction: descriptionOfTransaction.trim()
          }
        )
      );
    });
    return thunkAPI.rejectWithValue(error);
  }
});

export const adjustTransactionRequestBuilder = (
  builder: ActionReducerMapBuilder<StatementTransactionInitialState>
) => {
  builder
    .addCase(adjustTransactionRequest.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        adjust: {
          ...draftState[storeId].adjust,
          isLoading: true
        }
      };
    })
    .addCase(adjustTransactionRequest.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        adjust: {
          ...draftState[storeId].adjust,
          isLoading: false,
          isOpen: false
        }
      };
    })
    .addCase(adjustTransactionRequest.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        adjust: {
          ...draftState[storeId].adjust,
          isLoading: false
        }
      };
    });
};
