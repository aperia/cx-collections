import { memoActions } from 'pages/Memos/_redux/reducers';
import {
  responseDefault,
  storeId,
  accNbr,
  accEValue,
  mockActionCreator
} from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { statementTransactionServices } from '../api-services';
import { adjustTransactionRequest } from './adjustTransaction';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

describe('Test adjustTransactionRequest async action', () => {
  it('fulfilled', async () => {
    const mockActionToast = mockActionCreator(actionsToast);
    const mockAddToast = mockActionToast('addToast');
    const mockMemoToast = mockActionCreator(memoActions);
    const mockPostMemo = mockMemoToast('postMemo');
    const store = createStore(rootReducer, {
      statementTransactionList: {
        [storeId]: {
          detail: {
            data: {}
          },
          adjust: {}
        }
      },
      form: {
        [`${storeId}_transaction-adjustment`]: {
          values: {
            type: {
              adjustmentBatchCode: '123',
              adjustmentTranCode: '123',
              adjustmentDescription: '1',
              amount: '123'
            }
          }
        }
      }
    });

    jest
      .spyOn(statementTransactionServices, 'adjustTransaction')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          adjustTransaction: {
            deceasedPartyName: 'PRIMARY'
          }
        }
      });

    const args = {
      storeId,
      accEValue,
      accNbr
    };
    const result = await adjustTransactionRequest(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'transaction/adjustTransactionRequest/fulfilled'
    );
    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: 'txt_adjust_transaction_success_alert',
      msgVariables: {
        type: '123 - 1',
        transactionCode: '',
        amount: '$0.00'
      }
    });
    expect(mockPostMemo).toBeCalled();
  });

  it('rejected', async () => {
    const mockActionToast = mockActionCreator(actionsToast);
    const mockApiErrorNoti = mockActionCreator(apiErrorNotificationAction);
    const mockMemoToast = mockActionCreator(memoActions);
    const mockAddToast = mockActionToast('addToast');
    mockApiErrorNoti('updateApiError');
    mockMemoToast('postMemo');

    const store = createStore(rootReducer, {
      statementTransactionList: {
        [storeId]: {
          detail: {
            data: { aggregateTransactionAmount: 'text' }
          },
          adjust: {}
        }
      },
      form: {
        [`${storeId}_transaction-adjustment`]: {
          values: {
            type: {
              adjustmentBatchCode: '123',
              adjustmentTranCode: '123',
              amount: '123'
            }
          }
        }
      }
    });

    jest
      .spyOn(statementTransactionServices, 'adjustTransaction')
      .mockRejectedValue({
        ...responseDefault
      });

    const args = {
      storeId,
      accEValue,
      accNbr
    };
    const result = await adjustTransactionRequest(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'transaction/adjustTransactionRequest/rejected'
    );
    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: 'txt_adjust_transaction_failed_alert'
    });
  });
});
