import { accEValue, responseDefault, storeId } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { statementTransactionServices } from '../api-services';
import { getOverview } from './getOverview';

describe('Test getOverview async action', () => {
  it('fulfilled', async () => {
    const store = createStore(rootReducer, {
      statementTransactionList: {
        [storeId]: {
          detail: {
            data: {}
          },
          adjust: {}
        }
      },
      form: {
        [`${storeId}_transaction-adjustment`]: {
          values: {
            type: {
              adjustmentBatchCode: '123',
              adjustmentTranCode: '123',
              adjustmentDescription: '1',
              amount: '123'
            }
          }
        }
      },
      mapping: {
        data: {}
      }
    });

    jest
      .spyOn(statementTransactionServices, 'getStatementOverview')
      .mockResolvedValue({
        ...responseDefault,
        data: { customers: [{}] }
      });

    const args = {
      storeId,
      postData: {
        accountId: accEValue,
        statementDate: ''
      }
    };
    const result = await getOverview(args)(store.dispatch, store.getState, {});

    expect(result.type).toEqual(
      'statementTransaction/getMonthlyStatementSummary/fulfilled'
    );
  });

  it('rejected', async () => {
    const store = createStore(rootReducer, {
      statementTransactionList: {
        [storeId]: {
          detail: {
            data: {}
          },
          adjust: {}
        }
      },
      form: {
        [`${storeId}_transaction-adjustment`]: {
          values: {
            type: {
              adjustmentBatchCode: '123',
              adjustmentTranCode: '123',
              amount: '123'
            }
          }
        }
      },
      mapping: {
        data: {}
      }
    });

    jest
      .spyOn(statementTransactionServices, 'getStatementOverview')
      .mockRejectedValue({
        ...responseDefault
      });

    const args = {
      storeId,
      postData: {
        accountId: accEValue,
        statementDate: ''
      }
    };
    const result = await getOverview(args)(store.dispatch, store.getState, {});

    expect(result.type).toEqual(
      'statementTransaction/getMonthlyStatementSummary/rejected'
    );
  });
});
