import { useSelectorMock } from 'app/test-utils/useSelectorMock';
import { selectorWrapper } from 'app/test-utils/selectorWrapper';
import * as makeSelector from './selectors';
import { StatementTransactionInitialState } from '../types';

const storeId = '123456789';
const mockData = [
  {
    aPRMerchant: '96.00%',
    adjustmentAmount: '$121.00',
    adjustmentCount: '005',
    aprCash: '33.00%',
    availAbleCredit: '$98.00',
    behavior: 58,
    bureauScore: 16,
    cashAmount: '$578.40',
    cashBalance: '$5.00',
    cashBalanceADB: '$41.00',
    cashCount: '000',
    creditAmount: '$213.85',
    creditCount: 6,
    creditLimit: '$143.49',
    dueDate: '08/14/2020',
    financeChargeC: '$42.00',
    financeChargeP: '$40.00',
    lateFees: '$5.00',
    minPayDue: '$377.08',
    newBalance: '$891.77',
    overLimitFees: '$8.00',
    pastDue: '$678.93',
    paymentAmount: '$24.00',
    paymentCount: '005',
    purchaseAmount: '$30.00',
    purchaseBalanceADB: '$81.00',
    purchaseCount: 396,
    statementDate: '05/11/2020'
  }
];

const mockDataTransaction = [
  {
    aggregateTransactionAmount: 529,
    descriptionOfTransaction: 'LIONS EYE HOSPITAL',
    postDate: '2020-08-28T16:58:51.000Z',
    transactionDate: '2020-08-28T16:58:51.000Z'
  },
  {
    aggregateTransactionAmount: 226,
    descriptionOfTransaction: 'WANDER TRAILS SERVICES',
    postDate: '2020-08-28T12:17:12.000Z',
    transactionDate: '2020-08-28T12:17:12.000Z'
  }
];

const initStateData: StatementTransactionInitialState = {
  [storeId]: {
    isFirstCall: true,
    isLoading: false,
    isLoadMore: true,
    isReset: false,
    noTransaction: false,
    error: true,
    postData: {
      endSequence: '25',
      isCycleToDate: true,
      startSequence: '0',
      statementDate: 'cyc'
    },
    data: mockDataTransaction,
    detail: {
      isOpen: false,
      transactionCode: '1234',
      data: {
        authorizationDate: '1'
      }
    },
    monthList: {
      isLoading: false,
      data: [
        { code: 'cyc', text: 'txt_cycle_to_date' },
        { code: 'jan', text: '01/28/2020' }
      ]
    },
    statementHistory: {
      isLoading: false,
      data: mockData,
      noData: true
    },
    adjust: {
      data: {
        authorizationDate: '2'
      },
      isLoading: true,
      isOpen: true,
      adjustmentCodes: undefined
    }
  }
};

const initNoData: StatementTransactionInitialState = {
  [storeId]: {}
};

describe('Test selector state', () => {
  it('select data account statement month', () => {
    const data = useSelectorMock(
      makeSelector.selectDataAccountStatementMonth,
      {
        statementTransactionList: initStateData
      },
      storeId
    );
    const noData = useSelectorMock(
      makeSelector.selectDataAccountStatementMonth,
      {
        statementTransactionList: initNoData
      },
      storeId
    );

    expect(noData).toEqual([{ code: '', text: 'txt_cycle_to_date' }]);
    expect(data).toEqual([
      { code: 'cyc', text: 'txt_cycle_to_date' },
      { code: 'jan', text: '01/28/2020' }
    ]);
  });

  it('select data account history transaction', () => {
    const data = useSelectorMock(
      makeSelector.selectDataStatementHistory,
      {
        statementTransactionList: initStateData
      },
      storeId
    );
    const noData = useSelectorMock(
      makeSelector.selectDataStatementHistory,
      {
        statementTransactionList: initNoData
      },
      storeId
    );

    expect(noData).toEqual([]);
    expect(data).toEqual(mockData);
  });

  it('selectIsLoadingStatementHistory', () => {
    const loading = useSelectorMock(
      makeSelector.selectIsLoadingStatementHistory,
      {
        statementTransactionList: initStateData
      },
      storeId
    );
    const noLoading = useSelectorMock(
      makeSelector.selectIsLoadingStatementHistory,
      {
        statementTransactionList: initNoData
      },
      storeId
    );

    expect(loading).toEqual(false);
    expect(noLoading).toEqual(false);
  });

  it('selectDataStatementTransaction', () => {
    const data = useSelectorMock(
      makeSelector.selectDataStatementTransaction,
      {
        statementTransactionList: initStateData
      },
      storeId
    );
    const noData = useSelectorMock(
      makeSelector.selectDataStatementTransaction,
      {
        statementTransactionList: initNoData
      },
      storeId
    );

    expect(data).toEqual(mockDataTransaction);
    expect(noData).toEqual([]);
  });

  it('selectLoadingStatementTransaction', () => {
    const loading = useSelectorMock(
      makeSelector.selectLoadingStatementTransaction,
      {
        statementTransactionList: initStateData
      },
      storeId
    );
    const noLoading = useSelectorMock(
      makeSelector.selectLoadingStatementTransaction,
      {
        statementTransactionList: initNoData
      },
      storeId
    );

    expect(loading).toEqual(false);
    expect(noLoading).toEqual(false);
  });

  it('selectNoTransaction', () => {
    const transaction = useSelectorMock(
      makeSelector.selectNoTransaction,
      {
        statementTransactionList: initStateData
      },
      storeId
    );

    expect(transaction).toEqual(false);
  });

  it('selectLoadMoreStatementTransaction', () => {
    const loadMore = useSelectorMock(
      makeSelector.selectLoadMoreStatementTransaction,
      {
        statementTransactionList: initStateData
      },
      storeId
    );
    const noLoadMore = useSelectorMock(
      makeSelector.selectLoadMoreStatementTransaction,
      {
        statementTransactionList: initNoData
      },
      storeId
    );

    expect(loadMore).toEqual(true);
    expect(noLoadMore).toEqual(false);
  });

  it('selectIsResetStatementTransaction', () => {
    const isReset = useSelectorMock(
      makeSelector.selectIsResetStatementTransaction,
      {
        statementTransactionList: initStateData
      },
      storeId
    );
    const noIsReset = useSelectorMock(
      makeSelector.selectIsResetStatementTransaction,
      {
        statementTransactionList: initNoData
      },
      storeId
    );

    expect(isReset).toEqual(false);
    expect(noIsReset).toEqual(false);
  });

  it('selectPostDataStatementTransaction', () => {
    const postData = useSelectorMock(
      makeSelector.selectPostDataStatementTransaction,
      {
        statementTransactionList: initStateData
      },
      storeId
    );

    expect(postData).toEqual({
      endSequence: '25',
      isCycleToDate: true,
      startSequence: '0',
      statementDate: 'cyc'
    });
  });

  it('selectSortByStatementTransaction', () => {
    const sortBy = useSelectorMock(
      makeSelector.selectSortByStatementTransaction,
      {
        statementTransactionList: initStateData
      },
      storeId
    );
    const noSortBy = useSelectorMock(
      makeSelector.selectSortByStatementTransaction,
      {
        statementTransactionList: initNoData
      },
      storeId
    );

    expect(sortBy).toEqual({ id: 'transactionDate' });
    expect(noSortBy).toEqual({ id: 'transactionDate' });
  });

  it('selectDetailData', () => {
    const { data, emptyData } = selectorWrapper({
      statementTransactionList: initStateData
    })(makeSelector.selectDetailData);

    expect(data).toEqual({
      authorizationDate: '1'
    });
    expect(emptyData).toEqual({});
  });

  it('selectDetailIsOpen', () => {
    const isOpen = useSelectorMock(
      makeSelector.selectDetailIsOpen,
      {
        statementTransactionList: initStateData
      },
      storeId
    );

    expect(isOpen).toEqual(false);
  });

  it('selectDetailTransactionCode', () => {
    const { data, emptyData } = selectorWrapper({
      statementTransactionList: initStateData
    })(makeSelector.selectDetailTransactionCode);

    expect(data).toEqual('1234');
    expect(emptyData).toEqual('');
  });

  it('selectIsError', () => {
    const { data, emptyData } = selectorWrapper({
      statementTransactionList: initStateData
    })(makeSelector.selectIsError);

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });
  it('selectAdjustData', () => {
    const { data } = selectorWrapper({
      statementTransactionList: initStateData
    })(makeSelector.selectAdjustData);

    expect(data).toEqual({
      authorizationDate: '2'
    });
  });
  it('selectAdjustModalOpen', () => {
    const { data } = selectorWrapper({
      statementTransactionList: initStateData
    })(makeSelector.selectAdjustModalOpen);

    expect(data).toEqual(true);
  });
  it('selectAdjustLoading', () => {
    const { data } = selectorWrapper({
      statementTransactionList: initStateData
    })(makeSelector.selectAdjustLoading);

    expect(data).toEqual(true);
  });
  it('selectAdjustmentCodes', () => {
    const { data, emptyData } = selectorWrapper({
      statementTransactionList: initStateData
    })(makeSelector.selectAdjustmentCodes);

    expect(data).toEqual([]);
    expect(emptyData).toEqual([]);
  });
  it('selectNodataStatementHistory', () => {
    const { data, emptyData } = selectorWrapper({
      statementTransactionList: initStateData
    })(makeSelector.selectNodataStatementHistory);

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });
});
