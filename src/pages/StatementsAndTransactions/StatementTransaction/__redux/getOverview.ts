// Redux
import { AppState } from 'storeConfig';
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Helpers
import { mappingDataFromObj } from 'app/helpers';

// Types
import {
  StatementTransactionStateResult,
  StatementOverviewArg,
  StatementOverviewPayload
} from '../types';

// Services
import { statementTransactionServices } from '../api-services';

export const getOverview = createAsyncThunk<
  StatementOverviewPayload,
  StatementOverviewArg,
  ThunkAPIConfig
>('statementTransaction/getMonthlyStatementSummary', async (args, thunkAPI) => {
  try {
    const { accountId, statementDate } = args.postData;

    const { data } = await statementTransactionServices.getStatementOverview({
      accountId,
      statementDate
    });

    const state = thunkAPI.getState() as AppState;
    const mapping = state.mapping.data.statementSummary || {};

    const mappedData = mappingDataFromObj(data, mapping);

    return { data: mappedData } as StatementOverviewPayload;
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const getOverviewBuilder = (
  builder: ActionReducerMapBuilder<StatementTransactionStateResult>
) => {
  builder
    .addCase(getOverview.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        overview: { ...draftState[storeId]?.overview, isLoading: true }
      };
    })
    .addCase(getOverview.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        overview: { ...draftState[storeId]?.overview, isLoading: false, data }
      };
    })
    .addCase(getOverview.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        overview: { ...draftState[storeId]?.overview, isLoading: false }
      };
    });
};
