import { rootReducer } from 'storeConfig';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';

import { getAccountStatementHistory } from './getAccountStatementHistory';

// Services
import { statementTransactionServices } from '../api-services';
import { responseDefault } from 'app/test-utils';

const mockData = { storeId: 'storeId', postData: { accId: 'accountId' } };

const storeAsync = createStore(
  rootReducer,
  { mapping: { data: { transactionDetail: {} } } } as Partial<RootState>,
  applyMiddleware(thunk)
);

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('Test getStatementMonthList', () => {
  it('Fulfilled action', async () => {
    spy = jest
      .spyOn(statementTransactionServices, 'getAccountStatementHistory')
      .mockResolvedValue({
        ...responseDefault,
        data: { cardholderStatementHistory: [] }
      });

    const response = await getAccountStatementHistory(mockData)(
      storeAsync.dispatch,
      storeAsync.getState,
      {}
    );

    expect(response.payload).toEqual({ data: [] });
  });

  it('reject', async () => {
    spy = jest
      .spyOn(statementTransactionServices, 'getAccountStatementHistory')
      .mockRejectedValue(responseDefault);

    const response = await getAccountStatementHistory(mockData)(
      storeAsync.dispatch,
      storeAsync.getState,
      {}
    );

    expect(response.type).toEqual(getAccountStatementHistory.rejected.type);
  });
});
