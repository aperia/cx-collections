import { accEValue, responseDefault, storeId } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { statementTransactionServices } from '../api-services';
import { getStatementMonthList } from './getStatementMonthList';

describe('Test getStatementMonthList async action', () => {
  it('fulfilled', async () => {
    const store = createStore(rootReducer, {
      statementTransactionList: {
        [storeId]: {
          detail: {
            data: {}
          },
          adjust: {}
        }
      },
      form: {
        [`${storeId}_transaction-adjustment`]: {
          values: {
            type: {
              adjustmentBatchCode: '123',
              adjustmentTranCode: '123',
              adjustmentDescription: '1',
              amount: '123'
            }
          }
        }
      },
      mapping: {
        data: {}
      }
    });

    jest
      .spyOn(statementTransactionServices, 'getStatementMonthList')
      .mockResolvedValue({
        ...responseDefault,
        data: { statement: [{ statementDate: '' }] }
      });

    const args = {
      storeId,
      postData: {
        accId: accEValue
      }
    };
    const result = await getStatementMonthList(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'statementTransaction/getStatementMonthList/fulfilled'
    );
  });

  it('fulfilled with empty payload', async () => {
    const store = createStore(rootReducer, {
      statementTransactionList: {
        [storeId]: {
          detail: {
            data: {}
          },
          adjust: {}
        }
      },
      form: {
        [`${storeId}_transaction-adjustment`]: {
          values: {
            type: {
              adjustmentBatchCode: '123',
              adjustmentTranCode: '123',
              adjustmentDescription: '1',
              amount: '123'
            }
          }
        }
      },
      mapping: {
        data: {}
      }
    });

    jest
      .spyOn(statementTransactionServices, 'getStatementMonthList')
      .mockResolvedValue({
        ...responseDefault,
        data: undefined
      });

    const args = {
      storeId,
      postData: {
        accId: accEValue
      }
    };
    const result = await getStatementMonthList(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'statementTransaction/getStatementMonthList/fulfilled'
    );
  });

  it('fulfilled with empty statement', async () => {
    const store = createStore(rootReducer, {
      statementTransactionList: {
        [storeId]: {
          detail: {
            data: {}
          },
          adjust: {}
        }
      },
      form: {
        [`${storeId}_transaction-adjustment`]: {
          values: {
            type: {
              adjustmentBatchCode: '123',
              adjustmentTranCode: '123',
              adjustmentDescription: '1',
              amount: '123'
            }
          }
        }
      },
      mapping: {
        data: {}
      }
    });

    jest
      .spyOn(statementTransactionServices, 'getStatementMonthList')
      .mockResolvedValue({
        ...responseDefault,
        data: { statement: null }
      });

    const args = {
      storeId,
      postData: {
        accId: accEValue
      }
    };
    const result = await getStatementMonthList(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'statementTransaction/getStatementMonthList/fulfilled'
    );
  });

  it('rejected', async () => {
    const store = createStore(rootReducer, {
      statementTransactionList: {
        [storeId]: {
          detail: {
            data: {}
          },
          adjust: {}
        }
      },
      form: {
        [`${storeId}_transaction-adjustment`]: {
          values: {
            type: {
              adjustmentBatchCode: '123',
              adjustmentTranCode: '123',
              amount: '123'
            }
          }
        }
      },
      mapping: {
        data: {}
      }
    });

    jest
      .spyOn(statementTransactionServices, 'getStatementMonthList')
      .mockRejectedValue({
        ...responseDefault
      });

    const args = {
      storeId,
      postData: {
        accId: accEValue
      }
    };
    const result = await getStatementMonthList(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'statementTransaction/getStatementMonthList/rejected'
    );
  });
});
