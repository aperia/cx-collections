import {
  sortByGrid,
  formatStatementHistoryData,
  formatDataGrid
} from './helpers';
import { ID_SORT_GRID } from './constants';
import { AccountStatementHistoryFormatData } from './types';

const mockTranslate = (value:string) => value;

const sortData = [
  {
    aggregateTransactionAmount: 10,
    descriptionOfTransaction: 'string',
    postDate: '2020, 01, 27',
    transactionDate: '2020, 01, 27',
    transactionCode: 'string'
  },
  {
    aggregateTransactionAmount: 8,
    descriptionOfTransaction: 'string',
    postDate: '2020, 01, 28',
    transactionDate: '2020, 01, 28',
    transactionCode: 'string'
  },
  {
    aggregateTransactionAmount: 11,
    descriptionOfTransaction: 'string',
    postDate: '2020, 01, 26',
    transactionDate: '2020, 01, 26',
    transactionCode: 'string'
  }
];

describe('Test sortByGrid function', () => {
  it('Test sortType incorrect', () => {
    const data = sortByGrid({ id: 'aaa', order: 'asc' }, [
      {
        aggregateTransactionAmount: 10,
        descriptionOfTransaction: 'string',
        postDate: 'string',
        transactionDate: 'string',
        transactionCode: 'string'
      }
    ]);
    expect(data).toEqual([
      {
        aggregateTransactionAmount: 10,
        descriptionOfTransaction: 'string',
        postDate: 'string',
        transactionDate: 'string',
        transactionCode: 'string'
      }
    ]);
  });

  it('Test sortType amount asc', () => {
    const data = sortByGrid(
      { id: ID_SORT_GRID.AMOUNT, order: 'asc' },
      sortData
    );

    expect(data).toEqual([
      {
        aggregateTransactionAmount: 8,
        descriptionOfTransaction: 'string',
        postDate: '2020, 01, 28',
        transactionDate: '2020, 01, 28',
        transactionCode: 'string'
      },
      {
        aggregateTransactionAmount: 10,
        descriptionOfTransaction: 'string',
        postDate: '2020, 01, 27',
        transactionDate: '2020, 01, 27',
        transactionCode: 'string'
      },
      {
        aggregateTransactionAmount: 11,
        descriptionOfTransaction: 'string',
        postDate: '2020, 01, 26',
        transactionDate: '2020, 01, 26',
        transactionCode: 'string'
      }
    ]);
  });

  it('Test sortType amount order undefined or desc', () => {
    const data = sortByGrid(
      { id: ID_SORT_GRID.AMOUNT, order: 'desc' },
      sortData
    );

    const data1 = sortByGrid({ id: ID_SORT_GRID.AMOUNT }, sortData);

    const result = [
      {
        aggregateTransactionAmount: 11,
        descriptionOfTransaction: 'string',
        postDate: '2020, 01, 26',
        transactionDate: '2020, 01, 26',
        transactionCode: 'string'
      },
      {
        aggregateTransactionAmount: 10,
        descriptionOfTransaction: 'string',
        postDate: '2020, 01, 27',
        transactionDate: '2020, 01, 27',
        transactionCode: 'string'
      },
      {
        aggregateTransactionAmount: 8,
        descriptionOfTransaction: 'string',
        postDate: '2020, 01, 28',
        transactionDate: '2020, 01, 28',
        transactionCode: 'string'
      }
    ];

    expect(data).toEqual(result);
    expect(data1).toEqual(result);
  });

  it('Test sortType date order asc', () => {
    const data = sortByGrid(
      { id: ID_SORT_GRID.TRANSACTION_DATE, order: 'asc' },
      sortData
    );

    const data1 = sortByGrid(
      { id: ID_SORT_GRID.POST_DATE, order: 'asc' },
      sortData
    );

    const result = [
      {
        aggregateTransactionAmount: 11,
        descriptionOfTransaction: 'string',
        postDate: '2020, 01, 26',
        transactionDate: '2020, 01, 26',
        transactionCode: 'string'
      },
      {
        aggregateTransactionAmount: 10,
        descriptionOfTransaction: 'string',
        postDate: '2020, 01, 27',
        transactionDate: '2020, 01, 27',
        transactionCode: 'string'
      },
      {
        aggregateTransactionAmount: 8,
        descriptionOfTransaction: 'string',
        postDate: '2020, 01, 28',
        transactionDate: '2020, 01, 28',
        transactionCode: 'string'
      }
    ];

    expect(data).toEqual(result);
    expect(data1).toEqual(result);
  });

  it('Test sortType date order desc or undefined', () => {
    const data = sortByGrid(
      { id: ID_SORT_GRID.TRANSACTION_DATE, order: 'desc' },
      sortData
    );

    const data1 = sortByGrid({ id: ID_SORT_GRID.TRANSACTION_DATE }, sortData);

    const data2 = sortByGrid(
      { id: ID_SORT_GRID.POST_DATE, order: 'desc' },
      sortData
    );

    const data3 = sortByGrid({ id: ID_SORT_GRID.POST_DATE }, sortData);

    const result = [
      {
        aggregateTransactionAmount: 8,
        descriptionOfTransaction: 'string',
        postDate: '2020, 01, 28',
        transactionDate: '2020, 01, 28',
        transactionCode: 'string'
      },
      {
        aggregateTransactionAmount: 10,
        descriptionOfTransaction: 'string',
        postDate: '2020, 01, 27',
        transactionDate: '2020, 01, 27',
        transactionCode: 'string'
      },
      {
        aggregateTransactionAmount: 11,
        descriptionOfTransaction: 'string',
        postDate: '2020, 01, 26',
        transactionDate: '2020, 01, 26',
        transactionCode: 'string'
      }
    ];

    expect(data).toEqual(result);
    expect(data1).toEqual(result);
    expect(data2).toEqual(result);
    expect(data3).toEqual(result);
  });

  it('Test sortType post date, desc, empty data', () => {
    const data = sortByGrid({ id: ID_SORT_GRID.POST_DATE, order: 'desc' }, [
      {
        aggregateTransactionAmount: 10
      },
      {
        aggregateTransactionAmount: 2
      }
    ]);

    expect(data).toEqual([
      {
        aggregateTransactionAmount: 10
      },
      {
        aggregateTransactionAmount: 2
      }
    ]);
  });

  it('Test sortType post date, asc, empty data', () => {
    const data = sortByGrid({ id: ID_SORT_GRID.POST_DATE, order: 'asc' }, [
      {
        aggregateTransactionAmount: 10
      },
      {
        aggregateTransactionAmount: 2
      }
    ]);

    expect(data).toEqual([
      {
        aggregateTransactionAmount: 10
      },
      {
        aggregateTransactionAmount: 2
      }
    ]);
  });
});

describe('Test formatStatementHistoryData function', () => {
  it('Test format data', () => {
    const date = `new Date()`;
    const data = formatStatementHistoryData([
      {
        statementDate: date,
        statementBalanceAmount: 888,
        creditCount: 888,
        totalCreditAmount: 888,
        minimumPaymentDue: 888,
        minimumPaymentDueDate: date,
        pastDueAmount: 888,
        balanceCashAdvances: 888,
        availableCredit: 888,
        creditLimit: 888,
        purchaseCount: 888,
        totalPurchaseAmount: 888,
        cashAdvanceCount: 888,
        totalCashAdvanceAmount: 888,
        paymentsCount: 888,
        totalPaymentAmount: 888,
        adjustmentsCount: 888,
        totalAdjustmentsAmount: 888,
        annualPercentageRateCashAdvances: 888,
        annualPercentageRateMerchandise: 888,
        lateFees: 888,
        overlimitFees: 888,
        financeChargesPurchases: 888,
        financeChargeCashAdvances: 888,
        averageDailyBalanceCashAdvances: 888,
        averageDailyBalancePurchases: 888,
        behaviorScorecardIdentifier: 888,
        beaconScore: 888
      }
    ]);

    expect(data).toEqual([
      {
        statementDate: undefined,
        newBalance: '$888.00',
        minPayDue: '$888.00',
        dueDate: undefined,
        pastDue: '$888.00',
        cashBalance: '$888.00',
        availAbleCredit: '$888.00',
        creditLimit: '$888.00',
        purchaseCount: 888,
        purchaseAmount: '$888.00',
        creditCount: 888,
        creditAmount: '$888.00',
        cashCount: 888,
        cashAmount: '$888.00',
        paymentCount: 888,
        paymentAmount: '$888.00',
        adjustmentCount: 888,
        adjustmentAmount: '$888.00',
        aprCash: '888.00%',
        aPRMerchant: '888.00%',
        lateFees: '$888.00',
        overLimitFees: '$888.00',
        financeChargeP: '$888.00',
        financeChargeC: '$888.00',
        cashBalanceADB: '$888.00',
        purchaseBalanceADB: '$888.00',
        behavior: 888,
        bureauScore: 888
      }
    ]);
  });
});

describe('Test formatDataGrid function', () => {
  it('format data Grid', () => {
    const date = `${new Date()}`;
    const value: AccountStatementHistoryFormatData[] = [
      {
        statementDate: 'string',
        newBalance: 'string',
        minPayDue: 'string',
        dueDate: date,
        pastDue: 'string',
        cashBalance: 'string',
        availAbleCredit: 'string',
        creditLimit: 'string',
        purchaseCount: 9999,
        purchaseAmount: 'string',
        creditCount: 9999,
        creditAmount: 'string',
        cashCount: 'string',
        cashAmount: 'string',
        paymentCount: 'string',
        paymentAmount: 'string',
        adjustmentCount: 'string',
        adjustmentAmount: 'string',
        aprCash: 'string',
        aPRMerchant: 'string',
        lateFees: 'string',
        overLimitFees: 'string',
        financeChargeP: 'string',
        financeChargeC: 'string',
        cashBalanceADB: 'string',
        purchaseBalanceADB: 'string',
        behavior: 9999,
        bureauScore: 9999
      }
    ];
    const formatData = formatDataGrid(value, mockTranslate);
    expect(formatData).toEqual([
      { statementDate: 'txt_new_balance', month_1: 'string' },
      { statementDate: 'txt_credit_count', month_1: 9999 },
      { statementDate: 'txt_min_pay_due', month_1: 'string' },
      {
        statementDate: 'txt_payment_due_date',
        month_1: date
      },
      { statementDate: 'txt_past_due_amount', month_1: 'string' },
      { statementDate: 'txt_cash_balance', month_1: 'string' },
      { statementDate: 'txt_available_credit', month_1: 'string' },
      { statementDate: 'txt_credit_limit', month_1: 'string' },
      { statementDate: 'txt_purchase_count', month_1: 9999 },
      { statementDate: 'txt_purchase_amount', month_1: 'string' },
      { statementDate: 'txt_credit_amount', month_1: 'string' },
      { statementDate: 'txt_cash_count', month_1: 'string' },
      { statementDate: 'txt_cash_amount', month_1: 'string' },
      { statementDate: 'txt_payment_count', month_1: 'string' },
      { statementDate: 'txt_payment_amount', month_1: 'string' },
      { statementDate: 'txt_adjustment_count', month_1: 'string' },
      { statementDate: 'txt_adjustment_amount', month_1: 'string' },
      { statementDate: 'txt_apr_cash', month_1: 'string' },
      { statementDate: 'txt_apr_merchant', month_1: 'string' },
      { statementDate: 'txt_late_fees', month_1: 'string' },
      { statementDate: 'txt_over_limit_fees', month_1: 'string' },
      { statementDate: 'txt_finance_charge_(p)', month_1: 'string' },
      { statementDate: 'txt_finance_charge_(c)', month_1: 'string' },
      { statementDate: 'txt_cash_balance_adb', month_1: 'string' },
      { statementDate: 'txt_purchase_balance_adb', month_1: 'string' },
      { statementDate: 'txt_behavior', month_1: 9999 },
      { statementDate: 'txt_bureau_score', month_1: 9999 }
    ]);
  });
});
