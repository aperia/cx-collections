import { statementTransactionServices } from './api-services';

// utils
import { mockApiServices, storeId } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import {
  TransactionDetailPayload,
  StatementOverviewRequestBody,
  StatementTransactionRequestBody,
  IDataRequestAdjustmentTransaction
} from './types';

// constants
import { SELECT_FIELDS } from './constants';

describe('statementTransactionServices', () => {
  describe('getTransactionList', () => {
    const params: StatementTransactionRequestBody = {
      accId: storeId,
      startSequence: '12',
      endSequence: '56',
      statementDate: '12/02/2021',
      isCycleToDate: true
    };

    const mapRequestBody = (postData: StatementTransactionRequestBody) => {
      const {
        accId: accountId,
        startSequence,
        endSequence,
        statementDate
      } = postData;

      const options = !statementDate
        ? { displayUnprinted: true }
        : { statementDate };

      return {
        common: { accountId },
        startSequence: Number(startSequence),
        endSequence: Number(endSequence),
        selectFields: SELECT_FIELDS.ALL,
        ...options
      };
    };

    it('when params do not have statementDate', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      const newParams = {
        ...params,
        statementDate: undefined
      } as unknown as StatementTransactionRequestBody;

      statementTransactionServices.getTransactionList(newParams);

      expect(mockService).toBeCalledWith('', mapRequestBody(newParams));
    });

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      statementTransactionServices.getTransactionList(params);

      expect(mockService).toBeCalledWith('', mapRequestBody(params));
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      statementTransactionServices.getTransactionList(params);

      expect(mockService).toBeCalledWith(
        apiUrl.transaction.getTransactionList,
        mapRequestBody(params)
      );
    });
  });

  describe('getTransactionDetail', () => {
    const params: TransactionDetailPayload = {
      accId: storeId,
      transactionCode: 'SE'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      statementTransactionServices.getTransactionDetail(params);

      expect(mockService).toBeCalledWith('', {
        common: { accountId: params.accId },
        selectFields: SELECT_FIELDS.DETAIL,
        transactionCode: params.transactionCode
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      statementTransactionServices.getTransactionDetail(params);

      expect(mockService).toBeCalledWith(
        apiUrl.transaction.getTransactionDetail,
        {
          common: { accountId: params.accId },
          selectFields: SELECT_FIELDS.DETAIL,
          transactionCode: params.transactionCode
        }
      );
    });
  });

  describe('getStatementOverview', () => {
    const params: StatementOverviewRequestBody = {
      accountId: storeId,
      statementDate: '21/02/021'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      statementTransactionServices.getStatementOverview(params);

      expect(mockService).toBeCalledWith('', {
        common: { accountId: params.accountId },
        selectFields: SELECT_FIELDS.OVERVIEW,
        statementDate: params.statementDate
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      statementTransactionServices.getStatementOverview(params);

      expect(mockService).toBeCalledWith(
        apiUrl.statement.getAccountStatementOverview,
        {
          common: { accountId: params.accountId },
          selectFields: SELECT_FIELDS.OVERVIEW,
          statementDate: params.statementDate
        }
      );
    });
  });

  describe('getStatementMonthList', () => {
    const params = { accId: storeId };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      statementTransactionServices.getStatementMonthList(params);

      expect(mockService).toBeCalledWith('', {
        common: { accountId: params.accId },
        selectFields: SELECT_FIELDS.ALL
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      statementTransactionServices.getStatementMonthList(params);

      expect(mockService).toBeCalledWith(
        apiUrl.statement.getStatementMonthList,
        {
          common: { accountId: params.accId },
          selectFields: SELECT_FIELDS.ALL
        }
      );
    });
  });

  describe('getAccountStatementHistory', () => {
    const params = { accId: storeId };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      statementTransactionServices.getAccountStatementHistory(params);

      expect(mockService).toBeCalledWith('', {
        common: { accountId: params.accId },
        selectFields: SELECT_FIELDS.ALL
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      statementTransactionServices.getAccountStatementHistory(params);

      expect(mockService).toBeCalledWith(
        apiUrl.statement.getAccountStatementHistory,
        {
          common: { accountId: params.accId },
          selectFields: SELECT_FIELDS.ALL
        }
      );
    });
  });

  describe('adjustTransaction', () => {
    const params: IDataRequestAdjustmentTransaction = {
      common: { user: 'string', accountId: storeId, cycle: 'M' },
      batchType: 'batchType',
      batchPrefix: 'batchPrefix',
      transactionCode: 'SE',
      transactionAmount: '789',
      transactionPostingDate: '12/03/2021',
      monetaryRejectOverrideIndicator: ''
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      statementTransactionServices.adjustTransaction(params);

      expect(mockService).toBeCalledWith(undefined, params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      statementTransactionServices.adjustTransaction(params);

      expect(mockService).toBeCalledWith(
        apiUrl.transactionAdjustmentRequest.adjustTransaction,
        params
      );
    });
  });
});
