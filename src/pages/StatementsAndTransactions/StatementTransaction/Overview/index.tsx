import React, { useEffect, useMemo } from 'react';

// helper
import classnames from 'classnames';

// components
import { View } from 'app/_libraries/_dof/core';

// redux store
import { useDispatch, useSelector } from 'react-redux';
import { statementTransactionAction as action } from '../__redux/reducers';
import { useAccountDetail } from 'app/hooks';

// type
import { StatementOverviewData } from '../types';
import { OVERVIEW, STATEMENT_MONTH_DEFAULT } from '../constants';
import isEmpty from 'lodash.isempty';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface OverviewProps {
  statementDate: string;
}

const Overview: React.FC<OverviewProps> = ({ statementDate }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId, accEValue } = useAccountDetail();

  const data = useSelector<RootState, StatementOverviewData>(
    state => state.statementTransactionList[storeId]?.overview?.data || {}
  );

  const loading = useSelector<RootState, boolean>(
    state =>
      state.statementTransactionList[storeId]?.overview?.isLoading || false
  );

  useEffect(() => {
    if (STATEMENT_MONTH_DEFAULT.code === statementDate) return;

    dispatch(
      action.getOverview({
        storeId: storeId,
        postData: { statementDate, accountId: accEValue }
      })
    );
  }, [dispatch, storeId, statementDate, accEValue]);

  const statementOverviewView = useMemo(() => {
    if (isEmpty(data)) return null;
    return (
      <View
        id={`${storeId}_${OVERVIEW}`}
        formKey={`${storeId}_${OVERVIEW}_view`}
        descriptor={OVERVIEW}
        value={data}
      />
    );
  }, [data, storeId]);

  if (!statementDate || statementDate === STATEMENT_MONTH_DEFAULT.code) {
    return null;
  }

  return (
    <div className={classnames('mt-16', { loading })}>
      <h5
        data-testid={genAmtId('statementTransaction', 'overview', 'Overview')}
      >
        {t('txt_overview')}
      </h5>
      <div className="border bg-light-l20 br-radius-8 br-light-l04 mt-16 px-16 pb-16">
        {statementOverviewView}
      </div>
    </div>
  );
};

export default Overview;
