import React from 'react';

// components
import Overview from './index';

// utils
import { renderMockStoreId, storeId } from 'app/test-utils';
import { AccountDetailProvider } from 'app/hooks';
import { screen } from '@testing-library/react';

// constants
import { OVERVIEW } from '../constants';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

interface MockOverViewProps {
  initialState: object;
  statementDate: string;
}
const renderComponentWithMockProvider = ({
  initialState,
  statementDate
}: MockOverViewProps) => {
  jest.useFakeTimers();

  const { wrapper } = renderMockStoreId(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <Overview statementDate={statementDate} />
    </AccountDetailProvider>,
    { initialState },
    true
  );

  jest.runAllTimers();

  return {
    baseElement: wrapper.baseElement as HTMLElement,
    container: wrapper.container
  };
};

const initialState: object = {
  statementTransactionList: {
    [storeId]: {
      overview: {
        isLoading: false,
        data: {
          availableCreditRemainingAmount: '0000000000006000',
          cashAdb: '0000000000000000.00',
          cashApr: '017.99',
          cashCorrespondingApr: '000.00',
          cashDailyPeriodicRate: '.04928',
          costSavings: '0000000000000000.00',
          daysInBillingCycleNumber: '031',
          heldStatementDestinationCode: 'W',
          minimumPaymentAmount: '0000000000000000.00',
          mpdPayoffYears: '00',
          newAccountBalance: '0000000000000000.00',
          otherFees: '0000000000000000.00',
          pastDueAmount: '0000000000000000.00',
          payment36Month: '0000000000000000.00',
          paymentAndCredit: '0',
          paymentDueDate: '07-17',
          periodicFinanceCharge: '0000000000000000.00',
          previousMonthBalance: '0000000000000000.00',
          purAdb: '0000000000000000.00',
          purApr: '017.99',
          purCorrespondingApr: '000.00',
          purDailyPeriodicRate: '.04928',
          purchase: '0000000000000620.16',
          statementCreditLineAmount: '0000000000006000',
          statementDate: '2021-06-20',
          totalCost36: '0000000000000000.00',
          totalCostMfd: '0000000000000000.00',
          totalYearsMonths: '00000'
        }
      }
    }
  }
};
const statementDate = '12/12/2000';

describe('Test StatementTransaction/Overview.test.ts', () => {
  it('Render UI', () => {
    renderComponentWithMockProvider({
      initialState,
      statementDate
    });

    expect(screen.queryByTestId(OVERVIEW)).toBeInTheDocument();
  });

  it("Don't render UI when statementDate is empty", () => {
    renderComponentWithMockProvider({
      initialState,
      statementDate: ''
    });
    expect(screen.queryByTestId(OVERVIEW)).not.toBeInTheDocument();
  });
  it("Don't render UI when data is empty", () => {
    renderComponentWithMockProvider({
      initialState: {},
      statementDate: statementDate
    });
    expect(screen.queryByTestId(OVERVIEW)).not.toBeInTheDocument();
  });
});
