import React, { useEffect, useState } from 'react';

// components
import { ColumnType, Grid, Icon } from 'app/_libraries/_dls/components';

// helpers
import classNames from 'classnames';
import { formatDataGrid } from '../helpers';
import isEmpty from 'lodash.isempty';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// redux store
import { useDispatch } from 'react-redux';

// types
import { AccountStatementHistoryFormatData } from '../types';
import { statementTransactionAction } from '../__redux/reducers';
import {
  selectDataStatementHistory,
  selectIsLoadingStatementHistory,
  selectNodataStatementHistory
} from '../__redux/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface GridViewHistoryProps {}

const GridViewHistory: React.FC<GridViewHistoryProps> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [columns, setColumns] = useState<ColumnType[]>([]);
  const [dataHeader, setDataHeader] = useState<Record<string, string>>({});
  const { storeId, accEValue } = useAccountDetail();

  const data = useStoreIdSelector<AccountStatementHistoryFormatData[]>(
    selectDataStatementHistory
  );
  const noData = useStoreIdSelector<boolean>(selectNodataStatementHistory);
  const loading = useStoreIdSelector<boolean>(selectIsLoadingStatementHistory);

  useEffect(() => {
    let newColumn = [
      {
        id: 'statementDate',
        accessor: 'statementDate',
        isFixedLeft: true,
        width: 190
      }
    ] as ColumnType[];

    const length = isEmpty(data) ? 1 : data.length;

    for (let index = 0; index < length; index++) {
      newColumn.push({
        id: `month_${index + 1}`,
        accessor: `month_${index + 1}`,
        width: 170
      });
    }

    newColumn = newColumn.map((column: ColumnType) => ({
      ...column,
      cellHeaderProps: {
        className: 'fs-14 color-grey-d20 text-transform-none'
      },
      cellProps: { className: 'text-right' }
    }));

    newColumn[0].cellProps = { className: 'text-left' };
    newColumn[0].cellBodyProps = {
      className: 'fs-11 bg-white text-uppercase color-grey fw-500'
    };
    setColumns(newColumn);

    if (isEmpty(data)) return;

    const newDataHeader = data
      .map(item => item.statementDate)
      .reduce(
        (result, item, index) => ({
          ...result,
          [`month_${index + 1}`]: item
        }),
        { statementDate: 'Statement Date' }
      );
    setDataHeader(newDataHeader);
  }, [data]);

  useEffect(() => {
    dispatch(
      statementTransactionAction.getAccountStatementHistory({
        storeId,
        postData: { accId: accEValue }
      })
    );
  }, [dispatch, storeId, accEValue]);

  return (
    <div className={classNames({ loading })}>
      {noData && (
        <div className="text-center my-80">
          <Icon name="file" className="fs-80 color-light-l12" />
          <p
            className="color-grey mt-16"
            data-testid={genAmtId(
              'accountStatementHistory',
              'grid-no-data',
              ''
            )}
          >
            {t(I18N_COMMON_TEXT.NO_DATA)}
          </p>
        </div>
      )}
      {data && !noData && (
        <Grid
          classes={{ body: 'grid-body' }}
          columns={columns}
          dataHeader={dataHeader}
          data={formatDataGrid(data, t)}
          scrollable
          dataTestId={genAmtId('accountStatementHistory', 'grid', '')}
        />
      )}
    </div>
  );
};

export default GridViewHistory;
