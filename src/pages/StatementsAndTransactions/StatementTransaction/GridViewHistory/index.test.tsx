import React from 'react';
// test utils
import { screen } from '@testing-library/react';
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
// component
import GridViewHistory from './index';
// redux
import { statementTransactionAction } from '../__redux/reducers';
// constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
const data = [
  {
    financeChargeC: '$78.00'
  }
];
let spy: jest.SpyInstance;
afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});
const statementActionMock = mockActionCreator(statementTransactionAction);
describe('Test GridViewHistory component', () => {
  it('Render UI', () => {
    // mock
    const getAccountStatementHistoryMock = statementActionMock(
      'getAccountStatementHistory'
    );
    const initialState: object = {
      statementTransactionList: {
        [storeId]: {
          statementHistory: {
            data: data,
            isLoading: false,
            noData: false
          }
        }
      }
    };
    renderMockStoreId(<GridViewHistory />, { initialState });
    expect(screen.getByRole('table')).toBeInTheDocument();
    expect(getAccountStatementHistoryMock).toBeCalled();
    expect(
      screen.queryByText(I18N_COMMON_TEXT.NO_DATA)
    ).not.toBeInTheDocument();
    expect(screen.getByText('$78.00')).toBeInTheDocument;
  });
  it('Does not render when noData = true', () => {
    statementActionMock('getAccountStatementHistory');
    const initialState: object = {
      statementTransactionList: {
        [storeId]: {
          statementHistory: {
            data: data,
            isLoading: false,
            noData: true
          }
        }
      }
    };
    renderMockStoreId(<GridViewHistory />, { initialState });
    expect(screen.queryByRole('table')).not.toBeInTheDocument();
    expect(screen.queryByText(I18N_COMMON_TEXT.NO_DATA)).toBeInTheDocument();
  });

  it('Render UI with empty', () => {
    renderMockStoreId(<GridViewHistory />, { });
    expect(screen.getByRole('table')).toBeInTheDocument();
  });
});
