import React from 'react';

// Components
import AccountStatementHistory from './AccountStatementHistory';
import StatementTransactionGrid from './StatementTransactionGrid';
import AccountStatementMonth from './AccountStatementMonth';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

const StatementTransaction: React.FC = () => {
  const { t } = useTranslation();
  return (
    <div className="p-24 mb-64">
      <div className="d-flex align-items-center justify-content-between">
        <h4
          data-testid={genAmtId(
            'statementTransaction',
            'title',
            'StatementTransaction'
          )}
        >
          {t('txt_statement')}
        </h4>
        <AccountStatementHistory />
      </div>
      <AccountStatementMonth />
      <StatementTransactionGrid />
    </div>
  );
};

export default StatementTransaction;
