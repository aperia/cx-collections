import {
  ResponseStatementOverviewTransaction,
  TransactionDetailPayload,
  StatementOverviewRequestBody,
  StatementTransactionRequestBody,
  IDataRequestAdjustmentTransaction
} from './types';
import { apiService } from 'app/utils/api.service';
import { SELECT_FIELDS } from './constants';

class StatementTransactionServices {
  getTransactionList(postData: StatementTransactionRequestBody) {
    const {
      accId: accountId,
      startSequence,
      endSequence,
      statementDate
    } = postData;
    const url = window.appConfig?.api?.transaction?.getTransactionList || '';
    const options = !statementDate
      ? { displayUnprinted: true }
      : { statementDate };
    return apiService.post(url, {
      common: { accountId },
      startSequence: Number(startSequence),
      endSequence: Number(endSequence),
      selectFields: SELECT_FIELDS.ALL,
      ...options
    });
  }

  getTransactionDetail(postData: TransactionDetailPayload) {
    const { accId: accountId, transactionCode } = postData;
    const url = window.appConfig?.api?.transaction?.getTransactionDetail || '';

    return apiService.post(url, {
      common: { accountId },
      transactionCode,
      selectFields: SELECT_FIELDS.DETAIL
    });
  }

  getStatementOverview(postData: StatementOverviewRequestBody) {
    const { accountId, statementDate } = postData;
    const url =
      window.appConfig?.api?.statement?.getAccountStatementOverview || '';

    return apiService.post<ResponseStatementOverviewTransaction>(url, {
      common: { accountId },
      selectFields: SELECT_FIELDS.OVERVIEW,
      statementDate
    });
  }

  getStatementMonthList(postData: { accId: string }) {
    const { accId: accountId } = postData;
    const url = window.appConfig?.api?.statement?.getStatementMonthList || '';
    return apiService.post(url, {
      common: { accountId },
      selectFields: SELECT_FIELDS.ALL
    });
  }

  getAccountStatementHistory(postData: { accId: string }) {
    const { accId: accountId } = postData;
    const url =
      window.appConfig?.api?.statement?.getAccountStatementHistory || '';
    return apiService.post(url, {
      common: { accountId },
      selectFields: SELECT_FIELDS.ALL
    });
  }

  adjustTransaction(data: IDataRequestAdjustmentTransaction) {
    const url =
      window.appConfig?.api?.transactionAdjustmentRequest?.adjustTransaction;
    return apiService.post(url!, data);
  }
}

export const statementTransactionServices = new StatementTransactionServices();
