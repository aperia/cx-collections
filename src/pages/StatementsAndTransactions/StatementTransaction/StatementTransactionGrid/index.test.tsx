import * as React from 'react';
import { fireEvent, screen } from '@testing-library/react';

import { AccountDetailProvider } from 'app/hooks';
import {
  renderMockStore,
  storeId,
  accEValue,
  mockActionCreator
} from 'app/test-utils';

// components
import StatementTransactionGrid from './index';
import { SortType } from 'app/_libraries/_dls/components';

import {
  statementTransactionAction as action,
  statementTransactionAction
} from '../__redux/reducers';
import {
  ADJUST_ACCEPTABLE_TRANSACTION_CODE_LIST,
  POST_DATA_DEFAULT,
  TRANSACTION_ACTION
} from '../constants';
import { PAGINATION_TRANSACTION } from 'pages/StatementsAndTransactions/PendingAuthorizationTransaction/constants';
import userEvent from '@testing-library/user-event';

const data = [
  {
    aggregateTransactionAmount: 479,
    descriptionOfTransaction: 'INNER CHEF PRIVATE',
    postDate: '2020-08-27T03:44:38.000Z',
    transactionDate: '2020-08-27T03:44:38.000Z',
    transactionCode: `${ADJUST_ACCEPTABLE_TRANSACTION_CODE_LIST[0]}`,
    originalReferenceNumber: '0',
    originalPostDate: '',
    transactionAccountNumber: '0',
    presentationInstrumentTypeCode: '',
    merchantDescription: '',
    businessIdentification: '',
    merchantCategoryCode: '',
    merchantAccount: '',
    riskIdentificationServiceMerchantIndicator: '',
    merchantCity: '',
    merchantState: '',
    currencyCode: '',
    transactionAmount: '',
    authorizationNumber: '',
    authorizationSourceCode: '',
    agentNumber: '',
    dailyStatementSequenceNumber: '',
    automatedTellerMachineAtmFlag: '',
    debitProductCode: '',
    sourceTransactionIdentifier: '',
    promotionIdentifier: '',
    dualityFlag: '',
    industryTransactionIdentifier: '',
    flapIdentifier: '',
    visaNetInterchangeCenterProcessingDate: '',
    feeAttribute: '',
    mailPhoneIndicator: '',
    pointOfSaleEntryMode: ''
  },
  {
    descriptionOfTransaction: 'GIANT TECH LABS',
    postDate: '2020-08-25T20:07:08.000Z',
    transactionDate: '2020-08-25T20:07:08.000Z'
  },
  {
    postDate: ''
  }
];

const initialState: Partial<RootState> = {
  statementTransactionList: { [storeId]: { data } }
};

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponent = jest.requireActual('app/_libraries/_dls/components');
  const { MockGrid } = jest.requireActual('app/test-utils/mocks/MockGrid');

  return {
    ...dlsComponent,
    Grid: MockGrid
  };
});

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

const renderWrapper = (state: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <StatementTransactionGrid />
    </AccountDetailProvider>,
    { initialState: state },
    true
  );
};

describe('Test StatementTransactionGrid component', () => {
  it('Render UI', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy = jest.spyOn(action, 'updatePostData').mockImplementation(mockAction);

    const state: Partial<RootState> = {
      statementTransactionList: {
        [storeId]: { data, isLoading: true, isReset: true }
      }
    };
    renderWrapper(state);

    expect(screen.getByRole('table')).toBeInTheDocument();
    expect(mockAction).toBeCalledWith({
      storeId,
      postData: POST_DATA_DEFAULT
    });
  });

  it('Render UI > error', () => {
    renderWrapper({
      statementTransactionList: { [storeId]: { error: true } }
    });

    expect(
      screen.getByText('txt_data_load_unsuccessful_click_reload_to_try_again')
    ).toBeInTheDocument();
    expect(screen.queryByRole('table')).toBeNull();
  });

  it('Render UI > no transaction', () => {
    renderWrapper({
      statementTransactionList: { [storeId]: { noTransaction: true } }
    });

    expect(
      screen.getByText(/txt_no_transaction_to_display/)
    ).toBeInTheDocument();
    expect(screen.queryByRole('table')).toBeNull();
  });

  it('Render UI > load more', () => {
    renderWrapper({
      statementTransactionList: { [storeId]: { isLoadMore: false } }
    });

    expect(screen.getByText(/txt_back_to_top/)).toBeInTheDocument();
    expect(screen.queryByRole('table')).toBeInTheDocument();
  });

  it('Render UI > post data', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy = jest
      .spyOn(action, 'getStatementListTransaction')
      .mockImplementation(mockAction);

    // render
    renderWrapper({
      statementTransactionList: {
        [storeId]: { data: [], postData: POST_DATA_DEFAULT }
      }
    });

    expect(mockAction).toBeCalledWith({
      storeId,
      postData: { accId: accEValue, ...POST_DATA_DEFAULT }
    });
  });
});

describe('Actions', () => {
  it('handleReload', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy = jest
      .spyOn(action, 'getStatementListTransaction')
      .mockImplementation(mockAction);

    // render
    renderWrapper({
      statementTransactionList: { [storeId]: { error: true } }
    });

    screen.getByRole('button').click();

    expect(mockAction).toBeCalledWith({
      storeId,
      postData: { accId: accEValue, ...POST_DATA_DEFAULT }
    });
  });

  it('handleSortGrid', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy = jest
      .spyOn(action, 'sortByTransaction')
      .mockImplementation(mockAction);

    const sortType: SortType = { id: 'id', order: 'desc' };
    renderWrapper(initialState);

    fireEvent.change(screen.getByTestId('Grid_onSortChange'), {
      target: { value: 'undefined', ...sortType }
    });

    expect(mockAction).toBeCalledWith({ storeId, sortType });
  });

  describe('handleLoadMore', () => {
    it('with isLoadMore=false', () => {
      const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
      spy = jest.spyOn(action, 'updatePostData').mockImplementation(mockAction);

      renderWrapper(initialState);

      mockAction.mockClear();
      screen.getByTestId('Grid_onLoadMore').click();

      expect(mockAction).not.toBeCalled();
    });

    it('with isLoadMore=true', () => {
      const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
      spy = jest.spyOn(action, 'updatePostData').mockImplementation(mockAction);

      // render
      renderWrapper({
        statementTransactionList: {
          [storeId]: { isLoadMore: true, postData: POST_DATA_DEFAULT }
        }
      });

      mockAction.mockClear();
      screen.getByTestId('Grid_onLoadMore').click();

      expect(mockAction).toBeCalledWith({
        storeId,
        postData: {
          startSequence: `${
            Number(POST_DATA_DEFAULT.startSequence) +
            PAGINATION_TRANSACTION.NEXT_SEQUENCE
          }`,
          endSequence: `${
            Number(POST_DATA_DEFAULT.endSequence) +
            PAGINATION_TRANSACTION.NEXT_SEQUENCE
          }`
        }
      });
    });

    it('ActionButton', () => {
      const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
      spy = jest
        .spyOn(action, 'toggleDetailModal')
        .mockImplementation(mockAction);

      const state: Partial<RootState> = {
        statementTransactionList: {
          [storeId]: { data, isLoading: true, isReset: true }
        }
      };
      renderWrapper(state);

      const buttons = screen.getAllByText(TRANSACTION_ACTION.VIEW);
      userEvent.click(buttons[0]);

      expect(mockAction).toBeCalledWith({
        storeId,
        transactionCode: `${ADJUST_ACCEPTABLE_TRANSACTION_CODE_LIST[0]}`
      });
    });
  });

  it('should scroll to top', () => {
    renderWrapper(initialState);

    const btnBackToTop = screen.getByText('txt_back_to_top');
    btnBackToTop?.click();

    expect(screen.getByRole('table')).toBeInTheDocument();
  });

  it('adjust button', () => {
    renderWrapper(initialState);
    const mockAction = mockActionCreator(statementTransactionAction);
    const spyToggleAction = mockAction('toggleAdjustModal');

    const adjustBtn = screen.getByText(TRANSACTION_ACTION.ADJUST);
    adjustBtn?.click();

    expect(spyToggleAction).toBeCalledWith({
      transactionCode: ADJUST_ACCEPTABLE_TRANSACTION_CODE_LIST[0],
      storeId
    });
  });
});
