import React, { useCallback, useEffect, useMemo, useState } from 'react';

// components
import {
  ColumnType,
  Grid,
  GridRef,
  Icon,
  SortType,
  Button
} from 'app/_libraries/_dls/components';
import DetailModal from '../DetailModal';
import AdjustModal from '../AdjustModal';
import FailedApiReload from 'app/components/FailedApiReload';

// helpers
import classNames from 'classnames';
import isEmpty from 'lodash.isempty';
import { convertAPIDateToView, formatAPICurrency } from 'app/helpers';
import { parseJSON } from 'pages/Payment/PaymentList/helpers';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// redux store
import { batch, useDispatch } from 'react-redux';
import { statementTransactionAction as action } from '../__redux/reducers';

// types
import { PostData, RawStatementTransactionData } from '../types';

// constants
import {
  PAGINATION_TRANSACTION,
  TRANSACTION_ACTION,
  POST_DATA_DEFAULT,
  ADJUST_ACCEPTABLE_TRANSACTION_CODE_LIST
} from '../constants';
import {
  selectDataStatementTransaction as selectData,
  selectIsResetStatementTransaction as selectIsReset,
  selectLoadingStatementTransaction as selectLoading,
  selectLoadMoreStatementTransaction as selectLoadMore,
  selectPostDataStatementTransaction as selectPostData,
  selectSortByStatementTransaction as selectSortBy,
  selectNoTransaction,
  selectIsError
} from '../__redux/selectors';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface StatementTransactionGridProps {}

const StatementTransactionGrid: React.FC<StatementTransactionGridProps> =
  () => {
    const [gridRefs, setGridRefs] = useState<GridRef | null>(null);
    const [columns, setColumns] = useState<
      ColumnType<ReturnType<typeof formatGridData>[number]>[]
    >([]);
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const { storeId, accEValue } = useAccountDetail();

    const sortBy = useStoreIdSelector<SortType>(selectSortBy);
    const data = useStoreIdSelector<RawStatementTransactionData[]>(selectData);
    const loading = useStoreIdSelector<boolean>(selectLoading);
    const isLoadMore = useStoreIdSelector<boolean>(selectLoadMore);
    const isReset = useStoreIdSelector<boolean>(selectIsReset);
    const postData = useStoreIdSelector<PostData>(selectPostData);
    const noTransaction = useStoreIdSelector<boolean>(selectNoTransaction);
    const isError = useStoreIdSelector<Boolean>(selectIsError);

    const handleReload = useCallback(() => {
      dispatch(
        action.getStatementListTransaction({
          storeId,
          postData: {
            accId: accEValue,
            ...POST_DATA_DEFAULT
          }
        })
      );
    }, [dispatch, storeId, accEValue]);

    const isExistsAdjust = useMemo(() => {
      return data.filter(rowData => {
        const { transactionCode } = rowData;

        return transactionCode
          ? ADJUST_ACCEPTABLE_TRANSACTION_CODE_LIST.includes(transactionCode)
          : false;
      });
    }, [data]);

    const ActionButton = useCallback(
      (rowData: ReturnType<typeof formatGridData>[number], index: number) => {
        const testId = 'statementTransactionGrid_list_dls-grid';

        const { transactionCode, detail } = rowData;

        const isAdjustAcceptable = transactionCode
          ? ADJUST_ACCEPTABLE_TRANSACTION_CODE_LIST.includes(transactionCode)
          : false;

        return (
          <>
            <Button
              dataTestId={genAmtId(testId, `view-btn-${index}`, '')}
              variant="outline-primary"
              size="sm"
              onClick={() => {
                batch(() => {
                  dispatch(
                    action.toggleDetailModal({ transactionCode, storeId })
                  );
                  dispatch(action.setDataDetail({ data: detail, storeId }));
                });
              }}
            >
              {t(TRANSACTION_ACTION.VIEW)}
            </Button>
            {isAdjustAcceptable && (
              <Button
                dataTestId={genAmtId(testId, `adjust-btn-${index}`, '')}
                variant="outline-primary"
                size="sm"
                onClick={() => {
                  batch(() => {
                    dispatch(
                      action.toggleAdjustModal({ transactionCode, storeId })
                    );
                    dispatch(action.setDataDetail({ data: detail, storeId }));
                  });
                }}
              >
                {t(TRANSACTION_ACTION.ADJUST)}
              </Button>
            )}
          </>
        );
      },
      [dispatch, storeId, t]
    );

    const RenderTime = (time: string, testId?: string) => {
      return (
        <p
          className="form-group-static__text"
          data-testid={genAmtId(testId, '', '')}
        >
          {convertAPIDateToView(time)}
        </p>
      );
    };

    const RenderTransaction = useCallback(
      (rowData: ReturnType<typeof formatGridData>[number], index: number) => {
        const { transactionDate } = rowData;
        const testId = genAmtId(
          'statementTransactionGrid',
          `transactionDate-${index}`,
          ''
        );
        return RenderTime(transactionDate, testId);
      },
      []
    );

    const RenderPostDate = useCallback(
      (rowData: ReturnType<typeof formatGridData>[number], index: number) => {
        const { postDate } = rowData;
        const testId = genAmtId(
          'statementTransactionGrid',
          `postDate-${index}`,
          ''
        );
        return RenderTime(postDate, testId);
      },
      []
    );

    const formatGridData = (txnData: RawStatementTransactionData[]) => {
      return txnData.map(item => {
        const {
          aggregateTransactionAmount = '',
          descriptionOfTransaction,
          postDate,
          transactionCode,
          transactionDate,
          originalReferenceNumber,
          originalPostDate,
          transactionAccountNumber,
          presentationInstrumentTypeCode,
          merchantDescription,
          businessIdentification,
          merchantCategoryCode,
          merchantAccount,
          riskIdentificationServiceMerchantIndicator,
          merchantCity,
          merchantState,
          currencyCode,
          transactionAmount,
          authorizationNumber,
          authorizationSourceCode,
          agentNumber,
          dailyStatementSequenceNumber,
          automatedTellerMachineAtmFlag,
          debitProductCode,
          sourceTransactionIdentifier,
          promotionIdentifier,
          dualityFlag,
          detailIndustryTransactionIdentifier,
          flapIdentifier,
          visaNetInterchangeCenterProcessingDate,
          feeAttribute,
          mailPhoneIndicator,
          pointOfSaleEntryMode
        } = item;

        const objMerchantNumber = parseJSON(merchantAccount);
        const objAccNumber = parseJSON(transactionAccountNumber);
        const objBusinessIdentification = parseJSON(businessIdentification);

        return {
          aggregateTransactionAmount: formatAPICurrency(
            aggregateTransactionAmount
          ),
          transactionCode: (transactionCode || '').trim(),
          descriptionOfTransaction: (descriptionOfTransaction || '').trim(),
          postDate: (postDate || '').trim(),
          transactionDate: (transactionDate || '').trim(),
          detail: {
            transactionDate: (transactionDate || '').trim(),
            postDate: (postDate || '').trim(),
            aggregateTransactionAmount: aggregateTransactionAmount,
            originalReferenceNumber: (originalReferenceNumber || '').trim(),
            descriptionOfTransaction: descriptionOfTransaction,
            originalPostDate: (originalPostDate || '').trim(),
            transactionCode: (transactionCode || '').trim(),
            transactionAccountNumber: objAccNumber.maskedValue,
            presentationInstrumentTypeCode: (
              presentationInstrumentTypeCode || ''
            ).trim(),
            merchantName: merchantDescription,
            businessIdentification: objBusinessIdentification.maskedValue,
            merchantCategoryCode: (merchantCategoryCode || '').trim(),
            merchantAccount: objMerchantNumber.maskedValue,
            riskIdentificationServiceIndicator: (
              riskIdentificationServiceMerchantIndicator || ''
            ).trim(),
            merchantCity: (merchantCity || '').trim(),
            merchantState: (merchantState || '').trim(),
            currencyCode: (currencyCode || '').trim(),
            transactionAmount: (transactionAmount || '').trim(),
            authorizationNumber: (authorizationNumber || '').trim(),
            authorizationSourceCode: (authorizationSourceCode || '').trim(),
            agentNumber: (agentNumber || '').trim(),
            dailyStatementSequenceNumber: (
              dailyStatementSequenceNumber || ''
            ).trim(),
            automatedTellerMachineAtmFlag: (
              automatedTellerMachineAtmFlag || ''
            ).trim(),
            debitProductCode: (debitProductCode || '').trim(),
            sourceTransactionIdentifier: (
              sourceTransactionIdentifier || ''
            ).trim(),
            promotionIdentifier: (promotionIdentifier || '').trim(),
            dualityFlag: (dualityFlag || '').trim(),
            detailIndustryTransactionIdentifier: (
              detailIndustryTransactionIdentifier || ''
            ).trim(),
            flapIdentifier: (flapIdentifier || '').trim(),
            visaNetInterchangeCenterProcessingDate: (
              visaNetInterchangeCenterProcessingDate || ''
            ).trim(),
            feeAttribute: (feeAttribute || '').trim(),
            mailPhoneIndicator: (mailPhoneIndicator || '').trim(),
            pointOfSaleEntryMode: (pointOfSaleEntryMode || '').trim()
          }
        };
      });
    };

    // handle load more
    const handleScrollToTop = () => {
      gridRefs?.gridBodyElement?.scroll({ top: 0 });
    };
    const handleLoadMore = () => {
      if (!isLoadMore) return;
      dispatch(
        action.updatePostData({
          storeId,
          postData: {
            startSequence: `${
              Number(postData.startSequence) +
              PAGINATION_TRANSACTION.NEXT_SEQUENCE
            }`,
            endSequence: `${
              Number(postData.endSequence) +
              PAGINATION_TRANSACTION.NEXT_SEQUENCE
            }`
          }
        })
      );
    };
    const loadElement = (
      <tr className="row-loading">
        <td>
          {isLoadMore ? (
            <>
              <span className="loading" />
              <span
                data-testid={genAmtId(
                  'statementTransactionGrid',
                  'loading-more',
                  'StatementTransactionGrid'
                )}
              >
                {t('txt_load_more_transaction')}
              </span>
            </>
          ) : (
            <>
              <span
                className="mr-4"
                data-testid={genAmtId(
                  'statementTransactionGrid',
                  'end-list',
                  'StatementTransactionGrid'
                )}
              >
                {t('txt_end_of_transaction_list')}
              </span>
              <span
                data-testid={genAmtId(
                  'statementTransactionGrid',
                  'back-to-top',
                  'StatementTransactionGrid'
                )}
                className="link text-decoration-none"
                onClick={handleScrollToTop}
              >
                {t('txt_back_to_top')}
              </span>
            </>
          )}
        </td>
      </tr>
    );

    // handle sort gird
    const handleSortGrid = (sortType: SortType) => {
      gridRefs?.gridBodyElement?.scroll({ top: 0 });
      dispatch(
        action.sortByTransaction({
          storeId,
          sortType
        })
      );
    };

    useEffect(() => {
      setColumns([
        {
          id: 'transactionDate',
          Header: t('txt_trans_date'),
          accessor: RenderTransaction,
          isSort: true,
          width: 124
        },
        {
          id: 'postDate',
          Header: t('txt_posting_date'),
          accessor: RenderPostDate,
          isSort: true,
          width: 125
        },
        {
          id: 'amount',
          Header: t('txt_amount'),
          accessor: 'aggregateTransactionAmount',
          isSort: true,
          width: 120,
          cellProps: { className: 'text-right' }
        },
        {
          id: 'descriptionOfTransaction',
          Header: t('txt_statement_details'),
          accessor: 'descriptionOfTransaction',
          autoWidth: true,
          width: 249
        },
        {
          id: 'action',
          Header:
            (isExistsAdjust?.length || 0) > 0
              ? t('txt_actions')
              : t('txt_action'),
          accessor: ActionButton,
          width: 140,
          isFixedRight: true,
          cellBodyProps: {
            className: `${
              (isExistsAdjust?.length || 0) > 0 ? 'text-left' : 'text-center'
            } td-sm`
          },
          cellHeaderProps: {
            className: `text-center td-sm`
          }
        }
      ]);
    }, [ActionButton, RenderPostDate, RenderTransaction, t, isExistsAdjust]);

    useEffect(() => {
      if (isEmpty(postData)) {
        dispatch(
          action.updatePostData({
            storeId,
            postData: POST_DATA_DEFAULT
          })
        );
        return;
      }
      dispatch(
        action.getStatementListTransaction({
          storeId,
          postData: {
            accId: accEValue,
            ...postData
          }
        })
      );
    }, [storeId, dispatch, postData, accEValue]);

    // remove postData for unmount component
    useEffect(() => {
      return () => {
        dispatch(action.removeStore({ storeId }));
      };
    }, [dispatch, storeId]);

    return (
      <div className={classNames({ loading: loading && isReset })}>
        <h5
          className="mt-24"
          data-testid={genAmtId('statementTransactionGrid', 'title', '')}
        >
          {t('txt_transaction_list')}
        </h5>
        {isError && !noTransaction && (
          <FailedApiReload
            dataTestId={genAmtId(
              'statementTransactionGrid',
              'load-failed',
              'StatementTransactionGrid'
            )}
            id="account-detail-overview-fail"
            className="mt-80"
            onReload={handleReload}
          />
        )}
        {noTransaction && (
          <div className="text-center my-80">
            <Icon name="file" className="fs-80 color-light-l12" />
            <p
              className="color-grey mt-20"
              data-testid={genAmtId(
                'statementTransactionGrid',
                'no-data',
                'StatementTransactionGrid'
              )}
            >
              {t('txt_no_transaction_to_display')}
            </p>
          </div>
        )}
        {!noTransaction && !isError && (
          <>
            <p
              className="my-16"
              data-testid={genAmtId(
                'statementTransactionGrid',
                'total-transactions',
                'StatementTransactionGrid'
              )}
            >
              {t('txt_transactions_loaded', { count: data.length })}
            </p>
            <Grid
              dataTestId={genAmtId(
                'statementTransactionGrid',
                'list',
                'StatementTransactionGrid'
              )}
              maxHeight={482}
              ref={setGridRefs}
              columns={columns}
              sortBy={[sortBy]}
              onSortChange={handleSortGrid}
              data={formatGridData(data)}
              onLoadMore={handleLoadMore}
              loadElement={loadElement}
            />
            <DetailModal />
            <AdjustModal />
          </>
        )}
      </div>
    );
  };

export default StatementTransactionGrid;
