import { SortType } from 'app/_libraries/_dls/components';

export interface DetailModalArg {
  storeId: string;
  postData: PendingTransactionDetailPayload;
}

export interface DetailModalPayload {
  data?: DataDetail;
}

export interface DetailModal extends StoreIdPayload {
  data?: PendingAuthorizationData;
}

export interface DataDetail {
  authorizationDateTime?: string;
  addressVerificationServiceResponseCode?: string;
  agentIdentifier?: string;
  agentNumber?: string;
  authorizationApprovalCode?: string;
  authorizationAmount?: string;
  authorizationDate?: string;
  authorizationFilterCode?: string;
  authorizationLineId?: string;
  authorizationMethodCode?: string;
  debitCardStrategyEffectiveDate?: string;
  authorizationResponseDescriptionText?: string;
  authorizationTime?: string;
  beforeAuthorizationAvailableCreditAmount?: string;
  cardholderAccountNumber?: string;
  cardholderAuthenticationVerificationValue?: string;
  debitPinAuthorizationSettlementDate?: string;
  declineReasonCode?: string;
  enteredExpirationDate?: string;
  enteredIdentifier?: string;
  externalStatusCode?: string;
  industryTransactionIdentifier?: string;
  internalStatusCode?: string;
  merchantCity?: string;
  merchantCategoryCode?: string;
  merchantCountryCode?: string;
  merchantName?: string;
  merchantNumber?: string;
  merchantStateProvince?: string;
  merchantZipCode?: string;
  pinVerificationCode?: string;
  presentationInstrumentTypeCode?: string;
  track1NameMatchCode?: string;
  posEntryModeCode?: string;
  principalIdentifier?: string;
  transactedAccountNumber?: string;
  transactionCode?: string;
  transactionSourceIdentifier?: string;
  terminalIdentifier?: string;
  debitDropMessageDate?: string;
  debitCheckWorkDate?: string;
}

export interface PendingAuthorizationArg {
  storeId: string;
  postData: {
    accountId: string;
    startSequence: number;
    endSequence: number;
  };
}

export interface SortByTransactionPayload {
  storeId: string;
  sortType: SortType;
}

export interface PendingAuthorizationData {
  addressVerificationServiceResponseCode?: string;
  agentIdentifier?: string;
  agentNumber?: string;
  authorizationApprovalCode?: string;
  authorizationAmount?: string;
  authorizationDate?: string;
  authorizationFilterCode?: string;
  authorizationLineId?: string;
  authorizationMethodCode?: string;
  debitCardStrategyEffectiveDate?: string;
  authorizationResponseDescriptionText?: string;
  authorizationTime?: string;
  beforeAuthorizationAvailableCreditAmount?: string;
  cardholderAccountNumber?: string;
  cardholderAuthenticationVerificationValue?: string;
  debitPinAuthorizationSettlementDate?: string;
  declineReasonCode?: string;
  enteredExpirationDate?: string;
  enteredIdentifier?: string;
  externalStatusCode?: string;
  industryTransactionIdentifier?: string;
  internalStatusCode?: string;
  merchantCity?: string;
  merchantCategoryCode?: string;
  merchantCountryCode?: string;
  merchantName?: string;
  merchantNumber?: string;
  merchantStateProvince?: string;
  merchantZipCode?: string;
  pinVerificationCode?: string;
  presentationInstrumentTypeCode?: string;
  track1NameMatchCode?: string;
  posEntryModeCode?: string;
  principalIdentifier?: string;
  transactedAccountNumber?: string;
  transactionCode?: string;
  transactionSourceIdentifier?: string;
  terminalIdentifier?: string;
  debitDropMessageDate?: string;
  debitCheckWorkDate?: string;
}

export interface PendingAuthorizationPayload {
  authorizations: PendingAuthorizationGridData[];
}

export interface PendingAuthorizationGridData
  extends Pick<
    PendingAuthorizationData,
    | 'authorizationResponseDescriptionText'
    | 'authorizationApprovalCode'
    | 'authorizationAmount'
    | 'beforeAuthorizationAvailableCreditAmount'
    | 'merchantName'
    | 'merchantCity'
    | 'merchantStateProvince'
  > {
  authorizationDateTime?: string;
}

export interface PendingAuthorizationState {
  error?: boolean;
  isLoading?: boolean;
  isFirstCall?: boolean;
  noTransaction?: boolean;
  isLoadMore?: boolean;
  sortType?: SortType;
  data?: PendingAuthorizationGridData[];
  detail?: {
    isOpen?: boolean;
    data?: DataDetail;
    loading?: boolean;
    transactionCode?: React.ReactText;
  };
}

export interface PendingAuthorizationStateResult
  extends Record<string, PendingAuthorizationState> {}

export interface PendingAuthorizationTransactionInitialState {
  [storeId: string]: PendingAuthorizationState;
}

export interface PendingTransactionRequest extends CommonAccountIdRequestBody {
  startSequence: number;
  endSequence: number;
  selectFields: string[];
}

export interface PendingTransactionDetailPayload {
  accountId?: string;
  transactionCode?: React.ReactText;
}

export interface PaginationTransactionType {
  START_SEQUENCE: number;
  END_SEQUENCE: number;
  NEXT_SEQUENCE: number;
}

export interface IDSortGridType {
  AMOUNT: string;
  DATE_TIME: string;
  TRANSACTION_DATE: string;
  POST_DATE: string;
}
