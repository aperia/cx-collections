import { SortType } from 'app/_libraries/_dls/components';
import { IDSortGridType, PaginationTransactionType } from './types';

export const INFO = 'pendingAuthorizationDetailInfo';
export const MERCHANT = 'pendingAuthorizationDetailMerchant';
export const OTHER = 'pendingAuthorizationDetailOther';

export const PAGINATION_TRANSACTION: PaginationTransactionType = {
  START_SEQUENCE: 1,
  END_SEQUENCE: 25,
  NEXT_SEQUENCE: 25
};

export const SORT_PENDING_AUTHORIZATION: SortType[] = [{ id: 'dateTime' }];

export const ID_SORT_GRID: IDSortGridType = {
  AMOUNT: 'amount',
  DATE_TIME: 'dateTime',
  TRANSACTION_DATE: 'transactionDate',
  POST_DATE: 'postDate'
};

export const I18N_PENDING_AUTH = {
  TRANSACTIONS_LOADED: 'txt_pending_authorization_transaction_loaded',
  PENDING_AUTHORIZATIONS: 'txt_pending_authorizations',
  NO_TRANSACTION_TO_DISPLAY: 'txt_no_transaction_to_display',
  APPROVAL: 'txt_approval',
  MERCHANT_NAME: 'txt_merchant_name',
  MERCHANT_CITY: 'txt_merchant_city',
  MERCHANT_STATE: 'txt_merchant_state',
  AVAILABLE_CREDIT: 'txt_available_credit',
  AMOUNT: 'txt_amount',
  DATE_TIME: 'txt_date_&_time',
  BACK_TO_TOP: 'txt_back_to_top',
  END_OF_TRANSACTION_LIST: 'txt_end_of_transaction_list',
  LOAD_MORE_TRANSACTION: 'txt_load_more_transaction',
  PENDING_AUTHORIZATION_DETAILS: 'txt_pending_authorization_details',
  MERCHANT: 'txt_merchant',
  OTHER: 'txt_other'
};
