import React, { useCallback, useEffect, useState } from 'react';

// components
import DetailModal from './Modal';
import FailedApiReload from 'app/components/FailedApiReload';
import {
  Button,
  ColumnType,
  Grid,
  GridRef,
  Icon,
  SortType
} from 'app/_libraries/_dls/components';

// helpers
import { formatCommon } from 'app/helpers';
import classNames from 'classnames';
import { formatGridData } from './helpers';

// redux store
import { useDispatch } from 'react-redux';
import { pendingAuthorizationAction } from './__redux/reducers';
import {
  selectDataPendingAuthorization as selectData,
  selectIsLoadingPendingAuthorization as selectIsLoading,
  selectIsLoadMorePendingAuthorization as selectIsLoadMore,
  selectNoTransaction,
  selectIsError
} from './__redux/selectors';

// types
import {
  PendingAuthorizationData,
  PendingAuthorizationGridData
} from './types';

// constants
import {
  I18N_PENDING_AUTH,
  PAGINATION_TRANSACTION,
  SORT_PENDING_AUTHORIZATION
} from './constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'react-i18next';
import { genAmtId } from 'app/_libraries/_dls/utils';

const PendingAuthorizationTransaction: React.FC = () => {
  const dispatch = useDispatch();

  const { t } = useTranslation();

  const { storeId, accEValue } = useAccountDetail();

  const [gridRefs, setGridRefs] = useState<GridRef | null>(null);
  const [columns, setColumns] = useState<
    ColumnType<PendingAuthorizationGridData>[]
  >([]);
  const [startSequence, setStartSequence] = useState<number>(
    PAGINATION_TRANSACTION.START_SEQUENCE
  );
  const [endSequence, setEndSequence] = useState<number>(
    PAGINATION_TRANSACTION.END_SEQUENCE
  );
  const [sortBy, setSortBy] = useState<SortType[]>(SORT_PENDING_AUTHORIZATION);

  const data = useStoreIdSelector<PendingAuthorizationData[]>(selectData);
  const loading = useStoreIdSelector<boolean>(selectIsLoading);
  const isLoadMore = useStoreIdSelector<boolean>(selectIsLoadMore);
  const noTransaction = useStoreIdSelector<boolean>(selectNoTransaction);
  const isError = useStoreIdSelector<boolean>(selectIsError);
  const testId = 'pending-authorization-transaction';

  const RenderDateTime = (
    rowData: Partial<PendingAuthorizationGridData>,
    index: number
  ) => {
    const { authorizationDateTime = '' } = rowData;

    return (
      <div data-testid={genAmtId(testId, `datetime-${index}`, '')}>
        <p>{formatCommon(authorizationDateTime).time.date}</p>
        <p className="color-grey">
          {formatCommon(authorizationDateTime).time.fullTimeMeridiem}
        </p>
      </div>
    );
  };

  const ActionButton = useCallback(
    (rowData: Partial<PendingAuthorizationGridData>, index: number) => {
      const handleClick = () => {
        dispatch(
          pendingAuthorizationAction.toggleDetailModal({
            storeId,
            data: rowData
          })
        );
      };

      return (
        <Button
          size="sm"
          variant="outline-primary"
          onClick={handleClick}
          dataTestId={genAmtId(testId, `view-btn-${index}`, '')}
        >
          {t(I18N_COMMON_TEXT.VIEW)}
        </Button>
      );
    },
    [storeId, dispatch, t]
  );

  // handle reload
  const handleReload = useCallback(() => {
    dispatch(
      pendingAuthorizationAction.getPendingAuthorizationTransaction({
        storeId,
        postData: { accountId: accEValue, startSequence, endSequence }
      })
    );
  }, [storeId, accEValue, startSequence, endSequence, dispatch]);

  // handle load more
  const handleScrollToTop = () => {
    gridRefs && gridRefs?.gridBodyElement?.scroll({ top: 0 });
  };
  const handleLoadMore = () => {
    if (!isLoadMore) return;
    setStartSequence(startSequence + PAGINATION_TRANSACTION.NEXT_SEQUENCE);
    setEndSequence(endSequence + PAGINATION_TRANSACTION.NEXT_SEQUENCE);
  };
  const loadElement = (
    <tr className="row-loading">
      <td>
        {isLoadMore ? (
          <>
            <span className="loading" />
            <span>{t(I18N_PENDING_AUTH.LOAD_MORE_TRANSACTION)}</span>
          </>
        ) : (
          <>
            <span
              className="mr-4"
              data-testid={genAmtId(testId, 'end-of-list', '')}
            >
              {t(I18N_PENDING_AUTH.END_OF_TRANSACTION_LIST)}
            </span>
            <span
              className="link text-decoration-none"
              onClick={handleScrollToTop}
              data-testid={genAmtId(testId, 'back-to-top', '')}
            >
              {t(I18N_PENDING_AUTH.BACK_TO_TOP)}
            </span>
          </>
        )}
      </td>
    </tr>
  );

  // handle sort gird
  const handleSortGrid = (sortType: SortType) => {
    setSortBy([sortType]);
    gridRefs && gridRefs?.gridBodyElement?.scroll({ top: 0 });
    dispatch(
      pendingAuthorizationAction.sortByTransaction({ storeId, sortType })
    );
  };

  useEffect(() => {
    setColumns([
      {
        id: 'dateTime',
        Header: `${t(I18N_PENDING_AUTH.DATE_TIME)}`,
        accessor: RenderDateTime,
        isSort: true,
        width: 125
      },
      {
        id: 'approval',
        Header: `${t(I18N_PENDING_AUTH.APPROVAL)}`,
        accessor: 'authorizationApprovalCode',
        width: 102
      },
      {
        id: 'amount',
        Header: `${t(I18N_PENDING_AUTH.AMOUNT)}`,
        accessor: 'authorizationAmount',
        isSort: true,
        width: 140,
        cellProps: { className: 'text-right' }
      },
      {
        id: 'availCredit',
        Header: `${t(I18N_PENDING_AUTH.AVAILABLE_CREDIT)}`,
        accessor: 'beforeAuthorizationAvailableCreditAmount',
        width: 170,
        cellProps: { className: 'text-right' }
      },
      {
        id: 'description',
        Header: `${t(I18N_COMMON_TEXT.DESCRIPTION)}`,
        accessor: 'authorizationResponseDescriptionText',
        width: 268
      },
      {
        id: 'merchantName',
        Header: `${t(I18N_PENDING_AUTH.MERCHANT_NAME)}`,
        accessor: 'merchantName',
        width: 160
      },
      {
        id: 'merchantCity',
        Header: `${t(I18N_PENDING_AUTH.MERCHANT_CITY)}`,
        accessor: 'merchantCity',
        width: 138
      },
      {
        id: 'merchantState',
        Header: `${t(I18N_PENDING_AUTH.MERCHANT_STATE)}`,
        accessor: 'merchantStateProvince',
        width: 142
      },
      {
        id: 'action',
        Header: <div className="text-center">{t(I18N_COMMON_TEXT.ACTION)}</div>,
        accessor: ActionButton,
        isFixedRight: true,
        width: 87,
        cellBodyProps: { className: 'td-sm text-center' }
      }
    ]);
  }, [t, ActionButton]);

  useEffect(() => {
    dispatch(
      pendingAuthorizationAction.getPendingAuthorizationTransaction({
        storeId,
        postData: { accountId: accEValue, startSequence, endSequence }
      })
    );

    return () => {
      // clear data when component unmount
      dispatch(
        pendingAuthorizationAction.removeStore({
          storeId
        })
      );
    };
  }, [storeId, accEValue, dispatch, endSequence, startSequence]);

  return (
    <div
      className={classNames('p-24 mb-64', { loading: loading && !isLoadMore })}
    >
      <h4 className="mb-16" data-testid={genAmtId(testId, 'title', '')}>
        {t(I18N_PENDING_AUTH.PENDING_AUTHORIZATIONS)}
      </h4>
      {isError && (
        <FailedApiReload
          id="account-detail-overview-fail"
          className="mt-80"
          onReload={handleReload}
          dataTestId={testId}
        />
      )}
      {noTransaction && (
        <div className="text-center mt-80">
          <Icon
            name="file"
            className="fs-80 color-light-l12"
            dataTestId={testId}
          />
          <p
            className="color-grey mt-20"
            data-testid={genAmtId(testId, 'no-transaction', '')}
          >
            {t(I18N_PENDING_AUTH.NO_TRANSACTION_TO_DISPLAY)}
          </p>
        </div>
      )}
      {!noTransaction && !isError && (
        <>
          <p
            className="mb-16"
            data-testid={genAmtId(testId, 'transactions-loaded', '')}
          >
            {t(I18N_PENDING_AUTH.TRANSACTIONS_LOADED, { count: data.length })}
          </p>
          <Grid
            maxHeight={700}
            ref={setGridRefs}
            columns={columns}
            sortBy={sortBy}
            onSortChange={handleSortGrid}
            data={formatGridData(data)}
            onLoadMore={handleLoadMore}
            loadElement={loadElement}
            dataTestId={genAmtId(testId, 'grid', '')}
          />
        </>
      )}
      <DetailModal />
    </div>
  );
};

export default PendingAuthorizationTransaction;
