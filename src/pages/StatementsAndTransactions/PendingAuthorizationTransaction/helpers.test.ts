import {
  formatGridData,
  sortByGrid,
  prepareViewDetailData,
  combineDateAndTime
} from './helpers';
import { ID_SORT_GRID } from './constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';

jest.mock('app/_libraries/_dls/hooks', () => ({
  useTranslation: () => ({
    t: (key: string) => key,
    i18n: { changeLanguage: jest.fn() }
  })
}));

const DATA_9 = {
  authorizationAmount: '9',
  authorizationApprovalCode: '123',
  authorizationAvailable: 123,
  authorizationCode: 123,
  authorizationDateTime: '2020, 01, 27',
  authorizationDescription: 'string',
  actionText: 'string'
};

const DATA_8 = {
  authorizationAmount: '8',
  authorizationApprovalCode: '123',
  authorizationAvailable: 123,
  authorizationCode: 123,
  authorizationDateTime: '2020, 01, 28',
  authorizationDescription: 'string',
  actionText: 'string'
};

const DATA_10 = {
  authorizationAmount: '10',
  authorizationApprovalCode: '123',
  authorizationAvailable: 123,
  authorizationCode: 123,
  authorizationDateTime: '2020, 01, 26',
  authorizationDescription: 'string',
  actionText: 'string'
};

const DATA_WRONG = {
  authorizationApprovalCode: '123',
  authorizationAvailable: 123,
  authorizationCode: 123,
  authorizationDescription: 'string',
  actionText: 'string'
};

const dataSort = [DATA_9, DATA_8, DATA_10];

describe('Test sortByGrid function', () => {
  it('sort data grid for sortType incorrect', () => {
    const data = sortByGrid({ id: '111' }, dataSort);
    expect(data).toEqual(dataSort);
  });

  it('sort data gird for amount', () => {
    const data = sortByGrid({ id: ID_SORT_GRID.AMOUNT }, dataSort);
    const data1 = sortByGrid(
      { id: ID_SORT_GRID.AMOUNT, order: 'desc' },
      dataSort
    );
    const data2 = sortByGrid(
      { id: ID_SORT_GRID.AMOUNT, order: 'asc' },
      dataSort
    );

    expect(data).toEqual([DATA_10, DATA_9, DATA_8]);
    expect(data1).toEqual([DATA_10, DATA_9, DATA_8]);
    expect(data2).toEqual([DATA_8, DATA_9, DATA_10]);
  });

  it('sort data gird for amount with data wrong', () => {
    const data = sortByGrid({ id: ID_SORT_GRID.AMOUNT }, [
      DATA_WRONG,
      DATA_WRONG
    ]);
    const data1 = sortByGrid({ id: ID_SORT_GRID.AMOUNT, order: 'desc' }, [
      DATA_WRONG,
      DATA_WRONG
    ]);
    const data2 = sortByGrid({ id: ID_SORT_GRID.AMOUNT, order: 'asc' }, [
      DATA_WRONG,
      DATA_WRONG
    ]);

    expect(data).toEqual([DATA_WRONG, DATA_WRONG]);
    expect(data1).toEqual([DATA_WRONG, DATA_WRONG]);
    expect(data2).toEqual([DATA_WRONG, DATA_WRONG]);
  });

  it('sort data grid for date', () => {
    const data = sortByGrid({ id: ID_SORT_GRID.DATE_TIME }, dataSort);
    const data1 = sortByGrid(
      { id: ID_SORT_GRID.DATE_TIME, order: 'desc' },
      dataSort
    );
    const data2 = sortByGrid(
      { id: ID_SORT_GRID.DATE_TIME, order: 'asc' },
      dataSort
    );

    expect(data).toEqual([DATA_8, DATA_9, DATA_10]);
    expect(data1).toEqual([DATA_8, DATA_9, DATA_10]);
    expect(data2).toEqual([DATA_10, DATA_9, DATA_8]);
  });

  it('sort data gird for date with data wrong', () => {
    const data = sortByGrid({ id: ID_SORT_GRID.DATE_TIME }, [
      DATA_WRONG,
      DATA_WRONG
    ]);
    const data1 = sortByGrid({ id: ID_SORT_GRID.DATE_TIME, order: 'desc' }, [
      DATA_WRONG,
      DATA_WRONG
    ]);
    const data2 = sortByGrid({ id: ID_SORT_GRID.DATE_TIME, order: 'asc' }, [
      DATA_WRONG,
      DATA_WRONG
    ]);

    expect(data).toEqual([DATA_WRONG, DATA_WRONG]);
    expect(data1).toEqual([DATA_WRONG, DATA_WRONG]);
    expect(data2).toEqual([DATA_WRONG, DATA_WRONG]);
  });
});

describe('Test formatGridData function', () => {
  it('format data Grid with valid data', () => {
    const value = [
      {
        authorizationApprovalCode: '9999',
        authorizationAvailable: 9999,
        authorizationCode: 9999,
        authorizationDate: '2021-04-07',
        authorizationTime: '092806',
        authorizationDescription: 'string'
      }
    ];

    const formatData = formatGridData(value);

    expect(formatData).toEqual([
      {
        authorizationAmount: '',
        authorizationApprovalCode: '9999',
        authorizationAvailable: 9999,
        authorizationCode: 9999,
        authorizationDate: '2021-04-07',
        authorizationTime: '092806',
        authorizationDateTime: '04/07/2021 09:28:06 AM',
        authorizationDescription: 'string',
        beforeAuthorizationAvailableCreditAmount: ''
      }
    ]);
  });

  it('format data Grid with date and time not provided', () => {
    const value = [
      {
        authorizationApprovalCode: '9999',
        authorizationAvailable: 9999,
        authorizationCode: 9999,
        authorizationDate: '',
        authorizationTime: '',
        authorizationDescription: 'string'
      }
    ];

    const formatData = formatGridData(value);

    expect(formatData).toEqual([
      {
        authorizationAmount: '',
        authorizationApprovalCode: '9999',
        authorizationAvailable: 9999,
        authorizationCode: 9999,
        authorizationDate: '',
        authorizationTime: '',
        authorizationDateTime: undefined,
        authorizationDescription: 'string',
        beforeAuthorizationAvailableCreditAmount: ''
      }
    ]);
  });

  it('coverage combineDateAndTime', () => {
    const value = [
      {
        authorizationApprovalCode: '9999',
        authorizationAvailable: 9999,
        authorizationCode: 9999,
        authorizationDate: '2021-04-07',
        authorizationTime: '100910',
        authorizationDescription: 'string'
      }
    ];

    const formatData = formatGridData(value);

    expect(formatData).toEqual([
      {
        authorizationAmount: '',
        authorizationApprovalCode: '9999',
        authorizationAvailable: 9999,
        authorizationCode: 9999,
        authorizationDate: '2021-04-07',
        authorizationTime: '100910',
        authorizationDateTime: '04/07/2021 10:09:10 AM',
        authorizationDescription: 'string',
        beforeAuthorizationAvailableCreditAmount: ''
      }
    ]);
  });
});

describe('prepareViewDetailData', () => {
  const { t } = useTranslation();
  const input = {
    addressVerificationServiceResponseCode: '',
    agentIdentifier: '0000',
    agentNumber: '0000',
    authorizationApprovalCode: '012093',
    authorizationAmount: '00000000000001.00',
    authorizationDate: '2021-04-12',
    authorizationFilterCode: 'A',
    authorizationMethodCode: '0',
    debitCardStrategyEffectiveDate: '00000000',
    authorizationResponseDescriptionText: 'AUTH GRANTED               ',
    authorizationTime: '053306',
    beforeAuthorizationAvailableCreditAmount: '0000010000601221',
    cardholderAccountNumber:
      '{"maskedValue":"425769******5033","eValue":"VOL(ZWU0SjFgdDRgV3YlSDo5ew==)"}',
    cardholderAuthenticationVerificationValue:
      ' - Transaction did not require a CVV/CVC verification',
    debitPinAuthorizationSettlementDate: '0000',
    declineReasonCode: '  ',
    enteredExpirationDate: '2022-12',
    enteredIdentifier:
      '{"maskedValue":"425769******5033","eValue":"VOL(VGo4S2hKZkdoQ0N4T2FkUFolQA==)"}',
    externalStatusCode: 'U',
    industryTransactionIdentifier: '               ',
    internalStatusCode: ' ',
    merchantCity: 'ROANOKE      ',
    merchantCategoryCode: '0006',
    merchantCountryCode: '840',
    merchantName: 'BALANCE TRANSFER CASH    ',
    merchantNumber:
      '{"maskedValue":"425769******2055","eValue":"VOL(dltCX01ZKD9TTGBScG4wYA==)"}',
    merchantStateProvince: 'VA',
    merchantZipCode: '240172339',
    pinVerificationCode:
      ' - The transaction did not include track data, or Product Control File settings are not set to verify a CVV/CVC.',
    presentationInstrumentTypeCode: '08',
    track1NameMatchCode:
      ' - The transaction did not include track data, or Product Control File settings are not set to verify a CVV/CVC.',
    posEntryModeCode: '01',
    principalIdentifier: '1000',
    transactedAccountNumber:
      '{"maskedValue":"425769******9914","eValue":"VOL(OXV9W3RyYm9cSy4vWmJoY3MzQA==)"}',
    terminalIdentifier: 'BCON',
    debitDropMessageDate: '00000000',
    debitCheckWorkDate: '0000000'
  };

  it('should return data case undefined', () => {
    const result = prepareViewDetailData(input, t);
    expect(result).toEqual({
      ...input,
      authorizationDateTime: undefined,
      authorizationLineId: '',
      cardholderAuthenticationVerificationValue:
        '- Transaction did not require a CVV/CVC verification',
      pinVerificationCode:
        '- The transaction did not include track data, or Product Control File settings are not set to verify a CVV/CVC.',
      enteredIdentifier: '425769******5033',
      merchantNumber: '425769******2055',
      transactedAccountNumber: '425769******9914',
      transactionCode: '',
      transactionSourceIdentifier: ''
    });
  });

  it('coverage', () => {
    const result = prepareViewDetailData(
      {
        ...input,
        cardholderAuthenticationVerificationValue: '',
        pinVerificationCode: ''
      },
      t
    );
    expect(result).toEqual({
      ...input,
      authorizationDateTime: undefined,
      authorizationLineId: '',
      cardholderAuthenticationVerificationValue:
        ' - txt_transaction_did_not_require_a_cvv/cvc_verification',
      pinVerificationCode: ' - txt_the_transaction_did_not_include_track_data',
      enteredIdentifier: '425769******5033',
      merchantNumber: '425769******2055',
      transactedAccountNumber: '425769******9914',
      transactionCode: '',
      transactionSourceIdentifier: ''
    });
  });
});

describe('test combineDateAndTime', () => {
  it('test cover time is wrong format', () => {
    const result = combineDateAndTime('01-01-2021', '__:__:__');
    expect(result).toEqual(undefined);
  });
});
