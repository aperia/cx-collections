import { pendingAuthorizationServices } from './api-services';

// utils
import { mockApiServices, storeId } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import {
  PendingTransactionDetailPayload,
  PendingTransactionRequest
} from './types';

describe('pendingAuthorizationServices', () => {
  describe('getDetail', () => {
    const params: PendingTransactionDetailPayload = {
      accountId: storeId
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      pendingAuthorizationServices.getDetail(params);

      expect(mockService).toBeCalledWith(undefined, params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      pendingAuthorizationServices.getDetail(params);

      expect(mockService).toBeCalledWith(
        apiUrl.pendingAuthorizations.getPendingAuthTransactionDetail,
        params
      );
    });
  });

  describe('getPendingAuthorizationTransaction', () => {
    const params: PendingTransactionRequest = {
      startSequence: 324,
      endSequence: 324,
      selectFields: ['field_1']
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      pendingAuthorizationServices.getPendingAuthorizationTransaction(params);

      expect(mockService).toBeCalledWith(undefined, params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      pendingAuthorizationServices.getPendingAuthorizationTransaction(params);

      expect(mockService).toBeCalledWith(
        apiUrl.pendingAuthorizations.getPendingAuthTransactions,
        params
      );
    });
  });
});
