import { reducer, pendingAuthorizationAction } from './reducers';
import { PendingAuthorizationTransactionInitialState } from '../types';

const storeId = '123456789';

const initStateData: PendingAuthorizationTransactionInitialState = {
  [storeId]: {
    isLoading: false,
    isFirstCall: true,
    noTransaction: false,
    isLoadMore: true,
    sortType: { id: 'amount', order: 'asc' },
    data: [
      {
        authorizationAmount: 8,
        authorizationApprovalCode: 123,
        authorizationAvailable: 123,
        authorizationCode: 123,
        authorizationDateTime: '2020, 01, 28',
        authorizationDescription: 'string',
        actionText: 'string'
      },
      {
        authorizationAmount: 9,
        authorizationApprovalCode: 123,
        authorizationAvailable: 123,
        authorizationCode: 123,
        authorizationDateTime: '2020, 01, 27',
        authorizationDescription: 'string',
        actionText: 'string'
      },
      {
        authorizationAmount: 10,
        authorizationApprovalCode: 123,
        authorizationAvailable: 123,
        authorizationCode: 123,
        authorizationDateTime: '2020, 01, 26',
        authorizationDescription: 'string',
        actionText: 'string'
      }
    ],
    detail: {
      isOpen: false,
      data: {},
      loading: false,
      transactionCode: '121313'
    }
  }
};

describe('Test reducer pending authorization transaction', () => {
  it('remove store action', () => {
    const state = reducer(
      initStateData,
      pendingAuthorizationAction.removeStore({ storeId })
    );

    expect(state).toEqual({});
  });

  it('toggleDetailModal action', () => {
    const state = reducer(
      initStateData,
      pendingAuthorizationAction.toggleDetailModal({
        storeId,
        data: {}
      })
    );

    const state1 = reducer(
      initStateData,
      pendingAuthorizationAction.toggleDetailModal({
        storeId
      })
    );

    expect(state[storeId].detail!.data).toEqual({});
    expect(state1[storeId].detail!.isOpen).toBeTruthy();
  });

  it('sortByTransaction action', () => {
    const state = reducer(
      initStateData,
      pendingAuthorizationAction.sortByTransaction({
        storeId,
        sortType: { id: 'amount' }
      })
    );

    expect(state[storeId].sortType).toEqual({ id: 'amount' });
  });
});
