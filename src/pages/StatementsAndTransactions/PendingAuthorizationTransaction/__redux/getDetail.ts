// Redux
import { AppState } from 'storeConfig';
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Helpers
import { formatTime, mappingDataFromObj } from 'app/helpers';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';

// Types
import {
  DetailModalArg,
  DetailModalPayload,
  PendingAuthorizationStateResult
} from '../types';

// Services
import { pendingAuthorizationServices } from '../api-services';

export const getDetail = createAsyncThunk<
  DetailModalPayload,
  DetailModalArg,
  ThunkAPIConfig
>('pendingAuth/getDetail', async (args, thunkAPI) => {
  const { accountId, transactionCode } = args.postData;
  const { data } = await pendingAuthorizationServices.getDetail({
    accountId,
    transactionCode
  });

  const state = thunkAPI.getState();
  const mapping =
    (state as AppState).mapping.data.pendingAuthorizationDetail || {};
  if (isEmpty(data)) return {};

  const dataResponse = get(data, ['customers', '0']);
  const mappedData = mappingDataFromObj(dataResponse, mapping);
  mappedData.dateTime =
    formatTime(mappedData.authorizationDate).date +
    ' ' +
    mappedData.authorizationTime;

  return { data: mappedData };
});

export const getDetailBuilder = (
  builder: ActionReducerMapBuilder<PendingAuthorizationStateResult>
) => {
  builder
    .addCase(getDetail.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState?.[storeId],
        detail: {
          ...draftState?.[storeId]?.detail,
          loading: true
        }
      };
    })
    .addCase(getDetail.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      draftState[storeId] = {
        ...draftState?.[storeId],
        detail: {
          ...draftState?.[storeId]?.detail,
          data,
          loading: false
        }
      };
    })
    .addCase(getDetail.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState?.[storeId],
        detail: {
          ...draftState?.[storeId]?.detail,
          loading: false
        }
      };
    });
};
