import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  getPendingAuthorizationTransaction,
  getPendingAuthorizationTransactionBuilder
} from './getPendingAuthorizationTransaction';

import { getDetail, getDetailBuilder } from './getDetail';
import { sortByGrid } from '../helpers';

// types
import {
  PendingAuthorizationTransactionInitialState,
  DetailModal,
  SortByTransactionPayload
} from '../types';

const { actions, reducer } = createSlice({
  name: 'pendingAuthorization',
  initialState: {} as PendingAuthorizationTransactionInitialState,
  reducers: {
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    toggleDetailModal: (draftState, action: PayloadAction<DetailModal>) => {
      const { storeId, data } = action.payload;
      const isOpenModal = !!draftState[storeId]?.detail?.isOpen;

      draftState[storeId] = {
        ...draftState[storeId],
        detail: {
          ...draftState[storeId].detail,
          isOpen: !isOpenModal,
          data
        }
      };
    },
    sortByTransaction: (
      draftState,
      action: PayloadAction<SortByTransactionPayload>
    ) => {
      const { storeId, sortType } = action.payload;
      const gridData = sortByGrid(sortType, draftState[storeId].data!);
      draftState[storeId] = {
        ...draftState[storeId],
        data: gridData,
        sortType
      };
      return;
    }
  },

  extraReducers: builder => {
    getDetailBuilder(builder);
    getPendingAuthorizationTransactionBuilder(builder);
  }
});

const pendingAuthorizationAction = {
  ...actions,
  getDetail,
  getPendingAuthorizationTransaction
};

export { pendingAuthorizationAction, reducer };
