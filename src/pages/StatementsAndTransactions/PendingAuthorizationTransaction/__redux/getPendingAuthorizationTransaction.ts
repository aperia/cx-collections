// Redux
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Helpers
import isEmpty from 'lodash.isempty';
import isUndefined from 'lodash.isundefined';
import { sortByGrid } from '../helpers';

// Types
import {
  PendingAuthorizationStateResult,
  PendingAuthorizationArg,
  PendingAuthorizationPayload,
  PendingTransactionRequest
} from '../types';

// Constants
import { SORT_PENDING_AUTHORIZATION } from '../constants';

// Services
import { pendingAuthorizationServices } from '../api-services';
import { getFeatureConfig, trimData } from 'app/helpers';
import { NO_RESULT_CODE } from 'pages/AccountSearch/AccountSearchList/constants';

export const getPendingAuthorizationTransaction = createAsyncThunk<
  PendingAuthorizationPayload,
  PendingAuthorizationArg,
  ThunkAPIConfig
>('pendingAuthorization/getPendingAuthTransactions', async (args, thunkAPI) => {
  const { accountId, startSequence, endSequence } = args.postData;

  try {
    const requestConfig = await getFeatureConfig<PendingTransactionRequest>(
      'config/pendingAuthentication.json'
    );
    const requestBody: PendingTransactionRequest = {
      ...requestConfig,
      common: {
        accountId
      },
      endSequence,
      startSequence
    };

    const { data } =
      await pendingAuthorizationServices.getPendingAuthorizationTransaction(
        requestBody
      );

    return trimData(data);
  } catch (error) {
    const errorStatus = error?.status?.toString() || error?.response?.status?.toString()
    if (errorStatus === NO_RESULT_CODE) {
      return {
        authorizations: []
      };
    }
    return thunkAPI.rejectWithValue({ errorMessage: error?.message });
  }
});

export const getPendingAuthorizationTransactionBuilder = (
  builder: ActionReducerMapBuilder<PendingAuthorizationStateResult>
) => {
  builder
    .addCase(
      getPendingAuthorizationTransaction.pending,
      (draftState, action) => {
        const { storeId } = action.meta.arg;
        draftState[storeId] = {
          ...draftState[storeId],
          isLoading: true
        };
      }
    )
    .addCase(
      getPendingAuthorizationTransaction.fulfilled,
      (draftState, action) => {
        const { storeId } = action.meta.arg;
        const data = action.payload?.authorizations;
        const dataGrid = draftState[storeId]?.data
          ? [...data, ...draftState[storeId].data!]
          : data;
        const isFirstCall = draftState[storeId]?.isFirstCall;

        if (!isFirstCall) {
          draftState[storeId] = {
            ...draftState[storeId],
            isFirstCall: true,
            noTransaction: isEmpty(data),
            error: false
          };
        }

        if (isUndefined(draftState[storeId].sortType)) {
          const sortedData = sortByGrid(
            SORT_PENDING_AUTHORIZATION[0],
            dataGrid
          );
          draftState[storeId] = {
            ...draftState[storeId],
            isLoading: false,
            data: sortedData,
            isLoadMore: data.length === 25 ? true : false,
            error: false
          };
          return;
        }
        const dataSort = sortByGrid(draftState[storeId].sortType!, dataGrid);
        draftState[storeId] = {
          ...draftState[storeId],
          isLoading: false,
          data: dataSort,
          isLoadMore: data.length === 25 ? true : false,
          error: false
        };
      }
    )
    .addCase(
      getPendingAuthorizationTransaction.rejected,
      (draftState, action) => {
        const { storeId } = action.meta.arg;
        draftState[storeId] = {
          ...draftState[storeId],
          isLoading: false,
          error: true
        };
      }
    );
};
