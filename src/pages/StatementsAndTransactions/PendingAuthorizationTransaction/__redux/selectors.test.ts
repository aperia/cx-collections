import { PendingAuthorizationTransactionInitialState } from '../types';
import { useSelectorMock } from 'app/test-utils/useSelectorMock';
import * as makeSelector from './selectors';

const storeId = '123456789';
const mockData = [
  {
    authorizationAmount: 8,
    authorizationApprovalCode: 123,
    authorizationAvailable: 123,
    authorizationCode: 123,
    authorizationDateTime: '2020, 01, 28',
    authorizationDescription: 'string',
    actionText: 'string'
  }
];

const initStateData: PendingAuthorizationTransactionInitialState = {
  [storeId]: {
    error: true,
    isLoading: false,
    isFirstCall: true,
    noTransaction: false,
    isLoadMore: true,
    sortType: { id: 'amount', order: 'asc' },
    data: mockData,
    detail: {
      isOpen: false,
      data: {},
      loading: false,
      transactionCode: '121313'
    }
  }
};

const initStateNoData: PendingAuthorizationTransactionInitialState = {};

describe('Test selector for pending authorization', () => {
  it('selectDetailData', () => {
    const state = useSelectorMock(
      makeSelector.selectDetailData,
      {
        pendingAuthorization: initStateData
      },
      storeId
    );

    expect(state).toEqual({});
  });

  it('selectDetailLoading', () => {
    const state = useSelectorMock(
      makeSelector.selectDetailLoading,
      {
        pendingAuthorization: initStateData
      },
      storeId
    );

    expect(state).toEqual(false);
  });

  it('selectDetailTransactionCode', () => {
    let state = useSelectorMock(
      makeSelector.selectDetailTransactionCode,
      { pendingAuthorization: initStateData },
      storeId
    );

    expect(state).toEqual('121313');

    state = useSelectorMock(
      makeSelector.selectDetailTransactionCode,
      { pendingAuthorization: {} },
      storeId
    );

    expect(state).toEqual('');
  });

  it('selectDetailIsOpen', () => {
    const state = useSelectorMock(
      makeSelector.selectDetailIsOpen,
      {
        pendingAuthorization: initStateData
      },
      storeId
    );

    expect(state).toEqual(false);
  });

  it('selectNoTransaction', () => {
    const state = useSelectorMock(
      makeSelector.selectNoTransaction,
      {
        pendingAuthorization: initStateData
      },
      storeId
    );

    expect(state).toEqual(false);
  });

  it('selectIsLoadingPendingAuthorization', () => {
    const state = useSelectorMock(
      makeSelector.selectIsLoadingPendingAuthorization,
      {
        pendingAuthorization: initStateData
      },
      storeId
    );

    const state1 = useSelectorMock(
      makeSelector.selectIsLoadingPendingAuthorization,
      {
        pendingAuthorization: initStateNoData
      },
      storeId
    );

    expect(state).toEqual(false);
    expect(state1).toEqual(false);
  });

  it('selectIsLoadMorePendingAuthorization', () => {
    const state = useSelectorMock(
      makeSelector.selectIsLoadMorePendingAuthorization,
      {
        pendingAuthorization: initStateData
      },
      storeId
    );

    const state1 = useSelectorMock(
      makeSelector.selectIsLoadMorePendingAuthorization,
      {
        pendingAuthorization: initStateNoData
      },
      storeId
    );

    expect(state).toEqual(true);
    expect(state1).toEqual(false);
  });

  it('selectDataPendingAuthorization', () => {
    const state = useSelectorMock(
      makeSelector.selectDataPendingAuthorization,
      {
        pendingAuthorization: initStateData
      },
      storeId
    );

    const state1 = useSelectorMock(
      makeSelector.selectDataPendingAuthorization,
      {
        pendingAuthorization: initStateNoData
      },
      storeId
    );

    expect(state).toEqual(mockData);
    expect(state1).toEqual([]);
  });

  it('selectIsError', () => {
    const state = useSelectorMock(
      makeSelector.selectIsError,
      {
        pendingAuthorization: initStateData
      },
      storeId
    );

    const state1 = useSelectorMock(
      makeSelector.selectIsError,
      {
        pendingAuthorization: initStateNoData
      },
      storeId
    );

    expect(state).toBeTruthy();
    expect(state1).toBeFalsy();
  });
});
