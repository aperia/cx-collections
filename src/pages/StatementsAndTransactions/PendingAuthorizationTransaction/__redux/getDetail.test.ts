// actions
import { pendingAuthorizationAction as action, reducer } from './reducers';
import { PendingAuthorizationTransactionInitialState } from '../types';
import { rootReducer } from 'storeConfig';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { responseDefault } from 'app/test-utils';

// Services
import { pendingAuthorizationServices } from '../api-services';

const initialState: PendingAuthorizationTransactionInitialState = {
  storeId: {
    detail: {}
  }
};

const mockData = {
  storeId: 'storeId',
  postData: { accountId: 'accountId' }
};

const storeAsync = createStore(
  rootReducer,
  {
    mapping: {
      data: {
        pendingAuthorizationDetail: {
          accountIdentifier: 'accountIdentifier',
          authorizationDate: 'authorizationDate',
          authorizationTime: 'authorizationTime'
        }
      }
    }
  },
  applyMiddleware(thunk)
);

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('Test getDetail', () => {
  it('pending', () => {
    const pendingAction = action.getDetail.pending(
      'pendingAuthorization',
      mockData
    );
    const nextState = reducer(initialState, pendingAction);
    expect(nextState.storeId.detail!.loading).toEqual(true);
  });

  it('fulfilled', () => {
    const pendingAction = action.getDetail.fulfilled(
      { data: {} },
      'pendingAuthorization',
      mockData
    );
    const nextState = reducer(initialState, pendingAction);
    expect(nextState.storeId.detail!.data).toEqual({});
  });

  it('fulfilled', () => {
    const pendingAction = action.getDetail.fulfilled(
      { data: { accountIdentifier: '1' } },
      'pendingAuthorization',
      mockData
    );
    const nextState = reducer(initialState, pendingAction);
    expect(nextState.storeId.detail!.data).toEqual({
      accountIdentifier: '1'
    });
  });

  it('reject', () => {
    const pendingAction = action.getDetail.rejected(
      null,
      'pendingAuthorization',
      mockData
    );
    const nextState = reducer(initialState, pendingAction);
    expect(nextState.storeId.detail!.loading).toEqual(false);
  });

  it('Fulfilled action', async () => {
    spy = jest
      .spyOn(pendingAuthorizationServices, 'getDetail')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          customers: [
            {
              accountIdentifier: '1',
              authorizationDate: '11/11/2000',
              authorizationTime: '11:11'
            }
          ]
        }
      });

    const storeAsync = createStore(
      rootReducer,
      {
        mapping: { data: {} }
      } as Partial<RootState>,
      applyMiddleware(thunk)
    );
    const response = await action.getDetail(mockData)(
      storeAsync.dispatch,
      storeAsync.getState,
      {}
    );
    setTimeout(() => {
      expect(response.payload).toEqual({
        data: { dateTime: '11/11/2000 11:11' }
      });
    }, 0);
  });

  it('Fulfilled action > null response', async () => {
    spy = jest
      .spyOn(pendingAuthorizationServices, 'getDetail')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    const response = await action.getDetail(mockData)(
      storeAsync.dispatch,
      storeAsync.getState,
      {}
    );

    expect(response.payload).toEqual({});
  });
});
