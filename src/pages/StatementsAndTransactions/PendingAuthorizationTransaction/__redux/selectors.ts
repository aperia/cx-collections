import { createSelector } from '@reduxjs/toolkit';
import { DataDetail, PendingAuthorizationGridData } from '../types';

export const selectDetailData = createSelector(
  (states: RootState, storeId: string) =>
    states.pendingAuthorization[storeId]?.detail?.data,
  (data: DataDetail | undefined) => data
);

export const selectDetailLoading = createSelector(
  (states: RootState, storeId: string) =>
    states.pendingAuthorization[storeId]?.detail?.loading || false,
  (loading: boolean) => loading
);

export const selectDetailIsOpen = createSelector(
  (states: RootState, storeId: string) =>
    states.pendingAuthorization[storeId]?.detail?.isOpen || false,
  (isOpen: boolean) => isOpen
);

export const selectDetailTransactionCode = createSelector(
  (states: RootState, storeId: string) =>
    states.pendingAuthorization[storeId]?.detail?.transactionCode || '',
  (transactionCode: number | string) => transactionCode
);

export const selectIsLoadingPendingAuthorization = createSelector(
  (states: RootState, storeId: string) =>
    states.pendingAuthorization[storeId]?.isLoading ?? false,
  (loading: boolean) => loading
);

export const selectIsLoadMorePendingAuthorization = createSelector(
  (states: RootState, storeId: string) =>
    states.pendingAuthorization[storeId]?.isLoadMore ?? false,
  (isLoadMore: boolean) => isLoadMore
);

export const selectDataPendingAuthorization = createSelector(
  (states: RootState, storeId: string) =>
    states.pendingAuthorization[storeId]?.data ?? [],
  (data: PendingAuthorizationGridData[]) => data
);

export const selectNoTransaction = createSelector(
  (states: RootState, storeId: string) =>
    states.pendingAuthorization[storeId]?.noTransaction || false,
  (noTransaction: boolean) => noTransaction
);

export const selectIsError = createSelector(
  (states: RootState, storeId: string) =>
    states.pendingAuthorization[storeId]?.error || false,
  (error: boolean) => error
);
