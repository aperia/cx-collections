import { pendingAuthorizationAction as action } from './reducers';
import { createStore, Store } from 'redux';

// Services
import { pendingAuthorizationServices } from '../api-services';
import * as commonHelper from 'app/helpers/commons';
import {
  responseDefault,
  storeId as mockStoreId,
  storeId
} from 'app/test-utils';
import { rootReducer } from 'storeConfig';

const requestData = {
  storeId: mockStoreId,
  postData: {
    accountId: 'accountId',
    endSequence: 25,
    startSequence: 0
  }
};

const mockDataResponse = [{ authorizationAmount: '10.0' }];

const mock25DataResponse: any = Array.from({ length: 25 }).fill({
  authorizationAmount: '10.0'
});

let spy: jest.SpyInstance;

let state: RootState;

const spyConfig = jest
  .spyOn(commonHelper, 'getFeatureConfig')
  .mockResolvedValue({});

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('Test getPendingAuthorizationTransaction', () => {
  let store: Store<RootState>;
  beforeEach(() => {
    store = createStore(rootReducer, {
      pendingAuthorization: { [storeId]: { isFirstCall: true } }
    });
  });

  afterAll(() => {
    spyConfig.mockRestore();
  });

  it('fulfilled with Data', async () => {
    spy = jest
      .spyOn(pendingAuthorizationServices, 'getPendingAuthorizationTransaction')
      .mockResolvedValue({
        ...responseDefault,
        data: { authorizations: mockDataResponse }
      });
    await action.getPendingAuthorizationTransaction(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(store.getState().pendingAuthorization[mockStoreId].data).toEqual(
      mockDataResponse
    );
  });

  it('fulfilled with 25 data length', async () => {
    spy = jest
      .spyOn(pendingAuthorizationServices, 'getPendingAuthorizationTransaction')
      .mockResolvedValue({
        ...responseDefault,
        data: { authorizations: mock25DataResponse }
      });
    await action.getPendingAuthorizationTransaction(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(store.getState().pendingAuthorization[mockStoreId].data).toEqual(
      mock25DataResponse
    );
  });

  it('reject action with empty data - fiserv api template response for empty case', async () => {
    spy = jest
      .spyOn(pendingAuthorizationServices, 'getPendingAuthorizationTransaction')
      .mockRejectedValue({
        ...responseDefault,
        data: { message: 'RECORD NOT FOUND' }
      });
    await action.getPendingAuthorizationTransaction(requestData)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(
      store.getState().pendingAuthorization[mockStoreId].data
    ).toBeUndefined();
  });

  it('reject with error : network, other errors', async () => {
    spy = jest
      .spyOn(pendingAuthorizationServices, 'getPendingAuthorizationTransaction')
      .mockRejectedValue({
        ...responseDefault,
        data: {}
      });
    await action.getPendingAuthorizationTransaction(requestData)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(store.getState().pendingAuthorization[mockStoreId].error).toEqual(
      true
    );
  });

  it('reject with error : 455', async () => {
    spy = jest
      .spyOn(pendingAuthorizationServices, 'getPendingAuthorizationTransaction')
      .mockImplementation(() => {
        throw {
          response: { status: 455 },
          data: { message: 'NO TRANSACTION ADJUSTMENT REQUEST' }
        };
      });
    const response = await action.getPendingAuthorizationTransaction(
      requestData
    )(store.dispatch, store.getState, {});

    expect(response.payload as any).toEqual({ authorizations: [] });
  });
});

describe('should be getPendingAuthorizationTransactionBuilder', () => {
  it('should response getPendingAuthorizationTransaction pending', () => {
    const pendingAction = action.getPendingAuthorizationTransaction.pending(
      action.getPendingAuthorizationTransaction.pending.type,
      requestData
    );
    const actual = rootReducer(
      {
        ...state,
        pendingAuthorization: {
          [mockStoreId]: {
            data: mockDataResponse,
            sortType: { id: '1', order: 'desc' }
          }
        }
      },
      pendingAction
    );

    expect(actual.pendingAuthorization[mockStoreId].isLoading).toEqual(true);
  });

  it('should response getPendingAuthorizationTransaction fulfilled with 25 items length', () => {
    const fulfilled = action.getPendingAuthorizationTransaction.fulfilled(
      { authorizations: mock25DataResponse },
      action.getPendingAuthorizationTransaction.pending.type,
      requestData
    );
    const actual = rootReducer(
      {
        ...state,
        pendingAuthorization: {
          [mockStoreId]: {
            data: mock25DataResponse,
            sortType: { id: '1', order: 'desc' }
          }
        }
      },
      fulfilled
    );

    expect(actual.pendingAuthorization[mockStoreId].isLoading).toEqual(false);
  });

  it('should response getPendingAuthorizationTransaction fulfilled ', () => {
    const fulfilled = action.getPendingAuthorizationTransaction.fulfilled(
      { authorizations: mockDataResponse },
      action.getPendingAuthorizationTransaction.pending.type,
      requestData
    );
    const actual = rootReducer(
      {
        ...state,
        pendingAuthorization: {
          [mockStoreId]: {
            data: mockDataResponse,
            sortType: { id: '1', order: 'desc' }
          }
        }
      },
      fulfilled
    );

    expect(actual.pendingAuthorization[mockStoreId].isLoading).toEqual(false);
  });

  it('should response getPendingAuthorizationTransaction rejected', () => {
    const rejected = action.getPendingAuthorizationTransaction.rejected(
      null,
      action.getPendingAuthorizationTransaction.rejected.type,
      requestData
    );
    const actual = rootReducer(
      {
        ...state,
        pendingAuthorization: {
          [mockStoreId]: {
            data: mockDataResponse,
            sortType: { id: '1', order: 'desc' }
          }
        }
      },
      rejected
    );

    expect(actual.pendingAuthorization[mockStoreId].isLoading).toEqual(false);
  });
});
