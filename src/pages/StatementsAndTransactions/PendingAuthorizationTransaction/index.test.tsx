import React from 'react';
import { AccountDetailProvider } from 'app/hooks';
import {
  storeId,
  renderMockStore,
  mockActionCreator,
  renderMockStoreId
} from 'app/test-utils';
// component
import PendingAuthorizationTransaction from 'pages/StatementsAndTransactions/PendingAuthorizationTransaction';

import { I18N_PENDING_AUTH } from './constants';
import userEvent from '@testing-library/user-event';

import { fireEvent, screen } from '@testing-library/react';
import { pendingAuthorizationAction } from './__redux/reducers';
import { SortType } from 'app/_libraries/_dls/components/Grid/types';

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <PendingAuthorizationTransaction />
    </AccountDetailProvider>,
    { initialState },
    true
  );
};
const date = `${new Date()}`;
const mockData = [
  {
    authorizationAmount: 2403,
    authorizationApprovalCode: 'DAS002',
    authorizationAvailable: 65264,
    authorizationCode: 184527826,
    authorizationDateTime: date,
    authorizationDescription: 'GOL DIN WASTE SERVICES'
  }
];

const mock25Data: any = Array.from({ length: 25 }).fill({
  authorizationAmount: 2403,
  authorizationApprovalCode: 'DAS002',
  authorizationAvailable: 65264,
  authorizationCode: 184527826,
  authorizationDateTime: date,
  authorizationDescription: 'GOL DIN WASTE SERVICES'
});

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponent = jest.requireActual('app/_libraries/_dls/components');
  const { MockGrid } = jest.requireActual('app/test-utils/mocks/MockGrid');

  return {
    ...dlsComponent,
    Grid: MockGrid
  };
});

const mockAction = mockActionCreator(pendingAuthorizationAction);

describe('Test GridViewHistory component', () => {
  it('Render UI with data', () => {
    const initialState: object = {
      pendingAuthorization: {
        [storeId]: {
          data: mockData,
          isFirstCall: true,
          isLoadMore: true,
          isLoading: false,
          noTransaction: false
        }
      }
    };

    const { wrapper } = renderWrapper(initialState);

    const title = wrapper.getByText(I18N_PENDING_AUTH.PENDING_AUTHORIZATIONS);

    const dataName = wrapper.getByText('DAS002');

    const dataValue = wrapper.getByText('$2,403.00');

    expect(dataName).toBeInTheDocument();

    expect(dataValue).toBeInTheDocument();

    expect(title).toBeInTheDocument();
  });

  it('Render UI with empty data', () => {
    const initialState: object = {
      pendingAuthorization: {
        [storeId]: {
          data: [],
          isFirstCall: false,
          isLoadMore: false,
          isLoading: false,
          noTransaction: true
        }
      }
    };

    const { wrapper } = renderWrapper(initialState);

    const text = wrapper.getByText(I18N_PENDING_AUTH.NO_TRANSACTION_TO_DISPLAY);

    expect(text).toBeInTheDocument();
  });

  it('Render UI with error message', () => {
    const initialState: object = {
      pendingAuthorization: {
        [storeId]: {
          error: 'error',
          data: [],
          isFirstCall: false,
          isLoadMore: false,
          isLoading: false,
          noTransaction: false
        }
      }
    };

    const { wrapper } = renderWrapper(initialState);

    const error = wrapper.getByText(
      'txt_data_load_unsuccessful_click_reload_to_try_again'
    );

    const error2 = wrapper.getByText('txt_failure_ocurred_on');

    expect(error).toBeInTheDocument();

    expect(error2).toBeInTheDocument();
  });

  it('Render UI with error message', () => {
    const mockGetPendingAuthorizationTransaction = mockAction(
      'getPendingAuthorizationTransaction'
    );
    const initialState: object = {
      pendingAuthorization: {
        [storeId]: {
          error: 'error',
          data: [],
          isFirstCall: false,
          isLoadMore: false,
          isLoading: false,
          noTransaction: false
        }
      }
    };

    const { wrapper } = renderWrapper(initialState);

    const reloadButton = wrapper.getByRole('button', { name: 'txt_reload' });

    userEvent.click(reloadButton);

    expect(mockGetPendingAuthorizationTransaction).toBeCalledWith({
      storeId,
      postData: {
        accountId: '123456789',
        startSequence: 1,
        endSequence: 25
      }
    });
  });

  it('Render UI and click view button', () => {
    const date = `${new Date()}`;
    const initialState: object = {
      pendingAuthorization: {
        [storeId]: {
          data: [
            {
              authorizationAmount: 2403,
              authorizationApprovalCode: 'DAS002',
              authorizationAvailable: 65264,
              authorizationCode: 184527826,
              authorizationDateTime: date,
              authorizationDescription: 'GOL DIN WASTE SERVICES'
            }
          ],
          isFirstCall: false,
          isLoadMore: false,
          isLoading: false,
          noTransaction: false
        }
      }
    };

    const { wrapper } = renderWrapper(initialState);

    const viewButton = wrapper.getByRole('button', { name: 'txt_view' });

    userEvent.click(viewButton);

    const text = wrapper.getByText(
      I18N_PENDING_AUTH.PENDING_AUTHORIZATION_DETAILS
    );

    const text2 = wrapper.getByText(I18N_PENDING_AUTH.MERCHANT);

    const text3 = wrapper.getByText(I18N_PENDING_AUTH.OTHER);

    const closeButton = wrapper.getByText('txt_close');

    expect(text).toBeInTheDocument();

    expect(text2).toBeInTheDocument();

    expect(text3).toBeInTheDocument();

    expect(closeButton).toBeInTheDocument();
  });

  it('Should have handleScrollToTop', () => {
    const initialState: object = {
      pendingAuthorization: {
        [storeId]: {
          data: [
            {
              authorizationAmount: 2403,
              authorizationApprovalCode: 'DAS002',
              authorizationAvailable: 65264,
              authorizationCode: 184527826,
              authorizationDateTime: date,
              authorizationDescription: 'GOL DIN WASTE SERVICES'
            }
          ],
          isFirstCall: false,
          isLoadMore: false,
          isLoading: false,
          noTransaction: false
        }
      }
    };

    Object.defineProperty(window, 'innerHeight', {
      writable: true,
      value: 303
    });

    renderMockStoreId(<PendingAuthorizationTransaction />, {
      initialState
    });
    const queryTextEndOfTransactionList = screen.queryByText(
      'txt_end_of_transaction_list'
    );
    const queryBackToTop = screen.queryByText('txt_back_to_top');
    queryBackToTop!.click();

    expect(queryTextEndOfTransactionList).toBeInTheDocument();
  });

  it('render with isLoadMore is false', () => {
    const initialState: object = {
      pendingAuthorization: {
        [storeId]: {
          data: [
            {
              authorizationAmount: 2403,
              authorizationApprovalCode: 'DAS002',
              authorizationAvailable: 65264,
              authorizationCode: 184527826,
              authorizationDateTime: date,
              authorizationDescription: 'GOL DIN WASTE SERVICES'
            }
          ],
          isFirstCall: false,
          isLoadMore: false,
          isLoading: false,
          noTransaction: false
        }
      }
    };
    const getPendingAuthorizationTransaction = mockAction(
      'getPendingAuthorizationTransaction'
    );

    renderMockStoreId(<PendingAuthorizationTransaction />, {
      initialState
    });

    screen.getByTestId('Grid_onLoadMore').click();

    //expect
    expect(getPendingAuthorizationTransaction).toBeCalledWith({
      storeId,
      postData: {
        accountId: 'accEValue',
        startSequence: 1,
        endSequence: 25
      }
    });
  });

  it('render with isLoadMore is true', () => {
    const initialState: object = {
      pendingAuthorization: {
        [storeId]: {
          data: [
            {
              authorizationAmount: 2403,
              authorizationApprovalCode: 'DAS002',
              authorizationAvailable: 65264,
              authorizationCode: 184527826,
              authorizationDateTime: date,
              authorizationDescription: 'GOL DIN WASTE SERVICES'
            }
          ],
          isFirstCall: false,
          isLoadMore: true,
          isLoading: false,
          noTransaction: false
        }
      }
    };
    const getPendingAuthorizationTransaction = mockAction(
      'getPendingAuthorizationTransaction'
    );

    renderMockStoreId(<PendingAuthorizationTransaction />, {
      initialState
    });

    screen.getByTestId('Grid_onLoadMore').click();

    //expect
    expect(getPendingAuthorizationTransaction).toBeCalledTimes(2);
  });

  it('handleSortGrid', () => {
    const initialState: object = {
      pendingAuthorization: {
        [storeId]: {
          data: mock25Data,
          isFirstCall: false,
          isLoadMore: true,
          isLoading: false,
          noTransaction: false
        }
      }
    };
    const sortByTransaction = mockAction('sortByTransaction');

    const sortType: SortType = { id: 'id', order: 'desc' };

    renderMockStoreId(<PendingAuthorizationTransaction />, {
      initialState
    });

    // simulate
    fireEvent.change(screen.getByTestId('Grid_onSortChange'), {
      target: { value: 'undefined', ...sortType }
    });

    //expect
    expect(sortByTransaction).toBeCalledWith({ storeId, sortType });
  });
});
