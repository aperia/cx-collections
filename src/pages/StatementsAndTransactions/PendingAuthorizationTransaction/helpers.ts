import { TRANSACTION_SOURCE_IDENTIFIER_METHOD_CODE } from './../../../app/constants/index';
import { SortType } from 'app/_libraries/_dls/components';

// Constants
import { ID_SORT_GRID } from './constants';

// Types
import {
  DataDetail,
  PendingAuthorizationData,
  PendingAuthorizationGridData
} from './types';

// Helpers
import {
  formatAPICurrency,
  formatCommon,
  parseJSONString,
  prepareDateStringFormat
} from 'app/helpers';
import {
  AUTHORIZATION_LINE_ID_METHOD_CODE,
  AUTHORIZATION_TYPE_CODE_METHOD_CODE,
  NA_AVS,
  NA_CVV_VERIFICATION_METHOD_CODE,
  NA_PIN_VERIFICATION_METHOD_CODE
} from 'app/constants';

export const combineDateAndTime = (
  date?: string,
  time?: string
): string | undefined => {
  if (!date || !time) return undefined;

  const hours =
    Number(time.substr(0, 2)) > 9
      ? `${Number(time.substr(0, 2))}`
      : `0${isNaN(Number(time.substr(0, 2))) ? 0 : Number(time.substr(0, 2))}`;
  const min =
    Number(time.substr(2, 2)) > 9
      ? `${Number(time.substr(2, 2))}`
      : `0${isNaN(Number(time.substr(2, 2))) ? 0 : Number(time.substr(2, 2))}`;
  const sec =
    Number(time.substr(4, 2)) > 9
      ? `${Number(time.substr(4, 2))}`
      : `0${isNaN(Number(time.substr(4, 2))) ? 0 : Number(time.substr(4, 2))}`;

  const dateInstance = `${date}T${hours}:${min}:${sec}`;

  return formatCommon(dateInstance).time.allDatetime;
};

export const formatGridData = (
  data: PendingAuthorizationData[]
): PendingAuthorizationGridData[] => {
  return data.map((item: PendingAuthorizationData) => {
    return {
      ...item,
      authorizationAmount: formatAPICurrency(item.authorizationAmount || ''),
      beforeAuthorizationAvailableCreditAmount: formatAPICurrency(
        item.beforeAuthorizationAvailableCreditAmount || ''
      ),
      authorizationDateTime: combineDateAndTime(
        prepareDateStringFormat(item.authorizationDate || '', 'YYYY_MM_DD'),
        item.authorizationTime
      )?.toString()
    };
  });
};

export const sortByGrid = (
  sortType: SortType,
  data: PendingAuthorizationGridData[]
) => {
  const gridData = [...data];
  switch (sortType.id) {
    case ID_SORT_GRID.AMOUNT:
      if (!sortType.order || sortType.order === 'desc') {
        return gridData.sort(
          (currentItem, nextItem) =>
            Number(nextItem.authorizationAmount || 0) -
            Number(currentItem.authorizationAmount || 0)
        );
      }
      return gridData.sort(
        (currentItem, nextItem) =>
          Number(currentItem.authorizationAmount || 0) -
          Number(nextItem.authorizationAmount || 0)
      );
    case ID_SORT_GRID.DATE_TIME:
      if (!sortType.order || sortType.order === 'desc') {
        return gridData.sort(
          (currentItem, nextItem) =>
            new Date(nextItem?.authorizationDateTime || '').getTime() -
            new Date(currentItem?.authorizationDateTime || '').getTime()
        );
      }
      return gridData.sort(
        (currentItem, nextItem) =>
          new Date(currentItem?.authorizationDateTime || '').getTime() -
          new Date(nextItem?.authorizationDateTime || '').getTime()
      );
    default:
      return data;
  }
};

export const prepareViewDetailData = (data: DataDetail, t: Function) => {
  const mapTranslateData = (resources: Record<string, string>, key: string) => {
    return resources[key] ? `${key} - ${t(resources[key])}` : key;
  };
  return {
    ...data,
    transactedAccountNumber: parseJSONString(data?.transactedAccountNumber)
      ?.maskedValue,
    enteredIdentifier: parseJSONString(data?.enteredIdentifier)?.maskedValue,
    merchantNumber: parseJSONString(data?.merchantNumber)?.maskedValue,
    authorizationDateTime: formatCommon(data.authorizationDateTime!).time
      .allDatetime,
    authorizationLineId: mapTranslateData(
      AUTHORIZATION_LINE_ID_METHOD_CODE,
      data.authorizationLineId?.trim?.() || ''
    ),
    transactionSourceIdentifier: mapTranslateData(
      TRANSACTION_SOURCE_IDENTIFIER_METHOD_CODE,
      data.transactionSourceIdentifier?.trim?.() || ''
    ),
    cardholderAuthenticationVerificationValue: mapTranslateData(
      NA_CVV_VERIFICATION_METHOD_CODE,
      data.cardholderAuthenticationVerificationValue?.trim?.() || ''
    ),
    transactionCode: mapTranslateData(
      AUTHORIZATION_TYPE_CODE_METHOD_CODE,
      data.transactionCode?.trim?.() || ''
    ),
    pinVerificationCode: mapTranslateData(
      NA_PIN_VERIFICATION_METHOD_CODE,
      data.pinVerificationCode?.trim?.() || ''
    ),
    addressVerificationServiceResponseCode: mapTranslateData(
      NA_AVS,
      data.addressVerificationServiceResponseCode?.trim?.() || ''
    )
  };
};
