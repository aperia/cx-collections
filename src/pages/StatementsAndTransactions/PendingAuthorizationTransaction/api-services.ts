import {
  PendingTransactionDetailPayload,
  PendingTransactionRequest
} from './types';
import { apiService } from 'app/utils/api.service';

class PendingAuthorizationServices {
  getDetail(postData: PendingTransactionDetailPayload) {
    const { accountId, transactionCode } = postData;
    const url =
      window.appConfig?.api?.pendingAuthorizations
        ?.getPendingAuthTransactionDetail;

    return apiService.post(url!, { accountId, transactionCode });
  }

  getPendingAuthorizationTransaction(postData: PendingTransactionRequest) {
    const url =
      window.appConfig?.api?.pendingAuthorizations?.getPendingAuthTransactions;
    return apiService.post(url!, postData);
  }
}

export const pendingAuthorizationServices = new PendingAuthorizationServices();
