import React, { useMemo } from 'react';

// components
import { View } from 'app/_libraries/_dof/core';
import {
  Modal,
  ModalHeader,
  ModalTitle,
  ModalBody,
  ModalFooter
} from 'app/_libraries/_dls/components';

// constants
import { I18N_PENDING_AUTH, INFO, MERCHANT, OTHER } from '../constants';

// redux store
import { useDispatch } from 'react-redux';
import { pendingAuthorizationAction as action } from '../__redux/reducers';
import {
  selectDetailLoading as selectLoading,
  selectDetailData as selectData,
  selectDetailIsOpen as selectIsOpen
} from '../__redux/selectors';

// helper
import classnames from 'classnames';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { DataDetail } from '../types';
import isEmpty from 'lodash.isempty';
import { prepareViewDetailData } from '../helpers';

//hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface ModalProps {}

const DetailModal: React.FC<ModalProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();

  const data = useStoreIdSelector<DataDetail>(selectData);
  const loading = useStoreIdSelector<boolean>(selectLoading);
  const isOpenModal = useStoreIdSelector<boolean>(selectIsOpen);
  const testId = 'pendingAuthorizationDetailInfo';

  const handleClose = () => dispatch(action.toggleDetailModal({ storeId }));

  const infoView = useMemo(() => {
    if (isEmpty(data)) return null;
    return (
      <View
        id={`${storeId}_${INFO}`}
        formKey={`${storeId}_${INFO}_view`}
        descriptor={INFO}
        value={prepareViewDetailData(data, t)}
      />
    );
  }, [data, storeId, t]);

  const merchantView = useMemo(() => {
    if (isEmpty(data)) return null;
    return (
      <View
        id={`${storeId}_${MERCHANT}`}
        formKey={`${storeId}_${MERCHANT}_view`}
        descriptor={MERCHANT}
        value={prepareViewDetailData(data, t)}
      />
    );
  }, [data, storeId, t]);

  const otherView = useMemo(() => {
    if (isEmpty(data)) return null;
    return (
      <View
        id={`${storeId}_${OTHER}`}
        formKey={`${storeId}_${OTHER}_view`}
        descriptor={OTHER}
        value={prepareViewDetailData(data, t)}
      />
    );
  }, [data, storeId, t]);

  return (
    <Modal lg show={isOpenModal} className="window-lg-full" dataTestId={testId}>
      <ModalHeader
        border
        closeButton
        onHide={handleClose}
        dataTestId={`${testId}-header`}
      >
        <ModalTitle dataTestId={`${testId}-title`}>
          {t(I18N_PENDING_AUTH.PENDING_AUTHORIZATION_DETAILS)}
        </ModalTitle>
      </ModalHeader>
      <ModalBody noPadding className={classnames({ loading })}>
        <div className="bg-light-l20 border-bottom px-24 pb-24 pt-8">
          {infoView}
        </div>
        <div className="px-24 pt-24">
          <h5 data-testid={genAmtId(testId, 'merchant', '')}>
            {t(I18N_PENDING_AUTH.MERCHANT)}
          </h5>
          {merchantView}
          <h5 className="mt-24" data-testid={genAmtId(testId, 'other', '')}>
            {t(I18N_PENDING_AUTH.OTHER)}
          </h5>
          {otherView}
        </div>
      </ModalBody>
      <ModalFooter
        cancelButtonText={`${t('txt_close')}`}
        onCancel={handleClose}
        dataTestId={`${testId}-footer`}
      />
    </Modal>
  );
};

export default DetailModal;
