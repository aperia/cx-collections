import React from 'react';
// components
import DetailModal from './index';

import { AccountDetailProvider } from 'app/hooks';
import { mockActionCreator, renderMockStore, storeId } from 'app/test-utils';
import { I18N_PENDING_AUTH } from '../constants';
import userEvent from '@testing-library/user-event';
import { pendingAuthorizationAction } from '../__redux/reducers';

const renderComponentWithMockProvider = (initialState: object) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <DetailModal />
    </AccountDetailProvider>,
    {
      initialState
    },
    true
  );
};

const mockAction = mockActionCreator(pendingAuthorizationAction);

describe('Test GridViewHistory component', () => {
  it('Render UI', () => {
    const initialState: object = {
      pendingAuthorization: {
        [storeId]: {
          detail: { isOpen: true, loading: false }
        }
      }
    };

    const { wrapper } = renderComponentWithMockProvider(initialState);

    const text = wrapper.getByText(
      I18N_PENDING_AUTH.PENDING_AUTHORIZATION_DETAILS
    );

    const text2 = wrapper.getByText(I18N_PENDING_AUTH.MERCHANT);

    const text3 = wrapper.getByText(I18N_PENDING_AUTH.OTHER);

    const closeButton = wrapper.getByText('txt_close');

    expect(text).toBeInTheDocument();

    expect(text2).toBeInTheDocument();

    expect(text3).toBeInTheDocument();

    expect(closeButton).toBeInTheDocument();
  });

  it('Should close when trigger cancel button without callback', () => {
    const mockToggle = mockAction('toggleDetailModal');
    const initialState = {
      pendingAuthorization: {
        [storeId]: { detail: { isOpen: true, loading: false } }
      }
    };
    const { wrapper } = renderComponentWithMockProvider(initialState);

    const closeButton = wrapper.getByText('txt_close');

    userEvent.click(closeButton);

    expect(mockToggle).toBeCalledWith({
      storeId
    });
  });
});
