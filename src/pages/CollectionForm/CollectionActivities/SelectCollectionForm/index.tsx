// i18n
import { I18N_COLLECTION_FORM, I18N_COMMON_TEXT } from 'app/constants/i18n';
// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { isDefaultCallResultType } from 'pages/CollectionForm/helpers';
import { hardshipActions } from 'pages/HardShip/_redux/reducers';
import { selectIsOpenConfirmModal } from 'pages/HardShip/_redux/selectors';
import React from 'react';
import { useDispatch } from 'react-redux';
// Type
import { CallResult } from '../../types';
// Redux
import {
  selectIsHavingCCCSDataAcceptProposal,
  takeDefaultCallResultRefData,
  takeSelectedCallResult
} from '../../_redux/selectors';
// Helper
import { triggerOpenCollectionForm } from '../helper';
// components
import CallResultDropdownList from './CallResultDropdownList';

const SelectCollectionForm: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { storeId } = useAccountDetail();

  const callResultSelected = useStoreIdSelector<CallResult>(
    takeSelectedCallResult
  );

  const defaultRefData = useStoreIdSelector<Array<CallResult>>(
    takeDefaultCallResultRefData
  );

  const isHavingCCCSDataAcceptProposal = useStoreIdSelector(
    selectIsHavingCCCSDataAcceptProposal
  );

  const isOpenModal = useStoreIdSelector<boolean>(selectIsOpenConfirmModal);

  const handleStartCollectionForm = () => {
    if (
      callResultSelected?.code === CALL_RESULT_CODE.HARDSHIP &&
      isHavingCCCSDataAcceptProposal
    ) {
      dispatch(
        hardshipActions.toggleConfirmModal({
          storeId,
          opened: true
        })
      );
      return;
    }
    dispatch(triggerOpenCollectionForm());
  };

  const handleConfirmSubmit = () => {
    handleCloseModal();
    dispatch(triggerOpenCollectionForm());
  };

  const handleCloseModal = () => {
    dispatch(
      hardshipActions.toggleConfirmModal({
        storeId,
        opened: false
      })
    );
  };

  return (
    <>
      <div>
        <div className="bg-light-l20 px-24 py-16">
          <p data-testid={genAmtId('SelectCollectionForm', 'help-text', '')}>
            {t(I18N_COMMON_TEXT.COLLECTION_FORM_HELP_TEXT)}
          </p>
          <div className="mt-16">
            <CallResultDropdownList />
          </div>
          <div className="text-right mt-24">
            <Button
              dataTestId={genAmtId('SelectCollectionForm', 'submit-btn', '')}
              disabled={!callResultSelected}
              onClick={handleStartCollectionForm}
              size="sm"
            >
              {t(
                isDefaultCallResultType(
                  callResultSelected?.code,
                  defaultRefData
                ) || !callResultSelected
                  ? I18N_COMMON_TEXT.CONTINUE
                  : I18N_COMMON_TEXT.SUBMIT
              )}
            </Button>
          </div>
        </div>
        <hr className="mt-0" />
      </div>
      <Modal
        show={isOpenModal}
        xs
        dataTestId={genAmtId('SelectCollectionForm', 'confirm-modal', '')}
      >
        <ModalHeader
          border
          closeButton
          onHide={handleCloseModal}
          dataTestId={genAmtId(
            'SelectCollectionForm',
            'confirm-modal-header',
            ''
          )}
        >
          <ModalTitle
            dataTestId={genAmtId(
              'SelectCollectionForm',
              'confirm-modal-title',
              ''
            )}
          >
            {t(I18N_COLLECTION_FORM.CONFIRM_CHANGE_CALL_RESULT)}
          </ModalTitle>
        </ModalHeader>
        <ModalBody>
          <p data-testid={genAmtId('SelectCollectionForm', 'content', '')}>
            {t(I18N_COLLECTION_FORM.CONFIRM_SUBMIT_HARDSHIP_BODY)}
          </p>
        </ModalBody>
        <ModalFooter
          dataTestId={genAmtId(
            'SelectCollectionForm',
            'confirm-modal-footer',
            ''
          )}
          cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
          okButtonText={t(I18N_COMMON_TEXT.CONTINUE)}
          onCancel={handleCloseModal}
          onOk={handleConfirmSubmit}
        />
      </Modal>
    </>
  );
};

export default SelectCollectionForm;
