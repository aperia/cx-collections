import React from 'react';
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import { fireEvent } from '@testing-library/react';
import SelectCollectionForm from '.';
import { CCCS_ACTIONS } from 'pages/CCCS/constants';
import { hardshipActions } from 'pages/HardShip/_redux/reducers';

jest.mock('../helper', () => ({
  triggerOpenCollectionForm: () => () => jest.fn()
}));

const initialState: Partial<RootState> = {
  cccs: {
    [storeId]: {
      data: {
        action: CCCS_ACTIONS.ACCEPT_PROPOSAL
      }
    }
  },
  hardship: {
    [storeId]: {
      showConfirmModal: true
    }
  },
  collectionForm: {
    [storeId]: {
      selectedCallResult: {
        code: 'HS',
        description: 'des'
      },
      defaultCallResultRefData: [
        { code: 'code', description: 'des' },
        { code: 'code', description: 'des' }
      ]
    }
  },
  accountDetail: {
    [storeId]: {
      apiErrorSection: {
        errorDetail: {
          collectionForm: {
            request: '500',
            response: '500'
          }
        }
      }
    }
  }
};

const stateError: Partial<RootState> = {
  cccs: {
    [storeId]: {
      data: {
        action: CCCS_ACTIONS.ACCEPT_PROPOSAL
      }
    }
  },
  hardship: {
    [storeId]: {
      showConfirmModal: true
    }
  },
  collectionForm: {
    [storeId]: {
      selectedCallResult: {
        code: 'code',
        description: 'des'
      },
      defaultCallResultRefData: [
        { code: 'code', description: 'des' },
        { code: 'code', description: 'des' }
      ]
    }
  },
  accountDetail: {
    [storeId]: {
      apiErrorSection: {
        errorDetail: {
          collectionForm: {
            request: '500',
            response: '500'
          }
        }
      }
    }
  }
};

const hardshipActionsSpy = mockActionCreator(hardshipActions);

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<SelectCollectionForm />, {
    initialState
  });
};

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('SelectCollectionForm component', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterAll(() => {
    jest.runAllTimers();
  });

  it('render component', () => {
    const { wrapper } = renderWrapper(initialState);
    expect(
      wrapper.getByText('txt_collection_form_help_text')
    ).toBeInTheDocument();
  });

  it('handleCloseModal', () => {
    const spy = hardshipActionsSpy('toggleConfirmModal');
    const { wrapper } = renderWrapper(initialState);
    const buttonCancel = wrapper.getByText('txt_cancel');
    fireEvent.click(buttonCancel);
    expect(spy).toBeCalledWith({
      storeId,
      opened: false
    });
  });

  it('handleConfirmSubmit', () => {
    const { wrapper } = renderWrapper(initialState);
    const buttonOk = wrapper.getAllByText('txt_continue')[0];
    fireEvent.click(buttonOk);
    expect(
      wrapper.getByText('txt_confirm_change_call_result')
    ).toBeInTheDocument();
  });

  it('handleStartCollectionForm', () => {
    const spy = hardshipActionsSpy('toggleConfirmModal');
    const { wrapper } = renderWrapper(initialState);
    const buttonOk = wrapper.getByText('txt_submit');
    fireEvent.click(buttonOk);
    expect(spy).toBeCalledWith({
      storeId,
      opened: true
    });
  });

  it('handleStartCollectionForm with another case', () => {
    const spy = hardshipActionsSpy('toggleConfirmModal');
    const { wrapper } = renderWrapper(stateError);
    const buttonOk = wrapper.getAllByText('txt_continue')[0];
    fireEvent.click(buttonOk);
    expect(spy).toBeCalledTimes(0);
  });
});
