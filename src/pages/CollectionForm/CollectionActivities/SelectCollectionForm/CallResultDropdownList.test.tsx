import React from 'react';
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import { fireEvent } from '@testing-library/react';
import CallResultDropdownList from './CallResultDropdownList';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import {
  HARDSHIP_COMPLETE,
  HARDSHIP_IN_PROGRESS
} from '../../../HardShip/constants';

const initialState: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      selectedCallResult: {
        code: CALL_RESULT_CODE.BANKRUPTCY,
        description: 'des'
      },
      callResultRefData: [
        { code: CALL_RESULT_CODE.CCCS, description: 'des' },
        { code: CALL_RESULT_CODE.BANKRUPTCY, description: 'des' },
        { code: CALL_RESULT_CODE.BANKRUPTCY_PENDING, description: 'des' },
        { code: CALL_RESULT_CODE.NOT_BANKRUPTCY, description: 'des' },
        { code: CALL_RESULT_CODE.DECEASE, description: 'des' },
        { code: CALL_RESULT_CODE.NOT_DECEASED, description: 'des' },
        { code: CALL_RESULT_CODE.HARDSHIP, description: 'des' },
        { code: CALL_RESULT_CODE.NOT_HARDSHIP, description: 'des' },
        { code: CALL_RESULT_CODE.INCARCERATION, description: 'des' },
        { code: CALL_RESULT_CODE.NOT_INCARCERATION, description: 'des' },
        { code: CALL_RESULT_CODE.LONG_TERM_MEDICAL, description: 'des' },
        { code: CALL_RESULT_CODE.NOT_LONG_TERM_MEDICAL, description: 'des' },
        { code: CALL_RESULT_CODE.PROMISE_TO_PAY, description: 'des' },
        { code: CALL_RESULT_CODE.REGULATORY_SPOUSE_CONTACT, description: 'des' }
      ],
      defaultCallResultRefData: [
        { code: CALL_RESULT_CODE.BANKRUPTCY, description: 'des' },
        { code: 'code', description: 'des' }
      ],
      lastFollowUpData: {
        callResultType: 'type'
      },
      lastDelayCallResult: CALL_RESULT_CODE.BANKRUPTCY
    }
  },
  deceased: {
    [storeId]: {
      deceasedInformation: {
        deceasedConfirmation: 'Deceased Confirmed'
      }
    }
  },
  bankruptcy: {
    [storeId]: {
      rawData: {
        mainInfo: {
          case: '123'
        }
      },
      info: {
        actionEntryCode: CALL_RESULT_CODE.BANKRUPTCY
      }
    }
  },
  hardship: {
    [storeId]: {
      data: {
        processStatus: HARDSHIP_COMPLETE
      }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<CallResultDropdownList />, {
    initialState
  });
};

HTMLCanvasElement.prototype.getContext = jest.fn();

const collectionFormActionsSpy = mockActionCreator(collectionFormActions);

describe('SelectCollectionForm component', () => {
  beforeEach(() => {
    jest.useFakeTimers();
    collectionFormActionsSpy('getCallResultsRefData');
  });

  afterAll(() => {
    jest.runAllTimers();
  });

  it('render component', () => {
    const { wrapper } = renderWrapper(initialState);
    expect(wrapper.getByText('txt_call_result')).toBeInTheDocument();
  });

  it('render component with empty hardship', () => {
    const { wrapper } = renderWrapper({
      ...initialState,
      hardship: {
        [storeId]: {
          data: {}
        }
      }
    });
    expect(wrapper.getByText('txt_call_result')).toBeInTheDocument();
  });

  it('render component with empty deceased', () => {
    const { wrapper } = renderWrapper({
      ...initialState,
      deceased: {
        [storeId]: {
          deceasedInformation: {
            deceasedConfirmation: 'Not Deceased'
          }
        }
      }
    });
    expect(wrapper.getByText('txt_call_result')).toBeInTheDocument();
  });

  it('render component with empty bankruptcy', () => {
    const { wrapper } = renderWrapper({
      ...initialState,
      bankruptcy: {
        [storeId]: {}
      }
    });
    expect(wrapper.getByText('txt_call_result')).toBeInTheDocument();
  });

  it('render component with CALL_RESULT_CODE.HARDSHIP', () => {
    const { wrapper } = renderWrapper({
      ...initialState,
      collectionForm: {
        [storeId]: {
          selectedCallResult: {
            code: CALL_RESULT_CODE.BANKRUPTCY,
            description: 'des'
          },
          callResultRefData: [
            { code: CALL_RESULT_CODE.CCCS, description: 'des' },
            { code: CALL_RESULT_CODE.BANKRUPTCY, description: 'des' },
            { code: CALL_RESULT_CODE.BANKRUPTCY_PENDING, description: 'des' },
            { code: CALL_RESULT_CODE.NOT_BANKRUPTCY, description: 'des' },
            { code: CALL_RESULT_CODE.DECEASE, description: 'des' },
            { code: CALL_RESULT_CODE.NOT_DECEASED, description: 'des' },
            { code: CALL_RESULT_CODE.HARDSHIP, description: 'des' },
            { code: CALL_RESULT_CODE.NOT_HARDSHIP, description: 'des' },
            { code: CALL_RESULT_CODE.INCARCERATION, description: 'des' },
            { code: CALL_RESULT_CODE.NOT_INCARCERATION, description: 'des' },
            { code: CALL_RESULT_CODE.LONG_TERM_MEDICAL, description: 'des' },
            {
              code: CALL_RESULT_CODE.NOT_LONG_TERM_MEDICAL,
              description: 'des'
            },
            { code: CALL_RESULT_CODE.PROMISE_TO_PAY, description: 'des' },
            {
              code: CALL_RESULT_CODE.REGULATORY_SPOUSE_CONTACT,
              description: 'des'
            }
          ],
          defaultCallResultRefData: [
            { code: CALL_RESULT_CODE.BANKRUPTCY, description: 'des' },
            { code: 'code', description: 'des' }
          ],
          lastFollowUpData: {
            callResultType: 'type'
          },
          lastDelayCallResult: CALL_RESULT_CODE.HARDSHIP
        }
      }
    });
    expect(wrapper.getByText('txt_call_result')).toBeInTheDocument();
  });

  it('render component with empty hardship', () => {
    const { wrapper } = renderWrapper({
      ...initialState,
      hardship: {
        [storeId]: {
          data: undefined
        }
      }
    });
    expect(wrapper.getByText('txt_call_result')).toBeInTheDocument();
  });

  it('handleSelectCallResult when hardship complete', () => {
    const spy = collectionFormActionsSpy('selectCallResult');
    renderWrapper(initialState);
    const input = document.querySelector('input')!;

    fireEvent.change(input, { target: { value: '' } });

    expect(spy).toHaveBeenCalledWith({
      storeId,
      item: undefined
    });
  });

  it('handleSelectCallResult when hardship in progress', () => {
    const spy = collectionFormActionsSpy('selectCallResult');
    renderWrapper({
      ...initialState,
      hardship: {
        [storeId]: {
          data: {
            processStatus: HARDSHIP_IN_PROGRESS
          }
        }
      }
    });
    const input = document.querySelector('input')!;

    fireEvent.change(input, { target: { value: '' } });

    expect(spy).toHaveBeenCalledWith({
      storeId,
      item: undefined
    });
  });
});
