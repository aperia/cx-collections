// I18n
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
// Hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
// components
import {
  ComboBox,
  DropdownBaseChangeEvent
} from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import {
  BANKRUPTCY_GROUP_CODES,
  CALL_RESULT_CODE,
  DECEASED_GROUP_CODES,
  HARDSHIP_GROUP_CODES
} from 'pages/CollectionForm/constants';
import React, { useEffect, useMemo } from 'react';
import { useDispatch } from 'react-redux';
// Types
import { CallResult } from '../../types';
import { collectionFormActions } from '../../_redux/reducers';
// Actions
import {
  selectBankruptcyActionEntryCode,
  selectCallResult,
  selectCallResultRefData,
  selectDeceasedStatusCodeFromDeceasedDetailAPI,
  selectIsHavingBankruptcyConfirm,
  selectIsHavingBankruptcyPending,
  selectIsHavingHardshipData,
  selectExistsHardshipData,
  selectIsHavingIncarceration,
  selectIsHavingLongTermMedical,
  takeSelectedCallResult
} from '../../_redux/selectors';

export interface CallResultProps {}
const CallResultList: React.FC<CallResultProps> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId, accEValue } = useAccountDetail();

  const callResultSelected = useStoreIdSelector<CallResult>(
    takeSelectedCallResult
  );

  const callResultItems = useStoreIdSelector<Array<CallResult>>(
    selectCallResultRefData
  );

  const deceasedStatusCode = useStoreIdSelector<CALL_RESULT_CODE | undefined>(
    selectDeceasedStatusCodeFromDeceasedDetailAPI
  );

  const isHavingBankruptcyPending = useStoreIdSelector<boolean>(
    selectIsHavingBankruptcyPending
  );

  const isHavingBankruptcyConfirm = useStoreIdSelector<boolean>(
    selectIsHavingBankruptcyConfirm
  );

  const bankruptcyActionEntryCode = useStoreIdSelector<string>(
    selectBankruptcyActionEntryCode
  );

  const isHavingHardshipData = useStoreIdSelector<boolean>(
    selectIsHavingHardshipData
  );

  const isExistsHardshipData = useStoreIdSelector<boolean>(
    selectExistsHardshipData
  );

  const isHavingLongTermHardship = useStoreIdSelector<boolean>(
    selectIsHavingLongTermMedical
  );

  const isHavingIncarceration = useStoreIdSelector<boolean>(
    selectIsHavingIncarceration
  );

  const latestCallResult =
    useStoreIdSelector<CALL_RESULT_CODE>(selectCallResult);

  const filterCallResult = (
    callResultGroup: CALL_RESULT_CODE[],
    currentValue?: CALL_RESULT_CODE
  ) => {
    const currentStep = callResultGroup.findIndex(
      item => item === currentValue
    );
    const lengthGroup = callResultGroup.length;

    let start = Math.max(currentStep, 0);
    if (start === lengthGroup - 1) {
      start = 0;
    }

    const end = Math.min(start + 2, lengthGroup);
    return callResultGroup.slice(start, end);
  };

  const filterCallResults = callResultItems
    .map(item => ({
      ...item,
      description: t(item.description)
    }))
    .filter(({ code }) => {
      const callResultCode = code as CALL_RESULT_CODE;

      // for Deceased Group
      if (DECEASED_GROUP_CODES.includes(callResultCode)) {
        const filteredCallResult = filterCallResult(
          DECEASED_GROUP_CODES,
          deceasedStatusCode
        );

        if (
          deceasedStatusCode === CALL_RESULT_CODE.DECEASE_PENDING ||
          deceasedStatusCode === CALL_RESULT_CODE.DECEASE
        ) {
          filteredCallResult.push(
            DECEASED_GROUP_CODES[DECEASED_GROUP_CODES.length - 1]
          );
        }
        return filteredCallResult.includes(callResultCode);
      }

      // for bankruptcy Group
      if (BANKRUPTCY_GROUP_CODES.includes(callResultCode)) {
        const filteredCallResult = filterCallResult(
          BANKRUPTCY_GROUP_CODES,
          bankruptcyActionEntryCode as CALL_RESULT_CODE
        );

        // add not bankruptcy if have bankruptcy data
        if (isHavingBankruptcyPending || isHavingBankruptcyConfirm) {
          filteredCallResult.push(
            BANKRUPTCY_GROUP_CODES[BANKRUPTCY_GROUP_CODES.length - 1]
          );
        }
        return filteredCallResult.includes(callResultCode);
      }

      // for hardship group
      if (callResultCode === CALL_RESULT_CODE.HARDSHIP) {
        if (isHavingHardshipData) {
          return false;
        }
        if (
          isExistsHardshipData !== undefined &&
          isEmpty(isExistsHardshipData)
        ) {
          return true;
        }
        return !HARDSHIP_GROUP_CODES.includes(latestCallResult);
      }

      if (callResultCode === CALL_RESULT_CODE.NOT_HARDSHIP) {
        if (HARDSHIP_GROUP_CODES.includes(latestCallResult)) {
          return isHavingHardshipData;
        }
        if (!isExistsHardshipData || isEmpty(isExistsHardshipData))
          return false;

        const filteredCallResult = filterCallResult(
          HARDSHIP_GROUP_CODES,
          latestCallResult
        );

        // add not hardship if have hardship data
        if (isHavingHardshipData) {
          filteredCallResult.push(
            HARDSHIP_GROUP_CODES[HARDSHIP_GROUP_CODES.length - 1]
          );
        }
        return filteredCallResult.includes(callResultCode);
      }

      // for long term medical
      if (callResultCode === CALL_RESULT_CODE.LONG_TERM_MEDICAL) {
        return !isHavingLongTermHardship;
      }
      if (callResultCode === CALL_RESULT_CODE.NOT_LONG_TERM_MEDICAL) {
        return isHavingLongTermHardship;
      }

      // for incarceration
      if (callResultCode === CALL_RESULT_CODE.INCARCERATION) {
        return !isHavingIncarceration;
      }
      if (callResultCode === CALL_RESULT_CODE.NOT_INCARCERATION) {
        return isHavingIncarceration;
      }

      return true;
    });

  const dropdownListItem = useMemo(() => {
    return filterCallResults?.map((item, index) => (
      <ComboBox.Item
        key={`${item.code}-${index}`}
        label={t(item.description)}
        value={item}
      />
    ));
  }, [filterCallResults, t]);

  useEffect(() => {
    dispatch(
      collectionFormActions.getCallResultsRefData({
        storeId
      })
    );
  }, [dispatch, storeId, accEValue]);

  const handleSelectCallResult = (event: DropdownBaseChangeEvent) => {
    dispatch(
      collectionFormActions.selectCallResult({
        storeId,
        item: event.target.value
      })
    );
  };

  return (
    <ComboBox
      dataTestId="callResultDropdownList"
      textField="description"
      name="callResult"
      label={t(I18N_COLLECTION_FORM.CALL_RESULT)}
      id="code"
      value={callResultSelected}
      onChange={handleSelectCallResult}
      popupBaseProps={{
        popupBaseClassName: 'inside-infobar'
      }}
      required
      noResult={t('txt_no_results_found')}
      checkAllLabel={t('txt_all')}
    >
      {dropdownListItem}
    </ComboBox>
  );
};

export default CallResultList;
