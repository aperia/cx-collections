import React from 'react';

// hooks
import { useStoreIdSelector } from 'app/hooks';

// redux
import { selectCallResult, selectIsHavingData } from '../../_redux/selectors';

// components
import BankruptcyResult from 'pages/Bankruptcy/BankruptcyResults';
import Deceased from 'pages/Deceased/DeceasedResults';
import PromiseToPay from 'pages/PromiseToPay/PromiseToPayCallResults';
import Hardship from 'pages/HardShip/HardShipResults/HardShip';
import CCCS from 'pages/CCCS';
import LongTermMedical from 'pages/LongTermMedical/CallResults';
import Incarceration from 'pages/Incarceration/CallResults';
import Settlement from 'pages/Settlement/CallResults';

// constants
import {
  BANKRUPTCY_GROUP_CODES,
  CALL_RESULT_CODE,
  DECEASED_GROUP_CODES
} from '../../constants';

export interface BodyProps {}

const Body: React.FC<BodyProps> = () => {
  const callResult = useStoreIdSelector<CALL_RESULT_CODE>(selectCallResult);
  const isHavingData = useStoreIdSelector<boolean>(selectIsHavingData);

  const renderView = () => {
    if (
      callResult === CALL_RESULT_CODE.NOT_BANKRUPTCY ||
      callResult === CALL_RESULT_CODE.NOT_DECEASED
    ) {
      return null;
    }
    if (BANKRUPTCY_GROUP_CODES.includes(callResult)) {
      return <BankruptcyResult />;
    }
    if (DECEASED_GROUP_CODES.includes(callResult)) {
      return <Deceased />;
    }
    if (callResult === CALL_RESULT_CODE.CCCS) return <CCCS />;
    if (callResult === CALL_RESULT_CODE.PROMISE_TO_PAY) return <PromiseToPay />;
    if (callResult === CALL_RESULT_CODE.HARDSHIP) return <Hardship />;
    if (callResult === CALL_RESULT_CODE.LONG_TERM_MEDICAL)
      return <LongTermMedical />;
    if (callResult === CALL_RESULT_CODE.INCARCERATION) return <Incarceration />;
    if (callResult === CALL_RESULT_CODE.SETTLEMENT) return <Settlement />;
    return null;
  };

  if (!isHavingData) return null;

  return <div className="pt-16 pb-24">{renderView()}</div>;
};

export default Body;
