import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import React from 'react';
import ConfirmRemove from './ConfirmRemove';
import { screen } from '@testing-library/react';
import { hardshipActions } from 'pages/HardShip/_redux/reducers';
import userEvent from '@testing-library/user-event';

describe('ConfirmRemove component', () => {
  it('should show two radios', () => {
    renderMockStoreId(<ConfirmRemove storeId={storeId} />);
    const radios = screen.getAllByRole('radio');
    expect(radios.length).toEqual(2);
  });

  it('should call onChange function', () => {
    const spy = mockActionCreator(hardshipActions);
    const mockAction = spy('setReasonHardship');
    renderMockStoreId(<ConfirmRemove storeId={storeId} />);
    const radios = screen.getAllByRole('radio');
    userEvent.click(radios[1]);
    expect(mockAction).toBeCalledTimes(2);
  });
});
