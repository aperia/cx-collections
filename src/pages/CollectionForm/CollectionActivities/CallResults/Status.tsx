import React from 'react';

// components
import { Button, Icon, Badge } from 'app/_libraries/_dls/components';
import ConfirmRemove from './ConfirmRemove';

// Hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useDispatch } from 'react-redux';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// i18n
import { I18N_COMMON_TEXT, I18N_COLLECTION_FORM } from 'app/constants/i18n';

// action
import {
  selectCallResultsType,
  selectLabelCallResult,
  selectRemoveButtonName,
  selectShowRemoveButton
} from '../../_redux/selectors';
import { collectionFormActions } from '../../_redux/reducers';
import { hardshipActions } from 'pages/HardShip/_redux/reducers';
import { bankruptcyActions } from 'pages/Bankruptcy/_redux/reducers';
import { deceasedActions } from 'pages/Deceased/_redux/reducers';
import { selectCCCSData } from 'pages/CCCS/_redux/selectors';

// Constants
import { ICurrentResultCCCS } from 'pages/CCCS/types';
import { CCCS_STATUS_COLOR, CCCS_STATUS_TEXT } from 'pages/CCCS/constants';
import { CALL_RESULT_CODE } from '../../constants';
import { DECEASED_ENTRY_CODE } from 'pages/Deceased/constants';
import { selectIsDisableRemove } from 'pages/HardShip/_redux/selectors';
import { longTermMedicalActions } from 'pages/LongTermMedical/_redux/reducers';
import { incarcerationActions } from 'pages/Incarceration/_redux/reducers';
import { genAmtId } from 'app/_libraries/_dls/utils';

const Status: React.FC = () => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const labelCallResult = useStoreIdSelector<string>(selectLabelCallResult);
  const callResultsType = useStoreIdSelector<string>(selectCallResultsType);
  const statusCCCS = useStoreIdSelector<ICurrentResultCCCS>(selectCCCSData);
  const showRemoveButton = useStoreIdSelector<boolean>(selectShowRemoveButton);
  const removeButtonName = useStoreIdSelector<string>(selectRemoveButtonName);
  const statusHardship = useStoreIdSelector<boolean>(selectIsDisableRemove);
  const dispatch = useDispatch();

  const removeCollectionForm = () => {
    if (callResultsType === CALL_RESULT_CODE.BANKRUPTCY) {
      dispatch(
        bankruptcyActions.triggerRemoveBankruptcyStatus({
          storeId
        })
      );
    } else if (callResultsType === CALL_RESULT_CODE.HARDSHIP) {
      dispatch(
        hardshipActions.triggerRemoveHardshipStatusRequest({
          postData: {
            storeId
          }
        })
      );
    } else {
      dispatch(
        deceasedActions.triggerRemoveDeceasedStatus({
          storeId,
          entryCode: DECEASED_ENTRY_CODE.NOT_DECEASED
        })
      );
    }

    dispatch(
      collectionFormActions.getCallResultsRefData({
        storeId
      })
    );
  };

  const handleRemove = () => {
    let title = null;
    let body = null;
    if (callResultsType === CALL_RESULT_CODE.LONG_TERM_MEDICAL) {
      return dispatch(longTermMedicalActions.onChangeOpenModal({ storeId }));
    }
    if (callResultsType === CALL_RESULT_CODE.INCARCERATION) {
      return dispatch(
        incarcerationActions.onChangeOpenModal({
          storeId
        })
      );
    }
    if (callResultsType === CALL_RESULT_CODE.BANKRUPTCY) {
      title = I18N_COLLECTION_FORM.BANKRUPTCY_CONFIRM_REMOVE_STATUS_TITLE;
      body = I18N_COLLECTION_FORM.BANKRUPTCY_CONFIRM_REMOVE_STATUS;
    } else if (
      callResultsType === CALL_RESULT_CODE.NOT_HARDSHIP ||
      callResultsType === CALL_RESULT_CODE.HARDSHIP
    ) {
      title = I18N_COLLECTION_FORM.ARE_YOU_SURE_REMOVE_HARDSHIP;
      body = <ConfirmRemove storeId={storeId} />;
    } else {
      title = I18N_COLLECTION_FORM.CONFIRM_REMOVE_DECEASED_STATUS;
      body = I18N_COLLECTION_FORM.ARE_YOU_SURE_REMOVE_DECEASED_STATUS;
    }
    dispatch(
      confirmModalActions.open({
        title,
        body,
        confirmCallback: removeCollectionForm,
        confirmText: I18N_COMMON_TEXT.REMOVE,
        size: {
          sm: true
        }
      })
    );
  };

  const isDisableRemove =
    callResultsType === CALL_RESULT_CODE.HARDSHIP && statusHardship;

  return (
    <div className="mt-16 p-8 bg-light-l16 d-flex align-items-center rounded-lg">
      <div className="bubble-icon">
        <Icon
          name="megaphone"
          dataTestId="collectionForm_callResult_megaphone"
        />
      </div>

      <p
        className="mx-12"
        data-testid={genAmtId('callResults_status', 'label', '')}
      >
        {t(labelCallResult)}
      </p>

      {callResultsType === CALL_RESULT_CODE.CCCS && statusCCCS?.action ? (
        <div className="ml-auto">
          <Badge color={CCCS_STATUS_COLOR[statusCCCS?.action]}>
            {t(CCCS_STATUS_TEXT[statusCCCS?.action])}
          </Badge>
        </div>
      ) : null}

      {showRemoveButton ? (
        <Button
          onClick={handleRemove}
          size="sm"
          variant="outline-primary"
          className="ml-auto"
          disabled={isDisableRemove}
          dataTestId="callResults_status_remove-btn"
        >
          {t(removeButtonName)}
        </Button>
      ) : null}
    </div>
  );
};

export default Status;
