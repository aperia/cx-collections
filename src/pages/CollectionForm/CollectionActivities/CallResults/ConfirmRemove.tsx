import React, { useEffect } from 'react';

// redux
import { useDispatch, useSelector } from 'react-redux';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// components
import { Radio } from 'app/_libraries/_dls/components';

// i18n
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';

// Redux
import { hardshipActions } from 'pages/HardShip/_redux/reducers';
import { selectReasonRemove } from 'pages/HardShip/_redux/selectors';
import { REASON_REMOVE_HARDSHIP } from 'pages/HardShip/constants';

// helper
import classNames from 'classnames';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface ConfirmRemoveProps {
  storeId: string;
}

const ConfirmRemove: React.FC<ConfirmRemoveProps> = ({ storeId }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const checked: string = useSelector(selectReasonRemove(storeId));

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(
      hardshipActions.setReasonHardship({
        storeId,
        value: event.target.id,
        reason: t(`txt_reason_title${event.target.id}`)
      })
    );
  };

  useEffect(() => {
    dispatch(
      hardshipActions.setReasonHardship({
        storeId,
        value: REASON_REMOVE_HARDSHIP[0],
        reason: t(`txt_reason_title${REASON_REMOVE_HARDSHIP[0]}`)
      })
    );

    return () => {
      dispatch(
        hardshipActions.setReasonHardship({
          storeId,
          value: undefined,
          reason: ''
        })
      );
    };
  }, [dispatch, storeId, t]);

  return (
    <>
      <span
        className="color-grey-d20 fs-14"
        data-testid={genAmtId('confirmRemoveHardship', 'title', '')}
      >
        {t('txt_title_remove_hardship_status')}
      </span>
      <div className="d-flex mt-16">
        <span
          className="color-grey fs-14 fw-500"
          data-testid={genAmtId('confirmRemoveHardship', 'reason', '')}
        >
          {t(I18N_COLLECTION_FORM.REASON)}:
        </span>
        <div className="ml-24">
          {REASON_REMOVE_HARDSHIP.map((item, index) => (
            <Radio
              key={`${item}-${index}`}
              className={classNames({
                'mb-16': index < REASON_REMOVE_HARDSHIP.length - 1
              })}
              dataTestId={genAmtId(
                'confirmRemoveHardship_reason',
                t(`txt_reason_title${item}`),
                ''
              )}
            >
              <Radio.Input
                name="hardship_modal_remove"
                id={item}
                checked={item === checked}
                onChange={handleChange}
              />
              <Radio.Label>
                <div className="color-grey-d20 fs-14">
                  {t(`txt_reason_title${item}`)}
                </div>
                <div className="color-grey-d20 fs-14">
                  {t(`txt_reason${item}`)}
                </div>
              </Radio.Label>
            </Radio>
          ))}
        </div>
      </div>
    </>
  );
};

export default ConfirmRemove;
