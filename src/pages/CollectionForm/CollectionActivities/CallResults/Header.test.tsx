import React from 'react';
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import Header from './Header';
import { screen } from '@testing-library/react';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { collectionFormActions } from '../../_redux/reducers';
import {
  CALL_RESULT_CODE,
  COLLECTION_METHOD
} from 'pages/CollectionForm/constants';
import { CollectionForm } from 'pages/CollectionForm/types';

const initialState: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      lastFollowUpData: { callResultType: CALL_RESULT_CODE.BANKRUPTCY }
    } as CollectionForm
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  const { wrapper } = renderMockStoreId(<Header />, {
    initialState
  });

  return {
    container: wrapper.container as HTMLElement
  };
};

const collectionFormSpy = mockActionCreator(collectionFormActions);

describe('Body component', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isCollector: true
    }
  };

  it('should render', () => {
    renderWrapper(initialState);

    expect(screen.getByRole('button')).toBeInTheDocument();
    expect(
      screen.getByText(I18N_COLLECTION_FORM.CALL_RESULT)
    ).toBeInTheDocument();
  });

  it('should render with not bankruptcy', () => {
    renderWrapper({
      collectionForm: {
        [storeId]: {
          lastFollowUpData: { callResultType: CALL_RESULT_CODE.NOT_BANKRUPTCY }
        } as CollectionForm
      }
    });

    expect(screen.queryByRole('button')).toBeNull();
    expect(
      screen.getByText(I18N_COLLECTION_FORM.CALL_RESULT)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleEditButton', () => {
    const mockTriggerEditCollectionForm = collectionFormSpy(
      'triggerEditCollectionForm'
    );
    const mockChangeMethod = collectionFormSpy('changeMethod');

    renderWrapper(initialState);

    screen.getByRole('button').click();

    expect(mockTriggerEditCollectionForm).toBeCalledWith({ storeId });
    expect(mockChangeMethod).toBeCalledWith({
      storeId,
      method: COLLECTION_METHOD.EDIT
    });
  });
});
