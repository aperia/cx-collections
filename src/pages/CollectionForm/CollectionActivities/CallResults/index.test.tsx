import React from 'react';
import { renderMockStoreId, storeId } from 'app/test-utils';
import CallResults from '.';
import { screen } from '@testing-library/react';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { CollectionForm } from 'pages/CollectionForm/types';

jest.mock('./Header', () => () => <div>Header</div>);
jest.mock('./Status', () => () => <div>Status</div>);
jest.mock('./Body', () => () => <div>Body</div>);

const initialState: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      lastFollowUpData: { callResultType: CALL_RESULT_CODE.NOT_HARDSHIP }
    } as CollectionForm
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<CallResults />, { initialState });
};

describe('Body component', () => {
  it('should render', () => {
    renderWrapper(initialState);

    expect(screen.getByText('Header')).toBeInTheDocument();
    expect(screen.getByText('Status')).toBeInTheDocument();
    expect(screen.getByText('Body')).toBeInTheDocument();
  });

  it('should render > with form is inProgress', () => {
    const state = { ...initialState };
    state.collectionForm![storeId].isInProgress = true;
    renderWrapper(state);

    expect(screen.getByText('txt_call_result_in_progress')).toBeInTheDocument();
  });

  it('should render empty > with form is inProgress', () => {
    const state = {
      collectionForm: {
        [storeId]: {
          isInProgress: true,
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.PROMISE_TO_PAY
          }
        }
      }
    };
    renderWrapper(state as never);

    expect(screen.getByText('txt_call_result_in_progress')).toBeInTheDocument();
  });

  it('should render without state', () => {
    renderWrapper({});

    expect(screen.queryByText('Header')).toBeNull();
    expect(screen.queryByText('Status')).toBeNull();
    expect(screen.queryByText('Body')).toBeNull();
  });

  it('should render without empty promise', () => {
    const { wrapper } = renderWrapper({
      collectionForm: {
        [storeId]: {
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.PROMISE_TO_PAY
          },
          uploadFiles: {}
        }
      }
    });

    expect(wrapper.container.innerHTML).toEqual('<div>Body</div>');
  });
});
