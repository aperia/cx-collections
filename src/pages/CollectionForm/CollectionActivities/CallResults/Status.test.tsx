import React from 'react';
import Status from './Status';
import {
  renderMockStoreId,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';
import { AccountDetailProvider } from 'app/hooks';
import { CALL_RESULT_CODE } from '../../constants';

import { hardshipActions } from 'pages/HardShip/_redux/reducers';
import { bankruptcyActions } from 'pages/Bankruptcy/_redux/reducers';
import { deceasedActions } from 'pages/Deceased/_redux/reducers';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';

import { mockActionCreator } from 'app/test-utils';
import { ModalData } from 'pages/__commons/ConfirmModal/types';
import { CollectionForm, LastFollowUpData } from 'pages/CollectionForm/types';
import { ICurrentResultCCCS } from 'pages/CCCS/types';
import { CCCS_ACTIONS } from 'pages/CCCS/constants';

mockActionCreator(confirmModalActions);
mockActionCreator(bankruptcyActions);
mockActionCreator(hardshipActions);
mockActionCreator(deceasedActions);

describe('Test Status component in Collection Form Results', () => {
  it('Render component > not having Remove button', () => {
    const { wrapper } = renderMockStoreId(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <Status />
      </AccountDetailProvider>,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              lastFollowUpData: {
                callResultType: CALL_RESULT_CODE.DECEASE_PENDING,
                data: {
                  deceasedParty: { value: 'primary' },
                  primaryMainInfo: {
                    dateNoticeReceived: '2021-02-03T17:00:00.000Z',
                    dateOfDeath: '2021-01-31T17:00:00.000Z'
                  },
                  primaryPartyInfo: {},
                  secondaryMainInfo: {},
                  secondaryPartyInfo: {},
                  settlementInfo: {
                    settlementAmount: 10,
                    settlementDate: '2021-02-01T17:00:00.000Z',
                    settlementMethod: 'MG - Moneygram'
                  }
                }
              } as LastFollowUpData
            } as CollectionForm
          }
        }
      },
      false
    );

    const button = wrapper.baseElement.querySelector('button');
    expect(button).toEqual(null);

    wrapper.unmount();
  });

  it('Render component > having Remove button', () => {
    const { wrapper } = renderMockStoreId(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <Status />
      </AccountDetailProvider>,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              lastFollowUpData: {
                callResultType: CALL_RESULT_CODE.DECEASE,
                data: {
                  deceasedParty: { value: 'primary' },
                  primaryMainInfo: {
                    dateNoticeReceived: '2021-02-03T17:00:00.000Z',
                    dateOfDeath: '2021-01-31T17:00:00.000Z'
                  },
                  primaryPartyInfo: {},
                  secondaryMainInfo: {},
                  secondaryPartyInfo: {},
                  settlementInfo: {
                    settlementAmount: 10,
                    settlementDate: '2021-02-01T17:00:00.000Z',
                    settlementMethod: 'MG - Moneygram'
                  }
                }
              } as LastFollowUpData
            } as CollectionForm
          }
        }
      },
      false
    );

    const button = wrapper.baseElement.querySelector(
      'button'
    ) as HTMLButtonElement;
    expect(button.innerHTML).toEqual('txt_remove_deceased_status');
    button.click();

    wrapper.unmount();
  });

  it('Render component > having Remove button', () => {
    const { wrapper } = renderMockStoreId(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <Status />
      </AccountDetailProvider>,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              lastFollowUpData: {
                callResultType: CALL_RESULT_CODE.BANKRUPTCY,
                data: {
                  additionalAttorneyInfo: {},
                  clerkCourtInfo: {},
                  fillingInfo: {},
                  mainInfo: {
                    attorneyName: 'aaaaaaaaaaaaaaaa',
                    attorneyPhoneNumber: '1111111111',
                    case: '1',
                    chapter: '7'
                  },
                  trusteeInfo: {}
                }
              } as LastFollowUpData
            } as CollectionForm
          }
        }
      },
      false
    );

    const button = wrapper.baseElement.querySelector('button') as HTMLElement;
    expect(button.innerHTML).toEqual('txt_remove_bankruptcy_status');
    button.click();

    wrapper.unmount();
  });

  it('Render component > having Remove hardship', () => {
    const mockOpenModal = jest
      .fn()
      .mockImplementation(({ confirmCallback }: ModalData) => {
        // trigger confirmCallback
        confirmCallback!();
        return { type: 'type' };
      });
    jest.spyOn(confirmModalActions, 'open').mockImplementation(mockOpenModal);

    const { wrapper } = renderMockStoreId(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <Status />
      </AccountDetailProvider>,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              lastFollowUpData: {
                callResultType: CALL_RESULT_CODE.HARDSHIP,
                data: {}
              } as LastFollowUpData
            } as CollectionForm
          }
        }
      },
      false
    );

    const button = wrapper.baseElement.querySelector('button') as HTMLElement;
    expect(button.innerHTML).toEqual('txt_remove_hardship_status');
    button.click();

    wrapper.unmount();
  });

  it('Render component > having Remove hardship', () => {
    const mockOpenModal = jest
      .fn()
      .mockImplementation(({ confirmCallback }: ModalData) => {
        // trigger confirmCallback
        confirmCallback!();
        return { type: 'type' };
      });
    jest.spyOn(confirmModalActions, 'open').mockImplementation(mockOpenModal);

    const { wrapper } = renderMockStoreId(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <Status />
      </AccountDetailProvider>,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              lastFollowUpData: {
                callResultType: CALL_RESULT_CODE.HARDSHIP,
                data: {}
              } as LastFollowUpData
            } as CollectionForm
          }
        }
      },
      false
    );

    const button = wrapper.baseElement.querySelector('button') as HTMLElement;
    expect(button.innerHTML).toEqual('txt_remove_hardship_status');
    button.click();
    // expect(hardshipSpy('triggerRemoveHardshipStatusRequest')).toBeCalled();
    wrapper.unmount();
  });

  it('Render component > remove bankruptcy', () => {
    const mockOpenModal = jest
      .fn()
      .mockImplementation(({ confirmCallback }: ModalData) => {
        // trigger confirmCallback
        confirmCallback!();
        return { type: 'type' };
      });
    jest.spyOn(confirmModalActions, 'open').mockImplementation(mockOpenModal);

    const { wrapper } = renderMockStoreId(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <Status />
      </AccountDetailProvider>,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              lastFollowUpData: {
                callResultType: CALL_RESULT_CODE.BANKRUPTCY,
                data: {}
              } as LastFollowUpData
            } as CollectionForm
          }
        }
      },
      false
    );

    const button = wrapper.baseElement.querySelector('button') as HTMLElement;
    expect(button.innerHTML).toEqual('txt_remove_bankruptcy_status');
    button.click();

    wrapper.unmount();
  });

  it('Render component > remove deceased', () => {
    const mockOpenModal = jest
      .fn()
      .mockImplementation(({ confirmCallback }: ModalData) => {
        // trigger confirmCallback
        confirmCallback!();
        return { type: 'type' };
      });
    jest.spyOn(confirmModalActions, 'open').mockImplementation(mockOpenModal);

    const { wrapper } = renderMockStoreId(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <Status />
      </AccountDetailProvider>,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              lastFollowUpData: {
                callResultType: CALL_RESULT_CODE.DECEASE,
                data: {}
              } as LastFollowUpData
            } as CollectionForm
          }
        }
      },
      false
    );

    const button = wrapper.baseElement.querySelector('button') as HTMLElement;
    expect(button.innerHTML).toEqual('txt_remove_deceased_status');
    button.click();

    wrapper.unmount();
  });

  it('Render component > cccs action pendingProposal', () => {
    const mockOpenModal = jest
      .fn()
      .mockImplementation(({ confirmCallback }: ModalData) => {
        // trigger confirmCallback
        confirmCallback!();
        return { type: 'type' };
      });
    jest.spyOn(confirmModalActions, 'open').mockImplementation(mockOpenModal);

    const { wrapper } = renderMockStoreId(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <Status />
      </AccountDetailProvider>,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              lastFollowUpData: {
                callResultType: CALL_RESULT_CODE.CCCS
              } as LastFollowUpData
            } as CollectionForm
          },
          cccs: {
            [storeId]: {
              data: {
                action: CCCS_ACTIONS.PENDING_PROPOSAL
              } as ICurrentResultCCCS
            }
          }
        }
      },
      false
    );

    expect(wrapper.getByText('txt_pending_proposal')).toBeInTheDocument();
    expect(wrapper.getByText('txt_pending_proposal').className).toContain(
      'badge-orange'
    );
  });

  it('Render component > empty cccs', () => {
    const mockOpenModal = jest
      .fn()
      .mockImplementation(({ confirmCallback }: ModalData) => {
        // trigger confirmCallback
        confirmCallback!();
        return { type: 'type' };
      });
    jest.spyOn(confirmModalActions, 'open').mockImplementation(mockOpenModal);

    const { wrapper } = renderMockStoreId(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <Status />
      </AccountDetailProvider>,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              lastFollowUpData: {
                callResultType: CALL_RESULT_CODE.CCCS
              } as LastFollowUpData
            } as CollectionForm
          },
          cccs: {}
        }
      },
      false
    );

    expect(
      wrapper.getByText('txt_cccs').parentElement!.querySelector('.ml-auto')
    ).toBeNull();
  });

  it('Render component > having Remove button with LONG_TERM_MEDICAL', () => {
    const { wrapper } = renderMockStoreId(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <Status />
      </AccountDetailProvider>,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              lastFollowUpData: {
                callResultType: CALL_RESULT_CODE.LONG_TERM_MEDICAL,
                data: {
                  additionalAttorneyInfo: {},
                  clerkCourtInfo: {},
                  fillingInfo: {},
                  mainInfo: {
                    attorneyName: 'aaaaaaaaaaaaaaaa',
                    attorneyPhoneNumber: '1111111111',
                    case: '1',
                    chapter: '7'
                  },
                  trusteeInfo: {}
                }
              } as LastFollowUpData
            } as CollectionForm
          }
        }
      },
      false
    );

    const button = wrapper.baseElement.querySelector('button') as HTMLElement;
    expect(button.innerHTML).toEqual('txt_remove_status');
    button.click();

    wrapper.unmount();
  });

  it('Render component > having Remove button with INCARCERATION', () => {
    const { wrapper } = renderMockStoreId(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <Status />
      </AccountDetailProvider>,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              lastFollowUpData: {
                callResultType: CALL_RESULT_CODE.INCARCERATION,
                data: {
                  additionalAttorneyInfo: {},
                  clerkCourtInfo: {},
                  fillingInfo: {},
                  mainInfo: {
                    attorneyName: 'aaaaaaaaaaaaaaaa',
                    attorneyPhoneNumber: '1111111111',
                    case: '1',
                    chapter: '7'
                  },
                  trusteeInfo: {}
                }
              } as LastFollowUpData
            } as CollectionForm
          }
        }
      },
      false
    );

    const button = wrapper.baseElement.querySelector('button') as HTMLElement;
    expect(button.innerHTML).toEqual('txt_remove_status');
    button.click();

    wrapper.unmount();
  });
});
