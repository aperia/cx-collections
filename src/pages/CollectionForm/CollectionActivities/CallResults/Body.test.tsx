import { renderMockStoreId, storeId } from 'app/test-utils';
import React from 'react';
import Body from './Body';
import { screen } from '@testing-library/react';
import { CALL_RESULT_CODE } from '../../constants';
import { CollectionForm } from 'pages/CollectionForm/types';

jest.mock('pages/Bankruptcy/BankruptcyResults', () => () => (
  <div>BankruptcyResults</div>
));
jest.mock('pages/Deceased/DeceasedResults', () => () => (
  <div>DeceasedResults</div>
));
jest.mock('pages/PromiseToPay/PromiseToPayCallResults', () => () => (
  <div>PromiseToPayCallResults</div>
));
jest.mock('pages/HardShip/HardShipResults/HardShip', () => () => (
  <div>HardShipResults</div>
));
jest.mock('pages/LongTermMedical/CallResults', () => () => (
  <div>LongTermMedical</div>
));
jest.mock('pages/Incarceration/CallResults', () => () => (
  <div>Incarceration</div>
));
jest.mock('pages/Settlement/CallResults', () => () => <div>Settlement </div>);
jest.mock('pages/CCCS', () => () => <div>CCCSResults</div>);

const renderWrapper = (callResultType: CALL_RESULT_CODE) => {
  const { wrapper } = renderMockStoreId(<Body />, {
    initialState: {
      collectionForm: {
        [storeId]: { lastFollowUpData: { callResultType } } as CollectionForm
      }
    }
  });

  return {
    container: wrapper.container as HTMLElement
  };
};

describe('Body component', () => {
  it('should not render component', () => {
    renderMockStoreId(<Body />);

    expect(screen.queryByText('BankruptcyResults')).toBeNull();
    expect(screen.queryByText('DeceasedResults')).toBeNull();
    expect(screen.queryByText('PromiseToPayCallResults')).toBeNull();
    expect(screen.queryByText('HardShipResults')).toBeNull();
    expect(screen.queryByText('CCCSResults')).toBeNull();
    expect(screen.queryByText('LongTermMedical')).toBeNull();
    expect(screen.queryByText('Incarceration')).toBeNull();
  });

  it('should not render component when wrong type', () => {
    renderWrapper('wrong type' as CALL_RESULT_CODE);

    expect(screen.queryByText('BankruptcyResults')).toBeNull();
    expect(screen.queryByText('DeceasedResults')).toBeNull();
    expect(screen.queryByText('PromiseToPayCallResults')).toBeNull();
    expect(screen.queryByText('HardShipResults')).toBeNull();
    expect(screen.queryByText('CCCSResults')).toBeNull();
    expect(screen.queryByText('LongTermMedical')).toBeNull();
    expect(screen.queryByText('Incarceration')).toBeNull();
  });

  it('should show not bankruptcy', () => {
    renderWrapper(CALL_RESULT_CODE.NOT_BANKRUPTCY);

    expect(screen.queryByText('BankruptcyResults')).toBeNull();
    expect(screen.queryByText('DeceasedResults')).toBeNull();
    expect(screen.queryByText('PromiseToPayCallResults')).toBeNull();
    expect(screen.queryByText('HardShipResults')).toBeNull();
    expect(screen.queryByText('CCCSResults')).toBeNull();
    expect(screen.queryByText('LongTermMedical')).toBeNull();
    expect(screen.queryByText('Incarceration')).toBeNull();
  });

  it('should show not deceased', () => {
    renderWrapper(CALL_RESULT_CODE.NOT_DECEASED);

    expect(screen.queryByText('BankruptcyResults')).toBeNull();
    expect(screen.queryByText('DeceasedResults')).toBeNull();
    expect(screen.queryByText('PromiseToPayCallResults')).toBeNull();
    expect(screen.queryByText('HardShipResults')).toBeNull();
    expect(screen.queryByText('CCCSResults')).toBeNull();
    expect(screen.queryByText('LongTermMedical')).toBeNull();
    expect(screen.queryByText('Incarceration')).toBeNull();
  });

  it('should show bankruptcy', () => {
    renderWrapper(CALL_RESULT_CODE.BANKRUPTCY);

    expect(screen.queryByText('DeceasedResults')).toBeNull();
    expect(screen.queryByText('PromiseToPayCallResults')).toBeNull();
    expect(screen.queryByText('HardShipResults')).toBeNull();
    expect(screen.queryByText('CCCSResults')).toBeNull();
    expect(screen.getByText('BankruptcyResults')).toBeInTheDocument();
    expect(screen.queryByText('LongTermMedical')).toBeNull();
    expect(screen.queryByText('Incarceration')).toBeNull();
  });

  it('should show deceased', () => {
    renderWrapper(CALL_RESULT_CODE.DECEASE);

    expect(screen.queryByText('BankruptcyResults')).toBeNull();
    expect(screen.queryByText('PromiseToPayCallResults')).toBeNull();
    expect(screen.queryByText('HardShipResults')).toBeNull();
    expect(screen.queryByText('CCCSResults')).toBeNull();
    expect(screen.getByText('DeceasedResults')).toBeInTheDocument();
    expect(screen.queryByText('LongTermMedical')).toBeNull();
    expect(screen.queryByText('Incarceration')).toBeNull();
  });

  it('should show Promise to Pay', () => {
    renderWrapper(CALL_RESULT_CODE.PROMISE_TO_PAY);

    expect(screen.queryByText('BankruptcyResults')).toBeNull();
    expect(screen.queryByText('DeceasedResults')).toBeNull();
    expect(screen.queryByText('HardShipResults')).toBeNull();
    expect(screen.queryByText('CCCSResults')).toBeNull();
    expect(screen.getByText('PromiseToPayCallResults')).toBeInTheDocument();
    expect(screen.queryByText('LongTermMedical')).toBeNull();
    expect(screen.queryByText('Incarceration')).toBeNull();
  });

  it('should show CCCS', () => {
    renderWrapper(CALL_RESULT_CODE.CCCS);

    expect(screen.queryByText('BankruptcyResults')).toBeNull();
    expect(screen.queryByText('DeceasedResults')).toBeNull();
    expect(screen.queryByText('PromiseToPayCallResults')).toBeNull();
    expect(screen.queryByText('HardShipResults')).toBeNull();
    expect(screen.getByText('CCCSResults')).toBeInTheDocument();
    expect(screen.queryByText('LongTermMedical')).toBeNull();
    expect(screen.queryByText('Incarceration')).toBeNull();
  });

  it('should show Hardship', () => {
    renderWrapper(CALL_RESULT_CODE.HARDSHIP);

    expect(screen.queryByText('BankruptcyResults')).toBeNull();
    expect(screen.queryByText('DeceasedResults')).toBeNull();
    expect(screen.queryByText('PromiseToPayCallResults')).toBeNull();
    expect(screen.queryByText('CCCSResults')).toBeNull();
    expect(screen.getByText('HardShipResults')).toBeInTheDocument();
    expect(screen.queryByText('LongTermMedical')).toBeNull();
    expect(screen.queryByText('Incarceration')).toBeNull();
  });

  it('should show Long-Term Medical', () => {
    renderWrapper(CALL_RESULT_CODE.LONG_TERM_MEDICAL);

    expect(screen.queryByText('BankruptcyResults')).toBeNull();
    expect(screen.queryByText('DeceasedResults')).toBeNull();
    expect(screen.queryByText('PromiseToPayCallResults')).toBeNull();
    expect(screen.queryByText('HardShipResults')).toBeNull();
    expect(screen.queryByText('CCCSResults')).toBeNull();
    expect(screen.queryByText('LongTermMedical')).toBeInTheDocument();
    expect(screen.queryByText('Incarceration')).toBeNull();
  });

  it('should show Incarceration ', () => {
    renderWrapper(CALL_RESULT_CODE.INCARCERATION);

    expect(screen.queryByText('BankruptcyResults')).toBeNull();
    expect(screen.queryByText('DeceasedResults')).toBeNull();
    expect(screen.queryByText('PromiseToPayCallResults')).toBeNull();
    expect(screen.queryByText('HardShipResults')).toBeNull();
    expect(screen.queryByText('CCCSResults')).toBeNull();
    expect(screen.queryByText('LongTermMedical')).toBeNull();
    expect(screen.queryByText('Incarceration')).toBeInTheDocument();
  });

  it('should show Settlement ', () => {
    renderWrapper(CALL_RESULT_CODE.SETTLEMENT);

    expect(screen.queryByText('Settlement')).toBeInTheDocument();
  });
});
