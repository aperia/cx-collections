// i18n/constant
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
// Components
import { Button, Icon, Tooltip } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { CCCS_PROCESS_COMPLETE } from 'pages/CCCS/constants';
import { selectCCCSProcessStatus } from 'pages/CCCS/_redux/selectors';
import React, { useMemo } from 'react';
// Hooks
import { useDispatch } from 'react-redux';
import { CALL_RESULT_CODE, COLLECTION_METHOD } from '../../constants';
import { isRemoveEditCollectionForm } from '../../helpers';
import { collectionFormActions } from '../../_redux/reducers';
// Selectors
import {
  selectCallResult,
  selectViewDeathCertificate
} from '../../_redux/selectors';

const CallResultHeader: React.FC = () => {
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();

  const callResultValue =
    useStoreIdSelector<CALL_RESULT_CODE>(selectCallResult);
  const cccsProcessStatus: string = useStoreIdSelector(selectCCCSProcessStatus);

  const handleEditButton = () => {
    dispatch(collectionFormActions.triggerEditCollectionForm({ storeId }));
    dispatch(
      collectionFormActions.changeMethod({
        storeId,
        method: COLLECTION_METHOD.EDIT
      })
    );
  };

  const viewDeathCertificateMode = useStoreIdSelector<boolean>(
    selectViewDeathCertificate
  );

  const isHideEditButton = useMemo(() => {
    return (
      isRemoveEditCollectionForm(callResultValue) ||
      viewDeathCertificateMode ||
      callResultValue === CALL_RESULT_CODE.HARDSHIP ||
      callResultValue === CALL_RESULT_CODE.SETTLEMENT
    );
  }, [callResultValue, viewDeathCertificateMode]);

  return (
    <div className="d-flex justify-content-between align-items-center">
      <p
        className=" fs-14 fw-500 color-grey-d20"
        data-testid={genAmtId('CallResultHeader', 'title', '')}
      >
        {t(I18N_COLLECTION_FORM.CALL_RESULT)}
      </p>
      {isHideEditButton ? null : (
        <Tooltip
          element={t(I18N_COLLECTION_FORM.EDIT_INFORMATION)}
          placement="top"
          variant="primary"
          triggerClassName="mr-n4"
          dataTestId="callResultsHeader_edit-info"
        >
          <Button
            variant="icon-secondary"
            onClick={handleEditButton}
            dataTestId="callResultsHeader_edit-btn"
            disabled={
              cccsProcessStatus !== CCCS_PROCESS_COMPLETE &&
              callResultValue === CALL_RESULT_CODE.CCCS
            }
          >
            <Icon name="edit" />
          </Button>
        </Tooltip>
      )}
    </div>
  );
};

export default CallResultHeader;
