import React from 'react';
import classNames from 'classnames';
import isEmpty from 'lodash.isempty';

// Hooks
import { useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import {
  useDelayProgress,
  useTriggerDelayProgress
} from 'pages/CollectionForm/hooks/useDelayProgress';

// Selectors
import {
  selectCallResult,
  selectIsHavingData,
  selectIsLoadingLastFollowUpData,
  takeInProgressStatus
} from '../../_redux/selectors';

// Components
import Header from './Header';
import Status from './Status';
import Body from './Body';
import { InlineMessage } from 'app/_libraries/_dls/components';

// Const
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { selectPromiseToPayDetailData } from 'pages/PromiseToPay/_redux/selectors';
import {
  CALL_RESULT_CODE,
  STATUS_HARDSHIP
} from 'pages/CollectionForm/constants';
import { selectLoading as selectLoadingME } from 'pages/LongTermMedical/_redux/selectors';
import { selectLoading as selectLoadingFD } from 'pages/Incarceration/_redux/selectors';
import { selectIsLoading, selectStatus } from 'pages/HardShip/_redux/selectors';

const CallResults: React.FC = () => {
  const { t } = useTranslation();

  const isHavingData = useStoreIdSelector<boolean>(selectIsHavingData);

  const isInProgress = useStoreIdSelector(takeInProgressStatus);

  const callResultsType = useStoreIdSelector(selectCallResult);

  const promiseToPayData = useStoreIdSelector(selectPromiseToPayDetailData);

  const isLoadingFD = useStoreIdSelector(selectLoadingFD);

  const isLoadingME = useStoreIdSelector(selectLoadingME);

  const isLoadingCallResult = useStoreIdSelector(
    selectIsLoadingLastFollowUpData
  );

  const statusHardship = useStoreIdSelector(selectStatus);

  const loadingHardship = useStoreIdSelector(selectIsLoading);

  // display message for Remove Hardship
  const isMessage =
    callResultsType === CALL_RESULT_CODE.NOT_HARDSHIP &&
    statusHardship === STATUS_HARDSHIP;

  useDelayProgress();
  useTriggerDelayProgress();
  const isPromiseToPayEmpty =
    callResultsType === CALL_RESULT_CODE.PROMISE_TO_PAY &&
    isEmpty(promiseToPayData);
  /**
   * if Promise to pay
   * return body to get Data
   * if NO data return null
   */
  if (isPromiseToPayEmpty)
    return (
      <>
        {isInProgress && (
          <InlineMessage
            variant="info"
            withIcon
            dataTestId="callResults-inprogress"
          >
            {t(I18N_COLLECTION_FORM.IN_PROGRESS_STATUS)}
          </InlineMessage>
        )}
        <Body />
      </>
    );

  if (!isHavingData)
    return (
      <div
        className={classNames('call-result-info', {
          loading: isLoadingCallResult
        })}
      />
    );

  return (
    <div className="px-24 overlap-next-action">
      {isInProgress || isMessage ? (
        <InlineMessage
          variant="info"
          withIcon
          dataTestId="callResults-inprogress"
        >
          {t(I18N_COLLECTION_FORM.IN_PROGRESS_STATUS)}
        </InlineMessage>
      ) : (
        <div
          className={classNames({
            loading: isLoadingFD || isLoadingME || loadingHardship
          })}
        >
          <Header />
          <Status />
          <Body />
        </div>
      )}
    </div>
  );
};

export default CallResults;
