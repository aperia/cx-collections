import React from 'react';
import { screen } from '@testing-library/react';
import CollectionFormActivities from '.';
import { renderMockStoreId, storeId } from 'app/test-utils';
import * as entitlementsHelpers from 'app/entitlements/helpers';
import { CALL_RESULT_CODE } from '../constants';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

window.appConfig = {
  commonConfig: {
    operatorID: 'ACB'
  }
} as AppConfiguration;

describe('should render UI', () => {
  it('test render UI', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);

    renderMockStoreId(<CollectionFormActivities />);

    expect(screen.getByText('txt_call_result')).toBeInTheDocument();
  });

  it('test render UI', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);

    renderMockStoreId(<CollectionFormActivities />, {
      initialState: {
        collectionForm: {
          [storeId]: {
            lastDelayCallResult: 'ST',
            lastFollowUpData: {
              callResultType: CALL_RESULT_CODE.SETTLEMENT
            },
            isInProgress: true,
            uploadFiles: {}
          }
        },
        settlement: {
          [storeId]: {
            data: {
              acceptedToPay: false
            }
          }
        }
      }
    });

    expect(screen.getByText('txt_call_result')).toBeInTheDocument();
  });
});
