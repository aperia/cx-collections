import { createStore, applyMiddleware } from '@reduxjs/toolkit';
import { mockActionCreator, storeId, accEValue } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import thunk from 'redux-thunk';
import { triggerOpenCollectionForm } from './helper';
import { collectionFormActions } from '../_redux/reducers';
import { CALL_RESULT_CODE, COLLECTION_METHOD } from '../constants';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { bankruptcyActions } from 'pages/Bankruptcy/_redux/reducers';
import { deceasedActions } from 'pages/Deceased/_redux/reducers';
import { hardshipActions } from 'pages/HardShip/_redux/reducers';
import { longTermMedicalActions } from 'pages/LongTermMedical/_redux/reducers';
import { incarcerationActions } from 'pages/Incarceration/_redux/reducers';

const initialState: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      selectedCallResult: {
        code: 'code',
        description: 'des'
      },
      defaultCallResultRefData: [
        { code: 'code', description: 'des' },
        { code: 'code', description: 'des' }
      ],
      uploadFiles: {}
    }
  },
  tab: {
    tabStoreIdSelected: storeId,
    accEValueSelected: accEValue,
    tabs: []
  }
};

const store = createStore(rootReducer, initialState, applyMiddleware(thunk));

const storeError = createStore(
  rootReducer,
  {
    collectionForm: {
      [storeId]: {
        selectedCallResult: {
          code: 'cox',
          description: 'des'
        },
        defaultCallResultRefData: [
          { code: 'code', description: 'des' },
          { code: 'code', description: 'des' }
        ]
      }
    },
    tab: {
      tabStoreIdSelected: storeId,
      accEValueSelected: accEValue
    }
  },
  applyMiddleware(thunk)
);

const customStore = (type: CALL_RESULT_CODE) =>
  createStore(
    rootReducer,
    {
      collectionForm: {
        [storeId]: {
          selectedCallResult: {
            code: type,
            description: 'des'
          },
          defaultCallResultRefData: [
            { code: type, description: 'des' },
            { code: 'code', description: 'des' }
          ]
        }
      },
      tab: {
        tabStoreIdSelected: storeId,
        accEValueSelected: accEValue
      }
    },
    applyMiddleware(thunk)
  );

const collectionFormActionsSpy = mockActionCreator(collectionFormActions);
const bankruptcyActionsSpy = mockActionCreator(bankruptcyActions);
const deceasedActionsSpy = mockActionCreator(deceasedActions);
const hardshipActionsSpy = mockActionCreator(hardshipActions);
const longTermMedicalActionsSpy = mockActionCreator(longTermMedicalActions);
const incarcerationActionsSpy = mockActionCreator(incarcerationActions);

describe('triggerOpenCollectionForm', () => {
  afterAll(() => {
    jest.clearAllMocks();
  });

  it('without accEvalue selected', async () => {
    const spy = collectionFormActionsSpy('changeMethod');

    const newStore = createStore(
      rootReducer,
      {
        ...initialState,
        tab: { tabs: [], tabStoreIdSelected: storeId }
      },
      applyMiddleware(thunk)
    );

    triggerOpenCollectionForm()(newStore.dispatch, newStore.getState, {});

    expect(spy).toHaveBeenCalledWith({
      storeId,
      method: COLLECTION_METHOD.CREATE
    });
  });

  it('with normal case', async () => {
    const spy = collectionFormActionsSpy('changeMethod');
    triggerOpenCollectionForm()(store.dispatch, store.getState, {});

    expect(spy).toHaveBeenCalledWith({
      storeId,
      method: COLLECTION_METHOD.CREATE
    });
  });

  it('with isDefaultCallResultType', async () => {
    const spy = collectionFormActionsSpy('updateActionEntryWorkFlow');

    triggerOpenCollectionForm()(storeError.dispatch, storeError.getState, {});

    expect(spy).toHaveBeenCalledWith({
      storeId
    });
  });

  it('with CALL_RESULT_CODE.NOT_BANKRUPTCY', async () => {
    const bankSpy = bankruptcyActionsSpy('triggerRemoveBankruptcyStatus');

    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });

    jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);

    triggerOpenCollectionForm()(
      customStore(CALL_RESULT_CODE.NOT_BANKRUPTCY).dispatch,
      customStore(CALL_RESULT_CODE.NOT_BANKRUPTCY).getState,
      {}
    );

    expect(bankSpy).toHaveBeenCalledWith({
      storeId
    });
  });

  it('with CALL_RESULT_CODE.NOT_DECEASED', async () => {
    const bankSpy = deceasedActionsSpy('triggerRemoveDeceasedStatus');
    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });

    jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);

    triggerOpenCollectionForm()(
      customStore(CALL_RESULT_CODE.NOT_DECEASED).dispatch,
      customStore(CALL_RESULT_CODE.NOT_DECEASED).getState,
      {}
    );

    expect(bankSpy).toHaveBeenCalledWith({
      storeId,
      entryCode: 'ND'
    });
  });

  it('with CALL_RESULT_CODE.NOT_HARDSHIP', async () => {
    const bankSpy = hardshipActionsSpy('triggerRemoveHardshipStatusRequest');
    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });

    jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);

    triggerOpenCollectionForm()(
      customStore(CALL_RESULT_CODE.NOT_HARDSHIP).dispatch,
      customStore(CALL_RESULT_CODE.NOT_HARDSHIP).getState,
      {}
    );

    expect(bankSpy).toHaveBeenCalledWith({
      postData: {
        storeId
      }
    });
  });

  it('with CALL_RESULT_CODE.NOT_INCARCERATION', async () => {
    const spy = incarcerationActionsSpy('onChangeOpenModal');
    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });

    jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);

    triggerOpenCollectionForm()(
      customStore(CALL_RESULT_CODE.NOT_INCARCERATION).dispatch,
      customStore(CALL_RESULT_CODE.NOT_INCARCERATION).getState,
      {}
    );

    expect(spy).toHaveBeenCalledWith({
      storeId
    });
  });

  it('with CALL_RESULT_CODE.NOT_LONG_TERM_MEDICAL', async () => {
    const spy = longTermMedicalActionsSpy('onChangeOpenModal');
    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });

    jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);

    triggerOpenCollectionForm()(
      customStore(CALL_RESULT_CODE.NOT_LONG_TERM_MEDICAL).dispatch,
      customStore(CALL_RESULT_CODE.NOT_LONG_TERM_MEDICAL).getState,
      {}
    );

    expect(spy).toHaveBeenCalledWith({
      storeId
    });
  });
});
