import React from 'react';

import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

import { selectAcceptedToPay } from 'pages/Settlement/_redux/selectors';

import { CALL_RESULT_CODE } from '../constants';

// components
import CallResults from './CallResults';
import GeneralInformation from './GeneralInformation';
import SelectCollectionForm from './SelectCollectionForm';
import { selectCallResult } from '../_redux/selectors';

interface FollowUpProps {}

const FollowUp: React.FC<FollowUpProps> = () => {
  const { storeId } = useAccountDetail();

  const callResultsType = useStoreIdSelector(selectCallResult);

  const acceptedToPay = useStoreIdSelector<boolean>(selectAcceptedToPay);

  const isHideCallResult =
    callResultsType === CALL_RESULT_CODE.SETTLEMENT && acceptedToPay === false;

  return (
    <>
      <div>
        <SelectCollectionForm />
        {checkPermission(PERMISSIONS.COLLECTION_ACTIVITIES_VIEW, storeId) && (
          <>
            <GeneralInformation />
            {isHideCallResult ? null : <hr />}
          </>
        )}
        {isHideCallResult ? null : <CallResults />}
      </div>
    </>
  );
};

export default FollowUp;
