import React from 'react';
import { batch } from 'react-redux';

// Component
import ConfirmRemove from './CallResults/ConfirmRemove';

// Const
import { CALL_RESULT_CODE, COLLECTION_METHOD } from '../constants';
import { DECEASED_ENTRY_CODE } from 'pages/Deceased/constants';
import { I18N_COLLECTION_FORM, I18N_COMMON_TEXT } from 'app/constants/i18n';

// Actions
import { collectionFormActions } from '../_redux/reducers';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { bankruptcyActions } from 'pages/Bankruptcy/_redux/reducers';
import { deceasedActions } from 'pages/Deceased/_redux/reducers';
import { hardshipActions } from 'pages/HardShip/_redux/reducers';
import { triggerStartCollectionForm } from '../_redux/startCollectionForm';

// Helpers
import { isDefaultCallResultType } from '../helpers';
import { longTermMedicalActions } from 'pages/LongTermMedical/_redux/reducers';
import { incarcerationActions } from 'pages/Incarceration/_redux/reducers';
import { timerWorkingActions } from 'pages/TimerWorking/_redux/reducers';
import { COLLECTION_CODE_DEFAULT } from 'pages/TimerWorking/constants';

export const triggerOpenCollectionForm: AppThunk =
  () => (dispatch, getState) => {
    const { collectionForm, tab } = getState();
    const { tabStoreIdSelected: storeId } = tab;
    const { selectedCallResult, defaultCallResultRefData } =
      collectionForm[storeId];
    const callResultCode = selectedCallResult?.code;

    dispatch(
      timerWorkingActions.setCallResultCode({
        storeId,
        lastCollectionCode:
          callResultCode?.length === 2
            ? callResultCode
            : COLLECTION_CODE_DEFAULT
      })
    );

    // If call result is not default type then prevent open modal
    if (
      !isDefaultCallResultType(
        selectedCallResult?.code,
        defaultCallResultRefData
      )
    ) {
      return dispatch(
        collectionFormActions.updateActionEntryWorkFlow({
          storeId
        })
      );
    }

    if (selectedCallResult?.code === CALL_RESULT_CODE.NOT_BANKRUPTCY) {
      return dispatch(
        confirmModalActions.open({
          title: I18N_COLLECTION_FORM.BANKRUPTCY_CONFIRM_REMOVE_STATUS_TITLE,
          body: I18N_COLLECTION_FORM.BANKRUPTCY_CONFIRM_REMOVE_STATUS,
          confirmCallback: () => {
            batch(() => {
              dispatch(
                bankruptcyActions.triggerRemoveBankruptcyStatus({
                  storeId
                })
              );

              dispatch(
                collectionFormActions.selectCallResult({
                  storeId,
                  item: undefined
                })
              );
            });
          },
          confirmText: I18N_COMMON_TEXT.REMOVE,
          size: {
            sm: true
          }
        })
      );
    }

    if (selectedCallResult?.code === CALL_RESULT_CODE.NOT_DECEASED) {
      return dispatch(
        confirmModalActions.open({
          title: I18N_COLLECTION_FORM.CONFIRM_REMOVE_DECEASED_STATUS,
          body: I18N_COLLECTION_FORM.ARE_YOU_SURE_REMOVE_DECEASED_STATUS,
          confirmCallback: () => {
            batch(() => {
              dispatch(
                deceasedActions.triggerRemoveDeceasedStatus({
                  storeId,
                  entryCode: DECEASED_ENTRY_CODE.NOT_DECEASED
                })
              );

              dispatch(
                collectionFormActions.getCallResultsRefData({
                  storeId
                })
              );

              dispatch(
                collectionFormActions.selectCallResult({
                  storeId,
                  item: undefined
                })
              );
            });
          },
          confirmText: I18N_COMMON_TEXT.REMOVE,
          size: {
            sm: true
          }
        })
      );
    }

    if (selectedCallResult?.code === CALL_RESULT_CODE.NOT_LONG_TERM_MEDICAL) {
      return dispatch(longTermMedicalActions.onChangeOpenModal({ storeId }));
    }

    if (selectedCallResult?.code === CALL_RESULT_CODE.NOT_INCARCERATION) {
      return dispatch(incarcerationActions.onChangeOpenModal({ storeId }));
    }

    if (selectedCallResult?.code === CALL_RESULT_CODE.NOT_HARDSHIP) {
      return dispatch(
        confirmModalActions.open({
          title: I18N_COLLECTION_FORM.ARE_YOU_SURE_REMOVE_HARDSHIP,
          body: <ConfirmRemove storeId={storeId} />,
          confirmCallback: () => {
            batch(() => {
              dispatch(
                hardshipActions.triggerRemoveHardshipStatusRequest({
                  postData: {
                    storeId
                  }
                })
              );

              dispatch(
                collectionFormActions.selectCallResult({
                  storeId,
                  item: undefined
                })
              );

              dispatch(
                collectionFormActions.getCallResultsRefData({
                  storeId
                })
              );
            });
          },
          confirmText: I18N_COMMON_TEXT.REMOVE,
          size: {
            sm: true
          }
        })
      );
    }

    dispatch(triggerStartCollectionForm());
    dispatch(
      collectionFormActions.changeMethod({
        storeId,
        method: COLLECTION_METHOD.CREATE
      })
    );
  };
