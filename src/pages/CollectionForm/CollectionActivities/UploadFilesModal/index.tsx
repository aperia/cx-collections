import React, {
  FC,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';

import { CancelTokenSource } from 'axios';

// constant
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { EMPTY_STRING, SUPPORTED_FILE_TYPES } from 'app/constants';

// components
import {
  InlineMessage,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import Upload, { PromiseFile } from 'app/_libraries/_dls/components/Upload';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { StateFile } from 'app/_libraries/_dls/components/Upload/File';

// redux
import { batch, useDispatch } from 'react-redux';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

import { selectRawAccountDetail } from '../../../Deceased/_redux/selectors';
import { convertFileToBase64 } from 'app/helpers/convertFile';
import {
  selectUploadFileType,
  selectUploadModal
} from 'pages/CollectionForm/_redux/selectors';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { UploadFileType, UploadRequestBody } from 'pages/CollectionForm/types';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { collectionFormService } from 'pages/CollectionForm/collectionFormServices';
import { combineDocumentClassification } from 'pages/CollectionForm/helpers';
import { deceasedActions } from 'pages/Deceased/_redux/reducers';
import { DeceasedResultTab } from 'pages/Deceased/types';
import axios from 'axios';
import { hardshipActions } from 'pages/HardShip/_redux/reducers';
import { HardshipResultTab } from 'pages/HardShip/HardShipResults/MilitaryDeploymentResult/constants';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { UPLOAD_FILE_MODAL_TEXT } from 'pages/CollectionForm/constants';

interface StateFileProp extends StateFile {
  fileId?: string;
}

export interface DeathCertificatesProps {}

const UploadFilesModal: FC<DeathCertificatesProps> = ({}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const cancelTokenRef = useRef<CancelTokenSource | null>();

  const rawAccountDetailData = useStoreIdSelector<MagicKeyValue>(
    selectRawAccountDetail
  );
  const openModal = useStoreIdSelector<boolean>(selectUploadModal);
  const uploadType = useStoreIdSelector<UploadFileType>(selectUploadFileType);
  const ref = useRef<any>(null);

  const [selectedFiles, setSelectedFiles] = useState<StateFileProp[]>([]);
  const [isError, setIsError] = useState(false);

  const { storeId, accEValue } = useAccountDetail();

  const { title, description, toastMessage } =
    UPLOAD_FILE_MODAL_TEXT[uploadType] || {};

  const isDisableUpload = useMemo(() => {
    return !selectedFiles.some(
      (file: StateFile) =>
        file.status === STATUS.valid || file.status === STATUS.error
    );
  }, [selectedFiles]);

  const handleChange = (data: { files: StateFileProp[] }) => {
    setSelectedFiles(data.files);
  };

  const handleUploadFile = useCallback(() => {
    // upload the files first and then upload the deceased data later
    ref.current?.uploadFiles();
  }, []);

  const {
    clientIdentifier,
    systemIdentifier,
    principalIdentifier,
    agentIdentifier
  } = rawAccountDetailData || {};
  const { commonConfig } = window.appConfig || {};

  const formatRequest = async (file: any): Promise<UploadRequestBody> => {
    const fileName = file?.name;
    const fileBase64 = await convertFileToBase64(file);

    return {
      documentClassification: combineDocumentClassification(
        uploadType,
        accEValue,
        uploadType
      ),
      file: fileBase64,
      fileName,
      common: {
        agent: agentIdentifier,
        clientNumber: clientIdentifier,
        prin: principalIdentifier,
        system: systemIdentifier,
        app: commonConfig?.app || EMPTY_STRING,
        accountId: accEValue
      }
    };
  };

  const uploadFiles = (files: any[], onUploadProgress: any) => {
    cancelTokenRef.current = axios.CancelToken.source();
    return files.map(async (file: any) => {
      const requestBody = await formatRequest(file);

      return collectionFormService
        .uploadFiles(requestBody, {
          onUploadProgress: event => {
            onUploadProgress(file.idx, event);
          },
          cancelToken: cancelTokenRef.current?.token
        })
        .then(() => {
          return { idx: file.idx };
        })
        .catch(error => {
          return Promise.reject({ idx: file.idx });
        });
    });
  };

  //** this function is run after all of all of uploads is done */
  const handleFinallyPromise = (resultPromise: PromiseFile[]) => {
    const hasError = selectedFiles?.some(file => file.status === STATUS.error);

    const hasAtLeastOnSuccess = selectedFiles?.some(
      file => file.status === STATUS.success
    );

    const hasInValid = selectedFiles?.some(
      file => file.status === STATUS.invalid
    );

    setIsError(hasError);

    batch(() => {
      if (hasAtLeastOnSuccess) {
        dispatch(
          actionsToast.addToast({ message: toastMessage, type: 'success' })
        );
        dispatch(
          collectionFormActions.getListCaseDocument({
            storeId,
            type: uploadType
          })
        );
        if (uploadType === 'deathCertificate') {
          dispatch(
            deceasedActions.updateSelectedDeceasedResultTab({
              storeId,
              tabKey: DeceasedResultTab.DeathCertificate
            })
          );
        }
        if (uploadType === 'hardShip') {
          // active tab
          dispatch(
            hardshipActions.setActiveTabMilitary({
              storeId,
              value: HardshipResultTab.files
            })
          );
        }
      }
      if (!hasError && !hasInValid) {
        dispatch(collectionFormActions.closeUploadFileModal({ storeId }));
      }
    });
  };

  const handleOnRemoveFile = (indexInFiles: number) => {
    setSelectedFiles(files =>
      files?.filter((_, index) => index !== indexInFiles)
    );
  };

  const ErrorInline = () => {
    if (!selectedFiles.length) return null;
    return (
      <div className="mt-16">
        <h5
          className="pb-8"
          data-testid={genAmtId('uploadFilesModal', 'errorInline-files', '')}
        >
          {t(I18N_COMMON_TEXT.FILES)}
        </h5>
        {isError && (
          <InlineMessage
            variant="danger"
            dataTestId="uploadFilesModal-errorInline-error"
          >
            {t(I18N_COMMON_TEXT.UPLOAD_FAILED_MESSAGE)}
          </InlineMessage>
        )}
      </div>
    );
  };

  const handleCloseModal = () => {
    cancelTokenRef.current?.cancel();
    dispatch(
      collectionFormActions.closeUploadFileModal({
        storeId
      })
    );
  };

  useEffect(() => {
    if (!openModal) return;
    setSelectedFiles([]);
    setIsError(false);
  }, [openModal]);

  if (!openModal) return null;

  return (
    <>
      <Modal sm show={openModal} loading={false} dataTestId="uploadFilesModal">
        <ModalHeader
          border
          closeButton
          onHide={handleCloseModal}
          dataTestId="uploadFilesModal-header"
        >
          <ModalTitle dataTestId="uploadFilesModal-header-title">
            {t(title)}
          </ModalTitle>
        </ModalHeader>
        <ModalBody>
          <p
            className="mb-16"
            data-testid={genAmtId('uploadFilesModal-body', 'description', '')}
          >
            {t(description)}
          </p>
          <Upload
            onChangeFile={handleChange}
            files={selectedFiles}
            ref={ref}
            saveUrl={uploadFiles}
            multiple
            axiosOptions={{
              headers: { 'Content-Type': 'multipart/form-data' }
            }}
            typeFiles={SUPPORTED_FILE_TYPES}
            renderElm={ErrorInline}
            maxFile={5 * 1024 * 1024}
            onFinally={handleFinallyPromise}
            onRemove={handleOnRemoveFile}
            classNameFile="col-12"
            classNameListFile="row"
            dataTestId={genAmtId('uploadFilesModal-body', 'upload', '')}
          />
        </ModalBody>
        <ModalFooter
          dataTestId="uploadFilesModal-footer"
          okButtonText={t(I18N_COMMON_TEXT.UPLOAD)}
          cancelButtonText={t(I18N_COMMON_TEXT.CLOSE)}
          onCancel={handleCloseModal}
          disabledOk={isDisableUpload}
          onOk={handleUploadFile}
        />
      </Modal>
    </>
  );
};

export default UploadFilesModal;
