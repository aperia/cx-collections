import React from 'react';
import { fireEvent, screen } from '@testing-library/react';
import UploadFilesModal from '.';
import {
  renderMockStoreId,
  mockActionCreator,
  storeId,
  responseDefault
} from 'app/test-utils';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { collectionFormService } from 'pages/CollectionForm/collectionFormServices';
import * as convertFile from 'app/helpers/convertFile';

// redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { deceasedActions } from 'pages/Deceased/_redux/reducers';
import { hardshipActions } from 'pages/HardShip/_redux/reducers';
import { HardshipResultTab } from 'pages/HardShip/HardShipResults/MilitaryDeploymentResult/constants';
import { DeceasedResultTab } from 'pages/Deceased/types';
import { UploadProps } from 'app/_libraries/_dls/components/Upload';
import { STATUS } from 'app/_libraries/_dls/components/Upload/helper';
import { StateFile } from 'app/_libraries/_dls/components/Upload/File';

jest.mock('app/_libraries/_dls/components/Upload', () => {
  const uploadComponent = jest.requireActual(
    'app/_libraries/_dls/components/Upload'
  );
  const defaultComponent = (props: UploadProps) => {
    const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      const files = event.target.files as StateFile;
      files.idx = 'Uakgb_J5m9g-0JDMbcJqL';
      props.onChangeFile!({ files: [files] });
    };

    return (
      <div>
        <input onChange={handleOnChange} />
        <button
          data-testid="Upload_saveUrl"
          onClick={() => {
            props
              .saveUrl([{ idx: 'Uakgb_J5m9g-0JDMbcJqL' }], jest.fn())
              .map((filePromise: Promise<{ idx: string }>) =>
                filePromise.then().catch(() => {})
              );
          }}
        />
        <button
          data-testid="Upload_onFinally"
          onClick={() => props.onFinally!([])}
        />
        <button
          data-testid="Upload_onRemove"
          onClick={() => props.onRemove!(0)}
        />
        {props.renderElm!({ files: [] })}
      </div>
    );
  };
  return {
    ...uploadComponent,
    __esModule: true,
    default: defaultComponent,
    defaults: defaultComponent
  };
});

const initialState: Partial<RootState> = {
  accountDetail: {
    [storeId]: {
      rawData: {
        key: '1',
        value: '123'
      }
    }
  },
  collectionForm: {
    [storeId]: {
      uploadFiles: {
        type: 'hardShip',
        openModal: true
      }
    }
  }
};

const stateDeathCertificate: Partial<RootState> = {
  accountDetail: {
    [storeId]: {
      rawData: {
        key: '1',
        value: '123'
      }
    }
  },
  collectionForm: {
    [storeId]: {
      uploadFiles: {
        type: 'deathCertificate',
        openModal: true
      }
    }
  }
};

const errorState: Partial<RootState> = {
  accountDetail: {
    [storeId]: {
      rawData: {
        key: '1',
        value: '123'
      }
    }
  },
  collectionForm: {
    [storeId]: {
      uploadFiles: {
        type: 'deathCertificate',
        openModal: false
      }
    }
  }
};

const renderWrapper = (state: Partial<RootState>) => {
  return renderMockStoreId(<UploadFilesModal />, {
    initialState: state
  });
};

HTMLCanvasElement.prototype.getContext = jest.fn();

const toastSpy = mockActionCreator(actionsToast);
const collectionFormSpy = mockActionCreator(collectionFormActions);
const deceasedSpy = mockActionCreator(deceasedActions);
const hardshipSpy = mockActionCreator(hardshipActions);

const file = new File([new ArrayBuffer(8)], 'file.jpg');

beforeEach(() => {
  jest
    .spyOn(collectionFormService, 'uploadFiles')
    .mockResolvedValue(responseDefault);

  jest.spyOn(convertFile, 'convertFileToBase64').mockResolvedValue('MQ==');
});

describe('render', () => {
  it('render component with empty state', () => {
    const { wrapper } = renderWrapper({});
    expect(wrapper.container.innerHTML).toEqual('');
  });

  it('render component error', () => {
    const { wrapper } = renderWrapper(errorState);
    expect(wrapper.container.innerHTML).toEqual('');
  });
});

describe('Actions', () => {
  let addToastMock: jest.Mock;
  let getListCaseDocumentMock: jest.Mock;
  let updateSelectedDeceasedResultTabMock: jest.Mock;
  let setActiveTabMilitaryMock: jest.Mock;
  let closeUploadFileModalMock: jest.Mock;

  beforeEach(() => {
    addToastMock = toastSpy('addToast');
    getListCaseDocumentMock = collectionFormSpy('getListCaseDocument');
    updateSelectedDeceasedResultTabMock = deceasedSpy(
      'updateSelectedDeceasedResultTab'
    );
    setActiveTabMilitaryMock = hardshipSpy('setActiveTabMilitary');
    closeUploadFileModalMock = collectionFormSpy('closeUploadFileModal');
  });

  it('handleCloseModal', () => {
    renderWrapper(initialState);

    fireEvent.click(screen.getByText(I18N_COMMON_TEXT.CLOSE));

    expect(closeUploadFileModalMock).toBeCalledWith({ storeId });
  });

  it('uploadFiles error', async () => {
    jest
      .spyOn(collectionFormService, 'uploadFiles')
      .mockImplementation(
        (_, { onUploadProgress }: { onUploadProgress?: Function }) => {
          onUploadProgress && onUploadProgress({});
          return Promise.reject(responseDefault);
        }
      );

    renderWrapper(initialState);

    fireEvent.change(document.querySelector('input')!, {
      target: { files: { ...file, status: STATUS.error }, value: 'value' }
    });

    fireEvent.click(screen.getByTestId('Upload_saveUrl'));
    fireEvent.click(screen.getByTestId('Upload_onFinally'));
    fireEvent.click(screen.getByText(I18N_COMMON_TEXT.UPLOAD));
    fireEvent.click(screen.getByTestId('Upload_onRemove'));

    expect(addToastMock).not.toBeCalled();
    expect(getListCaseDocumentMock).not.toBeCalled();
    expect(updateSelectedDeceasedResultTabMock).not.toBeCalled();
    expect(setActiveTabMilitaryMock).not.toBeCalled();
    expect(closeUploadFileModalMock).not.toBeCalled();
  });

  it('uploadFiles hardship', async () => {
    renderWrapper(initialState);

    fireEvent.change(document.querySelector('input')!, {
      target: { files: { ...file, status: STATUS.success }, value: 'value' }
    });

    fireEvent.click(screen.getByTestId('Upload_saveUrl'));
    fireEvent.click(screen.getByTestId('Upload_onFinally'));
    fireEvent.click(screen.getByText(I18N_COMMON_TEXT.UPLOAD));

    expect(addToastMock).toBeCalledWith({
      message: 'txt_orders_of_deployment_uploaded',
      type: 'success'
    });
    expect(getListCaseDocumentMock).toBeCalled();
    expect(updateSelectedDeceasedResultTabMock).not.toBeCalled();
    expect(setActiveTabMilitaryMock).toBeCalledWith({
      storeId,
      value: HardshipResultTab.files
    });
    expect(closeUploadFileModalMock).toBeCalledWith({ storeId });
  });

  it('uploadFiles deathCertificate', async () => {
    renderWrapper(stateDeathCertificate);

    fireEvent.change(document.querySelector('input')!, {
      target: { files: { ...file, status: STATUS.success }, value: 'value' }
    });

    fireEvent.click(screen.getByTestId('Upload_saveUrl'));
    fireEvent.click(screen.getByTestId('Upload_onFinally'));
    fireEvent.click(screen.getByText(I18N_COMMON_TEXT.UPLOAD));

    expect(addToastMock).toBeCalledWith({
      message: 'txt_death_certificates_uploaded',
      type: 'success'
    });
    expect(getListCaseDocumentMock).toBeCalled();
    expect(updateSelectedDeceasedResultTabMock).toBeCalledWith({
      storeId,
      tabKey: DeceasedResultTab.DeathCertificate
    });
    expect(setActiveTabMilitaryMock).not.toBeCalled();
    expect(closeUploadFileModalMock).toBeCalledWith({ storeId });
  });
});
