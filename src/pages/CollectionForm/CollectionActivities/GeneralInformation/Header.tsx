import React, { useMemo } from 'react';
import { Button, Icon, Tooltip } from 'app/_libraries/_dls/components';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';

// constants
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { PERMISSIONS } from 'app/entitlements/constants';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail } from 'app/hooks';

// redux
import { useDispatch } from 'react-redux';
import { generalInformationAction } from './_redux/reducers';

interface HeaderProps {}
const Header: React.FC<HeaderProps> = () => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const handleEditButton = () => {
    dispatch(generalInformationAction.toggleEditModal({ storeId }));
  };

  const isAllowEdit = useMemo(() => {
    if (
      checkPermission(
        PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_NEXT_WORK_DATE,
        storeId
      ) ||
      checkPermission(
        PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_MOVE_TO_QUEUE,
        storeId
      ) ||
      checkPermission(
        PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_DELINQUENCY_REASON,
        storeId
      )
    ) {
      return true;
    }
    return false;
  }, [storeId]);

  return (
    <div className="d-flex justify-content-between align-items-center">
      <p
        className="fs-14 fw-500 color-grey-d20"
        data-testid={genAmtId(
          'generalInformation_header_title',
          'edit_generalInformation',
          ''
        )}
      >
        {t(I18N_COLLECTION_FORM.GENERAL_INFORMATION)}
      </p>
      {isAllowEdit && (
        <Tooltip
          element={t(I18N_COLLECTION_FORM.EDIT_GENERAL_INFORMATION)}
          placement="top"
          variant="primary"
          triggerClassName="mr-n4"
          dataTestId="generalInformation_header_edit-info"
        >
          <Button
            variant="icon-secondary"
            onClick={handleEditButton}
            dataTestId="generalInformation_header_edit-btn"
          >
            <Icon
              name="edit"
              dataTestId="generalInformation_header_edit-icon"
            />
          </Button>
        </Tooltip>
      )}
    </div>
  );
};

export default Header;
