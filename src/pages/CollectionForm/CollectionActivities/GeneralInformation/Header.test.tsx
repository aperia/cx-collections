import React from 'react';
import { screen } from '@testing-library/react';

// redux
import { generalInformationAction } from './_redux/reducers';

// Component
import Header from './Header';

// Helper
import { mockActionCreator, renderMockStore, storeId } from 'app/test-utils';

// Hooks
import { AccountDetailProvider } from 'app/hooks';

// constants
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import * as permissionHelpers from 'app/entitlements/helpers';
import { PERMISSIONS } from 'app/entitlements/constants';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const renderWrapper = () => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <Header />
    </AccountDetailProvider>,
    {}
  ).wrapper;
};

const generalInformationSpy = mockActionCreator(generalInformationAction);

describe('Render', () => {
  let spy: jest.SpyInstance;

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('Render UI', () => {
    renderWrapper();

    expect(
      screen.getByText(I18N_COLLECTION_FORM.GENERAL_INFORMATION)
    ).toBeInTheDocument();
  });

  it('render ui without allowing to edit', () => {
    spy = jest
      .spyOn(permissionHelpers, 'checkPermission')
      .mockReturnValue(false);

    renderWrapper();

    expect(
      screen.queryByTestId('generalInformation_header_edit-btn')
    ).toBeNull();
  });

  it('with allow to edit move to queue', () => {
    spy = jest
      .spyOn(permissionHelpers, 'checkPermission')
      .mockImplementation(permission => {
        if (
          permission ===
            PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_NEXT_WORK_DATE ||
          permission === PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_MOVE_TO_QUEUE
        ) {
          return false;
        }
        return true;
      });

    renderWrapper();

    expect(screen.getByRole('button')).toBeInTheDocument();
  });

  it('handleEditButton', () => {
    const mockAction = generalInformationSpy('toggleEditModal');

    spy = jest
      .spyOn(permissionHelpers, 'checkPermission')
      .mockReturnValue(true);

    renderWrapper();

    screen.getByRole('button').click();

    expect(mockAction).toBeCalledWith({ storeId });
  });
});
