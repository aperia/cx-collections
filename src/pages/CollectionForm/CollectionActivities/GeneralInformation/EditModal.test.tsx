import React from 'react';
import { screen, waitFor } from '@testing-library/react';

// redux
import { generalInformationAction } from './_redux/reducers';

// Component
import EditModal from './EditModal';

// Helper
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import * as permissionHelpers from 'app/entitlements/helpers';
import { PERMISSIONS } from 'app/entitlements/constants';
import userEvent from '@testing-library/user-event';
import { createMockMutationObserver } from 'app/_libraries/_dls/test-utils/mocks/mockMutationObserver';

HTMLCanvasElement.prototype.getContext = jest.fn();

window.appConfig = {
  commonConfig: {
    operatorID: 'ACB'
  }
} as AppConfiguration;

const secureInfo = [
  {
    component: 'component_reference_list_config',
    jsonValue: {
      moveToQueues: [
        {
          code: '123',
          description: 'des',
          active: true
        },
        {
          code: '124',
          description: 'des',
          active: true
        }
      ],
      delinquencyReason: [
        {
          code: '123',
          description: 'des',
          active: true
        },
        {
          code: '124',
          description: 'des',
          active: true
        }
      ],
      deceasedContact: []
    }
  }
];

const initialState: Partial<RootState> = {
  generalInformation: {
    [storeId]: {
      openEditModal: true
    } as any
  },
  accountDetail: {
    [storeId]: {
      rawData: {
        collectionStatus: {
          nextWorkDate: '10-10-2020'
        }
      },
      secureInfo: {
        codeToText: {
          components: secureInfo
        }
      }
    }
  },
  accessToken: {
    [storeId]: {
      accountId: '022009001002624',
      accountInfo: {
        token: '99f0ecae-dbf0-4c6c-b76b-2fbf97927674'
      }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>, isProvider = true) => {
  return renderMockStoreId(<EditModal />, { initialState }, isProvider).wrapper;
};

const generalInformationSpy = mockActionCreator(generalInformationAction);

const spyCheckMission = (permissions: string[]) => {
  return jest
    .spyOn(permissionHelpers, 'checkPermission')
    .mockImplementation(permission => permissions.includes(permission));
};

describe('Render', () => {
  it('Render UI > with error', () => {
    const wrapper = renderWrapper({
      generalInformation: {
        [storeId]: {
          openEditModal: false
        } as any
      },
      accountDetail: {
        [storeId]: {
          rawData: {
            collectionStatus: {
              nextWorkDate: '10-10-2020'
            }
          },
          secureInfo
        }
      },
      accessToken: {
        [storeId]: {
          accountId: '022009001002624',
          accountInfo: {
            token: '99f0ecae-dbf0-4c6c-b76b-2fbf97927674'
          }
        }
      }
    });

    expect(wrapper.container.innerHTML).toEqual('');
  });

  it('Render UI', () => {
    spyCheckMission([PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_NEXT_WORK_DATE]);
    renderWrapper(initialState);

    expect(
      screen.getByText('txt_edit_general_information')
    ).toBeInTheDocument();
  });

  it('handleCancel with empty secureData', () => {
    renderWrapper({
      ...initialState,
      generalInformation: {
        [storeId]: {
          openEditModal: false
        } as any
      },
      accountDetail: {
        ...initialState.accountDetail,
        [storeId]: {
          rawData: {
            collectionStatus: {
              nextWorkDate: undefined
            }
          }
        }
      }
    });
  });

  it('Render UI > isAllowUpdateNextWorkDate', () => {
    spyCheckMission([PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_NEXT_WORK_DATE]);
    renderWrapper({
      ...initialState,
      accountDetail: {
        ...initialState.accountDetail,
        [storeId]: {
          rawData: {
            collectionStatus: {
              nextWorkDate: ''
            }
          },
          secureInfo
        }
      }
    });

    expect(
      screen.getByText('txt_edit_general_information')
    ).toBeInTheDocument();
  });

  it('Render UI > isAllowUpdateMoveToQueue', () => {
    spyCheckMission([
      PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_MOVE_TO_QUEUE,
      PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_NEXT_WORK_DATE
    ]);
    renderWrapper(initialState);

    expect(
      screen.getByText('txt_edit_general_information')
    ).toBeInTheDocument();
  });

  it('Render UI > isAllowUpdateNextWorkDate & isAllowUpdateMoveToQueue', () => {
    spyCheckMission([
      PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_MOVE_TO_QUEUE,
      PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_NEXT_WORK_DATE
    ]);
    renderWrapper(initialState);

    expect(
      screen.getByText('txt_edit_general_information')
    ).toBeInTheDocument();
  });

  it('Render UI > isAllowUpdateNextWorkDate & isAllowUpdateDelinquencyReason', () => {
    spyCheckMission([
      PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_DELINQUENCY_REASON,
      PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_NEXT_WORK_DATE
    ]);
    renderWrapper(initialState);

    expect(
      screen.getByText('txt_edit_general_information')
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleCancel', () => {
    const mockAction = generalInformationSpy('toggleEditModal');

    renderWrapper({ ...initialState });

    screen.getByText('txt_cancel').click();

    expect(mockAction).toBeCalledWith({ storeId });
  });

  it('handleCancel with empty currentDate', () => {
    const mockAction = generalInformationSpy('toggleEditModal');
    renderWrapper({
      ...initialState,
      accountDetail: {
        ...initialState.accountDetail,
        [storeId]: {
          rawData: {
            collectionStatus: {
              nextWorkDate: ''
            }
          },
          secureInfo
        }
      }
    });

    screen.getByText('txt_cancel').click();

    expect(mockAction).toBeCalledWith({ storeId });
  });

  it('handleSubmit with undefined date', () => {
    spyCheckMission([
      PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_NEXT_WORK_DATE,
      PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_MOVE_TO_QUEUE
    ]);
    createMockMutationObserver([
      {
        target: {
          classList: {
            contains: jest.fn()
          }
        }
      }
    ]);
    const wrapper = renderWrapper({
      ...initialState,
      accountDetail: {
        ...initialState.accountDetail,
        [storeId]: {
          rawData: {
            collectionStatus: {
              nextWorkDate: undefined
            }
          },
          secureInfo: {
            codeToText: {
              components: [
                {
                  component: 'component_reference_list_config',
                  jsonValue: {
                    moveToQueues: [
                      {
                        code: '',
                        description: 'des',
                        active: true
                      },
                      {
                        code: '124',
                        description: 'des',
                        active: true
                      }
                    ],
                    delinquencyReason: [
                      {
                        code: '123',
                        description: 'des',
                        active: true
                      },
                      {
                        code: '124',
                        description: 'des',
                        active: true
                      }
                    ]
                  }
                }
              ]
            }
          }
        }
      }
    });

    const button = wrapper.getByText('txt_save');

    const icon = wrapper.baseElement.querySelectorAll('.icon-chevron-down');
    jest.useFakeTimers();
    userEvent.click(icon[0]);
    jest.runAllTimers();

    wrapper.getByText('- des').click();
    button.click();
  });

  it('handleSubmit', () => {
    spyCheckMission([
      PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_NEXT_WORK_DATE,
      PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_MOVE_TO_QUEUE
    ]);

    createMockMutationObserver([
      {
        target: {
          classList: {
            contains: jest.fn()
          }
        }
      }
    ]);
    const wrapper = renderWrapper(initialState);
    const button = wrapper.getByText('txt_save');

    const icon = wrapper.baseElement.querySelectorAll('.icon-chevron-down');
    jest.useFakeTimers();
    userEvent.click(icon[0]);
    jest.runAllTimers();

    wrapper.getByText('123 - des').click();
    waitFor(() => {
      button.click();
    });
  });
});
