import { apiService } from 'app/utils/api.service';
import { UpdateNextWorkDate, UpdateMoveToQueCode } from './types';

const generalInformationService = {
  updateNextWorkDate: (requestBody: UpdateNextWorkDate) => {
    const url =
      window?.appConfig?.api?.collectionForm?.updateNextWorkDate || '';
    return apiService.post(url, requestBody);
  },
  updateMoveToQueCode: (requestBody: UpdateMoveToQueCode) => {
    const url =
      window?.appConfig?.api?.collectionForm?.updateMoveToQueCode || '';
    return apiService.post(url, requestBody);
  }
};

export default generalInformationService;
