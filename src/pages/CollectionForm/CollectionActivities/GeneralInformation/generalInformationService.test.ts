import services from './generalInformationService';

// utils
import { storeId, mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import { UpdateMoveToQueCode, UpdateNextWorkDate } from './types';

describe('generalInformationService', () => {
  describe('updateNextWorkDate', () => {
    const params: UpdateNextWorkDate = {
      common: { accountId: storeId },
      date: '',
      collectorCode: ''
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      services.updateNextWorkDate(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      services.updateNextWorkDate(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.updateNextWorkDate,
        params
      );
    });
  });
  describe('updateMoveToQueCode', () => {
    const params: UpdateMoveToQueCode = {
      common: { accountId: storeId },
      code: ''
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      services.updateMoveToQueCode(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      services.updateMoveToQueCode(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.updateMoveToQueCode,
        params
      );
    });
  });
});
