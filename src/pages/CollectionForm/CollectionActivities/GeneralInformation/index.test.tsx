import React from 'react';

// redux

// Component
import GeneralInformation from '.';

// Helper
import { renderMockStore, storeId } from 'app/test-utils';

// Hooks
import { AccountDetailProvider } from 'app/hooks';
import { screen } from '@testing-library/react';

jest.mock('./Header', () => () => <div>Header</div>);
jest.mock('./Body', () => () => <div>Body</div>);
jest.mock('./EditModal', () => () => <div>EditModal</div>);

const renderWrapper = () => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <GeneralInformation />
    </AccountDetailProvider>,
    {}
  ).wrapper;
};

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper();
    expect(screen.getByText('Header')).toBeInTheDocument();
    expect(screen.getByText('Body')).toBeInTheDocument();
    expect(screen.getByText('EditModal')).toBeInTheDocument();
  });
});
