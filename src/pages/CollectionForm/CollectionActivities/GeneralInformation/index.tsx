import React from 'react';
import Body from './Body';
import Header from './Header';
import EditModal from './EditModal';

interface GeneralInformationProps {}

const GeneralInformation: React.FC<GeneralInformationProps> = () => {
  return (
    <div className="mt-8 px-24">
      <Header />
      <Body />
      <EditModal />
    </div>
  );
};

export default GeneralInformation;
