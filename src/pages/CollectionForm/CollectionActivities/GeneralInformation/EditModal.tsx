import React, { useEffect, useMemo } from 'react';
import { I18N_COMMON_TEXT, I18N_GENERAL_INFORMATION } from 'app/constants/i18n';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import {
  ComboBox,
  DatePicker,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import DateTime from 'date-and-time';
import { useFormik } from 'formik';
import isEqual from 'lodash.isequal';
// helpers
import isUndefined from 'lodash.isundefined';
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';
import { takeAccessToken } from 'pages/__commons/AccessToken/_redux/selector';
import { batch, useDispatch } from 'react-redux';
import { filterActiveItem, GeneralInfoValidationSchema } from './helpers';
import { generalInformationAction } from './_redux/reducers';
import {
  getConfigReferenceData,
  getEditModalOpen,
  getNextWorkDate,
  getSubmitLoading
} from './_redux/selectors';

const EditModal = () => {
  const dispatch = useDispatch();
  const {
    storeId,
    accEValue,
    memberSequenceIdentifier,
    socialSecurityIdentifier
  } = useAccountDetail();

  const { t } = useTranslation();
  const showModal = useStoreIdSelector<boolean>(getEditModalOpen);
  const loading = useStoreIdSelector<boolean>(getSubmitLoading);

  const { moveToQueues, delinquencyReason } = useStoreIdSelector<any>(
    getConfigReferenceData
  );

  const currentNextWorkDate = useStoreIdSelector<string>(getNextWorkDate);
  const accountToken = useStoreIdSelector<any>(takeAccessToken);

  const {
    errors,
    values,
    setValues,
    handleSubmit,
    handleChange,
    handleBlur,
    touched
  } = useFormik({
    initialValues: {
      nextWorkDate: currentNextWorkDate
        ? new Date(currentNextWorkDate)
        : undefined,
      moveToQueue: '',
      delinquencyReason: ''
    },
    enableReinitialize: true,
    validateOnBlur: true,
    validateOnChange: false,
    validationSchema: GeneralInfoValidationSchema(),
    onSubmit: values => {
      const nextWorkDateValue =
        values.nextWorkDate || new Date(currentNextWorkDate);
      const moveToQueueCode = values?.moveToQueue.split('-')[0].trim();

      const isSameNextWorkDate = isEqual(
        DateTime.format(new Date(nextWorkDateValue), 'MM-DD-YYYY'),
        currentNextWorkDate
      );

      const nextWorkDate = DateTime.format(new Date(nextWorkDateValue), 'MMDD');
      const nextWorkDateForMemoSubmit = DateTime.format(
        new Date(nextWorkDateValue),
        'MM/DD/YYYY'
      );

      const nextWorkDateForCollectSubmit = DateTime.format(
        new Date(nextWorkDateValue),
        'YYYY-MM-DD'
      );

      batch(() => {
        dispatch(
          generalInformationAction.triggerUpdateGeneralInformation({
            storeId,
            postData: {
              accountId: accEValue,
              delinquencyReason: values?.delinquencyReason,
              moveToQueueCode,
              nextWorkDate,
              nextWorkDateForMemoSubmit,
              memberSequenceIdentifier,
              socialSecurityIdentifier
            },
            isSameNextWorkDate,
            accountToken,
            nextWorkDateForCollectSubmit
          })
        );
      });
    }
  });

  const handleCancel = () => {
    setValues({
      nextWorkDate: currentNextWorkDate
        ? new Date(currentNextWorkDate)
        : undefined,
      moveToQueue: '',
      delinquencyReason: ''
    });
    dispatch(generalInformationAction.toggleEditModal({ storeId }));
  };

  useEffect(() => {
    if (!showModal) {
      setValues({
        nextWorkDate: currentNextWorkDate
          ? new Date(currentNextWorkDate)
          : undefined,
        moveToQueue: '',
        delinquencyReason: ''
      });
    }
  }, [showModal, setValues, currentNextWorkDate]);

  const isAllowUpdateNextWorkDate = checkPermission(
    PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_NEXT_WORK_DATE,
    storeId
  );

  const isAllowUpdateMoveToQueue = checkPermission(
    PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_MOVE_TO_QUEUE,
    storeId
  );

  const isAllowUpdateDelinquencyReason = checkPermission(
    PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_DELINQUENCY_REASON,
    storeId
  );

  const isDisableOkButton = useMemo(() => {
    if (
      values.nextWorkDate &&
      isEqual(values?.nextWorkDate, new Date(currentNextWorkDate)) &&
      !values.moveToQueue &&
      !values.delinquencyReason
    ) {
      return true;
    }
    if (values.nextWorkDate || values.moveToQueue || values.delinquencyReason) {
      return false;
    }
    return true;
  }, [
    currentNextWorkDate,
    values.delinquencyReason,
    values.moveToQueue,
    values.nextWorkDate
  ]);

  if (!showModal) return null;

  return (
    <form>
      <Modal
        show={showModal}
        loading={loading}
        dataTestId="generalInformation_editModal"
      >
        <ModalHeader
          border
          closeButton
          onHide={handleCancel}
          dataTestId="generalInformation_editModal-header"
        >
          <ModalTitle dataTestId="generalInformation_editModal-title">
            {t('txt_edit_general_information')}
          </ModalTitle>
        </ModalHeader>
        <ModalBodyWithApiError storeId={storeId} apiErrorClassName="mb-24">
          <div>
            <div className="row">
              {isAllowUpdateNextWorkDate && (
                <div className="col-12 mt-16">
                  <DatePicker
                    name="nextWorkDate"
                    value={values.nextWorkDate}
                    label={t(I18N_GENERAL_INFORMATION.NEXT_WORK_DATE)}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    error={
                      touched.nextWorkDate
                        ? {
                            status: !isUndefined(errors.nextWorkDate),
                            message: t(errors.nextWorkDate)
                          }
                        : undefined
                    }
                  />
                </div>
              )}
              {isAllowUpdateMoveToQueue && (
                <div className="col-12 mt-16">
                  <ComboBox
                    name="moveToQueue"
                    label={t(I18N_GENERAL_INFORMATION.MOVE_TO_QUEUE)}
                    onChange={handleChange}
                    value={values.moveToQueue}
                    onBlur={handleBlur}
                    error={
                      touched.moveToQueue
                        ? {
                            status: !isUndefined(errors.moveToQueue),
                            message: t(errors.moveToQueue)
                          }
                        : undefined
                    }
                    noResult={t('txt_no_results_found')}
                    checkAllLabel={t('txt_all')}
                  >
                    {filterActiveItem(moveToQueues)?.map((item: any) => (
                      <ComboBox.Item
                        key={item.code}
                        label={`${item.code} - ${item.description}`}
                        value={`${item.code} - ${item.description}`}
                      />
                    ))}
                  </ComboBox>
                </div>
              )}
              {isAllowUpdateDelinquencyReason && (
                <div className="col-12 mt-16">
                  <ComboBox
                    name="delinquencyReason"
                    label={t(I18N_GENERAL_INFORMATION.DELINQUENCY_REASON)}
                    value={values.delinquencyReason}
                    onChange={handleChange}
                    noResult={t('txt_no_results_found')}
                    checkAllLabel={t('txt_all')}
                  >
                    {filterActiveItem(delinquencyReason)?.map((item: any) => (
                      <ComboBox.Item
                        key={item.code}
                        label={`${item.code} - ${item.description}`}
                        value={`${item.code} - ${item.description}`}
                      />
                    ))}
                  </ComboBox>
                </div>
              )}
            </div>
          </div>
        </ModalBodyWithApiError>
        <ModalFooter
          dataTestId="generalInformation_editModal-footer"
          okButtonText={t(I18N_COMMON_TEXT.SAVE)}
          cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
          onCancel={handleCancel}
          onOk={handleSubmit}
          disabledOk={isDisableOkButton}
        />
      </Modal>
    </form>
  );
};

export default EditModal;
