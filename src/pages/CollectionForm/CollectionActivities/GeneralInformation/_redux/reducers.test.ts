import { generalInformationAction as actions, reducer } from './reducers';

// utils
import { storeId } from 'app/test-utils';

// types
import { GeneralInformationState } from '../types';

const initialState: GeneralInformationState = {};

describe('CallerAuthenticate > reducer', () => {
  it('toggleEditModal', () => {
    // invoke
    const params = { storeId };
    const state = reducer(initialState, actions.toggleEditModal(params));

    // expect
    expect(state[storeId]).toEqual({ openEditModal: true });
  });
});
