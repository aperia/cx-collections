import { generalInformationAction } from './reducers';
import {
  triggerUpdateGeneralInformation,
  updateNextWorkDateAsync
} from './updateGeneralInformation';

import generalInformationService from '../generalInformationService';

import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { memoActions } from 'pages/Memos/_redux/reducers';

import {
  accEValue,
  byPassFulfilled,
  byPassPending,
  byPassRejected,
  createStoreWithDefaultMiddleWare,
  mockActionCreator,
  responseDefault,
  storeId
} from 'app/test-utils';
import { UpdateGeneralInformationArgs } from '../types';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';
import * as helpers from 'app/entitlements/helpers';
import { rootReducer } from 'storeConfig';

window.appConfig = {
  ...window.appConfig,
  commonConfig: {} as CommonConfig
};

const request: UpdateGeneralInformationArgs = {
  storeId,
  postData: {
    accountId: 'VOL(M1VIW1pkXXQ3UlgpN0QwRnRpQA==)',
    delinquencyReason: 'Natural Disaster',
    memberSequenceIdentifier: '00001',
    moveToQueueCode: '088',
    nextWorkDate: '0729',
    nextWorkDateForMemoSubmit: '07/29/2021',
    socialSecurityIdentifier: ''
  },
  isSameNextWorkDate: false
};

const toastActionMock = mockActionCreator(actionsToast);
const memoActionMock = mockActionCreator(memoActions);
const apiErrorNotificationActionMock = mockActionCreator(
  apiErrorNotificationAction
);
const accountDetailActionsMock = mockActionCreator(accountDetailActions);

beforeEach(() => {
  jest.spyOn(helpers, 'checkPermission').mockImplementation(() => true);
  jest
    .spyOn(generalInformationService, 'updateMoveToQueCode')
    .mockResolvedValue(responseDefault);
  jest
    .spyOn(generalInformationService, 'updateNextWorkDate')
    .mockResolvedValue(responseDefault);
});

describe('test builder', () => {
  const store = createStoreWithDefaultMiddleWare({
    generalInformation: { [storeId]: { edit: {}, suggestionNextWorkDate: '' } }
  });
  const params = {
    storeId,
    postData: {
      accountId: storeId,
      moveToQueueCode: '',
      nextWorkDate: '',
      delinquencyReason: ''
    },
    isSameNextWorkDate: false
  };

  it('error', async () => {
    jest
      .spyOn(generalInformationService, 'updateNextWorkDate')
      .mockRejectedValue({});

    const response = await updateNextWorkDateAsync(params)(
      store.dispatch,
      store.getState,
      undefined
    );

    expect(response.payload).toEqual({ response: {} });
  });

  it('should be do nothing', async () => {
    jest
      .spyOn(generalInformationService, 'updateNextWorkDate')
      .mockRejectedValue({});

    const response = await updateNextWorkDateAsync({
      ...params,
      isSameNextWorkDate: true
    })(store.dispatch, store.getState, undefined);

    expect(response.payload).toEqual(undefined);
  });

  it('Should have pending', async () => {
    const action = updateNextWorkDateAsync.pending('', params);
    const state = rootReducer(store.getState(), action).generalInformation;
    expect(state[storeId].edit!.isSubmitting).toEqual(true);
  });

  it('Should have fulfilled', async () => {
    const action = updateNextWorkDateAsync.fulfilled({}, '', params);
    const state = rootReducer(store.getState(), action).generalInformation;
    expect(state[storeId].edit!.isSubmitting).toEqual(false);
  });

  it('Should have reject', async () => {
    const action = updateNextWorkDateAsync.rejected(null, '', params);
    const state = rootReducer(store.getState(), action).generalInformation;
    expect(state[storeId].edit!.isSubmitting).toEqual(false);
  });
});

describe('triggerUpdateGeneralInformation', () => {
  it('isFulfilled with isNWDSuccess = true, isMTQSuccess = false', async () => {
    const mockUpdateApiError = apiErrorNotificationActionMock('updateApiError');
    const store = createStoreWithDefaultMiddleWare({});

    await triggerUpdateGeneralInformation({
      storeId,
      isSameNextWorkDate: false,
      accountToken: '123',
      nextWorkDateForCollectSubmit: '123',
      postData: {
        accountId: storeId,
        nextWorkDate: '123',
        moveToQueueCode: '',
        delinquencyReason: '123',
        memberSequenceIdentifier: 'inf',
        socialSecurityIdentifier: 'sci',
      }
    })(
      store.dispatch,
      store.getState,
      {}
    )
    expect(mockUpdateApiError).not.toBeCalled();
  })
  it('success case', async () => {
    const postMemo = memoActionMock('postMemo');
    const addToast = toastActionMock('addToast');
    const getAccountDetail = accountDetailActionsMock('getAccountDetail');

    const store = createStoreWithDefaultMiddleWare({});
    await generalInformationAction.triggerUpdateGeneralInformation(request)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(postMemo).toHaveBeenCalledTimes(3);
    expect(addToast).toHaveBeenCalled();
    expect(getAccountDetail).toHaveBeenCalled();
  });

  it('success case isSameNextWorkDate: true', async () => {
    const postMemo = memoActionMock('postMemo');
    const addToast = toastActionMock('addToast');
    const getAccountDetail = accountDetailActionsMock('getAccountDetail');

    const store = createStoreWithDefaultMiddleWare({});
    await generalInformationAction.triggerUpdateGeneralInformation({
      ...request,
      isSameNextWorkDate: true
    })(store.dispatch, store.getState, {});
    expect(postMemo).toHaveBeenCalledTimes(2);
    expect(addToast).toHaveBeenCalled();
    expect(getAccountDetail).not.toHaveBeenCalled();
  });

  it('reject update Next WorkDate', async () => {
    const postMemo = memoActionMock('postMemo');
    const addToast = toastActionMock('addToast');
    const getAccountDetail = accountDetailActionsMock('getAccountDetail');

    const store = createStoreWithDefaultMiddleWare({});
    await generalInformationAction.triggerUpdateGeneralInformation({
      ...request,
      isSameNextWorkDate: false
    })(
      byPassRejected(updateNextWorkDateAsync.rejected.type),
      store.getState,
      {}
    );
    expect(postMemo).toHaveBeenCalledTimes(1);
    expect(addToast).toHaveBeenCalled();
    expect(getAccountDetail).toHaveBeenCalled();
  });

  it('fulfilled update Next WorkDate', async () => {
    const postMemo = memoActionMock('postMemo');
    const addToast = toastActionMock('addToast');
    const getAccountDetail = accountDetailActionsMock('getAccountDetail');
    const store = createStoreWithDefaultMiddleWare({});
    await generalInformationAction.triggerUpdateGeneralInformation({
      ...request,
      isSameNextWorkDate: false
    })(
      byPassFulfilled(updateNextWorkDateAsync.fulfilled.type),
      store.getState,
      {}
    );
    expect(postMemo).toHaveBeenCalledTimes(1);
    expect(addToast).toHaveBeenCalled();
    expect(getAccountDetail).toHaveBeenCalled();
  });

  it('pending update Next WorkDate', async () => {
    const postMemo = memoActionMock('postMemo');
    const addToast = toastActionMock('addToast');
    const store = createStoreWithDefaultMiddleWare({});
    await generalInformationAction.triggerUpdateGeneralInformation({
      ...request,
      isSameNextWorkDate: false
    })(byPassPending(updateNextWorkDateAsync.pending.type), store.getState, {});
    expect(postMemo).toHaveBeenCalledTimes(1);
    expect(addToast).toHaveBeenCalled();
  });

  it('failed case', async () => {
    jest.spyOn(helpers, 'checkPermission').mockImplementation(() => false);
    jest
      .spyOn(generalInformationService, 'updateNextWorkDate')
      .mockRejectedValue({});

    const addToast = toastActionMock('addToast');
    const updateApiError = apiErrorNotificationActionMock('updateApiError');
    const store = createStoreWithDefaultMiddleWare({});

    await generalInformationAction.triggerUpdateGeneralInformation(request)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(addToast).not.toHaveBeenCalled();
    expect(updateApiError).toHaveBeenCalled();
  });
});

describe('test triggerUpdateGeneralInformation', () => {
  const mockArgs: UpdateGeneralInformationArgs = {
    storeId,
    postData: {
      accountId: accEValue,
      nextWorkDateForMemoSubmit: '',
      nextWorkDate: '',
      moveToQueueCode: ''
    },
    isSameNextWorkDate: true
  };

  it('success', async () => {
    const mockUpdateApiError = apiErrorNotificationActionMock('updateApiError');

    const store = createStoreWithDefaultMiddleWare({});
    await triggerUpdateGeneralInformation(mockArgs)(
      () => null,
      store.getState,
      {} as any
    );

    expect(mockUpdateApiError).toBeCalled();
  });

  it('isFulfilled', async () => {
    const mockUpdateApiError = apiErrorNotificationActionMock('updateApiError');

    const store = createStoreWithDefaultMiddleWare({});
    await triggerUpdateGeneralInformation(mockArgs)(
      byPassFulfilled(triggerUpdateGeneralInformation.rejected.type),
      store.getState,
      {} as any
    );

    expect(mockUpdateApiError).toBeCalled();
  });
});
