import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { GeneralInformationState } from '../types';

import {
  triggerUpdateGeneralInformation,
  updateGeneralInformationBuilder
} from './updateGeneralInformation';

const { actions, reducer } = createSlice({
  name: 'generalInformation',
  initialState: {} as GeneralInformationState,
  reducers: {
    toggleEditModal: (
      draftState,
      action: PayloadAction<{ storeId: string }>
    ) => {
      const { storeId } = action.payload;
      const currentStatus = draftState[storeId]?.openEditModal;

      draftState[storeId] = {
        ...draftState[storeId],
        openEditModal: !currentStatus
      };
    }
  },
  extraReducers: builders => {
    updateGeneralInformationBuilder(builders);
  }
});

const generalInformationAction = {
  ...actions,
  triggerUpdateGeneralInformation
};

export { generalInformationAction, reducer };
