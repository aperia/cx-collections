import { batch } from 'react-redux';
import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected,
  PayloadAction
} from '@reduxjs/toolkit';
import generalInformationService from '../generalInformationService';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';
import { generalInformationAction } from './reducers';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { accessTokenActions } from 'pages/__commons/AccessToken/_redux/reducer';

// constants
import {
  UpdateGeneralInformationArgs,
  UpdateMoveToQueCode,
  UpdateNextWorkDate,
  GeneralInformationState,
  UpdateMoveToQueArgs
} from '../types';
import { GENERAL_INFORMATION } from 'pages/Memos/constants';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';
import { CHRONICLE_ACCOUNT_DETAILS_KEY } from 'pages/ClientConfiguration/Memos/constants';

interface triggerUpdateGeneralInformationPayload {
  nextWorkDate: string;
  moveToQueueCode: string;
  delinquencyReason?: string;
}

// CXC-4323 Integrate for General Information
export const updateMoveToQueueAsync = createAsyncThunk<
  void,
  UpdateMoveToQueArgs,
  ThunkAPIConfig
>('generalInformation/updateMoveToQueueAsync', async (args, thunkAPI) => {
  const { accountId, moveToQueueCode, storeId } = args;
  const { dispatch, getState } = thunkAPI;
  const { accountDetail } = getState();
  const requestMoveToQue: UpdateMoveToQueCode = {
    common: {
      accountId
    },
    code: moveToQueueCode
  };
  await generalInformationService.updateMoveToQueCode(requestMoveToQue);
  dispatch(
    memoActions.postMemo(GENERAL_INFORMATION.UPDATE_MOVE_TO_QUEUE, {
      storeId,
      accEValue: accountId,
      chronicleKey: CHRONICLE_ACCOUNT_DETAILS_KEY,
      generalInformation: {
        cardholderContactedNameAuth:
          accountDetail[storeId]?.selectedAuthenticationData?.customerName,
        moveToQueueCode
      }
    })
  );
});

// CXC-4323 Integrate for General Information
export const updateNextWorkDateAsync = createAsyncThunk<
  any,
  UpdateGeneralInformationArgs,
  ThunkAPIConfig
>(
  'generalInformation/updateNextWorkDate',
  async (args, { dispatch, rejectWithValue, getState }) => {
    const {
      storeId,
      isSameNextWorkDate,
      postData: { accountId, nextWorkDate, nextWorkDateForMemoSubmit }
    } = args;
    if (isSameNextWorkDate) return;
    const { operatorCode = '' } = window.appConfig.commonConfig;
    const { accountDetail } = getState();
    const requestNextWorkDate: UpdateNextWorkDate = {
      common: {
        accountId
      },
      collectorCode: operatorCode,
      date: nextWorkDate
    };

    // there will be three api, each update each field separately.
    try {
      await generalInformationService.updateNextWorkDate(requestNextWorkDate);
      dispatch(
        memoActions.postMemo(GENERAL_INFORMATION.UPDATE_NEXT_WORK_DATE, {
          storeId,
          accEValue: accountId,
          chronicleKey: CHRONICLE_ACCOUNT_DETAILS_KEY,
          generalInformation: {
            cardholderContactedNameAuth:
              accountDetail[storeId]?.selectedAuthenticationData?.customerName,
            nextWorkDate: nextWorkDateForMemoSubmit
          }
        })
      );
    } catch (error) {
      return rejectWithValue({ response: error });
    }
  }
);

export const triggerUpdateGeneralInformation = createAsyncThunk<
  triggerUpdateGeneralInformationPayload,
  UpdateGeneralInformationArgs,
  ThunkAPIConfig
>(
  'generalInformation/triggerUpdateGeneralInformation',
  async (args, { dispatch, getState }) => {
    const {
      storeId,
      isSameNextWorkDate,
      accountToken,
      nextWorkDateForCollectSubmit,
      postData: {
        accountId,
        nextWorkDate,
        memberSequenceIdentifier,
        socialSecurityIdentifier,
        moveToQueueCode,
        delinquencyReason
      }
    } = args;

    const { operatorID } = window.appConfig.commonConfig;

    const successToasts: string[] = [];
    const failToasts: string[] = [];
    const { accountDetail } = getState();
    let errorNWDResponse: PayloadAction<any> | undefined;
    let errorMTQResponse: PayloadAction<any> | undefined;

    let isNWDSuccess = false;
    let isMTQSuccess = false;
    let isDRSuccess = false;

    const isAllowUpdateNextWorkDate = checkPermission(
      PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_NEXT_WORK_DATE,
      storeId
    );

    const isAllowUpdateMoveToQueue = checkPermission(
      PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_MOVE_TO_QUEUE,
      storeId
    );

    const isAllowUpdateDelinquencyReason = checkPermission(
      PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_DELINQUENCY_REASON,
      storeId
    );

    if (!isSameNextWorkDate && isAllowUpdateNextWorkDate) {
      const updateNextWorkDateResponse = await dispatch(
        updateNextWorkDateAsync(args)
      );

      if (isRejected(updateNextWorkDateResponse)) {
        failToasts.push('txt_update_next_work_date_failed');
        errorNWDResponse = updateNextWorkDateResponse;
      } else if (isFulfilled(updateNextWorkDateResponse)) {
        isNWDSuccess = true;
        successToasts.push('txt_update_next_work_date_successfully');
      }
    }

    if (moveToQueueCode?.length && isAllowUpdateMoveToQueue) {
      const updateMoveToQueueResponse = await dispatch(
        updateMoveToQueueAsync({ accountId, moveToQueueCode, storeId })
      );

      if (isRejected(updateMoveToQueueResponse)) {
        failToasts.push('txt_update_move_to_queue_failed');
        errorMTQResponse = updateMoveToQueueResponse;
      } else if (isFulfilled(updateMoveToQueueResponse)) {
        isMTQSuccess = true;
        successToasts.push('txt_update_move_to_queue_successfully');
      }
    }

    if (isNWDSuccess || isMTQSuccess) {
      const reqBody = {
        accountToken,
        agentId: operatorID,
        nextWorkDate: isNWDSuccess ? nextWorkDateForCollectSubmit : undefined,
        permCollCd: isMTQSuccess ? moveToQueueCode : undefined
      };
      dispatch(accessTokenActions.updateCollectionAccount(reqBody));
    }

    if (delinquencyReason?.length && isAllowUpdateDelinquencyReason) {
      dispatch(
        memoActions.postMemo(GENERAL_INFORMATION.UPDATE_DELINQUENCY_REASON, {
          storeId,
          accEValue: accountId,
          chronicleKey: CHRONICLE_ACCOUNT_DETAILS_KEY,
          generalInformation: {
            cardholderContactedNameAuth:
              accountDetail[storeId]?.selectedAuthenticationData?.customerName,
            delinquencyReason
          }
        })
      );
      isDRSuccess = true;
      successToasts.push('txt_update_delinquency_reason_successfully');
    }

    batch(() => {
      successToasts.forEach(toast => {
        dispatch(
          actionsToast.addToast({
            message: toast,
            type: 'success'
          })
        );
      });
      failToasts.forEach(toast => {
        dispatch(
          actionsToast.addToast({
            message: toast,
            type: 'error'
          })
        );
      });
    });

    if (isNWDSuccess || isMTQSuccess || isDRSuccess) {
      batch(() => {
        dispatch(generalInformationAction.toggleEditModal({ storeId }));
        !isSameNextWorkDate &&
          dispatch(
            accountDetailActions.getAccountDetail({
              storeId,
              postData: {
                common: {
                  accountId
                },
                inquiryFields: {
                  memberSequenceIdentifier
                }
              },
              socialSecurityIdentifier,
              isReload: true
            })
          );
        dispatch(
          apiErrorNotificationAction.clearApiErrorData({
            storeId,
            forSection: 'inModalBody'
          })
        );
      });
    } else {
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inModalBody',
          apiResponse:
            errorNWDResponse?.payload?.response ||
            errorMTQResponse?.payload?.response
        })
      );
    }

    return { moveToQueueCode, delinquencyReason, nextWorkDate };
  }
);

export const updateGeneralInformationBuilder = (
  builder: ActionReducerMapBuilder<GeneralInformationState>
) => {
  builder
    .addCase(updateNextWorkDateAsync.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        edit: { isSubmitting: true }
      };
    })
    .addCase(updateNextWorkDateAsync.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        edit: { isSubmitting: false }
      };
    })
    .addCase(
      triggerUpdateGeneralInformation.fulfilled,
      (draftState, action) => {
        const { storeId } = action.meta.arg;
        draftState[storeId] = {
          ...draftState[storeId],
          data: action.payload
        };
      }
    )
    .addCase(updateNextWorkDateAsync.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        edit: { isSubmitting: false }
      };
    });
};
