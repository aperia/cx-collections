import * as selector from './selectors';

// utils
import { selectorWrapper, storeId } from 'app/test-utils';

const states: Partial<RootState> = {
  generalInformation: {
    [storeId]: {
      openEditModal: true,
      data: {
        nextWorkDate: '',
        moveToQueueCode: 'moveToQueue',
        delinquencyReason: 'delinquencyReason'
      },
      edit: { isSubmitting: true }
    } as any
  },
  accountDetail: {
    [storeId]: {
      rawData: {
        collectionStatus: {
          nextWorkDate: '10-10-2020'
        }
      }
    }
  }
};

const selectorTrigger = selectorWrapper(states);

describe('GeneralInformation > selectors', () => {
  it('getEditModalOpen', () => {
    const { data, emptyData } = selectorTrigger(selector.getEditModalOpen);

    expect(data).toEqual(states.generalInformation![storeId].openEditModal);
    expect(emptyData).toBeUndefined();
  });

  it('getSubmitLoading', () => {
    const { data, emptyData } = selectorTrigger(selector.getSubmitLoading);

    expect(data).toEqual(
      states.generalInformation![storeId].edit!.isSubmitting
    );
    expect(emptyData).toBeFalsy();
  });

  it('getNextWorkDate ', () => {
    const output = selector.getNextWorkDate(states as RootState, storeId);

    expect(output).toEqual(
      states.accountDetail![storeId]?.rawData?.collectionStatus?.nextWorkDate
    );
  });
  it('getGeneraInformationData ', () => {
    const output = selector.getGeneraInformationData(
      states as RootState,
      storeId
    );

    expect(output).toEqual(states.generalInformation![storeId].data);
  });

  it('getGeneralInformation', () => {
    const { data, emptyData } = selectorTrigger(
      selector.getGeneralInformation
    );

    expect(data).toEqual(states.generalInformation![storeId]);
    expect(emptyData).toEqual({});
  });

  it('getEditModalOpen', () => {
    const { data, emptyData } = selectorTrigger(selector.getEditModalOpen);

    expect(data).toEqual(states.generalInformation![storeId].openEditModal);
    expect(emptyData).toBeUndefined();
  });
});
