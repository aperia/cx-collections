import { createSelector } from '@reduxjs/toolkit';
import { GeneralInformationStateByStoreId } from '../types';

export const getGeneralInformation: CustomStoreIdSelector<GeneralInformationStateByStoreId> =
  (state, storeId) => state.generalInformation[storeId] || {};

export const getRawAccountDetail = (state: RootState, storeId: string) =>
  state.accountDetail[storeId]?.rawData || {};

export const getSecureInfo = (state: RootState, storeId: string) =>
  state.accountDetail[storeId]?.secureInfo || [];

export const getEditModalOpen = createSelector(
  getGeneralInformation,
  (generalInfo: GeneralInformationStateByStoreId) => generalInfo?.openEditModal
);

export const getGeneraInformationData = createSelector(
  getGeneralInformation,
  (generalInfo: GeneralInformationStateByStoreId) => generalInfo?.data
);

export const getSubmitLoading = createSelector(
  getGeneralInformation,
  (generalInfo: GeneralInformationStateByStoreId) =>
    generalInfo?.edit?.isSubmitting || false
);

export const getNextWorkDate = createSelector(
  getRawAccountDetail,
  (rawAccountDetail: MagicKeyValue) =>
    rawAccountDetail?.collectionStatus?.nextWorkDate
);

export const getConfigReferenceData = createSelector(
  getSecureInfo,
  (data: MagicKeyValue) =>
    data?.codeToText?.components?.filter(
      (item: any) => item.component === 'component_reference_list_config'
    )[0]?.jsonValue || {
      moveToQueues: [],
      delinquencyReason: [],
      deceasedContact: []
    }
);
