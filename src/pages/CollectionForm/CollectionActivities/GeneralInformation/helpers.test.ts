import * as helpers from './helpers';

describe('Test helpers General Information', () => {
  it('filterActiveItem', () => {
    const result = helpers.filterActiveItem(undefined as never);
    expect(result).toEqual([]);
  });
});
