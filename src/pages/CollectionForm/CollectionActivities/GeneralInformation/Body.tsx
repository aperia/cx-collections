import React from 'react';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { View } from 'app/_libraries/_dof/core';

// selector
import { getNextWorkDate } from './_redux/selectors';

//types

interface BodyProps {}
const Body: React.FC<BodyProps> = () => {
  const { storeId } = useAccountDetail();

  const nextWorkDate = useStoreIdSelector<string>(getNextWorkDate);

  return (
    <View
      descriptor="generalInformation"
      formKey={`${storeId}__generalInformation`}
      value={{
        nextWorkDate: nextWorkDate
      }}
    />
  );
};

export default Body;
