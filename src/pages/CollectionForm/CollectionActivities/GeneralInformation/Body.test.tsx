import React from 'react';

// Component
import Body from './Body';

// Helper
import { renderMockStore, storeId } from 'app/test-utils';

// Hooks
import { AccountDetailProvider } from 'app/hooks';
import { screen } from '@testing-library/react';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const renderWrapper = () => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <Body />
    </AccountDetailProvider>,
    {}
  ).wrapper;
};

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper();

    expect(screen.getByTestId('generalInformation')).toBeInTheDocument();
  });
});
