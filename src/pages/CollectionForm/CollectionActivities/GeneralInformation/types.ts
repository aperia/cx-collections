// state declare
export interface GeneralInformationStateByStoreId {
  loading?: boolean;
  error?: boolean;
  data?: GeneralInformationData;
  openEditModal?: boolean;
  edit?: {
    isSubmitting?: boolean;
    error?: boolean;
  };
  suggestionNextWorkDate: string;
}

export interface GeneralInformationState {
  [storeId: string]: GeneralInformationStateByStoreId;
}

export interface GeneralInformationData {
  nextWorkDate: string;
  moveToQueueCode: string;
  delinquencyReason?: string;
}

export interface GeneralInformationPayload {
  data: GeneralInformationData;
}

export interface GeneralInformationArgs extends ArgsCommon {}

export interface UpdateGeneralInformationArgs {
  storeId: string;
  isSameNextWorkDate: boolean;
  accountToken: string,
  nextWorkDateForCollectSubmit: string,
  postData: GeneralInformationData & {
    memberSequenceIdentifier?: string;
    socialSecurityIdentifier?: string;
    accountId: string;
    nextWorkDateForMemoSubmit?: string;
  };
}

export interface UpdateMoveToQueArgs {
  accountId: string;
  moveToQueueCode: string;
  storeId: string;
}

export interface GetGeneralInformationRequest
  extends CommonAccountIdRequestBody {}

// -------UPDATE GENERAL INFORMATION ---------
export interface UpdateNextWorkDate {
  common: {
    accountId: string;
  };
  collectorCode: string;
  date: string;
}

export interface UpdateMoveToQueCode {
  common: {
    accountId: string;
  };
  code: string;
}
