import * as Yup from 'yup';

export const GeneralInfoValidationSchema = () => {
  return Yup.object().shape({
    nextWorkDate: Yup.string().trim(),
    moveToQueue: Yup.string().trim()
  });
};

export const filterActiveItem = (referenceData: any[]) =>
  referenceData
    ? referenceData.filter((item: any) => item.active === true)
    : [];
