import React from 'react';

// Components
import { Tooltip, Button, Icon } from 'app/_libraries/_dls/components';

// Hooks
import { useDispatch } from 'react-redux';
import { useStoreIdSelector, useAccountDetail } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Selectors
import { selectCallResult, selectIsHavingData } from '../../_redux/selectors';

// i18n/constant
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { CALL_RESULT_CODE, COLLECTION_METHOD } from '../../constants';
import { isRemoveEditCollectionForm } from '../../helpers';
import { collectionFormActions } from '../../_redux/reducers';
import { genAmtId } from 'app/_libraries/_dls/utils';

const LastFollowUpHeader: React.FC = () => {
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();

  const isHavingData = useStoreIdSelector<boolean>(selectIsHavingData);
  const callResultValue =
    useStoreIdSelector<CALL_RESULT_CODE>(selectCallResult);

  const handleEditButton = () => {
    dispatch(collectionFormActions.triggerEditCollectionForm({ storeId }));
    dispatch(
      collectionFormActions.changeMethod({
        storeId,
        method: COLLECTION_METHOD.EDIT
      })
    );
  };

  const isShowEditCollectionForm =
    !isRemoveEditCollectionForm(callResultValue) &&
    callResultValue !== CALL_RESULT_CODE.PROMISE_TO_PAY;

  if (!isHavingData) return null;

  return (
    <div className="px-24">
      <hr />
      <div
        className="d-flex justify-content-between align-items-center"
        data-testid={genAmtId(
          'callResult_lastFollowUpdate',
          'editInformation',
          ''
        )}
      >
        {isShowEditCollectionForm && (
          <Tooltip
            element={t(I18N_COLLECTION_FORM.EDIT_INFORMATION)}
            placement="top"
            variant="primary"
            dataTestId="callResult_lastFollowUpdate_edit_tooltip"
          >
            <Button
              variant="icon-secondary"
              onClick={handleEditButton}
              dataTestId="callResult_lastFollowUpdate_edit-btn"
            >
              <Icon
                name="edit"
                dataTestId="callResult_lastFollowUpdate_edit_icon"
              />
            </Button>
          </Tooltip>
        )}
      </div>
    </div>
  );
};

export default LastFollowUpHeader;
