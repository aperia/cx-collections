import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import React from 'react';
import { screen } from '@testing-library/react';
import LastFollowUpHeader from './index';
import { CALL_RESULT_CODE, COLLECTION_METHOD } from '../../constants';
import userEvent from '@testing-library/user-event';
import { collectionFormActions } from '../../_redux/reducers';

const mockCollectionFormActions = mockActionCreator(collectionFormActions);

describe('LastFollowUpHeader component', () => {
  it('should not render component', () => {
    renderMockStoreId(<LastFollowUpHeader />);
    expect(screen.queryByText('txt_last_follow_up')).not.toBeInTheDocument();
  });

  it('should render component', () => {
    const initialState = {
      collectionForm: {
        [storeId]: {
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.DECEASE
          }
        }
      }
    };

    const triggerEditCollectionFormSpy = mockCollectionFormActions(
      'triggerEditCollectionForm'
    );
    const changeMethodSpy = mockCollectionFormActions('changeMethod');

    renderMockStoreId(<LastFollowUpHeader />, { initialState });

    const button = screen.getByRole('button');
    userEvent.click(button);

    expect(triggerEditCollectionFormSpy).toBeCalledWith({ storeId });
    expect(changeMethodSpy).toBeCalledWith({
      storeId,
      method: COLLECTION_METHOD.EDIT
    });
  });
});
