import React from 'react';

// utils
import classNames from 'classnames';

// components
import Memo from 'pages/Memos';
import ToggleButton from 'app/components/ToggleButton';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helper
import { useStoreIdSelector, useAccountDetail } from 'app/hooks';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// redux
import { useDispatch } from 'react-redux';
import { collectionFormActions } from '../_redux/reducers';
import { selectCollapseMemoStatus } from '../_redux/selectors';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

const MemosCollectionForm: React.FC = () => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();

  const collapsed = useStoreIdSelector<boolean>(selectCollapseMemoStatus);

  const handleCollapseMemo = () => {
    dispatch(collectionFormActions.toggleMemoSection({ storeId }));
  };

  return checkPermission(PERMISSIONS.VIEW_MEMO, storeId) ? (
    <div className={classNames('collection-memos', { collapsed })}>
      <ToggleButton
        collapseMessage={t(I18N_COMMON_TEXT.COLLAPSE)}
        expandMessage={t(I18N_COMMON_TEXT.EXPAND)}
        onToggleCollapsed={handleCollapseMemo}
        collapsed={collapsed}
        dataTestId="toggleMemo"
      />
      <Memo
        shouldRenderPinnedIcon={false}
        className={classNames({ collapsed: 'invisible' })}
      />
    </div>
  ) : null;
};

export default MemosCollectionForm;
