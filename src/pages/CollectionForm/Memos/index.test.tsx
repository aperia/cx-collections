import React from 'react';
import { storeId, mockActionCreator, renderMockStoreId } from 'app/test-utils';
import Memos from '.';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import { collectionFormActions } from '../_redux/reducers';
import * as permission from 'app/entitlements/helpers';

const initialState: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      isMemoSectionCollapsed: true
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<Memos />, {
    initialState
  });
};

const collectionFormActionsSpy = mockActionCreator(collectionFormActions);

describe('Memos', () => {
  it('handleCollapseMemo', () => {
    jest.spyOn(permission, 'checkPermission').mockImplementation(() => true);
    const spy = collectionFormActionsSpy('toggleMemoSection');
    const { wrapper } = renderWrapper(initialState);
    const button = queryByClass(wrapper.container, 'collapse-button')!;
    button.click();
    expect(spy).toBeCalledWith({
      storeId
    });
  });
  it('has no Permission', () => {
    const { wrapper } = renderWrapper(initialState);
    expect(wrapper.container).toBeEmptyDOMElement();
  });
});
