import React, {
  Fragment,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef
} from 'react';

// redux
import { batch, useDispatch } from 'react-redux';

// components
import { SimpleBar, HorizontalTabs } from 'app/_libraries/_dls/components';
import CollectionFormHeader from './CollectionFormHeader';
import CollectionFormActivities from './CollectionActivities';
import TalkingPoints from './TalkingPoints';
import Last10Actions from './Last25Actions';

import PromiseToPay from 'pages/PromiseToPay/PromiseToPayForm';
import HardShipForm from 'pages/HardShip/Form';
import LongTermMedical from 'pages/LongTermMedical/Form';
import Incarceration from 'pages/Incarceration/Form';
import Settlement from 'pages/Settlement/Form';

// Hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

import { checkPermission } from 'app/entitlements';
// Const
import { CALL_RESULT_CODE } from './constants';
import { PERMISSIONS } from 'app/entitlements/constants';

// redux
import {
  selectCallResult,
  selectIsLoadingCallResultRefData,
  selectLoadingForm,
  selectTalkingPointLoading,
  takeCurrentView
} from './_redux/selectors';
import { collectionFormActions } from './_redux/reducers';
import { genAmtId } from 'app/_libraries/_dls/utils';
import UploadFilesModal from './CollectionActivities/UploadFilesModal';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { useApiErrorNotification } from 'pages/ApiErrorNotification/hooks/useApiErrorNotification';
import { promiseToPayActions } from 'pages/PromiseToPay/_redux/reducers';
import { settlementActions } from 'pages/Settlement/_redux/reducers';
import { clientConfigPromiseToPayActions } from 'pages/ClientConfiguration/PromiseToPay/_redux/reducers';

const CollectionFormResults: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId, accEValue } = useAccountDetail();

  const isCallActivitiesLoading = useStoreIdSelector<boolean>(
    selectIsLoadingCallResultRefData
  );
  const isTalkingPointLoading = useStoreIdSelector<boolean>(
    selectTalkingPointLoading
  );
  const currentView = useStoreIdSelector<string>(takeCurrentView);
  const ApiErrorDetail = useApiErrorNotification({
    storeId,
    forSection: 'inCollectionFormFlyOut'
  });
  const loadingForm = useStoreIdSelector<boolean>(selectLoadingForm);
  const divRef = useRef<HTMLDivElement | null>(null);

  const loading =
    isCallActivitiesLoading || isTalkingPointLoading || loadingForm;

  const latestAction = useStoreIdSelector<string>(selectCallResult);

  useEffect(() => {
    dispatch(
      promiseToPayActions.getPromiseToPayData({
        storeId
      })
    );
    dispatch(
      collectionFormActions.getLatestActionRequest({
        storeId
      })
    );
  }, [storeId, dispatch]);

  useEffect(() => {
    if (latestAction === CALL_RESULT_CODE.SETTLEMENT) {
      batch(() => {
        dispatch(
          settlementActions.getSettlementInformation({
            storeId,
            postData: { accountId: accEValue }
          })
        );
        dispatch(
          settlementActions.getSettlement({
            storeId,
            postData: { accountId: accEValue }
          })
        );
      });
    }
    dispatch(clientConfigPromiseToPayActions.triggerGetPromiseToPayConfig({storeId}));
  }, [dispatch, latestAction, storeId, accEValue]);

  useLayoutEffect(() => {
    if (!divRef.current) return;
    divRef.current.style.height = loading ? `calc(100vh - 188px)` : 'auto';
  }, [loading]);

  useEffect(() => {
    // clear API Error when currentView change
    dispatch(
      apiErrorNotificationAction.clearApiErrorData({
        storeId,
        forSection: 'inCollectionFormFlyOut'
      })
    );
  }, [currentView, dispatch, storeId]);

  const renderView = useMemo(() => {
    if (currentView === CALL_RESULT_CODE.PROMISE_TO_PAY) {
      return <PromiseToPay />;
    }

    if (currentView === CALL_RESULT_CODE.HARDSHIP) {
      return <HardShipForm />;
    }

    if (currentView === CALL_RESULT_CODE.LONG_TERM_MEDICAL) {
      return <LongTermMedical />;
    }

    if (currentView === CALL_RESULT_CODE.INCARCERATION) {
      return <Incarceration />;
    }

    if (currentView === CALL_RESULT_CODE.SETTLEMENT) {
      return <Settlement />;
    }

    const showTalkingPointsView = checkPermission(
      PERMISSIONS.TALKING_POINTS_VIEW,
      storeId
    );

    return (
      <Fragment>
        <div
          ref={divRef}
          data-testid={genAmtId('collectionFormResults', 'container', '')}
        >
          <ApiErrorDetail
            dataTestId="collectionFormResults-error"
            className="ml-24 mb-24"
          />
          {showTalkingPointsView && (
            <HorizontalTabs
              mountOnEnter
              unmountOnExit={true}
              level2
              dataTestId="collectionFormResults"
            >
              <HorizontalTabs.Tab
                key="01"
                eventKey="01"
                title={t('txt_collection_activities')}
              >
                <CollectionFormActivities />
              </HorizontalTabs.Tab>
              <HorizontalTabs.Tab
                key="02"
                eventKey="02"
                title={t('txt_talking_points')}
              >
                <TalkingPoints />
              </HorizontalTabs.Tab>
            </HorizontalTabs>
          )}
          {!showTalkingPointsView && (
            <div className="border-top">
              <CollectionFormActivities />
            </div>
          )}
        </div>
      </Fragment>
    );
  }, [ApiErrorDetail, currentView, t, storeId]);

  return (
    <>
      <div className="collection-form-container-parent">
        <div
          className={`collection-form-container ${loading && 'loading'}`}
          data-testid={genAmtId(
            'collectionFormResults',
            'container-parent',
            ''
          )}
        >
          <SimpleBar>
            <CollectionFormHeader />
            {renderView}
          </SimpleBar>
        </div>
      </div>
      <Last10Actions />
      <UploadFilesModal />
    </>
  );
};

export default CollectionFormResults;
