import { collectionFormActions } from './reducers';
import { collectionFormService } from '../collectionFormServices';
import {
  createStoreWithDefaultMiddleWare,
  mockActionCreator,
  responseDefault,
  storeId
} from 'app/test-utils';
import { isFulfilled, isRejected } from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

const mockToastActions = mockActionCreator(actionsToast);

describe('downloadCaseDocumentFile', () => {
  it('success api', async () => {
    jest
      .spyOn(collectionFormService, 'downloadCaseDocument')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          responseStatus: 'success',
          file: 'thisisafile',
          fileName: 'fileName'
        }
      });

    const store = createStoreWithDefaultMiddleWare({
      accountDetail: {
        [storeId]: {
          rawData: {
            clientIdentifier: 'clientIdentifier',
            systemIdentifier: 'systemIdentifier',
            principalIdentifier: 'principalIdentifier',
            agentIdentifier: 'agentIdentifier'
          }
        }
      }
    });

    const requestBody = {
      storeId,
      postData: {
        accountId: storeId,
        fileName: 'fileName',
        uuid: '123-45-66'
      }
    };

    const response = await collectionFormActions.downloadCaseDocumentFile(
      requestBody
    )(store.dispatch, store.getState, {});

    expect(isFulfilled(response)).toBeTruthy();
  });

  it('failed API', async () => {
    const mockToastAdd = mockToastActions('addToast');
    jest
      .spyOn(collectionFormService, 'downloadCaseDocument')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          responseStatus: 'failed',
          file: 'thisisafile',
          fileName: 'fileName'
        }
      });

    const store = createStoreWithDefaultMiddleWare({
      accountDetail: {}
    });

    const requestBody = {
      storeId,
      postData: {
        accountId: storeId,
        fileName: 'fileName',
        uuid: '123-45-66'
      }
    };

    const response = await collectionFormActions.downloadCaseDocumentFile(
      requestBody
    )(store.dispatch, store.getState, {});

    expect(isRejected(response)).toBeTruthy();
    expect(mockToastAdd).toBeCalledWith({
      message: 'txt_death_certificate_failed_to_download',
      type: 'error'
    });

    await collectionFormActions.downloadCaseDocumentFile({
      ...requestBody,
      type: 'bankruptcyDocument'
    })(store.dispatch, store.getState, {});

    expect(mockToastAdd).toBeCalledWith({
      message: 'txt_bankruptcy_documents_failed_to_download',
      type: 'error'
    });
  });
});
