import { collectionFormActions, reducer } from './reducers';
import { storeId } from 'app/test-utils';
import { CollectionFormState, UploadFileType } from '../types';
import cloneDeep from 'lodash.clonedeep'
import set from 'lodash.set';

import '../helpers';

const initState: CollectionFormState = {
  [storeId]: {
    isOpenModal: true,
    selectedCallResult: { code: 'code', description: 'description' },
    uploadFiles: {
      openModal: true,
      type: 'hardShip'
    }
  }
};

describe('Testing collection form', () => {
  it('selectCallResult', () => {
    const mockCallResult = {
      description: 'test',
      value: 'test',
      code: 'code'
    };
    const action = collectionFormActions.selectCallResult({
      storeId,
      item: mockCallResult
    });

    const nextState = reducer(initState, action);

    expect(nextState[storeId].selectedCallResult).toEqual(mockCallResult);
  });

  it('removeStore', () => {
    const action = collectionFormActions.removeStore({
      storeId
    });

    const nextState = reducer(initState, action);

    expect(nextState[storeId]).toBeUndefined();
  });

  it('toggleModal', () => {
    const action = collectionFormActions.toggleModal({
      storeId
    });

    const nextState = reducer(initState, action);

    expect(nextState[storeId].isOpenModal).toBe(
      !initState[storeId].isOpenModal
    );
  });

  describe('setCurrentView', () => {
    it('with view', () => {
      const mockCurrentView = 'view';
      const action = collectionFormActions.setCurrentView({
        storeId,
        view: mockCurrentView
      });

      const nextState = reducer(initState, action);

      expect(nextState[storeId].currentView).toEqual(mockCurrentView);
    });

    it('without view', () => {
      const action = collectionFormActions.setCurrentView({
        storeId
      });

      const nextState = reducer(initState, action);

      expect(nextState[storeId].currentView).toEqual('default');
    });
  });

  it('clearSelectedCallResult', () => {
    const action = collectionFormActions.clearSelectedCallResult({
      storeId
    });

    const expectRes = {
      isOpenModal: true,
      uploadFiles: { openModal: true, type: 'hardShip' }
    };

    const nextState = reducer(initState, action);

    expect(nextState[storeId]).toEqual(expectRes);
  });

  describe('setViewName', () => {
    it('with newName', () => {
      const mockViewName = ['viewName'];
      const action = collectionFormActions.setViewName({
        storeId,
        viewName: mockViewName
      });

      const nextState = reducer(initState, action);

      expect(nextState[storeId].viewName).toEqual(mockViewName);
    });

    it('without newName', () => {
      const action = collectionFormActions.setViewName({
        storeId
      });

      const nextState = reducer(initState, action);

      expect(nextState[storeId].viewName).toEqual([]);
    });
  });

  it('toggleMemoSection', () => {
    const action = collectionFormActions.toggleMemoSection({
      storeId
    });

    const nextState = reducer(initState, action);

    expect(nextState[storeId].isMemoSectionCollapsed).toBeTruthy();
  });

  it('changeDisabledOk', () => {
    const action = collectionFormActions.changeDisabledOk({
      storeId,
      disabledOk: true
    });

    const nextState = reducer(initState, action);

    expect(nextState[storeId].disabledOk).toBeTruthy();
  });

  it('changeMethod', () => {
    const mockMethod = 'method';
    const action = collectionFormActions.changeMethod({
      storeId,
      method: mockMethod
    });

    const nextState = reducer(initState, action);

    expect(nextState[storeId].method).toEqual(mockMethod);
  });

  it('toggleLast10Actions', () => {
    const action = collectionFormActions.toggleLast10Actions({
      storeId
    });

    const nextState = reducer(initState, action);
    expect(nextState[storeId].last10Actions).toEqual({
      isOpen: true
    });
  });

  it('toggleCallResultInProgressStatus', () => {
    const mockData = {
      storeId,
      info: { key: '1' },
      lastDelayCallResult: 'code'
    };
    const action =
      collectionFormActions.toggleCallResultInProgressStatus(mockData);

    const nextState = reducer(initState, action);
    expect(nextState[storeId].lastDelayCallResult).toEqual('code');
  });

  it('toggleCallResultInProgressStatus > isInprogress is true', () => {
    const mockData = {
      storeId,
      info: { key: '1' },
    };

    const newState = cloneDeep(initState);
    set(newState, [storeId, 'isInProgress'], true);
    set(newState, [storeId, 'lastDelayCallResult'], 'lastDelayCallResult');

    const action =
      collectionFormActions.toggleCallResultInProgressStatus(mockData);

    const nextState = reducer(newState, action);
    expect(nextState[storeId].lastDelayCallResult).toEqual(
      'lastDelayCallResult'
    );
  });


  it('removeLastDelayCallResult', () => {
    const mockData = {
      storeId
    };
    const action = collectionFormActions.removeLastDelayCallResult(mockData);

    const nextState = reducer(initState, action);
    expect(nextState[storeId].lastDelayCallResult).toBeUndefined();
  });

  it('changeLastFollowUpData', () => {
    const mockData = {
      storeId,
      callResultType: 'callResultType'
    };
    const action = collectionFormActions.changeLastFollowUpData(mockData);

    const nextState = reducer(initState, action);
    expect(nextState[storeId].lastFollowUpData?.callResultType).toEqual(
      'callResultType'
    );
  });

  it('changeLastFollowUpData > callResultType is empty', () => {
    const mockData = {
      storeId,
      callResultType: ''
    };
    const action = collectionFormActions.changeLastFollowUpData(mockData);

    const nextState = reducer(initState, action);
    expect(nextState[storeId].lastFollowUpData?.callResultType).toBeUndefined();
  });

  it('openUploadFileModal', () => {
    const mockData = {
      storeId,
      type: 'hardShip' as UploadFileType
    };
    const action = collectionFormActions.openUploadFileModal(mockData);

    const nextState = reducer(initState, action);
    expect(nextState[storeId].uploadFiles).toEqual({
      type: 'hardShip',
      openModal: true
    });
  });

  it('closeUploadFileModal', () => {
    const mockData = {
      storeId
    };
    const action = collectionFormActions.closeUploadFileModal(mockData);

    const nextState = reducer(initState, action);
    expect(nextState[storeId].uploadFiles).toEqual({
      openModal: false
    });
  });

  it('setDirtyForm', () => {
    const mockData = {
      storeId,
      callResult: 'data',
      value: true
    };
    const action = collectionFormActions.setDirtyForm(mockData);

    const nextState = reducer(initState, action);
    expect(nextState[storeId].dirtyForm).toEqual({
      data: true
    });
  });
});
