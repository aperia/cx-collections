import {
  CollectionFormState,
  GetListDocumentRequest,
  GetListDocumentsArgs
} from '../types';
import { collectionFormService } from '../collectionFormServices';
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { EMPTY_STRING } from 'app/constants';
import { combineDocumentClassification } from 'pages/CollectionForm/helpers';

export const getListCaseDocument = createAsyncThunk<
  { files: MagicKeyValue[] },
  GetListDocumentsArgs,
  ThunkAPIConfig
>('deceased/getDeathCertificates', async (args, thunkAPI) => {
  const { storeId, type } = args;
  const { accountDetail } = thunkAPI.getState();
  const {
    clientIdentifier,
    systemIdentifier,
    principalIdentifier,
    agentIdentifier
  } = accountDetail[storeId]?.rawData || {};
  const { commonConfig } = window.appConfig || {};
  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;
  const requestData: GetListDocumentRequest = {
    common: {
      agent: agentIdentifier,
      clientNumber: clientIdentifier,
      system: systemIdentifier,
      prin: principalIdentifier,
      app: commonConfig?.app || EMPTY_STRING,
      user: commonConfig?.user,
      accountId: eValueAccountId
    },
    documentClassification: combineDocumentClassification(
      type,
      eValueAccountId!,
      type
    )
  };
  try {
    const response = await collectionFormService.getListCaseDocuments(
      requestData
    );
    if (response?.data?.responseStatus?.toLowerCase() !== 'success') {
      throw response;
    }
    return { files: response?.data?.documentDetailList };
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const getListCaseDocumentBuilder = (
  builder: ActionReducerMapBuilder<CollectionFormState>
) => {
  builder
    .addCase(getListCaseDocument.pending, (draftState, action) => {
      const { storeId, type } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        caseDocumentFiles: {
          ...draftState[storeId]?.caseDocumentFiles,
          [type]: {
            ...draftState[storeId]?.caseDocumentFiles?.[type],
            loading: true
          }
        }
      };
    })
    .addCase(getListCaseDocument.fulfilled, (draftState, action) => {
      const { storeId, type } = action.meta.arg;
      const { files } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        caseDocumentFiles: {
          ...draftState[storeId]?.caseDocumentFiles,
          [type]: {
            files,
            loading: false
          }
        }
      };
    })
    .addCase(getListCaseDocument.rejected, (draftState, action) => {
      const { storeId, type } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        caseDocumentFiles: {
          ...draftState[storeId]?.caseDocumentFiles,
          [type]: {
            files: [],
            loading: false,
            error: true
          }
        }
      };
    });
};
