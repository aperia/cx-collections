import './reducers';
import { applyMiddleware, createStore } from '@reduxjs/toolkit';
import { getTalkingPoint } from './getTalkingPoint';
import { responseDefault, storeId, accEValue } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import thunk from 'redux-thunk';
import { collectionFormService } from '../collectionFormServices';
import { GetTalkingPointPayload } from '../types';

describe('Test getTalkingPoint', () => {
  const mockArgs = {
    storeId,
    accEValue
  };

  const store = createStore(
    rootReducer,
    {
      accountDetail: {
        [storeId]: {
          rawData: {
            systemIdentifier: '123',
            clientIdentifier: '123',
            principalIdentifier: '123',
            agentIdentifier: '123'
          }
        }
      }
    },
    applyMiddleware(thunk)
  );

  it('getTalkingPoint fulfilled', async () => {
    window.appConfig = {
      commonConfig: {
        org: 'org',
        app: 'app'
      }
    } as AppConfiguration;

    jest.spyOn(collectionFormService, 'getTalkingPoint').mockResolvedValue({
      ...responseDefault,
      data: {
        panelContentText: '123'
      }
    });

    const response = await getTalkingPoint(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('collectionForm/getTalkingPoint/fulfilled');
  });

  it('getTalkingPoint rejected', async () => {
    jest.spyOn(collectionFormService, 'getTalkingPoint').mockRejectedValue({
      ...responseDefault,
      data: null
    });

    const response = await getTalkingPoint(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('collectionForm/getTalkingPoint/rejected');
  });

  it('coverage', async () => {
    window.appConfig = undefined as unknown as AppConfiguration;

    const emptyStore = createStore(
      rootReducer,
      {
        accountDetail: {}
      },
      applyMiddleware(thunk)
    );

    jest.spyOn(collectionFormService, 'getTalkingPoint').mockResolvedValue({
      ...responseDefault,
      data: {} as GetTalkingPointPayload
    });

    const response = await getTalkingPoint(mockArgs)(
      emptyStore.dispatch,
      emptyStore.getState,
      {}
    );

    expect(response.type).toEqual('collectionForm/getTalkingPoint/fulfilled');
  });
});
