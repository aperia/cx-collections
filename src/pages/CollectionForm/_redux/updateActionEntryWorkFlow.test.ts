import './reducers';
import { applyMiddleware, createStore } from '@reduxjs/toolkit';
import { updateActionEntryWorkFlow } from './updateActionEntryWorkFlow';
import { responseDefault, storeId } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import thunk from 'redux-thunk';
import { CALL_RESULT_CODE } from '../constants';
import { collectionFormService } from '../collectionFormServices';

describe('Test updateActionEntryWorkFlow', () => {
  const mockArgs = {
    storeId,
    postData: { accountId: storeId }
  };

  const store = createStore(
    rootReducer,
    {
      accountDetail: {
        [storeId]: {
          selectedAuthenticationData: {
            customerNameForCreateWorkflow: 'string'
          }
        }
      },
      collectionForm: {
        [storeId]: {
          selectedCallResult: {
            code: CALL_RESULT_CODE.BANKRUPTCY,
            description: 'des'
          }
        }
      }
    },
    applyMiddleware(thunk)
  );

  it('updateActionEntryWorkFlow fulfilled', async () => {
    window.appConfig = {
      commonConfig: {
        app: 'app',
        org: 'org',
        callingApplicationApr01: 'callingApplicationApr01',
        operatorID: 'operatorID'
      }
    };
    jest
      .spyOn(collectionFormService, 'updateActionEntryWorkFlow')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          status: 'success',
          value: '123'
        }
      });

    const response = await updateActionEntryWorkFlow(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'collectionForm/updateActionEntryWorkFlow/fulfilled'
    );
  });

  it('updateActionEntryWorkFlow fulfilled > appConfig is not existed', async () => {
    window.appConfig = null as unknown as AppConfiguration;
    jest
      .spyOn(collectionFormService, 'updateActionEntryWorkFlow')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          status: 'success',
          value: '123'
        }
      });

    const response = await updateActionEntryWorkFlow(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'collectionForm/updateActionEntryWorkFlow/fulfilled'
    );
  });

  it('updateActionEntryWorkFlow rejected', async () => {
    const response = await updateActionEntryWorkFlow(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'collectionForm/updateActionEntryWorkFlow/rejected'
    );
    expect(response.payload).toBeUndefined();
  });
});
