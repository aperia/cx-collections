import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { triggerStartCollectionForm } from './startCollectionForm';
import { mockActionCreator, storeId } from 'app/test-utils';
import { CALL_RESULT_CODE } from '../constants';
import { collectionFormActions } from './reducers';

const collectionFormActionsSpy = mockActionCreator(collectionFormActions);

describe('triggerStartCollectionForm', () => {
  describe('async thunk', () => {
    it('with state', async () => {
      const mockAction = collectionFormActionsSpy('setCurrentView');

      const store = createStore(rootReducer, {
        collectionForm: {
          [storeId]: { selectedCallResult: { code: CALL_RESULT_CODE.HARDSHIP } }
        },
        tab: { tabStoreIdSelected: storeId }
      });

      await triggerStartCollectionForm({
        storeId,
        postData: { accountId: storeId }
      })(store.dispatch, store.getState, {});

      expect(mockAction).toBeCalledWith({
        storeId,
        view: CALL_RESULT_CODE.HARDSHIP
      });
    });

    it('with empty state', async () => {
      const mockAction = collectionFormActionsSpy('toggleModal');

      const store = createStore(rootReducer, {
        tab: { tabStoreIdSelected: storeId }
      });

      await triggerStartCollectionForm({
        storeId,
        postData: { accountId: storeId }
      })(store.dispatch, store.getState, {});

      expect(mockAction).toBeCalledWith({ storeId });
    });
  });
});
