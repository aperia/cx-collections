import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// Const
import { EMPTY_STRING, RECORD_NOT_FOUND_MESSAGE } from 'app/constants';

// Helper
import { getFeatureConfig, mappingDataFromArray } from 'app/helpers';

// Service
import { collectionFormService } from '../collectionFormServices';

// Type
import {
  ActionCodesTypes,
  ClientInfoGetByIdRequest,
  CollectionFormState,
  GetLast25ActionArgs,
  Last25ActionData,
  Last25ActionPayload,
  Last25ActionRequest
} from '../types';

export const getLast25ActionsRequest = createAsyncThunk<
  Last25ActionPayload,
  GetLast25ActionArgs,
  ThunkAPIConfig
>('collectionForm/getLast25ActionsRequest', async (args, thunkAPI) => {
  const {
    storeId,
    postData: { accountId }
  } = args;

  const last25Config = await getFeatureConfig<Last25ActionRequest>(
    'config/last25actions.json'
  );

  const last25Request: Last25ActionRequest = {
    ...last25Config,
    common: {
      accountId
    }
  };

  const { accountDetail } = thunkAPI.getState();
  const {
    clientIdentifier,
    systemIdentifier,
    principalIdentifier,
    agentIdentifier
  } = accountDetail[storeId]?.rawData || {};
  const { commonConfig } = window.appConfig || {};

  const clientInfoId = `${clientIdentifier}${systemIdentifier}${principalIdentifier}${agentIdentifier}`;

  const codeDescriptionRequest: ClientInfoGetByIdRequest = {
    common: {
      agent: agentIdentifier,
      clientNumber: clientIdentifier,
      prin: principalIdentifier,
      system: systemIdentifier,
      app: commonConfig?.app || EMPTY_STRING
    },
    clientInfoId,
    enableFallback: true
  };

  try {
    let lastActionsData = [];
    let codeDescriptionData = {} as Record<string, string>;

    const [lastActionsRes, codeDescriptionRes] = await Promise.allSettled([
      collectionFormService.getLast25Actions(last25Request),
      collectionFormService.getDescriptionCodeForLast25Actions(
        codeDescriptionRequest
      )
    ]);

    if (lastActionsRes.status === 'fulfilled') {
      lastActionsData = lastActionsRes.value.data?.actionEntryFields;
    }
    if (codeDescriptionRes.status === 'fulfilled') {
      const actionCodes = (codeDescriptionRes.value.data?.clientInfo
        ?.actionCodes || []) as ActionCodesTypes[];

      codeDescriptionData = actionCodes.reduce(
        (acc, { actionCode = '', actionCodeDescription = '' }) => {
          acc[actionCode.trim()] = actionCodeDescription;
          return acc;
        },
        {} as Record<string, string>
      );
    }

    const mappingModel =
      thunkAPI.getState()?.mapping?.data?.last25Actions || {};

    const mappedData = mappingDataFromArray<Last25ActionData>(
      lastActionsData,
      mappingModel
    );

    const mapCodeDescription = mappedData.map(item => ({
      ...item,
      description: codeDescriptionData[item?.actionEntryPrefixCode]
    }));

    return { data: mapCodeDescription };
  } catch (error) {
    const message = error?.data?.message;
    if (message === RECORD_NOT_FOUND_MESSAGE) {
      return {
        data: []
      };
    }
    return thunkAPI.rejectWithValue({ errorMessage: error?.message });
  }
});

export const getLast25ActionsBuilder = (
  builder: ActionReducerMapBuilder<CollectionFormState>
) => {
  builder
    .addCase(getLast25ActionsRequest.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].last10Actions = {
        ...draftState[storeId].last10Actions,
        loading: true
      };
    })
    .addCase(getLast25ActionsRequest.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      draftState[storeId].last10Actions = {
        ...draftState[storeId].last10Actions,
        loading: false,
        data,
        error: false
      };
    })
    .addCase(getLast25ActionsRequest.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId].last10Actions = {
        ...draftState[storeId].last10Actions,
        loading: false,
        error: true
      };
    });
};
