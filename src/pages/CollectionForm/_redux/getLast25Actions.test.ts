import './reducers';
import { applyMiddleware, createStore } from '@reduxjs/toolkit';
import { getLast25ActionsRequest } from './getLast25Actions';
import { responseDefault, storeId } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import thunk from 'redux-thunk';
import * as commonActions from 'app/helpers/commons';
import { collectionFormService } from '../collectionFormServices';
import { RECORD_NOT_FOUND_MESSAGE } from 'app/constants';

describe('Test getLast25ActionsRequest', () => {
  const mockArgs = {
    storeId,
    postData: { accountId: storeId }
  };

  beforeEach(() => {
    jest.spyOn(commonActions, 'getFeatureConfig').mockResolvedValue({
      common: {
        accountId: 'accountId',
        app: 'IdApp'
      }
    });

    jest
      .spyOn(collectionFormService, 'getCallResultsRefData')
      .mockResolvedValue({
        data: '123'
      });
  });

  afterAll(() => {
    jest.clearAllMocks();
  });

  it('getLast25ActionsRequest fulfilled', async () => {
    window.appConfig = {
      commonConfig: {
        app: 'app'
      }
    } as AppConfiguration;

    const store = createStore(
      rootReducer,
      {
        collectionForm: {
          [storeId]: {
            last10Actions: {
              data: [],
              error: false,
              isOpen: true,
              loading: false
            }
          }
        },
        accountDetail: {
          [storeId]: {
            rawData: {
              clientIdentifier: 'clientIdentifier',
              systemIdentifier: 'systemIdentifier',
              principalIdentifier: 'principalIdentifier',
              agentIdentifier: 'agentIdentifier'
            }
          }
        },
        mapping: {
          data: {
            last25Actions: {
              record: 'DC'
            }
          }
        }
      },
      applyMiddleware(thunk)
    );

    jest.spyOn(collectionFormService, 'getLast25Actions').mockResolvedValue({
      ...responseDefault,
      data: {
        actionEntryFields: [{ actionEntryPrefixCode: 'DC' }]
      }
    });

    jest
      .spyOn(collectionFormService, 'getDescriptionCodeForLast25Actions')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          clientInfo: {
            actionCodes: [
              { actionCode: 'cox', actionCodeDescription: 'des' },
              { actionCode: 'code', actionCodeDescription: 'des' }
            ]
          }
        }
      });

    const response = await getLast25ActionsRequest(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'collectionForm/getLast25ActionsRequest/fulfilled'
    );
  });

  describe('getLast25ActionsRequest fulfilled with empty store and config', () => {
    beforeEach(() => {
      window.appConfig = undefined as unknown as AppConfiguration;
    });

    const emptyStore = createStore(
      rootReducer,
      {
        collectionForm: {
          [storeId]: {}
        },
        accountDetail: {
          [storeId]: {}
        },
        mapping: {
          data: {}
        }
      },
      applyMiddleware(thunk)
    );

    it('when action codes empty', async () => {
      jest.spyOn(collectionFormService, 'getLast25Actions').mockResolvedValue({
        ...responseDefault,
        data: {
          actionEntryFields: 'actionEntryFields'
        }
      });

      jest
        .spyOn(collectionFormService, 'getDescriptionCodeForLast25Actions')
        .mockResolvedValue(responseDefault);

      const response = await getLast25ActionsRequest(mockArgs)(
        emptyStore.dispatch,
        emptyStore.getState,
        {}
      );

      expect(response.type).toEqual(
        'collectionForm/getLast25ActionsRequest/fulfilled'
      );
    });

    it('when action codes provided', async () => {
      jest.spyOn(collectionFormService, 'getLast25Actions').mockResolvedValue({
        ...responseDefault,
        data: {
          actionEntryFields: 'actionEntryFields'
        }
      });

      jest
        .spyOn(collectionFormService, 'getDescriptionCodeForLast25Actions')
        .mockResolvedValue({
          ...responseDefault,
          data: {
            clientInfo: {
              actionCodes: [
                { actionCode: undefined, actionCodeDescription: 'des' },
                { actionCode: 'code', actionCodeDescription: undefined }
              ]
            }
          }
        });

      const response = await getLast25ActionsRequest(mockArgs)(
        emptyStore.dispatch,
        emptyStore.getState,
        {}
      );

      expect(response.type).toEqual(
        'collectionForm/getLast25ActionsRequest/fulfilled'
      );
    });
  });

  it('getLast25ActionsRequest rejected', async () => {
    window.appConfig = {
      commonConfig: {
        app: 'app'
      }
    } as AppConfiguration;

    const store = createStore(
      rootReducer,
      {
        collectionForm: {
          [storeId]: {
            last10Actions: {
              data: [],
              error: false,
              isOpen: true,
              loading: false
            }
          }
        },
        accountDetail: {
          [storeId]: {
            rawData: {
              clientIdentifier: 'clientIdentifier',
              systemIdentifier: 'systemIdentifier',
              principalIdentifier: 'principalIdentifier',
              agentIdentifier: 'agentIdentifier'
            }
          }
        },
        mapping: {
          data: {
            last25Actions: [
              {
                key: '1',
                value: '123'
              },
              {
                key: '2',
                value: '123'
              }
            ]
          }
        }
      },
      applyMiddleware(thunk)
    );

    jest.spyOn(collectionFormService, 'getLast25Actions').mockRejectedValue({});
    jest
      .spyOn(collectionFormService, 'getDescriptionCodeForLast25Actions')
      .mockRejectedValue({
        ...responseDefault,
        data: { clientInfo: { actionCodes: [] } }
      });

    const response = await getLast25ActionsRequest(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'collectionForm/getLast25ActionsRequest/fulfilled'
    );
    expect(response.payload).toEqual({
      data: []
    });

    jest.spyOn(collectionFormService, 'getLast25Actions').mockRejectedValue({});

    const response2 = await getLast25ActionsRequest(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response2.type).toEqual(
      'collectionForm/getLast25ActionsRequest/fulfilled'
    );
    expect(response2.payload).toEqual({
      data: []
    });

    jest
      .spyOn(collectionFormService, 'getDescriptionCodeForLast25Actions')
      .mockImplementation(() => {
        throw {
          data: {
            message: RECORD_NOT_FOUND_MESSAGE
          }
        };
      });
    await getLast25ActionsRequest(mockArgs)(store.dispatch, store.getState, {});
  });

  it('getLast25ActionsRequest rejected > api throw error', async () => {
    window.appConfig = {
      commonConfig: {
        app: 'app'
      }
    } as AppConfiguration;

    const store = createStore(
      rootReducer,
      {
        collectionForm: {
          [storeId]: {
            last10Actions: {
              data: [],
              error: false,
              isOpen: true,
              loading: false
            }
          }
        },
        accountDetail: {
          [storeId]: {
            rawData: {
              clientIdentifier: 'clientIdentifier',
              systemIdentifier: 'systemIdentifier',
              principalIdentifier: 'principalIdentifier',
              agentIdentifier: 'agentIdentifier'
            }
          }
        },
        mapping: {
          data: {
            last25Actions: [
              {
                key: '1',
                value: '123'
              },
              {
                key: '2',
                value: '123'
              }
            ]
          }
        }
      },
      applyMiddleware(thunk)
    );

    jest
      .spyOn(collectionFormService, 'getLast25Actions')
      .mockImplementation(() => {
        const error = new Error();
        error.data = { message: RECORD_NOT_FOUND_MESSAGE };
        throw error;
      });
    jest.spyOn(collectionFormService, 'getLast25Actions').mockRejectedValue({});
    jest
      .spyOn(collectionFormService, 'getDescriptionCodeForLast25Actions')
      .mockImplementation(() => {
        const error = new Error();
        error.message = 'error';
        throw error;
      });

    const response = await getLast25ActionsRequest(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'collectionForm/getLast25ActionsRequest/rejected'
    );
    expect(response.payload).toEqual({
      errorMessage: 'error'
    });
  });

  it('getLast25ActionsRequest rejected > api throw error message record not found', async () => {
    window.appConfig = {
      commonConfig: {
        app: 'app'
      }
    } as AppConfiguration;

    const store = createStore(
      rootReducer,
      {
        collectionForm: {
          [storeId]: {
            last10Actions: {
              data: [],
              error: false,
              isOpen: true,
              loading: false
            }
          }
        },
        accountDetail: {
          [storeId]: {
            rawData: {
              clientIdentifier: 'clientIdentifier',
              systemIdentifier: 'systemIdentifier',
              principalIdentifier: 'principalIdentifier',
              agentIdentifier: 'agentIdentifier'
            }
          }
        },
        mapping: {
          data: {
            last25Actions: [
              {
                key: '1',
                value: '123'
              },
              {
                key: '2',
                value: '123'
              }
            ]
          }
        }
      },
      applyMiddleware(thunk)
    );

    jest
      .spyOn(collectionFormService, 'getLast25Actions')
      .mockImplementation(() => {
        const error = new Error();
        error.data = { message: RECORD_NOT_FOUND_MESSAGE };
        throw error;
      });
    jest
      .spyOn(collectionFormService, 'getDescriptionCodeForLast25Actions')
      .mockRejectedValue({
        ...responseDefault,
        data: {
          clientInfo: {
            actionCodes: []
          }
        }
      });

    const response = await getLast25ActionsRequest(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'collectionForm/getLast25ActionsRequest/fulfilled'
    );
    expect(response.payload).toEqual({ data: [] });
  });
});
