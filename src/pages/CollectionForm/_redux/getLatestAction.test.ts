import './reducers';
import { applyMiddleware, createStore } from '@reduxjs/toolkit';
import { getLatestActionRequest } from './getLatestAction';
import { responseDefault, storeId, accEValue } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import thunk from 'redux-thunk';
import { collectionFormService } from '../collectionFormServices';
import { CALL_RESULT_CODE } from '../constants';
import { incarcerationService } from 'pages/Incarceration/api-services';
import { longTermMedicalService } from 'pages/LongTermMedical/longTermMedicalServices';

describe('Test getLatestAction', () => {
  const mockArgs = {
    storeId,
    accEValue
  };

  const store = createStore(
    rootReducer,
    {
      collectionForm: {
        [storeId]: {
          last10Actions: {
            data: [],
            error: false,
            isOpen: true,
            loading: false
          }
        }
      },
      accountDetail: {
        [storeId]: {
          rawData: {
            clientIdentifier: 'clientIdentifier',
            systemIdentifier: 'systemIdentifier',
            principalIdentifier: 'principalIdentifier',
            agentIdentifier: 'agentIdentifier'
          }
        }
      },
      mapping: {
        data: {
          last25Actions: [
            {
              key: '1',
              value: '123'
            },
            {
              key: '2',
              value: '123'
            }
          ]
        }
      }
    },
    applyMiddleware(thunk)
  );

  it('getLatestActionRequest fulfilled', async () => {
    jest.spyOn(collectionFormService, 'getLatestAction').mockResolvedValue({
      ...responseDefault,
      data: {
        actionEntryFields: [
          { actionEntryPrefixCode: CALL_RESULT_CODE.DECEASE_PENDING }
        ]
      }
    });
    const response = await getLatestActionRequest(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('collectionForm/getLatestAction/fulfilled');
    expect(response.payload).toEqual({
      latestCallResultCode: 'DP'
    });
  });

  it('getLatestActionRequest fulfilled > api return empty data', async () => {
    jest.spyOn(collectionFormService, 'getLatestAction').mockResolvedValue({
      ...responseDefault,
      data: {
        actionEntryFields: []
      }
    });
    const response = await getLatestActionRequest(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('collectionForm/getLatestAction/fulfilled');
    expect(response.payload).toEqual({});
  });

  it('getLatestActionRequest fulfilled > CALL_RESULT_CODE.LONG_TERM_MEDICAL', async () => {
    jest.spyOn(collectionFormService, 'getLatestAction').mockResolvedValue({
      ...responseDefault,
      data: {
        actionEntryFields: [
          { actionEntryPrefixCode: CALL_RESULT_CODE.LONG_TERM_MEDICAL }
        ]
      }
    });
    jest.spyOn(longTermMedicalService, 'getLongTermMedical').mockResolvedValue({
      ...responseDefault,
      data: {
        longTermMedicalStatus: 'INACTIVE' as never
      }
    });
    const response = await getLatestActionRequest(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('collectionForm/getLatestAction/fulfilled');
    expect(response.payload).toEqual({
      latestCallResultCode: 'ME'
    });
  });

  it('getLatestActionRequest fulfilled > CALL_RESULT_CODE.INCARCERATION', async () => {
    jest.spyOn(collectionFormService, 'getLatestAction').mockResolvedValue({
      ...responseDefault,
      data: {
        actionEntryFields: [
          { actionEntryPrefixCode: CALL_RESULT_CODE.INCARCERATION }
        ]
      }
    });
    jest.spyOn(incarcerationService, 'getIncarceration').mockResolvedValue({
      ...responseDefault,
      data: {
        incarcerationStatus: 'INACTIVE' as never
      }
    });
    const response = await getLatestActionRequest(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('collectionForm/getLatestAction/fulfilled');
    expect(response.payload).toEqual({
      latestCallResultCode: 'FD'
    });
  });

  it('getLatestActionRequest rejected', async () => {
    jest.spyOn(collectionFormService, 'getLatestAction').mockRejectedValue({});

    const response = await getLatestActionRequest(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('collectionForm/getLatestAction/rejected');
    expect(response.payload).toBeUndefined();
  });
});
