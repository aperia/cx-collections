import * as selector from './selectors';

// mocks
import { useSelectorMock } from 'app/test-utils/useSelectorMock';

// utils
import set from 'lodash.set';
import cloneDeep from 'lodash.clonedeep';
import { storeId } from 'app/test-utils';
import { CALL_RESULT_CODE } from '../constants';
import { CollectionForm } from '../types';
import { Hardship } from 'pages/HardShip/types';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { HARDSHIP_COMPLETE } from 'pages/HardShip/constants';
import { CCCS_ACTIONS } from 'pages/CCCS/constants';

const store: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      collectionCurrentData: {
        key: '1'
      },
      isMemoSectionCollapsed: true,
      lastFollowUpData: {},
      isLoadingLatestCollectionInfo: true,
      uploadFiles: {
        type: 'hardShip',
        openModal: true
      },
      talkingPoint: {
        data: 'talking point',
        loading: false
      },
      last10Actions: {
        isOpen: false,
        data: 'last10Actions',
        loading: false,
        error: false
      }
    } as CollectionForm
  },
  accountDetail: {
    [storeId]: {
      data: {
        internalStatusCode: '123'
      }
    }
  },
  bankruptcy: {
    [storeId]: {
      info: {
        actionEntryCode: CALL_RESULT_CODE.BANKRUPTCY_PENDING
      }
    }
  },
  hardship: {
    [storeId]: {
      typeForm: {
        id: 'id',
        value: 'value'
      },
      dataDetails: {
        key: 'value'
      },
      isLoading: false,
      data: {
        processStatus: HARDSHIP_COMPLETE
      }
    } as Hardship
  },
  deceased: {
    [storeId]: {
      loading: false,
      updateDeceasedLoading: false,
      deceasedInformation: {
        deceasedConfirmation: 'Deceased Pending'
      }
    }
  },
  longTermMedical: {
    [storeId]: {
      data: {
        longTermMedicalDetails: 'longTermMedicalDetails',
        longTermMedicalStatus: 'ACTIVE'
      },
      isLoading: false
    }
  },
  incarceration: {
    [storeId]: {
      data: {
        incarcerationStatus: 'ACTIVE',
        incarcerationContactName: 'incarcerationContactName',
        locationOfIncarceration: 'locationOfIncarceration',
        incarcerationStartDate: '1',
        incarcerationEndDate: '2'
      },
      isLoading: false
    }
  },
  cccs: {
    [storeId]: {
      data: {
        action: CCCS_ACTIONS.ACCEPT_PROPOSAL,
        processStatus: 'processStatus',
        currentBalance: ''
      }
    }
  },
  promiseToPay: {
    [storeId]: {
      loading: true
    }
  }
};

const emptyStore: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      selectedCallResult: {
        description: 'des',
        code: 'ME'
      },
      uploadFiles: {}
    }
  },
  hardship: {
    [storeId]: {
      isLoading: false
    } as Hardship
  },
  longTermMedical: {
    [storeId]: {
      isLoading: false
    }
  },
  incarceration: {
    [storeId]: {
      isLoading: false
    }
  },
  promiseToPay: {
    [storeId]: {
      loading: true
    }
  }
};

describe('Test Collection Form selectors', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isCollector: true
    }
  };
  it('selectLoadingLetter', () => {
    const data = useSelectorMock(
      selector.getCollectionFormData,
      store,
      storeId
    );
    const expected = store.collectionForm![storeId]?.collectionCurrentData;
    expect(data).toEqual(expected);
  });

  it('getMemoSectionCollapsed ', () => {
    const data = useSelectorMock(
      selector.getMemoSectionCollapsed,
      store,
      storeId
    );
    const expected = store.collectionForm![storeId]?.isMemoSectionCollapsed;
    expect(data).toEqual(expected);
  });

  it('selectLabelCallResult ', () => {
    const data = useSelectorMock(
      selector.selectLabelCallResult,
      store,
      storeId
    );
    expect(data).toEqual('');
  });

  it('takeCollectionResultData', () => {
    const data = useSelectorMock(
      selector.takeCollectionResultData,
      store,
      storeId
    );
    expect(data).toEqual({});
  });

  it('selectCollectionStatusCCCS', () => {
    const data = useSelectorMock(
      selector.selectCollectionStatusCCCS,
      store,
      storeId
    );
    expect(data).toBeUndefined();
  });

  it('takeCollectionFormData', () => {
    const data = useSelectorMock(
      selector.takeCollectionFormData,
      store,
      storeId
    );
    const expected = store.collectionForm![storeId]?.collectionCurrentData;
    expect(data).toEqual(expected);
  });

  it('takeLoadingLatestCollectionInfo', () => {
    const data = useSelectorMock(
      selector.takeLoadingLatestCollectionInfo,
      store,
      storeId
    );
    const expected =
      store.collectionForm![storeId]?.isLoadingLatestCollectionInfo;
    expect(data).toEqual(expected);
  });

  it('selectMemoSectionCollapsed', () => {
    const data = useSelectorMock(
      selector.selectMemoSectionCollapsed,
      store,
      storeId
    );
    const expected = store.collectionForm![storeId]?.isMemoSectionCollapsed;
    expect(data).toEqual(expected);
  });

  it('takeInternalStatusCode', () => {
    const data = useSelectorMock(
      selector.takeInternalStatusCode,
      store,
      storeId
    );
    const expected = store.accountDetail![storeId]?.data?.internalStatusCode;
    expect(data).toEqual(expected);
  });

  it('isHavingBankruptcyPending', () => {
    const data = useSelectorMock(
      selector.isHavingBankruptcyPending,
      store,
      storeId
    );
    expect(data).toBeTruthy();
  });

  it('selectIsHavingBankruptcyPending ', () => {
    const data = useSelectorMock(
      selector.selectIsHavingBankruptcyPending,
      store,
      storeId
    );
    expect(data).toBeTruthy();
  });

  it('isHavingValidHardshipData ', () => {
    const data = useSelectorMock(
      selector.isHavingValidHardshipData,
      store,
      storeId
    );
    expect(data).toBeTruthy();
  });

  it('existsHardshipData', () => {
    const data = useSelectorMock(selector.existsHardshipData, store, storeId);
    expect(data).toEqual(store.hardship![storeId].data);
  });

  it('selectIsHavingValidHardshipData ', () => {
    const data = useSelectorMock(
      selector.selectIsHavingValidHardshipData,
      store,
      storeId
    );
    expect(data).toBeTruthy();
  });

  it('selectExistsHardshipData', () => {
    const data = useSelectorMock(
      selector.selectExistsHardshipData,
      store,
      storeId
    );
    expect(data).toEqual(store.hardship![storeId].data);
  });

  it('getCallResultType ', () => {
    const data = useSelectorMock(
      selector.getCallResultType,
      { collectionForm: {} },
      storeId
    );
    expect(data).toEqual('');
  });

  it('getListCallResultRefData', () => {
    const data = useSelectorMock(
      selector.getListCallResultRefData,
      { collectionForm: {} },
      storeId
    );
    expect(data.length).toEqual(0);
  });

  it('getDefaultListCallResultRefData ', () => {
    const data = useSelectorMock(
      selector.getListCallResultRefData,
      { collectionForm: {} },
      storeId
    );
    expect(data.length).toEqual(0);
  });

  it('getSelectedCallResult ', () => {
    const data = useSelectorMock(
      selector.getSelectedCallResult,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeUndefined();
  });

  it('getCollectionResultData ', () => {
    const data = useSelectorMock(
      selector.getCollectionResultData,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeUndefined();
  });

  it('getOpenFormStatus ', () => {
    const data = useSelectorMock(
      selector.getOpenFormStatus,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('getCollectionFormData ', () => {
    const data = useSelectorMock(
      selector.getCollectionFormData,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeUndefined();
  });

  it('getCollectionFormMethod ', () => {
    const data = useSelectorMock(
      selector.getCollectionFormMethod,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeUndefined();
  });

  it('getCurrentView ', () => {
    const data = useSelectorMock(
      selector.getCurrentView,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeUndefined();
  });

  it('getDirtyForm ', () => {
    const data = useSelectorMock(
      selector.getDirtyForm,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeUndefined();
  });

  it('getMemoSectionCollapsed ', () => {
    const data = useSelectorMock(
      selector.getMemoSectionCollapsed,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeUndefined();
  });

  it('getTalkingPoint ', () => {
    const data = useSelectorMock(
      selector.getTalkingPoint,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeUndefined();
  });

  describe('getDisabledFormStatus ', () => {
    it('has NO invalid view', () => {
      const data = useSelectorMock(
        selector.getDisabledFormStatus,
        { collectionForm: {} },
        storeId
      );
      expect(data).toBeFalsy();
    });

    it('has invalid views', () => {
      const data = useSelectorMock(
        selector.getDisabledFormStatus,
        {
          collectionForm: {
            [storeId]: {
              viewName: ['1', '2'],
              uploadFiles: {}
            }
          }
        },
        storeId
      );
      expect(data).toBeFalsy();
    });
  });

  it('getLoadingLatestCollectionInfo ', () => {
    const data = useSelectorMock(
      selector.getLoadingLatestCollectionInfo,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('selectCallResultRefData', () => {
    const data = useSelectorMock(
      selector.selectCallResultRefData,
      { collectionForm: {} },
      storeId
    );
    expect(data.length).toEqual(0);
  });

  it('takeDefaultCallResultRefData ', () => {
    const data = useSelectorMock(
      selector.takeDefaultCallResultRefData,
      { collectionForm: {} },
      storeId
    );
    expect(data.length).toEqual(0);
  });

  it('selectIsHavingData ', () => {
    const data = useSelectorMock(
      selector.selectIsHavingData,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('selectCallResult ', () => {
    const data = useSelectorMock(
      selector.selectCallResult,
      { collectionForm: {} },
      storeId
    );
    expect(data).toEqual('');
  });

  describe('selectLabelCallResult ', () => {
    it('has no last call result type', () => {
      const data = useSelectorMock(
        selector.selectLabelCallResult,
        { collectionForm: {} },
        storeId
      );
      expect(data).toEqual('');
    });

    it('has valid last call result type', () => {
      const data = useSelectorMock(
        selector.selectLabelCallResult,
        {
          collectionForm: {
            [storeId]: {
              lastFollowUpData: {
                callResultType: CALL_RESULT_CODE.BANKRUPTCY_PENDING
              }
            } as CollectionForm
          }
        },
        storeId
      );
      expect(data).toEqual('txt_bankruptcy_(status_pending)');
    });

    it('has invalid last call result type', () => {
      const data = useSelectorMock(
        selector.selectLabelCallResult,
        {
          collectionForm: {
            [storeId]: {
              lastFollowUpData: {
                callResultType: '1'
              }
            } as CollectionForm
          }
        },
        storeId
      );
      expect(data).toEqual('');
    });
  });

  it('takeSelectedCallResult ', () => {
    const data = useSelectorMock(
      selector.takeSelectedCallResult,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeUndefined();
  });

  it('takeCollectionResultData ', () => {
    const data = useSelectorMock(
      selector.takeCollectionResultData,
      { collectionForm: {} },
      storeId
    );
    expect(data).toEqual({});
  });

  it('selectLastFollowUpData ', () => {
    const data = useSelectorMock(
      selector.selectLastFollowUpData,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeNull();
  });

  it('takeOpenFormStatus ', () => {
    const data = useSelectorMock(
      selector.takeOpenFormStatus,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('selectCollectionStatusCCCS ', () => {
    const data = useSelectorMock(
      selector.selectCollectionStatusCCCS,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeUndefined();
  });

  it('takeCollectionFormData ', () => {
    const data = useSelectorMock(
      selector.takeCollectionFormData,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeUndefined();
  });

  it('takeLoadingLatestCollectionInfo ', () => {
    const data = useSelectorMock(
      selector.takeLoadingLatestCollectionInfo,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('selectCollapseMemoStatus ', () => {
    const data = useSelectorMock(
      selector.selectCollapseMemoStatus,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('takeCollectionFormLoadingStatus ', () => {
    const data = useSelectorMock(
      selector.takeCollectionFormLoadingStatus,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('selectCollectionMethod ', () => {
    const data = useSelectorMock(
      selector.selectCollectionMethod,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeUndefined();
  });

  it('takeCurrentView ', () => {
    const data = useSelectorMock(
      selector.takeCurrentView,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeUndefined();
  });

  it('takeDirtyForm ', () => {
    const data = useSelectorMock(
      selector.takeDirtyForm,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeUndefined();
  });

  describe('selectShowRemoveButton ', () => {
    it('should return true', () => {
      const data = useSelectorMock(
        selector.selectShowRemoveButton,
        {
          collectionForm: {
            [storeId]: {
              lastDelayCallResult: CALL_RESULT_CODE.INCARCERATION
            } as CollectionForm
          }
        },
        storeId
      );
      expect(data).toBeTruthy();
    });

    it('should return false', () => {
      const data = useSelectorMock(
        selector.selectShowRemoveButton,
        { collectionForm: {} },
        storeId
      );
      expect(data).toBeFalsy();
    });
  });

  describe('selectRemoveButtonName', () => {
    it('should return txt_remove_hardship_status', () => {
      const data = useSelectorMock(
        selector.selectRemoveButtonName,
        {
          collectionForm: {
            [storeId]: {
              lastDelayCallResult: CALL_RESULT_CODE.HARDSHIP
            } as CollectionForm
          }
        },
        storeId
      );
      expect(data).toEqual(I18N_COLLECTION_FORM.REMOVE_HARDSHIP_STATUS);
    });

    it('should return txt_remove_deceased_status', () => {
      const data = useSelectorMock(
        selector.selectRemoveButtonName,
        {
          collectionForm: {
            [storeId]: {
              lastDelayCallResult: CALL_RESULT_CODE.DECEASE
            } as CollectionForm
          }
        },
        storeId
      );
      expect(data).toEqual(I18N_COLLECTION_FORM.REMOVE_DECEASED_STATUS);
    });

    it('should return txt_remove_bankruptcy_status', () => {
      const data = useSelectorMock(
        selector.selectRemoveButtonName,
        {
          collectionForm: {
            [storeId]: {
              lastDelayCallResult: CALL_RESULT_CODE.BANKRUPTCY
            } as CollectionForm
          }
        },
        storeId
      );
      expect(data).toEqual(I18N_COLLECTION_FORM.REMOVE_BANKRUPTCY_STATUS);
    });

    it('should return txt_remove_status', () => {
      const data = useSelectorMock(
        selector.selectRemoveButtonName,
        {
          collectionForm: {
            [storeId]: {
              lastDelayCallResult: CALL_RESULT_CODE.INCARCERATION
            } as CollectionForm
          }
        },
        storeId
      );
      expect(data).toEqual('txt_remove_status');
    });

    it('should return empty string', () => {
      const data = useSelectorMock(
        selector.selectRemoveButtonName,
        { collectionForm: {} },
        storeId
      );
      expect(data).toEqual('');
    });
  });

  it('selectCollectionFormMethod ', () => {
    const data = useSelectorMock(
      selector.selectCollectionFormMethod,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeUndefined();
  });

  it('selectCallResultsType ', () => {
    const data = useSelectorMock(
      selector.selectCallResultsType,
      { collectionForm: {} },
      storeId
    );
    expect(data).toEqual('');
  });

  it('selectMemoSectionCollapsed ', () => {
    const data = useSelectorMock(
      selector.selectMemoSectionCollapsed,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeUndefined();
  });

  it('takeDisabledFormStatus', () => {
    const data = useSelectorMock(
      selector.takeDisabledFormStatus,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('selectIsLoadingLastFollowUpData', () => {
    const data = useSelectorMock(
      selector.selectIsLoadingLastFollowUpData,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('selectIsLoadingCallResultRefData', () => {
    const data = useSelectorMock(
      selector.selectIsLoadingCallResultRefData,
      { collectionForm: {} },
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('takeCollectionLoadingStatus', () => {
    const data = useSelectorMock(
      selector.takeCollectionLoadingStatus,
      store,
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('takeCollectionLoadingStatus > isLoading is defined', () => {
    const isLoadingLastUploadStore = cloneDeep(store);
    set(isLoadingLastUploadStore, ['bankruptcy', storeId, 'isLoading'], true);

    const data = useSelectorMock(
      selector.takeCollectionLoadingStatus,
      isLoadingLastUploadStore,
      storeId
    );
    expect(data).toBeTruthy();
  });

  it('takeCollectionFormModalLoading ', () => {
    const data = useSelectorMock(
      selector.takeCollectionFormModalLoading,
      store,
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('takeInternalStatusCode ', () => {
    const data = useSelectorMock(
      selector.takeInternalStatusCode,
      store,
      storeId
    );
    expect(data).toEqual('123');
  });

  // ----------TALKING POINT SECTION ------------------

  it('selectTalkingPointData', () => {
    const data = useSelectorMock(
      selector.selectTalkingPointData,
      store,
      storeId
    );
    expect(data).toEqual('talking point');
  });

  it('selectTalkingPointLoading', () => {
    const data = useSelectorMock(
      selector.selectTalkingPointLoading,
      store,
      storeId
    );
    expect(data).toBeFalsy();
  });

  // ----------LAST 25 ACTIONS ------------------

  it('selectModalOpen', () => {
    const data = useSelectorMock(
      selector.last25ActionsSelector.selectModalOpen,
      store,
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('selectData', () => {
    const data = useSelectorMock(
      selector.last25ActionsSelector.selectData,
      store,
      storeId
    );
    expect(data).toEqual('last10Actions');
  });

  it('selectLoading', () => {
    const data = useSelectorMock(
      selector.last25ActionsSelector.selectLoading,
      store,
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('selectError', () => {
    const data = useSelectorMock(
      selector.last25ActionsSelector.selectError,
      store,
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('takeInProgressStatus', () => {
    const data = useSelectorMock(selector.takeInProgressStatus, store, storeId);
    expect(data).toBeFalsy();
  });

  it('selectDeceasedStatusCodeFromDeceasedDetailAPI', () => {
    const data = useSelectorMock(
      selector.selectDeceasedStatusCodeFromDeceasedDetailAPI,
      store,
      storeId
    );
    expect(data).toEqual(CALL_RESULT_CODE.DECEASE_PENDING);
  });

  it('selectIsHavingBankruptcyPending ', () => {
    const data = useSelectorMock(
      selector.selectIsHavingBankruptcyPending,
      store,
      storeId
    );
    expect(data).toBeTruthy();
  });

  it('selectIsHavingBankruptcyConfirm ', () => {
    const bankruptcyStore = cloneDeep(store);
    set(
      bankruptcyStore,
      ['bankruptcy', storeId, 'info', 'actionEntryCode'],
      CALL_RESULT_CODE.BANKRUPTCY
    );

    const data = useSelectorMock(
      selector.selectIsHavingBankruptcyConfirm,
      bankruptcyStore,
      storeId
    );

    expect(data).toBeTruthy();
  });

  it('selectBankruptcyActionEntryCode ', () => {
    const data = useSelectorMock(
      selector.selectBankruptcyActionEntryCode,
      store,
      storeId
    );
    expect(data).toEqual(CALL_RESULT_CODE.BANKRUPTCY_PENDING);
  });

  it('selectBankruptcyActionEntryCode ', () => {
    const data = useSelectorMock(
      selector.selectBankruptcyActionEntryCode,
      store,
      storeId
    );
    expect(data).toEqual(CALL_RESULT_CODE.BANKRUPTCY_PENDING);
  });

  it('selectIsHavingValidHardshipData ', () => {
    const hardShipStore = cloneDeep(store);
    set(
      hardShipStore,
      ['hardship', storeId, 'processStatus', 'processStatus'],
      HARDSHIP_COMPLETE
    );

    const data = useSelectorMock(
      selector.selectIsHavingValidHardshipData,
      hardShipStore,
      storeId
    );
    expect(data).toBeTruthy();
  });

  it('selectIsHavingHardshipData ', () => {
    const hardShipStore = cloneDeep(store);
    set(
      hardShipStore,
      ['hardship', storeId, 'data', 'processStatus'],
      HARDSHIP_COMPLETE
    );

    const data = useSelectorMock(
      selector.selectIsHavingHardshipData,
      hardShipStore,
      storeId
    );
    expect(data).toBeTruthy();
  });

  it('selectExistsHardshipData ', () => {
    const hardShipStore = cloneDeep(store);
    set(hardShipStore, ['hardship', storeId, 'data'], {});

    const data = useSelectorMock(
      selector.selectExistsHardshipData,
      hardShipStore,
      storeId
    );
    expect(data).toBeTruthy();
  });

  it('selectIsHavingLongTermMedical ', () => {
    const data = useSelectorMock(
      selector.selectIsHavingLongTermMedical,
      store,
      storeId
    );
    expect(data).toEqual(true);
  });

  it('isActiveLongTermMedical ', () => {
    const data = useSelectorMock(
      selector.isActiveLongTermMedical,
      store,
      storeId
    );
    expect(data).toBeTruthy();
  });

  it('selectIsHavingIncarceration ', () => {
    const data = useSelectorMock(
      selector.selectIsHavingIncarceration,
      store,
      storeId
    );
    expect(data).toBeTruthy();
  });

  it('selectIsHavingCCCSDataAcceptProposal ', () => {
    const data = useSelectorMock(
      selector.selectIsHavingCCCSDataAcceptProposal,
      store,
      storeId
    );
    expect(data).toBeTruthy();
  });
  //  ---------------------UPLOAD MODAL--------------------------------------

  it('selectUploadModal ', () => {
    const data = useSelectorMock(selector.selectUploadModal, store, storeId);
    expect(data).toBeTruthy();
  });

  it('selectUploadFileType ', () => {
    const data = useSelectorMock(selector.selectUploadFileType, store, storeId);
    expect(data).toEqual('hardShip');
  });

  it('selectViewDeathCertificate ', () => {
    const callResultEmptyStore = cloneDeep(store);
    set(
      callResultEmptyStore,
      ['collectionForm', storeId, 'lastDelayCallResult'],
      null
    );

    set(
      callResultEmptyStore,
      ['collectionForm', storeId, 'lastFollowUpData', 'callResultType'],
      CALL_RESULT_CODE.DECEASE
    );

    const data = useSelectorMock(
      selector.selectViewDeathCertificate,
      callResultEmptyStore,
      storeId
    );
    expect(data).toBeTruthy();
  });

  it('isActiveLongTermMedical', () => {
    const data = useSelectorMock(
      selector.isActiveLongTermMedical,
      store,
      storeId
    );
    expect(data).toBeTruthy();
  });

  it('getLoadingHardship', () => {
    const data = useSelectorMock(selector.getLoadingHardship, store, storeId);
    expect(data).toBeFalsy();
  });

  it('getLoadingPromiseToPay', () => {
    const data = useSelectorMock(
      selector.getLoadingPromiseToPay,
      store,
      storeId
    );
    expect(data).toBeTruthy();
  });

  it('getLoadingIncarceration', () => {
    const data = useSelectorMock(
      selector.getLoadingIncarceration,
      store,
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('getLoadingLongTerm ', () => {
    const data = useSelectorMock(selector.getLoadingLongTerm, store, storeId);
    expect(data).toBeFalsy();
  });

  it('selectLoadingForm with empty selectedCall', () => {
    const data = useSelectorMock(selector.selectLoadingForm, store, storeId);
    expect(data).toBeFalsy();
  });

  it('selectLoadingForm', () => {
    const data = useSelectorMock(
      selector.selectLoadingForm,
      emptyStore,
      storeId
    );
    expect(data).toBeTruthy();
  });
});
