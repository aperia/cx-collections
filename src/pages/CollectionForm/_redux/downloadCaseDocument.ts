import {
  DownloadCaseDocumentRequest,
  DownloadCaseDocumentArgs
} from '../types';
import { collectionFormService } from '../collectionFormServices';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { EMPTY_STRING } from 'app/constants';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

export const downloadCaseDocumentFile = createAsyncThunk<
  unknown,
  DownloadCaseDocumentArgs,
  ThunkAPIConfig
>('collectionForm/downloadDocumentsFile', async (args, thunkAPI) => {
  const { storeId, postData, type } = args;
  const { accountDetail } = thunkAPI.getState();
  const { fileName, uuid } = postData;
  const {
    clientIdentifier,
    systemIdentifier,
    principalIdentifier,
    agentIdentifier
  } = accountDetail[storeId]?.rawData || {};
  const { commonConfig } = window.appConfig || {};
  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;
  const requestData: DownloadCaseDocumentRequest = {
    common: {
      agent: agentIdentifier,
      clientNumber: clientIdentifier,
      system: systemIdentifier,
      prin: principalIdentifier,
      app: commonConfig?.app || EMPTY_STRING,
      user: commonConfig?.user,
      accountId: eValueAccountId
    },
    fileName,
    uuid
  };
  try {
    const response = await collectionFormService.downloadCaseDocument(
      requestData
    );
    if (response?.data?.responseStatus?.toLowerCase() !== 'success') {
      throw response;
    }
    const { file, fileName: responseFileName } = response?.data;
    const hrefFile = 'data:image/png;base64,' + file;
    const a = document.createElement('a');
    a.href = hrefFile;
    a.download = responseFileName;
    a.click();
  } catch (error) {
    thunkAPI.dispatch(
      actionsToast.addToast({
        message:
          type === 'bankruptcyDocument'
            ? 'txt_bankruptcy_documents_failed_to_download'
            : 'txt_death_certificate_failed_to_download',
        type: 'error'
      })
    );
    return thunkAPI.rejectWithValue({ response: error });
  }
});
