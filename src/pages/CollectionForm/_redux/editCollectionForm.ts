import { CALL_RESULT_CODE } from '../constants';
import { collectionFormActions } from './reducers';

export const triggerEditCollectionForm: AppThunk = () => (
  dispatch,
  getState
) => {
  const state = getState();
  const storeId = state.tab?.tabStoreIdSelected;
  const callResultCode =
    state.collectionForm[storeId]?.lastFollowUpData?.callResultType;

  const {
    setCurrentView,
    selectCallResult,
    toggleModal
  } = collectionFormActions;

  const {
    PROMISE_TO_PAY,
    HARDSHIP,
    LONG_TERM_MEDICAL,
    INCARCERATION
  } = CALL_RESULT_CODE;

  if (
    callResultCode === PROMISE_TO_PAY ||
    callResultCode === HARDSHIP ||
    callResultCode === LONG_TERM_MEDICAL ||
    callResultCode === INCARCERATION
  ) {
    dispatch(setCurrentView({ storeId, view: callResultCode }));
    return;
  }

  dispatch(
    selectCallResult({
      storeId,
      item: undefined
    })
  );

  dispatch(toggleModal({ storeId }));
};
