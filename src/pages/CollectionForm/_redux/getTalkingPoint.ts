import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { collectionFormService } from '../collectionFormServices';
import { CollectionFormState, GetTalkingPointArgs } from '../types';

export const getTalkingPoint = createAsyncThunk<
  string,
  GetTalkingPointArgs,
  ThunkAPIConfig
>('collectionForm/getTalkingPoint', async (args, thunkAPI) => {
  const { accEValue } = args;

  const { app = '' } = window.appConfig?.commonConfig || {};

  const {
    data: { panelContentText = '' }
  } = await collectionFormService.getTalkingPoint({
    common: {
      app,
      accountId: accEValue,
    }
  });
  return panelContentText;
});

export const getTalkingPointBuilder = (
  builder: ActionReducerMapBuilder<CollectionFormState>
) => {
  builder
    .addCase(getTalkingPoint.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        talkingPoint: { data: '', loading: true }
      };
    })
    .addCase(getTalkingPoint.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        talkingPoint: {
          loading: false,
          data: action.payload
        }
      };
    })
    .addCase(getTalkingPoint.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        talkingPoint: { data: '', loading: false }
      };
    });
};
