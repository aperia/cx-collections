import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { triggerEditCollectionForm } from './editCollectionForm';
import { storeId } from 'app/test-utils';
import { CALL_RESULT_CODE } from '../constants';
import { collectionFormActions } from './reducers';

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('Testing trigger collection form', () => {
  describe('async thunk', () => {
    it('with state', async () => {
      const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));

      spy = jest
        .spyOn(collectionFormActions, 'setCurrentView')
        .mockImplementation(mockAction);

      const store = createStore(rootReducer, {
        collectionForm: {
          [storeId]: {
            lastFollowUpData: { callResultType: CALL_RESULT_CODE.HARDSHIP }
          }
        },
        tab: { tabStoreIdSelected: storeId }
      });

      await triggerEditCollectionForm({
        storeId,
        postData: { accountId: storeId }
      })(store.dispatch, store.getState, {});

      expect(mockAction).toBeCalledWith({
        storeId,
        view: CALL_RESULT_CODE.HARDSHIP
      });
    });

    it('with empty state', async () => {
      const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));

      spy = jest
        .spyOn(collectionFormActions, 'selectCallResult')
        .mockImplementation(mockAction);

      const store = createStore(rootReducer, {
        tab: { tabStoreIdSelected: storeId }
      });

      await triggerEditCollectionForm({
        storeId,
        postData: { accountId: storeId }
      })(store.dispatch, store.getState, {});

      expect(mockAction).toBeCalledWith({ storeId, item: undefined });
    });
  });
});
