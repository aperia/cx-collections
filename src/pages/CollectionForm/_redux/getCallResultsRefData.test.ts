import axios from 'axios';
import './reducers';
import { applyMiddleware, createStore } from '@reduxjs/toolkit';
import { getCallResultsRefData } from './getCallResultsRefData';
import { storeId, responseDefault } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import thunk from 'redux-thunk';
import { collectionFormService } from '../collectionFormServices';

describe('Test getCallResultsRefData', () => {
  const mockArgs = { storeId, postData: { accountId: storeId } };

  const initialState: Partial<RootState> = {
    accountDetail: {
      [storeId]: {
        data: {
          agentId: 'agentId',
          sysId: 'sysId',
          principalId: 'principalId',
          clientID: 'clientID'
        }
      }
    },
    mapping: {
      loading: false,
      data: { callResultRef: { code: 'actionCode' } }
    }
  };

  const store = createStore(rootReducer, initialState, applyMiddleware(thunk));

  beforeEach(() => {
    window.appConfig = {
      commonConfig: {
        app: 'app'
      } as CommonConfig
    } as AppConfiguration;
  });

  const fulfilledType = 'collectionForm/getCallResultsRefData/fulfilled';
  const rejectedType = 'collectionForm/getCallResultsRefData/rejected';

  const callAction = async (reduxStore = store) => {
    return getCallResultsRefData(mockArgs)(
      reduxStore.dispatch,
      reduxStore.getState,
      {}
    );
  };

  it('getCallResultsRefData fulfilled', async () => {
    window.appConfig = undefined as unknown as AppConfiguration;

    jest.spyOn(axios, 'get').mockResolvedValue({
      ...responseDefault,
      data: [{ actionCode: 'BC' }]
    });

    jest
      .spyOn(collectionFormService, 'getCallResultsRefData')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          clientInfo: { actionCodes: [{ active: true, actionCode: 'DC' }] }
        }
      });
    const response = await callAction();

    expect(response.type).toEqual(fulfilledType);
    expect(response.payload).toEqual({
      data: [{ code: 'DC' }, { code: 'BC' }],
      customCallResult: [{ code: 'DC' }],
      defaultCallResult: [{ code: 'BC' }]
    });
  });

  it('getCallResultsRefData fulfilled > empty state and empty data', async () => {
    window.appConfig = undefined as unknown as AppConfiguration;

    const emptyStore = createStore(
      rootReducer,
      { accountDetail: { [storeId]: {} }, mapping: { data: {} } },
      applyMiddleware(thunk)
    );

    jest.spyOn(axios, 'get').mockResolvedValue(responseDefault);
    jest
      .spyOn(collectionFormService, 'getCallResultsRefData')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const response = await callAction(emptyStore);

    expect(response.type).toEqual(fulfilledType);
    expect(response.payload).toEqual({
      data: [],
      customCallResult: [],
      defaultCallResult: []
    });
  });

  it('getCallResultsRefData fulfilled > axios rejected', async () => {
    window.appConfig = undefined as unknown as AppConfiguration;

    jest.spyOn(axios, 'get').mockResolvedValue({
      ...responseDefault,
      data: [{ actionCode: 'BC' }]
    });
    jest
      .spyOn(collectionFormService, 'getCallResultsRefData')
      .mockRejectedValue({});

    const response = await callAction();

    expect(response.type).toEqual(fulfilledType);
    expect(response.payload).toEqual({
      data: [{ code: 'BC' }],
      customCallResult: [],
      defaultCallResult: [{ code: 'BC' }]
    });
  });

  it('getCallResultsRefData fulfilled > getCallResultsRefData rejected', async () => {
    window.appConfig = undefined as unknown as AppConfiguration;

    jest.spyOn(axios, 'get').mockRejectedValue({});
    jest
      .spyOn(collectionFormService, 'getCallResultsRefData')
      .mockResolvedValue({
        ...responseDefault,

        data: {
          clientInfo: { actionCodes: [{ active: true, actionCode: 'DC' }] }
        }
      });
    const response = await callAction();

    expect(response.type).toEqual(fulfilledType);
    expect(response.payload).toEqual({
      data: [{ code: 'DC' }],
      customCallResult: [{ code: 'DC' }],
      defaultCallResult: []
    });

    jest
      .spyOn(collectionFormService, 'getCallResultsRefData')
      .mockRejectedValue({});

    const response2 = await getCallResultsRefData(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response2.type).toEqual(
      'collectionForm/getCallResultsRefData/rejected'
    );
    expect(response2.payload).toEqual({});
  });

  it('getCallResultsRefData rejected', async () => {
    jest.spyOn(axios, 'get').mockRejectedValue({});
    jest
      .spyOn(collectionFormService, 'getCallResultsRefData')
      .mockRejectedValue({});

    const response = await callAction();

    expect(response.payload).toEqual({});
    expect(response.type).toEqual(rejectedType);
  });
});
