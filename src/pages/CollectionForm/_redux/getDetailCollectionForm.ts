import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { hardshipActions } from 'pages/HardShip/_redux/reducers';
import { CALL_RESULT_CODE } from '../constants';
import { collectionFormService } from '../collectionFormServices';
import {
  CollectionFormDetailArg,
  CollectionFormDetailPayload,
  CollectionFormState
} from '../types';

export const getDetailCollectionForm = createAsyncThunk<
  CollectionFormDetailPayload,
  CollectionFormDetailArg,
  ThunkAPIConfig
>('collectionForm/getDetailCollectionForm', async (args, thunkAPI) => {
  const { dispatch } = thunkAPI;
  const state = thunkAPI.getState();
  const storeId = state.tab.tabStoreIdSelected;

  const requestData = args.postData;
  const { callResultType } = requestData;

  const currentCallResultType = callResultType
    ? callResultType
    : state.collectionForm[storeId]?.selectedCallResult?.code ||
      state.collectionForm[storeId]?.lastFollowUpData?.callResultType;
  const eValueAccountId = state.accountDetail[storeId]?.data?.eValueAccountId;
  const { data } = await collectionFormService.getDetailCallResultType({
    common: { accountId: eValueAccountId },
    callResultType: currentCallResultType
  });

  if (data.callResultType === CALL_RESULT_CODE.HARDSHIP) {
    const currentBalance =
      state.accountDetail[storeId]?.data?.monetary?.currentBalance;
    dispatch(
      hardshipActions.updateData({
        storeId,
        data: { ...data?.data, currentBalance }
      })
    );
  }

  return data;
});

export const getDetailCollectionFormBuilder = (
  builder: ActionReducerMapBuilder<CollectionFormState>
) => {
  builder
    .addCase(getDetailCollectionForm.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId]
      };
    })
    .addCase(getDetailCollectionForm.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId]
      };
    })
    .addCase(getDetailCollectionForm.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId]
      };
    });
};
