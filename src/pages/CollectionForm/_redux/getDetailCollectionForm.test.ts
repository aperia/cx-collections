import './reducers';
import { getDetailCollectionForm } from './getDetailCollectionForm';
import { createStore, Store } from '@reduxjs/toolkit';
import { rootReducer } from 'storeConfig';

import { collectionFormService } from '../collectionFormServices';
import { responseDefault, storeId } from 'app/test-utils';
import { CALL_RESULT_CODE } from '../constants';
import { hardshipActions } from 'pages/HardShip/_redux/reducers';

const postData = {
  callResultType: 'DECEASED_PENDING',
  data: {}
};

let spy: jest.SpyInstance;
let spy2: jest.SpyInstance;
let store: Store<RootState>;

beforeEach(() => {
  store = createStore(rootReducer, { tab: { tabStoreIdSelected: storeId } });
});

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
  spy2?.mockReset();
  spy2?.mockRestore();
});

describe('get Detail Collection Form thunk', () => {
  describe('success', () => {
    describe('with callResultType', () => {
      it('callResultType is HARDSHIP', async () => {
        // mock collectionFormService.getDetailCallResultType()
        const mockPayload = {
          callResultType: CALL_RESULT_CODE.HARDSHIP,
          data: {}
        };
        const mockAction = jest
          .fn()
          .mockResolvedValue({ ...responseDefault, data: mockPayload });
        spy = jest
          .spyOn(collectionFormService, 'getDetailCallResultType')
          .mockImplementation(mockAction);

        // mock hardshipActions.updateData()
        const mockUpdateData = jest
          .fn()
          .mockImplementation(() => ({ type: 'type' }));
        spy2 = jest
          .spyOn(hardshipActions, 'updateData')
          .mockImplementation(mockUpdateData);

        await getDetailCollectionForm({
          storeId,
          postData
        })(store.dispatch, store.getState, {});

        expect(mockUpdateData).toBeCalledWith({
          storeId,
          data: mockPayload.data
        });
      });

      it('callResultType is CCCS', async () => {
        // mock collectionFormService.getDetailCallResultType()
        const mockPayload = {
          callResultType: CALL_RESULT_CODE.CCCS,
          data: {}
        };
        const mockAction = jest
          .fn()
          .mockResolvedValue({ ...responseDefault, data: mockPayload });
        spy = jest
          .spyOn(collectionFormService, 'getDetailCallResultType')
          .mockImplementation(mockAction);

        // mock hardshipActions.updateData()
        const mockUpdateData = jest
          .fn()
          .mockImplementation(() => ({ type: 'type' }));
        spy2 = jest
          .spyOn(hardshipActions, 'updateData')
          .mockImplementation(mockUpdateData);

        await getDetailCollectionForm({
          storeId,
          postData
        })(store.dispatch, store.getState, {});

        expect(mockUpdateData).toBeCalledTimes(0);
      });
    });

    it('without callResultType', async () => {
      const mockPayload = { data: 'mockData' };
      const mockAction = jest
        .fn()
        .mockResolvedValue({ ...responseDefault, data: mockPayload });
      spy = jest
        .spyOn(collectionFormService, 'getDetailCallResultType')
        .mockImplementation(mockAction);

      const response = await getDetailCollectionForm({
        storeId,
        postData: {}
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual(mockPayload);
    });
  });

  it('reject', async () => {
    const mockAction = jest
      .fn()
      .mockRejectedValue({ ...responseDefault, data: {} });
    spy = jest
      .spyOn(collectionFormService, 'getDetailCallResultType')
      .mockImplementation(mockAction);

    const response = await getDetailCollectionForm({
      storeId,
      postData
    })(store.dispatch, store.getState, {});

    expect(response.payload).toBeUndefined();
  });
});
