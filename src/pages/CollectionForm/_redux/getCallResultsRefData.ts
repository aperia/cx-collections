import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

// Const
import {
  EMPTY_ARRAY,
  EMPTY_OBJECT,
  EMPTY_STRING,
  FULFILLED,
  REJECTED
} from 'app/constants';
import { CALL_REF_DATA } from '../constants';

// Type
import { AccountInfo } from 'pages/AccountDetails/types';
import {
  CallResult,
  CollectionFormState,
  GetCallResultsRefDataArgs,
  GetCallResultsRefDataPayload
} from '../types';

// Service
import { collectionFormService } from '../collectionFormServices';

// Helpers
import { mappingDataFromArray } from 'app/helpers';

export const getCallResultsRefData = createAsyncThunk<
  GetCallResultsRefDataPayload,
  GetCallResultsRefDataArgs,
  ThunkAPIConfig
>('collectionForm/getCallResultsRefData', async (args, thunkAPI) => {
  const { getState, rejectWithValue } = thunkAPI;
  const { mapping, accountDetail } = getState();
  const { storeId } = args;
  const { commonConfig } = window.appConfig || {};

  const {
    agentId = EMPTY_STRING,
    sysId = EMPTY_STRING,
    principalId = EMPTY_STRING,
    clientID = EMPTY_STRING
  } = accountDetail[storeId]?.data || (EMPTY_OBJECT as AccountInfo);

  const clientCSPA = `${clientID}${sysId}${principalId}${agentId}`;

  const requestData = {
    common: {
      agent: agentId,
      app: commonConfig?.app || EMPTY_STRING,
      clientNumber: clientID,
      prin: principalId,
      system: sysId,
      org: window?.appConfig?.commonConfig?.org
    },
    clientInfoId: clientCSPA,
    enableFallback: true
  };

  const { callResultRef = EMPTY_OBJECT } = mapping.data;

  const response = await Promise.allSettled([
    axios.get(CALL_REF_DATA),
    collectionFormService.getCallResultsRefData(requestData)
  ]);

  const [standardResponse, customResponse] = response;

  if (
    standardResponse.status === REJECTED &&
    customResponse.status === REJECTED
  ) {
    return rejectWithValue({});
  }

  let standardData: CallResult[] = EMPTY_ARRAY;
  if (standardResponse.status === FULFILLED) {
    standardData = mappingDataFromArray<CallResult>(
      standardResponse.value.data || EMPTY_ARRAY,
      callResultRef
    );
  }

  let customData: CallResult[] = EMPTY_ARRAY;
  if (customResponse.status === FULFILLED) {
    customData = mappingDataFromArray<CallResult>(
      customResponse.value.data.clientInfo?.actionCodes?.filter(
        item => item.active
      ) || EMPTY_ARRAY,
      callResultRef
    );
  }

  const standardCode = standardData.map(item => item.code);

  const customDataRemovedDuplicate = customData.filter(
    item => !standardCode.includes(item.code)
  );

  return {
    data: [...customDataRemovedDuplicate, ...standardData],
    customCallResult: customData,
    defaultCallResult: standardData
  };
});

export const getCallResultsRefRequestDataBuilder = (
  builder: ActionReducerMapBuilder<CollectionFormState>
) => {
  builder
    .addCase(getCallResultsRefData.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingCallResultRefData: true
      };
    })
    .addCase(getCallResultsRefData.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data, defaultCallResult, customCallResult } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingCallResultRefData: false,
        callResultRefData: data,
        customCallResult,
        defaultCallResultRefData: defaultCallResult
      };
    })
    .addCase(getCallResultsRefData.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingCallResultRefData: false
      };
    });
};
