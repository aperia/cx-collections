import { CALL_RESULT_CODE } from '../constants';
import { collectionFormActions } from './reducers';

export const triggerStartCollectionForm: AppThunk =
  () => (dispatch, getState) => {
    const state = getState();
    const storeId = state.tab?.tabStoreIdSelected;
    const callResultCode =
      state.collectionForm[storeId]?.selectedCallResult?.code;

    if (
      callResultCode === CALL_RESULT_CODE.PROMISE_TO_PAY ||
      callResultCode === CALL_RESULT_CODE.HARDSHIP ||
      callResultCode === CALL_RESULT_CODE.LONG_TERM_MEDICAL ||
      callResultCode === CALL_RESULT_CODE.INCARCERATION ||
      callResultCode === CALL_RESULT_CODE.SETTLEMENT
    ) {
      dispatch(
        collectionFormActions.setCurrentView({ storeId, view: callResultCode })
      );
      return;
    }

    dispatch(collectionFormActions.toggleModal({ storeId }));
  };
