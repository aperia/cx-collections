import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Types
import {
  CallResultSelectPayload,
  ChangeMethodPayloadAction,
  CollectionFormState,
  CollectionViewNamePayload,
  DisabledOkPayload,
  InProgressInfoPayload,
  UploadFileType
} from '../types';

// Actions
import { triggerEditCollectionForm } from './editCollectionForm';
import {
  getCallResultsRefRequestDataBuilder,
  getCallResultsRefData
} from './getCallResultsRefData';
import {
  getDetailCollectionFormBuilder,
  getDetailCollectionForm
} from './getDetailCollectionForm';
import { getTalkingPointBuilder, getTalkingPoint } from './getTalkingPoint';
import {
  getLatestActionRequest,
  getLatestActionRequestBuilder
} from './getLatestAction';
import {
  getLast25ActionsBuilder,
  getLast25ActionsRequest
} from './getLast25Actions';

import {
  updateActionEntryWorkFlow,
  updateActionEntryWorkFlowBuilder
} from './updateActionEntryWorkFlow';

import {
  getListCaseDocument,
  getListCaseDocumentBuilder
} from './getListCaseDocuments';

import { downloadCaseDocumentFile } from './downloadCaseDocument';

// Helper
import { getDelayProgressRawData } from '../helpers';

const storeData = getDelayProgressRawData();

const { actions, reducer } = createSlice({
  name: 'collectionForm',
  initialState: storeData as CollectionFormState,
  reducers: {
    selectCallResult: (
      draftState,
      action: PayloadAction<CallResultSelectPayload>
    ) => {
      const { storeId, item } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        selectedCallResult: item
      };
    },
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    toggleModal: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isOpenModal: !draftState[storeId]?.isOpenModal
      };
    },
    setCurrentView: (
      draftState,
      action: PayloadAction<StoreIdPayload & { view?: string }>
    ) => {
      const { storeId, view } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        currentView: view ?? 'default'
      };
    },
    setDirtyForm: (
      draftState,
      action: PayloadAction<
        StoreIdPayload & { callResult: string; value: boolean }
      >
    ) => {
      const { storeId, callResult, value } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        dirtyForm: { ...draftState[storeId].dirtyForm, [callResult]: value }
      };
    },
    clearSelectedCallResult: (
      draffState,
      action: PayloadAction<StoreIdPayload>
    ) => {
      const { storeId } = action.payload;
      delete draffState[storeId]['selectedCallResult'];
    },
    setViewName: (
      draffState,
      action: PayloadAction<CollectionViewNamePayload>
    ) => {
      const { storeId, viewName } = action.payload;
      draffState[storeId] = {
        ...draffState[storeId],
        viewName: viewName || []
      };
    },
    toggleMemoSection: (draffState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      draffState[storeId].isMemoSectionCollapsed =
        !draffState[storeId].isMemoSectionCollapsed;
    },
    changeDisabledOk: (
      draftState,
      action: PayloadAction<DisabledOkPayload>
    ) => {
      const { storeId, disabledOk } = action.payload;
      draftState[storeId].disabledOk = disabledOk;
    },
    changeMethod: (
      draftState,
      action: PayloadAction<ChangeMethodPayloadAction>
    ) => {
      const { storeId, method } = action.payload;
      draftState[storeId].method = method;
    },
    toggleLast10Actions: (
      draftState,
      action: PayloadAction<{ storeId: string }>
    ) => {
      const { storeId } = action.payload;
      const currentState = draftState[storeId]?.last10Actions?.isOpen;
      draftState[storeId].last10Actions = {
        ...draftState[storeId].last10Actions,
        isOpen: !currentState
      };
    },
    toggleCallResultInProgressStatus: (
      draftState,
      action: PayloadAction<InProgressInfoPayload>
    ) => {
      const { storeId, info, lastDelayCallResult } = action.payload;
      const {
        isInProgress = false,
        lastDelayCallResult: currentDelayCallResult
      } = draftState[storeId];
      draftState[storeId] = {
        ...draftState[storeId],
        isInProgress: !isInProgress,
        inProgressInfo: info,
        lastFollowUpData: {
          callResultType: !isInProgress
            ? lastDelayCallResult
            : currentDelayCallResult
        },
        lastDelayCallResult: lastDelayCallResult ?? currentDelayCallResult
      };
    },
    removeLastDelayCallResult: (
      draftState,
      action: PayloadAction<StoreIdPayload>
    ) => {
      const { storeId } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        lastDelayCallResult: undefined
      };
    },
    changeLastFollowUpData: (
      draftState,
      action: PayloadAction<{
        storeId: string;
        callResultType?: string;
      }>
    ) => {
      const { storeId, callResultType } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        lastFollowUpData: {
          callResultType: callResultType ? callResultType : undefined
        }
      };
    },
    openUploadFileModal: (
      draftState,
      action: PayloadAction<{
        storeId: string;
        type: UploadFileType;
      }>
    ) => {
      const { storeId, type } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        uploadFiles: {
          type,
          openModal: true
        }
      };
    },
    closeUploadFileModal: (
      draftState,
      action: PayloadAction<{
        storeId: string;
      }>
    ) => {
      const { storeId } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        uploadFiles: {
          openModal: false
        }
      };
    }
  },
  extraReducers: builder => {
    getCallResultsRefRequestDataBuilder(builder);
    getDetailCollectionFormBuilder(builder);
    getTalkingPointBuilder(builder);
    getLatestActionRequestBuilder(builder);
    getLast25ActionsBuilder(builder);
    updateActionEntryWorkFlowBuilder(builder);
    getListCaseDocumentBuilder(builder);
  }
});

const collectionFormActions = {
  ...actions,
  getLatestActionRequest,
  getDetailCollectionForm,
  getCallResultsRefData,
  getTalkingPoint,
  triggerEditCollectionForm,
  updateActionEntryWorkFlow,
  getLast25ActionsRequest,
  getListCaseDocument,
  downloadCaseDocumentFile
};

export { collectionFormActions, reducer };
