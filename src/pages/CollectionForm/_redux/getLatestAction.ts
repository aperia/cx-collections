import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { collectionFormService } from '../collectionFormServices';
import { ACTION_ENTRY } from '../constants';
import {
  CollectionFormState,
  GetLatestActionArgs,
  GetLatestActionPayload
} from '../types';

export const getLatestActionRequest = createAsyncThunk<
  GetLatestActionPayload,
  GetLatestActionArgs,
  ThunkAPIConfig
>('collectionForm/getLatestAction', async (args, thunkAPI) => {
  const { storeId } = args;
  const { accountDetail } = thunkAPI.getState();
  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;
  const requestBody = {
    common: {
      accountId: eValueAccountId
    },
    selectFields: ['actionEntryPrefixCode']
  };

  const { data } = await collectionFormService.getLatestAction(requestBody);

  const filterActionCode = data?.actionEntryFields.filter(item =>
    item.actionEntryPrefixCode.trim()
  );

  const latestCallResultCode = filterActionCode?.[0]?.actionEntryPrefixCode;

  return { latestCallResultCode };
});

export const getLatestActionRequestBuilder = (
  builder: ActionReducerMapBuilder<CollectionFormState>
) => {
  builder.addCase(getLatestActionRequest.pending, (draftState, action) => {
    const { storeId } = action.meta.arg;
    draftState[storeId] = {
      ...draftState[storeId],
      isLoadingLastFollowUp: true
    };
  });
  builder.addCase(getLatestActionRequest.fulfilled, (draftState, action) => {
    const { storeId } = action.meta.arg;
    const { latestCallResultCode = '' } = action.payload;
    draftState[storeId] = {
      ...draftState[storeId],
      lastFollowUpData: {
        callResultType: ACTION_ENTRY[latestCallResultCode]
      },
      isLoadingLastFollowUp: false
    };
  });
  builder.addCase(getLatestActionRequest.rejected, (draftState, action) => {
    const { storeId } = action.meta.arg;
    draftState[storeId] = {
      ...draftState[storeId],
      isLoadingLastFollowUp: false
    };
  });
};
