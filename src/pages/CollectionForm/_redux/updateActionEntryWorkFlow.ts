import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// Service
import { collectionFormService } from '../collectionFormServices';

// Type
import { CollectionFormState } from '../types';

// Redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { collectionFormActions } from './reducers';
import { batch } from 'react-redux';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export const updateActionEntryWorkFlow = createAsyncThunk<
  unknown,
  StoreIdPayload,
  ThunkAPIConfig
>('collectionForm/updateActionEntryWorkFlow', async (args, thunkAPI) => {
  const { dispatch, rejectWithValue, getState } = thunkAPI;
  const { storeId } = args;
  const { commonConfig } = window.appConfig || {};
  const { collectionForm, accountDetail } = getState();
  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;
  const requestData = {
    common: {
      app: commonConfig?.app,
      org: commonConfig?.org,
      accountId: eValueAccountId
    },
    callingApplication: commonConfig?.callingApplicationApr01 || '',
    operatorCode: commonConfig?.operatorID,
    cardholderContactedName:
      accountDetail[storeId]?.selectedAuthenticationData
        ?.customerNameForCreateWorkflow,
    cardholderContactedPhoneNumber:
      accountDetail[storeId]?.numberPhoneAuthenticated,
    actionCode: collectionForm[storeId].selectedCallResult?.code
  };
  try {
    const response = await collectionFormService.updateActionEntryWorkFlow(
      requestData
    );

    if (response?.data?.status !== 'success') {
      throw response;
    }

    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'txt_call_result_submitted'
        })
      );
      dispatch(
        collectionFormActions.getLatestActionRequest({
          storeId
        })
      );
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inCollectionFormFlyOut'
        })
      );
    });
  } catch (error) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: 'txt_call_result_failed_submit'
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inCollectionFormFlyOut',
          apiResponse: error
        })
      );
    });
    return rejectWithValue(error);
  }
});

export const updateActionEntryWorkFlowBuilder = (
  builder: ActionReducerMapBuilder<CollectionFormState>
) => {
  builder
    .addCase(updateActionEntryWorkFlow.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingCallResultRefData: true
      };
    })
    .addCase(updateActionEntryWorkFlow.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingCallResultRefData: false
      };
    })
    .addCase(updateActionEntryWorkFlow.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingCallResultRefData: false
      };
    });
};
