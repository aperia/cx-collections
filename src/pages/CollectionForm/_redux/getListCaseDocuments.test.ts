import { collectionFormActions } from './reducers';
import { collectionFormService } from '../collectionFormServices';
import {
  createStoreWithDefaultMiddleWare,
  responseDefault,
  storeId
} from 'app/test-utils';
import { isFulfilled, isRejected } from '@reduxjs/toolkit';
import { UploadFileType } from '../types';

describe('downloadCaseDocumentFile', () => {
  it('success api', async () => {
    jest
      .spyOn(collectionFormService, 'getListCaseDocuments')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          responseStatus: 'success',
          file: 'thisisafile',
          fileName: 'fileName'
        }
      });

    const store = createStoreWithDefaultMiddleWare({
      accountDetail: {
        [storeId]: {
          rawData: {
            clientIdentifier: 'clientIdentifier',
            systemIdentifier: 'systemIdentifier',
            principalIdentifier: 'principalIdentifier',
            agentIdentifier: 'agentIdentifier'
          }
        }
      }
    });

    const requestBody = {
      storeId,
      accountId: storeId,
      type: 'deathCertificate' as UploadFileType
    };

    const response = await collectionFormActions.getListCaseDocument(
      requestBody
    )(store.dispatch, store.getState, {});

    expect(isFulfilled(response)).toBeTruthy();
  });
  it('failed API', async () => {
    jest
      .spyOn(collectionFormService, 'getListCaseDocuments')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          responseStatus: 'failed',
          file: 'thisisafile',
          fileName: 'fileName'
        }
      });

    const store = createStoreWithDefaultMiddleWare({
      accountDetail: {}
    });

    const requestBody = {
      storeId,
      accountId: storeId,
      type: 'deathCertificate' as UploadFileType
    };

    const response = await collectionFormActions.getListCaseDocument(
      requestBody
    )(store.dispatch, store.getState, {});

    expect(isRejected(response)).toBeTruthy();
  });
});
