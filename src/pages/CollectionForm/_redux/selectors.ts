import { createSelector } from '@reduxjs/toolkit';
// Constants
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import isEmpty from 'lodash.isempty';
import isNil from 'lodash.isnil';
import { getAccountDetailInternalStatusCode } from 'pages/AccountDetails/_redux/selectors';
// Selectors
import {
  getBankruptcyLoadingStatus,
  getBankruptcyUpdateStatus
} from 'pages/Bankruptcy/_redux/selectors';
import { CCCS_ACTIONS } from 'pages/CCCS/constants';
import {
  selectGetDeceasedInfoLoading,
  selectUpdateDeceasedLoading
} from 'pages/Deceased/_redux/selectors';
import { isInvalid } from 'redux-form';
import {
  callResultItems,
  CALL_RESULT_CODE,
  DECEASED_STATUS_MAPPING
} from '../constants';
import { CallResult } from '../types';
import {
  HARDSHIP_COMPLETE,
  HARDSHIP_IN_PROGRESS
} from './../../HardShip/constants';

declare type Loading = boolean | undefined;

const selectedCallResultLoading = ['ME', 'FD', 'HS', 'PP'];

export const getCallResultType = (state: RootState, storeId: string) => {
  const { lastFollowUpData, lastDelayCallResult } =
    state.collectionForm[storeId] || {};

  return lastDelayCallResult ?? lastFollowUpData?.callResultType ?? '';
};

export const getLoadingHardship = (
  states: RootState,
  storeId: string
): Loading => states.hardship[storeId]?.isLoading;

export const getLoadingPromiseToPay = (
  states: RootState,
  storeId: string
): Loading => states.promiseToPay[storeId]?.loading;

export const getLoadingIncarceration = (
  states: RootState,
  storeId: string
): Loading => states.incarceration[storeId]?.isLoading;

export const getLoadingLongTerm = (
  states: RootState,
  storeId: string
): Loading => states.longTermMedical[storeId]?.isLoading;

export const getListCallResultRefData = (state: RootState, storeId: string) =>
  state.collectionForm[storeId]?.callResultRefData || [];

export const getDefaultListCallResultRefData = (
  state: RootState,
  storeId: string
) => state.collectionForm[storeId]?.defaultCallResultRefData || [];

export const getSelectedCallResult = (state: RootState, storeId: string) =>
  state.collectionForm[storeId]?.selectedCallResult;

export const getCollectionResultData = (state: RootState, storeId: string) =>
  state.collectionForm[storeId]?.lastFollowUpData;

// Open form status
export const getOpenFormStatus = (states: RootState, storeId: string) =>
  states.collectionForm[storeId]?.isOpenModal || false;

// Get collection form data
export const getCollectionFormData = (state: RootState, storeId: string) =>
  state.collectionForm[storeId]?.collectionCurrentData;

// Get collection form method
export const getCollectionFormMethod = (state: RootState, storeId: string) =>
  state.collectionForm[storeId]?.method;

export const getCurrentView = (state: RootState, storeId: string) =>
  state.collectionForm[storeId]?.currentView;

export const getDirtyForm = (state: RootState, storeId: string) =>
  state.collectionForm[storeId]?.dirtyForm;

export const getMemoSectionCollapsed = (state: RootState, storeId: string) =>
  state.collectionForm[storeId]?.isMemoSectionCollapsed;

export const getTalkingPoint = (state: RootState, storeId: string) =>
  state.collectionForm[storeId]?.talkingPoint;

export const getDisabledFormStatus = (state: RootState, storeId: string) => {
  const views = state.collectionForm[storeId]?.viewName || [];
  const invalidData = views.map(item => {
    return isInvalid(item)(state);
  });

  return (
    invalidData.some(item => item === true) ||
    state.collectionForm[storeId]?.disabledOk ||
    false
  );
};

export const getLoadingLatestCollectionInfo = (
  state: RootState,
  storeId: string
) => state.collectionForm[storeId]?.isLoadingLatestCollectionInfo || false;

export const selectCallResultRefData = createSelector(
  getListCallResultRefData,
  data => data
);

export const takeDefaultCallResultRefData = createSelector(
  getDefaultListCallResultRefData,
  data => data
);

export const selectIsHavingData = createSelector(
  getCollectionResultData,
  lastFollowUpData => {
    return !isNil(lastFollowUpData?.callResultType);
  }
);

export const selectCallResult = createSelector(
  getCallResultType,
  (callResultType: string) => callResultType
);

export const selectLabelCallResult = createSelector(
  (states: RootState, storeId: string) => {
    const currentType =
      states.collectionForm[storeId]?.lastFollowUpData?.callResultType;
    if (!currentType) return '';
    const callResult = callResultItems.find(
      item => item.uniqueId === currentType
    );
    return callResult ? callResult.label : '';
  },
  (callResultsType: string) => callResultsType
);

export const takeSelectedCallResult = createSelector(
  getSelectedCallResult,
  data => data
);

export const takeCollectionResultData = createSelector(
  getCollectionResultData,
  result => result?.data ?? {}
);

export const selectLastFollowUpData = createSelector(
  [getCollectionResultData],
  result => result ?? null
);

export const takeOpenFormStatus = createSelector(
  getOpenFormStatus,
  (opened: boolean) => opened
);

export const selectCollectionStatusCCCS = createSelector(
  getCollectionFormData,
  data => data?.action
);

export const takeCollectionFormData = createSelector(
  getCollectionFormData,
  data => data
);

export const takeLoadingLatestCollectionInfo = createSelector(
  getLoadingLatestCollectionInfo,
  data => data
);

export const selectCollapseMemoStatus = createSelector(
  (states: RootState, storeId: string) =>
    states.collectionForm[storeId]?.isMemoSectionCollapsed || false,
  (collapsed: boolean) => collapsed
);

export const takeCollectionFormLoadingStatus = createSelector(
  getLoadingLatestCollectionInfo,
  isLoadingLatestInfo => isLoadingLatestInfo
);

export const selectCollectionMethod = createSelector(
  getCollectionFormMethod,
  data => data
);

export const takeCurrentView = createSelector(getCurrentView, data => data);

export const takeDirtyForm = createSelector(getDirtyForm, data => data);

export const selectShowRemoveButton = createSelector(
  getCallResultType,
  (currentType: string) => {
    const { HARDSHIP, DECEASE, BANKRUPTCY, LONG_TERM_MEDICAL, INCARCERATION } =
      CALL_RESULT_CODE;

    if (
      currentType === HARDSHIP ||
      currentType === DECEASE ||
      currentType === BANKRUPTCY ||
      currentType === LONG_TERM_MEDICAL ||
      currentType === INCARCERATION
    )
      return true;
    return false;
  }
);

export const selectRemoveButtonName = createSelector(
  getCallResultType,
  (currentType: string) => {
    const { HARDSHIP, DECEASE, BANKRUPTCY, LONG_TERM_MEDICAL, INCARCERATION } =
      CALL_RESULT_CODE;
    if (currentType === HARDSHIP)
      return I18N_COLLECTION_FORM.REMOVE_HARDSHIP_STATUS;
    if (currentType === DECEASE)
      return I18N_COLLECTION_FORM.REMOVE_DECEASED_STATUS;
    if (currentType === BANKRUPTCY)
      return I18N_COLLECTION_FORM.REMOVE_BANKRUPTCY_STATUS;
    if (currentType === LONG_TERM_MEDICAL || currentType === INCARCERATION)
      return 'txt_remove_status';
    return '';
  }
);

export const selectCollectionFormMethod = createSelector(
  getCollectionFormMethod,
  data => data
);

export const selectCallResultsType = createSelector(
  getCallResultType,
  (callResultsType: string) => callResultsType
);

export const selectMemoSectionCollapsed = createSelector(
  getMemoSectionCollapsed,
  data => data
);

export const takeDisabledFormStatus = createSelector(
  getDisabledFormStatus,
  status => status
);

export const selectIsLoadingLastFollowUpData = createSelector(
  (states: RootState, storeId: string) =>
    states.collectionForm[storeId]?.isLoadingLastFollowUp || false,
  (isLoadingLastFollowUp: boolean) => isLoadingLastFollowUp
);

export const selectIsLoadingCallResultRefData = createSelector(
  (states: RootState, storeId: string) =>
    states.collectionForm[storeId]?.isLoadingCallResultRefData || false,
  (isLoadingCallResultRefData: boolean) => isLoadingCallResultRefData
);

export const selectLoadingForm = createSelector(
  getSelectedCallResult,
  getLoadingIncarceration,
  getLoadingLongTerm,
  getLoadingHardship,
  getLoadingPromiseToPay,
  (
    selectedCall: CallResult | undefined,
    loadingLongTerm: Loading,
    loadingIncarceration: Loading,
    loadingHardship: Loading,
    loadingPromiseToPay: Loading
  ) => {
    if (isEmpty(selectedCall)) return false;
    return (
      selectedCallResultLoading.includes(selectedCall!.code) &&
      (loadingIncarceration ||
        loadingLongTerm ||
        loadingHardship ||
        loadingPromiseToPay)
    );
  }
);

export const takeCollectionLoadingStatus = createSelector(
  [getBankruptcyLoadingStatus, selectGetDeceasedInfoLoading],
  (isBankruptcyLoading, isDeceasedLoading) =>
    isBankruptcyLoading || isDeceasedLoading
);

export const takeCollectionFormModalLoading = createSelector(
  [getBankruptcyUpdateStatus, selectUpdateDeceasedLoading],
  (isBankruptcyLoading, isDeceasedLoading) =>
    isBankruptcyLoading || isDeceasedLoading
);

export const takeInternalStatusCode = createSelector(
  getAccountDetailInternalStatusCode,
  data => data
);

// ----------TALKING POINT SECTION ------------------

export const selectTalkingPointData = createSelector(
  getTalkingPoint,
  talkingPoint => talkingPoint?.data
);

export const selectTalkingPointLoading = createSelector(
  getTalkingPoint,
  talkingPoint => talkingPoint?.loading
);

// ----------LAST 25 ACTIONS ------------------

export const getLast25Actions = (state: RootState, storeId: string) =>
  state.collectionForm[storeId]?.last10Actions;

export const last25ActionsSelector = {
  selectModalOpen: createSelector(
    getLast25Actions,
    last10Actions => last10Actions?.isOpen
  ),
  selectData: createSelector(
    getLast25Actions,
    last10Actions => last10Actions?.data
  ),
  selectLoading: createSelector(
    getLast25Actions,
    last10Actions => last10Actions?.loading
  ),
  selectError: createSelector(
    getLast25Actions,
    last10Actions => last10Actions?.error
  )
};
// --------------------------------------------

export const getInProgressStatus = (state: RootState, storeId: string) => {
  return state.collectionForm[storeId]?.isInProgress;
};

export const takeInProgressStatus = createSelector(
  getInProgressStatus,
  data => data
);

export const getDeceasedConfirmation = (state: RootState, storeId: string) => {
  return state.deceased[storeId]?.deceasedInformation?.deceasedConfirmation;
};

export const selectDeceasedStatusCodeFromDeceasedDetailAPI = createSelector(
  getDeceasedConfirmation,
  deceasedConfirmation => {
    const deceasedStatus = DECEASED_STATUS_MAPPING.find(
      item =>
        item.deceasedConfirmation.toLocaleLowerCase() ===
        deceasedConfirmation?.toLocaleLowerCase()
    );
    return deceasedStatus?.code;
  }
);

export const isHavingBankruptcyPending = (
  state: RootState,
  storeId: string
) => {
  return (
    state.bankruptcy[storeId]?.info?.actionEntryCode ===
    CALL_RESULT_CODE.BANKRUPTCY_PENDING
  );
};

export const isHavingBankruptcyConfirm = (
  state: RootState,
  storeId: string
) => {
  return (
    state.bankruptcy[storeId]?.info?.actionEntryCode ===
    CALL_RESULT_CODE.BANKRUPTCY
  );
};

export const selectIsHavingBankruptcyPending = createSelector(
  isHavingBankruptcyPending,
  data => data
);

export const selectIsHavingBankruptcyConfirm = createSelector(
  isHavingBankruptcyConfirm,
  data => data
);

export const bankruptcyActionEntryCode = (
  state: RootState,
  storeId: string
) => {
  return state.bankruptcy[storeId]?.info?.actionEntryCode;
};

export const selectBankruptcyActionEntryCode = createSelector(
  bankruptcyActionEntryCode,
  data => data
);

export const isHavingHardshipData = (state: RootState, storeId: string) => {
  return state.hardship[storeId]?.data?.processStatus === HARDSHIP_COMPLETE;
};

export const isHavingValidHardshipData = (
  state: RootState,
  storeId: string
) => {
  return [HARDSHIP_COMPLETE, HARDSHIP_IN_PROGRESS].includes(
    state.hardship[storeId]?.data?.processStatus
  );
};

export const existsHardshipData = (state: RootState, storeId: string) => {
  return state.hardship[storeId]?.data;
};

export const selectIsHavingValidHardshipData = createSelector(
  isHavingValidHardshipData,
  data => data
);

export const selectIsHavingHardshipData = createSelector(
  isHavingHardshipData,
  data => data
);

export const selectExistsHardshipData = createSelector(
  existsHardshipData,
  data => data
);

export const selectIsHavingLongTermMedical = createSelector(
  (state: RootState, storeId: string) => state.longTermMedical[storeId]?.data,
  data => data?.longTermMedicalStatus === 'ACTIVE'
);

export const isActiveLongTermMedical = (state: RootState, storeId: string) => {
  return (
    state.longTermMedical[storeId]?.data?.longTermMedicalStatus === 'ACTIVE'
  );
};

export const selectIsHavingIncarceration = createSelector(
  (state: RootState, storeId: string) => state.incarceration[storeId]?.data,
  data => data?.incarcerationStatus === 'ACTIVE'
);

export const isHavingCCCSDataAcceptProposal = (
  state: RootState,
  storeId: string
) => {
  return state.cccs[storeId]?.data?.action === CCCS_ACTIONS.ACCEPT_PROPOSAL;
};

export const selectIsHavingCCCSDataAcceptProposal = createSelector(
  isHavingCCCSDataAcceptProposal,
  data => data
);

// ---------------------UPLOAD MODAL--------------------------------------
export const getUploadFiles = (state: RootState, storeId: string) =>
  state.collectionForm[storeId]?.uploadFiles;

export const selectUploadModal = createSelector(
  getUploadFiles,
  data => data?.openModal
);

export const selectUploadFileType = createSelector(
  getUploadFiles,
  data => data?.type
);

// ---------------------------------------------------------------------

export const selectViewDeathCertificate = createSelector(
  [getCallResultType],
  callResultValue => {
    const { isAdminRole, isCollector } = window.appConfig?.commonConfig;
    const hasPermissionUpload = isAdminRole || isCollector;
    return (
      [CALL_RESULT_CODE.DECEASE_PENDING, CALL_RESULT_CODE.DECEASE].includes(
        callResultValue as CALL_RESULT_CODE
      ) && hasPermissionUpload
    );
  }
);
