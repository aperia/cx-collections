// Const
import { EMPTY_STRING } from 'app/constants';

// Service
import { apiService } from 'app/utils/api.service';
import { AxiosRequestConfig } from 'axios';

// Types
import { TalkingPointRequestBody } from 'app/global-types/collectionForm';
import {
  ClientInfoGetByIdRequest,
  CollectionFormDetailPayload,
  CommonCollectionFormPayload,
  DownloadCaseDocumentRequest,
  GetCallResultRequest,
  GetCallResultResponse,
  GetListDocumentRequest,
  GetTalkingPointPayload,
  IResponseGetLatestAction,
  Last25ActionRequest,
  UploadRequestBody
} from './types';

export const collectionFormService = {
  getCallResultsRefData(data: GetCallResultRequest) {
    const url =
      window.appConfig?.api?.collectionForm?.getCallResultsRefData ||
      EMPTY_STRING;
    return apiService.post<GetCallResultResponse>(url, data);
  },
  getTalkingPoint(body: TalkingPointRequestBody) {
    const url =
      window.appConfig?.api?.collectionForm?.getTalkingPoint || EMPTY_STRING;
    return apiService.post<GetTalkingPointPayload>(url, body);
  },
  getDetailCallResultType(data: CommonCollectionFormPayload) {
    const url =
      window.appConfig?.api?.collectionForm?.getDetailCallResultType ||
      EMPTY_STRING;
    return apiService.post<CollectionFormDetailPayload>(url, data);
  },
  getLatestAction(data: MagicKeyValue) {
    const url =
      window.appConfig?.api?.collectionForm?.actionEntry || EMPTY_STRING;
    return apiService.post<IResponseGetLatestAction>(url, data);
  },
  getLast25Actions(data: Last25ActionRequest) {
    const url = window.appConfig?.api?.collectionForm?.getLast25Actions || '';
    return apiService.post(url, data);
  },
  getDescriptionCodeForLast25Actions(postRequest: ClientInfoGetByIdRequest) {
    const url =
      window.appConfig?.api?.collectionForm
        ?.getDescriptionCodeForLast25Actions || '';
    return apiService.post(url, postRequest);
  },
  updateActionEntryWorkFlow(data: any) {
    const url =
      window.appConfig?.api?.collectionForm?.updateActionEntryWorkFlow ||
      EMPTY_STRING;
    return apiService.post(url, data);
  },
  uploadFiles: (postData: UploadRequestBody, options: AxiosRequestConfig) => {
    return apiService.post(
      window.appConfig?.api?.collectionForm?.uploadCaseDocument || '',
      postData,
      options
    );
  },
  getListCaseDocuments(data: GetListDocumentRequest) {
    const url =
      window.appConfig?.api?.collectionForm?.getCaseDocumentList || '';
    return apiService.post(url, data);
  },
  downloadCaseDocument(data: DownloadCaseDocumentRequest) {
    const url =
      window.appConfig?.api?.collectionForm?.downloadCaseDocument || '';
    return apiService.post(url, data);
  }
};
