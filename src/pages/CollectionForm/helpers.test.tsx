import React, { Suspense } from 'react';
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import { CALL_RESULT_CODE, removeItems } from './constants';
import {
  isRemoveEditCollectionForm,
  isDefaultCallResultType,
  dispatchSubmitForm,
  takeCallResultsComponent,
  removeDelayProgress,
  getDelayProgress,
  dispatchDestroyDelayProgress,
  getDelayProgressRawData,
  setDelayProgress,
  dispatchDelayProgress,
  combineDocumentClassification,
  callResultsComponentDisc
} from './helpers';
import { collectionFormActions } from './_redux/reducers';

jest.mock('pages/Bankruptcy/BankruptcyForm/BankruptcyPendingForm', () => () => (
  <div>BankruptcyPendingForm</div>
));
jest.mock('pages/Deceased/DeceasedForm', () => () => <div>DeceasedForm</div>);
jest.mock('pages/Bankruptcy/BankruptcyForm/BankruptcyForm', () => () => (
  <div>BankruptcyForm</div>
));
jest.mock('pages/Deceased/DeceasedPendingForm', () => () => (
  <div>DeceasedPendingForm</div>
));
jest.mock('pages/Deceased/DeceasedForm', () => () => <div>DeceasedForm</div>);
jest.mock('pages/CCCS/CCCSForm', () => () => <div>CCCSForm</div>);

const collectionFormActionsSpy = mockActionCreator(collectionFormActions);

const sessionStorageMock = (() => {
  let store: MagicKeyValue = {};
  return {
    getItem: function (key: string) {
      return store[key] || undefined;
    },
    setItem: function (key: string, value: any) {
      store[key] = value ? value.toString() : value + '';
    },
    clear: function () {
      store = {};
    }
  };
})();

Object.defineProperty(window, 'sessionStorage', {
  value: sessionStorageMock
});

createStore(
  rootReducer,
  {
    tab: {
      tabStoreIdSelected: storeId
    }
  },
  applyMiddleware(thunk)
);

const customStore = (type: CALL_RESULT_CODE, tabStoreIdSelected?: string) =>
  createStore(
    rootReducer,
    {
      tab: {
        tabStoreIdSelected
      },
      collectionForm: {
        [storeId]: {
          selectedCallResult: {
            code: type
          },
          lastFollowUpData: {
            callResultType: type
          }
        }
      }
    },
    applyMiddleware(thunk)
  );

describe('Test function to take call result component', () => {
  beforeEach(() => {
    sessionStorage.clear();
  });

  it('takeCallResultsComponent', () => {
    const response = takeCallResultsComponent()(
      customStore(CALL_RESULT_CODE.BANKRUPTCY_PENDING, storeId).dispatch,
      customStore(CALL_RESULT_CODE.BANKRUPTCY_PENDING, storeId).getState,
      {}
    );

    expect(response?.type._result).toEqual(null);
  });

  it('takeCallResultsComponent > wrong type', () => {
    const response = takeCallResultsComponent()(
      customStore('wrong' as unknown as CALL_RESULT_CODE, storeId).dispatch,
      customStore('wrong' as unknown as CALL_RESULT_CODE, storeId).getState,
      {}
    );

    expect(response).toEqual(null);
  });

  it('takeCallResultsComponent > none tab selected', () => {
    const response = takeCallResultsComponent()(
      customStore(CALL_RESULT_CODE.BANKRUPTCY_PENDING).dispatch,
      customStore(CALL_RESULT_CODE.BANKRUPTCY_PENDING).getState,
      {}
    );

    expect(response).toEqual(null);
  });

  it('Should dispatch event when call func dispatchSubmitForm ', () => {
    const mockDispatchEvent = jest.fn();

    jest
      .spyOn(window, 'dispatchEvent')
      .mockImplementationOnce(() => mockDispatchEvent());
    dispatchSubmitForm();

    expect(mockDispatchEvent).toBeCalled();
  });

  it('isRemoveEditCollectionForm', () => {
    const type = '';
    const result = isRemoveEditCollectionForm(type);

    expect(removeItems.includes(type)).toEqual(result);
  });

  it('isDefaultCallResultType', () => {
    const code = 'code';
    const listCallResult = [{ description: 'description', code: 'code' }];
    const result = isDefaultCallResultType(code, listCallResult);
    expect(result).toBeTruthy();

    const resultFalse = isDefaultCallResultType();
    expect(resultFalse).toBeFalsy();
  });

  it('setDelayProgress', () => {
    sessionStorage.setItem(
      'collectionInProgressStatus',
      btoa(JSON.stringify({ storeData: { data: '123' } }))
    );
    const response = setDelayProgress(storeId, 'type', [{ key: '1' }]);
    expect(response).toBeUndefined();
  });

  it('setDelayProgress with error', () => {
    const response = setDelayProgress({} as string, {} as string, { x: 2n });
    expect(response).toBeUndefined();
  });

  it('removeDelayProgress > without delayprogress', async () => {
    collectionFormActionsSpy('removeLastDelayCallResult');
    sessionStorage.setItem(
      'collectionInProgressStatus',
      btoa(JSON.stringify({ storeData: { data: '123' } }))
    );
    const mockArgs = {
      storeId
    };
    const store = createStore(
      rootReducer,
      {
        generalInformation: { [storeId]: {} }
      },
      applyMiddleware(thunk)
    );

    getDelayProgress();

    const response = removeDelayProgress(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response).toBeUndefined();
  });

  it('removeDelayProgress > when storeId not in progress', async () => {
    collectionFormActionsSpy('removeLastDelayCallResult');
    window.delayProgress = { id: '123', id2: undefined };
    sessionStorage.setItem(
      'collectionInProgressStatus',
      btoa(JSON.stringify({ storeData: { data: '123' } }))
    );
    const mockArgs = {
      storeId
    };
    const store = createStore(
      rootReducer,
      {
        generalInformation: { [storeId]: {} }
      },
      applyMiddleware(thunk)
    );

    getDelayProgress();

    const response = removeDelayProgress(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response).toBeUndefined();
  });

  it('removeDelayProgress > when storeId in progress', async () => {
    collectionFormActionsSpy('removeLastDelayCallResult');
    window.delayProgress = { id: '123', [storeId]: '321' };
    sessionStorage.setItem(
      'collectionInProgressStatus',
      btoa(JSON.stringify({ storeData: { data: '123' } }))
    );
    const store = createStore(
      rootReducer,
      { generalInformation: { [storeId]: {} } },
      applyMiddleware(thunk)
    );

    getDelayProgress();

    const response = removeDelayProgress(storeId)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response).toBeUndefined();
  });

  it('getDelayProgressRawData', () => {
    sessionStorage.setItem(
      'collectionInProgressStatus',
      btoa(
        JSON.stringify({
          storeData: {
            ['123456']: {
              isInProgress: '123',
              lastDelayCallResult: '123',
              inProgressInfo: 'W3sia2V5IjogIjEifV0='
            }
          }
        })
      )
    );

    getDelayProgress();

    const response = getDelayProgressRawData();

    expect(response).toEqual({
      '123456': {
        inProgressInfo: [
          {
            key: '1'
          }
        ],
        isInProgress: '123',
        lastDelayCallResult: '123'
      }
    });
  });

  it('getDelayProgressRawData with error', () => {
    sessionStorage.setItem(
      'collectionInProgressStatus',
      btoa(
        JSON.stringify({
          storeData: {
            ['123456']: {
              isInProgress: '123'
            }
          }
        })
      )
    );

    getDelayProgress();

    const response = getDelayProgressRawData();

    expect(response).toEqual({});
  });

  it('dispatchDestroyDelayProgress', () => {
    const response = dispatchDestroyDelayProgress(storeId);
    expect(response).toBeUndefined();
  });

  it('dispatchDelayProgress', () => {
    const response = dispatchDelayProgress(jest.fn(), storeId);
    expect(response).toBeUndefined();
  });

  it('getDelayProgress with error', () => {
    const response = getDelayProgress();
    expect(response).toEqual({});
  });

  it('combineDocumentClassification', () => {
    const response = combineDocumentClassification('hardShip', storeId);
    expect(response).toEqual('hardShip_123456789');
  });

  it('combineDocumentClassification', () => {
    const response = combineDocumentClassification(
      'hardShip',
      storeId,
      'hardShip'
    );
    expect(response).toEqual('HardshipMillitaryDeploymentCertificate');
  });

  it('callResultsComponentDisc', () => {
    const renderComponent = (Component: React.FC) => {
      return renderMockStoreId(
        <Suspense fallback={<div />}>
          <Component />
        </Suspense>,
        { initialState: {} }
      );
    };

    const checkLazyImport = (code: CALL_RESULT_CODE) => {
      const result = callResultsComponentDisc.get(code);

      return renderComponent(result?.component as React.FC);
    };

    checkLazyImport(CALL_RESULT_CODE.BANKRUPTCY_PENDING);
    checkLazyImport(CALL_RESULT_CODE.NOT_DECEASED);
    checkLazyImport(CALL_RESULT_CODE.BANKRUPTCY);
    checkLazyImport(CALL_RESULT_CODE.DECEASE_PENDING);
    checkLazyImport(CALL_RESULT_CODE.DECEASE);
    checkLazyImport(CALL_RESULT_CODE.NOT_BANKRUPTCY);
    checkLazyImport(CALL_RESULT_CODE.CCCS);
  });
});
