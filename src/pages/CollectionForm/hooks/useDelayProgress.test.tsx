import React, { useRef } from 'react';
import { AccountDetailProvider } from 'app/hooks';
import {
  accEValue,
  mockActionCreator,
  renderMockStoreId,
  storeId
} from 'app/test-utils';
import { useDelayProgress, useTriggerDelayProgress } from './useDelayProgress';
import {
  CALL_RESULT_CODE,
  COLLECTION_EVENT,
  DEFAULT_DELAY_TIME
} from '../constants';
import { bankruptcyActions } from 'pages/Bankruptcy/_redux/reducers';
import { promiseToPayActions } from 'pages/PromiseToPay/_redux/reducers';
import { deceasedActions } from 'pages/Deceased/_redux/reducers';
import { incarcerationActions } from 'pages/Incarceration/_redux/reducers';
import { longTermMedicalActions } from 'pages/LongTermMedical/_redux/reducers';
import { cccsActions } from 'pages/CCCS/_redux/reducers';
import { hardshipActions } from 'pages/HardShip/_redux/reducers';
import { settlementActions } from 'pages/Settlement/_redux/reducers';

jest.mock('react', () => {
  const reactReal = jest.requireActual('react');

  return {
    ...reactReal,
    useRef: jest.fn()
  };
});

const sessionStorageMock = (() => {
  let store: MagicKeyValue = {};
  return {
    getItem: function (key: string) {
      return store[key] || undefined;
    },
    setItem: function (key: string, value: any) {
      store[key] = value ? value.toString() : value + '';
    },
    clear: function () {
      store = {};
    }
  };
})();

Object.defineProperty(window, 'sessionStorage', {
  value: sessionStorageMock
});

const hardshipActionsSpy = mockActionCreator(hardshipActions);
const bankruptcyActionsSpy = mockActionCreator(bankruptcyActions);
const promiseToPayActionsSpy = mockActionCreator(promiseToPayActions);
const deceasedActionsSpy = mockActionCreator(deceasedActions);
const incarcerationActionsSpy = mockActionCreator(incarcerationActions);
const longTermMedicalActionsSpy = mockActionCreator(longTermMedicalActions);
const getCCCSDetailSpy = mockActionCreator(cccsActions);
const settlementActionsSpy = mockActionCreator(settlementActions);

beforeEach(() => {
  (useRef as unknown as jest.Mock).mockImplementation(() => ({
    current: true
  }));
});

describe('useDelayProgress', () => {
  it('useDelayProgress with delayProgress', () => {
    jest.useFakeTimers();

    window.delayProgress = { [storeId]: '123' };
    const event = new CustomEvent(COLLECTION_EVENT.DELAY_PROGRESS, {
      detail: {
        delayFunction: jest.fn(),
        storeId
      }
    });
    const eventDestroy = new CustomEvent(
      COLLECTION_EVENT.DESTROY_DELAY_PROGRESS,
      {
        detail: {
          storeId
        }
      }
    );
    const Component = () => {
      useDelayProgress();
      return <div />;
    };

    renderMockStoreId(
      <AccountDetailProvider
        value={{
          storeId,
          accEValue
        }}
      >
        <Component />
      </AccountDetailProvider>,
      {}
    );

    jest.runOnlyPendingTimers();
    window.dispatchEvent(event);
    window.dispatchEvent(eventDestroy);
    jest.useRealTimers();
  });

  it('useDelayProgress > hanldeDelay', () => {
    const event = new CustomEvent(COLLECTION_EVENT.DELAY_PROGRESS, {
      detail: {
        delayFunction: jest.fn(),
        storeId
      }
    });
    const Component = () => {
      useDelayProgress();
      return <div />;
    };

    jest.useFakeTimers();

    renderMockStoreId(
      <AccountDetailProvider
        value={{
          storeId,
          accEValue
        }}
      >
        <Component />
      </AccountDetailProvider>,
      {}
    );

    window.dispatchEvent(event);
    jest.runTimersToTime(DEFAULT_DELAY_TIME);
  });

  it('useDelayProgress > handleDestroy', () => {
    const eventDestroy = new CustomEvent(
      COLLECTION_EVENT.DESTROY_DELAY_PROGRESS,
      { detail: { storeId } }
    );
    const Component = () => {
      useDelayProgress();
      return <div />;
    };

    renderMockStoreId(
      <AccountDetailProvider
        value={{
          storeId,
          accEValue
        }}
      >
        <Component />
      </AccountDetailProvider>,
      {}
    );

    window.dispatchEvent(eventDestroy);
  });
});

describe('useTriggerDelayProgress', () => {
  const customSession = (type?: CALL_RESULT_CODE) =>
    sessionStorage.setItem(
      'collectionInProgressStatus',
      type
        ? btoa(
            JSON.stringify({
              storeData: {
                [storeId]: {
                  lastDelayCallResult: type
                }
              }
            })
          )
        : ''
    );

  const initialState: Partial<RootState> = {
    collectionForm: {
      [storeId]: {
        isInProgress: true
      }
    }
  };

  it('useTriggerDelayProgress with error', () => {
    const Component = () => {
      useTriggerDelayProgress();
      return <div />;
    };

    const { store } = renderMockStoreId(
      <AccountDetailProvider
        value={{
          storeId,
          accEValue
        }}
      >
        <Component />
      </AccountDetailProvider>,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              isInProgress: false
            }
          }
        }
      }
    );

    expect(store.getState().collectionForm[storeId].isInProgress).toBeFalsy();
  });

  it('useTriggerDelayProgress > empty state', () => {
    (useRef as unknown as jest.Mock).mockImplementation(() => ({
      current: false
    }));
    const spy = bankruptcyActionsSpy('getBankruptcyInformation');
    customSession();
    const Component = () => {
      useTriggerDelayProgress();
      return <div />;
    };

    renderMockStoreId(
      <AccountDetailProvider
        value={{
          storeId,
          accEValue
        }}
      >
        <Component />
      </AccountDetailProvider>,
      { initialState: {} }
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it('useTriggerDelayProgress > not trigger', () => {
    (useRef as unknown as jest.Mock).mockImplementation(() => ({
      current: false
    }));
    const spy = bankruptcyActionsSpy('getBankruptcyInformation');
    customSession(CALL_RESULT_CODE.BANKRUPTCY);
    const Component = () => {
      useTriggerDelayProgress();
      return <div />;
    };

    renderMockStoreId(
      <AccountDetailProvider
        value={{
          storeId,
          accEValue
        }}
      >
        <Component />
      </AccountDetailProvider>,
      { initialState }
    );
    expect(spy).not.toHaveBeenCalled();
  });

  it('useTriggerDelayProgress > BANKRUPTCY_GROUP_CODES', () => {
    const spy = bankruptcyActionsSpy('getBankruptcyInformation');
    customSession(CALL_RESULT_CODE.BANKRUPTCY);
    const Component = () => {
      useTriggerDelayProgress();
      return <div />;
    };

    renderMockStoreId(
      <AccountDetailProvider
        value={{
          storeId,
          accEValue
        }}
      >
        <Component />
      </AccountDetailProvider>,
      { initialState }
    );
    expect(spy).toHaveBeenCalledWith({
      storeId
    });
  });

  it('useTriggerDelayProgress > SETTLEMENT', () => {
    const spy = settlementActionsSpy('getSettlement');
    customSession(CALL_RESULT_CODE.SETTLEMENT);
    const Component = () => {
      useTriggerDelayProgress();
      return <div />;
    };

    renderMockStoreId(
      <AccountDetailProvider
        value={{
          storeId,
          accEValue
        }}
      >
        <Component />
      </AccountDetailProvider>,
      { initialState }
    );
    expect(spy).toHaveBeenCalledWith({
      storeId,
      postData: {
        accountId: accEValue
      }
    });
  });

  it('useTriggerDelayProgress > BANKRUPTCY_GROUP_CODES', () => {
    const spy = bankruptcyActionsSpy('getBankruptcyInformation');
    customSession(CALL_RESULT_CODE.BANKRUPTCY);
    const Component = () => {
      useTriggerDelayProgress();
      return <div />;
    };

    renderMockStoreId(
      <AccountDetailProvider
        value={{
          storeId,
          accEValue
        }}
      >
        <Component />
      </AccountDetailProvider>,
      { initialState }
    );
    expect(spy).toHaveBeenCalledWith({
      storeId
    });
  });

  it('useTriggerDelayProgress > PROMISE_GROUP_CODES', () => {
    const spy = promiseToPayActionsSpy('getPromiseToPayData');
    customSession(CALL_RESULT_CODE.PROMISE_TO_PAY);
    const Component = () => {
      useTriggerDelayProgress();
      return <div />;
    };

    renderMockStoreId(
      <AccountDetailProvider
        value={{
          storeId,
          accEValue
        }}
      >
        <Component />
      </AccountDetailProvider>,
      { initialState }
    );
    expect(spy).toHaveBeenCalledWith({
      storeId
    });
  });

  it('useTriggerDelayProgress > PROMISE_GROUP_CODES', () => {
    const spy = promiseToPayActionsSpy('getPromiseToPayData');
    customSession(CALL_RESULT_CODE.PROMISE_TO_PAY);
    const Component = () => {
      useTriggerDelayProgress();
      return <div />;
    };

    renderMockStoreId(
      <AccountDetailProvider
        value={{
          storeId,
          accEValue
        }}
      >
        <Component />
      </AccountDetailProvider>,
      { initialState }
    );
    expect(spy).toHaveBeenCalledWith({
      storeId
    });
  });

  it('useTriggerDelayProgress > DECEASED_GROUP_CODES', () => {
    const spy = deceasedActionsSpy('deceasedInformationRequest');
    customSession(CALL_RESULT_CODE.DECEASE);
    const Component = () => {
      useTriggerDelayProgress();
      return <div />;
    };

    renderMockStoreId(
      <AccountDetailProvider
        value={{
          storeId,
          accEValue
        }}
      >
        <Component />
      </AccountDetailProvider>,
      { initialState }
    );
    expect(spy).toHaveBeenCalledWith({
      storeId
    });
  });

  it('useTriggerDelayProgress > INCARCERATION', () => {
    const spy = incarcerationActionsSpy('getIncarceration');
    customSession(CALL_RESULT_CODE.INCARCERATION);
    const Component = () => {
      useTriggerDelayProgress();
      return <div />;
    };

    renderMockStoreId(
      <AccountDetailProvider
        value={{
          storeId,
          accEValue
        }}
      >
        <Component />
      </AccountDetailProvider>,
      { initialState }
    );
    expect(spy).toHaveBeenCalledWith({
      storeId
    });
  });

  it('useTriggerDelayProgress > LONG_TERM_MEDICAL', () => {
    const spy = longTermMedicalActionsSpy('getLongTermMedical');
    customSession(CALL_RESULT_CODE.LONG_TERM_MEDICAL);
    const Component = () => {
      useTriggerDelayProgress();
      return <div />;
    };

    renderMockStoreId(
      <AccountDetailProvider
        value={{
          storeId,
          accEValue
        }}
      >
        <Component />
      </AccountDetailProvider>,
      { initialState }
    );
    expect(spy).toHaveBeenCalledWith({
      storeId
    });
  });

  it('useTriggerDelayProgress > CCCS', () => {
    const spy = getCCCSDetailSpy('getCCCSDetail');
    customSession(CALL_RESULT_CODE.CCCS);
    const Component = () => {
      useTriggerDelayProgress();
      return <div />;
    };

    renderMockStoreId(
      <AccountDetailProvider
        value={{
          storeId,
          accEValue
        }}
      >
        <Component />
      </AccountDetailProvider>,
      { initialState }
    );
    expect(spy).toHaveBeenCalledWith({
      storeId
    });
  });

  it('useTriggerDelayProgress > HARDSHIP', () => {
    const spy = hardshipActionsSpy('getHardshipDebtManagementDetails');
    customSession(CALL_RESULT_CODE.HARDSHIP);
    const Component = () => {
      useTriggerDelayProgress();
      return <div />;
    };

    renderMockStoreId(
      <AccountDetailProvider
        value={{
          storeId,
          accEValue
        }}
      >
        <Component />
      </AccountDetailProvider>,
      { initialState }
    );
    expect(spy).toHaveBeenCalledWith({
      postData: {
        storeId
      }
    });
  });
});
