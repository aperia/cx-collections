import { useAccountDetail } from 'app/hooks';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

// Redux
import { collectionFormActions } from '../_redux/reducers';

export const useSaveViewName = (viewName?: Array<string>) => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();

  useEffect(() => {
    dispatch(
      collectionFormActions.setViewName({
        storeId,
        viewName
      })
    );
  }, [dispatch, storeId, viewName]);

  return viewName;
};
