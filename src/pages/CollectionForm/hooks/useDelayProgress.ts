import { useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';
import isUndefined from 'lodash.isundefined';

// Const
import {
  BANKRUPTCY_GROUP_CODES,
  CALL_RESULT_CODE,
  COLLECTION_EVENT,
  DEFAULT_DELAY_TIME,
  COLLECTION_FORM_DELAY_STATUS,
  PROMISE_GROUP_CODES,
  DECEASED_GROUP_CODES,
  HARDSHIP_GROUP_CODES
} from '../constants';

// Type
import { DelayFunctionArgs, StoreInprogressData } from '../types';

// Hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// Helpers
import { dispatchDelayProgress, removeDelayProgress } from '../helpers';
import { getSessionStorage } from 'app/helpers';

// Redux
import { takeInProgressStatus } from '../_redux/selectors';
import { bankruptcyActions } from 'pages/Bankruptcy/_redux/reducers';
import { promiseToPayActions } from 'pages/PromiseToPay/_redux/reducers';
import { deceasedActions } from 'pages/Deceased/_redux/reducers';
import { incarcerationActions } from 'pages/Incarceration/_redux/reducers';
import { longTermMedicalActions } from 'pages/LongTermMedical/_redux/reducers';
import { cccsActions } from 'pages/CCCS/_redux/reducers';
import { hardshipActions } from 'pages/HardShip/_redux/reducers';
import { settlementActions } from 'pages/Settlement/_redux/reducers';

export const useDelayProgress = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    let currentStoreId: string;

    function handleDelay(this: Window, event: Event) {
      const { delayFunction, storeId } = (
        event as CustomEvent<DelayFunctionArgs>
      )?.detail;

      if (window.delayProgress && !isUndefined(window.delayProgress[storeId]))
        return;

      currentStoreId = storeId;

      window.delayProgress = {
        ...window.delayProgress,
        [storeId]: setInterval(() => {
          dispatch(delayFunction);
        }, DEFAULT_DELAY_TIME)
      };
    }

    function handleDestroy(this: Window, event: Event) {
      const { storeId } = (event as CustomEvent<StoreIdPayload>)?.detail;
      dispatch(removeDelayProgress(storeId));
    }

    window.addEventListener(
      COLLECTION_EVENT.DELAY_PROGRESS,
      handleDelay,
      false
    );

    window.addEventListener(
      COLLECTION_EVENT.DESTROY_DELAY_PROGRESS,
      handleDestroy,
      false
    );

    return () => {
      window.delayProgress &&
        clearInterval((window.delayProgress[currentStoreId] || 0) as number);
      window.removeEventListener(COLLECTION_EVENT.DELAY_PROGRESS, handleDelay);
      window.removeEventListener(
        COLLECTION_EVENT.DESTROY_DELAY_PROGRESS,
        handleDestroy
      );
    };
  }, [dispatch]);
};

export const useTriggerDelayProgress = () => {
  const { storeId, accEValue } = useAccountDetail();

  const isInProgress = useStoreIdSelector(takeInProgressStatus);

  const { storeData = {} } =
    getSessionStorage<StoreInprogressData>(COLLECTION_FORM_DELAY_STATUS) || {};

  const { lastDelayCallResult: callResultType } = storeData[storeId] || {};

  const isTriggered = useRef<boolean>(true);

  useEffect(() => {
    let action;
    if (!isInProgress) return;

    if (isTriggered.current) {
      if (BANKRUPTCY_GROUP_CODES.includes(callResultType as CALL_RESULT_CODE)) {
        action = bankruptcyActions.getBankruptcyInformation({
          storeId
        });
      }

      if (PROMISE_GROUP_CODES.includes(callResultType as CALL_RESULT_CODE)) {
        action = promiseToPayActions.getPromiseToPayData({
          storeId
        });
      }

      if (DECEASED_GROUP_CODES.includes(callResultType as CALL_RESULT_CODE)) {
        action = deceasedActions.deceasedInformationRequest({
          storeId
        });
      }

      if (callResultType === CALL_RESULT_CODE.INCARCERATION) {
        action = incarcerationActions.getIncarceration({
          storeId
        });
      }

      if (callResultType === CALL_RESULT_CODE.LONG_TERM_MEDICAL) {
        action = longTermMedicalActions.getLongTermMedical({
          storeId
        });
      }

      if (callResultType === CALL_RESULT_CODE.CCCS) {
        action = cccsActions.getCCCSDetail({
          storeId
        });
      }

      if (HARDSHIP_GROUP_CODES.includes(callResultType as CALL_RESULT_CODE)) {
        action = hardshipActions.getHardshipDebtManagementDetails({
          postData: {
            storeId
          }
        });
      }

      if (callResultType === CALL_RESULT_CODE.SETTLEMENT) {
        action = settlementActions.getSettlement({
          storeId,
          postData: { accountId: accEValue }
        });
      }

      action && dispatchDelayProgress(action, storeId);

      isTriggered.current = false;
    }

    return () => {
      isTriggered.current = false;
    };
  }, [isInProgress, storeId, accEValue, callResultType]);
};
