import { useEffect } from 'react';
import isFunction from 'lodash.isfunction';

// Const
import { COLLECTION_EVENT } from '../constants';

export const useSubmit = (func?: () => void) => {
  useEffect(() => {
    const handleSubmit = () => {
      isFunction(func) && func();
    };

    window.addEventListener(COLLECTION_EVENT.SUBMIT, handleSubmit);
    return () => {
      window.removeEventListener(COLLECTION_EVENT.SUBMIT, handleSubmit);
    };
  }, [func]);
};
