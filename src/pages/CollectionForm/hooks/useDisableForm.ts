import { useAccountDetail } from 'app/hooks';

// Hooks
import { useSelector } from 'react-redux';

// Selectors
import { takeDisabledFormStatus } from '../_redux/selectors';

export const useDisableForm = () => {
  const { storeId } = useAccountDetail();
  const dataSelector = useSelector<RootState, boolean>(states =>
    takeDisabledFormStatus(states, storeId)
  );

  return dataSelector;
};
