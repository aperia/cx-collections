import React from 'react';
import { screen } from '@testing-library/react';
import { renderMockStoreId, storeId } from 'app/test-utils';
import { AccountDetailProvider } from 'app/hooks';
import { Button } from 'app/_libraries/_dls/components';
import { useDisableForm } from './useDisableForm';

describe('Testing useDisableForm', () => {
  it('Should set disabled submit function', () => {
    const Component = () => {
      const disabled = useDisableForm();
      return <Button id={`useDisableForm-${storeId}`} disabled={disabled} />;
    };

    renderMockStoreId(
      <AccountDetailProvider
        value={{
          storeId,
          accEValue: storeId
        }}
      >
        <Component />
      </AccountDetailProvider>,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              viewName: ['viewName']
            }
          }
        }
      }
    );

    expect(screen.getByRole('button')).toBeInTheDocument();
  });
});
