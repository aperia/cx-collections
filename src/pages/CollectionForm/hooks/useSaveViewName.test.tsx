import React from 'react';
import { renderMockStoreId, storeId } from 'app/test-utils';
import { AccountDetailProvider } from 'app/hooks';
import { useSaveViewName } from './useSaveViewName';

describe('Testing useSaveViewName', () => {
  it('Should set view name', () => {
    const Component = () => {
      useSaveViewName(['viewName']);
      return <div />;
    };

    const { store } = renderMockStoreId(
      <AccountDetailProvider
        value={{
          storeId,
          accEValue: storeId
        }}
      >
        <Component />
      </AccountDetailProvider>
    );

    expect(store.getState().collectionForm[storeId].viewName).toEqual([
      'viewName'
    ]);
  });
});
