import React from 'react';

import { renderMockStoreId } from 'app/test-utils';
import { useSubmit } from './useSubmit';

// Const
import { COLLECTION_EVENT } from '../constants';

describe('Testing useSubmit', () => {
  it('Should set call submit function', () => {
    const mockFunc = jest.fn().mockImplementation();
    const Component = () => {
      useSubmit(mockFunc);
      return <div />;
    };

    renderMockStoreId(<Component />, {});

    const event = new CustomEvent(COLLECTION_EVENT.SUBMIT);
    window.dispatchEvent(event);

    expect(mockFunc).toHaveBeenCalled();
  });

  it('Should remove event listener', () => {
    const mockFunc = jest.fn().mockImplementation();
    const mockRemoveEvent = jest.spyOn(window, 'removeEventListener');

    const Component = () => {
      useSubmit(mockFunc);
      return <div />;
    };

    const { wrapper } = renderMockStoreId(<Component />, {});

    wrapper.unmount();
    expect(mockRemoveEvent).toHaveBeenCalled();
  });
});
