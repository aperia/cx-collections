import React from 'react';
import { screen } from '@testing-library/react';
import {
  renderMockStore,
  storeId,
  accEValue,
  mockActionCreator
} from 'app/test-utils';

import { AccountDetailProvider } from 'app/hooks';

import TalkingPoint from './index';
import { TalkingPoints } from '../types';
import { collectionFormActions } from '../_redux/reducers';

const collectionFormActionsSpy = mockActionCreator(collectionFormActions);

const generateState = (talkingPoint: TalkingPoints) => {
  const state: Partial<RootState> = {
    collectionForm: { [storeId]: { talkingPoint, uploadFiles: {} } }
  };
  return state;
};

describe('TalkingPoint', () => {
  const renderWrapper = (talkingPoint: {
    loading?: boolean;
    data?: string;
  }) => {
    return renderMockStore(
      <AccountDetailProvider value={{ storeId, accEValue }}>
        <TalkingPoint />
      </AccountDetailProvider>,
      { initialState: generateState(talkingPoint) }
    );
  };
  it('return null when has not talking point data', () => {
    collectionFormActionsSpy('getTalkingPoint');
    const { container } = renderWrapper({ data: '' });
    expect(container.firstChild).toBeNull();
  });

  it('return content', () => {
    collectionFormActionsSpy('getTalkingPoint');
    const mockContent1 = 'show something';
    const mockContent2 = 'show another something';
    renderWrapper({ data: `<p>${mockContent1}</p><p>${mockContent2}</p>` });
    expect(
      screen.getByText(new RegExp(`${mockContent1}`, 'i'))
    ).toBeInTheDocument();
    expect(
      screen.getByText(new RegExp(`${mockContent2}`, 'i'))
    ).toBeInTheDocument();
  });
});
