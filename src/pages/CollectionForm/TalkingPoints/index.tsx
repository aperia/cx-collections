import React, { useEffect, useRef } from 'react';

import { useDispatch } from 'react-redux';

import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

import { collectionFormActions } from '../_redux/reducers';
import { selectTalkingPointData } from '../_redux/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { useRenderElement } from 'app/_libraries/_dls/hooks';
import { markdownToHTML } from 'app/_libraries/_dls/components/TextEditor';

const TalkingPoint = () => {
  const { storeId, accEValue } = useAccountDetail();
  const elementRef = useRef<HTMLDivElement | null>(null);
  const dispatch = useDispatch();
  const talkingPointData = useStoreIdSelector<string>(selectTalkingPointData);

  useEffect(() => {
    dispatch(collectionFormActions.getTalkingPoint({ storeId, accEValue }));
  }, [dispatch, storeId, accEValue]);

  useRenderElement(
    elementRef,
    markdownToHTML(talkingPointData).element as HTMLElement
  );

  if (!talkingPointData) return null;
  return (
    <div
      data-testid={genAmtId('talkingPoint', 'content', '')}
      className="p-24"
      ref={elementRef}
    />
  );
};

export default TalkingPoint;
