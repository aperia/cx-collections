import React, { useRef } from 'react';
import classNames from 'classnames';

// components
import { SimpleBar } from 'app/_libraries/_dls/components';

// hooks
import { useAccountDetail } from 'app/hooks';

import { useApiErrorNotification } from 'pages/ApiErrorNotification/hooks/useApiErrorNotification';

export interface SimpleBarWithApiErrorProps {
  className?: string;
}

const SimpleBarWithApiError: React.FC<SimpleBarWithApiErrorProps> = ({
  children,
  className
}) => {
  const { storeId } = useAccountDetail();
  const ApiErrorDetail = useApiErrorNotification({
    storeId,
    forSection: 'inCollectionFormFlyOut'
  });
  const simpleBarRef = useRef<HTMLDivElement | null>(null);

  return (
    <SimpleBar scrollableNodeProps={{ ref: simpleBarRef }}>
      <ApiErrorDetail className={classNames(className)} />
      {children}
    </SimpleBar>
  );
};

export default SimpleBarWithApiError;
