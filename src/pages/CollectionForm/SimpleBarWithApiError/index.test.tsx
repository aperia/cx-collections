import React from 'react';
import { screen } from '@testing-library/react';
import { storeId, mockActionCreator, renderMockStoreId } from 'app/test-utils';
import SimpleBarWithApiError from '.';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

const errorState: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      isLoadingCallResultRefData: true,
      talkingPoint: {
        data: '123',
        loading: true
      },
      currentView: 'current'
    }
  },
  apiErrorNotification: {
    [storeId]: {
      errorDetail: {
        inCollectionFormFlyOut: {
          request: '500',
          response: '500'
        }
      }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<SimpleBarWithApiError />, {
    initialState
  });
};

const apiErrorNotificationActionSpy = mockActionCreator(
  apiErrorNotificationAction
);

describe('SimpleBarWithApiError', () => {
  beforeEach(() => {
    const mockScrollTo = jest.fn();
    Element.prototype.scrollTo = mockScrollTo;
  });

  it('render component', () => {
    renderWrapper(errorState);
    expect(screen.getByText('txt_api_error')).toBeInTheDocument();
  });

  it('handleClickErrorDetail', () => {
    const spy = apiErrorNotificationActionSpy('openApiDetailErrorModal');
    renderWrapper(errorState);
    const button = screen.getByText('txt_view_detail')!;
    button.click();

    expect(spy).toHaveBeenCalledWith({
      forSection: 'inCollectionFormFlyOut',
      storeId
    });
  });
});
