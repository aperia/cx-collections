import React from 'react';
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import { waitFor, fireEvent } from '@testing-library/react';

// components
import CollectionFormResults from '.';

// constants
import { CALL_RESULT_CODE } from './constants';

// redux
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { mockScrollToFn } from 'app/test-utils/mocks/mockProperty';
import * as entitlementsHelpers from 'app/entitlements/helpers';
import { TogglePinProps } from 'pages/__commons/InfoBar/TogglePin';
import { PERMISSIONS } from 'app/entitlements/constants';

HTMLCanvasElement.prototype.getContext = jest.fn();

window.appConfig = {
  commonConfig: {
    operatorID: 'ACB'
  }
} as AppConfiguration;

mockScrollToFn(jest.fn());

jest.mock(
  'pages/__commons/InfoBar/TogglePin',
  () => (props: TogglePinProps) => {
    return <div onClick={props.onToggle}>TogglePin</div>;
  }
);

const apiErrorNotificationActionSpy = mockActionCreator(
  apiErrorNotificationAction
);

const initialState: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      isLoadingCallResultRefData: true,
      talkingPoint: {
        data: '123',
        loading: true
      },
      currentView: 'current',
      uploadFiles: {}
    }
  },
  apiErrorNotification: {
    [storeId]: {
      errorDetail: {
        inCollectionFormFlyOut: {
          request: '500',
          response: '500'
        }
      }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<CollectionFormResults />, {
    initialState
  });
};

const { getComputedStyle } = global.window;

window.getComputedStyle = (elm, select) => getComputedStyle(elm, select);

describe('CollectionFormResults component', () => {
  beforeEach(() => {
    jest.useFakeTimers();
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
  });

  afterAll(() => {
    jest.runAllTimers();
  });

  it('render component', () => {
    waitFor(() => {
      const { wrapper } = renderWrapper(initialState);
      expect(
        wrapper.getByText('txt_collection_activities')
      ).toBeInTheDocument();
      expect(wrapper.getByText('txt_talking_points')).toBeInTheDocument();
    });
  });

  it('render component when hide talking point', () => {
    jest
      .spyOn(entitlementsHelpers, 'checkPermission')
      .mockImplementation((permission, _) => {
        return permission === PERMISSIONS.COLLECTION_ACTIVITIES_VIEW;
      });
    const { wrapper } = renderWrapper(initialState);

    expect(wrapper.queryByText('txt_talking_points')).toEqual(null);
  });

  it('render PromiseToPay component', () => {
    waitFor(() => {
      const { wrapper } = renderWrapper({
        collectionForm: {
          [storeId]: {
            isLoadingCallResultRefData: true,
            talkingPoint: {
              data: '123',
              loading: true
            },
            currentView: CALL_RESULT_CODE.PROMISE_TO_PAY,
            uploadFiles: {}
          }
        }
      });
      expect(wrapper.getByText('txt_promise_to_pay')).toBeInTheDocument();
    });
  });

  it('render HardShipForm component', () => {
    waitFor(() => {
      const { wrapper } = renderWrapper({
        collectionForm: {
          [storeId]: {
            isLoadingCallResultRefData: true,
            talkingPoint: {
              data: '123',
              loading: true
            },
            currentView: CALL_RESULT_CODE.HARDSHIP,
            uploadFiles: {}
          }
        }
      });
      expect(wrapper.getByText('txt_hardship')).toBeInTheDocument();
    });
  });

  it('render LongTermMedical component', () => {
    waitFor(() => {
      const { wrapper } = renderWrapper({
        collectionForm: {
          [storeId]: {
            isLoadingCallResultRefData: true,
            talkingPoint: {
              data: '123',
              loading: true
            },
            currentView: CALL_RESULT_CODE.LONG_TERM_MEDICAL,
            uploadFiles: {}
          }
        }
      });
      expect(wrapper.getByText('txt_long_term_medical')).toBeInTheDocument();
    });
  });

  it('render Incarceration component', () => {
    waitFor(() => {
      const { wrapper } = renderWrapper({
        collectionForm: {
          [storeId]: {
            isLoadingCallResultRefData: true,
            talkingPoint: {
              data: '123',
              loading: true
            },
            currentView: CALL_RESULT_CODE.INCARCERATION,
            uploadFiles: {}
          }
        }
      });
      expect(wrapper.getByText('txt_incarceration')).toBeInTheDocument();
    });
  });

  it('render Settlement component', () => {
    waitFor(() => {
      const { wrapper } = renderWrapper({
        collectionForm: {
          [storeId]: {
            isLoadingCallResultRefData: true,
            talkingPoint: {
              data: '123',
              loading: true
            },
            currentView: CALL_RESULT_CODE.SETTLEMENT,
            uploadFiles: {}
          }
        }
      });
      expect(wrapper.getByText('txt_incarceration')).toBeInTheDocument();
    });
  });

  it('render Settlement component > case 2', () => {
    waitFor(() => {
      const { wrapper } = renderWrapper({
        collectionForm: {
          [storeId]: {
            isLoadingCallResultRefData: true,
            talkingPoint: {
              data: '123',
              loading: true
            },
            currentView: CALL_RESULT_CODE.SETTLEMENT,
            uploadFiles: {},
            lastFollowUpData: {
              callResultType: 'ST'
            }
          }
        }
      });
      expect(wrapper.getByText('txt_incarceration')).toBeInTheDocument();
    });
  });

  it('handleClickErrorDetail', () => {
    const mockAction = apiErrorNotificationActionSpy('clearApiErrorData');
    const { wrapper } = renderWrapper({
      apiErrorNotification: {
        [storeId]: {
          errorDetail: {
            inCollectionFormFlyOut: {
              request: '500',
              response: '500'
            }
          }
        }
      }
    });
    const button = wrapper.getByText('txt_view_detail');

    fireEvent.click(button);

    expect(mockAction).toBeCalledWith({
      storeId,
      forSection: 'inCollectionFormFlyOut'
    });
  });

  it('should render api error', async () => {
    renderMockStoreId(<CollectionFormResults />, {
      initialState: {
        accountDetail: {
          [storeId]: {
            data: {
              internalStatusCode: 'CODE'
            }
          }
        },
        apiErrorNotification: {
          [storeId]: {
            errorDetail: {
              inCollectionFormFlyOut: {
                request: 'error',
                response: 'error'
              }
            }
          }
        }
      }
    });
  });
});
