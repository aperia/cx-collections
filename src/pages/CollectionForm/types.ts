import { IDataRequestDeceasedPending } from 'pages/Deceased/types';

export type UploadFileType =
  | 'hardShip'
  | 'deathCertificate'
  | 'bankruptcyDocument';

export interface UploadRequestBody extends CommonRequest {
  fileName: string;
  file: string;
  documentClassification: string;
}

export interface CollectionFormState {
  [storeId: string]: CollectionForm;
}

export interface TalkingPoints {
  loading?: boolean;
  data?: string;
}

export interface CollectionForm {
  customCallResult?: Array<CallResult>;
  callResultRefData?: Array<CallResult>;
  defaultCallResultRefData?: Array<CallResult>;
  selectedCallResult?: CallResult;
  lastFollowUpData?: LastFollowUpData;
  collectionCurrentData?: CollectionCurrentData;
  currentView?: string;
  isLoadingLatestCollectionInfo?: boolean;
  isLoadingCallResultRefData?: boolean;
  isLoadingLastFollowUp?: boolean;
  isOpenModal?: boolean;
  isMemoSectionCollapsed?: boolean;
  viewName?: Array<string>;
  method?: string;
  disabledOk?: boolean;
  talkingPoint?: TalkingPoints;
  last10Actions?: {
    loading?: boolean;
    data?: any;
    error?: boolean;
    isOpen?: boolean;
  };
  dirtyForm?: Record<string, boolean>;
  isInProgress?: boolean;
  inProgressInfo?: MagicKeyValue;
  // just using for store delay message
  lastDelayCallResult?: string;
  uploadFiles: {
    openModal?: boolean;
    type?: UploadFileType;
  };
  caseDocumentFiles?: {
    [type in UploadFileType]?: {
      loading?: boolean;
      files?: MagicKeyValue[];
      error?: boolean;
    };
  };
}

export interface RawStoreInprogressData {
  storeData?: CollectionDelayProgress;
}

export interface StoreInprogressData {
  storeData?: CollectionDelayProgress<string>;
}

export interface CollectionDelayProgress<
  T = MagicKeyValue | Array<MagicKeyValue>
> {
  [id: string]: {
    isInProgress?: boolean;
    lastDelayCallResult?: string;
    inProgressInfo?: T;
  };
}
export interface LastFollowUpData extends MagicKeyValue {
  callResultType?: string;
}
export interface CollectionCurrentData
  extends MagicKeyValue,
    IDataRequestDeceasedPending {}

export interface GetCallResultsRefDataArgs {
  storeId: string;
}

export interface GetCallResultRequest {
  common: {
    agent: string;
    clientNumber: string;
    system: string;
    prin: string;
    app: string;
  };
  clientInfoId: string;
}

export interface CallResultItem {
  actionCode?: string;
  actionCodeDescription?: string;
  active?: boolean;
}
export interface GetCallResultResponse {
  clientInfo?: {
    actionCodes?: Array<CallResultItem>;
  };
}

export interface GetCallResultsRefDataPayload {
  data: Array<CallResult>;
  defaultCallResult: Array<CallResult>;
  customCallResult: Array<CallResult>;
}

export interface CollectionLatestInfoArg
  extends CommonAccountIdRequestBody,
    StoreIdPayload {}

export interface CollectionViewNamePayload extends StoreIdPayload {
  viewName?: Array<string>;
}

export interface CollectionFormDetailPayload {
  callResultType?: string;
  data: CollectionCurrentData;
  method?: string;
}

export interface CollectionFormDetailArg extends StoreIdPayload {
  postData: CommonCollectionFormPayload;
}

export interface CommonCollectionFormPayload
  extends CommonAccountIdRequestBody {
  callResultType?: string;
}

export interface DisabledOkPayload extends StoreIdPayload {
  disabledOk: boolean;
}

export interface CallResultSelectPayload extends StoreIdPayload {
  item?: CallResult;
}

export interface CommonCollectionFormPayload
  extends CommonAccountIdRequestBody {
  callResultType?: string;
}

export interface CallResult {
  description: string;
  code: string;
  countAsContact?: boolean;
}

export interface ChangeMethodPayloadAction {
  storeId: string;
  method: string;
}

export interface GetTalkingPointPayload {
  panelContentText: string;
}

export interface GetTalkingPointArgs {
  storeId: string;
  accEValue: string;
}

export interface GetLatestActionArgs {
  storeId: string;
}

export interface GetLatestActionPayload {
  latestCallResultCode?: string;
}

export interface ActionEntryData {
  actionEntryPrefixCode: string;
}

// -----------LAST 25 ACTIONS ----------------
export interface Last25ActionData {
  actionEntryCd: string;
  collectorCd: string;
  actionEntryDate: string;
  description: string;
  sequenceNbr: string;
  actionEntryPrefixCode: string;
}

export interface Last25ActionRequest extends CommonAccountIdRequestBody {
  startSequence: number;
  endSequence: number;
}
export interface Last25ActionPayload {
  data: Last25ActionData[];
}

export interface GetLast25ActionArgs extends ArgsCommon {}

export interface ClientInfoGetByIdRequest {
  common: {
    agent: string;
    clientNumber: string;
    system: string;
    prin: string;
    app: string;
  };
  clientInfoId: string;
  enableFallback: boolean;
}

export interface ActionCodesTypes {
  actionCode: string;
  actionCodeDescription: string;
  actionCodeKey: string;
  nextWorkDateVariable: string;
  memoText: string;
  active: true;
}

// -------------------------------------------

export interface DelayFunctionArgs extends StoreIdPayload {
  delayFunction: AsyncThunkActionPayloadAction;
}

export interface InProgressInfoPayload extends StoreIdPayload {
  info?: MagicKeyValue;
  lastDelayCallResult?: string;
}

export interface ListDelayProgress {
  [id: string]: NodeJS.Timeout | undefined;
}

// -----------UPLOAD FILES----------------------------------
export interface GetListDocumentRequest extends CommonRequest {
  documentClassification: string;
}

export interface GetListDocumentsArgs extends StoreIdPayload {
  type: UploadFileType;
}

export interface DownloadCaseDocumentArgs extends StoreIdPayload {
  postData: {
    fileName: string;
    uuid: string;
  };
  type?: UploadFileType;
}

export interface DownloadCaseDocumentRequest extends CommonRequest {
  fileName: string;
  uuid: string;
}

export interface IResponseGetLatestAction {
  actionEntryFields: Array<{ actionEntryPrefixCode: string }>;
}
