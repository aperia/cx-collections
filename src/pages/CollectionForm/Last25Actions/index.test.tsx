import React from 'react';
import { storeId, mockActionCreator, renderMockStoreId } from 'app/test-utils';
import Last25Actions from '.';
import { collectionFormActions } from '../_redux/reducers';
import { fireEvent } from '@testing-library/react';

const initialState: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      last10Actions: {
        data: [],
        error: false,
        isOpen: true,
        loading: false
      }
    }
  }
};

const errorState: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      last10Actions: {
        data: [],
        error: true,
        isOpen: false,
        loading: false
      }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<Last25Actions />, {
    initialState
  });
};

const collectionFormActionsSpy = mockActionCreator(collectionFormActions);

describe('Last25Actions', () => {
  it('render component with error', () => {
    const { wrapper } = renderWrapper(errorState);
    expect(wrapper.container.innerHTML).toEqual('');
  });

  it('render component', () => {
    const { wrapper } = renderWrapper(initialState);
    expect(wrapper.getByText('txt_last_25_actions')).toBeInTheDocument();
  });

  it('handleCloseModal', () => {
    const spy = collectionFormActionsSpy('toggleLast10Actions');
    renderWrapper(initialState);
    const button = document.querySelector('.icon.icon-close')!;
    fireEvent.click(button);
    expect(spy).toHaveBeenCalledWith({
      storeId
    });
  });
});
