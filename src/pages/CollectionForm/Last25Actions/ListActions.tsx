import React, { useCallback, useEffect, useMemo } from 'react';
import { Icon, SimpleBar } from 'app/_libraries/_dls/components';
import ActionItem from './ActionItem';
import { FailedApiReload } from 'app/components';

import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useDispatch } from 'react-redux';
import { collectionFormActions } from '../_redux/reducers';

import { last25ActionsSelector } from '../_redux/selectors';

import classnames from 'classnames';

import { Last25ActionData } from '../types';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { genAmtId } from 'app/_libraries/_dls/utils';

const ListActions = () => {
  const dispatch = useDispatch();
  const { storeId, accEValue } = useAccountDetail();
  const { t } = useTranslation();
  const data = useStoreIdSelector<Last25ActionData[]>(
    last25ActionsSelector.selectData
  );
  const loading = useStoreIdSelector<boolean>(
    last25ActionsSelector.selectLoading
  );

  const error = useStoreIdSelector<boolean>(last25ActionsSelector.selectError);

  const fetchLast10Actions = useCallback(() => {
    dispatch(
      collectionFormActions.getLast25ActionsRequest({
        storeId,
        postData: { accountId: accEValue }
      })
    );
  }, [accEValue, dispatch, storeId]);

  const handleReload = useCallback(() => {
    fetchLast10Actions();
  }, [fetchLast10Actions]);

  useEffect(() => {
    fetchLast10Actions();
  }, [fetchLast10Actions]);

  const renderBody = useMemo(() => {
    if (loading) return null;
    if (error)
      return (
        <FailedApiReload
          onReload={handleReload}
          id={`${storeId}__last_10_actions`}
          dataTestId="lastActions-load-failed"
        />
      );
    if (!(data || []).length)
      return (
        <div className="text-center my-40">
          <Icon
            name="action-list"
            className="fs-80 color-light-l12"
            dataTestId="lastActions-action-list-icon"
          />
          <p
            className="mt-20 text-secondary"
            data-testid={genAmtId('lastActions', 'no-data', '')}
          >
            {t(I18N_COLLECTION_FORM.LAST_25_ACTIONS_NO_DATA)}
          </p>
        </div>
      );

    return data.map((item, index) => {
      const lastItem = index === data.length - 1;
      return (
        <ActionItem
          key={item.sequenceNbr}
          {...item}
          lastItem={lastItem}
          dataTestId={`${index}-action-item`}
        />
      );
    });
  }, [data, error, handleReload, loading, storeId, t]);

  return (
    <SimpleBar>
      <div className={classnames(loading, 'p-24')}>{renderBody}</div>
    </SimpleBar>
  );
};

export default ListActions;
