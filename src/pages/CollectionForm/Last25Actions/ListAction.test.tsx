import React from 'react';
import {
  storeId,
  mockActionCreator,
  renderMockStoreId,
  accEValue
} from 'app/test-utils';
import ListActions from './ListActions';
import { collectionFormActions } from '../_redux/reducers';
import { fireEvent, waitFor } from '@testing-library/react';

const initialState: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      last10Actions: {
        data: [
          {
            actionEntryCd: '123',
            collectorCd: '123',
            actionEntryDate: '123',
            description: '123',
            sequenceNbr: '123',
            actionEntryPrefixCode: '123'
          },
          {
            actionEntryCd: '124',
            collectorCd: '124',
            actionEntryDate: '124',
            description: '124',
            sequenceNbr: '124',
            actionEntryPrefixCode: '124'
          }
        ],
        error: false,
        isOpen: true,
        loading: false
      }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<ListActions />, {
    initialState
  });
};

const collectionFormActionsSpy = mockActionCreator(collectionFormActions);

describe('ListActions', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterAll(() => {
    jest.clearAllMocks();
    jest.clearAllTimers();
  });

  it('render component with loading', () => {
    waitFor(() => {
      renderWrapper({
        collectionForm: {
          [storeId]: {
            last10Actions: {
              data: {},
              error: true,
              isOpen: false,
              loading: true
            }
          }
        }
      });
      const simpleBar = document.querySelector('.p-24');
      expect(simpleBar?.innerHTML).toEqual('');
    });
  });

  it('render component with error', () => {
    // waitFor(() => {
    const spy = collectionFormActionsSpy('getLast25ActionsRequest');
    const { wrapper } = renderWrapper({
      collectionForm: {
        [storeId]: {
          last10Actions: {
            data: {},
            error: true,
            isOpen: false,
            loading: false
          }
        }
      }
    });

    const button = wrapper.getByText('txt_reload')!;
    fireEvent.click(button);

    expect(spy).toHaveBeenCalledWith({
      storeId,
      postData: { accountId: accEValue }
    });
  });

  it('render component with no data', () => {
    waitFor(() => {
      const { wrapper } = renderWrapper({
        collectionForm: {
          [storeId]: {
            last10Actions: {
              error: false,
              isOpen: false,
              loading: false
            }
          }
        }
      });
      expect(
        wrapper.getByText('txt_no_actions_to_display')
      ).toBeInTheDocument();
    });
  });

  it('render component with data', () => {
    const { wrapper } = renderWrapper(initialState);
    expect(wrapper.getAllByText('txt_collector_code')[0]).toBeInTheDocument();
  });
});
