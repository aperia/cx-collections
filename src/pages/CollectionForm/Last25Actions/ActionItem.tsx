import React from 'react';
import { Last25ActionData } from '../types';
import DateTime from 'date-and-time';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

interface ActionItemProps extends Last25ActionData {
  lastItem: boolean;
  dataTestId?: string;
}

const ActionItem: React.FC<ActionItemProps> = ({
  actionEntryDate,
  collectorCd,
  sequenceNbr,
  description,
  actionEntryPrefixCode,
  lastItem,
  dataTestId
}) => {
  const { t } = useTranslation();
  return (
    <div>
      <div className="d-flex justify-content-between">
        <div
          className="fw-500 fs-14"
          data-testid={genAmtId(dataTestId!, 'entryPrefixCode', '')}
        >
          {actionEntryPrefixCode}
        </div>
        <div data-testid={genAmtId(dataTestId!, 'entryDate', '')}>
          {DateTime.format(
            DateTime.parse(`20${actionEntryDate}`, 'YYYYMMDD'),
            'MM/DD/YYYY'
          )}
        </div>
      </div>
      <div className="d-flex justify-content-between fs-12 mt-4 color-grey">
        <div>
          <span data-testid={genAmtId(dataTestId!, 'collectorCd-label', '')}>
            {t('txt_collector_code')}{' '}
          </span>
          <span data-testid={genAmtId(dataTestId!, 'collectorCd', '')}>
            {collectorCd}
          </span>
        </div>
        <span data-testid={genAmtId(dataTestId!, 'sequenceNbr', '')}>
          {sequenceNbr}
        </span>
      </div>
      {description && (
        <div
          className="mt-8"
          data-testid={genAmtId(dataTestId!, 'description', '')}
        >
          {description}
        </div>
      )}
      {!lastItem && <hr />}
    </div>
  );
};

export default ActionItem;
