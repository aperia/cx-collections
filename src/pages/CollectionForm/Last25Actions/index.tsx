import React from 'react';
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import ListActions from './ListActions';

import { last25ActionsSelector } from '../_redux/selectors';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { collectionFormActions } from '../_redux/reducers';
import { useDispatch } from 'react-redux';
import { I18N_COLLECTION_FORM, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

const Last25Actions = () => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();

  const handleCloseModal = () => {
    dispatch(collectionFormActions.toggleLast10Actions({ storeId }));
  };

  const modalOpen = useStoreIdSelector<boolean>(
    last25ActionsSelector.selectModalOpen
  );

  const loading = useStoreIdSelector<boolean>(
    last25ActionsSelector.selectLoading
  );

  if (!modalOpen) return null;
  return (
    <div>
      <Modal
        show={modalOpen}
        rt
        loading={loading}
        dataTestId="last25ActionsModal"
      >
        <ModalHeader
          border
          onHide={handleCloseModal}
          closeButton
          dataTestId="last25ActionsModal-header"
        >
          <ModalTitle dataTestId="last25ActionsModal-header-title">
            {t(I18N_COLLECTION_FORM.LAST_25_ACTIONS)}
          </ModalTitle>
        </ModalHeader>
        <ModalBody
          noPadding
          dataTestId={genAmtId(
            'last25ActionsModal',
            'last25ActionsModal_modalBody',
            'ModalBody'
          )}
        >
          <ListActions />
        </ModalBody>
        <ModalFooter
          dataTestId="last25ActionsModal-footer"
          border
          cancelButtonText={t(I18N_COMMON_TEXT.CLOSE)}
          onCancel={handleCloseModal}
        />
      </Modal>
    </div>
  );
};

export default Last25Actions;
