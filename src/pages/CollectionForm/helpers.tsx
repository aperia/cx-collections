import React from 'react';

// Const
import {
  CALL_RESULT_CODE,
  COLLECTION_EVENT,
  COLLECTION_FORM_DELAY_STATUS,
  removeItems
} from './constants';
import { EMPTY_ARRAY, EMPTY_STRING } from 'app/constants';

// Types
import {
  CallResult,
  CollectionDelayProgress,
  DelayFunctionArgs,
  RawStoreInprogressData,
  StoreInprogressData,
  UploadFileType
} from './types';

// Helper
import { getSessionStorage, setSessionStorage } from 'app/helpers';
import { collectionFormActions } from './_redux/reducers';

export const isRemoveEditCollectionForm = (type: string) => {
  return removeItems.includes(type);
};

export const callResultsComponentDisc = new Map<
  string,
  {
    component: React.FC<unknown>;
  }
>([
  [
    CALL_RESULT_CODE.BANKRUPTCY_PENDING,
    {
      component: React.lazy(
        () => import('pages/Bankruptcy/BankruptcyForm/BankruptcyPendingForm')
      )
    }
  ],
  [
    CALL_RESULT_CODE.NOT_DECEASED,
    {
      component: React.lazy(() => import('pages/Deceased/DeceasedForm'))
    }
  ],
  [
    CALL_RESULT_CODE.BANKRUPTCY,
    {
      component: React.lazy(
        () => import('pages/Bankruptcy/BankruptcyForm/BankruptcyForm')
      )
    }
  ],
  [
    CALL_RESULT_CODE.DECEASE_PENDING,
    {
      component: React.lazy(
        () => import('pages/Deceased/DeceasedPendingForm/index')
      )
    }
  ],
  [
    CALL_RESULT_CODE.DECEASE,
    {
      component: React.lazy(() => import('pages/Deceased/DeceasedForm'))
    }
  ],
  [
    CALL_RESULT_CODE.NOT_BANKRUPTCY,
    {
      component: React.lazy(
        () => import('pages/Bankruptcy/BankruptcyForm/BankruptcyForm')
      )
    }
  ],
  [
    CALL_RESULT_CODE.CCCS,
    {
      component: React.lazy(() => import('pages/CCCS/CCCSForm'))
    }
  ]
]);

export const takeCallResultsComponent: AppThunk = () => (_, getState) => {
  const state = getState();
  const storeId = state.tab?.tabStoreIdSelected || '';

  const callResultCode =
    state.collectionForm[storeId]?.selectedCallResult?.code ||
    state.collectionForm[storeId]?.lastFollowUpData?.callResultType ||
    '';
  if (callResultCode) {
    const { component: Component } =
      callResultsComponentDisc.get(callResultCode) || {};
    return Component ? <Component /> : null;
  }
  return null;
};

export const dispatchSubmitForm = () => {
  const event = new CustomEvent<string>(COLLECTION_EVENT.SUBMIT, {
    bubbles: true,
    cancelable: true,
    detail: ''
  });

  window.dispatchEvent(event);
};

export const isDefaultCallResultType = (
  code: string = EMPTY_STRING,
  listCallResult: Array<CallResult> = EMPTY_ARRAY
) => {
  return listCallResult.findIndex(item => item.code === code) !== -1;
};

export const getDelayProgressRawData = () => {
  try {
    const { storeData = {} } =
      getSessionStorage<StoreInprogressData>(COLLECTION_FORM_DELAY_STATUS) ||
      {};

    return Object.keys(storeData).reduce(
      (nextData: CollectionDelayProgress, storeId) => {
        nextData = {
          ...nextData,
          [storeId]: {
            ...storeData[storeId],
            inProgressInfo: JSON.parse(
              atob(storeData[storeId].inProgressInfo || '')
            ) as RawStoreInprogressData
          }
        };

        return nextData;
      },
      {}
    );
  } catch {
    return {};
  }
};

export const getDelayProgress = () => {
  const { storeData = {} } =
    getSessionStorage<StoreInprogressData>(COLLECTION_FORM_DELAY_STATUS) || {};

  return storeData;
};

export const setDelayProgress = (
  storeId: string,
  callResultType: string,
  info: MagicKeyValue
) => {
  try {
    const storeData = getDelayProgress();

    setSessionStorage<StoreInprogressData>(COLLECTION_FORM_DELAY_STATUS, {
      storeData: {
        ...storeData,
        [storeId]: {
          isInProgress: true,
          lastDelayCallResult: callResultType,
          inProgressInfo: btoa(JSON.stringify(info))
        }
      }
    });
  } catch (e) {
    console.log(e);
  }
};

export const dispatchDelayProgress = (
  delayFunction: AsyncThunkActionPayloadAction,
  storeId: string
) => {
  const event = new CustomEvent<DelayFunctionArgs>(
    COLLECTION_EVENT.DELAY_PROGRESS,
    {
      bubbles: false,
      cancelable: true,
      detail: {
        delayFunction,
        storeId
      }
    }
  );

  window.dispatchEvent(event);
};

export const dispatchDestroyDelayProgress = (storeId: string) => {
  const event = new CustomEvent<StoreIdPayload>(
    COLLECTION_EVENT.DESTROY_DELAY_PROGRESS,
    {
      bubbles: false,
      cancelable: true,
      detail: {
        storeId
      }
    }
  );

  window.dispatchEvent(event);
};

export const removeDelayProgress: AppThunk =
  (storeId: string) => (dispatch, _) => {
    window.delayProgress &&
      clearInterval((window.delayProgress[storeId] || 0) as number);

    window.delayProgress = Object.keys(window.delayProgress || {}).reduce(
      (nextData, ky) => {
        if (ky !== storeId) {
          nextData = {
            ...nextData,
            [ky]: window.delayProgress![ky] || 0
          };
        }
        return nextData;
      },
      {}
    );

    dispatch(collectionFormActions.removeLastDelayCallResult({ storeId }));

    const storeData = getDelayProgress();

    const filterData = Object.keys(storeData)
      .filter(item => item !== storeId)
      .reduce((nextData, item) => {
        nextData = { ...nextData, [item]: storeData[item] };
        return nextData;
      }, {});

    setSessionStorage<StoreInprogressData>(COLLECTION_FORM_DELAY_STATUS, {
      storeData: filterData
    });
  };

export const combineDocumentClassification = (
  prefix: UploadFileType,
  accountId: string,
  type?: string
) =>
  type === 'hardShip'
    ? 'HardshipMillitaryDeploymentCertificate'
    : `${prefix}_${accountId}`;
