import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { UploadFileType } from './types';

export enum CALL_RESULT_CODE {
  HARDSHIP = 'HS',
  NOT_HARDSHIP = 'HR',
  BANKRUPTCY = 'BC',
  CCCS = 'CS',
  BANKRUPTCY_PENDING = 'BP',
  NOT_BANKRUPTCY = 'BR',
  DECEASE_PENDING = 'DP',
  DECEASE = 'DC',
  NOT_DECEASED = 'ND',
  PROMISE_TO_PAY = 'PP',
  LONG_TERM_MEDICAL = 'ME',
  NOT_LONG_TERM_MEDICAL = 'NME',
  INCARCERATION = 'FD',
  NOT_INCARCERATION = 'NFD',
  REGULATORY_SPOUSE_CONTACT = 'RS',
  SETTLEMENT = 'ST'
}

export const callResultItems = [
  {
    label: 'txt_bankruptcy_(status_pending)',
    uniqueId: CALL_RESULT_CODE.BANKRUPTCY_PENDING
  },
  {
    label: 'txt_bankruptcy',
    uniqueId: CALL_RESULT_CODE.BANKRUPTCY
  },
  {
    label: 'txt_not_bankruptcy',
    uniqueId: CALL_RESULT_CODE.NOT_BANKRUPTCY
  },
  {
    label: 'txt_deceased_(status_pending)',
    uniqueId: CALL_RESULT_CODE.DECEASE_PENDING
  },
  {
    label: 'txt_deceased',
    uniqueId: CALL_RESULT_CODE.DECEASE
  },
  {
    label: 'txt_not_deceased',
    uniqueId: CALL_RESULT_CODE.NOT_DECEASED
  },
  {
    label: 'txt_promise_to_pay',
    uniqueId: CALL_RESULT_CODE.PROMISE_TO_PAY
  },
  {
    label: 'txt_cccs',
    uniqueId: CALL_RESULT_CODE.CCCS
  },
  {
    label: 'txt_hardship',
    uniqueId: CALL_RESULT_CODE.HARDSHIP
  },
  {
    label: 'txt_not_hardship',
    uniqueId: CALL_RESULT_CODE.NOT_HARDSHIP
  },
  {
    label: 'txt_long-term_medical',
    uniqueId: CALL_RESULT_CODE.LONG_TERM_MEDICAL
  },
  {
    label: 'txt_incarceration',
    uniqueId: CALL_RESULT_CODE.INCARCERATION
  },
  {
    label: 'txt_not_incarceration',
    uniqueId: CALL_RESULT_CODE.NOT_INCARCERATION
  },
  {
    label: 'txt_settlements',
    uniqueId: CALL_RESULT_CODE.SETTLEMENT
  }
];

export const DeceasedParty = {
  PRIMARY: 'primary',
  SECONDARY: 'second',
  BOTH: 'both'
};

export const removeItems: Array<string> = [
  CALL_RESULT_CODE.NOT_BANKRUPTCY,
  CALL_RESULT_CODE.NOT_DECEASED,
  CALL_RESULT_CODE.NOT_HARDSHIP,
  CALL_RESULT_CODE.PROMISE_TO_PAY
];

export enum COLLECTION_METHOD {
  EDIT = 'edit',
  CREATE = 'create'
}

export const ACTION_ENTRY = {
  DP: CALL_RESULT_CODE.DECEASE_PENDING,
  DC: CALL_RESULT_CODE.DECEASE,
  ND: CALL_RESULT_CODE.NOT_DECEASED,
  BC: CALL_RESULT_CODE.BANKRUPTCY,
  BP: CALL_RESULT_CODE.BANKRUPTCY_PENDING,
  BR: CALL_RESULT_CODE.NOT_BANKRUPTCY,
  PP: CALL_RESULT_CODE.PROMISE_TO_PAY,
  CS: CALL_RESULT_CODE.CCCS,
  HS: CALL_RESULT_CODE.HARDSHIP,
  ME: CALL_RESULT_CODE.LONG_TERM_MEDICAL,
  FD: CALL_RESULT_CODE.INCARCERATION,
  HR: CALL_RESULT_CODE.NOT_HARDSHIP,
  ST: CALL_RESULT_CODE.SETTLEMENT
} as MagicKeyValue;

export const CALL_REF_DATA = 'refData/callResultRefData.json';

/** use to filter call result list base on their status CXC-3229 */
export const DECEASED_GROUP_CODES = [
  CALL_RESULT_CODE.DECEASE_PENDING,
  CALL_RESULT_CODE.DECEASE,
  CALL_RESULT_CODE.NOT_DECEASED
];

export const BANKRUPTCY_GROUP_CODES = [
  CALL_RESULT_CODE.BANKRUPTCY_PENDING,
  CALL_RESULT_CODE.BANKRUPTCY,
  CALL_RESULT_CODE.NOT_BANKRUPTCY
];

export const PROMISE_GROUP_CODES = [CALL_RESULT_CODE.PROMISE_TO_PAY];

export const HARDSHIP_GROUP_CODES = [
  CALL_RESULT_CODE.HARDSHIP,
  CALL_RESULT_CODE.NOT_HARDSHIP
];

export const LONG_TERM_MEDICAL_GROUP_CODES = [
  CALL_RESULT_CODE.LONG_TERM_MEDICAL,
  CALL_RESULT_CODE.NOT_LONG_TERM_MEDICAL
];

export const INCARCERATION_GROUP_CODES = [
  CALL_RESULT_CODE.INCARCERATION,
  CALL_RESULT_CODE.NOT_INCARCERATION
];

export enum COLLECTION_EVENT {
  SUBMIT = 'SubmitCollectionForm',
  DELAY_PROGRESS = 'CollectionFormDelayProgress',
  DESTROY_DELAY_PROGRESS = 'CollectionFormDestroyDelayProgress'
}

export const DEFAULT_DELAY_TIME = 30000;
export const COLLECTION_FORM_DELAY_STATUS = 'collectionInProgressStatus';

export const DECEASED_STATUS_MAPPING = [
  {
    code: CALL_RESULT_CODE.DECEASE_PENDING,
    deceasedConfirmation: 'Deceased Pending'
  },
  {
    code: CALL_RESULT_CODE.NOT_DECEASED,
    deceasedConfirmation: 'Not Deceased'
  },
  {
    code: CALL_RESULT_CODE.DECEASE,
    deceasedConfirmation: 'Deceased Confirmed'
  }
];

export const STATUS_HARDSHIP = 'IN_PROGRESS';
export const STATUS_HARDSHIP_SETUP = 'COMPLETE';
export const UPLOAD_FILE_MODAL_TEXT: Record<
  UploadFileType,
  {
    title: string;
    description: string;
    toastMessage: string;
  }
> = {
  deathCertificate: {
    title: I18N_COLLECTION_FORM.UPLOAD_DEATH_CERTIFICATE,
    description: I18N_COLLECTION_FORM.DECEASED_UPLOAD_CERTIFICATE_DESCRIPTION,
    toastMessage: I18N_COLLECTION_FORM.DEATH_CERTIFICATES_UPLOADED
  },
  hardShip: {
    title: I18N_COLLECTION_FORM.UPLOAD_HARDSHIP_TITLE,
    description: I18N_COLLECTION_FORM.UPLOAD_HARDSHIP_DESCRIPTION,
    toastMessage: I18N_COLLECTION_FORM.UPLOAD_HARDSHIP_UPLOADED
  },
  bankruptcyDocument: {
    title: I18N_COLLECTION_FORM.UPLOAD_BANKRUPTCY_TITLE,
    description: I18N_COLLECTION_FORM.UPLOAD_BANKRUPTCY_DESCRIPTION,
    toastMessage: I18N_COLLECTION_FORM.UPLOAD_BANKRUPTCY_UPLOADED
  }
};
