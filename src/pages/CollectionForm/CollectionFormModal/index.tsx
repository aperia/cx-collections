import React from 'react';

// Components
import BodyModal from './Body';
import ModalHeader from './Header';
import ModalFooter from './Footer';
import { Modal } from 'app/_libraries/_dls/components';

// Hooks
import { useStoreIdSelector } from 'app/hooks';

// Actions
import {
  takeCollectionFormModalLoading,
  takeOpenFormStatus
} from '../_redux/selectors';

export interface CollectionFormProps {}

const CollectionFormModal: React.FC<CollectionFormProps> = () => {
  const isUpdateLoading = useStoreIdSelector<boolean>(
    takeCollectionFormModalLoading
  );

  const isOpenForm = useStoreIdSelector<boolean>(takeOpenFormStatus);

  if (!isOpenForm) return null;

  return (
    <Modal
      full
      show={isOpenForm}
      loading={isUpdateLoading}
      enforceFocus={false}
      classes={{ content: 'collection-form' }}
      dataTestId="collectionForm_modal"
    >
      <ModalHeader />
      <BodyModal />
      <ModalFooter />
    </Modal>
  );
};

export default CollectionFormModal;
