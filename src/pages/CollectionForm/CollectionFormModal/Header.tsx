import React, { useEffect } from 'react';

// components
import {
  Button,
  Icon,
  ModalHeader as ModalHeaderDLS,
  ModalTitle
} from 'app/_libraries/_dls/components';
import isEmpty from 'lodash.isempty';
import TimerWorking from 'pages/TimerWorking';

// Hooks
import { useDispatch } from 'react-redux';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Redux
import { collectionFormActions } from '../_redux/reducers';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import {
  selectCollectionMethod,
  selectLastFollowUpData,
  takeDirtyForm,
  takeOpenFormStatus,
  takeSelectedCallResult
} from '../_redux/selectors';
// i18n
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';

import { CALL_RESULT_CODE, COLLECTION_METHOD } from '../constants';
import { CallResult, LastFollowUpData } from '../types';

const ModalHeader: React.FC = () => {
  const { EDIT_COLLECTION_FORM, CREATE_COLLECTION_FORM } = I18N_COLLECTION_FORM;
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();

  const isOpenForm = useStoreIdSelector<boolean>(takeOpenFormStatus);
  const method = useStoreIdSelector<string>(selectCollectionMethod);

  const lastFollowUpData = useStoreIdSelector<LastFollowUpData>(
    selectLastFollowUpData
  );
  const dirtyForm = useStoreIdSelector<Record<string, number>>(takeDirtyForm);
  const callResultSelected = useStoreIdSelector<CallResult>(
    takeSelectedCallResult
  );

  const handleCloseForm = () => {
    dispatch(collectionFormActions.toggleModal({ storeId }));
    dispatch(
      collectionFormActions.changeDisabledOk({
        storeId,
        disabledOk: false
      })
    );
  };

  const invokeCloseForm = () => {
    const currentCollectionForm =
      callResultSelected?.code || lastFollowUpData?.callResultType;

    // confirm modal
    if (
      (currentCollectionForm === CALL_RESULT_CODE.BANKRUPTCY &&
        dirtyForm[CALL_RESULT_CODE.BANKRUPTCY]) ||
      (currentCollectionForm === CALL_RESULT_CODE.BANKRUPTCY_PENDING &&
        dirtyForm[CALL_RESULT_CODE.BANKRUPTCY_PENDING]) ||
      (currentCollectionForm === CALL_RESULT_CODE.CCCS &&
        dirtyForm[CALL_RESULT_CODE.CCCS]) ||
      (currentCollectionForm === CALL_RESULT_CODE.DECEASE &&
        dirtyForm[CALL_RESULT_CODE.DECEASE]) ||
      (currentCollectionForm === CALL_RESULT_CODE.DECEASE_PENDING &&
        dirtyForm[CALL_RESULT_CODE.DECEASE_PENDING])
    ) {
      return dispatch(
        confirmModalActions.open({
          title: 'txt_unsaved_changes',
          body: 'txt_discard_change_body',
          cancelText: 'txt_continue_editing',
          confirmText: 'txt_discard_change',
          confirmCallback: handleCloseForm,
          variant: 'danger'
        })
      );
    }
    handleCloseForm();
  };

  useEffect(() => {
    return () => {
      dispatch(
        collectionFormActions.clearSelectedCallResult({
          storeId
        })
      );
    };
  }, [dispatch, storeId]);

  const renderTitle = () => {
    if (!isEmpty(lastFollowUpData)) {
      if (method === COLLECTION_METHOD.CREATE) return CREATE_COLLECTION_FORM;
      return EDIT_COLLECTION_FORM;
    }

    return CREATE_COLLECTION_FORM;
  };

  if (!isOpenForm) return null;

  return (
    <ModalHeaderDLS
      dataTestId="collectionForm_header"
      border
      className="border-bottom"
    >
      <ModalTitle
        dataTestId="collectionForm_header_title"
        className="d-flex align-items-center"
      >
        {t(renderTitle())}
        <TimerWorking className="ml-16" dataTestId="collectionForm_header_timmer" />
      </ModalTitle>
      <Button
        id={`collectionHeaderBtn-${storeId}`}
        dataTestId="collectionForm_header_close-btn"
        variant="icon-secondary"
        onClick={invokeCloseForm}
      >
        <Icon name="close" size="5x" />
      </Button>
    </ModalHeaderDLS>
  );
};

export default ModalHeader;
