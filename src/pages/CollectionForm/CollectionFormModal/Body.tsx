import React, { Suspense, useEffect, useMemo } from 'react';
import classNames from 'classnames';
import {
  ModalBody as ModalBodyDLS,
  SimpleBar
} from 'app/_libraries/_dls/components';
import isEmpty from 'lodash.isempty';
import { useDispatch } from 'react-redux';

// Components
import MemosCollectionForm from '../Memos';
import LazyLoadingFallback from 'app/components/LazyLoadingFallback';
import SelectedCallResult from './SelectedCallResult';

// Helpers
import { takeCallResultsComponent } from '../helpers';

// Hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Types
import { CallResult } from '../types';

// i18n
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';

// Selector
import {
  selectCollapseMemoStatus,
  selectLastFollowUpData,
  takeCollectionLoadingStatus,
  takeSelectedCallResult
} from '../_redux/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { useApiErrorNotification } from 'pages/ApiErrorNotification/hooks/useApiErrorNotification';

const ModalBody: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();

  const callResultSelected = useStoreIdSelector<CallResult>(
    takeSelectedCallResult
  );

  const ApiErrorDetail = useApiErrorNotification({
    storeId,
    forSection: 'inCollectionFormModal'
  });

  const lastCallResultSelected = useStoreIdSelector(selectLastFollowUpData);

  const isLoading = useStoreIdSelector<boolean>(takeCollectionLoadingStatus);

  const collapsed = useStoreIdSelector<boolean>(selectCollapseMemoStatus);

  /**
   * Function render collection component mapping
   * with each call result item
   * Ex: Bankruptcy item - Bankruptcy component
   **/
  const renderCollectionComponent = useMemo(() => {
    if (
      isEmpty(callResultSelected) &&
      isEmpty(lastCallResultSelected) &&
      isLoading
    )
      return null;

    return dispatch(takeCallResultsComponent());
  }, [dispatch, callResultSelected, lastCallResultSelected, isLoading]);

  useEffect(() => {
    //  clear data when modal is close
    return () => {
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          forSection: 'inCollectionFormModal',
          storeId
        })
      );
    };
  }, [dispatch, storeId]);

  return (
    <ModalBodyDLS
      noPadding
      dataTestId={genAmtId(
        'collectionForm',
        'callResult_modalBody',
        'ModalBody'
      )}
    >
      <div className="h-100 d-flex flex-row">
        <div
          className={classNames('h-100 w-100 flex-1', {
            loading: isLoading
          })}
        >
          <SimpleBar>
            <div className="mx-auto p-24 max-width-lg">
              <ApiErrorDetail
                dataTestId="collectionForm-errorDetail"
                className="ml-0 mt-0 mb-24"
              />
              <h5
                className="mb-16"
                data-testid={genAmtId(
                  'collectionForm',
                  'callResult',
                  'CollectionFormModal'
                )}
              >
                {t(I18N_COLLECTION_FORM.CALL_RESULT)}
              </h5>
              <div
                className={classNames('row', {
                  'memos-extend': !collapsed
                })}
              >
                <SelectedCallResult />
              </div>
            </div>
            <Suspense fallback={<LazyLoadingFallback />}>
              {renderCollectionComponent}
            </Suspense>
          </SimpleBar>
        </div>

        <MemosCollectionForm />
      </div>
    </ModalBodyDLS>
  );
};

export default ModalBody;
