import React, { useMemo } from 'react';
import isUndefined from 'lodash.isundefined';

// components
import { Icon } from 'app/_libraries/_dls/components';

// Hooks
import { useStoreIdSelector } from 'app/hooks';

// Types
import { CallResult, LastFollowUpData } from '../../types';
import { callResultItems, CALL_RESULT_CODE } from '../../constants';
import {
  selectLastFollowUpData,
  takeSelectedCallResult
} from '../../_redux/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface CallResultProps {}

const SelectedCallResult: React.FC<CallResultProps> = () => {
  const { t } = useTranslation();
  const callResultSelected = useStoreIdSelector<CallResult>(
    takeSelectedCallResult
  );
  const lastFollowUpData = useStoreIdSelector<LastFollowUpData>(
    selectLastFollowUpData
  );

  const renderCallResultDescription = () => {
    const resultItem = callResultItems.find(
      item => item.uniqueId === lastFollowUpData?.callResultType
    );
    if (isUndefined(callResultSelected)) return resultItem?.label;
    return callResultSelected?.description;
  };

  const columnStyle = useMemo(() => {
    const deceased = [
      CALL_RESULT_CODE.DECEASE,
      CALL_RESULT_CODE.DECEASE_PENDING,
      CALL_RESULT_CODE.NOT_DECEASED
    ];
    const resultType =
      callResultSelected?.code || lastFollowUpData?.callResultType;
    if (deceased.includes(resultType as CALL_RESULT_CODE)) {
      return 'col-extend-md-12 col-md-4 col-extend-lg-6 col-xl-3 col-extend-xl-4 col-extend-xxl-3';
    } else {
      return 'col-xl-3 col-lg-4 col-extend-xl-4 col-extend-lg-6 col-md-6 col-extend-md-12';
    }
  }, [callResultSelected?.code, lastFollowUpData?.callResultType]);

  if (
    (callResultSelected?.code || lastFollowUpData?.callResultType) ===
    CALL_RESULT_CODE.CCCS
  ) {
    return null;
  }

  return (
    <div className={columnStyle}>
      <div className="d-flex bg-light-l16 p-8 align-items-center rounded-lg">
        <div className="bg-light-d20 justify-content-center align-items-center rounded-lg mr-12">
          <Icon className="p-8 color-white" name={'megaphone'} size="4x"></Icon>
        </div>
        <p
          data-testid={genAmtId(
            'collectionForm',
            'call-result-description',
            'SelectedCallResult'
          )}
        >
          {t(renderCallResultDescription())}
        </p>
      </div>
    </div>
  );
};

export default SelectedCallResult;
