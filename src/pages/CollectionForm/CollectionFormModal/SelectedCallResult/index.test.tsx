import React from 'react';
import { storeId, renderMockStoreId } from 'app/test-utils';
import SelectedCallResult from '.';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { queryByClass } from 'app/_libraries/_dls/test-utils';

const initialState: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      selectedCallResult: {
        code: CALL_RESULT_CODE.BANKRUPTCY,
        description: 'des'
      },
      lastFollowUpData: {
        callResultType: CALL_RESULT_CODE.BANKRUPTCY
      }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<SelectedCallResult />, {
    initialState
  });
};

describe('SelectedCallResult', () => {
  it('render component', () => {
    const { wrapper } = renderWrapper(initialState);
    expect(wrapper.getByText('des')).toBeInTheDocument();
  });

  it('render component with empty callResultSelected', () => {
    const { wrapper } = renderWrapper({
      collectionForm: {
        [storeId]: {
          selectedCallResult: undefined,
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.DECEASE
          }
        }
      }
    });
    const icon = queryByClass(wrapper.container, /icon-megaphone/)!;
    expect(icon.innerHTML).toEqual('');
  });

  it('render component with decease type', () => {
    const { wrapper } = renderWrapper({
      collectionForm: {
        [storeId]: {
          selectedCallResult: {
            code: CALL_RESULT_CODE.DECEASE,
            description: 'des'
          },
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.DECEASE
          }
        }
      }
    });
    expect(wrapper.getByText('des')).toBeInTheDocument();
  });

  it('render component with error', () => {
    const { wrapper } = renderWrapper({
      collectionForm: {
        [storeId]: {
          selectedCallResult: {
            code: CALL_RESULT_CODE.CCCS,
            description: 'des'
          },
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.CCCS
          }
        }
      }
    });
    expect(wrapper.container.innerHTML).toEqual('');
  });
});
