import React, { useEffect } from 'react';

// components
import { Button, ModalFooter } from 'app/_libraries/_dls/components';

// Helpers
import { dispatchSubmitForm } from '../helpers';

// Hooks
import { batch, useDispatch, useSelector } from 'react-redux';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useDisableForm } from '../hooks/useDisableForm';

// Redux
import { collectionFormActions } from '../_redux/reducers';
import {
  selectCollectionMethod,
  selectLastFollowUpData,
  takeCollectionLoadingStatus,
  takeDirtyForm,
  takeOpenFormStatus,
  takeSelectedCallResult
} from '../_redux/selectors';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { actionsLiveVox } from 'app/livevox-dialer/_redux/reducers';
import * as liveVoxSelectors from 'app/livevox-dialer/_redux/selectors';

// Const
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { CALL_RESULT_CODE, COLLECTION_METHOD } from '../constants';

// Types
import { CallResult, LastFollowUpData } from '../types';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { LiveVoxCallResultArgs } from 'app/livevox-dialer/types';
import { isEmpty } from 'app/_libraries/_dls/lodash';

const ModalFooters: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const isDisabled = useDisableForm();

  const isOpenForm = useStoreIdSelector<boolean>(takeOpenFormStatus);
  const lastFollowUpData = useStoreIdSelector<LastFollowUpData>(
    selectLastFollowUpData
  );
  const method = useStoreIdSelector<string>(selectCollectionMethod);
  const isLoading = useStoreIdSelector<boolean>(takeCollectionLoadingStatus);
  const callResultSelected = useStoreIdSelector<CallResult>(
    takeSelectedCallResult
  );

  const dirtyForm = useStoreIdSelector<Record<string, number>>(takeDirtyForm);

  // LiveVox
  const liveVoxDialerSessionId = useSelector(liveVoxSelectors.getLiveVoxDialerSessionId);
  const liveVoxDialerLineNumber = useSelector(liveVoxSelectors.getLiveVoxDialerLineNumber);
  const liveVoxDialerLineActiveAndReadyData = useSelector(liveVoxSelectors.getLiveVoxDialerLineActiveAndReadyData);

  const handleCloseForm = () => {
    batch(() => {
      dispatch(collectionFormActions.toggleModal({ storeId }));
      dispatch(
        collectionFormActions.changeMethod({
          storeId,
          method: COLLECTION_METHOD.CREATE
        })
      );
      dispatch(
        collectionFormActions.changeDisabledOk({
          storeId,
          disabledOk: false
        })
      );
    });
  };

  const invokeCloseForm = () => {
    const currentCollectionForm =
      callResultSelected?.code || lastFollowUpData?.callResultType;

    // confirm modal
    if (
      (currentCollectionForm === CALL_RESULT_CODE.BANKRUPTCY &&
        dirtyForm[CALL_RESULT_CODE.BANKRUPTCY]) ||
      (currentCollectionForm === CALL_RESULT_CODE.BANKRUPTCY_PENDING &&
        dirtyForm[CALL_RESULT_CODE.BANKRUPTCY_PENDING]) ||
      (currentCollectionForm === CALL_RESULT_CODE.CCCS &&
        dirtyForm[CALL_RESULT_CODE.CCCS]) ||
      (currentCollectionForm === CALL_RESULT_CODE.DECEASE &&
        dirtyForm[CALL_RESULT_CODE.DECEASE]) ||
      (currentCollectionForm === CALL_RESULT_CODE.DECEASE_PENDING &&
        dirtyForm[CALL_RESULT_CODE.DECEASE_PENDING])
    ) {
      return dispatch(
        confirmModalActions.open({
          title: 'txt_unsaved_changes',
          body: 'txt_discard_change_body',
          cancelText: 'txt_continue_editing',
          confirmText: 'txt_discard_change',
          confirmCallback: handleCloseForm,
          variant: 'danger'
        })
      );
    }
    handleCloseForm();
  };

  const handleSubmitForm = () => {
    if (!isLoading) {
      if (liveVoxDialerLineActiveAndReadyData && !isEmpty(liveVoxDialerLineActiveAndReadyData.accountNumber)) {
        const liveVoxTermCodesArgs: LiveVoxCallResultArgs = {
          liveVoxDialerSessionId: liveVoxDialerSessionId,
          liveVoxDialerLineNumber: liveVoxDialerLineNumber,
          selectedCallResult: callResultSelected,
          serviceId: liveVoxDialerLineActiveAndReadyData.serviceId,
          callTransactionId: liveVoxDialerLineActiveAndReadyData.callTransactionId,
          callSessionId: liveVoxDialerLineActiveAndReadyData.callSessionId,
          account: liveVoxDialerLineActiveAndReadyData.accountNumber,
          phoneDialed: liveVoxDialerLineActiveAndReadyData.phoneNumber,
          moveAgentToNotReady: false
        };
        dispatch(actionsLiveVox.liveVoxDialerTermCodesThunk(liveVoxTermCodesArgs));
      }
      dispatchSubmitForm();
    }
  };

  useEffect(() => {
    return () => {
      dispatch(
        collectionFormActions.clearSelectedCallResult({
          storeId
        })
      );
    };
  }, [dispatch, storeId]);

  const renderTextButton = () => {
    if (lastFollowUpData) {
      if (method === COLLECTION_METHOD.CREATE) return I18N_COMMON_TEXT.SUBMIT;
      return I18N_COMMON_TEXT.SAVE;
    }

    return I18N_COMMON_TEXT.SUBMIT;
  };

  if (!isOpenForm) return null;

  return (
    <ModalFooter
      border
      dataTestId={genAmtId(
        'collectionForm',
        'callResult_modalFooter',
        'ModalBody'
      )}
    >
      <Button
        id={`closeCollectionForm-${storeId}`}
        dataTestId="closeCollectionForm"
        variant="secondary"
        onClick={invokeCloseForm}
      >
        {t(I18N_COMMON_TEXT.CANCEL)}
      </Button>
      <Button
        id={`saveCollectionForm-${storeId}`}
        dataTestId="saveCollectionForm"
        variant="primary"
        disabled={isDisabled}
        onClick={handleSubmitForm}
      >
        {t(renderTextButton())}
      </Button>
    </ModalFooter>
  );
};

export default ModalFooters;
