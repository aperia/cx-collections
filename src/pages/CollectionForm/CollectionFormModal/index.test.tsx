import React from 'react';
import { storeId, renderMockStoreId } from 'app/test-utils';
import CollectionFormModal from '.';

const initialState: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      isOpenModal: true
    }
  },
  bankruptcy: {
    [storeId]: {
      isLoading: false
    }
  },
  deceased: {
    [storeId]: {
      loading: false
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<CollectionFormModal />, {
    initialState
  });
};

describe('Footer', () => {
  it('render component', () => {
    const { wrapper } = renderWrapper(initialState);
    expect(wrapper.getByText('txt_create_collection_form')).toBeInTheDocument();
  });

  it('render with error', () => {
    const { wrapper } = renderWrapper({
      collectionForm: {
        [storeId]: {
          isOpenModal: false
        }
      },
      bankruptcy: {
        [storeId]: {
          isLoading: false
        }
      },
      deceased: {
        [storeId]: {
          loading: false
        }
      }
    });

    expect(wrapper.container.innerHTML).toEqual('');
  });
});
