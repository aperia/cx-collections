import React from 'react';
import { storeId, renderMockStoreId, mockActionCreator } from 'app/test-utils';
import Footer from './Footer';
import { CALL_RESULT_CODE, COLLECTION_METHOD } from '../constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { screen } from '@testing-library/dom';

import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import userEvent from '@testing-library/user-event';
import { actionsLiveVox } from 'app/livevox-dialer/_redux/reducers';

const initialState: Partial<RootState> = {
  liveVoxReducer: {
    sessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
    isAgentAuthenticated: true,
    lineNumber: 'ACD',
    accountNumber: '2345',
    lastInCallAccountNumber: '2345',
    inCallAccountNumber: '2345',
    call: {
      accountNumber: '5166480500018901',
      accountNumberRequired: true,
      callCenterId: 75,
      callRecordingStarted: true,
      callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
      callTransactionId: '126650403531',
      phoneNumber: '1234567890',
      serviceId: 1094568
    }
  },
  collectionForm: {
    [storeId]: {
      viewName: ['view', 'name'],
      isOpenModal: true,
      selectedCallResult: {
        code: CALL_RESULT_CODE.BANKRUPTCY,
        description: 'des'
      },
      lastFollowUpData: {
        callResultType: CALL_RESULT_CODE.BANKRUPTCY
      },
      method: COLLECTION_METHOD.EDIT,
      dirtyForm: { BC: true }
    }
  },
  bankruptcy: {
    [storeId]: {
      isLoading: false
    }
  },
  deceased: {
    [storeId]: {
      loading: false
    }
  }
};

const confirmModalSpy = mockActionCreator(confirmModalActions);
const mockLiveVoxDialerActions = mockActionCreator(actionsLiveVox);

const renderWrapper = (state: Partial<RootState>) => {
  return renderMockStoreId(<Footer />, {
    initialState: state
  });
};

describe('Footer', () => {
  it('render component', () => {
    const { wrapper } = renderWrapper(initialState);
    expect(wrapper.getByText('txt_save')).toBeInTheDocument();
  });

  it('handleSubmitForm', () => {
    const mockLiveVoxDialerTermCodesThunk = mockLiveVoxDialerActions(
      'liveVoxDialerTermCodesThunk'
    );
    const { wrapper } = renderWrapper(initialState);
    const button = wrapper.getByText('txt_save')!;

    button.click();
    expect(wrapper.getByText('txt_save')).toBeInTheDocument();
    expect(mockLiveVoxDialerTermCodesThunk).toBeCalledWith(
      {
        liveVoxDialerSessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
        liveVoxDialerLineNumber: 'ACD',
        selectedCallResult: {
          code: CALL_RESULT_CODE.BANKRUPTCY,
          description: 'des'
        },
        serviceId: 1094568,
        callTransactionId: '126650403531',
        callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
        account: '5166480500018901',
        phoneDialed: '1234567890',
        moveAgentToNotReady: false
      }
    );
  });

  it('should not dispatch liveVoxDialerTermCodesThunk if accountNumber is empty', () => {
    const mockLiveVoxDialerTermCodesThunk = mockLiveVoxDialerActions(
      'liveVoxDialerTermCodesThunk'
    );

    const initialStateWithEmptyAccountNumber: Partial<RootState> = {
      liveVoxReducer: {
        sessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
        isAgentAuthenticated: true,
        lineNumber: 'ACD',
        accountNumber: '',
        lastInCallAccountNumber: '',
        inCallAccountNumber: '',
        call: {
          accountNumber: '',
          accountNumberRequired: true,
          callCenterId: 75,
          callRecordingStarted: true,
          callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
          callTransactionId: '126650403531',
          phoneNumber: '1234567890',
          serviceId: 1094568
        }
      },
      collectionForm: {
        [storeId]: {
          viewName: ['view', 'name'],
          isOpenModal: true,
          selectedCallResult: {
            code: CALL_RESULT_CODE.BANKRUPTCY,
            description: 'des'
          },
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.BANKRUPTCY
          },
          method: COLLECTION_METHOD.EDIT,
          dirtyForm: { BC: true }
        }
      },
      bankruptcy: {
        [storeId]: {
          isLoading: false
        }
      },
      deceased: {
        [storeId]: {
          loading: false
        }
      }
    };
    const { wrapper } = renderWrapper(initialStateWithEmptyAccountNumber);
    const button = wrapper.getByText('txt_save')!;

    button.click();
    expect(wrapper.getByText('txt_save')).toBeInTheDocument();
    expect(mockLiveVoxDialerTermCodesThunk).not.toBeCalled();
  });

  it('handleSubmitForm when loading', () => {
    const { wrapper } = renderWrapper({
      ...initialState,
      bankruptcy: { [storeId]: { isLoading: true } }
    });
    const button = wrapper.getByText('txt_save')!;
    button.click();
    expect(wrapper.getByText('txt_save')).toBeInTheDocument();
  });

  it('invokeCloseForm', () => {
    const { wrapper } = renderWrapper(initialState);
    const button = wrapper.getByText('txt_cancel')!;
    button.click();
    expect(wrapper.getByText('txt_save')).toBeInTheDocument();
  });

  describe('invokeCloseForm', () => {
    const generateState = ({
      callResultType = CALL_RESULT_CODE.DECEASE_PENDING,
      dirtyForm = {}
    }: {
      callResultType?: CALL_RESULT_CODE;
      dirtyForm?: Record<string, boolean>;
    }) => {
      return {
        collectionForm: {
          [storeId]: {
            viewName: ['view', 'name'],
            isOpenModal: true,
            selectedCallResult: { code: '', description: 'des' },
            lastFollowUpData: { callResultType },
            method: COLLECTION_METHOD.EDIT,
            dirtyForm,
            uploadFiles: {}
          }
        },
        bankruptcy: { [storeId]: { isLoading: true } },
        deceased: { [storeId]: { loading: true } }
      } as Partial<RootState>;
    };

    let openModalMock: jest.Mock;

    beforeEach(() => {
      openModalMock = confirmModalSpy('open');
    });

    it('when collection form is BANKRUPTCY', () => {
      renderWrapper(
        generateState({
          callResultType: CALL_RESULT_CODE.BANKRUPTCY,
          dirtyForm: { [CALL_RESULT_CODE.BANKRUPTCY]: true }
        })
      );

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));

      expect(openModalMock).toBeCalledWith(
        expect.objectContaining({
          title: 'txt_unsaved_changes',
          body: 'txt_discard_change_body',
          cancelText: 'txt_continue_editing',
          confirmText: 'txt_discard_change',
          variant: 'danger'
        })
      );
    });

    it('when collection form is BANKRUPTCY_PENDING', () => {
      renderWrapper(
        generateState({
          callResultType: CALL_RESULT_CODE.BANKRUPTCY_PENDING,
          dirtyForm: { [CALL_RESULT_CODE.BANKRUPTCY_PENDING]: true }
        })
      );

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));

      expect(openModalMock).toBeCalledWith(
        expect.objectContaining({
          title: 'txt_unsaved_changes',
          body: 'txt_discard_change_body',
          cancelText: 'txt_continue_editing',
          confirmText: 'txt_discard_change',
          variant: 'danger'
        })
      );
    });

    it('when collection form is CCCS', () => {
      renderWrapper(
        generateState({
          callResultType: CALL_RESULT_CODE.CCCS,
          dirtyForm: { [CALL_RESULT_CODE.CCCS]: true }
        })
      );

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));

      expect(openModalMock).toBeCalledWith(
        expect.objectContaining({
          title: 'txt_unsaved_changes',
          body: 'txt_discard_change_body',
          cancelText: 'txt_continue_editing',
          confirmText: 'txt_discard_change',
          variant: 'danger'
        })
      );
    });

    it('when collection form is DECEASE', () => {
      renderWrapper(
        generateState({
          callResultType: CALL_RESULT_CODE.DECEASE,
          dirtyForm: { [CALL_RESULT_CODE.DECEASE]: true }
        })
      );

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));

      expect(openModalMock).toBeCalledWith(
        expect.objectContaining({
          title: 'txt_unsaved_changes',
          body: 'txt_discard_change_body',
          cancelText: 'txt_continue_editing',
          confirmText: 'txt_discard_change',
          variant: 'danger'
        })
      );
    });

    it('when collection form is DECEASE_PENDING', () => {
      renderWrapper(
        generateState({
          callResultType: CALL_RESULT_CODE.DECEASE_PENDING,
          dirtyForm: { [CALL_RESULT_CODE.DECEASE_PENDING]: true }
        })
      );

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));

      expect(openModalMock).toBeCalledWith(
        expect.objectContaining({
          title: 'txt_unsaved_changes',
          body: 'txt_discard_change_body',
          cancelText: 'txt_continue_editing',
          confirmText: 'txt_discard_change',
          variant: 'danger'
        })
      );
    });

    it('when collection form is not existed', () => {
      renderWrapper(generateState({}));

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));

      expect(openModalMock).not.toBeCalled();
    });
  });

  it('render component with openForm false', () => {
    const { wrapper } = renderWrapper({
      collectionForm: {
        [storeId]: {
          viewName: ['view', 'name'],
          isOpenModal: false,
          selectedCallResult: {
            code: CALL_RESULT_CODE.BANKRUPTCY,
            description: 'des'
          },
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.BANKRUPTCY
          },
          method: 'method',
          dirtyForm: { BC: true },
          uploadFiles: {}
        }
      }
    });
    expect(wrapper.container.innerHTML).toEqual('');
  });

  it('render component with renderTextButton submit', () => {
    const { wrapper } = renderWrapper({
      collectionForm: {
        [storeId]: {
          viewName: ['view', 'name'],
          isOpenModal: true,
          selectedCallResult: {
            code: CALL_RESULT_CODE.BANKRUPTCY,
            description: 'des'
          },
          method: COLLECTION_METHOD.CREATE,
          dirtyForm: { value: true },
          uploadFiles: {}
        }
      }
    });
    expect(wrapper.getByText('txt_submit')).toBeInTheDocument();
  });

  it('render component with renderTextButton save', () => {
    const { wrapper } = renderWrapper({
      collectionForm: {
        [storeId]: {
          viewName: ['view', 'name'],
          isOpenModal: true,
          selectedCallResult: {
            code: CALL_RESULT_CODE.BANKRUPTCY,
            description: 'des'
          },
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.BANKRUPTCY
          },
          method: COLLECTION_METHOD.CREATE,
          dirtyForm: { value: true },
          uploadFiles: {}
        }
      }
    });
    expect(wrapper.getByText('txt_submit')).toBeInTheDocument();
  });
});
