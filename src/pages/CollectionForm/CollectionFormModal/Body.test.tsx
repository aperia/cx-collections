import React from 'react';
import Body from './Body';
import {
  renderMockStoreId,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';
import { AccountDetailProvider } from 'app/hooks';
import { screen } from '@testing-library/react';
import { mockActionCreator } from 'app/test-utils';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

const apiErrorNotificationActionSpy = mockActionCreator(
  apiErrorNotificationAction
);

describe('Test Body component in Collection Form Results', () => {
  HTMLElement.prototype.scrollTo = jest.fn();

  const renderWrapper = (initialState: Partial<RootState>) =>
    renderMockStoreId(<Body />, { initialState });

  it('Render component > show Deceased', () => {
    const { wrapper } = renderWrapper({
      collectionForm: {
        [storeId]: {
          lastFollowUpData: {
            callResultType: 'DECEASED'
          }
        } as CollectionForm
      }
    });

    expect(screen.findByText('DECEASED')).toBeTruthy();
    wrapper.unmount();
  });

  it('Render component > show Bankruptcy', () => {
    const { wrapper } = renderWrapper({
      collectionForm: {
        [storeId]: {
          lastFollowUpData: {
            callResultType: 'BANKRUPTCY'
          }
        } as CollectionForm
      }
    });

    expect(screen.findByText('BANKRUPTCY')).toBeTruthy();

    wrapper.unmount();
  });

  it('Render component > empty call result', () => {
    const { wrapper } = renderWrapper({
      collectionForm: {
        [storeId]: {
          lastFollowUpData: {}
        } as CollectionForm
      },
      bankruptcy: {
        [storeId]: {
          isLoading: true
        }
      }
    });

    expect(screen.queryByText('DECEASED')).not.toBeInTheDocument();

    wrapper.unmount();
  });

  it('Render component > show api error', () => {
    const spy = apiErrorNotificationActionSpy('openApiDetailErrorModal');

    const { wrapper } = renderWrapper({
      collectionForm: {
        [storeId]: {
          lastFollowUpData: {
            callResultType: 'BANKRUPTCY'
          }
        } as CollectionForm
      },
      apiErrorNotification: {
        [storeId]: {
          errorDetail: {
            inCollectionFormModal: {
              request: 'error',
              response: 'error'
            }
          }
        }
      }
    });

    expect(screen.findByText('DECEASED')).toBeTruthy();

    const viewErrorBtn = screen.getByText('txt_call_result');

    viewErrorBtn.click();

    wrapper.unmount();
    spy.mockReset();
    spy.mockRestore();
  });

  it('Render component > with error return', () => {
    const { wrapper } = renderMockStoreId(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <Body />
      </AccountDetailProvider>,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              viewName: ['view', 'name'],
              isOpenModal: false,
              method: 'method',
              dirtyForm: { BC: true },
              uploadFiles: {}
            } as CollectionForm
          },
          bankruptcy: {
            [storeId]: {
              isLoading: true
            }
          },
          deceased: {
            [storeId]: {
              loading: true
            }
          }
        }
      },
      false
    );

    expect(wrapper.getByText('txt_call_result')).toBeInTheDocument();
    wrapper.unmount();
  });
});
