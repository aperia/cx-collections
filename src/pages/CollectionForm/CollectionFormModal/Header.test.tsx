import React from 'react';
import { storeId, renderMockStoreId, mockActionCreator } from 'app/test-utils';
import Header from './Header';
import { CALL_RESULT_CODE, COLLECTION_METHOD } from '../constants';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import { collectionFormActions } from '../_redux/reducers';
import userEvent from '@testing-library/user-event';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';

const initialState: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      viewName: ['view', 'name'],
      isOpenModal: true,
      selectedCallResult: {
        code: CALL_RESULT_CODE.BANKRUPTCY,
        description: 'des'
      },
      lastFollowUpData: {
        callResultType: CALL_RESULT_CODE.BANKRUPTCY
      },
      method: COLLECTION_METHOD.EDIT,
      dirtyForm: { BC: true },
      uploadFiles: {}
    }
  },
  bankruptcy: {
    [storeId]: {
      isLoading: false
    }
  },
  deceased: {
    [storeId]: {
      loading: false
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<Header />, {
    initialState
  });
};

const collectionFormActionsSpy = mockActionCreator(collectionFormActions);
const confirmModalSpy = mockActionCreator(confirmModalActions);

describe('Footer', () => {
  it('render component', () => {
    const { wrapper } = renderWrapper(initialState);
    expect(wrapper.getByText('txt_edit_collection_form')).toBeInTheDocument();
  });

  it('render component with error', () => {
    const { wrapper } = renderWrapper({
      collectionForm: {
        [storeId]: {
          viewName: ['view', 'name'],
          isOpenModal: false,
          selectedCallResult: {
            code: CALL_RESULT_CODE.BANKRUPTCY,
            description: 'des'
          },
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.BANKRUPTCY
          },
          method: 'method',
          dirtyForm: { BC: true },
          uploadFiles: {}
        }
      }
    });
    expect(wrapper.container.innerHTML).toEqual('');
  });

  it('invokeCloseForm', () => {
    const { wrapper } = renderWrapper(initialState);
    const button = queryByClass(wrapper.container, /icon-close/)!;
    button.click();
    expect(wrapper.getByText('txt_edit_collection_form')).toBeInTheDocument();
  });

  describe('handleCloseForm', () => {
    const generateState = ({
      callResultType = CALL_RESULT_CODE.DECEASE_PENDING,
      dirtyForm = { [CALL_RESULT_CODE.DECEASE_PENDING]: false }
    }: {
      callResultType?: CALL_RESULT_CODE;
      dirtyForm?: Record<string, boolean>;
    }) => {
      return {
        collectionForm: {
          [storeId]: {
            viewName: ['view', 'name'],
            isOpenModal: true,
            selectedCallResult: { code: '', description: 'des' },
            lastFollowUpData: { callResultType },
            method: COLLECTION_METHOD.EDIT,
            dirtyForm,
            uploadFiles: {}
          }
        },
        bankruptcy: { [storeId]: { isLoading: false } },
        deceased: { [storeId]: { loading: false } }
      } as Partial<RootState>;
    };

    let openModalMock: jest.Mock;
    let toggleModalMock: jest.Mock;
    let changeDisabledOkMock: jest.Mock;

    beforeEach(() => {
      openModalMock = confirmModalSpy('open');
      toggleModalMock = collectionFormActionsSpy('toggleModal');
      changeDisabledOkMock = collectionFormActionsSpy('changeDisabledOk');
    });

    it('when collection form is not existed', () => {
      const { wrapper } = renderWrapper(generateState({}));

      userEvent.click(queryByClass(wrapper.container, /icon-close/)!);

      expect(toggleModalMock).toBeCalledWith({ storeId });
      expect(changeDisabledOkMock).toBeCalledWith({
        storeId,
        disabledOk: false
      });
      expect(openModalMock).not.toBeCalled();
    });

    it('when collection form is BANKRUPTCY', () => {
      const { wrapper } = renderWrapper(
        generateState({
          callResultType: CALL_RESULT_CODE.BANKRUPTCY,
          dirtyForm: { [CALL_RESULT_CODE.BANKRUPTCY]: true }
        })
      );

      userEvent.click(queryByClass(wrapper.container, /icon-close/)!);

      expect(openModalMock).toBeCalledWith(
        expect.objectContaining({
          title: 'txt_unsaved_changes',
          body: 'txt_discard_change_body',
          cancelText: 'txt_continue_editing',
          confirmText: 'txt_discard_change',
          variant: 'danger'
        })
      );
    });

    it('when collection form is BANKRUPTCY_PENDING', () => {
      const { wrapper } = renderWrapper(
        generateState({
          callResultType: CALL_RESULT_CODE.BANKRUPTCY_PENDING,
          dirtyForm: { [CALL_RESULT_CODE.BANKRUPTCY_PENDING]: true }
        })
      );

      userEvent.click(queryByClass(wrapper.container, /icon-close/)!);

      expect(openModalMock).toBeCalledWith(
        expect.objectContaining({
          title: 'txt_unsaved_changes',
          body: 'txt_discard_change_body',
          cancelText: 'txt_continue_editing',
          confirmText: 'txt_discard_change',
          variant: 'danger'
        })
      );
    });

    it('when collection form is CCCS', () => {
      const { wrapper } = renderWrapper(
        generateState({
          callResultType: CALL_RESULT_CODE.CCCS,
          dirtyForm: { [CALL_RESULT_CODE.CCCS]: true }
        })
      );

      userEvent.click(queryByClass(wrapper.container, /icon-close/)!);

      expect(openModalMock).toBeCalledWith(
        expect.objectContaining({
          title: 'txt_unsaved_changes',
          body: 'txt_discard_change_body',
          cancelText: 'txt_continue_editing',
          confirmText: 'txt_discard_change',
          variant: 'danger'
        })
      );
    });

    it('when collection form is DECEASE', () => {
      const { wrapper } = renderWrapper(
        generateState({
          callResultType: CALL_RESULT_CODE.DECEASE,
          dirtyForm: { [CALL_RESULT_CODE.DECEASE]: true }
        })
      );

      userEvent.click(queryByClass(wrapper.container, /icon-close/)!);

      expect(openModalMock).toBeCalledWith(
        expect.objectContaining({
          title: 'txt_unsaved_changes',
          body: 'txt_discard_change_body',
          cancelText: 'txt_continue_editing',
          confirmText: 'txt_discard_change',
          variant: 'danger'
        })
      );
    });

    it('when collection form is DECEASE_PENDING', () => {
      const { wrapper } = renderWrapper(
        generateState({
          callResultType: CALL_RESULT_CODE.DECEASE_PENDING,
          dirtyForm: { [CALL_RESULT_CODE.DECEASE_PENDING]: true }
        })
      );

      userEvent.click(queryByClass(wrapper.container, /icon-close/)!);

      expect(openModalMock).toBeCalledWith(
        expect.objectContaining({
          title: 'txt_unsaved_changes',
          body: 'txt_discard_change_body',
          cancelText: 'txt_continue_editing',
          confirmText: 'txt_discard_change',
          variant: 'danger'
        })
      );
    });
  });

  it('renderTitle with empty lastFollowUpData', () => {
    const { wrapper } = renderWrapper({
      collectionForm: {
        [storeId]: {
          viewName: ['view', 'name'],
          isOpenModal: true,
          selectedCallResult: {
            code: CALL_RESULT_CODE.BANKRUPTCY,
            description: 'des'
          },
          method: COLLECTION_METHOD.CREATE,
          dirtyForm: { value: true },
          uploadFiles: {}
        }
      }
    });
    expect(wrapper.getByText('txt_create_collection_form')).toBeInTheDocument();
  });

  it('renderTitle with lastFollowUpData', () => {
    const { wrapper } = renderWrapper({
      collectionForm: {
        [storeId]: {
          viewName: ['view', 'name'],
          isOpenModal: true,
          selectedCallResult: {
            code: CALL_RESULT_CODE.BANKRUPTCY,
            description: 'des'
          },
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.BANKRUPTCY
          },
          method: COLLECTION_METHOD.CREATE,
          dirtyForm: { value: true },
          uploadFiles: {}
        }
      }
    });
    expect(wrapper.getByText('txt_create_collection_form')).toBeInTheDocument();
  });
});
