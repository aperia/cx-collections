import React, { useMemo } from 'react';
import classNames from 'classnames';

// components
import TogglePin from 'pages/__commons/InfoBar/TogglePin';
import { Button, Icon, Tooltip } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail } from 'app/hooks/useAccountDetail';

// redux
import { selectPinned } from 'pages/__commons/InfoBar/_redux/selectors';
import { useStoreIdSelector } from 'app/hooks';
import {
  selectCollectionFormMethod,
  takeCurrentView
} from '../_redux/selectors';
import { batch, useDispatch } from 'react-redux';
import { actionsInfoBar, InfoBarSection } from 'pages/__commons/InfoBar/_redux';
import { collectionFormActions } from '../_redux/reducers';

// Constants
import { CALL_RESULT_CODE } from '../constants';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

export interface HeaderProps {}

const Header: React.FC<HeaderProps> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const method = useStoreIdSelector<string>(selectCollectionFormMethod);
  const isEdit = method === 'edit';
  const currentView = useStoreIdSelector<string>(takeCurrentView);
  const pinned = useStoreIdSelector(selectPinned);
  const isPinned = pinned === InfoBarSection.CollectionForm;

  const isSummaryMode = useMemo(() => {
    return (
      currentView !== CALL_RESULT_CODE.PROMISE_TO_PAY &&
      currentView !== CALL_RESULT_CODE.HARDSHIP &&
      currentView !== CALL_RESULT_CODE.LONG_TERM_MEDICAL &&
      currentView !== CALL_RESULT_CODE.INCARCERATION &&
      currentView !== CALL_RESULT_CODE.SETTLEMENT
    );
  }, [currentView]);

  const handleLastActions = () => {
    dispatch(collectionFormActions.toggleLast10Actions({ storeId }));
  };

  const handleTogglePin = () => {
    batch(() => {
      const thisPinned = isPinned ? null : InfoBarSection.CollectionForm;

      dispatch(
        actionsInfoBar.updatePinned({
          storeId,
          pinned: thisPinned
        })
      );

      if (thisPinned) {
        dispatch(actionsInfoBar.updateRecover({ storeId, recover: null }));
      }

      dispatch(actionsInfoBar.updateIsExpand({ storeId, isExpand: !isPinned }));
    });
  };

  const renderTitle = () => {
    let title = '';
    if (isEdit) {
      title = 'txt_edit_collection_form';
    } else if (isSummaryMode) {
      title = 'txt_collection_summary';
    } else {
      title = 'txt_collection_form';
    }
    return t(title);
  };

  return (
    <div
      className={classNames('p-24 d-flex justify-content-between', {
        'border-bottom': !isSummaryMode
      })}
    >
      <div className="d-flex">
        <h4
          className="mr-8"
          data-testid={genAmtId('collectionFormHeader', 'title', '')}
        >
          {renderTitle()}
        </h4>
        {isSummaryMode && (
          <TogglePin
            dataTestId={genAmtId('collectionFormHeader', 'pin-btn', '')}
            isPinned={isPinned}
            onToggle={handleTogglePin}
          />
        )}
      </div>
      {isSummaryMode &&
        checkPermission(PERMISSIONS.LAST_25_ACTIONS_VIEW, storeId) && (
          <Tooltip
            variant="primary"
            placement="top"
            element={<span>{t(I18N_COLLECTION_FORM.LAST_25_ACTIONS)}</span>}
            triggerClassName="mr-n4"
            dataTestId={genAmtId(
              'collectionFormHeader',
              'action-list-info',
              ''
            )}
          >
            <Button
              id="last_actions_button"
              dataTestId={genAmtId(
                'collectionFormHeader',
                'action-list-btn',
                ''
              )}
              variant="icon-secondary"
              onClick={handleLastActions}
            >
              <Icon name="action-list" />
            </Button>
          </Tooltip>
        )}
    </div>
  );
};

export default Header;
