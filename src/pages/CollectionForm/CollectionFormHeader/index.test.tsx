import React from 'react';
import { storeId, mockActionCreator, renderMockStoreId } from 'app/test-utils';
import CollectionFormHeader from '.';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import { collectionFormActions } from '../_redux/reducers';
import { actionsInfoBar, InfoBarSection } from 'pages/__commons/InfoBar/_redux';
import { TogglePinProps } from 'pages/__commons/InfoBar/TogglePin';
import { CALL_RESULT_CODE } from '../constants';

const generateState = ({
  pinned,
  method = 'add',
  currentView
}: {
  pinned?: InfoBarSection;
  method?: string;
  currentView?: string;
}) => {
  return {
    collectionForm: {
      [storeId]: {
        method,
        currentView,
        uploadFiles: {}
      }
    },
    infoBar: {
      [storeId]: {
        pinned
      }
    }
  } as Partial<RootState>;
};

const renderWrapper = (state: Partial<RootState>) => {
  return renderMockStoreId(<CollectionFormHeader />, {
    initialState: state
  });
};

jest.mock('app/entitlements/helpers', () => {
  const helpers = jest.requireActual('app/entitlements/helpers');

  return {
    ...helpers,
    checkPermission: () => true
  };
});

jest.mock(
  'pages/__commons/InfoBar/TogglePin',
  () => (props: TogglePinProps) => {
    return <div onClick={props.onToggle}>TogglePin</div>;
  }
);

const collectionFormActionsSpy = mockActionCreator(collectionFormActions);
const actionsInfoBarSpy = mockActionCreator(actionsInfoBar);

describe('SimpleBarWithApiError', () => {
  it('render component', () => {
    const { wrapper } = renderWrapper(generateState({ method: 'edit' }));
    expect(wrapper.getByText('txt_edit_collection_form')).toBeInTheDocument();
  });
  it('render component with view', () => {
    const { wrapper } = renderWrapper(
      generateState({ currentView: CALL_RESULT_CODE.PROMISE_TO_PAY })
    );
    expect(wrapper.getByText('txt_collection_form')).toBeInTheDocument();
  });

  it('handleLastActions', () => {
    const spy = collectionFormActionsSpy('toggleLast10Actions');
    const { wrapper } = renderWrapper(generateState({}));

    const button = queryByClass(wrapper.container, /icon-action-list/)!;
    button.click();

    expect(spy).toHaveBeenCalledWith({
      storeId
    });
  });

  it('handleTogglePin', () => {
    const updatePinnedMock = actionsInfoBarSpy('updatePinned');
    const updateRecoverMock = actionsInfoBarSpy('updateRecover');
    const updateIsExpandMock = actionsInfoBarSpy('updateIsExpand');

    const { wrapper } = renderWrapper(generateState({}));
    const button = wrapper.getByText('TogglePin');
    button.click();

    expect(updateRecoverMock).toBeCalledWith({ storeId, recover: null });
    expect(updateIsExpandMock).toBeCalledWith({ storeId, isExpand: true });

    expect(updatePinnedMock).toHaveBeenCalledWith({
      storeId,
      pinned: InfoBarSection.CollectionForm
    });
  });

  it('handleTogglePin', () => {
    const updatePinnedMock = actionsInfoBarSpy('updatePinned');
    const updateRecoverMock = actionsInfoBarSpy('updateRecover');
    const updateIsExpandMock = actionsInfoBarSpy('updateIsExpand');

    const { wrapper } = renderWrapper(
      generateState({ pinned: InfoBarSection.CollectionForm })
    );
    const button = wrapper.getByText('TogglePin');
    button.click();

    expect(updateRecoverMock).not.toBeCalled();
    expect(updateIsExpandMock).toBeCalledWith({ storeId, isExpand: false });

    expect(updatePinnedMock).toHaveBeenCalledWith({
      storeId,
      pinned: null
    });
  });
});
