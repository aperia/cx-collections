// services
import { collectionFormService } from './collectionFormServices';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import { TalkingPointRequestBody } from 'app/global-types/collectionForm';
import {
  ClientInfoGetByIdRequest,
  CommonCollectionFormPayload,
  GetCallResultRequest,
  Last25ActionRequest
} from './types';

describe('collectionFormService', () => {
  describe('getCallResultsRefData', () => {
    const params: GetCallResultRequest = {
      common: {
        agent: 'agent',
        clientNumber: 'clientNumber',
        system: 'system',
        prin: 'prin',
        app: 'app'
      },
      clientInfoId: 'clientInfoId'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      collectionFormService.getCallResultsRefData(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      collectionFormService.getCallResultsRefData(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.getCallResultsRefData,
        params
      );
    });
  });

  describe('getTalkingPoint', () => {
    const params: TalkingPointRequestBody = {
      common: {
        agent: '',
        clientNumber: '',
        system: '',
        prin: '',
        app: '',
        user: '',
        accountId: '',
        org: ''
      }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      collectionFormService.getTalkingPoint(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      collectionFormService.getTalkingPoint(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.getTalkingPoint,
        params
      );
    });
  });

  describe('getDetailCallResultType', () => {
    const params: CommonCollectionFormPayload = {
      common: { app: 'app' }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      collectionFormService.getDetailCallResultType(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      collectionFormService.getDetailCallResultType(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.getDetailCallResultType,
        params
      );
    });
  });

  describe('getLatestAction', () => {
    const params: MagicKeyValue = { common: { app: 'app' } };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      collectionFormService.getLatestAction(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      collectionFormService.getLatestAction(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.actionEntry,
        params
      );
    });
  });

  describe('getLast25Actions', () => {
    const params: Last25ActionRequest = {
      startSequence: 234,
      endSequence: 236
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      collectionFormService.getLast25Actions(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      collectionFormService.getLast25Actions(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.getLast25Actions,
        params
      );
    });
  });

  describe('getDescriptionCodeForLast25Actions', () => {
    const params: ClientInfoGetByIdRequest = {
      common: {
        agent: 'agent',
        clientNumber: 'clientNumber',
        system: 'system',
        prin: 'prin',
        app: 'app'
      },
      clientInfoId: 'clientInfoId'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      collectionFormService.getDescriptionCodeForLast25Actions(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      collectionFormService.getDescriptionCodeForLast25Actions(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.getDescriptionCodeForLast25Actions,
        params
      );
    });
  });

  describe('updateActionEntryWorkFlow', () => {
    const params = { common: { app: 'app' } };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      collectionFormService.updateActionEntryWorkFlow(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      collectionFormService.updateActionEntryWorkFlow(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.updateActionEntryWorkFlow,
        params
      );
    });
  });

  describe('uploadFiles', () => {
    const params = {
      fileName: 'fileName',
      file: 'file',
      documentClassification: 'doc'
    };
    const option = { url: '' };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      collectionFormService.uploadFiles(params, option);

      expect(mockService).toBeCalledWith('', params, option);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      collectionFormService.uploadFiles(params, option);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.uploadCaseDocument,
        params,
        option
      );
    });
  });

  describe('getListCaseDocuments', () => {
    const params = {
      documentClassification: 'doc'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      collectionFormService.getListCaseDocuments(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      collectionFormService.getListCaseDocuments(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.getCaseDocumentList,
        params
      );
    });
  });

  describe('downloadCaseDocument', () => {
    const params = {
      fileName: 'fileName',
      uuid: 'uuid'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      collectionFormService.downloadCaseDocument(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      collectionFormService.downloadCaseDocument(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.downloadCaseDocument,
        params
      );
    });
  });
});
