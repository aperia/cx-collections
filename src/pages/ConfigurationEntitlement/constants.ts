import { ENTITLEMENT_COMPONENTS } from 'app/entitlements/constants';
import { CRITERIA_NAME } from 'app/entitlements/constants';

export enum SECTION_TAB_EVENT {
  CHANGING_TAB = 'EntitlementConfig_ChangingTab',
  CHANGE_TAB = 'EntitlementConfig_ControlChangeTab',
  CLOSING_MODAL = 'EntitlementConfig_ClosingTab',
  CLOSE_MODAL = 'EntitlementConfig_CloseModal'
}

export const PRIORITY_COMPONENT: Record<ENTITLEMENT_COMPONENTS, number> = {
  [ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH]: 1,
  [ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION]: 2,
  [ENTITLEMENT_COMPONENTS.ACCOUNT_SNAPSHOT]: 3,
  [ENTITLEMENT_COMPONENTS.CARDHOLDER_MAINTENANCE]: 4,
  [ENTITLEMENT_COMPONENTS.CHANGE_HISTORY]: 5,
  [ENTITLEMENT_COMPONENTS.CLIENT_CONTACT_INFO]: 6,
  [ENTITLEMENT_COMPONENTS.CLIENT_CONFIGURATION]: 7,
  [ENTITLEMENT_COMPONENTS.COLLECTIONS_ACTIVITIES]: 8,
  [ENTITLEMENT_COMPONENTS.COLLECTION_INFORMATION]: 9,
  [ENTITLEMENT_COMPONENTS.CONTACT_ACCOUNT_ENTITLEMENT]: 10,
  [ENTITLEMENT_COMPONENTS.CUSTOMER_AUTHENTICATION]: 11,
  [ENTITLEMENT_COMPONENTS.DASHBOARD]: 12,
  [ENTITLEMENT_COMPONENTS.LAST_25_ACTIONS]: 13,
  [ENTITLEMENT_COMPONENTS.LETTER]: 14,
  [ENTITLEMENT_COMPONENTS.MAKE_PAYMENT]: 15,
  [ENTITLEMENT_COMPONENTS.MEMO]: 16,
  [ENTITLEMENT_COMPONENTS.MONETARY_ADJUSTMENT]: 17,
  [ENTITLEMENT_COMPONENTS.OVERVIEW_HISTORICAL_INFORMATION]: 18,
  [ENTITLEMENT_COMPONENTS.OVERVIEW_MONETARY_INFORMATION]: 19,
  [ENTITLEMENT_COMPONENTS.PAYMENT_HISTORY]: 20,
  [ENTITLEMENT_COMPONENTS.PAYMENT_LIST]: 21,
  [ENTITLEMENT_COMPONENTS.PENDING_AUTHORIZATION]: 22,
  [ENTITLEMENT_COMPONENTS.PUSH_QUEUE_LIST]: 23,
  [ENTITLEMENT_COMPONENTS.RELATED_ACCOUNT]: 24,
  [ENTITLEMENT_COMPONENTS.REPORTING]: 25,
  [ENTITLEMENT_COMPONENTS.STATEMENT]: 26,
  [ENTITLEMENT_COMPONENTS.STATEMENT_HISTORY]: 27,
  [ENTITLEMENT_COMPONENTS.TALKING_POINTS]: 28,
  [ENTITLEMENT_COMPONENTS.USER_ROLE_ENTITLEMENT]: 29,
  [ENTITLEMENT_COMPONENTS.WEBSITE_LAUNCHER]: 30
};

export const CSPA_FORM_MAX_LENGTH = 4;
export const CSPA_DES_MAX_LENGTH = 100;
export const CODE_ALL = '$ALL';

export const CSPA_FORM_FIELDS: Record<string, string> = {
  CLIENT_RADIO: 'clientRadio',
  CLIENT_ID: 'clientId',
  SYSTEM_ID: 'systemId',
  SYSTEM_RADIO: 'systemRadio',
  PRINCIPLE_ID: 'principleId',
  PRINCIPLE_RADIO: 'principleRadio',
  AGENT_ID: 'agentId',
  AGENT_RADIO: 'agentRadio',
  DESCRIPTION: 'description'
};

export enum SECURE_CONFIG_KY {
  CLIENT_CONFIG = 'clientConfig',
  CALLER_ACC_ENTITLEMENT = 'callerAccountEntitlements',
  CODE_TO_TEXT = 'codeToText'
}

export const UI_CRITERIA = {
  [CRITERIA_NAME.COMMERCIAL_CARD_CODE]: [
    { value: 'C', priority: 1, className: 'col-12' },
    { value: 'I', priority: 3, className: 'col-12' },
    { value: 'S', priority: 3, className: 'col-12' }
  ],

  [CRITERIA_NAME.CUSTOMER_ROLE_TYPE_CODE]: [
    {
      value: '01',
      priority: 1,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: 'NC',
      priority: 2,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: 'CD4',
      priority: 3,
      className: 'col-xl-6 col-lg-4 col-md-12'
    },
    {
      value: '02',
      priority: 4,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: 'CD1',
      priority: 5,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: 'CD5',
      priority: 6,
      className: 'col-xl-6 col-lg-4 col-md-12'
    },
    {
      value: '03',
      priority: 7,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: 'DMC',
      priority: 8,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: 'SP',
      priority: 9,
      className: 'col-xl-6 col-lg-4 col-md-12'
    },
    {
      value: 'CD2',
      priority: 10,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: 'CD3',
      priority: 11,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: 'AT',
      priority: 12,
      className: 'col-xl-3 col-lg-4 col-md-12'
    }
  ],

  [CRITERIA_NAME.EXTERNAL_STATUS_CODE]: [
    {
      value: 'A',
      priority: 1,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: 'F',
      priority: 2,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: 'Z',
      priority: 3,
      className: 'col-xl-6 col-lg-4 col-md-12'
    },
    {
      value: 'B',
      priority: 4,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: 'I',
      priority: 5,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: ' ',
      priority: 6,
      className: 'col-xl-6 col-lg-4 col-md-12'
    },
    {
      value: 'C',
      priority: 7,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: 'L',
      priority: 8,
      className: 'col-xl-9 col-lg-4 col-md-12'
    },
    {
      value: 'E',
      priority: 9,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: 'U',
      priority: 10,
      className: 'col-xl-9 col-lg-4 col-md-12'
    }
  ],

  [CRITERIA_NAME.INTERNAL_STATUS_CODE]: [
    {
      value: 'D',
      priority: 1,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: 'X',
      priority: 2,
      className: 'col-xl-9 col-lg-4 col-md-12'
    },
    {
      value: 'O',
      priority: 3,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: ' ',
      priority: 4,
      className: 'col-xl-9 col-lg-4 col-md-12'
    }
  ],

  [CRITERIA_NAME.PRESENTATION_INS_STATUS_CODE]: [
    {
      value: 'A',
      priority: 1,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: 'L',
      priority: 2,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: ' ',
      priority: 3,
      className: 'col-xl-6 col-lg-4 col-md-12'
    },
    {
      value: 'C',
      priority: 4,
      className: 'col-xl-3 col-lg-4 col-md-12'
    },
    {
      value: 'U',
      priority: 5,
      className: 'col-xl-9 col-lg-4 col-md-12'
    }
  ]
};

export const ACTIVE_DESCRIPTION =
  'The configuration flag makes the config active';
