import * as helpers from './helpers';

import { TreeViewValue } from 'app/_libraries/_dls/components';

describe('test ConfigurationEntitlements helpers', () => {
  it('test checkDefaultCSPA', () => {
    expect(helpers.checkDefaultCSPA('')).toEqual(false);
  });

  it('test isDefaultClientConfig', () => {
    expect(helpers.isDefaultClientConfig({ id: '' }, true)).toEqual(false);

    expect(
      helpers.isDefaultClientConfig(
        {
          additionalFields: [
            {
              name: 'isDefaultClientInfoKey'
            }
          ] as AdditionalFieldsMapType[]
        },
        false
      )
    ).toEqual(true);
  });

  it('test checkCollapseAll', () => {
    expect(helpers.checkCollapseAll([])).toEqual(false);

    expect(
      helpers.checkCollapseAll([{ expanded: true }] as TreeViewValue[])
    ).toEqual(true);
  });

  it('test checkSelectAll', () => {
    expect(helpers.checkSelectAll([])).toEqual(false);

    expect(
      helpers.checkSelectAll([{ checked: true }] as TreeViewValue[])
    ).toEqual(false);
  });

  it('test searchCSPA', () => {
    expect(helpers.searchCSPA([], '')).toEqual([]);

    expect(helpers.searchCSPA([{ systemId: 'a' }] as CSPA[], 'a')).toEqual([
      { systemId: 'a' }
    ]);
  });

  describe('test sortCSPA', () => {
    it('test sort.id === description', () => {
      expect(helpers.sortCSPA([], { id: '', order: 'asc' })).toEqual([]);

      // description undefined
      const result1 = helpers.sortCSPA([{}, {}], {
        id: 'description',
        order: 'asc'
      });
      expect(result1).toEqual([{}, {}]);

      // equal
      const result2 = helpers.sortCSPA(
        [{ description: 'a' }, { description: 'a' }],
        {
          id: 'description',
          order: 'asc'
        }
      );
      expect(result2).toEqual([{ description: 'a' }, { description: 'a' }]);

      // desc
      const result3 = helpers.sortCSPA(
        [{ description: 'a' }, { description: 'b' }],
        {
          id: 'description',
          order: 'desc'
        }
      );
      expect(result3).toEqual([{ description: 'b' }, { description: 'a' }]);

      // desc
      const result4 = helpers.sortCSPA(
        [{ description: 'b' }, { description: 'a' }],
        {
          id: 'description',
          order: 'desc'
        }
      );
      expect(result4).toEqual([{ description: 'b' }, { description: 'a' }]);

      // asc
      const result5 = helpers.sortCSPA(
        [{ description: 'a' }, { description: 'b' }],
        {
          id: 'description',
          order: 'asc'
        }
      );
      expect(result5).toEqual([{ description: 'a' }, { description: 'b' }]);

      // asc
      const result6 = helpers.sortCSPA(
        [{ description: 'b' }, { description: 'a' }],
        {
          id: 'description',
          order: 'asc'
        }
      );
      expect(result6).toEqual([{ description: 'a' }, { description: 'b' }]);

      // defaultCSPA
      const result7 = helpers.sortCSPA(
        [
          {
            description: 'a',
            additionalFields: [
              { name: 'isDefaultClientInfoKey' }
            ] as AdditionalFieldsMapType[]
          },
          { description: 'b' }
        ],
        {
          id: 'description',
          order: 'asc'
        }
      );
      expect(result7).toEqual([
        {
          description: 'a',
          additionalFields: [
            { name: 'isDefaultClientInfoKey' }
          ] as AdditionalFieldsMapType[]
        },
        { description: 'b' }
      ]);
    });

    it('test sort.id === status', () => {
      const cspaItem1 = {
        additionalFields: [
          {
            name: 'isConfigActive',
            active: true
          }
        ] as AdditionalFieldsMapType[]
      };
      const cspaItem2 = {
        additionalFields: [
          {
            name: 'isConfigActive',
            active: false
          }
        ] as AdditionalFieldsMapType[]
      };

      const result1 = helpers.sortCSPA([cspaItem1, cspaItem1], {
        id: 'status',
        order: 'asc'
      });
      expect(result1).toEqual([cspaItem1, cspaItem1]);

      const result2 = helpers.sortCSPA([cspaItem1, cspaItem2], {
        id: 'status',
        order: 'desc'
      });
      expect(result2).toEqual([cspaItem2, cspaItem1]);

      const result3 = helpers.sortCSPA([cspaItem2, cspaItem1], {
        id: 'status',
        order: 'desc'
      });
      expect(result3).toEqual([cspaItem2, cspaItem1]);

      const result4 = helpers.sortCSPA([cspaItem1, cspaItem2], {
        id: 'status',
        order: 'asc'
      });
      expect(result4).toEqual([cspaItem1, cspaItem2]);

      const result5 = helpers.sortCSPA([cspaItem2, cspaItem1], {
        id: 'status',
        order: 'asc'
      });
      expect(result5).toEqual([cspaItem1, cspaItem2]);
    });

    it('test sort.id not found', () => {
      const cspaItem1 = {
        additionalFields: [
          {
            name: 'isDefaultClientInfoKey',
            active: true
          }
        ] as AdditionalFieldsMapType[]
      };
      const cspaItem2 = {
        additionalFields: [
          {
            name: 'isDefaultClientInfoKey',
            active: false
          }
        ] as AdditionalFieldsMapType[]
      };
      const result1 = helpers.sortCSPA([cspaItem1, cspaItem2], {
        id: '',
        order: undefined
      });
      expect(result1).toEqual([cspaItem1]);

      const result2 = helpers.sortCSPA([{}, {}], {
        id: '',
        order: undefined
      });
      expect(result2).toEqual([{}, {}]);
    });
  });

  describe('test sortCSPAUserRole', () => {
    it('test sort.id === description', () => {
      expect(helpers.sortCSPAUserRole([], { id: '', order: 'asc' })).toEqual(
        []
      );

      // description undefined
      const result1 = helpers.sortCSPAUserRole([{}, {}], {
        id: 'description',
        order: 'asc'
      });
      expect(result1).toEqual([{}, {}]);

      // equal
      const result2 = helpers.sortCSPAUserRole(
        [{ description: 'a' }, { description: 'a' }],
        {
          id: 'description',
          order: 'asc'
        }
      );
      expect(result2).toEqual([{ description: 'a' }, { description: 'a' }]);

      // desc
      const result3 = helpers.sortCSPAUserRole(
        [{ description: 'a' }, { description: 'b' }],
        {
          id: 'description',
          order: 'desc'
        }
      );
      expect(result3).toEqual([{ description: 'b' }, { description: 'a' }]);

      // desc
      const result4 = helpers.sortCSPAUserRole(
        [{ description: 'b' }, { description: 'a' }],
        {
          id: 'description',
          order: 'desc'
        }
      );
      expect(result4).toEqual([{ description: 'b' }, { description: 'a' }]);

      // asc
      const result5 = helpers.sortCSPAUserRole(
        [{ description: 'a' }, { description: 'b' }],
        {
          id: 'description',
          order: 'asc'
        }
      );
      expect(result5).toEqual([{ description: 'a' }, { description: 'b' }]);

      // asc
      const result6 = helpers.sortCSPAUserRole(
        [{ description: 'b' }, { description: 'a' }],
        {
          id: 'description',
          order: 'asc'
        }
      );
      expect(result6).toEqual([{ description: 'a' }, { description: 'b' }]);

      // defaultCSPA
      const result7 = helpers.sortCSPAUserRole(
        [
          {
            id: '1234-$ALL-$ALL-$ALL',
            description: 'a',
            additionalFields: [
              {
                name: ''
              }
            ] as AdditionalFieldsMapType[]
          },
          { description: 'b' }
        ],
        {
          id: 'description',
          order: 'asc'
        }
      );
      expect(result7).toEqual([
        {
          id: '1234-$ALL-$ALL-$ALL',
          description: 'a',
          additionalFields: [
            {
              name: ''
            }
          ] as AdditionalFieldsMapType[]
        },
        { description: 'b' }
      ]);
    });

    it('test sort.id === status', () => {
      const cspaItem1 = {
        additionalFields: [
          {
            name: 'isConfigActive',
            active: true
          }
        ] as AdditionalFieldsMapType[],
        active: true
      };
      const cspaItem2 = {
        additionalFields: [
          {
            name: 'isConfigActive',
            active: false
          }
        ] as AdditionalFieldsMapType[],
        active: false
      };

      const result1 = helpers.sortCSPAUserRole([cspaItem1, cspaItem2], {
        id: 'status',
        order: 'asc'
      });
      expect(result1).toEqual([cspaItem1, cspaItem2]);

      const result2 = helpers.sortCSPAUserRole([cspaItem1, cspaItem2], {
        id: 'status',
        order: 'desc'
      });
      expect(result2).toEqual([cspaItem2, cspaItem1]);

      const result3 = helpers.sortCSPAUserRole([cspaItem2, cspaItem1], {
        id: 'status',
        order: 'asc'
      });
      expect(result3).toEqual([cspaItem1, cspaItem2]);

      const result4 = helpers.sortCSPAUserRole([cspaItem2, cspaItem1], {
        id: 'status',
        order: 'desc'
      });
      expect(result4).toEqual([cspaItem2, cspaItem1]);

      const result5 = helpers.sortCSPAUserRole([cspaItem1, cspaItem1], {
        id: 'status',
        order: 'asc'
      });
      expect(result5).toEqual([cspaItem1, cspaItem1]);
    });

    it('default CSPA', () => {
      // defaultCSPA
      const result = helpers.sortCSPAUserRole(
        [
          {
            id: '1234-$ALL-$ALL-$ALL',
            description: 'a',
            additionalFields: [
              {
                name: ''
              }
            ] as AdditionalFieldsMapType[]
          },
          { description: 'b' }
        ],
        {
          id: undefined,
          order: undefined
        }
      );
      expect(result).toEqual([
        {
          id: '1234-$ALL-$ALL-$ALL',
          description: 'a',
          additionalFields: [
            {
              name: ''
            }
          ]
        },
        { description: 'b' }
      ]);

      const result2 = helpers.sortCSPAUserRole(
        [
          {
            id: '12345-$ALL-$ALL-$ALL',
            description: 'a',
            additionalFields: [
              {
                name: ''
              }
            ] as AdditionalFieldsMapType[]
          },
          { description: 'b' }
        ],
        {
          id: undefined,
          order: undefined
        }
      );

      expect(result2).toEqual([
        {
          id: '12345-$ALL-$ALL-$ALL',
          description: 'a',
          additionalFields: [
            {
              name: ''
            }
          ]
        },
        { description: 'b' }
      ]);
    });
  });

  it('test dispatchClosingModalEvent', () => {
    expect(helpers.dispatchClosingModalEvent()).toBeUndefined();
  });

  it('test dispatchCloseModalEvent', () => {
    expect(helpers.dispatchCloseModalEvent()).toBeUndefined();
  });

  it('test dispatchControlTabChangeEvent', () => {
    expect(helpers.dispatchControlTabChangeEvent()).toBeUndefined();
  });

  it('test dispatchTabChangingTabEvent ', () => {
    expect(helpers.dispatchTabChangingTabEvent()).toBeUndefined();
  });

  it('test buildCSPA', () => {
    const mockArgs = {
      cspa: 'clientId000-systemId000-principleId000-agentId000',
      description: 'mock description'
    };
    const data = helpers.buildCSPA(mockArgs.cspa, mockArgs.description);
    expect(data).toEqual({
      id: 'clientId000-systemId000-principleId000-agentId000',
      cspaId: 'clientId000-systemId000-principleId000-agentId000',
      clientId: 'clientId000',
      systemId: 'systemId000',
      principleId: 'principleId000',
      agentId: 'agentId000',
      description: 'mock description'
    });

    const mockArgs2 = {
      cspa: undefined,
      description: 'mock description'
    };
    const data2 = helpers.buildCSPA(
      mockArgs2.cspa as never,
      mockArgs2.description
    );
    expect(data2).toEqual({
      id: 'undefined-undefined-undefined-undefined',
      cspaId: 'undefined-undefined-undefined-undefined',
      clientId: undefined,
      systemId: undefined,
      principleId: undefined,
      agentId: undefined,
      description: 'mock description'
    });
  });

  it('test buildListDefaultSort', () => {
    const cspaItem1 = {
      clientId: '',
      systemId: '',
      principleId: '',
      agentId: '',
      active: false,
      description: undefined
    };
    const cspaItem2 = {
      clientId: '2',
      active: false
    };
    const cspaItem3 = {
      clientId: '',
      systemId: '2',
      active: false
    };
    const cspaItem4 = {
      clientId: '',
      systemId: '',
      principleId: '2',
      active: false
    };
    const cspaItem5 = {
      clientId: '',
      systemId: '',
      principleId: '',
      agentId: '2',
      active: false
    };
    const cspaItem6 = {
      clientId: '',
      systemId: '',
      principleId: '',
      agentId: '',
      active: false,
      description: undefined
    };
    const cspaItem7 = {
      clientId: '',
      systemId: '',
      principleId: '',
      agentId: '',
      active: false,
      description: '1'
    };
    const cspaItem8 = {
      clientId: '',
      systemId: '',
      principleId: '',
      agentId: '',
      active: false,
      description: '2'
    };
    const cspaItem9 = {
      clientId: '',
      systemId: '',
      principleId: '',
      agentId: '',
      active: true,
      description: '2'
    };
    const cspaItem10 = {
      clientId: '',
      systemId: '3',
      active: false
    };

    const result = helpers.buildListDefaultSort([cspaItem1, cspaItem2]);
    const result2 = helpers.buildListDefaultSort([cspaItem1, cspaItem3]);
    const result3 = helpers.buildListDefaultSort([
      cspaItem1,
      cspaItem10,
      cspaItem3
    ]);
    const result4 = helpers.buildListDefaultSort([cspaItem1, cspaItem4]);
    const result5 = helpers.buildListDefaultSort([cspaItem1, cspaItem5]);
    const result6 = helpers.buildListDefaultSort([cspaItem1, cspaItem6]);
    const result7 = helpers.buildListDefaultSort([cspaItem1, cspaItem7]);
    const result8 = helpers.buildListDefaultSort([cspaItem7, cspaItem8]);
    const result9 = helpers.buildListDefaultSort([cspaItem8, cspaItem7]);
    const result10 = helpers.buildListDefaultSort([cspaItem1, cspaItem9]);
    const result11 = helpers.buildListDefaultSort([cspaItem9, cspaItem1]);
    const result12 = helpers.buildListDefaultSort([
      cspaItem1,
      cspaItem3,
      cspaItem3
    ]);
    const result13 = helpers.buildListDefaultSort([
      cspaItem1,
      cspaItem3,
      cspaItem9
    ]);
    expect(result).toEqual([cspaItem1, cspaItem2]);
    expect(result2).toEqual([cspaItem1, cspaItem3]);
    expect(result3).toEqual([cspaItem1, cspaItem3, cspaItem10]);
    expect(result4).toEqual([cspaItem1, cspaItem4]);
    expect(result5).toEqual([cspaItem1, cspaItem5]);
    expect(result6).toEqual([cspaItem1, cspaItem6]);
    expect(result7).toEqual([cspaItem1, cspaItem7]);
    expect(result8).toEqual([cspaItem7, cspaItem8]);
    expect(result9).toEqual([cspaItem7, cspaItem8]);
    expect(result10).toEqual([cspaItem9, cspaItem1]);
    expect(result11).toEqual([cspaItem9, cspaItem1]);
    expect(result12).toEqual([cspaItem1, cspaItem3, cspaItem3]);
    expect(result13).toEqual([cspaItem9, cspaItem1, cspaItem3]);
  });

  it('test lowerCaseUserRole > case empty data', () => {
    const mockArgs: MagicKeyValue[] = [];
    const result = helpers.lowerCaseUserRole(mockArgs);
    expect(result).toEqual([]);
  });

  it('test lowerCaseUserRole > case with data', () => {
    const mockArgs: MagicKeyValue[] = [
      {
        cspas: [
          {
            cspa: '123456',
            components: [
              {
                component: 'COMPONENTS_MAP_ACCOUNT_SEARCH',
                entitlements: { view: true }
              }
            ]
          }
        ]
      }
    ];
    const result = helpers.lowerCaseUserRole(mockArgs);
    expect(result).toEqual([
      {
        cspas: [
          {
            components: [
              {
                component: 'components_map_account_search',
                entitlements: { view: true }
              }
            ],
            cspa: '123456'
          }
        ]
      }
    ]);
  });

  it('test lowerCaseViewEntitlement > case empty data', () => {
    const mockArgs: MagicKeyValue[] | undefined = [];
    const result = helpers.lowerCaseViewEntitlement(mockArgs);
    expect(result).toEqual([]);
  });

  it('test lowerCaseViewEntitlement > case undefined data', () => {
    const mockArgs: MagicKeyValue[] | undefined = undefined;
    const result = helpers.lowerCaseViewEntitlement(mockArgs);
    expect(result).toEqual([]);
  });

  it('test lowerCaseViewEntitlement > case with data', () => {
    const mockArgs: MagicKeyValue[] | undefined = [
      {
        cspa: '123456',
        components: [
          {
            component: 'COMPONENTS_MAP_ACCOUNT_SEARCH',
            entitlements: { view: true }
          }
        ]
      }
    ];
    const result = helpers.lowerCaseViewEntitlement(mockArgs);
    expect(result).toEqual({
      cspas: [
        {
          components: [
            {
              component: 'components_map_account_search',
              entitlements: { view: true }
            }
          ],
          cspa: '123456'
        }
      ]
    });
  });
});
