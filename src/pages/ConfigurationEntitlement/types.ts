export interface IPayloadGetUserRole {
  CSPA: CSPA[];
}
export interface RequestGetUserRole {
  common: {
    agent: string;
    clientNumber: string;
    system: string;
    prin: string;
    user?: string;
    app: string;
    org: string;
  };
  fieldsToInclude?: string[];
}

export interface SectionItem {
  id: string;
  title: string;
  Component: React.LazyExoticComponent<React.FC<{ activeTab: string }>>;
}
