import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';

// Component
import {
  Button,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Const
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';

// Redux
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export interface ActiveDeActiveModalProps {
  isOpenModal: boolean;
  isLoading: boolean;
  selectedConfig: CSPA;
  isActive: boolean;
  onCloseModal: () => void;
  onSubmit: () => void;
}

const ActiveDeActiveModal: React.FC<ActiveDeActiveModalProps> = ({
  isOpenModal,
  isLoading,
  selectedConfig,
  isActive,
  onCloseModal,
  onSubmit
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const formattedClientConfigurationText = [
    selectedConfig?.clientId,
    selectedConfig?.systemId,
    selectedConfig?.principleId,
    selectedConfig?.agentId,
    selectedConfig?.description
  ].join(' - ');

  useEffect(() => {
    return () => {
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          forSection: 'inModalBody',
          storeId: API_ERROR_STORE_ID.ENTITLEMENT
        })
      );
    };
  }, [dispatch]);

  return (
    <Modal xs show={isOpenModal} animationDuration={500} loading={isLoading}>
      <ModalHeader border closeButton onHide={onCloseModal}>
        <ModalTitle>
          {isActive
            ? t('txt_confirm_deactivate_cspa')
            : t('txt_confirm_activate_cspa')}
        </ModalTitle>
      </ModalHeader>
      <ModalBodyWithApiError
        apiErrorClassName="mb-24"
        storeId={API_ERROR_STORE_ID.ENTITLEMENT}
        dataTestId="client-configuration-error"
      >
        <p className="mb-16">
          {isActive
            ? t('txt_confirm_deactivate_description')
            : t('txt_confirm_activate_description')}
        </p>
        <p>
          {t('txt_cspa')}:{' '}
          <span className="fw-500 text-break">
            {formattedClientConfigurationText}
          </span>
        </p>
      </ModalBodyWithApiError>
      <ModalFooter>
        <Button variant="secondary" onClick={onCloseModal}>
          {t('txt_cancel')}
        </Button>
        <Button variant={isActive ? 'danger' : 'primary'} onClick={onSubmit}>
          {isActive ? t('txt_deactivate') : t('txt_activate')}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ActiveDeActiveModal;
