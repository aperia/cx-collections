import React from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useDispatch } from 'react-redux';
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';

// constants
import {
  DropdownButton,
  DropdownButtonSelectEvent
} from 'app/_libraries/_dls/components';
import { ItemTab } from 'pages/__commons/TabBar/types';

const Entitlements = [
  {
    id: 'Contact-And-Account-Entitlement',
    label: 'txt_contact_account_entitlement'
  },
  {
    id: 'User-Role-Entitlement',
    label: 'txt_user_role_entitlement'
  }
];

const DropdownEntitlement: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const handleSelect = (event: DropdownButtonSelectEvent) => {
    let data = {
      id: 'Contact-And-Account-Entitlement',
      title: 'txt_contact_account_entitlement',
      storeId: 'Contact-And-Account-Entitlement',
      tabType: 'contactAndAccountEntitlement',
      iconName: 'file'
    } as ItemTab;

    if (event.value === 'User-Role-Entitlement') {
      data = {
        id: 'User-Role-Entitlement',
        title: 'txt_user_role_entitlement',
        storeId: 'User-Role-Entitlement',
        tabType: 'userRoleEntitlement',
        iconName: 'file'
      };
    }

    dispatch(actionsTab.addTab(data));
  };

  return (
    <DropdownButton
      id={'DropdownEntitlement'}
      popupBaseProps={{
        popupBaseClassName: 'popup-header'
      }}
      buttonProps={{
        children: t('txt_entitlement'),
        variant: 'outline-secondary'
      }}
      onSelect={handleSelect}
      className="mr-24"
      dataTestId="header_dropdown-entitlement"
    >
      {Entitlements.map(item => (
        <DropdownButton.Item
          key={item.id}
          label={t(item.label)}
          value={item.id}
        />
      ))}
    </DropdownButton>
  );
};

export default DropdownEntitlement;
