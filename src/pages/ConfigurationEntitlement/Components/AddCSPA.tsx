import React from 'react';
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { Button } from 'app/_libraries/_dls/components';
import classNames from 'classnames';

export interface AddCSPAProps {
  className?: string;
  disabled?: boolean;
  onAddCSPA: () => void;
}

const AddCSPA: React.FC<AddCSPAProps> = ({
  className,
  disabled,
  onAddCSPA
}) => {
  const { t } = useTranslation();

  return (
    <Button
      size="sm"
      className={classNames('mr-n8', className)}
      onClick={onAddCSPA}
      variant="outline-primary"
      disabled={disabled}
      dataTestId="configurationEntitlement-addCSPA"
    >
      {t('txt_add_cspa')}
    </Button>
  );
};

export default AddCSPA;
