import React from 'react';
import userEvent from '@testing-library/user-event';

// Components
import AddCSPAModal from './AddCSPAModal';

// Utils
import { renderMockStore } from 'app/test-utils';

// Const
import { CSPA_FORM_FIELDS } from '../constants';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { fireEvent } from '@testing-library/react';

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('Test AddCSPAModal', () => {
  it('should render', () => {
    const onCloseModal = jest.fn();

    const { wrapper } = renderMockStore(
      <AddCSPAModal
        isOpenModal
        errorMessage="mock error"
        defaultClientId="defaultClientId"
        onSubmit={jest.fn()}
        onCloseModal={onCloseModal}
      />
    );

    userEvent.click(wrapper.getByText(I18N_COMMON_TEXT.CANCEL));

    expect(onCloseModal).toHaveBeenCalled();
    expect(wrapper.getByText('txt_add_cspa')).toBeInTheDocument();
    expect(wrapper.getByText('mock error')).toBeInTheDocument();
  });

  describe('should render with modal close', () => {
    it('when defaultClientId is not defined', () => {
      const { wrapper } = renderMockStore(
        <AddCSPAModal
          errorMessage=""
          isOpenModal={false}
          onSubmit={jest.fn()}
          onCloseModal={jest.fn()}
        />
      );

      expect(wrapper.queryByText('txt_add_cspa')).not.toBeInTheDocument();
    });

    it('when errorModalOpened is not empty', () => {
      const { wrapper } = renderMockStore(
        <AddCSPAModal
          errorMessage=""
          isOpenModal={false}
          defaultClientId="defaultClientId"
          errorModalOpened={{ key: 'value' }}
          onSubmit={jest.fn()}
          onCloseModal={jest.fn()}
        />
      );

      expect(wrapper.queryByText('txt_add_cspa')).not.toBeInTheDocument();
    });
  });

  it('trigger errors for coverage', () => {
    const { wrapper } = renderMockStore(
      <AddCSPAModal
        isOpenModal
        errorMessage="mock error"
        defaultClientId="defaultClientId"
        onSubmit={jest.fn()}
        onCloseModal={jest.fn()}
      />
    );

    // Touch and raise error for `principleId`
    const principleRadioCustom = wrapper.baseElement.querySelectorAll(
      `input[name="${CSPA_FORM_FIELDS.PRINCIPLE_RADIO}"]`
    )[1];
    userEvent.click(principleRadioCustom);

    const textboxPrincipleId = wrapper.getByText(
      I18N_CLIENT_CONFIG.PRINCIPLE_ID
    ).parentElement!.previousElementSibling!;
    userEvent.type(textboxPrincipleId, '123');
    userEvent.clear(textboxPrincipleId);

    // Touch and raise error for `agentId`
    const agentRadioCustom = wrapper.baseElement.querySelectorAll(
      `input[name="${CSPA_FORM_FIELDS.AGENT_RADIO}"]`
    )[1];
    userEvent.click(agentRadioCustom);

    const textboxAgentId = wrapper.getByText(I18N_CLIENT_CONFIG.AGENT_ID)
      .parentElement!.previousElementSibling!;
    userEvent.type(textboxAgentId, '123');
    fireEvent.blur(textboxAgentId);

    expect(wrapper.getByText('txt_add_cspa')).toBeInTheDocument();
  });

  it('should trigger onRetry', () => {
    const onRetry = jest.fn();

    const { wrapper } = renderMockStore(
      <AddCSPAModal
        isRetry
        isOpenModal
        errorMessage="mock error"
        defaultClientId="defaultClientId"
        onRetry={onRetry}
        onSubmit={jest.fn()}
        onCloseModal={jest.fn()}
      />
    );

    userEvent.click(wrapper.getByText('txt_retry'));

    expect(onRetry).toHaveBeenCalled();
  });
});
