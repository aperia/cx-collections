import React from 'react';
import DropdownEntitlement from './DropdownEntitlement';
import {
  queryByClass,
  renderMockStore
} from 'app/test-utils/renderComponentWithMockStore';
import { screen } from '@testing-library/react';
import { mockActionCreator } from 'app/test-utils';
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';
import userEvent from '@testing-library/user-event';
import { act } from 'react-dom/test-utils';

const spyActions = mockActionCreator(actionsTab);

describe('Test ModifyClientConfig.test', () => {
  it('Should click txt_contact_account_entitlement', () => {
    const addTab = spyActions('addTab');
    const { container } = renderMockStore(
      <div>
        <DropdownEntitlement />
      </div>
    );
    act(() => {
      const queryText = queryByClass(container, /dls-dropdown-button/);
      userEvent.click(queryText!);
    });
    const queryText = screen.queryByText('txt_contact_account_entitlement');
    queryText?.click();
    expect(addTab).toHaveBeenCalledWith({
      id: 'Contact-And-Account-Entitlement',
      title: 'txt_contact_account_entitlement',
      storeId: 'Contact-And-Account-Entitlement',
      tabType: 'contactAndAccountEntitlement',
      iconName: 'file'
    });
  });

  it('Should click txt_user_role_entitlement', () => {
    const addTab = spyActions('addTab');
    const { container } = renderMockStore(
      <div>
        <DropdownEntitlement />
      </div>
    );
    act(() => {
      const queryText = queryByClass(container, /dls-dropdown-button/);
      userEvent.click(queryText!);
    });
    const queryText = screen.queryByText('txt_user_role_entitlement');
    queryText?.click();
    expect(addTab).toHaveBeenCalledWith({
      id: 'User-Role-Entitlement',
      title: 'txt_user_role_entitlement',
      storeId: 'User-Role-Entitlement',
      tabType: 'userRoleEntitlement',
      iconName: 'file'
    });
  });
});
