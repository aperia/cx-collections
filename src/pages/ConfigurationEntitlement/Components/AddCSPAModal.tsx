import React, { useEffect } from 'react';
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import {
  Button,
  Icon,
  InlineMessage,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  Popover,
  Radio,
  TextBox,
  TransDLS
} from 'app/_libraries/_dls/components';
import { isFunction, isUndefined, isEmpty } from 'lodash';
import { useDispatch } from 'react-redux';

// Const
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import {
  CSPA_DES_MAX_LENGTH,
  CSPA_FORM_MAX_LENGTH,
  CODE_ALL,
  CSPA_FORM_FIELDS
} from '../constants';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';

// hooks
import { useAddCSPA } from '../hooks/useAddCSPA';

// Component
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';

// Redux
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { genAmtId } from 'app/_libraries/_dls/utils';

const {
  CLIENT_ID,
  SYSTEM_ID,
  PRINCIPLE_ID,
  PRINCIPLE_RADIO,
  AGENT_ID,
  AGENT_RADIO,
  DESCRIPTION
} = CSPA_FORM_FIELDS;

export interface AddCSPAModalProps {
  onSubmit: (values: FormCSPAValue) => void;
  onRetry?: (values: FormCSPAValue) => void;
  onCloseModal: () => void;
  isOpenModal: boolean;
  isModalLoading?: boolean;
  errorMessage: string;
  defaultClientId?: string;
  isRetry?: boolean;
  errorModalOpened?: MagicKeyValue;
}

const AddCSPAModal: React.FC<AddCSPAModalProps> = ({
  onSubmit,
  errorMessage,
  isOpenModal,
  onCloseModal,
  onRetry,
  isRetry,
  isModalLoading = false,
  defaultClientId,
  errorModalOpened
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const testId = 'configurationEntitlement-addCSPAModal';

  const {
    handleChangeRadioOptionAll,
    handleChangeRadioOptionCustom,
    handleChangeTextBox,
    handleChangeDescription,
    values,
    handleReset,
    handleBlur,
    handleSubmit,
    validateForm,
    errors,
    touched
  } = useAddCSPA(onSubmit);

  const handleCloseModal = () => {
    handleReset({});
    onCloseModal();
  };

  // revalidate form after close modal
  useEffect(() => {
    if (!isOpenModal) {
      handleReset({});
      return;
    }

    validateForm();
  }, [handleReset, isOpenModal, validateForm]);

  const handleClickRetry = () => {
    return isFunction(onRetry) && onRetry(values);
  };

  useEffect(() => {
    return () => {
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          forSection: 'inModalBody',
          storeId: API_ERROR_STORE_ID.ENTITLEMENT
        })
      );
    };
  }, [dispatch]);

  // avoid crash app with COLX, FDES orgId data issue
  if (isUndefined(defaultClientId)) return null;

  return (
    <Modal
      sm
      loading={isModalLoading}
      show={isEmpty(errorModalOpened) && isOpenModal}
      dataTestId={testId}
    >
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId={genAmtId(testId, 'header', '')}
      >
        <ModalTitle dataTestId={genAmtId(testId, 'title', '')}>
          {t('txt_add_cspa')}
        </ModalTitle>
      </ModalHeader>
      <ModalBodyWithApiError
        apiErrorClassName="mb-24"
        storeId={API_ERROR_STORE_ID.ENTITLEMENT}
        dataTestId={genAmtId(testId, 'body', '')}
      >
        {errorMessage && (
          <InlineMessage
            className="mb-24 mt-n8"
            variant="danger"
            dataTestId={genAmtId(testId, 'errorMessage', '')}
          >
            <span>
              {t(errorMessage)}{' '}
              {isRetry && (
                <a
                  onClick={handleClickRetry}
                  className="link text-decoration-none ml-4"
                  data-testid={'client-config-retry-text'}
                >
                  {t('txt_retry')}
                </a>
              )}
            </span>
          </InlineMessage>
        )}
        <div className="mb-16 d-flex align-items-center">
          <span
            className="mr-10"
            data-testid={genAmtId(testId, 'provide-hierarchy-add-CSPA', '')}
          >
            {t('txt_provide_hierarchy_add_CSPA')}
          </span>
          <Popover
            triggerClassName="d-inline-block"
            placement="top"
            size="md"
            element={
              <TransDLS
                keyTranslation={I18N_CLIENT_CONFIG.PROVIDE_HIERARCHY_TOOLTIP}
              >
                <p
                  className="mb-8"
                  data-testid={genAmtId(testId, 'hierarchy-tooltip-title', '')}
                >
                  {t('txt_hierarchy_tooltip_title')}
                </p>
                <p
                  data-testid={genAmtId(
                    testId,
                    'hierarchy-tooltip-sub-title',
                    ''
                  )}
                >
                  {t('txt_hierarchy_tooltip_sub_title')}
                </p>
              </TransDLS>
            }
          >
            <Button
              variant="icon-secondary"
              size="sm"
              dataTestId={genAmtId(testId, 'information', '')}
            >
              <Icon name="information"></Icon>
            </Button>
          </Popover>
        </div>
        <form>
          <div className="d-flex align-items-center mb-16">
            <div className="d-flex w-227px">
              <p
                className="w-65px color-grey fw-500"
                data-testid={genAmtId(testId, 'client', '')}
              >
                {t(I18N_CLIENT_CONFIG.CLIENT)}:
              </p>
            </div>
            <div className="flex-1 ml-24">
              <TextBox
                name={CLIENT_ID}
                label={t(I18N_CLIENT_CONFIG.CLIENT_ID)}
                value={defaultClientId}
                readOnly={true}
                type="text"
                dataTestId={genAmtId(testId, 'client-id', '')}
              />
            </div>
          </div>
          <div className="mb-16 divider-dashed" />
          <div className="d-flex align-items-center mb-16">
            <div className="d-flex w-227px">
              <p
                className="w-65px color-grey fw-500"
                data-testid={genAmtId(testId, 'system', '')}
              >
                {t(I18N_CLIENT_CONFIG.SYSTEM)}:
              </p>
            </div>
            <div className="flex-1 ml-24">
              <TextBox
                errorTooltipProps={{
                  triggerClassName: 'w-100'
                }}
                readOnly={isRetry}
                name={SYSTEM_ID}
                label={t(I18N_CLIENT_CONFIG.SYSTEM_ID)}
                value={values.systemId}
                onChange={handleChangeTextBox}
                onBlur={handleBlur}
                required
                error={{
                  status: Boolean(errors.systemId && touched.systemId),
                  message: errors.systemId!
                }}
                type="text"
                maxLength={CSPA_FORM_MAX_LENGTH}
                dataTestId={genAmtId(testId, 'system-id', '')}
              />
            </div>
          </div>
          <div className="mb-16 divider-dashed" />
          <div className="d-flex align-items-center mb-16">
            <div className="d-flex">
              <p
                className="w-65px color-grey fw-500"
                data-testid={genAmtId(testId, 'principle', '')}
              >
                {t(I18N_CLIENT_CONFIG.PRINCIPLE)}:
              </p>
              <Radio
                className="ml-24"
                dataTestId={genAmtId(testId, 'principleRadio-all', '')}
              >
                <Radio.Input
                  name={PRINCIPLE_RADIO}
                  checked={values.principleRadio.all}
                  readOnly={isRetry}
                  onChange={handleChangeRadioOptionAll}
                />
                <Radio.Label>{t(I18N_CLIENT_CONFIG.ALL)}</Radio.Label>
              </Radio>
              <Radio
                className="ml-24"
                dataTestId={genAmtId(testId, 'principleRadio-custom', '')}
              >
                <Radio.Input
                  name={PRINCIPLE_RADIO}
                  checked={values.principleRadio.custom}
                  onChange={handleChangeRadioOptionCustom}
                  disabled={isRetry}
                />
                <Radio.Label>{t(I18N_CLIENT_CONFIG.CUSTOM)}</Radio.Label>
              </Radio>
            </div>

            <div className="flex-1 ml-24">
              <TextBox
                errorTooltipProps={{
                  triggerClassName: 'w-100'
                }}
                name={PRINCIPLE_ID}
                label={t(I18N_CLIENT_CONFIG.PRINCIPLE_ID)}
                value={
                  values.principleRadio.all ? CODE_ALL : values.principleId
                }
                onChange={handleChangeTextBox}
                onBlur={handleBlur}
                readOnly={isRetry ? isRetry : values.principleRadio.all}
                required={values.principleRadio.custom}
                error={{
                  status: Boolean(
                    values.principleRadio.custom &&
                      errors.principleId &&
                      touched.principleId
                  ),
                  message: errors.principleId!
                }}
                type="text"
                maxLength={CSPA_FORM_MAX_LENGTH}
                dataTestId={genAmtId(testId, 'principleId', '')}
              />
            </div>
          </div>
          <div className="mb-16 divider-dashed" />
          <div className="d-flex align-items-center mb-16">
            <div className="d-flex">
              <p
                className="w-65px color-grey fw-500"
                data-testid={genAmtId(testId, 'agent', '')}
              >
                {t(I18N_CLIENT_CONFIG.AGENT)}:
              </p>
              <Radio
                className="ml-24"
                dataTestId={genAmtId(testId, 'agentRadio-all', '')}
              >
                <Radio.Input
                  name={AGENT_RADIO}
                  checked={values.agentRadio.all}
                  onChange={handleChangeRadioOptionAll}
                  readOnly={isRetry}
                />
                <Radio.Label>{t(I18N_CLIENT_CONFIG.ALL)}</Radio.Label>
              </Radio>
              <Radio
                className="ml-24"
                dataTestId={genAmtId(testId, 'agentRadio-custom', '')}
              >
                <Radio.Input
                  name={AGENT_RADIO}
                  checked={values.agentRadio.custom}
                  onChange={handleChangeRadioOptionCustom}
                  disabled={isRetry ? isRetry : values.principleRadio.all}
                />
                <Radio.Label>{t(I18N_CLIENT_CONFIG.CUSTOM)}</Radio.Label>
              </Radio>
            </div>
            <div className="flex-1 ml-24">
              <TextBox
                errorTooltipProps={{
                  triggerClassName: 'w-100'
                }}
                name={AGENT_ID}
                label={t(I18N_CLIENT_CONFIG.AGENT_ID)}
                value={values.agentRadio.all ? CODE_ALL : values.agentId}
                onChange={handleChangeTextBox}
                onBlur={handleBlur}
                readOnly={isRetry ? isRetry : values.agentRadio.all}
                required={values.agentRadio.custom}
                error={{
                  status: Boolean(
                    values.agentRadio.custom &&
                      errors.agentId &&
                      touched.agentId
                  ),
                  message: errors.agentId!
                }}
                type="text"
                maxLength={CSPA_FORM_MAX_LENGTH}
                dataTestId={genAmtId(testId, 'agentId', '')}
              />
            </div>
          </div>
          <div className="mb-16 divider-dashed" />
          <TextBox
            name={DESCRIPTION}
            label={t(I18N_COMMON_TEXT.DESCRIPTION)}
            value={values.description}
            onChange={handleChangeDescription}
            onBlur={handleBlur}
            required
            readOnly={isRetry}
            error={{
              status: Boolean(errors.description && touched.description),
              message: errors.description!
            }}
            maxLength={CSPA_DES_MAX_LENGTH}
            dataTestId={genAmtId(testId, 'description', '')}
          />
        </form>
      </ModalBodyWithApiError>
      <ModalFooter
        cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
        okButtonText={t(I18N_COMMON_TEXT.SUBMIT)}
        disabledOk={isRetry || !isEmpty(errors)}
        onCancel={handleCloseModal}
        onOk={handleSubmit}
        dataTestId={genAmtId(testId, 'footer', '')}
      />
    </Modal>
  );
};

export default AddCSPAModal;
