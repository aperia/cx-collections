import React from 'react';
import Search from './Search';
import {
  queryByClass,
  renderMockStore
} from 'app/test-utils/renderComponentWithMockStore';
import { fireEvent, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';

describe('Test Search', () => {
  it('Should have not render ClearAndReset', () => {
    const onChangeTextSearch = jest.fn();
    const onPageChange = jest.fn();
    const onClearAndReset = jest.fn();
    renderMockStore(
      <Search
        onClearAndReset={onClearAndReset}
        onPageChange={onPageChange}
        onSearch={onChangeTextSearch}
        currentPage={1}
      />
    );

    const queryText = screen.queryByText('txt_clear_and_reset');
    expect(queryText).not.toBeInTheDocument();
  });

  it('Should have click ClearAndReset', () => {
    const onChangeTextSearch = jest.fn();
    const onPageChange = jest.fn();
    const onClearAndReset = jest.fn();
    renderMockStore(
      <Search
        onClearAndReset={onClearAndReset}
        onPageChange={onPageChange}
        onSearch={onChangeTextSearch}
        currentPage={1}
        isDisplayClearAndReset={true}
        textSearch="textSearch"
      />
    );

    const queryText = screen.queryByText('txt_clear_and_reset');
    queryText!.click();
    expect(onClearAndReset).toHaveBeenCalled();
  });

  it('Should have click ClearAndReset', () => {
    const currentPage = jest.fn() as any;
    const onChangeTextSearch = jest.fn();
    const onPageChange = jest.fn();
    const onClearAndReset = jest.fn();
    const { container } = renderMockStore(
      <Search
        onClearAndReset={onClearAndReset}
        onPageChange={onPageChange}
        onSearch={onChangeTextSearch}
        currentPage={currentPage}
        textSearch="textSearch"
      />
    );

    const queryText = queryByClass(container, /icon icon-close/);
    act(() => {
      queryText!.click();
    });
    const queryInput = screen.queryByPlaceholderText('txt_type_to_search');
    expect(queryInput).toHaveValue('');
  });

  it('Should have onChange TextSearch', () => {
    const onChangeTextSearch = jest.fn();
    const onPageChange = jest.fn();
    renderMockStore(
      <Search
        onClearAndReset={() => {}}
        onPageChange={onPageChange}
        onSearch={onChangeTextSearch}
        currentPage={1}
        textSearch="textSearch"
      />
    );
    const valueChange = 'valueChange';
    const queryInput = screen.queryByPlaceholderText('txt_type_to_search');
    fireEvent.change(queryInput!, { target: { value: valueChange } });
    expect(queryInput).toHaveValue(valueChange);
  });

  it('Should have handleSearch textSearch === textSearchProps', () => {
    const onChangeTextSearch = jest.fn();
    const onPageChange = jest.fn();
    const { container } = renderMockStore(
      <Search
        onClearAndReset={() => {}}
        onPageChange={onPageChange}
        onSearch={onChangeTextSearch}
        currentPage={1}
        textSearch="textSearch"
      />
    );
    const queryIconClose = queryByClass(container, /icon icon-search/);
    queryIconClose?.click();
    expect(onChangeTextSearch).not.toHaveBeenCalled();
    expect(onPageChange).not.toHaveBeenCalled();
  });

  it('Should have handleSearch textSearch !== textSearchProps', () => {
    const onChangeTextSearch = jest.fn();
    const onPageChange = jest.fn();
    const { container } = renderMockStore(
      <Search
        onClearAndReset={() => {}}
        onPageChange={onPageChange}
        onSearch={onChangeTextSearch}
        currentPage={1}
        textSearch="textSearch"
      />
    );
    const valueChange = 'valueChange';
    const queryInput = screen.queryByPlaceholderText('txt_type_to_search');
    fireEvent.change(queryInput!, { target: { value: valueChange } });
    const queryIconClose = queryByClass(container, /icon icon-search/);
    queryIconClose?.click();
    expect(onChangeTextSearch).toHaveBeenCalledWith(valueChange);
    expect(onPageChange).not.toHaveBeenCalled();
  });

  it('Should have currentPage > 1', () => {
    const onChangeTextSearch = jest.fn();
    const onPageChange = jest.fn();
    const { container } = renderMockStore(
      <Search
        onClearAndReset={() => {}}
        onPageChange={onPageChange}
        onSearch={onChangeTextSearch}
        currentPage={2}
        textSearch="textSearch"
      />
    );
    const valueChange = 'valueChange';
    const queryInput = screen.queryByPlaceholderText('txt_type_to_search');
    fireEvent.change(queryInput!, { target: { value: valueChange } });
    const queryIconClose = queryByClass(container, /icon icon-search/);
    queryIconClose?.click();
    expect(onPageChange).toHaveBeenCalledWith(1);
  });
});
