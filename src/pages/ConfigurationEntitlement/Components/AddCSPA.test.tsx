import React from 'react';
import userEvent from '@testing-library/user-event';

import { renderMockStore } from 'app/test-utils';

import AddCSPA from './AddCSPA';

it('Test AddCSPA', () => {
  const onAddCSPA = jest.fn();

  const { wrapper } = renderMockStore(<AddCSPA onAddCSPA={onAddCSPA} />);

  userEvent.click(wrapper.getByText('txt_add_cspa'));

  expect(onAddCSPA).toHaveBeenCalled();
});
