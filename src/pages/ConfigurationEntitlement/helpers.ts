import orderBy from 'lodash.orderby';
import { SortType, TreeViewValue } from 'app/_libraries/_dls/components';

// Helper
import { isArrayHasValue } from 'app/helpers';

// Const
import { SECTION_TAB_EVENT } from './constants';
import { SORT_TYPE } from 'app/constants';
import {
  CLIENT_CONFIG_STATUS_KEY,
  DEFAULT_CLIENT_CONFIG_KEY
} from 'pages/ClientConfiguration/constants';
import { isUndefined } from 'lodash';

export const checkDefaultCSPA = (id: string) => {
  const checkDefaultReg = new RegExp(
    atob('XlxkezR9LVwkQUxMLVwkQUxMLVwkQUxMJA=='),
    'g'
  );
  return checkDefaultReg.test(id);
};

export const isDefaultClientConfig = (
  clientConfig: CSPA,
  isUserRole?: boolean
) => {
  if (isUserRole) return checkDefaultCSPA(clientConfig.id || '');

  return clientConfig.additionalFields?.some(
    (item: AdditionalFieldsMapType) => item.name === DEFAULT_CLIENT_CONFIG_KEY
  );
};

export const checkCollapseAll = (entitlements: TreeViewValue[]): boolean => {
  if (!isArrayHasValue(entitlements)) return false;
  return entitlements.some(item => item.expanded === true);
};

export const checkSelectAll = (entitlements: TreeViewValue[]): boolean => {
  if (!isArrayHasValue(entitlements)) return false;
  return entitlements.some(item => !item.checked);
};

export const searchCSPA = (CSPA: CSPA[], textSearch: string): CSPA[] => {
  if (!textSearch) return CSPA;
  const textSearchLowerCase = textSearch.toLowerCase().trim();
  return CSPA.filter(
    item =>
      item.agentId?.toLocaleLowerCase().includes(textSearchLowerCase) ||
      item.clientId?.toLocaleLowerCase().includes(textSearchLowerCase) ||
      item.description?.toLocaleLowerCase().includes(textSearchLowerCase) ||
      item.principleId?.toLocaleLowerCase().includes(textSearchLowerCase) ||
      item.systemId?.toLocaleLowerCase().includes(textSearchLowerCase)
  );
};

export const sortCSPA = (CSPA: Array<CSPA>, sort: SortType): Array<CSPA> => {
  if (!isArrayHasValue(CSPA)) return CSPA;

  const defaultCSPA = CSPA.find(item => {
    return isDefaultClientConfig(item);
  });

  let filterCSPA = CSPA.filter(item => {
    return !isDefaultClientConfig(item);
  });

  if (sort.id === 'description' && !isUndefined(sort.order)) {
    filterCSPA = filterCSPA.sort((first, second) => {
      const { description: firstDes = '' } = first;
      const { description: secondDes = '' } = second;

      if (firstDes.toLowerCase() === secondDes.toLowerCase()) {
        return 0;
      }

      if (sort.order === SORT_TYPE.DESC) {
        return firstDes.toLowerCase() > secondDes.toLowerCase() ? -1 : 1;
      }

      return firstDes.toLowerCase() > secondDes.toLowerCase() ? 1 : -1;
    });

    if (defaultCSPA) {
      return [defaultCSPA, ...filterCSPA];
    }

    return filterCSPA;
  }

  if (sort.id === 'status' && !isUndefined(sort.order)) {
    filterCSPA = filterCSPA.sort((first, second) => {
      const valueA = first?.additionalFields?.find(
        (el: AdditionalFieldsMapType) => el.name === CLIENT_CONFIG_STATUS_KEY
      );

      const valueB = second?.additionalFields?.find(
        (el: AdditionalFieldsMapType) => el.name === CLIENT_CONFIG_STATUS_KEY
      );

      if (valueA?.active === valueB?.active) return 0;

      if (sort.order === SORT_TYPE.DESC) {
        return valueA?.active ? 1 : -1;
      }
      return valueA?.active ? -1 : 1;
    });
  }

  if (defaultCSPA) {
    const order = orderBy(
      filterCSPA,
      [sort.id || 'status'],
      [sort.order || 'asc']
    );
    return [defaultCSPA, ...order];
  } else {
    return orderBy(filterCSPA, [sort.id || 'status'], [sort.order || 'asc']);
  }
};

export const sortCSPAUserRole = (
  CSPA: Array<CSPA>,
  sort: SortType
): Array<CSPA> => {
  if (!isArrayHasValue(CSPA)) return CSPA;

  const defaultCSPA = CSPA.find(item => {
    return isDefaultClientConfig(item, true);
  });

  let filterCSPA = CSPA.filter(item => {
    return !isDefaultClientConfig(item, true);
  });

  if (sort.id === 'description' && !isUndefined(sort.order)) {
    filterCSPA = filterCSPA.sort((first, second) => {
      const { description: firstDes = '' } = first;
      const { description: secondDes = '' } = second;

      if (firstDes.toLowerCase() === secondDes.toLowerCase()) {
        return 0;
      }

      if (sort.order === SORT_TYPE.DESC) {
        return firstDes.toLowerCase() > secondDes.toLowerCase() ? -1 : 1;
      }

      return firstDes.toLowerCase() > secondDes.toLowerCase() ? 1 : -1;
    });

    if (defaultCSPA) {
      return [defaultCSPA, ...filterCSPA];
    }

    return filterCSPA;
  }

  if (sort.id === 'status' && !isUndefined(sort.order)) {
    filterCSPA = filterCSPA.sort((first, second) => {
      if (first?.active === second?.active) return 0;

      if (sort.order === SORT_TYPE.DESC) {
        return first?.active ? 1 : -1;
      }
      return first?.active ? -1 : 1;
    });
  }

  const order = orderBy(
    filterCSPA,
    [sort.order ? sort.id : 'status'],
    [sort.order || 'desc']
  );
  return defaultCSPA ? [defaultCSPA, ...order] : order;
};

export const dispatchClosingModalEvent = () => {
  const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
    bubbles: true,
    cancelable: true
  });

  window.dispatchEvent(event);
};

export const dispatchCloseModalEvent = () => {
  const event = new CustomEvent(SECTION_TAB_EVENT.CLOSE_MODAL, {
    bubbles: true,
    cancelable: true
  });

  window.dispatchEvent(event);
};

export const dispatchTabChangingTabEvent = () => {
  const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGING_TAB, {
    bubbles: true,
    cancelable: true
  });

  window.dispatchEvent(event);
};

export const dispatchControlTabChangeEvent = () => {
  const event = new CustomEvent(SECTION_TAB_EVENT.CHANGE_TAB, {
    bubbles: true,
    cancelable: true
  });

  window.dispatchEvent(event);
};

export const buildCSPA = (cspa: string, description: string): CSPA => {
  const [clientId, systemId, principleId, agentId] = cspa?.split('-') || [];
  const id = `${clientId}-${systemId}-${principleId}-${agentId}`;

  return {
    id,
    cspaId: id,
    clientId,
    systemId,
    principleId,
    agentId,
    description
  };
};

export const buildListDefaultSort = (listCSPA: Array<CSPA>) => {
  const sortAlphabet = (firstItem: CSPA, secondItem: CSPA, ky: keyof CSPA) => {
    const firstValue = firstItem[ky] as string;
    const secondValue = secondItem[ky] as string;

    if (firstValue.toLowerCase() === secondValue.toLowerCase()) return 0;
    return firstValue.toLowerCase() > secondValue.toLowerCase() ? 1 : -1;
  };

  if (
    listCSPA.every(item => item.active) ||
    listCSPA.every(item => !item.active)
  ) {
    const [firstCSPA] = listCSPA;
    const isSortByClient = listCSPA.some(
      item => item.clientId !== firstCSPA.clientId
    );
    const isSortBySys = listCSPA.some(
      item => item.systemId !== firstCSPA.systemId
    );
    const isSortByPrin = listCSPA.some(
      item => item.principleId !== firstCSPA.principleId
    );
    const isSortByAgent = listCSPA.some(
      item => item.agentId !== firstCSPA.agentId
    );

    if (isSortByClient) {
      return listCSPA.sort((prevItem, nextItem) =>
        sortAlphabet(prevItem, nextItem, 'clientId')
      );
    }

    if (isSortBySys) {
      return listCSPA.sort((prevItem, nextItem) =>
        sortAlphabet(prevItem, nextItem, 'systemId')
      );
    }

    if (isSortByPrin) {
      return listCSPA.sort((prevItem, nextItem) =>
        sortAlphabet(prevItem, nextItem, 'principleId')
      );
    }

    if (isSortByAgent) {
      return listCSPA.sort((prevItem, nextItem) =>
        sortAlphabet(prevItem, nextItem, 'agentId')
      );
    }

    return listCSPA.sort((first, second) => {
      const { description: firstDes = '' } = first;
      const { description: secondDes = '' } = second;

      if (firstDes.toLowerCase() === secondDes.toLowerCase()) return 0;
      return firstDes.toLowerCase() > secondDes.toLowerCase() ? 1 : -1;
    });
  }

  return listCSPA.sort((prevItem, nextItem) => {
    if (prevItem?.active === nextItem?.active) return 0;
    return prevItem.active ? -1 : 1;
  });
};

export const lowerCaseUserRole = (data: MagicKeyValue[]) => {
  if (!isArrayHasValue(data)) return [];
  const resData = data[0];
  const resCSPAS = data[0]?.cspas[0];
  const components = resCSPAS?.components.map((r: MagicKeyValue) => {
    return { ...r, component: r.component.toLocaleLowerCase() };
  });
  return [{ ...resData, cspas: [{ ...resCSPAS, components }] }];
};

export const lowerCaseViewEntitlement = (data: MagicKeyValue[] | undefined) => {
  if (!data || !isArrayHasValue(data)) return [];
  const resData = data[0];
  const resCSPAS = data[0]?.components;
  const components = resCSPAS?.map((r: MagicKeyValue) => {
    return { ...r, component: r.component.toLocaleLowerCase() };
  });
  return { cspas: [{ ...resData, components }] };
};
