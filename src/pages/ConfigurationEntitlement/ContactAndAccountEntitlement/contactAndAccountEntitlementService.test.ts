import services from './contactAndAccountEntitlementService';

// Types
import {
  UpdateContactAccEntitlementRequest,
  GetContactAccEntitlementRequest,
  GetContactAccListCSPARequest
} from './types';

// Utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi } from 'app/test-utils/mocks/mockProperty';
import { SECURE_CONFIG_KY } from '../constants';

describe('Test contactAndAccountEntitlementService', () => {
  beforeEach(() => {
    mockAppConfigApi.clear();
  });

  it('getContactAccListCSPA', () => {
    const data: GetContactAccListCSPARequest = {
      fetchSize: '100'
    };

    const mockService = mockApiServices.post();

    services.getContactAccListCSPA(data);

    expect(mockService).toHaveBeenCalledWith('', data);
  });

  it('getContactAccEntitlement', () => {
    const data: GetContactAccEntitlementRequest = {
      selectConfig: SECURE_CONFIG_KY.CODE_TO_TEXT,
      selectCspa: 'selectCspa'
    };

    const mockService = mockApiServices.post();

    services.getContactAccEntitlement(data);

    expect(mockService).toHaveBeenCalledWith('', data);
  });

  it('updateContactAccEntitlement', () => {
    const data: UpdateContactAccEntitlementRequest = {
      callerAccountEntitlements: {}
    };

    const mockService = mockApiServices.post();

    services.updateContactAccEntitlement(data);

    expect(mockService).toHaveBeenCalledWith('', data);
  });
});
