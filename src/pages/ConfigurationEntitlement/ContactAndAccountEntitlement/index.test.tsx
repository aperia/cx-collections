import React from 'react';
import userEvent from '@testing-library/user-event';
import { queryByText, waitFor } from '@testing-library/react';

// Components
import ContactAndAccountEntitlement from './';

// Utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { configurationContactAccountEntitlementActions } from './_redux/reducer';

// Constants
import { DEFAULT_CLIENT_CONFIG_KEY } from 'pages/ClientConfiguration/constants';

HTMLCanvasElement.prototype.getContext = jest.fn();

const spyActions = mockActionCreator(
  configurationContactAccountEntitlementActions
);

jest.mock('./Grid.tsx', () => {
  return () => <div>GridClientConfig</div>;
});

jest.mock('../Components/AddCSPAModal.tsx', () => {
  return () => <div>AddCSPAModal</div>;
});

const render = (initialState: Partial<RootState>) => {
  return renderMockStore(<ContactAndAccountEntitlement />, { initialState });
};

describe('test render/action ContactAndAccountEntitlement', () => {
  it('should trigger handleOnAddCSPA', async () => {
    // prevent changing isLoadingListCSPA
    spyActions('getContactAccListCSPA');
    const action = spyActions('onOpenModalAddCSPA');
    const initialState: Partial<RootState> = {
      configurationContactAccountEntitlement: {
        isNoResultsFound: true,
        isLoadingListCSPA: false,
        listCSPA: [
          {
            clientId: 'clientId',
            additionalFields: [
              {
                name: DEFAULT_CLIENT_CONFIG_KEY,
                value: 'true'
              }
            ]
          }
        ]
      }
    };
    const { container } = await waitFor(() => render(initialState));
    const addCSPABtn = queryByText(container, 'txt_add_cspa')!;
    userEvent.click(addCSPABtn);

    expect(action).toBeCalled();
  });

  it('should trigger onReload', async () => {
    const action = spyActions('getContactAccListCSPA');
    const initialState: Partial<RootState> = {
      configurationContactAccountEntitlement: {
        isErrorLoadingListCSPA: true
      }
    };
    const { container } = render(initialState);
    const reloadBtn = queryByText(container, 'txt_reload')!;
    userEvent.click(reloadBtn);

    expect(action).toBeCalled();
  });

  it('should render GridClientConfig ', async () => {
    const initialState: Partial<RootState> = {
      configurationContactAccountEntitlement: {
        listCSPA: [
          {
            additionalFields: [
              {
                name: DEFAULT_CLIENT_CONFIG_KEY,
                value: 'true'
              }
            ]
          }
        ]
      },
      accessConfig: {
        cspaList: [{ cspa: '9999-1000-2000-3000' }]
      }
    };
    const { wrapper } = render(initialState);

    expect(wrapper.getByText('GridClientConfig')).toBeInTheDocument();
  });
});
