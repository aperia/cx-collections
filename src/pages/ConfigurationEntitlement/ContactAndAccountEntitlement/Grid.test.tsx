import React from 'react';
import cloneDeep from 'lodash.clonedeep';
import userEvent from '@testing-library/user-event';

// Components
import Grid from './Grid';

// Utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { configurationContactAccountEntitlementActions } from './_redux/reducer';

// Constants
import { listCriteria } from 'app/entitlements/constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

describe('Test Grid', () => {
  const initialState: Partial<RootState> = {
    configurationContactAccountEntitlement: {
      entitlements: [],
      isLoadingModal: false,
      isLoadingListCSPA: false,
      listContactAccountCSPA: [],
      selectedCSPA: {} as CSPA,
      textSearch: 'description',
      isNoResultsFound: true,
      isErrorSettingEntitlement: false,
      isErrorLoadingListCSPA: false,
      isOpenViewElement: false,
      sortBy: { id: 'clientId', order: undefined },
      activeTabEntitlement: {
        entitlements: [
          {
            entitlementCode: 'code'
          },
          {
            entitlementCode: 'code 2'
          }
        ]
      },
      activeEntitlement: {
        description: 'des',
        value: 'code'
      },
      listActiveEntitlements: [],
      listCriteria,
      listCSPA: [
        {
          description: 'description'
        },
        {
          description: 'description'
        },
        {
          description: 'description'
        },
        {
          description: 'description'
        },
        {
          description: 'description'
        },
        {
          description: 'description'
        },
        {
          description: 'description'
        },
        {
          description: 'description'
        },
        {
          description: 'description'
        },
        {
          description: 'description'
        },
        {
          description: 'description'
        }
      ],
      isToggle: false,
      isSelectedAll: false,
      isOpenAddCSPA: false,
      addCSPAErrorMsg: '',
      openStatusModal: true
    }
  };

  const spyActions = mockActionCreator(
    configurationContactAccountEntitlementActions
  );

  it('should render', () => {
    const { wrapper } = renderMockStore(<Grid />, { initialState });

    expect(wrapper.getByText('txt_cspa_list')).toBeInTheDocument();
  });

  it('should call handleSort', () => {
    const onChangeSort = spyActions('onChangeSort');

    const { wrapper } = renderMockStore(<Grid />, { initialState });

    userEvent.click(wrapper.getByText('txt_description'));

    expect(onChangeSort).toHaveBeenCalled();
  });

  it('should call handleSort with valid sort', () => {
    const onChangeSort = spyActions('onChangeSort');

    const { wrapper } = renderMockStore(<Grid />, {
      initialState: {
        ...initialState,
        configurationContactAccountEntitlement: {
          ...initialState.configurationContactAccountEntitlement,
          sortBy: { id: 'description', order: 'desc' }
        }
      }
    });

    userEvent.click(wrapper.getByText('txt_description'));

    expect(onChangeSort).toHaveBeenCalled();
  });

  it('should call handleClearAndReset', () => {
    const onChangeSort = spyActions('onChangeSort');
    const onChangeTextSearch = spyActions('onChangeTextSearch');
    const _initialState = cloneDeep(initialState);
    _initialState.configurationContactAccountEntitlement!.textSearch =
      'not matched';

    const { wrapper } = renderMockStore(<Grid />, {
      initialState: _initialState
    });

    userEvent.click(wrapper.getByText('txt_clear_and_reset'));

    expect(onChangeSort).toHaveBeenCalledWith({
      id: 'status',
      order: undefined
    });
    expect(onChangeTextSearch).toHaveBeenCalledWith('');
    expect(wrapper.getByText('txt_memos_no_results_found')).toBeInTheDocument();
  });

  it('should call handleReload', () => {
    const getContactAccListCSPA = spyActions('getContactAccListCSPA');
    const _initialState = cloneDeep(initialState);
    _initialState.configurationContactAccountEntitlement!.isErrorLoadingListCSPA =
      true;

    const { wrapper } = renderMockStore(<Grid />, {
      initialState: _initialState
    });

    userEvent.click(wrapper.getByText(I18N_COMMON_TEXT.RELOAD));

    expect(getContactAccListCSPA).toHaveBeenCalled();
  });
});
