import React from 'react';

// Components
import StatusModal from './StatusModal';

// Utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { configurationContactAccountEntitlementActions } from './_redux/reducer';

// Constants
import { listCriteria } from 'app/entitlements/constants';
import { CLIENT_CONFIG_STATUS_KEY } from 'pages/ClientConfiguration/constants';
import userEvent from '@testing-library/user-event';
import cloneDeep from 'lodash.clonedeep';

describe('Test StatusModal', () => {
  const initialState: Partial<RootState> = {
    configurationContactAccountEntitlement: {
      entitlements: [],
      isLoadingModal: false,
      isLoadingListCSPA: false,
      listContactAccountCSPA: [],
      textSearch: 'text search',
      isNoResultsFound: true,
      isErrorSettingEntitlement: false,
      isErrorLoadingListCSPA: false,
      isOpenViewElement: false,
      sortBy: { id: 'clientId', order: undefined },
      activeTabEntitlement: {
        entitlements: [
          {
            entitlementCode: 'code'
          },
          {
            entitlementCode: 'code 2'
          }
        ]
      },
      activeEntitlement: {
        description: 'des',
        value: 'code'
      },
      listActiveEntitlements: [],
      listCriteria,
      listCSPA: [],
      isToggle: false,
      isSelectedAll: false,
      isOpenAddCSPA: false,
      addCSPAErrorMsg: '',
      selectedCSPA: {
        id: 'id',
        cspaId: 'cspaId',
        clientId: 'clientId',
        systemId: 'systemId',
        principleId: 'principleId',
        agentId: 'agentId',
        description: 'description',
        userId: 'userId',
        additionalFields: [
          {
            name: CLIENT_CONFIG_STATUS_KEY,
            active: true
          }
        ]
      },
      openStatusModal: true
    }
  };

  const spyActions = mockActionCreator(
    configurationContactAccountEntitlementActions
  );

  it('should dispatch triggerChangeStatusCSPA', () => {
    const triggerChangeStatusCSPA = spyActions('triggerChangeStatusCSPA');

    const { wrapper } = renderMockStore(<StatusModal />, { initialState });

    expect(
      wrapper.getByText('txt_confirm_deactivate_cspa')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_confirm_deactivate_description')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_cancel')).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_deactivate'));

    expect(triggerChangeStatusCSPA).toHaveBeenCalledWith({
      id: initialState.configurationContactAccountEntitlement!.selectedCSPA.id,
      additionalFields:
        initialState.configurationContactAccountEntitlement!.selectedCSPA
          .additionalFields,
      translateFn: expect.any(Function)
    });
  });

  it('should dispatch closeStatusModal', () => {
    const closeStatusModal = spyActions('closeStatusModal');
    const _initialState = cloneDeep(initialState);
    // coverage
    _initialState.configurationContactAccountEntitlement!.selectedCSPA.additionalFields =
      [];

    const { wrapper, baseElement } = renderMockStore(<StatusModal />, {
      initialState: _initialState
    });

    expect(wrapper.getByText('txt_confirm_activate_cspa')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_confirm_activate_description')
    ).toBeInTheDocument();

    userEvent.click(baseElement.querySelector('.icon-close')!);

    expect(closeStatusModal).toHaveBeenCalled();
  });
});
