import React, { useMemo, Fragment } from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';

// components
import {
  ColumnType,
  Grid,
  Pagination,
  SortType,
  TruncateText
} from 'app/_libraries/_dls/components';
import SearchConfig from './Search';
import ClearAndReset from 'pages/__commons/ClearAndReset';
import Action from './Action';
import StatusColumn from 'pages/ClientConfiguration/List/StatusColumn';
import { FailedApiReload } from 'app/components';

// hooks
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { usePagination } from 'app/hooks';

// i18n
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { DEFAULT_SORT_VALUE } from 'app/constants';

// redux
import { configurationContactAccountEntitlementActions } from './_redux/reducer';
import {
  selectCSPA,
  selectIsSearchNoData,
  selectSort,
  takeErrorLoadingListCSPA
} from './_redux/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';

const GridClientConfig: React.FC = () => {
  const { t } = useTranslation();

  const CSPA: CSPA[] = useSelector(selectCSPA);
  const isSearchNoData = useSelector(selectIsSearchNoData);
  const sortBy = useSelector(selectSort);
  const isError = useSelector(takeErrorLoadingListCSPA);

  const dispatch = useDispatch();
  const testId = 'contactAndAccountEntitlement_gridClientConfig';

  const CLIENT_CONFIG_LIST_COLUMNS = useMemo(
    () => [
      {
        id: 'clientId',
        Header: t(I18N_COMMON_TEXT.CLIENT_ID),
        isSort: true,
        width: 120,
        accessor: 'clientId'
      },
      {
        id: 'systemId',
        Header: t(I18N_CLIENT_CONFIG.SYSTEM_ID),
        isSort: true,
        width: 120,
        accessor: 'systemId'
      },
      {
        id: 'principleId',
        Header: t(I18N_CLIENT_CONFIG.PRINCIPLE_ID),
        isSort: true,
        width: 120,
        accessor: 'principleId'
      },
      {
        id: 'agentId',
        Header: t(I18N_CLIENT_CONFIG.AGENT_ID),
        isSort: true,
        width: 120,
        accessor: 'agentId'
      },
      {
        id: 'description',
        Header: t(I18N_COMMON_TEXT.DESCRIPTION),
        width: 390,
        isSort: true,
        autoWidth: true,
        accessor: (data: CSPA, index: number) => (
          <TruncateText
            lines={2}
            title={data?.description}
            resizable
            ellipsisMoreText={t(I18N_COMMON_TEXT.MORE)}
            ellipsisLessText={t(I18N_COMMON_TEXT.LESS)}
            className="text-break"
            dataTestId={genAmtId(testId, `${index}-description`, '')}
          >
            {data?.description}
          </TruncateText>
        )
      },
      {
        id: 'status',
        Header: t(I18N_COMMON_TEXT.STATUS),
        isSort: true,
        width: 114,
        accessor: (data: CSPA, index: number) => (
          <StatusColumn
            data={data}
            dataTestId={genAmtId(testId, `${index}-status`, '')}
          />
        )
      },
      {
        id: 'action',
        Header: t(I18N_COMMON_TEXT.ACTIONS),
        cellHeaderProps: { className: 'text-center' },
        cellProps: { className: 'text-right td-sm' },
        width: 245,
        isFixedRight: true,
        accessor: (data: CSPA, index: number) => (
          <Action
            data={data}
            dataTestId={genAmtId(testId, `${index}-action`, '')}
          />
        )
      }
    ],
    [t]
  );

  const handleClearAndReset = () => {
    batch(() => {
      dispatch(
        configurationContactAccountEntitlementActions.onChangeTextSearch('')
      );
      dispatch(
        configurationContactAccountEntitlementActions.onChangeSort({
          id: 'status',
          order: undefined
        })
      );
    });
    onPageSizeChange({ page: 1, size: 10 });
  };

  const handleSort = (sort: SortType) => {
    dispatch(
      configurationContactAccountEntitlementActions.onChangeSort(
        sort?.order ? sort : DEFAULT_SORT_VALUE
      )
    );
  };

  const handleReload = () => {
    dispatch(
      configurationContactAccountEntitlementActions.getContactAccListCSPA()
    );
  };

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(CSPA);

  const showPagination = total > pageSize[0];

  if (isError) {
    return (
      <Fragment>
        <FailedApiReload
          id="contact-account-entitlement-failed"
          className="mt-80"
          onReload={handleReload}
          dataTestId={genAmtId(testId, 'failed-api', '')}
        />
      </Fragment>
    );
  }

  return (
    <Fragment>
      <SearchConfig
        onClearAndReset={handleClearAndReset}
        onPageChange={onPageChange}
        currentPage={currentPage}
      />
      {isSearchNoData ? (
        <ClearAndReset
          className="mt-24 mb-16"
          onClearAndReset={handleClearAndReset}
        />
      ) : (
        <Fragment>
          <Grid
            sortBy={[sortBy]}
            onSortChange={handleSort}
            dataItemKey={'id'}
            columns={CLIENT_CONFIG_LIST_COLUMNS as unknown as ColumnType[]}
            data={gridData}
            dataTestId={genAmtId(testId, 'grid', '')}
          />
          {showPagination && (
            <div className="pt-16">
              <Pagination
                totalItem={total}
                pageSize={pageSize}
                pageNumber={currentPage}
                pageSizeValue={currentPageSize}
                compact
                onChangePage={onPageChange}
                onChangePageSize={onPageSizeChange}
                dataTestId={genAmtId(testId, 'pagination', '')}
              />
            </div>
          )}
        </Fragment>
      )}
    </Fragment>
  );
};

export default GridClientConfig;
