import isEqual from 'lodash.isequal';

// Action
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';

const unSavedChange: AppThunk =
  (confirmCallback: () => void) => async (dispatch, getState) => {
    const { configurationContactAccountEntitlement } = getState();
    const { activeTabEntitlement, entitlements } =
      configurationContactAccountEntitlement;

    const currentEntitlement = entitlements.find(
      item => item.component === activeTabEntitlement.component
    );

    if (!isEqual(activeTabEntitlement, currentEntitlement)) {
      dispatch(
        confirmModalActions.open({
          title: 'txt_unsaved_changes',
          body: 'txt_discard_change_body',
          cancelText: 'txt_continue_editing',
          confirmText: 'txt_discard_change',
          confirmCallback: confirmCallback,
          variant: 'danger'
        })
      );
    } else {
      confirmCallback();
    }
  };

export default unSavedChange;
