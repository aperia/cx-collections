import isEqual from 'lodash.isequal';
import { batch } from 'react-redux';

// Const
import { MultipleLangDictionary } from 'app/entitlements/dictionary';
import { ENTITLEMENT_COMPONENTS } from 'app/entitlements/constants';

// Actions
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// Redux
import { configurationContactAccountEntitlementActions } from './reducer';

export const resetToPrevious: AppThunk =
  (translateFunc: Function) => async (dispatch, getState) => {
    const { configurationContactAccountEntitlement } = getState();
    const { activeTabEntitlement, entitlements } =
      configurationContactAccountEntitlement;

    const currentEntitlement = entitlements.find(
      item => item.component === activeTabEntitlement.component
    );

    if (!isEqual(activeTabEntitlement, currentEntitlement)) {
      batch(() => {
        dispatch(
          configurationContactAccountEntitlementActions.onChangePrevious(
            currentEntitlement || {}
          )
        );
      });

      dispatch(
        actionsToast.addToast({
          type: 'success',
          show: true,
          message: 'txt_section_reset_to_previous',
          msgVariables: {
            section: translateFunc(
              MultipleLangDictionary.get(
                activeTabEntitlement.component as ENTITLEMENT_COMPONENTS
              )
            )
          }
        })
      );
    }
  };
