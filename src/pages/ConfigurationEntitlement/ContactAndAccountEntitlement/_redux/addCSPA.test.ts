import { mockActionCreator, responseDefault } from 'app/test-utils';
import { clientConfigurationServices } from 'pages/ClientConfiguration/clientConfigurationServices';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import { ContactAccStatusEntitlementState } from '../types';
import { triggerAddCSPA } from './addCSPA';
import { configurationContactAccountEntitlementActions } from './reducer';

const initialState: Partial<RootState> = {
  configurationContactAccountEntitlement: {
    isRetry: true
  } as ContactAccStatusEntitlementState
};

let store = createStore(rootReducer, initialState, applyMiddleware(thunk));

window.appConfig = {
  commonConfig: {
    app: 'app',
    org: 'COLX'
  }
} as AppConfiguration;

const mockToast = mockActionCreator(actionsToast);
const mockConfigurationContactAccountEntitlementActions = mockActionCreator(
  configurationContactAccountEntitlementActions
);

describe('triggerAddCSPA', () => {
  let spySaveCollectionsClientConfig: jest.SpyInstance;

  beforeEach(() => {
    spySaveCollectionsClientConfig?.mockRestore();
  });

  it('should run correctly and has errors data', async () => {
    const showErrorAction = mockConfigurationContactAccountEntitlementActions(
      'setAddClientConfigErrorMessage'
    );

    const args = {
      defaultClientConfig: {},
      clientId: 'clientId',
      systemId: 'stsystemIdring',
      principleId: 'principleId',
      agentId: 'agentId',
      description: 'description'
    };

    jest
      .spyOn(clientConfigurationServices, 'add')
      .mockResolvedValue({ ...responseDefault, data: { errors: 'Error' } });
    spySaveCollectionsClientConfig = jest
      .spyOn(clientConfigurationServices, 'saveCollectionsClientConfig')
      .mockResolvedValue({ ...responseDefault });

    await triggerAddCSPA(args)(store.dispatch, store.getState, {});
    expect(showErrorAction).toBeCalledWith({
      error: 'txt_contact_entitlement_already_exists',
      isRetry: false
    });
  });

  it('should run correctly and has errors data > case 2', async () => {
    const showErrorAction = mockConfigurationContactAccountEntitlementActions(
      'setAddClientConfigErrorMessage'
    );

    const args = {
      defaultClientConfig: {},
      clientId: 'clientId',
      systemId: 'stsystemIdring',
      principleId: 'principleId',
      agentId: 'agentId',
      description: 'description'
    };

    jest
      .spyOn(clientConfigurationServices, 'add')
      .mockRejectedValue({ ...responseDefault });
    spySaveCollectionsClientConfig = jest
      .spyOn(clientConfigurationServices, 'saveCollectionsClientConfig')
      .mockResolvedValue({ ...responseDefault });

    await triggerAddCSPA(args)(store.dispatch, store.getState, {});
    expect(showErrorAction).toBeCalledWith({
      error: '',
      isRetry: false
    });
  });

  it('should run correctly', async () => {
    const successToast = mockToast('addToast');
    store = createStore(
      rootReducer,
      {
        configurationContactAccountEntitlement: {
          isRetry: true
        } as ContactAccStatusEntitlementState
      },
      applyMiddleware(thunk)
    );
    const args = {
      defaultClientConfig: {},
      clientId: 'clientId',
      systemId: 'stsystemIdring',
      principleId: 'principleId',
      agentId: 'agentId',
      description: 'description'
    };

    jest
      .spyOn(clientConfigurationServices, 'add')
      .mockResolvedValue({ ...responseDefault, data: { errors: null } });
    spySaveCollectionsClientConfig = jest
      .spyOn(clientConfigurationServices, 'saveCollectionsClientConfig')
      .mockResolvedValue({ ...responseDefault });

    await triggerAddCSPA(args)(store.dispatch, store.getState, {});

    expect(successToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: 'txt_save_contact_entitlement_config_message'
    });
  });

  it('should run correctly with retry request failed', async () => {
    const successToast = mockToast('addToast');
    const args = {
      defaultClientConfig: {},
      clientId: 'clientId',
      systemId: 'stsystemIdring',
      principleId: 'principleId',
      agentId: 'agentId',
      description: 'description'
    };

    jest
      .spyOn(clientConfigurationServices, 'add')
      .mockResolvedValue({ ...responseDefault, data: { errors: null } });
    spySaveCollectionsClientConfig = jest
      .spyOn(clientConfigurationServices, 'saveCollectionsClientConfig')
      .mockRejectedValue({ ...responseDefault });

    await triggerAddCSPA(args)(store.dispatch, store.getState, {});
    expect(successToast).toHaveBeenNthCalledWith(2, {
      show: true,
      type: 'error',
      message: 'txt_contact_entitlement_add_secure_case_management_failed'
    });
  });

  it('should reject', async () => {
    window.appConfig = {} as AppConfiguration;

    const args = {
      defaultClientConfig: undefined,
      clientId: 'clientId',
      systemId: 'stsystemIdring',
      principleId: 'principleId',
      agentId: 'agentId',
      description: 'description'
    };

    const errorToast = mockToast('addToast');

    jest
      .spyOn(clientConfigurationServices, 'add')
      .mockRejectedValue({ ...responseDefault });
    spySaveCollectionsClientConfig = jest
      .spyOn(clientConfigurationServices, 'saveCollectionsClientConfig')
      .mockRejectedValue({ ...responseDefault });

    await triggerAddCSPA(args)(store.dispatch, store.getState, {});
    expect(errorToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: 'txt_contact_entitlement_add_secure_case_management_failed'
    });
  });

  it('should reject on triggerSaveClientConfiguration', async () => {
    const args = {
      defaultClientConfig: undefined,
      clientId: 'clientId',
      systemId: 'systemId',
      principleId: 'principleId',
      agentId: 'agentId',
      description: 'description'
    };

    const errorToast = mockToast('addToast');

    jest
      .spyOn(clientConfigurationServices, 'add')
      .mockResolvedValue({ ...responseDefault, data: { errors: null } });
    spySaveCollectionsClientConfig = jest
      .spyOn(clientConfigurationServices, 'saveCollectionsClientConfig')
      .mockRejectedValue({ ...responseDefault });

    await triggerAddCSPA(args)(store.dispatch, store.getState, {});
    expect(errorToast).toHaveBeenNthCalledWith(1, {
      show: true,
      type: 'error',
      message: 'txt_contact_entitlement_add_secure_case_management_failed'
    });
  });

  it('changeStatusCSPABuilder > reject', () => {
    const store = createStore(rootReducer, {} as never);
    const state = store.getState();
    const payload = triggerAddCSPA.rejected(
      { name: 'ErrorName', message: 'error message' },
      triggerAddCSPA.rejected.type,
      {
        clientId: '',
        agentId: '',
        systemId: '',
        principleId: '',
        description: '',
        defaultClientConfig: undefined
      }
    );
    const actual = rootReducer(state, payload);
    expect(actual.configurationUserRoleEntitlement.isLoadingModal).toBeFalsy();
  });
});
