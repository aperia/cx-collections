import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isRejected,
  isFulfilled
} from '@reduxjs/toolkit';

// const
import {
  BINSEQUENCES,
  CLIENT_CONFIG_STATUS_KEY,
  NAME_ACTION
} from 'pages/ClientConfiguration/constants';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { CODE_ALL } from 'pages/ConfigurationEntitlement/constants';

// Type
import { ContactAccStatusEntitlementState } from '../types';
import {
  ConfigData,
  CreateClientConfigurationRequest,
  TriggerSaveClientConfigRequest
} from 'pages/ClientConfiguration/types';

// Helper
import {
  convertClientInfoId,
  rejectApi,
  successApi
} from 'pages/ClientConfiguration/helpers';

// Redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { configurationContactAccountEntitlementActions } from './reducer';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { addClientConfiguration } from 'pages/ClientConfiguration/_redux/addClientConfiguration';
import { saveChangedClientConfig } from 'pages/ClientConfiguration/ChangeHistory/_redux/saveChangedClientConfig';
import { batch } from 'react-redux';

export const triggerAddCSPA = createAsyncThunk<
  void,
  {
    defaultClientConfig: ConfigData | undefined;
    clientId: string;
    systemId: string;
    principleId: string;
    agentId: string;
    description: string;
  },
  ThunkAPIConfig
>(
  'CSPA/triggerAddCSPA',
  async (args, { dispatch, rejectWithValue, getState }) => {
    const {
      clientId,
      agentId,
      systemId,
      principleId,
      description,
      defaultClientConfig
    } = args;
    const {
      clientId: clientDefault = '',
      systemId: systemDefault = CODE_ALL,
      principleId: prinDefault = CODE_ALL,
      agentId: agentDefault = CODE_ALL
    } = defaultClientConfig ?? {};
    const {
      user = '',
      app = '',
      org = '',
      x500id = ''
    } = window?.appConfig?.commonConfig || {};

    const cspa = convertClientInfoId(clientId, systemId, principleId, agentId);

    const isRetry = getState().configurationContactAccountEntitlement.isRetry;

    const requestBody1: CreateClientConfigurationRequest = {
      common: {
        agent: agentId,
        clientNumber: clientId,
        system: systemId,
        prin: principleId,
        user,
        app
      },
      agent: agentId,
      clientNumber: clientId,
      system: systemId,
      prin: principleId,
      userId: user,
      portfolioName: description,
      x500Id: x500id,
      orgId: org,
      //need to hardcore binSequences
      binSequences: BINSEQUENCES,
      additionalFields: [
        {
          name: CLIENT_CONFIG_STATUS_KEY,
          active: true
        }
      ],
      copyDefaultConfig: true
    };
    const { privileges } = window.appConfig.commonConfig || {};
    const requestBody2: TriggerSaveClientConfigRequest = {
      common: {
        user,
        app,
        org,
        privileges
      },
      defaultCspa: convertClientInfoId(
        clientDefault,
        systemDefault,
        prinDefault,
        agentDefault
      ),
      callerAccountEntitlements: {
        cspas: [
          {
            cspa,
            description,
            active: true
          }
        ]
      },
      clientConfig: {
        cspas: [
          {
            cspa,
            description,
            active: true
          }
        ]
      },
      codeToText: {
        cspas: [
          {
            cspa,
            description,
            active: true
          }
        ]
      }
    };

    if (isRetry) {
      const retryResponse = await dispatch(
        clientConfigurationActions.triggerSaveClientConfiguration(requestBody2)
      );
      if (isFulfilled(retryResponse)) {
        batch(() => {
          dispatch(
            configurationContactAccountEntitlementActions.setAddClientConfigErrorMessage(
              {
                error: '',
                isRetry: false
              }
            )
          );
          dispatch(
            successApi({
              toastMsg:
                'txt_contact_entitlement_add_secure_case_management_success',
              nameAction: NAME_ACTION.addContact
            })
          );
          dispatch(
            saveChangedClientConfig({
              action: 'ADD',
              changedCategory: 'contactAccountStatusEntitlement',
              newValue: {
                cspa,
                description
              }
            })
          );
        });
      } else {
        dispatch(
          apiErrorNotificationAction.updateThunkApiError({
            storeId: API_ERROR_STORE_ID.ENTITLEMENT,
            forSection: 'inModalBody',
            rejectData: {
              ...retryResponse,
              payload: retryResponse.payload?.response
            }
          })
        );
        dispatch(
          rejectApi({
            errorMsgInline:
              'txt_contact_entitlement_inline_failed_secure_case_management',
            toastMsg:
              'txt_contact_entitlement_add_secure_case_management_failed',
            nameAction: NAME_ACTION.addContact
          })
        );
        return;
      }
    }

    const response1 = await dispatch(addClientConfiguration(requestBody1));

    if (isRejected(response1)) {
      batch(() => {
        dispatch(
          apiErrorNotificationAction.updateThunkApiError({
            storeId: API_ERROR_STORE_ID.ENTITLEMENT,
            forSection: 'inModalBody',
            rejectData: {
              ...response1,
              payload: response1.payload?.response
            }
          })
        );
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'error',
            message:
              'txt_contact_entitlement_failed_to_add_case_and_secure_management'
          })
        );
      });
      return rejectWithValue({});
    }

    if (response1?.payload?.data?.errors) {
      dispatch(
        configurationContactAccountEntitlementActions.setAddClientConfigErrorMessage(
          {
            error: 'txt_contact_entitlement_already_exists',
            isRetry: false
          }
        )
      );
      return;
    }

    const response2 = await dispatch(
      clientConfigurationActions.triggerSaveClientConfiguration(requestBody2)
    );

    if (isFulfilled(response2)) {
      batch(() => {
        dispatch(
          successApi({
            toastMsg: 'txt_save_contact_entitlement_config_message',
            nameAction: NAME_ACTION.addContact
          })
        );
        dispatch(
          saveChangedClientConfig({
            action: 'ADD',
            changedCategory: 'contactAccountStatusEntitlement',
            newValue: {
              cspa,
              description
            }
          })
        );
      });
    }

    if (isRejected(response2)) {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'txt_contact_entitlement_add_case_management_success'
        })
      );

      dispatch(
        apiErrorNotificationAction.updateThunkApiError({
          storeId: API_ERROR_STORE_ID.ENTITLEMENT,
          forSection: 'inModalBody',
          rejectData: {
            ...response2,
            payload: response2.payload?.response
          }
        })
      );

      dispatch(
        rejectApi({
          errorMsgInline:
            'txt_contact_entitlement_inline_failed_secure_case_management',
          toastMsg: 'txt_contact_entitlement_add_secure_case_management_failed',
          nameAction: NAME_ACTION.addContact
        })
      );
      return rejectWithValue({});
    }
  }
);

export const addCSPABuilder = (
  builder: ActionReducerMapBuilder<ContactAccStatusEntitlementState>
) => {
  builder
    .addCase(triggerAddCSPA.pending, (draftState, action) => {
      draftState.isLoadingModal = true;
    })
    .addCase(triggerAddCSPA.fulfilled, (draftState, action) => {
      draftState.isLoadingModal = false;
    })
    .addCase(triggerAddCSPA.rejected, (draftState, action) => {
      draftState.isLoadingModal = false;
    });
};
