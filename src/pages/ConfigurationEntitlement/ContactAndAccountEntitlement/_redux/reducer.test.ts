import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { SortType } from 'app/_libraries/_dls/components/Grid/types';
import {
  configurationContactAccountEntitlementActions as actions,
  initialState as configurationContactAccountEntitlement
} from './reducer';
import cloneDeep from 'lodash.clonedeep';
import set from 'lodash.set';
import { ENTITLEMENT_COMPONENTS } from 'app/entitlements/constants';

describe('Test Reducer Configuration Entilement', () => {
  it('onChangeTextSearch', () => {
    const expected = 'mock';
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(actions.onChangeTextSearch(expected));

    const actual =
      store.getState().configurationContactAccountEntitlement.textSearch;

    expect(actual).toEqual(expected);
  });

  it('setSelectedCSPA', () => {
    const expectPayload = {
      id: 'id',
      cspaId: 'cspaId',
      clientId: 'clientId',
      systemId: 'systemId',
      principleId: 'principleId',
      agentId: 'agentId'
    };
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(actions.setSelectedCSPA(expectPayload));

    const actual =
      store.getState().configurationContactAccountEntitlement.selectedCSPA;

    expect(actual).toEqual(expectPayload);
  });

  it('openStatusModal', () => {
    const expectPayload = true;
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(actions.openStatusModal());

    const actual =
      store.getState().configurationContactAccountEntitlement.openStatusModal;

    expect(actual).toEqual(expectPayload);
  });

  it('closeStatusModal', () => {
    const expectPayload = false;
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(actions.closeStatusModal());

    const actual =
      store.getState().configurationContactAccountEntitlement.openStatusModal;

    expect(actual).toEqual(expectPayload);
  });

  it('setSelectedCSPA', () => {
    const expectPayload = {
      id: 'id',
      cspaId: 'cspaId',
      clientId: 'clientId',
      systemId: 'systemId',
      principleId: 'principleId',
      agentId: 'agentId'
    };
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(actions.setSelectedCSPA(expectPayload));

    const actual =
      store.getState().configurationContactAccountEntitlement.selectedCSPA;

    expect(actual).toEqual(expectPayload);
  });

  it('openStatusModal', () => {
    const expectPayload = true;
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(actions.openStatusModal());

    const actual =
      store.getState().configurationContactAccountEntitlement.openStatusModal;

    expect(actual).toEqual(expectPayload);
  });

  it('closeStatusModal', () => {
    const expectPayload = false;
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(actions.closeStatusModal());

    const actual =
      store.getState().configurationContactAccountEntitlement.openStatusModal;

    expect(actual).toEqual(expectPayload);
  });

  it('onChangeViewEntitlement', () => {
    const expected = {
      id: 'id',
      clientId: 'clientId',
      systemId: 'systemId',
      principleId: 'principleId',
      agentId: 'agentId',
      description: 'description'
    };
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(actions.onChangeViewEntitlement(expected));

    const actual =
      store.getState().configurationContactAccountEntitlement.selectedCSPA;

    expect(actual).toEqual(expected);
  });

  it('onChangeSort', () => {
    const expected: SortType = { id: 'systemId', order: 'asc' };
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(actions.onChangeSort(expected));

    const actual =
      store.getState().configurationContactAccountEntitlement.sortBy;

    expect(actual).toEqual(expected);
  });

  it('onRemoveTab', () => {
    const expected = configurationContactAccountEntitlement;
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(actions.onRemoveTab());

    const actual = store.getState().configurationContactAccountEntitlement;

    expect(actual).toEqual(expected);
  });

  it('setAddClientConfigErrorMessage', () => {
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(
      actions.setAddClientConfigErrorMessage({ error: 'payload' })
    );

    const actual = store.getState().configurationContactAccountEntitlement;

    expect(actual.addCSPAErrorMsg).toEqual('payload');
    expect(actual.isRetry).toEqual(undefined);
  });

  it('setAddClientConfigErrorMessage > error is undefined', () => {
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(actions.setAddClientConfigErrorMessage({}));

    const actual = store.getState().configurationContactAccountEntitlement;

    expect(actual.addCSPAErrorMsg).toEqual('');
    expect(actual.isRetry).toEqual(undefined);
  });

  it('setConfigWarningMessage', () => {
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(actions.setConfigWarningMessage({}));

    const actual = store.getState().configurationContactAccountEntitlement;

    expect(actual).toBeTruthy();
  });

  it('onOpenModalAddCSPA', () => {
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(actions.onOpenModalAddCSPA());

    const actual = store.getState().configurationContactAccountEntitlement;

    expect(actual.isOpenAddCSPA).toBeTruthy();
  });

  it('onCloseModalAddCSPA', () => {
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(actions.onCloseModalAddCSPA());

    const actual = store.getState().configurationContactAccountEntitlement;

    expect(actual.isOpenAddCSPA).toBeFalsy();
  });

  it('onCloseModalAddCSPA', () => {
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(actions.onCloseModalAddCSPA());

    const actual = store.getState().configurationContactAccountEntitlement;

    expect(actual.isOpenAddCSPA).toBeFalsy();
  });

  it('onResetTab', () => {
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(actions.onResetTab());

    const actual = store.getState().configurationContactAccountEntitlement;

    expect(actual.entitlements).toEqual([]);
  });

  it('onChangeOpenViewElement', () => {
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(actions.onChangeOpenViewElement());

    const actual = store.getState().configurationContactAccountEntitlement;

    expect(actual.isOpenViewElement).toBeTruthy();
  });

  it('onChangeToggle', () => {
    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement
    });

    store.dispatch(actions.onChangeToggle());

    const actual = store.getState().configurationContactAccountEntitlement;

    expect(actual.isToggle).toBeTruthy();
  });

  it('onDiscardChange', () => {
    const entitlements = [
      {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION,
        entitlements: [{}]
      }
    ];

    const newState = cloneDeep(configurationContactAccountEntitlement);
    set(newState, ['entitlements'], entitlements);
    set(newState, ['activeTabEntitlement'], entitlements);

    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement: newState
    });
    store.dispatch(actions.onDiscardChange());
    const actual = store.getState().configurationContactAccountEntitlement;
    expect(actual.isSelectedAll).toBeFalsy();
  });

  it('onChangePrevious', () => {
    const entitlements = [
      {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION,
        entitlements: [{}]
      }
    ];

    const newState = cloneDeep(configurationContactAccountEntitlement);
    set(newState, ['entitlements'], entitlements);

    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement: newState
    });
    store.dispatch(actions.onChangePrevious(entitlements[0]));
    const actual = store.getState().configurationContactAccountEntitlement;
    expect(actual.isSelectedAll).toBeFalsy();
  });

  it('onChangePrevious > entitlement is undefined', () => {
    const entitlements = [
      {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION
      }
    ];

    const newState = cloneDeep(configurationContactAccountEntitlement);
    set(newState, ['entitlements'], entitlements);

    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement: newState
    });
    store.dispatch(actions.onChangePrevious(entitlements[0]));
    const actual = store.getState().configurationContactAccountEntitlement;
    expect(actual.isSelectedAll).toBeFalsy();
  });

  it('onChangeSelectAll', () => {
    const entitlements = [
      {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION
      }
    ];

    const newState = cloneDeep(configurationContactAccountEntitlement);
    set(newState, ['entitlements'], entitlements);

    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement: newState
    });
    store.dispatch(actions.onChangeSelectAll());
    const actual = store.getState().configurationContactAccountEntitlement;
    expect(actual.isSelectedAll).toBeTruthy();
  });

  it('onChangeCheckbox', () => {
    const payload = {
      checkedValue: 'checkedValue',
      typeCode: 'typeCode'
    };
    const entitlements = [
      {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION
      }
    ];

    const newState = cloneDeep(configurationContactAccountEntitlement);
    set(newState, ['entitlements'], entitlements);

    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement: newState
    });
    store.dispatch(actions.onChangeCheckbox(payload));
    const actual = store.getState().configurationContactAccountEntitlement;
    expect(actual.isSelectedAll).toBeFalsy();
  });

  it('onChangeEntitlement', () => {
    const payload = {
      value: 'value',
      description: 'description'
    };
    const entitlements = [
      {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION
      }
    ];

    const listActiveEntitlements = [
      { value: 'value', description: 'description' }
    ];

    const newState = cloneDeep(configurationContactAccountEntitlement);
    set(newState, ['entitlements'], entitlements);
    set(newState, ['listActiveEntitlements'], listActiveEntitlements);

    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement: newState
    });
    store.dispatch(actions.onChangeEntitlement(payload));
    const actual = store.getState().configurationContactAccountEntitlement;
    expect(actual.isSelectedAll).toBeFalsy();
  });

  it('onChangeEntitlement > activeEntitlement is empty', () => {
    const payload = {
      value: 'value-not-found',
      description: 'description'
    };
    const entitlements = [
      {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION
      }
    ];

    const listActiveEntitlements = [
      { value: 'value', description: 'description' }
    ];

    const newState = cloneDeep(configurationContactAccountEntitlement);
    set(newState, ['entitlements'], entitlements);
    set(newState, ['listActiveEntitlements'], listActiveEntitlements);

    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement: newState
    });
    store.dispatch(actions.onChangeEntitlement(payload));
    const actual = store.getState().configurationContactAccountEntitlement;
    expect(actual.isSelectedAll).toBeFalsy();
  });

  it('onChangeActiveTab', () => {
    const payload = {
      translateFn: jest.fn(),
      component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION
    };
    const entitlements = [
      {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION
      }
    ];

    const newState = cloneDeep(configurationContactAccountEntitlement);
    set(newState, ['entitlements'], entitlements);

    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement: newState
    });
    store.dispatch(actions.onChangeActiveTab(payload));
    const actual = store.getState().configurationContactAccountEntitlement;
    expect(actual.isSelectedAll).toBeFalsy();
  });

  it('onChangeActiveTab > when component was not found', () => {
    const payload = {
      translateFn: jest.fn(),
      component: ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH
    };
    const entitlements = [
      {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION
      }
    ];

    const newState = cloneDeep(configurationContactAccountEntitlement);
    set(newState, ['entitlements'], entitlements);

    const store = createStore(rootReducer, {
      configurationContactAccountEntitlement: newState
    });
    store.dispatch(actions.onChangeActiveTab(payload));
    const actual = store.getState().configurationContactAccountEntitlement;
    expect(actual.isSelectedAll).toBeFalsy();
  });
});
