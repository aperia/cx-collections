import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import isNil from 'lodash.isnil';

// Service
import contactAndAccountEntitlementService from '../contactAndAccountEntitlementService';

// Type
import {
  GetContactAccEntitlementPayload,
  ContactAccStatusEntitlementState,
  GetContactAccEntitlementArgs
} from '../types';

// Helper
import {
  buildContactAccountMasterStructure,
  buildListActiveEntitlements,
  convertContactEntitlementToSectionView
} from '../helpers';
import { detectAllDataChecked } from '../ViewEntitlement/helpers';

// Const
import { SECURE_CONFIG_KY } from 'pages/ConfigurationEntitlement/constants';
import { configurationContactAccountEntitlementActions } from './reducer';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { lowerCaseViewEntitlement } from 'pages/ConfigurationEntitlement/helpers';

export const getContactAccEntitlement = createAsyncThunk<
  GetContactAccEntitlementPayload,
  GetContactAccEntitlementArgs,
  ThunkAPIConfig
>(
  'configurationContactAndAccountEntitlement/getContactAccEntitlement',
  async (args, thunkAPI) => {
    const { getState, dispatch } = thunkAPI;

    const { commonConfig } = window.appConfig;
    const { configurationContactAccountEntitlement } = getState();
    const { selectedCSPA } = configurationContactAccountEntitlement;
    const {
      agentId,
      clientId,
      principleId,
      systemId,
      cspaId = ''
    } = selectedCSPA;

    const { data } =
      await contactAndAccountEntitlementService.getContactAccEntitlement({
        common: {
          agent: agentId,
          clientNumber: clientId,
          system: systemId,
          prin: principleId,
          app: commonConfig?.app,
          org: commonConfig?.org
        },
        selectConfig: SECURE_CONFIG_KY.CALLER_ACC_ENTITLEMENT,
        selectCspa: cspaId
      });

    const {
      configs: { callerAccountEntitlements: entitlement }
    } = data;

    const callerAccountEntitlements: MagicKeyValue = lowerCaseViewEntitlement(
      entitlement?.cspas
    );

    if (isEmpty(callerAccountEntitlements)) {
      dispatch(
        configurationContactAccountEntitlementActions.setConfigWarningMessage(
          'txt_contact_entitlement_warning_msg'
        )
      );
    }

    const currentCSPA = callerAccountEntitlements?.cspas?.find(
      (item: MagicKeyValue) => item.cspa === selectedCSPA.cspaId
    );

    return {
      entitlements: isNil(currentCSPA)
        ? buildContactAccountMasterStructure(args.translateFn)
        : convertContactEntitlementToSectionView(
            currentCSPA?.components || [],
            args.translateFn
          ),
      listContactAccountCSPA: callerAccountEntitlements?.cspas || []
    };
  }
);

export const getContactAccEntitlementBuilder = (
  builder: ActionReducerMapBuilder<ContactAccStatusEntitlementState>
) => {
  const { pending, fulfilled, rejected } = getContactAccEntitlement;

  builder
    .addCase(pending, draftState => {
      return {
        ...draftState,
        isLoadingModal: true
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      const { entitlements, listContactAccountCSPA } = action.payload;
      const [defaultActiveSection] = entitlements;
      const { translateFn } = action.meta.arg;

      const listActiveEntitlements = buildListActiveEntitlements(
        defaultActiveSection?.entitlements,
        translateFn
      );

      const [activeEntitlement] = listActiveEntitlements;

      const isSelectedAll = detectAllDataChecked(
        defaultActiveSection.entitlements,
        activeEntitlement
      );

      return {
        ...draftState,
        entitlements,
        activeTabEntitlement: defaultActiveSection,
        listContactAccountCSPA,
        activeEntitlement: activeEntitlement,
        listActiveEntitlements,
        isLoadingModal: false,
        isSelectedAll
      };
    })
    .addCase(rejected, draftState => {
      return {
        ...draftState,
        isLoadingModal: false
      };
    });
};
