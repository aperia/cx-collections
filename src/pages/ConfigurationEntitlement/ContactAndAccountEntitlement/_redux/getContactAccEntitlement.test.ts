import { createStore, PayloadAction } from '@reduxjs/toolkit';
import { rootReducer } from 'storeConfig';
import { responseDefault } from 'app/test-utils';
import { getContactAccEntitlement } from './getContactAccEntitlement';
import contactAndAccountEntitlementService from '../contactAndAccountEntitlementService';
import { GetContactAccEntitlementPayload } from '../types';
import { isEmpty } from 'app/_libraries/_dls/lodash';

let spy: jest.SpyInstance;

describe('Test getContactAccEntitlement async thunk', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  window.appConfig = {
    commonConfig: {
      app: '',
      org: ''
    }
  } as AppConfiguration;

  const fulfilledInitialState = {
    configurationContactAccountEntitlement: {
      selectedCSPA: {
        agentId: '',
        clientId: '',
        principleId: '',
        systemId: '',
        cspaId: ''
      }
    }
  };

  it('fulfilled', async () => {
    spy = jest
      .spyOn(contactAndAccountEntitlementService, 'getContactAccEntitlement')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          configs: {
            callerAccountEntitlements: {
              cspas: [
                {
                  cspa: ''
                }
              ]
            }
          }
        }
      });

    const store = createStore(rootReducer, fulfilledInitialState);

    const payloadAction = (await getContactAccEntitlement({
      translateFn: () => {}
    })(
      store.dispatch,
      store.getState,
      {}
    )) as PayloadAction<GetContactAccEntitlementPayload>;
    expect(payloadAction.type).toEqual(
      'configurationContactAndAccountEntitlement/getContactAccEntitlement/fulfilled'
    );
    expect(isEmpty(payloadAction.payload.entitlements)).toBe(false);
    expect(isEmpty(payloadAction.payload.listContactAccountCSPA)).toBe(false);
  });

  it('rejected', async () => {
    spy = jest
      .spyOn(contactAndAccountEntitlementService, 'getContactAccEntitlement')
      .mockRejectedValue({});
    const store = createStore(rootReducer, {});

    const payloadAction = await getContactAccEntitlement({
      translateFn: () => {}
    })(store.dispatch, store.getState, {});

    expect(payloadAction.type).toEqual(
      'configurationContactAndAccountEntitlement/getContactAccEntitlement/rejected'
    );
  });

  it('should run buildContactAccountMasterStructure if currentCSPA is null or undefined', async () => {
    spy = jest
      .spyOn(contactAndAccountEntitlementService, 'getContactAccEntitlement')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          configs: {
            callerAccountEntitlements: {}
          }
        }
      });

    const store = createStore(rootReducer, fulfilledInitialState);

    const payloadAction = (await getContactAccEntitlement({
      translateFn: () => {}
    })(
      store.dispatch,
      store.getState,
      {}
    )) as PayloadAction<GetContactAccEntitlementPayload>;

    expect(!isEmpty(payloadAction.payload.entitlements)).toBe(true);
    expect(isEmpty(payloadAction.payload.listContactAccountCSPA)).toBe(true);
  });
});
