import { batch } from 'react-redux';
import {
  createAsyncThunk,
  isRejected,
  isFulfilled,
  ActionReducerMapBuilder
} from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { configurationContactAccountEntitlementActions } from './reducer';

import {
  BINSEQUENCES,
  CLIENT_CONFIG_STATUS_KEY
} from 'pages/ClientConfiguration/constants';
import { changeStatusClientConfiguration } from 'pages/ClientConfiguration/_redux/changeStatusClientConfiguration';
import {
  AdditionalFieldsMapType,
  ChangeStatusClientConfigurationArgs,
  ChangeStatusClientConfigurationRequest,
  TriggerSaveClientConfigRequest
} from 'pages/ClientConfiguration/types';
import { ContactAccStatusEntitlementState } from '../types';
import { ACTIVE_DESCRIPTION } from 'pages/ConfigurationEntitlement/constants';
import { saveChangedClientConfig } from 'pages/ClientConfiguration/ChangeHistory/_redux/saveChangedClientConfig';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { convertClientInfoId } from 'pages/ClientConfiguration/helpers';

export const triggerChangeStatusCSPA = createAsyncThunk<
  void,
  ChangeStatusClientConfigurationArgs,
  ThunkAPIConfig
>('CSPA/triggerChangeStatusCSPA', async (args, thunkAPI) => {
  const { dispatch, rejectWithValue, getState } = thunkAPI;
  const { id, additionalFields } = args;
  const { user, app, org, privileges } = window.appConfig.commonConfig;
  const { accessConfig, configurationContactAccountEntitlement } = getState();
  const [clientNumber = '', system = '$ALL', prin = '$ALL', agent = '$ALL'] =
    accessConfig?.cspaList?.[0]?.cspa?.split('-') || [];
  const {
    agentId = '',
    clientId = '',
    systemId = '',
    principleId = '',
    description = ''
  } = configurationContactAccountEntitlement.selectedCSPA;
  const cspa = convertClientInfoId(clientId, systemId, principleId, agentId);

  const isConfigStatus = additionalFields?.find(
    (item: AdditionalFieldsMapType) => item.name === CLIENT_CONFIG_STATUS_KEY
  );

  const isActive = isConfigStatus?.active === true;

  const requestBody: ChangeStatusClientConfigurationRequest = {
    common: {
      clientNumber,
      system,
      prin,
      agent,
      user,
      app
    },
    clientInfoId: id,
    binSequences: BINSEQUENCES,
    additionalFields: [
      {
        name: CLIENT_CONFIG_STATUS_KEY,
        active: !isActive,
        description: ACTIVE_DESCRIPTION
      }
    ]
  };
  const response = await dispatch(changeStatusClientConfiguration(requestBody));

  const secureConfigItem = {
    cspas: [
      {
        cspa,
        description,
        active: !isActive
      }
    ]
  };

  const saveSecureInfoRequestBody: TriggerSaveClientConfigRequest = {
    common: {
      user,
      app,
      org,
      privileges
    },
    callerAccountEntitlements: secureConfigItem,
    clientConfig: secureConfigItem,
    codeToText: secureConfigItem
  };

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: isActive ? 'txt_cspa_deactivated' : 'txt_cspa_activated'
        })
      );
      dispatch(
        configurationContactAccountEntitlementActions.closeStatusModal()
      );
      dispatch(
        configurationContactAccountEntitlementActions.getContactAccListCSPA()
      );
      dispatch(
        saveChangedClientConfig({
          action: isActive ? 'DEACTIVATE' : 'ACTIVATE',
          changedCategory: 'contactAccountStatusEntitlement',
          oldValue: {
            active: isActive
          },
          newValue: {
            active: !isActive
          }
        })
      );
      dispatch(
        clientConfigurationActions.triggerSaveClientConfiguration(
          saveSecureInfoRequestBody
        )
      );
    });
  }

  if (isRejected(response)) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: isActive
          ? 'txt_cspa_failed_to_deactivate'
          : 'txt_cspa_failed_to_activate'
      })
    );
    return rejectWithValue({});
  }
});

export const changeStatusCSPABuilder = (
  builder: ActionReducerMapBuilder<ContactAccStatusEntitlementState>
) => {
  builder
    .addCase(triggerChangeStatusCSPA.pending, (draftState, action) => {
      draftState.isLoadingModal = true;
    })
    .addCase(triggerChangeStatusCSPA.fulfilled, (draftState, action) => {
      draftState.isLoadingModal = false;
    })
    .addCase(triggerChangeStatusCSPA.rejected, (draftState, action) => {
      draftState.isLoadingModal = false;
    });
};
