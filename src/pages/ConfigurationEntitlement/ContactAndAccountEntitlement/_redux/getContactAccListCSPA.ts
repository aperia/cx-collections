import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isRejected
} from '@reduxjs/toolkit';

// Service
import contactAndAccountEntitlementService from '../contactAndAccountEntitlementService';

// Type
import {
  ContactAccStatusEntitlementState,
  GetContactAccListCSPAPayload
} from '../types';

// Helper
import { mappingDataFromObj } from 'app/helpers';
import { AxiosResponse } from 'axios';
import { buildListDefaultSort } from 'pages/ConfigurationEntitlement/helpers';

export const getContactAccListCSPAApi = createAsyncThunk<
  AxiosResponse,
  undefined,
  ThunkAPIConfig
>(
  'configurationContactAndAccountEntitlement/getContactAccListCSPAApi',
  async (_, { rejectWithValue, getState }) => {
    try {
      const { commonConfig } = window.appConfig;
      const { app, user, org } = commonConfig;
      const { accessConfig } = getState();
      const [
        clientNumber = '',
        system = '$ALL',
        prin = '$ALL',
        agent = '$ALL'
      ] = accessConfig?.cspaList?.[0]?.cspa?.split('-') || [];

      return await contactAndAccountEntitlementService.getContactAccListCSPA({
        common: {
          agent,
          clientNumber,
          system,
          prin,
          clientName: 'firstdata',
          user,
          app
        },
        orgId: org,
        fetchSize: '500'
      });
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const getContactAccListCSPA = createAsyncThunk<
  GetContactAccListCSPAPayload,
  undefined,
  ThunkAPIConfig
>(
  'configurationContactAndAccountEntitlement/getContactAccListCSPA',
  async (_, { getState, dispatch, rejectWithValue }) => {
    try {
      const { mapping } = getState();

      const response = await dispatch(getContactAccListCSPAApi());

      if (isRejected(response)) throw response;

      const { clientInfoList } = response.payload.data;

      const { clientConfig: clientConfigMapping = {} } = mapping?.data || {};

      const cspas: Array<CSPA> = clientInfoList.map((item: MagicKeyValue) =>
        mappingDataFromObj(item, clientConfigMapping)
      );

      return {
        cspas: buildListDefaultSort(cspas)
      };
    } catch (error) {
      return rejectWithValue({ response: error });
    }
  }
);

export const getContactAccListCSPABuilder = (
  builder: ActionReducerMapBuilder<ContactAccStatusEntitlementState>
) => {
  const { pending, fulfilled, rejected } = getContactAccListCSPA;

  builder
    .addCase(pending, draftState => {
      return {
        ...draftState,
        isLoadingListCSPA: true,
        isErrorLoadingListCSPA: false
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      const { cspas } = action.payload;

      return {
        ...draftState,
        listCSPA: cspas,
        isLoadingListCSPA: false,
        isNoResultsFound: !cspas.length,
        isErrorLoadingListCSPA: false
      };
    })
    .addCase(rejected, draftState => {
      return {
        ...draftState,
        isLoadingListCSPA: false,
        isErrorLoadingListCSPA: true
      };
    });
};
