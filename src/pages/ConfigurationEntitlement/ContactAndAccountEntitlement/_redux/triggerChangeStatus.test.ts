import { mockActionCreator, responseDefault } from 'app/test-utils';
import { clientConfigurationServices } from 'pages/ClientConfiguration/clientConfigurationServices';
import { CLIENT_CONFIG_STATUS_KEY } from 'pages/ClientConfiguration/constants';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import { triggerChangeStatusCSPA } from './triggerChangeStatus';

const initialState: Partial<RootState> = {};

const store = createStore(rootReducer, initialState, applyMiddleware(thunk));

window.appConfig = {
  commonConfig: {
    app: 'app',
    org: 'COLX'
  }
} as AppConfiguration;

const mockToast = mockActionCreator(actionsToast);
const translateFn = (text: string) => text;

const mockCallApi = (isError: boolean) => {
  const mockChangeStatusClientConfig = jest.spyOn(
    clientConfigurationServices,
    'changeStatusClientConfig'
  );
  return isError
    ? mockChangeStatusClientConfig.mockRejectedValue({})
    : mockChangeStatusClientConfig.mockResolvedValue({
        ...responseDefault
      });
};

describe('triggerChangeStatusCSPA', () => {
  it('It should run correctly', async () => {
    const successToast = mockToast('addToast');
    const args = {
      id: 'id',
      additionalFields: [
        {
          name: 'string',
          active: false,
          value: 'value',
          description: 'description'
        }
      ],
      translateFn
    };
    mockCallApi(false);
    await triggerChangeStatusCSPA(args)(store.dispatch, store.getState, {});
    expect(successToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: 'txt_cspa_activated'
    });
  });

  it('It should run correctly and isActive is true', async () => {
    const successToast = mockToast('addToast');
    const args = {
      id: 'id',
      additionalFields: [
        {
          name: CLIENT_CONFIG_STATUS_KEY,
          active: true,
          value: 'value',
          description: 'description'
        }
      ],
      translateFn
    };
    mockCallApi(false);
    await triggerChangeStatusCSPA(args)(store.dispatch, store.getState, {});
    expect(successToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: 'txt_cspa_deactivated'
    });
  });

  it('It should run reject', async () => {
    const successToast = mockToast('addToast');
    const args = {
      id: 'id',
      additionalFields: [
        {
          name: 'string',
          active: false,
          value: 'value',
          description: 'description'
        }
      ],
      translateFn
    };
    mockCallApi(true);
    await triggerChangeStatusCSPA(args)(store.dispatch, store.getState, {});
    expect(successToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: 'txt_cspa_failed_to_activate'
    });
  });

  it('It should run reject > isActive is true', async () => {
    const successToast = mockToast('addToast');
    const args = {
      id: 'id',
      additionalFields: [
        {
          name: CLIENT_CONFIG_STATUS_KEY,
          active: true,
          value: 'value',
          description: 'description'
        }
      ],
      translateFn
    };
    mockCallApi(true);
    await triggerChangeStatusCSPA(args)(store.dispatch, store.getState, {});
    expect(successToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: 'txt_cspa_failed_to_deactivate'
    });
  });

  it('changeStatusCSPABuilder > reject', () => {
    const store = createStore(rootReducer, { initialState: {} } as never);
    const state = store.getState();
    const payload = triggerChangeStatusCSPA.rejected(
      { name: 'ErrorName', message: 'error message' },
      triggerChangeStatusCSPA.rejected.type,
      { id: 'id', translateFn }
    );
    const actual = rootReducer(state, payload);
    expect(actual.configurationUserRoleEntitlement.isLoadingModal).toBeFalsy();
  });
});
