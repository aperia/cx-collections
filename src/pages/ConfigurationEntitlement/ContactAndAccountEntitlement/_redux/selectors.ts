import { createSelector } from '@reduxjs/toolkit';
import { SortType } from 'app/_libraries/_dls/components';

// Helper
import { isArrayHasValue } from 'app/helpers';
import { searchCSPA, sortCSPA } from '../../helpers';
import {
  ConfigData,
  AdditionalFieldsMapType
} from 'pages/ClientConfiguration/types';
import { DEFAULT_CLIENT_CONFIG_KEY } from 'pages/ClientConfiguration/constants';

const getCSPA = (state: RootState) => {
  return state.configurationContactAccountEntitlement.listCSPA;
};

const getSelectedAllStatus = (state: RootState) => {
  return state.configurationContactAccountEntitlement.isSelectedAll;
};

const getNoResultsFoundStatus = (state: RootState) => {
  return state.configurationContactAccountEntitlement.isNoResultsFound;
};

const getLoadingListCSPAStatus = (state: RootState) => {
  return state.configurationContactAccountEntitlement.isLoadingListCSPA;
};

const getSortBy = (state: RootState): SortType => {
  return state.configurationContactAccountEntitlement.sortBy;
};

const getTextSearch = (state: RootState): string => {
  return state.configurationContactAccountEntitlement.textSearch;
};

const getOpenAddCSPAStatus = (state: RootState) =>
  state.configurationContactAccountEntitlement.isOpenAddCSPA;

const getAddErrorMsg = (state: RootState) =>
  state.configurationContactAccountEntitlement.addCSPAErrorMsg;

const getErrorLoadingListCSPA = (state: RootState) =>
  state.configurationContactAccountEntitlement.isErrorLoadingListCSPA;

export const getActiveTab = (state: RootState) =>
  state.configurationContactAccountEntitlement.activeTabEntitlement;

export const getListCSPALoadingStatus = (state: RootState) =>
  state.configurationContactAccountEntitlement.isLoadingModal;

export const getSelectedCSPA = (state: RootState) =>
  state.configurationContactAccountEntitlement.selectedCSPA;

export const getLoadingContactAccountEntitlementStatus = (state: RootState) =>
  state.configurationContactAccountEntitlement.isLoadingModal;

export const getOpenSettingStatusContactAccountEntitlement = (
  state: RootState
) => state.configurationContactAccountEntitlement.isOpenViewElement;

export const getStatusContactAccountEntitlement = (state: RootState) =>
  state.configurationContactAccountEntitlement.entitlements;

export const getActiveEntitlement = (state: RootState) =>
  state.configurationContactAccountEntitlement.activeEntitlement;

export const getListActiveEntitlements = (state: RootState) =>
  state.configurationContactAccountEntitlement.listActiveEntitlements;

export const getToggleStatus = (state: RootState) =>
  state.configurationContactAccountEntitlement.isToggle;

export const getListCriteria = (state: RootState) =>
  state.configurationContactAccountEntitlement.listCriteria;

export const getIsRetry = (state: RootState) =>
  state.configurationContactAccountEntitlement.isRetry;

export const getWarningMessage = (state: RootState) =>
  state.configurationContactAccountEntitlement.warningMessage;

export const getActiveTabEntitlementData = (
  state: RootState,
  criteriaCode: string
) => {
  const { activeEntitlement, activeTabEntitlement } =
    state.configurationContactAccountEntitlement;

  const currentEntitlement = activeTabEntitlement.entitlements?.find(
    item => item.entitlementCode === activeEntitlement.value
  );

  const currentCriteria = currentEntitlement?.data?.find(
    item => item.criteriaCode === criteriaCode
  );

  return currentCriteria?.items || [];
};

export const takeOpenAddCSPAStatus = createSelector(
  getOpenAddCSPAStatus,
  data => data
);

export const takeWarningMessage = createSelector(
  getWarningMessage,
  data => data
);

export const takeIsRetry = createSelector(getIsRetry, data => data);

export const takeAddErrorMsg = createSelector(getAddErrorMsg, data => data);

export const takeSelectedAllStatus = createSelector(
  getSelectedAllStatus,
  (data: boolean) => data
);

export const takeListCSPALoadingStatus = createSelector(
  getLoadingListCSPAStatus,
  (data: boolean) => data
);

export const takeNoResultsFoundStatus = createSelector(
  getNoResultsFoundStatus,
  (data: boolean) => data
);

export const takeToggleStatus = createSelector(
  getToggleStatus,
  (data: boolean) => data
);

export const takeActiveEntitlement = createSelector(
  getActiveEntitlement,
  data => data
);

export const takeListActiveEntitlements = createSelector(
  getListActiveEntitlements,
  data => data
);

export const takeStatusContactAccountEntitlement = createSelector(
  getStatusContactAccountEntitlement,
  data => data
);

export const selectTextSearch = createSelector(
  getTextSearch,
  (textSearch: string) => textSearch
);

export const selectRootLoading = createSelector(
  getListCSPALoadingStatus,
  (isLoadingModal: boolean) => isLoadingModal
);

export const selectSelectedCSPA = createSelector(getSelectedCSPA, CSPA => CSPA);

export const selectOpenStatusModal = createSelector(
  (state: RootState) =>
    state.configurationContactAccountEntitlement.openStatusModal,
  data => data
);

export const selectCSPA = createSelector(
  getCSPA,
  getTextSearch,
  getSortBy,
  (CSPA, textSearch, sortBy) =>
    sortCSPA(searchCSPA(CSPA || [], textSearch), sortBy)
);

export const takeErrorLoadingListCSPA = createSelector(
  getErrorLoadingListCSPA,
  (data: boolean) => data
);

export const takeActiveTabData = createSelector(getActiveTab, data => data);

export const takeListCriteria = createSelector([getListCriteria], data => data);

export const takeActiveTabEntitlementData = createSelector(
  [getActiveTabEntitlementData],
  data => data
);

export const selectIsSearchNoData = createSelector(
  [getCSPA, getTextSearch],
  (CSPA, textSearch: string) => {
    return !isArrayHasValue(searchCSPA(CSPA || [], textSearch)) && textSearch
      ? true
      : false;
  }
);

export const selectSort = createSelector(
  getSortBy,
  (sortBy: SortType) => sortBy
);

export const selectIsNoData = createSelector(
  getCSPA,
  getTextSearch,
  (CSPA, textSearch: string) => {
    return !isArrayHasValue(CSPA || []) && !textSearch ? true : false;
  }
);

export const selectDisplayClearAndReset = createSelector(
  getCSPA,
  getTextSearch,
  (CSPA, textSearch: string) => {
    return isArrayHasValue(searchCSPA(CSPA || [], textSearch)) && textSearch
      ? true
      : false;
  }
);

export const selectOpenViewElement = createSelector(
  getOpenSettingStatusContactAccountEntitlement,
  (data: boolean) => data
);

export const takeLoadingStatus = createSelector(
  getLoadingContactAccountEntitlementStatus,
  (data: boolean) => data
);

export const selectDefaultSettingsClientConfig = createSelector(
  getCSPA,
  (data: ConfigData[]) => {
    return data?.find((item: ConfigData) =>
      item.additionalFields?.find(
        (el: AdditionalFieldsMapType) =>
          el.name === DEFAULT_CLIENT_CONFIG_KEY && el.value === 'true'
      )
    );
  }
);
