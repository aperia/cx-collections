import { isFulfilled, isRejected } from '@reduxjs/toolkit';
import { responseDefault } from 'app/test-utils';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import contactAndAccountEntitlementService from '../contactAndAccountEntitlementService';
import { GetContactAccEntitlementListResponse } from '../types';
import { getContactAccListCSPA } from './getContactAccListCSPA';

const initialState: Partial<RootState> = {};

const store = createStore(rootReducer, initialState, applyMiddleware(thunk));

window.appConfig = {
  commonConfig: {
    app: 'app',
    org: 'COLX'
  }
} as AppConfiguration;

const mockCallApi = (
  isError: boolean,
  responseData: GetContactAccEntitlementListResponse
) => {
  const mockGetContactAccListCSPA = jest.spyOn(
    contactAndAccountEntitlementService,
    'getContactAccListCSPA'
  );
  return isError
    ? mockGetContactAccListCSPA.mockRejectedValue({})
    : mockGetContactAccListCSPA.mockResolvedValue({
        ...responseDefault,
        data: responseData
      });
};

describe('getContactAccListCSPA', () => {
  it('It should run correctly', async () => {
    const apiResponse = { clientInfoList: [{}] };
    mockCallApi(false, apiResponse);
    const response = await getContactAccListCSPA()(
      store.dispatch,
      store.getState,
      {}
    );
    expect(isFulfilled(response)).toBeTruthy();
  });

  it('It should run reject', async () => {
    const apiResponse = { clientInfoList: [{}] };
    mockCallApi(true, apiResponse);
    const response = await getContactAccListCSPA()(
      store.dispatch,
      store.getState,
      {}
    );
    expect(isRejected(response)).toBeTruthy();
  });
});
