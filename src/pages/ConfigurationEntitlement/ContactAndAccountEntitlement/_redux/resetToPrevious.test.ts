import { ENTITLEMENT_COMPONENTS } from 'app/entitlements/constants';
import { resetToPrevious } from './resetToPrevious';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import { configurationContactAccountEntitlementActions } from './reducer';

const initialState: Partial<RootState> = {};

const store = createStore(rootReducer, initialState, applyMiddleware(thunk));

describe('resetToPrevious', () => {
  it('onChangePrevious should be called', async () => {
    const onChangePreviosAction = jest.spyOn(
      configurationContactAccountEntitlementActions,
      'onChangePrevious'
    );
    const confirmMock = jest.fn();
    await resetToPrevious(confirmMock)(store.dispatch, store.getState, {});
    expect(onChangePreviosAction).toBeCalled();
  });

  it('successToast should be called', async () => {
    const entitlements = [
      {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION
      }
    ];
    const activeTabEntitlement = {
      component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION
    };
    const newState = {
      configurationContactAccountEntitlement: {
        addCSPAErrorMsg: '',
        activeTabEntitlement,
        entitlements
      }
    };

    const store = createStore(rootReducer, newState, applyMiddleware(thunk));
    const confirmMock = jest.fn();
    await resetToPrevious(confirmMock)(store.dispatch, store.getState, {});
    expect(confirmMock).not.toBeCalled();
  });
});
