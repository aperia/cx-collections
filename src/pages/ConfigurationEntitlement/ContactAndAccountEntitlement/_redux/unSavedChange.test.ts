import { ENTITLEMENT_COMPONENTS } from 'app/entitlements/constants';
import { mockActionCreator } from 'app/test-utils';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import unSavedChange from './unSavedChange';

const initialState: Partial<RootState> = {};

const store = createStore(rootReducer, initialState, applyMiddleware(thunk));
const mockModalAction = mockActionCreator(confirmModalActions);

describe('unSavedChange', () => {
  it('confirmModalActions should be called', async () => {
    const openModal = mockModalAction('open');
    const confirmMock = jest.fn();
    await unSavedChange(confirmMock)(store.dispatch, store.getState, {});
    expect(openModal).toBeCalledWith({
      title: 'txt_unsaved_changes',
      body: 'txt_discard_change_body',
      cancelText: 'txt_continue_editing',
      confirmText: 'txt_discard_change',
      confirmCallback: confirmMock,
      variant: 'danger'
    });
  });

  it('confirmCallback should be called', async () => {
    const entitlements = [
      {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION
      }
    ];
    const activeTabEntitlement = {
      component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION
    };
    const newState = {
      configurationContactAccountEntitlement: {
        addCSPAErrorMsg: '',
        activeTabEntitlement,
        entitlements
      }
    };

    const store = createStore(rootReducer, newState, applyMiddleware(thunk));
    const confirmMock = jest.fn();
    await unSavedChange(confirmMock)(store.dispatch, store.getState, {});
    expect(confirmMock).toBeCalled();
  });
});
