import { mockActionCreator, responseDefault } from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { applyMiddleware, createStore, Store } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import contactAndAccountEntitlementService from '../contactAndAccountEntitlementService';
import {
  triggerUpdateContactAccEntitlement,
  updateContactAccEntitlement
} from './updateContactAccEntitlement';

const mockToast = mockActionCreator(actionsToast);
const initialState: Partial<RootState> = {
  configurationContactAccountEntitlement: {
    listActiveEntitlements: [
      {
        description: 'activeEntitlement',
        value: 'activeEntitlement'
      },
      {
        description: 'inActiveEntitlement',
        value: 'inActiveEntitlement'
      }
    ],

    entitlements: [{ component: 'abc-component' }],
    listContactAccountCSPA: [{}],
    isLoadingModal: false,
    isLoadingListCSPA: false,
    isNoResultsFound: false,
    isOpenViewElement: true,
    selectedCSPA: {
      cspaId: '1234'
    },
    textSearch: 'textSearch',
    isErrorSettingEntitlement: false,
    isErrorLoadingListCSPA: false,
    sortBy: {
      id: 'id',
      order: 'desc'
    },
    activeTabEntitlement: {
      component: 'abc-component'
    },
    listCriteria: [],
    activeEntitlement: {
      description: 'activeEntitlement',
      value: 'activeEntitlement'
    },
    listCSPA: [{}],
    isToggle: false,
    isSelectedAll: false,
    isOpenAddCSPA: false,
    addCSPAErrorMsg: 'addCSPAErrorMsg',
    openStatusModal: false
  }
};

let store: Store<RootState>;

window.appConfig = {
  commonConfig: {
    app: 'app',
    org: 'COLX'
  }
} as AppConfiguration;

const mockCallApi = (isError: boolean) => {
  const mockUpdateContactAccEntitlement = jest.spyOn(
    contactAndAccountEntitlementService,
    'updateContactAccEntitlement'
  );
  return isError
    ? mockUpdateContactAccEntitlement.mockRejectedValue({})
    : mockUpdateContactAccEntitlement.mockResolvedValue({
        ...responseDefault,
        data: {
          configs: {
            callerAccountEntitlements: {
              cspas: null
            }
          }
        }
      });
};

describe('updateContactAccEntitlement', () => {
  beforeEach(() => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: {
        commonConfig: {
          app: 'app',
          org: 'COLX'
        }
      }
    });
    store = createStore(rootReducer, initialState, applyMiddleware(thunk));
  });

  it('Should run with empty config', async () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: {}
    });
    const successToast = mockToast('addToast');
    mockCallApi(false);
    const args = { translateFunc: jest.fn() };
    await triggerUpdateContactAccEntitlement(args)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(successToast).toBeCalledTimes(1);
  });

  it('Should run correctly', async () => {
    const successToast = mockToast('addToast');
    mockCallApi(false);
    const args = { translateFunc: jest.fn() };
    await triggerUpdateContactAccEntitlement(args)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(successToast).toBeCalledTimes(1);
  });

  it('Should run correctly', async () => {
    const successToast = mockToast('addToast');
    mockCallApi(false);
    const args = { translateFunc: jest.fn() };
    await triggerUpdateContactAccEntitlement(args)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(successToast).toBeCalledTimes(1);
  });

  it('Should run correctly with undefined entitlement', async () => {
    store = createStore(
      rootReducer,
      {
        configurationContactAccountEntitlement: {
          ...initialState.configurationContactAccountEntitlement,
          entitlements: undefined,
          activeTabEntitlement: {
            component: undefined
          }
        }
      },
      applyMiddleware(thunk)
    );
    const successToast = mockToast('addToast');
    mockCallApi(false);
    const args = { translateFunc: jest.fn() };
    await triggerUpdateContactAccEntitlement(args)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(successToast).toBeCalledTimes(1);
  });

  it('Should reject', async () => {
    window.appConfig = {} as AppConfiguration;
    const errorToast = mockToast('addToast');
    mockCallApi(true);
    const args = { translateFunc: jest.fn() };
    const response = await triggerUpdateContactAccEntitlement(args)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.payload).toBeUndefined();
    expect(errorToast).toBeCalledTimes(1);
  });
});

describe('updateContactAccEntitlementBuilder ', () => {
  it('should be fulfilled', () => {
    const store = createStore(rootReducer, { initialState: {} } as never);
    const state = store.getState();
    const payload = updateContactAccEntitlement.fulfilled(
      {
        listContactAccountCSPA: [
          {
            cspa: '8997-1234-1234-1234',
            components: undefined,
            description: 'description',
            active: true
          }
        ]
      },
      updateContactAccEntitlement.fulfilled.type,
      { translateFunc: (value: string) => value }
    );
    const actual = rootReducer(state, payload);
    expect(actual.configurationUserRoleEntitlement.entitlements).toEqual([]);
  });
});
