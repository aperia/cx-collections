import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';

// Const
import { ENTITLEMENT_COMPONENTS } from 'app/entitlements/constants';
import { MultipleLangDictionary } from 'app/entitlements/dictionary';
import { saveChangedClientConfig } from 'pages/ClientConfiguration/ChangeHistory/_redux/saveChangedClientConfig';

// Actions
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';

// Service
import contactAndAccountEntitlementService from '../contactAndAccountEntitlementService';

// Helper
import {
  convertContactEntitlementToSectionView,
  convertSectionViewToAccountStatusEntitlement
} from '../helpers';

// Type
import {
  ContactAccStatusEntitlementState,
  TriggerUpdateContactAccEntitlementArgs,
  UpdateContactAccEntitlementPayload
} from '../types';

export const updateContactAccEntitlement = createAsyncThunk<
  UpdateContactAccEntitlementPayload,
  TriggerUpdateContactAccEntitlementArgs,
  ThunkAPIConfig
>(
  'configurationViewEntitlement/updateContactAccEntitlement',
  async (args, thunkAPI) => {
    const { getState } = thunkAPI;

    const { configurationContactAccountEntitlement } = getState();
    const { activeTabEntitlement, selectedCSPA, entitlements } =
      configurationContactAccountEntitlement;

    const cspas = convertSectionViewToAccountStatusEntitlement(
      activeTabEntitlement,
      selectedCSPA,
      entitlements
    );
    const {
      user = '',
      app = '',
      org = '',
      privileges = []
    } = window.appConfig.commonConfig || {};

    const { data } =
      await contactAndAccountEntitlementService.updateContactAccEntitlement({
        common: {
          user,
          app,
          org,
          privileges
        },
        callerAccountEntitlements: {
          cspas
        }
      });
    const {
      configs: { callerAccountEntitlements }
    } = data;

    return {
      listContactAccountCSPA: callerAccountEntitlements.cspas || []
    };
  }
);

export const triggerUpdateContactAccEntitlement = createAsyncThunk<
  unknown,
  TriggerUpdateContactAccEntitlementArgs,
  ThunkAPIConfig
>(
  'configurationViewEntitlement/triggerUpdateContactAccEntitlement',
  async (args, thunkAPI) => {
    const { getState, dispatch } = thunkAPI;
    const { translateFunc } = args;

    const { configurationContactAccountEntitlement } = getState();
    const { activeTabEntitlement, entitlements } =
      configurationContactAccountEntitlement;
    const { component = '' } = activeTabEntitlement;

    const currentEntitlement = entitlements?.find(
      item => item.component === activeTabEntitlement?.component
    );

    const response = await dispatch(updateContactAccEntitlement(args));

    if (isFulfilled(response)) {
      batch(() => {
        dispatch(
          actionsToast.addToast({
            type: 'success',
            show: true,
            message: 'txt_update_config_entitlement_success',
            msgVariables: {
              section: translateFunc(
                MultipleLangDictionary.get(component as ENTITLEMENT_COMPONENTS)
              )
            }
          })
        );
        dispatch(
          saveChangedClientConfig({
            action: 'UPDATE',
            changedCategory: 'contactAccountStatusEntitlement',
            changedSection: {
              component:
                currentEntitlement?.component
                  ?.replace(/([A-Z])/g, ' $1')
                  .trim() || ''
            },
            changedItem: {
              entitlement:
                currentEntitlement?.entitlements?.[0].entitlementCode?.split(
                  '.'
                )[1]
            },
            oldValue: currentEntitlement?.entitlements?.[0].data,
            newValue: activeTabEntitlement.entitlements?.[0].data
          })
        );
      });
    }

    if (isRejected(response)) {
      dispatch(
        actionsToast.addToast({
          type: 'error',
          show: true,
          message: 'txt_update_config_entitlement_failed',
          msgVariables: {
            section: translateFunc(
              MultipleLangDictionary.get(component as ENTITLEMENT_COMPONENTS)
            )
          }
        })
      );
    }
  }
);

export const updateContactAccEntitlementBuilder = (
  builder: ActionReducerMapBuilder<ContactAccStatusEntitlementState>
) => {
  const { pending, fulfilled, rejected } = updateContactAccEntitlement;
  builder
    .addCase(pending, draftState => {
      return {
        ...draftState,
        isLoadingModal: true
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      const { selectedCSPA } = draftState;
      const { listContactAccountCSPA } = action.payload;
      const { translateFunc } = action.meta.arg;

      const currentCSPA = listContactAccountCSPA.find(
        item => item.cspa === selectedCSPA.cspaId
      );

      return {
        ...draftState,
        isLoadingModal: false,
        listContactAccountCSPA,
        entitlements: convertContactEntitlementToSectionView(
          currentCSPA?.components || [],
          translateFunc
        )
      };
    })
    .addCase(rejected, draftState => {
      return {
        ...draftState,
        isLoadingModal: false
      };
    });
};
