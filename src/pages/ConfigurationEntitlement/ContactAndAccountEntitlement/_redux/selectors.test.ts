import { listCriteria } from 'app/entitlements/constants';
import { selectorWrapper } from 'app/test-utils';
import cloneDeep from 'lodash.clonedeep';
import set from 'lodash.set';
import * as selectors from './selectors';

const initialState: Partial<RootState> = {
  configurationContactAccountEntitlement: {
    entitlements: [],
    isLoadingModal: false,
    isLoadingListCSPA: false,
    listContactAccountCSPA: [],
    selectedCSPA: {} as CSPA,
    textSearch: 'text search',
    isNoResultsFound: true,
    isErrorSettingEntitlement: false,
    isErrorLoadingListCSPA: false,
    isOpenViewElement: false,
    sortBy: { id: 'clientId', order: undefined },
    activeTabEntitlement: {
      entitlements: [
        {
          entitlementCode: 'code',
          data: [
            {
              criteriaCode: 'AccountInternalStatus'
            }
          ]
        },
        {
          entitlementCode: 'code 2'
        }
      ]
    },
    activeEntitlement: {
      description: 'des',
      value: 'code'
    },
    listActiveEntitlements: [],
    listCriteria,
    listCSPA: [
      {
        id: 'id',
        cspaId: 'cspaId',
        clientId: 'clientId',
        systemId: 'systemId',
        principleId: 'principleId',
        additionalFields: [{ name: 'isDefaultClientInfoKey', active: true }]
      }
    ],
    isToggle: false,
    isSelectedAll: false,
    isOpenAddCSPA: false,
    addCSPAErrorMsg: '',
    openStatusModal: false,
    isRetry: false,
    warningMessage: 'warningMessage'
  }
};

describe('Selector configurationContactAccountEntitlement', () => {
  it('getListCSPALoadingStatus', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.getListCSPALoadingStatus
    );
    expect(data).toBeFalsy();
  });

  it('getSelectedCSPA ', () => {
    const { data } = selectorWrapper(initialState)(selectors.getSelectedCSPA);
    expect(data).toEqual({});
  });

  it('getLoadingContactAccountEntitlementStatus ', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.getLoadingContactAccountEntitlementStatus
    );
    expect(data).toBeFalsy();
  });

  it('getStatusContactAccountEntitlement ', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.getStatusContactAccountEntitlement
    );
    expect(data).toEqual([]);
  });

  it('getActiveEntitlement ', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.getActiveEntitlement
    );
    expect(data).toEqual({
      description: 'des',
      value: 'code'
    });
  });

  it('getOpenSettingStatusContactAccountEntitlement ', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.getOpenSettingStatusContactAccountEntitlement
    );
    expect(data).toBeFalsy();
  });

  it('getListActiveEntitlements', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.getListActiveEntitlements
    );
    expect(data).toEqual([]);
  });

  it('getToggleStatus', () => {
    const { data } = selectorWrapper(initialState)(selectors.getToggleStatus);
    expect(data).toBeFalsy();
  });

  it('getListCriteria', () => {
    const { data } = selectorWrapper(initialState)(selectors.getListCriteria);
    expect(data).toEqual(listCriteria);
  });

  it('takeAddErrorMsg', () => {
    const { data } = selectorWrapper(initialState)(selectors.takeAddErrorMsg);
    expect(data).toEqual('');
  });

  it('takeListCSPALoadingStatus', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.takeListCSPALoadingStatus
    );
    expect(data).toBeFalsy();
  });

  it('takeOpenAddCSPAStatus', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.takeOpenAddCSPAStatus
    );
    expect(data).toBeFalsy();
  });

  it('takeSelectedAllStatus', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.takeSelectedAllStatus
    );
    expect(data).toBeFalsy();
  });

  it('takeNoResultsFoundStatus', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.takeNoResultsFoundStatus
    );
    expect(data).toBeTruthy();
  });

  it('takeToggleStatus', () => {
    const { data } = selectorWrapper(initialState)(selectors.takeToggleStatus);
    expect(data).toBeFalsy();
  });

  it('takeActiveEntitlement', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.takeActiveEntitlement
    );
    expect(data).toEqual({
      description: 'des',
      value: 'code'
    });
  });

  it('takeListActiveEntitlements ', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.takeListActiveEntitlements
    );
    expect(data).toEqual([]);
  });

  it('takeStatusContactAccountEntitlement', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.takeStatusContactAccountEntitlement
    );
    expect(data).toEqual([]);
  });

  it('selectTextSearch', () => {
    const { data } = selectorWrapper(initialState)(selectors.selectTextSearch);
    expect(data).toEqual('text search');
  });

  it('selectRootLoading', () => {
    const { data } = selectorWrapper(initialState)(selectors.selectRootLoading);
    expect(data).toBeFalsy();
  });

  it('takeActiveTabData', () => {
    const { data } = selectorWrapper(initialState)(selectors.takeActiveTabData);
    expect(data).toEqual({
      entitlements: [
        {
          entitlementCode: 'code',
          data: [
            {
              criteriaCode: 'AccountInternalStatus'
            }
          ]
        },
        { entitlementCode: 'code 2' }
      ]
    });
  });

  it('takeListCriteria', () => {
    const { data } = selectorWrapper(initialState)(selectors.takeListCriteria);
    expect(data).toEqual(listCriteria);
  });

  it('selectSort', () => {
    const { data } = selectorWrapper(initialState)(selectors.selectSort);
    expect(data).toEqual({ id: 'clientId', order: undefined });
  });

  it('selectIsNoData', () => {
    const newState = cloneDeep(initialState);
    set(newState, ['configurationContactAccountEntitlement', 'listCSPA'], null);
    const { data } = selectorWrapper(newState)(selectors.selectIsNoData);
    expect(data).toBeFalsy();
  });

  it('selectIsNoData with empty data', () => {
    const newState = cloneDeep(initialState);
    set(newState, ['configurationContactAccountEntitlement', 'listCSPA'], null);
    const { data } = selectorWrapper(newState)(selectors.selectIsNoData);
    expect(data).toBeFalsy();
  });

  it('selectDisplayClearAndReset', () => {
    const newState = cloneDeep(initialState);
    set(newState, ['configurationContactAccountEntitlement', 'listCSPA'], null);
    const { data } = selectorWrapper(newState)(
      selectors.selectDisplayClearAndReset
    );
    expect(data).toBeFalsy();
  });

  it('selectDisplayClearAndReset when has cspa', () => {
    const listCSPA = [
      {
        id: 'id',
        cspaId: 'cspaId',
        clientId: 'clientId',
        systemId: 'text search',
        principleId: 'text search',
        agentId: 'agentId',
        description: 'description',
        userId: 'userId'
      }
    ];
    const newState = cloneDeep(initialState);
    set(
      newState,
      ['configurationContactAccountEntitlement', 'listCSPA'],
      listCSPA
    );
    const { data } = selectorWrapper(newState)(
      selectors.selectDisplayClearAndReset
    );
    expect(data).toBeTruthy();
  });

  it('takeLoadingStatus ', () => {
    const { data } = selectorWrapper(initialState)(selectors.takeLoadingStatus);
    expect(data).toBeFalsy();
  });

  it('selectOpenViewElement', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.selectOpenViewElement
    );
    expect(data).toBeFalsy();
  });

  it('takeActiveTabEntitlementData ', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.takeActiveTabEntitlementData
    );
    expect(data).toEqual([]);
  });

  it('selectSelectedCSPA ', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.selectSelectedCSPA
    );
    expect(data).toEqual({});
  });

  it('getActiveTabEntitlementData ', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.getActiveTabEntitlementData
    );
    expect(data).toEqual([]);
  });

  it('selectIsSearchNoData ', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.selectIsSearchNoData
    );
    expect(data).toBeTruthy();
  });

  it('selectIsSearchNoData with empty data', () => {
    const newState = cloneDeep(initialState);
    set(newState, ['configurationContactAccountEntitlement', 'listCSPA'], null);
    const { data } = selectorWrapper(newState)(selectors.selectIsSearchNoData);
    expect(data).toBeTruthy();
  });

  it('selectCSPA ', () => {
    const { data } = selectorWrapper(initialState)(selectors.selectCSPA);
    expect(data).toEqual([]);
  });

  it('selectCSPA with empty data', () => {
    const newState = cloneDeep(initialState);
    set(newState, ['configurationContactAccountEntitlement', 'listCSPA'], null);
    const { data } = selectorWrapper(newState)(selectors.selectCSPA);
    expect(data).toEqual([]);
  });

  it('takeErrorLoadingListCSPA ', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.takeErrorLoadingListCSPA
    );
    expect(data).toBeFalsy();
  });

  it('getActiveTabEntitlementData ', () => {
    const { data } = selectorWrapper(initialState)((rootState: RootState) =>
      selectors.getActiveTabEntitlementData(rootState, 'AccountInternalStatus')
    );
    expect(data).toEqual([]);
  });

  it('selectOpenStatusModal', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.selectOpenStatusModal
    );
    expect(data).toBeFalsy();
  });

  it('selectDefaultSettingsClientConfig', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.selectDefaultSettingsClientConfig
    );
    expect(data).toBeFalsy();
  });

  it('takeWarningMessage', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.takeWarningMessage
    );
    expect(data).toEqual('warningMessage');
  });

  it('takeIsRetry', () => {
    const { data } = selectorWrapper(initialState)(
      selectors.takeIsRetry
    );
    expect(data).toBeFalsy();
  });

});
