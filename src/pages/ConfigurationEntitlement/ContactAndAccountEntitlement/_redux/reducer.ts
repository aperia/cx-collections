import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { SortType } from 'app/_libraries/_dls/components';

// Const
import { listCriteria } from 'app/entitlements/constants';

// Type
import {
  ContactAccountConfig,
  ContactAccStatusEntitlementState,
  OnChangeActiveTabArgs,
  OnChangeCheckboxArgs
} from '../types';

// Helper
import {
  buildEntitlementCheckedAllData,
  buildEntitlementCheckedData,
  detectAllDataChecked
} from '../ViewEntitlement/helpers';
import { buildListActiveEntitlements } from '../helpers';

// Actions
import {
  getContactAccEntitlement,
  getContactAccEntitlementBuilder
} from './getContactAccEntitlement';
import {
  getContactAccListCSPA,
  getContactAccListCSPABuilder
} from './getContactAccListCSPA';
import {
  triggerUpdateContactAccEntitlement,
  updateContactAccEntitlement,
  updateContactAccEntitlementBuilder
} from './updateContactAccEntitlement';
import {
  changeStatusCSPABuilder,
  triggerChangeStatusCSPA
} from './triggerChangeStatus';
import { addCSPABuilder, triggerAddCSPA } from './addCSPA';

export const initialState: ContactAccStatusEntitlementState = {
  entitlements: [],
  openStatusModal: false,
  isLoadingModal: false,
  isLoadingListCSPA: false,
  listContactAccountCSPA: [],
  selectedCSPA: {} as CSPA,
  textSearch: '',
  isNoResultsFound: true,
  isErrorSettingEntitlement: false,
  isErrorLoadingListCSPA: false,
  isOpenViewElement: false,
  sortBy: { id: 'status', order: undefined },
  activeTabEntitlement: {},
  listCriteria,
  activeEntitlement: {},
  listActiveEntitlements: [],
  listCSPA: [],
  isToggle: false,
  isSelectedAll: false,
  isOpenAddCSPA: false,
  addCSPAErrorMsg: '',
  isRetry: false,
  warningMessage: ''
};

const { actions, reducer } = createSlice({
  name: 'configurationContactAndAccountEntitlement',
  initialState,
  reducers: {
    onChangeTextSearch: (draftState, action: PayloadAction<string>) => {
      return {
        ...draftState,
        textSearch: action.payload
      };
    },
    setSelectedCSPA: (draftState, action: PayloadAction<CSPA>) => {
      draftState.selectedCSPA = action.payload;
    },
    openStatusModal: draftState => {
      draftState.openStatusModal = true;
    },
    closeStatusModal: draftState => {
      draftState.openStatusModal = false;
    },
    onChangeViewEntitlement: (draftState, action: PayloadAction<CSPA>) => {
      return {
        ...draftState,
        selectedCSPA: action.payload
      };
    },
    onChangeSort: (draftState, action: PayloadAction<SortType>) => {
      return {
        ...draftState,
        sortBy: action.payload
      };
    },
    onRemoveTab: () => {
      return { ...initialState };
    },
    setAddClientConfigErrorMessage: (
      draftState,
      action: PayloadAction<MagicKeyValue>
    ) => {
      const { error, isRetry } = action.payload;
      return {
        ...draftState,
        addCSPAErrorMsg: error ?? '',
        isRetry
      };
    },
    setConfigWarningMessage: (draftState, action: PayloadAction<string>) => {
      draftState.warningMessage = action.payload;
    },
    onOpenModalAddCSPA: draftState => {
      return {
        ...draftState,
        isOpenAddCSPA: true
      };
    },
    onCloseModalAddCSPA: draftState => {
      return {
        ...draftState,
        isOpenAddCSPA: false
      };
    },
    onResetTab: draftState => {
      return {
        ...draftState,
        entitlements: [],
        activeTabEntitlement: {},
        activeEntitlement: {},
        listActiveEntitlements: [],
        isSelectedAll: false
      };
    },
    onChangeOpenViewElement: draftState => {
      return {
        ...draftState,
        isOpenViewElement: !draftState.isOpenViewElement
      };
    },
    onChangeToggle: draftState => {
      return {
        ...draftState,
        isToggle: !draftState.isToggle
      };
    },
    onDiscardChange: draftState => {
      const { entitlements, activeTabEntitlement, activeEntitlement } =
        draftState;

      const discardEntitlementData =
        entitlements.find(
          item => item.component === activeTabEntitlement.component
        ) || {};

      const isSelectedAll = detectAllDataChecked(
        discardEntitlementData.entitlements || [],
        activeEntitlement
      );

      return {
        ...draftState,
        activeTabEntitlement: discardEntitlementData,
        isSelectedAll
      };
    },
    onChangePrevious: (
      draftState,
      action: PayloadAction<ContactAccountConfig>
    ) => {
      const { activeEntitlement } = draftState;

      const isSelectedAll = detectAllDataChecked(
        action.payload?.entitlements || [],
        activeEntitlement
      );

      return {
        ...draftState,
        activeTabEntitlement: action.payload,
        isSelectedAll
      };
    },
    onChangeSelectAll: draftState => {
      const { activeTabEntitlement, activeEntitlement, isSelectedAll } =
        draftState;

      const entitlements = buildEntitlementCheckedAllData(
        activeTabEntitlement.entitlements || [],
        activeEntitlement,
        !isSelectedAll
      );

      return {
        ...draftState,
        activeTabEntitlement: {
          ...draftState?.activeTabEntitlement,
          entitlements
        },
        isSelectedAll: !isSelectedAll
      };
    },
    onChangeCheckbox: (
      draftState,
      action: PayloadAction<OnChangeCheckboxArgs>
    ) => {
      const { checkedValue, typeCode } = action.payload;

      const { activeTabEntitlement, activeEntitlement } = draftState;

      const entitlements = buildEntitlementCheckedData(
        activeTabEntitlement.entitlements || [],
        activeEntitlement,
        checkedValue
      )(typeCode);

      const isSelectedAll = detectAllDataChecked(
        entitlements,
        activeEntitlement
      );

      return {
        ...draftState,
        activeTabEntitlement: {
          ...draftState?.activeTabEntitlement,
          entitlements
        },
        isSelectedAll
      };
    },
    onChangeEntitlement: (draftState, action: PayloadAction<RefDataValue>) => {
      const { listActiveEntitlements, activeTabEntitlement } = draftState;

      const activeEntitlement = listActiveEntitlements.find(
        item => item.value === action.payload.value
      );

      const isSelectedAll = detectAllDataChecked(
        activeTabEntitlement.entitlements || [],
        activeEntitlement || {}
      );

      return {
        ...draftState,
        activeEntitlement: activeEntitlement || {},
        isSelectedAll
      };
    },
    onChangeActiveTab: (
      draftState,
      action: PayloadAction<OnChangeActiveTabArgs>
    ) => {
      const { component, translateFn } = action.payload;

      const newEntitlementTab = draftState.entitlements.find(
        item => item.component === component
      );

      const listActiveEntitlements = buildListActiveEntitlements(
        newEntitlementTab?.entitlements || [],
        translateFn
      );
      const [activeEntitlement] = listActiveEntitlements;

      const isSelectedAll = detectAllDataChecked(
        newEntitlementTab?.entitlements || [],
        activeEntitlement || {}
      );

      return {
        ...draftState,
        activeTabEntitlement: newEntitlementTab || {},
        activeEntitlement: activeEntitlement,
        listActiveEntitlements,
        isSelectedAll
      };
    }
  },
  extraReducers: builder => {
    getContactAccEntitlementBuilder(builder);
    getContactAccListCSPABuilder(builder);
    updateContactAccEntitlementBuilder(builder);
    changeStatusCSPABuilder(builder);
    addCSPABuilder(builder);
  }
});

const allActions = {
  ...actions,
  getContactAccEntitlement,
  getContactAccListCSPA,
  updateContactAccEntitlement,
  triggerUpdateContactAccEntitlement,
  triggerChangeStatusCSPA,
  triggerAddCSPA
};

export {
  allActions as configurationContactAccountEntitlementActions,
  reducer as configurationContactAccountEntitlement
};
