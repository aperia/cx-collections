import React from 'react';
import { fireEvent } from '@testing-library/dom';

import { mockActionCreator, renderMockStoreId } from 'app/test-utils';

import SearchConfig from './Search';

import { configurationContactAccountEntitlementActions } from './_redux/reducer';
import { screen } from '@testing-library/react';
import { DEFAULT_CLIENT_CONFIG_KEY } from 'pages/ClientConfiguration/constants';

const initialState: Partial<RootState> = {
  configurationContactAccountEntitlement: {
    textSearch: '123',
    listCSPA: [
      {
        id: '1',
        cspaId: 'cspaId',
        clientId: 'clientId',
        systemId: 'systemId',
        principleId: 'principleId',
        agentId: 'agentId',
        description: 'description',
        userId: 'userId',
        additionalFields: [
          {
            name: DEFAULT_CLIENT_CONFIG_KEY,
            active: false,
            value: 'true'
          }
        ]
      }
    ]
  }
};

const spyActions = mockActionCreator(
  configurationContactAccountEntitlementActions
);

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(
    <SearchConfig
      onClearAndReset={jest.fn()}
      currentPage={1}
      onPageChange={jest.fn()}
    />,
    { initialState }
  );
};

describe('Test SearchConfig', () => {
  it('Render UI', () => {
    renderWrapper(initialState);
    expect(
      screen.queryByPlaceholderText('txt_type_to_search')
    ).toBeInTheDocument();
  });

  it('search text', () => {
    const action = spyActions('onChangeTextSearch');
    const { wrapper } = renderWrapper(initialState);

    const input = wrapper.container.querySelector('input')!;
    const button = wrapper.container.querySelector('.dls-text-search-search')!;
    fireEvent.change(input, { target: { value: '456' } });
    fireEvent.click(button);

    expect(action).toHaveBeenCalled();
  });

  it('handleOnAddCSPA ', () => {
    const action = spyActions('onOpenModalAddCSPA');
    const { wrapper } = renderWrapper(initialState);
    const button = wrapper.getByText('txt_add_cspa')!;
    button.click();

    expect(action).toHaveBeenCalled();
  });
});
