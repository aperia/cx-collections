import { apiService } from 'app/utils/api.service';

// Types
import {
  UpdateContactAccEntitlementRequest,
  GetContactAccEntitlementRequest,
  GetContactAccEntitlementResponse,
  GetContactAccListCSPARequest,
  GetContactAccEntitlementListResponse
} from './types';

const contactAndAccountEntitlementService = {
  getContactAccListCSPA(data: GetContactAccListCSPARequest) {
    const url =
      window.appConfig?.api?.contactAndAccountEntitlement
        ?.getContactAccListCSPA || '';
    return apiService.post<GetContactAccEntitlementListResponse>(url, data);
  },
  getContactAccEntitlement(data: GetContactAccEntitlementRequest) {
    const url =
      window.appConfig?.api?.contactAndAccountEntitlement
        ?.getContactAccEntitlement || '';
    return apiService.post<GetContactAccEntitlementResponse>(url, data);
  },
  updateContactAccEntitlement(data: UpdateContactAccEntitlementRequest) {
    const url =
      window.appConfig?.api?.contactAndAccountEntitlement
        ?.updateContactAccEntitlement || '';
    return apiService.post<GetContactAccEntitlementResponse>(url, data);
  }
};

export default contactAndAccountEntitlementService;
