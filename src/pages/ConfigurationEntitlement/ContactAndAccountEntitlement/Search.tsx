import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { isUndefined } from 'lodash';

// Hooks
import { useStoreIdSelector } from 'app/hooks';

// Redux
import {
  selectDefaultSettingsClientConfig,
  selectDisplayClearAndReset,
  selectTextSearch
} from './_redux/selectors';
import { configurationContactAccountEntitlementActions } from './_redux/reducer';

// Component
import Search from '../Components/Search';
import AddCSPA from '../Components/AddCSPA';

interface ISearchConfig {
  onPageChange: (number: number) => void;
  onClearAndReset: () => void;
  currentPage: number;
}

const SearchConfig: React.FC<ISearchConfig> = ({
  onPageChange,
  currentPage,
  onClearAndReset
}) => {
  const dispatch = useDispatch();

  const text = useStoreIdSelector<string>(selectTextSearch);
  const isDisplayClearAndReset = useSelector(selectDisplayClearAndReset);
  const defaultClientConfig = useStoreIdSelector<CSPA>(
    selectDefaultSettingsClientConfig
  );
  const isNotHaveDefaultConfig = isUndefined(defaultClientConfig);

  const handleSearch = (value: string) => {
    dispatch(
      configurationContactAccountEntitlementActions.onChangeTextSearch(value)
    );
  };

  const handleOnAddCSPA = () => {
    dispatch(
      configurationContactAccountEntitlementActions.onOpenModalAddCSPA()
    );
  };

  return (
    <Search
      onSearch={handleSearch}
      currentPage={currentPage}
      onPageChange={onPageChange}
      textSearch={text}
      onClearAndReset={onClearAndReset}
      isDisplayClearAndReset={isDisplayClearAndReset}
    >
      <AddCSPA
        className="mt-16"
        onAddCSPA={handleOnAddCSPA}
        disabled={isNotHaveDefaultConfig}
      />
    </Search>
  );
};

export default SearchConfig;
