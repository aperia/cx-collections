import * as helpers from './helpers';

// Constants
import {
  COMPONENTS_MAP,
  CRITERIA_NAME,
  ENTITLEMENT_COMPONENTS
} from 'app/entitlements/constants';
import {
  ContactAccountComponentEntitlement,
  ContactAccountConfig
} from './types';

const mockTranslate = (text: string) => text;

jest.mock('app/entitlements/MasterStructure/masterComponentStruture', () => {
  const { ENTITLEMENT_COMPONENTS, PERMISSIONS } = jest.requireActual(
    'app/entitlements/constants'
  );

  return [
    {
      component: null,
      entitlements: { items: [] }
    },
    {
      component: ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH,
      entitlements: {
        items: [
          {
            entitlementCode: 'AccountSearch.EditExternalStatusCode'
          }
        ]
      }
    },
    {
      component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION,
      entitlements: {
        items: [
          {
            entitlementCode: PERMISSIONS.ACCOUNT_INFORMATION_VIEW
          }
        ]
      }
    }
  ];
});

jest.mock('app/entitlements/dictionary', () => {
  const dictionaries = jest.requireActual('app/entitlements/dictionary');
  const { COMPONENTS_MAP, ENTITLEMENT_COMPONENTS } = jest.requireActual(
    'app/entitlements/constants'
  );

  const AppComponentDictionary = new Map([
    [COMPONENTS_MAP.ACCOUNT_SEARCH, ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH]
  ]);

  return {
    ...dictionaries,
    AppComponentDictionary
  };
});

jest.mock(
  'app/entitlements/MasterStructure/masterAccountStatusStructure',
  () => {
    return [
      {
        criteriaCode: 'AccountInternalStatus',
        items: [
          { value: 'D', description: 'Delinquent', active: false },
          { value: 'O', description: 'Overlimit', active: false },
          {
            value: 'X',
            description: 'Overlimit and Delinquent',
            active: false
          },
          { value: ' ', description: 'Normal', active: false }
        ]
      },
      {
        criteriaCode: 'AccountExternalStatus',
        items: []
      }
    ];
  }
);

jest.mock('../constants', () => {
  const constants = jest.requireActual('../constants');

  return {
    ...constants,
    UI_CRITERIA: {
      AccountInternalStatus: [
        {
          value: 'D'
        },
        {
          value: 'O'
        }
      ]
    }
  };
});

describe('Test helpers', () => {
  it('convertContactEntitlementToSectionView', () => {
    const contactAccountCSPAs: Array<ContactAccountComponentEntitlement> = [
      {
        component: COMPONENTS_MAP.ACCOUNT_SEARCH,
        jsonValue: {
          entitlementCode: {
            internalStatusCode: {
              AccountInternalStatus: true
            }
          },
          editExternalStatusCode: {
            internalStatusCode: {
              AccountInternalStatus: true
            }
          }
        }
      },
      {}
    ];

    const expected = [
      { component: null, entitlements: [] },
      {
        component: 'AccountSearch',
        entitlements: [
          {
            entitlementCode: 'AccountSearch.EditExternalStatusCode',
            data: [
              {
                criteriaCode: 'AccountInternalStatus',
                items: [
                  { value: 'D', description: 'Delinquent', active: false },
                  { value: 'O', description: 'Overlimit', active: false },
                  {
                    value: 'X',
                    description: 'Overlimit and Delinquent',
                    active: false
                  },
                  { value: ' ', description: 'Normal', active: false }
                ]
              },
              {
                criteriaCode: 'AccountExternalStatus',
                items: []
              }
            ]
          }
        ]
      },
      {
        component: 'AccountInformation',
        entitlements: [
          {
            entitlementCode: 'AccountInformation.View',
            data: [
              {
                criteriaCode: 'AccountInternalStatus',
                items: [
                  { value: 'D', description: 'Delinquent', active: false },
                  { value: 'O', description: 'Overlimit', active: false },
                  {
                    value: 'X',
                    description: 'Overlimit and Delinquent',
                    active: false
                  },
                  { value: ' ', description: 'Normal', active: false }
                ]
              },
              {
                criteriaCode: 'AccountExternalStatus',
                items: []
              }
            ]
          }
        ]
      }
    ];

    expect(
      helpers.convertContactEntitlementToSectionView(
        contactAccountCSPAs,
        mockTranslate
      )
    ).toEqual(expected);
  });

  it('buildContactAccountMasterStructure', () => {
    const expected = [
      { component: null, entitlements: [] },
      {
        component: 'AccountSearch',
        entitlements: [
          {
            entitlementCode: 'AccountSearch.EditExternalStatusCode',
            data: [
              {
                criteriaCode: 'AccountInternalStatus',
                items: [
                  { value: 'D', description: 'Delinquent', active: false },
                  { value: 'O', description: 'Overlimit', active: false },
                  {
                    value: 'X',
                    description: 'Overlimit and Delinquent',
                    active: false
                  },
                  { value: ' ', description: 'Normal', active: false }
                ]
              },
              {
                criteriaCode: 'AccountExternalStatus',
                items: []
              }
            ]
          }
        ]
      },
      {
        component: 'AccountInformation',
        entitlements: [
          {
            entitlementCode: 'AccountInformation.View',
            data: [
              {
                criteriaCode: 'AccountInternalStatus',
                items: [
                  { value: 'D', description: 'Delinquent', active: false },
                  { value: 'O', description: 'Overlimit', active: false },
                  {
                    value: 'X',
                    description: 'Overlimit and Delinquent',
                    active: false
                  },
                  { value: ' ', description: 'Normal', active: false }
                ]
              },
              {
                criteriaCode: 'AccountExternalStatus',
                items: []
              }
            ]
          }
        ]
      }
    ];

    expect(helpers.buildContactAccountMasterStructure(mockTranslate)).toEqual(
      expected
    );
  });

  describe('buildListActiveEntitlements', () => {
    it('when entitlements not empty', () => {
      const translateFn = jest.fn().mockImplementation((arg: any) => arg);

      const entitlements = [
        {
          entitlementCode: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION
        }
      ];

      const expected = [
        {
          value: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION,
          description: 'txt_entitlement_account_information'
        }
      ];

      expect(
        helpers.buildListActiveEntitlements(entitlements, translateFn)
      ).toEqual(expected);
    });

    it('when entitlements is empty', () => {
      const translateFn = jest.fn().mockImplementation((arg: any) => arg);

      const expected = [] as Array<RefDataValue>;

      expect(
        helpers.buildListActiveEntitlements(undefined, translateFn)
      ).toEqual(expected);
    });
  });

  describe('convertSectionViewToAccountStatusEntitlement', () => {
    it('with activeTabEntitlement matches componentEntitlements', () => {
      const activeTabEntitlement: ContactAccountConfig = {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION,
        entitlements: [
          {
            entitlementCode: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION,
            data: [
              {
                criteriaCode: CRITERIA_NAME.INTERNAL_STATUS_CODE,
                items: [
                  {
                    value: 'value',
                    active: true
                  },
                  // should cover default value
                  {}
                ]
              },
              {
                criteriaCode: 'CriteriaCodeAPIDictionaryShouldReturnNull'
              },
              {
                criteriaCode: CRITERIA_NAME.EXTERNAL_STATUS_CODE,
                // should cover default value
                items: undefined
              }
            ]
          },
          {}
        ]
      };

      const selectedCSPA: CSPA = {
        cspaId: 'cspaId'
      };

      const componentEntitlements: Array<ContactAccountConfig> = [
        activeTabEntitlement
      ];

      const expected = [
        {
          cspa: 'cspaId',
          components: [
            { component: 'account-information', jsonValue: { '': {} } }
          ]
        }
      ];

      expect(
        helpers.convertSectionViewToAccountStatusEntitlement(
          activeTabEntitlement,
          selectedCSPA,
          componentEntitlements
        )
      ).toEqual(expected);
    });

    it('with activeTabEntitlement not matching componentEntitlements', () => {
      const activeTabEntitlement: ContactAccountConfig = {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH,
        entitlements: [
          {
            entitlementCode: ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH
          }
        ]
      };

      const selectedCSPA: CSPA = {
        cspaId: 'cspaId'
      };

      const componentEntitlements: Array<ContactAccountConfig> = [
        {
          component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION,
          entitlements: [
            {
              entitlementCode: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION
            }
          ]
        }
      ];

      const expected = [
        {
          cspa: 'cspaId',
          components: [],
          active: undefined
        }
      ];

      expect(
        helpers.convertSectionViewToAccountStatusEntitlement(
          activeTabEntitlement,
          selectedCSPA,
          componentEntitlements
        )
      ).toEqual(expected);
    });

    it('with empty activeTabEntitlemen entitlements', () => {
      const activeTabEntitlement: ContactAccountConfig = {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH
      };

      const selectedCSPA: CSPA = {
        cspaId: 'cspaId'
      };

      const componentEntitlements: Array<ContactAccountConfig> = [
        {
          component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION,
          entitlements: [
            {
              entitlementCode: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION
            }
          ]
        }
      ];

      const expected = [
        {
          cspa: 'cspaId',
          components: [],
          description: undefined
        }
      ];

      expect(
        helpers.convertSectionViewToAccountStatusEntitlement(
          activeTabEntitlement,
          selectedCSPA,
          componentEntitlements
        )
      ).toEqual(expected);
    });

    it('with empty componentEntitlements entitlements', () => {
      const activeTabEntitlement: ContactAccountConfig = {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH
      };

      const selectedCSPA: CSPA = {
        cspaId: 'cspaId'
      };

      const componentEntitlements: Array<ContactAccountConfig> = [
        {
          component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION
        }
      ];

      const expected = [
        {
          cspa: 'cspaId',
          components: [],
          description: undefined
        }
      ];

      expect(
        helpers.convertSectionViewToAccountStatusEntitlement(
          activeTabEntitlement,
          selectedCSPA,
          componentEntitlements
        )
      ).toEqual(expected);
    });
  });
});
