import React from 'react';

// Hooks
import { useStoreIdSelector } from 'app/hooks';
import { useDispatch } from 'react-redux';

// Redux
import {
  selectSelectedCSPA,
  selectRootLoading,
  selectOpenStatusModal
} from './_redux/selectors';
import { configurationContactAccountEntitlementActions } from './_redux/reducer';

// Component
import ActiveDeActiveModal from '../Components/ActiveDeActiveModal';

// Const
import { CLIENT_CONFIG_STATUS_KEY } from 'pages/ClientConfiguration/constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';

const StatusModal = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const isOpenModal = useStoreIdSelector<boolean>(selectOpenStatusModal);

  const isLoading = useStoreIdSelector<boolean>(selectRootLoading);

  const selectedConfig = useStoreIdSelector<CSPA>(selectSelectedCSPA);

  const handleCloseModal = () => {
    dispatch(configurationContactAccountEntitlementActions.closeStatusModal());
  };

  const handleSubmit = () => {
    dispatch(
      configurationContactAccountEntitlementActions.triggerChangeStatusCSPA({
        id: selectedConfig.id!,
        additionalFields: selectedConfig.additionalFields,
        translateFn: t
      })
    );
  };

  const isConfigActive = selectedConfig?.additionalFields?.find(
    (item: AdditionalFieldsMapType) => item.name === CLIENT_CONFIG_STATUS_KEY
  );

  const isActive = !!isConfigActive?.active;

  return (
    <ActiveDeActiveModal
      isLoading={isLoading}
      isOpenModal={isOpenModal}
      onCloseModal={handleCloseModal}
      onSubmit={handleSubmit}
      selectedConfig={selectedConfig}
      isActive={isActive}
    />
  );
};

export default StatusModal;
