import React from 'react';
import { Button } from 'app/_libraries/_dls/components';
import { batch, useDispatch } from 'react-redux';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Const
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { configurationContactAccountEntitlementActions } from './_redux/reducer';
import { CLIENT_CONFIG_STATUS_KEY } from 'pages/ClientConfiguration/constants';
import { isDefaultClientConfig } from 'pages/ClientConfiguration/helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface ActionsProps {
  data: CSPA;
  dataTestId?: string;
}

const Actions: React.FC<ActionsProps> = ({ data, dataTestId }) => {
  //query check client config status with "isConfigActive" key
  const isConfigActive = data?.additionalFields?.find(
    (item: AdditionalFieldsMapType) => item.name === CLIENT_CONFIG_STATUS_KEY
  );

  const isActive = isConfigActive?.active === true;

  const dispatch = useDispatch();
  const { t } = useTranslation();

  const handleChangeStatus = () => {
    batch(() => {
      dispatch(
        configurationContactAccountEntitlementActions.setSelectedCSPA(data)
      );
      dispatch(configurationContactAccountEntitlementActions.openStatusModal());
    });
  };

  const handleViewSetting = () => {
    batch(() => {
      dispatch(
        configurationContactAccountEntitlementActions.setConfigWarningMessage(
          ''
        )
      );
      dispatch(
        configurationContactAccountEntitlementActions.onChangeOpenViewElement()
      );
      dispatch(
        configurationContactAccountEntitlementActions.onChangeViewEntitlement(
          data
        )
      );
    });
  };

  const isModified = !isDefaultClientConfig(data);

  return (
    <>
      {isModified && (
        <Button
          variant={isActive ? 'outline-danger' : 'outline-primary'}
          size="sm"
          onClick={handleChangeStatus}
          dataTestId={genAmtId(dataTestId, 'change-status', '')}
        >
          {isActive
            ? t(I18N_COMMON_TEXT.DEACTIVATE)
            : t(I18N_COMMON_TEXT.ACTIVATE)}
        </Button>
      )}
      <Button
        variant="outline-primary"
        size="sm"
        onClick={handleViewSetting}
        dataTestId={genAmtId(dataTestId, 'view-entitlement', '')}
      >
        {t('txt_view_entitlement')}
      </Button>
    </>
  );
};

export default Actions;
