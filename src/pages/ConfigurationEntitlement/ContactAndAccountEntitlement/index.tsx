import React, { useEffect } from 'react';
import classNames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { isUndefined } from 'lodash';

// components
import GridClientConfig from './Grid';
import ViewEntitlementWrapper from './ViewEntitlement';
import { SimpleBar } from 'app/_libraries/_dls/components';
import NoData from '../Components/NoData';
import { FailedApiReload } from 'app/components';
import AddCSPA from '../Components/AddCSPA';
import AddCSPAConfig from './AddCSPAConfig';
import StatusModal from './StatusModal';

// i18
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';

// redux
import { configurationContactAccountEntitlementActions } from './_redux/reducer';
import {
  selectDefaultSettingsClientConfig,
  takeErrorLoadingListCSPA,
  takeListCSPALoadingStatus,
  takeNoResultsFoundStatus
} from './_redux/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { selectCSPAFromAccessConfig } from 'pages/__commons/AccessConfig/_redux/selector';

const ContactAndAccountEntitlement: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const loading = useSelector<RootState, boolean>(takeListCSPALoadingStatus);
  const isError = useSelector<RootState, boolean>(takeErrorLoadingListCSPA);
  const isNoData: boolean = useSelector(takeNoResultsFoundStatus);
  const defaultClientConfig = useSelector(selectDefaultSettingsClientConfig);
  const accessConfig = useSelector(selectCSPAFromAccessConfig);

  const isNotHaveDefaultConfig = isUndefined(defaultClientConfig);
  const testId = 'contactAndAccountEntitlement';

  useEffect(() => {
    if (!accessConfig.clientNumber) return;
    dispatch(
      configurationContactAccountEntitlementActions.getContactAccListCSPA()
    );
    return () => {
      dispatch(configurationContactAccountEntitlementActions.onRemoveTab());
    };
  }, [accessConfig.clientNumber, dispatch]);

  const handleOnAddCSPA = () => {
    dispatch(
      configurationContactAccountEntitlementActions.onOpenModalAddCSPA()
    );
  };

  const renderLayout = () => {
    if (isError)
      return (
        <FailedApiReload
          id="client-config-list-failed"
          className="mt-80"
          onReload={() =>
            dispatch(
              configurationContactAccountEntitlementActions.getContactAccListCSPA()
            )
          }
          dataTestId={genAmtId(testId, 'failed-api', '')}
        />
      );

    if (isNoData && !loading) {
      return (
        <NoData>
          <AddCSPA
            onAddCSPA={handleOnAddCSPA}
            disabled={isNotHaveDefaultConfig}
          />
        </NoData>
      );
    }

    return <GridClientConfig />;
  };

  return (
    <div className="h-100 overflow-auto">
      <SimpleBar className={classNames({ loading })}>
        <div className="p-24 max-width-lg mx-auto">
          <h3 data-testid={genAmtId(testId, 'title', '')}>
            {t('txt_contact_and_account_status_entitlement')}
          </h3>
          {renderLayout()}
        </div>
      </SimpleBar>
      <ViewEntitlementWrapper />
      <StatusModal />
      <AddCSPAConfig />
    </div>
  );
};

export default ContactAndAccountEntitlement;
