import { SortType } from 'app/_libraries/_dls/components';

// Const
import {
  COMPONENTS_MAP,
  CRITERIA_NAME,
  ENTITLEMENT_COMPONENTS
} from 'app/entitlements/constants';
import { SECURE_CONFIG_KY } from '../constants';

export interface ContactAccStatusEntitlementState {
  entitlements: Array<ContactAccountConfig>;
  openStatusModal: boolean;
  listContactAccountCSPA: Array<ContactAccountCSPA>;
  isLoadingModal: boolean;
  isLoadingListCSPA: boolean;
  isNoResultsFound: boolean;
  isOpenViewElement: boolean;
  selectedCSPA: CSPA;
  textSearch: string;
  isErrorSettingEntitlement: boolean;
  isErrorLoadingListCSPA: boolean;
  sortBy: SortType;
  activeTabEntitlement: ContactAccountConfig;
  listCriteria: Array<CRITERIA_NAME>;
  activeEntitlement: Partial<RefDataValue>;
  listActiveEntitlements: Array<Partial<RefDataValue>>;
  listCSPA: Array<CSPA>;
  isToggle: boolean;
  isSelectedAll: boolean;
  isOpenAddCSPA: boolean;
  addCSPAErrorMsg: string;
  isRetry?: boolean;
  warningMessage?: string;
}

export interface GetContactAccEntitlementPayload {
  entitlements: Array<ContactAccountConfig>;
  listContactAccountCSPA: Array<ContactAccountCSPA>;
}

export interface GetContactAccListCSPAPayload {
  cspas: Array<CSPA>;
}

export interface GetContactAccEntitlementArgs {
  translateFn: Function;
}

export interface ContactAccountJsonValue {
  [criteriaCode: string]: Record<string, boolean>;
}
export interface ContactAccountEntitlement {
  [entitlementCode: string]: ContactAccountJsonValue;
}
export interface ContactAccountComponentEntitlement {
  component?: string;
  jsonValue?: ContactAccountEntitlement;
}

export interface ContactAccountCSPA {
  cspa?: string;
  components?: Array<ContactAccountComponentEntitlement>;
  description?: string;
  active?: boolean;
}
export interface GetContactAccEntitlementResponse {
  configs: {
    callerAccountEntitlements: {
      cspas?: Array<ContactAccountCSPA>;
    };
  };
}

export interface TriggerUpdateContactAccEntitlementArgs {
  translateFunc: Function;
}

export interface UpdateContactAccEntitlementPayload {
  listContactAccountCSPA: Array<ContactAccountCSPA>;
}

export interface UpdateContactAccEntitlementRequest extends CommonRequest {
  callerAccountEntitlements: {
    cspas?: Array<ContactAccountCSPA>;
  };
}
export interface GetContactAccEntitlementRequest extends CommonRequest {
  selectConfig: SECURE_CONFIG_KY;
  selectCspa: string;
  selectComponent?: COMPONENTS_MAP;
}

export interface GetContactAccListCSPARequest extends CommonRequest {
  fetchSize: string;
  orgId?: string;
}

export interface GetContactAccEntitlementListResponse {
  clientInfoList: Array<CSPA>;
}
export interface OnChangeActiveTabArgs {
  translateFn: Function;
  component: string;
}

export interface OnChangeCheckboxArgs {
  checkedValue: string;
  typeCode: string;
}

/**
 * Contact Account status modal mapping
 */
export interface ContactAccountDataConfig {
  value?: string;
  active?: boolean;
  description?: string;
}
export interface CriteriaConfig {
  criteriaCode?: string;
  items?: Array<ContactAccountDataConfig>;
}
export interface ContactAccountEntitlementConfig {
  entitlementCode?: string;
  data?: Array<CriteriaConfig>;
}
export interface ContactAccountConfig {
  component?: ENTITLEMENT_COMPONENTS;
  entitlements?: Array<ContactAccountEntitlementConfig>;
}
