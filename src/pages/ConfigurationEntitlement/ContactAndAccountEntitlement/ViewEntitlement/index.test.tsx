import React from 'react';
import ViewEntitlementWrapper from './index';
import { renderMockStore } from 'app/test-utils/renderComponentWithMockStore';
import { screen } from '@testing-library/react';
import cloneDeep from 'lodash.clonedeep';
import set from 'lodash.set';

jest.mock('./Settings/index', () => () => {
  return <div>ViewElementSetting</div>;
});

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(<ViewEntitlementWrapper />, { initialState });
};

const initialState: Partial<RootState> = {
  configurationContactAccountEntitlement: {
    listActiveEntitlements: [
      {
        description: 'activeEntitlement',
        value: 'activeEntitlement'
      },
      {
        description: 'inActiveEntitlement',
        value: 'inActiveEntitlement'
      }
    ],

    entitlements: [{}],
    listContactAccountCSPA: [{}],
    isLoadingModal: false,
    isLoadingListCSPA: false,
    isNoResultsFound: false,
    isOpenViewElement: true,
    selectedCSPA: {},
    textSearch: 'textSearch',
    isErrorSettingEntitlement: false,
    isErrorLoadingListCSPA: false,
    sortBy: {
      id: 'id',
      order: 'desc'
    },
    activeTabEntitlement: {},
    listCriteria: [],
    activeEntitlement: {
      description: 'activeEntitlement',
      value: 'activeEntitlement'
    },
    listCSPA: [{}],
    isToggle: false,
    isSelectedAll: false,
    isOpenAddCSPA: false,
    addCSPAErrorMsg: 'addCSPAErrorMsg'
  }
};

describe('Test ViewEntitlementWrapper', () => {
  it('ViewEntitlementWrapper', () => {
    renderWrapper(initialState);
    expect(screen.getByText('ViewElementSetting')).toBeInTheDocument();
  });

  it('ViewEntitlementWrapper > isOpenViewElement is fasle', () => {
    const newState = cloneDeep(initialState);
    set(
      newState,
      ['configurationContactAccountEntitlement', 'isOpenViewElement'],
      false
    );
    renderWrapper(newState);
    const res = document.getElementsByTagName('div');
    expect(res[0]!).toBeInTheDocument();
  });
});
