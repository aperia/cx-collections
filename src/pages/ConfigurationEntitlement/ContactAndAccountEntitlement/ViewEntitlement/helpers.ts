import {
  ContactAccountDataConfig,
  ContactAccountEntitlementConfig,
  CriteriaConfig
} from '../types';

/**
 * Build checked on criteria section
 * @param entitlements
 * @param activeEntitlement
 * @param checkedValue
 * @returns
 */
export const buildEntitlementCheckedData =
  (
    entitlements: Array<ContactAccountEntitlementConfig>,
    activeEntitlement: Partial<RefDataValue>,
    checkedValue: string
  ) =>
  (typeCode: string) => {
    const buildCriteriaData = (criteriaDataItem: CriteriaConfig) => {
      const { criteriaCode, items } = criteriaDataItem;

      return {
        criteriaCode,
        items: items?.reduce(
          (dataItems: Array<ContactAccountDataConfig>, dataItem) => {
            if (dataItem.value === checkedValue) {
              return [
                ...dataItems,
                {
                  ...dataItem,
                  active: !dataItem.active
                }
              ];
            }

            dataItems = [...dataItems, dataItem];
            return dataItems;
          },
          []
        )
      };
    };

    return entitlements.reduce(
      (newEntitlement: Array<ContactAccountEntitlementConfig>, item) => {
        if (item.entitlementCode === activeEntitlement?.value) {
          const { data } = item;

          const currentCriteriaData = data?.reduce(
            (newCriteriaData: Array<CriteriaConfig>, criteriaDataItem) => {
              const { criteriaCode } = criteriaDataItem;

              if (typeCode === criteriaCode) {
                return [
                  ...newCriteriaData,
                  buildCriteriaData(criteriaDataItem)
                ];
              }

              newCriteriaData = [...newCriteriaData, criteriaDataItem];
              return newCriteriaData;
            },
            []
          );

          return [
            ...newEntitlement,
            {
              ...item,
              data: currentCriteriaData
            }
          ];
        }

        newEntitlement = [...newEntitlement, item];

        return newEntitlement;
      },
      []
    );
  };

/**
 * Build check all data for all criteria
 * @param entitlements
 * @param activeEntitlement
 * @param isCheckedAll
 * @returns
 */
export const buildEntitlementCheckedAllData = (
  entitlements: Array<ContactAccountEntitlementConfig>,
  activeEntitlement: Partial<RefDataValue>,
  isCheckedAll: boolean
) => {
  const buildCriteriaData = (
    newCriteriaData: Array<CriteriaConfig>,
    criteriaDataItem: CriteriaConfig
  ) => {
    const { criteriaCode, items } = criteriaDataItem;

    return [
      ...newCriteriaData,
      {
        criteriaCode,
        items: items?.reduce(
          (dataItems: Array<ContactAccountDataConfig>, dataItem) => {
            dataItems = [
              ...dataItems,
              {
                ...dataItem,
                active: isCheckedAll
              }
            ];
            return dataItems;
          },
          []
        )
      }
    ];
  };

  return entitlements.reduce(
    (newEntitlement: Array<ContactAccountEntitlementConfig>, item) => {
      if (item.entitlementCode === activeEntitlement?.value) {
        const { data } = item;

        const currentCriteriaData = data?.reduce(
          (newCriteriaData: Array<CriteriaConfig>, criteriaDataItem) => {
            newCriteriaData = buildCriteriaData(
              newCriteriaData,
              criteriaDataItem
            );
            return newCriteriaData;
          },
          []
        );

        return [
          ...newEntitlement,
          {
            ...item,
            data: currentCriteriaData
          }
        ];
      }

      newEntitlement = [...newEntitlement, item];

      return newEntitlement;
    },
    []
  );
};

export const detectAllDataChecked = (
  entitlements: Array<ContactAccountEntitlementConfig> = [],
  activeEntitlement: Partial<RefDataValue>
) => {
  const entitlementData = entitlements?.find(
    item => item.entitlementCode === activeEntitlement?.value
  );

  return !!entitlementData?.data?.every(data =>
    data.items?.every(i => i.active)
  );
};
