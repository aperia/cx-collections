import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { ActionInTileDictionary, MultipleLangDictionary } from 'app/entitlements/dictionary';
import { getActiveTab, takeActiveEntitlement } from '../_redux/selectors';
import { ENTITLEMENT_COMPONENTS, PERMISSIONS } from 'app/entitlements/constants';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface TitleProps {
  title: string;
}

const Title: React.FC<TitleProps> = ({ title }) => {
  const { t } = useTranslation();
  const activeTab = useSelector(getActiveTab);
  const activeEntitlement = useSelector(takeActiveEntitlement);
  
  const tabName = t(
    MultipleLangDictionary.get(activeTab.component as ENTITLEMENT_COMPONENTS)
  );
  
  const actionText = t(
    ActionInTileDictionary.get(activeEntitlement.value as PERMISSIONS)
  );

  return (
    <Fragment>
      <div className="divider-dashed mb-16" />
      <p
        className="fw-500 mb-8"
        data-testid={genAmtId('viewEntitlement', `${title}`, '')}
      >
        {t(title)}
      </p>
      <p data-testid={genAmtId('viewEntitlement', `${tabName}`, '')}>
        {t('txt_entitlement_criteria_help_text', { actionText, tabName })}
      </p>
    </Fragment>
  );
};

export default Title;
