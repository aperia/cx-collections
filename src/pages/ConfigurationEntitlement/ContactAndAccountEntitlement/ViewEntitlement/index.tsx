import React from 'react';
import { useSelector } from 'react-redux';

// Redux
import { selectOpenViewElement } from '../_redux/selectors';

// Component
import ViewElementSetting from './Settings/index';

const ViewEntitlementWrapper: React.FC = () => {
  const isOpen = useSelector(selectOpenViewElement);
  if (!isOpen) return null;
  return <ViewElementSetting />;
};

export default ViewEntitlementWrapper;
