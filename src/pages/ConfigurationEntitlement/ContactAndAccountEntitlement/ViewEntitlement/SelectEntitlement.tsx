import React, { Fragment } from 'react';

// components
import {
  DropdownBaseChangeEvent,
  DropdownList
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

//lodash
import isEmpty from 'lodash.isempty';

// Const
import { ENTITLEMENT_COMPONENTS } from 'app/entitlements/constants';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import {
  takeActiveEntitlement,
  takeActiveTabData,
  takeListActiveEntitlements
} from '../_redux/selectors';
import unSavedChange from '../_redux/unSavedChange';
import { configurationContactAccountEntitlementActions } from '../_redux/reducer';

// Dictionary
import { MultipleLangDictionary } from 'app/entitlements/dictionary';
import { genAmtId } from 'app/_libraries/_dls/utils';

const SelectEntitlement: React.FC = () => {
  const { t } = useTranslation();
  const activeTabData = useSelector(takeActiveTabData);
  const activeEntitlement = useSelector(takeActiveEntitlement);
  const listActiveEntitlement = useSelector(takeListActiveEntitlements);
  const dispatch = useDispatch();

  const onChange = (event: DropdownBaseChangeEvent) => {
    const { value: selectEntitlement } = event;
    if (activeEntitlement.value === (selectEntitlement as RefDataValue).value)
      return;
    const callbackFunction = () => {
      batch(() => {
        dispatch(
          configurationContactAccountEntitlementActions.onDiscardChange()
        );

        dispatch(
          configurationContactAccountEntitlementActions.onChangeEntitlement(
            selectEntitlement as RefDataValue
          )
        );
      });
    };

    dispatch(unSavedChange(callbackFunction));
  };

  return (
    <Fragment>
      <div className="d-lg-flex align-items-center justify-content-between">
        <div>
          <h5 data-testid={genAmtId('viewEntitlement', 'activeTab', '')}>
            {t(
              MultipleLangDictionary.get(
                activeTabData.component as ENTITLEMENT_COMPONENTS
              )
            )}
          </h5>
          <p
            className="mt-16"
            data-testid={genAmtId(
              'viewEntitlement',
              'select-an-entitlement',
              ''
            )}
          >
            {t('txt_select_an_entitlement_to_configure')}
          </p>
        </div>
        <div className="w-320px mt-md-16 mt-lg-0">
          <DropdownList
            name="entitlement"
            label={t('txt_entitlement')}
            onChange={onChange}
            textField="description"
            value={isEmpty(activeEntitlement) ? null : activeEntitlement}
            dataTestId="ViewEntitlement_select-entitlement"
            noResult={t('txt_no_results_found')}
          >
            {listActiveEntitlement.map(item => (
              <DropdownList.Item
                key={item.value}
                label={item.description}
                value={item}
              />
            ))}
          </DropdownList>
        </div>
      </div>
    </Fragment>
  );
};

export default React.memo(SelectEntitlement);
