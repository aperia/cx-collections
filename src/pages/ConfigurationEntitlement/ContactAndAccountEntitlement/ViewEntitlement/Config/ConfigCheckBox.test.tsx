import React from 'react';
import { queryByClass, renderMockStore } from 'app/test-utils';
import { screen } from '@testing-library/react';

import { CRITERIA_NAME } from 'app/entitlements/constants';
import ConfigCheckbox from './ConfigCheckBox';

interface ConfigCheckBoxProps {
  title: string;
  type: CRITERIA_NAME;
  onChange: (value: string, type: CRITERIA_NAME) => void | undefined;
}

const initialState: Partial<RootState> = {
  configurationContactAccountEntitlement: {
    listActiveEntitlements: [
      {
        description: 'activeEntitlement',
        value: 'activeEntitlement'
      },
      {
        description: 'inActiveEntitlement',
        value: 'inActiveEntitlement'
      }
    ],

    entitlements: [{}],
    listContactAccountCSPA: [{}],
    isLoadingModal: false,
    isLoadingListCSPA: false,
    isNoResultsFound: false,
    isOpenViewElement: false,
    selectedCSPA: {},
    textSearch: 'textSearch',
    isErrorSettingEntitlement: false,
    isErrorLoadingListCSPA: false,
    sortBy: {
      id: 'id',
      order: 'desc'
    },
    activeTabEntitlement: {
      entitlements: [
        {
          entitlementCode: 'activeEntitlement',
          data: [
            {
              criteriaCode: CRITERIA_NAME.COMMERCIAL_CARD_CODE,
              items: [
                {
                  value: 'item1',
                  active: true,
                  description: 'desc'
                }
              ]
            },
            {
              criteriaCode: CRITERIA_NAME.CUSTOMER_ROLE_TYPE_CODE,
              items: [
                {
                  active: true,
                  description: 'desc'
                }
              ]
            }
          ]
        }
      ]
    },
    listCriteria: [],
    activeEntitlement: {
      description: 'activeEntitlement',
      value: 'activeEntitlement'
    },
    listCSPA: [{}],
    isToggle: false,
    isSelectedAll: false,
    isOpenAddCSPA: false,
    addCSPAErrorMsg: 'addCSPAErrorMsg'
  }
};

const renderWrapper = (
  initialState: Partial<RootState>,
  checkboxProps: ConfigCheckBoxProps
) => {
  return renderMockStore(<ConfigCheckbox {...checkboxProps} />, {
    initialState
  });
};

describe('Test ConfigCheckBox', () => {
  const onChangeMock = jest.fn();
  const props = {
    title: 'Config Checkbox',
    onChange: onChangeMock,
    type: CRITERIA_NAME.COMMERCIAL_CARD_CODE
  };

  it('Should render checkbox', () => {
    renderWrapper(initialState, props);
    expect(screen.getByText(/Config Checkbox/)).toBeInTheDocument();
  });

  it('Should run onChange', () => {
    jest.useFakeTimers();
    const { container } = renderWrapper(initialState, {
      ...props,
      type: CRITERIA_NAME.CUSTOMER_ROLE_TYPE_CODE
    });
    jest.runAllTimers();

    const firstCheckboxItem = queryByClass(
      container,
      'custom-control-input dls-checkbox-input'
    );

    firstCheckboxItem?.click();
    expect(onChangeMock).toBeCalled();
  });
});
