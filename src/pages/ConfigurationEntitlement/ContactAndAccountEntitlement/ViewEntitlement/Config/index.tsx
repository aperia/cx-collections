import React, { useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// component
import { Button, SimpleBar } from 'app/_libraries/_dls/components';
import SelectEntitlement from '../SelectEntitlement';
import SelectAll from '../SelectAll';
import ConfigCheckBox from './ConfigCheckBox';

// helpers
import {
  dispatchCloseModalEvent,
  dispatchControlTabChangeEvent
} from '../../../helpers';
import isEqual from 'lodash.isequal';

// hooks
import { useListenChangingTabEvent } from '../../../hooks/useListenChangingTabEvent';
import { useListenClosingModal } from '../../../hooks/useListenClosingModal';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Const
import { CRITERIA_NAME } from 'app/entitlements/constants';

// Redux
import { resetToPrevious } from '../../_redux/resetToPrevious';
import {
  getActiveTab,
  getStatusContactAccountEntitlement,
  selectSelectedCSPA,
  takeListCriteria
} from '../../_redux/selectors';
import unSavedChange from '../../_redux/unSavedChange';
import { configurationContactAccountEntitlementActions } from '../../_redux/reducer';

// Dictionary
import { MultipleLangDictionary } from 'app/entitlements/dictionary';
import { genAmtId } from 'app/_libraries/_dls/utils';

const ConfigViewEntitlement: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const activeTabEntitlement = useSelector(getActiveTab);
  const entitlements = useSelector(getStatusContactAccountEntitlement);
  const currentEntitlement = entitlements?.find(
    item => item.component === activeTabEntitlement?.component
  );

  const selectedCSPA = useSelector(selectSelectedCSPA);

  const listCriteria = useSelector(takeListCriteria);

  const isDisabledSaveButton = useMemo(
    () => isEqual(activeTabEntitlement, currentEntitlement),
    [activeTabEntitlement, currentEntitlement]
  );

  useListenChangingTabEvent(() => {
    dispatch(unSavedChange(dispatchControlTabChangeEvent));
  });

  useListenClosingModal(() => {
    dispatch(unSavedChange(dispatchCloseModalEvent));
  });

  const handlePrevious = () => {
    dispatch(resetToPrevious(t));
  };

  const handleSave = () => {
    dispatch(
      configurationContactAccountEntitlementActions.triggerUpdateContactAccEntitlement(
        { translateFunc: t }
      )
    );
  };

  const handleOnchange = (value: string, typeCode: CRITERIA_NAME) => {
    dispatch(
      configurationContactAccountEntitlementActions.onChangeCheckbox({
        checkedValue: value,
        typeCode
      })
    );
  };

  return (
    <div className={'position-relative has-footer-button'}>
      <SimpleBar>
        <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
          <SelectEntitlement />
          <SelectAll active={selectedCSPA?.active} />
          {listCriteria?.map(item => {
            return (
              <ConfigCheckBox
                title={t(MultipleLangDictionary.get(item))}
                type={item}
                key={item}
                onChange={handleOnchange}
                disableAll={!selectedCSPA.active}
              />
            );
          })}
        </div>
      </SimpleBar>
      <div className="group-button-footer d-flex justify-content-end">
        <Button
          disabled={!selectedCSPA.active}
          onClick={handlePrevious}
          variant="secondary"
          dataTestId={genAmtId('viewEntitlement_config', 'reset-btn', '')}
        >
          {t('txt_reset_to_previous')}
        </Button>
        <Button
          onClick={handleSave}
          variant="primary"
          disabled={!selectedCSPA.active || isDisabledSaveButton}
          dataTestId={genAmtId('viewEntitlement_config', 'save-btn', '')}
        >
          {t('txt_save_changes')}
        </Button>
      </div>
    </div>
  );
};

export default React.memo(ConfigViewEntitlement);
