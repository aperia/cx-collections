import React from 'react';
import { screen } from '@testing-library/react';

// utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { queryAllByClass } from 'app/_libraries/_dls/test-utils';

import ConfigViewEntitlement from './';
import { configurationContactAccountEntitlementActions } from '../../_redux/reducer';
import * as resetToPreviousAction from '../../_redux/resetToPrevious';
import * as helpers from '../../../helpers';

import {
  CRITERIA_NAME,
  ENTITLEMENT_COMPONENTS
} from 'app/entitlements/constants';
import { SECTION_TAB_EVENT } from '../../../constants';

const initialState: Partial<RootState> = {
  configurationContactAccountEntitlement: {
    listActiveEntitlements: [
      {
        description: 'activeEntitlement',
        value: 'activeEntitlement'
      },
      {
        description: 'inActiveEntitlement',
        value: 'inActiveEntitlement'
      }
    ],

    entitlements: [
      {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH
      }
    ],
    listContactAccountCSPA: [{}],
    isLoadingModal: false,
    isLoadingListCSPA: false,
    isNoResultsFound: false,
    isOpenViewElement: false,
    selectedCSPA: {
      id: 'string',
      cspaId: 'string',
      clientId: 'string',
      systemId: 'string',
      principleId: 'string',
      agentId: 'string',
      description: 'string',
      userId: 'string',
      active: true
    },
    textSearch: 'textSearch',
    isErrorSettingEntitlement: false,
    isErrorLoadingListCSPA: false,
    sortBy: {
      id: 'id',
      order: 'desc'
    },
    activeTabEntitlement: {
      entitlements: [
        {
          entitlementCode: 'activeEntitlement',
          data: [
            {
              criteriaCode: CRITERIA_NAME.COMMERCIAL_CARD_CODE,
              items: [
                {
                  value: 'item1',
                  active: true,
                  description: 'desc'
                }
              ]
            },
            {
              criteriaCode: CRITERIA_NAME.CUSTOMER_ROLE_TYPE_CODE,
              items: [
                {
                  active: true,
                  description: 'desc'
                }
              ]
            }
          ]
        }
      ],
      component: ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH
    },
    listCriteria: [
      CRITERIA_NAME.COMMERCIAL_CARD_CODE,
      CRITERIA_NAME.CUSTOMER_ROLE_TYPE_CODE
    ],
    activeEntitlement: {
      description: 'activeEntitlement',
      value: 'activeEntitlement'
    },
    listCSPA: [{}],
    isToggle: false,
    isSelectedAll: false,
    isOpenAddCSPA: false,
    addCSPAErrorMsg: 'addCSPAErrorMsg',
    openStatusModal: true
  }
};
const configurationContactAccountEntitlementActionsMock = mockActionCreator(
  configurationContactAccountEntitlementActions
);
const resetToPreviousActionMock = mockActionCreator(resetToPreviousAction);

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(<ConfigViewEntitlement />, {
    initialState
  });
};

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

jest.mock('../SelectEntitlement', () => {
  return () => <div>SelectEntitlement</div>;
});

jest.mock('../SelectAll', () => {
  return () => <div>SelectAll</div>;
});

describe('Test ConfigViewEntitlement', () => {
  it('Should render UI', () => {
    renderWrapper(initialState);
    expect(screen.getByText(/SelectEntitlement/)).toBeInTheDocument();
  });

  it('Test handleOnchange', () => {
    const handleOnchangeAction =
      configurationContactAccountEntitlementActionsMock('onChangeCheckbox');

    const { container } = renderWrapper(initialState);

    const checkboxItem = queryAllByClass(
      container,
      'custom-control-input dls-checkbox-input'
    );

    checkboxItem[0]?.click();
    expect(handleOnchangeAction).toBeCalledWith({
      checkedValue: 'item1',
      typeCode: CRITERIA_NAME.COMMERCIAL_CARD_CODE
    });
  });

  it('Test handleSave', () => {
    const triggerUpdateContactAccEntitlementAction =
      configurationContactAccountEntitlementActionsMock(
        'triggerUpdateContactAccEntitlement'
      );

    renderWrapper(initialState);
    const saveBtn = screen.getByText(/txt_save_changes/);
    saveBtn.click();

    expect(triggerUpdateContactAccEntitlementAction).toBeCalled();
  });

  it('Test handlePrevious', () => {
    const resetToPreviousAction = resetToPreviousActionMock('resetToPrevious');
    renderWrapper(initialState);
    const resetBtn = screen.getByText(/txt_reset_to_previous/);
    resetBtn.click();

    expect(resetToPreviousAction).toBeCalled();
  });

  it('Should trigger useListenChangingTabEven', () => {
    const mockDispatchEventChangeTab = jest.fn();

    const spy = jest
      .spyOn(helpers, 'dispatchControlTabChangeEvent')
      .mockImplementation(mockDispatchEventChangeTab);

    const emitChangingTabEvent = () => {
      const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGING_TAB, {
        bubbles: true,
        cancelable: true
      });
      window.dispatchEvent(event);
    };

    renderWrapper(initialState);
    emitChangingTabEvent();
    expect(screen.getByText(/SelectEntitlement/)).toBeInTheDocument();
    expect(window.clientConfiguration?.isHavingTabListener).toBeTruthy();

    spy.mockReset();
    spy.mockRestore();
  });

  it('Should trigger useListenClosingModal', () => {
    const mockDispatchCloseModalEvent = jest.fn();
    const spy = jest
      .spyOn(helpers, 'dispatchCloseModalEvent')
      .mockImplementation(mockDispatchCloseModalEvent);

    const emitClosingModalEvent = () => {
      const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
        bubbles: true,
        cancelable: true
      });

      window.dispatchEvent(event);
    };

    renderWrapper(initialState);
    expect(screen.getByText(/SelectEntitlement/)).toBeInTheDocument();
    emitClosingModalEvent();

    spy.mockReset();
    spy.mockRestore();
  });
});
