import React from 'react';
import { useSelector } from 'react-redux';
import classNames from 'classnames';
import { isEmpty } from 'lodash';

// component
import Title from '../Title';
import { CheckBox } from 'app/_libraries/_dls/components';

// Const
import { CRITERIA_NAME } from 'app/entitlements/constants';

// Actions
import { takeActiveTabEntitlementData } from '../../_redux/selectors';

// Const
import { UI_CRITERIA } from 'pages/ConfigurationEntitlement/constants';

// Type
import { ContactAccountDataConfig } from '../../types';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { useTranslation } from 'app/_libraries/_dls/hooks';

interface ConfigCheckBoxProps {
  title: string;
  type: CRITERIA_NAME;
  onChange: (value: string, type: CRITERIA_NAME) => void | undefined;
  disableAll: boolean;
}

const ConfigCheckBox: React.FC<ConfigCheckBoxProps> = React.memo(
  ({ type, title, onChange, disableAll }) => {
    const { t } = useTranslation();

    const dataCheckBox = useSelector((state: RootState) =>
      takeActiveTabEntitlementData(state, type)
    );

    const getClassName = (value: string) => {
      const { className = '' } =
        UI_CRITERIA[type].find(item => item.value === value) || {};

      return className;
    };

    const getDescription = (item: ContactAccountDataConfig) => {
      const { description, value } = item;
      if (
        isEmpty(value?.trim()) ||
        type === CRITERIA_NAME.CUSTOMER_ROLE_TYPE_CODE
      )
        return t(description);
      return `${value} - ${t(description)}`;
    };

    return (
      <div className="mt-16">
        <Title title={title} />
        <div className="row px-12">
          {dataCheckBox.map(item => (
            <CheckBox
              key={`${item.value}-${type}`}
              className={classNames(getClassName(item.value || ''), 'mt-16')}
              dataTestId={genAmtId(
                'configCheckBox',
                `${item?.description}-radio`,
                ''
              )}
            >
              <CheckBox.Input
                onChange={event => {
                  onChange(event.target.name, type);
                }}
                id={`${item.value}-${type}`}
                name={item.value}
                checked={item.active}
                readOnly={disableAll}
              />
              <CheckBox.Label>{getDescription(item)}</CheckBox.Label>
            </CheckBox>
          ))}
        </div>
      </div>
    );
  }
);

export default ConfigCheckBox;
