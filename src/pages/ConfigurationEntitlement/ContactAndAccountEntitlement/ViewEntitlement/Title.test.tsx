import React from 'react';
import Title from './Title';
import { screen } from '@testing-library/react';

// utils
import { renderMockStore } from 'app/test-utils/renderComponentWithMockStore';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

describe('Test render Title', () => {
  it('should render correctly', () => {
    const title = 'ViewEntilement';
    renderMockStore(<Title title={title} />);

    expect(screen.getByText(/ViewEntilement/)).toBeInTheDocument();
  });
});
