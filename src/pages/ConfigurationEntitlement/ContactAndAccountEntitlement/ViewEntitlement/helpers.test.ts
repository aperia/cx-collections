import { ContactAccountEntitlementConfig } from '../types';
import {
  buildEntitlementCheckedData,
  buildEntitlementCheckedAllData,
  detectAllDataChecked
} from './helpers';

const entitilements: ContactAccountEntitlementConfig[] = [
  {
    entitlementCode: 'entitlementCode',
    data: [
      {
        criteriaCode: 'criteriaCode',
        items: [
          {
            value: 'criteriaCodeA',
            active: true,
            description: 'description'
          }
        ]
      }
    ]
  }
];

const activeEntitlement: RefDataValue = {
  description: 'description',
  value: 'entitlementCode'
};

const checkedValue = 'criteriaCodeA';

describe('buildEntitlementCheckedData', () => {
  it('should run correctly with buildCriteriaData', () => {
    const typeCode = 'criteriaCode';

    const expectedRes = [
      {
        entitlementCode: 'entitlementCode',
        data: [
          {
            criteriaCode: 'criteriaCode',
            items: [
              {
                value: 'criteriaCodeA',
                active: false,
                description: 'description'
              }
            ]
          }
        ]
      }
    ];

    const result = buildEntitlementCheckedData(
      entitilements,
      activeEntitlement,
      checkedValue
    )(typeCode);

    expect(result).toEqual(expectedRes);
  });

  it('should run correctly with buildCriteriaData > checkedValue is difference with value', () => {
    const typeCode = 'criteriaCode';

    const expectedRes = [
      {
        entitlementCode: 'entitlementCode',
        data: [
          {
            criteriaCode: 'criteriaCode',
            items: [
              {
                value: 'criteriaCodeA',
                active: true,
                description: 'description'
              }
            ]
          }
        ]
      }
    ];

    const checkedValue = 'criteriaCodeN';

    const result = buildEntitlementCheckedData(
      entitilements,
      activeEntitlement,
      checkedValue
    )(typeCode);

    expect(result).toEqual(expectedRes);
  });

  it('should run correctly with buildCriteriaData > checkedValue is difference with value', () => {
    const typeCode = 'notCriteriaCode';

    const expectedRes = [
      {
        entitlementCode: 'entitlementCode',
        data: [
          {
            criteriaCode: 'criteriaCode',
            items: [
              {
                value: 'criteriaCodeA',
                active: true,
                description: 'description'
              }
            ]
          }
        ]
      }
    ];

    const checkedValue = 'criteriaCodeN';

    const result = buildEntitlementCheckedData(
      entitilements,
      activeEntitlement,
      checkedValue
    )(typeCode);

    expect(result).toEqual(expectedRes);
  });

  it('should run correctly activeEntitlement is not the same', () => {
    const typeCode = 'notCriteriaCode';

    const expectedRes = [
      {
        entitlementCode: 'entitlementCode',
        data: [
          {
            criteriaCode: 'criteriaCode',
            items: [
              {
                value: 'criteriaCodeA',
                active: true,
                description: 'description'
              }
            ]
          }
        ]
      }
    ];

    const checkedValue = 'criteriaCodeN';

    const result = buildEntitlementCheckedData(
      entitilements,
      { ...activeEntitlement, value: 'notSame' },
      checkedValue
    )(typeCode);

    expect(result).toEqual(expectedRes);
  });
});

describe('buildEntitlementCheckedAllData', () => {
  it('should run correctly', () => {
    const isCheckedAll = false;
    const expectedRes = [
      {
        entitlementCode: 'entitlementCode',
        data: [
          {
            criteriaCode: 'criteriaCode',
            items: [
              {
                value: 'criteriaCodeA',
                active: isCheckedAll,
                description: 'description'
              }
            ]
          }
        ]
      }
    ];

    const res = buildEntitlementCheckedAllData(
      entitilements,
      activeEntitlement,
      isCheckedAll
    );
    expect(res).toEqual(expectedRes);
  });

  it('should run correctly with isCheckAll is true', () => {
    const isCheckedAll = true;
    const expectedRes = [
      {
        entitlementCode: 'entitlementCode',
        data: [
          {
            criteriaCode: 'criteriaCode',
            items: [
              {
                value: 'criteriaCodeA',
                active: isCheckedAll,
                description: 'description'
              }
            ]
          }
        ]
      }
    ];

    const res = buildEntitlementCheckedAllData(
      entitilements,
      { ...activeEntitlement, value: 'notSample' },
      isCheckedAll
    );
    expect(res).toEqual(expectedRes);
  });
});

describe('detectAllDataChecked', () => {
  it('should return false', () => {
    const res = detectAllDataChecked(entitilements, {
      ...activeEntitlement,
      value: 'notSample'
    });
    expect(res).toBeFalsy();
  });

  it('should return true', () => {
    const res = detectAllDataChecked(entitilements, activeEntitlement);
    expect(res).toBeTruthy();
  });

  it('should return true', () => {
    const res = detectAllDataChecked(undefined, activeEntitlement);
    expect(res).toBeFalsy();
  });
});
