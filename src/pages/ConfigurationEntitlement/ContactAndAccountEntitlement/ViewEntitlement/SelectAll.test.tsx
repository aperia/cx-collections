import React from 'react';
import SelectAll from './SelectAll';
import { fireEvent, screen } from '@testing-library/react';

// utils
import { renderMockStore } from 'app/test-utils/renderComponentWithMockStore';
import { cloneDeep, set } from 'lodash';
import { mockActionCreator } from 'app/test-utils';
import { configurationContactAccountEntitlementActions } from '../_redux/reducer';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

const configurationAction = mockActionCreator(
  configurationContactAccountEntitlementActions
);

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(<SelectAll />, { initialState });
};

const initialState: Partial<RootState> = {
  configurationContactAccountEntitlement: {
    entitlements: [{}],
    listContactAccountCSPA: [{}],
    isLoadingModal: false,
    isLoadingListCSPA: false,
    isNoResultsFound: false,
    isOpenViewElement: false,
    selectedCSPA: {},
    textSearch: 'textSearch',
    isErrorSettingEntitlement: false,
    isErrorLoadingListCSPA: false,
    sortBy: {
      id: 'id',
      order: 'desc'
    },
    activeTabEntitlement: {},
    listCriteria: [],
    activeEntitlement: {},
    listActiveEntitlements: [{}],
    listCSPA: [{}],
    isToggle: false,
    isSelectedAll: false,
    isOpenAddCSPA: false,
    addCSPAErrorMsg: 'addCSPAErrorMsg'
  }
};

describe('Test UI', () => {
  it('txt_select_all', () => {
    const onCheckAllAction = configurationAction('onChangeSelectAll');
    renderWrapper(initialState);
    const btn = screen.getByText('txt_select_all');
    fireEvent.click(btn);
    expect(onCheckAllAction).toBeCalled();
  });

  it('txt_deselect_all', () => {
    const newState = cloneDeep(initialState);
    set(
      newState,
      ['configurationContactAccountEntitlement', 'isSelectedAll'],
      true
    );
    renderWrapper(newState);
    expect(screen.getByText('txt_deselect_all')).toBeInTheDocument();
  });
});
