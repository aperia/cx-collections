import React from 'react';
import { screen } from '@testing-library/react';

import {
  CRITERIA_NAME,
  ENTITLEMENT_COMPONENTS
} from 'app/entitlements/constants';
import { queryByClass, renderMockStore } from 'app/test-utils';

import ViewElementSetting from './';
import { SECTION_TAB_EVENT } from 'pages/ClientConfiguration/constants';
import { ContactAccStatusEntitlementState } from '../../types';

const initialState: Partial<RootState> = {
  configurationContactAccountEntitlement: {
    listActiveEntitlements: [
      {
        description: 'activeEntitlement',
        value: 'activeEntitlement'
      },
      {
        description: 'inActiveEntitlement',
        value: 'inActiveEntitlement'
      }
    ],
    entitlements: [
      {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION,
        entitlements: [
          {
            entitlementCode: 'entitlementCode',
            data: [
              {
                criteriaCode: CRITERIA_NAME.COMMERCIAL_CARD_CODE,
                items: [
                  {
                    value: 'item1',
                    active: true,
                    description: 'desc'
                  }
                ]
              }
            ]
          }
        ]
      }
    ],
    listContactAccountCSPA: [{}],
    isLoadingModal: false,
    isLoadingListCSPA: false,
    isNoResultsFound: false,
    isOpenViewElement: false,
    selectedCSPA: {
      id: 'id',
      clientId: 'clientId',
      systemId: 'systemId',
      principleId: 'principleId',
      agentId: 'agentId',
      description: 'description'
    },
    textSearch: 'textSearch',
    isErrorSettingEntitlement: false,
    isErrorLoadingListCSPA: false,
    sortBy: {
      id: 'id',
      order: 'desc'
    },
    activeTabEntitlement: {
      entitlements: [
        {
          entitlementCode: 'activeEntitlement',
          data: [
            {
              criteriaCode: CRITERIA_NAME.COMMERCIAL_CARD_CODE,
              items: [
                {
                  value: 'item1',
                  active: true,
                  description: 'desc'
                }
              ]
            },
            {
              criteriaCode: CRITERIA_NAME.CUSTOMER_ROLE_TYPE_CODE,
              items: [
                {
                  active: true,
                  description: 'desc'
                }
              ]
            }
          ]
        }
      ]
    },
    listCriteria: [],
    activeEntitlement: {
      description: 'activeEntitlement',
      value: 'activeEntitlement'
    },
    listCSPA: [{}],
    isToggle: false,
    isSelectedAll: false,
    isOpenAddCSPA: false,
    addCSPAErrorMsg: 'addCSPAErrorMsg',
    openStatusModal: true
  } as ContactAccStatusEntitlementState
};

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

jest.mock('resize-observer-polyfill', () =>
  jest.requireActual('app/test-utils/mocks/resizeObserver.ts')
);

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(<ViewElementSetting />, {
    initialState
  });
};

describe('Test UI', () => {
  it('Should render UI', () => {
    renderWrapper(initialState);
    expect(
      screen.getByText(/txt_contact_and_account_status_entitlement_setting/)
    ).toBeInTheDocument();
  });

  it('Should trigger handleCloseModal', () => {
    const emitClosingModalEvent = () => {
      const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
        bubbles: true,
        cancelable: true
      });

      window.dispatchEvent(event);
    };
    renderWrapper(initialState);
    emitClosingModalEvent();
  });

  it('Should trigger onChangeActiveTab', () => {
    const emitChangeTabEvent = () => {
      const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGE_TAB, {
        bubbles: true,
        cancelable: true
      });

      window.dispatchEvent(event);
    };
    const { baseElement } = renderWrapper(initialState);
    const selectedTab = queryByClass(baseElement, 'vertical-tab scrolled-nav');
    selectedTab!.click();
    emitChangeTabEvent();
  });

  it('Should trigger handleCloseModal ', () => {
    const { baseElement } = renderWrapper(initialState);
    const queryIconClose = queryByClass(baseElement, 'icon icon-close');
    queryIconClose!.click();
  });

  it('Should trigger handleOnCloseModal with isHavingTabListener is true ', () => {
    window.clientConfiguration = { isHavingTabListener: true };
    const { baseElement } = renderWrapper(initialState);
    const queryIconClose = queryByClass(baseElement, 'icon icon-close');
    queryIconClose!.click();
  });
});
