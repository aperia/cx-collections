import React, { useEffect, useRef } from 'react';
import { View } from 'app/_libraries/_dof/core';
import { useElementSize } from 'app/hooks';
import { InlineMessage } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface TopInformationProps {
  data?: MagicKeyValue;
  setHeight?: (height: string) => void;
  warningMessage?: string;
}

const TopInformation: React.FC<TopInformationProps> = ({
  data,
  setHeight,
  warningMessage
}) => {
  const containerRef = useRef<HTMLDivElement>(null);
  const { t } = useTranslation();
  const containerElement = useElementSize(containerRef);

  const active = data?.active || false;
  useEffect(() => {
    const containerHeight = containerElement
      ? `${
          containerElement?.height + containerElement?.x + containerElement?.y
        }`
      : '88';

    containerElement && setHeight && setHeight(containerHeight);
  }, [containerElement, setHeight]);

  return (
    <div className="pt-8 pb-24 px-24 bg-light-l20" ref={containerRef}>
      {!active && (
        <InlineMessage
          id="contact_entitlement-information__inactive-message"
          className="mt-16 mr-8 mb-8"
          dataTestId="topInformation_inactive-message"
          variant="warning"
        >
          {t('txt_contact_entitlement_inactive_message')}
        </InlineMessage>
      )}
      {warningMessage && (
        <InlineMessage
          id="contact_entitlement-information__warning-message"
          className="mt-16 mb-8"
          dataTestId="topInformation_warning-message"
          variant="warning"
        >
          {t(warningMessage)}
        </InlineMessage>
      )}
      <View
        id="clientConfigTopInfo"
        formKey="clientConfigTopInfo"
        descriptor="clientConfigTopInfo"
        value={data}
      />
    </div>
  );
};

export default TopInformation;
