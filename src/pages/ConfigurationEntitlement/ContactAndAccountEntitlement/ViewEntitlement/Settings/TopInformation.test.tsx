import React from 'react';

import TopInformation from './TopInformation';

import { renderMockStore } from 'app/test-utils';

import * as hooks from 'app/hooks/useElementResize';

jest.mock('resize-observer-polyfill', () =>
  jest.requireActual('app/test-utils/mocks/resizeObserver.ts')
);

describe('Test TopInfomation', () => {
  const data: CSPA = {
    id: 'id',
    clientId: 'clientId',
    systemId: 'systemId',
    principleId: 'principleId',
    agentId: 'agentId',
    description: 'description'
  };

  it('should render', () => {
    const { wrapper } = renderMockStore(<TopInformation />);

    expect(wrapper.baseElement).toBeInTheDocument();
  });

  it('should call setHeight', () => {
    const setHeight = jest.fn();

    renderMockStore(
      <TopInformation data={data} setHeight={setHeight} warningMessage="test" />
    );

    expect(setHeight).toHaveBeenCalled();
  });

  it('should NOT call setHeight', () => {
    const setHeight = jest.fn();

    jest.spyOn(hooks, 'useElementSize').mockImplementation(() => undefined);

    renderMockStore(<TopInformation setHeight={setHeight} />);

    expect(setHeight).not.toHaveBeenCalled();
  });
});
