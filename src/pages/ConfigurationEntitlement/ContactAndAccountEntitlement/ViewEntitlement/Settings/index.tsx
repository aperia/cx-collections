import React, { useCallback, useEffect, useState } from 'react';
import {
  Modal,
  ModalBody,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { batch, useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// components
import Sections from './Sections';
import TopInformation from './TopInformation';

// redux
import { configurationContactAccountEntitlementActions } from '../../_redux/reducer';
import {
  selectSelectedCSPA,
  takeLoadingStatus,
  takeWarningMessage
} from '../../_redux/selectors';

// Helper
import { dispatchClosingModalEvent } from '../../../helpers';

// Const
import { SECTION_TAB_EVENT } from 'pages/ConfigurationEntitlement/constants';
import { genAmtId } from 'app/_libraries/_dls/utils';

const ViewElementSetting: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const selectedCSPA = useSelector(selectSelectedCSPA);
  const loading = useSelector(takeLoadingStatus);
  const [topInfoHeight, setTopInfoHeight] = useState('88');

  const warningMessage = useSelector(takeWarningMessage);
  const testId = 'viewEntitlement-setting';

  useEffect(() => {
    dispatch(
      configurationContactAccountEntitlementActions.getContactAccEntitlement({
        translateFn: t
      })
    );
  }, [dispatch, t]);

  const handleOnCloseModal = () => {
    const { isHavingTabListener } = window.clientConfiguration || {};
    if (isHavingTabListener) {
      dispatchClosingModalEvent();
    } else {
      handleCloseModal();
    }
  };

  const handleCloseModal = useCallback(() => {
    batch(() => {
      dispatch(
        configurationContactAccountEntitlementActions.onChangeOpenViewElement()
      );
      dispatch(
        configurationContactAccountEntitlementActions.onChangeViewEntitlement(
          {}
        )
      );
      dispatch(configurationContactAccountEntitlementActions.onResetTab());
    });
  }, [dispatch]);

  /**
   * Listen close modal from closed event inside section's component
   * and execute close modal action
   *
   */
  useEffect(() => {
    window.addEventListener(SECTION_TAB_EVENT.CLOSE_MODAL, handleCloseModal);

    return () => {
      window.removeEventListener(
        SECTION_TAB_EVENT.CLOSE_MODAL,
        handleCloseModal
      );
    };
  }, [dispatch, handleCloseModal]);

  const handleSetTopInformationHeight = useCallback((value: string) => {
    setTopInfoHeight(value);
  }, []);

  return (
    <Modal full loading={loading} show enforceFocus={false} dataTestId={testId}>
      <ModalHeader
        border
        closeButton
        onHide={handleOnCloseModal}
        dataTestId={genAmtId(testId, 'header', '')}
      >
        <ModalTitle
          className="d-flex align-items-center"
          dataTestId={genAmtId(testId, 'title', '')}
        >
          {t('txt_contact_and_account_status_entitlement_setting')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody className="overflow-hidden" noPadding>
        <TopInformation
          warningMessage={warningMessage}
          data={selectedCSPA}
          setHeight={handleSetTopInformationHeight}
        />
        <Sections topInfoHeight={topInfoHeight} />
      </ModalBody>
    </Modal>
  );
};

export default ViewElementSetting;
