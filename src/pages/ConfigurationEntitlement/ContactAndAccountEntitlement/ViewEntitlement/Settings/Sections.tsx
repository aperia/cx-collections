import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
  useState
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import ExtraStaticTabs from 'app/_libraries/_dls/components/VerticalTabs';
import { SimpleBar } from 'app/_libraries/_dls/components';
import classNames from 'classnames';

// Component
import ConfigViewEntitlement from '../Config';

// helpers
import { dispatchTabChangingTabEvent } from 'pages/ConfigurationEntitlement/helpers';

// Const
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { SECTION_TAB_EVENT } from 'pages/ConfigurationEntitlement/constants';
import { ENTITLEMENT_COMPONENTS } from 'app/entitlements/constants';

// Actions
import {
  takeActiveTabData,
  takeStatusContactAccountEntitlement,
  takeToggleStatus
} from '../../_redux/selectors';
import { configurationContactAccountEntitlementActions } from '../../_redux/reducer';

// Dictionary
import { MultipleLangDictionary } from 'app/entitlements/dictionary';

export interface SectionsProps {
  topInfoHeight?: string;
}

const Sections: React.FC<SectionsProps> = ({ topInfoHeight }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const [collapsing, setCollapsing] = useState<boolean>(false);
  const unSettledTab = useRef<string>('');
  const divRef = useRef<HTMLDivElement | null>(null);

  const activeTab = useSelector(takeActiveTabData);
  const sections = useSelector(takeStatusContactAccountEntitlement);
  const toggle = useSelector(takeToggleStatus);

  const handleOnSelect = (eventKey: string | null) => {
    if (!eventKey) return;
    if (activeTab?.component === eventKey) return;
    unSettledTab.current = eventKey;
    const { isHavingTabListener } = window.clientConfiguration || {};

    /**
     * If there is have any section's component listen tab change event,
     * then dispatch a changing tab event, else just set the active tab
     */
    if (isHavingTabListener) {
      dispatchTabChangingTabEvent();
    } else {
      dispatch(
        configurationContactAccountEntitlementActions.onChangeActiveTab({
          component: unSettledTab.current,
          translateFn: t
        })
      );
    }
  };

  const handleToggleCollapsed = useCallback(() => {
    dispatch(configurationContactAccountEntitlementActions.onChangeToggle());

    setCollapsing(true);
    const collapsingTimeout = setTimeout(() => {
      setCollapsing(false);
    }, 500);

    return () => clearTimeout(collapsingTimeout);
  }, [dispatch]);

  /**
   * Control tab change, use to update the active tav,
   * when there are have any action come from other section component
   */
  useEffect(() => {
    const handleChangeTab = () => {
      dispatch(
        configurationContactAccountEntitlementActions.onChangeActiveTab({
          component: unSettledTab.current,
          translateFn: t
        })
      );
    };
    window.addEventListener(SECTION_TAB_EVENT.CHANGE_TAB, handleChangeTab);

    return () => {
      window.removeEventListener(SECTION_TAB_EVENT.CHANGE_TAB, handleChangeTab);
    };
  }, [dispatch, t]);

  useLayoutEffect(() => {
    divRef.current!.style.height = `calc(100% - ${topInfoHeight}px)`;
  }, [topInfoHeight]);

  return (
    <div className="config-setting" ref={divRef}>
      <ExtraStaticTabs
        activeKey={activeTab?.component}
        onSelect={handleOnSelect}
        className="scrolled-nav"
        unmountOnExit
        mountOnEnter
        onToggleCollapsed={handleToggleCollapsed}
        collapsed={toggle}
        expandMessage={t(I18N_COMMON_TEXT.EXPAND)}
        collapseMessage={t(I18N_COMMON_TEXT.COLLAPSE)}
        dataTestId="viewEntitlement-setting-section"
      >
        <div
          className={classNames('tab-left', {
            'is-collapsed': toggle,
            'is-collapsing': collapsing
          })}
        >
          <SimpleBar>
            <ExtraStaticTabs.Nav collapsed={toggle}>
              {sections.map(({ component }) => (
                <ExtraStaticTabs.Nav.Item key={component}>
                  <ExtraStaticTabs.Nav.Link eventKey={component}>
                    {t(
                      MultipleLangDictionary.get(
                        component as ENTITLEMENT_COMPONENTS
                      )
                    )}
                  </ExtraStaticTabs.Nav.Link>
                </ExtraStaticTabs.Nav.Item>
              ))}
            </ExtraStaticTabs.Nav>
          </SimpleBar>
        </div>

        <ExtraStaticTabs.Content>
          <ExtraStaticTabs.Pane
            key={activeTab?.component}
            eventKey={activeTab?.component}
          >
            <ConfigViewEntitlement />
          </ExtraStaticTabs.Pane>
        </ExtraStaticTabs.Content>
      </ExtraStaticTabs>
    </div>
  );
};

export default Sections;
