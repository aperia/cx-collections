import React, { SyntheticEvent } from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { fireEvent, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';
import userEvent from '@testing-library/user-event';

import Sections from './Sections';
import { TabsProps } from 'app/_libraries/_dls/components/VerticalTabs';

// Actions
import { configurationContactAccountEntitlementActions } from '../../_redux/reducer';

// helpers
import * as helpers from 'pages/ConfigurationEntitlement/helpers';

import {
  CRITERIA_NAME,
  ENTITLEMENT_COMPONENTS
} from 'app/entitlements/constants';
import { ContactAccountConfig } from '../../types';
import { SECTION_TAB_EVENT } from 'pages/ConfigurationEntitlement/constants';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls/components/VerticalTabs', () => {
  const Nav = ({ children }: TabsProps) => {
    return <div>{children}</div>;
  };

  Nav.Item = ({ children }: TabsProps) => {
    return <div>{children}</div>;
  };

  Nav.Link = ({ children }: TabsProps) => {
    return <div>{children}</div>;
  };

  const VerticalTabs = ({
    onToggleCollapsed,
    onSelect,
    children
  }: TabsProps) => {
    return (
      <div>
        <input
          data-testid="VerticalTabs_onToggleCollapsed"
          onChange={(e: any) => onToggleCollapsed!(e.target.collapsed)}
        />
        <div
          data-testid="VerticalTabs_onToggleCollapsed_return"
          onClick={() => {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            //@ts-ignore
            onToggleCollapsed!(false)?.();
          }}
        />
        <input
          data-testid="VerticalTabs_onSelect"
          onChange={(e: any) =>
            onSelect!(
              e.target.eventKey,
              {} as unknown as SyntheticEvent<unknown, Event>
            )
          }
        />
        {children}
      </div>
    );
  };

  VerticalTabs.Nav = Nav;
  VerticalTabs.Pane = ({ children }: TabsProps) => {
    return <div>{children}</div>;
  };
  VerticalTabs.Content = ({ children }: TabsProps) => {
    return <div>{children}</div>;
  };

  return {
    __esModule: true,
    default: VerticalTabs
  };
});

jest.mock('../Config', () => () => {
  return <div>ConfigViewEntitlement</div>;
});

const spyClientConfiguration = mockActionCreator(
  configurationContactAccountEntitlementActions
);

const entitlementsTab: ContactAccountConfig[] = [
  {
    component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION,
    entitlements: [
      {
        entitlementCode: 'entitlementCode1',
        data: [
          {
            criteriaCode: CRITERIA_NAME.COMMERCIAL_CARD_CODE,
            items: [
              {
                value: 'item1',
                active: true,
                description: 'desc'
              }
            ]
          }
        ]
      }
    ]
  },
  {
    component: ENTITLEMENT_COMPONENTS.CARDHOLDER_MAINTENANCE,
    entitlements: [
      {
        entitlementCode: 'entitlementCode2',
        data: [
          {
            criteriaCode: CRITERIA_NAME.CUSTOMER_ROLE_TYPE_CODE,
            items: [
              {
                value: 'item1',
                active: true,
                description: 'desc'
              }
            ]
          }
        ]
      }
    ]
  }
];

const activeEntitlementTab: ContactAccountConfig = {
  ...entitlementsTab[0]
};

const initialState: Partial<RootState> = {
  configurationContactAccountEntitlement: {
    listActiveEntitlements: [],
    entitlements: entitlementsTab,
    listContactAccountCSPA: [{}],
    isLoadingModal: false,
    isLoadingListCSPA: false,
    isNoResultsFound: false,
    isOpenViewElement: false,
    selectedCSPA: {
      id: 'string',
      cspaId: 'string',
      clientId: 'string',
      systemId: 'string',
      principleId: 'string',
      agentId: 'string',
      description: 'string',
      userId: 'string'
    },
    textSearch: 'textSearch',
    isErrorSettingEntitlement: false,
    isErrorLoadingListCSPA: false,
    sortBy: {
      id: 'id',
      order: 'desc'
    },
    activeTabEntitlement: activeEntitlementTab,
    listCriteria: [
      CRITERIA_NAME.COMMERCIAL_CARD_CODE,
      CRITERIA_NAME.CUSTOMER_ROLE_TYPE_CODE
    ],
    activeEntitlement: {
      description: 'activeEntitlement',
      value: 'activeEntitlement'
    },
    listCSPA: [{}],
    isToggle: false,
    isSelectedAll: false,
    isOpenAddCSPA: false,
    addCSPAErrorMsg: 'addCSPAErrorMsg',
    openStatusModal: true
  }
};

describe('Render', () => {
  it('should render', () => {
    const { container } = renderMockStore(<Sections />, { initialState });
    expect(container.querySelector('.config-setting')).toBeInTheDocument();
  });

  it('handleOnSelect > when has no tab listener', () => {
    const dispatchTabChangingTabEvent = jest.fn();
    jest
      .spyOn(helpers, 'dispatchTabChangingTabEvent')
      .mockImplementation(dispatchTabChangingTabEvent);
    renderMockStore(<Sections />, { initialState });
    act(() => {
      fireEvent.change(screen.getByTestId('VerticalTabs_onSelect'), {
        target: {
          value: entitlementsTab[1].component,
          eventKey: entitlementsTab[1].component
        }
      });
    });
  });

  it('handleOnSelect > when has tab listener', () => {
    window.clientConfiguration = { isHavingTabListener: true };
    const dispatchTabChangingTabEvent = jest.fn();
    jest
      .spyOn(helpers, 'dispatchTabChangingTabEvent')
      .mockImplementation(dispatchTabChangingTabEvent);

    renderMockStore(<Sections />, { initialState });
    act(() => {
      fireEvent.change(screen.getByTestId('VerticalTabs_onSelect'), {
        target: {
          value: entitlementsTab[1].component,
          eventKey: entitlementsTab[1].component
        }
      });
    });
    expect(dispatchTabChangingTabEvent).toBeCalled();
  });

  it('handleOnSelect > when has no target', () => {
    const { container } = renderMockStore(<Sections />, { initialState });
    act(() => {
      fireEvent.change(screen.getByTestId('VerticalTabs_onSelect'), {
        target: {
          value: entitlementsTab[1].component,
          eventKey: null
        }
      });
    });
    expect(container.querySelector('.config-setting')).toBeInTheDocument();
  });

  it('handleOnSelect > activeTab is the same as current tab', () => {
    const { container } = renderMockStore(<Sections />, { initialState });
    act(() => {
      fireEvent.change(screen.getByTestId('VerticalTabs_onSelect'), {
        target: {
          value: entitlementsTab[0].component,
          eventKey: entitlementsTab[0].component
        }
      });
    });
    expect(container.querySelector('.config-setting')).toBeInTheDocument();
  });

  it('handleToggleCollapsed', () => {
    const mockAction = spyClientConfiguration('onChangeToggle');
    jest.useFakeTimers();
    renderMockStore(<Sections />, { initialState });
    act(() => {
      fireEvent.change(screen.getByTestId('VerticalTabs_onToggleCollapsed'), {
        target: { value: 'value', collapsed: true }
      });
      jest.runAllTimers();
    });
    userEvent.click(
      screen.getByTestId('VerticalTabs_onToggleCollapsed_return')
    );
    expect(mockAction).toBeCalled();
  });

  it('listen to tab change event', () => {
    const onChangeActiveTabAction = spyClientConfiguration('onChangeActiveTab');

    const emitChangingTabEvent = () => {
      const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGE_TAB, {
        bubbles: true,
        cancelable: true
      });
      window.dispatchEvent(event);
    };

    renderMockStore(<Sections />, { initialState });
    emitChangingTabEvent();
    expect(onChangeActiveTabAction).toBeCalled();
  });
});
