import React from 'react';

// components
import { Button } from 'app/_libraries/_dls/components';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { configurationContactAccountEntitlementActions } from '../_redux/reducer';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { takeSelectedAllStatus } from '../_redux/selectors';

interface SelectAllProps {
  active: boolean | undefined;
}

const SelectAll: React.FC<SelectAllProps> = ({ active = true }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const isDisplaySelectAll = useSelector(takeSelectedAllStatus);

  const onClickSelectAll = () => {
    dispatch(configurationContactAccountEntitlementActions.onChangeSelectAll());
  };

  return (
    <Button
      size="sm"
      disabled={!active}
      onClick={onClickSelectAll}
      className="ml-n8 mt-12"
      variant="outline-primary"
      dataTestId="viewEntitlement_select-all-btn"
    >
      {t(isDisplaySelectAll ? 'txt_deselect_all' : 'txt_select_all')}
    </Button>
  );
};

export default SelectAll;
