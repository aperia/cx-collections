import React from 'react';
import SelectEntitlement from './SelectEntitlement';
import { renderMockStore } from 'app/test-utils/renderComponentWithMockStore';
import { fireEvent, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { configurationContactAccountEntitlementActions } from '../_redux/reducer';
import { mockActionCreator } from 'app/test-utils';
import { act } from '@testing-library/react-hooks';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const configurationAction = mockActionCreator(
  configurationContactAccountEntitlementActions
);

const initialState: Partial<RootState> = {
  configurationContactAccountEntitlement: {
    listActiveEntitlements: [
      {
        description: 'activeEntitlement',
        value: 'activeEntitlement'
      },
      {
        description: 'inActiveEntitlement',
        value: 'inActiveEntitlement'
      }
    ],
    entitlements: [{}],
    listContactAccountCSPA: [{}],
    isLoadingModal: false,
    isLoadingListCSPA: false,
    isNoResultsFound: false,
    isOpenViewElement: false,
    selectedCSPA: {},
    textSearch: 'textSearch',
    isErrorSettingEntitlement: false,
    isErrorLoadingListCSPA: false,
    sortBy: {
      id: 'id',
      order: 'desc'
    },
    activeTabEntitlement: {},
    listCriteria: [],
    activeEntitlement: {
      description: 'activeEntitlement',
      value: 'activeEntitlement'
    },
    listCSPA: [{}],
    isToggle: false,
    isSelectedAll: false,
    isOpenAddCSPA: false,
    addCSPAErrorMsg: 'addCSPAErrorMsg',
    openStatusModal: true
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(<SelectEntitlement />, { initialState });
};

describe('SelectEntitlement', () => {
  it('should render UI correctly', () => {
    renderWrapper(initialState);
    expect(
      screen.getByText(/txt_select_an_entitlement_to_configure/)
    ).toBeInTheDocument();
  });

  it('should render UI with empty listActiveEntitlements', () => {
    renderWrapper({});
    expect(
      screen.getByText(/txt_select_an_entitlement_to_configure/)
    ).toBeInTheDocument();
  });
});

describe('Test actions', () => {
  it('active value is diff from selected value', () => {
    const onChangeEntitlement = configurationAction('onChangeEntitlement');
    renderWrapper(initialState);

    const dropdownList = screen.queryByText('txt_entitlement');
    act(() => {
      userEvent.click(dropdownList!);
    });
    const option = screen.getByText(/inActiveEntitlement/);
    act(() => {
      fireEvent.click(option);
    });
    expect(onChangeEntitlement).toBeCalled();
  });

  it('active value is same as selected value', () => {
    renderWrapper(initialState);

    const dropdownList = screen.queryByText('txt_entitlement');
    act(() => {
      userEvent.click(dropdownList!);
    });
    const option = screen.getAllByText(/activeEntitlement/);
    act(() => {
      fireEvent.click(option[1]!);
    });
  });
});
