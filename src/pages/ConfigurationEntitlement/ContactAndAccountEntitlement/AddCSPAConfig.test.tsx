import React from 'react';
import { fireEvent, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import cloneDeep from 'lodash.clonedeep';

// Components
import AddCSPAConfig from './AddCSPAConfig';

// Constants
import { ContactAccStatusEntitlementState } from './types';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';

// Utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { configurationContactAccountEntitlementActions } from './_redux/reducer';
import { CSPA_FORM_FIELDS } from '../constants';
import { DEFAULT_CLIENT_CONFIG_KEY } from 'pages/ClientConfiguration/constants';

HTMLCanvasElement.prototype.getContext = jest.fn();

const spyActions = mockActionCreator(
  configurationContactAccountEntitlementActions
);

describe('Test AddCSPAConfig', () => {
  const initialState: Partial<RootState> = {
    configurationContactAccountEntitlement: {
      isOpenAddCSPA: true,
      addCSPAErrorMsg: '',
      listCSPA: [
        {
          clientId: 'clientId',
          additionalFields: [
            {
              name: DEFAULT_CLIENT_CONFIG_KEY,
              value: 'true'
            }
          ]
        }
      ]
    } as unknown as ContactAccStatusEntitlementState
  };

  describe('should render', () => {
    it('default', () => {
      renderMockStore(<AddCSPAConfig />, { initialState });

      expect(screen.getByText('txt_add_cspa')).toBeInTheDocument();
    });

    it('null', () => {
      const _initialState = cloneDeep(initialState);
      _initialState.configurationContactAccountEntitlement!.listCSPA = [];
      renderMockStore(<AddCSPAConfig />, { initialState: _initialState });

      expect(screen.queryByText('txt_add_cspa')).not.toBeInTheDocument();
    });
  });

  it('should call handleCloseModal', () => {
    const onCloseModalAddCSPA = spyActions('onCloseModalAddCSPA');
    const setAddClientConfigErrorMessage = spyActions(
      'setAddClientConfigErrorMessage'
    );

    const _initialState = cloneDeep(initialState);
    _initialState.configurationContactAccountEntitlement!.isRetry = true;

    renderMockStore(<AddCSPAConfig />, { initialState: _initialState });

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));

    expect(onCloseModalAddCSPA).toHaveBeenCalled();
    expect(setAddClientConfigErrorMessage).toHaveBeenCalled();
  });

  describe('should call handleOnSubmit', () => {
    it('should handleOnSubmit', async () => {
      const triggerAddCSPA = spyActions('triggerAddCSPA');
      const setAddClientConfigErrorMessage = spyActions(
        'setAddClientConfigErrorMessage'
      );

      renderMockStore(<AddCSPAConfig />, { initialState });

      // enable submit button
      const textboxSystemId = screen.getByText(I18N_CLIENT_CONFIG.SYSTEM_ID)
        .parentElement!.previousElementSibling!;
      userEvent.type(textboxSystemId, '1234');

      const textboxDescription = screen.getByText(I18N_COMMON_TEXT.DESCRIPTION)
        .parentElement!.previousElementSibling!;
      userEvent.type(textboxDescription, '1234');

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.SUBMIT));

      await waitFor(() => {
        expect(triggerAddCSPA).toHaveBeenCalled();
        expect(setAddClientConfigErrorMessage).toHaveBeenCalled();
      });
    });

    it('should coverage', async () => {
      const triggerAddCSPA = spyActions('triggerAddCSPA');
      const setAddClientConfigErrorMessage = spyActions(
        'setAddClientConfigErrorMessage'
      );

      const { baseElement } = renderMockStore(<AddCSPAConfig />, {
        initialState
      });

      // enable submit button
      const textboxSystemId = screen.getByText(I18N_CLIENT_CONFIG.SYSTEM_ID)
        .parentElement!.previousElementSibling!;
      userEvent.type(textboxSystemId, '1234');

      const textboxDescription = screen.getByText(I18N_COMMON_TEXT.DESCRIPTION)
        .parentElement!.previousElementSibling!;
      userEvent.type(textboxDescription, '1234');

      const principleRadioCustom = baseElement.querySelectorAll(
        `input[name="${CSPA_FORM_FIELDS.PRINCIPLE_RADIO}"]`
      )[1];
      userEvent.click(principleRadioCustom);

      const textboxPrincipleId = screen.getByText(
        I18N_CLIENT_CONFIG.PRINCIPLE_ID
      ).parentElement!.previousElementSibling!;
      userEvent.type(textboxPrincipleId, '1234');

      const agentRadioCustom = baseElement.querySelectorAll(
        `input[name="${CSPA_FORM_FIELDS.AGENT_RADIO}"]`
      )[1];
      userEvent.click(agentRadioCustom);

      const textboxAgentId = screen.getByText(I18N_CLIENT_CONFIG.AGENT_ID)
        .parentElement!.previousElementSibling!;
      userEvent.type(textboxAgentId, '1234');
      fireEvent.blur(textboxAgentId);

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.SUBMIT));

      await waitFor(() => {
        expect(triggerAddCSPA).toHaveBeenCalled();
        expect(setAddClientConfigErrorMessage).toHaveBeenCalled();
      });
    });
  });

  describe('should call handleOnRetry', () => {
    it('with default form value', () => {
      const triggerAddCSPA = spyActions('triggerAddCSPA');
      const _initialState = cloneDeep(initialState);
      _initialState.configurationContactAccountEntitlement!.isRetry = true;
      _initialState.configurationContactAccountEntitlement!.addCSPAErrorMsg =
        'addCSPAErrorMsg';

      renderMockStore(<AddCSPAConfig />, { initialState: _initialState });

      userEvent.click(screen.getByText('txt_retry'));

      expect(triggerAddCSPA).toHaveBeenCalled();
    });

    it('with custom form value', () => {
      const triggerAddCSPA = spyActions('triggerAddCSPA');

      const { store, baseElement } = renderMockStore(<AddCSPAConfig />, {
        initialState
      });

      const textboxSystemId = screen.getByText(I18N_CLIENT_CONFIG.SYSTEM_ID)
        .parentElement!.previousElementSibling!;
      userEvent.type(textboxSystemId, '1234');

      const textboxDescription = screen.getByText(I18N_COMMON_TEXT.DESCRIPTION)
        .parentElement!.previousElementSibling!;
      userEvent.type(textboxDescription, '1234');

      const principleRadioCustom = baseElement.querySelectorAll(
        `input[name="${CSPA_FORM_FIELDS.PRINCIPLE_RADIO}"]`
      )[1];
      userEvent.click(principleRadioCustom);

      const textboxPrincipleId = screen.getByText(
        I18N_CLIENT_CONFIG.PRINCIPLE_ID
      ).parentElement!.previousElementSibling!;
      userEvent.type(textboxPrincipleId, '1234');

      const agentRadioCustom = baseElement.querySelectorAll(
        `input[name="${CSPA_FORM_FIELDS.AGENT_RADIO}"]`
      )[1];
      userEvent.click(agentRadioCustom);

      const textboxAgentId = screen.getByText(I18N_CLIENT_CONFIG.AGENT_ID)
        .parentElement!.previousElementSibling!;
      userEvent.type(textboxAgentId, '1234');
      fireEvent.blur(textboxAgentId);

      store.dispatch(
        configurationContactAccountEntitlementActions.setAddClientConfigErrorMessage(
          { error: 'addCSPAErrorMsg', isRetry: true }
        )
      );

      userEvent.click(screen.getByText('txt_retry'));

      expect(triggerAddCSPA).toHaveBeenCalled();
    });
  });
});
