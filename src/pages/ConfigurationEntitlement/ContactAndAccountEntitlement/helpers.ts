import { isNil } from 'lodash';

// Const
import {
  COMPONENTS_MAP,
  CRITERIA_CODE,
  CRITERIA_NAME,
  ENTITLEMENT_COMPONENTS,
  PERMISSIONS
} from 'app/entitlements/constants';
import { PRIORITY_COMPONENT, UI_CRITERIA } from '../constants';

// Master data
import masterComponentStructure from 'app/entitlements/MasterStructure/masterComponentStruture';
import masterAccountStatusStructure from 'app/entitlements/MasterStructure/masterAccountStatusStructure';

// Type
import {
  ContactAccountComponentEntitlement,
  ContactAccountConfig,
  ContactAccountCSPA,
  ContactAccountDataConfig,
  ContactAccountEntitlement,
  ContactAccountEntitlementConfig,
  ContactAccountJsonValue
} from './types';

// Dictionary
import {
  APIComponentDictionary,
  AppComponentDictionary,
  CriteriaCodeAPIDictionary,
  CriteriaCodeDictionary,
  MultipleLangDictionary
} from 'app/entitlements/dictionary';

// Helper
import { buildEntitlementCode } from 'app/entitlements/helpers';

/**
 * Build the contact component permission based the selected CSPA
 * @param contactAccountCSPAs
 * @returns Array<ContactAccountConfig>
 */
export const convertContactEntitlementToSectionView = (
  contactAccountCSPAs: Array<ContactAccountComponentEntitlement>,
  translateFn: Function
): Array<ContactAccountConfig> => {
  return masterComponentStructure
    .reduce((newStructure: Array<ContactAccountConfig>, item) => {
      const {
        component,
        entitlements: { items }
      } = item;

      const { jsonValue: currentContactValues = {} } =
        contactAccountCSPAs.find(
          _ =>
            AppComponentDictionary.get(_.component as COMPONENTS_MAP) ===
            component
        ) || {};

      const masterEntitlements = items.reduce(
        (newItems: Array<ContactAccountEntitlementConfig>, itemValue) => {
          const { entitlementCode } = itemValue;

          const contactCriteria = Object.keys(currentContactValues).reduce(
            (newObject: ContactAccountJsonValue, ky) => {
              if (buildEntitlementCode(component, ky) === entitlementCode) {
                newObject = currentContactValues[ky];
              }
              return newObject;
            },
            {}
          );

          newItems = [
            ...newItems,
            {
              entitlementCode: entitlementCode,
              data: masterAccountStatusStructure.map(structureItem => {
                const contactCriteriaValue = Object.keys(
                  contactCriteria
                ).reduce((newObject: Record<string, boolean>, ky) => {
                  if (
                    CriteriaCodeDictionary.get(ky as CRITERIA_CODE) ===
                    structureItem.criteriaCode
                  ) {
                    newObject = contactCriteria[ky];
                  }
                  return newObject;
                }, {});

                return {
                  criteriaCode: structureItem.criteriaCode,
                  items: structureItem.items
                    .map(criteriaItem => {
                      return {
                        ...criteriaItem,
                        active: !!contactCriteriaValue[criteriaItem.value],
                        description: translateFn(criteriaItem.description)
                      };
                    })
                    .sort(
                      (
                        previousItem: ContactAccountDataConfig,
                        nextItem: ContactAccountDataConfig
                      ) => {
                        const { priority: perviousPriority = 0 } =
                          UI_CRITERIA[structureItem.criteriaCode].find(
                            _ => _.value === previousItem.value
                          ) || {};
                        const { priority: nextPriority = 0 } =
                          UI_CRITERIA[structureItem.criteriaCode].find(
                            _ => _.value === nextItem.value
                          ) || {};
                        return perviousPriority - nextPriority;
                      }
                    )
                };
              })
            }
          ];
          return newItems;
        },
        []
      );

      newStructure = [
        ...newStructure,
        {
          component: component,
          entitlements: masterEntitlements
        }
      ];
      return newStructure;
    }, [])
    .sort((previousItem, nextItem) => {
      if (isNil(previousItem.component) || isNil(nextItem.component)) return 0;
      return (
        PRIORITY_COMPONENT[previousItem.component] -
        PRIORITY_COMPONENT[nextItem.component]
      );
    });
};

/**
 * Build the master structure if selected CSPA is not have data
 * @returns Array ContactAccountConfig
 */
export const buildContactAccountMasterStructure = (
  translateFn: Function
): Array<ContactAccountConfig> => {
  return masterComponentStructure
    .reduce((newStructure: Array<ContactAccountConfig>, item) => {
      const {
        component,
        entitlements: { items }
      } = item;

      const masterEntitlements = items.reduce(
        (newItems: Array<ContactAccountEntitlementConfig>, itemValue) => {
          const { entitlementCode } = itemValue;
          newItems = [
            ...newItems,
            {
              entitlementCode: entitlementCode,
              data: masterAccountStatusStructure.map(structureItem => {
                return {
                  criteriaCode: structureItem.criteriaCode,
                  items: structureItem.items
                    .map(criteriaItem => {
                      return {
                        ...criteriaItem,
                        description: translateFn(criteriaItem.description)
                      };
                    })
                    .sort(
                      (
                        previousItem: ContactAccountDataConfig,
                        nextItem: ContactAccountDataConfig
                      ) => {
                        const { priority: perviousPriority = 0 } =
                          UI_CRITERIA[structureItem.criteriaCode].find(
                            _ => _.value === previousItem.value
                          ) || {};
                        const { priority: nextPriority = 0 } =
                          UI_CRITERIA[structureItem.criteriaCode].find(
                            _ => _.value === nextItem.value
                          ) || {};
                        return perviousPriority - nextPriority;
                      }
                    )
                };
              })
            }
          ];
          return newItems;
        },
        []
      );

      newStructure = [
        ...newStructure,
        {
          component,
          entitlements: masterEntitlements
        }
      ];
      return newStructure;
    }, [])
    .sort((previousItem, nextItem) => {
      if (isNil(previousItem.component) || isNil(nextItem.component)) return 0;
      return (
        PRIORITY_COMPONENT[previousItem.component] -
        PRIORITY_COMPONENT[nextItem.component]
      );
    });
};

/**
 * Build list active entitlement
 * @param entitlements
 * @param translateFn
 * @returns
 */
export const buildListActiveEntitlements = (
  entitlements: Array<ContactAccountEntitlementConfig> = [],
  translateFn: Function
) => {
  return entitlements.reduce(
    (newActiveEntitlements: Array<Partial<RefDataValue>>, item) => {
      newActiveEntitlements = [
        ...newActiveEntitlements,
        {
          value: item.entitlementCode,
          description: translateFn(
            MultipleLangDictionary.get(item.entitlementCode as PERMISSIONS)
          )
        }
      ];

      return newActiveEntitlements;
    },
    []
  );
};

/**
 * Convert
 * @param activeTabEntitlement
 * @param selectedCSPA
 * @param componentEntitlements
 * @returns
 */
export const convertSectionViewToAccountStatusEntitlement = (
  activeTabEntitlement: ContactAccountConfig,
  selectedCSPA: CSPA,
  componentEntitlements: ContactAccountConfig[]
): Array<ContactAccountCSPA> => {
  const buildJsonValue = (
    entitlements: Array<ContactAccountEntitlementConfig>
  ) => {
    return entitlements?.reduce(
      (newJsonValue: ContactAccountEntitlement, jsonItem) => {
        const { data, entitlementCode } = jsonItem;
        const [, code = ''] = entitlementCode?.split('.') || [];

        const nextBuildEntitlementCode =
          code.charAt(0).toLocaleLowerCase() + code.slice(1);

        const jsonData = (data || []).reduce(
          (newJsonData: ContactAccountJsonValue, jsonDataItem) => {
            const { criteriaCode, items } = jsonDataItem;
            const buildCriteriaKy = CriteriaCodeAPIDictionary.get(
              criteriaCode as CRITERIA_NAME
            ) as string;

            // If there is have any trash data, doesn't match criteria code return current arr
            if (isNil(buildCriteriaKy)) return newJsonData;

            const newItems = (items || []).reduce(
              (newItem: Record<string, boolean>, i) => {
                const { value = '', active = false } = i;
                newItem = { ...newItem, [value]: active };
                return newItem;
              },
              {}
            );

            newJsonData = {
              ...newJsonData,
              [buildCriteriaKy]: newItems
            };
            return newJsonData;
          },
          {}
        );

        newJsonValue = {
          ...newJsonValue,
          [nextBuildEntitlementCode]: jsonData
        };
        return newJsonValue;
      },
      {}
    );
  };

  const { component: activeComponent, entitlements: activeEntitlements = [] } =
    activeTabEntitlement;

  const components: Array<ContactAccountComponentEntitlement> =
    componentEntitlements?.reduce(
      (newEntitlements: Array<ContactAccountComponentEntitlement>, item) => {
        if (item.component === activeComponent) {
          newEntitlements = [
            ...newEntitlements,
            {
              component: APIComponentDictionary.get(
                activeComponent as ENTITLEMENT_COMPONENTS
              ),
              jsonValue: buildJsonValue(activeEntitlements)
            }
          ];
        }
        return newEntitlements;
      },
      []
    );

  const { cspaId, description, active: cspaActive } = selectedCSPA;

  return [
    {
      cspa: cspaId,
      components,
      description,
      active: cspaActive
    }
  ];
};
