import React from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';
import { isUndefined } from 'lodash';

// Component
import AddCSPAModal from '../Components/AddCSPAModal';

// Selector
import {
  selectDefaultSettingsClientConfig,
  selectRootLoading,
  takeAddErrorMsg,
  takeIsRetry,
  takeOpenAddCSPAStatus
} from './_redux/selectors';
import { configurationContactAccountEntitlementActions } from './_redux/reducer';

// Const
import { CODE_ALL } from '../constants';
import { useStoreIdSelector } from 'app/hooks';
import { ApiErrorSection } from 'pages/ApiErrorNotification/types';
import { takeApiErrorDetailOpenModalForData } from 'pages/ApiErrorNotification/_redux/selectors';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';

export interface AddCSPAConfigProps {}

const AddCSPAConfig: React.FC<AddCSPAConfigProps> = () => {
  const dispatch = useDispatch();

  const isOpenAddCSPA = useSelector<RootState, boolean>(takeOpenAddCSPAStatus);
  const addErrorMsg = useSelector<RootState, string>(takeAddErrorMsg);
  const defaultClientConfig = useSelector(selectDefaultSettingsClientConfig);
  const isLoadingModal = useSelector(selectRootLoading);
  const isRetry = useSelector(takeIsRetry);
  const defaultClientId = defaultClientConfig?.clientId;
  const errorModalOpened = useStoreIdSelector<
    Record<ApiErrorSection, MagicKeyValue>
  >(takeApiErrorDetailOpenModalForData, API_ERROR_STORE_ID.ENTITLEMENT);

  if (isUndefined(defaultClientId)) return null;

  const handleCloseModal = () => {
    batch(() => {
      isRetry &&
        dispatch(
          configurationContactAccountEntitlementActions.getContactAccListCSPA()
        );
      dispatch(
        configurationContactAccountEntitlementActions.onCloseModalAddCSPA()
      );
      dispatch(
        configurationContactAccountEntitlementActions.setAddClientConfigErrorMessage(
          { error: '', isRetry: false }
        )
      );
    });
  };

  const handleOnSubmit = (formValue: FormCSPAValue) => {
    dispatch(
      configurationContactAccountEntitlementActions.setAddClientConfigErrorMessage(
        { error: '', isRetry: false }
      )
    );
    dispatch(
      configurationContactAccountEntitlementActions.triggerAddCSPA({
        defaultClientConfig: defaultClientConfig,
        clientId: defaultClientId,
        systemId: formValue.systemId!.toString(),
        principleId: formValue?.principleRadio?.custom
          ? formValue.principleId!.toString()
          : CODE_ALL,
        agentId: formValue?.agentRadio?.custom
          ? formValue.agentId!.toString()
          : CODE_ALL,
        description: formValue.description!.toString()
      })
    );
  };

  const handleOnRetry = (formValue: FormCSPAValue) => {
    dispatch(
      configurationContactAccountEntitlementActions.triggerAddCSPA({
        defaultClientConfig: defaultClientConfig,
        clientId: defaultClientId,
        systemId: formValue.systemId!.toString(),
        principleId: formValue?.principleRadio?.custom
          ? formValue.principleId!.toString()
          : CODE_ALL,
        agentId: formValue?.agentRadio?.custom
          ? formValue.agentId!.toString()
          : CODE_ALL,
        description: formValue.description!.toString()
      })
    );
  };

  return (
    <AddCSPAModal
      errorMessage={addErrorMsg}
      isOpenModal={isOpenAddCSPA}
      onCloseModal={handleCloseModal}
      isModalLoading={isLoadingModal}
      onSubmit={handleOnSubmit}
      defaultClientId={defaultClientConfig?.clientId}
      isRetry={isRetry}
      onRetry={handleOnRetry}
      errorModalOpened={errorModalOpened}
    />
  );
};

export default AddCSPAConfig;
