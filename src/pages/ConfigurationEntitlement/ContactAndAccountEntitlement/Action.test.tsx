import React from 'react';
import userEvent from '@testing-library/user-event';

// Components
import Action from './Action';

// Constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { CLIENT_CONFIG_STATUS_KEY } from 'pages/ClientConfiguration/constants';

// Utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { configurationContactAccountEntitlementActions } from './_redux/reducer';

describe('Test Action', () => {
  const spyActions = mockActionCreator(
    configurationContactAccountEntitlementActions
  );

  it('should render with active CSPA', () => {
    const CSPA: CSPA = {
      id: 'id',
      clientId: 'clientId',
      systemId: 'systemId',
      principleId: 'principleId',
      agentId: 'agentId',
      description: 'description',
      additionalFields: [
        {
          name: CLIENT_CONFIG_STATUS_KEY,
          active: true
        }
      ]
    };

    const setSelectedCSPA = spyActions('setSelectedCSPA');
    const openStatusModal = spyActions('openStatusModal');
    const onChangeOpenViewElement = spyActions('onChangeOpenViewElement');
    const onChangeViewEntitlement = spyActions('onChangeViewEntitlement');

    const { wrapper } = renderMockStore(<Action data={CSPA} />, {});

    userEvent.click(wrapper.getByText(I18N_COMMON_TEXT.DEACTIVATE));
    userEvent.click(wrapper.getByText('txt_view_entitlement'));

    expect(openStatusModal).toHaveBeenCalled();
    expect(onChangeOpenViewElement).toHaveBeenCalled();
    expect(setSelectedCSPA).toHaveBeenCalledWith(CSPA);
    expect(onChangeViewEntitlement).toHaveBeenCalledWith(CSPA);
    expect(wrapper.getByText('txt_view_entitlement')).toBeInTheDocument();
  });

  it('should render with inactive CSPA', () => {
    const CSPA: CSPA = {
      id: 'id',
      clientId: 'clientId',
      systemId: 'systemId',
      principleId: 'principleId',
      agentId: 'agentId',
      description: 'description',
      additionalFields: [
        {
          name: CLIENT_CONFIG_STATUS_KEY,
          active: false
        }
      ]
    };

    const setSelectedCSPA = spyActions('setSelectedCSPA');
    const openStatusModal = spyActions('openStatusModal');
    const onChangeOpenViewElement = spyActions('onChangeOpenViewElement');
    const onChangeViewEntitlement = spyActions('onChangeViewEntitlement');

    const { wrapper } = renderMockStore(<Action data={CSPA} />, {});

    userEvent.click(wrapper.getByText(I18N_COMMON_TEXT.ACTIVATE));
    userEvent.click(wrapper.getByText('txt_view_entitlement'));

    expect(openStatusModal).toHaveBeenCalled();
    expect(onChangeOpenViewElement).toHaveBeenCalled();
    expect(setSelectedCSPA).toHaveBeenCalledWith(CSPA);
    expect(onChangeViewEntitlement).toHaveBeenCalledWith(CSPA);
    expect(wrapper.getByText('txt_view_entitlement')).toBeInTheDocument();
  });
});
