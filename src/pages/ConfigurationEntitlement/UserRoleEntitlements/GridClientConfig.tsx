import React, { useMemo, Fragment, useCallback, useEffect } from 'react';

// components
import {
  ColumnType,
  Grid,
  Pagination,
  SortType,
  TruncateText
} from 'app/_libraries/_dls/components';
import SearchConfig from './Search';
import ClearAndReset from 'pages/__commons/ClearAndReset';
import StatusColumn from './StatusColumn';
import Actions from './Actions';

// redux
import { clientConfigurationEntitlementActions } from './_redux/reducer';
import {
  selectCSPA,
  selectIsSearchNoData,
  selectSort,
  takeSelectedRole
} from './_redux/selector';
import { batch, useDispatch, useSelector } from 'react-redux';

// hooks
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { usePagination } from 'app/hooks';

// Const
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';

// Utils
import { genAmtId } from 'app/_libraries/_dls/utils';

const GridClientConfig: React.FC = () => {
  const { t } = useTranslation();
  const CSPA: CSPA[] = useSelector(selectCSPA);
  const isSearchNoData = useSelector(selectIsSearchNoData);
  const userSelected = useSelector(takeSelectedRole);

  const sortBy = useSelector(selectSort);

  const dispatch = useDispatch();

  const testId = 'userRoleEntitlements-gridClientConfig';

  const CLIENT_CONFIG_LIST_COLUMNS = useMemo(
    () => [
      {
        id: 'clientId',
        Header: t(I18N_COMMON_TEXT.CLIENT_ID),
        isSort: true,
        width: 120,
        accessor: 'clientId'
      },
      {
        id: 'systemId',
        Header: t(I18N_CLIENT_CONFIG.SYSTEM_ID),
        isSort: true,
        width: 120,
        accessor: 'systemId'
      },
      {
        id: 'principleId',
        Header: t(I18N_CLIENT_CONFIG.PRINCIPLE_ID),
        isSort: true,
        width: 120,
        accessor: 'principleId'
      },
      {
        id: 'agentId',
        Header: t(I18N_CLIENT_CONFIG.AGENT_ID),
        isSort: true,
        width: 120,
        accessor: 'agentId'
      },
      {
        id: 'description',
        Header: t(I18N_COMMON_TEXT.DESCRIPTION),
        width: 390,
        isSort: true,
        autoWidth: true,
        accessor: (data: CSPA) => (
          <TruncateText
            lines={2}
            title={data?.description}
            resizable
            ellipsisMoreText={t(I18N_COMMON_TEXT.MORE)}
            ellipsisLessText={t(I18N_COMMON_TEXT.LESS)}
            className="text-break"
          >
            {data?.description}
          </TruncateText>
        )
      },
      {
        id: 'status',
        Header: t(I18N_COMMON_TEXT.STATUS),
        isSort: true,
        width: 114,
        accessor: (data: CSPA) => <StatusColumn data={data} />
      },
      {
        id: 'action',
        Header: t(I18N_COMMON_TEXT.ACTIONS),
        cellBodyProps: { className: 'text-right td-sm' },
        cellHeaderProps: { className: 'text-center' },
        width: 222,
        isFixedRight: true,
        accessor: (data: CSPA) => <Actions data={data} />
      }
    ],
    [t]
  );

  const handleSort = (sort: SortType) => {
    dispatch(clientConfigurationEntitlementActions.onChangeSort(sort));
  };

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange,
    setCurrentPage,
    setCurrentPageSize
  } = usePagination(CSPA);

  const cleanSortSearch = useCallback(() => {
    batch(() => {
      dispatch(clientConfigurationEntitlementActions.onChangeTextSearch(''));
      dispatch(
        clientConfigurationEntitlementActions.onChangeSort({
          id: 'status',
          order: undefined
        })
      );
    });
  }, [dispatch]);

  const handleClearAndReset = useCallback(() => {
    cleanSortSearch();
    onPageSizeChange({ page: 1, size: 10 });
  }, [onPageSizeChange, cleanSortSearch]);

  useEffect(() => {
    cleanSortSearch();
    setCurrentPage(1);
    setCurrentPageSize(10);
  }, [setCurrentPage, setCurrentPageSize, cleanSortSearch, userSelected]);

  const showPagination = total > pageSize[0];

  return (
    <Fragment>
      <SearchConfig
        onClearAndReset={handleClearAndReset}
        onPageChange={onPageChange}
        currentPage={currentPage}
      />
      {isSearchNoData ? (
        <ClearAndReset
          className="mt-80"
          onClearAndReset={handleClearAndReset}
        />
      ) : (
        <Fragment>
          <Grid
            sortBy={[sortBy]}
            onSortChange={handleSort}
            dataItemKey={'id'}
            columns={CLIENT_CONFIG_LIST_COLUMNS as unknown as ColumnType[]}
            data={gridData}
            dataTestId={genAmtId(testId, 'grid', '')}
          />
          {showPagination && (
            <div className="pt-16">
              <Pagination
                totalItem={total}
                pageSize={pageSize}
                pageNumber={currentPage}
                pageSizeValue={currentPageSize}
                compact
                onChangePage={onPageChange}
                onChangePageSize={onPageSizeChange}
                dataTestId={genAmtId(testId, 'pagination', '')}
              />
            </div>
          )}
        </Fragment>
      )}
    </Fragment>
  );
};

export default GridClientConfig;
