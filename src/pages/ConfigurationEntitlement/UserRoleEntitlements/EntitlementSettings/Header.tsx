import React from 'react';

// components
import { Button, TreeViewValue } from 'app/_libraries/_dls/components';
import Search from './Search';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { clientConfigurationEntitlementActions as actions } from '../_redux/reducer';
import {
  selectEntitlementsFiltered,
  selectEntitlementsTextSearch,
  selectIsCollapseAll,
  selectIsSelectAll,
  selectSelectedCSPA,
  selectTotalSelectedEntitlement
} from '../_redux/selector';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

const Header: React.FC = () => {
  const { t } = useTranslation();

  const dispatch = useDispatch();
  const entitlementsFiltered: TreeViewValue[] = useSelector(
    selectEntitlementsFiltered
  );
  const entitlementsTextSearch = useSelector(selectEntitlementsTextSearch);
  const totalEntitlement: number = useSelector(selectTotalSelectedEntitlement);
  const isCollapseAll: boolean = useSelector(selectIsCollapseAll);
  const isSelectAll: boolean = useSelector(selectIsSelectAll);
  const CSPA: CSPA = useSelector(selectSelectedCSPA);

  const active = CSPA?.active;

  const isShownClearAndResetBtn = Boolean(
    entitlementsTextSearch && entitlementsFiltered.length
  );
  const isDisabledCollapseSelectBtn = Boolean(
    entitlementsTextSearch && !entitlementsFiltered.length
  );

  const testId = 'userRoleEntitlements-settings-header';

  const handleToggleCollapseAll = () => {
    dispatch(actions.onChangeCollapseAll({ entitlementsFiltered }));
  };

  const handleToggleSelectAll = () => {
    dispatch(actions.onChangeSelectAll({ entitlementsFiltered }));
  };

  const handleClearAndReset = () => {
    dispatch(actions.onChangeEntitlementsTextSearch(''));
  };

  return (
    <div className="px-24 pt-24">
      <div className="d-flex justify-content-between">
        <h5 data-testid={genAmtId(testId, 'title', '')}>
          {t('txt_entitlement')}
        </h5>
        <Search />
      </div>
      <div className="entitlement-settings-header mt-16 d-flex justify-content-between">
        <p data-testid={genAmtId(testId, 'select-item', '')}>
          {t('txt_select_the_item_grand_entitlement')}
        </p>
        {isShownClearAndResetBtn && (
          <Button
            onClick={handleClearAndReset}
            size="sm"
            variant="outline-primary"
            className="mr-n8"
            dataTestId={genAmtId(testId, 'reset-btn', '')}
          >
            {t('txt_clear_and_reset')}
          </Button>
        )}
      </div>
      <div className="d-flex align-items-center justify-content-between mt-16">
        {t('txt_total_selected_entitlement', { count: totalEntitlement })}
        <div className="mr-n8">
          <Button
            onClick={handleToggleCollapseAll}
            size="sm"
            variant="outline-primary"
            disabled={isDisabledCollapseSelectBtn}
            dataTestId={genAmtId(testId, 'expand-btn', '')}
          >
            {t(isCollapseAll ? 'txt_collapse_all' : 'txt_expand_all')}
          </Button>
          <Button
            onClick={handleToggleSelectAll}
            size="sm"
            variant="outline-primary"
            disabled={!active || isDisabledCollapseSelectBtn}
            dataTestId={genAmtId(testId, 'select-all-btn', '')}
          >
            {t(isSelectAll ? 'txt_select_all' : 'txt_deselect_all')}
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Header;
