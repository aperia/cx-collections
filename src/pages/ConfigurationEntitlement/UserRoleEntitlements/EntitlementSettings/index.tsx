import React, { useEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';

// components
import {
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TreeViewValue
} from 'app/_libraries/_dls/components';
import Snapshot from './Snapshot';
import Header from './Header';
import TreeViewEntitlements, {
  TreeViewEntitlementsRef
} from './TreeViewEntitlements';
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';

// redux
import {
  selectDirtyEntitlements,
  selectEntitlementsFiltered,
  selectLoadingModal
} from '../_redux/selector';
import { clientConfigurationEntitlementActions } from '../_redux/reducer';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';

// Const
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';

const EntitlementSettings: React.FC = () => {
  const { t } = useTranslation();

  const dispatch = useDispatch();
  const isLoading: boolean = useSelector(selectLoadingModal);
  const isDirtyEntitlements: boolean = useSelector(selectDirtyEntitlements);
  const entitlementsFiltered: TreeViewValue[] = useSelector(
    selectEntitlementsFiltered
  );

  // refs
  const treeViewEntitlementsRef = useRef<TreeViewEntitlementsRef>();

  // states
  const [show, setShow] = useState(true);

  const testId = 'userRoleEntitlements-settings';

  const handleChangeModal = () => {
    setShow(false);

    if (!treeViewEntitlementsRef.current?.isDirty) {
      return dispatch(
        clientConfigurationEntitlementActions.onChangeOpenModal()
      );
    }

    dispatch(
      confirmModalActions.open({
        title: 'txt_unsaved_changes',
        body: 'txt_discard_change_body',
        cancelText: 'txt_continue_editing',
        confirmText: 'txt_discard_change',
        cancelCallback: () => setShow(true),
        confirmCallback: () =>
          dispatch(clientConfigurationEntitlementActions.onChangeOpenModal())
      })
    );
  };

  const handleSave = () => {
    dispatch(clientConfigurationEntitlementActions.triggerUpdateEntitlement());
  };

  useEffect(() => {
    dispatch(
      clientConfigurationEntitlementActions.getUserRoleEntitlements({
        translateFn: t
      })
    );

    return () => {
      dispatch(clientConfigurationEntitlementActions.removeModalData());
    };
  }, [dispatch, t]);

  return (
    <Modal md loading={isLoading} show={show} dataTestId={testId}>
      <ModalHeader
        border
        closeButton
        onHide={handleChangeModal}
        dataTestId={genAmtId(testId, 'header', '')}
      >
        <ModalTitle dataTestId={genAmtId(testId, 'title', '')}>
          {t('txt_user_role_entitlement_settings')}
        </ModalTitle>
      </ModalHeader>
      <ModalBodyWithApiError
        noPadding
        apiErrorClassName="mb-24"
        storeId={API_ERROR_STORE_ID.ENTITLEMENT}
        dataTestId={genAmtId(testId, 'body', '')}
      >
        <Snapshot />
        <Header />
        <TreeViewEntitlements ref={treeViewEntitlementsRef as any} />
      </ModalBodyWithApiError>
      <ModalFooter
        onCancel={handleChangeModal}
        onOk={handleSave}
        cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
        okButtonText={t(I18N_COMMON_TEXT.SAVE)}
        dataTestId={genAmtId(testId, 'footer', '')}
        disabledOk={
          entitlementsFiltered.every(item => item.readOnly) ||
          !isDirtyEntitlements
        }
      />
    </Modal>
  );
};

export default EntitlementSettings;
