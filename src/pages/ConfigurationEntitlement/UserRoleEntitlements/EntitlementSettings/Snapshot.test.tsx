import React from 'react';
import { renderMockStore } from 'app/test-utils';
import { IConfigurationUserRoleEntitlement } from '../types';
import { screen } from '@testing-library/react';
import SnapshotEntitlementSettings from './Snapshot';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

describe('Test SnapshotEntitlementSettings', () => {
  it('Should have', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        selectedCSPA: {
          id: '1',
          agentId: 'agentId',
          systemId: 'systemId',
          clientId: 'clientId',
          description: 'description',
          principleId: 'principleId'
        },
        selectedUserRole: {
          description: 'UserRole'
        }
      } as IConfigurationUserRoleEntitlement
    };
    renderMockStore(<SnapshotEntitlementSettings />, { initialState });
    const queryText = screen.queryByText('txt_user_role:');
    const queryView = screen.queryByTestId('entitlementSettingsSnapshot');
    const queryValue = screen.queryByText(
      '{"id":"1","agentId":"agentId","systemId":"systemId","clientId":"clientId","description":"description","principleId":"principleId"}'
    );
    expect(queryValue).toBeInTheDocument();
    expect(queryText).toBeInTheDocument();
    expect(queryView).toBeInTheDocument();
  });
});
