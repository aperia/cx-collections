import React, { useEffect, useImperativeHandle } from 'react';

// components
import {
  Icon,
  TreeView,
  TreeViewOnChangeEvent,
  TreeViewValue
} from 'app/_libraries/_dls/components';
import { NoResultIcon } from 'app/components';

// redux
import {
  selectDirtyEntitlements,
  selectEntitlementsFiltered,
  selectEntitlementsTextSearch,
  selectLoadingModal
} from '../_redux/selector';
import { useDispatch, useSelector } from 'react-redux';
import { clientConfigurationEntitlementActions as actions } from '../_redux/reducer';

export interface TreeViewEntitlementsRef {
  isDirty: boolean;
}

export interface TreeViewEntitlementsProps {}

const TreeViewEntitlements: React.RefForwardingComponent<
  TreeViewEntitlementsRef,
  TreeViewEntitlementsProps
> = (_, ref) => {
  const divRef = React.useRef<HTMLDivElement | null>(null);

  // store
  const dispatch = useDispatch();
  const isLoadingModal: boolean = useSelector(selectLoadingModal);
  const isDirtyEntitlements: boolean = useSelector(selectDirtyEntitlements);
  const entitlementsFiltered: TreeViewValue[] = useSelector(
    selectEntitlementsFiltered
  );
  const entitlementsTextSearch: string = useSelector(
    selectEntitlementsTextSearch
  );

  const handleClearAndReset = () => {
    dispatch(actions.onChangeEntitlementsTextSearch(''));
  };

  const handleOnChange = (event: TreeViewOnChangeEvent) => {
    const entitlements = event.value as TreeViewValue[];
    dispatch(
      actions.onChangeEntitlements({
        entitlements
      })
    );
  };

  const noResultFound = () => {
    if (entitlementsFiltered.length || isLoadingModal) return null;
    return (
      <NoResultIcon
        icon={<Icon name="file" className="fs-80 color-light-l12 mt-80" />}
        description="txt_no_search_results_found"
        resetText="txt_clear_and_reset"
        onReset={handleClearAndReset}
        dataTestId='no-entitlements-found'
      />
    );
  };

  useImperativeHandle(
    ref,
    () => ({
      isDirty: isDirtyEntitlements
    }),
    [isDirtyEntitlements]
  );

  // always scroll top if entitlementsTextSearch changes
  useEffect(() => {
    divRef.current?.scrollTo({ top: 0, left: 0 });
  }, [entitlementsTextSearch]);

  return (
    <div
      className="tree-view-entitlements border br-light-l04 br-radius-8 mx-24 mt-16 overflow-y-auto"
      ref={divRef}
    >
      {noResultFound() || (
        <div className="p-24">
          <TreeView
            value={entitlementsFiltered}
            onChange={handleOnChange}
            highlightText={entitlementsTextSearch}
            variant="checkbox"
            dataTestId="treeViewEntitlements"
          />
        </div>
      )}
    </div>
  );
};

export default React.forwardRef<
  TreeViewEntitlementsRef,
  TreeViewEntitlementsProps
>(TreeViewEntitlements);
