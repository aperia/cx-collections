import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { clientConfigurationEntitlementActions } from '../_redux/reducer';
import { UserRoleEntitlementState } from '../types';
import { fireEvent, screen } from '@testing-library/react';
import Search from './Search';

HTMLCanvasElement.prototype.getContext = jest.fn();

const spyActions = mockActionCreator(clientConfigurationEntitlementActions);

describe('Test Header', () => {
  it('Should render input search with value', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        entitlementsTextSearch: 'mock search'
      } as UserRoleEntitlementState
    };
    const onChangeEntitlementsTextSearch = spyActions(
      'onChangeEntitlementsTextSearch'
    );

    const { wrapper } = renderMockStore(<Search />, { initialState });
    const input = wrapper.baseElement.querySelector(
      '.dls-text-box > input'
    ) as HTMLInputElement;

    expect(input.value).toEqual('mock search');

    fireEvent.change(input, { target: { value: 'update value' } });

    const inputBtn = wrapper.baseElement.querySelector(
      '.dls-text-search-icon > button'
    ) as HTMLButtonElement;

    fireEvent.click(inputBtn);

    expect(onChangeEntitlementsTextSearch).toHaveBeenCalledWith('update value');

    const clearDiv = screen.getByTestId(
      'search-entitlement_dls-text-search_clear-btn'
    ) as HTMLDivElement;

    fireEvent.click(clearDiv);

    expect(input.value).toEqual('');
  });
});
