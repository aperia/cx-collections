import React, { useEffect, useState } from 'react';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { TextSearch, TextSearchProps } from 'app/_libraries/_dls/components';
import { useDispatch, useSelector } from 'react-redux';

// Redux
import { selectEntitlementsTextSearch } from '../_redux/selector';
import { clientConfigurationEntitlementActions as actions } from '../_redux/reducer';

const Search: React.FC = () => {
  const { t } = useTranslation();

  const dispatch = useDispatch();
  const entitlementsTextSearch: string = useSelector(
    selectEntitlementsTextSearch
  );

  const [value, setValue] = useState(entitlementsTextSearch);

  const handleOnChange: TextSearchProps['onChange'] = event => {
    setValue(event.target.value);
  };

  const handleClearSearch: TextSearchProps['onClear'] = () => {
    setValue('');
  };

  const handleSearch: TextSearchProps['onSearch'] = nextValue => {
    dispatch(actions.onChangeEntitlementsTextSearch(nextValue));
  };

  useEffect(() => {
    setValue(entitlementsTextSearch);
  }, [entitlementsTextSearch]);

  return (
    <TextSearch
      value={value || ''}
      onChange={handleOnChange}
      onClear={handleClearSearch}
      onSearch={handleSearch}
      placeholder={t('txt_type_to_search')}
      dataTestId="search-entitlement"
    />
  );
};

export default Search;
