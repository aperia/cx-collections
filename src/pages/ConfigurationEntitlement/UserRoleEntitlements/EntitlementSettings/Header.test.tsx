import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { clientConfigurationEntitlementActions } from '../_redux/reducer';
import { screen } from '@testing-library/react';
import Header from './Header';
import { TreeViewValue } from 'app/_libraries/_dls/components';
import { UserRoleEntitlementState } from '../types';

HTMLCanvasElement.prototype.getContext = jest.fn();

const spyActions = mockActionCreator(clientConfigurationEntitlementActions);

describe('Test Header', () => {
  const initialState: Partial<RootState> = {
    configurationUserRoleEntitlement: {
      defaultEntitlements: [] as TreeViewValue[],
      selectedCSPA: {
        active: true,
        agentId: '3000',
        clientId: '8997',
        cspaId: '8997-1286-2000-3000',
        description: 'Test data',
        id: '8997-1286-2000-3000',
        isDefaultClientInfoKey: false,
        principleId: '2000'
      },
      entitlements: [
        {
          id: 'AccountSnapshot',
          label: 'Account Snapshot',
          expanded: true,
          checked: true,
          readOnly: false,
          items: [
            {
              id: 'ViewAccountSnapshot',
              label: 'View',
              checked: true,
              readOnly: false
            },
            {
              id: 'EditExternalStatusCode',
              label: 'Edit External Status Code',
              readOnly: false
            },
            {
              id: 'EditExternalStatusReasonCode',
              label: 'Edit External Status Reason Code',
              checked: true,
              readOnly: false
            }
          ]
        }
      ],
      entitlementsTextSearch: 'Edit'
    } as UserRoleEntitlementState
  };

  it('Should have txt_collapse_all and txt_deselect_all', () => {
    const onChangeCollapseAll = spyActions('onChangeCollapseAll');
    const handleExpandAll = spyActions('onChangeSelectAll');
    renderMockStore(<Header />, { initialState });
    const queryCollapse = screen.queryByText('txt_collapse_all');
    const querySelect = screen.queryByText('txt_deselect_all');
    queryCollapse?.click();
    querySelect?.click();
    expect(onChangeCollapseAll).toHaveBeenCalledWith({
      entitlementsFiltered:
        initialState.configurationUserRoleEntitlement!.entitlements
    });
    expect(handleExpandAll).toHaveBeenCalled();
  });

  it('Should render clear search btn', () => {
    const onChangeEntitlementsTextSearch = spyActions(
      'onChangeEntitlementsTextSearch'
    );
    renderMockStore(<Header />, { initialState });
    const btnClear = screen.queryByText('txt_clear_and_reset');
    btnClear?.click();
    expect(onChangeEntitlementsTextSearch).toHaveBeenCalled();
  });

  it('Should have txt_expand_all and txt_select_all with readonly false', () => {
    const onChangeCollapseAll = spyActions('onChangeCollapseAll');
    const handleExpandAll = spyActions('onChangeSelectAll');

    renderMockStore(<Header />, {
      initialState: {
        configurationUserRoleEntitlement: {
          selectedCSPA: {
            active: true,
            agentId: '3000',
            clientId: '8997',
            cspaId: '8997-1286-2000-3000',
            description: 'Test data',
            id: '8997-1286-2000-3000',
            isDefaultClientInfoKey: false,
            principleId: '2000'
          },
          entitlements: [
            {
              id: 'AccountSnapshot',
              label: 'Account Snapshot',
              expanded: false,
              checked: false,
              items: [
                {
                  id: 'AccountSnapshot.View',
                  label: 'View',
                  readOnly: false,
                  checked: false
                }
              ],
              readOnly: false
            }
          ] as TreeViewValue[]
        } as UserRoleEntitlementState
      }
    });
    const queryCollapse = screen.queryByText('txt_expand_all');
    const querySelect = screen.queryByText('txt_select_all');
    queryCollapse?.click();
    querySelect?.click();
    expect(onChangeCollapseAll).toHaveBeenCalled();
    expect(handleExpandAll).toHaveBeenCalled();
  });

  it('Should have txt_expand_all and txt_select_all with readOnly true', () => {
    const onChangeCollapseAll = spyActions('onChangeCollapseAll');
    const handleExpandAll = spyActions('onChangeSelectAll');

    renderMockStore(<Header />, {
      initialState: {
        configurationUserRoleEntitlement: {
          selectedCSPA: {
            active: false,
            agentId: '3000',
            clientId: '8997',
            cspaId: '8997-1286-2000-3000',
            description: 'Test data',
            id: '8997-1286-2000-3000',
            isDefaultClientInfoKey: false,
            principleId: '2000'
          },
          entitlements: [
            {
              id: 'AccountSnapshot',
              label: 'Account Snapshot',
              expanded: false,
              checked: false,
              items: [],
              readOnly: true
            }
          ] as TreeViewValue[]
        } as UserRoleEntitlementState
      }
    });
    const queryCollapse = screen.queryByText('txt_expand_all');
    const querySelect = screen.queryByText('txt_select_all');
    queryCollapse?.click();
    querySelect?.click();
    expect(onChangeCollapseAll).toHaveBeenCalled();
    expect(handleExpandAll).not.toHaveBeenCalled();
  });

  it('Should call handleCleanAndReset', () => {
    const onChangeEntitlementsTextSearch = spyActions(
      'onChangeEntitlementsTextSearch'
    );

    renderMockStore(<Header />, { initialState });
    const queryClearAndReset = screen.queryByText('txt_clear_and_reset');
    queryClearAndReset?.click();
    expect(onChangeEntitlementsTextSearch).toHaveBeenCalledWith('');
  });
});
