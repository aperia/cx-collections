import React from 'react';
import {
  mockActionCreator,
  queryAllByClass,
  renderMockStore
} from 'app/test-utils';
import { clientConfigurationEntitlementActions } from '../_redux/reducer';
import { UserRoleEntitlementState } from '../types';
import { screen } from '@testing-library/react';
import EntitlementSettings from './';
import { TreeViewValue } from 'app/_libraries/_dls/components';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';

HTMLElement.prototype.scrollTo = jest.fn();

HTMLCanvasElement.prototype.getContext = jest.fn();

const spyActions = mockActionCreator(clientConfigurationEntitlementActions);

describe('Test EntitlementSettings', () => {
  it('Should have clicked cancel', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        isOpenModal: true,
        entitlements: [] as TreeViewValue[],
        defaultEntitlements: [] as TreeViewValue[]
      } as UserRoleEntitlementState
    };
    const onChangeOpenModal = spyActions('onChangeOpenModal');

    renderMockStore(<EntitlementSettings />, {
      initialState
    });

    const queryText = screen.queryByText('txt_cancel');
    queryText?.click();
    expect(onChangeOpenModal).toHaveBeenCalled();
  });

  it('Should have clicked icon close', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        isOpenModal: true,
        entitlements: [] as TreeViewValue[],
        defaultEntitlements: [] as TreeViewValue[]
      } as UserRoleEntitlementState
    };
    const onChangeOpenModal = spyActions('onChangeOpenModal');

    const { baseElement } = renderMockStore(<EntitlementSettings />, {
      initialState
    });

    const queryIconClose = queryAllByClass(baseElement, /icon icon-close/)[0];
    queryIconClose?.click();

    expect(onChangeOpenModal).toHaveBeenCalled();
  });

  it('Should have save button', () => {
    const entitlements: TreeViewValue[] = [
      {
        id: 'id',
        label: 'label',
        indeterminate: false,
        expanded: false,
        checked: true
      }
    ];

    const defaultEntitlements: TreeViewValue[] = [
      {
        id: 'default-id',
        label: 'default-label',
        indeterminate: false,
        expanded: false,
        checked: true,
        items: [
          {
            id: 'default-id-1',
            label: 'default-label-1',
            indeterminate: false,
            expanded: false,
            checked: true
          }
        ]
      }
    ];
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        isOpenModal: true,
        entitlements,
        defaultEntitlements
      } as UserRoleEntitlementState
    };

    renderMockStore(<EntitlementSettings />, {
      initialState
    });
    const queryText = screen.queryByText(I18N_COMMON_TEXT.SAVE);
    queryText?.click();
    expect(queryText).toBeInTheDocument();
  });

  it('Should have clicked cancel in confirm modal', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        isDirty: true,
        isOpenModal: true,
        entitlements: [
          {
            id: 'id',
            checked: true
          }
        ],
        defaultEntitlements: [] as TreeViewValue[]
      } as unknown as UserRoleEntitlementState
    };

    const cancelCallbackAction = jest.fn(x => {
      if (x.cancelCallback) {
        x.cancelCallback();
      }
      return { type: 'ok' };
    });

    jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(cancelCallbackAction as never);

    renderMockStore(<EntitlementSettings />, {
      initialState
    });

    const modalCancelButton = screen.getByText(I18N_COMMON_TEXT.CANCEL);
    modalCancelButton?.click();
    expect(
      screen.getByText('txt_user_role_entitlement_settings')
    ).toBeInTheDocument();
  });

  it('Should have clicked confirm in confirm modal', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        isDirty: true,
        isOpenModal: true,
        entitlements: [
          {
            id: 'id',
            checked: true
          }
        ],
        defaultEntitlements: [] as TreeViewValue[]
      } as unknown as UserRoleEntitlementState
    };

    const onChangeOpenModal = spyActions('onChangeOpenModal');

    const confirmCallbackAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });

    jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(confirmCallbackAction as never);

    renderMockStore(<EntitlementSettings />, {
      initialState
    });

    const modalCancelButton = screen.getByText(I18N_COMMON_TEXT.CANCEL);
    modalCancelButton?.click();
    expect(onChangeOpenModal).toHaveBeenCalled();
  });
});
