import React from 'react';

// redux
import { takeSelectedRole, selectSelectedCSPA } from '../_redux/selector';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSelector } from 'react-redux';

// types
import { View } from 'app/_libraries/_dof/core';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { InlineMessage } from 'app/_libraries/_dls/components';

const SnapshotEntitlementSettings: React.FC = () => {
  const { t } = useTranslation();
  const CSPA: CSPA = useSelector(selectSelectedCSPA);
  const userRole: RefDataValue = useSelector(takeSelectedRole);
  const active = CSPA?.active;

  return (
    <div className="bg-light-l20 px-24 border-bottom">
      {!active && (
        <InlineMessage
          id="user_role_entitlement-information__inactive-message"
          className="mt-16 mr-8 mb-8"
          dataTestId="topInformation_inactive-message"
          variant="warning"
        >
          {t('txt_contact_entitlement_inactive_message')}
        </InlineMessage>
      )}
      <div className="pt-16">
        <span
          data-testid={genAmtId(
            'userRoleEntitlements-settings',
            'user-role',
            ''
          )}
        >
          {t('txt_user_role')}:{' '}
        </span>
        <span
          className="fw-500"
          data-testid={genAmtId(
            'userRoleEntitlements-settings',
            'description',
            ''
          )}
        >
          {userRole?.description}
        </span>
      </div>
      <div className="pb-24">
        <View
          id={`entitlementSettingsSnapshot`}
          formKey={`entitlementSettingsSnapshot`}
          descriptor={'entitlementSettingsSnapshot'}
          value={{ ...CSPA }}
        />
      </div>
    </div>
  );
};

export default SnapshotEntitlementSettings;
