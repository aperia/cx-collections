import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { clientConfigurationEntitlementActions } from '../_redux/reducer';
import { UserRoleEntitlementState } from '../types';
import { screen } from '@testing-library/react';
import TreeViewEntitlements from './TreeViewEntitlements';
import { TreeViewValue } from 'app/_libraries/_dls/components';

HTMLCanvasElement.prototype.getContext = jest.fn();

HTMLElement.prototype.scrollTo = jest.fn();

const spyActions = mockActionCreator(clientConfigurationEntitlementActions);

Object.defineProperty(window.Element.prototype, 'scrollTo', {
  writable: true,
  value: jest.fn()
});

describe('Test TreeViewEntitlements', () => {
  it('Should have onChange parent of node', () => {
    const spy = spyActions('onChangeEntitlements');
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        defaultEntitlements: [] as TreeViewValue[],
        entitlements: [
          {
            id: 'AccountSnapshot',
            label: 'Account Snapshot',
            expanded: true,
            items: [{ id: 'ViewAccountSnapshot', label: 'View' }]
          }
        ]
      } as UserRoleEntitlementState
    };
    renderMockStore(<TreeViewEntitlements />, { initialState });
    const queryText = screen.queryByText('Account Snapshot');
    queryText!.click();
    expect(spy).toHaveBeenCalledWith({
      entitlements: [
        {
          id: 'AccountSnapshot',
          indeterminate: undefined,
          label: 'Account Snapshot',
          expanded: true,
          checked: true,
          items: [
            {
              id: 'ViewAccountSnapshot',
              indeterminate: undefined,
              label: 'View',
              checked: true
            }
          ],
          path: ['0']
        }
      ]
    });
  });

  it('Should have onChange children of node', () => {
    const spy = spyActions('onChangeEntitlements');
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        defaultEntitlements: [] as TreeViewValue[],
        entitlements: [
          {
            id: 'AccountSnapshot',
            label: 'Account Snapshot',
            expanded: true,
            items: [{ id: 'ViewAccountSnapshot', label: 'View' }]
          }
        ]
      } as UserRoleEntitlementState
    };
    renderMockStore(<TreeViewEntitlements />, { initialState });
    const queryText = screen.queryByText('View');
    queryText!.click();
    expect(spy).toHaveBeenCalledWith({
      entitlements: [
        {
          id: 'AccountSnapshot',
          label: 'Account Snapshot',
          expanded: true,
          checked: true,
          indeterminate: false,
          items: [
            {
              id: 'ViewAccountSnapshot',
              label: 'View',
              checked: true,
              indeterminate: undefined,
              expanded: false,
              path: ['0', 'items', '0']
            }
          ]
        }
      ]
    });
  });

  it('Should dispatch onChangeEntitlementsTextSearch', () => {
    const spy = spyActions('onChangeEntitlementsTextSearch');

    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        entitlements: [] as TreeViewValue[],
        defaultEntitlements: [] as TreeViewValue[]
      } as UserRoleEntitlementState
    };

    const { baseElement } = renderMockStore(<TreeViewEntitlements />, {
      initialState
    });

    const clearButton = baseElement.querySelector('#resetText') as HTMLElement;

    clearButton?.click();

    expect(spy).toHaveBeenCalledWith('');
  });
});
