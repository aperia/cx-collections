import React from 'react';
import { screen } from '@testing-library/dom';

import {
  mockActionCreator,
  queryByClass,
  renderMockStore
} from 'app/test-utils';

import DropdownUserRole from './DropdownUserRole';

import { clientConfigurationEntitlementActions } from './_redux/reducer';
import userEvent from '@testing-library/user-event';

const initialState: Partial<RootState> = {
  configurationUserRoleEntitlement: {
    userRoles: [{ value: 'CXCOL1', description: 'CXCOL1' }]
  } as any
};

const spyActions = mockActionCreator(clientConfigurationEntitlementActions);
HTMLCanvasElement.prototype.getContext = jest.fn();
describe('Test DropdownUserRole', () => {
  it('Should render ui', () => {
    const spy = spyActions('onChangeUserRole');
    const { container } = renderMockStore(<DropdownUserRole />, {
      initialState
    });
    const queryInput = queryByClass(container, /text-field-container/);
    userEvent.click(queryInput!);
    const queryItem = screen.queryByText('CXCOL1');
    queryItem!.click();
    expect(spy).toHaveBeenCalled();
  });
});
