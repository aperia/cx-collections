import { FailedApiReload } from 'app/components';
// components
import { SimpleBar } from 'app/_libraries/_dls/components';
// i18
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { genAmtId } from 'app/_libraries/_dls/utils';
import classNames from 'classnames';
import { isEmpty } from 'lodash';
import React, { Fragment, useEffect, useLayoutEffect, useRef } from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';
import AddCSPA from '../Components/AddCSPA';
import NoData from '../Components/NoData';
import AddCSPAConfig from './AddCSPAConfig';
import DropdownUserRole from './DropdownUserRole';
import EntitlementSettings from './EntitlementSettings';
import GridClientConfig from './GridClientConfig';
import StatusModal from './StatusModal';
// redux
import { clientConfigurationEntitlementActions } from './_redux/reducer';
import {
  takeDefaultCSPA,
  takeListCSPAErrorStatus,
  takeLoadingListCSPAStatus,
  takeNoResultStatus,
  takeOpenModalStatus,
  takeSelectedRole
} from './_redux/selector';

const UserRoleEntitlement: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const isOpenSettingModal =
    useSelector<RootState, boolean>(takeOpenModalStatus);
  const isError = useSelector<RootState, boolean>(takeListCSPAErrorStatus);

  const loading = useSelector<RootState, boolean>(takeLoadingListCSPAStatus);
  const divRef = useRef<HTMLDivElement | null>(null);
  const isNoData = useSelector<RootState, boolean>(takeNoResultStatus);
  const selectedRole = useSelector(takeSelectedRole);
  const defaultCSPA = useSelector(takeDefaultCSPA);

  const testId = 'userRoleEntitlements';

  useEffect(() => {
    batch(() => {
      dispatch(clientConfigurationEntitlementActions.getUserRoles());
    });
    return () => {
      dispatch(clientConfigurationEntitlementActions.onRemoveTabEntitlements());
    };
  }, [dispatch]);

  const handleOnAddCSPA = () => {
    dispatch(clientConfigurationEntitlementActions.onOpenModalAddCSPA());
  };

  const renderLayout = () => {
    if (isEmpty(selectedRole)) return null;
    if (isError)
      return (
        <FailedApiReload
          id="client-config-list-failed"
          className="mt-80"
          onReload={() =>
            dispatch(
              clientConfigurationEntitlementActions.getListCSPABasedRole()
            )
          }
          dataTestId={genAmtId(testId, 'failed-api', '')}
        />
      );

    if (isNoData && !loading)
      return (
        <Fragment>
          <div className="mt-24 mb-16 d-flex justify-content-between">
            <h5 data-testid={genAmtId(testId, 'cspa-list', '')}>
              {t('txt_cspa_list')}{' '}
            </h5>
          </div>
          <NoData>
            <AddCSPA
              onAddCSPA={handleOnAddCSPA}
              disabled={!defaultCSPA?.isDefaultClientInfoKey}
            />
          </NoData>
        </Fragment>
      );

    return (
      <Fragment>
        <GridClientConfig />
      </Fragment>
    );
  };

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100vh - 112px)`);
  }, []);

  return (
    <Fragment>
      <div ref={divRef}>
        <SimpleBar className={classNames({ loading })}>
          <div className="p-24 max-width-lg mx-auto">
            <h3 data-testid={genAmtId(testId, 'title', '')}>
              {t('txt_user_role_entitlement')}
            </h3>
            <DropdownUserRole />
            {renderLayout()}
          </div>
        </SimpleBar>
      </div>
      {isOpenSettingModal && <EntitlementSettings />}
      <AddCSPAConfig />
      <StatusModal />
    </Fragment>
  );
};

export default UserRoleEntitlement;
