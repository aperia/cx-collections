import React from 'react';

// components
import {
  DropdownBaseChangeEvent,
  DropdownList
} from 'app/_libraries/_dls/components';

// redux
import { clientConfigurationEntitlementActions } from './_redux/reducer';
import { batch, useDispatch, useSelector } from 'react-redux';

// i18n
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { selectUserRoles } from './_redux/selector';
import { genAmtId } from 'app/_libraries/_dls/utils';

const DropdownUserRole: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const userRoles: RefDataValue[] = useSelector(selectUserRoles);

  const onChangeUserRole = (event: DropdownBaseChangeEvent) => {
    const { value } = event.target;
    batch(() => {
      dispatch(clientConfigurationEntitlementActions.onChangeUserRole(value));
      dispatch(clientConfigurationEntitlementActions.getListCSPABasedRole());
    });
  };

  return (
    <React.Fragment>
      <p
        className="mt-24"
        data-testid={genAmtId('userRoleEntitlements', 'select-a-role', '')}
      >
        {t('txt_select_a_role')}
      </p>
      <div className="w-300px mt-24">
        <DropdownList
          onChange={onChangeUserRole}
          textField="description"
          required
          label={t('txt_user_role')}
          dataTestId="userRoleEntitlements_user-role"
          noResult={t('txt_no_results_found')}
        >
          {userRoles.map(item => (
            <DropdownList.Item
              key={item.value}
              label={item.description}
              value={item}
            />
          ))}
        </DropdownList>
      </div>
    </React.Fragment>
  );
};

export default DropdownUserRole;
