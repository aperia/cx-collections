import React from 'react';
import { Badge } from 'app/_libraries/_dls/components';

// Const
import { ACTIVE, INACTIVE } from 'pages/ClientConfiguration/constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface ActionsProps {
  data: CSPA;
}
const StatusColumn: React.FC<ActionsProps> = ({ data }) => {
  const { t } = useTranslation();
  const isActive = data?.active;
  return (
    <Badge color={isActive ? 'green' : 'grey'} textTransformNone>
      {t(isActive ? ACTIVE : INACTIVE)}
    </Badge>
  );
};

export default StatusColumn;
