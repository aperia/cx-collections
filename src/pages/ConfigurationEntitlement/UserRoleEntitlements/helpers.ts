import { TreeViewValue } from 'app/_libraries/_dls/components';
import { isUndefined } from 'lodash';

// Type
import {
  APIComponentRoleEntitlement,
  APIRoleEntitlement,
  AppRoleCSPA,
  AppRoleEntitlement,
  RolePermissions
} from './types';

// Const
import {
  COMPONENTS_MAP,
  ENTITLEMENT_COMPONENTS,
  PERMISSIONS
} from 'app/entitlements/constants';

// Dictionary
import {
  APIComponentDictionary,
  AppComponentDictionary,
  MultipleLangDictionary
} from 'app/entitlements/dictionary';

// Helper
import masterComponentStructure from 'app/entitlements/MasterStructure/masterComponentStruture';
import { buildEntitlementCode } from 'app/entitlements';
import { buildListDefaultSort, checkDefaultCSPA } from '../helpers';

/**
 * Convert data from API response to Application entitlement data
 * @param componentEntitlement
 * @returns
 */
export const covertAPIDataToAppRoleEntitlement = (
  componentEntitlement?: Array<APIComponentRoleEntitlement>
): Array<EntitlementConfig> => {
  return masterComponentStructure.map(item => {
    const { component, entitlements } = item;

    const { entitlements: currentEntitlementValues = {} } =
      componentEntitlement?.find(
        i =>
          AppComponentDictionary.get(i.component as COMPONENTS_MAP) ===
          component
      ) || {};

    return {
      component,
      entitlements: {
        items: entitlements.items.reduce(
          (newArr: Array<EntitlementItem>, entitlement) => {
            const { entitlementCode } = entitlement;

            const { active: currentActive = false } = Object.keys(
              currentEntitlementValues
            ).reduce((newObject: EntitlementItem, ky) => {
              const buildKy = buildEntitlementCode(component, ky);

              if (isUndefined(buildKy)) return newObject;

              if (entitlement.entitlementCode === buildKy) {
                newObject = {
                  entitlementCode: buildKy,
                  active: currentEntitlementValues[ky]
                };
              }

              return newObject;
            }, {});

            newArr = [
              ...newArr,
              {
                active: currentActive,
                entitlementCode
              }
            ];

            return newArr;
          },
          []
        )
      }
    };
  });
};

/**
 * Convert Application data to API data format
 * @param componentEntitlement
 * @returns
 */
export const covertAppRoleDataToAPI = (
  componentEntitlement?: Array<EntitlementConfig>
) => {
  return componentEntitlement?.reduce(
    (newArray: Array<APIComponentRoleEntitlement>, item) => {
      const { component = '', entitlements } = item;

      newArray = [
        ...newArray,
        {
          component: APIComponentDictionary.get(
            component as ENTITLEMENT_COMPONENTS
          ),
          entitlements: entitlements?.items?.reduce(
            (newEntitlements: RolePermissions, entitlement) => {
              const { entitlementCode, active } = entitlement;
              const [, rawPermission = ''] = entitlementCode?.split('.') || [];

              const permissionKy =
                rawPermission.charAt(0).toLocaleLowerCase() +
                rawPermission.slice(1);

              newEntitlements = {
                ...newEntitlements,
                [permissionKy]: active
              };
              return newEntitlements;
            },
            {}
          )
        }
      ];
      return newArray;
    },
    []
  );
};

/**
 * Build list role entitlement which is used in the application
 * @param roles
 * @returns
 */
export const buildListRoleEntitlements = (roles: Array<APIRoleEntitlement>) => {
  return roles.reduce((newRolesData: Array<AppRoleEntitlement>, item) => {
    const { cspas, role } = item;

    const convertCSPAData = cspas?.reduce(
      (newCSPAData: Array<AppRoleCSPA>, cspaData) => {
        const { cspa, components } = cspaData;

        newCSPAData = [
          ...newCSPAData,
          {
            components: covertAPIDataToAppRoleEntitlement(components),
            cspa
          }
        ];

        return newCSPAData;
      },
      []
    );

    newRolesData = [
      ...newRolesData,
      {
        role,
        cspas: convertCSPAData
      }
    ];
    return newRolesData;
  }, []);
};

/**
 * Convert entitlement
 */

/**
 * Covert Application entitlement to tree view data
 * @param entitlements
 * @param translateFn
 * @returns
 */
export const convertRoleEntitlementToTreeView = (
  entitlements: Array<EntitlementConfig>,
  currentCSPA: CSPA | undefined,
  translateFn: Function
) => {
  const treeViewValue = entitlements.map(item => {
    const {
      component = '' as ENTITLEMENT_COMPONENTS,
      entitlements: childEntitlements = {}
    } = item;

    const items =
      childEntitlements.items?.map(childItem => {
        return {
          id: childItem.entitlementCode,
          checked: childItem.active,
          readOnly: !currentCSPA?.active,
          label: translateFn(
            MultipleLangDictionary.get(childItem.entitlementCode as PERMISSIONS)
          )
        };
      }) || [];
    const itemsChecked = items.filter(_ => _.checked);
    const isParentChecked = itemsChecked.length === items.length;
    const isParentIndeterminate =
      itemsChecked.length && itemsChecked.length < items.length;

    return {
      id: component,
      label: translateFn(MultipleLangDictionary.get(component || '')),
      expanded: true,
      checked: isParentChecked,
      readOnly: !currentCSPA?.active,
      indeterminate: isParentChecked ? undefined : isParentIndeterminate,
      items
    };
  });

  return treeViewValue as TreeViewValue[];
};

/**
 * Convert tree view data to Application data
 * @param treeView
 * @returns
 */
export const convertTreeViewToRoleEntitlement = (
  treeView: Array<TreeViewValue>
) => {
  return treeView.reduce(
    (
      newEntitlements: Array<EntitlementConfig>,
      treeViewItem: TreeViewValue
    ) => {
      const { id = '', items } = treeViewItem;
      let listItems: Array<EntitlementItem> = [];

      items?.forEach(item => {
        const { checked = false, id: childId = '' } = item;
        listItems = [
          ...listItems,
          {
            active: checked,
            entitlementCode: childId as PERMISSIONS
          }
        ];
      });

      newEntitlements = [
        ...newEntitlements,
        {
          component: id as ENTITLEMENT_COMPONENTS,
          entitlements: {
            items: listItems
          }
        }
      ];

      return newEntitlements;
    },
    []
  );
};

/**
 * Build all entitlement from App data to API data format
 * @param selectedUserRole
 * @param selectedCSPA,
 * @param entitlements,
 * @returns
 */
export const buildUpdateRoleEntitlement = (
  selectedUserRole: RefDataValue,
  selectedCSPA: CSPA,
  entitlements: TreeViewValue[]
): Array<APIRoleEntitlement> => {
  const { value: role } = selectedUserRole;
  const { cspaId, active, description } = selectedCSPA;
  return [
    {
      role,
      cspas: [
        {
          cspa: cspaId,
          components: covertAppRoleDataToAPI(
            convertTreeViewToRoleEntitlement(entitlements)
          ),
          active,
          description
        }
      ]
    }
  ];
};

export const buildUserRoleCSPA = (
  roleEntitlements: Array<APIRoleEntitlement>,
  selectedUserRole: RefDataValue
) => {
  const selectedRoleCSPA = roleEntitlements.find(
    item => item.role === selectedUserRole.value
  );

  const { cspas = [] } = selectedRoleCSPA || {};

  const listCSPA = cspas.map(item => {
    const { cspa = '', active, description } = item;
    const [clientId, systemId, principleId, agentId] = cspa?.split('-');
    const id = `${clientId}-${systemId}-${principleId}-${agentId}`;
    const isDefaultClientInfoKey = checkDefaultCSPA(id);

    return {
      active,
      agentId,
      clientId,
      cspaId: id,
      id,
      description,
      principleId,
      systemId,
      isDefaultClientInfoKey
    } as CSPA;
  });

  return buildListDefaultSort(listCSPA);
};
