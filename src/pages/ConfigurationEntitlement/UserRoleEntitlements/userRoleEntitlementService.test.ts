// services
import userRoleEntitlementService from './userRoleEntitlementService';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

describe('clientConfigurationServices', () => {
  it('getEntitlement', () => {
    mockAppConfigApi.setup();

    const mockService = mockApiServices.post();

    userRoleEntitlementService.getEntitlement('id');

    expect(mockService).toBeCalledWith(
      apiUrl.userRoleEntitlement.getEntitlement,
      'id'
    );
  });

  it('getEntitlement have empty', () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: {
        api: {
          userRoleEntitlement: null
        }
      }
    });

    const mockService = mockApiServices.post();

    userRoleEntitlementService.getEntitlement('id');

    expect(mockService).toBeCalledWith('', 'id');
  });

  it('updateUserRoleEntitlement', () => {
    mockAppConfigApi.setup();

    const mockService = mockApiServices.post();
    const postData = {
      id: 'id',
      entitlements: [
        {
          id: 'AccountSnapshot',
          label: 'Account Snapshot',
          expanded: true,
          items: [{ id: 'ViewAccountSnapshot', label: 'View' }]
        }
      ]
    };
    userRoleEntitlementService.updateUserRoleEntitlement(postData);

    expect(mockService).toBeCalledWith(
      apiUrl.userRoleEntitlement.updateUserRoleEntitlement,
      postData
    );
  });

  it('updateUserRoleEntitlement have empty', () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: {
        api: {
          userRoleEntitlement: null
        }
      }
    });

    const mockService = mockApiServices.post();
    const postData = {
      id: 'id',
      entitlements: [
        {
          id: 'AccountSnapshot',
          label: 'Account Snapshot',
          expanded: true,
          items: [{ id: 'ViewAccountSnapshot', label: 'View' }]
        }
      ]
    };
    userRoleEntitlementService.updateUserRoleEntitlement(postData);

    expect(mockService).toBeCalledWith('', postData);
  });

  it('getEntitlementBasedOnRoleCSPA', () => {
    mockAppConfigApi.setup();

    const mockService = mockApiServices.post();
    const postData = {
      common: {
        agent: '3000',
        clientNumber: '3000',
        system: '3000',
        prin: '3000',
        user: '3000',
        app: '3000'
      }
    };
    userRoleEntitlementService.getEntitlementBasedOnRoleCSPA(postData);

    expect(mockService).toBeCalledWith(
      apiUrl.userRoleEntitlement.getEntitlement,
      postData
    );
  });

  it('getEntitlementBasedOnRoleCSPA have empty', () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: {
        api: {
          userRoleEntitlement: null
        }
      }
    });

    const mockService = mockApiServices.post();
    const postData = {
      common: {
        agent: '3000',
        clientNumber: '3000',
        system: '3000',
        prin: '3000',
        user: '3000',
        app: '3000'
      }
    };
    userRoleEntitlementService.getEntitlementBasedOnRoleCSPA(postData);

    expect(mockService).toBeCalledWith('', postData);
  });

  it('addUserRoleCSPA', () => {
    const mockService = mockApiServices.post();

    userRoleEntitlementService.addUserRoleCSPA({});

    expect(mockService).toBeCalledWith('', {});
  });

  it('updateUserRoleCSPA', () => {
    const mockService = mockApiServices.post();

    userRoleEntitlementService.updateUserRoleCSPA({});

    expect(mockService).toBeCalledWith('', {});
  });
});
