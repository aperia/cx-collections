import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

// Hooks
import { useStoreIdSelector } from 'app/hooks';
import {
  selectDisplayClearAndReset,
  selectTextSearch,
  takeDefaultCSPA
} from './_redux/selector';

// Component
import Search from '../Components/Search';
import AddCSPA from '../Components/AddCSPA';

// Redux
import { clientConfigurationEntitlementActions as actions } from './_redux/reducer';

interface ISearchConfig {
  onPageChange: (number: number) => void;
  currentPage: number;
  onClearAndReset: () => void;
}

const SearchConfig: React.FC<ISearchConfig> = ({
  onPageChange,
  currentPage,
  onClearAndReset
}) => {
  const dispatch = useDispatch();
  const text = useStoreIdSelector<string>(selectTextSearch);
  const isDisplayClearAndReset = useSelector(selectDisplayClearAndReset);
  const defaultCSPA = useSelector(takeDefaultCSPA);

  const handleChange = (value: string) => {
    dispatch(actions.onChangeTextSearch(value));
  };

  const handleOnAddCSPA = () => {
    dispatch(actions.onOpenModalAddCSPA());
  };

  return (
    <Search
      onClearAndReset={onClearAndReset}
      onSearch={handleChange}
      currentPage={currentPage}
      onPageChange={onPageChange}
      textSearch={text}
      isDisplayClearAndReset={isDisplayClearAndReset}
    >
      <AddCSPA
        className="mt-16"
        onAddCSPA={handleOnAddCSPA}
        disabled={!defaultCSPA?.isDefaultClientInfoKey}
      />
    </Search>
  );
};

export default SearchConfig;
