import React from 'react';
import { fireEvent, screen } from '@testing-library/react';
import {
  mockActionCreator,
  queryAllBy,
  queryByClass,
  renderMockStore
} from 'app/test-utils';

import AddCSPAConfig from './AddCSPAConfig';
import { clientConfigurationEntitlementActions } from './_redux/reducer';
import userEvent from '@testing-library/user-event';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { CSPA_FORM_FIELDS } from '../constants';

const initialState: Partial<RootState> = {
  configurationUserRoleEntitlement: {
    isOpenModal: true,
    isGetListCSPAError: false,
    isLoading: false,
    isNoResult: false,
    selectedUserRole: {
      description: 'des',
      value: 'value'
    },
    userRoles: [],
    entitlements: [],
    entitlementsTextSearch: 'text search',
    defaultEntitlements: [],
    listRoleEntitlements: [],
    listRoleCSPA: [
      {
        clientId: 'clientId',
        isDefaultClientInfoKey: true
      }
    ],
    isGetListEntitlementError: false,
    isOpenAddCSPA: true,
    addCSPAErrorMsg: '',
    isLoadingModal: false,
    textSearch: 'textSearch',
    selectedCSPA: {},
    sortBy: {
      id: 'id'
    },
    isOpenStatusModal: true
  }
};

HTMLCanvasElement.prototype.getContext = jest.fn();

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(<AddCSPAConfig />, { initialState });
};

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

const clientConfigurationEntitlementActionsMock = mockActionCreator(
  clientConfigurationEntitlementActions
);

describe('Test AddCSPAConfig', () => {
  it('Should render UI', () => {
    const onOpenModalAddCSPA =
      clientConfigurationEntitlementActionsMock('onOpenModalAddCSPA');
    const { wrapper } = renderWrapper(initialState);
    const queryIconClose = queryByClass(
      wrapper.baseElement as HTMLElement,
      /icon icon-close/
    );
    queryIconClose!.click();
    expect(onOpenModalAddCSPA).toBeCalled();
  });

  describe('Test handle submit', () => {
    it('with default form value', () => {
      jest.useFakeTimers();
      const { wrapper } = renderWrapper(initialState);
      jest.runAllTimers();

      const queryRadios = queryAllBy(
        wrapper.baseElement as HTMLElement,
        'type',
        'radio'
      );

      queryRadios.forEach(e => {
        fireEvent.change(e!, { target: { value: 'all' } });
      });

      // enable submit button
      const textboxSystemId = screen.getByText(I18N_CLIENT_CONFIG.SYSTEM_ID)
        .parentElement!.previousElementSibling!;
      userEvent.type(textboxSystemId, '1234');

      const textboxDescription = screen.getByText(I18N_COMMON_TEXT.DESCRIPTION)
        .parentElement!.previousElementSibling!;
      userEvent.type(textboxDescription, '1234');

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.SUBMIT));

      expect(screen.getByText(I18N_COMMON_TEXT.SUBMIT)).toBeInTheDocument();
    });

    it('with custom form value', () => {
      jest.useFakeTimers();
      const { baseElement } = renderWrapper(initialState);
      jest.runAllTimers();

      const textboxSystemId = screen.getByText(I18N_CLIENT_CONFIG.SYSTEM_ID)
        .parentElement!.previousElementSibling!;
      userEvent.type(textboxSystemId, '1234');

      const textboxDescription = screen.getByText(I18N_COMMON_TEXT.DESCRIPTION)
        .parentElement!.previousElementSibling!;
      userEvent.type(textboxDescription, '1234');

      const principleRadioCustom = baseElement.querySelectorAll(
        `input[name="${CSPA_FORM_FIELDS.PRINCIPLE_RADIO}"]`
      )[1];
      userEvent.click(principleRadioCustom);

      const textboxPrincipleId = screen.getByText(
        I18N_CLIENT_CONFIG.PRINCIPLE_ID
      ).parentElement!.previousElementSibling!;
      userEvent.type(textboxPrincipleId, '1234');

      const agentRadioCustom = baseElement.querySelectorAll(
        `input[name="${CSPA_FORM_FIELDS.AGENT_RADIO}"]`
      )[1];
      userEvent.click(agentRadioCustom);

      const textboxAgentId = screen.getByText(I18N_CLIENT_CONFIG.AGENT_ID)
        .parentElement!.previousElementSibling!;
      userEvent.type(textboxAgentId, '1234');
      fireEvent.blur(textboxAgentId);

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.SUBMIT));

      expect(screen.getByText(I18N_COMMON_TEXT.SUBMIT)).toBeInTheDocument();
    });
  });
});
