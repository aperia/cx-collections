import React from 'react';

// Components
import StatusModal from './StatusModal';

// Utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { clientConfigurationEntitlementActions } from './_redux/reducer';

// Constants
import { CLIENT_CONFIG_STATUS_KEY } from 'pages/ClientConfiguration/constants';
import userEvent from '@testing-library/user-event';
import cloneDeep from 'lodash.clonedeep';

describe('Test StatusModal', () => {
  const initialState: Partial<RootState> = {
    configurationUserRoleEntitlement: {
      entitlements: [],
      isOpenModal: true,
      isLoadingModal: false,
      listRoleCSPA: [],
      textSearch: 'text search',
      isNoResult: true,
      sortBy: { id: 'clientId', order: undefined },
      isOpenAddCSPA: false,
      addCSPAErrorMsg: '',
      selectedCSPA: {
        id: 'id',
        cspaId: 'cspaId',
        clientId: 'clientId',
        systemId: 'systemId',
        principleId: 'principleId',
        agentId: 'agentId',
        description: 'description',
        userId: 'userId',
        additionalFields: [
          {
            name: CLIENT_CONFIG_STATUS_KEY,
            active: true
          }
        ]
      },
      isOpenStatusModal: true
    }
  };

  const spyActions = mockActionCreator(clientConfigurationEntitlementActions);

  it('should dispatch triggerUpdateActiveDeActiveCSPA', () => {
    const triggerUpdateActiveDeActiveCSPA = spyActions(
      'triggerUpdateActiveDeActiveCSPA'
    );

    const { wrapper } = renderMockStore(<StatusModal />, { initialState });

    expect(wrapper.getByText('txt_confirm_activate_cspa')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_confirm_activate_description')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_cancel')).toBeInTheDocument();

    userEvent.click(wrapper.getByText('txt_activate'));

    expect(triggerUpdateActiveDeActiveCSPA).toHaveBeenCalledWith({
      cspa: initialState.configurationUserRoleEntitlement!.selectedCSPA,
      status: true
    });
  });

  it('should dispatch onChangeOpenStatusModal', () => {
    const onChangeOpenStatusModal = spyActions('onChangeOpenStatusModal');
    const _initialState = cloneDeep(initialState);
    // coverage
    _initialState.configurationUserRoleEntitlement!.selectedCSPA.additionalFields =
      [];

    const { wrapper, baseElement } = renderMockStore(<StatusModal />, {
      initialState: _initialState
    });

    expect(wrapper.getByText('txt_confirm_activate_cspa')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_confirm_activate_description')
    ).toBeInTheDocument();

    userEvent.click(baseElement.querySelector('.icon-close')!);

    expect(onChangeOpenStatusModal).toHaveBeenCalled();
  });
});
