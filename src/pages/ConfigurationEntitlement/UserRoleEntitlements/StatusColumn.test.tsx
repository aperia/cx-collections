import React from 'react';
import { renderMockStoreId } from 'app/test-utils';
import StatusColumn from './StatusColumn';
import { screen } from '@testing-library/react';
import { ACTIVE, INACTIVE } from 'pages/ClientConfiguration/constants';

describe('Test StatusColumn component', () => {
  it('render inactive', () => {
    renderMockStoreId(<StatusColumn data={{ additionalFields: [] }} />);

    expect(screen.getByText(INACTIVE)).toBeInTheDocument();
  });

  it('render active', () => {
    const data = {
      active: true
    };
    renderMockStoreId(<StatusColumn data={data} />);

    expect(screen.getByText(ACTIVE)).toBeInTheDocument();
  });
});
