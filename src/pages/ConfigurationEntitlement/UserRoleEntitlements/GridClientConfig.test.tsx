import React from 'react';
import { screen } from '@testing-library/dom';

import { mockActionCreator, renderMockStore } from 'app/test-utils';

import GridClientConfig from './GridClientConfig';

import { clientConfigurationEntitlementActions } from './_redux/reducer';
import { act } from 'react-dom/test-utils';
import { UserRoleEntitlementState } from './types';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { queryByText } from '@testing-library/react';

const CPSA: CSPA = {
  id: '1',
  clientId: 'clientId',
  systemId: 'systemId',
  agentId: 'agentId',
  description: 'description',
  principleId: 'principleId'
};

const spyActions = mockActionCreator(clientConfigurationEntitlementActions);

jest.mock('app/_libraries/_dls/components/Pagination', () => {
  return () => <div>Pagination</div>;
});

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('Test GridClientConfig', () => {
  afterEach(() => {
    jest.resetAllMocks();
  });

  it('Should render ui', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        listRoleCSPA: [CPSA],
        sortBy: { id: 'clientId', order: undefined }
      } as UserRoleEntitlementState
    };
    act(() => {
      renderMockStore(<GridClientConfig />, {
        initialState
      });
    });
    const queryHeaderClientId = screen.queryByText(I18N_COMMON_TEXT.CLIENT_ID);
    const queryHeaderSystemId = screen.queryByText(
      I18N_CLIENT_CONFIG.SYSTEM_ID
    );
    const queryHeaderPrincipleId = screen.queryByText(
      I18N_CLIENT_CONFIG.PRINCIPLE_ID
    );
    const queryHeaderAgentId = screen.queryByText(I18N_CLIENT_CONFIG.AGENT_ID);
    const queryHeaderDes = screen.queryByText(I18N_COMMON_TEXT.DESCRIPTION);
    expect(queryHeaderClientId).toBeInTheDocument();
    expect(queryHeaderSystemId).toBeInTheDocument();
    expect(queryHeaderPrincipleId).toBeInTheDocument();
    expect(queryHeaderAgentId).toBeInTheDocument();
    expect(queryHeaderDes).toBeInTheDocument();
  });

  it('Should have pagination', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        listRoleCSPA: [
          CPSA,
          { ...CPSA, id: '1' },
          { ...CPSA, id: '2' },
          { ...CPSA, id: '3' },
          { ...CPSA, id: '4' },
          { ...CPSA, id: '5' },
          { ...CPSA, id: '6' },
          { ...CPSA, id: '7' },
          { ...CPSA, id: '8' },
          { ...CPSA, id: '9' },
          { ...CPSA, id: '10' },
          { ...CPSA, id: '11' }
        ],
        sortBy: { id: 'clientId', order: undefined }
      } as UserRoleEntitlementState
    };
    act(() => {
      renderMockStore(<GridClientConfig />, {
        initialState
      });
    });
    const queryPagination = screen.queryByText('Pagination');
    expect(queryPagination).toBeInTheDocument();
  });

  it('Should have search no data', () => {
    const spy = spyActions('onChangeTextSearch');
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        listRoleCSPA: [CPSA],
        sortBy: { id: 'clientId', order: undefined },
        textSearch: 'textSearch'
      } as UserRoleEntitlementState
    };
    act(() => {
      renderMockStore(<GridClientConfig />, {
        initialState
      });
    });
    const queryClearAndReset = screen.queryByText('txt_clear_and_reset');
    queryClearAndReset!.click();
    expect(spy).toHaveBeenCalled();
  });

  it('Should have sort clientId', () => {
    const spy = spyActions('onChangeSort');
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        listRoleCSPA: [CPSA],
        sortBy: { id: 'clientId', order: undefined },
        textSearch: ''
      } as UserRoleEntitlementState
    };
    act(() => {
      renderMockStore(<GridClientConfig />, {
        initialState
      });
    });

    const queryClientId = screen.queryByText(I18N_COMMON_TEXT.CLIENT_ID);
    queryClientId!.click();
    expect(spy).toHaveBeenCalledWith({
      id: 'clientId',
      order: 'asc'
    });
  });

  it('Should have sort principleId', () => {
    const spy = spyActions('onChangeSort');
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        listRoleCSPA: [CPSA],
        sortBy: { id: 'clientId', order: undefined },
        textSearch: ''
      } as UserRoleEntitlementState
    };
    act(() => {
      renderMockStore(<GridClientConfig />, {
        initialState
      });
    });

    const queryClientId = screen.queryByText(I18N_CLIENT_CONFIG.PRINCIPLE_ID);
    queryClientId!.click();
    expect(spy).toHaveBeenCalledWith({
      id: 'principleId',
      order: 'asc'
    });
  });

  it('Should have sort systemId', () => {
    const spy = spyActions('onChangeSort');
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        listRoleCSPA: [CPSA],
        sortBy: { id: 'clientId', order: undefined },
        textSearch: ''
      } as UserRoleEntitlementState
    };
    act(() => {
      renderMockStore(<GridClientConfig />, {
        initialState
      });
    });

    const queryClientId = screen.queryByText(I18N_CLIENT_CONFIG.SYSTEM_ID);
    queryClientId!.click();
    expect(spy).toHaveBeenCalledWith({
      id: 'systemId',
      order: 'asc'
    });
  });

  it('Should have sort agentId', () => {
    const spy = spyActions('onChangeSort');
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        listRoleCSPA: [CPSA],
        sortBy: { id: 'clientId', order: undefined },
        textSearch: ''
      } as UserRoleEntitlementState
    };
    act(() => {
      renderMockStore(<GridClientConfig />, {
        initialState
      });
    });

    const queryClientId = screen.queryByText(I18N_CLIENT_CONFIG.AGENT_ID);
    queryClientId!.click();
    expect(spy).toHaveBeenCalledWith({
      id: 'agentId',
      order: 'asc'
    });
  });

  it('Should have change CSPA', () => {
    let wrapper: any;
    const spy = spyActions('onChangeCSPA');
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        listRoleCSPA: [CPSA],
        sortBy: { id: 'clientId', order: undefined },
        textSearch: ''
      } as UserRoleEntitlementState
    };
    act(() => {
      const { container } = renderMockStore(<GridClientConfig />, {
        initialState
      });
      wrapper = container;
    });

    const queryButton = queryByText(wrapper, /txt_view_settings/);
    queryButton!.click();
    expect(spy).toHaveBeenCalledWith({ selectedCSPA: CPSA });
  });
});
