import React from 'react';
import { fireEvent, screen } from '@testing-library/dom';

import {
  mockActionCreator,
  queryByClass,
  renderMockStore
} from 'app/test-utils';

import SearchConfig from './Search';

import { clientConfigurationEntitlementActions } from './_redux/reducer';
import userEvent from '@testing-library/user-event';

const mockState: Partial<RootState> = {};

const spyActions = mockActionCreator(clientConfigurationEntitlementActions);
const onPageChange = jest.fn();

describe('Test SearchConfig', () => {
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(
      <SearchConfig
        onClearAndReset={() => {}}
        currentPage={1}
        onPageChange={onPageChange}
      />,
      { initialState }
    );
  };

  it('Should have click', () => {
    const action = spyActions('onChangeTextSearch');
    const textSearch = '1';
    const { container } = renderWrapper(mockState);
    const queryInput = screen.queryByPlaceholderText('txt_type_to_search');
    fireEvent.change(queryInput!, { target: { value: textSearch } });

    const queryIconSearch = queryByClass(container, /icon icon-search/);
    queryIconSearch?.click();
    expect(action).toHaveBeenCalledWith(textSearch);
  });

  it('Should trigger handleOnAddCSPA', () => {
    const action = spyActions('onOpenModalAddCSPA');

    const { wrapper } = renderWrapper({
      configurationUserRoleEntitlement: {
        listRoleCSPA: [
          {
            isDefaultClientInfoKey: true
          }
        ]
      }
    });

    userEvent.click(wrapper.getByText('txt_add_cspa'));

    expect(action).toHaveBeenCalled();
  });
});
