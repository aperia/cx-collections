import React from 'react';

// Hooks
import { useStoreIdSelector } from 'app/hooks';
import { useDispatch } from 'react-redux';

// Component
import ActiveDeActiveModal from '../Components/ActiveDeActiveModal';

// Redux
import {
  selectLoadingModal,
  selectSelectedCSPA,
  takeStatusActiveDeActiveModal
} from './_redux/selector';
import { clientConfigurationEntitlementActions } from './_redux/reducer';

const StatusModal = () => {
  const dispatch = useDispatch();

  const isOpenModal = useStoreIdSelector<boolean>(
    takeStatusActiveDeActiveModal
  );

  const isLoading = useStoreIdSelector<boolean>(selectLoadingModal);

  const selectedConfig = useStoreIdSelector<CSPA>(selectSelectedCSPA);

  const handleCloseModal = () => {
    dispatch(clientConfigurationEntitlementActions.onChangeOpenStatusModal());
  };

  const handleSubmit = () => {
    dispatch(
      clientConfigurationEntitlementActions.triggerUpdateActiveDeActiveCSPA({
        cspa: selectedConfig,
        status: !selectedConfig.active
      })
    );
  };

  return (
    <ActiveDeActiveModal
      isLoading={isLoading}
      isOpenModal={isOpenModal}
      onCloseModal={handleCloseModal}
      onSubmit={handleSubmit}
      selectedConfig={selectedConfig}
      isActive={!!selectedConfig.active}
    />
  );
};

export default StatusModal;
