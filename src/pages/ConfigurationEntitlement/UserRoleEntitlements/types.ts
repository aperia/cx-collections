import { SortType, TreeViewValue } from 'app/_libraries/_dls/components';

export interface UserRoleEntitlementState {
  isLoading: boolean;
  isOpenModal: boolean;
  isLoadingModal: boolean;
  textSearch: string;
  selectedCSPA: CSPA;
  sortBy: SortType;
  selectedUserRole: RefDataValue;
  userRoles: RefDataValue[];
  entitlements: TreeViewValue[];
  entitlementsTextSearch: string;
  defaultEntitlements: TreeViewValue[];
  listRoleEntitlements: Array<AppRoleEntitlement>;
  listRoleCSPA: Array<CSPA>;
  isGetListCSPAError: boolean;
  isGetListEntitlementError: boolean;
  isNoResult: boolean;
  isOpenAddCSPA: boolean;
  addCSPAErrorMsg: string;
  isOpenStatusModal: boolean;
}

/**
 * Request / response thunk
 */

export interface AppRoleCSPA {
  cspa?: string;
  components?: Array<EntitlementConfig>;
}

export interface AppRoleEntitlement {
  role?: string;
  cspas?: Array<AppRoleCSPA>;
}

export interface ChangeCSPAPayload {
  selectedCSPA: CSPA;
}

export interface GetUserEntitlementPayload {
  entitlements: Array<TreeViewValue>;
  listRoleEntitlements: Array<AppRoleEntitlement>;
}

export interface GetListCSPABasedRolePayload {
  cspas: Array<CSPA>;
}

export interface GetUserEntitlementArgs {
  translateFn: Function;
}

export interface GetListUserRoles {
  userRoles: Array<RefDataValue>;
}

/**
 * Service type
 */

export interface RolePermissions {
  [entitlement: string]: boolean | undefined;
}
export interface APIComponentRoleEntitlement {
  component?: string;
  entitlements?: RolePermissions;
}

export interface APIRoleCSPA {
  cspa?: string;
  components?: Array<APIComponentRoleEntitlement>;
  active?: boolean;
  description?: string;
}

export interface APIRoleEntitlement {
  role?: string;
  cspas?: Array<APIRoleCSPA>;
}

export interface GetRoleEntitlementResponse {
  roleEntitlements?: Array<APIRoleEntitlement>;
}

export interface GetRoleEntitlementRequest extends CommonRequest {
  selectRole: Array<string>;
}

export interface GetRoleEntitlementRoleCSPARequest extends CommonRequest {
  selectRole: Array<string>;
  selectCspa: Array<string>;
}
export interface UpdateUserRoleEntitlementRequest extends CommonRequest {
  roleEntitlementsRequest: {
    roleEntitlements?: Array<APIRoleEntitlement>;
  };
}

export interface AddUserRoleCSPA extends CommonRequest {
  defaultCspa: string;
  roleEntitlementsRequest: {
    roleEntitlements?: Array<APIRoleEntitlement>;
  };
}

export interface AddUserRoleCSPAArgs {
  clientId: string;
  systemId: string;
  principleId: string;
  agentId: string;
  description: string;
}

export interface UpdateStatusUserRoleCSPA extends CommonRequest {
  roleEntitlementsRequest: {
    roleEntitlements?: Array<APIRoleEntitlement>;
  };
}

export interface UpdateStatusUserRoleCSPAArgs {
  cspa: CSPA;
  status: boolean;
}
