import React from 'react';

import Actions from './Actions';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import userEvent from '@testing-library/user-event';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { clientConfigurationEntitlementActions } from './_redux/reducer';

describe('Test component Actions', () => {
  const mockClientConfigurationEntitlementActions = mockActionCreator(
    clientConfigurationEntitlementActions
  );

  it('should view settings and deactivate', () => {
    const onChangeOpenModal =
      mockClientConfigurationEntitlementActions('onChangeOpenModal');
    const onChangeCSPA =
      mockClientConfigurationEntitlementActions('onChangeCSPA');
    const mockData = { active: true };

    const { wrapper } = renderMockStore(<Actions data={mockData} />);

    userEvent.click(wrapper.getByText(I18N_COMMON_TEXT.DEACTIVATE));
    userEvent.click(wrapper.getByText('txt_view_settings'));

    expect(onChangeOpenModal).toHaveBeenCalled();
    expect(onChangeCSPA).toHaveBeenCalledWith({ selectedCSPA: mockData });
  });

  it('should view settings and activate', () => {
    const onChangeOpenModal =
      mockClientConfigurationEntitlementActions('onChangeOpenModal');
    const onChangeCSPA =
      mockClientConfigurationEntitlementActions('onChangeCSPA');
    const mockData = { active: false };

    const { wrapper } = renderMockStore(<Actions data={mockData} />);

    userEvent.click(wrapper.getByText(I18N_COMMON_TEXT.ACTIVATE));
    userEvent.click(wrapper.getByText('txt_view_settings'));

    expect(onChangeOpenModal).toHaveBeenCalled();
    expect(onChangeCSPA).toHaveBeenCalledWith({ selectedCSPA: mockData });
  });
});
