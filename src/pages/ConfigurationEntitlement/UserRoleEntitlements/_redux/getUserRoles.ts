import axios from 'axios';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// Types
import { GetListUserRoles, UserRoleEntitlementState } from '../types';

export const getUserRoles = createAsyncThunk<
  GetListUserRoles,
  undefined,
  ThunkAPIConfig
>('userRoleEntitlement/getUserRole', async () => {
  const { data } = await axios.get('refData/userRoles.json');
  return { userRoles: data };
});

export const getUserRolesBuilder = (
  builder: ActionReducerMapBuilder<UserRoleEntitlementState>
) => {
  const { fulfilled } = getUserRoles;

  builder.addCase(fulfilled, (draftState, action) => {
    draftState.userRoles = action.payload.userRoles;
  });
};
