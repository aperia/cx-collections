import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// Types
import { GetListCSPABasedRolePayload } from '../types';
import { UserRoleEntitlementState } from '../types';

// Service
import userRoleEntitlementService from '../userRoleEntitlementService';

// Helper
import { buildUserRoleCSPA } from '../helpers';

export const getListCSPABasedRole = createAsyncThunk<
  GetListCSPABasedRolePayload,
  undefined,
  ThunkAPIConfig
>('userRoleEntitlement/getListCSPABasedRole', async (args, thunkAPI) => {
  const { commonConfig } = window.appConfig;
  const { getState } = thunkAPI;
  const { configurationUserRoleEntitlement } = getState();
  const { selectedUserRole } = configurationUserRoleEntitlement;
  const { privileges } = window.appConfig.commonConfig || {};
  const { data } = await userRoleEntitlementService.getEntitlement({
    common: {
      privileges,
      app: commonConfig?.app,
      org: commonConfig?.org
    },
    selectRole: [selectedUserRole.value]
  });

  const { roleEntitlements = [] } = data || {};

  const cspas = buildUserRoleCSPA(roleEntitlements, selectedUserRole);

  return {
    cspas
  };
});

export const getListCSPABasedRoleBuilder = (
  builder: ActionReducerMapBuilder<UserRoleEntitlementState>
) => {
  const { pending, fulfilled, rejected } = getListCSPABasedRole;
  builder.addCase(pending, draftState => {
    return {
      ...draftState,
      isLoading: true
    };
  });
  builder.addCase(fulfilled, (draftState, action) => {
    const { cspas } = action.payload;

    return {
      ...draftState,
      isLoading: false,
      listRoleCSPA: cspas,
      isNoResult: !cspas.length,
      isGetListCSPAError: false
    };
  });
  builder.addCase(rejected, draftState => {
    return {
      ...draftState,
      isLoading: false,
      isGetListCSPAError: true
    };
  });
};
