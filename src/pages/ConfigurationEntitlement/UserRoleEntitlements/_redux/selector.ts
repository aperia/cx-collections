import { createSelector } from '@reduxjs/toolkit';
import { SortType, TreeViewValue } from 'app/_libraries/_dls/components';
import { cloneDeep } from 'lodash';

// Helper
import { isArrayHasValue } from 'app/helpers';
import {
  searchCSPA,
  checkCollapseAll,
  checkSelectAll,
  sortCSPAUserRole
} from '../../helpers';
import isEqual from 'react-fast-compare';

const getCSPA = (state: RootState) =>
  state.configurationUserRoleEntitlement.listRoleCSPA;

const getSortBy = (state: RootState): SortType => {
  return state.configurationUserRoleEntitlement.sortBy;
};

const getSelectedCSPA = (state: RootState): CSPA => {
  return state.configurationUserRoleEntitlement.selectedCSPA;
};

const getTextSearch = (state: RootState): string => {
  return state.configurationUserRoleEntitlement.textSearch;
};

const getSelectedUserRole = (state: RootState): RefDataValue => {
  return state.configurationUserRoleEntitlement.selectedUserRole;
};

const getListUserRole = (state: RootState) =>
  state.configurationUserRoleEntitlement.userRoles;

const getEntitlements = (state: RootState): TreeViewValue[] => {
  return state.configurationUserRoleEntitlement.entitlements;
};

const getDefaultEntitlements = (state: RootState): TreeViewValue[] => {
  return state.configurationUserRoleEntitlement.defaultEntitlements;
};

const getEntitlementsTextSearch = (state: RootState): string => {
  return state.configurationUserRoleEntitlement.entitlementsTextSearch;
};

const getStatusActiveDeActiveModal = (state: RootState) => {
  return state.configurationUserRoleEntitlement.isOpenStatusModal;
};

const getLoadingModalStatus = (state: RootState) =>
  state.configurationUserRoleEntitlement.isLoadingModal;

const getOpenModalStatus = (state: RootState) =>
  state.configurationUserRoleEntitlement.isOpenModal;

const getErrorLoadingCSPAList = (state: RootState) =>
  state.configurationUserRoleEntitlement.isGetListCSPAError;

const getOpenAddCSPAStatus = (state: RootState) =>
  state.configurationUserRoleEntitlement.isOpenAddCSPA;

const getLoadingCSPAStatus = (state: RootState) =>
  state.configurationUserRoleEntitlement.isLoading;

const getNoResultStatus = (state: RootState) =>
  state.configurationUserRoleEntitlement.isNoResult;

const getAddErrorMsg = (state: RootState) =>
  state.configurationUserRoleEntitlement.addCSPAErrorMsg;

export const takeNoResultStatus = createSelector(
  getNoResultStatus,
  data => data
);

export const takeDefaultCSPA = createSelector(getCSPA, data => {
  return data.find(item => !!item.isDefaultClientInfoKey);
});

export const takeStatusActiveDeActiveModal = createSelector(
  getStatusActiveDeActiveModal,
  data => data
);

export const takeSelectedRole = createSelector(
  getSelectedUserRole,
  data => data
);

export const selectCSPA = createSelector(
  getCSPA,
  getTextSearch,
  getSortBy,
  (CSPA, textSearch, sortBy) =>
    sortCSPAUserRole(searchCSPA(CSPA || [], textSearch), sortBy)
);

export const selectTextSearch = createSelector(
  getTextSearch,
  (textSearch: string) => textSearch
);

export const selectSelectedCSPA = createSelector(
  getSelectedCSPA,
  (CSPA: CSPA) => CSPA
);

export const takeOpenModalStatus = createSelector(
  getOpenModalStatus,
  (isOpenModal: boolean) => isOpenModal
);

export const selectLoadingModal = createSelector(
  getLoadingModalStatus,
  (isLoadingModal: boolean) => isLoadingModal
);

export const takeListCSPAErrorStatus = createSelector(
  getErrorLoadingCSPAList,
  (isGetListCSPAError: boolean) => isGetListCSPAError
);

export const takeOpenAddCSPAStatus = createSelector(
  getOpenAddCSPAStatus,
  data => data
);

export const takeAddErrorMsg = createSelector(getAddErrorMsg, data => data);

export const takeLoadingListCSPAStatus = createSelector(
  getLoadingCSPAStatus,
  (isLoading: boolean) => isLoading
);

export const selectDirtyEntitlements = createSelector(
  getEntitlements,
  getDefaultEntitlements,
  (entitlements: TreeViewValue[], defaultEntitlements: TreeViewValue[]) => {
    const nextEntitlements = entitlements.map(item => ({
      checked: item.checked,
      items: item.items?.map(childItem => ({ checked: childItem.checked }))
    }));
    const nextDefaultEntitlements = defaultEntitlements.map(item => ({
      checked: item.checked,
      items: item.items?.map(childItem => ({ checked: childItem.checked }))
    }));

    return !isEqual(nextEntitlements, nextDefaultEntitlements);
  }
);

export const selectEntitlementsFiltered = createSelector(
  getEntitlements,
  getEntitlementsTextSearch,
  (entitlements: TreeViewValue[], entitlementsTextSearch) => {
    if (!entitlementsTextSearch) {
      return entitlements;
    }

    const find = (text: string | undefined, matcher: string) => {
      return text?.toLowerCase().indexOf(matcher.toLowerCase()) !== -1;
    };

    let nextEntitlements = cloneDeep(entitlements);

    nextEntitlements = nextEntitlements.filter(item => {
      let isMatch = find(item.label, entitlementsTextSearch);

      if (!isMatch) {
        const isExists = item.items?.some(childrenItem => {
          return find(childrenItem.label, entitlementsTextSearch);
        });

        isMatch = Boolean(isExists);
      }

      return isMatch;
    });

    return nextEntitlements;
  }
);

export const selectEntitlementsTextSearch = createSelector(
  getEntitlementsTextSearch,
  (entitlementsTextSearch: string) => entitlementsTextSearch
);

export const selectIsCollapseAll = createSelector(
  selectEntitlementsFiltered,
  (entitlements: TreeViewValue[]) => checkCollapseAll(entitlements)
);

export const selectIsSelectAll = createSelector(
  selectEntitlementsFiltered,
  (entitlements: TreeViewValue[]) => checkSelectAll(entitlements)
);

export const selectTotalSelectedEntitlement = createSelector(
  getEntitlements,
  (entitlements: TreeViewValue[]) => {
    let total = 0;
    if (!isArrayHasValue(entitlements)) return total;
    entitlements.forEach(entitlement =>
      entitlement.items?.forEach(item => (item.checked ? (total += 1) : total))
    );
    return total;
  }
);

export const selectIsSearchNoData = createSelector(
  getCSPA,
  getTextSearch,
  (CSPA, textSearch: string) => {
    return !isArrayHasValue(searchCSPA(CSPA || [], textSearch)) && textSearch
      ? true
      : false;
  }
);

export const selectDisplayClearAndReset = createSelector(
  getCSPA,
  getTextSearch,
  (CSPA, textSearch: string) => {
    return isArrayHasValue(searchCSPA(CSPA || [], textSearch)) && textSearch
      ? true
      : false;
  }
);

export const selectIsNoData = createSelector(
  getCSPA,
  getTextSearch,
  (CSPA, textSearch: string) => {
    return !isArrayHasValue(CSPA || []) && !textSearch ? true : false;
  }
);

export const selectSort = createSelector(
  getSortBy,
  (sortBy: SortType) => sortBy
);

export const selectUserRoles = createSelector(
  getListUserRole,
  (data: RefDataValue[]) => data
);
