import { createStore, Store } from '@reduxjs/toolkit';

import { rootReducer } from 'storeConfig';

import { getUserRoles } from './getUserRoles';
import { initialState, configurationUserRoleEntitlement as reducer } from './reducer';

import axios from 'axios';


describe('UserRoleEntitlements > redux getUserRoles.test', () => {
  let store: Store<RootState>;
  const mockUserRoles = [{ value: 'CXCOL1', description: 'CXCOL1' }];

  beforeEach(() => {
    store = createStore(rootReducer, {
      configurationUserRoleEntitlement: initialState
    });
  });

  it('fulfilled', () => {
    const nextState = reducer(
      initialState,
      getUserRoles.fulfilled(
        { userRoles: mockUserRoles },
        'getUserRoles',
        undefined
      )
    );
    expect(nextState.userRoles).toEqual(mockUserRoles);
  });

  it('should return data', async () => {
    jest.spyOn(axios, 'get').mockResolvedValue({
      data: mockUserRoles
    });

    const response = await getUserRoles()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ userRoles: mockUserRoles });
  });

});
