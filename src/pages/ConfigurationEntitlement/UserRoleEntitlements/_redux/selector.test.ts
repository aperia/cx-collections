import * as selectors from './selector';

import { selectorWrapper } from 'app/test-utils';
import { UserRoleEntitlementState } from '../types';
import { TreeViewValue } from 'app/_libraries/_dls/components';

const CSPA = {
  id: 'id',
  clientId: 'clientId',
  systemId: 'systemId',
  principleId: 'principleId',
  agentId: 'agentId',
  description: 'description'
};

const CSPA1 = {
  id: 'id',
  clientId: 'clientId',
  systemId: 'systemId',
  principleId: 'principleId',
  agentId: 'agentId',
  isDefaultClientInfoKey: true,
  description: 'description'
};

const selectedUserRole = { description: 'description', value: 'value' };

const entitlements: TreeViewValue[] = [
  {
    id: 'AccountSnapshot',
    label: 'Account Snapshot',
    expanded: true,
    items: [{ id: 'ViewAccountSnapshot', label: 'View' }]
  }
];

const sortBy = { id: 'clientId', order: undefined };

describe('UserRoleEntitlements selectors', () => {
  it('selectCSPA', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        listRoleCSPA: [CSPA],
        sortBy
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectCSPA);
    expect(data).toEqual([CSPA]);
  });

  it('selectCSPA is empty', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        sortBy
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectCSPA);
    expect(data).toEqual([]);
  });

  it('selectTextSearch', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        textSearch: 'textSearch'
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectTextSearch);

    expect(data).toEqual('textSearch');
  });

  it('selectSelectedCSPA', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        selectedCSPA: CSPA
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectSelectedCSPA);

    expect(data).toEqual(CSPA);
  });

  it('takeOpenModalStatus', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        isOpenModal: true
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.takeOpenModalStatus);

    expect(data).toEqual(true);
  });

  it('selectLoadingModal', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        isLoadingModal: true
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectLoadingModal);

    expect(data).toEqual(true);
  });

  it('takeListCSPAErrorStatus', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        isGetListCSPAError: false
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.takeListCSPAErrorStatus);

    expect(data).toEqual(false);
  });

  it('takeLoadingListCSPAStatus', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        isLoading: false
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.takeLoadingListCSPAStatus);

    expect(data).toEqual(false);
  });

  it('selectIsSearchNoData', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        listRoleCSPA: [CSPA],
        textSearch: 'textSearch',
        sortBy
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectIsSearchNoData);

    expect(data).toEqual(true);
  });

  it('selectIsSearchNoData is CSPA empty', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        textSearch: 'textSearch',
        sortBy
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectIsSearchNoData);

    expect(data).toEqual(true);
  });

  it('selectDisplayClearAndReset', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        listRoleCSPA: [CSPA],
        textSearch: 'clientId',
        sortBy
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectDisplayClearAndReset);

    expect(data).toEqual(true);
  });

  it('selectDisplayClearAndReset is CSPA empty', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        textSearch: 'textSearch',
        sortBy
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectDisplayClearAndReset);

    expect(data).toEqual(false);
  });

  it('selectDirtyEntitlements', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        defaultEntitlements: entitlements,
        entitlements: entitlements
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectDirtyEntitlements);

    expect(data).toEqual(false);
  });

  it('selectSort', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        sortBy
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectSort);

    expect(data).toEqual(sortBy);
  });

  it('selectIsNoData', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        textSearch: 'textSearch'
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectIsNoData);

    expect(data).toEqual(false);
  });

  it('selectIsSelectAll', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        entitlements: [{ ...entitlements[0], checked: true }]
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectIsSelectAll);

    expect(data).toEqual(false);
  });

  it('selectIsSelectAll', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        entitlements: [{ ...entitlements[0], checked: false }]
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectIsSelectAll);

    expect(data).toEqual(true);
  });

  it('selectTotalSelectedEntitlement', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        entitlements
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectTotalSelectedEntitlement);

    expect(data).toEqual(0);
  });

  it('selectTotalSelectedEntitlement', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        entitlements: [
          {
            ...entitlements[0],
            items: [{ id: 'ViewAccountSnapshot', label: 'View', checked: true }]
          } as unknown
        ]
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectTotalSelectedEntitlement);

    expect(data).toEqual(1);
  });

  it('selectUserRoles ', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        userRoles: [selectedUserRole]
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectUserRoles);

    expect(data).toEqual([selectedUserRole]);
  });

  it('selectEntitlementsTextSearch ', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        entitlementsTextSearch: 'mock-text'
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectEntitlementsTextSearch);

    expect(data).toEqual('mock-text');
  });

  it('selectIsCollapseAll ', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        entitlementsTextSearch: 'account',
        entitlements: [{ ...entitlements[0], checked: true }]
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectIsCollapseAll);

    expect(data).toEqual(true);
  });

  it('selectIsCollapseAll dont select all', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        entitlementsTextSearch: 'mock search',
        entitlements: [{ ...entitlements[0], checked: true }]
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.selectIsCollapseAll);

    expect(data).toEqual(false);
  });

  it('takeAddErrorMsg', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        addCSPAErrorMsg: 'Test Error'
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.takeAddErrorMsg);

    expect(data).toEqual('Test Error');
  });

  it('takeOpenAddCSPAStatus', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        isOpenAddCSPA: true
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.takeOpenAddCSPAStatus);

    expect(data).toEqual(true);
  });

  it('takeNoResultStatus', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        isNoResult: true
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.takeNoResultStatus);

    expect(data).toEqual(true);
  });

  it('takeDefaultCSPA  ', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        listRoleCSPA: [CSPA, CSPA1]
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.takeDefaultCSPA);

    expect(data).toEqual(CSPA1);
  });

  it('takeStatusActiveDeActiveModal ', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        isOpenStatusModal: true
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.takeStatusActiveDeActiveModal);

    expect(data).toBeTruthy();
  });

  it('takeSelectedRole', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        selectedUserRole: { description: 'Test', value: 'CXCTest' }
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);

    const { data } = selectorTrigger(selectors.takeSelectedRole);

    expect(data).toEqual({ description: 'Test', value: 'CXCTest' });
  });

  it('takeStatusActiveDeActiveModal', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        isOpenStatusModal: true
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);
    const { data } = selectorTrigger(selectors.takeStatusActiveDeActiveModal);

    expect(data).toBeTruthy();
  });

  it('takeDefaultCSPA', () => {
    const initialState: Partial<RootState> = {
      configurationUserRoleEntitlement: {
        listRoleCSPA: [
          {
            isDefaultClientInfoKey: true
          }
        ]
      } as UserRoleEntitlementState
    };
    const selectorTrigger = selectorWrapper(initialState);
    const { data } = selectorTrigger(selectors.takeDefaultCSPA);

    expect(data).toBeTruthy();
  });
});
