import { isFulfilled, isRejected } from '@reduxjs/toolkit';
import {
  mockActionCreator,
  responseDefault,
  byPassFulfilled,
  byPassRejected
} from 'app/test-utils';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { createStore } from 'redux';

import { rootReducer } from 'storeConfig';
import { AddUserRoleCSPAArgs } from '../types';
import userRoleEntitlementService from '../userRoleEntitlementService';
import { addUserRoleCSPA, triggerAddUserRoleCSPA } from './addUserRoleCSPA';
import { clientConfigurationEntitlementActions } from './reducer';

describe('Test async thunk addUserRoleCSPA', () => {
  const mockToast = mockActionCreator(actionsToast);
  const mockClientConfigurationEntitlementActions = mockActionCreator(
    clientConfigurationEntitlementActions
  );
  const mockApiErrorNotificationAction = mockActionCreator(
    apiErrorNotificationAction
  );

  const mockArgs: AddUserRoleCSPAArgs = {
    agentId: 'agentId',
    clientId: 'clientId',
    description: 'description',
    principleId: 'principleId',
    systemId: 'systemId'
  };

  describe('addUserRoleCSPA', () => {
    beforeEach(() => {
      Object.defineProperty(window, 'appConfig', {
        writable: true,
        value: {
          commonConfig: {}
        }
      })
    })

    it('should fulfill', async () => {
      const store = createStore(rootReducer, {
        configurationUserRoleEntitlement: {
          listRoleCSPA: [
            {
              isDefaultClientInfoKey: true
            }
          ],
          selectedUserRole: {}
        }
      });

      jest
        .spyOn(userRoleEntitlementService, 'addUserRoleCSPA')
        .mockResolvedValue({
          ...responseDefault
        });

      const response = await addUserRoleCSPA(mockArgs)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(isFulfilled(response)).toBeTruthy();
    });

    it('should reject with empty config', async () => {
      Object.defineProperty(window, 'appConfig', {
        writable: true,
        value: {}
      })
      const store = createStore(rootReducer, {
        configurationUserRoleEntitlement: {
          listRoleCSPA: [
            {
              isDefaultClientInfoKey: true
            }
          ],
          selectedUserRole: {}
        }
      });

      jest
        .spyOn(userRoleEntitlementService, 'addUserRoleCSPA')
        .mockRejectedValue({
          ...responseDefault
        });

      const response = await addUserRoleCSPA(mockArgs)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(isRejected(response)).toBeTruthy();
    });

    it('should reject', async () => {
      const store = createStore(rootReducer, {
        configurationUserRoleEntitlement: {
          listRoleCSPA: [
            {
              isDefaultClientInfoKey: true
            }
          ],
          selectedUserRole: {}
        }
      });

      jest
        .spyOn(userRoleEntitlementService, 'addUserRoleCSPA')
        .mockRejectedValue({
          ...responseDefault
        });

      const response = await addUserRoleCSPA(mockArgs)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(isRejected(response)).toBeTruthy();
    });
  });

  describe('triggerAddUserRoleCSPA', () => {
    it('should fulfill with existed CSPA', async () => {
      const setAddClientConfigErrorMessage =
        mockClientConfigurationEntitlementActions(
          'setAddClientConfigErrorMessage'
        );

      const store = createStore(rootReducer, {
        configurationUserRoleEntitlement: {
          listRoleCSPA: [
            {
              id: 'clientId-systemId-principleId-agentId'
            }
          ],
          selectedUserRole: {}
        }
      });

      jest
        .spyOn(userRoleEntitlementService, 'addUserRoleCSPA')
        .mockResolvedValue({
          ...responseDefault
        });

      const response = await triggerAddUserRoleCSPA(mockArgs)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(isFulfilled(response)).toBeTruthy();
      expect(setAddClientConfigErrorMessage).toHaveBeenCalledWith(
        'txt_cspa_already_exists'
      );
    });

    it('should fulfill with non-existed CSPA', async () => {
      const successToast = mockToast('addToast');
      const setAddClientConfigErrorMessage =
        mockClientConfigurationEntitlementActions(
          'setAddClientConfigErrorMessage'
        );
      const onOpenModalAddCSPA =
        mockClientConfigurationEntitlementActions('onOpenModalAddCSPA');

      const store = createStore(rootReducer, {
        configurationUserRoleEntitlement: {
          listRoleCSPA: [],
          selectedUserRole: {}
        }
      });

      jest
        .spyOn(userRoleEntitlementService, 'addUserRoleCSPA')
        .mockResolvedValue({
          ...responseDefault
        });

      const response = await triggerAddUserRoleCSPA(mockArgs)(
        byPassFulfilled(triggerAddUserRoleCSPA.fulfilled.type),
        store.getState,
        {}
      );

      expect(isFulfilled(response)).toBeTruthy();
      expect(successToast).toHaveBeenCalledWith({
        show: true,
        type: 'success',
        message: 'txt_cspa_added'
      });
      expect(setAddClientConfigErrorMessage).toHaveBeenCalledWith('');
      expect(onOpenModalAddCSPA).toHaveBeenCalled();
    });

    it('should show error toast', async () => {
      const errorToast = mockToast('addToast');
      const updateThunkApiError = mockApiErrorNotificationAction(
        'updateThunkApiError'
      );

      const store = createStore(rootReducer, {
        configurationUserRoleEntitlement: {
          listRoleCSPA: [],
          selectedUserRole: {}
        }
      });

      jest
        .spyOn(userRoleEntitlementService, 'addUserRoleCSPA')
        .mockRejectedValue({
          ...responseDefault
        });

      await triggerAddUserRoleCSPA(mockArgs)(
        byPassRejected(addUserRoleCSPA.rejected.type),
        store.getState,
        {}
      );

      expect(errorToast).toHaveBeenCalledWith({
        show: true,
        type: 'error',
        message: 'txt_cspa_failed_to_add'
      });
      expect(updateThunkApiError).toHaveBeenCalled();
    });
  });
});
