import { applyMiddleware, createStore } from '@reduxjs/toolkit';
import { rootReducer } from 'storeConfig';
import { responseDefault } from 'app/test-utils';
import userRoleEntitlementService from '../userRoleEntitlementService';
import thunk from 'redux-thunk';
import { getUserRoleEntitlements } from './getUserRoleEntitlements';
import { GetUserEntitlementArgs } from '../types';
import { COMPONENTS_MAP } from 'app/entitlements/constants';
import cloneDeep from 'lodash.clonedeep';
import set from 'lodash.set';

const initialState: Partial<RootState> = {
  configurationUserRoleEntitlement: {
    isOpenModal: true,
    isGetListCSPAError: false,
    isLoading: false,
    isNoResult: false,
    selectedUserRole: {
      description: 'des',
      value: 'roleA'
    },
    userRoles: [],
    entitlements: [],
    entitlementsTextSearch: 'text search',
    defaultEntitlements: [],
    listRoleEntitlements: [],
    listRoleCSPA: [],
    isGetListEntitlementError: false,
    isOpenAddCSPA: true,
    addCSPAErrorMsg: '',
    isLoadingModal: false,
    textSearch: 'textSearch',
    selectedCSPA: {
      cspaId: '123456'
    },
    sortBy: {
      id: 'id'
    }
  }
};

window.appConfig = {
  commonConfig: {
    user: 'user',
    app: 'app',
    org: 'org'
  }
} as AppConfiguration;

const mockResponse = {
  roleEntitlements: [
    {
      role: 'roleA',
      cspas: [
        {
          cspa: '123456',
          components: [
            {
              component: COMPONENTS_MAP.ACCOUNT_SEARCH,
              entitlements: { view: true }
            }
          ]
        }
      ]
    }
  ]
};

const callApi = (
  isError = false,
  args: GetUserEntitlementArgs,
  response: any,
  state = initialState
) => {
  const store = createStore(rootReducer, state, applyMiddleware(thunk));

  if (!isError) {
    jest
      .spyOn(userRoleEntitlementService, 'getEntitlementBasedOnRoleCSPA')
      .mockResolvedValue({
        ...responseDefault,
        data: response
      });
  } else {
    jest
      .spyOn(userRoleEntitlementService, 'getEntitlementBasedOnRoleCSPA')
      .mockRejectedValue(new Error('Async Error'));
  }

  return getUserRoleEntitlements(args)(store.dispatch, store.getState, {});
};
const translateFn = jest
  .fn()
  .mockImplementation(() => ({ t: (text: string) => text }));

describe('Test getUserRoleEntitlements', () => {
  const args: GetUserEntitlementArgs = { translateFn };
  it('It should run correctly', async () => {
    const response = await callApi(false, args, mockResponse);
    expect(response?.payload?.entitlements[0].id).toEqual('AccountInformation');
  });

  it('It should run correctly when response is empty', async () => {
    const mockResponse = {
      roleEntitlements: [
        {
          role: 'roleA',
          cspas: [
            {
              cspa: '123456'
            }
          ]
        }
      ]
    };
    const response = await callApi(false, args, mockResponse);
    expect(response?.payload?.entitlements[0].id).toEqual(undefined);
  });

  it('It should run correctly when selectedCSPA is undefined', async () => {
    const response = await callApi(false, args, undefined);
    expect(response?.payload?.entitlements[0].id).toEqual('AccountInformation');
  });

  it('It should run rejected', async () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: {}
    })
    const newState = cloneDeep(initialState);
    set(
      newState,
      ['configurationUserRoleEntitlement', 'selectedCSPA'],
      undefined
    );
    const response = await callApi(true, args, {}, newState);
    expect(response.type).toEqual(getUserRoleEntitlements.rejected.type);
  });
});
