import { isFulfilled, isRejected } from '@reduxjs/toolkit';
import { byPassFulfilled, byPassRejected } from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { mockActionCreator, responseDefault } from 'app/test-utils';
import { createStore } from 'redux';

import { rootReducer } from 'storeConfig';
import { UpdateStatusUserRoleCSPAArgs } from '../types';
import userRoleEntitlementService from '../userRoleEntitlementService';
import { clientConfigurationEntitlementActions } from './reducer';
import {
  triggerUpdateActiveDeActiveCSPA,
  updateActiveDeActiveCSPA
} from './updateActiveDeActiveCSPA';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

describe('updateActiveDeActiveCSPA', () => {
  window.appConfig = {
    commonConfig: {}
  } as AppConfiguration;

  const mockToast = mockActionCreator(actionsToast);
  const mockClientConfigurationEntitlementActions = mockActionCreator(
    clientConfigurationEntitlementActions
  );
  const mockApiErrorNotificationAction = mockActionCreator(
    apiErrorNotificationAction
  );

  describe('updateActiveDeActiveCSPA', () => {
    it('should fulfill', async () => {
      const mockArgs: UpdateStatusUserRoleCSPAArgs = {
        cspa: {},
        status: true
      };

      const store = createStore(rootReducer, {
        configurationUserRoleEntitlement: {}
      });

      jest
        .spyOn(userRoleEntitlementService, 'updateUserRoleCSPA')
        .mockResolvedValue({ ...responseDefault });

      const response = await updateActiveDeActiveCSPA(mockArgs)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(isFulfilled(response)).toBeTruthy();
    });

    it('should reject', async () => {
      Object.defineProperty(window, 'appConfig', {
        writable: true,
        value: {}
      })
      const mockArgs: UpdateStatusUserRoleCSPAArgs = {
        cspa: {},
        status: true
      };

      const store = createStore(rootReducer, {
        configurationUserRoleEntitlement: {}
      });

      jest
        .spyOn(userRoleEntitlementService, 'updateUserRoleCSPA')
        .mockRejectedValue({ ...responseDefault });

      const response = await updateActiveDeActiveCSPA(mockArgs)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(isRejected(response)).toBeTruthy();
    });
  });

  describe('triggerUpdateActiveDeActiveCSPA', () => {
    it('should fulfill with status true', async () => {
      const successToast = mockToast('addToast');
      const onChangeOpenStatusModal = mockClientConfigurationEntitlementActions(
        'onChangeOpenStatusModal'
      );

      const mockArgs: UpdateStatusUserRoleCSPAArgs = {
        status: true,
        cspa: {}
      };

      const store = createStore(rootReducer, {
        configurationUserRoleEntitlement: {
          selectedUserRole: {}
        }
      });

      jest
        .spyOn(userRoleEntitlementService, 'updateUserRoleCSPA')
        .mockResolvedValue({ ...responseDefault });

      const response = await triggerUpdateActiveDeActiveCSPA(mockArgs)(
        byPassFulfilled(triggerUpdateActiveDeActiveCSPA.fulfilled.type),
        store.getState,
        {}
      );

      expect(isFulfilled(response)).toBeTruthy();
      expect(successToast).toHaveBeenCalledWith({
        show: true,
        type: 'success',
        message: 'txt_cspa_activated'
      });
      expect(onChangeOpenStatusModal).toHaveBeenCalled();
    });

    it('should fulfill with status false', async () => {
      const successToast = mockToast('addToast');
      const onChangeOpenStatusModal = mockClientConfigurationEntitlementActions(
        'onChangeOpenStatusModal'
      );

      const mockArgs: UpdateStatusUserRoleCSPAArgs = {
        status: false,
        cspa: {}
      };

      const store = createStore(rootReducer, {
        configurationUserRoleEntitlement: {
          selectedUserRole: {}
        }
      });

      jest
        .spyOn(userRoleEntitlementService, 'updateUserRoleCSPA')
        .mockResolvedValue({ ...responseDefault });

      const response = await triggerUpdateActiveDeActiveCSPA(mockArgs)(
        byPassFulfilled(triggerUpdateActiveDeActiveCSPA.fulfilled.type),
        store.getState,
        {}
      );

      expect(isFulfilled(response)).toBeTruthy();
      expect(successToast).toHaveBeenCalledWith({
        show: true,
        type: 'success',
        message: 'txt_cspa_deactivated'
      });
      expect(onChangeOpenStatusModal).toHaveBeenCalled();
    });

    it('should reject with status true', async () => {
      const errorToast = mockToast('addToast');
      const updateThunkApiError = mockApiErrorNotificationAction(
        'updateThunkApiError'
      );

      const mockArgs: UpdateStatusUserRoleCSPAArgs = {
        status: true,
        cspa: {}
      };

      const store = createStore(rootReducer, {
        configurationUserRoleEntitlement: {
          selectedUserRole: {}
        }
      });

      jest
        .spyOn(userRoleEntitlementService, 'updateUserRoleCSPA')
        .mockRejectedValue({ ...responseDefault });

      const response = await triggerUpdateActiveDeActiveCSPA(mockArgs)(
        byPassRejected(updateActiveDeActiveCSPA.rejected.type),
        store.getState,
        {}
      );

      expect(isFulfilled(response)).toBeTruthy();
      expect(errorToast).toHaveBeenCalledWith({
        show: true,
        type: 'error',
        message: 'txt_cspa_failed_to_activate'
      });
      expect(updateThunkApiError).toHaveBeenCalled();
    });

    it('should reject with status false', async () => {
      const errorToast = mockToast('addToast');
      const updateThunkApiError = mockApiErrorNotificationAction(
        'updateThunkApiError'
      );

      const mockArgs: UpdateStatusUserRoleCSPAArgs = {
        status: false,
        cspa: {}
      };

      const store = createStore(rootReducer, {
        configurationUserRoleEntitlement: {
          selectedUserRole: {}
        }
      });

      jest
        .spyOn(userRoleEntitlementService, 'updateUserRoleCSPA')
        .mockRejectedValue({ ...responseDefault });

      const response = await triggerUpdateActiveDeActiveCSPA(mockArgs)(
        byPassRejected(updateActiveDeActiveCSPA.rejected.type),
        store.getState,
        {}
      );

      expect(isFulfilled(response)).toBeTruthy();
      expect(errorToast).toHaveBeenCalledWith({
        show: true,
        type: 'error',
        message: 'txt_cspa_failed_to_deactivate'
      });
      expect(updateThunkApiError).toHaveBeenCalled();
    });
  });
});
