import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';

// Helper
import { buildUpdateRoleEntitlement } from '../helpers';

//Redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// Type
import { UserRoleEntitlementState } from '../types';

// Service
import userRoleEntitlementService from '../userRoleEntitlementService';

// Const
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';

// Redux
import { saveChangedClientConfig } from 'pages/ClientConfiguration/ChangeHistory/_redux/saveChangedClientConfig';
import { batch } from 'react-redux';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export const updateEntitlement = createAsyncThunk<
  unknown,
  undefined,
  ThunkAPIConfig
>('userRoleEntitlement/updateEntitlement', async (args, thunkAPI) => {
  try {
    const { getState } = thunkAPI;
    const { configurationUserRoleEntitlement } = getState();

    const { selectedUserRole, selectedCSPA, entitlements } =
      configurationUserRoleEntitlement;
    const { agentId, clientId, principleId, systemId } = selectedCSPA;

    const newRoleEntitlements = buildUpdateRoleEntitlement(
      selectedUserRole,
      selectedCSPA,
      entitlements
    );
    const { privileges } = window.appConfig.commonConfig || {};

    await userRoleEntitlementService.updateUserRoleEntitlement({
      common: {
        privileges,
        agent: agentId,
        clientNumber: clientId,
        system: systemId,
        prin: principleId,
        user: window.appConfig.commonConfig.user,
        app: window.appConfig.commonConfig.app,
        org: window.appConfig.commonConfig.org
      },
      roleEntitlementsRequest: {
        roleEntitlements: newRoleEntitlements
      }
    });
  } catch (error) {
    throw thunkAPI.rejectWithValue(error);
  }
});

export const updateEntitlementBuilder = (
  builder: ActionReducerMapBuilder<UserRoleEntitlementState>
) => {
  const { pending, fulfilled, rejected } = updateEntitlement;
  builder.addCase(pending, draftState => {
    draftState.isLoadingModal = true;
  });
  builder.addCase(fulfilled, draftState => {
    draftState.isLoadingModal = false;
    draftState.isOpenModal = false;
  });
  builder.addCase(rejected, draftState => {
    draftState.isLoadingModal = false;
  });
};

export const triggerUpdateEntitlement = createAsyncThunk<
  unknown,
  undefined,
  ThunkAPIConfig
>('userRoleEntitlement/triggerUpdateEntitlement', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;
  const { configurationUserRoleEntitlement } = getState();

  const { entitlements, defaultEntitlements, selectedUserRole } =
    configurationUserRoleEntitlement;

  const response = await dispatch(updateEntitlement());

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'txt_user_role_entitlement_update_success'
        })
      );
      dispatch(
        saveChangedClientConfig({
          action: 'UPDATE',
          changedCategory: 'userRoleEntitlement',
          changedSection: { userRole: selectedUserRole.value },
          oldValue: defaultEntitlements,
          newValue: entitlements
        })
      );
    });
  }

  if (isRejected(response)) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: 'txt_user_role_entitlement_update_failed'
      })
    );

    dispatch(
      apiErrorNotificationAction.updateThunkApiError({
        storeId: API_ERROR_STORE_ID.ENTITLEMENT,
        forSection: 'inModalBody',
        rejectData: {
          ...response,
          payload: response.payload?.response
        }
      })
    );
  }
});
