import { SortType, TreeViewValue } from 'app/_libraries/_dls/components';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { UserRoleEntitlementState } from '../types';
import {
  clientConfigurationEntitlementActions as actions,
  initialState
} from './reducer';

describe('Test Reducer User Role Entitlement', () => {
  const entitlements: TreeViewValue = {
    id: 'AccountSnapshot',
    label: 'Account Snapshot',
    expanded: true,
    checked: true,
    items: [{ id: 'ViewAccountSnapshot', label: 'View', checked: true }]
  };
  const configurationUserRoleEntitlement: UserRoleEntitlementState = {
    listRoleCSPA: [],
    textSearch: '',
    entitlementsTextSearch: '',
    selectedCSPA: {} as CSPA,
    selectedUserRole: {} as RefDataValue,
    entitlements: [entitlements],
    sortBy: { id: 'clientId', order: undefined },
    isLoadingModal: false,
    isOpenModal: false,
    isLoading: false,
    userRoles: [],
    defaultEntitlements: [],
    addCSPAErrorMsg: '',
    isGetListCSPAError: false,
    isGetListEntitlementError: false,
    isNoResult: false,
    isOpenAddCSPA: false,
    listRoleEntitlements: [],
    isOpenStatusModal: true
  };

  it('onChangeOpenModalUpdate', () => {
    const store = createStore(rootReducer, {
      configurationUserRoleEntitlement
    });

    const { getState, dispatch } = store;

    dispatch(actions.onChangeOpenModal());

    const dataExpect = getState().configurationUserRoleEntitlement.isOpenModal;

    expect(dataExpect).toEqual(!configurationUserRoleEntitlement.isOpenModal);
  });

  it('onChangeTextSearch', () => {
    const store = createStore(rootReducer, {
      configurationUserRoleEntitlement
    });

    const { getState, dispatch } = store;
    const textSearch = 'TextSearch';
    dispatch(actions.onChangeTextSearch(textSearch));

    const dataExpect = getState().configurationUserRoleEntitlement.textSearch;

    expect(dataExpect).toEqual(textSearch);
  });

  it('onChangeUserRole', () => {
    const store = createStore(rootReducer, {
      configurationUserRoleEntitlement
    });

    const { getState, dispatch } = store;
    const useRole = {
      value: 'value',
      description: 'description'
    };
    dispatch(actions.onChangeUserRole(useRole));

    const dataExpect =
      getState().configurationUserRoleEntitlement.selectedUserRole;

    expect(dataExpect).toEqual(useRole);
  });

  it('onChangeSort', () => {
    const store = createStore(rootReducer, {
      configurationUserRoleEntitlement
    });

    const { getState, dispatch } = store;
    const sortBy: SortType = { id: 'clientId', order: 'desc' };
    dispatch(actions.onChangeSort(sortBy));

    const dataExpect = getState().configurationUserRoleEntitlement.sortBy;

    expect(dataExpect).toEqual(sortBy);
  });

  it('onRemoveTabEntitlements', () => {
    const store = createStore(rootReducer, {
      configurationUserRoleEntitlement
    });

    const { getState, dispatch } = store;
    dispatch(actions.onRemoveTabEntitlements());

    const dataExpect = getState().configurationUserRoleEntitlement;

    expect(dataExpect).toEqual(initialState);
  });

  it('onOpenModalAddCSPA', () => {
    const store = createStore(rootReducer, {
      configurationUserRoleEntitlement
    });

    const { getState, dispatch } = store;
    dispatch(actions.onOpenModalAddCSPA());

    const dataExpect =
      getState().configurationUserRoleEntitlement.isOpenAddCSPA;

    expect(dataExpect).toEqual(true);
  });

  it('removeModalData', () => {
    const store = createStore(rootReducer, {
      configurationUserRoleEntitlement
    });

    const { getState, dispatch } = store;
    dispatch(actions.removeModalData());

    const dataExpect = getState().configurationUserRoleEntitlement;

    expect(dataExpect).toEqual({
      ...configurationUserRoleEntitlement,
      isDirtyEntitlements: false,
      entitlements: [],
      entitlementsTextSearch: '',
      defaultEntitlements: []
    });
  });

  it('onChangeCSPA', () => {
    const store = createStore(rootReducer, {
      configurationUserRoleEntitlement
    });
    const param = {
      selectedCSPA: {
        id: 'id',
        clientId: 'clientId',
        systemId: 'systemId',
        principleId: 'principleId',
        agentId: 'agentId',
        description: 'description'
      } as CSPA
    };
    const { getState, dispatch } = store;
    dispatch(actions.onChangeCSPA(param));

    const dataExpect = getState().configurationUserRoleEntitlement.selectedCSPA;

    expect(dataExpect).toEqual({
      id: 'id',
      clientId: 'clientId',
      systemId: 'systemId',
      principleId: 'principleId',
      agentId: 'agentId',
      description: 'description'
    });
  });

  it('setAddClientConfigErrorMessage', () => {
    const store = createStore(rootReducer, {
      configurationUserRoleEntitlement
    });

    const { getState, dispatch } = store;
    dispatch(actions.setAddClientConfigErrorMessage(''));

    const dataExpect =
      getState().configurationUserRoleEntitlement.addCSPAErrorMsg;

    expect(dataExpect).toEqual('');
  });

  it('onChangeEntitlements', () => {
    const store = createStore(rootReducer, {
      configurationUserRoleEntitlement
    });
    const param = {
      entitlements: [entitlements]
    };
    const { getState, dispatch } = store;
    dispatch(actions.onChangeEntitlements(param));

    const dataExpect = getState().configurationUserRoleEntitlement.entitlements;

    expect(dataExpect).toEqual([entitlements]);
  });

  it('onChangeEntitlements', () => {
    const store = createStore(rootReducer, {
      configurationUserRoleEntitlement
    });
    const param = {
      entitlements: [{ ...entitlements, id: 'Test' }]
    };
    const { getState, dispatch } = store;
    dispatch(actions.onChangeEntitlements(param));

    const dataExpect = getState().configurationUserRoleEntitlement.entitlements;
    expect(dataExpect).toEqual([entitlements]);
  });

  it('onChangeEntitlementsTextSearch', () => {
    const store = createStore(rootReducer, {
      configurationUserRoleEntitlement
    });
    const { getState, dispatch } = store;
    dispatch(actions.onChangeEntitlementsTextSearch('Test'));

    const dataExpect =
      getState().configurationUserRoleEntitlement.entitlementsTextSearch;

    expect(dataExpect).toEqual('Test');
  });

  it('onChangeOpenStatusModal', () => {
    const store = createStore(rootReducer, {
      configurationUserRoleEntitlement
    });
    const { getState, dispatch } = store;
    dispatch(actions.onChangeOpenStatusModal());

    const dataExpect =
      getState().configurationUserRoleEntitlement.isOpenStatusModal;

    expect(dataExpect).toBeFalsy();
  });

  it('onChangeCollapseAll', () => {
    const store = createStore(rootReducer, {
      configurationUserRoleEntitlement
    });
    const param = {
      entitlementsFiltered: [entitlements]
    };
    const { getState, dispatch } = store;
    dispatch(actions.onChangeCollapseAll(param));

    const dataExpect = getState().configurationUserRoleEntitlement;

    expect(dataExpect).toEqual({
      ...configurationUserRoleEntitlement,
      entitlements: [
        {
          ...entitlements,
          expanded: false
        }
      ]
    });
  });

  it('onChangeCollapseAll', () => {
    const store = createStore(rootReducer, {
      configurationUserRoleEntitlement
    });
    const param = {
      entitlementsFiltered: []
    };
    const { getState, dispatch } = store;
    dispatch(actions.onChangeCollapseAll(param));

    const dataExpect = getState().configurationUserRoleEntitlement;

    expect(dataExpect).toEqual({
      ...configurationUserRoleEntitlement,
      entitlements: [
        {
          ...entitlements
        }
      ]
    });
  });

  it('onChangeSelectAll', () => {
    const store = createStore(rootReducer, {
      configurationUserRoleEntitlement
    });
    const param = {
      entitlementsFiltered: [entitlements]
    };
    const { getState, dispatch } = store;
    dispatch(actions.onChangeSelectAll(param));

    const dataExpect = getState().configurationUserRoleEntitlement;

    expect(dataExpect).toEqual({
      ...configurationUserRoleEntitlement,
      entitlements: [
        {
          ...entitlements,
          checked: false,
          items: [
            {
              checked: false,
              id: 'ViewAccountSnapshot',
              label: 'View'
            }
          ]
        }
      ]
    });
  });

  it('onChangeSelectAll', () => {
    const store = createStore(rootReducer, {
      configurationUserRoleEntitlement
    });
    const param = {
      entitlementsFiltered: []
    };
    const { getState, dispatch } = store;
    dispatch(actions.onChangeSelectAll(param));

    const dataExpect = getState().configurationUserRoleEntitlement;

    expect(dataExpect).toEqual({
      ...configurationUserRoleEntitlement,
      entitlements: [
        {
          ...entitlements
        }
      ]
    });
  });

  it('onChangeOpenStatusModal', () => {
    const store = createStore(rootReducer, {
      configurationUserRoleEntitlement
    });
    const { getState, dispatch } = store;

    dispatch(actions.onChangeOpenStatusModal());

    const dataExpect = getState().configurationUserRoleEntitlement;

    expect(dataExpect).toEqual({
      ...configurationUserRoleEntitlement,
      isOpenStatusModal: false
    });
  });
});
