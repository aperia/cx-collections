import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { lowerCaseUserRole } from 'pages/ConfigurationEntitlement/helpers';

// Helper
import {
  buildListRoleEntitlements,
  covertAPIDataToAppRoleEntitlement,
  convertRoleEntitlementToTreeView
} from '../helpers';

// Types
import {
  APIRoleEntitlement,
  GetUserEntitlementArgs,
  GetUserEntitlementPayload
} from '../types';
import { UserRoleEntitlementState } from '../types';

// Service
import userRoleEntitlementService from '../userRoleEntitlementService';

export const getUserRoleEntitlements = createAsyncThunk<
  GetUserEntitlementPayload,
  GetUserEntitlementArgs,
  ThunkAPIConfig
>('userRoleEntitlement/getUserRoleEntitlements', async (args, thunkAPI) => {
  const { commonConfig } = window.appConfig;
  const { translateFn } = args;

  const { getState } = thunkAPI;
  const { configurationUserRoleEntitlement } = getState();
  const { selectedCSPA = {}, selectedUserRole: role } =
    configurationUserRoleEntitlement;
  const { privileges } = window.appConfig.commonConfig || {};

  const { data } =
    await userRoleEntitlementService.getEntitlementBasedOnRoleCSPA({
      common: {
        privileges,
        app: commonConfig?.app,
        org: commonConfig?.org
      },
      selectRole: [role.value],
      selectCspa: [selectedCSPA.cspaId || '']
    });

  const { roleEntitlements: entitlements = [] } = data || {};

  const roleEntitlements: APIRoleEntitlement[] =
    lowerCaseUserRole(entitlements);

  const roleCSPA = roleEntitlements.find(item => item.role === role.value);
  const currentCSPA = roleCSPA?.cspas?.find(
    item => item.cspa === selectedCSPA?.cspaId
  );

  const appEntitlements = covertAPIDataToAppRoleEntitlement(
    currentCSPA?.components
  );

  const listRoleEntitlements = buildListRoleEntitlements(roleEntitlements);

  return {
    entitlements: convertRoleEntitlementToTreeView(
      appEntitlements,
      currentCSPA,
      translateFn
    ),
    listRoleEntitlements
  };
});

export const getUserRoleEntitlementBuilder = (
  builder: ActionReducerMapBuilder<UserRoleEntitlementState>
) => {
  const { pending, fulfilled, rejected } = getUserRoleEntitlements;
  builder.addCase(pending, draftState => {
    return {
      ...draftState,
      isLoadingModal: true
    };
  });
  builder.addCase(fulfilled, (draftState, action) => {
    const { entitlements, listRoleEntitlements } = action.payload;

    return {
      ...draftState,
      isLoadingModal: false,
      entitlements,
      defaultEntitlements: entitlements,
      listRoleEntitlements
    };
  });
  builder.addCase(rejected, draftState => {
    return {
      ...draftState,
      isLoadingModal: false
    };
  });
};
