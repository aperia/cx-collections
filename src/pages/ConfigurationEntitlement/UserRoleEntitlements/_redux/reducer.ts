import { createSlice, current, PayloadAction } from '@reduxjs/toolkit';
import { SortType, TreeViewValue } from 'app/_libraries/_dls/components';

// Types
import { ChangeCSPAPayload, UserRoleEntitlementState } from '../types';

// Actions
import { getUserRoles, getUserRolesBuilder } from './getUserRoles';
import {
  getUserRoleEntitlements,
  getUserRoleEntitlementBuilder
} from './getUserRoleEntitlements';
import {
  triggerUpdateEntitlement,
  updateEntitlement,
  updateEntitlementBuilder
} from './updateEntitlement';
import {
  getListCSPABasedRole,
  getListCSPABasedRoleBuilder
} from './getListCSPABasedRole';
import {
  addUserRoleCSPA,
  addUserRoleCSPABuilder,
  triggerAddUserRoleCSPA
} from './addUserRoleCSPA';
import {
  triggerUpdateActiveDeActiveCSPA,
  updateActiveDeActiveCSPA,
  updateActiveDeActiveCSPABuilder
} from './updateActiveDeActiveCSPA';

export const initialState: UserRoleEntitlementState = {
  textSearch: '',
  selectedCSPA: {} as CSPA,
  selectedUserRole: {} as RefDataValue,
  userRoles: [],
  entitlements: [],
  entitlementsTextSearch: '',
  defaultEntitlements: [],
  sortBy: { id: 'clientId', order: undefined },
  isLoadingModal: false,
  isLoading: false,
  isGetListCSPAError: false,
  isGetListEntitlementError: false,
  isOpenModal: false,
  listRoleEntitlements: [],
  listRoleCSPA: [],
  isNoResult: false,
  isOpenAddCSPA: false,
  addCSPAErrorMsg: '',
  isOpenStatusModal: false
};

const { actions, reducer } = createSlice({
  name: 'configurationUserRoleEntitlement',
  initialState,
  reducers: {
    onChangeTextSearch: (draftState, action: PayloadAction<string>) => {
      draftState.textSearch = action.payload;
    },
    onChangeCSPA: (draftState, action: PayloadAction<ChangeCSPAPayload>) => {
      const { selectedCSPA } = action.payload;
      draftState.selectedCSPA = selectedCSPA;
    },
    onChangeUserRole: (draftState, action: PayloadAction<RefDataValue>) => {
      draftState.selectedUserRole = action.payload;
    },
    onOpenModalAddCSPA: draftState => {
      return {
        ...draftState,
        isOpenAddCSPA: !draftState.isOpenAddCSPA
      };
    },
    removeModalData: draftState => {
      return {
        ...draftState,
        entitlements: [],
        entitlementsTextSearch: '',
        defaultEntitlements: [],
        isDirtyEntitlements: false
      };
    },
    setAddClientConfigErrorMessage: (
      draftState,
      action: PayloadAction<string>
    ) => {
      return {
        ...draftState,
        addCSPAErrorMsg: action.payload
      };
    },
    onChangeOpenModal: draftState => {
      const { isOpenModal } = draftState;

      return {
        ...draftState,
        isOpenModal: !isOpenModal
      };
    },
    onChangeOpenStatusModal: draftState => {
      const { isOpenStatusModal } = draftState;

      return {
        ...draftState,
        isOpenStatusModal: !isOpenStatusModal
      };
    },
    onChangeEntitlements: (
      draftState,
      action: PayloadAction<{ entitlements: TreeViewValue[] }>
    ) => {
      const { entitlements } = action.payload;

      const nextEntitleElements = draftState.entitlements.map(
        (item: TreeViewValue) => {
          const currentItem = current(item);
          const nextItem = entitlements.find(_ => {
            return currentItem.id === _.id;
          });
          return nextItem ? nextItem : item;
        }
      );

      draftState.entitlements = nextEntitleElements;
    },
    onChangeEntitlementsTextSearch: (
      draftState,
      action: PayloadAction<string>
    ) => {
      draftState.entitlementsTextSearch = action.payload;
    },
    onChangeCollapseAll: (
      draftState,
      action: PayloadAction<{ entitlementsFiltered: TreeViewValue[] }>
    ) => {
      const { entitlementsFiltered } = action.payload;
      const isCollapseAll = entitlementsFiltered.some(item => item.expanded);

      draftState.entitlements.forEach((item: TreeViewValue) => {
        const currentItem = current(item);
        const isExists = entitlementsFiltered.some(itemFromFilterList => {
          return currentItem.id === itemFromFilterList.id;
        });
        if (isExists) {
          item.expanded = !isCollapseAll;
        }
      });
    },
    onChangeSelectAll: (
      draftState,
      action: PayloadAction<{ entitlementsFiltered: TreeViewValue[] }>
    ) => {
      const { entitlementsFiltered } = action.payload;
      const isSelectAll = entitlementsFiltered.some(item => !item.checked);

      draftState.entitlements.forEach((item: TreeViewValue) => {
        const currentItem = current(item);
        const isExists = entitlementsFiltered.some(itemFromFilterList => {
          return currentItem.id === itemFromFilterList.id;
        });
        if (isExists) {
          item.indeterminate = undefined;
          item.checked = isSelectAll;
          item.items = item.items?.map(childrenItem => ({
            ...childrenItem,
            checked: isSelectAll
          }));
        }
      });
    },
    onChangeSort: (draftState, action: PayloadAction<SortType>) => {
      draftState.sortBy = action.payload;
    },
    onRemoveTabEntitlements: () => {
      return { ...initialState };
    }
  },
  extraReducers: builder => {
    getUserRoleEntitlementBuilder(builder);
    getUserRolesBuilder(builder);
    getListCSPABasedRoleBuilder(builder);
    updateEntitlementBuilder(builder);
    addUserRoleCSPABuilder(builder);
    updateActiveDeActiveCSPABuilder(builder);
  }
});

const allActions = {
  ...actions,
  getUserRoles,
  getUserRoleEntitlements,
  getListCSPABasedRole,
  updateEntitlement,
  triggerUpdateEntitlement,
  addUserRoleCSPA,
  triggerAddUserRoleCSPA,
  updateActiveDeActiveCSPA,
  triggerUpdateActiveDeActiveCSPA
};

export {
  allActions as clientConfigurationEntitlementActions,
  reducer as configurationUserRoleEntitlement
};
