import { applyMiddleware, createStore } from '@reduxjs/toolkit';
import { rootReducer } from 'storeConfig';
import { responseDefault } from 'app/test-utils';
import userRoleEntitlementService from '../userRoleEntitlementService';
import thunk from 'redux-thunk';
import { getListCSPABasedRole } from './getListCSPABasedRole';
import { COMPONENTS_MAP } from 'app/entitlements/constants';
import cloneDeep from 'lodash.clonedeep';
import set from 'lodash.set';

window.appConfig = {
  commonConfig: {
    user: 'user',
    app: 'app',
    org: 'org'
  }
} as AppConfiguration;

const initialState: Partial<RootState> = {
  configurationUserRoleEntitlement: {
    isOpenModal: true,
    isGetListCSPAError: false,
    isLoading: false,
    isNoResult: false,
    selectedUserRole: {
      description: 'des',
      value: 'roleA'
    },
    userRoles: [],
    entitlements: [],
    entitlementsTextSearch: 'text search',
    defaultEntitlements: [],
    listRoleEntitlements: [],
    listRoleCSPA: [],
    isGetListEntitlementError: false,
    isOpenAddCSPA: true,
    addCSPAErrorMsg: '',
    isLoadingModal: false,
    textSearch: 'textSearch',
    selectedCSPA: {
      cspaId: '123456'
    },
    sortBy: {
      id: 'id'
    }
  }
};

const store = createStore(rootReducer, initialState, applyMiddleware(thunk));

const roleEntitlements = [
  {
    role: 'roleA',
    cspas: [
      {
        cspa: '123456',
        components: [
          {
            component: COMPONENTS_MAP.ACCOUNT_SEARCH,
            entitlements: { view: true }
          }
        ]
      }
    ]
  }
];

const mockResponse = {
  roleEntitlements
};

const callApi = (isError = false, response: any) => {
  if (!isError) {
    jest.spyOn(userRoleEntitlementService, 'getEntitlement').mockResolvedValue({
      ...responseDefault,
      data: response
    });
  } else {
    jest
      .spyOn(userRoleEntitlementService, 'getEntitlement')
      .mockRejectedValue(new Error('Async Error'));
  }

  return getListCSPABasedRole()(store.dispatch, store.getState, {});
};

describe('getListCSPABasedRole', () => {
  it('It should run correctly', async () => {
    const response = await callApi(false, mockResponse);
    expect(response.payload?.cspas[0].clientId).toEqual('123456');
  });

  it('It should run correctly and cspa is empty', async () => {
    const newRoleEntitlements = cloneDeep(roleEntitlements);
    set(newRoleEntitlements, [0, 'cspas', 0, 'cspa'], undefined);
    const response = await callApi(false, {
      roleEntitlements: newRoleEntitlements
    });
    expect(response.payload?.cspas[0].clientId).toEqual('');
  });

  it('It should run correctly and data is not return', async () => {
    const response = await callApi(false, undefined);
    expect(response.payload?.cspas).toEqual([]);
  });

  it('It should reject', async () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: {}
    })
    const response = await callApi(true, undefined);
    expect(response.payload).toBeUndefined();
  });
});
