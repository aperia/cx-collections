import { applyMiddleware, createStore } from '@reduxjs/toolkit';
import { clientConfigurationEntitlementActions } from './reducer';
import { rootReducer } from 'storeConfig';
import { mockActionCreator, responseDefault } from 'app/test-utils';
import userRoleEntitlementService from '../userRoleEntitlementService';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import thunk from 'redux-thunk';
import { triggerUpdateEntitlement } from './updateEntitlement';

const spyActions = mockActionCreator(actionsToast);

window.appConfig = {
  commonConfig: {
    user: 'user',
    app: 'app',
    org: 'org'
  }
} as AppConfiguration;

describe('Test getCompanies', () => {
  const initialState: Partial<RootState> = {
    configurationUserRoleEntitlement: {
      isOpenModal: true,
      isGetListCSPAError: false,
      isLoading: false,
      isNoResult: false,
      selectedUserRole: {
        description: 'des',
        value: 'value'
      },
      userRoles: [],
      entitlements: [],
      entitlementsTextSearch: 'text search',
      defaultEntitlements: [],
      listRoleEntitlements: [],
      listRoleCSPA: [],
      isGetListEntitlementError: false,
      isOpenAddCSPA: true,
      addCSPAErrorMsg: '',
      isLoadingModal: false,
      textSearch: 'textSearch',
      selectedCSPA: {},
      sortBy: {
        id: 'id'
      }
    }
  };
  const store = createStore(rootReducer, initialState, applyMiddleware(thunk));

  const callApi = (isError = false) => {
    if (!isError) {
      jest
        .spyOn(userRoleEntitlementService, 'updateUserRoleEntitlement')
        .mockResolvedValue({
          ...responseDefault
        });
    } else {
      jest
        .spyOn(userRoleEntitlementService, 'updateUserRoleEntitlement')
        .mockRejectedValue(new Error('Async Error'));
    }

    return clientConfigurationEntitlementActions.updateEntitlement()(
      store.dispatch,
      store.getState,
      {}
    );
  };

  it('Should have callApi', async () => {
    const toastSuccess = spyActions('addToast');
    await callApi();
    expect(toastSuccess).not.toBeCalled();
  });

  it('Should have error', async () => {
    const toastSuccess = spyActions('addToast');
    await callApi(true);
    expect(toastSuccess).not.toBeCalled();
  });

  it('Should have fulfilled', async () => {
    const actions =
      clientConfigurationEntitlementActions.updateEntitlement.fulfilled(
        {},
        'userRoleEntitlement/updateEntitlement',
        undefined
      );

    const actual = rootReducer(
      store.getState(),
      actions
    ).configurationUserRoleEntitlement;

    expect(actual.isLoadingModal).toEqual(false);
    expect(actual.isOpenModal).toEqual(false);
  });

  it('Should have pending', async () => {
    const actions =
      clientConfigurationEntitlementActions.updateEntitlement.pending(
        'userRoleEntitlement/updateEntitlement',
        undefined
      );

    const actual = rootReducer(
      store.getState(),
      actions
    ).configurationUserRoleEntitlement;

    expect(actual.isLoadingModal).toEqual(true);
  });

  it('Should have reject', async () => {
    const actions =
      clientConfigurationEntitlementActions.updateEntitlement.rejected(
        null,
        'userRoleEntitlement/updateEntitlement',
        undefined
      );

    const actual = rootReducer(
      store.getState(),
      actions
    ).configurationUserRoleEntitlement;

    expect(actual.isLoadingModal).toEqual(false);
  });

  it('triggerUpdateEntitlement fullfiled', async () => {
    const toastSuccess = spyActions('addToast');
    jest
      .spyOn(userRoleEntitlementService, 'updateUserRoleEntitlement')
      .mockResolvedValue({
        ...responseDefault
      });
    await triggerUpdateEntitlement()(store.dispatch, store.getState, {});
    expect(toastSuccess).toBeCalledWith({
      show: true,
      type: 'success',
      message: 'txt_user_role_entitlement_update_success'
    });
  });

  it('triggerUpdateEntitlement reject', async () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: {}
    })
    const toastSuccess = spyActions('addToast');
    jest
      .spyOn(userRoleEntitlementService, 'updateUserRoleEntitlement')
      .mockRejectedValue({
        ...responseDefault
      });
    await triggerUpdateEntitlement()(store.dispatch, store.getState, {});
    expect(toastSuccess).toBeCalledWith({
      show: true,
      type: 'error',
      message: 'txt_user_role_entitlement_update_failed'
    });
  });
});
