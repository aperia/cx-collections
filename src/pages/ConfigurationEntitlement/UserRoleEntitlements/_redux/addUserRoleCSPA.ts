import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';

import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { saveChangedClientConfig } from 'pages/ClientConfiguration/ChangeHistory/_redux/saveChangedClientConfig';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';

// Helpers
import { buildUserRoleCSPA } from '../helpers';

// Type
import {
  AddUserRoleCSPAArgs,
  GetListCSPABasedRolePayload,
  UserRoleEntitlementState
} from '../types';

// Service
import userRoleEntitlementService from '../userRoleEntitlementService';

// Reducer
import { clientConfigurationEntitlementActions } from './reducer';

export const addUserRoleCSPA = createAsyncThunk<
  GetListCSPABasedRolePayload,
  AddUserRoleCSPAArgs,
  ThunkAPIConfig
>('userRoleEntitlement/addUserRoleCSPA', async (args, thunkAPI) => {
  try {
    const { getState } = thunkAPI;
    const { configurationUserRoleEntitlement, accessConfig } = getState();

    const { listRoleCSPA, selectedUserRole } = configurationUserRoleEntitlement;
    const defaultCSPA = listRoleCSPA.find(
      item => !!item.isDefaultClientInfoKey
    );
    const { agentId, clientId, description, principleId, systemId } = args;
    const cspa = `${clientId}-${systemId}-${principleId}-${agentId}`;
    const { privileges } = window.appConfig.commonConfig || {};
    const [
      clientNumber = '1234',
      system = '$ALL',
      prin = '$ALL',
      agent = '$ALL'
    ] = (accessConfig as any)?.cspaList?.[0]?.cspa?.split('-') || [];

    const { data } = await userRoleEntitlementService.addUserRoleCSPA({
      defaultCspa:
        (accessConfig as any)?.cspaList?.[0]?.cspa || defaultCSPA?.cspaId || '',
      common: {
        privileges,
        agent,
        clientNumber,
        system,
        prin,
        user: window.appConfig.commonConfig.user,
        app: window.appConfig.commonConfig.app,
        org: window.appConfig.commonConfig.org
      },
      roleEntitlementsRequest: {
        roleEntitlements: [
          {
            role: selectedUserRole?.value,
            cspas: [
              {
                cspa,
                description,
                active: true
              }
            ]
          }
        ]
      }
    });

    const { roleEntitlements = [] } = data || {};

    const cspas = buildUserRoleCSPA(roleEntitlements, selectedUserRole);

    return {
      cspas
    };
  } catch (error) {
    throw thunkAPI.rejectWithValue(error);
  }
});

export const addUserRoleCSPABuilder = (
  builder: ActionReducerMapBuilder<UserRoleEntitlementState>
) => {
  const { pending, fulfilled, rejected } = addUserRoleCSPA;
  builder.addCase(pending, draftState => {
    draftState.isLoadingModal = true;
  });
  builder.addCase(fulfilled, (draftState, action) => {
    const { cspas } = action.payload;

    return {
      ...draftState,
      listRoleCSPA: cspas,
      isNoResult: !cspas.length,
      isLoadingModal: false
    };
  });
  builder.addCase(rejected, draftState => {
    draftState.isLoadingModal = false;
  });
};

export const triggerAddUserRoleCSPA = createAsyncThunk<
  unknown,
  AddUserRoleCSPAArgs,
  ThunkAPIConfig
>('userRoleEntitlement/triggerAddUserRoleCSPA', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;

  const { configurationUserRoleEntitlement } = getState();
  const { listRoleCSPA, selectedUserRole } = configurationUserRoleEntitlement;
  const { agentId, clientId, principleId, systemId, description } = args;
  const cspa = `${clientId}-${systemId}-${principleId}-${agentId}`;
  const isExistCSPA = listRoleCSPA.findIndex(item => item.id === cspa) !== -1;

  if (isExistCSPA) {
    dispatch(
      clientConfigurationEntitlementActions.setAddClientConfigErrorMessage(
        'txt_cspa_already_exists'
      )
    );
    return;
  } else {
    dispatch(
      clientConfigurationEntitlementActions.setAddClientConfigErrorMessage('')
    );
  }

  const response = await dispatch(addUserRoleCSPA(args));

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'txt_cspa_added'
        })
      );
      dispatch(clientConfigurationEntitlementActions.onOpenModalAddCSPA());
      dispatch(
        saveChangedClientConfig({
          action: 'ADD',
          changedCategory: 'userRoleEntitlement',
          changedSection: {
            userRole: selectedUserRole.value
          },
          newValue: {
            cspa,
            description
          }
        })
      );
    });
  }

  if (isRejected(response)) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: 'txt_cspa_failed_to_add'
      })
    );

    dispatch(
      apiErrorNotificationAction.updateThunkApiError({
        storeId: API_ERROR_STORE_ID.ENTITLEMENT,
        forSection: 'inModalBody',
        rejectData: {
          ...response,
          payload: response.payload?.response
        }
      })
    );
  }
});
