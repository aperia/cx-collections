import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// Const
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';

// Redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// Helper
import { buildUserRoleCSPA } from '../helpers';

// Type
import {
  GetListCSPABasedRolePayload,
  UpdateStatusUserRoleCSPAArgs,
  UserRoleEntitlementState
} from '../types';

// Redux
import userRoleEntitlementService from '../userRoleEntitlementService';
import { clientConfigurationEntitlementActions } from './reducer';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { saveChangedClientConfig } from 'pages/ClientConfiguration/ChangeHistory/_redux/saveChangedClientConfig';

export const updateActiveDeActiveCSPA = createAsyncThunk<
  GetListCSPABasedRolePayload,
  UpdateStatusUserRoleCSPAArgs,
  ThunkAPIConfig
>('userRoleEntitlement/updateActiveDeactiveCSPA', async (args, thunkAPI) => {
  try {
    const { getState } = thunkAPI;
    const { configurationUserRoleEntitlement } = getState();

    const { selectedUserRole } = configurationUserRoleEntitlement;
    const { cspa: selectedCSPA, status } = args;
    const { agentId, clientId, description, principleId, systemId, cspaId } =
      selectedCSPA;
    const { privileges } = window.appConfig.commonConfig || {};

    const { data } = await userRoleEntitlementService.updateUserRoleCSPA({
      common: {
        privileges,
        agent: agentId,
        clientNumber: clientId,
        system: systemId,
        prin: principleId,
        user: window.appConfig.commonConfig.user,
        app: window.appConfig.commonConfig.app,
        org: window.appConfig.commonConfig.org
      },
      roleEntitlementsRequest: {
        roleEntitlements: [
          {
            role: selectedUserRole?.value,
            cspas: [
              {
                cspa: cspaId,
                description,
                active: status
              }
            ]
          }
        ]
      }
    });

    const { roleEntitlements = [] } = data || {};

    const cspas = buildUserRoleCSPA(roleEntitlements, selectedUserRole);

    return {
      cspas
    };
  } catch (error) {
    throw thunkAPI.rejectWithValue(error);
  }
});

export const updateActiveDeActiveCSPABuilder = (
  builder: ActionReducerMapBuilder<UserRoleEntitlementState>
) => {
  const { pending, fulfilled, rejected } = updateActiveDeActiveCSPA;
  builder.addCase(pending, draftState => {
    draftState.isLoadingModal = true;
  });
  builder.addCase(fulfilled, (draftState, action) => {
    const { cspas } = action.payload;

    return {
      ...draftState,
      listRoleCSPA: cspas,
      isNoResult: !cspas.length,
      isLoadingModal: false
    };
  });
  builder.addCase(rejected, draftState => {
    draftState.isLoadingModal = false;
  });
};

export const triggerUpdateActiveDeActiveCSPA = createAsyncThunk<
  void,
  UpdateStatusUserRoleCSPAArgs,
  ThunkAPIConfig
>(
  'userRoleEntitlement/triggerUpdateActiveDeActiveCSPA',
  async (args, thunkAPI) => {
    const { dispatch, getState } = thunkAPI;
    const { status } = args;
    const { configurationUserRoleEntitlement } = getState();

    const { selectedUserRole } = configurationUserRoleEntitlement;

    const response = await dispatch(updateActiveDeActiveCSPA(args));

    if (isFulfilled(response)) {
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: status ? 'txt_cspa_activated' : 'txt_cspa_deactivated'
          })
        );
        dispatch(
          clientConfigurationEntitlementActions.onChangeOpenStatusModal()
        );
        dispatch(
          saveChangedClientConfig({
            action: status ? 'DEACTIVATE' : 'ACTIVATE',
            changedCategory: 'userRoleEntitlement',
            changedSection: {
              userRole: selectedUserRole.value
            },
            oldValue: {
              active: status
            },
            newValue: {
              active: !status
            }
          })
        );
      });
    }

    if (isRejected(response)) {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: status
            ? 'txt_cspa_failed_to_activate'
            : 'txt_cspa_failed_to_deactivate'
        })
      );

      dispatch(
        apiErrorNotificationAction.updateThunkApiError({
          storeId: API_ERROR_STORE_ID.ENTITLEMENT,
          forSection: 'inModalBody',
          rejectData: {
            ...response,
            payload: response.payload?.response
          }
        })
      );
    }
  }
);
