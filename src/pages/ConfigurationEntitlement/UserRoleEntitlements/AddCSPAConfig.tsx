// Const
import { CODE_ALL } from 'pages/ClientConfiguration/constants';
import { selectCSPAFromAccessConfig } from 'pages/__commons/AccessConfig/_redux/selector';
import React from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';
// Component
import AddCSPAModal from '../Components/AddCSPAModal';
import { clientConfigurationEntitlementActions } from './_redux/reducer';
// Selector
import {
  selectLoadingModal,
  takeAddErrorMsg,
  takeOpenAddCSPAStatus
} from './_redux/selector';

export interface AddCSPAConfigProps {}

const AddCSPAConfig: React.FC<AddCSPAConfigProps> = () => {
  const dispatch = useDispatch();

  const isOpenAddCSPA = useSelector<RootState, boolean>(takeOpenAddCSPAStatus);
  const isModalLoading = useSelector<RootState, boolean>(selectLoadingModal);
  const addErrorMsg = useSelector<RootState, string>(takeAddErrorMsg);
  const defaultClientId = useSelector(selectCSPAFromAccessConfig).clientNumber;

  const handleCloseModal = () => {
    batch(() => {
      dispatch(clientConfigurationEntitlementActions.onOpenModalAddCSPA());
      dispatch(
        clientConfigurationEntitlementActions.setAddClientConfigErrorMessage('')
      );
    });
  };

  const handleOnSubmit = (formValue: FormCSPAValue) => {
    dispatch(
      clientConfigurationEntitlementActions.triggerAddUserRoleCSPA({
        clientId: defaultClientId!,
        systemId: formValue.systemId!.toString(),
        principleId: formValue.principleRadio?.custom
          ? formValue.principleId!.toString()
          : CODE_ALL,
        agentId: formValue.agentRadio?.custom
          ? formValue.agentId!.toString()
          : CODE_ALL,
        description: formValue.description!.toString()
      })
    );
  };

  return (
    <AddCSPAModal
      errorMessage={addErrorMsg}
      isOpenModal={isOpenAddCSPA}
      isModalLoading={isModalLoading}
      onCloseModal={handleCloseModal}
      onSubmit={handleOnSubmit}
      defaultClientId={defaultClientId}
    />
  );
};

export default AddCSPAConfig;
