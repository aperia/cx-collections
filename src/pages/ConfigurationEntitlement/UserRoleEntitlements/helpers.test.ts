import {
  COMPONENTS_MAP,
  ENTITLEMENT_COMPONENTS,
  PERMISSIONS
} from 'app/entitlements/constants';

import cloneDeep from 'lodash.clonedeep';
import set from 'lodash.set';
import {
  buildListRoleEntitlements,
  convertRoleEntitlementToTreeView,
  buildUpdateRoleEntitlement,
  covertAPIDataToAppRoleEntitlement,
  covertAppRoleDataToAPI,
  convertTreeViewToRoleEntitlement
} from './helpers';
import { APIComponentRoleEntitlement, APIRoleEntitlement } from './types';

const entitlementItems = [
  {
    entitlementCode: PERMISSIONS.ACCOUNT_SEARCH_VIEW,
    active: true,
    checked: true
  },
  {
    active: false,
    checked: true
  }
];

const entitlementsArgs: Array<EntitlementConfig> = [
  {
    component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION,
    entitlements: {
      items: entitlementItems
    }
  },
  {
    entitlements: {
      items: entitlementItems
    }
  }
];
const cspaItems = [
  {
    cspa: 'cspa-123456',
    components: [
      {
        component: 'component',
        entitlements: {
          entitlement: true
        }
      }
    ]
  },
  {
    cspa: 'cspa-123456',
    components: []
  }
];
const roleArgs: Array<APIRoleEntitlement> = [
  {
    role: 'roleA',
    cspas: cspaItems
  }
];

const translateFn = jest
  .fn()
  .mockImplementation(() => ({ t: (text: string) => text }));

describe('Test helper ', () => {
  it('buildListRoleEntitlements', () => {
    const response = buildListRoleEntitlements(roleArgs);
    expect(response[0].role).toEqual('roleA');
  });

  it('buildUpdateRoleEntitlement', () => {
    const selectedUserRole = {
      description: 'description',
      value: 'roleA'
    };
    const treeViewItem = [
      {
        id: 'id-1',
        label: 'label-1',
        indeterminate: false,
        expanded: false,
        checked: false
      }
    ];
    const entitlementsTree = [
      {
        id: 'id',
        label: 'string',
        indeterminate: false,
        expanded: false,
        checked: false,
        items: treeViewItem
      },
      {
        id: '',
        label: '',
        indeterminate: false,
        expanded: false,
        checked: false,
        items: treeViewItem
      }
    ];
    const response = buildUpdateRoleEntitlement(
      selectedUserRole,
      {},
      entitlementsTree
    );
    expect(response[0].role).toEqual('roleA');
  });

  it('covertAPIDataToAppRoleEntitlement', () => {
    const componentEntitlement: APIComponentRoleEntitlement[] = [
      {
        component: COMPONENTS_MAP.ACCOUNT_SEARCH,
        entitlements: { view: true }
      },
      {
        component: COMPONENTS_MAP.ACCOUNT_INFORMATION,
        entitlements: { view: false }
      },
      {
        component: 'componentC'
      },
      {
        component: COMPONENTS_MAP.ACCOUNT_SNAPSHOT,
        entitlements: { view: true }
      },
      {
        component: COMPONENTS_MAP.CLIENT_CONTACT_INFO,
        entitlements: { notExistFeature: true }
      }
    ];
    const response = covertAPIDataToAppRoleEntitlement(componentEntitlement);
    expect(response[0].component).toEqual('AccountInformation');
  });

  it('convertRoleEntitlementToTreeView', () => {
    const expected = [
      {
        id: 'AccountInformation',
        expanded: true,
        checked: false,
        indeterminate: true,
        items: [
          {
            id: 'AccountSearch.View',
            checked: true,
            label: undefined,
            readOnly: true
          },
          { checked: false, id: undefined, label: undefined, readOnly: true }
        ],
        label: undefined,
        readOnly: true
      },
      {
        id: '',
        expanded: true,
        checked: false,
        indeterminate: true,
        items: [
          {
            id: 'AccountSearch.View',
            checked: true,
            label: undefined,
            readOnly: true
          },
          { checked: false, id: undefined, label: undefined, readOnly: true }
        ],
        label: undefined,
        readOnly: true
      }
    ];
    const res = convertRoleEntitlementToTreeView(
      entitlementsArgs,
      {},
      translateFn
    );
    expect(res).toEqual(expected);
  });

  it('convertRoleEntitlementToTreeView > entitlements is empty', () => {
    const newEntitlementsArgs = cloneDeep(entitlementsArgs);
    set(newEntitlementsArgs, [1], []);
    const expected = [
      {
        id: 'AccountInformation',
        expanded: true,
        checked: false,
        indeterminate: true,
        items: [
          {
            id: 'AccountSearch.View',
            checked: true,
            readOnly: true,
            label: undefined
          },
          { checked: false, id: undefined, label: undefined, readOnly: true }
        ],
        label: undefined,
        readOnly: true
      },
      {
        id: '',
        expanded: true,
        checked: true,
        items: [],
        indeterminate: undefined,
        label: undefined,
        readOnly: true
      }
    ];
    const res = convertRoleEntitlementToTreeView(
      newEntitlementsArgs,
      {},
      translateFn
    );
    expect(res).toEqual(expected);
  });

  it('covertAppRoleDataToAPI > when component and entitlementCode are undefined', () => {
    const componentEntitilement: Array<EntitlementConfig> = [
      {
        entitlements: {
          items: [{ entitlementCode: undefined, active: false }]
        }
      }
    ];
    const response = covertAppRoleDataToAPI(componentEntitilement);
    expect(response).not.toEqual([]);
  });

  it('convertTreeViewToRoleEntitlement > when component and entitlementCode are undefined', () => {
    const response = convertTreeViewToRoleEntitlement([
      {
        items: [{}]
      }
    ] as any);
    expect(response).not.toEqual([]);
  });
});
