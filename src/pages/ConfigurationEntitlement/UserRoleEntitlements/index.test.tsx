import React from 'react';
import { waitFor } from '@testing-library/react';
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import { clientConfigurationEntitlementActions } from './_redux/reducer';
import UserRoleEntitlement from '.';
import cloneDeep from 'lodash.clonedeep';
import set from 'lodash.set';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('./EntitlementSettings', () => {
  return () => <div>EntitlementSettings</div>;
});

const initialState: Partial<RootState> = {
  configurationUserRoleEntitlement: {
    isOpenModal: true,
    isGetListCSPAError: false,
    isLoading: false,
    isNoResult: false,
    selectedUserRole: {
      description: 'des',
      value: 'value'
    },
    userRoles: [
      {
        description: 'des',
        value: 'value'
      },
      {
        description: 'des',
        value: 'value 2'
      }
    ],
    entitlements: [
      {
        id: '1',
        label: 'label',
        indeterminate: false,
        expanded: false,
        checked: false,
        items: [
          {
            id: '1c',
            label: 'label',
            indeterminate: false,
            expanded: false,
            checked: false
          }
        ]
      },
      {
        id: '2',
        label: 'label',
        indeterminate: false,
        expanded: false,
        checked: false,
        items: [
          {
            id: '2c',
            label: 'label',
            indeterminate: false,
            expanded: false,
            checked: false
          }
        ]
      }
    ],
    entitlementsTextSearch: 'text search',
    defaultEntitlements: [],
    listRoleEntitlements: [],
    listRoleCSPA: [
      {
        isDefaultClientInfoKey: true
      }
    ],
    isGetListEntitlementError: false,
    isOpenAddCSPA: false,
    addCSPAErrorMsg: '',
    isLoadingModal: false,
    textSearch: 'textSearch',
    selectedCSPA: {},
    sortBy: {
      id: 'id'
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<UserRoleEntitlement />, { initialState });
};

const mockSpy = mockActionCreator(clientConfigurationEntitlementActions);

describe('Test UserRoleEntitlement', () => {
  it('Render UI', () => {
    waitFor(() => {
      const { wrapper } = renderWrapper(initialState);
      expect(
        wrapper.getByText('txt_user_role_entitlement')
      ).toBeInTheDocument();
    });
  });

  it('Render with empty selectedRole', () => {
    const newState = cloneDeep(initialState);
    set(
      newState,
      ['configurationUserRoleEntitlement', 'selectedUserRole'],
      undefined
    );
    waitFor(() => {
      const { wrapper } = renderWrapper(newState);
      expect(
        wrapper.getByText('txt_user_role_entitlement')
      ).toBeInTheDocument();
    });
  });

  it('Render with error & onReload', () => {
    const spy = mockSpy('getListCSPABasedRole');
    const newState = cloneDeep(initialState);
    set(
      newState,
      ['configurationUserRoleEntitlement', 'isGetListCSPAError'],
      true
    );
    const { wrapper } = renderWrapper(newState);
    expect(wrapper.getByText('txt_user_role_entitlement')).toBeInTheDocument();

    const button = wrapper.getByText('txt_reload')!;
    button.click();

    expect(spy).toHaveBeenCalled();
  });

  it('Render with isNoData & !loading', () => {
    const newState = cloneDeep(initialState);
    set(newState, ['configurationUserRoleEntitlement', 'isNoResult'], true);
    const { wrapper } = renderWrapper(newState);

    expect(wrapper.getByText('txt_cspa_list')).toBeInTheDocument();
  });

  it('should handleOnAddCSPA', () => {
    // prevent changing isLoadingListCSPA
    mockSpy('getUserRoles');
    const spy = mockSpy('onOpenModalAddCSPA');
    const newState = cloneDeep(initialState);
    set(newState, ['configurationUserRoleEntitlement', 'isNoResult'], true);
    set(
      newState,
      ['configurationUserRoleEntitlement', 'isLoadingListCSPA'],
      false
    );

    const { wrapper } = renderWrapper(newState);

    const button = wrapper.getByText('txt_add_cspa');
    button.click();

    expect(spy).toHaveBeenCalled();
  });
});
