import { apiService } from 'app/utils/api.service';

// Types
import {
  AddUserRoleCSPA,
  GetRoleEntitlementRequest,
  GetRoleEntitlementResponse,
  GetRoleEntitlementRoleCSPARequest,
  UpdateStatusUserRoleCSPA,
  UpdateUserRoleEntitlementRequest
} from './types';

const userRoleEntitlementService = {
  getEntitlement(body: GetRoleEntitlementRequest) {
    const { getEntitlement = '' } =
      window.appConfig?.api?.userRoleEntitlement || {};
    return apiService.post<GetRoleEntitlementResponse>(getEntitlement, body);
  },

  getEntitlementBasedOnRoleCSPA(body: GetRoleEntitlementRoleCSPARequest) {
    const { getEntitlement = '' } =
      window.appConfig?.api?.userRoleEntitlement || {};
    return apiService.post<GetRoleEntitlementResponse>(getEntitlement, body);
  },

  updateUserRoleEntitlement(data: UpdateUserRoleEntitlementRequest) {
    const { updateUserRoleEntitlement = '' } =
      window.appConfig?.api?.userRoleEntitlement || {};
    return apiService.post(updateUserRoleEntitlement, data);
  },

  addUserRoleCSPA(data: AddUserRoleCSPA) {
    const { updateUserRoleEntitlement = '' } =
      window.appConfig?.api?.userRoleEntitlement || {};
    return apiService.post<GetRoleEntitlementResponse>(
      updateUserRoleEntitlement,
      data
    );
  },

  updateUserRoleCSPA(data: UpdateStatusUserRoleCSPA) {
    const { updateUserRoleEntitlement = '' } =
      window.appConfig?.api?.userRoleEntitlement || {};
    return apiService.post<GetRoleEntitlementResponse>(
      updateUserRoleEntitlement,
      data
    );
  }
};

export default userRoleEntitlementService;
