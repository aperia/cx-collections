import React from 'react';
import { Button } from 'app/_libraries/_dls/components';
import { batch, useDispatch } from 'react-redux';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Const
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// Redux
import { clientConfigurationEntitlementActions } from './_redux/reducer';

export interface ActionsProps {
  data: CSPA;
}

const Actions: React.FC<ActionsProps> = ({ data }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const isNotDefaultCSPA = !data.isDefaultClientInfoKey;
  const isActive = !!data.active;

  const handleViewSettings = (data: CSPA) => {
    batch(() => {
      dispatch(clientConfigurationEntitlementActions.onChangeOpenModal());
      dispatch(
        clientConfigurationEntitlementActions.onChangeCSPA({
          selectedCSPA: data
        })
      );
    });
  };

  const handleChangeStatus = () => {
    batch(() => {
      dispatch(clientConfigurationEntitlementActions.onChangeOpenStatusModal());
      dispatch(
        clientConfigurationEntitlementActions.onChangeCSPA({
          selectedCSPA: data
        })
      );
    });
  };

  return (
    <>
      {isNotDefaultCSPA && (
        <Button
          variant={isActive ? 'outline-danger' : 'outline-primary'}
          size="sm"
          onClick={handleChangeStatus}
        >
          {isActive
            ? t(I18N_COMMON_TEXT.DEACTIVATE)
            : t(I18N_COMMON_TEXT.ACTIVATE)}
        </Button>
      )}
      <Button
        size="sm"
        onClick={() => handleViewSettings(data)}
        variant="outline-primary"
      >
        {t('txt_view_settings')}
      </Button>
    </>
  );
};

export default Actions;
