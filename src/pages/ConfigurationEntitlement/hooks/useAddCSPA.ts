import findIndex from 'lodash.findindex';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useFormik } from 'formik';
import { ChangeEvent } from 'react';

// Const
import { NUMERIC_REGEX } from 'app/constants';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { CSPA_FORM_MAX_LENGTH, CSPA_FORM_FIELDS } from '../constants';
import { REGEX_CSPA_DESCRIPTION } from 'pages/ClientConfiguration/constants';

const {
  CLIENT_RADIO,
  SYSTEM_RADIO,
  PRINCIPLE_RADIO,
  AGENT_RADIO,
  PRINCIPLE_ID,
  AGENT_ID
} = CSPA_FORM_FIELDS;

const initialValues: FormCSPAValue = {
  clientRadio: { all: true, custom: false },
  clientId: '',
  systemId: '',
  systemRadio: { all: true, custom: false },
  principleId: '',
  principleRadio: { all: true, custom: false },
  agentId: '',
  agentRadio: { all: true, custom: false },
  description: ''
};

export const useAddCSPA = (onSubmit: (values: FormCSPAValue) => void) => {
  const { t } = useTranslation();
  const validate = ({
    systemId,
    principleRadio,
    principleId,
    agentRadio,
    agentId,
    description
  }: FormCSPAValue) => {
    const nextErrors = {} as Record<FormCSPAKy, string>;

    if (!description || !description.trim()) {
      nextErrors.description = t(I18N_CLIENT_CONFIG.DESCRIPTION_IS_REQUIRED);
    }
    if (systemId && systemId.length < CSPA_FORM_MAX_LENGTH) {
      nextErrors.systemId = t(I18N_COMMON_TEXT.INVALID_FORMAT);
    }
    if (!systemId) {
      nextErrors.systemId = t(I18N_CLIENT_CONFIG.SYSTEM_ID_IS_REQUIRED);
    }
    if (
      principleRadio.custom &&
      principleId &&
      principleId.length < CSPA_FORM_MAX_LENGTH
    ) {
      nextErrors.principleId = t(I18N_COMMON_TEXT.INVALID_FORMAT);
    }
    if (principleRadio.custom && !principleId) {
      nextErrors.principleId = t(I18N_CLIENT_CONFIG.PRINCIPLE_ID_IS_REQUIRED);
    }
    if (agentRadio.custom && agentId && agentId.length < CSPA_FORM_MAX_LENGTH) {
      nextErrors.agentId = t(I18N_COMMON_TEXT.INVALID_FORMAT);
    }
    if (agentRadio.custom && !agentId) {
      nextErrors.agentId = t(I18N_CLIENT_CONFIG.AGENT_ID_IS_REQUIRED);
    }

    return nextErrors;
  };

  const {
    values,
    setFieldValue,
    setValues,
    handleReset,
    handleChange,
    handleBlur,
    handleSubmit,
    validateForm,
    errors,
    touched
  } = useFormik({
    initialValues,
    validateOnMount: true,
    validateOnBlur: true,
    validateOnChange: false,
    onSubmit,
    validate
  });

  /**
   * Auto set radio value to '$ALL' for the fields that followed by @param fieldName
   * in the field order list
   */
  const handleAutoSetAllValue = (fieldName: RadioCSPAFieldKy) => {
    const FIELD_ORDER: Array<FormCSPAKy> = [
      CLIENT_RADIO,
      SYSTEM_RADIO,
      PRINCIPLE_RADIO,
      AGENT_RADIO
    ];

    const fieldIndex = findIndex(FIELD_ORDER, field => field === fieldName);
    const fieldListNeededToAutoSetValue = FIELD_ORDER.slice(
      fieldIndex,
      FIELD_ORDER.length
    );

    fieldListNeededToAutoSetValue.forEach(currentFieldName => {
      setFieldValue(currentFieldName as string, {
        all: true,
        custom: false
      });
    });
  };

  const handleChangeRadioOptionAll = (e: ChangeEvent<HTMLInputElement>) => {
    const fieldName = e.target.name;
    setFieldValue(fieldName, {
      all: true,
      custom: false
    });
    handleAutoSetAllValue(fieldName as RadioCSPAFieldKy);
    if (fieldName === PRINCIPLE_RADIO) {
      setFieldValue(PRINCIPLE_ID, '');
      setFieldValue(AGENT_ID, '');
      delete errors.principleId;
      delete errors.agentId;
    }

    if (fieldName === AGENT_RADIO) {
      setFieldValue(AGENT_ID, '');
      delete errors.agentId;
    }
  };

  const handleChangeRadioOptionCustom = (e: ChangeEvent<HTMLInputElement>) => {
    const fieldName = e.target.name;
    setFieldValue(fieldName, {
      all: false,
      custom: true
    });
  };

  const handleChangeTextBox = (e: ChangeEvent<HTMLInputElement>) => {
    const fieldName = e.target.name;
    const { value } = e.target;
    const numberRegex = new RegExp(NUMERIC_REGEX);
    if (numberRegex.test(value)) {
      setFieldValue(fieldName, value);
    }
  };

  const handleChangeDescription = (e: ChangeEvent<HTMLInputElement>) => {
    const fieldName = e.target.name;
    const { value } = e.target;
    const description = new RegExp(REGEX_CSPA_DESCRIPTION);
    if (description.test(value)) {
      setFieldValue(fieldName, value);
    }
  };

  return {
    handleChangeRadioOptionAll,
    handleChangeRadioOptionCustom,
    handleChangeDescription,
    handleChangeTextBox,
    values,
    setFieldValue,
    setValues,
    handleReset,
    handleChange,
    handleBlur,
    handleSubmit,
    validateForm,
    errors,
    touched
  };
};
