import React from 'react';
import { renderMockStore } from 'app/test-utils';

import { SECTION_TAB_EVENT } from '../constants';
import { useListenClosingModal } from './useListenClosingModal';

describe('Test useListenClosingModal', () => {
  const mockCallback = jest.fn();

  const closingModal = () => {
    const event = new CustomEvent(SECTION_TAB_EVENT.CLOSING_MODAL, {
      bubbles: true,
      cancelable: true
    });
    window.dispatchEvent(event);
  };

  const Component: React.FC = () => {
    useListenClosingModal(mockCallback);
    return null;
  };

  it('should run callback when event emmited', () => {
    renderMockStore(<Component />);

    closingModal();

    expect(mockCallback).toHaveBeenCalled();
  });

  it('should not run callback when component unmount', () => {
    const { wrapper } = renderMockStore(<Component />);

    wrapper.unmount();

    closingModal();

    expect(mockCallback).not.toHaveBeenCalled();
  });
});
