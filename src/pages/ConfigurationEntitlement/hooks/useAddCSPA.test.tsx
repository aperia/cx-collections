import React, { ChangeEvent, useState } from 'react';
import { render, screen } from '@testing-library/react';
import * as formik from 'formik';

import userEvent from '@testing-library/user-event';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { useAddCSPA } from './useAddCSPA';
import { CSPA_FORM_FIELDS } from '../constants';
import { FormValue } from 'pages/ClientConfiguration/types';

const RenderComponent = ({
  onSubmit
}: {
  onSubmit: (values: FormValue) => void;
}) => {
  const [error, setError] = useState({});

  const {
    handleChangeRadioOptionAll,
    handleChangeRadioOptionCustom,
    handleChangeTextBox,
    handleChangeDescription,
    validateForm
  } = useAddCSPA(onSubmit);

  const validateRequired = () => {
    const err = validateForm({
      clientRadio: { all: false, custom: false },
      clientId: 'cId',
      systemRadio: { all: false, custom: false },
      systemId: '',
      principleRadio: { all: false, custom: true },
      principleId: '',
      agentRadio: { all: false, custom: true },
      agentId: '',
      description: ' '
    });

    setError(err);
  };

  const validateInvalidFormat = () => {
    const err = validateForm({
      clientRadio: { all: false, custom: false },
      clientId: 'cId',
      systemRadio: { all: false, custom: false },
      systemId: 'sId',
      principleRadio: { all: false, custom: true },
      principleId: 'pId',
      agentRadio: { all: false, custom: true },
      agentId: 'aId',
      description: 'description'
    });

    setError(err);
  };

  const selectAllPrinciple = () => {
    handleChangeRadioOptionAll({
      target: { name: CSPA_FORM_FIELDS.PRINCIPLE_RADIO }
    } as ChangeEvent<HTMLInputElement>);
  };
  const selectAllAgentRadio = () => {
    handleChangeRadioOptionAll({
      target: { name: CSPA_FORM_FIELDS.AGENT_RADIO }
    } as ChangeEvent<HTMLInputElement>);
  };

  const selectRadioCustom = () => {
    handleChangeRadioOptionCustom({
      target: { name: CSPA_FORM_FIELDS.AGENT_RADIO }
    } as ChangeEvent<HTMLInputElement>);
  };
  const changeTextBox = () => {
    handleChangeTextBox({
      target: { name: CSPA_FORM_FIELDS.AGENT_RADIO, value: '90' }
    } as ChangeEvent<HTMLInputElement>);
  };

  const changeTextBoxWithAlphabet = () => {
    handleChangeTextBox({
      target: { name: CSPA_FORM_FIELDS.AGENT_RADIO, value: 'abc' }
    } as ChangeEvent<HTMLInputElement>);
  };

  const changeDescription = () => {
    handleChangeDescription({
      target: { name: 'name', value: '123' }
    } as ChangeEvent<HTMLInputElement>);
  };

  const changeDescriptionWithSpecialCharacters = () => {
    handleChangeDescription({
      target: { name: 'name', value: '%^&' }
    } as ChangeEvent<HTMLInputElement>);
  };

  return (
    <div>
      <div data-testid="errors">{JSON.stringify(error)}</div>
      <div data-testid="validateRequired" onClick={validateRequired} />
      <div
        data-testid="validateInvalidFormat"
        onClick={validateInvalidFormat}
      />
      <div data-testid="selectAllPrinciple" onClick={selectAllPrinciple} />
      <div data-testid="selectAllAgentRadio" onClick={selectAllAgentRadio} />
      <div data-testid="selectRadioCustom" onClick={selectRadioCustom} />
      <div data-testid="changeTextBox" onClick={changeTextBox} />
      <div
        data-testid="changeTextBoxWithAlphabet"
        onClick={changeTextBoxWithAlphabet}
      />
      <div data-testid="changeDescription" onClick={changeDescription} />

      <div
        data-testid="changeDescriptionWithSpecialCharacters"
        onClick={changeDescriptionWithSpecialCharacters}
      />
    </div>
  );
};
const setFieldValue = jest.fn();

beforeEach(() => {
  setFieldValue.mockClear();

  jest.spyOn(formik, 'useFormik').mockImplementation((({
    validate
  }: {
    validate: Function;
  }) => {
    return {
      values: {},
      setFieldValue,
      setValues: jest.fn(),
      handleReset: jest.fn(),
      handleChange: jest.fn(),
      handleBlur: jest.fn(),
      handleSubmit: jest.fn(),
      validateForm: validate,
      errors: {},
      touched: {}
    };
  }) as any);
});

describe('useAddCSPA', () => {
  it('validate required', () => {
    const onSubmit = jest.fn();
    render(<RenderComponent onSubmit={onSubmit} />);
    let errors = screen.getByTestId('errors').textContent!;
    userEvent.click(screen.getByTestId('validateRequired'));
    errors = screen.getByTestId('errors').textContent!;
    expect(JSON.parse(errors)).toEqual({
      description: I18N_CLIENT_CONFIG.DESCRIPTION_IS_REQUIRED,
      systemId: I18N_CLIENT_CONFIG.SYSTEM_ID_IS_REQUIRED,
      principleId: I18N_CLIENT_CONFIG.PRINCIPLE_ID_IS_REQUIRED,
      agentId: I18N_CLIENT_CONFIG.AGENT_ID_IS_REQUIRED
    });
  });

  it('validate invalid format', () => {
    const onSubmit = jest.fn();
    render(<RenderComponent onSubmit={onSubmit} />);
    let errors = screen.getByTestId('errors').textContent!;
    userEvent.click(screen.getByTestId('validateInvalidFormat'));
    errors = screen.getByTestId('errors').textContent!;
    expect(JSON.parse(errors)).toEqual({
      systemId: I18N_COMMON_TEXT.INVALID_FORMAT,
      principleId: I18N_COMMON_TEXT.INVALID_FORMAT,
      agentId: I18N_COMMON_TEXT.INVALID_FORMAT
    });
  });

  it('selectAllPrinciple', () => {
    const onSubmit = jest.fn();
    render(<RenderComponent onSubmit={onSubmit} />);
    userEvent.click(screen.getByTestId('selectAllPrinciple'));
    expect(setFieldValue).toBeCalled();
  });

  it('selectAllAgentRadio', () => {
    const onSubmit = jest.fn();
    render(<RenderComponent onSubmit={onSubmit} />);
    userEvent.click(screen.getByTestId('selectAllAgentRadio'));
    expect(setFieldValue).toBeCalled();
  });

  it('selectRadioCustom', () => {
    const onSubmit = jest.fn();
    render(<RenderComponent onSubmit={onSubmit} />);
    userEvent.click(screen.getByTestId('selectRadioCustom'));
    expect(setFieldValue).toBeCalled();
  });

  it('changeTextBox with numberic', () => {
    const onSubmit = jest.fn();
    render(<RenderComponent onSubmit={onSubmit} />);
    userEvent.click(screen.getByTestId('changeTextBox'));
    expect(setFieldValue).toBeCalled();
  });
  it('changeTextBox with alphabet', () => {
    const onSubmit = jest.fn();
    render(<RenderComponent onSubmit={onSubmit} />);
    userEvent.click(screen.getByTestId('changeTextBoxWithAlphabet'));
    expect(setFieldValue).not.toBeCalled();
  });

  it('handleChangeDescription', () => {
    const onSubmit = jest.fn();

    render(<RenderComponent onSubmit={() => onSubmit()} />);

    userEvent.click(screen.getByTestId('changeDescription'));
    expect(setFieldValue).toBeCalledTimes(1);
  });

  it('handleChangeDescriptionWithSpecialCharacters', () => {
    const onSubmit = jest.fn();

    render(<RenderComponent onSubmit={() => onSubmit()} />);

    userEvent.click(
      screen.getByTestId('changeDescriptionWithSpecialCharacters')
    );
    expect(setFieldValue).toBeCalledTimes(0);
  });
});
