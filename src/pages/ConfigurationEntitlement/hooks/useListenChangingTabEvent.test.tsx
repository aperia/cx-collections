import React from 'react';
import { renderMockStore } from 'app/test-utils';

import { SECTION_TAB_EVENT } from '../constants';
import { useListenChangingTabEvent } from './useListenChangingTabEvent';

describe('Test useListenChangingTabEvent', () => {
  const mockCallback = jest.fn();

  const changingTab = () => {
    const event = new CustomEvent(SECTION_TAB_EVENT.CHANGING_TAB, {
      bubbles: true,
      cancelable: true
    });
    window.dispatchEvent(event);
  };

  const Component: React.FC = () => {
    useListenChangingTabEvent(mockCallback);
    return null;
  };

  window.clientConfiguration = {};

  it('should run callback when event emmited', () => {
    renderMockStore(<Component />);

    changingTab();

    expect(mockCallback).toHaveBeenCalled();
    expect(window.clientConfiguration?.isHavingTabListener).toBeTruthy();
  });

  it('should not run callback when component unmount', () => {
    const { wrapper } = renderMockStore(<Component />);

    wrapper.unmount();

    changingTab();

    expect(mockCallback).not.toHaveBeenCalled();
    expect(window.clientConfiguration?.isHavingTabListener).toBeFalsy();
  });
});
