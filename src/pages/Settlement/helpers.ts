import { FormatTime } from 'app/constants/enums';
import { addDays, addMonths, formatTimeDefault } from 'app/helpers';
import {
  PromiseToPayRecurringData,
  PromiseToPaySingleData,
  PromiseToPaySingleDetail
} from 'pages/PromiseToPay/types';
import { SettlementPayload, SETTLEMENT_TYPE } from './types';
import { PAYMENT_CARD_TYPE } from 'pages/MakePayment/SchedulePayment/types';

export const mappingPromiseData = (
  promiseToPayList: Array<PromiseToPaySingleDetail>
) => {
  return promiseToPayList.map(
    item =>
      `${item?.method?.value} ${formatTimeDefault(
        item.paymentDate!,
        FormatTime.UTCDate
      )} ${item.amount}`
  );
};

export const prepareUpdateSettlementData = (data: SettlementPayload) => {
  const {
    settlementType,
    acceptedBalanceAmount,
    promiseToPayData,
    settlementTypeSubmit,
    paymentMedium
  } = data;

  let paymentMethod = 'ACH';

  if (paymentMedium === PAYMENT_CARD_TYPE.BANK_INFO) {
    paymentMethod = 'CHECK';
  }

  if (paymentMedium === PAYMENT_CARD_TYPE.CARD_INFO) {
    paymentMethod = 'DEBIT';
  }
  const response: MagicKeyValue = {
    acceptedSettlementBalanceAmount: acceptedBalanceAmount
  };
  const {
    paymentDate = '',
    method,
    amount,
    interval,
    count
  } = promiseToPayData as PromiseToPaySingleData & PromiseToPayRecurringData;

  switch (settlementType) {
    case SETTLEMENT_TYPE.MAKE_PAYMENT: {
      return {
        ...response,
        settlementType: settlementTypeSubmit,
        paymentAmount: +amount,
        paymentMethod,
        paymentDate: formatTimeDefault(paymentDate, FormatTime.UTCDate),
        startDate: formatTimeDefault(paymentDate, FormatTime.UTCDate),
        endDate: formatTimeDefault(paymentDate, FormatTime.UTCDate)
      };
    }
    case SETTLEMENT_TYPE.SINGLE:
      return {
        ...response,
        settlementType: 'SINGLE_PROMISE',
        paymentAmount: amount,
        paymentMethod: method?.value,
        paymentDate: formatTimeDefault(paymentDate, FormatTime.UTCDate),
        startDate: formatTimeDefault(paymentDate, FormatTime.UTCDate),
        endDate: formatTimeDefault(paymentDate, FormatTime.UTCDate)
      };

    case SETTLEMENT_TYPE.RECURRING:
      let endDate = '';
      if (paymentDate) {
        const newCount = parseInt(count?.value || '2', 10);
        const lastPeriod = newCount - 1;
        endDate = (function () {
          switch (interval?.value) {
            case 'WEEKLY':
              return addDays(paymentDate, 7 * lastPeriod);
            case 'BIWEEKLY':
              return addDays(paymentDate, 14 * lastPeriod);
            case 'MONTHLY':
              return addMonths(paymentDate, lastPeriod);
            default:
              return '';
          }
        })();
      }
      return {
        ...response,
        paymentAmount: amount,
        paymentMethod: method?.value || '',
        interval: interval?.value || '',
        count: count?.value || '',
        startDate:
          formatTimeDefault(paymentDate || '', FormatTime.UTCDate) || '',
        endDate: formatTimeDefault(endDate, FormatTime.UTCDate) || '',
        paymentDate:
          formatTimeDefault(paymentDate || '', FormatTime.UTCDate) || ''
      };

    case SETTLEMENT_TYPE.NON_RECURRING:
      const listPromiseToPay =
        promiseToPayData as Array<PromiseToPaySingleDetail>;
      const startDateNonRecurring =
        formatTimeDefault(
          listPromiseToPay[0].paymentDate || '',
          FormatTime.UTCDate
        ) || '';
      const endDateNonRecurring =
        formatTimeDefault(
          listPromiseToPay[listPromiseToPay.length - 1].paymentDate || '',
          FormatTime.UTCDate
        ) || '';
      return {
        ...response,
        methodDateAmount: [...mappingPromiseData(listPromiseToPay)],
        startDate: startDateNonRecurring,
        endDate: endDateNonRecurring,
        paymentDate: startDateNonRecurring
      };

    default:
      return {};
  }
};

export const ceilNumberValue = (obj: MagicKeyValue) => {
  for (const key in obj) {
    obj[key] = Math.ceil(Number(obj[key])).toString();
  }
  return obj;
};
