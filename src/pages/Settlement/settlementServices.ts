import { apiService } from 'app/utils/api.service';
import { UpdateSettlementRequest } from './types';

export const settlementServices = {
  getSettlementInformation(requestData: any) {
    const url =
      window.appConfig?.api?.collectionForm.getSettlementInformation || '';
    return apiService.post(url, requestData);
  },
  getSettlement(requestData: any) {
    const url = window.appConfig?.api?.collectionForm.getSettlement || '';
    return apiService.post(url, requestData);
  },
  updateSettlementSingle(requestData: any) {
    const url =
      window.appConfig?.api?.collectionForm.updateSettlementSingle || '';
    return apiService.post(url, requestData);
  },
  updateSettlementRecurring(requestData: any) {
    const url =
      window.appConfig?.api?.collectionForm.updateSettlementRecurring || '';
    return apiService.post(url, requestData);
  },
  updateSettlementNonRecurring(requestData: any) {
    const url =
      window.appConfig?.api?.collectionForm.updateSettlementNonRecurring || '';
    return apiService.post(url, requestData);
  },
  updateSettlement(requestData: UpdateSettlementRequest) {
    const url = window.appConfig?.api?.collectionForm.updateSettlement || '';
    return apiService.post(url, requestData);
  }
};
