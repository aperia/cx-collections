import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi } from 'app/test-utils/mocks/mockProperty';
import { settlementServices } from './settlementServices';

describe('mockService', () => {
  it('getSettlementInformation', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    settlementServices.getSettlementInformation({});

    expect(mockService).toBeCalled();
  });

  it('getSettlement', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    settlementServices.getSettlement({});

    expect(mockService).toBeCalled();
  });
  it('updateSettlementSingle', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    settlementServices.updateSettlementSingle({});

    expect(mockService).toBeCalled();
  });
  it('updateSettlementRecurring', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    settlementServices.updateSettlementRecurring({});

    expect(mockService).toBeCalled();
  });
  it('updateSettlementNonRecurring', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    settlementServices.updateSettlementNonRecurring({});

    expect(mockService).toBeCalled();
  });
  it('updateSettlement', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    settlementServices.updateSettlement({} as any);

    expect(mockService).toBeCalled();
  });
});
