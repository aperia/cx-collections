import { PAYMENT_CARD_TYPE } from 'pages/MakePayment/SchedulePayment/types';
import {
  mappingPromiseData,
  prepareUpdateSettlementData,
  ceilNumberValue
} from './helpers';

describe('Test helpers', () => {
  it('mappingPromiseData', () => {
    const mockData = [
      {
        amount: '10',
        paymentDate: '2022-10-10',
        method: { value: 'ACH', description: 'ACH' }
      }
    ];
    const result = mappingPromiseData(mockData);
    expect(result).toEqual(['ACH 2022-10-10 10']);
  });
  it('prepareUpdateSettlementData > type other', () => {
    const mockData = {
      settlementType: 'XXX',
      acceptedBalanceAmount: '',
      promiseToPayData: '100'
    };
    const result = prepareUpdateSettlementData(mockData as any);
    expect(result).toEqual({});
  });
  it('prepareUpdateSettlementData > type undefined', () => {
    const mockData = {
      settlementType: undefined,
      acceptedBalanceAmount: '',
      promiseToPayData: '100'
    };
    const result = prepareUpdateSettlementData(mockData as any);
    expect(result).toEqual({});
  });

  it('prepareUpdateSettlementData > type Single > case 1', () => {
    const mockData = {
      settlementType: 'Single',
      acceptedBalanceAmount: '100',
      promiseToPayData: {
        paymentDate: '2020-10-10',
        method: { value: 'ACH' },
        amount: '0'
      }
    };
    const result = prepareUpdateSettlementData(mockData as any);
    expect(result).toEqual({
      acceptedSettlementBalanceAmount: '100',
      endDate: '2020-10-10',
      paymentAmount: '0',
      paymentDate: '2020-10-10',
      paymentMethod: 'ACH',
      startDate: '2020-10-10',
      settlementType: 'SINGLE_PROMISE'
    });
  });

  it('prepareUpdateSettlementData > type MakePayment > case Bank', () => {
    const mockData = {
      settlementType: 'MakePayment',
      settlementTypeSubmit: 'PAY_POINT_PAYMENT',
      acceptedBalanceAmount: '100',
      paymentMedium: PAYMENT_CARD_TYPE.BANK_INFO,
      promiseToPayData: {
        paymentDate: '2020-10-10',
        method: { value: 'Bank' },
        amount: '0'
      }
    };
    const result = prepareUpdateSettlementData(mockData as any);
    expect(result).toEqual({
      acceptedSettlementBalanceAmount: '100',
      endDate: '2020-10-10',
      paymentAmount: 0,
      paymentDate: '2020-10-10',
      paymentMethod: 'CHECK',
      startDate: '2020-10-10',
      settlementType: 'PAY_POINT_PAYMENT'
    });
  });

  it('prepareUpdateSettlementData > type MakePayment > case Card', () => {
    const mockData = {
      settlementType: 'MakePayment',
      settlementTypeSubmit: 'PAY_POINT_PAYMENT',
      acceptedBalanceAmount: '100',
      paymentMedium: PAYMENT_CARD_TYPE.CARD_INFO,
      promiseToPayData: {
        paymentDate: '2020-10-10',
        method: { value: 'Bank' },
        amount: '0'
      }
    };
    const result = prepareUpdateSettlementData(mockData as any);
    expect(result).toEqual({
      acceptedSettlementBalanceAmount: '100',
      endDate: '2020-10-10',
      paymentAmount: 0,
      paymentDate: '2020-10-10',
      paymentMethod: 'DEBIT',
      startDate: '2020-10-10',
      settlementType: 'PAY_POINT_PAYMENT'
    });
  });

  it('prepareUpdateSettlementData > type Recurring > case 1', () => {
    const mockData = {
      settlementType: 'Recurring',
      acceptedBalanceAmount: '100',
      promiseToPayData: {
        paymentDate: '',
        method: 'XXX',
        amount: '0',
        interval: { value: undefined },
        count: '1'
      }
    };
    const result = prepareUpdateSettlementData(mockData as any);
    expect(result).toEqual({
      acceptedSettlementBalanceAmount: '100',
      endDate: '',
      paymentAmount: '0',
      paymentDate: '',
      paymentMethod: '',
      startDate: '',
      count: '',
      interval: ''
    });
  });

  it('prepareUpdateSettlementData > type Recurring > case 2', () => {
    const mockData = {
      settlementType: 'Recurring',
      acceptedBalanceAmount: '100',
      promiseToPayData: {
        paymentDate: '2022-10-10',
        method: 'XXX',
        amount: '0',
        interval: { value: 'WEEKLY' },
        count: '1'
      }
    };
    const result = prepareUpdateSettlementData(mockData as any);
    expect(result).toEqual({
      acceptedSettlementBalanceAmount: '100',
      endDate: '2022-10-17',
      paymentAmount: '0',
      paymentDate: '2022-10-10',
      paymentMethod: '',
      startDate: '2022-10-10',
      count: '',
      interval: 'WEEKLY'
    });
  });

  it('prepareUpdateSettlementData > type Recurring > case 3', () => {
    const mockData = {
      settlementType: 'Recurring',
      acceptedBalanceAmount: '100',
      promiseToPayData: {
        paymentDate: '2022-10-10',
        method: 'XXX',
        amount: '0',
        interval: { value: 'BIWEEKLY' },
        count: '1'
      }
    };
    const result = prepareUpdateSettlementData(mockData as any);
    expect(result).toEqual({
      acceptedSettlementBalanceAmount: '100',
      endDate: '2022-10-24',
      paymentAmount: '0',
      paymentDate: '2022-10-10',
      paymentMethod: '',
      startDate: '2022-10-10',
      count: '',
      interval: 'BIWEEKLY'
    });
  });

  it('prepareUpdateSettlementData > type Recurring > case 4', () => {
    const mockData = {
      settlementType: 'Recurring',
      acceptedBalanceAmount: '100',
      promiseToPayData: {
        paymentDate: '2022-10-10',
        method: 'XXX',
        amount: '0',
        interval: { value: 'MONTHLY' },
        count: '1'
      }
    };
    const result = prepareUpdateSettlementData(mockData as any);
    expect(result).toEqual({
      acceptedSettlementBalanceAmount: '100',
      endDate: '2022-11-10',
      paymentAmount: '0',
      paymentDate: '2022-10-10',
      paymentMethod: '',
      startDate: '2022-10-10',
      count: '',
      interval: 'MONTHLY'
    });
  });

  it('prepareUpdateSettlementData > type Recurring > case 5', () => {
    const mockData = {
      settlementType: 'Recurring',
      acceptedBalanceAmount: '100',
      promiseToPayData: {
        paymentDate: '2022-10-10',
        method: 'XXX',
        amount: '0',
        interval: { value: 'xxx' },
        count: '1'
      }
    };
    const result = prepareUpdateSettlementData(mockData as any);
    expect(result).toEqual({
      acceptedSettlementBalanceAmount: '100',
      endDate: '',
      paymentAmount: '0',
      paymentDate: '2022-10-10',
      paymentMethod: '',
      startDate: '2022-10-10',
      count: '',
      interval: 'xxx'
    });
  });

  it('prepareUpdateSettlementData > type NonRecurring', () => {
    const mockData = {
      settlementType: 'NonRecurring',
      acceptedBalanceAmount: '100',
      promiseToPayData: [
        {
          paymentDate: '2022-10-10',
          amount: '1',
          method: { value: 'ACH', description: 'ACH' }
        },
        {
          paymentDate: '2022-10-20',
          amount: '1',
          method: { value: 'ACH', description: 'ACH' }
        }
      ]
    };
    const result = prepareUpdateSettlementData(mockData as any);
    expect(result).toEqual({
      acceptedSettlementBalanceAmount: '100',
      paymentDate: '2022-10-10',
      startDate: '2022-10-10',
      endDate: '2022-10-20',
      methodDateAmount: ['ACH 2022-10-10 1', 'ACH 2022-10-20 1']
    });
  });

  it('prepareUpdateSettlementData > type NonRecurring > case 2', () => {
    const mockData = {
      settlementType: 'NonRecurring',
      acceptedBalanceAmount: '100',
      promiseToPayData: [
        {
          paymentDate: undefined,
          amount: '1',
          method: { value: undefined, description: undefined }
        },
        {
          paymentDate: undefined,
          amount: '1',
          method: { value: 'ACH', description: 'ACH' }
        }
      ]
    };
    const result = prepareUpdateSettlementData(mockData as any);
    expect(result).toEqual({
      acceptedSettlementBalanceAmount: '100',
      paymentDate: '',
      startDate: '',
      endDate: '',
      methodDateAmount: ['undefined undefined 1', 'ACH undefined 1']
    });
  });
  it('ceilNumberValue ', () => {
    const mockData = {
      100: '398.12'
    };
    const result = ceilNumberValue(mockData as any);
    expect(result).toEqual({
      100: '399'
    });
  });
});
