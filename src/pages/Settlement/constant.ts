export const FORM_FIELD = {
  ACCEPTED_BALANCE_AMOUNT: {
    name: 'acceptedBalanceAmount',
    label: 'txt_accepted_balance_amount'
  },
  ACCEPT_TO_PAY: {
    name: 'acceptedToPay',
    label: 'txt_yes'
  },
  DECLINE_TO_PAY: {
    name: 'declineToPay',
    label: 'txt_no'
  },
  MAKE_PAYMENT: {
    name: 'makePayment',
    label: 'txt_make_payment'
  },
  PROMISE: {
    name: 'Promise',
    label: 'txt_promise'
  }
};
