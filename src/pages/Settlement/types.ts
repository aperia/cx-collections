import { PromiseToPayRequestData } from 'pages/PromiseToPay/types';

export interface SettlementState {
  [storeId: string]: SettlementData;
}

export interface SettlementData {
  callResultType?: string;
  data?: SettlementPayload;
  settlementInfo?: SettlementInfoPayload;
  isLoading?: boolean;
  isError?: boolean;
  isFormValid?: boolean;
  isPaymentSuccess?: boolean;
  confirmationNumber?: string;
  method?: string;
  type?: string;
}

export interface SettlementInfoPayload {
  currentBalanceAmount?: string;
  lowestAllowedSettlementPercentage?: string;
  newFixedSettlementAmount?: string;
  paymentSuggestion?: MagicKeyValue;
}

export enum TRIGGER_TYPE {
  CREATE = 'create',
  EDIT = 'edit'
}

export type AcceptanceToPayType = 'MakePayment' | 'Promise';
export type SettlementType =
  | 'MakePayment'
  | 'NoPayment'
  | 'Single'
  | 'Recurring'
  | 'NonRecurring'
  | 'Host Payment'
  | 'PayPoint Payment';

export enum SETTLEMENT_TYPE {
  MAKE_PAYMENT = 'MakePayment',
  SINGLE = 'Single',
  RECURRING = 'Recurring',
  NON_RECURRING = 'NonRecurring'
}

export type SettlementTypeApi =
  | 'Single Promise'
  | 'Recurring Promise'
  | 'Non-Recurring Promise'
  | 'Host Payment'
  | 'PayPoint Payment';
export interface SettlementPayload {
  settlementStatus?: string;
  settlementType?: SettlementType;
  settlementTypeSubmit?: string;
  promiseToPayData?: PromiseToPayRequestData;
  commentMemo?: string;
  commentType?: string;
  acceptedBalanceAmount?: string;
  method?: string;
  acceptedToPay?: boolean;
  paymentMedium?: string;
}

export interface UpdateSettlementRequest extends CommonAccountIdRequestBody {
  data: SettlementPayload;
}

export interface TriggerUpdateSettlementArg extends StoreIdPayload {
  postData: UpdateSettlementRequest;
  type?: TRIGGER_TYPE;
}

export interface GetSettlementArg {
  storeId: string;
  postData: {
    accountId: string;
  };
}

export enum SETTLEMENT_TYPE_API {
  SINGLE = 'Single Promise',
  RECURRING = 'Recurring Promise',
  NON_RECURRING = 'Non-Recurring Promise',
  HOST_PAYMENT = 'Host Payment',
  PAY_POINT_PAYMENT = 'PayPoint Payment'
}
