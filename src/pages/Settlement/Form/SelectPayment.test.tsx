import { fireEvent, screen, waitFor } from '@testing-library/react';
import { AccountDetailProvider } from 'app/hooks';
// helpers
import { accEValue, renderMockStore, storeId } from 'app/test-utils';
import React from 'react';
import { FORM_FIELD } from '../constant';
// components
import SelectPayment from './SelectPayment';

const initialState: Partial<RootState> = {
  settlement: {
    [storeId]: {
      data: {
        settlementType: 'MakePayment'
      }
    }
  }
};

const renderWrapper = (
  initialState: Partial<RootState>,
  { paymentVia, setPaymentVia }: any
) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <SelectPayment
        paymentVia={paymentVia || 'MakePayment'}
        setPaymentVia={setPaymentVia || jest.fn()}
      />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('render', () => {
  it(`Render UI`, async () => {
    renderWrapper(initialState, {});
  });
});

describe('action', () => {
  it.each`
    labelText                        | props
    ${FORM_FIELD.PROMISE.label}      | ${{ paymentVia: 'MakePayment' }}
    ${FORM_FIELD.MAKE_PAYMENT.label} | ${{ paymentVia: 'Promise' }}
  `(`onChange $labelText`, async ({ labelText, props }) => {
    const setPaymentVia = jest.fn();
    renderWrapper(initialState, { ...props, setPaymentVia });

    fireEvent.click(screen.getByLabelText(labelText));

    await waitFor(() => expect(setPaymentVia).toHaveBeenCalled());
  });
});
