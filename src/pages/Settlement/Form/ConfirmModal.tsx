import React from 'react';

// components
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

// helpers
import { formatCommon } from 'app/helpers';

import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface DataType {
  amount: string;
  date: Date | string;
}
export interface ConfirmModalProps {
  handleCloseModal: () => void;
  handleSubmit: () => void;
  openConfirm: boolean;
  data: DataType;
}

const ConfirmModal: React.FC<ConfirmModalProps> = ({
  handleCloseModal,
  handleSubmit,
  openConfirm,
  data
}) => {
  const { t } = useTranslation();

  const { amount, date } = data;

  const total = Number(amount);

  const dateView = formatCommon(date).time.date;

  const totalView = formatCommon(total).currency();

  const testId = 'settelement_modalConfirm';

  const renderBodyByType = () => {
    return (
      <>
        <p className="pb-16" data-testid={genAmtId(testId, 'information', '')}>
          <span>{t('txt_settlement_promises_to_pay_content')}</span>
          <span className="fw-500">{` ${totalView} `}</span>
          <span>on</span>
          <span className="fw-500"> {dateView}.</span>
        </p>
        <p data-testid={genAmtId(testId, 'confirm', '')}>
          {t('txt_can_you_please_confirm_your_information_to_proceed')}
        </p>
      </>
    );
  };

  return (
    <Modal sm show={openConfirm} dataTestId={testId}>
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId={`${testId}-header`}
      >
        <ModalTitle dataTestId={`${testId}-title`}>
          {t('txt_confirm_promise_to_pay')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>{renderBodyByType()}</ModalBody>
      <ModalFooter
        okButtonText={t('txt_confirm')}
        cancelButtonText={t('txt_cancel')}
        onCancel={handleCloseModal}
        onOk={handleSubmit}
        dataTestId={`${testId}-footer`}
      />
    </Modal>
  );
};

export default ConfirmModal;
