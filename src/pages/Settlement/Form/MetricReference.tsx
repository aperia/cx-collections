import React, { ReactNode } from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useStoreIdSelector } from 'app/hooks';

// helper
import { formatCommon } from 'app/helpers';
import classNames from 'classnames';

// types
import { SettlementInfoPayload } from '../types';

// Const
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';

// selectors
import { selectSettlementInfoData } from '../_redux/selectors';

export interface MetricReferenceProps {}

const MetricReference: React.FC<MetricReferenceProps> = ({}) => {
  const { t } = useTranslation();
  const settlementInfo = useStoreIdSelector<SettlementInfoPayload>(
    selectSettlementInfoData
  );
  const { paymentSuggestion } = settlementInfo || {};

  const renderSuggestItem = (percent: number, value: number) => (
    <div className="d-flex">
      <p className="py-2 px-8 bg-blue-l40 fs-12 color-grey-d20 border br-blue-l40 rounded">
        {percent}%
      </p>
      <p
        className={classNames(
          'fs-14 color-grey-d20',
          percent === 100 ? 'ml-10' : 'ml-16'
        )}
      >
        {formatCommon(value).currency(2)}
      </p>
    </div>
  );

  const renderSuggestPayment = () => {
    const suggestPayment: ReactNode[] = [];

    for (const key in paymentSuggestion) {
      suggestPayment.push(
        <React.Fragment key={key}>
          <div className="col-6">
            {renderSuggestItem(Number(key), paymentSuggestion[key])}
          </div>
        </React.Fragment>
      );
    }
    return (
      <div className="mt-16">
        <p className="color-grey-d20">
          {t(I18N_COLLECTION_FORM.SETTLEMENT_SUGGEST_PAYMENT_AMOUNT)}
        </p>
        <div className="mt-16">{suggestPayment}</div>
      </div>
    );
  };

  return <>{renderSuggestPayment()}</>;
};

export default MetricReference;
