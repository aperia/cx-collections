import { fireEvent, screen, waitFor } from '@testing-library/react';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { AccountDetailProvider } from 'app/hooks';
// helpers
import { accEValue, renderMockStore, storeId, mockActionCreator } from 'app/test-utils';
import React from 'react';
import { FORM_FIELD } from '../constant';
import * as SettlementSelector from '../_redux/selectors';
import * as AcceptToPayFormModule from './AcceptToPayForm';
import * as DeclineToPayFormModule from './DeclineToPayForm';
import { actionsLiveVox } from 'app/livevox-dialer/_redux/reducers';
import Form from '.';

HTMLCanvasElement.prototype.getContext = jest.fn();

const initialState: Partial<RootState> = {
  settlement: {
    [storeId]: {
      isFormValid: true,
      isPaymentSuccess: true,
      isError: false,
      data: {
        settlementType: 'MakePayment',
        acceptedToPay: true
      }
    }
  }
};

const initialStateDialer: Partial<RootState> = {
  liveVoxReducer: {
    sessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
    isAgentAuthenticated: true,
    lineNumber: 'ACD',
    accountNumber: '2345',
    lastInCallAccountNumber: '2345',
    inCallAccountNumber: '2345',
    call: {
      accountNumber: '5166480500018901',
      accountNumberRequired: true,
      callCenterId: 75,
      callRecordingStarted: true,
      callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
      callTransactionId: '126650403531',
      phoneNumber: '1234567890',
      serviceId: 1094568
    }
  },
  settlement: {
    [storeId]: {
      isFormValid: true,
      isPaymentSuccess: true,
      isError: false,
      data: {
        settlementType: 'MakePayment',
        acceptedToPay: true
      }
    }
  },
  collectionForm: { [storeId]: {
    selectedCallResult: {
      code: 'PP',
      description: 'Promise to Pay'
    }
  } }
};

const initialStateDialerWithEmptyAccountNumber: Partial<RootState> = {
  liveVoxReducer: {
    sessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
    isAgentAuthenticated: true,
    lineNumber: 'ACD',
    accountNumber: '',
    lastInCallAccountNumber: '',
    inCallAccountNumber: '',
    call: {
      accountNumber: '',
      accountNumberRequired: true,
      callCenterId: 75,
      callRecordingStarted: true,
      callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
      callTransactionId: '126650403531',
      phoneNumber: '1234567890',
      serviceId: 1094568
    }
  },
  settlement: {
    [storeId]: {
      isFormValid: true,
      isPaymentSuccess: true,
      isError: false,
      data: {
        settlementType: 'MakePayment',
        acceptedToPay: true
      }
    }
  },
  collectionForm: { [storeId]: {
    selectedCallResult: {
      code: 'PP',
      description: 'Promise to Pay'
    }
  } }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  const View = (
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <Form />
    </AccountDetailProvider>
  );
  const result = renderMockStore(View, { initialState });

  return {
    View,
    ...result
  };
};

const setupMockForm = (type: string) => {
  let mockModule = AcceptToPayFormModule;

  if (type === 'DeclineToPayForm') {
    mockModule = DeclineToPayFormModule;
  }

  const submitBtnText = `Submit ${type}`;

  return async ({ selectIsErrorWithPaymentSuccess, submitData, initialState }: any) => {
    jest
      .spyOn(mockModule, 'default')
      .mockImplementation(({ onSubmit }: any) => {
        return (
          <form
            onSubmit={() => {
              onSubmit(submitData);
            }}
          >
            <button type="submit">{submitBtnText}</button>
          </form>
        );
      });

    const result = renderWrapper(initialState);

    if (type === 'DeclineToPayForm') {
      fireEvent.click(screen.getByText(FORM_FIELD.DECLINE_TO_PAY.label));
    }

    jest
      .spyOn(SettlementSelector, 'selectIsErrorWithPaymentSuccess')
      .mockReturnValue(selectIsErrorWithPaymentSuccess);

    await waitFor(() =>
      fireEvent.click(screen.getByText(new RegExp(submitBtnText, 'i')))
    );

    return {
      ...result
    };
  };
};

const mockLiveVoxDialerActions = mockActionCreator(actionsLiveVox);
const setupSubmitAcceptToPayForm = setupMockForm('AcceptToPayForm');
const setupSubmitDeclineToPayForm = setupMockForm('DeclineToPayForm');

describe('render', () => {
  it(`Render UI`, () => {
    renderWrapper(initialState);
  });

  it(`Render UI with isPaymentSuccess === false`, () => {
    renderWrapper({
      ...initialState,
      settlement: {
        [storeId]: {
          ...[storeId],
          isPaymentSuccess: false
        }
      }
    });
  });

  it(`Render DeclineToPayForm after setAcceptedToPay to false`, async () => {
    await setupSubmitDeclineToPayForm({
      selectIsErrorWithPaymentSuccess: true,
      submitData: { settlementType: 'diffMakePayment' },
      initialState
    });
  });
});

describe('action', () => {
  beforeEach(() => {
    jest.resetModules();
    jest.restoreAllMocks();
  });

  it(`handleCancel AcceptToPayForm`, async () => {
    await setupSubmitAcceptToPayForm({
      selectIsErrorWithPaymentSuccess: true,
      submitData: { settlementType: 'diffMakePayment' },
      initialState
    });

    await waitFor(() =>
      screen.getAllByText(I18N_COMMON_TEXT.CANCEL).forEach(item => {
        fireEvent.click(item);
      })
    );
  });

  it(`handleSubmit AcceptToPayForm with settlementType: 'MakePayment'`, async () => {
    await setupSubmitAcceptToPayForm({
      selectIsErrorWithPaymentSuccess: false,
      submitData: { settlementType: 'MakePayment' },
      initialState
    });
  });

  it(`handleSubmit AcceptToPayForm with settlementType: 'diffMakePayment'`, async () => {
    await setupSubmitAcceptToPayForm({
      selectIsErrorWithPaymentSuccess: true,
      submitData: { settlementType: 'diffMakePayment' },
      initialState
    });
  });

  it(`handelSubmitConfirmModal AcceptToPayForm`, async () => {
    await setupSubmitAcceptToPayForm({
      selectIsErrorWithPaymentSuccess: true,
      submitData: { settlementType: 'diffMakePayment' },
      initialState
    });

    await waitFor(() => screen.getByText(/txt_confirm_promise_to_pay/i));
    fireEvent.click(screen.getByText('txt_confirm'));
  });

  it(`handelSubmitConfirmModal AcceptToPayForm > case 2`, async () => {
    await setupSubmitAcceptToPayForm({
      selectIsErrorWithPaymentSuccess: false,
      submitData: { settlementType: 'diffMakePayment' },
      initialState
    });

    await waitFor(() => screen.getByText(/txt_confirm_promise_to_pay/i));
    fireEvent.click(screen.getByText('txt_confirm'));
  });

  it(`handelSubmitConfirmModal AcceptToPayForm for dialer`, async () => {
    const mockLiveVoxDialerTermCodesThunk = mockLiveVoxDialerActions(
      'liveVoxDialerTermCodesThunk'
    );
    await setupSubmitAcceptToPayForm({
      selectIsErrorWithPaymentSuccess: true,
      submitData: { settlementType: 'diffMakePayment' },
      initialState: initialStateDialer
    });

    await waitFor(() => screen.getByText(/txt_confirm_promise_to_pay/i));
    fireEvent.click(screen.getByText('txt_confirm'));

    expect(mockLiveVoxDialerTermCodesThunk).toBeCalledWith(
      {
        liveVoxDialerSessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
        liveVoxDialerLineNumber: 'ACD',
        selectedCallResult: {
          code: 'PP',
          description: 'Promise to Pay'
        },
        serviceId: 1094568,
        callTransactionId: '126650403531',
        callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
        account: '5166480500018901',
        phoneDialed: '1234567890',
        moveAgentToNotReady: false
      }
    );
  });

  it(`should not dispatch DialerTermCodes when no accountNumber`, async () => {
    const mockLiveVoxDialerTermCodesThunk = mockLiveVoxDialerActions(
      'liveVoxDialerTermCodesThunk'
    );
    await setupSubmitAcceptToPayForm({
      selectIsErrorWithPaymentSuccess: true,
      submitData: { settlementType: 'diffMakePayment' },
      initialState: initialStateDialerWithEmptyAccountNumber
    });

    await waitFor(() => screen.getByText(/txt_confirm_promise_to_pay/i));
    fireEvent.click(screen.getByText('txt_confirm'));

    expect(mockLiveVoxDialerTermCodesThunk).not.toBeCalled();
  });

  it(`handelSubmitConfirmModal AcceptToPayForm with empty submitData`, async () => {
    await setupSubmitAcceptToPayForm({ selectIsErrorWithPaymentSuccess: true, initialState });

    await waitFor(() => screen.getByText(/txt_confirm_promise_to_pay/i));
    fireEvent.click(screen.getByText('txt_confirm'));
  });

  it(`handleSubmit DeclineToPayForm`, async () => {
    await setupSubmitDeclineToPayForm({
      selectIsErrorWithPaymentSuccess: false,
      submitData: { settlementType: 'diffMakePayment' },
      initialState
    });
  });

  it(`handleSubmit with settlementType === MakePayment`, async () => {
    await setupSubmitAcceptToPayForm({
      selectIsErrorWithPaymentSuccess: true,
      submitData: { settlementType: 'MakePayment' },
      initialState: {
        settlement: {
          [storeId]: {
            isFormValid: true,
            isPaymentSuccess: undefined,
            isError: true,
            data: {
              settlementType: 'MakePayment',
              acceptedToPay: true
            },
            settlementInfo: {
              currentBalance: '123'
            }
          }
        }
      }
    });
  });
});
