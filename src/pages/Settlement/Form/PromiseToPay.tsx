import React, { useEffect } from 'react';

// components
import { SinglePromiseToPay } from '../../PromiseToPay/PromiseToPayForm/SinglePromiseToPay';
import { RecurringPromiseToPay } from '../../PromiseToPay/PromiseToPayForm/RecurringPromiseToPay';
import { CustomPromiseToPay } from '../../PromiseToPay/PromiseToPayForm/CustomPromiseToPay';
import { promiseToPayActions } from '../../PromiseToPay/_redux/reducers';
import { SelectType } from '../../PromiseToPay/SelectType';

// redux
import { useDispatch } from 'react-redux';

// hooks
import { useAccountDetail } from 'app/hooks';

// constants
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';

// Actions
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';

// types
import {
  PromiseToPayRequestData,
  PromiseToPayType
} from '../../PromiseToPay/types';

export interface SettlementPromiseToPayProps {
  acceptedBalanceAmount?: string;
  promiseToPayType: PromiseToPayType;
  onSubmit: (
    values: PromiseToPayRequestData,
    settlementType: PromiseToPayType
  ) => void;
  setPromiseToPayType: (value: PromiseToPayType) => void;
}

export enum MultiplePromiseToPayAction {
  ADD = 'ADD',
  REMOVE = 'REMOVE'
}

const SettlementPromiseToPay: React.FC<SettlementPromiseToPayProps> = ({
  acceptedBalanceAmount,
  promiseToPayType,
  onSubmit,
  setPromiseToPayType
}) => {
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();

  const isSingleActive = promiseToPayType === PromiseToPayType.SINGLE;
  const isRecurringActive = promiseToPayType === PromiseToPayType.RECURRING;
  const isMultipleActive = promiseToPayType === PromiseToPayType.MULTIPLE;

  const handleChangeType = (type: PromiseToPayType) => {
    typeof setPromiseToPayType === 'function' && setPromiseToPayType(type);
  };

  const handleSubmit = (promiseValues: PromiseToPayRequestData) => {
    onSubmit(promiseValues, promiseToPayType);
  };

  const typePromiseToPay = (
    <SelectType
      uniqueId={storeId}
      type={promiseToPayType}
      setType={handleChangeType}
    />
  );

  useEffect(() => {
    typeof setPromiseToPayType === 'function' &&
      setPromiseToPayType(PromiseToPayType.SINGLE);
  }, [setPromiseToPayType]);

  useEffect(() => {
    // get promise to pay data
    dispatch(
      collectionFormActions.getDetailCollectionForm({
        storeId,
        postData: {
          callResultType: CALL_RESULT_CODE.SETTLEMENT
        }
      })
    );
    // get config
    dispatch(promiseToPayActions.getPromiseToPayConfig({ storeId }));
  }, [dispatch, storeId]);

  useEffect(() => {
    dispatch(promiseToPayActions.getPromiseToPayMethodRefData({ storeId }));
  }, [dispatch, storeId]);

  const formId = `${storeId}-settlement`;

  return (
    <div>
      {typePromiseToPay}
      <div>
        {isSingleActive && (
          <SinglePromiseToPay
            formId={formId}
            acceptedBalanceAmount={acceptedBalanceAmount}
            onSubmit={handleSubmit}
          />
        )}
        {isRecurringActive && (
          <RecurringPromiseToPay
            formId={formId}
            acceptedBalanceAmount={acceptedBalanceAmount}
            onSubmit={handleSubmit}
          />
        )}
        {isMultipleActive && (
          <CustomPromiseToPay
            formId={formId}
            acceptedBalanceAmount={acceptedBalanceAmount}
            onSubmit={handleSubmit}
          />
        )}
      </div>
    </div>
  );
};

export default SettlementPromiseToPay;
