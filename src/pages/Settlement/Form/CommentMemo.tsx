import React, { useEffect, useState } from 'react';

// components
import { DropdownList, TextArea } from 'app/_libraries/_dls/components';
import { FormikProps } from 'formik';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// Redux
import { useSelector } from 'react-redux';

// helper
import classNames from 'classnames';

// Const
import { I18N_COLLECTION_FORM, I18N_COMMON_TEXT } from 'app/constants/i18n';

// selector
import { selectTypeRefData } from 'pages/Memos/CIS/_redux/selectors';

// types
import { AppState } from 'storeConfig';
import { MemoType } from 'pages/Memos/types';
import { SettlementPayload } from '../types';
import { PromiseToPayType } from 'pages/PromiseToPay/types';

export interface CommentMemoProps {
  required?: boolean;
  promiseToPayType: PromiseToPayType;
  formik: FormikProps<SettlementPayload>;
}

const CommentMemo: React.FC<CommentMemoProps> = ({
  required,
  promiseToPayType,
  formik
}) => {
  const { t } = useTranslation();

  const { storeId } = useAccountDetail();

  const maxLength = 1000;

  const [restCharacter, setRestCharacter] = useState<number>(maxLength);

  const memoType = useSelector<AppState, MemoType | undefined>(
    appState => appState.memo[storeId]?.configMemo?.data?.type
  );

  const typeRefData =
    useStoreIdSelector<Record<string, string>[]>(selectTypeRefData);

  const { values, setFieldValue, handleChange, handleBlur, errors, touched } =
    formik;

  useEffect(() => {
    setFieldValue('commentType', '');
    setFieldValue('commentMemo', '');
    setRestCharacter(0);
    setRestCharacter(maxLength);
  }, [setFieldValue, promiseToPayType]);

  const formatCharacterLeft = (restChar: number) => {
    if (restChar < 0)
      return t('txt_character_over_limit', { count: Math.abs(restChar) });
    return t('txt_character_left', { count: restChar });
  };

  return (
    <>
      <h6 className="color-grey mt-24">
        {t(I18N_COLLECTION_FORM.SETTLEMENT_COMMENT)}
        {required && <span className="color-red ml-4">*</span>}
      </h6>
      <p className="color-grey-d20 mt-16">
        {t(
          memoType === MemoType.CIS
            ? I18N_COLLECTION_FORM.SETTLEMENT_COMMENT_DESCRIPTION_REQUIRED
            : I18N_COLLECTION_FORM.SETTLEMENT_COMMENT_DESCRIPTION
        )}
      </p>
      {memoType === MemoType.CIS && (
        <div className="mt-16">
          <DropdownList
            required={required}
            name="commentType"
            textField="description"
            label={t(I18N_COMMON_TEXT.TYPE)}
            onBlur={handleBlur}
            value={values.commentType}
            onChange={handleChange}
            popupBaseProps={{
              popupBaseClassName: 'inside-infobar'
            }}
            error={{
              status:
                required && Boolean(errors.commentType && touched.commentType),
              message: t(errors.commentType) as string
            }}
            dataTestId="CIS-memo-header_search_description"
            noResult={t('txt_no_results_found')}
          >
            {typeRefData?.map(item => (
              <DropdownList.Item
                key={item.value}
                label={item.description}
                value={item}
              />
            ))}
          </DropdownList>
        </div>
      )}

      <div className="mt-16 pb-24">
        <TextArea
          name="commentMemo"
          placeholder={t(
            required
              ? I18N_COLLECTION_FORM.SETTLEMENT_COMMENT_PLACEHOLDER
              : I18N_COLLECTION_FORM.SETTLEMENT_COMMENT_OPTIONAL_PLACEHOLDER
          )}
          className={classNames('resize-none h-100', {
            'dls-error': Boolean(errors.commentMemo && touched.commentMemo)
          })}
          required={required}
          maxLength={maxLength}
          value={values.commentMemo}
          onChange={e => {
            setRestCharacter(maxLength - e.target.value.length);
            handleChange(e);
          }}
          onBlur={handleBlur}
          error={{
            status: Boolean(
              required && errors.commentMemo && touched.commentMemo
            ),
            message: t(errors.commentMemo) as string
          }}
        />
        <p
          className={classNames(
            'color-grey-l24 fs-12 mt-8',
            restCharacter < 0 && 'color-danger'
          )}
        >
          {formatCharacterLeft(restCharacter)}
        </p>
      </div>
    </>
  );
};

export default CommentMemo;
