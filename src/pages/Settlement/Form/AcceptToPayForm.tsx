import React, { useEffect, useMemo, useState } from 'react';

// components
import {
  Icon,
  InlineMessage,
  NumericTextBox
} from 'app/_libraries/_dls/components';
import SelectPayment from './SelectPayment';
import SettlementPromiseToPay from 'pages/Settlement/Form/PromiseToPay';
import CommentMemo from './CommentMemo';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useFormik } from 'formik';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// Redux
import { useDispatch, useSelector } from 'react-redux';

// helper
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import classNames from 'classnames';

// Const

import { FORM_FIELD } from '../constant';

// Selectors
import { selectPromiseToPayValidForm } from 'pages/PromiseToPay/_redux/selectors';
import { settlementActions } from '../_redux/reducers';
import {
  selectConfirmationNumber,
  selectIsErrorWithMethod,
  selectIsErrorWithPaymentSuccess,
  selectSettlementInfoData
} from '../_redux/selectors';

// types
import {
  PromiseToPayRequestData,
  PromiseToPayType
} from 'pages/PromiseToPay/types';
import {
  AcceptanceToPayType,
  SettlementInfoPayload,
  SettlementPayload
} from '../types';

import { MemoType } from 'pages/Memos/types';
import { AppState } from 'storeConfig';
import { PAYMENT_TYPE } from 'pages/MakePayment/SchedulePayment/types';
import { formatCommon } from 'app/helpers';

export interface AcceptToPayFormProps {
  formId: string;
  paymentVia: AcceptanceToPayType;
  setPaymentVia: (value: AcceptanceToPayType) => void;
  onSubmit: (values: SettlementPayload) => void;
}

const AcceptToPayForm: React.FC<AcceptToPayFormProps> = ({
  formId,
  paymentVia,
  setPaymentVia,
  onSubmit
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();

  const memoType = useSelector<AppState, MemoType | undefined>(
    appState => appState.memo[storeId]?.configMemo?.data?.type
  );

  const [promiseToPayType, setPromiseToPayType] = useState<PromiseToPayType>(
    PromiseToPayType.SINGLE
  );

  const settlementInfo = useStoreIdSelector<SettlementInfoPayload>(
    selectSettlementInfoData
  );
  const { newFixedSettlementAmount, currentBalanceAmount: currentBalance } =
    settlementInfo || {};
  const isPromiseFormValid = useStoreIdSelector<boolean>(
    selectPromiseToPayValidForm
  );

  const submitErrorWithPaymentSuccess = useStoreIdSelector<boolean | undefined>(
    selectIsErrorWithPaymentSuccess
  );

  const submitErrorWithMethod = useStoreIdSelector<string | undefined>(
    selectIsErrorWithMethod
  );

  const confirmationNumber = useStoreIdSelector<string>(
    selectConfirmationNumber
  );

  const formik = useFormik<SettlementPayload>({
    initialValues: {
      settlementType: 'MakePayment',
      acceptedBalanceAmount: '',
      commentMemo: '',
      commentType: ''
    },
    onSubmit: formValues => {
      onSubmit({
        settlementType:
          paymentVia === 'MakePayment'
            ? 'MakePayment'
            : formValues?.settlementType,
        promiseToPayData: formValues?.promiseToPayData,
        acceptedBalanceAmount: formValues?.acceptedBalanceAmount,
        commentMemo: formValues?.commentMemo,
        commentType: formValues?.commentType
      });
    },
    validateOnBlur: true,
    validateOnChange: false,
    validate: ({ acceptedBalanceAmount, commentMemo, commentType }) => {
      const formErrors: Record<string, string> | undefined = {};
      if (!acceptedBalanceAmount) {
        formErrors['acceptedBalanceAmount'] =
          I18N_COLLECTION_FORM.SETTLEMENT_ACCEPTED_BALANCE_AMOUNT_REQUIRED;
      }
      if (
        acceptedBalanceAmount &&
        (Number(acceptedBalanceAmount) < Number(newFixedSettlementAmount) ||
          Number(acceptedBalanceAmount) > Number(currentBalance))
      ) {
        formErrors['acceptedBalanceAmount'] =
          I18N_COLLECTION_FORM.SETTLEMENT_ACCEPTED_BALANCE_AMOUNT_ERROR;
      }

      if (!!commentMemo?.trim() && memoType === MemoType.CIS) {
        if (memoType === MemoType.CIS && !commentType) {
          formErrors['commentType'] =
            I18N_COLLECTION_FORM.SETTLEMENT_TYPE_MEMO_REQUIRED;
        }
      }

      return formErrors;
    }
  });
  const {
    values,
    handleChange,
    handleBlur,
    isValid,
    errors,
    touched,
    setFieldValue,
    submitForm,
    validateForm
  } = formik;

  const handleSubmit = (
    promiseValues: PromiseToPayRequestData,
    settlementType: PromiseToPayType
  ) => {
    setFieldValue('promiseToPayData', promiseValues);
    setFieldValue('settlementType', settlementType);
    submitForm();
  };

  useEffect(() => {
    typeof setPaymentVia === 'function' && setPaymentVia('MakePayment');
  }, [setPaymentVia]);

  const isFormValid = useMemo(
    () => isValid && (isPromiseFormValid || paymentVia === 'MakePayment'),

    [isValid, isPromiseFormValid, paymentVia]
  );
  useEffect(() => {
    validateForm();
  }, [validateForm]);

  useEffect(() => {
    dispatch(
      settlementActions.setValidForm({
        storeId,
        isValid: isFormValid
      })
    );
  }, [dispatch, storeId, isFormValid]);

  const renderPromiseToPay = () => {
    return (
      <div className="mx-n24 bg-light-l20 px-24 pt-24 border-top">
        <h6 className="color-grey">
          {t(I18N_COLLECTION_FORM.SETTLEMENT_PROMISE_TO_PAY)}
        </h6>
        <SettlementPromiseToPay
          acceptedBalanceAmount={values?.acceptedBalanceAmount || ''}
          promiseToPayType={promiseToPayType}
          onSubmit={handleSubmit}
          setPromiseToPayType={setPromiseToPayType}
        />
        <CommentMemo formik={formik} promiseToPayType={promiseToPayType} />
      </div>
    );
  };

  return (
    <>
      <form
        id={paymentVia === 'Promise' ? undefined : formId}
        onSubmit={e => {
          e.preventDefault();
          submitForm();
        }}
      >
        <NumericTextBox
          required={!submitErrorWithPaymentSuccess}
          className={classNames({
            'dls-error': Boolean(
              errors.acceptedBalanceAmount && touched.acceptedBalanceAmount
            )
          })}
          name={FORM_FIELD.ACCEPTED_BALANCE_AMOUNT.name}
          format="c0"
          label={t(FORM_FIELD.ACCEPTED_BALANCE_AMOUNT.label)}
          value={values?.acceptedBalanceAmount}
          onChange={handleChange}
          onBlur={handleBlur}
          readOnly={submitErrorWithPaymentSuccess}
          error={{
            status: Boolean(
              errors.acceptedBalanceAmount && touched.acceptedBalanceAmount
            ),
            message: t(errors.acceptedBalanceAmount, {
              newFixedSettlementAmount: formatCommon(
                newFixedSettlementAmount || ''
              ).currency(2),
              currentBalance: formatCommon(currentBalance || '').currency(2)
            }) as string
          }}
        />
        <p className="color-grey-l24 fs-12 mt-8 pre-wrap">
          {t(
            I18N_COLLECTION_FORM.SETTLEMENT_ACCEPTED_BALANCE_AMOUNT_DESCRIPTION
          )}
        </p>
        <SelectPayment paymentVia={paymentVia} setPaymentVia={setPaymentVia} />
        {paymentVia === 'MakePayment' && (
          <div className="mx-n24 bg-light-l20 p-24 border-top br-light-l12">
            <div className="d-flex justify-content-center">
              <Icon name="run-transaction" className="color-light-l04 fs-60" />
            </div>
            {submitErrorWithPaymentSuccess ? (
              <InlineMessage variant="warning" className="mt-16 mb-0">
                {submitErrorWithMethod === PAYMENT_TYPE.SCHEDULE
                  ? t('txt_payment_has_schedule_with_confirmation_number', {
                      confirmationNumber: confirmationNumber
                    })
                  : t('txt_payment_has_schedule')}
              </InlineMessage>
            ) : (
              <p className="color-grey-d20 text-center mt-16 pre-wrap">
                {t(I18N_COLLECTION_FORM.SETTLEMENT_PAYMENT_NOTE)}
              </p>
            )}
          </div>
        )}
        {paymentVia === 'Promise' && renderPromiseToPay()}
      </form>
    </>
  );
};

export default AcceptToPayForm;
