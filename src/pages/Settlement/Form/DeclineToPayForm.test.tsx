import React from 'react';
import { renderMockStoreId, storeId } from 'app/test-utils';
import DeclineToPayForm from './DeclineToPayForm';
import { MemoType } from 'pages/Memos/types';
import userEvent from '@testing-library/user-event';
import { fireEvent } from '@testing-library/react';

HTMLCanvasElement.prototype.getContext = jest.fn();

const initialState: Partial<RootState> = {
  memo: {
    [storeId]: {
      configMemo: {
        data: {
          type: MemoType.CIS
        }
      }
    }
  },
  cisMemo: {
    [storeId]: {
      memoCISRefData: {
        typeRefData: [
          {
            value: '1',
            description: 'des'
          }
        ]
      }
    }
  }
};

describe('DeclineToPayForm', () => {
  it('Render UI', async () => {
    const { wrapper } = renderMockStoreId(
      <DeclineToPayForm formId="123" onSubmit={jest.fn} />,
      {
        initialState
      }
    );

    expect(wrapper.getByText('txt_comment')).toBeInTheDocument();

    const form = wrapper.container.querySelector('form')!;
    const textBox = wrapper.container.querySelector('textarea')!;
    const icon = wrapper.baseElement.querySelector('.icon-chevron-down')!;

    // OnClick Menu
    userEvent.click(icon);

    // Select Item
    const item = wrapper.getByText('des')!;

    jest.useFakeTimers();
    userEvent.click(item);
    jest.runAllTimers();

    // OnChange
    userEvent.type(textBox, '12345');

    fireEvent.submit(form);
  });
});
