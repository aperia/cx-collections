import React from 'react';
// components
import { Radio } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useStoreIdSelector } from 'app/hooks';

// Const
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { FORM_FIELD } from '../constant';

// selector
import { selectIsErrorWithPaymentSuccess } from '../_redux/selectors';

export interface SelectAcceptanceToPayProps {
  storeId: string;
  acceptedToPay: boolean;
  setAcceptedToPay: (value: boolean) => void;
}

const SelectAcceptanceToPay: React.FC<SelectAcceptanceToPayProps> = ({
  storeId,
  acceptedToPay,
  setAcceptedToPay
}) => {
  const { t } = useTranslation();
  const submitErrorWithPaymentSuccess = useStoreIdSelector<boolean | undefined>(
    selectIsErrorWithPaymentSuccess
  );

  return (
    <div className="mx-n24 mt-16 mb-24 bg-light-l20 border-top br-light-l12 border-bottom">
      <div className="p-24">
        <p className="color-grey mb-16 fw-500">
          {t(I18N_COLLECTION_FORM.SETTLEMENT_CARDHOLDER_ACCEPTANCE_TO_PAY)}
        </p>
        <div className="row">
          <div className="col-6">
            <div
              className="card card-radio cursor-pointer br-radius-8 p-16"
              onClick={() => setAcceptedToPay(true)}
            >
              <div className="d-flex justify-content-between">
                <span>{t(FORM_FIELD.ACCEPT_TO_PAY.label)}</span>
                <Radio>
                  <Radio.Input
                    checked={acceptedToPay}
                    name={`${FORM_FIELD.ACCEPT_TO_PAY.name}-${storeId}`}
                    className="checked-style style-radio"
                  />
                </Radio>
              </div>
            </div>
          </div>
          <div className="col-6">
            <div
              className="card card-radio cursor-pointer br-radius-8 p-16"
              onClick={() =>
                !submitErrorWithPaymentSuccess && setAcceptedToPay(false)
              }
            >
              <div className="d-flex justify-content-between">
                <span>{t(FORM_FIELD.DECLINE_TO_PAY.label)}</span>
                <Radio>
                  <Radio.Input
                    checked={acceptedToPay === false}
                    name={`${FORM_FIELD.DECLINE_TO_PAY.name}-${storeId}`}
                    disabled={submitErrorWithPaymentSuccess}
                    className="checked-style style-radio"
                  />
                </Radio>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SelectAcceptanceToPay;
