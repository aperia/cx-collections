import { fireEvent, screen, waitFor } from '@testing-library/react';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { AccountDetailProvider } from 'app/hooks';
// helpers
import { accEValue, renderMockStore, storeId } from 'app/test-utils';
import { PromiseToPayType } from 'pages/PromiseToPay/types';
import * as React from 'react';
import SettlementPromiseToPay from './PromiseToPay';

jest.mock('pages/PromiseToPay/PromiseToPayForm/SinglePromiseToPay', () => ({
  __esModule: true,
  ...jest.requireActual(
    'pages/PromiseToPay/PromiseToPayForm/SinglePromiseToPay'
  ),
  SinglePromiseToPay: ({ onSubmit }: any) => {
    return (
      <form onSubmit={onSubmit}>
        <input id="test" type="text" value="testValue" />
        <button type="submit">Submit</button>
      </form>
    );
  }
}));

const initialState: Partial<RootState> = {
  settlement: {
    [storeId]: {
      data: {
        settlementType: 'MakePayment'
      }
    }
  }
};

const renderWrapper = (
  initialState: Partial<RootState>,
  { promiseToPayType }: any
) => {
  const handleSubmit = jest.fn();
  const setPromiseToPayType = jest.fn();
  const result = renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <SettlementPromiseToPay
        acceptedBalanceAmount={'100'}
        promiseToPayType={promiseToPayType || PromiseToPayType.SINGLE}
        onSubmit={handleSubmit}
        setPromiseToPayType={setPromiseToPayType}
      />
    </AccountDetailProvider>,
    { initialState }
  );

  return {
    ...result,
    handleSubmit,
    setPromiseToPayType
  };
};

describe('render', () => {
  it.each`
    promiseToPayType              | props
    ${PromiseToPayType.SINGLE}    | ${{ promiseToPayType: PromiseToPayType.SINGLE }}
    ${PromiseToPayType.RECURRING} | ${{ promiseToPayType: PromiseToPayType.RECURRING }}
    ${PromiseToPayType.MULTIPLE}  | ${{ promiseToPayType: PromiseToPayType.MULTIPLE }}
  `(`Render UI when promiseToPayType === $promiseToPayType`, ({ props }) => {
    renderWrapper(initialState, { ...props });
  });
});

describe('action', () => {
  beforeEach(() => {
    jest.resetModules();
  });

  it(`handleChangeType`, async () => {
    const { setPromiseToPayType } = renderWrapper(initialState, {
      promiseToPayType: PromiseToPayType.SINGLE
    });

    fireEvent.click(screen.getByLabelText(I18N_COLLECTION_FORM.RECURRING));

    await waitFor(() =>
      expect(setPromiseToPayType).toHaveBeenCalledWith(
        PromiseToPayType.RECURRING
      )
    );
  });

  it(`handleSubmit`, async () => {
    const { handleSubmit } = renderWrapper(initialState, {
      promiseToPayType: PromiseToPayType.SINGLE
    });

    fireEvent.click(screen.getByText(/Submit/i));

    await waitFor(() => expect(handleSubmit).toHaveBeenCalled());
  });
});
