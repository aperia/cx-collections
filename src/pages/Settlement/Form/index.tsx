import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
  useState
} from 'react';

// components
import { Button, Icon } from 'app/_libraries/_dls/components';
import { View } from 'app/_libraries/_dof/core';
import SelectAcceptanceToPay from './SelectAcceptanceToPay';
import AcceptToPayForm from './AcceptToPayForm';
import DeclineToPayForm from './DeclineToPayForm';
import MetricReference from './MetricReference';
import ConfirmModal from './ConfirmModal';
import SimpleBarWithApiError from 'pages/CollectionForm/SimpleBarWithApiError';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helper
import classNames from 'classnames';

// Const
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';
import { I18N_COLLECTION_FORM, I18N_COMMON_TEXT } from 'app/constants/i18n';

// types
import {
  AcceptanceToPayType,
  SettlementInfoPayload,
  SettlementPayload,
  TRIGGER_TYPE
} from '../types';

// selectors
import {
  selectIsErrorWithPaymentSuccess,
  selectLoading,
  selectMethod,
  selectSettlementInfoData,
  selectSettlementValidForm
} from '../_redux/selectors';

// actions
import { memoActions } from 'pages/Memos/_redux/reducers';
import { cisMemoActions } from 'pages/Memos/CIS/_redux/reducers';
import { schedulePaymentActions } from 'pages/MakePayment/SchedulePayment/_redux/reducers';
import { settlementActions } from '../_redux/reducers';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { LiveVoxCallResultArgs } from 'app/livevox-dialer/types';
import { CallResult } from 'pages/CollectionForm/types';
import { takeSelectedCallResult } from 'pages/CollectionForm/_redux/selectors';
import * as liveVoxSelectors from 'app/livevox-dialer/_redux/selectors';
import { actionsLiveVox } from 'app/livevox-dialer/_redux/reducers';
import isEmpty from 'lodash.isempty';
import { MAKE_PAYMENT_FROM } from 'pages/MakePayment/SchedulePayment/types';

export interface SettlementProps {}

const Settlement: React.FC<SettlementProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const divRef = useRef<HTMLDivElement | null>(null);

  const [paymentVia, setPaymentVia] =
    useState<AcceptanceToPayType>('MakePayment');

  const [acceptedToPay, setAcceptedToPay] = useState(true);

  const [openConfirmModal, setOpenConfirmModal] = useState(false);

  const [settlementData, setSettlementData] =
    useState<SettlementPayload | undefined>(undefined);

  const { storeId, accEValue } = useAccountDetail();

  const loading = useStoreIdSelector<boolean>(selectLoading);

  const isFormValid = useStoreIdSelector<boolean>(selectSettlementValidForm);

  const methodMakePayment =
    useStoreIdSelector<string | undefined>(selectMethod);

  const submitErrorWithPaymentSuccess = useStoreIdSelector<boolean | undefined>(
    selectIsErrorWithPaymentSuccess
  );

  const settlementInfo = useStoreIdSelector<SettlementInfoPayload>(
    selectSettlementInfoData
  );

  // LiveVox
  const liveVoxDialerSessionId = useSelector(
    liveVoxSelectors.getLiveVoxDialerSessionId
  );
  const liveVoxDialerLineNumber = useSelector(
    liveVoxSelectors.getLiveVoxDialerLineNumber
  );
  const liveVoxDialerLineActiveAndReadyData = useSelector(
    liveVoxSelectors.getLiveVoxDialerLineActiveAndReadyData
  );
  const callResultSelected = useStoreIdSelector<CallResult>(
    takeSelectedCallResult
  );

  const isEdit =
    acceptedToPay &&
    paymentVia === 'MakePayment' &&
    !submitErrorWithPaymentSuccess;

  const formId = `${storeId}-settlement`;

  const submitCallResult = useCallback(
    (values: SettlementPayload) => {
      batch(() => {
        dispatch(
          settlementActions.triggerUpdateSettlement({
            storeId,
            postData: {
              common: { accountId: accEValue },
              data: {
                ...values,
                method: methodMakePayment,
                acceptedToPay: acceptedToPay
              }
            },
            type: isEdit ? TRIGGER_TYPE.EDIT : TRIGGER_TYPE.CREATE
          })
        );
      });
    },
    [dispatch, storeId, accEValue, methodMakePayment, acceptedToPay, isEdit]
  );

  useEffect(() => {
    document.body.classList.add('hide-float-btn');
    return () => {
      document.body.classList.remove('hide-float-btn');
    };
  }, []);

  useEffect(() => {
    if (!settlementInfo) {
      batch(() => {
        dispatch(
          cisMemoActions.getMemoCISRefData({
            storeId,
            postData: { accountId: accEValue }
          })
        );
        dispatch(
          memoActions.getMemoConfig({
            storeId,
            accountId: accEValue
          })
        );
        dispatch(
          settlementActions.getSettlementInformation({
            storeId,
            postData: { accountId: accEValue }
          })
        );
      });
    }
  }, [accEValue, dispatch, settlementInfo, storeId]);

  const handleCancel = () => {
    batch(() => {
      dispatch(collectionFormActions.setCurrentView({ storeId }));
      dispatch(
        collectionFormActions.selectCallResult({ storeId, item: undefined })
      );
      dispatch(
        collectionFormActions.changeMethod({
          storeId,
          method: COLLECTION_METHOD.CREATE
        })
      );
    });
  };

  const handleCloseConfirmModal = useCallback(() => {
    setOpenConfirmModal(false);
  }, []);

  const dispatchingTermCodesToDialer = useCallback(() => {
    if (
      liveVoxDialerLineActiveAndReadyData &&
      !isEmpty(liveVoxDialerLineActiveAndReadyData.accountNumber)
    ) {
      const liveVoxTermCodesArgs: LiveVoxCallResultArgs = {
        liveVoxDialerSessionId: liveVoxDialerSessionId,
        liveVoxDialerLineNumber: liveVoxDialerLineNumber,
        selectedCallResult: callResultSelected,
        serviceId: liveVoxDialerLineActiveAndReadyData.serviceId,
        callTransactionId:
          liveVoxDialerLineActiveAndReadyData.callTransactionId,
        callSessionId: liveVoxDialerLineActiveAndReadyData.callSessionId,
        account: liveVoxDialerLineActiveAndReadyData.accountNumber,
        phoneDialed: liveVoxDialerLineActiveAndReadyData.phoneNumber,
        moveAgentToNotReady: false
      };
      const thunkResult =
        actionsLiveVox.liveVoxDialerTermCodesThunk(liveVoxTermCodesArgs);
      dispatch(thunkResult);
    }
  }, [
    callResultSelected,
    dispatch,
    liveVoxDialerLineActiveAndReadyData,
    liveVoxDialerLineNumber,
    liveVoxDialerSessionId
  ]);

  const handelSubmitConfirmModal = useCallback(() => {
    if (!settlementData) return;
    submitCallResult(settlementData);
    dispatchingTermCodesToDialer();
    handleCloseConfirmModal();
  }, [
    dispatchingTermCodesToDialer,
    handleCloseConfirmModal,
    settlementData,
    submitCallResult
  ]);

  const handleSubmit = (values: SettlementPayload) => {
    dispatchingTermCodesToDialer();

    if (
      values?.settlementType === 'MakePayment' &&
      !submitErrorWithPaymentSuccess &&
      acceptedToPay
    ) {
      setSettlementData(values);
      // handle make payment
      batch(() => {
        dispatch(
          schedulePaymentActions.toggleModal({
            storeId,
            from: MAKE_PAYMENT_FROM.SETTLEMENT
          })
        );
        dispatch(
          schedulePaymentActions.setAcceptedBalanceAmount({
            storeId,
            acceptedBalanceAmount: values?.acceptedBalanceAmount
          })
        );
      });

      return;
    }
    if (values?.settlementType !== 'MakePayment' && acceptedToPay) {
      setSettlementData(values);
      setOpenConfirmModal(true);
      return;
    }
    submitCallResult(values);
  };

  useLayoutEffect(() => {
    divRef.current &&
      (divRef.current.style.height = `calc(100vh - ${loading ? 189 : 258}px)`);
  }, [loading]);

  return (
    <div className={classNames({ loading })}>
      <div ref={divRef}>
        <SimpleBarWithApiError className="ml-24 mt-24">
          <div className="pt-24 px-24">
            <h5 className="mr-auto">{t(I18N_COLLECTION_FORM.CALL_RESULT)}</h5>
            <div className="mt-16 p-8 bg-light-l16 d-flex align-items-center rounded-lg">
              <div className="bubble-icon">
                <Icon name="megaphone" />
              </div>

              <p className="ml-12">{t(I18N_COLLECTION_FORM.SETTLEMENT)}</p>
            </div>
            <h6 className="color-grey mt-16">
              {t(I18N_COLLECTION_FORM.SETTLEMENT_INFORMATION)}
            </h6>
            <View
              descriptor="settlementInformation"
              formKey={`${storeId}__settlementInformation`}
              value={settlementInfo}
            />
            <MetricReference />
            <SelectAcceptanceToPay
              storeId={storeId}
              acceptedToPay={acceptedToPay}
              setAcceptedToPay={setAcceptedToPay}
            />
            {acceptedToPay ? (
              <AcceptToPayForm
                formId={formId}
                paymentVia={paymentVia}
                setPaymentVia={setPaymentVia}
                onSubmit={handleSubmit}
              />
            ) : (
              <DeclineToPayForm formId={formId} onSubmit={handleSubmit} />
            )}
          </div>
        </SimpleBarWithApiError>
      </div>
      <div className="group-button-footer d-flex justify-content-end">
        <Button variant="secondary" onClick={handleCancel}>
          {t(I18N_COMMON_TEXT.CANCEL)}
        </Button>
        <Button disabled={!isFormValid} form={formId} type="submit">
          {isEdit ? t(I18N_COMMON_TEXT.CONTINUE) : t(I18N_COMMON_TEXT.SUBMIT)}
        </Button>
      </div>
      {openConfirmModal && (
        <ConfirmModal
          handleCloseModal={handleCloseConfirmModal}
          handleSubmit={handelSubmitConfirmModal}
          openConfirm={openConfirmModal}
          data={{
            amount: settlementData?.acceptedBalanceAmount || '0',
            date: new Date()
          }}
        />
      )}
    </div>
  );
};

export default Settlement;
