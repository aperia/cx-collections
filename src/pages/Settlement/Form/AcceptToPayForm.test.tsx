import { fireEvent, screen, waitFor } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { AccountDetailProvider } from 'app/hooks';
// helpers
import {
  accEValue,
  renderMockStore,
  renderMockStoreId,
  storeId
} from 'app/test-utils';
import { PAYMENT_TYPE } from 'pages/MakePayment/SchedulePayment/types';
import { MemoType } from 'pages/Memos/types';
import React from 'react';
import * as SettlementSelector from '../_redux/selectors';
// components
import AcceptToPayForm from './AcceptToPayForm';
import * as SettlementPromiseToPayModule from './PromiseToPay';

HTMLCanvasElement.prototype.getContext = jest.fn();

const initialState: Partial<RootState> = {
  memo: {
    [storeId]: {
      configMemo: {
        data: {
          type: MemoType.CIS
        }
      }
    }
  },
  cisMemo: {
    [storeId]: {
      memoCISRefData: {
        typeRefData: [
          {
            value: '1',
            description: 'des'
          }
        ]
      }
    }
  },
  settlement: {
    [storeId]: {
      settlementInfo: {
        newFixedSettlementAmount: '200'
      },
      data: {
        settlementType: 'MakePayment'
      }
    }
  }
};

const renderWrapper = (
  initialState: Partial<RootState>,
  { paymentVia, setPaymentVia, onSubmit }: any
) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <AcceptToPayForm
        formId={'formId-123'}
        paymentVia={paymentVia || 'MakePayment'}
        setPaymentVia={setPaymentVia || jest.fn()}
        onSubmit={onSubmit || jest.fn()}
      />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('render', () => {
  beforeEach(() => {
    jest.resetModules();
    jest.restoreAllMocks();
  });

  afterEach(() => {
    jest.useRealTimers();
  });

  it(`Render UI with empty settlementInfo`, () => {
    const setPaymentVia = jest.fn();
    const onSubmit = jest.fn();
    renderWrapper(
      {
        settlement: {
          [storeId]: {
            data: {
              settlementType: 'MakePayment'
            }
          }
        }
      },
      { paymentVia: 'MakePayment', setPaymentVia, onSubmit }
    );
  });

  it(`Render UI paymentVia={MakePayment}`, () => {
    const setPaymentVia = jest.fn();
    const onSubmit = jest.fn();
    renderWrapper(initialState, {
      paymentVia: 'MakePayment',
      setPaymentVia,
      onSubmit
    });
  });

  it(`Render UI paymentVia={MakePayment} with submitErrorWithMethod === PAYMENT_TYPE.SCHEDULE`, () => {
    const setPaymentVia = jest.fn();
    const onSubmit = jest.fn();

    jest
      .spyOn(SettlementSelector, 'selectIsErrorWithPaymentSuccess')
      .mockReturnValue(true);

    jest
      .spyOn(SettlementSelector, 'selectIsErrorWithMethod')
      .mockReturnValue(PAYMENT_TYPE.SCHEDULE);

    renderWrapper(initialState, {
      paymentVia: 'MakePayment',
      setPaymentVia,
      onSubmit
    });
  });

  it(`Render UI paymentVia={MakePayment} with submitErrorWithMethod === PAYMENT_TYPE.DEMAND_ACH`, () => {
    const setPaymentVia = jest.fn();
    const onSubmit = jest.fn();

    jest
      .spyOn(SettlementSelector, 'selectIsErrorWithPaymentSuccess')
      .mockReturnValue(true);

    jest
      .spyOn(SettlementSelector, 'selectIsErrorWithMethod')
      .mockReturnValue(PAYMENT_TYPE.DEMAND_ACH);

    renderWrapper(initialState, {
      paymentVia: 'MakePayment',
      setPaymentVia,
      onSubmit
    });
  });

  it(`Render UI paymentVia={Promise}`, () => {
    const setPaymentVia = jest.fn();
    const onSubmit = jest.fn();
    renderWrapper(initialState, {
      paymentVia: 'Promise',
      setPaymentVia,
      onSubmit
    });
  });

  it('onChange Form with invalid value', () => {
    const setPaymentVia = jest.fn();
    const onSubmit = jest.fn();
    const { wrapper } = renderWrapper(initialState, {
      paymentVia: 'Promise',
      setPaymentVia,
      onSubmit
    });

    const input = wrapper.container.querySelectorAll('input')!;
    const textArea = wrapper.container.querySelector('textarea')!;

    fireEvent.change(input[0], { target: { value: '12' } });
    fireEvent.blur(input[0]);

    fireEvent.change(textArea, { target: { value: 'textArea' } });
    fireEvent.blur(textArea);

    expect(wrapper.getByText('txt_comment')).toBeInTheDocument();

    const form = wrapper.container.querySelectorAll('form');
    waitFor(() => {
      form[0].submit();
    });
  });

  it('onChange Form with valid value and paymentVia="Promise"', async () => {
    jest.useFakeTimers();

    const { wrapper } = renderMockStoreId(
      <AcceptToPayForm
        formId="formId"
        paymentVia="Promise"
        setPaymentVia={jest.fn()}
        onSubmit={jest.fn()}
      />,
      {
        initialState
      }
    );

    const form = wrapper.container.querySelectorAll('form');
    const input = wrapper.container.querySelectorAll('input')!;
    const textArea = wrapper.container.querySelector('textarea')!;

    fireEvent.change(input[0], { target: { value: '220' } });
    fireEvent.blur(input[0]);

    fireEvent.change(textArea, { target: { value: 'textArea' } });
    fireEvent.blur(textArea);

    const { container } = wrapper;
    const iconDropdownListElement = container.querySelectorAll(
      '.icon.icon-chevron-down'
    );

    userEvent.click(
      iconDropdownListElement[iconDropdownListElement.length - 1]!
    );

    await waitFor(() =>
      screen.getByTestId(
        'CIS-memo-header_search_description-popup-dropdown-list_dls-popup-base'
      )
    );

    await waitFor(() => userEvent.click(screen.getByText('des')));

    waitFor(() => {
      form[0].submit();
    });
  });

  it('onChange Form with valid value and paymentVia="MakePayment"', async () => {
    const { wrapper } = renderMockStoreId(
      <AcceptToPayForm
        formId="formId"
        paymentVia="MakePayment"
        setPaymentVia={jest.fn()}
        onSubmit={jest.fn()}
      />,

      {
        ...initialState,
        memo: {
          [storeId]: {
            configMemo: {
              data: {
                type: MemoType.CHRONICLE
              }
            }
          }
        }
      } as any
    );

    const form = wrapper.container.querySelectorAll('form');
    const input = wrapper.container.querySelectorAll('input')!;

    fireEvent.change(input[0], { target: { value: '220' } });
    fireEvent.blur(input[0]);

    waitFor(() => {
      form[0].submit();
    });
  });

  it('submit SettlementPromiseToPay form with valid value', async () => {
    jest
      .spyOn(SettlementPromiseToPayModule, 'default')
      .mockImplementation(({ onSubmit }: any) => {
        return (
          <form
            onSubmit={() => {
              onSubmit({
                promiseValues: 'promiseValues',
                settlementType: 'settlementType'
              });
            }}
          >
            <button type="submit">SubmitSettlementPromiseToPay</button>
          </form>
        );
      });

    renderMockStoreId(
      <AcceptToPayForm
        formId="formId"
        paymentVia="Promise"
        setPaymentVia={jest.fn()}
        onSubmit={jest.fn()}
      />,
      {
        initialState
      }
    );

    await waitFor(() =>
      fireEvent.click(
        screen.getByText(new RegExp('SubmitSettlementPromiseToPay', 'i'))
      )
    );
  });
});
