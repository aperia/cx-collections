import React from 'react';
import { renderMockStoreId, storeId } from 'app/test-utils';
import CommentMemo from './CommentMemo';
import { PromiseToPayType } from 'pages/PromiseToPay/types';
import { SettlementPayload } from '../types';
import { FormikProps } from 'formik';
import { MemoType } from 'pages/Memos/types';
import userEvent from '@testing-library/user-event';
import { waitFor } from '@testing-library/react';

HTMLCanvasElement.prototype.getContext = jest.fn();

const generateString = (number: number) => {
  let text = '';
  for (let i = 0; i <= number; i++) {
    text += String(i);
  }

  return text;
};

const initialState: Partial<RootState> = {
  memo: {
    [storeId]: {
      configMemo: {
        data: {
          type: MemoType.CIS
        }
      }
    }
  },
  cisMemo: {
    [storeId]: {
      memoCISRefData: {
        typeRefData: [
          {
            value: '123',
            description: 'des'
          }
        ]
      }
    }
  }
};

const memoTypeState: Partial<RootState> = {
  memo: {
    [storeId]: {
      configMemo: {
        data: {
          type: MemoType.CHRONICLE
        }
      }
    }
  }
};

describe('CommentMemo', () => {
  it('Render UI', () => {
    const formik: FormikProps<SettlementPayload> = {
      values: {
        settlementStatus: '',
        settlementType: 'MakePayment',
        promiseToPayData: [],
        commentMemo: '123',
        commentType: '123',
        acceptedBalanceAmount: '',
        method: '',
        acceptedToPay: true
      },
      setFieldValue: jest.fn(),
      handleChange: jest.fn(),
      handleBlur: jest.fn(),
      errors: {
        commentType: 'errors',
        commentMemo: 'errors'
      },
      touched: {
        commentType: true,
        commentMemo: true
      }
    };
    const { wrapper } = renderMockStoreId(
      <CommentMemo
        required
        formik={formik}
        promiseToPayType={PromiseToPayType.SINGLE}
      />,
      {
        initialState
      }
    );

    // OnChange
    const textBox = wrapper.container.querySelector('textarea')!;

    userEvent.type(textBox, '12345');

    expect(wrapper.getByText('txt_comment')).toBeInTheDocument();
  });

  it('Render UI with case 2', () => {
    const formik: FormikProps<SettlementPayload> = {
      values: {
        settlementStatus: '',
        settlementType: 'MakePayment',
        promiseToPayData: [],
        commentMemo: generateString(1002),
        commentType: generateString(1002),
        acceptedBalanceAmount: '',
        method: '',
        acceptedToPay: true
      },
      setFieldValue: jest.fn(),
      handleChange: jest.fn(),
      handleBlur: jest.fn(),
      errors: {
        commentType: 'errors',
        commentMemo: 'errors'
      },
      touched: {
        commentType: true,
        commentMemo: true
      }
    };
    const { wrapper } = renderMockStoreId(
      <CommentMemo
        required={false}
        formik={formik}
        promiseToPayType={PromiseToPayType.SINGLE}
      />,
      {
        initialState
      }
    );

    // OnChange
    const textBox = wrapper.container.querySelector('textarea')!;

    waitFor(() => {
      userEvent.type(textBox, generateString(1002));
    });

    expect(wrapper.getByText('txt_comment')).toBeInTheDocument();
  });

  it('Render UI with case 3', () => {
    const formik: FormikProps<SettlementPayload> = {
      values: {
        settlementStatus: '',
        settlementType: 'MakePayment',
        promiseToPayData: [],
        commentMemo: generateString(1002),
        commentType: generateString(1002),
        acceptedBalanceAmount: '',
        method: '',
        acceptedToPay: true
      },
      setFieldValue: jest.fn(),
      handleChange: jest.fn(),
      handleBlur: jest.fn(),
      errors: {
        commentType: 'errors',
        commentMemo: 'errors'
      },
      touched: {
        commentType: true,
        commentMemo: true
      }
    };
    const { wrapper } = renderMockStoreId(
      <CommentMemo
        required={false}
        formik={formik}
        promiseToPayType={PromiseToPayType.SINGLE}
      />,
      {
        initialState: memoTypeState
      }
    );

    // OnChange
    const textBox = wrapper.container.querySelector('textarea')!;

    waitFor(() => {
      userEvent.type(textBox, generateString(1002));
    });

    expect(wrapper.getByText('txt_comment')).toBeInTheDocument();
  });
});
