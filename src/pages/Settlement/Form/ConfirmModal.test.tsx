import React from 'react';
// components

// helpers
import ConfirmModal from './ConfirmModal';
import { render } from '@testing-library/react';

const setupRender = ({ openConfirm, data }: any) => {
  const handleCloseModal = jest.fn();
  const handleSubmit = jest.fn();

  const ui = (
    <ConfirmModal
      openConfirm={openConfirm || false}
      data={data || { amount: '100', date: '01-01-2021' }}
      handleSubmit={handleSubmit}
      handleCloseModal={handleCloseModal}
    />
  );

  const result = render(ui);

  return {
    ...result
  };
};

describe('render', () => {
  it.each`
    caseName                      | props
    ${'should not display modal'} | ${{}}
    ${'should not display modal'} | ${{ openConfirm: false }}
    ${'should display modal'}     | ${{ openConfirm: true }}
  `('$caseName', ({ props }) => {
    setupRender(props);
  });
});
