import React, { useEffect } from 'react';

// components
import { useFormik } from 'formik';
import CommentMemo from './CommentMemo';

// hooks
import { useAccountDetail } from 'app/hooks';

// Redux
import { useDispatch, useSelector } from 'react-redux';

// Const
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';

// types
import { AppState } from 'storeConfig';
import { MemoType } from 'pages/Memos/types';
import { SettlementPayload } from '../types';

// actions
import { settlementActions } from '../_redux/reducers';
import { PromiseToPayType } from 'pages/PromiseToPay/types';

export interface DeclineToPayFormProps {
  formId: string;
  onSubmit: (values: SettlementPayload) => void;
}

const DeclineToPayForm: React.FC<DeclineToPayFormProps> = ({
  formId,
  onSubmit
}) => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const memoType = useSelector<AppState, MemoType | undefined>(
    appState => appState.memo[storeId]?.configMemo?.data?.type
  );

  const formik = useFormik<SettlementPayload>({
    initialValues: {
      settlementType: 'NoPayment',
      commentMemo: '',
      commentType: ''
    },
    onSubmit: values => {
      onSubmit({
        commentMemo: values?.commentMemo,
        commentType: values?.commentType,
        settlementType: values?.settlementType
      });
    },
    validateOnBlur: true,
    validateOnChange: false,
    validate: ({ commentMemo, commentType }) => {
      const errors: Record<string, string> | undefined = {};
      if (!commentMemo?.trim()) {
        errors['commentMemo'] =
          I18N_COLLECTION_FORM.SETTLEMENT_COMMENT_REQUIRED;
      }
      if (memoType === MemoType.CIS && !commentType) {
        errors['commentType'] =
          I18N_COLLECTION_FORM.SETTLEMENT_TYPE_MEMO_REQUIRED;
      }

      return errors;
    }
  });

  const { submitForm, isValid, validateForm } = formik;

  useEffect(() => {
    validateForm();
  }, [validateForm]);

  useEffect(() => {
    dispatch(
      settlementActions.setValidForm({
        storeId,
        isValid: isValid
      })
    );
  }, [dispatch, storeId, isValid]);

  return (
    <>
      <form
        id={formId}
        onSubmit={e => {
          e.preventDefault();
          submitForm();
        }}
      >
        <CommentMemo
          required
          formik={formik}
          promiseToPayType={PromiseToPayType.SINGLE}
        />
      </form>
    </>
  );
};

export default DeclineToPayForm;
