import React from 'react';
// components

// helpers
import { renderMockStore, storeId, accEValue } from 'app/test-utils';
import { AccountDetailProvider } from 'app/hooks';
import MetricReference from './MetricReference';

const initialState: Partial<RootState> = {
  settlement: {
    [storeId]: {
      settlementInfo: {
        currentBalance: '100',
        lowestAllowedPercentage: '50'
      },
      data: {
        settlementType: 'MakePayment'
      }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <MetricReference />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('render', () => {
  it(`Render UI`, () => {
    renderWrapper(initialState);
  });

  it(`Render UI with empty settlementInfo`, () => {
    renderWrapper({
      ...initialState,
      settlement: {
        [storeId]: {
          ...[storeId],
          settlementInfo: undefined
        }
      }
    });
  });
  it(`Render UI with settlementInfo`, () => {
    renderWrapper({
      ...initialState,
      settlement: {
        [storeId]: {
          ...[storeId],
          settlementInfo: {
            paymentSuggestion: { '100': '400', '60': '120' }
          } as any
        }
      }
    });
  });
});
