import React from 'react';
// components
import { Radio } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Const
import { FORM_FIELD } from '../constant';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';

// types
import { AcceptanceToPayType } from '../types';

// selector
import { selectIsErrorWithPaymentSuccess } from '../_redux/selectors';
import { useStoreIdSelector } from 'app/hooks';

export interface SelectPaymentProps {
  paymentVia: AcceptanceToPayType;
  setPaymentVia: (value: AcceptanceToPayType) => void;
}

const SelectPayment: React.FC<SelectPaymentProps> = ({
  paymentVia,
  setPaymentVia
}) => {
  const { t } = useTranslation();
  const submitErrorWithPaymentSuccess = useStoreIdSelector<boolean | undefined>(
    selectIsErrorWithPaymentSuccess
  );

  return (
    <div className="my-24">
      <p className="fw-500 color-grey d-inline-block">
        {t(I18N_COLLECTION_FORM.SETTLEMENT_PAYMENT_VIA)}
      </p>
      <Radio className="d-inline-block ml-24">
        <Radio.Input
          checked={paymentVia === 'MakePayment'}
          name={FORM_FIELD.MAKE_PAYMENT.name}
          onChange={() => setPaymentVia('MakePayment')}
        />
        <Radio.Label>{t(FORM_FIELD.MAKE_PAYMENT.label)}</Radio.Label>
      </Radio>
      <Radio className="d-inline-block ml-24">
        <Radio.Input
          checked={paymentVia === 'Promise'}
          name={FORM_FIELD.PROMISE.name}
          onChange={() => setPaymentVia('Promise')}
          disabled={submitErrorWithPaymentSuccess}
        />
        <Radio.Label>{t(FORM_FIELD.PROMISE.label)}</Radio.Label>
      </Radio>
    </div>
  );
};

export default SelectPayment;
