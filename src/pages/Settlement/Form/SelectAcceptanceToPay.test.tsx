import { fireEvent, screen, waitFor } from '@testing-library/react';
import { AccountDetailProvider } from 'app/hooks';
// helpers
import { accEValue, renderMockStore, storeId } from 'app/test-utils';
import React from 'react';
import { FORM_FIELD } from '../constant';
import SelectAcceptanceToPay from './SelectAcceptanceToPay';

const initialState: Partial<RootState> = {
  settlement: {
    [storeId]: {
      data: {
        settlementType: 'MakePayment'
      }
    }
  }
};

const renderWrapper = (
  initialState: Partial<RootState>,
  { acceptedToPay, setAcceptedToPay }: any
) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <SelectAcceptanceToPay
        storeId={storeId}
        acceptedToPay={acceptedToPay || false}
        setAcceptedToPay={setAcceptedToPay || jest.fn()}
      />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('render', () => {
  it(`Render UI`, async () => {
    renderWrapper(initialState, {});
  });
});

describe('action', () => {
  it.each`
    labelText                          | expected
    ${FORM_FIELD.ACCEPT_TO_PAY.label}  | ${true}
    ${FORM_FIELD.DECLINE_TO_PAY.label} | ${false}
  `(`onChange $labelText`, async ({ labelText, expected }) => {
    const setAcceptedToPay = jest.fn();
    renderWrapper(initialState, { setAcceptedToPay });

    fireEvent.click(screen.getByText(labelText));

    await waitFor(() => expect(setAcceptedToPay).toHaveBeenCalledWith(expected));
  });
});
