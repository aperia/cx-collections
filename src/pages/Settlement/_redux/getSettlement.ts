import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { EMPTY_STRING } from 'app/constants';
import { isEqual } from 'lodash';
import isEmpty from 'lodash.isempty';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { dispatchDestroyDelayProgress } from 'pages/CollectionForm/helpers';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { PromiseToPayRecurringData } from 'pages/PromiseToPay/types';

// Service
import { settlementServices } from '../settlementServices';

// Type
import {
  GetSettlementArg,
  SettlementState,
  SETTLEMENT_TYPE_API
} from '../types';

export const getSettlement = createAsyncThunk<
  MagicKeyValue,
  GetSettlementArg,
  ThunkAPIConfig
>(
  'Settlement/getSettlement',
  async (args, { dispatch, getState, rejectWithValue }) => {
    const { accountId } = args.postData;
    const { storeId } = args;

    const { callingApplication, operatorID, org, app } =
      window?.appConfig?.commonConfig || {};
    const { inProgressInfo = {}, isInProgress } =
      getState().collectionForm[storeId] || {};
    const { accountDetail } = getState();
    const selectedCaller = accountDetail[storeId]?.selectedCaller;

    try {
      const requestBody = {
        common: {
          org: org || EMPTY_STRING,
          app: app || EMPTY_STRING,
          accountId
        },
        callingApplication: callingApplication || EMPTY_STRING,
        operatorCode: operatorID || EMPTY_STRING,
        cardholderContactedName: selectedCaller?.customerName ?? EMPTY_STRING,
        requestType: 'GET_STATUS'
      };

      const { data } = await settlementServices.getSettlement(requestBody);
      const info = {
        callResultType: CALL_RESULT_CODE.SETTLEMENT
      };
      if (
        isEqual(inProgressInfo, info) &&
        !isEmpty(inProgressInfo) &&
        isInProgress
      ) {
        dispatch(
          collectionFormActions.toggleCallResultInProgressStatus({
            storeId,
            info
          })
        );
        dispatchDestroyDelayProgress(storeId);
      }

      return data;
    } catch (error) {
      return rejectWithValue(error);
    }
  },
  {}
);

export const getSettlementBuilder = (
  builder: ActionReducerMapBuilder<SettlementState>
) => {
  builder
    .addCase(getSettlement.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true
      };
    })
    .addCase(getSettlement.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const data = action.payload;
      const { settlementType } = data;

      if (
        settlementType === SETTLEMENT_TYPE_API.SINGLE ||
        settlementType === SETTLEMENT_TYPE_API.HOST_PAYMENT ||
        settlementType === SETTLEMENT_TYPE_API.PAY_POINT_PAYMENT
      ) {
        const { latestSettlementSinglePtpDetail = {} } = action.payload;
        const { singlePromiseData } = latestSettlementSinglePtpDetail;
        const {
          paymentMethod = '',
          promiseAmount = '',
          promiseDueDate = ''
        } = singlePromiseData || {};
        draftState[storeId] = {
          ...draftState[storeId],
          isLoading: false,
          isError: false,
          data: {
            ...draftState[storeId].data,
            settlementType,
            acceptedBalanceAmount: promiseAmount,
            method: paymentMethod,
            promiseToPayData: {
              amount: promiseAmount,
              paymentDate: promiseDueDate,
              method: { value: paymentMethod, description: paymentMethod }
            }
          }
        };
      }
      switch (settlementType) {
        case SETTLEMENT_TYPE_API.RECURRING: {
          const { latestSettlementRecurringPtpDetail = {} } = action.payload;
          const {
            recurringPromiseDataList = [],
            totalPromiseAmount = '',
            recurringInterval = ''
          } = latestSettlementRecurringPtpDetail;

          const prepareData: PromiseToPayRecurringData = {
            interval: recurringInterval,
            listRecurringData: [
              ...recurringPromiseDataList.sort(
                (first: any, second: any) => first.promiseId - second.promiseId
              )
            ],
            amount: totalPromiseAmount
          };

          draftState[storeId] = {
            ...draftState[storeId],
            isLoading: false,
            isError: false,
            data: {
              ...draftState[storeId].data,
              settlementType,
              acceptedBalanceAmount: totalPromiseAmount,
              promiseToPayData: prepareData
            }
          };

          break;
        }
        case SETTLEMENT_TYPE_API.NON_RECURRING: {
          const { latestSettlementNonRecurringPtpDetail = {} } = action.payload;
          const { nonRecurringPromiseDataList = [], totalPromiseAmount = '' } =
            latestSettlementNonRecurringPtpDetail;

          draftState[storeId] = {
            ...draftState[storeId],
            isLoading: false,
            isError: false,
            data: {
              ...draftState[storeId].data,
              settlementType,
              acceptedBalanceAmount: totalPromiseAmount,
              promiseToPayData: [
                ...nonRecurringPromiseDataList.sort(
                  (first: any, second: any) =>
                    first.promiseId - second.promiseId
                )
              ]
            }
          };

          break;
        }
        default:
          break;
      }
    })
    .addCase(getSettlement.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false
      };
    });
};
