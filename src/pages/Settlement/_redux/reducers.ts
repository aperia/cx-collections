import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { SettlementState } from '../types';
import { getSettlementBuilder, getSettlement } from './getSettlement';
import {
  getSettlementInformationBuilder,
  getSettlementInformation
} from './getSettlementInformation';
import {
  triggerUpdateSettlementBuilder,
  triggerUpdateSettlement
} from './updateSettlement';

const { actions, reducer } = createSlice({
  name: 'settlement',
  initialState: {} as SettlementState,
  reducers: {
    setValidForm: (
      draftState,
      action: PayloadAction<StoreIdPayload & { isValid: boolean }>
    ) => {
      const { storeId, isValid } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isFormValid: isValid
      };
    },
    setPaymentSuccess: (
      draftState,
      action: PayloadAction<
        StoreIdPayload & {
          isSuccess?: boolean;
          confirmationNumber?: string;
          method: string;
          type: string;
        }
      >
    ) => {
      const { storeId, isSuccess, confirmationNumber, method, type } =
        action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isPaymentSuccess: isSuccess,
        confirmationNumber: confirmationNumber,
        method,
        type
      };
    },
    updatePaymentSuccess: (
      draftState,
      action: PayloadAction<StoreIdPayload & { isSuccess?: boolean }>
    ) => {
      const { storeId, isSuccess } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isPaymentSuccess: isSuccess
      };
    },
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    setAcceptPayment: (
      draftState,
      action: PayloadAction<StoreIdPayload & { acceptedToPay?: boolean }>
    ) => {
      const { storeId, acceptedToPay } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        data: {
          ...draftState[storeId]?.data,
          acceptedToPay
        }
      };
    }
  },
  extraReducers: builder => {
    getSettlementBuilder(builder);
    getSettlementInformationBuilder(builder);
    triggerUpdateSettlementBuilder(builder);
  }
});

const settlementActions = {
  ...actions,
  getSettlementInformation,
  getSettlement,
  triggerUpdateSettlement
};

export { settlementActions, reducer };
