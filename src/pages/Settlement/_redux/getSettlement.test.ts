import { FULFILLED, PENDING, REJECTED } from 'app/constants';
import {
  accEValue,
  mockAsyncThunk,
  storeId,
  responseDefault
} from 'app/test-utils';
import { mockBuilder } from 'app/test-utils/mockBuilder';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { settlementServices } from '../settlementServices';
import { GetSettlementArg, SettlementData } from '../types';
import { getSettlement } from './getSettlement';
import { createStore } from '@reduxjs/toolkit';
import { rootReducer } from 'storeConfig';

window['appConfig'] = {} as any;
const response = {};

const initialState = {
  settlement: {
    [storeId]: {} as SettlementData
  }
};

const args = {
  storeId,
  postData: { accountId: accEValue }
} as GetSettlementArg;

const setupGetSettlement = mockAsyncThunk({
  service: settlementServices,
  fn: getSettlement
})({ args });

const setupGetSettlementBuilder = mockBuilder(getSettlement, args);

const getSettlementCases = [
  {
    caseName: 'getSettlement > fulfilled with exists inProgressInfo',
    actionState: FULFILLED,
    collectionForm: {
      [storeId]: {
        inProgressInfo: {
          callResultType: CALL_RESULT_CODE.SETTLEMENT
        },
        isInProgress: true
      }
    },
    expected: {
      type: getSettlement.fulfilled.type,
      data: response
    }
  },
  {
    caseName: 'getSettlement > fulfilled with no inProgressInfo',
    actionState: FULFILLED,
    expected: {
      type: getSettlement.fulfilled.type,
      data: response
    }
  },
  {
    caseName: 'getSettlement > rejected from axios',
    actionState: REJECTED,
    expected: {
      type: getSettlement.rejected.type,
      data: new Error('rejected error')
    }
  },
  {
    caseName: 'getSettlement > rejected from condition',
    actionState: REJECTED,
    isLoading: true,
    expected: {
      type: getSettlement.rejected.type,
      data: new Error('rejected error')
    }
  }
];

describe.each(getSettlementCases)(
  'getSettlement',
  ({ caseName, actionState, expected, isLoading, collectionForm = {} }) => {
    it(caseName, async () => {
      const { result } = await setupGetSettlement({
        actionState,
        payload: response,
        initialState: {
          ...initialState,
          collectionForm,
          settlement: {
            [storeId]: {
              ...[storeId],
              isLoading: isLoading || false
            }
          }
        }
      });

      // Assert
      const { type, payload } = result;
      expect(type).toEqual(expected.type);
      expect(payload).toEqual(expected.data);
    });
  }
);

describe('getSettlementBuilder', () => {
  it.each`
    actionState  | expected
    ${PENDING}   | ${{ isLoading: true }}
    ${FULFILLED} | ${{ isLoading: false, '0': '123456789' }}
    ${REJECTED}  | ${{ isLoading: false }}
  `('$actionState', ({ actionState, expected }) => {
    const { actual } = setupGetSettlementBuilder(actionState, {
      ...initialState,
      settlement: {
        [storeId]: {
          ...[storeId],
          isLoading: false
        }
      }
    });

    // Then
    expect(actual?.settlement?.[storeId]).toEqual(
      expect.objectContaining(expected)
    );
  });
});

describe('Cover getSettlement', () => {
  it('Case 1 > type Single Promise', async () => {
    jest.spyOn(settlementServices, 'getSettlement').mockResolvedValue({
      ...responseDefault,
      data: {
        settlementType: 'Single Promise',
        latestSettlementSinglePtpDetail: {
          singlePromiseData: {
            paymentMethod: 'ACH',
            promiseAmount: '10',
            promiseDueDate: '2022-01-01'
          }
        }
      }
    });

    const store = createStore(rootReducer, initialState);

    await getSettlement(args)(store.dispatch, store.getState, {});
  });
  it('Case 1 > type Single Promise > latestSettlementSinglePtpDetail undefined', async () => {
    jest.spyOn(settlementServices, 'getSettlement').mockResolvedValue({
      ...responseDefault,
      data: {
        settlementType: 'Single Promise',
        latestSettlementSinglePtpDetail: undefined
      }
    });

    const store = createStore(rootReducer, initialState);

    await getSettlement(args)(store.dispatch, store.getState, {});
  });

  it('Case 2 > type Recurring Promise', async () => {
    jest.spyOn(settlementServices, 'getSettlement').mockResolvedValue({
      ...responseDefault,
      data: {
        settlementType: 'Recurring Promise',
        latestSettlementRecurringPtpDetail: {
          recurringPromiseDataList: [
            { promiseId: '9', paymentDate: '2022-10-10' },
            { promiseId: '1', paymentDate: '2022-11-10' }
          ],
          totalPromiseAmount: '100',
          recurringInterval: 'WEEKLY'
        }
      }
    });

    const store = createStore(rootReducer, initialState);

    const response = await getSettlement(args)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.payload).toEqual({
      latestSettlementRecurringPtpDetail: {
        recurringInterval: 'WEEKLY',
        recurringPromiseDataList: [
          { promiseId: '1', paymentDate: '2022-11-10' },
          { promiseId: '9', paymentDate: '2022-10-10' }
        ],
        totalPromiseAmount: '100'
      },
      settlementType: 'Recurring Promise'
    });
  });

  it('Case 2 > type Recurring Promise > latestSettlementRecurringPtpDetail undefined', async () => {
    jest.spyOn(settlementServices, 'getSettlement').mockResolvedValue({
      ...responseDefault,
      data: {
        settlementType: 'Recurring Promise',
        latestSettlementRecurringPtpDetail: undefined
      }
    });

    const store = createStore(rootReducer, initialState);

    const response = await getSettlement(args)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.payload).toEqual({
      latestSettlementRecurringPtpDetail: undefined,
      settlementType: 'Recurring Promise'
    });
  });

  it('Case 3 > type Non-Recurring Promise', async () => {
    jest.spyOn(settlementServices, 'getSettlement').mockResolvedValue({
      ...responseDefault,
      data: {
        settlementType: 'Non-Recurring Promise',
        latestSettlementNonRecurringPtpDetail: {
          nonRecurringPromiseDataList: [
            { promiseId: '9', paymentDate: '2022-10-10' },
            { promiseId: '1', paymentDate: '2022-11-10' }
          ],
          totalPromiseAmount: '100'
        }
      }
    });

    const store = createStore(rootReducer, initialState);

    const response = await getSettlement(args)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.payload).toEqual({
      latestSettlementNonRecurringPtpDetail: {
        nonRecurringPromiseDataList: [
          { promiseId: '1', paymentDate: '2022-11-10' },
          { promiseId: '9', paymentDate: '2022-10-10' }
        ],
        totalPromiseAmount: '100'
      },
      settlementType: 'Non-Recurring Promise'
    });
  });

  it('Case 3 > type Non-Recurring Promise > latestSettlementNonRecurringPtpDetail undefined', async () => {
    jest.spyOn(settlementServices, 'getSettlement').mockResolvedValue({
      ...responseDefault,
      data: {
        settlementType: 'Non-Recurring Promise',
        latestSettlementNonRecurringPtpDetail: undefined
      }
    });

    const store = createStore(rootReducer, initialState);

    const response = await getSettlement(args)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.payload).toEqual({
      latestSettlementNonRecurringPtpDetail: undefined,
      settlementType: 'Non-Recurring Promise'
    });
  });

  it('Case 4 > type undefined', async () => {
    jest.spyOn(settlementServices, 'getSettlement').mockResolvedValue({
      ...responseDefault,
      data: {
        settlementType: ''
      }
    });

    const store = createStore(rootReducer, initialState);

    const response = await getSettlement(args)(
      store.dispatch,
      store.getState,
      {}
    );
    // console.log(response);
    expect(response.payload).toEqual({
      settlementType: ''
    });
  });
});
