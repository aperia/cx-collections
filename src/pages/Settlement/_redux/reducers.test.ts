import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { convertToExpectedKeys, storeId } from 'app/test-utils';
import { AnyAction } from 'redux';
import { SettlementState } from '../types';
import { reducer, settlementActions } from './reducers';

const initialState: SettlementState = {
  [storeId]: {}
};

const paymentSuccessPayload = {
  isSuccess: true,
  confirmationNumber: '123',
  method: 'Bank',
  type: I18N_COLLECTION_FORM.SETTLEMENT_TYPE_MEMO_REQUIRED
};

const testCases = [
  {
    reducerName: 'setValidForm',
    payload: { isValid: true },
    expected: {
      toExpectedKeys: {
        isValid: 'isFormValid'
      }
    }
  },
  {
    reducerName: 'setPaymentSuccess',
    payload: paymentSuccessPayload,
    expected: {
      toExpectedKeys: {
        isSuccess: 'isPaymentSuccess'
      }
    }
  },
  {
    reducerName: 'updatePaymentSuccess',
    payload: { isSuccess: true, storeId },
    expected: {
      toExpectedKeys: {
        isSuccess: 'isPaymentSuccess'
      },
      removeKeys: ['storeId']
    }
  },
  {
    reducerName: 'removeStore',
    payload: { storeId },
    expected: undefined
  },
  {
    reducerName: 'setAcceptPayment',
    payload: { acceptedToPay: true },
    expected: {
      data: {
        acceptedToPay: true
      }
    }
  }
];

describe.each(testCases)(
  'Test settlement',
  ({ reducerName, payload, expected }) => {
    it(`${reducerName}`, () => {
      // Given
      const actionCreator =
        settlementActions[reducerName as keyof typeof settlementActions];
      const action = actionCreator({ storeId, ...payload } as any) as AnyAction;

      // When
      const state = reducer(initialState, action);

      // Then
      if (!expected) return expect(state?.[storeId]).toEqual(expected);

      const { toExpectedKeys = {}, removeKeys = [] } = expected;

      const expectedResult = convertToExpectedKeys({
        payload,
        mappingRule: toExpectedKeys,
        removeKeys
      });

      expect(state?.[storeId]?.data || state?.[storeId]).toEqual(
        expect.objectContaining(expectedResult)
      );
    });
  }
);
