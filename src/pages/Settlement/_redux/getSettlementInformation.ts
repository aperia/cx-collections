import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { EMPTY_STRING } from 'app/constants';
import { ceilNumberValue } from '../helpers';

// Service
import { settlementServices } from '../settlementServices';

// Type
import { GetSettlementArg, SettlementState } from '../types';

export const getSettlementInformation = createAsyncThunk<
  MagicKeyValue,
  GetSettlementArg,
  ThunkAPIConfig
>(
  'Settlement/getSettlementInformation',
  async (args, { dispatch, getState, rejectWithValue }) => {
    const { accountId } = args.postData;
    const { customerName } =
      getState().accountDetail[args.storeId]?.selectedCaller || {};

    const { callingApplication, operatorID, org, app } =
      window.appConfig?.commonConfig || {};

    try {
      const requestBody = {
        common: {
          org: org || EMPTY_STRING,
          app: app || EMPTY_STRING,
          accountId
        },
        callingApplication: callingApplication || '',
        cardholderContactedName: customerName,
        operatorCode: operatorID || EMPTY_STRING,
        requestType: 'GET_STATUS'
      };

      const { data } = await settlementServices.getSettlementInformation(
        requestBody
      );

      return {
        ...data,
        paymentSuggestion: ceilNumberValue(data.paymentSuggestion),
        currentBalanceAmount: Math.ceil(
          Number(data.currentBalanceAmount)
        ).toString(),
        newFixedSettlementAmount: Math.ceil(
          Number(data.newFixedSettlementAmount)
        ).toString()
      };
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const getSettlementInformationBuilder = (
  builder: ActionReducerMapBuilder<SettlementState>
) => {
  builder
    .addCase(getSettlementInformation.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true
      };
    })
    .addCase(getSettlementInformation.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const data = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        settlementInfo: data
      };
    })
    .addCase(getSettlementInformation.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false
      };
    });
};
