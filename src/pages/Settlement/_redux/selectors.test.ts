// utils
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { selectorWrapper, storeId } from 'app/test-utils';
import { PromiseToPayRequestData } from 'pages/PromiseToPay/types';
import * as selectors from 'pages/Settlement/_redux/selectors';
import { SettlementData, SettlementPayload, SettlementType } from '../types';

const defaultTest = {
  data: {
    acceptedBalanceAmount: '100',
    method: 'Bank'
  },
  method: 'Bank',
  isPaymentSuccess: true,
  isError: true,
  type: I18N_COLLECTION_FORM.SETTLEMENT_TYPE_MEMO_REQUIRED,
  confirmationNumber: '123',
  isLoading: true
};

const store: Partial<RootState> = {
  settlement: {
    [storeId]: {
      isFormValid: true,
      settlementInfo: {},
      data: {
        promiseToPayData: {} as PromiseToPayRequestData,
        settlementType: {} as SettlementType,
        acceptedBalanceAmount: defaultTest.data.acceptedBalanceAmount,
        method: defaultTest.data.method,
        acceptedToPay: true
      } as SettlementPayload,
      method: defaultTest.method,
      isPaymentSuccess: defaultTest.isPaymentSuccess,
      isLoading: defaultTest.isLoading,
      isError: defaultTest.isError,
      confirmationNumber: defaultTest.confirmationNumber,
      type: defaultTest.type
    } as SettlementData
  }
};

it.each`
  selectorName                         | expected
  ${'selectSettlementInfoData'}        | ${{ data: {}, emptyData: undefined }}
  ${'selectPromiseToPayData'}          | ${{ data: {}, emptyData: undefined }}
  ${'selectSettlementType'}            | ${{ data: {}, emptyData: undefined }}
  ${'selectAcceptedToPay'}             | ${{ data: true, emptyData: undefined }}
  ${'selectAcceptedBalanceAmount'}     | ${{ data: defaultTest.data.acceptedBalanceAmount, emptyData: undefined }}
  ${'methodMakePayment'}               | ${{ data: defaultTest.data.method, emptyData: undefined }}
  ${'selectIsPaymentSuccess'}          | ${{ data: defaultTest.isPaymentSuccess, emptyData: undefined }}
  ${'selectMethod'}                    | ${{ data: defaultTest.method, emptyData: undefined }}
  ${'selectIsError'}                   | ${{ data: defaultTest.isError, emptyData: undefined }}
  ${'selectIsErrorWithPaymentSuccess'} | ${{ data: defaultTest.isPaymentSuccess, emptyData: undefined }}
  ${'selectIsErrorWithMethod'}         | ${{ data: defaultTest.type, emptyData: undefined }}
  ${'selectConfirmationNumber'}        | ${{ data: defaultTest.confirmationNumber, emptyData: undefined }}
  ${'selectLoading'}                   | ${{ data: defaultTest.isLoading, emptyData: undefined }}
  ${'selectSettlementValidForm'}       | ${{ data: true, emptyData: false }}
`('$selectorName', ({ selectorName, expected }) => {
  // Given
  const handleSelector = selectors[selectorName as keyof typeof selectors];

  // When
  const { data, emptyData } = selectorWrapper(store)(handleSelector);

  // Then
  expect(data).toEqual(expected.data);
  expect(emptyData).toEqual(expected.emptyData);
});
