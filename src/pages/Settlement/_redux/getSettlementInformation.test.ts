import { FULFILLED, PENDING, REJECTED } from 'app/constants';
import { accEValue, mockAsyncThunk, storeId } from 'app/test-utils';
import { mockBuilder } from 'app/test-utils/mockBuilder';
import { settlementServices } from '../settlementServices';
import { GetSettlementArg, SettlementData } from '../types';
import { getSettlementInformation } from './getSettlementInformation';

window['appConfig'] = {} as any;
const response = {};

const initialState = {
  settlement: {
    [storeId]: {} as SettlementData
  }
};

const args = {
  storeId,
  postData: { accountId: accEValue }
} as GetSettlementArg;

const setupGetSettlementInformation = mockAsyncThunk({
  service: settlementServices,
  fn: getSettlementInformation
})({ args });

const setupGetSettlementInformationBuilder = mockBuilder(
  getSettlementInformation,
  args
);

const getSettlementInformationCases = [
  {
    caseName: 'getSettlementInformation > fulfilled',
    actionState: FULFILLED,
    expected: {
      type: getSettlementInformation.fulfilled.type,
      data: {
        currentBalanceAmount: 'NaN',
        newFixedSettlementAmount: 'NaN',
        paymentSuggestion: undefined
      }
    }
  },
  {
    caseName: 'getSettlementInformation > rejected from axios',
    actionState: REJECTED,
    expected: {
      type: getSettlementInformation.rejected.type,
      data: new Error('rejected error')
    }
  },
  {
    caseName: 'getSettlementInformation > rejected from condition',
    actionState: REJECTED,
    isLoading: true,
    expected: {
      type: getSettlementInformation.rejected.type,
      data: new Error('rejected error')
    }
  }
];

describe.each(getSettlementInformationCases)(
  'getSettlementInformation',
  ({ caseName, actionState, expected, isLoading }) => {
    it(caseName, async () => {
      const { result } = await setupGetSettlementInformation({
        actionState,
        payload: response,
        initialState: {
          ...initialState,
          settlement: {
            [storeId]: {
              ...[storeId],
              isLoading: isLoading || false
            }
          }
        }
      });

      // Assert
      const { type, payload } = result;
      expect(type).toEqual(expected.type);
      expect(payload).toEqual(expected.data);
    });
  }
);

describe('getSettlementInformationBuilder', () => {
  it.each`
    actionState  | expected
    ${PENDING}   | ${{ isLoading: true }}
    ${FULFILLED} | ${{ isLoading: false, settlementInfo: response }}
    ${REJECTED}  | ${{ isLoading: false }}
  `('$actionState', ({ actionState, expected }) => {
    const { actual } = setupGetSettlementInformationBuilder(actionState, {
      ...initialState,
      settlement: {
        [storeId]: {
          ...[storeId],
          isLoading: false
        }
      }
    });

    // Then
    expect(actual?.settlement?.[storeId]).toEqual(
      expect.objectContaining(expected)
    );
  });
});
