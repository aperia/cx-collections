import { FULFILLED, PENDING, REJECTED } from 'app/constants';
import { mockAsyncThunk, responseDefault, storeId } from 'app/test-utils';
import { mockBuilder } from 'app/test-utils/mockBuilder';
import { MemoType } from 'pages/Memos/types';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { settlementServices } from '../settlementServices';
import {
  SettlementType,
  SETTLEMENT_TYPE,
  TriggerUpdateSettlementArg,
  TRIGGER_TYPE
} from '../types';
import { triggerUpdateSettlement, updateSettlement } from './updateSettlement';

window['appConfig'] = {} as any;
const response = { status: 'success' };

const initialState = {
  memo: {
    [storeId]: {
      configMemo: {
        data: {
          type: MemoType.CIS
        }
      }
    }
  },
  settlement: {
    [storeId]: {
      ...[storeId],
      isLoading: false
    }
  }
};

const singlePromiseToPayData = {
  paymentDate: new Date().toString(),
  method: 'ACH',
  amount: 'test'
};

const args = {
  storeId,
  type: TRIGGER_TYPE.CREATE,
  postData: {
    data: {
      acceptedToPay: 'acceptedToPay',
      commentMemo: 'commentMemo',
      settlementType: SETTLEMENT_TYPE.SINGLE,
      method: 'ACH',
      promiseToPayData: {
        ...singlePromiseToPayData,
        interval: {
          value: 'WEEKLY'
        },
        count: {
          value: 'test'
        }
      }
    }
  } as any
} as TriggerUpdateSettlementArg;


const setupTriggerCreateSettlement = mockAsyncThunk({
  service: settlementServices,
  fn: updateSettlement,
  triggerFn: triggerUpdateSettlement
});

const setupTriggerEditSettlement = mockAsyncThunk({
  service: settlementServices,
  fn: updateSettlement,
  triggerFn: triggerUpdateSettlement
});

const setupUpdateSettlementBuilder = mockBuilder(triggerUpdateSettlement, args);
const getMockUpdateSettlementArgs = (type: SettlementType) => {
  if (type === SETTLEMENT_TYPE.NON_RECURRING)
    return {
      ...args,
      postData: {
        ...args.postData,
        data: {
          ...args.postData.data,
          settlementType: type,
          promiseToPayData: [singlePromiseToPayData]
        }
      }
    };
  return {
    ...args,
    postData: {
      ...args.postData,
      data: {
        ...args.postData.data,
        settlementType: type
      }
    }
  };
};

const updateSettlementCases = [
  {
    caseName: 'NoPayment > fulfilled',
    args: getMockUpdateSettlementArgs('NoPayment'),
    actionState: FULFILLED,
    expected: {
      type: updateSettlement.fulfilled.type,
      data: {}
    }
  },
  {
    caseName: 'MakePayment > fulfilled',
    args: getMockUpdateSettlementArgs(SETTLEMENT_TYPE.MAKE_PAYMENT),
    actionState: FULFILLED,
    expected: {
      type: updateSettlement.fulfilled.type,
      data: response
    }
  },
  {
    caseName: 'single > fulfilled',
    args: getMockUpdateSettlementArgs(SETTLEMENT_TYPE.SINGLE),
    actionState: FULFILLED,
    expected: {
      type: updateSettlement.fulfilled.type,
      data: response
    }
  },
  {
    caseName: 'recurring > fulfilled',
    args: getMockUpdateSettlementArgs(SETTLEMENT_TYPE.RECURRING),
    actionState: FULFILLED,
    expected: {
      type: updateSettlement.fulfilled.type,
      data: response
    }
  },
  {
    caseName: 'non recurring > fulfilled',
    args: getMockUpdateSettlementArgs(SETTLEMENT_TYPE.NON_RECURRING),
    actionState: FULFILLED,
    expected: {
      type: updateSettlement.fulfilled.type,
      data: response
    }
  },
  {
    caseName: 'rejected from axios',
    args: getMockUpdateSettlementArgs(SETTLEMENT_TYPE.SINGLE),
    actionState: REJECTED,
    expected: {
      type: updateSettlement.rejected.type,
      data: { response: new Error('rejected error') }
    }
  }
];

describe.each(updateSettlementCases)(
  'updateSettlement > create > ',
  ({ caseName, actionState, expected, args: updateSettlementArgs }) => {
    let updateSingleSettlementSpy: jest.SpyInstance,
      updateRecurringSettlementSpy: jest.SpyInstance,
      updateSettlementNonRecurringSpy: jest.SpyInstance;
    let store: Store<RootState>;

    beforeEach(() => {
      if (actionState === FULFILLED) {
        updateSingleSettlementSpy = jest
          .spyOn(settlementServices, 'updateSettlementSingle')
          .mockResolvedValue({
            ...responseDefault,
            data: expected.data
          });
        updateRecurringSettlementSpy = jest
          .spyOn(settlementServices, 'updateSettlementRecurring')
          .mockResolvedValue({
            ...responseDefault,
            data: expected.data
          });
        updateSettlementNonRecurringSpy = jest
          .spyOn(settlementServices, 'updateSettlementNonRecurring')
          .mockResolvedValue({
            ...responseDefault,
            data: expected.data
          });
      } else {
        window.appConfig = undefined as any;
        delete updateSettlementArgs.postData.common;
        updateSingleSettlementSpy = jest
          .spyOn(settlementServices, 'updateSettlementSingle')
          .mockRejectedValue({
            ...responseDefault,
            data: expected.data
          });
        updateRecurringSettlementSpy = jest
          .spyOn(settlementServices, 'updateSettlementRecurring')
          .mockRejectedValue({
            ...responseDefault,
            data: expected.data
          });
        updateSettlementNonRecurringSpy = jest
          .spyOn(settlementServices, 'updateSettlementNonRecurring')
          .mockRejectedValue({
            ...responseDefault,
            data: expected.data
          });
      }
      store = createStore(rootReducer, initialState);
    });

    afterEach(() => {
      updateSingleSettlementSpy?.mockReset();
      updateSingleSettlementSpy?.mockRestore();

      updateRecurringSettlementSpy?.mockReset();
      updateRecurringSettlementSpy?.mockRestore();

      updateSettlementNonRecurringSpy?.mockReset();
      updateSettlementNonRecurringSpy?.mockRestore();
    });

    it(caseName, async () => {
      const response = await updateSettlement(updateSettlementArgs as any)(
        store.dispatch,
        store.getState,
        {}
      );

      // Assert
      expect(response.type).toEqual(expected.type);
      if (actionState === FULFILLED) {
        expect(response.payload).toEqual(expected.data);
      }
    });
  }
);

const triggerUpdateSettlementCases = [
  {
    caseName: `fulfilled when memoType === ${MemoType.CIS}`,
    actionState: FULFILLED,
    expected: {
      type: triggerUpdateSettlement.fulfilled.type,
      data: undefined
    }
  },
  {
    caseName: `fulfilled when memoType === ${MemoType.CHRONICLE}`,
    actionState: FULFILLED,
    initialState: {
      ...initialState,
      memo: {
        [storeId]: {
          configMemo: {
            data: {
              type: MemoType.CHRONICLE
            }
          }
        }
      }
    },
    expected: {
      type: triggerUpdateSettlement.fulfilled.type,
      data: undefined
    }
  },
  {
    caseName: 'fulfilled with settlementType === NoPayment',
    actionState: FULFILLED,
    postData: {
      common: {},
      data: {
        settlementType: 'NoPayment',
        commentMemo: 'commentMemo'
      }
    },
    expected: {
      type: triggerUpdateSettlement.fulfilled.type,
      data: undefined
    }
  },
  {
    caseName: 'fulfilled with empty postData',
    actionState: FULFILLED,
    postData: {},
    expected: {
      type: triggerUpdateSettlement.fulfilled.type,
      data: undefined
    }
  },
  {
    caseName: 'rejected from axios',
    actionState: REJECTED,
    expected: {
      type: triggerUpdateSettlement.rejected.type,
      data: {}
    }
  }
];

describe.each(triggerUpdateSettlementCases)(
  'triggerUpdateSettlement > create > ',
  ({ caseName, actionState, initialState, postData, expected }) => {
    it(caseName, async () => {
      const { result } = await setupTriggerCreateSettlement({
        args: {
          ...args,
          ...(postData && { postData })
        }
      })({
        actionState,
        payload: { ...response },
        initialState
      });

      // Assert
      expect(result.type).toEqual(expected.type);
      expect(result.payload).toEqual(expected.data);
    });
  }
);

describe.each(triggerUpdateSettlementCases)(
  'triggerUpdateSettlement > edit > ',
  ({ caseName, actionState, expected }) => {
    it(caseName, async () => {
      const { result } = await setupTriggerEditSettlement({
        args: { ...args, type: TRIGGER_TYPE.EDIT }
      })({
        actionState,
        payload: response,
        initialState
      });

      // Assert
      expect(result.type).toEqual(expected.type);
      expect(result.payload).toEqual(expected.data);
    });
  }
);

describe('triggerUpdateSettlementBuilder', () => {
  it.each`
    actionState | expected
    ${PENDING}  | ${{ isLoading: true }}
    ${REJECTED} | ${{ isLoading: false, isError: true }}
    ${FULFILLED} | ${{
  isLoading: false,
  isError: false,
  isPaymentSuccess: undefined,
  confirmationNumber: undefined
}}
  `('$actionState', ({ actionState, expected }) => {
    const { actual } = setupUpdateSettlementBuilder(actionState, initialState);

    // Then
    expect(actual?.settlement?.[storeId]).toEqual(
      expect.objectContaining(expected)
    );
  });
});
