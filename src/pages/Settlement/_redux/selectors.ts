import { createSelector } from '@reduxjs/toolkit';

const getSettlementData = (state: RootState, storeId: string) => {
  return state.settlement?.[storeId];
};

export const selectSettlementInfoData = createSelector(
  getSettlementData,
  settlement => settlement?.settlementInfo
);

export const selectPromiseToPayData = createSelector(
  getSettlementData,
  settlement => settlement?.data?.promiseToPayData
);

export const selectSettlementType = createSelector(
  getSettlementData,
  settlement => settlement?.data?.settlementType
);

export const selectAcceptedToPay = createSelector(
  getSettlementData,
  settlement => settlement?.data?.acceptedToPay
);

export const selectAcceptedBalanceAmount = createSelector(
  getSettlementData,
  settlement => settlement?.data?.acceptedBalanceAmount
);

export const methodMakePayment = createSelector(
  getSettlementData,
  settlement => settlement?.data?.method
);

export const selectIsPaymentSuccess = createSelector(
  getSettlementData,
  data => data?.isPaymentSuccess
);

export const selectMethod = createSelector(
  getSettlementData,
  data => data?.method
);

export const selectIsError = createSelector(
  getSettlementData,
  loading => loading?.isError
);

export const selectIsErrorWithPaymentSuccess = createSelector(
  getSettlementData,
  data => data?.isError && data?.isPaymentSuccess
);

export const selectIsErrorWithMethod = createSelector(
  getSettlementData,
  data => data?.isError && data?.type
);

export const selectConfirmationNumber = createSelector(
  getSettlementData,
  data => data?.isError && data?.confirmationNumber
);

export const selectLoading = createSelector(
  getSettlementData,
  loading => loading?.isLoading
);

export const selectSettlementValidForm = createSelector(
  getSettlementData,
  settlement => settlement?.isFormValid ?? false
);
