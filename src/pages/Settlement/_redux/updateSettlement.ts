import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import {
  CALL_RESULT_CODE,
  COLLECTION_METHOD
} from 'pages/CollectionForm/constants';
import {
  dispatchDelayProgress,
  setDelayProgress
} from 'pages/CollectionForm/helpers';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { MemoChronicleType } from 'pages/Memos/Chronicle/types';
import { triggerAddChronicleMemo } from 'pages/Memos/Chronicle/_redux/add';
import { cisMemoActions } from 'pages/Memos/CIS/_redux/reducers';
import { MemoType } from 'pages/Memos/types';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import { settlementServices } from '../settlementServices';
import {
  TriggerUpdateSettlementArg,
  SettlementState,
  SETTLEMENT_TYPE
} from '../types';
import { settlementActions } from './reducers';
import { prepareUpdateSettlementData } from '../helpers';

export const updateSettlement = createAsyncThunk<
  MagicKeyValue,
  TriggerUpdateSettlementArg,
  ThunkAPIConfig
>('Settlement/updateSettlement', async (args, thunkAPI) => {
  try {
    const { storeId, postData } = args;
    const { accountId } = postData.common || {};
    const { commonConfig } = window.appConfig || {};

    const { accountDetail } = thunkAPI.getState();
    const selectedCaller = accountDetail[storeId]?.selectedCaller;

    const { data: settlementPayload } = postData;
    const { settlementType } = settlementPayload;

    if (settlementType === 'NoPayment') {
      return {};
    }

    const submittedData = prepareUpdateSettlementData(settlementPayload);
    const requestData = {
      common: {
        app: commonConfig?.app,
        org: commonConfig?.org,
        accountId
      },
      accountExternalStatus: 'A',
      returnPhoneNumber: accountDetail[storeId]?.numberPhoneAuthenticated,
      callingApplication: commonConfig?.callingApplication,
      operatorCode: commonConfig?.operatorID,
      cardholderContactedName: selectedCaller?.customerName ?? '',
      ...submittedData
    };

    const settlementService = {
      [SETTLEMENT_TYPE.MAKE_PAYMENT]: settlementServices.updateSettlementSingle,
      [SETTLEMENT_TYPE.SINGLE]: settlementServices.updateSettlementSingle,
      [SETTLEMENT_TYPE.RECURRING]: settlementServices.updateSettlementRecurring,
      [SETTLEMENT_TYPE.NON_RECURRING]:
        settlementServices.updateSettlementNonRecurring
    };

    const settlementServiceHandler =
      settlementService[settlementType as keyof typeof settlementService];

    const { data } = await settlementServiceHandler(requestData);

    return data;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerUpdateSettlement = createAsyncThunk<
  unknown,
  TriggerUpdateSettlementArg,
  ThunkAPIConfig
>('Settlement/triggerUpdateSettlement', async (args, thunkAPI) => {
  const { storeId, postData } = args;

  const { accountId = '' } = postData.common || {};
  const { dispatch, rejectWithValue, getState } = thunkAPI;

  const response = await dispatch(updateSettlement(args));

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
        })
      );

      dispatch(collectionFormActions.setCurrentView({ storeId }));

      dispatch(
        collectionFormActions.selectCallResult({ storeId, item: undefined })
      );

      dispatch(
        collectionFormActions.changeMethod({
          storeId,
          method: COLLECTION_METHOD.CREATE
        })
      );

      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inCollectionFormFlyOut'
        })
      );

      // add memo
      if (
        postData?.data?.settlementType !== 'MakePayment' &&
        !!postData?.data?.commentMemo
      ) {
        const memoType = getState().memo[storeId]?.configMemo?.data?.type;
        if (memoType === MemoType.CIS) {
          const { operatorID = '' } = window.appConfig?.commonConfig || {};
          dispatch(
            cisMemoActions.triggerAddCISMemoRequest({
              storeId,
              payload: {
                accountId: accountId!,
                memoText: postData?.data?.commentMemo,
                memoType: postData.data.commentType!,
                memoOperatorIdentifier: operatorID
              }
            })
          );
        }
        if (memoType === MemoType.CHRONICLE) {
          const memoTypeDefault: MemoChronicleType = {
            value: 'X',
            description: 'TYPE X - collection Memo Behavior'
          };
          dispatch(
            triggerAddChronicleMemo({
              storeId,
              postData: {
                memoText: postData?.data?.commentMemo,
                memoType: memoTypeDefault,
                accountId: accountId!
              }
            })
          );
        }
      }

      const info = {
        callResultType: CALL_RESULT_CODE.SETTLEMENT
      };
      dispatch(
        settlementActions.setAcceptPayment({
          storeId,
          acceptedToPay: postData?.data?.acceptedToPay
        })
      );

      dispatch(
        collectionFormActions.toggleCallResultInProgressStatus({
          storeId,
          info,
          lastDelayCallResult: CALL_RESULT_CODE.SETTLEMENT
        })
      );

      // getSettlementOffer
      dispatch(
        settlementActions.getSettlementInformation({
          storeId,
          postData: { accountId }
        })
      );

      dispatchDelayProgress(
        settlementActions.getSettlement({
          storeId,
          postData: { accountId: accountId || '' }
        }),
        storeId
      );

      setDelayProgress(storeId, CALL_RESULT_CODE.SETTLEMENT, info);
    });
  }

  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMIT_FAILED
        })
      );

      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inCollectionFormFlyOut',
          apiResponse: response.payload?.response
        })
      );
    });
    return rejectWithValue({});
  }
});

export const triggerUpdateSettlementBuilder = (
  builder: ActionReducerMapBuilder<SettlementState>
) => {
  builder
    .addCase(triggerUpdateSettlement.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true
      };
    })
    .addCase(triggerUpdateSettlement.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: false,
        isPaymentSuccess: undefined,
        confirmationNumber: undefined
      };
    })
    .addCase(triggerUpdateSettlement.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: true
      };
    });
};
