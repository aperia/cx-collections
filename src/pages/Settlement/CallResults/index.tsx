import React, { useMemo } from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// redux
import {
  selectAcceptedBalanceAmount,
  selectPromiseToPayData,
  selectSettlementInfoData,
  selectSettlementType
} from '../_redux/selectors';

// helper
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';

import { View } from 'app/_libraries/_dof/core';
import PromiseCallResults from './PromiseCallResults';
import {
  SettlementInfoPayload,
  SettlementType,
  SettlementTypeApi,
  SETTLEMENT_TYPE_API
} from '../types';
import { PromiseToPayDetailData } from 'pages/PromiseToPay/types';
import { isEmpty } from 'lodash';
import { formatCommon } from 'app/helpers';
import { prepareDataView } from './helpers';

export interface CallResultsProps {}

const CallResults: React.FC<CallResultsProps> = () => {
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();

  const promiseToPayDetailType =
    useStoreIdSelector<SettlementType>(selectSettlementType);

  const promiseToPayDetailData = useStoreIdSelector<PromiseToPayDetailData>(
    selectPromiseToPayData
  );
  const acceptedBalanceAmount = useStoreIdSelector<string>(
    selectAcceptedBalanceAmount
  );

  const settlementInfo = useStoreIdSelector<SettlementInfoPayload>(
    selectSettlementInfoData
  );

  const hasPromiseData =
    !isEmpty(promiseToPayDetailType) &&
    promiseToPayDetailType !== SETTLEMENT_TYPE_API.PAY_POINT_PAYMENT &&
    promiseToPayDetailType !== SETTLEMENT_TYPE_API.HOST_PAYMENT;

  const isPaymentType =
    promiseToPayDetailType === SETTLEMENT_TYPE_API.HOST_PAYMENT ||
    promiseToPayDetailType === SETTLEMENT_TYPE_API.PAY_POINT_PAYMENT;

  const renderSettlementInformationCallResult = useMemo(() => {
    return (
      <View
        descriptor="settlementInformationCallResult"
        formKey={`${storeId}__settlementInformationCallResult`}
        value={settlementInfo}
      />
    );
  }, [storeId, settlementInfo]);

  const renderPaymentInformation = useMemo(() => {
    return (
      <View
        descriptor="promiseToPayDetailSingleViews"
        formKey={`${storeId}__settlementPaymentInfo`}
        value={{
          ...prepareDataView(
            promiseToPayDetailData,
            promiseToPayDetailType as SettlementTypeApi
          )
        }}
      />
    );
  }, [storeId, promiseToPayDetailData, promiseToPayDetailType]);

  return (
    <>
      <h6 className="color-grey mt-8">
        {t(I18N_COLLECTION_FORM.SETTLEMENT_INFORMATION)}
      </h6>
      {renderSettlementInformationCallResult}
      <div className="mt-14 bg-light-l20 border br-light-l04 br-radius-8 p-16">
        <div className="d-flex">
          <p className="fs-14 fw-500 color-grey-d20">
            {t('txt_cardholder_acceptance_to_pay')}
          </p>
          <p className="ml-8"> {t('txt_yes')}</p>
        </div>
        <div className="d-flex mt-8">
          <p className="fs-14 fw-500 color-grey-d20">
            {t('txt_accepted_balance_amount')}:
          </p>
          <p className="ml-8">
            {formatCommon(acceptedBalanceAmount!).currency(2)}
          </p>
        </div>
      </div>
      {isPaymentType && (
        <div>
          <div className="divider-dashed my-24" />
          <h6 className="color-grey mb-16">{t('txt_paymentInformation')}</h6>
          {renderPaymentInformation}
        </div>
      )}

      {hasPromiseData && (
        <PromiseCallResults
          promiseToPayDetailType={promiseToPayDetailType}
          promiseToPayDetailData={promiseToPayDetailData}
        />
      )}
    </>
  );
};

export default CallResults;
