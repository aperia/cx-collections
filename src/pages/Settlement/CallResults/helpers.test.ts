import { prepareDataView, genRecurringPromisesData } from './helpers';

describe('test heplers CallResults', () => {
  it('prepareDataView case 1', () => {
    const result = prepareDataView(
      {
        amount: '1',
        method: { value: 'xxx', description: 'yyy' },
        paymentDate: '2022-10-10'
      },
      'Single Promise'
    );
    expect(result).toEqual({
      amount: '1',
      method: 'xxx',
      paymentDate: '2022-10-10'
    });
  });

  it('prepareDataView case 2 > Non data', () => {
    const result = prepareDataView(
      {
        interval: 'WEEKLY',
        listRecurringData: undefined
      },
      'Recurring Promise'
    );
    expect(result).toEqual({});
  });
  it('prepareDataView case 2', () => {
    const result = prepareDataView(
      {
        interval: 'WEEKLY',
        listRecurringData: [
          {
            promiseAmount: '1',
            paymentMethod: 'ACH',
            promiseDueDate: '2022-10-10'
          },
          {
            promiseAmount: '1',
            paymentMethod: 'ACH',
            promiseDueDate: '2022-10-10'
          }
        ]
      },
      'Recurring Promise'
    );
    expect(result).toEqual({
      amount: 2,
      method: 'ACH',
      interval: 'WEEKLY',
      count: 2,
      startDate: '2022-10-10',
      endDate: '2022-10-10'
    });
  });

  it('prepareDataView case 3', () => {
    const result = prepareDataView({}, 'Non-Recurring Promise');
    expect(result).toEqual({
      amount: undefined,
      method: undefined,
      paymentDate: undefined
    });
  });

  it('prepareDataView case 4', () => {
    const result = prepareDataView({}, 'xxx' as never);
    expect(result).toEqual(undefined);
  });

  it('genRecurringPromisesData case 1 > Non Data', () => {
    const result = genRecurringPromisesData({
      interval: 'WEEKLY',
      listRecurringData: undefined
    });
    expect(result).toEqual([]);
  });

  it('genRecurringPromisesData case 1', () => {
    const result = genRecurringPromisesData({
      interval: 'WEEKLY',
      listRecurringData: [
        {
          promiseAmount: '1',
          paymentMethod: 'ACH',
          promiseDueDate: '2022-10-10'
        },
        {
          promiseAmount: '1',
          paymentMethod: 'ACH',
          promiseDueDate: '2022-10-10'
        }
      ]
    });
    expect(result).toEqual([
      { amount: '1', paymentDate: '2022-10-10', paymentMethod: 'ACH' },
      { amount: '1', paymentDate: '2022-10-10', paymentMethod: 'ACH' }
    ]);
  });

  it('genRecurringPromisesData case 2', () => {
    const result = genRecurringPromisesData({
      interval: 'BIWEEKLY',
      listRecurringData: [
        {
          promiseAmount: '1',
          paymentMethod: 'ACH',
          promiseDueDate: '2022-10-10'
        },
        {
          promiseAmount: '1',
          paymentMethod: 'ACH',
          promiseDueDate: '2022-10-24'
        }
      ]
    });
    expect(result).toEqual([
      { amount: '1', paymentDate: '2022-10-10', paymentMethod: 'ACH' },
      { amount: '1', paymentDate: '2022-10-24', paymentMethod: 'ACH' }
    ]);
  });

  it('genRecurringPromisesData case 3', () => {
    const result = genRecurringPromisesData({
      interval: 'MONTHLY',
      listRecurringData: [
        {
          promiseAmount: '1',
          paymentMethod: 'ACH',
          promiseDueDate: '2022-10-10'
        },
        {
          promiseAmount: '1',
          paymentMethod: 'ACH',
          promiseDueDate: '2022-10-11'
        }
      ]
    });
    expect(result).toEqual([
      { amount: '1', paymentDate: '2022-10-10', paymentMethod: 'ACH' },
      { amount: '1', paymentDate: '2022-10-11', paymentMethod: 'ACH' }
    ]);
  });

  it('genRecurringPromisesData case 4', () => {
    const result = genRecurringPromisesData({
      interval: '',
      listRecurringData: [
        {
          promiseAmount: '1',
          paymentMethod: 'ACH',
          promiseDueDate: '2022-10-10'
        },
        {
          promiseAmount: '1',
          paymentMethod: 'ACH',
          promiseDueDate: '2022-10-10'
        }
      ]
    });
    expect(result).toEqual([
      { amount: '1', paymentDate: '2022-10-10', paymentMethod: 'ACH' },
      { amount: '1', paymentDate: '2022-10-10', paymentMethod: 'ACH' }
    ]);
  });

  it('genRecurringPromisesData case 5', () => {
    const result = genRecurringPromisesData({
      interval: '',
      listRecurringData: []
    });
    expect(result).toEqual([]);
  });
});
