import { isArray } from 'app/_libraries/_dls/lodash';
import {
  SettlementType,
  SettlementTypeApi,
  SETTLEMENT_TYPE_API
} from '../types';
import { PromiseToPayRecurringResponse } from 'pages/PromiseToPay/types';

export const prepareDataView = (
  dataView: any,
  settlementType: SettlementType | SettlementTypeApi
) => {
  if (
    settlementType === SETTLEMENT_TYPE_API.SINGLE ||
    settlementType === SETTLEMENT_TYPE_API.HOST_PAYMENT ||
    settlementType === SETTLEMENT_TYPE_API.PAY_POINT_PAYMENT
  ) {
    return {
      amount: dataView.amount,
      method: dataView?.method.value,
      paymentDate: dataView?.paymentDate
    };
  }
  switch (settlementType) {
    case SETTLEMENT_TYPE_API.RECURRING: {
      const { interval, listRecurringData } = dataView;
      if (!isArray(listRecurringData)) {
        return {};
      }
      const totalAmount = listRecurringData.reduce(
        (acc, cur: any) => (acc += Number(cur.promiseAmount)),
        0
      );

      return {
        amount: totalAmount,
        method: listRecurringData[0].paymentMethod,
        interval,
        count: listRecurringData?.length,
        startDate: listRecurringData[0].promiseDueDate,
        endDate: listRecurringData[listRecurringData.length - 1].promiseDueDate
      };
    }

    case SETTLEMENT_TYPE_API.NON_RECURRING: {
      return {
        amount: dataView?.promiseAmount,
        method: dataView?.paymentMethod,
        paymentDate: dataView?.promiseDueDate
      };
    }

    default:
      break;
  }
};

export const genRecurringPromisesData = (
  dataView: PromiseToPayRecurringResponse
) => {
  const { listRecurringData } = dataView;
  if (!isArray(listRecurringData) || listRecurringData.length === 0) {
    return [];
  }
  const result = [];

  const paymentMethod = listRecurringData[0]?.paymentMethod;

  for (let i = 0; i < listRecurringData?.length; i++) {
    result.push({
      paymentMethod,
      paymentDate: listRecurringData[i]?.promiseDueDate,
      amount: listRecurringData[i]?.promiseAmount
    });
  }

  return result;
};
