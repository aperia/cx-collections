import React from 'react';
// components
import CallResult from './index';

// helpers
import { renderMockStore, storeId, accEValue } from 'app/test-utils';
import { AccountDetailProvider } from 'app/hooks';

const initialState: Partial<RootState> = {
  settlement: {
    [storeId]: {
      data: {
        settlementType: 'Host Payment',
        promiseToPayData: {
          amount: '100',
          paymentDate: '2022-10-10',
          method: { value: 'ACH', description: 'ACH' }
        }
      } as any
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <CallResult />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('render', () => {
  it(`Render UI when promiseToPayDetailType === 'Host Payment'`, () => {
    renderWrapper(initialState);
  });

  it(`Render UI when promiseToPayDetailType === 'PayPoint Payment'`, () => {
    renderWrapper({
      settlement: {
        [storeId]: {
          data: {
            settlementType: 'PayPoint Payment',
            promiseToPayData: {
              amount: '100',
              paymentDate: '2022-10-10',
              method: { value: 'ACH', description: 'ACH' }
            }
          }
        }
      } as any
    });
  });

  it(`Render UI when hasPromiseData`, () => {
    renderWrapper({
      settlement: {
        [storeId]: {
          data: {
            settlementType: 'Single Promise',
            promiseToPayData: {
              amount: '100',
              paymentDate: '2022-10-10',
              method: { value: 'ACH', description: 'ACH' }
            }
          }
        }
      } as any
    });
  });
});
