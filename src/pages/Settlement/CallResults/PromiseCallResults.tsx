import React, { useCallback, useMemo } from 'react';

// components
import { View } from 'app/_libraries/_dof/core';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail } from 'app/hooks';

// helpers
import isArray from 'lodash.isarray';
import {
  PromiseToPayDetailData,
  PromiseToPayRecurringResponse,
  RecurringGridItem
} from 'pages/PromiseToPay/types';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { RecurringPromisesGrid } from 'pages/PromiseToPay/RecurringPromisesGrid';
import {
  SettlementType,
  SettlementTypeApi,
  SETTLEMENT_TYPE,
  SETTLEMENT_TYPE_API
} from '../types';
import { genRecurringPromisesData, prepareDataView } from './helpers';

export interface PromiseToPayProps {
  promiseToPayDetailType?: SettlementType | SettlementTypeApi;
  promiseToPayDetailData?: PromiseToPayDetailData;
}

const PromiseCallResults: React.FC<PromiseToPayProps> = ({
  promiseToPayDetailType,
  promiseToPayDetailData
}) => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const generatePromiseToPayType = () => {
    if (
      promiseToPayDetailType === SETTLEMENT_TYPE.RECURRING ||
      promiseToPayDetailType === SETTLEMENT_TYPE_API.RECURRING
    )
      return t(I18N_COLLECTION_FORM.RECURRING);
    if (
      promiseToPayDetailType === SETTLEMENT_TYPE.NON_RECURRING ||
      promiseToPayDetailType === SETTLEMENT_TYPE_API.NON_RECURRING
    )
      return t(I18N_COLLECTION_FORM.CUSTOM);
    return t(I18N_COLLECTION_FORM.SINGLE);
  };
  const recurringGridData = useMemo(() => {
    if (
      promiseToPayDetailType !== SETTLEMENT_TYPE.RECURRING &&
      promiseToPayDetailType !== SETTLEMENT_TYPE_API.RECURRING
    )
      return [];

    return genRecurringPromisesData(
      promiseToPayDetailData as PromiseToPayRecurringResponse
    );
  }, [promiseToPayDetailData, promiseToPayDetailType]);

  const renderSinglePromiseToPayView = useMemo(() => {
    return (
      (promiseToPayDetailType === SETTLEMENT_TYPE.SINGLE ||
        promiseToPayDetailType === SETTLEMENT_TYPE_API.SINGLE) && (
        <View
          id={`${storeId}-promiseToPayDetail`}
          formKey={`${storeId}-promiseToPayDetail`}
          descriptor="promiseToPayDetailSingleViews"
          value={{
            ...prepareDataView(
              promiseToPayDetailData,
              promiseToPayDetailType as SettlementTypeApi
            )
          }}
        />
      )
    );
  }, [storeId, promiseToPayDetailData, promiseToPayDetailType]);

  const renderRecurringPromiseToPayView = useMemo(() => {
    return (
      (promiseToPayDetailType === SETTLEMENT_TYPE.RECURRING ||
        promiseToPayDetailType === SETTLEMENT_TYPE_API.RECURRING) && (
        <>
          <View
            id={`${storeId}-promiseToPayDetail`}
            formKey={`${storeId}-promiseToPayDetail`}
            descriptor="promiseToPayDetailRecurringViews"
            value={{
              ...prepareDataView(promiseToPayDetailData, promiseToPayDetailType)
            }}
          />
          <div className="mt-16">
            <RecurringPromisesGrid
              data={recurringGridData as RecurringGridItem[]}
            />
          </div>
        </>
      )
    );
  }, [
    storeId,
    recurringGridData,
    promiseToPayDetailData,
    promiseToPayDetailType
  ]);

  const renderMultiplePromiseToPayView = useMemo(() => {
    return (
      (promiseToPayDetailType === SETTLEMENT_TYPE.NON_RECURRING ||
        promiseToPayDetailType === SETTLEMENT_TYPE_API.NON_RECURRING) &&
      promiseToPayDetailData &&
      isArray(promiseToPayDetailData) &&
      promiseToPayDetailData.map((viewInfo: any, idx) => {
        return (
          <React.Fragment key={`${idx}`}>
            {idx > 0 && <div className="divider-dashed my-16" />}
            <View
              id={`${storeId}-promiseToPayDetail-${idx}`}
              formKey={`${storeId}-promiseToPayDetail-${idx}`}
              descriptor="promiseToPayDetailSingleViews"
              value={{
                ...prepareDataView(viewInfo, promiseToPayDetailType)
              }}
            />
          </React.Fragment>
        );
      })
    );
  }, [storeId, promiseToPayDetailData, promiseToPayDetailType]);

  const renderPromiseToPayView = useCallback(() => {
    if (promiseToPayDetailType === SETTLEMENT_TYPE_API.SINGLE) {
      return renderSinglePromiseToPayView;
    }

    if (promiseToPayDetailType === SETTLEMENT_TYPE_API.RECURRING) {
      return renderRecurringPromiseToPayView;
    }

    if (promiseToPayDetailType === SETTLEMENT_TYPE_API.NON_RECURRING) {
      return renderMultiplePromiseToPayView;
    }
  }, [
    promiseToPayDetailType,
    renderSinglePromiseToPayView,
    renderRecurringPromiseToPayView,
    renderMultiplePromiseToPayView
  ]);

  return (
    <div>
      <div className="divider-dashed my-24" />
      <h6 className="color-grey mt-8">{t('txt_promise_to_pay')}</h6>
      <div className="my-16">
        <span className="form-group-static__label">{t('txt_type')}</span>
        <div className="form-group-static__text">
          {generatePromiseToPayType()}
        </div>
      </div>
      {renderPromiseToPayView()}
    </div>
  );
};

export default PromiseCallResults;
