import { AccountDetailProvider } from 'app/hooks';
// helpers
import {
  accEValue,
  convertToExpectedKeys,
  renderMockStore,
  storeId
} from 'app/test-utils';
import React from 'react';
// components
import PromiseCallResults from './PromiseCallResults';

const initialState: Partial<RootState> = {
  settlement: {
    [storeId]: {
      //   data: {
      //     settlementType: 'NonRecurring'
      //   }
    }
  }
};

const renderWrapper = (
  initialState: Partial<RootState>,
  { promiseToPayDetailType, promiseToPayDetailData }: any
) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <PromiseCallResults
        promiseToPayDetailType={promiseToPayDetailType}
        promiseToPayDetailData={promiseToPayDetailData}
      />
    </AccountDetailProvider>,
    { initialState }
  );
};

const nonRecurringData = [
  {
    amount: '1',
    paymentDate: '01-01-2021',
    method: {
      value: 'bank',
      description: 'Bank'
    }
  },
  {
    amount: '2',
    paymentDate: '02-02-2021',
    method: {
      value: 'bank2',
      description: 'Bank2'
    }
  }
];

const renderCases = [
  {
    caseName: `Type PromiseToPaySingleDetail`,
    props: {
      promiseToPayDetailType: 'Single Promise',
      promiseToPayDetailData: nonRecurringData[0]
    }
  },
  {
    caseName: `Type PromiseToPayNonRecurring`,
    props: {
      promiseToPayDetailType: 'Non-Recurring Promise',
      promiseToPayDetailData: nonRecurringData
    }
  },
  {
    caseName: `Type PromiseToPayRecurring`,
    props: {
      promiseToPayDetailType: 'Recurring Promise',
      promiseToPayDetailData: {
        count: 'count',
        interval: {
          value: 'interval',
          description: 'Interval'
        },
        dataList: nonRecurringData.map((item: any) => {
          const renamedKeyItem = convertToExpectedKeys({
            payload: item,
            mappingRule: {
              method: 'paymentMethod'
            }
          });

          return {
            ...renamedKeyItem,
            paymentMethod: item.method.description
          };
        })
      }
    }
  },
  {
    caseName: `Type undefined`,
    props: {
      promiseToPayDetailType: undefined,
      promiseToPayDetailData: []
    }
  }
];

describe.each(renderCases)('Render UI', ({ caseName, props }) => {
  it(caseName, () => {
    renderWrapper(initialState, props);
  });
});
