// I18 CONSTANTS
export const I18N_MEMOS = {
  EDIT: 'txt_memos_edit',
  COLLAPSE: 'txt_memos_collapse',
  EXPAND: 'txt_memos_expand',
  MEMOS: 'txt_memo',
  RESET_TO_DEFAULT: 'txt_reset_to_default',
  SORT_ORDER: 'txt_memos_sort_order',
  KEYWORD: 'txt_memos_keyword',
  SEQUENCE_NUMBER: 'txt_memos_sequence_number',
  LOADING_MORE: 'txt_memos_loading_more',
  END_OF_MEMOS: 'txt_memos_end_of_memos',
  CONFIRM_DELETE: 'txt_memos_confirm_delete',
  CONFIRM_DELETE_MESSAGE: 'txt_memos_confirm_delete_message',
  DATE_AND_TIME: 'txt_memos_date_and_time',
  NO_RESULTS_FOUND: 'txt_memos_no_results_found',
  NO_RESULTS_DISPLAY: 'txt_memos_no_memos_display',
  CHRONICLE: 'txt_memos_chronicle',
  VALIDATE_DATE_RANGE: 'txt_memos_validate_date_range',
  CIS: 'txt_memos_cis_memo',
  WARNING_TEXT_MESSAGE: 'txt_memos_text_warning_message',

  // TOAST
  MEMOS_DELETE_SUCCESS: 'txt_memos_delete_success',
  MEMOS_ADD_SUCCESS: 'txt_memos_add_success',
  MEMOS_UPDATE_SUCCESS: 'txt_memos_update_success',

  MEMOS_DELETE_FAILURE: 'txt_memos_delete_failure',
  MEMOS_ADD_FAILURE: 'txt_memos_add_failure',
  MEMOS_UPDATE_FAILURE: 'txt_memos_update_failure'
};

export const CALLER_AUTHENTICATION_ACTIONS = {
  VERIFY_SUCCESS: 'VERIFY_SUCCESS',
  FAILED_TO_VERIFY: 'FAILED_TO_VERIFY',
  BYPASS_AUTHENTICATION: 'BYPASS_AUTHENTICATION'
};

export const ACCOUNT_DETAILS_ACTIONS = {
  UPDATE_EXTERNAL_ACC_STATUS: 'UPDATE_EXTERNAL_ACC_STATUS',
  UPDATE_EXTERNAL_REASON_CD: 'UPDATE_EXTERNAL_REASON_CD'
};

export const LETTER_ACTIONS = {
  SEND_LETTER_WITHOUT_VARIABLES: 'SEND_LETTER_WITHOUT_VARIABLES',
  SEND_LETTER_WITH_VARIABLES: 'SEND_LETTER_WITH_VARIABLES',
  DELETE_PENDING_LETTER: 'DELETE_PENDING_LETTER'
};

export const ACH_ACTION = {
  SUBMIT: 'SUBMIT',
  VERIFIED_SAVING: 'VERIFIED_SAVING',
  VERIFIED_CHECKING: 'VERIFIED_CHECKING',
  ADD_CHECKING: 'ADD_CHECKING',
  ADD_SAVING: 'ADD_SAVING',
  UPDATE_CHECKING: 'UPDATE_CHECKING',
  UPDATE_SAVING: 'UPDATE_SAVING'
};

export const CARDHOLDER_MAINTENANCE_ACTIONS = {
  ADD_EXPANDED_ADDRESS: 'ADD_EXPANDED_ADDRESS',
  EDIT_EXPANDED_ADDRESS: 'EDIT_EXPANDED_ADDRESS',
  DELETE_EXPANDED_ADDRESS: 'DELETE_EXPANDED_ADDRESS',
  ADD_STANDARD_ADDRESS: 'ADD_STANDARD_ADDRESS',
  EDIT_STANDARD_ADDRESS: 'EDIT_STANDARD_ADDRESS',
  DELETE_STANDARD_ADDRESS: 'DELETE_STANDARD_ADDRESS',
  UPDATE_PHONE_NUMBER: 'UPDATE_PHONE_NUMBER',
  UPDATE_EMAIL_INFORMATION: 'UPDATE_EMAIL_INFORMATION',
  UPDATE_HOME_EMAIL: 'UPDATE_HOME_EMAIL',
  UPDATE_WORK_EMAIL: 'UPDATE_WORK_EMAIL'
};

export const MONETARY_ADJUSTMENT_ACTIONS = {
  ADJUST_TRANSACTION: 'ADJUST_TRANSACTION',
  ADJUST_FAIL_TRANSACTION: 'ADJUST_FAIL_TRANSACTION',
  APPROVE_ADJUSTMENT: 'APPROVE_ADJUSTMENT',
  REJECT_ADJUSTMENT: 'REJECT_ADJUSTMENT'
};

export const SCHEDULE_ACTIONS = {
  ADD_CARD: 'ADD_CARD',

  VERIFIED_CARD: 'VERIFIED_CARD',
  VERIFIED_BANK: 'VERIFIED_BANK',
  ADD_BANK: 'ADD_BANK',

  DEACTIVATED: 'DEACTIVATED',
  DEACTIVATED_BANK: 'DEACTIVATED_BANK',

  SUBMIT_BANK: 'SUBMIT_BANK',
  SUBMIT_CARD: 'SUBMIT_CARD'
};

export const GENERAL_INFORMATION = {
  UPDATE_NEXT_WORK_DATE: 'NEXT_WORK_DATE',
  UPDATE_MOVE_TO_QUEUE: 'MOVE_TO_QUEUE',
  UPDATE_DELINQUENCY_REASON: 'DELINQUENCY_REASON'
};

export const LONG_TERM_MEDICAL_ACTIONS = {
  ADD_LONG_TERM_MEDICAL: 'ADD_LONG_TERM_MEDICAL',
  UPDATE_LONG_TERM_MEDICAL: 'UPDATE_LONG_TERM_MEDICAL',
  DELETE_LONG_TERM_MEDICAL: 'DELETE_LONG_TERM_MEDICAL'
};
export const INCARCERATION_ACTIONS = {
  ADD_INCARCERATION: 'ADD_INCARCERATION',
  UPDATE_INCARCERATION: 'UPDATE_INCARCERATION',
  DELETE_INCARCERATION: 'DELETE_INCARCERATION'
};

export const ACTIONS_CODE = {
  LONG_TERM_MEDICAL: 'ME',
  INCARCERATION: 'FD'
};
export const ACTIONS_TYPE = {
  ADD: 'ADD',
  UPDATE: 'UPDATE',
  DELETE: 'DELETE'
};
