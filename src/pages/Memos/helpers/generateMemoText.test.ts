import {
  ACCOUNT_DETAILS_ACTIONS,
  ACH_ACTION,
  CALLER_AUTHENTICATION_ACTIONS,
  CARDHOLDER_MAINTENANCE_ACTIONS,
  GENERAL_INFORMATION,
  LETTER_ACTIONS,
  MONETARY_ADJUSTMENT_ACTIONS,
  SCHEDULE_ACTIONS
} from '../constants';
import { storeId, accEValue } from 'app/test-utils';
import generateMemoText from './generateMemoText';
import { formatMaskValueToPost } from 'pages/MakePayment/SchedulePayment/helpers';
import { formatCommon } from 'app/helpers';

const data = {
  storeId,
  accEValue,
  cspa: {
    clientNumber: 'string',
    system: 'string',
    prin: 'string',
    agen: 'string'
  },
  chronicleKey: 'chronicleKey',
  actionCode: 'actionCode',
  actionType: 'actionType',

  value: 'value',
  cardholderMaintenance: {
    cardholderContactedNameAuth: 'cardholderContactedNameAuth',
    addValue: 'addValue',
    deleteValue: 'deleteValue',
    contentEdit: 'contentEdit',
    role: 'role',
    cardholderContactedName: 'cardholderContactedName',
    existingValue: 'existingValue',
    newValue: 'newValue'
  },
  operatorId: 'operatorId',
  cardholderContactedName: 'cardholderContactedName',
  existingValue: 'existingValue',
  newValue: 'newValue',
  letterId: 'letterId',
  letterDescription: 'letterDescription',
  dollarAmount1: 'dollarAmount1',
  dollarAmount2: 'dollarAmount2',
  batchCd: 'batchCd',
  adjTranCd: 'adjTranCd',
  reason: 'reason',
  maskedAccNo: 'maskedAccNo',
  hostCdFromTable: 'hostCdFromTable',
  timeACH: 'timeACH',
  acctACH: 'acctACH',
  routingNumberACH: 'routingNumberACH',
  checkingACH: 'checkingACH',
  bankNameACH: 'bankNameACH',
  savingACH: 'savingACH',
  paymentAmtACH: 'paymentAmtACH',
  paypointACH: 'paypointACH',
  makePayment: {
    expirationDate: 'expirationDate',
    cardNumber: 'cardNumber',
    paymentMedium: 'paymentMedium',

    bankNumber: 'bankNumber',
    paymentMethod: 'paymentMethod',

    registrationId: 'registrationId',
    scheduledDate: 'scheduledDate',
    routingNumber: 'routingNumber',
    bankName: 'bankName',
    recurringId: 'recurringId',
    channel: 'channel',
    feeAmount: 'feeAmount',
    scheduledStart: 'scheduledStart',
    scheduledEnd: 'scheduledEnd',
    paymentAmt: 'paymentAmt',
    startDate: 'startDate',
    endDate: 'endDate',
    paypoint: 'paypoint',
    confirmationNumber: 'confirmationNumber',
    maskedNumber: 'maskedNumber',
    typeMethod: 'typeMethod',
    methodAcc: 'methodAcc'
  },
  generalInformation: {
    cardholderContactedNameAuth: 'cardholderContactedNameAuth',
    nextWorkDate: 'nextWorkDate',
    moveToQueueCode: 'moveToQueueCode',
    delinquencyReason: 'delinquencyReason'
  }
};

describe('generateMemoText', () => {
  it('ACH_ACTION.VERIFIED_CHECKING', () => {
    const today = formatCommon(new Date()).time.date;
    const result = generateMemoText(ACH_ACTION.VERIFIED_CHECKING, data);
    expect(result).toContain('Pay by Phone Bank info verified on');
    expect(result).toContain(
      `Pay by Phone Bank info verified on ${today}. Bank Name: bankName, Routing #: routingNumber, Checking acct #: ****mber, Payment Channel = CX Agent.`
    );
  });

  it('ACH_ACTION.VERIFIED_SAVING', () => {
    const today = formatCommon(new Date()).time.date;
    const result = generateMemoText(ACH_ACTION.VERIFIED_SAVING, data);
    expect(result).toContain('Pay by Phone Bank info verified on');
    expect(result).toContain(
      `Pay by Phone Bank info verified on ${today}. Bank Name: bankName, Routing #: routingNumber, Savings acct #: ****mber, Payment Channel = CX Agent.`
    );
  });

  it('SCHEDULE_ACTIONS.VERIFIED_CARD', () => {
    const today = formatCommon(new Date()).time.date;
    const result = generateMemoText(SCHEDULE_ACTIONS.VERIFIED_CARD, data);
    expect(result).toContain('Pay by Phone Card info verified on');
    expect(result).toContain(
      `Pay by Phone Card info verified on ${today}. Credit Card #: ****mber, Exp Date #: scheduledDate , Payment Medium #: paymentMedium, Payment Channel = CX Agent.`
    );
  });

  it('GENERAL_INFORMATION.UPDATE_NEXT_WORK_DATE', () => {
    const result = generateMemoText(
      GENERAL_INFORMATION.UPDATE_NEXT_WORK_DATE,
      data
    );
    expect(result).toEqual(
      'cardholderContactedNameAuth updated Next Work Date as nextWorkDate'
    );
  });

  it('GENERAL_INFORMATION.UPDATE_MOVE_TO_QUEUE', () => {
    const result = generateMemoText(
      GENERAL_INFORMATION.UPDATE_MOVE_TO_QUEUE,
      data
    );
    expect(result).toEqual(
      'cardholderContactedNameAuth updated Move To Queue as moveToQueueCode'
    );
  });

  it('GENERAL_INFORMATION.UPDATE_DELINQUENCY_REASON', () => {
    const result = generateMemoText(
      GENERAL_INFORMATION.UPDATE_DELINQUENCY_REASON,
      data
    );
    expect(result).toEqual(
      'cardholderContactedNameAuth updated Delinquency Reason as delinquencyReason'
    );
  });

  it('CALLER_AUTHENTICATION_ACTIONS.VERIFY_SUCCESS', () => {
    const result = generateMemoText(
      CALLER_AUTHENTICATION_ACTIONS.VERIFY_SUCCESS,
      data
    );
    expect(result).toEqual(
      `Verification SUCCESSFUL for ${data?.cardholderContactedName} for collections account access by ${data?.operatorId}`
    );
  });

  it('CALLER_AUTHENTICATION_ACTIONS.FAILED_TO_VERIFY', () => {
    const result = generateMemoText(
      CALLER_AUTHENTICATION_ACTIONS.FAILED_TO_VERIFY,
      data
    );
    expect(result).toEqual(
      ` Verification FAILED for ${data?.cardholderContactedName} for collections account access by ${data?.operatorId}`
    );
  });

  it('CALLER_AUTHENTICATION_ACTIONS.BYPASS_AUTHENTICATION', () => {
    const result = generateMemoText(
      CALLER_AUTHENTICATION_ACTIONS.BYPASS_AUTHENTICATION,
      data
    );
    expect(result).toEqual(
      `Verification BYPASSED for collections account access by ${data?.operatorId}`
    );
  });

  // ACCOUNT_DETAILS
  it('ACCOUNT_DETAILS_ACTIONS.UPDATE_EXTERNAL_ACC_STATUS', () => {
    const result = generateMemoText(
      ACCOUNT_DETAILS_ACTIONS.UPDATE_EXTERNAL_ACC_STATUS,
      data
    );
    expect(result).toEqual(
      `EXT STAT CHANGED FROM ${data?.existingValue} TO ${data?.newValue}`
    );
  });

  it('ACCOUNT_DETAILS_ACTIONS.UPDATE_EXTERNAL_REASON_CD', () => {
    const result = generateMemoText(
      ACCOUNT_DETAILS_ACTIONS.UPDATE_EXTERNAL_REASON_CD,
      data
    );
    expect(result).toEqual(
      `EXT RSN CODE CHANGED FROM ${data?.existingValue} TO ${data?.newValue}`
    );
  });

  // LETTERS
  it('LETTER_ACTIONS.SEND_LETTER_WITHOUT_VARIABLES', () => {
    const result = generateMemoText(
      LETTER_ACTIONS.SEND_LETTER_WITHOUT_VARIABLES,
      data
    );
    expect(result).toBeTruthy();
  });

  it('LETTER_ACTIONS.SEND_LETTER_WITH_VARIABLES', () => {
    const result = generateMemoText(
      LETTER_ACTIONS.SEND_LETTER_WITH_VARIABLES,
      data
    );
    expect(result).toBeTruthy();
  });

  it('LETTER_ACTIONS.DELETE_PENDING_LETTER', () => {
    const result = generateMemoText(LETTER_ACTIONS.DELETE_PENDING_LETTER, data);
    expect(result).toBeTruthy();
  });

  // CARDHOLDER_MAINTENANCE
  it('CARDHOLDER_MAINTENANCE_ACTIONS.ADD_EXPANDED_ADDRESS', () => {
    const result = generateMemoText(
      CARDHOLDER_MAINTENANCE_ACTIONS.ADD_EXPANDED_ADDRESS,
      data
    );
    expect(result).toEqual(
      `Changes made to ${data?.cardholderMaintenance?.role} ${data?.cardholderMaintenance?.cardholderContactedName}: ${data?.cardholderMaintenance?.addValue} by ${data?.operatorId}`
    );
  });

  it('CARDHOLDER_MAINTENANCE_ACTIONS.EDIT_EXPANDED_ADDRESS', () => {
    const result = generateMemoText(
      CARDHOLDER_MAINTENANCE_ACTIONS.EDIT_EXPANDED_ADDRESS,
      data
    );
    expect(result).toBeTruthy();
  });

  it('CARDHOLDER_MAINTENANCE_ACTIONS.DELETE_EXPANDED_ADDRESS', () => {
    const result = generateMemoText(
      CARDHOLDER_MAINTENANCE_ACTIONS.DELETE_EXPANDED_ADDRESS,
      data
    );
    expect(result).toEqual(
      `${data?.cardholderMaintenance?.cardholderContactedName} requested to delete current expanded address ${data?.cardholderMaintenance?.role} ${data?.cardholderMaintenance?.cardholderContactedName} ${data?.cardholderMaintenance?.deleteValue} by ${data?.operatorId}`
    );
  });

  it('CARDHOLDER_MAINTENANCE_ACTIONS.ADD_STANDARD_ADDRESS', () => {
    const result = generateMemoText(
      CARDHOLDER_MAINTENANCE_ACTIONS.ADD_STANDARD_ADDRESS,
      data
    );
    expect(result).toBeTruthy();
  });

  it('CARDHOLDER_MAINTENANCE_ACTIONS.EDIT_STANDARD_ADDRESS', () => {
    const result = generateMemoText(
      CARDHOLDER_MAINTENANCE_ACTIONS.EDIT_STANDARD_ADDRESS,
      data
    );
    expect(result).toBeTruthy();
  });

  it('CARDHOLDER_MAINTENANCE_ACTIONS.DELETE_STANDARD_ADDRESS', () => {
    const result = generateMemoText(
      CARDHOLDER_MAINTENANCE_ACTIONS.DELETE_STANDARD_ADDRESS,
      data
    );
    expect(result).toEqual(
      `${data?.cardholderMaintenance?.cardholderContactedName} requested to delete current standard address ${data?.cardholderMaintenance?.existingValue} by ${data?.operatorId}`
    );
  });

  it('CARDHOLDER_MAINTENANCE_ACTIONS.UPDATE_PHONE_NUMBER', () => {
    const result = generateMemoText(
      CARDHOLDER_MAINTENANCE_ACTIONS.UPDATE_PHONE_NUMBER,
      data
    );
    expect(result).toBeTruthy();
  });

  it('CARDHOLDER_MAINTENANCE_ACTIONS.UPDATE_EMAIL_INFORMATION', () => {
    const result = generateMemoText(
      CARDHOLDER_MAINTENANCE_ACTIONS.UPDATE_EMAIL_INFORMATION,
      data
    );
    expect(result).toBeTruthy();
  });

  it('MONETARY_ADJUSTMENT_ACTIONS.ADJUST_TRANSACTION', () => {
    const result = generateMemoText(
      MONETARY_ADJUSTMENT_ACTIONS.ADJUST_TRANSACTION,
      data
    );
    expect(result).toBeTruthy();
  });
  it('MONETARY_ADJUSTMENT_ACTIONS.ADJUST_FAIL_TRANSACTION', () => {
    const result = generateMemoText(
      MONETARY_ADJUSTMENT_ACTIONS.ADJUST_FAIL_TRANSACTION,
      data
    );
    expect(result).toBeTruthy();
  });
  it('MONETARY_ADJUSTMENT_ACTIONS.APPROVE_ADJUSTMENT', () => {
    const result = generateMemoText(
      MONETARY_ADJUSTMENT_ACTIONS.APPROVE_ADJUSTMENT,
      data
    );
    expect(result).toEqual(
      `${data?.operatorId} approved an adjustment on Acct ${data?.maskedAccNo} initiated by ${data?.hostCdFromTable} for amount \$${data?.dollarAmount1} Batch code ${data?.batchCd} Tran Code ${data?.adjTranCd}`
    );
  });

  it('MONETARY_ADJUSTMENT_ACTIONS.REJECT_ADJUSTMENT', () => {
    const result = generateMemoText(
      MONETARY_ADJUSTMENT_ACTIONS.REJECT_ADJUSTMENT,
      data
    );
    expect(result).toEqual(
      `${data?.operatorId} rejected an adjustment on Acct ${data?.maskedAccNo} initiated by ${data?.hostCdFromTable} for amount \$${data?.dollarAmount1} Batch code ${data?.batchCd} Tran Code ${data?.adjTranCd}`
    );
  });
  it('SCHEDULE_ACTIONS.VERIFIED_BANK', () => {
    const result = generateMemoText(SCHEDULE_ACTIONS.VERIFIED_BANK, data);
    const today = formatCommon(new Date()).time.date;

    expect(result).toEqual(
      `Pay by Phone Bank info verified on ${today}. Bank Name: ${
        data.makePayment?.bankName
      }, Routing #: ${
        data.makePayment?.routingNumber
      }, Acct #: ${formatMaskValueToPost(
        data.makePayment?.bankNumber
      )}, Payment Medium #: ${
        data.makePayment?.paymentMedium
      }, Payment Channel = CX Agent.`
    );
  });

  it('SCHEDULE_ACTIONS.DEACTIVATED', () => {
    const result = generateMemoText(SCHEDULE_ACTIONS.DEACTIVATED, data);
    expect(result).toBeTruthy();
  });

  it('SCHEDULE_ACTIONS.DEACTIVATED', () => {
    const result = generateMemoText(SCHEDULE_ACTIONS.DEACTIVATED_BANK, data);
    expect(result).toBeTruthy();
  });

  it('SCHEDULE_ACTIONS.SUBMIT_BANK', () => {
    const result = generateMemoText(SCHEDULE_ACTIONS.SUBMIT_BANK, data);
    expect(result).toBeTruthy();
  });

  it('SCHEDULE_ACTIONS.SUBMIT_CARD', () => {
    const result = generateMemoText(SCHEDULE_ACTIONS.SUBMIT_CARD, data);
    expect(result).toBeTruthy();
  });

  it('ACH_ACTION.ADD_CHECKING', () => {
    const result = generateMemoText(ACH_ACTION.ADD_CHECKING, data);
    expect(result).toBeTruthy();
  });

  it('ACH_ACTION.UPDATE_CHECKING', () => {
    const result = generateMemoText(ACH_ACTION.UPDATE_CHECKING, data);
    expect(result).toEqual(
      `Pay by Phone bank info updated on ${data?.timeACH}. Routing #: ${
        data?.routingNumberACH
      }, Checking acct #: ${formatMaskValueToPost(
        data?.checkingACH
      )}, Bank name: ${data?.bankNameACH}, Payment Channel = CX Agent.`
    );
  });

  it('ACH_ACTION.ADD_SAVING', () => {
    const result = generateMemoText(ACH_ACTION.ADD_SAVING, data);
    expect(result).toBeTruthy();
  });

  it('ACH_ACTION.UPDATE_SAVING', () => {
    const result = generateMemoText(ACH_ACTION.UPDATE_SAVING, data);
    expect(result).toEqual(
      `Pay by Phone bank info updated on ${data?.timeACH}. Routing #: ${
        data?.routingNumberACH
      }, Savings acct #: ${formatMaskValueToPost(
        data?.savingACH
      )}, Bank name: ${data?.bankNameACH}, Payment Channel = CX Agent.`
    );
  });

  it('ACH_ACTION.SUBMIT', () => {
    const today = formatCommon(new Date()).time.date;
    const result = generateMemoText(ACH_ACTION.SUBMIT, data);
    expect(result).toEqual(
      `Phone payment scheduled: Bank Name: ${data.bankNameACH}, Routing: #${data.routingNumberACH}, Acct: #${data.acctACH}, Payment Date: ${today}, Payment Amt: ${data.paymentAmtACH}, Payment Channel = CX Agent.`
    );
  });

  it('SCHEDULE_ACTIONS.ADD_BANK', () => {
    const result = generateMemoText(SCHEDULE_ACTIONS.ADD_BANK, data);
    expect(result).toEqual(
      `NEW PAYPOINT REGISTRATION ID ${data.makePayment?.registrationId} CREATED BY ${data?.operatorId}`
    );
  });

  it('SCHEDULE_ACTIONS.ADD_CARD', () => {
    const result = generateMemoText(SCHEDULE_ACTIONS.ADD_CARD, data);
    const today = formatCommon(new Date()).time.date;
    expect(result).toEqual(
      `Pay by Phone Card info added on ${today}. Card Number #: ${formatMaskValueToPost(
        data.makePayment?.cardNumber
      )}, Exp Date #: ${data.makePayment?.expirationDate}, Payment Medium #: ${
        data.makePayment?.paymentMedium
      }, Payment Channel = CX Agent.`
    );
  });

  it('default', () => {
    const result = generateMemoText('default', data);
    expect(result).toEqual('');
  });
});
