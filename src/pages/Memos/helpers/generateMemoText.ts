import { convertAPIDateToView, formatCommon } from 'app/helpers';
import { formatMaskValueToPost } from 'pages/MakePayment/SchedulePayment/helpers';
import {
  SCHEDULE_ACTIONS,
  ACCOUNT_DETAILS_ACTIONS,
  ACH_ACTION,
  CALLER_AUTHENTICATION_ACTIONS,
  CARDHOLDER_MAINTENANCE_ACTIONS,
  LETTER_ACTIONS,
  MONETARY_ADJUSTMENT_ACTIONS,
  GENERAL_INFORMATION
} from '../constants';
import { PostMemoData } from '../types';

const generateMemoText = (actionType: string, data: PostMemoData) => {
  const today = formatCommon(new Date()).time.date;
  const dateTime = formatCommon(new Date()).time.allDatetime;

  switch (actionType) {
    // CALLER_AUTHENTICATION
    case CALLER_AUTHENTICATION_ACTIONS.VERIFY_SUCCESS:
      return `Verification SUCCESSFUL for ${data?.cardholderContactedName} for collections account access by ${data?.operatorId}`;

    case CALLER_AUTHENTICATION_ACTIONS.FAILED_TO_VERIFY:
      return ` Verification FAILED for ${data?.cardholderContactedName} for collections account access by ${data?.operatorId}`;

    case CALLER_AUTHENTICATION_ACTIONS.BYPASS_AUTHENTICATION:
      return `Verification BYPASSED for collections account access by ${data?.operatorId}`;

    // ACCOUNT_DETAILS
    case ACCOUNT_DETAILS_ACTIONS.UPDATE_EXTERNAL_ACC_STATUS:
      return `EXT STAT CHANGED FROM ${data?.existingValue} TO ${data?.newValue}`;

    case ACCOUNT_DETAILS_ACTIONS.UPDATE_EXTERNAL_REASON_CD:
      return `EXT RSN CODE CHANGED FROM ${data?.existingValue} TO ${data?.newValue}`;

    // LETTERS
    case LETTER_ACTIONS.SEND_LETTER_WITHOUT_VARIABLES:
      return `LETTER ${data?.letterId} ${data?.letterDescription} CREATED BY ${data?.operatorId} AT ${dateTime}`;

    case LETTER_ACTIONS.SEND_LETTER_WITH_VARIABLES:
      return `LETTER ${data?.letterId} ${data?.letterDescription} CREATED BY ${data?.operatorId} AT ${dateTime}`;

    case LETTER_ACTIONS.DELETE_PENDING_LETTER:
      return `PENDING LETTER ${data?.letterId} ${data?.letterDescription} DELETED BY ${data?.operatorId} AT ${dateTime}`;

    // CARDHOLDER_MAINTENANCE
    case CARDHOLDER_MAINTENANCE_ACTIONS.ADD_EXPANDED_ADDRESS:
      return `Changes made to ${data?.cardholderMaintenance?.role} ${data?.cardholderMaintenance?.cardholderContactedName}: ${data?.cardholderMaintenance?.addValue} by ${data?.operatorId}`;

    case CARDHOLDER_MAINTENANCE_ACTIONS.EDIT_EXPANDED_ADDRESS:
      return `Changes made to ${data?.cardholderMaintenance?.role} ${data?.cardholderMaintenance?.cardholderContactedName}: ${data?.cardholderMaintenance?.contentEdit} by ${data?.operatorId}`;

    case CARDHOLDER_MAINTENANCE_ACTIONS.DELETE_EXPANDED_ADDRESS:
      return `${data?.cardholderMaintenance?.cardholderContactedName} requested to delete current expanded address ${data?.cardholderMaintenance?.role} ${data?.cardholderMaintenance?.cardholderContactedName} ${data?.cardholderMaintenance?.deleteValue} by ${data?.operatorId}`;

    case CARDHOLDER_MAINTENANCE_ACTIONS.ADD_STANDARD_ADDRESS:
      return `Changes made to ${data?.cardholderMaintenance?.role} ${data?.cardholderMaintenance?.cardholderContactedName}: ${data?.cardholderMaintenance?.addValue} by ${data?.operatorId}`;

    case CARDHOLDER_MAINTENANCE_ACTIONS.EDIT_STANDARD_ADDRESS:
      return `Changes made to ${data?.cardholderMaintenance?.role} ${data?.cardholderMaintenance?.cardholderContactedName}: ${data?.cardholderMaintenance?.contentEdit} by ${data?.operatorId}`;

    // not use
    case CARDHOLDER_MAINTENANCE_ACTIONS.DELETE_STANDARD_ADDRESS:
      return `${data?.cardholderMaintenance?.cardholderContactedName} requested to delete current standard address ${data?.cardholderMaintenance?.existingValue} by ${data?.operatorId}`;

    case CARDHOLDER_MAINTENANCE_ACTIONS.UPDATE_PHONE_NUMBER:
      return `Changes made to ${data?.cardholderMaintenance?.role} ${data?.cardholderMaintenance?.cardholderContactedName}: ${data?.cardholderMaintenance?.contentEdit} by ${data?.operatorId}`;

    case CARDHOLDER_MAINTENANCE_ACTIONS.UPDATE_EMAIL_INFORMATION:
      return `Changes made to ${data?.cardholderMaintenance?.role} ${data?.cardholderMaintenance?.cardholderContactedName}: ${data?.cardholderMaintenance?.contentEdit} by ${data?.operatorId}`;

    // MONETARY_ADJUSTMENT
    case MONETARY_ADJUSTMENT_ACTIONS.ADJUST_TRANSACTION:
      return `ADJUSTMENT PROCESSES FOR ${data?.transactionCode} TO ${
        data?.adjTranCd
      }, TRAN DATE ${convertAPIDateToView(
        data?.transactionDate as string
      )}, POSTED ${convertAPIDateToView(
        data?.postDate as string
      )}, REF #, DESCRIPTION ${data?.descriptionOfTransaction}, REASON ${
        data.reason
      }, AMOUNT ${data?.dollarAmount2}, OPER ID ${
        data.operatorId
      }, DATE/TIME ${dateTime}`;
    case MONETARY_ADJUSTMENT_ACTIONS.ADJUST_FAIL_TRANSACTION:
      return `ADJUSTMENT DECLINED FOR ${
        data?.adjTranCd
      }, TRAN DATE ${convertAPIDateToView(
        data?.transactionDate as string
      )}, POSTED ${convertAPIDateToView(
        data?.postDate as string
      )}, REF #, REASON ${data.reason}, AMOUNT ${
        data?.dollarAmount2
      }, OPER ID ${data.operatorId}, DATE/TIME ${dateTime}`;
    case MONETARY_ADJUSTMENT_ACTIONS.APPROVE_ADJUSTMENT:
      return `${data?.operatorId} approved an adjustment on Acct ${data?.maskedAccNo} initiated by ${data?.hostCdFromTable} for amount \$${data?.dollarAmount1} Batch code ${data?.batchCd} Tran Code ${data?.adjTranCd}`;

    case MONETARY_ADJUSTMENT_ACTIONS.REJECT_ADJUSTMENT:
      return `${data?.operatorId} rejected an adjustment on Acct ${data?.maskedAccNo} initiated by ${data?.hostCdFromTable} for amount \$${data?.dollarAmount1} Batch code ${data?.batchCd} Tran Code ${data?.adjTranCd}`;

    // ACH
    case ACH_ACTION.VERIFIED_CHECKING:
      return `Pay by Phone Bank info verified on ${today}. Bank Name: ${
        data.makePayment?.bankName
      }, Routing #: ${
        data.makePayment?.routingNumber
      }, Checking acct #: ${formatMaskValueToPost(
        data.makePayment?.bankNumber
      )}, Payment Channel = CX Agent.`;

    case ACH_ACTION.VERIFIED_SAVING:
      return `Pay by Phone Bank info verified on ${today}. Bank Name: ${
        data.makePayment?.bankName
      }, Routing #: ${
        data.makePayment?.routingNumber
      }, Savings acct #: ${formatMaskValueToPost(
        data.makePayment?.bankNumber
      )}, Payment Channel = CX Agent.`;

    case ACH_ACTION.ADD_CHECKING:
      return `PAY BY PHONE BANK INFO ADDED ON ${data?.timeACH}, Routing # ${
        data?.routingNumberACH
      }, Checking acct #: ${formatMaskValueToPost(
        data?.checkingACH
      )}, Payment Channel = CX Agent.`;

    case ACH_ACTION.UPDATE_CHECKING:
      return `Pay by Phone bank info updated on ${data?.timeACH}. Routing #: ${
        data?.routingNumberACH
      }, Checking acct #: ${formatMaskValueToPost(
        data?.checkingACH
      )}, Bank name: ${data?.bankNameACH}, Payment Channel = CX Agent.`;

    case ACH_ACTION.ADD_SAVING:
      return `PAY BY PHONE BANK INFO ADDED ON ${data?.timeACH}, Routing # ${
        data?.routingNumberACH
      }, Savings acct #: ${formatMaskValueToPost(
        data?.savingACH
      )}, Payment Channel = CX Agent.`;

    case ACH_ACTION.UPDATE_SAVING:
      return `Pay by Phone bank info updated on ${data?.timeACH}. Routing #: ${
        data?.routingNumberACH
      }, Savings acct #: ${formatMaskValueToPost(
        data?.savingACH
      )}, Bank name: ${data?.bankNameACH}, Payment Channel = CX Agent.`;

    case ACH_ACTION.SUBMIT:
      return `Phone payment scheduled: Bank Name: ${data.bankNameACH}, Routing: #${data.routingNumberACH}, Acct: #${data.acctACH}, Payment Date: ${today}, Payment Amt: ${data.paymentAmtACH}, Payment Channel = CX Agent.`;
  }

  // fix sonar maximum 30 switch case
  switch (actionType) {
    case SCHEDULE_ACTIONS.ADD_BANK:
      return `NEW PAYPOINT REGISTRATION ID ${data.makePayment?.registrationId} CREATED BY ${data?.operatorId}`;

    case SCHEDULE_ACTIONS.ADD_CARD:
      return `Pay by Phone Card info added on ${today}. Card Number #: ${formatMaskValueToPost(
        data.makePayment?.cardNumber
      )}, Exp Date #: ${data.makePayment?.expirationDate}, Payment Medium #: ${
        data.makePayment?.paymentMedium
      }, Payment Channel = CX Agent.`;

    case SCHEDULE_ACTIONS.VERIFIED_CARD:
      return `Pay by Phone Card info verified on ${today}. Credit Card #: ${formatMaskValueToPost(
        data.makePayment?.cardNumber
      )}, Exp Date #: ${data.makePayment?.scheduledDate} , Payment Medium #: ${
        data.makePayment?.paymentMedium
      }, Payment Channel = CX Agent.`;

    case SCHEDULE_ACTIONS.VERIFIED_BANK:
      return `Pay by Phone Bank info verified on ${today}. Bank Name: ${
        data.makePayment?.bankName
      }, Routing #: ${
        data.makePayment?.routingNumber
      }, Acct #: ${formatMaskValueToPost(
        data.makePayment?.bankNumber
      )}, Payment Medium #: ${
        data.makePayment?.paymentMedium
      }, Payment Channel = CX Agent.`;

    case SCHEDULE_ACTIONS.DEACTIVATED:
      return `Successfully Deactivated : Payment Method : ${
        data.makePayment?.paymentMethod
      }, Account no: ${formatMaskValueToPost(
        data.makePayment?.bankNumber
      )}, Registration Id: ${
        data.makePayment?.registrationId
      }, Payment Channel = CX Agent.`;

    case SCHEDULE_ACTIONS.DEACTIVATED_BANK:
      return `PAY BY PHONE BANK INFO DELETED ON ${today}, ROUTING # ${
        data?.makePayment?.routingNumber
      }, ${data.makePayment?.paymentMethod}  ACCT # ${formatMaskValueToPost(
        data.makePayment?.bankNumber
      )}, Payment Channel = CX Agent.`;

    case SCHEDULE_ACTIONS.SUBMIT_BANK:
    case SCHEDULE_ACTIONS.SUBMIT_CARD:
      return `ONE-TIME PAYMENT OF ${data.makePayment?.paymentAmt} MADE ${data.makePayment?.scheduledDate} BY ${data?.operatorId}. CONFIRMATION # ${data.makePayment?.confirmationNumber}`;

    case GENERAL_INFORMATION.UPDATE_NEXT_WORK_DATE:
      return `${data?.generalInformation?.cardholderContactedNameAuth} updated Next Work Date as ${data?.generalInformation?.nextWorkDate}`;
    case GENERAL_INFORMATION.UPDATE_MOVE_TO_QUEUE:
      return `${data?.generalInformation?.cardholderContactedNameAuth} updated Move To Queue as ${data?.generalInformation?.moveToQueueCode}`;
    case GENERAL_INFORMATION.UPDATE_DELINQUENCY_REASON:
      return `${data?.generalInformation?.cardholderContactedNameAuth} updated Delinquency Reason as ${data?.generalInformation?.delinquencyReason}`;

    default:
      return '';
  }
};

export default generateMemoText;
