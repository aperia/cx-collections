import { CSPA } from 'pages/Memos/Chronicle/types';

export enum MemoType {
  CIS = '0',
  CHRONICLE = '2'
}

export enum MemoAction {
  GET_MEMO_CONFIG = 'getMemoConfig'
}

export interface GetMemoConfigArgs {
  storeId: string;
  accountId: string;
  token?: string;
}

export interface GetMemoConfigPayload {
  memoSystemUseCode: MemoType;
}

export interface GetMemoConfigPayload {
  data?: { type?: string };
}

export interface MemoState {
  [storeId: string]: {
    configMemo?: {
      loading?: boolean;
      error?: boolean;
      data?: { type?: MemoType };
    };
  };
}

export interface GetMemoConfigProps {
  common: {
    accountId: string;
  };
  selectFields: string[];
}

export interface PostMemoData {
  storeId: string;
  accEValue: string;
  chronicleKey?: string;
  actionCode?: string;
  actionType?: string;

  // sp for post memo not storeId
  cspa: CSPA;
  value?: string;
  cardholderMaintenance?: {
    cardholderContactedNameAuth?: string;
    addValue?: string;
    deleteValue?: string;
    contentEdit?: string;
    role?: string;
    cardholderContactedName?: string;
    existingValue?: string;
    newValue?: string;
  };
  transactionCode?: string;
  descriptionOfTransaction?: string;
  postDate?: string;
  transactionDate?: string;
  operatorId?: string;
  cardholderContactedName?: string;
  existingValue?: string;
  newValue?: string;
  letterId?: string;
  letterDescription?: string;
  dollarAmount1?: string;
  dollarAmount2?: string;
  batchCd?: string;
  adjTranCd?: string;
  reason?: string;
  maskedAccNo?: string;
  hostCdFromTable?: string;
  timeACH?: string;
  acctACH?: string;
  routingNumberACH?: string;
  checkingACH?: string;
  bankNameACH?: string;
  savingACH?: string;
  paymentAmtACH?: string;
  paypointACH?: string;
  makePayment?: {
    // verify
    expirationDate?: string;
    cardNumber?: string;
    paymentMedium?: string;

    bankNumber?: string;
    paymentMethod?: string; //eCheck | CheckCard

    registrationId?: string;
    scheduledDate?: string;
    routingNumber?: string;
    bankName?: string;
    recurringId?: string;
    channel?: string;
    feeAmount?: string;
    scheduledStart?: string;
    scheduledEnd?: string;
    paymentAmt?: string;
    startDate?: string;
    endDate?: string;
    paypoint?: string;
    confirmationNumber?: string;
    maskedNumber?: string;
    typeMethod?: string;
    methodAcc?: string;
  };
  generalInformation?: {
    cardholderContactedNameAuth?: string;
    nextWorkDate: string;
    moveToQueueCode: string;
    delinquencyReason?: string;
  };
}
