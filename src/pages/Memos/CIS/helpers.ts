// Helpers
import groupBy from 'lodash.groupby';
import orderByLodash from 'lodash.orderby';
import isEqual from 'lodash.isequal';
import {
  MemoCISType,
  MEMO_FLAG,
  PREFIX_PERMANENT,
  PREFIX_PRIORITY,
  PREFIX_PRIORITY_PERMANENT
} from './constants';
// Types
import {
  IMemoCIS,
  ISort,
  OrderBy,
  SortBy,
  valueSort,
  SearchFormValues
} from './types';

const { MEMO_DATE, MEMO_SEQUENCE_NUMBER, MEMO_TYPE } = SortBy;

export const orderData = <T>(
  data: T[],
  orderBy?: OrderBy.ASC | OrderBy.DESC,
  sortBy?: valueSort
): T[] => {
  if (sortBy === MEMO_DATE) {
    return orderByLodash(
      data,
      (item: MagicKeyValue) => new Date(item['memoDate']),
      orderBy
    );
  }
  return orderByLodash(data, sortBy, orderBy);
};

export const handleDataMemoDate = (memos: IMemoCIS[], originalSort: ISort) => {
  let revertData = null;
  revertData = orderData(memos, originalSort?.orderBy, MEMO_DATE);
  return [
    {
      memos: revertData
    }
  ];
};

export const handleDataSequenceNumber = (
  memos: IMemoCIS[],
  originalSort: ISort
) => {
  let revertData = null;
  revertData = orderData(memos, originalSort.orderBy, MEMO_SEQUENCE_NUMBER);
  return [
    {
      memos: revertData
    }
  ];
};

export const handleDataWithType = (memos: IMemoCIS[], originalSort: ISort) => {
  let groupOrSortData: Record<string, IMemoCIS[]> = {};
  let objectKeys = null;
  groupOrSortData = groupBy(memos, MEMO_TYPE);
  objectKeys = Object.keys(groupOrSortData);
  // CIS Memo Type Order: Priority Permanent > Permanent > Priority > Standard
  const orderedKeys = [];
  if (objectKeys.indexOf(MemoCISType.PriorityPermanent) > -1) {
    orderedKeys.push(MemoCISType.PriorityPermanent);
  }
  if (objectKeys.indexOf(MemoCISType.Permanent) > -1) {
    orderedKeys.push(MemoCISType.Permanent);
  }
  if (objectKeys.indexOf(MemoCISType.Priority) > -1) {
    orderedKeys.push(MemoCISType.Priority);
  }
  if (objectKeys.indexOf(MemoCISType.Standard) > -1) {
    orderedKeys.push(MemoCISType.Standard);
  }

  return orderedKeys.map(key => {
    const reverseMemos = orderData(
      groupOrSortData[key],
      originalSort.orderBy,
      MEMO_SEQUENCE_NUMBER
    );
    return {
      memos: reverseMemos,
      memoType: key
    };
  });
};

export const handleSort = (memos: IMemoCIS[], originalSort: ISort) => {
  let data = null;
  if (originalSort?.sortBy?.value === MEMO_TYPE) {
    data = handleDataWithType(memos, originalSort);
  } else if (originalSort?.sortBy?.value === MEMO_SEQUENCE_NUMBER) {
    data = handleDataSequenceNumber(memos, originalSort);
  } else if (originalSort?.sortBy?.value === MEMO_DATE) {
    data = handleDataMemoDate(memos, originalSort);
  }

  return data;
};

export const handleFilter = (data: IMemoCIS[], filter: SearchFormValues) =>
  data.filter(
    item =>
      item.memoText.includes(filter?.keyword?.toUpperCase() || '') &&
      (!filter?.sequenceNumber ||
        `${item.memoSequenceNumber}` === `${filter?.sequenceNumber}`) &&
      (!filter?.type || isEqual(item.memoType, filter?.type?.value))
  );

export const handleGenerateMemoFlags = (memoType: string) => {
  switch (memoType) {
    case MemoCISType.PriorityPermanent:
      return MEMO_FLAG.PRIORITY_PERMANENT;
    case MemoCISType.Permanent:
      return MEMO_FLAG.PERMANENT;
    case MemoCISType.Priority:
      return MEMO_FLAG.PRIORITY;
    case MemoCISType.Standard:
      return MEMO_FLAG.STANDARD;
    default:
      return '';
  }
};

export const checkMemoTextHasTimestamp = (memoText: string) => {
  // Memo Text with timestamp ex: 1234  !*CONTENT
  if (memoText.length <= 5) return false;
  const characters = memoText.split('');
  const [firstCharacter, secondCharacter, thirdCharacter, fourthCharacter] =
    characters;

  return (
    !isNaN(parseInt(firstCharacter)) &&
    !isNaN(parseInt(secondCharacter)) &&
    !isNaN(parseInt(thirdCharacter)) &&
    !isNaN(parseInt(fourthCharacter))
  );
};

export const handleSplitTypes = (data: IMemoCIS[]) => {
  const newList = [...data];
  for (let i = 0; i < data.length; i++) {
    const memoText = data[i].memoText;
    const isHavingTimestamp = checkMemoTextHasTimestamp(memoText);

    let startPosition = 0;
    if (isHavingTimestamp) startPosition = 6;

    if (
      data[i].memoText.substr(startPosition, 2) === PREFIX_PRIORITY_PERMANENT
    ) {
      newList[i] = {
        ...newList[i],
        memoType: MemoCISType.PriorityPermanent,
        memoText
      };
    } else if (
      data[i].memoText.substr(startPosition, 1).includes(PREFIX_PERMANENT)
    ) {
      newList[i] = {
        ...newList[i],
        memoType: MemoCISType.Permanent,
        memoText
      };
    } else if (
      data[i].memoText.substr(startPosition, 1).includes(PREFIX_PRIORITY)
    ) {
      newList[i] = {
        ...newList[i],
        memoType: MemoCISType.Priority,
        memoText
      };
    } else {
      newList[i] = {
        ...newList[i],
        memoType: MemoCISType.Standard,
        memoText
      };
    }
  }
  return newList;
};
