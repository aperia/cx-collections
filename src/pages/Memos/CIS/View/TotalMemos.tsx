import React from 'react';

// components
import { Button } from 'app/_libraries/_dls/components';

// Selectors
import {
  selectIsFilteringCISMemo,
  selectTotalVisibleMemoCIS
} from '../_redux/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useCustomStoreIdSelector } from 'app/hooks';
import { useHandleResetSearch } from '../Search/useResetSearch';

// Constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';

const TotalMemos = () => {
  const total = useCustomStoreIdSelector<number>(selectTotalVisibleMemoCIS);
  const { t } = useTranslation();
  const handleResetSearch = useHandleResetSearch();

  const handleClearAndResetButton = () => {
    setTimeout(() => {
      handleResetSearch();
    }, 300);
  };

  const isFilteringCISMemo = useCustomStoreIdSelector<boolean>(
    selectIsFilteringCISMemo
  );

  return (
    <div className="d-flex justify-content-between align-items-center mt-24">
      <p data-testid={genAmtId('CIS-memo-list', 'total-item', 'TotalMemos')}>
        {t(total === 1 ? 'txt_total_memo' : 'txt_total_memo_plural', {
          count: total
        })}
      </p>
      {isFilteringCISMemo && (
        <Button
          className="mx-n8"
          size="sm"
          variant="outline-primary"
          onClick={handleClearAndResetButton}
          dataTestId="CIS-memo-list_clear-and-reset-btn"
        >
          {t(I18N_COMMON_TEXT.CLEAR_AND_RESET)}
        </Button>
      )}
    </div>
  );
};

export default React.memo(TotalMemos);
