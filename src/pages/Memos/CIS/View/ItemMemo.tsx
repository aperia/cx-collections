import React, { Fragment, useMemo } from 'react';

// Components
import MoreActions, { ActionValue } from 'app/components/MoreActions';

// Types
import { IMemoCIS } from '../types';

// Helpers
import { formatTimeDefault, isToday, isYesterday } from 'app/helpers';
import isFunction from 'lodash.isfunction';
import classNames from 'classnames';

// Constants
import { FormatTime } from 'app/constants/enums';

// Redux
import { useDispatch } from 'react-redux';

// Actions
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { cisMemoActions } from '../_redux/reducers';

// hooks
import { useCustomStoreId, useAccountDetail, useMemoProvider } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// i18n
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

import { I18N_MEMOS } from '../../constants';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

export interface IItemMemo {
  dataTestId?: string;
  data: IMemoCIS[];
  isMemoType: boolean;
  onEditAction?: (data: IMemoCIS) => void;
}

const ItemMemo: React.FC<IItemMemo> = ({
  data,
  onEditAction,
  isMemoType,
  dataTestId
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { isHiddenAddSection = false } = useMemoProvider();

  const { customStoreId: storeId } = useCustomStoreId();
  const { accEValue, storeId: accStoreId } = useAccountDetail();

  const renderTime = (date: string) => {
    if (isToday(date)) return t(I18N_COMMON_TEXT.TODAY);
    if (isYesterday(date)) return t(I18N_COMMON_TEXT.YESTERDAY);
    return formatTimeDefault(date, FormatTime.Date);
  };

  const actions = useMemo(() => {
    const allActions = [
      {
        label: t(I18N_COMMON_TEXT.EDIT),
        uniqueId: 'edit'
      },
      {
        label: t(I18N_COMMON_TEXT.DELETE),
        uniqueId: 'delete'
      }
    ];
    return allActions.filter(item => {
      if (item.uniqueId === 'edit') {
        return checkPermission(PERMISSIONS.EDIT_MEMO, accStoreId);
      }
      return checkPermission(PERMISSIONS.DELETE_MEMO, accStoreId);
    });
  }, [t, accStoreId]);

  const handleConfirmDeleteMemo = (memo: IMemoCIS) => () => {
    dispatch(
      cisMemoActions.triggerDeleteCISMemo({
        storeId,
        accountId: accEValue,
        memoOperatorIdentifier: memo.memoOperatorIdentifier || '',
        memoSequenceNumber: memo.memoSequenceNumber
      })
    );
  };

  const onSelectActions = (event: ActionValue, item: IMemoCIS) => {
    switch (event.uniqueId) {
      case 'delete':
        dispatch(
          confirmModalActions.open({
            title: I18N_MEMOS.CONFIRM_DELETE,
            body: I18N_MEMOS.CONFIRM_DELETE_MESSAGE,
            confirmCallback: handleConfirmDeleteMemo(item),
            confirmText: I18N_COMMON_TEXT.DELETE,
            cancelText: I18N_COMMON_TEXT.CANCEL
          })
        );
        break;
      case 'edit':
        isFunction(onEditAction) && onEditAction(item);
        break;
    }
  };

  const renderItemsMemo = () => {
    return data.map((item, index) => {
      const testId = `${item.memoSequenceNumber}-memo-id_${dataTestId || ''}`;

      return (
        <div
          key={item.memoSequenceNumber}
          className={classNames(
            'bg-light-l16 p-8 br-radius-12',
            !isMemoType && index === 0 ? 'mt-16' : 'mt-8'
          )}
        >
          <div className="d-flex align-items-center justify-content-between">
            <p
              className="fs-14 color-grey-d20 fw-500"
              data-testid={genAmtId(
                testId!,
                'memoOperatorIdentifier',
                'ItemMemo'
              )}
            >
              {item.memoOperatorIdentifier}
            </p>
            <div className="d-flex align-items-center">
              <p
                className="fs-12 color-grey"
                data-testid={genAmtId(testId!, 'memoDate', 'ItemMemo')}
              >
                {renderTime(item.memoDate || '')}
              </p>
              <div
                className={classNames('ml-8', { 'd-none': isHiddenAddSection })}
              >
                <MoreActions
                  onSelect={event => onSelectActions(event, item)}
                  actions={actions}
                  placementDropdown="bottom-end"
                  dataTestId={genAmtId(testId!, 'more-actions', 'ItemMemo')}
                />
              </div>
            </div>
          </div>
          <div className="d-flex justify-content-between fs-12 color-grey mt-2">
            <p data-testid={genAmtId(testId!, 'memoType', 'ItemMemo')}>
              {t(`txt_${item.memoType.toLowerCase()}`)}
            </p>
            <p
              data-testid={genAmtId(testId!, 'memoSequenceNumber', 'ItemMemo')}
            >
              {item.memoSequenceNumber}
            </p>
          </div>
          <p
            className="fs-14 text-break color-grey-d20 mt-8 overflow-wrap-word"
            data-testid={genAmtId(testId!, '_memoText', 'ItemMemo')}
          >
            {item.memoText}
          </p>
        </div>
      );
    });
  };

  return <Fragment>{renderItemsMemo()}</Fragment>;
};

export default React.memo(ItemMemo);
