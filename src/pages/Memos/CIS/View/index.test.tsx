import React from 'react';
import { fireEvent, screen } from '@testing-library/react';

// hooks
import { CustomStoreIdProvider } from 'app/hooks';

// component
import ViewCISMemo from '.';

// types
import { EditMemoCISModalProps } from '../Edit';
import { IItemMemo } from './ItemMemo';
import { IMemoCIS } from '../types';
import { OrderBy, SortBy } from '../types';

// utils
import {
  renderMockStore,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';

jest.mock('./ItemMemo', () => ({ onEditAction }: IItemMemo) => (
  <>
    ItemMemo
    <input
      data-testid="ItemMemo_onEditAction"
      onChange={(e: any) => onEditAction!(e.target.data)}
    />
  </>
));

jest.mock(
  '../Edit',
  () => ({ onCancel, opened, data }: EditMemoCISModalProps) => (
    <>
      EditMemoCISModal
      {opened && <p>EditMemoCISModal_opened</p>}
      <p data-testid="EditMemoCISModal_data">{JSON.stringify(data)}</p>
      <button data-testid="EditMemoCISModal_onCancel" onClick={onCancel} />
    </>
  )
);

const today = new Date();

const memos: IMemoCIS[] = [
  {
    memoSequenceNumber: '1',
    memoDate: today.toString(),
    memoIdentifier: 1,
    memoOperatorIdentifier: '1',
    memoText: 'This !* MEMO',
    memoType: '',
    type: {}
  },
  {
    memoSequenceNumber: '2',
    memoIdentifier: 1,
    memoText: 'This !* MEMO',
    memoType: '',
    type: {}
  }
];

const memosForSequenceNumber: IMemoCIS[] = [
  {
    memoSequenceNumber: '1',
    memoDate: today.toString(),
    memoIdentifier: 1,
    memoOperatorIdentifier: '1',
    memoText: 'This is MEMO',
    memoType: '',
    type: {}
  },
  {
    memoSequenceNumber: '2',
    memoIdentifier: 1,
    memoText: 'This is MEMO',
    memoType: '',
    type: {}
  }
];

const initialState: Partial<RootState> = {
  cisMemo: {
    [storeId]: {
      originalSort: {
        orderBy: OrderBy.ASC,
        sortBy: { value: SortBy.MEMO_TYPE, description: 'des' }
      },
      filter: { keyword: 'memo' },
      memos,
      startSequence: '',
      endSequence: '',
      memoCISRefData: {},
      deleteCISMemo: { loading: false }
    }
  }
};

const stateSequenceNumber: Partial<RootState> = {
  cisMemo: {
    [storeId]: {
      originalSort: {
        orderBy: OrderBy.ASC,
        sortBy: { value: SortBy.MEMO_SEQUENCE_NUMBER, description: 'des' }
      },
      filter: { keyword: 'memo' },
      memos: memosForSequenceNumber,
      startSequence: '',
      endSequence: '',
      memoCISRefData: {},
      deleteCISMemo: { loading: false }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <CustomStoreIdProvider value={{ customStoreId: storeId }}>
      <ViewCISMemo />
    </CustomStoreIdProvider>,
    { initialState }
  );
};

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper(initialState);

    expect(screen.getByText(/ItemMemo/)).toBeInTheDocument();
    expect(screen.getByText(/EditMemoCISModal/)).toBeInTheDocument();
  });
  it('Render UI > filter by sequence number', () => {
    renderWrapper(stateSequenceNumber);

    expect(screen.getByText(/ItemMemo/)).toBeInTheDocument();
    expect(screen.getByText(/EditMemoCISModal/)).toBeInTheDocument();
  });
  it('Render UI > empty state', () => {
    renderWrapper({});

    expect(screen.queryByText('ItemMemo')).toBeNull();
    expect(screen.getByText(/EditMemoCISModal/)).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('onEditAction', () => {
    const data = { memoType: 'memoType', type: {} };
    renderWrapper(initialState);

    fireEvent.change(screen.getByTestId('ItemMemo_onEditAction'), {
      target: { value: 'undefined', data }
    });

    const dataRendered = JSON.parse(
      screen.getByTestId('EditMemoCISModal_data').textContent!
    );
    expect(dataRendered).toEqual(data);
    expect(screen.getByText('EditMemoCISModal_opened')).toBeInTheDocument();
  });

  it('handleCloseEditModal', () => {
    renderWrapper(initialState);

    screen.getByTestId('EditMemoCISModal_onCancel').click();

    expect(screen.queryByText('EditMemoCISModal_opened')).toBeNull();
  });
});
