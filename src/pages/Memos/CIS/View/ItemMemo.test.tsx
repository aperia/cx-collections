import React from 'react';
// component
import ItemMemo from './ItemMemo';
// hooks
import { CustomStoreIdProvider } from 'app/hooks';

import {
  renderMockStore,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';
import { IMemoCIS } from '../types';
import { MoreActionProps } from 'app/components/MoreActions';
import { fireEvent, screen } from '@testing-library/react';

// Actions
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { cisMemoActions } from '../_redux/reducers';
import { mockActionCreator } from 'app/test-utils';
import { I18N_MEMOS } from 'pages/Memos/constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { ModalData } from 'pages/__commons/ConfirmModal/types';
import dateTime from 'date-and-time';

jest.mock(
  'app/components/MoreActions',
  () => ({ onSelect }: MoreActionProps) => (
    <input
      data-testid="MoreActions_onSelect"
      onChange={(e: any) => onSelect!(e.target.event)}
    />
  )
);

const today = new Date();
const yesterday = dateTime.addDays(today, -1);

const mockData: IMemoCIS[] = [
  {
    memoSequenceNumber: '1',
    memoDate: today.toString(),
    memoIdentifier: 1,
    memoOperatorIdentifier: '1',
    memoText: 'This is memo',
    memoType: 'Permanent',
    type: {}
  },
  {
    memoSequenceNumber: '2',
    memoIdentifier: 1,
    memoText: 'This is memo',
    memoType: 'Permanent',
    type: {}
  },
  {
    memoSequenceNumber: '3',
    memoDate: yesterday.toString(),
    memoIdentifier: 1,
    memoOperatorIdentifier: '1',
    memoText: 'This is memo',
    memoType: 'Permanent',
    type: {}
  }
];

const renderWrapper = (onEditAction?: jest.Mock) => {
  return renderMockStore(
    <CustomStoreIdProvider value={{ customStoreId: storeId }}>
      <ItemMemo
        data={mockData}
        isMemoType={false}
        onEditAction={onEditAction}
      />
    </CustomStoreIdProvider>
  );
};

const memoSpy = mockActionCreator(cisMemoActions);
const modalSpy = mockActionCreator(confirmModalActions);

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper();
  });
});

describe('Actions', () => {
  describe('onSelectActions', () => {
    it('delete', () => {
      const mockAction = modalSpy('open');
      renderWrapper();

      fireEvent.change(screen.getAllByTestId('MoreActions_onSelect')[0], {
        target: { value: 'undefined', event: { uniqueId: 'delete' } }
      });

      expect(mockAction).toBeCalledWith(
        expect.objectContaining({
          title: I18N_MEMOS.CONFIRM_DELETE,
          body: I18N_MEMOS.CONFIRM_DELETE_MESSAGE,
          confirmText: I18N_COMMON_TEXT.DELETE
        })
      );
    });

    it('edit', () => {
      const mockOnEditAction = jest.fn();
      renderWrapper(mockOnEditAction);

      fireEvent.change(screen.getAllByTestId('MoreActions_onSelect')[0], {
        target: { value: 'undefined', event: { uniqueId: 'edit' } }
      });

      expect(mockOnEditAction).toBeCalledWith(mockData[0]);
    });
  });

  describe('handleConfirmDeleteMemo', () => {
    const mockAction = memoSpy('triggerDeleteCISMemo');
    jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(({ confirmCallback }: ModalData) => {
        // trigger handleConfirmDeleteMemo
        confirmCallback!();

        return { type: 'type', payload: {} };
      });

    renderWrapper();

    fireEvent.change(screen.getAllByTestId('MoreActions_onSelect')[1], {
      target: { value: 'undefined', event: { uniqueId: 'delete' } }
    });

    expect(mockAction).toBeCalledWith({
      storeId,
      accountId: '',
      memoOperatorIdentifier: mockData[1].memoOperatorIdentifier || '',
      memoSequenceNumber: mockData[1].memoSequenceNumber
    });
  });
});
