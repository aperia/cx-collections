import React from 'react';
import dateTime from 'date-and-time';

// component
import TotalMemos from './TotalMemos';

// hooks
import { CustomStoreIdProvider } from 'app/hooks';

import {
  renderMockStore,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';
import { IMemoCIS } from '../types';
import { MoreActionProps } from 'app/components/MoreActions';
import { screen } from '@testing-library/react';

// Actions
import { OrderBy, SortBy } from '../types';

import * as searchHooks from '../Search/useResetSearch';

jest.mock(
  'app/components/MoreActions',
  () =>
    ({ onSelect }: MoreActionProps) =>
      (
        <input
          data-testid="MoreActions_onSelect"
          onChange={(e: any) => onSelect!(e.target.event)}
        />
      )
);

jest.useFakeTimers();

const today = new Date();
const yesterday = dateTime.addDays(today, -1);

const mockData: IMemoCIS[] = [
  {
    memoSequenceNumber: '1',
    memoDate: today.toString(),
    memoIdentifier: 1,
    memoOperatorIdentifier: '1',
    memoText: 'This is MEMO',
    memoType: 'Permanent',
    type: {}
  },
  {
    memoSequenceNumber: '2',
    memoIdentifier: 1,
    memoText: 'This is MEMO',
    memoType: 'Permanent',
    type: {}
  },
  {
    memoSequenceNumber: '3',
    memoDate: yesterday.toString(),
    memoIdentifier: 1,
    memoOperatorIdentifier: '1',
    memoText: 'This is MEMO',
    memoType: 'Permanent',
    type: {}
  }
];

const initialState: Partial<RootState> = {
  cisMemo: {
    [storeId]: {
      originalSort: {
        orderBy: OrderBy.ASC,
        sortBy: { value: SortBy.MEMO_SEQUENCE_NUMBER, description: 'des' }
      },
      filter: { keyword: 'memo' },
      memos: mockData,
      startSequence: '',
      endSequence: '',
      memoCISRefData: {},
      deleteCISMemo: { loading: false }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <CustomStoreIdProvider value={{ customStoreId: storeId }}>
      <TotalMemos />
    </CustomStoreIdProvider>,
    { initialState }
  );
};

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper(initialState);

    expect(screen.getAllByText('txt_total_memo_plural')[0]).toBeInTheDocument();
  });

  it('Render UI => single memo', () => {
    renderWrapper({
      cisMemo: {
        [storeId]: {
          originalSort: {
            orderBy: OrderBy.ASC,
            sortBy: { value: SortBy.MEMO_SEQUENCE_NUMBER, description: 'des' }
          },
          filter: { keyword: 'memo' },
          memos: [mockData[0]],
          startSequence: '',
          endSequence: '',
          memoCISRefData: {},
          deleteCISMemo: { loading: false }
        }
      }
    });

    expect(screen.getByText('txt_total_memo')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleClearAndResetButton', () => {
    jest.useFakeTimers();
    const mockActions = jest.fn();
    jest
      .spyOn(searchHooks, 'useHandleResetSearch')
      .mockImplementation(() => mockActions);

    renderWrapper(initialState);

    expect(mockActions).not.toBeCalled();

    screen.getByRole('button').click();

    jest.runAllTimers();

    expect(mockActions).toBeCalled();
  });
});
