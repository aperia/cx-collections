import React, { Fragment, useState } from 'react';

// Components
import ItemMemo from './ItemMemo';
import TotalMemos from './TotalMemos';
import EditMemoCISModal from '../Edit';

// Selectors
import { selectMemosCIS } from '../_redux/selectors';

// Helpers
import { isArrayHasValue } from 'app/helpers';

// Hooks
import { useCustomStoreIdSelector } from 'app/hooks';

// Types
import { IMemoCIS } from '../types';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface IMemosCIS {
  memoType?: string;
  memos: IMemoCIS[];
}

const ViewCISMemo: React.FC = () => {
  const memos = useCustomStoreIdSelector<IMemosCIS[]>(selectMemosCIS);
  const { t } = useTranslation();

  const [openedEditModal, setOpenedEditModal] = useState(false);
  const [editMemo, setEditMemo] = useState<IMemoCIS>();

  const renderMemos = () => {
    if (!isArrayHasValue(memos)) return null;
    return memos.map((item, index) => (
      <Fragment key={index}>
        {item.memoType ? (
          <h6
            className="mt-16 d-flex justify-content-center color-grey"
            data-testid={genAmtId(
              item.memoType?.toLowerCase(),
              'CIS-memo-list-item',
              'ViewCISMemo'
            )}
          >
            {t(`txt_${item.memoType.toLowerCase()}`)}
          </h6>
        ) : null}
        <ItemMemo
          dataTestId="CIS-memo-list-item"
          isMemoType={item.memoType ? true : false}
          data={item.memos}
          onEditAction={handleSelectEditAction}
        />
      </Fragment>
    ));
  };

  const handleSelectEditAction = (data: IMemoCIS) => {
    setEditMemo(data);
    setOpenedEditModal(true);
  };

  const handleCloseEditModal = () => {
    setOpenedEditModal(false);
  };

  return (
    <Fragment>
      <TotalMemos />
      {renderMemos()}
      <EditMemoCISModal
        opened={openedEditModal}
        data={editMemo}
        onCancel={handleCloseEditModal}
      />
    </Fragment>
  );
};

export default ViewCISMemo;
