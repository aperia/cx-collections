import React from 'react';
import LoadMoreCISMemo from './index';
import { screen } from '@testing-library/react';
import { renderMockStoreId, storeId } from 'app/test-utils';
import { LoadMoreProps } from 'app/components/LoadMore';
import { CustomStoreIdProvider } from 'app/hooks';
import { cisMemoActions } from '../_redux/reducers';

jest.mock('app/components/LoadMore', () => (loadMoreProps: LoadMoreProps) => {
  return <div onClick={() => loadMoreProps.onLoadMore()}>Mock Component</div>;
});

jest.mock('react-redux', () => {
  return {
    ...jest.requireActual('react-redux'),
    useDispatch: () => (x: Function) => jest.fn().mockImplementation(() => x())
  };
});

describe('LoadMoreCISMemo component', () => {
  it('should call request function', () => {
    // Arrange
    const spy = jest
      .spyOn(cisMemoActions, 'getMemoCISRequest')
      .mockImplementation(jest.fn());

    // Act
    renderMockStoreId(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <LoadMoreCISMemo />
      </CustomStoreIdProvider>,
      {
        initialState: {
          cisMemo: {
            [storeId]: {
              memos: [],
              startSequence: '1',
              endSequence: '50',
              loadMore: false,
              hasLoadMore: true,
              memoCISRefData: {},
              deleteCISMemo: {
                loading: false
              }
            }
          }
        }
      }
    );

    const ele = screen.getByText('Mock Component');
    ele.click();

    // Assertion
    expect(spy).toBeCalled();
  });

  it('should call request function > full data', () => {
    // Arrange
    const spy = jest
      .spyOn(cisMemoActions, 'getMemoCISRequest')
      .mockImplementation(jest.fn());

    // Act
    renderMockStoreId(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <LoadMoreCISMemo />
      </CustomStoreIdProvider>,
      {
        initialState: {
          cisMemo: {
            [storeId]: {
              memos: [],
              startSequence: '1',
              endSequence: '50',
              loadMore: false,
              hasLoadMore: true,
              memoCISRefData: {},
              deleteCISMemo: {
                loading: false
              },
              filter: {
                specificDate: true,
                specificDateValue: new Date('04/02/2021'),
                dateRange: true,
                dateRangeValue: {
                  start: new Date('04/02/2021'),
                  end: new Date('04/03/2021')
                }
              }
            }
          }
        }
      }
    );

    const ele = screen.getByText('Mock Component');
    ele.click();

    // Assertion
    expect(spy).toBeCalled();
  });
});
