import React, { useCallback } from 'react';

// Components
import LoadMore from 'app/components/LoadMore';

// Redux
import { useDispatch } from 'react-redux';

// Selectors
import {
  selectIsLoadingMoreMemos,
  selectIsHasLoadMore,
  selectFilterCriteria
} from '../_redux/selectors';

// Actions
import { cisMemoActions } from '../_redux/reducers';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import {
  useCustomStoreId,
  useCustomStoreIdSelector,
  useAccountDetail
} from 'app/hooks';

// Types
import { SearchFormValues } from '../types';

// Constants
import { I18N_MEMOS } from '../../constants';

const LoadMoreCISMemo: React.FC<{ hideBackToTop: boolean }> = ({
  hideBackToTop
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { customStoreId: storeId } = useCustomStoreId();
  const { accEValue } = useAccountDetail();

  const isLoadingMoreMemos = useCustomStoreIdSelector<boolean>(
    selectIsLoadingMoreMemos
  );

  const hasLoadMore = useCustomStoreIdSelector<boolean>(selectIsHasLoadMore);

  const filterCriteria =
    useCustomStoreIdSelector<SearchFormValues>(selectFilterCriteria);

  const getMemoCIS = useCallback(() => {
    const { specificDate, specificDateValue, dateRange, dateRangeValue } =
      filterCriteria;
    let startDate, endDate;
    if (specificDate) {
      startDate = endDate = specificDateValue;
    }
    if (dateRange) {
      startDate = dateRangeValue?.start;
      endDate = dateRangeValue?.end;
    }

    hasLoadMore &&
      dispatch(
        cisMemoActions.getMemoCISRequest({
          storeId,
          postData: {
            accountId: accEValue,
            startDate: startDate && startDate.toString(),
            endDate: endDate && endDate.toString()
          },
          isLoadMore: true
        })
      );
  }, [filterCriteria, hasLoadMore, dispatch, storeId, accEValue]);

  return (
    <LoadMore
      loading={isLoadingMoreMemos}
      isEnd={!hasLoadMore}
      onLoadMore={getMemoCIS}
      loadingText={t(I18N_MEMOS.LOADING_MORE)}
      hideBackToTop={hideBackToTop}
      endLoadMoreText={t(I18N_MEMOS.END_OF_MEMOS)}
      dataTestId="CIS-memo-list_load-more"
    />
  );
};

export default LoadMoreCISMemo;
