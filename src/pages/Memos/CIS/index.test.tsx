import React from 'react';

// Component
import CISMemo from '.';

// Helper
import {
  renderMockStore,
  storeId,
  accEValue,
  mockActionCreator,
  queryByClass
} from 'app/test-utils';
import * as helpers from 'app/helpers/getScrollParent';

// Hooks
import { AccountDetailProvider, CustomStoreIdProvider } from 'app/hooks';
import { fireEvent, screen, waitFor } from '@testing-library/react';

// Types
import { I18N_MEMOS } from '../constants';
import { IReducerMemo } from './types';

// Actions
import { cisMemoActions } from './_redux/reducers';
import { initialSearchValues } from './Search/Body';
import { TogglePinProps } from 'pages/__commons/InfoBar/TogglePin';
import { actionsInfoBar, InfoBarSection } from 'pages/__commons/InfoBar/_redux';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { mockScrollToFn } from 'app/test-utils/mocks/mockProperty';

import * as entitlementsHelpers from 'app/entitlements/helpers';

jest.mock('lodash.debounce', () => {
  const entries = [{ contentRect: {} }];

  return (fn: Function) => () => fn(entries);
});

jest.mock(
  'pages/__commons/InfoBar/TogglePin',
  () => (props: TogglePinProps) => {
    return <div onClick={props.onToggle}>TogglePin</div>;
  }
);

const cisMemo: IReducerMemo = {
  memos: [
    {
      memoIdentifier: 141,
      memoOperatorIdentifier: '208',
      memoType: 'Priority',
      memoSequenceNumber: '9',
      memoText:
        'hacking FTP Manors deposit connect withdrawal schemas New Technician Pizza 24/7 Operations lime Metrics mindshare withdrawal extend RAM Rustic open-source',
      memoDate: 'Thu Nov 12 2020 09:36:17 GMT+0700 (Indochina Time)',
      type: {}
    }
  ],
  filter: { ...initialSearchValues, specificDate: false },
  startSequence: '',
  endSequence: '',
  memoCISRefData: {},
  deleteCISMemo: { loading: false }
};

const initialState: Partial<RootState> = {
  cisMemo: { [storeId]: cisMemo },
  apiErrorNotification: {
    [storeId]: {
      errorDetail: {
        inMemoFlyOut: {
          response: '500',
          request: '500'
        }
      }
    }
  }
};

const renderWrapper = (
  initialState: Partial<RootState>,
  shouldRenderPinnedIcon?: boolean
) => {
  const { wrapper } = renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <CISMemo shouldRenderPinnedIcon={!!shouldRenderPinnedIcon} />
      </CustomStoreIdProvider>
    </AccountDetailProvider>,
    { initialState }
  );

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement
  };
};

const memoSpy = mockActionCreator(cisMemoActions);
const infoBarSpy = mockActionCreator(actionsInfoBar);
const apiErrorNotificationActionSpy = mockActionCreator(
  apiErrorNotificationAction
);

mockScrollToFn(jest.fn());

beforeEach(() => {
  jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
});

describe('Render', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterEach(() => {
    jest.runAllTimers();
    jest.clearAllMocks();
  });

  it('Render UI > empty memos', () => {
    waitFor(() => {
      renderWrapper({
        cisMemo: { [storeId]: { ...cisMemo, memos: [] } }
      });
    });
    expect(screen.queryByText(I18N_MEMOS.MEMOS)).toBeInTheDocument();
  });

  it('Render UI > forceRefresh', () => {
    // mock
    const mockAction = memoSpy('getMemoCISRequest');

    // render
    const state: Partial<RootState> = {
      cisMemo: { [storeId]: { ...cisMemo, isForceRefresh: true } }
    };
    waitFor(() => {
      renderWrapper(state);
    });

    // expect
    expect(mockAction).toBeCalledWith({
      storeId,
      isLoadMore: false,
      postData: { accountId: accEValue }
    });
    expect(screen.queryByText(I18N_MEMOS.MEMOS)).toBeInTheDocument();
  });
});

describe('handleClickErrorDetail', () => {
  it('render with Error', () => {
    const openApiDetailErrorModalSpy = apiErrorNotificationActionSpy(
      'openApiDetailErrorModal'
    );
    waitFor(() => {
      renderWrapper(initialState);
      const button = screen.getByText('txt_view_detail');
      button?.click();
    });

    expect(openApiDetailErrorModalSpy).toBeCalledWith({
      forSection: 'inMemoFlyOut',
      storeId
    });
  });
});

describe('Actions', () => {
  describe('handleSetPositionAfterScroll', () => {
    it('has scroll parent', async () => {
      jest.spyOn(helpers, 'getScrollParent').mockReturnValue({
        scrollTop: 100,
        scrollTo: () => undefined
      } as never);

      const { baseElement } = renderWrapper(initialState);

      fireEvent.scroll(
        queryByClass(baseElement, 'simplebar-content-wrapper')!,
        { target: { scrollY: 100 } }
      );
    });
    it('do not has scroll parent', async () => {
      jest.spyOn(helpers, 'getScrollParent').mockReturnValue(null);

      const { baseElement } = renderWrapper(initialState);

      fireEvent.scroll(
        queryByClass(baseElement, 'simplebar-content-wrapper')!,
        { target: { scrollY: 100 } }
      );
    });
  });
});

describe('Test shouldRenderPinnedIcon props', () => {
  it('should show', () => {
    const updatePinnedSpy = infoBarSpy('updatePinned');
    const updateRecoverSpy = infoBarSpy('updateRecover');
    const updateIsExpandSpy = infoBarSpy('updateIsExpand');

    waitFor(() => {
      renderWrapper(initialState, true);
    });

    const toggleElement = screen.getByText('TogglePin');
    toggleElement.click();

    expect(updatePinnedSpy).toBeCalledWith({
      storeId,
      pinned: InfoBarSection.Memos
    });
    expect(updateRecoverSpy).toBeCalledWith({
      storeId,
      recover: null
    });
    expect(updateIsExpandSpy).toBeCalledWith({
      storeId,
      isExpand: true
    });
  });

  it('pinned', () => {
    const state: Partial<RootState> = {
      cisMemo: { [storeId]: { ...cisMemo, isForceRefresh: true } },
      infoBar: {
        [storeId]: {
          pinned: InfoBarSection.Memos,
          isExpand: true,
          recover: InfoBarSection.Memos
        }
      }
    };

    const updatePinnedSpy = infoBarSpy('updatePinned');
    const updateIsExpandSpy = infoBarSpy('updateIsExpand');

    waitFor(() => {
      renderWrapper(state, true);
    });

    const toggleElement = screen.getByText('TogglePin');
    toggleElement.click();

    expect(updatePinnedSpy).toBeCalledWith({
      storeId,
      pinned: null
    });
    expect(updateIsExpandSpy).toBeCalledWith({
      storeId,
      isExpand: false
    });
  });

  it('handleClickErrorDetail', () => {
    const state: Partial<RootState> = {
      cisMemo: { [storeId]: { ...cisMemo, isForceRefresh: true } },
      infoBar: {
        [storeId]: {
          pinned: InfoBarSection.Memos,
          isExpand: true,
          recover: InfoBarSection.Memos
        }
      },
      apiErrorNotification: {
        [storeId]: {
          errorDetail: {
            inMemoFlyOut: {
              response: '500',
              request: '500'
            }
          }
        }
      }
    };
    const openApiDetailErrorModalSpy = apiErrorNotificationActionSpy(
      'openApiDetailErrorModal'
    );

    renderWrapper(state, true);

    expect(screen.getByText('txt_api_error')).toBeInTheDocument();
    const button = screen.getByText('txt_view_detail');
    button.click();

    expect(openApiDetailErrorModalSpy).toBeCalledWith({
      forSection: 'inMemoFlyOut',
      storeId
    });
  });
});
