import { MemoCISType } from './constants';
import {
  orderData,
  handleDataMemoDate,
  handleDataSequenceNumber,
  handleDataWithType,
  handleSort,
  handleFilter,
  handleGenerateMemoFlags,
  handleSplitTypes
} from './helpers';

import {
  OrderBy,
  SortBy,
  IResponseCustomMemoCIS,
  IMemoCIS,
  ISort,
  SearchFormValues
} from './types';

const dataMemoCIS: IMemoCIS[] = [
  {
    memoText: 'this -!* Memo',
    memoSequenceNumber: '2',
    memoDate: '02/01/2021',
    memoType: MemoCISType.PriorityPermanent,
    type: { text: '2' }
  },
  {
    memoText: 'this -* Memo',
    memoSequenceNumber: '1',
    memoDate: '01/01/2021',
    memoType: MemoCISType.Permanent,
    type: { text: '1' }
  },
  {
    memoText: 'this -! Memo',
    memoSequenceNumber: '3',
    memoDate: '03/01/2021',
    memoType: MemoCISType.Priority,
    type: { text: '3' }
  },
  {
    memoText: 'this Memo',
    memoSequenceNumber: '4',
    memoDate: '03/01/2021',
    memoType: MemoCISType.Standard,
    type: { text: '4' }
  }
];

const dataMemoSplitCIS: IMemoCIS[] = [
  {
    memoText: '!* Memo',
    memoSequenceNumber: '2',
    memoDate: '02/01/2021',
    memoType: MemoCISType.PriorityPermanent,
    type: { text: '2' }
  },
  {
    memoText: '* Memo',
    memoSequenceNumber: '1',
    memoDate: '01/01/2021',
    memoType: MemoCISType.Permanent,
    type: { text: '1' }
  },
  {
    memoText: '! Memo',
    memoSequenceNumber: '3',
    memoDate: '03/01/2021',
    memoType: MemoCISType.Priority,
    type: { text: '3' }
  },
  {
    memoText: 'this Memo',
    memoSequenceNumber: '4',
    memoDate: '03/01/2021',
    memoType: MemoCISType.Standard,
    type: { text: '4' }
  },
];

const expectedDataMemoCIS: IMemoCIS[] = [
  {
    memoText: 'this -* Memo',
    memoSequenceNumber: '1',
    memoDate: '01/01/2021',
    memoType: MemoCISType.Permanent,
    type: { text: '1' }
  },
  {
    memoText: 'this -!* Memo',
    memoSequenceNumber: '2',
    memoDate: '02/01/2021',
    memoType: MemoCISType.PriorityPermanent,
    type: { text: '2' }
  },
  {
    memoText: 'this -! Memo',
    memoSequenceNumber: '3',
    memoDate: '03/01/2021',
    memoType: MemoCISType.Priority,
    type: { text: '3' }
  },
  {
    memoText: 'this Memo',
    memoSequenceNumber: '4',
    memoDate: '03/01/2021',
    memoType: MemoCISType.Standard,
    type: { text: '4' }
  }
];

const expectedDataMemoCISGrouped = [
  {
    memoType: MemoCISType.PriorityPermanent,
    memos: [
      {
        memoText: 'this -!* Memo',
        memoSequenceNumber: '2',
        memoDate: '02/01/2021',
        memoType: MemoCISType.PriorityPermanent,
        type: { text: '2' }
      }
    ]
  },
  {
    memoType: MemoCISType.Permanent,
    memos: [
      {
        memoText: 'this -* Memo',
        memoSequenceNumber: '1',
        memoDate: '01/01/2021',
        memoType: MemoCISType.Permanent,
        type: { text: '1' }
      }
    ]
  },
  {
    memoType: MemoCISType.Priority,
    memos: [
      {
        memoText: 'this -! Memo',
        memoSequenceNumber: '3',
        memoDate: '03/01/2021',
        memoType: MemoCISType.Priority,
        type: { text: '3' }
      }
    ]
  },
  {
    memoType: MemoCISType.Standard,
    memos: [
      {
        memoText: 'this Memo',
        memoSequenceNumber: '4',
        memoDate: '03/01/2021',
        memoType: MemoCISType.Standard,
        type: { text: '4' }
      }
    ]
  }
];

const dataResponseCustomMemoCIS: IResponseCustomMemoCIS[] = [
  { memoText: '', memoSequenceNumber: '2', memoDate: '02/01/2021' },
  { memoText: '', memoSequenceNumber: '1', memoDate: '01/01/2021' },
  { memoText: '', memoSequenceNumber: '3', memoDate: '03/01/2021' }
];

const expectDataResponseCustomMemoCIS = [
  { memoText: '', memoSequenceNumber: '1', memoDate: '01/01/2021' },
  { memoText: '', memoSequenceNumber: '2', memoDate: '02/01/2021' },
  { memoText: '', memoSequenceNumber: '3', memoDate: '03/01/2021' }
];

const expectDataMemoSplit = [
  {
    memoText: '!* Memo',
    memoSequenceNumber: '2',
    memoDate: '02/01/2021',
    memoType: MemoCISType.PriorityPermanent,
    type: { text: '2' }
  },
  {
    memoText: '* Memo',
    memoSequenceNumber: '1',
    memoDate: '01/01/2021',
    memoType: MemoCISType.Permanent,
    type: { text: '1' }
  },
  {
    memoText: '! Memo',
    memoSequenceNumber: '3',
    memoDate: '03/01/2021',
    memoType: MemoCISType.Priority,
    type: { text: '3' }
  },
  {
    memoText: 'this Memo',
    memoSequenceNumber: '4',
    memoDate: '03/01/2021',
    memoType: MemoCISType.Standard,
    type: { text: '4' }
  },
]

describe('Memo > CIS > helpers', () => {
  describe('orderData', () => {
    it('sort by Sequence Number', () => {
      const result = orderData(
        dataResponseCustomMemoCIS,
        OrderBy.ASC,
        SortBy.MEMO_SEQUENCE_NUMBER
      );

      expect(result).toEqual(expectDataResponseCustomMemoCIS);
    });

    it('sort by Memo Date', () => {
      const result = orderData(
        dataResponseCustomMemoCIS,
        OrderBy.ASC,
        SortBy.MEMO_DATE
      );

      expect(result).toEqual(expectDataResponseCustomMemoCIS);
    });
  });

  it('handleDataMemoDate', () => {
    const originalSort: ISort = {
      sortBy: { value: SortBy.MEMO_DATE, description: 'des' }
    };
    const result = handleDataMemoDate(dataMemoCIS, originalSort);

    expect(result).toEqual([{ memos: expectedDataMemoCIS }]);
  });

  it('handleDataSequenceNumber', () => {
    const originalSort: ISort = {
      sortBy: { value: SortBy.MEMO_SEQUENCE_NUMBER, description: 'des' }
    };
    const result = handleDataSequenceNumber(dataMemoCIS, originalSort);

    expect(result).toEqual([{ memos: expectedDataMemoCIS }]);
  });

  describe('handleDataWithType', () => {
    it('with memo type', () => {
      const originalSort: ISort = {
        sortBy: { value: SortBy.MEMO_SEQUENCE_NUMBER, description: 'des' }
      };
      const result = handleDataWithType(dataMemoCIS, originalSort);

      expect(result).toEqual(expectedDataMemoCISGrouped);
    });

    it('with memo type', () => {
      const originalSort: ISort = {
        sortBy: { value: SortBy.MEMO_SEQUENCE_NUMBER, description: 'des' }
      };
      const result = handleDataWithType(dataMemoCIS, originalSort);

      expect(result).toEqual(expectedDataMemoCISGrouped);
    });

    it('without memo type', () => {
      const originalSort: ISort = {
        sortBy: { value: SortBy.MEMO_SEQUENCE_NUMBER, description: 'des' }
      };

      const newDataMemoCIS = dataMemoCIS.map(item => ({
        memoText: item.memoText,
        memoSequenceNumber: item.memoSequenceNumber,
        memoDate: item.memoDate,
        type: item.type
      })) as unknown as IMemoCIS[];

      const result = handleDataWithType(newDataMemoCIS, originalSort);

      expect(result).toEqual([]);
    });
  });

  describe('handleSort', () => {
    it('not sort', () => {
      const originalSort: ISort = {};

      const result = handleSort(dataMemoCIS, originalSort);

      expect(result).toBeNull();
    });

    it('sort by memoType', () => {
      const originalSort: ISort = {
        sortBy: { value: SortBy.MEMO_TYPE, description: 'des' }
      };

      const result = handleSort(dataMemoCIS, originalSort);

      expect(result).toEqual(handleDataWithType(dataMemoCIS, originalSort));
    });

    it('sort by memoSequenceNumber', () => {
      const originalSort: ISort = {
        sortBy: { value: SortBy.MEMO_SEQUENCE_NUMBER, description: 'des' }
      };

      const result = handleSort(dataMemoCIS, originalSort);

      expect(result).toEqual(
        handleDataSequenceNumber(dataMemoCIS, originalSort)
      );
    });

    it('sort by memoDate', () => {
      const originalSort: ISort = {
        sortBy: { value: SortBy.MEMO_DATE, description: 'des' }
      };

      const result = handleSort(dataMemoCIS, originalSort);

      expect(result).toEqual(handleDataMemoDate(dataMemoCIS, originalSort));
    });
  });

  describe('handleFilter', () => {
    it('with empty filter', () => {
      const filter: SearchFormValues = {};

      const result = handleFilter(dataMemoCIS, filter);

      expect(result).toEqual(dataMemoCIS);
    });

    it('filter by memoSequenceNumber', () => {
      const memoSequenceNumber = '1';
      const memoType = MemoCISType.Permanent;
      const filter: SearchFormValues = {
        sequenceNumber: memoSequenceNumber,
        type: { value: memoType }
      };

      const result = handleFilter(dataMemoCIS, filter);

      expect(result).toEqual(
        dataMemoCIS.filter(
          item =>
            item.memoSequenceNumber === memoSequenceNumber ||
            item.memoType === memoType
        )
      );
    });
  });

  describe('handleFilter', () => {
    it('with empty filter', () => {
      const filter: SearchFormValues = {};

      const result = handleFilter(dataMemoCIS, filter);

      expect(result).toEqual(dataMemoCIS);
    });
  });

  describe('handleGenerateMemoFlags', () => {
    it('generate memo flag by Priority Permanent', () => {
      const result = handleGenerateMemoFlags(MemoCISType.PriorityPermanent);

      expect(result).toEqual('both');
    });

    it('generate memo flag by Permanent', () => {
      const result = handleGenerateMemoFlags(MemoCISType.Permanent);

      expect(result).toEqual('system');
    });

    it('generate memo flag by Priority', () => {
      const result = handleGenerateMemoFlags(MemoCISType.Priority);

      expect(result).toEqual('priority');
    });

    it('generate memo flag by Standard', () => {
      const result = handleGenerateMemoFlags(MemoCISType.Standard);

      expect(result).toEqual('standard');
    });

    it('generate memo by empty type', () => {
      const result = handleGenerateMemoFlags('');

      expect(result).toEqual('');
    });
  });

  describe('handleSplitTypes', () => {
    it ('handleSplitTypes with multi case', () => {
      const result = handleSplitTypes(dataMemoSplitCIS)
      expect(result).toEqual(expectDataMemoSplit);
    })

    it ('handleSplitTypes with short memoText', () => {
      const data = [
        {
          memoText: '12345',
          memoSequenceNumber: '2',
          memoDate: '02/01/2021',
          memoType: MemoCISType.PriorityPermanent,
          type: { text: '2' }
        },
      ]
      const result = handleSplitTypes(data)
      expect(result).toEqual([
        {
          memoText: '12345',
          memoSequenceNumber: '2',
          memoDate: '02/01/2021',
          memoType: MemoCISType.Standard,
          type: { text: '2' }
        },
      ]);
    })

    it ('handleSplitTypes with timestamp case', () => {
      const data = [
        {
          memoText: '1234  !*CONTENT',
          memoSequenceNumber: '2',
          memoDate: '02/01/2021',
          memoType: MemoCISType.PriorityPermanent,
          type: { text: '2' }
        },
      ]
      const result = handleSplitTypes(data)
      expect(result).toEqual(data);
    })
  })
});
