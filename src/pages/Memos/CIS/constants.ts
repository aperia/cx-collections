export enum ADD_MEMO_CIS_VIEWS {
  DESCRIPTOR = 'addMemoCISFormViews',
  ID = 'add-memo-cis-form-views'
}

export enum EDIT_MEMO_CIS_VIEWS {
  DESCRIPTOR = 'editMemoCISFormViews',
  ID = 'edit-memo-cis-form-views'
}

export enum MemoCISType {
  PriorityPermanent = 'Priority Permanent',
  Permanent = 'Permanent',
  Priority = 'Priority',
  Standard = 'Standard'
}

export enum MemoCISTypeText {
  PriorityPermanent = 'txt_priority_permanent',
  Permanent = 'txt_permanent',
  Priority = 'txt_priority',
  Standard = 'txt_standard'
}

export const MEMO_CIS_TYPE_LIST = [
  {
    key: `${MemoCISType.PriorityPermanent}-key`,
    text: MemoCISTypeText.PriorityPermanent
  },
  { key: `${MemoCISType.Permanent}-key`, text: MemoCISTypeText.Permanent },
  { key: `${MemoCISType.Priority}-key`, text: MemoCISTypeText.Priority },
  { key: `${MemoCISType.Standard}-key`, text: MemoCISTypeText.Standard }
];

export const MEMO_FLAG = {
  PRIORITY_PERMANENT: 'both',
  PRIORITY: 'priority',
  PERMANENT: 'system',
  STANDARD: 'standard'
};

export const MEMO_OP_ID = 'NSA';
export const PREFIX_PRIORITY_PERMANENT = '!*';
export const PREFIX_PERMANENT = '*';
export const PREFIX_PRIORITY = '!';
