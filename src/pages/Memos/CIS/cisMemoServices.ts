import { apiService } from 'app/utils/api.service';

// Types
import {
  IMemoCIS,
  DeleteCISRequestBody,
  GetCISMemosRequestBody,
  AddCISMemoRequestBody,
  UpdateCISMemoRequestBody
} from './types';

const cisMemoServices = {
  getCISMemos(body: GetCISMemosRequestBody) {
    const url = window.appConfig?.api?.memo.getCISMemos || '';
    return apiService.post<{ cisMemos: IMemoCIS[] }>(url, body);
  },

  addCISMemo(data: AddCISMemoRequestBody, token?: string) {
    const url = window.appConfig?.api?.memo.addCISMemo || '';
    return apiService.post(
      url,
      { ...data },
      {
        headers: { Authorization: token }
      }
    );
  },

  deleteCISMemo(postData: DeleteCISRequestBody) {
    const url = window.appConfig?.api?.memo?.deleteCISMemo || '';
    return apiService.post(url, { ...postData });
  },

  updateCISMemo(body: UpdateCISMemoRequestBody) {
    const url = window.appConfig?.api?.memo.updateCISMemo || '';
    return apiService.post(url, { ...body });
  },

  getCISSource(body: MagicKeyValue) {
    const url = window.appConfig?.api?.memo.CISSource || '';
    return apiService.get(url, body);
  },

  getCISType(body: MagicKeyValue) {
    const url = window.appConfig?.api?.memo.CISType || '';
    return apiService.get(url, body);
  }
};

export default cisMemoServices;
