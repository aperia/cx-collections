import { DateRangePickerValue } from 'app/_libraries/_dls/components';

export interface AddCisMemoFormValues {
  memoContent: string;
  memoType: {
    key: string;
    text: string;
  };
}

export interface EditCisMemoFormValues {
  memoText: string;
  memoType: {
    key: string;
    text: string;
  };
}

export interface GetCISMemosRequestBody {
  common: {
    accountId: string;
  };
  selectFields: string[];
  startSequence: string;
  endSequence: string;
  memoDate?: string;
  memoDateEnd?: string;
  memoDateRelationalOperator?: string;
}

export interface AddCISMemoPayload {
  status: number;
  data: object;
}

export interface AddCISMemoArgs {
  storeId: string;
  payload: {
    accountId: string;
    memoText: string;
    memoType: string;
    memoOperatorIdentifier: string;
    memoFlags?: string;
  };
  onSuccess?: Function;
  onFailure?: Function;
  token?: string;
}

export interface DeleteCISRequestArgs {
  storeId: string;
  accountId: string;
  memoOperatorIdentifier: string;
  memoSequenceNumber: string;
}

export interface AddCISMemoRequestBody {
  common: {
    accountId: string;
  };
  behaviorIdentifier: string;
  memoText: string;
  memoCisClerkNumberCollectorIdentifier: string;
}

export interface DeleteCISRequestBody {
  common: {
    accountId: string;
  };
  memoCisClerkNumberCollectorIdentifier: string;
  memoSequenceNumber: string;
}

export interface UpdateCISMemoRequestBody {
  common: {
    accountId: string;
  };
  memoCisClerkNumberCollectorIdentifier: string;
  memoSequenceNumber: string;
  memoText: string;
}

export declare type valueSort =
  | SortBy.MEMO_SEQUENCE_NUMBER
  | SortBy.MEMO_DATE
  | SortBy.MEMO_TYPE;

export enum SortBy {
  MEMO_SEQUENCE_NUMBER = 'memoSequenceNumber',
  MEMO_DATE = 'memoDate',
  MEMO_TYPE = 'memoType'
}

export enum OrderBy {
  ASC = 'asc',
  DESC = 'desc'
}

export const defaultSort = {
  orderBy: OrderBy.DESC,
  sortBy: { value: SortBy.MEMO_TYPE, description: 'Type' }
};

export interface ISortBy {
  sortBy?: {
    value: valueSort;
    description: string;
  };
}

export interface IOrderBy {
  orderBy?: OrderBy.ASC | OrderBy.DESC;
}

export interface ISort extends ISortBy, IOrderBy, MagicKeyValue {}

export interface IResponseCustomMemoCIS {
  memoDate?: string;
  memoIdentifier?: number;
  memoOperatorIdentifier?: string;
  memoSequenceNumber: string;
  memoText: string;
}

export interface IResponseMemoCIS extends IResponseCustomMemoCIS {
  memoType: Record<string, string>;
}

export interface IMemoCIS extends IResponseCustomMemoCIS {
  memoType: string;
  type: Record<string, string>;
  memoText: string;
}

export interface IReducerMemo {
  memos: IMemoCIS[];
  hasLoadMore?: boolean;
  startSequence: string;
  endSequence: string;
  loadMore?: boolean;
  loading?: boolean;
  originalSort?: ISort;
  changingSort?: ISort;
  isForceRefresh?: boolean;
  filter?: SearchFormValues;
  memoCISRefData: {
    sourceRefData?: RefDataValue[];
    typeRefData?: RefDataValue[];
    isLoading?: boolean;
  };
  deleteCISMemo: DeleteCISMemo;
  editModalLoading?: boolean;
  isUpdateScrollPosition?: boolean;
}

export interface IInitialStateMemoCIS extends MagicKeyValue {
  [storeId: string]: IReducerMemo;
}

export interface IApplySortAndOrder {
  storeId: string;
  isOriginalToChanging: boolean;
}

export interface IChangeSortAndOrder {
  storeId: string;
  fieldName: string;
  value: string | { value: valueSort; description: string };
}

export interface FilterPayload extends SearchFormValues {
  storeId: string;
}

export interface DeleteCISMemo {
  loading: boolean;
}
export interface UpdateScrollPositionPayload {
  storeId: string;
  isUpdateScrollPosition: boolean;
}

export interface MemoCISRefDataPayload {
  sourceRefData?: Array<RefDataValue>;
  typeRefData?: Array<RefDataValue>;
}

export interface MemoCISRefDataArgs {
  storeId: string;
  postData: { accountId: string };
}

export interface MemoCISPayload {
  data: IMemoCIS[];
  startSequence?: string;
  endSequence?: string;
}

export interface GetMemoCISAfterUpdatePayload {
  data?: IResponseCustomMemoCIS;
}

export interface MemoCISArgs {
  storeId: string;
  postData: {
    accountId: string;
    startDate?: string;
    endDate?: string;
  };
  isLoadMore?: boolean;
  isSearch?: boolean;
  memoSequenceNumber?: string;
}

export interface UpdateCISMemoPayload {
  memoSequenceNumber: string;
  memoText: string;
  memoType: string;
}

export interface UpdateCISMemoArgs {
  storeId: string;
  payload: {
    accountId: string;
    memoText: string;
    memoCisClerkNumberCollectorIdentifier: string;
    memoSequenceNumber: string;
    memoType: string;
    memoFlags?: string;
  };
  onSuccess?: Function;
  onFailure?: Function;
}

export interface TriggerUpdateCISMemoArgs {
  storeId: string;
  payload: {
    accountId: string;
    memoText: string;
    memoType: string;
    memoCisClerkNumberCollectorIdentifier: string;
    memoSequenceNumber: string;
    memoFlags?: string;
  };
  onSuccess?: Function;
  onFailure?: Function;
}

export interface MemoCISSubmitDataType {
  memoType: string;
  memoText: string;
}

export interface MemoCISItemType {
  key: string;
  text: string;
}

export interface ISearchCISMemo {}

export interface IContent {
  handleTogglePopover: () => void;
}

export interface SearchFormValues {
  keyword?: string;
  sequenceNumber?: number | string;
  type?: Record<string, string> | null;
  specificDate?: boolean;
  dateRange?: boolean;
  specificDateValue?: Date;
  dateRangeValue?: DateRangePickerValue;
}
