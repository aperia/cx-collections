import { responseDefault, storeId } from 'app/test-utils';
import { NO_RESULT_CODE } from 'pages/AccountSearch/AccountSearchList/constants';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import cisMemoServices from '../cisMemoServices';
import { refreshCISMemo } from './refreshCISMemo';

let spy: jest.SpyInstance;

describe('Test refreshCISMemo async thunk', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  describe('Case fulfilled', () => {
    const store = createStore(rootReducer, {
      cisMemo: {
        [storeId]: {
          endSequence: '10',
          filter: {
            specificDate: true,
            specificDateValue: new Date()
          }
        }
      },
      mapping: {}
    });

    it('returns empty data', async () => {
      spy = jest.spyOn(cisMemoServices, 'getCISMemos').mockResolvedValue({
        ...responseDefault
      });

      const response = await refreshCISMemo({
        storeId,
        postData: { accountId: storeId }
      })(store.dispatch, store.getState, {});

      expect(response.type).toEqual('memoCIS/refreshCISMemo/fulfilled');
      expect(response.payload).toEqual({ data: [] });
    });

    it('returns empty data > response status 455', async () => {
      spy = jest.spyOn(cisMemoServices, 'getCISMemos').mockRejectedValue({
        response: {
          status: '455'
        }
      });

      const response = await refreshCISMemo({
        storeId,
        postData: { accountId: storeId }
      })(store.dispatch, store.getState, {});

      expect(response.type).toEqual('memoCIS/refreshCISMemo/fulfilled');
      expect(response.payload).toEqual({ data: [] });
    });

    it('returns empty data > response status 455 data = []', async () => {
      spy = jest
        .spyOn(cisMemoServices, 'getCISMemos')
        .mockImplementation(async () => {
          throw {
            response: { status: NO_RESULT_CODE }
          };
        });

      const response = await refreshCISMemo({
        storeId,
        postData: { accountId: storeId }
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({ data: [] });
    });
  });

  describe('Case fulfilled with date range', () => {
    const startDate = new Date();
    const endDate = new Date();
    endDate.setDate(startDate.getDate() + 1);

    const store = createStore(rootReducer, {
      cisMemo: {
        [storeId]: {
          endSequence: '10',
          filter: {
            dateRange: true,
            dateRangeValue: {
              start: startDate,
              end: endDate
            }
          }
        }
      },
      mapping: {}
    });

    it('has date range value', async () => {
      spy = jest.spyOn(cisMemoServices, 'getCISMemos').mockResolvedValue({
        ...responseDefault
      });

      const response = await refreshCISMemo({
        storeId,
        postData: { accountId: storeId }
      })(store.dispatch, store.getState, {});

      expect(response.type).toEqual('memoCIS/refreshCISMemo/fulfilled');
      expect(response.payload).toEqual({ data: [] });
    });
  });

  describe('Case rejected', () => {
    it('return status 500', async () => {
      spy = jest.spyOn(cisMemoServices, 'getCISMemos').mockRejectedValue({
        status: 500
      });

      const store = createStore(rootReducer, {});

      const response = await refreshCISMemo({
        storeId,
        postData: { accountId: storeId }
      })(store.dispatch, store.getState, {});

      expect(response.type).toEqual('memoCIS/refreshCISMemo/rejected');
      expect(response.payload).toEqual({ status: 500 });
    });
  });
});
