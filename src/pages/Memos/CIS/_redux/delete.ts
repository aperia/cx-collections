// Redux
import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';

// Types
import { IInitialStateMemoCIS, DeleteCISRequestArgs } from '../types';

// Actions
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { cisMemoActions } from './reducers';

// Services
import cisMemoServices from '../cisMemoServices';
import { batch } from 'react-redux';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

// --------------------------------------------- //
// -------------- ASYNC REQUESTS --------------- //
// --------------------------------------------- //

export const deleteCISMemoRequest = createAsyncThunk<
  unknown,
  DeleteCISRequestArgs,
  ThunkAPIConfig
>('memoCIS/deleteMemoCIS', async (args, thunkAPI) => {
  try {
    const { accountId, memoOperatorIdentifier, memoSequenceNumber } = args;

    const body = {
      common: {
        accountId
      },
      memoCisClerkNumberCollectorIdentifier: memoOperatorIdentifier,
      memoSequenceNumber
    };

    const res = await cisMemoServices.deleteCISMemo(body);

    return res;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

// --------------------------------------------- //
// ----------- TRIGGER ASYNC REQUESTS ---------- //
// --------------------------------------------- //

export const triggerDeleteCISMemo = createAsyncThunk<
  void,
  DeleteCISRequestArgs,
  ThunkAPIConfig
>('memoCIS/triggerDelete', async (args, { dispatch }) => {
  const response = await dispatch(deleteCISMemoRequest(args));

  const { storeId, accountId } = args;

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'txt_memos_delete_success'
        })
      );
      dispatch(
        cisMemoActions.refreshCISMemo({
          storeId,
          postData: {
            accountId
          }
        })
      );
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inMemoFlyOut'
        })
      );
    });
  }

  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: 'txt_memos_delete_failure'
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inMemoFlyOut',
          apiResponse: response.payload?.response
        })
      );
    });
  }
});
// ------------------------------- //
// ----------- BUILDERS ---------- //
// ------------------------------- //

export const deleteCISMemoBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateMemoCIS>
) => {
  builder
    .addCase(deleteCISMemoRequest.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        deleteCISMemo: {
          loading: true
        }
      };
    })
    .addCase(deleteCISMemoRequest.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].deleteCISMemo.loading = false;
    })
    .addCase(deleteCISMemoRequest.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].deleteCISMemo.loading = false;
    });
};
