// Redux
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Types
import {
  IInitialStateMemoCIS,
  MemoCISRefDataArgs,
  MemoCISRefDataPayload
} from '../types';

// Helpers
import sortBy from 'lodash.sortby';

// Service
import cisMemoServices from '../cisMemoServices';

export const getMemoCISRefData = createAsyncThunk<
  MemoCISRefDataPayload,
  MemoCISRefDataArgs,
  ThunkAPIConfig
>('memoCIS/getMemoCISRefData', async args => {
  const { accountId } = args.postData;

  const [sourceRefDataResponse, typeRefDataResponse] = await Promise.all([
    cisMemoServices.getCISSource({
      accountId
    }),
    cisMemoServices.getCISType({
      accountId
    })
  ]);

  const sourceRefData = sourceRefDataResponse?.data;
  const typeRefData = typeRefDataResponse?.data;
  return { sourceRefData, typeRefData };
});

export const getCISMemoRefDataBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateMemoCIS>
) => {
  builder
    .addCase(getMemoCISRefData.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        memoCISRefData: {
          isLoading: true
        }
      };
    })
    .addCase(getMemoCISRefData.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { sourceRefData, typeRefData } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        memoCISRefData: {
          ...draftState[storeId]?.memoCISRefData,
          typeRefData,
          sourceRefData: {
            ...draftState[storeId]?.memoCISRefData?.sourceRefData,
            ...sortBy(sourceRefData, [item => item.value])
          }
        }
      };
    })
    .addCase(getMemoCISRefData.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        memoCISRefData: {
          ...draftState[storeId]?.memoCISRefData,
          isLoading: false
        }
      };
    });
};
