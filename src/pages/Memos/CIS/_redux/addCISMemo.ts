// Redux
import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';

// Types
import {
  IInitialStateMemoCIS,
  AddCISMemoPayload,
  AddCISMemoArgs
} from '../types';

// Actions
import { cisMemoActions } from './reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// I18N
import { I18N_MEMOS } from '../../constants';

// Helpers
import isFunction from 'lodash.isfunction';

// Services
import cisMemoServices from '../cisMemoServices';
import { handleGenerateMemoFlags } from '../helpers';
import { batch } from 'react-redux';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export const addCISMemoRequest = createAsyncThunk<
  AddCISMemoPayload,
  AddCISMemoArgs,
  ThunkAPIConfig
>('cisMemo/addCISMemo', async (args, thunkAPI) => {
  try {
    const { accountId, memoText, memoFlags, memoOperatorIdentifier } =
      args.payload;
    const { token } = args;

    const body = {
      common: {
        accountId
      },
      behaviorIdentifier: 'CIS',
      memoText,
      memoFlags,
      memoCisClerkNumberCollectorIdentifier: memoOperatorIdentifier
    };

    const result = await cisMemoServices.addCISMemo(body, token);

    return {
      status: result.status,
      data: result.data
    };
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerAddCISMemoRequest = createAsyncThunk<
  unknown,
  AddCISMemoArgs,
  ThunkAPIConfig
>('cisMemo/triggerAdd', async (args, { dispatch }) => {
  const { storeId, onSuccess, onFailure, payload, token } = args;

  const data = { ...payload };
  data.memoFlags = handleGenerateMemoFlags(payload.memoType);

  const response = await dispatch(
    addCISMemoRequest({
      storeId,
      payload: data,
      token
    })
  );

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        cisMemoActions.refreshCISMemo({
          storeId,
          postData: {
            accountId: data.accountId
          }
        })
      );
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_MEMOS.MEMOS_ADD_SUCCESS
        })
      );
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inMemoFlyOut'
        })
      );
    });
    isFunction(onSuccess) && onSuccess();
  }

  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_MEMOS.MEMOS_ADD_FAILURE
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inMemoFlyOut',
          apiResponse: response.payload?.response
        })
      );
    });
    isFunction(onFailure) && onFailure();
  }
});

export const addCISMemoBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateMemoCIS>
) => {
  builder
    .addCase(addCISMemoRequest.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: true
      };
    })
    .addCase(addCISMemoRequest.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: false
      };
    })
    .addCase(addCISMemoRequest.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: false
      };
    });
};
