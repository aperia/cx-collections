// Redux
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Types
import {
  IInitialStateMemoCIS,
  MemoCISArgs,
  MemoCISPayload,
  IMemoCIS,
  defaultSort,
  GetCISMemosRequestBody
} from '../types';

// Constants
import { NUMBER_PAGINATION } from 'app/constants';
import { initialSearchValues } from 'pages/Memos/CIS/Search/Body';
import { DefaultSequence } from 'pages/Memos/Chronicle/constants';

// Helpers
import isEmpty from 'lodash.isempty';
import { formatTime, isArrayHasValue, mappingDataFromArray } from 'app/helpers';

// Services
import cisMemoServices from '../cisMemoServices';
import { NO_RESULT_CODE } from 'pages/AccountSearch/AccountSearchList/constants';

export const getMemoCISRequest = createAsyncThunk<
  MemoCISPayload,
  MemoCISArgs,
  ThunkAPIConfig
>('memoCIS/getMemoCIS', async (args, thunkAPI) => {
  try {
    const { storeId, isLoadMore, postData } = args;
    const { cisMemo, mapping } = thunkAPI.getState();
    const { startDate, endDate, accountId } = postData;

    const endSequence = isLoadMore
      ? calculationStartEndSequence(cisMemo[storeId].endSequence)
      : NUMBER_PAGINATION.END_SEQUENCE_FIFTY.toString();
    const startSequence = isLoadMore
      ? calculationStartEndSequence(cisMemo[storeId].endSequence, true)
      : NUMBER_PAGINATION.START_SEQUENCE.toString();

    const body = {
      common: {
        accountId
      },
      selectFields: [
        'memoDate',
        'memoText',
        'memoCisCollectionFlag',
        'memoSequenceNumber',
        'memoCisClerkNumberCollectorIdentifier'
      ],
      startSequence,
      endSequence
    } as GetCISMemosRequestBody;

    if (startDate && endDate && startDate === endDate) {
      body.memoDate = formatTime(startDate).shortYearShortMonthShortDate;
      body.memoDateRelationalOperator = 'EQUAL';
    }

    if (startDate && endDate && startDate !== endDate) {
      body.memoDate = formatTime(startDate).shortYearShortMonthShortDate;
      body.memoDateEnd = formatTime(endDate).shortYearShortMonthShortDate;
      body.memoDateRelationalOperator = 'BETWEEN';
    }

    const { data } = await cisMemoServices.getCISMemos(body);

    const memoMapping = mapping.data?.cisMemos || {};
    const mappedData = mappingDataFromArray<IMemoCIS>(
      data?.cisMemos,
      memoMapping
    );

    return {
      data: mappedData,
      startSequence,
      endSequence
    };
  } catch (error) {
    const errorStatus = error?.status || error?.response?.status;
    // no record
    if (errorStatus === NO_RESULT_CODE) return { data: [] };

    return thunkAPI.rejectWithValue(error);
  }
});

export const calculationStartEndSequence = (
  StartOrEndSequence: string,
  isStartSequence = false
) => {
  const parseNumber = Number(StartOrEndSequence);

  const numberPagination = isStartSequence
    ? NUMBER_PAGINATION.START_SEQUENCE
    : NUMBER_PAGINATION.END_SEQUENCE_FIFTY;

  const sequence = parseNumber + numberPagination;
  return sequence.toString();
};

export const getCISMemoBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateMemoCIS>
) => {
  builder
    .addCase(getMemoCISRequest.pending, (draftState, action) => {
      const { storeId, isLoadMore, isSearch } = action.meta.arg;
      if (!isLoadMore && !isSearch) {
        draftState[storeId] = {
          ...draftState[storeId],
          loading: true,
          loadMore: false,
          hasLoadMore: false,
          endSequence: DefaultSequence.END_SEQUENCE,
          startSequence: DefaultSequence.START_SEQUENCE,
          filter: initialSearchValues,
          originalSort: defaultSort,
          changingSort: defaultSort,
          memos: []
        };
      } else if (!isLoadMore && isSearch) {
        draftState[storeId] = {
          ...draftState[storeId],
          loading: true,
          endSequence: DefaultSequence.END_SEQUENCE,
          startSequence: DefaultSequence.START_SEQUENCE
        };
      } else {
        draftState[storeId].loadMore = true;
      }
    })

    .addCase(getMemoCISRequest.fulfilled, (draftState, action) => {
      const { storeId, isLoadMore, isSearch } = action.meta.arg;

      if (isEmpty(action.payload?.data) && !isLoadMore) {
        draftState[storeId].loading = false;
        draftState[storeId].loadMore = false;
        draftState[storeId].hasLoadMore = false;
        draftState[storeId].memos = [];
      } else {
        const { data, startSequence, endSequence } = action.payload;
        const hasLoadMore =
          !isArrayHasValue(data) ||
          data.length < NUMBER_PAGINATION.END_SEQUENCE_FIFTY
            ? false
            : true;

        // if is searching cis-memo with date, replace all data with new payload
        const memoList = isSearch
          ? data
          : [...draftState[storeId].memos, ...data];

        // Handle case: duplicate data when responsive info-bar
        const seen = new Set();
        const uniqueMemoList = memoList.filter(el => {
          const duplicate = seen.has(el.memoSequenceNumber);
          seen.add(el.memoSequenceNumber);
          return !duplicate;
        });

        draftState[storeId].memos = uniqueMemoList;
        draftState[storeId].loading = false;
        draftState[storeId].loadMore = false;

        draftState[storeId].hasLoadMore = hasLoadMore;
        draftState[storeId].startSequence = startSequence!;
        draftState[storeId].endSequence = endSequence!;
      }

      if (draftState[storeId].isForceRefresh) {
        draftState[storeId].isForceRefresh = false;
      }
    })
    .addCase(getMemoCISRequest.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].loading = false;
      if (draftState[storeId].loadMore) {
        draftState[storeId].loadMore = false;
      } else {
        draftState[storeId].memos = [];
      }
    });
};
