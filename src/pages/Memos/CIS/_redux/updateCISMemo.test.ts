import { createStore } from 'redux';
import dateTime from 'date-and-time';
import { rootReducer } from 'storeConfig';
import { IMemoCIS, UpdateCISMemoArgs } from '../types';
import {
  triggerUpdateCISMemoRequest,
  updateCISMemoRequest,
  getMemoCISAfterUpdateRequest
} from './updateCISMemo';

import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
// Services
import cisMemoServices from '../cisMemoServices';
import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault,
  storeId
} from 'app/test-utils';
import { I18N_MEMOS } from 'pages/Memos/constants';
import { isRejected } from '@reduxjs/toolkit';

const memos: IMemoCIS[] = [
  {
    memoSequenceNumber: '1',
    memoDate: new Date().toString(),
    memoIdentifier: 1,
    memoOperatorIdentifier: '1',
    memoText: 'This !* MEMO',
    memoType: '',
    type: {}
  },
  {
    memoSequenceNumber: '2',
    memoIdentifier: 1,
    memoText: 'This !* MEMO',
    memoType: '',
    type: {}
  }
];

const initialState: Partial<RootState> = {
  mapping: {
    loading: false,
    data: {
      cisMemos: {
        memoSequenceNumber: 'memoSequenceNumber',
        memoDate: 'memoDate',
        memoIdentifier: 'memoIdentifier',
        memoOperatorIdentifier: 'memoOperatorIdentifier',
        memoType: 'memoType',
        memoText: 'memoText'
      },
    }
  },
  cisMemo: {
    [storeId]: {
      endSequence: '51',
      hasLoadMore: true,
      loadMore: false,
      loading: false,
      memos,
      changingSort: {},
      originalSort: {},
      startSequence: '1',
      isForceRefresh: false,
      filter: {
        keyword: ''
      },
      memoCISRefData: {
        isLoading: true,
        sourceRefData: [],
        typeRefData: []
      },
    }
  }
};

const args: UpdateCISMemoArgs = {
  storeId,
  payload: {
    memoSequenceNumber: '1',
    accountId: storeId,
    memoText: 'Memo text',
    memoCisClerkNumberCollectorIdentifier: '',
    memoType: '',
    memoFlags: ''
  }
};

const toastSpy = mockActionCreator(actionsToast);

let spy: jest.SpyInstance;
let spy2: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();

  spy2?.mockReset();
  spy2?.mockRestore();
});

describe('updateCISMemoRequest', () => {
  describe('success', () => {
    it('memoText not contains \n', async () => {
      spy = jest
        .spyOn(cisMemoServices, 'updateCISMemo')
        .mockResolvedValue({ ...responseDefault, data: [] });

      const store = createStore(rootReducer, initialState);
      const response = await updateCISMemoRequest(args)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(response.payload).toEqual({
        memoSequenceNumber: args.payload.memoSequenceNumber,
        memoText: args.payload.memoText,
        memoType: args.payload.memoType
      });
    });

    it('memoText contains \n', async () => {
      spy = jest
        .spyOn(cisMemoServices, 'updateCISMemo')
        .mockResolvedValue({ ...responseDefault, data: [] });

      const store = createStore(rootReducer, initialState);
      const response = await updateCISMemoRequest({
        storeId,
        payload: {
          accountId: storeId,
          memoText: 'Memo \n text',
          memoCisClerkNumberCollectorIdentifier: '',
          memoSequenceNumber: '',
          memoType: '',
          memoFlags: ''
        }
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({
        memoSequenceNumber: '',
        memoText: 'Memo   text',
        memoType: ''
      });
    });
  });

  it('reject', async () => {
    spy = jest.spyOn(cisMemoServices, 'updateCISMemo').mockRejectedValue({});

    const store = createStore(rootReducer, initialState);
    const response = await updateCISMemoRequest(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(isRejected(response)).toBeTruthy();
  });
});

describe('triggerUpdateCISMemoRequest', () => {
  describe('success', () => {
    it('isFulfilled', async () => {
      // mock
      const mockAction = toastSpy('addToast');
      const mockOnSuccess = jest.fn();

      const store = createStore(rootReducer, initialState);
      await triggerUpdateCISMemoRequest({
        ...args,
        onSuccess: mockOnSuccess
      })(
        byPassFulfilled(updateCISMemoRequest.fulfilled.type),
        store.getState,
        {}
      );

      expect(mockAction).toBeCalledWith({
        show: true,
        type: 'success',
        message: I18N_MEMOS.MEMOS_UPDATE_SUCCESS
      });
      expect(mockOnSuccess).toBeCalled();
    });

    it('isRejected', async () => {
      // mock
      const mockAction = toastSpy('addToast');
      const mockOnFailure = jest.fn();

      const store = createStore(rootReducer, initialState);
      await triggerUpdateCISMemoRequest({
        ...args,
        onFailure: mockOnFailure
      })(
        byPassRejected(updateCISMemoRequest.fulfilled.type),
        store.getState,
        {}
      );

      expect(mockAction).toBeCalledWith({
        show: true,
        type: 'error',
        message: I18N_MEMOS.MEMOS_UPDATE_FAILURE
      });
      expect(mockOnFailure).toBeCalled();
    });
  });
});

describe('getMemoCISAfterUpdateRequest', () => {
  const today = new Date();
  const yesterday = dateTime.addDays(today, -1);

  const cisMemo_1: IMemoCIS = {
    memoSequenceNumber: '1',
    memoDate: new Date().toString(),
    memoIdentifier: 1,
    memoOperatorIdentifier: '1',
    memoText: 'This MEMO',
    memoType: '',
    type: {}
  };
  const cisMemo_3: IMemoCIS = {
    memoSequenceNumber: '3',
    memoDate: new Date().toString(),
    memoIdentifier: 3,
    memoOperatorIdentifier: '3',
    memoText: 'This MEMO',
    memoType: '',
    type: {}
  };

  describe('success', () => {
    it('with empty state', async () => {
      spy = jest.spyOn(cisMemoServices, 'getCISMemos').mockResolvedValue({
        ...responseDefault,
        data: { cisMemos: [cisMemo_1] }
      });

      const store = createStore(rootReducer, {});
      const response = await getMemoCISAfterUpdateRequest({
        storeId,
        isLoadMore: false,
        postData: {
          accountId: storeId,
          startDate: today.toString(),
          endDate: today.toString()
        }
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({ data: undefined });
    });

    it('startDate and endDate is in the same', async () => {
      spy = jest.spyOn(cisMemoServices, 'getCISMemos').mockResolvedValue({
        ...responseDefault,
        data: {
          cisMemos: [cisMemo_3]
        }
      });

      const store = createStore(rootReducer, initialState);
      const response = await getMemoCISAfterUpdateRequest({
        storeId,
        isLoadMore: false,
        postData: {
          accountId: storeId,
          startDate: today.toString(),
          endDate: today.toString()
        },
        memoSequenceNumber: '3'
      })(store.dispatch, store.getState, {});

      expect(cisMemo_3).toEqual({ ...response.payload!.data!, type: {} });
    });

    it('startDate and endDate is not in the same', async () => {
      spy = jest.spyOn(cisMemoServices, 'getCISMemos').mockResolvedValue({
        ...responseDefault,
        data: { cisMemos: [cisMemo_1] }
      });

      const store = createStore(rootReducer, initialState);
      const response = await getMemoCISAfterUpdateRequest({
        storeId,
        isLoadMore: true,
        postData: {
          accountId: storeId,
          startDate: yesterday.toString(),
          endDate: today.toString()
        },
        memoSequenceNumber: '1'
      })(store.dispatch, store.getState, {});

      expect(cisMemo_1).toEqual({ ...response.payload!.data, type: {} });
    });
  });

  describe('error', () => {
    it('error 455', async () => {
      spy = jest
        .spyOn(cisMemoServices, 'getCISMemos')
        .mockRejectedValue({ status: '455' });

      const store = createStore(rootReducer, initialState);
      const response = await getMemoCISAfterUpdateRequest({
        storeId,
        isLoadMore: true,
        postData: {
          accountId: storeId,
          startDate: yesterday.toString(),
          endDate: today.toString()
        }
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({ data: undefined });
    });

    it('another error', async () => {
      spy = jest.spyOn(cisMemoServices, 'getCISMemos').mockRejectedValue({});

      const store = createStore(rootReducer, initialState);
      const response = await getMemoCISAfterUpdateRequest({
        storeId,
        isLoadMore: true,
        postData: {
          accountId: storeId,
          startDate: yesterday.toString(),
          endDate: today.toString()
        }
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({});
    });
  });
});
