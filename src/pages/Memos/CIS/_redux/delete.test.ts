import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { deleteCISMemoRequest, triggerDeleteCISMemo } from './delete';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// Services
import cisMemoServices from '../cisMemoServices';

const storeId = 'aaaa';

const requestActionData = {
  storeId,
  postData: {
    accountId: storeId,
    memoIdentifier: 123
  }
};

describe('delete Memo CIS', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {});
  });

  it('should delete success', async () => {
    jest
      .spyOn(cisMemoServices, 'deleteCISMemo')
      .mockResolvedValue({ data: { message: 'success' } });

    const data = await deleteCISMemoRequest(requestActionData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(data.type).toBe(deleteCISMemoRequest.fulfilled.type);
    expect(store.getState().cisMemo[storeId].deleteCISMemo.loading).toBe(false);
  });

  it('should delete fail', async () => {
    jest
      .spyOn(cisMemoServices, 'deleteCISMemo')
      .mockRejectedValue({ data: { message: 'failed' } });

    const data = await deleteCISMemoRequest(requestActionData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(data.type).toBe(deleteCISMemoRequest.rejected.type);
    expect(store.getState().cisMemo[storeId].deleteCISMemo.loading).toBe(false);
  });

  it('should call triggerDeleteCISMemo fulfilled with onSuccess', async () => {
    const mockAddToast = jest
      .fn()
      .mockResolvedValue({ type: 'xxx', payload: 'xxx' });
    jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);

    await triggerDeleteCISMemo(requestActionData)(
      jest.fn().mockResolvedValue({
        meta: {
          arg: requestActionData,
          requestStatus: 'fulfilled',
          requestId: 'nUNxO06PDeUf07SQLr2GD'
        },
        payload: { data: { status: 1 } }
      }),
      undefined,
      {}
    );
    expect(mockAddToast).toBeCalled();
  });

  it('should call triggerDeleteCISMemo reject with onFailure', async () => {
    const mockAddToast = jest
      .fn()
      .mockResolvedValue({ type: 'xxx', payload: 'xxx' });
    jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);

    await triggerDeleteCISMemo(requestActionData)(
      jest.fn().mockResolvedValue({
        meta: {
          arg: requestActionData,
          requestStatus: 'rejected',
          requestId: 'nUNxO06PDeUf07SQLr2GD'
        },
        payload: {}
      }),
      store.getState,
      {}
    );

    expect(mockAddToast).toBeCalled();
  });
});
