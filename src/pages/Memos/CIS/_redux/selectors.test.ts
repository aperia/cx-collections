import isEqual from 'lodash.isequal';

// selectors
import {
  selectMemosCIS,
  selectIsDisplayIndicator,
  selectIsLoadingMoreMemos,
  getMemos,
  getOriginalSort,
  getChangingSort,
  selectIsSort,
  selectIsHasLoadMore,
  selectIsLoadingMemos,
  selectIsDisplayMemos,
  selectSortBy,
  selectOrderBy,
  selectIsFilteringCISMemo,
  selectFilterCriteria,
  selectSourceRefData,
  selectTypeRefData,
  selectTotalVisibleMemoCIS,
  selectorIsForceRefresh,
  getFilter,
  selectEditModalLoading,
  getAddCisFormValues,
  getEditCisFormValues,
  selectEditCisFormValues,
  selectAddCisFormValues
} from './selectors';
import { IMemoCIS, defaultSort, SearchFormValues } from '../types';

import { storeId } from 'app/test-utils/renderComponentWithMockStore';

// helpers
import { handleSort, handleFilter, handleSplitTypes } from '../helpers';
import { initialSearchValues } from '../Search/Body';
import { isArrayHasValue } from 'app/helpers';
import { selectorWrapper } from 'app/test-utils';
import { ADD_MEMO_CIS_VIEWS, EDIT_MEMO_CIS_VIEWS } from '../constants';

const filter = {
  keyword: ''
} as SearchFormValues;

const memos: IMemoCIS[] = [
  {
    memoIdentifier: 141,
    memoOperatorIdentifier: '208',
    memoType: 'Priority',
    memoSequenceNumber: '9',
    memoText:
      'hacking FTP Manors deposit connect withdrawal schemas New Technician Pizza 24/7 Operations lime Metrics mindshare withdrawal extend RAM Rustic open-source',
    memoDate: 'Thu Nov 12 2020 09:36:17 GMT+0700 (Indochina Time)',
    type: {}
  },
  {
    memoIdentifier: 141,
    memoOperatorIdentifier: '208',
    memoType: 'Permanent',
    memoSequenceNumber: '9',
    memoText:
      'hacking FTP Manors deposit connect withdrawal schemas New Technician Pizza 24/7 Operations lime Metrics mindshare withdrawal extend RAM Rustic open-source',
    memoDate: 'Thu Nov 12 2020 09:36:17 GMT+0700 (Indochina Time)',
    type: {}
  }
];

const mockData: Partial<RootState> = {
  cisMemo: {
    [storeId]: {
      endSequence: '100',
      hasLoadMore: true,
      editModalLoading: true,
      loadMore: false,
      loading: false,
      memos: memos,
      changingSort: defaultSort,
      originalSort: defaultSort,
      startSequence: '51',
      isForceRefresh: false,
      filter: {
        keyword: ''
      },
      memoCISRefData: {
        isLoading: true,
        sourceRefData: [
          { value: 'A', description: 'A - Account Level Processing' },
          { value: 'J', description: 'J - Adjustment' },
          { value: 'B', description: 'B - Chargeback' },
          { value: 'W', description: 'W - CIMS' },
          { value: 'C', description: 'C - CIS' },
          { value: 'X', description: 'X - Collections memo' },
          { value: 'N', description: 'N - Non-monetary transaction' },
          { value: 'P', description: 'P - Payment' },
          { value: 'R', description: 'R - Retrieval' }
        ],
        typeRefData: [
          { value: 'Permanent', description: 'Permanent' },
          { value: 'Priority', description: 'Priority' },
          { value: 'Standard', description: 'Standard' }
        ]
      }
    }
  },
  form: {
    [`${storeId}-${ADD_MEMO_CIS_VIEWS.ID}`]: {
      values: {},
      registeredFields: []
    },
    [`${storeId}-${EDIT_MEMO_CIS_VIEWS.ID}`]: {
      values: {},
      registeredFields: []
    }
  }
};

const CISMemo = mockData.cisMemo![storeId];

const selectorTrigger = selectorWrapper(mockData);

describe('Test Selector CIS Memos', () => {
  it('Test function getMemos', () => {
    const { data, emptyData } = selectorTrigger(getMemos);

    expect(data).toEqual(CISMemo.memos);
    expect(emptyData).toEqual([]);
  });

  it('Test function getFilter', () => {
    const { data, emptyData } = selectorTrigger(getFilter);

    expect(data).toEqual(mockData.cisMemo![storeId].filter);
    expect(emptyData).toEqual({});
  });

  it('Test function getOriginalSort', () => {
    const { data, emptyData } = selectorTrigger(getOriginalSort);

    expect(data).toEqual(CISMemo.originalSort);
    expect(emptyData).toEqual({});
  });

  it('Test function getChangingSort ', () => {
    const { data, emptyData } = selectorTrigger(getChangingSort);

    expect(data).toEqual(CISMemo.originalSort);
    expect(emptyData).toEqual({});
  });

  it('Test function getAddCisFormValues', () => {
    const { data, emptyData } = selectorTrigger(getAddCisFormValues);

    expect(data).toEqual(
      mockData.form![`${storeId}-${ADD_MEMO_CIS_VIEWS.ID}`].values
    );
    expect(emptyData).toEqual({});
  });

  it('Test function getEditCisFormValues', () => {
    const { data, emptyData } = selectorTrigger(getEditCisFormValues);

    expect(data).toEqual(
      mockData.form![`${storeId}-${EDIT_MEMO_CIS_VIEWS.ID}`].values
    );
    expect(emptyData).toEqual({});
  });

  it('Test function selectEditCisFormValues', () => {
    const { data, emptyData } = selectorTrigger(selectEditCisFormValues);

    expect(data).toEqual(
      mockData.form![`${storeId}-${EDIT_MEMO_CIS_VIEWS.ID}`].values
    );
    expect(emptyData).toEqual({});
  });

  it('Test function selectAddCisFormValues', () => {
    const { data, emptyData } = selectorTrigger(selectAddCisFormValues);

    expect(data).toEqual(
      mockData.form![`${storeId}-${ADD_MEMO_CIS_VIEWS.ID}`].values
    );
    expect(emptyData).toEqual({});
  });

  it('Test Selector selectOrderBy', () => {
    const { data, emptyData } = selectorTrigger(selectOrderBy);

    expect(data).toEqual(CISMemo.changingSort!.orderBy);
    expect(emptyData).toBeUndefined();
  });

  it('Test Selector selectSortBy', () => {
    const { data, emptyData } = selectorTrigger(selectSortBy);

    expect(data).toEqual(CISMemo.changingSort!.sortBy);
    expect(emptyData).toBeUndefined();
  });

  it('Test Selector selectMemosCIS without memos', () => {
    const { originalSort } = CISMemo;
    const { data, emptyData } = selectorTrigger(selectMemosCIS);

    const sortData = handleSort(handleFilter([], filter), originalSort!);
    expect(data).toEqual(expect.arrayContaining(sortData!));
    expect(emptyData).toEqual([]);
  });

  it('Test Selector selectMemosCIS within memos', () => {
    const { originalSort, memos } = CISMemo;

    const { data, emptyData } = selectorTrigger(selectMemosCIS);

    const sortData = handleSort(
      handleFilter(handleSplitTypes(memos), filter),
      originalSort!
    );
    expect(data).toEqual(sortData);
    expect(emptyData).toEqual([]);
  });

  it('Test Selector selectIsDisplayMemos', () => {
    const { data, emptyData } = selectorTrigger(selectIsDisplayMemos);

    expect(data).toEqual(isArrayHasValue(CISMemo.memos));
    expect(emptyData).toBeFalsy();
  });

  it('Test Selector selectIsLoadingMemos', () => {
    const { data, emptyData } = selectorTrigger(selectIsLoadingMemos);

    expect(data).toEqual(CISMemo.loading);
    expect(emptyData).toBeFalsy();
  });

  it('Test Selector selectIsLoadingMoreMemos', () => {
    const { data, emptyData } = selectorTrigger(selectIsLoadingMoreMemos);

    expect(data).toEqual(CISMemo.loadMore);
    expect(emptyData).toBeFalsy();
  });

  it('Test Selector selectIsHasLoadMore', () => {
    const { data, emptyData } = selectorTrigger(selectIsHasLoadMore);

    expect(data).toEqual(CISMemo.hasLoadMore);
    expect(emptyData).toBeFalsy();
  });

  it('Test Selector selectEditModalLoading', () => {
    const { data, emptyData } = selectorTrigger(selectEditModalLoading);

    expect(data).toEqual(CISMemo.editModalLoading);
    expect(emptyData).toBeFalsy();
  });

  it('Test Selector IsDisplayIndicator', () => {
    const { data, emptyData } = selectorTrigger(selectIsDisplayIndicator);

    const { originalSort, changingSort } = CISMemo;
    expect(data).toEqual(!isEqual([changingSort], [originalSort]));
    expect(emptyData).toBeTruthy();
  });

  it('Test Selector selectIsDisabledButton', () => {
    const { data, emptyData } = selectorTrigger(selectIsSort);

    expect(data).toEqual(
      !isEqual([CISMemo.originalSort], [CISMemo.changingSort])
    );
    expect(emptyData).toBeFalsy();
  });

  it('Test Selector selectIsFilteringCISMemo', () => {
    const { data, emptyData } = selectorTrigger(selectIsFilteringCISMemo);

    expect(data).toEqual(!isEqual(filter, initialSearchValues));
    expect(emptyData).toBeFalsy();
  });

  it('Test Selector selectFilterCriteria', () => {
    const { data, emptyData } = selectorTrigger(selectFilterCriteria);

    expect(data).toEqual(CISMemo.filter);
    expect(emptyData).toEqual({});
  });

  it('Test Selector selectSourceRefData', () => {
    const { data, emptyData } = selectorTrigger(selectSourceRefData);

    expect(data).toEqual(CISMemo.memoCISRefData.sourceRefData);
    expect(emptyData).toEqual([]);
  });

  it('Test Selector selectTypeRefData', () => {
    const { data, emptyData } = selectorTrigger(selectTypeRefData);

    expect(data).toEqual(CISMemo.memoCISRefData?.typeRefData);
    expect(emptyData).toEqual([]);
  });

  it('Test Selector selectTotalVisibleMemoCIS', () => {
    const { data, emptyData } = selectorTrigger(selectTotalVisibleMemoCIS);

    expect(data).toEqual(CISMemo.memos.length);
    expect(emptyData).toBeFalsy();
  });

  it('Test Selector selectorIsForceRefresh', () => {
    const { data, emptyData } = selectorTrigger(selectorIsForceRefresh);

    expect(data).toEqual(CISMemo.isForceRefresh);
    expect(emptyData).toBeFalsy();
  });
});
