import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { formatTime, mappingDataFromArray } from 'app/helpers';
import { NO_RESULT_CODE } from 'pages/AccountSearch/AccountSearchList/constants';
import cisMemoServices from '../cisMemoServices';
import {
  GetCISMemosRequestBody,
  IInitialStateMemoCIS,
  IMemoCIS,
  MemoCISArgs,
  MemoCISPayload
} from '../types';

export const refreshCISMemo = createAsyncThunk<
  MemoCISPayload,
  MemoCISArgs,
  ThunkAPIConfig
>('memoCIS/refreshCISMemo', async (args, thunkAPI) => {
  try {
    const { storeId, postData } = args;
    const { cisMemo, mapping } = thunkAPI.getState();
    const { accountId } = postData;

    const startSequence = '1';
    const endSequence = cisMemo[storeId].endSequence;

    const { specificDate, specificDateValue, dateRange, dateRangeValue } =
      cisMemo[storeId].filter ?? {};
    const startDate =
      (specificDate && specificDateValue) ||
      (dateRange && dateRangeValue?.start);
    const endDate =
      (specificDate && specificDateValue) || (dateRange && dateRangeValue?.end);

    const body = {
      common: {
        accountId
      },
      selectFields: [
        'memoDate',
        'memoText',
        'memoCisCollectionFlag',
        'memoSequenceNumber',
        'memoCisClerkNumberCollectorIdentifier'
      ],
      startSequence,
      endSequence
    } as GetCISMemosRequestBody;

    if (startDate && endDate && startDate === endDate) {
      body.memoDate = formatTime(
        startDate.toDateString()
      ).shortYearShortMonthShortDate;
      body.memoDateRelationalOperator = 'EQUAL';
    }

    if (startDate && endDate && startDate !== endDate) {
      body.memoDate = formatTime(
        startDate.toDateString()
      ).shortYearShortMonthShortDate;
      body.memoDateEnd = formatTime(
        endDate.toDateString()
      ).shortYearShortMonthShortDate;
      body.memoDateRelationalOperator = 'BETWEEN';
    }

    const { data } = await cisMemoServices.getCISMemos(body);

    const memoMapping = mapping.data?.cisMemos || {};
    const mappedData = mappingDataFromArray<IMemoCIS>(
      data?.cisMemos,
      memoMapping
    );

    return { data: mappedData };
  } catch (error) {
    const errorStatus = error?.status || error?.response?.status;
    // no record
    if (errorStatus === NO_RESULT_CODE) {
      return { data: [] };
    }
    return thunkAPI.rejectWithValue(error);
  }
});

export const refreshCISMemoBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateMemoCIS>
) => {
  builder
    .addCase(refreshCISMemo.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: true
      };
    })
    .addCase(refreshCISMemo.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      draftState[storeId].memos = data;
      draftState[storeId].loading = false;
    })
    .addCase(refreshCISMemo.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: false
      };
    });
};
