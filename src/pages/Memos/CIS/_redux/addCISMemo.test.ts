// actions
import { cisMemoActions } from './reducers';
import {
  mockCreateStore,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';
import { mockApiMapping } from 'app/test-utils/mockApiMapping';
import { triggerAddCISMemoRequest } from './addCISMemo';
import { AddCISMemoArgs } from '../types';

// Services
import cisMemoServices from '../cisMemoServices';

const initialState = {};

const mockData = {
  storeId,
  payload: { accountId: storeId, memoText: 'string', memoType: 'string' }
};

describe('Test reducer getCISMemo', () => {
  const { addCISMemo } = mockApiMapping.cisMemoService;
  const mockStore = mockCreateStore(initialState);
  const mockDataStore = () => mockStore.getState().cisMemo[storeId];
  const mockResolvedData = () =>
    addCISMemo.mockResolvedValueOnce({
      status: 200,
      data: 'add memo successfully'
    });
  const mockRejectedData = () =>
    addCISMemo.mockRejectedValueOnce(new Error('Async'));

  const mockApi = (data: AddCISMemoArgs) =>
    mockStore.dispatch(cisMemoActions.addCISMemoRequest(data) as any);

  it('Test addCISMemo', async () => {
    try {
      jest.spyOn(cisMemoServices, 'addCISMemo').mockResolvedValue({ data: {} });
      mockResolvedData();
      await mockApi(mockData);
      const dataAfterUpdateStore = mockDataStore();
      expect(dataAfterUpdateStore).toEqual({
        loading: true
      });
    } catch (error) {}
  });

  it('Test reject addCISMemo', async () => {
    try {
      mockRejectedData();
      await mockApi(mockData);
      const dataAfterUpdateStore = mockDataStore();
      expect(dataAfterUpdateStore).toEqual({
        loading: false
      });
    } catch (error) {}
  });

  it('should call triggerAddCISMemoRequest fulfilled with onSuccess', async () => {
    const onSuccess = jest.fn();

    await triggerAddCISMemoRequest({
      ...mockData,
      onSuccess
    })(
      jest.fn().mockResolvedValue({
        meta: {
          arg: mockData,
          requestStatus: 'fulfilled',
          requestId: 'nUNxO06PDeUf07SQLr2GD'
        }
      }),
      undefined,
      {}
    );

    expect(onSuccess).toBeCalled();
  });

  it('should call triggerAddCISMemoRequest isRejected with onFailure', async () => {
    const onFailure = jest.fn();

    await triggerAddCISMemoRequest({ ...mockData, onFailure })(
      jest.fn().mockResolvedValue({
        meta: {
          arg: mockData,
          requestStatus: 'rejected',
          requestId: 'nUNxO06PDeUf07SQLr2GD'
        }
      }),
      undefined,
      {}
    );

    expect(onFailure).toBeCalled();
  });
});
