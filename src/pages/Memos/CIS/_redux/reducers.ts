// Redux
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Types
import {
  IInitialStateMemoCIS,
  IApplySortAndOrder,
  IChangeSortAndOrder,
  FilterPayload
} from '../types';

// Async actions
import {
  addCISMemoRequest,
  addCISMemoBuilder,
  triggerAddCISMemoRequest
} from './addCISMemo';
import {
  deleteCISMemoBuilder,
  deleteCISMemoRequest,
  triggerDeleteCISMemo
} from './delete';
import { getCISMemoBuilder, getMemoCISRequest } from './getMemoCIS';
import { refreshCISMemo, refreshCISMemoBuilder } from './refreshCISMemo';
import {
  getCISMemoRefDataBuilder,
  getMemoCISRefData
} from './getMemoCISRefData';
import {
  triggerUpdateCISMemoRequest,
  updateCISMemoBuilder,
  updateCISMemoRequest,
  getMemoCISAfterUpdateRequestBuilder
} from './updateCISMemo';

const { actions, reducer } = createSlice({
  name: 'cisMemo',
  initialState: {} as IInitialStateMemoCIS,
  reducers: {
    changeSortAndOrder: (
      draftState,
      action: PayloadAction<IChangeSortAndOrder>
    ) => {
      const { storeId, fieldName, value } = action.payload;
      draftState[storeId].changingSort![fieldName] = value;
    },
    loading: (draftState, action: PayloadAction<{ storeId: string }>) => {
      const { storeId } = action.payload;
      draftState[storeId].loading = !draftState[storeId].loading;
    },
    applySortAndOrder: (
      draftState,
      action: PayloadAction<IApplySortAndOrder>
    ) => {
      const { storeId, isOriginalToChanging } = action.payload;
      if (isOriginalToChanging) {
        draftState[storeId].originalSort = draftState[storeId].changingSort;
      } else {
        draftState[storeId].changingSort = draftState[storeId].originalSort;
      }
    },
    forceRefreshCISMemoList: (
      draftState,
      action: PayloadAction<StoreIdPayload>
    ) => {
      const { storeId } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isForceRefresh: true
      };
    },
    searchAndFilter: (draftState, action: PayloadAction<FilterPayload>) => {
      const { storeId, ...filter } = action.payload;
      draftState[storeId].filter = filter;
    }
  },
  extraReducers: builder => {
    getCISMemoBuilder(builder);
    addCISMemoBuilder(builder);
    getCISMemoRefDataBuilder(builder);
    deleteCISMemoBuilder(builder);
    updateCISMemoBuilder(builder);
    getMemoCISAfterUpdateRequestBuilder(builder);
    refreshCISMemoBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getMemoCISRequest,
  refreshCISMemo,
  addCISMemoRequest,
  getMemoCISRefData,
  deleteCISMemoRequest,
  updateCISMemoRequest,
  triggerAddCISMemoRequest,
  triggerDeleteCISMemo,
  triggerUpdateCISMemoRequest
};

export { allActions as cisMemoActions, reducer };
