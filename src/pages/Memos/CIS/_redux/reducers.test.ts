import { IInitialStateMemoCIS } from '../types';
import { storeId } from 'app/test-utils';
import { cisMemoActions, reducer } from './reducers';

describe('Test CIS Memo Reducers', () => {
  const initialState: IInitialStateMemoCIS = {
    [storeId]: {
      memos: [],
      startSequence: '1',
      endSequence: '10',
      memoCISRefData: {},
      deleteCISMemo: {
        loading: false
      },
      changingSort: {
        changing: 'test'
      },
      originalSort: {
        origin: 'test'
      }
    }
  };

  describe('changeSortAndOrder', () => {
    it('should update changingSort field', () => {
      const cisStore = reducer(
        initialState,
        cisMemoActions.changeSortAndOrder({
          storeId,
          fieldName: 'fieldName',
          value: 'value'
        })
      );

      expect(cisStore[storeId].changingSort!['fieldName']).toEqual('value');
    });
  });

  describe('loading', () => {
    it('update loading = true', () => {
      const cisStore = reducer(
        initialState,
        cisMemoActions.loading({
          storeId
        })
      );

      expect(cisStore[storeId].loading).toEqual(true);
    });
  });

  describe('applySortAndOrder', () => {
    it('isOriginalToChanging = true', () => {
      const cisStore = reducer(
        initialState,
        cisMemoActions.applySortAndOrder({
          storeId,
          isOriginalToChanging: true
        })
      );

      expect(cisStore[storeId].originalSort).toEqual(
        cisStore[storeId].changingSort
      );
    });

    it('isOriginalToChanging = false', () => {
      const cisStore = reducer(
        initialState,
        cisMemoActions.applySortAndOrder({
          storeId,
          isOriginalToChanging: false
        })
      );

      expect(cisStore[storeId].changingSort).toEqual(
        cisStore[storeId].originalSort
      );
    });
  });

  describe('forceRefreshCISMemoList', () => {
    it('set isForceRefresh = true', () => {
      const cisStore = reducer(
        initialState,
        cisMemoActions.forceRefreshCISMemoList({
          storeId
        })
      );

      expect(cisStore[storeId].isForceRefresh).toEqual(true);
    });
  });

  describe('searchAndFilter', () => {
    it('set data to filter field', () => {
      const cisStore = reducer(
        initialState,
        cisMemoActions.searchAndFilter({
          storeId,
          keyword: 'a'
        })
      );

      expect(cisStore[storeId].filter).toEqual({ keyword: 'a' });
    });
  });
});
