// Redux
import { createSelector } from '@reduxjs/toolkit';

// Types
import {
  IMemoCIS,
  ISort,
  defaultSort,
  AddCisMemoFormValues,
  EditCisMemoFormValues
} from '../types';
import { SearchFormValues } from '../types';

// Helpers
import { isArrayHasValue } from 'app/helpers';
import { handleFilter, handleSort, handleSplitTypes } from '../helpers';
import isEmpty from 'lodash.isempty';
import isEqual from 'lodash.isequal';

// Constants
import { initialSearchValues } from '../Search/Body';
import { ADD_MEMO_CIS_VIEWS, EDIT_MEMO_CIS_VIEWS } from '../constants';

export const getMemos: CustomStoreIdSelector<IMemoCIS[]> = (
  states,
  storeId
) => {
  return states.cisMemo[storeId]?.memos || [];
};

export const getOriginalSort: CustomStoreIdSelector<ISort> = (
  states,
  storeId
) => {
  return states.cisMemo[storeId]?.originalSort || {};
};

export const getFilter: CustomStoreIdSelector<SearchFormValues> = (
  states,
  storeId
) => {
  return states.cisMemo[storeId]?.filter || {};
};

export const getChangingSort: CustomStoreIdSelector<ISort> = (
  states,
  storeId
) => {
  return states.cisMemo[storeId]?.changingSort || {};
};

export const getAddCisFormValues: CustomStoreIdSelector<
  AddCisMemoFormValues | MagicKeyValue
> = (states, storeId) => {
  return states.form[`${storeId}-${ADD_MEMO_CIS_VIEWS.ID}`]?.values || {};
};

export const getEditCisFormValues: CustomStoreIdSelector<
  EditCisMemoFormValues | MagicKeyValue
> = (states, storeId) => {
  return states.form[`${storeId}-${EDIT_MEMO_CIS_VIEWS.ID}`]?.values || {};
};

export const selectEditCisFormValues = createSelector(
  getEditCisFormValues,
  values => values
);

export const selectAddCisFormValues = createSelector(
  getAddCisFormValues,
  values => values
);

export const selectOrderBy = createSelector(
  getChangingSort,
  changingSort => changingSort?.orderBy
);

export const selectSortBy = createSelector(
  getChangingSort,
  changingSort => changingSort?.sortBy
);

export const selectMemosCIS = createSelector(
  getMemos,
  getOriginalSort,
  getFilter,
  (data: IMemoCIS[], originalSort: ISort, filter: SearchFormValues) => {
    if (!isArrayHasValue(data)) return [];
    const newList = handleSplitTypes(data);
    return handleSort(handleFilter(newList, filter), originalSort);
  }
);

export const selectIsDisplayMemos = createSelector(
  getMemos,
  (data: IMemoCIS[]) => isArrayHasValue(data)
);

export const selectorIsForceRefresh = createSelector(
  [
    (states: RootState, storeId: string) =>
      states.cisMemo[storeId]?.isForceRefresh || false
  ],
  (isForceRefresh: boolean) => isForceRefresh
);

export const selectIsLoadingMemos = createSelector(
  [
    (states: RootState, storeId: string) =>
      states.cisMemo[storeId]?.loading || false
  ],
  (loading: boolean) => loading
);

export const selectIsLoadingMoreMemos = createSelector(
  [
    (states: RootState, storeId: string) =>
      states.cisMemo[storeId]?.loadMore || false
  ],
  (loadMore: boolean) => loadMore
);

export const selectIsHasLoadMore = createSelector(
  (states: RootState, storeId: string) =>
    states.cisMemo[storeId]?.hasLoadMore || false,
  (hasLoadMore: boolean) => hasLoadMore
);

export const selectEditModalLoading = createSelector(
  (states: RootState, storeId: string) =>
    states.cisMemo[storeId]?.editModalLoading || false,
  (editModalLoading: boolean) => editModalLoading
);

export const selectIsDisplayIndicator = createSelector(
  getOriginalSort,
  (originalSort: ISort) => !isEqual(originalSort, defaultSort)
);

export const selectIsSort = createSelector(
  getOriginalSort,
  getChangingSort,
  (originalSort: ISort, changingSort: ISort) => {
    return !isEqual([changingSort], [originalSort]);
  }
);

export const selectIsFilteringCISMemo = createSelector(
  (states: RootState, storeId: string) => states.cisMemo[storeId]?.filter || {},
  filter => !isEmpty(filter) && !isEqual(filter, initialSearchValues)
);

export const selectFilterCriteria = createSelector(
  (states: RootState, storeId: string) => states.cisMemo[storeId]?.filter || {},
  (filter: SearchFormValues) => filter
);

export const selectTotalVisibleMemoCIS = createSelector(
  selectMemosCIS,
  data => {
    return data?.reduce((prev, curr) => {
      return prev + curr.memos.length;
    }, 0);
  }
);

export const selectSourceRefData = createSelector(
  (states: RootState, storeId: string) =>
    states.cisMemo[storeId]?.memoCISRefData?.sourceRefData || [],
  (data: RefDataValue[]) => data
);

export const selectTypeRefData = createSelector(
  (states: RootState, storeId: string) =>
    states.cisMemo[storeId]?.memoCISRefData?.typeRefData || [],
  (data: RefDataValue[]) => data
);
