// Redux
import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';

// Types
import {
  IInitialStateMemoCIS,
  UpdateCISMemoPayload,
  UpdateCISMemoArgs,
  TriggerUpdateCISMemoArgs,
  MemoCISArgs,
  GetCISMemosRequestBody,
  IMemoCIS,
  GetMemoCISAfterUpdatePayload
} from '../types';

// Actions
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// i18n
import { I18N_MEMOS } from '../../constants';

// Helpers
import isEmpty from 'lodash.isempty';
import isFunction from 'lodash.isfunction';

// Services
import cisMemoServices from '../cisMemoServices';
import { handleGenerateMemoFlags } from '../helpers';
import { NUMBER_PAGINATION } from 'app/constants';
import { formatTime, mappingDataFromArray } from 'app/helpers';
import { batch } from 'react-redux';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { NO_RESULT_CODE } from 'pages/AccountSearch/AccountSearchList/constants';

export const updateCISMemoRequest = createAsyncThunk<
  UpdateCISMemoPayload,
  UpdateCISMemoArgs,
  ThunkAPIConfig
>('cisMemo/updateCISMemo', async (args, thunkAPI) => {
  try {
    const {
      accountId,
      memoCisClerkNumberCollectorIdentifier,
      memoSequenceNumber,
      memoText,
      memoType,
      memoFlags
    } = args.payload;

    const body = {
      common: {
        accountId
      },
      memoCisClerkNumberCollectorIdentifier,
      memoSequenceNumber,
      memoText,
      memoFlags
    };

    await cisMemoServices.updateCISMemo(body);
    let parsedMemoText = memoText;
    if (memoText.includes('\n')) {
      parsedMemoText = parsedMemoText.replace('\n', ' ');
    }

    return {
      memoSequenceNumber,
      memoText: parsedMemoText,
      memoType
    };
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerUpdateCISMemoRequest = createAsyncThunk<
  void,
  TriggerUpdateCISMemoArgs,
  ThunkAPIConfig
>(`cisMemo/triggerEdit`, async (args, { dispatch }) => {
  const { storeId, payload, onFailure, onSuccess } = args;

  const data = { ...payload };
  data.memoFlags = handleGenerateMemoFlags(payload.memoType);

  const response = await dispatch(
    updateCISMemoRequest({
      storeId,
      payload: data
    })
  );

  if (isFulfilled(response)) {
    isFunction(onSuccess) && onSuccess();
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_MEMOS.MEMOS_UPDATE_SUCCESS
        })
      );

      dispatch(
        getMemoCISAfterUpdateRequest({
          storeId,
          isLoadMore: false,
          postData: { accountId: payload.accountId },
          memoSequenceNumber: payload.memoSequenceNumber
        })
      );

      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inModalBody'
        })
      );
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inMemoFlyOut'
        })
      );
    });
  }

  if (isRejected(response)) {
    isFunction(onFailure) && onFailure();

    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_MEMOS.MEMOS_UPDATE_FAILURE
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inModalBody',
          apiResponse: response.payload?.response
        })
      );
    });
  }
});

export const updateCISMemoBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateMemoCIS>
) => {
  builder
    .addCase(updateCISMemoRequest.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        editModalLoading: true
      };
    })
    .addCase(updateCISMemoRequest.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        editModalLoading: false
      };
    })
    .addCase(updateCISMemoRequest.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        editModalLoading: false
      };
    });
};

export const getMemoCISAfterUpdateRequest = createAsyncThunk<
  GetMemoCISAfterUpdatePayload,
  MemoCISArgs,
  ThunkAPIConfig
>('memoCIS/getMemoCISAfterUpdateRequest', async (args, thunkAPI) => {
  try {
    const { storeId, isLoadMore, postData } = args;
    const { cisMemo, mapping } = thunkAPI.getState();
    const { startDate, endDate, accountId } = postData;

    const startSequence = '1';
    const endSequence = isLoadMore
      ? cisMemo[storeId].endSequence
      : NUMBER_PAGINATION.END_SEQUENCE_FIFTY.toString();

    const body = {
      common: {
        accountId
      },
      selectFields: [
        'memoDate',
        'memoText',
        'memoCisCollectionFlag',
        'memoSequenceNumber',
        'memoCisClerkNumberCollectorIdentifier'
      ],
      startSequence,
      endSequence
    } as GetCISMemosRequestBody;

    if (startDate && endDate && startDate === endDate) {
      body.memoDate = formatTime(startDate).shortYearShortMonthShortDate;
      body.memoDateRelationalOperator = 'EQUAL';
    }

    if (startDate && endDate && startDate !== endDate) {
      body.memoDate = formatTime(startDate).shortYearShortMonthShortDate;
      body.memoDateEnd = formatTime(endDate).shortYearShortMonthShortDate;
      body.memoDateRelationalOperator = 'BETWEEN';
    }

    const { data } = await cisMemoServices.getCISMemos(body);

    const memoMapping = mapping.data?.cisMemos || {};
    const mappedData = mappingDataFromArray<IMemoCIS>(
      data?.cisMemos,
      memoMapping
    );

    const updatedMemo = mappedData.find(
      item => item.memoSequenceNumber === args.memoSequenceNumber
    );

    return {
      data: updatedMemo || undefined
    };
  } catch (error) {
    const errorStatus = error?.status || error?.response?.status
    // no record
    if (errorStatus === NO_RESULT_CODE) {
      return {
        data: undefined
      };
    }
    return thunkAPI.rejectWithValue(error);
  }
});

export const getMemoCISAfterUpdateRequestBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateMemoCIS>
) => {
  builder
    .addCase(getMemoCISAfterUpdateRequest.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: true
      };
    })
    .addCase(getMemoCISAfterUpdateRequest.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      if (!isEmpty(data)) {
        const memoIndex = draftState[storeId].memos.findIndex(
          item => item.memoSequenceNumber === data!.memoSequenceNumber
        );
        if (memoIndex > -1) {
          draftState[storeId].memos[memoIndex].memoSequenceNumber =
            data!.memoSequenceNumber;
          draftState[storeId].memos[memoIndex].memoText = data!.memoText;
          draftState[storeId].memos[memoIndex].memoDate = data!.memoDate;
        }
      }
      draftState[storeId] = {
        ...draftState[storeId],
        loading: false
      };
    })
    .addCase(getMemoCISAfterUpdateRequest.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: false
      };
    });
};
