import { responseDefault, storeId } from 'app/test-utils';

import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { getMemoCISRefData } from './getMemoCISRefData';
import cisMemoServices from '../cisMemoServices';

let spy: jest.SpyInstance;
let spy1: jest.SpyInstance;

describe('Test getMemoCISRefData async thunk', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();

    spy1?.mockReset();
    spy1?.mockRestore();
  });

  it('fulfilled', async () => {
    spy = jest.spyOn(cisMemoServices, 'getCISSource').mockResolvedValue({
      ...responseDefault,
      data: [{ value: 'A', description: 'A - Account Level Processing' }]
    });
    spy1 = jest.spyOn(cisMemoServices, 'getCISType').mockResolvedValue({
      ...responseDefault
    });

    const store = createStore(rootReducer);
    const response = await getMemoCISRefData({
      storeId,
      postData: { accountId: storeId }
    })(store.dispatch, store.getState, {});

    expect(response.type).toEqual('memoCIS/getMemoCISRefData/fulfilled');
    expect(response.payload).toEqual({
      sourceRefData: [
        { value: 'A', description: 'A - Account Level Processing' }
      ],
      typeRefData: null
    });
  });

  it('rejected', async () => {
    spy = jest.spyOn(cisMemoServices, 'getCISSource').mockRejectedValue({
      status: 500
    });
    spy1 = jest.spyOn(cisMemoServices, 'getCISType').mockResolvedValue({
      ...responseDefault
    });

    const store = createStore(rootReducer);
    const response = await getMemoCISRefData({
      storeId,
      postData: { accountId: storeId }
    })(store.dispatch, store.getState, {});

    expect(response.type).toEqual('memoCIS/getMemoCISRefData/rejected');
  });
});
