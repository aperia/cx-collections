// actions
import './reducers';
import {
  responseDefault,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';
import { getMemoCISRequest } from './getMemoCIS';
import { IMemoCIS, IReducerMemo, defaultSort } from '../types';
import { initialSearchValues } from '../Search/Body';
import { createStore } from 'redux';

import dateTime from 'date-and-time';

// Services
import cisMemoServices from '../cisMemoServices';
import { rootReducer } from 'storeConfig';
import { NO_RESULT_CODE } from 'pages/AccountSearch/AccountSearchList/constants';

export const dataEqual = {
  memos: [],
  hasLoadMore: false,
  startSequence: '1',
  endSequence: '50',
  loadMore: false,
  loading: false,
  originalSort: defaultSort,
  changingSort: defaultSort,
  isForceRefresh: false,
  filter: initialSearchValues,
  memoCISRefData: {},
  deleteCISMemo: { loading: true }
} as IReducerMemo;

const cisMemos: IMemoCIS[] = new Array(50).fill({}).map((item, index) => ({
  memoType: 'type',
  memoSequenceNumber: index.toString(),
  memoOperatorIdentifier: '74R',
  memoText: 'Quisquam officiis facere non. Incidunt error.',
  memoDate: '2020-12-20T12:36:27.880Z',
  memoIdentifier: 1,
  type: {}
}));

const today = new Date().toString();
const tomorrow = dateTime.addDays(new Date(today), 1).toString();

const cisMemo: IReducerMemo = {
  endSequence: '50',
  hasLoadMore: true,
  loadMore: false,
  loading: false,
  isForceRefresh: true,
  memos: [
    {
      memoType: 'type',
      type: {},
      memoSequenceNumber: '10',
      memoOperatorIdentifier: '74R',
      memoText: 'Quisquam officiis facere non. Incidunt error.',
      memoDate: '2020-12-20T12:36:27.880Z',
      memoIdentifier: 1
    }
  ],
  changingSort: {},
  originalSort: {},
  startSequence: '1',
  filter: {
    keyword: ''
  },
  memoCISRefData: {
    isLoading: true,
    sourceRefData: [],
    typeRefData: []
  },
  deleteCISMemo: { loading: false }
};

const initialState: Partial<RootState> = {
  mapping: {
    loading: false,
    data: {
      cisMemos: {
        memoType: 'memoType',
        memoSequenceNumber: 'memoSequenceNumber',
        memoOperatorIdentifier: 'memoOperatorIdentifier',
        memoText: 'memoText',
        memoDate: 'memoDate',
        memoIdentifier: 'memoIdentifier',
        type: 'type'
      }
    }
  },
  cisMemo: { [storeId]: cisMemo }
};

describe('Memos > CIS > getMemoCISRequest', () => {
  describe('success', () => {
    it('empty data', async () => {
      jest
        .spyOn(cisMemoServices, 'getCISMemos')
        .mockResolvedValue({ ...responseDefault, data: { cisMemos } });

      const store = createStore(rootReducer, {
        cisMemo: { [storeId]: { ...cisMemo, isForceRefresh: false } }
      });

      const response = await getMemoCISRequest({
        storeId,
        isLoadMore: true,
        isSearch: true,
        postData: {
          accountId: storeId,
          startDate: today,
          endDate: today
        }
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({
        data: [],
        startSequence: '51',
        endSequence: '100'
      });
    });

    it('empty data', async () => {
      jest
        .spyOn(cisMemoServices, 'getCISMemos')
        .mockResolvedValue({ ...responseDefault, data: { cisMemos } });

      const store = createStore(rootReducer, {
        cisMemo: { [storeId]: { ...cisMemo, isForceRefresh: false } }
      });

      const response = await getMemoCISRequest({
        storeId,
        isLoadMore: false,
        isSearch: true,
        postData: {
          accountId: storeId,
          startDate: today,
          endDate: today
        }
      } as any)(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({
        data: [],
        startSequence: '1',
        endSequence: '50'
      });
    });

    it('load more', async () => {
      jest
        .spyOn(cisMemoServices, 'getCISMemos')
        .mockResolvedValue({ ...responseDefault, data: { cisMemos } });

      const store = createStore(rootReducer, initialState);

      const response = await getMemoCISRequest({
        storeId,
        isLoadMore: true,
        isSearch: true,
        postData: {
          accountId: storeId,
          startDate: today,
          endDate: today
        }
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({
        data: cisMemos,
        startSequence: '51',
        endSequence: '100'
      });
    });

    it('no load more', async () => {
      jest
        .spyOn(cisMemoServices, 'getCISMemos')
        .mockResolvedValue({ ...responseDefault, data: { cisMemos } });

      const store = createStore(rootReducer, initialState);
      const response = await getMemoCISRequest({
        storeId,
        postData: {
          accountId: storeId,
          startDate: today,
          endDate: tomorrow
        }
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({
        data: cisMemos,
        startSequence: '1',
        endSequence: '50'
      });
    });

    it('search', async () => {
      jest
        .spyOn(cisMemoServices, 'getCISMemos')
        .mockResolvedValue({ ...responseDefault, data: { cisMemos } });

      const store = createStore(rootReducer, initialState);

      const response = await getMemoCISRequest({
        storeId,
        isSearch: true,
        postData: {
          accountId: storeId,
          startDate: today,
          endDate: today
        }
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({
        data: cisMemos,
        startSequence: '1',
        endSequence: '50'
      });
    });
  });

  describe('error', () => {
    it('455 error', async () => {
      jest.spyOn(cisMemoServices, 'getCISMemos').mockRejectedValue({
        response: {
          status: '455'
        }
      });

      const store = createStore(rootReducer, initialState);
      const response = await getMemoCISRequest({
        storeId,
        postData: {
          accountId: storeId,
          startDate: today,
          endDate: tomorrow
        }
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({
        data: []
      });
    });

    it('455 error', async () => {
      jest.spyOn(cisMemoServices, 'getCISMemos').mockImplementation(() => {
        throw { response: { status: NO_RESULT_CODE } };
      });

      const store = createStore(rootReducer, initialState);
      const response = await getMemoCISRequest({
        storeId,
        postData: {
          accountId: storeId,
          startDate: today,
          endDate: tomorrow
        },
        isLoadMore: true
      } as any)(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({ data: [] });
    });

    it('another error', async () => {
      jest
        .spyOn(cisMemoServices, 'getCISMemos')
        .mockRejectedValue({ ...responseDefault });

      const store = createStore(rootReducer, initialState);
      const response = await getMemoCISRequest({
        storeId,
        postData: {
          accountId: storeId,
          startDate: today,
          endDate: tomorrow
        },
        isLoadMore: true
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({ ...responseDefault });
    });

    it('another error', async () => {
      jest
        .spyOn(cisMemoServices, 'getCISMemos')
        .mockRejectedValue({ ...responseDefault });

      const store = createStore(rootReducer, initialState);
      const response = await getMemoCISRequest({
        storeId,
        postData: {
          accountId: storeId,
          startDate: today,
          endDate: tomorrow
        },
        isLoadMore: true
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({
        config: {},
        headers: {},
        data: null,
        status: 200,
        statusText: ''
      });
    });

    it('another error when raftState[storeId].loadMore no exists', async () => {
      jest
        .spyOn(cisMemoServices, 'getCISMemos')
        .mockRejectedValue({ ...responseDefault });

      const store = createStore(rootReducer, {
        ...initialState,
        cisMemo: { [storeId]: {} }
      });
      const response = await getMemoCISRequest({
        storeId,
        postData: {
          accountId: storeId,
          startDate: today,
          endDate: tomorrow
        },
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({
        config: {},
        headers: {},
        data: null,
        status: 200,
        statusText: ''
      });
    });
  });
});
