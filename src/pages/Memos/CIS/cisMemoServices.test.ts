import cisMemoServices from './cisMemoServices';

// utils
import { mockApiServices, storeId } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import {
  DeleteCISRequestBody,
  GetCISMemosRequestBody,
  AddCISMemoRequestBody,
  UpdateCISMemoRequestBody
} from './types';

describe('cisMemoServices', () => {
  describe('getCISMemos', () => {
    const params: GetCISMemosRequestBody = {
      common: { accountId: storeId },
      selectFields: ['field'],
      startSequence: 'startSequence',
      endSequence: 'endSequence'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cisMemoServices.getCISMemos(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cisMemoServices.getCISMemos(params);

      expect(mockService).toBeCalledWith(apiUrl.memo.getCISMemos, params);
    });
  });

  describe('addCISMemo', () => {
    const params: AddCISMemoRequestBody = {
      common: { accountId: storeId },
      behaviorIdentifier: 'behaviorIdentifier',
      memoText: 'memoText',
      memoCisClerkNumberCollectorIdentifier: ''
    };
    const token = 'token';

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cisMemoServices.addCISMemo(params, token);

      expect(mockService).toBeCalledWith('', params, {
        headers: { Authorization: token }
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cisMemoServices.addCISMemo(params, token);

      expect(mockService).toBeCalledWith(apiUrl.memo.addCISMemo, params, {
        headers: { Authorization: token }
      });
    });
  });

  describe('deleteCISMemo', () => {
    const params: DeleteCISRequestBody = {
      common: { accountId: storeId },
      memoSequenceNumber: '435',
      memoCisClerkNumberCollectorIdentifier: ''
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cisMemoServices.deleteCISMemo(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cisMemoServices.deleteCISMemo(params);

      expect(mockService).toBeCalledWith(apiUrl.memo.deleteCISMemo, params);
    });
  });

  describe('updateCISMemo', () => {
    const params: UpdateCISMemoRequestBody = {
      common: { accountId: storeId },
      memoSequenceNumber: '435',
      memoText: 'memoText',
      memoCisClerkNumberCollectorIdentifier: ''
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cisMemoServices.updateCISMemo(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cisMemoServices.updateCISMemo(params);

      expect(mockService).toBeCalledWith(apiUrl.memo.updateCISMemo, params);
    });
  });

  describe('getCISSource', () => {
    const params: MagicKeyValue = {
      common: { accountId: storeId }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      cisMemoServices.getCISSource(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      cisMemoServices.getCISSource(params);

      expect(mockService).toBeCalledWith(apiUrl.memo.CISSource, params);
    });
  });

  describe('getCISType', () => {
    const params: MagicKeyValue = {
      common: { accountId: storeId }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      cisMemoServices.getCISType(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      cisMemoServices.getCISType(params);

      expect(mockService).toBeCalledWith(apiUrl.memo.CISType, params);
    });
  });
});
