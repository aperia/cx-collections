import React, { useState, useCallback } from 'react';

// Redux
import { useDispatch } from 'react-redux';

// Hooks
import { useAccountDetail, useCustomStoreId } from 'app/hooks';

// Components
import AddSection from 'app/components/AddSection';
import AddMemoCISForm from './AddMemoCISForm';

// Constants
import { MEMO_CIS_TYPE_LIST, MemoCISType } from '../constants';

// Types
import { MemoCISSubmitDataType } from '../types';

// Actions
import { cisMemoActions } from '../_redux/reducers';
import { useTranslation } from 'app/_libraries/_dls/hooks';

const AddCISMemo = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [isOpenAddMemoForm, setIsOpenAddMemoForm] = useState(false);

  const { customStoreId: storeId } = useCustomStoreId();
  const { accEValue } = useAccountDetail();
  const memoCisTypeList = MEMO_CIS_TYPE_LIST.map(item => ({
    ...item,
    text: t(item.text)
  }));

  const handleOpenForm = () => {
    setIsOpenAddMemoForm(true);
  };

  const handleCloseForm = useCallback(() => {
    setIsOpenAddMemoForm(false);
  }, []);

  const handleSubmit = useCallback(
    (data: MemoCISSubmitDataType) => {
      const { memoText, memoType } = data;
      const { operatorID = '' } = window.appConfig?.commonConfig || {};
      dispatch(
        cisMemoActions.triggerAddCISMemoRequest({
          storeId,
          payload: {
            accountId: accEValue,
            memoText,
            memoType,
            memoOperatorIdentifier: operatorID
          },
          onSuccess: () => setIsOpenAddMemoForm(false)
        })
      );
    },
    [dispatch, storeId, accEValue]
  );

  const defaultType = memoCisTypeList.find(
    memoType => memoType.text === MemoCISType.Standard
  );

  return (
    <>
      {!isOpenAddMemoForm ? (
        <AddSection
          id={`${storeId}-add-section-id`}
          text={'txt_enter_memo_here'}
          onClick={handleOpenForm}
          dataTestId="add-memo-CIS-section"
        />
      ) : (
        <AddMemoCISForm
          storeId={storeId}
          types={memoCisTypeList}
          defaultType={defaultType}
          onCancel={handleCloseForm}
          onSubmit={handleSubmit}
        />
      )}
    </>
  );
};

export default AddCISMemo;
