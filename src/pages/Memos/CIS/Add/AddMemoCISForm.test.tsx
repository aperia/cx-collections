import React from 'react';
import { screen } from '@testing-library/react';

// components
import AddMemoCISForm from './AddMemoCISForm';
// hooks
import { AccountDetailProvider, CustomStoreIdProvider } from 'app/hooks';
// helpers
import { renderMockStore, storeId } from 'app/test-utils';

// Actions
import { ADD_MEMO_CIS_VIEWS } from '../constants';
import { MemoCISItemType } from '../types';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const initialState: Partial<RootState> = {
  form: {
    [`${storeId}-${ADD_MEMO_CIS_VIEWS.ID}`]: {
      values: { memoText: 'memoText', memoType: { text: 'text' } }
    }
  }
};

const renderWrapper = (
  initialState: Partial<RootState>,
  onCancel?: jest.Mock,
  onSubmit?: jest.Mock,
  types?: MemoCISItemType[],
  defaultType?: MemoCISItemType
) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <AddMemoCISForm
          storeId={storeId}
          onCancel={onCancel}
          onSubmit={onSubmit}
          types={types}
          defaultType={defaultType}
        />
      </CustomStoreIdProvider>
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('Render', () => {
  it('Render UI', () => {
    jest.useFakeTimers();

    renderWrapper(initialState);

    jest.runAllTimers();

    expect(
      screen.getByTestId(`${ADD_MEMO_CIS_VIEWS.DESCRIPTOR}`)
    ).toBeInTheDocument();
  });

  it('Render UI > with types and defaultType', () => {
    jest.useFakeTimers();

    renderWrapper(
      initialState,
      jest.fn(),
      jest.fn(),
      [{ key: 'ky', text: 'text' }],
      { key: 'ky', text: 'text' }
    );

    jest.runAllTimers();

    expect(
      screen.getByTestId(`${ADD_MEMO_CIS_VIEWS.DESCRIPTOR}`)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  describe('handleCancelButton', () => {
    it('with props onCancel', () => {
      const mockOnCancel = jest.fn();

      renderWrapper(initialState, mockOnCancel);

      screen.getByText(I18N_COMMON_TEXT.CANCEL).click();

      expect(mockOnCancel).toBeCalled();
    });
    it('without props onCancel', () => {
      renderWrapper(initialState);

      screen.getByText(I18N_COMMON_TEXT.CANCEL).click();
    });
  });

  describe('handleSubmitButton', () => {
    it('with props onSubmit', () => {
      const mockOnSubmit = jest.fn();

      renderWrapper(initialState, jest.fn(), mockOnSubmit);

      screen.getByText(I18N_COMMON_TEXT.SUBMIT).click();

      expect(mockOnSubmit).toBeCalled();
    });

    it('without props onSubmit', () => {
      renderWrapper(initialState);

      screen.getByText(I18N_COMMON_TEXT.SUBMIT).click();
    });
  });
});
