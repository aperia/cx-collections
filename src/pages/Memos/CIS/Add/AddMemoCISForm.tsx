import React, { useRef, useEffect, useCallback } from 'react';

// Redux
import { AppState } from 'storeConfig';
import { useSelector } from 'react-redux';
import { Field, isInvalid } from 'redux-form';

// Selectors
import { selectAddCisFormValues } from '../_redux/selectors';

// Hooks
import { useCustomStoreIdSelector } from 'app/hooks';

// Types
import {
  MemoCISSubmitDataType,
  MemoCISItemType,
  AddCisMemoFormValues
} from '../types';

// components
import { Button } from 'app/_libraries/_dls/components';
import { View, ExtraFieldProps } from 'app/_libraries/_dof/core';

// helpers
import isFunction from 'lodash.isfunction';

// constants
import { ADD_MEMO_CIS_VIEWS } from '../constants';

// i18n
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface AddMemoCISFormProps {
  storeId: string;
  types?: MemoCISItemType[];
  defaultType?: MemoCISItemType;
  onCancel?: (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => void;
  onSubmit?: (data: MemoCISSubmitDataType) => void;
}

const AddMemoCISForm: React.FC<AddMemoCISFormProps> = ({
  storeId,
  types,
  defaultType,
  onCancel,
  onSubmit
}) => {
  const viewRef = useRef<any>(null);
  const { t } = useTranslation();

  const isFormInValid = useSelector<AppState, boolean>(
    isInvalid(`${storeId}-${ADD_MEMO_CIS_VIEWS.ID}`)
  );

  const formValues = useCustomStoreIdSelector<AddCisMemoFormValues>(
    selectAddCisFormValues
  );

  const handleCancelButton = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    if (isFunction(onCancel)) onCancel(event);
  };

  const handleSubmitButton = () => {
    if (isFunction(onSubmit)) {
      onSubmit({
        memoType: formValues.memoType.text,
        memoText: formValues.memoContent
      });
    }
  };

  const handleSetDataForMemoTypes = useCallback(() => {
    const memoType: Field<ExtraFieldProps> = viewRef.current?.props.onFind(
      'memoType'
    );
    if (types) {
      memoType?.props?.props.setData(types);
    }
    if (defaultType) {
      memoType?.props?.props.setValue(defaultType);
    }
  }, [types, defaultType]);

  useEffect(() => {
    const immediateId = setImmediate(() => {
      handleSetDataForMemoTypes();
    });

    return () => {
      clearImmediate(immediateId);
    };
  }, [handleSetDataForMemoTypes]);

  return (
    <>
      <div className="py-16 px-24 bg-light-l20 border-bottom">
        <View
          id={`${storeId}-${ADD_MEMO_CIS_VIEWS.ID}`}
          formKey={`${storeId}-${ADD_MEMO_CIS_VIEWS.ID}`}
          descriptor={`${ADD_MEMO_CIS_VIEWS.DESCRIPTOR}`}
          ref={viewRef}
        />
        <div className="d-flex justify-content-end mt-24">
          <Button
            variant="secondary"
            size="sm"
            onClick={handleCancelButton}
            dataTestId="add-memo-CIS_cancel-btn"
          >
            {t(I18N_COMMON_TEXT.CANCEL)}
          </Button>
          <Button
            variant="primary"
            size="sm"
            className="ml-8"
            disabled={isFormInValid}
            onClick={handleSubmitButton}
            dataTestId="add-memo-CIS_submit-btn"
          >
            {t(I18N_COMMON_TEXT.SUBMIT)}
          </Button>
        </div>
      </div>
    </>
  );
};

export default AddMemoCISForm;
