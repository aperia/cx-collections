import React from 'react';
import { screen, fireEvent } from '@testing-library/react';

// components
import AddMemoCIS from './index';
// hooks
import { AccountDetailProvider, CustomStoreIdProvider } from 'app/hooks';
// helpers
import { renderMockStore, storeId, mockActionCreator } from 'app/test-utils';
import { AddMemoCISFormProps } from './AddMemoCISForm';
import { AddSectionProps } from 'app/components/AddSection';
import { AddCISMemoArgs } from '../types';
import { cisMemoActions } from '../_redux/reducers';

jest.mock('app/components/AddSection', () => ({ onClick }: AddSectionProps) => (
  <>
    AddSection
    <button
      data-testid="AddSection_onClick"
      onClick={(e: any) => onClick!(e)}
    />
  </>
));
jest.mock(
  './AddMemoCISForm',
  () =>
    ({ onCancel, onSubmit }: AddMemoCISFormProps) =>
      (
        <>
          AddMemoCISForm
          <button data-testid="AddMemoCISForm_onCancel" onClick={onCancel} />
          <input
            data-testid="AddMemoCISForm_onSubmit"
            onChange={(e: any) => onSubmit!(e.target.data)}
          />
        </>
      )
);

const initialState: Partial<RootState> = {
  accountSearchList: {}
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <AddMemoCIS />
      </CustomStoreIdProvider>
    </AccountDetailProvider>,
    { initialState }
  );
};

const cisMemoSpy = mockActionCreator(cisMemoActions);

describe('Render', () => {
  it('Render UI > AddSection', () => {
    renderWrapper(initialState);

    expect(screen.getByText('AddSection')).toBeInTheDocument();
  });

  it('Render UI > AddSection', () => {
    renderWrapper({});

    screen.getByTestId('AddSection_onClick').click();

    expect(screen.getByText('AddMemoCISForm')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleOpenForm and handleCloseForm', () => {
    renderWrapper({});

    expect(screen.getByText('AddSection')).toBeInTheDocument();

    screen.getByTestId('AddSection_onClick').click();

    expect(screen.getByText('AddMemoCISForm')).toBeInTheDocument();

    screen.getByTestId('AddMemoCISForm_onCancel').click();

    expect(screen.getByText('AddSection')).toBeInTheDocument();
  });

  it('handleSubmit', () => {
    const mockAction = cisMemoSpy('triggerAddCISMemoRequest');

    renderWrapper({});

    screen.getByTestId('AddSection_onClick').click();

    fireEvent.change(screen.getByTestId('AddMemoCISForm_onSubmit'), {
      target: {
        value: 'undefined',
        data: { memoText: 'memoText', memoType: 'memoType' }
      }
    });

    expect(mockAction).toBeCalledWith(
      expect.objectContaining({
        storeId,
        payload: {
          accountId: storeId,
          memoText: 'memoText',
          memoType: 'memoType',
          memoOperatorIdentifier: ''
        }
      })
    );
  });

  it('handleSubmit > onSuccess', () => {
    const mockAction = cisMemoSpy(
      'triggerAddCISMemoRequest',
      jest.fn().mockImplementation(({ onSuccess }: AddCISMemoArgs) => {
        // trigger onSuccess
        onSuccess!();
        return { type: 'type' };
      })
    );

    renderWrapper({});

    screen.getByTestId('AddSection_onClick').click();

    fireEvent.change(screen.getByTestId('AddMemoCISForm_onSubmit'), {
      target: {
        value: 'undefined',
        data: { memoText: 'memoText', memoType: 'memoType' }
      }
    });

    expect(mockAction).toBeCalledWith(
      expect.objectContaining({
        storeId,
        payload: {
          accountId: storeId,
          memoText: 'memoText',
          memoType: 'memoType',
          memoOperatorIdentifier: ''
        }
      })
    );
  });
});

