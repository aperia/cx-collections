import React, {
  useMemo,
  useEffect,
  useCallback,
  useRef,
  useState
} from 'react';

// component
import { SimpleBar } from 'app/_libraries/_dls/components';
import SortCISMemo from './Sort';
import ViewCISMemo, { IMemosCIS } from './View';
import NoMemo from 'app/components/NoMemo';
import AddCISMemo from './Add';
import LoadMoreCISMemo from './LoadMore';
import SearchCISMemo from './Search';
import NoMemoResult from './Search/NoMemoResults';

// redux
import { batch, useDispatch } from 'react-redux';
import {
  selectorIsForceRefresh,
  selectIsDisplayMemos,
  selectIsLoadingMemos,
  selectTotalVisibleMemoCIS,
  selectIsFilteringCISMemo,
  selectMemosCIS
} from './_redux/selectors';
import { selectPinned } from 'pages/__commons/InfoBar/_redux/selectors';
import { actionsInfoBar, InfoBarSection } from 'pages/__commons/InfoBar/_redux';

// Actions
import { cisMemoActions } from './_redux/reducers';

// Hooks
import {
  useAccountDetail,
  useCustomStoreId,
  useCustomStoreIdSelector,
  useMemoProvider
} from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Helpers
import { getScrollParent } from 'app/helpers';
import debounce from 'lodash.debounce';
import classnames from 'classnames';

// Constants
import { I18N_MEMOS } from '../constants';
import TogglePin from 'pages/__commons/InfoBar/TogglePin';
import { useStoreIdSelector } from 'app/hooks/useStoreIdSelector';
import { MemoProps } from '..';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { useApiErrorNotification } from 'pages/ApiErrorNotification/hooks/useApiErrorNotification';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';
import { ITEM_PER_PAGE } from '../Chronicle/constants';

export interface CISMemoProps extends MemoProps {}

const CISMemo: React.FC<CISMemoProps> = ({
  shouldRenderPinnedIcon,
  className
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { accEValue, storeId: infoBarId } = useAccountDetail();
  const { customStoreId: storeId } = useCustomStoreId();

  const ref = useRef<HTMLDivElement | null>(null);
  const addMemoRef = useRef<HTMLDivElement | null>(null);
  const scrollParentRef = useRef<HTMLElement | null>(null);
  const { isHiddenAddSection = false } = useMemoProvider();

  const isDisplayMemos =
    useCustomStoreIdSelector<boolean>(selectIsDisplayMemos);
  const isLoadingMemos =
    useCustomStoreIdSelector<boolean>(selectIsLoadingMemos);
  const isForceRefresh = useCustomStoreIdSelector<boolean>(
    selectorIsForceRefresh
  );
  const totalVisibleMemos = useCustomStoreIdSelector<number>(
    selectTotalVisibleMemoCIS
  );
  const isFilteringCISMemo = useCustomStoreIdSelector<boolean>(
    selectIsFilteringCISMemo
  );
  const memos = useCustomStoreIdSelector<IMemosCIS[]>(selectMemosCIS);
  const pinned = useStoreIdSelector(selectPinned);

  const ApiErrorDetail = useApiErrorNotification({
    storeId,
    forSection: 'inMemoFlyOut'
  });
  const isPinned = pinned === InfoBarSection.Memos;

  const [scrollPosition, setScrollPosition] = useState(0);

  const getMemoCIS = useCallback(() => {
    dispatch(
      cisMemoActions.getMemoCISRequest({
        storeId,
        isLoadMore: false,
        postData: { accountId: accEValue }
      })
    );
  }, [dispatch, storeId, accEValue]);

  const renderMemo = useMemo(() => {
    if (isLoadingMemos) return null;
    if (isFilteringCISMemo && totalVisibleMemos === 0) return <NoMemoResult />;
    if (!isDisplayMemos) return <NoMemo />;

    return <ViewCISMemo />;
  }, [isDisplayMemos, isLoadingMemos, totalVisibleMemos, isFilteringCISMemo]);

  const handleUpdateScrollPosition = debounce(
    () => {
      scrollParentRef.current = getScrollParent(ref.current);
      if (scrollParentRef?.current) {
        setScrollPosition(scrollParentRef.current.scrollTop);
      }
    },
    300,
    { leading: false, trailing: true }
  );

  const handleSetPositionAfterScroll = (event: React.UIEvent<HTMLElement>) => {
    handleUpdateScrollPosition();
  };

  const handleTogglePin = () => {
    batch(() => {
      const thisPinned = isPinned ? null : InfoBarSection.Memos;

      dispatch(
        actionsInfoBar.updatePinned({
          storeId: infoBarId,
          pinned: thisPinned
        })
      );

      if (thisPinned) {
        dispatch(
          actionsInfoBar.updateRecover({ storeId: infoBarId, recover: null })
        );
      }

      dispatch(
        actionsInfoBar.updateIsExpand({
          storeId: infoBarId,
          isExpand: !isPinned
        })
      );
    });
  };

  useEffect(() => {
    getMemoCIS();
  }, [getMemoCIS]);

  useEffect(() => {
    if (isForceRefresh) {
      getMemoCIS();
    }
  }, [isForceRefresh, getMemoCIS]);

  useEffect(() => {
    if (scrollPosition) {
      scrollParentRef.current = getScrollParent(ref.current);
      scrollParentRef.current?.scrollTo({
        top: scrollPosition
      });
    }
  }, [memos, scrollPosition]);

  // fetching ref data
  useEffect(() => {
    dispatch(
      cisMemoActions.getMemoCISRefData({
        storeId,
        postData: { accountId: accEValue }
      })
    );
  }, [accEValue, dispatch, storeId]);

  return (
    <div
      className={classnames(
        'h-100',
        {
          loading: isLoadingMemos && !isHiddenAddSection
        },
        className
      )}
    >
      <SimpleBar onScroll={handleSetPositionAfterScroll}>
        <div ref={ref}>
          {checkPermission(PERMISSIONS.ADD_MEMO, infoBarId) && (
            <div
              ref={addMemoRef}
              className={classnames('comment-info-bar', {
                'd-none': isHiddenAddSection
              })}
            >
              <AddCISMemo />
            </div>
          )}

          <div className="p-24 overlap-next-action">
            <div className="d-flex align-items-center justify-content-between">
              <div className="d-inline-flex">
                <h4
                  className="mr-8"
                  data-testid={genAmtId('chronicle-memo-header', 'title', '')}
                >
                  {t(I18N_MEMOS.MEMOS)}
                </h4>
                {shouldRenderPinnedIcon && (
                  <TogglePin
                    isPinned={isPinned}
                    onToggle={handleTogglePin}
                    dataTestId="CIS-memo-header_pin-btn"
                  />
                )}
              </div>
              <div className="d-flex">
                <SearchCISMemo />
                <SortCISMemo />
              </div>
            </div>
            <h6
              className="mt-4 color-grey-l24 text-uppercase"
              data-testid={genAmtId('chronicle-memo-header', 'CIS-title', '')}
            >
              {t(I18N_MEMOS.CIS)}
            </h6>
            <ApiErrorDetail
              className="mt-24"
              dataTestId="CIS-memo-list_error"
            />
            {renderMemo}
            <LoadMoreCISMemo hideBackToTop={memos?.length < ITEM_PER_PAGE} />
          </div>
        </div>
      </SimpleBar>
    </div>
  );
};

export default CISMemo;
