import React from 'react';

// Components
import {
  DropdownList,
  DropdownBaseChangeEvent
} from 'app/_libraries/_dls/components';

// Redux
import { useDispatch } from 'react-redux';

// Actions
import { cisMemoActions } from '../_redux/reducers';

// Selectors
import { selectSortBy } from '../_redux/selectors';

// Types
import { ISortBy } from '../types';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useCustomStoreId, useCustomStoreIdSelector } from 'app/hooks';

// Helpers
import find from 'lodash.find';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

const data = [
  { value: 'memoDate', description: 'Date' },
  { value: 'memoSequenceNumber', description: 'Sequence Number' },
  { value: 'memoType', description: 'Type' }
];

const SortBy: React.FC = () => {
  const { customStoreId: storeId } = useCustomStoreId();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const value = useCustomStoreIdSelector<ISortBy>(selectSortBy);
  const handleChangeDropDown = (e: DropdownBaseChangeEvent) => {
    const value = e.value;
    dispatch(
      cisMemoActions.changeSortAndOrder({
        storeId,
        fieldName: 'sortBy',
        value
      })
    );
  };

  return (
    <DropdownList
      value={find(data, value)}
      onChange={handleChangeDropDown}
      popupBaseProps={{ popupBaseClassName: 'inside-infobar' }}
      id="value"
      name="sortBy"
      textField="description"
      label={t(I18N_COMMON_TEXT.SORT_BY)}
      dataTestId="CIS-memo-header_sort-by"
      noResult={t('txt_no_results_found')}
    >
      {data.map((item, index) => (
        <DropdownList.Item key={index} label={item.description} value={item} />
      ))}
    </DropdownList>
  );
};

export default SortBy;
