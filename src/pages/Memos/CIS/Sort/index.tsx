import React, { useCallback, useState } from 'react';
import classnames from 'classnames';

// components
import { Popover, Icon, Button, Tooltip } from 'app/_libraries/_dls/components';
import SortBy from './SortBy';
import Footer from './Footer';
import OrderBy from './OrderBy';

// Hooks
import { useCustomStoreId, useCustomStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Redux
import { useDispatch } from 'react-redux';

// Actions
import { cisMemoActions } from '../_redux/reducers';

// Selectors
import { selectIsDisplayIndicator } from '../_redux/selectors';

// Constants
import { I18N_MEMOS } from '../../constants';
import { genAmtId } from 'app/_libraries/_dls/utils';

const SearchSortCISMemo: React.FC = () => {
  const { customStoreId: storeId } = useCustomStoreId();
  const { t } = useTranslation();
  const [isOpen, setOpen] = useState(false);
  const dispatch = useDispatch();

  const isDisplayIndicator = useCustomStoreIdSelector<boolean>(
    selectIsDisplayIndicator
  );
  const handleOpenPopper = useCallback(() => {
    setOpen(!isOpen);
  }, [isOpen]);

  const handleVisibilityChange = useCallback(
    isOpen => {
      if (!isOpen) {
        dispatch(
          cisMemoActions.applySortAndOrder({
            storeId,
            isOriginalToChanging: false
          })
        );
      }
      setOpen(isOpen);
    },
    [dispatch, storeId, setOpen]
  );

  return (
    <Tooltip
      triggerClassName="mr-n4 ml-16"
      placement="top"
      element={t(I18N_MEMOS.SORT_ORDER)}
      variant="primary"
    >
      <Popover
        size="md"
        placement="bottom-end"
        containerClassName="inside-infobar"
        opened={isOpen}
        onVisibilityChange={handleVisibilityChange}
        element={
          <>
            <h4
              className="mb-16"
              data-testid={genAmtId('CIS-memo-header', 'sort-title', '')}
            >{t(I18N_MEMOS.SORT_ORDER)}</h4>
            <SortBy />
            <OrderBy />
            <Footer handleOpenPopper={handleOpenPopper} />
          </>
        }
      >
        <Button
          variant="icon-secondary"
          className={classnames({
            asterisk: isDisplayIndicator,
            active: isOpen
          })}
          onClick={handleOpenPopper}
          dataTestId="CIS-memo-header_sort-btn"
        >
          <Icon name="sort-by" />
        </Button>
      </Popover>
    </Tooltip>
  );
};

export default SearchSortCISMemo;
