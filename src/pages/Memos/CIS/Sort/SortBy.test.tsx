import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

// hooks
import { CustomStoreIdProvider } from 'app/hooks';

// component
import SortByComponent from './SortBy';

// types
import { OrderBy, SortBy } from '../types';

// utils
import {
  renderMockStoreId,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';
import { mockActionCreator, queryByClass } from 'app/test-utils';

//reduce
import { cisMemoActions } from '../_redux/reducers';

const today = new Date();

const initialState: Partial<RootState> = {
  cisMemo: {
    [storeId]: {
      originalSort: {
        orderBy: OrderBy.ASC,
        sortBy: { value: SortBy.MEMO_TYPE, description: 'des' }
      },
      filter: { keyword: 'memo' },
      memos: [
        {
          memoSequenceNumber: '1',
          memoDate: today.toString(),
          memoIdentifier: 1,
          memoOperatorIdentifier: '1',
          memoText: 'This !* MEMO',
          memoType: '',
          type: {}
        },
        {
          memoSequenceNumber: '2',
          memoIdentifier: 1,
          memoText: 'This !* MEMO',
          memoType: '',
          type: {}
        }
      ],
      startSequence: '',
      endSequence: '',
      memoCISRefData: {},
      deleteCISMemo: { loading: false }
    }
  }
};

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(
    <div>
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <SortByComponent />
      </CustomStoreIdProvider>
    </div>,
    { initialState }
  );
};

const cisMemoSpy = mockActionCreator(cisMemoActions);

describe('Render', () => {
  it('Render UI', () => {
    jest.useFakeTimers();
    renderWrapper(initialState);
    jest.runAllTimers();
    expect(screen.getByText('Date')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleChangeDropDown', () => {
    const mockAction = cisMemoSpy('changeSortAndOrder');
    const { wrapper } = renderWrapper(initialState);
    const button = queryByClass(wrapper.container, 'text-field-container');
    userEvent.click(button!);
    const el = screen.getByText('Sequence Number');
    userEvent.click(el!);

    expect(mockAction).toBeCalledWith({
      storeId,
      fieldName: 'sortBy',
      value: {
        description: 'Sequence Number',
        value: 'memoSequenceNumber'
      }
    });
  });
});
