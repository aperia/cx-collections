import React from 'react';

// components
import { Button } from 'app/_libraries/_dls/components';

// Redux
import { useDispatch, batch } from 'react-redux';

// Actions
import { cisMemoActions } from '../_redux/reducers';

// Selectors
import { selectIsSort } from '../_redux/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useCustomStoreIdSelector, useCustomStoreId } from 'app/hooks';

// Types
import { OrderBy, defaultSort } from '../types';

// Constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

interface IFooter {
  handleOpenPopper: () => void;
}

const Footer: React.FC<IFooter> = ({ handleOpenPopper }) => {
  const { t } = useTranslation();
  const { customStoreId: storeId } = useCustomStoreId();
  const dispatch = useDispatch();

  const isSort = useCustomStoreIdSelector<boolean>(selectIsSort);

  const handleApply = () => {
    isSort &&
      dispatch(
        cisMemoActions.applySortAndOrder({
          storeId,
          isOriginalToChanging: true
        })
      );
    handleOpenPopper();
    dispatch(
      apiErrorNotificationAction.clearApiErrorData({
        storeId,
        forSection: 'inMemoFlyOut'
      })
    );
  };

  const resetToDefault = () => {
    batch(() => {
      dispatch(
        cisMemoActions.changeSortAndOrder({
          storeId,
          fieldName: 'orderBy',
          value: OrderBy.DESC
        })
      );
      dispatch(
        cisMemoActions.changeSortAndOrder({
          storeId,
          fieldName: 'sortBy',
          value: defaultSort.sortBy
        })
      );
    });
  };

  return (
    <div className="d-flex mt-24 justify-content-end">
      <Button
        onClick={resetToDefault}
        size="sm"
        variant="secondary"
        dataTestId="CIS-memo-header_sort-by_reset-to-default-btn"
      >
        {t(I18N_COMMON_TEXT.CLEAR_AND_RESET)}
      </Button>
      <Button
        size="sm"
        onClick={handleApply}
        variant="primary"
        dataTestId="CIS-memo-header_sort-by_apply-btn"
      >
        {t(I18N_COMMON_TEXT.APPLY)}
      </Button>
    </div>
  );
};

export default React.memo(Footer);
