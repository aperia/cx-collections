import React from 'react';
import { screen } from '@testing-library/react';

// hooks
import { CustomStoreIdProvider } from 'app/hooks';

// component
import OrderByComponent from './OrderBy';

// types
import { OrderBy, SortBy } from '../types';

// utils
import {
  renderMockStore,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';

// Actions
import { cisMemoActions } from '../_redux/reducers';
import { mockActionCreator } from 'app/test-utils';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

const today = new Date();

const initialState: Partial<RootState> = {
  cisMemo: {
    [storeId]: {
      originalSort: {
        orderBy: OrderBy.ASC,
        sortBy: { value: SortBy.MEMO_TYPE, description: 'des' }
      },
      filter: { keyword: 'memo' },
      memos: [
        {
          memoSequenceNumber: '1',
          memoDate: today.toString(),
          memoIdentifier: 1,
          memoOperatorIdentifier: '1',
          memoText: 'This !* MEMO',
          memoType: '',
          type: {}
        },
        {
          memoSequenceNumber: '2',
          memoIdentifier: 1,
          memoText: 'This !* MEMO',
          memoType: '',
          type: {}
        }
      ],
      startSequence: '',
      endSequence: '',
      memoCISRefData: {},
      deleteCISMemo: { loading: false }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <CustomStoreIdProvider value={{ customStoreId: storeId }}>
      <OrderByComponent />
    </CustomStoreIdProvider>,
    { initialState }
  );
};

const cisMemoSpy = mockActionCreator(cisMemoActions);

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper(initialState);

    expect(
      screen.getByText(`${I18N_COMMON_TEXT.ORDER_BY}:`)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleChangeRadio', () => {
    const mockAction = cisMemoSpy('changeSortAndOrder');

    renderWrapper(initialState);

    screen.getAllByRole('radio')[0].click();

    expect(mockAction).toBeCalledWith({
      storeId,
      fieldName: 'orderBy',
      value: OrderBy.ASC
    });
  });
});
