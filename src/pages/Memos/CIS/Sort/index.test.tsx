import React from 'react';
import { fireEvent, screen } from '@testing-library/react';

// hooks
import { CustomStoreIdProvider } from 'app/hooks';

// component
import SearchSortCISMemo from '.';

// types
import { OrderBy, SortBy } from '../types';

// utils
import {
  renderMockStore,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';

// Actions
import { cisMemoActions } from '../_redux/reducers';
import { mockActionCreator } from 'app/test-utils';
import { I18N_MEMOS } from 'pages/Memos/constants';
import { PopoverProps, TooltipProps } from 'app/_libraries/_dls/components';

jest.mock('./SortBy', () => () => <div>SortBy</div>);
jest.mock('./OrderBy', () => () => <div>OrderBy</div>);
jest.mock('./Footer', () => () => <div>Footer</div>);

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');

  return {
    ...dlsComponents,

    Tooltip: ({ children }: TooltipProps) => <>{children}</>,
    Popover: ({
      opened,
      onVisibilityChange,
      element,
      children
    }: PopoverProps) => (
      <>
        {opened && <p>Popover_opened</p>}
        <input
          data-testid="Popover_handleVisibilityChange"
          onChange={(e: any) => onVisibilityChange!(e.target.isOpen)}
        />
        {element}
        {children}
      </>
    )
  };
});

const today = new Date();

const initialState: Partial<RootState> = {
  cisMemo: {
    [storeId]: {
      originalSort: {
        orderBy: OrderBy.ASC,
        sortBy: { value: SortBy.MEMO_TYPE, description: 'des' }
      },
      filter: { keyword: 'memo' },
      memos: [
        {
          memoSequenceNumber: '1',
          memoDate: today.toString(),
          memoIdentifier: 1,
          memoOperatorIdentifier: '1',
          memoText: 'This !* MEMO',
          memoType: '',
          type: {}
        },
        {
          memoSequenceNumber: '2',
          memoIdentifier: 1,
          memoText: 'This !* MEMO',
          memoType: '',
          type: {}
        }
      ],
      startSequence: '',
      endSequence: '',
      memoCISRefData: {},
      deleteCISMemo: { loading: false }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <CustomStoreIdProvider value={{ customStoreId: storeId }}>
      <SearchSortCISMemo />
    </CustomStoreIdProvider>,
    { initialState }
  );
};

const cisMemoSpy = mockActionCreator(cisMemoActions);

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper(initialState);

    expect(screen.getByText(I18N_MEMOS.SORT_ORDER)).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleOpenPopper', () => {
    renderWrapper(initialState);

    screen.getByRole('button').click();
  });

  describe('handleVisibilityChange', () => {
    it('open popover', () => {
      renderWrapper(initialState);

      fireEvent.change(screen.getByTestId('Popover_handleVisibilityChange'), {
        target: { value: 'undefined', isOpen: true }
      });

      expect(screen.getByText('Popover_opened')).toBeInTheDocument();
    });

    it('close popover', () => {
      const mockAction = cisMemoSpy('applySortAndOrder');

      renderWrapper(initialState);

      fireEvent.change(screen.getByTestId('Popover_handleVisibilityChange'), {
        target: { value: 'undefined', isOpen: false }
      });

      expect(mockAction).toBeCalledWith({
        storeId,
        isOriginalToChanging: false
      });

      expect(screen.queryByText('Popover_opened')).toBeNull();
    });
  });
});
