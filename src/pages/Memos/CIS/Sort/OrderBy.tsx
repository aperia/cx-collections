import React, { useCallback } from 'react';

// components
import { Radio } from 'app/_libraries/_dls/components';

// Redux
import { useDispatch } from 'react-redux';

// Actions
import { cisMemoActions } from '../_redux/reducers';

// Selectors
import { selectOrderBy } from '../_redux/selectors';

// Types
import { OrderBy as OrderByEnum } from '../types';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useCustomStoreId, useCustomStoreIdSelector } from 'app/hooks';

import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';

const OrderBy: React.FC = () => {
  const { customStoreId: storeId } = useCustomStoreId();
  const { t } = useTranslation();

  const dispatch = useDispatch();
  const valueRadio =
    useCustomStoreIdSelector<OrderByEnum.ASC | OrderByEnum.DESC>(selectOrderBy);

  const handleChangeRadio = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const value = e.target.value;
      dispatch(
        cisMemoActions.changeSortAndOrder({
          storeId,
          fieldName: 'orderBy',
          value
        })
      );
    },
    [dispatch, storeId]
  );

  return (
    <div className="d-flex mt-16">
      <p
        className="mr-24 color-grey fs-14 fw-500"
        data-testid={genAmtId('CIS-memo-header', 'order-by', '')}
      >
        {t(I18N_COMMON_TEXT.ORDER_BY)}:
      </p>
      <Radio className="mr-24" dataTestId="CIS-memo-header_order-by-asc">
        <Radio.Input
          onChange={handleChangeRadio}
          checked={valueRadio === OrderByEnum.ASC}
          value={OrderByEnum.ASC}
          name="sortBy"
          id="ASC"
        />
        <Radio.Label>{t(I18N_COMMON_TEXT.ASCENDING)}</Radio.Label>
      </Radio>
      <Radio dataTestId="CIS-memo-header_order-by-desc">
        <Radio.Input
          onChange={handleChangeRadio}
          checked={valueRadio === OrderByEnum.DESC}
          value={OrderByEnum.DESC}
          name="sortBy"
          id="DESC"
        />
        <Radio.Label>{t(I18N_COMMON_TEXT.DESCENDING)}</Radio.Label>
      </Radio>
    </div>
  );
};

export default OrderBy;
