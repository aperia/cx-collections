import React from 'react';
// component
import EditMemoCISModal from './index';
// hooks
import { CustomStoreIdProvider } from 'app/hooks';
import {
  storeId,
  accEValue,
  renderMockStoreId
} from 'app/test-utils/renderComponentWithMockStore';
import { screen } from '@testing-library/react';
import { I18N_MEMOS } from 'pages/Memos/constants';
import { ModalFooterProps } from 'app/_libraries/_dls/components';
import { mockActionCreator } from 'app/test-utils';

// Actions
import { cisMemoActions } from '../_redux/reducers';
import { EDIT_MEMO_CIS_VIEWS } from '../constants';

jest.mock('app/_libraries/_dls/components/Modal', () => {
  const dlsComponents = jest.requireActual(
    'app/_libraries/_dls/components/Modal'
  );

  return {
    ...dlsComponents,
    ModalFooter: ({ onOk }: ModalFooterProps) => (
      <>
        <button data-testid="ModalFooter_onOk" onClick={onOk} />
      </>
    )
  };
});

const initialState: Partial<RootState> = {
  form: {
    [`${storeId}-${EDIT_MEMO_CIS_VIEWS.ID}`]: {
      values: { memoText: 'memoText', memoType: { text: 'text' } }
    }
  },
  cisMemo: {
    [storeId]: {
      memos: [
        {
          memoDate: 'string',
          memoIdentifier: 0,
          memoOperatorIdentifier: '0',
          memoSequenceNumber: '0',
          memoText: 'string',
          memoType: '',
          type: { text: 'text' }
        }
      ],
      hasLoadMore: false,
      startSequence: '1',
      endSequence: '50',
      loadMore: false,
      loading: false,
      isForceRefresh: false,
      memoCISRefData: {
        isLoading: false
      },
      deleteCISMemo: {
        loading: false
      },
      editModalLoading: false,
      isUpdateScrollPosition: false
    }
  }
};

const dataDefault = {
  memoDate: 'string',
  memoIdentifier: 0,
  memoOperatorIdentifier: '0',
  memoText: 'string',
  memoType: 'string',
  memoSource: 'string',
  type: {},
  source: {}
};

const renderWrapper = (
  initialState: Partial<RootState>,
  isOpen = true,
  data = dataDefault
) => {
  return renderMockStoreId(
    <CustomStoreIdProvider value={{ customStoreId: storeId }}>
      <EditMemoCISModal opened={isOpen} data={data} onCancel={jest.fn()} />
    </CustomStoreIdProvider>,
    { initialState }
  );
};

const cisMemoSpy = mockActionCreator(cisMemoActions);

describe('Render', () => {
  it('Render UI', () => {
    jest.useFakeTimers();

    renderWrapper(initialState);

    jest.runAllTimers();

    expect(screen.getByText(I18N_MEMOS.EDIT)).toBeInTheDocument();
  });

  it('Render UI => empty data', () => {
    jest.useFakeTimers();

    renderWrapper(initialState, true, {});

    jest.runAllTimers();

    expect(screen.getByText(I18N_MEMOS.EDIT)).toBeInTheDocument();
  });

  it('Render UI > not open modal', () => {
    jest.useFakeTimers();

    renderWrapper(initialState, false);

    jest.runAllTimers();

    expect(screen.queryByText(I18N_MEMOS.EDIT)).toBeNull();
  });
});

describe('Actions', () => {
  it('onOk', () => {
    const mockAction = cisMemoSpy('triggerUpdateCISMemoRequest');

    renderWrapper(initialState);

    screen.getByTestId('ModalFooter_onOk').click();

    expect(mockAction).toBeCalledWith(
      expect.objectContaining({
        storeId,
        payload: {
          accountId: accEValue,
          memoCisClerkNumberCollectorIdentifier: 'NSA',
          memoSequenceNumber: '',
          memoText: 'memoText',
          memoType: 'text'
        }
      })
    );
  });
});
