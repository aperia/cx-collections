import React, { useRef, useEffect, useMemo } from 'react';

// components
import { View, ExtraFieldProps } from 'app/_libraries/_dof/core';
import {
  Modal,
  ModalHeader,
  ModalTitle,
  ModalFooter
} from 'app/_libraries/_dls/components';
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';

// Redux
import { useDispatch, useSelector } from 'react-redux';
import { Field, isInvalid } from 'redux-form';
import { AppState } from 'storeConfig';
import {
  selectEditCisFormValues,
  selectEditModalLoading
} from '../_redux/selectors';
import { cisMemoActions } from '../_redux/reducers';

// Types
import { EditCisMemoFormValues, IMemoCIS } from '../types';

// Hooks
import {
  useCustomStoreIdSelector,
  useCustomStoreId,
  useAccountDetail
} from 'app/hooks';

// Constants
import { EDIT_MEMO_CIS_VIEWS, MEMO_CIS_TYPE_LIST } from '../constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { I18N_MEMOS } from '../../constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useCheckMemoText } from 'pages/Memos/hooks/useCheckMemoText';
import { useDisableSubmitButton } from 'app/hooks/useDisableSubmitButton';

export interface EditMemoData {
  memoType: string;
  memoContent: string;
}

export interface EditMemoCISModalProps {
  opened: boolean;
  data?: IMemoCIS;
  onCancel?: () => void;
  onSave?: (data: EditMemoData) => void;
}

const EditMemoCISModal: React.FC<EditMemoCISModalProps> = ({
  opened,
  data,
  onCancel
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { customStoreId: storeId } = useCustomStoreId();
  const { accEValue } = useAccountDetail();
  const memoCisTypeList = useMemo(
    () =>
      MEMO_CIS_TYPE_LIST.map(item => ({
        ...item,
        text: t(item.text)
      })),
    [t]
  );

  const viewRef = useRef<any>();
  useCheckMemoText(viewRef, data ? data?.memoText : '', opened);

  const isFormInvalid = useSelector<AppState, boolean>(
    isInvalid(`${storeId}-${EDIT_MEMO_CIS_VIEWS.ID}`)
  );

  const editModalLoading = useCustomStoreIdSelector<boolean>(
    selectEditModalLoading
  );

  const formValues: any = useCustomStoreIdSelector<EditCisMemoFormValues>(
    selectEditCisFormValues
  );

  const { disableSubmit, setPrimitiveValues } = useDisableSubmitButton({
    memoText: formValues.memoText,
    memoType: formValues.memoType?.text
  });

  const handleSaveButton = () => {
    dispatch(
      cisMemoActions.triggerUpdateCISMemoRequest({
        storeId,
        payload: {
          accountId: accEValue,
          memoText: formValues.memoText,
          memoType: formValues.memoType.text,
          memoCisClerkNumberCollectorIdentifier: 'NSA',
          memoSequenceNumber: data?.memoSequenceNumber || ''
        },
        onSuccess: onCancel
      })
    );
  };

  useEffect(() => {
    if (!opened) return;
    setPrimitiveValues({
      memoText: data?.memoText,
      memoType: memoCisTypeList.find(item => item.text === data?.memoType)?.text
    });
  }, [
    data?.memoText,
    data?.memoType,
    memoCisTypeList,
    opened,
    setPrimitiveValues
  ]);

  useEffect(() => {
    if (!opened) return;
    const immediateId = setImmediate(() => {
      const memoType: Field<ExtraFieldProps> =
        viewRef.current?.props.onFind('memoType');

      memoType?.props?.props.setData(memoCisTypeList);
      // Help show selected value on DropdownList DLS
      // Value on DropdownList DLS must be reference from Data value
      // Ex: Data -> memoCisTypeList; Value -> memoCisTypeList[0]
      if (data?.memoType) {
        const memoTypeValue = memoCisTypeList.find(
          item => item.text === data.memoType
        );
        memoType?.props?.props.setValue(memoTypeValue);
      }

      const memoText: Field<ExtraFieldProps> =
        viewRef.current?.props.onFind('memoText');

      memoText?.props?.props.setValue(data?.memoText);
    });

    return () => {
      clearImmediate(immediateId);
    };
  }, [opened, data, memoCisTypeList]);

  return (
    <Modal
      id={`${storeId}-edit-cis-memo-modal`}
      xs
      show={opened}
      loading={editModalLoading}
      dataTestId="edit-memo-CIS-modal-modal"
    >
      <ModalHeader
        border
        closeButton
        onHide={onCancel}
        dataTestId="edit-memo-CIS-modal-header"
      >
        <ModalTitle dataTestId="edit-memo-CIS-modal-header-title">
          {t(I18N_MEMOS.EDIT)}
        </ModalTitle>
      </ModalHeader>
      <ModalBodyWithApiError
        apiErrorClassName="mb-24"
        storeId={storeId}
        dataTestId="edit-memo-CIS-modal-body"
      >
        <View
          id={`${storeId}-${EDIT_MEMO_CIS_VIEWS.ID}`}
          formKey={`${storeId}-${EDIT_MEMO_CIS_VIEWS.ID}`}
          descriptor={`${EDIT_MEMO_CIS_VIEWS.DESCRIPTOR}`}
          ref={viewRef}
        />
      </ModalBodyWithApiError>
      <ModalFooter
        okButtonText={t(I18N_COMMON_TEXT.SAVE)}
        cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
        onCancel={onCancel}
        onOk={handleSaveButton}
        disabledOk={isFormInvalid || disableSubmit}
        dataTestId="edit-memo-CIS-modal-footer"
      />
    </Modal>
  );
};

export default EditMemoCISModal;
