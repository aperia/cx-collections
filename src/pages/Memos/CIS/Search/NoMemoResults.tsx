import React from 'react';

// components
import { Icon, Button } from 'app/_libraries/_dls/components';

// Hooks
import { useHandleResetSearch } from './useResetSearch';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Constants
import { I18N_MEMOS } from '../../constants';

// constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';

const NoMemoResult: React.FC = () => {
  const handleResetSearch = useHandleResetSearch();
  const { t } = useTranslation();
  return (
    <div className="mt-40 d-flex justify-content-center">
      <div className="d-flex flex-column align-items-center">
        <Icon
          className="color-light-l12 fs-80"
          name="comment"
          dataTestId="CIS-memo-list_comment-icon"
        />
        <p
          className="fs-14 color-grey mt-16"
          data-testid={genAmtId('CIS-memo-list', 'no-results-found', '')}
        >
          {t(I18N_MEMOS.NO_RESULTS_FOUND)}
        </p>
        <Button
          className="mt-24"
          size="sm"
          variant="outline-primary"
          onClick={handleResetSearch}
          dataTestId="CIS-memo-list_reset-search-btn"
        >
          {t(I18N_COMMON_TEXT.CLEAR_AND_RESET)}
        </Button>
      </div>
    </div>
  );
};

export default NoMemoResult;
