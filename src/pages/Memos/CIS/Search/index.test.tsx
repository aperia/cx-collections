import { queryByClass, renderMockStoreId } from 'app/test-utils';
import React from 'react';
import SearchCISMemo from './index';
import { screen } from '@testing-library/react';
import { PopoverProps } from 'app/_libraries/_dls/components';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');
  const { Popover } = dlsComponents;

  return {
    ...dlsComponents,
    Popover: (props: PopoverProps) => (
      <>
        <div
          data-testid="onVisibilityChange"
          onClick={() => props.onVisibilityChange!(true)}
        />
        <Popover opened {...props} />
      </>
    )
  };
});

describe('Test Search CIS', () => {
  it('check button is clicked', () => {
    const { wrapper } = renderMockStoreId(<SearchCISMemo />);

    const element = queryByClass(wrapper.container, /btn btn-icon-secondary/);
    element?.click();

    expect(
      queryByClass(wrapper.container, /btn btn-icon-secondary active/)
    ).toBeInTheDocument();
  });

  it('run handleVisibilityChange function', () => {
    const { wrapper } = renderMockStoreId(<SearchCISMemo />);

    const div = screen.getByTestId('onVisibilityChange');
    div.click();

    expect(
      queryByClass(wrapper.container, /btn btn-icon-secondary active/)
    ).toBeInTheDocument();
  });
});
