import React from 'react';
import { storeId, mockActionCreator, renderMockStoreId } from 'app/test-utils';
import NoMemoResult from './NoMemoResults';
import { screen } from '@testing-library/react';
import { cisMemoActions } from '../_redux/reducers';
import { initialSearchValues } from './Body';
import { CustomStoreIdProvider } from 'app/hooks';

const cisMemoActionsSpy = mockActionCreator(cisMemoActions);

jest.mock('./Body', () => ({
  initialSearchValues: {
    keyword: '',
    sequenceNumber: '',
    type: null,
    specificDate: true,
    dateRange: false,
    specificDateValue: 'Mon Aug 30 2021 11:44:30 GMT+0700 (Indochina Time)',
    dateRangeValue: {
      start: 'Mon Aug 30 2021 11:44:30 GMT+0700 (Indochina Time)',
      end: 'Mon Aug 30 2021 11:44:30 GMT+0700 (Indochina Time)'
    }
  }
}));

describe('Test NoMemoResult component', () => {
  it('should render component', () => {
    renderMockStoreId(<NoMemoResult />);

    expect(screen.getByText('txt_memos_no_results_found')).toBeInTheDocument();
    expect(screen.getByText('txt_clear_and_reset'));
  });

  it('clicks clear and reset button', () => {
    const searchAndFilterSpy = cisMemoActionsSpy('searchAndFilter');

    renderMockStoreId(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <NoMemoResult />
      </CustomStoreIdProvider>,
      {
        initialState: {
          cisMemo: {
            [storeId]: {
              filter: {
                specificDateValue:
                  'Mon Aug 30 2021 11:44:30 GMT+0700 (Indochina Time)',
                dateRangeValue: {
                  start: 'Mon Aug 29 2021 11:44:30 GMT+0700 (Indochina Time)',
                  end: 'Mon Aug 30 2021 11:44:30 GMT+0700 (Indochina Time)'
                }
              }
            }
          }
        }
      }
    );

    const clearAndResetBtn = screen.getByText('txt_clear_and_reset');
    clearAndResetBtn?.click();

    expect(searchAndFilterSpy).toBeCalledWith({
      storeId,
      ...initialSearchValues
    });
  });
});
