import React, { FC, useState } from 'react';

// components
import { Button, Icon, Popover, Tooltip } from 'app/_libraries/_dls/components';
import Body from './Body';

// Selectors
import { selectIsFilteringCISMemo } from '../_redux/selectors';

// Hooks
import { useCustomStoreIdSelector } from 'app/hooks';

// Types
import { ISearchCISMemo } from '../types';

// Helpers
import classNames from 'classnames';

const SearchCISMemo: FC<ISearchCISMemo> = () => {
  const [opened, setOpened] = useState(false);

  const handleTogglePopover = () => {
    setOpened(opened => !opened);
  };

  const handleVisibilityChange = (opened: boolean) => setOpened(opened);

  const isFilteringCISMemo = useCustomStoreIdSelector<boolean>(
    selectIsFilteringCISMemo
  );

  return (
    <Tooltip
      placement="top"
      element="Search"
      variant="primary"
      dataTestId="CIS-memo-header_search-tooltip"
    >
      <Popover
        size="md"
        placement="bottom-end"
        containerClassName="inside-infobar"
        opened={opened}
        onVisibilityChange={handleVisibilityChange}
        element={<Body handleTogglePopover={handleTogglePopover} />}
        dataTestId="CIS-memo-header_search"
      >
        <Button
          variant="icon-secondary"
          onClick={handleTogglePopover}
          className={classNames({
            asterisk: isFilteringCISMemo,
            active: opened
          })}
          dataTestId="CIS-memo-header_search-btn"
        >
          <Icon name="search" dataTestId="CIS-memo-header_search-icon" />
        </Button>
      </Popover>
    </Tooltip>
  );
};

export default SearchCISMemo;
