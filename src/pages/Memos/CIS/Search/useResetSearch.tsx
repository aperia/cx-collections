// Redux
import { batch, useDispatch } from 'react-redux';
import { cisMemoActions } from '../_redux/reducers';
import { selectFilterCriteria } from '../_redux/selectors';

// Hooks
import {
  useCustomStoreId,
  useCustomStoreIdSelector,
  useAccountDetail
} from 'app/hooks';

// Constants
import { initialSearchValues } from '../Search/Body';

// Types
import { SearchFormValues } from '../types';

// Helpers
import isEqual from 'lodash.isequal';

export const useHandleResetSearch = () => {
  const dispatch = useDispatch();

  const { customStoreId: storeId } = useCustomStoreId();
  const { accEValue } = useAccountDetail();

  const filterCriteria = useCustomStoreIdSelector<SearchFormValues>(
    selectFilterCriteria
  );

  return () =>
    batch(() => {
      dispatch(
        cisMemoActions.searchAndFilter({
          storeId,
          ...initialSearchValues
        })
      );

      // only fetch api if date change
      const shouldFetchNewData =
        initialSearchValues.specificDateValue !==
          filterCriteria.specificDateValue ||
        !isEqual(
          initialSearchValues.dateRangeValue,
          filterCriteria.dateRangeValue
        );
      shouldFetchNewData &&
        dispatch(
          cisMemoActions.getMemoCISRequest({
            storeId,
            postData: { accountId: accEValue },
            isSearch: true
          })
        );
    });
};
