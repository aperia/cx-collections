import React, { ChangeEvent, FC, useEffect, useState } from 'react';

// Components
import {
  Button,
  DatePicker,
  DropdownList,
  Radio,
  TextBox,
  DateRangePicker
} from 'app/_libraries/_dls/components';

// Redux
import { batch, useDispatch } from 'react-redux';
import { cisMemoActions } from '../_redux/reducers';

// Selectors
import { selectFilterCriteria, selectTypeRefData } from '../_redux/selectors';

// Hooks
import {
  useCustomStoreId,
  useCustomStoreIdSelector,
  useAccountDetail
} from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useFormik } from 'formik';

// Types
import { SearchFormValues, IContent } from '../types';

// Helpers
import isEmpty from 'lodash.isempty';
import isEqual from 'lodash.isequal';
import { stringValidate } from 'app/helpers';
import { I18N_MEMOS } from '../../constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export const initialSearchValues: SearchFormValues = {
  keyword: '',
  sequenceNumber: '',
  type: null,
  specificDate: true,
  dateRange: false
};

const Body: FC<IContent> = ({ handleTogglePopover }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { customStoreId: storeId } = useCustomStoreId();
  const { accEValue } = useAccountDetail();

  const [disabledSubmit, setDisabledSubmit] = useState(true);

  const typeRefData =
    useCustomStoreIdSelector<Record<string, string>[]>(selectTypeRefData);

  const filterCriteria =
    useCustomStoreIdSelector<SearchFormValues>(selectFilterCriteria);

  const {
    values,
    setValues,
    isValid,
    submitForm,
    handleChange,
    handleBlur,
    errors,
    touched
  } = useFormik({
    initialValues: !isEmpty(filterCriteria)
      ? filterCriteria
      : initialSearchValues,
    onSubmit: (values: SearchFormValues) => {
      const { specificDate, specificDateValue, dateRange, dateRangeValue } =
        values;
      let startDate: Date | undefined, endDate: Date | undefined;
      if (specificDate) {
        startDate = endDate = specificDateValue;
      }
      if (dateRange) {
        startDate = dateRangeValue?.start;
        endDate = dateRangeValue?.end;
      }
      batch(() => {
        // only fetch api if date filter is changed
        const shouldFetchNewData =
          specificDateValue !== filterCriteria.specificDateValue ||
          !isEqual(dateRangeValue, filterCriteria.dateRangeValue);
        if (shouldFetchNewData)
          dispatch(
            cisMemoActions.getMemoCISRequest({
              storeId,
              postData: {
                accountId: accEValue,
                startDate: startDate?.toString(),
                endDate: endDate?.toString()
              },
              isSearch: true
            })
          );

        const newValues = { ...values };
        if (!newValues?.dateRangeValue && !newValues?.specificDateValue) {
          newValues.specificDate = true;
          newValues.dateRange = false;
        }
        newValues.sequenceNumber = values.sequenceNumber || '';

        dispatch(
          cisMemoActions.searchAndFilter({
            storeId,
            ...newValues
          })
        );
      });
      handleTogglePopover();
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inMemoFlyOut'
        })
      );
    },
    validateOnMount: true,
    validateOnBlur: true,
    validateOnChange: true,
    validate: (values: SearchFormValues) => {
      handleValidateForm(values);
      const { dateRangeValue } = values;
      if (
        (!dateRangeValue?.start && dateRangeValue?.end) ||
        (dateRangeValue?.start && !dateRangeValue?.end)
      ) {
        return {
          dateRangeValue: t(I18N_MEMOS.VALIDATE_DATE_RANGE)
        };
      }
    }
  });

  const handleReset = () => {
    setValues(initialSearchValues);
    if (!isEqual(filterCriteria, initialSearchValues)) {
      setDisabledSubmit(false);
    }
  };

  const handleValidateForm = (values: SearchFormValues) => {
    if (!isEqual(filterCriteria, initialSearchValues)) {
      return setDisabledSubmit(false);
    }

    const { keyword, sequenceNumber, type } = values;
    const specificDateValid = values.specificDate
      ? !!values.specificDateValue
      : false;
    const dateRangeValid = values.dateRange ? !!values.dateRangeValue : false;

    const isDisabled =
      !keyword &&
      !sequenceNumber &&
      !type &&
      !specificDateValid &&
      !dateRangeValid;
    setDisabledSubmit(isDisabled);
  };

  const handleChangeDate = (name: string) => {
    setValues({
      ...values,
      specificDate: false,
      dateRange: false,
      specificDateValue: undefined,
      dateRangeValue: undefined,
      [name]: true
    });
  };

  const handleChangeSequenceNumber = (e: ChangeEvent<HTMLInputElement>) => {
    if (!stringValidate(e.target.value).isNumber()) return;

    handleChange(e);
  };

  const handleChangeDateType = (dateType: string) => {
    // Handle error if close popup date immediately
    setTimeout(() => {
      handleChangeDate(dateType);
    }, 300);
  };

  // fetching ref data
  useEffect(() => {
    dispatch(
      cisMemoActions.getMemoCISRefData({
        storeId,
        postData: { accountId: accEValue }
      })
    );
  }, [accEValue, dispatch, storeId]);

  return (
    <div className="inside-infobar">
      <h4
        className="mb-16"
        data-testid={genAmtId('CIS-memo-header_search', 'title', '')}
      >
        {t(I18N_COMMON_TEXT.SEARCH)}
      </h4>
      <div className="row">
        <div className="col-12">
          <TextBox
            name="keyword"
            label={t(I18N_MEMOS.KEYWORD)}
            className="mb-16"
            value={values.keyword}
            onChange={handleChange}
            maxLength={50}
            dataTestId="CIS-memo-header_search_keyword"
          />
        </div>
      </div>
      <div className="row">
        <div className="col-12 mb-16">
          <TextBox
            name="sequenceNumber"
            label={t(I18N_MEMOS.SEQUENCE_NUMBER)}
            value={values.sequenceNumber || ''}
            onChange={handleChangeSequenceNumber}
            maxLength={9}
            autoComplete="off"
            datatype=""
            dataTestId="CIS-memo-header_search_sequenceNumber"
          />
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          <DropdownList
            name="type"
            textField="description"
            label={t(I18N_COMMON_TEXT.TYPE)}
            onBlur={handleBlur}
            value={values.type}
            onChange={handleChange}
            popupBaseProps={{
              popupBaseClassName: 'inside-infobar'
            }}
            dataTestId="CIS-memo-header_search_description"
            noResult={t('txt_no_results_found')}
          >
            {typeRefData?.map(item => (
              <DropdownList.Item
                key={item.value}
                label={item.description}
                value={item}
              />
            ))}
          </DropdownList>
        </div>
      </div>
      <div className="row my-16">
        <div className="col-12">
          <span className="mr-24 fw-500 color-grey">
            {t(I18N_COMMON_TEXT.DATE)}:
          </span>
          <Radio
            className="d-inline-block mr-24"
            dataTestId="CIS-memo-header_search_specificDate"
          >
            <Radio.Input
              name="date"
              checked={values.specificDate}
              onChange={() => handleChangeDateType('specificDate')}
            ></Radio.Input>
            <Radio.Label>{t(I18N_COMMON_TEXT.SPECIFIC_DATE)}</Radio.Label>
          </Radio>
          <Radio
            className="d-inline-block"
            dataTestId="CIS-memo-header_search_dateRange"
          >
            <Radio.Input
              name="date"
              checked={values.dateRange}
              onChange={() => handleChangeDateType('dateRange')}
            ></Radio.Input>
            <Radio.Label>{t(I18N_COMMON_TEXT.DATE_RANGE)}</Radio.Label>
          </Radio>
        </div>
      </div>
      <div className="row">
        <div className="col-12">
          {values.specificDate ? (
            <DatePicker
              name="specificDateValue"
              popupBaseProps={{
                popupBaseClassName: 'inside-infobar'
              }}
              label={t(I18N_COMMON_TEXT.DATE)}
              value={values.specificDateValue}
              onChange={handleChange}
              maxDate={new Date()}
              dataTestId="CIS-memo-header_search_specificDateValue"
            />
          ) : (
            <DateRangePicker
              name="dateRangeValue"
              popupBaseProps={{
                popupBaseClassName: 'inside-infobar'
              }}
              label={t(I18N_COMMON_TEXT.DATE)}
              value={values.dateRangeValue}
              onChange={handleChange}
              onBlur={handleBlur}
              error={{
                message: errors.dateRangeValue as string,
                status: touched.dateRangeValue && !!errors.dateRangeValue
              }}
              maxDate={new Date()}
              dataTestId="CIS-memo-header_search_dateRangeValue"
            />
          )}
        </div>
      </div>
      <div className="d-flex justify-content-end mt-24">
        <Button
          className="ml-8"
          size="sm"
          variant="secondary"
          onClick={handleReset}
          dataTestId="CIS-memo-header_search_reset-to-default-btn"
        >
          {t(I18N_MEMOS.RESET_TO_DEFAULT)}
        </Button>
        <Button
          size="sm"
          onClick={submitForm}
          disabled={!isValid || disabledSubmit}
          dataTestId="CIS-memo-header_search_apply-btn"
        >
          {t(I18N_COMMON_TEXT.APPLY)}
        </Button>
      </div>
    </div>
  );
};

export default Body;
