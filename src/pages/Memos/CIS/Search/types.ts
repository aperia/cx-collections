import { DateRangePickerValue } from 'app/_libraries/_dls/components';

export interface ISearchCISMemo {}

export interface IContent {
  handleTogglePopover: () => void;
}

export interface SearchFormValues {
  keyword?: string;
  sequenceNumber?: number;
  type?: Record<string, string> | null;
  specificDate?: boolean;
  dateRange?: boolean;
  specificDateValue?: Date;
  dateRangeValue?: DateRangePickerValue;
}
