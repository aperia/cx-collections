import React from 'react';
import Body, { initialSearchValues } from './Body';
import {
  mockActionCreator,
  renderMockStore,
  storeId,
  accEValue,
  waitFakeTime
} from 'app/test-utils';
import { AccountDetailProvider, CustomStoreIdProvider } from 'app/hooks';
import { IInitialStateMemoCIS, OrderBy } from '../types';
import { fireEvent, screen, waitFor } from '@testing-library/react';
import {
  DatePickerChangeEvent,
  DatePickerProps,
  DateRangePickerChangeEvent,
  DateRangePickerProps,
  TextBoxProps
} from 'app/_libraries/_dls/components';

import { cisMemoActions } from '../_redux/reducers';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { I18N_MEMOS } from 'pages/Memos/constants';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');

  return {
    ...dlsComponents,
    TextBox: ({ onChange, label }: TextBoxProps) => {
      return (
        <>
          <input
            name="savingsAccountNumber"
            data-testid={`TextBox_onChange_${label}`}
            onChange={onChange}
          />
        </>
      );
    },
    DatePicker: ({ onChange }: DatePickerProps) => (
      <>
        <input
          data-testid="DatePicker_onChange"
          onChange={(e: any) =>
            onChange!({
              target: { value: e.target.date, name: e.target.name }
            } as DatePickerChangeEvent)
          }
        />
      </>
    ),
    DateRangePicker: ({ onChange }: DateRangePickerProps) => (
      <>
        <input
          data-testid="DateRangePicker_onChange"
          onChange={(e: any) =>
            onChange!({
              target: { name: e.target.name, value: e.target.range }
            } as DateRangePickerChangeEvent)
          }
        />
      </>
    )
  };
});

export interface MockInterface {
  cisMemo: IInitialStateMemoCIS;
}

const initialState: Partial<RootState> = {
  cisMemo: {
    [storeId]: {
      loading: true,
      loadMore: false,
      hasLoadMore: false,
      endSequence: '50',
      startSequence: '1',
      filter: initialSearchValues,
      originalSort: {
        orderBy: OrderBy.DESC,
        sortBy: undefined
      },
      changingSort: {
        orderBy: OrderBy.DESC,
        sortBy: undefined
      },
      memos: [],
      memoCISRefData: {
        sourceRefData: [],
        typeRefData: [{ value: 'value', description: 'des' }]
      }
    }
  }
};

const stateWithSpecificFilter: Partial<RootState> = {
  cisMemo: {
    [storeId]: {
      loading: true,
      loadMore: false,
      hasLoadMore: false,
      endSequence: '50',
      startSequence: '1',
      filter: { specificDate: false, dateRange: false },
      originalSort: {
        orderBy: OrderBy.DESC,
        sortBy: undefined
      },
      changingSort: {
        orderBy: OrderBy.DESC,
        sortBy: undefined
      },
      memos: [],
      memoCISRefData: {
        sourceRefData: []
      }
    }
  }
};

const renderWrapper = (
  initialState: Partial<RootState>,
  handleTogglePopover?: jest.Mock
) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <Body handleTogglePopover={handleTogglePopover || jest.fn()} />
      </CustomStoreIdProvider>
    </AccountDetailProvider>,
    { initialState }
  );
};

const cisMemoSpy = mockActionCreator(cisMemoActions);

describe('Render', () => {
  it('Render UI', () => {
    const mockAction = cisMemoSpy('getMemoCISRefData');
    jest.useFakeTimers();
    renderWrapper(initialState);
    jest.runAllTimers();
    expect(mockAction).toBeCalledWith({
      storeId,
      postData: { accountId: accEValue }
    });
    expect(screen.getByText(I18N_COMMON_TEXT.SEARCH)).toBeInTheDocument();
  });

  it('Render UI with empty init filter', () => {
    const mockAction = cisMemoSpy('getMemoCISRefData');
    jest.useFakeTimers();
    renderWrapper({
      cisMemo: {
        [storeId]: {
          loading: true,
          loadMore: false,
          hasLoadMore: false,
          endSequence: '50',
          startSequence: '1',
          originalSort: {
            orderBy: OrderBy.DESC,
            sortBy: undefined
          },
          changingSort: {
            orderBy: OrderBy.DESC,
            sortBy: undefined
          },
          memos: [],
          memoCISRefData: {
            sourceRefData: [],
            typeRefData: [{ value: 'value', description: 'des' }]
          }
        }
      }
    });
    jest.runAllTimers();
    expect(mockAction).toBeCalledWith({
      storeId,
      postData: { accountId: accEValue }
    });
    expect(screen.getByText(I18N_COMMON_TEXT.SEARCH)).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleChangeDateType', () => {
    jest.useFakeTimers();
    renderWrapper(initialState);
    jest.runAllTimers();

    waitFakeTime(() => screen.getAllByRole('radio')[1].click());
    // expect
  });

  describe('handleChangeSequenceNumber', () => {
    it('input number', () => {
      jest.useFakeTimers();
      renderWrapper(initialState);
      jest.runAllTimers();
      fireEvent.change(
        screen.getByTestId(`TextBox_onChange_${I18N_MEMOS.SEQUENCE_NUMBER}`),
        { target: { value: 123 } }
      );
      // expect
    });
    it('input string', () => {
      jest.useFakeTimers();
      renderWrapper(initialState);
      jest.runAllTimers();
      fireEvent.change(
        screen.getByTestId(`TextBox_onChange_${I18N_MEMOS.SEQUENCE_NUMBER}`),
        { target: { value: 'abc' } }
      );
      // expect
    });
  });

  describe('handleReset', () => {
    it('reset with empty filter', () => {
      jest.useFakeTimers();
      renderWrapper(initialState);
      jest.runAllTimers();
      screen.getByText(I18N_MEMOS.RESET_TO_DEFAULT).click();
      // expect
    });
    it('reset with default filter', () => {
      renderWrapper(stateWithSpecificFilter);
      screen.getByText(I18N_MEMOS.RESET_TO_DEFAULT).click();
      // expect
    });
  });

  describe('submitForm', () => {
    it('without value', () => {
      jest.useFakeTimers();
      renderWrapper(stateWithSpecificFilter);
      jest.runAllTimers();

      screen.getByText(I18N_COMMON_TEXT.APPLY).click();
      // expect
    });

    it('specificDate', () => {
      const mockAction = cisMemoSpy('getMemoCISRequest');
      jest.useFakeTimers();
      renderWrapper(stateWithSpecificFilter);
      jest.runAllTimers();

      waitFakeTime(() => screen.getAllByRole('radio')[0].click());

      fireEvent.change(screen.getByTestId('DatePicker_onChange'), {
        target: {
          value: 'undefined',
          name: 'specificDateValue',
          date: new Date()
        }
      });

      screen.getByText(I18N_COMMON_TEXT.APPLY).click();

      waitFor(() => {
        expect(mockAction).toBeCalledWith({
          storeId,
          postData: { accountId: accEValue },
          isSearch: true
        });
      });
    });

    describe('dateRange', () => {
      it('invalidation', () => {
        const mockAction = cisMemoSpy('getMemoCISRequest');

        jest.useFakeTimers();
        renderWrapper(initialState);
        jest.runAllTimers();

        waitFakeTime(() => screen.getAllByRole('radio')[1].click());

        fireEvent.change(screen.getByTestId('DateRangePicker_onChange'), {
          target: {
            value: 'undefined',
            name: 'dateRangeValue',
            range: { start: new Date() }
          }
        });

        screen.getByText(I18N_COMMON_TEXT.APPLY).click();

        waitFor(() => {
          expect(mockAction).toBeCalledWith({
            storeId,
            postData: { accountId: accEValue },
            isSearch: true
          });
        });
      });

      it('validation', () => {
        const mockAction = cisMemoSpy('getMemoCISRequest');
        jest.useFakeTimers();
        renderWrapper(initialState);
        jest.runAllTimers();

        waitFakeTime(() => screen.getAllByRole('radio')[1].click());

        fireEvent.change(screen.getByTestId('DateRangePicker_onChange'), {
          target: {
            value: 'name',
            name: 'dateRangeValue',
            range: { start: new Date(), end: new Date() }
          }
        });

        screen.getByText(I18N_COMMON_TEXT.APPLY).click();

        waitFor(() => {
          expect(mockAction).toBeCalledWith({
            storeId,
            postData: { accountId: accEValue },
            isSearch: true
          });
        });
      });
    });
  });
});
