import { createSelector } from '@reduxjs/toolkit';
import { AppState } from 'storeConfig';

export const getMemoType = (state: AppState, storeId: string) => {
  return state.memo[storeId]?.configMemo?.data?.type;
};

export const takeMemoType = createSelector(getMemoType, data => data);
