import { selectorWrapper, storeId } from 'app/test-utils';
import { MemoType } from '../types';
import { takeMemoType } from './selectors';

describe('Test Memo selectors', () => {
  const store: Partial<RootState> = {
    memo: {
      [storeId]: {
        configMemo: {
          data: {
            type: MemoType.CIS
          }
        }
      }
    }
  };

  it('takeMemoType', () => {
    const { data, emptyData } = selectorWrapper(store)(takeMemoType);
    expect(data).toEqual('0');
    expect(emptyData).toEqual(undefined);
  });
});
