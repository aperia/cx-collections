import { memoActions, reducer } from './reducers';
import { storeId } from 'app/test-utils';
import { MemoState } from '../types';
const { removeMemo } = memoActions;

const initialState: MemoState = {};

describe('Memos > reducers', () => {
  it('removeMemo', () => {
    // invoke
    const params = { storeId };
    const state = reducer(initialState, removeMemo(params));

    // expect
    expect(state[storeId]).toBeUndefined();
  });
});
