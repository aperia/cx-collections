import { createStore, applyMiddleware } from '@reduxjs/toolkit';
import {
  mockActionCreator,
  responseDefault,
  storeId,
  accEValue,
  byPassFulfilled
} from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import {
  ACCOUNT_DETAILS_ACTIONS,
  CARDHOLDER_MAINTENANCE_ACTIONS,
  CALLER_AUTHENTICATION_ACTIONS,
  LETTER_ACTIONS,
  MONETARY_ADJUSTMENT_ACTIONS,
  ACH_ACTION,
  SCHEDULE_ACTIONS
} from 'pages/Memos/constants';
import { postMemo } from './postMemo';
import { memoActions } from './reducers';
import { callerAuthenticationService } from 'pages/CallerAuthenticate/callerAuthenticationService';
import { MemoType } from '../types';
import { cisMemoActions } from '../CIS/_redux/reducers';
import thunk from 'redux-thunk';
import { actionMemoChronicle } from '../Chronicle/_redux/reducers';

const memoActionsSpy = mockActionCreator(memoActions);
const cisMemoActionsSpy = mockActionCreator(cisMemoActions);
const actionMemoChronicleSpy = mockActionCreator(actionMemoChronicle);
HTMLCanvasElement.prototype.getContext = jest.fn();

describe('postMemo', () => {
  let spy: jest.SpyInstance;

  beforeEach(() => {
    spy = jest
      .spyOn(callerAuthenticationService, 'triggerRenewToken')
      .mockResolvedValue({ ...responseDefault });
  });

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('case empty type fulfilled', async () => {
    const memoActionsRequest = memoActionsSpy('getMemoConfig');
    const store = createStore(
      rootReducer,
      {
        memo: {
          [storeId]: {
            configMemo: {
              data: {
                type: '' as MemoType
              }
            }
          }
        }
      },
      applyMiddleware(thunk)
    );

    await postMemo(ACCOUNT_DETAILS_ACTIONS.UPDATE_EXTERNAL_ACC_STATUS, {
      storeId,
      accEValue
    })(byPassFulfilled(''), store.getState, {});

    expect(memoActionsRequest).toBeCalled();
  });

  it('case empty', async () => {
    const memoActionsRequest = memoActionsSpy('getMemoConfig');
    const store = createStore(
      rootReducer,
      {
        memo: {
          [storeId]: {
            configMemo: {
              data: {
                type: '' as MemoType
              }
            }
          }
        }
      },
      applyMiddleware(thunk)
    );

    await postMemo(ACCOUNT_DETAILS_ACTIONS.UPDATE_EXTERNAL_ACC_STATUS, {
      storeId,
      accEValue
    })(store.dispatch, store.getState, {});

    expect(memoActionsRequest).toBeCalled();
  });

  it('case CIS type', async () => {
    const addCISMemoRequest = cisMemoActionsSpy('addCISMemoRequest');
    const store = createStore(
      rootReducer,
      {
        memo: {
          [storeId]: {
            configMemo: {
              data: {
                type: MemoType.CIS
              }
            }
          }
        }
      },
      applyMiddleware(thunk)
    );

    await postMemo(ACCOUNT_DETAILS_ACTIONS.UPDATE_EXTERNAL_REASON_CD, {
      storeId,
      accEValue
    })(store.dispatch, store.getState, {});

    expect(addCISMemoRequest).toBeCalled();
  });

  it('case CHRONICLE type', async () => {
    const actionMemoChronicleRequest =
      actionMemoChronicleSpy('addMemoChronicle');
    const store = createStore(
      rootReducer,
      {
        memo: {
          [storeId]: {
            configMemo: {
              data: {
                type: MemoType.CHRONICLE
              }
            }
          }
        }
      },
      applyMiddleware(thunk)
    );

    await postMemo(CARDHOLDER_MAINTENANCE_ACTIONS.ADD_EXPANDED_ADDRESS, {
      storeId,
      accEValue
    })(store.dispatch, store.getState, {});

    expect(actionMemoChronicleRequest).toBeCalled();
  });

  describe('generateMemoText', () => {
    const checkTypeCard = async (typeCard: any) => {
      const actionMemoChronicleRequest =
        actionMemoChronicleSpy('addMemoChronicle');
      const store = createStore(
        rootReducer,
        {
          memo: {
            [storeId]: {
              configMemo: {
                data: {
                  type: MemoType.CHRONICLE
                }
              }
            }
          }
        },
        applyMiddleware(thunk)
      );

      await postMemo(typeCard, {
        storeId,
        accEValue
      })(store.dispatch, store.getState, {});

      expect(actionMemoChronicleRequest).toBeCalled();
    };

    beforeEach(() => {
      spy = jest
        .spyOn(callerAuthenticationService, 'triggerRenewToken')
        .mockResolvedValue({ ...responseDefault });
    });

    afterEach(() => {
      spy?.mockReset();
      spy?.mockRestore();
    });

    it('CARDHOLDER_MAINTENANCE_ACTIONS -> ADD_STANDARD_ADDRESS', async () => {
      await checkTypeCard(CARDHOLDER_MAINTENANCE_ACTIONS.ADD_STANDARD_ADDRESS);
    });

    it('CARDHOLDER_MAINTENANCE_ACTIONS -> DELETE_EXPANDED_ADDRESS', async () => {
      await checkTypeCard(
        CARDHOLDER_MAINTENANCE_ACTIONS.DELETE_EXPANDED_ADDRESS
      );
    });

    it('CARDHOLDER_MAINTENANCE_ACTIONS -> DELETE_STANDARD_ADDRESS', async () => {
      await checkTypeCard(
        CARDHOLDER_MAINTENANCE_ACTIONS.DELETE_STANDARD_ADDRESS
      );
    });

    it('CARDHOLDER_MAINTENANCE_ACTIONS -> EDIT_EXPANDED_ADDRESS', async () => {
      await checkTypeCard(CARDHOLDER_MAINTENANCE_ACTIONS.EDIT_EXPANDED_ADDRESS);
    });

    it('CARDHOLDER_MAINTENANCE_ACTIONS -> EDIT_STANDARD_ADDRESS', async () => {
      await checkTypeCard(CARDHOLDER_MAINTENANCE_ACTIONS.EDIT_STANDARD_ADDRESS);
    });

    it('CARDHOLDER_MAINTENANCE_ACTIONS -> UPDATE_EMAIL_INFORMATION', async () => {
      await checkTypeCard(
        CARDHOLDER_MAINTENANCE_ACTIONS.UPDATE_EMAIL_INFORMATION
      );
    });

    it('CARDHOLDER_MAINTENANCE_ACTIONS -> UPDATE_PHONE_NUMBER', async () => {
      await checkTypeCard(CARDHOLDER_MAINTENANCE_ACTIONS.UPDATE_PHONE_NUMBER);
    });

    it('CALLER_AUTHENTICATION_ACTIONS -> BYPASS_AUTHENTICATION', async () => {
      await checkTypeCard(CALLER_AUTHENTICATION_ACTIONS.BYPASS_AUTHENTICATION);
    });

    it('CALLER_AUTHENTICATION_ACTIONS -> FAILED_TO_VERIFY', async () => {
      await checkTypeCard(CALLER_AUTHENTICATION_ACTIONS.FAILED_TO_VERIFY);
    });

    it('CALLER_AUTHENTICATION_ACTIONS -> VERIFY_SUCCESS', async () => {
      await checkTypeCard(CALLER_AUTHENTICATION_ACTIONS.VERIFY_SUCCESS);
    });

    it('LETTER_ACTIONS -> DELETE_PENDING_LETTER', async () => {
      await checkTypeCard(LETTER_ACTIONS.DELETE_PENDING_LETTER);
    });

    it('LETTER_ACTIONS -> SEND_LETTER_WITHOUT_VARIABLES', async () => {
      await checkTypeCard(LETTER_ACTIONS.SEND_LETTER_WITHOUT_VARIABLES);
    });

    it('LETTER_ACTIONS -> SEND_LETTER_WITH_VARIABLES', async () => {
      await checkTypeCard(LETTER_ACTIONS.SEND_LETTER_WITH_VARIABLES);
    });

    it('MONETARY_ADJUSTMENT_ACTIONS -> ADJUST_TRANSACTION', async () => {
      await checkTypeCard(MONETARY_ADJUSTMENT_ACTIONS.ADJUST_TRANSACTION);
    });

    it('MONETARY_ADJUSTMENT_ACTIONS -> APPROVE_ADJUSTMENT', async () => {
      await checkTypeCard(MONETARY_ADJUSTMENT_ACTIONS.APPROVE_ADJUSTMENT);
    });

    it('MONETARY_ADJUSTMENT_ACTIONS -> REJECT_ADJUSTMENT', async () => {
      await checkTypeCard(MONETARY_ADJUSTMENT_ACTIONS.REJECT_ADJUSTMENT);
    });

    it('ACH_ACTION -> ADD_CHECKING', async () => {
      await checkTypeCard(ACH_ACTION.ADD_CHECKING);
    });

    it('ACH_ACTION -> UPDATE_CHECKING', async () => {
      await checkTypeCard(ACH_ACTION.UPDATE_CHECKING);
    });

    it('ACH_ACTION -> ADD_SAVING', async () => {
      await checkTypeCard(ACH_ACTION.ADD_SAVING);
    });

    it('ACH_ACTION -> UPDATE_SAVING', async () => {
      await checkTypeCard(ACH_ACTION.UPDATE_SAVING);
    });

    it('ACH_ACTION -> SUBMIT', async () => {
      await checkTypeCard(ACH_ACTION.SUBMIT);
    });

    it('SCHEDULE_ACTIONS -> ADD_BANK', async () => {
      await checkTypeCard(SCHEDULE_ACTIONS.ADD_BANK);
    });

    it('SCHEDULE_ACTIONS -> ADD_CARD', async () => {
      await checkTypeCard(SCHEDULE_ACTIONS.ADD_CARD);
    });

    it('SCHEDULE_ACTIONS -> VERIFIED_BANK', async () => {
      await checkTypeCard(SCHEDULE_ACTIONS.VERIFIED_BANK);
    });

    it('SCHEDULE_ACTIONS -> DEACTIVATED', async () => {
      await checkTypeCard(SCHEDULE_ACTIONS.DEACTIVATED);
    });

    it('SCHEDULE_ACTIONS -> SUBMIT_BANK', async () => {
      await checkTypeCard(SCHEDULE_ACTIONS.SUBMIT_BANK);
    });

    it('SCHEDULE_ACTIONS -> SUBMIT_CARD', async () => {
      await checkTypeCard(SCHEDULE_ACTIONS.SUBMIT_CARD);
    });

    it('EMPTY CASE', async () => {
      await checkTypeCard('');
    });
  });
});
