import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// type
import { MemoState } from '../types';

// async-thunk
import { getMemoConfig, getMemoConfigBuilder } from './getMemoConfig';
import { postMemo } from './postMemo';

const initialState: MemoState = {};

const { actions, reducer } = createSlice({
  name: 'memo',
  initialState,
  reducers: {
    removeMemo: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    }
  },
  extraReducers: builder => {
    getMemoConfigBuilder(builder);
  }
});

const extraActions = {
  ...actions,
  getMemoConfig,
  postMemo
};

export { extraActions as memoActions, reducer };
