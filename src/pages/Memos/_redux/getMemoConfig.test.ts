import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { getMemoConfig } from './getMemoConfig';

import memoService from '../memoService';
import { responseDefault, storeId } from 'app/test-utils';

const initialState: Partial<RootState> = {
  memoChronicle: {
    [storeId]: {
      memos: []
    }
  }
};

describe('Memos > addMemoChronicle', () => {
  it('success', async () => {
    jest
      .spyOn(memoService, 'getMemoConfig')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const store = createStore(rootReducer, initialState);

    const response = await getMemoConfig({
      storeId,
      accountId: storeId
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({});
  });

  it('reject', async () => {
    jest.spyOn(memoService, 'getMemoConfig').mockRejectedValue({});

    const store = createStore(rootReducer, {});
    const response = await getMemoConfig({
      storeId,
      accountId: storeId
    })(store.dispatch, store.getState, {});

    expect(response.payload).toBeUndefined();
  });
});
