import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import {
  MemoAction,
  GetMemoConfigArgs,
  MemoState,
  GetMemoConfigPayload
} from '../types';

import memoService from '../memoService';

export const getMemoConfig = createAsyncThunk<
  GetMemoConfigPayload,
  GetMemoConfigArgs,
  ThunkAPIConfig
>(MemoAction.GET_MEMO_CONFIG, async (args, thunkAPI) => {
  const body = {
    common: {
      accountId: args.accountId
    },
    selectFields: ['memoSystemUseCode']
  };
  const { data } = await memoService.getMemoConfig(body, args?.token);

  return data;
});

export const getMemoConfigBuilder = (
  builder: ActionReducerMapBuilder<MemoState>
) => {
  builder
    .addCase(getMemoConfig.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        configMemo: {
          ...draftState[storeId]?.configMemo,
          loading: true
        }
      };
    })
    .addCase(getMemoConfig.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { memoSystemUseCode } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        configMemo: {
          ...draftState[storeId]?.configMemo,
          loading: false,
          data: {
            type: memoSystemUseCode
          }
        }
      };
    })
    .addCase(getMemoConfig.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        configMemo: {
          ...draftState[storeId].configMemo,
          error: true,
          loading: false
        }
      };
    });
};
