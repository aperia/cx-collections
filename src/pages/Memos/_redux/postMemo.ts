import { isFulfilled } from '@reduxjs/toolkit';

// types
import { AddCISMemoArgs } from '../CIS/types';
import { MemoType, PostMemoData } from '../types';

// actions
import { memoActions } from './reducers';
import { cisMemoActions } from '../CIS/_redux/reducers';
import { actionMemoChronicle } from '../Chronicle/_redux/reducers';

// constants
import { MEMO_FLAG } from '../CIS/constants';
import { EMPTY_STRING } from 'app/constants';

// hooks
import useGenerateMemoText from '../helpers/generateMemoText';

// Service
import { callerAuthenticationService } from 'pages/CallerAuthenticate/callerAuthenticationService';

// helpers
import { getDefaultTokenBasedOnHostName } from 'app/helpers/setupInterceptor';

export const postMemo: AppThunk =
  (actionType: string, data: PostMemoData) => async (dispatch, getState) => {
    const {
      storeId,
      accEValue,
      chronicleKey,
      cspa,
      actionCode,
      actionType: actionTypeCallResultConfig
    } = data;
    const { operatorID = '', app } = window.appConfig?.commonConfig || {};
    const { memo } = getState();

    // get renewToken
    const defaultTokenByRole = dispatch(getDefaultTokenBasedOnHostName());
    const renewResponse = await callerAuthenticationService.triggerRenewToken({
      common: {
        accountId: accEValue,
        applicationId: app || EMPTY_STRING
      },
      jwt: `${defaultTokenByRole}`
    });

    // Get memo type of account
    let memoType = memo[storeId]?.configMemo?.data?.type;
    if (!memoType) {
      const response = await dispatch<Promise<MagicKeyValue>>(
        memoActions.getMemoConfig({
          storeId,
          accountId: accEValue,
          token: renewResponse?.data?.jwt
        })
      );
      memoType = isFulfilled(response)
        ? (response?.payload as MagicKeyValue)?.memoSystemUseCode
        : '';
    }

    const content = useGenerateMemoText(actionType, {
      ...data,
      operatorId: operatorID
    });

    if (memoType === MemoType.CIS) {
      const args = {
        storeId,
        payload: {
          accountId: accEValue,
          memoText: content,
          memoOperatorIdentifier: operatorID,
          memoFlags: MEMO_FLAG.STANDARD
        },
        token: renewResponse?.data?.jwt
      } as AddCISMemoArgs;
      dispatch(cisMemoActions.addCISMemoRequest(args));
    }

    if (memoType === MemoType.CHRONICLE) {
      dispatch(
        actionMemoChronicle.addMemoChronicle({
          storeId,
          postData: {
            accountId: accEValue,
            memoText: content
          },
          data: {
            cspa,
            chronicleKey,
            actionCode,
            actionType: actionTypeCallResultConfig
          },
          token: renewResponse?.data?.jwt
        })
      );
    }
  };
