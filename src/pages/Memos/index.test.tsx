import React from 'react';
import { screen } from '@testing-library/react';
import { CustomStoreIdProvider } from 'app/hooks';
import { renderMockStore, storeId } from 'app/test-utils';
import Memos from '.';
import { MemoType } from './types';

jest.mock('./Chronicle', () => () => <div>ChronicleMemo</div>);
jest.mock('./CIS', () => () => <div>CISMemo</div>);

// const initialState: Partial<RootState> = {
//   memo: {
//     [storeId]: {
//       configMemo: { data: { type: MemoType.CHRONICLE } }
//     }
//   }
// };

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <CustomStoreIdProvider value={{ customStoreId: storeId }}>
      <Memos />
    </CustomStoreIdProvider>,
    { initialState }
  );
};

describe('Render', () => {
  it('Render UI > empty state', () => {
    renderWrapper({ memo: { [storeId]: {} } });

    expect(screen.queryByText('ChronicleMemo')).toBeNull();
    expect(screen.queryByText('CISMemo')).toBeNull();
  });

  it('Render UI > ChronicleMemo', () => {
    renderWrapper({
      memo: {
        [storeId]: { configMemo: { data: { type: MemoType.CHRONICLE } } }
      }
    });

    expect(screen.queryByText('ChronicleMemo')).toBeInTheDocument();
  });

  it('Render UI > CISMemo', () => {
    renderWrapper({
      memo: {
        [storeId]: { configMemo: { data: { type: MemoType.CIS } } }
      }
    });

    expect(screen.queryByText('CISMemo')).toBeInTheDocument();
  });
});
