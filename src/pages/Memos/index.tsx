import React, { useEffect, useLayoutEffect, useRef } from 'react';
import classNames from 'classnames';

// Redux
import { AppState } from 'storeConfig';

// Components
import ChronicleMemo from './Chronicle';
import CISMemo from './CIS';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { MemoType } from 'pages/Memos/types';

// hooks
import { useCustomStoreId, useAccountDetail } from 'app/hooks';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export interface MemoProps {
  shouldRenderPinnedIcon?: boolean;
  className?: string;
}

const Memo: React.FC<MemoProps> = ({ shouldRenderPinnedIcon, className }) => {
  const dispatch = useDispatch();
  const { customStoreId: storeId } = useCustomStoreId();
  const { accEValue } = useAccountDetail();
  const divRef = useRef<HTMLDivElement | null>(null);

  const memoType = useSelector<AppState, MemoType | undefined>(
    appState => appState.memo[storeId]?.configMemo?.data?.type
  );

  const isLoadingConfig = useSelector<AppState, boolean>(
    state => state.memo[storeId]?.configMemo?.loading || false
  );

  useEffect(() => {
    if (!memoType) {
      dispatch(memoActions.getMemoConfig({ storeId, accountId: accEValue }));
    }
  }, [dispatch, storeId, memoType, accEValue]);

  useLayoutEffect(() => {
    if (!divRef.current) return;
    divRef.current.style.height = `calc(100vh - 112px)`;
  }, []);

  useEffect(() => {
    //  clear data when memo is close is close
    return () => {
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          forSection: 'inMemoFlyOut',
          storeId
        })
      );
    };
  }, [dispatch, storeId]);

  if (!memoType) {
    return (
      <div className={classNames({ loading: isLoadingConfig })} ref={divRef} />
    );
  }

  return memoType === MemoType.CHRONICLE ? (
    <ChronicleMemo
      shouldRenderPinnedIcon={shouldRenderPinnedIcon}
      className={className}
    />
  ) : (
    <CISMemo
      shouldRenderPinnedIcon={shouldRenderPinnedIcon}
      className={className}
    />
  );
};

export default Memo;
