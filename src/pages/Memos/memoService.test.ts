import memoService from './memoService';

// utils
import { mockApiServices, storeId } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import { GetMemoConfigProps } from './types';

describe('memoService', () => {
  describe('getMemoConfig', () => {
    const params: GetMemoConfigProps = {
      common: { accountId: storeId },
      selectFields: ['field']
    };
    const token = 'token';

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      memoService.getMemoConfig(params, token);

      expect(mockService).toBeCalledWith('', params, {
        headers: { Authorization: token }
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      memoService.getMemoConfig(params, token);

      expect(mockService).toBeCalledWith(apiUrl.memo.getMemoConfig, params, {
        headers: { Authorization: token }
      });
    });
  });
});
