import {
  MemoChronicle,
  MemoChronicleSortField,
  MemoChronicleSortType
} from 'pages/Memos/Chronicle/types';

export const chronicleSearchFormView = 'chronicleSearchFormView';

export enum AddMemoChronicleView {
  ID = 'add-memo-chronicle',
  DESCRIPTOR = 'addMemoChronicleViews'
}

export enum EditMemoChronicleView {
  ID = 'edit-memo-chronicle',
  DESCRIPTOR = 'editMemoChronicleViews'
}

export enum AddMemoChronicleDataField {
  MEMO_TYPE = 'memoType',
  MEMO_GROUP = 'memoGroup'
}

export enum EditMemoChronicleDataField {
  MEMO_TYPE = 'memoType',
  MEMO_GROUP = 'memoGroup'
}

export const ITEM_PER_PAGE = 50;

export enum DefaultSequence {
  START_SEQUENCE = '1',
  END_SEQUENCE = '50'
}

export enum SearchMemoChronicleDataField {
  MEMO_SEARCH_TYPE = 'memoType',
  MEMO_SEARCH_KEYWORD = 'memoKeyword',
  MEMO_SEARCH_DATE = 'memoDate',
  DATE_TYPE_RADIO = 'dateType',
  DATE_PICKER = 'memoDate',
  DATE_RANGE_PICKER = 'memoDateRange',
  SEARCH_BY_RADIO = 'searchBy',
  MEMO_SEARCH_GROUP = 'memoGroup'
}

export const MAX_LENGTH_MEMO_TEXT = 90;

export const SEARCH_DEFAULT: Partial<MemoChronicle> = {
  memoText: '',
  memoDate: null,
  memoType: null,
  memoDateRange: null,
  dateType: { dateRange: false, specificDate: true },
  searchBy: { group: true, type: false }
};

export const SORT_DEFAULT = {
  field: MemoChronicleSortField.DATE_TIME,
  type: MemoChronicleSortType.DESC
};

export const CONFIG_DEFAULT = {
  start: 1,
  end: ITEM_PER_PAGE,
  sort: SORT_DEFAULT,
  search: SEARCH_DEFAULT
};

export const FIRST_PAGE = 1;
export const FIRST_SEQUENCE = 1;

export enum GetListType {
  REFRESH = 'refresh',
  FIRST = 'first',
  LOAD_MORE = 'loadMore',
  RESET_SEARCH = 'resetSearch'
}
