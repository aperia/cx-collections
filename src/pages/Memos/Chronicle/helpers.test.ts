import * as helpers from './helpers';
import {
  MemoChronicle,
  MemoChronicleConfig,
  MemoChronicleSort,
  MemoChronicleSortField,
  MemoChronicleSortType
} from './types';

const memos: MemoChronicle[] = [
  {
    memoGroup: { value: 'val', description: 'des' },
    memoType: { value: 'val', description: 'des' },
    memoDate: '2021-02-27T15:53:07.502Z',
    memoIdentifier: '1',
    memoOperatorIdentifier: 'JZ5',
    memoText: 'Beatae qui magnam quaerat voluptates.',
    memoTime: '2020-09-07T14:20:34.028Z',
    behaviorIdentifier: '',
    filterIdentifier: ''
  }
];

const sort: MemoChronicleSort = {
  type: MemoChronicleSortType.ASC,
  field: MemoChronicleSortField.DATE_TIME
};

const configs: MemoChronicleConfig = {
  sort,
  search: { dateType: {} }
};

describe('Memos > Chronicle > helpers', () => {
  describe('buildMemoData', () => {
    it('memo is null', () => {
      const result = helpers.buildMemoData(undefined, configs);

      expect(result).toEqual([]);
    });

    it('configs is null', () => {
      const result = helpers.buildMemoData(
        memos,
        null as unknown as MemoChronicleConfig
      );

      expect(result).toEqual(memos);
    });

    it('configs is empty', () => {
      const result = helpers.buildMemoData(
        memos,
        {} as unknown as MemoChronicleConfig
      );

      expect(result).toEqual(memos);
    });

    it('search config is empty, soft config is empty', () => {
      const result = helpers.buildMemoData(memos, {
        search: {},
        sort: {}
      } as unknown as MemoChronicleConfig);

      expect(result).toEqual(memos);
    });

    describe('memos has data', () => {
      it('search by empty', () => {
        const result = helpers.buildMemoData(memos, configs);
        expect(result).toEqual(memos);
      });
      it('search by dateType', () => {
        const result = helpers.buildMemoData(memos, {
          sort,
          search: { dateType: { dateRange: true } }
        });
        expect(result).toEqual(memos);
      });
      it('search by searchBy', () => {
        const result = helpers.buildMemoData(memos, {
          sort,
          search: { searchBy: { type: true } }
        });
        expect(result).toEqual(memos);
      });
      it('search by memoText', () => {
        const result = helpers.buildMemoData(memos, {
          sort,
          search: { memoText: 'memoText' }
        });
        expect(result).toEqual([]);
      });
      it('search by memoDateRange', () => {
        const result = helpers.buildMemoData(memos, {
          sort,
          search: { memoDateRange: { end: '50', start: '1' } }
        });
        expect(result).toEqual(memos);
      });
      it('search by memoType', () => {
        const result = helpers.buildMemoData(memos, {
          sort,
          search: { memoType: { value: 'val', description: 'des' } }
        });
        expect(result).toEqual(memos);
      });
      it('search by memoType', () => {
        const result = helpers.buildMemoData(memos, {
          sort,
          search: { memoType: { value: 'val', description: 'des' } }
        });
        expect(result).toEqual(memos);
      });
    });
  });

  it('handleSortMemos', () => {
    const memos = [
      {
        behaviorIdentifier: '!ADJST',
        filterIdentifier: 'ALL',
        memoOperatorIdentifier: 'SA1',
        memoIdentifier: 'memoIdentifier',
        memoText: 'memoText',
        memoTime: '1'
      },
      {
        behaviorIdentifier: '!ADJST',
        filterIdentifier: 'ALL',
        memoOperatorIdentifier: 'SA1',
        memoIdentifier: 'memoIdentifier',
        memoText: 'memoText',
        memoTime: '2'
      }
    ] as MemoChronicle[];
    const result = helpers.handleSortMemos(memos, {
      field: MemoChronicleSortField.DATE_TIME,
      type: MemoChronicleSortType.DESC
    });

    expect(result).toEqual([memos[1], memos[0]]);
  });

  it('parseChronicleDateFormat', () => {
    const result = helpers.parseChronicleDateFormat('09/02/2021');

    expect(result).toEqual('2021-09-02');
  });

  it('parseChronicleDateFormat > Case 2', () => {
    const result = helpers.parseChronicleDateFormat('9/20/2021');

    expect(result).toEqual('2021-09-20');
  });

  it('parseChronicleDateFormat > Case 3', () => {
    const result = helpers.parseChronicleDateFormat('12/12/2021');

    expect(result).toEqual('2021-12-12');
  });
});
