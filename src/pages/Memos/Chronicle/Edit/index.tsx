import React, { useRef, useEffect, useCallback, useMemo } from 'react';

// dof
import { View, ExtraFieldProps } from 'app/_libraries/_dof/core';

// components
import {
  Button,
  Icon,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';

// redux
import { useSelector, useDispatch, batch } from 'react-redux';
import { isInvalid, Field } from 'redux-form';
import { actionMemoChronicle } from 'pages/Memos/Chronicle/_redux/reducers';
import { AppState } from 'storeConfig';
import {
  selectChronicleMemoTypeList,
  selectChronicleMemoItem,
  selectEditFormChronicleMemo
} from 'pages/Memos/Chronicle/_redux/selectors';
import { triggerEditMemoChronicle } from 'pages/Memos/Chronicle/_redux/edit';

// hooks
import {
  useAccountDetail,
  useCustomStoreId,
  useCustomStoreIdSelector
} from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// types
import { EditChronicleMemoRequestData } from '../types';
import { MemoChronicleData } from 'pages/Memos/Chronicle/types';

// constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { I18N_MEMOS } from '../../constants';
import { EMPTY_STRING, INVALID_DATE } from 'app/constants';
import {
  EditMemoChronicleView,
  EditMemoChronicleDataField
} from '../constants';
import { useCheckMemoText } from 'pages/Memos/hooks/useCheckMemoText';
import { useDisableSubmitButton } from 'app/hooks/useDisableSubmitButton';
import { formatCommon } from 'app/helpers';
import { isEmpty } from 'app/_libraries/_dls/lodash';

export interface EditMemoChronicleProps {
  open: boolean;
}

const EditMemoChronicle: React.FC<EditMemoChronicleProps> = ({ open }) => {
  const viewRef = useRef<any>(null);

  const dispatch = useDispatch();
  const { customStoreId: storeId } = useCustomStoreId();
  const { accEValue } = useAccountDetail();
  const { t } = useTranslation();

  const isFormInValid = useSelector<AppState, boolean>(
    isInvalid(`${storeId}-${EditMemoChronicleView.ID}`)
  );

  const isLoading = useSelector<AppState, boolean>(
    state => state.memoChronicle[storeId]?.editMemoChronicle?.loading ?? false
  );

  const memoItem = useCustomStoreIdSelector<MemoChronicleData>(
    selectChronicleMemoItem
  );

  const memoTypeData = useCustomStoreIdSelector<RefDataValue[]>(
    selectChronicleMemoTypeList
  );

  const editMemoValues: any =
    useCustomStoreIdSelector<EditChronicleMemoRequestData>(
      selectEditFormChronicleMemo
    );

  const formValue = useMemo(() => {
    const overrideRetentionDate = INVALID_DATE.includes(
      editMemoValues?.overrideRetentionDate || EMPTY_STRING
    )
      ? undefined
      : formatCommon(editMemoValues?.overrideRetentionDate || EMPTY_STRING).time
          .utcDate;

    return {
      ...editMemoValues,
      memoIdentifier: editMemoValues.memoIdentifier,
      memoText: editMemoValues.memoText,
      overrideRetentionDate: overrideRetentionDate,
      memoType: editMemoValues?.memoType?.value
    };
  }, [editMemoValues]);

  const { disableSubmit, setPrimitiveValues } =
    useDisableSubmitButton(formValue);

  const handleClose = useCallback(() => {
    dispatch(actionMemoChronicle.closeEditChronicleMemo({ storeId }));
  }, [dispatch, storeId]);

  const handleSubmit = () => {
    dispatch(
      triggerEditMemoChronicle({
        accountId: accEValue,
        storeId,
        postData: editMemoValues,
        onSuccess: handleClose
      })
    );
  };

  useEffect(() => {
    if (!isEmpty(memoTypeData)) return;
    batch(() => {
      dispatch(
        actionMemoChronicle.getMemoChronicleBehaviors({
          storeId,
          postData: {
            accountId: accEValue
          }
        })
      );
      dispatch(
        actionMemoChronicle.getMemoChronicleFilters({
          storeId,
          postData: {
            accountId: accEValue
          }
        })
      );
    });
  }, [accEValue, dispatch, memoTypeData, storeId]);

  const formDefaultValue: any = useMemo(() => {
    if (!memoItem) return;
    return {
      memoIdentifier: memoItem.memoIdentifier,
      memoText: memoItem.memoText,
      overrideRetentionDate: INVALID_DATE.includes(
        memoItem.memoDeletionDate || EMPTY_STRING
      )
        ? undefined
        : memoItem.memoDeletionDate,
      memoType: memoTypeData.find(
        type => type.value === memoItem.behaviorIdentifier.trim()
      ),
      memoDate: memoItem.memoDate
    };
  }, [memoItem, memoTypeData]);

  useEffect(() => {
    if (!open) return;
    setPrimitiveValues({
      ...formDefaultValue,
      memoType: formDefaultValue?.memoType?.value,
      overrideRetentionDate: formDefaultValue?.overrideRetentionDate
        ? formatCommon(formDefaultValue?.overrideRetentionDate).time.utcDate
        : undefined
    });
  }, [formDefaultValue, open, setPrimitiveValues]);

  useEffect(() => {
    if (!open) return;
    setImmediate(() => {
      const memoType: Field<ExtraFieldProps> = viewRef.current?.props.onFind(
        EditMemoChronicleDataField.MEMO_TYPE
      );
      memoType?.props?.props.setData(memoTypeData);
    });
  }, [memoTypeData, open]);

  useCheckMemoText(viewRef, memoItem?.memoText, open);

  if (!open) return null;

  return (
    <Modal
      xs
      loading={isLoading}
      show={open}
      dataTestId="edit-memo-chronicle-modal"
    >
      <ModalHeader
        border
        className="border-bottom"
        dataTestId="edit-memo-chronicle-modal_header"
      >
        <ModalTitle dataTestId="edit-memo-chronicle-modal_header-title">
          {t(I18N_MEMOS.EDIT)}
        </ModalTitle>
        <Button
          onClick={handleClose}
          variant="icon-secondary"
          dataTestId="edit-memo-chronicle-modal_close-btn"
        >
          <Icon
            name="close"
            size="5x"
            dataTestId="edit-memo-chronicle-modal-close-icon"
          />
        </Button>
      </ModalHeader>
      <ModalBodyWithApiError
        apiErrorClassName="mb-24"
        storeId={storeId}
        dataTestId="edit-memo-chronicle-modal_body"
      >
        <View
          id={`${storeId}-${EditMemoChronicleView.ID}`}
          formKey={`${storeId}-${EditMemoChronicleView.ID}`}
          descriptor={EditMemoChronicleView.DESCRIPTOR}
          value={formDefaultValue}
          ref={viewRef}
        />
      </ModalBodyWithApiError>
      <ModalFooter>
        <Button
          variant="secondary"
          onClick={handleClose}
          dataTestId="edit-memo-chronicle-modal_cancel-btn"
        >
          {t(I18N_COMMON_TEXT.CANCEL)}
        </Button>
        <Button
          disabled={isFormInValid || disableSubmit}
          variant="primary"
          onClick={handleSubmit}
          dataTestId="edit-memo-chronicle-modal_save-btn"
        >
          {t(I18N_COMMON_TEXT.SAVE)}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default EditMemoChronicle;
