import React from 'react';
import { screen } from '@testing-library/react';
import { CustomStoreIdProvider } from 'app/hooks';
import { renderMockStore, storeId } from 'app/test-utils';
import EditMemoChronicle from './index';
import { actionMemoChronicle } from 'pages/Memos/Chronicle/_redux/reducers';
import { formatTime } from 'app/helpers';
import * as constants from 'app/constants';
import { MemoChronicleData } from '../types';

jest.mock('react-redux', () => {
  return {
    ...(jest.requireActual('react-redux') as object),
    useDispatch: () => (x: Function) => jest.fn().mockImplementation(() => x())
  };
});
const initialState: Partial<RootState> = {
  memoChronicle: {
    [storeId]: {
      refData: {
        typeList: [{ value: 'behaviorIdentifier', description: 'description' }]
      },
      editMemoChronicle: {
        memoItem: {
          memoOperatorIdentifier: 'PSF',
          memoText: 'Id qui beatae laborum qui.',
          memoDate: formatTime(new Date().toISOString()).date!,
          memoTime: '04:23:59',
          memoIdentifier: '1',
          behaviorIdentifier: 'behaviorIdentifier',
          filterIdentifier: 'filterIdentifier',
          memoDeletionDate: '00000000'
        }
      }
    }
  },
  form: {
    ['123456789-edit-memo-chronicle']: {
      values: {
        overrideRetentionDate: new Date()
      }
    } as any
  }
};

const errorInitialState: Partial<RootState> = {
  memoChronicle: {
    [storeId]: {
      refData: {
        typeList: [{ value: 'behaviorIdentifier', description: 'description' }]
      },
      editMemoChronicle: {
        memoItem: {
          memoOperatorIdentifier: 'PSF',
          memoText: '@!*&(@!*($%@!^(*$(',
          memoDate: formatTime(new Date().toISOString()).date!,
          memoTime: '04:23:59',
          memoIdentifier: '1',
          behaviorIdentifier: 'behaviorIdentifier',
          filterIdentifier: 'filterIdentifier',
          memoDeletionDate: '00000000'
        }
      }
    }
  }
};

describe('Test edit chronicle memo', () => {
  it('Case edit modal not open', async () => {
    // Arrange
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <EditMemoChronicle open={false} />
      </CustomStoreIdProvider>
    );
    // Assertion
    expect(wrapper.container.firstChild).not.toBeInTheDocument();
  });

  it('Case edit modal open > with memoItem', async () => {
    // Arrange
    const invalidDate = constants.INVALID_DATE;
    (constants as any).INVALID_DATE = [''];

    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <EditMemoChronicle open={false} />
      </CustomStoreIdProvider>,
      { initialState }
    );

    (constants as any).INVALID_DATE = invalidDate;

    // Assertion
    expect(wrapper.container.firstChild).not.toBeInTheDocument();
  });

  it('Case edit modal open > with error memoItem', async () => {
    // Arrange

    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <EditMemoChronicle open={false} />
      </CustomStoreIdProvider>,
      { initialState: errorInitialState }
    );
    // Assertion
    expect(wrapper.container.firstChild).not.toBeInTheDocument();
  });

  it('Case edit modal open > with empty memoItem', async () => {
    // Arrange
    const state: Partial<RootState> = {
      memoChronicle: {
        [storeId]: {
          editMemoChronicle: {
            memoItem: {} as unknown as MemoChronicleData
          }
        }
      }
    };
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <EditMemoChronicle open={false} />
      </CustomStoreIdProvider>,
      { initialState: state }
    );
    // Assertion
    expect(wrapper.container.firstChild).not.toBeInTheDocument();
  });

  it('Case click buttons', async () => {
    // Arrange
    const mockGetRefData = jest.fn().mockResolvedValue(() => {});
    jest
      .spyOn(actionMemoChronicle, 'closeEditChronicleMemo')
      .mockImplementation(mockGetRefData);
    const invalidDate = constants.INVALID_DATE;
    (constants as any).INVALID_DATE = [''];

    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <EditMemoChronicle open={true} />
      </CustomStoreIdProvider>,
      { initialState: { ...initialState, form: {} } }
    );

    await screen.findAllByRole('button');

    const cancelButton = wrapper.baseElement.getElementsByClassName(
      'btn btn-secondary'
    )[0] as HTMLElement;
    cancelButton.click();

    const submitButton = wrapper.baseElement.getElementsByClassName(
      'btn btn-primary'
    )[0] as HTMLElement;
    submitButton.click();

    (constants as any).INVALID_DATE = invalidDate;

    // Assertion
    expect(submitButton).toBeInTheDocument();
    expect(cancelButton).toBeInTheDocument();
  });

  it('Case click buttons with Error text', async () => {
    // Arrange
    const mockGetRefData = jest.fn().mockResolvedValue(() => {});
    jest
      .spyOn(actionMemoChronicle, 'closeEditChronicleMemo')
      .mockImplementation(mockGetRefData);
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <EditMemoChronicle open={true} />
      </CustomStoreIdProvider>,
      { initialState: errorInitialState }
    );

    await screen.findAllByRole('button');

    const cancelButton = wrapper.baseElement.getElementsByClassName(
      'btn btn-secondary'
    )[0] as HTMLElement;
    cancelButton.click();

    const submitButton = wrapper.baseElement.getElementsByClassName(
      'btn btn-primary'
    )[0] as HTMLElement;
    submitButton.click();

    // Assertion
    expect(submitButton).toBeInTheDocument();
    expect(cancelButton).toBeInTheDocument();
  });
});

describe('Test edit chronicle memo > isDate return true', () => {
  it('Case edit modal open > with memoItem', async () => {
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <EditMemoChronicle open={false} />
      </CustomStoreIdProvider>,
      { initialState }
    );
    // Assertion
    expect(wrapper.container.firstChild).not.toBeInTheDocument();
  });
});
