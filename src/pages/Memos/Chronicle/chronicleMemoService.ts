import { formatDateToSubmit } from 'app/helpers';
import { apiService } from 'app/utils/api.service';

// types
import {
  AddChronicleMemoRequestData,
  EditChronicleMemoRequestData,
  DeleteChronicleMemoRequestArgs,
  GetMemoChronicleMemoRequestData
} from 'pages/Memos/Chronicle/types';

export interface MemoLookupRequest {
  common: { accountId: string };
  selectFields: string[];
  startSequence: string;
  endSequence: string;
  odsNaming?: boolean;
  enteredIdentifierRequestActionCode?: string;
  filterIdentifier?: string;
  memoDate?: string;
  memoEndDate?: string;
  memoDateCriteria?:
    | 'BETWEEN'
    | 'EQUAL'
    | 'GREATER_THAN_OR_EQUAL'
    | 'GREATER_THAN'
    | 'LESS_THAN_OR_EQUAL'
    | 'LESS_THAN';
  memoTime?: string;
  memoIdentifier?: string;
  behaviorIdentifier?: string;
}

export interface MemoChronicleBehaviorsRequest {
  common?: {
    accountId: string;
  };
  selectFields?: string[];
  odsNaming?: boolean;
  startSequence?: string;
  endSequence?: string;
  clientIdentifier?: string;
  behaviorIdentifier?: string;
}

export interface MemoChronicleFiltersRequest {
  common?: {
    accountId: string;
  };
  selectFields?: string[];
  odsNaming?: boolean;
  startSequence?: string;
  endSequence?: string;
  clientIdentifier?: string;
  filterIdentifier?: string;
}

export interface AddChronicleMemoRequest {
  common?: { accountId: string };
  behaviorIdentifier?: string;
  memoText?: string;
  clerkIdentifier?: string;
  overrideRetentionDate?: string;
  presentationInstrumentMemo?: boolean;
}

export interface EditChronicleMemoRequest {
  common?: { accountId: string };
  behaviorIdentifier?: string;
  memoText?: string;
  clerkIdentifier?: string;
  overrideRetentionDate?: string;
  presentationInstrumentMemo?: boolean;
  memoIdentifier: string;
}

const chronicleMemoService = {
  getChronicleMemosList(data: GetMemoChronicleMemoRequestData) {
    const url = window.appConfig?.api?.memo?.getChronicleMemosList || '';
    const requestBody: MemoLookupRequest = {
      common: { accountId: data.accountId },
      startSequence: data.startSequence,
      endSequence: data.endSequence,
      filterIdentifier: 'ALL',
      selectFields: [
        'memoIdentifier',
        'memoOperatorIdentifier',
        'memoDate',
        'memoTime',
        'behaviorIdentifier',
        'memoText',
        'memoDeletionDate',
        'filterIdentifier'
      ]
    };
    if (data.endDate && data.startDate) {
      requestBody.memoDate = formatDateToSubmit(data.startDate);
      requestBody.memoEndDate = formatDateToSubmit(data.endDate);
      requestBody.memoDateCriteria = 'BETWEEN';
    } else {
      requestBody.memoDate =
        data.memoDate !== undefined
          ? formatDateToSubmit(data.memoDate)
          : undefined;
    }
    return apiService.post(url, requestBody);
  },

  getMemoChronicleBehaviors(requestData?: MemoChronicleBehaviorsRequest) {
    const defaultRequestBody = {
      selectFields: ['behaviorDescriptionText', 'behaviorIdentifier'],
      startSequence: 1,
      endSequence: 1000
    };

    const requestBody = requestData
      ? {
          ...defaultRequestBody,
          ...requestData
        }
      : defaultRequestBody;
    const url = window.appConfig?.api?.memo?.getMemoChronicleBehaviors || '';
    return apiService.post(url, requestBody);
  },

  getMemoChronicleFilters(requestData?: MemoChronicleFiltersRequest) {
    const defaultRequestBody = {
      selectFields: ['filterDescriptionText', 'filterIdentifier'],
      startSequence: 1,
      endSequence: 1000
    };

    const requestBody = requestData
      ? {
          ...defaultRequestBody,
          ...requestData
        }
      : defaultRequestBody;
    const url = window.appConfig?.api?.memo?.getMemoChronicleFilters || '';
    return apiService.post(url, requestBody);
  },

  deleteChronicleMemo(requestData: DeleteChronicleMemoRequestArgs['postData']) {
    const url = window.appConfig?.api?.memo?.deleteChronicleMemo || '';
    return apiService.post(url, {
      common: { accountId: requestData.accountId },
      memoIdentifier: requestData.memoIdentifier
    });
  },

  addChronicleMemo(requestData: AddChronicleMemoRequestData, token?: string) {
    const requestBody: AddChronicleMemoRequest = {
      common: { accountId: requestData.accountId },
      behaviorIdentifier: requestData?.memoType?.value,
      memoText: requestData.memoText ?? ''
    };
    if (requestData.overrideRetentionDate) {
      const newOverrideRetentionDate = formatDateToSubmit(
        requestData.overrideRetentionDate
      );
      requestBody.overrideRetentionDate = newOverrideRetentionDate;
    }
    const url = window.appConfig?.api?.memo?.addChronicleMemo || '';
    return apiService.post(url, requestBody, {
      headers: { Authorization: token }
    });
  },

  updateChronicleMemo(requestData: EditChronicleMemoRequestData) {
    const requestBody: EditChronicleMemoRequest = {
      common: { accountId: requestData.accountId },
      behaviorIdentifier: requestData.memoType?.value,
      memoText: requestData.memoText ?? '',
      memoIdentifier: requestData.memoIdentifier
    };
    if (requestData.overrideRetentionDate === null) {
      requestBody.overrideRetentionDate = undefined;
    }
    if (requestData.overrideRetentionDate) {
      const newOverrideRetentionDate = formatDateToSubmit(
        requestData.overrideRetentionDate
      );
      requestBody.overrideRetentionDate = newOverrideRetentionDate;
    }
    const url = window.appConfig?.api?.memo?.updateChronicleMemo || '';
    return apiService.post(url, requestBody);
  }
};

export default chronicleMemoService;
