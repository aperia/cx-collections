import React, { useEffect } from 'react';

// components
import { Button, Icon, Tooltip } from 'app/_libraries/_dls/components';

// hooks
import { useCustomStoreIdSelector, useCustomStoreId } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useDispatch } from 'react-redux';
import { selectIsExpand } from 'pages/Memos/Chronicle/_redux/selectors';
import { actionMemoChronicle } from 'pages/Memos/Chronicle/_redux/reducers';

// i18n
import { I18N_MEMOS } from '../../constants';

const ExpandMemoChronicle = () => {
  const dispatch = useDispatch();
  const { customStoreId: storeId } = useCustomStoreId();
  const { t } = useTranslation();

  const isExpand = useCustomStoreIdSelector<boolean>(selectIsExpand);

  const handleExpand = () => {
    dispatch(actionMemoChronicle.setExpand({ isExpand: !isExpand, storeId }));
  };

  useEffect(() => {
    return () => {
      dispatch(actionMemoChronicle.removeChronicleMemo({ storeId }));
    };
  }, [dispatch, storeId]);

  return (
    <Tooltip
      element={isExpand ? t(I18N_MEMOS.COLLAPSE) : t(I18N_MEMOS.EXPAND)}
      placement="top"
      variant="primary"
      dataTestId="chronicle-memo-header_expand-tooltip"
    >
      <Button
        id="chronicle-memo-header_expand-btn"
        onClick={handleExpand}
        variant="icon-secondary"
        dataTestId="chronicle-memo-header_expand-btn"
      >
        <Icon
          name={isExpand ? 'collapse-all' : 'expand-all'}
          dataTestId="chronicle-memo-header_expand-icon"
        />
      </Button>
    </Tooltip>
  );
};

export default ExpandMemoChronicle;
