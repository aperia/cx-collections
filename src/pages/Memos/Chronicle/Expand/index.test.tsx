import React from 'react';
import { queryHelpers } from '@testing-library/dom';
import { CustomStoreIdProvider } from 'app/hooks';
import { renderMockStore, storeId } from 'app/test-utils';
import ExpandMemoChronicle from './index';

describe('Test Expand Chronicle memo component', () => {
  it('Should render expand all icon', () => {
    // Arrange
    const initialState = {
      memoChronicle: {
        [storeId]: {
          isExpand: false
        }
      }
    };
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <ExpandMemoChronicle />
      </CustomStoreIdProvider>,
      { initialState },
      true
    );
    // Assertion
    expect(
      queryHelpers.queryByAttribute(
        'class',
        wrapper.container,
        'icon icon-expand-all'
      )
    ).toBeInTheDocument();
  });

  it('Should render collapse all icon', () => {
    // Arrange
    const initialState = {
      memoChronicle: {
        [storeId]: {
          isExpand: true
        }
      }
    };
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <ExpandMemoChronicle />
      </CustomStoreIdProvider>,
      { initialState },
      true
    );
    // Assertion
    expect(
      queryHelpers.queryByAttribute(
        'class',
        wrapper.container,
        'icon icon-expand-all'
      )
    ).not.toBeInTheDocument();
    expect(
      queryHelpers.queryByAttribute(
        'class',
        wrapper.container,
        'icon icon-collapse-all'
      )
    ).toBeInTheDocument();
  });

  it('Click button', () => {
    // Arrange
    const initialState = {
      memoChronicle: {
        [storeId]: {
          isExpand: false
        }
      }
    };
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <ExpandMemoChronicle />
      </CustomStoreIdProvider>,
      { initialState },
      true
    );

    expect(
      queryHelpers.queryByAttribute(
        'class',
        wrapper.container,
        'icon icon-expand-all'
      )
    ).toBeInTheDocument();

    const button = queryHelpers.queryByAttribute(
      'class',
      wrapper.container,
      'btn btn-icon-secondary'
    );
    button?.click();
    // Assertion
    expect(
      queryHelpers.queryByAttribute(
        'class',
        wrapper.container,
        'icon icon-collapse-all'
      )
    ).toBeInTheDocument();
  });
});
