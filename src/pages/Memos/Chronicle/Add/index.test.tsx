import React from 'react';
import { screen } from '@testing-library/react';
import { queries } from '@testing-library/dom';
import { CustomStoreIdProvider } from 'app/hooks';
import { renderMockStore, storeId } from 'app/test-utils';
import AddMemoChronicle from './index';

describe('Test Add Chronicle memo', () => {
  it('Case click cancel', async () => {
    // Arrange
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <AddMemoChronicle />
      </CustomStoreIdProvider>
    );

    const enterSection = await queries.findByText(
      wrapper.container,
      'txt_enter_memo_here'
    );
    enterSection.click();

    await screen.findByText('txt_submit');

    const cancelButton = await queries.findByText(
      wrapper.container,
      'txt_cancel'
    );
    cancelButton.click();

    // Assertion
    expect(enterSection).toBeEnabled();
  });

  it('Case click submit', async () => {
    // Arrange
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <AddMemoChronicle />
      </CustomStoreIdProvider>
    );

    const enterSection = await queries.findByText(
      wrapper.container,
      'txt_enter_memo_here'
    );
    enterSection.click();

    await screen.findByText('txt_submit');

    const submitButton = await queries.findByText(
      wrapper.container,
      'txt_submit'
    );
    submitButton.click();

    // Assertion
    expect(submitButton).toBeEnabled();
  });
});
