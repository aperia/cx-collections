import React, { useState, useRef, useEffect, useCallback } from 'react';

// dof
import { View, ExtraFieldProps } from 'app/_libraries/_dof/core';

// components
import { Button } from 'app/_libraries/_dls/components';
import AddSection from 'app/components/AddSection';

// hooks
import {
  useAccountDetail,
  useCustomStoreIdSelector,
  useCustomStoreId
} from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { AddMemoChronicleView, AddMemoChronicleDataField } from '../constants';

// redux-store
import { useSelector, useDispatch } from 'react-redux';
import { isInvalid, Field } from 'redux-form';
import { actionMemoChronicle } from 'pages/Memos/Chronicle/_redux/reducers';
import { AppState } from 'storeConfig';
import {
  selectAddFormChronicleMemo,
  selectChronicleMemoTypeList
} from 'pages/Memos/Chronicle/_redux/selectors';
import { triggerAddChronicleMemo } from 'pages/Memos/Chronicle/_redux/add';

// i18n
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

export interface AddMemoChronicleProps {}

export interface AddFormChronicle {
  memoText?: string;
  memoType: {
    value: string;
    description: string;
  };
  overrideRetentionDate?: Date;
}

const AddMemoChronicle: React.FC<AddMemoChronicleProps> = () => {
  // ref
  const viewRef = useRef<any>(null);

  // hooks
  const dispatch = useDispatch();

  const { customStoreId: storeId } = useCustomStoreId();
  const { accEValue } = useAccountDetail();

  const { t } = useTranslation();

  // state
  const [openForm, setOpenForm] = useState(false);

  // variables
  const isFormInValid = useSelector<AppState, boolean>(
    isInvalid(`${storeId}-${AddMemoChronicleView.ID}`)
  );

  const memoTypeData = useCustomStoreIdSelector<RefDataValue[]>(
    selectChronicleMemoTypeList
  );
  const addFormValue = useCustomStoreIdSelector<AddFormChronicle>(
    selectAddFormChronicleMemo
  );

  // handlers
  const handleOpenForm = () => {
    setOpenForm(true);
    process.nextTick(() => {
      const memoType: Field<ExtraFieldProps> = viewRef.current?.props.onFind(
        AddMemoChronicleDataField.MEMO_TYPE
      );
      memoType?.props?.props.setData(memoTypeData);
    });
  };

  const handleCloseForm = () => {
    setOpenForm(false);
  };

  const handleSubmit = useCallback(() => {
    const values = addFormValue;
    dispatch(
      triggerAddChronicleMemo({
        storeId,
        postData: { ...values, accountId: accEValue },
        onSuccess: handleCloseForm
      })
    );
  }, [accEValue, dispatch, storeId, addFormValue]);

  // life cycle
  useEffect(() => {
    dispatch(
      actionMemoChronicle.getMemoChronicleBehaviors({
        storeId,
        postData: {
          accountId: accEValue
        }
      })
    );
    dispatch(
      actionMemoChronicle.getMemoChronicleFilters({
        storeId,
        postData: {
          accountId: accEValue
        }
      })
    );
  }, [accEValue, dispatch, storeId]);

  // render
  return !openForm ? (
    <AddSection
      id={`${storeId}-add-chronicle-section`}
      text="txt_enter_memo_here"
      dataTestId="add-memo-chronicle-section"
      onClick={handleOpenForm}
    />
  ) : (
    <div className="py-16 px-24 bg-light-l20 border-bottom">
      <View
        id={`${storeId}-${AddMemoChronicleView.ID}`}
        formKey={`${storeId}-${AddMemoChronicleView.ID}`}
        descriptor={AddMemoChronicleView.DESCRIPTOR}
        ref={viewRef}
      />
      <div className="d-flex justify-content-end mt-24">
        <Button
          size="sm"
          variant="secondary"
          onClick={handleCloseForm}
          dataTestId="add-memo-chronicle_cancel-btn"
        >
          {t(I18N_COMMON_TEXT.CANCEL)}
        </Button>
        <Button
          size="sm"
          disabled={isFormInValid}
          variant="primary"
          onClick={handleSubmit}
          dataTestId="add-memo-chronicle_submit-btn"
        >
          {t(I18N_COMMON_TEXT.SUBMIT)}
        </Button>
      </div>
    </div>
  );
};

export default AddMemoChronicle;
