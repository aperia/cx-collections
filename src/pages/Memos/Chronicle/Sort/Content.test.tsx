import React from 'react';
import { screen } from '@testing-library/react';
import { CustomStoreIdProvider } from 'app/hooks';
import { renderMockStore, storeId } from 'app/test-utils';
import SortAndOrder from './Content';

describe('Test Content component', () => {
  it('Should', async () => {
    // Arrange
    const initialState = {
      memoChronicle: {
        [storeId]: {
          configs: {
            sort: {}
          }
        }
      }
    };
    // Act
    renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <SortAndOrder onClose={jest.fn()} />
      </CustomStoreIdProvider>,
      { initialState } as MagicKeyValue,
      true
    );

    const resetBtn = await screen.findByText('txt_reset_to_default');
    resetBtn.click();

    const ascInput = await screen.findByText('txt_ascending');
    ascInput.click();

    const applyBtn = await screen.findByText('txt_apply');
    applyBtn.click();
    // Assertion
    expect(resetBtn).toBeInTheDocument();
    expect(ascInput).toBeInTheDocument();
    expect(applyBtn).toBeInTheDocument();
  });
});
