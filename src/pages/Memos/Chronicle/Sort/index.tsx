import React, { useState } from 'react';
import { useSelector } from 'react-redux';

// component
import { Popover, Tooltip, Button, Icon } from 'app/_libraries/_dls/components';
import SortContent from './Content';

// helper/ utils
import classnames from 'classnames';

// redux/selector
import { useCustomStoreId } from 'app/hooks';
import { selectHasIndicator } from 'pages/Memos/Chronicle/_redux/selectors';
import { I18N_MEMOS } from '../../constants';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

const Sort = () => {
  const [openSort, setOpenSort] = useState(false);
  const { customStoreId: storeId } = useCustomStoreId();
  const { t } = useTranslation();

  const { isHasSortIndicator } = useSelector<
    RootState,
    Record<string, boolean>
  >(state => selectHasIndicator(state, storeId));

  return (
    <Tooltip
      element={t(I18N_MEMOS.SORT_ORDER)}
      placement="top"
      variant="primary"
    >
      <Popover
        element={<SortContent onClose={() => setOpenSort(false)} />}
        placement="bottom-end"
        containerClassName="inside-infobar"
        size="md"
        opened={openSort}
        onVisibilityChange={value => setOpenSort(value)}
      >
        <Button
          variant="icon-secondary"
          className={classnames({
            active: openSort,
            asterisk: isHasSortIndicator
          })}
          onClick={() => setOpenSort(value => !value)}
          dataTestId="chronicle-memo-header_sort-btn"
        >
          <Icon name="sort-by" />
        </Button>
      </Popover>
    </Tooltip>
  );
};

export default Sort;
