import React, { memo, useState } from 'react';

// component
import { Button, Radio } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helper / util
import isFunction from 'lodash.isfunction';

// type
import {
  MemoChronicleSort,
  MemoChronicleSortType,
  MemoChronicleSortField,
  MemoChronicleConfig
} from 'pages/Memos/Chronicle/types';

// redux /selector
import { useDispatch, useSelector } from 'react-redux';
import { actionMemoChronicle } from 'pages/Memos/Chronicle/_redux/reducers';
import { useCustomStoreId } from 'app/hooks';
import { selectConfigChronicleMemo } from 'pages/Memos/Chronicle/_redux/selectors';
import { AppState } from 'storeConfig';

// constants
import { SORT_DEFAULT } from '../constants';

// i18n
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { I18N_MEMOS } from '../../constants';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export interface SortAndOrderProps {
  onClose?: () => void;
}

const SortAndOrder: React.FC<SortAndOrderProps> = ({ onClose }) => {
  const dispatch = useDispatch();

  const { customStoreId: storeId } = useCustomStoreId();
  const { t } = useTranslation();

  const { sort: sortReducer } = useSelector<AppState, MemoChronicleConfig>(
    state => selectConfigChronicleMemo(state, storeId)
  );
  const [sort, setSort] = useState<MemoChronicleSort | undefined>(sortReducer);

  const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const type = event.target.value as MemoChronicleSortType;
    setSort({
      field: MemoChronicleSortField.DATE_TIME,
      type
    });
  };

  const handleApply = () => {
    dispatch(
      actionMemoChronicle.setConfigsFilter({ storeId, params: { sort } })
    );
    dispatch(
      apiErrorNotificationAction.clearApiErrorData({
        storeId,
        forSection: 'inMemoFlyOut'
      })
    );
    isFunction(onClose) && onClose();
  };

  const handleResetSort = () => {
    setSort(SORT_DEFAULT);
  };

  return (
    <div>
      <h4
        className="mb-16"
        data-testid={genAmtId('chronicle-memo-header', 'sort-order-title', '')}
      >
        {t(I18N_MEMOS.SORT_ORDER)}
      </h4>
      <div className="d-flex">
        <div className="d-flex flex-column mr-24">
          <div
            className="fw-500 color-grey fs-14"
            data-testid={genAmtId('chronicle-memo-header', 'sort-by', '')}
          >
            {t(I18N_COMMON_TEXT.SORT_BY)}:
          </div>
          <div
            className="fw-500 color-grey fs-14 mt-20"
            data-testid={genAmtId('chronicle-memo-header', 'order-by', '')}
          >
            {t(I18N_COMMON_TEXT.ORDER_BY)}:
          </div>
        </div>
        <div className="flex-column d-flex">
          <div
            data-testid={genAmtId(
              'chronicle-memo-header',
              'sort-by-date-and-time',
              ''
            )}
          >
            {t(I18N_MEMOS.DATE_AND_TIME)}
          </div>
          <div className="mt-20 d-flex">
            <Radio
              className="mr-24"
              dataTestId="chronicle-memo-header_sort-by-asc"
            >
              <Radio.Input
                value={MemoChronicleSortType.ASC}
                name="sortBy"
                id={MemoChronicleSortType.ASC}
                checked={MemoChronicleSortType.ASC === sort?.type}
                onChange={handleOnChange}
              />
              <Radio.Label>{t(I18N_COMMON_TEXT.ASCENDING)}</Radio.Label>
            </Radio>
            <Radio dataTestId="chronicle-memo-header_sort-by-desc">
              <Radio.Input
                value={MemoChronicleSortType.DESC}
                name="sortBy"
                id={MemoChronicleSortType.DESC}
                checked={MemoChronicleSortType.DESC === sort?.type}
                onChange={handleOnChange}
              />
              <Radio.Label>{t(I18N_COMMON_TEXT.DESCENDING)}</Radio.Label>
            </Radio>
          </div>
        </div>
      </div>
      <div className="d-flex mt-24 justify-content-end">
        <Button
          size="sm"
          variant="secondary"
          onClick={handleResetSort}
          dataTestId="chronicle-memo-header_sort-by_reset-to-default-btn"
        >
          {t(I18N_MEMOS.RESET_TO_DEFAULT)}
        </Button>
        <Button
          size="sm"
          variant="primary"
          onClick={handleApply}
          dataTestId="chronicle-memo-header_sort-by_apply-btn"
        >
          {t(I18N_COMMON_TEXT.APPLY)}
        </Button>
      </div>
    </div>
  );
};

export default memo(SortAndOrder);
