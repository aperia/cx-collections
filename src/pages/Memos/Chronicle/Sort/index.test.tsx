import React from 'react';
import { act, queryHelpers, screen } from '@testing-library/react';
import { CustomStoreIdProvider } from 'app/hooks';
import { renderMockStore, storeId } from 'app/test-utils';
import Sort from './index';
import userEvent from '@testing-library/user-event';

describe('Test Sort Chronicle memo component', () => {
  it('Should open sort popover', async () => {
    // Arrange
    const initialState = {
      memoChronicle: {
        [storeId]: {
          configs: {
            sort: {}
          }
        }
      }
    };
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <Sort />
      </CustomStoreIdProvider>,
      { initialState } as MagicKeyValue,
      true
    );

    const button = await queryHelpers.queryByAttribute(
      'class',
      wrapper.container,
      'btn btn-icon-secondary asterisk'
    );
    userEvent.click(button!);

    // Assertion
    expect(screen.getByText('txt_ascending')).toBeInTheDocument();
    expect(screen.getByText('txt_descending')).toBeInTheDocument();
    expect(screen.getByText('txt_reset_to_default')).toBeInTheDocument();
    expect(screen.getByText('txt_apply')).toBeInTheDocument();
  });

  it('Should close sort popover', async () => {
    // Arrange
    const initialState = {
      memoChronicle: {
        [storeId]: {
          configs: {
            sort: {}
          }
        }
      }
    };
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <Sort />
      </CustomStoreIdProvider>,
      { initialState } as MagicKeyValue,
      true
    );

    const button = await queryHelpers.queryByAttribute(
      'class',
      wrapper.container,
      'btn btn-icon-secondary asterisk'
    );
    userEvent.click(button!);

    expect(screen.getByText('txt_ascending')).toBeInTheDocument();
    expect(screen.getByText('txt_descending')).toBeInTheDocument();
    expect(screen.getByText('txt_reset_to_default')).toBeInTheDocument();
    expect(screen.getByText('txt_apply')).toBeInTheDocument();

    const applyBtn = screen.getByText('txt_apply');
    act(() => {
      applyBtn.click();
    });

    // Assertion
    expect(button).toBeInTheDocument();
  });

  it('Should close popover when click out side', async () => {
    // Arrange
    const initialState = {
      memoChronicle: {
        [storeId]: {
          configs: {
            sort: {}
          }
        }
      }
    };
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <Sort />
        <div onClick={() => {}}>Out</div>
      </CustomStoreIdProvider>,
      { initialState } as MagicKeyValue,
      true
    );

    const button = await queryHelpers.queryByAttribute(
      'class',
      wrapper.container,
      'btn btn-icon-secondary asterisk'
    );
    userEvent.click(button!);

    const applyBtn = screen.getByText('txt_apply');
    act(() => {
      applyBtn.click();
    });

    userEvent.click(button!);

    expect(screen.getByText('txt_ascending')).toBeInTheDocument();
    expect(screen.getByText('txt_descending')).toBeInTheDocument();
    expect(screen.getByText('txt_reset_to_default')).toBeInTheDocument();
    expect(screen.getByText('txt_apply')).toBeInTheDocument();

    const outSide = screen.getByText('Out');
    act(() => {
      userEvent.click(outSide);
    });

    // Assertion
    expect(button).toBeInTheDocument();
  });
});
