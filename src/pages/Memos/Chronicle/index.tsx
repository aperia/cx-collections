import React, { useRef } from 'react';

// components
import AddMemoChronicle from './Add';
import List from './List';
import EditMemoChronicle from './Edit';
import { MemoProps } from '..';
import { SimpleBar } from 'app/_libraries/_dls/components';

// hooks
import { useCustomStoreId, useMemoProvider, useAccountDetail } from 'app/hooks';

// redux store
import { useSelector } from 'react-redux';
import { AppState } from 'storeConfig';
import { selectLoading } from 'pages/Memos/Chronicle/_redux/selectors';

// helper
import classnames from 'classnames';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

export interface MemoChronicleProps extends MemoProps {}

const MemoChronicle: React.FC<MemoChronicleProps> = ({
  shouldRenderPinnedIcon,
  className
}) => {
  // refs
  const addMemoRef = useRef<HTMLDivElement | null>(null);

  // hooks
  const { isHiddenAddSection = false } = useMemoProvider();

  const { customStoreId: storeId } = useCustomStoreId();
  const { storeId: accStoreId } = useAccountDetail();

  // variables
  const getMemoLoading = useSelector<AppState, boolean>(state =>
    selectLoading(state, storeId)
  );

  const addMemoLoading = useSelector<AppState, boolean>(
    state => state.memoChronicle[storeId]?.addMemoChronicle?.loading ?? false
  );

  const openEdit = useSelector<AppState, boolean>(state => {
    return state.memoChronicle[storeId]?.editMemoChronicle?.open ?? false;
  });

  const isLoading = getMemoLoading || addMemoLoading;

  return (
    <div className={classnames('h-100', { loading: isLoading }, className)}>
      <SimpleBar>
        {checkPermission(PERMISSIONS.ADD_MEMO, accStoreId) && (
          <div
            className={classnames('comment-info-bar', {
              'd-none': isHiddenAddSection
            })}
            ref={addMemoRef}
          >
            <AddMemoChronicle />
          </div>
        )}
        <div>
          <List shouldRenderPinnedIcon={shouldRenderPinnedIcon} />
        </div>
        <EditMemoChronicle open={openEdit} />
      </SimpleBar>
    </div>
  );
};

export default MemoChronicle;
