import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';

// types
import {
  MemoChronicleAction,
  MemoChronicleState,
  AddMemoChronicleArgs
} from '../types';

// helpers
import { batch } from 'react-redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { actionMemoChronicle } from './reducers';
import { GetListType } from '../constants';
import isFunction from 'lodash.isfunction';

// I18N
import { I18N_MEMOS } from '../../constants';

import chronicleMemoService from '../chronicleMemoService';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { clientConfigMemosServices } from 'pages/ClientConfiguration/Memos/clientConfigMemosService';
import { defaultChronicleType } from 'pages/TransactionAdjustment/RequestList/constants';
import { CHRONICLE_DEFAULT_KEY } from 'pages/ClientConfiguration/Memos/constants';
import find from 'lodash.find';
import { getClientInfoData } from 'pages/AccountDetails/_redux/getClientInfoById';
import { isEmpty } from 'app/_libraries/_dls/lodash';

const component = 'component_memos';

export const addMemoChronicle = createAsyncThunk<
  unknown,
  AddMemoChronicleArgs,
  ThunkAPIConfig
>(MemoChronicleAction.ADD_MEMO_CHRONICLE, async (args, thunkAPI) => {
  const { dispatch } = thunkAPI;
  try {
    const { storeId } = args;
    let chronicleType = '';
    const { cspa, chronicleKey, actionCode, actionType } = args?.data || {};

    if (actionCode) {
      const response = await dispatch(
        getClientInfoData({
          dataType: 'actionCodes',
          storeId: storeId
        })
      );

      if (isFulfilled(response)) {
        const actionCodes = response.payload.actionCodes || [];
        const actionTypes = find(
          actionCodes,
          item => item?.actionCode?.toLowerCase() === actionCode?.toLowerCase()
        )?.actionTypes;

        chronicleType = defaultChronicleType;

        if (!isEmpty(actionTypes)) {
          chronicleType =
            find(
              actionTypes,
              item =>
                item.actionTypeName?.toLowerCase() === actionType?.toLowerCase()
            )?.chronicleBehaviorIdentifier ||
            actionTypes![0].chronicleBehaviorIdentifier;
        }
      }
    } else if (chronicleKey) {
      const { app, org } = window?.appConfig?.commonConfig || {};
      const { accountDetail } = thunkAPI.getState();
      const {
        clientIdentifier,
        systemIdentifier,
        principalIdentifier,
        agentIdentifier
      } = accountDetail[storeId]?.rawData || {};

      const _cspa =
        !cspa || isEmpty(cspa)
          ? {
              clientNumber: clientIdentifier,
              system: systemIdentifier,
              prin: principalIdentifier,
              agent: agentIdentifier
            }
          : cspa;

      const { data } = await clientConfigMemosServices.getMemoConfig({
        common: { app, org, ..._cspa },
        component,
        configs: 'clientConfig'
      });

      const { jsonValue } =
        (data.codeToText?.components || []).find(
          (item: MagicKeyValue) => item.component === component
        ) || {};

      chronicleType =
        jsonValue?.[chronicleKey] ||
        jsonValue?.[CHRONICLE_DEFAULT_KEY] ||
        defaultChronicleType;
    }

    const postData = chronicleType
      ? {
          ...args.postData,
          memoType: {
            value: chronicleType,
            description: chronicleType
          }
        }
      : {
          ...args.postData,
          memoType: {
            value: args?.postData?.memoType?.value || defaultChronicleType,
            description:
              args?.postData?.memoType?.description || defaultChronicleType
          }
        };

    return await chronicleMemoService.addChronicleMemo(postData, args?.token);
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerAddChronicleMemo = createAsyncThunk<
  unknown,
  AddMemoChronicleArgs,
  ThunkAPIConfig
>(
  `${MemoChronicleAction.ADD_MEMO_CHRONICLE}/triggerAdd`,
  async (args, { dispatch }) => {
    const { storeId, postData, onSuccess, onFailure, token } = args;
    const response = await dispatch(
      addMemoChronicle({
        storeId,
        postData,
        token
      })
    );

    // add memo success
    if (isFulfilled(response)) {
      isFunction(onSuccess) && onSuccess();
      batch(() => {
        // success toast
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_MEMOS.MEMOS_ADD_SUCCESS
          })
        );
        // get memo list
        dispatch(
          actionMemoChronicle.getMemoChronicle({
            storeId,
            postData: {
              type: GetListType.REFRESH,
              accountId: postData.accountId
            }
          })
        );
        dispatch(
          apiErrorNotificationAction.clearApiErrorData({
            storeId,
            forSection: 'inMemoFlyOut'
          })
        );
      });
    }

    if (isRejected(response)) {
      isFunction(onFailure) && onFailure();
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'error',
            message: I18N_MEMOS.MEMOS_ADD_FAILURE
          })
        );
        dispatch(
          apiErrorNotificationAction.updateApiError({
            storeId,
            forSection: 'inMemoFlyOut',
            apiResponse: response.payload?.response
          })
        );
      });
    }
  }
);

export const addMemoChronicleBuilder = (
  builder: ActionReducerMapBuilder<MemoChronicleState>
) => {
  builder
    .addCase(addMemoChronicle.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        addMemoChronicle: {
          ...draftState[storeId]?.addMemoChronicle,
          loading: true
        }
      };
    })
    .addCase(addMemoChronicle.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        addMemoChronicle: {
          ...draftState[storeId]?.addMemoChronicle,
          loading: false
        }
      };
    })
    .addCase(addMemoChronicle.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        addMemoChronicle: {
          ...draftState[storeId]?.addMemoChronicle,
          error: true,
          loading: false
        }
      };
    });
};
