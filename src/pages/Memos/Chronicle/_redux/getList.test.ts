import {
  CONFIG_DEFAULT,
  FIRST_SEQUENCE,
  GetListType,
  ITEM_PER_PAGE,
  SEARCH_DEFAULT,
  SORT_DEFAULT
} from '../constants';

// redux
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { getMemoChronicle } from 'pages/Memos/Chronicle/_redux/getList';

import chronicleMemoService from '../chronicleMemoService';
import { responseDefault, storeId } from 'app/test-utils';
import { GetMemoChroniclePostData, MemoChronicleConfig } from '../types';
import { NO_RESULT_CODE } from 'pages/AccountSearch/AccountSearchList/constants';

const postData: GetMemoChroniclePostData = {
  accountId: storeId,
  type: GetListType.LOAD_MORE
};

describe('Memos > getList', () => {
  let store: Store<RootState>;
  let storeEmpty: Store<RootState>;

  const configs: MemoChronicleConfig = {
    search: { memoDate: new Date().toString() }
  };

  beforeEach(() => {
    store = createStore(rootReducer, {
      memoChronicle: { [storeId]: { configs } }
    });

    storeEmpty = createStore(rootReducer, {});
  });

  describe('success', () => {
    it('with over 50 memos', async () => {
      const memos = new Array(51).fill({});
      jest
        .spyOn(chronicleMemoService, 'getChronicleMemosList')
        .mockResolvedValue({
          ...responseDefault,
          data: { memos }
        });

      const response = await getMemoChronicle({
        storeId,
        postData: {
          accountId: storeId,
          type: GetListType.FIRST
        }
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({
        memos,
        configs: {
          end: (configs.end! || 0) + ITEM_PER_PAGE,
          start: FIRST_SEQUENCE,
          search: SEARCH_DEFAULT,
          sort: SORT_DEFAULT
        }
      });
    });

    describe('with state', () => {
      it('FIRST', async () => {
        jest
          .spyOn(chronicleMemoService, 'getChronicleMemosList')
          .mockResolvedValue({ ...responseDefault, data: [] });

        const response = await getMemoChronicle({
          storeId,
          postData: {
            accountId: storeId,
            type: GetListType.FIRST
          }
        })(store.dispatch, store.getState, {});

        expect(response.payload).toEqual({
          memos: [],
          configs: {
            end: (configs.end! || 0) + ITEM_PER_PAGE,
            start: FIRST_SEQUENCE,
            search: SEARCH_DEFAULT,
            sort: SORT_DEFAULT
          }
        });
      });

      it('LOAD_MORE', async () => {
        jest
          .spyOn(chronicleMemoService, 'getChronicleMemosList')
          .mockResolvedValue({ ...responseDefault, data: [] });

        const response = await getMemoChronicle({
          storeId,
          postData: {
            accountId: storeId,
            type: GetListType.LOAD_MORE
          }
        })(store.dispatch, store.getState, {});

        expect(response.payload).toEqual({
          memos: [],
          configs: {
            end: (configs.end! || 0) + ITEM_PER_PAGE,
            start: (configs.end! || 0) + 1,
            search: configs.search!
          }
        });
      });

      describe('REFRESH', () => {
        it('with memoDate', async () => {
          const memoDate = new Date().toString();

          jest
            .spyOn(chronicleMemoService, 'getChronicleMemosList')
            .mockResolvedValue({ ...responseDefault, data: [] });

          const response = await getMemoChronicle({
            storeId,
            postData: {
              accountId: storeId,
              type: GetListType.REFRESH,
              memoDate
            }
          })(store.dispatch, store.getState, {});

          expect(response.payload).toEqual({
            memos: [],
            configs: {
              end: (configs.end! || 0) + ITEM_PER_PAGE,
              start: (configs.end! || 0) + 1,
              search: { ...configs.search!, memoDate }
            }
          });
        });

        it('without memoDate', async () => {
          jest
            .spyOn(chronicleMemoService, 'getChronicleMemosList')
            .mockResolvedValue({ ...responseDefault, data: [] });

          const response = await getMemoChronicle({
            storeId,
            postData: {
              accountId: storeId,
              type: GetListType.REFRESH
            }
          })(store.dispatch, store.getState, {});

          expect(response.payload).toEqual({
            memos: [],
            configs: {
              end: (configs.end! || 0) + ITEM_PER_PAGE,
              start: (configs.end! || 0) + 1,
              search: configs.search!
            }
          });
        });
      });

      it('RESET_SEARCH', async () => {
        jest
          .spyOn(chronicleMemoService, 'getChronicleMemosList')
          .mockResolvedValue({ ...responseDefault, data: [] });

        const response = await getMemoChronicle({
          storeId,
          postData: {
            accountId: storeId,
            type: GetListType.RESET_SEARCH
          }
        })(store.dispatch, store.getState, {});

        expect(response.payload).toEqual({
          memos: [],
          configs: {
            end: ITEM_PER_PAGE,
            start: FIRST_SEQUENCE,
            search: SEARCH_DEFAULT,
            sort: configs!.sort
          }
        });
      });

      it('default', async () => {
        jest
          .spyOn(chronicleMemoService, 'getChronicleMemosList')
          .mockResolvedValue({ ...responseDefault, data: [] });

        const response = await getMemoChronicle({
          storeId,
          postData: {
            accountId: storeId
          } as unknown as GetMemoChroniclePostData
        })(store.dispatch, store.getState, {});

        expect(response.payload).toEqual({
          memos: [],
          configs: {
            end: ITEM_PER_PAGE,
            start: FIRST_SEQUENCE,
            search: {
              ...configs?.search,
              memoDate: configs?.search?.memoDate,
              startDate: configs?.search?.memoDateRange?.start,
              endDate: configs?.search?.memoDateRange?.end
            }
          }
        });
      });
    });

    it('with empty state', async () => {
      jest
        .spyOn(chronicleMemoService, 'getChronicleMemosList')
        .mockResolvedValue({ ...responseDefault, data: [] });
      const response = await getMemoChronicle({
        storeId,
        postData: {} as unknown as GetMemoChroniclePostData
      })(storeEmpty.dispatch, storeEmpty.getState, {});

      expect(response.payload).toEqual({ memos: [], configs: CONFIG_DEFAULT });
    });
  });

  describe('error', () => {
    it('455 error', async () => {
      jest
        .spyOn(chronicleMemoService, 'getChronicleMemosList')
        .mockRejectedValue({
          response: {
            status: '455'
          },
          data: { message: 'NO MEMOS FOR THIS ACCOUNT' }
        });

      const response = await getMemoChronicle({
        storeId,
        postData
      })(store.dispatch, store.getState, {});

      expect((response.payload as any)?.data).toBeUndefined();
    });

    it('455 error', async () => {
      const dataError = {
        response: { status: NO_RESULT_CODE },
        data: { message: 'NO MEMOS FOR THIS ACCOUNT' }
      };
      jest
        .spyOn(chronicleMemoService, 'getChronicleMemosList')
        .mockImplementation(() => {
          throw dataError;
        });

      const response = await getMemoChronicle({
        storeId,
        postData
      })(store.dispatch, store.getState, {});

      expect(response.payload).toBeTruthy();
    });

    it('another error', async () => {
      jest
        .spyOn(chronicleMemoService, 'getChronicleMemosList')
        .mockRejectedValue({ ...responseDefault, data: {} });

      const response = await getMemoChronicle({
        storeId,
        postData
      })(store.dispatch, store.getState, {});

      expect(response.type).toEqual('getChronicleMemos/rejected');
    });
  });
});
