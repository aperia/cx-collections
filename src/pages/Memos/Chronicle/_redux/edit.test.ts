import { mockStoreId as storeId } from 'app/test-utils/mockData';

// redux
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import {
  editMemoChronicle,
  triggerEditMemoChronicle
} from 'pages/Memos/Chronicle/_redux/edit';

import chronicleMemoService from '../chronicleMemoService';
import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault
} from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { I18N_MEMOS } from 'pages/Memos/constants';
import { EditChronicleMemoRequestData } from '../types';
import { isRejected } from '@reduxjs/toolkit';

const postData: EditChronicleMemoRequestData = {
  accountId: storeId,
  memoIdentifier: '122',
  memoType: { value: '111', description: 'des' },
  memoText: 'Memo Test',
  overrideRetentionDate: new Date()
};

const initialState: Partial<RootState> = {
  memoChronicle: {
    [storeId]: {
      memos: []
    }
  }
};

const toastSpy = mockActionCreator(actionsToast);

describe('Memos > Chronicle > editMemoChronicle', () => {
  it('success', async () => {
    jest
      .spyOn(chronicleMemoService, 'updateChronicleMemo')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const store = createStore(rootReducer, initialState);

    const response = await editMemoChronicle({
      accountId: storeId,
      storeId,
      postData
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({ ...responseDefault, data: {} });
  });

  it('reject', async () => {
    jest
      .spyOn(chronicleMemoService, 'updateChronicleMemo')
      .mockRejectedValue({});

    const store = createStore(rootReducer, {});
    const response = await editMemoChronicle({
      accountId: storeId,
      storeId,
      postData
    })(store.dispatch, store.getState, {});
    expect(isRejected(response)).toBeTruthy();
  });
});

describe('Memos > Chronicle > triggerEditMemoChronicle', () => {
  describe('success', () => {
    it('isFulfilled', async () => {
      const mockOnSuccess = jest.fn();
      const mockAction = toastSpy('addToast');

      const store = createStore(rootReducer, {});
      await triggerEditMemoChronicle({
        accountId: storeId,
        storeId,
        postData,
        onSuccess: mockOnSuccess
      })(
        byPassFulfilled(triggerEditMemoChronicle.fulfilled.type),
        store.getState,
        {}
      );

      expect(mockAction).toBeCalledWith({
        show: true,
        type: 'success',
        message: I18N_MEMOS.MEMOS_UPDATE_SUCCESS
      });
      expect(mockOnSuccess).toBeCalled();
    });

    it('isFulfilled', async () => {
      const mockOnFailure = jest.fn();
      const mockAction = toastSpy('addToast');

      const store = createStore(rootReducer, {});
      await triggerEditMemoChronicle({
        accountId: storeId,
        storeId,
        postData,
        onFailure: mockOnFailure
      })(
        byPassRejected(triggerEditMemoChronicle.rejected.type),
        store.getState,
        {}
      );

      expect(mockAction).toBeCalledWith({
        show: true,
        type: 'error',
        message: I18N_MEMOS.MEMOS_UPDATE_FAILURE
      });
      expect(mockOnFailure).toBeCalled();
    });
  });
});
