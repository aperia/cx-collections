import './reducers';
import { getMemoChronicleBehaviors } from './getMemoBehaviors';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { responseDefault, storeId } from 'app/test-utils';

import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';

import chronicleMemoService from '../chronicleMemoService';

const postDataHardShip = {
  accountId: storeId,
  callResultType: CALL_RESULT_CODE.HARDSHIP
};

const initialState: Partial<RootState> = {
  mapping: {
    loading: false,
    data: {
      memoChronicleBehaviors: {}
    }
  }
};

describe('getMemoChronicleBehaviors', () => {
  describe('success', () => {
    it('with data', async () => {
      jest
        .spyOn(chronicleMemoService, 'getMemoChronicleBehaviors')
        .mockResolvedValue({ ...responseDefault, data: { behaviors: {} } });

      const store = createStore(rootReducer, initialState);
      const response = await getMemoChronicleBehaviors({
        storeId,
        postData: postDataHardShip
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual([]);
    });

    it('with empty data', async () => {
      jest
        .spyOn(chronicleMemoService, 'getMemoChronicleBehaviors')
        .mockResolvedValue({ ...responseDefault, data: {} });

      const store = createStore(rootReducer, initialState);
      const response = await getMemoChronicleBehaviors({
        storeId,
        postData: postDataHardShip
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual([]);
    });
  });

  it('reject', async () => {
    jest
      .spyOn(chronicleMemoService, 'getMemoChronicleBehaviors')
      .mockRejectedValue({});

    const store = createStore(rootReducer, initialState);
    const response = await getMemoChronicleBehaviors({
      storeId,
      postData: postDataHardShip
    })(store.dispatch, store.getState, {});

    expect(response.payload).toBeUndefined();
  });
});
