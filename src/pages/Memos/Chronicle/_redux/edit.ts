import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';

// types
import {
  MemoChronicleAction,
  MemoChronicleState,
  EditMemoChronicleArgs
} from '../types';

// helpers
import findIndex from 'lodash.findindex';
import isFunction from 'lodash.isfunction';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { getFormattedDate, trimData } from 'app/helpers';

// i18n
import { I18N_MEMOS } from '../../constants';
import chronicleMemoService from '../chronicleMemoService';
import { batch } from 'react-redux';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export const editMemoChronicle = createAsyncThunk<
  unknown,
  EditMemoChronicleArgs,
  ThunkAPIConfig
>(MemoChronicleAction.EDIT_MEMO_CHRONICLE, async (args, thunkAPI) => {
  const { postData, accountId } = args;
  try {
    const res = await chronicleMemoService.updateChronicleMemo({
      ...postData,
      accountId
    });
    return res;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerEditMemoChronicle = createAsyncThunk<
  void,
  EditMemoChronicleArgs,
  ThunkAPIConfig
>(
  `${MemoChronicleAction.EDIT_MEMO_CHRONICLE}/triggerEdit`,
  async (args, { dispatch }) => {
    const { storeId, accountId, postData, onFailure, onSuccess } = args;
    const trimmedData = trimData(postData);
    const response = await dispatch(
      editMemoChronicle({
        storeId,
        accountId,
        postData: trimmedData
      })
    );

    // update memo success
    if (isFulfilled(response)) {
      isFunction(onSuccess) && onSuccess();
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_MEMOS.MEMOS_UPDATE_SUCCESS
          })
        );
        dispatch(
          apiErrorNotificationAction.clearApiErrorData({
            forSection: 'inModalBody',
            storeId
          })
        );
        // Clear error in the fly out
        dispatch(
          apiErrorNotificationAction.clearApiErrorData({
            forSection: 'inMemoFlyOut',
            storeId
          })
        );
      });
    }

    if (isRejected(response)) {
      isFunction(onFailure) && onFailure();
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'error',
            message: I18N_MEMOS.MEMOS_UPDATE_FAILURE
          })
        );
        dispatch(
          apiErrorNotificationAction.updateApiError({
            storeId,
            forSection: 'inModalBody',
            apiResponse: response.payload?.response
          })
        );
      });
    }
  }
);

export const editMemoChronicleBuilder = (
  builder: ActionReducerMapBuilder<MemoChronicleState>
) => {
  builder
    .addCase(editMemoChronicle.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        editMemoChronicle: {
          ...draftState[storeId]?.editMemoChronicle,
          loading: true
        }
      };
    })
    .addCase(editMemoChronicle.fulfilled, (draftState, action) => {
      const { storeId, postData } = action.meta.arg;
      const updateMemoIndex = findIndex(draftState[storeId].memos, {
        memoIdentifier: postData.memoIdentifier
      });
      const newMemos = draftState[storeId]?.memos;
      const newOverrideRetentionDate: Date | string | undefined =
        postData.overrideRetentionDate;

      const dataUpdated = {
        behaviorIdentifier: postData.memoType.value,
        memoText: postData.memoText,
        memoDeletionDate: getFormattedDate(newOverrideRetentionDate)
      };
      newMemos![updateMemoIndex] = {
        ...newMemos![updateMemoIndex],
        ...dataUpdated
      };
      draftState[storeId] = {
        ...draftState[storeId],
        editMemoChronicle: {
          ...draftState[storeId]?.editMemoChronicle,
          loading: false,
          error: false
        },
        memos: newMemos
      };
    })
    .addCase(editMemoChronicle.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        editMemoChronicle: {
          ...draftState[storeId]?.editMemoChronicle,
          error: true,
          loading: false
        }
      };
    });
};
