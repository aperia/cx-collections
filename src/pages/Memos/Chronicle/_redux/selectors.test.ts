import * as selectors from './selectors';
import {
  MemoChronicleSortType,
  MemoChronicleSortField,
  MemoChronicleState
} from '../types';
import {
  CONFIG_DEFAULT,
  SORT_DEFAULT,
  SEARCH_DEFAULT,
  chronicleSearchFormView,
  AddMemoChronicleView,
  EditMemoChronicleView
} from '../constants';
import { selectorWrapper, storeId } from 'app/test-utils';
import isEqual from 'lodash.isequal';

// create store
const memoChronicle: MemoChronicleState = {
  [storeId]: {
    isExpand: true,
    loading: true,
    isLoadMore: true,
    isEnd: true,
    editMemoChronicle: {
      memoItem: {
        memoOperatorIdentifier: '',
        memoText: '',
        memoDate: '',
        memoTime: '',
        memoIdentifier: '',
        behaviorIdentifier: '',
        filterIdentifier: '',
        memoDeletionDate: ''
      }
    },
    configs: {
      sort: {
        type: MemoChronicleSortType.ASC,
        field: MemoChronicleSortField.DATE_TIME
      },
      search: {}
    },
    refData: {
      typeList: [{ value: 'test', description: 'test' }],
      groupList: [{ value: 'test', description: 'test' }]
    },
    memos: [
      {
        memoOperatorIdentifier: '74R',
        memoText: 'Quisquam officiis facere non. Incidunt error.',
        memoDate: '2020-12-20T12:36:27.880Z',
        memoTime: '2020-05-12T11:29:13.272Z',
        memoIdentifier: '1',
        behaviorIdentifier: '',
        filterIdentifier: ''
      },
      {
        memoOperatorIdentifier: '75R',
        memoText: 'Quisquam officiis facere non. Incidunt error.',
        memoDate: '2020-12-20T12:36:27.880Z',
        memoTime: '2020-05-12T11:29:13.272Z',
        memoIdentifier: '2',
        behaviorIdentifier: '',
        filterIdentifier: ''
      }
    ]
  }
};

const state: Partial<RootState> = {
  memoChronicle,
  form: {
    [`${chronicleSearchFormView}-${storeId}`]: { values: { name: 'value' } },
    [`${storeId}-${AddMemoChronicleView.ID}`]: { values: { name: 'value' } },
    [`${storeId}-${EditMemoChronicleView.ID}`]: { values: { name: 'value' } }
  }
};

const selectorTrigger = selectorWrapper(state);

describe('chronicle memo > select', () => {
  it('selectChronicleMemo', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectChronicleMemo);

    expect(data).toEqual(memoChronicle[storeId].memos);
    expect(emptyData).toEqual([]);
  });

  it('selectChronicleMemoItem', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectChronicleMemoItem
    );

    expect(data).toEqual(memoChronicle[storeId].editMemoChronicle!.memoItem);
    expect(emptyData).toBeUndefined();
  });

  it('selectConfigChronicleMemo', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectConfigChronicleMemo
    );

    expect(data).toEqual(memoChronicle[storeId].configs);
    expect(emptyData).toEqual(CONFIG_DEFAULT);
  });

  it('selectLoading', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectLoading);

    expect(data).toEqual(memoChronicle[storeId].loading);
    expect(emptyData).toBeFalsy();
  });

  it('selectLoadMore', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectLoadMore);

    expect(data).toEqual(memoChronicle[storeId].isLoadMore);
    expect(emptyData).toBeFalsy();
  });

  it('selectEndLoadMore', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectEndLoadMore);

    expect(data).toEqual(memoChronicle[storeId].isEnd);
    expect(emptyData).toBeFalsy();
  });

  it('selectEndLoadMore', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectIsExpand);

    expect(data).toEqual(memoChronicle[storeId].isExpand);
    expect(emptyData).toBeFalsy();
  });

  it('selectTotalMemo', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectTotalMemo);

    expect(data).toEqual(memoChronicle[storeId].memos!.length);
    expect(emptyData).toEqual(0);
  });

  it('selectChronicleMemoTypeList', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectChronicleMemoTypeList
    );

    expect(data).toEqual(memoChronicle[storeId].refData!.typeList);
    expect(emptyData).toEqual([]);
  });

  it('selectHasIndicator', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectHasIndicator);

    expect(data).toEqual({
      isHasSortIndicator: !isEqual(
        memoChronicle[storeId].configs!.sort,
        SORT_DEFAULT
      ),
      isHasSearchIndicator: !isEqual(
        memoChronicle[storeId].configs!.search,
        SEARCH_DEFAULT
      )
    });
    expect(emptyData).toEqual({
      isHasSortIndicator: false,
      isHasSearchIndicator: false
    });
  });

  it('selectChronicleMemoGroupList', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectChronicleMemoGroupList
    );

    expect(data).toEqual(memoChronicle[storeId].refData!.groupList);
    expect(emptyData).toEqual([]);
  });

  it('selectSearchParamsChronicleMemo', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectSearchParamsChronicleMemo
    );

    expect(data).toEqual(memoChronicle[storeId].configs!.search);
    expect(emptyData).toEqual({});
  });

  it('selectSearchFormChronicleMemo', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectSearchFormChronicleMemo
    );

    expect(data).toEqual(
      state.form![`${chronicleSearchFormView}-${storeId}`]?.values
    );
    expect(emptyData).toEqual({});
  });

  it('selectAddFormChronicleMemo', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectAddFormChronicleMemo
    );

    expect(data).toEqual(
      state.form![`${storeId}-${AddMemoChronicleView.ID}`]?.values
    );
    expect(emptyData).toEqual({});
  });

  it('selectEditFormChronicleMemo', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectEditFormChronicleMemo
    );

    expect(data).toEqual(
      state.form![`${storeId}-${EditMemoChronicleView.ID}`]?.values
    );
    expect(emptyData).toEqual({});
  });
});
