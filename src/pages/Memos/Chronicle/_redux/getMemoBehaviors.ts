import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Type
import {
  MemoChronicleState,
  MemoChronicleAction,
  GetMemoChronicleBehaviorsArgs,
  MemoChronicleType
} from '../types';

// Helper
import { mappingDataFromArray } from 'app/helpers';

// Service
import chronicleMemoService from '../chronicleMemoService';

// Const

export const getMemoChronicleBehaviors = createAsyncThunk<
  MemoChronicleType[],
  GetMemoChronicleBehaviorsArgs,
  ThunkAPIConfig
>(MemoChronicleAction.GET_CHRONICLE_BEHAVIORS, async (args, thunkAPI) => {
  const { mapping, accountDetail, tab } = thunkAPI.getState();
  const { tabStoreIdSelected: storeId } = tab;
  const { clientID } = accountDetail[storeId]?.data || {};
  const { postData } = args;

  const { data } = await chronicleMemoService.getMemoChronicleBehaviors({
    common: { accountId: postData.accountId },
    clientIdentifier: clientID
  });

  if (!data?.behaviors) return [];

  const refDataMapping = mapping.data.memoChronicleBehaviors as {};

  const mappedData: MemoChronicleType[] = mappingDataFromArray(
    data.behaviors,
    refDataMapping
  );

  return mappedData;
});

export const getMemoChronicleBehaviorsBuilder = (
  builder: ActionReducerMapBuilder<MemoChronicleState>
) => {
  builder
    .addCase(getMemoChronicleBehaviors.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        refData: {
          ...draftState[storeId]?.refData,
          loading: true
        }
      };
    })
    .addCase(getMemoChronicleBehaviors.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const data = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        refData: {
          ...draftState[storeId]?.refData,
          loading: false,
          typeList: data
        }
      };
    })
    .addCase(getMemoChronicleBehaviors.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        refData: {
          ...draftState[storeId]?.refData,
          error: true,
          loading: false
        }
      };
    });
};
