import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Type
import {
  MemoChronicleState,
  MemoChronicleAction,
  GetMemoChronicleFiltersArgs,
  MemoChronicleGroup
} from '../types';

// Helper
import { mappingDataFromArray } from 'app/helpers';

// Service
import chronicleMemoService from '../chronicleMemoService';

// Const
import { EMPTY_STRING } from 'app/constants';

export const getMemoChronicleFilters = createAsyncThunk<
  MemoChronicleGroup[],
  GetMemoChronicleFiltersArgs,
  ThunkAPIConfig
>(MemoChronicleAction.GET_CHRONICLE_FILTERS, async (args, thunkAPI) => {
  const { mapping, accountDetail, tab } = thunkAPI.getState();
  const refDataMapping = mapping.data.memoChronicleFilters || {};
  const { tabStoreIdSelected: storeId } = tab;
  const { clientID = EMPTY_STRING } = accountDetail[storeId]?.data || {};
  const { accountId } = args.postData;

  const { data } = await chronicleMemoService.getMemoChronicleFilters({
    common: { accountId },
    clientIdentifier: clientID
  });

  if (!data?.filters) return [];

  const mappedData: MemoChronicleGroup[] = mappingDataFromArray(
    data.filters,
    refDataMapping
  );

  return mappedData;
});

export const getChronicleFiltersBuilder = (
  builder: ActionReducerMapBuilder<MemoChronicleState>
) => {
  builder
    .addCase(getMemoChronicleFilters.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        refData: {
          ...draftState[storeId]?.refData,
          loading: true
        }
      };
    })
    .addCase(getMemoChronicleFilters.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const data = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        refData: {
          ...draftState[storeId]?.refData,
          loading: false,
          groupList: data
        }
      };
    })
    .addCase(getMemoChronicleFilters.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        refData: {
          ...draftState[storeId]?.refData,
          error: true,
          loading: false
        }
      };
    });
};
