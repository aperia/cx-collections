import './reducers';
import { getMemoChronicleFilters } from './getMemoFilters';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { responseDefault, storeId } from 'app/test-utils';

import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';

import chronicleMemoService from '../chronicleMemoService';

const postDataHardShip = {
  accountId: storeId,
  callResultType: CALL_RESULT_CODE.HARDSHIP
};

const initialState: Partial<RootState> = {
  mapping: { loading: false, data: { memoChronicleBehaviors: {} } }
};

describe('getMemoChronicleFilters', () => {
  describe('success', () => {
    it('with data', async () => {
      jest
        .spyOn(chronicleMemoService, 'getMemoChronicleFilters')
        .mockResolvedValue({ ...responseDefault, data: { filters: {} } });

      const store = createStore(rootReducer, initialState);
      const response = await getMemoChronicleFilters({
        storeId,
        postData: postDataHardShip
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual([]);
    });

    it('with empty data', async () => {
      jest
        .spyOn(chronicleMemoService, 'getMemoChronicleFilters')
        .mockResolvedValue({ ...responseDefault, data: {} });

      const store = createStore(rootReducer, initialState);
      const response = await getMemoChronicleFilters({
        storeId,
        postData: postDataHardShip
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual([]);
    });
  });

  it('reject', async () => {
    jest
      .spyOn(chronicleMemoService, 'getMemoChronicleFilters')
      .mockRejectedValue({});

    const store = createStore(rootReducer, initialState);
    const response = await getMemoChronicleFilters({
      storeId,
      postData: postDataHardShip
    })(store.dispatch, store.getState, {});

    expect(response.payload).toBeUndefined();
  });
});
