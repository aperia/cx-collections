import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// type
import {
  MemoChronicleState,
  MemoChronicleConfigPayload,
  ExpandPayload,
  EditMemoChroniclePayload
} from '../types';

// constants
import { SEARCH_DEFAULT, SORT_DEFAULT } from 'pages/Memos/Chronicle/constants';

// async-thunk
import { getMemoChronicleBuilder, getMemoChronicle } from './getList';
import { addMemoChronicleBuilder, addMemoChronicle } from './add';
import { editMemoChronicleBuilder, editMemoChronicle } from './edit';
import { deleteChronicleMemoBuilder, deleteChronicleMemo } from './delete';
import {
  getChronicleFiltersBuilder,
  getMemoChronicleFilters
} from './getMemoFilters';
import {
  getMemoChronicleBehaviorsBuilder,
  getMemoChronicleBehaviors
} from './getMemoBehaviors';

const initialState: MemoChronicleState = {};

const { actions, reducer } = createSlice({
  name: 'memoChronicle',
  initialState,
  reducers: {
    setExpand: (draftState, action: PayloadAction<ExpandPayload>) => {
      const { storeId, isExpand } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isExpand
      };
    },
    emptyMemoList: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        memos: []
      };
    },
    openEditChronicleMemo: (
      draftState,
      action: PayloadAction<EditMemoChroniclePayload>
    ) => {
      const { storeId, memoItem } = action.payload;
      draftState[storeId].editMemoChronicle = {
        ...draftState[storeId].editMemoChronicle,
        open: true,
        memoItem
      };
    },
    closeEditChronicleMemo: (
      draftState,
      action: PayloadAction<EditMemoChroniclePayload>
    ) => {
      const { storeId } = action.payload;
      draftState[storeId].editMemoChronicle = {
        ...draftState[storeId].editMemoChronicle,
        open: false,
        memoItem: undefined
      };
    },
    removeChronicleMemo: (
      draftState,
      action: PayloadAction<StoreIdPayload>
    ) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    setConfigsFilter: (
      draftState,
      action: PayloadAction<MemoChronicleConfigPayload>
    ) => {
      const { params, storeId } = action.payload;
      const { sort, search } = params;

      const currentSearch =
        draftState[storeId]?.configs?.search || SEARCH_DEFAULT;
      const currentSort = draftState[storeId]?.configs?.sort || SORT_DEFAULT;

      draftState[storeId] = {
        ...draftState[storeId],
        configs: {
          ...draftState[storeId]?.configs,
          search: search || currentSearch,
          sort: sort || currentSort
        }
      };
    }
  },
  extraReducers: builder => {
    getMemoChronicleBuilder(builder);
    addMemoChronicleBuilder(builder);
    editMemoChronicleBuilder(builder);
    deleteChronicleMemoBuilder(builder);
    getChronicleFiltersBuilder(builder);
    getMemoChronicleBehaviorsBuilder(builder);
  }
});

const actionMemoChronicle = {
  ...actions,
  getMemoChronicle,
  addMemoChronicle,
  editMemoChronicle,
  deleteChronicleMemo,
  getMemoChronicleFilters,
  getMemoChronicleBehaviors
};

export { actionMemoChronicle, reducer };
