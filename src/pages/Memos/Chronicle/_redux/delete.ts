import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// helpers
import { mappingDataFromObj } from 'app/helpers';

// types
import {
  MemoChronicleAction,
  MemoChronicleState,
  DeleteChronicleMemoRequestArgs
} from '../types';

// actions
import { actionMemoChronicle } from './reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// constants
import { GetListType } from 'pages/Memos/Chronicle/constants';
import chronicleMemoService from '../chronicleMemoService';

// I18N
import { I18N_MEMOS } from '../../constants';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export const deleteChronicleMemo = createAsyncThunk<
  unknown,
  DeleteChronicleMemoRequestArgs,
  ThunkAPIConfig
>(MemoChronicleAction.DELETE_MEMO_CHRONICLE, async (args, thunkAPI) => {
  const state = thunkAPI.getState();
  const { accountId, ...otherFields } = args.postData;
  const memoChronicleMapping = state.mapping.data.memoChronicle || {};
  const postDataMapped = mappingDataFromObj(
    otherFields,
    memoChronicleMapping,
    true
  );
  try {
    const res = await chronicleMemoService.deleteChronicleMemo({
      ...postDataMapped,
      accountId
    });
    return res;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerDeleteChronicleMemo = createAsyncThunk<
  void,
  DeleteChronicleMemoRequestArgs,
  ThunkAPIConfig
>(
  `${MemoChronicleAction.DELETE_MEMO_CHRONICLE}/triggerDelete`,
  async (args, { dispatch }) => {
    const { storeId, postData } = args;
    const response = await dispatch(
      deleteChronicleMemo({
        storeId,
        postData
      })
    );

    // delete memo success
    if (isFulfilled(response)) {
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_MEMOS.MEMOS_DELETE_SUCCESS
          })
        );
        dispatch(
          actionMemoChronicle.getMemoChronicle({
            storeId,
            postData: {
              type: GetListType.REFRESH,
              accountId: postData.accountId
            }
          })
        );
        dispatch(
          apiErrorNotificationAction.clearApiErrorData({
            storeId,
            forSection: 'inMemoFlyOut'
          })
        );
      });
    }

    if (isRejected(response)) {
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'error',
            message: I18N_MEMOS.MEMOS_DELETE_FAILURE
          })
        );
        dispatch(
          apiErrorNotificationAction.updateApiError({
            storeId,
            forSection: 'inMemoFlyOut',
            apiResponse: response.payload?.response
          })
        );
      });
    }
  }
);

export const deleteChronicleMemoBuilder = (
  builder: ActionReducerMapBuilder<MemoChronicleState>
) => {
  builder
    .addCase(deleteChronicleMemo.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        deleteChronicleMemo: {
          ...draftState[storeId]?.deleteChronicleMemo,
          loading: true
        }
      };
    })
    .addCase(deleteChronicleMemo.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        deleteChronicleMemo: {
          ...draftState[storeId]?.deleteChronicleMemo,
          loading: false
        }
      };
    })
    .addCase(deleteChronicleMemo.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        deleteChronicleMemo: {
          ...draftState[storeId]?.deleteChronicleMemo,
          error: true,
          loading: false
        }
      };
    });
};
