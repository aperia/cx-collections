import { applyMiddleware, createStore } from 'redux';
import reduxThunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import { addMemoChronicle, triggerAddChronicleMemo } from './add';
import { AddChronicleMemoRequestData } from '../types';
import { actionMemoChronicle } from './reducers';

import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault,
  storeId
} from 'app/test-utils';

import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

import chronicleMemoService from '../chronicleMemoService';
import { I18N_MEMOS } from 'pages/Memos/constants';
import { GetListType } from '../constants';
import { isRejected } from '@reduxjs/toolkit';
import { clientConfigMemosServices } from 'pages/ClientConfiguration/Memos/clientConfigMemosService';
import { clientConfigurationServices } from 'pages/ClientConfiguration/clientConfigurationServices';

const initialState: Partial<RootState> = {
  memoChronicle: {
    [storeId]: {
      memos: []
    }
  }
};

const postData: AddChronicleMemoRequestData = {
  accountId: storeId,
  memoType: { value: 'memo type', description: 'memo type' },
  memoText: 'Memo text',
  overrideRetentionDate: new Date()
};

const toastSpy = mockActionCreator(actionsToast);
const memoSpy = mockActionCreator(actionMemoChronicle);

beforeEach(() => {
  jest.spyOn(clientConfigMemosServices, 'getMemoConfig').mockResolvedValue({
    ...responseDefault,
    data: {
      clientConfig: { components: [{ component: 'memosConfiguration' }] },
      codeToText: { components: [{ component: 'component_memos' }] }
    }
  });

  jest
    .spyOn(chronicleMemoService, 'addChronicleMemo')
    .mockResolvedValue({ ...responseDefault, data: {} });
});

describe('Memos > Chronicle > addMemoChronicle', () => {
  describe('fulfilled', () => {
    it('actionCodes does not exist', async () => {
      jest.spyOn(clientConfigMemosServices, 'getMemoConfig').mockResolvedValue({
        ...responseDefault,
        data: { clientConfig: {} }
      });
      jest
        .spyOn(clientConfigurationServices, 'getClientInfoById')
        .mockResolvedValue({
          ...responseDefault,
          data: {
            responseStatus: 'success',
            clientInfo: {
              actionCodes: undefined
            }
          }
        });

      const store = createStore(
        rootReducer,
        initialState,
        applyMiddleware(reduxThunk)
      );

      const response = await addMemoChronicle({
        storeId,
        data: { cspa: {}, chronicleKey: 'key', actionCode: '123' },
        postData
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({ ...responseDefault, data: {} });
    });

    it('actionCodes is exist', async () => {
      jest.spyOn(clientConfigMemosServices, 'getMemoConfig').mockResolvedValue({
        ...responseDefault,
        data: { clientConfig: {} }
      });
      jest
        .spyOn(clientConfigurationServices, 'getClientInfoById')
        .mockResolvedValue({
          ...responseDefault,
          data: {
            responseStatus: 'success',
            clientInfo: {
              actionCodes: [
                {
                  actionCode: '123',
                  actionTypes: ['111']
                }
              ]
            }
          }
        });

      const store = createStore(
        rootReducer,
        initialState,
        applyMiddleware(reduxThunk)
      );

      const response = await addMemoChronicle({
        storeId,
        data: { cspa: {}, chronicleKey: 'key', actionCode: '123' },
        postData
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({ ...responseDefault, data: {} });
    });

    it('getClientInfoData response reject', async () => {
      jest.spyOn(clientConfigMemosServices, 'getMemoConfig').mockResolvedValue({
        ...responseDefault,
        data: { clientConfig: {} }
      });
      jest
        .spyOn(clientConfigurationServices, 'getClientInfoById')
        .mockRejectedValue({
          ...responseDefault,
          data: {
            responseStatus: 'success'
          }
        });

      const store = createStore(
        rootReducer,
        initialState,
        applyMiddleware(reduxThunk)
      );

      const response = await addMemoChronicle({
        storeId,
        data: { cspa: {}, chronicleKey: 'key', actionCode: '123' },
        postData
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({ ...responseDefault, data: {} });
    });

    it('success with empty cspa data', async () => {
      jest.spyOn(clientConfigMemosServices, 'getMemoConfig').mockResolvedValue({
        ...responseDefault,
        data: { clientConfig: {} }
      });

      const store = createStore(rootReducer, initialState);

      const response = await addMemoChronicle({
        storeId,
        data: { cspa: {}, chronicleKey: 'key' },
        postData
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({ ...responseDefault, data: {} });
    });

    it('success with cspa data', async () => {
      const store = createStore(rootReducer, initialState);

      const response = await addMemoChronicle({
        storeId,
        data: { cspa: { clientNumber: '09798' }, chronicleKey: 'key' },
        postData
      })(store.dispatch, store.getState, {});

      expect(response.payload).toEqual({ ...responseDefault, data: {} });
    });
  });

  describe('reject', () => {
    it('reject', async () => {
      jest
        .spyOn(chronicleMemoService, 'addChronicleMemo')
        .mockRejectedValue({});

      const store = createStore(rootReducer, {});
      const response = await addMemoChronicle({
        storeId,
        postData: { accountId: storeId }
      })(store.dispatch, store.getState, {});

      expect(isRejected(response)).toBeTruthy;
    });
  });
});

describe('Memos > Chronicle > triggerAddChronicleMemo', () => {
  describe('success', () => {
    it('isFulfilled', async () => {
      const mockOnSuccess = jest.fn();
      const mockAddToast = toastSpy('addToast');
      const mockGetMemoChronicle = memoSpy('getMemoChronicle');

      const store = createStore(rootReducer, {});
      await triggerAddChronicleMemo({
        storeId,
        postData,
        onSuccess: mockOnSuccess
      })(
        byPassFulfilled(triggerAddChronicleMemo.fulfilled.type),
        store.getState,
        {}
      );

      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: I18N_MEMOS.MEMOS_ADD_SUCCESS
      });

      expect(mockGetMemoChronicle).toBeCalledWith({
        storeId,
        postData: {
          type: GetListType.REFRESH,
          accountId: postData.accountId
        }
      });

      expect(mockOnSuccess).toBeCalled();
    });

    it('isFulfilled', async () => {
      const mockOnFailure = jest.fn();
      const mockAction = toastSpy('addToast');

      const store = createStore(rootReducer, {});
      await triggerAddChronicleMemo({
        storeId,
        postData,
        onFailure: mockOnFailure
      })(
        byPassRejected(triggerAddChronicleMemo.rejected.type),
        store.getState,
        {}
      );

      expect(mockAction).toBeCalledWith({
        show: true,
        type: 'error',
        message: I18N_MEMOS.MEMOS_ADD_FAILURE
      });
      expect(mockOnFailure).toBeCalled();
    });
  });
});
