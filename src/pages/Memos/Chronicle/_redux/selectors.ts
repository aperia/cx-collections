import { createSelector } from '@reduxjs/toolkit';

// constants
import {
  SORT_DEFAULT,
  SEARCH_DEFAULT,
  CONFIG_DEFAULT,
  chronicleSearchFormView,
  AddMemoChronicleView,
  EditMemoChronicleView
} from 'pages/Memos/Chronicle/constants';

// types
import { AppState } from 'storeConfig';
import {
  EditChronicleMemoRequestData,
  MemoChronicleGroup,
  MemoChronicleType,
  ReferenceData
} from '../types';

// helpers
import isEqual from 'lodash.isequal';
import { MemoChronicle } from '../types';
import { SearchFormProps } from 'pages/Memos/Chronicle/Search';
import { AddFormChronicle } from 'pages/Memos/Chronicle/Add';
import { EMPTY_ARRAY, EMPTY_OBJECT } from 'app/constants';

const getChronicleMemo = (state: AppState, storeId: string) => {
  return state.memoChronicle[storeId]?.memos || [];
};

const getChronicleMemoItem = (state: AppState, storeId: string) => {
  return state.memoChronicle[storeId]?.editMemoChronicle?.memoItem;
};

const getConfigChronicleMemo = (state: AppState, storeId: string) => {
  return state.memoChronicle[storeId]?.configs || CONFIG_DEFAULT;
};

const getSearchParamsChronicleMemo = (state: AppState, storeId: string) => {
  return state.memoChronicle[storeId]?.configs?.search || {};
};

const getSearchFormChronicleMemo = (state: AppState, storeId: string) => {
  return state.form[`${chronicleSearchFormView}-${storeId}`]?.values || {};
};

const getLoading = (state: AppState, storeId: string) => {
  return state.memoChronicle[storeId]?.loading || false;
};

const getLoadMore = (state: AppState, storeId: string) => {
  return state.memoChronicle[storeId]?.isLoadMore || false;
};

const getEndLoadMore = (state: AppState, storeId: string) => {
  return state.memoChronicle[storeId]?.isEnd || false;
};

const getIsExpand = (state: AppState, storeId: string) => {
  return state.memoChronicle[storeId]?.isExpand || false;
};

const getChronicleMemoTypeList: CustomStoreIdSelector<MemoChronicleType[]> = (
  state,
  storeId
) => {
  return state.memoChronicle[storeId]?.refData?.typeList ?? EMPTY_ARRAY;
};

const getChronicleMemoGroupList: CustomStoreIdSelector<MemoChronicleGroup[]> = (
  state,
  storeId
) => {
  return state.memoChronicle[storeId]?.refData?.groupList ?? EMPTY_ARRAY;
};

const getAddFormChronicleMemo = (state: AppState, storeId: string) => {
  return (
    state.form[`${storeId}-${AddMemoChronicleView.ID}`]?.values || EMPTY_OBJECT
  );
};

const getEditFormChronicleMemo = (state: AppState, storeId: string) => {
  return (
    state.form[`${storeId}-${EditMemoChronicleView.ID}`]?.values || EMPTY_OBJECT
  );
};

export const selectChronicleMemo = createSelector(
  getChronicleMemo,
  memo => memo
);

export const selectChronicleMemoItem = createSelector(
  getChronicleMemoItem,
  memo => memo
);

export const selectConfigChronicleMemo = createSelector(
  getConfigChronicleMemo,
  config => config
);

export const selectLoading = createSelector(getLoading, loading => loading);

export const selectLoadMore = createSelector(
  getLoadMore,
  isLoadMore => isLoadMore
);
export const selectEndLoadMore = createSelector(getEndLoadMore, isEnd => isEnd);

export const selectIsExpand = createSelector(getIsExpand, isExpand => isExpand);

export const selectTotalMemo = createSelector(
  getChronicleMemo,
  memos => memos.length || 0
);

export const selectHasIndicator = createSelector(
  getConfigChronicleMemo,
  config => {
    return {
      isHasSortIndicator: !isEqual(config.sort, SORT_DEFAULT),
      isHasSearchIndicator: !isEqual(config.search, SEARCH_DEFAULT)
    };
  }
);

export const selectChronicleMemoTypeList = createSelector(
  getChronicleMemoTypeList,
  (typeList: ReferenceData[]) => typeList
);

export const selectChronicleMemoGroupList = createSelector(
  getChronicleMemoGroupList,
  (groupList: ReferenceData[]) => groupList
);

export const selectSearchParamsChronicleMemo = createSelector(
  getSearchParamsChronicleMemo,
  (searchParams: Partial<MemoChronicle>) => searchParams
);

export const selectSearchFormChronicleMemo = createSelector(
  getSearchFormChronicleMemo,
  (searchForm: Partial<SearchFormProps>) => searchForm
);

export const selectAddFormChronicleMemo = createSelector(
  getAddFormChronicleMemo,
  (addForm: Partial<AddFormChronicle>) => addForm
);

export const selectEditFormChronicleMemo = createSelector(
  getEditFormChronicleMemo,
  (addForm: Partial<EditChronicleMemoRequestData>) => addForm
);
