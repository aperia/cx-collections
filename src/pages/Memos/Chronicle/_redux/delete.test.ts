import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { actionMemoChronicle } from './reducers';
import { deleteChronicleMemo, triggerDeleteChronicleMemo } from './delete';
import { DeleteChronicleMemoRequestData } from '../types';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault,
  storeId
} from 'app/test-utils';
import { GetListType } from '../constants';
import { I18N_MEMOS } from 'pages/Memos/constants';

import chronicleMemoService from '../chronicleMemoService';

const initialState: Partial<RootState> = {
  mapping: {
    loading: false,
    data: {}
  },
  memoChronicle: {
    [storeId]: {
      memos: []
    }
  }
};

const postData: DeleteChronicleMemoRequestData = {
  accountId: storeId,
  memoIdentifier: '123'
};

const toastSpy = mockActionCreator(actionsToast);
const memoSpy = mockActionCreator(actionMemoChronicle);

describe('Memos > Chronicle > deleteChronicleMemo', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    jest.restoreAllMocks()
  })

  it('success', async () => {
    jest
      .spyOn(chronicleMemoService, 'deleteChronicleMemo')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const store = createStore(rootReducer, initialState);

    const response = await deleteChronicleMemo({
      storeId,
      postData
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({ ...responseDefault, data: {} });
  });

  it('reject', async () => {
    jest
      .spyOn(chronicleMemoService, 'deleteChronicleMemo')
      .mockRejectedValue({ ...responseDefault, data: {} });

    const store = createStore(rootReducer, initialState);

    const response = await deleteChronicleMemo({
      storeId,
      postData
    })(store.dispatch, store.getState, {});

    expect(response.type).toEqual('deleteMemoChronicle/rejected');
  });
});

describe('Memos > Chronicle > triggerAddChronicleMemo', () => {
  beforeEach(() => {
    jest.clearAllMocks()
    jest.restoreAllMocks()
  })

  describe('success', () => {
    it('isFulfilled', async () => {
      const mockAddToast = toastSpy('addToast');
      const mockGetMemoChronicle = memoSpy('getMemoChronicle');

      const store = createStore(rootReducer, {});
      await triggerDeleteChronicleMemo({
        storeId,
        postData
      })(
        byPassFulfilled(deleteChronicleMemo.fulfilled.type),
        store.getState,
        {}
      );

      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: I18N_MEMOS.MEMOS_DELETE_SUCCESS
      });

      expect(mockGetMemoChronicle).toBeCalledWith({
        storeId,
        postData: {
          type: GetListType.REFRESH,
          accountId: postData.accountId
        }
      });
    });

    it('isFulfilled', async () => {
      const mockAction = toastSpy('addToast');

      const store = createStore(rootReducer, {});
      await triggerDeleteChronicleMemo({
        storeId,
        postData
      })(byPassRejected(deleteChronicleMemo.rejected.type), store.getState, {});

      expect(mockAction).toBeCalledWith({
        show: true,
        type: 'error',
        message: I18N_MEMOS.MEMOS_DELETE_FAILURE
      });
    });
  });
});
