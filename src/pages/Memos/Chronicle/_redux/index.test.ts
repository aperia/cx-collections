import {
  reducer,
  actionMemoChronicle
} from 'pages/Memos/Chronicle/_redux/reducers';
import {
  MemoChronicleState,
  MemoChronicleSortField,
  MemoChronicleSortType,
  MemoChronicle
} from 'pages/Memos/Chronicle/types';
import { CONFIG_DEFAULT } from '../constants';

const storeId = '210000000000000';
const initReducer: MemoChronicleState = {};
const initReducerHasData: MemoChronicleState = {
  [storeId]: {
    configs: {
      search: {
        memoDate: '12-12-2020'
      },
      sort: {
        field: MemoChronicleSortField.DATE_TIME,
        type: MemoChronicleSortType.ASC
      }
    }
  }
};

describe('redux-store > getMemoChronicle > action', () => {
  it('setExpand', () => {
    const nextState = reducer(
      initReducer,
      actionMemoChronicle.setExpand({ isExpand: true, storeId })
    );
    expect(nextState[storeId].isExpand).toEqual(true);
  });

  it('emptyMemoList', () => {
    const nextState = reducer(
      initReducer,
      actionMemoChronicle.emptyMemoList({ storeId })
    );
    expect(nextState[storeId].memos).toEqual([]);
  });
  it('removeChronicleMemo', () => {
    const nextState = reducer(
      initReducer,
      actionMemoChronicle.removeChronicleMemo({ storeId })
    );
    expect(nextState[storeId]).toEqual(undefined);
  });
  it('setConfigsFilter', () => {
    const nextState = reducer(
      initReducer,
      actionMemoChronicle.setConfigsFilter({ storeId, params: CONFIG_DEFAULT })
    );
    expect(nextState[storeId]?.configs?.search).toEqual(CONFIG_DEFAULT.search);
    expect(nextState[storeId]?.configs?.sort).toEqual(CONFIG_DEFAULT.sort);
  });
  it('setConfigsFilter', () => {
    const nextState = reducer(
      initReducerHasData,
      actionMemoChronicle.setConfigsFilter({ storeId, params: {} })
    );
    expect(nextState[storeId]?.configs?.search).toEqual({
      memoDate: '12-12-2020'
    });
    expect(nextState[storeId]?.configs?.sort).toEqual({
      field: MemoChronicleSortField.DATE_TIME,
      type: MemoChronicleSortType.ASC
    });
  });
  it('openEditChronicleMemo', () => {
    const memoItem = ({
      memoIdentifier: 122,
      memoType: { value: '111', description: 'des' },
      memoText: 'Memo Test',
      memoOperatorIdentifier: new Date().toString(),
      memoDate: new Date().toISOString(),
      memoTime: new Date().toISOString()
    } as unknown) as MemoChronicle;
    const nextState = reducer(
      initReducerHasData,
      actionMemoChronicle.openEditChronicleMemo({
        storeId,
        memoItem
      })
    );
    expect(nextState[storeId]?.editMemoChronicle?.memoItem).toEqual(memoItem);
    expect(nextState[storeId]?.editMemoChronicle?.open).toEqual(true);
  });
  it('closeEditChronicleMemo', () => {
    const nextState = reducer(
      initReducerHasData,
      actionMemoChronicle.closeEditChronicleMemo({
        storeId
      })
    );
    expect(nextState[storeId]?.editMemoChronicle?.open).toEqual(false);
  });
});
