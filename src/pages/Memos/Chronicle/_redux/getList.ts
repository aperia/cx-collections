import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import {
  GetListType,
  FIRST_SEQUENCE,
  ITEM_PER_PAGE,
  CONFIG_DEFAULT,
  SEARCH_DEFAULT,
  SORT_DEFAULT
} from 'pages/Memos/Chronicle/constants';
import {
  MemoChronicleAction,
  GetMemoChroniclePayload,
  MemoChronicleState,
  MemoChronicleConfig,
  GetMemoChronicleActionArgs,
  GetMemoChroniclePostData
} from '../types';

import chronicleMemoService from '../chronicleMemoService';
import { trimData } from 'app/helpers';
import { NO_RESULT_CODE } from 'pages/AccountSearch/AccountSearchList/constants';

export const getMemoChronicle = createAsyncThunk<
  GetMemoChroniclePayload,
  GetMemoChronicleActionArgs,
  ThunkAPIConfig
>(MemoChronicleAction.GET_CHRONICLE_MEMO, async (args, thunkAPI) => {
  const { storeId } = args;
  const { accountId } = args.postData;
  const state: RootState = thunkAPI.getState();
  const currentConfig = state.memoChronicle[storeId]?.configs || CONFIG_DEFAULT;
  const newConfigs = getConfigRequest(currentConfig, args.postData);

  try {
    const { data } = await chronicleMemoService.getChronicleMemosList({
      startSequence: `${newConfigs.start}`,
      endSequence: `${newConfigs.end}`,
      accountId,
      memoDate:
        newConfigs?.search?.memoDate !== null
          ? newConfigs?.search?.memoDate
          : undefined,
      startDate:
        newConfigs?.search?.memoDateRange !== null
          ? newConfigs?.search?.memoDateRange?.start
          : undefined,
      endDate:
        newConfigs?.search?.memoDateRange !== null
          ? newConfigs?.search?.memoDateRange?.end
          : undefined
    });

    const { memos } = data;
    const memoList = memos && memos.length ? trimData(memos) : [];

    return {
      configs: newConfigs,
      memos: memoList
    };
  } catch (err) {
    const errorStatus = err?.status || err?.response?.status;

    if (errorStatus === NO_RESULT_CODE) {
      return { configs: newConfigs, memos: [] };
    }

    return thunkAPI.rejectWithValue(err);
  }
});

export const getMemoChronicleBuilder = (
  builder: ActionReducerMapBuilder<MemoChronicleState>
) => {
  builder.addCase(getMemoChronicle.pending, (draftState, action) => {
    const {
      storeId,
      postData: { type }
    } = action.meta.arg;
    draftState[storeId] = {
      ...draftState[storeId],
      loading: type !== GetListType.LOAD_MORE,
      isLoadMore: type === GetListType.LOAD_MORE,
      isEnd: false
    };
  });

  builder.addCase(getMemoChronicle.fulfilled, (draftState, action) => {
    const {
      storeId,
      postData: { type }
    } = action.meta.arg;
    const { memos, configs } = action.payload;
    const oldMemo = draftState[storeId]?.memos || [];

    let isEnd = draftState[storeId]?.isEnd || false;

    let newMemo = [...memos];

    if (newMemo.length < ITEM_PER_PAGE) {
      isEnd = true;
    }

    if (type === GetListType.LOAD_MORE) {
      newMemo = oldMemo.concat(newMemo);
    }

    draftState[storeId] = {
      ...draftState[storeId],
      loading: false,
      memos: newMemo,
      isLoadMore: false,
      isEnd,
      configs
    };
  });

  builder.addCase(getMemoChronicle.rejected, (draftState, action) => {
    const {
      storeId,
      postData: { type }
    } = action.meta.arg;

    draftState[storeId] = {
      ...draftState[storeId],
      loading: false,
      isLoadMore: false,
      ...(type !== GetListType.LOAD_MORE && { memos: [] })
    };
  });
};
const getConfigRequest = (
  config: MemoChronicleConfig,
  args: GetMemoChroniclePostData
): MemoChronicleConfig => {
  switch (args.type) {
    case GetListType.FIRST:
      return {
        end: args.end || ITEM_PER_PAGE,
        start: FIRST_SEQUENCE,
        search: SEARCH_DEFAULT,
        sort: SORT_DEFAULT
      };
    case GetListType.LOAD_MORE:
      const end = config?.end || 0;
      return {
        ...config,
        end: end + ITEM_PER_PAGE,
        start: end + 1,
        search: {
          ...config?.search,
          memoDate: config?.search?.memoDate
        }
      };
    case GetListType.REFRESH:
      return {
        ...config,
        end: args?.end || config?.end || ITEM_PER_PAGE,
        start: FIRST_SEQUENCE,
        search: {
          ...config?.search,
          memoDate:
            args?.memoDate === undefined
              ? config?.search?.memoDate
              : args?.memoDate
        }
      };
    case GetListType.RESET_SEARCH:
      return {
        end: args?.end || config?.end || ITEM_PER_PAGE,
        start: FIRST_SEQUENCE,
        search: SEARCH_DEFAULT,
        sort: config.sort
      };
    default:
      return {
        ...config,
        end: config.end || ITEM_PER_PAGE,
        start: config.start || FIRST_SEQUENCE,
        search: {
          ...config?.search,
          memoDate: config?.search?.memoDate,
          startDate: config?.search?.memoDateRange?.start,
          endDate: config?.search?.memoDateRange?.end
        }
      };
  }
};
