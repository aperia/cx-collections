import React, { useEffect, useMemo, useRef, useState } from 'react';

// components/types
import { ExtraFieldProps, View } from 'app/_libraries/_dof/core';
import { Button } from 'app/_libraries/_dls/components';
import { MemoChronicle } from 'pages/Memos/Chronicle/types';

// utils
import isEmpty from 'lodash.isempty';
import isEqual from 'lodash.isequal';
import isFunction from 'lodash.isfunction';
import { Field } from 'redux-form';
import { useDispatch } from 'react-redux';

// reducers
import { actionMemoChronicle } from 'pages/Memos/Chronicle/_redux/reducers';

// constants
import {
  chronicleSearchFormView,
  SearchMemoChronicleDataField,
  SEARCH_DEFAULT,
  GetListType,
  ITEM_PER_PAGE
} from '../constants';

// hooks
import {
  useAccountDetail,
  useCustomStoreId,
  useCustomStoreIdSelector
} from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// i18n
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

import { I18N_MEMOS } from '../../constants';

// selectors
import {
  selectChronicleMemoTypeList,
  selectSearchParamsChronicleMemo,
  selectSearchFormChronicleMemo,
  selectChronicleMemoGroupList
} from 'pages/Memos/Chronicle/_redux/selectors';
import { handleWithRefOnFind } from 'app/helpers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export interface SearchProps {
  onClose?: () => void;
}

export interface SearchFormProps {
  memoDate: string;
  memoDateRange: { start: MagicKeyValue; end: MagicKeyValue };
  memoText: string;
  memoType: string;
  memoGroup: string;
  dateType: {
    dateRange: boolean;
    specificDate: boolean;
  };
}

const Search: React.FC<SearchProps> = ({ onClose }) => {
  const dispatch = useDispatch();

  const { customStoreId: storeId } = useCustomStoreId();
  const { accEValue } = useAccountDetail();

  const { t } = useTranslation();

  const viewRef = useRef<any>(null);

  const [value, setValue] = useState<Partial<MemoChronicle>>(SEARCH_DEFAULT);
  const [disabledSubmit, setDisabledSubmit] = useState(true);

  const memoTypeData = useCustomStoreIdSelector<RefDataValue[]>(
    selectChronicleMemoTypeList
  );

  const memoGroupData = useCustomStoreIdSelector<RefDataValue[]>(
    selectChronicleMemoGroupList
  );

  const searchParams = useCustomStoreIdSelector<Partial<MemoChronicle>>(
    selectSearchParamsChronicleMemo
  );

  const searchForm = useCustomStoreIdSelector<Partial<SearchFormProps>>(
    selectSearchFormChronicleMemo
  );

  const handleSearch = () => {
    const formValue: MagicKeyValue = { ...searchForm };
    if (formValue?.dateType?.dateRange) {
      if (formValue?.memoDateRange?.start && !formValue?.memoDateRange?.end) {
        const field = handleWithRefOnFind(
          viewRef,
          SearchMemoChronicleDataField.DATE_RANGE_PICKER
        );
        field?.props?.setOthers((pre: MagicKeyValue) => ({
          ...pre,
          options: {
            ...pre?.options,
            isForceInvalid: true,
            messageForceValid: 'txt_range_date_must_have_from_to_date'
          }
        }));
        return;
      }

      if (formValue?.memoDateRange?.end && !formValue?.memoDateRange?.start) {
        const field = handleWithRefOnFind(
          viewRef,
          SearchMemoChronicleDataField.DATE_RANGE_PICKER
        );
        field?.props?.setOthers((pre: MagicKeyValue) => ({
          ...pre,
          options: {
            ...pre?.options,
            isForceInvalid: true,
            messageForceValid: 'txt_range_date_must_have_from_to_date'
          }
        }));
        return;
      }

      if (formValue?.memoDateRange?.end && formValue?.memoDateRange?.start) {
        const field = handleWithRefOnFind(
          viewRef,
          SearchMemoChronicleDataField.DATE_RANGE_PICKER
        );
        field?.props?.setOthers((pre: MagicKeyValue) => ({
          ...pre,
          options: {
            ...pre?.options,
            isForceInvalid: false,
            messageForceValid: ''
          }
        }));
      }
    }
    if (!formValue?.memoDate && !formValue?.memoDateRange) {
      formValue.dateType = {
        specificDate: true,
        dateRange: false
      };
    }
    formValue.searchBy = {
      group: formValue.searchBy.first,
      type: formValue.searchBy.second
    };
    if (!formValue?.memoType && !formValue?.memoGroup) {
      formValue.searchBy = {
        group: true,
        type: false
      };
    }
    // apply filter
    dispatch(
      actionMemoChronicle.setConfigsFilter({
        storeId,
        params: {
          search: formValue
        }
      })
    );

    // refresh data if memoDate is cleared
    const isReset = !!(
      !formValue?.memoDateRange &&
      !formValue?.memoDate &&
      (searchParams?.memoDate || searchParams?.memoDateRange)
    );

    // get new list memo with memoDate
    const isRefreshList =
      isReset ||
      formValue?.memoDate ||
      formValue?.memoDateRange?.start ||
      formValue?.memoDateRange?.end;

    if (isRefreshList) {
      dispatch(
        actionMemoChronicle.getMemoChronicle({
          storeId,
          postData: {
            accountId: accEValue,
            type: GetListType.REFRESH,
            end: ITEM_PER_PAGE,
            memoDate: !isReset ? (formValue.memoDate as string) : null,
            startDate: !isReset
              ? (formValue?.memoDateRange?.start as string)
              : null,
            endDate: !isReset ? (formValue?.memoDateRange?.end as string) : null
          }
        })
      );
    }

    isFunction(onClose) && onClose();

    dispatch(
      apiErrorNotificationAction.clearApiErrorData({
        storeId,
        forSection: 'inMemoFlyOut'
      })
    );
  };

  const resetToDefault = () => {
    setValue({ ...SEARCH_DEFAULT });
    if (!isEqual(searchParams, SEARCH_DEFAULT)) {
      setDisabledSubmit(false);
    }
  };

  useEffect(() => {
    if (isEmpty(searchParams)) return;
    setValue(searchParams);
  }, [searchParams]);

  useEffect(() => {
    if (!isEmpty(memoTypeData)) return;
    dispatch(
      actionMemoChronicle.getMemoChronicleBehaviors({
        storeId,
        postData: {
          accountId: accEValue
        }
      })
    );
    dispatch(
      actionMemoChronicle.getMemoChronicleFilters({
        storeId,
        postData: {
          accountId: accEValue
        }
      })
    );
  }, [accEValue, dispatch, memoTypeData, storeId]);

  useEffect(() => {
    if (isEmpty(memoTypeData) || isEmpty(memoGroupData)) return;

    // bind data into type field
    setImmediate(() => {
      const typeField: Field<ExtraFieldProps> = viewRef.current?.props?.onFind(
        SearchMemoChronicleDataField.MEMO_SEARCH_TYPE
      );
      const groupField: Field<ExtraFieldProps> = viewRef.current?.props?.onFind(
        SearchMemoChronicleDataField.MEMO_SEARCH_GROUP
      );
      typeField?.props.props.setData(memoTypeData);
      groupField?.props.props.setData(memoGroupData);
    });
  }, [memoTypeData, memoGroupData, value, storeId]);

  useEffect(() => {
    // Handle to show date type and search by
    setImmediate(() => {
      const dateTypeField: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(
          SearchMemoChronicleDataField.DATE_TYPE_RADIO
        );
      const memoDateField: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(
          SearchMemoChronicleDataField.DATE_PICKER
        );
      const memoDateRangeField: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(
          SearchMemoChronicleDataField.DATE_RANGE_PICKER
        );
      const searchByField: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(
          SearchMemoChronicleDataField.SEARCH_BY_RADIO
        );
      const memoTypeField: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(
          SearchMemoChronicleDataField.MEMO_SEARCH_TYPE
        );
      const memoGroupField: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(
          SearchMemoChronicleDataField.MEMO_SEARCH_GROUP
        );

      dateTypeField?.props.props.setValue(value.dateType);
      memoDateField?.props.props.setVisible(
        Boolean(value?.dateType?.specificDate)
      );
      memoDateRangeField?.props.props.setVisible(
        Boolean(value?.dateType?.dateRange)
      );
      searchByField?.props.props.setValue({
        first: value?.searchBy?.group,
        second: value?.searchBy?.type
      });
      memoGroupField?.props.props.setVisible(Boolean(value?.searchBy?.group));
      memoTypeField?.props.props.setVisible(Boolean(value?.searchBy?.type));
    });
  }, [value]);

  useEffect(() => {
    if (!isEmpty(searchForm)) {
      const { memoText, memoType, memoDate, memoDateRange, memoGroup } =
        searchForm;

      if (!isEqual(searchParams, SEARCH_DEFAULT)) {
        return setDisabledSubmit(false);
      }

      const isDisabled =
        !memoText && !memoType && !memoDate && !memoDateRange && !memoGroup;

      setDisabledSubmit(isDisabled);
    }
  }, [searchForm, searchParams]);

  useEffect(() => {
    if (searchForm.dateType?.dateRange) {
      if (searchForm?.memoDateRange?.end && searchForm?.memoDateRange?.start) {
        const field = handleWithRefOnFind(
          viewRef,
          SearchMemoChronicleDataField.DATE_RANGE_PICKER
        );
        field?.props?.setOthers((pre: MagicKeyValue) => ({
          ...pre,
          options: {
            ...pre?.options,
            isForceInvalid: false,
            messageForceValid: ''
          }
        }));
      }
    }
  }, [searchForm]);

  const view = useMemo(() => {
    return (
      <View
        id={`${chronicleSearchFormView}-${storeId}`}
        formKey={`${chronicleSearchFormView}-${storeId}`}
        descriptor={`${chronicleSearchFormView}`}
        ref={viewRef}
        value={value}
      />
    );
  }, [storeId, value]);

  return (
    <div className="inside-infobar">
      {view}
      <div className="d-flex justify-content-end mt-24">
        <Button
          size="sm"
          className="ml-8"
          variant="secondary"
          onClick={resetToDefault}
          dataTestId="chronicle-memo-header_search_reset-to-default-btn"
        >
          {t(I18N_MEMOS.RESET_TO_DEFAULT)}
        </Button>
        <Button
          size="sm"
          onClick={handleSearch}
          disabled={disabledSubmit}
          dataTestId="chronicle-memo-header_search_apply-btn"
        >
          {t(I18N_COMMON_TEXT.APPLY)}
        </Button>
      </div>
    </div>
  );
};

export default Search;
