import React from 'react';
import { queries } from '@testing-library/dom';
import { CustomStoreIdProvider } from 'app/hooks';
import { renderMockStore, storeId } from 'app/test-utils';
import Search from './index';
import { SEARCH_DEFAULT } from '../constants';

jest.mock('app/helpers', () => ({
  handleWithRefOnFind: () => ({
    props: {
      setOthers: (fn: Function) => fn()
    }
  }),
  getSessionStorage: () => jest.fn(),
  getSessionStorageWithUF8: () => jest.fn(),
  createDeepEqualSelector: () => jest.fn()
}));

jest.mock('pages/CollectionForm/helpers', () => ({
  getDelayProgressRawData: () => jest.fn(),
  dispatchDestroyDelayProgress: () => jest.fn()
}));

const defaultInitialState = {
  memoChronicle: {
    [storeId]: {
      configs: {
        start: 0,
        end: 50,
        search: SEARCH_DEFAULT
      },
      refData: {
        loading: false,
        typeList: [{ value: 'Memo 1', description: 'Memo 1' }],
        groupList: [{ value: 'Group 1', description: 'Group 1' }]
      }
    }
  },
  form: {
    [`chronicleSearchFormView-${storeId}`]: {
      initial: {
        dateType: { dateRange: false, specificDate: true },
        memoText: '',
        searchBy: { group: true, type: false }
      },
      registeredFields: {
        dateType: { name: 'dateType', type: 'Field', count: 1 },
        memoChronicleSearchTitle: {
          name: 'memoChronicleSearchTitle',
          type: 'Field',
          count: 1
        },
        memoDate: { name: 'memoDate', type: 'Field', count: 1 },
        memoDateRange: { name: 'memoDateRange', type: 'Field', count: 1 },
        memoGroup: { name: 'memoGroup', type: 'Field', count: 1 },
        memoText: { name: 'memoText', type: 'Field', count: 1 },
        memoType: { name: 'memoType', type: 'Field', count: 1 },
        searchBy: { name: 'searchBy', type: 'Field', count: 1 }
      },
      values: {
        dateType: { dateRange: false, specificDate: true },
        memoText: '',
        searchBy: { first: true, second: false }
      }
    }
  }
};

const emptyRefDataInitialState = {
  memoChronicle: {
    [storeId]: {
      configs: {
        start: 0,
        end: 50
      },
      refData: {}
    }
  }
};

const initialState = {
  memoChronicle: {
    [storeId]: {
      configs: {
        start: 0,
        end: 50,
        search: {
          memoText: 'a',
          dateType: { dateRange: false, specificDate: true },
          memoDate: new Date().toISOString(),
          searchBy: { group: true, type: false }
        }
      },
      refData: {
        loading: false,
        typeList: [{ value: 'Memo 1', description: 'Memo 1' }],
        groupList: [{ value: 'Group 1', description: 'Group 1' }]
      }
    }
  },
  form: {
    [`chronicleSearchFormView-${storeId}`]: {
      initial: {
        dateType: { dateRange: false, specificDate: true },
        memoText: '',
        searchBy: { group: true, type: false }
      },
      registeredFields: {
        dateType: { name: 'dateType', type: 'Field', count: 1 },
        memoChronicleSearchTitle: {
          name: 'memoChronicleSearchTitle',
          type: 'Field',
          count: 1
        },
        memoDate: { name: 'memoDate', type: 'Field', count: 1 },
        memoDateRange: { name: 'memoDateRange', type: 'Field', count: 1 },
        memoGroup: { name: 'memoGroup', type: 'Field', count: 1 },
        memoText: { name: 'memoText', type: 'Field', count: 1 },
        memoType: { name: 'memoType', type: 'Field', count: 1 },
        searchBy: { name: 'searchBy', type: 'Field', count: 1 }
      },
      values: {
        dateType: { dateRange: false, specificDate: true },
        memoText: 'a',
        searchBy: { first: true, second: false }
      }
    }
  }
};

jest.mock('react-redux', () => {
  return {
    ...(jest.requireActual('react-redux') as object),
    useDispatch: () => (x: Function) => jest.fn().mockImplementation(() => x())
  };
});

describe('Test Search Chronicle memo', () => {
  it('Case render component', async () => {
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <Search />
      </CustomStoreIdProvider>,
      { initialState: defaultInitialState } as MagicKeyValue,
      true
    );

    const resetButton = queries.getByText(
      wrapper.container,
      'txt_reset_to_default'
    );
    resetButton.click();

    const applyButton = await queries.findByText(
      wrapper.container,
      'txt_apply'
    );

    expect(applyButton).toBeDisabled();
  });

  it('Case empty ref data', async () => {
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <Search />
      </CustomStoreIdProvider>,
      { initialState: emptyRefDataInitialState },
      true
    );

    const resetButton = queries.getByText(
      wrapper.container,
      'txt_reset_to_default'
    );

    const applyButton = queries.getByText(wrapper.container, 'txt_apply');
    // Assertion
    expect(resetButton).toBeEnabled();
    expect(applyButton).toBeDisabled();
  });

  it('Case click apply button', async () => {
    // Arrange
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <Search onClose={jest.fn()} />
      </CustomStoreIdProvider>,
      { initialState } as MagicKeyValue,
      true
    );

    const resetButton = queries.getByText(
      wrapper.container,
      'txt_reset_to_default'
    );
    resetButton.click();

    const applyButton = queries.getByText(wrapper.container, 'txt_apply');
    applyButton.click();
    // Assertion
    expect(resetButton).toBeEnabled();
    expect(applyButton).toBeEnabled();
  });

  it('Case search with date rage', async () => {
    // Arrange
    const state = {
      memoChronicle: {
        [storeId]: {
          configs: {
            start: 0,
            end: 50,
            search: {
              memoText: 'a',
              dateType: { dateRange: false, specificDate: true },
              memoDateRange: {
                start: new Date().toISOString(),
                end: new Date().toISOString()
              },
              searchBy: { group: true, type: false }
            }
          },
          refData: {
            loading: false,
            typeList: [{ value: 'Memo 1', description: 'Memo 1' }],
            groupList: [{ value: 'Group 1', description: 'Group 1' }]
          }
        }
      },
      form: {
        [`chronicleSearchFormView-${storeId}`]: {
          initial: {
            dateType: { dateRange: false, specificDate: true },
            memoText: '',
            searchBy: { group: true, type: false }
          },
          registeredFields: {
            dateType: { name: 'dateType', type: 'Field', count: 1 },
            memoChronicleSearchTitle: {
              name: 'memoChronicleSearchTitle',
              type: 'Field',
              count: 1
            },
            memoDate: { name: 'memoDate', type: 'Field', count: 1 },
            memoDateRange: { name: 'memoDateRange', type: 'Field', count: 1 },
            memoGroup: { name: 'memoGroup', type: 'Field', count: 1 },
            memoText: { name: 'memoText', type: 'Field', count: 1 },
            memoType: { name: 'memoType', type: 'Field', count: 1 },
            searchBy: { name: 'searchBy', type: 'Field', count: 1 }
          },
          values: {
            dateType: { dateRange: false, specificDate: true },
            memoText: 'a',
            memoDateRange: {
              start: new Date().toISOString(),
              end: new Date().toISOString()
            },
            searchBy: { first: true, second: false }
          }
        }
      }
    };
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <Search onClose={jest.fn()} />
      </CustomStoreIdProvider>,
      { initialState: state } as MagicKeyValue,
      true
    );

    const applyButton = queries.getByText(wrapper.container, 'txt_apply');
    applyButton.click();
    // Assertion
    expect(applyButton).toBeEnabled();
  });

  it('Case search with date range - just having redux form data', async () => {
    // Arrange
    const state = {
      memoChronicle: {
        [storeId]: {
          configs: {
            start: 0,
            end: 50,
            search: {
              memoText: 'a',
              dateType: { dateRange: false, specificDate: true },
              searchBy: { group: true, type: false }
            }
          },
          refData: {
            loading: false,
            typeList: [{ value: 'Memo 1', description: 'Memo 1' }],
            groupList: [{ value: 'Group 1', description: 'Group 1' }]
          }
        }
      },
      form: {
        [`chronicleSearchFormView-${storeId}`]: {
          initial: {
            dateType: { dateRange: false, specificDate: true },
            memoText: '',
            searchBy: { group: true, type: false }
          },
          registeredFields: {
            dateType: { name: 'dateType', type: 'Field', count: 1 },
            memoChronicleSearchTitle: {
              name: 'memoChronicleSearchTitle',
              type: 'Field',
              count: 1
            },
            memoDate: { name: 'memoDate', type: 'Field', count: 1 },
            memoDateRange: { name: 'memoDateRange', type: 'Field', count: 1 },
            memoGroup: { name: 'memoGroup', type: 'Field', count: 1 },
            memoText: { name: 'memoText', type: 'Field', count: 1 },
            memoType: { name: 'memoType', type: 'Field', count: 1 },
            searchBy: { name: 'searchBy', type: 'Field', count: 1 }
          },
          values: {
            dateType: { dateRange: false, specificDate: true },
            memoText: 'a',
            memoDateRange: {
              end: new Date().toISOString()
            },
            searchBy: { first: true, second: false }
          }
        }
      }
    };
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <Search onClose={jest.fn()} />
      </CustomStoreIdProvider>,
      { initialState: state } as MagicKeyValue,
      true
    );

    const applyButton = queries.getByText(wrapper.container, 'txt_apply');
    applyButton.click();
    // Assertion
    expect(applyButton).toBeEnabled();
  });

  it('Case search with keyword and memo type', async () => {
    // Arrange
    const state = {
      memoChronicle: {
        [storeId]: {
          configs: {
            start: 0,
            end: 50,
            search: {
              memoText: 'a',
              memoType: { value: 'test', description: 'test ' },
              dateType: { dateRange: false, specificDate: true },
              searchBy: { group: true, type: false }
            }
          },
          refData: {
            loading: false,
            typeList: [{ value: 'Memo 1', description: 'Memo 1' }],
            groupList: [{ value: 'Group 1', description: 'Group 1' }]
          }
        }
      },
      form: {
        [`chronicleSearchFormView-${storeId}`]: {
          initial: {
            dateType: { dateRange: false, specificDate: true },
            memoText: '',
            searchBy: { group: true, type: false }
          },
          registeredFields: {
            dateType: { name: 'dateType', type: 'Field', count: 1 },
            memoChronicleSearchTitle: {
              name: 'memoChronicleSearchTitle',
              type: 'Field',
              count: 1
            },
            memoDate: { name: 'memoDate', type: 'Field', count: 1 },
            memoDateRange: { name: 'memoDateRange', type: 'Field', count: 1 },
            memoGroup: { name: 'memoGroup', type: 'Field', count: 1 },
            memoText: { name: 'memoText', type: 'Field', count: 1 },
            memoType: { name: 'memoType', type: 'Field', count: 1 },
            searchBy: { name: 'searchBy', type: 'Field', count: 1 }
          },
          values: {
            dateType: { dateRange: false, specificDate: true },
            memoText: 'a',
            memoType: { value: 'test', description: 'test ' },
            searchBy: { first: true, second: false }
          }
        }
      }
    };
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <Search onClose={jest.fn()} />
      </CustomStoreIdProvider>,
      { initialState: state } as MagicKeyValue,
      true
    );

    const applyButton = queries.getByText(wrapper.container, 'txt_apply');
    applyButton.click();
    // Assertion
    expect(applyButton).toBeEnabled();
  });

  it('Case search with dateRange start', async () => {
    // Arrange
    const state = {
      memoChronicle: {
        [storeId]: {
          configs: {
            start: 0,
            end: 50,
            search: {
              memoText: 'a',
              memoType: { value: 'test', description: 'test ' },
              dateType: { dateRange: false, specificDate: true },
              searchBy: { group: true, type: false }
            }
          },
          refData: {
            loading: false,
            typeList: [{ value: 'Memo 1', description: 'Memo 1' }],
            groupList: [{ value: 'Group 1', description: 'Group 1' }]
          }
        }
      },
      form: {
        [`chronicleSearchFormView-${storeId}`]: {
          initial: {
            dateType: { dateRange: true, specificDate: true },
            memoText: '',
            searchBy: { group: true, type: false }
          },
          registeredFields: {
            dateType: { name: 'dateType', type: 'Field', count: 1 },
            memoChronicleSearchTitle: {
              name: 'memoChronicleSearchTitle',
              type: 'Field',
              count: 1
            },
            memoDate: { name: 'memoDate', type: 'Field', count: 1 },
            memoDateRange: {
              name: 'memoDateRange',
              type: 'Field',
              count: 1,
              start: 'start',
              end: 'end'
            },
            memoGroup: { name: 'memoGroup', type: 'Field', count: 1 },
            memoText: { name: 'memoText', type: 'Field', count: 1 },
            memoType: { name: 'memoType', type: 'Field', count: 1 },
            searchBy: { name: 'searchBy', type: 'Field', count: 1 }
          },
          values: {
            memoDateRange: {
              name: 'memoDateRange',
              type: 'Field',
              count: 1,
              start: 'start'
            },
            dateType: { dateRange: true, specificDate: true },
            memoText: 'a',
            memoType: { value: 'test', description: 'test ' },
            searchBy: { first: true, second: false }
          }
        }
      }
    };
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <Search onClose={jest.fn()} />
      </CustomStoreIdProvider>,
      { initialState: state } as MagicKeyValue,
      true
    );

    const applyButton = queries.getByText(wrapper.container, 'txt_apply');
    applyButton.click();
    // Assertion
    expect(applyButton).toBeEnabled();
  });

  it('Case search with dateRange end', async () => {
    // Arrange
    const state = {
      memoChronicle: {
        [storeId]: {
          configs: {
            start: 0,
            end: 50,
            search: {
              memoText: 'a',
              memoType: { value: 'test', description: 'test ' },
              dateType: { dateRange: false, specificDate: true },
              searchBy: { group: true, type: false }
            }
          },
          refData: {
            loading: false,
            typeList: [{ value: 'Memo 1', description: 'Memo 1' }],
            groupList: [{ value: 'Group 1', description: 'Group 1' }]
          }
        }
      },
      form: {
        [`chronicleSearchFormView-${storeId}`]: {
          initial: {
            dateType: { dateRange: true, specificDate: true },
            memoText: '',
            searchBy: { group: true, type: false }
          },
          registeredFields: {
            dateType: { name: 'dateType', type: 'Field', count: 1 },
            memoChronicleSearchTitle: {
              name: 'memoChronicleSearchTitle',
              type: 'Field',
              count: 1
            },
            memoDate: { name: 'memoDate', type: 'Field', count: 1 },
            memoDateRange: {
              name: 'memoDateRange',
              type: 'Field',
              count: 1,
              start: 'start',
              end: 'end'
            },
            memoGroup: { name: 'memoGroup', type: 'Field', count: 1 },
            memoText: { name: 'memoText', type: 'Field', count: 1 },
            memoType: { name: 'memoType', type: 'Field', count: 1 },
            searchBy: { name: 'searchBy', type: 'Field', count: 1 }
          },
          values: {
            memoDateRange: {
              name: 'memoDateRange',
              type: 'Field',
              count: 1,
              end: 'end'
            },
            dateType: { dateRange: true, specificDate: true },
            memoText: 'a',
            memoType: { value: 'test', description: 'test ' },
            searchBy: { first: true, second: false }
          }
        }
      }
    };
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <Search onClose={jest.fn()} />
      </CustomStoreIdProvider>,
      { initialState: state } as MagicKeyValue,
      true
    );

    const applyButton = queries.getByText(wrapper.container, 'txt_apply');
    applyButton.click();
    // Assertion
    expect(applyButton).toBeEnabled();
  });

  it('Case search with dateRange start & end', async () => {
    // Arrange
    const state = {
      memoChronicle: {
        [storeId]: {
          configs: {
            start: 0,
            end: 50,
            search: {
              memoText: 'a',
              memoType: { value: 'test', description: 'test ' },
              dateType: { dateRange: false, specificDate: true },
              searchBy: { group: true, type: false }
            }
          },
          refData: {
            loading: false,
            typeList: [{ value: 'Memo 1', description: 'Memo 1' }],
            groupList: [{ value: 'Group 1', description: 'Group 1' }]
          }
        }
      },
      form: {
        [`chronicleSearchFormView-${storeId}`]: {
          initial: {
            dateType: { dateRange: true, specificDate: true },
            memoText: '',
            searchBy: { group: true, type: false }
          },
          registeredFields: {
            dateType: { name: 'dateType', type: 'Field', count: 1 },
            memoChronicleSearchTitle: {
              name: 'memoChronicleSearchTitle',
              type: 'Field',
              count: 1
            },
            memoDate: { name: 'memoDate', type: 'Field', count: 1 },
            memoDateRange: {
              name: 'memoDateRange',
              type: 'Field',
              count: 1,
              start: 'start',
              end: 'end'
            },
            memoGroup: { name: 'memoGroup', type: 'Field', count: 1 },
            memoText: { name: 'memoText', type: 'Field', count: 1 },
            memoType: { name: 'memoType', type: 'Field', count: 1 },
            searchBy: { name: 'searchBy', type: 'Field', count: 1 }
          },
          values: {
            memoDateRange: {
              name: 'memoDateRange',
              type: 'Field',
              count: 1,
              start: 'start',
              end: 'end'
            },
            dateType: { dateRange: true, specificDate: true },
            memoText: 'a',
            memoType: { value: 'test', description: 'test ' },
            searchBy: { first: true, second: false }
          }
        }
      }
    };
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <Search onClose={jest.fn()} />
      </CustomStoreIdProvider>,
      { initialState: state } as MagicKeyValue,
      true
    );

    const applyButton = queries.getByText(wrapper.container, 'txt_apply');
    applyButton.click();
    // Assertion
    expect(applyButton).toBeEnabled();
  });

  it('Case search with empty memoDate & memoDateRange', async () => {
    // Arrange
    const state = {
      memoChronicle: {
        [storeId]: {
          configs: {
            start: 0,
            end: 50,
            search: {
              memoText: 'a',
              memoType: { value: 'test', description: 'test ' },
              dateType: { dateRange: false, specificDate: true },
              searchBy: { group: true, type: false }
            }
          },
          refData: {
            loading: false,
            typeList: [{ value: 'Memo 1', description: 'Memo 1' }],
            groupList: [{ value: 'Group 1', description: 'Group 1' }]
          }
        }
      },
      form: {
        [`chronicleSearchFormView-${storeId}`]: {
          initial: {
            dateType: { dateRange: true, specificDate: true },
            memoText: '',
            searchBy: { group: true, type: false }
          },
          registeredFields: {
            dateType: { name: 'dateType', type: 'Field', count: 1 },
            memoChronicleSearchTitle: {
              name: 'memoChronicleSearchTitle',
              type: 'Field',
              count: 1
            },
            memoDate: { name: 'memoDate', type: 'Field', count: 1 },
            memoDateRange: {
              name: 'memoDateRange',
              type: 'Field',
              count: 1,
              start: 'start',
              end: 'end'
            },
            memoGroup: { name: 'memoGroup', type: 'Field', count: 1 },
            memoText: { name: 'memoText', type: 'Field', count: 1 },
            memoType: { name: 'memoType', type: 'Field', count: 1 },
            searchBy: { name: 'searchBy', type: 'Field', count: 1 }
          },
          values: {
            dateType: { dateRange: true, specificDate: true },
            memoText: 'a',
            memoType: { value: 'test', description: 'test ' },
            searchBy: { first: true, second: false }
          }
        }
      }
    };
    // Act
    const { wrapper } = renderMockStore(
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <Search onClose={jest.fn()} />
      </CustomStoreIdProvider>,
      { initialState: state } as MagicKeyValue,
      true
    );

    const applyButton = queries.getByText(wrapper.container, 'txt_apply');
    applyButton.click();
    // Assertion
    expect(applyButton).toBeEnabled();
  });
});
