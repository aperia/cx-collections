// helpers
import { FormatTime } from 'app/constants/enums';
import { formatTimeDefault } from 'app/helpers';
import isEmpty from 'lodash.isempty';
import isEqual from 'lodash.isequal';
import isObject from 'lodash.isobject';
import isString from 'lodash.isstring';
import orderBy from 'lodash.orderby';

// types
import {
  MemoChronicle,
  MemoChronicleConfig,
  MemoChronicleSort
} from 'pages/Memos/Chronicle/types';
import { SearchMemoChronicleDataField } from './constants';

export const buildMemoData = (
  memos: MemoChronicle[] = [],
  configs: MemoChronicleConfig
) => {
  if (memos.length === 0 || !configs) return memos;
  const { sort, search } = configs;
  if (search) {
    memos = handleSearchMemos(memos, search);
  }
  if (sort) {
    memos = handleSortMemos(memos, sort);
  }
  return memos;
};

export const handleSearchMemos = (
  memos: MemoChronicle[],
  search: Partial<MemoChronicle>
) => {
  if (isEmpty(search)) return memos;

  return memos.filter((memo: MemoChronicle) => {
    return Object.keys(search).every(item => {
      const field = item as keyof MemoChronicle;

      if (isEmpty(search[field])) return true;
      if (field === SearchMemoChronicleDataField.DATE_TYPE_RADIO) return true;
      if (field === SearchMemoChronicleDataField.SEARCH_BY_RADIO) return true;
      if (
        field === SearchMemoChronicleDataField.MEMO_SEARCH_DATE &&
        search[field]
      ) {
        const searchDateValue = parseChronicleDateFormat(`${search[field]}`);
        return memo[field] === searchDateValue;
      }
      if (isString(search[field]))
        return memo[field]
          ?.toString()
          ?.toLocaleLowerCase()
          ?.includes(search[field]!.toString().toLowerCase());
      const fieldObj = search[field];
      if (
        isObject(fieldObj) &&
        item === SearchMemoChronicleDataField.DATE_RANGE_PICKER
      ) {
        return true;
      }
      return isEqual(memo[field], fieldObj);
    });
  });
};

export const handleSortMemos = (
  memos: MemoChronicle[],
  sort: MemoChronicleSort
) => {
  const { field, type } = sort;

  const sortByType = {
    dateTime: () =>
      orderBy(
        memos,
        item =>
          formatTimeDefault(item.memoDate, FormatTime.UTCDate)?.toString() ||
          item.memoTime,
        [type, type]
      )
  };

  const handleSortType = sortByType[field];
  if (!handleSortType) return memos;
  return handleSortType();
};

// Return date format: YYYY-MM-DD
export const parseChronicleDateFormat = (value: string) => {
  const parsedDate = new Date(value);
  const year = parsedDate.getFullYear();
  const month = parsedDate.getMonth() + 1;
  const finalMonth = month >= 10 ? month : `0${month}`;
  const day = parsedDate.getDate();
  const finalDay = day >= 10 ? day : `0${day}`;
  return `${year}-${finalMonth}-${finalDay}`;
};
