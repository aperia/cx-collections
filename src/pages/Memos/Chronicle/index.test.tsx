import React from 'react';
import { screen } from '@testing-library/react';
import { CustomStoreIdProvider } from 'app/hooks';
import { renderMockStore, storeId } from 'app/test-utils';
import AddMemoChronicle from './index';
import * as entitlementsHelpers from 'app/entitlements/helpers';

jest.mock('./Add', () => () => <div>AddMemoChronicle</div>);
jest.mock('./Edit', () => () => <div>EditMemoChronicle</div>);

const initialState: Partial<RootState> = { memoChronicle: { [storeId]: {} } };

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <CustomStoreIdProvider value={{ customStoreId: storeId }}>
      <AddMemoChronicle />
    </CustomStoreIdProvider>,
    { initialState }
  );
};

beforeEach(() => {
  jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
});

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper(initialState);

    expect(screen.getByText('AddMemoChronicle')).toBeInTheDocument();
    expect(screen.getByText('EditMemoChronicle')).toBeInTheDocument();
  });
});
