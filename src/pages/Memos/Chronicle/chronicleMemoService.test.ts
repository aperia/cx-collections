import cisMemoServices, {
  AddChronicleMemoRequest,
  EditChronicleMemoRequest,
  MemoChronicleBehaviorsRequest,
  MemoChronicleFiltersRequest,
  MemoLookupRequest
} from './chronicleMemoService';

// utils
import { mockApiServices, storeId } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import {
  AddChronicleMemoRequestData,
  EditChronicleMemoRequestData,
  DeleteChronicleMemoRequestArgs,
  GetMemoChronicleMemoRequestData
} from './types';
import { formatDateToSubmit } from 'app/helpers';

describe('cisMemoServices', () => {
  describe('getChronicleMemosList', () => {
    const params: GetMemoChronicleMemoRequestData = {
      accountId: storeId,
      startSequence: 'startSequence',
      endSequence: 'endSequence',
      endDate: '03/04/2021',
      startDate: '09/04/2021',
      memoDate: '03/54/2021'
    };

    const mapRequestBody = (data: GetMemoChronicleMemoRequestData) => {
      const requestBody: MemoLookupRequest = {
        common: { accountId: data.accountId },
        startSequence: data.startSequence,
        endSequence: data.endSequence,
        filterIdentifier: 'ALL',
        selectFields: [
          'memoIdentifier',
          'memoOperatorIdentifier',
          'memoDate',
          'memoTime',
          'behaviorIdentifier',
          'memoText',
          'memoDeletionDate',
          'filterIdentifier'
        ]
      };
      if (data.endDate && data.startDate) {
        requestBody.memoDate = formatDateToSubmit(data.startDate);
        requestBody.memoEndDate = formatDateToSubmit(data.endDate);
        requestBody.memoDateCriteria = 'BETWEEN';
      } else {
        requestBody.memoDate =
          data.memoDate !== undefined
            ? formatDateToSubmit(data.memoDate)
            : undefined;
      }

      return requestBody;
    };

    it('when params do not have endDate and memoDate', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      const newParams = {
        ...params,
        endDate: undefined,
        memoDate: undefined
      };

      cisMemoServices.getChronicleMemosList(newParams);

      expect(mockService).toBeCalledWith('', mapRequestBody(newParams));
    });

    it('when params do not have endDate', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      const newParams = {
        ...params,
        endDate: undefined
      };

      cisMemoServices.getChronicleMemosList(newParams);

      expect(mockService).toBeCalledWith('', mapRequestBody(newParams));
    });

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cisMemoServices.getChronicleMemosList(params);

      expect(mockService).toBeCalledWith('', mapRequestBody(params));
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cisMemoServices.getChronicleMemosList(params);

      expect(mockService).toBeCalledWith(
        apiUrl.memo.getChronicleMemosList,
        mapRequestBody(params)
      );
    });
  });

  describe('getMemoChronicleBehaviors', () => {
    const params: MemoChronicleBehaviorsRequest = {
      common: { accountId: storeId },
      behaviorIdentifier: 'behaviorIdentifier'
    };

    const defaultRequestBody = {
      selectFields: ['behaviorDescriptionText', 'behaviorIdentifier'],
      startSequence: 1,
      endSequence: 1000
    };

    it('when params is defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cisMemoServices.getMemoChronicleBehaviors();

      expect(mockService).toBeCalledWith('', defaultRequestBody);
    });

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cisMemoServices.getMemoChronicleBehaviors(params);

      expect(mockService).toBeCalledWith('', {
        ...defaultRequestBody,
        ...params
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cisMemoServices.getMemoChronicleBehaviors(params);

      expect(mockService).toBeCalledWith(
        apiUrl.memo.getMemoChronicleBehaviors,
        { ...defaultRequestBody, ...params }
      );
    });
  });

  describe('getMemoChronicleFilters', () => {
    const params: MemoChronicleFiltersRequest = {
      common: { accountId: storeId }
    };
    const defaultRequestBody = {
      selectFields: ['filterDescriptionText', 'filterIdentifier'],
      startSequence: 1,
      endSequence: 1000
    };

    it('when params is undefined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cisMemoServices.getMemoChronicleFilters();

      expect(mockService).toBeCalledWith('', defaultRequestBody);
    });

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cisMemoServices.getMemoChronicleFilters(params);

      expect(mockService).toBeCalledWith('', {
        ...defaultRequestBody,
        ...params
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cisMemoServices.getMemoChronicleFilters(params);

      expect(mockService).toBeCalledWith(apiUrl.memo.getMemoChronicleFilters, {
        ...defaultRequestBody,
        ...params
      });
    });
  });

  describe('deleteChronicleMemo', () => {
    const params: DeleteChronicleMemoRequestArgs['postData'] = {
      accountId: storeId,
      memoIdentifier: 'memoIdentifier'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cisMemoServices.deleteChronicleMemo(params);

      expect(mockService).toBeCalledWith('', {
        common: { accountId: params.accountId },
        memoIdentifier: params.memoIdentifier
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cisMemoServices.deleteChronicleMemo(params);

      expect(mockService).toBeCalledWith(apiUrl.memo.deleteChronicleMemo, {
        common: { accountId: params.accountId },
        memoIdentifier: params.memoIdentifier
      });
    });
  });

  describe('addChronicleMemo', () => {
    const params: AddChronicleMemoRequestData = {
      accountId: storeId,
      memoType: { value: 'memoType', description: '' },
      overrideRetentionDate: new Date()
    };
    const token = 'token';

    const mapRequestBody = (requestData: AddChronicleMemoRequestData) => {
      const requestBody: AddChronicleMemoRequest = {
        common: { accountId: requestData.accountId },
        behaviorIdentifier: requestData.memoType.value,
        memoText: requestData.memoText ?? ''
      };
      if (requestData.overrideRetentionDate) {
        const newOverrideRetentionDate = formatDateToSubmit(
          requestData.overrideRetentionDate
        );
        requestBody.overrideRetentionDate = newOverrideRetentionDate;
      }

      return requestBody;
    };

    it('when params do not have overrideRetentionDate', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      const newParams = { ...params, overrideRetentionDate: undefined };

      cisMemoServices.addChronicleMemo(newParams, token);

      expect(mockService).toBeCalledWith('', mapRequestBody(newParams), {
        headers: { Authorization: token }
      });
    });

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cisMemoServices.addChronicleMemo(params, token);

      expect(mockService).toBeCalledWith('', mapRequestBody(params), {
        headers: { Authorization: token }
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cisMemoServices.addChronicleMemo(params, token);

      expect(mockService).toBeCalledWith(
        apiUrl.memo.addChronicleMemo,
        mapRequestBody(params),
        { headers: { Authorization: token } }
      );
    });
  });

  describe('updateChronicleMemo', () => {
    const params: EditChronicleMemoRequestData = {
      memoType: { value: 'memoType', description: '' },
      overrideRetentionDate: new Date(),
      memoText: 'memoText',
      memoIdentifier: 'memoIdentifier',
      accountId: storeId
    };

    const mapRequestBody = (requestData: EditChronicleMemoRequestData) => {
      const requestBody: EditChronicleMemoRequest = {
        common: { accountId: requestData.accountId },
        behaviorIdentifier: requestData.memoType?.value,
        memoText: requestData.memoText ?? '',
        memoIdentifier: requestData.memoIdentifier
      };
      if (requestData.overrideRetentionDate === null) {
        requestBody.overrideRetentionDate = undefined;
      }
      if (requestData.overrideRetentionDate) {
        const newOverrideRetentionDate = formatDateToSubmit(
          requestData.overrideRetentionDate
        );
        requestBody.overrideRetentionDate = newOverrideRetentionDate;
      }

      return requestBody;
    };

    it('when params is empty', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      const params = {
        overrideRetentionDate: null
      } as unknown as EditChronicleMemoRequestData;

      cisMemoServices.updateChronicleMemo(params);

      expect(mockService).toBeCalledWith('', mapRequestBody(params));
    });

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cisMemoServices.updateChronicleMemo(params);

      expect(mockService).toBeCalledWith('', mapRequestBody(params));
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cisMemoServices.updateChronicleMemo(params);

      expect(mockService).toBeCalledWith(
        apiUrl.memo.updateChronicleMemo,
        mapRequestBody(params)
      );
    });
  });
});
