import React, { useState } from 'react';

// utils
import classnames from 'classnames';

// hooks
import { useCustomStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// components/ types
import { Icon, Button, Popover, Tooltip } from 'app/_libraries/_dls/components';
import TogglePin from 'pages/__commons/InfoBar/TogglePin';
import Search from '../Search';
import Sort from '../Sort';
import Expand from '../Expand';
import { MemoProps } from 'pages/Memos';

import { selectHasIndicator } from 'pages/Memos/Chronicle/_redux/selectors';

// i18 next
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { I18N_MEMOS } from '../../constants';

// redux
import { batch, useDispatch } from 'react-redux';
import { selectPinned } from 'pages/__commons/InfoBar/_redux/selectors';
import { actionsInfoBar, InfoBarSection } from 'pages/__commons/InfoBar/_redux';
import { useStoreIdSelector } from 'app/hooks/useStoreIdSelector';
import { useAccountDetail } from 'app/hooks/useAccountDetail';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface HeaderProps extends MemoProps {}

const Header: React.FC<HeaderProps> = ({ shouldRenderPinnedIcon = true }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const { isHasSearchIndicator } =
    useCustomStoreIdSelector<Record<string, boolean>>(selectHasIndicator);
  const pinned = useStoreIdSelector(selectPinned);
  const isPinned = pinned === InfoBarSection.Memos;

  const [openSearch, setOpenSearch] = useState(false);

  const handleTogglePin = () => {
    batch(() => {
      const thisPinned = isPinned ? null : InfoBarSection.Memos;

      dispatch(
        actionsInfoBar.updatePinned({
          storeId,
          pinned: thisPinned
        })
      );

      if (thisPinned) {
        dispatch(actionsInfoBar.updateRecover({ storeId, recover: null }));
      }

      dispatch(actionsInfoBar.updateIsExpand({ storeId, isExpand: !isPinned }));
    });
  };

  return (
    <div className="p-24">
      <div className="d-flex justify-content-between">
        <div className="d-inline-flex">
          <h4
            className="mr-8"
            data-testid={genAmtId('chronicle-memo-header', 'title', '')}
          >
            {t(I18N_MEMOS.MEMOS)}
          </h4>
          {shouldRenderPinnedIcon && (
            <TogglePin
              isPinned={isPinned}
              onToggle={handleTogglePin}
              dataTestId="chronicle-memo-header_pin-btn"
            />
          )}
        </div>
        <div className="d-flex flex-row column ">
          <div className="mr-16">
            <Tooltip
              element={t(I18N_COMMON_TEXT.SEARCH)}
              placement="top"
              variant="primary"
              dataTestId="chronicle-memo-header_search-tooltip"
            >
              <Popover
                element={<Search onClose={() => setOpenSearch(false)} />}
                placement="bottom-end"
                containerClassName="inside-infobar mr-16 search-tooltip"
                size="md"
                opened={openSearch}
                onVisibilityChange={value => setOpenSearch(value)}
                dataTestId="chronicle-memo-header_search"
              >
                <Button
                  id="chronicle-memo-header_search-btn"
                  variant="icon-secondary"
                  className={classnames({
                    asterisk: isHasSearchIndicator,
                    active: openSearch
                  })}
                  onClick={() => setOpenSearch(value => !value)}
                  dataTestId="chronicle-memo-header_search-btn"
                >
                  <Icon
                    name="search"
                    dataTestId="chronicle-memo-header_search-icon"
                  />
                </Button>
              </Popover>
            </Tooltip>
          </div>
          <div className="mr-16">
            <Sort />
          </div>
          <Expand />
        </div>
      </div>
      <h6
        className="color-grey-l24 text-uppercase mt-4"
        data-testid={genAmtId('chronicle-memo-header', 'chronicle-title', '')}
      >
        {t(I18N_MEMOS.CHRONICLE)}
      </h6>
    </div>
  );
};

export default Header;
