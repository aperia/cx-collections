import React, { ReactElement, ReactNode } from 'react';
import { AccountDetailProvider, CustomStoreIdProvider } from 'app/hooks';
import {
  mockActionCreator,
  renderMockStore,
  storeId,
  accEValue
} from 'app/test-utils';
import Header from './index';
import { screen } from '@testing-library/react';
import { SearchProps } from '../Search';
import { TogglePinProps } from 'pages/__commons/InfoBar/TogglePin';
import { actionsInfoBar, InfoBarSection } from 'pages/__commons/InfoBar/_redux';
import { queryById } from 'app/_libraries/_dls/test-utils';

jest.mock('../Search/index', () => {
  const MockSearchComponent = (props: SearchProps) => (
    <div>
      Search Component<button onClick={props.onClose}>onClose</button>
    </div>
  );
  return MockSearchComponent;
});

jest.mock('../Sort/index', () => {
  const MockSortComponent = () => <div>Sort Component</div>;
  return MockSortComponent;
});

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');

  return {
    ...dlsComponents,
    Popover: (props: {
      element: ReactElement;
      onVisibilityChange: () => void;
      children: ReactNode;
    }) => {
      return (
        <div>
          Popover mock {props.element}
          <button onClick={props.onVisibilityChange}>Click</button>
          {props.children}
        </div>
      );
    }
  };
});

jest.mock(
  'pages/__commons/InfoBar/TogglePin',
  () => (props: TogglePinProps) => {
    return <div onClick={props.onToggle}>TogglePin</div>;
  }
);

const renderComponent = (initialState: Partial<RootState>) => {
  const { wrapper } = renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <CustomStoreIdProvider value={{ customStoreId: storeId }}>
        <Header />
      </CustomStoreIdProvider>
    </AccountDetailProvider>,
    { initialState }
  );

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement
  };
};

const infoBarSpy = mockActionCreator(actionsInfoBar);

describe('Test Header Chronicle memo', () => {
  it('Should render Search component', async () => {
    // Arrange
    const initialState = {
      memoChronicle: {
        [storeId]: {
          configs: {}
        }
      }
    };

    // Act
    renderComponent(initialState);

    const btn = screen.getByText('onClose');
    btn.click();

    const clickBtn = screen.getByText('Click');
    clickBtn.click();

    // Assertion
    const searchComponent = await screen.findByText('Search Component');
    expect(searchComponent).toBeInTheDocument();
  });

  it('call actions of toggle', async () => {
    // Arrange
    const updatePinnedSpy = infoBarSpy('updatePinned');
    const updateRecoverSpy = infoBarSpy('updateRecover');
    const updateIsExpandSpy = infoBarSpy('updateIsExpand');

    const initialState = {
      memoChronicle: {
        [storeId]: {
          configs: {}
        }
      }
    };

    // Act
    renderComponent(initialState);

    const toggle = screen.getByText('TogglePin');
    toggle.click();

    // Assertion
    expect(updatePinnedSpy).toBeCalledWith({
      storeId,
      pinned: InfoBarSection.Memos
    });
    expect(updateRecoverSpy).toBeCalledWith({
      storeId,
      recover: null
    });
    expect(updateIsExpandSpy).toBeCalledWith({
      storeId,
      isExpand: true
    });
  });

  it('info bar is pinned', async () => {
    // Arrange
    const updatePinnedSpy = infoBarSpy('updatePinned');
    const updateIsExpandSpy = infoBarSpy('updateIsExpand');

    const initialState = {
      memoChronicle: {
        [storeId]: {
          configs: {}
        }
      },
      infoBar: {
        [storeId]: {
          pinned: InfoBarSection.Memos,
          isExpand: true,
          recover: InfoBarSection.Memos
        }
      }
    };

    // Act
    renderComponent(initialState);

    const toggle = screen.getByText('TogglePin');
    toggle.click();

    // Assertion
    expect(updatePinnedSpy).toBeCalledWith({
      storeId,
      pinned: null
    });
    expect(updateIsExpandSpy).toBeCalledWith({
      storeId,
      isExpand: false
    });
  });

  it('onSearch Click', async () => {
    const initialState = {
      memoChronicle: {
        [storeId]: {
          configs: {}
        }
      },
      infoBar: {
        [storeId]: {
          pinned: InfoBarSection.Memos,
          isExpand: true,
          recover: InfoBarSection.Memos
        }
      }
    };

    // Act
    const { wrapper } = renderComponent(initialState);
    const button = queryById(
      wrapper.container,
      'chronicle-memo-header_search-btn'
    )!;
    button.click();

    expect(wrapper.getByText('txt_memos_chronicle')).toBeInTheDocument();
  });
});
