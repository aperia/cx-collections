import React, { useMemo } from 'react';

// component
import TextTruncateCharacter from 'app/components/TruncateTextCharacter';
import MoreActions, { ActionValue } from 'app/components/MoreActions';

// redux
import { useDispatch } from 'react-redux';
import { actionMemoChronicle } from 'pages/Memos/Chronicle/_redux/reducers';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { selectChronicleMemoTypeList } from 'pages/Memos/Chronicle/_redux/selectors';
import { triggerDeleteChronicleMemo } from 'pages/Memos/Chronicle/_redux/delete';

// hooks
import {
  useAccountDetail,
  useCustomStoreIdSelector,
  useCustomStoreId,
  useMemoProvider
} from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constant/ helper
import { MAX_LENGTH_MEMO_TEXT } from '../constants';
import { formatTime, timeSplit } from 'app/helpers';
import { subtract } from 'date-and-time';
import isArray from 'lodash.isarray';
import { MemoChronicleData, MemoChronicleType } from '../types';
import classNames from 'classnames';

// i18 constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { I18N_MEMOS } from '../../constants';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

export interface MemoChronicleCardProps {
  id: string;
  memo: MemoChronicleData;
  isExpand?: boolean;
  dataTestId?: string;
}

const Item: React.FC<MemoChronicleCardProps> = ({
  memo,
  isExpand,
  id,
  dataTestId
}) => {
  const { t } = useTranslation();
  const { isHiddenAddSection = false } = useMemoProvider();

  const getDisplayDate = (memoDateStr: string) => {
    const memoDate = formatTime(memoDateStr).date || '';
    const currentDate = formatTime(new Date().toISOString()).date;
    const days = subtract(new Date(memoDate), new Date(currentDate!)).toDays();
    let display = '';

    if (days === 0) {
      display = t(I18N_COMMON_TEXT.TODAY);
    } else if (days === -1) {
      display = t(I18N_COMMON_TEXT.YESTERDAY);
    }

    if (display) {
      return `${display} ${formatTime(memoDateStr).fullTimeMeridiem}`;
    }
    return formatTime(memoDateStr).allDatetime;
  };

  const dispatch = useDispatch();
  const { accEValue, storeId: accStoreId } = useAccountDetail();
  const { customStoreId: storeId } = useCustomStoreId();

  const memoTypeData = useCustomStoreIdSelector<MemoChronicleType[]>(
    selectChronicleMemoTypeList
  );

  const testId = genAmtId(
    dataTestId!,
    'chronicle-memo-list-item',
    'ChronicleMemo.List.Item'
  );

  const handleConfirmDeleteMemo = () => {
    dispatch(
      triggerDeleteChronicleMemo({
        storeId,
        postData: {
          memoIdentifier: memo.memoIdentifier,
          accountId: accEValue
        }
      })
    );
  };

  const handleDeleteMemo = () => {
    dispatch(
      confirmModalActions.open({
        title: I18N_MEMOS.CONFIRM_DELETE,
        body: I18N_MEMOS.CONFIRM_DELETE_MESSAGE,
        confirmCallback: handleConfirmDeleteMemo,
        confirmText: I18N_COMMON_TEXT.DELETE,
        cancelText: I18N_COMMON_TEXT.CANCEL
      })
    );
  };

  const handleEditMemo = () => {
    const memoItem = {
      ...memo,
      memoType: null,
      memoGroup: null
    };
    dispatch(
      actionMemoChronicle.openEditChronicleMemo({
        storeId,
        memoItem
      })
    );
  };

  const handleSelectAction = (value: ActionValue) => {
    if (value.uniqueId === 'delete') {
      return handleDeleteMemo();
    } else {
      return handleEditMemo();
    }
  };

  const actions = useMemo(() => {
    const allActions = [
      {
        label: t(I18N_COMMON_TEXT.EDIT),
        uniqueId: 'edit'
      },
      {
        label: t(I18N_COMMON_TEXT.DELETE),
        uniqueId: 'delete'
      }
    ];

    return allActions.filter(item => {
      if (item.uniqueId === 'edit') {
        return checkPermission(PERMISSIONS.EDIT_MEMO, accStoreId);
      }
      return checkPermission(PERMISSIONS.DELETE_MEMO, accStoreId);
    });
  }, [t, accStoreId]);

  const memoBehaviorDescription: string = useMemo(() => {
    if (!isArray(memoTypeData)) return memo.behaviorIdentifier;
    const currentBehavior = memoTypeData.find(
      memoTypeDataItem =>
        memoTypeDataItem.value === memo.behaviorIdentifier.trim()
    );
    return currentBehavior?.description ?? memo.behaviorIdentifier;
  }, [memo.behaviorIdentifier, memoTypeData]);

  return (
    <div
      key={memo.memoIdentifier}
      className="mt-8 bg-light-l16 p-8 br-radius-12"
    >
      <div className="d-flex align-items-center justify-content-between">
        <div
          id={`${id}_memoChronicleOperatorIdentifier`}
          className="fs-14 color-grey-d20 fw-500"
          data-testid={genAmtId(
            testId!,
            'memoOperatorIdentifier',
            'MemosChronicle.Item'
          )}
        >
          {memo.memoOperatorIdentifier}
        </div>
        <div className="d-flex align-items-center">
          <div
            id={`${id}_memoChronicleDisplayDate`}
            data-testid={genAmtId(
              testId!,
              'memoChronicleDisplayDate',
              'MemosChronicle.Item'
            )}
            className="fs-12 color-grey"
          >
            {getDisplayDate(
              `${memo?.memoDate || ''} ${timeSplit(memo?.memoTime)}`
            )}
          </div>
          <div className={classNames('ml-8', { 'd-none': isHiddenAddSection })}>
            <MoreActions
              actions={actions}
              onSelect={handleSelectAction}
              id={`${storeId}-dropdown-button`}
              dataTestId={`${testId}_more-actions`}
              placementDropdown="bottom-end"
            />
          </div>
        </div>
      </div>
      <div className="d-flex align-items-center justify-content-between">
        <div
          className="fs-12 color-grey pt-4"
          data-testid={genAmtId(
            testId!,
            'behaviorIdentifier',
            'MemosChronicle.Item'
          )}
        >{`${memo?.behaviorIdentifier} - ${memoBehaviorDescription}`}</div>
      </div>
      <div className="fs-14 pt-8 color-grey-d20 text-break">
        <TextTruncateCharacter
          dataTestId={`${testId}_memo-text`}
          isShowMore={isExpand}
          maxLength={MAX_LENGTH_MEMO_TEXT}
          text={memo.memoText}
          textMore={t(I18N_COMMON_TEXT.MORE)}
          textLess={t(I18N_COMMON_TEXT.LESS)}
        />
      </div>
    </div>
  );
};

export default Item;
