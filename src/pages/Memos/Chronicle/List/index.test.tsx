import React from 'react';
import List from './index';
import { waitFor, screen } from '@testing-library/react';
import { CustomStoreIdProvider } from 'app/hooks';
import {
  renderMockStore,
  storeId,
  mockActionCreator,
  queryByClass
} from 'app/test-utils';
import {
  MemoChronicleData,
  MemoChronicleSortField,
  MemoChronicleSortType
} from '../types';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { actionMemoChronicle } from 'pages/Memos/Chronicle/_redux/reducers';
import { GetListType, SEARCH_DEFAULT } from '../constants';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

import * as useScreenType from 'app/_libraries/_dls/hooks/useScreenType';
import { ScreenType } from 'app/_libraries/_dls/hooks/useScreenType/types';

HTMLElement.prototype.scrollTo = jest.fn();

const memos = [
  {
    memoDate: '2021-02-27T15:53:07.502Z',
    memoIdentifier: '1',
    memoOperatorIdentifier: 'JZ5',
    memoText: 'Beatae qui magnam quaerat voluptates.',
    memoTime: '2020-09-07T14:20:34.028Z',
    behaviorIdentifier: '',
    filterIdentifier: ''
  },
  {
    memoDate: '2021-02-26T17:26:16.486Z',
    memoIdentifier: '2',
    memoOperatorIdentifier: 'K98',
    memoText: 'Minima sit magnam ut consequatur eveniet.',
    memoTime: '2020-06-14T15:18:17.111Z',
    behaviorIdentifier: '',
    filterIdentifier: ''
  }
];

const initialState: Partial<RootState> = {
  memoChronicle: {
    [storeId]: {
      memos
    }
  },
  apiErrorNotification: {
    [storeId]: {
      errorDetail: {
        inMemoFlyOut: {
          response: '500',
          request: '500'
        }
      }
    }
  }
};

const initialStateSingleMemo: Partial<RootState> = {
  memoChronicle: {
    [storeId]: {
      memos: [memos[0]]
    }
  }
};

const emptyState: Partial<RootState> = {
  memoChronicle: {
    [storeId]: {
      memos: {} as unknown as MemoChronicleData[]
    }
  }
};

const loadMemoState: Partial<RootState> = {
  memoChronicle: {
    [storeId]: {
      memos,
      loading: false
    }
  }
};

const searchMemoState: Partial<RootState> = {
  memoChronicle: {
    [storeId]: {
      configs: {
        search: { memoText: 'a', memoDate: new Date().toISOString() },
        sort: {
          field: MemoChronicleSortField.DATE_TIME,
          type: MemoChronicleSortType.ASC
        }
      },
      memos
    }
  }
};

const searchMemoWithRangeState: Partial<RootState> = {
  memoChronicle: {
    [storeId]: {
      configs: {
        search: {},
        sort: {
          field: MemoChronicleSortField.DATE_TIME,
          type: MemoChronicleSortType.ASC
        }
      },
      memos
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <CustomStoreIdProvider value={{ customStoreId: storeId }}>
      <List shouldRenderPinnedIcon={true} />
    </CustomStoreIdProvider>,
    { initialState }
  );
};

beforeEach(() => {
  jest
    .spyOn(useScreenType, 'default')
    .mockImplementation(() => ScreenType.DESKTOP);
});

const memoSpy = mockActionCreator(actionMemoChronicle);
const apiErrorNotificationActionSpy = mockActionCreator(
  apiErrorNotificationAction
);

describe('Test Chronicle List memo component', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterEach(() => {
    jest.runAllTimers();
    jest.clearAllMocks();
  });

  it('Render component -> empty configs', () => {
    waitFor(() => {
      renderWrapper({ memoChronicle: { [storeId]: { configs: {} } } });
    });

    //Render Header
    expect(screen.getByText('txt_memo')).toBeInTheDocument();
    //Render Body
  });

  it('Render component -> many memos', () => {
    waitFor(() => {
      renderWrapper(initialState);
    });

    //Render Header
    expect(screen.getByText('txt_memo')).toBeInTheDocument();
    //Render Body
  });

  it('Render component -> with empty memos', () => {
    waitFor(() => {
      renderWrapper(emptyState);
    });

    //Render Header
    expect(screen.getByText('txt_memo')).toBeInTheDocument();
  });

  it('Render component -> with single memos', () => {
    waitFor(() => {
      renderWrapper(initialStateSingleMemo);
    });

    //Render Header
    expect(screen.getByText('txt_memo')).toBeInTheDocument();
  });

  describe('handleLoadMore', () => {
    beforeEach(() => {
      memoSpy('getMemoChronicle');
    });

    it('when loading false', () => {
      waitFor(() => {
        const { wrapper } = renderWrapper(loadMemoState);
        expect(
          queryByClass(wrapper.container, /element-to-watch/)
        ).toBeInTheDocument();
      });
    });

    it('render when Error', () => {
      waitFor(() => {
        const { rerender } = renderWrapper(initialState);
        Object.defineProperty(Element.prototype, 'scrollHeight', {
          value: 100
        });
        rerender(
          <CustomStoreIdProvider value={{ customStoreId: storeId }}>
            <List shouldRenderPinnedIcon={true} />
          </CustomStoreIdProvider>
        );
        expect(screen.getByText('txt_view_detail')).toBeInTheDocument();
      });
    });
  });

  describe('handleClickErrorDetail', () => {
    beforeEach(() => {
      memoSpy('getMemoChronicle');
      apiErrorNotificationActionSpy('openApiDetailErrorModal');
    });

    it('render when Error', () => {
      const openApiDetailErrorModalSpy = apiErrorNotificationActionSpy(
        'openApiDetailErrorModal'
      );

      waitFor(() => {
        renderWrapper(initialState);
      });
      // Render Error
      expect(screen.getByText('txt_api_error')).toBeInTheDocument();
      const button = screen.getByText('txt_view_detail');
      button.click();
      //Trigger Button
      expect(openApiDetailErrorModalSpy).toBeCalledWith({
        forSection: 'inMemoFlyOut',
        storeId
      });
    });
  });

  describe('Click handleResetSearch', () => {
    beforeEach(() => {
      memoSpy('getMemoChronicle');
      memoSpy('setConfigsFilter');
    });

    afterAll(() => {
      jest.clearAllMocks();
    });

    it('case IsReset', () => {
      const getMemoChronicleSpy = memoSpy('getMemoChronicle');
      waitFor(() => {
        renderWrapper(searchMemoState);
      });

      const clearAndResetBtn = screen.getByText(
        I18N_COMMON_TEXT.CLEAR_AND_RESET
      );
      clearAndResetBtn.click();

      expect(getMemoChronicleSpy).toBeCalledWith({
        storeId,
        postData: {
          accountId: '',
          type: GetListType.RESET_SEARCH
        }
      });
    });

    it('case not IsReset', () => {
      const setConfigsFilterSpy = memoSpy('setConfigsFilter');
      waitFor(() => {
        renderWrapper(searchMemoWithRangeState);
      });

      const clearAndResetBtn = screen.getByText(
        I18N_COMMON_TEXT.CLEAR_AND_RESET
      );
      clearAndResetBtn.click();

      expect(setConfigsFilterSpy).toBeCalledWith({
        storeId,
        params: {
          search: SEARCH_DEFAULT
        }
      });
    });
  });
});
