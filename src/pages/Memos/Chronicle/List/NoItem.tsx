import React from 'react';

// component
import { Icon } from 'app/_libraries/_dls/components';
import NoResultIcon from 'app/components/NoResultIcon';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// i18n constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { I18N_MEMOS } from '../../constants';

export interface NoMemoProps {
  dataTestId?: string;
  isApplySearch: boolean;
  onResetSearch?: () => void;
}

export const NoMemo: React.FC<NoMemoProps> = ({
  dataTestId,
  isApplySearch,
  onResetSearch
}) => {
  const { t } = useTranslation();

  return isApplySearch ? (
    <NoResultIcon
      icon={<Icon className="color-light-l12 fs-80" name="comment" />}
      description={I18N_MEMOS.NO_RESULTS_FOUND}
      onReset={onResetSearch}
      resetText={t(I18N_COMMON_TEXT.CLEAR_AND_RESET)}
      dataTestId={dataTestId}
    />
  ) : (
    <NoResultIcon
      icon={<Icon className="color-light-l12 fs-80" name="comment" />}
      description={I18N_MEMOS.NO_RESULTS_DISPLAY}
      dataTestId={dataTestId}
    />
  );
};
export default NoMemo;
