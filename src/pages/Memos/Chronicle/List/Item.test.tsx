import React from 'react';
import dateTime from 'date-and-time';

import { fireEvent, screen } from '@testing-library/dom';
import { CustomStoreIdProvider } from 'app/hooks';
import { queryById, renderMockStore, storeId } from 'app/test-utils';
import Item from './Item';

import { MemoChronicleData, MemoChronicleType } from '../types';
import { MoreActionProps } from 'app/components/MoreActions';

// redux
import { actionMemoChronicle } from 'pages/Memos/Chronicle/_redux/reducers';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import * as deleteActions from 'pages/Memos/Chronicle/_redux/delete';
import { I18N_MEMOS } from 'pages/Memos/constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { formatTime } from 'app/helpers';

jest.mock('app/components/MoreActions', () => (props: MoreActionProps) => {
  const { onSelect = () => undefined } = props;
  return (
    <input
      data-testid="MoreActions_onSelect"
      onChange={(e: any) => onSelect(e.target)}
    />
  );
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const initialState: Partial<RootState> = {
  memoChronicle: {
    [storeId]: {
      refData: {
        typeList: [{ description: 'description', value: 'value' }]
      }
    }
  }
};

const wrongState: Partial<RootState> = {
  memoChronicle: {
    [storeId]: {
      refData: {
        typeList: {
          description: 'description',
          value: 'value'
        } as unknown as MemoChronicleType[]
      }
    }
  }
};

const memo: MemoChronicleData = {
  memoOperatorIdentifier: 'PSF',
  memoText: 'Id qui beatae laborum qui.',
  memoDate: formatTime(new Date().toISOString()).date!,
  memoTime: '04:23:59',
  memoIdentifier: '1',
  behaviorIdentifier: 'behaviorIdentifier',
  filterIdentifier: 'filterIdentifier'
};

const generateActions = () =>
  jest.fn().mockImplementation(() => ({ type: 'type' }));

const renderWrapper = (initialState: Partial<RootState>, memoData = memo) => {
  return renderMockStore(
    <CustomStoreIdProvider value={{ customStoreId: storeId }}>
      <Item id={storeId} memo={memoData} />
    </CustomStoreIdProvider>,
    { initialState }
  );
};

let spy: jest.SpyInstance;
let spy2: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();

  spy2?.mockReset();
  spy2?.mockRestore();
});

describe('Render', () => {
  it('Render UI with typeList', () => {
    const { baseElement } = renderWrapper(initialState);

    expect(queryById(baseElement, 'memoChronicleDisplayDate')).toBeNull();
  });

  it('Render UI with wrong typeList', () => {
    const { baseElement } = renderWrapper(wrongState);

    expect(queryById(baseElement, 'memoChronicleDisplayDate')).toBeNull();
  });

  describe('Render UI with date', () => {
    it('empty', () => {
      const newState = { ...memo, memoDate: '' };

      const { baseElement } = renderWrapper(initialState, newState);

      expect(
        queryById(baseElement, 'memoChronicleDisplayDate')?.textContent
      ).toBeUndefined();
    });

    it('today', () => {
      const today = new Date();
      const newState = {
        ...memo,
        memoDate: formatTime(today.toISOString()).date!
      };

      renderWrapper(initialState, newState);

      expect(
        screen.getByText(new RegExp(I18N_COMMON_TEXT.TODAY))
      ).toBeInTheDocument();
    });

    it('yesterday', () => {
      const yesterday = dateTime.addDays(new Date(), -1);
      const newState = {
        ...memo,
        memoDate: formatTime(yesterday.toISOString()).date!
      };

      renderWrapper(initialState, newState);

      expect(
        screen.getByText(new RegExp(I18N_COMMON_TEXT.YESTERDAY))
      ).toBeInTheDocument();
    });

    it('tomorrow', () => {
      const tomorrow = dateTime.addDays(new Date(), 1);
      const newState = {
        ...memo,
        memoDate: formatTime(tomorrow.toISOString()).date!
      };

      renderWrapper(initialState, newState);

      expect(
        screen.getByText(new RegExp(formatTime(tomorrow.toISOString()).date!))
      ).toBeInTheDocument();
    });
  });
});

describe('Actions', () => {
  describe('handleSelectAction', () => {
    it('handleDeleteMemo', () => {
      const mockAction = generateActions();
      spy = jest
        .spyOn(confirmModalActions, 'open')
        .mockImplementation(mockAction);

      renderWrapper(initialState);

      fireEvent.change(screen.getByTestId('MoreActions_onSelect'), {
        target: { value: 'undefined', label: 'label', uniqueId: 'delete' }
      });

      expect(mockAction).toBeCalledWith(
        expect.objectContaining({
          title: I18N_MEMOS.CONFIRM_DELETE,
          body: I18N_MEMOS.CONFIRM_DELETE_MESSAGE,
          confirmText: I18N_COMMON_TEXT.DELETE
        })
      );
    });

    it('handleConfirmDeleteMemo', () => {
      // trigger handleConfirmDeleteMemo()
      spy = jest.spyOn(confirmModalActions, 'open').mockImplementation(
        jest.fn().mockImplementation(({ confirmCallback }: any) => {
          confirmCallback();

          return { type: 'type' };
        })
      );

      const mockAction = generateActions();
      spy2 = jest
        .spyOn(deleteActions, 'triggerDeleteChronicleMemo')
        .mockImplementation(mockAction);

      renderWrapper(initialState);

      fireEvent.change(screen.getByTestId('MoreActions_onSelect'), {
        target: { value: 'undefined', label: 'label', uniqueId: 'delete' }
      });

      expect(mockAction).toBeCalledWith({
        storeId,
        postData: {
          memoIdentifier: memo.memoIdentifier,
          accountId: ''
        }
      });
    });

    it('handleEditMemo', () => {
      const mockAction = generateActions();
      spy = jest
        .spyOn(actionMemoChronicle, 'openEditChronicleMemo')
        .mockImplementation(mockAction);

      renderWrapper(initialState);

      fireEvent.change(screen.getByTestId('MoreActions_onSelect'), {
        target: { value: 'undefined', label: 'label', uniqueId: 'edit' }
      });

      expect(mockAction).toBeCalledWith({
        storeId,
        memoItem: { ...memo, memoType: null, memoGroup: null }
      });
    });
  });
});
