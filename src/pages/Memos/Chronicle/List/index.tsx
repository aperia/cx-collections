import React, {
  memo,
  useMemo,
  useState,
  useEffect,
  Fragment,
  useRef,
  useCallback
} from 'react';

// utils
import isArray from 'lodash.isarray';
import isEqual from 'lodash.isequal';

// component
import { Button } from 'app/_libraries/_dls/components';
import Item from './Item';
import Header from '../Header';
import LoadMore from 'app/components/LoadMore';
import NoItem from './NoItem';
import { MemoProps } from 'pages/Memos';

// redux
import {
  MemoChronicle,
  MemoChronicleConfig,
  MemoChronicleData
} from 'pages/Memos/Chronicle/types';

// selector
import { useDispatch } from 'react-redux';
import {
  selectChronicleMemo,
  selectLoading,
  selectLoadMore,
  selectEndLoadMore,
  selectConfigChronicleMemo,
  selectIsExpand
} from 'pages/Memos/Chronicle/_redux/selectors';
import {
  useAccountDetail,
  useCustomStoreIdSelector,
  useCustomStoreId
} from 'app/hooks';

// action
import { actionMemoChronicle } from 'pages/Memos/Chronicle/_redux/reducers';
import { buildMemoData } from '../helpers';
import { SEARCH_DEFAULT, GetListType, ITEM_PER_PAGE } from '../constants';

// i18n
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { I18N_MEMOS } from '../../constants';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

import { useApiErrorNotification } from 'pages/ApiErrorNotification/hooks/useApiErrorNotification';

export interface ListProps extends MemoProps {}

const List: React.FC<ListProps> = ({ shouldRenderPinnedIcon }) => {
  const headerRef = useRef<HTMLDivElement | null>(null);
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { customStoreId: storeId } = useCustomStoreId();
  const { accEValue } = useAccountDetail();

  const { search: searchParams = {}, sort } =
    useCustomStoreIdSelector<MemoChronicleConfig>(selectConfigChronicleMemo);
  const memos = useCustomStoreIdSelector<MemoChronicle[]>(selectChronicleMemo);
  const isLoading = useCustomStoreIdSelector<boolean>(selectLoading);
  const isLoadMore = useCustomStoreIdSelector<boolean>(selectLoadMore);
  const isEnd = useCustomStoreIdSelector<boolean>(selectEndLoadMore);
  const isExpand = useCustomStoreIdSelector<boolean>(selectIsExpand);

  const ApiErrorDetail = useApiErrorNotification({
    storeId,
    forSection: 'inMemoFlyOut'
  });

  const [filterMemo, setFilterMemo] = useState<MemoChronicle[]>(memos);

  // Handle change configs
  useEffect(() => {
    if (!isArray(memos)) return;
    // apply filter memo
    const searchParamsMapped = {
      ...searchParams,
      behaviorIdentifier: searchParams.memoType?.value,
      filterIdentifier: searchParams.memoGroup?.value,
      memoType: undefined,
      memoGroup: undefined
    };

    const filterData = buildMemoData(memos, {
      search: searchParamsMapped,
      sort
    });

    setFilterMemo(filterData);
  }, [memos, searchParams, sort]);

  // Handle for first loading
  useEffect(() => {
    dispatch(
      actionMemoChronicle.getMemoChronicle({
        storeId,
        postData: { type: GetListType.FIRST, accountId: accEValue }
      })
    );
    return () => {
      dispatch(actionMemoChronicle.emptyMemoList({ storeId }));
    };
  }, [accEValue, dispatch, storeId]);

  const renderItems = useMemo(() => {
    return (
      isArray(filterMemo) &&
      filterMemo.map((memo, idx) => (
        <Item
          id={`${memo.memoIdentifier}-memo-id`}
          dataTestId={`${memo.memoIdentifier}-memo-id`}
          isExpand={isExpand}
          memo={memo as MemoChronicleData}
          key={memo.memoIdentifier}
        />
      ))
    );
  }, [filterMemo, isExpand]);

  const handleLoadMore = () => {
    dispatch(
      actionMemoChronicle.getMemoChronicle({
        storeId,
        postData: {
          accountId: accEValue,
          type: GetListType.LOAD_MORE
        }
      })
    );
  };

  const handleResetSearch = useCallback(() => {
    const isReset = !!searchParams?.memoDate || !!searchParams?.memoDateRange;

    // get new list memo with memoDate
    if (isReset) {
      dispatch(
        actionMemoChronicle.getMemoChronicle({
          storeId,
          postData: {
            accountId: accEValue,
            type: GetListType.RESET_SEARCH
          }
        })
      );
    }

    dispatch(
      actionMemoChronicle.setConfigsFilter({
        storeId,
        params: {
          search: SEARCH_DEFAULT
        }
      })
    );
  }, [
    accEValue,
    dispatch,
    searchParams?.memoDate,
    searchParams?.memoDateRange,
    storeId
  ]);

  const totalItem = useMemo(() => filterMemo.length, [filterMemo.length]);

  return (
    <>
      <div ref={headerRef}>
        <Header shouldRenderPinnedIcon={shouldRenderPinnedIcon} />
      </div>
      <ApiErrorDetail
        className="ml-24 mb-24"
        dataTestId="chronicle-memo-list_error"
      />
      <div className="px-24 overlap-next-action">
        {totalItem > 0 && !isLoading && (
          <div className="d-flex justify-content-between align-items-center">
            <span
              id="totalItemMemoChronicle"
              data-testid={genAmtId(
                'chronicle-memo-list',
                'total-item',
                'MemosChronicle.List'
              )}
              className="color-grey-d20 text-decoration-none"
            >
              {t('txt_total_memo', { count: totalItem })}
            </span>
            {!isEqual(searchParams, SEARCH_DEFAULT) && (
              <Button
                id="chronicle-memo-list_reset-search-btn"
                size="sm"
                variant="outline-primary"
                className="mx-n8"
                onClick={handleResetSearch}
                dataTestId="chronicle-memo-list_reset-search-btn"
              >
                {t(I18N_COMMON_TEXT.CLEAR_AND_RESET)}
              </Button>
            )}
          </div>
        )}
        <div className="pt-8">
          {totalItem !== 0 && !isLoading ? (
            <Fragment>
              {renderItems}
              <LoadMore
                className="pb-16"
                loading={isLoadMore}
                onLoadMore={handleLoadMore}
                isEnd={isEnd}
                hideBackToTop={memos?.length < ITEM_PER_PAGE}
                loadingText={t(I18N_MEMOS.LOADING_MORE)}
                endLoadMoreText={t(I18N_MEMOS.END_OF_MEMOS)}
                dataTestId="chronicle-memo-list_load-more"
              />
            </Fragment>
          ) : (
            !isLoading && (
              <div className="d-flex justify-content-center mt-8">
                <NoItem
                  isApplySearch={!isEqual(searchParams, SEARCH_DEFAULT)}
                  onResetSearch={handleResetSearch}
                  dataTestId="chronicle-memo-list_no-item"
                />
              </div>
            )
          )}
        </div>
      </div>
    </>
  );
};

export default memo(List);
