import React from 'react';
import { screen } from '@testing-library/dom';
import NoItem from './NoItem';
import { I18N_MEMOS } from 'pages/Memos/constants';
import { NoResultIconProps } from 'app/components/NoResultIcon';
import { render } from '@testing-library/react';

jest.mock(
  'app/components/NoResultIcon',
  () => ({ onReset, description }: NoResultIconProps) => (
    <>
      <button data-testid="NoResultIcon_onReset" onClick={onReset} />
      {description}
    </>
  )
);

jest.mock('app/_libraries/_dls/hooks', () => {
  const hooks = jest.requireActual('app/_libraries/_dls/hooks');

  return {
    ...hooks,
    useTranslation: () => ({ t: (text: string) => text })
  };
});

describe('Render', () => {
  it('Render UI', async () => {
    render(<NoItem isApplySearch={false} />);

    expect(screen.getByText(I18N_MEMOS.NO_RESULTS_DISPLAY)).toBeInTheDocument();
  });

  it('Render UI => applySearch', async () => {
    const mockAction = jest.fn();

    render(<NoItem isApplySearch={true} onResetSearch={mockAction} />);

    // trigger onReset()
    screen.getByTestId('NoResultIcon_onReset').click();

    expect(screen.getByText(I18N_MEMOS.NO_RESULTS_FOUND)).toBeInTheDocument();
    expect(mockAction).toBeCalled();
  });
});
