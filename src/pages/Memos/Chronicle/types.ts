import { GetListType } from 'pages/Memos/Chronicle/constants';

export interface MemoChronicleGroup {
  value: string;
  description: string;
}

export interface MemoChronicleState {
  [storeId: string]: {
    refData?: {
      typeList?: MemoChronicleType[];
      groupList?: MemoChronicleGroup[];
      loading?: boolean;
      error?: boolean;
    };
    addMemoChronicle?: {
      loading?: boolean;
      error?: boolean;
    };
    editMemoChronicle?: {
      loading?: boolean;
      error?: boolean;
      open?: boolean;
      memoItem?: MemoChronicleData;
    };
    deleteChronicleMemo?: {
      loading?: boolean;
      error?: boolean;
    };
    isExpand?: boolean;
    loading?: boolean;
    isLoadMore?: boolean;
    isEnd?: boolean;
    memos?: MemoChronicleData[];
    configs?: MemoChronicleConfig;
    selectedItem?: MemoChronicleData;
  };
}

export enum MemoChronicleAction {
  ADD_MEMO_CHRONICLE = 'addMemoChronicle',
  EDIT_MEMO_CHRONICLE = 'editMemoChronicle',
  GET_CHRONICLE_MEMO = 'getChronicleMemos',
  DELETE_MEMO_CHRONICLE = 'deleteMemoChronicle',
  GET_CHRONICLE_BEHAVIORS = 'getChronicleBehaviors',
  GET_CHRONICLE_FILTERS = 'getChronicleFilter'
}

export interface MemoChronicle {
  behaviorIdentifier: string;
  filterIdentifier: string;
  memoOperatorIdentifier: string;
  memoType: MemoChronicleType | null;
  memoGroup: MemoChronicleGroup | null;
  memoText: string;
  memoDate: string | null;
  memoTime: string;
  memoIdentifier: string;
  dateType?: {
    dateRange?: boolean;
    specificDate?: boolean;
  };
  memoDateRange?: {
    start?: string;
    end?: string;
  } | null;
  startDate?: string;
  endDate?: string;
  searchBy?: {
    group?: boolean;
    type?: boolean;
  };
}

export interface MemoChronicleData {
  memoOperatorIdentifier: string;
  memoText: string;
  memoDate: string | null;
  memoTime: string;
  memoIdentifier: string;
  behaviorIdentifier: string;
  filterIdentifier: string;
  memoDeletionDate?: string;
}

export interface MemoChronicleSort {
  field: MemoChronicleSortField;
  type: MemoChronicleSortType;
}

export interface MemoChronicleConfig {
  start?: number;
  end?: number;
  sort?: MemoChronicleSort;
  search?: Partial<MemoChronicle>;
}

// Arguments
export interface GetMemoChroniclePostData {
  end?: number;
  type: GetListType;
  memoDate?: string | null;
  startDate?: string | null;
  endDate?: string | null;
  accountId: string;
}
export interface GetMemoChronicleActionArgs {
  storeId: string;
  postData: GetMemoChroniclePostData;
}

export interface MemoChronicleArg {
  storeId?: string;
  postData?: {};
}

export interface GetMemoChronicleBehaviorsArgs {
  storeId: string;
  postData: { accountId: string };
}

export interface GetMemoChronicleFiltersArgs {
  storeId: string;
  postData: { accountId: string };
}

// Payload
export interface GetMemoChroniclePayload {
  memos: MemoChronicleData[];
  configs: MemoChronicleConfig;
}
export interface MemoChroniclePayload {
  data?: unknown;
}
export interface ExpandPayload {
  storeId: string;
  isExpand: boolean;
}

export interface EditMemoChroniclePayload extends StoreIdPayload {
  memoItem?: MemoChronicle;
}

export interface MemoChronicleConfigPayload {
  storeId: string;
  params: MemoChronicleConfig;
}

// enum
export enum MemoChronicleSortField {
  DATE_TIME = 'dateTime'
}

export enum MemoChronicleSortType {
  ASC = 'asc',
  DESC = 'desc'
}

export interface AddChronicleMemoRequestData {
  memoType?: MemoChronicleType;
  memoText?: string;
  overrideRetentionDate?: Date;
  accountId: string;
}
export interface AddMemoChronicleArgs {
  storeId: string;
  data?: {
    chronicleKey?: string;
    cspa: CSPA;
    actionCode?: string;
    actionType?: string
  };
  postData: AddChronicleMemoRequestData;
  onSuccess?: Function;
  onFailure?: Function;
  token?: string;
}

export interface CSPA {
  clientNumber?: string;
  system?: string;
  prin?: string;
  agen?: string;
}

export interface EditChronicleMemoRequestData {
  memoType: MemoChronicleType;
  memoText: string;
  overrideRetentionDate?: Date;
  memoIdentifier: string;
  accountId: string;
}

export interface EditMemoChronicleArgs {
  accountId: string;
  storeId: string;
  postData: EditChronicleMemoRequestData;
  onSuccess?: Function;
  onFailure?: Function;
}

export interface DeleteChronicleMemoRequestData {
  memoIdentifier: string;
  accountId: string;
}

export interface DeleteChronicleMemoRequestArgs {
  storeId: string;
  postData: DeleteChronicleMemoRequestData;
}

export interface GetMemoChronicleMemoRequestData {
  accountId: string;
  startSequence: string;
  endSequence: string;
  memoDate?: string;
  startDate?: string;
  endDate?: string;
}

export interface GetMemoChronicleServiceArgs {
  storeId: string;
  postData: GetMemoChronicleMemoRequestData;
}

export interface ReferenceData {
  value?: string;
  description?: string;
}

export interface MemoChronicleType {
  value: string;
  description: string;
}
