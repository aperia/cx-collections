import { MEMO_TEXT_REGEX } from 'app/constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useEffect } from 'react';
import { I18N_MEMOS } from '../constants';

export const useCheckMemoText = (
  viewRef: any,
  memoText: string,
  isOpen: boolean
) => {
  const { t } = useTranslation();

  useEffect(() => {
    if (!isOpen) return;
    const immediateId = setImmediate(() => {
      const alertMessageField = viewRef.current?.props.onFind('alertMsgMemo');
      const isTextValid = new RegExp(MEMO_TEXT_REGEX).test(memoText);
      if (isTextValid) {
        alertMessageField?.props?.props.setValue('');
      } else {
        alertMessageField?.props?.props.setValue(
          t(I18N_MEMOS.WARNING_TEXT_MESSAGE)
        );
      }
    });

    return () => {
      clearImmediate(immediateId);
    };
  }, [viewRef, memoText, isOpen, t]);
};
