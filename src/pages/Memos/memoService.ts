import { apiService } from 'app/utils/api.service';

import { GetMemoConfigProps } from './types';

const memoService = {
  getMemoConfig(requestData: GetMemoConfigProps, token?: string) {
    const url = window.appConfig?.api?.memo?.getMemoConfig || '';
    return apiService.post(url, requestData, {
      headers: { Authorization: token }
    });
  }
};

export default memoService;
