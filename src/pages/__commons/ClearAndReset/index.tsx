import React from 'react';

// components
import { Button, Icon } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import classNames from 'classnames';
import { genAmtId } from 'app/_libraries/_dls/utils';

const ClearAndReset: React.FC<{
  onClearAndReset: () => void;
  className?: string;
  testId?: string;
}> = ({ onClearAndReset, className, testId = '' }) => {
  const { t } = useTranslation();

  return (
    <div className={classNames(className, 'text-center')}>
      <Icon name="file" className="fs-80 color-light-l12" dataTestId={testId} />
      <p
        className="mt-16 mb-24 color-grey"
        data-testid={genAmtId(testId, 'not-found', '')}
      >
        {t('txt_memos_no_results_found')}
      </p>
      <Button
        size="sm"
        onClick={onClearAndReset}
        variant="outline-primary"
        dataTestId={`${testId}_clear-and-reset-btn`}
      >
        {t('txt_clear_and_reset')}
      </Button>
    </div>
  );
};

export default ClearAndReset;
