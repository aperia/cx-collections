import React from 'react';
import { screen } from '@testing-library/react';
import { queryByClass, renderMockStore } from 'app/test-utils';
import ClearAndReset from './';

describe('Test ClearAndReset', () => {
  it('Should have render and click clear', () => {
    const onClearAndReset = jest.fn();
    renderMockStore(<ClearAndReset onClearAndReset={onClearAndReset} />);
    const queryTextClearReset = screen.queryByText('txt_clear_and_reset');
    const queryNoFound = screen.queryByText('txt_memos_no_results_found');
    queryTextClearReset!.click();
    expect(queryNoFound).toBeInTheDocument();
    expect(onClearAndReset).toHaveBeenCalled();
  });

  it('Should have className', () => {
    const onClearAndReset = jest.fn();
    const { container } = renderMockStore(
      <ClearAndReset className="className" onClearAndReset={onClearAndReset} />
    );
    const queryClass = queryByClass(container, /className/);
    expect(queryClass).toBeInTheDocument();
  });
});
