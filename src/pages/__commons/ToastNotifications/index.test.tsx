import {
  mockActionCreator,
  queryByClass,
  renderMockStoreId
} from 'app/test-utils';
import React from 'react';
import ToastNotifications from './index';
import { actionsToast } from './_redux/index';
HTMLCanvasElement.prototype.getContext = jest.fn();

const mockActionToast = mockActionCreator(actionsToast);
describe('ToastNotifications component', () => {
  it('should have render and click close', () => {
    const spyRemoveToast = mockActionToast('removeToast');
    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(<ToastNotifications />, {
      initialState: {
        toastNotifications: {
          notifications: [
            {
              id: 'testId',
              show: true,
              type: 'success',
              message: 'test message'
            }
          ]
        }
      }
    });

    const queryIconClose = queryByClass(
      wrapper.baseElement as HTMLElement,
      /icon-close/
    );

    queryIconClose!.click();
    jest.runAllTimers();
    expect(spyRemoveToast).toHaveBeenCalledWith({ id: 'testId' });
  });

  it('should have render and click close > message start with txt', () => {
    const spyRemoveToast = mockActionToast('removeToast');
    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(<ToastNotifications />, {
      initialState: {
        toastNotifications: {
          notifications: [
            {
              id: 'testId',
              show: true,
              type: 'success',
              message: 'txt_test message'
            }
          ]
        }
      }
    });

    const queryIconClose = queryByClass(
      wrapper.baseElement as HTMLElement,
      /icon-close/
    );

    queryIconClose!.click();
    jest.runAllTimers();
    expect(spyRemoveToast).toHaveBeenCalledWith({ id: 'testId' });
  })
});
