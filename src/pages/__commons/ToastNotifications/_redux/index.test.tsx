import { actionsToast, reducer, ToastState } from './index';

const initReducer = {
  notifications: [
    {
      id: '123',
      show: true,
      type: 'attention',
      message: 'a message for test'
    },
    {
      id: '1234',
      show: false,
      type: 'success',
      message: 'a message for test one'
    },
    {
      id: '12345',
      show: true,
      type: 'warning',
      message: 'a message for test two'
    },
    {
      id: '123456',
      show: true,
      type: 'errors',
      message: 'a message for test three'
    }
  ]
} as ToastState;

describe('Tab redux-store', () => {
  it('should add new toast to store', () => {
    const message = 'a message for test four';
    const nextState = reducer(
      initReducer,
      actionsToast.addToast({
        id: '1234567',
        show: true,
        type: 'attention',
        message
      })
    );
    const nextStateNotifications = nextState.notifications?.find(
      notification => notification.message === message
    );

    expect(nextState.notifications.length).toEqual(5);
    expect(nextStateNotifications).toBeDefined();
  });

  it('should remove a toast to store', () => {
    const nextState = reducer(
      initReducer,
      actionsToast.removeToast({ id: '123' })
    );

    expect(nextState.notifications.length).toEqual(3);
  });

  it('should remove all toast to store', () => {
    const nextState = reducer(initReducer, actionsToast.clearAllToast());
    expect(nextState.notifications.length).toEqual(0);
  });
});
