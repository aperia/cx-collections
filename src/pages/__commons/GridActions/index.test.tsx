import React from 'react';
import GridActions from './index';
import { screen } from '@testing-library/react';
import { renderMockStoreId } from 'app/test-utils';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

describe('Test GridActions', () => {
  it('Should have render to UI', () => {
    renderMockStoreId(<GridActions />);
    const queryTextDelete = screen.queryByText(I18N_COMMON_TEXT.DELETE);
    const queryTextEdit = screen.queryByText(I18N_COMMON_TEXT.EDIT);
    expect(queryTextEdit).toBeInTheDocument();
    expect(queryTextDelete).toBeInTheDocument();
  });

  it('Should have click', () => {
    const onClickDelete = jest.fn();
    const onClickEdit = jest.fn();

    renderMockStoreId(
      <GridActions onClickDelete={onClickDelete} onClickEdit={onClickEdit} />
    );
    const queryTextDelete = screen.queryByText(I18N_COMMON_TEXT.DELETE);
    const queryTextEdit = screen.queryByText(I18N_COMMON_TEXT.EDIT);

    queryTextDelete!.click();
    queryTextEdit!.click();

    expect(onClickDelete).toHaveBeenCalled();
    expect(onClickEdit).toHaveBeenCalled();
  });
});
