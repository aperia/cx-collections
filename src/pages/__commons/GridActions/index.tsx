import React, { Fragment } from 'react';

import { Button } from 'app/_libraries/_dls/components';

import { useTranslation } from 'app/_libraries/_dls/hooks';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

export interface GridActionsProps {
  status?: boolean;
  onClickDelete?: () => void;
  onClickEdit?: () => void;
  dataTestId?: string;
}

const GridActions: React.FC<GridActionsProps> = ({
  onClickDelete,
  onClickEdit,
  status = false,
  dataTestId
}) => {
  const { t } = useTranslation();

  return (
    <Fragment>
      <Button
        disabled={status}
        onClick={onClickDelete}
        size="sm"
        variant="outline-danger"
        dataTestId={`${dataTestId}-delete`}
      >
        {t(I18N_COMMON_TEXT.DELETE)}
      </Button>
      <Button
        disabled={status}
        onClick={onClickEdit}
        size="sm"
        variant="outline-primary"
        dataTestId={`${dataTestId}-edit`}
      >
        {t(I18N_COMMON_TEXT.EDIT)}
      </Button>
    </Fragment>
  );
};

export default GridActions;
