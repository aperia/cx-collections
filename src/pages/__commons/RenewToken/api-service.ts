import { apiService } from 'app/utils/api.service';

class CommonService {
  getAccessToken(postData: AccessTokenPayload) {
    const url = window.appConfig?.api?.common?.accessToken || '';

    return apiService.post(url, postData);
  }
  getRenewToken(postData: RenewTokenPayload) {
    const url = window.appConfig?.api?.common?.renewToken || '';

    return apiService.post(url, postData);
  }
}

export const commonService = new CommonService();
