import { storeId } from 'app/test-utils';
import get from 'lodash.get';
import * as helpers from './helpers';

const mockJWT = { [storeId]: 2 };

describe('Test Helper', () => {
  it('Should call setTokenTimeout return setTimeout id', () => {
    const expirationTime = 100;

    jest.useFakeTimers();

    const timeoutId = helpers.setTokenTimeout(storeId, expirationTime);

    jest.runAllTimers();
    expect(timeoutId).not.toBeNull();
    expect(setTimeout).toHaveBeenLastCalledWith(
      expect.any(Function),
      expirationTime
    );
  });

  describe('Should call setLastedRenewToken', () => {
    it('with store id has not appeared yet', () => {
      const time = 940000 - Date.now();

      const mockValue = JSON.stringify({
        [storeId]: {
          token: 'mockToken',
          time
        }
      });

      jest.spyOn({ get }, 'get').mockReturnValue(mockValue);
      const mockRemove = jest
        .spyOn(helpers, 'removeSetTimeoutBasedStoreId')
        .mockImplementation();

      helpers.setLastedRenewToken(storeId, 1000, false);

      expect(window.jwtTimes![storeId]).not.toEqual(2);
      mockRemove.mockClear();
      mockRemove.mockRestore();
    });

    it('and update token with store id has not appeared yet', () => {
      const time = 940000 - Date.now();

      const mockValue = JSON.stringify({
        [storeId]: {
          token: 'mockToken',
          time
        }
      });

      jest.spyOn({ get }, 'get').mockReturnValue(mockValue);
      const mockRemove = jest
        .spyOn(helpers, 'removeSetTimeoutBasedStoreId')
        .mockImplementation();

      helpers.setLastedRenewToken(storeId, 0, true);

      expect(window.jwtTimes![storeId]).not.toEqual(2);
      mockRemove.mockClear();
      mockRemove.mockRestore();
    });

    it('and update token with exists store id', () => {
      window.jwtTimes = {
        ...window.jwtTimes,
        [storeId]: setTimeout(() => {})
      };
      const time = 940000 - Date.now();

      const mockValue = JSON.stringify({
        [storeId]: {
          token: 'mockToken',
          time
        }
      });

      jest.spyOn({ get }, 'get').mockReturnValue(mockValue);
      const mockRemove = jest
        .spyOn(helpers, 'removeSetTimeoutBasedStoreId')
        .mockImplementation();

      helpers.setLastedRenewToken(storeId, 1000, true);

      expect(window.jwtTimes![storeId]).not.toEqual(2);
      mockRemove.mockClear();
      mockRemove.mockRestore();
    });
  });

  describe('Should call isNotTabAccDetail', () => {
    it('with store id undefined', () => {
      const value = helpers.isNotTabAccDetail([
        {
          storeId,
          title: '',
          tabType: 'accountDetail',
          accEValue: storeId
        }
      ]);

      expect(value).toEqual(true);
    });

    it('with store id is account detail', () => {
      const value = helpers.isNotTabAccDetail(
        [
          {
            storeId,
            title: '',
            accEValue: storeId,
            tabType: 'accountDetail'
          }
        ],
        storeId
      );

      expect(value).toEqual(false);
    });
  });

  describe('Should call removeAllSetTimeout', () => {
    it('with exist jwtTimes', () => {
      window.jwtTimes = {
        [storeId]: 10,
        undefinedId: undefined
      } as any;

      helpers.removeAllSetTimeout();

      expect(window.jwtTimes).toEqual(undefined);
    });

    it('with jwtTimes has not appeared yet', () => {
      helpers.removeAllSetTimeout();

      expect(window.jwtTimes).toEqual(undefined);
    });
  });

  describe('Should call removeSetTimeoutBasedStoreId', () => {
    it('with jwtTimes is not existed', () => {
      delete window.jwtTimes;
      helpers.removeSetTimeoutBasedStoreId('');
    });

    it('with jwtTimes has not appeared yet', () => {
      Object.defineProperty(window, 'jwtTimes', {
        value: mockJWT
      });
      helpers.removeSetTimeoutBasedStoreId('');
    });
  });
});
