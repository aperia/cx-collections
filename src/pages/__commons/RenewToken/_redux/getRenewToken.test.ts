import { FULFILLED, PENDING, REJECTED } from 'app/constants';
import { mockAsyncThunk, storeId } from 'app/test-utils';
import { mockBuilder } from 'app/test-utils/mockBuilder';
import { commonService } from '../api-service';
import { RenewTokenArgs } from '../types';
import { getRenewToken } from './getRenewToken';

const mockJWT = 'header.body.signature';
const response = {};

const initialState = {
  renewToken: {}
};

const args = {
  payload: {
    jwt: mockJWT,
    common: {
      applicationId: '',
      accountId: ''
    }
  }
} as RenewTokenArgs;

const setupGetRenewToken = mockAsyncThunk({
  service: commonService,
  fn: getRenewToken
})({ args });

const setupGetRenewTokenBuilder = mockBuilder(getRenewToken, args);

const getRenewTokenCases = [
  {
    caseName: 'getRenewToken > fulfilled with exists inProgressInfo',
    actionState: FULFILLED,
    expected: {
      type: getRenewToken.fulfilled.type,
      data: response
    }
  },
  {
    caseName: 'getRenewToken > fulfilled with no inProgressInfo',
    actionState: FULFILLED,
    expected: {
      type: getRenewToken.fulfilled.type,
      data: response
    }
  },
  {
    caseName: 'getRenewToken > rejected from axios',
    actionState: REJECTED,
    expected: {
      type: getRenewToken.rejected.type,
      data: undefined
    }
  },
  {
    caseName: 'getRenewToken > rejected from condition',
    actionState: REJECTED,
    isLoading: true,
    expected: {
      type: getRenewToken.rejected.type,
      data: undefined
    }
  }
];

describe.each(getRenewTokenCases)(
  'getRenewToken',
  ({ caseName, actionState, expected }) => {
    it(caseName, async () => {
      const { result } = await setupGetRenewToken({
        actionState,
        payload: response,
        initialState
      });

      // Assert
      const { type, payload } = result;
      expect(type).toEqual(expected.type);
      expect(payload).toEqual(expected.data);
    });
  }
);

describe('getRenewTokenBuilder', () => {
  it.each`
    actionState  | expected
    ${PENDING}   | ${{ isLoading: true }}
    ${FULFILLED} | ${{ isLoading: false }}
    ${REJECTED}  | ${{ isLoading: false }}
  `('$actionState', ({ actionState, expected }) => {
    const { actual } = setupGetRenewTokenBuilder(actionState, {
      ...initialState,
      renewToken: {
        [storeId]: {
          ...[storeId],
          isLoading: expected.isLoading || false
        }
      }
    });

    // Then
    expect(actual?.renewToken?.[storeId]).toEqual(
      expect.objectContaining(expected)
    );
  });
});
