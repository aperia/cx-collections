import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { parseJwt } from 'app/helpers';

// Service
import { commonService } from '../api-service';

// Helper
import { setLastedRenewToken } from '../helpers';

// Types
import {
  RenewTokenArgs,
  RenewTokenResult,
  TriggerRenewTokenPayload
} from '../types';

export const getRenewToken = createAsyncThunk<
  TriggerRenewTokenPayload,
  RenewTokenArgs,
  ThunkAPIConfig
>('common/getRenewToken', async (args, thunkAPI) => {
  const { payload } = args;

  const { data } = await commonService.getRenewToken(payload);

  return data;
});

export const getRenewTokenBuilder = (
  builder: ActionReducerMapBuilder<RenewTokenResult>
) => {
  builder.addCase(getRenewToken.pending, (draftState, action) => {
    const { storeId } = action.meta.arg;

    return {
      ...draftState,
      [storeId]: {
        isLoading: true
      }
    };
  });

  builder.addCase(getRenewToken.fulfilled, (draftState, action) => {
    const { storeId } = action.meta.arg;
    const { jwt } = action.payload;

    const isUpdateToken = Object.keys(draftState).includes(storeId);

    const { exp = 0 } = parseJwt(jwt) || {};
    const timeExpired = Math.abs(
      (new Date(exp * 1000).getMinutes() - new Date().getMinutes()) / 2
    );

    setLastedRenewToken(storeId, timeExpired, isUpdateToken);

    return {
      ...draftState,
      [storeId]: {
        token: jwt,
        isLoading: false,
        isLoadingRelated: false
      }
    };
  });

  builder.addCase(getRenewToken.rejected, (draftState, action) => {
    const { storeId } = action.meta.arg;

    return {
      ...draftState,
      [storeId]: {
        isLoading: false,
        isLoadingRelated: false
      }
    };
  });
};
