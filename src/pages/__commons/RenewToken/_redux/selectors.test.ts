import { USER_TOKEN_ID } from 'app/constants';
import { selectorWrapper } from 'app/test-utils';
import { takeUserToken } from './selectors';

describe('should test renew token selectors', () => {
  const initialState: Partial<RootState> = {
    renewToken: {
      [USER_TOKEN_ID]: {
        token: 'token'
      }
    }
  };

  const selectorTrigger = selectorWrapper(initialState);

  it('get token', () => {
    const { data, emptyData } = selectorTrigger(takeUserToken);

    expect(data).toEqual('token');
    expect(emptyData).toBeUndefined();
  });
});
