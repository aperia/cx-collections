import { renewToken, renewTokenActions } from './reducer';
import { storeId } from 'app/test-utils';
import { RenewTokenResult } from '../types';

const { removeRenewTokenBasedOnAccId } = renewTokenActions;

const initialState: RenewTokenResult = { [storeId]: {} };

describe('RenewToken reducer', () => {
  it('should run removeRenewTokenBasedOnAccId action', () => {
    const params = { storeId };
    const state = renewToken(
      initialState,
      removeRenewTokenBasedOnAccId(params)
    );
    expect(state[storeId]).toEqual({});
  });

  it('should run removeRenewTokenBasedOnAccId with empty storeId', () => {
    const params = { storeId: '' };
    const state = renewToken(
      initialState,
      removeRenewTokenBasedOnAccId(params)
    );
    expect(state[storeId]).toEqual({});
  });
});
