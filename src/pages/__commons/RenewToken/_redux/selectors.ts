import { USER_TOKEN_ID } from 'app/constants';
import { createSelector } from 'reselect';

const getToken = (state: RootState) => {
  return state.renewToken[USER_TOKEN_ID]?.token;
};

export const takeUserToken = createSelector(getToken, data => data);
