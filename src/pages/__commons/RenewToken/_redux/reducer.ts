import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Helper
import { removeSetTimeoutBasedStoreId } from '../helpers';

// Type
import { RenewTokenResult } from '../types';

// Redux
import { getRenewToken, getRenewTokenBuilder } from './getRenewToken';
import { triggerExpiredToken } from './triggerGetRenewToken';

const initialState: RenewTokenResult = {};
const { reducer, actions } = createSlice({
  initialState,
  name: 'renewToken',
  reducers: {
    removeRenewTokenBasedOnAccId: (
      draftState,
      action: PayloadAction<StoreIdPayload>
    ) => {
      const { storeId } = action.payload;
      removeSetTimeoutBasedStoreId(storeId);
      const listToken: RenewTokenResult = {};

      Object.keys(draftState).forEach(item => {
        if (item !== storeId) {
          listToken[item] = draftState[item];
        }
      });

      return {
        ...draftState,
        ...listToken
      };
    }
  },
  extraReducers: builder => {
    getRenewTokenBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getRenewToken,
  triggerExpiredToken
};

export { allActions as renewTokenActions, reducer as renewToken };
