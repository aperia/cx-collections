import { createAsyncThunk } from '@reduxjs/toolkit';

import { EMPTY_STRING, USER_TOKEN_ID } from 'app/constants';

// Types
import { ExpireTokenArgs } from '../types';
// Helpers
import { getDefaultTokenBasedOnHostName } from 'app/helpers/setupInterceptor';

// Action
import { renewTokenActions } from './reducer';

export const triggerExpiredToken = createAsyncThunk<
  unknown,
  ExpireTokenArgs,
  ThunkAPIConfig
>('common/triggerExpiredToken', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;
  const { tab, renewToken } = getState();
  const { storeId } = args;
  const { commonConfig } = window.appConfig || {};

  const tabExpire = tab.tabs.find(item => item.storeId === storeId);

  if (tabExpire?.tabType !== 'accountDetail') {
    await dispatch(
      renewTokenActions.getRenewToken({
        storeId: USER_TOKEN_ID,
        payload: {
          jwt: renewToken[USER_TOKEN_ID]?.token || EMPTY_STRING
        }
      })
    );
  } else {
    const token = renewToken
      ? renewToken[tabExpire?.storeId].token
      : EMPTY_STRING;

    const defaultToken = dispatch(getDefaultTokenBasedOnHostName());

    await dispatch(
      renewTokenActions.getRenewToken({
        storeId: tabExpire?.storeId,
        payload: {
          jwt: token || defaultToken,
          common: {
            applicationId: commonConfig?.app || EMPTY_STRING,
            accountId: tabExpire?.accEValue || EMPTY_STRING
          }
        }
      })
    );
  }
});
