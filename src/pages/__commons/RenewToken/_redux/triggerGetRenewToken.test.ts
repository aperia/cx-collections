import { createStore, Store } from 'redux';

// actions
import { renewTokenActions } from './reducer';

// Helper
import { mockActionCreator, storeId } from 'app/test-utils';
import * as interceptor from 'app/helpers/setupInterceptor';

// Utils
import { rootReducer } from 'storeConfig';

const spyRenewTokenActions = mockActionCreator(renewTokenActions);
const spyInterceptor = mockActionCreator(interceptor);

describe('Test triggerGetRenewToken', () => {
  const mockJWT =
    'eyJhbGciOiJIUzI1NiJ9.eyJlc3NVc2VyTmFtZSI6IlRlc3QgTmFtZSIsImV4cCI6MTYxNjEyNzY4MSwiYXBwSWQiOiJjeENvbGxlY3Rpb25zIiwib3BlcmF0b3JJZCI6IkZERVNNQVNUIiwiT3JnSUQiOiJGREVTIiwidXNlcklkIjoiRkRFU01BU1QiLCJuYmYiOjE2MTYxMjY3ODEsImlzcyI6Ik5TQSIsIm9jc1NoZWxsIjoiRkRFU01BU1QiLCJwcml2aWxlZ2VzIjpbIkNYQ09MMSIsIkNYQ09MNSJdLCJ1c2VyTmFtZSI6InRlc3RuYW1lIiwieDUwMElkIjoiQUFBQTExMTQiLCJlbnRpdGxlbWVudCI6W10sImdyb3VwcyI6WyJITE5TQSJdLCJzZXNzaW9uIjoiOGIwZjA4NGItMzcxNC00MTFlLWFjNmYtZTQ5ZWFjNDg0NzBkIiwiY2xpZW50SWRlbnRpZmllciI6IjUzMDIiLCJzeXN0ZW1JZGVudGlmaWVyIjoiNjM5NSIsInByaW5jaXBhbElkZW50aWZpZXIiOiIwMDAwIiwiYWdlbnRJZGVudGlmaWVyIjoiMDkwMCIsImFjY291bnRJZCI6IkZTRU5DKGV5SmpiMjVtYVdjaU9pSm1jeTV3WW1VdWEyVjVMakV1VUVKRlYwbFVTRk5JUVRJMU5rRk9SREkxTmtKSlZFRkZVeTFEUWtNdFFrTWlMQ0prWVhSaElqb2lSVTVES0Zkb1FXZHJORzlLV0dVclREQTJXVzR5TVdGRU5IVkhSelJVTmxoWWJsSkxiMUJyUzJneFUxUXZjbU42WW05VU9UbENTMDh6ZFdWNlVuVTNlbE50TTBJcEluMD0pIiwicGlpZCI6IlZPTChZVlpHV2tGdktXOUpNaTlTVTFWV2VDcFhMUT09KSIsImN1c3RvbWVyRXh0ZXJuYWxJZGVudGlmaWVyIjoiQzIwMjMyMDczMjU2NzUwMDAwOTM4NTQ4IiwicHJlc2VudGF0aW9uSWQiOiJGU0VOQyhleUpqYjI1bWFXY2lPaUptY3k1d1ltVXVhMlY1TGpFdVVFSkZWMGxVU0ZOSVFUSTFOa0ZPUkRJMU5rSkpWRUZGVXkxRFFrTXRRa01pTENKa1lYUmhJam9pUlU1REtETkxSbEowY1hGbGMwOUNXa3hqT0ZWeWMyUjFVbTVUYTNKSVJXSXdObWhxU1RWUGNHUXZSVzhyVXpGeVdqQkxRbWR4TWpScFlqbHlOM2xDZFdSdWMyd3BJbjA9KSIsImNhcmRUeXBlIjoiY29tbWVyY2lhbCIsIm1lbW9UeXBlIjoiQ2hyb25pY2xlIiwianRpIjoiYjc1ZWEzMjItMTI5Mi00YTg1LTllYTMtMjIxMjkyOGE4NTQxIn0.BlJcYCeaYEqvjqL31PdL8-1G1wTf49tmCEdsdtYiBtM';

  const initState: Partial<RootState> = {
    tab: {
      accEValueSelected: storeId,
      tabStoreIdSelected: storeId,
      tabs: [
        {
          title: '',
          storeId,
          tabType: 'accountDetail'
        }
      ]
    },
    renewToken: {
      [storeId]: {
        isLoading: false,
        token: mockJWT
      }
    }
  };

  const store: Store<RootState> = createStore(rootReducer, initState);

  describe('Test triggerExpiredToken', () => {
    it('Should not setup interceptor when setupAxiosInterceptors is undefined', async () => {
      const mockGetRenewToken = spyRenewTokenActions('getRenewToken');
      const mockGetDefaultToken = spyInterceptor(
        'getDefaultTokenBasedOnHostName'
      );

      const response = await renewTokenActions.triggerExpiredToken({
        storeId: storeId
      })(store.dispatch, store.getState, {});

      expect(response.payload).toBe(undefined);

      expect(mockGetRenewToken).toBeCalled();
      expect(mockGetDefaultToken).toBeCalled();
    });

    it('Should not setup interceptor when renewToken is undefined', async () => {
      spyRenewTokenActions('getRenewToken');
      spyInterceptor('getDefaultTokenBasedOnHostName');

      const response = await renewTokenActions.triggerExpiredToken({
        storeId: storeId
      })(store.dispatch, () => ({ tab: initState.tab }), {});

      expect(response.payload).toBe(undefined);
    });

    it('Should not setup interceptor when tab type is not account detail ', async () => {
      const customStore: Store<RootState> = createStore(rootReducer, {
        tab: {
          accEValueSelected: storeId,
          tabStoreIdSelected: storeId,
          tabs: [
            {
              accEValue: storeId,
              storeId: storeId,
              tabType: 'accountSearchList'
            }
          ]
        }
      });

      const response = await renewTokenActions.triggerExpiredToken({
        storeId: storeId
      })(store.dispatch, customStore.getState, {});

      expect(response.payload).toBe(undefined);
    });
  });
});
