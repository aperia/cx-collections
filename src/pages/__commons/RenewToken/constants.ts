export enum RENEW_TOKEN_CONFIG {
  RENEW_TOKEN = 'renewTK',
  TOKEN_EXPIRED = 'JWTTokenExpired'
}

export const millisecond = 1000;

export const defaultExpTime = 2;
export const defaultInactiveTime = 1;
