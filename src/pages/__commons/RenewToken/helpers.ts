import isUndefined from 'lodash.isundefined';

// Constant
import { defaultExpTime, millisecond, RENEW_TOKEN_CONFIG } from './constants';

// Types
import { ExpireToken } from './types';
import { ItemTab } from '../TabBar/types';

export const setTokenTimeout = (storeId: string, expirationTime: number) =>
  setTimeout(() => {
    const event = new CustomEvent<ExpireToken>(
      RENEW_TOKEN_CONFIG.TOKEN_EXPIRED,
      {
        bubbles: true,
        cancelable: true,
        detail: {
          storeId: storeId
        }
      }
    );

    window.dispatchEvent(event);
  }, expirationTime);

export const setLastedRenewToken = (
  storeId: string,
  expTime: number,
  isUpdateToken: boolean
) => {
  // expTime will be configuration on public.json
  const timeRemaining = defaultExpTime * 60 * millisecond;

  /**
   * If there is exist stored token then remove timeout & register another one
   * else if add new jwt token
   */
  if (isUpdateToken) removeSetTimeoutBasedStoreId(storeId);
  window.jwtTimes = {
    ...window.jwtTimes,
    [storeId]: setTokenTimeout(storeId, timeRemaining)
  };
};

export const isNotTabAccDetail = (tabs: Array<ItemTab>, storeId?: string) => {
  if (isUndefined(storeId)) return true;

  const tabData = tabs.find(item => item.storeId === storeId);

  return tabData?.tabType !== 'accountDetail';
};

export const removeAllSetTimeout = () => {
  const { jwtTimes = {} } = window;
  Object.keys(jwtTimes).forEach(item => {
    const timeout = (jwtTimes[item] || 0) as number;
    clearTimeout(timeout);
  });

  window.jwtTimes = undefined;
};

export const removeSetTimeoutBasedStoreId = (storeId: string) => {
  const { jwtTimes = {} } = window;

  // Clear timeout with current stored timeout
  const timeout = (jwtTimes[storeId] || 0) as number;

  clearTimeout(timeout);

  // Remove stored timeout
  if (window.jwtTimes) {
    delete window.jwtTimes[storeId];
  }
};
