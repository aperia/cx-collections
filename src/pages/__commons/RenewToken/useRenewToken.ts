import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

// Constant
import { RENEW_TOKEN_CONFIG } from './constants';

// Helper
import { removeAllSetTimeout } from './helpers';

// Types
import { ExpireToken } from './types';

// Redux
import { renewTokenActions } from './_redux/reducer';

export const useRenewToken = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    function handleTokenExpired(this: Window, event: Event) {
      const { storeId } = (event as CustomEvent<ExpireToken>)?.detail;
      dispatch(renewTokenActions.triggerExpiredToken({ storeId }));
    }

    window.addEventListener(
      RENEW_TOKEN_CONFIG.TOKEN_EXPIRED,
      handleTokenExpired
    );
  }, [dispatch]);

  useEffect(() => {
    return () => {
      removeAllSetTimeout();
    };
  }, [dispatch]);
};
