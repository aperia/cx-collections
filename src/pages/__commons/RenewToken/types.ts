import { AxiosRequestConfig } from 'axios';

export interface RenewTokenArgs extends StoreIdPayload {
  payload: RenewTokenPayload;
}

export interface TriggerRenewTokenArgs {
  storeId: string;
  accountId: string;
}

export interface TriggerRenewTokenPayload {
  jwt: string;
}

export interface RenewTokenState {
  token?: string;
  isLoading?: boolean;
  isLoadingRelated?: boolean;
}

export interface RenewTokenResult {
  [storeId: string]: RenewTokenState;
}

export interface RenewTokenConfig extends AxiosRequestConfig {
  token?: string;
}

export interface ExpireToken extends StoreIdPayload {}

export interface ExpireTokenArgs extends StoreIdPayload {}
