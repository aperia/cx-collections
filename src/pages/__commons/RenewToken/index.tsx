import React from 'react';

// Hooks
import { useRenewToken } from './useRenewToken';

const RenewToken: React.FC = () => {
  useRenewToken();

  return null;
};

export default RenewToken;
