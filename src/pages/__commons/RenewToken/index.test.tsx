import React from 'react';
import { AccountDetailProvider } from 'app/hooks';
import RenewToken from './index';
import { renderMockStoreId, storeId } from 'app/test-utils';
import { RENEW_TOKEN_CONFIG } from './constants';
import { ExpireToken } from './types';

describe('Test index', () => {
  it('Should run RenewToken', () => {
    const { wrapper } = renderMockStoreId(
      <AccountDetailProvider
        value={{
          accEValue: storeId,
          storeId: storeId
        }}
      >
        <RenewToken />
      </AccountDetailProvider>
    );

    const event = new CustomEvent<ExpireToken>(
      RENEW_TOKEN_CONFIG.TOKEN_EXPIRED,
      {
        bubbles: true,
        cancelable: true,
        detail: {
          storeId: storeId
        }
      }
    );

    window.dispatchEvent(event);

    expect(wrapper).toBeTruthy();
  });
});
