import { accEValue } from 'app/test-utils';
// services
import { commonService } from './api-service';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

describe('commonService', () => {
  describe('getMapping', () => {
    const params: RenewTokenPayload = {
      jwt: 'jwt'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      commonService.getRenewToken(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      commonService.getRenewToken(params);

      expect(mockService).toBeCalledWith(apiUrl.common.renewToken, params);
    });

    it('getAccessToken', () => {
      mockAppConfigApi.clear();
      const mockService = mockApiServices.post();
      commonService.getAccessToken({ accountId: accEValue });
  
      expect(mockService).toBeCalledWith('', { accountId: accEValue });
    });
  });
});
