export const LANGUAGE_DATA: RefDataValue[] = [
  {
    value: 'en',
    description: 'English (US)'
  },
  {
    value: 'fr',
    description: 'Français'
  },
  {
    value: 'es',
    description: 'Español'
  }
];

export const CURRENT_LANGUAGE = 'CURRENT_LANGUAGE';
