import React from 'react';

import { renderMockStore } from 'app/test-utils';
import LanguageDropdown from '.';
import { screen, fireEvent } from '@testing-library/react';
import {
  DropdownBaseChangeEvent,
  DropdownListProps
} from 'app/_libraries/_dls/components';

jest.mock('app/_libraries/_dls/components', () => {
  return {
    ...(jest.requireActual('app/_libraries/_dls/components') as object),
    DropdownList: (props: DropdownListProps) => {
      const {
        onChange = (event: DropdownBaseChangeEvent) => undefined,
        onBlur = () => undefined,
        onFocus = () => undefined
      } = props;
      return (
        <>
          <input
            data-testid="DropdownList_onChange"
            onChange={(event: any) => {
              event.target = {
                ...event.target,
                value: JSON.parse(event.target.value)
              };
              onChange(event);
            }}
          />
          <button data-testid="DropdownList_onFocus" onFocus={onFocus} />
          <button data-testid="DropdownList_onBlur" onBlur={onBlur} />
        </>
      );
    }
  };
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const initialState: Partial<RootState> = {
  i18next: { lang: 'vn', isLoading: false, resources: {} }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(<LanguageDropdown onChange={jest.fn} />, {
    initialState
  });
};

describe('Test Language Dropdown component', () => {
  it('render Language Dropdown component', () => {
    renderWrapper(initialState);
  });

  it('should render component onChange', () => {
    renderWrapper(initialState);

    fireEvent.change(screen.getByTestId('DropdownList_onChange'), {
      target: {
        value: JSON.stringify({ value: 'fr' })
      }
    });
  });

  it('should render component onBlur', () => {
    renderWrapper(initialState);

    fireEvent.blur(screen.getByTestId('DropdownList_onBlur'), {
      target: {
        value: JSON.stringify({ value: 'fr' })
      }
    });
  });

  it('should render component onFocus', () => {
    renderWrapper(initialState);

    fireEvent.focus(screen.getByTestId('DropdownList_onFocus'));
  });
});
