import {
  DropdownBaseChangeEvent,
  DropdownList,
  DropdownListItem
} from 'app/_libraries/_dls/components';
import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { i18nextActions } from '../I18nextProvider/_redux/reducers';
import { getLang } from '../I18nextProvider/_redux/selectors';
import { CURRENT_LANGUAGE, LANGUAGE_DATA } from './constants';
import { getSessionStorage, setSessionStorage } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls/hooks';

interface ILanguageDropdown {
  onChange?: () => void;
}

const LanguageDropdown: React.FC<ILanguageDropdown> = ({ onChange }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const currentLang: string = useSelector(getLang);
  const selected =
    useMemo(
      () => LANGUAGE_DATA.find(item => item.value === currentLang),
      [currentLang]
    ) || LANGUAGE_DATA[0];
  const [, setIsFocus] = useState(false);

  const handleChange = (e: DropdownBaseChangeEvent) => {
    const langCode = e.target.value.value;
    dispatch(i18nextActions.changeLang({ lang: langCode }));
    setSessionStorage(CURRENT_LANGUAGE, { lang: langCode });
    onChange && onChange();
  };

  const handleFocus = () => {
    setIsFocus(true);
  };

  const handleBlur = () => {
    setIsFocus(false);
  };

  useEffect(() => {
    const activeLanguage = getSessionStorage(CURRENT_LANGUAGE);
    if (!activeLanguage || activeLanguage.lang === currentLang) return;
    dispatch(i18nextActions.changeLang({ lang: activeLanguage.lang }));
  }, [currentLang, dispatch]);

  return (
    <DropdownList
      value={selected}
      onChange={handleChange}
      textField="description"
      variant="no-border"
      onFocus={handleFocus}
      onBlur={handleBlur}
      popupBaseProps={{
        popupBaseClassName: 'popup-header'
      }}
      noResult={t('txt_no_results_found')}
    >
      {LANGUAGE_DATA.map(item => (
        <DropdownListItem
          key={`${item.value}-${item.description}`}
          label={item.description}
          value={item}
        />
      ))}
    </DropdownList>
  );
};

export default LanguageDropdown;
