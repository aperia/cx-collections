// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi } from 'app/test-utils/mocks/mockProperty';
import { commonService } from './api-services';

describe('accessConfigServices', () => {
  it('getAccessConfig', () => {
    mockAppConfigApi.clear();
    const mockService = mockApiServices.post();
    commonService.getAccessConfig({ common: {} });

    expect(mockService).toBeCalledWith('', { common: {} });
  });
});
