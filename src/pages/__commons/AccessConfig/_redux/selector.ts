import { createSelector } from '@reduxjs/toolkit';

export const getCSPAList = (states: RootState) =>
  states?.accessConfig?.cspaList || [];

export const selectCSPAFromAccessConfig = createSelector(
  getCSPAList,
  (cspaList: any) => {
    const [clientNumber = '', system = '', prin = '', agent = ''] =
      cspaList?.[0]?.cspa?.split('-') || [];

    return {
      clientNumber,
      system,
      prin,
      agent
    };
  }
);

const getLoadingStatus = (state: RootState) => {
  return state.accessConfig?.loading;
};

export const selectAccessConfigLoading = createSelector(getLoadingStatus, data => data);
