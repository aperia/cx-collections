import { FULFILLED, PENDING, REJECTED } from 'app/constants';
import { mockAsyncThunk } from 'app/test-utils';
import { mockBuilder } from 'app/test-utils/mockBuilder';
import { commonService } from '../api-services';
import { getAccessConfig } from './getAccessConfig';

window['appConfig'] = {} as any;
const response = {};

const initialState = {
  accessConfig: {}
};

const args = undefined;

const setupGetAccessConfig = mockAsyncThunk({
  service: commonService,
  fn: getAccessConfig
})({ args });

const setupGetAccessConfigBuilder = mockBuilder(getAccessConfig, args);

const getAccessConfigCases = [
  {
    caseName: 'getAccessConfig > fulfilled',
    actionState: FULFILLED,
    expected: {
      type: getAccessConfig.fulfilled.type,
      data: undefined
    }
  },
  {
    caseName: 'getAccessConfig > rejected from axios',
    actionState: REJECTED,
    expected: {
      type: getAccessConfig.rejected.type,
      data: { response: new Error('rejected error') }
    }
  }
];

describe.each(getAccessConfigCases)(
  'getAccessConfig',
  ({ caseName, actionState, expected }) => {
    it(caseName, async () => {
      const { result } = await setupGetAccessConfig({
        actionState,
        payload: undefined,
        initialState
      });

      // Assert
      const { type, payload } = result;
      expect(type).toEqual(expected.type);
      expect(payload).toEqual(expected.data);
    });
  }
);

describe('getAccessConfigBuilder', () => {
  it.each`
    actionState  | expected
    ${PENDING}   | ${{ loading: true }}
    ${FULFILLED} | ${{ loading: false, cspaList: response }}
    ${REJECTED}  | ${{ loading: false }}
  `('$actionState', ({ actionState, expected }) => {
    const { actual } = setupGetAccessConfigBuilder(actionState, {
      ...initialState,
      accessConfig: {
        loading: false
      }
    });

    // Then
    expect(actual?.accessConfig).toEqual(expect.objectContaining(expected));
  });
});
