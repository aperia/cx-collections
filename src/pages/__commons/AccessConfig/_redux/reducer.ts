import { createSlice } from '@reduxjs/toolkit';
import { getAccessConfig, getAccessConfigBuilder } from './getAccessConfig';

// Type
import { AccessConfigResult } from '../types';

// Redux
const { reducer, actions } = createSlice({
  name: 'accessConfig',
  initialState: {} as AccessConfigResult,
  reducers: {},
  extraReducers: builder => {
    getAccessConfigBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getAccessConfig
};

export { allActions as accessConfigActions, reducer as accessConfig };
