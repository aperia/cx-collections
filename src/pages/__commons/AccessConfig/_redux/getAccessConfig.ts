import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
// Service
import { commonService } from '../api-services';
import { GetAccessConfigReturned } from '../types';

export const getAccessConfig = createAsyncThunk<
  GetAccessConfigReturned,
  undefined,
  ThunkAPIConfig
>('accessConfig/getAccessConfig', async (args, thunkAPI) => {
  const { x500id, privileges } = window.appConfig?.commonConfig || {};

  try {
    const response = await commonService.getAccessConfig({
      common: {
        x500id,
        privileges
      }
    });

    const data = response.data.x500IdCspaList;
    return data;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const getAccessConfigBuilder = (
  builder: ActionReducerMapBuilder<any>
) => {
  builder
    .addCase(getAccessConfig.pending, (draftState, action) => {
      return {
        ...draftState,
        loading: true
      };
    })
    .addCase(getAccessConfig.fulfilled, (draftState, action) => {
      const data = action.payload;
      return {
        ...draftState,
        loading: false,
        cspaList: data
      };
    })
    .addCase(getAccessConfig.rejected, (draftState, action) => {
      return {
        ...draftState,
        loading: false,
        isError: true
      };
    });
};
