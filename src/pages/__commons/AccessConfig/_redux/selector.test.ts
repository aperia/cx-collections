// utils
import { selectorWrapper } from 'app/test-utils';
import * as selectors from 'pages/__commons/AccessConfig/_redux/selector';

const fakeCspa = 'clientNumber-system-prin-agent';
const defaultTest = {
  cspaList: [
    {
      cspa: fakeCspa,
      active: true,
      description: 'default'
    }
  ],
  loading: true,
  isError: true
};

const store: Partial<RootState> = {
  accessConfig: {
    cspaList: defaultTest.cspaList,
    loading: defaultTest.loading,
    isError: defaultTest.isError
  } as any
};

const getExpectCspa = (cspa: string) => {
  const [clientNumber = '', system = '', prin = '', agent = ''] =
    cspa.split('-');

  return {
    clientNumber,
    system,
    prin,
    agent
  };
};

it.each`
  selectorName                    | expected
  ${'selectCSPAFromAccessConfig'} | ${{ data: getExpectCspa(fakeCspa), emptyData: getExpectCspa('') }}
  ${'selectAccessConfigLoading'}  | ${{ data: true, emptyData: undefined }}
`('$selectorName', ({ selectorName, expected }) => {
  // Given
  const handleSelector = selectors[selectorName as keyof typeof selectors];

  // When
  const { data, emptyData } = selectorWrapper(store)(handleSelector);

  // Then
  expect(data).toEqual(expected.data);
  expect(emptyData).toEqual(expected.emptyData);
});
