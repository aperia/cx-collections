import { apiService } from 'app/utils/api.service';
import { GetAccessConfigRequestBody } from './types';

export const commonService = {
  getAccessConfig(requestBody: GetAccessConfigRequestBody) {
    const url = window.appConfig?.api?.accessConfig?.getAccessConfig || '';
    return apiService.post(url, requestBody);
  }
};
