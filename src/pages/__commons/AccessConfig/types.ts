export interface AccessConfigResult {
  cspaList?: any[];
  loading?: boolean;
}

export interface GetAccessConfigRequestBody extends CommonRequest {}

export interface GetAccessConfigReturned {}
