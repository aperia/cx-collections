import React from 'react';
import TabBar from './index';
import {
  mockActionCreator,
  queryAllByClass,
  renderMockStoreId
} from 'app/test-utils';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { actions } from './_redux';
import { ItemTab } from './types';

const initialTabs: ItemTab[] = [
  {
    storeId: 'tab01',
    title: 'Tab 01',
    tabType: 'accountDetail',
    className: 'page-home',
    iconName: 'home',
    props: {
      queueId: 'queueId'
    }
  },
  {
    storeId: 'home',
    title: 'Home',
    tabType: 'accountDetail',
    className: 'page-home',
    iconName: 'home'
  },
  {
    storeId: 'tab01',
    title: 'Tab 01',
    tabType: 'accountDetail',
    className: 'page-home',
    iconName: 'home'
  },
  {
    storeId: 'tab02',
    title: 'Tab 02',
    tabType: 'accountDetail',
    className: 'page-home'
  },
  {
    storeId: 'tab03',
    title: 'Tab QueueId',
    tabType: 'accountDetail',
    className: 'page-home',
    iconName: 'home',
    props: {
      queueId: 'queueId'
    }
  }
];

window.HTMLElement.prototype.scrollTo = jest.fn();

const tabActions = mockActionCreator(actions);

describe('Render', () => {
  it('shoud render when tabs is undefined', () => {
    const { wrapper } = renderMockStoreId(<TabBar />, {
      initialState: {
        tab: {
          tabStoreIdSelected: '',
          tabs: undefined as unknown as ItemTab[]
        }
      }
    });

    expect(wrapper.container.querySelector('.dls-tabbar')).toBeNull();
  });

  it('shoud render when in production envirement', () => {
    const { wrapper } = renderMockStoreId(<TabBar />, {
      initialState: {
        tab: {
          tabStoreIdSelected: 'tab',
          tabs: initialTabs
        }
      }
    });

    expect(wrapper.container.querySelector('.dls-tabbar')).toBeInTheDocument();
  });
});

describe('TabBar Component', () => {
  it('should call select function', async () => {
    const spy = tabActions('selectTab');
    renderMockStoreId(<TabBar />, {
      initialState: {
        tab: {
          tabStoreIdSelected: 'tab',
          tabs: initialTabs
        }
      }
    });

    const ele = screen.getByText('Tab 02');
    userEvent.click(ele);

    expect(spy).toBeCalled();
  });

  it('should call select function when no tab was selected', () => {
    const spy = tabActions('selectTab');
    renderMockStoreId(<TabBar />, {
      initialState: {
        tab: {
          tabStoreIdSelected: '',
          tabs: initialTabs
        }
      }
    });

    const ele = screen.getByText('Tab 02');
    userEvent.click(ele);

    expect(spy).toBeCalled();
  });

  it('should call remove tab', () => {
    const spy = tabActions('removeTab');

    const { wrapper } = renderMockStoreId(<TabBar />, {
      initialState: {
        tab: {
          tabStoreIdSelected: 'home',
          tabs: initialTabs
        }
      }
    });
    const icons = queryAllByClass(wrapper.container, 'icon icon-close');
    userEvent.click(icons[0]);

    expect(spy).toBeCalled();
  });

  it('should call remove tab and unlock account when have queueId', () => {
    const spy = tabActions('removeTab');
    const { wrapper } = renderMockStoreId(<TabBar />, {
      initialState: {
        tab: {
          tabStoreIdSelected: 'tab03',
          tabs: initialTabs
        }
      }
    });
    const icons = queryAllByClass(wrapper.container, 'icon icon-close');
    userEvent.click(icons[0]);

    expect(spy).toBeCalled();
  });
});
