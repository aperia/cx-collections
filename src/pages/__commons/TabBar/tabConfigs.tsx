// TabComponent
import AccountDetail from 'pages/AccountDetails';
import Home from 'pages/AccountSearch/Home';
import TransactionAdjustmentRequestList from 'pages/TransactionAdjustment/RequestList';
import ClientConfigurationList from 'pages/ClientConfiguration/List';
import SearchResultList from 'pages/AccountSearch/AccountSearchList';
import RelatedAccount from 'pages/AccountDetails/RelatedAccount';
import QueueList from 'pages/QueueList';
import WorkQueueCore from 'pages/Collector/WorkQueue';
import CollectorList from 'pages/Collector/CollectorList';
import UserRoleEntitlement from 'pages/ConfigurationEntitlement/UserRoleEntitlements';
import ContactAndAccountEntitlement from 'pages/ConfigurationEntitlement/ContactAndAccountEntitlement';

import { TabTypeComponent } from './types';
import ReportingTabs from 'pages/Reporting/ReportingTabs';
import ChangeHistory from 'pages/ClientConfiguration/ChangeHistory';
import CSPAList from 'pages/ExceptionHandling/CSPAList';
import QueueRoleMappingConfiguration from 'pages/ClientConfiguration/QueueAndRoleMapping/QueueRoleMappingConfiguration';
import QueueDetail from 'pages/AccountSearch/Home/Dashboard/QueueDetail';

export const tabComponents: Record<TabTypeComponent, any> = {
  accountDetail: AccountDetail,
  queueDetail: QueueDetail,
  home: Home,
  accountRelated: RelatedAccount,
  accountSearchList: SearchResultList,
  queueList: QueueList,
  clientConfiguration: ClientConfigurationList,
  transactionAdjustment: TransactionAdjustmentRequestList,
  reporting: ReportingTabs,
  workQueueCore: WorkQueueCore,
  collectorList: CollectorList,
  userRoleEntitlement: UserRoleEntitlement,
  contactAndAccountEntitlement: ContactAndAccountEntitlement,
  changeHistory: ChangeHistory,
  exceptionHandling: CSPAList,
  queueRoleMapping: QueueRoleMappingConfiguration
};
