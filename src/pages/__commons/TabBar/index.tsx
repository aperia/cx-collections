import React, { useCallback, useMemo } from 'react';

// components
import TabTitle from './TabTitle';
import {
  TabBarProps,
  TabBar as TabBarTabStrip,
  TabBarTab as TabBarTabStripTab,
  Icon
} from 'app/_libraries/_dls/components';

// redux store
import { batch, useDispatch } from 'react-redux';
import { actions as tabActions } from './_redux';
import { TabState } from './types';
import get from 'lodash.get';

// helpers
import { removeStoreRedux } from 'app/helpers';
import { useStoreIdSelector } from 'app/hooks';

// Redux
import { takeTabData } from './_redux/selectors';
import { useTranslation } from 'app/_libraries/_dls/hooks';

import { tabComponents } from './tabConfigs';

interface TabSelectedProps {
  selected: number;
}

const TabBar: React.FC<TabBarProps> = ({ dataTestId }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const { tabs, tabStoreIdSelected } =
    useStoreIdSelector<TabState>(takeTabData);

  const indexTabSelected = useMemo(() => {
    if (!Array.isArray(tabs) || !tabStoreIdSelected) return 0;

    const index = tabs.findIndex(tab => tab.storeId === tabStoreIdSelected);
    return index === -1 ? 0 : index;
  }, [tabs, tabStoreIdSelected]);

  // handle select tab
  const handleOnSelectTab = (event: TabSelectedProps) => {
    const tabSelected = get(tabs, event.selected);

    dispatch(
      tabActions.selectTab({
        storeId: tabSelected.storeId,
        accEValue: tabSelected.accEValue
      })
    );
  };

  // handle remove tab
  const handleOnRemoveTab = useCallback(
    (storeId: string) => {
      // Remove data from store based on store id
      const removeTab = tabs.find(tab => tab.storeId === storeId);

      // Trigger action remove tab
      batch(() => {
        dispatch(tabActions.removeTab({ storeId: storeId }));
        dispatch(
          removeStoreRedux(removeTab?.tabType, storeId, removeTab?.accEValue)
        );
      });
    },
    [dispatch, tabs]
  );

  const tabsElement = useMemo(() => {
    if (!Array.isArray(tabs)) return [];

    return tabs.map(({ storeId, title, iconName, props = {}, tabType }) => {
      const ChildTabComponent = tabComponents[tabType];
      return (
        <TabBarTabStripTab
          key={storeId}
          title={
            <>
              {iconName && (
                <Icon size="4x" name={iconName} dataTestId={dataTestId} />
              )}
              <TabTitle
                dataTestId={dataTestId}
                storeId={storeId}
                title={t(title)}
                isHideCloseIcon={storeId === 'home'}
                onRemoveTab={handleOnRemoveTab}
              />
            </>
          }
          hoverTitle={t(title)}
        >
          <ChildTabComponent {...props} />
        </TabBarTabStripTab>
      );
    });
  }, [tabs, handleOnRemoveTab, t, dataTestId]);

  return (
    <div>
      <TabBarTabStrip
        onSelect={handleOnSelectTab}
        selected={indexTabSelected}
        dataTestId={dataTestId}
      >
        {tabsElement}
      </TabBarTabStrip>
    </div>
  );
};

export default TabBar;
