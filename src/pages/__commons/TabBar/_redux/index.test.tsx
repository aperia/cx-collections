import { actions, reducer } from './index';
import { storeId, accEValue } from 'app/test-utils';
import { ItemTab, TabState } from '../types';

const mockTabs: ItemTab[] = [
  {
    storeId: 'home',
    title: 'Home',
    tabType: 'home',
    className: 'page-home'
  },
  {
    storeId: '1111',
    title: '1111',
    tabType: 'accountDetail',
    className: '1111',
    accEValue
  },
  {
    storeId: 'accountRelated',
    title: 'accountRelated',
    tabType: 'accountRelated'
  }
];

const initReducer: TabState = {
  tabStoreIdSelected: '1111',
  accEValueSelected: accEValue,
  tabs: mockTabs
};

const MOCK_ACCOUNT_ID = '0421';
const mockTab: ItemTab = {
  id: MOCK_ACCOUNT_ID,
  title: `John - ${MOCK_ACCOUNT_ID}`,
  storeId: MOCK_ACCOUNT_ID,
  tabType: 'accountDetail',
  iconName: 'account-details'
};

const { addTab, selectTab, removeTab, updateTabPops, updateTabTitle } = actions;

describe('Tab redux-store', () => {
  it('Add tab > already exist', () => {
    const nextState = reducer(
      initReducer,
      addTab({
        storeId: '1111',
        title: '1111',
        iconName: 'account-details',
        props: {},
        tabType: 'accountDetail',
        isNotUnique: true
      })
    );
    const nextStateNewTab = nextState.tabs?.find(tab => tab.storeId === '1111');
    // home + action addTab above
    expect(nextState.tabs.length).toEqual(mockTabs.length + 1);
    expect(nextStateNewTab).toBeDefined();
  });

  it('Add tab > Unique = true', () => {
    const nextState = reducer(
      initReducer,
      addTab({ ...mockTab, isNotUnique: true })
    );
    const nextStateNewTab = nextState.tabs?.find(
      tab => tab.storeId === MOCK_ACCOUNT_ID
    );
    // home + action addTab above
    expect(nextState.tabs.length).toEqual(mockTabs.length + 1);
    expect(nextStateNewTab).toBeDefined();
  });

  it('should run addTab having accEValue', () => {
    const nextState = reducer(
      initReducer,
      addTab({
        storeId: '1111',
        title: '1111',
        tabType: 'accountDetail',
        className: '1111',
        accEValue
      })
    );
    expect(nextState.tabStoreIdSelected).toEqual('1111');
    expect(nextState.accEValueSelected).toEqual(accEValue);
    expect(nextState.tabs.length).toEqual(mockTabs.length);
  });

  it('should run addTab without having accEValue', () => {
    const nextState = reducer(
      initReducer,
      addTab({
        storeId: '1111',
        title: '1111',
        iconName: 'account-details',
        tabType: 'accountDetail'
      })
    );
    expect(nextState.tabStoreIdSelected).toEqual('1111');
    expect(nextState.tabs.length).toEqual(mockTabs.length);
  });

  it('should run addTab with having accEValue > add new tab', () => {
    const nextState = reducer(
      initReducer,
      addTab({
        storeId: '2222',
        title: '1111',
        iconName: 'account-details',
        tabType: 'accountDetail'
      })
    );
    expect(nextState.tabStoreIdSelected).toEqual('2222');
    expect(nextState.tabs.length).toEqual(mockTabs.length + 1);
  });

  it('should run addTab', () => {
    const nextState = reducer(
      initReducer,
      addTab({
        storeId: '1111',
        title: '1111',
        iconName: 'account-details',
        tabType: 'accountDetail',
        accEValue
      })
    );
    expect(nextState.tabStoreIdSelected).toEqual('1111');
    expect(nextState.tabs.length).toEqual(mockTabs.length);
  });

  it('should run addTab', () => {
    const nextState = reducer(
      initReducer,
      addTab({
        storeId: '3333',
        title: '1111',
        iconName: 'account-details',
        tabType: 'accountDetail',
        className: '1111',
        accEValue: '3333'
      })
    );
    expect(nextState.tabStoreIdSelected).toEqual('3333');
    expect(nextState.tabs.length).toEqual(mockTabs.length + 1);
  });

  it('should run removeTab', () => {
    const initReducers = {
      tabStoreIdSelected: '2222',
      accEValueSelected: accEValue,
      tabs: [
        {
          storeId: 'home',
          title: 'Home',
          tabType: 'home',
          className: 'page-home'
        },
        {
          storeId: '1111',
          title: '1111',
          tabType: 'accountDetail',
          className: '1111',
          accEValue
        },
        {
          storeId: '2222',
          title: '2222',
          tabType: 'accountDetail',
          className: '2222',
          accEValue
        },
        {
          storeId: '3333',
          title: '3333',
          tabType: 'accountDetail',
          className: '3333',
          accEValue
        }
      ]
    } as TabState;

    const nextState = reducer(
      initReducers,
      removeTab({
        storeId: '3333'
      })
    );

    // Home not be remove
    expect(nextState.tabs.length).toEqual(3);
  });

  it('Remove Tab > not found tab', () => {
    const nextState = reducer(
      initReducer,
      removeTab({
        storeId: '0292'
      })
    );
    // Home not be remove
    expect(nextState.tabs.length).toEqual(mockTabs.length);
  });

  it('Remove Tab > remove success and step back 1', () => {
    const nextState = reducer(
      initReducer,
      removeTab({
        storeId: 'home'
      })
    );

    // Home not be remove
    expect(nextState.tabs.length).toEqual(mockTabs.length - 1);
    expect(nextState.tabs?.[0].storeId).toEqual('1111');
  });

  it('should run removeTab', () => {
    const initReducer: TabState = {
      tabStoreIdSelected: '1111',
      accEValueSelected: accEValue,
      tabs: [
        {
          storeId: '1111',
          title: '1111',
          tabType: 'accountDetail',
          className: '1111',
          accEValue
        },
        {
          storeId: '2222',
          title: '2222',
          tabType: 'accountDetail',
          className: '2222',
          accEValue
        },
        {
          storeId: '3333',
          title: '3333',
          tabType: 'accountDetail',
          className: '3333',
          accEValue
        }
      ]
    };

    const nextState = reducer(
      initReducer,
      removeTab({
        storeId: '1111'
      })
    );

    // Home not be remove
    expect(nextState.tabs.length).toEqual(2);
    expect(nextState.tabs?.[0].storeId).toEqual('2222');
  });

  it('should run selectTab', () => {
    const nextState = reducer(initReducer, selectTab({ storeId, accEValue }));
    expect(nextState.tabStoreIdSelected).toEqual(storeId);
    expect(nextState.accEValueSelected).toEqual(accEValue);
  });

  it('should run updateTabPops > not update', () => {
    const nextState = reducer(
      initReducer,
      updateTabPops({ storeId, title: 'TEST' })
    );
    expect(nextState.tabs).toEqual(initReducer.tabs);
  });

  it('should run updateTabPops', () => {
    const nextState = reducer(
      initReducer,
      updateTabPops({ storeId: 'home', title: 'TEST' })
    );
    expect(nextState.tabs[0].title).toEqual('TEST');
  });

  it('should run updateTabTitle', () => {
    const nextState = reducer(
      initReducer,
      updateTabTitle({ storeId: 'home', primaryName: 'TEST' })
    );
    expect(nextState.tabs[0].title).toEqual('TEST - NaN');
  });

  it('should run updateTabTitle with tab type accountRelated', () => {
    const nextState = reducer(
      initReducer,
      updateTabTitle({ storeId: 'accountRelated', primaryName: 'TEST' })
    );
    expect(nextState.tabs[2].title).toEqual('accountRelated');
  });
});
