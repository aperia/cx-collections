import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import isUndefined from 'lodash.isundefined';

// Helper
import { formatMaskNumber, getSessionStorage } from 'app/helpers';

// Types
import { SelectedTabPayload, TabState, ItemTab } from '../types';

const existingTabs = getSessionStorage('tab');

const defaultTab = {
  tabs: [
    {
      storeId: 'home',
      title: 'txt_home',
      iconName: 'home',
      tabType: 'home',
      props: {},
      className: 'page-home'
    }
  ],
  tabStoreIdSelected: 'home'
};

const initialTabState = existingTabs || defaultTab;

export const { actions, reducer } = createSlice({
  name: 'tab',
  initialState: initialTabState as TabState,
  reducers: {
    // add new tab and active it, or active it if exists
    addTab: (draftState, action: PayloadAction<ItemTab>) => {
      const { storeId, isNotUnique, accEValue } = action.payload;
      if (isNotUnique) {
        draftState.tabs.push(action.payload);
        draftState.tabStoreIdSelected = storeId;
        draftState.accEValueSelected = accEValue;
        return;
      }

      // if have not accEValue
      if (!accEValue) {
        const tabActiveIndex = draftState.tabs.findIndex(
          tab => tab.storeId === storeId
        );

        // always active tab
        draftState.tabStoreIdSelected = storeId;

        // if not exists
        if (tabActiveIndex < 0) draftState.tabs.push(action.payload);
        return;
      }

      // if tab is unique, check is it exists ?
      const tabActive = draftState.tabs.find(tab => tab.storeId === storeId);

      // always active tab
      draftState.tabStoreIdSelected = isUndefined(tabActive)
        ? storeId
        : tabActive?.storeId;
      draftState.accEValueSelected = accEValue;

      // if not exists
      if (isUndefined(tabActive)) draftState.tabs.push(action.payload);
      // tab exists and unique => update info
      if (tabActive) {
        const indexOf = draftState.tabs.indexOf(tabActive);
        draftState.tabs[indexOf] = action.payload;
      }
    },
    // remove tab, if tab removed is tab selected => turn on logic select tab
    removeTab: (
      draftState,
      action: PayloadAction<Pick<ItemTab, 'storeId'>>
    ) => {
      const { storeId } = action.payload;
      const removeIndex = draftState.tabs.findIndex(
        tab => tab.storeId === storeId
      );
      const selectedIndex = draftState.tabs.findIndex(
        tab => tab.storeId === draftState.tabStoreIdSelected
      );

      // not found tab
      if (removeIndex === -1) return;

      draftState.tabs.splice(removeIndex, 1);

      const tabsLength = draftState.tabs.length;
      let newSelectedTab = selectedIndex;

      if (removeIndex < selectedIndex) newSelectedTab = selectedIndex - 1;
      if (newSelectedTab >= tabsLength - 1) newSelectedTab = tabsLength - 1;

      draftState.tabStoreIdSelected = draftState.tabs[newSelectedTab].storeId;
      draftState.accEValueSelected = draftState.tabs[newSelectedTab].accEValue;
    },
    // select tab
    selectTab: (draftState, action: PayloadAction<SelectedTabPayload>) => {
      const { storeId, accEValue } = action.payload;
      return {
        ...draftState,
        tabStoreIdSelected: storeId,
        accEValueSelected: accEValue
      };
    },
    // update tab
    updateTabPops: (draftState, action: PayloadAction<Partial<ItemTab>>) => {
      const { storeId, ...newProps } = action.payload;
      const tabIndex = draftState.tabs.findIndex(
        item => item.storeId === storeId
      );
      const tab = draftState.tabs[tabIndex];
      if (!tab) return;
      draftState.tabs[tabIndex] = Object.assign(tab, newProps);
    },
    updateTabTitle: (
      draftState,
      action: PayloadAction<{ storeId: string; primaryName: string }>
    ) => {
      const { storeId, primaryName } = action.payload;
      const tabIndex = draftState.tabs.findIndex(
        item => item.storeId === storeId
      );
      if (draftState.tabs[tabIndex]?.tabType === 'accountRelated') return;
      const accNbr = draftState.tabs[tabIndex]?.props?.accNbr;
      const mask = formatMaskNumber(accNbr) || ({} as MagicKeyValue);

      draftState.tabs[tabIndex] = {
        ...draftState.tabs[tabIndex],
        title: `${primaryName} - ${
          mask!.firstText + mask!.maskText + mask!.lastText
        }`
      };
    }
  }
});

export { actions as tabActions };
