import { createSelector } from '@reduxjs/toolkit';

export const getTabSelected = (state: RootState) => {
  return state.tab.tabStoreIdSelected;
};

export const takeTabSelected = createSelector([getTabSelected], data => data);

export const getTabData = (state: RootState) => {
  return state.tab;
};

export const takeTabData = createSelector([getTabData], data => data);
