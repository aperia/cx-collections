import * as selectors from './selectors';
import { selectorWrapper, storeId, accNbr, accEValue } from 'app/test-utils';

const states: Partial<RootState> = {
  tab: {
    tabs: [
      {
        storeId: '1111',
        title: '1111',
        tabType: 'accountDetail',
        className: '1111',
        accEValue
      }
    ],
    tabStoreIdSelected: storeId,
    accEValueSelected: accNbr
  }
};

const selectorTrigger = selectorWrapper(states);

describe('TabBar selectors', () => {
  it('should run getTabSelected', () => {
    const { data } = selectorTrigger(selectors.getTabSelected);
    const expectData = storeId;
    expect(data).toEqual(expectData);
  });
  it('should run takeTabSelected', () => {
    const { data } = selectorTrigger(selectors.takeTabSelected);
    const expectData = storeId;
    expect(data).toEqual(expectData);
  });
  it('should run getTabData', () => {
    const { data } = selectorTrigger(selectors.getTabData);
    const expectData = states.tab;
    expect(data).toEqual(expectData);
  });
  it('should run takeTabData', () => {
    const { data } = selectorTrigger(selectors.takeTabData);
    const expectData = states.tab;
    expect(data).toEqual(expectData);
  });
});
