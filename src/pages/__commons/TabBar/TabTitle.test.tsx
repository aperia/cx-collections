import React from 'react';
import { render } from '@testing-library/react';

import TabTitle from './TabTitle';
import userEvent from '@testing-library/user-event';
import { storeId } from 'app/test-utils';

describe('Render', () => {
  it('should render', () => {
    const { container } = render(<TabTitle storeId={storeId} />);

    expect(container.querySelector('.tab-title')!).toBeInTheDocument();
  });
});

describe('Actions', () => {
  describe('handleOnClick', () => {
    it('when has storeId', () => {
      const onRemoveTab = jest.fn();
      const { container } = render(
        <TabTitle storeId={storeId} onRemoveTab={onRemoveTab} />
      );

      userEvent.click(container.querySelector('.icon')!);

      expect(onRemoveTab).toBeCalledWith(storeId);
    });

    it('when has no storeId', () => {
      const onRemoveTab = jest.fn();
      const { container } = render(
        <TabTitle
          storeId={undefined as unknown as string}
          onRemoveTab={onRemoveTab}
        />
      );

      userEvent.click(container.querySelector('.icon')!);

      expect(onRemoveTab).not.toBeCalled();
    });
  });
});
