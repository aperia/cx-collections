export interface ItemTab {
  id?: string;
  storeId: string;
  title: string;
  iconName?:
    | 'account-details'
    | 'home'
    | 'file'
    | 'search-result'
    | 'action-list'
    | 'cross';
  isNotUnique?: boolean;
  props?: MagicKeyValue;
  className?: string;
  tabType: TabTypeComponent;
  accEValue?: string;
  customerRoleTypeCode?: string;
  customerExternalIdentifier?: string;
}

export interface TabState {
  tabs: ItemTab[];
  tabStoreIdSelected: string;
  accEValueSelected?: string;
}

export interface SelectedTabPayload {
  storeId: string;
  accEValue?: string;
}

export type TabTypeComponent =
  | 'home'
  | 'accountDetail'
  | 'queueDetail'
  | 'accountSearchList'
  | 'transactionAdjustment'
  | 'queueList'
  | 'accountRelated'
  | 'clientConfiguration'
  | 'reporting'
  | 'changeHistory'
  | 'exceptionHandling'
  | 'workQueueCore'
  | 'userRoleEntitlement'
  | 'contactAndAccountEntitlement'
  | 'collectorList'
  | 'queueRoleMapping';
