import React from 'react';
import { renderMockStore } from 'app/test-utils';
import Entitlements from '.';
import { queryByText } from '@testing-library/react';

jest.mock('pages/__commons/TabBar', () => () => <div>TabBar</div>);

describe('Render', () => {
  it('Should render loading', () => {
    const initialState: Partial<RootState> = {
      entitlement: { isLoading: true }
    };

    const { container } = renderMockStore(<Entitlements />, { initialState });
    const element = queryByText(container, 'TabBar');

    expect(element).toBeInTheDocument();
  });
});
