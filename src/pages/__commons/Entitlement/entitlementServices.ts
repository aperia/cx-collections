import { apiService } from 'app/utils/api.service';

// Types
import {
  GetAccountEntitlementPayload,
  GetEntitlementResponse,
  GetRoleEntitlementPayload
} from './types';

const entitlementServices = {
  getRoleEntitlement: (payload: GetRoleEntitlementPayload) => {
    const { entitlement = '' } = window.appConfig?.api?.common || {};
    return apiService.post<GetEntitlementResponse>(entitlement, payload);
  },
  getRefRoleEntitlement: () => {
    const { refEntitlement = '' } = window.appConfig?.api?.common || {};
    return apiService.get(refEntitlement);
  },
  getAccountEntitlement: (payload: GetAccountEntitlementPayload) => {
    const { entitlement = '' } = window.appConfig?.api?.common || {};
    return apiService.post<GetEntitlementResponse>(entitlement, payload);
  }
};

export default entitlementServices;
