import { createSlice } from '@reduxjs/toolkit';

// Types
import { EntitlementState } from '../types';

import {
  getContactEntitlement,
  getContactEntitlementBuilder
} from './getContactEntitlement';

const { actions, reducer } = createSlice({
  name: 'entitlement',
  initialState: {} as EntitlementState,
  reducers: {},
  extraReducers: builder => {
    getContactEntitlementBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getContactEntitlement
};

export { allActions as entitlementActions, reducer as entitlement };
