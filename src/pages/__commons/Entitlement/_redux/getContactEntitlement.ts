import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// Service
import entitlementServices from '../entitlementServices';

// Type
import {
  EntitlementState,
  GetContactEntitlementArg,
  UserEntitlementPayload
} from '../types';

// Utils
import {
  contactEntitlement,
  convertCallerRoleEntitlement,
  convertUserRoleEntitlement,
  mappingPermission
} from 'app/entitlements';
import { CallerEntitlementConfig } from 'app/entitlements/types';

export const lowerCaseEntitlement = (data: CallerEntitlementConfig) => {
  if (!data?.components) return;
  const components = data?.components?.map(r => ({
    ...r,
    component: r.component?.toLowerCase()
  }));

  return { components };
};

export const getContactEntitlement = createAsyncThunk<
  UserEntitlementPayload,
  GetContactEntitlementArg,
  ThunkAPIConfig
>('entitlement/getContactEntitlement', async (args, thunkAPI) => {
  const { privileges } = window.appConfig.commonConfig || {};

  const { agent, clientNumber, prin, system, accountId, customerRoleTypeCode } =
    args;

  const { app, org } = window.appConfig.commonConfig;

  const { data } = await entitlementServices.getAccountEntitlement({
    common: {
      privileges,
      clientNumber,
      system,
      prin,
      agent,
      app,
      org,
      accountId
    },
    customerRoleTypeCode: customerRoleTypeCode || '01'
  });

  const { callerAccountEntitlements, roleEntitlements } = data;
  const entitlements: MagicKeyValue|undefined = lowerCaseEntitlement(
    callerAccountEntitlements
  );

  const useRoleEntitlements: MagicKeyValue|undefined =
    lowerCaseEntitlement(roleEntitlements);

  return {
    callerAccountEntitlements: entitlements,
    roleEntitlements: useRoleEntitlements
  };
});

export const getContactEntitlementBuilder = (
  builder: ActionReducerMapBuilder<EntitlementState>
) => {
  builder
    .addCase(getContactEntitlement.pending, (draftState, actions) => {
      const { storeId } = actions.meta.arg;

      return {
        ...draftState,
        contactEntitlements: {
          ...draftState.contactEntitlements,
          [storeId]: {
            isLoading: true
          }
        }
      };
    })
    .addCase(getContactEntitlement.fulfilled, (draftState, actions) => {
      const { callerAccountEntitlements = {}, roleEntitlements = {} } =
        actions.payload;
      const { storeId } = actions.meta.arg;

      const callerData = convertCallerRoleEntitlement(
        callerAccountEntitlements
      );
      const roleData = convertUserRoleEntitlement(roleEntitlements);

      const finalContactEntitlement = mappingPermission(roleData, callerData);

      contactEntitlement.registerAll(finalContactEntitlement, storeId);

      return {
        ...draftState,
        contactEntitlements: {
          ...draftState.contactEntitlements,
          [storeId]: {
            isLoading: false
          }
        }
      };
    })
    .addCase(getContactEntitlement.rejected, (draftState, actions) => {
      const { storeId } = actions.meta.arg;
      return {
        ...draftState,
        contactEntitlements: {
          ...draftState.contactEntitlements,
          [storeId]: {
            isLoading: false
          }
        }
      };
    });
};
