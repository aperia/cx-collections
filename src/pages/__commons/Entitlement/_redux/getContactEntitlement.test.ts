import { createStore, isFulfilled, Store } from '@reduxjs/toolkit';
import { storeId } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import entitlementServices from '../entitlementServices';
import { GetContactEntitlementArg } from '../types';
import { getContactEntitlement } from './getContactEntitlement';

describe('Test getContactEntitlement', () => {
  const mockContactEntitlementArgs: GetContactEntitlementArg = {
    storeId,
    agent: '',
    clientNumber: '',
    prin: '',
    system: '',
    accountId: '',
    customerRoleTypeCode: ''
  };

  let store: Store<RootState>;

  beforeEach(() => {
    window.appConfig = {
      commonConfig: {}
    } as AppConfiguration;

    store = createStore(rootReducer, {
      userRole: {
        role: 'COLLECTOR1'
      }
    } as any);
  });

  it('fulfilled', async () => {
    jest
      .spyOn(entitlementServices, 'getRefRoleEntitlement')
      .mockResolvedValue({ data: {} } as any);
    jest.spyOn(entitlementServices, 'getAccountEntitlement').mockResolvedValue({
      data: {}
    } as any);

    const responseGetContactEntitlement = await getContactEntitlement(
      mockContactEntitlementArgs
    )(store.dispatch, store.getState, {});

    expect(isFulfilled(responseGetContactEntitlement)).toBeTruthy();
    expect(responseGetContactEntitlement.payload).toEqual({
      callerAccountEntitlements: undefined,
      roleEntitlements: undefined
    });
  });

  it('fulfilled with isAdminRole === true', async () => {
    window.appConfig = {
      commonConfig: {
        isAdminRole: true
      }
    } as AppConfiguration;

    jest
      .spyOn(entitlementServices, 'getRefRoleEntitlement')
      .mockResolvedValue({ data: {} } as any);
    jest.spyOn(entitlementServices, 'getAccountEntitlement').mockResolvedValue({
      data: {}
    } as any);

    const responseGetContactEntitlement = await getContactEntitlement(
      mockContactEntitlementArgs
    )(store.dispatch, store.getState, {});

    expect(isFulfilled(responseGetContactEntitlement)).toBeTruthy();
    expect(responseGetContactEntitlement.payload).toEqual({
      callerAccountEntitlements: undefined,
      roleEntitlements: undefined
    });
  });

  it('fulfilled without commonConfig', async () => {
    window.appConfig = {} as AppConfiguration;

    jest
      .spyOn(entitlementServices, 'getRefRoleEntitlement')
      .mockResolvedValue({ data: {} } as any);
    jest.spyOn(entitlementServices, 'getAccountEntitlement').mockResolvedValue({
      data: {}
    } as any);

    const responseGetContactEntitlement = await getContactEntitlement(
      mockContactEntitlementArgs
    )(store.dispatch, store.getState, {});

    expect(isFulfilled(responseGetContactEntitlement)).toBeFalsy();
    expect(responseGetContactEntitlement.payload).toEqual(undefined);
  });

  it('fulfilled and lowerCaseEntitlement', async () => {
    jest
      .spyOn(entitlementServices, 'getRefRoleEntitlement')
      .mockResolvedValue({ data: {} } as any);
    jest.spyOn(entitlementServices, 'getAccountEntitlement').mockResolvedValue({
      data: {
        callerAccountEntitlements: {
          components: [
            {
              component: 'Component1'
            }
          ]
        },
        roleEntitlements: {
          components: [
            {
              component: 'Component1'
            }
          ]
        },
      }
    } as any);

    const responseGetContactEntitlement = await getContactEntitlement(
      mockContactEntitlementArgs
    )(store.dispatch, store.getState, {});

    expect(isFulfilled(responseGetContactEntitlement)).toBeTruthy();
    expect(responseGetContactEntitlement.payload).toEqual({
      callerAccountEntitlements: {
        components: [
          {
            component: 'component1'
          }
        ]
      },
      roleEntitlements: {
        components: [
          {
            component: 'component1'
          }
        ]
      }
    });
  });
});
