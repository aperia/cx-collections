// utils
import { selectorWrapper, storeId } from 'app/test-utils';
import * as selectors from './selectors';

const store: Partial<RootState> = {
  entitlement: {
    roleEntitlements: {
      isLoading: true
    },
    contactEntitlements: {
      [storeId]: {
        isLoading: true
      }
    }
  }
};
describe('Test selectors', () => {
  it.each`
    selectorName                             | expected
    ${'takeRoleEntitlementLoadingStatus'}    | ${{ data: true, emptyData: undefined }}
    ${'takeContactEntitlementLoadingStatus'} | ${{ data: true, emptyData: undefined }}
  `('$selectorName', ({ selectorName, expected }) => {
    // Given
    const handleSelector = selectors[selectorName as keyof typeof selectors];

    // When
    const { data, emptyData } = selectorWrapper(store)(handleSelector);

    // Then
    expect(data).toEqual(expected.data);
    expect(emptyData).toEqual(expected.emptyData);
  });
});
