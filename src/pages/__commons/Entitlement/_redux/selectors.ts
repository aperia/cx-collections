import { createSelector } from '@reduxjs/toolkit';

const getRoleEntitlementLoadingStatus = (state: RootState) => {
  return state.entitlement?.roleEntitlements?.isLoading;
};

const getContactEntitlementLoadingStatus = (
  state: RootState,
  storeId: string
) => {
  const { contactEntitlements = {} } = state.entitlement;
  return contactEntitlements[storeId]?.isLoading;
};

export const takeRoleEntitlementLoadingStatus = createSelector(
  getRoleEntitlementLoadingStatus,
  data => data
);

export const takeContactEntitlementLoadingStatus = createSelector(
  getContactEntitlementLoadingStatus,
  data => data
);
