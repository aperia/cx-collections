// utils
import { mockApiServices } from 'app/test-utils';

// services
import entitlementServices from './entitlementServices';
import { mockAppConfigApi } from 'app/test-utils/mocks/mockProperty';

describe('entitlementServices', () => {
  it('getAccountEntitlement', () => {
    mockAppConfigApi.clear();
    const mockService = mockApiServices.post();
    entitlementServices.getAccountEntitlement({ common: {} });

    expect(mockService).toBeCalledWith('', { common: {} });
  });

  it('getRefRoleEntitlement', () => {
    mockAppConfigApi.clear();
    const mockService = mockApiServices.get();
    entitlementServices.getRefRoleEntitlement();

    expect(mockService).toBeCalledWith('');
  });

  it('getRoleEntitlement', () => {
    mockAppConfigApi.clear();
    const mockService = mockApiServices.post();
    entitlementServices.getRoleEntitlement({ common: {} });

    expect(mockService).toBeCalledWith('', { common: {} });
  });
});
