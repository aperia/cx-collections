import {
  CallerEntitlementConfig,
  RoleEntitlementConfig
} from 'app/entitlements/types';

export interface UserEntitlementPayload {
  callerAccountEntitlements?: CallerEntitlementConfig;
  roleEntitlements?: RoleEntitlementConfig;
}

export interface EntitlementState {
  roleEntitlements?: {
    isLoading?: boolean;
  };
  contactEntitlements?: {
    [storeId: string]:
      | {
          isLoading?: boolean;
        }
      | undefined;
  };
}

export interface GetAccountEntitlementPayload extends CommonRequest {
  customerRoleTypeCode?: string;
}
export interface GetRoleEntitlementPayload extends CommonRequest {}

export interface GetEntitlementArgs {}

export interface GetContactEntitlementArg {
  clientNumber: string;
  system: string;
  prin: string;
  agent: string;
  storeId: string;
  accountId: string;
  customerRoleTypeCode?: string;
}
export interface GetEntitlementResponse {
  callerAccountEntitlements: CallerEntitlementConfig;
  roleEntitlements: RoleEntitlementConfig;
}
