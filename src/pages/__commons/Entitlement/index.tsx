import React, { Fragment } from 'react';

// Components
import TabBar from 'pages/__commons/TabBar';

const Entitlement: React.FC = () => {
  return (
    <Fragment>
      <TabBar dataTestId="entitlement-tabbar" />
    </Fragment>
  );
};

export default Entitlement;
