import React from 'react';
import { renderMockStoreId } from 'app/test-utils';
import { screen } from '@testing-library/react';
import DataSection from '.';
describe('Test DataSection', () => {
  it('should have label', () => {
    renderMockStoreId(<DataSection label="test-label" />);
    const queryLabel = screen.queryByText('test-label');
    expect(queryLabel).toBeInTheDocument();
  });

  it('should have not label', () => {
    renderMockStoreId(<DataSection isNoData label="test-label" />);
    const queryLabel = screen.queryByText('txt_no_data');
    expect(queryLabel).toBeInTheDocument();
  });

  it('should have nodata with component', () => {
    renderMockStoreId(
      <DataSection
        isNoData={false}
        component={<div className="test-no-data">test-no-data</div>}
      />
    );
    const queryLabel = screen.queryByText('test-no-data');
    expect(queryLabel).toBeInTheDocument();
  });
});
