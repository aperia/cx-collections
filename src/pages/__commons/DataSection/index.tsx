import React from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface DataSectionProps {
  dataTestId?: string;
  label?: string;
  component?: React.ReactElement;
  isNoData?: boolean;
}

const DataSection: React.FC<DataSectionProps> = ({
  dataTestId,
  label,
  component,
  isNoData
}) => {
  const { t } = useTranslation();

  return (
    <div>
      <h6
        className="color-grey"
        data-testid={genAmtId(dataTestId!, 'label', '')}
      >{t(label)}</h6>
      {isNoData ? (
        <p
          className="color-grey mt-16"
          data-testid={genAmtId(dataTestId!, 'no-data', '')}
        >{t('txt_no_data')}</p>
      ) : (
        component
      )}
    </div>
  );
};

export default DataSection;
