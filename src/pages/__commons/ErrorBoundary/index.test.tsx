import React from 'react';
import { screen } from '@testing-library/react';
import ErrorBoundary from '.';
import FallbackErrorComponent from './FallbackErrorComponent';
import { renderMockStoreId } from 'app/test-utils';

describe('Test ErrorBoundary ', () => {
  it('Should render fallback component when crash render', () => {
    const Throws = () => {
      throw new Error('Error Boundary');
    };
    renderMockStoreId(
      <ErrorBoundary fallbackComponent={FallbackErrorComponent}>
        <Throws />
      </ErrorBoundary>
    );

    window.dispatchEvent(new Event('error'));

    const queryTextWrong = screen.queryByText('txt_some_thing_went_wrong');
    const queryTextTryAgain = screen.queryByText('txt_try_again');
    expect(queryTextWrong).toBeInTheDocument();
    expect(queryTextTryAgain).toBeInTheDocument();
  });
});
