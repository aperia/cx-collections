import React from 'react';
import { screen } from '@testing-library/react';
import FallbackErrorComponent from './FallbackErrorComponent';
import { renderMockStoreId } from 'app/test-utils';

describe('Test ErrorBoundary ', () => {
  beforeEach(() => jest.resetAllMocks());
  it('Should have render layout', () => {
    renderMockStoreId(<FallbackErrorComponent />);
    const queryTextWrong = screen.queryByText('txt_some_thing_went_wrong');
    const queryTextTryAgain = screen.queryByText('txt_try_again');
    expect(queryTextWrong).toBeInTheDocument();
    expect(queryTextTryAgain).toBeInTheDocument();
  });

  it('Should have click', () => {
    Object.defineProperty(window, 'location', {
      writable: true,
      value: { assign: jest.fn() }
    });
    Object.defineProperty(window.location, 'reload', {
      writable: true,
      value: { assign: jest.fn() }
    });
    window.location.reload = jest.fn();
    renderMockStoreId(<FallbackErrorComponent />, {}, false, false);
    const queryTextTryAgain = screen.queryByText('txt_try_again');
    queryTextTryAgain!.click();
    expect(window.location.reload).toHaveBeenCalled();
  });
});
