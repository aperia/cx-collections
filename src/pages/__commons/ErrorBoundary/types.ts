import { ComponentType, ErrorInfo } from 'react';

export interface ErrorProps {
  children?: React.ReactNode;
  fallbackComponent: ComponentType<unknown>;
}

export interface ErrorState {
  error: Error | null;
  errorInfo: ErrorInfo | null;
}
