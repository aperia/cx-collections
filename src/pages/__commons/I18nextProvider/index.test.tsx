import React from 'react';
import I18nextProvider from './index';
import { screen } from '@testing-library/react';
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import { i18nextActions } from './_redux/reducers';
const spyAction = mockActionCreator(i18nextActions);
const text = 'Test-Component';
const ComponentTest = () => <div>{text}</div>;

describe('Test RAF/Header', () => {
  beforeAll(() => {
    jest.resetAllMocks();
  });
  it('render to UI', () => {
    const action = spyAction('getMultipleLanguage');
    renderMockStoreId(
      <I18nextProvider>
        <ComponentTest />
      </I18nextProvider>,
      {},
      false,
      false
    );
    const queryByText = screen.queryByText(text);
    expect(action).toHaveBeenCalled();
    expect(queryByText).toBeInTheDocument();
  });
});
