// mocks
import { mockAxiosResolve } from 'app/test-utils/mocks/mockAxiosResolve';

// services
import getMultipleLanguage from './i18nextService';

describe('getMultipleLanguage', () => {
  it('getMultipleLanguage', () => {
    const lang = 'en';

    getMultipleLanguage.getMultipleLanguage(lang);

    expect(mockAxiosResolve).toBeCalledWith('i18n/en/translation.json');
  });
});
