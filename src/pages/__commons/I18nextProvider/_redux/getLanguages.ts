import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { II18nextReturn, II18nextArg, II18nextReducer } from '../types';
import { mappingDataFromObj } from 'app/helpers';
import i18nextService from '../i18nextService';

export const getMultipleLanguage = createAsyncThunk<
  II18nextReturn,
  II18nextArg,
  ThunkAPIConfig
>('i18next/getMultipleLanguage', async ({ lang = 'en' }, thunkAPI) => {
  try {
    const { data } = await i18nextService.getMultipleLanguage(lang);
    const { mapping } = thunkAPI.getState();
    const i18nextMapping = mapping?.data.i18next || {};
    const mappingData = mappingDataFromObj<II18nextReturn>(
      data,
      i18nextMapping
    );

    return mappingData;
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const getMultipleLanguageBuilder = (
  builder: ActionReducerMapBuilder<II18nextReducer>
) => {
  builder
    .addCase(getMultipleLanguage.pending, draftState => {
      draftState.isLoading = true;
    })
    .addCase(getMultipleLanguage.fulfilled, (draftState, action) => {
      const { lang, resource } = action.payload;
      draftState.resources[lang] = { ...resource };
      draftState.lang = lang;
      draftState.isLoading = false;
    })
    .addCase(getMultipleLanguage.rejected, draftState => {
      draftState.isLoading = false;
    });
};
