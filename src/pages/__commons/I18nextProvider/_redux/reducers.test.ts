import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { i18nextActions } from './reducers';

describe('I18next Reducer', () => {
  const store = createStore(rootReducer);
  it('Change Languages', () => {
    const langVi = 'vi';
    store.dispatch(i18nextActions.changeLang({ lang: langVi }));
    const state = store.getState();
    expect(state.i18next.lang).toEqual(langVi);
  });
});
