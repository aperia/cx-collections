import { responseDefault } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import i18nextService from '../i18nextService';
import { getMultipleLanguage } from './getLanguages';

let spy: jest.SpyInstance;

describe('getMultipleLanguage', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('should return error', async () => {
    spy = jest
      .spyOn(i18nextService, 'getMultipleLanguage')
      .mockResolvedValue({ ...responseDefault });

    const store = createStore(rootReducer, {});
    const response = await getMultipleLanguage({ lang: 'en' })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('i18next/getMultipleLanguage/rejected');
  });

  it('should return fulfilled', async () => {
    spy = jest
      .spyOn(i18nextService, 'getMultipleLanguage')
      .mockResolvedValue({ ...responseDefault });

    const store = createStore(rootReducer, {
      mapping: {
        data: {
          i18next: {}
        }
      }
    });
    const response = await getMultipleLanguage({})(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('i18next/getMultipleLanguage/fulfilled');
  });

  it('should return fulfilled', async () => {
    spy = jest
      .spyOn(i18nextService, 'getMultipleLanguage')
      .mockResolvedValue({ ...responseDefault });

    const store = createStore(rootReducer, {
      mapping: {
        data: {}
      }
    });
    const response = await getMultipleLanguage({})(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('i18next/getMultipleLanguage/fulfilled');
  });
});
