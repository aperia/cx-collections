import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { getSessionStorage } from 'app/helpers';
import { CURRENT_LANGUAGE } from 'pages/__commons/LanguageDropdown/constants';
import { II18nextReducer } from '../types';
import {
  getMultipleLanguage,
  getMultipleLanguageBuilder
} from './getLanguages';
const initialState: II18nextReducer = {
  lang: getSessionStorage(CURRENT_LANGUAGE)?.lang || 'en',
  isLoading: false,
  resources: {}
};

const { actions, reducer } = createSlice({
  name: 'i18next',
  initialState,
  reducers: {
    changeLang: (draftState, action: PayloadAction<{ lang: string }>) => {
      const { lang } = action.payload;
      draftState.lang = lang;
    }
  },
  extraReducers: builder => {
    getMultipleLanguageBuilder(builder);
  }
});

const extraActions = {
  ...actions,
  getMultipleLanguage
};

export { extraActions as i18nextActions, reducer };
