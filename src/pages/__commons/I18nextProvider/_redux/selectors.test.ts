import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { getLang, getResource } from './selectors';

const store = createStore(rootReducer);
const state = store.getState();

describe('I18next Selector', () => {
  it('Test getLang', () => {
    const result = getLang(state);
    expect(result).toEqual(state.i18next.lang);
  });

  it('Test getResource', () => {
    const result = getResource(state);
    const dataEqual = {
      lang: result.lang,
      data: result.data
    };
    expect(result).toEqual(dataEqual);
  });
});
