import React from 'react';
import { fireEvent, screen, waitFor } from '@testing-library/react';
import ConfirmModal from './index';
import { queryByClass, renderMockStoreId } from 'app/test-utils';

describe('Confirm Modal Component', () => {
  it('Should have not open modal', () => {
    const initialState = {
      confirmModal: {
        open: false
      }
    };
    const { wrapper } = renderMockStoreId(
      <div>
        <div className="modal-md-hidden modal-lg-hidden" />
        <ConfirmModal />
      </div>,
      { initialState }
    );
    const queryClassOpenModal = queryByClass(
      wrapper.baseElement as HTMLElement,
      /modal-backdrop show/
    );
    expect(queryClassOpenModal).toBeNull();
  });

  it('Should open modal', () => {
    const initialState = {
      confirmModal: {
        open: true,
        data: { title: 'test title', body: 'test body' }
      }
    };
    const { wrapper } = renderMockStoreId(
      <div>
        <div className="dls-modal-root">
          <div className="dls-modal-dialog window-md window-lg" />
        </div>
        <ConfirmModal />
      </div>,
      { initialState }
    );
    const queryClassOpenModal = queryByClass(
      wrapper.baseElement as HTMLElement,
      /modal-backdrop show/
    );
    const queryTitleHeader = screen.queryByText('test title');
    const queryTitleBody = screen.queryByText('test title');
    expect(queryTitleHeader).toBeInTheDocument();
    expect(queryTitleBody).toBeInTheDocument();
    expect(queryClassOpenModal).toBeInTheDocument();
  });

  it('Should open modal with title and body is React Element', () => {
    const initialState = {
      confirmModal: {
        open: true,
        data: {
          title: <div className="test-title">test title</div>,
          body: <div className="test-body">test body</div>
        }
      }
    };
    const { wrapper } = renderMockStoreId(<ConfirmModal />, { initialState });
    const queryClassHeader = queryByClass(
      wrapper.baseElement as HTMLElement,
      /test-title/
    );
    const queryClassBody = queryByClass(
      wrapper.baseElement as HTMLElement,
      /test-body/
    );
    expect(queryClassHeader).toBeInTheDocument();
    expect(queryClassBody).toBeInTheDocument();
  });

  it('Should have close when trigger icon close', () => {
    const cancelCallbackMock = jest.fn();
    const initialState = {
      confirmModal: {
        open: true,
        data: {
          title: 'test title',
          body: 'test body',
          cancelCallback: cancelCallbackMock
        }
      }
    };
    const { wrapper, store } = renderMockStoreId(<ConfirmModal />, {
      initialState
    });
    const queryIconClose = queryByClass(
      wrapper.baseElement as HTMLElement,
      /icon icon-close/
    );
    queryIconClose!.click();

    expect(store.getState().confirmModal.open).toEqual(false);
  });

  it('Should close when trigger cancel button within callback', () => {
    const cancelCallback = jest.fn();
    const initialState = {
      confirmModal: {
        open: true,
        data: {
          cancelCallback,
          title: 'test title',
          body: 'test body'
        }
      }
    };
    const { wrapper, store } = renderMockStoreId(<ConfirmModal />, {
      initialState
    });
    const queryFooter = queryByClass(
      wrapper.baseElement as HTMLElement,
      /dls-modal-footer/
    );
    const queryButtonClose = queryByClass(queryFooter!, /btn btn-secondary/);
    queryButtonClose?.click();
    expect(store.getState().confirmModal.open).toEqual(false);
    expect(cancelCallback).toHaveBeenCalled();
  });

  it('Should close when trigger cancel button without callback', () => {
    const initialState = {
      confirmModal: {
        open: true,
        data: {
          title: 'test title',
          body: 'test body'
        }
      }
    };
    const { wrapper, store } = renderMockStoreId(<ConfirmModal />, {
      initialState
    });
    const queryFooter = queryByClass(
      wrapper.baseElement as HTMLElement,
      /dls-modal-footer/
    );
    const queryButtonClose = queryByClass(queryFooter!, /btn btn-secondary/);
    queryButtonClose?.click();
    expect(store.getState().confirmModal.open).toEqual(false);
  });

  it('Should close when trigger confirm button', () => {
    const initialState = {
      confirmModal: {
        open: true,
        data: {
          title: 'test title',
          body: 'test body'
        }
      }
    };
    const { wrapper, store } = renderMockStoreId(<ConfirmModal />, {
      initialState
    });
    const queryFooter = queryByClass(
      wrapper.baseElement as HTMLElement,
      /dls-modal-footer/
    );
    const queryButtonConfirm = queryByClass(queryFooter!, /btn btn-primary/);
    queryButtonConfirm?.click();
    expect(store.getState().confirmModal.open).toEqual(false);
  });

  it('Should close when trigger confirm button with confirmCallback', async () => {
    const confirmCallback = jest.fn();
    const initialState = {
      confirmModal: {
        open: true,
        data: {
          confirmCallback,
          title: 'test title',
          body: 'test body'
        }
      }
    };
    const { wrapper, store } = renderMockStoreId(<ConfirmModal />, {
      initialState
    });

    const queryFooter = queryByClass(
      wrapper.baseElement as HTMLElement,
      /dls-modal-footer/
    );
    const queryButtonConfirm = queryByClass(queryFooter!, /btn btn-primary/);
    fireEvent.click(queryButtonConfirm!);
    await waitFor(() => {
      expect(store.getState().confirmModal.open).toEqual(false);
      expect(confirmCallback).toHaveBeenCalled();
    });
  });

  it('Should close when trigger confirm button with error', async () => {
    const consoleError = jest
      .spyOn(global.console, 'error')
      .mockImplementation();
    const confirmCallback = jest.fn().mockRejectedValue(() => false);
    const initialState = {
      confirmModal: {
        open: true,
        data: {
          confirmCallback,
          title: 'test title',
          body: 'test body'
        }
      }
    };
    const { wrapper, store } = renderMockStoreId(<ConfirmModal />, {
      initialState
    });
    const queryFooter = queryByClass(
      wrapper.baseElement as HTMLElement,
      /dls-modal-footer/
    );
    const queryButtonConfirm = queryByClass(queryFooter!, /btn btn-primary/);
    fireEvent.click(queryButtonConfirm!);
    await waitFor(() => {
      expect(store.getState().confirmModal.open).toEqual(false);
      expect(confirmCallback).toHaveBeenCalled();
      expect(consoleError).toBeCalled();
    });
  });
});
