import React, { useEffect, useState } from 'react';

// components
import {
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  ModalTitle,
  Button,
  Icon
} from 'app/_libraries/_dls/components';

// utils
import isFunction from 'lodash.isfunction';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { AppState } from 'storeConfig';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { ModalData } from 'pages/__commons/ConfirmModal/types';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface ConfirmModalProps {
  dataTestId?: string;
}

const ConfirmModal: React.FC<ConfirmModalProps> = ({ dataTestId }) => {
  // hooks
  const dispatch = useDispatch();
  const { t } = useTranslation();

  // state
  const [loading, setLoading] = useState(false);

  // variables
  const isOpened = useSelector<AppState, boolean>(
    state => state.confirmModal.open
  );

  const modalData = useSelector<AppState, ModalData>(
    state => state.confirmModal.data || {}
  );

  // handlers
  const handleClose = () => {
    dispatch(confirmModalActions.close());
  };

  const handleCancel = () => {
    if (isFunction(modalData.cancelCallback)) {
      modalData.cancelCallback();
    }
    handleClose();
  };

  const handleConfirm = async () => {
    try {
      if (isFunction(modalData.confirmCallback)) {
        setLoading(true);
        await modalData.confirmCallback();
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
      handleClose();
    }
  };

  useEffect(() => {
    const hideModalClassMd = 'modal-md-hidden';
    const hideModalClassLg = 'modal-lg-hidden';

    if (isOpened) {
      const mdModals = document.querySelectorAll(
        '.dls-modal-root .dls-modal-dialog.window-md'
      );
      const lgModals = document.querySelectorAll(
        '.dls-modal-root .dls-modal-dialog.window-lg'
      );
      mdModals.forEach(node => node.classList.add(hideModalClassMd));
      lgModals.forEach(node => node.classList.add(hideModalClassLg));
    } else {
      const hiddenMdModals = document.querySelectorAll(`.${hideModalClassMd}`);
      hiddenMdModals.forEach(node => node.classList.remove(hideModalClassMd));
      const hiddenLgModals = document.querySelectorAll(`.${hideModalClassLg}`);
      hiddenLgModals.forEach(node => node.classList.remove(hideModalClassLg));
    }
  }, [isOpened]);

  if (!isOpened) return null;

  const title =
    typeof modalData.title === 'string'
      ? t(modalData.title as string, {
          count: modalData?.count
        })
      : modalData.title;

  const body =
    typeof modalData.body === 'string'
      ? t(modalData.body as string, {
          count: modalData?.count
        })
      : modalData.body;

  const disabledConfirm = !!modalData.disabledConfirm;

  const cancelText = t(modalData.cancelText);
  const confirmText = t(modalData.confirmText);

  return (
    <Modal
      {...modalData?.size}
      loading={loading}
      show={isOpened}
      dataTestId={`${dataTestId}-confirm-modal`}
    >
      <ModalHeader border dataTestId={`${dataTestId}-confirm-modal-header`}>
        <ModalTitle dataTestId={`${dataTestId}-confirm-modal-title`}>
          <div>{title}</div>
        </ModalTitle>
        <Button
          onClick={handleClose}
          variant="icon-secondary"
          dataTestId={`${dataTestId}-confirm-modal-close-btn`}
        >
          <Icon name="close" size="5x" />
        </Button>
      </ModalHeader>
      <ModalBody dataTestId={`${dataTestId}-confirm-modal-body`}>
        {body}
      </ModalBody>
      <ModalFooter>
        <Button
          id="confirm-modal-cancel-btn"
          dataTestId={`${dataTestId}-confirm-modal-cancel-btn`}
          variant="secondary"
          onClick={handleCancel}
        >
          {cancelText}
        </Button>
        <Button
          id="confirm-modal-confirm-btn"
          dataTestId={`${dataTestId}-confirm-modal-confirm-btn`}
          disabled={disabledConfirm}
          variant={modalData.variant}
          onClick={handleConfirm}
        >
          {confirmText}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ConfirmModal;
