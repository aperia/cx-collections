export interface ModalData {
  title?: React.ReactNode;
  body?: React.ReactNode;
  confirmText?: string;
  cancelText?: string;
  confirmCallback?: Function;
  cancelCallback?: Function;
  disabledConfirm?: boolean;
  variant?: 'primary' | 'secondary' | 'danger';
  size?: {
    sm?: boolean;
    xs?: boolean;
    lg?: boolean;
    md?: boolean;
  };
  count?: number;
}

export interface ConfirmModalState {
  open: boolean;
  data?: ModalData;
}
