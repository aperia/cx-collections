import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ConfirmModalState, ModalData } from '../types';

const initialState: ConfirmModalState = { open: false };

const defaultModalData: ModalData = {
  title: '',
  body: '',
  cancelText: 'txt_cancel',
  confirmText: 'txt_submit',
  variant: 'danger',
  size: {
    xs: true
  }
};

const { actions, reducer } = createSlice({
  name: 'confirmModal',
  initialState,
  reducers: {
    open: (draftState, action: PayloadAction<ModalData>) => {
      draftState.open = true;
      draftState.data = { ...defaultModalData, ...action.payload };
    },
    close: draftState => {
      draftState.open = false;
    }
  }
});

const allActions = {
  ...actions
};

export { allActions as confirmModalActions, reducer as confirmModal };
