import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { confirmModalActions } from './index';

const store = createStore(rootReducer);

const state = store.getState();

describe('confirmModal reducer', () => {
  it('should be open', () => {
    const modalData = { title: 'test', body: 'test' };
    state.confirmModal = {
      open: false
    };
    const action = confirmModalActions.open(modalData);
    const actual = rootReducer(state, action);

    expect(actual.confirmModal.open).toEqual(true);
  });

  it('should be close', () => {
    const action = confirmModalActions.close();
    const actual = rootReducer(state, action);

    expect(actual.confirmModal.open).toEqual(false);
    expect(actual.confirmModal.data).toBeFalsy();
  });
});
