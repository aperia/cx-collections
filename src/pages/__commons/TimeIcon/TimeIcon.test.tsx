import { render, queryHelpers } from '@testing-library/react';
import React from 'react';

import TimeIcon from '.';

describe('TimeIcon Component', () => {
  it('check color', () => {
    const color = '#333333';
    const { container } = render(<TimeIcon color={color} />);
    expect(
      queryHelpers.queryByAttribute('fill', container, color)
    ).toBeInTheDocument();
  });
});
