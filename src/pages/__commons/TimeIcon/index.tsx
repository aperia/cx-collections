import React from 'react';

export interface TimeIconProps {
  color?: string;
}

const TimeIcon: React.FC<TimeIconProps> = ({ color }) => {
  const path =
    'M12,0 C18.627417,0 24,5.372583 24,12 C24,18.627417 18.627417,24 12,24 C5.372583,24 0,18.627417 0,12 C0,5.372583 5.372583,0 12,0 Z M12,5.33333333 C12.506261,5.33333333 12.9246546,5.70953851 12.9908712,6.19763926 L13,6.33333333 L13,11.5626667 L17.0095587,15.2710076 C17.3781595,15.6119105 17.4320811,16.1657803 17.1571008,16.5682953 L17.0647254,16.6841448 C16.7238225,17.0527456 16.1699527,17.1066671 15.7674377,16.8316868 L15.6515882,16.7393114 L11,12.4372593 L11,6.33333333 C11,5.78104858 11.4477153,5.33333333 12,5.33333333 Z';
  return (
    <svg
      width="24px"
      height="24px"
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
        <g transform="translate(-111.000000, -38.000000)">
          <g transform="translate(111.000000, 38.000000)">
            <mask fill="white">
              <path d={path} />
            </mask>
            <g fill={color}>
              <path d={path} />
            </g>
          </g>
        </g>
      </g>
    </svg>
  );
};

export default TimeIcon;
