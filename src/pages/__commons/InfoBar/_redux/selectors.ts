import { InfoBarSection } from '.';

export const selectPinned: CustomStoreIdSelector<InfoBarSection | null> = (
  states,
  storeId
) => {
  return states.infoBar[storeId]?.pinned as InfoBarSection;
};

export const selectIsExpand: CustomStoreIdSelector<boolean | null> = (
  states,
  storeId
) => {
  return states.infoBar[storeId]?.isExpand as boolean;
};

export const selectRecover: CustomStoreIdSelector<
  InfoBarSection | undefined
> = (states, storeId) => {
  return states.infoBar[storeId]?.recover as InfoBarSection;
};
