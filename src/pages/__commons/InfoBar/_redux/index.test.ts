import { actionsInfoBar, InfoBarSection, reducer } from './index';
import { storeId } from 'app/test-utils';

const initialState = {
  [storeId]: {
    pinned: null,
    isExpand: false,
    recover: null
  }
};

describe('InfoBar > reducers', () => {
  it('updatePinned', () => {
    const params = {
      storeId,
      pinned: InfoBarSection.AccountInformation
    };
    // invoke
    const state = reducer(initialState, actionsInfoBar.updatePinned(params));

    // expect
    expect(state[storeId].pinned).toEqual(InfoBarSection.AccountInformation);
  });

  it('updateIsExpand', () => {
    const params = {
      storeId,
      isExpand: true
    };
    // invoke
    const state = reducer(initialState, actionsInfoBar.updateIsExpand(params));

    // expect
    expect(state[storeId].isExpand).toEqual(true);
  });

  it('updateIsExpand', () => {
    const params = {
      storeId,
      recover: InfoBarSection.CardholderMaintenance
    };
    // invoke
    const state = reducer(initialState, actionsInfoBar.updateRecover(params));

    // expect
    expect(state[storeId].recover).toEqual(
      InfoBarSection.CardholderMaintenance
    );
  });

  it('removeInfoBar', () => {
    const params = {
      storeId
    };
    // invoke
    const state = reducer(initialState, actionsInfoBar.removeInfoBar(params));

    // expect
    expect(state[storeId]).toEqual(undefined);
  });
});
