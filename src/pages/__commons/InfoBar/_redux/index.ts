import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface InfoBarPayload {
  storeId: string;
  pinned?: InfoBarSection | null;
  isExpand?: boolean | null;
  recover?: InfoBarSection | null;
}

export enum InfoBarSection {
  CollectionForm = 'Collection Summary',
  Memos = 'Memos',
  AccountInformation = 'Account Information',
  CollectionInformation = 'Collection Information',
  CardholderMaintenance = 'Cardholder Maintenance',
  ClientContactInfo = 'Client Contact Info',
  websiteLauncher = 'Website Launcher'
}

export interface InfoBarState {
  pinned?: InfoBarSection | null;
  isExpand?: boolean | null;
  recover?: InfoBarSection | null;
}

const { actions, reducer } = createSlice({
  name: 'infoBar',
  initialState: {} as Record<string, InfoBarState>,
  reducers: {
    removeInfoBar: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    updatePinned: (draftState, action: PayloadAction<InfoBarPayload>) => {
      const { storeId, pinned } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        pinned: pinned!
      };
    },
    updateIsExpand: (draftState, action: PayloadAction<InfoBarPayload>) => {
      const { storeId, isExpand } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        isExpand: isExpand!
      };
    },
    updateRecover: (draftState, action: PayloadAction<InfoBarPayload>) => {
      const { storeId, recover } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        recover: recover!
      };
    }
  }
});

const allActions = {
  ...actions
};

export { allActions as actionsInfoBar, reducer };
