import React from 'react';
import { screen } from '@testing-library/react';

// utils
import {
  queryAllByClass,
  queryByClass,
  renderMockStoreId,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';

// components
import InfoBar from 'pages/__commons/InfoBar';
import { actionsInfoBar, InfoBarSection } from './_redux';

// Redux
import { schedulePaymentActions } from 'pages/MakePayment/SchedulePayment/_redux/reducers';
import { letterAction } from 'pages/Letters/_redux/reducers';
import { mockActionCreator } from 'app/test-utils';
import * as entitlementsHelpers from 'app/entitlements/helpers';
import { PERMISSIONS } from 'app/entitlements/constants';
import userEvent from '@testing-library/user-event';
import { MAKE_PAYMENT_FROM } from 'pages/MakePayment/SchedulePayment/types';

jest.mock('app/hooks', () => ({
  ...jest.requireActual('app/hooks'),
  useDesktopScreenSize: () => true
}));

jest.mock('app/_libraries/_dls/hooks/useScreenType', () => () => 0);

jest.mock('pages/Memos', () => () => <div>Memos</div>);
jest.mock('pages/CollectionForm', () => () => <div>CollectionForm</div>);
jest.mock('pages/AccountDetails/AccountInformation', () => () => (
  <div>AccountInformation</div>
));
jest.mock('pages/AccountDetails/AccountCollection', () => () => (
  <div>AccountCollection</div>
));
jest.mock('pages/AccountDetails/CardHolderMaintenance', () => () => (
  <div>CardholderMaintenance</div>
));

jest.mock('pages/AccountDetails/ClientContactInfo', () => () => (
  <div>ClientContactInfo</div>
));

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<InfoBar primaryName={'primaryName'} />, {
    initialState
  });
};

const actionsInfoBarSpy = mockActionCreator(actionsInfoBar);

beforeEach(() => {
  jest.useFakeTimers();
  jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
});

describe('Render', () => {
  it('Render UI', () => {
    renderMockStoreId(<InfoBar primaryName={'primaryName'} />);
    expect(screen.getByText('txt_make_payment')).toBeInTheDocument();
  });

  it('Render UI > pin CollectionForm', () => {
    renderWrapper({
      infoBar: { [storeId]: { pinned: InfoBarSection.CollectionForm } }
    });
    expect(screen.getByText('CollectionForm')).toBeInTheDocument();
  });

  it('Render UI > pin Memos', () => {
    renderWrapper({ infoBar: { [storeId]: { pinned: InfoBarSection.Memos } } });
    expect(screen.getByText('Memos')).toBeInTheDocument();
  });

  it('Render UI > pin AccountInformation', () => {
    renderWrapper({
      infoBar: { [storeId]: { pinned: InfoBarSection.AccountInformation } }
    });
    expect(screen.getByText('AccountInformation')).toBeInTheDocument();
  });

  it('Render UI > pin AccountCollection', () => {
    renderWrapper({
      infoBar: { [storeId]: { pinned: InfoBarSection.CollectionInformation } }
    });
    expect(screen.getByText('AccountCollection')).toBeInTheDocument();
  });

  it('Render UI > pin CardholderMaintenance', () => {
    renderWrapper({
      infoBar: { [storeId]: { pinned: InfoBarSection.CardholderMaintenance } }
    });
    expect(screen.getByText('CardholderMaintenance')).toBeInTheDocument();
  });

  it('Render UI > pin ClientContactInfo', () => {
    renderWrapper({
      infoBar: { [storeId]: { pinned: InfoBarSection.ClientContactInfo } }
    });
    expect(screen.getByText('ClientContactInfo')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('click Make Payment', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    jest
      .spyOn(schedulePaymentActions, 'toggleModal')
      .mockImplementation(mockAction);
    const { wrapper } = renderWrapper({});
    const baseElement = wrapper.baseElement as HTMLElement;

    const makePaymentBtn = queryAllByClass(
      baseElement,
      'infobar-dropdown-item'
    )[0];

    userEvent.click(makePaymentBtn);

    expect(mockAction).toBeCalledWith({
      from: MAKE_PAYMENT_FROM.QUICK_ACTION,
      storeId
    });
  });

  it('click Send Letter', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    jest.spyOn(letterAction, 'updateOpenModal').mockImplementation(mockAction);
    const { wrapper } = renderWrapper({});
    const baseElement = wrapper.baseElement as HTMLElement;

    const sendLetterBtn = queryAllByClass(
      baseElement,
      'infobar-dropdown-item'
    )[1];

    userEvent.click(sendLetterBtn);

    expect(mockAction).toBeCalledWith({
      storeId,
      openModal: true
    });
  });

  it('handleExpand', () => {
    const mockAction: jest.Mock = actionsInfoBarSpy('updateIsExpand');
    const { wrapper } = renderWrapper({});
    const baseElement = wrapper.baseElement as HTMLElement;

    const makePaymentBtn = queryByClass(baseElement, /css-tooltip-wrapper/);

    mockAction.mockClear();
    // collapse
    makePaymentBtn!.click();
    // expand
    makePaymentBtn!.click();

    expect(mockAction.mock.calls).toEqual([
      [{ storeId, isExpand: false }],
      [{ storeId, isExpand: true }]
    ]);
  });

  it('handleExpand with timeout', () => {
    const mockAction: jest.Mock = actionsInfoBarSpy('updateIsExpand');
    const { wrapper } = renderWrapper({});
    const baseElement = wrapper.baseElement as HTMLElement;

    const makePaymentBtn = queryByClass(baseElement, /css-tooltip-wrapper/);

    jest.runOnlyPendingTimers();

    mockAction.mockClear();
    // collapse
    makePaymentBtn!.click();
    // expand
    makePaymentBtn!.click();

    expect(mockAction.mock.calls).toBeTruthy();
  });

  it('should hide Make Payment when no permission to view SchedulePayment and ACHPayment', () => {
    jest
      .spyOn(entitlementsHelpers, 'checkPermission')
      .mockImplementation(permission => {
        if (
          permission === PERMISSIONS.MAKE_PAYMENT_SCHEDULE_PAYMENT ||
          permission === PERMISSIONS.MAKE_PAYMENT_MAKE_ON_DEMAND_ACH
        )
          return false;

        return true;
      });
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    jest.spyOn(letterAction, 'updateOpenModal').mockImplementation(mockAction);
    const { wrapper } = renderWrapper({});
    const baseElement = wrapper.baseElement as HTMLElement;

    const sendLetterBtn = queryAllByClass(
      baseElement,
      'infobar-dropdown-item'
    )[0];

    userEvent.click(sendLetterBtn);

    expect(mockAction).toBeCalledWith({
      storeId,
      openModal: true
    });
  });

  it('should hide Make Payment when no permission to view SchedulePayment and ACHPayment', () => {
    jest
      .spyOn(entitlementsHelpers, 'checkPermission')
      .mockImplementation(() => false);

    const { wrapper } = renderWrapper({});

    expect(wrapper.getByText('CollectionForm')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_no_sections_pinner_here')
    ).toBeInTheDocument();
  });
});
