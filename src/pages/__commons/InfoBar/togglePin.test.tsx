import React from 'react';
import TogglePin from './TogglePin';
import { queryByClass, renderMockStoreId } from 'app/test-utils';
import {
  mockInnerHeight,
  mockInnerWidth
} from 'app/test-utils/mocks/mockProperty';

describe('Test Toggle Pin', () => {
  beforeEach(() => {
    jest.clearAllTimers();
  });
  it('Should have icon unpin', () => {
    jest.useFakeTimers();
    mockInnerWidth(1280);
    mockInnerHeight(1024);

    const { wrapper } = renderMockStoreId(
      <TogglePin isPinned onToggle={() => {}} />
    );
    jest.runAllTimers();
    const queryIconUnpin = queryByClass(wrapper.container, 'icon icon-unpin');
    expect(queryIconUnpin).toBeInTheDocument();
  });

  it('Should have icon-pin', () => {
    jest.useFakeTimers();
    mockInnerWidth(1024);
    mockInnerHeight(768);
    const { wrapper } = renderMockStoreId(
      <TogglePin isPinned={false} onToggle={() => {}} />
    );
    jest.runAllTimers();
    const queryIconUnpin = queryByClass(wrapper.container, 'icon icon-pin');
    expect(queryIconUnpin).toBeInTheDocument();
  });

  it('Should have click onToggle', () => {
    const onToggle = jest.fn();
    jest.useFakeTimers();
    mockInnerWidth(1024);
    mockInnerHeight(768);
    const { wrapper } = renderMockStoreId(
      <TogglePin isPinned={false} onToggle={onToggle} />
    );
    jest.runAllTimers();
    const queryIconUnpin = queryByClass(wrapper.container, 'icon icon-pin');
    queryIconUnpin!.click();
    expect(onToggle).toHaveBeenCalled();
  });

  it('Should have icon unpin', () => {
    jest.useFakeTimers();
    mockInnerWidth(1280);
    mockInnerHeight(1024);

    const { wrapper } = renderMockStoreId(
      <TogglePin isPinned onToggle={() => {}} />
    );
    jest.runAllTimers();
    const queryIconUnpin = queryByClass(wrapper.container, 'icon icon-unpin');
    expect(queryIconUnpin).toBeInTheDocument();
  });
});
