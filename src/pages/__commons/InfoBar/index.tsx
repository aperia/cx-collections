import React, { Fragment, useEffect, useLayoutEffect, useMemo } from 'react';

// components
import {
  InfoBar as InfoBarBS,
  InfoBarDropdown,
  InfoBarDropDownItem,
  InfoBarItem,
  InfoBarProps
} from 'app/_libraries/_dls/components';
import Memo from 'pages/Memos';
import MakePayment from 'pages/MakePayment';
import AccountCollection from 'pages/AccountDetails/AccountCollection';
import AccountInformation from 'pages/AccountDetails/AccountInformation';
import CollectionFormModal from 'pages/CollectionForm/CollectionFormModal';
import CollectionFormResults from 'pages/CollectionForm';
import Letter from 'pages/Letters';
import CardHolderMaintenance from 'pages/AccountDetails/CardHolderMaintenance';
import PhoneAndAddressList from 'pages/AccountDetails/ClientContactInfo';
import WebsiteLauncher from 'pages/AccountDetails/WebsiteLauncher';

// Hooks
import { batch, useDispatch } from 'react-redux';
import { useAccountDetail, CustomStoreIdProvider } from 'app/hooks';
import { useScreenType, useTranslation } from 'app/_libraries/_dls/hooks';

// Constants
import { I18N_INFO_BAR, quickActionItemConfig } from './constants';

// Redux
import { schedulePaymentActions } from 'pages/MakePayment/SchedulePayment/_redux/reducers';
import { letterAction } from 'pages/Letters/_redux/reducers';
import {
  selectIsExpand,
  selectPinned,
  selectRecover
} from './_redux/selectors';
import { actionsInfoBar, InfoBarSection } from './_redux';
import EmptySection from './EmptySection';
import { ScreenType } from 'app/_libraries/_dls/hooks/useScreenType/types';
import { useStoreIdSelector } from 'app/hooks/useStoreIdSelector';
import isUndefined from 'lodash.isundefined';
import { I18N_CARD_MAINTENANCE } from 'app/constants/i18n';
import { I18N_ACCOUNT_COLLECTION } from 'pages/AccountDetails/AccountCollection/constants';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';
import { MAKE_PAYMENT_FROM } from 'pages/MakePayment/SchedulePayment/types';

export interface InfoBarDropDownItemProps extends InfoBarDropDownItem {
  dataTestId?: string;
}

interface Props {
  primaryName: string;
}

const InfoBar: React.FC<Props> = ({ primaryName }) => {
  const { makePayment, sendLetter } = quickActionItemConfig;
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();

  const pinned = useStoreIdSelector<InfoBarSection>(selectPinned);
  const isExpand = useStoreIdSelector<boolean>(selectIsExpand);
  const recover = useStoreIdSelector<InfoBarSection>(selectRecover);
  const pinnedRecover = isExpand && recover ? recover : pinned;
  const testId = 'account-detail-infobar';

  // InfoBar tablet portrait logic
  const screenType = useScreenType();

  const quickActionItems: InfoBarDropDownItemProps[] = useMemo(() => {
    const allItems: InfoBarDropDownItemProps[] = [];

    if (
      checkPermission(PERMISSIONS.MAKE_PAYMENT_SCHEDULE_PAYMENT, storeId) ||
      checkPermission(PERMISSIONS.MAKE_PAYMENT_MAKE_ON_DEMAND_ACH, storeId)
    ) {
      allItems.push({
        dataTestId: genAmtId(`make-payment_${testId}`, 'quick-actions', ''),
        text: t(makePayment.text),
        icon: { name: 'billing' },
        onClick: () =>
          dispatch(
            schedulePaymentActions.toggleModal({
              storeId,
              from: MAKE_PAYMENT_FROM.QUICK_ACTION
            })
          )
      });
    }

    if (
      checkPermission(PERMISSIONS.LETTER_VIEW_LETTER_LIST, storeId) ||
      checkPermission(PERMISSIONS.LETTER_VIEW_PENDING_LIST, storeId)
    ) {
      allItems.push({
        dataTestId: genAmtId(`send-letter_${testId}`, 'quick-actions', ''),
        text: t(sendLetter.text),
        icon: { name: 'email' },
        onClick: () => {
          dispatch(letterAction.updateOpenModal({ storeId, openModal: true }));
        }
      });
    }

    return allItems;
  }, [dispatch, makePayment.text, sendLetter.text, storeId, t]);

  const items: InfoBarItem[] = useMemo(() => {
    const allItems = [
      {
        id: 'Quick Actions',
        name: t(I18N_INFO_BAR.QUICK_ACTIONS),
        icon: { name: 'quick-action' },
        mode: 'dropdown',
        separator: true,
        component: ({ ...props }) => {
          return (
            <InfoBarDropdown
              header={t(I18N_INFO_BAR.QUICK_ACTIONS)}
              items={quickActionItems}
              dataTestId={genAmtId(testId, 'quick-actions', '')}
              {...props}
            />
          );
        }
      },
      {
        id: InfoBarSection.CollectionForm,
        name: t(I18N_INFO_BAR.COLLECTION_SUMMARY),
        icon: { name: 'file' },
        component: CollectionFormResults
      },
      {
        id: InfoBarSection.Memos,
        name: t(I18N_INFO_BAR.MEMOS),
        icon: { name: 'comment' },
        component: () => {
          const customStoreId = `${storeId}__infoBar`;
          return (
            <CustomStoreIdProvider value={{ customStoreId }}>
              <Memo shouldRenderPinnedIcon={true} />
            </CustomStoreIdProvider>
          );
        }
      },
      {
        id: InfoBarSection.AccountInformation,
        name: t(I18N_INFO_BAR.ACCOUNT_INFORMATION),
        icon: { name: 'client-communications' },
        component: props => (
          <AccountInformation primaryName={primaryName} {...props} />
        )
      },
      {
        id: InfoBarSection.CollectionInformation,
        name: t(I18N_ACCOUNT_COLLECTION.COLLECTION_INFORMATION),
        icon: { name: 'collection-information' },
        component: AccountCollection
      },
      {
        id: InfoBarSection.CardholderMaintenance,
        name: t(I18N_CARD_MAINTENANCE.CARDHOLDER_MAINTENANCE),
        icon: { name: 'cardholder' },
        component: CardHolderMaintenance
      },
      {
        id: InfoBarSection.ClientContactInfo,
        name: t(I18N_INFO_BAR.CLIENT_CONTACT_INFO),
        icon: { name: 'client-contact' },
        component: PhoneAndAddressList
      },
      {
        id: InfoBarSection.websiteLauncher,
        name: t(I18N_INFO_BAR.WEBSITE_LAUNCHER),
        icon: { name: 'globe' },
        component: WebsiteLauncher
      }
    ] as InfoBarItem[];

    return allItems.filter(item => {
      if (item.id === 'Quick Actions') return quickActionItems.length > 0;

      if (item.id === InfoBarSection.CollectionInformation)
        return checkPermission(
          PERMISSIONS.COLLECTION_INFORMATION_VIEW,
          storeId
        );
      if (item.id === InfoBarSection.Memos)
        return checkPermission(PERMISSIONS.VIEW_MEMO, storeId);
      if (item.id === InfoBarSection.websiteLauncher)
        return checkPermission(PERMISSIONS.WEBSITE_LAUNCHER_VIEW, storeId);
      if (item.id === InfoBarSection.AccountInformation)
        return checkPermission(PERMISSIONS.ACCOUNT_INFORMATION_VIEW, storeId);
      if (item.id === InfoBarSection.ClientContactInfo)
        return checkPermission(
          PERMISSIONS.COLLECTION_CONTACT_INFO_VIEW,
          storeId
        );
      if (item.id === InfoBarSection.CardholderMaintenance)
        return checkPermission(
          PERMISSIONS.CARDHOLDER_MAINTENANCE_VIEW,
          storeId
        );
      return true;
    });
  }, [primaryName, quickActionItems, storeId, t]);

  const handleExpand = () =>
    dispatch(actionsInfoBar.updateIsExpand({ storeId, isExpand: true }));
  const handleCollapse = () =>
    dispatch(actionsInfoBar.updateIsExpand({ storeId, isExpand: false }));

  // InfoBar pinned default is CollectionForm
  useLayoutEffect(() => {
    if (!isUndefined(pinned)) return;

    dispatch(
      actionsInfoBar.updatePinned({
        storeId,
        pinned: InfoBarSection.CollectionForm
      })
    );
  }, [dispatch, pinned, storeId]);

  // InfoBar recover logic
  useEffect(() => {
    let timeoutId: ReturnType<typeof setTimeout>;

    if (!isExpand && pinned) {
      dispatch(actionsInfoBar.updateRecover({ storeId, recover: pinned }));
      timeoutId = setTimeout(() => {
        dispatch(actionsInfoBar.updatePinned({ storeId, pinned: null }));
      }, 300);
    }

    if (isExpand && recover) {
      batch(() => {
        dispatch(actionsInfoBar.updateRecover({ storeId, recover: null }));
        dispatch(actionsInfoBar.updatePinned({ storeId, pinned: recover }));
      });
    }

    return () => clearTimeout(timeoutId);
  }, [dispatch, isExpand, pinned, recover, storeId]);

  // InfoBar tablet portrait logic
  useLayoutEffect(() => {
    dispatch(
      actionsInfoBar.updateIsExpand({
        storeId,
        isExpand: screenType !== ScreenType.TABLET_PORTRAIT
      })
    );
  }, [dispatch, screenType, storeId]);

  return (
    <Fragment>
      <InfoBarBS
        items={items}
        pinned={pinnedRecover as InfoBarProps['pinned']}
        expand={isExpand}
        onExpand={handleExpand}
        onCollapse={handleCollapse}
        emptyElement={<EmptySection />}
        dataTestId={testId}
        collapseText={t('txt_collapse_sidebar')}
        expandText={t('txt_expand_sidebar')}
      />
      <MakePayment />
      <CustomStoreIdProvider
        value={{ customStoreId: `${storeId}__collectionForm` }}
      >
        <CollectionFormModal />
      </CustomStoreIdProvider>
      <Letter />
    </Fragment>
  );
};

export default InfoBar;
