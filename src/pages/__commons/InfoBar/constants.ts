export const quickActionItemConfig = {
  makePayment: {
    text: 'txt_make_payment'
  },
  sendLetter: {
    text: 'txt_send_letter'
  }
};

export const I18N_INFO_BAR = {
  COLLECTION_SUMMARY: 'txt_collection_summary',
  MEMOS: 'txt_memo',
  ACCOUNT_INFORMATION: 'txt_account_information',
  COLLECTION_INFORMATION: 'txt_collection_information',
  CARDHOLDER_MAINTENANCE: 'txt_cardholder_maintenance',
  CLIENT_CONTACT_INFO: 'txt_client_contact_info',
  WEBSITE_LAUNCHER: 'txt_website_launcher',
  QUICK_ACTIONS: 'txt_quick_actions'
};
