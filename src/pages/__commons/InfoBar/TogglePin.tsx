import React from 'react';

import {
  Button,
  Icon,
  Tooltip,
  TooltipProps
} from 'app/_libraries/_dls/components';
import { useScreenType, useTranslation } from 'app/_libraries/_dls/hooks';
import { ScreenType } from 'app/_libraries/_dls/hooks/useScreenType/types';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface TogglePinProps extends TooltipProps {
  dataTestId?: string;
  isPinned: boolean;
  onToggle: () => void;
}

const TogglePin: React.FC<TogglePinProps> = ({
  dataTestId,
  isPinned,
  onToggle,
  ...props
}) => {
  const { t } = useTranslation();
  const screenType = useScreenType();
  const shouldRender =
    screenType === ScreenType.DESKTOP ||
    screenType === ScreenType.TABLET_LANDSCAPE;

  const testId = genAmtId(dataTestId!, 'toggle-pin-unpin', 'TogglePin');

  return shouldRender ? (
    <Tooltip
      element={t(isPinned ? 'txt_unpin' : 'txt_pin_to_right')}
      placement="top"
      variant="primary"
      dataTestId={testId}
      {...props}
    >
      <Button variant="icon-secondary" onClick={onToggle} dataTestId={testId}>
        <Icon name={isPinned ? 'unpin' : 'pin'} dataTestId={testId} />
      </Button>
    </Tooltip>
  ) : null;
};

export default TogglePin;
