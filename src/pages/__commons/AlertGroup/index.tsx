import React, { useState } from 'react';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { useStoreIdSelector } from 'app/hooks';
import { Button } from 'app/_libraries/_dls/components';
import isEmpty from 'lodash.isempty';
import { useTranslation } from 'react-i18next';
import Alerts from './Alerts';
import AllAlertsModal from './AllAlertsModal';
import { getAlerts } from './_redux/selector';
import { NUMBER_OF_ALERT_TO_SHOW } from './constant';
import { genAmtId } from 'app/_libraries/_dls/utils';

interface AlertGroupProps {
  dataTestId?: string;
}
const AlertGroup: React.FC<AlertGroupProps> = ({ dataTestId }) => {
  const { t } = useTranslation();

  const alerts = useStoreIdSelector<Record<string, string>[]>(getAlerts);

  const [showAll, setShowAll] = useState(false);
  const onShowAllAlerts = (): void => {
    setShowAll(true);
  };

  const onCloseAllAlerts = (): void => {
    setShowAll(false);
  };

  const numberRemainAlerts = alerts.length - NUMBER_OF_ALERT_TO_SHOW;

  if (isEmpty(alerts)) {
    return null;
  }

  return (
    <>
      <div className="group-alert mb-8">
        <Alerts
          alerts={alerts.slice(0, NUMBER_OF_ALERT_TO_SHOW)}
          dataTestId={genAmtId(dataTestId!, 'alert-group_alerts', 'AlertGroup')}
        />
        {numberRemainAlerts > 0 && (
          <Button
            onClick={onShowAllAlerts}
            variant="outline-primary"
            dataTestId={genAmtId(dataTestId!, 'alert-group_more-alerts-btn', 'AlertGroup')}
          >
            {t(I18N_COMMON_TEXT.ALERT, { count: numberRemainAlerts })}
          </Button>
        )}
        {showAll && (<AllAlertsModal
          onClose={onCloseAllAlerts}
          dataTestId={genAmtId(dataTestId!, 'alert-group_alerts-modal', 'AlertGroup')}
        />)}
      </div>
    </>
  );
};

export default AlertGroup;
