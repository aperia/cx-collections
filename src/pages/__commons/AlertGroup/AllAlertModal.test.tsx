import React from 'react';
import { fireEvent, screen } from '@testing-library/react';

// hooks
import { AccountDetailProvider } from 'app/hooks';

// helpers
import { renderMockStore, storeId } from 'app/test-utils';
import AlertGroup from '.';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

import * as selectors from './_redux/selector';
import { AllAlertsModalProps } from './AllAlertsModal';

jest.mock('./AllAlertsModal', () => {
  return (props: AllAlertsModalProps) => (
    <>
      <div data-testid="AllAlertsModal"></div>
      <input data-testid="onClose" onChange={props.onClose} />
    </>
  );
});

const initialState: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      lastFollowUpData: {
        callResultType: CALL_RESULT_CODE.DECEASE_PENDING
      }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <AlertGroup />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('Render view', () => {
  it('should able to close', () => {
    jest.spyOn(selectors, 'getAlerts').mockReturnValue([
      {
        message:
          'I18N_COLLECTION_FORM.ACCOUNT_STATUS_DECEASED_STATUS_PENDING I18N_COLLECTION_FORM.ACCOUNT_STATUS_DECEASED_STATUS_PENDING',
        severity: 'warning'
      },
      {
        message: 'I18N_COLLECTION_FORM.ACCOUNT_STATUS_DECEASED_STATUS_PENDING',
        severity: 'warning'
      },
      {
        message: 'I18N_COLLECTION_FORM.ACCOUNT_STATUS_DECEASED_STATUS_PENDING',
        severity: 'warning'
      },
      {
        message: 'I18N_COLLECTION_FORM.ACCOUNT_STATUS_DECEASED_STATUS_PENDING',
        severity: 'warning'
      },
      {
        message: 'q',
        severity: 'warning'
      }
    ]);
    renderWrapper(initialState);

    const showAllButton = screen.getByText(I18N_COMMON_TEXT.ALERT);
    showAllButton.click();

    fireEvent.change(screen.getByTestId('onClose'), {
      target: { value: 'dataKy' }
    });

    expect(screen.queryByTestId('AllAlertsModal')).toBeNull();
  });
});
