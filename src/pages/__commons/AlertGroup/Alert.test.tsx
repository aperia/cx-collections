import React from 'react';
import { render } from '@testing-library/react';

import Alert from './Alert';
import { ALERT_MAX_WIDTH } from './constant';

jest.mock('react-i18next', () => ({
  useTranslation: () => ({ t: (text: string) => text })
}));

describe('Render', () => {
  it('should render when width is ALERT_MAX_WIDTH', () => {
    Object.defineProperty(HTMLElement.prototype, 'offsetWidth', {
      writable: true,
      value: ALERT_MAX_WIDTH
    });

    const { container } = render(<Alert />);

    expect(container.querySelector('.group-alert-item')).toBeInTheDocument();
  });
});
