import React from 'react';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { useTranslation } from 'react-i18next';
import Alerts from './Alerts';
import { useStoreIdSelector } from 'app/hooks';
import { selectAlerts } from './_redux/selector';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface AllAlertsModalProps {
  dataTestId?: string;
  onClose: () => void;
}

const AllAlertsModal: React.FC<AllAlertsModalProps> = ({
  dataTestId,
  onClose
}) => {
  const { t } = useTranslation();

  const alerts = useStoreIdSelector<Record<string, string>[]>(selectAlerts);
  const testId = `${dataTestId}_all-alerts-modal`;

  return (
    <Modal md show dataTestId={genAmtId(testId, '', '')}>
      <ModalHeader
        border
        closeButton
        onHide={onClose}
        dataTestId={genAmtId(testId, 'header', '')}
      >
        <ModalTitle dataTestId={genAmtId(testId, 'header-title', '')}>
          {t(I18N_COMMON_TEXT.ALL_ALERTS)}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <div className="group-alert">
          <Alerts
            alerts={alerts}
            dataTestId={genAmtId(testId, 'body_alerts', '')}
          />
        </div>
      </ModalBody>
      <ModalFooter>
        <Button
          id="confirm-modal-cancel-btn"
          variant="secondary"
          onClick={onClose}
          dataTestId={genAmtId(testId, 'footer_close-btn', '')}
        >
          {t(I18N_COMMON_TEXT.CLOSE)}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default AllAlertsModal;
