import React, { useState, useCallback } from 'react';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { InlineMessage, Popover } from 'app/_libraries/_dls/components';
import { useTranslation } from 'react-i18next';
import { ALERT_MAX_WIDTH } from './constant';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface AlertProps {
  severity?: string;
  dataTestId?: string;
}

const Alert: React.FC<AlertProps> = ({ children, severity, dataTestId }) => {
  const [width, setWidth] = useState<number>(0);

  const { t } = useTranslation();

  /**
   * Get offsetWidth to show ellipsis
   */
  const wrapperRef = useCallback(ref => {
    if (!ref) return;

    setWidth(ref.offsetWidth);
  }, []);

  return (
    <div ref={wrapperRef} className="group-alert-item">
      <InlineMessage variant={severity} dataTestId={dataTestId}>
        <div
          className="text-message"
          data-testid={genAmtId(dataTestId!, 'alert_message', 'Alert')}
        >
          {children}
        </div>
        {width === ALERT_MAX_WIDTH && (
          <div className="px-8">
            <Popover
              placement="bottom-end"
              size="md"
              element={
                <div
                  className="my-8"
                  data-testid={genAmtId(
                    dataTestId!,
                    'alert_full-message',
                    'Alert'
                  )}
                >
                  {children}
                </div>
              }
              dataTestId={dataTestId}
            >
              <span
                className="link"
                data-testid={genAmtId(
                  dataTestId!,
                  'alert_view-full-btn',
                  'Alert'
                )}
              >
                {t(I18N_COMMON_TEXT.VIEW)}
              </span>
            </Popover>
          </div>
        )}
      </InlineMessage>
    </div>
  );
};

export default Alert;
