export const ALERT_MAX_WIDTH = 384;
export const NUMBER_OF_ALERT_TO_SHOW = 4;
export const COMPLETE = 'COMPLETE';
export const IN_PROGRESS = 'IN_PROGRESS';
