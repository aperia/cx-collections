import React from 'react';
import Alert from './Alert';
import { useAccountDetail } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface AllAlertsProps {
  alerts: MagicKeyValue[];
  dataTestId?: string;
}

const Alerts: React.FC<AllAlertsProps> = ({ alerts, dataTestId }) => {
  const { storeId } = useAccountDetail();

  const { t } = useTranslation();

  return (
    <>
      {alerts?.map((alert, index: number) => (
        <Alert
          key={`${storeId}_alert_${index}`} severity={alert.severity}
          dataTestId={`${index}_${dataTestId}`}
        >
          {t(alert.message)}
        </Alert>
      ))}
    </>
  );
};

export default Alerts;
