import { CCCS_ACTIONS } from './../../../CCCS/constants';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { storeId } from 'app/test-utils';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { getAlerts, selectAlerts } from './selector';
import { ICurrentResultCCCS } from 'pages/CCCS/types';
import { IN_PROGRESS } from '../constant';

describe('selector getAlerts', () => {
  const commonState: Partial<RootState> = {
    promiseToPay: {},
    cccs: {},
    collectionForm: {},
    hardship: {},
    deceased: {},
    bankruptcy: {},
    incarceration: {},
    longTermMedical: {}
  };

  it('should return promiseToPay', () => {
    const state: Partial<RootState> = {
      ...commonState,
      promiseToPay: {
        [storeId]: {
          promiseToPayData: { data: { type: 'mock type' } }
        } as any
      }
    };
    expect(getAlerts(state as RootState, storeId)).toStrictEqual([
      {
        message: I18N_COLLECTION_FORM.ACCOUNT_STATUS_PROMISE_TO_PAY,
        severity: 'warning'
      }
    ]);
  });

  it('should return hardship', () => {
    const state: Partial<RootState> = {
      ...commonState,
      collectionForm: {
        [storeId]: {
          lastFollowUpData: { callResultType: CALL_RESULT_CODE.NOT_HARDSHIP }
        } as any
      },
      hardship: {
        [storeId]: {
          data: {},
          status: 'IN_PROGRESS'
        }
      } as any
    };
    expect(getAlerts(state as RootState, storeId)).toStrictEqual([
      {
        message: 'txt_hardship_remove',
        severity: 'warning'
      }
    ]);
  });

  it('should return hardship', () => {
    const state: Partial<RootState> = {
      ...commonState,
      collectionForm: {
        [storeId]: {
          lastFollowUpData: { callResultType: CALL_RESULT_CODE.HARDSHIP }
        } as any
      },
      hardship: {
        [storeId]: {
          data: {
            processStatus: 'IN_PROGRESS'
          },
          status: 'IN_PROGRESS'
        }
      } as any
    };
    expect(getAlerts(state as RootState, storeId)).toStrictEqual([
      {
        message: 'txt_hardship_in_progress',
        severity: 'warning'
      }
    ]);
  });

  it('should return hardship', () => {
    const state: Partial<RootState> = {
      ...commonState,
      collectionForm: {
        [storeId]: {
          lastFollowUpData: { callResultType: CALL_RESULT_CODE.HARDSHIP }
        } as any
      },
      hardship: {
        [storeId]: {
          data: {
            processStatus: 'COMPLETE'
          },
          status: 'COMPLETE'
        }
      } as any
    };
    expect(getAlerts(state as RootState, storeId)).toStrictEqual([
      {
        message: I18N_COLLECTION_FORM.ACCOUNT_STATUS_HARDSHIP,
        severity: 'warning'
      }
    ]);
  });

  it('should return hardship', () => {
    const state: Partial<RootState> = {
      ...commonState,
      collectionForm: {
        [storeId]: {
          lastFollowUpData: { callResultType: CALL_RESULT_CODE.NOT_HARDSHIP }
        } as any
      },
      hardship: {
        [storeId]: {
          data: {},
          status: undefined
        }
      } as any
    };
    expect(getAlerts(state as RootState, storeId)).toStrictEqual([
      {
        message: I18N_COLLECTION_FORM.ACCOUNT_STATUS_NOT_HARDSHIP,
        severity: 'info'
      }
    ]);
  });

  it('should return cccs pending proposal', () => {
    const state: Partial<RootState> = {
      ...commonState,
      cccs: {
        [storeId]: {
          data: {
            action: CCCS_ACTIONS.PENDING_PROPOSAL
          } as ICurrentResultCCCS
        }
      }
    };
    expect(getAlerts(state as RootState, storeId)).toStrictEqual([
      {
        message: I18N_COLLECTION_FORM.CCCS_PROPOSAL_PENDING,
        severity: 'warning'
      }
    ]);
  });

  it('should return cccs accepted proposal', () => {
    const state: Partial<RootState> = {
      ...commonState,
      cccs: {
        [storeId]: {
          data: {
            action: CCCS_ACTIONS.ACCEPT_PROPOSAL
          } as ICurrentResultCCCS
        }
      }
    };
    expect(getAlerts(state as RootState, storeId)).toStrictEqual([
      {
        message: I18N_COLLECTION_FORM.CCCS_PROPOSAL_ACCEPTED,
        severity: 'info'
      }
    ]);
  });

  it('should return cccs setup in progress if update a cccs accepted proposal', () => {
    const state: Partial<RootState> = {
      ...commonState,
      cccs: {
        [storeId]: {
          data: {
            processStatus: IN_PROGRESS,
            action: CCCS_ACTIONS.ACCEPT_PROPOSAL
          } as ICurrentResultCCCS
        }
      }
    };
    expect(getAlerts(state as RootState, storeId)).toStrictEqual([
      {
        message: I18N_COLLECTION_FORM.CCCS_IN_PROGRESS,
        severity: 'warning'
      }
    ]);
  });

  it('should return empty by default', () => {
    const state: Partial<RootState> = {
      ...commonState
    };
    expect(getAlerts(state as RootState, storeId)).toStrictEqual([]);
  });

  it('selectAlerts', () => {
    const state: Partial<RootState> = {
      ...commonState
    };
    expect(selectAlerts(state as RootState, '')).toEqual([]);
  });

  it('isDeceasedPending  True', () => {
    const state: Partial<RootState> = {
      ...commonState,
      deceased: {
        [storeId]: {
          deceasedInformation: {
            deceasedConfirmation: 'Deceased Pending'
          }
        }
      }
    };

    expect(getAlerts(state as RootState, storeId)).toStrictEqual([
      {
        message: I18N_COLLECTION_FORM.ACCOUNT_STATUS_DECEASED_STATUS_PENDING,
        severity: 'warning'
      }
    ]);
  });

  it('isDeceasedPending  True', () => {
    const state: Partial<RootState> = {
      ...commonState,
      deceased: {
        [storeId]: {
          deceasedInformation: {
            deceasedConfirmation: 'Deceased Confirmed'
          }
        }
      }
    };

    expect(getAlerts(state as RootState, storeId)).toStrictEqual([
      {
        message: I18N_COLLECTION_FORM.ACCOUNT_STATUS_DECEASED,
        severity: 'warning'
      }
    ]);
  });

  it('should show bankruptcy confirm', () => {
    const state: Partial<RootState> = {
      ...commonState,
      bankruptcy: {
        [storeId]: {
          info: { actionEntryCode: CALL_RESULT_CODE.BANKRUPTCY }
        }
      }
    };

    expect(getAlerts(state as RootState, storeId)).toStrictEqual([
      {
        message: I18N_COLLECTION_FORM.ACCOUNT_STATUS_BANKRUPTCY,
        severity: 'warning'
      }
    ]);
  });

  it('should show bankruptcy pending', () => {
    const state: Partial<RootState> = {
      ...commonState,
      bankruptcy: {
        [storeId]: {
          info: { actionEntryCode: CALL_RESULT_CODE.BANKRUPTCY_PENDING }
        }
      }
    };

    expect(getAlerts(state as RootState, storeId)).toStrictEqual([
      {
        message: I18N_COLLECTION_FORM.ACCOUNT_STATUS_BANKRUPTCY_STATUS_PENDING,
        severity: 'warning'
      }
    ]);
  });

  it('should show incarceration', () => {
    const state: Partial<RootState> = {
      ...commonState,
      incarceration: {
        [storeId]: {
          data: { incarcerationStatus: 'ACTIVE' }
        } as any
      }
    };

    expect(getAlerts(state as RootState, storeId)).toStrictEqual([
      {
        message: I18N_COLLECTION_FORM.ACCOUNT_STATUS_INCARCERATION,
        severity: 'warning'
      }
    ]);
  });

  it('should show longterm medical', () => {
    const state: Partial<RootState> = {
      ...commonState,
      longTermMedical: {
        [storeId]: {
          data: {
            longTermMedicalStatus: 'ACTIVE'
          }
        }
      }
    };

    expect(getAlerts(state as RootState, storeId)).toStrictEqual([
      {
        message: I18N_COLLECTION_FORM.LONG_TEAM_MEDICAL,
        severity: 'warning'
      }
    ]);
  });
});
