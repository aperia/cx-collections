import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import isEmpty from 'lodash.isempty';
import isEqual from 'lodash.isequal';
import {
  selectCCCSAction,
  selectCCCSProcessStatus
} from 'pages/CCCS/_redux/selectors';
import {
  CALL_RESULT_CODE,
  STATUS_HARDSHIP,
  STATUS_HARDSHIP_SETUP
} from 'pages/CollectionForm/constants';
import {
  isActiveLongTermMedical,
  isHavingBankruptcyConfirm,
  isHavingBankruptcyPending,
  selectDeceasedStatusCodeFromDeceasedDetailAPI,
  selectIsHavingIncarceration
} from 'pages/CollectionForm/_redux/selectors';
import { selectPromiseToPayDetailData } from 'pages/PromiseToPay/_redux/selectors';
import { IN_PROGRESS } from '../constant';
import { createDeepEqualSelector } from './../../../../app/helpers/deepEqualSelector';
import { CCCS_ACTIONS } from './../../../CCCS/constants';
/**
 * Currently get alerts data from collection form,
 */
export const getAlerts = (state: RootState, storeId: string) => {
  const callResultType =
    state.collectionForm[storeId]?.lastFollowUpData?.callResultType;

  const promiseToPayData = selectPromiseToPayDetailData(state, storeId);
  const results = [];

  const statusHardship = state.hardship[storeId]?.status;
  const hardshipData = state.hardship[storeId]?.data;

  if (!isEmpty(promiseToPayData)) {
    results.push({
      message: I18N_COLLECTION_FORM.ACCOUNT_STATUS_PROMISE_TO_PAY,
      severity: 'warning'
    });
  }

  // cccs
  const cccsProcessStatus = selectCCCSProcessStatus(state, storeId);
  const isCCCSInprogress = cccsProcessStatus === IN_PROGRESS;

  if (isCCCSInprogress) {
    results.push({
      message: I18N_COLLECTION_FORM.CCCS_IN_PROGRESS,
      severity: 'warning'
    });
  } else {
    switch (selectCCCSAction(state, storeId)) {
      case CCCS_ACTIONS.PENDING_PROPOSAL:
        results.push({
          message: I18N_COLLECTION_FORM.CCCS_PROPOSAL_PENDING,
          severity: 'warning'
        });

        break;
      case CCCS_ACTIONS.ACCEPT_PROPOSAL:
        results.push({
          message: I18N_COLLECTION_FORM.CCCS_PROPOSAL_ACCEPTED,
          severity: 'info'
        });

        break;
      default:
        break;
    }
  }

  // remove hardship
  const isNotHardship = callResultType === CALL_RESULT_CODE.NOT_HARDSHIP;
  const isRemoveHardship = statusHardship !== STATUS_HARDSHIP;

  // setup hardship
  const hardshipSetup = callResultType === CALL_RESULT_CODE.HARDSHIP;
  const isSetupComplete = statusHardship === STATUS_HARDSHIP_SETUP;
  const isInprogress = statusHardship === IN_PROGRESS;

  if (!isEmpty(hardshipData) && isSetupComplete) {
    results.push({
      message: I18N_COLLECTION_FORM.ACCOUNT_STATUS_HARDSHIP,
      severity: 'warning'
    });
  }

  if (isEqual(hardshipData, {}) && isNotHardship && isRemoveHardship) {
    results.push({
      message: I18N_COLLECTION_FORM.ACCOUNT_STATUS_NOT_HARDSHIP,
      severity: 'info'
    });
  }

  // hardship setup in progress
  if (!isEmpty(hardshipData) && hardshipSetup && isInprogress) {
    results.push({
      message: 'txt_hardship_in_progress',
      severity: 'warning'
    });
  }

  // hardship remove in progress
  if (isEqual(hardshipData, {}) && isNotHardship && isInprogress) {
    results.push({
      message: 'txt_hardship_remove',
      severity: 'warning'
    });
  }

  // deceased
  const deceasedCode = selectDeceasedStatusCodeFromDeceasedDetailAPI(
    state,
    storeId
  );

  if (deceasedCode === CALL_RESULT_CODE.DECEASE) {
    results.push({
      message: I18N_COLLECTION_FORM.ACCOUNT_STATUS_DECEASED,
      severity: 'warning'
    });
  }

  if (deceasedCode === CALL_RESULT_CODE.DECEASE_PENDING) {
    results.push({
      message: I18N_COLLECTION_FORM.ACCOUNT_STATUS_DECEASED_STATUS_PENDING,
      severity: 'warning'
    });
  }

  // bankruptcy
  if (isHavingBankruptcyConfirm(state, storeId)) {
    results.push({
      message: I18N_COLLECTION_FORM.ACCOUNT_STATUS_BANKRUPTCY,
      severity: 'warning'
    });
  }

  if (isHavingBankruptcyPending(state, storeId)) {
    results.push({
      message: I18N_COLLECTION_FORM.ACCOUNT_STATUS_BANKRUPTCY_STATUS_PENDING,
      severity: 'warning'
    });
  }

  // incarceration
  if (selectIsHavingIncarceration(state, storeId)) {
    results.push({
      message: I18N_COLLECTION_FORM.ACCOUNT_STATUS_INCARCERATION,
      severity: 'warning'
    });
  }

  // long term medical
  if (isActiveLongTermMedical(state, storeId)) {
    results.push({
      message: I18N_COLLECTION_FORM.LONG_TEAM_MEDICAL,
      severity: 'warning'
    });
  }

  return results;
};

export const selectAlerts = createDeepEqualSelector(
  getAlerts,
  alerts => alerts
);
