import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';

// Actions
import { userRoleActions } from './_redux/reducer';

//components
import UserRole from './';

const spyUserRole = mockActionCreator(userRoleActions);

const renderWrapper = () => {
  return renderMockStore(<UserRole />, { initialState: {} });
};

describe('User Role', () => {
  it('render UI', () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: {
        commonConfig: {
          roles: { a: 'button 1', b: 'button 2', c: 'button 3' }
        },
        orgList: ['MOCK']
      }
    });
    const { wrapper } = renderWrapper();
    expect(wrapper.getByText('txt_welcome_to_collections')).toBeInTheDocument();
  });

  it('render UI > with empty configs', () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true
    });
    const { wrapper } = renderWrapper();
    expect(wrapper.getByText('txt_welcome_to_collections')).toBeInTheDocument();
  });

  it('handleSelectRole', () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: {
        commonConfig: {
          roles: { COLLECTOR1: 'COLLECTOR1', b: 'button 2', c: 'button 3' }
        },
        orgList: ['COLLECTOR1', '234', '345']
      }
    });
    const spy = spyUserRole('triggerSelectedUserRole');
    const { wrapper } = renderWrapper();
    const button = wrapper.getAllByText('234')[0]!;
    button.click();
    expect(spy).toBeCalledWith({
      org: '234',
      role: 'COLLECTOR1',
      token: 'COLLECTOR1'
    });
  });
});
