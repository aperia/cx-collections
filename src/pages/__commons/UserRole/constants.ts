export const ADMIN_ROLE = 'CXADMN';
export const SUPERVISOR_ROLE = 'CXCOL5';

// 3 new User Roles in Welcome page for testing Transaction Adjustment
export const TRANSACTION_ADJUSTMENT_ROLE = [
  'COLLECTOR1',
  'SUPERVISOR',
  'ADMIN'
];
export const SUPERVISOR = 'SUPERVISOR';
export const TRANSACTION_ADJUSTMENT_VIEW_ROLE = ['ADMIN', 'SUPERVISOR'];
// User roles admin & supervisor for access reporting
export const REPORTING_VIEW_ROLE = [
  'ADMIN',
  'SUPERVISOR',
  'CXADMN1',
  'CXADMN2'
];
export const CHANGE_HISTORY_VIEW_ROLE = ['ADMIN', 'CXADMN1', 'CXADMN2'];
