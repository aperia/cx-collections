// Types
import { UserRoleState } from '../types';

// Actions
import { userRoleActions, userRole } from './reducer';

describe('User Role Reducers', () => {
  const initialState: UserRoleState = {
    role: '',
    unExpiredToken: ''
  };

  it('should triggerSelectedUserRole', () => {
    const state = userRole(
      initialState,
      userRoleActions.triggerSelectedUserRole({
        role: 'role',
        token: 'unExpiredToken'
      })
    );
    expect(state?.role).toEqual('role');

    expect(state?.unExpiredToken).toEqual('unExpiredToken');
  });
});
