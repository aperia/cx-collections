import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Helpers
import { getSessionStorage } from 'app/helpers';

// Types
import { TriggerUserSelectRole, UserRoleState } from '../types';

const savedUserRole = getSessionStorage('FS_TOKEN');

const initialState = savedUserRole || {};
const { actions, reducer } = createSlice({
  name: 'userRole',
  initialState: initialState as UserRoleState,
  reducers: {
    triggerSelectedUserRole: (
      draftState,
      action: PayloadAction<TriggerUserSelectRole>
    ) => {
      const { role, token, org = '' } = action.payload;

      return {
        role,
        unExpiredToken: token,
        org
      };
    }
  }
});

const allActions = {
  ...actions
};

export { allActions as userRoleActions, reducer as userRole };
