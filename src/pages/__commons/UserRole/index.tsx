import React from 'react';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useDispatch } from 'react-redux';

// Actions
import { userRoleActions } from './_redux/reducer';

// Helper
import { genAmtId } from 'app/_libraries/_dls/utils';

// Constant
import { TRANSACTION_ADJUSTMENT_ROLE } from './constants';
import { Button, SimpleBar } from 'app/_libraries/_dls/components';

const UserRole: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const handleSelectRole = (role: string, token: string, org: string) => {
    dispatch(
      userRoleActions.triggerSelectedUserRole({
        role,
        token,
        org
      })
    );
  };

  // TODO: currently, userRoles in TRANSACTION_ADJUSTMENT_ROLE just for testing with CAT env. Plz remove/update when it becomes available
  const renderRoles = () => {
    const { roles = {} } = window.appConfig?.commonConfig || {};
    const { orgList = [] } = window.appConfig || {};
    return Object.keys(roles).map((item, index) => {
      return (
        <div
          className="item mb-16"
          key={index}
          data-testid={genAmtId(`${item}_landing-page`, 'role', '')}
        >
          <div className="role border-right">
            <p className=" fs-14 fw-500">{item}</p>
            {TRANSACTION_ADJUSTMENT_ROLE.some(
              (ele: string) => ele === item
            ) && <span className="fw-500">(CAT)</span>}
          </div>
          <div className="org">
            {orgList.map(org => (
              <Button
                dataTestId={genAmtId(`${item}_landing-page`, 'org', '')}
                onClick={() => handleSelectRole(item, roles[item], org)}
                key={org}
                variant="outline-primary"
              >
                {org}
              </Button>
            ))}
          </div>
        </div>
      );
    });
  };

  return (
    <div className="h-landing-page bg-light-l20">
      <SimpleBar
        data-testid={genAmtId('landing-page', 'simple-bar', 'UserRole')}
      >
        <div className="d-flex flex-column justify-content-start align-items-center">
          <h2
            className="text-center mb-16 mt-80"
            data-testid={genAmtId('landing-page', 'welcome-title', '')}
          >
            {t('txt_welcome_to_collections')}
          </h2>
          <div className="user-selection mb-40">{renderRoles()}</div>
        </div>
      </SimpleBar>
    </div>
  );
};

export default UserRole;
