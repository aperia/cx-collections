export interface UserRoleState {
  role?: string;
  unExpiredToken?: string;
  org?: string;
}

export interface TriggerUserSelectRole {
  role: string;
  token: string;
  org?: string;
}

export interface TriggerUserSelectRoleArgs extends TriggerUserSelectRole {}
