import { byPassFulfilled, responseDefault } from 'app/test-utils';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { commonService } from '../api-service';
import { SM_USER } from '../constants';
import { getESSToken } from './getESSToken';
import * as commonHelpers from 'app/helpers/commons';

describe('should test get ess token', () => {
  let spyGetSmUser: jest.SpyInstance;
  let spyGetESSToken: jest.SpyInstance;
  let store: Store<RootState>;
  let state: RootState;

  afterEach(() => {
    spyGetSmUser?.mockReset();
    spyGetSmUser?.mockRestore();
    spyGetESSToken?.mockReset();
    spyGetESSToken?.mockRestore();
  });

  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      user: 'user',
      org: 'org',
      app: 'app'
    }
  };

  const initialState: Partial<RootState> = {};

  it('should return data', async () => {
    store = createStore(rootReducer, initialState);

    spyGetSmUser = jest.spyOn(commonService, 'getSmUser').mockResolvedValue({
      ...responseDefault,
      data: {
        headers: {
          [SM_USER]: {}
        }
      }
    });

    spyGetESSToken = jest
      .spyOn(commonService, 'getESSToken')
      .mockResolvedValue({
        ...responseDefault,
        data: undefined as any
      });

    const response = await getESSToken()(
      byPassFulfilled(getESSToken.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.type).toEqual(getESSToken.fulfilled.type);
  });

  it('should be pending', () => {
    const pending = getESSToken.pending(getESSToken.pending.type, undefined);
    const actual = rootReducer(state, pending);
    expect(actual.appConfig.isLoading).toEqual(true);
  });

  it('should be fulfilled', () => {
    const fulfilled = getESSToken.fulfilled(
      '',
      getESSToken.fulfilled.type,
      undefined
    );
    const actual = rootReducer(state, fulfilled);
    expect(actual.appConfig.isLoading).toEqual(false);
  });

  it('should be fulfilled with mock return value from parseJwt > CXMGR', () => {
    jest.spyOn(commonHelpers, 'parseJwt').mockReturnValue({
      OrgID: '',
      userName: '',
      operatorId: '',
      x500Id: '',
      privileges: ['CXMGR']
    });
    const fulfilled = getESSToken.fulfilled(
      '',
      getESSToken.fulfilled.type,
      undefined
    );
    const actual = rootReducer(state, fulfilled);
    expect(actual.appConfig.isLoading).toEqual(false);
  });

  it('should be fulfilled with mock return value from parseJwt > CXSUP_ROLE', () => {
    jest.spyOn(commonHelpers, 'parseJwt').mockReturnValue({
      OrgID: '',
      userName: '',
      operatorId: '',
      x500Id: '',
      privileges: ['CXSUP_ROLE']
    });
    const fulfilled = getESSToken.fulfilled(
      '',
      getESSToken.fulfilled.type,
      undefined
    );
    const actual = rootReducer(state, fulfilled);
    expect(actual.appConfig.isLoading).toEqual(false);
  });

  it('should be fulfilled with mock return value from parseJwt > operator >= 3', () => {
    jest.spyOn(commonHelpers, 'parseJwt').mockReturnValue({
      OrgID: '',
      userName: '',
      operatorId: '12345',
      x500Id: '',
      privileges: ['CXSUP_ROLE']
    });
    const fulfilled = getESSToken.fulfilled(
      '',
      getESSToken.fulfilled.type,
      undefined
    );
    const actual = rootReducer(state, fulfilled);
    expect(actual.appConfig.isLoading).toEqual(false);
  });

  it('should be rejected', () => {
    const rejected = getESSToken.rejected(
      null,
      getESSToken.rejected.type,
      undefined
    );
    const actual = rootReducer(state, rejected);
    expect(actual.appConfig.isLoading).toEqual(false);
  });
});
