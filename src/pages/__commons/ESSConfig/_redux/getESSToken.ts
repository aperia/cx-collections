import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// Helper
import { parseJwt, setSessionStorage } from 'app/helpers';

// Service
import { commonService } from '../api-service';

// Const
import {
  ADMIN_ROLE,
  CXMGR_ROLE,
  CXSUP_ROLE,
  USER_TOKEN_ID
} from 'app/constants';
import { SM_USER } from '../constants';

// Type
import { AppConfigState } from '../types';

// Actions
import { renewTokenActions } from 'pages/__commons/RenewToken/_redux/reducer';

export const getESSToken = createAsyncThunk<string, undefined, ThunkAPIConfig>(
  'common/getESSToken',
  async (_, thunkAPI) => {
    const { dispatch } = thunkAPI;
    const smUserData = await commonService.getSmUser();

    const { data = '' } = await commonService.getESSToken(
      smUserData.headers[SM_USER]
    );

    await dispatch(
      renewTokenActions.getRenewToken({
        storeId: USER_TOKEN_ID,
        payload: {
          jwt: data
        }
      })
    );

    return data;
  }
);

export const getESSTokenBuilder = (
  builder: ActionReducerMapBuilder<AppConfigState>
) => {
  const { pending, fulfilled, rejected } = getESSToken;

  builder.addCase(pending, () => {
    return {
      isLoading: true
    };
  });

  builder.addCase(fulfilled, (draftState, action) => {
    setSessionStorage('FS_TOKEN', action.payload);

    const {
      OrgID = '',
      OrgId = '',
      userName = '',
      operatorId = '',
      x500Id = '',
      privileges = [],
      essUserName = ''
    } = parseJwt(action.payload) || {};

    const isAdminRole = !!privileges.find(item => item.startsWith(ADMIN_ROLE));
    const isManager =
      !!privileges.find(item => item.startsWith(CXMGR_ROLE)) && !isAdminRole;
    const isSupervisor =
      !!privileges.find(item => item.startsWith(CXSUP_ROLE)) && !isManager;

    const customOperatorId =
      operatorId.length > 3
        ? operatorId.substr(operatorId.length - 3, operatorId.length)
        : operatorId;

    window.appConfig.commonConfig = {
      ...window.appConfig.commonConfig,
      org: OrgID || OrgId,
      user: userName,
      operatorID: customOperatorId,
      x500id: x500Id,
      privileges,
      isAdminRole,
      isManager,
      isSupervisor,
      essUserName,
      isCollector: !(isAdminRole || isManager || isSupervisor)
    };

    return {
      isLoading: false
    };
  });

  builder.addCase(rejected, () => {
    return {
      isLoading: false
    };
  });
};
