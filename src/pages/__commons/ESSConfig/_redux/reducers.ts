import { createSlice } from '@reduxjs/toolkit';

// Type
import { AppConfigState } from '../types';

// Redux
import { getESSToken, getESSTokenBuilder } from './getESSToken';

const initialState: AppConfigState = {};
const { reducer, actions } = createSlice({
  initialState,
  name: 'appConfig',
  reducers: {},
  extraReducers: builder => {
    getESSTokenBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getESSToken
};

export { allActions as appConfigActions, reducer as appConfig };
