import { selectorWrapper } from 'app/test-utils';
import { takeLoadingStatus } from './selectors';

describe('ess config selector', () => {
  const store: Partial<RootState> = {
    appConfig: {
      isLoading: false
    }
  };
  it('take loading status', () => {
    const { data } = selectorWrapper(store)(takeLoadingStatus);

    expect(data).toEqual(false);
  });
});
