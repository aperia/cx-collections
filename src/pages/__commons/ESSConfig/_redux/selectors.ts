import { createSelector } from 'reselect';

const getLoadingStatus = (state: RootState) => {
  return state.appConfig?.isLoading;
};

export const takeLoadingStatus = createSelector(getLoadingStatus, data => data);
