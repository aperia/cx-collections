import React, { useEffect } from 'react';
import classNames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { isUndefined } from 'lodash';

// Hooks
import { useSaveSessionStorage } from 'app/hooks';

// Component
import AppContainer from 'AppContainer';
import MappingProvider from 'pages/__commons/MappingProvider';
import ErrorBoundary from 'pages/__commons/ErrorBoundary';
import FallbackErrorComponent from 'pages/__commons/ErrorBoundary/FallbackErrorComponent';
import I18next from 'pages/__commons/I18nextProvider';
import RenewToken from '../RenewToken';

// Redux
import { takeLoadingStatus } from './_redux/selectors';
import { appConfigActions } from './_redux/reducers';

export interface AppConfigProps {}

const AppConfig: React.FC<AppConfigProps> = ({ children }) => {
  const dispatch = useDispatch();

  const isLoadingConfig = useSelector(takeLoadingStatus);

  useEffect(() => {
    dispatch(appConfigActions.getESSToken());
  }, [dispatch]);

  useSaveSessionStorage();

  if (isLoadingConfig || isUndefined(isLoadingConfig))
    return (
      <div
        className={classNames({
          loading: isLoadingConfig,
          'vh-100': isLoadingConfig
        })}
      ></div>
    );

  return (
    <MappingProvider>
      <I18next>
        <ErrorBoundary fallbackComponent={FallbackErrorComponent}>
          <AppContainer />
          <RenewToken />
        </ErrorBoundary>
      </I18next>
    </MappingProvider>
  );
};

export default AppConfig;
