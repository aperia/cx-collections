import { apiService } from 'app/utils/api.service';
import axios from 'axios';

export const commonService = {
  getSmUser() {
    if (
      window.location.host === 'cxc-qa.local' ||
      window.location.host === 'cxc-int.vnqa.local' ||
      process.env.NODE_ENV === 'development'
    ) {
      return apiService.get('mockApi/cx-collections/');
    }

    return axios.get(window.location.pathname);
  },
  getESSToken(smUser: string) {
    let url = window.appConfig?.api?.common?.getESSToken || '';

    if (
      window.location.host === 'cxc-qa.local' ||
      window.location.host === 'cxc-int.vnqa.local' ||
      process.env.NODE_ENV === 'development'
    ) {
      url = `mockApi/${url}`;
      return apiService.get<string>(`${url}/${smUser.replace('.', '/')}`, {
        withCredentials: true
      });
    }

    return axios.get<string>(`${url}/${smUser.replace('.', '/')}`, {
      withCredentials: true,
      baseURL: window.location.origin
    });
  }
};
