import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi } from 'app/test-utils/mocks/mockProperty';
import { commonService } from './api-service';
const OLD_ENV = process.env;
describe('ess config services', () => {
  describe('getSmUser', () => {
    it('not development', () => {
      process.env = { ...OLD_ENV, NODE_ENV: 'production' };
      commonService.getSmUser();
      expect(true).toBeTruthy();
    });
    it('development', () => {
      process.env = { ...OLD_ENV, NODE_ENV: 'development' };
      const mockService = mockApiServices.get();
      commonService.getSmUser();
      expect(mockService).toBeCalled();
    });
  });
  describe('getESSToken', () => {
    it('url is defined', () => {
      mockAppConfigApi.setup();
      process.env = { ...OLD_ENV, NODE_ENV: 'development' };

      const mockService = mockApiServices.get();

      commonService.getESSToken('');

      expect(mockService).toBeCalled();
    });

    it('url is undefined with production', () => {
      mockAppConfigApi.clear();
      process.env = { ...OLD_ENV, NODE_ENV: 'production' };

      const mockService = mockApiServices.get();

      commonService.getESSToken('');

      expect(mockService).toBeTruthy();
    });
  });
});
