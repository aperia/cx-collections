import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import AppConfig from '.';
import { appConfigActions } from './_redux/reducers';

const mockAppConfigActions = mockActionCreator(appConfigActions);

describe('should test ess config', () => {
  const renderWrapper = (initialState: Partial<RootState>) =>
    renderMockStore(<AppConfig />, { initialState });

  it('should render ui', () => {
    const mockGetESSToken = mockAppConfigActions('getESSToken');

    jest.useFakeTimers();

    renderWrapper({
      appConfig: {
        isLoading: false
      },
      mapping: {
        data: {},
        loading: false
      }
    });

    jest.runAllTimers();

    expect(mockGetESSToken).toBeCalled();
  });

  it('should render ui with loading', () => {
    const mockGetESSToken = mockAppConfigActions('getESSToken');

    jest.useFakeTimers();

    renderWrapper({
      appConfig: {
        isLoading: true
      },
      mapping: {
        data: {},
        loading: false
      }
    });

    jest.runAllTimers();

    expect(mockGetESSToken).toBeCalled();
  });
});
