export interface AppConfigState {
  isLoading?: boolean;
}

export interface GetESSTokenResponse {
  renewedToken: string;
}
