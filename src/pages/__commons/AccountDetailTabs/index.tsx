import React from 'react';

// components/types
import Overview from 'pages/AccountDetails/Overview';
import StatementTransaction from 'pages/StatementsAndTransactions/StatementTransaction';
import PendingAuthorizationTransaction from 'pages/StatementsAndTransactions/PendingAuthorizationTransaction';
import Payments from 'pages/Payment';
import { HorizontalTabs } from 'app/_libraries/_dls/components';
import { SelectCallback } from 'react-bootstrap/helpers';

// redux
import { useDispatch } from 'react-redux';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { selectActiveKey } from './_redux/selectors';
import { actions } from './_redux';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export const KEYS = {
  overview: 'overview',
  statement: 'statement',
  pendingAuthorizations: 'pendingAuthorizations',
  payments: 'payments'
};

const TransactionTab: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const [tabList, setTabList] = React.useState<MagicKeyValue>([]);
  const activeKey =
    useStoreIdSelector<string>(selectActiveKey) || tabList[0]?.eventKey;
  const testId = 'account-detail-tabs';

  const handleSelect: SelectCallback = newActiveKey => {
    dispatch(
      actions.setActiveTab({
        storeId,
        activeKey: newActiveKey!
      })
    );
  };

  React.useEffect(() => {
    const listTab = [];
    if (
      checkPermission(
        PERMISSIONS.OVERVIEW_HISTORICAL_INFORMATION_VIEW,
        storeId
      ) ||
      checkPermission(PERMISSIONS.OVERVIEW_MONETARY_INFORMATION_VIEW, storeId)
    ) {
      listTab.push({
        key: `${storeId}-${KEYS.overview}`,
        eventKey: KEYS.overview,
        title: t('txt_overview'),
        component: Overview
      });
    }

    if (checkPermission(PERMISSIONS.STATEMENT_VIEW, storeId)) {
      listTab.push({
        key: `${storeId}-${KEYS.statement}`,
        eventKey: KEYS.statement,
        title: t('txt_statement'),
        component: StatementTransaction
      });
    }

    if (checkPermission(PERMISSIONS.PENDING_AUTHORIZATION_VIEW, storeId)) {
      listTab.push({
        key: `${storeId}-${KEYS.pendingAuthorizations}`,
        eventKey: KEYS.pendingAuthorizations,
        title: t('txt_pending_authorizations'),
        component: PendingAuthorizationTransaction
      });
    }

    if (
      checkPermission(PERMISSIONS.PAYMENT_HISTORY_VIEW, storeId) ||
      checkPermission(PERMISSIONS.PAYMENT_LIST_VIEW, storeId)
    ) {
      listTab.push({
        key: `${storeId}-${KEYS.payments}`,
        eventKey: KEYS.payments,
        title: t('txt_payments'),
        component: Payments
      });
    }

    setTabList(listTab);
  }, [dispatch, storeId, t]);

  const renderListTab = tabList.map(
    ({ key, eventKey, title, component: Component }: MagicKeyValue) => {
      return (
        <HorizontalTabs.Tab key={key} eventKey={eventKey} title={title}>
          <Component />
        </HorizontalTabs.Tab>
      );
    }
  );

  const FinalComponent = tabList[0]?.component;

  const renderInfo = () =>
    (tabList.length > 1 && (
      <HorizontalTabs
        defaultActiveKey={tabList[0].eventKey}
        onSelect={handleSelect}
        activeKey={activeKey}
        mountOnEnter
        unmountOnExit
        className="bg-light-l20"
        dataTestId={genAmtId(testId, 'transaction-tab', 'TransactionTab')}
      >
        {renderListTab}
      </HorizontalTabs>
    )) ||
    (tabList.length === 1 && <FinalComponent />);

  return <div>{isEmpty(tabList) ? null : renderInfo()}</div>;
};

export default TransactionTab;
