import { reducer, actions } from '.';
import { storeId } from 'app/test-utils';
import { State } from '../types';

const { removeStore, setActiveTab } = actions;

const initialState: State = { [storeId]: {} };

describe('AccountDetailTabs reducer', () => {
  it('should run setActiveTab action', () => {
    const params = { storeId, activeKey: 'activeKey' };
    const state = reducer(initialState, setActiveTab(params));
    expect(state[storeId]).toEqual({ activeKey: 'activeKey' });
  });

  it('should run removeStore action', () => {
    const params = { storeId };
    const state = reducer(initialState, removeStore(params));
    expect(state[storeId]).toBeUndefined();
  });
});
