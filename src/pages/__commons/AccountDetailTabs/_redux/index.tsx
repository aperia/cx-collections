import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// types
import { State, SetActiveKey } from '../types';

const { actions, reducer } = createSlice({
  name: 'accountDetailTabs',
  initialState: {} as State,
  reducers: {
    setActiveTab: (draftState, action: PayloadAction<SetActiveKey>) => {
      const { activeKey, storeId } = action.payload;

      draftState[storeId] = { ...draftState[storeId], activeKey };
    },
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    }
  }
});

export { actions, reducer };
export * from './selectors';
