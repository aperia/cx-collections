import { createSelector } from '@reduxjs/toolkit';
import { getSystemCommon } from 'app/helpers';

export const selectActiveKey = createSelector(
  (states: RootState, storeId: string) =>
    states.accountDetailTabs[storeId]?.activeKey,
  (activeKey?: string) => activeKey
);

export const selectClientInfoGetByIdRequest = createSelector(
  (states: RootState, storeId: string) => {
    const { accountDetail } = states;
    const {
      agent,
      clientID: clientNumber,
      system,
      prin
    } = getSystemCommon(accountDetail[storeId] || {});
    const { user = '', app = '' } = window.appConfig?.commonConfig || {};
    return {
      common: { agent, clientNumber, system, prin, app, user },
      clientInfoId: `${clientNumber}${system}${prin}${agent}`
    };
  },
  data => data
);
