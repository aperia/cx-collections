import { storeId } from 'app/test-utils';

import { selectActiveKey } from './selectors';

describe('selector getAlerts', () => {
  it('should return deceased', () => {
    const state: Partial<RootState> = {
      accountDetailTabs: {
        [storeId]: {
          activeKey: 'activeKey'
        }
      }
    };
    expect(selectActiveKey(state as RootState, storeId)).toStrictEqual(
      'activeKey'
    );
  });
});
