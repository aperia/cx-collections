import React from 'react';
import {
  mockActionCreator,
  queryAllByClass,
  renderMockStoreId,
  storeId
} from 'app/test-utils';
import * as entitlementsHelpers from 'app/entitlements/helpers';

// Components
import TransactionTab from './index';

// redux
import { actions } from './_redux';
import { PERMISSIONS } from 'app/entitlements/constants';

HTMLCanvasElement.prototype.getContext = jest.fn();
const {getComputedStyle} = window
window.getComputedStyle = (elt) => getComputedStyle(elt);

const mockAction = mockActionCreator(actions);

let spy: jest.SpyInstance;

describe('Test TransactionTab', () => {
  afterEach(() => {
      spy.mockReset();
      spy.mockRestore();
  });

  it('should have dispatch action', () => {
    jest.useFakeTimers();
    const mockSetActiveTab = mockAction('setActiveTab');
    spy = jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
    const { wrapper } = renderMockStoreId(<TransactionTab />, {
      initialState: {}
    });
    jest.runAllTimers();
    const queryTab = queryAllByClass(wrapper.container, /nav-item nav-link/);
    queryTab[0]!.click();
    expect(mockSetActiveTab).toBeCalledWith({
      storeId,
      activeKey: queryTab[0].getAttribute('data-rb-event-key')
    });

  });

  it('should render UI with OVERVIEW_MONETARY_INFORMATION_VIEW is true', () => {
    jest.useFakeTimers();
    spy = jest.spyOn(entitlementsHelpers, 'checkPermission')
    .mockImplementation((permission) => 
      permission === PERMISSIONS.OVERVIEW_MONETARY_INFORMATION_VIEW ? true : false
    );
    const { wrapper } = renderMockStoreId(<TransactionTab />, {
      initialState: {}
    });
    jest.runAllTimers();
    expect(wrapper.getByText('txt_overview')).toBeInTheDocument();
  });

  it('should render UI with OVERVIEW_MONETARY_INFORMATION_VIEW is false', () => {
    jest.useFakeTimers();
    spy = jest.spyOn(entitlementsHelpers, 'checkPermission')
    .mockImplementation((permission) => 
      (permission === PERMISSIONS.OVERVIEW_MONETARY_INFORMATION_VIEW ||
        permission === PERMISSIONS.OVERVIEW_HISTORICAL_INFORMATION_VIEW) ? false : true
    );
    const { wrapper } = renderMockStoreId(<TransactionTab />, {
      initialState: {}
    });
    expect(wrapper.getByRole('table')).toBeInTheDocument();
  });
});
