export interface SetActiveKey {
  activeKey: string;
  storeId: string;
}

export interface AccountDetailTabs {
  activeKey?: string;
}

export interface State extends Record<string, AccountDetailTabs> {}
