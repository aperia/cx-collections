
interface AccessTokenResponse {
  token?: string;
  voltageValue?: string[];
}

export interface AccessTokenArgs extends ArgsCommon { }

export interface AccessTokenPayload extends AccessTokenState { }

export interface AccessTokenState {
  accountInfo?: AccessTokenResponse;
  accountId?: string;
  isLoading?: boolean;
}

export interface AccessTokenResult {
  [storeId: string]: AccessTokenState;
}

interface AccessTokenResponse {
  token?: string;
  voltageValue?: string[];
}

export interface AccessTokenState {
  accountInfo?: AccessTokenResponse;
  accountId?: string;
  isLoading?: boolean;
}