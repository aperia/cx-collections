import { createAsyncThunk } from '@reduxjs/toolkit';

// Service
import { dashboardServices } from '../../../AccountSearch/Home/Dashboard/dashboardServices';

// Types
import { UpdateCollectionAccountArgs, UpdateCollectionAccountPayload } from '../../../AccountSearch/Home/Dashboard/types';


export const updateCollectionAccount = createAsyncThunk<
  UpdateCollectionAccountPayload,
  UpdateCollectionAccountArgs,
  ThunkAPIConfig
>('dashboard/updateCollectionAccount', async (args, thunkAPI) => {
  const request = args;
  const { rejectWithValue } = thunkAPI;

  const { data } = await dashboardServices.updateCollectionAccount(request);

  if (data?.success) {
    return data
  } else {
    return rejectWithValue({ errorMessage: data.errorMessage });
  }
});