import { createSlice } from '@reduxjs/toolkit';

// Type
import { AccessTokenResult } from '../types';
import { getAccessToken, getAccessTokenBuilder } from './getAccessToken';
import { updateCollectionAccount } from './updateCollectionAccount';

// Redux
const { reducer, actions } = createSlice({
  name: 'accessToken',
  initialState: {} as AccessTokenResult,
  reducers: {},
  extraReducers: builder => {
    getAccessTokenBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getAccessToken,
  updateCollectionAccount
};

export { allActions as accessTokenActions, reducer as accessToken };
