import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
// Service
import { commonService } from '../../RenewToken/api-service';
// Types
import {
  AccessTokenArgs, AccessTokenPayload, AccessTokenResult
} from '../types';



export const getAccessToken = createAsyncThunk<
  AccessTokenPayload,
  AccessTokenArgs,
  ThunkAPIConfig
>('common/getAccessToken', async (args, thunkAPI) => {
  const { rejectWithValue } = thunkAPI;
  const { accountId } = args.postData;

  try {
    const { data } = await commonService.getAccessToken({ accountId });
    return data;
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const getAccessTokenBuilder = (
  builder: ActionReducerMapBuilder<AccessTokenResult>
) => {
  builder.addCase(getAccessToken.pending, (draftState, action) => {
    const { storeId } = action.meta.arg;

    return {
      ...draftState, [storeId]: {
        isLoading: true
      }
    }
  });

  builder.addCase(getAccessToken.fulfilled, (draftState, action) => {
    const { storeId } = action.meta.arg;
    const { accountInfo, accountId } = action.payload;

    return {
      ...draftState, [storeId]: {
        accountInfo,
        accountId,
        isLoading: false
      }
    }
  });

  builder.addCase(getAccessToken.rejected, (draftState, action) => {
    const { storeId } = action.meta.arg;

    return {
      ...draftState, [storeId]: {
        isLoading: false
      }
    }
  });
};
