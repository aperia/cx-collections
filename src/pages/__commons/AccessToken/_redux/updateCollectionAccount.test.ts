import { responseDefault } from 'app/test-utils';
import { dashboardServices } from 'pages/AccountSearch/Home/Dashboard/dashboardServices';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import { updateCollectionAccount } from './updateCollectionAccount';

describe('getAccessToken.test', () => {
  const store = createStore(rootReducer, {}, applyMiddleware(thunk));

  afterAll(() => {
    jest.clearAllMocks();
  });

  it('have data', async () => {
    jest.spyOn(dashboardServices, 'updateCollectionAccount').mockResolvedValue({
      ...responseDefault,
      data: {
        success: true
      }
    });

    const response = await updateCollectionAccount({
      accountToken: '01eb381b-5567-4d68-8814-e5d1ed2cfc26',
      homePhone: '1234567890',
      workPhone: '1234567890',
      nextWorkDate: '2021-06-28',
      permCollCd: '123',
      extStatus: 'C',
      statusRcd: '97',
      agentId: 'John Doe'
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({
      success: true
    });
  });

  it('have data', async () => {
    jest.spyOn(dashboardServices, 'updateCollectionAccount').mockResolvedValue({
      ...responseDefault,
      data: {
        success: false,
        errorMessage: 'error'
      }
    });

    const response = await updateCollectionAccount({
      accountToken: '01eb381b-5567-4d68-8814-e5d1ed2cfc26',
      homePhone: '1234567890',
      workPhone: '1234567890',
      nextWorkDate: '2021-06-28',
      permCollCd: '123',
      extStatus: 'C',
      statusRcd: '97',
      agentId: 'John Doe'
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({
      errorMessage: 'error'
    });
  });
});
