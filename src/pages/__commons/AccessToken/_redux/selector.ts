import { createSelector } from "@reduxjs/toolkit";

const getAccessToken = (state: RootState, storeId: string) => state.accessToken[storeId]?.accountInfo;

export const takeAccessToken = createSelector(
  getAccessToken,
  data => data?.token || ''
);