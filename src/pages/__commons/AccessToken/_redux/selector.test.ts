import { storeId } from 'app/test-utils';
import { takeAccessToken } from './selector';

describe('selector test', () => {
  const store: Partial<RootState> = {
    accessToken: {
      [storeId]: {
        accountInfo: {
          token: '123'
        },
      }
    }
  };

  const emptyStore: Partial<RootState> = {
    accessToken: {
      [storeId]: {
        accountInfo: {},
      }
    }
  };

  it('takeAccessToken', () => {
    const result = takeAccessToken(store, storeId)
    expect(result).toEqual("123");
  })

  it('takeAccessToken with error data', () => {
    const result = takeAccessToken(emptyStore, storeId)
    expect(result).toEqual("");
  })
});
