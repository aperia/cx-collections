import { responseDefault, storeId } from 'app/test-utils';
import { commonService } from 'pages/__commons/RenewToken/api-service';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import { getAccessToken } from './getAccessToken';

describe('getAccessToken.test', () => {
  const store = createStore(rootReducer, {}, applyMiddleware(thunk));

  it('have data', async () => {

    jest.spyOn(commonService, 'getAccessToken').mockResolvedValue({
      ...responseDefault,
      data: {
        success: true
      }
    })

    const response = await getAccessToken({
      storeId,
      postData: { accountId: storeId }
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({
      success: true,
    })
  });

  it('no data', async () => {
    jest.spyOn(commonService, 'getAccessToken').mockRejectedValue({
      ...responseDefault,
      data: {
        success: false
      }
    })

    const response = await getAccessToken({
      storeId,
      postData: { accountId: storeId }
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({
      config: {},
      data: {
        success: false,
      },
      headers: {},
      status: 200,
      statusText: ""
    })
  });
});
