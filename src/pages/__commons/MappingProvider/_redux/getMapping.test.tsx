import { mockCreateStore } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import mappingService from '../mappingService';
import { actionsMapping } from './reducers';
describe('Test getMapping', () => {
  const store = mockCreateStore({ caches: {}, mapping: {} });

  const data = {
    accountMoreInfo: {
      cycleCounts: 'daysDelinquentCount',
      daysDelinquent: 'daysDelinquent',
      delinquentAmount: 'delinquentAmount',
      externalStatusCode: 'externalStatusCode',
      internalStatusCode: 'internalStatusCode'
    }
  };

  const callApi = async () => {
    jest.spyOn(mappingService, 'getMapping').mockResolvedValue({ data } as any);

    return await actionsMapping.getMapping()(
      store.dispatch,
      store.getState,
      {}
    );
  };

  it('Should have call api success', async () => {
    await callApi();
    const state = store.getState().mapping;
    expect(state.data.accountMoreInfo).toEqual(data.accountMoreInfo);
  });

  it('Should have pending', async () => {
    const action = actionsMapping.getMapping.pending(
      'mapping/getMapping',
      undefined
    );
    const state = rootReducer(store.getState(), action).mapping;
    expect(state.loading).toEqual(true);
  });

  it('Should have fulfilled', async () => {
    const action = actionsMapping.getMapping.fulfilled(
      data,
      'mapping/getMapping',
      undefined
    );
    const state = rootReducer(store.getState(), action).mapping;
    expect(state.loading).toEqual(false);
    expect(state.data.accountMoreInfo).toEqual(data.accountMoreInfo);
  });

  it('Should have reject', async () => {
    const action = actionsMapping.getMapping.rejected(
      null,
      'mapping/getMapping',
      undefined
    );
    const state = rootReducer(store.getState(), action).mapping;
    expect(state.loading).toEqual(true);
  });
});
