import axios from 'axios';
const mappingService = {
  getMapping() {
    const url = window.appConfig?.mappingUrl || '';
    return axios.get(url);
  }
};

export default mappingService;
