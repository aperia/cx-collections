// mocks
import { mockAxiosResolve } from 'app/test-utils/mocks/mockAxiosResolve';

// services
import mappingService from './mappingService';

// utils
import {
  mockAppConfigApi,
  mappingUrl
} from 'app/test-utils/mocks/mockProperty';

describe('mappingService', () => {
  describe('getMapping', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      mappingService.getMapping();

      expect(mockAxiosResolve).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      mappingService.getMapping();

      expect(mockAxiosResolve).toBeCalledWith(mappingUrl);
    });
  });
});
