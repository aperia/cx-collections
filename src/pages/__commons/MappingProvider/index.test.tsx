import React from 'react';
import MappingProvider from './index';
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import { screen } from '@testing-library/react';
import { actionsMapping } from './_redux/reducers';
const spyAction = mockActionCreator(actionsMapping);
const text = 'Test Mapping Provider';

const Test = () => {
  return <div>{text}</div>;
};

describe('Test RAF/Mapping Provider', () => {
  it('Should have empty reducer', () => {
    const mockGetMapping = spyAction('getMapping');
    renderMockStoreId(
      <MappingProvider>
        <Test />
      </MappingProvider>,
      { mapping: {} }
    );
    const queryText = screen.queryByText('text');
    expect(queryText).not.toBeInTheDocument();
    expect(mockGetMapping).toHaveBeenCalled();
  });

  it('Should have  reducer', () => {
    const mockGetMapping = spyAction('getMapping');
    renderMockStoreId(
      <MappingProvider>
        <Test />
      </MappingProvider>,
      {
        mapping: {
          accountMoreInfo: {
            cycleCounts: 'daysDelinquentCount',
            daysDelinquent: 'daysDelinquent',
            delinquentAmount: 'delinquentAmount',
            externalStatusCode: 'externalStatusCode',
            internalStatusCode: 'internalStatusCode'
          }
        }
      }
    );
    const queryText = screen.queryByText(text);
    expect(queryText).toBeInTheDocument();
    expect(mockGetMapping).toHaveBeenCalled();
  });
});
