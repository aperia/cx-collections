import React from 'react';

import { default as HeaderRAC } from 'app/components/Header';

export interface HeaderProps {
  dataTestId?: string;
}

const Header: React.FC<HeaderProps> = ({ dataTestId }) => {
  return <HeaderRAC dataTestId={dataTestId} />;
};

export default Header;
