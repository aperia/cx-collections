import React from 'react';
import Header from './index';
import { queryByClass, renderMockStoreId } from 'app/test-utils';

describe('Test RAF/Header', () => {
  it('render to UI', () => {
    const { wrapper } = renderMockStoreId(<Header />);
    const queryClassHeader = queryByClass(wrapper.container, 'header');
    expect(queryClassHeader).toBeInTheDocument();
  });
});
