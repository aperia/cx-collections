import axios from 'axios';

export const sharedInfoService = {
  // mock common info like operatorID, phoneNumber, ....
  getInfo() {
    const url = 'config/sharedInfo.json';
    return axios.get(url);
  }
};
