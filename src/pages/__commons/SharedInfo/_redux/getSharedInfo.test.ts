import { responseDefault } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { sharedInfoService } from '../sharedInfoService';
import { getSharedInfo } from './getSharedInfo';
import { sharedInfoActions } from './reducer';
let spy: jest.SpyInstance;
const store = createStore(rootReducer, {});
const data = { endofCallAction: 'CallWorked', phoneNumber: '123-456-7890' };
describe('getSharedInfo', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('should return data', async () => {
    spy = jest.spyOn(sharedInfoService, 'getInfo').mockResolvedValue({
      ...responseDefault,
      data
    });
    await getSharedInfo()(store.dispatch, store.getState, {});
    expect(store.getState().sharedInfo.data).toEqual(data);
  });

  it('should return fulfilled', async () => {
    const action = sharedInfoActions.getSharedInfo.fulfilled(
      data,
      'sharedInfo/getSharedInfo',
      undefined
    );
    const state = rootReducer(store.getState(), action).sharedInfo;
    expect(state.data).toEqual(data);
  });
});
