import * as selectors from './selector';

import { selectorWrapper } from 'app/test-utils';

const initialState: Partial<RootState> = {
  sharedInfo: {
    data: {
      endofCallAction: 'CallWorked',
      phoneNumber: '123-456-7890'
    }
  }
};

const selectorTrigger = selectorWrapper(initialState);

describe('Selector SharedInfo', () => {
  it('selectSharedInfoData', () => {
    const { data } = selectorTrigger(selectors.selectSharedInfoData);
    expect(data).toEqual({
      endofCallAction: 'CallWorked',
      phoneNumber: '123-456-7890'
    });
  });
});
