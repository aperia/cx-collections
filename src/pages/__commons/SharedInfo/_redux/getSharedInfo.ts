import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { sharedInfoService } from '../sharedInfoService';
import { SharedInfoPayLoad, SharedInfoState } from '../types';

export const getSharedInfo = createAsyncThunk<
  SharedInfoPayLoad,
  undefined,
  ThunkAPIConfig
>('sharedInfo/getSharedInfo', async () => {
  const { data: infos } = await sharedInfoService.getInfo();
  return infos;
});

export const getSharedInfoBuilder = (
  builder: ActionReducerMapBuilder<SharedInfoState>
) => {
  builder.addCase(getSharedInfo.fulfilled, (draftState, action) => {
    draftState.data = action.payload;
  });
};
