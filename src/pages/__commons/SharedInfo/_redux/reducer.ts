import { createSlice } from '@reduxjs/toolkit';
import { SharedInfoState } from '../types';
import { getSharedInfo, getSharedInfoBuilder } from './getSharedInfo';

const initialState: SharedInfoState = { data: {} };

const { actions, reducer } = createSlice({
  name: 'sharedInfo',
  initialState,
  reducers: {},
  extraReducers: builder => {
    getSharedInfoBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getSharedInfo
};

export { allActions as sharedInfoActions, reducer };
