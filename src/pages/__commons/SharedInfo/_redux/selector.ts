import { createSelector } from '@reduxjs/toolkit';

const getSharedInfoData = (state: RootState) => {
  return state.sharedInfo.data;
};

export const selectSharedInfoData = createSelector(
  getSharedInfoData,
  data => data
);
