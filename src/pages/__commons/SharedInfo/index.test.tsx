import React from 'react';
import SharedInfo from './index';
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import { sharedInfoActions } from './_redux/reducer';

const spyActions = mockActionCreator(sharedInfoActions);
describe('Test Toggle Pin', () => {
  it('Should have icon unpin', () => {
    const getSharedInfo = spyActions('getSharedInfo');
    renderMockStoreId(<SharedInfo />);
    expect(getSharedInfo).toHaveBeenCalled();
  });
});
