// mocks
import { mockAxiosResolve } from 'app/test-utils/mocks/mockAxiosResolve';

// services
import { sharedInfoService } from './sharedInfoService';

describe('sharedInfoService', () => {
  it('getInfo', () => {
    sharedInfoService.getInfo();

    expect(mockAxiosResolve).toBeCalledWith('config/sharedInfo.json');
  });
});
