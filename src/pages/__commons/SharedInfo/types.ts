export type SharedInfoPayLoad = Record<string, string>;

export interface SharedInfoState {
  data: SharedInfoPayLoad;
}
