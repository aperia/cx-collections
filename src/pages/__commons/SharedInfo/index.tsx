import { homeActions } from 'pages/AccountSearch/Home/_redux/reducers';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { sharedInfoActions } from './_redux/reducer';

const SharedInfo = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(sharedInfoActions.getSharedInfo());
    dispatch(homeActions.getStateRefDataRequest({}));
  }, [dispatch]);

  return null;
};

export default SharedInfo;
