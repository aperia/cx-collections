import { screen } from '@testing-library/react';
import React from 'react';

// components
import CallResults from '.';

// hooks
import { AccountDetailProvider } from 'app/hooks';

// utils
import {
  renderMockStore,
  storeId,
  accEValue,
} from 'app/test-utils';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';

// redux

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <CallResults />
    </AccountDetailProvider>,
    { initialState }
  );
};

const initialState: Partial<RootState> = {
  incarceration: { [storeId]: { isLoading: false } }
};


describe('Render', () => {
  it('should render', () => {

    renderWrapper(initialState);

    expect(
      screen.getByText(I18N_COLLECTION_FORM.INCARCERATION_INFORMATION)
    ).toBeInTheDocument();
    expect(
      screen.getByTestId('incarcerationCallResultsViews')
    ).toBeInTheDocument();
  });
});
