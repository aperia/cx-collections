import React from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useStoreIdSelector } from 'app/hooks';
import { useAccountDetail } from 'app/hooks';

// redux
import { useDispatch } from 'react-redux';
import { selectLoadingModal, selectOpenModal } from '../_redux/selectors';
import { incarcerationActions } from '../_redux/reducers';

// helper
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { genAmtId } from 'app/_libraries/_dls/utils';

const RemoveIncarceration: React.FC = () => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const loading: boolean = useStoreIdSelector(selectLoadingModal);
  const isOpen: boolean = useStoreIdSelector(selectOpenModal);
  const { t } = useTranslation();
  const testId = 'incarceration-removeModal';

  const handleRemove = () => {
    dispatch(
      incarcerationActions.removeIncarceration({
        storeId
      })
    );
  };

  const handleCloseModal = () => {
    dispatch(
      incarcerationActions.onChangeOpenModal({
        storeId
      })
    );
  };

  return (
    <Modal
      sm
      show={isOpen}
      loading={loading}
      className="window-lg-full"
      dataTestId={testId}
    >
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId={`${testId}-header`}
      >
        <ModalTitle dataTestId={`${testId}-title`}>
          {t('txt_confirm_remove_incarceration')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <p data-testid={genAmtId(testId, 'body', '')}>
          {t('txt_are_you_sure_incarceration')}
        </p>
      </ModalBody>
      <ModalFooter>
        <Button
          type="submit"
          variant="secondary"
          onClick={handleCloseModal}
          dataTestId={`${testId}_cancel-btn`}
        >
          {t('txt_cancel')}
        </Button>
        <Button
          type="submit"
          onClick={handleRemove}
          variant="danger"
          dataTestId={`${testId}_remove-btn`}
        >
          {t('txt_remove')}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default RemoveIncarceration;
