import React from 'react';

// components
import RemoveIncarceration from './RemoveIncarceration';

// hooks
import { AccountDetailProvider } from 'app/hooks';

// utils
import {
  renderMockStore,
  storeId,
  accEValue,
  mockActionCreator
} from 'app/test-utils';

// redux
import { incarcerationActions } from '../_redux/reducers';

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <RemoveIncarceration />
    </AccountDetailProvider>,
    { initialState }
  );
};

const initialState: Partial<RootState> = {
  incarceration: {
    [storeId]: {
      isLoadingModal: true,
      isOpenModal: true
    }
  }
};

const incarcerationSpy = mockActionCreator(incarcerationActions);

describe('Render', () => {
  it('should render', () => {
    const { wrapper } = renderWrapper(initialState);
    expect(
      wrapper.getByText('txt_confirm_remove_incarceration')
    ).toBeInTheDocument();
  });

  it('handleCloseModal', () => {
    const spy = incarcerationSpy('onChangeOpenModal');
    const { wrapper } = renderWrapper(initialState);
    const button = wrapper.getByText('txt_cancel')!;
    button.click();

    expect(spy).toBeCalledWith({
      storeId
    });
  });

  it('handleRemove', () => {
    const spy = incarcerationSpy('removeIncarceration');
    const { wrapper } = renderWrapper(initialState);
    const button = wrapper.getByText('txt_remove')!;
    button.click();

    expect(spy).toBeCalledWith({
      storeId
    });
  });
});
