import React, { useEffect } from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// redux
import { useDispatch } from 'react-redux';
import { selectData } from '../_redux/selectors';
import { incarcerationActions } from '../_redux/reducers';
import RemoveIncarceration from './RemoveIncarceration';

// component
import { View } from 'app/_libraries/_dof/core';

// helper
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { prepareViewData } from '../helpers';
import { IncarcerationPayload } from '../types';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface CallResultsProps {}

const CallResults: React.FC<CallResultsProps> = () => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();

  const { t } = useTranslation();

  const incarcerationData =
    useStoreIdSelector<IncarcerationPayload>(selectData);

  useEffect(() => {
    dispatch(
      incarcerationActions.getIncarceration({
        storeId
      })
    );
  }, [dispatch, storeId]);

  useEffect(() => {
    return () => {
      dispatch(
        incarcerationActions.removeStore({
          storeId
        })
      );
    };
  }, [dispatch, storeId]);

  return (
    <>
      <h6
        className="color-grey mt-8"
        data-testid={genAmtId('incarceration-callResults', 'title', '')}
      >
        {t(I18N_COLLECTION_FORM.INCARCERATION_INFORMATION)}
      </h6>
      <View
        id={`${storeId}-incarcerationViews`}
        formKey={`${storeId}-incarcerationViews`}
        descriptor="incarcerationCallResultsViews"
        value={prepareViewData(incarcerationData)}
      />
      <RemoveIncarceration />
    </>
  );
};

export default CallResults;
