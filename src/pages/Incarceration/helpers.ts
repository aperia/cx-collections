import isEqual from 'lodash.isequal';
import { IncarcerationPayload } from './types';

export const prepareViewData = (data: IncarcerationPayload) => {
  return {
    contactName: data?.incarcerationContactName,
    startDay: `${data?.incarcerationStartDate}T00:00:00`,
    endDay: `${data?.incarcerationEndDate}T00:00:00`,
    location: data?.locationOfIncarceration
  };
};

export const isEqualPostDataAndSavedData = (
  postData: MagicKeyValue,
  savedData: MagicKeyValue
) => {
  const compareData = {
    contactName: savedData?.incarcerationContactName,
    incarcerationStartDate: savedData?.incarcerationStartDate,
    incarcerationEndDate: savedData?.incarcerationEndDate,
    locationOfIncarceration: savedData?.locationOfIncarceration
  };

  return isEqual(postData, compareData);
};
