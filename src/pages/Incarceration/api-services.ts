import { apiService } from 'app/utils/api.service';
import {
  GetIncarcerationRequestBody,
  RemoveIncarcerationRequestBody,
  UpdateIncarcerationRequestBody
} from './types';

export const incarcerationService = {
  getIncarceration(requestData: GetIncarcerationRequestBody) {
    const url = window.appConfig?.api?.collectionForm.getIncarceration || '';
    return apiService.post(url, requestData);
  },

  removeIncarceration(requestData: RemoveIncarcerationRequestBody) {
    const url = window.appConfig?.api?.collectionForm.removeIncarceration || '';
    return apiService.post(url, requestData);
  },

  updateIncarceration(requestData: UpdateIncarcerationRequestBody) {
    const url = window.appConfig?.api?.collectionForm.updateIncarceration || '';
    return apiService.post(url, requestData);
  }
};
