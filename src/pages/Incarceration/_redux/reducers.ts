import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IncarcerationState } from '../types';

import {
  triggerUpdateIncarceration,
  triggerUpdateIncarcerationBuilder
} from './updateIncarcerationData';

import {
  getIncarceration,
  getIncarcerationBuilder
} from './getIncarcerationData';

import {
  removeIncarceration,
  removeIncarcerationBuilder
} from './removeIncarceration';

const { actions, reducer } = createSlice({
  name: 'incarceration',
  initialState: {} as IncarcerationState,
  reducers: {
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    onChangeOpenModal: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isOpenModal: !draftState[storeId]?.isOpenModal
      };
    }
  },
  extraReducers: builder => {
    getIncarcerationBuilder(builder);
    triggerUpdateIncarcerationBuilder(builder);
    removeIncarcerationBuilder(builder);
  }
});

const incarcerationActions = {
  ...actions,
  removeIncarceration,
  getIncarceration,
  triggerUpdateIncarceration
};

export { incarcerationActions, reducer };
