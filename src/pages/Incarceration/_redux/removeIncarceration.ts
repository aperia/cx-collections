import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import isEmpty from 'lodash.isempty';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { selectCardholderContactName } from 'pages/LongTermMedical/_redux/selectors';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// Service
import { incarcerationService } from '../api-services';

// Type
import { GetIncarcerationArg, IncarcerationState } from '../types';
import { incarcerationActions } from './reducers';

export const removeIncarceration = createAsyncThunk<
  undefined,
  GetIncarcerationArg,
  ThunkAPIConfig
>('incarceration/removeIncarceration', async (args, thunkAPI) => {
  const { storeId } = args;
  const { dispatch, getState } = thunkAPI;

  const { callingApplicationApr01, operatorID, org, app } =
    window.appConfig?.commonConfig || {};

  const state = getState();
  const numberPhoneAuthenticated =
    state.accountDetail[storeId]?.numberPhoneAuthenticated || '';
  const eValueAccountId = state.accountDetail[storeId]?.data?.eValueAccountId;

  const { selectedCallResult } = state.collectionForm[storeId];

  let apiResponse = null;

  const contactName = selectCardholderContactName(state, storeId) as string;
  try {
    await incarcerationService.removeIncarceration({
      common: { accountId: eValueAccountId, org, app },
      callingApplication: callingApplicationApr01 || '',
      operatorCode: operatorID || '',
      requestType: 'SET_INACTIVE',
      contactName,
      cardholderContactedPhoneNumber: numberPhoneAuthenticated
    });

    if (!isEmpty(selectedCallResult)) {
      dispatch(
        collectionFormActions.selectCallResult({ item: undefined, storeId })
      );
    }

    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'success',
        message: 'txt_incarceration_removed'
      })
    );

    dispatch(
      incarcerationActions.getIncarceration({
        storeId
      })
    );
  } catch (error) {
    apiResponse = error;
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: 'txt_incarceration_remove_failed'
      })
    );
    return thunkAPI.rejectWithValue(error);
  } finally {
    dispatch(
      apiErrorNotificationAction.updateApiError({
        storeId,
        forSection: 'inCollectionFormFlyOut',
        apiResponse
      })
    );
  }
});

export const removeIncarcerationBuilder = (
  builder: ActionReducerMapBuilder<IncarcerationState>
) => {
  builder
    .addCase(removeIncarceration.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingModal: true
      };
    })
    .addCase(removeIncarceration.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingModal: false,
        isOpenModal: false
      };
    })
    .addCase(removeIncarceration.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingModal: false,
        isOpenModal: false
      };
    });
};
