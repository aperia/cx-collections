import { createStore, Store } from '@reduxjs/toolkit';
import { incarcerationActions } from './reducers';

import { incarcerationService } from '../api-services';
import { mockActionCreator, responseDefault, storeId } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';

const spyAction = mockActionCreator(collectionFormActions);

describe('Test getIncarceration', () => {
  const mockArgs = {
    storeId,
    postData: {
      accountId: storeId,
      data: {
        contactName: '123',
        incarcerationStartDate: '123',
        incarcerationEndDate: '123',
        locationOfIncarceration: '123'
      }
    }
  };

  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {
      collectionForm: {
        [storeId]: {
          inProgressInfo: {
            contactName: '123',
            incarcerationStartDate: '123',
            incarcerationEndDate: '123',
            locationOfIncarceration: '123'
          },
          isInProgress: true
        }
      }
    });
  });

  it('Should return undefined when loading', async () => {
    const mockToggleCallResultInProgressStatus = spyAction(
      'toggleCallResultInProgressStatus'
    );
    const mockChangeLastFollowUpData = spyAction('changeLastFollowUpData');

    store = createStore(rootReducer, {
      collectionForm: {},
      incarceration: { [storeId]: { isLoading: true } }
    });

    spyAction('toggleCallResultInProgressStatus');
    const incarcerationServiceMock = jest
      .spyOn(incarcerationService, 'getIncarceration')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    const response = await incarcerationActions.getIncarceration(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(mockToggleCallResultInProgressStatus).not.toBeCalled();
    expect(mockChangeLastFollowUpData).not.toBeCalled();
    expect(response.payload).toEqual(undefined);
    incarcerationServiceMock.mockClear();
  });

  it('Should return data with empty state', async () => {
    const mockToggleCallResultInProgressStatus = spyAction(
      'toggleCallResultInProgressStatus'
    );
    const mockChangeLastFollowUpData = spyAction('changeLastFollowUpData');

    store = createStore(rootReducer, { collectionForm: {} });

    spyAction('toggleCallResultInProgressStatus');
    const incarcerationServiceMock = jest
      .spyOn(incarcerationService, 'getIncarceration')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    const response = await incarcerationActions.getIncarceration(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(mockToggleCallResultInProgressStatus).not.toBeCalled();
    expect(mockChangeLastFollowUpData).not.toBeCalled();
    expect(response.payload).toEqual({});
    incarcerationServiceMock.mockClear();
  });

  it('Should return data', async () => {
    const data = {
      incarcerationContactName: '123',
      incarcerationStartDate: '123',
      incarcerationEndDate: '123',
      locationOfIncarceration: '123',
      incarcerationStatus: 'INACTIVE'
    };

    const mockToggleCallResultInProgressStatus = spyAction(
      'toggleCallResultInProgressStatus'
    );
    const mockChangeLastFollowUpData = spyAction('changeLastFollowUpData');
    const incarcerationServiceMock = jest
      .spyOn(incarcerationService, 'getIncarceration')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          latestIncarcerationDetail: {
            incarcerationChangeHistoryList: [
              {
                ...data,
                contactName: data.incarcerationContactName
              },
              {
                ...data,
                contactName: data.incarcerationContactName
              }
            ]
          },
          latestIncarcerationStatus: 'INACTIVE'
        }
      });

    const response = await incarcerationActions.getIncarceration(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    const expectResult = { ...data, contactName: '123' };
    expect(response.payload).toEqual(expectResult);
    expect(mockToggleCallResultInProgressStatus).toBeCalledWith({
      storeId,
      info: expectResult
    });
    expect(mockChangeLastFollowUpData).toBeCalledWith({ storeId });
    incarcerationServiceMock.mockClear();
  });

  it('Should error', async () => {
    const incarcerationServiceMock = jest
      .spyOn(incarcerationService, 'getIncarceration')
      .mockRejectedValue({ ...responseDefault });

    const response = await incarcerationActions.getIncarceration(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual(responseDefault);
    incarcerationServiceMock.mockClear();
  });
});
