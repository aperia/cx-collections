import * as selector from './selectors';

// utils
import { selectorWrapper, storeId } from 'app/test-utils';

const states: Partial<RootState> = {
  incarceration: {
    [storeId]: {
      data: {
        incarcerationContactName: 'name',
        incarcerationEndDate: '',
        incarcerationStartDate: '',
        incarcerationStatus: '',
        locationOfIncarceration: ''
      },
      isLoading: true,
      isLoadingModal: true,
      isOpenModal: true
    }
  }
};

const selectorTrigger = selectorWrapper(states);

describe('Incarceration > selector', () => {
  it('selectData', () => {
    const { data, emptyData } = selectorTrigger(selector.selectData);

    expect(data).toEqual(states.incarceration![storeId].data);
    expect(emptyData).toBeUndefined();
  });

  it('selectLoading', () => {
    const { data, emptyData } = selectorTrigger(selector.selectLoading);

    expect(data).toEqual(states.incarceration![storeId].isLoading);
    expect(emptyData).toBeUndefined();
  });

  it('selectLoadingModal', () => {
    const { data, emptyData } = selectorTrigger(selector.selectLoadingModal);

    expect(data).toEqual(states.incarceration![storeId].isLoadingModal);
    expect(emptyData).toBeUndefined();
  });

  it('selectOpenModal', () => {
    const { data, emptyData } = selectorTrigger(selector.selectOpenModal);

    expect(data).toEqual(states.incarceration![storeId].isOpenModal);
    expect(emptyData).toBeUndefined();
  });
});
