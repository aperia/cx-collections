import { incarcerationActions as actions, reducer } from './reducers';
import { storeId } from 'app/test-utils';
import { IncarcerationState } from '../types';

const initialState: IncarcerationState = {};

describe('Incarceration > reducer', () => {
  it('removeStore', () => {
    // invoke
    const params = { storeId };
    const state = reducer(initialState, actions.removeStore(params));

    // expect
    expect(state[storeId]).toBeUndefined();
  });

  it('onChangeOpenModal', () => {
    // invoke
    const params = { storeId };
    const state = reducer(initialState, actions.onChangeOpenModal(params));

    // expect
    expect(state[storeId]).toEqual({ isOpenModal: true });
  });
});
