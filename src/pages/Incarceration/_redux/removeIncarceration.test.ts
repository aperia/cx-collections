import './reducers';
import { createStore } from '@reduxjs/toolkit';
import { mockActionCreator, responseDefault, storeId } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import { incarcerationService } from '../api-services';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { incarcerationActions } from './reducers';
import { removeIncarceration } from './removeIncarceration';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

const mockArgs = {
  storeId,
  postData: {
    accountId: storeId
  }
};

const initState: Partial<RootState> = {
  accountDetail: {
    [storeId]: { numberPhoneAuthenticated: '123456789' }
  },
  collectionForm: {
    [storeId]: {
      selectedCallResult: {
        code: 'code',
        description: 'des'
      }
    } as never
  }
};

let store = createStore(rootReducer, initState);

const collectionActionSpy = mockActionCreator(collectionFormActions);
const toastActionSpy = mockActionCreator(actionsToast);
const incarcerationActionSpy = mockActionCreator(incarcerationActions);
const apiErrorNotificationActionSpy = mockActionCreator(
  apiErrorNotificationAction
);

describe('Test removeIncarceration', () => {
  beforeEach(() => {
    window.appConfig = {
      commonConfig: {
        app: 'app',
        org: 'COLX',
        operatorID: 'ABC',
        callingApplicationApr01: 'APR01'
      } as CommonConfig
    } as AppConfiguration;
  });

  const callApi = (isError = false) => {
    window.appConfig = undefined as unknown as AppConfiguration;

    if (isError) {
      jest
        .spyOn(incarcerationService, 'removeIncarceration')
        .mockRejectedValue(new Error('Fail'));
    } else {
      jest
        .spyOn(incarcerationService, 'removeIncarceration')
        .mockResolvedValue({ ...responseDefault });
    }

    return incarcerationActions.removeIncarceration(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );
  };

  it('Call success API success without selectedCallResult', async () => {
    const addToast = toastActionSpy('addToast');
    const getIncarceration = incarcerationActionSpy('getIncarceration');
    const apiErrorNotificationAction =
      apiErrorNotificationActionSpy('updateApiError');

    store = createStore(rootReducer, {
      ...initState,
      accountDetail: { [storeId]: {} },
      collectionForm: { [storeId]: {} }
    });

    await callApi();

    expect(apiErrorNotificationAction).toHaveBeenCalledWith({
      storeId,
      forSection: 'inCollectionFormFlyOut',
      apiResponse: null
    });

    expect(addToast).toHaveBeenCalledWith({
      show: true,
      type: 'success',
      message: 'txt_incarceration_removed'
    });

    expect(getIncarceration).toHaveBeenCalledWith({
      storeId
    });
  });

  it('Call API success with selectedCallResult', async () => {
    const selectCallResult = collectionActionSpy('selectCallResult');
    store = createStore(rootReducer, {
      ...initState
    });

    await callApi();

    expect(selectCallResult).toHaveBeenCalledWith({ item: undefined, storeId });
  });

  it('Call Api fail', async () => {
    const addToast = toastActionSpy('addToast');
    const apiErrorNotificationAction =
      apiErrorNotificationActionSpy('updateApiError');
    store = createStore(rootReducer, {
      ...initState
    });

    await callApi(true);
    expect(addToast).toHaveBeenCalledWith({
      show: true,
      type: 'error',
      message: 'txt_incarceration_remove_failed'
    });

    expect(apiErrorNotificationAction).toHaveBeenCalledWith({
      storeId,
      forSection: 'inCollectionFormFlyOut',
      apiResponse: new Error('Fail')
    });
  });

  it('pending', () => {
    const pendingAction = removeIncarceration.pending(
      removeIncarceration.pending.type,
      mockArgs
    );
    const actual = rootReducer(store.getState(), pendingAction).incarceration[
      storeId
    ];

    expect(actual.isLoadingModal).toEqual(true);
  });

  it('fulfilled', () => {
    const pendingAction = removeIncarceration.fulfilled(
      undefined,
      removeIncarceration.pending.type,
      mockArgs
    );
    const actual = rootReducer(store.getState(), pendingAction).incarceration[
      storeId
    ];

    expect(actual.isLoadingModal).toEqual(false);
    expect(actual.isOpenModal).toEqual(false);
  });

  it('rejected', () => {
    const pendingAction = removeIncarceration.rejected(
      null,
      removeIncarceration.pending.type,
      mockArgs
    );

    const actual = rootReducer(store.getState(), pendingAction).incarceration[
      storeId
    ];

    expect(actual.isLoadingModal).toEqual(false);
    expect(actual.isOpenModal).toEqual(false);
  });
});
