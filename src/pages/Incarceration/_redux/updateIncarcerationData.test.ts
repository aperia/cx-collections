import './reducers';
import { applyMiddleware, createStore, Store } from '@reduxjs/toolkit';
import {
  updateIncarceration,
  triggerUpdateIncarceration
} from './updateIncarcerationData';

import { incarcerationService } from '../api-services';
import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault,
  storeId
} from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import {
  TriggerUpdateIncarcerationArg,
  TRIGGER_TYPE,
  UpdateIncarcerationArg
} from '../types';

// redux
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';
import thunk from 'redux-thunk';

const mockArgs: UpdateIncarcerationArg = {
  storeId,
  postData: {
    contactName: '123',
    incarcerationStartDate: '123',
    incarcerationEndDate: '123',
    locationOfIncarceration: '123',
    callingApplication: '123',
    cardholderContactedPhoneNumber: '123',
    operatorCode: '123'
  }
};

const mockIncarcerationPayload = {
  incarcerationStatus: 'incarcerationStatus',
  incarcerationContactName: 'incarcerationContactName',
  incarcerationStartDate: 'incarcerationStartDate',
  incarcerationEndDate: 'incarcerationEndDate',
  locationOfIncarceration: 'locationOfIncarceration'
};

let store: Store<RootState>;
let state: RootState;
const initState: Partial<RootState> = {
  form: {
    [`${storeId}-incarcerationFormViews`]: {
      registeredFields: true,
      values: {}
    },
    caches: {}
  }
};

beforeEach(() => {
  store = createStore(rootReducer, initState);
  state = store.getState();
});

const collectionFormSpy = mockActionCreator(collectionFormActions);
const toastSpy = mockActionCreator(actionsToast);

describe('Test updateIncarceration', () => {
  it('Should return data', async () => {
    jest
      .spyOn(incarcerationService, 'updateIncarceration')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const response = await updateIncarceration(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({});
  });

  it('Should return empty', async () => {
    jest
      .spyOn(incarcerationService, 'updateIncarceration')
      .mockRejectedValue({ ...responseDefault });

    const response = await updateIncarceration(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual('incarceration/updateIncarceration/rejected');
  });
});

describe('test triggerUpdateIncarceration', () => {
  const triggerArgs: TriggerUpdateIncarcerationArg = {
    storeId,
    postData: {
      common: {
        accountId: 'accountId'
      },
      type: TRIGGER_TYPE.CREATE
    }
  };

  describe('isFulfilled', () => {
    it('when method is create with special case', async () => {
      window.appConfig = {
        api: {
          collectionForm: {
            getIncarceration: 'url'
          }
        }
      };
      jest
        .spyOn(incarcerationService, 'updateIncarceration')
        .mockResolvedValue({ ...responseDefault, data: {} });
      jest.spyOn(incarcerationService, 'getIncarceration').mockResolvedValue({
        ...responseDefault,
        data: {
          test: '123'
        }
      });

      const mockAddToast = toastSpy('addToast');
      const mockSetCurrentView = collectionFormSpy('setCurrentView');
      const mockSelectCallResult = collectionFormSpy('selectCallResult');
      const mockChangeMethod = collectionFormSpy('changeMethod');

      const store = createStore(
        rootReducer,
        {
          incarceration: {
            [storeId]: {}
          },
          form: {
            [`${storeId}-incarcerationFormViews`]: {
              registeredFields: true,
              values: {
                contactName: '123',
                startDay: '123',
                endDay: '123',
                location: '123'
              }
            }
          }
        },
        applyMiddleware(thunk)
      );

      await triggerUpdateIncarceration(triggerArgs)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
      });
      expect(mockSetCurrentView).toBeCalledWith({ storeId });
      expect(mockSelectCallResult).toBeCalledWith({ storeId });
      expect(mockChangeMethod).toBeCalledWith({
        storeId,
        method: COLLECTION_METHOD.CREATE
      });
    });

    it('when method is create', async () => {
      const mockAddToast = toastSpy('addToast');

      const mockSetCurrentView = collectionFormSpy('setCurrentView');
      const mockSelectCallResult = collectionFormSpy('selectCallResult');
      const mockChangeMethod = collectionFormSpy('changeMethod');

      jest
        .spyOn(incarcerationService, 'updateIncarceration')
        .mockResolvedValue({ ...responseDefault, data: {} });
      jest
        .spyOn(incarcerationService, 'getIncarceration')
        .mockResolvedValue({ ...responseDefault, data: { test: '123' } });

      await triggerUpdateIncarceration(triggerArgs)(
        byPassFulfilled(triggerUpdateIncarceration.fulfilled.type),
        store.getState,
        {}
      );

      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
      });

      expect(mockSetCurrentView).toBeCalledWith({ storeId });
      expect(mockSelectCallResult).toBeCalledWith({ storeId });
      expect(mockChangeMethod).toBeCalledWith({
        storeId,
        method: COLLECTION_METHOD.CREATE
      });
    });

    it('when method is edit', async () => {
      const mockAddToast = toastSpy('addToast');

      const mockSetCurrentView = collectionFormSpy('setCurrentView');
      const mockSelectCallResult = collectionFormSpy('selectCallResult');
      const mockChangeMethod = collectionFormSpy('changeMethod');

      jest
        .spyOn(incarcerationService, 'updateIncarceration')
        .mockResolvedValue({ ...responseDefault, data: {} });

      await triggerUpdateIncarceration({
        storeId,
        postData: {
          common: { accountId: storeId },
          type: TRIGGER_TYPE.EDIT,
          data: {
            incarcerationStatus: 'incarcerationStatus',
            incarcerationContactName: 'incarcerationContactName',
            incarcerationStartDate: 'incarcerationStartDate',
            incarcerationEndDate: 'incarcerationEndDate',
            locationOfIncarceration: 'locationOfIncarceration'
          }
        }
      })(
        byPassFulfilled(triggerUpdateIncarceration.fulfilled.type),
        store.getState,
        {}
      );

      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATED
      });

      expect(mockSetCurrentView).toBeCalledWith({ storeId });
      expect(mockSelectCallResult).toBeCalledWith({ storeId });
      expect(mockChangeMethod).toBeCalledWith({
        storeId,
        method: COLLECTION_METHOD.CREATE
      });
    });
  });

  describe('isRejected', () => {
    const triggerArgs: TriggerUpdateIncarcerationArg = {
      storeId,
      postData: {
        common: {
          accountId: 'accountId'
        },
        type: TRIGGER_TYPE.CREATE
      }
    };

    it('when method is create', async () => {
      jest
        .spyOn(incarcerationService, 'updateIncarceration')
        .mockResolvedValue({ ...responseDefault, data: {} });
      const mockAddToast = toastSpy('addToast');
      await triggerUpdateIncarceration(triggerArgs)(
        byPassRejected(triggerUpdateIncarceration.rejected.type),
        store.getState,
        {}
      );
      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'error',
        message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMIT_FAILED
      });
    });
    it('when method is edit', async () => {
      jest
        .spyOn(incarcerationService, 'updateIncarceration')
        .mockResolvedValue({ ...responseDefault, data: {} });
      const mockAddToast = toastSpy('addToast');
      await triggerUpdateIncarceration({
        storeId,
        postData: {
          common: { accountId: storeId },
          type: TRIGGER_TYPE.EDIT,
          data: mockIncarcerationPayload
        }
      })(
        byPassRejected(triggerUpdateIncarceration.rejected.type),
        store.getState,
        {}
      );
      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'error',
        message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATE_FAILED
      });
    });
  });

  it('should response getIncarceration pending', () => {
    const pendingAction = triggerUpdateIncarceration.pending(
      triggerUpdateIncarceration.pending.type,
      triggerArgs
    );
    const actual = rootReducer(state, pendingAction);
    expect(actual.incarceration[storeId].isLoading).toEqual(true);
  });

  it('should response getIncarceration fulfilled', () => {
    const fulfilled = triggerUpdateIncarceration.fulfilled(
      {},
      triggerUpdateIncarceration.pending.type,
      triggerArgs
    );
    const actual = rootReducer(state, fulfilled);
    expect(actual.incarceration[storeId].isLoading).toEqual(false);
  });

  it('should response getIncarceration rejected', () => {
    const rejected = triggerUpdateIncarceration.rejected(
      null,
      triggerUpdateIncarceration.rejected.type,
      triggerArgs
    );
    const actual = rootReducer(state, rejected);
    expect(actual.incarceration[storeId].isLoading).toEqual(false);
  });
});
