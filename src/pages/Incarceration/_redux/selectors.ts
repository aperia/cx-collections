import { createSelector } from '@reduxjs/toolkit';

const getIncarcerationData = (state: RootState, storeId: string) => {
  return state.incarceration[storeId];
};

export const selectData = createSelector(
  getIncarcerationData,
  incarceration => incarceration?.data
);

export const selectLoading = createSelector(
  getIncarcerationData,
  loading => loading?.isLoading
);

export const selectLoadingModal = createSelector(
  (state: RootState, storeId: string) =>
    state.incarceration[storeId]?.isLoadingModal,
  (data?: boolean) => data
);

export const selectOpenModal = createSelector(
  (state: RootState, storeId: string) =>
    state.incarceration[storeId]?.isOpenModal,
  (data?: boolean) => data
);
