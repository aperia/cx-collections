import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// Const
import { FormatTime } from 'app/constants/enums';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { formatTimeDefault } from 'app/helpers';
import {
  CALL_RESULT_CODE,
  COLLECTION_METHOD
} from 'pages/CollectionForm/constants';

// Helper

// Redux
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// Service
import { incarcerationService } from '../api-services';

// Type
import {
  UpdateIncarcerationArg,
  TriggerUpdateIncarcerationArg,
  TRIGGER_TYPE,
  IncarcerationState
} from '../types';
import {
  dispatchDelayProgress,
  setDelayProgress
} from 'pages/CollectionForm/helpers';
import { incarcerationActions } from './reducers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export const updateIncarceration = createAsyncThunk<
  MagicKeyValue,
  UpdateIncarcerationArg,
  ThunkAPIConfig
>('incarceration/updateIncarceration', async (args, thunkAPI) => {
  try {
    const { data } = await incarcerationService.updateIncarceration(
      args.postData
    );

    return data;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerUpdateIncarceration = createAsyncThunk<
  unknown,
  TriggerUpdateIncarcerationArg,
  ThunkAPIConfig
>('incarceration/triggerUpdateIncarceration', async (args, thunkAPI) => {
  const {
    storeId,
    postData: { type }
  } = args;

  const { callingApplicationApr01, operatorID, org, app } =
    window.appConfig?.commonConfig || {};
  const { accountDetail } = thunkAPI.getState();
  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;

  const formValues =
    thunkAPI.getState().form[`${storeId}-incarcerationFormViews`].values;

  const { dispatch } = thunkAPI;

  const postData = {
    contactName: formValues?.contactName,
    incarcerationStartDate:
      formatTimeDefault(formValues?.startDay, FormatTime.UTCDate) || '',
    incarcerationEndDate:
      formatTimeDefault(formValues?.endDay, FormatTime.UTCDate) || '',
    locationOfIncarceration: formValues?.location || ''
  };

  const response = await dispatch(
    updateIncarceration({
      storeId,
      postData: {
        common: { accountId: eValueAccountId, org, app },
        callingApplication: callingApplicationApr01 || '',
        operatorCode: operatorID || '',
        cardholderContactedPhoneNumber:
          accountDetail[storeId]?.numberPhoneAuthenticated || '',
        ...postData
      }
    })
  );

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message:
            type === TRIGGER_TYPE.EDIT
              ? I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATED
              : I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
        })
      );
      dispatch(collectionFormActions.setCurrentView({ storeId }));
      dispatch(
        collectionFormActions.selectCallResult({ storeId, item: undefined })
      );
      dispatch(
        collectionFormActions.changeMethod({
          storeId,
          method: COLLECTION_METHOD.CREATE
        })
      );

      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          forSection: 'inCollectionFormFlyOut',
          storeId
        })
      );

      dispatch(
        collectionFormActions.toggleCallResultInProgressStatus({
          storeId,
          info: postData,
          lastDelayCallResult: CALL_RESULT_CODE.INCARCERATION
        })
      );

      dispatchDelayProgress(
        incarcerationActions.getIncarceration({
          storeId
        }),
        storeId
      );

      setDelayProgress(storeId, CALL_RESULT_CODE.INCARCERATION, postData);
    });
  }

  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message:
            type === TRIGGER_TYPE.EDIT
              ? I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATE_FAILED
              : I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMIT_FAILED
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          apiResponse: response.payload?.response,
          forSection: 'inCollectionFormFlyOut'
        })
      );
    });
  }
});

export const triggerUpdateIncarcerationBuilder = (
  builder: ActionReducerMapBuilder<IncarcerationState>
) => {
  builder
    .addCase(triggerUpdateIncarceration.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true
      };
    })
    .addCase(triggerUpdateIncarceration.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false
      };
    })
    .addCase(triggerUpdateIncarceration.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false
      };
    });
};
