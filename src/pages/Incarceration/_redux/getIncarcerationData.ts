import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import isEmpty from 'lodash.isempty';
import { dispatchDestroyDelayProgress } from 'pages/CollectionForm/helpers';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';

// Service
import { incarcerationService } from '../api-services';
import { isEqualPostDataAndSavedData } from '../helpers';

// Type
import {
  GetIncarcerationArg,
  IncarcerationState,
  IncarcerationPayload
} from '../types';

export const getIncarceration = createAsyncThunk<
  IncarcerationPayload,
  GetIncarcerationArg,
  ThunkAPIConfig
>(
  'incarceration/getIncarceration',
  async (args, thunkAPI) => {
    const { storeId } = args;
    const { dispatch } = thunkAPI;
    const { accountDetail, collectionForm } = thunkAPI.getState();

    const { callingApplicationApr01, operatorID, org, app } =
      window.appConfig?.commonConfig || {};

    const { inProgressInfo = {}, isInProgress } = collectionForm[storeId] || {};

    const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;
    try {
      const {
        data: { latestIncarcerationDetail, latestIncarcerationStatus }
      } = await incarcerationService.getIncarceration({
        common: { accountId: eValueAccountId, org, app },
        callingApplication: callingApplicationApr01 || '',
        operatorCode: operatorID || '',
        requestType: 'GET_STATUS'
      });

      const latest =
        latestIncarcerationDetail?.incarcerationChangeHistoryList?.sort(
          (a: any, b: any) =>
            new Date(b.actionDateTime).getTime() -
            new Date(a.actionDateTime).getTime()
        )?.[0];
      const data: IncarcerationPayload = {
        ...latest,
        incarcerationStatus: latestIncarcerationStatus,
        incarcerationContactName: latest?.contactName
      };

      if (
        isEqualPostDataAndSavedData(inProgressInfo, data) &&
        !isEmpty(inProgressInfo) &&
        isInProgress
      ) {
        dispatch(
          collectionFormActions.toggleCallResultInProgressStatus({
            storeId,
            info: data
          })
        );
        dispatchDestroyDelayProgress(storeId);
      }
      if (data?.incarcerationStatus === 'INACTIVE') {
        dispatch(collectionFormActions.changeLastFollowUpData({ storeId }));
      }
      return data;
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  },
  {
    // Condition of redux toolkit prevent call api if this condition return false
    condition: (args, thunkAPI) => {
      const { storeId } = args;
      const loading = thunkAPI?.getState().incarceration[storeId]?.isLoading;
      if (loading) return false;
    }
  }
);

export const getIncarcerationBuilder = (
  builder: ActionReducerMapBuilder<IncarcerationState>
) => {
  builder
    .addCase(getIncarceration.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true
      };
    })
    .addCase(getIncarceration.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const data = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        data: data
      };
    })
    .addCase(getIncarceration.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false
      };
    });
};
