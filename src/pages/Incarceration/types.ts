export interface IncarcerationState {
  [storeId: string]: IncarcerationData;
}

export interface GetIncarcerationArg {
  storeId: string;
}

export interface GetIncarcerationRequestBody
  extends CommonAccountIdRequestBody {
  callingApplication: string;
  operatorCode: string;
  requestType: string;
}

export interface RemoveIncarcerationRequestBody
  extends CommonAccountIdRequestBody {
  callingApplication: string;
  operatorCode: string;
  requestType: string;
  contactName: string;
  cardholderContactedPhoneNumber: string;
}

export interface UpdateIncarcerationArg {
  storeId: string;
  postData: UpdateIncarcerationRequestBody;
}

export interface UpdateIncarcerationRequestBody
  extends CommonAccountIdRequestBody {
  callingApplication: string;
  operatorCode: string;
  cardholderContactedPhoneNumber: string;
  contactName: string;
  incarcerationStartDate: string;
  incarcerationEndDate: string;
  locationOfIncarceration: string;
}

export interface IncarcerationData {
  callResultType?: string;
  data?: IncarcerationPayload;
  isLoading?: boolean;
  isLoadingModal?: boolean;
  isOpenModal?: boolean;
}

export interface IncarcerationPayload {
  incarcerationStatus: string;
  incarcerationContactName: string;
  incarcerationStartDate: string;
  incarcerationEndDate: string;
  locationOfIncarceration: string;
}

export interface UpdateDataAction extends StoreIdPayload {
  data: IncarcerationPayload;
}

export interface UpdateDataFromLastFollowUpPayloadAction {
  storeId: string;
  callResultType?: string;
  data?: IncarcerationPayload;
}

export enum TRIGGER_TYPE {
  CREATE = 'create',
  EDIT = 'edit'
}

export interface TriggerUpdateIncarcerationArg extends StoreIdPayload {
  postData: {
    data?: IncarcerationPayload;
    type?: TRIGGER_TYPE;
  };
}
