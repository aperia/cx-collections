import { incarcerationService } from './api-services';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import {
  GetIncarcerationRequestBody,
  UpdateIncarcerationRequestBody
} from './types';

describe('incarcerationService', () => {
  describe('getIncarceration', () => {
    const params: GetIncarcerationRequestBody = {
      requestType: 'requestType',
      operatorCode: 'operatorCode',
      callingApplication: 'callingApplication'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      incarcerationService.getIncarceration(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      incarcerationService.getIncarceration(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.getIncarceration,
        params
      );
    });
  });

  describe('updateIncarceration', () => {
    const params: UpdateIncarcerationRequestBody = {
      callingApplication: 'callingApplication',
      operatorCode: 'operatorCode',
      cardholderContactedPhoneNumber: 'cardholderContactedPhoneNumber',
      contactName: 'contactName',
      incarcerationStartDate: '12/12/2021',
      incarcerationEndDate: '12/12/2021',
      locationOfIncarceration: 'locationOfIncarceration'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      incarcerationService.updateIncarceration(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      incarcerationService.updateIncarceration(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.updateIncarceration,
        params
      );
    });
  });

  describe('removeIncarceration', () => {
    const params: GetIncarcerationRequestBody = {
      requestType: 'requestType',
      operatorCode: 'operatorCode',
      callingApplication: 'callingApplication'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      incarcerationService.removeIncarceration(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      incarcerationService.removeIncarceration(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.removeIncarceration,
        params
      );
    });
  });
});
