import React from 'react';
import { screen } from '@testing-library/react';

// components
import Form from '.';

// hooks
import { AccountDetailProvider } from 'app/hooks';

// utils
import {
  renderMockStore,
  storeId,
  accEValue,
  mockActionCreator
} from 'app/test-utils';
import { I18N_COLLECTION_FORM, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { TRIGGER_TYPE } from '../types';

// redux
import * as reduxForm from 'redux-form';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { incarcerationActions } from '../_redux/reducers';
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';
import { actionsLiveVox } from 'app/livevox-dialer/_redux/reducers';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <Form />
    </AccountDetailProvider>,
    { initialState }
  );
};

const generateState = (method = TRIGGER_TYPE.CREATE) => {
  const initialState: Partial<RootState> = {
    liveVoxReducer: {
      sessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
      isAgentAuthenticated: true,
      lineNumber: 'ACD',
      accountNumber: '2345',
      lastInCallAccountNumber: '2345',
      inCallAccountNumber: '2345',
      call: {
        accountNumber: '5166480500018901',
        accountNumberRequired: true,
        callCenterId: 75,
        callRecordingStarted: true,
        callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
        callTransactionId: '126650403531',
        phoneNumber: '1234567890',
        serviceId: 1094568
      }
    },
    collectionForm: { [storeId]: {
      method,
      selectedCallResult: {
        code: 'PP',
        description: 'Promise to Pay'
      }
    } },
    incarceration: { [storeId]: { isLoading: false } },
    generalInformation: {
      [storeId]: {
        data: {
          delinquencyReason: 'delinquencyReason',
          nextWorkDate: new Date().toString()
        }
      }
    }
  };

  return initialState;
};

const generateStateWithEmptyAccountNumber = (method = TRIGGER_TYPE.CREATE) => {
  const initialState: Partial<RootState> = {
    liveVoxReducer: {
      sessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
      isAgentAuthenticated: true,
      lineNumber: 'ACD',
      accountNumber: '',
      lastInCallAccountNumber: '',
      inCallAccountNumber: '',
      call: {
        accountNumber: '',
        accountNumberRequired: true,
        callCenterId: 75,
        callRecordingStarted: true,
        callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
        callTransactionId: '126650403531',
        phoneNumber: '1234567890',
        serviceId: 1094568
      }
    },
    collectionForm: { [storeId]: {
      method,
      selectedCallResult: {
        code: 'PP',
        description: 'Promise to Pay'
      }
    } },
    incarceration: { [storeId]: { isLoading: false } },
    generalInformation: {
      [storeId]: {
        data: {
          delinquencyReason: 'delinquencyReason',
          nextWorkDate: new Date().toString()
        }
      }
    }
  };

  return initialState;
};

const collectionFormSpy = mockActionCreator(collectionFormActions);
const incarcerationSpy = mockActionCreator(incarcerationActions);
const liveVoxDialerSpy = mockActionCreator(actionsLiveVox);

describe('Render', () => {
  it('should render', () => {
    renderWrapper(generateState());

    expect(
      screen.getByText(I18N_COLLECTION_FORM.CALL_RESULT)
    ).toBeInTheDocument();
  });

  it('should render with empty state', () => {
    renderWrapper({});

    expect(
      screen.getByText(I18N_COLLECTION_FORM.CALL_RESULT)
    ).toBeInTheDocument();
  });

  it('should render when loading', () => {
    renderWrapper({
      incarceration: { [storeId]: { isLoading: true } }
    });

    expect(
      screen.getByText(I18N_COLLECTION_FORM.CALL_RESULT)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleCancel', () => {
    const mockSetCurrentView = collectionFormSpy('setCurrentView');
    const mockSelectCallResult = collectionFormSpy('selectCallResult');
    const mockChangeMethod = collectionFormSpy('changeMethod');

    renderWrapper(generateState());

    screen.getByText(I18N_COMMON_TEXT.CANCEL).click();

    expect(mockSetCurrentView).toBeCalledWith({ storeId });
    expect(mockSelectCallResult).toBeCalledWith({ storeId });
    expect(mockChangeMethod).toBeCalledWith({
      storeId,
      method: COLLECTION_METHOD.CREATE
    });
  });

  describe('handleSubmit', () => {
    it('when create', () => {
      const isDirtySpy = jest
        .spyOn(reduxForm, 'isDirty')
        .mockImplementation(() => () => true);

      const mockTriggerUpdateIncarceration = incarcerationSpy(
        'triggerUpdateIncarceration'
      );

      const mockLiveVoxDialerTermCodesThunk = liveVoxDialerSpy(
        'liveVoxDialerTermCodesThunk'
      );

      renderWrapper(generateState(TRIGGER_TYPE.CREATE));

      screen.getByText(I18N_COMMON_TEXT.SUBMIT).click();

      expect(mockLiveVoxDialerTermCodesThunk).toBeCalledWith({
        liveVoxDialerSessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
        liveVoxDialerLineNumber: 'ACD',
        selectedCallResult: {
          code: 'PP',
          description: 'Promise to Pay'
        },
        serviceId: 1094568,
        callTransactionId: '126650403531',
        callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
        account: '5166480500018901',
        phoneDialed: '1234567890',
        moveAgentToNotReady: false
      });

      expect(mockTriggerUpdateIncarceration).toBeCalledWith({
        storeId,
        postData: {
          type: TRIGGER_TYPE.CREATE
        }
      });

      isDirtySpy.mockReset();
      isDirtySpy.mockRestore();
    });

    it('should not dispatch DialerTermCodes when no accountNumber', () => {
      const isDirtySpy = jest
        .spyOn(reduxForm, 'isDirty')
        .mockImplementation(() => () => true);

      const mockTriggerUpdateIncarceration = incarcerationSpy(
        'triggerUpdateIncarceration'
      );

      const mockLiveVoxDialerTermCodesThunk = liveVoxDialerSpy(
        'liveVoxDialerTermCodesThunk'
      );

      renderWrapper(generateStateWithEmptyAccountNumber(TRIGGER_TYPE.CREATE));

      screen.getByTestId('incarceration_submit-btn_dls-button').click();

      expect(mockLiveVoxDialerTermCodesThunk).not.toBeCalled();

      expect(mockTriggerUpdateIncarceration).toBeCalledWith({
        storeId,
        postData: {
          type: TRIGGER_TYPE.CREATE
        }
      });

      isDirtySpy.mockReset();
      isDirtySpy.mockRestore();
    });

    it('when edit', () => {
      const isDirtySpy = jest
        .spyOn(reduxForm, 'isDirty')
        .mockImplementation(() => () => true);

      const mockTriggerUpdateIncarceration = incarcerationSpy(
        'triggerUpdateIncarceration'
      );

      renderWrapper(generateState(TRIGGER_TYPE.EDIT));

      screen.getByText(I18N_COMMON_TEXT.SAVE).click();

      expect(mockTriggerUpdateIncarceration).toBeCalledWith({
        storeId,
        postData: {
          type: TRIGGER_TYPE.EDIT
        }
      });

      isDirtySpy.mockReset();
      isDirtySpy.mockRestore();
    });
  });
});
