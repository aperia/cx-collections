import React, {
  Fragment,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef
} from 'react';

// components
import { Button, Icon } from 'app/_libraries/_dls/components';
import { View } from 'app/_libraries/_dof/core';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { incarcerationActions } from '../_redux/reducers';
import { getFormValues, isInvalid } from 'redux-form';
import { selectData } from '../_redux/selectors';
import { getCollectionFormMethod } from 'pages/CollectionForm/_redux/selectors';
import * as liveVoxSelectors from 'app/livevox-dialer/_redux/selectors';
import { actionsLiveVox } from 'app/livevox-dialer/_redux/reducers';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

//helper
import { I18N_COLLECTION_FORM, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';
import { IncarcerationPayload, TRIGGER_TYPE } from '../types';
import { prepareViewData } from '../helpers';
import SimpleBarWithApiError from 'pages/CollectionForm/SimpleBarWithApiError';
import { genAmtId } from 'app/_libraries/_dls/utils';

import { formatCommon } from 'app/helpers';
import { useDisableSubmitButton } from 'app/hooks/useDisableSubmitButton';
import { LiveVoxCallResultArgs } from 'app/livevox-dialer/types';
import { CallResult } from 'pages/CollectionForm/types';
import { takeSelectedCallResult } from 'pages/CollectionForm/_redux/selectors';
import { isEmpty } from 'app/_libraries/_dls/lodash';

export interface IncarcerationProps {}

const Incarceration: React.FC<IncarcerationProps> = () => {
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();
  const divRef = useRef<HTMLDivElement | null>(null);

  const incarcerationData =
    useStoreIdSelector<IncarcerationPayload>(selectData);

  const method = useStoreIdSelector<string>(getCollectionFormMethod);

  const isFormInvalid = useSelector(
    isInvalid(`${storeId}-incarcerationFormViews`)
  );

  const formValues: any = useSelector(
    getFormValues(`${storeId}-incarcerationFormViews`)
  );

  const isCreate = method === TRIGGER_TYPE.CREATE;
  const testId = 'incarceration';

  const viewData = useMemo(
    () => prepareViewData(incarcerationData),
    [incarcerationData]
  );

  // LiveVox
  const liveVoxDialerSessionId = useSelector(liveVoxSelectors.getLiveVoxDialerSessionId);
  const liveVoxDialerLineNumber = useSelector(liveVoxSelectors.getLiveVoxDialerLineNumber);
  const liveVoxDialerLineActiveAndReadyData = useSelector(liveVoxSelectors.getLiveVoxDialerLineActiveAndReadyData);
  const callResultSelected = useStoreIdSelector<CallResult>(
    takeSelectedCallResult
  );

  useEffect(() => {
    document.body.classList.add('hide-float-btn');
    return () => {
      document.body.classList.remove('hide-float-btn');
    };
  }, []);

  const handleCancel = () => {
    batch(() => {
      dispatch(collectionFormActions.setCurrentView({ storeId }));
      dispatch(
        collectionFormActions.selectCallResult({ storeId, item: undefined })
      );
      dispatch(
        collectionFormActions.changeMethod({
          storeId,
          method: COLLECTION_METHOD.CREATE
        })
      );
    });
  };

  const handleSubmit = () => {
    if (liveVoxDialerLineActiveAndReadyData && !isEmpty(liveVoxDialerLineActiveAndReadyData.accountNumber)) {
      const liveVoxTermCodesArgs: LiveVoxCallResultArgs = {
        liveVoxDialerSessionId: liveVoxDialerSessionId,
        liveVoxDialerLineNumber: liveVoxDialerLineNumber,
        selectedCallResult: callResultSelected,
        serviceId: liveVoxDialerLineActiveAndReadyData.serviceId,
        callTransactionId: liveVoxDialerLineActiveAndReadyData.callTransactionId,
        callSessionId: liveVoxDialerLineActiveAndReadyData.callSessionId,
        account: liveVoxDialerLineActiveAndReadyData.accountNumber,
        phoneDialed: liveVoxDialerLineActiveAndReadyData.phoneNumber,
        moveAgentToNotReady: false
      };
      dispatch(actionsLiveVox.liveVoxDialerTermCodesThunk(liveVoxTermCodesArgs));
    }

    dispatch(
      incarcerationActions.triggerUpdateIncarceration({
        storeId,
        postData: {
          type: isCreate ? TRIGGER_TYPE.CREATE : TRIGGER_TYPE.EDIT
        }
      })
    );
  };

  const { disableSubmit, setPrimitiveValues } = useDisableSubmitButton({
    ...formValues,
    startDay:
      formatCommon(formValues?.startDay || '').time.yearMonthDayWithHyphen ||
      '',
    endDay:
      formatCommon(formValues?.endDay || '').time.yearMonthDayWithHyphen || ''
  });

  useEffect(() => {
    if (isCreate) return;
    setPrimitiveValues({
      ...viewData,
      startDay:
        formatCommon(viewData?.startDay).time.yearMonthDayWithHyphen || '',
      endDay: formatCommon(viewData?.endDay).time.yearMonthDayWithHyphen || ''
    });
  }, [setPrimitiveValues, viewData, isCreate]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100vh - 258px)`);
  }, []);

  useEffect(() => {
    if (!isCreate) {
      dispatch(
        incarcerationActions.getIncarceration({
          storeId
        })
      );
    }
    return () => {
      dispatch(incarcerationActions.removeStore({ storeId }));
    };
  }, [dispatch, storeId, isCreate]);

  return (
    <Fragment>
      <div ref={divRef}>
        <SimpleBarWithApiError className="mt-24 ml-24">
          <div className="pt-24 px-24">
            <h5
              className="mr-auto"
              data-testid={genAmtId(testId, 'call-result', '')}
            >
              {t(I18N_COLLECTION_FORM.CALL_RESULT)}
            </h5>
            <div className="mt-16 p-8 bg-light-l16 d-flex align-items-center rounded-lg">
              <div className="bubble-icon">
                <Icon name="megaphone" />
              </div>

              <p className="ml-12" data-testid={genAmtId(testId, 'title', '')}>
                {t(I18N_COLLECTION_FORM.INCARCERATION)}
              </p>
            </div>
            <h6
              className="color-grey mt-24"
              data-testid={genAmtId(testId, 'information', '')}
            >
              {t(I18N_COLLECTION_FORM.INCARCERATION_INFORMATION)}
            </h6>
            <View
              id={`${storeId}-incarcerationFormViews`}
              formKey={`${storeId}-incarcerationFormViews`}
              descriptor="incarcerationFormViews"
              value={isCreate ? undefined : viewData}
            />
          </div>
        </SimpleBarWithApiError>
      </div>
      <div className="group-button-footer d-flex justify-content-end">
        <Button
          variant="secondary"
          onClick={handleCancel}
          dataTestId={`${testId}_cancel-btn`}
        >
          {t(I18N_COMMON_TEXT.CANCEL)}
        </Button>
        <Button
          disabled={isFormInvalid || (!isCreate && disableSubmit)}
          onClick={handleSubmit}
          dataTestId={`${testId}_submit-btn`}
        >
          {isCreate ? t(I18N_COMMON_TEXT.SUBMIT) : t(I18N_COMMON_TEXT.SAVE)}
        </Button>
      </div>
    </Fragment>
  );
};

export default Incarceration;
