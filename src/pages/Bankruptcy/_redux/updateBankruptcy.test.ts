import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import {
  triggerUpdateBankruptcy,
  updateBankruptcyRequest
} from './updateBankruptcy';
import { TriggerUpdateBankruptcyArgs } from '../types';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';
import {
  byPassFulfilled,
  byPassRejected,
  responseDefault,
  storeId,
  mockActionCreator
} from 'app/test-utils';
import * as helperCommon from 'app/helpers/commons';
import bankruptcyService from '../bankruptcyService';
import { FAILURE_MSG } from 'app/constants';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { bankruptcyActions } from './reducers';
import * as collectionFormHelper from 'pages/CollectionForm/helpers';
import { BANKRUPTCY_TYPE_JOINT, BANKRUPTCY_TYPE_SINGLE } from '../constants';

const mockArg: TriggerUpdateBankruptcyArgs = {
  storeId: storeId,
  postData: {
    accountId: 'mockStoreId',
    viewName: ['name', 'test'],
    type: COLLECTION_METHOD.EDIT,
    method: undefined
  }
};
let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    operatorID: '123'
  } as CommonConfig
};

describe('Test Update bankruptcy async action', () => {
  it('should be fulfill with default accountDetail', async () => {
    const store = createStore(rootReducer, {
      form: { name: { values: { test: '11' } }, test: {} }
    });
    jest
      .spyOn(helperCommon, 'getFeatureConfig')
      .mockResolvedValue({ selectFields: [] });
    jest
      .spyOn(bankruptcyService, 'updateDetailForCallResultType')
      .mockResolvedValue({ ...responseDefault, data: { value: '123' } });

    const data = await updateBankruptcyRequest(mockArg)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(data.type).toEqual('bankruptcy/updateBankruptcyRequest/fulfilled');
    expect(data.payload).toEqual({ value: '123' });
  });

  it('should be fulfill when updateBankruptcyRequest', async () => {
    const store = createStore(rootReducer, {
      form: { name: { values: { test: '11' } }, test: {} },
      accountDetail: {
        [storeId]: {
          data: {
            primaryName: 'text'
          },
          customerInquiry: [
            { customerRoleTypeCode: '01', delinquentAmount: '1' },
            { customerRoleTypeCode: '02', delinquentAmount: '2' }
          ]
        }
      }
    });
    const arg = {
      ...mockArg,
      formData: {
        filerName: 'text',
        bankruptcyType: BANKRUPTCY_TYPE_JOINT.value,
        bankruptcyChapterCode: '13',
        bankruptcyCaseIdentifier: 'ABC'
      }
    };
    jest
      .spyOn(helperCommon, 'getFeatureConfig')
      .mockResolvedValue({ selectFields: [] });
    jest
      .spyOn(bankruptcyService, 'updateDetailForCallResultType')
      .mockResolvedValue({ ...responseDefault, data: { value: '123' } });

    const data = await updateBankruptcyRequest(arg)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(data.type).toEqual('bankruptcy/updateBankruptcyRequest/fulfilled');
    expect(data.payload).toEqual({ value: '123' });
  });

  it('should be fulfill when primarySSN', async () => {
    const store = createStore(rootReducer, {
      form: { name: { values: { test: '11' } }, test: {} },
      accountDetail: {
        [storeId]: {
          data: {
            primaryName: 'text'
          },
          customerInquiry: [
            { customerRoleTypeCode: '01', delinquentAmount: '1' },
            { customerRoleTypeCode: '02', delinquentAmount: '2' }
          ]
        }
      }
    });
    const arg = {
      ...mockArg,
      formData: {
        filerName: 'text different primaryName',
        bankruptcyType: BANKRUPTCY_TYPE_JOINT.value,
        bankruptcyChapterCode: '13',
        bankruptcyCaseIdentifier: 'ABC'
      }
    };
    jest
      .spyOn(helperCommon, 'getFeatureConfig')
      .mockResolvedValue({ selectFields: [] });
    jest
      .spyOn(bankruptcyService, 'updateDetailForCallResultType')
      .mockResolvedValue({ ...responseDefault, data: { value: '123' } });

    const data = await updateBankruptcyRequest(arg)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(data.type).toEqual('bankruptcy/updateBankruptcyRequest/fulfilled');
    expect(data.payload).toEqual({ value: '123' });
  });

  it('should be rejected when updateBankruptcyRequest', async () => {
    window.appConfig = {} as AppConfiguration;
    const store = createStore(rootReducer, {
      form: { name: { values: { test: '11' } }, test: {} },
      accountDetail: {
        [storeId]: {
          data: {
            primaryName: 'text'
          }
        }
      }
    });
    const arg = {
      ...mockArg,
      formData: {
        filerName: 'no-text',
        bankruptcyType: BANKRUPTCY_TYPE_SINGLE.value
      }
    };
    jest
      .spyOn(helperCommon, 'getFeatureConfig')
      .mockResolvedValue({ selectFields: [] });
    jest
      .spyOn(bankruptcyService, 'updateDetailForCallResultType')
      .mockResolvedValue({ ...responseDefault, data: { status: FAILURE_MSG } });

    const data = await updateBankruptcyRequest(arg)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(data.type).toEqual('bankruptcy/updateBankruptcyRequest/rejected');
  });
});

describe('it should update collection form builder correctly', () => {
  const store = createStore(rootReducer, {
    form: { name: { values: { test: '11' } } },
    accountDetail: {
      [storeId]: {
        data: { cardHolderSecondaryName: '123' }
      }
    }
  });
  it('check builder > pending', async () => {
    const state = store.getState();
    const pending = updateBankruptcyRequest.pending(storeId, {
      storeId
    });

    const actual = rootReducer(state, pending);
    expect(actual.bankruptcy[storeId].isUpdating).toEqual(true);
  });

  it('check builder > fulfilled', async () => {
    const state = store.getState();
    const fulfilled = updateBankruptcyRequest.fulfilled(
      { storeId },
      '',
      mockArg
    );
    const actual = rootReducer(state, fulfilled);
    expect(actual.bankruptcy[storeId].isUpdating).toEqual(false);
  });

  it('check builder > rejected', async () => {
    const state = store.getState();
    const rejected = updateBankruptcyRequest.rejected(null, '', mockArg);
    const actual = rootReducer(state, rejected);
    expect(actual.bankruptcy[storeId].isUpdating).toEqual(false);
  });
});

describe('it should trigger action correctly after update bankruptcy', () => {
  const store = createStore(rootReducer, {
    form: { name: { values: { test: '11' } } },
    accountDetail: {
      [storeId]: {
        data: { cardHolderSecondaryName: '123' }
      }
    }
  });
  it('it should toggle modal > fulfilled', async () => {
    jest
      .spyOn(helperCommon, 'getFeatureConfig')
      .mockResolvedValue({ selectFields: [] });
    jest
      .spyOn(bankruptcyService, 'updateDetailForCallResultType')
      .mockResolvedValue({ ...responseDefault, data: {} });
    const collectionFormSpy = mockActionCreator(collectionFormActions);
    const toggleModalSpy = collectionFormSpy('toggleModal');

    expect(toggleModalSpy).not.toBeCalledWith({ storeId });
    await updateBankruptcyRequest(mockArg)(store.dispatch, store.getState, {});
    await triggerUpdateBankruptcy(mockArg)(
      byPassFulfilled(triggerUpdateBankruptcy.fulfilled.type),
      store.getState,
      {}
    );
    expect(toggleModalSpy).toBeCalledWith({ storeId });
  });

  it('it should show toast message correctly > fulfilled', async () => {
    jest
      .spyOn(helperCommon, 'getFeatureConfig')
      .mockResolvedValue({ selectFields: [] });
    jest
      .spyOn(bankruptcyService, 'updateDetailForCallResultType')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const toastSpy = mockActionCreator(actionsToast);
    const addToastActionSpy = toastSpy('addToast');

    const mogArgParams = { ...mockArg };
    mogArgParams.postData.method = COLLECTION_METHOD.EDIT;

    await updateBankruptcyRequest(mockArg)(store.dispatch, store.getState, {});
    await triggerUpdateBankruptcy(mogArgParams)(
      byPassFulfilled(triggerUpdateBankruptcy.fulfilled.type),
      store.getState,
      {}
    );
    expect(addToastActionSpy).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATED
    });

    mogArgParams.postData.method = COLLECTION_METHOD.CREATE;
    await updateBankruptcyRequest(mockArg)(store.dispatch, store.getState, {});
    await triggerUpdateBankruptcy(mogArgParams)(
      byPassFulfilled(triggerUpdateBankruptcy.fulfilled.type),
      store.getState,
      {}
    );

    expect(addToastActionSpy).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
    });
  });

  it('it should be change method collection form', async () => {
    jest
      .spyOn(helperCommon, 'getFeatureConfig')
      .mockResolvedValue({ selectFields: [] });
    jest
      .spyOn(bankruptcyService, 'updateDetailForCallResultType')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const collectionFormSpy = mockActionCreator(collectionFormActions);
    const changeMethodSpy = collectionFormSpy('changeMethod');
    await updateBankruptcyRequest(mockArg)(store.dispatch, store.getState, {});
    await triggerUpdateBankruptcy(mockArg)(
      byPassFulfilled(triggerUpdateBankruptcy.fulfilled.type),
      store.getState,
      {}
    );

    expect(changeMethodSpy).toBeCalledWith({
      storeId,
      method: COLLECTION_METHOD.CREATE
    });
  });

  it('it should be get Latest Action Request collection form', async () => {
    jest
      .spyOn(helperCommon, 'getFeatureConfig')
      .mockResolvedValue({ selectFields: [] });
    jest
      .spyOn(bankruptcyService, 'updateDetailForCallResultType')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const collectionFormSpy = mockActionCreator(collectionFormActions);
    const getLatestActionRequestSpy = collectionFormSpy(
      'getLatestActionRequest'
    );
    await updateBankruptcyRequest(mockArg)(store.dispatch, store.getState, {});
    await triggerUpdateBankruptcy(mockArg)(
      byPassFulfilled(triggerUpdateBankruptcy.fulfilled.type),
      store.getState,
      {}
    );

    expect(getLatestActionRequestSpy).toBeCalledWith({
      storeId
    });
  });

  it('it should show toast message correctly > rejected', async () => {
    jest
      .spyOn(helperCommon, 'getFeatureConfig')
      .mockResolvedValue({ selectFields: [] });
    jest
      .spyOn(bankruptcyService, 'updateDetailForCallResultType')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const toastSpy = mockActionCreator(actionsToast);
    const addToastActionSpy = toastSpy('addToast');

    const mogArgParams = { ...mockArg };
    mogArgParams.postData.method = COLLECTION_METHOD.EDIT;
    await updateBankruptcyRequest(mockArg)(store.dispatch, store.getState, {});
    await triggerUpdateBankruptcy(mogArgParams)(
      byPassRejected(triggerUpdateBankruptcy.rejected.type),
      store.getState,
      {}
    );
    expect(addToastActionSpy).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATE_FAILED
    });

    mogArgParams.postData.method = COLLECTION_METHOD.CREATE;
    await updateBankruptcyRequest(mockArg)(store.dispatch, store.getState, {});
    await triggerUpdateBankruptcy(mogArgParams)(
      byPassRejected(triggerUpdateBankruptcy.rejected.type),
      store.getState,
      {}
    );
    expect(addToastActionSpy).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMIT_FAILED
    });
  });

  it('should be work delay progress correctly', async () => {
    jest
      .spyOn(helperCommon, 'getFeatureConfig')
      .mockResolvedValue({ selectFields: [] });
    jest
      .spyOn(bankruptcyService, 'updateDetailForCallResultType')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const collectionFormSpy = mockActionCreator(collectionFormActions);
    const bankruptcyActionsSpy = mockActionCreator(bankruptcyActions);
    const setDelayProgressSpy = jest.spyOn(
      collectionFormHelper,
      'setDelayProgress'
    );

    const getBankruptcyInformationSpy = bankruptcyActionsSpy(
      'getBankruptcyInformation'
    );
    const toggleCallResultInProgressStatusSpy = collectionFormSpy(
      'toggleCallResultInProgressStatus'
    );

    const mockArgParam = { ...mockArg };
    await updateBankruptcyRequest(mockArg)(store.dispatch, store.getState, {});
    await triggerUpdateBankruptcy(mockArgParam)(
      byPassFulfilled(triggerUpdateBankruptcy.fulfilled.type),
      store.getState,
      {}
    );

    expect(setDelayProgressSpy).toBeCalledWith(storeId, 'edit', {});
    expect(getBankruptcyInformationSpy).toBeCalled();
    expect(toggleCallResultInProgressStatusSpy).toBeCalled();
  });

  it('should be work delay progress correctly with type undefined', async () => {
    jest
      .spyOn(helperCommon, 'getFeatureConfig')
      .mockResolvedValue({ selectFields: [] });
    jest
      .spyOn(bankruptcyService, 'updateDetailForCallResultType')
      .mockResolvedValue({ ...responseDefault, data: {} });
    const setDelayProgressSpy = jest.spyOn(
      collectionFormHelper,
      'setDelayProgress'
    );

    const mockArgParam = { ...mockArg };
    mockArgParam.postData.type = undefined;
    await updateBankruptcyRequest(mockArgParam)(
      store.dispatch,
      store.getState,
      {}
    );
    await triggerUpdateBankruptcy(mockArgParam)(
      byPassFulfilled(triggerUpdateBankruptcy.fulfilled.type),
      store.getState,
      {}
    );

    expect(setDelayProgressSpy).toBeCalledWith(storeId, '', {});
  });

  it('should not call work delay progress', async () => {
    jest
      .spyOn(helperCommon, 'getFeatureConfig')
      .mockResolvedValue({ selectFields: [] });
    jest
      .spyOn(bankruptcyService, 'updateDetailForCallResultType')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const collectionFormSpy = mockActionCreator(collectionFormActions);
    const bankruptcyActionsSpy = mockActionCreator(bankruptcyActions);
    const setDelayProgressSpy = jest.spyOn(
      collectionFormHelper,
      'setDelayProgress'
    );

    const getBankruptcyInformationSpy = bankruptcyActionsSpy(
      'getBankruptcyInformation'
    );
    const toggleCallResultInProgressStatusSpy = collectionFormSpy(
      'toggleCallResultInProgressStatus'
    );

    const mockArgParam = { ...mockArg };

    mockArgParam.postData.viewName = [];
    await updateBankruptcyRequest(mockArg)(store.dispatch, store.getState, {});
    await triggerUpdateBankruptcy(mockArgParam)(
      byPassFulfilled(triggerUpdateBankruptcy.fulfilled.type),
      store.getState,
      {}
    );

    expect(setDelayProgressSpy).not.toBeCalled();
    expect(getBankruptcyInformationSpy).not.toBeCalled();
    expect(toggleCallResultInProgressStatusSpy).not.toBeCalled();
  });
});
