import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import unique from 'lodash.uniq';

// Service
import bankruptcyService from '../bankruptcyService';

// Types
import {
  BankruptcyResult,
  BankruptcyState,
  GetBankruptcyArgs,
  GetBankruptcyPayload,
  GetBankruptcyResponseData
} from '../types';
import { BankruptcyConfiguration } from 'app/global-types/bankruptcy';

// Const
import {
  BANKRUPTCY_COMPLETE,
  BANKRUPTCY_CONFIG,
  BANKRUPTCY_IGNORE_TRIM
} from '../constants';

// Helper
import {
  getFeatureConfig,
  mappingDataFromObj,
  removeUnMatchingData,
  trimData
} from 'app/helpers';
import { dispatchDestroyDelayProgress } from 'pages/CollectionForm/helpers';
import { buildAmountDataField, mappingDescription } from '../helpers';

// Reducer
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { apiService } from 'app/utils/api.service';
import { AxiosResponse } from 'axios';

const checkAmount = (data: string) => {
  return data && Number(data) === 0 ? '' : data;
};

export const getBankruptcyInformation = createAsyncThunk<
  GetBankruptcyPayload,
  GetBankruptcyArgs,
  ThunkAPIConfig
>('bankruptcy/getBankruptcyInformation', async (args, thunkAPI) => {
  const { selectFields } = await getFeatureConfig<BankruptcyConfiguration>(
    BANKRUPTCY_CONFIG
  );
  let viewData: BankruptcyResult = {};

  const { storeId, mapValueWithDescription } = args;
  const { commonConfig } = window.appConfig || {};
  const { app, org } = commonConfig || {};
  const { dispatch, getState } = thunkAPI;
  const { mapping, collectionForm, accountDetail } = getState();
  const { inProgressInfo = {}, isInProgress } = collectionForm[storeId] || {};

  let data = {} as GetBankruptcyResponseData;

  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;

  try {
    ({ data } = await bankruptcyService.getBankruptcyInformation({
      common: {
        app,
        org,
        accountId: eValueAccountId
      },
      selectFields
    }));
  } catch (error) {}

  const bankruptcyMapping = mapping?.data?.bankruptcyInfo || {};
  const [bankruptcyInfo] = data?.bankruptcyInformationList || [];
  const info = trimData(
    bankruptcyInfo || {
      bankruptcyCurrentBalanceAmount: ''
    },
    BANKRUPTCY_IGNORE_TRIM
  );

  const covertData = dispatch(
    buildAmountDataField(
      {
        ...info,
        originalPlanAmount: checkAmount(info?.originalPlanAmount),
        planPaymentAmount: checkAmount(info?.planPaymentAmount),
        planDifferenceAmount: checkAmount(info?.planDifferenceAmount)
      },
      storeId
    )
  );
  const mappedData = mappingDataFromObj(covertData, bankruptcyMapping);
  const rawInfo = removeUnMatchingData(inProgressInfo, covertData);

  const processStatus = data?.processStatus;

  if (processStatus === BANKRUPTCY_COMPLETE && isInProgress) {
    dispatch(
      collectionFormActions.toggleCallResultInProgressStatus({
        storeId,
        info: rawInfo
      })
    );
    dispatchDestroyDelayProgress(storeId);
  }

  if (mapValueWithDescription) {
    // get reference to get description BUG:https://jira.aperia.local/browse/CXC-5027
    const bankruptcyRefUrls = {
      voluntarySurrenderIndicator: 'refData/surrenderIndicator.json',
      accountNonfilerIndicator: 'refData/nonfilerInd.json',
      accountNonfilerCollectibleIndicator: 'refData/nonfierCollectible.json',
      primaryCreditBureauReportCode: 'refData/creditBureauCode.json',
      secondaryCreditBureauReportCode: 'refData/creditBureauCode.json',
      primaryConsumerInformationIndicatorCode: 'refData/customerInfoCode.json',
      secondaryConsumerInformationIndicatorCode: 'refData/customerInfoCode.json'
    };

    const allRefUrlKeys = Object.keys(
      bankruptcyRefUrls
    ) as (keyof typeof bankruptcyRefUrls)[];

    const allRefDataValues = unique(Object.values(bankruptcyRefUrls));

    const allRefPromise = allRefDataValues.map((url: string) =>
      apiService.get(url)
    );

    const refResponse = await Promise.allSettled(allRefPromise);

    const successfulData = refResponse.filter(
      response => response.status === 'fulfilled'
    ) as PromiseFulfilledResult<AxiosResponse<RefDataValue[]>>[];

    const refDataByUrl = successfulData.reduce((pre, cur) => {
      const { value } = cur;
      pre[value.config.url!] = value.data;

      return pre;
    }, {} as MagicKeyValue);

    const refDataByField = allRefUrlKeys.reduce((pre, cur) => {
      const refUrl = bankruptcyRefUrls[cur];
      if (refDataByUrl[refUrl]) {
        pre[cur] = refDataByUrl[refUrl];
      }
      return pre;
    }, {} as MagicKeyValue);

    const mappedDescription = mappingDescription(info, refDataByField);
    const mappingData = mappingDataFromObj(
      mappedDescription,
      bankruptcyMapping
    );
    viewData = {
      ...mappingData,
      mainInfo: {
        ...mappingData?.mainInfo,
        originalPlanAmount: checkAmount(
          mappingData.mainInfo?.originalPlanAmount
        ),
        planPaymentAmount: checkAmount(mappingData.mainInfo?.planPaymentAmount),
        planDifferenceAmount: checkAmount(
          mappingData.mainInfo?.planDifferenceAmount
        )
      }
    };
  }

  return {
    info: covertData,
    data: mappedData,
    rawData: bankruptcyInfo,
    viewData
  };
});

export const getBankruptcyInformationBuilder = (
  builder: ActionReducerMapBuilder<BankruptcyState>
) => {
  builder
    .addCase(getBankruptcyInformation.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true
      };
    })
    .addCase(getBankruptcyInformation.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data, info, rawData, viewData } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        data,
        info,
        rawData,
        viewData
      };
    })
    .addCase(getBankruptcyInformation.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false
      };
    });
};
