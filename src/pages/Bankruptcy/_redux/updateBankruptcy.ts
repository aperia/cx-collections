import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { batch } from 'react-redux';
import isEqual from 'lodash.isequal';

// Types
import {
  BankruptcyState,
  TriggerUpdateBankruptcyArgs,
  UpdateBankruptcyRequestArg,
  UpdateBankruptcyRequestPayload
} from '../types';
import { BankruptcyConfiguration } from 'app/global-types/bankruptcy';

// Const
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { CUSTOMER_ROLE_CODE, EMPTY_STRING, FAILURE_MSG } from 'app/constants';
import { BANKRUPTCY_CONFIG, BANKRUPTCY_TYPE_SINGLE } from '../constants';

// Service
import bankruptcyService from '../bankruptcyService';

// Redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { bankruptcyActions } from './reducers';

// Helper
import {
  getDOFViewValue,
  getFeatureConfig,
  parseJSONString,
  removeUnMatchingData
} from 'app/helpers';
import {
  dispatchDelayProgress,
  setDelayProgress
} from 'pages/CollectionForm/helpers';
import { getRawViewValue } from '../helpers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export const updateBankruptcyRequest = createAsyncThunk<
  UpdateBankruptcyRequestPayload,
  UpdateBankruptcyRequestArg,
  ThunkAPIConfig
>('bankruptcy/updateBankruptcyRequest', async (args, thunkAPI) => {
  const { common, formData, storeId, type = EMPTY_STRING } = args;
  const { operatorID = '' } = window.appConfig.commonConfig || {};

  const { getState } = thunkAPI;
  const { accountDetail } = getState();
  const {
    customerNameForCreateWorkflow: cardholderContactedName = EMPTY_STRING
  } = accountDetail[storeId]?.selectedAuthenticationData || {};
  const { primaryName, secondaryName } = accountDetail[storeId]?.data || {};

  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;
  const isFilerNameEqualPrimary = formData?.filerName === primaryName;
  const jointFilerName = isFilerNameEqualPrimary ? secondaryName : primaryName;
  const primarySSN = parseJSONString(
    accountDetail[storeId]?.customerInquiry?.find(
      customer => customer?.customerRoleTypeCode === CUSTOMER_ROLE_CODE.primary
    )?.socialSecurityIdentifier
  )?.eValue;
  const secondarySSN = parseJSONString(
    accountDetail[storeId]?.customerInquiry?.find(
      customer =>
        customer?.customerRoleTypeCode === CUSTOMER_ROLE_CODE.secondary
    )?.socialSecurityIdentifier
  )?.eValue;

  try {
    const { defaultFields = {} } =
      await getFeatureConfig<BankruptcyConfiguration>(BANKRUPTCY_CONFIG);

    const response = await bankruptcyService.updateDetailForCallResultType({
      common: {
        ...common,
        accountId: eValueAccountId
      },
      operatorCode: operatorID,
      ...formData,
      ...defaultFields,
      actionEntryCode: type,
      cardholderContactedName,
      bankruptcyCaseIdentifier: formData?.bankruptcyCaseIdentifier
        ? formData?.bankruptcyCaseIdentifier
        : ' ',
      cardholderEntityType:
        accountDetail[storeId]?.data?.separateEntityIndicator,
      filerSocialSecurityNumberIdentifier: isFilerNameEqualPrimary
        ? primarySSN
        : secondarySSN,
      jointFilerName:
        formData?.bankruptcyType === BANKRUPTCY_TYPE_SINGLE?.value
          ? ''
          : jointFilerName,
      jointFilerSocialSecurityNumberIdentifier:
        formData?.bankruptcyType === BANKRUPTCY_TYPE_SINGLE?.value
          ? ''
          : isFilerNameEqualPrimary
          ? secondarySSN
          : primarySSN,
      originalPlanAmount:
        formData?.bankruptcyChapterCode === '13'
          ? formData?.originalPlanAmount
          : '000',
      planDifferenceAmount:
        formData?.bankruptcyChapterCode === '13'
          ? formData?.planDifferenceAmount
          : '000',
      planPaymentAmount:
        formData?.bankruptcyChapterCode === '13'
          ? formData?.planPaymentAmount
          : '000'
    });

    // If bankruptcy return with failure msg
    if (response?.data?.status === FAILURE_MSG) {
      throw response;
    }
    const { data } = response;

    return data;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerUpdateBankruptcy = createAsyncThunk<
  unknown,
  TriggerUpdateBankruptcyArgs,
  ThunkAPIConfig
>('bankruptcy/triggerUpdateBankruptcy', async (args, thunkAPI) => {
  const { storeId, postData } = args;
  const { viewName, method, accountId, type } = postData;
  const { dispatch, getState } = thunkAPI;
  const { form, bankruptcy, collectionForm } = getState();
  const { app, org } = window.appConfig?.commonConfig || {};

  const { selectedCallResult, lastFollowUpData } =
    collectionForm[storeId] || {};

  const { mappingFields = {} } =
    await getFeatureConfig<BankruptcyConfiguration>(BANKRUPTCY_CONFIG);

  let viewDataInfo = {};
  let viewValue = {};
  let values: MagicKeyValue = {};
  let rawValues: MagicKeyValue = {};
  const { info = {} } = bankruptcy[storeId];

  viewName?.forEach(item => {
    values = form[item]?.values || {};
    viewValue = getDOFViewValue(values, mappingFields);

    viewDataInfo = { ...viewDataInfo, ...viewValue };
    rawValues = { ...rawValues, ...getRawViewValue(values) };
  });

  const currentBankruptcyData = removeUnMatchingData(rawValues, info);

  const response = await dispatch(
    updateBankruptcyRequest({
      storeId,
      common: {
        accountId,
        app,
        org
      },
      formData: viewDataInfo,
      method,
      type
    })
  );

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(collectionFormActions.toggleModal({ storeId }));
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message:
            method === COLLECTION_METHOD.EDIT
              ? I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATED
              : I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
        })
      );

      dispatch(
        collectionFormActions.changeMethod({
          storeId,
          method: method || ''
        })
      );

      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inCollectionFormModal'
        })
      );

      dispatch(
        collectionFormActions.getLatestActionRequest({
          storeId
        })
      );

      method === COLLECTION_METHOD.CREATE &&
        dispatch(
          collectionFormActions.openUploadFileModal({
            storeId,
            type: 'bankruptcyDocument'
          })
        );

      if (
        !isEqual(rawValues, currentBankruptcyData) ||
        lastFollowUpData?.callResultType !== selectedCallResult?.code
      ) {
        dispatch(
          collectionFormActions.toggleCallResultInProgressStatus({
            storeId,
            info: {},
            lastDelayCallResult: type
          })
        );
        dispatchDelayProgress(
          bankruptcyActions.getBankruptcyInformation({
            storeId
          }),
          storeId
        );

        setDelayProgress(storeId, type || '', {});
      }
    });
  }

  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message:
            method === COLLECTION_METHOD.EDIT
              ? I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATE_FAILED
              : I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMIT_FAILED
        })
      );

      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inCollectionFormModal',
          apiResponse: response.payload?.response
        })
      );
    });
  }
});

export const updateCollectionFormBuilder = (
  builder: ActionReducerMapBuilder<BankruptcyState>
) => {
  builder
    .addCase(updateBankruptcyRequest.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isUpdating: true
      };
    })
    .addCase(updateBankruptcyRequest.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isUpdating: false
      };
    })
    .addCase(updateBankruptcyRequest.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isUpdating: false
      };
    });
};
