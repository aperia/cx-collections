import * as selectors from './selectors';
import { selectorWrapper, storeId } from 'app/test-utils';
import { BankruptcyData } from '../types';

const SAMPLE_DATA_BANKRUPTCY = {
  data: {
    additionalAttorneyInfo: {
      attorneyInfoAddressLineOne: '',
      attorneyInfoAddressLineTwo: '',
      attorneyInfoCity: '',
      attorneyInfoState: '',
      attorneyInfoZip: ''
    },
    clerkCourtInfo: {
      clerkCourtInfoName: '',
      clerkCourtInfoPhone: '',
      clerkCourtInfoEmail: 'clerk@aperia.com',
      clerkCourtInfoAddressLineOne: '',
      clerkCourtInfoAddressLineTwo: ''
    },
    fillingInfo: {
      fillingInfoType: 'Single',
      fillingInfoAssets: '',
      fillingInfoJurisdiction: '',
      fillingInfoFileDate: '10/10/2010',
      fillingInfoPOCAmount: '100'
    },
    mainInfo: {
      case: '2222',
      chapter: '7',
      attorneyName: 'GEORGE FORD APERIA',
      attorneyPhoneNumber: '4034031243'
    },
    trusteeInfo: {
      trusteeInfoName: '',
      trusteeInfoPhone: '',
      trusteeInfoEmail: 'trustee@aperia.com',
      trusteeInfoAddressLineOne: '',
      trusteeInfoAddressLineTwo: ''
    }
  },
  info: {
    accountNonfilerCollectibleIndicator: ' ',
    accountNonfilerIndicator: ' ',
    accountRedeemedAmount: '00000000000000.00',
    accountSettledAmount: '00000000000000.00',
    adjustedBalanceAmount: '00000000001110.00'
  }
};

const SAMPLE_DATA_COLLECTION_FORM = {
  lastFollowUpData: {
    callResultType: 'PR',
    data: {}
  }
};

const bankruptcyData: BankruptcyData = {
  info: { ...SAMPLE_DATA_BANKRUPTCY.info },
  isLoading: false,
  isUpdating: false,
  creditBureauCodeRefData: [
    {
      value: 'name',
      description: 'name'
    }
  ],
  data: {},
  viewData: {
    ...SAMPLE_DATA_BANKRUPTCY.data
  },
  assetsRefData: [{ description: 'AssetDataTest', value: '999' }],
  proSERefData: [{ description: 'ProSEDataTest', value: '888' }]
};

const statesBankruptcy: Partial<RootState> = {
  bankruptcy: {
    [storeId]: {
      ...bankruptcyData
    }
  }
};

const statesCollectionForm: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      lastFollowUpData: { ...SAMPLE_DATA_COLLECTION_FORM.lastFollowUpData },
      caseDocumentFiles: {
        bankruptcyDocument: {
          files: [],
          error: false,
          loading: false
        }
      },
      uploadFiles: {}
    }
  }
};

const selectorTrigger = selectorWrapper(statesBankruptcy);

const selectorTriggerCollectionForm = selectorWrapper(statesCollectionForm);

describe('Test selectData', () => {
  it('should return info', () => {
    const { data, emptyData } = selectorTrigger(selectors.takeBankruptcyInfo);
    expect(data).toEqual(SAMPLE_DATA_BANKRUPTCY.info);
    expect(emptyData).toEqual(undefined);
  });

  it('should return BankruptcyResult', () => {
    const { data, emptyData } = selectorTrigger(selectors.takeBankruptcyResult);
    expect(data).toEqual(bankruptcyData.viewData);
    expect(emptyData).toEqual(undefined);
  });

  it('should return bankruptcy Main Info', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectBankruptcyMainInfo
    );
    expect(data).toEqual(SAMPLE_DATA_BANKRUPTCY.data.mainInfo);
    expect(emptyData).toEqual({});
  });

  it('should return bankruptcy Filling Info', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.takeBankruptcyFillingInfo
    );
    expect(data).toEqual(SAMPLE_DATA_BANKRUPTCY.data.fillingInfo);
    expect(emptyData).toEqual(undefined);
  });

  it('should return bankruptcy Attorney Info', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.takeBankruptcyAttorneyInfo
    );
    expect(data).toEqual(SAMPLE_DATA_BANKRUPTCY.data.additionalAttorneyInfo);
    expect(emptyData).toEqual({});
  });

  it('should return bankruptcy Trustee Info', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.takeBankruptcyTrusteeInfo
    );
    expect(data).toEqual(SAMPLE_DATA_BANKRUPTCY.data.trusteeInfo);
    expect(emptyData).toEqual({});
  });

  it('should return bankruptcy Clerk Info', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.takeBankruptcyClerkCourtInfo
    );
    expect(data).toEqual(SAMPLE_DATA_BANKRUPTCY.data.clerkCourtInfo);
    expect(emptyData).toEqual({});
  });

  it('should return bankruptcy data', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectBankruptcyData);
    expect(data).toEqual({});
    expect(emptyData).toEqual(null);
  });

  it('should return bankruptcy collection form data', () => {
    const { data, emptyData } = selectorTriggerCollectionForm(
      selectors.takeCollectionFormData
    );
    expect(data).toEqual(SAMPLE_DATA_COLLECTION_FORM.lastFollowUpData);
    expect(emptyData).toEqual(undefined);
  });

  it('should return bankruptcy loading', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.takeBankruptcyLoadingStatus
    );
    expect(data).toEqual(false);
    expect(emptyData).toEqual(undefined);
  });

  it('should return bankruptcy updating', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.getBankruptcyUpdateStatus
    );
    expect(data).toEqual(false);
    expect(emptyData).toEqual(undefined);
  });

  it('should return creditBureauCodeRefData', () => {
    const { data, emptyData } = selectorTrigger(selectors.takeCreditBureauCode);
    expect(data).toEqual([
      {
        value: 'name',
        description: 'name'
      }
    ]);
    expect(emptyData).toEqual(undefined);
  });

  it('should return assetsRefData', () => {
    const { data, emptyData } = selectorTrigger(selectors.assetsRefData);
    expect(data).toEqual([{ description: 'AssetDataTest', value: '999' }]);
    expect(emptyData).toEqual(undefined);
  });

  it('should return proSERefData', () => {
    const { data, emptyData } = selectorTrigger(selectors.proSERefData);
    expect(data).toEqual([{ description: 'ProSEDataTest', value: '888' }]);
    expect(emptyData).toEqual(undefined);
  });

  it('should return getLoadingRefData', () => {
    const { data, emptyData } = selectorTrigger(selectors.getLoadingRefData);
    expect(data).toBeFalsy();
    expect(emptyData).toEqual(undefined);
  });

  it('should return bankruptcy document files', () => {
    const { data, emptyData } = selectorTriggerCollectionForm(
      selectors.selectBankruptcyDocumentFiles
    );
    expect(data).toEqual([]);
    expect(emptyData).toEqual(undefined);
  });

  it('should return bankruptcy document loading', () => {
    const { data, emptyData } = selectorTriggerCollectionForm(
      selectors.selectBankruptcyDocumentLoading
    );
    expect(data).toEqual(false);
    expect(emptyData).toEqual(undefined);
  });

  it('should return bankruptcy document files', () => {
    const { data, emptyData } = selectorTriggerCollectionForm(
      selectors.selectBankruptcyDocumentError
    );
    expect(data).toEqual(false);
    expect(emptyData).toEqual(undefined);
  });
});
