import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import {
  removeBankruptcyStatus,
  triggerRemoveBankruptcyStatus
} from './removeBankruptcy';
import bankruptcyService from '../bankruptcyService';
import {
  mockActionCreator,
  responseDefault,
  storeId,
  byPassRejected,
  byPassFulfilled
} from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { FAILURE_MSG } from 'app/constants';


const requestActionData = {
  common: { accountId: storeId },
  operatorCode: '',
  actionEntryCode: '',
  cardholderContactedName:'',
  storeId
};

describe('remove bankruptcy thunk', () => {
  let spy: jest.SpyInstance;
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {
      mapping: {
        loading: false,
        data: {
          collectionForm: {}
        }
      },
      bankruptcy: { [storeId]: {} }
    });
  });
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('should is fulfilled', async () => {
    spy = jest
      .spyOn(bankruptcyService, 'removeBankruptcyStatus')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const data = await removeBankruptcyStatus(requestActionData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(data.type).toEqual('bankruptcy/removeBankruptcyStatus/fulfilled');

    const toastSpy = mockActionCreator(actionsToast);
    const mockAction = toastSpy('addToast');

    await triggerRemoveBankruptcyStatus(requestActionData)(
      byPassFulfilled(removeBankruptcyStatus.fulfilled.type),
      store.getState,
      {}
    );

    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.TOAST_REMOVE_BANKRUPTCY_SUCCESS
    });
  });

  it('should is rejected', async () => {
    spy = jest
      .spyOn(bankruptcyService, 'removeBankruptcyStatus')
      .mockResolvedValue({ ...responseDefault, data: {status: FAILURE_MSG} });

    const data = await removeBankruptcyStatus(requestActionData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(data.type).toEqual('bankruptcy/removeBankruptcyStatus/rejected');
    const toastSpy = mockActionCreator(actionsToast);
    const mockAction = toastSpy('addToast');
    await triggerRemoveBankruptcyStatus({storeId})(
      byPassRejected(removeBankruptcyStatus.rejected.type),
      store.getState,
      {}
    );

    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_COLLECTION_FORM.TOAST_REMOVE_BANKRUPTCY_FAILURE
    });
  });
});
