import { BankruptcyState } from '../types';
import { reducer, bankruptcyActions } from './reducers';
import { storeId } from 'app/test-utils';

const { removeStore } = bankruptcyActions;

const initialState: BankruptcyState = {
  [storeId]: {}
};

describe('bankruptcy reducer', () => {
  it('should run removeStore action', () => {
    const state = reducer(initialState, removeStore({ storeId }));
    expect(state[storeId]).toEqual(undefined);
  });
});
