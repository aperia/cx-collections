import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import bankruptcyService from '../bankruptcyService';
import { BankruptcyState } from '../types';
export const getAssets = createAsyncThunk<
  RefDataValue[],
  StoreIdPayload,
  ThunkAPIConfig
>('refData/getAssets', async (args, { getState }) => {
  const { data } = await bankruptcyService.getAssets();
  return data;
});
export const getAssetsBuilder = (
  builder: ActionReducerMapBuilder<BankruptcyState>
) => {
  builder.addCase(getAssets.fulfilled, (draftState, action) => {
    const { storeId } = action.meta.arg;
    draftState[storeId] = {
      ...draftState[storeId],
      assetsRefData: action.payload
    };
  });
};
