import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import {
  storeId,
  responseDefault
} from 'app/test-utils';

import { getAssets } from './getAssets';
import bankruptcyService from '../bankruptcyService';

const store = createStore(rootReducer);

describe('test getAssets', () => {
  it('return data correctly', async () => {
    jest
      .spyOn(bankruptcyService, 'getAssets')
      .mockResolvedValue({ ...responseDefault, data: { value: '123' } });
    const response = await getAssets({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.payload).toEqual({ value: '123' });
  });
});

describe('test getAssets', () => {
  it('fulfilled', async () => {
    const payload = [{ value: '123', description: '123' }];
    const fulfilled = getAssets.fulfilled(payload, '', { storeId });
    const { bankruptcy } = rootReducer(store.getState(), fulfilled);
    expect(bankruptcy[storeId].assetsRefData).toEqual(payload);
  });
});
