import { createSelector } from '@reduxjs/toolkit';

// Bankruptcy info
export const getBankruptcyInfo = (state: RootState, storeId: string) => {
  return state.bankruptcy[storeId]?.info;
};

export const takeBankruptcyInfo = createSelector(
  [getBankruptcyInfo],
  data => data
);

// Bankruptcy result - view only
export const getBankruptcyResult = (state: RootState, storeId: string) => {
  return state.bankruptcy[storeId]?.viewData;
};

export const takeBankruptcyResult = createSelector(
  [getBankruptcyResult],
  data => data
);

// get bankruptcy loading status
export const getBankruptcyLoadingStatus = (
  state: RootState,
  storeId: string
) => {
  return state.bankruptcy[storeId]?.isLoading;
};

export const takeBankruptcyLoadingStatus = createSelector(
  [getBankruptcyLoadingStatus],
  data => data
);

export const selectBankruptcyMainInfo = createSelector(
  [getBankruptcyResult],
  data => data?.mainInfo || {}
);

// Filling Info

export const takeBankruptcyFillingInfo = createSelector(
  [getBankruptcyResult],
  data => data?.fillingInfo
);

// Attorney Info
export const takeBankruptcyAttorneyInfo = createSelector(
  [getBankruptcyResult],
  data => data?.additionalAttorneyInfo || {}
);

// Trustee Info

export const takeBankruptcyTrusteeInfo = createSelector(
  [getBankruptcyResult],
  data => data?.trusteeInfo || {}
);

// Clerk Info

export const takeBankruptcyClerkCourtInfo = createSelector(
  [getBankruptcyResult],
  data => data?.clerkCourtInfo || {}
);

export const selectBankruptcyData = createSelector(
  (states: RootState, storeId: string) => {
    const data = states.bankruptcy[storeId]?.data as any;
    return data ? data : null;
  },
  (data: object) => data
);

export const getCollectionFormData = (state: RootState, storeId: string) =>
  state.collectionForm[storeId]?.lastFollowUpData;

export const getBankruptcyDocument = (state: RootState, storeId: string) =>
  state.collectionForm[storeId]?.caseDocumentFiles?.bankruptcyDocument;

export const takeCollectionFormData = createSelector(
  [getCollectionFormData],
  data => data
);

export const selectBankruptcyDocumentFiles = createSelector(
  getBankruptcyDocument,
  data => data?.files
);

export const selectBankruptcyDocumentLoading = createSelector(
  getBankruptcyDocument,
  data => data?.loading
);

export const selectBankruptcyDocumentError = createSelector(
  getBankruptcyDocument,
  data => data?.error
);

// Loading update
export const getBankruptcyUpdateStatus = (state: RootState, storeId: string) =>
  state.bankruptcy[storeId]?.isUpdating;

const getCreditBureauCode = (state: RootState, storeId: string) =>
  state.bankruptcy[storeId]?.creditBureauCodeRefData;

const getAssets = (state: RootState, storeId: string) =>
  state.bankruptcy[storeId]?.assetsRefData;

const getProSE = (state: RootState, storeId: string) =>
  state.bankruptcy[storeId]?.proSERefData;

export const assetsRefData = createSelector(getAssets, data => data);

export const proSERefData = createSelector(getProSE, data => data);

export const takeCreditBureauCode = createSelector(
  getCreditBureauCode,
  data => data
);

export const getLoadingRefData = (state: RootState, storeId: string) =>
  state.bankruptcy[storeId]?.isLoadingRefData;
