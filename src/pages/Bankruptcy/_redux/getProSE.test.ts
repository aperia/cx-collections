import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import {
  storeId,
  responseDefault
} from 'app/test-utils';

import { getProSE } from './getProSE';
import bankruptcyService from '../bankruptcyService';

const store = createStore(rootReducer);

describe('test getProSE', () => {
  it('return data correctly', async () => {
    jest
      .spyOn(bankruptcyService, 'getProSE')
      .mockResolvedValue({ ...responseDefault, data: { value: '123' } });
    const response = await getProSE({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.payload).toEqual({ value: '123' });
  });
});

describe('test getProSE', () => {
  it('fulfilled', async () => {
    const payload = [{ value: '123', description: '123' }];
    const fulfilled = getProSE.fulfilled(payload, '', { storeId });
    const { bankruptcy } = rootReducer(store.getState(), fulfilled);
    expect(bankruptcy[storeId].proSERefData).toEqual(payload);
  });
});
