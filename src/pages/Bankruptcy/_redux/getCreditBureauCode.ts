import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import bankruptcyService from '../bankruptcyService';
import { BankruptcyState } from '../types';

export const getBureauCodeRefData = createAsyncThunk<
  RefDataValue[],
  StoreIdPayload,
  ThunkAPIConfig
>('account/getExternalStatusRefData', async (args, { getState }) => {
  const { data } = await bankruptcyService.getCreditBureauCode();
  return data;
});

export const getBureauCodeRefDataBuilder = (
  builder: ActionReducerMapBuilder<BankruptcyState>
) => {

  builder.addCase(getBureauCodeRefData.pending, (draftState, action) => {
    const { storeId } = action.meta.arg;
    draftState[storeId] = {
      ...draftState[storeId],
      isLoadingRefData: true
    };
  }).addCase(getBureauCodeRefData.fulfilled, (draftState, action) => {
    const { storeId } = action.meta.arg;

    draftState[storeId] = {
      ...draftState[storeId],
      creditBureauCodeRefData: action.payload,
      isLoadingRefData: false
    };
  }).addCase(getBureauCodeRefData.rejected, (draftState, action) => {
    const { storeId } = action.meta.arg;
    draftState[storeId] = {
      ...draftState[storeId],
      isLoadingRefData: false
    };
  });
};
