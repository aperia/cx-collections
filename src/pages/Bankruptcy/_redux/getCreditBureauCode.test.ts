import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import {
  storeId,
  responseDefault
} from 'app/test-utils';

import { getBureauCodeRefData } from './getCreditBureauCode';
import bankruptcyService from '../bankruptcyService';

const store = createStore(rootReducer);

describe('test getBureauCodeRefData', () => {
  it('return data correctly', async () => {
    jest
      .spyOn(bankruptcyService, 'getCreditBureauCode')
      .mockResolvedValue({ ...responseDefault, data: { value: '123' } });
    const response = await getBureauCodeRefData({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.payload).toEqual({ value: '123' });
  });
});

describe('test getBureauCodeRefDataBuilder', () => {
  it('fulfilled', async () => {
    const payload = [{ value: '123', description: '123' }];
    const fulfilled = getBureauCodeRefData.fulfilled(payload, '', { storeId });
    const { bankruptcy } = rootReducer(store.getState(), fulfilled);
    expect(bankruptcy[storeId].creditBureauCodeRefData).toEqual(payload);
  });

  it('rejected', async () => {
    const error = { name: 'rejected', message: 'rejected' };
    const rejected = getBureauCodeRefData.rejected(error, '', { storeId });
    const { bankruptcy } = rootReducer(store.getState(), rejected);
    expect(bankruptcy[storeId].creditBureauCodeRefData).toEqual({ value: '123' });
  });
});
