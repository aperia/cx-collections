import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { getBankruptcyInformation } from './getBankruptcyInformation';
import { GetBankruptcyPayload } from '../types';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { responseDefault, storeId, mockActionCreator } from 'app/test-utils';
import * as helperCommon from 'app/helpers/commons';
import bankruptcyService from '../bankruptcyService';
import * as helpers from '../helpers';
import { apiService } from 'app/utils/api.service';

const mockPayload: GetBankruptcyPayload = {
  info: {},
  data: {
    mainInfo: {},
    clerkCourtInfo: {}
  }
};
import * as collectionFormHelper from 'pages/CollectionForm/helpers';

let spy: jest.SpyInstance;
let store: Store<RootState> = createStore(rootReducer, {
  collectionForm: {
    [storeId]: {
      isInProgress: true
    }
  },
  accountDetail: {
    [storeId]: {}
  }
});

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('it should update get Bankruptcy Information builder work correctly', () => {
  const state = store.getState();
  it('check builder > pending', async () => {
    const pending = getBankruptcyInformation.pending(storeId, {
      storeId
    });
    const actual = rootReducer(state, pending);
    expect(actual.bankruptcy[storeId].isLoading).toEqual(true);
  });

  it('check builder > fulfilled', async () => {
    const fulfilled = getBankruptcyInformation.fulfilled(mockPayload, '', {
      storeId
    });
    const actual = rootReducer(state, fulfilled);
    expect(actual.bankruptcy[storeId].isLoading).toEqual(false);
    expect(actual.bankruptcy[storeId].data).toEqual(mockPayload.data);
  });

  it('check builder > rejected', async () => {
    const rejected = getBankruptcyInformation.rejected(null, '', { storeId });
    const actual = rootReducer(state, rejected);
    expect(actual.bankruptcy[storeId].isLoading).toEqual(false);
  });
});

describe('it should  get Bankruptcy Information work correctly', () => {
  it('get default value correctly inProgressInfo', async () => {
    store = createStore(rootReducer, {
      collectionForm: {
        [storeId]: {
          isInProgress: true
        }
      }
    });

    jest
      .spyOn(helperCommon, 'getFeatureConfig')
      .mockResolvedValue({ selectFields: [] });
    await getBankruptcyInformation({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );
    // Just for coverage case get inProgressInfo Default value
    expect(store.getState().collectionForm[storeId].inProgressInfo).toEqual(
      undefined
    );
  });

  it('get default value correctly collectionForm default', async () => {
    store = createStore(rootReducer, {});
    jest
      .spyOn(helperCommon, 'getFeatureConfig')
      .mockResolvedValue({ selectFields: [] });
    jest
      .spyOn(bankruptcyService, 'getBankruptcyInformation')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });
    jest
      .spyOn(collectionFormActions, 'toggleCallResultInProgressStatus')
      .mockImplementation(
        jest.fn().mockImplementation(() => ({ type: 'type' }))
      );
    jest
      .spyOn(helpers, 'buildAmountDataField')
      .mockImplementation(
        jest.fn().mockImplementation(() => ({ type: 'type' }))
      );
    await getBankruptcyInformation({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );

    // Just for coverage case get collectionForm Default value
    expect(store.getState().collectionForm[storeId]).toEqual(undefined);
  });

  it('it return data correctly', async () => {
    jest
      .spyOn(helperCommon, 'getFeatureConfig')
      .mockResolvedValue({ selectFields: [] });
    jest
      .spyOn(bankruptcyService, 'getBankruptcyInformation')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          bankruptcyInformationList: [
            { bankruptcyAssetIndicator: '123', planDifferenceAmount: '0' }
          ]
        }
      });
    jest
      .spyOn(helpers, 'buildAmountDataField')
      .mockImplementation(
        jest.fn().mockImplementation(() => ({ type: 'type' }))
      );
    const data = await getBankruptcyInformation({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );
    expect(data.type).toEqual('bankruptcy/getBankruptcyInformation/fulfilled');
  });

  it('it should call toggleCallResultInProgressStatus correctly', async () => {
    jest.spyOn(apiService as any, 'get').mockResolvedValue({
      data: {},
      config: { url: 'refData/creditBureauCode.json' }
    });

    store = createStore(rootReducer, {
      collectionForm: {
        [storeId]: {
          inProgressInfo: { test: '1234' },
          isInProgress: true
        }
      }
    });
    jest
      .spyOn(helpers, 'buildAmountDataField')
      .mockImplementation(
        jest.fn().mockImplementation(() => ({ type: 'type', test: '1234' }))
      );
    jest
      .spyOn(helperCommon, 'getFeatureConfig')
      .mockResolvedValue({ selectFields: [] });
    jest
      .spyOn(bankruptcyService, 'getBankruptcyInformation')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          bankruptcyInformationList: [{ test: '1234' }],
          processStatus: 'COMPLETE'
        }
      });

    const collectionFormActionSpy = mockActionCreator(collectionFormActions);
    const toggleCallResultInProgressStatusSpy = collectionFormActionSpy(
      'toggleCallResultInProgressStatus'
    );

    const collectionFormHelperSpy = jest.spyOn(
      collectionFormHelper,
      'dispatchDestroyDelayProgress'
    );
    await getBankruptcyInformation({ storeId, mapValueWithDescription: true })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(toggleCallResultInProgressStatusSpy).toBeCalledWith({
      storeId,
      info: { test: '1234' }
    });
    expect(collectionFormHelperSpy).toBeCalled();
  });
});
