import { createAsyncThunk, isFulfilled, isRejected } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// Types
import { RemoveBankruptcyDetailArg, RemoveBankruptcyStatusArg } from '../types';

// Service
import bankruptcyService from '../bankruptcyService';

// Const
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { BANKRUPTCY_TYPE } from '../constants';
import { FAILURE_MSG } from 'app/constants';

// Redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { bankruptcyActions } from './reducers';

// Helper
import {
  dispatchDelayProgress,
  setDelayProgress
} from 'pages/CollectionForm/helpers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export const removeBankruptcyStatus = createAsyncThunk<
  unknown,
  RemoveBankruptcyStatusArg,
  ThunkAPIConfig
>('bankruptcy/removeBankruptcyStatus', async (args, thunkAPI) => {
  const requestData = args;
  try {
    const response = await bankruptcyService.removeBankruptcyStatus(
      requestData
    );

    if (response?.data?.status === FAILURE_MSG) {
      throw response;
    }

    return response.data;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerRemoveBankruptcyStatus = createAsyncThunk<
  unknown,
  RemoveBankruptcyDetailArg,
  ThunkAPIConfig
>('bankruptcy/triggerRemoveBankruptcyStatus', async (args, thunkAPI) => {
  const { storeId } = args;
  const { dispatch, getState } = thunkAPI;
  const { accountDetail } = getState();
  const { commonConfig } = window.appConfig || {};
  const { operatorID = '', app, org } = commonConfig || {};

  const { customerNameForCreateWorkflow: cardholderContactedName = '' } =
    accountDetail[storeId]?.selectedAuthenticationData || {};

  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;

  const response = await dispatch(
    removeBankruptcyStatus({
      common: {
        app,
        accountId: eValueAccountId!,
        org
      },
      operatorCode: operatorID,
      actionEntryCode: BANKRUPTCY_TYPE.BANKRUPTCY_REMOVE,
      cardholderContactedName
    })
  );

  if (isFulfilled(response)) {
    batch(() => {
      dispatchDelayProgress(
        bankruptcyActions.getBankruptcyInformation({
          storeId
        }),
        storeId
      );

      setDelayProgress(storeId, BANKRUPTCY_TYPE.BANKRUPTCY_REMOVE, {});

      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_COLLECTION_FORM.TOAST_REMOVE_BANKRUPTCY_SUCCESS
        })
      );

      dispatch(
        collectionFormActions.toggleCallResultInProgressStatus({
          storeId,
          info: {},
          lastDelayCallResult: BANKRUPTCY_TYPE.BANKRUPTCY_REMOVE
        })
      );
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inCollectionFormFlyOut'
        })
      );
    });
  }

  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_COLLECTION_FORM.TOAST_REMOVE_BANKRUPTCY_FAILURE
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inCollectionFormFlyOut',
          apiResponse: response.payload?.response
        })
      );
    });
  }
});
