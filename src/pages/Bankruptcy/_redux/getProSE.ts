import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import bankruptcyService from '../bankruptcyService';
import { BankruptcyState } from '../types';
export const getProSE = createAsyncThunk<
  RefDataValue[],
  StoreIdPayload,
  ThunkAPIConfig
>('refData/getProSE', async (args, { getState }) => {
  const { data } = await bankruptcyService.getProSE();
  return data;
});
export const getProSEBuilder = (
  builder: ActionReducerMapBuilder<BankruptcyState>
) => {
  builder.addCase(getProSE.fulfilled, (draftState, action) => {
    const { storeId } = action.meta.arg;
    draftState[storeId] = {
      ...draftState[storeId],
      proSERefData: action.payload
    };
  });
};
