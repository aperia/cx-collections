import {
  getBureauCodeRefData,
  getBureauCodeRefDataBuilder
} from './getCreditBureauCode';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Type
import { BankruptcyState } from '../types';

// Actions
import {
  triggerUpdateBankruptcy,
  updateCollectionFormBuilder
} from './updateBankruptcy';
import { triggerRemoveBankruptcyStatus } from './removeBankruptcy';
import {
  getBankruptcyInformation,
  getBankruptcyInformationBuilder
} from './getBankruptcyInformation';
import { getAssets, getAssetsBuilder } from './getAssets';
import { getProSE, getProSEBuilder } from './getProSE';

const { actions, reducer } = createSlice({
  name: 'bankruptcy',
  initialState: {} as BankruptcyState,
  reducers: {
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    }
  },
  extraReducers: builder => {
    updateCollectionFormBuilder(builder);
    getBankruptcyInformationBuilder(builder);
    getBureauCodeRefDataBuilder(builder);
    getAssetsBuilder(builder);
    getProSEBuilder(builder);
  }
});

const allActions = {
  ...actions,
  triggerUpdateBankruptcy,
  triggerRemoveBankruptcyStatus,
  getBankruptcyInformation,
  getBureauCodeRefData,
  getAssets,
  getProSE
};

export { allActions as bankruptcyActions, reducer };
