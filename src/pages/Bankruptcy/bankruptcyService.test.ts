import bankruptcyService from './bankruptcyService';

// utils
import { mockApiServices, storeId } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import {
  GetBankruptcyRequestData,
  UpdateBankruptcyRequestData,
  RemoveBankruptcyStatusArg
} from './types';

describe('bankruptcyService', () => {
  describe('getBankruptcyInformation', () => {
    const params: GetBankruptcyRequestData = {
      selectFields: ['field_1', 'field_2']
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      bankruptcyService.getBankruptcyInformation(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      bankruptcyService.getBankruptcyInformation(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.bankruptcyInformation,
        params
      );
    });
  });

  describe('updateDetailForCallResultType', () => {
    const params: UpdateBankruptcyRequestData = {
      common: { accountId: storeId }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      bankruptcyService.updateDetailForCallResultType(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      bankruptcyService.updateDetailForCallResultType(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.bankruptcyUpdateWorkflow,
        params
      );
    });
  });

  describe('removeBankruptcyStatus', () => {
    const params: RemoveBankruptcyStatusArg = {
      operatorCode: 'ED',
      actionEntryCode: 'DW',
      cardholderContactedName: 'Smith'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      bankruptcyService.removeBankruptcyStatus(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      bankruptcyService.removeBankruptcyStatus(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.bankruptcyInformationDelete,
        params
      );
    });
  });

  describe('getCreditBureauCode', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      bankruptcyService.getCreditBureauCode();

      expect(mockService).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      bankruptcyService.getCreditBureauCode();

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.getCreditBureauCodeRefData
      );
    });
  });
});
