import {
  getRawViewValue,
  buildAmountDataField,
  parseBankruptcyViewPending,
  parseBankruptcyAttorney,
  parseBankruptcyTrustee,
  parseBankruptcyClerk,
  mappingDescription
} from './helpers';
import { createStore } from 'redux';
import dateTime from 'date-and-time';
import { FormatTime } from 'app/constants/enums';
import { rootReducer } from 'storeConfig';
import { storeId } from 'app/test-utils';

describe('mappingDescription', () => {
  it('return data correctly', () => {
    const info = {
      name: 'text',
      description: 'text',
      text: '123'
    };
    const refDataByField = { text: [{value: '123', description: '123'}], address: [{value: '123', description: '123'}] };

    const data = mappingDescription(info, refDataByField);
    expect(data).toEqual( { name: 'text', description: 'text', text: '123 - 123' })
    
  });
});

describe('parseBankruptcyViewPending', () => {
  it('return empty object', () => {
    const data = parseBankruptcyViewPending();
    expect(data).toEqual({});
  });

  it('return data correctly', () => {
    const formValue = {
      bankruptcyCaseIdentifier: '',
      bankruptcyChapterCode: '123',
      attorneyName: '123',
      attorneyPhoneNumber: '123',
      bankruptcyType: {}
    };
    const data = parseBankruptcyViewPending(formValue);
    expect(data).toEqual(expect.objectContaining(formValue));
  });
});

describe('parseBankruptcyAttorney', () => {
  it('return empty object', () => {
    const data = parseBankruptcyAttorney();
    expect(data).toEqual({});
  });

  it('return data correctly', () => {
    const formValue = {
      attorneyAddressLine1Text: 'text 1',
      attorneyAddressLine2Text: 'text 2',
      attorneyCityName: 'city name',
      attorneyStateCode: '123'
    };
    const data = parseBankruptcyAttorney(formValue);
    expect(data).toEqual(expect.objectContaining(formValue));
  });
});

describe('parseBankruptcyTrustee', () => {
  it('return empty object', () => {
    const data = parseBankruptcyTrustee();
    expect(data).toEqual({});
  });

  it('return data correctly', () => {
    const formValue = {
      trusteeName: 'text',
      trusteePhoneNumber: '123456789',
      trusteeAddressLine1Text: 'text 1',
      trusteeAddressLine2Text: 'text 2'
    };
    const data = parseBankruptcyTrustee(formValue);
    expect(data).toEqual(expect.objectContaining(formValue));
  });
});

describe('parseBankruptcyClerk', () => {
  it('return empty object', () => {
    const data = parseBankruptcyClerk();
    expect(data).toEqual({});
  });

  it('return data correctly', () => {
    const formValue = {
      clerkCourtName: 'text',
      clerkCourtPhoneNumber: '123456789',
      clerkCourtAddressLine1Text: 'text 1',
      clerkCourtAddressLine2Text: 'text 2'
    };
    const data = parseBankruptcyClerk(formValue);
    expect(data).toEqual(expect.objectContaining(formValue));
  });
});

describe('test getRawViewValue', () => {
  it('remove undefined form value', () => {
    const formValue = { test: undefined, name: 'aa' };
    expect(getRawViewValue(formValue)).toEqual({ name: 'aa' });
  });

  it('format date valid', () => {
    const formValue = { test: undefined, Date: '2021-12-20' };
    expect(getRawViewValue(formValue)).toEqual({
      Date: dateTime.format(
        new Date(formValue.Date),
        FormatTime.DefaultPostDate
      )
    });
  });

  it('format if data is object', () => {
    const formValue = { name: { value: '123' } };
    expect(getRawViewValue(formValue)).toEqual({
      name: '123'
    });
  });
});

describe('test buildAmountDataField', () => {
  it('it should be work correctly', () => {
    const store = createStore(rootReducer, {
      accountDetail: {
        [storeId]: {
          data: {
            currentBalanceAmount: '10'
          }
        }
      }
    });
    const formValue = {
      test: undefined,
      name: 'aa',
      Amount: 10000,
      bankruptcyCurrentBalanceAmount: ''
    };
    const result = buildAmountDataField(formValue, storeId)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(result).toEqual({
      name: 'aa',
      test: undefined,
      Amount: '10000.00',
      bankruptcyCurrentBalanceAmount: '10.00'
    });
  });

  it('it should be work correctly with no currentBalanceAmount value from accountDetail', () => {
    const store = createStore(rootReducer, {
      accountDetail: {
        [storeId]: {}
      }
    });
    const formValue = {
      test: undefined,
      name: 'aa',
      Amount: 10000,
      bankruptcyCurrentBalanceAmount: '100'
    };
    const result = buildAmountDataField(formValue, storeId)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(result).toEqual({
      name: 'aa',
      test: undefined,
      Amount: '10000.00',
      bankruptcyCurrentBalanceAmount: '100.00'
    });
  });

  it('it should be work correctly with no amount value', () => {
    const store = createStore(rootReducer, {
      accountDetail: {
        [storeId]: {
          data: {
            currentBalanceAmount: '10'
          }
        }
      }
    });
    const formValue = {
      test: undefined,
      name: 'aa',
      Amount: '',
      bankruptcyCurrentBalanceAmount: ''
    };
    const result = buildAmountDataField(formValue, storeId)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(result).toEqual({
      name: 'aa',
      test: undefined,
      Amount: '',
      bankruptcyCurrentBalanceAmount: '10.00'
    });
  });
});
