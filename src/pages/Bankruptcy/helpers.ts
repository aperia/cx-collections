import dateTime from 'date-and-time';
import isObject from 'lodash.isobject';
import isNil from 'lodash.isnil';
import isEmpty from 'lodash.isempty';

// Const
import { INVALID_DATE } from 'app/constants';

// Helper
import { isValidDate } from 'app/helpers';
import { FormatTime } from 'app/constants/enums';

export const parseBankruptcyViewPending = (formValue?: MagicKeyValue) => {
  const {
    bankruptcyCaseIdentifier,
    bankruptcyChapterCode,
    attorneyName,
    attorneyPhoneNumber,
    bankruptcyType,
    bankruptcyAssetIndicator,
    bankruptcyJurisdiction,
    bankruptcyFileDate,
    bankruptcyCurrentBalanceAmount,
    bankruptcyProSeIndicator,
    bankruptcyDismissalDate,
    bankruptcyDischargeDate,
    bankruptcyCloseDate,
    bankruptcyReopenDate,
    bankruptcyConversationDate,
    bankruptcyReaffirmedDate,
    bankruptcyConfirmationDate,
    proofOfClaimBarcodeDate,
    voluntarySurrenderIndicator,
    accountNonfilerIndicator,
    accountNonfilerCollectibleIndicator,
    accountSettledAmount,
    accountRedeemedAmount,
    originalPlanAmount,
    planDifferenceAmount,
    planPaymentAmount,
    primaryConsumerInformationIndicatorCode,
    primaryCreditBureauReportCode,
    secondaryConsumerInformationIndicatorCode,
    secondaryCreditBureauReportCode,
    filerName,
    jointFilerName
  } = formValue || {};

  return {
    bankruptcyCaseIdentifier,
    bankruptcyChapterCode,
    attorneyName,
    attorneyPhoneNumber,
    bankruptcyType,
    bankruptcyAssetIndicator,
    bankruptcyJurisdiction,
    bankruptcyFileDate,
    bankruptcyCurrentBalanceAmount,
    bankruptcyProSeIndicator,
    bankruptcyDismissalDate,
    bankruptcyDischargeDate,
    bankruptcyCloseDate,
    bankruptcyReopenDate,
    bankruptcyConversationDate,
    bankruptcyReaffirmedDate,
    bankruptcyConfirmationDate,
    proofOfClaimBarcodeDate,
    voluntarySurrenderIndicator,
    accountNonfilerIndicator,
    accountNonfilerCollectibleIndicator,
    accountSettledAmount,
    accountRedeemedAmount,
    originalPlanAmount,
    planDifferenceAmount,
    planPaymentAmount,
    primaryConsumerInformationIndicatorCode,
    primaryCreditBureauReportCode,
    secondaryConsumerInformationIndicatorCode,
    secondaryCreditBureauReportCode,
    filerName,
    jointFilerName
  };
};

export const parseBankruptcyAttorney = (formValue?: MagicKeyValue) => {
  const {
    attorneyAddressLine1Text,
    attorneyAddressLine2Text,
    attorneyCityName,
    attorneyStateCode,
    attorneyPostalCode,
    attorneyFaxNumber,
    attorneyFirm,
    attorneyRetained,
    attorneyEmail
  } = formValue || {};

  return {
    attorneyAddressLine1Text,
    attorneyAddressLine2Text,
    attorneyCityName,
    attorneyStateCode,
    attorneyPostalCode,
    attorneyFaxNumber,
    attorneyFirm,
    attorneyRetained,
    attorneyEmail
  };
};

export const parseBankruptcyTrustee = (formValue?: MagicKeyValue) => {
  const {
    trusteeName,
    trusteePhoneNumber,
    trusteeAddressLine1Text,
    trusteeAddressLine2Text,
    trusteeCityName,
    trusteeStateCode,
    trusteePostalCode,
    trusteeFaxNumber,
    trusteeEmail
  } = formValue || {};

  return {
    trusteeName,
    trusteePhoneNumber,
    trusteeAddressLine1Text,
    trusteeAddressLine2Text,
    trusteeCityName,
    trusteeStateCode,
    trusteePostalCode,
    trusteeFaxNumber,
    trusteeEmail
  };
};

export const parseBankruptcyClerk = (formValue?: MagicKeyValue) => {
  const {
    clerkCourtName,
    clerkCourtPhoneNumber,
    clerkCourtAddressLine1Text,
    clerkCourtAddressLine2Text,
    clerkCourtCityName,
    clerkCourtStateCode,
    clerkCourtPostalCode,
    clerkCourtEmail
  } = formValue || {};

  return {
    clerkCourtName,
    clerkCourtPhoneNumber,
    clerkCourtAddressLine1Text,
    clerkCourtAddressLine2Text,
    clerkCourtCityName,
    clerkCourtStateCode,
    clerkCourtPostalCode,
    clerkCourtEmail
  };
};

export const getRawViewValue = (formValues: MagicKeyValue) => {
  return Object.keys(formValues)?.reduce(
    (reduceInfo: MagicKeyValue, kyInfo: string) => {
      if (isNil(formValues[kyInfo])) return reduceInfo;

      if (
        isValidDate(formValues[kyInfo]) &&
        kyInfo.includes('Date') &&
        !INVALID_DATE.includes(formValues[kyInfo])
      ) {
        return {
          ...reduceInfo,
          [kyInfo]: dateTime.format(
            new Date(formValues[kyInfo]),
            FormatTime.DefaultPostDate
          )
        };
      }

      if (isObject(formValues[kyInfo])) {
        return {
          ...reduceInfo,
          [kyInfo]: formValues[kyInfo]?.value
        };
      }

      reduceInfo = { ...reduceInfo, [kyInfo]: formValues[kyInfo] };

      return reduceInfo;
    },
    {}
  );
};

export const buildAmountDataField: AppThunk<MagicKeyValue> =
  (data: MagicKeyValue, storeId: string) => (_, getState) => {
    const { accountDetail } = getState();
    const { currentBalanceAmount } = accountDetail[storeId]?.data || {};

    return Object.keys(data).reduce((nextData, ky) => {
      if (ky === 'bankruptcyCurrentBalanceAmount') {
        const value = isEmpty(data[ky]) ? currentBalanceAmount : data[ky];
        return { ...nextData, [ky]: parseFloat(value).toFixed(2) };
      }

      if (ky.includes('Amount')) {
        return {
          ...nextData,
          [ky]: data[ky] ? parseFloat(data[ky]).toFixed(2) : ''
        };
      }

      nextData = { ...nextData, [ky]: data[ky] };

      return nextData;
    }, {});
  };

export const mappingDescription = (
  info: MagicKeyValue,
  refDataByField: Record<string, RefDataValue[]>
) => {
  const refDataByFieldKeys = Object.keys(refDataByField);
  return refDataByFieldKeys.reduce(
    (pre, currField) => {
      if (info?.[currField]) {
        const codeValue = info[currField];
        const selectedRefData = refDataByField[currField].find(
          item => item?.value?.trim() === codeValue?.trim()
        );
        const newValue = `${selectedRefData?.value} - ${selectedRefData?.description}`;
        pre[currField] = newValue;
      }
      return pre;
    },
    { ...info }
  );
};
