import React, { useCallback, useEffect, useMemo, useRef } from 'react';

// Redux
import { useDispatch } from 'react-redux';
import {
  selectCollectionFormMethod,
  selectMemoSectionCollapsed
} from 'pages/CollectionForm/_redux/selectors';
import { bankruptcyActions } from '../_redux/reducers';
import {
  getLoadingRefData,
  takeBankruptcyInfo,
  takeCreditBureauCode
} from '../_redux/selectors';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import {
  takeHasOnlyPrimaryCardholder,
  useFilerNameDropdownData
} from 'pages/AccountDetails/_redux/selectors';

// Hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useSubmit } from 'pages/CollectionForm/hooks/useSubmit';
import { useSaveViewName } from 'pages/CollectionForm/hooks/useSaveViewName';

// Constants
import {
  BANKRUPTCY_INFO_TYPE,
  BANKRUPTCY_TYPE,
  BANKRUPTCY_VIEW,
  BANKRUPTCY_TYPE_SINGLE,
  FILER_NAME,
  SECONDARY_CREDIT_BUREAU_REPORT_CODE,
  SECONDARY_CUSTOMER_INFO_INDICATOR_CODE,
  BANKRUPTCY_TYPE_JOINT,
  JOINT_FILER_NAME,
  PRIMARY_CREDIT_BUREAU_REPORT_CODE,
  PRIMARY_CONSUMER_INFORMATION_INDICATOR_CODE,
  ORIGINAL_PLAN_AMOUNT,
  PLAN_DIFFERENCE_AMOUNT,
  PLAN_PAYMENT_AMOUNT,
  CHAPTER_13
} from '../constants';
import {
  CALL_RESULT_CODE,
  COLLECTION_METHOD
} from 'pages/CollectionForm/constants';

// types
import { ExtraFieldProps } from 'app/_libraries/_dof/core';
import { Field } from 'redux-form';

// Context
import BankruptcyContext from '../BankruptcyContext/context';

// Components
import { View } from 'app/_libraries/_dof/core';
import BankruptcyAttorney from './BankruptcyAttorney';
import BankruptcyTrustee from './BankruptcyTrustee';
import BankruptcyClerk from './BankruptcyClerk';

// Helpers
import classNames from 'classnames';
import { parseBankruptcyViewPending } from '../helpers';
import isEqual from 'lodash.isequal';
import isObject from 'lodash.isobject';
import isEmpty from 'lodash.isempty';
import { useIsDirtyForms } from '../hooks';

export interface BankruptcyFormProps {}

const BankruptcyForm: React.FC<BankruptcyFormProps> = () => {
  const { storeId, accEValue } = useAccountDetail();
  const dispatch = useDispatch();
  const viewRef = useRef<any>();

  // Views name
  const mainViewName = `${BANKRUPTCY_VIEW.BANKRUPTCY}-${storeId}`;
  const attorneyViewName = `${BANKRUPTCY_VIEW.PENDING_ATTORNEY}-${storeId}`;
  const trusteeViewName = `${BANKRUPTCY_VIEW.PENDING_TRUSTEE}-${storeId}`;
  const clerkViewName = `${BANKRUPTCY_VIEW.PENDING_CLERK}-${storeId}`;
  const viewNames = useMemo(
    () => [mainViewName, attorneyViewName, trusteeViewName, clerkViewName],
    [mainViewName, attorneyViewName, trusteeViewName, clerkViewName]
  );

  // Redux
  const bankruptcyData = useStoreIdSelector<MagicKeyValue>(takeBankruptcyInfo);
  const isMemoSectionCollapsed = useStoreIdSelector<MagicKeyValue>(
    selectMemoSectionCollapsed
  );

  const isLoadingRefData = useStoreIdSelector<boolean>(getLoadingRefData);

  const method = useStoreIdSelector<
    COLLECTION_METHOD.CREATE | COLLECTION_METHOD.EDIT
  >(selectCollectionFormMethod);
  const isEdit = method === COLLECTION_METHOD.EDIT;
  const hasOnlyPrimaryCardholder = useStoreIdSelector<boolean>(
    takeHasOnlyPrimaryCardholder
  );

  const filerNameDropdownData = useFilerNameDropdownData();

  const bureauReportCodeDropdownData =
    useStoreIdSelector<RefDataValue[]>(takeCreditBureauCode);

  const isDirty = useIsDirtyForms(
    viewNames,
    bankruptcyData,
    filerNameDropdownData
  );

  const handleSubmit = () => {
    dispatch(
      bankruptcyActions.triggerUpdateBankruptcy({
        storeId,
        postData: {
          method,
          type: BANKRUPTCY_TYPE.BANKRUPTCY,
          viewName: [
            clerkViewName,
            trusteeViewName,
            attorneyViewName,
            mainViewName
          ],
          accountId: accEValue
        }
      })
    );
  };

  useSubmit(handleSubmit);
  useSaveViewName(viewNames);

  useEffect(() => {
    dispatch(bankruptcyActions.getBureauCodeRefData({ storeId }));
  }, [dispatch, storeId]);

  useEffect(() => {
    if (isLoadingRefData === false) {
      dispatch(
        bankruptcyActions.getBankruptcyInformation({
          storeId
        })
      );
    }
  }, [dispatch, storeId, accEValue, isLoadingRefData]);

  // set dirty
  useEffect(() => {
    dispatch(
      collectionFormActions.setDirtyForm({
        storeId,
        callResult: CALL_RESULT_CODE.BANKRUPTCY,
        value: isDirty
      })
    );
    isEdit &&
      dispatch(
        collectionFormActions.changeDisabledOk({
          storeId,
          disabledOk: !isDirty
        })
      );
  }, [dispatch, isDirty, storeId, isEdit]);

  const checkBankruptcyType = useMemo(() => {
    if (isEmpty(bankruptcyData?.bankruptcyType)) {
      return hasOnlyPrimaryCardholder
        ? BANKRUPTCY_TYPE_SINGLE
        : BANKRUPTCY_TYPE_JOINT;
    }

    if (bankruptcyData?.bankruptcyType === BANKRUPTCY_TYPE_JOINT?.value) {
      return BANKRUPTCY_TYPE_JOINT;
    }

    return BANKRUPTCY_TYPE_SINGLE;
  }, [bankruptcyData?.bankruptcyType, hasOnlyPrimaryCardholder]);

  const formData = useMemo(() => {
    return parseBankruptcyViewPending({
      ...bankruptcyData,
      [BANKRUPTCY_INFO_TYPE]: checkBankruptcyType
    });
  }, [bankruptcyData, checkBankruptcyType]);

  // hide, show secondary fields if select single type
  // set dropdown data for filer name
  // set value for filer name, joint filer name
  // filter bureau report code dropdown data
  useEffect(() => {
    setImmediate(() => {
      const primaryCreditBureauReportCode: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(PRIMARY_CREDIT_BUREAU_REPORT_CODE);
      const secondaryCreditBureauReportCode: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(SECONDARY_CREDIT_BUREAU_REPORT_CODE);
      const secondaryConsumerInformationIndicatorCode: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(SECONDARY_CUSTOMER_INFO_INDICATOR_CODE);
      const jointFilerName: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(JOINT_FILER_NAME);
      const filerName: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(FILER_NAME);
      const originalPlanAmount: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(ORIGINAL_PLAN_AMOUNT);
      const planDifferenceAmount: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(PLAN_DIFFERENCE_AMOUNT);
      const planPaymentAmount: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(PLAN_PAYMENT_AMOUNT);

      const isTypeJoint = !isEqual(
        formData?.bankruptcyType,
        BANKRUPTCY_TYPE_SINGLE
      );

      // hide, show secondary fields
      secondaryCreditBureauReportCode?.props?.props?.setVisible(isTypeJoint);
      secondaryCreditBureauReportCode?.props?.props?.setRequired(isTypeJoint);
      secondaryConsumerInformationIndicatorCode?.props?.props?.setVisible(
        isTypeJoint
      );
      secondaryConsumerInformationIndicatorCode?.props?.props?.setRequired(
        isTypeJoint
      );

      // filter dropdown options
      if (!isTypeJoint) {
        primaryCreditBureauReportCode?.props?.props?.setData(
          bureauReportCodeDropdownData?.filter(
            item => item.value !== '2' && item.value !== '3'
          )
        );
        secondaryCreditBureauReportCode?.props?.props?.setData(
          bureauReportCodeDropdownData?.filter(
            item => item.value !== '2' && item.value !== '3'
          )
        );
      } else {
        primaryCreditBureauReportCode?.props?.props?.setData(
          bureauReportCodeDropdownData?.filter(
            item => item.value !== '1' && item.value !== '3'
          )
        );
        secondaryCreditBureauReportCode?.props?.props?.setData(
          bureauReportCodeDropdownData?.filter(
            item => item.value !== '1' && item.value !== '3'
          )
        );
      }

      const typeField: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(BANKRUPTCY_INFO_TYPE);

      typeField?.props?.props?.setReadOnly(hasOnlyPrimaryCardholder);
      typeField?.props?.props?.setOthers((other: MagicKeyValue) => ({
        ...other,
        className: classNames({
          'readonly-no-arrow': hasOnlyPrimaryCardholder
        })
      }));

      filerName?.props?.props?.setData(filerNameDropdownData);
      if (!formData?.filerName) {
        filerName?.props?.props?.setValue(filerNameDropdownData[0]);
      } else {
        filerName?.props?.props?.setValue(
          filerNameDropdownData?.filter(
            item => item.value === formData?.filerName
          )?.[0]
        );
      }

      filerName?.props?.props?.setReadOnly(
        !isTypeJoint && hasOnlyPrimaryCardholder
      );
      filerName?.props?.props?.setOthers((other: MagicKeyValue) => ({
        ...other,
        className: classNames({ 'readonly-no-arrow': hasOnlyPrimaryCardholder })
      }));

      jointFilerName?.props?.props?.setVisible(isTypeJoint);
      const filerNameValue = isObject(formData?.filerName as MagicKeyValue)
        ? formData.filerName?.value
        : formData?.filerName;
      jointFilerName?.props?.props?.setValue(
        filerNameDropdownData.filter(
          item => !isEqual(item?.value, filerNameValue)
        )?.[0]?.description
      );

      // when select chapter 13, show amount fields
      const isChapter13 = formData?.bankruptcyChapterCode === CHAPTER_13;
      originalPlanAmount?.props?.props?.setVisible(isChapter13);
      planDifferenceAmount?.props?.props?.setVisible(isChapter13);
      planPaymentAmount?.props?.props?.setVisible(isChapter13);
    });
  }, [
    formData,
    filerNameDropdownData,
    bureauReportCodeDropdownData,
    hasOnlyPrimaryCardholder
  ]);

  const handleOnChangeView = useCallback(
    (values: MagicKeyValue, _, __, previousValues: MagicKeyValue) => {
      if (isEqual(values, previousValues)) return;

      const primaryCreditBureauReportCode: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(PRIMARY_CREDIT_BUREAU_REPORT_CODE);
      const primaryConsumerInformationIndicatorCode: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(
          PRIMARY_CONSUMER_INFORMATION_INDICATOR_CODE
        );
      const secondaryCreditBureauReportCode: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(SECONDARY_CREDIT_BUREAU_REPORT_CODE);
      const secondaryConsumerInformationIndicatorCode: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(SECONDARY_CUSTOMER_INFO_INDICATOR_CODE);
      const jointFilerName: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(JOINT_FILER_NAME);
      const originalPlanAmount: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(ORIGINAL_PLAN_AMOUNT);
      const planDifferenceAmount: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(PLAN_DIFFERENCE_AMOUNT);
      const planPaymentAmount: Field<ExtraFieldProps> =
        viewRef.current?.props?.onFind(PLAN_PAYMENT_AMOUNT);

      const filerNameValue = isObject(values?.filerName as MagicKeyValue)
        ? values.filerName?.value
        : values?.filerName;

      const isTypeJoint = !isEqual(
        values?.bankruptcyType || formData?.bankruptcyType,
        BANKRUPTCY_TYPE_SINGLE
      );

      secondaryCreditBureauReportCode?.props?.props?.setVisible(
        isTypeJoint || filerNameValue === filerNameDropdownData?.[1]?.value
      );
      secondaryCreditBureauReportCode?.props?.props?.setRequired(
        isTypeJoint || filerNameValue === filerNameDropdownData?.[1]?.value
      );
      !isTypeJoint &&
        filerNameValue === filerNameDropdownData?.[0]?.value &&
        secondaryCreditBureauReportCode?.props?.props?.setValue(undefined);

      secondaryConsumerInformationIndicatorCode?.props?.props?.setVisible(
        isTypeJoint || filerNameValue === filerNameDropdownData?.[1]?.value
      );
      secondaryConsumerInformationIndicatorCode?.props?.props?.setRequired(
        isTypeJoint || filerNameValue === filerNameDropdownData?.[1]?.value
      );
      !isTypeJoint &&
        filerNameValue === filerNameDropdownData?.[0]?.value &&
        secondaryConsumerInformationIndicatorCode?.props?.props?.setValue(
          undefined
        );

      primaryConsumerInformationIndicatorCode?.props?.props?.setVisible(
        isTypeJoint || filerNameValue === filerNameDropdownData?.[0]?.value
      );
      primaryConsumerInformationIndicatorCode?.props?.props?.setRequired(
        isTypeJoint || filerNameValue === filerNameDropdownData?.[0]?.value
      );
      !isTypeJoint &&
        filerNameValue === filerNameDropdownData?.[1]?.value &&
        primaryConsumerInformationIndicatorCode?.props?.props?.setValue(
          undefined
        );

      primaryCreditBureauReportCode?.props?.props?.setVisible(
        isTypeJoint || filerNameValue === filerNameDropdownData?.[0]?.value
      );
      primaryCreditBureauReportCode?.props?.props?.setRequired(
        isTypeJoint || filerNameValue === filerNameDropdownData?.[0]?.value
      );
      !isTypeJoint &&
        filerNameValue === filerNameDropdownData?.[1]?.value &&
        primaryCreditBureauReportCode?.props?.props?.setValue(undefined);

      // set joinFilerName value to the another filerNameDropdown option
      jointFilerName?.props?.props.setVisible(isTypeJoint);
      !isTypeJoint && jointFilerName?.props?.props.setValue(undefined);

      jointFilerName?.props?.props?.setValue(
        filerNameDropdownData.filter(
          item => !isEqual(item?.value, filerNameValue)
        )?.[0]?.description
      );

      // filter bureau report code dropdown
      if (isEmpty(bureauReportCodeDropdownData)) return;

      if (!isTypeJoint) {
        primaryCreditBureauReportCode?.props?.props?.setData(
          bureauReportCodeDropdownData?.filter(
            item => item.value !== '2' && item.value !== '3'
          )
        );
        secondaryCreditBureauReportCode?.props?.props?.setData(
          bureauReportCodeDropdownData?.filter(
            item => item.value !== '2' && item.value !== '3'
          )
        );
      } else {
        primaryCreditBureauReportCode?.props?.props?.setData(
          bureauReportCodeDropdownData?.filter(
            item => item.value !== '1' && item.value !== '3'
          )
        );
        secondaryCreditBureauReportCode?.props?.props?.setData(
          bureauReportCodeDropdownData?.filter(
            item => item.value !== '1' && item.value !== '3'
          )
        );
      }

      // when select chapter 13, hide amount fields
      const isChapter13 =
        values?.bankruptcyChapterCode?.value === CHAPTER_13 ||
        values?.bankruptcyChapterCode === CHAPTER_13;
      originalPlanAmount?.props?.props?.setVisible(isChapter13);
      planDifferenceAmount?.props?.props?.setVisible(isChapter13);
      planPaymentAmount?.props?.props?.setVisible(isChapter13);
    },
    [filerNameDropdownData, formData, bureauReportCodeDropdownData]
  );

  return (
    <div
      className={classNames('px-24 pb-24 mx-auto max-width-lg', {
        'memos-extend': !isMemoSectionCollapsed
      })}
    >
      <BankruptcyContext.Provider
        value={{
          attorneyViewName,
          clerkViewName,
          mainViewName,
          trusteeViewName,
          formData: bankruptcyData
        }}
      >
        <View
          value={formData}
          descriptor={BANKRUPTCY_VIEW.BANKRUPTCY}
          formKey={`${BANKRUPTCY_VIEW.BANKRUPTCY}-${storeId}`}
          id={`${BANKRUPTCY_VIEW.BANKRUPTCY}-${storeId}`}
          ref={viewRef}
          onChange={handleOnChangeView}
        />
        <BankruptcyAttorney />
        <BankruptcyTrustee />
        <BankruptcyClerk />
      </BankruptcyContext.Provider>
    </div>
  );
};

export default BankruptcyForm;
