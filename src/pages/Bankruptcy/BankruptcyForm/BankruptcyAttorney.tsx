import React, { useContext, useMemo, useState } from 'react';

// components
import { View } from 'app/_libraries/_dof/core';
import { Button, Icon, Tooltip } from 'app/_libraries/_dls/components';

// Constants
import { BANKRUPTCY_VIEW } from '../constants';
import { I18N_COMMON_TEXT, I18N_COLLECTION_FORM } from 'app/constants/i18n';

// Context
import BankruptcyContext from '../BankruptcyContext/context';

// helpers
import className from 'classnames';
import { parseBankruptcyAttorney } from '../helpers';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

const BankruptcyAttorney: React.FC = () => {
  const { t } = useTranslation();
  const { attorneyViewName, formData } = useContext(BankruptcyContext);

  const [isExpand, setExpand] = useState(false);
  const testId = 'collectionForm_bankruptcy-attorney';

  const formValues = useMemo(
    () => parseBankruptcyAttorney(formData),
    [formData]
  );

  return (
    <div className="d-flex flex-column mt-24">
      <div className="d-flex align-items-center mr-12">
        <div className="mr-4">
          <Tooltip
            triggerClassName="ml-n4"
            placement="top"
            variant={'primary'}
            element={
              isExpand
                ? t(I18N_COMMON_TEXT.COLLAPSE)
                : t(I18N_COMMON_TEXT.EXPAND)
            }
          >
            <Button
              variant="icon-secondary"
              size="sm"
              onClick={() => setExpand(status => !status)}
              dataTestId={genAmtId(testId, 'collapse-expand-btn', '')}
            >
              <Icon name={isExpand ? 'minus' : 'plus'} size="4x" />
            </Button>
          </Tooltip>
        </div>
        <h6
          className="color-grey"
          data-testid={genAmtId(testId, 'addition-attorney-info', '')}
        >
          {t(I18N_COLLECTION_FORM.BANKRUPTCY_ADDITIONAL_ATTORNEY_INFO)}
        </h6>
      </div>
      <div
        className={className({
          'd-none': !isExpand
        })}
      >
        <View
          value={formValues}
          id={attorneyViewName}
          formKey={attorneyViewName || ''}
          descriptor={BANKRUPTCY_VIEW.PENDING_ATTORNEY}
        />
      </div>
    </div>
  );
};

export default BankruptcyAttorney;
