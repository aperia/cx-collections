import React from 'react';
import { fireEvent, screen } from '@testing-library/react';
import { CUSTOMER_ROLE_CODE } from 'app/constants/index';
import {
  renderMockStoreId,
  storeId,
  mockActionCreator,
  accEValue
} from 'app/test-utils';
// Redux
import { bankruptcyActions } from '../_redux/reducers';
import BankruptcyPendingForm from 'pages/Bankruptcy/BankruptcyForm/BankruptcyPendingForm';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
// Const
// constant
import {
  CHAPTER_13,
  BANKRUPTCY_TYPE_SINGLE,
  BANKRUPTCY_TYPE,
  BANKRUPTCY_VIEW,
  BANKRUPTCY_TYPE_JOINT
} from '../constants';
import { COLLECTION_EVENT } from 'pages/CollectionForm/constants';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);
const bankruptcyActionsSpy = mockActionCreator(bankruptcyActions);
const collectionFormActionsSpy = mockActionCreator(collectionFormActions);
beforeEach(() => {
  bankruptcyActionsSpy('getBureauCodeRefData');
  bankruptcyActionsSpy('getBankruptcyInformation');
  collectionFormActionsSpy('setDirtyForm');
});

describe('UI', () => {
  it('Should be render correctly with bankruptcyType is Single', () => {
    const getBankruptcyInformationSpy = bankruptcyActionsSpy(
      'getBankruptcyInformation'
    );
    jest.useFakeTimers();
    renderMockStoreId(<BankruptcyPendingForm />, {
      initialState: {
        bankruptcy: {
          [storeId]: {
            isLoadingRefData: false,
            info: {
              filerName: {
                value: 'filerName'
              },
              bankruptcyType: BANKRUPTCY_TYPE_SINGLE
            }
          }
        }
      }
    });
    jest.runAllTimers();
    expect(
      screen.getByText('txt_bankruptcy_additional_attorney_information')
    ).toBeInTheDocument();
    expect(
      screen.getByText('txt_bankruptcy_trustee_information')
    ).toBeInTheDocument();
    expect(
      screen.getByText('txt_bankruptcy_clerk_court_information')
    ).toBeInTheDocument();
    expect(getBankruptcyInformationSpy).toBeCalledWith({
      storeId
    });
  });

  it('Should be render correctly with bankruptcyType is Type Joint', () => {
    jest.useFakeTimers();
    renderMockStoreId(<BankruptcyPendingForm />, {
      initialState: {
        accountDetail: {
          [storeId]: {
            data: {
              primaryName: 'filerName',
              secondaryName: 'filerName'
            }
          }
        },
        bankruptcy: {
          [storeId]: {
            isLoadingRefData: false,
            info: {
              filerName: {
                value: 'filerName'
              },
              bankruptcyType: BANKRUPTCY_TYPE_JOINT.value
            }
          }
        }
      }
    });
    jest.runAllTimers();
    expect(
      screen.getByText('txt_bankruptcy_clerk_court_information')
    ).toBeInTheDocument();
  });

  it('Should be render correctly with bankruptcyType is empty and hasOnlyPrimaryCardholder is true', () => {
    jest.useFakeTimers();
    renderMockStoreId(<BankruptcyPendingForm />, {
      initialState: {
        accountDetail: {
          [storeId]: {
            customerInquiry: [
              { customerRoleTypeCode: CUSTOMER_ROLE_CODE.primary }
            ]
          }
        },
        bankruptcy: {
          [storeId]: {
            isLoadingRefData: false
          }
        }
      }
    });
    jest.runAllTimers();
    expect(
      screen.getByText('txt_bankruptcy_clerk_court_information')
    ).toBeInTheDocument();
  });

  it('Should be render correctly with bankruptcyType is Create', () => {
    jest.useFakeTimers();
    renderMockStoreId(<BankruptcyPendingForm />, {
      initialState: {
        accountDetail: {
          [storeId]: {
            data: {
              primaryName: 'filerName',
              secondaryName: 'filerName'
            }
          }
        },
        bankruptcy: {
          [storeId]: {
            info: {
              bankruptcyType: BANKRUPTCY_TYPE_SINGLE
            },
            creditBureauCodeRefData: [
              { value: 'filerName', description: 'test' },
              { value: 'filerName', description: 'test' }
            ]
          }
        },
        collectionForm: {
          [storeId]: {
            method: 'create'
          }
        }
      }
    });
    jest.runAllTimers();
    expect(
      screen.getByText('txt_bankruptcy_clerk_court_information')
    ).toBeInTheDocument();
  });

  it('Should be render correctly with bankruptcyType is empty and hasOnlyPrimaryCardholder is false', () => {
    jest.useFakeTimers();
    renderMockStoreId(<BankruptcyPendingForm />, {
      initialState: {
        accountDetail: {
          [storeId]: {
            customerInquiry: [
              { customerRoleTypeCode: CUSTOMER_ROLE_CODE.secondary }
            ]
          }
        },
        bankruptcy: {
          [storeId]: {
            isLoadingRefData: false
          }
        }
      }
    });
    jest.runAllTimers();
    expect(
      screen.getByText('txt_bankruptcy_clerk_court_information')
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  describe('onchange', () => {
    it('when values was not changed', () => {
      jest.useFakeTimers();
      renderMockStoreId(<BankruptcyPendingForm />, {
        initialState: {
          accountDetail: {
            [storeId]: {
              customerInquiry: [
                {
                  customerRoleTypeCode: CUSTOMER_ROLE_CODE.primary
                }
              ]
            }
          },
          bankruptcy: {
            [storeId]: {
              isLoadingRefData: false,
              creditBureauCodeRefData: [
                { value: '4', description: 'test' },
                { value: '5', description: 'test' }
              ]
            }
          }
        }
      });
      jest.runAllTimers();

      const input = screen.getByTestId(
        'bankruptcyStatusPendingMainInformationFormView_onChange'
      );

      fireEvent.change(input, {
        target: {
          value: 'value',
          values: {
            filerName: 'filerName'
          },
          previousValues: { filerName: 'filerName' }
        }
      });
      expect(
        screen.getByText('txt_bankruptcy_additional_attorney_information')
      ).toBeInTheDocument();
    });
    it('when values was changed with filerName is a object with isTypePoint equal true', () => {
      const triggerUpdateBankruptcySpy = bankruptcyActionsSpy(
        'triggerUpdateBankruptcy'
      );
      jest.useFakeTimers();
      renderMockStoreId(<BankruptcyPendingForm />, {
        initialState: {
          accountDetail: {
            [storeId]: {
              data: {
                primaryName: 'filerName',
                secondaryName: 'filerName'
              }
            }
          },
          bankruptcy: {
            [storeId]: {
              info: {
                bankruptcyType: BANKRUPTCY_TYPE_SINGLE
              },
              creditBureauCodeRefData: [
                { value: 'filerName', description: 'test' },
                { value: 'filerName', description: 'test' }
              ]
            }
          }
        }
      });
      jest.runAllTimers();
      fireEvent.change(
        screen.getByTestId(
          'bankruptcyStatusPendingMainInformationFormView_onChange'
        ),
        {
          target: {
            value: 'value',
            values: {
              filerName: { value: 'filerName' }
            },
            previousValues: { filerName: 'test' }
          }
        }
      );
      const event = new CustomEvent<string>(COLLECTION_EVENT.SUBMIT, {
        bubbles: true,
        cancelable: true,
        detail: ''
      });

      window.dispatchEvent(event);

      const expectViewName = [
        BANKRUPTCY_VIEW.PENDING_CLERK,
        BANKRUPTCY_VIEW.PENDING_TRUSTEE,
        BANKRUPTCY_VIEW.PENDING_ATTORNEY,
        BANKRUPTCY_VIEW.PENDING
      ].map(item => `${item}-${storeId}`);

      expect(triggerUpdateBankruptcySpy).toBeCalledWith({
        storeId,
        postData: {
          accountId: accEValue,
          type: BANKRUPTCY_TYPE.BANKRUPTCY_PENDING,
          viewName: expectViewName
        }
      });
    });
    it('when values was changed with filerName is a string with isTypePoint equal false', () => {
      jest.useFakeTimers();
      renderMockStoreId(<BankruptcyPendingForm />, {
        initialState: {
          accountDetail: {
            [storeId]: {
              data: {
                primaryName: 'filerName',
                secondaryName: 'filerName'
              }
            }
          },
          bankruptcy: {
            [storeId]: {
              isLoadingRefData: true,
              creditBureauCodeRefData: [
                { value: 'filerName', description: 'test' },
                { value: 'filerName', description: 'test' }
              ]
            }
          }
        }
      });
      jest.runAllTimers();

      const input = screen.getByTestId(
        'bankruptcyStatusPendingMainInformationFormView_onChange'
      );

      fireEvent.change(input, {
        target: {
          value: 'value',
          values: {
            filerName: 'filerName',
            bankruptcyType: BANKRUPTCY_TYPE_JOINT,
            bankruptcyChapterCode: { value: CHAPTER_13 }
          },
          previousValues: { filerName: 'test' }
        }
      });
      // for coverage
      expect(input).toBeInTheDocument();
    });

    it('when values was changed with filerName and bureauReportCodeDropdownData is empty ', () => {
      jest.useFakeTimers();
      renderMockStoreId(<BankruptcyPendingForm />, {
        initialState: {
          accountDetail: {
            [storeId]: {
              data: {
                primaryName: 'filerName',
                secondaryName: 'filerName'
              }
            }
          },
          bankruptcy: {
            [storeId]: {
              creditBureauCodeRefData: []
            }
          }
        }
      });
      jest.runAllTimers();
      const input = screen.getByTestId(
        'bankruptcyStatusPendingMainInformationFormView_onChange'
      );

      fireEvent.change(input, {
        target: {
          value: 'value',
          values: {
            filerName: 'filerName',
            bankruptcyType: BANKRUPTCY_TYPE_SINGLE
          },
          previousValues: { filerName: 'test' }
        }
      });
      // for coverage
      expect(input).toBeInTheDocument();
    });
  });
});
