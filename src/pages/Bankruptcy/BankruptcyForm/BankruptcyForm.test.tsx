import React from 'react';

// Component
import BankruptcyForm from 'pages/Bankruptcy/BankruptcyForm/BankruptcyForm';
import { CUSTOMER_ROLE_CODE } from 'app/constants/index';
// Reducer
import { bankruptcyActions } from '../_redux/reducers';

// Test util
import {
  mockActionCreator,
  renderMockStoreId,
  storeId,
  queryByClass,
  accEValue
} from 'app/test-utils';
import {
  CALL_RESULT_CODE,
  COLLECTION_EVENT
} from 'pages/CollectionForm/constants';
import { fireEvent, screen } from '@testing-library/react';

// Const
import {
  BANKRUPTCY_VIEW,
  CHAPTER_13,
  BANKRUPTCY_TYPE_SINGLE,
  BANKRUPTCY_TYPE,
  BANKRUPTCY_TYPE_JOINT
} from '../constants';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const bankruptcyActionsSpy = mockActionCreator(bankruptcyActions);
const collectionFormSpy = mockActionCreator(collectionFormActions);

beforeEach(() => {
  bankruptcyActionsSpy('getBureauCodeRefData');
  bankruptcyActionsSpy('getBankruptcyInformation');
  collectionFormSpy('setDirtyForm');
});

describe('UI', () => {
  it('Should be render correctly with bankruptcyType is Single', () => {
    const getBankruptcyInformationSpy = bankruptcyActionsSpy(
      'getBankruptcyInformation'
    );
    const setDirtyFormSpy = collectionFormSpy('setDirtyForm');
    jest.useFakeTimers();
    renderMockStoreId(<BankruptcyForm />, {
      initialState: {
        bankruptcy: {
          [storeId]: {
            isLoadingRefData: false,
            info: {
              filerName: {
                value: 'filerName'
              },
              bankruptcyType: BANKRUPTCY_TYPE_SINGLE
            }
          }
        },
        collectionForm: {
          [storeId]: {
            method: 'edit'
          }
        }
      }
    });
    jest.runAllTimers();
    expect(
      screen.getByText('txt_bankruptcy_additional_attorney_information')
    ).toBeInTheDocument();
    expect(
      screen.getByText('txt_bankruptcy_trustee_information')
    ).toBeInTheDocument();
    expect(
      screen.getByText('txt_bankruptcy_clerk_court_information')
    ).toBeInTheDocument();
    expect(getBankruptcyInformationSpy).toBeCalledWith({
      storeId
    });
    expect(setDirtyFormSpy).toBeCalledWith({
      storeId,
      callResult: CALL_RESULT_CODE.BANKRUPTCY,
      value: false
    });
  });

  it('Should be render correctly with bankruptcyType with Type Joint is true', () => {
    jest.useFakeTimers();
    renderMockStoreId(<BankruptcyForm />, {
      initialState: {
        accountDetail: {
          [storeId]: {
            data: {
              primaryName: 'filerName',
              secondaryName: 'filerName'
            }
          }
        },
        bankruptcy: {
          [storeId]: {
            isLoadingRefData: true,
            info: {
              filerName: {
                value: 'filerName'
              },
              bankruptcyType: BANKRUPTCY_TYPE_JOINT.value
            }
          }
        }
      }
    });
    jest.runAllTimers();
    expect(
      screen.getByText('txt_bankruptcy_clerk_court_information')
    ).toBeInTheDocument();
  });

  it('Should be render correctly with bankruptcyType with Type Joint is false', () => {
    jest.useFakeTimers();
    renderMockStoreId(<BankruptcyForm />, {
      initialState: {
        accountDetail: {
          [storeId]: {
            data: {
              primaryName: 'filerName',
              secondaryName: 'filerName'
            }
          }
        },
        bankruptcy: {
          [storeId]: {
            isLoadingRefData: true,
            info: {
              filerName: {
                value: 'filerName'
              },
              bankruptcyType: BANKRUPTCY_TYPE_SINGLE.value
            },
            creditBureauCodeRefData: [
              { value: 'filerName', description: 'test' },
              { value: 'filerName', description: 'test' }
            ]
          }
        }
      }
    });
    jest.runAllTimers();
    expect(
      screen.getByText('txt_bankruptcy_clerk_court_information')
    ).toBeInTheDocument();
  });

  it('Should be render correctly with bankruptcyType is empty and hasOnlyPrimaryCardholder is true', () => {
    jest.useFakeTimers();
    renderMockStoreId(<BankruptcyForm />, {
      initialState: {
        accountDetail: {
          [storeId]: {
            customerInquiry: [
              { customerRoleTypeCode: CUSTOMER_ROLE_CODE.primary }
            ]
          }
        },
        bankruptcy: {
          [storeId]: {
            isLoadingRefData: true
          }
        }
      }
    });
    jest.runAllTimers();
    expect(
      screen.getByText('txt_bankruptcy_clerk_court_information')
    ).toBeInTheDocument();
  });

  it('Should be render correctly with bankruptcyType is empty and hasOnlyPrimaryCardholder is false', () => {
    jest.useFakeTimers();
    renderMockStoreId(<BankruptcyForm />, {
      initialState: {
        accountDetail: {
          [storeId]: {
            customerInquiry: [
              { customerRoleTypeCode: CUSTOMER_ROLE_CODE.secondary }
            ]
          }
        },
        bankruptcy: {
          [storeId]: {
            isLoadingRefData: true
          }
        }
      }
    });
    jest.runAllTimers();
    expect(
      screen.getByText('txt_bankruptcy_clerk_court_information')
    ).toBeInTheDocument();
  });
});

describe('BankruptcyForm Action', () => {
  it('should render bankruptcy forms', () => {
    jest.useFakeTimers();
    bankruptcyActionsSpy('getBankruptcyInformation');
    renderMockStoreId(<BankruptcyForm />, {
      initialState: {
        collectionForm: {
          [storeId]: {
            lastFollowUpData: {
              callResultType: CALL_RESULT_CODE.BANKRUPTCY,
              data: {
                additionalAttorneyInfo: {},
                clerkCourtInfo: {},
                fillingInfo: {},
                mainInfo: {
                  attorneyName: 'aaaaaaaaaaaaaaaa',
                  attorneyPhoneNumber: '1111111111',
                  case: '1',
                  chapter: '7'
                },
                trusteeInfo: {}
              }
            },
            uploadFiles: {}
          }
        }
      }
    });
    jest.runAllTimers();
    const input = screen.getByTestId(
      'bankruptcyStatusPendingAdditionalClerkInformationFormView_onChange'
    );
    fireEvent.change(input, {
      target: {
        value: 'value',
        values: { filerName: 'filerName' },
        previousValues: { filerName: '' }
      }
    });

    expect(screen.getByTestId(BANKRUPTCY_VIEW.BANKRUPTCY)).toBeInTheDocument();
    expect(
      screen.getByTestId(BANKRUPTCY_VIEW.PENDING_ATTORNEY)
    ).toBeInTheDocument();
    expect(
      screen.getByTestId(BANKRUPTCY_VIEW.PENDING_TRUSTEE)
    ).toBeInTheDocument();
    expect(
      screen.getByTestId(BANKRUPTCY_VIEW.PENDING_CLERK)
    ).toBeInTheDocument();
  });

  it('it should have class "memos-extend" is work correctly', () => {
    bankruptcyActionsSpy('getBankruptcyInformation');
    bankruptcyActionsSpy('triggerUpdateBankruptcy');
    const { wrapper: wrapperOpenMemoCollapsed } = renderMockStoreId(
      <BankruptcyForm />,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              isMemoSectionCollapsed: false,
              uploadFiles: {}
            }
          }
        }
      }
    );
    expect(
      queryByClass(wrapperOpenMemoCollapsed.container, /memos-extend/)
    ).toBeInTheDocument();
    const { wrapper: wrapperCloseMemoCollapsed } = renderMockStoreId(
      <BankruptcyForm />,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              isMemoSectionCollapsed: true,
              uploadFiles: {}
            }
          }
        }
      }
    );
    expect(
      queryByClass(wrapperCloseMemoCollapsed.container, /memos-extend/)
    ).not.toBeInTheDocument();
  });
});

describe('Action onChange form', () => {
  it('when values was not changed', () => {
    jest.useFakeTimers();
    renderMockStoreId(<BankruptcyForm />);
    jest.runAllTimers();

    const input = screen.getByTestId(
      'bankruptcyStatusMainInformationFormView_onChange'
    );

    fireEvent.change(input, {
      target: {
        value: 'value',
        values: {
          filerName: 'filerName'
        },
        previousValues: { filerName: 'filerName' }
      }
    });
    // for coverage
    expect(input).toBeInTheDocument();
  });

  it('when values was changed with filerName is a object with isTypePoint equal true', () => {
    const triggerUpdateBankruptcySpy = bankruptcyActionsSpy(
      'triggerUpdateBankruptcy'
    );
    jest.useFakeTimers();
    renderMockStoreId(<BankruptcyForm />, {
      initialState: {
        accountDetail: {
          [storeId]: {
            data: {
              primaryName: 'filerName',
              secondaryName: 'filerName'
            }
          }
        },
        bankruptcy: {
          [storeId]: {
            creditBureauCodeRefData: [
              { value: 'filerName', description: 'test' },
              { value: 'filerName', description: 'test' }
            ]
          }
        }
      }
    });
    jest.runAllTimers();
    const input = screen.getByTestId(
      'bankruptcyStatusMainInformationFormView_onChange'
    );
    fireEvent.change(input, {
      target: {
        value: 'value',
        values: {
          filerName: { value: 'filerName' },
          bankruptcyType: BANKRUPTCY_TYPE_SINGLE
        },
        previousValues: { filerName: 'test' }
      }
    });
    const event = new CustomEvent<string>(COLLECTION_EVENT.SUBMIT, {
      bubbles: true,
      cancelable: true,
      detail: ''
    });

    window.dispatchEvent(event);

    const expectViewName = [
      BANKRUPTCY_VIEW.PENDING_CLERK,
      BANKRUPTCY_VIEW.PENDING_TRUSTEE,
      BANKRUPTCY_VIEW.PENDING_ATTORNEY,
      BANKRUPTCY_VIEW.BANKRUPTCY
    ].map(item => `${item}-${storeId}`);

    expect(triggerUpdateBankruptcySpy).toBeCalledWith({
      storeId,
      postData: {
        accountId: accEValue,
        type: BANKRUPTCY_TYPE.BANKRUPTCY,
        viewName: expectViewName
      }
    });
  });

  it('when values was changed with filerName is a string with isTypePoint equal false', () => {
    jest.useFakeTimers();
    renderMockStoreId(<BankruptcyForm />, {
      initialState: {
        accountDetail: {
          [storeId]: {
            data: {
              primaryName: 'filerName',
              secondaryName: 'filerName'
            }
          }
        },
        bankruptcy: {
          [storeId]: {
            creditBureauCodeRefData: [
              { value: 'filerName', description: 'test' },
              { value: 'filerName', description: 'test' }
            ]
          }
        }
      }
    });
    jest.runAllTimers();

    const input = screen.getByTestId(
      'bankruptcyStatusMainInformationFormView_onChange'
    );

    fireEvent.change(input, {
      target: {
        value: 'value',
        values: {
          filerName: 'filerName',
          bankruptcyType: null,
          bankruptcyChapterCode: { value: CHAPTER_13 }
        },
        previousValues: { filerName: 'test' }
      }
    });
    // for coverage
    expect(input).toBeInTheDocument();
  });

  it('when values was changed with filerName and bureauReportCodeDropdownData is empty ', () => {
    jest.useFakeTimers();
    renderMockStoreId(<BankruptcyForm />, {
      initialState: {
        accountDetail: {
          [storeId]: {
            data: {
              primaryName: 'filerName',
              secondaryName: 'filerName'
            }
          }
        },
        bankruptcy: {
          [storeId]: {
            creditBureauCodeRefData: []
          }
        }
      }
    });
    jest.runAllTimers();
    const input = screen.getByTestId(
      'bankruptcyStatusMainInformationFormView_onChange'
    );

    fireEvent.change(input, {
      target: {
        value: 'value',
        values: {
          filerName: 'filerName',
          bankruptcyType: BANKRUPTCY_TYPE_SINGLE
        },
        previousValues: { filerName: 'test' }
      }
    });
    // for coverage
    expect(input).toBeInTheDocument();
  });
});
