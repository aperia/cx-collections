import React, { useContext, useMemo, useState } from 'react';

// components
import { View } from 'app/_libraries/_dof/core';
import { Button, Icon, Tooltip } from 'app/_libraries/_dls/components';

// utils
import classNames from 'classnames';

// Constant
import { I18N_COMMON_TEXT, I18N_COLLECTION_FORM } from 'app/constants/i18n';

// Context
import BankruptcyContext from '../BankruptcyContext/context';
import { BANKRUPTCY_VIEW } from '../constants';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Helper
import { parseBankruptcyTrustee } from '../helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';

const BankruptcyTrustee: React.FC = () => {
  const { t } = useTranslation();
  const { trusteeViewName = '', formData } = useContext(BankruptcyContext);

  const [isExpand, setExpand] = useState(false);
  const testId = 'collectionForm_bankruptcy-trustee';

  const formValues = useMemo(
    () => parseBankruptcyTrustee(formData),
    [formData]
  );

  return (
    <div className="d-flex flex-column mt-24">
      <div className="d-flex align-items-center mr-12">
        <div className="mr-4">
          <Tooltip
            triggerClassName="ml-n4"
            placement="top"
            variant={'primary'}
            element={
              isExpand
                ? t(I18N_COMMON_TEXT.COLLAPSE)
                : t(I18N_COMMON_TEXT.EXPAND)
            }
          >
            <Button
              variant="icon-secondary"
              size="sm"
              onClick={() => setExpand(status => !status)}
              dataTestId={genAmtId(testId, 'collapse-expand-btn', '')}
            >
              <Icon name={isExpand ? 'minus' : 'plus'} size="4x" />
            </Button>
          </Tooltip>
        </div>
        <h6
          className="color-grey"
          data-testid={genAmtId(testId, 'trustee-info', '')}
        >
          {t(I18N_COLLECTION_FORM.BANKRUPTCY_TRUSTEE_INFO)}
        </h6>
      </div>
      <div
        className={classNames({
          'd-none': !isExpand
        })}
      >
        <View
          value={formValues}
          id={trusteeViewName}
          formKey={trusteeViewName}
          descriptor={BANKRUPTCY_VIEW.PENDING_TRUSTEE}
        />
      </div>
    </div>
  );
};

export default BankruptcyTrustee;
