import React from 'react';
import BankruptcyTrustee from 'pages/Bankruptcy/BankruptcyForm/BankruptcyTrustee';
import {  renderMockStoreId } from 'app/test-utils';
import { screen } from '@testing-library/react';
import { BANKRUPTCY_VIEW } from '../constants';
import { queryByClass } from 'app/test-utils';
import userEvent from '@testing-library/user-event';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);


describe('BankruptcyForm Component', () => {
  it('should render component correctly', () => {
    renderMockStoreId(<BankruptcyTrustee />);

    expect(
      screen.getByTestId(BANKRUPTCY_VIEW.PENDING_TRUSTEE)
    ).toBeInTheDocument();
  });

  it('should render icon correctly', () => {
    renderMockStoreId(<BankruptcyTrustee />);
    const buttonExpand = screen.getByRole('button');

    const iconPlus = buttonExpand.getElementsByClassName('icon-plus');

    expect(iconPlus).toHaveLength(1);

    userEvent.click(buttonExpand);

    const iconMinus = buttonExpand.getElementsByClassName('icon-minus');
    expect(iconMinus).toHaveLength(1);
  });

  it('should have class name "d-none" when expanded', () => {
    const {wrapper} = renderMockStoreId(<BankruptcyTrustee />);
    const buttonExpand = screen.getByRole('button');
    expect(queryByClass(wrapper.container,'d-none')).toBeInTheDocument()
    userEvent.click(buttonExpand);
    expect(queryByClass(wrapper.container,'d-none')).not.toBeInTheDocument()
  })

  it('tooltip is work correctly',  () => {
     renderMockStoreId(<BankruptcyTrustee />);
    const buttonExpand = screen.getByRole('button');

    userEvent.hover(buttonExpand)
    const tooltip =  screen.getByText('txt_expand')
    expect(tooltip).toBeInTheDocument();
    userEvent.unhover(buttonExpand)
    expect(tooltip).not.toBeInTheDocument();
  })

});
