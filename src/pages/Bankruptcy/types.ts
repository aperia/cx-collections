// Const
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';

// Type
import { IDataRequestDeceasedPending } from 'pages/Deceased/types';

export interface MainInfo {
  attorneyName?: string;
  attorneyPhoneNumber?: string;
  case?: string;
  chapter?: string;
  originalPlanAmount: string;
  planDifferenceAmount: string;
  planPaymentAmount: string;
}

export interface FillingInfo {
  fillingInfoAssets?: string;
  fillingInfoCloseDate?: string;
  fillingInfoConfirmedDate?: string;
  fillingInfoConversionDate?: string;
  fillingInfoDischargeDate?: string;
  fillingInfoDismissalDate?: string;
  fillingInfoFileDate?: string;
  fillingInfoJurisdiction?: string;
  fillingInfoNonfilerCollectibleIndicator?: string;
  fillingInfoNonfilerIndicator?: string;
  fillingInfoPOCAmount?: string;
  primaryCreditBureauReportCode?: string;
  primaryConsumerInformationIndicatorCode?: string;
  fillingInfoProSe?: string;
  fillingInfoProofOfClaimBarDate?: string;
  fillingInfoReaffirmedDate?: string;
  fillingInfoRedeemedAmount?: string;
  fillingInfoReopenDate?: string;
  secondaryCreditBureauReportCode?: string;
  secondaryConsumerInformationIndicatorCode?: string;
  fillingInfoSettledAmount?: string;
  fillingInfoSurrenderIndicator?: string;
  bankruptcyType?: string;
  filerName?: string;
  jointFilerName?: string;
}

export interface AdditionalAttorneyInfo {
  attorneyInfoAddressLineOne?: string;
  attorneyInfoAddressLineTwo?: string;
  attorneyInfoCity?: string;
  attorneyInfoEmail?: string;
  attorneyInfoFax?: string;
  attorneyInfoFirm?: string;
  attorneyInfoRetained?: string;
  attorneyInfoState?: string;
  attorneyInfoZip?: string;
}

export interface TrusteeInfo {
  trusteeInfoAddressLineOne?: string;
  trusteeInfoAddressLineTwo?: string;
  trusteeInfoCity?: string;
  trusteeInfoEmail?: string;
  trusteeInfoFax?: string;
  trusteeInfoName?: string;
  trusteeInfoPhone?: string;
  trusteeInfoState?: string;
  trusteeInfoZip?: string;
}

export interface ClerkCourtInfo {
  clerkCourtInfoAddressLineOne?: string;
  clerkCourtInfoAddressLineTwo?: string;
  clerkCourtInfoCity?: string;
  clerkCourtInfoEmail?: string;
  clerkCourtInfoName?: string;
  clerkCourtInfoPhone?: string;
  clerkCourtInfoState?: string;
  clerkCourtInfoZip?: string;
}
export interface BankruptcyData {
  isUpdating?: boolean;
  isLoading?: boolean;
  isLoadingRefData?: boolean;
  info?: MagicKeyValue;
  data?: BankruptcyResult;
  rawData?: BankruptcyResult;
  creditBureauCodeRefData?: RefDataValue[];
  assetsRefData?: RefDataValue[];
  proSERefData?: RefDataValue[];
  viewData?: BankruptcyResult;
}

export interface BankruptcyResult {
  mainInfo?: MainInfo;
  fillingInfo?: FillingInfo;
  additionalAttorneyInfo?: AdditionalAttorneyInfo;
  trusteeInfo?: TrusteeInfo;
  clerkCourtInfo?: ClerkCourtInfo;
}

// Common Bankruptcy Payload
export interface CommonBankruptcyPayload {
  common?: {
    app?: string;
    accountId?: string;
    org?: string;
  };
}

// Update Bankruptcy
export interface TriggerUpdateBankruptcyArgs extends StoreIdPayload {
  postData: {
    viewName?: Array<string>;
    method?: COLLECTION_METHOD.CREATE | COLLECTION_METHOD.EDIT;
    type?: string;
    accountId: string;
  };
}

export interface UpdateBankruptcyRequestPayload {}
export interface UpdateBankruptcyRequestArg
  extends CommonBankruptcyPayload,
    StoreIdPayload {
  formData?: MagicKeyValue;
  method?: COLLECTION_METHOD.CREATE | COLLECTION_METHOD.EDIT;
  type?: string;
}

export interface UpdateBankruptcyRequestData
  extends CommonBankruptcyPayload,
    MagicKeyValue {}

// Form detail
export interface CollectionFormDetailPayload {
  callResultType?: string;
  data: CollectionCurrentData;
  method?: string;
}

export interface CollectionCurrentData
  extends MagicKeyValue,
    IDataRequestDeceasedPending {}

// Remove Bankruptcy
export interface RemoveBankruptcyDetailArg extends StoreIdPayload {
  type?: string;
}

export interface RemoveBankruptcyStatusArg extends CommonBankruptcyPayload {
  operatorCode: string;
  actionEntryCode: string;
  cardholderContactedName: string;
}

// Get bankruptcy information
export interface GetBankruptcyRequestData extends CommonBankruptcyPayload {
  selectFields?: Array<string>;
}

export interface GetBankruptcyResponseData {
  bankruptcyInformationList?: Array<MagicKeyValue>;
  processStatus?: string;
}

export interface GetBankruptcyArgs extends StoreIdPayload {
  mapValueWithDescription?: boolean;
}

export interface GetBankruptcyPayload {
  info?: MagicKeyValue;
  data?: BankruptcyData['data'];
  rawData?: MagicKeyValue;
  viewData?: BankruptcyResult;
}

// State
export interface BankruptcyState {
  [storeId: string]: BankruptcyData;
}

export enum BankruptcyResultTab {
  BankruptcyInfo = '01',
  BankruptcyCertificate = '02'
}
