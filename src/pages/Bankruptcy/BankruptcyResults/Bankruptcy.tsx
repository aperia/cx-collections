import React, { useCallback, useEffect, useMemo, useRef } from 'react';
import classnames from 'classnames';
import { batch, useDispatch } from 'react-redux';
import isEmpty from 'lodash.isempty';

// components
import { ExtraFieldProps, View } from 'app/_libraries/_dof/core';
import DataSection from 'pages/__commons/DataSection';

// Hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// Redux
import {
  takeBankruptcyAttorneyInfo,
  takeBankruptcyClerkCourtInfo,
  takeBankruptcyFillingInfo,
  selectBankruptcyMainInfo,
  takeBankruptcyTrusteeInfo,
  takeBankruptcyLoadingStatus,
  takeBankruptcyResult,
  assetsRefData,
  proSERefData
} from '../_redux/selectors';
import { bankruptcyActions } from '../_redux/reducers';

// Types
import {
  AdditionalAttorneyInfo,
  BankruptcyResult,
  ClerkCourtInfo,
  FillingInfo,
  MainInfo,
  TrusteeInfo
} from '../types';
import { genAmtId } from 'app/_libraries/_dls/utils';
import {
  ASSETS,
  BANKRUPTCY_CHAPTER_CODE,
  BANKRUPTCY_INFO_TYPE,
  BANKRUPTCY_TYPE_SINGLE,
  CHAPTER_13,
  FILER_NAME,
  JOINT_FILER_NAME,
  ORIGINAL_PLAN_AMOUNT,
  PLAN_DIFFERENCE_AMOUNT,
  PLAN_PAYMENT_AMOUNT,
  PRIMARY_CONSUMER_INFORMATION_INDICATOR_CODE,
  PRIMARY_CREDIT_BUREAU_REPORT_CODE,
  PROSE,
  SECONDARY_CREDIT_BUREAU_REPORT_CODE,
  SECONDARY_CUSTOMER_INFO_INDICATOR_CODE
} from '../constants';
import { Field } from 'redux-form';
import { useFilerNameDropdownData } from 'pages/AccountDetails/_redux/selectors';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { isUndefined } from 'lodash';

const Bankruptcy: React.FC = () => {
  const { storeId, accEValue } = useAccountDetail();
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const viewMainInfoRef = useRef<any>();
  const viewFillingInfoRef = useRef<any>();

  const isLoading = useStoreIdSelector<boolean>(takeBankruptcyLoadingStatus);
  const info = useStoreIdSelector<BankruptcyResult>(takeBankruptcyResult);
  const mainInfo = useStoreIdSelector<MainInfo>(selectBankruptcyMainInfo);
  const fillingInfoData = useStoreIdSelector<FillingInfo>(
    takeBankruptcyFillingInfo
  );
  const attorneyInfo = useStoreIdSelector<AdditionalAttorneyInfo>(
    takeBankruptcyAttorneyInfo
  );
  const trusteeInfo = useStoreIdSelector<TrusteeInfo>(
    takeBankruptcyTrusteeInfo
  );
  const clerkCourtInfo = useStoreIdSelector<ClerkCourtInfo>(
    takeBankruptcyClerkCourtInfo
  );

  const filerNameDropdownData = useFilerNameDropdownData();

  const assetData = useStoreIdSelector<RefDataValue[]>(assetsRefData);

  const proSEData = useStoreIdSelector<RefDataValue[]>(proSERefData);

  const translateGroupValue = useCallback(
    (text: string) => {
      return text
        .split(' - ')
        .map(item => t(item.trim()))
        .join(' - ');
    },
    [t]
  );

  const fillingInfo: FillingInfo = useMemo(() => {
    const infoData = { ...fillingInfoData };

    if (!isUndefined(fillingInfoData?.fillingInfoSurrenderIndicator)) {
      infoData.fillingInfoSurrenderIndicator = translateGroupValue(
        infoData.fillingInfoSurrenderIndicator!
      );
    }

    if (!isUndefined(fillingInfoData?.fillingInfoNonfilerIndicator)) {
      infoData.fillingInfoNonfilerIndicator = translateGroupValue(
        fillingInfoData.fillingInfoNonfilerIndicator
      );
    }

    if (
      !isUndefined(fillingInfoData?.fillingInfoNonfilerCollectibleIndicator)
    ) {
      infoData.fillingInfoNonfilerCollectibleIndicator = translateGroupValue(
        fillingInfoData.fillingInfoNonfilerCollectibleIndicator
      );
    }

    if (
      !isUndefined(fillingInfoData?.primaryConsumerInformationIndicatorCode)
    ) {
      infoData.primaryConsumerInformationIndicatorCode = translateGroupValue(
        fillingInfoData.primaryConsumerInformationIndicatorCode
      );
    }

    if (!isUndefined(fillingInfoData?.primaryCreditBureauReportCode)) {
      infoData.primaryCreditBureauReportCode = translateGroupValue(
        fillingInfoData.primaryCreditBureauReportCode
      );
    }

    if (
      !isUndefined(fillingInfoData?.secondaryConsumerInformationIndicatorCode)
    ) {
      infoData.secondaryConsumerInformationIndicatorCode = translateGroupValue(
        fillingInfoData.secondaryConsumerInformationIndicatorCode
      );
    }

    if (!isUndefined(fillingInfoData?.secondaryCreditBureauReportCode)) {
      infoData.secondaryCreditBureauReportCode = translateGroupValue(
        fillingInfoData.secondaryCreditBureauReportCode
      );
    }

    return infoData;
  }, [fillingInfoData, translateGroupValue]);

  useEffect(() => {
    batch(() => {
      dispatch(bankruptcyActions.getAssets({ storeId }));
      dispatch(bankruptcyActions.getProSE({ storeId }));
    });
  }, [dispatch, storeId]);

  useEffect(() => {
    if (isEmpty(info)) {
      dispatch(
        bankruptcyActions.getBankruptcyInformation({
          storeId,
          mapValueWithDescription: true
        })
      );
    }
  }, [dispatch, storeId, accEValue, info]);

  useEffect(() => {
    setImmediate(() => {
      const originalPlanAmount: Field<ExtraFieldProps> =
        viewMainInfoRef.current?.props?.onFind(ORIGINAL_PLAN_AMOUNT);
      const planDifferenceAmount: Field<ExtraFieldProps> =
        viewMainInfoRef.current?.props?.onFind(PLAN_DIFFERENCE_AMOUNT);
      const planPaymentAmount: Field<ExtraFieldProps> =
        viewMainInfoRef.current?.props?.onFind(PLAN_PAYMENT_AMOUNT);
      const secondaryCreditBureauReportCode: Field<ExtraFieldProps> =
        viewFillingInfoRef.current?.props?.onFind(
          SECONDARY_CREDIT_BUREAU_REPORT_CODE
        );
      const secondaryConsumerInformationIndicatorCode: Field<ExtraFieldProps> =
        viewFillingInfoRef.current?.props?.onFind(
          SECONDARY_CUSTOMER_INFO_INDICATOR_CODE
        );
      const jointFilerName: Field<ExtraFieldProps> =
        viewFillingInfoRef.current?.props?.onFind(JOINT_FILER_NAME);
      const primaryCreditBureauReportCode: Field<ExtraFieldProps> =
        viewFillingInfoRef.current?.props?.onFind(
          PRIMARY_CREDIT_BUREAU_REPORT_CODE
        );
      const primaryConsumerInformationIndicatorCode: Field<ExtraFieldProps> =
        viewFillingInfoRef.current?.props?.onFind(
          PRIMARY_CONSUMER_INFORMATION_INDICATOR_CODE
        );

      const shouldShowAmount =
        mainInfo?.[BANKRUPTCY_CHAPTER_CODE] === CHAPTER_13;
      originalPlanAmount?.props?.props?.setVisible(shouldShowAmount);
      planDifferenceAmount?.props?.props?.setVisible(shouldShowAmount);
      planPaymentAmount?.props?.props?.setVisible(shouldShowAmount);

      if (
        fillingInfo?.[BANKRUPTCY_INFO_TYPE] === BANKRUPTCY_TYPE_SINGLE?.value
      ) {
        jointFilerName?.props?.props?.setVisible(false);
      }
      if (
        fillingInfo?.[BANKRUPTCY_INFO_TYPE] === BANKRUPTCY_TYPE_SINGLE?.value &&
        fillingInfo?.[FILER_NAME] === filerNameDropdownData?.[0]?.value
      ) {
        secondaryCreditBureauReportCode?.props?.props?.setVisible(false);
        secondaryConsumerInformationIndicatorCode?.props?.props?.setVisible(
          false
        );
      }
      if (
        fillingInfo?.[BANKRUPTCY_INFO_TYPE] === BANKRUPTCY_TYPE_SINGLE?.value &&
        fillingInfo?.[FILER_NAME] === filerNameDropdownData?.[1]?.value
      ) {
        primaryCreditBureauReportCode?.props?.props?.setVisible(false);
        primaryConsumerInformationIndicatorCode?.props?.props?.setVisible(
          false
        );
      }

      const fillingInfoAssetInfo: Field<ExtraFieldProps> =
        viewFillingInfoRef.current?.props?.onFind(ASSETS);
      if (fillingInfoAssetInfo) {
        const fillingInfoAssetDesc = t(
          assetData?.find(
            item =>
              item?.value.trim() === fillingInfo?.fillingInfoAssets?.trim()
          )?.description
        );
        fillingInfoAssetInfo?.props?.props?.setValue(
          fillingInfoAssetDesc
            ? `${fillingInfo?.fillingInfoAssets} - ${fillingInfoAssetDesc} `
            : fillingInfoAssetDesc || fillingInfo?.fillingInfoAssets
        );
      }

      const fillingInfoProSe: Field<ExtraFieldProps> =
        viewFillingInfoRef.current?.props?.onFind(PROSE);
      if (fillingInfoProSe) {
        const fillingInfoProSeDesc = t(
          proSEData?.find(
            item => item?.value.trim() === fillingInfo?.fillingInfoProSe?.trim()
          )?.description
        );
        fillingInfoProSe?.props?.props?.setValue(
          fillingInfoProSeDesc
            ? `${fillingInfo?.fillingInfoProSe} - ${fillingInfoProSeDesc}`
            : fillingInfoProSeDesc || fillingInfo?.fillingInfoProSe
        );
      }
    });
  }, [mainInfo, fillingInfo, filerNameDropdownData, assetData, proSEData, t]);

  return (
    <div
      className={classnames('mt-16', {
        loading: isLoading
      })}
    >
      <div className={'pb-24'}>
        <DataSection
          label="txt_bankruptcy_main_information"
          isNoData={isEmpty(mainInfo)}
          component={
            <View
              id={`${storeId}-bankruptcyMainInfo`}
              formKey={`${storeId}-bankruptcyMainInfo`}
              descriptor="bankruptcyMainInfo"
              value={mainInfo}
              ref={viewMainInfoRef}
            />
          }
          dataTestId={genAmtId('collectionForm', 'bankruptcyMainInfo', '')}
        />
      </div>
      <div className={'divider-dashed'} />
      <div className="mt-24 pb-24">
        <DataSection
          label="txt_bankruptcy_general_information"
          isNoData={isEmpty(fillingInfo)}
          component={
            <View
              id={`${storeId}-bankruptcyFillingInfo`}
              formKey={`${storeId}-bankruptcyFillingInfo`}
              descriptor="bankruptcyFillingInfo"
              value={fillingInfo}
              ref={viewFillingInfoRef}
            />
          }
          dataTestId={genAmtId('collectionForm', 'bankruptcyFillingInfo', '')}
        />
      </div>
      <div className="divider-dashed" />
      <div className="mt-24 pb-24">
        <DataSection
          label="txt_bankruptcy_additional_attorney_information"
          isNoData={isEmpty(attorneyInfo)}
          component={
            <View
              id={`${storeId}-bankruptcyAdditionalAttorneyInfo`}
              formKey={`${storeId}-bankruptcyAdditionalAttorneyInfo`}
              descriptor="bankruptcyAdditionalAttorneyInfo"
              value={attorneyInfo}
            />
          }
          dataTestId={genAmtId(
            'collectionForm',
            'bankruptcyAdditionalAttorneyInfo',
            ''
          )}
        />
      </div>
      <div className="divider-dashed" />
      <div className="mt-24 pb-24">
        <DataSection
          label="txt_bankruptcy_trustee_information"
          isNoData={isEmpty(trusteeInfo)}
          component={
            <View
              id={`${storeId}-bankruptcyTrusteeInfo`}
              formKey={`${storeId}-bankruptcyTrusteeInfo`}
              descriptor="bankruptcyTrusteeInfo"
              value={trusteeInfo}
            />
          }
          dataTestId={genAmtId('collectionForm', 'bankruptcyTrusteeInfo', '')}
        />
      </div>
      <div className="divider-dashed" />
      <div className="mt-24">
        <DataSection
          label="txt_bankruptcy_clerk_court_information"
          isNoData={isEmpty(clerkCourtInfo)}
          component={
            <View
              id={`${storeId}-bankruptcyClerkCourtInfo`}
              formKey={`${storeId}-bankruptcyClerkCourtInfo`}
              descriptor="bankruptcyClerkCourtInfo"
              value={clerkCourtInfo}
            />
          }
          dataTestId={genAmtId(
            'collectionForm',
            'bankruptcyClerkCourtInfo',
            ''
          )}
        />
      </div>
    </div>
  );
};

export default Bankruptcy;
