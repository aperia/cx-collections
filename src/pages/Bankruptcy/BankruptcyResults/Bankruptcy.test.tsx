import React from 'react';
import Bankruptcy from './Bankruptcy';

import { renderMockStoreId } from 'app/test-utils/renderComponentWithMockStore';
import { AccountDetailProvider } from 'app/hooks';
import { screen } from '@testing-library/react';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { mockUseTranslate, queryByClass } from 'app/test-utils';
import { mockActionCreator } from 'app/test-utils';
import { bankruptcyActions } from '../_redux/reducers';
import { storeId } from 'app/test-utils';
import { BANKRUPTCY_TYPE_SINGLE } from '../constants';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

mockUseTranslate();

const bankruptcyActionsSpy = mockActionCreator(bankruptcyActions);

describe('Test Bankruptcy component in Collection Form Results', () => {
  it('should render component', () => {
    renderMockStoreId(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <Bankruptcy />
      </AccountDetailProvider>,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              lastFollowUpData: {
                callResultType: CALL_RESULT_CODE.BANKRUPTCY,
                data: {
                  additionalAttorneyInfo: {},
                  clerkCourtInfo: {},
                  fillingInfo: {},
                  mainInfo: {
                    attorneyName: 'aaaaaaaaaaaaaaaa',
                    attorneyPhoneNumber: '1111111111',
                    case: '1',
                    chapter: '7'
                  },
                  trusteeInfo: {}
                }
              },
              callResultType: ''
            }
          }
        }
      }
    );

    expect(
      screen.getByText('txt_bankruptcy_main_information')
    ).toBeInTheDocument();
    expect(
      screen.getByText('txt_bankruptcy_general_information')
    ).toBeInTheDocument();
    expect(
      screen.getByText('txt_bankruptcy_additional_attorney_information')
    ).toBeInTheDocument();
    expect(
      screen.getByText('txt_bankruptcy_trustee_information')
    ).toBeInTheDocument();
    expect(
      screen.getByText('txt_bankruptcy_clerk_court_information')
    ).toBeInTheDocument();
  });

  it('it should be have class loading, when loading is true', () => {
    const { wrapper } = renderMockStoreId(<Bankruptcy />, {
      initialState: {
        bankruptcy: {
          [storeId]: {
            isLoading: true
          }
        }
      }
    });
    expect(queryByClass(wrapper.container, /loading/)).toBeInTheDocument();
  });

  it('get Bankruptcy Information should be call', () => {
    const getBankruptcyInformationSpy = bankruptcyActionsSpy(
      'getBankruptcyInformation'
    );
    jest.useFakeTimers();
    renderMockStoreId(<Bankruptcy />, {});
    jest.runAllTimers();
    expect(getBankruptcyInformationSpy).toBeCalled();
  });

  it('get Bankruptcy Information not should be call', () => {
    jest.useFakeTimers();
    const getBankruptcyInformationSpy = bankruptcyActionsSpy(
      'getBankruptcyInformation'
    );
    renderMockStoreId(<Bankruptcy />, {
      initialState: {
        bankruptcy: {
          [storeId]: {
            viewData: {
              clerkCourtInfo: {
                clerkCourtInfoZip: '700'
              },
              fillingInfo: {
                bankruptcyType: BANKRUPTCY_TYPE_SINGLE?.value,
                fillingInfoAssets: '999',
                fillingInfoProSe: '888',
                fillingInfoSurrenderIndicator: '',
                fillingInfoNonfilerIndicator: '',
                fillingInfoNonfilerCollectibleIndicator: '',
                primaryConsumerInformationIndicatorCode: '',
                primaryCreditBureauReportCode: '',
                secondaryConsumerInformationIndicatorCode: '',
                secondaryCreditBureauReportCode: ''
              }
            },
            assetsRefData: [{ description: 'AssetDataTest', value: '999' }],
            proSERefData: [{ description: 'ProSEDataTest', value: '888' }]
          }
        }
      }
    });

    jest.runAllTimers();

    expect(getBankruptcyInformationSpy).not.toBeCalled();
    getBankruptcyInformationSpy.mockClear();
  });

  it('get Bankruptcy Information not should be call with not correctly data', () => {
    jest.useFakeTimers();
    const getBankruptcyInformationSpy = bankruptcyActionsSpy(
      'getBankruptcyInformation'
    );
    renderMockStoreId(<Bankruptcy />, {
      initialState: {
        bankruptcy: {
          [storeId]: {
            viewData: {
              clerkCourtInfo: {
                clerkCourtInfoZip: '700'
              },
              fillingInfo: {
                bankruptcyType: BANKRUPTCY_TYPE_SINGLE?.value,
                fillingInfoAssets: '123',
                fillingInfoProSe: '456'
              }
            },
            assetsRefData: [{ description: 'AssetDataTest', value: '999' }],
            proSERefData: [{ description: 'ProSEDataTest', value: '888' }]
          }
        }
      }
    });

    jest.runAllTimers();

    expect(getBankruptcyInformationSpy).not.toBeCalled();
    getBankruptcyInformationSpy.mockClear();
  });
});
