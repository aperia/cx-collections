import { HorizontalTabs, Icon, Tooltip } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import React from 'react';
import Bankruptcy from './Bankruptcy';
import { BankruptcyResultTab } from '../types';
import BankruptcyDocuments from './BankruptcyDocuments';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';

const BankruptcyResult: React.FC = () => {
  const { t } = useTranslation();

  return (
    <HorizontalTabs>
      <HorizontalTabs.Tab
        title={
          <Tooltip
            element={t('txt_main_information')}
            placement="top"
            variant="primary"
          >
            <Icon name="file" size="6x" />
          </Tooltip>
        }
        key={BankruptcyResultTab.BankruptcyInfo}
        eventKey={BankruptcyResultTab.BankruptcyInfo}
      >
        <Bankruptcy />
      </HorizontalTabs.Tab>
      <HorizontalTabs.Tab
        title={
          <Tooltip
            element={t(I18N_COLLECTION_FORM.BANKRUPTCY_DOCUMENT)}
            placement="top"
            variant="primary"
          >
            <Icon name="attachment" size="6x" />
          </Tooltip>
        }
        key={BankruptcyResultTab.BankruptcyCertificate}
        eventKey={BankruptcyResultTab.BankruptcyCertificate}
      >
        <BankruptcyDocuments />
      </HorizontalTabs.Tab>
    </HorizontalTabs>
  );
};

export default BankruptcyResult;
