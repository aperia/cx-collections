import React, { useEffect } from 'react';
import classNames from 'classnames';
import { Button, Icon, Tooltip } from 'app/_libraries/_dls/components';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import DocumentList, { FileDetail } from 'app/components/FileList';

// constants
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// redux
import {
  selectBankruptcyDocumentLoading,
  selectBankruptcyDocumentFiles
} from '../_redux/selectors';
import { useDispatch } from 'react-redux';

// utils
import { genAmtId } from 'app/_libraries/_dls/utils';

const BankruptcyDocuments: React.FC = () => {
  const dataTestId = 'bankruptcyDocuments';
  const { t } = useTranslation();
  const { storeId, accEValue } = useAccountDetail();
  const dispatch = useDispatch();

  const loading = useStoreIdSelector<boolean>(selectBankruptcyDocumentLoading);
  const bankruptcyFiles = useStoreIdSelector<FileDetail[]>(
    selectBankruptcyDocumentFiles
  );

  const handleUpLoadFile = () => {
    dispatch(
      collectionFormActions.openUploadFileModal({
        storeId,
        type: 'bankruptcyDocument'
      })
    );
  };

  const handleDownloadFile = (file: FileDetail) => {
    dispatch(
      collectionFormActions.downloadCaseDocumentFile({
        storeId,
        postData: {
          fileName: file?.documentName,
          uuid: file?.documentObjectId
        },
        type: 'bankruptcyDocument'
      })
    );
  };

  useEffect(() => {
    dispatch(
      collectionFormActions.getListCaseDocument({
        storeId,
        type: 'bankruptcyDocument'
      })
    );
  }, [accEValue, dispatch, storeId]);

  return (
    <div className={classNames('dls-upload mt-16', { loading })}>
      <div className="d-flex justify-content-between align-items-center my-16">
        <p
          className=" fs-14 fw-500 color-grey"
          data-testid={genAmtId(dataTestId, 'title', '')}
        >
          {t(I18N_COLLECTION_FORM.BANKRUPTCY_DOCUMENT)}
        </p>
        <Tooltip
          element={t(I18N_COLLECTION_FORM.UPLOAD_BANKRUPTCY_TITLE)}
          placement="top"
          variant="primary"
          triggerClassName="mr-n4"
          dataTestId={genAmtId(dataTestId, 'upload-file', '')}
        >
          <Button
            variant="icon-secondary"
            onClick={handleUpLoadFile}
            dataTestId={genAmtId(dataTestId, 'upload-file-btn', '')}
          >
            <Icon name="add-file" />
          </Button>
        </Tooltip>
      </div>
      <DocumentList
        dataTestId={dataTestId}
        files={bankruptcyFiles}
        onDownload={handleDownloadFile}
      />
    </div>
  );
};

export default BankruptcyDocuments;
