import React from 'react';
import { screen } from '@testing-library/dom';
import { AccountDetailProvider } from 'app/hooks';
import { renderMockStoreId, storeId } from 'app/test-utils';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import BankruptcyResult from '.';

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    org: 'org',
    app: 'app',
    isCollector: true
  }
};

describe('Bankruptcy Results', () => {
  it('should render UI', () => {
    renderMockStoreId(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <BankruptcyResult />
      </AccountDetailProvider>,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              lastFollowUpData: {
                callResultType: CALL_RESULT_CODE.BANKRUPTCY,
                data: {
                  additionalAttorneyInfo: {},
                  clerkCourtInfo: {},
                  fillingInfo: {},
                  mainInfo: {
                    attorneyName: 'aaaaaaaaaaaaaaaa',
                    attorneyPhoneNumber: '1111111111',
                    case: '1',
                    chapter: '7'
                  },
                  trusteeInfo: {}
                }
              },
              uploadFiles: {}
            }
          }
        }
      }
    );

    expect(
      screen.queryByText('txt_bankruptcy_main_information')
    ).toBeInTheDocument();
  });
});
