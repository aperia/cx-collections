import React from 'react';
import { screen } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { AccountDetailProvider } from 'app/hooks';
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import BankruptcyDocuments from './BankruptcyDocuments';

const collectionFormActionsMock = mockActionCreator(collectionFormActions);

describe('Bankruptcy Documents', () => {
  const renderWrapper = () =>
    renderMockStoreId(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <BankruptcyDocuments />
      </AccountDetailProvider>,
      {
        initialState: {
          collectionForm: {
            [storeId]: {
              caseDocumentFiles: {
                bankruptcyDocument: {
                  files: [
                    {
                      documentName: 'searchAccount.png',
                      documentObjectId: '123-456-789'
                    }
                  ],
                  error: false,
                  loading: false
                }
              },
              uploadFiles: {}
            }
          }
        }
      }
    );

  it('should render UI', () => {
    renderWrapper();

    expect(
      screen.getByText(I18N_COLLECTION_FORM.BANKRUPTCY_DOCUMENT)
    ).toBeInTheDocument();
  });

  it('handleUpLoadFile', () => {
    const openUploadFileModal = collectionFormActionsMock(
      'openUploadFileModal'
    );
    const getListCaseDocument = collectionFormActionsMock(
      'getListCaseDocument'
    );
    const { wrapper } = renderWrapper();

    userEvent.click(queryByClass(wrapper.container, /icon icon-add-file/)!);
    expect(getListCaseDocument).toHaveBeenCalled();
    expect(openUploadFileModal).toHaveBeenCalled();
  });

  it('downloadCaseDocumentFile', () => {
    const downloadCaseDocumentFile = collectionFormActionsMock(
      'downloadCaseDocumentFile'
    );
    const getListCaseDocument = collectionFormActionsMock(
      'getListCaseDocument'
    );
    const { wrapper } = renderWrapper();

    userEvent.click(queryByClass(wrapper.container, /icon icon-download/)!);
    expect(getListCaseDocument).toHaveBeenCalled();
    expect(downloadCaseDocumentFile).toHaveBeenCalled();
  });
});
