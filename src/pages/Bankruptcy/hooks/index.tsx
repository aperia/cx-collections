import isObject from 'lodash.isobject';
import { useSelector } from 'react-redux';
import { getFormValues } from 'redux-form';

export const getFormValuesSelector =
  (formViewNames: string[]) => (state: RootState) => {
    return formViewNames.map(formViewName => {
      return getFormValues(formViewName)(state);
    });
  };

export const useIsDirtyForms = (
  formViewNames: string[],
  formData: MagicKeyValue,
  filerNameDropdown: RefDataValue[]
) => {
  let isDirty = false;
  const formValues = useSelector(getFormValuesSelector(formViewNames));

  formValues.forEach(formValue => {
    formValue &&
      Object.entries(formValue).forEach(([ky, value]) => {
        if (ky === 'jointFilerName') {
          value = filerNameDropdown.find(
            item => item.description === value
          )?.value;
        }
        if (
          isObject(value) &&
          (value as MagicKeyValue)?.value !== formData?.[ky]
        ) {
          isDirty = true;
        }
        if (!isObject(value) && value !== formData?.[ky]) {
          isDirty = true;
        }
      });
  });

  return isDirty;
};
