import { useIsDirtyForms } from './index';
import { renderHookWithStore } from 'app/test-utils';

describe('test useIsDirtyForms', () => {
  const mFormName = 'formViewName';
  const mformViewNames = [mFormName];
  const mformData: MagicKeyValue = { jointFilerName: 'john' };
  const mfilerNameDropdown: RefDataValue[] = [
    { value: 'value', description: 'description' }
  ];
  it('should be return false', () => {
    const { result } = renderHookWithStore(
      () => useIsDirtyForms(mformViewNames, mformData, mfilerNameDropdown),
      { initialState: {} }
    );
    expect(result.current).toEqual(false);
  });

  it('should be return true when ky equal jointFilerName', () => {
    const { result } = renderHookWithStore(
      () => useIsDirtyForms(mformViewNames, mformData, mfilerNameDropdown),
      {
        initialState: {
          form: {
            [mFormName]: {
              values: {
                jointFilerName: 'description',
                field2: { value: 'value', description: 'description' }
              },
              registeredFields: []
            }
          }
        }
      }
    );
    expect(result.current).toEqual(true);
  });
});
