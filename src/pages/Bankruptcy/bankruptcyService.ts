import { apiService } from 'app/utils/api.service';

// Types
import {
  GetBankruptcyRequestData,
  GetBankruptcyResponseData,
  RemoveBankruptcyStatusArg,
  UpdateBankruptcyRequestData
} from './types';

const bankruptcyService = {
  getBankruptcyInformation(data: GetBankruptcyRequestData) {
    const url =
      window.appConfig?.api?.collectionForm?.bankruptcyInformation || '';
    return apiService.post<GetBankruptcyResponseData>(url, data);
  },
  updateDetailForCallResultType(data: UpdateBankruptcyRequestData) {
    const url =
      window.appConfig?.api?.collectionForm?.bankruptcyUpdateWorkflow || '';
    return apiService.post(url, data);
  },
  removeBankruptcyStatus(data: RemoveBankruptcyStatusArg) {
    const url =
      window.appConfig?.api?.collectionForm?.bankruptcyInformationDelete || '';
    return apiService.post(url, data);
  },
  getCreditBureauCode() {
    const url =
      window.appConfig?.api?.collectionForm?.getCreditBureauCodeRefData || '';
    return apiService.get(url);
  },
  getAssets() {
    const url =
      window.appConfig?.api?.refData?.getAssets || '';
    return apiService.get(url);
  },
  getProSE() {
    const url =
      window.appConfig?.api?.refData?.getProSE || '';
    return apiService.get(url);
  }
};

export default bankruptcyService;
