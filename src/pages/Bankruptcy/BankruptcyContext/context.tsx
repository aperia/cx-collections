import React from 'react';
export interface BankruptcyContextProps {
  mainViewName?: string;
  attorneyViewName?: string;
  trusteeViewName?: string;
  clerkViewName?: string;
  formData?: MagicKeyValue;
}
const BankruptcyContext = React.createContext<BankruptcyContextProps>({});

export default BankruptcyContext;
