export enum BANKRUPTCY_VIEW {
  PENDING = 'bankruptcyStatusPendingMainInformationFormView',
  PENDING_ATTORNEY = 'bankruptcyStatusPendingAdditionalAttorneyInformationFormView',
  PENDING_TRUSTEE = 'bankruptcyStatusPendingAdditionalTrusteeInformationFormView',
  PENDING_CLERK = 'bankruptcyStatusPendingAdditionalClerkInformationFormView',
  BANKRUPTCY = 'bankruptcyStatusMainInformationFormView',
  MORE_INFO = 'bankruptcyStatusMoreFormView'
}

export enum TRIGGER_TYPE {
  CREATE = 'CREATE',
  UPDATE = 'UPDATE'
}

export const BANKRUPTCY_CONFIG = 'config/bankruptcy.json';

export const BANKRUPTCY_INFO_TYPE = 'bankruptcyType';
export const PRIMARY_CREDIT_BUREAU_REPORT_CODE =
  'primaryCreditBureauReportCode';
export const PRIMARY_CONSUMER_INFORMATION_INDICATOR_CODE =
  'primaryConsumerInformationIndicatorCode';
export const SECONDARY_CREDIT_BUREAU_REPORT_CODE =
  'secondaryCreditBureauReportCode';
export const SECONDARY_CUSTOMER_INFO_INDICATOR_CODE =
  'secondaryConsumerInformationIndicatorCode';
export const FILER_NAME = 'filerName';
export const JOINT_FILER_NAME = 'jointFilerName';
export const ORIGINAL_PLAN_AMOUNT = 'originalPlanAmount';
export const PLAN_DIFFERENCE_AMOUNT = 'planDifferenceAmount';
export const PLAN_PAYMENT_AMOUNT = 'planPaymentAmount';
export const BANKRUPTCY_FILE_DATE = 'bankruptcyFileDate';
export const BANKRUPTCY_CHAPTER_CODE = 'chapter';
export const ASSETS = 'fillingInfoAssets';
export const PROSE = 'fillingInfoProSe';

export const BANKRUPTCY_IGNORE_TRIM = [
  'bankruptcyAssetIndicator',
  'bankruptcyProSeIndicator',
  'voluntarySurrenderIndicator',
  'accountNonfilerIndicator',
  'accountNonfilerCollectibleIndicator'
];

export enum BANKRUPTCY_TYPE {
  BANKRUPTCY = 'BC',
  BANKRUPTCY_PENDING = 'BP',
  BANKRUPTCY_REMOVE = 'BR'
}

export const BANKRUPTCY_TYPE_SINGLE = {
  description: 'Single',
  value: 'Single'
};

export const BANKRUPTCY_TYPE_JOINT = {
  description: 'Joint',
  value: 'Joint'
};

export const INDIVIDUAL_CODE = '1';
export const JOINT_CONTRACTUAL_LIABILITY_CODE = '2';
export const AUTHORIZED_USER_CODE = '3';

export const CHAPTER_13 = '13';

export const BANKRUPTCY_COMPLETE = 'COMPLETE';
export const BANKRUPTCY_IN_PROGRESS = 'IN_PROGRESS';
