import React from 'react';
import { useDispatch } from 'react-redux';

// Redux
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';
import { I18N_TXN_ADJ } from './constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

const TransactionAdjustment: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  return (
    <span
      className="link-header"
      onClick={() => {
        dispatch(
          actionsTab.addTab({
            id: 'transaction-adjustment-request',
            title: I18N_TXN_ADJ.TRANSACTION_ADJUSTMENT_REQUEST,
            storeId: 'transaction-adjustment-request',
            tabType: 'transactionAdjustment',
            iconName: 'file'
          })
        );
      }}
      data-testid={genAmtId(
        'header_transaction-adjustment-request',
        'title',
        ''
      )}
    >
      {t(I18N_TXN_ADJ.TRANSACTION_ADJUSTMENT_REQUEST)}
    </span>
  );
};

export default TransactionAdjustment;
