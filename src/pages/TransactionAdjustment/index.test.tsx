import React from 'react';
import { screen } from '@testing-library/react';

import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';
import TransactionAdjustment from './index';

import { I18N_TXN_ADJ } from './constants';

import {
  mockActionCreator,
  renderMockStore,
  storeId,
  accEValue
} from 'app/test-utils';
import { AccountDetailProvider } from 'app/hooks';

jest.mock('react-redux', () => {
  const original = jest.requireActual('react-redux');
  return {
    ...original,
    useDispatch: () => (x: Function) => jest.fn().mockImplementation(() => x())
  };
});

const generateWrapper = () => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <TransactionAdjustment />
    </AccountDetailProvider>,
    { initialState: {} }
  );
};

const tabSpy = mockActionCreator(actionsTab);

describe('Transaction Adjustment', () => {
  it('render correctly', () => {
    global.window = Object.create(window);
    Object.defineProperty(window, 'location', {
      value: { pathname: '/supervisor/vi', hash: '/#/supervisor' }
    });

    const mockAction = tabSpy('addTab');

    generateWrapper();

    const link = screen.getByText(I18N_TXN_ADJ.TRANSACTION_ADJUSTMENT_REQUEST);
    link.click();

    expect(mockAction).toBeCalledWith(
      expect.objectContaining({
        id: 'transaction-adjustment-request',
        title: I18N_TXN_ADJ.TRANSACTION_ADJUSTMENT_REQUEST,
        storeId: 'transaction-adjustment-request',
        tabType: 'transactionAdjustment',
        iconName: 'file'
      })
    );
  });
});
