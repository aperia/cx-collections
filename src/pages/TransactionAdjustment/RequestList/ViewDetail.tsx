import React from 'react';
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

// components
import AdjustmentBody from './Body';

// i18n
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// constant
import { I18N_TXN_ADJ } from '../constants';
import { TRANSACTION_STATUS } from './constants';

// Hook
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { AccountDetailProvider, CustomStoreIdProvider } from 'app/hooks';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { transactionAdjustmentAction } from './_redux/reducer';
import {
  takeCurrentTransactionAdjustmentRequest,
  takeDataAdjustmentDetail,
  takeOpenModalStatus,
  takeViewLoadingStatus
} from './_redux/selector';

export interface ViewAdjustmentDetailProps {}
const ViewAdjustmentDetail: React.FC<ViewAdjustmentDetailProps> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const status = useSelector(takeCurrentTransactionAdjustmentRequest);
  const show = useSelector(takeOpenModalStatus);
  const data = useSelector(takeDataAdjustmentDetail);
  const isLoading = useSelector(takeViewLoadingStatus);
  const { isAdminRole: isAdmin, isSupervisor } = window.appConfig?.commonConfig;
  const hasReviewPermission = isSupervisor || isAdmin;
  const storeId = data?.accNbr?.eValue || '';
  const maskedValue = data?.accNbr?.maskedValue ?? '';
  const testId = 'transaction-adjustment-request_detail-modal';

  const isViewOnly =
    status !== TRANSACTION_STATUS.PENDING || !hasReviewPermission;

  const handleAdjustment = (type: string) => {
    const {
      operatorId = '',
      managerIdentifier = '',
      supervisorIdentifier = '',
      date = '',
      time = '',
      amountTransaction = '',
      amount = '',
      statusCode = ''
    } = data || {};

    dispatch(
      transactionAdjustmentAction.triggerAdjustment({
        storeId,
        payload: {
          common: {
            accountId: storeId
          },
          operatorIdentifier: operatorId,
          managerIdentifier,
          supervisorIdentifier,
          entryDate: date,
          entryTime: time,
          amount: amountTransaction,
          newAmount: amount,
          statusCode
        },
        maskedValue,
        type
      })
    );
  };

  const handleCloseModal = () => {
    dispatch(
      transactionAdjustmentAction.updateOpenModal({
        data: {},
        openModal: false
      })
    );
  };

  return (
    <Modal
      className="modal-review-adjustment"
      lg
      show={show}
      loading={isLoading}
      enforceFocus={false}
      dataTestId={testId}
    >
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId={`${testId}_header`}
      >
        <ModalTitle dataTestId={`${testId}_header-title`}>
          {t(
            !isViewOnly
              ? I18N_TXN_ADJ.REVIEW_ADJUSTMENT
              : I18N_TXN_ADJ.TRANSACTION_ADJUSTMENT_DETAIL
          )}
        </ModalTitle>
      </ModalHeader>
      <ModalBody noPadding>
        <AccountDetailProvider
          value={{
            accEValue: storeId,
            storeId
          }}
        >
          <CustomStoreIdProvider
            value={{
              customStoreId: storeId
            }}
          >
            <AdjustmentBody />
          </CustomStoreIdProvider>
        </AccountDetailProvider>
      </ModalBody>
      <ModalFooter border>
        <Button
          variant="secondary"
          onClick={handleCloseModal}
          dataTestId={`${testId}_footer_close-btn`}
        >
          {isViewOnly ? t(I18N_COMMON_TEXT.CLOSE) : t(I18N_COMMON_TEXT.CANCEL)}
        </Button>
        {isViewOnly ? null : (
          <>
            <Button
              variant="danger"
              onClick={() => handleAdjustment(TRANSACTION_STATUS.REJECTED)}
              dataTestId={`${testId}_footer_reject-btn`}
            >
              {t(I18N_TXN_ADJ.REJECT)}
            </Button>
            <Button
              variant="primary"
              onClick={() => handleAdjustment(TRANSACTION_STATUS.APPROVED)}
              dataTestId={`${testId}_footer_approve-btn`}
            >
              {t(I18N_TXN_ADJ.APPROVE)}
            </Button>
          </>
        )}
      </ModalFooter>
    </Modal>
  );
};

export default ViewAdjustmentDetail;
