export const haveDot = (value: string) => value?.indexOf('.') >= 0;

export const formatAmount = (amount: string) => {
  const newAmount = amount?.trim();

  if (haveDot(newAmount) || !newAmount) return newAmount;

  const intPartAmount = newAmount.substr(0, newAmount.length - 2);
  const decimalPartAmount = newAmount.substr(-2);

  return intPartAmount + '.' + decimalPartAmount;
};
