import {
  ApproveAdjustmentRequestArg,
  GetTransactionAdjustmentRequestArg,
  RejectAdjustmentRequestArg
} from './types';
import { apiService } from 'app/utils/api.service';
import { DEFAULT_PAGE_SIZE, TRANSACTION_STATUS } from './constants';

class TransactionAdjustmentRequestServices {
  getTransactionAdjustmentRequest(
    postData: GetTransactionAdjustmentRequestArg
  ) {
    const { type, startSequence, endSequence } = postData;
    let url = '';

    switch (type) {
      case TRANSACTION_STATUS.PENDING:
        url =
          window.appConfig?.api?.transactionAdjustmentRequest?.pendingQueue ||
          '';
        break;
      case TRANSACTION_STATUS.APPROVED:
        url =
          window.appConfig?.api?.transactionAdjustmentRequest?.approvedQueue ||
          '';
        break;
      case TRANSACTION_STATUS.REJECTED:
        url =
          window.appConfig?.api?.transactionAdjustmentRequest?.rejectedQueue ||
          '';
        break;
    }

    return apiService.post(url, {
      selectFields: ['*'],
      startSequence: startSequence ? startSequence.toString() : '1',
      endSequence: endSequence
        ? endSequence.toString()
        : DEFAULT_PAGE_SIZE.toString()
    });
  }

  approveAdjustment(body: ApproveAdjustmentRequestArg) {
    const url =
      window.appConfig?.api?.transactionAdjustmentRequest?.approveAdjustment ||
      '';

    return apiService.post(url, body);
  }

  rejectAdjustment(body: RejectAdjustmentRequestArg) {
    const url =
      window.appConfig?.api?.transactionAdjustmentRequest?.rejectAdjustment ||
      '';

    return apiService.post(url, body);
  }
}

export const transactionAdjustmentRequestServices =
  new TransactionAdjustmentRequestServices();
