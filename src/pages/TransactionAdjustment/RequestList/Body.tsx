import React, { useEffect, useLayoutEffect, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Badge, SimpleBar } from 'app/_libraries/_dls/components';
import { Field } from 'redux-form';
import { ExtraFieldProps, View } from 'app/_libraries/_dof/core';

// Hook
import {
  MemoContext,
  useAccountDetail,
  useCustomStoreIdSelector,
  useStoreIdSelector
} from 'app/hooks';
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';

// components
import Memo from 'pages/Memos';

// Const & Type
import { I18N_TXN_ADJ } from '../constants';
import {
  transactionStatus,
  TRANSACTION_STATUS,
  viewAdjustmentAddMemoChronicle,
  viewAdjustmentAddMemoCIS,
  viewAdjustmentName,
  viewOriginalName
} from './constants';
import { MemoCISType } from 'pages/Memos/CIS/constants';
import { MemoType } from 'pages/Memos/types';

// Helper
import { capitalizeWord } from 'app/helpers';

// Redux
import { takeMemoType } from 'pages/Memos/_redux/selectors';
import { transactionAdjustmentAction } from './_redux/reducer';
import {
  takeCurrentTransactionAdjustmentRequest,
  takeDataAdjustmentDetail
} from './_redux/selector';
import { selectTypeRefData } from 'pages/Memos/CIS/_redux/selectors';
import { cisMemoActions } from 'pages/Memos/CIS/_redux/reducers';
import { genAmtId } from 'app/_libraries/_dls/utils';

const AdjustmentBody: React.FC = () => {
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();
  const divRef = useRef<HTMLDivElement | null>(null);

  const viewRef = useRef<any>(null);
  const [viewAddMemoName, setMemoViewName] = useState<string>();

  const status = useSelector(takeCurrentTransactionAdjustmentRequest);
  const data = useSelector(takeDataAdjustmentDetail);
  const memoType = useStoreIdSelector<string>(takeMemoType);
  const typeRefData =
    useCustomStoreIdSelector<Record<string, string>[]>(selectTypeRefData);
  const { isAdminRole: isAdmin, isSupervisor } = window.appConfig?.commonConfig;
  const hasReviewPermission = isSupervisor || isAdmin;

  const viewOriginal = `${viewOriginalName}_${status}`;
  const viewAdjustment = `${viewAdjustmentName}_${status}`;
  const testId = 'transaction-adjustment-request_detail-modal-body';

  const defaultType = typeRefData?.find(
    item => item.value === MemoCISType.Standard
  );

  useEffect(() => {
    const memoName =
      memoType === MemoType.CHRONICLE
        ? viewAdjustmentAddMemoChronicle
        : viewAdjustmentAddMemoCIS;
    dispatch(
      transactionAdjustmentAction.setViewName({
        viewName: `${storeId}-${memoName}`
      })
    );

    setMemoViewName(memoName);
  }, [dispatch, storeId, memoType]);

  useEffect(() => {
    const immediateId = setImmediate(() => {
      const memoTypeField: Field<ExtraFieldProps> =
        viewRef.current?.props.onFind('memoType');

      memoTypeField?.props?.props.setData(typeRefData);

      if (defaultType) {
        memoTypeField?.props?.props.setValue(defaultType);
      }
    });

    return () => {
      clearImmediate(immediateId);
    };
  }, [defaultType, memoType, viewAddMemoName, typeRefData]);

  useEffect(() => {
    dispatch(
      cisMemoActions.getMemoCISRefData({
        storeId,
        postData: { accountId: storeId }
      })
    );
  }, [dispatch, storeId]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.width = '408px');
  }, []);

  return (
    <div className="d-flex flex-row">
      <div className="adjustment-content">
        <SimpleBar>
          <div className="bg-light-l20 border-bottom p-24">
            <h5 data-testid={genAmtId(testId, 'original-title', '')}>
              {t(I18N_TXN_ADJ.ORIGINAL)}
            </h5>
            <View
              id={viewOriginal}
              formKey={viewOriginal}
              descriptor={viewOriginalName}
              value={data}
            />
          </div>
          <div className="p-24">
            <div className="d-flex align-items-center">
              <h5
                className="mr-8"
                data-testid={genAmtId(testId, 'adjustment-title', '')}
              >
                {t(I18N_TXN_ADJ.ADJUSTMENT)}
              </h5>
              <Badge
                color={transactionStatus[status].color}
                dataTestId={`${testId}_transaction-status`}
              >
                {capitalizeWord(t(transactionStatus[status].title))}
              </Badge>
            </div>
            <View
              id={viewAdjustment}
              formKey={viewAdjustment}
              descriptor={viewAdjustmentName}
              value={data}
            />
            {hasReviewPermission &&
            status === TRANSACTION_STATUS.PENDING &&
            memoType?.length ? (
              <div className="pt-24">
                <h5 data-testid={genAmtId(testId, 'comment-title', '')}>
                  {t(I18N_TXN_ADJ.COMMENT)}
                </h5>
                <p
                  className="pt-16"
                  data-testid={genAmtId(testId, 'sub-comment-title', '')}
                >
                  {memoType === MemoType.CHRONICLE
                    ? t(I18N_TXN_ADJ.SUB_COMMENT_CHRONICLE)
                    : t(I18N_TXN_ADJ.SUB_COMMENT_CIS)}
                </p>
                <View
                  id={`${storeId}-${viewAddMemoName}`}
                  formKey={`${storeId}-${viewAddMemoName}`}
                  descriptor={`${viewAddMemoName}`}
                  ref={viewRef}
                />
              </div>
            ) : null}
          </div>
        </SimpleBar>
      </div>
      <div className="border-left" ref={divRef}>
        <MemoContext.Provider
          value={{
            isHiddenAddSection: true
          }}
        >
          <div className="adjustment-memos">
            <SimpleBar>
              <Memo shouldRenderPinnedIcon={false} />
            </SimpleBar>
          </div>
        </MemoContext.Provider>
      </div>
    </div>
  );
};

export default AdjustmentBody;
