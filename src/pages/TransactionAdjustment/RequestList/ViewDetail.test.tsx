import React from 'react';
import { screen } from '@testing-library/react';

import ViewDetail from './ViewDetail';

import { I18N_TXN_ADJ } from '../constants';

import { mockActionCreator, storeId, renderMockStoreId } from 'app/test-utils';
import { TRANSACTION_STATUS } from './constants';
import { MemoCISType } from 'pages/Memos/CIS/constants';

import { cisMemoActions } from 'pages/Memos/CIS/_redux/reducers';
import { transactionAdjustmentAction } from './_redux/reducer';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { MemoType } from 'pages/Memos/types';
import { IInitialStateMemoCIS } from 'pages/Memos/CIS/types';
import { TransactionAdjustmentRequest } from './types';

jest.mock('pages/Memos', () => () => <div />);
jest.mock('./Body', () => () => <div />);

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const generateState = (currentRequest = TRANSACTION_STATUS.PENDING) => {
  const initialState: Partial<RootState> = {
    memo: {
      [storeId]: {
        configMemo: { data: { type: MemoType.CHRONICLE } }
      }
    },
    cisMemo: {
      [storeId]: {
        memoCISRefData: {
          typeRefData: [
            { value: MemoCISType.Standard, description: 'description' },
            { value: MemoCISType.Permanent, description: 'description' },
            { value: MemoCISType.Priority, description: 'description' },
            { value: MemoCISType.PriorityPermanent, description: 'description' }
          ]
        }
      }
    } as unknown as IInitialStateMemoCIS,
    transactionAdjustment: {
      currentRequest,
      openModal: true,
      adjustmentDetail: { id: 'id', accNbr: { eValue: storeId } },
      isError: false,
      isLoading: false,
      isLoadingView: false,
      sortBy: { id: '' },
      transactionAdjustmentRequestList: []
    }
  };

  return initialState;
};

const generateWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<ViewDetail />, { initialState });
};

mockActionCreator(cisMemoActions);
const transactionAdjustmentActionSpy = mockActionCreator(
  transactionAdjustmentAction
);

describe('Render', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isAdminRole: true
    }
  };
  it('render UI > empty state', () => {
    generateWrapper({
      transactionAdjustment: {
        openModal: true
      } as unknown as TransactionAdjustmentRequest
    });

    expect(
      screen.getByText(I18N_TXN_ADJ.TRANSACTION_ADJUSTMENT_DETAIL)
    ).toBeInTheDocument();
  });

  it('Render UI > status pending', () => {
    global.window = Object.create(window);
    Object.defineProperty(window, 'location', {
      value: { pathname: '/supervisor/vi', hash: '/#/supervisor/hasPermission' }
    });
    generateWrapper(generateState(TRANSACTION_STATUS.PENDING));

    expect(
      screen.getByText(I18N_TXN_ADJ.REVIEW_ADJUSTMENT)
    ).toBeInTheDocument();
  });

  it('Render UI > status rejected', () => {
    generateWrapper(generateState(TRANSACTION_STATUS.REJECTED));

    expect(
      screen.getByText(I18N_TXN_ADJ.TRANSACTION_ADJUSTMENT_DETAIL)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleCloseModal', () => {
    const mockAction = transactionAdjustmentActionSpy('updateOpenModal');

    generateWrapper(generateState());

    screen.getByText(I18N_COMMON_TEXT.CANCEL).click();

    expect(mockAction).toBeCalledWith({ data: {}, openModal: false });
  });

  describe('handleAdjustment', () => {
    it('rejected', () => {
      const mockAction = transactionAdjustmentActionSpy('triggerAdjustment');

      generateWrapper(generateState());

      screen.getByText(I18N_TXN_ADJ.REJECT).click();

      expect(mockAction).toBeCalledWith({
        maskedValue: '',
        payload: {
          amount: '',
          common: {
            accountId: '123456789'
          },
          entryDate: '',
          entryTime: '',
          managerIdentifier: '',
          newAmount: '',
          operatorIdentifier: '',
          statusCode: '',
          supervisorIdentifier: ''
        },
        storeId,
        type: TRANSACTION_STATUS.REJECTED
      });
    });
  });
  it('approve', () => {
    const mockAction = transactionAdjustmentActionSpy('triggerAdjustment');

    generateWrapper(generateState());

    screen.getByText(I18N_TXN_ADJ.APPROVE).click();

    expect(mockAction).toBeCalledWith({
      maskedValue: '',
      payload: {
        amount: '',
        common: {
          accountId: '123456789'
        },
        entryDate: '',
        entryTime: '',
        managerIdentifier: '',
        newAmount: '',
        operatorIdentifier: '',
        statusCode: '',
        supervisorIdentifier: ''
      },
      storeId,
      type: TRANSACTION_STATUS.APPROVED
    });
  });

  it('approve with item detail empty', () => {
    const mockAction = transactionAdjustmentActionSpy('triggerAdjustment');

    generateWrapper({
      memo: {
        [storeId]: {
          configMemo: { data: { type: MemoType.CHRONICLE } }
        }
      },
      cisMemo: {
        [storeId]: {
          memoCISRefData: {
            typeRefData: [
              { value: MemoCISType.Standard, description: 'description' },
              { value: MemoCISType.Permanent, description: 'description' },
              { value: MemoCISType.Priority, description: 'description' },
              {
                value: MemoCISType.PriorityPermanent,
                description: 'description'
              }
            ]
          }
        }
      } as unknown as IInitialStateMemoCIS,
      transactionAdjustment: {
        currentRequest: TRANSACTION_STATUS.PENDING,
        openModal: true,
        adjustmentDetail: undefined,
        isError: false,
        isLoading: false,
        isLoadingView: false,
        sortBy: { id: '' },
        transactionAdjustmentRequestList: []
      }
    });

    screen.getByText(I18N_TXN_ADJ.APPROVE).click();

    expect(mockAction).toBeCalledWith({
      maskedValue: '',
      payload: {
        amount: '',
        common: {
          accountId: ''
        },
        entryDate: '',
        entryTime: '',
        managerIdentifier: '',
        newAmount: '',
        operatorIdentifier: '',
        statusCode: '',
        supervisorIdentifier: ''
      },
      storeId: '',
      type: TRANSACTION_STATUS.APPROVED
    });
  });
});
