import React from 'react';
import { screen } from '@testing-library/react';

import Body from './Body';

import { I18N_TXN_ADJ } from '../constants';

import { mockActionCreator, storeId, renderMockStoreId } from 'app/test-utils';
import { CustomStoreIdProvider } from 'app/hooks';
import { MemoType } from 'pages/Memos/types';
import {
  TRANSACTION_STATUS,
  viewAdjustmentAddMemoChronicle,
  viewAdjustmentAddMemoCIS
} from './constants';
import { MemoCISType } from 'pages/Memos/CIS/constants';

import { cisMemoActions } from 'pages/Memos/CIS/_redux/reducers';
import { transactionAdjustmentAction } from './_redux/reducer';

jest.mock('pages/Memos', () => () => <div />);

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const generateState = (
  type = MemoType.CHRONICLE,
  currentRequest = TRANSACTION_STATUS.PENDING
) => {
  const initialState: Partial<RootState> = {
    memo: {
      [storeId]: {
        configMemo: { data: { type } }
      }
    },
    cisMemo: {
      [storeId]: {
        memoCISRefData: {
          typeRefData: [
            { value: MemoCISType.Standard },
            { value: MemoCISType.Permanent },
            { value: MemoCISType.Priority },
            { value: MemoCISType.PriorityPermanent }
          ]
        }
      }
    },
    transactionAdjustment: {
      currentRequest
    }
  } as MagicKeyValue;

  return initialState;
};

const generateWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(
    <CustomStoreIdProvider value={{ customStoreId: storeId }}>
      <Body />
    </CustomStoreIdProvider>,
    { initialState }
  );
};

const cisMemoSpy = mockActionCreator(cisMemoActions);
const transactionAdjustmentActionSpy = mockActionCreator(
  transactionAdjustmentAction
);

describe('Render', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isAdminRole: true
    }
  };
  beforeEach(() => {
    cisMemoSpy('getMemoCISRefData');
  });

  it('Render UI > empty state', () => {
    const mockGetMemoCISRefData = cisMemoSpy('getMemoCISRefData');
    const mockSetViewName = transactionAdjustmentActionSpy('setViewName');

    jest.useFakeTimers();

    generateWrapper({});

    jest.runAllTimers();

    expect(screen.queryByText(I18N_TXN_ADJ.SUB_COMMENT_CHRONICLE)).toBeNull();
    expect(mockGetMemoCISRefData).toBeCalledWith({
      storeId,
      postData: { accountId: storeId }
    });
    expect(mockSetViewName).toBeCalledWith({
      viewName: `${storeId}-${viewAdjustmentAddMemoCIS}`
    });
  });

  it('Render UI > CHRONICLE', () => {
    const mockGetMemoCISRefData = cisMemoSpy('getMemoCISRefData');
    const mockSetViewName = transactionAdjustmentActionSpy('setViewName');

    jest.useFakeTimers();

    generateWrapper(generateState());

    jest.runAllTimers();

    expect(
      screen.queryByText(I18N_TXN_ADJ.SUB_COMMENT_CHRONICLE)
    ).toBeInTheDocument();
    expect(mockGetMemoCISRefData).toBeCalledWith({
      storeId,
      postData: { accountId: storeId }
    });
    expect(mockSetViewName).toBeCalledWith({
      viewName: `${storeId}-${viewAdjustmentAddMemoChronicle}`
    });
  });

  describe('Render UI > CIS', () => {
    it('Pending', () => {
      const mockGetMemoCISRefData = cisMemoSpy('getMemoCISRefData');
      const mockSetViewName = transactionAdjustmentActionSpy('setViewName');

      jest.useFakeTimers();

      generateWrapper(generateState(MemoType.CIS));

      jest.runAllTimers();

      expect(
        screen.queryByText(I18N_TXN_ADJ.SUB_COMMENT_CIS)
      ).toBeInTheDocument();
      expect(mockGetMemoCISRefData).toBeCalledWith({
        storeId,
        postData: { accountId: storeId }
      });
      expect(mockSetViewName).toBeCalledWith({
        viewName: `${storeId}-${viewAdjustmentAddMemoCIS}`
      });
    });

    it('Rejected', () => {
      const mockGetMemoCISRefData = cisMemoSpy('getMemoCISRefData');
      const mockSetViewName = transactionAdjustmentActionSpy('setViewName');

      jest.useFakeTimers();

      generateWrapper(generateState(MemoType.CIS, TRANSACTION_STATUS.REJECTED));

      jest.runAllTimers();

      expect(screen.queryByText(I18N_TXN_ADJ.SUB_COMMENT_CHRONICLE)).toBeNull();
      expect(screen.queryByText(I18N_TXN_ADJ.SUB_COMMENT_CIS)).toBeNull();
      expect(screen.queryByText(I18N_TXN_ADJ.ORIGINAL)).toBeInTheDocument();
      expect(mockGetMemoCISRefData).toBeCalledWith({
        storeId,
        postData: { accountId: storeId }
      });
      expect(mockSetViewName).toBeCalledWith({
        viewName: `${storeId}-${viewAdjustmentAddMemoCIS}`
      });
    });
  });
});
