import { I18N_TXN_ADJ } from '../constants';
import { AdjustmentStatus } from './types';

export type Many<T> = T | ReadonlyArray<T>;

export const DEFAULT_PAGE_SIZE = 10;

export enum TRANSACTION_STATUS {
  PENDING = 'pending',
  APPROVED = 'approved',
  REJECTED = 'rejected'
}

export const transactionStatus: AdjustmentStatus = {
  [TRANSACTION_STATUS.PENDING]: {
    title: I18N_TXN_ADJ.PENDING,
    headerLabel: I18N_TXN_ADJ.OPERATION_DATE_TIME,
    color: 'orange'
  },
  [TRANSACTION_STATUS.APPROVED]: {
    title: I18N_TXN_ADJ.APPROVED,
    headerLabel: I18N_TXN_ADJ.APPROVED_DATE_TIME,
    color: 'green'
  },
  [TRANSACTION_STATUS.REJECTED]: {
    title: I18N_TXN_ADJ.REJECTED,
    headerLabel: I18N_TXN_ADJ.REJECTED_DATE_TIME,
    color: 'red'
  }
};

export enum GRID_COLUMN_NAME {
  OPERATOR_ID = 'operatorId',
  OPERATION_DATE_TIME = 'operatorDateTime',
  ACC_NBR = 'accNbr',
  BATCH = 'batch',
  TRANSACTION_TYPE = 'transactionType',
  AMOUNT = 'amount'
}

export const defaultOrderDateTime = ['date', 'time'];
export const defaultSort: Many<boolean | 'asc' | 'desc'> = ['desc', 'desc'];
export const defaultChronicleType = 'X'; //api not support behavior type CXCOLL

export const viewOriginalName = 'adjustmentDetailOriginal';
export const viewAdjustmentName = 'adjustmentDetailAdjustment';
export const viewAdjustmentAddMemoCIS = 'addMemoAdjustmentCIS';
export const viewAdjustmentAddMemoChronicle = 'addMemoAdjustmentChronicle';
