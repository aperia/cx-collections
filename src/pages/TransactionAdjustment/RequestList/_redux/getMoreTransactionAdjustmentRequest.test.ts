import './reducer';

import { createStore, Store } from '@reduxjs/toolkit';

import { getMoreTransactionAdjustmentRequest } from './getMoreTransactionAdjustmentRequest';

import { transactionAdjustmentRequestServices } from '../requestListService';

//redux
import { rootReducer } from 'storeConfig';

import { responseDefault } from 'app/test-utils';
import { GetTransactionAdjustmentRequestArg } from '../types';

const mockArgs: GetTransactionAdjustmentRequestArg = { type: 'type' };

let store: Store<RootState>;

beforeEach(() => {
  store = createStore(rootReducer, {});
});

describe('getMoreTransactionAdjustmentRequest', () => {
  it('success with empty data', async () => {
    jest
      .spyOn(
        transactionAdjustmentRequestServices,
        'getTransactionAdjustmentRequest'
      )
      .mockResolvedValue({ ...responseDefault, data: {} });

    const response = await getMoreTransactionAdjustmentRequest(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      data: undefined,
      endSequence: NaN,
      startSequence: NaN,
      type: 'pending'
    });
  });

  it('success with data', async () => {
    const data = {
      pending: [
        { id: 'id_1' },
        { id: 'id_2' },
        { id: 'id_3' },
        { id: 'id_4' },
        { id: 'id_5' },
        { id: 'id_6' },
        { id: 'id_7' },
        { id: 'id_8' },
        { id: 'id_9' },
        { id: 'id_10' }
      ],
      adjustmentReviewQueueList: [{ id: 'mock' }]
    };

    jest
      .spyOn(
        transactionAdjustmentRequestServices,
        'getTransactionAdjustmentRequest'
      )
      .mockResolvedValue({ ...responseDefault, data });

    const response = (await getMoreTransactionAdjustmentRequest(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    )) as MagicKeyValue;

    expect(response.payload.data.length).toEqual(1);
  });

  it('success with data with load more', async () => {
    const data = {
      pending: [
        { id: 'id_1' },
        { id: 'id_2' },
        { id: 'id_3' },
        { id: 'id_4' },
        { id: 'id_5' },
        { id: 'id_6' },
        { id: 'id_7' },
        { id: 'id_8' },
        { id: 'id_9' },
        { id: 'id_10' }
      ],
      adjustmentReviewQueueList: [
        {
          id: 'mock'
        },
        {
          id: 'mock1'
        },
        {
          id: 'mock2'
        },
        {
          id: 'mock3'
        },
        {
          id: 'mock4'
        },
        {
          id: 'mock7'
        },
        {
          id: 'mock8'
        },
        {
          id: 'mock9'
        },
        {
          id: 'mock10'
        },
        {
          id: 'mock11'
        },
        {
          id: 'mock12'
        }
      ]
    };

    jest
      .spyOn(
        transactionAdjustmentRequestServices,
        'getTransactionAdjustmentRequest'
      )
      .mockResolvedValue({ ...responseDefault, data });

    const response = (await getMoreTransactionAdjustmentRequest(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    )) as MagicKeyValue;

    expect(response.payload.data.length).toEqual(11);
  });

  it('error', async () => {
    jest
      .spyOn(
        transactionAdjustmentRequestServices,
        'getTransactionAdjustmentRequest'
      )
      .mockRejectedValue({
        status: 400,
        data: {
          errors: {
            message: 'RECORD NOT FOUND'
          }
        }
      });

    const response = (await getMoreTransactionAdjustmentRequest(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    )) as MagicKeyValue;

    expect(response.payload).toEqual({
      data: { errors: { message: 'RECORD NOT FOUND' } },
      status: 400
    });
  });

  it('error 455', async () => {
    jest
      .spyOn(
        transactionAdjustmentRequestServices,
        'getTransactionAdjustmentRequest'
      )
      .mockRejectedValue({
        response: { status: 455 },
        data: {
          message: 'NO TRANSACTION ADJUSTMENT REQUEST'
        }
      });

    const response = (await getMoreTransactionAdjustmentRequest(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    )) as MagicKeyValue;

    expect(response.payload).toEqual({
      data: [],
      endSequence: NaN,
      startSequence: NaN,
      type: 'pending'
    });
  });
});
