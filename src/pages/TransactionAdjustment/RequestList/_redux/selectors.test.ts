import * as selector from './selector';

// utils
import { selectorWrapper } from 'app/test-utils';
import { SORT_TYPE } from 'app/constants';

const states: Partial<RootState> = {
  transactionAdjustment: {
    currentRequest: 'POST',
    isError: true,
    isLoading: true,
    isLoadingView: true,
    openModal: true,
    sortBy: { id: 'id', order: SORT_TYPE.DESC },
    adjustmentDetail: {},
    viewName: 'viewName',
    transactionAdjustmentRequestList: [],
    hasLoadMore: true,
    isLoadMoreLoading: false
  },
  form: {
    viewName: {
      registeredFields: [],
      values: ['values']
    }
  }
};

const selectorTrigger = selectorWrapper(states);

describe('HardShip > selector', () => {
  it('takeCurrentTransactionAdjustmentRequest', () => {
    const { data, emptyData } = selectorTrigger(
      selector.takeCurrentTransactionAdjustmentRequest
    );

    expect(data).toEqual(states.transactionAdjustment!.currentRequest);
    expect(emptyData).toBeUndefined();
  });

  it('selectErrorTransactionAdjustmentRequest', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectErrorTransactionAdjustmentRequest
    );

    expect(data).toEqual(states.transactionAdjustment!.isError);
    expect(emptyData).toBeUndefined();
  });

  it('takeLoadingTransactionAdjustmentRequest', () => {
    const { data, emptyData } = selectorTrigger(
      selector.takeLoadingTransactionAdjustmentRequest
    );

    expect(data).toEqual(states.transactionAdjustment!.isLoading);
    expect(emptyData).toBeUndefined();
  });

  it('selectTransactionAdjustmentRequestList', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectTransactionAdjustmentRequestList
    );

    expect(data).toEqual(
      states.transactionAdjustment!.transactionAdjustmentRequestList
    );
    expect(emptyData).toBeUndefined();
  });

  it('selectPendingSortTransactionAdjustmentRequest', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectPendingSortTransactionAdjustmentRequest
    );

    expect(data).toEqual(states.transactionAdjustment!.sortBy);
    expect(emptyData).toBeUndefined();
  });

  it('takeOpenModalStatus', () => {
    const { data, emptyData } = selectorTrigger(selector.takeOpenModalStatus);

    expect(data).toEqual(states.transactionAdjustment!.openModal);
    expect(emptyData).toBeUndefined();
  });

  it('takeDataAdjustmentDetail', () => {
    const { data, emptyData } = selectorTrigger(
      selector.takeDataAdjustmentDetail
    );

    expect(data).toEqual(states.transactionAdjustment!.adjustmentDetail);
    expect(emptyData).toBeUndefined();
  });

  it('takeViewLoadingStatus', () => {
    const { data, emptyData } = selectorTrigger(selector.takeViewLoadingStatus);

    expect(data).toEqual(states.transactionAdjustment!.isLoadingView);
    expect(emptyData).toBeUndefined();
  });

  it('takeAddFormValue', () => {
    const { data, emptyData } = selectorTrigger(selector.takeAddFormValue);

    expect(data).toEqual(states.form!.viewName.values);
    expect(emptyData).toEqual({});
  });

  it('takeHasLoadMore', () => {
    const { data, emptyData } = selectorTrigger(selector.takeHasLoadMore);

    expect(data).toEqual(states.transactionAdjustment!.hasLoadMore);
    expect(emptyData).toBeUndefined();
  });

  it('takeIsLoadMoreLoading ', () => {
    const { data, emptyData } = selectorTrigger(selector.takeIsLoadMoreLoading);

    expect(data).toEqual(states.transactionAdjustment!.isLoadMoreLoading);
    expect(emptyData).toBeUndefined();
  });
});
