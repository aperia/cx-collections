// Redux
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Services
import { transactionAdjustmentRequestServices } from '../requestListService';
import {
  RejectAdjustmentRequestArg,
  TransactionAdjustmentRequest
} from '../types';

export const rejectAdjustment = createAsyncThunk<
  unknown,
  RejectAdjustmentRequestArg,
  ThunkAPIConfig
>('transactionAdjustment/rejectAdjustment', async (args, _) => {
  const response = await transactionAdjustmentRequestServices.rejectAdjustment(
    args
  );

  return response;
});

export const rejectAdjustmentBuilder = (
  builder: ActionReducerMapBuilder<TransactionAdjustmentRequest>
) => {
  builder
    .addCase(rejectAdjustment.pending, (draftState, action) => {
      draftState.isLoadingView = true;
    })
    .addCase(rejectAdjustment.fulfilled, (draftState, action) => {
      draftState.isLoadingView = false;
    })
    .addCase(rejectAdjustment.rejected, (draftState, action) => {
      draftState.isLoadingView = false;
    });
};
