import { createSelector } from '@reduxjs/toolkit';
import { MemoData } from '../types';

const getTransactionAdjustmentState = (state: RootState) => {
  return state.transactionAdjustment;
};

const getCurrentTransactionAdjustmentRequest = (state: RootState) => {
  return state.transactionAdjustment.currentRequest;
};

export const takeCurrentTransactionAdjustmentRequest = createSelector(
  getCurrentTransactionAdjustmentRequest,
  data => data
);

export const selectErrorTransactionAdjustmentRequest = createSelector(
  getTransactionAdjustmentState,
  data => data.isError
);

const getLoadingTransactionAdjustmentRequest = (state: RootState) => {
  return state.transactionAdjustment.isLoading;
};

export const takeLoadingTransactionAdjustmentRequest = createSelector(
  getLoadingTransactionAdjustmentRequest,
  data => data
);

export const selectTransactionAdjustmentRequestList = createSelector(
  getTransactionAdjustmentState,
  data => data.transactionAdjustmentRequestList
);

export const selectPendingSortTransactionAdjustmentRequest = createSelector(
  getTransactionAdjustmentState,
  data => data.sortBy
);

const getOpenModalStatus = (state: RootState) => {
  return state.transactionAdjustment.openModal;
};

export const takeOpenModalStatus = createSelector(
  getOpenModalStatus,
  data => data
);

const getTransactionAdjustmentDetail = (state: RootState) => {
  return state.transactionAdjustment.adjustmentDetail;
};

export const takeDataAdjustmentDetail = createSelector(
  getTransactionAdjustmentDetail,
  data => data
);

const getViewLoadingStatus = (state: RootState) => {
  return state.transactionAdjustment.isLoadingView;
};

export const takeViewLoadingStatus = createSelector(
  getViewLoadingStatus,
  data => data
);

const getAddFormValue = (state: RootState) => {
  const { viewName = '' } = state.transactionAdjustment;
  return state.form[viewName]?.values || {};
};

export const takeAddFormValue = createSelector(
  getAddFormValue,
  data => data as MemoData
);

export const takeHasLoadMore = createSelector(
  getTransactionAdjustmentState,
  data => data.hasLoadMore
);

export const takeIsLoadMoreLoading = createSelector(
  getTransactionAdjustmentState,
  data => data.isLoadMoreLoading
);
