import { rejectAdjustment } from './rejectAdjustment';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { responseDefault, storeId } from 'app/test-utils';

import { transactionAdjustmentRequestServices } from '../requestListService';

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('TransactionAdjustment > rejectAdjustment', () => {
  it('success', async () => {
    spy = jest
      .spyOn(transactionAdjustmentRequestServices, 'rejectAdjustment')
      .mockResolvedValue({ ...responseDefault, data: [] });

    const store = createStore(rootReducer, {});
    const response = await rejectAdjustment({
      common: {
        accountId: storeId
      }
    } as any)(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({ ...responseDefault, data: [] });
  });

  it('reject', async () => {
    spy = jest
      .spyOn(transactionAdjustmentRequestServices, 'rejectAdjustment')
      .mockRejectedValue({});

    const store = createStore(rootReducer, {});
    const response = await rejectAdjustment({
      common: {
        accountId: storeId
      }
    } as any)(store.dispatch, store.getState, {});

    expect(response.payload).toBeUndefined();
  });
});
