import {
  transactionAdjustmentAction,
  reducer,
  initialState as stateDefault
} from './reducer';
import { GRID_COLUMN_NAME } from '../constants';
import { SORT_TYPE } from 'app/constants';
import { TransactionAdjustmentRequest } from '../types';

const {
  changeRequestType,
  removeStore,
  setPendingSortBy,
  setViewName,
  updateOpenModal,
  removeTransaction
} = transactionAdjustmentAction;

const initialState: TransactionAdjustmentRequest = {
  transactionAdjustmentRequestList: [{ id: 'id' }]
};

describe('Hardship > reducer', () => {
  it('removeStore', () => {
    // invoke
    const state = reducer(initialState, removeStore());

    // expect
    expect(state).toEqual(stateDefault);
  });

  it('changeRequestType', () => {
    // invoke
    const state = reducer(
      initialState,
      changeRequestType({ requestStatusSelected: 'requestStatusSelected' })
    );

    // expect
    expect(state.sortBy).toEqual({ id: GRID_COLUMN_NAME.OPERATION_DATE_TIME });
    expect(state.currentRequest).toEqual('requestStatusSelected');
  });

  it('setPendingSortBy', () => {
    // invoke
    const sortBy = { id: 'id', order: SORT_TYPE.DESC };
    const state = reducer(initialState, setPendingSortBy({ sortBy }));

    // expect
    expect(state.sortBy).toEqual(sortBy);
  });

  it('updateOpenModal', () => {
    // invoke
    const params = { openModal: true, data: [] };
    const state = reducer(initialState, updateOpenModal(params));

    // expect
    expect(state.openModal).toEqual(params.openModal);
    expect(state.adjustmentDetail).toEqual(params.data);
  });

  it('setViewName', () => {
    // invoke
    const params = { viewName: 'viewName' };
    const state = reducer(initialState, setViewName(params));

    // expect
    expect(state.viewName).toEqual(params.viewName);
  });

  it('removeTransaction', () => {
    // invoke
    const params = { id: 'id' };
    const state = reducer(initialState, removeTransaction(params));

    // expect
    expect(state.startSequence).toEqual(NaN);
    expect(state.endSequence).toEqual(NaN);
  });

  it('removeTransaction when startSequence is 1', () => {
    const initialState: TransactionAdjustmentRequest = {
      transactionAdjustmentRequestList: [{ id: 'id' }],
      startSequence: 1
    };

    // invoke
    const params = { id: 'id' };
    const state = reducer(initialState, removeTransaction(params));

    // expect
    expect(state.startSequence).toEqual(1);
    expect(state.endSequence).toEqual(NaN);
  });
});
