// Redux
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Services
import { transactionAdjustmentRequestServices } from '../requestListService';
import {
  ApproveAdjustmentRequestArg,
  TransactionAdjustmentRequest
} from '../types';

export const approveAdjustment = createAsyncThunk<
  unknown,
  ApproveAdjustmentRequestArg,
  ThunkAPIConfig
>('transactionAdjustment/approveAdjustment', async (args, _) => {
  const response = await transactionAdjustmentRequestServices.approveAdjustment(
    args
  );

  return response;
});

export const approveAdjustmentBuilder = (
  builder: ActionReducerMapBuilder<TransactionAdjustmentRequest>
) => {
  builder
    .addCase(approveAdjustment.pending, (draftState, action) => {
      draftState.isLoadingView = true;
    })
    .addCase(approveAdjustment.fulfilled, (draftState, action) => {
      draftState.isLoadingView = false;
    })
    .addCase(approveAdjustment.rejected, (draftState, action) => {
      draftState.isLoadingView = false;
    });
};
