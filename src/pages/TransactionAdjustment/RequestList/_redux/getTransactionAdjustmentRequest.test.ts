import './reducer';
import { getTransactionAdjustmentRequest } from './getTransactionAdjustmentRequest';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { responseDefault } from 'app/test-utils';

import { transactionAdjustmentRequestServices } from '../requestListService';

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('TransactionAdjustment > getTransactionAdjustmentRequest', () => {
  describe('success', () => {
    it('with empty data', async () => {
      spy = jest
        .spyOn(
          transactionAdjustmentRequestServices,
          'getTransactionAdjustmentRequest'
        )
        .mockResolvedValue({
          ...responseDefault,
          data: {
            adjustmentReviewQueueList: []
          }
        });
      const store = createStore(rootReducer, {});
      const response = await getTransactionAdjustmentRequest({})(
        store.dispatch,
        store.getState,
        {}
      );

      expect(response.payload).toEqual({
        type: 'pending',
        data: [],
        endSequence: 10,
        startSequence: 1
      });
    });

    it('with data', async () => {
      const data = {
        pending: [
          { id: 'id_1' },
          { id: 'id_2' },
          { id: 'id_3' },
          { id: 'id_4' },
          { id: 'id_5' },
          { id: 'id_6' },
          { id: 'id_7' },
          { id: 'id_8' },
          { id: 'id_9' },
          { id: 'id_10' }
        ],
        adjustmentReviewQueueList: [
          { accNbr: undefined },
          { accNbr: undefined },
          { accNbr: undefined },
          { accNbr: undefined },
          { accNbr: undefined },
          { accNbr: undefined },
          { accNbr: undefined },
          { accNbr: undefined },
          { accNbr: undefined },
          { accNbr: undefined },
          { accNbr: undefined }
        ]
      };
      spy = jest
        .spyOn(
          transactionAdjustmentRequestServices,
          'getTransactionAdjustmentRequest'
        )
        .mockResolvedValue({
          ...responseDefault,
          data
        });

      const store = createStore(rootReducer, {});
      const response = await getTransactionAdjustmentRequest({})(
        store.dispatch,
        store.getState,
        {}
      );

      expect(response.payload).toEqual({
        type: 'pending',
        data: data.adjustmentReviewQueueList,
        endSequence: 10,
        startSequence: 1
      });
    });
  });

  describe('reject', () => {
    it('error 200', async () => {
      spy = jest
        .spyOn(
          transactionAdjustmentRequestServices,
          'getTransactionAdjustmentRequest'
        )
        .mockImplementation(async () => {
          throw {
            status: 200,
            data: { message: 'NO TRANSACTION ADJUSTMENT REQUEST' }
          };
        });
      const store = createStore(rootReducer, {});
      const response = await getTransactionAdjustmentRequest({})(
        store.dispatch,
        store.getState,
        {}
      );
      expect((response.payload as any)?.data).toEqual({
        message: 'NO TRANSACTION ADJUSTMENT REQUEST'
      });
      expect((response.payload as any)?.status).toEqual(200);
    });

    it('error 455', async () => {
      spy = jest
        .spyOn(
          transactionAdjustmentRequestServices,
          'getTransactionAdjustmentRequest'
        )
        .mockImplementation(async () => {
          throw {
            status: 455,
            data: { message: 'NO TRANSACTION ADJUSTMENT REQUEST' }
          };
        });
      const store = createStore(rootReducer, {});
      const response = await getTransactionAdjustmentRequest({})(
        store.dispatch,
        store.getState,
        {}
      );
      expect(response.payload).toEqual({
        data: [],
        endSequence: 10,
        startSequence: 1,
        type: 'pending'
      });
    });

    it('should return record not found when error 455 and messgae RECORD NOT FOUND', async () => {
      spy = jest
        .spyOn(
          transactionAdjustmentRequestServices,
          'getTransactionAdjustmentRequest'
        )
        .mockRejectedValue({
          status: 400,
          data: { errors: { message: 'RECORD NOT FOUND' } }
        });
      const store = createStore(rootReducer, {});
      const response = await getTransactionAdjustmentRequest({})(
        store.dispatch,
        store.getState,
        {}
      );

      expect(response.payload).toEqual({
        data: {
          errors: {
            message: 'RECORD NOT FOUND'
          }
        },
        status: 400
      });
    });

    it('another error', async () => {
      spy = jest
        .spyOn(
          transactionAdjustmentRequestServices,
          'getTransactionAdjustmentRequest'
        )
        .mockRejectedValue({});

      const store = createStore(rootReducer, {});
      const response = await getTransactionAdjustmentRequest({})(
        store.dispatch,
        store.getState,
        {}
      );

      expect(response.payload).toBeUndefined();
    });
  });
});
