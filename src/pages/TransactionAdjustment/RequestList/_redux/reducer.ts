import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// types & constants
import { GRID_COLUMN_NAME, TRANSACTION_STATUS } from '../constants';
import {
  ChangeRequestTypePayload,
  RemoveTransactionPayload,
  SetSortByPayload,
  TransactionAdjustmentRequest,
  UpdateModal,
  ViewNamePayload
} from '../types';

// Redux
import {
  getTransactionAdjustmentRequest,
  getTransactionAdjustmentRequestBuilder
} from './getTransactionAdjustmentRequest';
import {
  getMoreTransactionAdjustmentRequest,
  getMoreTransactionAdjustmentRequestBuilder
} from './getMoreTransactionAdjustmentRequest';
import { rejectAdjustment, rejectAdjustmentBuilder } from './rejectAdjustment';
import {
  approveAdjustment,
  approveAdjustmentBuilder
} from './approveAdjustment';
import { triggerAdjustment } from './triggerAdjustment';

export const initialState: TransactionAdjustmentRequest = {
  transactionAdjustmentRequestList: [],
  isLoading: false,
  isLoadingView: false,
  isError: false,
  openModal: false,
  currentRequest: TRANSACTION_STATUS.PENDING,
  sortBy: { id: GRID_COLUMN_NAME.OPERATION_DATE_TIME },
  adjustmentDetail: {}
};

const { actions, reducer } = createSlice({
  name: 'transactionAdjustmentRequest',
  initialState,
  reducers: {
    removeStore: () => initialState,
    changeRequestType: (
      draftState,
      action: PayloadAction<ChangeRequestTypePayload>
    ) => {
      const { requestStatusSelected } = action.payload;
      // reset sort when change status
      draftState.sortBy = { id: GRID_COLUMN_NAME.OPERATION_DATE_TIME };
      draftState.currentRequest = requestStatusSelected;
    },
    setPendingSortBy: (draftState, action: PayloadAction<SetSortByPayload>) => {
      const { sortBy } = action.payload;
      draftState.sortBy = sortBy;
    },
    updateOpenModal: (draftState, action: PayloadAction<UpdateModal>) => {
      const { openModal, data } = action.payload;
      draftState.openModal = openModal;
      draftState.adjustmentDetail = data;
    },
    setViewName: (draftState, action: PayloadAction<ViewNamePayload>) => {
      const { viewName } = action.payload;
      draftState.viewName = viewName;
    },
    removeTransaction: (
      draftState,
      action: PayloadAction<RemoveTransactionPayload>
    ) => {
      const { id } = action.payload;
      draftState.transactionAdjustmentRequestList = draftState.transactionAdjustmentRequestList.filter(
        item => item.id !== id
      );
      draftState.endSequence! -= 1;
      if (draftState.startSequence !== 1) {
        draftState.startSequence! -= 1;
      }
    }
  },

  extraReducers: builder => {
    getTransactionAdjustmentRequestBuilder(builder);
    getMoreTransactionAdjustmentRequestBuilder(builder);
    rejectAdjustmentBuilder(builder);
    approveAdjustmentBuilder(builder);
  }
});

const transactionAdjustmentAction = {
  ...actions,
  getTransactionAdjustmentRequest,
  getMoreTransactionAdjustmentRequest,
  rejectAdjustment,
  approveAdjustment,
  triggerAdjustment
};

export { transactionAdjustmentAction, reducer };
