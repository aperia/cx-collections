// Redux
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Services
import { transactionAdjustmentRequestServices } from '../requestListService';

// types
import {
  GetTransactionAdjustmentRequestArg,
  GetTransactionAdjustmentRequestPayload,
  TransactionAdjustmentRequest
} from '../types';
import { DEFAULT_PAGE_SIZE } from '../constants';

// helpers
import { mappingDataFromObj } from 'app/helpers/mappingData';
import { parseJSONString } from 'app/helpers/commons';
import { formatAmount } from './../helpers';
import { NO_RESULT_CODE } from 'pages/AccountSearch/AccountSearchList/constants';

export const getTransactionAdjustmentRequest = createAsyncThunk<
  GetTransactionAdjustmentRequestPayload,
  Partial<GetTransactionAdjustmentRequestArg>,
  ThunkAPIConfig
>(
  'transactionAdjustment/getTransactionAdjustmentRequest',
  async (args, thunkAPI) => {
    const state = thunkAPI.getState();
    const { currentRequest } = state.transactionAdjustment;
    const mapping = state.mapping?.data?.transactionAdjustmentRequest || {};

    try {
      const { data } =
        await transactionAdjustmentRequestServices.getTransactionAdjustmentRequest(
          { type: currentRequest }
        );

      const mappedData = data?.adjustmentReviewQueueList?.map(
        (item: MagicKeyValue) => mappingDataFromObj(item, mapping)
      );

      const newData = mappedData?.map((item: any) => ({
        ...item,
        amount: formatAmount(item?.amount),
        amountTransaction: formatAmount(item?.amountTransaction),
        accNbr: parseJSONString(item?.accNbr)
      }));

      return {
        type: currentRequest,
        data: newData,
        startSequence: 1,
        endSequence: DEFAULT_PAGE_SIZE
      };
    } catch (error) {
      const errorStatus: number = error?.status || error?.response?.status;
      if (errorStatus.toString() === NO_RESULT_CODE) {
        return {
          type: currentRequest,
          data: [],
          startSequence: 1,
          endSequence: DEFAULT_PAGE_SIZE
        };
      }
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const getTransactionAdjustmentRequestBuilder = (
  builder: ActionReducerMapBuilder<TransactionAdjustmentRequest>
) => {
  builder
    .addCase(getTransactionAdjustmentRequest.pending, (draftState, action) => {
      draftState.transactionAdjustmentRequestList = [];
      draftState.isLoading = true;
      draftState.isError = false;
    })
    .addCase(
      getTransactionAdjustmentRequest.fulfilled,
      (draftState, action) => {
        const { data } = action.payload;
        draftState.isLoading = false;
        draftState.isError = false;
        draftState.transactionAdjustmentRequestList = data;
        draftState.startSequence = 1;
        draftState.endSequence = DEFAULT_PAGE_SIZE;
        draftState.hasLoadMore =
          data?.length < DEFAULT_PAGE_SIZE ? false : true;
      }
    )
    .addCase(getTransactionAdjustmentRequest.rejected, (draftState, action) => {
      draftState.transactionAdjustmentRequestList = [];
      draftState.isLoading = false;
      draftState.isError = true;
    });
};
