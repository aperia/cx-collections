import { getAccInformation, triggerAdjustment } from './triggerAdjustment';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault,
  storeId
} from 'app/test-utils';

import { transactionAdjustmentRequestServices } from '../requestListService';
import { TRANSACTION_STATUS } from '../constants';
import { MemoType } from 'pages/Memos/types';

import { actionMemoChronicle } from 'pages/Memos/Chronicle/_redux/reducers';
import { cisMemoActions } from 'pages/Memos/CIS/_redux/reducers';
import { transactionAdjustmentAction } from './reducer';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import accountInformationService from 'pages/AccountDetails/AccountInformation/accountInformationService';

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

const cisMemoSpy = mockActionCreator(cisMemoActions);
const cisMemoChronicleSpy = mockActionCreator(actionMemoChronicle);
const transactionAdjustmentSpy = mockActionCreator(transactionAdjustmentAction);
const toastSpy = mockActionCreator(actionsToast);

const generateState = (
  type?: MemoType,
  hasMemoType = true,
  hasMemoContent = true
) => {
  const initialState: Partial<RootState> = {
    transactionAdjustment: {
      viewName: 'viewName',
      transactionAdjustmentRequestList: [{ id: 'id' }]
    } as any,
    memo: {
      [storeId]: {
        configMemo: { data: { type } }
      }
    },
    form: {
      viewName: {
        registeredFields: [],
        values: {
          memoType: hasMemoType ? { value: 'text' } : undefined,
          memoContent: hasMemoContent ? [{}, {}] : []
        }
      }
    }
  };
  return initialState;
};

describe('TransactionAdjustment > triggerAdjustment', () => {
  beforeEach(() => {
    cisMemoChronicleSpy('addMemoChronicle');
    cisMemoSpy('addCISMemoRequest');
    transactionAdjustmentSpy('approveAdjustment');
    transactionAdjustmentSpy('rejectAdjustment');
  });

  describe('success', () => {
    it('getAccInformation', async () => {
      jest
        .spyOn(accountInformationService, 'getAccountInformation')
        .mockResolvedValue({
          data: '123'
        });

      const store = createStore(rootReducer, {});
      const response = await getAccInformation({ accountId: '123456789' })(
        store.dispatch,
        store.getState,
        {}
      );

      expect(response.type).toEqual(
        'transactionAdjustment/getAccInformation/fulfilled'
      );
      expect(response.payload).toEqual({ data: '123' });
    });

    it('empty state', async () => {
      spy = jest
        .spyOn(transactionAdjustmentRequestServices, 'approveAdjustment')
        .mockResolvedValue({ ...responseDefault, data: [] });

      const mockAction = transactionAdjustmentSpy('approveAdjustment');

      const store = createStore(rootReducer, {});
      const response = await triggerAdjustment({
        id: storeId,
        storeId,
        type: TRANSACTION_STATUS.APPROVED,
        payload: {
          common: { accountId: storeId }
        } as any,
        maskedValue: ''
      })(store.dispatch, store.getState, {});

      expect(response.payload).toBeUndefined();
      expect(mockAction).toBeCalledWith({
        common: { accountId: storeId }
      });
    });

    it('without type', async () => {
      spy = jest
        .spyOn(transactionAdjustmentRequestServices, 'approveAdjustment')
        .mockResolvedValue({ ...responseDefault, data: [] });

      const mockAction = transactionAdjustmentSpy('approveAdjustment');

      const store = createStore(rootReducer, generateState());
      const response = await triggerAdjustment({
        id: storeId,
        storeId,
        payload: {
          common: { accountId: storeId }
        } as any,
        maskedValue: ''
      })(store.dispatch, store.getState, {});

      expect(response.payload).toBeUndefined();
      expect(mockAction).not.toBeCalled();
    });

    describe('isFulfilled and isRejected', () => {
      it('fulfilled case 1', async () => {
        const store = createStore(rootReducer, generateState('2' as MemoType));
        const mockAddToast = toastSpy('addToast');
        await triggerAdjustment({
          id: storeId,
          storeId,
          payload: {
            common: { accountId: storeId }
          } as any,
          maskedValue: ''
        })(
          byPassFulfilled('transactionAdjustment/getAccInformation/fulfilled', {
            data: {
              messages: [{ status: 'success' }],
              customerInquiry: [
                {
                  clientIdentifier: '000111',
                  systemIdentifier: 'xxxyyy',
                  principalIdentifier: 'aaabbbccc',
                  agentIdentifier: '666xxx999'
                }
              ]
            }
          }),
          store.getState,
          {}
        );
        expect(mockAddToast).toBeCalledWith({
          type: 'success',
          message: 'txt_reject_successful'
        });
      });

      it('fulfilled case 2', async () => {
        const store = createStore(rootReducer, generateState('0' as MemoType));
        const mockAddToast = toastSpy('addToast');
        await triggerAdjustment({
          id: storeId,
          storeId,
          payload: {
            common: { accountId: storeId }
          } as any,
          maskedValue: ''
        })(
          byPassFulfilled('transactionAdjustment/getAccInformation/fulfilled', {
            data: {
              messages: [{ status: 'success' }],
              customerInquiry: [
                {
                  clientIdentifier: '000111',
                  systemIdentifier: 'xxxyyy',
                  principalIdentifier: 'aaabbbccc',
                  agentIdentifier: '666xxx999'
                }
              ]
            }
          }),
          store.getState,
          {}
        );
        expect(mockAddToast).toBeCalledWith({
          type: 'success',
          message: 'txt_reject_successful'
        });
      });

      it('Not have memoContent', async () => {
        const store = createStore(
          rootReducer,
          generateState('2' as MemoType, true, false)
        );
        const mockAddToast = toastSpy('addToast');
        await triggerAdjustment({
          id: storeId,
          storeId,
          payload: {
            common: { accountId: storeId }
          } as any,
          maskedValue: ''
        })(
          byPassFulfilled('transactionAdjustment/getAccInformation/fulfilled', {
            data: {
              messages: [{ status: 'success' }],
              customerInquiry: [
                {
                  clientIdentifier: '000111',
                  systemIdentifier: 'xxxyyy',
                  principalIdentifier: 'aaabbbccc',
                  agentIdentifier: '666xxx999'
                }
              ]
            }
          }),
          store.getState,
          {}
        );
        expect(mockAddToast).toBeCalledWith({
          type: 'success',
          message: 'txt_reject_successful'
        });
      });

      it('fulfilled case 3 > approve', async () => {
        const store = createStore(rootReducer, generateState('2' as MemoType));
        const mockAddToast = toastSpy('addToast');
        await triggerAdjustment({
          id: storeId,
          storeId,
          type: 'approved',
          payload: {
            common: { accountId: storeId }
          } as any,
          maskedValue: ''
        })(
          byPassFulfilled('transactionAdjustment/getAccInformation/fulfilled', {
            data: {
              messages: [{ status: 'success' }],
              customerInquiry: [
                {
                  clientIdentifier: '000111',
                  systemIdentifier: 'xxxyyy',
                  principalIdentifier: 'aaabbbccc',
                  agentIdentifier: '666xxx999'
                }
              ]
            }
          }),
          store.getState,
          {}
        );
        expect(mockAddToast).toBeCalledWith({
          type: 'success',
          message: 'txt_approve_successful'
        });
      });

      it('rejected', async () => {
        const store = createStore(rootReducer, generateState());
        const mockAddToast = toastSpy('addToast');
        await triggerAdjustment({
          id: storeId,
          storeId,
          payload: {
            common: { accountId: storeId }
          } as any,
          maskedValue: ''
        })(
          byPassRejected('transactionAdjustment/getAccInformation/rejected', {
            data: {
              messages: [{ status: 'success' }]
            }
          }),
          store.getState,
          {}
        );
        expect(mockAddToast).toBeCalledWith({
          type: 'error',
          message: 'txt_reject_failed'
        });
      });

      it('rejected > approved', async () => {
        const store = createStore(rootReducer, generateState());
        const mockAddToast = toastSpy('addToast');
        await triggerAdjustment({
          id: storeId,
          storeId,
          type: 'approved',
          payload: {
            common: { accountId: storeId }
          } as any,
          maskedValue: ''
        })(
          byPassRejected('transactionAdjustment/getAccInformation/rejected', {
            data: {
              messages: [{ status: 'success' }]
            }
          }),
          store.getState,
          {}
        );
        expect(mockAddToast).toBeCalledWith({
          type: 'error',
          message: 'txt_approve_failed'
        });
      });
    });
  });
});
