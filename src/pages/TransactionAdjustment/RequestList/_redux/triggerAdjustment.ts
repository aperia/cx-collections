import {
  AsyncThunkAction,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// Const
import { TRANSACTION_STATUS } from '../constants';
import { TriggerAdjustmentRequestArg } from '../types';

// Redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { transactionAdjustmentAction } from './reducer';

// Const
import { I18N_TXN_ADJ } from 'pages/TransactionAdjustment/constants';
import { MemoType } from 'pages/Memos/types';
import { MONETARY_ADJUSTMENT_ACTIONS } from 'pages/Memos/constants';

// Redux
import { actionMemoChronicle } from 'pages/Memos/Chronicle/_redux/reducers';
import { cisMemoActions } from 'pages/Memos/CIS/_redux/reducers';
import { handleGenerateMemoFlags } from 'pages/Memos/CIS/helpers';
import { memoActions } from 'pages/Memos/_redux/reducers';
import {
  CHRONICLE_DEFAULT_KEY,
  CHRONICLE_TRANSACTION_ADJUSTMENT_KEY
} from 'pages/ClientConfiguration/Memos/constants';
import accountInformationService from 'pages/AccountDetails/AccountInformation/accountInformationService';

export const getAccInformation = createAsyncThunk<
  unknown,
  MagicKeyValue,
  ThunkAPIConfig
>('transactionAdjustment/getAccInformation', async (args, _) => {
  const { accountId } = args;
  const response = await accountInformationService.getAccountInformation(
    accountId,
    [
      'customerInquiry.clientIdentifier',
      'customerInquiry.systemIdentifier',
      'customerInquiry.principalIdentifier',
      'customerInquiry.agentIdentifier'
    ]
  );

  return response;
});

export const triggerAdjustment = createAsyncThunk<
  unknown,
  TriggerAdjustmentRequestArg,
  ThunkAPIConfig
>('transactionAdjustment/triggerAdjustment', async (args, thunkAPI) => {
  const { storeId, type, payload, maskedValue } = args;
  const { operatorID = '' } = window?.appConfig?.commonConfig || {};
  const { dispatch, getState } = thunkAPI;
  const { memo, form, transactionAdjustment } = getState();

  const { common, entryDate, entryTime } = payload;
  const accountId = common?.accountId;

  const { memoType, memoContent = '' } =
    form[transactionAdjustment?.viewName || '']?.values || {};

  const isApprove = type === TRANSACTION_STATUS.APPROVED;

  let action: AsyncThunkAction<unknown, unknown, ThunkAPIConfig>[] = isApprove
    ? [transactionAdjustmentAction.approveAdjustment(payload)]
    : [transactionAdjustmentAction.rejectAdjustment(payload)];

  // get account detail to genera CSPA
  const accountInfoResponse = await dispatch(getAccInformation({ accountId }));
  const { data } = accountInfoResponse.payload as MagicKeyValue;

  const {
    clientIdentifier: clientNumber,
    systemIdentifier: system,
    principalIdentifier: prin,
    agentIdentifier: agent
  } = data?.customerInquiry?.[0] || {};

  const cspa = { clientNumber, system, prin, agent };
  /*-----------------------------*/

  if (memoContent.length) {
    switch (memo[storeId].configMemo?.data?.type) {
      case MemoType.CHRONICLE:
        action = [
          ...action,
          actionMemoChronicle.addMemoChronicle({
            storeId,
            data: { chronicleKey: CHRONICLE_DEFAULT_KEY, cspa },
            postData: {
              accountId,
              memoText: memoContent
            }
          })
        ];
        break;
      case MemoType.CIS:
        action = [
          ...action,
          cisMemoActions.addCISMemoRequest({
            storeId,
            payload: {
              accountId,
              memoText: memoContent,
              memoType: memoType.value,
              memoOperatorIdentifier: operatorID,
              memoFlags: handleGenerateMemoFlags(memoType.value)
            }
          })
        ];
        break;
      default:
        break;
    }
  }

  const [adjustmentResponse] = await Promise.all(
    action.map(item => dispatch(item))
  );

  if (isFulfilled(adjustmentResponse)) {
    const trans = transactionAdjustment.transactionAdjustmentRequestList.find(
      item => item.date === entryDate && item.time === entryTime
    );
    const memoData = {
      operatorId: operatorID,
      maskedAccNo: maskedValue,
      hostCdFromTable: trans?.operatorId,
      dollarAmount1: trans?.amount,
      batchCd: trans?.batch,
      adjTranCd: trans?.transactionType
    };
    batch(() => {
      dispatch(
        actionsToast.addToast({
          type: 'success',
          message: isApprove
            ? I18N_TXN_ADJ.APPROVE_SUCCESSFUL
            : I18N_TXN_ADJ.REJECT_SUCCESSFUL
        })
      );
      dispatch(transactionAdjustmentAction.getTransactionAdjustmentRequest({}));
      dispatch(
        transactionAdjustmentAction.updateOpenModal({
          data: {},
          openModal: false
        })
      );
      dispatch(
        memoActions.postMemo(
          isApprove
            ? MONETARY_ADJUSTMENT_ACTIONS.APPROVE_ADJUSTMENT
            : MONETARY_ADJUSTMENT_ACTIONS.REJECT_ADJUSTMENT,
          {
            storeId,
            cspa,
            accEValue: common?.accountId,
            chronicleKey: CHRONICLE_TRANSACTION_ADJUSTMENT_KEY,
            ...memoData
          }
        )
      );
    });
  }

  if (isRejected(adjustmentResponse)) {
    dispatch(
      actionsToast.addToast({
        type: 'error',
        message: isApprove
          ? I18N_TXN_ADJ.APPROVE_FAILED
          : I18N_TXN_ADJ.REJECT_FAILED
      })
    );
  }
});
