// Redux
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Services
import { transactionAdjustmentRequestServices } from '../requestListService';

// types
import {
  GetTransactionAdjustmentRequestArg,
  GetTransactionAdjustmentRequestPayload,
  TransactionAdjustmentRequest
} from '../types';
import { DEFAULT_PAGE_SIZE } from '../constants';

// helpers
import { mappingDataFromObj } from 'app/helpers/mappingData';
import { parseJSONString } from 'app/helpers/commons';
import { formatAmount } from './../helpers';
import { NO_RESULT_CODE } from 'pages/AccountSearch/AccountSearchList/constants';

export const getMoreTransactionAdjustmentRequest = createAsyncThunk<
  GetTransactionAdjustmentRequestPayload,
  Partial<GetTransactionAdjustmentRequestArg>,
  ThunkAPIConfig
>(
  'transactionAdjustment/getMoreTransactionAdjustmentRequest',
  async (args, thunkAPI) => {
    const state = thunkAPI.getState();
    const { currentRequest, endSequence } = state.transactionAdjustment;

    const newStartSequence = endSequence! + 1;
    const newEndSequence = endSequence! + DEFAULT_PAGE_SIZE;

    const mapping = state.mapping?.data?.transactionAdjustmentRequest || {};

    try {
      const { data } =
        await transactionAdjustmentRequestServices.getTransactionAdjustmentRequest(
          {
            type: currentRequest,
            startSequence: newStartSequence,
            endSequence: newEndSequence
          }
        );

      const mappedData = data?.adjustmentReviewQueueList?.map(
        (item: MagicKeyValue) => mappingDataFromObj(item, mapping)
      );

      const newData = mappedData?.map((item: any) => ({
        ...item,
        amount: formatAmount(item?.amount),
        amountTransaction: formatAmount(item?.amountTransaction),
        accNbr: parseJSONString(item?.accNbr)
      }));

      return {
        type: currentRequest,
        data: newData,
        startSequence: newStartSequence,
        endSequence: newEndSequence
      };
    } catch (error) {
      const errorStatus = error?.status || error?.response?.status;
      if (errorStatus.toString() === NO_RESULT_CODE) {
        return {
          type: currentRequest,
          data: [],
          startSequence: newStartSequence,
          endSequence: newEndSequence
        };
      }
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const getMoreTransactionAdjustmentRequestBuilder = (
  builder: ActionReducerMapBuilder<TransactionAdjustmentRequest>
) => {
  builder
    .addCase(
      getMoreTransactionAdjustmentRequest.pending,
      (draftState, action) => {
        draftState.isLoadMoreLoading = true;
        draftState.isError = false;
      }
    )
    .addCase(
      getMoreTransactionAdjustmentRequest.fulfilled,
      (draftState, action) => {
        const { data, startSequence, endSequence } = action.payload;
        draftState.isLoading = false;
        draftState.isError = false;
        draftState.transactionAdjustmentRequestList =
          draftState.transactionAdjustmentRequestList.concat(data);
        draftState.startSequence = startSequence;
        draftState.endSequence = endSequence;
        draftState.hasLoadMore =
          data?.length < DEFAULT_PAGE_SIZE ? false : true;
      }
    )
    .addCase(
      getMoreTransactionAdjustmentRequest.rejected,
      (draftState, action) => {
        draftState.transactionAdjustmentRequestList = [];
        draftState.isLoading = false;
        draftState.isError = true;
      }
    );
};
