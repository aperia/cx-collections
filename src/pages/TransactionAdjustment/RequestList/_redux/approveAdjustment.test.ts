import { approveAdjustment } from './approveAdjustment';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { responseDefault, storeId } from 'app/test-utils';

import { transactionAdjustmentRequestServices } from '../requestListService';

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('TransactionAdjustment > approveAdjustment', () => {
  it('success', async () => {
    spy = jest
      .spyOn(transactionAdjustmentRequestServices, 'approveAdjustment')
      .mockResolvedValue({ ...responseDefault, data: [] });

    const store = createStore(rootReducer, {});
    const response = await approveAdjustment({
      common: {
        accountId: storeId
      }
    } as any)(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({ ...responseDefault, data: [] });
  });

  it('reject', async () => {
    spy = jest
      .spyOn(transactionAdjustmentRequestServices, 'approveAdjustment')
      .mockRejectedValue({});

    const store = createStore(rootReducer, {});
    const response = await approveAdjustment({
      common: {
        accountId: storeId
      }
    } as any)(store.dispatch, store.getState, {});

    expect(response.payload).toBeUndefined();
  });
});
