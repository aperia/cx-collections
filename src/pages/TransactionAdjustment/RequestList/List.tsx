import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import orderBy from 'lodash.orderby';
import { useDispatch, useSelector } from 'react-redux';
import {
  Button,
  ColumnType,
  Grid,
  GridRef,
  SortType
} from 'app/_libraries/_dls/components';

// Constant
import {
  defaultOrderDateTime,
  defaultSort,
  transactionStatus,
  TRANSACTION_STATUS,
  GRID_COLUMN_NAME,
  DEFAULT_PAGE_SIZE
} from './constants';
import { I18N_TXN_ADJ } from '../constants';

// Component
import {
  DateTimeGrid,
  FailedApiReload,
  MaskAccNbr,
  NoDataGrid
} from 'app/components';

// Helper
import { formatCommon } from 'app/helpers';

// Type
import { TransactionAdjustmentRequestData } from './types';

// Hook
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Redux
import {
  takeCurrentTransactionAdjustmentRequest,
  selectErrorTransactionAdjustmentRequest,
  takeLoadingTransactionAdjustmentRequest,
  selectPendingSortTransactionAdjustmentRequest,
  selectTransactionAdjustmentRequestList,
  takeHasLoadMore,
  takeIsLoadMoreLoading
} from './_redux/selector';
import { transactionAdjustmentAction } from './_redux/reducer';
import { animationSetTimeout, genAmtId } from 'app/_libraries/_dls/utils';

export interface TransactionAdjustmentRequestListProps {}

const List: React.FC<TransactionAdjustmentRequestListProps> = () => {
  const gridRefs = useRef<GridRef>(null);
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const status = useSelector(takeCurrentTransactionAdjustmentRequest);
  const isLoading = useSelector(takeLoadingTransactionAdjustmentRequest);
  const isError = useSelector(selectErrorTransactionAdjustmentRequest);
  const data = useSelector(selectTransactionAdjustmentRequestList);
  const sortBy = useSelector(selectPendingSortTransactionAdjustmentRequest);
  const hasLoadMore = useSelector(takeHasLoadMore);
  const isLoadMoreLoading = useSelector(takeIsLoadMoreLoading);
  const { isAdminRole: isAdmin, isSupervisor } = window.appConfig?.commonConfig;
  const hasReviewPermission = isSupervisor || isAdmin;
  const testId = 'transaction-adjustment-request_list';

  const [maxHeight, setMaxHeight] = useState(0);

  const handleSortBy = (sort: SortType) => {
    dispatch(transactionAdjustmentAction.setPendingSortBy({ sortBy: sort }));
  };

  const handleReload = useCallback(() => {
    dispatch(transactionAdjustmentAction.getTransactionAdjustmentRequest({}));
  }, [dispatch]);

  const handleLoadMore = () => {
    if (!hasLoadMore) return;
    dispatch(
      transactionAdjustmentAction.getMoreTransactionAdjustmentRequest({})
    );
  };

  const handleOpenModal = useCallback(
    (requestData: TransactionAdjustmentRequestData) => {
      return dispatch(
        transactionAdjustmentAction.updateOpenModal({
          openModal: true,
          data: requestData
        })
      );
    },
    [dispatch]
  );

  const handleScrollToTop = () => {
    const bodyElement = gridRefs.current?.gridBodyElement;
    bodyElement &&
      animationSetTimeout((ratio: number) => {
        const { scrollTop } = bodyElement;
        bodyElement.scrollTo({ top: (1 - ratio) * scrollTop });
      }, 200);
  };

  const loadElement =
    data.length < DEFAULT_PAGE_SIZE ? null : (
      <tr className="row-loading">
        <td data-testid={genAmtId(testId, 'load-more', '')}>
          {hasLoadMore && isLoadMoreLoading ? (
            <>
              <span className="loading" />
              <span
                data-testid={genAmtId(testId, 'load-more_loading-text', '')}
              >
                {t(I18N_TXN_ADJ.LOADING_MORE_ADJUSTMENT_REQUEST)}
              </span>
            </>
          ) : (
            <>
              <span
                className="mr-4"
                data-testid={genAmtId(testId, 'load-more_end-of-list', '')}
              >
                {t(I18N_TXN_ADJ.END_OF_ADJUSTMENT_REQUEST_LIST)}
              </span>
              <span
                className="link text-decoration-none"
                onClick={handleScrollToTop}
                data-testid={genAmtId(testId, 'load-more_back-to-top-btn', '')}
              >
                {t(I18N_TXN_ADJ.BACK_TO_TOP)}
              </span>
            </>
          )}
        </td>
      </tr>
    );

  const dataColumn = useMemo(() => {
    return [
      {
        id: GRID_COLUMN_NAME.OPERATOR_ID,
        Header: t(I18N_TXN_ADJ.OPERATOR_ID),
        accessor: GRID_COLUMN_NAME.OPERATOR_ID,
        isSort: true,
        width: 117
      },
      {
        id: GRID_COLUMN_NAME.OPERATION_DATE_TIME,
        Header: t(transactionStatus[status].headerLabel),
        isSort: true,
        accessor: ({
          date = '',
          time = ''
        }: TransactionAdjustmentRequestData) => (
          <DateTimeGrid date={date} time={time} />
        ),
        width: 192
      },
      {
        id: GRID_COLUMN_NAME.ACC_NBR,
        Header: t(I18N_TXN_ADJ.ACC_NBR),
        accessor: (
          { accNbr }: TransactionAdjustmentRequestData,
          index: number
        ) => (
          <MaskAccNbr
            accNbr={accNbr?.maskedValue || ''}
            dataTestId={`${index}_acc-nbr`}
          />
        ),
        width: 184,
        isSort: true
      },
      {
        id: GRID_COLUMN_NAME.BATCH,
        Header: t(I18N_TXN_ADJ.BATCH),
        accessor: GRID_COLUMN_NAME.BATCH,
        isSort: true,
        width: 100
      },
      {
        id: GRID_COLUMN_NAME.TRANSACTION_TYPE,
        Header: t(I18N_TXN_ADJ.TRANSACTION_TYPE),
        accessor: GRID_COLUMN_NAME.TRANSACTION_TYPE,
        autoWidth: true,
        isSort: true,
        width: 388
      },
      {
        id: GRID_COLUMN_NAME.AMOUNT,
        Header: t(I18N_TXN_ADJ.AMOUNT),
        isSort: true,
        accessor: ({ amount = '' }: TransactionAdjustmentRequestData) =>
          formatCommon(amount).currency(2),
        width: 140,
        cellProps: { className: 'text-right' },
        cellBodyProps: { className: 'text-right' }
      },
      {
        id: 'action',
        Header: t(I18N_TXN_ADJ.ACTION),
        accessor: (requestData: TransactionAdjustmentRequestData) => (
          <Button
            variant="outline-primary"
            size="sm"
            onClick={() => handleOpenModal(requestData)}
          >
            {status === TRANSACTION_STATUS.PENDING && hasReviewPermission
              ? t(I18N_TXN_ADJ.REVIEW)
              : t(I18N_TXN_ADJ.VIEW)}
          </Button>
        ),
        width: 92,
        isFixedRight: true,
        cellProps: { className: 'text-center' },
        cellBodyProps: { className: 'text-center' },
        className: 'td-sm'
      }
    ];
  }, [t, status, hasReviewPermission, handleOpenModal]);

  const gridDataOrdered = useMemo(() => {
    if (!sortBy.order) {
      return orderBy(data, defaultOrderDateTime, defaultSort);
    }

    switch (sortBy.id) {
      case GRID_COLUMN_NAME.OPERATION_DATE_TIME:
        return orderBy(data, defaultOrderDateTime, [
          sortBy.order,
          sortBy.order
        ]);
      case GRID_COLUMN_NAME.ACC_NBR:
        return orderBy(
          data,
          [item => item.accNbr?.maskedValue],
          [sortBy.order]
        );
      case GRID_COLUMN_NAME.AMOUNT:
        return orderBy(
          data,
          [item => parseFloat(item?.amount || '0')],
          [sortBy.order]
        );
      default:
        return orderBy(data, [sortBy.id], [sortBy.order]);
    }
  }, [data, sortBy]);

  useEffect(() => {
    const resizeEvent = () => {
      setMaxHeight(window.innerHeight - 260);
    };

    window.addEventListener('resize', resizeEvent);

    resizeEvent();

    return () => window.removeEventListener('resize', resizeEvent);
  }, []);

  if (isLoading) return null;

  if (isError)
    return (
      <FailedApiReload
        id="transaction-adjustment-request-fail"
        className="mt-80"
        onReload={handleReload}
        dataTestId={`${testId}_failed-api-reload`}
      />
    );

  if (!data.length)
    return (
      <NoDataGrid
        text={t(I18N_TXN_ADJ.NO_DATA)}
        dataTestId={`${testId}_no-data`}
      />
    );

  return (
    <div>
      <Grid
        ref={gridRefs}
        maxHeight={maxHeight}
        columns={dataColumn as unknown as ColumnType[]}
        data={gridDataOrdered}
        sortBy={[sortBy]}
        onSortChange={handleSortBy}
        onLoadMore={handleLoadMore}
        loadElement={loadElement}
        dataTestId={`${testId}_grid`}
      />
    </div>
  );
};

export default List;
