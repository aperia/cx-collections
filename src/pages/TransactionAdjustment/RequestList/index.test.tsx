import React from 'react';
import { screen } from '@testing-library/react';

import RequestList from '.';

import { I18N_TXN_ADJ } from '../constants';

import { mockActionCreator, storeId, renderMockStoreId } from 'app/test-utils';
import { transactionStatus, TRANSACTION_STATUS } from './constants';
import { MemoCISType } from 'pages/Memos/CIS/constants';

import { cisMemoActions } from 'pages/Memos/CIS/_redux/reducers';
import { transactionAdjustmentAction } from './_redux/reducer';
import { MemoType } from 'pages/Memos/types';

jest.mock('./List', () => () => <div />);
jest.mock('./ViewDetail', () => () => <div />);

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const initialState: Partial<RootState> = {
  memo: {
    [storeId]: {
      configMemo: { data: { type: MemoType.CHRONICLE } }
    }
  },
  cisMemo: {
    [storeId]: {
      memoCISRefData: {
        typeRefData: [
          { value: MemoCISType.Standard },
          { value: MemoCISType.Permanent },
          { value: MemoCISType.Priority },
          { value: MemoCISType.PriorityPermanent }
        ]
      }
    }
  },
  transactionAdjustment: {
    currentRequest: TRANSACTION_STATUS.PENDING,
    openModal: true,
    adjustmentDetail: { id: 'id', accNbr: { eValue: storeId } }
  }
};

const generateWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<RequestList />, { initialState });
};

mockActionCreator(cisMemoActions);
const transactionAdjustmentActionSpy = mockActionCreator(
  transactionAdjustmentAction
);

describe('Render', () => {
  it('render UI > empty state', () => {
    const mokcAction = transactionAdjustmentActionSpy(
      'getTransactionAdjustmentRequest'
    );

    generateWrapper(initialState);

    expect(
      screen.getByText(I18N_TXN_ADJ.TRANSACTION_ADJUSTMENT_REQUEST_LIST)
    ).toBeInTheDocument();

    expect(mokcAction).toBeCalledWith({});
  });
});

describe('Actions', () => {
  describe('setStatus', () => {
    it('status pending', () => {
      const mockChangeRequestType = transactionAdjustmentActionSpy(
        'changeRequestType'
      );
      const mockGetTransactionAdjustmentRequest = transactionAdjustmentActionSpy(
        'getTransactionAdjustmentRequest'
      );

      generateWrapper(initialState);

      mockGetTransactionAdjustmentRequest.mockClear();

      screen
        .getByText(transactionStatus[TRANSACTION_STATUS.PENDING].title)
        .click();

      expect(mockChangeRequestType).not.toBeCalled();
      expect(mockGetTransactionAdjustmentRequest).not.toBeCalled();
    });

    it('status rejected', () => {
      const mockChangeRequestType = transactionAdjustmentActionSpy(
        'changeRequestType'
      );
      const mockGetTransactionAdjustmentRequest = transactionAdjustmentActionSpy(
        'getTransactionAdjustmentRequest'
      );

      generateWrapper(initialState);

      screen
        .getByText(transactionStatus[TRANSACTION_STATUS.REJECTED].title)
        .click();

      expect(mockChangeRequestType).toBeCalledWith({
        requestStatusSelected: TRANSACTION_STATUS.REJECTED
      });
      expect(mockGetTransactionAdjustmentRequest).toBeCalledWith({
        type: TRANSACTION_STATUS.REJECTED
      });
    });
  });
});
