import React, { useEffect } from 'react';
import classNames from 'classnames';
import { batch, useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// components
import { Button, GroupButton } from 'app/_libraries/_dls/components';
import List from './List';
import ViewAdjustmentDetail from './ViewDetail';

// types & constants
import { I18N_TXN_ADJ } from '../constants';
import { transactionStatus } from './constants';

// redux
import {
  takeCurrentTransactionAdjustmentRequest,
  takeLoadingTransactionAdjustmentRequest,
  takeOpenModalStatus
} from './_redux/selector';
import { transactionAdjustmentAction } from './_redux/reducer';
import { genAmtId } from 'app/_libraries/_dls/utils';

const RequestList: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const status = useSelector(takeCurrentTransactionAdjustmentRequest);
  const isLoading = useSelector(takeLoadingTransactionAdjustmentRequest);
  const isShowView = useSelector(takeOpenModalStatus);
  const testId = 'transaction-adjustment-request';

  const setStatus = (requestStatusSelected: string) => {
    if (requestStatusSelected === status) return;
    batch(() => {
      dispatch(
        transactionAdjustmentAction.changeRequestType({ requestStatusSelected })
      );
      dispatch(
        transactionAdjustmentAction.getTransactionAdjustmentRequest({
          type: requestStatusSelected
        })
      );
    });
  };

  useEffect(() => {
    dispatch(transactionAdjustmentAction.getTransactionAdjustmentRequest({}));
  }, [dispatch]);

  return (
    <div
      className={classNames('p-24 h-100 overflow-y-auto', {
        loading: isLoading
      })}
    >
      <h3 data-testid={genAmtId(testId, 'title', '')}>
        {t(I18N_TXN_ADJ.TRANSACTION_ADJUSTMENT_REQUEST_LIST)}
      </h3>
      <div className="mt-24">
        <GroupButton
          dataTestId={genAmtId(testId, 'filter_transaction-status', '')}
        >
          {Object.keys(transactionStatus).map(ky => (
            <Button
              variant="secondary"
              key={ky}
              size="sm"
              selected={status === ky}
              onClick={() => setStatus(ky)}
              dataTestId={genAmtId(
                `${ky}_${testId}`,
                'filter_transaction-status',
                ''
              )}
            >
              {t(transactionStatus[ky].title)}
            </Button>
          ))}
        </GroupButton>
      </div>
      <div className="mt-16">
        <List />
        {isShowView && <ViewAdjustmentDetail />}
      </div>
    </div>
  );
};

export default RequestList;
