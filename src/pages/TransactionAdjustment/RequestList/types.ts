import { SortType, BadgeProps } from 'app/_libraries/_dls/components';
import { MemoChronicleType } from 'pages/Memos/Chronicle/types';
export interface TransactionAdjustmentRequestData {
  operatorId?: string;
  date?: string;
  time?: string;
  accNbr?: { eValue?: string; maskedValue?: string };
  batch?: string;
  transactionType?: string;
  amount?: string;
  batchType?: string;
  referenceNbr?: string;
  postDate?: string;
  id?: string;
  managerIdentifier?: string;
  supervisorIdentifier?: string;
  amountTransaction?: string;
  statusCode?: string;
}

export interface TransactionAdjustmentRequest {
  currentRequest: string;
  transactionAdjustmentRequestList: TransactionAdjustmentRequestData[];
  isLoading: boolean;
  isLoadingView: boolean;
  isError: boolean;
  sortBy: SortType;
  openModal: boolean;
  adjustmentDetail?: TransactionAdjustmentRequestData;
  viewName?: string;
  startSequence?: number;
  endSequence?: number;
  hasLoadMore?: boolean;
  isLoadMoreLoading?: boolean;
}

export interface GetTransactionAdjustmentRequestPayload {
  data: TransactionAdjustmentRequestData[];
  type: string;
  startSequence: number;
  endSequence: number;
}

export interface GetTransactionAdjustmentRequestArg {
  type?: string;
  startSequence?: number;
  endSequence?: number;
}

export interface ApproveAdjustmentRequestArg {
  common: {
    accountId: string;
  };
  operatorIdentifier: string;
  managerIdentifier: string;
  supervisorIdentifier: string;
  entryDate: string;
  entryTime: string;
  amount: string;
  newAmount: string;
  statusCode: string;
}

export interface RejectAdjustmentRequestArg {
  common: {
    accountId: string;
  };
  operatorIdentifier: string;
  managerIdentifier: string;
  supervisorIdentifier: string;
  entryDate: string;
  entryTime: string;
  amount: string;
  newAmount: string;
  statusCode: string;
}

export interface TriggerAdjustmentRequestArg {
  id?: string;
  type?: string;
  storeId: string;
  maskedValue: string;
  payload: ApproveAdjustmentRequestArg;
}

export interface MemoData {
  memoType: MemoChronicleType;
  memoContent: string;
}

export interface ChangeRequestTypePayload {
  requestStatusSelected: string;
}

export interface SetSortByPayload {
  sortBy: SortType;
}

export interface UpdateModal {
  openModal: boolean;
  data: TransactionAdjustmentRequestData | {};
}

export interface AdjustmentStatus {
  [ky: string]: {
    title: string;
    headerLabel: string;
    color: BadgeProps['color'];
  };
}

export interface ViewNamePayload {
  viewName?: string;
}

export interface RemoveTransactionPayload {
  id?: string;
}
