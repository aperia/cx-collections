import { formatAmount } from './helpers';

describe('formatAmount', () => {
  it('its working correctly with newAmount is empty string', () => {
    const result = formatAmount('');
    expect(result).toEqual('');
  });

  it('its working correctly with newAmount has dot in string', () => {
    const result = formatAmount('123.45');
    expect(result).toEqual('123.45');
  });

  it('its working correctly with newAmount has dot in string', () => {
    const result = formatAmount('123.45');
    expect(result).toEqual('123.45');
  });

  it('its working correctly with newAmount full string', () => {
    const result = formatAmount('12345');
    expect(result).toEqual('123.45');
  });
});
