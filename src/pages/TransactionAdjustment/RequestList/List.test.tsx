import React from 'react';
import { fireEvent, screen } from '@testing-library/react';
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import List from './List';
import { GRID_COLUMN_NAME, TRANSACTION_STATUS } from './constants';
import { transactionAdjustmentAction } from './_redux/reducer';
import userEvent from '@testing-library/user-event';
import { I18N_TXN_ADJ } from '../constants';
import {
  mockScrollHeight,
  mockScrollTop,
  mockScrollToFn
} from 'app/test-utils/mocks/mockProperty';
import { act } from 'react-dom/test-utils';
import { SortType } from 'app/_libraries/_dls/components';

jest.mock('app/_libraries/_dls/components', () => {
  const allComponents = jest.requireActual('app/_libraries/_dls/components');
  return {
    ...allComponents,
    Pagination: () => <div>Mock Pagination</div>
  };
});

jest.mock('resize-observer-polyfill', () =>
  jest.requireActual('app/test-utils/mocks/resizeObserver.ts')
);

const initialState: Partial<RootState> = {
  transactionAdjustment: {
    currentRequest: TRANSACTION_STATUS.PENDING,
    isLoading: false,
    isError: false,
    isLoadingView: false,
    openModal: false,
    sortBy: { id: GRID_COLUMN_NAME.OPERATION_DATE_TIME },
    transactionAdjustmentRequestList: [
      {
        operatorId: 'operatorId',
        date: '04/02/2021',
        time: '08:00',
        accNbr: { eValue: storeId, maskedValue: 'string' },
        batch: 'batch',
        transactionType: 'transactionType',
        amount: '10'
      }
    ]
  }
};

const loadMoreState: Partial<RootState> = {
  transactionAdjustment: {
    currentRequest: TRANSACTION_STATUS.PENDING,
    isLoading: false,
    isError: false,
    isLoadingView: false,
    openModal: false,
    hasLoadMore: true,
    sortBy: { id: GRID_COLUMN_NAME.OPERATION_DATE_TIME },
    transactionAdjustmentRequestList: [
      {
        operatorId: 'operatorId',
        date: '04/02/2021',
        time: '08:00',
        accNbr: { eValue: storeId, maskedValue: 'string' },
        batch: 'batch',
        transactionType: 'transactionType',
        amount: '10'
      },
      { operatorId: 'operatorId_1' },
      { operatorId: 'operatorId_2' },
      { operatorId: 'operatorId_3' },
      { operatorId: 'operatorId_4' },
      { operatorId: 'operatorId_5' },
      { operatorId: 'operatorId_6' },
      { operatorId: 'operatorId_7' },
      { operatorId: 'operatorId_8' },
      { operatorId: 'operatorId_9' },
      { operatorId: 'operatorId_10' }
    ]
  }
};

const loadMoreWithLoadingState: Partial<RootState> = {
  transactionAdjustment: {
    currentRequest: TRANSACTION_STATUS.REJECTED,
    isLoading: false,
    isError: false,
    isLoadingView: false,
    openModal: false,
    hasLoadMore: true,
    isLoadMoreLoading: true,
    sortBy: { id: GRID_COLUMN_NAME.OPERATION_DATE_TIME },
    transactionAdjustmentRequestList: [
      {
        operatorId: 'operatorId',
        date: '04/02/2021',
        time: '08:00',
        accNbr: { eValue: storeId, maskedValue: 'string' },
        batch: 'batch',
        transactionType: 'transactionType',
        amount: '10'
      },
      { operatorId: 'operatorId_1' },
      { operatorId: 'operatorId_2' },
      { operatorId: 'operatorId_3' },
      { operatorId: 'operatorId_4' },
      { operatorId: 'operatorId_5' },
      { operatorId: 'operatorId_6' },
      { operatorId: 'operatorId_7' },
      { operatorId: 'operatorId_8' },
      { operatorId: 'operatorId_9' },
      { operatorId: 'operatorId_10' }
    ]
  }
};

const otherState: Partial<RootState> = {
  transactionAdjustment: {
    currentRequest: TRANSACTION_STATUS.PENDING as TRANSACTION_STATUS,
    isLoading: false,
    isError: false,
    isLoadingView: false,
    openModal: false,
    sortBy: { id: GRID_COLUMN_NAME.OPERATION_DATE_TIME },
    transactionAdjustmentRequestList: [
      {
        operatorId: 'operatorId',
        date: '04/02/2021',
        time: '08:00',
        accNbr: { eValue: storeId, maskedValue: 'string' },
        batch: 'batch',
        transactionType: 'transactionType',
        amount: '10'
      }
    ]
  }
};

const orderState: Partial<RootState> = {
  transactionAdjustment: {
    currentRequest: TRANSACTION_STATUS.PENDING as TRANSACTION_STATUS,
    isLoading: false,
    isError: false,
    isLoadingView: false,
    openModal: false,
    sortBy: { id: GRID_COLUMN_NAME.OPERATION_DATE_TIME, order: 'desc' },
    transactionAdjustmentRequestList: [
      {
        operatorId: 'operatorId',
        date: '04/01/2021',
        time: '08:00',
        accNbr: { eValue: storeId, maskedValue: 'string' },
        batch: 'batch',
        transactionType: 'transactionType',
        amount: '9'
      },
      {
        operatorId: 'operatorId',
        date: '04/02/2021',
        time: '08:00',
        accNbr: { eValue: storeId, maskedValue: 'string' },
        batch: 'batch',
        transactionType: 'transactionType',
        amount: '10'
      }
    ]
  }
};

const transactionAdjustmentActionSpy = mockActionCreator(
  transactionAdjustmentAction
);

describe('List component', () => {
  it('should render', () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: { commonConfig: { org: 'org', app: 'app' } }
    });
    const { wrapper } = renderMockStoreId(<List />, {
      initialState
    });
    expect(wrapper.container.querySelector('.dls-grid')).toBeInTheDocument();
  });

  it('should render no data', () => {
    renderMockStoreId(<List />, {
      initialState: {}
    });
    expect(
      screen.getByText('txt_transaction_adjustment_request_no_data')
    ).toBeInTheDocument();
  });

  it('should render with loadMore', () => {
    const { wrapper } = renderMockStoreId(<List />, {
      initialState: loadMoreState
    });
    expect(wrapper.container.querySelector('.dls-grid')).toBeInTheDocument();
  });

  it('should render with loadMore and loading', () => {
    const { wrapper } = renderMockStoreId(<List />, {
      initialState: loadMoreWithLoadingState
    });
    expect(wrapper.container.querySelector('.dls-grid')).toBeInTheDocument();
  });

  it('should render other data', () => {
    renderMockStoreId(<List />, {
      initialState: otherState
    });
    expect(screen.getByText('operatorId')).toBeInTheDocument();
    expect(screen.getByText('batch')).toBeInTheDocument();
    expect(screen.getByText('transactionType')).toBeInTheDocument();
    expect(screen.getByText('$10.00')).toBeInTheDocument();
  });

  it('should show empty when loading', () => {
    const loadingState = {
      transactionAdjustment: {
        currentRequest: TRANSACTION_STATUS.PENDING,
        isLoading: true,
        isError: false,
        isLoadingView: false,
        openModal: false,
        sortBy: { id: GRID_COLUMN_NAME.OPERATION_DATE_TIME },
        transactionAdjustmentRequestList: []
      }
    };
    const { wrapper } = renderMockStoreId(<List />, {
      initialState: loadingState
    });

    expect(wrapper.baseElement.innerHTML).toEqual('<div></div>');
  });

  it('should show empty when loading', () => {
    const loadingState = {
      transactionAdjustment: {
        currentRequest: TRANSACTION_STATUS.PENDING,
        isLoading: true,
        isError: false,
        isLoadingView: false,
        openModal: false,
        sortBy: { id: GRID_COLUMN_NAME.OPERATION_DATE_TIME },
        transactionAdjustmentRequestList: []
      }
    };
    const { wrapper } = renderMockStoreId(<List />, {
      initialState: loadingState
    });

    expect(wrapper.baseElement.innerHTML).toEqual('<div></div>');
  });

  it('should show error when error occurs', () => {
    // Arrange
    const errorState = {
      transactionAdjustment: {
        currentRequest: TRANSACTION_STATUS.PENDING,
        isLoading: false,
        isError: true,
        isLoadingView: false,
        openModal: false,
        sortBy: { id: GRID_COLUMN_NAME.OPERATION_DATE_TIME },
        transactionAdjustmentRequestList: []
      }
    };

    renderMockStoreId(<List />, {
      initialState: errorState
    });

    expect(
      screen.getByText('txt_data_load_unsuccessful_click_reload_to_try_again')
    ).toBeInTheDocument();
  });

  it('should call reload function', () => {
    const mockAction = transactionAdjustmentActionSpy(
      'getTransactionAdjustmentRequest'
    );

    const errorState = {
      transactionAdjustment: {
        currentRequest: TRANSACTION_STATUS.PENDING,
        isLoading: false,
        isError: true,
        isLoadingView: false,
        openModal: false,
        sortBy: { id: GRID_COLUMN_NAME.OPERATION_DATE_TIME },
        transactionAdjustmentRequestList: []
      }
    };
    renderMockStoreId(<List />, {
      initialState: errorState
    });
    const reloadButton = screen.getByRole('button');
    reloadButton.click();

    expect(mockAction).toBeCalled();
  });

  it('should call sort function', () => {
    const mockAction = transactionAdjustmentActionSpy('setPendingSortBy');

    renderMockStoreId(<List />, {
      initialState
    });
    const column = screen.getByText('txt_operator_id');
    userEvent.click(column);

    expect(mockAction).toBeCalled();
  });

  describe('Should render with sortBy', () => {
    const mapState = (sortBy: SortType) => {
      return {
        transactionAdjustment: {
          currentRequest: TRANSACTION_STATUS.PENDING as TRANSACTION_STATUS,
          isLoading: false,
          isError: false,
          isLoadingView: false,
          openModal: false,
          sortBy,
          transactionAdjustmentRequestList: [
            {
              operatorId: 'operatorId',
              date: '04/01/2021',
              time: '08:00',
              accNbr: { eValue: storeId, maskedValue: 'string' },
              batch: 'batch',
              transactionType: 'transactionType',
              amount: '9'
            },
            {
              operatorId: 'operatorId',
              date: '04/02/2021',
              time: '08:00',
              accNbr: { eValue: storeId, maskedValue: 'string' },
              batch: 'batch',
              transactionType: 'transactionType',
              amount: '10'
            },
            {
              operatorId: 'operatorId',
              date: '04/02/2021',
              time: '08:00',
              accNbr: { eValue: storeId, maskedValue: 'string' },
              batch: 'batch',
              transactionType: 'transactionType'
            }
          ]
        }
      } as Partial<RootState>;
    };

    it('should order amount data', () => {
      const orderState: Partial<RootState> = mapState({
        id: GRID_COLUMN_NAME.AMOUNT,
        order: 'desc'
      });

      renderMockStoreId(<List />, {
        initialState: orderState
      });

      expect(screen.getByText(/[$]10.00/)).toBeInTheDocument();
      expect(screen.getByText(/[$]9.00/)).toBeInTheDocument();
    });

    it('should order id data', () => {
      const orderState: Partial<RootState> = mapState({
        id: GRID_COLUMN_NAME.OPERATOR_ID,
        order: 'desc'
      });

      const { wrapper } = renderMockStoreId(<List />, {
        initialState: orderState
      });

      expect(wrapper.container.querySelector('.dls-grid')).toBeInTheDocument();
    });

    it('should order accNbr data', () => {
      const orderState: Partial<RootState> = mapState({
        id: GRID_COLUMN_NAME.ACC_NBR,
        order: 'desc'
      });

      const { wrapper } = renderMockStoreId(<List />, {
        initialState: orderState
      });

      expect(wrapper.container.querySelector('.dls-grid')).toBeInTheDocument();
    });
  });

  it('should open modal', () => {
    const mockAction = transactionAdjustmentActionSpy('updateOpenModal');

    renderMockStoreId(<List />, { initialState: orderState });

    screen.getAllByText(I18N_TXN_ADJ.VIEW)[0].click();

    expect(mockAction).toBeCalled();
  });

  it('should open modal', () => {
    const mockAction = transactionAdjustmentActionSpy('updateOpenModal');
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: { commonConfig: { isSupervisor: true, org: 'org', app: 'app' } }
    });

    renderMockStoreId(<List />, { initialState: otherState });

    screen.getByText(I18N_TXN_ADJ.REVIEW).click();

    expect(mockAction).toBeCalled();
  });

  it('handleLoadMore', () => {
    mockScrollHeight(1000);

    const { wrapper } = renderMockStoreId(<List />, {
      initialState: loadMoreState
    });
    expect(wrapper.container.querySelector('.dls-grid')).toBeInTheDocument();
    expect(screen.getByText(I18N_TXN_ADJ.BACK_TO_TOP)).toBeInTheDocument();
  });

  it('handleScrollToTop', () => {
    const scrollTo = jest.fn();

    mockScrollToFn(scrollTo);

    jest.useFakeTimers();
    renderMockStoreId(<List />, { initialState: loadMoreState });

    act(() => {
      userEvent.click(screen.getByText(I18N_TXN_ADJ.BACK_TO_TOP));
      jest.runAllTimers();
    });

    expect(scrollTo).toBeCalled();
  });

  describe('handleLoadMore', () => {
    it('when has load more', () => {
      const mockAction = transactionAdjustmentActionSpy(
        'getMoreTransactionAdjustmentRequest'
      );

      mockScrollHeight(1000);
      mockScrollTop(1000);

      const { wrapper } = renderMockStoreId(<List />, {
        initialState: loadMoreState
      });

      act(() => {
        fireEvent.scroll(wrapper.container.querySelector('.dls-grid-body')!, {
          target: {},
          currentTarget: { scrollTop: 1000 }
        });
      });

      expect(mockAction).toBeCalled();
    });

    it('when do not has load more', () => {
      const mockAction = transactionAdjustmentActionSpy(
        'getMoreTransactionAdjustmentRequest'
      );

      mockScrollHeight(1000);
      mockScrollTop(1000);

      const { wrapper } = renderMockStoreId(<List />, {
        initialState
      });

      act(() => {
        fireEvent.scroll(wrapper.container.querySelector('.dls-grid-body')!, {
          target: {},
          currentTarget: { scrollTop: 1000 }
        });
      });

      expect(mockAction).not.toBeCalled();
    });
  });
});
