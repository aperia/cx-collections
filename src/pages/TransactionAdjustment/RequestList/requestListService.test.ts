import { transactionAdjustmentRequestServices } from './requestListService';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import {
  ApproveAdjustmentRequestArg,
  GetTransactionAdjustmentRequestArg,
  RejectAdjustmentRequestArg
} from './types';

describe('transactionAdjustmentRequestServices', () => {
  describe('getTransactionAdjustmentRequest', () => {
    const params: GetTransactionAdjustmentRequestArg = {
      type: 'pending',
      startSequence: 13,
      endSequence: 123
    };

    it('when params type is pending', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      transactionAdjustmentRequestServices.getTransactionAdjustmentRequest(
        params
      );
      expect(mockService).toHaveBeenCalledWith(
        'fs/adjustments/v2/pendingQueue',
        { endSequence: '123', selectFields: ['*'], startSequence: '13' }
      );

      mockAppConfigApi.clear();

      transactionAdjustmentRequestServices.getTransactionAdjustmentRequest(
        params
      );
      expect(mockService).toHaveBeenCalledWith('', {
        endSequence: '123',
        selectFields: ['*'],
        startSequence: '13'
      });
    });

    it('when params type is approved', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      transactionAdjustmentRequestServices.getTransactionAdjustmentRequest({
        ...params,
        type: 'approved'
      });
      expect(mockService).toHaveBeenCalledWith(
        'fs/adjustments/v2/approvedQueue',
        { endSequence: '123', selectFields: ['*'], startSequence: '13' }
      );

      mockAppConfigApi.clear();

      transactionAdjustmentRequestServices.getTransactionAdjustmentRequest({
        ...params,
        type: 'approved'
      });
      expect(mockService).toHaveBeenCalledWith('', {
        endSequence: '123',
        selectFields: ['*'],
        startSequence: '13'
      });
    });

    it('when params type is rejected', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();
      transactionAdjustmentRequestServices.getTransactionAdjustmentRequest({
        ...params,
        type: 'rejected'
      });
      expect(mockService).toHaveBeenCalledWith(
        'fs/adjustments/v2/rejectedQueue',
        { endSequence: '123', selectFields: ['*'], startSequence: '13' }
      );

      mockAppConfigApi.clear();

      transactionAdjustmentRequestServices.getTransactionAdjustmentRequest({
        ...params,
        type: 'rejected'
      });
      expect(mockService).toHaveBeenCalledWith('', {
        endSequence: '123',
        selectFields: ['*'],
        startSequence: '13'
      });
    });

    it('when params type is empty', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      transactionAdjustmentRequestServices.getTransactionAdjustmentRequest({});

      expect(mockService).toHaveBeenCalledWith('', {
        endSequence: '10',
        selectFields: ['*'],
        startSequence: '1'
      });
    });
  });

  describe('approveAdjustment', () => {
    const params: ApproveAdjustmentRequestArg = {
      id: 'id'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      transactionAdjustmentRequestServices.approveAdjustment(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      transactionAdjustmentRequestServices.approveAdjustment(params);

      expect(mockService).toBeCalledWith(
        apiUrl.transactionAdjustmentRequest.approveAdjustment,
        params
      );
    });
  });

  describe('rejectAdjustment', () => {
    const params: RejectAdjustmentRequestArg = {
      id: 'id'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      transactionAdjustmentRequestServices.rejectAdjustment(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      transactionAdjustmentRequestServices.rejectAdjustment(params);

      expect(mockService).toBeCalledWith(
        apiUrl.transactionAdjustmentRequest.rejectAdjustment,
        params
      );
    });
  });
});
