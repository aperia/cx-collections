import { useCallback, useEffect, useRef, useState } from 'react';

// types
import { TimePickerValue } from "app/_libraries/_dls/components/TimePicker";
import { ErrorControlValidation } from "../PayByPhone/types";

// constants
import { I18N_FUNCTION_RULE_MAPPING } from "./constants";
import { InputError, TTouch } from './types';

export const getHourTimePicker = (hour: string, meridiem: string): number => {
  if (!hour || !meridiem) return 0;

  if (hour === '12' && meridiem === 'AM') return 0;

  if (hour !== '12' && meridiem === 'PM') return Number(hour) + 12;

  return Number(hour)
}

export const getEffectiveTimeString = (hour: string, minute: number, meridiem: string): string => {
  const efftiveHour = getHourTimePicker(hour, meridiem);

  return `${efftiveHour > 9 ? '' : '0'}${efftiveHour}:${minute > 9 ? '' : '0'}${minute}:00`
}

export const getEffectiveTimeErrors = (effectiveDate: Date, effectiveTime: TimePickerValue, t: (s: string) => void) => {
  let statusError = false;
  if (effectiveDate && effectiveTime) {
    const currentDate = new Date();

    const meridiem = effectiveTime?.meridiem;

    const hour = getHourTimePicker(
      effectiveTime?.hour || '',
      meridiem || ''
    );

    const minute = Number(effectiveTime?.minute);

    effectiveDate.setHours(hour);

    effectiveDate.setMinutes(minute);

    effectiveDate.setSeconds(0);

    currentDate.setMilliseconds(0);

    statusError = effectiveDate.getTime() <= currentDate.getTime();
  }
  return {
    effectiveTime: {
      status: statusError,
      message: t(I18N_FUNCTION_RULE_MAPPING.EFFECTIVE_TIME_ERROR)
    }
  } as Record<'effectiveTime', ErrorControlValidation>;
}

export const getEffectiveDateErrors = (effectiveDate: Date, t: (s: string) => void) => {
  let statusError = false;
  if (effectiveDate) {
    const currentDate = new Date();

    currentDate.setHours(0);

    currentDate.setMinutes(0);

    currentDate.setSeconds(0);

    currentDate.setMilliseconds(0);

    statusError = effectiveDate.getTime() > currentDate.getTime();
  }
  return {
    effectiveDate: {
      status: !statusError,
      message: t(I18N_FUNCTION_RULE_MAPPING.EFFECTIVE_DATE_ERROR)
    }
  } as Record<'effectiveDate', ErrorControlValidation>;
}

export const useFormValidations = <TFields extends string>(
  currentErrors: Record<TFields, InputError>,
  changes: number,
  ...defaultTouchesParam: TFields[]
)
  : [Record<TFields, TTouch>, Record<TFields, InputError>, (field: TFields, blur?: boolean) => (() => void)] => {

  const defaultTouches = useRef<TFields[]>(defaultTouchesParam);
  const [touches, setTouches] = useState<Record<TFields, TTouch>>(
    defaultTouches.current.reduceRight(
      (pre, curr) => ({ ...pre, [curr]: { touched: true, blur: true } }),
      {} as Record<TFields, TTouch>
    )
  );
  const [errors, setErrors] = useState<Record<TFields, InputError>>({} as Record<TFields, InputError>);

  useEffect(() => {
    setTouches(defaultTouches.current.reduceRight(
      (pre, curr) => ({ ...pre, [curr]: { touched: true, blur: true } }),
      {} as Record<TFields, TTouch>)
    );
    setErrors({} as Record<TFields, InputError>);
  }, [defaultTouches, changes]);

  useEffect(() => {
    const touchesErrors = Object.keys(touches)
      .map(k => k as TFields)
      .filter(k => touches[k].blur)
      .reduceRight((pre, curr) => ({ ...pre, [curr]: currentErrors[curr] }), {}) as Record<TFields, InputError>;

    setErrors(errors => ({ ...errors, ...touchesErrors }));
  }, [touches, currentErrors]);

  const setTouched = useCallback((field: TFields, blur?: boolean): () => void => {
    const handleTouched = () => {
      setTouches(touches => {
        const touch = (touches[field] && { ...touches[field] }) || { touched: false, blur: false };
        if (blur && touch.blur === false) {
          touch.blur = true;
          touch.touched = true;
        }
        if (!blur && touch.blur === true) touch.blur = false;

        const isChange = touches[field]?.blur !== touch.blur;
        return !isChange ? touches : { ...touches, [field]: touch }
      });
    };

    return handleTouched;
  }, []);

  return [touches, errors, setTouched];
};
