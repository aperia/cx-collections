import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { fireEvent, screen } from '@testing-library/react';
import { AddFunctionRuleMapping } from './AddFunctionRuleMapping';
import { I18N_FUNCTION_RULE_MAPPING } from './constants';
import userEvent from '@testing-library/user-event';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// hooks
import * as useFunctionRule from '../hooks/useFunctionRuleMappingForm';

// redux
import { functionRuleMappingActions } from './_redux/reducers';

import { SubmitFunctionRuleMappingArgs } from './types';
import { act } from 'react-dom/test-utils';
import { DatePickerProps } from 'app/_libraries/_dls/components';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');
  return {
    ...dlsComponents,
    DatePicker: ({ dateTooltip }: DatePickerProps) => (
      <>
        <input
          data-testid="DatePicker_dateTooltip"
          onChange={(e: any) => dateTooltip!(e.target.date as Date)}
        />
      </>
    )
  };
});

const initialState: Partial<RootState> = {
  functionRuleMapping: {
    functionRuleMappingList: {
      loading: false,
      data: [
        { currentRuleName: 'name 1', functionCode: 'code_1' },
        { currentRuleName: 'name 2', functionCode: 'code_2' }
      ],
      sortBy: { id: 'currentRuleName', order: 'asc' }
    },
    functionRuleMappingForm: {
      loading: false,
      openAddModal: true,
      openEditModal: true,
      refData: [{ code: 'code_1', text: 'text_1' }, { text: 'text_2' }]
    }
  }
};

const spyFunctionRuleMappingActions = mockActionCreator(
  functionRuleMappingActions
);

beforeEach(() => {
  spyFunctionRuleMappingActions('closeModalAddFunctionRuleMapping');
});

const mappingForm = {
  handleChangeDropDownList: jest.fn(),
  handleChangeTextBoxWithoutAlpahnumber: jest.fn(),
  values: {},
  setFieldValue: jest.fn(),
  setValues: jest.fn(),
  handleReset: jest.fn(),
  handleChange: jest.fn(),
  handleBlur: jest.fn(),
  validateForm: jest.fn(),
  handleSubmit: jest.fn(),
  errors: {},
  touched: {}
};

describe('Render', () => {
  it('Should render with empty state', () => {
    renderMockStore(<AddFunctionRuleMapping />, {});

    expect(
      screen.queryByText(I18N_FUNCTION_RULE_MAPPING.RULE_NAME_DESCRIPTION)
    ).toBeNull();
  });

  it('Should render', () => {
    renderMockStore(<AddFunctionRuleMapping />, {
      initialState
    });

    expect(
      screen.getByText(I18N_FUNCTION_RULE_MAPPING.RULE_NAME_DESCRIPTION)
    ).toBeInTheDocument();
  });

  it('Should render with error', () => {
    jest
      .spyOn(useFunctionRule, 'useFunctionRuleMappingForm')
      .mockImplementation(() => ({
        ...mappingForm,
        errors: { functionName: 'error', effectiveDate: 'invalid date' }
      }));

    renderMockStore(<AddFunctionRuleMapping />, {
      initialState
    });

    expect(
      screen.getByText(I18N_FUNCTION_RULE_MAPPING.RULE_NAME_DESCRIPTION)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  beforeEach(() => {
    jest
      .spyOn(useFunctionRule, 'useFunctionRuleMappingForm')
      .mockImplementation(() => mappingForm);
  });

  it('handleCancelModal', () => {
    const mockAction = spyFunctionRuleMappingActions(
      'closeModalAddFunctionRuleMapping'
    );

    renderMockStore(<AddFunctionRuleMapping />, {
      initialState
    });

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));

    expect(mockAction).toBeCalled();
  });

  describe('onSubmit', () => {
    it('submit undefined values', () => {
      jest
        .spyOn(useFunctionRule, 'useFunctionRuleMappingForm')
        .mockImplementation((onSubmit: Function) => ({
          ...mappingForm,
          handleSubmit: () => onSubmit(undefined)
        }));

      const mockAction = spyFunctionRuleMappingActions(
        'triggerAddFunctionRuleMapping'
      );

      renderMockStore(<AddFunctionRuleMapping />, {
        initialState
      });

      act(() => {
        userEvent.click(screen.getByText(I18N_COMMON_TEXT.SUBMIT));
      });

      expect(mockAction).not.toBeCalled();
    });

    it('submit empty values', () => {
      jest
        .spyOn(useFunctionRule, 'useFunctionRuleMappingForm')
        .mockImplementation((onSubmit: Function) => ({
          ...mappingForm,
          handleSubmit: () => onSubmit({})
        }));

      const mockAction = spyFunctionRuleMappingActions(
        'triggerAddFunctionRuleMapping'
      );

      renderMockStore(<AddFunctionRuleMapping />, {
        initialState
      });

      fireEvent.change(screen.getByTestId('DatePicker_dateTooltip'), {
        target: { value: 'value', date: new Date('01/01/1901') }
      });

      act(() => {
        userEvent.click(screen.getByText(I18N_COMMON_TEXT.SUBMIT));
      });

      expect(mockAction).toHaveBeenCalled();
    });

    it('submit valid values', () => {
      jest
        .spyOn(useFunctionRule, 'useFunctionRuleMappingForm')
        .mockImplementation((onSubmit: Function) => ({
          ...mappingForm,
          handleSubmit: () =>
            onSubmit({
              effectiveDate: new Date('01/01/2999')
            })
        }));

      const mockAction = jest.fn();

      spyFunctionRuleMappingActions(
        'triggerAddFunctionRuleMapping',
        (params: SubmitFunctionRuleMappingArgs) => {
          params.callback!();
          mockAction();
          return { type: 'type' };
        }
      );

      renderMockStore(<AddFunctionRuleMapping />, {
        initialState
      });

      fireEvent.change(screen.getByTestId('DatePicker_dateTooltip'), {
        target: { value: 'value', date: new Date('01/01/2901') }
      });

      act(() => {
        userEvent.click(screen.getByText(I18N_COMMON_TEXT.SUBMIT));
      });

      expect(mockAction).toBeCalled();
    });

    it('submit invalid values', () => {
      jest
        .spyOn(useFunctionRule, 'useFunctionRuleMappingForm')
        .mockImplementation((onSubmit: Function) => ({
          ...mappingForm,
          handleSubmit: () =>
            onSubmit({
              effectiveDate: new Date('01/01/1990')
            })
        }));

      const mockAction = jest.fn();

      spyFunctionRuleMappingActions(
        'triggerAddFunctionRuleMapping',
        (params: SubmitFunctionRuleMappingArgs) => {
          params.callback!();
          mockAction();
          return { type: 'type' };
        }
      );

      renderMockStore(<AddFunctionRuleMapping />, {
        initialState
      });

      fireEvent.change(screen.getByTestId('DatePicker_dateTooltip'), {
        target: { value: 'value', date: new Date('01/01/2901') }
      });

      act(() => {
        userEvent.click(screen.getByText(I18N_COMMON_TEXT.SUBMIT));
      });

      expect(mockAction).not.toBeCalled();
    });
  });
});
