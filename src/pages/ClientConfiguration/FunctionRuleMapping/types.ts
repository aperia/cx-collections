import { SortType } from "app/_libraries/_dls/components";
export interface FunctionRuleMappingItem {
  functionCode?: string;
  functionText?: string;
  currentRuleName?: string;
  currentRuleVersion?: string;
  nextRuleName?: string;
  nextRuleVersion?: string;
  effectiveDate?: string;
}
export interface FunctionRuleMappingForm {
  functionName?: FunctionName;
  currentRuleName?: string;
  currentRuleVersion?: string;
  nextRuleName?: string;
  nextRuleVersion?: string;
  effectiveDate?: Date;
}

export interface FunctionRuleMappingState {
  functionRuleMappingList: {
    loading: boolean;
    data: FunctionRuleMappingItem[];
    error?: string;
    sortBy: SortType;
  };
  functionRuleMappingForm: {
    loading: boolean;
    data?: FunctionRuleMappingItem;
    refData?: FunctionName[];
    openAddModal: boolean;
    openEditModal: boolean;
    error?: string;
  };
}

export interface FunctionName {
  code?: string;
  text?: string;
}

export interface SubmitFunctionRuleMappingArgs {
  data?: FunctionRuleMappingItem;
  callback?: () => void;
}

export interface TTouch {
  touched: boolean;
  blur: boolean;
}

export interface InputError {
  status: boolean;
  message: string;
}

export type TFunctionRuleMappingProperties = keyof FunctionRuleMappingForm;