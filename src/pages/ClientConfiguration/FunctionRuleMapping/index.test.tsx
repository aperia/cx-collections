import React from 'react';
import { renderMockStore } from 'app/test-utils';
import { screen } from '@testing-library/react';
import FunctionRuleMapping from '.';
import { I18N_FUNCTION_RULE_MAPPING } from './constants';

jest.mock('./FunctionRuleMappingGrid.tsx', () => () => (
  <div>FunctionRuleMappingGrid</div>
));

const initialState: Partial<RootState> = {
  functionRuleMapping: {
    functionRuleMappingList: { loading: false, data: [] },
    functionRuleMappingForm: {
      loading: false,
      openAddModal: true,
      openEditModal: true
    }
  }
};

describe('Render', () => {
  it('Should render', () => {
    renderMockStore(<FunctionRuleMapping />, { initialState });

    expect(
      screen.getByText(I18N_FUNCTION_RULE_MAPPING.FUNCTION_RULE_MAPPING)
    ).toBeInTheDocument();
  });
});
