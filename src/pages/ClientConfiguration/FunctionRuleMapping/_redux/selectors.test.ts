import * as selectors from './selectors';
import { selectorWrapper } from 'app/test-utils';

const initialState: Partial<RootState> = {
  functionRuleMapping: {
    functionRuleMappingList: {
      loading: true,
      data: [
        { currentRuleName: 'name 1', functionCode: 'code_1' },
        { currentRuleName: 'name 2', functionCode: 'code_2' }
      ],
      error: 'error',
      sortBy: { id: 'currentRuleName', order: null }
    },
    functionRuleMappingForm: {
      loading: true,
      error: 'error',
      openAddModal: true,
      openEditModal: true,
      data: {},
      refData: []
    }
  }
};

const triggerSelector = selectorWrapper(initialState);

describe('Selector test', () => {
  it('selectFunctionRuleMappingListLoading', () => {
    const { data, emptyData } = triggerSelector(
      selectors.selectFunctionRuleMappingListLoading
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectFunctionRuleMappingListData', () => {
    const { data, emptyData } = triggerSelector(
      selectors.selectFunctionRuleMappingListData
    );

    expect(data).toEqual([
      { currentRuleName: 'name 1', functionCode: 'code_1' },
      { currentRuleName: 'name 2', functionCode: 'code_2' }
    ]);
    expect(emptyData).toEqual([]);
  });

  it('selectFunctionRuleMappingListError', () => {
    const { data, emptyData } = triggerSelector(
      selectors.selectFunctionRuleMappingListError
    );

    expect(data).toEqual(
      initialState.functionRuleMapping!.functionRuleMappingList.error
    );
    expect(emptyData).toEqual(undefined);
  });

  it('selectFunctionRuleMappingFormLoading', () => {
    const { data, emptyData } = triggerSelector(
      selectors.selectFunctionRuleMappingFormLoading
    );

    expect(data).toEqual(
      initialState.functionRuleMapping!.functionRuleMappingForm.loading
    );
    expect(emptyData).toEqual(false);
  });

  it('selectFunctionRuleMappingData', () => {
    const { data, emptyData } = triggerSelector(
      selectors.selectFunctionRuleMappingData
    );

    expect(data).toEqual(
      initialState.functionRuleMapping!.functionRuleMappingForm.data
    );
    expect(emptyData).toEqual(undefined);
  });

  it('selectFunctionRuleMappingRefData', () => {
    const { data, emptyData } = triggerSelector(
      selectors.selectFunctionRuleMappingRefData
    );

    expect(data).toEqual(
      initialState.functionRuleMapping!.functionRuleMappingForm.refData
    );
    expect(emptyData).toEqual(undefined);
  });

  it('selectOpenAddModal', () => {
    const { data, emptyData } = triggerSelector(selectors.selectOpenAddModal);

    expect(data).toEqual(
      initialState.functionRuleMapping!.functionRuleMappingForm.openAddModal
    );
    expect(emptyData).toEqual(false);
  });

  it('selectOpenEditModal', () => {
    const { data, emptyData } = triggerSelector(selectors.selectOpenEditModal);

    expect(data).toEqual(
      initialState.functionRuleMapping!.functionRuleMappingForm.openEditModal
    );
    expect(emptyData).toEqual(false);
  });
});
