import { ActionReducerMapBuilder, createAsyncThunk, isFulfilled } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

// types
import {
  FunctionRuleMappingItem,
  FunctionRuleMappingState
} from '../types';

export const getFunctionRuleMapping = createAsyncThunk<
  FunctionRuleMappingItem[],
  undefined,
  ThunkAPIConfig
>(
  'functionRuleMapping/getFunctionRuleMapping',
  async (args, thunkAPI) => {
    const { dispatch, getState } = thunkAPI;
    const response = await dispatch(
      clientConfigurationActions.getClientInfoData({
        dataType: 'rulesetsConfig',
        fieldsToQuery: "rulesets"
      })
    );

    if (isFulfilled(response)) {
      const ruleMappingModel =
        getState().mapping.data?.clientConfigFunctionAndRuleMapping || {};

      const data = response.payload.rulesetsConfig || [];
      return mappingDataFromArray(data, ruleMappingModel);
    }

    return thunkAPI.rejectWithValue({});
  }
);

export const getFunctionRuleMappingBuilder = (
  builder: ActionReducerMapBuilder<FunctionRuleMappingState>
) => {
  builder
    .addCase(getFunctionRuleMapping.pending, draftState => {
      draftState.functionRuleMappingList = {
        ...draftState.functionRuleMappingList,
        loading: true,
        data: []
      };
    })
    .addCase(getFunctionRuleMapping.fulfilled, (draftState, action) => {
      draftState.functionRuleMappingList = {
        ...draftState.functionRuleMappingList,
        loading: false,
        data: action.payload,
        error: ''
      };
    })
    .addCase(getFunctionRuleMapping.rejected, (draftState, action) => {
      draftState.functionRuleMappingList = {
        ...draftState.functionRuleMappingList,
        loading: false,
        error: action.error.message
      };
    });
};
