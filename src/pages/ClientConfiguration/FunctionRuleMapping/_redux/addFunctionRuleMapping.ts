import {
  createAsyncThunk, isRejected,
} from '@reduxjs/toolkit';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';


// type
import { FunctionRuleMappingItem } from '../types';

export const addFunctionRuleMapping = createAsyncThunk<
  undefined,
  FunctionRuleMappingItem,
  ThunkAPIConfig
>(
  'functionRuleMapping/addFunctionRuleMapping',
  async (args, thunkAPI) => {
    const { dispatch, rejectWithValue } = thunkAPI;

    const getClientInfoResponse = await dispatch(
      clientConfigurationActions.getClientInfoData({
        dataType: 'rulesets',
      })
    );
    if (isRejected(getClientInfoResponse)) return rejectWithValue({});
    
    const newFunction = {
      name: args.functionCode!,
      description: args.functionText!,
      value: args.nextRuleName!,
      version: args.nextRuleVersion!,
    };
    const rulesetsData = getClientInfoResponse?.payload?.rulesets || [];

    const response = await dispatch(
      clientConfigurationActions.updateClientInfo({
        rulesets: [
          ...rulesetsData,
          newFunction
        ],
      })
    );

    if (isRejected(response)) return rejectWithValue({});

    return undefined;
  }
);
