import { createSelector } from '@reduxjs/toolkit';
import { SortType } from 'app/_libraries/_dls/components';
import { orderBy } from 'app/_libraries/_dls/lodash';
import { FunctionRuleMappingItem } from '../types';

const getFunctionRuleMappingList = (state: RootState) => {
  return state.functionRuleMapping.functionRuleMappingList;
};

const getFunctionRuleMappingForm = (state: RootState) => {
  return state.functionRuleMapping.functionRuleMappingForm;
};

export const getSortBy = (state: RootState): SortType => {
  return state.functionRuleMapping.functionRuleMappingList.sortBy;
};

export const selectFunctionRuleMappingListLoading = createSelector(
  getFunctionRuleMappingList,
  data => data?.loading
);

export const selectFunctionRuleMappingListData = createSelector(
  [getFunctionRuleMappingList, getSortBy],
  (data, sortBy) => {
    const sorted = orderBy(
      data.data,
      [
        (item: FunctionRuleMappingItem) => item[sortBy.id as keyof FunctionRuleMappingItem]?.toLowerCase(),
        item => item.functionText?.toLowerCase()
    ],
      [sortBy.order || false, 'asc']
    );

    return sorted;
  }
);

export const selectFunctionRuleMappingListError = createSelector(
  getFunctionRuleMappingList,
  data => data?.error
);

export const selectFunctionRuleMappingFormLoading = createSelector(
  getFunctionRuleMappingForm,
  data => data?.loading
);

export const selectFunctionRuleMappingData = createSelector(
  getFunctionRuleMappingForm,
  data => data?.data
);

export const selectFunctionRuleMappingRefData = createSelector(
  getFunctionRuleMappingForm,
  data => data?.refData
);

export const selectOpenAddModal = createSelector(
  getFunctionRuleMappingForm,
  data => data.openAddModal
);

export const selectOpenEditModal = createSelector(
  getFunctionRuleMappingForm,
  data => data.openEditModal
);