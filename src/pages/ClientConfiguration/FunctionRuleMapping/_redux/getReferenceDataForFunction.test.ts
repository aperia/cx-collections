import { createStore } from '@reduxjs/toolkit';
import { getReferenceDataForFunction } from './getReferenceDataForFunction';
import { functionRuleMappingService } from '../functionRuleMappingService';
import { rootReducer } from 'storeConfig';
import { responseDefault } from 'app/test-utils';

describe('Test getReferenceDataForFunction', () => {
  const store = createStore(rootReducer);

  const callApi = (isError = false) => {
    if (!isError) {
      jest
        .spyOn(functionRuleMappingService, 'getReferenceDataForFunction')
        .mockResolvedValue({ ...responseDefault, data: {} });
    } else {
      jest
        .spyOn(functionRuleMappingService, 'getReferenceDataForFunction')
        .mockRejectedValue(new Error('Async Error'));
    }

    return getReferenceDataForFunction()(store.dispatch, store.getState, {});
  };

  it('Should have return data', async () => {
    const response = await callApi();
    expect(response.payload).toEqual({});
  });

  it('Should have error', async () => {
    const response = await callApi(true);
    expect(response.payload).toEqual(undefined);
  });

  it('Should have fulfilled', async () => {
    const pendingAction = getReferenceDataForFunction.fulfilled(
      [],
      'clientConfigDebtCompanies/getCompanies',
      undefined
    );

    const actual = rootReducer(
      store.getState(),
      pendingAction
    ).functionRuleMapping;

    expect(actual.functionRuleMappingForm.loading).toEqual(false);
    expect(actual.functionRuleMappingForm.refData).toEqual([]);
  });

  it('Should have pending', async () => {
    const pendingAction = getReferenceDataForFunction.pending(
      'clientConfigDebtCompanies/getCompanies',
      undefined
    );

    const actual = rootReducer(
      store.getState(),
      pendingAction
    ).functionRuleMapping;

    expect(actual.functionRuleMappingForm.loading).toEqual(true);
    expect(actual.functionRuleMappingForm.refData).toEqual([]);
  });

  it('Should have reject', async () => {
    const pendingAction = getReferenceDataForFunction.rejected(
      null,
      'clientConfigDebtCompanies/getCompanies',
      undefined
    );

    const actual = rootReducer(
      store.getState(),
      pendingAction
    ).functionRuleMapping;

    expect(actual.functionRuleMappingForm.loading).toEqual(false);
  });
});
