import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { FunctionRuleMappingState } from '../types';
import { functionRuleMappingActions } from './reducers';

const functionRuleMapping: FunctionRuleMappingState = {
  functionRuleMappingList: {
    loading: true,
    data: [],
    error: 'error',
    sortBy: {
      id: 'test',
      order: 'desc'
    }
  },
  functionRuleMappingForm: {
    loading: true,
    error: 'error',
    openAddModal: true,
    openEditModal: true,
    data: {},
    refData: []
  }
};

describe('Test Reducer FunctionRuleMapping', () => {
  it('openModalAddFunctionRuleMapping', () => {
    const store = createStore(rootReducer, { functionRuleMapping });

    const { getState, dispatch } = store;

    dispatch(functionRuleMappingActions.openModalAddFunctionRuleMapping());

    const dataExpect = getState().functionRuleMapping.functionRuleMappingForm;

    expect(dataExpect.openAddModal).toEqual(true);
    expect(dataExpect.openEditModal).toEqual(false);
  });

  it('closeModalAddFunctionRuleMapping', () => {
    const store = createStore(rootReducer, { functionRuleMapping });

    const { getState, dispatch } = store;

    dispatch(functionRuleMappingActions.closeModalAddFunctionRuleMapping());

    const dataExpect = getState().functionRuleMapping.functionRuleMappingForm;

    expect(dataExpect.openAddModal).toEqual(false);
  });

  it('openModalEditFunctionRuleMapping', () => {
    const store = createStore(rootReducer, { functionRuleMapping });

    const { getState, dispatch } = store;

    dispatch(functionRuleMappingActions.openModalEditFunctionRuleMapping({}));

    const dataExpect = getState().functionRuleMapping.functionRuleMappingForm;

    expect(dataExpect.openAddModal).toEqual(false);
    expect(dataExpect.openEditModal).toEqual(true);
    expect(dataExpect.data).toEqual({});
  });

  it('closeModalEditFunctionRuleMapping', () => {
    const store = createStore(rootReducer, { functionRuleMapping });

    const { getState, dispatch } = store;

    dispatch(functionRuleMappingActions.closeModalEditFunctionRuleMapping());

    const dataExpect = getState().functionRuleMapping.functionRuleMappingForm;

    expect(dataExpect.openEditModal).toEqual(false);
    expect(dataExpect.data).toEqual(undefined);
  });

  it('onChangeSort', () => {
    const store = createStore(rootReducer, { functionRuleMapping });

    const { getState, dispatch } = store;

    dispatch(
      functionRuleMappingActions.onChangeSort({ id: 'id', order: 'asc' })
    );

    const dataExpect = getState().functionRuleMapping.functionRuleMappingList;

    expect(dataExpect.sortBy).toEqual({ id: 'id', order: 'asc' });
  });
});
