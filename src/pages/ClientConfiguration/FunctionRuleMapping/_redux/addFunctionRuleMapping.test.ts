import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { createStore, Store } from '@reduxjs/toolkit';
import { mockActionCreator } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import { addFunctionRuleMapping } from './addFunctionRuleMapping';

const spyClientConfigurationActions = mockActionCreator(
  clientConfigurationActions
);

const mockData = {
  functionCode: 'name',
  functionText: 'functionText',
  nextRuleName: 'nextRuleName',
  nextRuleVersion: 'nextRuleVersion',
  effectiveDate: 'effectiveDate'
};

describe('should have test addFunctionRuleMapping', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {});
  });

  it('Should response fulfilled', async () => {
    spyClientConfigurationActions('getClientInfoData').mockReturnValue({
      payload: { rulesets: [{ name: 'name' }, { name: 'name' }] },
      type: 'fulfilled',
      meta: { requestId: 'id', requestStatus: 'fulfilled' }
    });

    spyClientConfigurationActions('updateClientInfo').mockReturnValue({
      payload: {},
      type: 'fulfilled',
      meta: { requestId: 'id', requestStatus: 'fulfilled' }
    });

    const response = await addFunctionRuleMapping(mockData)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'functionRuleMapping/addFunctionRuleMapping/fulfilled'
    );
    expect(response.payload).toEqual(undefined);
  });

  it('Should response fulfilled, data undefined', async () => {
    spyClientConfigurationActions('getClientInfoData').mockReturnValue({
      payload: {},
      type: 'fulfilled',
      meta: { requestId: 'id', requestStatus: 'fulfilled' }
    });

    spyClientConfigurationActions('updateClientInfo').mockReturnValue({
      payload: {},
      type: 'fulfilled',
      meta: { requestId: 'id', requestStatus: 'fulfilled' }
    });

    const response = await addFunctionRuleMapping(mockData)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'functionRuleMapping/addFunctionRuleMapping/fulfilled'
    );
    expect(response.payload).toEqual(undefined);
  });

  it('Should response rejected getClientInfoData', async () => {
    spyClientConfigurationActions('getClientInfoData').mockReturnValue({
      payload: {},
      type: 'rejected',
      meta: { requestId: 'id', requestStatus: 'rejected' }
    });

    const response = await addFunctionRuleMapping({})(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'functionRuleMapping/addFunctionRuleMapping/rejected'
    );
    expect(response.payload).toEqual({});
  });

  it('Should response rejected updateClientInfo', async () => {
    spyClientConfigurationActions('getClientInfoData').mockReturnValue({
      payload: { rulesets: [{ name: 'name' }, { name: 'name' }] },
      type: 'fulfilled',
      meta: { requestId: 'id', requestStatus: 'fulfilled' }
    });

    spyClientConfigurationActions('updateClientInfo').mockReturnValue({
      payload: {},
      type: 'rejected',
      meta: { requestId: 'id', requestStatus: 'rejected' }
    });

    const response = await addFunctionRuleMapping({
      ...mockData,
      functionCode: 'functionCode'
    })(store.dispatch, store.getState, {});
    expect(response.type).toEqual(
      'functionRuleMapping/addFunctionRuleMapping/rejected'
    );
    expect(response.payload).toEqual({});
  });
});
