import { mockActionCreator, responseDefault } from 'app/test-utils';
import { triggerEditFunctionRuleMapping } from './triggerEditFunctionRuleMapping';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { I18N_FUNCTION_RULE_MAPPING } from '../constants';
import { clientConfigurationServices } from 'pages/ClientConfiguration/clientConfigurationServices';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

const data = {
  data: {
    functionCode: 'functionCode',
    functionText: 'functionText',
    currentRuleName: 'currentRuleName',
    currentRuleVersion: 'currentRuleVersion',
    nextRuleName: 'nextRuleName',
    nextRuleVersion: 'nextRuleVersion',
    effectiveDate: 'effectiveDate'
  },
  callback: jest.fn()
};

const actionsToastSpy = mockActionCreator(actionsToast);
const changeHistorySpy = mockActionCreator(changeHistoryActions);

describe('Test triggerEditFunctionRuleMapping', () => {
  it('Should have rejected', async () => {
    const spy = actionsToastSpy('addToast');
    const store = createStore(rootReducer, {}, applyMiddleware(thunk));
    const response = await triggerEditFunctionRuleMapping(data)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'functionRuleMapping/triggerEditFunctionRuleMapping/rejected'
    );
    expect(spy).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_FUNCTION_RULE_MAPPING.EDIT_FUNCTION_FAIL
    });
  });

  it('Should have rejected with empty data', async () => {
    const store = createStore(rootReducer, {}, applyMiddleware(thunk));
    const response = await triggerEditFunctionRuleMapping({
      data: null,
      callback: jest.fn()
    })(store.dispatch, store.getState, {});

    expect(response.type).toEqual(
      'functionRuleMapping/triggerEditFunctionRuleMapping/rejected'
    );
  });

  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      user: 'user'
    }
  };

  it('Should have fulfilled', async () => {
    const spy = changeHistorySpy('saveChangedClientConfig');
    jest
      .spyOn(clientConfigurationServices, 'getClientInfoById')
      .mockResolvedValueOnce({
        data: {
          responseStatus: 'success',
          clientInfo: {
            rulesets: [
              {
                name: 'functionCode',
                description: 'des',
                value: 'val',
                version: 'ver',
                effectiveDate: 'effectiveDate'
              },
              {
                name: 'functionCode',
                description: 'des',
                value: 'val',
                version: 'ver',
                effectiveDate: 'effectiveDate'
              }
            ]
          }
        }
      });

    jest
      .spyOn(clientConfigurationServices, 'addUpdateClientInfo')
      .mockResolvedValueOnce({
        ...responseDefault,
        data: {
          responseStatus: 'success'
        }
      });

    const store = createStore(
      rootReducer,
      {
        clientConfiguration: {
          settings: {
            selectedConfig: {
              id: 'id',
              agentId: 'agentId',
              clientId: 'clientId',
              systemId: 'systemId',
              principleId: 'principleId'
            }
          }
        },
        functionRuleMapping: {
          functionRuleMappingList: {
            data: [
              {
                functionCode: 'functionCode',
                functionText: 'functionText',
                currentRuleName: 'currentRuleName',
                currentRuleVersion: 'currentRuleVersion',
                nextRuleName: 'nextRuleName',
                nextRuleVersion: 'nextRuleVersion',
                effectiveDate: 'effectiveDate'
              },
              {
                functionCode: 'functionCode 1',
                functionText: 'functionText 2',
                currentRuleName: 'currentRuleName 3',
                currentRuleVersion: 'currentRuleVersion 4',
                nextRuleName: 'nextRuleName 5',
                nextRuleVersion: 'nextRuleVersion 6',
                effectiveDate: 'effectiveDate 7'
              }
            ]
          }
        }
      },
      applyMiddleware(thunk)
    );

    const response = await triggerEditFunctionRuleMapping(data)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'functionRuleMapping/triggerEditFunctionRuleMapping/fulfilled'
    );
    expect(spy).toBeCalledWith({
      action: 'UPDATE',
      changedCategory: 'functionRuleMapping',
      changedItem: {
        functionText: 'functionText'
      },
      newValue: {
        currentRuleName: 'currentRuleName',
        currentRuleVersion: 'currentRuleVersion',
        effectiveDate: 'effectiveDate',
        functionCode: 'functionCode',
        functionText: 'functionText',
        nextRuleName: 'nextRuleName',
        nextRuleVersion: 'nextRuleVersion'
      },
      oldValue: {
        currentRuleName: 'currentRuleName',
        currentRuleVersion: 'currentRuleVersion',
        effectiveDate: 'effectiveDate',
        functionCode: 'functionCode',
        functionText: 'functionText',
        nextRuleName: 'nextRuleName',
        nextRuleVersion: 'nextRuleVersion'
      }
    });
  });
});
