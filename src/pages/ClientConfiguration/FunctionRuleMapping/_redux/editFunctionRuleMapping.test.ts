import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { createStore, Store } from '@reduxjs/toolkit';
import { mockActionCreator } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import { editFunctionRuleMapping } from './editFunctionRuleMapping';

const spyClientConfigurationActions = mockActionCreator(
  clientConfigurationActions
);

const mockData = {
  functionCode: 'name',
  functionText: 'functionText',
  nextRuleName: 'nextRuleName',
  nextRuleVersion: 'nextRuleVersion',
  effectiveDate: 'effectiveDate'
};

describe('should have test editFunctionRuleMapping', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {});
  });

  it('Should response fulfilled', async () => {
    spyClientConfigurationActions('getClientInfoData').mockReturnValue({
      payload: { rulesets: [{ name: 'name' }, { name: 'name' }] },
      type: 'fulfilled',
      meta: { requestId: 'id', requestStatus: 'fulfilled' }
    });

    spyClientConfigurationActions('updateClientInfo').mockReturnValue({
      payload: {},
      type: 'fulfilled',
      meta: { requestId: 'id', requestStatus: 'fulfilled' }
    });

    const response = await editFunctionRuleMapping(mockData)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'functionRuleMapping/editFunctionRuleMapping/fulfilled'
    );
    expect(response.payload).toEqual(undefined);
  });

  it('Should response rejected, nextRuleExisted undefined', async () => {
    spyClientConfigurationActions('getClientInfoData').mockReturnValue({
      payload: { rulesets: [{ name: 'name' }] },
      type: 'fulfilled',
      meta: { requestId: 'id', requestStatus: 'fulfilled' }
    });

    const response = await editFunctionRuleMapping(mockData)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'functionRuleMapping/editFunctionRuleMapping/rejected'
    );
    expect(response.payload).toEqual(undefined);
  });

  it('Should response rejected, data undefined', async () => {
    spyClientConfigurationActions('getClientInfoData').mockReturnValue({
      payload: {},
      type: 'fulfilled',
      meta: { requestId: 'id', requestStatus: 'fulfilled' }
    });

    const response = await editFunctionRuleMapping({})(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'functionRuleMapping/editFunctionRuleMapping/rejected'
    );
    expect(response.payload).toEqual({});
  });

  it('Should response rejected', async () => {
    spyClientConfigurationActions('getClientInfoData').mockReturnValue({
      payload: { rulesets: [{ name: 'name' }, { name: 'name' }] },
      type: 'rejected',
      meta: { requestId: 'id', requestStatus: 'rejected' }
    });

    spyClientConfigurationActions('updateClientInfo').mockReturnValue({
      payload: {},
      type: 'rejected',
      meta: { requestId: 'id', requestStatus: 'rejected' }
    });

    const response = await editFunctionRuleMapping({
      ...mockData,
      functionCode: 'functionCode'
    })(store.dispatch, store.getState, {});
    expect(response.type).toEqual(
      'functionRuleMapping/editFunctionRuleMapping/rejected'
    );
    expect(response.payload).toEqual({});
  });

  it('Should response rejected, updateClientInfo rejected', async () => {
    spyClientConfigurationActions('getClientInfoData').mockReturnValue({
      payload: { rulesets: [{ name: 'name' }, { name: 'name' }] },
      type: 'fulfilled',
      meta: { requestId: 'id', requestStatus: 'fulfilled' }
    });

    spyClientConfigurationActions('updateClientInfo').mockReturnValue({
      payload: {},
      type: 'rejected',
      meta: { requestId: 'id', requestStatus: 'rejected' }
    });

    const response = await editFunctionRuleMapping(mockData)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'functionRuleMapping/editFunctionRuleMapping/rejected'
    );
    expect(response.payload).toEqual({});
  });
});
