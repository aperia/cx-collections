import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// services
import { functionRuleMappingService } from '../functionRuleMappingService';

// types
import {
  FunctionName,
  FunctionRuleMappingState
} from '../types';

export const getReferenceDataForFunction = createAsyncThunk<
  FunctionName[],
  undefined,
  ThunkAPIConfig
>(
  'functionRuleMapping/getReferenceDataForFunction',
  async (args, thunkAPI) => {

    const { data } = await functionRuleMappingService.getReferenceDataForFunction();

    return data;
  }
);

export const getReferenceDataForFunctionBuilder = (
  builder: ActionReducerMapBuilder<FunctionRuleMappingState>
) => {
  builder
    .addCase(getReferenceDataForFunction.pending, draftState => {
      draftState.functionRuleMappingForm = {
        ...draftState.functionRuleMappingForm,
        loading: true,
        refData: []
      };
    })
    .addCase(getReferenceDataForFunction.fulfilled, (draftState, action) => {
      draftState.functionRuleMappingForm = {
        ...draftState.functionRuleMappingForm,
        loading: false,
        refData: action.payload,
        error: ''
      };
    })
    .addCase(getReferenceDataForFunction.rejected, (draftState, action) => {
      draftState.functionRuleMappingForm = {
        ...draftState.functionRuleMappingForm,
        loading: false,
        error: action.error.message
      };
    });
};
