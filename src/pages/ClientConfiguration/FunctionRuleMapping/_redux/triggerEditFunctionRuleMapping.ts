import { FunctionRuleMappingItem } from './../types';
import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

// redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';

// constants
import { I18N_FUNCTION_RULE_MAPPING } from '../constants';

// types
import {
  FunctionRuleMappingState,
  SubmitFunctionRuleMappingArgs
} from '../types';
import { editFunctionRuleMapping } from './editFunctionRuleMapping';
import { getFunctionRuleMapping } from './getFunctionRuleMapping';

export const customDataForChangeHistory: string[] = [
  'functionText',
  'nextRuleName',
  'nextRuleVersion',
  'currentRuleName',
  'currentRuleVersion',
  'effectiveDate'
];

export const triggerEditFunctionRuleMapping = createAsyncThunk<
  unknown,
  SubmitFunctionRuleMappingArgs,
  ThunkAPIConfig
>(
  'functionRuleMapping/triggerEditFunctionRuleMapping',
  async (args, thunkAPI) => {
    const { dispatch, rejectWithValue, getState } = thunkAPI;

    const { data, callback } = args;
    if (!data) return rejectWithValue({});

    const response = await dispatch(editFunctionRuleMapping(data));

    //change history

    const previousData =
      getState().functionRuleMapping.functionRuleMappingList.data.find(
        (item: FunctionRuleMappingItem) =>
          item.functionText === data.functionText
      ) || {};

    if (isFulfilled(response)) {
      dispatch(
        changeHistoryActions.saveChangedClientConfig({
          action: 'UPDATE',
          changedCategory: 'functionRuleMapping',
          changedItem: {
            functionText: data?.functionText
          },
          oldValue: previousData,
          newValue: data
        })
      );
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_FUNCTION_RULE_MAPPING.EDIT_FUNCTION_SUCCESS
          })
        );
        dispatch(getFunctionRuleMapping());
      });
      typeof callback === 'function' && callback();
    }

    if (isRejected(response)) {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_FUNCTION_RULE_MAPPING.EDIT_FUNCTION_FAIL
        })
      );
      return rejectWithValue({});
    }

    return undefined;
  }
);

export const triggerEditFunctionRuleMappingBuilder = (
  builder: ActionReducerMapBuilder<FunctionRuleMappingState>
) => {
  builder
    .addCase(triggerEditFunctionRuleMapping.pending, (draftState, action) => {
      draftState.functionRuleMappingForm = {
        ...draftState.functionRuleMappingForm,
        loading: true
      };
    })
    .addCase(triggerEditFunctionRuleMapping.fulfilled, (draftState, action) => {
      draftState.functionRuleMappingForm = {
        ...draftState.functionRuleMappingForm,
        loading: false,
        error: '',
        openEditModal: false,
        data: undefined
      };
    })
    .addCase(triggerEditFunctionRuleMapping.rejected, (draftState, action) => {
      draftState.functionRuleMappingForm = {
        ...draftState.functionRuleMappingForm,
        loading: false,
        error: action.error.message
      };
    });
};
