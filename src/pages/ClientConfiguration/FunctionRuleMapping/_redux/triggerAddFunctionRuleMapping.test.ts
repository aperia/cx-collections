import { mockActionCreator, responseDefault } from 'app/test-utils';
import { triggerAddFunctionRuleMapping } from './triggerAddFunctionRuleMapping';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { I18N_FUNCTION_RULE_MAPPING } from '../constants';
import { clientConfigurationServices } from 'pages/ClientConfiguration/clientConfigurationServices';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

const data = {
  data: {
    functionCode: 'functionCode',
    functionText: 'functionText',
    currentRuleName: 'currentRuleName',
    currentRuleVersion: 'currentRuleVersion',
    nextRuleName: 'nextRuleName',
    nextRuleVersion: 'nextRuleVersion',
    effectiveDate: 'effectiveDate'
  },
  callback: jest.fn()
};

const actionsToastSpy = mockActionCreator(actionsToast);
const changeHistorySpy = mockActionCreator(changeHistoryActions);

describe('Test triggerAddFunctionRuleMapping', () => {
  it('Should have rejected', async () => {
    const spy = actionsToastSpy('addToast');
    const store = createStore(rootReducer, {}, applyMiddleware(thunk));
    const response = await triggerAddFunctionRuleMapping(data)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'functionRuleMapping/triggerAddFunctionRuleMapping/rejected'
    );
    expect(spy).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_FUNCTION_RULE_MAPPING.ADD_FUNCTION_FAIL
    });
  });

  it('Should have rejected with empty data', async () => {
    const store = createStore(rootReducer, {}, applyMiddleware(thunk));
    const response = await triggerAddFunctionRuleMapping({
      data: null,
      callback: jest.fn()
    })(store.dispatch, store.getState, {});

    expect(response.type).toEqual(
      'functionRuleMapping/triggerAddFunctionRuleMapping/rejected'
    );
  });

  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app'
    }
  };

  it('Should have fulfilled', async () => {
    const spy = changeHistorySpy('saveChangedClientConfig');
    jest
      .spyOn(clientConfigurationServices, 'getClientInfoById')
      .mockResolvedValueOnce({
        ...responseDefault,
        data: {
          responseStatus: 'success'
        }
      });

    jest
      .spyOn(clientConfigurationServices, 'addUpdateClientInfo')
      .mockResolvedValueOnce({
        ...responseDefault,
        data: {
          responseStatus: 'success'
        }
      });

    const store = createStore(
      rootReducer,
      {
        clientConfiguration: {
          settings: {
            selectedConfig: {
              id: 'id',
              agentId: 'agentId',
              clientId: 'clientId',
              systemId: 'systemId',
              principleId: 'principleId'
            }
          }
        }
      },
      applyMiddleware(thunk)
    );

    const response = await triggerAddFunctionRuleMapping(data)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'functionRuleMapping/triggerAddFunctionRuleMapping/fulfilled'
    );
    expect(spy).toBeCalledWith({
      action: 'ADD',
      changedCategory: 'functionRuleMapping',
      changedItem: {
        functionText: 'functionText'
      },
      newValue: {
        effectiveDate: 'effectiveDate',
        functionText: 'functionText',
        nextRuleName: 'nextRuleName',
        nextRuleVersion: 'nextRuleVersion'
      }
    });
  });
});
