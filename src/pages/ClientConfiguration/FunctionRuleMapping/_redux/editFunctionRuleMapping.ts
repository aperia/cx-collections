import {
  createAsyncThunk,
  isRejected
} from '@reduxjs/toolkit';

import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

// types
import { FunctionRuleMappingItem } from '../types';

export const editFunctionRuleMapping = createAsyncThunk<
  undefined,
  FunctionRuleMappingItem,
  ThunkAPIConfig
>(
  'functionRuleMapping/editFunctionRuleMapping',
  async (args, thunkAPI) => {
    const { dispatch, rejectWithValue } = thunkAPI;

    const getClientInfoResponse = await dispatch(
      clientConfigurationActions.getClientInfoData({
        dataType: 'rulesets',
      })
    );
    if (isRejected(getClientInfoResponse)) return rejectWithValue({});

    const nextRule = {
      name: args.functionCode!,
      description: args.functionText!,
      value: args.nextRuleName!,
      version: args.nextRuleVersion!,
      effectiveDate: args.effectiveDate!
    };
    const rulesetsData = getClientInfoResponse?.payload?.rulesets || [];

    const existRules = rulesetsData.filter(
      r => r.name == nextRule.name
    );
    if (existRules.length === 0) return rejectWithValue({});

    const nextRuleExisted = existRules.length >= 2 ? existRules[1] : undefined;

    let rulesets = [...rulesetsData, nextRule];
    if (nextRuleExisted) {
      nextRuleExisted.value = nextRule.value;
      nextRuleExisted.version = nextRule.version;
      nextRuleExisted.effectiveDate = nextRule.effectiveDate;
      rulesets = rulesetsData;
    }

    const response = await dispatch(
      clientConfigurationActions.updateClientInfo({ rulesets })
    );

    if (isRejected(response)) return rejectWithValue({});

    return undefined;
  }
);
