import { Dispatch } from 'react';
import { AsyncThunkAction, createStore } from '@reduxjs/toolkit';
import { getFunctionRuleMapping } from './getFunctionRuleMapping';
import { functionRuleMappingService } from '../functionRuleMappingService';
import { rootReducer } from 'storeConfig';
import { responseDefault } from 'app/test-utils';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

describe('Test getFunctionRuleMapping', () => {
  const store = createStore(rootReducer);

  const callApi = (isError = false) => {
    if (!isError) {
      jest
        .spyOn(functionRuleMappingService, 'getFunctionRuleMapping')
        .mockResolvedValue({ ...responseDefault, data: { data: {} } });
    } else {
      jest
        .spyOn(functionRuleMappingService, 'getFunctionRuleMapping')
        .mockRejectedValue(new Error('Async Error'));
    }

    return getFunctionRuleMapping()(store.dispatch, store.getState, {});
  };

  it('Should have return data', async () => {
    const response = await callApi();
    expect(response.payload).toEqual(undefined);
  });

  it('Should have error', async () => {
    const response = await callApi(true);
    expect(response.payload).toEqual(undefined);
  });

  it('Should have fulfilled', async () => {
    const pendingAction = getFunctionRuleMapping.fulfilled(
      [],
      'clientConfigDebtCompanies/getCompanies',
      undefined
    );

    const actual = rootReducer(
      store.getState(),
      pendingAction
    ).functionRuleMapping;

    expect(actual.functionRuleMappingList.loading).toEqual(false);
    expect(actual.functionRuleMappingList.data).toEqual([]);
    expect(actual.functionRuleMappingList.error).toEqual('');
  });

  it('Should have pending', async () => {
    const pendingAction = getFunctionRuleMapping.pending(
      'clientConfigDebtCompanies/getCompanies',
      undefined
    );

    const actual = rootReducer(
      store.getState(),
      pendingAction
    ).functionRuleMapping;

    expect(actual.functionRuleMappingList.loading).toEqual(true);
    expect(actual.functionRuleMappingList.data).toEqual([]);
  });

  it('Should have reject', async () => {
    const pendingAction = getFunctionRuleMapping.rejected(
      null,
      'clientConfigDebtCompanies/getCompanies',
      undefined
    );

    const actual = rootReducer(
      store.getState(),
      pendingAction
    ).functionRuleMapping;

    expect(actual.functionRuleMappingList.loading).toEqual(false);
  });
});

describe('getFunctionRuleMapping', () => {
  // it('dispatch a add function rules mapping request');

  describe('when get function rules mapping success', () => {
    let dispatch: Dispatch<any>;
    let getState: any;
    let action: AsyncThunkAction<unknown, any, {}>;

    beforeEach(() => {
      dispatch = jest.fn(cb => cb);
      getState = jest.fn();
      action = getFunctionRuleMapping();
    });

    test('dispatches success with ruleMappingModel and data exist', async () => {
      getState.mockReturnValue({
        mapping: {
          data: {
            clientConfigFunctionAndRuleMapping: {
              inputKy: 'test key'
            }
          }
        }
      });
      jest
        .spyOn(clientConfigurationActions, 'getClientInfoData')
        .mockResolvedValue({
          payload: {
            rulesetsConfig: [
              {
                inputKy: 'code_1'
              }
            ]
          },
          type: 'functionRuleMapping/editFunctionRuleMapping/fulfilled',
          meta: {
            arg: null,
            requestId: 'test request id',
            requestStatus: 'fulfilled'
          }
        } as never);

      const res = await action(dispatch, getState, undefined);

      expect(res.payload).toEqual([{ inputKy: undefined }]);
      expect(dispatch).toBeCalledTimes(3);
    });

    test('dispatches success with ruleMappingModel and data empty', async () => {
      getState.mockReturnValue({
        mapping: {
          data: {}
        }
      });
      jest
        .spyOn(clientConfigurationActions, 'getClientInfoData')
        .mockResolvedValue({
          payload: {},
          type: 'functionRuleMapping/editFunctionRuleMapping/fulfilled',
          meta: {
            arg: null,
            requestId: 'test request id',
            requestStatus: 'fulfilled'
          }
        } as never);

      const res = await action(dispatch, getState, undefined);

      expect(res.payload).toEqual([]);
      expect(dispatch).toBeCalledTimes(3);
    });
  });

  describe('when get function rules mapping error', () => {
    let dispatch: Dispatch<any>;
    let getState: any;
    let action: AsyncThunkAction<unknown, any, {}>;

    beforeEach(() => {
      dispatch = jest.fn(cb => cb);
      getState = jest.fn();
      action = getFunctionRuleMapping();
    });

    test('dispatches error when data return with status rejected', async () => {
      jest
        .spyOn(clientConfigurationActions, 'getClientInfoData')
        .mockResolvedValue({
          payload: null,
          type: 'functionRuleMapping/addFunctionRuleMapping/rejected',
          meta: {
            arg: {},
            requestId: 'test request id',
            rejectedWithValue: true,
            requestStatus: 'rejected',
            aborted: true,
            condition: true
          },
          error: 'error'
        } as never);

      const res = await action(dispatch, getState, undefined);

      expect(res.payload).toEqual({});
      expect(dispatch).toBeCalledTimes(3);
    });
  });
});
