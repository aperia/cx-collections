import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { SortType } from 'app/_libraries/_dls/components';

// types
import { FunctionRuleMappingState } from '../types';
import {
  triggerAddFunctionRuleMappingBuilder,
  triggerAddFunctionRuleMapping
} from './triggerAddFunctionRuleMapping';
import {
  triggerEditFunctionRuleMappingBuilder,
  triggerEditFunctionRuleMapping
} from './triggerEditFunctionRuleMapping';

import {
  getFunctionRuleMapping,
  getFunctionRuleMappingBuilder
} from './getFunctionRuleMapping';

import {
  getReferenceDataForFunction,
  getReferenceDataForFunctionBuilder
} from './getReferenceDataForFunction';

const initialState: FunctionRuleMappingState = {
  functionRuleMappingList: {
    loading: false,
    data: [],
    error: '',
    sortBy: { id: 'functionCode', order: undefined }
  },
  functionRuleMappingForm: {
    loading: false,
    openAddModal: false,
    openEditModal: false
  }
};

const { actions, reducer } = createSlice({
  name: 'functionRuleMapping',
  initialState,
  reducers: {
    openModalAddFunctionRuleMapping: draftState => {
      draftState.functionRuleMappingForm = {
        ...draftState.functionRuleMappingForm,
        openAddModal: true,
        openEditModal: false
      };
    },
    closeModalAddFunctionRuleMapping: draftState => {
      draftState.functionRuleMappingForm = {
        ...draftState.functionRuleMappingForm,
        openAddModal: false
      };
    },
    openModalEditFunctionRuleMapping: (draftState, action) => {
      draftState.functionRuleMappingForm = {
        ...draftState.functionRuleMappingForm,
        openAddModal: false,
        openEditModal: true,
        data: action.payload
      };
    },
    closeModalEditFunctionRuleMapping: draftState => {
      draftState.functionRuleMappingForm = {
        ...draftState.functionRuleMappingForm,
        openEditModal: false,
        data: undefined
      };
    },
    onChangeSort: (draftState, action: PayloadAction<SortType>) => {
      draftState.functionRuleMappingList = {
        ...draftState.functionRuleMappingList,
        sortBy: action.payload
      };
    }
  },
  extraReducers: builder => {
    getFunctionRuleMappingBuilder(builder);
    getReferenceDataForFunctionBuilder(builder);
    triggerAddFunctionRuleMappingBuilder(builder);
    triggerEditFunctionRuleMappingBuilder(builder);
  }
});

const functionRuleMappingActions = {
  ...actions,
  getFunctionRuleMapping,
  getReferenceDataForFunction,
  triggerAddFunctionRuleMapping,
  triggerEditFunctionRuleMapping
};

export { functionRuleMappingActions, reducer };
