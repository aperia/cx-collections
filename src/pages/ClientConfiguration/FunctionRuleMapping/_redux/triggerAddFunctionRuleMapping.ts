import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

// redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';

// constants
import { I18N_FUNCTION_RULE_MAPPING } from '../constants';

// type
import {
  FunctionRuleMappingState,
  SubmitFunctionRuleMappingArgs
} from '../types';
import { addFunctionRuleMapping } from './addFunctionRuleMapping';
import { getFunctionRuleMapping } from './getFunctionRuleMapping';

export const customDataForChangeHistory: string[] = [
  'functionText',
  'nextRuleName',
  'nextRuleVersion',
  'effectiveDate'
];

export const triggerAddFunctionRuleMapping = createAsyncThunk<
  unknown,
  SubmitFunctionRuleMappingArgs,
  ThunkAPIConfig
>(
  'functionRuleMapping/triggerAddFunctionRuleMapping',
  async (args, thunkAPI) => {
    const { dispatch, rejectWithValue } = thunkAPI;

    const { data, callback } = args;

    if (!data) return rejectWithValue({});

    const newData = { ...data } as MagicKeyValue;

    const addData = Object.keys(newData)
      .filter((filterKey: string) =>
        customDataForChangeHistory.includes(filterKey)
      )
      .reduce((obj: any, key: string) => {
        obj[key] = newData[key];
        return obj;
      }, {});

    const response = await dispatch(addFunctionRuleMapping(data));

    if (isFulfilled(response)) {
      batch(() => {
        dispatch(
          changeHistoryActions.saveChangedClientConfig({
            action: 'ADD',
            changedCategory: 'functionRuleMapping',
            changedItem: {
              functionText: data?.functionText
            },
            newValue: addData
          })
        );
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_FUNCTION_RULE_MAPPING.ADD_FUNCTION_SUCCESS
          })
        );
        dispatch(getFunctionRuleMapping());
      });
      typeof callback === 'function' && callback();
    }

    if (isRejected(response)) {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_FUNCTION_RULE_MAPPING.ADD_FUNCTION_FAIL
        })
      );
      return rejectWithValue({});
    }

    return undefined;
  }
);

export const triggerAddFunctionRuleMappingBuilder = (
  builder: ActionReducerMapBuilder<FunctionRuleMappingState>
) => {
  builder
    .addCase(triggerAddFunctionRuleMapping.pending, (draftState, action) => {
      draftState.functionRuleMappingForm = {
        ...draftState.functionRuleMappingForm,
        loading: true
      };
    })
    .addCase(triggerAddFunctionRuleMapping.fulfilled, (draftState, action) => {
      draftState.functionRuleMappingForm = {
        ...draftState.functionRuleMappingForm,
        loading: false,
        error: '',
        openAddModal: false,
        refData: []
      };
    })
    .addCase(triggerAddFunctionRuleMapping.rejected, (draftState, action) => {
      draftState.functionRuleMappingForm = {
        ...draftState.functionRuleMappingForm,
        loading: false,
        error: action.error.message
      };
    });
};
