import React, {
  Fragment,
  useCallback,
  useEffect,
  useMemo,
  useRef
} from 'react';
import { useDispatch, useSelector } from 'react-redux';

// libaries
import classNames from 'classnames';

// components
import { FailedApiReload, NoDataGrid } from 'app/components';
import {
  Button,
  ColumnType,
  Grid,
  GridRef,
  Pagination,
  SortType,
  Tooltip,
  TruncateText
} from 'app/_libraries/_dls/components';
import { AddFunctionRuleMapping } from './AddFunctionRuleMapping';

// hooks
import { usePagination } from 'app/hooks';
import { functionRuleMappingActions } from './_redux/reducers';
import {
  getSortBy,
  selectFunctionRuleMappingListData,
  selectFunctionRuleMappingListError,
  selectFunctionRuleMappingListLoading,
  selectOpenAddModal
} from './_redux/selectors';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import {
  FUNCTION_RULE_MAPPING_GRID_COLUMN_NAME,
  I18N_FUNCTION_RULE_MAPPING
} from './constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// type
import { FunctionRuleMappingItem } from './types';

// helper
import { formatCommon } from 'app/helpers';
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';
import { genAmtId } from 'app/_libraries/_dls/utils';

const Header = ({ className }: { className?: string }) => {
  const { t } = useTranslation();
  return (
    <p
      className={classNames('fs-14 fw-500 color-grey-d20', className)}
      data-testid={genAmtId('clientConfig_functionRuleMapping_grid', 'header', '')}
    >
      {t(I18N_FUNCTION_RULE_MAPPING.FUNCTION_RULE_MAPPING_LIST)}
    </p>
  );
};

const FunctionRuleMappingGrid = () => {
  const gridRefs = useRef<GridRef>(null);
  const testId = 'clientConfig_functionRuleMapping_grid';

  const dispatch = useDispatch();

  const { t } = useTranslation();

  const isLoading = useSelector(selectFunctionRuleMappingListLoading);

  const isError = useSelector(selectFunctionRuleMappingListError);

  const modalAddOpened = useSelector(selectOpenAddModal);

  const sortBy = useSelector(getSortBy);

  const status = useCheckClientConfigStatus();

  const functionRuleMappingConfigData = useSelector(
    selectFunctionRuleMappingListData
  );

  const handleEditFunction = useCallback(
    record => {
      dispatch(
        functionRuleMappingActions.openModalEditFunctionRuleMapping(record)
      );
    },
    [dispatch]
  );

  const handleOpenModal = () => {
    dispatch(functionRuleMappingActions.openModalAddFunctionRuleMapping());
  };

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(functionRuleMappingConfigData);

  const handleReload = useCallback(() => {
    dispatch(functionRuleMappingActions.getFunctionRuleMapping());
  }, [dispatch]);

  useEffect(() => {
    handleReload();
  }, [handleReload]);

  const handleSort = (sort: SortType) => {
    dispatch(functionRuleMappingActions.onChangeSort(sort));
  };
  // reset sort
  useEffect(() => () => {
    dispatch(functionRuleMappingActions.onChangeSort({ id: '' }));
  }, [dispatch]);

  const showPagination = total > pageSize[0];

  const dataColumn = useMemo(() => {
    return [
      {
        id: FUNCTION_RULE_MAPPING_GRID_COLUMN_NAME.FUNCTION_TEXT,
        Header: t(I18N_FUNCTION_RULE_MAPPING.FUNCTION),
        accessor: FUNCTION_RULE_MAPPING_GRID_COLUMN_NAME.FUNCTION_TEXT,
        isSort: true,
        width: 180
      },
      {
        id: FUNCTION_RULE_MAPPING_GRID_COLUMN_NAME.CURRENT_RULE_NAME,
        Header: t(I18N_FUNCTION_RULE_MAPPING.CURRENT_RULE_NAME),
        isSort: true,
        accessor: (col: FunctionRuleMappingItem) => (
          <TruncateText
            lines={2}
            title={col?.currentRuleName}
            resizable
            ellipsisMoreText={t(I18N_COMMON_TEXT.MORE)}
            ellipsisLessText={t(I18N_COMMON_TEXT.LESS)}
            className="text-break"
          >
            {col?.currentRuleName}
          </TruncateText>
        ),
        width: 210
      },
      {
        id: FUNCTION_RULE_MAPPING_GRID_COLUMN_NAME.CURRENT_RULE_VERSION,
        Header: t(I18N_FUNCTION_RULE_MAPPING.CURRENT_RULE_VERSION),
        accessor: FUNCTION_RULE_MAPPING_GRID_COLUMN_NAME.CURRENT_RULE_VERSION,
        width: 182,
        isSort: true
      },
      {
        id: FUNCTION_RULE_MAPPING_GRID_COLUMN_NAME.NEXT_RULE_NAME,
        Header: t(I18N_FUNCTION_RULE_MAPPING.NEXT_RULE_NAME),
        accessor: (col: FunctionRuleMappingItem) => (
          <TruncateText
            lines={2}
            title={col?.nextRuleName}
            resizable
            ellipsisMoreText={t(I18N_COMMON_TEXT.MORE)}
            ellipsisLessText={t(I18N_COMMON_TEXT.LESS)}
            className="text-break"
          >
            {col?.nextRuleName}
          </TruncateText>
        ),
        isSort: true,
        width: 180
      },
      {
        id: FUNCTION_RULE_MAPPING_GRID_COLUMN_NAME.NEXT_RULE_VERSION,
        Header: t(I18N_FUNCTION_RULE_MAPPING.NEXT_RULE_VERSION),
        accessor: FUNCTION_RULE_MAPPING_GRID_COLUMN_NAME.NEXT_RULE_VERSION,
        isSort: true,
        width: 182
      },
      {
        id: FUNCTION_RULE_MAPPING_GRID_COLUMN_NAME.EFFECTIVE_DATE,
        Header: t(I18N_FUNCTION_RULE_MAPPING.NEXT_EFFECTIVE_DATE),
        accessor: (col: FunctionRuleMappingItem) => {
          return (
            <div className="text-left">
              {col?.effectiveDate ? (
                <p
                  className="text-left"
                  id={`function_rule_mapping_effectiveDate`}
                >
                  {formatCommon(col.effectiveDate).time.date}
                </p>
              ) : (
                <span className="form-group-static__text" />
              )}
            </div>
          );
        },
        isSort: true,
        width: 180,
        autoWidth: true
      },
      {
        id: 'action',
        Header: t(I18N_FUNCTION_RULE_MAPPING.ACTION),
        accessor: (col: FunctionRuleMappingItem, idx: number) => (
          <Fragment>
            <Button
              disabled={status}
              onClick={() => handleEditFunction(col)}
              size="sm"
              variant="outline-primary"
              dataTestId={`${idx}_editBtn_${testId}`}
            >
              {t(I18N_COMMON_TEXT.EDIT)}
            </Button>
          </Fragment>
        ),
        width: 92,
        isFixedRight: true,
        cellProps: { className: 'text-center' },
        cellBodyProps: { className: 'text-center' },
        className: 'td-sm'
      }
    ];
  }, [t, handleEditFunction, status]);

  if (isLoading) return <div className="vh-50" />;

  if (isError)
    return (
      <>
        <Header className="mt-16" />
        <FailedApiReload
          id="function-rule-mapping-request-fail"
          className="mt-80"
          onReload={handleReload}
          dataTestId={`${testId}_failedApi`}
        />
      </>
    );

  if (!gridData.length)
    return (
      <>
        <Header className="mt-16" />
        <NoDataGrid
          text={t(I18N_FUNCTION_RULE_MAPPING.NO_DATA)}
          dataTestId={`${testId}_noData`}
        >
          <div className="mt-24">
            <Button
              disabled={status}
              onClick={handleOpenModal}
              size="sm"
              variant="outline-primary"
              dataTestId={`${testId}_addFunctionBtn`}
            >
              {t(I18N_FUNCTION_RULE_MAPPING.ADD_FUNCTION)}
            </Button>
            {modalAddOpened && <AddFunctionRuleMapping />}
          </div>
        </NoDataGrid>
      </>
    );

  return (
    <>
      <div className="mt-16 d-flex justify-content-between align-items-center">
        <Header />
        <div className="mr-n8">
          {functionRuleMappingConfigData?.length < 4 ? (
            <Button
              disabled={status}
              onClick={handleOpenModal}
              size="sm"
              variant="outline-primary"
              dataTestId={`${testId}_addFunctionBtn`}
            >
              {t(I18N_FUNCTION_RULE_MAPPING.ADD_FUNCTION)}
            </Button>
          ) : (
            <Tooltip
              element={
                <span>{t(I18N_FUNCTION_RULE_MAPPING.FUNCTION_ARE_ADDED)}</span>
              }
            >
              <Button
                disabled size="sm"
                variant="outline-primary"
                dataTestId={`${testId}_addFunctionBtn`}
              >
                {t(I18N_FUNCTION_RULE_MAPPING.ADD_FUNCTION)}
              </Button>
            </Tooltip>
          )}

          {modalAddOpened && <AddFunctionRuleMapping />}
        </div>
      </div>
      <div className="mt-16">
        <Grid
          ref={gridRefs}
          columns={dataColumn as unknown as ColumnType[]}
          data={gridData}
          onSortChange={handleSort}
          sortBy={[sortBy]}
          dataTestId={`${testId}_grid`}
        />
        {showPagination && (
          <div className="pt-16">
            <Pagination
              totalItem={total}
              pageSize={pageSize}
              pageNumber={currentPage}
              pageSizeValue={currentPageSize}
              compact
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
              dataTestId={`${testId}_pagination`}
            />
          </div>
        )}
      </div>
    </>
  );
};

export default FunctionRuleMappingGrid;
