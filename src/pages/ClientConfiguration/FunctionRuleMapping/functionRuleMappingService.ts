import { apiService } from 'app/utils/api.service';
import { SubmitFunctionRuleMappingArgs } from './types';

export const functionRuleMappingService = {
  getFunctionRuleMapping() {
    const url =
      window.appConfig?.api?.functionRuleMapping
        ?.getFunctionRuleMapping || '';
    return apiService.post(url, {});
  },

  addFunctionRuleMapping(data: SubmitFunctionRuleMappingArgs) {
    const url =
      window.appConfig?.api?.functionRuleMapping
        ?.addFunctionRuleMapping || '';
    return apiService.post(url, { data });
  },

  editFunctionRuleMapping(data: SubmitFunctionRuleMappingArgs) {
    const url =
      window.appConfig?.api?.functionRuleMapping
        ?.editFunctionRuleMapping || '';
    return apiService.post(url, { data });
  },

  getReferenceDataForFunction() {
    const url =
      window.appConfig?.api?.functionRuleMapping
        ?.getReferenceDataForFunction || '';
    return apiService.get(url);
  }
};
