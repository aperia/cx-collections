import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { screen } from '@testing-library/react';
import FunctionRuleMappingGrid from './FunctionRuleMappingGrid';
import {
  FUNCTION_RULE_MAPPING_GRID_COLUMN_NAME,
  I18N_FUNCTION_RULE_MAPPING
} from './constants';
// Hooks
import * as usePagination from 'app/hooks/usePagination';
import { functionRuleMappingActions } from './_redux/reducers';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import userEvent from '@testing-library/user-event';
//
import * as mockStatus from '../hooks/useCheckClientConfigStatus';

const initialState: Partial<RootState> = {
  functionRuleMapping: {
    functionRuleMappingList: {
      loading: false,
      data: [
        {
          currentRuleName: 'name 1',
          functionCode: 'S',
          effectiveDate: '2021-01-01',
          effectiveTime: '00:00:00'
        },
        { currentRuleName: 'name 2', functionCode: 'D' }
      ],
      sortBy: { id: 'currentRuleName', order: 'asc' }
    },
    functionRuleMappingForm: {
      loading: false,
      openAddModal: true,
      openEditModal: true
    }
  }
};

const spyFunctionRuleMapping = mockActionCreator(functionRuleMappingActions);

beforeEach(() => {
  spyFunctionRuleMapping('getFunctionRuleMapping');

  jest.spyOn(usePagination, 'usePagination').mockImplementation(
    (data: any[]) =>
      ({
        total: 200,
        currentPage: 1,
        pageSize: [10, 25, 50],
        currentPageSize: 10,
        gridData: data,
        onPageChange: jest.fn(),
        onPageSizeChange: jest.fn()
      } as any)
  );
});

describe('Render', () => {
  it('Should render with default data', () => {
    renderMockStore(<FunctionRuleMappingGrid />, {
      initialState: {
        functionRuleMapping: {
          functionRuleMappingList: {
            data: [],
            loading: false,
            sortBy: { id: 'currentRuleName', order: 'asc' }
          },
          functionRuleMappingForm: {
            loading: false,
            openAddModal: true,
            openEditModal: false
          }
        }
      }
    });

    expect(
      screen.getByText(I18N_FUNCTION_RULE_MAPPING.NO_DATA)
    ).toBeInTheDocument();
  });

  it('Should render with error', () => {
    renderMockStore(<FunctionRuleMappingGrid />, {
      initialState: {
        functionRuleMapping: {
          functionRuleMappingList: {
            data: [],
            loading: false,
            error: 'error',
            sortBy: { id: 'currentRuleName', order: 'asc' }
          },
          functionRuleMappingForm: {
            loading: false,
            openAddModal: false,
            openEditModal: false
          }
        }
      }
    });

    expect(screen.getByText(I18N_COMMON_TEXT.RELOAD)).toBeInTheDocument();
  });

  it('Should render with 5 data', () => {
    const { container } = renderMockStore(<FunctionRuleMappingGrid />, {
      initialState: {
        functionRuleMapping: {
          functionRuleMappingList: {
            data: [
              { currentRuleName: 'name 1', functionCode: 'D' },
              { currentRuleName: 'name 2', functionCode: 'D' },
              { currentRuleName: 'name 3', functionCode: 'E' },
              { currentRuleName: 'name 4', functionCode: 'C' },
              { currentRuleName: 'name 5', functionCode: 'Z' }
            ],
            loading: false,
            sortBy: { id: 'currentRuleName', order: 'asc' }
          },
          functionRuleMappingForm: {
            loading: false,
            openAddModal: false,
            openEditModal: false
          }
        }
      }
    });

    expect(container.querySelector('.dls-grid')).toBeInTheDocument();
  });

  it('Should render when loading', () => {
    const { container } = renderMockStore(<FunctionRuleMappingGrid />, {
      initialState: {
        functionRuleMapping: {
          functionRuleMappingList: {
            data: [],
            loading: true,
            sortBy: { id: 'currentRuleName', order: 'asc' }
          },
          functionRuleMappingForm: {
            loading: false,
            openAddModal: false,
            openEditModal: false
          }
        }
      }
    });

    expect(container.querySelector('.dls-grid')).toBeNull();
  });

  it('Should render', () => {
    const { container } = renderMockStore(<FunctionRuleMappingGrid />, {
      initialState
    });

    expect(container.querySelector('.dls-grid')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  beforeEach(() => {
    jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(false);
  });

  it('handleOpenModal', () => {
    const mockAction = spyFunctionRuleMapping(
      'openModalAddFunctionRuleMapping'
    );

    renderMockStore(<FunctionRuleMappingGrid />, {
      initialState
    });

    userEvent.click(
      screen.getAllByText(I18N_FUNCTION_RULE_MAPPING.ADD_FUNCTION)[0]
    );

    expect(mockAction).toBeCalled();
  });

  it('handleEditFunction', () => {
    const mockAction = spyFunctionRuleMapping(
      'openModalEditFunctionRuleMapping'
    );

    renderMockStore(<FunctionRuleMappingGrid />, {
      initialState
    });

    userEvent.click(screen.getAllByText(I18N_COMMON_TEXT.EDIT)[0]);

    expect(mockAction).toBeCalled();
  });

  it('handleSort', () => {
    const mockAction = spyFunctionRuleMapping('onChangeSort');

    renderMockStore(<FunctionRuleMappingGrid />, {
      initialState
    });

    userEvent.click(screen.getByText(I18N_FUNCTION_RULE_MAPPING.FUNCTION));

    expect(mockAction).toBeCalledWith({
      id: FUNCTION_RULE_MAPPING_GRID_COLUMN_NAME.FUNCTION_TEXT,
      order: 'asc'
    });
  });
});
