import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { useState } from 'react';
import { I18N_FUNCTION_RULE_MAPPING } from './constants';
import * as helpers from './helpers';

describe('test helpers', () => {
  it('getHourTimePicker', () => {
    expect(
      helpers.getHourTimePicker(
        undefined as unknown as string,
        undefined as unknown as string
      )
    ).toEqual(0);
    expect(helpers.getHourTimePicker('12', 'AM')).toEqual(0);
    expect(helpers.getHourTimePicker('10', 'PM')).toEqual(22);
    expect(helpers.getHourTimePicker('8', 'AM')).toEqual(8);
  });

  it('getEffectiveTimeString', () => {
    expect(helpers.getEffectiveTimeString('12', 1, 'AM')).toEqual('00:01:00');
    expect(helpers.getEffectiveTimeString('10', 50, 'PM')).toEqual('22:50:00');
  });

  it('getEffectiveTimeErrors', () => {
    expect(
      helpers.getEffectiveTimeErrors(
        new Date('01/01/2999'),
        { minute: '10' },
        (text: string) => text
      )
    ).toEqual({
      effectiveTime: {
        message: I18N_FUNCTION_RULE_MAPPING.EFFECTIVE_TIME_ERROR,
        status: false
      }
    });
    expect(
      helpers.getEffectiveTimeErrors(
        undefined as unknown as Date,
        { minute: '10' },
        (text: string) => text
      )
    ).toEqual({
      effectiveTime: {
        message: I18N_FUNCTION_RULE_MAPPING.EFFECTIVE_TIME_ERROR,
        status: false
      }
    });
  });

  it('getEffectiveDateErrors', () => {
    expect(
      helpers.getEffectiveDateErrors(
        new Date('01/01/2999'),
        (text: string) => text
      )
    ).toEqual({
      effectiveDate: {
        message: I18N_FUNCTION_RULE_MAPPING.EFFECTIVE_DATE_ERROR,
        status: false
      }
    });

    expect(
      helpers.getEffectiveDateErrors(
        undefined as unknown as Date,
        (text: string) => text
      )
    ).toEqual({
      effectiveDate: {
        message: I18N_FUNCTION_RULE_MAPPING.EFFECTIVE_DATE_ERROR,
        status: true
      }
    });
  });

  it('getEffectiveDateErrors', () => {
    const Comp = () => {
      const [currentErrors] = useState({
        text: { status: false, message: '', blur: true }
      });

      const [touches, , setTouched] = helpers.useFormValidations(
        currentErrors,
        1,
        'text'
      );

      return (
        <>
          <div data-testid="touches">{JSON.stringify(touches)}</div>
          <div onClick={() => setTouched('wrongField')()}>
            setTouchedWrongField
          </div>
          <div onClick={() => setTouched('text')()}>setTouched</div>
          <div onClick={() => setTouched('text', true)()}>setTouchedBlur</div>
        </>
      );
    };

    render(<Comp />);

    userEvent.click(screen.getByText('setTouchedWrongField'));
    expect(JSON.parse(screen.getByTestId('touches').textContent!)).toEqual({
      text: { blur: true, touched: true },
      wrongField: { blur: false, touched: false }
    });

    userEvent.click(screen.getByText('setTouched'));
    expect(JSON.parse(screen.getByTestId('touches').textContent!)).toEqual({
      text: { blur: false, touched: true },
      wrongField: { blur: false, touched: false }
    });

    userEvent.click(screen.getByText('setTouched'));
    expect(JSON.parse(screen.getByTestId('touches').textContent!)).toEqual({
      text: { blur: false, touched: true },
      wrongField: { blur: false, touched: false }
    });

    userEvent.click(screen.getByText('setTouchedBlur'));
    expect(JSON.parse(screen.getByTestId('touches').textContent!)).toEqual({
      text: { blur: true, touched: true },
      wrongField: { blur: false, touched: false }
    });
  });
});
