import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { fireEvent, screen } from '@testing-library/react';
import { EditFunctionRuleMapping } from './EditFunctionRuleMapping';
import { I18N_FUNCTION_RULE_MAPPING } from './constants';
import userEvent from '@testing-library/user-event';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// hooks
import * as useFunctionRule from '../hooks/useFunctionRuleMappingForm';

// redux
import { functionRuleMappingActions } from './_redux/reducers';

import { SubmitFunctionRuleMappingArgs } from './types';
import { act } from 'react-dom/test-utils';
import { DatePickerProps } from 'app/_libraries/_dls/components';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');
  return {
    ...dlsComponents,
    DatePicker: ({ dateTooltip }: DatePickerProps) => (
      <>
        <input
          data-testid="DatePicker_dateTooltip"
          onChange={(e: any) => dateTooltip!(e.target.date as Date)}
        />
      </>
    )
  };
});

const initialState: Partial<RootState> = {
  functionRuleMapping: {
    functionRuleMappingList: {
      loading: false,
      data: [
        { currentRuleName: 'name 1', functionCode: 'code_1' },
        { currentRuleName: 'name 2', functionCode: 'code_2' }
      ],
      sortBy: { id: '' }
    },
    functionRuleMappingForm: {
      loading: false,
      openAddModal: true,
      openEditModal: true,
      refData: [{ code: 'code_1', text: 'text_1' }, { text: 'text_2' }],
      data: {
        currentRuleName: 'name',
        effectiveDate: '2021-01-01',
        effectiveTime: '00:00:00'
      }
    }
  }
};

const spyFunctionRuleMappingActions = mockActionCreator(
  functionRuleMappingActions
);

beforeEach(() => {
  spyFunctionRuleMappingActions('closeModalEditFunctionRuleMapping');
});

const mappingForm = {
  handleChangeDropDownList: jest.fn(),
  handleChangeTextBoxWithoutAlpahnumber: jest.fn(),
  values: {},
  setFieldValue: jest.fn(),
  setValues: jest.fn(),
  handleReset: jest.fn(),
  handleChange: jest.fn(),
  handleBlur: jest.fn(),
  validateForm: jest.fn(),
  handleSubmit: jest.fn(),
  errors: {},
  touched: {}
};

describe('Render', () => {
  it('Should render with empty state', () => {
    renderMockStore(<EditFunctionRuleMapping />, {});

    expect(
      screen.queryByText(I18N_FUNCTION_RULE_MAPPING.EDIT_FUNCTION)
    ).toBeNull();
  });

  it('Should render with empty effective date', async () => {
    renderMockStore(<EditFunctionRuleMapping />, {
      initialState: {
        functionRuleMapping: {
          functionRuleMappingList: {
            loading: false,
            data: [
              { currentRuleName: 'name 1', functionCode: 'code_1' },
              { currentRuleName: 'name 2', functionCode: 'code_2' }
            ],
            sortBy: { id: '' }
          },
          functionRuleMappingForm: {
            loading: false,
            openAddModal: true,
            openEditModal: true,
            refData: [{ code: 'code_1', text: 'text_1' }, { text: 'text_2' }],
            data: {
              currentRuleName: 'name',
              effectiveDate: null,
              effectiveTime: '00:00:00'
            }
          }
        }
      }
    });

    expect(
      await screen.findByTestId('DatePicker_dateTooltip')
    ).toBeInTheDocument();
  });

  it('Should render', () => {
    renderMockStore(<EditFunctionRuleMapping />, {
      initialState
    });

    expect(
      screen.getByText(I18N_FUNCTION_RULE_MAPPING.EDIT_FUNCTION)
    ).toBeInTheDocument();
  });

  it('Should render with date has effectiveTime hour is 0', () => {
    renderMockStore(<EditFunctionRuleMapping />, {
      initialState: {
        functionRuleMapping: {
          functionRuleMappingList: {
            loading: false,
            data: [
              { currentRuleName: 'name 1', functionCode: 'code_1' },
              { currentRuleName: 'name 2', functionCode: 'code_2' }
            ],
            sortBy: { id: '' }
          },
          functionRuleMappingForm: {
            loading: false,
            openAddModal: true,
            openEditModal: true,
            refData: [{ code: 'code_1', text: 'text_1' }, { text: 'text_2' }],
            data: {
              currentRuleName: 'name',
              effectiveDate: '2021-01-01T00:50:00',
              effectiveTime: '00:50:00'
            }
          }
        }
      }
    });

    expect(
      screen.getByText(I18N_FUNCTION_RULE_MAPPING.EDIT_FUNCTION)
    ).toBeInTheDocument();
  });

  it('Should render with date has effectiveTime hour is 15', () => {
    renderMockStore(<EditFunctionRuleMapping />, {
      initialState: {
        functionRuleMapping: {
          functionRuleMappingList: {
            loading: false,
            data: [
              { currentRuleName: 'name 1', functionCode: 'code_1' },
              { currentRuleName: 'name 2', functionCode: 'code_2' }
            ],
            sortBy: { id: '' }
          },
          functionRuleMappingForm: {
            loading: false,
            openAddModal: true,
            openEditModal: true,
            refData: [{ code: 'code_1', text: 'text_1' }, { text: 'text_2' }],
            data: {
              currentRuleName: 'name',
              effectiveDate: '2021-01-01T15:00:00',
              effectiveTime: '15:00:00'
            }
          }
        }
      }
    });

    expect(
      screen.getByText(I18N_FUNCTION_RULE_MAPPING.EDIT_FUNCTION)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  beforeEach(() => {
    jest
      .spyOn(useFunctionRule, 'useFunctionRuleMappingForm')
      .mockImplementation(() => mappingForm);
  });

  it('handleCancelModal', () => {
    const mockAction = spyFunctionRuleMappingActions(
      'closeModalEditFunctionRuleMapping'
    );

    renderMockStore(<EditFunctionRuleMapping />, {
      initialState
    });

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));

    expect(mockAction).toBeCalled();
  });

  describe('onSubmit', () => {
    it('submit undefined values', () => {
      jest
        .spyOn(useFunctionRule, 'useFunctionRuleMappingForm')
        .mockImplementation((onSubmit: Function) => ({
          ...mappingForm,
          handleSubmit: () => onSubmit(undefined)
        }));

      const mockAction = spyFunctionRuleMappingActions(
        'triggerEditFunctionRuleMapping'
      );

      renderMockStore(<EditFunctionRuleMapping />, {
        initialState
      });

      act(() => {
        userEvent.click(screen.getByText(I18N_COMMON_TEXT.SAVE));
      });

      expect(mockAction).not.toBeCalled();
    });

    it('submit empty values', () => {
      jest
        .spyOn(useFunctionRule, 'useFunctionRuleMappingForm')
        .mockImplementation((onSubmit: Function) => ({
          ...mappingForm,
          handleSubmit: () => onSubmit({})
        }));

      const mockAction = spyFunctionRuleMappingActions(
        'triggerEditFunctionRuleMapping'
      );

      renderMockStore(<EditFunctionRuleMapping />, {
        initialState
      });

      fireEvent.change(screen.getByTestId('DatePicker_dateTooltip'), {
        target: { value: 'value', date: new Date('01/01/1901') }
      });

      act(() => {
        userEvent.click(screen.getByText(I18N_COMMON_TEXT.SAVE));
      });

      expect(mockAction).not.toBeCalled();
    });

    it('submit invalid values', () => {
      jest
        .spyOn(useFunctionRule, 'useFunctionRuleMappingForm')
        .mockImplementation((onSubmit: Function) => ({
          ...mappingForm,
          handleSubmit: () =>
            onSubmit({
              effectiveDate: new Date('01/01/2999')
            })
        }));

      const mockAction = jest.fn();

      spyFunctionRuleMappingActions(
        'triggerEditFunctionRuleMapping',
        (params: SubmitFunctionRuleMappingArgs) => {
          params.callback!();
          mockAction();
          return { type: 'type' };
        }
      );

      renderMockStore(<EditFunctionRuleMapping />, {
        initialState
      });

      fireEvent.change(screen.getByTestId('DatePicker_dateTooltip'), {
        target: { value: 'value', date: new Date('01/01/2901') }
      });

      act(() => {
        userEvent.click(screen.getByText(I18N_COMMON_TEXT.SAVE));
      });

      expect(mockAction).toBeCalled();
    });
  });
});
