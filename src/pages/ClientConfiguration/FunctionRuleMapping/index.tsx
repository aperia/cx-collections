import React from 'react';
import { useSelector } from 'react-redux';

// components
import FunctionRuleMappingGrid from './FunctionRuleMappingGrid';
import { SimpleBar } from 'app/_libraries/_dls/components';

// redux
import { selectOpenEditModal } from './_redux/selectors';

// language
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_FUNCTION_RULE_MAPPING } from './constants';
import { EditFunctionRuleMapping } from './EditFunctionRuleMapping';
import { genAmtId } from 'app/_libraries/_dls/utils';

const FunctionRuleMapping = () => {
  const modalEditOpened = useSelector(selectOpenEditModal);

  const { t } = useTranslation();

  const testId = 'clientConfig_functionRuleMapping';

  return (
    <div className={'position-relative h-100'}>
      <SimpleBar>
        <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
          <h5 data-testid={genAmtId(testId, 'title', '')}>
            {t(I18N_FUNCTION_RULE_MAPPING.FUNCTION_RULE_MAPPING)}
          </h5>
          <FunctionRuleMappingGrid />
        </div>
      </SimpleBar>
      {modalEditOpened && <EditFunctionRuleMapping />}
    </div>
  );
};

export default FunctionRuleMapping;
