import React, { useEffect, useMemo } from 'react';

import { useDispatch, useSelector } from 'react-redux';

// libaries
import { isEmpty } from 'app/_libraries/_dls/lodash';

// components
import {
  DatePicker,
  DropdownList,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  NumericTextBox,
  TextBox,
  Tooltip
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { I18N_FUNCTION_RULE_MAPPING } from './constants';

// redux
import { functionRuleMappingActions } from './_redux/reducers';

// selector
import {
  selectFunctionRuleMappingFormLoading,
  selectFunctionRuleMappingListData,
  selectFunctionRuleMappingRefData,
  selectOpenAddModal
} from './_redux/selectors';

// types
import { FunctionRuleMappingForm, FunctionRuleMappingItem } from './types';

// helpers
import { formatCommon } from 'app/helpers';
import { getEffectiveDateErrors } from './helpers';

// hooks
import { useFunctionRuleMappingForm } from '../hooks/useFunctionRuleMappingForm';
import ModalBodyWithErrorClientConfig from '../ApiErrorSection/ModalBodyWithErrorClientConfig';
import { genAmtId } from 'app/_libraries/_dls/utils';

export const AddFunctionRuleMapping = () => {
  const dispatch = useDispatch();
  const testId = 'clientConfig_functionRuleMapping_addModal';

  const { t } = useTranslation();

  const addFunctionRuleMappingFormLoading = useSelector(
    selectFunctionRuleMappingFormLoading
  );

  const dataList = useSelector(selectFunctionRuleMappingListData);

  const dataSource = useSelector(selectFunctionRuleMappingRefData);

  const modalOpened = useSelector(selectOpenAddModal);

  const getMinDate = (): Date => {
    const minDate = new Date();
    minDate.setDate(minDate.getDate() + 1);
    return minDate;
  };

  const refData = useMemo(() => {
    return (
      dataSource?.filter(ref => {
        return (
          dataList.find(item => item.functionCode === ref.code) === undefined
        );
      }) || []
    );
  }, [dataList, dataSource]);

  const handleCancelModal = () => {
    dispatch(functionRuleMappingActions.closeModalAddFunctionRuleMapping());
    handleReset({});
  };

  const onSubmit = (formValue: FunctionRuleMappingForm) => {
    if (!formValue) return;

    const effectiveDateError =
      formValue.effectiveDate &&
      getEffectiveDateErrors(formValue.effectiveDate, t);

    if (effectiveDateError?.effectiveDate?.status) {
      validateForm();
      return;
    }

    const postData = {
      functionCode: formValue?.functionName?.code,
      functionText: formValue?.functionName?.text,
      nextRuleName: formValue?.currentRuleName,
      nextRuleVersion: formValue?.currentRuleVersion,
      effectiveDate: formatCommon(formValue.effectiveDate!).time
        .yearMonthDayWithHyphen
    } as FunctionRuleMappingItem;

    const clearFormValue = () => {
      handleReset({});
    };

    dispatch(
      functionRuleMappingActions.triggerAddFunctionRuleMapping({
        data: postData,
        callback: clearFormValue
      })
    );
  };

  const {
    handleChangeDropDownList,
    handleChangeTextBoxWithoutAlpahnumber,
    values,
    handleReset,
    handleChange,
    handleBlur,
    handleSubmit,
    validateForm,
    setFieldValue,
    errors,
    touched
  } = useFunctionRuleMappingForm(onSubmit, 'Add');

  const handleDateTooltip = (date: Date) => {
    const today = new Date();
    today.setHours(0);

    today.setMinutes(0);

    today.setSeconds(0);

    today.setMilliseconds(0);
    if (date <= today) {
      return (
        <Tooltip
          element={
            <span>{t(I18N_FUNCTION_RULE_MAPPING.EFFECTIVE_DATE_ERROR)}</span>
          }
        />
      );
    }
  };

  useEffect(() => {
    modalOpened &&
      dispatch(functionRuleMappingActions.getReferenceDataForFunction());
  }, [dispatch, modalOpened]);

  useEffect(() => {
    if (refData?.length === 1 && !values?.functionName) {
      setFieldValue('functionName', refData[0]);
    }
  }, [values, refData, setFieldValue]);

  // revalidate form after close modal
  useEffect(() => {
    if (!modalOpened) return;

    validateForm();
  }, [modalOpened, validateForm]);

  const modalBody = () => {
    return (
      <>
        <form>
          {refData?.length > 0 && (
            <DropdownList
              label={t(I18N_FUNCTION_RULE_MAPPING.FUNCTION)}
              required
              name={'functionName'}
              value={values?.functionName}
              textField={'text'}
              onBlur={handleBlur}
              onChange={handleChangeDropDownList}
              error={{
                status: Boolean(errors.functionName && touched.functionName),
                message: errors.functionName!
              }}
              dataTestId={`${testId}_functionName`}
              noResult={t('txt_no_results_found')}
            >
              {refData?.map(item => (
                <DropdownList.Item
                  key={item.code}
                  value={item}
                  label={item.text}
                />
              ))}
            </DropdownList>
          )}
          <div className="mt-16">
            <TextBox
              required
              label={t(I18N_FUNCTION_RULE_MAPPING.RULE_NAME)}
              name={'currentRuleName'}
              value={values?.currentRuleName}
              maxLength={100}
              onBlur={handleBlur}
              onChange={handleChangeTextBoxWithoutAlpahnumber}
              error={{
                status: Boolean(
                  errors.currentRuleName && touched.currentRuleName
                ),
                message: errors.currentRuleName!
              }}
              dataTestId={`${testId}_currentRuleName`}
            />
            <p
              className="mt-8 fs-12 color-grey-l24"
              data-testid={genAmtId(testId, 'ruleNameDescription', '')}
            >
              {t(I18N_FUNCTION_RULE_MAPPING.RULE_NAME_DESCRIPTION)}
            </p>
          </div>
          <div className="row mt-16">
            <div className="col-6">
              <NumericTextBox
                required
                label={t(I18N_FUNCTION_RULE_MAPPING.VERSION)}
                name={'currentRuleVersion'}
                value={values?.currentRuleVersion}
                onBlur={handleBlur}
                onChange={handleChange}
                maxLength={3}
                format={'n0'}
                error={{
                  status: Boolean(
                    errors.currentRuleVersion && touched.currentRuleVersion
                  ),
                  message: errors.currentRuleVersion!
                }}
                dataTestId={`${testId}_currentRuleVersion`}
              />
            </div>
            <div className="col-6">
              <DatePicker
                disabled
                label={t(I18N_FUNCTION_RULE_MAPPING.EFFECTIVE_DATE)}
                name={'effectiveDate'}
                value={values?.effectiveDate}
                onBlur={handleBlur}
                onChange={handleChange}
                dateTooltip={handleDateTooltip}
                minDate={getMinDate()}
                error={{
                  status: Boolean(
                    errors.effectiveDate && touched.effectiveDate
                  ),
                  message: errors.effectiveDate!
                }}
                dataTestId={`${testId}_effectiveDate`}
              />
            </div>
          </div>
        </form>
      </>
    );
  };

  return (
    <>
      <Modal
        sm
        show={modalOpened}
        loading={addFunctionRuleMappingFormLoading}
        dataTestId={testId}
      >
        <ModalHeader
          border
          closeButton
          onHide={handleCancelModal}
          dataTestId={`${testId}_header`}
        >
          <ModalTitle dataTestId={`${testId}_title`}>
            {t(I18N_FUNCTION_RULE_MAPPING.ADD_FUNCTION)}
          </ModalTitle>
        </ModalHeader>
        <ModalBodyWithErrorClientConfig
          apiErrorClassName="mb-24"
          dataTestId={`${testId}_body`}
        >
          {modalBody()}
        </ModalBodyWithErrorClientConfig>
        <ModalFooter
          dataTestId={`${testId}_footer`}
          className="pt-32 pb-24"
          cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
          onCancel={handleCancelModal}
          okButtonText={t(I18N_COMMON_TEXT.SUBMIT)}
          onOk={handleSubmit}
          disabledOk={!isEmpty(errors)}
        />
      </Modal>
    </>
  );
};
