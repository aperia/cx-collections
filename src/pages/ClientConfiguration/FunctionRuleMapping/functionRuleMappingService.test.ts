// services
import { functionRuleMappingService } from './functionRuleMappingService';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';
import { SubmitFunctionRuleMappingArgs } from './types';

describe('functionRuleMappingService', () => {
  describe('getFunctionRuleMapping', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      functionRuleMappingService.getFunctionRuleMapping();

      expect(mockService).toBeCalledWith('', {});
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      functionRuleMappingService.getFunctionRuleMapping();

      expect(mockService).toBeCalledWith(
        apiUrl.functionRuleMapping.getFunctionRuleMapping,
        {}
      );
    });
  });

  describe('addFunctionRuleMapping', () => {
    const data: SubmitFunctionRuleMappingArgs = {
      data: { currentRuleName: 'name' }
    };
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      functionRuleMappingService.addFunctionRuleMapping(data);

      expect(mockService).toBeCalledWith('', { data });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      functionRuleMappingService.addFunctionRuleMapping(data);

      expect(mockService).toBeCalledWith(
        apiUrl.functionRuleMapping.addFunctionRuleMapping,
        { data }
      );
    });
  });

  describe('editFunctionRuleMapping', () => {
    const data: SubmitFunctionRuleMappingArgs = {
      data: { currentRuleName: 'name' }
    };
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      functionRuleMappingService.editFunctionRuleMapping(data);

      expect(mockService).toBeCalledWith('', { data });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      functionRuleMappingService.editFunctionRuleMapping(data);

      expect(mockService).toBeCalledWith(
        apiUrl.functionRuleMapping.editFunctionRuleMapping,
        { data }
      );
    });
  });

  describe('getReferenceDataForFunction', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      functionRuleMappingService.getReferenceDataForFunction();

      expect(mockService).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      functionRuleMappingService.getReferenceDataForFunction();

      expect(mockService).toBeCalledWith(
        apiUrl.functionRuleMapping.getReferenceDataForFunction
      );
    });
  });
});
