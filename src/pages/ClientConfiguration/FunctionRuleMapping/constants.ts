export const I18N_FUNCTION_RULE_MAPPING = {
  NO_DATA: 'txt_no_data',
  EDIT: 'txt_edit',
  ACTIONS: 'txt_actions',
  ACTION: 'txt_action',
  FUNCTION_RULE_MAPPING: 'txt_function_rule_mapping',
  FUNCTION_RULE_MAPPING_LIST: 'txt_function_rule_mapping_list',
  FUNCTION_NAME: "txt_function_name",
  CURRENT_RULE_NAME: "txt_current_rule_name",
  CURRENT_RULE_VERSION: "txt_current_rule_version",
  NEXT_RULE_NAME: "txt_next_rule_name",
  NEXT_RULE_VERSION: "txt_next_rule_version",
  NEXT_EFFECTIVE_DATE_TIME: "txt_next_effective_date_time",
  NEXT_EFFECTIVE_DATE_AND_TIME: "txt_next_effective_date_and_time",
  ADD_FUNCTION: "txt_add_function",
  EDIT_FUNCTION: "txt_edit_function",
  FUNCTION: "txt_function",
  RULE_NAME: "txt_rule_name",
  VERSION: "txt_version",
  CURRENT_VERSION: "txt_current_version",
  NEXT_VERSION: "txt_next_version",
  EFFECTIVE_DATE_TIME: "txt_effective_date_time",
  EFFECTIVE_DATE: "txt_effective_date",
  NEXT_EFFECTIVE_DATE: "txt_next_effective_date",
  EFFECTIVE_TIME: "txt_effective_time",
  NEXT_EFFECTIVE_TIME: "txt_next_effective_time",
  RULE_NAME_DESCRIPTION: "txt_rule_name_description",
  NEXT_RULE_NAME_DESCRIPTION: "txt_next_rule_name_description",
  EFFECTIVE_TIME_ERROR: "txt_effective_time_error",
  EFFECTIVE_DATE_ERROR: 'txt_effective_date_error',
  ADD_FUNCTION_SUCCESS: "txt_add_function_success",
  ADD_FUNCTION_FAIL: "txt_add_function_fail",
  EDIT_FUNCTION_SUCCESS: "txt_edit_function_success",
  EDIT_FUNCTION_FAIL: "txt_edit_function_fail",
  IS_REQUIRED: 'txt_is_required',
  FUNCTION_ARE_ADDED: 'txt_function_are_added'
};

export const FUNCTION_RULE_MAPPING_GRID_COLUMN_NAME = {
  FUNCTION_TEXT: 'functionText',
  CURRENT_RULE_NAME: 'currentRuleName',
  CURRENT_RULE_VERSION: 'currentRuleVersion',
  NEXT_RULE_NAME: 'nextRuleName',
  NEXT_RULE_VERSION: 'nextRuleVersion',
  EFFECTIVE_DATE: 'effectiveDate',
  EFFECTIVE_TIME: 'effectiveTime'
};


export const TIMEPICKERDATA = {
  HOURS: [
    '01',
    '02',
    '03',
    '04',
    '05',
    '06',
    '07',
    '08',
    '09',
    '10',
    '11',
    '12'
  ]
};
