import React, { useEffect, useMemo, useRef } from 'react';

import { useDispatch, useSelector } from 'react-redux';

// libaries
import { isEmpty } from 'app/_libraries/_dls/lodash';

// components
import {
  DatePicker,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  NumericTextBox,
  TextBox,
  Tooltip,
  TruncateText
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { I18N_FUNCTION_RULE_MAPPING } from './constants';

// redux
import { functionRuleMappingActions } from './_redux/reducers';

// selector
import {
  selectFunctionRuleMappingData,
  selectFunctionRuleMappingFormLoading,
  selectOpenEditModal
} from './_redux/selectors';

// types
import { FunctionRuleMappingForm, FunctionRuleMappingItem } from './types';

// helpers
import { formatCommon } from 'app/helpers';
import { getEffectiveDateErrors } from './helpers';

// hooks
import { useFunctionRuleMappingForm } from '../hooks/useFunctionRuleMappingForm';
import ModalBodyWithErrorClientConfig from '../ApiErrorSection/ModalBodyWithErrorClientConfig';
import { genAmtId } from 'app/_libraries/_dls/utils';

export const EditFunctionRuleMapping = () => {
  const dispatch = useDispatch();
  const testId = 'clientConfig_functionRuleMapping_editModal';

  const { t } = useTranslation();
  const divRef = useRef<HTMLDivElement>(null);

  const addFunctionRuleMappingFormLoading = useSelector(
    selectFunctionRuleMappingFormLoading
  );

  const data = useSelector(selectFunctionRuleMappingData);

  const modalOpened = useSelector(selectOpenEditModal);

  const getMinDate = (): Date => {
    const minDate = new Date();
    minDate.setDate(minDate.getDate() + 1);
    return minDate;
  };

  const handleCancelModal = () => {
    dispatch(functionRuleMappingActions.closeModalEditFunctionRuleMapping());
    handleReset({});
  };

  const onSubmit = (formValue: FunctionRuleMappingForm) => {
    if (!formValue) return;

    const effectiveDateError = getEffectiveDateErrors(
      formValue.effectiveDate!,
      t
    );

    if (effectiveDateError.effectiveDate.status) {
      validateForm();
      return;
    }

    const postData = {
      functionCode: data?.functionCode,
      functionText: data?.functionText,
      currentRuleName: data?.currentRuleName,
      currentRuleVersion: data?.currentRuleVersion,
      nextRuleName: formValue?.nextRuleName,
      nextRuleVersion: formValue?.nextRuleVersion,
      effectiveDate: formatCommon(formValue.effectiveDate!).time
        .yearMonthDayWithHyphen
    } as FunctionRuleMappingItem;

    const clearFormValue = () => {
      handleReset({});
    };

    dispatch(
      functionRuleMappingActions.triggerEditFunctionRuleMapping({
        data: postData,
        callback: clearFormValue
      })
    );
  };

  const handleDateTooltip = (date: Date) => {
    const today = new Date();
    today.setHours(0);

    today.setMinutes(0);

    today.setSeconds(0);

    today.setMilliseconds(0);
    if (date <= today) {
      return (
        <Tooltip
          element={
            <span>{t(I18N_FUNCTION_RULE_MAPPING.EFFECTIVE_DATE_ERROR)}</span>
          }
        />
      );
    }
  };

  const {
    handleChangeTextBoxWithoutAlpahnumber,
    values,
    setValues,
    handleReset,
    handleChange,
    handleBlur,
    handleSubmit,
    validateForm,
    errors,
    touched
  } = useFunctionRuleMappingForm(onSubmit, 'Edit');

  const isDisabledButtonSubmit = useMemo(() => {
    return (
      values?.nextRuleName !== data?.nextRuleName ||
      values?.nextRuleVersion !== data?.nextRuleVersion ||
      formatCommon(values?.effectiveDate || '').time.yearMonthDayWithHyphen !==
      data?.effectiveDate
    );
  }, [
    data?.effectiveDate,
    data?.nextRuleName,
    data?.nextRuleVersion,
    values?.effectiveDate,
    values?.nextRuleName,
    values?.nextRuleVersion
  ]);

  useEffect(() => {
    if (!data || !modalOpened) return;
    const date = data?.effectiveDate
      ? new Date(data?.effectiveDate)
      : undefined;

    const newValues = {
      nextRuleName: data?.nextRuleName,
      nextRuleVersion: data?.nextRuleVersion,
      effectiveDate: date
    };

    setValues(newValues);

    validateForm(newValues);
  }, [data, modalOpened, setValues, validateForm]);

  const modalBody = () => {
    return (
      <>
        <div className=" mx-n24 mt-n24 p-24 bg-light-l20 border-bottom">
          <div className="row">
            <div className="col-4">
              <h6 className="color-grey-l16 mb-4" data-testid={genAmtId(testId, 'title', '')}>
                {t(I18N_FUNCTION_RULE_MAPPING.FUNCTION)}
              </h6>
              <span className="content-empty" data-testid={genAmtId(testId, 'function', '')}>
                {data?.functionText}
              </span>
            </div>
            <div ref={divRef} className="col-4">
              <h6 className="color-grey-l16 mb-4" data-testid={genAmtId(testId, 'currentRuleNameTitle', '')}>
                {t(I18N_FUNCTION_RULE_MAPPING.CURRENT_RULE_NAME)}
              </h6>
              <TruncateText
                followRef={divRef}
                lines={2}
                title={data?.currentRuleName}
                resizable
                ellipsisMoreText={t(I18N_COMMON_TEXT.MORE)}
                ellipsisLessText={t(I18N_COMMON_TEXT.LESS)}
                className="content-empty text-break"
                dataTestId={`${testId}_currentRuleName`}
              >
                {data?.currentRuleName}
              </TruncateText>
            </div>
            <div className="col-4">
              <h6 className="color-grey-l16 mb-4" data-testid={genAmtId(testId, 'currentRuleVersion', '')}>
                {t(I18N_FUNCTION_RULE_MAPPING.CURRENT_RULE_VERSION)}
              </h6>
              <span className="content-empty" data-testid={genAmtId(testId, 'currentRuleVersion', '')}>
                {data?.currentRuleVersion}
              </span>
            </div>
          </div>
        </div>
        <form>
          <div className="mt-24">
            <TextBox
              required
              label={t(I18N_FUNCTION_RULE_MAPPING.NEXT_RULE_NAME)}
              name="nextRuleName"
              value={values?.nextRuleName}
              maxLength={100}
              onBlur={handleBlur}
              onChange={handleChangeTextBoxWithoutAlpahnumber}
              error={{
                status: Boolean(errors.nextRuleName && touched.nextRuleName),
                message: errors.nextRuleName!
              }}
              dataTestId={`${testId}_nextRuleName`}
            />
            <p className="mt-8 fs-12 color-grey-l24" data-testid={genAmtId(testId, 'nextRuleNameDescription', '')}>
              {t(I18N_FUNCTION_RULE_MAPPING.NEXT_RULE_NAME_DESCRIPTION)}
            </p>
          </div>
          <div className="row mt-16">
            <div className="col-6">
              <NumericTextBox
                required
                label={t(I18N_FUNCTION_RULE_MAPPING.NEXT_VERSION)}
                name="nextRuleVersion"
                value={values?.nextRuleVersion}
                onBlur={handleBlur}
                onChange={handleChange}
                maxLength={3}
                format={'n0'}
                error={{
                  status: Boolean(
                    errors.nextRuleVersion && touched.nextRuleVersion
                  ),
                  message: errors.nextRuleVersion!
                }}
                dataTestId={`${testId}_nextRuleVersion`}
              />
            </div>
            <div className="col-6">
              <DatePicker
                required
                label={t(I18N_FUNCTION_RULE_MAPPING.NEXT_EFFECTIVE_DATE)}
                name="effectiveDate"
                value={values?.effectiveDate}
                onBlur={handleBlur}
                onChange={handleChange}
                dateTooltip={handleDateTooltip}
                minDate={getMinDate()}
                popupBaseProps={{
                  popupBaseClassName: 'my-4'
                }}
                error={{
                  status: Boolean(
                    errors.effectiveDate && touched.effectiveDate
                  ),
                  message: errors.effectiveDate!
                }}
                dataTestId={`${testId}_effectiveDate`}
              />
            </div>
          </div>
        </form>
      </>
    );
  };

  return (
    <>
      <Modal sm show={modalOpened} loading={addFunctionRuleMappingFormLoading} dataTestId={testId}>
        <ModalHeader border closeButton onHide={handleCancelModal} dataTestId={`${testId}_header`}>
          <ModalTitle dataTestId={`${testId}_title`}>{t(I18N_FUNCTION_RULE_MAPPING.EDIT_FUNCTION)}</ModalTitle>
        </ModalHeader>
        <ModalBodyWithErrorClientConfig
          apiErrorClassName="mx-n24 mt-n24 p-24 bg-light-l20"
          dataTestId={`${testId}_body`}
        >
          {modalBody()}
        </ModalBodyWithErrorClientConfig>
        <ModalFooter
          dataTestId={`${testId}_footer`}
          className="pt-32 pb-24"
          cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
          onCancel={handleCancelModal}
          okButtonText={t(I18N_COMMON_TEXT.SAVE)}
          onOk={handleSubmit}
          disabledOk={!isEmpty(errors) || !isDisabledButtonSubmit}
        />
      </Modal>
    </>
  );
};
