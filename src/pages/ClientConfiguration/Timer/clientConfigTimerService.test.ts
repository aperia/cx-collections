// services
import { clientConfigTimerServices } from './clientConfigTimerService';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';
import { TimerConfigRequest } from './types';

describe('clientConfigTimerServices', () => {
  describe('getTimerConfig', () => {
    const params = {
      common: {
        agent: "3000",
        clientNumber: "9999",
        system: "1000",
        prin: "2000",
        user: "NSA1",
        app: "cx"
      }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      clientConfigTimerServices.getTimerConfig(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      clientConfigTimerServices.getTimerConfig(params);

      expect(mockService).toBeCalledWith(
        apiUrl.clientConfigTimer.getTimerConfig,
        params
      );
    });
  });

  describe('updateTimerConfig', () => {
    const params = {
      common: {
        agent: '3000',
        clientNumber: '9999',
        system: '1000',
        prin: '2000',
        user: 'NSA1',
        app: 'cx'
      },
      codeToText: {
        cspas: [
          {
            cspa: '9999-1000-2000-3000',
            components: [
              {
                component: 'component_timer_config',
                jsonValue: {
                  green: { hour: '00', minute: '01', second: '02' }
                }
              }
            ]
          }
        ]
      }
    } as TimerConfigRequest;

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      clientConfigTimerServices.updateTimerConfig(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      clientConfigTimerServices.updateTimerConfig(params);

      expect(mockService).toBeCalledWith(
        apiUrl.clientConfigTimer.updateTimerConfig,
        params
      );
    });
  });
});
