import React, { useEffect, useRef } from 'react';
import { Form, FormikProps } from 'formik';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// components
import TimeIcon from 'pages/__commons/TimeIcon';
import { FormikCheckboxControl } from 'app/components/_formik_controls/FormikCheckboxControl';
import { FormikTimePickerControl } from 'app/components/_formik_controls/FormikTimePickerControl';
import { FormikTextAreaControl } from 'app/components/_formik_controls/FormikTextAreaControl';

// constants & helpers
import { COLOR_CODE } from 'pages/TimerWorking/constants';
import { FormValues, Time } from './types';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';
import { genAmtId } from 'app/_libraries/_dls/utils';

const SUGGESTION_TIME_IN_MINUTES = 3;

export function addMinutesTime(min: number, time?: Time) {
  if (isEmpty(time) || !time) return undefined;
  const result = { ...time };
  const minResult = +result.minute + min;
  result.hour = `0${+result.hour + Math.floor(minResult / 60)}`.slice(-2);
  result.minute = `0${minResult % 60}`.slice(-2);
  return result;
}

export const TimerConfigForm = (props: FormikProps<FormValues>) => {
  const { values, setValues } = props;
  const prevCheck = useRef<boolean | undefined>(undefined);
  const testId = 'clientConfig_timmer_form';

  const { t } = useTranslation();

  const status = useCheckClientConfigStatus();

  useEffect(() => {
    if (!values.showMessage && values.message) {
      setValues({ ...values, message: '' });
    }
    if (
      !values.message &&
      prevCheck.current !== undefined &&
      prevCheck.current !== values.showMessage
    ) {
      setValues({
        ...values,
        message: t('txt_default_warning_message')
      });
    }
    prevCheck.current = values.showMessage;
  }, [setValues, t, values]);

  return (
    <Form>
      <div className="p-16 mt-32 border br-light-l04 br-radius-8">
        <div className="row align-items-center">
          <div className="w-lg-35 w-md-30 d-flex align-items-center">
            <TimeIcon color={COLOR_CODE.green} />
            <p className="fw-500 ml-8" data-testid={genAmtId(testId, 'green', '')}>
              {t('txt_green')}
            </p>
          </div>
          <div className="w-lg-65 w-md-70">
            <div className="d-flex align-items-center">
              <h6
                className="text-nowrap color-grey mr-24"
                data-testid={genAmtId(testId, 'green_startTimeTitle', '')}
              >
                {t('txt_start_time')}:
              </h6>
              <FormikTimePickerControl
                name="green"
                label={t('txt_start_time')}
                small
                readOnly
                showSecond
                dataTestId={`${testId}_green_startTime`}
              />
            </div>
          </div>
        </div>
        <hr className="mx-n16" />
        <div className="row align-items-center">
          <div className="w-lg-35 w-md-30 d-flex align-items-center">
            <TimeIcon color={COLOR_CODE.amber} />
            <p
              className="fw-500 ml-8"
              data-testid={genAmtId(testId, 'amber', '')}
            >{t('txt_amber')}</p>
          </div>
          <div className="w-lg-65 w-md-70">
            <div className="d-flex align-items-center">
              <h6
                className="text-nowrap color-grey mr-24"
                data-testid={genAmtId(testId, 'amber_startTimeTitle', '')}
              >
                {t('txt_start_time')}:
              </h6>
              <FormikTimePickerControl
                name="amber"
                label={t('txt_start_time')}
                small
                showSecond
                changeCallback={amber => {
                  setValues({
                    ...values,
                    red: addMinutesTime(
                      SUGGESTION_TIME_IN_MINUTES,
                      amber as Time
                    )
                  });
                }}
                readOnly={status}
                dataTestId={`${testId}_amber_startTime`}
              />
            </div>
          </div>
        </div>
        <hr className="mx-n16" />
        <div className="row align-items-center">
          <div className="w-lg-35 w-md-30 d-flex align-items-center">
            <TimeIcon color={COLOR_CODE.red} />
            <p
              className="fw-500 ml-8"
              data-testid={genAmtId(testId, 'red', '')}
            >{t('txt_red')}</p>
          </div>
          <div className="w-lg-65 w-md-70">
            <div className="d-flex align-items-center">
              <h6
                className="text-nowrap color-grey mr-24"
                data-testid={genAmtId(testId, 'red_startTimeTitle', '')}
              >
                {t('txt_start_time')}:
              </h6>
              <FormikTimePickerControl
                name="red"
                label={t('txt_start_time')}
                small
                showSecond
                readOnly={status}
                dataTestId={`${testId}_red_startTime`}
              />
            </div>
          </div>
        </div>
        <div className="row mt-16">
          <div className="w-lg-35 w-md-30"></div>
          <div className="w-lg-65 w-md-70">
            <FormikCheckboxControl
              name="showMessage"
              label={t('txt_show_message')}
              status={status}
              dataTestId={`${testId}_showMessage`}
            />

            {values.showMessage && (
              <div>
                <h6 className="color-grey mt-16">
                  {t('txt_warning_message')}{' '}
                  <span className="asterisk color-danger">*</span>
                </h6>
                <FormikTextAreaControl
                  readOnly={status}
                  className="mt-16"
                  maxLength={100}
                  name="message"
                  required
                  disabledEnter
                  dataTestId={`${testId}_message`}
                />
              </div>
            )}
          </div>
        </div>
      </div>
    </Form>
  );
};
