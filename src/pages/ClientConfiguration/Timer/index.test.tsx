import React from 'react';
import { fireEvent, screen, waitFor } from '@testing-library/react';
import set from 'lodash.set';
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import { clientConfigTimerActions, DEFAULT_VALUE } from './_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

import * as clientConfigHelper from 'pages/ClientConfiguration/helpers';

import Timer from './index';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { SECTION_TAB_EVENT } from '../constants';
import * as mockStatus from '../hooks/useCheckClientConfigStatus';
import { act } from 'react-dom/test-utils';

const mockAction = mockActionCreator(clientConfigTimerActions);
const mockToastAction = mockActionCreator(actionsToast);

Element.prototype.scrollTo = jest.fn();

const defaultState = {
  clientConfigTimer: { originalValue: DEFAULT_VALUE, loading: false },
  clientConfiguration: {
    settings: {
      selectedConfig: {
        additionalFields: [
          {
            name: 'isConfigActive',
            active: true
          }
        ]
      }
    }
  }
};

beforeEach(() => {
  mockAction('getTimerConfiguration');
  jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(false);
});

describe('test Timer component', () => {
  it('should render', () => {
    renderMockStoreId(<Timer />);
    jest.runAllTimers();
    expect(screen.getByText('txt_timer')).toBeInTheDocument();
  });

  it('should render with loading', () => {
    renderMockStoreId(<Timer />, {
      initialState: {
        clientConfigTimer: {
          loading: true,
          originalValue: { green: { hour: '00', minute: '00', second: '00' } }
        }
      }
    });
    jest.runAllTimers();
    expect(screen.getByText('txt_timer')).toBeInTheDocument();
  });
});

jest.useFakeTimers();

describe('useListenChangingTabEvent', () => {
  it('data not change', () => {
    const mockDispatchEventChangeTab = jest.fn();
    const spy = jest
      .spyOn(clientConfigHelper, 'dispatchControlTabChangeEvent')
      .mockImplementation(mockDispatchEventChangeTab);
    renderMockStoreId(<Timer />, { initialState: defaultState });
    const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGING_TAB, {
      bubbles: true,
      cancelable: true
    });

    window.dispatchEvent(event);
    expect(mockDispatchEventChangeTab).toBeCalled();
    spy.mockReset();
    spy.mockRestore();
  });

  it('data has been change', () => {
    const stateWithShowMessage = { ...defaultState };
    set(
      stateWithShowMessage,
      ['clientConfigTimer', 'originalValue', 'showMessage'],
      true
    );

    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });
    const spy = jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);

    renderMockStoreId(<Timer />, { initialState: stateWithShowMessage });
    const checkbox = screen.getByRole('checkbox');
    checkbox.click();
    const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGING_TAB, {
      bubbles: true,
      cancelable: true
    });

    window.dispatchEvent(event);
    expect(openConfirmModalAction).toBeCalledWith(
      expect.objectContaining({
        title: 'txt_unsaved_changes',
        body: 'txt_discard_change_body',
        cancelText: 'txt_continue_editing',
        confirmText: 'txt_discard_change',
        variant: 'danger'
      })
    );
    spy.mockRestore();
    spy.mockReset();
  });
});

describe('useListenClosing', () => {
  it('data not change', () => {
    const mockDispatchEventCloseModal = jest.fn();
    const spy = jest
      .spyOn(clientConfigHelper, 'dispatchCloseModalEvent')
      .mockImplementation(mockDispatchEventCloseModal);
    renderMockStoreId(<Timer />, { initialState: defaultState });
    const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
      bubbles: true,
      cancelable: true
    });

    window.dispatchEvent(event);
    expect(mockDispatchEventCloseModal).toBeCalled();
    spy.mockReset();
    spy.mockRestore();
  });

  it('data has been change', () => {
    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });
    const spy = jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);
    renderMockStoreId(<Timer />, { initialState: defaultState });
    const checkbox = screen.getByRole('checkbox');
    checkbox.click();
    const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
      bubbles: true,
      cancelable: true
    });

    window.dispatchEvent(event);
    expect(openConfirmModalAction).toBeCalledWith(
      expect.objectContaining({
        title: 'txt_unsaved_changes',
        body: 'txt_discard_change_body',
        cancelText: 'txt_continue_editing',
        confirmText: 'txt_discard_change',
        variant: 'danger'
      })
    );
    spy.mockRestore();
    spy.mockReset();
  });
});

describe('handle Action', () => {
  beforeEach(() => {
    jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(false);
  });

  describe('handle reset to prev', () => {
    it('reset without change', () => {
      const mockAddToast = mockToastAction('addToast');
      renderMockStoreId(<Timer />, { initialState: defaultState });
      const resetToPreviousButton = screen.getByText('txt_reset_to_previous');
      resetToPreviousButton.click();
      expect(mockAddToast).not.toBeCalled();
    });

    it('reset with change', () => {
      const mockAddToast = mockToastAction('addToast');
      renderMockStoreId(<Timer />, { initialState: defaultState });
      // make form changed
      const checkbox = screen.getByRole('checkbox');
      checkbox.click();
      const resetToPreviousButton = screen.getByText('txt_reset_to_previous');
      resetToPreviousButton.click();
      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: 'txt_timer_reset_to_previous_success'
      });
    });
  });

  describe('handle Validate', () => {
    it('validate error amber', () => {
      renderMockStoreId(<Timer />, {
        initialState: defaultState
      });
      const textField = screen.getAllByRole('textbox');
      fireEvent.change(textField[1], { target: { value: '000000' } });
      fireEvent.blur(textField[1]);
      expect(screen.getByText('txt_timer')).toBeInTheDocument();
    });

    it('validate error red', () => {
      renderMockStoreId(<Timer />, {
        initialState: defaultState
      });
      const textField = screen.getAllByRole('textbox');
      fireEvent.change(textField[1], { target: { value: '000700' } });
      fireEvent.blur(textField[1]);
      expect(screen.getByText('txt_timer')).toBeInTheDocument();
    });
  });

  it('handle submit', () => {
    const mockTriggerUpdate = mockAction('triggerUpdateTimerConfig');
    renderMockStoreId(<Timer />, { initialState: defaultState });
    const textField = screen.getAllByRole('textbox');
    act(() => {
      fireEvent.change(textField[1], { target: { value: '000700' } });
    });
    const submitButton = screen.getByText('txt_save_changes');

    waitFor(() => {
      submitButton.click();
    });
    expect(mockTriggerUpdate).not.toBeCalledWith({});
  });

  it('handle submit invalid', () => {
    jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(true);
    const mockTriggerUpdate = mockAction('triggerUpdateTimerConfig');
    renderMockStoreId(<Timer />, { initialState: defaultState });
    const textField = screen.getAllByRole('textbox');
    act(() => {
      fireEvent.change(textField[1], { target: { value: '000700' } });
    });
    const submitButton = screen.getByText('txt_save_changes');

    waitFor(() => {
      submitButton.click();
    });
    expect(mockTriggerUpdate).not.toBeCalledWith({});
  });
});
