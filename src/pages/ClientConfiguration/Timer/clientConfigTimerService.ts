import { apiService } from 'app/utils/api.service';
import { TimerConfigRequest } from './types';

export const clientConfigTimerServices = {
  getTimerConfig(requestBody: {
    common: Record<string, any>;
    selectConfig: string;
    selectCspa: string;
    selectComponent: string;
  }) {
    const url = window.appConfig?.api?.clientConfigTimer?.getTimerConfig || '';
    return apiService.post(url, requestBody);
  },
  updateTimerConfig(requestBody: TimerConfigRequest) {
    const url =
      window.appConfig?.api?.clientConfigTimer?.updateTimerConfig || '';
    return apiService.post(url, requestBody);
  }
};
