import { addMinutesTime } from './TimerConfigForm';

describe('test addMinutesTime function', () => {
  it('should return undefined', () => {
    const result = addMinutesTime(3);
    expect(result).toEqual(undefined);
  });

  it('should be working correctly', () => {
    const result = addMinutesTime(3, {
      hour: '00',
      minute: '06',
      second: '00'
    });
    expect(result).toEqual({ hour: '00', minute: '09', second: '00' });
  });
});
