export interface Time {
  hour: string;
  minute: string;
  second: string;
}

export interface FormValues {
  green: Time;
  amber?: Time;
  red?: Time;
  showMessage?: boolean;
  message?: string;
}

export interface TimerClientConfigState {
  originalValue: FormValues;
  loading: boolean;
}

export interface TimerConfigRequest {
  common: {
    clientNumber?: string;
    system?: string;
    prin?: string;
    agent?: string;
    user?: string;
    app?: string;
    org?: string;
    privileges?: string[];
  }
  codeToText: {
    cspas: [
      {
        cspa: string;
        components: [{ component: string; jsonValue: FormValues }];
        description: string;
        active: string;
      }
    ];
  };
}
