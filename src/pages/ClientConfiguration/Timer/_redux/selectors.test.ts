import { selectorWrapper } from 'app/test-utils';
import { selectOriginalFormValue, selectLoading } from './selectors';

describe('Test Client Configuration Code To Text Selectors', () => {
  const store: Partial<RootState> = {
    clientConfigTimer: {
      originalValue: {
        message: 'mock message'
      },
      loading: false
    } as any
  };

  it('selectOriginalFormValue', () => {
    const { data, emptyData } = selectorWrapper(store)(selectOriginalFormValue);

    expect(data).toEqual({ message: 'mock message' });
    expect(emptyData).toEqual(undefined);
  });

  it('selectLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(selectLoading);

    expect(data).toEqual(false);
    expect(emptyData).toEqual(undefined);
  });
});
