import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { TIMER_COMPONENT_ID } from 'app/constants';
import { selectConfig } from 'pages/ClientConfiguration/_redux/selectors';

import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';

import { clientConfigTimerServices } from '../clientConfigTimerService';
import { FormValues, TimerClientConfigState } from '../types';
import { isUndefined } from 'lodash';
import { saveChangedClientConfig } from 'pages/ClientConfiguration/ChangeHistory/_redux/saveChangedClientConfig';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';

export const updateTimerConfigRequest = createAsyncThunk<
  FormValues,
  FormValues,
  ThunkAPIConfig
>('clientConfigTimer/updateTimerConfigRequest', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;

  const {
    user = '',
    app = '',
    org = '',
    privileges
  } = window.appConfig?.commonConfig || {};

  const { clientId, systemId, principleId, agentId, description, status } =
    selectConfig(getState()) as Record<string, string>;
  const cspaSelected = `${clientId}-${systemId}-${principleId}-${agentId}`;
  try {
    await clientConfigTimerServices.updateTimerConfig({
      common: {
        agent: agentId,
        clientNumber: clientId,
        system: systemId,
        prin: principleId,
        user,
        app,
        org,
        privileges
      },
      codeToText: {
        cspas: [
          {
            cspa: cspaSelected,
            components: [{ component: TIMER_COMPONENT_ID, jsonValue: args }],
            description,
            active: status
          }
        ]
      }
    });
    return args;
  } catch (error) {
    dispatch(
      apiErrorNotificationAction.updateApiError({
        storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION,
        forSection: 'inClientConfigurationFeatureHeader',
        apiResponse: error
      })
    );

    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const formatTimerTime = (obj?: MagicKeyValue) => {
  if (isUndefined(obj)) return ' ';
  return `${obj.hour}:${obj.minute}:${obj.second}`;
};

export const triggerUpdateTimerConfig = createAsyncThunk<
  unknown,
  FormValues,
  ThunkAPIConfig
>('clientConfigtimer/triggerUpdateTimerConfig', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;
  const { clientConfigTimer } = getState();
  const oldValue = clientConfigTimer.originalValue;
  const response = await dispatch(updateTimerConfigRequest(args));

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'txt_timer_updated'
        })
      );
      dispatch(
        saveChangedClientConfig({
          action: 'UPDATE',
          changedCategory: 'timer',
          oldValue,
          newValue: args
        })
      );
    });
  }

  if (isRejected(response)) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: 'txt_timer_failed_to_update'
      })
    );
  }
});

export const triggerUpdateTimerConfigBuilder = (
  builder: ActionReducerMapBuilder<TimerClientConfigState>
) => {
  builder
    .addCase(updateTimerConfigRequest.pending, (draftState, action) => {
      draftState.loading = true;
    })
    .addCase(updateTimerConfigRequest.fulfilled, (draftState, action) => {
      draftState.loading = false;
      draftState.originalValue = action.payload;
    })
    .addCase(updateTimerConfigRequest.rejected, (draftState, action) => {
      draftState.loading = false;
    });
};
