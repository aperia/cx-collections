import { createSelector } from '@reduxjs/toolkit';

const getOriginalFormValue = (state: RootState) => {
  return state.clientConfigTimer?.originalValue;
};

const getLoading = (state: RootState) => {
  return state.clientConfigTimer?.loading;
};

export const selectOriginalFormValue = createSelector(
  getOriginalFormValue,
  data => data
);

export const selectLoading = createSelector(getLoading, data => data);
