import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { ROLE, TIMER_COMPONENT_ID } from 'app/constants';
import { selectConfig } from 'pages/ClientConfiguration/_redux/selectors';
import { clientConfigTimerServices } from '../clientConfigTimerService';
import { FormValues, TimerClientConfigState } from '../types';
import { DEFAULT_VALUE } from './reducers';

export const getTimerConfiguration = createAsyncThunk<
  FormValues,
  undefined,
  ThunkAPIConfig
>('clientConfigTimer/getTimerConfig', async (args, thunkAPI) => {
  const { getState } = thunkAPI;

  const { clientId, systemId, principleId, agentId } = selectConfig(
    getState()
  ) as Record<string, string>;
  const cspaSelected = `${clientId}-${systemId}-${principleId}-${agentId}`;
  const { data } = await clientConfigTimerServices.getTimerConfig({
    common: {
      privileges: [ROLE.ADMIN]
    },
    selectConfig: 'codeToText',
    selectComponent: TIMER_COMPONENT_ID,
    selectCspa: cspaSelected
  });
  const configs = data.configs.codeToText.cspas;
  const cspaConfig = (configs as CollectionsClientConfigItem[]).find(
    item => item.cspa === cspaSelected
  );

  if (cspaConfig === undefined) {
    return thunkAPI.rejectWithValue({ errorMessage: 'no cspa config' });
  }

  const timerConfig = cspaConfig.components.find(
    item => item.component === TIMER_COMPONENT_ID
  );

  if (timerConfig === undefined) {
    return thunkAPI.rejectWithValue({ errorMessage: 'no timer config' });
  }

  const returnData = timerConfig?.jsonValue;

  return returnData as unknown as FormValues;
});

export const getTimerConfigurationBuilder = (
  builder: ActionReducerMapBuilder<TimerClientConfigState>
) => {
  builder
    .addCase(getTimerConfiguration.pending, draftState => {
      draftState.loading = true;
    })
    .addCase(getTimerConfiguration.fulfilled, (draftState, action) => {
      draftState.originalValue = {
        ...action.payload,
        showMessage: action.payload.showMessage ?? false,
        message: action.payload.message
      };
      draftState.loading = false;
    })
    .addCase(getTimerConfiguration.rejected, (draftState, action) => {
      draftState.loading = false;
      draftState.originalValue = DEFAULT_VALUE;
    });
};
