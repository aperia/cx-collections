import './reducers';

//redux
import { createStore, Store } from '@reduxjs/toolkit';
import { rootReducer } from 'storeConfig';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

//api
import {
  updateTimerConfigRequest,
  triggerUpdateTimerConfig,
  formatTimerTime
} from './updateTimerConfig';

//services
import { clientConfigTimerServices } from '../clientConfigTimerService';

import {
  responseDefault,
  byPassFulfilled,
  mockActionCreator,
  byPassRejected
} from 'app/test-utils';

let spy: jest.SpyInstance;

let store: Store<RootState>;
let state: RootState;

const mockToast = mockActionCreator(actionsToast);

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

beforeEach(() => {
  store = createStore(rootReducer, {
    clientConfigTimer: {
      originalValue: {
        green: { hour: '00', minute: '00', second: '00' },
        amber: { hour: '00', minute: '11', second: '11' }
      },
      loading: false
    },
    clientConfiguration: {
      settings: {
        selectedConfig: {
          id: 'id',
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId'
        }
      }
    }
  });

  state = store.getState();
});

const mArgRequest = {
  green: { hour: '00', minute: '00', second: '00' },
  amber: { hour: '00', minute: '03', second: '00' },
  red: { hour: '00', minute: '06', second: '00' }
};

const mResult = {
  green: { hour: '00', minute: '00', second: '00' },
  amber: { hour: '00', minute: '00', second: '00' },
  red: { hour: '00', minute: '06', second: '00' },
  showMessage: false
};

describe('Test updateTimerConfigRequest', () => {
  it('Should return data', async () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        user: 'user',
        org: 'org',
        app: 'app'
      } as CommonConfig
    };
    jest
      .spyOn(clientConfigTimerServices, 'updateTimerConfig')
      .mockResolvedValue({ ...responseDefault });

    const response = await updateTimerConfigRequest(mArgRequest)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(updateTimerConfigRequest.fulfilled.type);
  });

  it('should return error', async () => {
    window.appConfig = {} as AppConfiguration;

    jest
      .spyOn(clientConfigTimerServices, 'updateTimerConfig')
      .mockRejectedValue({ ...responseDefault });

    const response = await updateTimerConfigRequest(mArgRequest)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(updateTimerConfigRequest.rejected.type);
  });
});

describe('test triggerUpdateTimerConfig', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {});
  });

  it('success', async () => {
    spy = jest
      .spyOn(clientConfigTimerServices, 'updateTimerConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    const toast = mockToast('addToast');

    const response = await triggerUpdateTimerConfig(mResult)(
      byPassFulfilled(triggerUpdateTimerConfig.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.type).toBe(triggerUpdateTimerConfig.fulfilled.type);

    expect(toast).toBeCalledWith({
      show: true,
      type: 'success',
      message: 'txt_timer_updated'
    });
  });

  it('success > case amber in args is null, in args is undefined', async () => {
    store = createStore(rootReducer, {
      clientConfigTimer: {
        originalValue: {
          green: { hour: '00', minute: '00', second: '00' },
          amber: null as never
        },
        loading: false
      }
    });

    spy = jest
      .spyOn(clientConfigTimerServices, 'updateTimerConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    const toast = mockToast('addToast');

    const response = await triggerUpdateTimerConfig({
      ...mResult,
      amber: undefined as never
    })(
      byPassFulfilled(triggerUpdateTimerConfig.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.type).toBe(triggerUpdateTimerConfig.fulfilled.type);

    expect(toast).toBeCalledWith({
      show: true,
      type: 'success',
      message: 'txt_timer_updated'
    });
  });

  it('success > case red in args is null, in args is null', async () => {
    store = createStore(rootReducer, {
      clientConfigTimer: {
        originalValue: {
          green: { hour: '00', minute: '00', second: '00' },
          amber: null as never,
          red: null as never,
          showMessage: false
        },
        loading: false
      }
    });

    spy = jest
      .spyOn(clientConfigTimerServices, 'updateTimerConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    const toast = mockToast('addToast');

    const response = await triggerUpdateTimerConfig({
      ...mResult,
      amber: undefined as never,
      red: null as never
    })(
      byPassFulfilled(triggerUpdateTimerConfig.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.type).toBe(triggerUpdateTimerConfig.fulfilled.type);

    expect(toast).toBeCalledWith({
      show: true,
      type: 'success',
      message: 'txt_timer_updated'
    });
  });

  it('success > case red in args is undefined, in argshave data', async () => {
    store = createStore(rootReducer, {
      clientConfigTimer: {
        originalValue: {
          green: { hour: '00', minute: '00', second: '00' },
          amber: null as never,
          red: { hour: '00', minute: '22', second: '00' },
          showMessage: false
        },
        loading: false
      }
    });

    spy = jest
      .spyOn(clientConfigTimerServices, 'updateTimerConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    const toast = mockToast('addToast');

    const response = await triggerUpdateTimerConfig({
      ...mResult,
      amber: undefined as never,
      red: undefined as never
    })(
      byPassFulfilled(triggerUpdateTimerConfig.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.type).toBe(triggerUpdateTimerConfig.fulfilled.type);

    expect(toast).toBeCalledWith({
      show: true,
      type: 'success',
      message: 'txt_timer_updated'
    });
  });

  it('error', async () => {
    const toast = mockToast('addToast');

    const response = await triggerUpdateTimerConfig(mArgRequest)(
      byPassRejected(triggerUpdateTimerConfig.rejected.type),
      store.getState,
      {}
    );

    expect(response.payload).toBeUndefined();

    expect(toast).toBeCalledWith({
      show: true,
      type: 'error',
      message: 'txt_timer_failed_to_update'
    });
  });

  describe('should be triggerUpdateTimerConfigBuilder', () => {
    it('should response triggerUpdateTimerConfig pending', () => {
      const pendingAction = updateTimerConfigRequest.pending(
        updateTimerConfigRequest.pending.type,
        mArgRequest
      );
      const actual = rootReducer(state, pendingAction);

      expect(actual.clientConfigTimer?.loading).toEqual(true);
    });

    it('should response updateTimerConfigRequest fulfilled', () => {
      const fulfilled = updateTimerConfigRequest.fulfilled(
        mArgRequest,
        updateTimerConfigRequest.pending.type,
        mArgRequest
      );
      const actual = rootReducer(state, fulfilled);

      expect(actual.clientConfigTimer?.loading).toEqual(false);

      expect(actual.clientConfigTimer.originalValue).toEqual({
        ...mArgRequest
      });
    });

    it('should response updateTimerConfigRequest rejected', () => {
      const rejected = updateTimerConfigRequest.rejected(
        null,
        updateTimerConfigRequest.rejected.type,
        mArgRequest
      );
      const actual = rootReducer(state, rejected);

      expect(actual.clientConfigTimer?.loading).toEqual(false);
    });
  });
});

describe('formatTimerTime', () => {
  it('valid data', () => {
    const result = formatTimerTime({
      hour: '0',
      minute: '0',
      second: '0'
    });

    expect(result).toEqual('0:0:0');
  });

  it('invalid', () => {
    const result = formatTimerTime(undefined);

    expect(result).toEqual(' ');
  });
});
