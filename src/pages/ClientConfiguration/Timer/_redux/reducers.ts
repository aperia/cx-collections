import { createSlice } from '@reduxjs/toolkit';
import { FormValues, TimerClientConfigState } from '../types';
import {
  getTimerConfiguration,
  getTimerConfigurationBuilder
} from './getTimerConfig';
import {
  triggerUpdateTimerConfig,
  triggerUpdateTimerConfigBuilder
} from './updateTimerConfig';

export const DEFAULT_VALUE: FormValues = {
  green: { hour: '00', minute: '00', second: '00' },
  amber: { hour: '00', minute: '03', second: '00' },
  red: { hour: '00', minute: '06', second: '00' },
  showMessage: false,
  message: undefined
};

const initialState: TimerClientConfigState = {
  originalValue: {
    ...DEFAULT_VALUE
  },
  loading: false
};

const { actions, reducer } = createSlice({
  name: 'timerClientConfig',
  initialState,
  reducers: {},
  extraReducers: builder => {
    getTimerConfigurationBuilder(builder);
    triggerUpdateTimerConfigBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getTimerConfiguration,
  triggerUpdateTimerConfig
};

export { allActions as clientConfigTimerActions, reducer };
