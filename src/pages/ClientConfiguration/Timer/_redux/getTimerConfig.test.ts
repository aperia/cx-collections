import { createStore, Store } from '@reduxjs/toolkit';
import './reducers';

import { getTimerConfiguration } from './getTimerConfig';

import { clientConfigTimerServices } from '../clientConfigTimerService';

import { responseDefault } from 'app/test-utils';

import { rootReducer } from 'storeConfig';

import set from 'lodash.set';
import cloneDeep from 'lodash.clonedeep';

import { TIMER_COMPONENT_ID } from 'app/constants';

let store: Store<RootState>;
let state: RootState;

const cspaValue = '8997-0020-0060-0900';

beforeEach(() => {
  store = createStore(rootReducer, {
    clientConfiguration: {
      settings: {
        selectedConfig: {
          id: cspaValue,
          clientId: '8997',
          systemId: '0020',
          principleId: '0060',
          agentId: '0900'
        }
      }
    }
  });
  state = store.getState();
});

let spy1: jest.SpyInstance;

afterEach(() => {
  spy1?.mockReset();
  spy1?.mockRestore();
});

const mockConfigResponse = {
  configs: {
    codeToText: {
      cspas: [
        {
          cspa: cspaValue,
          components: [
            {
              component: TIMER_COMPONENT_ID,
              jsonValue: {
                green: { hour: '00', minute: '00', second: '00' },
                amber: { hour: '00', minute: '03', second: '00' },
                red: { hour: '00', minute: '08', second: '00' },
                showMessage: true,
                message: 'Call has exceed expected time limit.'
              }
            }
          ],
          description: '',
          active: true
        }
      ]
    }
  }
};

describe('should have test getTimerConfiguration', () => {
 
  it('Should return data', async () => {
    const expectValue = { ...mockConfigResponse.configs.codeToText }.cspas.find(
      f => f.cspa === cspaValue
    )?.components[0].jsonValue;

    spy1 = jest
      .spyOn(clientConfigTimerServices, 'getTimerConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: mockConfigResponse
      });
  
    const response = await getTimerConfiguration()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual(expectValue);
  });

  it('It should run failed when cspa not found', async () => {
    const expectValue = { errorMessage: 'no cspa config' };
    const mockCSPANotFound = cloneDeep(mockConfigResponse);
    set(
      mockCSPANotFound,
      ['configs', 'codeToText', 'cspas', '0', 'cspa'],
      '8997-0020-0060-0901'
    );
    spy1 = jest
      .spyOn(clientConfigTimerServices, 'getTimerConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: mockCSPANotFound
      });
  
    const response = await getTimerConfiguration()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual(expectValue);
  });

  
  it('It should run failed when timerConfig not found', async () => {
    const expectValue = { errorMessage: 'no timer config' };
    const mockResponseNoTimerConfig = cloneDeep(mockConfigResponse);
    set(
      mockResponseNoTimerConfig,
      ['configs', 'codeToText', 'cspas', '0', 'components', '0', 'component'],
      `${TIMER_COMPONENT_ID}_not_found`
    );

    spy1 = jest
      .spyOn(clientConfigTimerServices, 'getTimerConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: mockResponseNoTimerConfig
      });

    const response = await getTimerConfiguration()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual(expectValue);
  });

});

describe('Test getTimerConfigurationBuilder', () => {

  it('should response getTimerConfiguration pending', () => {
    const pendingAction = getTimerConfiguration.pending('pending', undefined);
    const actual = rootReducer(state, pendingAction);
    expect(actual?.clientConfigTimer?.loading).toEqual(true);
  });

  it('should response getTimerConfiguration fulfilled', () => {

    const payload = {
      green: { hour: '00', minute: '00', second: '00' },
      amber: { hour: '00', minute: '03', second: '00' },
      red: { hour: '00', minute: '08', second: '00' }
    };
    const fulfilled = getTimerConfiguration.fulfilled(
      payload,
      getTimerConfiguration.fulfilled.type,
      undefined
    );
    const actual = rootReducer(state, fulfilled);
    expect(actual?.clientConfigTimer?.loading).toEqual(false);
    expect(actual?.clientConfigTimer?.originalValue).toEqual({
      ...payload,
      showMessage: false
    });
  });

  it('should response getTimerConfiguration rejected', () => {
    const rejected = getTimerConfiguration.rejected(
      null,
      getTimerConfiguration.rejected.type,
      undefined
    );
    const actual = rootReducer(state, rejected);
    expect(actual?.clientConfigTimer?.loading).toEqual(false);
  });
});
