import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useFormik, FormikContext } from 'formik';

// component
import { Button, SimpleBar } from 'app/_libraries/_dls/components';
import { TimerConfigForm } from './TimerConfigForm';

// redux
import { selectOriginalFormValue } from './_redux/selectors';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { clientConfigTimerActions } from './_redux/reducers';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useListenClosingModal } from '../hooks/useListenClosingModal';
import { useListenChangingTabEvent } from '../hooks/useListenChangingTabEvent';

// helpers
import { isEqual } from 'app/_libraries/_dls/lodash';
import { compareTime } from 'app/helpers';
import {
  dispatchCloseModalEvent,
  dispatchControlTabChangeEvent
} from '../helpers';
import { FormValues } from './types';
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';
import { genAmtId } from 'app/_libraries/_dls/utils';
import ApiErrorForHeaderSection from '../ApiErrorSection/ApiErrorForHeaderSection';

const Timer: React.FC = () => {
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const initialValues = useSelector(selectOriginalFormValue);

  const status = useCheckClientConfigStatus();

  const testId = 'clientConfig_timmer';

  const handleValidation = (values: FormValues) => {
    const errors: Record<string, string> = {};
    const { message, showMessage, amber, green, red } = values;
    if (showMessage && !message) {
      errors.message = t('txt_waring_message_is_required');
    }
    if (amber && !compareTime(amber, 'greaterThan', green)) {
      errors.amber = t('txt_start_time_of_amber_error_tooltip');
    }
    if (red && amber && !compareTime(red, 'greaterThan', amber)) {
      errors.red = t('txt_start_time_of_red_error_tooltip');
      errors.amber = t('txt_start_time_of_amber_must_be_less_than_');
    }
    return errors;
  };

  const formik = useFormik({
    initialValues,
    onSubmit: null as never,
    validateOnBlur: true,
    validateOnMount: true,
    validate: handleValidation
  });

  const isDataChange = !isEqual(
    {
      amber: initialValues.amber,
      red: initialValues.red,
      msg: initialValues.showMessage && initialValues.message
    },
    {
      amber: formik.values.amber,
      red: formik.values.red,
      msg: formik.values.showMessage && formik.values.message
    }
  );

  const { resetForm } = formik;

  const handleResetToPrev = () => {
    if (!isDataChange) return;
    formik.resetForm({ values: initialValues });
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'success',
        message: 'txt_timer_reset_to_previous_success'
      })
    );
  };

  const handleSubmit = () => {
    dispatch(clientConfigTimerActions.triggerUpdateTimerConfig(formik.values));
  };

  useEffect(() => {
    dispatch(clientConfigTimerActions.getTimerConfiguration());
  }, [dispatch]);

  useEffect(() => {
    resetForm({ values: initialValues });
  }, [initialValues, resetForm]);

  useListenChangingTabEvent(() => {
    if (!isDataChange) {
      dispatchControlTabChangeEvent();
      return;
    }
    dispatch(
      confirmModalActions.open({
        title: 'txt_unsaved_changes',
        body: 'txt_discard_change_body',
        cancelText: 'txt_continue_editing',
        confirmText: 'txt_discard_change',
        confirmCallback: dispatchControlTabChangeEvent,
        variant: 'danger'
      })
    );
  });

  useListenClosingModal(() => {
    if (!isDataChange) {
      dispatchCloseModalEvent();
      return;
    }
    dispatch(
      confirmModalActions.open({
        title: 'txt_unsaved_changes',
        body: 'txt_discard_change_body',
        cancelText: 'txt_continue_editing',
        confirmText: 'txt_discard_change',
        confirmCallback: dispatchCloseModalEvent,
        variant: 'danger'
      })
    );
  });

  return (
    <div className={'position-relative has-footer-button'}>
      <SimpleBar>
        <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
          <ApiErrorForHeaderSection
            className="mb-24"
            dataTestId={`${testId}_apiError`}
          />
          <div className="max-width-552">
            <h5 data-testid={genAmtId(testId, 'title', '')}>
              {t('txt_timer')}
            </h5>
            <p className="mt-16" data-testid={genAmtId(testId, 'desc1', '')}>
              {t('txt_timer_config_description1')}
            </p>
            <p className="mt-16" data-testid={genAmtId(testId, 'desc2', '')}>
              {t('txt_timer_config_description2')}
            </p>
            <div className="row no-gutters mt-16">
              <div className="col-4 justify-content-center">
                <div className="h-5px w-100 bg-green-l16"></div>
                <p
                  className="fs-12 text-center mt-8"
                  data-testid={genAmtId(testId, 'green', '')}
                >
                  {t('txt_green')}
                </p>
              </div>
              <div className="col-4 pl-2 justify-content-center">
                <div className="h-5px w-100 bg-orange-l16"></div>
                <p
                  className="fs-12 text-center mt-8"
                  data-testid={genAmtId(testId, 'amber', '')}
                >
                  {t('txt_amber')}
                </p>
              </div>
              <div className="col-4 pl-2 justify-content-center align-items-center">
                <div className="d-flex h-5px align-items-center w-100">
                  <div className="h-5px w-100 bg-red-l16"></div>
                  <div className="arrow-red"></div>
                </div>
                <p
                  className="fs-12 text-center mt-8"
                  data-testid={genAmtId(testId, 'red', '')}
                >
                  {t('txt_red')}
                </p>
              </div>
            </div>
            <FormikContext.Provider value={formik}>
              {<TimerConfigForm {...formik} />}
            </FormikContext.Provider>
          </div>
        </div>
      </SimpleBar>
      <div className="group-button-footer d-flex justify-content-end">
        <Button
          disabled={status}
          onClick={handleResetToPrev}
          variant="secondary"
          dataTestId={`${testId}_resetToPreviousBtn`}
        >
          {t('txt_reset_to_previous')}
        </Button>
        <Button
          disabled={(status ? status : !formik.isValid) || !isDataChange}
          onClick={handleSubmit}
          variant="primary"
          dataTestId={`${testId}_saveChangesBtn`}
        >
          {t('txt_save_changes')}
        </Button>
      </div>
    </div>
  );
};

export default Timer;
