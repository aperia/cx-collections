import React from 'react';
import { SortType } from 'app/_libraries/_dls/components';
import {
  AccountExternalStatusDataType,
  ReasonCodesDataType
} from './AccountExternalStatus/types';
import { SendLetterConfigData } from './SendLetter/types';
import { MemoConfigItem } from './Memos/types';
import { ClientConfigurationHistory } from './ChangeHistory/types';

export interface ClientConfigurationState extends MagicKeyValue {
  list?: {
    loading?: boolean;
    error?: boolean;
    data?: ConfigData[];
    sortBy?: SortType;
    searchValue?: string;
    textSearchValue?: string;
  };
  settings?: {
    isOpen?: boolean;
    selectedConfig?: ConfigData;
    sections?: RefDataValue[];
    loading?: boolean;
    toggle?: boolean;
  };
  openAddModal?: boolean;
  openEditModal?: boolean;
  openStatusModal?: boolean;

  loadingAddModal?: boolean;
  loadingEditModal?: boolean;
  loadingStatusModal?: boolean;
  loadingSaveClientConfig?: boolean;
  warningMessage?: string;

  add?: {
    errorMessage?: string;
    isRetry?: boolean;
  };
  update?: {
    errorMessage?: string;
    isRetry?: boolean;
  };
  lastHistory?: ClientConfigurationHistory;
}

export interface GetClientConfigListPayload {
  data?: ConfigData[];
}

export interface GetSectionListPayload {
  data?: RefDataValue[];
}
export interface GetSectionListArgs {}

export interface ConfigData extends CSPA {}
export interface AddClientConfigurationRequest {
  clientId: string;
  systemId: string;
  principleId: string;
  agentId: string;
  description: string;
  defaultCspa: string;
  retryEndpoint?: string;
}

export interface ClientConfigStatusProps {
  status: boolean;
}

export interface GetClientConfigurationRequest {
  common?: {
    clientName?: string;
    org?: string;
    agent: string;
    clientNumber: string;
    system: string;
    prin: string;
    user?: string;
    app: string;
  };
  orgId?: string;
  fetchSize?: string;
}

export interface CreateClientConfigurationRequest {
  common?: {
    agent: string;
    clientNumber: string;
    system: string;
    prin: string;
    app: string;
    user?: string;
  };
  privileges?: string[];
  agent: string;
  clientNumber: string;
  system: string;
  prin: string;
  userId: string;
  portfolioName: string;
  x500Id: string;
  orgId: string;
  binSequences: MagicKeyValue[];
  additionalFields?: AdditionalFieldsMapType[];
  copyDefaultConfig?: boolean;
}

export interface AddClientConfigurationArgs
  extends AddClientConfigurationRequest,
    MagicKeyValue {}

export interface UpdateClientConfigurationArgs {
  clientId: string;
  systemId: string;
  principleId: string;
  agentId: string;
  description: string;
  id: string;
  additionalFields?: AdditionalFieldsMapType[];
}

export interface ChangeStatusClientConfigurationArgs {
  id: string;
  additionalFields?: AdditionalFieldsMapType[];
  translateFn: Function;
}

export interface UpdateClientConfigurationRequest {
  common?: {
    agent: string;
    clientNumber: string;
    system: string;
    prin: string;
    app: string;
    user?: string;
  };
  clientInfoId: string;
  orgId?: string;
  portfolioName: string;
  binSequences?: MagicKeyValue[];
  additionalFields?: AdditionalFieldsMapType[];
}

export interface CSPAClientConfigType {
  cspa: string;
  description: string;
  active: boolean;
}

export interface ChangeStatusClientConfigurationRequest {
  common?: {
    agent: string;
    clientNumber: string;
    system: string;
    prin: string;
    app: string;
    user?: string;
  };
  clientInfoId: string;
  orgId?: string;
  binSequences: MagicKeyValue[];
  additionalFields: AdditionalFieldsMapType[];
}

export interface AdditionalFieldsMapType {
  name: string;
  active?: boolean;
  value?: string;
  description?: string;
}

export interface UpdateClientConfigurationArgs
  extends AddClientConfigurationRequest {}

export interface RadioOptions {
  all: boolean;
  custom: boolean;
}

export interface FormValue {
  clientRadio: RadioOptions;
  clientId?: string;
  systemId?: string;
  systemRadio: RadioOptions;
  principleId?: string;
  principleRadio: RadioOptions;
  agentId?: string;
  agentRadio: RadioOptions;
  description?: string;
}

export type FormKy = keyof FormValue;

export type RadioFieldKy = Extract<
  FormKy,
  'clientRadio' | 'systemRadio' | 'principleRadio' | 'agentRadio'
>;

export interface SectionItem {
  id: string;
  title: string;
  Component: React.FC<unknown>;
}

export interface ClientConfigId {
  clientConfigId: string;
}

export interface ChangeSearchValue {
  searchValue?: string;
}

export interface ChangeTextSearchValue {
  textSearchValue?: string;
}

interface RuleSetsProp {
  name: string;
  description: string;
  value: string;
  version: string;
  /**
   * effectiveDate format : YYYY-MM-DD
   * */
  effectiveDate?: string;
}

interface FunctionRuleMappingItem {
  functionName: string;
  description: string;
  currentRuleName: string;
  currentRuleVersion: string;
  nextRuleName: string;
  nextRuleVersion: string;
  /**
   * format returned YYYY-MM-DD
   * */
  nextRuleEffectiveDate: string;
}

export interface ActionTypes {
  actionTypeName: string;
  nextWorkDateVariable: string;
  memoText: string;
  letterId: string;
  externalStatusReason: string;
  externalStatusCode: string;
  chronicleBehaviorIdentifier: string;
}
export interface CallResultProp {
  actionCode: string;
  actionCodeDescription: string;
  actionTypes: ActionTypes[];
  countAsContact: boolean;
  active: boolean;
}

export interface AdjustmentTransactionItem {
  originalTranCode: string;
  adjustmentDescription: string;
  adjustmentTranCode: string;
  adjustmentBatchCode: string;
}

export interface AdjustmentGroupItem {
  groupId: string;
  groupName: string;
  active: boolean;
}

export interface ClusterQueueMapping {
  clusterName: string;
  queue: string;
}

export interface DebtManagementCompany {
  companyName?: string;
  companyAddressLine1?: string;
  companyAddressLine2?: string;
  companyCity?: string;
  companyPhone?: string;
  companyFax?: string;
  companyContact?: string;
  companyState?: string;
  companyZip?: string;
  companyEmail?: string;
  active?: false;
}

export interface PromiseConfig {
  name: string;
  value: string;
}

export interface ClientInfoLetterTemplate {
  letterId: string;
  letterDescription: string;
  letterGroup?: string;
  letterCode: string;
  variableLetter: boolean;
  captionMapping?: MagicKeyValue;
}

export interface UpdateClientInfoArgs {
  /**
   * rulesets key use to add/update/delete for Function Rule Mapping
   * */
  rulesets?: RuleSetsProp[];
  /**
   * actioncodes key use to add/update/delete function custom Call Results
   * */
  actionCodes?: CallResultProp[];
  adjustments?: {
    adjustmentCodes?: AdjustmentTransactionItem[];
    adjustmentGroups?: AdjustmentGroupItem[];
  };
  clusterQueueMappings?: ClusterQueueMapping[];

  debtMgmtCompanyInfo?: DebtManagementCompany[];
  promiseConfigList?: PromiseConfig[];
  clientInfoLetterTemplate?: ClientInfoLetterTemplate[];
  updateAction?: 'delete';
}

export interface UpdateClientInfoRequestBody
  extends CommonRequest,
    UpdateClientInfoArgs {
  clientInfoId: string;
}

export interface UpdateClientInfoResponse {
  responseStatus: 'success' | 'failure ';
  fieldKey: string;
  responseMessage: string;
}

// ----------------------GET CLIENT INFO BY ID ------------------------------

// ----------------RESPONSE CLIENT CONFIG BY ID ----------------------------
export interface ClientInfoByIdResponse {
  responseStatus: 'success' | 'failure';
  clientInfo: {
    emailTemplates: Object;
    clientInfoLetterTemplate: ClientInfoLetterTemplate[];
    actionCodes: CallResultProp[];
    adjustments: {
      adjustmentCodes?: AdjustmentTransactionItem[];
      adjustmentGroups?: AdjustmentGroupItem[];
    };
    debtMgmtCompanyInfo: DebtManagementCompany[];
    clusterQueueMappings: ClusterQueueMapping[];
    promiseConfigList: PromiseConfig[];
    rulesetsConfig: FunctionRuleMappingItem[];
    rulesets: RuleSetsProp[];
  };
  secureInfo: any;
}

export type GetClientInfoResponseKey =
  keyof ClientInfoByIdResponse['clientInfo'];
export interface GetClientInfoRequestBody extends CommonRequest {
  clientInfoId: string;
  fieldsToInclude: GetClientInfoResponseKey[];
  enableFallback: boolean;
}
export interface GetSecureInfoRequestBody extends CommonRequest {
  clientInfoId: string;
}

export interface GetClientInfoArgs {
  dataType: GetClientInfoResponseKey;
  fieldsToQuery?: GetClientInfoResponseKey | GetClientInfoResponseKey[];
}

export type GetClientInfoReturned = Partial<
  ClientInfoByIdResponse['clientInfo']
>;

export type GetSecureInfoReturned = Partial<
  ClientInfoByIdResponse['secureInfo']
>;

export enum CollectionsClientConfigGroup {
  ClientConfig = 'clientConfig',
  CallerAccountEntitlements = 'callerAccountEntitlements',
  CodeToText = 'codeToText'
}

export interface ITimerConfig {
  name: string;
  value: string;
  showMessageFlag?: boolean;
  message?: string;
}

export interface IRegulatoryConfig {
  'send-letter': {
    state: string;
    letterNumber: string;
  };
  'call-frequency': {
    state: string;
    maxPhoneContact: string;
    interval1: string;
    maxOtherPhoneContact: string;
    interval2: string;
  };
}

export interface IDeceasedConfig {
  code: string;
  description: string;
  active?: boolean;
  priorityOrder?: string;
}

export interface IPayByPhoneConfig {
  maxDay: string;
  paymentAgreementMessageWithConvenienceFee: string;
  paymentAgreementMessageWithoutConvenienceFee: string;
}

export interface ISettlementPaymentConfig {
  agreementMessageWithConvenienceFee: string;
  agreementMessageWithoutConvenienceFee: string;
  agreementMessage: string;
}
export interface IAccountExternalStatusConfig {
  currentStatusChange: MagicKeyValue;
  reasonCodeBasedOnStatusChange: MagicKeyValue;
}

export interface ICodeToTextConfig {
  transactionCode: {
    code: string;
    description: string;
  }[];
  letterCode: {
    code: string;
    description: string;
  }[];
}

export interface IGetCollectionsClientConfigServiceArgs {
  common: {
    clientNumber?: string;
    system?: string;
    prin?: string;
    agent?: string;
    user?: string;
    app?: string;
    org?: string;
    privileges?: string[];
  };
  selectConfig?: CollectionsClientConfigGroup;
  selectCspa?: string;
  selectComponent?: string;
}

export interface TriggerSaveClientConfigRequest {
  common: {
    user?: string;
    app?: string;
    org?: string;
    privileges?: string[];
  };
  defaultCspa?: string;
  callerAccountEntitlements?: {
    cspas: CSPAClientConfigType[];
  };
  clientConfig?: {
    cspas: CSPAClientConfigType[];
  };
  codeToText?: {
    cspas: CSPAClientConfigType[];
  };
}

export type ClientConfigValueType = Partial<
  GetCollectionsClientConfigResponse | undefined
>;

export interface IClientConfigComponent
  extends IGetCollectionsClientConfigArgs {
  jsonValue: ClientConfigValueType;
}

export interface IClientConfigCSPA {
  cspa: string;
  components: IClientConfigComponent[];
  description?: string;
  active?: boolean;
}

export interface ICollectionsClientConfigItem {
  cspas?: IClientConfigCSPA[];
}

export interface ISaveCollectionsClientConfigArgs
  extends IClientConfigComponent {
  updateAction?: 'delete';
}

export interface ISaveCollectionsClientConfigServiceArgs
  extends IGetCollectionsClientConfigServiceArgs {
  codeToText?: ICollectionsClientConfigItem;
}

export interface IReferenceItem {
  code: string;
  description: string;
  active: boolean;
}

export interface AccountExternalStatus {
  externalStatusCode?: AccountExternalStatusDataType[];
  externalStatusReasonCode?: ReasonCodesDataType[];
}

export interface GetCollectionsClientConfigResponse {
  timerConfig: ITimerConfig;
  regulatoryConfig: IRegulatoryConfig;
  deceasedConfig: IDeceasedConfig;
  payByPhoneConfig: IPayByPhoneConfig;
  settlementPaymentConfig: ISettlementPaymentConfig;
  accountExternalStatusConfig: IAccountExternalStatusConfig;
  moveToQueues: IReferenceItem[];
  deceasedContact: IReferenceItem[];
  delinquencyReason: IReferenceItem[];
  accountExternalStatus: AccountExternalStatus;
  'send-letter': SendLetterConfigData[];
  memos: MemoConfigItem;
}

export type IGetCollectionsClientConfigPayload = ClientConfigValueType;

export interface IGetCollectionsClientConfigArgs {
  storeId?: string;
  component:
    | 'component_timer_config'
    | 'regulatory-config'
    | 'component_deceased_config'
    | 'component_pay_by_phone_config'
    | 'component_settlement_payment_config'
    | 'component_account_external_status_config'
    | 'component_reference_list_config'
    | 'component_code_to_text_config'
    | 'component_authentication'
    | 'component_memos'
    | 'code_to_text_config'
    | 'ods-code-dictionary-admin-component'
    | 'account-external-status-component';
}

export interface ICollectionsClientConfigs {
  configs: {
    clientConfig?: ICollectionsClientConfigItem;
    callerAccountEntitlements?: ICollectionsClientConfigItem;
    codeToText?: ICollectionsClientConfigItem;
  };
}

export interface SaveConfigThunkPropsType {
  errorMsgInline?: string;
  toastMsg: string;
  nameAction: string;
}

export interface GetLastHistoryPayload extends ClientConfigurationHistory {}

export interface GetLastHistoryArgs {
  cspaList?: ConfigData[];
  categoryList?: string[];
}

export interface IGetAccessConfigArgs {
  common: {
    x500Id: string;
    privileges: string[];
  };
}
