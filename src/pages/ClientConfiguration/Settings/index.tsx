import React, { useCallback, useEffect, useMemo, useState } from 'react';
import {
  Modal,
  ModalBody,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// components
import Sections from './Sections';
import TopInformation from './TopInformation';

// Actions
import { selectConfig, selectOpenSettings } from '../_redux/selectors';
import { clientConfigurationActions } from '../_redux/reducers';

// Helper
import { dispatchClosingModalEvent } from '../helpers';

// Const
import { I18N_CLIENT_CONFIG } from 'app/constants/i18n';
import { SECTION_TAB_EVENT, DEFAULT_TOP_HEIGHT } from '../constants';
import { selectGetAccountExternalStatusLoading as selectLoadingAccountExternalStatus } from '../AccountExternalStatus/_redux/selectors';
import { selectAdjustmentConfigurationLoading as selectLoadingAdjustmentConfiguration } from '../AdjustmentTransaction/_redux/selectors';
import { selectLoading as selectLoadingAuthentication } from '../Authentication/_redux/selectors';
import { selectLoading as selectLoadingCallResult } from '../CallResult/_redux/selector';
import { selectedLoadingClientContactInformation } from '../ClientContactInformation/_redux/selectors';
import { selectClusterLoading } from '../ClusterToQueueMapping/_redux/selectors';
import { selectedGridLoading as selectedLoadingCodeToText } from '../CodeToText/_redux/selectors';
import { selectorRootLoading as selectorLoadingDebtManagementCompanies } from '../DebtManageCompanies/_redux/selectors';
import { selectFunctionRuleMappingListLoading as selectLoadingFunctionRuleMapping } from '../FunctionRuleMapping/_redux/selectors';
import { selectMemosConfigurationLoading as selectLoadingMemosConfiguration } from '../Memos/_redux/selectors';
import { selectLetterConfigurationLoading } from '../Letter/_redux/selectors';
import { selectLoading as selectedLoadingPayByPhone } from '../PayByPhone/_redux/selectors';
import { selectLoading as selectedLoadingSettlementPayment } from '../SettlementPayment/_redux/selectors';
import { selectLoading as selectedLoadingPromiseToPay } from '../PromiseToPay/_redux/selectors';
import { selectLoading as selectedLoadingQueueAndRoleMapping } from '../QueueAndRoleMapping/_redux/selectors';
import { selectedReferenceLoading } from '../References/_redux/selectors';
import { selectLetterConfigurationLoading as selectLoadingSendLetterConfiguration } from '../SendLetter/_redux/selectors';
import { selectLoading as selectedLoadingTalkingPoint } from '../TalkingPoint/_redux/selector';
import { selectLoading as selectedLoadingTimer } from '../Timer/_redux/selectors';

const Settings: React.FC = () => {
  const dispatch = useDispatch();

  const { t } = useTranslation();

  const testId = 'clientConfig_setting';

  const isOpen = useSelector(selectOpenSettings);

  const selectedConfig = useSelector(selectConfig);

  // Loading
  const isLoadingAccExternalStatus = useSelector(
    selectLoadingAccountExternalStatus
  );

  const isLoadingAdjusConfig = useSelector(
    selectLoadingAdjustmentConfiguration
  );

  const isLoadingAuthentication = useSelector(selectLoadingAuthentication);

  const isLoadingCallResult = useSelector(selectLoadingCallResult);

  const isLoadingClientContactInfo = useSelector(
    selectedLoadingClientContactInformation
  );

  const isLoadingCluster = useSelector(selectClusterLoading);

  const isLoadingCodeToText = useSelector(selectedLoadingCodeToText);

  const isLoadingDebtManagementCompanies = useSelector(
    selectorLoadingDebtManagementCompanies
  );

  const isLoadingFunctionRuleMapping = useSelector(
    selectLoadingFunctionRuleMapping
  );

  const isLoadingMemosConfiguration = useSelector(
    selectLoadingMemosConfiguration
  );

  const isLoadingLetterConfiguration = useSelector(
    selectLetterConfigurationLoading
  );

  const isLoadingPayByPhone = useSelector(selectedLoadingPayByPhone);

  const isLoadingSettlementPayment = useSelector(
    selectedLoadingSettlementPayment
  );

  const isLoadingPromiseToPay = useSelector(selectedLoadingPromiseToPay);

  const isLoadingQueueAndRoleMapping = useSelector(
    selectedLoadingQueueAndRoleMapping
  );

  const isLoadingReference = useSelector(selectedReferenceLoading);

  const isLoadSendLetterConfiguration = useSelector(
    selectLoadingSendLetterConfiguration
  );

  const isLoadingTalkingPoint = useSelector(selectedLoadingTalkingPoint);

  const isLoadingTimer = useSelector(selectedLoadingTimer);

  const isLoading = useMemo(() => {
    return (
      isLoadingAccExternalStatus ||
      isLoadingAdjusConfig ||
      isLoadingAuthentication ||
      isLoadingCallResult ||
      isLoadingClientContactInfo ||
      isLoadingCluster ||
      isLoadingCodeToText ||
      isLoadingDebtManagementCompanies ||
      isLoadingFunctionRuleMapping ||
      isLoadingMemosConfiguration ||
      isLoadingLetterConfiguration ||
      isLoadingPayByPhone ||
      isLoadingSettlementPayment ||
      isLoadingPromiseToPay ||
      isLoadingQueueAndRoleMapping ||
      isLoadingReference ||
      isLoadSendLetterConfiguration ||
      isLoadingTalkingPoint ||
      isLoadingTimer
    );
  }, [
    isLoadingAccExternalStatus,
    isLoadingAdjusConfig,
    isLoadingAuthentication,
    isLoadingCallResult,
    isLoadingClientContactInfo,
    isLoadingCluster,
    isLoadingCodeToText,
    isLoadingDebtManagementCompanies,
    isLoadingFunctionRuleMapping,
    isLoadingMemosConfiguration,
    isLoadingLetterConfiguration,
    isLoadingPayByPhone,
    isLoadingSettlementPayment,
    isLoadingPromiseToPay,
    isLoadingQueueAndRoleMapping,
    isLoadingReference,
    isLoadSendLetterConfiguration,
    isLoadingTalkingPoint,
    isLoadingTimer
  ]);

  const [topInfoHeight, setTopInfoHeight] = useState(DEFAULT_TOP_HEIGHT);

  const handleOnCloseModal = () => {
    const { isHavingTabListener } = window.clientConfiguration || {};
    if (isHavingTabListener) {
      dispatchClosingModalEvent();
    } else {
      handleCloseModal();
    }
  };

  const handleCloseModal = useCallback(() => {
    dispatch(clientConfigurationActions.toggleSettingsModal());
  }, [dispatch]);

  /**
   * Listen close modal from closed event inside section's component
   * and execute close modal action
   *
   */
  useEffect(() => {
    window.addEventListener(SECTION_TAB_EVENT.CLOSE_MODAL, handleCloseModal);

    return () => {
      window.removeEventListener(
        SECTION_TAB_EVENT.CLOSE_MODAL,
        handleCloseModal
      );
    };
  }, [handleCloseModal]);

  const handleSetTopInformationHeight = useCallback((value: string) => {
    setTopInfoHeight(value);
  }, []);

  if (!isOpen) return null;

  return (
    <Modal
      full
      show={isOpen}
      enforceFocus={false}
      dataTestId={testId}
      loading={isLoading}
    >
      <ModalHeader
        border
        closeButton
        onHide={handleOnCloseModal}
        dataTestId={`${testId}_header`}
      >
        <ModalTitle
          className="d-flex align-items-center"
          dataTestId={`${testId}_title`}
        >
          {t(I18N_CLIENT_CONFIG.CLIENT_CONFIG_SETTINGS)}
        </ModalTitle>
      </ModalHeader>
      <ModalBody
        className="overflow-hidden"
        noPadding
        dataTestId={`${testId}_body`}
      >
        <TopInformation
          data={selectedConfig}
          setHeight={handleSetTopInformationHeight}
        />
        <Sections topInfoHeight={topInfoHeight} />
      </ModalBody>
    </Modal>
  );
};

export default Settings;
