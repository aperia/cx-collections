import React, { useEffect, useRef } from 'react';
import { View } from 'app/_libraries/_dof/core';
import { useElementSize } from 'app/hooks';
import { InlineMessage } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';
import { takeWarningMsg } from '../_redux/selectors';
import { useSelector } from 'react-redux';

export interface TopInformationProps {
  data?: MagicKeyValue;
  setHeight?: (height: string) => void;
}

const TopInformation: React.FC<TopInformationProps> = ({ data, setHeight }) => {
  const containerRef = useRef<HTMLDivElement>(null);

  const { t } = useTranslation();

  const status = useCheckClientConfigStatus();

  const warningMsg = useSelector(takeWarningMsg);

  const containerElement = useElementSize(containerRef);

  useEffect(() => {
    const containerHeight = containerElement
      ? `${
          containerElement?.height + containerElement?.x + containerElement?.y
        }`
      : '88';

    containerElement && setHeight && setHeight(containerHeight);
  }, [containerElement, setHeight]);

  return (
    <div className="pt-8 pb-24 px-24 bg-light-l20" ref={containerRef}>
      {status && (
        <InlineMessage
          id="clientConfig__top-information__inactive-message"
          className="mt-16 mr-8 mb-8"
          dataTestId="topInformation_inactive-message"
          variant="warning"
        >
          {t('txt_cspa_inactive_message')}
        </InlineMessage>
      )}

      {warningMsg && (
        <InlineMessage
          id="clientConfig__top-information__warning-message"
          className="mt-16 mb-8"
          dataTestId="topInformation_warning-message"
          variant="warning"
        >
          {t(warningMsg)}
        </InlineMessage>
      )}
      <View
        id="clientConfigTopInfo"
        formKey="clientConfigTopInfo"
        descriptor="clientConfigTopInfo"
        value={data}
      />
    </div>
  );
};

export default TopInformation;
