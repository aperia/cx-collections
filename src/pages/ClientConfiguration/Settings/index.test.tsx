import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { screen } from '@testing-library/react';

import Settings from '.';

import { I18N_CLIENT_CONFIG } from 'app/constants/i18n';
import { TopInformationProps } from './TopInformation';
import userEvent from '@testing-library/user-event';
import { SectionsProps } from './Sections';

// Actions
import { clientConfigurationActions } from '../_redux/reducers';
import { SECTION_TAB_EVENT } from '../constants';

// helpers
import * as helpers from '../helpers';

const initialState: Partial<RootState> = {
  clientConfiguration: {
    settings: { isOpen: true }
  }
};

jest.mock('./TopInformation', () => ({ setHeight }: TopInformationProps) => (
  <div
    data-testid="TopInformation_setHeight"
    onClick={() => setHeight!('100')}
  />
));

jest.mock('./Sections', () => ({ topInfoHeight }: SectionsProps) => (
  <div data-testid="Sections_topInfoHeight">{topInfoHeight}</div>
));

const spyClientConfiguration = mockActionCreator(clientConfigurationActions);

describe('Render', () => {
  it('should render', () => {
    renderMockStore(<Settings />, { initialState });

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.CLIENT_CONFIG_SETTINGS)
    ).toBeInTheDocument();
  });

  it('should render with empty value', () => {
    renderMockStore(<Settings />, { initialState: {} });

    expect(
      screen.queryByText(I18N_CLIENT_CONFIG.CLIENT_CONFIG_SETTINGS)
    ).toBeNull();
  });
});

describe('Actions', () => {
  describe('handleOnCloseModal', () => {
    it('when has no close modal listener', () => {
      const mockToggleSettingsModal = spyClientConfiguration(
        'toggleSettingsModal'
      );
      const dispatchTabChangingTabEvent = jest.fn();
      jest
        .spyOn(helpers, 'dispatchClosingModalEvent')
        .mockImplementation(dispatchTabChangingTabEvent);

      window.clientConfiguration = undefined;

      renderMockStore(<Settings />, { initialState });

      userEvent.click(screen.getByRole('button'));

      expect(dispatchTabChangingTabEvent).not.toBeCalled();
      expect(mockToggleSettingsModal).toBeCalled();
    });

    it('when has close modal  listener', () => {
      const mockToggleSettingsModal = spyClientConfiguration(
        'toggleSettingsModal'
      );
      const dispatchTabChangingTabEvent = jest.fn();
      jest
        .spyOn(helpers, 'dispatchClosingModalEvent')
        .mockImplementation(dispatchTabChangingTabEvent);

      window.clientConfiguration = { isHavingTabListener: true };

      renderMockStore(<Settings />, { initialState });

      userEvent.click(screen.getByRole('button'));

      expect(dispatchTabChangingTabEvent).toBeCalled();
      expect(mockToggleSettingsModal).not.toBeCalled();
    });
  });

  it('handleSetTopInformationHeight', () => {
    renderMockStore(<Settings />, { initialState });

    userEvent.click(screen.getByTestId('TopInformation_setHeight'));

    expect(screen.getByTestId('Sections_topInfoHeight').textContent).toEqual(
      '100'
    );
  });

  it('handleCloseModal', () => {
    const mockAction = spyClientConfiguration('toggleSettingsModal');

    renderMockStore(<Settings />, { initialState });

    window.dispatchEvent(new Event(SECTION_TAB_EVENT.CLOSE_MODAL));

    expect(mockAction).toBeCalled();
  });
});
