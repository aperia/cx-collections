import React, {
  Suspense,
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
  useState
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ExtraStaticTabs from 'app/_libraries/_dls/components/VerticalTabs';
import isEmpty from 'lodash.isempty';
import isNull from 'lodash.isnull';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import classNames from 'classnames';

// Component
import { SimpleBar } from 'app/_libraries/_dls/components';
import LazyLoadingFallback from 'app/components/LazyLoadingFallback';

// helpers
import { dispatchTabChangingTabEvent, mapSectionList } from '../helpers';

// Actions
import { clientConfigurationActions } from '../_redux/reducers';
import { selectSections, selectToggleSettings } from '../_redux/selectors';

// Const
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { EMPTY_STRING } from 'app/constants';
import { SECTION_TAB_EVENT } from '../constants';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export interface SectionsProps {
  topInfoHeight?: string;
}

const Sections: React.FC<SectionsProps> = ({ topInfoHeight }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [tab, setTabActive] = useState<string>();
  const [collapsing, setCollapsing] = useState<boolean>(false);
  const unSettledTab = useRef<string>(EMPTY_STRING);
  const divRef = useRef<HTMLDivElement | null>(null);
  const testId = 'clientConfig_setting_tabs';

  const sections = useSelector(selectSections);
  const toggle = useSelector(selectToggleSettings);
  const data = mapSectionList(sections);

  const handleOnSelect = (eventKey: string | null) => {
    if (tab === eventKey) return;
    if (isNull(eventKey)) return;
    unSettledTab.current = eventKey;
    const { isHavingTabListener } = window.clientConfiguration || {};

    /**
     * If there is have any section's component listen tab change event,
     * then dispatch a changing tab event, else just set the active tab
     */
    if (isHavingTabListener) {
      dispatchTabChangingTabEvent();
    } else {
      setTabActive(unSettledTab.current);
    }
  };

  const handleChangeTab = useCallback(() => {
    setTabActive(unSettledTab.current);
  }, []);

  const handleToggleCollapsed = useCallback(
    collapsed => {
      dispatch(clientConfigurationActions.toggleSetting(collapsed));

      setCollapsing(true);
      const collapsingTimeout = setTimeout(() => {
        setCollapsing(false);
      }, 500);

      return () => clearTimeout(collapsingTimeout);
    },
    [dispatch]
  );

  /**
   * Control tab change, use to update the active tav,
   * when there are have any action come from other section component
   */
  useEffect(() => {
    window.addEventListener(SECTION_TAB_EVENT.CHANGE_TAB, handleChangeTab);

    return () => {
      window.removeEventListener(SECTION_TAB_EVENT.CHANGE_TAB, handleChangeTab);
    };
  }, [handleChangeTab]);

  useEffect(() => {
    dispatch(clientConfigurationActions.getSectionList({}));
    dispatch(clientConfigurationActions.checkSecureClientConfig());
  }, [dispatch]);

  useEffect(() => {
    if (data.length && isEmpty(tab)) {
      const [defaultTab] = data;
      setTabActive(defaultTab.id);
    }
  }, [data, tab]);

  useLayoutEffect(() => {
    divRef.current &&
      (divRef.current.style.height = `calc(100% - ${topInfoHeight}px)`);
  }, [topInfoHeight]);

  useEffect(() => {
    // whenever changing tab, will clear api error occur when deleting configs
    dispatch(
      apiErrorNotificationAction.clearApiErrorData({
        forSection: 'inClientConfigurationFeatureHeader',
        storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION
      })
    );
  }, [dispatch, tab]);

  return (
    <div className="config-setting" ref={divRef}>
      <ExtraStaticTabs
        activeKey={tab}
        onSelect={handleOnSelect}
        className="scrolled-nav"
        unmountOnExit
        mountOnEnter
        onToggleCollapsed={handleToggleCollapsed}
        collapsed={toggle}
        expandMessage={t(I18N_COMMON_TEXT.EXPAND)}
        collapseMessage={t(I18N_COMMON_TEXT.COLLAPSE)}
        dataTestId={testId}
      >
        <div
          className={classNames('tab-left', {
            'is-collapsed': toggle,
            'is-collapsing': collapsing
          })}
        >
          <SimpleBar>
            <ExtraStaticTabs.Nav collapsed={toggle}>
              {data.map(({ id, title }) => (
                <ExtraStaticTabs.Nav.Item key={id}>
                  <ExtraStaticTabs.Nav.Link eventKey={id}>
                    {t(title)}
                  </ExtraStaticTabs.Nav.Link>
                </ExtraStaticTabs.Nav.Item>
              ))}
            </ExtraStaticTabs.Nav>
          </SimpleBar>
        </div>

        <ExtraStaticTabs.Content>
          {data.map(({ id, Component }) => {
            return (
              <ExtraStaticTabs.Pane eventKey={id} key={id}>
                <Suspense fallback={<LazyLoadingFallback />}>
                  <Component />
                </Suspense>
              </ExtraStaticTabs.Pane>
            );
          })}
        </ExtraStaticTabs.Content>
      </ExtraStaticTabs>
    </div>
  );
};

export default Sections;
