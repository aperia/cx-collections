import React, { SyntheticEvent } from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { fireEvent, screen } from '@testing-library/react';
import { act } from 'react-dom/test-utils';

import Sections from './Sections';

import { SECTION_TAB_EVENT } from '../constants';
import { TabsProps } from 'app/_libraries/_dls/components/VerticalTabs';

// Actions
import { clientConfigurationActions } from '../_redux/reducers';
import userEvent from '@testing-library/user-event';

// helpers
import * as helpers from '../helpers';
import { EMPTY_STRING } from 'app/constants';

jest.mock('app/_libraries/_dls/components/VerticalTabs', () => {
  const Nav = ({ children }: TabsProps) => {
    return <div>{children}</div>;
  };

  Nav.Item = ({ children }: TabsProps) => {
    return <div>{children}</div>;
  };

  Nav.Link = ({ children }: TabsProps) => {
    return <div>{children}</div>;
  };

  const VerticalTabs = ({
    onToggleCollapsed,
    onSelect,
    children
  }: TabsProps) => {
    return (
      <div>
        <input
          data-testid="VerticalTabs_onToggleCollapsed"
          onChange={(e: any) => onToggleCollapsed!(e.target.collapsed)}
        />
        <div
          data-testid="VerticalTabs_onToggleCollapsed_return"
          onClick={() => {
            // eslint-disable-next-line @typescript-eslint/ban-ts-comment
            //@ts-ignore
            onToggleCollapsed!(false)?.();
          }}
        />
        <input
          data-testid="VerticalTabs_onSelect"
          onChange={(e: any) =>
            onSelect!(
              e.target.eventKey,
              {} as unknown as SyntheticEvent<unknown, Event>
            )
          }
        />
        {children}
      </div>
    );
  };

  VerticalTabs.Nav = Nav;
  VerticalTabs.Pane = ({ children }: TabsProps) => {
    return <div>{children}</div>;
  };
  VerticalTabs.Content = ({ children }: TabsProps) => {
    return <div>{children}</div>;
  };

  return {
    __esModule: true,
    default: VerticalTabs
  };
});

const spyClientConfiguration = mockActionCreator(clientConfigurationActions);

const sections: RefDataValue[] = [
  { value: 'value_1', description: 'description_1' },
  { value: 'value_2', description: 'description_2' }
];

const initialState: Partial<RootState> = {
  clientConfiguration: {
    settings: {
      sections
    }
  }
};

describe('Render', () => {
  it('should render', () => {
    const { container } = renderMockStore(<Sections />, { initialState });

    expect(container.querySelector('.config-setting')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  describe('handleOnSelect', () => {
    it('when has no tab listener', () => {
      const dispatchTabChangingTabEvent = jest.fn();
      jest
        .spyOn(helpers, 'dispatchTabChangingTabEvent')
        .mockImplementation(dispatchTabChangingTabEvent);

      window.clientConfiguration = undefined;

      renderMockStore(<Sections />, { initialState });

      act(() => {
        fireEvent.change(screen.getByTestId('VerticalTabs_onSelect'), {
          target: { value: sections[0].value, eventKey: sections[0].value }
        });
      });

      expect(dispatchTabChangingTabEvent).not.toBeCalled();

      act(() => {
        fireEvent.change(screen.getByTestId('VerticalTabs_onSelect'), {
          target: { value: 'null', eventKey: null }
        });
      });

      expect(dispatchTabChangingTabEvent).not.toBeCalled();

      act(() => {
        window.dispatchEvent(new Event(SECTION_TAB_EVENT.CHANGE_TAB));
      });

      act(() => {
        fireEvent.change(screen.getByTestId('VerticalTabs_onSelect'), {
          target: { value: 'EMPTY_STRING', eventKey: EMPTY_STRING }
        });
      });

      expect(dispatchTabChangingTabEvent).not.toBeCalled();

      act(() => {
        fireEvent.change(screen.getByTestId('VerticalTabs_onSelect'), {
          target: { value: sections[1].value, eventKey: sections[1].value }
        });
      });
    });

    it('when has no tab listener', () => {
      const dispatchTabChangingTabEvent = jest.fn();
      jest
        .spyOn(helpers, 'dispatchTabChangingTabEvent')
        .mockImplementation(dispatchTabChangingTabEvent);

      window.clientConfiguration = { isHavingTabListener: true };

      renderMockStore(<Sections />, { initialState });

      act(() => {
        fireEvent.change(screen.getByTestId('VerticalTabs_onSelect'), {
          target: { value: sections[1].value, eventKey: sections[1].value }
        });
      });
      expect(dispatchTabChangingTabEvent).toBeCalled();
    });
  });

  it('handleToggleCollapsed', () => {
    const mockAction = spyClientConfiguration('toggleSetting');

    jest.useFakeTimers();
    renderMockStore(<Sections />, { initialState });
    act(() => {
      fireEvent.change(screen.getByTestId('VerticalTabs_onToggleCollapsed'), {
        target: { value: 'value', collapsed: true }
      });

      jest.runAllTimers();
    });

    userEvent.click(
      screen.getByTestId('VerticalTabs_onToggleCollapsed_return')
    );

    expect(mockAction).toBeCalledWith(true);
  });
});
