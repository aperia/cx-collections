import React from 'react';
import { renderMockStore } from 'app/test-utils';
import { screen } from '@testing-library/react';

import TopInformation from './TopInformation';

import * as useElementResize from 'app/hooks/useElementResize';

const initialState: Partial<RootState> = {
  clientConfiguration: {
    warningMessage: 'warning'
  }
};

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

describe('Render', () => {
  it('should render', () => {
    renderMockStore(<TopInformation />, { initialState });

    expect(screen.getByTestId('clientConfigTopInfo')).toBeInTheDocument();
  });

  it('should render when element has size', () => {
    const spy = jest
      .spyOn(useElementResize, 'useElementSize')
      .mockImplementation(() => ({
        bottom: 0,
        height: 100,
        left: 0,
        right: 0,
        top: 0,
        width: 0,
        x: 0,
        y: 0,
        toJSON: () => undefined
      }));

    const setHeight = jest.fn();

    renderMockStore(<TopInformation setHeight={setHeight} />, { initialState });

    expect(screen.getByTestId('clientConfigTopInfo')).toBeInTheDocument();
    expect(setHeight).toBeCalledWith('100');

    spy.mockReset();
    spy.mockRestore();
  });
});
