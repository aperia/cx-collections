import React from 'react';
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import FormCluster from './FormCluster';
import { fireEvent, screen } from '@testing-library/react';
import { clusterToQueueMappingActions } from './_redux/reducers';

const clusterToQueueMappingActionsSpy = mockActionCreator(
  clusterToQueueMappingActions
);

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('./BodyModalCluster', () => ({
  __esModule: true,
  default: () => (
    <div>BodyModalCluster</div>
  )
}))

describe('Test FormCluster component', () => {
  it('render component', () => {
    renderMockStoreId(<FormCluster />);

    expect(screen.getByText('txt_add_cluster')).toBeInTheDocument();
  });

  it('render component', () => {
    renderMockStoreId(<FormCluster />, {
      initialState: {
        clientConfigClusterToQueueMapping: {
          loading: false,
          loadingForm: false,
          cluster: { clusterName: 'clusterName' },
          clusterList: [],
          error: '',
          errorForm: '',
          isOpenModalForm: false,
          isOpenModalDelete: false,
          sortBy: { id: 'clusterName', order: undefined },
          queueList: []
        }
      }
    });

    expect(screen.getByText('txt_add_cluster')).toBeInTheDocument();
  });

  it('click add btn', () => {
    const openModalFormCluster = clusterToQueueMappingActionsSpy(
      'openModalFormCluster'
    );
    const setEditData = clusterToQueueMappingActionsSpy('setEditData');

    renderMockStoreId(<FormCluster />);

    const addBtn = screen.getByText('txt_add_cluster');
    fireEvent.click(addBtn);

    expect(openModalFormCluster).toBeCalled();
    expect(setEditData).toBeCalled();
  });

  it('handleCloseModal', () => {
    const cancelModalFormCluster = clusterToQueueMappingActionsSpy(
      'cancelModalFormCluster'
    );

    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(<FormCluster />, {
      initialState: {
        clientConfigClusterToQueueMapping: {
          loading: false,
          loadingForm: false,
          cluster: { clusterName: 'clusterName' },
          clusterList: [],
          error: '',
          errorForm: '',
          isOpenModalForm: true,
          isOpenModalDelete: false,
          sortBy: { id: 'clusterName', order: undefined },
          queueList: []
        }
      }
    });
    jest.runAllTimers();

    const cancelBtn = wrapper.baseElement.querySelector(
      'i[class="icon icon-close"]'
    );
    fireEvent.click(cancelBtn!);

    expect(cancelModalFormCluster).toBeCalled();
  });
});
