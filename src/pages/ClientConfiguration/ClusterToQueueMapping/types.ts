import { SortType } from 'app/_libraries/_dls/components';

export interface ClusterToQueueMappingItem {
  clusterName?: string;
  clusterQueueAssignment?: string;
}

export interface QueueItem {
  queueId?: string;
  queueName?: string;
}

export interface GetClusterQueueAssignmentMethodRequest extends CommonRequest {
  fetchSize: string;
}

export interface AddClusterToQueueMappingArgs {
  postData: ClusterToQueueMappingItem[];
  isEditingMode?: boolean;
  addingCluster?: ClusterToQueueMappingItem;
  editingCluster?: ClusterToQueueMappingItem;
}

export interface DeleteClusterToQueueMappingArgs {
  postData: ClusterToQueueMappingItem[];
  deletingCluster?: ClusterToQueueMappingItem;
}

export interface ClusterToQueueMappingState {
  loading: boolean;
  loadingForm: boolean;
  clusterList: ClusterToQueueMappingItem[];
  cluster: ClusterToQueueMappingItem;
  error?: string;
  errorForm?: string;
  isOpenModalForm: boolean;
  isOpenModalDelete: boolean;
  sortBy: SortType;
  queueList: QueueItem[];
}

export interface UpdateClusterToQueueMappingArgs {
  data: ClusterToQueueMappingItem[];
  updateAction?: 'delete';
}
