import React, { useEffect } from 'react';
// hooks
import { SimpleBar } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { useDispatch, useSelector } from 'react-redux';
import ApiErrorForHeaderSection from '../ApiErrorSection/ApiErrorForHeaderSection';
// components
import ClusterToQueueMappingGrid from './ClusterToQueueMappingGrid';
// constants
import { I18N_CLUSTER_TO_QUEUE_MAPPING } from './constants';
import { DeleteCluster } from './DeleteCluster';
// redux
import { clusterToQueueMappingActions } from './_redux/reducers';
import { selectSelectedClientConfig } from './_redux/selectors';

const ClusterToQueueMapping = () => {
  const dispatch = useDispatch();

  const { t } = useTranslation();

  const { clientId } = useSelector(selectSelectedClientConfig);

  const testId = 'clientConfig_clusterToQueueMapping';

  useEffect(() => {
    dispatch(clusterToQueueMappingActions.getClusterQueueAssignmentMethod());
  }, [dispatch]);

  return (
    <div className={'position-relative h-100'}>
      <SimpleBar>
        <ApiErrorForHeaderSection
          className="mt-24 ml-24"
          dataTestId={`${testId}_apiError`}
        />
        <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
          <h5 data-testid={genAmtId(testId, 'title', '')}>
            {t(I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_TO_QUEUE_MAPPING)}
          </h5>
          <p
            className="alert alert-warning mt-16"
            style={{ width: 'fit-content' }}
          >
            {t(I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_ALERT_DESCRIPTION, {
              clientId
            })}
          </p>
          <ClusterToQueueMappingGrid />
        </div>
      </SimpleBar>
      <DeleteCluster />
    </div>
  );
};

export default ClusterToQueueMapping;
