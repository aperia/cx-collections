import React from 'react';

// components
import {
  Button,
  Modal,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import BodyModalCluster from './BodyModalCluster';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import isEmpty from 'lodash.isempty';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import { clusterToQueueMappingActions } from './_redux/reducers';
import {
  selectClusterFormModalOpen,
  selectClusterFormLoading,
  selectCurrentRecord
} from './_redux/selectors';

// constants
import { I18N_CLUSTER_TO_QUEUE_MAPPING } from './constants';

//types
import { ClientConfigStatusProps } from '../types';

const FormCluster: React.FC<ClientConfigStatusProps> = ({ status }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const testId = 'clientConfig_clusterToQueueMapping_formCluster';

  const modalOpened = useSelector(selectClusterFormModalOpen);
  const loading = useSelector(selectClusterFormLoading);
  const currentRecord = useSelector(selectCurrentRecord);

  const handleAddCluster = () => {
    batch(() => {
      dispatch(clusterToQueueMappingActions.openModalFormCluster());
      dispatch(clusterToQueueMappingActions.setEditData({}));
    });
  };

  const handleCloseModal = () => {
    dispatch(clusterToQueueMappingActions.cancelModalFormCluster());
  };
  return (
    <>
      <Button
        disabled={status}
        onClick={handleAddCluster}
        size="sm"
        variant="outline-primary"
        dataTestId={`${testId}_addBtn`}
      >
        {t(I18N_CLUSTER_TO_QUEUE_MAPPING.ADD_CLUSTER)}
      </Button>
      <Modal sm show={modalOpened} loading={loading} dataTestId={testId}>
        <ModalHeader
          border
          closeButton
          onHide={handleCloseModal}
          dataTestId={`${testId}_header`}
        >
          <ModalTitle dataTestId={`${testId}_title`}>
            {isEmpty(currentRecord)
              ? t(I18N_CLUSTER_TO_QUEUE_MAPPING.ADD_CLUSTER)
              : t(I18N_CLUSTER_TO_QUEUE_MAPPING.EDIT_CLUSTER)}
          </ModalTitle>
        </ModalHeader>
        <BodyModalCluster />
      </Modal>
    </>
  );
};

export default FormCluster;
