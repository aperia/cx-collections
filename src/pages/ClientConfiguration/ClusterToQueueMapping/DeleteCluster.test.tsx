import React from 'react';
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import { DeleteCluster } from './DeleteCluster';
import { fireEvent, screen } from '@testing-library/react';
import { clusterToQueueMappingActions } from './_redux/reducers';

const clusterToQueueMappingActionsSpy = mockActionCreator(
  clusterToQueueMappingActions
);

describe('Test DeleteCluster component', () => {
  it('render component', () => {
    const { wrapper } = renderMockStoreId(<DeleteCluster />);

    expect(wrapper.container.innerHTML).toEqual('');
  });

  it('handleClose', () => {
    const deleteClusterRequest = clusterToQueueMappingActionsSpy(
      'deleteClusterRequest'
    );

    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(<DeleteCluster />, {
      initialState: {
        clientConfigClusterToQueueMapping: {
          loading: false,
          loadingForm: false,
          cluster: { clusterName: 'clusterName' },
          clusterList: [],
          error: '',
          errorForm: '',
          isOpenModalForm: false,
          isOpenModalDelete: true,
          sortBy: { id: 'clusterName', order: undefined },
          methodRefData: []
        }
      }
    });
    jest.runAllTimers();

    const cancelBtn = wrapper.baseElement.querySelector(
      'i[class="icon icon-close"]'
    );
    fireEvent.click(cancelBtn!);

    expect(deleteClusterRequest).toBeCalled();
  });

  it('handleDelete', () => {
    const triggerDeleteClusterToQueueMapping = clusterToQueueMappingActionsSpy(
      'triggerDeleteClusterToQueueMapping'
    );

    jest.useFakeTimers();
    renderMockStoreId(<DeleteCluster />, {
      initialState: {
        clientConfigClusterToQueueMapping: {
          loading: false,
          loadingForm: false,
          cluster: { clusterName: 'clusterName' },
          clusterList: [],
          error: '',
          errorForm: '',
          isOpenModalForm: false,
          isOpenModalDelete: true,
          sortBy: { id: 'clusterName', order: undefined },
          methodRefData: []
        }
      }
    });
    jest.runAllTimers();

    const deleteBtn = screen.getByText('txt_delete');
    fireEvent.click(deleteBtn!);

    expect(triggerDeleteClusterToQueueMapping).toBeCalled();
  });
});
