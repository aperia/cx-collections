import React from 'react';
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import ClusterToQueueMapping from './index';
import { clusterToQueueMappingActions } from './_redux/reducers';

const clusterToQueueMappingActionsSpy = mockActionCreator(
  clusterToQueueMappingActions
);

describe('Test ClusterToQueueMapping component', () => {
  it('render component', () => {
    const getClusterQueueAssignmentMethod = clusterToQueueMappingActionsSpy(
      'getClusterQueueAssignmentMethod'
    );

    renderMockStoreId(<ClusterToQueueMapping />);

    expect(getClusterQueueAssignmentMethod).toBeCalled();
  });
});
