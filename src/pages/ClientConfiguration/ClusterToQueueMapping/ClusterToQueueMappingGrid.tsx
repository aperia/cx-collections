import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';

// components
import { FailedApiReload, NoDataGrid } from 'app/components';
import {
  ColumnType,
  Grid,
  GridRef,
  Pagination,
  SortType,
  TruncateText
} from 'app/_libraries/_dls/components';
import GridActions from 'pages/__commons/GridActions';
import FormCluster from './FormCluster';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { usePagination } from 'app/hooks';

// helpers
import classNames from 'classnames';

// redux
import { useDispatch, useSelector } from 'react-redux';
import {
  selectClusterGridData,
  selectClusterError,
  selectClusterLoading
} from './_redux/selectors';
import { clusterToQueueMappingActions } from './_redux/reducers';

// constants
import { I18N_CLUSTER_TO_QUEUE_MAPPING } from './constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// types
import { ClusterToQueueMappingItem } from './types';
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface ClusterToQueueMappingGridProps {}

const Header = ({ className }: { className?: string }) => {
  const { t } = useTranslation();
  return (
    <>
      <p
        className={classNames('fw-500', className)}
        data-testid={genAmtId(
          'clientConfig_clusterToQueueMapping_grid',
          'header',
          ''
        )}
      >
        {t(I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_TO_QUEUE_MAPPING_LIST)}
      </p>
    </>
  );
};

const ClusterToQueueMappingGrid: React.FC<ClusterToQueueMappingGridProps> =
  () => {
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const gridRefs = useRef<GridRef>(null);
    const testId = 'clientConfig_clusterToQueueMapping_grid';

    const status = useCheckClientConfigStatus();

    const isLoading = useSelector(selectClusterLoading);
    const isError = useSelector(selectClusterError);
    const data = useSelector(selectClusterGridData);
    const [sortBy, setSortBy] = useState<SortType[]>([]);

    const handleDeleteRequest = useCallback(
      record => {
        dispatch(
          clusterToQueueMappingActions.deleteClusterRequest({ cluster: record })
        );
      },
      [dispatch]
    );

    const handleEditRequest = useCallback(
      record => {
        dispatch(
          clusterToQueueMappingActions.setEditData({
            cluster: record
          })
        );
        dispatch(clusterToQueueMappingActions.openModalFormCluster());
      },
      [dispatch]
    );

    const {
      total,
      currentPage,
      pageSize,
      currentPageSize,
      gridData,
      onPageChange,
      onPageSizeChange
    } = usePagination(data);

    const handleReload = useCallback(() => {
      dispatch(clusterToQueueMappingActions.getClusterToQueueMapping());
    }, [dispatch]);

    useEffect(() => {
      handleReload();
    }, [handleReload]);

    const showPagination = total > pageSize[0];

    const dataColumn = useMemo(() => {
      return [
        {
          id: 'clusterName',
          Header: t(I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_NAME),
          accessor: (col: ClusterToQueueMappingItem) => (
            <TruncateText
              lines={2}
              title={col?.clusterName}
              resizable
              ellipsisMoreText={t(I18N_COMMON_TEXT.MORE)}
              ellipsisLessText={t(I18N_COMMON_TEXT.LESS)}
              className="text-break"
            >
              {col?.clusterName}
            </TruncateText>
          ),
          isSort: true,
          autoWidth: true,
          width: 240
        },
        {
          id: 'clusterQueueAssignment',
          Header: t(I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_QUEUE_ASSIGNMENT),
          accessor: 'clusterQueueAssignment',
          isSort: true,
          autoWidth: true,
          width: 240
        },
        {
          id: 'action',
          Header: t('txt_actions'),
          accessor: (col: ClusterToQueueMappingItem, idx: number) => (
            <GridActions
              status={status}
              onClickEdit={() => handleEditRequest(col)}
              onClickDelete={() => handleDeleteRequest(col)}
              dataTestId={`${idx}_action_${testId}`}
            />
          ),
          width: 147,
          isFixedRight: true,
          cellProps: { className: 'text-center' },
          cellBodyProps: { className: 'text-center' },
          className: 'td-sm'
        }
      ];
    }, [handleDeleteRequest, handleEditRequest, t, status]);

    const handleSort = (sort: SortType) => {
      setSortBy([sort]);
      dispatch(clusterToQueueMappingActions.onChangeSort(sort));
    };

    // reset sort
    useEffect(
      () => () => {
        dispatch(clusterToQueueMappingActions.onChangeSort({ id: '' }));
      },
      [dispatch]
    );

    if (isLoading) return <div className="vh-50" />;

    if (isError)
      return (
        <>
          <Header className="mt-16" />
          <FailedApiReload
            id="cluster-config-request-fail"
            className="mt-80"
            onReload={handleReload}
            dataTestId={`${testId}_failedApi`}
          />
        </>
      );
    if (!data.length)
      return (
        <>
          <Header className="mt-16" />
          <NoDataGrid
            text={t(I18N_CLUSTER_TO_QUEUE_MAPPING.NO_DATA)}
            dataTestId={`${testId}_noData`}
          >
            <div className="mt-24">
              <FormCluster status={status} />
            </div>
          </NoDataGrid>
        </>
      );
    return (
      <>
        <div className="d-flex justify-content-between align-items-center mt-16 ">
          <Header />
          <div className="mr-n8">
            <FormCluster status={status} />
          </div>
        </div>
        <div className="mt-16">
          <Grid
            ref={gridRefs}
            onSortChange={handleSort}
            columns={dataColumn as unknown as ColumnType[]}
            data={gridData}
            sortBy={sortBy}
            dataTestId={`${testId}_grid`}
          />
          {showPagination && (
            <div className="pt-16">
              <Pagination
                totalItem={total}
                pageSize={pageSize}
                pageNumber={currentPage}
                pageSizeValue={currentPageSize}
                compact
                onChangePage={onPageChange}
                onChangePageSize={onPageSizeChange}
                dataTestId={`${testId}_pagination`}
              />
            </div>
          )}
        </div>
      </>
    );
  };

export default ClusterToQueueMappingGrid;
