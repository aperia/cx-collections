import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi } from 'app/test-utils/mocks/mockProperty';
import { AxiosRequestConfig } from 'axios';
import { clusterToQueueMappingService } from './clusterToQueueMappingService';
import { GetClusterQueueAssignmentMethodRequest } from './types';

jest.mock('axios', () => {
  const axios = jest.requireActual('axios');
  const mockAction = (params?: AxiosRequestConfig) => {
    return Promise.resolve({ data: params });
  };
  const axiosMock = (params?: AxiosRequestConfig) => mockAction(params);
  axiosMock.get = (params?: AxiosRequestConfig) => mockAction(params);
  axiosMock.create = axios.create;
  return {
    ...axios,
    __esModule: true,
    default: axiosMock,
    defaults: axiosMock
  };
});

describe('Test clusterToQueueMappingService', () => {
  it('getClusterQueueAssignmentMethod > dont have url', async () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    const requestBody = {
      common: {},
      fetchSize: '10'
    } as GetClusterQueueAssignmentMethodRequest;

    clusterToQueueMappingService.getClusterQueueAssignmentMethod(requestBody);

    expect(mockService).toHaveBeenCalledWith('', requestBody);
  });

  it('getClusterQueueAssignmentMethod > has url', async () => {
    mockAppConfigApi.setup();

    const mockService = mockApiServices.post();

    const requestBody = {
      common: {},
      fetchSize: '10'
    } as GetClusterQueueAssignmentMethodRequest;

    clusterToQueueMappingService.getClusterQueueAssignmentMethod(requestBody);

    expect(mockService).toHaveBeenCalledWith(
      window.appConfig?.api?.clientConfigClusterToQueueMapping
        ?.getClusterQueueAssignmentMethod,
      requestBody
    );
  });
});
