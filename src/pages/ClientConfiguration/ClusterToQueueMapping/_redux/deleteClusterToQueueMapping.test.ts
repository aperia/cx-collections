import {
  mockActionCreator,
  byPassRejected,
  byPassFulfilled
} from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import {
  CLUSTER_GRID_COLUMN_NAME,
  I18N_CLUSTER_TO_QUEUE_MAPPING
} from '../constants';
import { triggerDeleteClusterToQueueMapping } from './deleteClusterToQueueMapping';

const toastSpy = mockActionCreator(actionsToast);

describe('triggerDeleteClusterToQueueMapping', () => {
  const store = createStore(
    rootReducer,
    {
      clientConfiguration: {
        settings: {
          selectedConfig: {
            id: '',
            agentId: 'agentId',
            clientId: 'clientId',
            systemId: 'systemId',
            principleId: 'principleId',
            userId: 'userId'
          }
        }
      },
      mapping: {
        data: {
          clientConfigCluster: {
            clusterName: CLUSTER_GRID_COLUMN_NAME.CLUSTER_NAME,
            clusterQueueAssignment:
              CLUSTER_GRID_COLUMN_NAME.CLUSTER_QUEUE_ASSIGNMENT
          }
        }
      }
    },
    applyMiddleware(thunk)
  );

  const params = {
    postData: [
      {
        clusterName: CLUSTER_GRID_COLUMN_NAME.CLUSTER_NAME,
        clusterQueueAssignment:
          CLUSTER_GRID_COLUMN_NAME.CLUSTER_QUEUE_ASSIGNMENT
      }
    ]
  };

  beforeAll(() => {
    jest.clearAllMocks();
  });

  it('thunk fulfilled', async () => {
    const addToastSpy = toastSpy('addToast');
    await triggerDeleteClusterToQueueMapping(params)(
      byPassFulfilled(triggerDeleteClusterToQueueMapping.fulfilled.type),
      store.getState,
      {}
    );
    expect(addToastSpy).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_TOAST_DELETE_SUCCESS
    });
  });

  it('thunk rejected', async () => {
    const addToastSpy = toastSpy('addToast');
    await triggerDeleteClusterToQueueMapping(params)(
      byPassRejected(triggerDeleteClusterToQueueMapping.rejected.type),
      store.getState,
      {}
    );
    expect(addToastSpy).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_TOAST_DELETE_FAIL
    });
  });

  it('builder pending', () => {
    const pendingAction = triggerDeleteClusterToQueueMapping.pending(
      triggerDeleteClusterToQueueMapping.pending.type,
      params
    );
    const actual = rootReducer(store.getState(), pendingAction);
    expect(actual?.clientConfigClusterToQueueMapping?.loadingForm).toEqual(
      true
    );
  });

  it('builder fulfilled', () => {
    const fulfilledAction = triggerDeleteClusterToQueueMapping.fulfilled(
      {},
      triggerDeleteClusterToQueueMapping.fulfilled.type,
      params
    );
    const actual = rootReducer(store.getState(), fulfilledAction);
    expect(actual?.clientConfigClusterToQueueMapping?.loadingForm).toEqual(
      false
    );
    expect(
      actual?.clientConfigClusterToQueueMapping?.isOpenModalDelete
    ).toEqual(false);
  });

  it('builder rejected', () => {
    const rejectAction = triggerDeleteClusterToQueueMapping.rejected(
      new Error('error'),
      triggerDeleteClusterToQueueMapping.rejected.type,
      params
    );
    const actual = rootReducer(store.getState(), rejectAction);
    expect(actual?.clientConfigClusterToQueueMapping?.loadingForm).toEqual(
      false
    );
    expect(actual?.clientConfigClusterToQueueMapping?.errorForm).toEqual(
      'error'
    );
  });
});
