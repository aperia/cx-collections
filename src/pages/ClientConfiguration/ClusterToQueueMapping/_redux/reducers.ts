import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// types
import {
  ClusterToQueueMappingItem,
  ClusterToQueueMappingState
} from '../types';

import {
  getClusterToQueueMapping,
  getClusterToQueueMappingBuilder
} from './getClusterToQueueMapping';

import {
  triggerDeleteClusterToQueueMapping,
  deleteClusterToQueueMappingBuilder
} from './deleteClusterToQueueMapping';

import {
  triggerAddAndEditClusterToQueueMappingBuilder,
  triggerAddAndEditClusterToQueueMapping
} from './addAndEditClusterToQueueMapping';

import {
  getClusterQueueAssignmentMethod,
  getClusterQueueAssignmentMethodBuilder
} from './getClusterQueueAssignmentMethod';
import { SortType } from 'app/_libraries/_dls/components';

export const initialState: ClusterToQueueMappingState = {
  loading: false,
  loadingForm: false,
  cluster: {},
  clusterList: [],
  error: '',
  errorForm: '',
  isOpenModalForm: false,
  isOpenModalDelete: false,
  sortBy: { id: 'clusterName', order: undefined },
  queueList: []
};

const { actions, reducer } = createSlice({
  name: 'clusterToQueueMappingConfiguration',
  initialState,
  reducers: {
    deleteClusterRequest: (
      draftState,
      action: PayloadAction<{ cluster: ClusterToQueueMappingItem | {} }>
    ) => {
      draftState.isOpenModalDelete = !draftState.isOpenModalDelete;
      draftState.cluster = action.payload.cluster;
    },

    cancelModalFormCluster: draftState => {
      draftState.isOpenModalForm = false;
    },

    openModalFormCluster: draftState => {
      draftState.isOpenModalForm = true;
    },
    setEditData: (
      draftState,
      action: PayloadAction<{ cluster?: ClusterToQueueMappingItem }>
    ) => {
      draftState.cluster = action.payload.cluster || {};
    },

    onChangeSort: (draftState, action: PayloadAction<SortType>) => {
      draftState.sortBy = action.payload;
    }
  },

  extraReducers: builder => {
    getClusterToQueueMappingBuilder(builder);
    deleteClusterToQueueMappingBuilder(builder);
    triggerAddAndEditClusterToQueueMappingBuilder(builder);
    getClusterQueueAssignmentMethodBuilder(builder);
  }
});

const clusterToQueueMappingActions = {
  ...actions,
  getClusterToQueueMapping,
  triggerDeleteClusterToQueueMapping,
  triggerAddAndEditClusterToQueueMapping,
  getClusterQueueAssignmentMethod
};

export { clusterToQueueMappingActions, reducer };
