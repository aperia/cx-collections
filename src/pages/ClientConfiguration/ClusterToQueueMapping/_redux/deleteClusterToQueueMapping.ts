import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import { I18N_CLUSTER_TO_QUEUE_MAPPING } from '../constants';
import {
  ClusterToQueueMappingState,
  DeleteClusterToQueueMappingArgs
} from '../types';
import { clusterToQueueMappingActions } from './reducers';
import { updateClusterToQueueMapping } from './updateClusterToQueueMapping';

export const triggerDeleteClusterToQueueMapping = createAsyncThunk<
  unknown,
  DeleteClusterToQueueMappingArgs,
  ThunkAPIConfig
>(
  'clusterToQueueMapping/triggerDeleteClusterToQueueMapping',
  async (args, thunkAPI) => {
    const { dispatch } = thunkAPI;

    const { postData, deletingCluster } = args;

    const response = await dispatch(
      updateClusterToQueueMapping({
        data: postData,
        updateAction: 'delete'
      })
    );

    if (isFulfilled(response)) {
      batch(() => {
        dispatch(
          changeHistoryActions.saveChangedClientConfig({
            action: 'DELETE',
            changedCategory: 'clusterToQueueMapping',
            changedItem: {
              clusterName: deletingCluster?.clusterName
            },
            oldValue: deletingCluster
          })
        );
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_TOAST_DELETE_SUCCESS
          })
        );
        dispatch(clusterToQueueMappingActions.getClusterToQueueMapping());
      });
    }

    if (isRejected(response)) {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_TOAST_DELETE_FAIL
        })
      );
    }
  }
);

export const deleteClusterToQueueMappingBuilder = (
  builder: ActionReducerMapBuilder<ClusterToQueueMappingState>
) => {
  builder
    .addCase(triggerDeleteClusterToQueueMapping.pending, draftState => {
      draftState.loadingForm = true;
    })
    .addCase(triggerDeleteClusterToQueueMapping.fulfilled, draftState => {
      draftState.loadingForm = false;
      draftState.isOpenModalDelete = false;
    })
    .addCase(
      triggerDeleteClusterToQueueMapping.rejected,
      (draftState, action) => {
        draftState.loadingForm = false;
        draftState.errorForm = action.error.message;
      }
    );
};
