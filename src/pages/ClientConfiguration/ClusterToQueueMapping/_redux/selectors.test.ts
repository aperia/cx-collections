import { selectorWrapper } from 'app/test-utils';
import * as selectors from './selectors';

describe('Test ClusterToQueueMapping selectors', () => {
  const store: Partial<RootState> = {
    clientConfigClusterToQueueMapping: {
      loading: true,
      loadingForm: true,
      cluster: { clusterName: 'clusterName' },
      clusterList: [{ clusterName: 'clusterName' }],
      error: 'error',
      errorForm: '',
      isOpenModalForm: true,
      isOpenModalDelete: true,
      sortBy: { id: 'clusterName', order: 'asc' },
      queueList: [{ queueName: 'queue 1' }]
    }
  };

  it('selectClusterLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectClusterLoading
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectClusterFormLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectClusterFormLoading
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectClusterGridData', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectClusterGridData
    );

    expect(data).toEqual([{ clusterName: 'clusterName' }]);
    expect(emptyData).toEqual([]);
  });

  it('selectClusterError', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectClusterError
    );

    expect(data).toEqual('error');
    expect(emptyData).toEqual('');
  });

  it('selectDeleteModalOpen', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectDeleteModalOpen
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectCurrentRecord', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectCurrentRecord
    );

    expect(data).toEqual({ clusterName: 'clusterName' });
    expect(emptyData).toEqual({});
  });

  it('selectClusterFormModalOpen', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectClusterFormModalOpen
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectClusterQueueAssignmentMethod', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectClusterQueueAssignmentMethod
    );

    expect(data).toEqual([{ queueName: 'queue 1' }]);
    expect(emptyData).toEqual([]);
  });
});
