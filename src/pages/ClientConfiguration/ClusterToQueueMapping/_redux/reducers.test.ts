import { ClusterToQueueMappingState } from '../types';
import { clusterToQueueMappingActions, reducer } from './reducers';

describe('Test ClusterToQueueMapping', () => {
  const initialState: ClusterToQueueMappingState = {
    loading: false,
    loadingForm: false,
    cluster: {},
    clusterList: [],
    error: '',
    errorForm: '',
    isOpenModalForm: false,
    isOpenModalDelete: true,
    sortBy: { id: 'clusterName', order: undefined },
    methodRefData: []
  };

  it('deleteClusterRequest', () => {
    const state = reducer(
      initialState,
      clusterToQueueMappingActions.deleteClusterRequest({
        cluster: { clusterName: 'clusterName' }
      })
    );

    expect(state.isOpenModalDelete).toEqual(false);
    expect(state.cluster).toEqual({ clusterName: 'clusterName' });
  });

  it('cancelModalFormCluster', () => {
    const state = reducer(
      initialState,
      clusterToQueueMappingActions.cancelModalFormCluster()
    );

    expect(state.isOpenModalForm).toEqual(false);
  });

  it('openModalFormCluster', () => {
    const state = reducer(
      initialState,
      clusterToQueueMappingActions.openModalFormCluster()
    );

    expect(state.isOpenModalForm).toEqual(true);
  });

  it('setEditData', () => {
    const state = reducer(
      initialState,
      clusterToQueueMappingActions.setEditData({})
    );

    expect(state.cluster).toEqual({});
  });

  it('onChangeSort', () => {
    const state = reducer(
      initialState,
      clusterToQueueMappingActions.onChangeSort({ id: 'a', order: 'asc' })
    );

    expect(state.sortBy).toEqual({ id: 'a', order: 'asc' });
  });
});
