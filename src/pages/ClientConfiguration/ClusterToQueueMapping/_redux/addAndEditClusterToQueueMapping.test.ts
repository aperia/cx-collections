import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator
} from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { AnyAction, createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { I18N_CLUSTER_TO_QUEUE_MAPPING } from '../constants';
import { FULFILLED, PENDING, REJECTED } from 'app/constants';
import { AddClusterToQueueMappingArgs } from './../types';
import { triggerAddAndEditClusterToQueueMapping } from './addAndEditClusterToQueueMapping';
import {
  clusterToQueueMappingActions,
  initialState,
  reducer
} from './reducers';
const actionsToastSpy = mockActionCreator(actionsToast);
const clusterQueueMappings = [
  { clusterName: 'clusterName1', queue: 'queue1' },
  { clusterName: 'clusterName2', queue: 'queue2' }
];

const triggerAddAndEditClusterToQueueMappingCases = [
  {
    caseName: 'AddToQueueMapping > fulfilled',
    actionState: FULFILLED,
    isEditingMode: false,
    expected: {
      type: triggerAddAndEditClusterToQueueMapping.fulfilled.type,
      toast: {
        show: true,
        type: 'success',
        message: I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_TOAST_ADD_SUCCESS
      }
    }
  },
  {
    caseName: 'EditToQueueMapping > fulfilled',
    actionState: FULFILLED,
    isEditingMode: true,
    expected: {
      type: triggerAddAndEditClusterToQueueMapping.fulfilled.type,
      toast: {
        show: true,
        type: 'success',
        message: I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_TOAST_EDIT_SUCCESS
      }
    }
  },
  {
    caseName: 'AddToQueueMapping > rejected',
    actionState: REJECTED,
    isEditingMode: false,
    expected: {
      type: triggerAddAndEditClusterToQueueMapping.rejected.type,
      toast: {
        show: true,
        type: 'error',
        message: I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_TOAST_ADD_FAIL
      }
    }
  },
  {
    caseName: 'EditToQueueMapping > rejected',
    actionState: REJECTED,
    isEditingMode: true,
    expected: {
      type: triggerAddAndEditClusterToQueueMapping.rejected.type,
      toast: {
        show: true,
        type: 'error',
        message: I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_TOAST_EDIT_FAIL
      }
    }
  }
];

const setupTriggerAddAndEditClusterToQueueMapping = async ({
  actionState = FULFILLED,
  isEditingMode = false,
  data = clusterQueueMappings
}) => {
  const store = createStore(rootReducer, {});
  const spyToast = actionsToastSpy('addToast');
  let byPassDispatchAction = byPassFulfilled(
    triggerAddAndEditClusterToQueueMapping.fulfilled.type,
    {}
  );

  switch (actionState) {
    case REJECTED:
      byPassDispatchAction = byPassRejected(
        triggerAddAndEditClusterToQueueMapping.rejected.type,
        {}
      );
      break;
    case FULFILLED:
    default:
      break;
  }

  const result = await triggerAddAndEditClusterToQueueMapping({
    postData: data,
    isEditingMode
  })(byPassDispatchAction, store.getState, {});

  return {
    result,
    spyToast
  };
};

afterEach(() => {
  jest.restoreAllMocks();
});

describe.each(triggerAddAndEditClusterToQueueMappingCases)(
  'Test triggerAddAndEditClusterToQueueMapping async thunk',
  ({ caseName, actionState, isEditingMode, expected }) => {
    it(caseName, async () => {
      // Arrange
      const spyClusterToQueueMappingActions = mockActionCreator(
        clusterToQueueMappingActions
      );

      const getClusterToQueueMappingMock = spyClusterToQueueMappingActions(
        'getClusterToQueueMapping'
      );

      // Act
      const { result, spyToast } =
        await setupTriggerAddAndEditClusterToQueueMapping({
          actionState,
          isEditingMode
        });

      // Assert
      switch (actionState) {
        case PENDING:
          break;
        case REJECTED:
          expect(spyToast).toBeCalledWith(expected.toast);
          break;
        case FULFILLED:
        default:
          expect(getClusterToQueueMappingMock).toHaveBeenCalled();
          expect(spyToast).toBeCalledWith(expected.toast);
          break;
      }

      expect(result.type).toEqual(expected.type);
    });
  }
);

const triggerAddAndEditClusterToQueueMappingBuilderCases = [
  {
    caseName: 'should have pending',
    actionState: PENDING
  },
  {
    caseName: 'should have fulfilled',
    actionState: FULFILLED
  },
  {
    caseName: 'should have rejected',
    actionState: REJECTED
  }
];

const setupTriggerAddAndEditClusterToQueueMappingBuilder = ({
  actionState = FULFILLED
}) => {
  const mockError = new Error('Mock Error Message');
  const initialClusterToQueueMappingState = { ...initialState };

  let action: AnyAction =
    clusterToQueueMappingActions.triggerAddAndEditClusterToQueueMapping.fulfilled(
      undefined,
      triggerAddAndEditClusterToQueueMapping.pending.type,
      {} as AddClusterToQueueMappingArgs
    );

  switch (actionState) {
    case PENDING:
      action =
        clusterToQueueMappingActions.triggerAddAndEditClusterToQueueMapping.pending(
          triggerAddAndEditClusterToQueueMapping.pending.type,
          {} as AddClusterToQueueMappingArgs
        );
      break;
    case REJECTED:
      action =
        clusterToQueueMappingActions.triggerAddAndEditClusterToQueueMapping.rejected(
          mockError,
          triggerAddAndEditClusterToQueueMapping.pending.type,
          {} as AddClusterToQueueMappingArgs
        );
      break;
    case FULFILLED:
    default:
      break;
  }

  const nextState = reducer(initialClusterToQueueMappingState, action);

  return {
    nextState,
    mockError
  };
};

describe.each(triggerAddAndEditClusterToQueueMappingBuilderCases)(
  'Test triggerAddAndEditClusterToQueueMappingBuilder',
  ({ caseName, actionState }) => {
    it(caseName, () => {
      // Arrange
      const { nextState, mockError } =
        setupTriggerAddAndEditClusterToQueueMappingBuilder({ actionState });

      // Assert
      switch (actionState) {
        case PENDING:
          expect(nextState.loadingForm).toEqual(true);
          break;
        case REJECTED:
          expect(nextState.loadingForm).toEqual(false);
          expect(nextState.errorForm).toEqual(mockError.message);
          break;
        case FULFILLED:
        default:
          expect(nextState.loadingForm).toEqual(false);
          expect(nextState.isOpenModalForm).toEqual(false);
          break;
      }
    });
  }
);
