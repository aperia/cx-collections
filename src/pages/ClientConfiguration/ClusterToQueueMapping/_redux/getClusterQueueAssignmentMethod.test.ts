import { FULFILLED, PENDING, REJECTED } from 'app/constants';
import { responseDefault } from 'app/test-utils';
import { AnyAction, createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { clusterToQueueMappingService } from '../clusterToQueueMappingService';
import { getClusterQueueAssignmentMethod } from './getClusterQueueAssignmentMethod';
import {
  clusterToQueueMappingActions,
  initialState,
  reducer
} from './reducers';

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: ''
  }
};

const mockClusterQueueAssignmentMethodData = {
  responseStatus: 'success',
  queueDetails: [
    {
      clientId: '8981',
      systemId: '$ALL',
      principleId: '$ALL',
      agentId: '$ALL',
      userId: 'NSA1',
      queueId: '162825439561',
      queueName: 'CTSM-DEFAULT',
      queueGroupIdOwner: 'FD - ADMIN',
      accessGroupIds: ['FD - ADMIN'],
      accessLevel: 'UPDATE',
      autoUserAssign: false,
      isAscendingOrder: true,
      ascendingOrder: true
    }
  ]
};

const setupGetClusterQueueAssignmentMethod = async (isDefaults = false) => {
  const store = createStore(rootReducer, {
    mapping: {
      data: isDefaults
        ? {}
        : {
            queueAssignmentMethod: {
              queueName: 'queueName'
            }
          }
    }
  });

  const result = await getClusterQueueAssignmentMethod()(
    store.dispatch,
    store.getState,
    {}
  );

  return {
    result
  };
};

const setupGetClusterQueueAssignmentMethodBuilder = ({
  actionState = FULFILLED,
  data = mockClusterQueueAssignmentMethodData.queueDetails
}) => {
  const mockError = new Error('Mock Error Message');
  const mockState: Partial<RootState> = {
    clientConfigClusterToQueueMapping: initialState,
    clientConfiguration: {
      settings: {
        selectedConfig: {
          clientId: '9000',
          systemId: '1000',
          principleId: '2000',
          agentId: '3000'
        }
      }
    }
  };

  let action: AnyAction =
    clusterToQueueMappingActions.getClusterQueueAssignmentMethod.fulfilled(
      data,
      getClusterQueueAssignmentMethod.pending.type,
      undefined
    );

  switch (actionState) {
    case PENDING:
      action =
        clusterToQueueMappingActions.getClusterQueueAssignmentMethod.pending(
          getClusterQueueAssignmentMethod.pending.type,
          undefined
        );
      break;
    case REJECTED:
      action =
        clusterToQueueMappingActions.getClusterQueueAssignmentMethod.rejected(
          mockError,
          getClusterQueueAssignmentMethod.pending.type,
          undefined
        );
      break;
    case FULFILLED:
    default:
      break;
  }

  const nextState = reducer(
    mockState.clientConfigClusterToQueueMapping,
    action
  );

  return {
    nextState,
    mockError
  };
};

const getClusterQueueAssignmentMethodCases = [
  {
    caseName: 'getClusterQueueAssignmentMethod > fulfilled',
    actionState: FULFILLED,
    expected: {
      type: getClusterQueueAssignmentMethod.fulfilled.type,
      data: mockClusterQueueAssignmentMethodData.queueDetails.map(item => ({
        queueName: item.queueName
      }))
    }
  },
  {
    caseName: 'getClusterQueueAssignmentMethod > rejected',
    actionState: REJECTED,
    expected: {
      type: getClusterQueueAssignmentMethod.rejected.type,
      data: undefined
    }
  },
  {
    caseName: 'getClusterQueueAssignmentMethod > fulfilled with defaults',
    subCase: 'defaults',
    actionState: FULFILLED,
    expected: {
      type: getClusterQueueAssignmentMethod.fulfilled.type,
      data: []
    }
  }
];

describe.each(getClusterQueueAssignmentMethodCases)(
  'Test getClusterQueueAssignmentMethod',
  ({ caseName, actionState, expected, subCase }) => {
    it(caseName, async () => {
      // Arrange
      const spyGetClusterQueueAssignmentMethod = jest.spyOn(
        clusterToQueueMappingService,
        'getClusterQueueAssignmentMethod'
      );

      switch (actionState) {
        case REJECTED:
          window.appConfig = {} as AppConfiguration;
          spyGetClusterQueueAssignmentMethod.mockRejectedValue({});
          break;
        case FULFILLED:
        default:
          if (subCase === 'defaults') {
            spyGetClusterQueueAssignmentMethod.mockResolvedValue({
              ...responseDefault,
              data: {}
            });
          } else {
            spyGetClusterQueueAssignmentMethod.mockResolvedValue({
              ...responseDefault,
              data: mockClusterQueueAssignmentMethodData
            });
          }
          break;
      }

      // Act
      const { result } = await setupGetClusterQueueAssignmentMethod(
        subCase === 'defaults'
      );

      // Assert
      expect(result.type).toEqual(expected.type);
      expect(result.payload).toEqual(expected.data);
    });
  }
);

const getClusterToQueueMappingBuilderCases = [
  {
    caseName: 'should have pending',
    actionState: PENDING
  },
  {
    caseName: 'should have fulfilled',
    actionState: FULFILLED
  },
  {
    caseName: 'should have rejected',
    actionState: REJECTED
  }
];

describe.each(getClusterToQueueMappingBuilderCases)(
  'Test getClusterToQueueMappingBuilder',
  ({ caseName, actionState }) => {
    it(caseName, () => {
      // Arrange
      const { nextState } = setupGetClusterQueueAssignmentMethodBuilder({
        actionState,
        data: mockClusterQueueAssignmentMethodData.queueDetails
      });

      // Assert
      switch (actionState) {
        case PENDING:
          expect(nextState.loadingForm).toEqual(true);
          expect(nextState.queueList).toEqual([]);
          break;
        case REJECTED:
          expect(nextState.loadingForm).toEqual(false);
          expect(nextState.queueList).toEqual([]);
          break;
        case FULFILLED:
        default:
          expect(nextState.loadingForm).toEqual(false);
          expect(nextState.queueList).toEqual(
            mockClusterQueueAssignmentMethodData.queueDetails
          );
          break;
      }
    });
  }
);
