import { createSelector } from '@reduxjs/toolkit';
import { SortType } from 'app/_libraries/_dls/components';
import { orderBy } from 'lodash';
import { ClusterToQueueMappingItem } from '../types';

const getClusterToQueueMappingList = (state: RootState) => {
  return state.clientConfigClusterToQueueMapping;
};

const getSortBy = (states: RootState): SortType => {
  return states.clientConfigClusterToQueueMapping.sortBy;
};

const getSelectedClientConfig = (state: RootState) => {
  return state.clientConfiguration.settings?.selectedConfig || {};
};

export const selectClusterLoading = createSelector(
  getClusterToQueueMappingList,
  data => data.loading
);

export const selectClusterFormLoading = createSelector(
  getClusterToQueueMappingList,
  data => data.loadingForm
);

export const selectClusterGridData = createSelector(
  [getClusterToQueueMappingList, getSortBy],
  (data, sortBy) => {

    return orderBy(
      data.clusterList,
      [
        item =>
          item[sortBy.id as keyof ClusterToQueueMappingItem]?.toLowerCase(),
        item => item.clusterName?.toLowerCase()
      ],
      [sortBy.order || false, 'asc']
    );
  }
);

export const selectClusterError = createSelector(
  getClusterToQueueMappingList,
  data => data.error
);

const getDeleteModalOpen = (state: RootState): boolean => {
  return state.clientConfigClusterToQueueMapping.isOpenModalDelete;
};

export const selectDeleteModalOpen = createSelector(
  getDeleteModalOpen,
  (data: boolean) => data
);

export const selectCurrentRecord = createSelector(
  getClusterToQueueMappingList,
  data => data.cluster
);

const getFormClusterModalOpen = (state: RootState): boolean => {
  return state.clientConfigClusterToQueueMapping.isOpenModalForm;
};

export const selectClusterFormModalOpen = createSelector(
  getFormClusterModalOpen,
  (data: boolean) => data
);

export const selectClusterQueueAssignmentMethod = createSelector(
  getClusterToQueueMappingList,
  data => data.queueList
);

export const selectSelectedClientConfig = createSelector(
  getSelectedClientConfig,
  data => data
);

