import { createAsyncThunk, isRejected } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers/mappingData';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { UpdateClusterToQueueMappingArgs } from '../types';

export const updateClusterToQueueMapping = createAsyncThunk<
  undefined,
  UpdateClusterToQueueMappingArgs,
  ThunkAPIConfig
>('clusterTransaction/updateClusterToQueueMapping', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;

  const { data, updateAction } = args;

  const clusterModel = getState().mapping.data?.clientConfigCluster || {};

  const response = await dispatch(
    clientConfigurationActions.updateClientInfo({
      clusterQueueMappings: mappingDataFromArray(data, clusterModel, true),
      updateAction
    })
  );

  if (isRejected(response)) return thunkAPI.rejectWithValue({});

  return undefined;
});
