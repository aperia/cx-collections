import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { orderBy } from 'app/_libraries/_dls/lodash';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

import { CLUSTER_GRID_COLUMN_NAME } from '../constants';
import {
  ClusterToQueueMappingItem,
  ClusterToQueueMappingState
} from '../types';

export const getClusterToQueueMapping = createAsyncThunk<
  ClusterToQueueMappingItem[],
  undefined,
  ThunkAPIConfig
>('clusterToQueueMapping/getClusterToQueueMapping', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;
  const response = await dispatch(
    clientConfigurationActions.getClientInfoData({
      dataType: 'clusterQueueMappings'
    })
  );

  if (isFulfilled(response)) {
    const clusterModel = getState().mapping.data?.clientConfigCluster || {};
    const clusters = response.payload.clusterQueueMappings || [];

    return orderBy(
      mappingDataFromArray(clusters, clusterModel),
      [
        item =>
          item[
            CLUSTER_GRID_COLUMN_NAME.CLUSTER_NAME as keyof ClusterToQueueMappingItem
          ]?.toLowerCase()
      ],
      'asc'
    );
  }
  return thunkAPI.rejectWithValue({});
});

export const getClusterToQueueMappingBuilder = (
  builder: ActionReducerMapBuilder<ClusterToQueueMappingState>
) => {
  builder
    .addCase(getClusterToQueueMapping.pending, draftState => {
      draftState.loading = true;
      draftState.clusterList = [];
    })
    .addCase(getClusterToQueueMapping.fulfilled, (draftState, action) => {
      draftState.loading = false;
      draftState.clusterList = action.payload;
    })
    .addCase(getClusterToQueueMapping.rejected, (draftState, action) => {
      draftState.loading = false;
      draftState.error = action.error.message;
    });
};
