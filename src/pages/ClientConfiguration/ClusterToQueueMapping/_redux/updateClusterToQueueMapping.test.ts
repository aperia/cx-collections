import { FULFILLED, REJECTED } from 'app/constants';
import { byPassFulfilled, byPassRejected } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { UpdateClusterToQueueMappingArgs } from '../types';
import { updateClusterToQueueMapping } from './updateClusterToQueueMapping';

const clusterQueueMappings = [
  { clusterName: 'clusterName1', queue: 'queue1' },
  { clusterName: 'clusterName2', queue: 'queue2' }
];

const setupUpdateClusterToQueueMapping = async ({
  actionState = FULFILLED,
  updateAction = 'delete',
  data = clusterQueueMappings
}) => {
  const store = createStore(rootReducer, {});

  let byPassDispatchAction = byPassFulfilled(
    updateClusterToQueueMapping.fulfilled.type,
    {}
  );

  switch (actionState) {
    case REJECTED:
      byPassDispatchAction = byPassRejected(
        updateClusterToQueueMapping.rejected.type,
        {}
      );
      break;
    case FULFILLED:
    default:
      break;
  }

  const result = await updateClusterToQueueMapping({
    data,
    updateAction
  } as UpdateClusterToQueueMappingArgs)(
    byPassDispatchAction,
    store.getState,
    {}
  );

  return {
    result
  };
};

const updateClusterToQueueMappingCases = [
  {
    caseName: 'updateClusterToQueueMapping > fulfilled',
    actionState: FULFILLED,
    updateAction: undefined,
    expected: {
      type: updateClusterToQueueMapping.fulfilled.type,
      data: undefined
    }
  },
  {
    caseName: 'updateClusterToQueueMapping > rejected',
    actionState: REJECTED,
    updateAction: undefined,
    expected: {
      type: updateClusterToQueueMapping.rejected.type,
      data: {}
    }
  },
  {
    caseName:
      'updateClusterToQueueMapping with updateAction === "delete" > fulfilled',
    actionState: FULFILLED,
    updateAction: 'delete',
    expected: {
      type: updateClusterToQueueMapping.fulfilled.type,
      data: undefined
    }
  },
  {
    caseName:
      'updateClusterToQueueMapping with updateAction === "delete" > rejected',
    actionState: REJECTED,
    updateAction: 'delete',
    expected: {
      type: updateClusterToQueueMapping.rejected.type,
      data: {}
    }
  }
];

describe.each(updateClusterToQueueMappingCases)(
  'Test updateClusterToQueueMapping async thunk',
  ({ caseName, actionState, expected, updateAction }) => {
    it(caseName, async () => {
      // Act
      const { result } = await setupUpdateClusterToQueueMapping({
        actionState,
        updateAction
      });
      // Assert
      expect(result.type).toEqual(expected.type);
      expect(result.payload).toEqual(expected.data);
    });
  }
);
