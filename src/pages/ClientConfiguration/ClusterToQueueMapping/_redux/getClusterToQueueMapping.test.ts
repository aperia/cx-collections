import { FULFILLED, PENDING, REJECTED } from 'app/constants';
import { byPassFulfilled, byPassRejected } from 'app/test-utils';
import { AnyAction, createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { CLUSTER_GRID_COLUMN_NAME } from '../constants';
import { getClusterToQueueMapping } from './getClusterToQueueMapping';
import {
  clusterToQueueMappingActions,
  initialState,
  reducer
} from './reducers';

const clusterQueueMappings = [
  { clusterName: 'clusterName1', queue: 'queue1' },
  { clusterName: 'clusterName2', queue: 'queue2' }
];

const setupGetClusterToQueueMapping = async ({
  actionState = FULFILLED,
  mockData = clusterQueueMappings
}) => {
  const store = createStore(rootReducer, {
    mapping: {
      data: {
        clientConfigCluster: {
          clusterName: CLUSTER_GRID_COLUMN_NAME.CLUSTER_NAME
        }
      }
    }
  });

  const payload = {
    ['clusterQueueMappings']: mockData
  };

  let byPassDispatchAction = byPassFulfilled(
    getClusterToQueueMapping.fulfilled.type,
    payload
  );

  switch (actionState) {
    case REJECTED:
      byPassDispatchAction = byPassRejected(
        getClusterToQueueMapping.rejected.type,
        payload
      );
      break;
    case FULFILLED:
    default:
      break;
  }

  const result = await getClusterToQueueMapping()(
    byPassDispatchAction,
    store.getState,
    {}
  );

  return {
    result
  };
};

const setupGetClusterToQueueMappingBuilderCases = ({
  actionState = FULFILLED,
  data = clusterQueueMappings
}) => {
  const mockError = new Error('Mock Error Message');
  const initialClusterToQueueMappingState = { ...initialState };

  let action: AnyAction =
    clusterToQueueMappingActions.getClusterToQueueMapping.fulfilled(
      data,
      getClusterToQueueMapping.pending.type,
      undefined
    );

  switch (actionState) {
    case PENDING:
      action = clusterToQueueMappingActions.getClusterToQueueMapping.pending(
        getClusterToQueueMapping.pending.type,
        undefined
      );
      break;
    case REJECTED:
      action = clusterToQueueMappingActions.getClusterToQueueMapping.rejected(
        mockError,
        getClusterToQueueMapping.pending.type,
        undefined
      );
      break;
    case FULFILLED:
    default:
      break;
  }

  const nextState = reducer(initialClusterToQueueMappingState, action);

  return {
    nextState,
    mockError
  };
};

const getClusterToQueueMappingCases = [
  {
    caseName: 'getClusterToQueueMapping > fulfilled',
    actionState: FULFILLED,
    expected: {
      type: getClusterToQueueMapping.fulfilled.type,
      data: clusterQueueMappings.map((item: any) => ({
        clusterName: item.clusterName
      }))
    }
  },
  {
    caseName: 'getClusterToQueueMapping > rejected',
    actionState: REJECTED,
    expected: {
      type: getClusterToQueueMapping.rejected.type,
      data: {}
    }
  }
];

describe.each(getClusterToQueueMappingCases)(
  'Test getClusterToQueueMapping',
  ({ caseName, actionState, expected }) => {
    it(caseName, async () => {
      // Arrange

      // Act
      const { result } = await setupGetClusterToQueueMapping({
        actionState,
        mockData: clusterQueueMappings
      });

      // Assert
      expect(result.type).toEqual(expected.type);
      expect(result.payload).toEqual(expected.data);
    });
  }
);

it('thunk fulfilled with no mapping and no response', async () => {
  const store = createStore(rootReducer, {
    mapping: {
      data: {}
    }
  });
  const result = await getClusterToQueueMapping()(
    byPassFulfilled(getClusterToQueueMapping.fulfilled.type, {}),
    store.getState,
    {}
  );
  expect(result.payload).toEqual([]);
});

const getClusterToQueueMappingBuilderCases = [
  {
    caseName: 'should have pending',
    actionState: PENDING
  },
  {
    caseName: 'should have fulfilled',
    actionState: FULFILLED
  },
  {
    caseName: 'should have rejected',
    actionState: REJECTED
  }
];

describe.each(getClusterToQueueMappingBuilderCases)(
  'Test getClusterToQueueMappingBuilder',
  ({ caseName, actionState }) => {
    it(caseName, () => {
      // Arrange
      const { nextState, mockError } =
        setupGetClusterToQueueMappingBuilderCases({
          actionState,
          data: clusterQueueMappings
        });

      // Assert
      switch (actionState) {
        case PENDING:
          expect(nextState.loading).toEqual(true);
          expect(nextState.clusterList).toEqual([]);
          break;
        case REJECTED:
          expect(nextState.loading).toEqual(false);
          expect(nextState.error).toEqual(mockError.message);
          break;
        case FULFILLED:
        default:
          expect(nextState.clusterList).toEqual(clusterQueueMappings);
          expect(nextState.loading).toEqual(false);
          break;
      }
    });
  }
);
