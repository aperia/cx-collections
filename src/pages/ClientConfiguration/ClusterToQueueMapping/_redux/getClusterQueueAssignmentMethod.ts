import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { orderBy } from 'app/_libraries/_dls/lodash';

// Service
import { clusterToQueueMappingService } from '../clusterToQueueMappingService';

// Type
import {
  ClusterToQueueMappingState,
  GetClusterQueueAssignmentMethodRequest,
  QueueItem
} from '../types';

export const getClusterQueueAssignmentMethod = createAsyncThunk<
  QueueItem[],
  undefined,
  ThunkAPIConfig
>(
  'ClusterQueueAssignment/getClusterQueueAssignmentMethod',
  async (args, thunkAPI) => {
    const { getState } = thunkAPI;
    const { mapping, accessConfig } = getState();
    const [clientNumber = '', system = '$ALL', prin = '$ALL', agent = '$ALL'] =
      accessConfig?.cspaList?.[0]?.cspa?.split('-') || [];

    const {
      user = '',
      app = '',
      org = ''
    } = window.appConfig?.commonConfig || {};

    const requestData: GetClusterQueueAssignmentMethodRequest = {
      common: {
        agent,
        clientNumber,
        system,
        prin,
        user,
        app,
        org
      },
      fetchSize: '500'
    };
    const response =
      await clusterToQueueMappingService.getClusterQueueAssignmentMethod(
        requestData
      );

    const queueModel = mapping.data?.queueAssignmentMethod || {};
    const queues = response.data.queueDetails || [];

    return orderBy(
      mappingDataFromArray(queues, queueModel),
      [item => item['queueName' as keyof QueueItem]?.toLowerCase()],
      'asc'
    );
  }
);

export const getClusterQueueAssignmentMethodBuilder = (
  builder: ActionReducerMapBuilder<ClusterToQueueMappingState>
) => {
  builder
    .addCase(getClusterQueueAssignmentMethod.pending, draftState => {
      draftState.loadingForm = true;
      draftState.queueList = [];
    })
    .addCase(
      getClusterQueueAssignmentMethod.fulfilled,
      (draftState, action) => {
        draftState.loadingForm = false;
        draftState.queueList = action.payload;
      }
    )
    .addCase(getClusterQueueAssignmentMethod.rejected, draftState => {
      draftState.loadingForm = false;
      draftState.queueList = [];
    });
};
