import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';

import {
  AddClusterToQueueMappingArgs,
  ClusterToQueueMappingState
} from '../types';
import { clusterToQueueMappingActions } from './reducers';
import { I18N_CLUSTER_TO_QUEUE_MAPPING } from '../constants';
import { updateClusterToQueueMapping } from './updateClusterToQueueMapping';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

export const triggerAddAndEditClusterToQueueMapping = createAsyncThunk<
  unknown,
  AddClusterToQueueMappingArgs,
  ThunkAPIConfig
>('triggerAddAndEditClusterToQueueMapping', async (args, thunkAPI) => {
  const { dispatch, rejectWithValue } = thunkAPI;
  const { postData, isEditingMode, editingCluster, addingCluster } = args;

  const response = await dispatch(
    updateClusterToQueueMapping({ data: postData })
  );

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        changeHistoryActions.saveChangedClientConfig({
          action: isEditingMode ? 'UPDATE' : 'ADD',
          changedCategory: 'clusterToQueueMapping',
          changedItem: {
            clusterName: isEditingMode
              ? editingCluster?.clusterName
              : addingCluster?.clusterName
          },
          newValue: addingCluster,
          oldValue: isEditingMode ? editingCluster : {}
        })
      );
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: isEditingMode
            ? I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_TOAST_EDIT_SUCCESS
            : I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_TOAST_ADD_SUCCESS
        })
      );
      dispatch(clusterToQueueMappingActions.getClusterToQueueMapping());
    });
  }

  if (isRejected(response)) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: isEditingMode
          ? I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_TOAST_EDIT_FAIL
          : I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_TOAST_ADD_FAIL
      })
    );
    return rejectWithValue({});
  }
});

export const triggerAddAndEditClusterToQueueMappingBuilder = (
  builder: ActionReducerMapBuilder<ClusterToQueueMappingState>
) => {
  builder
    .addCase(triggerAddAndEditClusterToQueueMapping.pending, draftState => {
      draftState.loadingForm = true;
    })
    .addCase(triggerAddAndEditClusterToQueueMapping.fulfilled, draftState => {
      draftState.loadingForm = false;
      draftState.isOpenModalForm = false;
    })
    .addCase(
      triggerAddAndEditClusterToQueueMapping.rejected,
      (draftState, action) => {
        draftState.loadingForm = false;
        draftState.errorForm = action.error.message;
      }
    );
};
