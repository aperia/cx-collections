import React from 'react';
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import ClusterToQueueMappingGrid from './ClusterToQueueMappingGrid';
import { fireEvent, screen } from '@testing-library/react';
import { clusterToQueueMappingActions } from './_redux/reducers';
import { ClusterToQueueMappingItem } from './types';
import { CLIENT_CONFIG_STATUS_KEY } from '../constants';
import * as mockStatus from '../hooks/useCheckClientConfigStatus';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

const clusterToQueueMappingActionsSpy = mockActionCreator(
  clusterToQueueMappingActions
);

describe('Test ClusterToQueueMappingGrid component', () => {
  beforeEach(() => {
    jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(false);
  });

  it('render component', () => {
    const getClusterToQueueMapping = clusterToQueueMappingActionsSpy(
      'getClusterToQueueMapping'
    );
    const deleteClusterRequest = clusterToQueueMappingActionsSpy(
      'deleteClusterRequest'
    );
    const setEditData = clusterToQueueMappingActionsSpy('setEditData');
    const openModalFormCluster = clusterToQueueMappingActionsSpy(
      'openModalFormCluster'
    );
    const onChangeSort = clusterToQueueMappingActionsSpy('onChangeSort');

    jest.useFakeTimers();
    renderMockStoreId(<ClusterToQueueMappingGrid />, {
      initialState: {
        clientConfigClusterToQueueMapping: {
          loading: false,
          loadingForm: false,
          cluster: {},
          clusterList: [{ clusterName: 'clusterName' }],
          error: '',
          errorForm: '',
          isOpenModalForm: false,
          isOpenModalDelete: false,
          sortBy: { id: 'clusterName', order: 'desc' },
          queueList: [{ queueName: 'mock name' }]
        },
        clientConfiguration: {
          settings: {
            selectedConfig: {
              additionalFields: [
                {
                  active: true,
                  name: CLIENT_CONFIG_STATUS_KEY
                }
              ]
            }
          }
        }
      }
    });
    jest.runAllTimers();

    const editBtn = screen.getByText('txt_edit');
    fireEvent.click(editBtn);

    const deleteBtn = screen.getByText('txt_delete');
    fireEvent.click(deleteBtn);

    const col = screen.getByText('txt_cluster_name');
    fireEvent.click(col);

    expect(getClusterToQueueMapping).toBeCalled();
    expect(deleteClusterRequest).toBeCalled();
    expect(setEditData).toBeCalled();
    expect(openModalFormCluster).toBeCalled();
    expect(onChangeSort).toBeCalled();
  });

  it('loading', () => {
    clusterToQueueMappingActionsSpy('getClusterToQueueMapping');

    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(<ClusterToQueueMappingGrid />, {
      initialState: {
        clientConfigClusterToQueueMapping: {
          loading: true,
          loadingForm: false,
          cluster: { clusterName: 'clusterName' },
          clusterList: [{ clusterName: 'clusterName' }],
          error: '',
          errorForm: '',
          isOpenModalForm: false,
          isOpenModalDelete: false,
          sortBy: { id: 'clusterName', order: 'desc' },
          queueList: [{ queueName: 'mock name' }]
        }
      }
    });
    jest.runAllTimers();

    expect(wrapper.container.innerHTML).toEqual('<div class="vh-50"></div>');
  });

  it('error', () => {
    clusterToQueueMappingActionsSpy('getClusterToQueueMapping');

    jest.useFakeTimers();
    renderMockStoreId(<ClusterToQueueMappingGrid />, {
      initialState: {
        clientConfigClusterToQueueMapping: {
          loading: false,
          loadingForm: false,
          cluster: { clusterName: 'clusterName' },
          clusterList: [{ clusterName: 'clusterName' }],
          error: 'Error Message',
          errorForm: '',
          isOpenModalForm: false,
          isOpenModalDelete: false,
          sortBy: { id: 'clusterName', order: 'desc' },
          queueList: [{ queueName: 'mock name' }]
        }
      }
    });
    jest.runAllTimers();

    expect(
      screen.getByText('txt_data_load_unsuccessful_click_reload_to_try_again')
    ).toBeInTheDocument();
  });

  it('show pagination', () => {
    clusterToQueueMappingActionsSpy('getClusterToQueueMapping');
    const list = [];
    for (let i = 0; i < 12; i++) {
      list.push({ clusterName: 'clusterName' } as ClusterToQueueMappingItem);
    }

    jest.useFakeTimers();
    renderMockStoreId(<ClusterToQueueMappingGrid />, {
      initialState: {
        clientConfigClusterToQueueMapping: {
          loading: false,
          loadingForm: false,
          cluster: { clusterName: 'clusterName' },
          clusterList: list,
          error: '',
          errorForm: '',
          isOpenModalForm: false,
          isOpenModalDelete: false,
          sortBy: { id: 'clusterName', order: 'desc' },
          queueList: [{ queueName: 'mock name' }]
        }
      }
    });
    jest.runAllTimers();

    expect(screen.getByText('1')).toBeInTheDocument();
  });
});
