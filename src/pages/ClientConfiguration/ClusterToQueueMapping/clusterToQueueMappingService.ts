import { apiService } from 'app/utils/api.service';
import { GetClusterQueueAssignmentMethodRequest } from './types';

export const clusterToQueueMappingService = {
  getClusterQueueAssignmentMethod(
    requestBody: GetClusterQueueAssignmentMethodRequest
  ) {
    const url =
      window.appConfig?.api?.clientConfigClusterToQueueMapping
        ?.getClusterQueueAssignmentMethod || '';
    return apiService.post(url, requestBody);
  }
};
