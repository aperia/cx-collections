export const I18N_CLUSTER_TO_QUEUE_MAPPING = {
  CLUSTER: 'txt_cluster',
  CLUSTER_ALERT_DESCRIPTION: 'txt_cluster_alert_description',
  CLUSTER_TO_QUEUE_MAPPING: 'txt_cluster_to_queue_mapping',
  CLUSTER_TO_QUEUE_MAPPING_LIST: 'txt_cluster_to_queue_mapping_list',
  CLUSTER_NAME: 'txt_cluster_name',
  CLUSTER_NAME_HELPER: 'txt_cluster_name_helper',
  ADD_CLUSTER: 'txt_add_cluster',
  EDIT_CLUSTER: 'txt_edit_cluster',
  CONFIRM_DELETE_CLUSTER: 'txt_confirm_delete_cluster',
  CONFIRM_DELETE_CLUSTER_QUESTION: 'txt_confirm_delete_cluster_question',
  CLUSTER_QUEUE_ASSIGNMENT: 'txt_cluster_queue_assignment',
  NO_DATA: 'txt_no_data',
  EDIT: 'txt_edit',
  DELETE: 'txt_delete',
  ACTIONS: 'txt_actions',
  CLUSTER_NAME_ALREADY_EXISTS: 'txt_cluster_name_already_exists',
  CLUSTER_NAME_IS_REQUIRED: 'txt_cluster_name_is_required',
  CLUSTER_NAME_NO_SPACE: 'txt_cluster_name_no_space',
  CLUSTER_QUEUE_ASSIGNMENT_IS_REQUIRED:
    'txt_cluster_queue_assignment_is_required',
  CLUSTER_TOAST_DELETE_SUCCESS: 'txt_cluster_toast_delete_success',
  CLUSTER_TOAST_DELETE_FAIL: 'txt_cluster_toast_delete_fail',
  CLUSTER_TOAST_ADD_SUCCESS: 'txt_cluster_toast_add_success',
  CLUSTER_TOAST_ADD_FAIL: 'txt_cluster_toast_add_fail',
  CLUSTER_TOAST_EDIT_SUCCESS: 'txt_cluster_toast_edit_success',
  CLUSTER_TOAST_EDIT_FAIL: 'txt_cluster_toast_edit_fail'
};

export const CLUSTER_GRID_COLUMN_NAME = {
  CLUSTER_NAME: 'clusterName',
  CLUSTER_QUEUE_ASSIGNMENT: 'clusterQueueAssignment'
};

export const ADD_CLUSTER_ERR_TYPE = {
  DUPLICATE_CLUSTER_NAME: 'duplicate'
};

export const INLINE_MSG = {
  [ADD_CLUSTER_ERR_TYPE.DUPLICATE_CLUSTER_NAME]:
    I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_NAME_ALREADY_EXISTS
};

export const TYPE_FORM = {
  ADD: 'add',
  EDIT: 'edit'
};

export const FORM_FIELD = {
  CLUSTER_NAME: {
    name: 'clusterName',
    label: 'txt_cluster_name'
  },
  CLUSTER_QUEUE_ASSIGNMENT: {
    name: 'clusterQueueAssignment',
    label: 'txt_cluster_queue_assignment'
  }
};
