import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

// components
import {
  Button,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import ModalBodyWithErrorClientConfig from '../ApiErrorSection/ModalBodyWithErrorClientConfig';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import {
  selectClusterFormLoading,
  selectDeleteModalOpen,
  selectCurrentRecord,
  selectClusterGridData
} from './_redux/selectors';
import { clusterToQueueMappingActions } from './_redux/reducers';

// constants
import { I18N_CLUSTER_TO_QUEUE_MAPPING } from './constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import findIndex from 'lodash.findindex';
import { genAmtId } from 'app/_libraries/_dls/utils';

export const DeleteCluster: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const isLoading = useSelector(selectClusterFormLoading);
  const isOpened = useSelector(selectDeleteModalOpen);
  const currentRecord = useSelector(selectCurrentRecord);
  const existingClusters = useSelector(selectClusterGridData);
  const testId = 'clientConfig_clusterToQueueMapping_deleteCluster';

  const handleClose = () => {
    dispatch(
      clusterToQueueMappingActions.deleteClusterRequest({ cluster: {} })
    );
  };

  const handleDelete = () => {
    const updateData = [...existingClusters];
    const editIndex = findIndex(existingClusters, currentRecord);
    updateData.splice(editIndex, 1);

    dispatch(
      clusterToQueueMappingActions.triggerDeleteClusterToQueueMapping({
        postData: updateData,
        deletingCluster: currentRecord
      })
    );
  };

  return (
    <Modal xs loading={isLoading} show={isOpened} dataTestId={testId}>
      <ModalHeader border closeButton onHide={handleClose} dataTestId={`${testId}_header`}>
        <ModalTitle dataTestId={`${testId}_title`}>
          {t(I18N_CLUSTER_TO_QUEUE_MAPPING.CONFIRM_DELETE_CLUSTER)}
        </ModalTitle>
      </ModalHeader>
      <ModalBodyWithErrorClientConfig
        apiErrorClassName="mb-24"
        dataTestId={`${testId}_body`}
      >
        <p data-testid={genAmtId(testId, 'confirmMessage', '')}>
          {t(I18N_CLUSTER_TO_QUEUE_MAPPING.CONFIRM_DELETE_CLUSTER_QUESTION)}
        </p>
        <p className="mt-16" data-testid={genAmtId(testId, 'clusterName', '')}>
          {`${t(I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER)}:`}{' '}
          <strong className="text-break">{currentRecord?.clusterName}</strong>
        </p>
      </ModalBodyWithErrorClientConfig>
      <ModalFooter dataTestId={`${testId}_footer`}>
        <Button
          id="cluster-confirm-modal-cancel-btn"
          variant="secondary"
          onClick={handleClose}
          dataTestId={`${testId}_closebtn`}
        >
          {t(I18N_COMMON_TEXT.CANCEL)}
        </Button>
        <Button
          id="cluster-confirm-modal-confirm-btn"
          variant="danger"
          onClick={handleDelete}
          dataTestId={`${testId}_deleteBtn`}
        >
          {t(I18N_COMMON_TEXT.DELETE)}
        </Button>
      </ModalFooter>
    </Modal>
  );
};
