import React from 'react';
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import BodyModalCluster from './BodyModalCluster';
import {
  act,
  createEvent,
  fireEvent,
  screen,
  waitFor
} from '@testing-library/react';
import { clusterToQueueMappingActions } from './_redux/reducers';
import userEvent from '@testing-library/user-event';

HTMLCanvasElement.prototype.getContext = jest.fn();

const clusterToQueueMappingActionsSpy = mockActionCreator(
  clusterToQueueMappingActions
);

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

jest.mock('../clientConfigurationServices.ts', () => {
  const { responseDefault } = jest.requireActual('app/test-utils');
  const services = jest.requireActual('../clientConfigurationServices.ts');
  return {
    ...services,
    getClientInfoById: jest.fn().mockResolvedValue(responseDefault)
  };
});

describe('Test BodyModalCluster', () => {
  it('render component', () => {
    jest.useFakeTimers();
    renderMockStoreId(<BodyModalCluster />);
    jest.runAllTimers();
    expect(screen.getByText('txt_cluster_name')).toBeInTheDocument();
  });

  it('render component > click cancel', () => {
    const cancelModalFormCluster = clusterToQueueMappingActionsSpy(
      'cancelModalFormCluster'
    );

    jest.useFakeTimers();
    renderMockStoreId(<BodyModalCluster />, {
      initialState: {
        clientConfigClusterToQueueMapping: {
          loading: false,
          loadingForm: false,
          cluster: { clusterName: 'clusterName' },
          clusterList: [{ clusterName: 'A' }, { clusterName: 'B' }],
          error: '',
          errorForm: '',
          isOpenModalForm: false,
          isOpenModalDelete: false,
          sortBy: { id: 'clusterName', order: 'desc' },
          queueList: [{ queueName: 'mock name' }]
        }
      }
    });
    jest.runAllTimers();

    const cancelBtn = screen.getByText('txt_cancel');
    fireEvent.click(cancelBtn);

    expect(cancelModalFormCluster).toBeCalled();
  });

  describe('triggerAddAndEditClusterToQueueMapping', () => {
    it('edit mode', async () => {
      const triggerAddAndEditClusterToQueueMapping =
        clusterToQueueMappingActionsSpy(
          'triggerAddAndEditClusterToQueueMapping'
        );

      jest.useFakeTimers();
      renderMockStoreId(<BodyModalCluster />, {
        initialState: {
          clientConfigClusterToQueueMapping: {
            loading: false,
            loadingForm: false,
            cluster: {
              clusterName: 'clusterName',
              clusterQueueAssignment: 'mock name'
            },
            clusterList: [{ clusterName: 'A' }, { clusterName: 'B' }],
            error: '',
            errorForm: '',
            isOpenModalForm: false,
            isOpenModalDelete: false,
            sortBy: { id: 'clusterName', order: 'desc' },
            queueList: [{ queueId: '123456789', queueName: 'mock name' }]
          }
        }
      });
      jest.runAllTimers();

      const textbox = screen.getAllByRole('textbox');

      const paste = createEvent.paste(textbox[0], {
        clipboardData: {
          getData: () => 'a '
        }
      });
      fireEvent(textbox[0], paste);

      const paste1 = createEvent.paste(textbox[0], {
        clipboardData: {
          getData: () => 'a'
        }
      });
      fireEvent(textbox[0], paste1);
      fireEvent.change(textbox[0], { target: { value: 'editClusterName' } });

      const btn = screen.getByText('txt_save');
      await waitFor(() => {
        userEvent.click(btn);
      });

      expect(triggerAddAndEditClusterToQueueMapping).toBeCalledWith({
        addingCluster: {
          clusterName: 'editClusterName',
          clusterQueueAssignment: '123456789'
        },
        editingCluster: {
          clusterName: 'clusterName',
          clusterQueueAssignment: 'mock name'
        },
        postData: [
          { clusterName: 'B' },
          {
            clusterName: 'editClusterName',
            clusterQueueAssignment: '123456789'
          }
        ],
        isEditingMode: true
      });
    });

    it('add mode', async () => {
      const triggerAddAndEditClusterToQueueMapping =
        clusterToQueueMappingActionsSpy(
          'triggerAddAndEditClusterToQueueMapping'
        );

      jest.useFakeTimers();
      const { wrapper } = renderMockStoreId(<BodyModalCluster />, {
        initialState: {
          clientConfigClusterToQueueMapping: {
            loading: false,
            loadingForm: false,
            cluster: {},
            clusterList: [{ clusterName: 'clusterName' }, { clusterName: 'B' }],
            error: '',
            errorForm: '',
            isOpenModalForm: false,
            isOpenModalDelete: false,
            sortBy: { id: 'clusterName', order: 'desc' },
            queueList: [{ queueId: '123456789', queueName: 'mock name' }]
          }
        }
      });
      jest.runAllTimers();

      const textbox = screen.getAllByRole('textbox');
      fireEvent.change(textbox[0], {
        target: { value: 'a' }
      });

      act(() => {
        userEvent.click(textbox[1]);
      });

      const item = wrapper.getByText('mock name')!;
      act(() => {
        userEvent.click(item);
      });

      const btn = screen.getByText('txt_submit');
      await waitFor(() => {
        userEvent.click(btn);
      });

      expect(triggerAddAndEditClusterToQueueMapping).toHaveBeenCalledWith({
        addingCluster: {
          clusterName: 'a',
          clusterQueueAssignment: '123456789'
        },
        editingCluster: {},
        postData: [
          { clusterName: 'clusterName' },
          { clusterName: 'B' },
          { clusterName: 'a', clusterQueueAssignment: '123456789' }
        ],
        isEditingMode: false
      });
    });
  });

  it('show inline message', async () => {
    const { wrapper } = renderMockStoreId(<BodyModalCluster />, {
      initialState: {
        clientConfigClusterToQueueMapping: {
          loading: false,
          loadingForm: false,
          cluster: {},
          clusterList: [{ clusterName: 'clusterName' }, { clusterName: 'B' }],
          error: '',
          errorForm: '',
          isOpenModalForm: false,
          isOpenModalDelete: false,
          sortBy: { id: 'clusterName', order: 'desc' },
          queueList: [{ queueName: 'mock name' }]
        }
      }
    });

    const textbox = screen.getAllByRole('textbox');
    fireEvent.change(textbox[0], {
      target: { value: 'clusterName' }
    });

    act(() => {
      userEvent.click(textbox[1]);
    });

    const item = wrapper.getByText('mock name')!;
    act(() => {
      userEvent.click(item);
    });

    const btn = screen.getByText('txt_submit');
    await waitFor(() => {
      userEvent.click(btn);
    });

    expect(
      wrapper.getByText('txt_cluster_name_already_exists')
    ).toBeInTheDocument();
  });

  it('pass error and keyDown', async () => {
    renderMockStoreId(<BodyModalCluster />, {
      initialState: {
        clientConfigClusterToQueueMapping: {
          loading: false,
          loadingForm: false,
          cluster: {},
          clusterList: [{ clusterName: 'clusterName' }, { clusterName: 'B' }],
          error: '',
          errorForm: '',
          isOpenModalForm: false,
          isOpenModalDelete: false,
          sortBy: { id: 'clusterName', order: 'desc' },
          queueList: [{ queueName: 'mock name' }]
        }
      }
    });

    const textbox = screen.getAllByRole('textbox');
    act(() => {
      userEvent.click(textbox[0]);
    });

    await waitFor(() => {
      userEvent.click(textbox[1]);
    });

    fireEvent.keyDown(textbox[0], {
      keyCode: 32
    });

    fireEvent.keyDown(textbox[0], {
      keyCode: 27
    });

    expect(screen.getByText('txt_cluster_name')).toBeInTheDocument();
  });
});
