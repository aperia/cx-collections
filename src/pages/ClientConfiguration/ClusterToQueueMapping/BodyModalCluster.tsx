import React, { useEffect, useState } from 'react';

// components
import {
  ComboBox,
  InlineMessage,
  ModalFooter,
  TextBox
} from 'app/_libraries/_dls/components';
import ModalBodyWithErrorClientConfig from '../ApiErrorSection/ModalBodyWithErrorClientConfig';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useFormik } from 'formik';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import findIndex from 'lodash.findindex';
import { useDispatch, useSelector } from 'react-redux';

// redux
import { clusterToQueueMappingActions } from './_redux/reducers';
import {
  selectClusterGridData,
  selectClusterQueueAssignmentMethod,
  selectCurrentRecord
} from './_redux/selectors';

// constants
import { I18N_CLUSTER_TO_QUEUE_MAPPING } from './constants';
import { FORM_FIELD } from './constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import classNames from 'classnames';
import { QueueItem } from './types';
import { useDisableSubmitButton } from 'app/hooks/useDisableSubmitButton';
import { genAmtId } from 'app/_libraries/_dls/utils';

const BodyModalCluster: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const existingClusters = useSelector(selectClusterGridData);
  const editingCluster = useSelector(selectCurrentRecord);
  const testId = 'clientConfig_clusterToQueueMapping_formCluster';

  const isEditingMode = !isEmpty(editingCluster);

  const clustersToCheck = existingClusters.filter(
    cluster => cluster.clusterName !== editingCluster?.clusterName
  );

  const clusterQueueMethod = useSelector(selectClusterQueueAssignmentMethod);

  const [isMessage, setIsMessage] = useState<boolean>(false);

  const { values, handleChange, handleBlur, handleSubmit, errors, touched } =
    useFormik<{
      clusterName?: string;
      clusterQueueAssignment?: QueueItem;
    }>({
      initialValues: {
        clusterName: editingCluster?.clusterName,
        clusterQueueAssignment: clusterQueueMethod?.find(
          item => item?.queueName == editingCluster?.clusterQueueAssignment
        )
      },
      onSubmit: values => {
        // check if cluster is already existed
        const { clusterName, clusterQueueAssignment } = values;
        const isExisted = clustersToCheck.find(
          cluster => cluster.clusterName === clusterName
        );
        if (isExisted) {
          setIsMessage(true);
          return;
        }

        setIsMessage(false);

        let updateData = [...existingClusters];
        const clusterValue = {
          clusterName,
          clusterQueueAssignment: clusterQueueAssignment?.queueId
        };
        if (isEditingMode) {
          const editIndex = findIndex(existingClusters, editingCluster);
          updateData.splice(editIndex, 1, clusterValue);
        } else {
          updateData = updateData.concat(clusterValue);
        }

        dispatch(
          clusterToQueueMappingActions.triggerAddAndEditClusterToQueueMapping({
            postData: updateData,
            isEditingMode,
            addingCluster: clusterValue,
            editingCluster
          })
        );
      },
      validateOnBlur: true,
      validateOnChange: false,
      validate: ({ clusterName, clusterQueueAssignment }) => {
        const errors: Record<string, string> | undefined = {};
        if (!clusterName) {
          errors['clusterName'] =
            I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_NAME_IS_REQUIRED;
        }
        if (!clusterQueueAssignment) {
          errors['clusterQueueAssignment'] =
            I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_QUEUE_ASSIGNMENT_IS_REQUIRED;
        }

        return errors;
      }
    });

  const { disableSubmit, setPrimitiveValues } = useDisableSubmitButton(values);

  useEffect(() => {
    const newValues = {
      clusterName: editingCluster?.clusterName,
      clusterQueueAssignment: clusterQueueMethod?.find(
        item => item?.queueName == editingCluster?.clusterQueueAssignment
      )
    };
    setPrimitiveValues(newValues);
  }, [
    clusterQueueMethod,
    editingCluster?.clusterName,
    editingCluster?.clusterQueueAssignment,
    setPrimitiveValues
  ]);

  const handleCancelModal = () => {
    dispatch(clusterToQueueMappingActions.cancelModalFormCluster());
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    // prevent enter space/whitespace
    if (event.keyCode === 32) {
      event.preventDefault();
    }
  };

  const handlePaste = (event: React.ClipboardEvent<HTMLInputElement>) => {
    // prevent paste space/whitespace
    if (event.clipboardData.getData('Text').includes(' ')) {
      event.preventDefault();
    }
  };

  return (
    <>
      <ModalBodyWithErrorClientConfig
        apiErrorClassName="mb-24"
        dataTestId={`${testId}_body`}
      >
        {isMessage && (
          <InlineMessage
            className="mb-24"
            variant="danger"
            dataTestId={`${testId}_errorMessage`}
          >
            {t(I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_NAME_ALREADY_EXISTS)}
          </InlineMessage>
        )}
        <TextBox
          className={classNames({ 'dls-error': isMessage })}
          name={FORM_FIELD.CLUSTER_NAME.name}
          label={t(FORM_FIELD.CLUSTER_NAME.label)}
          value={values?.clusterName}
          required
          onChange={handleChange}
          onBlur={handleBlur}
          error={{
            status: Boolean(errors.clusterName && touched.clusterName),
            message: t(errors.clusterName) as string
          }}
          onKeyDown={handleKeyDown}
          onPaste={handlePaste}
          dataTestId={`${testId}_clusterName`}
        />
        <p
          className="fs-12 color-grey-l24 mt-8 mb-16"
          data-testid={genAmtId(testId, 'clusterNameHelper', '')}
        >
          {t(I18N_CLUSTER_TO_QUEUE_MAPPING.CLUSTER_NAME_HELPER)}
        </p>
        <ComboBox
          name={FORM_FIELD.CLUSTER_QUEUE_ASSIGNMENT.name}
          label={t(FORM_FIELD.CLUSTER_QUEUE_ASSIGNMENT.label)}
          value={values?.clusterQueueAssignment}
          textField="queueName"
          required
          onChange={handleChange}
          onBlur={handleBlur}
          error={{
            status: Boolean(
              errors.clusterQueueAssignment && touched.clusterQueueAssignment
            ),
            message: t(errors.clusterQueueAssignment) as string
          }}
          dataTestId={`${testId}_clusterQueueAssignment`}
          noResult={t('txt_no_results_found')}
          checkAllLabel={t('txt_all')}
        >
          {clusterQueueMethod.map((item, index) => {
            return (
              <ComboBox.Item
                key={`${item?.queueName} + ${index}`}
                label={item?.queueName}
                value={item}
              />
            );
          })}
        </ComboBox>
      </ModalBodyWithErrorClientConfig>
      <ModalFooter
        className="pt-32 pb-24"
        cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
        onCancel={handleCancelModal}
        okButtonText={
          isEditingMode ? t(I18N_COMMON_TEXT.SAVE) : t(I18N_COMMON_TEXT.SUBMIT)
        }
        onOk={handleSubmit}
        disabledOk={
          !values.clusterName ||
          !values.clusterQueueAssignment ||
          (isEditingMode && disableSubmit)
        }
        dataTestId={`${testId}_footer`}
      />
    </>
  );
};

export default BodyModalCluster;
