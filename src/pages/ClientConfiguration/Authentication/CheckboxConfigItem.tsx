import React from 'react';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { CheckBox } from 'app/_libraries/_dls/components';
import classNames from 'classnames';

// types
import { ToggleConfigPayload } from './types';

export interface CheckboxConfigItemProps extends DLSId {
  checked: boolean;
  id: string;
  title: string;
  status: boolean;
  onChange?: ({ id, checked }: ToggleConfigPayload) => void;
  className?: string;
}

export const CheckboxConfigItem: React.FC<CheckboxConfigItemProps> = props => {
  const { className, id, title, checked, onChange, status, dataTestId } = props;
  const { t } = useTranslation();
  const handleChecked = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { checked } = event.target;
    onChange && onChange({ id, checked });
  };

  return (
    <div className={classNames(className)}>
      <CheckBox dataTestId={dataTestId}>
        <CheckBox.Input
          readOnly={status}
          id={`client-config-${id}`}
          checked={checked}
          onChange={handleChecked}
        />
        <CheckBox.Label>{t(title)}</CheckBox.Label>
      </CheckBox>
    </div>
  );
};
