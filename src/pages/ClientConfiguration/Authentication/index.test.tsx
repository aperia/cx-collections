import React from 'react';

//mock test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';

import * as clientConfigHelper from 'pages/ClientConfiguration/helpers';
//components
import Authentication from '.';

//actions
import { clientConfigAuthenticationActions } from './_redux/reducers';

import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';

import { I18N_CLIENT_CONFIG_AUTH } from './constants';
import { CLIENT_CONFIG_STATUS_KEY, SECTION_TAB_EVENT } from '../constants';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import * as mockStatus from '../hooks/useCheckClientConfigStatus';
import userEvent from '@testing-library/user-event';
const mockAuthenticationActions = mockActionCreator(
  clientConfigAuthenticationActions
);
const mockSpy = mockActionCreator(actionsToast);
describe('render actions buttons ', () => {
  const initialState: Partial<RootState> = {
    authenticationConfig: {
      previousSourceConfig: ['previousSource', 'currentSource'],
      previousQuestionConfig: ['currentQuestion'],
      currentSourceConfig: ['currentSource'],
      currentQuestionConfig: ['currentQuestion'],
      loading: false
    }
  };
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<Authentication />, { initialState });
  };

  beforeEach(() => {
    jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(false);
  });

  it('should render with data ', () => {
    renderWrapper(initialState);

    const sourceConfiguration = screen.getByText(
      I18N_CLIENT_CONFIG_AUTH.AUTHENTICATION_SOURCE_CONFIGURATION
    );

    const questionConfiguration = screen.getByText(
      I18N_CLIENT_CONFIG_AUTH.AUTHENTICATION_QUESTION_CONFIGURATION
    );

    const title = screen.getByText(I18N_CLIENT_CONFIG_AUTH.AUTHENTICATION);

    expect(title).toBeInTheDocument();

    expect(sourceConfiguration).toBeInTheDocument();

    expect(questionConfiguration).toBeInTheDocument();
  });

  const stateDataChange: Partial<RootState> = {
    authenticationConfig: {
      previousSourceConfig: ['previousSource', 'currentSource'],
      previousQuestionConfig: ['currentQuestion'],
      currentSourceConfig: ['currentSource'],
      currentQuestionConfig: ['currentQuestion'],
      loading: false
    }
  };

  const stateHasNoDataChange: Partial<RootState> = {
    authenticationConfig: {
      previousSourceConfig: ['currentSource'],
      previousQuestionConfig: ['currentQuestion'],
      currentSourceConfig: ['currentSource'],
      currentQuestionConfig: ['currentQuestion'],
      loading: false
    }
  };

  it('handleSaveChanges ', () => {
    const spy = mockAuthenticationActions('triggerSubmitAuthenticationConfig');
    const { wrapper } = renderWrapper({
      ...initialState,
      clientConfiguration: {
        settings: {
          selectedConfig: {
            id: '123',
            cspaId: '1234',
            additionalFields: [
              {
                name: CLIENT_CONFIG_STATUS_KEY,
                active: true
              }
            ]
          }
        }
      }
    });

    const button = wrapper.getByText('txt_save_changes')!;
    button.click();

    expect(spy).toBeCalled();
  });

  it('handleResetToPrev ', () => {
    const spy = mockAuthenticationActions('resetToPrevious');
    const toastSpy = mockSpy('addToast');
    const { wrapper } = renderWrapper({
      ...initialState,
      clientConfiguration: {
        settings: {
          selectedConfig: {
            id: '123',
            cspaId: '1234',
            additionalFields: [
              {
                name: CLIENT_CONFIG_STATUS_KEY,
                active: true
              }
            ]
          }
        }
      }
    });

    const button = wrapper.getByText('txt_reset_to_previous')!;
    button.click();

    expect(spy).toBeCalled();
    expect(toastSpy).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_CLIENT_CONFIG_AUTH.RESET_TO_PREVIOUS_SUCCESS
    });
  });

  it('handleResetToPrev with no change data', () => {
    const spy = mockAuthenticationActions('resetToPrevious');
    const toastSpy = mockSpy('addToast');
    const { wrapper } = renderWrapper({
      ...stateHasNoDataChange,
      clientConfiguration: {
        settings: {
          selectedConfig: {
            id: '123',
            cspaId: '1234',
            additionalFields: [
              {
                name: CLIENT_CONFIG_STATUS_KEY,
                active: true
              }
            ]
          }
        }
      }
    });

    const button = wrapper.getByText('txt_reset_to_previous')!;
    button.click();

    expect(spy).not.toBeCalled();
    expect(toastSpy).not.toBeCalledWith();
  });

  it('handleCheckedSourceConfig', () => {
    const spy = mockAuthenticationActions('toggleSourceConfig');
    const { wrapper } = renderWrapper({
      ...stateDataChange,
      clientConfiguration: {
        settings: {
          selectedConfig: {
            id: '123',
            cspaId: '1234',
            additionalFields: [
              {
                name: CLIENT_CONFIG_STATUS_KEY,
                active: true
              }
            ]
          }
        }
      }
    });

    const checkboxInput = wrapper
      .getByText('txt_search_result')
      .parentElement!.querySelector('input')!;
    userEvent.click(checkboxInput);
    expect(spy).toBeCalled();
  });

  it('handleCheckedQuestionConfig', () => {
    const spy = mockAuthenticationActions('toggleQuestionConfig');
    const { wrapper } = renderWrapper({
      ...stateDataChange,
      clientConfiguration: {
        settings: {
          selectedConfig: {
            id: '123',
            cspaId: '1234',
            additionalFields: [
              {
                name: CLIENT_CONFIG_STATUS_KEY,
                active: true
              }
            ]
          }
        }
      }
    });

    const checkboxInput = wrapper
      .getByText('txt_date_of_birth')
      .parentElement!.querySelector('input')!;
    userEvent.click(checkboxInput);
    expect(spy).toBeCalled();
  });

  describe('useListenChangingTabEvent', () => {
    it('data not change', () => {
      const mockDispatchEventChangeTab = jest.fn();
      const spy = jest
        .spyOn(clientConfigHelper, 'dispatchControlTabChangeEvent')
        .mockImplementation(mockDispatchEventChangeTab);
      renderWrapper(stateHasNoDataChange);
      const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGING_TAB, {
        bubbles: true,
        cancelable: true
      });

      window.dispatchEvent(event);
      expect(mockDispatchEventChangeTab).toBeCalled();
      spy.mockReset();
      spy.mockRestore();
    });

    it('data has been change', () => {
      const openConfirmModalAction = jest.fn(x => {
        if (x.confirmCallback) {
          x.confirmCallback();
        }
        return { type: 'ok' };
      });
      const spy = jest
        .spyOn(confirmModalActions, 'open')
        .mockImplementation(openConfirmModalAction as never);

      renderWrapper(stateDataChange);
      const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGING_TAB, {
        bubbles: true,
        cancelable: true
      });

      window.dispatchEvent(event);
      expect(openConfirmModalAction).toBeCalledWith(
        expect.objectContaining({
          title: I18N_CLIENT_CONFIG_AUTH.UNSAVED_CHANGE,
          body: I18N_CLIENT_CONFIG_AUTH.DISCARD_CHANGE_BODY,
          cancelText: I18N_CLIENT_CONFIG_AUTH.CONTINUE_EDITING,
          confirmText: I18N_CLIENT_CONFIG_AUTH.DISCARD_CHANGE,
          variant: 'danger'
        })
      );
      spy.mockRestore();
      spy.mockReset();
    });
  });

  describe('useListenClosing', () => {
    it('data not change', () => {
      const mockDispatchEventCloseModal = jest.fn();
      const spy = jest
        .spyOn(clientConfigHelper, 'dispatchCloseModalEvent')
        .mockImplementation(mockDispatchEventCloseModal);
      renderWrapper(stateHasNoDataChange);
      const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
        bubbles: true,
        cancelable: true
      });

      window.dispatchEvent(event);
      expect(mockDispatchEventCloseModal).toBeCalled();
      spy.mockReset();
      spy.mockRestore();
    });

    it('data has been change', () => {
      const openConfirmModalAction = jest.fn(x => {
        if (x.confirmCallback) {
          x.confirmCallback();
        }
        return { type: 'ok' };
      });
      const spy = jest
        .spyOn(confirmModalActions, 'open')
        .mockImplementation(openConfirmModalAction as never);
      renderWrapper(stateDataChange);
      const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
        bubbles: true,
        cancelable: true
      });

      window.dispatchEvent(event);
      expect(openConfirmModalAction).toBeCalledWith(
        expect.objectContaining({
          title: I18N_CLIENT_CONFIG_AUTH.UNSAVED_CHANGE,
          body: I18N_CLIENT_CONFIG_AUTH.DISCARD_CHANGE_BODY,
          cancelText: I18N_CLIENT_CONFIG_AUTH.CONTINUE_EDITING,
          confirmText: I18N_CLIENT_CONFIG_AUTH.DISCARD_CHANGE,
          variant: 'danger'
        })
      );
      spy.mockRestore();
      spy.mockReset();
    });
  });
});
