import React, { useEffect } from 'react';
import { Button, SimpleBar } from 'app/_libraries/_dls/components';
import { batch, useDispatch, useSelector } from 'react-redux';
import classNames from 'classnames';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useListenChangingTabEvent } from '../hooks/useListenChangingTabEvent';
import { useListenClosingModal } from '../hooks/useListenClosingModal';
// component
import { CheckboxConfigItem } from './CheckboxConfigItem';

// helpers & constants & types
import {
  dispatchCloseModalEvent,
  dispatchControlTabChangeEvent
} from '../helpers';
import {
  I18N_CLIENT_CONFIG_AUTH,
  QUESTION_CONFIG,
  SOURCE_CONFIG
} from './constants';
import { ToggleConfigPayload } from './types';

// redux
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { clientConfigAuthenticationActions } from './_redux/reducers';
import {
  selectCurrentQuestionConfig,
  selectCurrentSourceConfig,
  selectDataChange
} from './_redux/selectors';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';
import { genAmtId } from 'app/_libraries/_dls/utils';
import ApiErrorForHeaderSection from '../ApiErrorSection/ApiErrorForHeaderSection';

const Authentication: React.FC = () => {
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const testId = 'clientConfig_authentication';

  const currentSourceConfig = useSelector(selectCurrentSourceConfig);

  const currentQuestionConfig = useSelector(selectCurrentQuestionConfig);

  const isDataChange = useSelector(selectDataChange);

  const currentSourceConfigSet = new Set(currentSourceConfig);

  const currentQuestionConfigSet = new Set(currentQuestionConfig);

  const status = useCheckClientConfigStatus();

  const handleCheckedSourceConfig = ({ id, checked }: ToggleConfigPayload) => {
    dispatch(
      clientConfigAuthenticationActions.toggleSourceConfig({ id, checked })
    );
  };

  const handleCheckedQuestionConfig = ({
    id,
    checked
  }: ToggleConfigPayload) => {
    dispatch(
      clientConfigAuthenticationActions.toggleQuestionConfig({ id, checked })
    );
  };

  const handleResetToPrev = () => {
    if (!isDataChange) return;
    batch(() => {
      dispatch(clientConfigAuthenticationActions.resetToPrevious());
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_CLIENT_CONFIG_AUTH.RESET_TO_PREVIOUS_SUCCESS
        })
      );
    });
  };

  const handleSaveChanges = () => {
    dispatch(
      clientConfigAuthenticationActions.triggerSubmitAuthenticationConfig()
    );
  };

  const sourceConfigDataRender = SOURCE_CONFIG.map(item => ({
    ...item,
    checked: currentSourceConfigSet.has(item.id)
  }));

  const questionConfigDataRender = QUESTION_CONFIG.map(item => ({
    ...item,
    checked: currentQuestionConfigSet.has(item.id)
  }));

  const renderSourceConfig = (sourceConfigStatus: boolean) => (
    <>
      <p
        className="mt-16 fw-500"
        data-testid={genAmtId(testId, 'sourceConfig-title', '')}
      >
        {t(I18N_CLIENT_CONFIG_AUTH.AUTHENTICATION_SOURCE_CONFIGURATION)}
      </p>

      <p
        className="mt-8"
        data-testid={genAmtId(testId, 'selectSourceConfig-title', '')}
      >
        {t(
          I18N_CLIENT_CONFIG_AUTH.SELECT_THE_SOURCE_FOR_AUTHENTICATION_TO_DISPLAY
        )}
      </p>
      <div>
        {sourceConfigDataRender.map(item => (
          <CheckboxConfigItem
            className="mt-16"
            status={sourceConfigStatus}
            key={item.id}
            {...item}
            onChange={handleCheckedSourceConfig}
            dataTestId={`${item.id}_${testId}_sourceConfigItem`}
          />
        ))}
      </div>
    </>
  );

  const renderQuestionConfig = (questionConfigStatus: boolean) => (
    <>
      <p
        className="mt-24 fw-500"
        data-testid={genAmtId(testId, 'questionConfig-title', '')}
      >
        {t(I18N_CLIENT_CONFIG_AUTH.AUTHENTICATION_QUESTION_CONFIGURATION)}
      </p>

      <p
        className="mt-8"
        data-testid={genAmtId(testId, 'selectQuestionConfig-title', '')}
      >
        {t(I18N_CLIENT_CONFIG_AUTH.SELECT_INFORMATION_TO_DISPLAY)}
      </p>
      <div className="row">
        {questionConfigDataRender.map((item, index) => (
          <CheckboxConfigItem
            className={classNames(
              'mt-16 col-6 col-lg-4',
              (index + 1) % 3 === 0 ? 'col-xl-6' : 'col-xl-3'
            )}
            key={item.id}
            status={questionConfigStatus}
            {...item}
            onChange={handleCheckedQuestionConfig}
            dataTestId={`${item.id}_${testId}_questionConfigItem`}
          />
        ))}
      </div>
    </>
  );

  useListenChangingTabEvent(() => {
    if (!isDataChange) {
      dispatchControlTabChangeEvent();
      return;
    }
    dispatch(
      confirmModalActions.open({
        title: I18N_CLIENT_CONFIG_AUTH.UNSAVED_CHANGE,
        body: I18N_CLIENT_CONFIG_AUTH.DISCARD_CHANGE_BODY,
        cancelText: I18N_CLIENT_CONFIG_AUTH.CONTINUE_EDITING,
        confirmText: I18N_CLIENT_CONFIG_AUTH.DISCARD_CHANGE,
        confirmCallback: dispatchControlTabChangeEvent,
        variant: 'danger'
      })
    );
  });

  useListenClosingModal(() => {
    if (!isDataChange) {
      dispatchCloseModalEvent();
      return;
    }
    dispatch(
      confirmModalActions.open({
        title: I18N_CLIENT_CONFIG_AUTH.UNSAVED_CHANGE,
        body: I18N_CLIENT_CONFIG_AUTH.DISCARD_CHANGE_BODY,
        cancelText: I18N_CLIENT_CONFIG_AUTH.CONTINUE_EDITING,
        confirmText: I18N_CLIENT_CONFIG_AUTH.DISCARD_CHANGE,
        confirmCallback: () => {
          dispatchCloseModalEvent();
        },
        variant: 'danger'
      })
    );
  });

  useEffect(() => {
    dispatch(
      clientConfigAuthenticationActions.getAuthenticationConfiguration()
    );
  }, [dispatch]);

  return (
    <div className={'position-relative has-footer-button'}>
      <SimpleBar>
        <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
          <ApiErrorForHeaderSection
            className="mb-24"
            dataTestId={`${testId}_apiError`}
          />
          <h5 data-testid={genAmtId(testId, 'title', '')}>
            {t(I18N_CLIENT_CONFIG_AUTH.AUTHENTICATION)}
          </h5>
          {renderSourceConfig(status)}
          {renderQuestionConfig(status)}
        </div>
      </SimpleBar>
      <div className="group-button-footer d-flex justify-content-end">
        <Button
          disabled={status}
          onClick={handleResetToPrev}
          variant="secondary"
          dataTestId={`${testId}_resetToPrevious-btn`}
        >
          {t(I18N_CLIENT_CONFIG_AUTH.RESET_TO_PREVIOUS)}
        </Button>
        <Button
          disabled={status || !isDataChange}
          onClick={() => handleSaveChanges()}
          variant="primary"
          dataTestId={`${testId}_saveChanges-btn`}
        >
          {t(I18N_CLIENT_CONFIG_AUTH.SAVE_CHANGES)}
        </Button>
      </div>
    </div>
  );
};

export default Authentication;
