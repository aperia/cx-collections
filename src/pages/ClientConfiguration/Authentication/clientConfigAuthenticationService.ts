import { apiService } from 'app/utils/api.service';

export const clientConfigAuthenticationServices = {
  getAuthenticationConfig(requestBody: {
    common: Record<string, any>;
    selectConfig: string;
    selectCspa: string;
    selectComponent: string;
  }) {
    const url =
      window.appConfig?.api?.clientConfigAuthentication
        ?.getAuthenticationConfig || '';
    return apiService.post(url, requestBody);
  },
  submitAuthentication(requestBody: {
    common: {
      clientNumber?: string;
      system?: string;
      prin?: string;
      agent?: string;
      user?: string;
      app?: string;
      org?: string;
      privileges?: string[];
    };
    clientConfig: {
      cspas: [
        {
          cspa: string;
          components: [
            {
              component: string;
              jsonValue: {
                sourceConfig: string[];
                questionConfig: string[];
              };
            }
          ];
          description: string;
          active: string;
        }
      ];
    };
  }) {
    const url =
      window.appConfig?.api?.clientConfigAuthentication?.submitAuthentication ||
      '';
    return apiService.post(url, requestBody);
  }
};
