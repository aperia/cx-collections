// services
import { clientConfigAuthenticationServices } from './clientConfigAuthenticationService';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';
import { AUTHENTICATION_COMPONENT_ID } from 'app/constants';

const mockRequest = { common: {} };

describe('clientConfigAuthenticationServices', () => {
  describe('getAuthenticationConfig', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      clientConfigAuthenticationServices.getAuthenticationConfig(mockRequest);

      expect(mockService).toBeCalledWith('', mockRequest);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      clientConfigAuthenticationServices.getAuthenticationConfig(mockRequest);

      expect(mockService).toBeCalledWith(
        apiUrl.clientConfigAuthentication.getAuthenticationConfig,
        mockRequest
      );
    });
  });

  describe('submitAuthentication', () => {
    const params = {
      clientConfig: {
        cspas: [
          {
            cspa: '1234-1234-1234-1234',
            components: [
              {
                component: AUTHENTICATION_COMPONENT_ID,
                jsonValue: { sourceConfig: ['src'], questionConfig: ['ques'] }
              }
            ]
          }
        ]
      },
      ...mockRequest
    } as never;

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      clientConfigAuthenticationServices.submitAuthentication(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      clientConfigAuthenticationServices.submitAuthentication(params);

      expect(mockService).toBeCalledWith(
        apiUrl.clientConfigAuthentication.submitAuthentication,
        params
      );
    });
  });
});
