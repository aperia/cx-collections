export const I18N_CLIENT_CONFIG_AUTH = {
  RESET_TO_PREVIOUS: 'txt_reset_to_previous',
  SAVE_CHANGES: 'txt_save_changes',
  AUTHENTICATION: 'txt_authentication',
  AUTHENTICATION_SOURCE_CONFIGURATION:
    'txt_authentication_source_configuration',
  SELECT_THE_SOURCE_FOR_AUTHENTICATION_TO_DISPLAY:
    'txt_select_the_source_for_authentication_to_display',
  SEARCH_RESULT: 'txt_search_result',
  PUSH_QUEUE: 'txt_push_queue',
  DIALER_QUEUE: 'txt_dialer_queue',
  AUTHENTICATION_QUESTION_CONFIGURATION:
    'txt_authentication_question_configuration',
  SELECT_INFORMATION_TO_DISPLAY:
    'txt_select_information_to_display_for_authenticating',
  MOTHERS_MAIDEN_NAME: 'txt_maiden_name_of_mother',
  DATE_OF_BIRTH: 'txt_date_of_birth',
  LAST_4_SSN: 'txt_last_4_ssn',
  LAST_PAYMENT_AMOUNT: 'txt_last_payment_amount',
  CREDIT_LIMIT: 'txt_credit_limit',
  HOME_PHONE: 'txt_home_phone',
  WORK_PHONE: 'txt_work_phone',
  MOBILE_PHONE: 'txt_mobile_phone',
  FAX_BILLING_ADDRESS: 'txt_fax_billing_address',
  MISCELLANEOUS: 'txt_miscellaneous',
  PERMANENT_BILLING_ADDRESS: 'txt_permanent_billing_address',
  PERMANENT_MAILING_ADDRESS: 'txt_permanent_mailing_address',
  UNSAVED_CHANGE_BODY: 'txt_unsaved_change_body',
  DISCARD_CHANGE_BODY: 'txt_discard_change_body',
  UNSAVED_CHANGE: 'txt_unsaved_changes',
  CONTINUE_EDITING: 'txt_continue_editing',
  SAVE_AND_CONTINUE: 'txt_save_and_continue',
  DISCARD_CHANGE: 'txt_discard_change',
  RESET_TO_PREVIOUS_SUCCESS: 'txt_authentication_reset_to_previous_success',
  SAVE_CHANGE_SUCCESS: 'txt_authentication_settings_updated',
  SAVE_CHANGE_FAILURE: 'txt_authentication_settings_failed_to_update'
};

export const SOURCE_CONFIG_ID = {
  SEARCH_RESULT: 'search_result',
  PUSH_QUEUE: 'push_queue',
  DIALER_QUEUE: 'dialer_queue'
};

export const QUESTION_CONFIG_ID = {
  MOTHERS_MAIDEN_NAME: 'mothers_maiden_name',
  DATE_OF_BIRTH: 'date_of_birth',
  LAST_4_SSN: 'last_4_ssn',
  LAST_PAYMENT_AMOUNT: 'last_payment_amount',
  CREDIT_LIMIT: 'credit_limit',
  HOME_PHONE: 'home_phone',
  WORK_PHONE: 'work_phone',
  MOBILE_PHONE: 'mobile_phone',
  FAX_BILLING_ADDRESS: 'fax_billing_address',
  MISCELLANEOUS: 'miscellaneous',
  PERMANENT_BILLING_ADDRESS: 'permanent_billing_address',
  PERMANENT_MAILING_ADDRESS: 'permanent_mailing_address'
};

export const SOURCE_CONFIG = [
  {
    id: SOURCE_CONFIG_ID.SEARCH_RESULT,
    title: I18N_CLIENT_CONFIG_AUTH.SEARCH_RESULT
  },
  {
    id: SOURCE_CONFIG_ID.PUSH_QUEUE,
    title: I18N_CLIENT_CONFIG_AUTH.PUSH_QUEUE
  },
  {
    id: SOURCE_CONFIG_ID.DIALER_QUEUE,
    title: I18N_CLIENT_CONFIG_AUTH.DIALER_QUEUE
  }
];

export const QUESTION_CONFIG = [
  {
    id: QUESTION_CONFIG_ID.MOTHERS_MAIDEN_NAME,
    title: I18N_CLIENT_CONFIG_AUTH.MOTHERS_MAIDEN_NAME
  },
  {
    id: QUESTION_CONFIG_ID.DATE_OF_BIRTH,
    title: I18N_CLIENT_CONFIG_AUTH.DATE_OF_BIRTH
  },
  {
    id: QUESTION_CONFIG_ID.LAST_4_SSN,
    title: I18N_CLIENT_CONFIG_AUTH.LAST_4_SSN
  },
  {
    id: QUESTION_CONFIG_ID.LAST_PAYMENT_AMOUNT,
    title: I18N_CLIENT_CONFIG_AUTH.LAST_PAYMENT_AMOUNT
  },
  {
    id: QUESTION_CONFIG_ID.CREDIT_LIMIT,
    title: I18N_CLIENT_CONFIG_AUTH.CREDIT_LIMIT
  },
  {
    id: QUESTION_CONFIG_ID.HOME_PHONE,
    title: I18N_CLIENT_CONFIG_AUTH.HOME_PHONE
  },
  {
    id: QUESTION_CONFIG_ID.WORK_PHONE,
    title: I18N_CLIENT_CONFIG_AUTH.WORK_PHONE
  },
  {
    id: QUESTION_CONFIG_ID.MOBILE_PHONE,
    title: I18N_CLIENT_CONFIG_AUTH.MOBILE_PHONE
  },
  {
    id: QUESTION_CONFIG_ID.MISCELLANEOUS,
    title: I18N_CLIENT_CONFIG_AUTH.MISCELLANEOUS
  },
  {
    id: QUESTION_CONFIG_ID.FAX_BILLING_ADDRESS,
    title: I18N_CLIENT_CONFIG_AUTH.FAX_BILLING_ADDRESS
  },
  {
    id: QUESTION_CONFIG_ID.PERMANENT_BILLING_ADDRESS,
    title: I18N_CLIENT_CONFIG_AUTH.PERMANENT_BILLING_ADDRESS
  }
];

export const DEFAULT_QUESTION_CONFIG = [
  'mothers_maiden_name',
  'last_payment_amount',
  'work_phone',
  'miscellaneous',
  'date_of_birth',
  'credit_limit',
  'mobile_phone',
  'permanent_billing_address',
  'last_4_ssn',
  'home_phone',
  'fax_billing_address',
  'permanent_mailing_address'
];

export const DEFAULT_SOURCE_CONFIG = [
  'search_result',
  'push_queue',
  'dialer_queue'
];
