import { selectorWrapper } from 'app/test-utils';
import {
  selectCurrentQuestionConfig,
  selectCurrentSourceConfig,
  selectDataChange,
  selectLoading
} from './selectors';

describe('Test Client Configuration Authentication Selectors', () => {
  const store: Partial<RootState> = {
    authenticationConfig: {
      previousSourceConfig: ['previousSource', 'currentSource'],
      previousQuestionConfig: ['currentQuestion'],
      currentSourceConfig: ['currentSource'],
      currentQuestionConfig: ['currentQuestion'],
      loading: false
    }
  };

  it('selectCurrentQuestionConfig', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectCurrentQuestionConfig
    );

    expect(data).toEqual(['currentQuestion']);
    expect(emptyData).toEqual([]);
  });

  it('selectCurrentSourceConfig', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectCurrentSourceConfig
    );

    expect(data).toEqual(['currentSource']);
    expect(emptyData).toEqual([]);
  });

  it('selectDataChange', () => {
    const store: Partial<RootState> = {
      authenticationConfig: {
        previousSourceConfig: ['previousSource', 'currentSource'],
        previousQuestionConfig: [],
        currentSourceConfig: ['currentSource'],
        currentQuestionConfig: [],
        loading: false
      }
    };
    const { data, emptyData } = selectorWrapper(store)(selectDataChange);

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(selectLoading);

    expect(data).toEqual(false);
    expect(emptyData).toEqual(false);
  });
});
