// types
import { ClientConfigurationAuthenticationState } from '../types';

//reducers
import { clientConfigAuthenticationActions, reducer } from './reducers';

describe('Test Client Configuration Code To Text Reducers', () => {
  const initialState: ClientConfigurationAuthenticationState = {
    previousSourceConfig: ['previousSource'],
    previousQuestionConfig: ['previousQuestion'],
    currentSourceConfig: ['currentSource'],
    currentQuestionConfig: ['currentQuestion'],
    loading: false
  };

  it('should toggleSourceConfig when check is false ', () => {
    const state = reducer(
      initialState,
      clientConfigAuthenticationActions.toggleSourceConfig({
        id: 'test',
        checked: false
      })
    );
    expect(state?.currentSourceConfig).toEqual(['currentSource']);
  });

  it('should toggleSourceConfig when check is true ', () => {
    const state = reducer(
      initialState,
      clientConfigAuthenticationActions.toggleSourceConfig({
        id: 'test',
        checked: true
      })
    );
    expect(state?.currentSourceConfig).toEqual(['currentSource', 'test']);
  });

  it('should toggleQuestionConfig when check is false', () => {
    const state = reducer(
      initialState,
      clientConfigAuthenticationActions.toggleQuestionConfig({
        id: 'test',
        checked: false
      })
    );
    expect(state?.currentQuestionConfig).toEqual(['currentQuestion']);
  });

  it('should toggleQuestionConfig when check is true', () => {
    const state = reducer(
      initialState,
      clientConfigAuthenticationActions.toggleQuestionConfig({
        id: 'test',
        checked: true
      })
    );
    expect(state?.currentQuestionConfig).toEqual(['currentQuestion', 'test']);
  });

  it('should resetToPrevious ', () => {
    const state = reducer(
      initialState,
      clientConfigAuthenticationActions.resetToPrevious()
    );

    expect(state?.currentSourceConfig).toEqual(['previousSource']);

    expect(state?.currentQuestionConfig).toEqual(['previousQuestion']);
  });
});
