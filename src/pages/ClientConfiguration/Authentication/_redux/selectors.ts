import { createSelector } from '@reduxjs/toolkit';
import { isEqual } from 'app/_libraries/_dls/lodash';

const getCurrentSourceConfig = (state: RootState) => {
  return state.authenticationConfig.currentSourceConfig;
};

const getCurrentQuestionConfig = (state: RootState) => {
  return state.authenticationConfig.currentQuestionConfig;
};

const getDataChange = (state: RootState) => {
  const {
    currentQuestionConfig,
    currentSourceConfig,
    previousQuestionConfig,
    previousSourceConfig
  } = state.authenticationConfig;
  const isSourceChange = !isEqual(currentSourceConfig?.slice().sort(), previousSourceConfig?.slice().sort());
  const isQuestionChange = !isEqual(
    currentQuestionConfig?.slice().sort(),
    previousQuestionConfig?.slice().sort()
  );
  return isSourceChange || isQuestionChange;
};

const getLoading = (state: RootState) => {
  return state.authenticationConfig.loading;
};

export const selectCurrentSourceConfig = createSelector(
  getCurrentSourceConfig,
  data => data
);

export const selectCurrentQuestionConfig = createSelector(
  getCurrentQuestionConfig,
  data => data
);

export const selectDataChange = createSelector(getDataChange, data => data);

export const selectLoading = createSelector(getLoading, data => data);
