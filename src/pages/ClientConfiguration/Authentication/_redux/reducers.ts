import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  ClientConfigurationAuthenticationState,
  ToggleConfigPayload
} from '../types';
import {
  getAuthenticationConfiguration,
  getAuthenticationConfigurationBuilder
} from './getAuthenticationConfig';
import {
  triggerSubmitAuthenticationConfig,
  triggerSubmitAuthenticationConfigBuilder
} from './submitAuthenticationConfig';

const initialState: ClientConfigurationAuthenticationState = {
  previousSourceConfig: [],
  previousQuestionConfig: [],
  currentSourceConfig: [],
  currentQuestionConfig: [],
  loading: false
};

const { actions, reducer } = createSlice({
  name: 'clientConfigAuthentication',
  initialState,
  reducers: {
    toggleSourceConfig: (
      draftState,
      action: PayloadAction<ToggleConfigPayload>
    ) => {
      const { id, checked } = action.payload;
      const prev = new Set(draftState.currentSourceConfig);
      if (checked) {
        prev.add(id);
      } else {
        prev.delete(id);
      }
      draftState.currentSourceConfig = Array.from(prev);
    },
    toggleQuestionConfig: (
      draftState,
      action: PayloadAction<ToggleConfigPayload>
    ) => {
      const { id, checked } = action.payload;
      const prev = new Set(draftState.currentQuestionConfig);
      if (checked) {
        prev.add(id);
      } else {
        prev.delete(id);
      }
      draftState.currentQuestionConfig = Array.from(prev);
    },
    resetToPrevious: draftState => {
      draftState.currentSourceConfig = draftState.previousSourceConfig;
      draftState.currentQuestionConfig = draftState.previousQuestionConfig;
    }
  },
  extraReducers: builder => {
    triggerSubmitAuthenticationConfigBuilder(builder);
    getAuthenticationConfigurationBuilder(builder);
  }
});

const allActions = {
  ...actions,
  triggerSubmitAuthenticationConfig,
  getAuthenticationConfiguration
};

export { allActions as clientConfigAuthenticationActions, reducer };
