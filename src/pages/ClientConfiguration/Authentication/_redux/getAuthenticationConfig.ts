import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { AUTHENTICATION_COMPONENT_ID, ROLE } from 'app/constants';
import { selectConfig } from 'pages/ClientConfiguration/_redux/selectors';
import { clientConfigAuthenticationServices } from '../clientConfigAuthenticationService';
import { DEFAULT_QUESTION_CONFIG, DEFAULT_SOURCE_CONFIG } from '../constants';
import {
  ClientConfigurationAuthenticationPayload,
  ClientConfigurationAuthenticationState
} from '../types';

export const getAuthenticationConfiguration = createAsyncThunk<
  ClientConfigurationAuthenticationPayload,
  undefined,
  ThunkAPIConfig
>(
  'clientConfigAuthentication/getAuthenticationConfig',
  async (args, thunkAPI) => {
    const { getState } = thunkAPI;
    const { clientId, systemId, principleId, agentId } = selectConfig(
      getState()
    ) as Record<string, string>;
    const cspaSelected = `${clientId}-${systemId}-${principleId}-${agentId}`;
    const { data } =
      await clientConfigAuthenticationServices.getAuthenticationConfig({
        common: {
          privileges: [ROLE.ADMIN]
        },
        selectConfig: 'clientConfig',
        selectComponent: AUTHENTICATION_COMPONENT_ID,
        selectCspa: cspaSelected
      });

    const configs = data.configs.clientConfig.cspas;
    const cspaConfig = (configs as CollectionsClientConfigItem[]).find(
      item => item.cspa === cspaSelected
    );
    if (cspaConfig === undefined)
      return thunkAPI.rejectWithValue({ errorMessage: 'no cspa config' });

    const authenticationConfig = cspaConfig.components.find(
      item => item.component === AUTHENTICATION_COMPONENT_ID
    );

    if (authenticationConfig === undefined)
      return thunkAPI.rejectWithValue({
        errorMessage: 'no authentication config'
      });

    const returnData = authenticationConfig?.jsonValue;

    return returnData as unknown as ClientConfigurationAuthenticationPayload;
  }
);

export const getAuthenticationConfigurationBuilder = (
  builder: ActionReducerMapBuilder<ClientConfigurationAuthenticationState>
) => {
  builder
    .addCase(getAuthenticationConfiguration.pending, draftState => {
      draftState.loading = true;
    })
    .addCase(getAuthenticationConfiguration.fulfilled, (draftState, action) => {
      const { questionConfig, sourceConfig } = action.payload;
      draftState.currentQuestionConfig = questionConfig ?? [];
      draftState.currentSourceConfig = sourceConfig ?? [];
      draftState.previousQuestionConfig = questionConfig ?? [];
      draftState.previousSourceConfig = sourceConfig ?? [];
      draftState.loading = false;
    })
    .addCase(getAuthenticationConfiguration.rejected, (draftState, action) => {
      draftState.loading = false;
      draftState.currentQuestionConfig = DEFAULT_QUESTION_CONFIG;
      draftState.currentSourceConfig = DEFAULT_SOURCE_CONFIG;
      draftState.previousQuestionConfig = DEFAULT_QUESTION_CONFIG;
      draftState.previousSourceConfig = DEFAULT_SOURCE_CONFIG;
    });
};
