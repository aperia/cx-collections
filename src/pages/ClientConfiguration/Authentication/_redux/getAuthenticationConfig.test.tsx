import { createStore, Store } from '@reduxjs/toolkit';

import { getAuthenticationConfiguration } from './getAuthenticationConfig';

import { clientConfigAuthenticationServices } from '../clientConfigAuthenticationService';

import { responseDefault } from 'app/test-utils';

import { rootReducer } from 'storeConfig';
import { AUTHENTICATION_COMPONENT_ID } from 'app/constants';

let store: Store<RootState>;
let state: RootState;

const mClientId = '1234';
const mSystemId = '1234';
const mPrincipleId = '1234';
const mAgentId = '1234';

const mCspa = `${mClientId}-${mSystemId}-${mPrincipleId}-${mAgentId}`;

beforeEach(() => {
  store = createStore(rootReducer, {
    clientConfiguration: {
      settings: {
        selectedConfig: {
          clientId: mClientId,
          systemId: mSystemId,
          principleId: mPrincipleId,
          agentId: mAgentId
        }
      }
    }
  });
  state = store.getState();
});

let spy1: jest.SpyInstance;

afterEach(() => {
  spy1?.mockReset();
  spy1?.mockRestore();
});

const mockResponseValue = (cspa?: string, component?: string) => ({
  configs: {
    clientConfig: {
      cspas: [
        {
          cspa: cspa ?? mCspa,
          components: [
            {
              component: component ?? AUTHENTICATION_COMPONENT_ID,
              jsonValue: {
                sourceConfig: ['sourceConfig'],
                questionConfig: ['questionConfig']
              }
            }
          ]
        }
      ]
    }
  }
});

describe('should have test getAuthenticationConfig', () => {
  it('Should return data', async () => {
    spy1 = jest
      .spyOn(clientConfigAuthenticationServices, 'getAuthenticationConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: mockResponseValue()
      });

    const response = await getAuthenticationConfiguration()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      sourceConfig: ['sourceConfig'],
      questionConfig: ['questionConfig']
    });
  });

  it('Should return empty cspaConfig', async () => {
    spy1 = jest
      .spyOn(clientConfigAuthenticationServices, 'getAuthenticationConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: mockResponseValue('1111-1111-1111-1111')
      });

    const response = await getAuthenticationConfiguration()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ errorMessage: 'no cspa config' });
  });

  it('Should return empty authenticationConfig', async () => {
    spy1 = jest
      .spyOn(clientConfigAuthenticationServices, 'getAuthenticationConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: mockResponseValue(undefined, 'any-component')
      });

    const response = await getAuthenticationConfiguration()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      errorMessage: 'no authentication config'
    });
  });

  it('should response getAuthenticationConfig pending', () => {
    const pendingAction = getAuthenticationConfiguration.pending(
      getAuthenticationConfiguration.pending.type,
      undefined
    );
    const actual = rootReducer(state, pendingAction);

    expect(actual?.authenticationConfig?.loading).toEqual(true);
  });

  it('should response getAuthenticationConfiguration fulfilled', () => {
    const fulfilled = getAuthenticationConfiguration.fulfilled(
      { sourceConfig: ['sourceConfig'], questionConfig: ['questionConfig'] },
      getAuthenticationConfiguration.fulfilled.type,
      undefined
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual?.authenticationConfig?.loading).toEqual(false);

    expect(actual?.authenticationConfig?.currentQuestionConfig).toEqual([
      'questionConfig'
    ]);
    expect(actual?.authenticationConfig?.currentSourceConfig).toEqual([
      'sourceConfig'
    ]);
    expect(actual?.authenticationConfig?.previousQuestionConfig).toEqual([
      'questionConfig'
    ]);
    expect(actual?.authenticationConfig?.previousSourceConfig).toEqual([
      'sourceConfig'
    ]);
  });

  it('should response getAuthenticationConfiguration fulfilled with empty data', () => {
    const fulfilled = getAuthenticationConfiguration.fulfilled(
      {} as never,
      getAuthenticationConfiguration.fulfilled.type,
      undefined
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual?.authenticationConfig?.loading).toEqual(false);

    expect(actual?.authenticationConfig?.currentQuestionConfig).toEqual([]);
    expect(actual?.authenticationConfig?.currentSourceConfig).toEqual([]);
    expect(actual?.authenticationConfig?.previousQuestionConfig).toEqual([]);
    expect(actual?.authenticationConfig?.previousSourceConfig).toEqual([]);
  });

  it('should response getAuthenticationConfiguration rejected', () => {
    const rejected = getAuthenticationConfiguration.rejected(
      null,
      getAuthenticationConfiguration.rejected.type,
      undefined
    );
    const actual = rootReducer(state, rejected);

    expect(actual?.authenticationConfig?.loading).toEqual(false);
  });
});
