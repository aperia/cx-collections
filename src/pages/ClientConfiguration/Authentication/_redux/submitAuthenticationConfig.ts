import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { selectConfig } from 'pages/ClientConfiguration/_redux/selectors';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

import { clientConfigAuthenticationServices } from '../clientConfigAuthenticationService';

import { AUTHENTICATION_COMPONENT_ID } from 'app/constants';
import { I18N_CLIENT_CONFIG_AUTH } from '../constants';
import { ClientConfigurationAuthenticationState } from '../types';
import { saveChangedClientConfig } from 'pages/ClientConfiguration/ChangeHistory/_redux/saveChangedClientConfig';
import { batch } from 'react-redux';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';

export const submitAuthenticationConfigRequest = createAsyncThunk<
  undefined,
  undefined,
  ThunkAPIConfig
>(
  'clientConfigAuthentication/submitAuthenticationConfigRequest',
  async (args, thunkAPI) => {
    const { authenticationConfig } = thunkAPI.getState();
    const requestValue = {
      sourceConfig: authenticationConfig.currentSourceConfig,
      questionConfig: authenticationConfig.currentQuestionConfig
    };
    const { dispatch, getState } = thunkAPI;

    const {
      user = '',
      app = '',
      org = '',
      privileges
    } = window.appConfig?.commonConfig || {};

    const { clientId, systemId, principleId, agentId, description, status } =
      selectConfig(getState()) as Record<string, string>;
    const cspaSelected = `${clientId}-${systemId}-${principleId}-${agentId}`;
    try {
      await clientConfigAuthenticationServices.submitAuthentication({
        common: {
          agent: agentId,
          clientNumber: clientId,
          system: systemId,
          prin: principleId,
          user,
          app,
          org,
          privileges
        },
        clientConfig: {
          cspas: [
            {
              cspa: cspaSelected,
              components: [
                {
                  component: AUTHENTICATION_COMPONENT_ID,
                  jsonValue: requestValue
                }
              ],
              description,
              active: status
            }
          ]
        }
      });
      return undefined;
    } catch (error) {
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION,
          forSection: 'inClientConfigurationFeatureHeader',
          apiResponse: error
        })
      );

      return thunkAPI.rejectWithValue({ response: error });
    }
  }
);

export const triggerSubmitAuthenticationConfig = createAsyncThunk<
  unknown,
  undefined,
  ThunkAPIConfig
>(
  'clientConfigAuthentication/triggerSubmitAuthenticationConfig',
  async (args, thunkAPI) => {
    const { dispatch, getState } = thunkAPI;
    const oldValue = {
      sourceConfig: getState().authenticationConfig.previousSourceConfig,
      questionConfig: getState().authenticationConfig.previousQuestionConfig
    };
    const newValue = {
      sourceConfig: getState().authenticationConfig.currentSourceConfig,
      questionConfig: getState().authenticationConfig.currentQuestionConfig
    };
    const response = await dispatch(submitAuthenticationConfigRequest());

    if (isFulfilled(response)) {
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_CLIENT_CONFIG_AUTH.SAVE_CHANGE_SUCCESS
          })
        );
        dispatch(
          saveChangedClientConfig({
            action: 'UPDATE',
            changedCategory: 'authentication',
            oldValue,
            newValue
          })
        );
      });
    }

    if (isRejected(response)) {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_CLIENT_CONFIG_AUTH.SAVE_CHANGE_FAILURE
        })
      );
    }
  }
);

export const triggerSubmitAuthenticationConfigBuilder = (
  builder: ActionReducerMapBuilder<ClientConfigurationAuthenticationState>
) => {
  builder
    .addCase(
      triggerSubmitAuthenticationConfig.pending,
      (draftState, action) => {
        draftState.loading = true;
      }
    )
    .addCase(
      triggerSubmitAuthenticationConfig.fulfilled,
      (draftState, action) => {
        draftState.loading = false;
        draftState.previousQuestionConfig = draftState.currentQuestionConfig;
        draftState.previousSourceConfig = draftState.currentSourceConfig;
      }
    )
    .addCase(
      triggerSubmitAuthenticationConfig.rejected,
      (draftState, action) => {
        draftState.loading = false;
      }
    );
};
