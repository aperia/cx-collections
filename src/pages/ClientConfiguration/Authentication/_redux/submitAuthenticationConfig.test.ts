import './reducers';

//redux
import { createStore, Store } from '@reduxjs/toolkit';
import { rootReducer } from 'storeConfig';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

//api
import {
  submitAuthenticationConfigRequest,
  triggerSubmitAuthenticationConfig
} from './submitAuthenticationConfig';

//services
import { clientConfigAuthenticationServices } from '../clientConfigAuthenticationService';

//constants
import { I18N_CLIENT_CONFIG_AUTH } from '../constants';

import {
  responseDefault,
  byPassFulfilled,
  mockActionCreator,
  byPassRejected
} from 'app/test-utils';

let spy: jest.SpyInstance;

let store: Store<RootState>;
let state: RootState;

const mockToast = mockActionCreator(actionsToast);
const mClientId = '1234';
const mSystemId = '1234';
const mPrincipleId = '1234';
const mAgentId = '1234';

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

beforeEach(() => {
  store = createStore(rootReducer, {
    clientConfiguration: {
      settings: {
        selectedConfig: {
          clientId: mClientId,
          systemId: mSystemId,
          principleId: mPrincipleId,
          agentId: mAgentId
        }
      }
    },
    authenticationConfig: {
      previousSourceConfig: ['previousSource'],
      previousQuestionConfig: ['previousQuestion'],
      currentSourceConfig: ['currentSource'],
      currentQuestionConfig: ['currentQuestion'],
      loading: false
    }
  });

  state = store.getState();
});

describe('Test submitAuthenticationConfigRequest', () => {
  it('Should return data', async () => {
    jest
      .spyOn(clientConfigAuthenticationServices, 'submitAuthentication')
      .mockResolvedValue({ ...responseDefault });

    const response = await submitAuthenticationConfigRequest()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('should return error', async () => {
    jest
      .spyOn(clientConfigAuthenticationServices, 'submitAuthentication')
      .mockRejectedValue({ ...responseDefault });

    const response = await submitAuthenticationConfigRequest()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      submitAuthenticationConfigRequest.rejected.type
    );
  });
});

describe('Authentication  > triggerSubmitAuthenticationConfig', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {});
  });

  it('success', async () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        user: 'user',
        app: 'app',
        org: 'org'
      } as CommonConfig
    };

    spy = jest
      .spyOn(clientConfigAuthenticationServices, 'submitAuthentication')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    const toast = mockToast('addToast');

    const response = await triggerSubmitAuthenticationConfig()(
      byPassFulfilled(triggerSubmitAuthenticationConfig.fulfilled.type),
      store.getState,
      { sourceConfig: ['123'], questionConfig: ['123'] }
    );

    expect(response.type).toBe(
      triggerSubmitAuthenticationConfig.fulfilled.type
    );

    expect(toast).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_CLIENT_CONFIG_AUTH.SAVE_CHANGE_SUCCESS
    });
  });

  it('error', async () => {
    const toast = mockToast('addToast');

    jest
      .spyOn(clientConfigAuthenticationServices, 'submitAuthentication')
      .mockRejectedValue({ ...responseDefault });

    await triggerSubmitAuthenticationConfig()(
      byPassRejected(triggerSubmitAuthenticationConfig.rejected.type),
      store.getState,
      {}
    );

    expect(toast).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_CLIENT_CONFIG_AUTH.SAVE_CHANGE_FAILURE
    });
  });

  describe('should be triggerSubmitAuthenticationConfigBuilder', () => {
    it('should response triggerSubmitAuthenticationConfig pending', () => {
      const pendingAction = triggerSubmitAuthenticationConfig.pending(
        triggerSubmitAuthenticationConfig.pending.type,
        undefined
      );
      const actual = rootReducer(state, pendingAction);

      expect(actual.authenticationConfig?.loading).toEqual(true);
    });

    it('should response triggerSubmitAuthenticationConfig fulfilled', () => {
      const fulfilled = triggerSubmitAuthenticationConfig.fulfilled(
        null,
        triggerSubmitAuthenticationConfig.pending.type,
        undefined
      );
      const actual = rootReducer(state, fulfilled);

      expect(actual.authenticationConfig?.loading).toEqual(false);

      expect(actual.authenticationConfig.previousQuestionConfig).toEqual([
        'currentQuestion'
      ]);
      expect(actual.authenticationConfig.previousSourceConfig).toEqual([
        'currentSource'
      ]);
    });

    it('should response triggerSubmitAuthenticationConfig rejected', () => {
      const rejected = triggerSubmitAuthenticationConfig.rejected(
        null,
        triggerSubmitAuthenticationConfig.rejected.type,
        undefined
      );
      const actual = rootReducer(state, rejected);

      expect(actual.authenticationConfig?.loading).toEqual(false);
    });
  });
});
