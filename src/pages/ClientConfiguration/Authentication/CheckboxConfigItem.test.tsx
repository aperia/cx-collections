import React from 'react';

//react-testing-lib
import { screen, render } from '@testing-library/react';

//components
import { CheckboxConfigItem } from './CheckboxConfigItem';

import userEvent from '@testing-library/user-event';

jest.mock('app/_libraries/_dls/hooks', () => ({
  ...jest.requireActual('app/_libraries/_dls/hooks'),
  useTranslation: () => ({ t: (text: string) => text })
}));

describe('render actions buttons ', () => {
  const mockOnChange = jest.fn();
  const { getByText } = render(
    <CheckboxConfigItem
      checked={false}
      title="Test"
      id="checkbox1"
      onChange={mockOnChange}
    />
  );

  it('should render check box', () => {
    const checkBoxTitle = getByText('Test');

    expect(checkBoxTitle).toBeInTheDocument();
  });

  it('should render check box and click', () => {
    render(
      <CheckboxConfigItem
        checked={false}
        title="Test"
        id="checkbox1"
        onChange={mockOnChange}
      />
    );

    const checkbox = screen.getByRole('checkbox');

    userEvent.click(checkbox);

    expect(mockOnChange).toBeCalled();
  });
});
