export interface AuthenticationConfigItem {
  title: string;
  checked: boolean;
  id: string;
}

export type ToggleConfigPayload = Omit<AuthenticationConfigItem, 'title'>;

export interface ClientConfigurationAuthenticationPayload {
  sourceConfig: string[];
  questionConfig: string[];
}

export interface ClientConfigurationAuthenticationState {
  previousSourceConfig: string[];
  previousQuestionConfig: string[];
  currentSourceConfig: string[];
  currentQuestionConfig: string[];
  loading: boolean;
}
