import { FormKy } from './types';

export enum SECTION_TAB_EVENT {
  CHANGING_TAB = 'ClientConfig_ChangingTab',
  CHANGE_TAB = 'ClientConfig_ControlChangeTab',
  CLOSING_MODAL = 'ClientConfig_ClosingTab',
  CLOSE_MODAL = 'ClientConfig_CloseModal'
}

export const CODE_ALL = '$ALL';

export const ACTIVE = 'txt_active';

export const ACTIVATED = 'txt_activated';

export const DEACTIVATED = 'txt_deactivated';

export const DEACTIVATE = 'txt_deactivate';

export const ACTIVATE = 'txt_activate';

export const INACTIVE = 'txt_inactive';

export const DEFAULT_CLIENT_CONFIG_KEY = 'isDefaultClientInfoKey';

export const CLIENT_CONFIG_STATUS_KEY = 'isConfigActive';

export const NAME_ACTION = {
  addCspa: 'addCSPA',
  editCspa: 'editCSPA',
  addContact: 'addContactAcc'
};

export const CLIENT_CONFIG_FORM_FIELDS: Record<string, FormKy> = {
  CLIENT_RADIO: 'clientRadio',
  CLIENT_ID: 'clientId',
  SYSTEM_ID: 'systemId',
  SYSTEM_RADIO: 'systemRadio',
  PRINCIPLE_ID: 'principleId',
  PRINCIPLE_RADIO: 'principleRadio',
  AGENT_ID: 'agentId',
  AGENT_RADIO: 'agentRadio',
  DESCRIPTION: 'description'
};

export const DEFAULT_COMMON_REQUEST_CLIENT_CONFIG = {
  common: {
    agent: '3000',
    clientNumber: '9999',
    system: '1000',
    prin: '2000',
    user: 'NSA1',
    app: 'cx'
  }
};

export const GET_ALL_CLIENT_CONFIG_REQUEST = {
  common: {
    clientName: 'firstdata',
    agent: '3000',
    clientNumber: '9999',
    system: '1000',
    prin: '2000',
    user: 'NSA1',
    app: 'cx',
    org: 'TEST'
  },
  fetchSize: '500'
};

//hard code binsequences
export const BINSEQUENCES = [
  {
    startingBinSequence: '100000',
    endingBinSequence: '200000'
  }
];
export const CLIENT_CONFIG_FORM_MAX_LENGTH = 4;
export const CLIENT_CONFIG_FORM_DESCRIPTION_MAX_LENGTH = 100;
export const DEFAULT_TOP_HEIGHT = '88';
export const FETCH_SIZE = '500';
export const QUEUE_MAPPING_COMPONENT = 'queue-mapping';

export const REGEX_ALLOW_INPUT_CHARS =
  /^[a-zA-Z0-9\s!@#$^&*_\-=\[\]':|,.<>\/?]*$/g;
export const REGEX_CSPA_DESCRIPTION = /^[a-zA-Z0-9\s$-]*$/g;
export const REGEX_CHECK_SPECIAL_CHARS = /["%(){}+;\\]/;
