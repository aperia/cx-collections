import React from 'react';
import { screen } from '@testing-library/dom';

import { mockActionCreator, renderMockStore } from 'app/test-utils';

import * as helpers from '../helpers';
import AdjustmentTransaction from '.';
import { letterConfigActions as actions } from './_redux/reducers';
import { FormModalProps, LetterConfigData, LetterState } from './types';
import { SECTION_TAB_EVENT } from '../constants';

jest.mock('./FormModal', () => (props: FormModalProps) => (
  <button
    data-testid="submitBtn"
    onClick={() => {
      props.onSubmit({} as LetterConfigData);
    }}
  />
));

const initialState: Partial<RootState> = {
  letterConfig: {
    letterConfigurationList: { loading: false, data: [] },
    saveLetterModal: { open: false } as LetterState['saveLetterModal']
  }
};

describe('Test Client Configuration Leter component', () => {
  const spyActions = mockActionCreator(actions);

  it('Should render UI', () => {
    const mockRemoveStoreAction = spyActions('saveLetterConfiguration');
    renderMockStore(<AdjustmentTransaction />, {
      initialState
    });

    const submitBtn = screen.getByTestId('submitBtn');
    submitBtn.click();

    expect(screen.getByText('txt_letter_config_title')).toBeInTheDocument();
    expect(mockRemoveStoreAction).toBeCalledWith({});
  });

  it('Should not call anything due to lockSubmit true', () => {
    const mockRemoveStoreAction = spyActions('saveLetterConfiguration');
    renderMockStore(<AdjustmentTransaction />, {
      initialState: {
        letterConfig: {
          letterConfigurationList: { loading: false, data: [] },
          saveLetterModal: {
            open: false,
            loading: true
          } as LetterState['saveLetterModal']
        }
      }
    });

    const submitBtn = screen.getByTestId('submitBtn');
    submitBtn.click();

    expect(screen.getByText('txt_letter_config_title')).toBeInTheDocument();
    expect(mockRemoveStoreAction).not.toBeCalled();
  });

  it('Should trigger useListenChangingTabEvent', () => {
    const mockRemoveStoreAction = spyActions('removeStore');
    const mockDispatchControlTabChangeEvent = jest
      .spyOn(helpers, 'dispatchControlTabChangeEvent')
      .mockImplementationOnce(jest.fn);
    const emitChangingTabEvent = () => {
      const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGING_TAB, {
        bubbles: true,
        cancelable: true
      });

      window.dispatchEvent(event);
    };

    renderMockStore(<AdjustmentTransaction />, {
      initialState
    });

    emitChangingTabEvent();

    expect(mockRemoveStoreAction).toHaveBeenCalled();
    expect(mockDispatchControlTabChangeEvent).toHaveBeenCalled();
  });

  it('Should trigger useListenClosingModal', () => {
    const mockRemoveStoreAction = spyActions('removeStore');
    const mockDispatchCloseModalEvent = jest
      .spyOn(helpers, 'dispatchCloseModalEvent')
      .mockImplementationOnce(jest.fn);
    const emitClosingModalEvent = () => {
      const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
        bubbles: true,
        cancelable: true
      });

      window.dispatchEvent(event);
    };

    renderMockStore(<AdjustmentTransaction />, {
      initialState
    });

    emitClosingModalEvent();

    expect(mockRemoveStoreAction).toHaveBeenCalled();
    expect(mockDispatchCloseModalEvent).toHaveBeenCalled();
  });
});
