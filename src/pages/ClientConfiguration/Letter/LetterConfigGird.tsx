import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';

// components
import { FailedApiReload, NoDataGrid } from 'app/components';
import {
  Button,
  ColumnType,
  Grid,
  GridRef,
  Pagination,
  SortType
} from 'app/_libraries/_dls/components';
import { Actions } from './Actions';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { usePagination } from 'app/hooks';

// helpers
import classNames from 'classnames';

// redux
import { useDispatch, useSelector } from 'react-redux';
import {
  selectLetterConfigurationData,
  selectedLetterConfigurationListError,
  selectLetterConfigurationLoading
} from './_redux/selectors';
import { letterConfigActions } from './_redux/reducers';

// constants
import { FORM_FIELD } from './constants';
import { I18N_ADJUSTMENT_TRANSACTION } from '../AdjustmentTransaction/constants';
import { I18N_LETTER_CONFIG } from 'app/constants/i18n';

// types
import { LetterConfigData } from './types';
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface LetterConfigGridProps { }

const Header = ({ className }: { className?: string }) => {
  const { t } = useTranslation();
  return (
    <p
      className={classNames('fs-14 fw-500 color-grey-d20', className)}
      data-testid={genAmtId('clientConfig_letter_grid', 'header', '')}
    >
      {t('txt_letter_config_title_header')}
    </p>
  );
};

const LetterConfigGrid: React.FC<LetterConfigGridProps> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const gridRefs = useRef<GridRef>(null);
  const testId = 'clientConfig_letter_grid';

  const status = useCheckClientConfigStatus();

  const isLoading = useSelector(selectLetterConfigurationLoading);
  const isError = useSelector(selectedLetterConfigurationListError);
  const data = useSelector(selectLetterConfigurationData);
  const [sortBy, setSortBy] = useState<SortType[]>([]);

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(data);

  const handleReload = useCallback(() => {
    dispatch(letterConfigActions.getLetterConfiguration());
  }, [dispatch]);

  useEffect(() => {
    handleReload();
  }, [handleReload]);
  
  // reset sort
  useEffect(() => () => {
    dispatch(letterConfigActions.sortBy({ id: '' }));
  }, [dispatch]);

  const showPagination = total > pageSize[0];

  const dataColumn = useMemo(
    (): ColumnType<LetterConfigData>[] => [
      {
        id: FORM_FIELD.ID.name,
        Header: t(I18N_LETTER_CONFIG.LETTER_CONFIG_NUMBER),
        accessor: 'letterId',
        isSort: true,
        width: 140
      },
      {
        id: FORM_FIELD.TYPE_CODE.name,
        Header: t(I18N_LETTER_CONFIG.LETTER_TYPE_CODE),
        accessor: 'letterCode',
        isSort: true,
        width: 155
      },
      {
        id: FORM_FIELD.DESCRIPTION.name,
        Header: t(I18N_LETTER_CONFIG.LETTER_CONFIG_DESCRIPTION),
        accessor: 'letterDescription',
        isSort: true,
        width: 380,
        autoWidth: true
      },
      {
        id: FORM_FIELD.LETTER_VARIABLES.name,
        Header: t(I18N_LETTER_CONFIG.LETTER_VARIABLES),
        accessor: values => (values.variableLetter ? 'Yes' : 'No'),
        isSort: true,
        width: 172
      },
      {
        id: 'action',
        Header: t(I18N_LETTER_CONFIG.ACTIONS),
        accessor: (values, idx) => (
          <Actions
            status={status}
            record={values}
            dataTestId={`${idx}_action_${testId}`}
          />
        ),
        width: 136,
        isFixedRight: true,
        cellProps: { className: 'text-center' },
        cellBodyProps: { className: 'text-center' },
        className: 'td-sm'
      }
    ],
    [t, status]
  );

  const handleOpenAddLetterModal = () => {
    dispatch(letterConfigActions.toggleSaveLetterModal());
  };

  if (isLoading) return <div className="vh-50" />;

  if (isError)
    return (
      <>
        <Header className="mt-16" />
        <FailedApiReload
          id="letter-config-request-fail"
          className="mt-80"
          onReload={handleReload}
          dataTestId={`${testId}_failedApi`}
        />
      </>
    );

  if (!data.length)
    return (
      <>
        <Header className="mt-16" />
        <NoDataGrid
          text={t(I18N_ADJUSTMENT_TRANSACTION.NO_DATA)}
          dataTestId={`${testId}_noData`}
        >
          <div className="mt-24">
            <Button
              disabled={status}
              size="sm"
              variant="outline-primary"
              onClick={handleOpenAddLetterModal}
              dataTestId={`${testId}_addLetterBtn`}
            >
              {t(I18N_LETTER_CONFIG.ADD_LETTER)}
            </Button>
          </div>
        </NoDataGrid>
      </>
    );

  const handleSortChange = (sortType: SortType) => {
    setSortBy([sortType]);
    dispatch(letterConfigActions.sortBy(sortType));
  };

  return (
    <>
      <div className="mt-16 d-flex justify-content-between align-items-center">
        <Header />
        <div className="mr-n8">
          <Button
            disabled={status}
            size="sm"
            variant="outline-primary"
            onClick={handleOpenAddLetterModal}
            dataTestId={`${testId}_addLetterBtn`}
          >
            {t(I18N_LETTER_CONFIG.ADD_LETTER)}
          </Button>
        </div>
      </div>
      <div className="mt-16">
        <Grid
          ref={gridRefs}
          columns={dataColumn}
          data={gridData}
          onSortChange={handleSortChange}
          sortBy={sortBy}
          dataTestId={`${testId}_grid`}
        />
        {showPagination && (
          <div className="pt-16">
            <Pagination
              totalItem={total}
              pageSize={pageSize}
              pageNumber={currentPage}
              pageSizeValue={currentPageSize}
              compact
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
              dataTestId={`${testId}_pagination`}
            />
          </div>
        )}
      </div>
    </>
  );
};

export default LetterConfigGrid;
