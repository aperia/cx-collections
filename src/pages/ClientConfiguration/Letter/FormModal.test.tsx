import React from 'react';
import { INITIAL_FORM_DATA } from './constants';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import FormModal from './FormModal';
import { LetterState } from './types';
import userEvent from '@testing-library/user-event';
import { screen } from '@testing-library/dom';
import { letterConfigActions } from './_redux/reducers';
import { waitFor } from '@testing-library/react';

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockLetterConfigActions = mockActionCreator(letterConfigActions);

const mockSubmit = jest.fn();

describe('should test letter form modal', () => {
  it('should render ui with add', () => {
    const mockToggleSaveLetterModal = mockLetterConfigActions(
      'toggleSaveLetterModal'
    );

    // The use of `act` and/or `jest.useFakeTimers`
    // could not solve jest warning of wrapping the render function in `act`
    jest.useFakeTimers();
    waitFor(() => {
      renderMockStore(<FormModal onSubmit={mockSubmit} />, {
        initialState: {
          letterConfig: {
            saveLetterModal: {
              open: true,
              letter: INITIAL_FORM_DATA
            }
          } as LetterState
        }
      });
    });
    jest.runAllTimers();

    const cancelBtn = screen.getByText('txt_cancel');
    userEvent.click(cancelBtn);
    expect(mockToggleSaveLetterModal).toBeCalled();
    expect(screen.getByText('txt_submit')).toBeInTheDocument();
    expect(screen.queryByText('txt_save')).not.toBeInTheDocument();
  });

  it('should render ui with edit', () => {
    const mockToggleSaveLetterModal = mockLetterConfigActions(
      'toggleSaveLetterModal'
    );

    // The use of `act` and/or `jest.useFakeTimers`
    // could not solve jest warning of wrapping the render function in `act`
    jest.useFakeTimers();
    waitFor(() => {
      renderMockStore(<FormModal onSubmit={mockSubmit} />, {
        initialState: {
          letterConfig: {
            saveLetterModal: {
              open: true,
              letter: {
                ...INITIAL_FORM_DATA,
                letterId: '123'
              }
            }
          } as LetterState
        }
      });
    });
    jest.runAllTimers();

    const cancelBtn = screen.getByText('txt_cancel');
    userEvent.click(cancelBtn);
    expect(mockToggleSaveLetterModal).toBeCalled();
    expect(screen.getByText('txt_save')).toBeInTheDocument();
    expect(screen.queryByText('txt_submit')).not.toBeInTheDocument();
  });

  it('should render ui with letter variable', () => {
    const mockToggleSaveLetterModal = mockLetterConfigActions(
      'toggleSaveLetterModal'
    );

    // The use of `act` and/or `jest.useFakeTimers`
    // could not solve jest warning of wrapping the render function in `act`
    jest.useFakeTimers();
    waitFor(() => {
      renderMockStore(<FormModal onSubmit={mockSubmit} />, {
        initialState: {
          letterConfig: {
            saveLetterModal: {
              open: true,
              letter: {
                ...INITIAL_FORM_DATA,
                captionMapping: {
                  letterCaptionFields: '',
                  generateLetterFieldMapping: ''
                } as MagicKeyValue
              }
            }
          } as LetterState
        }
      });
    });
    jest.runAllTimers();

    const cancelBtn = screen.getByText('txt_cancel');
    userEvent.click(cancelBtn);
    expect(mockToggleSaveLetterModal).toBeCalled();
    expect(screen.getByText('txt_submit')).toBeInTheDocument();
    expect(screen.queryByText('txt_save')).not.toBeInTheDocument();
  });
});
