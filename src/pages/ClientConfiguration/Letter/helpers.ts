import { FORM_FIELD } from './constants';
import * as Yup from 'yup';
import { I18N_LETTER_CONFIG } from 'app/constants/i18n';

export const letterConfigValidationSchema = () => {
  return Yup.object().shape({
    [FORM_FIELD.ID.name]: Yup.string()
      .trim()
      .required(I18N_LETTER_CONFIG.LETTER_NUMBER_IS_REQUIRED),
    [FORM_FIELD.TYPE_CODE.name]: Yup.string()
      .trim()
      .required(I18N_LETTER_CONFIG.LETTER_TYPE_CODE_IS_REQUIRED),
    [FORM_FIELD.DESCRIPTION.name]: Yup.string()
      .trim()
      .required(I18N_LETTER_CONFIG.LETTER_DESCRIPTION_IS_REQUIRED),
    [FORM_FIELD.CAPTION_MAPPING.name]: Yup.array().of(
      Yup.object().shape({
        [FORM_FIELD.LETTER_CAPTION_FIELDS.name]: Yup.string().required(
          I18N_LETTER_CONFIG.LETTER_CATION_FIELDS_IS_REQUIRED
        ),
        [FORM_FIELD.GENERATE_LETTER_FIELD_MAPPING.name]: Yup.string().required(
          I18N_LETTER_CONFIG.GENERATE_LETTER_FIELD_MAPPING_IS_REQUIRED
        )
      })
    )
  });
};
