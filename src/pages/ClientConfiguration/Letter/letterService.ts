import { apiService } from 'app/utils/api.service';
import { LetterConfigData } from './types';

export const letterConfigService = {
  getLetterConfiguration() {
    const url =
      window.appConfig?.api?.letterConfig?.getLetterConfiguration || '';
    return apiService.post(url, {});
  },

  deleteLetterConfiguration(data: LetterConfigData) {
    const url =
      window.appConfig?.api?.letterConfig?.deleteLetterConfiguration || '';
    return apiService.post(url, { data });
  },

  saveLetterConfiguration(args: LetterConfigData) {
    const url =
      window.appConfig?.api?.letterConfig?.saveLetterConfiguration || '';

    return apiService.post(url, args);
  },

  getLetterCaptionFields() {
    const url =
      window.appConfig?.api?.letterConfig?.getLetterCaptionFields || '';

    return apiService.get(url);
  },

  getGenerateLetterFieldMapping() {
    const url =
      window.appConfig?.api?.letterConfig?.getGenerateLetterFieldMapping || '';

    return apiService.get(url);
  }
};
