import { renderMockStore } from 'app/test-utils';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import React from 'react';
import LetterVariable from './LetterVariable';
import { LetterConfigData, LetterVariableProps } from './types';

HTMLCanvasElement.prototype.getContext = jest.fn();

const actionLetter: LetterConfigData = {
  id: '1',
  letterId: '1',
  letterCode: '1',
  letterDescription: '1',
  variableLetter: true,
  captionMapping: [
    {
      letterCaptionFields: '1',
      generateLetterFieldMapping: '1'
    }
  ]
};

const mockSetValues = jest.fn();
const mockSetTouched = jest.fn();
const mockSetErrors = jest.fn();

const params = {
  index: 0,
  values: actionLetter,
  touched: {
    captionMapping: [
      {
        letterCaptionFields: '1',
        generateLetterFieldMapping: '1'
      }
    ]
  },
  errors: {
    captionMapping: [
      {
        letterCaptionFields: true,
        generateLetterFieldMapping: false
      }
    ]
  },
  setValues: mockSetValues,
  setTouched: mockSetTouched,
  setErrors: mockSetErrors
} as unknown as LetterVariableProps;

const paramsWithEmptyTouchedErrors = {
  index: 0,
  values: actionLetter,
  touched: {
    captionMapping: null
  },
  errors: {
    captionMapping: null
  },
  setValues: mockSetValues,
  setTouched: mockSetTouched,
  setErrors: mockSetErrors
} as unknown as LetterVariableProps;

describe('should test letter variable', () => {
  const letterVariableState: Partial<RootState> = {
    letterConfig: {
      letterConfigurationList: {
        data: []
      },
      saveLetterModal: {
        open: true,
        letter: actionLetter,
        letterVariableRefData: {
          letterCaptionFieldsData: [{ id: '1', value: '1', description: '1' }],
          generateLetterFieldMappingData: [
            { id: '1', value: '1', description: '1' }
          ]
        }
      }
    }
  };

  const renderWrapper = (
    initialState: Partial<RootState>,
    letterVariableParams: LetterVariableProps
  ) => {
    return renderMockStore(<LetterVariable {...letterVariableParams} />, {
      initialState
    });
  };

  it('should render ui', () => {
    const { baseElement } = renderWrapper(letterVariableState, params);

    const removeButton = queryByClass(
      baseElement as HTMLElement,
      'btn btn-icon-danger btn-sm align-self-end'
    );

    removeButton?.click();

    expect(mockSetValues).toBeCalled();
    expect(mockSetErrors).toBeCalled();
    expect(mockSetTouched).toBeCalled();
  });

  it('should should not render error', () => {
    const { baseElement } = renderWrapper(
      letterVariableState,
      paramsWithEmptyTouchedErrors
    );

    const removeButton = queryByClass(
      baseElement as HTMLElement,
      'btn btn-icon-danger btn-sm align-self-end'
    );

    removeButton?.click();

    expect(mockSetValues).toBeCalled();
    expect(mockSetErrors).toBeCalled();
    expect(mockSetTouched).toBeCalled();
  });
});
