import { FormikProps } from 'formik';
import { ClientInfoLetterTemplate } from '../types';

export interface LetterConfigData {
  letterId: string;
  letterCode: string;
  letterGroup?: string;
  letterDescription: string;
  variableLetter: boolean;
  captionMapping?: CaptionMapping[];
}

export interface LetterConfigError {
  letterId?: string;
  letterCode?: string;
  letterDescription?: string;
  captionMapping?: CaptionMapping[];
}

export interface CaptionMapping {
  letterCaptionFields: string;
  generateLetterFieldMapping: string;
}

export interface AddLetterPayload {
  postData: {
    letterId?: string;
    letterDescription?: string;
  };
}

export interface EditLetterPayload {
  postData: {
    prevRecord: LetterConfigData;
    newRecord: LetterConfigData;
  };
}

export interface LetterState {
  letterConfigurationList: {
    loading?: boolean;
    data: ClientInfoLetterTemplate[];
    error?: string;
  };
  saveLetterModal: {
    open: boolean;
    loading?: boolean;
    letter: ClientInfoLetterTemplate;
    letterVariableRefData?: LetterVariableRefData;
    error?: string;
  };
}

export interface FormModalProps {
  onSubmit: (values: LetterConfigData) => void;
}

export interface LetterVariableProps extends FormikProps<LetterConfigData>, DLSId {
  index: number;
}

export interface FilterItem {
  id?: string;
  value?: string;
  description?: string;
}

export interface LetterVariableRefData {
  letterCaptionFieldsData?: FilterItem[];
  generateLetterFieldMappingData?: FilterItem[];
}

export interface GetLetterVariableRefDataPayload
  extends LetterVariableRefData {}
