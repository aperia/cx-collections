import React from 'react';

import { mockActionCreator, renderMockStore } from 'app/test-utils';

import LetterConfigGird, { LetterConfigGridProps } from './LetterConfigGird';

// Hooks
import * as usePagination from 'app/hooks/usePagination';

import { letterConfigActions } from './_redux/reducers';
import { LetterConfigData, LetterState } from './types';
import { screen } from '@testing-library/dom';
import * as mockStatus from '../hooks/useCheckClientConfigStatus';

HTMLCanvasElement.prototype.getContext = jest.fn();

const spyLetterConfig = mockActionCreator(letterConfigActions);

const mockSetValues = jest.fn();
const mockSetTouched = jest.fn();
const mockSetErrors = jest.fn();

const actionLetter: LetterConfigData = {
  id: '1',
  letterId: '1',
  letterCode: '1',
  letterDescription: '1',
  variableLetter: true,
  captionMapping: [
    {
      letterCaptionFields: '1',
      generateLetterFieldMapping: '1'
    }
  ]
};

const params = {
  index: 0,
  values: actionLetter,
  touched: {
    captionMapping: [
      {
        letterCaptionFields: '1',
        generateLetterFieldMapping: '1'
      }
    ]
  },
  errors: {
    captionMapping: [
      {
        letterCaptionFields: true,
        generateLetterFieldMapping: false
      }
    ]
  },
  setValues: mockSetValues,
  setTouched: mockSetTouched,
  setErrors: mockSetErrors
} as unknown as LetterConfigGridProps;

beforeEach(() => {
  spyLetterConfig('getLetterConfiguration');

  jest.spyOn(usePagination, 'usePagination').mockImplementation(
    (list: Array<any>) =>
      ({
        total: 200,
        currentPage: 1,
        pageSize: [10, 25, 50],
        currentPageSize: 10,
        gridData: list,
        onPageChange: () => {},
        onPageSizeChange: () => {}
      } as any)
  );

  jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(false);
});

describe('should test letter config grid', () => {
  const letterConfigGridState: Partial<RootState> = {
    letterConfig: {
      letterConfigurationList: {
        loading: false,
        data: [
          {
            letterId: 'id',
            letterCode: '123',
            letterDescription: 'description',
            variableLetter: true,
            captionMapping: [
              {
                letterCaptionFields: '1',
                generateLetterFieldMapping: '1'
              }
            ]
          }
        ]
      }
    } as LetterState
  };

  const letterConfigGridStateWithEmptyCaptionMapping: Partial<RootState> = {
    letterConfig: {
      letterConfigurationList: {
        loading: false,
        data: [
          {
            letterId: 'id',
            letterCode: '123',
            letterDescription: 'description',
            variableLetter: false,
            captionMapping: []
          }
        ]
      },
      saveLetterModal: {
        open: false
      }
    } as LetterState
  };

  const letterConfigGridStateWithError: Partial<RootState> = {
    letterConfig: {
      letterConfigurationList: {
        loading: false,
        data: [{ letterId: 'id', letterDescription: 'description' }],
        error: 'error'
      }
    } as LetterState
  };

  const renderWrapper = (
    initialState: Partial<RootState>,
    initialParams: LetterConfigGridProps = params
  ) => {
    return renderMockStore(<LetterConfigGird {...initialParams} />, {
      initialState
    });
  };

  it('should render', () => {
    jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(false);
    const mockAddLetterAction = spyLetterConfig('toggleSaveLetterModal');

    renderWrapper(letterConfigGridState);

    const addLetterBtn = screen.getByText('txt_letter_config_add_title');

    addLetterBtn.click();

    expect(mockAddLetterAction).toBeCalled();
  });

  it('should handle with error', () => {
    renderWrapper(letterConfigGridStateWithError);

    expect(
      screen.queryByText('txt_letter_config_add_title')
    ).not.toBeInTheDocument();
  });

  it('should handle sort button', () => {
    const mockSortAction = spyLetterConfig('sortBy');

    renderWrapper(letterConfigGridStateWithEmptyCaptionMapping);

    const sortButton = screen.getByText('txt_letter_config_number');

    sortButton.click();

    expect(mockSortAction).toBeCalledWith({ id: 'letterId', order: 'asc' });
  });
});
