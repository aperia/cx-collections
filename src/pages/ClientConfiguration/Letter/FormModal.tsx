import { I18N_COMMON_TEXT, I18N_LETTER_CONFIG } from 'app/constants/i18n';
import {
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { Formik, FormikProps } from 'formik';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import ModalBodyWithErrorClientConfig from '../ApiErrorSection/ModalBodyWithErrorClientConfig';
import FormDetail from './FormDetail';
import { letterConfigValidationSchema } from './helpers';
import { CaptionMapping, FormModalProps, LetterConfigData } from './types';
import { letterConfigActions } from './_redux/reducers';
import {
  selectedModalOpen,
  selectedActionLetter,
  selectedSaveLoading
} from './_redux/selectors';

const FormModal: React.FC<FormModalProps> = ({ onSubmit }) => {
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const testId = 'clientConfig_letter_formModal';

  const letter = useSelector(selectedActionLetter);

  const modalOpened = useSelector(selectedModalOpen);

  const saveLoading = useSelector(selectedSaveLoading);

  const handleCancelModal = () => {
    dispatch(letterConfigActions.toggleSaveLetterModal(letter));
  };

  const captionMapping: CaptionMapping[] = [];

  if (letter.captionMapping) {
    for (const key in letter.captionMapping) {
      captionMapping.push({
        letterCaptionFields: key,
        generateLetterFieldMapping: letter.captionMapping[key]
      });
    }
  }

  const initialValues: LetterConfigData = {
    ...letter,
    captionMapping
  };

  return (
    <Modal md show={modalOpened} dataTestId={testId} loading={saveLoading}>
      <Formik
        initialValues={initialValues}
        initialErrors={{}}
        validateOnChange
        validateOnMount
        validationSchema={letterConfigValidationSchema}
        onSubmit={onSubmit}
      >
        {(props: FormikProps<LetterConfigData>) => {
          const { handleSubmit, isValid, values } = props;

          const checkValuesChanged =
            JSON.stringify(values) !== JSON.stringify(initialValues);

          return (
            <>
              <ModalHeader
                border
                closeButton
                onHide={handleCancelModal}
                dataTestId={`${testId}_header`}
              >
                <ModalTitle dataTestId={`${testId}_title`}>
                  {t(
                    letter.letterId
                      ? I18N_LETTER_CONFIG.MODAL_EDIT_TITLE
                      : I18N_LETTER_CONFIG.MODAL_ADD_TITLE
                  )}
                </ModalTitle>
              </ModalHeader>
              <ModalBodyWithErrorClientConfig
                apiErrorClassName="mb-24"
                dataTestId={`${testId}_body`}
              >
                <FormDetail {...props} />
              </ModalBodyWithErrorClientConfig>
              <ModalFooter
                dataTestId={`${testId}_footer`}
                cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
                onCancel={handleCancelModal}
                okButtonText={t(
                  letter.letterId
                    ? I18N_COMMON_TEXT.SAVE
                    : I18N_COMMON_TEXT.SUBMIT
                )}
                onOk={handleSubmit}
                disabledOk={!isValid || saveLoading || !checkValuesChanged}
              />
            </>
          );
        }}
      </Formik>
    </Modal>
  );
};

export default FormModal;
