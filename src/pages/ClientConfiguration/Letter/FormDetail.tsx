import React, { ChangeEvent, useCallback, useEffect, useState } from 'react';

// constants
import { FORM_FIELD, INITIAL_LETTER_VARIABLE } from './constants';

// components
import {
  Button,
  CheckBox,
  InlineMessage,
  TextBox
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import isUndefined from 'lodash.isundefined';
import isEmpty from 'lodash.isempty';
import isArray from 'lodash.isarray';
import { FormikProps } from 'formik';
import { LetterConfigData } from './types';
import LetterVariable from './LetterVariable';
import { useDispatch, useSelector } from 'react-redux';
import { letterConfigActions } from './_redux/reducers';
import {
  selectedActionLetter,
  selectedSaveLetterError
} from './_redux/selectors';
import { I18N_LETTER_CONFIG } from 'app/constants/i18n';
import classNames from 'classnames';
import { stringValidate } from 'app/helpers';

const FormDetail: React.FC<FormikProps<LetterConfigData>> = props => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const {
    values,
    setValues,
    setErrors,
    setTouched,
    handleChange,
    handleBlur,
    validateForm,
    errors,
    touched
  } = props;
  const [letterVariablesChecked, setLetterVariablesChecked] =
    useState<boolean>(false);
  const letter = useSelector(selectedActionLetter);
  const isEdit = !isEmpty(letter.letterId);
  const saveError = useSelector(selectedSaveLetterError);
  const testId = 'clientConfig_letter_formDetail';

  useEffect(() => {
    dispatch(letterConfigActions.getLetterVariableRefData());
  }, [dispatch]);

  useEffect(() => {
    if (isArray(values.captionMapping)) {
      setLetterVariablesChecked(values.captionMapping?.length > 0);
    }
    validateForm(values);
  }, [validateForm, values, values.captionMapping]);

  const toggleLetterVariablesVisible = useCallback(() => {
    setValues({
      ...values,
      captionMapping: letterVariablesChecked ? [] : [INITIAL_LETTER_VARIABLE],
      variableLetter: !letterVariablesChecked
    });
    if (!letterVariablesChecked) {
      setErrors({
        ...errors,
        captionMapping: undefined
      });
      setTouched({
        ...touched,
        captionMapping: undefined
      });
    }
    setLetterVariablesChecked(!letterVariablesChecked);
  }, [
    errors,
    letterVariablesChecked,
    setErrors,
    setTouched,
    setValues,
    touched,
    values
  ]);

  const handleAddVariable = () => {
    setValues({
      ...values,
      captionMapping: values.captionMapping!.concat(INITIAL_LETTER_VARIABLE)
    });
  };

  const handleChangeAlphaNumericInput = (e: ChangeEvent<HTMLInputElement>) => {
    if (!stringValidate(e.target.value).isAlphanumeric()) return;
    handleChange(e);
  };

  const handleChangeDescriptionAlphaNumericInput = (
    e: ChangeEvent<HTMLInputElement>
  ) => {
    if (!stringValidate(e.target.value.split(' ').join('')).isAlphanumeric())
      return;
    handleChange(e);
  };

  return (
    <div>
      {saveError && (
        <InlineMessage className="mb-24" variant="danger" dataTestId={`${testId}_errorMessage`}>
          {t(I18N_LETTER_CONFIG.LETTER_NUMBER_ALREADY_EXISTS)}
        </InlineMessage>
      )}
      <div className="row">
        <div className="col-12 col-md-6">
          <TextBox
            className={classNames({ 'dls-error': saveError })}
            required={!isEdit}
            name={FORM_FIELD.ID.name}
            label={t(FORM_FIELD.ID.label)}
            value={values.letterId}
            onChange={handleChangeAlphaNumericInput}
            onBlur={handleBlur}
            error={
              touched.letterId
                ? {
                  status: !isUndefined(errors?.letterId),
                  message: t(errors?.letterId)
                }
                : undefined
            }
            readOnly={isEdit}
            maxLength={FORM_FIELD.ID.maxLength}
            dataTestId={`${testId}_letterId`}
          />
        </div>
        <div className="col-12 col-md-6">
          <TextBox
            required
            name={FORM_FIELD.TYPE_CODE.name}
            label={t(FORM_FIELD.TYPE_CODE.label)}
            value={values.letterCode}
            onChange={handleChangeAlphaNumericInput}
            onBlur={handleBlur}
            maxLength={FORM_FIELD.TYPE_CODE.maxLength}
            error={
              touched.letterCode
                ? {
                  status: !isUndefined(errors?.letterCode),
                  message: t(errors?.letterCode)
                }
                : undefined
            }
            dataTestId={`${testId}_letterCode`}
          />
        </div>
      </div>
      <div className="mt-16">
        <TextBox
          required
          name={FORM_FIELD.DESCRIPTION.name}
          label={t(FORM_FIELD.DESCRIPTION.label)}
          value={values.letterDescription}
          onChange={handleChangeDescriptionAlphaNumericInput}
          onBlur={handleBlur}
          error={
            touched.letterDescription
              ? {
                status: !!errors?.letterDescription,
                message: t(errors?.letterDescription)
              }
              : undefined
          }
          maxLength={FORM_FIELD.DESCRIPTION.maxLength}
          dataTestId={`${testId}_letterDescription`}
        />
      </div>
      <div className="mt-16">
        <CheckBox dataTestId={`${testId}_letterVariables`}>
          <CheckBox.Input
            onChange={toggleLetterVariablesVisible}
            checked={letterVariablesChecked}
          />
          <CheckBox.Label>
            {t(I18N_LETTER_CONFIG.LETTER_VARIABLES)}
          </CheckBox.Label>
        </CheckBox>
      </div>
      {letterVariablesChecked && (
        <div className="mx-n24 mt-16">
          <div className="px-24 pb-16 bg-light-l20 border-top border-bottom">
            {values.captionMapping?.map((item, index) => (
              <LetterVariable
                key={index} {...props}
                index={index}
                dataTestId={`${index}_${testId}_letterVariable`}
              />
            ))}
            <Button
              size="sm"
              variant="outline-primary"
              className="mt-16 ml-n8"
              onClick={handleAddVariable}
              dataTestId={`${testId}_addVariableBtn`}
            >
              {t(I18N_LETTER_CONFIG.ADD_VARIABLE)}
            </Button>
          </div>
        </div>
      )}
    </div>
  );
};

export default FormDetail;
