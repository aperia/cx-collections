import React from 'react';

// components
import { Button } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useDispatch } from 'react-redux';
import { letterConfigActions } from './_redux/reducers';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';

// types
import { LetterConfigData } from './types';

// constant
import { I18N_ADJUSTMENT_TRANSACTION } from '../AdjustmentTransaction/constants';
import { I18N_COMMON_TEXT, I18N_LETTER_CONFIG } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface ActionsProps extends DLSId {
  record: LetterConfigData;
  status: boolean;
}

export const Actions: React.FC<ActionsProps> = ({ status, record, dataTestId }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const handleDeleteRequest = () => {
    dispatch(
      confirmModalActions.open({
        title: t(I18N_LETTER_CONFIG.DELETE_LETTER_CONFIRM_TITLE),
        body: (
          <>
            <p className="fs-14">
              {t(I18N_LETTER_CONFIG.DELETE_LETTER_CONFIFM_BODY)}
            </p>
            <p className="fs-14 mt-16">
              {`${t(I18N_LETTER_CONFIG.LETTER_CONFIG)}:`}{' '}
              <b>{`${record?.letterId} - ${record?.letterDescription}`}</b>
            </p>
          </>
        ),
        variant: 'danger',
        confirmText: t(I18N_COMMON_TEXT.DELETE),
        cancelText: t(I18N_COMMON_TEXT.CANCEL),
        confirmCallback: () => {
          dispatch(letterConfigActions.deleteLetterConfiguration(record));
        }
      })
    );
  };

  const handleEditRequest = () => {
    dispatch(letterConfigActions.toggleSaveLetterModal(record));
  };

  return (
    <>
      <Button
        disabled={status}
        onClick={handleDeleteRequest}
        size="sm"
        variant="outline-danger"
        dataTestId={genAmtId(dataTestId, 'deleteBtn', '')}
      >
        {t(I18N_ADJUSTMENT_TRANSACTION.DELETE)}
      </Button>
      <Button
        disabled={status}
        onClick={handleEditRequest}
        size="sm"
        variant="outline-primary"
        dataTestId={genAmtId(dataTestId, 'editBtn', '')}
      >
        {t(I18N_ADJUSTMENT_TRANSACTION.EDIT)}
      </Button>
    </>
  );
};
