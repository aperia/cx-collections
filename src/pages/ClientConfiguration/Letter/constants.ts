import { I18N_LETTER_CONFIG } from 'app/constants/i18n';
import { CaptionMapping, LetterConfigData } from './types';

export const FORM_FIELD = {
  ID: {
    name: 'letterId',
    label: I18N_LETTER_CONFIG.LETTER_CONFIG_NUMBER,
    maxLength: 4
  },
  TYPE_CODE: {
    name: 'letterCode',
    label: I18N_LETTER_CONFIG.LETTER_TYPE_CODE,
    maxLength: 3
  },
  DESCRIPTION: {
    name: 'letterDescription',
    label: I18N_LETTER_CONFIG.LETTER_CONFIG_DESCRIPTION,
    maxLength: 50
  },
  LETTER_VARIABLES: {
    name: 'variableLetter'
  },
  CAPTION_MAPPING: {
    name: 'captionMapping'
  },
  LETTER_CAPTION_FIELDS: {
    name: 'letterCaptionFields'
  },
  GENERATE_LETTER_FIELD_MAPPING: {
    name: 'generateLetterFieldMapping'
  }
};

export const INITIAL_FORM_DATA: LetterConfigData = {
  letterId: '',
  letterCode: '',
  letterDescription: '',
  variableLetter: false
};

export const INITIAL_LETTER_VARIABLE: CaptionMapping = {
  letterCaptionFields: '',
  generateLetterFieldMapping: ''
};
