import React from 'react';
import { screen } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';

import { mockActionCreator, renderMockStore } from 'app/test-utils';

import { I18N_ADJUSTMENT_TRANSACTION } from '../AdjustmentTransaction/constants';

import { Actions } from './Actions';
import { LetterConfigData } from './types';

import { letterConfigActions } from './_redux/reducers';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { ModalData } from 'pages/__commons/ConfirmModal/types';

const spyLetterConfig = mockActionCreator(letterConfigActions);
const mockConfirmModalActions = mockActionCreator(confirmModalActions);

const initialState: Partial<RootState> = {};

describe('should test letter list actions', () => {
  it('handleEditRequest', () => {
    const mockToggleSaveLetterModal = spyLetterConfig('toggleSaveLetterModal');

    const record: LetterConfigData = {};

    renderMockStore(<Actions record={record} />, {
      initialState
    });

    userEvent.click(screen.getByText(I18N_ADJUSTMENT_TRANSACTION.EDIT));

    expect(mockToggleSaveLetterModal).toBeCalledWith(record);
  });

  it('handleDeleteRequest ', () => {
    mockConfirmModalActions('open', ({ confirmCallback }: ModalData) => {
      confirmCallback!();
      return { type: 'type' };
    });

    const mockDeleteLetterConfiguration = spyLetterConfig(
      'deleteLetterConfiguration'
    );

    const record: LetterConfigData = {};

    renderMockStore(<Actions record={record} />, {
      initialState
    });

    userEvent.click(screen.getByText(I18N_ADJUSTMENT_TRANSACTION.DELETE));

    expect(mockDeleteLetterConfiguration).toBeCalledWith(record);
  });
});
