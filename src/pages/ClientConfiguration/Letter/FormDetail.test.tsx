import React from 'react';
import {
  fireEvent,
  queryByTestId,
  screen
} from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { renderMockStore } from 'app/test-utils';
import { INITIAL_FORM_DATA } from './constants';
import FormDetail from './FormDetail';
import { LetterConfigData } from './types';
import { FormikProps } from 'formik';
import { queryBy } from 'app/_libraries/_dls/test-utils';

HTMLCanvasElement.prototype.getContext = jest.fn();

const actionLetter: LetterConfigData = {
  letterId: '1',
  letterCode: '1',
  letterGroup: '1',
  letterDescription: '1',
  variableLetter: true,
  captionMapping: [
    {
      letterCaptionFields: '1',
      generateLetterFieldMapping: '1'
    }
  ]
};

const mockSetValues = jest.fn();
const mockSetTouched = jest.fn();
const mockSetErrors = jest.fn();
const mockHandleChange = jest.fn();

const params = {
  index: 0,
  values: INITIAL_FORM_DATA,
  touched: {
    captionMapping: [
      {
        letterCaptionFields: '1',
        generateLetterFieldMapping: '1'
      }
    ]
  },
  errors: {
    captionMapping: [
      {
        letterCaptionFields: true,
        generateLetterFieldMapping: false
      }
    ]
  },
  setValues: mockSetValues,
  setTouched: mockSetTouched,
  setErrors: mockSetErrors,
  handleChange: mockHandleChange,
  validateForm: () => {}
} as unknown as FormikProps<LetterConfigData>;

const paramsCheckedVariable = {
  index: 0,
  values: actionLetter,
  touched: {
    letterId: true,
    letterCode: true,
    letterDescription: true,
    captionMapping: [
      {
        letterCaptionFields: true,
        generateLetterFieldMapping: true
      }
    ]
  },
  errors: {
    captionMapping: [
      {
        letterCaptionFields: true,
        generateLetterFieldMapping: false
      }
    ]
  },
  setValues: mockSetValues,
  setTouched: mockSetTouched,
  setErrors: mockSetErrors,
  handleChange: mockHandleChange,
  validateForm: () => {}
} as unknown as FormikProps<LetterConfigData>;

describe('should test form detail', () => {
  const letterConfigState: Partial<RootState> = {
    letterConfig: {
      letterConfigurationList: {
        data: []
      },
      saveLetterModal: {
        open: true,
        letter: INITIAL_FORM_DATA,
        letterVariableRefData: {
          letterCaptionFieldsData: [],
          generateLetterFieldMappingData: []
        },
        error: 'error'
      }
    }
  };

  const renderWrapper = (
    initialState: Partial<RootState>,
    initialParams: FormikProps<LetterConfigData> = params
  ) => {
    return renderMockStore(<FormDetail {...initialParams} />, { initialState });
  };

  it('uncheck letter variable', () => {
    renderWrapper(letterConfigState);

    const checkboxLetterVariable = screen.getByText('txt_letter_variables');
    userEvent.click(checkboxLetterVariable);

    expect(mockSetValues).toBeCalled();
    expect(mockSetErrors).toBeCalled();
    expect(mockSetTouched).toBeCalled();
  });

  it('check letter variable', () => {
    renderWrapper(letterConfigState, paramsCheckedVariable);

    const checkboxLetterVariable = screen.getByText('txt_letter_variables');
    userEvent.click(checkboxLetterVariable);

    expect(mockSetValues).toBeCalled();
    expect(mockSetErrors).not.toBeCalled();
    expect(mockSetTouched).not.toBeCalled();
  });

  it('add variable', () => {
    renderWrapper(letterConfigState, paramsCheckedVariable);

    const addLetterVariableBtn = screen.getByText('txt_add_variable');
    addLetterVariableBtn.click();

    expect(mockSetValues).toBeCalled();
  });

  it('change text box > valid value', () => {
    const { baseElement } = renderWrapper(letterConfigState);

    const letterIdInput = queryBy(baseElement as HTMLElement, 'maxlength', '4');

    fireEvent.change(letterIdInput!, {
      target: {
        value: '123'
      }
    });

    expect(mockHandleChange).toBeCalled();
  });

  it('change text box > invalid value', () => {
    const { baseElement } = renderWrapper(letterConfigState);

    const letterIdInput = queryBy(baseElement as HTMLElement, 'maxlength', '4');

    fireEvent.change(letterIdInput!, {
      target: {
        value: '!@#'
      }
    });

    expect(mockHandleChange).not.toBeCalled();
  });

  it('handleChangeAlphaNumericInput 1', () => {
    const { baseElement } = renderWrapper(letterConfigState);

    const descriptionInput = queryByTestId(
      baseElement,
      'clientConfig_letter_formDetail_letterDescription_dls-text-box_input'
    );

    fireEvent.change(descriptionInput!, {
      target: {
        value: '!@#'
      }
    });

    expect(mockHandleChange).not.toBeCalled();
  });

  it('handleChangeAlphaNumericInput 2', () => {
    const { baseElement } = renderWrapper(letterConfigState);

    const descriptionInput = queryByTestId(
      baseElement,
      'clientConfig_letter_formDetail_letterDescription_dls-text-box_input'
    );

    fireEvent.change(descriptionInput!, {
      target: {
        value: '123a'
      }
    });

    expect(mockHandleChange).toBeCalled();
  });
});
