import React from 'react';
import {
  Button,
  ComboBox,
  Icon,
  Tooltip
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_COMMON_TEXT, I18N_LETTER_CONFIG } from 'app/constants/i18n';
import { FORM_FIELD } from './constants';
import { LetterVariableProps } from './types';

// helpers
import isArray from 'lodash.isarray';
import isUndefined from 'lodash.isundefined';

// redux
import { useSelector } from 'react-redux';
import {
  selectedGenerateLetterFieldMappingRefData,
  selectedLetterCaptionFieldsRefData
} from './_redux/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';

const LetterVariable: React.FC<LetterVariableProps> = ({
  values,
  setValues,
  setErrors,
  setTouched,
  handleChange,
  handleBlur,
  index,
  errors,
  touched,
  dataTestId
}) => {
  const { t } = useTranslation();
  const letterCaptionFieldsRefData = useSelector(
    selectedLetterCaptionFieldsRefData
  );
  const generateLetterFieldMappingRefData = useSelector(
    selectedGenerateLetterFieldMappingRefData
  );

  const handleRemoveLetterVariable = () => {
    const filterData = values.captionMapping?.filter(
      (item, pos) => pos !== index
    );
    setValues({
      ...values,
      variableLetter: !isUndefined(filterData) && filterData?.length > 0,
      captionMapping: filterData
    });
    setTouched({
      ...touched,
      captionMapping: (touched.captionMapping as any)?.filter(
        (item: any, pos: number) => pos !== index
      )
    });
    setErrors({
      ...errors,
      captionMapping: (errors.captionMapping as any)?.filter(
        (item: any, pos: number) => pos !== index
      )
    });
  };

  const handleLetterVariableError = (key: string) => {
    if (!isArray(touched.captionMapping) || !isArray(errors.captionMapping)) {
      return undefined;
    }
    const error = errors.captionMapping[index]?.[key];
    const errorTouched = touched.captionMapping[index]?.[key];
    if (errorTouched && error) {
      return {
        status: true,
        message: t(error)
      };
    }
    return {
      status: false,
      message: ''
    };
  };

  return (
    <div className="d-flex w-100 align-items-center mt-16">
      <div className="row w-100">
        <div className="col-6">
          <ComboBox
            required
            id={`${FORM_FIELD.CAPTION_MAPPING.name}[${index}].${FORM_FIELD.LETTER_CAPTION_FIELDS.name}`}
            name={`${FORM_FIELD.CAPTION_MAPPING.name}[${index}].${FORM_FIELD.LETTER_CAPTION_FIELDS.name}`}
            label={t(I18N_LETTER_CONFIG.LETTER_CAPTION_FIELDS)}
            value={values.captionMapping?.[index].letterCaptionFields}
            onChange={handleChange}
            onBlur={handleBlur}
            textField="description"
            error={handleLetterVariableError(
              FORM_FIELD.LETTER_CAPTION_FIELDS.name
            )}
            dataTestId={genAmtId(dataTestId, 'letterCaptionFields', '')}
            noResult={t('txt_no_results_found')}
            checkAllLabel={t('txt_all')}
          >
            {letterCaptionFieldsRefData.map(item => (
              <ComboBox.Item
                key={item.id}
                label={item.description}
                value={item.value}
              />
            ))}
          </ComboBox>
        </div>
        <div className="col-6">
          <ComboBox
            required
            id={`${FORM_FIELD.CAPTION_MAPPING.name}[${index}].${FORM_FIELD.GENERATE_LETTER_FIELD_MAPPING.name}`}
            name={`${FORM_FIELD.CAPTION_MAPPING.name}[${index}].${FORM_FIELD.GENERATE_LETTER_FIELD_MAPPING.name}`}
            label={t(I18N_LETTER_CONFIG.GENERATE_LETTER_FIELD_MAPPING)}
            value={values.captionMapping?.[index].generateLetterFieldMapping}
            onChange={handleChange}
            onBlur={handleBlur}
            textField="description"
            error={handleLetterVariableError(
              FORM_FIELD.GENERATE_LETTER_FIELD_MAPPING.name
            )}
            dataTestId={genAmtId(dataTestId, 'generateLetterFieldMapping', '')}
            noResult={t('txt_no_results_found')}
            checkAllLabel={t('txt_all')}
          >
            {generateLetterFieldMappingRefData.map(item => (
              <ComboBox.Item
                key={item.id}
                label={item.description}
                value={item.value}
              />
            ))}
          </ComboBox>
        </div>
      </div>
      <div className="ml-14">
        <Tooltip
          triggerClassName="d-flex mr-n2"
          variant="primary"
          element={t(I18N_COMMON_TEXT.REMOVE)}
        >
          <Button
            size="sm"
            variant="icon-danger"
            className="align-self-end"
            onClick={handleRemoveLetterVariable}
            dataTestId={genAmtId(dataTestId, 'removeBtn', '')}
          >
            <Icon name="close" />
          </Button>
        </Tooltip>
      </div>
    </div>
  );
};

export default LetterVariable;
