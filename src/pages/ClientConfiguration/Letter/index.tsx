import React, { useMemo } from 'react';

// components
import { SimpleBar } from 'app/_libraries/_dls/components';
import LetterGrid from './LetterConfigGird';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import { selectedSaveLoading } from './_redux/selectors';

// constants
import FormModal from './FormModal';
import { LetterConfigData } from './types';
import { letterConfigActions } from './_redux/reducers';
import { I18N_LETTER_CONFIG } from 'app/constants/i18n';
import ApiErrorForHeaderSection from '../ApiErrorSection/ApiErrorForHeaderSection';
import { useListenChangingTabEvent } from '../hooks/useListenChangingTabEvent';
import { useListenClosingModal } from '../hooks/useListenClosingModal';
import {
  dispatchCloseModalEvent,
  dispatchControlTabChangeEvent
} from '../helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';

const AdjustmentTransaction: React.FC = () => {
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const testId = 'clientConfig_letter';

  const saveLoading = useSelector(selectedSaveLoading);

  let lockSubmit = useMemo(() => saveLoading, [saveLoading]);

  const handleSubmitForm = (values: LetterConfigData) => {
    if (!lockSubmit) {
      dispatch(letterConfigActions.saveLetterConfiguration(values));
      lockSubmit = true;
    }
  };

  useListenChangingTabEvent(() => {
    batch(() => {
      dispatch(letterConfigActions.removeStore());
      dispatchControlTabChangeEvent();
    });
  });

  useListenClosingModal(() => {
    batch(() => {
      dispatch(letterConfigActions.removeStore());
      dispatchCloseModalEvent();
    });
  });

  return (
    <div className={'position-relative h-100'}>
      <SimpleBar>
        <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
          <ApiErrorForHeaderSection
            className="mb-24"
            dataTestId={`${testId}_apiError`}
          />
          <h5 data-testid={genAmtId(testId, 'title', '')}>
            {t(I18N_LETTER_CONFIG.LETTER_CONFIG_TITLE)}
          </h5>
          <LetterGrid />
        </div>
        <FormModal onSubmit={handleSubmitForm} />
      </SimpleBar>
    </div>
  );
};

export default AdjustmentTransaction;
