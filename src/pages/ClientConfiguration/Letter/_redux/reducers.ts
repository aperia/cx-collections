import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { LetterState } from '../types';
import {
  getLetterConfiguration,
  getLetterConfigurationBuilder
} from './getLetterList';
import { deleteLetterConfiguration } from './deleteLetterConfig';
import {
  saveLetterConfiguration,
  saveLetterConfigurationBuilder
} from './saveLetterConfig';
import {
  getLetterVariableRefData,
  getLetterVariableRefDataBuilder
} from './getLetterVariableRefData';
import orderBy from 'lodash.orderby';
import { SortType } from 'app/_libraries/_dls/components';
import { INITIAL_FORM_DATA } from '../constants';
import { ClientInfoLetterTemplate } from 'pages/ClientConfiguration/types';

const initialState: LetterState = {
  letterConfigurationList: { loading: false, data: [] },
  saveLetterModal: {
    open: false,
    letter: {
      letterId: '',
      letterCode: '',
      letterDescription: '',
      variableLetter: false
    }
  }
};

const { actions, reducer } = createSlice({
  name: 'letterConfiguration',
  initialState,
  reducers: {
    toggleSaveLetterModal: (
      draftState,
      action: PayloadAction<ClientInfoLetterTemplate | undefined>
    ) => {
      draftState.saveLetterModal = {
        open: !draftState.saveLetterModal.open,
        letter: action.payload || INITIAL_FORM_DATA,
        loading: false
      };
    },
    sortBy: (draftState, action: PayloadAction<SortType>) => {
      const sortType = action.payload;
      draftState.letterConfigurationList.data = orderBy(
        draftState.letterConfigurationList.data,
        [
          (item: any) => item[sortType.id]?.toString()?.toLowerCase(),
          item => item.letterId?.toLowerCase()
        ],
        [sortType.order || false, 'asc']
      );
    },
    removeStore: () => initialState
  },
  extraReducers: builder => {
    getLetterConfigurationBuilder(builder);
    saveLetterConfigurationBuilder(builder);
    getLetterVariableRefDataBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getLetterConfiguration,
  deleteLetterConfiguration,
  saveLetterConfiguration,
  getLetterVariableRefData
};

export { allActions as letterConfigActions, reducer };
