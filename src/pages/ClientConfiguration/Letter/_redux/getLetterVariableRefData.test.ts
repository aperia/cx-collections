import { responseDefault } from 'app/test-utils';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { letterConfigService } from '../letterService';
import { getLetterVariableRefData } from './getLetterVariableRefData';

describe('should test get letter variable ref data', () => {
  let store: Store<RootState>;
  let state: RootState;

  let spyGetLetterCaptionFields: jest.SpyInstance;
  let spyGenerateLetterFieldMapping: jest.SpyInstance;

  beforeEach(() => {
    store = createStore(rootReducer, {
      letterConfig: {
        saveLetterModal: {
          open: true
        }
      }
    });
    state = store.getState();
  });

  afterEach(() => {
    spyGetLetterCaptionFields?.mockReset();
    spyGetLetterCaptionFields?.mockRestore();
    spyGenerateLetterFieldMapping?.mockReset();
    spyGenerateLetterFieldMapping?.mockRestore();
  });

  it('should return data', async () => {
    spyGetLetterCaptionFields = jest
      .spyOn(letterConfigService, 'getLetterCaptionFields')
      .mockResolvedValue({
        ...responseDefault,
        data: []
      });

    spyGenerateLetterFieldMapping = jest
      .spyOn(letterConfigService, 'getGenerateLetterFieldMapping')
      .mockResolvedValue({
        ...responseDefault,
        data: []
      });

    const response = await getLetterVariableRefData()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      letterCaptionFieldsData: [],
      generateLetterFieldMappingData: []
    });
  });

  it('should fulfilled', () => {
    const fulfilled = getLetterVariableRefData.fulfilled(
      {
        letterCaptionFieldsData: [],
        generateLetterFieldMappingData: []
      },
      getLetterVariableRefData.fulfilled.type,
      undefined
    );

    const actual = rootReducer(state, fulfilled);

    expect(actual.letterConfig.saveLetterModal.letterVariableRefData).toEqual({
      letterCaptionFieldsData: [],
      generateLetterFieldMappingData: []
    });
  });

  it('should rejected', () => {
    const rejected = getLetterVariableRefData.rejected(
      {
        name: 'error',
        message: 'error'
      },
      getLetterVariableRefData.rejected.type,
      undefined
    );

    const actual = rootReducer(state, rejected);

    expect(actual.letterConfig.saveLetterModal.letterVariableRefData).toEqual({
      letterCaptionFieldsData: [],
      generateLetterFieldMappingData: []
    });
  });
});
