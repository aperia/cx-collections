import { letterConfigActions } from './reducers';
import { deleteLetterConfiguration } from './deleteLetterConfig';

import { createStore, Store } from '@reduxjs/toolkit';

import { letterConfigService } from '../letterService';

// redux
import { rootReducer } from 'storeConfig';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault
} from 'app/test-utils';
import { I18N_LETTER_CONFIG } from 'app/constants/i18n';

let spy: jest.SpyInstance;
let store: Store<RootState>;
let state: RootState;

beforeEach(() => {
  store = createStore(rootReducer, {
    letterConfig: {
      letterConfigurationList: { data: [] },
      saveLetterModal: { open: false }
    }
  });
  state = store.getState();
});

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

const spyLetterConfig = mockActionCreator(letterConfigActions);
const spyToast = mockActionCreator(actionsToast);

const dataRequest = {
  letterId: '1',
  letterCode: 'AAA',
  letterDescription: 'aaa',
  variableLetter: false
};

describe('Test deleteLetterConfigurationApi', () => {
  it('Should return data', async () => {
    const newStore: RootState = {
      letterConfig: {
        letterConfigurationList: {
          data: [
            {
              letterId: '1',
              variableLetter: false,
              letterCode: 'A',
              letterDescription: 'a'
            }
          ]
        },
        saveLetterModal: {
          open: false,
          letter: {
            letterId: '1',
            variableLetter: false,
            letterCode: 'A',
            letterDescription: 'a'
          }
        }
      }
    };
    jest
      .spyOn(letterConfigService, 'deleteLetterConfiguration')
      .mockResolvedValue({ ...responseDefault, data: [] });

    const response = await deleteLetterConfiguration({ ...dataRequest })(
      byPassFulfilled(deleteLetterConfiguration.fulfilled.type, {}),
      () => newStore,
      {}
    );

    expect(response.type).toEqual(deleteLetterConfiguration.fulfilled.type);
  });
});

describe('Test deleteLetterConfiguration', () => {
  it('isFulfilled', async () => {
    const mockAddToast = spyToast('addToast');
    const mockGetLetterConfiguration = spyLetterConfig(
      'getLetterConfiguration'
    );

    await deleteLetterConfiguration({ ...dataRequest })(
      byPassFulfilled(deleteLetterConfiguration.fulfilled.type),
      store.getState,
      {}
    );

    expect(mockGetLetterConfiguration).toBeCalled();
    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_LETTER_CONFIG.DELETE_LETTER_CONFIG_SUCCESS
    });
  });

  it('isRejected', async () => {
    const mockAddToast = spyToast('addToast');

    await deleteLetterConfiguration({ ...dataRequest })(
      byPassRejected(deleteLetterConfiguration.rejected.type),
      store.getState,
      {}
    );

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_LETTER_CONFIG.DELETE_LETTER_CONFIG_FAIL
    });
  });

  it('handle fulfilled', () => {
    const fulfilled = deleteLetterConfiguration.fulfilled(
      [],
      deleteLetterConfiguration.fulfilled.type,
      dataRequest
    );

    const actual = rootReducer(state, fulfilled);

    expect(actual.letterConfig.letterConfigurationList.data).toEqual([]);
  });
});
