import { createSelector } from '@reduxjs/toolkit';

const getLetterConfigurationState = (state: RootState) => state.letterConfig;

const getLetterConfigurationList = (state: RootState) =>
  state.letterConfig.letterConfigurationList;

const getLetterConfigurationModal = (state: RootState) =>
  state.letterConfig.saveLetterModal;

const getLetterVariableRefDataState = (state: RootState) =>
  state.letterConfig.saveLetterModal.letterVariableRefData;

export const selectLetterConfigurationLoading = createSelector(
  getLetterConfigurationList,
  data => data.loading
);

export const selectLetterConfigurationData = createSelector(
  getLetterConfigurationList,
  data => data.data
);

export const selectedLetterConfigurationListError = createSelector(
  getLetterConfigurationState,
  data => data.letterConfigurationList.error
);

export const selectedModalOpen = createSelector(
  getLetterConfigurationModal,
  data => data.open || false
);

export const selectedActionLetter = createSelector(
  getLetterConfigurationModal,
  data => data.letter
);

export const selectedSaveLoading = createSelector(
  getLetterConfigurationModal,
  data => data.loading || false
);

export const selectedLetterCaptionFieldsRefData = createSelector(
  getLetterVariableRefDataState,
  data => data?.letterCaptionFieldsData || []
);

export const selectedGenerateLetterFieldMappingRefData = createSelector(
  getLetterVariableRefDataState,
  data => data?.generateLetterFieldMappingData || []
);

export const selectedSaveLetterError = createSelector(
  getLetterConfigurationModal,
  data => data.error
);
