import './reducers';

import { getLetterConfiguration } from './getLetterList';

import { createStore, Store } from '@reduxjs/toolkit';

import { letterConfigService } from '../letterService';

//redux
import { rootReducer } from 'storeConfig';

import {
  byPassFulfilled,
  byPassRejected,
  responseDefault
} from 'app/test-utils';

let store: Store<RootState>;
let state: RootState;

beforeEach(() => {
  store = createStore(rootReducer, {
    letterConfig: {},
    mapping: {
      data: {}
    }
  });
  state = store.getState();
});

describe('should test get letter list', () => {
  it('should return data', async () => {
    jest
      .spyOn(letterConfigService, 'getLetterConfiguration')
      .mockResolvedValue({ ...responseDefault, data: [] });

    const response = await getLetterConfiguration()(
      byPassFulfilled(getLetterConfiguration.fulfilled.type, []),
      store.getState,
      {}
    );

    expect(response.type).toEqual(getLetterConfiguration.fulfilled.type);
  });

  it('should return data []', async () => {
    jest
      .spyOn(letterConfigService, 'getLetterConfiguration')
      .mockResolvedValue({ ...responseDefault, data: [] });

    const newState: RootState = {
      mapping: {
        loading: false,
        data: {
          clientConfigurationLetterList: {
            letterId: 'letterId'
          }
        }
      }
    };

    const response = await getLetterConfiguration()(
      byPassFulfilled(getLetterConfiguration.fulfilled.type, {
        clientInfoLetterTemplate: [
          {
            letterId: '12'
          }
        ]
      }),
      () => newState,
      {}
    );

    expect(response.type).toEqual(getLetterConfiguration.fulfilled.type);
  });

  it('rejected', async () => {
    jest
      .spyOn(letterConfigService, 'getLetterConfiguration')
      .mockResolvedValue({ ...responseDefault, data: [] });

    const response = await getLetterConfiguration()(
      byPassFulfilled(getLetterConfiguration.fulfilled.type, []),
      store.getState,
      {}
    );

    expect(response.type).toEqual(getLetterConfiguration.fulfilled.type);
  });

  it('should return data []', async () => {
    jest
      .spyOn(letterConfigService, 'getLetterConfiguration')
      .mockResolvedValue({ ...responseDefault, data: [] });

    const newState: RootState = {
      mapping: {
        loading: false,
        data: {
          clientConfigurationLetterList: {
            letterId: 'letterId'
          }
        }
      }
    };

    const response = await getLetterConfiguration()(
      byPassFulfilled(getLetterConfiguration.fulfilled.type, {
        clientInfoLetterTemplate: [
          {
            letterId: '12'
          }
        ]
      }),
      () => newState,
      {}
    );

    expect(response.type).toEqual(getLetterConfiguration.fulfilled.type);
  });

  it('rejected', async () => {
    jest
      .spyOn(letterConfigService, 'getLetterConfiguration')
      .mockResolvedValue({ ...responseDefault, data: [] });

    const response = await getLetterConfiguration()(
      byPassRejected(getLetterConfiguration.fulfilled.type, []),
      store.getState,
      {}
    );

    expect(response.type).toEqual(getLetterConfiguration.rejected.type);
  });

  it('pending', () => {
    const pending = getLetterConfiguration.pending(
      getLetterConfiguration.pending.type,
      undefined
    );

    const actual = rootReducer(state, pending);

    expect(actual.letterConfig.letterConfigurationList.loading).toEqual(true);
  });

  it('fulfilled', () => {
    const fulfilled = getLetterConfiguration.fulfilled(
      [],
      getLetterConfiguration.fulfilled.type,
      undefined
    );

    const actual = rootReducer(state, fulfilled);

    expect(actual.letterConfig.letterConfigurationList.data).toEqual([]);
    expect(actual.letterConfig.letterConfigurationList.loading).toEqual(false);
  });

  it('rejected', () => {
    const rejected = getLetterConfiguration.rejected(
      {
        name: 'error',
        message: 'error'
      },
      getLetterConfiguration.rejected.type,
      undefined
    );

    const actual = rootReducer(state, rejected);

    expect(actual.letterConfig.letterConfigurationList.error).toEqual('error');
  });
});
