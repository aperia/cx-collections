import { selectorWrapper } from 'app/test-utils';
import { INITIAL_FORM_DATA } from '../constants';
import * as selectors from './selectors';

const store: Partial<RootState> = {
  letterConfig: {
    letterConfigurationList: {
      loading: false,
      data: []
    },
    saveLetterModal: {
      open: false,
      loading: false,
      letter: INITIAL_FORM_DATA,
      letterVariableRefData: {
        letterCaptionFieldsData: [],
        generateLetterFieldMappingData: []
      }
    }
  }
};

describe('should test letter list selector', () => {
  it('selectLetterConfigurationLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectLetterConfigurationLoading
    );

    expect(data).toEqual(store.letterConfig?.letterConfigurationList.loading);
    expect(emptyData).toEqual(false);
  });

  it('selectLetterConfigurationData', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectLetterConfigurationData
    );

    expect(data).toEqual(store.letterConfig?.letterConfigurationList.data);
    expect(emptyData).toEqual([]);
  });

  it('selectLetterConfigurationError', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectedLetterConfigurationListError
    );

    expect(data).toEqual(store.letterConfig?.letterConfigurationList.error);
    expect(emptyData).toBeUndefined();
  });

  it('selectedModalOpen', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectedModalOpen
    );

    expect(data).toEqual(store.letterConfig?.saveLetterModal.open);
    expect(emptyData).toEqual(false);
  });

  it('selectedActionLetter', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectedActionLetter
    );

    expect(data).toEqual(store.letterConfig?.saveLetterModal.letter);
    expect(emptyData).toEqual({
      letterCode: '',
      letterDescription: '',
      letterId: '',
      variableLetter: false
    });
  });

  it('selectedSaveLoading', () => {
    const { data } = selectorWrapper(store)(selectors.selectedSaveLoading);
   
    expect(data).toBeFalsy();
  });

  it('selectedLetterCaptionFieldsRefData', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectedLetterCaptionFieldsRefData
    );

    expect(data).toEqual(
      store.letterConfig?.saveLetterModal.letterVariableRefData
        ?.letterCaptionFieldsData
    );
    expect(emptyData).toEqual([]);
  });

  it('selectedGenerateLetterFieldMappingRefData', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectedGenerateLetterFieldMappingRefData
    );

    expect(data).toEqual(
      store.letterConfig?.saveLetterModal.letterVariableRefData
        ?.generateLetterFieldMappingData
    );
    expect(emptyData).toEqual([]);
  });

  it('selectedSaveLetterError', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectedSaveLetterError
    );

    expect(data).toEqual(store.letterConfig?.saveLetterModal.error);
    expect(emptyData).toBeUndefined();
  });
});
