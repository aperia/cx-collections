import { I18N_LETTER_CONFIG } from 'app/constants/i18n';
import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault
} from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { letterConfigService } from '../letterService';
import {
  saveLetterConfiguration
} from './saveLetterConfig';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { INITIAL_FORM_DATA } from '../constants';


const mockToastActions = mockActionCreator(actionsToast);
const mockClientConfig = mockActionCreator(clientConfigurationActions);

describe('should test save letter config', () => {
  let store: Store<RootState>;
  let state: RootState;
  let spy: jest.SpyInstance;

  const dataRequest = {
    letterId: '1',
    letterCode: 'AAA',
    letterDescription: 'aaa',
    variableLetter: false
  }

  beforeEach(() => {
    store = createStore(rootReducer, {
      letterConfig: {
        saveLetterModal: {
          open: true
        },
        letterConfigurationList: {
          data: [
            {}
          ]
        }
      }
    });
    state = store.getState();
  });

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('should saveLetterConfiguration return data', async () => {
    const action = mockClientConfig("updateClientInfo");
    const newState: Partial<RootState> = {
      letterConfig: {
        saveLetterModal: {
          letter: INITIAL_FORM_DATA,
          open: true
        },
        letterConfigurationList: {
          data: [{ ...dataRequest }]
        }
      }
    };
    await saveLetterConfiguration({ ...dataRequest })(
      byPassFulfilled(saveLetterConfiguration.fulfilled.type, []),
      () => newState as RootState,
      {}
    );

    expect(action).not.toBeCalled();
  });

  it('should saveLetterConfiguration return data', async () => {
    const action = mockClientConfig("updateClientInfo");
    const newState: Partial<RootState> = {
      letterConfig: {
        saveLetterModal: {
          letter: INITIAL_FORM_DATA,
          open: true
        },
        letterConfigurationList: {
          data: [{ ...dataRequest }]
        }
      }
    };
    await saveLetterConfiguration({
      ...dataRequest,
      captionMapping: [
        {
          letterCaptionFields: '',
          generateLetterFieldMapping: ''
        }
      ]
    })(
      byPassFulfilled(saveLetterConfiguration.fulfilled.type, []),
      () => newState as RootState,
      {}
    );

    expect(action).not.toBeCalled();
  });

  it('should saveLetterConfiguration return data', async () => {
    const action = mockClientConfig("updateClientInfo");

    await saveLetterConfiguration({...dataRequest})(
      byPassFulfilled(saveLetterConfiguration.fulfilled.type, []),
      store.getState,
      {}
    );

    expect(action).toBeCalled();
  });

  it('should saveLetterConfiguration return data', async () => {
    const newState: Partial<RootState> = {
      letterConfig: {
        saveLetterModal: {
          letter: { ...dataRequest },
          open: true
        },
        letterConfigurationList: {
          data: [{ ...dataRequest }]
        }
      }
    };
    spy = jest
      .spyOn(letterConfigService, 'saveLetterConfiguration')
      .mockResolvedValue({
        ...responseDefault,
        data: []
      });

    const response = await saveLetterConfiguration({ ...dataRequest })(
      byPassFulfilled(saveLetterConfiguration.fulfilled.type, []),
      () => newState as RootState,
      {}
    );

    expect(response.type).toEqual(saveLetterConfiguration.fulfilled.type);
  });

  it('should saveLetterConfiguration return data', async () => {
    spy = jest
      .spyOn(letterConfigService, 'saveLetterConfiguration')
      .mockRejectedValue({});

      const newState: Partial<RootState> = {
        letterConfig: {
          saveLetterModal: {
            letter: { ...dataRequest },
            open: true
          },
          letterConfigurationList: {
            data: [{ ...dataRequest }]
          }
        }
      };

    const addToastActions = mockToastActions('addToast');

    await saveLetterConfiguration({ ...dataRequest })(
      byPassRejected(saveLetterConfiguration.rejected.type, []),
      () => newState as RootState,
      {}
    );

    expect(addToastActions).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_LETTER_CONFIG.EDIT_LETTER_CONFIG_FAIL
    });
  });

  it('should saveLetterConfiguration return data', async () => {
    spy = jest
      .spyOn(letterConfigService, 'saveLetterConfiguration')
      .mockRejectedValue({});

    const addToastActions = mockToastActions('addToast');

    await saveLetterConfiguration({...dataRequest})(
      byPassRejected(saveLetterConfiguration.rejected.type, []),
      store.getState,
      {}
    );

    expect(addToastActions).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_LETTER_CONFIG.ADD_LETTER_CONFIG_FAIL
    });
  });

  it('pending', () => {
    const pending = saveLetterConfiguration.pending(
      saveLetterConfiguration.pending.type,
      { ...dataRequest }
    );
    const actual = rootReducer(state, pending);
    expect(actual.letterConfig.saveLetterModal.loading).toBeTruthy();
  });

  it('fulfilled', () => {
    const fulfilled = saveLetterConfiguration.fulfilled(
      {},
      saveLetterConfiguration.fulfilled.type,
      {...dataRequest}
    );

    const actual = rootReducer(state, fulfilled);

    expect(actual.letterConfig.saveLetterModal.open).toEqual(false);
  });

  it('rejected', () => {
    const rejected = saveLetterConfiguration.rejected(
      new Error(),
      saveLetterConfiguration.rejected.type,
      {...dataRequest}
    );

    const actual = rootReducer(state, rejected);

    expect(actual.letterConfig.saveLetterModal.error).toEqual(undefined);
  });


});

