import { letterConfigActions as actions, reducer } from './reducers';
import { LetterState } from '../types';
import { INITIAL_FORM_DATA } from '../constants';

const initialState: LetterState = {
  letterConfigurationList: {
    loading: true,
    data: [],
    error: 'error'
  },
  saveLetterModal: {
    open: false,
    letter: INITIAL_FORM_DATA
  }
};

describe('ClientConfiguration Letter reducer', () => {
  it('toggleSaveLetterModal', () => {
    const state = reducer(initialState, actions.toggleSaveLetterModal());

    expect(state.saveLetterModal.open).toEqual(true);
    expect(state.saveLetterModal.letter).toEqual(INITIAL_FORM_DATA);
  });

  it('sortBy', () => {
    const state = reducer(initialState, actions.sortBy({ id: 'letterId' }));

    expect(state.letterConfigurationList.data).toEqual([]);
  });

  it('sortBy with defined order', () => {
    const state = reducer(
      initialState,
      actions.sortBy({ id: 'letterId', order: 'desc' })
    );

    expect(state.letterConfigurationList.data).toEqual([]);
  });

  it('remove store', () => {
    const state = reducer(initialState, actions.removeStore());
    expect(state.letterConfigurationList.loading).toEqual(false);
  });
});
