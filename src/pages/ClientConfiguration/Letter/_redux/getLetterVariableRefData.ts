import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { letterConfigService } from '../letterService';
import { GetLetterVariableRefDataPayload, LetterState } from '../types';

export const getLetterVariableRefData = createAsyncThunk<
  GetLetterVariableRefDataPayload,
  undefined,
  ThunkAPIConfig
>('letterConfig/getLetterVariableRefData', async () => {
  const [responseLetterCaptionFields, responseGenerateLetterFieldMapping] =
    await Promise.all([
      letterConfigService.getLetterCaptionFields(),
      letterConfigService.getGenerateLetterFieldMapping()
    ]);

  return {
    letterCaptionFieldsData: responseLetterCaptionFields.data,
    generateLetterFieldMappingData: responseGenerateLetterFieldMapping.data
  };
});

export const getLetterVariableRefDataBuilder = (
  builder: ActionReducerMapBuilder<LetterState>
) => {
  builder
    .addCase(getLetterVariableRefData.fulfilled, (draftState, action) => {
      draftState.saveLetterModal.letterVariableRefData = action.payload;
    })
    .addCase(getLetterVariableRefData.rejected, (draftState, action) => {
      draftState.saveLetterModal.letterVariableRefData = {
        letterCaptionFieldsData: [],
        generateLetterFieldMappingData: []
      };
    });
};
