import { createAsyncThunk, isFulfilled, isRejected } from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import { LetterConfigData } from '../types';
import { letterConfigActions } from './reducers';
import { I18N_LETTER_CONFIG } from 'app/constants/i18n';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

export const deleteLetterConfiguration = createAsyncThunk<
  void,
  LetterConfigData,
  ThunkAPIConfig
>(
  'letterConfiguration/deleteLetterConfig',
  async (args, { dispatch, getState, rejectWithValue }) => {
    const letterList = getState().letterConfig.letterConfigurationList.data;

    const deleteItemIndex = letterList.findIndex(
      item => item.letterId === args.letterId
    );

    const response = await dispatch(
      clientConfigurationActions.updateClientInfo({
        clientInfoLetterTemplate: letterList.filter(
          (_, index) => index !== deleteItemIndex
        ),
        updateAction: 'delete'
      })
    );

    if (isRejected(response)) {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_LETTER_CONFIG.DELETE_LETTER_CONFIG_FAIL
        })
      );
    }

    if (isFulfilled(response)) {
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_LETTER_CONFIG.DELETE_LETTER_CONFIG_SUCCESS
          })
        );
        dispatch(letterConfigActions.getLetterConfiguration());
        dispatch(
          changeHistoryActions.saveChangedClientConfig({
            action: 'DELETE',
            changedCategory: 'letterList',
            changedItem: {
              letterId: letterList[deleteItemIndex].letterId
            },
            oldValue: letterList[deleteItemIndex]
          })
        );
      });
    }
  }
);
