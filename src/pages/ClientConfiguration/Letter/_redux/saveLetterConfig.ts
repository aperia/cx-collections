import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';
import { I18N_LETTER_CONFIG } from 'app/constants/i18n';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { LetterConfigData, LetterState } from '../types';
import { letterConfigActions } from './reducers';
import { batch } from 'react-redux';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import isArray from 'lodash.isarray';
import isEqual from 'lodash.isequal';
import { INITIAL_FORM_DATA } from '../constants';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

export const saveLetterConfiguration = createAsyncThunk<
  MagicKeyValue,
  LetterConfigData,
  ThunkAPIConfig
>(
  'letterConfiguration/saveLetterConfig',
  async (args, { dispatch, getState, rejectWithValue }) => {
    try {
      const { letterConfigurationList, saveLetterModal } =
        getState().letterConfig;

      const captionMapping: MagicKeyValue = {};
      if (isArray(args.captionMapping)) {
        for (const item of args.captionMapping) {
          captionMapping[item.letterCaptionFields] =
            item.generateLetterFieldMapping;
        }
      }
      const formatLetter = {
        ...args,
        captionMapping
      };

      const letterList = letterConfigurationList.data;
      const index = letterList.findIndex(
        e => e.letterId === formatLetter.letterId
      );

      const isEdit = index >= 0;

      if (isEqual(saveLetterModal.letter, INITIAL_FORM_DATA) && isEdit) {
        return rejectWithValue({
          errorMessage: I18N_LETTER_CONFIG.LETTER_NUMBER_ALREADY_EXISTS
        });
      }

      const response = await dispatch(
        clientConfigurationActions.updateClientInfo({
          clientInfoLetterTemplate: isEdit
            ? Object.assign([], letterList, { [index]: formatLetter })
            : [...letterList, formatLetter]
        })
      );

      if (isFulfilled(response)) {
        return batch(() => {
          dispatch(
            changeHistoryActions.saveChangedClientConfig({
              action: isEdit ? 'UPDATE' : 'ADD',
              changedCategory: 'letterList',
              changedItem: {
                letterId: isEdit
                  ? letterList[index].letterId
                  : formatLetter.letterId
              },
              oldValue: isEdit ? letterList[index] : {},
              newValue: formatLetter
            })
          );
          dispatch(
            actionsToast.addToast({
              show: true,
              type: 'success',
              message: isEdit
                ? I18N_LETTER_CONFIG.EDIT_LETTER_CONFIG_SUCCESS
                : I18N_LETTER_CONFIG.ADD_LETTER_CONFIG_SUCCESS
            })
          );
          dispatch(letterConfigActions.getLetterConfiguration());
        });
      }

      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: isEdit
            ? I18N_LETTER_CONFIG.EDIT_LETTER_CONFIG_FAIL
            : I18N_LETTER_CONFIG.ADD_LETTER_CONFIG_FAIL
        })
      );
      throw response;
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const saveLetterConfigurationBuilder = (
  builder: ActionReducerMapBuilder<LetterState>
) => {
  builder
    .addCase(saveLetterConfiguration.pending, (draftState, action) => {
      draftState.saveLetterModal.loading = true;
    })
    .addCase(saveLetterConfiguration.fulfilled, (draftState, action) => {
      draftState.saveLetterModal = {
        ...draftState.saveLetterModal,
        open: false
      };
    })
    .addCase(saveLetterConfiguration.rejected, (draftState, action) => {
      draftState.saveLetterModal = {
        ...draftState.saveLetterModal,
        error: action.payload?.errorMessage,
        loading: false
      };
    });
};
