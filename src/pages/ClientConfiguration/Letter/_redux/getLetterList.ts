import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import orderBy from 'lodash.orderby';
import { ClientInfoLetterTemplate } from 'pages/ClientConfiguration/types';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

import { LetterState } from '../types';

export const getLetterConfiguration = createAsyncThunk<
  ClientInfoLetterTemplate[],
  undefined,
  ThunkAPIConfig
>('getLetterConfiguration', async (args, thunkAPI) => {
  const { getState, dispatch } = thunkAPI;
  const response = await dispatch(
    clientConfigurationActions.getClientInfoData({
      dataType: 'clientInfoLetterTemplate'
    })
  );

  if (isFulfilled(response)) {
    return orderBy(
      mappingDataFromArray(
        response.payload.clientInfoLetterTemplate || [],
        getState().mapping.data.clientConfigurationLetterList || {}
      ),
      [item => item.letterId.toLowerCase()],
      'asc'
    );
  }

  return thunkAPI.rejectWithValue({
    errorMessage: response.error.message
  });
});

export const getLetterConfigurationBuilder = (
  builder: ActionReducerMapBuilder<LetterState>
) => {
  builder
    .addCase(getLetterConfiguration.pending, draftState => {
      draftState.letterConfigurationList = {
        ...draftState.letterConfigurationList,
        loading: true,
        data: []
      };
    })
    .addCase(getLetterConfiguration.fulfilled, (draftState, action) => {
      draftState.letterConfigurationList = {
        ...draftState.letterConfigurationList,
        loading: false,
        data: action.payload,
        error: undefined
      };
    })
    .addCase(getLetterConfiguration.rejected, (draftState, action) => {
      draftState.letterConfigurationList = {
        ...draftState.letterConfigurationList,
        loading: false,
        data: [],
        error: action.error.message
      };
    });
};
