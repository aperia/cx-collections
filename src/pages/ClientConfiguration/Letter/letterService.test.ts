// services
import { letterConfigService } from './letterService';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import { LetterConfigData } from './types';

describe('letterConfigService', () => {
  describe('getLetterConfiguration', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      letterConfigService.getLetterConfiguration();

      expect(mockService).toBeCalledWith('', {});
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      letterConfigService.getLetterConfiguration();

      expect(mockService).toBeCalledWith(
        apiUrl.letterConfig.getLetterConfiguration,
        {}
      );
    });
  });

  describe('deleteLetterConfiguration', () => {
    const params: LetterConfigData = { letterId: 'letter_1' };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      letterConfigService.deleteLetterConfiguration(params);

      expect(mockService).toBeCalledWith('', { data: params });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      letterConfigService.deleteLetterConfiguration(params);

      expect(mockService).toBeCalledWith(
        apiUrl.letterConfig.deleteLetterConfiguration,
        { data: params }
      );
    });
  });

  describe('saveLetterConfiguration', () => {
    const params = {} as LetterConfigData;

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      letterConfigService.saveLetterConfiguration(params);

      expect(mockService).toBeCalledWith('', {});
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      letterConfigService.saveLetterConfiguration(params);

      expect(mockService).toBeCalledWith(
        apiUrl.letterConfig.saveLetterConfiguration,
        {}
      );
    });
  });
});
