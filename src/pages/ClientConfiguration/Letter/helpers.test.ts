import * as helpers from './helpers';

describe('ClientConfiguration Letter helpers', () => {
  it('validation schema', () => {
    const validationSchema = helpers.letterConfigValidationSchema();

    expect(validationSchema.fields.letterId.required()).toBeTruthy();
    expect(validationSchema.fields.letterCode.required()).toBeTruthy();
    expect(validationSchema.fields.letterDescription.required()).toBeTruthy();
  });
});
