import React from 'react';
import axios from 'axios';

// Components
import QueueRoleMappingConfiguration from './QueueRoleMappingConfiguration';

// Utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

// Const
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { queueAndRoleMappingActions } from './_redux/reducers';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest
  .mock('app/_libraries/_dls/hooks/useTranslation', () => {
    return () => ({ t: (text: string) => text });
  })
  .mock('./Components/AddQueue', () => ({
    __esModule: true,
    default: ({ onAddQueue }: any) => (
      <button data-testid="addQueueMock" onClick={onAddQueue}>
        Click to add
      </button>
    )
  }))
  .mock('./QueueNameGrid', () => ({
    __esModule: true,
    default: () => <p>txt_queue_grid</p>
  }));

const spyQueueAndRoleMapping = mockActionCreator(queueAndRoleMappingActions);

const initialState: Partial<RootState> = {
  clientConfigQueueAndRoleMapping: {
    loading: false,
    loadingModal: false,
    loadingQueueName: false,
    queueAndRoleList: [
      {
        queueId: '160675456170',
        queueName: 'COLX_DEFAULT',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [
          { code: 'CXCOL1', text: 'CXCOL1' },
          { code: 'CXCOL2', text: 'CXCOL2' }
        ],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '001',
        queueName: 'Non Verified Bankrupt',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [{ code: 'CXSUP1', text: 'CXSUP1' }],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '002',
        queueName: 'Verified Bankrupt',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [
          { code: 'CXCOL2', text: 'CXCOL2' },
          { code: 'CXCOL3', text: 'CXCOL3' },
          { code: 'CXCOL4', text: 'CXCOL4' }
        ],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '003',
        queueName: 'Discharge Bankrupt',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [{ code: 'CXADMIN', text: 'CXADMIN' }],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '004',
        queueName: 'Dismissed BK',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [{ code: 'CXMGR1', text: 'CXMGR1' }],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '005',
        queueName: 'Cease and Desist',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [{ code: 'CXSUP1', text: 'CXSUP1' }],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '006',
        queueName: 'Customer Represented by Attorney',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [
          { code: 'CXCOL1', text: 'CXCOL1' },
          { code: 'CXCOL2', text: 'CXCOL2' }
        ],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '007',
        queueName: 'Pending Deceased',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [{ code: 'CXADMIN', text: 'CXADMIN' }],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '008',
        queueName: 'Temporary Reform',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [
          { code: 'CXCOL1', text: 'CXCOL1' },
          { code: 'CXSUP1', text: 'CXSUP1' },
          { code: 'CXMGR1', text: 'CXMGR1' }
        ],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '009',
        queueName: 'Customer Enrolled into Hardship',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [{ code: 'CXMGR1', text: 'CXMGR1' }],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '010',
        queueName: 'Non Speak Call Back',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [{ code: 'CXCOL5', text: 'CXCOL5' }],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '011',
        queueName: 'Queue Name 11',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [
          { code: 'CXCOL1', text: 'CXCOL1' },
          { code: 'CXCOL2', text: 'CXCOL2' }
        ],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '012',
        queueName: 'Queue Name 12',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [{ code: 'CXCOL1', text: 'CXCOL1' }],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '013',
        queueName: 'Queue Name 13',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [{ code: 'CXCOL2', text: 'CXCOL2' }],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '014',
        queueName: 'Queue Name 14',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [
          { code: 'CXCOL1', text: 'CXCOL1' },
          { code: 'CXCOL2', text: 'CXCOL2' },
          { code: 'CXCOL3', text: 'CXCOL3' },
          { code: 'CXCOL4', text: 'CXCOL4' },
          { code: 'CXCOL5', text: 'CXCOL5' },
          { code: 'CXMGR1', text: 'CXMGR1' },
          { code: 'CXMGR2', text: 'CXMGR2' },
          { code: 'CXMGR3', text: 'CXMGR3' },
          { code: 'CXMGR4', text: 'CXMGR4' },
          { code: 'CXMGR5', text: 'CXMGR5' }
        ],
        accessLevel: 'UPDATE'
      }
    ],
    queueAndRole: {},
    error: '',
    errorModal: '',
    errorQueueName: '',
    isOpenModal: false,
    isOpenModalDelete: true,
    sortBy: { id: 'queueName', order: undefined },
    queueNameList: [],
    userRoleList: [],
    selectedUserRole: undefined,
    loadingUserRoleList: false,
    textSearch: ''
  }
};

describe('Test QueueRoleMappingConfiguration', () => {
  it('should render QueueRoleMappingConfiguration with empty grid', async () => {
    // spyQueueAndRoleMapping('getUserRoleList');
    jest.spyOn(axios, 'get');

    renderMockStore(<QueueRoleMappingConfiguration />, {
      initialState
    });

    expect(
      await screen.findByText('txt_queue_and_role_mapping')
    ).toBeInTheDocument();
  });

  it('should render QueueRoleMappingConfiguration with grid', async () => {
    // spyQueueAndRoleMapping('getUserRoleList');
    jest.spyOn(axios, 'get');

    renderMockStore(<QueueRoleMappingConfiguration />, {
      initialState: {
        clientConfigQueueAndRoleMapping: {
          ...initialState.clientConfigQueueAndRoleMapping,
          selectedUserRole: {
            code: 'CXCOL1',
            text: 'CXCOL1'
          }
        } as any
      }
    });

    expect(await screen.findByText('txt_queue_grid')).toBeInTheDocument();
  });

  it('should render QueueRoleMappingConfiguration with noData', async () => {
    // spyQueueAndRoleMapping('getUserRoleList');
    jest.spyOn(axios, 'get');
    const openModalFormMock = spyQueueAndRoleMapping('openModalForm');

    renderMockStore(<QueueRoleMappingConfiguration />, {
      initialState: {
        clientConfigQueueAndRoleMapping: {
          ...initialState.clientConfigQueueAndRoleMapping,
          queueAndRoleList: [],
          selectedUserRole: {
            code: 'CXCOL1',
            text: 'CXCOL1'
          }
        } as any
      }
    });

    userEvent.click(await screen.findByTestId('addQueueMock'));

    expect(openModalFormMock).toBeCalled();
  });
});
