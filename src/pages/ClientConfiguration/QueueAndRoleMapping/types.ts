import { SortType } from 'app/_libraries/_dls/components';

export interface ICollectionEntitlement {
  cspas: {
    active: boolean;
    components: {
      component: string;
      entitlements: {
        queues: MagicKeyValue[];
      };
    }[];
    cspa: string;
    description: string;
  }[];
  role: string;
}
export interface IQueueRoleMappingCommon {
  clientNumber: string;
  system: string;
  prin: string;
  agent: string;
  app: string;
  org: string;
  privileges?: string[];
  user?: string;
  userPrivileges?: {
    cspa: string[];
    groups: string[];
  };
}

export interface QueueAndRoleMappingItem {
  queueId?: string;
  queueName?: string;
  queueGroupIdOwner?: string;
  userRoleName?: UserRoleItem[];
  userRoleMapping?: string;
  accessLevel?: string;
}

export interface UserRoleItem {
  value: string;
  description: string;
}

export interface AddQueueAndRoleMappingPayload {
  postData: QueueAndRoleMappingItem[];
}

export interface EditQueueAndRolePayload {
  postData: {
    prevRecord: QueueAndRoleMappingItem;
    newRecord: QueueAndRoleMappingItem;
  };
}

export interface QueueAndRoleMappingState {
  loading: boolean;
  loadingModal: boolean;
  loadingQueueName: boolean;
  queueAndRoleList: QueueAndRoleMappingItem[];
  queueAndRole?: QueueAndRoleMappingItem;
  error?: string;
  errorModal?: string;
  errorQueueName?: string;
  isOpenModal: boolean;
  isOpenModalDelete: boolean;
  sortBy: SortType;
  queueNameList: QueueAndRoleMappingItem[];
  userRoleList: UserRoleItem[];
  selectedUserRole?: UserRoleItem;
  loadingUserRoleList: boolean;
  textSearch: string;
}
