import React, { useEffect, useMemo } from 'react';
import { useFormik } from 'formik';
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import {
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { genAmtId } from 'app/_libraries/_dls/utils';
import {
  // isFunction,
  isEmpty
} from 'lodash';
import { useDispatch, useSelector } from 'react-redux';

// Const
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { QueueAndRoleMappingItem } from '../types';
import { I18N_QUEUE_AND_ROLE_MAPPING } from '../constants';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';

// hooks

// Component
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';
import QueueMultiSelect from './QueueMultiSelect';

// Redux
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { queueAndRoleMappingActions } from '../_redux/reducers';
import {
  selectModalOpen,
  selectModalLoading,
  selectQueueNameList,
  selectGridData
} from '../_redux/selectors';

export interface AddQueueModalProps {}

const AddQueueModal: React.FC<AddQueueModalProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const testId = 'configurationEntitlement-addQueueModal';

  const isLoadingModal = useSelector(selectModalLoading);
  const isOpenModal = useSelector(selectModalOpen);
  const data: QueueAndRoleMappingItem[] = useSelector(selectQueueNameList);
  const gridData = useSelector(selectGridData);

  const queueDropdownList = useMemo(
    () =>
      data?.filter(
        item =>
          gridData.findIndex(data => data?.queueId === item.queueId) === -1
      ),
    [data, gridData]
  );

  const {
    values,
    touched,
    errors,
    handleBlur,
    handleChange,
    resetForm,
    validateForm,
    handleSubmit
  } = useFormik<{
    queues: QueueAndRoleMappingItem[];
  }>({
    initialValues: {
      queues: queueDropdownList?.length === 1 ? queueDropdownList : []
    },
    enableReinitialize: true,
    onSubmit: values => {
      dispatch(
        queueAndRoleMappingActions.triggerAddQueueAndRoleMapping({
          postData: values.queues.map(item => ({
            queueId: item.queueId,
            queueName: item.queueName
          }))
        })
      );
    },
    validateOnBlur: true,
    validateOnChange: false,
    validateOnMount: true,
    validate: ({ queues }) => {
      const errors: Record<string, string> | undefined = {};

      if (!queues.length) {
        errors['queues'] = t(
          I18N_QUEUE_AND_ROLE_MAPPING.QUEUE_NAME_IS_REQUIRED
        );
      }

      return errors;
    }
  });

  const handleCloseModal = () => {
    resetForm();
    dispatch(queueAndRoleMappingActions.cancelModalForm());
  };

  // revalidate form after close modal
  useEffect(() => {
    if (!isOpenModal) {
      resetForm();
      return;
    }

    validateForm();
  }, [isOpenModal, resetForm, validateForm]);

  useEffect(() => {
    return () => {
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          forSection: 'inModalBody',
          storeId: API_ERROR_STORE_ID.QUEUE_ROLE_MAPPING_MODAL
        })
      );
    };
  }, [dispatch]);

  return (
    <Modal sm loading={isLoadingModal} show={isOpenModal} dataTestId={testId}>
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId={genAmtId(testId, 'header', '')}
      >
        <ModalTitle dataTestId={genAmtId(testId, 'title', '')}>
          {t('txt_add_queue')}
        </ModalTitle>
      </ModalHeader>
      <ModalBodyWithApiError
        apiErrorClassName="mb-24"
        storeId={API_ERROR_STORE_ID.QUEUE_ROLE_MAPPING_MODAL}
        dataTestId={genAmtId(testId, 'body', '')}
      >
        <form>
          <div className="row-cols-1">
            <QueueMultiSelect
              list={queueDropdownList}
              value={values?.queues}
              error={errors?.queues}
              touched={touched?.queues}
              handleChange={handleChange}
              handleBlur={handleBlur}
            />
          </div>
        </form>
      </ModalBodyWithApiError>
      <ModalFooter
        cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
        okButtonText={t(I18N_COMMON_TEXT.SUBMIT)}
        disabledOk={!isEmpty(errors)}
        onCancel={handleCloseModal}
        onOk={handleSubmit}
        dataTestId={genAmtId(testId, 'footer', '')}
      />
    </Modal>
  );
};

export default AddQueueModal;
