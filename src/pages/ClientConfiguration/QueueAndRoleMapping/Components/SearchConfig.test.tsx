import React from 'react';

// Components
import SearchConfig from './SearchConfig';

// Utils
import {
  mockActionCreator,
  queryAllByClass,
  renderMockStore
} from 'app/test-utils';

// Const
import { fireEvent, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { queueAndRoleMappingActions } from '../_redux/reducers';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

const spyQueueAndRoleMapping = mockActionCreator(queueAndRoleMappingActions);

const initialState: Partial<RootState> = {
  clientConfigQueueAndRoleMapping: {
    loading: false,
    loadingModal: false,
    loadingQueueName: false,
    queueAndRoleList: [],
    queueAndRole: {
      queueId: '1',
      queueName: 'test',
      queueGroupIdOwner: '1',
      accessLevel: '1',
      userRoleName: [
        {
          code: 'ADMIN',
          text: 'admin'
        }
      ],
      userRoleMapping: '1'
    },
    error: '',
    errorModal: '',
    errorQueueName: '',
    isOpenModal: false,
    isOpenModalDelete: true,
    sortBy: { id: 'queueName', order: undefined },
    queueNameList: [
      {
        queueId: '1',
        queueName: 'test',
        queueGroupIdOwner: '1',
        accessLevel: '1'
      }
    ],
    userRoleList: [
      {
        code: 'ADMIN',
        text: 'admin'
      }
    ],
    selectedUserRole: undefined,
    loadingUserRoleList: false,
    textSearch: ''
  }
};

const props = {
  onPageChange: jest.fn(),
  currentPage: 1,
  onClearAndReset: jest.fn()
};

describe('Test SearchConfig', () => {
  it('trigger change text searchConfig', async () => {
    const changeTextSearchMock = spyQueueAndRoleMapping('onChangeTextSearch');

    const { wrapper } = renderMockStore(<SearchConfig {...props} />, {
      initialState
    });

    const principleMultiSelect = await screen.findByTestId(
      'queueName_search_textSearch_dls-text-search_input_dls-text-box_input'
    );

    fireEvent.change(principleMultiSelect, { target: { value: 'test' } });

    userEvent.click(queryAllByClass(wrapper.container, /icon icon-search/)[0]!);

    expect(changeTextSearchMock).toBeCalledWith('test');
  });

  it('trigger change text searchConfig 2', async () => {
    const changeTextSearchMock = spyQueueAndRoleMapping('onChangeTextSearch');

    const { wrapper } = renderMockStore(<SearchConfig {...props} />, {
      initialState
    });

    const principleMultiSelect = await screen.findByTestId(
      'queueName_search_textSearch_dls-text-search_input_dls-text-box_input'
    );

    fireEvent.change(principleMultiSelect, { target: { value: '  ' } });

    userEvent.click(queryAllByClass(wrapper.container, /icon icon-search/)[0]!);

    expect(changeTextSearchMock).not.toBeCalled();
  });

  it('should not trigger change text searchConfig when init text not changed', async () => {
    const openModalFormMock = spyQueueAndRoleMapping('openModalForm');

    const { wrapper } = renderMockStore(<SearchConfig {...props} />, {
      initialState
    });

    userEvent.click(wrapper.getByText('txt_add_queue'));

    expect(openModalFormMock).toBeCalled();
  });
});
