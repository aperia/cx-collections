import React from 'react';
import { render, screen } from '@testing-library/react';
import QueueMultiSelect from './QueueMultiSelect';

jest.mock('app/_libraries/_dls/components/MultiSelect', () =>
  jest.requireActual('app/test-utils/mocks/MockMultiSelect')
);

const onChange = jest.fn();
const onBlur = jest.fn();

describe('should test queue multi select', () => {
  it('should be rendered', () => {
    render(
      <QueueMultiSelect
        list={[
          {
            queueName: 'queue 1'
          },
          {
            queueName: 'queue 2'
          }
        ]}
        value={[
          {
            queueName: 'queue 1'
          },
          {
            queueName: 'queue 2'
          }
        ]}
        handleChange={onChange}
        handleBlur={onBlur}
      />
    );

    screen.getByTestId('MultiSelect_groupFormatInputText').click();
    expect(screen.getByText('groupFormatInputText')).toBeInTheDocument();
  });
});
