import React from 'react';
import Nodata from './NoData';
import { renderMockStore } from 'app/test-utils/renderComponentWithMockStore';
import { screen } from '@testing-library/react';

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('Test NoData', () => {
  it('Should render ui', () => {
    renderMockStore(<Nodata />, {});
    const queryTextNodata = screen.queryByText('txt_no_data');
    expect(queryTextNodata).toBeInTheDocument();
  });
});
