import React from 'react';

// Components
import Search from './Search';

// Utils
import { queryAllByClass, renderMockStore } from 'app/test-utils';

// Const
import { fireEvent, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

const initialState: Partial<RootState> = {
  clientConfigQueueAndRoleMapping: {
    loading: false,
    loadingModal: false,
    loadingQueueName: false,
    queueAndRoleList: [],
    queueAndRole: {
      queueId: '1',
      queueName: 'test',
      queueGroupIdOwner: '1',
      accessLevel: '1',
      userRoleName: [
        {
          code: 'ADMIN',
          text: 'admin'
        }
      ],
      userRoleMapping: '1'
    },
    error: '',
    errorModal: '',
    errorQueueName: '',
    isOpenModal: false,
    isOpenModalDelete: true,
    sortBy: { id: 'queueName', order: undefined },
    queueNameList: [],
    userRoleList: [
      {
        code: 'ADMIN',
        text: 'admin'
      }
    ],
    selectedUserRole: undefined,
    loadingUserRoleList: false,
    textSearch: ''
  }
};

const props = {
  onPageChange: jest.fn(),
  currentPage: 1,
  onChangeTextSearch: jest.fn(),
  onClearAndReset: jest.fn(),
  isDisplayClearAndReset: true
};

describe('Test Search', () => {
  it('trigger change text search', async () => {
    const { wrapper } = renderMockStore(
      <Search {...{ ...props, currentPage: 2 }} />,
      {
        initialState
      }
    );

    const principleMultiSelect = await screen.findByTestId(
      'queueName_search_textSearch_dls-text-search_input_dls-text-box_input'
    );

    fireEvent.change(principleMultiSelect, { target: { value: 'test' } });

    userEvent.click(queryAllByClass(wrapper.container, /icon icon-search/)[0]!);

    expect(props.onChangeTextSearch).toBeCalledWith('test');
    expect(props.onPageChange).toBeCalledWith(1);
  });

  it('should not trigger change text search when text trim empty', async () => {
    const { wrapper } = renderMockStore(
      <Search {...{ ...props, textSearch: '  ', currentPage: 1 }} />,
      {
        initialState
      }
    );

    userEvent.click(queryAllByClass(wrapper.container, /icon icon-search/)[0]!);

    expect(props.onChangeTextSearch).not.toBeCalled();
  });

  it('should not trigger change text search when init text not changed', async () => {
    const { wrapper } = renderMockStore(
      <Search {...{ ...props, textSearch: '123', currentPage: 2 }} />,
      {
        initialState
      }
    );

    userEvent.click(queryAllByClass(wrapper.container, /icon icon-search/)[0]!);

    expect(props.onChangeTextSearch).not.toBeCalled();
  });

  it('should trigger clear text search', async () => {
    const { wrapper } = renderMockStore(
      <Search {...{ ...props, textSearch: 'new text', currentPage: 2 }} />,
      {
        initialState
      }
    );

    userEvent.click(queryAllByClass(wrapper.container, /icon icon-close/)[0]!);
    userEvent.click(await screen.findByText('txt_clear_and_reset'));

    expect(props.onChangeTextSearch).not.toBeCalled();
    expect(props.onClearAndReset).toBeCalled();
  });
});
