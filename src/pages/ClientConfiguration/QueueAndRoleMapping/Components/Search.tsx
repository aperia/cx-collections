import React, { useEffect, useState } from 'react';
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { Button, TextSearch } from 'app/_libraries/_dls/components';

// Component
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

interface ISearch {
  currentPage: number;
  onPageChange: (number: number) => void;
  onChangeTextSearch: (text: string) => void;
  textSearch?: string;
  onClearAndReset: () => void;
  isDisplayClearAndReset?: boolean;
}

const Search: React.FC<ISearch> = ({
  onPageChange,
  currentPage,
  onChangeTextSearch,
  onClearAndReset,
  isDisplayClearAndReset,
  textSearch: textSearchProps = '',
  children
}) => {
  const { t } = useTranslation();
  const [textSearch, setTextSearch] = useState<string>('');

  useEffect(() => {
    setTextSearch(textSearchProps);
  }, [textSearchProps]);

  const handleChangeSearch = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTextSearch(e.target.value);
  };

  const handleSearch = (data: string) => {
    if (textSearch === textSearchProps) return undefined;
    onChangeTextSearch(data);
    currentPage !== 1 && onPageChange(1);
  };

  const handleClearSearch = () => {
    setTextSearch('');
  };

  const handleClearAndResetSearch = () => {
    setTextSearch('');
    onClearAndReset();
  };

  return (
    <div className="mt-24 mb-16 d-flex justify-content-between">
      <h5>{t('txt_queue_list')} </h5>
      <div className="text-right">
        <TextSearch
          value={textSearch}
          onChange={handleChangeSearch}
          onSearch={handleSearch}
          onClear={handleClearSearch}
          placeholder={t(I18N_COMMON_TEXT.TYPE_TO_SEARCH)}
          dataTestId="queueName_search_textSearch"
        />
        {children}
        {isDisplayClearAndReset && (
          <Button
            size="sm"
            className="mt-16 mr-n8 ml-16"
            onClick={handleClearAndResetSearch}
            variant="outline-primary"
            dataTestId="queueName_search_reset-btn"
          >
            {t('txt_clear_and_reset')}
          </Button>
        )}
      </div>
    </div>
  );
};

export default Search;
