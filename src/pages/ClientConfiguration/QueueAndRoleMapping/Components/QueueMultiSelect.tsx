import React, { useMemo } from 'react';
import { FormikErrors, FormikTouched } from 'formik';

// components
import MultiSelect from 'app/_libraries/_dls/components/MultiSelect';
import { DropdownBaseChangeEvent } from 'app/_libraries/_dls/components';

// constants
import { QueueAndRoleMappingItem } from '../types';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { getGroupFormatInputText } from 'app/helpers/formatMultiSelectText';

// redux

const QueueMultiSelect: React.FC<{
  list: QueueAndRoleMappingItem[];
  value: QueueAndRoleMappingItem[];
  error?: string | string[] | FormikErrors<QueueAndRoleMappingItem>[];
  touched?: FormikTouched<QueueAndRoleMappingItem>[];
  handleChange: (e: DropdownBaseChangeEvent) => void;
  handleBlur: (e: React.FocusEvent) => void;
}> = ({ list, value, error, touched, handleChange, handleBlur }) => {
  const { t } = useTranslation();
  const testId = 'queueNameMultiSelect';

  const dropdownListItem = useMemo(() => {
    return list?.map((item: QueueAndRoleMappingItem) => (
      <MultiSelect.Item
        key={item?.queueId}
        label={item?.queueName}
        value={item}
        variant="checkbox"
      />
    ));
  }, [list]);

  return (
    <MultiSelect
      name="queues"
      label={t('txt_queue_name')}
      value={value}
      onChange={handleChange}
      onBlur={handleBlur}
      textField="queueName"
      checkAll
      checkAllLabel={t('txt_all')}
      dataTestId={testId}
      required
      error={{
        status: Boolean(error && touched),
        message: t(error) as string
      }}
      noResult={t('txt_no_results_found')}
      searchBarPlaceholder={t('txt_type_to_search')}
      groupFormatInputText={(...args) =>
        t(getGroupFormatInputText(...args), {
          selected: args[0],
          items: args[1]
        })
      }
    >
      {dropdownListItem}
    </MultiSelect>
  );
};

export default QueueMultiSelect;
