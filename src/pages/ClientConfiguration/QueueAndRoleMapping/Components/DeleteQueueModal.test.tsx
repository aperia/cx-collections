import React from 'react';
import userEvent from '@testing-library/user-event';

// Components
import DeleteQueueModal from './DeleteQueueModal';

// Utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

// Const
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { act } from '@testing-library/react';
import { queueAndRoleMappingActions } from '../_redux/reducers';

HTMLCanvasElement.prototype.getContext = jest.fn();

const spyQueueAndRoleMapping = mockActionCreator(queueAndRoleMappingActions);

const initialState: Partial<RootState> = {
  clientConfigQueueAndRoleMapping: {
    loading: false,
    loadingModal: false,
    loadingQueueName: false,
    queueAndRoleList: [],
    queueAndRole: {
      queueId: '1',
      queueName: 'test',
      queueGroupIdOwner: '1',
      accessLevel: '1',
      userRoleName: [
        {
          code: 'ADMIN',
          text: 'admin'
        }
      ],
      userRoleMapping: '1'
    },
    error: '',
    errorModal: '',
    errorQueueName: '',
    isOpenModal: false,
    isOpenModalDelete: true,
    sortBy: { id: 'queueName', order: undefined },
    queueNameList: [],
    userRoleList: [
      {
        code: 'ADMIN',
        text: 'admin'
      }
    ],
    selectedUserRole: undefined,
    loadingUserRoleList: false,
    textSearch: ''
  }
};

describe('Test DeleteQueueModal', () => {
  it('trigger cancel', () => {
    const mockDeleteQueueAndRoleMappingRequest = spyQueueAndRoleMapping(
      'deleteQueueAndRoleMappingRequest'
    );

    const { wrapper } = renderMockStore(<DeleteQueueModal />, {
      initialState
    });

    act(() => {
      userEvent.click(wrapper.getByText(I18N_COMMON_TEXT.CANCEL));
    });

    expect(mockDeleteQueueAndRoleMappingRequest).toBeCalledWith({});
  });

  it('trigger submit', async () => {
    const mockTriggerDeleteQueueAndRoleMapping = spyQueueAndRoleMapping(
      'triggerDeleteQueueAndRoleMapping'
    );

    const { wrapper } = renderMockStore(<DeleteQueueModal />, {
      initialState
    });

    act(() => {
      userEvent.click(wrapper.getByText(I18N_COMMON_TEXT.DELETE));
    });

    expect(mockTriggerDeleteQueueAndRoleMapping).toHaveBeenCalled();
  });
});
