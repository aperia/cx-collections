import React from 'react';

// components
import { Icon } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

const NoData: React.FC = ({ children }) => {
  const { t } = useTranslation();
  return (
    <div className="text-center mt-80 mb-16">
      <Icon name="file" className="fs-80 color-light-l12" />
      <p
        className="mt-20 mb-24 color-grey"
        data-testid={genAmtId('queueRoleMapping', 'no-QueueName', '')}
      >
        {t('txt_no_data')}
      </p>
      {children}
    </div>
  );
};

export default NoData;
