import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

// Hooks
import { useStoreIdSelector } from 'app/hooks';

// Redux
import {
  selectDisplayClearAndReset,
  selectTextSearch
} from '../_redux/selectors';
import { queueAndRoleMappingActions } from '../_redux/reducers';

// Component
import Search from './Search';
import AddQueue from './AddQueue';

interface ISearchConfig {
  onPageChange: (number: number) => void;
  onClearAndReset: () => void;
  currentPage: number;
}

const SearchConfig: React.FC<ISearchConfig> = ({
  onPageChange,
  currentPage,
  onClearAndReset
}) => {
  const dispatch = useDispatch();

  const text = useStoreIdSelector<string>(selectTextSearch);
  const isDisplayClearAndReset = useSelector(selectDisplayClearAndReset);

  const handleChange = (value: string) => {
    // if (!value.trim().length && value !== '') return;
    dispatch(queueAndRoleMappingActions.onChangeTextSearch(value));
  };

  const handleOnAddQueue = () => {
    dispatch(queueAndRoleMappingActions.openModalForm());
  };

  return (
    <Search
      onChangeTextSearch={handleChange}
      currentPage={currentPage}
      onPageChange={onPageChange}
      textSearch={text}
      onClearAndReset={onClearAndReset}
      isDisplayClearAndReset={isDisplayClearAndReset}
    >
      <AddQueue className="mt-16" onAddQueue={handleOnAddQueue} />
    </Search>
  );
};

export default SearchConfig;
