import React from 'react';
import userEvent from '@testing-library/user-event';

import { renderMockStore } from 'app/test-utils';

import AddQueue from './AddQueue';

HTMLCanvasElement.prototype.getContext = jest.fn();

it('Test AddQueue', () => {
  const onAddQueue = jest.fn();

  const { wrapper } = renderMockStore(<AddQueue onAddQueue={onAddQueue} />, {
    initialState: {
      clientConfigQueueAndRoleMapping: {
        queueNameList: [
          {
            queueId: '1',
            queueName: 'test',
            queueGroupIdOwner: '1',
            accessLevel: '1'
          }
        ]
      } as any
    }
  });

  userEvent.click(wrapper.getByText('txt_add_queue'));

  expect(onAddQueue).toHaveBeenCalled();
});

it('Test AddQueue tooltip', () => {
  const onAddQueue = jest.fn();

  const { wrapper } = renderMockStore(<AddQueue onAddQueue={onAddQueue} />, {
    initialState: {
      clientConfigQueueAndRoleMapping: {
        queueNameList: [
          {
            queueId: '1',
            queueName: 'test',
            queueGroupIdOwner: '1',
            accessLevel: '1'
          }
        ],
        queueAndRoleList: [
          {
            queueId: '1',
            queueName: 'test'
          }
        ]
      } as any
    }
  });

  expect(wrapper.getByText('txt_add_queue')).toHaveAttribute('disabled');
});
