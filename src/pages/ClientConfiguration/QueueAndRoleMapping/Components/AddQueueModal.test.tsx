import React from 'react';
import userEvent from '@testing-library/user-event';

// Components
import AddQueueModal from './AddQueueModal';

// Utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

// Const
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { act, fireEvent, screen } from '@testing-library/react';
import { queueAndRoleMappingActions } from '../_redux/reducers';

HTMLCanvasElement.prototype.getContext = jest.fn();

const spyQueueAndRoleMapping = mockActionCreator(queueAndRoleMappingActions);

const initialState: Partial<RootState> = {
  clientConfigQueueAndRoleMapping: {
    loading: false,
    loadingModal: true,
    loadingQueueName: false,
    queueAndRoleList: [],
    queueAndRole: {},
    error: '',
    errorModal: '',
    errorQueueName: '',
    isOpenModal: true,
    isOpenModalDelete: false,
    sortBy: { id: 'queueName', order: undefined },
    queueNameList: [
      {
        queueId: '1',
        queueName: 'test',
        queueGroupIdOwner: '1',
        accessLevel: '1'
      }
    ],
    userRoleList: [
      {
        description: 'ADMIN',
        value: 'admin'
      }
    ],
    selectedUserRole: undefined,
    loadingUserRoleList: false,
    textSearch: ''
  }
};

describe('Test AddQueueModal', () => {
  it('should render', () => {
    const mockOnCloseModal = spyQueueAndRoleMapping('cancelModalForm');

    const { wrapper } = renderMockStore(<AddQueueModal />, {
      initialState: {
        clientConfigQueueAndRoleMapping: {
          loading: false,
          loadingModal: true,
          loadingQueueName: false,
          queueAndRoleList: [
            {
              queueId: '1',
              queueName: 'test'
            },
            {
              queueId: '2',
              queueName: 'test1'
            }
          ],
          queueAndRole: {},
          error: '',
          errorModal: '',
          errorQueueName: '',
          isOpenModal: true,
          isOpenModalDelete: false,
          sortBy: { id: 'queueName', order: undefined },
          queueNameList: [
            {
              queueId: '1',
              queueName: 'test',
              queueGroupIdOwner: '1',
              accessLevel: '1'
            }
          ],
          userRoleList: [
            {
              description: 'ADMIN',
              value: 'admin'
            }
          ],
          selectedUserRole: undefined,
          loadingUserRoleList: false,
          textSearch: ''
        }
      }
    });

    act(() => {
      userEvent.click(wrapper.getByText(I18N_COMMON_TEXT.CANCEL));
    });

    expect(mockOnCloseModal).toBeCalledWith();
    expect(wrapper.getByText('txt_add_queue')).toBeInTheDocument();
  });

  it('trigger submit', async () => {
    spyQueueAndRoleMapping('triggerAddQueueAndRoleMapping');

    const { wrapper } = renderMockStore(<AddQueueModal />, {
      initialState
    });

    // Touch and raise error for `agentId`
    const principleMultiSelect = await screen.findByTestId(
      'queueNameMultiSelect_dls-multiselect'
    );

    act(() => {
      userEvent.click(principleMultiSelect);
      fireEvent.blur(principleMultiSelect);
      userEvent.click(wrapper.getByText(I18N_COMMON_TEXT.SUBMIT));
    });

    expect(wrapper.getByText(I18N_COMMON_TEXT.SUBMIT)).not.toHaveAttribute(
      'disabled'
    );
  });

  it('trigger submit 2', async () => {
    spyQueueAndRoleMapping('triggerAddQueueAndRoleMapping');

    const { wrapper } = renderMockStore(<AddQueueModal />, {
      initialState: {
        clientConfigQueueAndRoleMapping: {
          ...initialState.clientConfigQueueAndRoleMapping,
          selectedUserRole: {
            code: 'ADMIN',
            text: 'admin'
          }
        } as any
      }
    });

    // Touch and raise error for `agentId`
    const principleMultiSelect = await screen.findByTestId(
      'queueNameMultiSelect_dls-multiselect'
    );

    act(() => {
      userEvent.click(principleMultiSelect);
      fireEvent.blur(principleMultiSelect);
      userEvent.click(wrapper.getByText(I18N_COMMON_TEXT.SUBMIT));
    });

    expect(wrapper.getByText(I18N_COMMON_TEXT.SUBMIT)).not.toHaveAttribute(
      'disabled'
    );
  });

  it('trigger validate errors', async () => {
    const { wrapper } = renderMockStore(<AddQueueModal />, {
      initialState: {
        clientConfigQueueAndRoleMapping: {
          ...initialState.clientConfigQueueAndRoleMapping,
          queueNameList: [
            {
              queueId: '1',
              queueName: 'test',
              queueGroupIdOwner: '1',
              accessLevel: '1'
            },
            {
              queueId: '2',
              queueName: 'test2',
              queueGroupIdOwner: '1',
              accessLevel: '2'
            }
          ]
        } as any
      }
    });

    // Touch and raise error for `agentId`
    const principleMultiSelect = wrapper.baseElement.querySelector('input')!;

    act(() => {
      userEvent.click(principleMultiSelect);
      fireEvent.blur(principleMultiSelect);
    });

    expect(wrapper.getByText(I18N_COMMON_TEXT.SUBMIT)).not.toHaveAttribute(
      'disabled=""'
    );
  });

  it('should render nothing', async () => {
    const { wrapper } = renderMockStore(<AddQueueModal />, {
      initialState: {
        clientConfigQueueAndRoleMapping: {
          ...initialState.clientConfigQueueAndRoleMapping,
          isOpenModal: false
        } as any
      }
    });

    expect(
      wrapper.baseElement.querySelector(I18N_COMMON_TEXT.SUBMIT)
    ).toBeNull();
  });
});
