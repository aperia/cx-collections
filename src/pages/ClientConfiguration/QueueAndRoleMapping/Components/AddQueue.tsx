import React, { useMemo } from 'react';
import classNames from 'classnames';
import { useSelector } from 'react-redux';

// hooks
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';

// Components
import { Button, Tooltip } from 'app/_libraries/_dls/components';

// redux
import { selectGridData, selectQueueNameList } from '../_redux/selectors';

// Constants
import { I18N_QUEUE_AND_ROLE_MAPPING } from '../constants';

export interface AddQueueProps {
  className?: string;
  disabled?: boolean;
  onAddQueue: () => void;
}

const AddQueue: React.FC<AddQueueProps> = ({
  className,
  disabled,
  onAddQueue
}) => {
  const { t } = useTranslation();
  const queueNameData = useSelector(selectQueueNameList);
  const gridData = useSelector(selectGridData);

  const queueDropdownList = useMemo(
    () =>
      queueNameData?.filter(
        item =>
          gridData.findIndex(data => data?.queueId === item.queueId) === -1
      ),
    [queueNameData, gridData]
  );

  return queueDropdownList?.length ? (
    <Button
      disabled={disabled}
      className={classNames('mr-n8', className)}
      id="queue-name__add-queue-name-btn"
      onClick={onAddQueue}
      size="sm"
      variant="outline-primary"
      dataTestId="queueRoleMapping-addQueue"
    >
      {t('txt_add_queue')}
    </Button>
  ) : (
    <Tooltip
      triggerClassName={classNames('mr-n8', className)}
      element={
        <span>{t(I18N_QUEUE_AND_ROLE_MAPPING.ALL_QUEUE_NAMES_ARE_ADDED)}</span>
      }
    >
      <Button
        id="queue-name__add-queue-name-btn-disabled"
        size="sm"
        variant="outline-primary"
        disabled
        dataTestId="queueRoleMapping-addQueue"
      >
        {t('txt_add_queue')}
      </Button>
    </Tooltip>
  );
};

export default AddQueue;
