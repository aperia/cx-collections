import React from 'react';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Constants
import { I18N_QUEUE_AND_ROLE_MAPPING } from 'pages/ClientConfiguration/QueueAndRoleMapping/constants';

// Redux
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';
import { useDispatch } from 'react-redux';
import { genAmtId } from 'app/_libraries/_dls/utils';

const QueueListNavigation: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const handleQueueRoleMappingTab = () => {
    dispatch(
      actionsTab.addTab({
        id: 'queue-role-mapping',
        title: I18N_QUEUE_AND_ROLE_MAPPING.QUEUE_AND_ROLE_MAPPING,
        storeId: 'queue-role-mapping',
        tabType: 'queueRoleMapping',
        iconName: 'file'
      })
    );
  };

  return (
    <span
      className="link-header"
      onClick={handleQueueRoleMappingTab}
      data-testid={genAmtId('header_queue_role_mapping', 'title', '')}
    >
      {t(I18N_QUEUE_AND_ROLE_MAPPING.QUEUE_AND_ROLE_MAPPING)}
    </span>
  );
};

export default QueueListNavigation;
