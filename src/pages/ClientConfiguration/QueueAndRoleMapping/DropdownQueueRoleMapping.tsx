import React from 'react';

// components
import {
  DropdownBaseChangeEvent,
  DropdownList
} from 'app/_libraries/_dls/components';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';

// i18n
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { selectUserRoleList } from './_redux/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { UserRoleItem } from './types';
import { queueAndRoleMappingActions } from './_redux/reducers';

const DropdownQueueRoleMapping: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const userRoles: UserRoleItem[] = useSelector(selectUserRoleList);

  const onChangeUserRole = (event: DropdownBaseChangeEvent) => {
    const { value } = event.target;
    batch(() => {
      dispatch(queueAndRoleMappingActions.onChangeUserRole(value));
      dispatch(queueAndRoleMappingActions.getQueueAndRoleMapping());
      dispatch(queueAndRoleMappingActions.getQueueNameList());
    });
  };

  return (
    <React.Fragment>
      <p
        className="mt-24"
        data-testid={genAmtId('queueUserRoles', 'select-a-role', '')}
      >
        {t('txt_select_a_role_configure_queue_role_mapping')}
      </p>
      <div className="w-300px mt-24">
        <DropdownList
          onChange={onChangeUserRole}
          textField="description"
          required
          label={t('txt_user_role')}
          dataTestId="queueUserRoles_user-role"
          noResult={t('txt_no_results_found')}
        >
          {userRoles.map(item => (
            <DropdownList.Item
              key={item.value}
              label={item.description}
              value={item}
            />
          ))}
        </DropdownList>
      </div>
    </React.Fragment>
  );
};

export default DropdownQueueRoleMapping;
