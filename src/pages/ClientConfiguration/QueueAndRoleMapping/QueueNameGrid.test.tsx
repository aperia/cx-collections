import React from 'react';

// Components
import QueueNameGrid from './QueueNameGrid';

// Utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

// Const
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { queueAndRoleMappingActions } from './_redux/reducers';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

const spyQueueAndRoleMapping = mockActionCreator(queueAndRoleMappingActions);

const initialState: Partial<RootState> = {
  clientConfigQueueAndRoleMapping: {
    loading: false,
    loadingModal: false,
    loadingQueueName: false,
    queueAndRoleList: [
      {
        queueId: '160675456170',
        queueName: 'COLX_DEFAULT',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [
          { code: 'CXCOL1', text: 'CXCOL1' },
          { code: 'CXCOL2', text: 'CXCOL2' }
        ],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '001',
        queueName: 'Non Verified Bankrupt',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [{ code: 'CXSUP1', text: 'CXSUP1' }],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '002',
        queueName: 'Verified Bankrupt',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [
          { code: 'CXCOL2', text: 'CXCOL2' },
          { code: 'CXCOL3', text: 'CXCOL3' },
          { code: 'CXCOL4', text: 'CXCOL4' }
        ],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '003',
        queueName: 'Discharge Bankrupt',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [{ code: 'CXADMIN', text: 'CXADMIN' }],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '004',
        queueName: 'Dismissed BK',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [{ code: 'CXMGR1', text: 'CXMGR1' }],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '005',
        queueName: 'Cease and Desist',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [{ code: 'CXSUP1', text: 'CXSUP1' }],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '006',
        queueName: 'Customer Represented by Attorney',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [
          { code: 'CXCOL1', text: 'CXCOL1' },
          { code: 'CXCOL2', text: 'CXCOL2' }
        ],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '007',
        queueName: 'Pending Deceased',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [{ code: 'CXADMIN', text: 'CXADMIN' }],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '008',
        queueName: 'Temporary Reform',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [
          { code: 'CXCOL1', text: 'CXCOL1' },
          { code: 'CXSUP1', text: 'CXSUP1' },
          { code: 'CXMGR1', text: 'CXMGR1' }
        ],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '009',
        queueName: 'Customer Enrolled into Hardship',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [{ code: 'CXMGR1', text: 'CXMGR1' }],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '010',
        queueName: 'Non Speak Call Back',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [{ code: 'CXCOL5', text: 'CXCOL5' }],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '011',
        queueName: 'Queue Name 11',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [
          { code: 'CXCOL1', text: 'CXCOL1' },
          { code: 'CXCOL2', text: 'CXCOL2' }
        ],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '012',
        queueName: 'Queue Name 12',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [{ code: 'CXCOL1', text: 'CXCOL1' }],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '013',
        queueName: 'Queue Name 13',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [{ code: 'CXCOL2', text: 'CXCOL2' }],
        accessLevel: 'UPDATE'
      },
      {
        queueId: '014',
        queueName: 'Queue Name 14',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [
          { code: 'CXCOL1', text: 'CXCOL1' },
          { code: 'CXCOL2', text: 'CXCOL2' },
          { code: 'CXCOL3', text: 'CXCOL3' },
          { code: 'CXCOL4', text: 'CXCOL4' },
          { code: 'CXCOL5', text: 'CXCOL5' },
          { code: 'CXMGR1', text: 'CXMGR1' },
          { code: 'CXMGR2', text: 'CXMGR2' },
          { code: 'CXMGR3', text: 'CXMGR3' },
          { code: 'CXMGR4', text: 'CXMGR4' },
          { code: 'CXMGR5', text: 'CXMGR5' }
        ],
        accessLevel: 'UPDATE'
      }
    ],
    queueAndRole: {},
    error: '',
    errorModal: '',
    errorQueueName: '',
    isOpenModal: false,
    isOpenModalDelete: true,
    sortBy: { id: 'queueName', order: undefined },
    queueNameList: [],
    userRoleList: [],
    selectedUserRole: undefined,
    loadingUserRoleList: false,
    textSearch: ''
  }
};

describe('Test QueueNameGrid', () => {
  it('should render QueueNameGrid with pagination', async () => {
    spyQueueAndRoleMapping('onChangeUserRole');
    spyQueueAndRoleMapping('getQueueAndRoleMapping');
    spyQueueAndRoleMapping('getQueueNameList');
    const deleteQueueAndRoleMappingMock = spyQueueAndRoleMapping(
      'deleteQueueAndRoleMappingRequest'
    );
    const changeSortMock = spyQueueAndRoleMapping('onChangeSort');

    renderMockStore(<QueueNameGrid />, {
      initialState
    });

    expect(
      await screen.findByTestId(
        'clientConfig_queueName_grid_pagination_dls-pagination'
      )
    ).toBeInTheDocument();

    userEvent.click(
      await screen.findByTestId(
        '0_action_clientConfig_queueName_grid-delete_dls-button'
      )
    );

    expect(deleteQueueAndRoleMappingMock).toBeCalledWith({
      queueAndRole: {
        accessLevel: 'UPDATE',
        queueGroupIdOwner: 'FD - Admin',
        queueId: '005',
        queueName: 'Cease and Desist',
        userRoleName: [
          {
            code: 'CXSUP1',
            text: 'CXSUP1'
          }
        ]
      }
    });

    userEvent.click(
      await screen.findByTestId(
        'clientConfig_queueName_grid_grid_dls-grid_header-txt_queue_name'
      )
    );

    expect(changeSortMock).toBeCalledWith({
      id: 'queueName',
      order: 'asc'
    });
  });

  it('should show no search data', async () => {
    spyQueueAndRoleMapping('onChangeUserRole');
    spyQueueAndRoleMapping('getQueueAndRoleMapping');
    spyQueueAndRoleMapping('getQueueNameList');
    const changeTextSearchMock = spyQueueAndRoleMapping('onChangeTextSearch');
    const changeSortMock = spyQueueAndRoleMapping('onChangeSort');

    renderMockStore(<QueueNameGrid />, {
      initialState: {
        clientConfigQueueAndRoleMapping: {
          ...initialState.clientConfigQueueAndRoleMapping,
          textSearch: 'tes'
        } as any
      }
    });

    expect(
      await screen.findByText('txt_memos_no_results_found')
    ).toBeInTheDocument();

    userEvent.click(await screen.findByText('txt_clear_and_reset'));

    expect(changeTextSearchMock).toBeCalledWith('');
    expect(changeSortMock).toBeCalledWith({
      id: 'queueName',
      order: undefined
    });
  });

  it('should render loading content', async () => {
    spyQueueAndRoleMapping('onChangeTextSearch');
    spyQueueAndRoleMapping('onChangeSort');
    spyQueueAndRoleMapping('onChangeUserRole');
    spyQueueAndRoleMapping('getQueueAndRoleMapping');
    spyQueueAndRoleMapping('getQueueNameList');

    const { baseElement } = renderMockStore(<QueueNameGrid />, {
      initialState: {
        clientConfigQueueAndRoleMapping: {
          ...initialState.clientConfigQueueAndRoleMapping,
          loading: true
        } as any
      }
    });

    const pagination = baseElement.querySelector(
      'clientConfig_queueName_grid_pagination_dls-pagination'
    );

    expect(pagination).toBeNull();
  });

  it('should render error content', async () => {
    spyQueueAndRoleMapping('onChangeTextSearch');
    spyQueueAndRoleMapping('onChangeSort');
    spyQueueAndRoleMapping('onChangeUserRole');
    const getQueueAndRoleMappingMock = spyQueueAndRoleMapping(
      'getQueueAndRoleMapping'
    );
    const getQueueNameListMock = spyQueueAndRoleMapping('getQueueNameList');

    renderMockStore(<QueueNameGrid />, {
      initialState: {
        clientConfigQueueAndRoleMapping: {
          ...initialState.clientConfigQueueAndRoleMapping,
          error: 'test error message'
        } as any
      }
    });

    expect(
      await screen.findByText(
        'txt_data_load_unsuccessful_click_reload_to_try_again'
      )
    ).toBeInTheDocument();

    userEvent.click(await screen.findByText('txt_reload'));

    expect(getQueueAndRoleMappingMock).toBeCalled();
    expect(getQueueNameListMock).toBeCalled();
  });
});
