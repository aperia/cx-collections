import { apiService } from 'app/utils/api.service';
import { ICollectionEntitlement, IQueueRoleMappingCommon } from './types';

export const queueAndRoleMappingService = {
  getQueueAndRoleMapping(data: {
    common: {
      privileges: string[];
      app: string;
      org: string;
    };
    selectRole: string[];
    selectCspa: string[];
  }) {
    const url =
      window.appConfig?.api?.clientConfigQueueAndRoleMapping
        ?.getQueueAndRoleMapping || '';
    return apiService.post(url, data);
  },

  addQueueAndRoleMapping(data: {
    common: IQueueRoleMappingCommon;
    roleEntitlementsRequest: {
      roleEntitlements: ICollectionEntitlement[];
    };
  }) {
    const url =
      window.appConfig?.api?.clientConfigQueueAndRoleMapping
        ?.addQueueAndRoleMapping || '';
    return apiService.post(url, data);
  },

  deleteQueueAndRoleMapping(data: {
    common: IQueueRoleMappingCommon;
    roleEntitlementsRequest: {
      roleEntitlements: ICollectionEntitlement[];
    };
  }) {
    const url =
      window.appConfig?.api?.clientConfigQueueAndRoleMapping
        ?.deleteQueueAndRoleMapping || '';
    return apiService.post(url, data);
  },

  getQueueNameList(data: {
    common: IQueueRoleMappingCommon;
    fetchSize?: string | null;
    pagingKey?: string | null;
  }) {
    const url =
      window.appConfig?.api?.clientConfigQueueAndRoleMapping
        ?.getQueueNameList || '';
    return apiService.post(url, data);
  },

  getUserRoleList() {
    const url =
      window.appConfig?.api?.clientConfigQueueAndRoleMapping?.getUserRoleList ||
      '';
    return apiService.post(url);
  }
};
