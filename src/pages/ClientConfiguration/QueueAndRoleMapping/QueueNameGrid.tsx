import React, { useCallback, useEffect, useMemo, useRef } from 'react';

// helpers
import classNames from 'classnames';
import { genAmtId } from 'app/_libraries/_dls/utils';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import {
  selectError,
  selectSortBy,
  selectLoading,
  selectGridDataWithSearch,
  selectIsSearchNoData,
  selectedUserRole
} from './_redux/selectors';
import { queueAndRoleMappingActions } from './_redux/reducers';

// components
import { FailedApiReload } from 'app/components';
import {
  Button,
  ColumnType,
  Grid,
  GridRef,
  Pagination,
  SortType
} from 'app/_libraries/_dls/components';
import ClearAndReset from 'pages/__commons/ClearAndReset';
import SearchConfig from './Components/SearchConfig';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { usePagination } from 'app/hooks';

// constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// types
import { QueueAndRoleMappingItem } from './types';

const Header = ({ className }: { className?: string }) => {
  const { t } = useTranslation();
  const testId = 'clientConfig_queueAndRoleMapping_grid';
  return (
    <h5
      className={classNames('fw-500', className)}
      data-testid={genAmtId(testId, 'header', '')}
    >
      {t('txt_queue_list')}
    </h5>
  );
};

const QueueNameGrid: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const gridRefs = useRef<GridRef>(null);
  const testId = 'clientConfig_queueName_grid';

  const isLoading = useSelector(selectLoading);
  const isError = useSelector(selectError);
  const data = useSelector(selectGridDataWithSearch);
  const sortBy = useSelector(selectSortBy);

  const isSearchNoData = useSelector(selectIsSearchNoData);
  const selectedRole = useSelector(selectedUserRole);

  const handleDeleteRequest = useCallback(
    record => {
      dispatch(
        queueAndRoleMappingActions.deleteQueueAndRoleMappingRequest({
          queueAndRole: record
        })
      );
    },
    [dispatch]
  );

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange,
    setCurrentPage,
    setCurrentPageSize
  } = usePagination(data);

  const handleReload = useCallback(() => {
    batch(() => {
      dispatch(queueAndRoleMappingActions.getQueueAndRoleMapping());
      dispatch(queueAndRoleMappingActions.getQueueNameList());
    });
  }, [dispatch]);

  const showPagination = total > pageSize[0];

  const dataColumn = useMemo(() => {
    return [
      {
        id: 'queueName',
        Header: t(I18N_COMMON_TEXT.QUEUE_NAME),
        accessor: 'queueName',
        isSort: true,
        autoWidth: true,
        width: 280
      },
      {
        id: 'action',
        Header: t(I18N_COMMON_TEXT.ACTIONS),
        accessor: (col: QueueAndRoleMappingItem, idx: number) => (
          <Button
            onClick={() => handleDeleteRequest(col)}
            size="sm"
            variant="outline-danger"
            dataTestId={`${idx}_action_${testId}-delete`}
          >
            {t(I18N_COMMON_TEXT.DELETE)}
          </Button>
        ),
        width: 136,
        isFixedRight: true,
        cellProps: { className: 'text-center' },
        cellBodyProps: { className: 'text-center' },
        className: 'td-sm'
      }
    ];
  }, [handleDeleteRequest, t]);

  const cleanSortSearch = useCallback(() => {
    batch(() => {
      dispatch(queueAndRoleMappingActions.onChangeTextSearch(''));
      dispatch(
        queueAndRoleMappingActions.onChangeSort({
          id: 'queueName',
          order: undefined
        })
      );
    });
  }, [dispatch]);

  const handleClearAndReset = useCallback(() => {
    cleanSortSearch();
    onPageSizeChange({ page: 1, size: 10 });
  }, [cleanSortSearch, onPageSizeChange]);

  const handleSort = (sort: SortType) => {
    dispatch(queueAndRoleMappingActions.onChangeSort(sort));
  };

  useEffect(() => {
    cleanSortSearch();
    setCurrentPage(1);
    setCurrentPageSize(10);
  }, [cleanSortSearch, setCurrentPage, setCurrentPageSize, selectedRole]);

  if (isLoading) return <div className="vh-50" />;

  if (isError)
    return (
      <>
        <Header className="mt-16" />
        <FailedApiReload
          id="queue-and-role-mapping__request-failed"
          className="mt-80"
          onReload={handleReload}
          dataTestId={`${testId}_failedApi`}
        />
      </>
    );

  return (
    <>
      <SearchConfig
        onClearAndReset={handleClearAndReset}
        onPageChange={onPageChange}
        currentPage={currentPage}
      />
      {isSearchNoData ? (
        <ClearAndReset
          className="mt-24 mb-16"
          onClearAndReset={handleClearAndReset}
        />
      ) : (
        <div className="mt-16">
          <Grid
            id="queue-and-role-mapping__grid-data"
            ref={gridRefs}
            onSortChange={handleSort}
            dataItemKey={'id'}
            columns={dataColumn as unknown as ColumnType[]}
            data={gridData}
            sortBy={[sortBy]}
            dataTestId={`${testId}_grid`}
          />
          {showPagination && (
            <div className="pt-16">
              <Pagination
                totalItem={total}
                pageSize={pageSize}
                pageNumber={currentPage}
                pageSizeValue={currentPageSize}
                compact
                onChangePage={onPageChange}
                onChangePageSize={onPageSizeChange}
                dataTestId={`${testId}_pagination`}
              />
            </div>
          )}
        </div>
      )}
    </>
  );
};

export default QueueNameGrid;
