import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi } from 'app/test-utils/mocks/mockProperty';
import { AxiosRequestConfig } from 'axios';
import { queueAndRoleMappingService } from './queueAndRoleMappingService';

jest.mock('axios', () => {
  const axios = jest.requireActual('axios');
  const mockAction = (params?: AxiosRequestConfig) => {
    return Promise.resolve({ data: params });
  };
  const axiosMock = (params?: AxiosRequestConfig) => mockAction(params);
  axiosMock.get = (params?: AxiosRequestConfig) => mockAction(params);
  axiosMock.create = axios.create;
  return {
    ...axios,
    __esModule: true,
    default: axiosMock,
    defaults: axiosMock
  };
});

describe('Test queueAndRoleMappingService', () => {
  it('getQueueAndRoleMapping', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    queueAndRoleMappingService.getQueueAndRoleMapping({
      common: {
        privileges: ['string'],
        app: 'string',
        org: 'string'
      },
      selectRole: ['string'],
      selectCspa: ['string']
    });

    expect(mockService).toBeCalled();
  });

  it('getQueueAndRoleMapping', () => {
    mockAppConfigApi.setup();

    const mockService = mockApiServices.post();

    queueAndRoleMappingService.getQueueAndRoleMapping({
      common: {
        privileges: ['string'],
        app: 'string',
        org: 'string'
      },
      selectRole: ['string'],
      selectCspa: ['string']
    });

    expect(mockService).toBeCalled();
  });

  it('getQueueNameList', async () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    queueAndRoleMappingService.getQueueNameList({
      common: {
        clientNumber: 'string',
        system: 'string',
        prin: 'string',
        agent: 'string',
        app: 'string',
        org: 'string'
      },
      fetchSize: '500'
    });

    expect(mockService).toBeCalled();
  });

  it('getQueueNameList', async () => {
    mockAppConfigApi.setup();

    const mockService = mockApiServices.post();

    queueAndRoleMappingService.getQueueNameList({
      common: {
        clientNumber: 'string',
        system: 'string',
        prin: 'string',
        agent: 'string',
        app: 'string',
        org: 'string'
      },
      fetchSize: '500'
    });

    expect(mockService).toBeCalled();
  });

  it('getUserRoleList', async () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    queueAndRoleMappingService.getUserRoleList();

    expect(mockService).toBeCalled();
  });

  it('getUserRoleList', async () => {
    mockAppConfigApi.setup();

    const mockService = mockApiServices.post();

    queueAndRoleMappingService.getUserRoleList();

    expect(mockService).toBeCalled();
  });

  it('addQueueAndRoleMapping', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    queueAndRoleMappingService.addQueueAndRoleMapping({
      common: {
        clientNumber: 'string',
        system: 'string',
        prin: 'string',
        agent: 'string',
        app: 'string',
        org: 'string'
      },
      roleEntitlementsRequest: {
        roleEntitlements: []
      }
    });

    expect(mockService).toBeCalled();
  });

  it('addQueueAndRoleMapping', () => {
    mockAppConfigApi.setup();

    const mockService = mockApiServices.post();

    queueAndRoleMappingService.addQueueAndRoleMapping({
      common: {
        clientNumber: 'string',
        system: 'string',
        prin: 'string',
        agent: 'string',
        app: 'string',
        org: 'string'
      },
      roleEntitlementsRequest: {
        roleEntitlements: []
      }
    });

    expect(mockService).toBeCalled();
  });

  it('deleteQueueAndRoleMapping', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    queueAndRoleMappingService.deleteQueueAndRoleMapping({
      common: {
        clientNumber: 'string',
        system: 'string',
        prin: 'string',
        agent: 'string',
        app: 'string',
        org: 'string'
      },
      roleEntitlementsRequest: {
        roleEntitlements: []
      }
    });

    expect(mockService).toBeCalled();
  });

  it('deleteQueueAndRoleMapping', () => {
    mockAppConfigApi.setup();

    const mockService = mockApiServices.post();

    queueAndRoleMappingService.deleteQueueAndRoleMapping({
      common: {
        clientNumber: 'string',
        system: 'string',
        prin: 'string',
        agent: 'string',
        app: 'string',
        org: 'string'
      },
      roleEntitlementsRequest: {
        roleEntitlements: []
      }
    });

    expect(mockService).toBeCalled();
  });
});
