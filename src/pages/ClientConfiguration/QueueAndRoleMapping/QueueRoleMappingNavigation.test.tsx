import React from 'react';

// Components
import QueueRoleMappingNavigation from './QueueRoleMappingNavigation';

// Utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

// Const
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

const spyActionsTab = mockActionCreator(actionsTab);

describe('Test QueueRoleMappingNavigation', () => {
  it('should render QueueRoleMappingNavigation with noData', async () => {
    const addTabMock = spyActionsTab('addTab');

    renderMockStore(<QueueRoleMappingNavigation />);

    userEvent.click(
      await screen.findByTestId('header_queue_role_mapping_title')
    );

    expect(addTabMock).toBeCalledWith({
      iconName: 'file',
      id: 'queue-role-mapping',
      storeId: 'queue-role-mapping',
      tabType: 'queueRoleMapping',
      title: 'txt_queue_and_role_mapping'
    });
  });
});
