import React from 'react';

// redux
import { useDispatch, useSelector } from 'react-redux';
import {
  selectModalLoading,
  selectDeleteModalOpen,
  selectCurrentRecord
} from './_redux/selectors';
import { queueAndRoleMappingActions } from './_redux/reducers';

// components
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_QUEUE_AND_ROLE_MAPPING } from './constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';

export const DeleteQueueAndRoleMapping: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const isLoading = useSelector(selectModalLoading);
  const isOpened = useSelector(selectDeleteModalOpen);
  const currentRecord = useSelector(selectCurrentRecord);
  const testId = 'clientConfig_queueAndRoleMapping_deleteModal';

  const handleClose = () => {
    dispatch(
      queueAndRoleMappingActions.deleteQueueAndRoleMappingRequest({
        queueAndRole: {}
      })
    );
  };

  const handleDelete = () => {
    dispatch(queueAndRoleMappingActions.triggerDeleteQueueAndRoleMapping());
  };

  return (
    <Modal
      id="queue-and-role-mapping__delete-modal"
      xs
      loading={isLoading}
      show={isOpened}
      dataTestId={testId}
    >
      <ModalHeader
        border
        closeButton
        onHide={handleClose}
        dataTestId={`${testId}_header`}
      >
        <ModalTitle dataTestId={`${testId}_title`}>
          {t(I18N_QUEUE_AND_ROLE_MAPPING.CONFIRM_DELETE_QUEUE_AND_ROLE_MAPPING)}
        </ModalTitle>
      </ModalHeader>
      <ModalBody dataTestId={`${testId}_modal`}>
        <p data-testid={genAmtId(testId, 'description', '')}>
          {t(
            I18N_QUEUE_AND_ROLE_MAPPING.CONFIRM_DELETE_QUEUE_AND_ROLE_MAPPING_CONTENT
          )}
        </p>
        <p className="mt-16" data-testid={genAmtId(testId, 'queueName', '')}>
          {`${t(I18N_COMMON_TEXT.QUEUE_NAME)}:`}{' '}
          <strong id="queue-and-role-mapping__delete-modal__queue-name">
            {currentRecord?.queueName}
          </strong>
        </p>
      </ModalBody>
      <ModalFooter dataTestId={`${testId}_footer`}>
        <Button
          id="queue-and-role-mapping__delete-modal__cancel-btn"
          variant="secondary"
          onClick={handleClose}
          dataTestId={`${testId}_cancelBtn`}
        >
          {t(I18N_COMMON_TEXT.CANCEL)}
        </Button>
        <Button
          id="queue-and-role-mapping__delete-modal__delete-btn"
          variant="danger"
          onClick={handleDelete}
          dataTestId={`${testId}_deleteBtn`}
        >
          {t(I18N_COMMON_TEXT.DELETE)}
        </Button>
      </ModalFooter>
    </Modal>
  );
};
