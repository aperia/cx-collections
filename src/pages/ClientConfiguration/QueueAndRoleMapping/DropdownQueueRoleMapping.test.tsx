import React from 'react';

// Components
import DropdownQueueRoleMapping from './DropdownQueueRoleMapping';

// Utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

// Const
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { queueAndRoleMappingActions } from './_redux/reducers';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

const spyQueueAndRoleMapping = mockActionCreator(queueAndRoleMappingActions);

const initialState: Partial<RootState> = {
  clientConfigQueueAndRoleMapping: {
    loading: false,
    loadingModal: false,
    loadingQueueName: false,
    queueAndRoleList: [],
    queueAndRole: {},
    error: '',
    errorModal: '',
    errorQueueName: '',
    isOpenModal: false,
    isOpenModalDelete: true,
    sortBy: { id: 'queueName', order: undefined },
    queueNameList: [],
    userRoleList: [
      {
        value: 'ADMIN',
        description: 'admin'
      },
      {
        value: 'CXCOL1',
        description: 'xcol1'
      }
    ],
    selectedUserRole: undefined,
    loadingUserRoleList: false,
    textSearch: ''
  }
};

describe('Test DropdownQueueRoleMapping', () => {
  it('trigger change text DropdownQueueRoleMapping', async () => {
    const changeUserRoleMock = spyQueueAndRoleMapping('onChangeUserRole');
    const getQueueAndRoleMappingMock = spyQueueAndRoleMapping(
      'getQueueAndRoleMapping'
    );
    const getQueueNameListMock = spyQueueAndRoleMapping('getQueueNameList');

    renderMockStore(<DropdownQueueRoleMapping />, {
      initialState
    });

    userEvent.click(await screen.findByText('txt_user_role'));
    userEvent.click(
      await screen.findByTestId(
        'admin-queueUserRoles_user-role_dropdown-base-item_text'
      )
    );

    expect(changeUserRoleMock).toBeCalledWith({
      value: 'ADMIN',
      description: 'admin'
    });
    expect(getQueueAndRoleMappingMock).toBeCalled();
    expect(getQueueNameListMock).toBeCalled();
  });
});
