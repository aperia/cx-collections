import React from 'react';
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import { DeleteQueueAndRoleMapping } from './ModalDelete';
import { fireEvent, screen } from '@testing-library/react';
import { queueAndRoleMappingActions } from './_redux/reducers';

const queueAndRoleMappingActionsSpy = mockActionCreator(
  queueAndRoleMappingActions
);

describe('Test DeleteQueueAndRoleMapping component', () => {
  it('render component', () => {
    const { wrapper } = renderMockStoreId(<DeleteQueueAndRoleMapping />);

    expect(wrapper.container.innerHTML).toEqual('');
  });

  it('handleClose', () => {
    const deleteQueueAndRoleMappingRequest = queueAndRoleMappingActionsSpy(
      'deleteQueueAndRoleMappingRequest'
    );

    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(<DeleteQueueAndRoleMapping />, {
      initialState: {
        clientConfigQueueAndRoleMapping: {
          loading: false,
          loadingModal: false,
          loadingQueueName: false,
          queueAndRole: { queueName: 'queueName' },
          queueAndRoleList: [],
          error: '',
          errorModal: '',
          errorQueueName: '',
          isOpenModal: false,
          isOpenModalDelete: true,
          sortBy: { id: 'queueName', order: 'desc' },
          queueNameList: [],
          userRoleList: [],
          loadingUserRoleList: false,
          textSearch: ''
        }
      }
    });
    jest.runAllTimers();

    const cancelBtn = wrapper.baseElement.querySelector(
      'i[class="icon icon-close"]'
    );
    fireEvent.click(cancelBtn!);

    expect(deleteQueueAndRoleMappingRequest).toBeCalled();
  });

  it('handleDelete', () => {
    const triggerDeleteQueueAndRoleMapping = queueAndRoleMappingActionsSpy(
      'triggerDeleteQueueAndRoleMapping'
    );

    jest.useFakeTimers();
    renderMockStoreId(<DeleteQueueAndRoleMapping />, {
      initialState: {
        clientConfigQueueAndRoleMapping: {
          loading: false,
          loadingModal: false,
          loadingQueueName: false,
          queueAndRole: { queueName: 'queueName' },
          queueAndRoleList: [],
          error: '',
          errorModal: '',
          errorQueueName: '',
          isOpenModal: false,
          isOpenModalDelete: true,
          sortBy: { id: 'queueName', order: 'desc' },
          queueNameList: [],
          userRoleList: [],
          loadingUserRoleList: false,
          textSearch: ''
        }
      }
    });
    jest.runAllTimers();

    const deleteBtn = screen.getByText('txt_delete');
    fireEvent.click(deleteBtn!);

    expect(triggerDeleteQueueAndRoleMapping).toBeCalled();
  });
});
