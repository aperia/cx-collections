import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// helpers
import { mappingDataFromArray } from 'app/helpers';
import { orderBy } from 'app/_libraries/_dls/lodash';

// service
import { queueAndRoleMappingService } from '../queueAndRoleMappingService';

// constant
import { QUEUE_GRID_COLUMN_NAME } from '../constants';
import { QUEUE_MAPPING_COMPONENT } from 'pages/ClientConfiguration/constants';

// types
import { QueueAndRoleMappingItem, QueueAndRoleMappingState } from '../types';
import { EMPTY_STRING } from 'app/constants';

export const getQueueAndRoleMapping = createAsyncThunk<
  QueueAndRoleMappingItem[],
  undefined,
  ThunkAPIConfig
>(
  'clientConfigQueueAndRoleMapping/getQueueAndRoleMapping',
  async (args, thunkAPI) => {
    const { clientConfigQueueAndRoleMapping, accessConfig, mapping } =
      thunkAPI.getState();
    const { selectedUserRole } = clientConfigQueueAndRoleMapping;
    const { commonConfig } = window.appConfig || {};

    const requestData = {
      common: {
        privileges: commonConfig?.privileges || EMPTY_STRING,
        app: commonConfig?.app || EMPTY_STRING,
        org: commonConfig?.org || EMPTY_STRING
      },
      selectRole: [selectedUserRole?.value || EMPTY_STRING],
      selectCspa: [accessConfig?.cspaList?.[0]?.cspa || EMPTY_STRING],
      selectComponent: [QUEUE_MAPPING_COMPONENT]
    };

    const { data } = await queueAndRoleMappingService.getQueueAndRoleMapping(
      requestData
    );

    const queueAndRoleModel = mapping?.data?.clientConfigQueueAndRole || {};

    let queuesList: QueueAndRoleMappingItem[] = [];
    if (
      data?.roleEntitlements?.[0].cspas?.[0].components[0].entitlements?.queues
    ) {
      queuesList = queuesList.concat(
        data?.roleEntitlements?.[0].cspas?.[0].components[0].entitlements
          ?.queues
      );
    }

    const dataMapped = mappingDataFromArray<QueueAndRoleMappingItem>(
      queuesList,
      queueAndRoleModel
    );

    return orderBy(dataMapped, QUEUE_GRID_COLUMN_NAME.QUEUE_NAME, 'asc');
  }
);

export const getQueueAndRoleMappingBuilder = (
  builder: ActionReducerMapBuilder<QueueAndRoleMappingState>
) => {
  builder
    .addCase(getQueueAndRoleMapping.pending, draftState => {
      draftState.loading = true;
      draftState.queueAndRoleList = [];
    })
    .addCase(getQueueAndRoleMapping.fulfilled, (draftState, action) => {
      draftState.loading = false;
      draftState.queueAndRoleList = action.payload;
    })
    .addCase(getQueueAndRoleMapping.rejected, (draftState, action) => {
      draftState.loading = false;
      draftState.error = action.error.message;
    });
};
