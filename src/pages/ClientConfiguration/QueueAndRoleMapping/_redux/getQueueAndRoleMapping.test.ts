import { responseDefault } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { queueAndRoleMappingService } from './../queueAndRoleMappingService';
import { getQueueAndRoleMapping } from './getQueueAndRoleMapping';

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    org: 'org',
    app: 'app'
  }
};

describe('Test getQueueAndRoleMapping', () => {
  it('getQueueAndRoleMapping > fulfilled', async () => {
    const store = createStore(rootReducer, {
      accessConfig: {
        cspaList: [
          {
            cspa: '9999-1000-2000-3000',
            active: true,
            description: 'default CSPA for AAAA1114'
          }
        ]
      }
    });

    jest
      .spyOn(queueAndRoleMappingService, 'getQueueAndRoleMapping')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          roleEntitlements: [
            {
              role: 'CXCOL1',
              cspas: [
                {
                  components: [
                    {
                      component: 'queue-mapping',
                      entitlements: {
                        queues: [
                          {
                            queueId: '162747799591',
                            queueName: 'FDRT-DEFAULT'
                          },
                          { queueId: '163275966685', queueName: 'chargedOff' },
                          {
                            queueId: '163281945679',
                            queueName: 'earlyCallingQueue'
                          }
                        ]
                      }
                    }
                  ],
                  cspa: '8997-$ALL-$ALL-$ALL',
                  description: 'default CSPA for AAAA1114',
                  active: true
                }
              ]
            }
          ]
        }
      });

    const result = await getQueueAndRoleMapping()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(getQueueAndRoleMapping.fulfilled.type);
  });

  it('getQueueAndRoleMapping > fulfilled 2', async () => {
    const store = createStore(rootReducer, {
      accessConfig: {
        cspaList: [
          {
            cspa: '9999-1000-2000-3000',
            active: true,
            description: 'default CSPA for AAAA1114'
          }
        ]
      }
    });

    jest
      .spyOn(queueAndRoleMappingService, 'getQueueAndRoleMapping')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          roleEntitlements: [
            {
              role: 'XCOL1'
            }
          ]
        }
      });

    const result = await getQueueAndRoleMapping()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(getQueueAndRoleMapping.fulfilled.type);
  });

  it('getQueueAndRoleMapping > app org empty', async () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: '',
        app: ''
      }
    };
    const store = createStore(rootReducer, {});

    jest
      .spyOn(queueAndRoleMappingService, 'getQueueAndRoleMapping')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          roleEntitlements: [
            {
              role: 'XCOL1'
            }
          ]
        }
      });

    const result = await getQueueAndRoleMapping()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(getQueueAndRoleMapping.fulfilled.type);
  });

  it('getQueueAndRoleMapping > rejected', async () => {
    window.appConfig = undefined as any;
    const store = createStore(rootReducer, {});

    jest
      .spyOn(queueAndRoleMappingService, 'getQueueAndRoleMapping')
      .mockRejectedValue({ ...responseDefault });

    const result = await getQueueAndRoleMapping()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(getQueueAndRoleMapping.rejected.type);
  });
});
