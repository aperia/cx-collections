import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// helpers
import orderBy from 'lodash.orderby';

// Service
import { queueAndRoleMappingService } from '../queueAndRoleMappingService';

// Type
import { QueueAndRoleMappingState, QueueAndRoleMappingItem } from '../types';

// constant
import { QUEUE_GRID_COLUMN_NAME } from '../constants';
import { EMPTY_STRING } from 'app/constants';

export const getQueueNameList = createAsyncThunk<
  QueueAndRoleMappingItem[],
  undefined,
  ThunkAPIConfig
>(
  'clientConfigQueueAndRoleMapping/getQueueNameList',
  async (args, thunkAPI) => {
    const { getState } = thunkAPI;
    const { accessConfig } = getState();
    const { commonConfig } = window.appConfig || {};

    const [clientNumber = '', system = '$ALL', prin = '$ALL', agent = '$ALL'] =
      accessConfig?.cspaList?.[0]?.cspa?.split('-') || [];
    const { data } = await queueAndRoleMappingService.getQueueNameList({
      common: {
        user: commonConfig?.user || EMPTY_STRING,
        clientNumber,
        system,
        prin,
        agent,
        app: commonConfig?.app || EMPTY_STRING,
        org: commonConfig?.org || EMPTY_STRING,
        userPrivileges: {
          cspa: [accessConfig?.cspaList?.[0]?.cspa || EMPTY_STRING],
          groups: ['admin']
        }
      },
      fetchSize: null,
      pagingKey: null
    });

    return orderBy(
      data?.queueDetails,
      QUEUE_GRID_COLUMN_NAME.QUEUE_NAME,
      'asc'
    );
  }
);

export const getQueueNameListBuilder = (
  builder: ActionReducerMapBuilder<QueueAndRoleMappingState>
) => {
  builder
    .addCase(getQueueNameList.pending, draftState => {
      draftState.loadingQueueName = true;
      draftState.queueNameList = [];
    })
    .addCase(getQueueNameList.fulfilled, (draftState, action) => {
      draftState.loadingQueueName = false;
      draftState.queueNameList = action.payload;
    })
    .addCase(getQueueNameList.rejected, draftState => {
      draftState.loadingQueueName = false;
      draftState.queueNameList = [];
    });
};
