import { createSelector } from '@reduxjs/toolkit';

// helpers
import orderBy from 'lodash.orderby';
import { isArrayHasValue } from 'app/helpers';
import { searchQueueName } from 'pages/ClientConfiguration/helpers';

// types
import { QueueAndRoleMappingItem, UserRoleItem } from './../types';
import { SortType } from 'app/_libraries/_dls/components';

// constant
import { QUEUE_GRID_COLUMN_NAME } from '../constants';

const getQueueAndRoleMappingList = (state: RootState) => {
  return state.clientConfigQueueAndRoleMapping;
};

const getSortBy = (states: RootState): SortType => {
  return (
    states.clientConfigQueueAndRoleMapping?.sortBy || {
      id: 'queueName',
      order: undefined
    }
  );
};

export const selectLoading = createSelector(
  getQueueAndRoleMappingList,
  data => data?.loading || false
);

export const selectModalLoading = createSelector(
  getQueueAndRoleMappingList,
  data => data?.loadingModal || false
);

export const selectSortBy = createSelector(getSortBy, data => data);
export const selectGridData = createSelector(
  [getQueueAndRoleMappingList, getSortBy],
  (data, sortBy) => {
    const sorted = orderBy(
      data?.queueAndRoleList,
      [
        (item: any) => item[sortBy.id]?.toLowerCase(),
        item => item.queueName?.toLowerCase()
      ],
      [sortBy.order || false, 'asc']
    );

    return sorted;
  }
);

export const selectError = createSelector(
  getQueueAndRoleMappingList,
  data => data?.error || ''
);

const getDeleteModalOpen = (state: RootState): boolean => {
  return state.clientConfigQueueAndRoleMapping?.isOpenModalDelete || false;
};

export const selectDeleteModalOpen = createSelector(
  getDeleteModalOpen,
  (data: boolean) => data
);

export const selectCurrentRecord = createSelector(
  getQueueAndRoleMappingList,
  data => data?.queueAndRole
);

const getModalOpen = (state: RootState): boolean => {
  return state.clientConfigQueueAndRoleMapping?.isOpenModal || false;
};

export const selectModalOpen = createSelector(
  getModalOpen,
  (data: boolean) => data || false
);

const getQueueNameList = (states: RootState) => {
  return states.clientConfigQueueAndRoleMapping?.queueNameList || [];
};

export const selectQueueNameList = createSelector(
  getQueueNameList,
  (data: QueueAndRoleMappingItem[]) =>
    orderBy(data, QUEUE_GRID_COLUMN_NAME.QUEUE_NAME, 'asc')
);

const getUserRoleList = (states: RootState) => {
  return states.clientConfigQueueAndRoleMapping?.userRoleList || [];
};

export const selectUserRoleList = createSelector(
  getUserRoleList,
  (data: UserRoleItem[]) => orderBy(data, 'description', 'asc')
);

export const selectQueueNameLoading = createSelector(
  getQueueAndRoleMappingList,
  data => data?.loadingQueueName || false
);

export const selectUserRoleListLoading = createSelector(
  getQueueAndRoleMappingList,
  data => data?.loadingUserRoleList || false
);

export const selectedUserRole = createSelector(
  getQueueAndRoleMappingList,
  data => data?.selectedUserRole
);

const getTextSearch = (state: RootState): string => {
  return state.clientConfigQueueAndRoleMapping?.textSearch || '';
};

export const selectTextSearch = createSelector(
  getTextSearch,
  (textSearch: string) => textSearch
);

export const selectIsSearchNoData = createSelector(
  [getQueueAndRoleMappingList, getSortBy, getTextSearch],
  (data, sortBy, textSearch: string) => {
    const sorted = orderBy(
      data?.queueAndRoleList,
      [
        (item: any) => item[sortBy.id]?.toLowerCase(),
        item => item.queueName?.toLowerCase()
      ],
      [sortBy.order || false, 'asc']
    );
    return !isArrayHasValue(searchQueueName(sorted, textSearch)) && textSearch
      ? true
      : false;
  }
);

export const selectDisplayClearAndReset = createSelector(
  getQueueAndRoleMappingList,
  getTextSearch,
  (data, textSearch: string) => {
    return isArrayHasValue(
      searchQueueName(data?.queueAndRoleList || [], textSearch)
    ) && textSearch
      ? true
      : false;
  }
);

export const selectGridDataWithSearch = createSelector(
  [getQueueAndRoleMappingList, getSortBy, getTextSearch],
  (data, sortBy, textSearch) => {
    return orderBy(
      searchQueueName(data?.queueAndRoleList || [], textSearch),
      [
        (item: any) => item[sortBy.id]?.toLowerCase(),
        item => item.queueName?.toLowerCase()
      ],
      [sortBy.order || false, 'asc']
    );
  }
);
