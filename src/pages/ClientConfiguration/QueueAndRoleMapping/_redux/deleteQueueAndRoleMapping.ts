import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { queueAndRoleMappingActions } from './reducers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

// service
import { queueAndRoleMappingService } from '../queueAndRoleMappingService';

// constant
import { I18N_QUEUE_AND_ROLE_MAPPING } from '../constants';

// type
import { QueueAndRoleMappingState } from '../types';
import { EMPTY_STRING } from 'app/constants';
import { QUEUE_MAPPING_COMPONENT } from 'pages/ClientConfiguration/constants';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';

export const deleteQueueAndRoleMapping = createAsyncThunk<
  undefined,
  undefined,
  ThunkAPIConfig
>(
  'clientConfigQueueAndRoleMapping/deleteQueueAndRoleMapping',
  async (args, thunkAPI) => {
    const { clientConfigQueueAndRoleMapping, accessConfig } =
      thunkAPI.getState();
    const { queueAndRoleList, selectedUserRole, queueAndRole } =
      clientConfigQueueAndRoleMapping;
    const { commonConfig } = window.appConfig || {};
    const { privileges } = window.appConfig?.commonConfig || {};

    const [clientNumber = '', system = '$ALL', prin = '$ALL', agent = '$ALL'] =
      accessConfig?.cspaList?.[0]?.cspa?.split('-') || [];

    const requestData = {
      common: {
        privileges,
        clientNumber,
        system,
        prin,
        agent,
        app: commonConfig?.app || EMPTY_STRING,
        org: commonConfig?.org || EMPTY_STRING
      },
      roleEntitlementsRequest: {
        roleEntitlements: [
          {
            role: selectedUserRole?.value || '',
            cspas: [
              {
                cspa: accessConfig?.cspaList?.[0]?.cspa || EMPTY_STRING,
                description:
                  accessConfig?.cspaList?.[0]?.description || EMPTY_STRING,
                active: accessConfig?.cspaList?.[0]?.active || EMPTY_STRING,
                components: [
                  {
                    component: QUEUE_MAPPING_COMPONENT,
                    entitlements: {
                      queues: queueAndRoleList.filter(
                        queueRole => queueRole.queueId !== queueAndRole?.queueId
                      )
                    }
                  }
                ]
              }
            ]
          }
        ]
      }
    };

    const { data } = await queueAndRoleMappingService.deleteQueueAndRoleMapping(
      requestData
    );

    return data;
  }
);

export const triggerDeleteQueueAndRoleMapping = createAsyncThunk<
  unknown,
  undefined,
  ThunkAPIConfig
>('triggerDeleteQueueAndRoleMapping', async (args, thunkAPI) => {
  const { dispatch } = thunkAPI;
  const response = await dispatch(deleteQueueAndRoleMapping());

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_QUEUE_AND_ROLE_MAPPING.QUEUE_NAME_DELETED
        })
      );
      dispatch(queueAndRoleMappingActions.getQueueAndRoleMapping());
      dispatch(queueAndRoleMappingActions.getQueueNameList());
    });
  }

  if (isRejected(response)) {
    dispatch(
      apiErrorNotificationAction.updateThunkApiError({
        storeId: API_ERROR_STORE_ID.QUEUE_ROLE_MAPPING_MODAL,
        forSection: 'inModalBody',
        rejectData: {
          ...response,
          payload: response.payload?.response
        }
      })
    );
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_QUEUE_AND_ROLE_MAPPING.QUEUE_NAME_FAILED_TO_DELETED
      })
    );
  }
});

export const deleteQueueAndRoleMappingBuilder = (
  builder: ActionReducerMapBuilder<QueueAndRoleMappingState>
) => {
  builder
    .addCase(deleteQueueAndRoleMapping.pending, draftState => {
      draftState.loadingModal = true;
    })
    .addCase(deleteQueueAndRoleMapping.fulfilled, draftState => {
      draftState.loadingModal = false;
      draftState.isOpenModalDelete = false;
    })
    .addCase(deleteQueueAndRoleMapping.rejected, (draftState, action) => {
      draftState.loadingModal = false;
      draftState.errorModal = action.error.message;
    });
};
