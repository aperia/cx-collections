import { selectorWrapper } from 'app/test-utils';
import * as selectors from './selectors';

describe('Test QueueAndRoleMapping selectors', () => {
  const store: Partial<RootState> = {
    clientConfigQueueAndRoleMapping: {
      textSearch: 'textSearch',
      selectedUserRole: { value: 'code', description: 'text' },
      loading: true,
      loadingModal: true,
      loadingQueueName: true,
      queueAndRole: { queueName: 'queueName' },
      queueAndRoleList: [{ queueName: 'queueName' }],
      error: 'error',
      errorModal: '',
      errorQueueName: '',
      isOpenModal: true,
      isOpenModalDelete: true,
      sortBy: { id: 'queueName', order: 'asc' },
      queueNameList: [
        {
          queueId: 'queueIdA',
          queueName: 'queueNameA',
          queueGroupIdOwner: 'queueGroupIdOwner',
          accessLevel: 'accessLevel'
        },
        {
          queueId: 'queueIdB',
          queueName: 'queueNameB',
          queueGroupIdOwner: 'queueGroupIdOwner',
          accessLevel: 'accessLevel'
        }
      ],
      userRoleList: [
        { value: 'CXCOL1', description: 'CXCOL1' },
        { value: 'CXCOL2', description: 'CXCOL2' }
      ],
      loadingUserRoleList: false
    }
  };

  it('selectLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(selectors.selectLoading);

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectModalLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectModalLoading
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectError', () => {
    const { data, emptyData } = selectorWrapper(store)(selectors.selectError);

    expect(data).toEqual('error');
    expect(emptyData).toEqual('');
  });

  it('selectDeleteModalOpen', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectDeleteModalOpen
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectCurrentRecord', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectCurrentRecord
    );

    expect(data).toEqual({ queueName: 'queueName' });
    expect(emptyData).toEqual(undefined);
  });

  it('selectModalOpen', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectModalOpen
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectQueueNameList', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectQueueNameList
    );

    expect(data).toEqual([
      {
        queueId: 'queueIdA',
        queueName: 'queueNameA',
        queueGroupIdOwner: 'queueGroupIdOwner',
        accessLevel: 'accessLevel'
      },
      {
        queueId: 'queueIdB',
        queueName: 'queueNameB',
        queueGroupIdOwner: 'queueGroupIdOwner',
        accessLevel: 'accessLevel'
      }
    ]);
    expect(emptyData).toEqual([]);
  });

  it('selectQueueNameList', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectUserRoleList
    );

    expect(data).toEqual([
      { value: 'CXCOL1', description: 'CXCOL1' },
      { value: 'CXCOL2', description: 'CXCOL2' }
    ]);
    expect(emptyData).toEqual([]);
  });

  it('selectSortBy', () => {
    const { data, emptyData } = selectorWrapper(store)(selectors.selectSortBy);

    expect(data).toEqual({ id: 'queueName', order: 'asc' });
    expect(emptyData).toEqual({ id: 'queueName', order: undefined });
  });

  it('selectGridData', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectGridData
    );

    expect(data).toEqual([
      {
        queueName: 'queueName'
      }
    ]);
    expect(emptyData).toEqual([]);
  });

  it('selectQueueNameLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectQueueNameLoading
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectUserRoleListLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectUserRoleListLoading
    );

    expect(data).toEqual(false);
    expect(emptyData).toEqual(false);
  });

  it('selectDisplayClearAndReset', () => {
    const { data, emptyData } = selectorWrapper({
      clientConfigQueueAndRoleMapping: {
        ...store.clientConfigQueueAndRoleMapping,
        queueAndRoleList: [
          {
            queueId: '160675456170',
            queueName: 'COLX_DEFAULT',
            queueGroupIdOwner: 'FD - Admin',
            userRoleName: [
              { value: 'CXCOL1', description: 'CXCOL1' },
              { value: 'CXCOL2', description: 'CXCOL2' }
            ],
            accessLevel: 'UPDATE'
          },
          {
            queueId: '001',
            queueName: 'Non Verified Bankrupt',
            queueGroupIdOwner: 'FD - Admin',
            userRoleName: [{ value: 'CXSUP1', description: 'CXSUP1' }],
            accessLevel: 'UPDATE'
          },
          {
            queueId: '002',
            queueName: 'Verified Bankrupt',
            queueGroupIdOwner: 'FD - Admin',
            userRoleName: [
              { value: 'CXCOL2', description: 'CXCOL2' },
              { value: 'CXCOL3', description: 'CXCOL3' },
              { value: 'CXCOL4', description: 'CXCOL4' }
            ],
            accessLevel: 'UPDATE'
          },
          {
            queueId: '003',
            queueName: 'Discharge Bankrupt',
            queueGroupIdOwner: 'FD - Admin',
            userRoleName: [{ value: 'CXADMIN', description: 'CXADMIN' }],
            accessLevel: 'UPDATE'
          },
          {
            queueId: '004',
            queueName: 'Dismissed BK',
            queueGroupIdOwner: 'FD - Admin',
            userRoleName: [{ value: 'CXMGR1', description: 'CXMGR1' }],
            accessLevel: 'UPDATE'
          },
          {
            queueId: '005',
            queueName: 'Cease and Desist',
            queueGroupIdOwner: 'FD - Admin',
            userRoleName: [{ value: 'CXSUP1', description: 'CXSUP1' }],
            accessLevel: 'UPDATE'
          },
          {
            queueId: '006',
            queueName: 'Customer Represented by Attorney',
            queueGroupIdOwner: 'FD - Admin',
            userRoleName: [
              { value: 'CXCOL1', description: 'CXCOL1' },
              { value: 'CXCOL2', description: 'CXCOL2' }
            ],
            accessLevel: 'UPDATE'
          },
          {
            queueId: '007',
            queueName: 'Pending Deceased',
            queueGroupIdOwner: 'FD - Admin',
            userRoleName: [{ value: 'CXADMIN', description: 'CXADMIN' }],
            accessLevel: 'UPDATE'
          },
          {
            queueId: '008',
            queueName: 'Temporary Reform',
            queueGroupIdOwner: 'FD - Admin',
            userRoleName: [
              { value: 'CXCOL1', description: 'CXCOL1' },
              { value: 'CXSUP1', description: 'CXSUP1' },
              { value: 'CXMGR1', description: 'CXMGR1' }
            ],
            accessLevel: 'UPDATE'
          },
          {
            queueId: '009',
            queueName: 'Customer Enrolled into Hardship',
            queueGroupIdOwner: 'FD - Admin',
            userRoleName: [{ value: 'CXMGR1', description: 'CXMGR1' }],
            accessLevel: 'UPDATE'
          },
          {
            queueId: '010',
            queueName: 'Non Speak Call Back',
            queueGroupIdOwner: 'FD - Admin',
            userRoleName: [{ value: 'CXCOL5', description: 'CXCOL5' }],
            accessLevel: 'UPDATE'
          },
          {
            queueId: '011',
            queueName: 'Queue Name 11',
            queueGroupIdOwner: 'FD - Admin',
            userRoleName: [
              { value: 'CXCOL1', description: 'CXCOL1' },
              { value: 'CXCOL2', description: 'CXCOL2' }
            ],
            accessLevel: 'UPDATE'
          },
          {
            queueId: '012',
            queueName: 'Queue Name 12',
            queueGroupIdOwner: 'FD - Admin',
            userRoleName: [{ value: 'CXCOL1', description: 'CXCOL1' }],
            accessLevel: 'UPDATE'
          },
          {
            queueId: '013',
            queueName: 'Queue Name 13',
            queueGroupIdOwner: 'FD - Admin',
            userRoleName: [{ value: 'CXCOL2', description: 'CXCOL2' }],
            accessLevel: 'UPDATE'
          },
          {
            queueId: '014',
            queueName: 'Queue Name 14',
            queueGroupIdOwner: 'FD - Admin',
            userRoleName: [
              { value: 'CXCOL1', description: 'CXCOL1' },
              { value: 'CXCOL2', description: 'CXCOL2' },
              { value: 'CXCOL3', description: 'CXCOL3' },
              { value: 'CXCOL4', description: 'CXCOL4' },
              { value: 'CXCOL5', description: 'CXCOL5' },
              { value: 'CXMGR1', description: 'CXMGR1' },
              { value: 'CXMGR2', description: 'CXMGR2' },
              { value: 'CXMGR3', description: 'CXMGR3' },
              { value: 'CXMGR4', description: 'CXMGR4' },
              { value: 'CXMGR5', description: 'CXMGR5' }
            ],
            accessLevel: 'UPDATE'
          }
        ],
        textSearch: 'Queue Name'
      } as any
    })(selectors.selectDisplayClearAndReset);

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectGridDataWithSearch', () => {
    const { data, emptyData } = selectorWrapper({
      clientConfigQueueAndRoleMapping: {
        ...store.clientConfigQueueAndRoleMapping,
        queueAndRoleList: undefined as any
      } as any
    })(selectors.selectGridDataWithSearch);

    expect(data).toEqual([]);
    expect(emptyData).toEqual([]);
  });

  it('selectedUserRole ', () => {
    const { data } = selectorWrapper({
      clientConfigQueueAndRoleMapping: {
        ...store.clientConfigQueueAndRoleMapping,
        queueAndRoleList: undefined as any
      } as any
    })(selectors.selectedUserRole);

    expect(data).toEqual(
      store.clientConfigQueueAndRoleMapping?.selectedUserRole
    );
  });

  it('selectTextSearch', () => {
    const { data } = selectorWrapper({
      clientConfigQueueAndRoleMapping: {
        ...store.clientConfigQueueAndRoleMapping,
        queueAndRoleList: undefined as any
      } as any
    })(selectors.selectTextSearch);

    expect(data).toEqual(store.clientConfigQueueAndRoleMapping?.textSearch);
  });

  it('selectIsSearchNoData return true', () => {
    const { data } = selectorWrapper({
      clientConfigQueueAndRoleMapping: {
        ...store.clientConfigQueueAndRoleMapping
      } as any
    })(selectors.selectIsSearchNoData);

    expect(data).toEqual(true);
  });

  it('selectIsSearchNoData', () => {
    const { data } = selectorWrapper({
      clientConfigQueueAndRoleMapping: {
        ...store.clientConfigQueueAndRoleMapping
      } as any
    })(selectors.selectIsSearchNoData);

    expect(data).toEqual(true);
  });

  it('selectGridDataWithSearch', () => {
    const { data } = selectorWrapper({
      clientConfigQueueAndRoleMapping: {
        ...store.clientConfigQueueAndRoleMapping,
        textSearch: 'COLX_DEFAULT',
        queueAndRoleList: [
          {
            queueId: '160675456170',
            queueName: 'COLX_DEFAULT',
            queueGroupIdOwner: 'FD - Admin',
            userRoleName: [
              { value: 'CXCOL1', description: 'CXCOL1' },
              { value: 'CXCOL2', description: 'CXCOL2' }
            ],
            accessLevel: 'UPDATE'
          }
        ]
      } as any
    })(selectors.selectGridDataWithSearch);

    expect(data).toEqual([
      {
        queueId: '160675456170',
        queueName: 'COLX_DEFAULT',
        queueGroupIdOwner: 'FD - Admin',
        userRoleName: [
          { value: 'CXCOL1', description: 'CXCOL1' },
          { value: 'CXCOL2', description: 'CXCOL2' }
        ],
        accessLevel: 'UPDATE'
      }
    ]);
  });
});
