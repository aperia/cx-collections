import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault
} from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { queueAndRoleMappingService } from './../queueAndRoleMappingService';
import {
  deleteQueueAndRoleMapping,
  triggerDeleteQueueAndRoleMapping
} from './deleteQueueAndRoleMapping';
import { storeId } from 'app/test-utils';

const store = createStore(rootReducer, {
  accountDetail: {
    [storeId]: {
      rawData: {
        systemIdentifier: '123',
        clientIdentifier: '123',
        principalIdentifier: '123',
        agentIdentifier: '123'
      }
    }
  },
  clientConfigQueueAndRoleMapping: {
    loading: true,
    loadingModal: true,
    loadingQueueName: false,
    queueAndRole: undefined,
    queueAndRoleList: [{ queueName: 'queueNameA' }],
    error: 'error',
    errorModal: '',
    errorQueueName: '',
    isOpenModal: true,
    isOpenModalDelete: true,
    sortBy: { id: 'queueName', order: 'desc' },
    queueNameList: [
      {
        queueId: 'queueIdB',
        queueName: 'queueNameB',
        queueGroupIdOwner: 'queueGroupIdOwner',
        accessLevel: 'accessLevel'
      },
      {
        queueId: 'queueIdC',
        queueName: 'queueNameC',
        queueGroupIdOwner: 'queueGroupIdOwner',
        accessLevel: 'accessLevel'
      }
    ],
    userRoleList: [
      { code: 'CXCOL1', text: 'CXCOL1' },
      { code: 'CXCOL2', text: 'CXCOL2' }
    ]
  }
});

const actionsToastSpy = mockActionCreator(actionsToast);

describe('Test deleteQueueAndRoleMapping', () => {
  it('deleteQueueAndRoleMapping > rejected', async () => {
    jest
      .spyOn(queueAndRoleMappingService, 'deleteQueueAndRoleMapping')
      .mockRejectedValue({ ...responseDefault });

    const store = createStore(rootReducer, {
      clientConfigQueueAndRoleMapping: {
        queueAndRole: {
          queueId: '1',
          queueName: 'name'
        }
      }
    });

    const result = await deleteQueueAndRoleMapping()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'clientConfigQueueAndRoleMapping/deleteQueueAndRoleMapping/rejected'
    );
  });

  it('deleteQueueAndRoleMapping > rejected', async () => {
    jest
      .spyOn(queueAndRoleMappingService, 'deleteQueueAndRoleMapping')
      .mockRejectedValue({
        data: null,
        status: 400,
        statusText: '',
        headers: {},
        config: {}
      });

    const result = await deleteQueueAndRoleMapping()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'clientConfigQueueAndRoleMapping/deleteQueueAndRoleMapping/rejected'
    );
  });

  it('deleteQueueAndRoleMapping > fulfilled', async () => {
    const store = createStore(rootReducer, {
      clientConfiguration: {
        x500IdCspaList: {
          data: [
            {
              cspa: '8997-3000-2000-1111',
              description: 'default',
              active: true
            }
          ]
        }
      },
      clientConfigQueueAndRoleMapping: {
        loading: true,
        loadingModal: true,
        loadingQueueName: false,
        queueAndRole: {},
        queueAndRoleList: [{ queueName: 'queueNameA' }],
        error: 'error',
        errorModal: '',
        errorQueueName: '',
        isOpenModal: true,
        isOpenModalDelete: true,
        sortBy: { id: 'queueName', order: 'desc' },
        queueNameList: [
          {
            queueId: 'queueIdB',
            queueName: 'queueNameB',
            queueGroupIdOwner: 'queueGroupIdOwner',
            accessLevel: 'accessLevel'
          },
          {
            queueId: 'queueIdC',
            queueName: 'queueNameC',
            queueGroupIdOwner: 'queueGroupIdOwner',
            accessLevel: 'accessLevel'
          }
        ],
        userRoleList: [
          { code: 'CXCOL1', text: 'CXCOL1' },
          { code: 'CXCOL2', text: 'CXCOL2' }
        ]
      }
    });

    jest
      .spyOn(queueAndRoleMappingService, 'deleteQueueAndRoleMapping')
      .mockResolvedValue({ ...responseDefault });

    const result = await deleteQueueAndRoleMapping()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'clientConfigQueueAndRoleMapping/deleteQueueAndRoleMapping/fulfilled'
    );
  });

  it('triggerDeleteQueueAndRoleMapping > fulfilled', async () => {
    const spy = actionsToastSpy('addToast');

    await triggerDeleteQueueAndRoleMapping()(
      byPassFulfilled(triggerDeleteQueueAndRoleMapping.fulfilled.type, {}),
      store.getState,
      {}
    );

    expect(spy).toBeCalled();
  });

  it('triggerDeleteQueueAndRoleMapping > rejected', async () => {
    const spy = actionsToastSpy('addToast');

    await triggerDeleteQueueAndRoleMapping()(
      byPassRejected(triggerDeleteQueueAndRoleMapping.rejected.type, {}),
      store.getState,
      {}
    );

    expect(spy).toBeCalled();
  });
});
