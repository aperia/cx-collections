import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

// helper
import orderBy from 'lodash.orderby';

// Service
// import { queueAndRoleMappingService } from '../queueAndRoleMappingService';

// Type
import { QueueAndRoleMappingState, UserRoleItem } from '../types';

export const getUserRoleList = createAsyncThunk<
  UserRoleItem[],
  undefined,
  ThunkAPIConfig
>('clientConfigQueueAndRoleMapping/getUserRoleList', async (args, thunkAPI) => {
  // const { data } = await queueAndRoleMappingService.getUserRoleList();
  const { data } = await axios.get('refData/userRoles.json');

  return orderBy(data, 'description', 'asc');
});

export const getUserRoleListBuilder = (
  builder: ActionReducerMapBuilder<QueueAndRoleMappingState>
) => {
  builder
    .addCase(getUserRoleList.pending, draftState => {
      draftState.loadingUserRoleList = true;
      draftState.userRoleList = [];
    })
    .addCase(getUserRoleList.fulfilled, (draftState, action) => {
      draftState.loadingUserRoleList = false;
      draftState.userRoleList = action.payload;
    })
    .addCase(getUserRoleList.rejected, draftState => {
      draftState.loadingUserRoleList = false;
      draftState.userRoleList = [];
    });
};
