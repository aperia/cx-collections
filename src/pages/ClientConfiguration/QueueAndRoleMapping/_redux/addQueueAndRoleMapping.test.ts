import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault
} from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { queueAndRoleMappingService } from './../queueAndRoleMappingService';
import {
  addQueueAndRoleMapping,
  triggerAddQueueAndRoleMapping
} from './addQueueAndRoleMapping';

const actionsToastSpy = mockActionCreator(actionsToast);

describe('Test addQueueAndRoleMapping async thunk', () => {
  it('addQueueAndRoleMapping > fulfilled', async () => {
    const store = createStore(rootReducer, {
      clientConfiguration: {
        x500IdCspaList: {
          data: [
            {
              cspa: '8997-3000-2000-1111',
              description: 'default',
              active: true
            }
          ]
        }
      }
    });

    jest
      .spyOn(queueAndRoleMappingService, 'addQueueAndRoleMapping')
      .mockResolvedValue({ ...responseDefault });

    const result = await addQueueAndRoleMapping({ postData: [] })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual('addQueueAndRoleMapping/fulfilled');
  });

  it('addQueueAndRoleMapping > rejected', async () => {
    const store = createStore(rootReducer, {});

    jest
      .spyOn(queueAndRoleMappingService, 'addQueueAndRoleMapping')
      .mockRejectedValue({ ...responseDefault });

    const result = await addQueueAndRoleMapping({ postData: [] })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual('addQueueAndRoleMapping/rejected');
  });

  it('triggerAddQueueAndRoleMapping > fulfilled', async () => {
    const store = createStore(rootReducer, {});

    const result = await triggerAddQueueAndRoleMapping({
      postData: [{ queueName: 'queueName' }]
    })(
      byPassFulfilled(triggerAddQueueAndRoleMapping.fulfilled.type, {}),
      store.getState,
      {}
    );

    expect(result.type).toEqual('triggerAddQueueAndRoleMapping/fulfilled');
  });

  it('triggerAddQueueAndRoleMapping > rejected', async () => {
    const store = createStore(rootReducer, {});
    const spy = actionsToastSpy('addToast');

    await triggerAddQueueAndRoleMapping({
      postData: [{ queueName: 'queueName' }]
    })(
      byPassRejected(triggerAddQueueAndRoleMapping.rejected.type, {}),
      store.getState,
      {}
    );

    expect(spy).toBeCalled();
  });
});
