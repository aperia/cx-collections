import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// types
import {
  QueueAndRoleMappingItem,
  QueueAndRoleMappingState,
  UserRoleItem
} from '../types';
import { SortType } from 'app/_libraries/_dls/components';

import {
  getQueueAndRoleMapping,
  getQueueAndRoleMappingBuilder
} from './getQueueAndRoleMapping';

import {
  deleteQueueAndRoleMapping,
  triggerDeleteQueueAndRoleMapping,
  deleteQueueAndRoleMappingBuilder
} from './deleteQueueAndRoleMapping';

import {
  addQueueAndRoleMapping,
  addQueueAndRoleMappingBuilder,
  triggerAddQueueAndRoleMapping
} from './addQueueAndRoleMapping';

import { getQueueNameList, getQueueNameListBuilder } from './getQueueNameList';

import { getUserRoleList, getUserRoleListBuilder } from './getUserRoleList';

const initialState: QueueAndRoleMappingState = {
  loading: false,
  loadingModal: false,
  loadingQueueName: false,
  queueAndRoleList: [],
  queueAndRole: undefined,
  error: '',
  errorModal: '',
  errorQueueName: '',
  isOpenModal: false,
  isOpenModalDelete: false,
  sortBy: { id: 'queueName', order: undefined },
  queueNameList: [],
  userRoleList: [],
  selectedUserRole: undefined,
  loadingUserRoleList: false,
  textSearch: ''
};

const { actions, reducer } = createSlice({
  name: 'clientConfigQueueAndRoleMapping',
  initialState,
  reducers: {
    deleteQueueAndRoleMappingRequest: (
      draftState,
      action: PayloadAction<{ queueAndRole?: QueueAndRoleMappingItem }>
    ) => {
      draftState.isOpenModalDelete = !draftState.isOpenModalDelete;
      draftState.queueAndRole = action.payload.queueAndRole;
    },

    cancelModalForm: draftState => {
      draftState.isOpenModal = false;
    },

    openModalForm: draftState => {
      draftState.isOpenModal = true;
    },

    onChangeSort: (draftState, action: PayloadAction<SortType>) => {
      draftState.sortBy = action.payload;
    },

    onChangeUserRole: (
      draftState,
      action: PayloadAction<UserRoleItem | undefined>
    ) => {
      draftState.selectedUserRole = action.payload;
    },

    onChangeTextSearch: (draftState, action: PayloadAction<string>) => {
      return {
        ...draftState,
        textSearch: action.payload
      };
    },

    reset: () => initialState
  },

  extraReducers: builder => {
    getQueueAndRoleMappingBuilder(builder);
    deleteQueueAndRoleMappingBuilder(builder);
    addQueueAndRoleMappingBuilder(builder);
    getQueueNameListBuilder(builder);
    getUserRoleListBuilder(builder);
  }
});

const queueAndRoleMappingActions = {
  ...actions,
  getQueueAndRoleMapping,
  deleteQueueAndRoleMapping,
  triggerDeleteQueueAndRoleMapping,
  addQueueAndRoleMapping,
  triggerAddQueueAndRoleMapping,
  getQueueNameList,
  getUserRoleList
};

export { queueAndRoleMappingActions, reducer };
