import { responseDefault } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { queueAndRoleMappingService } from './../queueAndRoleMappingService';
import { getQueueNameList } from './getQueueNameList';

describe('Test getQueueNameList', () => {
  it('getQueueNameList > fulfilled', async () => {
    const store = createStore(rootReducer, {
      accessConfig: {
        cspaList: [
          {
            cspa: '9999-1000-2000-3000',
            active: true,
            description: 'default CSPA for AAAA1114'
          }
        ]
      }
    });

    jest
      .spyOn(queueAndRoleMappingService, 'getQueueNameList')
      .mockResolvedValue({ ...responseDefault });

    const result = await getQueueNameList()(store.dispatch, store.getState, {});

    expect(result.type).toEqual(getQueueNameList.fulfilled.type);
  });

  it('getQueueNameList > rejected', async () => {
    const store = createStore(rootReducer, {});

    jest
      .spyOn(queueAndRoleMappingService, 'getQueueNameList')
      .mockRejectedValue({ ...responseDefault });

    const result = await getQueueNameList()(store.dispatch, store.getState, {});

    expect(result.type).toEqual(getQueueNameList.rejected.type);
  });
});
