import { QueueAndRoleMappingState } from '../types';
import { queueAndRoleMappingActions, reducer } from './reducers';

describe('Test QueueAndRoleMapping reducers', () => {
  const initialState: QueueAndRoleMappingState = {
    loading: false,
    loadingModal: false,
    loadingQueueName: false,
    queueAndRoleList: [],
    queueAndRole: {},
    error: '',
    errorModal: '',
    errorQueueName: '',
    isOpenModal: false,
    isOpenModalDelete: false,
    sortBy: { id: 'queueName', order: undefined },
    queueNameList: [],
    userRoleList: [],
    loadingUserRoleList: false,
    textSearch: ''
  };

  it('deleteQueueAndRoleMappingRequest', () => {
    const state = reducer(
      initialState,
      queueAndRoleMappingActions.deleteQueueAndRoleMappingRequest({
        queueAndRole: { queueName: 'queueName' }
      })
    );

    expect(state.isOpenModalDelete).toEqual(true);
    expect(state.queueAndRole).toEqual({ queueName: 'queueName' });
  });

  it('cancelModalForm', () => {
    const state = reducer(
      initialState,
      queueAndRoleMappingActions.cancelModalForm()
    );

    expect(state.isOpenModal).toEqual(false);
  });

  it('openModalForm', () => {
    const state = reducer(
      initialState,
      queueAndRoleMappingActions.openModalForm()
    );

    expect(state.isOpenModal).toEqual(true);
  });

  it('onChangeSort', () => {
    const state = reducer(
      initialState,
      queueAndRoleMappingActions.onChangeSort({ id: 'a', order: 'asc' })
    );

    expect(state.sortBy).toEqual({ id: 'a', order: 'asc' });
  });

  it('onChangeUserRole', () => {
    const state = reducer(
      initialState,
      queueAndRoleMappingActions.onChangeUserRole({
        code: 'CXCOL1',
        text: 'CXCOL1'
      })
    );

    expect(state.selectedUserRole).toEqual({
      code: 'CXCOL1',
      text: 'CXCOL1'
    });
  });

  it('onChangeTextSearch', () => {
    const state = reducer(
      initialState,
      queueAndRoleMappingActions.onChangeTextSearch('test')
    );

    expect(state.textSearch).toEqual('test');
  });

  it('onReset', () => {
    const state = reducer(initialState, queueAndRoleMappingActions.reset());

    expect(state.queueAndRoleList).toEqual([]);
  });
});
