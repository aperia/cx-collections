import { responseDefault } from 'app/test-utils';
import axios from 'axios';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
// import { queueAndRoleMappingService } from './../queueAndRoleMappingService';
import { getUserRoleList } from './getUserRoleList';

describe('Test getUserRoleList', () => {
  it('getUserRoleList > fulfilled', async () => {
    const store = createStore(rootReducer, {});

    // jest
    //   .spyOn(queueAndRoleMappingService, 'getUserRoleList')
    //   .mockResolvedValue({ ...responseDefault });

    jest.spyOn(axios, 'get').mockResolvedValue({ ...responseDefault });

    const result = await getUserRoleList()(store.dispatch, store.getState, {});

    expect(result.type).toEqual(getUserRoleList.fulfilled.type);
  });

  it('getUserRoleList > rejected', async () => {
    const store = createStore(rootReducer, {});

    // jest
    //   .spyOn(queueAndRoleMappingService, 'getUserRoleList')
    //   .mockRejectedValue({ ...responseDefault });

    jest.spyOn(axios, 'get').mockRejectedValue({ ...responseDefault });

    const result = await getUserRoleList()(store.dispatch, store.getState, {});

    expect(result.type).toEqual(getUserRoleList.rejected.type);
  });
});
