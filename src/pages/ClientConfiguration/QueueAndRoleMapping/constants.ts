export const I18N_QUEUE_AND_ROLE_MAPPING = {
  QUEUE_AND_ROLE_MAPPING: 'txt_queue_and_role_mapping',
  QUEUE_AND_ROLE_MAPPING_LIST: 'txt_queue_and_role_mapping_list',
  ADD_QUEUE_AND_ROLE_MAPPING: 'txt_add_queue_and_role_mapping',
  QUEUE_GROUP_OWNER: 'txt_queue_group_owner',
  ACCESS_LEVEL: 'txt_access_level',
  QUEUE_AND_ROLE_MAPPING_ADDED: 'txt_queue_and_role_mapping_added',
  QUEUE_AND_ROLE_MAPPING_FAILED_TO_ADDED:
    'txt_queue_and_role_mapping_failed_to_added',
  EDIT_QUEUE_AND_ROLE_MAPPING: 'txt_edit_queue_and_role_mapping',
  QUEUE_AND_ROLE_MAPPING_UPDATED: 'txt_queue_and_role_mapping_updated',
  QUEUE_AND_ROLE_MAPPING_FAILED_TO_UPDATE:
    'txt_queue_and_role_mapping_failed_to_update',
  CONFIRM_DELETE_QUEUE_AND_ROLE_MAPPING:
    'txt_confirm_delete_queue_and_role_mapping',
  CONFIRM_DELETE_QUEUE_AND_ROLE_MAPPING_CONTENT:
    'txt_confirm_delete_queue_and_role_mapping_content',
  QUEUE_AND_ROLE_MAPPING_DELETED: 'txt_queue_and_role_mapping_deleted',
  QUEUE_AND_ROLE_MAPPING_FAILED_TO_DELETED:
    'txt_queue_and_role_mapping_failed_to_deleted',
  USER_ROLE: 'txt_user_role',
  QUEUE_NAME_IS_REQUIRED: 'txt_queue_name_is_required',
  USER_ROLE_IS_REQUIRED: 'txt_user_role_is_required',
  QUEUE_NAME: 'txt_queue_name',
  ALL_QUEUE_NAMES_ARE_ADDED: 'txt_all_queue_names_are_added',
  QUEUE_NAME_ADDED: 'txt_queue_name_added',
  QUEUE_NAME_FAILED_TO_ADDED: 'txt_queue_name_failed_to_added',
  QUEUE_NAME_DELETED: 'txt_queue_name_deleted',
  QUEUE_NAME_FAILED_TO_DELETED: 'txt_queue_name_failed_to_deleted',
  CONFIRM_DELETE_QUEUE_NAME: 'txt_queue_name_confirm_delete',
  CONFIRM_DELETE_QUEUE_NAME_CONTENT: 'txt_queue_name_confirm_delete_content'
};

export const QUEUE_GRID_COLUMN_NAME = {
  QUEUE_NAME: 'queueName'
};

export const FORM_FIELD = {
  QUEUE_NAME: {
    name: 'queueName',
    label: I18N_QUEUE_AND_ROLE_MAPPING.QUEUE_NAME
  },
  USER_ROLE: {
    name: 'userRole',
    label: I18N_QUEUE_AND_ROLE_MAPPING.USER_ROLE
  }
};
