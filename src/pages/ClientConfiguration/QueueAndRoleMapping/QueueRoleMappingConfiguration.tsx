import React, { useEffect, useLayoutEffect, useRef, Fragment } from 'react';
import classNames from 'classnames';
import isUndefined from 'lodash.isundefined';
import isEmpty from 'lodash.isempty';

// components
import { SimpleBar } from 'app/_libraries/_dls/components';
import NoData from './Components/NoData';
import DropdownQueueRoleMapping from './DropdownQueueRoleMapping';
import QueueNameGrid from './QueueNameGrid';
import AddQueue from './Components/AddQueue';
import AddQueueModal from './Components/AddQueueModal';
import DeleteQueueModal from './Components/DeleteQueueModal';

// i18
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';

// redux
import { queueAndRoleMappingActions } from './_redux/reducers';
import { batch, useDispatch, useSelector } from 'react-redux';
import { genAmtId } from 'app/_libraries/_dls/utils';
import {
  selectedUserRole,
  selectGridData,
  selectLoading
} from './_redux/selectors';

const QueueRoleMappingConfiguration: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const loading = useSelector<RootState, boolean>(selectLoading);
  const divRef = useRef<HTMLDivElement | null>(null);
  const data = useSelector(selectGridData);
  const selectedRole = useSelector(selectedUserRole);

  const testId = 'queueRoleMappingConfigurations';

  useEffect(() => {
    batch(() => {
      dispatch(queueAndRoleMappingActions.getUserRoleList());
    });
  }, [dispatch]);

  const handleOnAddQueue = () => {
    dispatch(queueAndRoleMappingActions.openModalForm());
  };

  const renderLayout = () => {
    if (isUndefined(selectedRole)) return null;

    if (isEmpty(data) && !loading)
      return (
        <Fragment>
          <div className="mt-24 mb-16 d-flex justify-content-between">
            <h5 data-testid={genAmtId(testId, 'queue-list', '')}>
              {t('txt_queue_list')}{' '}
            </h5>
          </div>
          <NoData>
            <AddQueue onAddQueue={handleOnAddQueue} />
          </NoData>
        </Fragment>
      );

    return (
      <Fragment>
        <QueueNameGrid />
      </Fragment>
    );
  };

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100vh - 112px)`);
  }, []);

  return (
    <Fragment>
      <div ref={divRef}>
        <SimpleBar className={classNames({ loading })}>
          <div className="p-24 max-width-lg mx-auto">
            <h3 data-testid={genAmtId(testId, 'title', '')}>
              {t('txt_queue_and_role_mapping')}
            </h3>
            <DropdownQueueRoleMapping />
            {renderLayout()}
          </div>
        </SimpleBar>
      </div>
      <AddQueueModal />
      <DeleteQueueModal />
    </Fragment>
  );
};

export default QueueRoleMappingConfiguration;
