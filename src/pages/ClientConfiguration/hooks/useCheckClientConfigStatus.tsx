import { useSelector } from 'react-redux';
import { selectConfig } from '../_redux/selectors';

export const useCheckClientConfigStatus = () => {
  const { status } = useSelector(selectConfig);

  return !status;
};
