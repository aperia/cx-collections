import React from 'react';
import { render } from '@testing-library/react';
import { SECTION_TAB_EVENT } from '../constants';

import { useListenChangingTabEvent } from './useListenChangingTabEvent';

const Comp = ({ callbackFunc }: { callbackFunc: Function }) => {
  useListenChangingTabEvent(callbackFunc);

  return <div />;
};

it('useListenChangingTabEvent', () => {
  const callBackFunc = jest.fn();

  render(<Comp callbackFunc={() => callBackFunc()} />);

  window.dispatchEvent(new Event(SECTION_TAB_EVENT.CHANGING_TAB));

  expect(callBackFunc).toBeCalled();
});
