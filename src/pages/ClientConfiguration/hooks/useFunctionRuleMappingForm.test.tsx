import React, { ChangeEvent, useState } from 'react';
import dateTime from 'date-and-time';
import { render, screen } from '@testing-library/react';
import * as formik from 'formik';

import { useFunctionRuleMappingForm } from './useFunctionRuleMappingForm';
import userEvent from '@testing-library/user-event';
import { FunctionRuleMappingForm } from '../FunctionRuleMapping/types';
import { DropdownBaseChangeEvent } from 'app/_libraries/_dls/components';
import { I18N_FUNCTION_RULE_MAPPING } from '../FunctionRuleMapping/constants';

jest.mock('react-i18next', () => {
  return {
    ...jest.requireActual('react-i18next'),
    useTranslation: () => ({ t: (text: string) => text })
  };
});

const Comp = ({
  onSubmit,
  action
}: {
  onSubmit: (values: FunctionRuleMappingForm) => void;
  action: string;
}) => {
  const [errors, setError] = useState({});
  const {
    handleChangeTextBoxWithoutAlpahnumber,
    handleChangeDropDownList,
    validateForm
  } = useFunctionRuleMappingForm(onSubmit, action, []);

  const validateAddWithErrorRequired = () => {
    const err = validateForm({
      functionName: undefined,
      currentRuleName: '',
      currentRuleVersion: '',
      effectiveDate: undefined,
      effectiveTime: undefined
    });

    setError(err);
  };

  const validateAddWithoutError = () => {
    const err = validateForm({
      functionName: { text: 'text' },
      currentRuleName: 'currentRuleName',
      nextRuleName: 'nextRuleName',
      currentRuleVersion: 'currentRuleVersion',
      nextRuleVersion: 'nextRuleVersion',
      effectiveDate: new Date(),
      effectiveTime: { hour: '10' }
    });

    setError(err);
  };

  const validateAddWithErrorDate = () => {
    const err = validateForm({
      functionName: { text: 'text' },
      currentRuleName: 'currentRuleName',
      nextRuleName: 'nextRuleName',
      currentRuleVersion: 'currentRuleVersion',
      nextRuleVersion: 'nextRuleVersion',
      effectiveDate: dateTime.addDays(new Date(), -1),
      effectiveTime: { hour: '10', minute: '20' }
    });

    setError(err);
  };

  const changeDropDownList = () => {
    handleChangeDropDownList({
      target: { name: 'name', value: 'value' }
    } as DropdownBaseChangeEvent);
  };

  const changeTextBoxWithoutAlpahnumber = () => {
    handleChangeTextBoxWithoutAlpahnumber({
      target: { name: 'name', value: '123' }
    } as ChangeEvent<HTMLInputElement>);
  };

  const changeTextBoxWithAlpahnumber = () => {
    handleChangeTextBoxWithoutAlpahnumber({
      target: { name: 'name', value: '***123' }
    } as ChangeEvent<HTMLInputElement>);
  };

  const changeTextBoxWithASCIICharacters = () => {
    handleChangeTextBoxWithoutAlpahnumber({
      target: { name: 'name', value: 'ắ123' /* ASCII Characters */ }
    } as ChangeEvent<HTMLInputElement>);
  };

  return (
    <div>
      <div data-testid="errors">{JSON.stringify(errors)}</div>
      <div
        data-testid="validateAddWithErrorRequired"
        onClick={validateAddWithErrorRequired}
      />
      <div
        data-testid="validateAddWithoutError"
        onClick={validateAddWithoutError}
      />
      <div
        data-testid="validateAddWithErrorDate"
        onClick={validateAddWithErrorDate}
      />
      <div data-testid="changeDropDownList" onClick={changeDropDownList} />
      <div
        data-testid="changeTextBoxWithoutAlpahnumber"
        onClick={changeTextBoxWithoutAlpahnumber}
      />
      <div
        data-testid="changeTextBoxWithAlpahnumber"
        onClick={changeTextBoxWithAlpahnumber}
      />
      <div
        data-testid="changeTextBoxWithASCIICharacters"
        onClick={changeTextBoxWithASCIICharacters}
      />
    </div>
  );
};

const setFieldValue = jest.fn();

beforeEach(() => {
  setFieldValue.mockClear();

  jest.spyOn(formik, 'useFormik').mockImplementation((({
    validate
  }: {
    validate: Function;
  }) => {
    return {
      values: {},
      setFieldValue,
      setValues: jest.fn(),
      handleReset: jest.fn(),
      handleChange: jest.fn(),
      handleBlur: jest.fn(),
      handleSubmit: jest.fn(),
      validateForm: validate,
      errors: {},
      touched: {}
    };
  }) as any);
});

describe('useFunctionRuleMappingForm', () => {
  it('validate Add', () => {
    const onSubmit = jest.fn();

    render(<Comp onSubmit={() => onSubmit()} action="Add" />);

    userEvent.click(screen.getByTestId('validateAddWithoutError'));
    let errors = screen.getByTestId('errors').textContent!;
    expect(JSON.parse(errors)).toEqual({});

    userEvent.click(screen.getByTestId('validateAddWithErrorRequired'));
    errors = screen.getByTestId('errors').textContent!;
    expect(JSON.parse(errors)).toEqual({
      functionName: `${I18N_FUNCTION_RULE_MAPPING.FUNCTION} ${I18N_FUNCTION_RULE_MAPPING.IS_REQUIRED}`,
      currentRuleName: `${I18N_FUNCTION_RULE_MAPPING.RULE_NAME} ${I18N_FUNCTION_RULE_MAPPING.IS_REQUIRED}`,
      currentRuleVersion: `${I18N_FUNCTION_RULE_MAPPING.VERSION} ${I18N_FUNCTION_RULE_MAPPING.IS_REQUIRED}`,
    });

    userEvent.click(screen.getByTestId('validateAddWithErrorDate'));
    errors = screen.getByTestId('errors').textContent!;
    expect(JSON.parse(errors)).toEqual({
      effectiveDate: I18N_FUNCTION_RULE_MAPPING.EFFECTIVE_DATE_ERROR
    });
  });

  it('validate Edit', () => {
    const onSubmit = jest.fn();

    render(<Comp onSubmit={() => onSubmit()} action="Edit" />);

    userEvent.click(screen.getByTestId('validateAddWithoutError'));
    let errors = screen.getByTestId('errors').textContent!;
    expect(JSON.parse(errors)).toEqual({});

    userEvent.click(screen.getByTestId('validateAddWithErrorRequired'));
    errors = screen.getByTestId('errors').textContent!;
    expect(JSON.parse(errors)).toEqual({
      nextRuleName: `${I18N_FUNCTION_RULE_MAPPING.NEXT_RULE_NAME}${I18N_FUNCTION_RULE_MAPPING.IS_REQUIRED}`,
      nextRuleVersion: `${I18N_FUNCTION_RULE_MAPPING.NEXT_VERSION}${I18N_FUNCTION_RULE_MAPPING.IS_REQUIRED}`,
      effectiveDate: `${I18N_FUNCTION_RULE_MAPPING.NEXT_EFFECTIVE_DATE}${I18N_FUNCTION_RULE_MAPPING.IS_REQUIRED}`
    });

    userEvent.click(screen.getByTestId('validateAddWithErrorDate'));
    errors = screen.getByTestId('errors').textContent!;
    expect(JSON.parse(errors)).toEqual({
      effectiveDate: I18N_FUNCTION_RULE_MAPPING.EFFECTIVE_DATE_ERROR
    });
  });

  it('handleChangeDropDownList', () => {
    const onSubmit = jest.fn();

    render(<Comp onSubmit={() => onSubmit()} action="Add" />);

    userEvent.click(screen.getByTestId('changeDropDownList'));
    expect(setFieldValue).toBeCalledWith('name', 'value');
  });

  it('handleChangeTextBox Without Alpahnumber', () => {
    const onSubmit = jest.fn();

    render(<Comp onSubmit={() => onSubmit()} action="Add" />);

    userEvent.click(screen.getByTestId('changeTextBoxWithoutAlpahnumber'));
    expect(setFieldValue).toBeCalledWith('name', '123');
  });

  it('handleChangeTextBox With Alpahnumber', () => {
    const onSubmit = jest.fn();

    render(<Comp onSubmit={() => onSubmit()} action="Add" />);

    userEvent.click(screen.getByTestId('changeTextBoxWithAlpahnumber'));
    expect(setFieldValue).not.toBeCalled();
  });

  it('handleChangeTextBox With ASCII Characters', () => {
    const onSubmit = jest.fn();

    render(<Comp onSubmit={() => onSubmit()} action="Add" />);

    userEvent.click(screen.getByTestId('changeTextBoxWithASCIICharacters'));
    expect(setFieldValue).not.toBeCalled();
  });
});
