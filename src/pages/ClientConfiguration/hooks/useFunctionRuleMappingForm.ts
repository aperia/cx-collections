import { ChangeEvent } from 'react';

// formik
import { useFormik } from 'formik';
// helpers
import { stringValidate } from 'app/helpers';

// types
import { DropdownBaseChangeEvent } from 'app/_libraries/_dls/components';
import {
  FunctionName,
  FunctionRuleMappingForm,
  TFunctionRuleMappingProperties
} from '../FunctionRuleMapping/types';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_FUNCTION_RULE_MAPPING } from '../FunctionRuleMapping/constants';

// helpers
import { getEffectiveDateErrors } from '../FunctionRuleMapping/helpers';

const initialValues: FunctionRuleMappingForm = {
  functionName: undefined,
  currentRuleName: '',
  currentRuleVersion: '',
  nextRuleName: '',
  nextRuleVersion: '',
  effectiveDate: undefined
};

export const useFunctionRuleMappingForm = (
  onSubmit: (values: FunctionRuleMappingForm) => void,
  action: string,
  refData?: FunctionName[]
) => {
  const { t } = useTranslation();
  const validateAdd = ({
    functionName,
    currentRuleName,
    currentRuleVersion,
    nextRuleName,
    nextRuleVersion,
    effectiveDate
  }: FunctionRuleMappingForm) => {
    const nextErrors = {} as Record<TFunctionRuleMappingProperties, string>;

    if (!functionName) {
      nextErrors.functionName = `${t(I18N_FUNCTION_RULE_MAPPING.FUNCTION)} ${t(
        I18N_FUNCTION_RULE_MAPPING.IS_REQUIRED
      )}`;
    }
    if (!currentRuleName) {
      nextErrors.currentRuleName = `${t(
        I18N_FUNCTION_RULE_MAPPING.RULE_NAME
      )} ${t(I18N_FUNCTION_RULE_MAPPING.IS_REQUIRED)}`;
    }
    if (!currentRuleVersion) {
      nextErrors.currentRuleVersion = `${t(
        I18N_FUNCTION_RULE_MAPPING.VERSION
      )} ${t(I18N_FUNCTION_RULE_MAPPING.IS_REQUIRED)}`;
    }

    if (effectiveDate) {
      const effectiveDateErrors = getEffectiveDateErrors(effectiveDate, t);
      if (effectiveDateErrors.effectiveDate.status)
        nextErrors.effectiveDate = effectiveDateErrors.effectiveDate.message!;
    }

    return nextErrors;
  };

  const validateEdit = ({
    functionName,
    currentRuleName,
    currentRuleVersion,
    nextRuleName,
    nextRuleVersion,
    effectiveDate
  }: FunctionRuleMappingForm) => {
    const nextErrors = {} as Record<TFunctionRuleMappingProperties, string>;

    if (!nextRuleName) {
      nextErrors.nextRuleName = `${t(
        I18N_FUNCTION_RULE_MAPPING.NEXT_RULE_NAME
      )}${t(I18N_FUNCTION_RULE_MAPPING.IS_REQUIRED)}`;
    }
    if (!nextRuleVersion) {
      nextErrors.nextRuleVersion = `${t(
        I18N_FUNCTION_RULE_MAPPING.NEXT_VERSION
      )}${t(I18N_FUNCTION_RULE_MAPPING.IS_REQUIRED)}`;
    }
    if (!effectiveDate) {
      nextErrors.effectiveDate = `${t(
        I18N_FUNCTION_RULE_MAPPING.NEXT_EFFECTIVE_DATE
      )}${t(I18N_FUNCTION_RULE_MAPPING.IS_REQUIRED)}`;
    }

    if (effectiveDate) {
      const effectiveDateErrors = getEffectiveDateErrors(effectiveDate, t);
      if (effectiveDateErrors.effectiveDate.status)
        nextErrors.effectiveDate = effectiveDateErrors.effectiveDate.message!;
    }

    return nextErrors;
  };

  const {
    values,
    setFieldValue,
    setValues,
    handleReset,
    handleChange,
    handleBlur,
    handleSubmit,
    validateForm,
    errors,
    touched
  } = useFormik({
    initialValues,
    validateOnMount: true,
    validateOnBlur: true,
    validateOnChange: false,
    onSubmit,
    validate: action === 'Add' ? validateAdd : validateEdit
  });

  /**
   * Auto set radio value to '$ALL' for the fields that followed by @param fieldName
   * in the field order list
   */

  const handleChangeTextBoxWithoutAlpahnumber = (
    e: ChangeEvent<HTMLInputElement>
  ) => {
    const fieldName = e.target.name;
    const { value } = e.target;

    const validateValue = stringValidate(value);

    if (validateValue.isNoneASCIICharacters()) return;

    if (!validateValue.isAlphanumeric()) return;

    setFieldValue(fieldName, value);
  };

  const handleChangeDropDownList = (e: DropdownBaseChangeEvent) => {
    const fieldName = e.target.name;
    const { value } = e.target;

    setFieldValue(fieldName, value);
  };

  return {
    handleChangeDropDownList,
    handleChangeTextBoxWithoutAlpahnumber,
    values,
    setFieldValue,
    setValues,
    handleReset,
    handleChange,
    handleBlur,
    handleSubmit,
    validateForm,
    errors,
    touched
  };
};
