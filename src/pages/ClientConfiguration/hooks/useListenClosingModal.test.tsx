import React from 'react';
import { render } from '@testing-library/react';
import { SECTION_TAB_EVENT } from '../constants';

import { useListenClosingModal } from './useListenClosingModal';

const Comp = ({ callbackFunc }: { callbackFunc: Function }) => {
  useListenClosingModal(callbackFunc);

  return <div />;
};

it('useListenClosingModal', () => {
  const callBackFunc = jest.fn();

  render(<Comp callbackFunc={() => callBackFunc()} />);

  window.dispatchEvent(new Event(SECTION_TAB_EVENT.CLOSING_MODAL));

  expect(callBackFunc).toBeCalled();
});
