import { useEffect } from 'react';
import { SECTION_TAB_EVENT } from '../constants';

/**
 *
 * Use this hook to control when an approvement event called, the event come from any section's component
 * @param callbackFunc a callback function will be execute after approved
 *
 */
export const useListenChangingTabEvent = (callbackFunc: Function) => {
  useEffect(() => {
    function handleChangeTabEvent() {
      callbackFunc();
    }

    window.addEventListener(
      SECTION_TAB_EVENT.CHANGING_TAB,
      handleChangeTabEvent
    );

    window.clientConfiguration = {
      ...window.clientConfiguration,
      isHavingTabListener: true
    };

    return () => {
      window.removeEventListener(
        SECTION_TAB_EVENT.CHANGING_TAB,
        handleChangeTabEvent
      );

      window.clientConfiguration = {
        ...window.clientConfiguration,
        isHavingTabListener: false
      };
    };
  }, [callbackFunc]);
};
