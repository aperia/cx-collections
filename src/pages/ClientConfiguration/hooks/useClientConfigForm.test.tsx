import React, { ChangeEvent, useState } from 'react';
import { render, screen } from '@testing-library/react';
import * as formik from 'formik';

import { useClientConfigForm } from './useClientConfigForm';
import { FormValue } from '../types';
import userEvent from '@testing-library/user-event';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';

jest.mock('react-i18next', () => {
  return {
    ...jest.requireActual('react-i18next'),
    useTranslation: () => ({ t: (text: string) => text })
  };
});

const Comp = ({ onSubmit }: { onSubmit: (values: FormValue) => void }) => {
  const [errors, setError] = useState({});
  const {
    handleChangeRadioOptionAll,
    handleChangeRadioOptionCustom,
    handleChangeTextBox,
    handleChangeDescription,
    validateForm
  } = useClientConfigForm(onSubmit);

  const validateWithErrorDescription = () => {
    const err = validateForm({
      clientRadio: { all: false, custom: false },
      clientId: 'cId',
      systemRadio: { all: false, custom: false },
      systemId: 'sId',
      principleRadio: { all: false, custom: false },
      principleId: 'pId',
      agentRadio: { all: false, custom: false },
      agentId: 'aId'
    });

    setError(err);
  };

  const validateWithErrorId = () => {
    const err = validateForm({
      clientRadio: { all: false, custom: true },
      clientId: '',
      systemRadio: { all: false, custom: true },
      systemId: '',
      principleRadio: { all: false, custom: true },
      principleId: '',
      agentRadio: { all: false, custom: true },
      agentId: ''
    });

    setError(err);
  };

  const validateWithErrorIdRequire = () => {
    const err = validateForm({
      clientRadio: { all: false, custom: undefined as never },
      clientId: '',
      systemRadio: { all: false, custom: true },
      systemId: '',
      principleRadio: { all: false, custom: true },
      principleId: '',
      agentRadio: { all: false, custom: true },
      agentId: ''
    });

    setError(err);
  };

  const validateWithErrorInvalidFormat = () => {
    const err = validateForm({
      clientRadio: { all: true, custom: true },
      clientId: 'cId',
      systemRadio: { all: true, custom: true },
      systemId: 'sId',
      principleRadio: { all: true, custom: true },
      principleId: 'pId',
      agentRadio: { all: true, custom: true },
      agentId: 'aId',
      description: 'description'
    });

    setError(err);
  };

  const changeRadioOptionAll = () => {
    handleChangeRadioOptionAll({
      target: { name: 'name' }
    } as ChangeEvent<HTMLInputElement>);
  };

  const changeRadioOptionAllTargetCase2 = () => {
    handleChangeRadioOptionAll({
      target: { name: 'principleRadio' }
    } as ChangeEvent<HTMLInputElement>);
  };

  const changeRadioOptionAllTargetCase3 = () => {
    handleChangeRadioOptionAll({
      target: { name: 'agentRadio' }
    } as ChangeEvent<HTMLInputElement>);
  };

  const changeRadioOptionCustom = () => {
    handleChangeRadioOptionCustom({
      target: { name: 'name' }
    } as ChangeEvent<HTMLInputElement>);
  };

  const changeTextBox = () => {
    handleChangeTextBox({
      target: { name: 'name', value: 'value' }
    } as ChangeEvent<HTMLInputElement>);
  };

  const changeTextBoxWithNumber = () => {
    handleChangeTextBox({
      target: { name: 'name', value: '123' }
    } as ChangeEvent<HTMLInputElement>);
  };

  const changeDescription = () => {
    handleChangeDescription({
      target: { name: 'name', value: '123' }
    } as ChangeEvent<HTMLInputElement>);
  };

  const changeDescriptionWithSpecialCharacters = () => {
    handleChangeDescription({
      target: { name: 'name', value: '%^&' }
    } as ChangeEvent<HTMLInputElement>);
  };

  return (
    <div>
      <div data-testid="errors">{JSON.stringify(errors)}</div>
      <div
        data-testid="validateWithErrorDescription"
        onClick={validateWithErrorDescription}
      />
      <div data-testid="validateWithErrorId" onClick={validateWithErrorId} />
      <div
        data-testid="validateWithErrorIdRequire"
        onClick={validateWithErrorIdRequire}
      />
      <div
        data-testid="validateWithErrorInvalidFormat"
        onClick={validateWithErrorInvalidFormat}
      />
      <div data-testid="changeRadioOptionAll" onClick={changeRadioOptionAll} />
      <div
        data-testid="changeRadioOptionAllTargetCase2"
        onClick={changeRadioOptionAllTargetCase2}
      />
      <div
        data-testid="changeRadioOptionAllTargetCase3"
        onClick={changeRadioOptionAllTargetCase3}
      />

      <div
        data-testid="changeRadioOptionCustom"
        onClick={changeRadioOptionCustom}
      />
      <div data-testid="changeTextBox" onClick={changeTextBox} />
      <div
        data-testid="changeTextBoxWithNumber"
        onClick={changeTextBoxWithNumber}
      />

      <div data-testid="changeDescription" onClick={changeDescription} />

      <div
        data-testid="changeDescriptionWithSpecialCharacters"
        onClick={changeDescriptionWithSpecialCharacters}
      />
    </div>
  );
};

const setFieldValue = jest.fn();

beforeEach(() => {
  setFieldValue.mockClear();

  jest.spyOn(formik, 'useFormik').mockImplementation((({
    validate
  }: {
    validate: Function;
  }) => {
    return {
      values: {},
      setFieldValue,
      setValues: jest.fn(),
      handleReset: jest.fn(),
      handleChange: jest.fn(),
      handleBlur: jest.fn(),
      handleSubmit: jest.fn(),
      validateForm: validate,
      errors: {},
      touched: {}
    };
  }) as any);
});

describe('useClientConfigForm', () => {
  it('validate', () => {
    const onSubmit = jest.fn();

    render(<Comp onSubmit={() => onSubmit()} />);

    userEvent.click(screen.getByTestId('validateWithErrorDescription'));
    let errors = screen.getByTestId('errors').textContent!;
    expect(JSON.parse(errors)).toEqual({
      description: I18N_CLIENT_CONFIG.DESCRIPTION_IS_REQUIRED
    });

    userEvent.click(screen.getByTestId('validateWithErrorInvalidFormat'));
    errors = screen.getByTestId('errors').textContent!;
    expect(JSON.parse(errors)).toEqual({
      agentId: I18N_COMMON_TEXT.INVALID_FORMAT,
      clientId: I18N_COMMON_TEXT.INVALID_FORMAT,
      principleId: I18N_COMMON_TEXT.INVALID_FORMAT,
      systemId: I18N_COMMON_TEXT.INVALID_FORMAT
    });

    userEvent.click(screen.getByTestId('validateWithErrorId'));
    errors = screen.getByTestId('errors').textContent!;
    expect(JSON.parse(errors)).toEqual({
      agentId: I18N_CLIENT_CONFIG.AGENT_ID_IS_REQUIRED,
      description: I18N_CLIENT_CONFIG.DESCRIPTION_IS_REQUIRED,
      principleId: I18N_CLIENT_CONFIG.PRINCIPLE_ID_IS_REQUIRED,
      systemId: I18N_CLIENT_CONFIG.SYSTEM_ID_IS_REQUIRED
    });

    userEvent.click(screen.getByTestId('validateWithErrorIdRequire'));
    errors = screen.getByTestId('errors').textContent!;
    expect(JSON.parse(errors)).toEqual({
      agentId: I18N_CLIENT_CONFIG.AGENT_ID_IS_REQUIRED,
      clientId: I18N_CLIENT_CONFIG.CLIENT_ID_IS_REQUIRED,
      description: I18N_CLIENT_CONFIG.DESCRIPTION_IS_REQUIRED,
      principleId: I18N_CLIENT_CONFIG.PRINCIPLE_ID_IS_REQUIRED,
      systemId: I18N_CLIENT_CONFIG.SYSTEM_ID_IS_REQUIRED
    });
  });

  it('handleChangeRadioOptionAll', () => {
    const onSubmit = jest.fn();

    render(<Comp onSubmit={() => onSubmit()} />);

    userEvent.click(screen.getByTestId('changeRadioOptionAll'));
    expect(setFieldValue).toBeCalledTimes(2);
    expect(setFieldValue).toBeCalledWith('name', { all: true, custom: false });
    expect(setFieldValue).toBeCalledWith('agentRadio', {
      all: true,
      custom: false
    });
  });

  it('handleChangeRadioOptionAllCase2', () => {
    const onSubmit = jest.fn();

    render(<Comp onSubmit={() => onSubmit()} />);

    userEvent.click(screen.getByTestId('changeRadioOptionAllTargetCase2'));
    expect(setFieldValue).toBeCalledTimes(5);
    expect(setFieldValue).toBeCalledWith('principleRadio', {
      all: true,
      custom: false
    });
    expect(setFieldValue).toBeCalledWith('agentRadio', {
      all: true,
      custom: false
    });
  });

  it('handleChangeRadioOptionAllCase3', () => {
    const onSubmit = jest.fn();

    render(<Comp onSubmit={() => onSubmit()} />);

    userEvent.click(screen.getByTestId('changeRadioOptionAllTargetCase3'));
    expect(setFieldValue).toBeCalledTimes(3);
    expect(setFieldValue).toBeCalledWith('agentRadio', {
      all: true,
      custom: false
    });
    expect(setFieldValue).toBeCalledWith('agentId', '');
  });

  it('handleChangeRadioOptionCustom', () => {
    const onSubmit = jest.fn();

    render(<Comp onSubmit={() => onSubmit()} />);

    userEvent.click(screen.getByTestId('changeRadioOptionCustom'));
    expect(setFieldValue).toBeCalledTimes(1);
    expect(setFieldValue).toBeCalledWith('name', { all: false, custom: true });
  });

  it('handleChangeTextBox', () => {
    const onSubmit = jest.fn();

    render(<Comp onSubmit={() => onSubmit()} />);

    userEvent.click(screen.getByTestId('changeTextBox'));
    expect(setFieldValue).toBeCalledTimes(0);
  });

  it('handleChangeDescription', () => {
    const onSubmit = jest.fn();

    render(<Comp onSubmit={() => onSubmit()} />);

    userEvent.click(screen.getByTestId('changeDescription'));
    expect(setFieldValue).toBeCalledTimes(1);
  });

  it('handleChangeDescriptionWithSpecialCharacters', () => {
    const onSubmit = jest.fn();

    render(<Comp onSubmit={() => onSubmit()} />);

    userEvent.click(
      screen.getByTestId('changeDescriptionWithSpecialCharacters')
    );
    expect(setFieldValue).toBeCalledTimes(0);
  });

  it('handleChangeTextBox with number', () => {
    const onSubmit = jest.fn();

    render(<Comp onSubmit={() => onSubmit()} />);

    userEvent.click(screen.getByTestId('changeTextBoxWithNumber'));
    expect(setFieldValue).toBeCalledTimes(1);
    expect(setFieldValue).toBeCalledWith('name', '123');
  });
});
