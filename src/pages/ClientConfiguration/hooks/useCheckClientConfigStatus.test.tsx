import React from 'react';
import { useCheckClientConfigStatus } from './useCheckClientConfigStatus';
import { renderMockStore } from 'app/test-utils';

describe('Test useListenChangingTabEvent hook', () => {
  const initalState: Partial<RootState> = {
    clientConfiguration: {
      settings: {
        selectedConfig: {}
      },
      status: true
    }
  };

  const Comp = () => {
    useCheckClientConfigStatus();

    return <div />;
  };

  it('useListenChangingTabEvent', () => {
    renderMockStore(<Comp />, { initalState });
  });
});
