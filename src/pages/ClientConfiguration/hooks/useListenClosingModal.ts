import { useEffect } from 'react';
import { SECTION_TAB_EVENT } from '../constants';

/**
 * Listen close modal and execute callbackFunction to trigger any action inside section's component
 * @param callbackFunc
 */
export const useListenClosingModal = (callbackFunc: Function) => {
  useEffect(() => {
    const eventHandler = () => {
      callbackFunc();
    };

    window.addEventListener(SECTION_TAB_EVENT.CLOSING_MODAL, eventHandler);

    return () => {
      window.removeEventListener(SECTION_TAB_EVENT.CLOSING_MODAL, eventHandler);
    };
  }, [callbackFunc]);
};
