import { orderBy } from 'lodash';
import { configurationContactAccountEntitlementActions } from 'pages/ConfigurationEntitlement/ContactAndAccountEntitlement/_redux/reducer';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import React from 'react';
import { batch } from 'react-redux';

// constants
import {
  CLIENT_CONFIG_STATUS_KEY,
  DEFAULT_CLIENT_CONFIG_KEY,
  NAME_ACTION,
  SECTION_TAB_EVENT
} from './constants';
import { DEFAULT_MEMO_CONFIG } from './Memos/constants';
import { MemoConfigItem } from './Memos/types';
import { QueueAndRoleMappingItem } from './QueueAndRoleMapping/types';

// Types
import {
  AdditionalFieldsMapType,
  ConfigData,
  SaveConfigThunkPropsType,
  SectionItem
} from './types';
import { clientConfigurationActions } from './_redux/reducers';

export const mapConfig = (values: string[]) => {
  const [clientId, systemId, principleId, agentId] = values;
  const descriptions = values.slice(4, values.length);

  return {
    clientId,
    systemId,
    principleId,
    agentId,
    description: descriptions.join('-')
  };
};

export const dispatchTabChangingTabEvent = () => {
  const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGING_TAB, {
    bubbles: true,
    cancelable: true
  });

  window.dispatchEvent(event);
};

export const dispatchControlTabChangeEvent = () => {
  const event = new CustomEvent(SECTION_TAB_EVENT.CHANGE_TAB, {
    bubbles: true,
    cancelable: true
  });

  window.dispatchEvent(event);
};

export const dispatchClosingModalEvent = () => {
  const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
    bubbles: true,
    cancelable: true
  });

  window.dispatchEvent(event);
};

export const dispatchCloseModalEvent = () => {
  const event = new CustomEvent(SECTION_TAB_EVENT.CLOSE_MODAL, {
    bubbles: true,
    cancelable: true
  });

  window.dispatchEvent(event);
};

export const ACC_EXTERNAL_STATUS = 'AES';
export const REASON_CODE = 'RC';
export const ADJUSTMENT_TRANSACTION = 'AT';
export const CODE_TO_TEXT = 'CTT';
export const CALL_RESULT = 'CR';
export const TALKING_POINT = 'TP';
export const AUTHENTICATION = 'AUTH';
export const LETTER_LIST = 'LL';
export const PROMISE_TO_PAY = 'PTP';
export const CLIENT_CONTACT_INFORMATION = 'CCI';
export const DEBT_MANAGEMENT_COMPANIES = 'DMC';
export const TIMER = 'TMR';
export const PAY_BY_PHONE = 'PBP';
export const CLUSTER_TO_QUEUE_MAPPING = 'CTQM';
export const REFERENCES = 'R';
export const FUNCTION_RULE_MAPPING = 'FRM';
export const REGULATORY = 'RL';
export const MEMOS = 'MM';
export const SETTEMENT_PAYMENT = 'SP';

export const SectionsMap = new Map<string, { component: React.FC<unknown> }>([
  [
    ACC_EXTERNAL_STATUS,
    {
      component: React.lazy(() => import('./AccountExternalStatus'))
    }
  ],
  [
    ADJUSTMENT_TRANSACTION,
    {
      component: React.lazy(() => import('./AdjustmentTransaction'))
    }
  ],
  [
    CODE_TO_TEXT,
    {
      component: React.lazy(() => import('./CodeToText'))
    }
  ],
  [
    CALL_RESULT,
    {
      component: React.lazy(() => import('./CallResult'))
    }
  ],
  [
    TALKING_POINT,
    {
      component: React.lazy(() => import('./TalkingPoint'))
    }
  ],
  [
    TIMER,
    {
      component: React.lazy(() => import('./Timer'))
    }
  ],
  [
    AUTHENTICATION,
    {
      component: React.lazy(() => import('./Authentication'))
    }
  ],
  [
    LETTER_LIST,
    {
      component: React.lazy(() => import('./Letter'))
    }
  ],
  [
    PAY_BY_PHONE,
    {
      component: React.lazy(() => import('./PayByPhone'))
    }
  ],
  [
    PROMISE_TO_PAY,
    {
      component: React.lazy(() => import('./PromiseToPay'))
    }
  ],
  [
    CLIENT_CONTACT_INFORMATION,
    {
      component: React.lazy(() => import('./ClientContactInformation'))
    }
  ],
  [
    DEBT_MANAGEMENT_COMPANIES,
    {
      component: React.lazy(() => import('./DebtManageCompanies'))
    }
  ],
  [
    CLUSTER_TO_QUEUE_MAPPING,
    {
      component: React.lazy(() => import('./ClusterToQueueMapping'))
    }
  ],
  [
    REFERENCES,
    {
      component: React.lazy(() => import('./References'))
    }
  ],
  [
    FUNCTION_RULE_MAPPING,
    {
      component: React.lazy(() => import('./FunctionRuleMapping'))
    }
  ],
  [
    REGULATORY,
    {
      component: React.lazy(() => import('./SendLetter'))
    }
  ],
  [
    MEMOS,
    {
      component: React.lazy(() => import('./Memos'))
    }
  ],
  [
    SETTEMENT_PAYMENT,
    {
      component: React.lazy(() => import('./SettlementPayment'))
    }
  ]
]);

const EmptyComponent: React.FC = () => {
  return <div></div>;
};

export const mapSectionList = (sections: RefDataValue[]): SectionItem[] => {
  return sections.map(item => {
    return {
      id: item.value,
      title: item.description,
      Component: SectionsMap.get(item.value)?.component || EmptyComponent
    };
  });
};

export const filterClientConfigList = (
  data: ConfigData[],
  searchValue: string
) => {
  return data.filter(
    item =>
      item?.agentId?.toLowerCase?.()?.includes?.(searchValue) ||
      item?.clientId?.toLowerCase?.()?.includes?.(searchValue) ||
      item?.systemId?.toLowerCase?.()?.includes?.(searchValue) ||
      item?.principleId?.toLowerCase?.()?.includes?.(searchValue) ||
      item?.description?.toLowerCase?.()?.includes?.(searchValue)
  );
};

export const sortClientConfigList = (
  data: ConfigData[],
  sortBy: MagicKeyValue
) => {
  const defaultCspa = data.find(isDefaultClientConfig);

  const getItemStatus = (item: ConfigData) => {
    return !item.additionalFields?.find(
      i => i.name === CLIENT_CONFIG_STATUS_KEY || {}
    )?.active;
  };

  const interatees = [
    (item: any) =>
      sortBy.id === 'status'
        ? getItemStatus(item)
        : item[sortBy.id]?.toLowerCase(),
    (item: any) => item.clientId?.toLowerCase(),
    (item: any) => item.systemId?.toLowerCase(),
    (item: any) => item.principleId?.toLowerCase(),
    (item: any) => item.agentId?.toLowerCase()
  ];
  const orders = [sortBy.order || false, 'asc', 'asc', 'asc', 'asc'];

  if (sortBy.id !== 'status') {
    interatees.unshift(getItemStatus);
    orders.unshift('asc');
  }

  const sortList = orderBy(
    data.filter(item => !isDefaultClientConfig(item)),
    interatees,
    orders
  ) as ConfigData[];

  defaultCspa && sortList.unshift(defaultCspa);
  return sortList;
};

export const isDefaultClientConfig = (clientConfig: ConfigData) => {
  return clientConfig.additionalFields?.some(
    (item: AdditionalFieldsMapType) => item.name === DEFAULT_CLIENT_CONFIG_KEY
  );
};

export const convertClientInfoId = (
  clientId: string,
  systemId: string,
  principleId: string,
  agentId: string
) => {
  return `${clientId}-${systemId}-${principleId}-${agentId}`;
};

export const mappingMemoConfig = (data: MemoConfigItem) => {
  return {
    default: data.default || DEFAULT_MEMO_CONFIG.default,
    accountDetails: data.accountDetails || DEFAULT_MEMO_CONFIG.accountDetails,
    payment: data.payment || DEFAULT_MEMO_CONFIG.payment,
    callerAuthentication:
      data.callerAuthentication || DEFAULT_MEMO_CONFIG.callerAuthentication,
    letters: data.letters || DEFAULT_MEMO_CONFIG.letters,
    cardHolderMaintenance:
      data.cardHolderMaintenance || DEFAULT_MEMO_CONFIG.cardHolderMaintenance,
    transactionAdjustment:
      data.transactionAdjustment || DEFAULT_MEMO_CONFIG.transactionAdjustment,
    reviewAdjustment:
      data.reviewAdjustment || DEFAULT_MEMO_CONFIG.reviewAdjustment
  };
};

export const rejectApi: AppThunk =
  ({ errorMsgInline, toastMsg, nameAction }: SaveConfigThunkPropsType) =>
  dispatch => {
    batch(() => {
      nameAction === NAME_ACTION.addCspa &&
        dispatch(
          clientConfigurationActions.setAddClientConfigErrorMessage({
            error: errorMsgInline,
            isRetry: true
          })
        );

      nameAction === NAME_ACTION.editCspa &&
        dispatch(
          clientConfigurationActions.setEditClientConfigErrorMessage({
            error: errorMsgInline,
            isRetry: true
          })
        );

      nameAction === NAME_ACTION.addContact &&
        dispatch(
          configurationContactAccountEntitlementActions.setAddClientConfigErrorMessage(
            {
              error: errorMsgInline,
              isRetry: true
            }
          )
        );

      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: toastMsg
        })
      );
    });
    return;
  };

export const successApi: AppThunk =
  ({ toastMsg, nameAction }: SaveConfigThunkPropsType) =>
  dispatch => {
    batch(() => {
      nameAction === NAME_ACTION.addCspa &&
        dispatch(clientConfigurationActions.toggleAddModal());
      nameAction === NAME_ACTION.editCspa &&
        dispatch(clientConfigurationActions.toggleEditModal());
      nameAction !== NAME_ACTION.addContact &&
        dispatch(clientConfigurationActions.getAllClientConfigList());

      if (nameAction === NAME_ACTION.addContact) {
        dispatch(
          configurationContactAccountEntitlementActions.onCloseModalAddCSPA()
        );

        dispatch(
          configurationContactAccountEntitlementActions.getContactAccListCSPA()
        );
      }

      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: toastMsg
        })
      );
    });
    return;
  };

export const searchQueueName = (
  queues: QueueAndRoleMappingItem[],
  textSearch: string
): QueueAndRoleMappingItem[] => {
  if (!textSearch) return queues;
  const textSearchLowerCase = textSearch.toLowerCase().trim();
  return queues.filter(item =>
    item.queueName?.toLocaleLowerCase().includes(textSearchLowerCase)
  );
};
