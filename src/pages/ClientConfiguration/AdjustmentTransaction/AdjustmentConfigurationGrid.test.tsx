import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { adjustmentTransactionActions } from './_redux/reducers';
import AdjustmentConfigurationGrid from './AdjustmentConfigurationGrid';
import { DEFAULT_FORM_ID_LIST, I18N_ADJUSTMENT_TRANSACTION } from './constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import * as usePagination from 'app/hooks/usePagination';
import * as mockStatus from '../hooks/useCheckClientConfigStatus';

const initialState: Partial<RootState> = {
  adjustmentTransactionConfig: {
    adjustmentConfigurationList: {
      loading: false,
      data: [
        {
          active: true,
          adjustmentDescription: 'adjustmentDescription',
          adjustmentPostingBatchCode: 'adjustmentPostingBatchCode',
          adjustmentPostingTranCode: 'adjustmentPostingTranCode',
          originalTransactionCode: 'originalTransactionCode'
        }
      ]
    },
    addAdjustmentConfiguration: {
      loading: false,
      openModal: false,
      formIdList: DEFAULT_FORM_ID_LIST
    },
    deleteAdjustmentConfiguration: {
      openModal: false,
      loading: false
    },
    editAdjustmentConfiguration: {
      openModal: false,
      loading: false,
      validateError: ''
    }
  }
};

const renderWrapper = (state: Partial<RootState>) =>
  renderMockStore(<AdjustmentConfigurationGrid />, {
    initialState: state
  });

const mockAdjustmentTransactionActions = mockActionCreator(
  adjustmentTransactionActions
);

beforeEach(() => {
  mockAdjustmentTransactionActions('getAdjustmentConfiguration');

  jest.spyOn(usePagination, 'usePagination').mockImplementation(
    (data: any[]) =>
      ({
        total: 200,
        currentPage: 1,
        pageSize: [10, 25, 50],
        currentPageSize: 10,
        gridData: data,
        onPageChange: jest.fn(),
        onPageSizeChange: jest.fn(),
        setCurrentPage: jest.fn(),
        setCurrentPageSize: jest.fn()
      } as any)
  );

  jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(false)
});

describe('AdjustmentConfigurationGrid component', () => {
  it('should render with empty state', () => {
    renderWrapper({});
    expect(screen.getByText('txt_no_data')).toBeInTheDocument();
  });

  it('should render with loading', () => {
    const { container } = renderWrapper({
      adjustmentTransactionConfig: {
        deleteAdjustmentConfiguration: { loading: false, openModal: false },
        editAdjustmentConfiguration: {
          loading: false,
          openModal: false,
          validateError: ''
        },
        adjustmentConfigurationList: {
          loading: true,
          error: '',
          data: []
        },
        addAdjustmentConfiguration: {
          openModal: false,
          formIdList: [],
          loading: false
        }
      }
    });
    expect(container.querySelector('.vh-50')).toBeInTheDocument();
  });

  it('should render with error', () => {
    renderWrapper({
      adjustmentTransactionConfig: {
        deleteAdjustmentConfiguration: { loading: false, openModal: false },
        editAdjustmentConfiguration: {
          loading: false,
          openModal: false,
          validateError: ''
        },
        adjustmentConfigurationList: {
          loading: false,
          error: 'error',
          data: []
        },
        addAdjustmentConfiguration: {
          openModal: false,
          formIdList: [],
          loading: false
        }
      }
    });
    expect(screen.getByText(I18N_COMMON_TEXT.RELOAD)).toBeInTheDocument();
  });

  it('should render', () => {
    const { container } = renderWrapper(initialState);
    expect(container.querySelector('.dls-grid')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  beforeEach(() => {
    jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(false);
  });
  it('handleDeleteRequest', () => {
    const mockAction = mockAdjustmentTransactionActions(
      'deleteAdjustmentConfigRequest'
    );

    renderWrapper(initialState);

    userEvent.click(
      screen.getByText(I18N_ADJUSTMENT_TRANSACTION.ORIGINAL_TRAN_CODE)
    );
    userEvent.click(screen.getAllByText(I18N_COMMON_TEXT.DELETE)[0]);

    expect(mockAction).toBeCalledWith(
      initialState.adjustmentTransactionConfig?.adjustmentConfigurationList
        .data[0]
    );
  });

  it('handleEditRequest', () => {
    const mockAction = mockAdjustmentTransactionActions(
      'editAdjustmentConfigRequest'
    );

    renderWrapper(initialState);

    userEvent.click(
      screen.getByText(I18N_ADJUSTMENT_TRANSACTION.ORIGINAL_TRAN_CODE)
    );
    userEvent.click(screen.getAllByText(I18N_COMMON_TEXT.EDIT)[0]);

    expect(mockAction).toBeCalledWith(
      initialState.adjustmentTransactionConfig?.adjustmentConfigurationList
        .data[0]
    );
  });
});
