import { createAsyncThunk, isRejected } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers/mappingData';
import { AdjustmentTransactionItem } from 'pages/ClientConfiguration/types';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { UpdateAdjustmentConfigurationArgs } from '../types';

export const updateAdjustmentConfiguration = createAsyncThunk<
  undefined,
  UpdateAdjustmentConfigurationArgs,
  ThunkAPIConfig
>(
  'adjustmentTransaction/updateAdjustmentConfiguration',
  async (args, thunkAPI) => {
    const { dispatch, getState } = thunkAPI;

    const { data, updateAction } = args;

    //  need to fetch adjustmentGroup data to attach to the api
    const getClientInfoResponse = await dispatch(
      clientConfigurationActions.getClientInfoData({ dataType: 'adjustments' })
    );

    const adjustmentModel =
      getState().mapping.data?.clientConfigurationAdjustmentTransaction || {};

    const adjustmentCodes = mappingDataFromArray<AdjustmentTransactionItem>(
      data,
      adjustmentModel,
      true
    );

    if (isRejected(getClientInfoResponse)) return thunkAPI.rejectWithValue({});

    const adjustmentsData = getClientInfoResponse?.payload?.adjustments;

    const response = await dispatch(
      clientConfigurationActions.updateClientInfo({
        adjustments: {
          ...adjustmentsData,
          adjustmentCodes
        },
        updateAction
      })
    );

    if (isRejected(response)) return thunkAPI.rejectWithValue({});

    return undefined;
  }
);
