import { byPassFulfilled, mockActionCreator, responseDefault } from "app/test-utils";
import { clientConfigurationServices } from "pages/ClientConfiguration/clientConfigurationServices";
import { ClientInfoByIdResponse } from "pages/ClientConfiguration/types";
import { actionsToast } from "pages/__commons/ToastNotifications/_redux";
import { applyMiddleware, createStore } from "redux";
import thunk from "redux-thunk";
import { rootReducer } from "storeConfig";
import { CLIENT_CONFIG_SINGLE_FORM_VIEW, I18N_ADJUSTMENT_TRANSACTION, ORIGINAL_TRAN_CODE_VIEW } from "../constants";
import { checkError, triggerEditAdjustmentConfiguration } from "./editAdjustmentConfiguration";

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    org: 'org',
    app: 'app',
    user: 'user'
  }
};

const toastSpy = mockActionCreator(actionsToast);

const initialState = {
  clientConfiguration: {
    settings: {
      selectedConfig: {
        agentId: 'agentId',
        clientId: 'clientId',
        systemId: 'systemId',
        principleId: 'principleId'
      }
    }
  },
  adjustmentTransactionConfig: {
    adjustmentConfigurationList: {
      data: [
        {
          active: true
        }
      ]
    },
    addAdjustmentConfiguration: {
      formIdList: ['form_1', 'form_2', 'form_3']
    },
    editAdjustmentConfiguration: {
      currentRecord: {
        originalTransactionCode: '123',
        adjustmentPostingTranCode: '234',
        adjustmentPostingBatchCode: 'VV',
        adjustmentDescription: 'mock Description'
      }
    }
  },
  form: {
    [ORIGINAL_TRAN_CODE_VIEW]: {
      values: { originalTransactionCode: '124', }
    },
    [CLIENT_CONFIG_SINGLE_FORM_VIEW]: {
      values: {
        originalTransactionCode: '124',
        adjustmentPostingTranCode: '234',
        adjustmentPostingBatchCode: 'VV',
        adjustmentDescription: 'Fake Description',
        active: false
      }
    }
  }
}
describe('Test checkError', () => {
  it('should return false', async () => {
    const store = createStore(rootReducer, initialState);
    const response = checkError()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response).toEqual(false);
  });

  it('should return true', async () => {
    const store = createStore(rootReducer, {
      ...initialState,
      form: {
        ...initialState.form,
        [CLIENT_CONFIG_SINGLE_FORM_VIEW]: {
          values: {
            originalTransactionCode: '124',
            adjustmentPostingTranCode: '124',
            adjustmentPostingBatchCode: 'VV',
            adjustmentDescription: 'Fake Description',
            active: false
          }
        }
      }
    });
    const response = checkError()(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response).toEqual(true);
  });

  it('should return true', async () => {
    const store = createStore(rootReducer, {
      ...initialState,
      adjustmentTransactionConfig: {
        ...initialState.adjustmentTransactionConfig,
        adjustmentConfigurationList: {
          data: [
            {
              originalTransactionCode: '124',
              adjustmentPostingTranCode: '234',
              adjustmentPostingBatchCode: 'VV',
              adjustmentDescription: 'Fake Description 1',
              active: true
            }
          ]
        },
      }

    });
    const response = checkError()(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response).toEqual(true);
  });
  
});

describe('Test triggerAddAdjustmentConfiguration', () => {
  it('isFulfilled', async () => {
    jest
      .spyOn(clientConfigurationServices, 'getClientInfoById')
      .mockResolvedValueOnce({
        ...responseDefault,
        data: {
          responseStatus: 'success',
          clientInfo: {
            emailTemplates: [],
            adjustments: []
          } as unknown as ClientInfoByIdResponse['clientInfo']
        }
      });

    jest
      .spyOn(clientConfigurationServices, 'addUpdateClientInfo')
      .mockResolvedValueOnce({
        ...responseDefault,
        data: {
          responseStatus: 'success',
        } as any
      });

    const mockAddToast = toastSpy('addToast');
    const store = createStore(rootReducer, {
      ...initialState,
    }, applyMiddleware(thunk));

    await triggerEditAdjustmentConfiguration()(
      store.dispatch,
      store.getState,
      {}
    );
    expect(mockAddToast).toBeCalledWith({
      show: true,
            type: 'success',
            message: I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_UPDATED
    });
  });

  it('isRejected with checkError', async () => {
    const store = createStore(rootReducer, {
      ...initialState,
    }, applyMiddleware(thunk));

    const response = await triggerEditAdjustmentConfiguration()(
      byPassFulfilled(triggerEditAdjustmentConfiguration.fulfilled.type),
      store.getState,
      {}
    );

    expect(response?.payload).toEqual({});
  });

  it('isRejected with currentRecord', async () => {
    const store = createStore(rootReducer, {
      ...initialState,
      adjustmentTransactionConfig: {
        ...initialState.adjustmentTransactionConfig,
        editAdjustmentConfiguration: {
          currentRecord: undefined
        }
      }
    }, applyMiddleware(thunk));

    const response = await triggerEditAdjustmentConfiguration()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response?.payload).toEqual(undefined);
  });

  it('isRejected', async () => {
    // jest
    //   .spyOn(clientConfigurationServices, 'getClientInfoById')
    //   .mockResolvedValueOnce({
    //     ...responseDefault,
    //     data: {
    //       responseStatus: 'success',
    //       clientInfo: {
    //         emailTemplates: [],
    //         adjustments: []
    //       } as unknown as ClientInfoByIdResponse['clientInfo']
    //     }
    //   });

    // jest
    //   .spyOn(clientConfigurationServices, 'addUpdateClientInfo')
    //   .mockResolvedValueOnce({
    //     ...responseDefault,
    //     data: {
    //       responseStatus: 'success',
    //     } as any
    //   });

    const mockAddToast = toastSpy('addToast');
    const store = createStore(rootReducer, {
      ...initialState,
    }, applyMiddleware(thunk));

    await triggerEditAdjustmentConfiguration()(
      store.dispatch,
      store.getState,
      {}
    );
    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_FAILED_TO_UPDATE
    });
  });
});