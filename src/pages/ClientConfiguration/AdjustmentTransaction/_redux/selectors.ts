import { createSelector } from '@reduxjs/toolkit';

const getAdjustmentConfigurationList = (state: RootState) => {
  return state.adjustmentTransactionConfig.adjustmentConfigurationList;
};

const getAddAdjustmentConfiguration = (state: RootState) => {
  return state.adjustmentTransactionConfig.addAdjustmentConfiguration;
};

const getFormIdList = (state: RootState) => {
  return state.adjustmentTransactionConfig.addAdjustmentConfiguration
    .formIdList;
};

const getValidateErrorAddAdjustment = (state: RootState) => {
  return state.adjustmentTransactionConfig.addAdjustmentConfiguration
    .validateError;
};

export const selectValidateErrorAddAdjustment = createSelector(
  getValidateErrorAddAdjustment,
  data => data
);

export const selectAdjustmentConfigurationLoading = createSelector(
  getAdjustmentConfigurationList,
  data => data.loading
);

export const selectAdjustmentConfigurationData = createSelector(
  getAdjustmentConfigurationList,
  data => data.data.filter(item => item.active)
);

export const selectAdjustmentConfigurationError = createSelector(
  getAdjustmentConfigurationList,
  data => data.error
);

export const selectFormIdList = createSelector(getFormIdList, data => data);

export const selectAddAdjustmentLoading = createSelector(
  getAddAdjustmentConfiguration,
  data => data.loading
);

export const selectAddModalOpen = createSelector(
  getAddAdjustmentConfiguration,
  data => data.openModal
);

export const getDeleteAdjustmentConfiguration = (state: RootState) => {
  return state.adjustmentTransactionConfig.deleteAdjustmentConfiguration;
};

export const selectDeleteModalOpen = createSelector(
  getDeleteAdjustmentConfiguration,
  data => data.openModal
);

export const selectDeleteLoading = createSelector(
  getDeleteAdjustmentConfiguration,
  data => data.loading
);

export const selectDeleteRecord = createSelector(
  getDeleteAdjustmentConfiguration,
  data => data.currentRecord
);

export const getEditAdjustmentConfiguration = (state: RootState) => {
  return state.adjustmentTransactionConfig.editAdjustmentConfiguration;
};

export const selectEditModalOpen = createSelector(
  getEditAdjustmentConfiguration,
  data => data.openModal
);

export const selectEditLoading = createSelector(
  getEditAdjustmentConfiguration,
  data => data.loading
);

export const getCurrentRecord = (state: RootState) => {
  return state.adjustmentTransactionConfig.editAdjustmentConfiguration
    .currentRecord;
};

export const selectEditRecord = createSelector(getCurrentRecord, data => data);

export const selectEditSingleFormRecord = createSelector(
  getCurrentRecord,
  data => ({
    adjustmentPostingTranCode: data?.adjustmentPostingTranCode,
    adjustmentPostingBatchCode: data?.adjustmentPostingBatchCode,
    adjustmentDescription: data?.adjustmentDescription
  })
);

export const selectEditError = createSelector(
  getEditAdjustmentConfiguration,
  data => data.validateError
);
