import { byPassFulfilled, mockActionCreator, responseDefault } from "app/test-utils";
import { actionsToast } from "pages/__commons/ToastNotifications/_redux";
import { rootReducer } from "storeConfig";
import { applyMiddleware, createStore } from 'redux';
import { checkError, triggerAddAdjustmentConfiguration } from "./addAdjustmentConfiguration";
import { CLIENT_CONFIG_SINGLE_FORM_VIEW, I18N_ADJUSTMENT_TRANSACTION, ORIGINAL_TRAN_CODE_VIEW } from "../constants";
import thunk from "redux-thunk";
import { clientConfigurationServices } from "pages/ClientConfiguration/clientConfigurationServices";
import { ClientInfoByIdResponse } from "pages/ClientConfiguration/types";

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    org: 'org',
    app: 'app',
    user: 'user'
  }
};

const toastSpy = mockActionCreator(actionsToast);

const initialState = {
  clientConfiguration: {
    settings: {
      selectedConfig: {
        agentId: 'agentId',
        clientId: 'clientId',
        systemId: 'systemId',
        principleId: 'principleId'
      }
    }
  },
  adjustmentTransactionConfig: {
    adjustmentConfigurationList: {
      data: [
        {
          active: true
        }
      ]
    },
    addAdjustmentConfiguration: {
      formIdList: ['form_1', 'form_2', 'form_3']
    }
  },
  form: {
    [ORIGINAL_TRAN_CODE_VIEW]: {
      values: { originalTransactionCode: '124', }
    },
    [`${CLIENT_CONFIG_SINGLE_FORM_VIEW}-form_1`]: {
      values: {
        originalTransactionCode: '124',
        adjustmentPostingTranCode: '234',
        adjustmentPostingBatchCode: 'VV',
        adjustmentDescription: 'Fake Description',
        active: false
      }
    }
  }
}
describe('Test checkError', () => {
  it('should return data', async () => {
    const store = createStore(rootReducer, initialState);
    const response = checkError()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response).toEqual([{
      originalTransactionCode: '124',
      adjustmentPostingTranCode: '234',
      adjustmentPostingBatchCode: 'VV',
      adjustmentDescription: 'Fake Description',
      active: false
    }]);
  });

  it('should not return data with clientConfigAdjustmentOriginalTransactionCode', async () => {
    const store = createStore(rootReducer, {
      ...initialState,
      form: {
        [`${CLIENT_CONFIG_SINGLE_FORM_VIEW}-form_1`]: {
          values: {
            originalTransactionCode: '124',
            adjustmentPostingTranCode: '234',
            adjustmentPostingBatchCode: 'VV',
            adjustmentDescription: 'Fake Description',
            active: false
          }
        }
      }

    });
    const response = checkError()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response).toEqual([]);
  });

  it('should not return data with originalTransactionCode', async () => {
    const store = createStore(rootReducer, {
      ...initialState,
      form: {
        ...initialState.form,
        [ORIGINAL_TRAN_CODE_VIEW]: {
          values: { originalTransactionCode: '234', }
        },
      }

    });
    const response = checkError()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response).toEqual([]);
  });


  it('should not return data with duplicateTranCode', async () => {
    const store = createStore(rootReducer, {
      ...initialState,
      form: {
        ...initialState.form,
        [`${CLIENT_CONFIG_SINGLE_FORM_VIEW}-form_1`]: {
          values: {
            originalTransactionCode: '124',
            adjustmentPostingTranCode: '234',
            adjustmentPostingBatchCode: 'VV',
            adjustmentDescription: 'Fake Description',
            active: false
          }
        },
        [`${CLIENT_CONFIG_SINGLE_FORM_VIEW}-form_2`]: {
          values: {
            originalTransactionCode: '124',
            adjustmentPostingTranCode: '234',
            adjustmentPostingBatchCode: 'VV',
            adjustmentDescription: 'Fake Description',
            active: false
          }
        }
      }

    });
    const response = checkError()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response).toEqual([]);
  });


  it('should not return data with idErrorList = 1', async () => {
    const store = createStore(rootReducer, {
      ...initialState,
      adjustmentTransactionConfig: {
        ...initialState.adjustmentTransactionConfig,
        adjustmentConfigurationList: {
          data: [
            {
              originalTransactionCode: '124',
              adjustmentPostingTranCode: '234',
              adjustmentPostingBatchCode: 'VV',
              adjustmentDescription: 'Fake Description 1',
              active: true
            }
          ]
        },
      }

    });
    const response = checkError()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response).toEqual([]);
  });

  it('should not return data with idErrorList > 1', async () => {
    const store = createStore(rootReducer, {
      ...initialState,
      adjustmentTransactionConfig: {
        ...initialState.adjustmentTransactionConfig,
        adjustmentConfigurationList: {
          data: [
            {
              originalTransactionCode: '124',
              adjustmentPostingTranCode: '234',
              adjustmentPostingBatchCode: 'VV',
              adjustmentDescription: 'Fake Description 1',
              active: true
            },
            {
              originalTransactionCode: '124',
              adjustmentPostingTranCode: '234',
              adjustmentPostingBatchCode: 'VV',
              adjustmentDescription: 'Fake Description 2',
              active: true
            }
          ]
        },
      }

    });
    const response = checkError()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response).toEqual([]);
  });

});

describe('Test triggerAddAdjustmentConfiguration', () => {
  it('isFulfilled', async () => {
    jest
      .spyOn(clientConfigurationServices, 'getClientInfoById')
      .mockResolvedValueOnce({
        ...responseDefault,
        data: {
          responseStatus: 'success',
          clientInfo: {
            emailTemplates: [],
            adjustments: []
          } as unknown as ClientInfoByIdResponse['clientInfo']
        }
      });

    jest
      .spyOn(clientConfigurationServices, 'addUpdateClientInfo')
      .mockResolvedValueOnce({
        ...responseDefault,
        data: {
          responseStatus: 'success',
        } as any
      });

    const mockAddToast = toastSpy('addToast');
    const store = createStore(rootReducer, {
      ...initialState,
    }, applyMiddleware(thunk));

    await triggerAddAdjustmentConfiguration()(
      store.dispatch,
      store.getState,
      {}
    );
    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_ADDED,
      msgVariables: { count: 1 }
    });
  });

  it('isRejected', async () => {
    const mockAddToast = toastSpy('addToast');
    const store = createStore(rootReducer, {
      ...initialState,
    }, applyMiddleware(thunk));

    await triggerAddAdjustmentConfiguration()(
      store.dispatch,
      store.getState,
      {}
    );
    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_FAILED_TO_ADD,
      msgVariables: { count: 1 }
    });
  });

  it('isRejected', async () => {
    const store = createStore(rootReducer, {
      ...initialState,
    });

    const response = await triggerAddAdjustmentConfiguration()(
      byPassFulfilled(triggerAddAdjustmentConfiguration.fulfilled.type),
      store.getState,
      {}
    );
    expect(response?.payload).toEqual({});
  });
});