import './reducers';
import { updateAdjustmentConfiguration } from './updateAdjustmentConfiguration';
import {
  byPassFulfilled,
  byPassRejectedSpecificAction,
  mockActionCreator,
  specificName
} from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { ORIGINAL_TRAN_CODE_VIEW } from '../constants';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

const spyClientConfiguration = mockActionCreator(clientConfigurationActions);

const store = createStore(rootReducer, {
  adjustmentTransactionConfig: {
    adjustmentConfigurationList: {
      data: [
        {
          active: true
        }
      ]
    },
    addAdjustmentConfiguration: {
      formIdList: ['form_1', 'form_2', 'form_3']
    }
  },
  form: {
    [ORIGINAL_TRAN_CODE_VIEW]: {
      values: { originalTransactionCode: 'original_code' }
    }
  }
});

beforeEach(() => {
  spyClientConfiguration('getClientInfoData');
});

describe('updateAdjustmentConfiguration', () => {
  it('isFulfilled', async () => {
    const response = await updateAdjustmentConfiguration({ data: [] })(
      byPassFulfilled(updateAdjustmentConfiguration.fulfilled.type, {}),
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('isRejected getClientInfoData', async () => {
    spyClientConfiguration('getClientInfoData', () => specificName);

    const response = await updateAdjustmentConfiguration({ data: [] })(
      byPassRejectedSpecificAction(
        updateAdjustmentConfiguration.rejected.type,
        {}
      ),
      store.getState,
      {}
    );

    expect(response.payload).toEqual({});
  });

  it('isRejected updateClientInfo', async () => {
    spyClientConfiguration('updateClientInfo', () => specificName);

    const response = await updateAdjustmentConfiguration({ data: [] })(
      byPassRejectedSpecificAction(
        updateAdjustmentConfiguration.rejected.type,
        {}
      ),
      store.getState,
      {}
    );

    expect(response.payload).toEqual({});
  });
});
