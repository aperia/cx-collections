import { byPassFulfilled, byPassRejected, mockActionCreator } from "app/test-utils";
import { actionsToast } from "pages/__commons/ToastNotifications/_redux";
import { createStore, Store } from "redux";
import { rootReducer } from "storeConfig";
import { I18N_ADJUSTMENT_TRANSACTION } from "../constants";
import { triggerDeleteAdjustmentConfiguration } from "./deleteAdjustmentConfiguration";

let store: Store<RootState>;

const toastSpy = mockActionCreator(actionsToast);

beforeEach(() => {
  store = createStore(rootReducer, {});
});

describe('Test triggerDeleteAdjustmentConfiguration', () => {
  it('isFulfilled', async () => {
    const mockAddToast = toastSpy('addToast');

    await triggerDeleteAdjustmentConfiguration()(
      byPassFulfilled(triggerDeleteAdjustmentConfiguration.fulfilled.type),
      store.getState,
      {}
    );

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_DELETED
    });
  });

  it('isRejected', async () => {
    const mockAddToast = toastSpy('addToast');

    await triggerDeleteAdjustmentConfiguration()(
      byPassRejected(triggerDeleteAdjustmentConfiguration.rejected.type),
      store.getState,
      {}
    );

    expect(mockAddToast).toBeCalledWith({
      show: true,
          type: 'error',
          message: I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_FAILED_TO_DELETE
    });
  });
});

describe('Test triggerDeleteAdjustmentConfiguration', () => {
  it('Should have fulfilled', async () => {
    const actual = rootReducer(store.getState(), {
      type: triggerDeleteAdjustmentConfiguration.fulfilled.type,
      payload: {
        data: undefined,
        callback: jest.fn()
      }
    }).adjustmentTransactionConfig;

    expect(actual.deleteAdjustmentConfiguration.loading).toEqual(false);
  });

  it('Should have pending', async () => {
    const actual = rootReducer(store.getState(), {
      type: triggerDeleteAdjustmentConfiguration.pending.type
    }).adjustmentTransactionConfig;

    expect(actual.deleteAdjustmentConfiguration.loading).toEqual(true);
  });

  it('Should have reject', async () => {
    const actual = rootReducer(store.getState(), {
      type: triggerDeleteAdjustmentConfiguration.rejected.type,
      error: { message: 'test error' }
    }).adjustmentTransactionConfig;

    expect(actual.deleteAdjustmentConfiguration.loading).toEqual(false);
  });
});