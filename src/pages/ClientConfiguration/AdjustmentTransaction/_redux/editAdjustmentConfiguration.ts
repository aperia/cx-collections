import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import isEqual from 'lodash.isequal';
import differenceWith from 'lodash.differencewith';
import { compareAdjustmentCodes, getViewErrorId } from '../helpers';

import {
  CLIENT_CONFIG_SINGLE_FORM_VIEW,
  I18N_ADJUSTMENT_TRANSACTION,
  ORIGINAL_TRAN_CODE_VIEW
} from '../constants';

import {
  AdjustmentConfigurationItem,
  AdjustmentTransactionState
} from '../types';
import { adjustmentTransactionActions } from './reducers';
import { updateAdjustmentConfiguration } from './updateAdjustmentConfiguration';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

export const checkError: AppThunk<boolean> = () => (dispatch, getState) => {
  const { adjustmentTransactionConfig } = getState();
  const configList =
    adjustmentTransactionConfig.adjustmentConfigurationList.data.filter(
      item => item?.active
    );

  const currentRecord =
    adjustmentTransactionConfig.editAdjustmentConfiguration.currentRecord;
  const { form } = getState();
  const newRecord = {
    ...form[ORIGINAL_TRAN_CODE_VIEW].values,
    ...form[CLIENT_CONFIG_SINGLE_FORM_VIEW].values
  } as AdjustmentConfigurationItem;

  // check same as original code
  if (
    newRecord.originalTransactionCode === newRecord.adjustmentPostingTranCode
  ) {
    dispatch(
      adjustmentTransactionActions.setErrorEditAdjustmentConfig(
        I18N_ADJUSTMENT_TRANSACTION.ADD_ADJUSTMENT_SAME_AS_TRANSACTION_CODE_INLINE_MSG
      )
    );
    return true;
  }

  // check record is already exists
  const formKy = 'fakeFormKy';
  const newConfigList = configList.filter(item => {
    return !isEqual(item, currentRecord);
  });
  const viewErrorIds = getViewErrorId(newConfigList, { [formKy]: newRecord });

  if (viewErrorIds[0] === formKy) {
    dispatch(
      adjustmentTransactionActions.setErrorEditAdjustmentConfig(
        I18N_ADJUSTMENT_TRANSACTION.ADD_ADJUSTMENT_INLINE_MSG
      )
    );
    return true;
  }
  return false;
};

export const triggerEditAdjustmentConfiguration = createAsyncThunk<
  unknown,
  undefined,
  ThunkAPIConfig
>(
  'adjustmentTransaction/triggerEditAdjustmentConfiguration',
  async (args, thunkAPI) => {
    const { dispatch, getState, rejectWithValue } = thunkAPI;
    const error = dispatch(checkError());
    if (error) return rejectWithValue({});

    const { adjustmentTransactionConfig, form } = getState();

    const newRecord = {
      ...form[ORIGINAL_TRAN_CODE_VIEW].values,
      ...form[CLIENT_CONFIG_SINGLE_FORM_VIEW].values
    } as AdjustmentConfigurationItem;

    const prevRecord =
      adjustmentTransactionConfig?.editAdjustmentConfiguration?.currentRecord;
    if (!prevRecord) return;

    const allAdjustments =
      getState().adjustmentTransactionConfig.adjustmentConfigurationList.data;

    // remove same adjustment codes but active:false;
    const existingAdjustment = differenceWith(
      allAdjustments,
      [prevRecord, newRecord],
      compareAdjustmentCodes
    );

    existingAdjustment.push(newRecord);
    const response = await dispatch(
      updateAdjustmentConfiguration({
        data: existingAdjustment
      })
    );

    if (isFulfilled(response)) {
      batch(() => {
        dispatch(
          changeHistoryActions.saveChangedClientConfig({
            action: 'UPDATE',
            changedCategory: 'adjustmentTransaction',
            newValue: [newRecord],
            oldValue: [prevRecord]
          })
        );
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_UPDATED
          })
        );
        dispatch(adjustmentTransactionActions.getAdjustmentConfiguration());
      });
    }

    if (isRejected(response)) {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_FAILED_TO_UPDATE
        })
      );
      return rejectWithValue({});
    }
  }
);

export const editAdjustmentConfigurationBuilder = (
  builder: ActionReducerMapBuilder<AdjustmentTransactionState>
) => {
  builder
    .addCase(
      triggerEditAdjustmentConfiguration.pending,
      (draftState, action) => {
        draftState.editAdjustmentConfiguration = {
          ...draftState.editAdjustmentConfiguration,
          loading: true
        };
      }
    )
    .addCase(
      triggerEditAdjustmentConfiguration.fulfilled,
      (draftState, action) => {
        draftState.editAdjustmentConfiguration = {
          ...draftState.editAdjustmentConfiguration,
          loading: false,
          error: '',
          openModal: false,
          validateError: ''
        };
      }
    )
    .addCase(
      triggerEditAdjustmentConfiguration.rejected,
      (draftState, action) => {
        draftState.editAdjustmentConfiguration = {
          ...draftState.editAdjustmentConfiguration,
          loading: false,
          error: action.error.message
        };
      }
    );
};
