import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { orderBy } from 'app/_libraries/_dls/lodash';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

// import { adjustmentTransactionService } from '../adjustmentTransactionService';

import { ADJUSTMENT_CONFIGURATION_GRID_COLUMN_NAME } from '../constants';
import {
  AdjustmentConfigurationItem,
  AdjustmentTransactionState
} from '../types';

export const getAdjustmentConfiguration = createAsyncThunk<
  AdjustmentConfigurationItem[],
  undefined,
  ThunkAPIConfig
>(
  'adjustmentTransaction/getAdjustmentConfiguration',
  async (args, thunkAPI) => {
    const { dispatch, getState } = thunkAPI;
    const response = await dispatch(
      clientConfigurationActions.getClientInfoData({
        dataType: 'adjustments'
      })
    );

    if (isFulfilled(response)) {
      const adjustmentModel =
        getState().mapping.data?.clientConfigurationAdjustmentTransaction || {};

      const adjustments = response.payload.adjustments?.adjustmentCodes || [];
      return orderBy(
        mappingDataFromArray(adjustments, adjustmentModel),
        ADJUSTMENT_CONFIGURATION_GRID_COLUMN_NAME.ORIGINAL_TRAN_CODE,
        'asc'
      );
    }

    return thunkAPI.rejectWithValue({});
  }
);

export const getAdjustmentConfigurationBuilder = (
  builder: ActionReducerMapBuilder<AdjustmentTransactionState>
) => {
  builder
    .addCase(getAdjustmentConfiguration.pending, draftState => {
      draftState.adjustmentConfigurationList = {
        ...draftState.adjustmentConfigurationList,
        loading: true,
        data: []
      };
    })
    .addCase(getAdjustmentConfiguration.fulfilled, (draftState, action) => {
      draftState.adjustmentConfigurationList = {
        ...draftState.adjustmentConfigurationList,
        loading: false,
        data: action.payload,
        error: ''
      };
    })
    .addCase(getAdjustmentConfiguration.rejected, (draftState, action) => {
      draftState.adjustmentConfigurationList = {
        ...draftState.adjustmentConfigurationList,
        loading: false,
        error: action.error.message
      };
    });
};
