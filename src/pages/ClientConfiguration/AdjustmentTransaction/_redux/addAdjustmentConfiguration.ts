import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';
import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import differenceWith from 'lodash.differencewith';

import {
  ADD_ADJUSTMENT_ERR_TYPE,
  CLIENT_CONFIG_SINGLE_FORM_VIEW,
  DEFAULT_FORM_ID_LIST,
  I18N_ADJUSTMENT_TRANSACTION,
  ORIGINAL_TRAN_CODE_VIEW
} from '../constants';
import {
  compareAdjustmentCodes,
  getDuplicatePostingTranCodeAndBatchCode,
  getViewErrorId
} from '../helpers';

import {
  AdjustmentConfigurationItem,
  AdjustmentTransactionState
} from '../types';
import { adjustmentTransactionActions } from './reducers';
import { updateAdjustmentConfiguration } from './updateAdjustmentConfiguration';

export const checkError: AppThunk<AdjustmentConfigurationItem[]> =
  () => (dispatch, getState) => {
    const { form, adjustmentTransactionConfig } = getState();
    const adjustmentTransactionConfigList =
      adjustmentTransactionConfig.adjustmentConfigurationList.data.filter(
        item => item?.active
      );
    const { formIdList } =
      adjustmentTransactionConfig.addAdjustmentConfiguration;
    const clientConfigAdjustmentOriginalTransactionCode =
      form[ORIGINAL_TRAN_CODE_VIEW]?.values;
    if (!clientConfigAdjustmentOriginalTransactionCode) return [];

    const { originalTransactionCode } =
      clientConfigAdjustmentOriginalTransactionCode;
    const data: AdjustmentConfigurationItem[] = [];
    const viewDataObj: Record<string, AdjustmentConfigurationItem> = {};
    const sameOriginalTranCodeList: string[] = [];
    formIdList.forEach((item: string) => {
      const formKy = `${CLIENT_CONFIG_SINGLE_FORM_VIEW}-${item}`;
      const formValues = form[formKy]?.values as
        | Omit<AdjustmentConfigurationItem, 'originalTransactionCode'>
        | undefined;
      if (formValues) {
        data.push({
          originalTransactionCode,
          ...formValues
        });
        if (formValues.adjustmentPostingTranCode === originalTransactionCode) {
          sameOriginalTranCodeList.push(item);
        }
        viewDataObj[item] = { originalTransactionCode, ...formValues };
      }
    });

    if (sameOriginalTranCodeList.length) {
      dispatch(
        adjustmentTransactionActions.setErrorAddAdjustmentConfig({
          type: ADD_ADJUSTMENT_ERR_TYPE.SAME_AS_ORIGINAL_CODE,
          idList: sameOriginalTranCodeList
        })
      );
      return [];
    }

    const duplicateTranCode =
      getDuplicatePostingTranCodeAndBatchCode(viewDataObj);

    if (duplicateTranCode.length) {
      dispatch(
        adjustmentTransactionActions.setErrorAddAdjustmentConfig({
          type: ADD_ADJUSTMENT_ERR_TYPE.DUPLICATE_POSTING_TRAN_CODE,
          idList: duplicateTranCode
        })
      );
      return [];
    }

    const idErrorList = getViewErrorId(
      adjustmentTransactionConfigList,
      viewDataObj
    );

    if (idErrorList.length === 1) {
      dispatch(
        adjustmentTransactionActions.setErrorAddAdjustmentConfig({
          type: ADD_ADJUSTMENT_ERR_TYPE.ONE_COMBINE_ALREADY_EXISTS,
          idList: idErrorList
        })
      );
      return [];
    }

    if (idErrorList.length > 1) {
      dispatch(
        adjustmentTransactionActions.setErrorAddAdjustmentConfig({
          type: ADD_ADJUSTMENT_ERR_TYPE.MULTIPLE_COMBINE_ALREADY_EXISTS,
          idList: idErrorList
        })
      );
      return [];
    }

    return data;
  };

export const triggerAddAdjustmentConfiguration = createAsyncThunk<
  unknown,
  undefined,
  ThunkAPIConfig
>(
  'adjustmentTransaction/triggerAddAdjustmentConfiguration',
  async (args, thunkAPI) => {
    const { dispatch, getState, rejectWithValue } = thunkAPI;
    const addAdjustments = dispatch(checkError());

    if (!addAdjustments.length) return rejectWithValue({});

    const allAdjustments =
      getState().adjustmentTransactionConfig.adjustmentConfigurationList.data;

    // remove same adjustment codes but active:false;
    const existingAdjustment = differenceWith(
      allAdjustments,
      addAdjustments,
      compareAdjustmentCodes
    );

    const adjustments = [...existingAdjustment, ...addAdjustments];

    const response = await dispatch(
      updateAdjustmentConfiguration({ data: adjustments })
    );
    //originalTransactionCode
    if (isFulfilled(response)) {
      batch(() => {
        dispatch(
          changeHistoryActions.saveChangedClientConfig({
            action: 'ADD',
            changedCategory: 'adjustmentTransaction',
            newValue: addAdjustments
          })
        );

        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_ADDED,
            msgVariables: { count: addAdjustments.length }
          })
        );
        dispatch(adjustmentTransactionActions.getAdjustmentConfiguration());
      });
    }

    if (isRejected(response)) {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_FAILED_TO_ADD,
          msgVariables: { count: addAdjustments.length }
        })
      );

      return rejectWithValue({});
    }
  }
);

export const triggerAddAdjustmentConfigurationBuilder = (
  builder: ActionReducerMapBuilder<AdjustmentTransactionState>
) => {
  builder
    .addCase(
      triggerAddAdjustmentConfiguration.pending,
      (draftState, action) => {
        draftState.addAdjustmentConfiguration = {
          ...draftState.addAdjustmentConfiguration,
          loading: true
        };
      }
    )
    .addCase(
      triggerAddAdjustmentConfiguration.fulfilled,
      (draftState, action) => {
        draftState.addAdjustmentConfiguration = {
          ...draftState.addAdjustmentConfiguration,
          loading: false,
          error: '',
          openModal: false,
          validateError: undefined,
          formIdList: DEFAULT_FORM_ID_LIST
        };
      }
    )
    .addCase(
      triggerAddAdjustmentConfiguration.rejected,
      (draftState, action) => {
        draftState.addAdjustmentConfiguration = {
          ...draftState.addAdjustmentConfiguration,
          loading: false,
          error: action.error.message
        };
      }
    );
};
