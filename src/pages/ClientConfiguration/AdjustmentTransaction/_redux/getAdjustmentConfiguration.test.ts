import { byPassFulfilled, byPassRejected } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { getAdjustmentConfiguration } from './getAdjustmentConfiguration';

const store = createStore(rootReducer, {});

describe('Test getAdjustmentConfiguration', () => {
  it('should fulfilled', async () => {
    const response = await getAdjustmentConfiguration()(
      byPassFulfilled(getAdjustmentConfiguration.fulfilled.type, {}),
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'adjustmentTransaction/getAdjustmentConfiguration/fulfilled'
    );
    expect(response?.payload).toEqual([]);
  });

  it('should rejected', async () => {

    const response = await getAdjustmentConfiguration()(
      byPassRejected(getAdjustmentConfiguration.fulfilled.type, {}),
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'adjustmentTransaction/getAdjustmentConfiguration/rejected'
    );
    expect(response?.payload).toEqual({});
  });

  it('Should have fulfilled', async () => {
    const actual = rootReducer(store.getState(), {
      type: 'adjustmentTransaction/getAdjustmentConfiguration/fulfilled',
      payload: {
        data: undefined,
        callback: jest.fn()
      }
    }).adjustmentTransactionConfig;

    expect(actual.adjustmentConfigurationList.loading).toEqual(false);
  });

  it('Should have pending', async () => {
    const actual = rootReducer(store.getState(), {
      type: 'adjustmentTransaction/getAdjustmentConfiguration/pending'
    }).adjustmentTransactionConfig;

    expect(actual.adjustmentConfigurationList.loading).toEqual(true);
  });

  it('Should have reject', async () => {
    const actual = rootReducer(store.getState(), {
      type: 'adjustmentTransaction/getAdjustmentConfiguration/rejected',
      error: { message: 'test error' }
    }).adjustmentTransactionConfig;

    expect(actual.adjustmentConfigurationList.loading).toEqual(false);
  });
});