import { ADD_ADJUSTMENT_ERR_TYPE, DEFAULT_FORM_ID_LIST } from '../constants';
import { AdjustmentTransactionState } from '../types';
import { adjustmentTransactionActions, reducer } from './reducers';

describe('test adjustment transaction reducers', () => {
  const MOCK_DEFAULT_FORM_ID_LIST = ['123', '12', '1'];
  const initialState: AdjustmentTransactionState = {
    addAdjustmentConfiguration: {
      formIdList: MOCK_DEFAULT_FORM_ID_LIST,
      openModal: false,
      loading: false
    },
    adjustmentConfigurationList: { loading: false, data: [], error: '' },
    deleteAdjustmentConfiguration: {
      openModal: false,
      loading: false,
      currentRecord: undefined
    },
    editAdjustmentConfiguration: {
      openModal: false,
      loading: false,
      validateError: '',
      currentRecord: {
        originalTransactionCode: '123',
        adjustmentPostingTranCode: '234',
        adjustmentPostingBatchCode: 'VV',
        adjustmentDescription: 'mock Description'
      }
    }
  };

  const mockCurrentRecord = {
    originalTransactionCode: '123',
    adjustmentPostingTranCode: '234',
    adjustmentPostingBatchCode: 'VV',
    adjustmentDescription: 'mock Description'
  };

  const mockValidateError = {
    type: ADD_ADJUSTMENT_ERR_TYPE.DUPLICATE_POSTING_TRAN_CODE,
    idList: ['1']
  };

  it('setErrorAddAdjustmentConfig', () => {
    const nextState = reducer(
      initialState,
      adjustmentTransactionActions.setErrorAddAdjustmentConfig(
        mockValidateError
      )
    );
    expect(nextState.addAdjustmentConfiguration.validateError).toEqual(
      mockValidateError
    );
  });

  it('addAdjustmentConfigRequest', () => {
    const nextState = reducer(
      initialState,
      adjustmentTransactionActions.addAdjustmentConfigRequest()
    );
    expect(nextState.addAdjustmentConfiguration.openModal).toEqual(true);
  });

  it('cancelAddAdjustmentConfig', () => {
    const nextState = reducer(
      initialState,
      adjustmentTransactionActions.cancelAddAdjustmentConfig()
    );
    expect(nextState.addAdjustmentConfiguration.openModal).toEqual(false);
    expect(nextState.addAdjustmentConfiguration.formIdList).toEqual(
      DEFAULT_FORM_ID_LIST
    );
    expect(nextState.addAdjustmentConfiguration.validateError).toEqual(
      undefined
    );
  });

  it('addAddAdjustmentConfigForm', () => {
    const mockId = '2';
    const nextState = reducer(
      initialState,
      adjustmentTransactionActions.addAddAdjustmentConfigForm(mockId)
    );

    expect(nextState.addAdjustmentConfiguration.formIdList).toEqual([
      ...MOCK_DEFAULT_FORM_ID_LIST,
      '2'
    ]);
  });

  describe('removeAddAdjustmentConfigForm', () => {
    it('removeAddAdjustmentConfigForm with no init validateError', () => {
      const removeId = MOCK_DEFAULT_FORM_ID_LIST[1];
      const nextState = reducer(
        initialState,
        adjustmentTransactionActions.removeAddAdjustmentConfigForm(removeId)
      );

      expect(nextState.addAdjustmentConfiguration.formIdList).toEqual(
        MOCK_DEFAULT_FORM_ID_LIST.filter(id => id !== removeId)
      );
    });

    it('removeAddAdjustmentConfigForm, validateError list id include remove id', () => {
      const removeId = MOCK_DEFAULT_FORM_ID_LIST[2];
      const nextState = reducer(
        {
          ...initialState,
          addAdjustmentConfiguration: {
            ...initialState.addAdjustmentConfiguration,
            validateError: mockValidateError
          }
        },
        adjustmentTransactionActions.removeAddAdjustmentConfigForm(removeId)
      );

      expect(nextState.addAdjustmentConfiguration.formIdList).toEqual(
        MOCK_DEFAULT_FORM_ID_LIST.filter(id => id !== removeId)
      );
      expect(nextState.addAdjustmentConfiguration.validateError).toEqual(
        undefined
      );
    });

    it('removeAddAdjustmentConfigForm has init validateError', () => {
      const removeId = MOCK_DEFAULT_FORM_ID_LIST[1];
      const nextState = reducer(
        {
          ...initialState,
          addAdjustmentConfiguration: {
            ...initialState.addAdjustmentConfiguration,
            validateError: mockValidateError
          }
        },
        adjustmentTransactionActions.removeAddAdjustmentConfigForm(removeId)
      );

      expect(nextState.addAdjustmentConfiguration.formIdList).toEqual(
        MOCK_DEFAULT_FORM_ID_LIST.filter(id => id !== removeId)
      );
      expect(nextState.addAdjustmentConfiguration.validateError).toEqual(
        mockValidateError
      );
    });
  });

  it('deleteAdjustmentConfigRequest', () => {
    const nextState = reducer(
      initialState,
      adjustmentTransactionActions.deleteAdjustmentConfigRequest(
        mockCurrentRecord
      )
    );

    expect(nextState.deleteAdjustmentConfiguration.openModal).toEqual(true);
    expect(nextState.deleteAdjustmentConfiguration.currentRecord).toEqual(
      mockCurrentRecord
    );
  });

  it('cancelDeleteAdjustmentConfig', () => {
    const nextState = reducer(
      initialState,
      adjustmentTransactionActions.cancelDeleteAdjustmentConfig()
    );

    expect(nextState.deleteAdjustmentConfiguration.openModal).toEqual(false);
    expect(nextState.deleteAdjustmentConfiguration.currentRecord).toEqual(
      undefined
    );
  });

  it('editAdjustmentConfigRequest', () => {
    const nextState = reducer(
      initialState,
      adjustmentTransactionActions.editAdjustmentConfigRequest(
        mockCurrentRecord
      )
    );

    expect(nextState.editAdjustmentConfiguration.openModal).toEqual(true);
    expect(nextState.editAdjustmentConfiguration.currentRecord).toEqual(
      mockCurrentRecord
    );
  });

  it('cancelEditAdjustmentConfig', () => {
    const nextState = reducer(
      initialState,
      adjustmentTransactionActions.cancelEditAdjustmentConfig()
    );

    expect(nextState.editAdjustmentConfiguration.openModal).toEqual(false);
    expect(nextState.editAdjustmentConfiguration.currentRecord).toEqual(
      undefined
    );
  });

  it('setErrorEditAdjustmentConfig', () => {
    const nextState = reducer(
      initialState,
      adjustmentTransactionActions.setErrorEditAdjustmentConfig(
        mockValidateError
      )
    );

    expect(nextState.editAdjustmentConfiguration.validateError).toEqual(
      mockValidateError
    );
  });
});
