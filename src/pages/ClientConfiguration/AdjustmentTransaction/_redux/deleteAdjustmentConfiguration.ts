import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import findIndex from 'lodash.findindex';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';

import { I18N_ADJUSTMENT_TRANSACTION } from '../constants';

import {
  AdjustmentConfigurationItem,
  AdjustmentTransactionState
} from '../types';
import { adjustmentTransactionActions } from './reducers';
import { updateAdjustmentConfiguration } from './updateAdjustmentConfiguration';

export const triggerDeleteAdjustmentConfiguration = createAsyncThunk<
  unknown,
  undefined,
  ThunkAPIConfig
>(
  'adjustmentTransaction/triggerDeleteAdjustmentConfiguration',
  async (args, thunkAPI) => {
    const { getState, dispatch, rejectWithValue } = thunkAPI;
    const { adjustmentTransactionConfig } = getState();
    const deleteItem = adjustmentTransactionConfig.deleteAdjustmentConfiguration
      .currentRecord as AdjustmentConfigurationItem;

    const existingAdjustmentList =
      adjustmentTransactionConfig.adjustmentConfigurationList.data;

    const deleteIndex = findIndex(existingAdjustmentList, deleteItem);

    const newUpdateData = [...existingAdjustmentList];
    newUpdateData.splice(deleteIndex, 1, { ...deleteItem, active: false });

    const response = await dispatch(
      updateAdjustmentConfiguration({
        data: newUpdateData,
        updateAction: 'delete'
      })
    );

    if (isFulfilled(response)) {
      batch(() => {
        dispatch(
          changeHistoryActions.saveChangedClientConfig({
            action: 'DELETE',
            changedCategory: 'adjustmentTransaction',
            oldValue: [existingAdjustmentList[deleteIndex]]
          })
        );
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_DELETED
          })
        );
        dispatch(adjustmentTransactionActions.getAdjustmentConfiguration());
      });
    }

    if (isRejected(response)) {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_FAILED_TO_DELETE
        })
      );
      return rejectWithValue({});
    }
  }
);

export const deleteAdjustmentConfigurationBuilder = (
  builder: ActionReducerMapBuilder<AdjustmentTransactionState>
) => {
  builder
    .addCase(
      triggerDeleteAdjustmentConfiguration.pending,
      (draftState, action) => {
        draftState.deleteAdjustmentConfiguration = {
          ...draftState.deleteAdjustmentConfiguration,
          loading: true
        };
      }
    )
    .addCase(
      triggerDeleteAdjustmentConfiguration.fulfilled,
      (draftState, action) => {
        draftState.deleteAdjustmentConfiguration = {
          ...draftState.deleteAdjustmentConfiguration,
          loading: false,
          openModal: false
        };
      }
    )
    .addCase(
      triggerDeleteAdjustmentConfiguration.rejected,
      (draftState, action) => {
        draftState.deleteAdjustmentConfiguration = {
          ...draftState.deleteAdjustmentConfiguration,
          loading: false,
          openModal: false
        };
      }
    );
};
