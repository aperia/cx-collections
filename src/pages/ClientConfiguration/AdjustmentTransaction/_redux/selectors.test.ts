import { selectorWrapper } from 'app/test-utils';
import { ADD_ADJUSTMENT_ERR_TYPE } from '../constants';

import {
  selectValidateErrorAddAdjustment,
  selectAdjustmentConfigurationLoading,
  selectAdjustmentConfigurationData,
  selectAdjustmentConfigurationError,
  selectFormIdList,
  selectAddAdjustmentLoading,
  selectAddModalOpen,
  selectDeleteModalOpen,
  selectDeleteLoading,
  selectDeleteRecord,
  selectEditModalOpen,
  selectEditLoading,
  selectEditRecord,
  selectEditSingleFormRecord,
  selectEditError
} from './selectors';

const store = {
  adjustmentTransactionConfig: {
    addAdjustmentConfiguration: {
      validateError: {
        type: ADD_ADJUSTMENT_ERR_TYPE.DUPLICATE_POSTING_TRAN_CODE,
        idList: ['1']
      },
      formIdList: ['1'],
      openModal: false,
      loading: false
    },
    adjustmentConfigurationList: { loading: false, data: [], error: '' },
    deleteAdjustmentConfiguration: {
      openModal: false,
      loading: false,
      currentRecord: undefined
    },
    editAdjustmentConfiguration: {
      openModal: false,
      loading: false,
      validateError: '',
      currentRecord: {
        originalTransactionCode: '123',
        adjustmentPostingTranCode: '234',
        adjustmentPostingBatchCode: 'VV',
        adjustmentDescription: 'mock Description'
      }
    }
  }
};

describe('test adjustment transaction selectors', () => {
  it('selectValidateErrorAddAdjustment', () => {
    const { data } = selectorWrapper(store)(selectValidateErrorAddAdjustment);

    expect(data).toEqual(
      store.adjustmentTransactionConfig.addAdjustmentConfiguration.validateError
    );
  });

  it('selectAdjustmentConfigurationLoading', () => {
    const { data } = selectorWrapper(store)(
      selectAdjustmentConfigurationLoading
    );

    expect(data).toEqual(
      store.adjustmentTransactionConfig.adjustmentConfigurationList.loading
    );
  });

  it('selectAdjustmentConfigurationData', () => {
    const { data } = selectorWrapper(store)(selectAdjustmentConfigurationData);

    expect(data).toEqual(
      store.adjustmentTransactionConfig.adjustmentConfigurationList.data
    );
  });

  it('selectAdjustmentConfigurationError', () => {
    const { data } = selectorWrapper(store)(selectAdjustmentConfigurationError);

    expect(data).toEqual(
      store.adjustmentTransactionConfig.adjustmentConfigurationList.error
    );
  });

  it('selectFormIdList', () => {
    const { data } = selectorWrapper(store)(selectFormIdList);

    expect(data).toEqual(
      store.adjustmentTransactionConfig.addAdjustmentConfiguration.formIdList
    );
  });

  it('selectAddAdjustmentLoading', () => {
    const { data } = selectorWrapper(store)(selectAddAdjustmentLoading);

    expect(data).toEqual(
      store.adjustmentTransactionConfig.addAdjustmentConfiguration.loading
    );
  });

  it('selectAddModalOpen', () => {
    const { data } = selectorWrapper(store)(selectAddModalOpen);

    expect(data).toEqual(
      store.adjustmentTransactionConfig.addAdjustmentConfiguration.openModal
    );
  });

  it('selectDeleteModalOpen', () => {
    const { data } = selectorWrapper(store)(selectDeleteModalOpen);

    expect(data).toEqual(
      store.adjustmentTransactionConfig.deleteAdjustmentConfiguration.openModal
    );
  });

  it('selectDeleteLoading', () => {
    const { data } = selectorWrapper(store)(selectDeleteLoading);

    expect(data).toEqual(
      store.adjustmentTransactionConfig.deleteAdjustmentConfiguration.loading
    );
  });

  it('selectDeleteRecord', () => {
    const { data } = selectorWrapper(store)(selectDeleteRecord);

    expect(data).toEqual(
      store.adjustmentTransactionConfig.deleteAdjustmentConfiguration
        .currentRecord
    );
  });

  it('selectEditModalOpen', () => {
    const { data } = selectorWrapper(store)(selectEditModalOpen);

    expect(data).toEqual(
      store.adjustmentTransactionConfig.editAdjustmentConfiguration.openModal
    );
  });

  it('selectEditLoading', () => {
    const { data } = selectorWrapper(store)(selectEditLoading);

    expect(data).toEqual(
      store.adjustmentTransactionConfig.editAdjustmentConfiguration.loading
    );
  });

  it('selectEditRecord', () => {
    const { data } = selectorWrapper(store)(selectEditRecord);

    expect(data).toEqual(
      store.adjustmentTransactionConfig.editAdjustmentConfiguration
        .currentRecord
    );
  });

  it('selectEditSingleFormRecord', () => {
    const { data } = selectorWrapper(store)(selectEditSingleFormRecord);

    expect(data).toEqual({
      adjustmentPostingTranCode: '234',
      adjustmentPostingBatchCode: 'VV',
      adjustmentDescription: 'mock Description'
    });
  });

  it('selectEditError', () => {
    const { data } = selectorWrapper(store)(selectEditError);

    expect(data).toEqual(
      store.adjustmentTransactionConfig.editAdjustmentConfiguration
        .validateError
    );
  });
});
