import { createSlice } from '@reduxjs/toolkit';

// types
import { AdjustmentTransactionState } from '../types';

import {
  getAdjustmentConfiguration,
  getAdjustmentConfigurationBuilder
} from './getAdjustmentConfiguration';

import {
  triggerAddAdjustmentConfiguration,
  triggerAddAdjustmentConfigurationBuilder
} from './addAdjustmentConfiguration';

import { DEFAULT_FORM_ID_LIST } from '../constants';
import {
  triggerDeleteAdjustmentConfiguration,
  deleteAdjustmentConfigurationBuilder
} from './deleteAdjustmentConfiguration';
import {
  editAdjustmentConfigurationBuilder,
  triggerEditAdjustmentConfiguration
} from './editAdjustmentConfiguration';

const initialState: AdjustmentTransactionState = {
  adjustmentConfigurationList: { loading: false, data: [], error: '' },
  addAdjustmentConfiguration: {
    loading: false,
    openModal: false,
    formIdList: DEFAULT_FORM_ID_LIST
  },
  deleteAdjustmentConfiguration: {
    openModal: false,
    loading: false
  },
  editAdjustmentConfiguration: {
    openModal: false,
    loading: false,
    validateError: ''
  }
};

const { actions, reducer } = createSlice({
  name: 'adjustmentTransactionConfiguration',
  initialState,
  reducers: {
    setErrorAddAdjustmentConfig: (draftState, action) => {
      draftState.addAdjustmentConfiguration.validateError = action.payload;
    },
    addAdjustmentConfigRequest: draftState => {
      draftState.addAdjustmentConfiguration = {
        ...draftState.addAdjustmentConfiguration,
        openModal: true
      };
    },
    cancelAddAdjustmentConfig: draftState => {
      draftState.addAdjustmentConfiguration = {
        ...draftState.addAdjustmentConfiguration,
        openModal: false,
        formIdList: DEFAULT_FORM_ID_LIST,
        validateError: undefined
      };
    },
    addAddAdjustmentConfigForm: (draftState, action) => {
      draftState.addAdjustmentConfiguration.formIdList = [
        ...draftState.addAdjustmentConfiguration.formIdList,
        action.payload
      ];
    },
    removeAddAdjustmentConfigForm: (draftState, action) => {
      const removeId = action.payload;
      draftState.addAdjustmentConfiguration.formIdList =
        draftState.addAdjustmentConfiguration.formIdList.filter(
          id => id !== removeId
        );
      const validateError = draftState.addAdjustmentConfiguration.validateError;
      if (validateError === undefined) return;
      const newErrIdList = validateError.idList.filter(id => id !== removeId);
      if (!newErrIdList.length) {
        draftState.addAdjustmentConfiguration.validateError = undefined;
        return;
      }
      draftState.addAdjustmentConfiguration.validateError = {
        type: validateError.type,
        idList: newErrIdList
      };
    },
    deleteAdjustmentConfigRequest: (draftState, action) => {
      draftState.deleteAdjustmentConfiguration = {
        ...draftState.deleteAdjustmentConfiguration,
        openModal: true,
        currentRecord: action.payload
      };
    },
    cancelDeleteAdjustmentConfig: draftState => {
      draftState.deleteAdjustmentConfiguration = {
        ...draftState.deleteAdjustmentConfiguration,
        openModal: false,
        currentRecord: undefined
      };
    },
    editAdjustmentConfigRequest: (draftState, action) => {
      draftState.editAdjustmentConfiguration = {
        ...draftState.editAdjustmentConfiguration,
        openModal: true,
        currentRecord: action.payload
      };
    },
    cancelEditAdjustmentConfig: draftState => {
      draftState.editAdjustmentConfiguration = {
        ...draftState.editAdjustmentConfiguration,
        openModal: false,
        currentRecord: undefined,
        validateError: ''
      };
    },
    setErrorEditAdjustmentConfig: (draftState, action) => {
      draftState.editAdjustmentConfiguration.validateError = action.payload;
    }
  },
  extraReducers: builder => {
    getAdjustmentConfigurationBuilder(builder);
    deleteAdjustmentConfigurationBuilder(builder);
    editAdjustmentConfigurationBuilder(builder);
    triggerAddAdjustmentConfigurationBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getAdjustmentConfiguration,
  triggerAddAdjustmentConfiguration,
  triggerDeleteAdjustmentConfiguration,
  triggerEditAdjustmentConfiguration
};

export { allActions as adjustmentTransactionActions, reducer };
