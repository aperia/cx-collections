export const I18N_ADJUSTMENT_TRANSACTION = {
  NO_DATA: 'txt_no_data',
  ORIGINAL_TRAN_CODE: 'txt_original_tran_code',
  ORIGINAL_TRANSACTION_CODE: 'txt_original_transaction_code',
  ADJUSTMENT_POSTING_TRAN_CODE: 'txt_adjustment_posting_tran_code',
  ADJUSTMENT_POSTING_CODE: 'txt_adjustment_posting_code',
  ADJUSTMENT_POSTING_BATCH_CODE: 'txt_adjustment_posting_batch_code',
  ADJUSTMENT_DESCRIPTION: 'txt_adjustment_description',
  EDIT: 'txt_edit',
  DELETE: 'txt_delete',
  ACTIONS: 'txt_actions',
  ADJUSTMENT_TRANSACTION: 'txt_adjustment_transaction',
  ADJUSTMENT_CONFIGURATION: 'txt_adjustment_configuration',
  ADD_ADJUSTMENT: 'txt_add_adjustment',
  EDIT_ADJUSTMENT: 'txt_edit_adjustment',
  ADJUSTMENT_TYPE: 'txt_adjustment_type',
  ADD_ADJUSTMENT_TYPE: 'txt_add_adjustment_type',
  ADD_ADJUSTMENT_GUIDE: 'txt_add_adjustment_guide',
  ADD_ADJUSTMENT_INLINE_MSG: 'txt_add_adjustment_inline_msg',
  ADD_ADJUSTMENT_MULTIPLE_ERROR_INLINE_MSG:
    'txt_add_adjustment_multiple_error_inline_msg',
  ADD_ADJUSTMENT_SAME_AS_TRANSACTION_CODE_INLINE_MSG:
    'txt_add_adjustment_same_as_original_transaction_code_inline_msg',
  ADJUSTMENT_POSTING_TRAN_CODE_CANNOT_BE_THE_SAME:
    'txt_add_adjustment_adjustment_tran_code_cannot_be_the_same',
  ADJUSTMENT_ADDED: 'txt_adjustment_added',
  ADJUSTMENT_UPDATED: 'txt_adjustment_updated',
  ADJUSTMENT_FAILED_TO_ADD: 'txt_adjustment_failed_to_add',
  ADJUSTMENT_FAILED_TO_DELETE: 'txt_adjustment_failed_to_delete',
  ADJUSTMENT_FAILED_TO_UPDATE: 'txt_adjustment_failed_to_update',
  ADJUSTMENT_DELETED: 'txt_adjustment_deleted',
  CONFIRM_DELETE_ADJUSTMENT: 'txt_confirm_delete_adjustment',
  CONFIRM_DELETE_ADJUSTMENT_MSG: 'txt_delete_confirm_msg',
  ADJUSTMENT: 'txt_adjustment'
};

export const ADJUSTMENT_CONFIGURATION_GRID_COLUMN_NAME = {
  ORIGINAL_TRAN_CODE: 'originalTransactionCode',
  ADJUSTMENT_POSTING_TRAN_CODE: 'adjustmentPostingTranCode',
  ADJUSTMENT_POSTING_CODE: 'adjustmentPostingBatchCode',
  ADJUSTMENT_DESCRIPTION: 'adjustmentDescription'
};

export const DEFAULT_FORM_ID_LIST = ['1'];

export const ORIGINAL_TRAN_CODE_VIEW =
  'clientConfigAdjustmentOriginalTransactionCode';

export const CLIENT_CONFIG_SINGLE_FORM_VIEW =
  'clientConfigAdjustmentSingleForm';

export const ADD_ADJUSTMENT_ERR_TYPE = {
  DUPLICATE_POSTING_TRAN_CODE: 'duplicate',
  SAME_AS_ORIGINAL_CODE: 'same_as_original_code',
  ONE_COMBINE_ALREADY_EXISTS: 'one_combine_already_exists',
  MULTIPLE_COMBINE_ALREADY_EXISTS: 'multiple_combine_already_exists'
};

export const INLINE_MSG = {
  [ADD_ADJUSTMENT_ERR_TYPE.DUPLICATE_POSTING_TRAN_CODE]:
    I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_POSTING_TRAN_CODE_CANNOT_BE_THE_SAME,
  [ADD_ADJUSTMENT_ERR_TYPE.SAME_AS_ORIGINAL_CODE]:
    I18N_ADJUSTMENT_TRANSACTION.ADD_ADJUSTMENT_SAME_AS_TRANSACTION_CODE_INLINE_MSG,
  [ADD_ADJUSTMENT_ERR_TYPE.ONE_COMBINE_ALREADY_EXISTS]:
    I18N_ADJUSTMENT_TRANSACTION.ADD_ADJUSTMENT_INLINE_MSG,
  [ADD_ADJUSTMENT_ERR_TYPE.MULTIPLE_COMBINE_ALREADY_EXISTS]:
    I18N_ADJUSTMENT_TRANSACTION.ADD_ADJUSTMENT_MULTIPLE_ERROR_INLINE_MSG
};
