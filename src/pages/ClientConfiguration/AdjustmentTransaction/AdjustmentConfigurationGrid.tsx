import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import classNames from 'classnames';
import orderBy from 'lodash.orderby';

// components
import { FailedApiReload, NoDataGrid } from 'app/components';
import {
  ColumnType,
  Grid,
  GridRef,
  Pagination,
  SortType
} from 'app/_libraries/_dls/components';
import GridActions from 'pages/__commons/GridActions';
import { AddAdjustment } from './AddAdjustment';

// hooks
import { usePagination } from 'app/hooks';
import { adjustmentTransactionActions } from './_redux/reducers';
import {
  selectAdjustmentConfigurationData,
  selectAdjustmentConfigurationError,
  selectAdjustmentConfigurationLoading
} from './_redux/selectors';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import {
  ADJUSTMENT_CONFIGURATION_GRID_COLUMN_NAME,
  I18N_ADJUSTMENT_TRANSACTION
} from './constants';

import { AdjustmentConfigurationItem } from './types';
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';
import { genAmtId } from 'app/_libraries/_dls/utils';

const Header = ({ className }: { className?: string }) => {
  const { t } = useTranslation();
  return (
    <p
      className={classNames('fs-14 fw-500 color-grey-d20', className)}
      data-testid={genAmtId('clientConfig_adjustmentGrid', 'header', '')}
    >
      {t(I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_CONFIGURATION)}
    </p>
  );
};

const AdjustmentTransactionGrid = () => {
  const gridRefs = useRef<GridRef>(null);
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const status = useCheckClientConfigStatus();
  const isLoading = useSelector(selectAdjustmentConfigurationLoading);
  const isError = useSelector(selectAdjustmentConfigurationError);
  const adjustmentTransactionConfigData = useSelector(
    selectAdjustmentConfigurationData
  );
  const testId = 'clientConfig_adjustmentGrid';

  const [sortBy, setSortBy] = useState<SortType>({ id: '' });

  const handleSortChange = (sortVal: SortType) => {
    setSortBy(sortVal);
  };

  const gridDataOrdered = useMemo(() => {
    return orderBy(
      adjustmentTransactionConfigData,
      [
        item =>
          item[
            sortBy.id as keyof Omit<AdjustmentConfigurationItem, 'active'>
          ]?.toLowerCase(),
        item => item['originalTransactionCode']
      ],
      [sortBy.order || false, 'asc']
    );
  }, [adjustmentTransactionConfigData, sortBy]);

  const handleDeleteRequest = useCallback(
    record => {
      dispatch(
        adjustmentTransactionActions.deleteAdjustmentConfigRequest(record)
      );
    },
    [dispatch]
  );

  const handleEditRequest = useCallback(
    record => {
      dispatch(
        adjustmentTransactionActions.editAdjustmentConfigRequest(record)
      );
    },
    [dispatch]
  );

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(gridDataOrdered);

  const handleReload = useCallback(() => {
    dispatch(adjustmentTransactionActions.getAdjustmentConfiguration());
  }, [dispatch]);

  useEffect(() => {
    handleReload();
  }, [handleReload]);

  const showPagination = total > pageSize[0];

  const dataColumn = useMemo(() => {
    return [
      {
        id: ADJUSTMENT_CONFIGURATION_GRID_COLUMN_NAME.ORIGINAL_TRAN_CODE,
        Header: t(I18N_ADJUSTMENT_TRANSACTION.ORIGINAL_TRAN_CODE),
        accessor: ADJUSTMENT_CONFIGURATION_GRID_COLUMN_NAME.ORIGINAL_TRAN_CODE,
        isSort: true,
        width: 122
      },
      {
        id: ADJUSTMENT_CONFIGURATION_GRID_COLUMN_NAME.ADJUSTMENT_POSTING_TRAN_CODE,
        Header: t(I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_POSTING_TRAN_CODE),
        isSort: true,
        accessor:
          ADJUSTMENT_CONFIGURATION_GRID_COLUMN_NAME.ADJUSTMENT_POSTING_TRAN_CODE,
        width: 170
      },
      {
        id: ADJUSTMENT_CONFIGURATION_GRID_COLUMN_NAME.ADJUSTMENT_POSTING_CODE,
        Header: t(I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_POSTING_CODE),
        accessor:
          ADJUSTMENT_CONFIGURATION_GRID_COLUMN_NAME.ADJUSTMENT_POSTING_CODE,
        width: 198,
        isSort: true
      },
      {
        id: ADJUSTMENT_CONFIGURATION_GRID_COLUMN_NAME.ADJUSTMENT_DESCRIPTION,
        Header: t(I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_DESCRIPTION),
        accessor:
          ADJUSTMENT_CONFIGURATION_GRID_COLUMN_NAME.ADJUSTMENT_DESCRIPTION,
        isSort: true,
        width: 361,
        autoWidth: true
      },
      {
        id: 'action',
        Header: t(I18N_ADJUSTMENT_TRANSACTION.ACTIONS),
        accessor: (col: AdjustmentConfigurationItem, idx: number) => (
          <GridActions
            status={status}
            onClickEdit={() => handleEditRequest(col)}
            onClickDelete={() => handleDeleteRequest(col)}
            dataTestId={`${idx}_action_${testId}`}
          />
        ),
        width: 147,
        isFixedRight: true,
        cellProps: { className: 'text-center' },
        cellBodyProps: { className: 'text-center' },
        className: 'td-sm'
      }
    ];
  }, [t, handleDeleteRequest, handleEditRequest, status]);

  if (isLoading) return <div className="vh-50" />;

  if (isError)
    return (
      <>
        <Header className="mt-16" />
        <FailedApiReload
          id="transaction-adjustment-request-fail"
          className="mt-80"
          onReload={handleReload}
          dataTestId={`${testId}_failedApi`}
        />
      </>
    );

  if (!gridDataOrdered.length)
    return (
      <>
        <Header className="mt-16" />
        <NoDataGrid
          text={t(I18N_ADJUSTMENT_TRANSACTION.NO_DATA)}
          dataTestId={`${testId}_noData`}
        >
          <div className="mt-24">
            <AddAdjustment />
          </div>
        </NoDataGrid>
      </>
    );

  return (
    <>
      <div className="mt-16 d-flex justify-content-between align-items-center">
        <Header />
        <div className="mr-n8">
          <AddAdjustment />
        </div>
      </div>
      <div className="mt-16">
        <Grid
          ref={gridRefs}
          sortBy={[sortBy]}
          onSortChange={handleSortChange}
          columns={dataColumn as unknown as ColumnType[]}
          data={gridData}
          dataTestId={`${testId}_grid`}
        />
        {showPagination && (
          <div className="pt-16">
            <Pagination
              totalItem={total}
              pageSize={pageSize}
              pageNumber={currentPage}
              pageSizeValue={currentPageSize}
              compact
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
              dataTestId={`${testId}_pagination`}
            />
          </div>
        )}
      </div>
    </>
  );
};

export default AdjustmentTransactionGrid;
