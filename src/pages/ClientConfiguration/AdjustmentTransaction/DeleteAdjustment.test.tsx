import React from 'react';
import { screen } from '@testing-library/react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { adjustmentTransactionActions } from './_redux/reducers';

import { DeleteAdjustment } from './DeleteAdjustment';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

describe('DeleteAdjustment component', () => {
  const renderWrapper = (deleteState: {
    loading: boolean;
    openModal: boolean;
  }) =>
    renderMockStore(<DeleteAdjustment />, {
      initialState: {
        adjustmentTransactionConfig: {
          adjustmentConfigurationList: { data: [], loading: false },
          addAdjustmentConfiguration: {
            formIdList: [],
            loading: false,
            openModal: false
          },
          deleteAdjustmentConfiguration: deleteState,
          editAdjustmentConfiguration: {
            loading: false,
            openModal: false,
            validateError: ''
          }
        }
      }
    });
  it('not show modal', () => {
    const deleteState = { loading: false, openModal: false };
    const { wrapper } = renderWrapper(deleteState);
    expect(wrapper.container.firstChild).toBeNull();
  });

  it('handle Close', () => {
    const deleteState = { loading: false, openModal: true };
    const { wrapper } = renderWrapper(deleteState);
    const cancelBtn = screen.getByText(I18N_COMMON_TEXT.CANCEL);
    cancelBtn.click();
    expect(wrapper.container.firstChild).toBeNull();
  });

  it('handle Delete', () => {
    const mockAdjustmentActions = mockActionCreator(
      adjustmentTransactionActions
    );
    const spyTriggerDeleteAdjustmentConfiguration = mockAdjustmentActions(
      'triggerDeleteAdjustmentConfiguration'
    );
    const deleteState = { loading: false, openModal: true };
    renderWrapper(deleteState);
    const deleteBtn = screen.getByText(I18N_COMMON_TEXT.DELETE);
    deleteBtn.click();
    expect(spyTriggerDeleteAdjustmentConfiguration).toBeCalled();
  });
});
