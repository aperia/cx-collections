import React from 'react';
import { screen } from '@testing-library/react';
import {
  renderMockStore,
  mockActionCreator,
  queryAllByClass
} from 'app/test-utils';

import { AddAdjustment } from './AddAdjustment';
import {
  ADD_ADJUSTMENT_ERR_TYPE,
  I18N_ADJUSTMENT_TRANSACTION
} from './constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { adjustmentTransactionActions } from './_redux/reducers';
import * as mockStatus from '../hooks/useCheckClientConfigStatus';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponent = jest.requireActual('app/_libraries/_dls/components');
  // because view dof render after modal,
  // modal take time to render
  // then view not render yet
  return {
    ...dlsComponent,
    Modal: ({
      children,
      show
    }: {
      children: React.ReactNode;
      show: boolean;
    }) => {
      if (!show) return null;
      return <div>{children}</div>;
    }
  };
});

describe('AddAdjustment component', () => {
  const renderComponent = (addAdjustmentConfiguration: {
    formIdList: string[];
    loading: boolean;
    openModal: boolean;
    validateError?: {
      type: string;
      idList: string[];
    };
  }) =>
    renderMockStore(<AddAdjustment />, {
      initialState: {
        adjustmentTransactionConfig: {
          adjustmentConfigurationList: { data: [], loading: false },
          addAdjustmentConfiguration,
          deleteAdjustmentConfiguration: {
            openModal: false,
            loading: false
          },
          editAdjustmentConfiguration: {
            loading: false,
            openModal: false,
            validateError: ''
          }
        }
      }
    });

  describe('test render', () => {
    it('not open add modal', () => {
      renderComponent({ formIdList: ['1'], loading: false, openModal: false });
      expect(
        screen.getByText(I18N_ADJUSTMENT_TRANSACTION.ADD_ADJUSTMENT)
      ).toBeInTheDocument();
      expect(
        screen.queryByTestId('clientConfigAdjustmentOriginalTransactionCode')
      ).toBeNull();
    });

    it('render duplicate error', () => {
      renderComponent({
        formIdList: ['1'],
        loading: false,
        openModal: true,
        validateError: {
          type: ADD_ADJUSTMENT_ERR_TYPE.DUPLICATE_POSTING_TRAN_CODE,
          idList: ['1']
        }
      });
      expect(
        screen.getByText(
          I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_POSTING_TRAN_CODE_CANNOT_BE_THE_SAME
        )
      ).toBeInTheDocument();
    });

    it('render same as transaction code error', () => {
      renderComponent({
        formIdList: ['1'],
        loading: false,
        openModal: true,
        validateError: {
          type: ADD_ADJUSTMENT_ERR_TYPE.SAME_AS_ORIGINAL_CODE,
          idList: ['1']
        }
      });
      expect(
        screen.getByText(
          I18N_ADJUSTMENT_TRANSACTION.ADD_ADJUSTMENT_SAME_AS_TRANSACTION_CODE_INLINE_MSG
        )
      ).toBeInTheDocument();
    });

    it('render one combine already exist error', () => {
      renderComponent({
        formIdList: ['1'],
        loading: false,
        openModal: true,
        validateError: {
          type: ADD_ADJUSTMENT_ERR_TYPE.ONE_COMBINE_ALREADY_EXISTS,
          idList: ['1']
        }
      });
      expect(
        screen.getByText(I18N_ADJUSTMENT_TRANSACTION.ADD_ADJUSTMENT_INLINE_MSG)
      ).toBeInTheDocument();
    });

    it('render multiple combine already exist error', () => {
      renderComponent({
        formIdList: ['1'],
        loading: false,
        openModal: true,
        validateError: {
          type: ADD_ADJUSTMENT_ERR_TYPE.MULTIPLE_COMBINE_ALREADY_EXISTS,
          idList: ['1']
        }
      });
      expect(
        screen.getByText(
          I18N_ADJUSTMENT_TRANSACTION.ADD_ADJUSTMENT_MULTIPLE_ERROR_INLINE_MSG
        )
      ).toBeInTheDocument();
    });
  });

  describe('handleActions', () => {
    const mockAction = mockActionCreator(adjustmentTransactionActions);
    it('handle Open add modal', () => {
      jest
        .spyOn(mockStatus, 'useCheckClientConfigStatus')
        .mockReturnValue(false);
      const mockAddAdjustmentRequest = mockAction('addAdjustmentConfigRequest');
      renderComponent({ formIdList: ['1'], loading: false, openModal: false });

      const addBtn = screen.getByText(
        I18N_ADJUSTMENT_TRANSACTION.ADD_ADJUSTMENT
      );
      addBtn.click();
      expect(mockAddAdjustmentRequest).toBeCalled();
    });

    it('handle cancel modal', () => {
      const mockCancelAddAdjustment = mockAction('cancelAddAdjustmentConfig');
      renderComponent({ formIdList: ['1'], loading: false, openModal: true });
      const cancelBtn = screen.getByText(I18N_COMMON_TEXT.CANCEL);
      cancelBtn.click();
      expect(mockCancelAddAdjustment).toBeCalled();
    });

    it('handle add more form', () => {
      const mockAddMoreAction = mockAction('addAddAdjustmentConfigForm');
      renderComponent({ formIdList: ['1'], loading: false, openModal: true });
      const addMoreBtn = screen.getByText(
        I18N_ADJUSTMENT_TRANSACTION.ADD_ADJUSTMENT_TYPE
      );
      addMoreBtn.click();
      expect(mockAddMoreAction).toBeCalled();
    });

    it('handle remove form', () => {
      const mockRemoveFormAction = mockAction('removeAddAdjustmentConfigForm');
      const { wrapper } = renderComponent({
        formIdList: ['1', '2'],
        loading: false,
        openModal: true
      });
      const removeFormBtn = queryAllByClass(
        wrapper.baseElement as HTMLElement,
        /btn-icon-danger/
      );
      removeFormBtn[1].click();
      expect(mockRemoveFormAction).toBeCalledWith('2');
    });

    it('handle submit', () => {
      const mockTriggerAddAdjustmentAction = mockAction(
        'triggerAddAdjustmentConfiguration'
      );
      renderComponent({
        formIdList: ['1'],
        loading: false,
        openModal: true
      });
      const submitBtn = screen.getByText(I18N_COMMON_TEXT.SUBMIT);
      submitBtn.click();
      expect(mockTriggerAddAdjustmentAction).toBeCalled();
    });
  });
});
