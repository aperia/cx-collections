import findKey from 'lodash.findkey';
import { AdjustmentConfigurationItem } from './types';

export const compareAdjustmentCodes = (
  current: AdjustmentConfigurationItem,
  next: AdjustmentConfigurationItem
): boolean => {
  return (
    current.adjustmentPostingBatchCode?.toLowerCase() ===
      next.adjustmentPostingBatchCode?.toLowerCase() &&
    current.adjustmentPostingTranCode === next.adjustmentPostingTranCode &&
    current.originalTransactionCode === next.originalTransactionCode
  );
};

export const getViewErrorId = (
  configList: AdjustmentConfigurationItem[],
  recordValue: Record<string, AdjustmentConfigurationItem>
) => {
  return configList.reduce((prev: string[], curr) => {
    const ky = findKey(recordValue, function (o) {
      return compareAdjustmentCodes(curr, o);
    });
    if (ky === undefined) {
      return prev;
    }
    prev.push(ky);
    return prev;
  }, []);
};

export const getDuplicatePostingTranCodeAndBatchCode = (
  formValue?: Record<string, AdjustmentConfigurationItem>
) => {
  if (!formValue) return [];
  const setPostingTranCode = new Set();
  const duplicateValue = new Set();
  Object.values(formValue).forEach(item => {
    const parseString = `${item['adjustmentPostingTranCode']}-${item['adjustmentPostingBatchCode']}`;
    if (setPostingTranCode.has(parseString)) {
      duplicateValue.add(parseString);
    }
    setPostingTranCode.add(parseString);
  });

  const dupKeys = Object.keys(formValue).filter(objKy =>
    duplicateValue.has(
      `${formValue[objKy].adjustmentPostingTranCode}-${formValue[objKy].adjustmentPostingBatchCode}`
    )
  );
  return dupKeys;
};
