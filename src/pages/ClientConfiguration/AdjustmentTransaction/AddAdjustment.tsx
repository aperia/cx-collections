import React, { useCallback, useEffect, useRef, useState } from 'react';

// components
import { ExtraFieldProps, View } from 'app/_libraries/_dof/core';
import {
  Button,
  Icon,
  InlineMessage,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useInvalidForm } from 'app/hooks';

// constants
import {
  ADD_ADJUSTMENT_ERR_TYPE,
  CLIENT_CONFIG_SINGLE_FORM_VIEW,
  I18N_ADJUSTMENT_TRANSACTION,
  INLINE_MSG
} from './constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { adjustmentTransactionActions } from './_redux/reducers';
import {
  selectAddAdjustmentLoading,
  selectAddModalOpen,
  selectFormIdList,
  selectValidateErrorAddAdjustment
} from './_redux/selectors';
import { Field } from 'redux-form';
import { setErrorField } from 'app/helpers';
import ModalBodyWithErrorClientConfig from '../ApiErrorSection/ModalBodyWithErrorClientConfig';
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';
import { genAmtId } from 'app/_libraries/_dls/utils';

const DEFAULT_FORM_REF_LIST = { '1': React.createRef() };

export const AddAdjustment = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const testId = 'clientConfig_addAdjustment';

  const status = useCheckClientConfigStatus();

  const originalTranCodeViewRef = useRef<any>();
  const renderAdjustmentListView = useRef<any>(null);
  const renderOriginalTranCodeView = useRef<any>(null);

  const formIdList = useSelector(selectFormIdList);
  const validateError = useSelector(selectValidateErrorAddAdjustment);

  const addAdjustmentConfigurationLoading = useSelector(
    selectAddAdjustmentLoading
  );
  const modalOpened = useSelector(selectAddModalOpen);

  const disableSubmit = useInvalidForm([
    ...formIdList.map(id => `${CLIENT_CONFIG_SINGLE_FORM_VIEW}-${id}`),
    'clientConfigAdjustmentOriginalTransactionCode'
  ]);

  const [inlineErrMsg, setInlineErrMsg] = useState('');
  const [formRefList, setFormRefList] = useState<
    Record<string, React.RefObject<any>>
  >(DEFAULT_FORM_REF_LIST);

  const handleOpenModal = () => {
    dispatch(adjustmentTransactionActions.addAdjustmentConfigRequest());
  };

  const handleAddMore = () => {
    const newId = Date.now().toString();
    dispatch(adjustmentTransactionActions.addAddAdjustmentConfigForm(newId));
    setFormRefList(prev => ({ ...prev, [newId]: React.createRef() }));
  };

  const handleRemove = (id: string) => {
    dispatch(adjustmentTransactionActions.removeAddAdjustmentConfigForm(id));
    setFormRefList(prev => {
      const newState = { ...prev };
      delete newState[id];
      return newState;
    });
  };

  const handleCancelModal = () => {
    dispatch(adjustmentTransactionActions.cancelAddAdjustmentConfig());
  };

  const handleSubmit = () => {
    dispatch(adjustmentTransactionActions.triggerAddAdjustmentConfiguration());
  };

  const handleSetFieldError = useCallback(() => {
    if (!validateError) {
      setInlineErrMsg('');
      return;
    }
    const originalTransactionCodeFields: Field<ExtraFieldProps> =
      originalTranCodeViewRef.current?.props?.onFind('originalTransactionCode');

    const originalTranCodeAbleError =
      validateError.type !==
      ADD_ADJUSTMENT_ERR_TYPE.DUPLICATE_POSTING_TRAN_CODE;
    const postingTranCodeAbleError =
      validateError.type !==
      ADD_ADJUSTMENT_ERR_TYPE.DUPLICATE_POSTING_TRAN_CODE;
    const postingBatchCodeAbleError =
      validateError.type !== ADD_ADJUSTMENT_ERR_TYPE.SAME_AS_ORIGINAL_CODE;

    setErrorField(originalTransactionCodeFields, originalTranCodeAbleError);

    formIdList.forEach(id => {
      const currentAdjustmentPostingTranCodeField: Field<ExtraFieldProps> =
        formRefList[id].current.props.onFind('adjustmentPostingTranCode');
      setErrorField(
        currentAdjustmentPostingTranCodeField,
        postingTranCodeAbleError && validateError.idList.includes(id)
      );
      const currentAdjustmentPostingBatchCodeField: Field<ExtraFieldProps> =
        formRefList[id].current.props.onFind('adjustmentPostingBatchCode');
      setErrorField(
        currentAdjustmentPostingBatchCodeField,
        postingBatchCodeAbleError && validateError.idList.includes(id)
      );
    });

    setInlineErrMsg(t(INLINE_MSG[validateError.type]));
  }, [validateError, formIdList, formRefList, t]);

  useEffect(() => {
    handleSetFieldError();
  }, [handleSetFieldError]);

  const showRemove = formIdList.length > 1;

  renderOriginalTranCodeView.current = (
    <View
      id={`clientConfigAdjustmentOriginalTransactionCode`}
      formKey={`clientConfigAdjustmentOriginalTransactionCode`}
      descriptor="clientConfigAdjustmentOriginalTransactionCode"
      ref={originalTranCodeViewRef}
    />
  );

  renderAdjustmentListView.current = (
    <div className="mx-n24 mt-16 mb-n24 px-24 py-16 bg-light-l20 border-top">
      {formIdList.map((id, index) => {
        return (
          <div key={id}>
            {index > 0 && <div className="divider-dashed my-16" />}
            <div className="d-flex justify-content-between align-items-center">
              <h6 className="color-grey" data-testid={genAmtId(testId, 'adjustmentType', '')}>
                {formIdList.length > 1
                  ? `${t(I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_TYPE)} ${index + 1
                  }`
                  : `${t(I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_TYPE)}`}
              </h6>
              {showRemove && (
                <Button
                  variant="icon-danger"
                  size="sm"
                  onClick={() => handleRemove(id)}
                  dataTestId={`${testId}_closebtn`}
                >
                  <Icon name="close" />
                </Button>
              )}
            </div>

            {
              <View
                id={`${CLIENT_CONFIG_SINGLE_FORM_VIEW}-${id}`}
                formKey={`${CLIENT_CONFIG_SINGLE_FORM_VIEW}-${id}`}
                descriptor={CLIENT_CONFIG_SINGLE_FORM_VIEW}
                ref={formRefList[id]}
              />
            }
          </div>
        );
      })}
      <Button
        className="mt-16 ml-n8"
        variant="outline-primary"
        size="sm"
        onClick={handleAddMore}
        dataTestId={`${testId}_addAdjustmentTypeBtn`}
      >
        {t(I18N_ADJUSTMENT_TRANSACTION.ADD_ADJUSTMENT_TYPE)}
      </Button>
    </div>
  );

  return (
    <>
      <Button
        disabled={status}
        onClick={handleOpenModal}
        size="sm"
        variant="outline-primary"
        dataTestId={`${testId}_addAdjustment`}
      >
        {t(I18N_ADJUSTMENT_TRANSACTION.ADD_ADJUSTMENT)}
      </Button>
      <Modal
        md
        show={modalOpened}
        loading={addAdjustmentConfigurationLoading}
        dataTestId={testId}
      >
        <ModalHeader
          border
          closeButton
          onHide={handleCancelModal}
          dataTestId={`${testId}_header`}
        >
          <ModalTitle dataTestId={`${testId}_title`}>
            {t(I18N_ADJUSTMENT_TRANSACTION.ADD_ADJUSTMENT)}
          </ModalTitle>
        </ModalHeader>
        <ModalBodyWithErrorClientConfig
          apiErrorClassName="mb-24"
          dataTestId={`${testId}_body`}
        >
          <>
            {inlineErrMsg && (
              <InlineMessage
                className="mb-24"
                variant="danger"
                dataTestId={`${testId}_errorMessage`}
              >
                {inlineErrMsg}
              </InlineMessage>
            )}
            <p className="color-grey-d20" data-testid={genAmtId(testId, 'addAdjustmentGuideTitle', '')}>
              {t(I18N_ADJUSTMENT_TRANSACTION.ADD_ADJUSTMENT_GUIDE)}
            </p>
            {renderOriginalTranCodeView.current}
            {renderAdjustmentListView.current}
          </>
        </ModalBodyWithErrorClientConfig>
        <ModalFooter
          className="pt-32 pb-24"
          border
          cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
          onCancel={handleCancelModal}
          okButtonText={t(I18N_COMMON_TEXT.SUBMIT)}
          onOk={handleSubmit}
          disabledOk={disableSubmit}
          dataTestId={`${testId}_footer`}
        />
      </Modal>
    </>
  );
};
