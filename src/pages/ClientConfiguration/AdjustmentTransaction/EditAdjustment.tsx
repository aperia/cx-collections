import React, { useCallback, useEffect, useMemo, useRef } from 'react';
import { ExtraFieldProps, View } from 'app/_libraries/_dof/core';

// components
import {
  InlineMessage,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useInvalidForm, useIsDirtyForm } from 'app/hooks';

// redux
import { useDispatch, useSelector } from 'react-redux';
import {
  selectEditError,
  selectEditLoading,
  selectEditModalOpen,
  selectEditRecord,
  selectEditSingleFormRecord
} from './_redux/selectors';
import { adjustmentTransactionActions } from './_redux/reducers';

// constants & types
import {
  CLIENT_CONFIG_SINGLE_FORM_VIEW,
  I18N_ADJUSTMENT_TRANSACTION,
  ORIGINAL_TRAN_CODE_VIEW
} from './constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { Field } from 'redux-form';
import ModalBodyWithErrorClientConfig from '../ApiErrorSection/ModalBodyWithErrorClientConfig';
import { genAmtId } from 'app/_libraries/_dls/utils';

export const EditAdjustment = () => {
  const { t } = useTranslation();
  const originalTranCodeViewRef = useRef<any>(null);
  const clientConfigSingleFormViewRef = useRef<any>(null);
  const dispatch = useDispatch();
  const loading = useSelector(selectEditLoading);
  const isOpened = useSelector(selectEditModalOpen);
  const currentRecord = useSelector(selectEditRecord);
  const singleFormValues = useSelector(selectEditSingleFormRecord);
  const validateMsgError = useSelector(selectEditError);
  const disableSubmit = useInvalidForm([
    ORIGINAL_TRAN_CODE_VIEW,
    CLIENT_CONFIG_SINGLE_FORM_VIEW
  ]);
  const isDirtyFrom = useIsDirtyForm([
    CLIENT_CONFIG_SINGLE_FORM_VIEW,
    ORIGINAL_TRAN_CODE_VIEW
  ]);
  const testId = 'clientConfig_editAdjustment';

  const { originalTransactionCode } = currentRecord ?? {};

  const handleClose = () => {
    dispatch(adjustmentTransactionActions.cancelEditAdjustmentConfig());
  };

  const handleSubmit = () => {
    dispatch(adjustmentTransactionActions.triggerEditAdjustmentConfiguration());
  };

  const renderOriginalTranCodeView = useMemo(
    () => (
      <View
        id={ORIGINAL_TRAN_CODE_VIEW}
        formKey={ORIGINAL_TRAN_CODE_VIEW}
        descriptor={ORIGINAL_TRAN_CODE_VIEW}
        value={{ originalTransactionCode }}
        ref={originalTranCodeViewRef}
      />
    ),
    [originalTransactionCode]
  );

  const renderAdjustmentTransactionSingleView = useMemo(() => {
    return (
      <View
        id={CLIENT_CONFIG_SINGLE_FORM_VIEW}
        formKey={CLIENT_CONFIG_SINGLE_FORM_VIEW}
        descriptor={CLIENT_CONFIG_SINGLE_FORM_VIEW}
        value={singleFormValues}
        ref={clientConfigSingleFormViewRef}
      />
    );
  }, [singleFormValues]);

  const handleSetFieldError = useCallback(() => {
    if (!validateMsgError) return;
    const originalTransactionCodeFields: Field<ExtraFieldProps> =
      originalTranCodeViewRef.current?.props?.onFind('originalTransactionCode');
    const adjustmentPostingTranCodeFields: Field<ExtraFieldProps> =
      clientConfigSingleFormViewRef.current?.props?.onFind(
        'adjustmentPostingTranCode'
      );
    const adjustmentPostingBatchCodeFields: Field<ExtraFieldProps> =
      clientConfigSingleFormViewRef.current?.props?.onFind(
        'adjustmentPostingBatchCode'
      );
    originalTransactionCodeFields.props.props.setOthers(
      (prevOthers: MagicKeyValue) => ({
        ...prevOthers,
        options: { ...prevOthers.options, isError: true }
      })
    );
    adjustmentPostingTranCodeFields.props.props.setOthers(
      (prevOthers: MagicKeyValue) => ({
        ...prevOthers,
        options: {
          ...prevOthers.options,
          isError: true
        }
      })
    );
    adjustmentPostingBatchCodeFields.props.props.setOthers(
      (prevOthers: MagicKeyValue) => ({
        ...prevOthers,
        options: {
          ...prevOthers.options,
          isError:
            validateMsgError ===
            I18N_ADJUSTMENT_TRANSACTION.ADD_ADJUSTMENT_INLINE_MSG
        }
      })
    );
  }, [validateMsgError]);

  useEffect(() => {
    handleSetFieldError();
  }, [handleSetFieldError]);

  return (
    <Modal md show={isOpened} loading={loading} dataTestId={testId}>
      <ModalHeader border closeButton onHide={handleClose} dataTestId={`${testId}_header`}>
        <ModalTitle dataTestId={`${testId}_title`}>
          {t(I18N_ADJUSTMENT_TRANSACTION.EDIT_ADJUSTMENT)}
        </ModalTitle>
      </ModalHeader>
      <ModalBodyWithErrorClientConfig
        apiErrorClassName="mb-24"
        dataTestId={`${testId}_body`}
      >
        <>
          {!!validateMsgError && (
            <InlineMessage
              className="mb-24"
              variant="danger"
              dataTestId={`${testId}_errorMessage`}
            >
              {t(validateMsgError)}
            </InlineMessage>
          )}
          <p className="color-grey-d20" data-testid={genAmtId(testId, 'description', '')}>
            {t(I18N_ADJUSTMENT_TRANSACTION.ADD_ADJUSTMENT_GUIDE)}
          </p>
          {renderOriginalTranCodeView}
          <div className="mx-n24 mt-16 mb-n24 px-24 py-16 bg-light-l20 border-top">
            <h6 className="color-grey" data-testid={genAmtId(testId, 'adjustmentType', '')}>
              {t(I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_TYPE)}
            </h6>
            {renderAdjustmentTransactionSingleView}
          </div>
        </>
      </ModalBodyWithErrorClientConfig>
      <ModalFooter
        className="pt-32 pb-24"
        border
        cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
        onCancel={handleClose}
        okButtonText={t(I18N_COMMON_TEXT.SAVE)}
        onOk={handleSubmit}
        disabledOk={disableSubmit || !isDirtyFrom}
        dataTestId={`${testId}_footer`}
      />
    </Modal>
  );
};
