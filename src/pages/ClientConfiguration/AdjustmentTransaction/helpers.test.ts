import {
  getViewErrorId,
  getDuplicatePostingTranCodeAndBatchCode
} from './helpers';
import { AdjustmentConfigurationItem } from './types';

describe('getViewErrorId function', () => {
  const configList: AdjustmentConfigurationItem[] = [
    {
      originalTransactionCode: '123',
      adjustmentPostingTranCode: '234',
      adjustmentPostingBatchCode: 'VV',
      adjustmentDescription: 'Fake Description'
    },
    {
      originalTransactionCode: '124',
      adjustmentPostingTranCode: '234',
      adjustmentPostingBatchCode: 'VV',
      adjustmentDescription: 'Fake Description'
    }
  ];

  const recordValue = {
    '1': {
      originalTransactionCode: '123',
      adjustmentPostingTranCode: '234',
      adjustmentPostingBatchCode: 'VV',
      adjustmentDescription: 'mock Description'
    }
  };

  it('working correctly', () => {
    const result = getViewErrorId(configList, recordValue);
    expect(result).toEqual(['1']);
  });
});

describe('getDuplicatePostingTranCodeAndBatchCode', () => {
  const fakeFormValue = {
    '1': {
      originalTransactionCode: '123',
      adjustmentPostingTranCode: '234',
      adjustmentPostingBatchCode: 'VV',
      adjustmentDescription: 'mock Description'
    },
    '2': {
      originalTransactionCode: '123',
      adjustmentPostingTranCode: '234',
      adjustmentPostingBatchCode: 'VV',
      adjustmentDescription: 'mock Description'
    }
  };

  it('has not form value', () => {
    const result = getDuplicatePostingTranCodeAndBatchCode(undefined);
    expect(result).toEqual([]);
  });

  it('working correctly', () => {
    const result = getDuplicatePostingTranCodeAndBatchCode(fakeFormValue);
    expect(result).toEqual(['1', '2']);
  });
});
