import React from 'react';
import { screen } from '@testing-library/react';
import { renderMockStore } from 'app/test-utils';
import AdjustmentTransaction from '.';
import { I18N_ADJUSTMENT_TRANSACTION } from './constants';

jest.mock('./AdjustmentConfigurationGrid', () => () => (
  <div>AdjustmentTransactionGrid</div>
));

jest.mock('./DeleteAdjustment', () => ({
  DeleteAdjustment: () => <div>DeleteAdjustment</div>
}));

jest.mock('./EditAdjustment', () => ({
  EditAdjustment: () => <div>EditAdjustment</div>
}));

describe('Adjustment Transaction', () => {
  const renderComponent = (loading: boolean) => {
    return renderMockStore(<AdjustmentTransaction />, {
      initialState: {
        adjustmentTransactionConfig: {
          adjustmentConfigurationList: { data: [], loading },
          addAdjustmentConfiguration: {
            formIdList: [],
            loading: false,
            openModal: false
          },
          deleteAdjustmentConfiguration: { loading: false, openModal: false },
          editAdjustmentConfiguration: {
            loading: false,
            openModal: false,
            validateError: ''
          }
        }
      }
    });
  };
  it('loading', () => {
    renderComponent(true);
    expect(
      screen.getByText(I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_TRANSACTION)
    ).toBeInTheDocument();
    expect(screen.getByText('AdjustmentTransactionGrid')).toBeInTheDocument();
  });
});
