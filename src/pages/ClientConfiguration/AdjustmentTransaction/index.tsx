import React from 'react';
// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// components
import AdjustmentTransactionGrid from './AdjustmentConfigurationGrid';
import { DeleteAdjustment } from './DeleteAdjustment';
import { EditAdjustment } from './EditAdjustment';
import { SimpleBar } from 'app/_libraries/_dls/components';

// constants
import { I18N_ADJUSTMENT_TRANSACTION } from './constants';

// redux
import ApiErrorForHeaderSection from '../ApiErrorSection/ApiErrorForHeaderSection';
import { genAmtId } from 'app/_libraries/_dls/utils';

const AdjustmentTransaction = () => {
  const { t } = useTranslation();

  return (
    <div className={'position-relative h-100'}>
      <SimpleBar>
        <ApiErrorForHeaderSection
          className="mt-24 ml-24"
          dataTestId="clientConfig_apiError"
        />
        <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
          <h5 data-testid={genAmtId('clientConfig', 'title', '')}>
            {t(I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT_TRANSACTION)}
          </h5>
          <AdjustmentTransactionGrid />
        </div>
      </SimpleBar>
      <DeleteAdjustment />
      <EditAdjustment />
    </div>
  );
};

export default AdjustmentTransaction;
