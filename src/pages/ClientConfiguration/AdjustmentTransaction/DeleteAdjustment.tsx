import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

// components
import {
  Button,
  Icon,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import {
  selectDeleteLoading,
  selectDeleteModalOpen,
  selectDeleteRecord
} from './_redux/selectors';
import { adjustmentTransactionActions } from './_redux/reducers';

// constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { I18N_ADJUSTMENT_TRANSACTION } from './constants';
import { genAmtId } from 'app/_libraries/_dls/utils';

export const DeleteAdjustment: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const loading = useSelector(selectDeleteLoading);
  const isOpened = useSelector(selectDeleteModalOpen);
  const currentRecord = useSelector(selectDeleteRecord);
  const testId = 'clientConfig_deleteAdjustment';

  const handleClose = () => {
    dispatch(adjustmentTransactionActions.cancelDeleteAdjustmentConfig());
  };

  const handleDelete = () => {
    dispatch(
      adjustmentTransactionActions.triggerDeleteAdjustmentConfiguration()
    );
  };

  return (
    <Modal xs loading={loading} show={isOpened} dataTestId={testId}>
      <ModalHeader border dataTestId={`${testId}_header`}>
        <ModalTitle dataTestId={`${testId}_title`}>
          <div>{t(I18N_ADJUSTMENT_TRANSACTION.CONFIRM_DELETE_ADJUSTMENT)}</div>
        </ModalTitle>
        <Button onClick={handleClose} variant="icon-secondary" dataTestId={`${testId}_closeBtn`}>
          <Icon name="close" size="5x" />
        </Button>
      </ModalHeader>
      <ModalBody dataTestId={`${testId}_body`}>
        <p className="fs-14" data-testid={genAmtId(testId, 'confirmMessage', '')}>
          {t(I18N_ADJUSTMENT_TRANSACTION.CONFIRM_DELETE_ADJUSTMENT_MSG)}
        </p>
        <p className="fs-14 mt-16" data-testid={genAmtId(testId, 'info', '')}>
          {`${t(I18N_ADJUSTMENT_TRANSACTION.ADJUSTMENT)}:`}{' '}
          <b>{`${currentRecord?.originalTransactionCode} - ${currentRecord?.adjustmentPostingTranCode} - ${currentRecord?.adjustmentPostingBatchCode} - ${currentRecord?.adjustmentDescription}`}</b>
        </p>
      </ModalBody>
      <ModalFooter dataTestId={`${testId}_footer`}>
        <Button
          id="confirm-modal-cancel-btn"
          variant="secondary"
          onClick={handleClose}
          dataTestId={`${testId}_cancelBtn`}
        >
          {t(I18N_COMMON_TEXT.CANCEL)}
        </Button>
        <Button
          id="confirm-modal-confirm-btn"
          variant="danger"
          onClick={handleDelete}
          dataTestId={`${testId}_deleteBtn`}
        >
          {t(I18N_COMMON_TEXT.DELETE)}
        </Button>
      </ModalFooter>
    </Modal>
  );
};
