export interface AdjustmentConfigurationItem {
  originalTransactionCode: string;
  adjustmentPostingTranCode: string;
  adjustmentPostingBatchCode: string;
  adjustmentDescription: string;
  active?: boolean;
}

export interface AdjustmentTransactionState {
  adjustmentConfigurationList: {
    loading: boolean;
    data: AdjustmentConfigurationItem[];
    error?: string;
  };
  addAdjustmentConfiguration: {
    formIdList: string[];
    loading: boolean;
    error?: string;
    openModal: boolean;
    validateError?: {
      type: string;
      idList: string[];
    };
  };
  deleteAdjustmentConfiguration: {
    loading: boolean;
    error?: string;
    openModal: boolean;
    currentRecord?: AdjustmentConfigurationItem;
  };
  editAdjustmentConfiguration: {
    loading: boolean;
    error?: string;
    openModal: boolean;
    currentRecord?: AdjustmentConfigurationItem;
    validateError: string;
  };
}

export interface UpdateAdjustmentConfigurationArgs {
  data: AdjustmentConfigurationItem[];
  updateAction?: 'delete';
}
