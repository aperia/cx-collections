import React from 'react';
import { screen } from '@testing-library/react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';

import { EditAdjustment } from './EditAdjustment';
import { adjustmentTransactionActions } from './_redux/reducers';
import {
  CLIENT_CONFIG_SINGLE_FORM_VIEW,
  ORIGINAL_TRAN_CODE_VIEW
} from './constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponent = jest.requireActual('app/_libraries/_dls/components');
  // because view dof render after modal,
  // modal take time to render
  // then view not render yet
  return {
    ...dlsComponent,
    Modal: ({ children }: { children: React.ReactNode }) => (
      <div>{children}</div>
    )
  };
});

describe('EditAdjustment component', () => {
  const renderWrapper = (editAdjustmentConfiguration: {
    loading: boolean;
    openModal: boolean;
    validateError: string;
  }) =>
    renderMockStore(<EditAdjustment />, {
      initialState: {
        adjustmentTransactionConfig: {
          adjustmentConfigurationList: { data: [], loading: false },
          addAdjustmentConfiguration: {
            formIdList: [],
            loading: false,
            openModal: false
          },
          deleteAdjustmentConfiguration: {
            openModal: false,
            loading: false
          },
          editAdjustmentConfiguration
        },
        form: {
          [ORIGINAL_TRAN_CODE_VIEW]: {
            values: { originalTransactionCode: '124', }
          },
          [CLIENT_CONFIG_SINGLE_FORM_VIEW]: {
            values: {
              originalTransactionCode: '124',
              adjustmentPostingTranCode: '234',
              adjustmentPostingBatchCode: 'VV',
              adjustmentDescription: 'Fake Description',
              active: false
            }
          }
        }
      }
    });
  describe('render', () => {
    it('open modal', () => {
      renderWrapper({ loading: false, openModal: true, validateError: '' });
      expect(screen.getByTestId(ORIGINAL_TRAN_CODE_VIEW)).toBeInTheDocument();
      expect(
        screen.getByTestId(CLIENT_CONFIG_SINGLE_FORM_VIEW)
      ).toBeInTheDocument();
    });

    it('has validate Error', () => {
      const mockValidateError = 'Error Mock';
      renderWrapper({
        loading: false,
        openModal: true,
        validateError: mockValidateError
      });
      expect(screen.getByText(mockValidateError)).toBeInTheDocument();
    });
  });

  describe('handleActions', () => {
    const mockAdjustmentTransactionActions = mockActionCreator(
      adjustmentTransactionActions
    );
    it('handleClose ', () => {
      const spyCancelEditAdjustmentConfig = mockAdjustmentTransactionActions(
        'cancelEditAdjustmentConfig'
      );
      renderWrapper({ loading: false, openModal: true, validateError: '' });
      const cancelBtn = screen.getByText(I18N_COMMON_TEXT.CANCEL);
      cancelBtn.click();
      expect(spyCancelEditAdjustmentConfig).toBeCalled();
    });

    it('handleSubmit ', () => {
      const spyTriggerEditAdjustmentConfiguration =
        mockAdjustmentTransactionActions('triggerEditAdjustmentConfiguration');
      renderWrapper({ loading: false, openModal: true, validateError: '' });
      const submitBtn = screen.getByText(I18N_COMMON_TEXT.SAVE);
      submitBtn.click();
      expect(spyTriggerEditAdjustmentConfiguration).toBeCalled();
    });
  });
});
