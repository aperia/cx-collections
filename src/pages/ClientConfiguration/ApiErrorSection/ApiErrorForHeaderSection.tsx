import React from 'react';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { useApiErrorNotification } from 'pages/ApiErrorNotification/hooks/useApiErrorNotification';

interface ApiErrorForDeleteActionProps extends DLSId {
  className?: string;
}
const ApiErrorForHeaderSection: React.FC<ApiErrorForDeleteActionProps> = ({
  className,
  dataTestId
}) => {
  const ApiErrorDetail = useApiErrorNotification({
    storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION,
    forSection: 'inClientConfigurationFeatureHeader'
  });

  return (
    <ApiErrorDetail
      className={className}
      dataTestId={dataTestId}
    />
  );
};

export default ApiErrorForHeaderSection;
