import React, { useEffect } from 'react';

import ModalBodyWithApiError, {
  ModalBodyWithApiErrorProp
} from 'pages/ApiErrorNotification/ModalBodyWithApiError';

import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { useDispatch } from 'react-redux';

export interface ModalBodyWithErrorClientConfigProp
  extends Partial<ModalBodyWithApiErrorProp> {}

const ModalBodyWithErrorClientConfig: React.FC<ModalBodyWithErrorClientConfigProp> =
  ({ children, ...props }) => {
    const dispatch = useDispatch();
    useEffect(() => {
      return () => {
        dispatch(
          apiErrorNotificationAction.clearApiErrorData({
            forSection: 'inModalBody',
            storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION
          })
        );
      };
    }, [dispatch]);
    return (
      <ModalBodyWithApiError
        {...props}
        storeId={API_ERROR_STORE_ID.CLIENT_CONFIGURATION}
      >
        {children}
      </ModalBodyWithApiError>
    );
  };

export default ModalBodyWithErrorClientConfig;
