// services
import { clientConfigMemosServices } from './clientConfigMemosService';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

describe('getMemosConfig', () => {
  it('when url was not defined', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    clientConfigMemosServices.getMemosConfig({common: {}});

    expect(mockService).toBeCalledWith("", {common: {}});
  });

  it('when url was defined', () => {
    mockAppConfigApi.setup();

    const mockService = mockApiServices.post();

    clientConfigMemosServices.getMemosConfig({common: {}});

    expect(mockService).toBeCalledWith(
      apiUrl.clientConfigMemos.getMemosConfig,
      {common: {}}
    );
  });
});

describe('getMemoConfig', () => {
  it('when url was not defined', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    clientConfigMemosServices.getMemoConfig({common: {}});

    expect(mockService).toBeCalledWith("", {common: {}});
  });

  it('when url was defined', () => {
    mockAppConfigApi.setup();

    const mockService = mockApiServices.post();

    clientConfigMemosServices.getMemoConfig({common: {}});

    expect(mockService).toBeCalledWith(
      apiUrl.clientConfigMemos.getMemoConfig,
      {common: {}}
    );
  });
});

describe('submitMemos', () => {
  it('when url was not defined', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    clientConfigMemosServices.submitMemos({});

    expect(mockService).toBeCalledWith("", {});
  });

  it('when url was defined', () => {
    mockAppConfigApi.setup();

    const mockService = mockApiServices.post();

    clientConfigMemosServices.submitMemos({});

    expect(mockService).toBeCalledWith(
      apiUrl.clientConfigMemos.saveMemosConfig,
      {}
    );
  });
});