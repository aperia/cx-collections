import React, { useEffect } from 'react';

// components
import { Button, SimpleBar } from 'app/_libraries/_dls/components';
import ApiErrorForHeaderSection from '../ApiErrorSection/ApiErrorForHeaderSection';

// helpers
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { View } from 'app/_libraries/_dof/core';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { memosConfigActions } from './_redux/reducers';
import {
  selectDataForm,
  selectMemosConfigurationData
} from './_redux/selectors';
import { isInvalid } from 'redux-form';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import isEqual from 'lodash.isequal';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import {
  dispatchCloseModalEvent,
  dispatchControlTabChangeEvent
} from '../helpers';

// hooks
import { useListenChangingTabEvent } from '../hooks/useListenChangingTabEvent';
import { useListenClosingModal } from '../hooks/useListenClosingModal';
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';
import { handleWithRefOnFind } from 'app/helpers';
import { useIsDirtyForm, useSelectFormValue } from 'app/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

const CLIENT_CONFIG_MEMOS_FORM = 'clientConfigMemosForm';

const MemoConfig: React.FC = () => {
  const dispatch = useDispatch();

  const { t } = useTranslation();

  const status = useCheckClientConfigStatus();

  const testId = 'clientConfig_memos';

  const data = useSelector(selectMemosConfigurationData);

  const dataForm = useSelector(selectDataForm);

  const isFormInValid = useSelector<RootState, boolean>(
    isInvalid(CLIENT_CONFIG_MEMOS_FORM)
  );

  const clientConfigFormRef = React.useRef<any>(null);

  const formValue = useSelectFormValue(CLIENT_CONFIG_MEMOS_FORM);

  const isNotChangeData = useIsDirtyForm([CLIENT_CONFIG_MEMOS_FORM]);

  const handleResetToPrev = () => {
    if (!isEqual(data, dataForm)) {
      dispatch(memosConfigActions.getMemosConfiguration());
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'txt_memos_previous'
        })
      );
    }
  };

  const handleSaveChanges = () => {
    dispatch(
      memosConfigActions.triggerUpdateMemosConfig({
        postData: dataForm
      })
    );
  };

  useListenChangingTabEvent(() => {
    if (isEqual(data, dataForm)) {
      dispatchControlTabChangeEvent();
      return;
    }
    dispatch(
      confirmModalActions.open({
        title: 'txt_unsaved_changes',
        body: 'txt_discard_change_body',
        cancelText: 'txt_continue_editing',
        confirmText: 'txt_unsaved_changes',
        confirmCallback: dispatchControlTabChangeEvent,
        variant: 'danger'
      })
    );
  });

  useListenClosingModal(() => {
    if (isEqual(data, dataForm)) {
      dispatchCloseModalEvent();
      return;
    }
    dispatch(
      confirmModalActions.open({
        title: 'txt_unsaved_changes',
        body: 'txt_discard_change_body',
        cancelText: 'txt_continue_editing',
        confirmText: 'txt_unsaved_changes',
        confirmCallback: () => {
          dispatchCloseModalEvent();
        },
        variant: 'danger'
      })
    );
  });

  useEffect(() => {
    dispatch(memosConfigActions.getMemosConfiguration());
  }, [dispatch]);

  useEffect(() => {
    const setImmediateInstance = setImmediate(() => {
      if (status && clientConfigFormRef.current && formValue) {
        const memoConfigListKey = Object.keys(formValue);
        memoConfigListKey.forEach((item: string) => {
          const selectDOF = handleWithRefOnFind(clientConfigFormRef, item);
          selectDOF?.props?.setReadOnly(true);
          selectDOF?.props?.setRequired(false);
        });
      }
    });
    return () => clearImmediate(setImmediateInstance);
  }, [status, formValue]);

  return (
    <div className={'position-relative has-footer-button'}>
      <SimpleBar>
        <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
          <ApiErrorForHeaderSection
            className="mb-24"
            dataTestId={`${testId}_apiError`}
          />
          <h5 data-testid={genAmtId(testId, 'title', '')}>
            {t('txt_client_config_title')}
          </h5>
          <p
            className="mt-16 mb-16 fs-14 color-grey-d20"
            data-testid={genAmtId(testId, 'description', '')}
          >
            {t('txt_client_config_title_desc')}
          </p>
          <View
            id={CLIENT_CONFIG_MEMOS_FORM}
            formKey={CLIENT_CONFIG_MEMOS_FORM}
            descriptor={CLIENT_CONFIG_MEMOS_FORM}
            value={data}
            ref={clientConfigFormRef}
          />
        </div>
      </SimpleBar>
      <div className="group-button-footer d-flex justify-content-end">
        <Button
          disabled={status}
          onClick={handleResetToPrev}
          variant="secondary"
          dataTestId={`${testId}_resetToPreviousBtn`}
        >
          {t('txt_reset_to_previous')}
        </Button>
        <Button
          onClick={handleSaveChanges}
          variant="primary"
          disabled={status || isFormInValid || !isNotChangeData}
          dataTestId={`${testId}_saveChangesBtn`}
        >
          {t('txt_save_changes')}
        </Button>
      </div>
    </div>
  );
};

export default MemoConfig;
