import { MemoConfigItem } from './types';

export const CHRONICLE_DEFAULT_KEY = 'default';
export const CHRONICLE_ACCOUNT_DETAILS_KEY = 'accountDetails';
export const CHRONICLE_PAYMENT_KEY = 'payment';
export const CHRONICLE_CALL_AUTHENTICATION_KEY = 'callerAuthentication';
export const CHRONICLE_LETTERS_KEY = 'letters';
export const CHRONICLE_CARD_HOLDER_MAINTENANCE_KEY = 'cardHolderMaintenance';
export const CHRONICLE_TRANSACTION_ADJUSTMENT_KEY = 'transactionAdjustment';
export const CHRONICLE_REVIEW_ADJUSTMENT_KEY = 'reviewAdjustment';

export const DEFAULT_MEMO_CONFIG: MemoConfigItem = {
  default: 'CXCOLL',
  accountDetails: '',
  payment: '',
  callerAuthentication: '',
  letters: '',
  cardHolderMaintenance: '',
  transactionAdjustment: '',
  reviewAdjustment: ''
};
