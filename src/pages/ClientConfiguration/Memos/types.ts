export interface MemosConfigurationState {
  memosConfiguration: {
    loading: boolean;
    data: MemoConfigItem;
    error: boolean;
  };
}

export interface MemoConfigItem {
  default?: string;
  accountDetails?: string;
  payment?: string;
  callerAuthentication?: string;
  letters?: string;
  cardHolderMaintenance?: string;
  transactionAdjustment?: string;
  reviewAdjustment?: string;
}
