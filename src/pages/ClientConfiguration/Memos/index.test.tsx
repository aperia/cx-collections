import { mockActionCreator, renderMockStore } from 'app/test-utils';
import MemoConfig from './index';
import React from 'react';
import { memosConfigActions } from './_redux/reducers';
import userEvent from '@testing-library/user-event';
import * as clientConfigHelper from 'pages/ClientConfiguration/helpers';
import { CLIENT_CONFIG_STATUS_KEY, SECTION_TAB_EVENT } from '../constants';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import * as mockStatus from '../hooks/useCheckClientConfigStatus';
import * as mockHook from 'app/hooks/useDirtyForm';
import { DEFAULT_MEMO_CONFIG } from './constants';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const spyMemosConfigActions = mockActionCreator(memosConfigActions);
const spyConfirmModalActions = mockActionCreator(confirmModalActions);

describe('Render', () => {
  it('Should render', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStore(<MemoConfig />, {
      initialState: {
        form: {
          ['clientConfigMemosForm']: {
            values: { value: '123456' }
          } as any
        }
      }
    });
    jest.runAllTimers();
    expect(wrapper.getByText('txt_client_config_title')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_client_config_title_desc')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_reset_to_previous')).toBeInTheDocument();
    expect(wrapper.getByText('txt_save_changes')).toBeInTheDocument();
  });

  it('Should render', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStore(<MemoConfig />, {});
    jest.runAllTimers();
    expect(wrapper.getByText('txt_client_config_title')).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_client_config_title_desc')
    ).toBeInTheDocument();
    expect(wrapper.getByText('txt_reset_to_previous')).toBeInTheDocument();
    expect(wrapper.getByText('txt_save_changes')).toBeInTheDocument();
  });
});

describe('useListenChangingTabEvent', () => {
  it('data change', () => {
    const state: Partial<RootState> = {
      form: {
        ['clientConfigMemosForm']: {
          values: { value: '123456' }
        } as any
      },
      memosConfiguration: {
        memosConfiguration: {
          loading: false,
          data: DEFAULT_MEMO_CONFIG,
          error: false
        }
      }
    };
    const spyOpen = spyConfirmModalActions('open');
    renderMockStore(<MemoConfig />, { initialState: state });
    const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGING_TAB, {
      bubbles: true,
      cancelable: true
    });

    window.dispatchEvent(event);
    expect(spyOpen).toBeCalled();
  });

  it('data not change', () => {
    const mockDispatchEventChangeTab = jest.fn();
    const spy = jest
      .spyOn(clientConfigHelper, 'dispatchControlTabChangeEvent')
      .mockImplementation(mockDispatchEventChangeTab);
    const state: Partial<RootState> = {
      form: {
        ['clientConfigMemosForm']: {
          values: DEFAULT_MEMO_CONFIG
        } as any
      },
      memosConfiguration: {
        memosConfiguration: {
          loading: false,
          data: DEFAULT_MEMO_CONFIG,
          error: false
        }
      }
    };
    renderMockStore(<MemoConfig />, { initialState: state });
    const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGING_TAB, {
      bubbles: true,
      cancelable: true
    });

    window.dispatchEvent(event);
    expect(mockDispatchEventChangeTab).toBeCalled();
    spy.mockReset();
    spy.mockRestore();
  });
});

describe('useListenClosingModal', () => {
  it('data not change', () => {
    const state: Partial<RootState> = {
      form: {
        ['clientConfigMemosForm']: {
          values: DEFAULT_MEMO_CONFIG
        } as any
      },
      memosConfiguration: {
        memosConfiguration: {
          loading: false,
          data: DEFAULT_MEMO_CONFIG,
          error: false
        }
      }
    };
    const mockDispatchEventCloseModal = jest.fn();
    const spy = jest
      .spyOn(clientConfigHelper, 'dispatchCloseModalEvent')
      .mockImplementation(mockDispatchEventCloseModal);
    renderMockStore(<MemoConfig />, { initialState: state });
    const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
      bubbles: true,
      cancelable: true
    });

    window.dispatchEvent(event);
    expect(mockDispatchEventCloseModal).toBeCalled();
    spy.mockReset();
    spy.mockRestore();
  });

  it('data has been change', () => {
    const state: Partial<RootState> = {
      form: {
        ['clientConfigMemosForm']: {
          values: { value: '123456' }
        } as any
      },
      memosConfiguration: {
        memosConfiguration: {
          loading: false,
          data: DEFAULT_MEMO_CONFIG,
          error: false
        }
      }
    };
    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });
    const spy = jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);
    renderMockStore(<MemoConfig />, { initialState: state });
    const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
      bubbles: true,
      cancelable: true
    });

    window.dispatchEvent(event);
    expect(openConfirmModalAction).toBeCalled();
    spy.mockRestore();
    spy.mockReset();
  });
});

describe('Action', () => {
  it('handleResetToPrev', () => {
    const state: Partial<RootState> = {
      form: {
        ['clientConfigMemosForm']: {
          values: { value: '123456' }
        } as any
      },
      memosConfiguration: {
        memosConfiguration: {
          loading: false,
          data: DEFAULT_MEMO_CONFIG,
          error: false
        }
      },
      clientConfiguration: {
        settings: {
          selectedConfig: {
            additionalFields: [
              {
                name: CLIENT_CONFIG_STATUS_KEY,
                active: true
              }
            ]
          }
        }
      }
    };
    const spyGetMemosConfiguration = spyMemosConfigActions(
      'getMemosConfiguration'
    );
    const { wrapper } = renderMockStore(<MemoConfig />, {
      initialState: state
    });

    userEvent.click(wrapper.getByText('txt_reset_to_previous')!);

    expect(spyGetMemosConfiguration).toBeCalled();
  });

  it('handleResetToPrev', () => {
    const state: Partial<RootState> = {
      form: {
        ['clientConfigMemosForm']: {
          values: DEFAULT_MEMO_CONFIG
        } as any
      },
      memosConfiguration: {
        memosConfiguration: {
          loading: false,
          data: DEFAULT_MEMO_CONFIG,
          error: false
        }
      },
      clientConfiguration: {
        settings: {
          selectedConfig: {
            additionalFields: [
              {
                name: CLIENT_CONFIG_STATUS_KEY,
                active: true
              }
            ]
          }
        }
      }
    };
    const spyGetMemosConfiguration = spyMemosConfigActions(
      'getMemosConfiguration'
    );
    const { wrapper } = renderMockStore(<MemoConfig />, {
      initialState: state
    });

    userEvent.click(wrapper.getByText('txt_reset_to_previous')!);

    expect(spyGetMemosConfiguration).toBeCalled();
  });

  it('handleSaveChanges', () => {
    jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(false);
    jest.spyOn(mockHook, 'useIsDirtyForm').mockImplementation(() => true);
    const spyTriggerUpdateMemosConfig = spyMemosConfigActions(
      'triggerUpdateMemosConfig'
    );
    const { wrapper } = renderMockStore(<MemoConfig />, {
      initialState: {
        clientConfiguration: {
          settings: {
            selectedConfig: {
              additionalFields: [
                {
                  name: CLIENT_CONFIG_STATUS_KEY,
                  active: true
                }
              ]
            }
          }
        }
      }
    });

    userEvent.click(wrapper.getByText('txt_save_changes')!);

    expect(spyTriggerUpdateMemosConfig).toBeCalledWith({ postData: undefined });
  });
});
