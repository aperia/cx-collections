import { memosConfigActions as actions, reducer } from './reducers';
import { MemosConfigurationState } from '../types';

const initialState: MemosConfigurationState = {
  memosConfiguration: {
    loading: false,
    data: [],
    error: false
  }
};

describe('ClientConfiguration Letter reducer', () => {

  it('remove store', () => {
    const state = reducer(initialState, actions.removeStore());
    expect(state.memosConfiguration.loading).toEqual(false);
  });

  it('update data', () => {
    const state = reducer(initialState, actions.updateData({postData: {}}));
    expect(state.memosConfiguration.data).toEqual({});
  });
});
