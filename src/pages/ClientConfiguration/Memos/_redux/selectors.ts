import { createSelector } from '@reduxjs/toolkit';
import { mappingMemoConfig } from 'pages/ClientConfiguration/helpers';
import { MemoConfigItem } from '../types';

const getMemosConfigurationState = (state: RootState) =>
  state.memosConfiguration.memosConfiguration;

const getMemosConfigurationDataState = (state: RootState) =>
  state.memosConfiguration.memosConfiguration.data;

const getFormData = (state: RootState) => {
  return state.form?.clientConfigMemosForm?.values;
};

export const selectMemosConfigurationLoading = createSelector(
  getMemosConfigurationState,
  data => data.loading
);

export const selectMemosConfigurationData = createSelector(
  getMemosConfigurationDataState,
  (data: MemoConfigItem) => {
    return mappingMemoConfig(data);
  }
);

export const selectMemosConfigurationError = createSelector(
  getMemosConfigurationState,
  data => data.error
);

export const selectDataForm = createSelector(getFormData, data => data);
