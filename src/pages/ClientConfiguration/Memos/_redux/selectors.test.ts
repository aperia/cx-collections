import { selectorWrapper } from 'app/test-utils';
import * as selectors from './selectors';

const store: Partial<RootState> = {
  memosConfiguration: {
    memosConfiguration: {
      loading: false,
      data: {},
      error: false
    }
  }
};

describe('should test memos config selectors', () => {
  it('selectLetterConfigurationLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectMemosConfigurationLoading
    );

    expect(data).toEqual(store.memosConfiguration?.memosConfiguration.loading);
    expect(emptyData).toEqual(false);
  });

  it('selectMemosConfigurationData', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectMemosConfigurationData
    );

    expect(data).toEqual({
      accountDetails: '',
      callerAuthentication: '',
      cardHolderMaintenance: '',
      default: 'CXCOLL',
      letters: '',
      payment: '',
      reviewAdjustment: '',
      transactionAdjustment: ''
    });

    expect(emptyData).toEqual({
      accountDetails: '',
      callerAuthentication: '',
      cardHolderMaintenance: '',
      default: 'CXCOLL',
      letters: '',
      payment: '',
      reviewAdjustment: '',
      transactionAdjustment: ''
    });
  });

  it('selectMemosConfigurationError', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectMemosConfigurationError
    );

    expect(data).toEqual(store.memosConfiguration?.memosConfiguration.error);
    expect(emptyData).toEqual(false);
  });

  it('selectDataForm', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectDataForm
    );

    expect(data).toEqual(undefined);
    expect(emptyData).toEqual(undefined);
  });
});
