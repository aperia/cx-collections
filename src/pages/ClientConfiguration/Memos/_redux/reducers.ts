import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { MemosConfigurationState } from '../types';

import {
  getMemosConfiguration,
  getMemosConfigurationBuilder
} from './getMemosConfiguration';

import {
  triggerUpdateMemosConfig,
  triggerUpdateMemosConfigBuilder
} from './updateMemosConfiguration';

const initialState: MemosConfigurationState = {
  memosConfiguration: { loading: false, data: {}, error: false }
};

const { actions, reducer } = createSlice({
  name: 'memosConfiguration',
  initialState,
  reducers: {
    removeStore: () => initialState,
    updateData: (draftState, action: PayloadAction<{postData: MagicKeyValue}>) => {
      draftState.memosConfiguration.data = action.payload?.postData
    }
  },
  extraReducers: builder => {
    getMemosConfigurationBuilder(builder);
    triggerUpdateMemosConfigBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getMemosConfiguration,
  triggerUpdateMemosConfig
};

export { allActions as memosConfigActions, reducer };
