import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';
import { getCollectionsClientConfig } from 'pages/ClientConfiguration/_redux/getCollectionsClientConfig';
import { DEFAULT_MEMO_CONFIG } from '../constants';
import { MemosConfigurationState, MemoConfigItem } from '../types';

export const getMemosConfiguration = createAsyncThunk<
  MemoConfigItem,
  undefined,
  ThunkAPIConfig
>(
  'clientConfiguration/getMemosConfiguration',
  async (_, { rejectWithValue, dispatch }) => {
    try {
      const response = await dispatch(
        getCollectionsClientConfig({
          component: 'component_memos'
        })
      );
      if (isFulfilled(response)) {
        const data = response.payload;
        return (data as MemoConfigItem) || DEFAULT_MEMO_CONFIG;
      }
      throw response;
    } catch (error) {
      return rejectWithValue({ response: error });
    }
  }
);

export const getMemosConfigurationBuilder = (
  builder: ActionReducerMapBuilder<MemosConfigurationState>
) => {
  builder
    .addCase(getMemosConfiguration.pending, draftState => {
      draftState.memosConfiguration = {
        ...draftState.memosConfiguration,
        loading: true,
        data: {}
      };
    })
    .addCase(getMemosConfiguration.fulfilled, (draftState, action) => {
      draftState.memosConfiguration = {
        ...draftState.memosConfiguration,
        loading: false,
        data: action.payload,
        error: false
      };
    })
    .addCase(getMemosConfiguration.rejected, (draftState, action) => {
      draftState.memosConfiguration = {
        ...draftState.memosConfiguration,
        data: DEFAULT_MEMO_CONFIG,
        loading: false,
        error: true
      };
    });
};
