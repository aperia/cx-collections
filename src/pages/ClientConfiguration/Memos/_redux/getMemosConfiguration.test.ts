import { applyMiddleware, createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { getMemosConfiguration } from './getMemosConfiguration';
import { byPassFulfilled, byPassRejected } from 'app/test-utils';
import { DEFAULT_MEMO_CONFIG } from '../constants';
import thunk from 'redux-thunk';
import { reducer } from './reducers';

const initialState: Partial<RootState> = {
  clientConfiguration: {
    settings: {
      selectedConfig: {
        clientId: '9999',
        systemId: '1000',
        principleId: '2000',
        agentId: '3000'
      }
    }
  },
  memosConfiguration: {
    memosConfiguration: {
      loading: false,
      data: {},
      error: false
    }
  }
};

window.appConfig = {
  commonConfig: {
    user: 'user',
    app: 'app',
    org: 'org'
  } as CommonConfig,
  api: {
    clientConfig: {
      getCollectionsClientConfig: ''
    }
  }
} as AppConfiguration;

describe('Test getMemosConfiguration', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, initialState, applyMiddleware(thunk));
  });

  it('fulfilled', () => {
    const nextState = reducer(
      initialState.memosConfiguration,
      getMemosConfiguration.fulfilled(
        DEFAULT_MEMO_CONFIG,
        'getPaybyPhoneConfiguration',
        undefined
      )
    );

    expect(nextState.memosConfiguration.loading).toEqual(false);
    expect(nextState.memosConfiguration.data).toEqual(DEFAULT_MEMO_CONFIG);
  });

  it('should be fulfilled', async () => {
    const response = await getMemosConfiguration()(
      byPassFulfilled(getMemosConfiguration.fulfilled.type),
      store.getState,
      {}
    );
    expect(response.type).toEqual(getMemosConfiguration.fulfilled.type);
  });

  it('should be rejected', async () => {
    const response = await getMemosConfiguration()(
      byPassRejected(getMemosConfiguration.rejected.type),
      store.getState,
      {}
    );
    expect(response.type).toEqual(getMemosConfiguration.rejected.type);
  });
});
