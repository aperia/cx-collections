import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { MemosConfigurationState } from '../types';
import { memosConfigActions } from './reducers';
import { saveCollectionsClientConfig } from 'pages/ClientConfiguration/_redux/saveCollectionsClientConfig';
import { saveChangedClientConfig } from 'pages/ClientConfiguration/ChangeHistory/_redux/saveChangedClientConfig';
import { batch } from 'react-redux';

export const triggerUpdateMemosConfig = createAsyncThunk<
  unknown,
  MagicKeyValue,
  ThunkAPIConfig
>(
  'clientConfigAuthentication/triggerSubmitAuthenticationConfig',
  async (args, { dispatch, getState }) => {
    const { postData } = args;
    const { memosConfiguration } = getState();
    const response = await dispatch(
      saveCollectionsClientConfig({
        component: 'component_memos',
        jsonValue: postData,
        updateAction: 'delete'
      })
    );
    if (isFulfilled(response)) {
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: 'txt_memos_settings_updated'
          })
        );
        dispatch(memosConfigActions.updateData({ postData }));
        dispatch(
          saveChangedClientConfig({
            action: 'UPDATE',
            changedCategory: 'memos',
            oldValue: memosConfiguration.memosConfiguration.data,
            newValue: postData
          })
        );
      });
    }

    if (isRejected(response)) {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: 'txt_memos_settings_failed_to_update'
        })
      );
    }
  }
);

export const triggerUpdateMemosConfigBuilder = (
  builder: ActionReducerMapBuilder<MemosConfigurationState>
) => {
  builder
    .addCase(triggerUpdateMemosConfig.pending, (draftState, action) => {
      draftState.memosConfiguration = {
        ...draftState.memosConfiguration,
        loading: true
      };
    })
    .addCase(triggerUpdateMemosConfig.fulfilled, (draftState, action) => {
      draftState.memosConfiguration = {
        ...draftState.memosConfiguration,
        loading: false
      };
    })
    .addCase(triggerUpdateMemosConfig.rejected, (draftState, action) => {
      draftState.memosConfiguration = {
        ...draftState.memosConfiguration,
        loading: false
      };
    });
};
