import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault
} from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { clientConfigMemosServices } from '../clientConfigMemosService';
import { triggerUpdateMemosConfig } from './updateMemosConfiguration';

const initialState: Partial<RootState> = {
  clientConfiguration: {
    settings: {
      selectedConfig: {
        clientId: '9999',
        systemId: '1000',
        principleId: '2000',
        agentId: '3000'
      }
    }
  }
};

let store: Store<RootState>;

const toastSpy = mockActionCreator(actionsToast);

beforeEach(() => {
  store = createStore(rootReducer, initialState);
});

describe('Test updateMemosConfigRequest', () => {
  it('Should return data', async () => {
    jest
      .spyOn(clientConfigMemosServices, 'submitMemos')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const response = await triggerUpdateMemosConfig({
      postData: {}
    })(store.dispatch, store.getState, {});

    expect(response.payload).toBeUndefined();
  });

  it('Should error', async () => {
    jest
      .spyOn(clientConfigMemosServices, 'submitMemos')
      .mockRejectedValue({ ...responseDefault, data: {} });

    const response = await triggerUpdateMemosConfig({
      postData: {}
    })(store.dispatch, store.getState, {});

    expect(response.payload).toBeUndefined();
  });
});

describe('Test triggerUpdateMemosConfig', () => {
  it('Should have fulfilled', async () => {
    const actual = rootReducer(store.getState(), {
      type: triggerUpdateMemosConfig.fulfilled.type,
      payload: {
        data: undefined,
        callback: jest.fn()
      }
    }).memosConfiguration;

    expect(actual.memosConfiguration.loading).toEqual(false);
  });

  it('Should have pending', async () => {
    const actual = rootReducer(store.getState(), {
      type: triggerUpdateMemosConfig.pending.type
    }).memosConfiguration;

    expect(actual.memosConfiguration.loading).toEqual(true);
  });

  it('Should have reject', async () => {
    const actual = rootReducer(store.getState(), {
      type: triggerUpdateMemosConfig.rejected.type,
      error: { message: 'test error' }
    }).memosConfiguration;

    expect(actual.memosConfiguration.loading).toEqual(false);
  });
});

describe('Test triggerUpdateMemosConfig', () => {
  it('isFulfilled', async () => {
    jest
      .spyOn(clientConfigMemosServices, 'submitMemos')
      .mockResolvedValue({ ...responseDefault, data: {} });
    const mockAddToast = toastSpy('addToast');

    await triggerUpdateMemosConfig({
      postData: {}
    })(
      byPassFulfilled(triggerUpdateMemosConfig.fulfilled.type),
      store.getState,
      {}
    );

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: 'txt_memos_settings_updated'
    });
  });

  it('isRejected', async () => {
    jest
      .spyOn(clientConfigMemosServices, 'submitMemos')
      .mockResolvedValue({ ...responseDefault, data: {} });
    const mockAddToast = toastSpy('addToast');

    await triggerUpdateMemosConfig({
      postData: {}
    })(
      byPassRejected(triggerUpdateMemosConfig.rejected.type),
      store.getState,
      {}
    );

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: 'txt_memos_settings_failed_to_update'
    });
  });
});
