import { apiService } from 'app/utils/api.service';

export const clientConfigMemosServices = {
  getMemoConfig(requestBody: MagicKeyValue) {
    const url = window.appConfig?.api?.clientConfigMemos?.getMemoConfig || '';
    return apiService.post(url, requestBody);
  },
  getMemosConfig(requestBody: { common: Record<string, string> }) {
    const url = window.appConfig?.api?.clientConfigMemos?.getMemosConfig || '';
    return apiService.post(url, requestBody);
  },
  submitMemos(requestBody: {
    common: Record<string, string>;
    clientConfig: {
      cspas: [
        {
          cspa: string;
          components: [
            {
              component: string;
              jsonValue: {
                sourceConfig: string[];
                questionConfig: string[];
              };
            }
          ];
        }
      ];
    };
  }) {
    const url = window.appConfig?.api?.clientConfigMemos?.saveMemosConfig || '';
    return apiService.post(url, requestBody);
  }
};
