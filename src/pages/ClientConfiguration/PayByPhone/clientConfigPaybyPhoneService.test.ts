import { responseDefault } from 'app/test-utils';

import { apiService } from 'app/utils/api.service';
import { clientConfigPaybyPhoneService } from './clientConfigPaybyPhoneService';

describe('PayByPhone > clientConfigPayByPhoneService', () => {
  it('should return data when call getPayByPhoneConfig', async () => {
    jest.spyOn(apiService, 'post').mockResolvedValue({
      ...responseDefault,
      data: { value: 10 }
    });

    const response = await clientConfigPaybyPhoneService.getPayByPhoneConfig();
    expect(response.data).toEqual({ value: 10 });
  });
  it('should return data when call getPayByPhoneData', async () => {
    jest.spyOn(apiService, 'post').mockResolvedValue({
      ...responseDefault,
      data: { value: 10 }
    });

    const response = await clientConfigPaybyPhoneService.getPayByPhoneConfig();
    expect(response.data).toEqual({ value: 10 });
  });
  it('should return data when call submitPaybyPhone', async () => {
    jest.spyOn(apiService, 'post').mockResolvedValue({
      ...responseDefault,
      data: { value: 10 }
    });

    const response = await clientConfigPaybyPhoneService.submitPaybyPhone({
      value: 10
    });
    expect(response.data).toEqual({ value: 10 });
  });
});
