import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { selectClientInfoGetByIdRequest } from 'pages/__commons/AccountDetailTabs/_redux';
import { clientConfigPaybyPhoneService } from '../clientConfigPaybyPhoneService';
import {
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE,
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE
} from '../constants';
import {
  GetPayByPhonePayload,
  GetPayByPhoneArgs,
  ClientConfigPayByPhoneState
} from '../types';
import { GetCollectionsClientConfigResponse } from '../../types';

export const getPayByPhoneData = createAsyncThunk<
  GetPayByPhonePayload,
  GetPayByPhoneArgs,
  ThunkAPIConfig
>('account/getPayByPhoneData', async (args, thunkAPI) => {
  const { getState, rejectWithValue } = thunkAPI;
  try {
    const commonClientInfoRequest = selectClientInfoGetByIdRequest(
      getState(),
      args.storeId!
    );

    const { data } = await clientConfigPaybyPhoneService.getPayByPhoneData({
      common: { ...commonClientInfoRequest.common },
      selectConfig: 'codeToText',
      selectComponent: 'component_pay_by_phone_config'
    });

    const {
      maxDay,
      paymentAgreementMessageWithConvenienceFee = DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE,
      paymentAgreementMessageWithoutConvenienceFee = DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE
    } = (
      (
        data.codeToText.components as CollectionsClientConfigItem['components']
      ).find(item => item.component === 'component_pay_by_phone_config')
        ?.jsonValue as GetCollectionsClientConfigResponse
    ).payByPhoneConfig;

    return {
      maxDay: maxDay ? +maxDay : undefined,
      paymentAgreementMessageWithConvenienceFee,
      paymentAgreementMessageWithoutConvenienceFee
    };
  } catch (error) {
    return rejectWithValue({ errorMessage: error.message });
  }
});
export const getPayByPhoneDataBuilder = (
  builder: ActionReducerMapBuilder<ClientConfigPayByPhoneState>
) => {
  const { pending, fulfilled, rejected } = getPayByPhoneData;
  builder
    .addCase(pending, draftState => {
      draftState.loading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      draftState.currentValue = {
        ...action.payload
      };
      draftState.loading = false;
    })
    .addCase(rejected, draftState => {
      draftState.loading = false;
    });
};
