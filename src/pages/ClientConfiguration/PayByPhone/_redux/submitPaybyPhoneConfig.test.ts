import { createStore, Store } from '@reduxjs/toolkit';
import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator
} from 'app/test-utils';

import { rootReducer } from 'storeConfig';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

import { triggerSubmitPayByPhoneConfig } from './submitPaybyPhoneConfig';
import { I18N_CLIENT_CONFIG_PAY_BY_PHONE } from '../constants';
import { reducer } from './reducers';
import { PayByPhoneItem } from '../types';

const spyToast = mockActionCreator(actionsToast);

const mockSubmitArgs: PayByPhoneItem = {
  maxDay: 10,
  paymentAgreementMessageWithConvenienceFee: 'test1',
  paymentAgreementMessageWithoutConvenienceFee: 'test2'
};

const mockUndefinedArgs: PayByPhoneItem = {
  maxDay: undefined,
  paymentAgreementMessageWithConvenienceFee: 'test1',
  paymentAgreementMessageWithoutConvenienceFee: 'test2'
};

const mockSubmitPayload: PayByPhoneItem = {
  maxDay: 10,
  paymentAgreementMessageWithConvenienceFee: 'test1',
  paymentAgreementMessageWithoutConvenienceFee: 'test2'
};

describe('PayByPhone > redux submitPaybyPhoneConfig', () => {
  const initialState: Partial<RootState> = {
    clientConfiguration: {
      settings: {
        selectedConfig: {
          id: '9999100020003000',
          agentId: '3000',
          clientId: '9999',
          systemId: '1000',
          principleId: '2000'
        }
      }
    },
    paybyPhoneConfig: {
      loading: false
    }
  };

  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, initialState);
  });

  it('fulfilled', () => {
    const nextState = reducer(
      initialState.paybyPhoneConfig,
      triggerSubmitPayByPhoneConfig.fulfilled(
        mockSubmitPayload,
        'triggerSubmitPayByPhoneConfig',
        mockSubmitArgs
      )
    );

    expect(nextState.loading).toEqual(false);
    expect(nextState.currentValue?.maxDay).toEqual(10);
    expect(nextState.previousValue?.maxDay).toEqual(10);
  });

  it('should return data', async () => {
    const response = await triggerSubmitPayByPhoneConfig(mockSubmitArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('should show success message when triggerSubmitPayByPhoneConfig fulfilled', async () => {
    const mockAddToast = spyToast('addToast');

    await triggerSubmitPayByPhoneConfig(mockSubmitArgs)(
      byPassFulfilled(triggerSubmitPayByPhoneConfig.fulfilled.type),
      store.getState,
      {}
    );

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_CLIENT_CONFIG_PAY_BY_PHONE.SAVE_CHANGE_SUCCESS
    });
  });

  it('should show success message when triggerSubmitPayByPhoneConfig fulfilled > with default', async () => {
    const mockAddToast = spyToast('addToast');

    await triggerSubmitPayByPhoneConfig(mockUndefinedArgs)(
      byPassFulfilled(triggerSubmitPayByPhoneConfig.fulfilled.type),
      store.getState,
      {}
    );

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_CLIENT_CONFIG_PAY_BY_PHONE.SAVE_CHANGE_SUCCESS
    });
  });

  it('should show fail message when triggerSubmitPayByPhoneConfig reject', async () => {
    const mockAddToast = spyToast('addToast');

    await triggerSubmitPayByPhoneConfig({
      ...mockSubmitArgs,
      maxDay: undefined
    })(
      byPassRejected(triggerSubmitPayByPhoneConfig.rejected.type),
      store.getState,
      {}
    );

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_CLIENT_CONFIG_PAY_BY_PHONE.SAVE_CHANGE_FAILURE
    });
  });
});
