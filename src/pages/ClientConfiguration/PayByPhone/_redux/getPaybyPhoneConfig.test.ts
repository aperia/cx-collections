import { createStore, Store } from '@reduxjs/toolkit';
import { byPassFulfilled, byPassRejected } from 'app/test-utils';

import { rootReducer } from 'storeConfig';
import { getPayByPhoneConfiguration } from './getPaybyPhoneConfig';
import { reducer } from './reducers';

const initialState: Partial<RootState> = {
  clientConfiguration: {
    settings: {
      selectedConfig: {
        id: '9999100020003000',
        agentId: '3000',
        clientId: '9999',
        systemId: '1000',
        principleId: '2000'
      }
    }
  },
  paybyPhoneConfig: {
    loading: false
  }
};

window.appConfig = {
  commonConfig: {
    user: 'user',
    app: 'app',
    org: 'org'
  } as CommonConfig
} as AppConfiguration;

describe('PayByPhone > redux getPaybyPhoneConfig', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, initialState);
  });

  it('pending', () => {
    const nextState = reducer(
      initialState.paybyPhoneConfig,
      getPayByPhoneConfiguration.pending('getPayByPhoneConfiguration', {})
    );

    expect(nextState.loading).toEqual(true);
  });

  it('fulfilled', () => {
    const nextState = reducer(
      initialState.paybyPhoneConfig,
      getPayByPhoneConfiguration.fulfilled(
        {
          maxDay: 10,
          paymentAgreementMessageWithConvenienceFee: 'test1',
          paymentAgreementMessageWithoutConvenienceFee: 'test2'
        },
        'getPayByPhoneConfiguration',
        {}
      )
    );

    expect(nextState.loading).toEqual(false);
    expect(nextState.currentValue?.maxDay).toEqual(10);
    expect(nextState.previousValue?.maxDay).toEqual(10);
  });

  it('rejected', () => {
    const nextState = reducer(
      initialState.paybyPhoneConfig,
      getPayByPhoneConfiguration.rejected(
        null,
        'getPayByPhoneConfiguration',
        {}
      )
    );

    expect(nextState.loading).toEqual(false);
  });

  it('should fulfilled', async () => {
    const response = await getPayByPhoneConfiguration({})(
      byPassFulfilled(getPayByPhoneConfiguration.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.type).toEqual(getPayByPhoneConfiguration.fulfilled.type);
  });

  it('should fulfilled with maxDay', async () => {
    const response = await getPayByPhoneConfiguration({})(
      byPassFulfilled(getPayByPhoneConfiguration.fulfilled.type, {
        payByPhoneConfig: {
          maxDay: 30
        }
      }),
      store.getState,
      {}
    );

    expect(response.type).toEqual(getPayByPhoneConfiguration.fulfilled.type);
  });

  it('should be rejected', async () => {
    const response = await getPayByPhoneConfiguration({})(
      byPassRejected(getPayByPhoneConfiguration.rejected.type),
      store.getState,
      {}
    );

    expect(response.type).toEqual(getPayByPhoneConfiguration.rejected.type);
  });
});
