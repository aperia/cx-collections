import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ClientConfigPayByPhoneState, PayByPhoneItem } from '../types';
import {
  getPayByPhoneConfiguration,
  getPayByPhoneConfigurationBuilder
} from './getPaybyPhoneConfig';
import {
  getPayByPhoneData,
  getPayByPhoneDataBuilder
} from './getPayByPhoneData';
import {
  triggerSubmitPayByPhoneConfig,
  triggerSubmitPayByPhoneConfigBuilder
} from './submitPaybyPhoneConfig';

export const initialState: ClientConfigPayByPhoneState = {
  loading: false
};

const { actions, reducer } = createSlice({
  name: 'clientConfigPaybyPhone',
  initialState,
  reducers: {
    resetToPrevious: draftState => {
      draftState.currentValue = draftState.previousValue;
    },
    setCurrentValue: (draftState, action: PayloadAction<PayByPhoneItem>) => {
      draftState.currentValue = action.payload;
    },
    removeStore: () => initialState
  },
  extraReducers: builder => {
    triggerSubmitPayByPhoneConfigBuilder(builder);
    getPayByPhoneConfigurationBuilder(builder);
    getPayByPhoneDataBuilder(builder);
  }
});

const clientConfigPayByPhoneActions = {
  ...actions,
  triggerSubmitPayByPhoneConfig,
  getPayByPhoneConfiguration,
  getPayByPhoneData
};

export { clientConfigPayByPhoneActions, reducer };
