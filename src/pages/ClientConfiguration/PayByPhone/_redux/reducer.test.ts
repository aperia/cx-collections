import { clientConfigPayByPhoneActions as actions, reducer } from './reducers';

import { ClientConfigPayByPhoneState } from '../types';

const initialState: ClientConfigPayByPhoneState = {
  loading: false,
  currentValue: {
    maxDay: 0,
    paymentAgreementMessageWithConvenienceFee: 'test1',
    paymentAgreementMessageWithoutConvenienceFee: 'test2'
  },
  previousValue: {
    maxDay: 0,
    paymentAgreementMessageWithConvenienceFee: 'test1',
    paymentAgreementMessageWithoutConvenienceFee: 'test2'
  }
};

describe('CallResult Reducers', () => {
  it('setCurrentValue', () => {
    const state = reducer(
      initialState,
      actions.setCurrentValue({
        maxDay: 20,
        paymentAgreementMessageWithConvenienceFee: 'test10',
        paymentAgreementMessageWithoutConvenienceFee: 'test20'
      })
    );

    expect(state.currentValue?.maxDay).toEqual(20);
    expect(
      state.currentValue?.paymentAgreementMessageWithConvenienceFee
    ).toEqual('test10');
    expect(
      state.currentValue?.paymentAgreementMessageWithoutConvenienceFee
    ).toEqual('test20');
  });

  it('resetToPrevious', () => {
    const state = reducer(initialState, actions.resetToPrevious());

    expect(state.currentValue).toEqual(initialState.previousValue);
  });

  it('removeStore', () => {
    const state = reducer(initialState, actions.removeStore());

    expect(state.currentValue).toBeUndefined();
  });
});
