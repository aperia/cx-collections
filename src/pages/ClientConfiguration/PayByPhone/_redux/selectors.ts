import { createSelector } from '@reduxjs/toolkit';
import { DEFAULT_PAY_BY_PHONE } from '../constants';

const getPayByPhoneConfigState = (state: RootState) => {
  return state.paybyPhoneConfig;
};

export const selectCurrentConfig = createSelector(
  getPayByPhoneConfigState,
  data => data?.currentValue || DEFAULT_PAY_BY_PHONE
);

export const selectPreviousConfig = createSelector(
  getPayByPhoneConfigState,
  data => data?.previousValue
);

export const selectLoading = createSelector(
  getPayByPhoneConfigState,
  data => data?.loading
);
