import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  PayloadAction
} from '@reduxjs/toolkit';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';
import { saveCollectionsClientConfig } from 'pages/ClientConfiguration/_redux/saveCollectionsClientConfig';

import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';

import { I18N_CLIENT_CONFIG_PAY_BY_PHONE } from '../constants';
import {
  ClientConfigPayByPhoneState,
  PayByPhoneArgs,
  GetPayByPhonePayload
} from '../types';

export const triggerSubmitPayByPhoneConfig = createAsyncThunk<
  GetPayByPhonePayload,
  PayByPhoneArgs,
  ThunkAPIConfig
>(
  'clientConfigPaybyPhone/triggerSubmitPayByPhoneConfig',
  async (args, thunkAPI) => {
    const { dispatch, rejectWithValue, getState } = thunkAPI;

    const {
      maxDay,
      paymentAgreementMessageWithConvenienceFee,
      paymentAgreementMessageWithoutConvenienceFee
    } = args;

    const currentData = getState().paybyPhoneConfig.currentValue;

    const response = await dispatch(
      saveCollectionsClientConfig({
        component: 'component_pay_by_phone_config',
        jsonValue: {
          payByPhoneConfig: {
            maxDay: maxDay?.toString() || '',
            paymentAgreementMessageWithConvenienceFee,
            paymentAgreementMessageWithoutConvenienceFee
          }
        },
        updateAction: 'delete'
      })
    );
    if (isFulfilled(response)) {
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_CLIENT_CONFIG_PAY_BY_PHONE.SAVE_CHANGE_SUCCESS
          })
        );
        dispatch(
          changeHistoryActions.saveChangedClientConfig({
            action: 'UPDATE',
            changedCategory: 'payByPhone',
            oldValue: {
              maxDay: currentData?.maxDay,
              paymentAgreementMessageWithConvenienceFee:
                currentData?.paymentAgreementMessageWithConvenienceFee,
              paymentAgreementMessageWithoutConvenienceFee:
                currentData?.paymentAgreementMessageWithoutConvenienceFee
            },
            newValue: {
              maxDay: maxDay ? maxDay.toString() : ' ',
              paymentAgreementMessageWithConvenienceFee,
              paymentAgreementMessageWithoutConvenienceFee
            }
          })
        );
      });
      return args;
    }

    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_CLIENT_CONFIG_PAY_BY_PHONE.SAVE_CHANGE_FAILURE
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION,
          forSection: 'inClientConfigurationFeatureHeader',
          apiResponse: response.payload?.response
        })
      );
    });
    return rejectWithValue({ errorMessage: response.error.message });
  }
);

export const triggerSubmitPayByPhoneConfigBuilder = (
  builder: ActionReducerMapBuilder<ClientConfigPayByPhoneState>
) => {
  builder
    .addCase(triggerSubmitPayByPhoneConfig.pending, (draftState, action) => {
      draftState.loading = true;
    })
    .addCase(
      triggerSubmitPayByPhoneConfig.fulfilled,
      (draftState, action: PayloadAction<GetPayByPhonePayload>) => {
        draftState.loading = false;
        draftState.previousValue = {
          ...action.payload
        };
        draftState.currentValue = {
          ...action.payload
        };
      }
    )
    .addCase(triggerSubmitPayByPhoneConfig.rejected, (draftState, action) => {
      draftState.loading = false;
    });
};
