import {
  byPassFulfilled,
  byPassRejected,
  responseDefault,
  storeId
} from 'app/test-utils';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { clientConfigPaybyPhoneService } from '../clientConfigPaybyPhoneService';
import {
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE,
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE
} from '../constants';
import { getPayByPhoneData } from './getPayByPhoneData';
import { reducer } from './reducers';

const initialState: Partial<RootState> = {
  clientConfiguration: {
    settings: {
      selectedConfig: {
        id: '9999100020003000',
        agentId: '3000',
        clientId: '9999',
        systemId: '1000',
        principleId: '2000'
      }
    }
  },
  paybyPhoneConfig: {
    loading: false
  }
};

window.appConfig = {
  commonConfig: {
    user: 'user',
    app: 'app',
    org: 'org'
  } as CommonConfig
} as AppConfiguration;

describe('PayByPhone > redux getPayByPhoneData', () => {
  let store: Store<RootState>;
  let spy: jest.SpyInstance;

  beforeEach(() => {
    store = createStore(rootReducer, initialState);
  });

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('pending', () => {
    const nextState = reducer(
      initialState.paybyPhoneConfig,
      getPayByPhoneData.pending('getPayByPhoneData', {})
    );

    expect(nextState.loading).toEqual(true);
  });

  it('fulfilled', () => {
    const nextState = reducer(
      initialState.paybyPhoneConfig,
      getPayByPhoneData.fulfilled(
        {
          maxDay: 10,
          paymentAgreementMessageWithConvenienceFee: 'test1',
          paymentAgreementMessageWithoutConvenienceFee: 'test2'
        },
        'getPayByPhoneConfiguration',
        {}
      )
    );

    expect(nextState.loading).toEqual(false);
    expect(nextState.currentValue?.maxDay).toEqual(10);
  });

  it('rejected', () => {
    const nextState = reducer(
      initialState.paybyPhoneConfig,
      getPayByPhoneData.rejected(null, 'getPayByPhoneConfiguration', {})
    );

    expect(nextState.loading).toEqual(false);
  });

  it('should fulfilled', async () => {
    spy = jest
      .spyOn(clientConfigPaybyPhoneService, 'getPayByPhoneData')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          codeToText: {
            components: [
              {
                component: 'component_pay_by_phone_config',
                jsonValue: {
                  payByPhoneConfig: {}
                }
              }
            ]
          }
        }
      });

    const response = await getPayByPhoneData({ storeId: storeId })(
      byPassFulfilled(getPayByPhoneData.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      maxDay: undefined,
      paymentAgreementMessageWithConvenienceFee:
        DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE,
      paymentAgreementMessageWithoutConvenienceFee:
        DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE
    });
  });

  it('should fulfilled with maxDay', async () => {
    spy = jest
      .spyOn(clientConfigPaybyPhoneService, 'getPayByPhoneData')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          codeToText: {
            components: [
              {
                component: 'component_pay_by_phone_config',
                jsonValue: {
                  payByPhoneConfig: {
                    maxDay: 30,
                    paymentAgreementMessageWithConvenienceFee: 'test',
                    paymentAgreementMessageWithoutConvenienceFee: 'test'
                  }
                }
              }
            ]
          }
        }
      });

    const response = await getPayByPhoneData({ storeId: storeId })(
      byPassFulfilled(getPayByPhoneData.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      maxDay: 30,
      paymentAgreementMessageWithConvenienceFee: 'test',
      paymentAgreementMessageWithoutConvenienceFee: 'test'
    });
  });

  it('should be rejected', async () => {
    const response = await getPayByPhoneData({})(
      byPassRejected(getPayByPhoneData.rejected.type),
      store.getState,
      {}
    );

    expect(response.type).toEqual(getPayByPhoneData.rejected.type);
  });
});
