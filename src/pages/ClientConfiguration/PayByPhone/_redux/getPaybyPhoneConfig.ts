import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';

import { getCollectionsClientConfig } from 'pages/ClientConfiguration/_redux/getCollectionsClientConfig';
import {
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE,
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE,
} from '../constants';
import {
  ClientConfigPayByPhoneState,
  GetPayByPhoneArgs,
  GetPayByPhonePayload
} from '../types';

export const getPayByPhoneConfiguration = createAsyncThunk<
  GetPayByPhonePayload,
  GetPayByPhoneArgs | undefined,
  ThunkAPIConfig
>(
  'clientConfigPaybyPhone/getPaybyPhoneConfig',
  async (_, { dispatch, rejectWithValue }) => {
    const response = await dispatch(
      getCollectionsClientConfig({
        component: 'component_pay_by_phone_config',
        storeId: _?.storeId
      })
    );

    if (isFulfilled(response)) {
      const {
        maxDay,
        paymentAgreementMessageWithConvenienceFee = DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE,
        paymentAgreementMessageWithoutConvenienceFee = DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE
      } = response.payload?.payByPhoneConfig || {};
      return {
        maxDay: maxDay ? +maxDay : undefined,
        paymentAgreementMessageWithConvenienceFee,
        paymentAgreementMessageWithoutConvenienceFee
      };
    }
    return rejectWithValue({ errorMessage: response.error.message });
  }
);

export const getPayByPhoneConfigurationBuilder = (
  builder: ActionReducerMapBuilder<ClientConfigPayByPhoneState>
) => {
  builder
    .addCase(getPayByPhoneConfiguration.pending, draftState => {
      draftState.loading = true;
    })
    .addCase(getPayByPhoneConfiguration.fulfilled, (draftState, action) => {
      draftState.previousValue = {
        ...action.payload
      };
      draftState.currentValue = {
        ...action.payload
      };
      draftState.loading = false;
    })
    .addCase(getPayByPhoneConfiguration.rejected, (draftState, action) => {
      draftState.loading = false;
    });
};
