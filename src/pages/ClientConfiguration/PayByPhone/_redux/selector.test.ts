import { selectorWrapper } from 'app/test-utils';
import { DEFAULT_PAY_BY_PHONE } from '../constants';
import * as selectors from './selectors';

const store: Partial<RootState> = {
  paybyPhoneConfig: {
    loading: true,
    currentValue: DEFAULT_PAY_BY_PHONE,
    previousValue: DEFAULT_PAY_BY_PHONE
  }
};

describe('PayByPhone Selectors', () => {
  it('selectLoading', () => {
    const { data } = selectorWrapper(store)(selectors.selectLoading);

    expect(data).toEqual(store.paybyPhoneConfig?.loading);
  });
  it('selectCurrentConfig', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectCurrentConfig
    );

    expect(data).toEqual(store.paybyPhoneConfig?.currentValue);
    expect(emptyData).toEqual(DEFAULT_PAY_BY_PHONE);
  });
  it('selectPreviousConfig', () => {
    const { data } = selectorWrapper(store)(selectors.selectPreviousConfig);

    expect(data).toEqual(store.paybyPhoneConfig?.previousValue);
  });
});
