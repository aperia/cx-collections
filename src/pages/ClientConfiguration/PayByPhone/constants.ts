import { PayByPhoneItem } from './types';

export const I18N_CLIENT_CONFIG_PAY_BY_PHONE = {
  PAY_BY_PHONE: 'txt_pay_by_phone',
  PAY_BY_PHONE_TITLE: 'txt_pay_by_phone_title',
  PAY_BY_PHONE_MAX_NUMBER_OF_DAY: 'txt_pay_by_phone_max_number_of_day',
  PAY_BY_PHONE_DESCRIPTION: 'txt_pay_by_phone_description',
  PAY_BY_PHONE_ERROR: 'txt_pay_by_phone_error',
  RESET_TO_PREVIOUS: 'txt_reset_to_previous',
  UNSAVED_CHANGE_BODY: 'txt_unsaved_change_body',
  DISCARD_CHANGE_BODY: 'txt_discard_change_body',
  UNSAVED_CHANGE: 'txt_unsaved_changes',
  CONTINUE_EDITING: 'txt_continue_editing',
  SAVE_AND_CONTINUE: 'txt_save_and_continue',
  DISCARD_CHANGE: 'txt_discard_change',
  SAVE_CHANGES: 'txt_save_changes',
  SAVE_CHANGE_SUCCESS: 'txt_pay_by_phone_save_success',
  SAVE_CHANGE_FAILURE: 'txt_pay_by_phone_save_failed',
  RESET_PAY_BY_PHONE_SUCCESS: 'txt_pay_by_phone_reset_to_previous_success',
  PAYMENT_DAYS: 'txt_payment_days',
  PAYMENT_CONFIRMATION_MESSAGE: 'txt_payment_confirmation_message',
  PAYMENT_CONFIRMATION_MESSAGE_DESCRIPTION:
    'txt_payment_confirmation_message_description',
  PAYMENT_AGREEMENT_MESSAGE_WITH_FEE:
    'txt_payment_agreement_message_with_convenience_fee',
  PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE:
    'txt_payment_agreement_message_without_convenience_fee',
  PAYMENT_AGREEMENT_CHARACTERS_LEFT: 'txt_payment_agreement_characters_left',
  SET_AS_DEFAULT_MESSAGE: 'txt_set_as_default_message',
  MESSAGE_CONTENT_IS_REQUIRED: 'txt_message_content_is_required'
};

export const MAX_NUMBER_OF_DAY = {
  MAX_DAY: 365
};

export const MENTION_DATA = [
  'Account Number',
  'Convenience Fee',
  'Payment Amount',
  'Payment Date'
];

export const PAYMENT_AGREEMENT_MAX_LENGTH = 2000;

export const FORM_FIELDS = {
  MAX_DAY: 'maxDay',
  PAYMENT_AGREEMENT_MESSAGE_WITH_CONVENIENCE_FEE:
    'paymentAgreementMessageWithConvenienceFee',
  PAYMENT_AGREEMENT_MESSAGE_WITHOUT_CONVENIENCE_FEE:
    'paymentAgreementMessageWithoutConvenienceFee'
};

export const DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE = `Before I process your payment, let me review your payment information with you. Today you are authorizing us to deduct the amount of {Payment Amount} from your account ending in {Account Number} via a one-time electronic funds transfer. You will be charged a {Convenience Fee} convenience fee. Your payment will be debited from your account on or after {Payment Date}. If you have questions or would like to revoke this authorization, please call 1-800-575-6457. Can you please confirm your authorization to proceed?`;

export const DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE = `Before I process your payment, let me review your payment information with you. Today you are authorizing us to deduct the amount of {Payment Amount} from your account ending in {Account Number} via a one-time electronic funds transfer. Your payment will be debited from your account on or after {Payment Date} .If you have questions or would like to revoke this authorization, please call 1-800-575-6457. Can you please confirm your authorization to proceed?`;

export const DEFAULT_PAY_BY_PHONE: PayByPhoneItem = {
  maxDay: undefined,
  paymentAgreementMessageWithConvenienceFee: '',
  paymentAgreementMessageWithoutConvenienceFee: ''
};
