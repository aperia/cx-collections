import React, { useCallback, useEffect, useRef, useState } from 'react';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import { selectCurrentConfig } from './_redux/selectors';
import { clientConfigPayByPhoneActions } from './_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';

// components
import {
  Button,
  NumericTextBox,
  SimpleBar
} from 'app/_libraries/_dls/components';
import TextEditor, {
  replaceMentionWith,
  revertMentionWith
} from 'app/_libraries/_dls/components/TextEditor';
import ApiErrorForHeaderSection from '../ApiErrorSection/ApiErrorForHeaderSection';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';
import { useListenClosingModal } from '../hooks/useListenClosingModal';
import { useListenChangingTabEvent } from '../hooks/useListenChangingTabEvent';

// constants & types & helpers
import {
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE,
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE,
  FORM_FIELDS,
  I18N_CLIENT_CONFIG_PAY_BY_PHONE,
  MAX_NUMBER_OF_DAY,
  MENTION_DATA,
  PAYMENT_AGREEMENT_MAX_LENGTH
} from './constants';
import { REGEX_CHECK_SPECIAL_CHARS } from '../constants';
import { I18N_SETTLEMENT_PAYMENT } from '../SettlementPayment/constants';

import { PayByPhoneErrors, PayByPhoneItem } from './types';
import { TextEditorRef } from 'app/_libraries/_dls/components/TextEditor/types';

import { genAmtId } from 'app/_libraries/_dls/utils';
import isEmpty from 'lodash.isempty';
import isEqual from 'lodash.isequal';
import {
  dispatchCloseModalEvent,
  dispatchControlTabChangeEvent
} from 'pages/ClientConfiguration/helpers';
import { formatCommon } from 'app/helpers';
import {
  replaceMentionText,
  replaceSpecialValidChars
} from '../SettlementPayment/helpers';

const PayByPhone = () => {
  const dispatch = useDispatch();

  const testId = 'clientConfig_payByPhone';

  const agreementMessageWithFeeEditorRef = useRef<TextEditorRef | null>(null);
  const agreementMessageWithoutFeeEditorRef =
    useRef<TextEditorRef | null>(null);

  const { t } = useTranslation();

  const currentValue = useSelector(selectCurrentConfig);
  const status = useCheckClientConfigStatus();

  const [agreementMessageWithFeeLength, setAgreementMessageWithFeeLength] =
    useState(0);
  const [
    agreementMessageWithoutFeeLength,
    setAgreementMessageWithoutFeeLength
  ] = useState(0);

  const [values, setValues] = useState<PayByPhoneItem>({
    ...currentValue
  });
  const [errors, setErrors] = useState<PayByPhoneErrors>(
    {} as PayByPhoneErrors
  );

  const isDataChanged = !isEqual({ ...currentValue }, { ...values });

  const handleSaveChanges = () => {
    const amWithCFMention = replaceMentionWith(
      agreementMessageWithFeeEditorRef.current!.getMarkdown()
    );
    const amWithoutCFMention = replaceMentionWith(
      agreementMessageWithoutFeeEditorRef.current!.getMarkdown()
    );
    const requestBody = {
      maxDay: values.maxDay,
      paymentAgreementMessageWithConvenienceFee: amWithCFMention,
      paymentAgreementMessageWithoutConvenienceFee: amWithoutCFMention
    };
    dispatch(
      clientConfigPayByPhoneActions.triggerSubmitPayByPhoneConfig(requestBody)
    );
  };

  const handleResetToPrev = () => {
    if (!isDataChanged) return;
    // handle Reset
    agreementMessageWithFeeEditorRef.current?.setMarkdown(
      revertMentionWith(currentValue.paymentAgreementMessageWithConvenienceFee!)
    );
    agreementMessageWithoutFeeEditorRef.current?.setMarkdown(
      revertMentionWith(
        currentValue.paymentAgreementMessageWithoutConvenienceFee!
      )
    );
    setValues({ ...currentValue });
    clearErrors('', true);
    batch(() => {
      dispatch(clientConfigPayByPhoneActions.resetToPrevious());
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_CLIENT_CONFIG_PAY_BY_PHONE.RESET_PAY_BY_PHONE_SUCCESS
        })
      );
    });
  };

  const handleChangePaymentAgreementWithFee = (text: string) => {
    setAgreementMessageWithFeeLength(text?.length);
  };

  const handleChangePaymentAgreementWithoutFee = (text: string) => {
    setAgreementMessageWithoutFeeLength(text?.length);
  };

  const getCharactersLeft = useCallback(
    (current: number) => {
      const charsLeft = formatCommon(
        PAYMENT_AGREEMENT_MAX_LENGTH - current
      ).quantity;
      return `${charsLeft} ${t(
        I18N_CLIENT_CONFIG_PAY_BY_PHONE.PAYMENT_AGREEMENT_CHARACTERS_LEFT
      )}`;
    },
    [t]
  );

  const handleChangingTab = () => {
    batch(() => {
      dispatchControlTabChangeEvent();
      dispatch(clientConfigPayByPhoneActions.removeStore());
    });
  };

  const handleClosingModal = () => {
    batch(() => {
      dispatchCloseModalEvent();
      dispatch(clientConfigPayByPhoneActions.removeStore());
    });
  };

  const validateCustom = (textData: string, editorId: string) => {
    clearErrors(editorId);
    const reg = new RegExp(REGEX_CHECK_SPECIAL_CHARS, 'g');
    const formatTextData = replaceSpecialValidChars(textData);

    if (formatTextData?.length === 0) {
      setErrors({
        ...errors,
        [editorId]: I18N_CLIENT_CONFIG_PAY_BY_PHONE.MESSAGE_CONTENT_IS_REQUIRED
      });
    }
    if (reg.test(formatTextData)) {
      setErrors({
        ...errors,
        [editorId]: I18N_SETTLEMENT_PAYMENT.NON_SPECIAL_CHARS
      });
    }
  };

  const clearErrors = (fieldId: string, isClearAll?: boolean) => {
    if (isClearAll) {
      setErrors({});
      return;
    }
    const cloneErrors = { ...errors };
    delete cloneErrors?.[fieldId as keyof PayByPhoneErrors];
    setErrors({ ...cloneErrors });
  };

  const handleBlurNumeric = (e: any) => {
    if (e.target.value && Number(e.target.value) > MAX_NUMBER_OF_DAY.MAX_DAY) {
      setErrors({
        ...errors,
        maxDay: I18N_CLIENT_CONFIG_PAY_BY_PHONE.PAY_BY_PHONE_ERROR
      });
    } else {
      clearErrors(FORM_FIELDS.MAX_DAY);
    }
  };

  const handleChangeNumerixBox = (e: any) => {
    setValues({ ...values, maxDay: Number(e.target.value) });
  };

  const handleBlurEditor = (editorId: string) => {
    if (
      editorId === FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITH_CONVENIENCE_FEE
    ) {
      const markdown = replaceMentionWith(
        agreementMessageWithFeeEditorRef?.current!.getMarkdown()
      ).trim();
      validateCustom(replaceMentionText(markdown), editorId);
      setValues({
        ...values,
        paymentAgreementMessageWithConvenienceFee: markdown
      });
    } else {
      const markdown = replaceMentionWith(
        agreementMessageWithoutFeeEditorRef?.current!.getMarkdown()
      ).trim();
      validateCustom(replaceMentionText(markdown), editorId);
      setValues({
        ...values,
        paymentAgreementMessageWithoutConvenienceFee: markdown
      });
    }
  };

  const setDefaultMessage = (editorId: string) => {
    if (
      editorId === FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITH_CONVENIENCE_FEE
    ) {
      const markdown = replaceMentionWith(
        agreementMessageWithFeeEditorRef?.current!.getMarkdown()
      );
      if (isEqual(markdown, DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE)) return;
      agreementMessageWithFeeEditorRef.current?.setMarkdown(
        revertMentionWith(DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE)
      );
      setValues({
        ...values,
        paymentAgreementMessageWithConvenienceFee:
          DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE
      });
    } else {
      const markdown = replaceMentionWith(
        agreementMessageWithoutFeeEditorRef?.current!.getMarkdown()
      );
      if (isEqual(markdown, DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE)) {
        return;
      }
      agreementMessageWithoutFeeEditorRef.current?.setMarkdown(
        revertMentionWith(DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE)
      );
      setValues({
        ...values,
        paymentAgreementMessageWithoutConvenienceFee:
          DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE
      });
    }
  };

  useListenChangingTabEvent(() => {
    if (isEqual(currentValue, values)) {
      handleChangingTab();
      return;
    }
    dispatch(
      confirmModalActions.open({
        title: I18N_CLIENT_CONFIG_PAY_BY_PHONE.UNSAVED_CHANGE,
        body: I18N_CLIENT_CONFIG_PAY_BY_PHONE.DISCARD_CHANGE_BODY,
        cancelText: I18N_CLIENT_CONFIG_PAY_BY_PHONE.CONTINUE_EDITING,
        confirmText: I18N_CLIENT_CONFIG_PAY_BY_PHONE.DISCARD_CHANGE,
        confirmCallback: handleChangingTab,
        variant: 'danger'
      })
    );
  });

  useListenClosingModal(() => {
    if (isEqual(currentValue, values)) {
      handleClosingModal();
      return;
    }
    dispatch(
      confirmModalActions.open({
        title: I18N_CLIENT_CONFIG_PAY_BY_PHONE.UNSAVED_CHANGE,
        body: I18N_CLIENT_CONFIG_PAY_BY_PHONE.DISCARD_CHANGE_BODY,
        cancelText: I18N_CLIENT_CONFIG_PAY_BY_PHONE.CONTINUE_EDITING,
        confirmText: I18N_CLIENT_CONFIG_PAY_BY_PHONE.DISCARD_CHANGE,
        confirmCallback: handleClosingModal,
        variant: 'danger'
      })
    );
  });

  useEffect(() => {
    dispatch(clientConfigPayByPhoneActions.getPayByPhoneConfiguration());
  }, [dispatch]);

  useEffect(() => {
    setValues({ ...currentValue });
  }, [currentValue]);

  useEffect(() => {
    const revertMarkDown = revertMentionWith(
      currentValue.paymentAgreementMessageWithConvenienceFee || ''
    );
    agreementMessageWithFeeEditorRef.current?.setMarkdown(revertMarkDown);
  }, [currentValue.paymentAgreementMessageWithConvenienceFee]);

  useEffect(() => {
    const revertMarkDown = revertMentionWith(
      currentValue.paymentAgreementMessageWithoutConvenienceFee || ''
    );
    agreementMessageWithoutFeeEditorRef.current?.setMarkdown(revertMarkDown);
  }, [currentValue.paymentAgreementMessageWithoutConvenienceFee]);

  const EDITORS = [
    {
      id: FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITH_CONVENIENCE_FEE,
      title: I18N_CLIENT_CONFIG_PAY_BY_PHONE.PAYMENT_AGREEMENT_MESSAGE_WITH_FEE,
      onPlainTextChange: handleChangePaymentAgreementWithFee,
      editorRef: agreementMessageWithFeeEditorRef,
      currentLength: agreementMessageWithFeeLength
    },
    {
      id: FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITHOUT_CONVENIENCE_FEE,
      title:
        I18N_CLIENT_CONFIG_PAY_BY_PHONE.PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE,
      onPlainTextChange: handleChangePaymentAgreementWithoutFee,
      editorRef: agreementMessageWithoutFeeEditorRef,
      currentLength: agreementMessageWithoutFeeLength
    }
  ];
  return (
    <div className="position-relative has-footer-button">
      <SimpleBar>
        <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
          <ApiErrorForHeaderSection
            className="mb-24"
            dataTestId={`${testId}_apiError`}
          />
          <>
            <h5 data-testid={genAmtId(testId, 'title', '')}>
              {t(I18N_CLIENT_CONFIG_PAY_BY_PHONE.PAY_BY_PHONE)}
            </h5>
            <p className="fw-500 mt-16">
              {t(I18N_CLIENT_CONFIG_PAY_BY_PHONE.PAYMENT_DAYS)}
            </p>
            <p
              className="mt-8"
              data-testid={genAmtId(testId, 'description', '')}
            >
              {t(I18N_CLIENT_CONFIG_PAY_BY_PHONE.PAY_BY_PHONE_TITLE)}
            </p>
            <div className="w-300px mt-16">
              <NumericTextBox
                readOnly={status}
                id={FORM_FIELDS.MAX_DAY}
                label={t(
                  I18N_CLIENT_CONFIG_PAY_BY_PHONE.PAY_BY_PHONE_MAX_NUMBER_OF_DAY
                )}
                format={'n0'}
                name={FORM_FIELDS.MAX_DAY}
                value={values.maxDay}
                onChange={handleChangeNumerixBox}
                onBlur={handleBlurNumeric}
                dataTestId={`${testId}_maxDay`}
                error={
                  errors.maxDay
                    ? {
                        status: true,
                        message: t(errors.maxDay)
                      }
                    : undefined
                }
              />
            </div>
            <p
              className="mt-8 fs-12 color-grey-l24"
              data-testid={genAmtId(testId, 'numOfDay_description', '')}
            >
              {t(I18N_CLIENT_CONFIG_PAY_BY_PHONE.PAY_BY_PHONE_DESCRIPTION)}
            </p>
            <div className="mt-24">
              <b>
                {t(
                  I18N_CLIENT_CONFIG_PAY_BY_PHONE.PAYMENT_CONFIRMATION_MESSAGE
                )}
              </b>
              <p
                className="mt-8"
                data-testid={genAmtId(testId, 'payment_confirm_message', '')}
              >
                {t(
                  I18N_CLIENT_CONFIG_PAY_BY_PHONE.PAYMENT_CONFIRMATION_MESSAGE_DESCRIPTION
                )}
              </p>
              {EDITORS.map(item => (
                <div key={item.id}>
                  <div className="custom-editor-payment">
                    <h6 className="color-grey mt-16 mb-8">
                      {t(item.title)}
                      <span className="color-red"> *</span>
                    </h6>
                    <TextEditor
                      dataTestId={`${testId}_${item.id}_editor`}
                      onPlainTextChange={item.onPlainTextChange}
                      placeholder={t('txt_enter_message')}
                      maxPlainText={PAYMENT_AGREEMENT_MAX_LENGTH}
                      mention
                      mentionData={MENTION_DATA}
                      onBlur={() => {
                        handleBlurEditor(item.id);
                      }}
                      ref={item.editorRef}
                      error={
                        errors[item.id as keyof PayByPhoneItem]
                          ? {
                              status: true,
                              message: t(
                                errors[item.id as keyof PayByPhoneItem]
                              )
                            }
                          : undefined
                      }
                    />
                    <div className="d-flex align-items-center justify-content-between mt-4">
                      <p
                        className="fs-12 color-grey-l24"
                        data-testid={genAmtId(
                          testId,
                          `${item.id}_character_left`,
                          ''
                        )}
                      >
                        {getCharactersLeft(item.currentLength)}
                      </p>
                      <Button
                        size="sm"
                        className="mr-n8"
                        dataTestId={`${testId}_${item.id}_setDefaultButton`}
                        variant="outline-primary"
                        onClick={() => setDefaultMessage(item.id)}
                      >
                        {t(
                          I18N_CLIENT_CONFIG_PAY_BY_PHONE.SET_AS_DEFAULT_MESSAGE
                        )}
                      </Button>
                    </div>
                  </div>
                  {item.id ===
                  FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITH_CONVENIENCE_FEE ? (
                    <div className="divider-dashed mt-12"></div>
                  ) : (
                    ''
                  )}
                </div>
              ))}
            </div>
          </>
        </div>
      </SimpleBar>
      <div className="group-button-footer d-flex justify-content-end">
        <Button
          disabled={status}
          onClick={handleResetToPrev}
          variant="secondary"
          dataTestId={`${testId}_resetToPreviousBtn`}
        >
          {t(I18N_CLIENT_CONFIG_PAY_BY_PHONE.RESET_TO_PREVIOUS)}
        </Button>
        <Button
          onClick={handleSaveChanges}
          type="submit"
          variant="primary"
          disabled={
            status ||
            (values.maxDay && values.maxDay > MAX_NUMBER_OF_DAY.MAX_DAY) ||
            !isEmpty(errors) ||
            !isDataChanged
          }
          dataTestId={`${testId}_saveChangesBtn`}
        >
          {t(I18N_CLIENT_CONFIG_PAY_BY_PHONE.SAVE_CHANGES)}
        </Button>
      </div>
    </div>
  );
};

export default PayByPhone;
