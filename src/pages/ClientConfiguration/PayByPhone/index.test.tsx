import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';

// component
import PayByPhone from '.';

// redux
import { clientConfigPayByPhoneActions } from './_redux/reducers';

// types
import { fireEvent, screen } from '@testing-library/dom';
import {
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE,
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE,
  FORM_FIELDS,
  I18N_CLIENT_CONFIG_PAY_BY_PHONE
} from './constants';
import { CLIENT_CONFIG_STATUS_KEY, SECTION_TAB_EVENT } from '../constants';
import * as status from '../hooks/useCheckClientConfigStatus';
import { act } from 'react-dom/test-utils';
import * as helpers from '../helpers';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';

jest.mock('app/_libraries/_dls/components/TextEditor', () => {
  return {
    __esModule: true,
    ...jest.requireActual('app/_libraries/_dls/components/TextEditor'),
    ...jest.requireActual('app/test-utils/mocks/MockPayByPhoneTextEditor')
  };
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockInitialState: Partial<RootState> = {
  clientConfiguration: {
    settings: {
      selectedConfig: {
        additionalFields: [
          {
            name: CLIENT_CONFIG_STATUS_KEY,
            active: true
          }
        ]
      }
    }
  },
  paybyPhoneConfig: {
    loading: false,
    currentValue: {
      maxDay: 15,
      paymentAgreementMessageWithConvenienceFee: 'test1',
      paymentAgreementMessageWithoutConvenienceFee: 'test2'
    },
    previousValue: {
      maxDay: 10,
      paymentAgreementMessageWithConvenienceFee: 'test10',
      paymentAgreementMessageWithoutConvenienceFee: 'test20'
    }
  }
};

let spy: jest.SpyInstance;
let spyAction: jest.SpyInstance;

const emitChangingTabEvent = () => {
  const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGING_TAB, {
    bubbles: true,
    cancelable: true
  });

  window.dispatchEvent(event);
};

const emitClosingModalEvent = () => {
  const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
    bubbles: true,
    cancelable: true
  });

  window.dispatchEvent(event);
};

const renderWrapper = (initialState: Partial<RootState>) =>
  renderMockStore(<PayByPhone />, { initialState });

const testId = 'clientConfig_payByPhone';

const payByPhoneSpy = mockActionCreator(clientConfigPayByPhoneActions);
const triggerSaveChangeSpy = payByPhoneSpy('triggerSubmitPayByPhoneConfig');

describe('PayByPhone', () => {
  beforeEach(() => {
    jest.spyOn(status, 'useCheckClientConfigStatus').mockReturnValue(false);
  });

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();

    spyAction?.mockReset();
    spyAction?.mockRestore();
  });

  it('should render UI', () => {
    jest.useFakeTimers();

    renderWrapper(mockInitialState);

    jest.runAllTimers();

    expect(
      screen.getByText(I18N_CLIENT_CONFIG_PAY_BY_PHONE.PAY_BY_PHONE)
    ).toBeInTheDocument();
  });

  it('should click save button', () => {
    jest.useFakeTimers();

    renderWrapper(mockInitialState);

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_payByPhone_paymentAgreementMessageWithConvenienceFee_setDefaultButton_dls-button'
        )
      );
    });
    jest.runAllTimers();

    fireEvent.click(
      screen.getByTestId('clientConfig_payByPhone_saveChangesBtn_dls-button')
    );
    jest.runAllTimers();
    expect(triggerSaveChangeSpy).not.toBeCalled();
  });

  it('should click reset button', () => {
    renderWrapper(mockInitialState);

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_payByPhone_paymentAgreementMessageWithConvenienceFee_setDefaultButton_dls-button'
        )
      );
    });
    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_payByPhone_resetToPreviousBtn_dls-button'
        )
      );
    });
    jest.runAllTimers();
  });

  it('should click reset button > case 2', () => {
    renderWrapper(mockInitialState);

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_payByPhone_resetToPreviousBtn_dls-button'
        )
      );
    });
    jest.runAllTimers();
  });
});

describe('payment agreement text editor event', () => {
  it('on blur editor maxday', () => {
    jest.useFakeTimers();

    renderWrapper(mockInitialState);

    jest.runAllTimers();

    const editor = screen.getByTestId(
      `clientConfig_payByPhone_maxDay_dls-numeric-textbox-input`
    );

    fireEvent.change(editor, { target: { value: 30 } });
    fireEvent.click(editor);
    fireEvent.blur(editor);
    jest.runAllTimers();
  });

  it('on blur editor maxday > case 2', () => {
    jest.useFakeTimers();

    renderWrapper(mockInitialState);

    jest.runAllTimers();

    const editor = screen.getByTestId(
      `clientConfig_payByPhone_maxDay_dls-numeric-textbox-input`
    );

    fireEvent.change(editor, { target: { value: 500 } });
    fireEvent.click(editor);
    fireEvent.blur(editor);
    jest.runAllTimers();
  });
  it('on blur editor agreement message with fee when formatTextData empty', () => {
    jest.useFakeTimers();

    renderWrapper({
      ...mockInitialState,
      paybyPhoneConfig: {
        loading: false,
        currentValue: {
          maxDay: 15,
          paymentAgreementMessageWithConvenienceFee: '',
          paymentAgreementMessageWithoutConvenienceFee: 'test2'
        },
        previousValue: {
          maxDay: 10,
          paymentAgreementMessageWithConvenienceFee: 'test10',
          paymentAgreementMessageWithoutConvenienceFee: 'test20'
        }
      }
    } as Partial<RootState>);

    jest.runAllTimers();

    const editor = screen.getByTestId(
      `${testId}_${FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITH_CONVENIENCE_FEE}_editor`
    );
    fireEvent.change(editor, { target: { value: 'message content %%%' } });
    fireEvent.click(editor);
    fireEvent.blur(editor);
    jest.runAllTimers();
  });

  it('on blur editor agreement message with fee', () => {
    jest.useFakeTimers();

    renderWrapper(mockInitialState);

    jest.runAllTimers();

    const editor = screen.getByTestId(
      `${testId}_${FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITH_CONVENIENCE_FEE}_editor`
    );
    fireEvent.change(editor, { target: { value: 'message content %%%' } });
    fireEvent.click(editor);
    fireEvent.blur(editor);
    jest.runAllTimers();
  });

  it('on blur editor agreement message without fee when formatTextData empty', () => {
    jest.useFakeTimers();

    renderWrapper({
      ...mockInitialState,
      paybyPhoneConfig: {
        loading: false,
        currentValue: {
          maxDay: 15,
          paymentAgreementMessageWithConvenienceFee: 'test1',
          paymentAgreementMessageWithoutConvenienceFee: ''
        },
        previousValue: {
          maxDay: 10,
          paymentAgreementMessageWithConvenienceFee: 'test10',
          paymentAgreementMessageWithoutConvenienceFee: 'test20'
        }
      }
    } as Partial<RootState>);

    jest.runAllTimers();

    const editor = screen.getByTestId(
      `${testId}_${FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITHOUT_CONVENIENCE_FEE}_editor`
    );

    fireEvent.change(editor, { target: { value: 'message content' } });
    fireEvent.click(editor);
    fireEvent.blur(editor);
    jest.runAllTimers();
  });

  it('on blur editor agreement message without fee', () => {
    jest.useFakeTimers();

    renderWrapper(mockInitialState);

    jest.runAllTimers();

    const editor = screen.getByTestId(
      `${testId}_${FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITHOUT_CONVENIENCE_FEE}_editor`
    );

    fireEvent.change(editor, { target: { value: 'message content' } });
    fireEvent.click(editor);
    fireEvent.blur(editor);
    jest.runAllTimers();
  });
});

describe('Actions', () => {
  it('Set default values > case 1', () => {
    jest.useFakeTimers();

    renderWrapper({
      ...mockInitialState,
      paybyPhoneConfig: {
        loading: false,
        currentValue: {
          maxDay: 10,
          paymentAgreementMessageWithConvenienceFee:
            DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE,
          paymentAgreementMessageWithoutConvenienceFee:
            DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE
        },
        previousValue: {
          maxDay: 20,
          paymentAgreementMessageWithConvenienceFee: 'test1',
          paymentAgreementMessageWithoutConvenienceFee: 'test2'
        }
      }
    });

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_payByPhone_paymentAgreementMessageWithConvenienceFee_setDefaultButton_dls-button'
        )
      );
    });
    jest.runAllTimers();
  });

  it('Set default values > case 1 > return', () => {
    jest.useFakeTimers();

    renderWrapper({
      ...mockInitialState,
      paybyPhoneConfig: {
        loading: false,
        currentValue: {
          maxDay: 10,
          paymentAgreementMessageWithConvenienceFee: 'XXX',
          paymentAgreementMessageWithoutConvenienceFee: 'YYY'
        },
        previousValue: {
          maxDay: 20,
          paymentAgreementMessageWithConvenienceFee: 'test1',
          paymentAgreementMessageWithoutConvenienceFee: 'test2'
        }
      }
    });

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_payByPhone_paymentAgreementMessageWithConvenienceFee_setDefaultButton_dls-button'
        )
      );
      fireEvent.click(
        screen.getByTestId('clientConfig_payByPhone_saveChangesBtn_dls-button')
      );
    });
    jest.runAllTimers();
  });

  it('Set default values > case 2 empty', () => {
    jest.useFakeTimers();

    renderWrapper({
      ...mockInitialState,
      paybyPhoneConfig: {
        loading: false,
        currentValue: {
          maxDay: 10,
          paymentAgreementMessageWithConvenienceFee:
            DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE,
          paymentAgreementMessageWithoutConvenienceFee: ''
        },
        previousValue: {
          maxDay: 20,
          paymentAgreementMessageWithConvenienceFee: 'test1',
          paymentAgreementMessageWithoutConvenienceFee: 'test2'
        }
      }
    });

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_payByPhone_paymentAgreementMessageWithoutConvenienceFee_setDefaultButton_dls-button'
        )
      );
      fireEvent.click(
        screen.getByTestId('clientConfig_payByPhone_saveChangesBtn_dls-button')
      );
    });
    jest.runAllTimers();
  });

  it('Set default values > case 2', () => {
    jest.useFakeTimers();

    renderWrapper({
      ...mockInitialState,
      paybyPhoneConfig: {
        loading: false,
        currentValue: {
          maxDay: 10,
          paymentAgreementMessageWithConvenienceFee: 'test',
          paymentAgreementMessageWithoutConvenienceFee:
            DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE
        },
        previousValue: {
          maxDay: 20,
          paymentAgreementMessageWithConvenienceFee: 'test1',
          paymentAgreementMessageWithoutConvenienceFee: 'test2'
        }
      }
    });

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_payByPhone_paymentAgreementMessageWithoutConvenienceFee_setDefaultButton_dls-button'
        )
      );
      fireEvent.click(
        screen.getByTestId('clientConfig_payByPhone_saveChangesBtn_dls-button')
      );
    });
    jest.runAllTimers();
  });
});

describe('trigger useListenChangingTabEvent', () => {
  const mockDispatchControlTabChangeEvent = jest
    .spyOn(helpers, 'dispatchControlTabChangeEvent')
    .mockImplementationOnce(jest.fn);

  it('data not change', () => {
    renderWrapper(mockInitialState);

    emitChangingTabEvent();

    expect(mockDispatchControlTabChangeEvent).toBeCalled();
  });

  it('data was changed', () => {
    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });

    spy = jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);

    jest.useFakeTimers();

    renderWrapper(mockInitialState);

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_payByPhone_paymentAgreementMessageWithConvenienceFee_setDefaultButton_dls-button'
        )
      );
    });
    jest.runAllTimers();

    emitChangingTabEvent();

    expect(mockDispatchControlTabChangeEvent).toBeCalled();
    expect(openConfirmModalAction).toBeCalled();
  });
});

describe('trigger useListenClosingModal', () => {
  const mockDispatchCloseModalEvent = jest
    .spyOn(helpers, 'dispatchCloseModalEvent')
    .mockImplementationOnce(jest.fn);

  it('data not change', () => {
    renderWrapper(mockInitialState);

    emitClosingModalEvent();

    expect(mockDispatchCloseModalEvent).toBeCalled();
  });

  it('data was changed', () => {
    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });

    spy = jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);
    renderWrapper(mockInitialState);

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_payByPhone_paymentAgreementMessageWithConvenienceFee_setDefaultButton_dls-button'
        )
      );
    });
    jest.runAllTimers();

    emitClosingModalEvent();

    expect(mockDispatchCloseModalEvent).toBeCalled();
  });
});
