export interface PayByPhoneItem {
  maxDay?: number;
  paymentAgreementMessageWithConvenienceFee: string;
  paymentAgreementMessageWithoutConvenienceFee: string;
}
export interface PayByPhoneErrors {
  maxDay?: string;
  paymentAgreementMessageWithConvenienceFee?: string;
  paymentAgreementMessageWithoutConvenienceFee?: string;
}
export interface ClientConfigPayByPhoneState {
  previousValue?: PayByPhoneItem;
  currentValue?: PayByPhoneItem;
  loading: boolean;
}

export interface GetPayByPhonePayload extends PayByPhoneItem {}

export interface GetPayByPhoneArgs {
  storeId?: string;
}

export interface PayByPhoneArgs extends PayByPhoneItem {}

export type ErrorControlValidation = {
  status?: boolean;
  message?: string;
};
