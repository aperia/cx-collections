import { apiService } from 'app/utils/api.service';

export const clientConfigPaybyPhoneService = {
  getPayByPhoneConfig() {
    const url =
      window.appConfig?.api?.clientConfigPayByPhone?.getPayByPhoneConfig || '';
    return apiService.post(url, {});
  },
  submitPaybyPhone(postData: { value?: number }) {
    const url =
      window.appConfig?.api?.clientConfigPayByPhone?.submitPayByPhone || '';
    return apiService.post(url, { postData });
  },
  getPayByPhoneData(requestBody: {
    common: Record<string, string>;
    selectConfig: string;
    selectComponent: string;
  }) {
    const url = window.appConfig?.api?.account?.getExternalConfig || '';
    return apiService.post(url, requestBody);
  }
};
