import React from 'react';
import { screen } from '@testing-library/react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';

import ClientConfigPromiseToPay from './index';
import {
  clientConfigPromiseToPayActions,
  initialState
} from './_redux/reducers';

import * as clientConfigHelper from 'pages/ClientConfiguration/helpers';

import {
  CLIENT_CONFIG_PROMISE_TO_PAY_VIEW,
  DEFAULT_VIEW_VALUES,
  I18N_CLIENT_CONFIG_PROMISE_TO_PAY
} from './constants';
import { SECTION_TAB_EVENT } from '../constants';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import * as mockStatus from '../hooks/useCheckClientConfigStatus';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const storeInitWithNoDataChange = {
  initialState: {
    promiseToPayConfig: initialState,
    form: {
      [CLIENT_CONFIG_PROMISE_TO_PAY_VIEW]: {
        values: DEFAULT_VIEW_VALUES
      } as never
    }
  }
};

const storeInitWithDataChange = {
  initialState: {
    promiseToPayConfig: initialState,
    form: {
      [CLIENT_CONFIG_PROMISE_TO_PAY_VIEW]: {
        values: {
          ...DEFAULT_VIEW_VALUES,
          earliestPromiseInDaysFromToday: {
            enable: true,
            rawValue: 'text'
          }
        }
      } as never
    }
  }
};

describe('Test component ClientConfigPromiseToPay', () => {
  const renderComponent = () => {
    return renderMockStore(
      <ClientConfigPromiseToPay />,
      storeInitWithNoDataChange
    );
  };

  beforeEach(() => {
    jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(false);
  });

  describe('render', () => {
    beforeEach(() => {
      jest
        .spyOn(mockStatus, 'useCheckClientConfigStatus')
        .mockReturnValue(false);
    });

    it('should render correctly', () => {
      jest
        .spyOn(mockStatus, 'useCheckClientConfigStatus')
        .mockReturnValue(true);
      jest.useFakeTimers();
      renderComponent();
      jest.runAllTimers();
      expect(
        screen.getByTestId(CLIENT_CONFIG_PROMISE_TO_PAY_VIEW)
      ).toBeInTheDocument();
    });

    it('should render select all', () => {
      jest.useFakeTimers();
      renderComponent();
      jest.runAllTimers();
      const selectAllBtn = screen.getByText(
        I18N_CLIENT_CONFIG_PROMISE_TO_PAY.SELECT_ALL
      );
      selectAllBtn.click();
      expect(
        screen.getByText(I18N_CLIENT_CONFIG_PROMISE_TO_PAY.DESELECT_ALL)
      ).toBeInTheDocument();
    });
  });

  describe('handle actions ', () => {
    const spyAction = mockActionCreator(clientConfigPromiseToPayActions);
    describe('handle reset to previousConfig', () => {
      it('not call actions', () => {
        const spyReset = spyAction('resetToPrevious');
        renderMockStore(
          <ClientConfigPromiseToPay />,
          storeInitWithNoDataChange
        );
        const resetBtn = screen.getByText(
          I18N_CLIENT_CONFIG_PROMISE_TO_PAY.RESET_TO_PREVIOUS
        );
        resetBtn.click();
        expect(spyReset).toHaveBeenCalledTimes(0);
      });

      it('call dispatch', () => {
        const spyReset = spyAction('resetToPrevious');
        renderMockStore(<ClientConfigPromiseToPay />, {
          initialState: {
            promiseToPayConfig: initialState,
            form: {
              [CLIENT_CONFIG_PROMISE_TO_PAY_VIEW]: {
                values: undefined
              } as never
            }
          }
        });
        const resetBtn = screen.getByText(
          I18N_CLIENT_CONFIG_PROMISE_TO_PAY.RESET_TO_PREVIOUS
        );
        resetBtn.click();
        expect(spyReset).toHaveBeenCalled();
      });
    });

    it('handle save', () => {
      const spyTrigger = spyAction('triggerSubmitPromiseToPayConfig');
      renderMockStore(<ClientConfigPromiseToPay />, storeInitWithDataChange);
      const submitBtn = screen.getByText(
        I18N_CLIENT_CONFIG_PROMISE_TO_PAY.SAVE_CHANGES
      );
      submitBtn.click();
      expect(spyTrigger).toHaveBeenCalledWith();
    });

    it('handle select all', () => {
      const spyAction = mockActionCreator(clientConfigPromiseToPayActions);
      const selectAllAction = spyAction('setCurrentValue');
      renderMockStore(<ClientConfigPromiseToPay />, {
        initialState: {
          promiseToPayConfig: initialState,
          form: {
            [CLIENT_CONFIG_PROMISE_TO_PAY_VIEW]: {
              values: {
                ...DEFAULT_VIEW_VALUES,
                earliestPromiseInDaysFromToday: undefined
              }
            }
          } as never
        }
      });
      const selectAllBtn = screen.getByText(
        I18N_CLIENT_CONFIG_PROMISE_TO_PAY.SELECT_ALL
      );
      selectAllBtn.click();
      const deselectAllBtn = screen.getByText(
        I18N_CLIENT_CONFIG_PROMISE_TO_PAY.DESELECT_ALL
      );
      deselectAllBtn.click();
      expect(selectAllAction).toBeCalledTimes(2);
    });

    it('handle deselect all', () => {
      jest.useFakeTimers();
      const spyAction = mockActionCreator(clientConfigPromiseToPayActions);
      const selectAllAction = spyAction('setCurrentValue');
      renderMockStore(<ClientConfigPromiseToPay />, {
        initialState: {
          promiseToPayConfig: initialState,
          form: {
            [CLIENT_CONFIG_PROMISE_TO_PAY_VIEW]: {
              values: {
                earliestPromiseInDaysFromToday: { enable: true, rawValue: '23' }
              }
            }
          } as never
        }
      });
      const deselectAllBtn = screen.getByText(
        I18N_CLIENT_CONFIG_PROMISE_TO_PAY.DESELECT_ALL
      );
      deselectAllBtn.click();
      expect(selectAllAction).toBeCalledTimes(1);
    });
  });
});

describe('useListenChangingTabEvent', () => {
  it('data not change', () => {
    const mockDispatchEventChangeTab = jest.fn();
    const spy = jest
      .spyOn(clientConfigHelper, 'dispatchControlTabChangeEvent')
      .mockImplementation(mockDispatchEventChangeTab);
    renderMockStore(<ClientConfigPromiseToPay />, storeInitWithNoDataChange);
    const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGING_TAB, {
      bubbles: true,
      cancelable: true
    });

    window.dispatchEvent(event);
    expect(mockDispatchEventChangeTab).toBeCalled();
    spy.mockReset();
    spy.mockRestore();
  });

  it('data has been change', () => {
    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });
    const spy = jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);

    renderMockStore(<ClientConfigPromiseToPay />, storeInitWithDataChange);
    const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGING_TAB, {
      bubbles: true,
      cancelable: true
    });

    window.dispatchEvent(event);
    expect(openConfirmModalAction).toBeCalledWith(
      expect.objectContaining({
        title: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.UNSAVED_CHANGE,
        body: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.DISCARD_CHANGE_BODY,
        cancelText: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.CONTINUE_EDITING,
        confirmText: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.DISCARD_CHANGE,
        variant: 'danger'
      })
    );
    spy.mockRestore();
    spy.mockReset();
  });
});

describe('useListenClosing', () => {
  it('data not change', () => {
    const mockDispatchEventCloseModal = jest.fn();
    const spy = jest
      .spyOn(clientConfigHelper, 'dispatchCloseModalEvent')
      .mockImplementation(mockDispatchEventCloseModal);
    renderMockStore(<ClientConfigPromiseToPay />, storeInitWithNoDataChange);
    const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
      bubbles: true,
      cancelable: true
    });

    window.dispatchEvent(event);
    expect(mockDispatchEventCloseModal).toBeCalled();
    spy.mockReset();
    spy.mockRestore();
  });

  it('data has been change', () => {
    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });
    const spy = jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);
    renderMockStore(<ClientConfigPromiseToPay />, storeInitWithDataChange);
    const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
      bubbles: true,
      cancelable: true
    });

    window.dispatchEvent(event);
    expect(openConfirmModalAction).toBeCalledWith(
      expect.objectContaining({
        title: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.UNSAVED_CHANGE,
        body: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.DISCARD_CHANGE_BODY,
        cancelText: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.CONTINUE_EDITING,
        confirmText: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.DISCARD_CHANGE,
        variant: 'danger'
      })
    );
    spy.mockRestore();
    spy.mockReset();
  });
});
