import { MappingState } from './../../../__commons/MappingProvider/types';
import { getPromiseToPayConfiguration } from './getPromiseToPayConfig';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { mockActionCreator } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';

const spyClientConfigurationActions = mockActionCreator(
  clientConfigurationActions
);

const initialState = {
  loading: false,
  data: {
    promiseToPayConfig: { text: 'text' },
    promiseToPayConfigItem: { text: 'text' }
  }
} as MappingState;

const mockData = {
  earliestPromiseInDaysFromToday: { enable: false, rawValue: '' },
  latestPromiseInDaysFromToday: { enable: false, rawValue: '' },
  latestFirstPromiseInDaysFromToday: { enable: false, rawValue: '' },
  minAmountPerPromise: { enable: false, rawValue: '' },
  maxAmountPerPromise: { enable: false, rawValue: '' },
  minSumTotalPromiseAmount: { enable: false, rawValue: '' },
  maxSumTotalPromiseAmount: { enable: false, rawValue: '' },
  minDaysBetweenPromise: { enable: false, rawValue: '' },
  maxDaysBetweenPromise: { enable: false, rawValue: '' },
  minNumberPromise: { enable: false, rawValue: '' },
  maxNumberPromise: { enable: false, rawValue: '' }
};

describe('getPromiseToPayConfiguration', () => {
  const store = createStore(rootReducer, {
    mapping: initialState
  });

  const storeEmpty = createStore(rootReducer, {
    mapping: {
      data: {}
    } as MappingState
  });

  it('should call successful action', async () => {
    const getCollectionsClientConfig = spyClientConfigurationActions(
      'getClientInfoData'
    ).mockReturnValue({
      payload: { promiseConfigList: [{ name: 'name' }] },
      type: 'fulfilled',
      meta: { requestId: 'id', requestStatus: 'fulfilled' }
    });

    const response = await getPromiseToPayConfiguration()(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'clientConfigPromiseToPay/getPromiseToPayConfig/fulfilled'
    );
    expect(response.payload).toEqual(mockData);
    expect(getCollectionsClientConfig).toHaveBeenCalled();
  });

  it('should call successful action, empty data', async () => {
    const getCollectionsClientConfig = spyClientConfigurationActions(
      'getClientInfoData'
    ).mockReturnValue({
      payload: { promiseConfigList: [{ name: 'name' }] },
      type: 'fulfilled',
      meta: { requestId: 'id', requestStatus: 'fulfilled' }
    });

    const response = await getPromiseToPayConfiguration()(
      storeEmpty.dispatch,
      storeEmpty.getState,
      {}
    );

    expect(response.type).toEqual(
      'clientConfigPromiseToPay/getPromiseToPayConfig/fulfilled'
    );
    expect(response.payload).toEqual(mockData);
    expect(getCollectionsClientConfig).toHaveBeenCalled();
  });

  it('should call successful action, empty data > case 2', async () => {
    const getCollectionsClientConfig = spyClientConfigurationActions(
      'getClientInfoData'
    ).mockReturnValue({
      payload: { promiseConfigList: undefined },
      type: 'fulfilled',
      meta: { requestId: 'id', requestStatus: 'fulfilled' }
    });

    const response = await getPromiseToPayConfiguration()(
      storeEmpty.dispatch,
      storeEmpty.getState,
      {}
    );

    expect(response.type).toEqual(
      'clientConfigPromiseToPay/getPromiseToPayConfig/fulfilled'
    );
    expect(response.payload).toEqual(mockData);
    expect(getCollectionsClientConfig).toHaveBeenCalled();
  });

  it('should call rejected action', async () => {
    const getCollectionsClientConfig = spyClientConfigurationActions(
      'getClientInfoData'
    ).mockReturnValue({
      payload: {},
      type: 'rejected',
      meta: { requestId: 'id', requestStatus: 'rejected' }
    });

    const response = await getPromiseToPayConfiguration()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'clientConfigPromiseToPay/getPromiseToPayConfig/rejected'
    );
    expect(response.payload).toEqual({});
    expect(getCollectionsClientConfig).toHaveBeenCalled();
  });
});
