import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected,
  PayloadAction
} from '@reduxjs/toolkit';
import { cleanObj, mappingDataFromObj } from 'app/helpers';
import { saveChangedClientConfig } from 'pages/ClientConfiguration/ChangeHistory/_redux/saveChangedClientConfig';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';

import {
  CLIENT_CONFIG_PROMISE_TO_PAY_VIEW,
  I18N_CLIENT_CONFIG_PROMISE_TO_PAY
} from '../constants';
import { ClientConfigPromiseToPayState, PromiseToPayConfig } from '../types';

export const submitPromiseToPayConfigRequest = createAsyncThunk<
  PromiseToPayConfig,
  undefined,
  ThunkAPIConfig
>(
  'clientConfigPromiseToPay/submitPromiseToPayConfigRequest',
  async (args, thunkAPI) => {
    const { dispatch, getState } = thunkAPI;
    const { form, mapping } = getState();

    const configValues = form[CLIENT_CONFIG_PROMISE_TO_PAY_VIEW]
      .values! as PromiseToPayConfig;

    /*
     * Example:
     * mapping from:
     * {"minAmountPerPromise": {enable: true, rawValue: "32"}}
     * to:
     * {"Min Amount per Promise": {enable: true, rawValue: "32"}}
     **/
    const configValuesMapped = mappingDataFromObj(
      configValues,
      mapping.data.promiseToPayConfig ?? {},
      true
    );
    // clean undefined entries
    const configValuesMappedClean = cleanObj(configValuesMapped);
    /*
     * Example:
     * mapping from:
     * {"Min Amount per Promise": {enable: true, rawValue: "32"}}
     * to:
     * [{name: "Min Amount per Promise", active: true, value: "32"}]
     **/
    const promiseConfigList = Object.entries(
      configValuesMappedClean as PromiseToPayConfig
    ).map(([k, v]) => ({
      name: k,
      value: `${v.rawValue}`,
      active: v.enable
    }));

    const response = await dispatch(
      clientConfigurationActions.updateClientInfo({
        promiseConfigList,
        updateAction: 'delete'
      })
    );
    if (isFulfilled(response)) return configValues;

    return thunkAPI.rejectWithValue({});
  }
);

export const triggerSubmitPromiseToPayConfig = createAsyncThunk<
  unknown,
  undefined,
  ThunkAPIConfig
>(
  'clientConfigPromiseToPay/triggerSubmitPromiseToPayConfig',
  async (args, thunkAPI) => {
    const { dispatch, getState } = thunkAPI;
    const { promiseToPayConfig, form } = getState();
    const response = await dispatch(submitPromiseToPayConfigRequest());
    const { previousConfig } = promiseToPayConfig;

    if (isFulfilled(response)) {
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.SAVE_CHANGE_SUCCESS
          })
        );
        dispatch(
          saveChangedClientConfig({
            action: 'UPDATE',
            changedCategory: 'promiseToPay',
            oldValue: previousConfig,
            newValue: form[CLIENT_CONFIG_PROMISE_TO_PAY_VIEW]?.values
          })
        );
      });
    }

    if (isRejected(response)) {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.SAVE_CHANGE_FAILURE
        })
      );
    }
  }
);

export const triggerSubmitPromiseToPayConfigBuilder = (
  builder: ActionReducerMapBuilder<ClientConfigPromiseToPayState>
) => {
  builder
    .addCase(submitPromiseToPayConfigRequest.pending, (draftState, action) => {
      draftState.loading = true;
    })
    .addCase(
      submitPromiseToPayConfigRequest.fulfilled,
      (draftState, action: PayloadAction<PromiseToPayConfig>) => {
        draftState.loading = false;
        draftState.previousConfig = action.payload;
        draftState.currentConfig = action.payload;
      }
    )
    .addCase(submitPromiseToPayConfigRequest.rejected, (draftState, action) => {
      draftState.loading = false;
    });
};
