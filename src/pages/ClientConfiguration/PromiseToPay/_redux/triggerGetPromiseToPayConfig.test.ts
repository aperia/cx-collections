import { createStore, Store } from '@reduxjs/toolkit';
import { byPassFulfilled, byPassRejected, storeId } from 'app/test-utils';
import { clientConfigurationServices } from 'pages/ClientConfiguration/clientConfigurationServices';
import { rootReducer } from 'storeConfig';
import './reducers';
import { triggerGetPromiseToPayConfig } from './triggerGetPromiseToPayConfig';

let store: Store<RootState>;

let spy: jest.SpyInstance;

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    user: 'user',
    org: 'org',
    app: 'app'
  }
};

describe('Test triggerGetPromiseToPayConfig async thunk', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('FulFilled', async () => {
    jest
      .spyOn(clientConfigurationServices, 'clientInfoGetList')
      .mockReturnValue({});
    store = createStore(rootReducer, {
      accountDetail: {
        [storeId]: {
          data: {
            agentId: '1234',
            clientID: '4567',
            sysId: '7890',
            principalId: '1234'
          }
        }
      }
    } as any);
    const res = await triggerGetPromiseToPayConfig({ storeId })(
      byPassFulfilled(
        'clientConfiguration/triggerUpdateSelectedConfig/fulfilled',
        {
          data: {}
        }
      ),
      store.getState,
      {}
    );
    expect(res.type).toEqual(
      'clientConfiguration/triggerGetPromiseToPayConfig/fulfilled'
    );
  });

  it('Rejected > case 1', async () => {
    jest
      .spyOn(clientConfigurationServices, 'clientInfoGetList')
      .mockReturnValue({});
    store = createStore(rootReducer, {
      accountDetail: {
        [storeId]: {
          data: {
            agentId: '1234',
            clientID: '4567',
            sysId: '7890',
            principalId: '1234'
          }
        }
      }
    } as any);
    const res = await triggerGetPromiseToPayConfig({ storeId } as never)(
      byPassRejected('clientConfiguration/getAllClientConfigListApi/rejected', {
        data: undefined
      }),
      store.getState,
      {}
    );
    expect(res.type).toEqual(
      'clientConfiguration/triggerGetPromiseToPayConfig/fulfilled'
    );
  });

  it('Rejected > case 2', async () => {
    jest
      .spyOn(clientConfigurationServices, 'clientInfoGetList')
      .mockRejectedValue({});

    store = createStore(rootReducer, {
      accountDetail: {
        [storeId]: {
          data: {
            agentId: undefined,
            clientID: undefined,
            sysId: undefined,
            principalId: undefined
          }
        }
      }
    } as any);
    const res = await triggerGetPromiseToPayConfig({ storeId } as never)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(res.type).toEqual(
      'clientConfiguration/triggerGetPromiseToPayConfig/rejected'
    );
  });
});
