import { selectorWrapper } from 'app/test-utils';
import {
  CLIENT_CONFIG_PROMISE_TO_PAY_VIEW,
  DEFAULT_VIEW_VALUES
} from '../constants';
import { initialState } from './reducers';
import {
  selectCurrentConfig,
  selectPreviousConfig,
  selectDataChange,
  selectLoading
} from './selectors';

describe('Test Client Configuration Authentication Selectors', () => {
  const store: Partial<RootState> = {
    promiseToPayConfig: initialState
  };

  it('selectCurrentConfig', () => {
    const { data } = selectorWrapper(store)(selectCurrentConfig);

    expect(data).toEqual(initialState.currentConfig);
  });

  it('selectPreviousConfig', () => {
    const { data } = selectorWrapper(store)(selectPreviousConfig);

    expect(data).toEqual(initialState.previousConfig);
  });

  it('selectDataChange', () => {
    const { data } = selectorWrapper({
      promiseToPayConfig: initialState,
      form: {
        [CLIENT_CONFIG_PROMISE_TO_PAY_VIEW]: {
          values: DEFAULT_VIEW_VALUES
        } as never
      }
    })(selectDataChange);

    expect(data).toEqual(false);
  });

  it('selectLoading', () => {
    const { data } = selectorWrapper(store)(selectLoading);

    expect(data).toEqual(false);
  });
});
