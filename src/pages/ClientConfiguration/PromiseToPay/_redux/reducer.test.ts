// types
import { DEFAULT_VIEW_VALUES } from '../constants';
import { ClientConfigPromiseToPayState } from '../types';

//reducers
import { clientConfigPromiseToPayActions, reducer } from './reducers';

describe('Test Client Configuration Code To Text Reducers', () => {
  const mockDirtyValue = {
    ...DEFAULT_VIEW_VALUES,
    earliestPromiseInDaysFromToday: { enable: true, rawValue: 'new value' }
  };

  const initialState: ClientConfigPromiseToPayState = {
    previousConfig: DEFAULT_VIEW_VALUES,
    currentConfig: mockDirtyValue,
    isSelectAll: false,
    loading: false
  };

  it('should reset to previous ', () => {
    const state = reducer(
      initialState,
      clientConfigPromiseToPayActions.resetToPrevious()
    );
    expect(state.currentConfig).toEqual(state.previousConfig);
  });

  it('should set current value ', () => {
    const state = reducer(
      initialState,
      clientConfigPromiseToPayActions.setCurrentValue(DEFAULT_VIEW_VALUES)
    );
    expect(state.currentConfig).toEqual(DEFAULT_VIEW_VALUES);
  });
});
