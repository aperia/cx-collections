import { createAsyncThunk, isFulfilled } from '@reduxjs/toolkit';
import { AccountInfo } from 'pages/AccountDetails/types';

import { clientConfigurationActions } from '../../_redux/reducers';
import { clientConfigPromiseToPayActions } from './reducers';

export const triggerGetPromiseToPayConfig = createAsyncThunk<
  void,
  { storeId: string },
  ThunkAPIConfig
>(
  'clientConfiguration/triggerGetPromiseToPayConfig',
  async ({ storeId }, { rejectWithValue, dispatch, getState }) => {
    
    try {
      const {
        agentId = '',
        clientID = '',
        sysId = '',
        principalId = ''
      } = getState().accountDetail?.[storeId]?.data as AccountInfo;
      
      const response = await dispatch(
        clientConfigurationActions.triggerUpdateSelectedConfiguration({
          clientId: clientID,
          systemId: sysId,
          principleId: principalId,
          agentId
        })
      );
      if (isFulfilled(response)) {
        dispatch(
          clientConfigPromiseToPayActions.getPromiseToPayConfiguration()
        );
      }
    } catch (error) {
      return rejectWithValue({ response: error });
    }
  }
);
