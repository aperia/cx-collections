import { byPassFulfilled, byPassRejected } from 'app/test-utils';
import { MappingState } from './../../../__commons/MappingProvider/types';
import {
  submitPromiseToPayConfigRequest,
  triggerSubmitPromiseToPayConfig
} from './submitPromiseToPayConfig';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { mockActionCreator } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';

const spyClientConfigurationActions = mockActionCreator(
  clientConfigurationActions
);

const initialState = {
  loading: false,
  data: {
    promiseToPayConfig: { text: '32', value: 'value' },
    promiseToPayConfigItem: { text: '32', value: 'value' }
  }
} as MappingState;

const initialStateForm = {
  clientConfigPromiseToPay: {
    values: {
      text: '32',
      value: 'value'
    }
  }
};

describe('submitPromiseToPayConfigRequest', () => {
  const store = createStore(rootReducer, {
    mapping: initialState,
    form: initialStateForm
  });

  const storeEmpty = createStore(rootReducer, {
    mapping: { data: {} },
    form: initialStateForm
  });

  it('should call successful action', async () => {
    const getCollectionsClientConfig = spyClientConfigurationActions(
      'updateClientInfo'
    ).mockReturnValue({
      payload: {
        promiseConfigList: [],
        updateAction: 'delete'
      },
      type: 'fulfilled',
      meta: { requestId: 'id', requestStatus: 'fulfilled' }
    });

    const response = await submitPromiseToPayConfigRequest()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'clientConfigPromiseToPay/submitPromiseToPayConfigRequest/fulfilled'
    );
    expect(response.payload).toEqual({ value: 'value', text: '32' });
    expect(getCollectionsClientConfig).toHaveBeenCalled();
  });

  it('should call successful action, empty data', async () => {
    const getCollectionsClientConfig =
      spyClientConfigurationActions('updateClientInfo');

    const response = await submitPromiseToPayConfigRequest()(
      storeEmpty.dispatch,
      storeEmpty.getState,
      {}
    );

    expect(response.type).toEqual(
      'clientConfigPromiseToPay/submitPromiseToPayConfigRequest/rejected'
    );
    expect(response.payload).toEqual({});
    expect(getCollectionsClientConfig).toBeTruthy();
  });

  it('should call rejected action', async () => {
    const getCollectionsClientConfig = spyClientConfigurationActions(
      'updateClientInfo'
    ).mockReturnValue({
      payload: {},
      type: 'rejected',
      meta: { requestId: 'id', requestStatus: 'rejected' }
    });

    const response = await submitPromiseToPayConfigRequest()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'clientConfigPromiseToPay/submitPromiseToPayConfigRequest/rejected'
    );
    expect(response.payload).toEqual({});
    expect(getCollectionsClientConfig).toHaveBeenCalled();
  });
});

describe('triggerSubmitPromiseToPayConfig', () => {
  const store = createStore(rootReducer, {
    mapping: initialState,
    form: initialStateForm
  });

  it('should call successful action', async () => {
    const response = await triggerSubmitPromiseToPayConfig()(
      byPassFulfilled(triggerSubmitPromiseToPayConfig.fulfilled.type),
      store.getState,
      {}
    );
    
    expect(response.type).toEqual(
      triggerSubmitPromiseToPayConfig.fulfilled.type
    );
  });

  it('should call rejected action', async () => {
    const response = await triggerSubmitPromiseToPayConfig()(
      byPassRejected(triggerSubmitPromiseToPayConfig.rejected.type),
      store.getState,
      {}
    );
   
    expect(response.payload).toBeUndefined()
  });
});
