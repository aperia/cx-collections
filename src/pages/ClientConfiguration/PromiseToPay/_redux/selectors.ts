import { createSelector } from '@reduxjs/toolkit';
import { isEqual } from 'app/_libraries/_dls/lodash';
import { CLIENT_CONFIG_PROMISE_TO_PAY_VIEW } from '../constants';

const getCurrentConfig = (state: RootState) => {
  return state.promiseToPayConfig.currentConfig;
};

const getPreviousConfig = (state: RootState) => {
  return state.promiseToPayConfig.previousConfig;
};

const getDataChange = (state: RootState) => {
  const { form, promiseToPayConfig } = state;
  const currentConfig = form[CLIENT_CONFIG_PROMISE_TO_PAY_VIEW]?.values;
  const { previousConfig } = promiseToPayConfig;
  return !isEqual(currentConfig, previousConfig);
};

const getLoading = (state: RootState) => {
  return state.promiseToPayConfig.loading;
};

export const selectCurrentConfig = createSelector(
  getCurrentConfig,
  data => data
);

export const selectPreviousConfig = createSelector(
  getPreviousConfig,
  data => data
);

export const selectDataChange = createSelector(getDataChange, data => data);

export const selectLoading = createSelector(getLoading, data => data);
