import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';
import { cleanObj, mappingDataFromObj } from 'app/helpers';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { DEFAULT_VIEW_VALUES } from '../constants';
import { ClientConfigPromiseToPayState, PromiseToPayConfig } from '../types';

export const getPromiseToPayConfiguration = createAsyncThunk<
  PromiseToPayConfig,
  undefined,
  ThunkAPIConfig
>('clientConfigPromiseToPay/getPromiseToPayConfig', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;
  const { mapping } = getState();
  const response = await dispatch(
    clientConfigurationActions.getClientInfoData({
      dataType: 'promiseConfigList'
    })
  );
  if (isFulfilled(response)) {
    const result: Record<string, { enable: boolean; rawValue: string }> = {};
    const { promiseConfigList = [] } = response.payload;
    promiseConfigList?.forEach(config => {
      result[config.name] = mappingDataFromObj(
        config,
        mapping.data.promiseToPayConfigItem ?? {}
      );
    });
    const mappedData = mappingDataFromObj(
      result,
      mapping.data.promiseToPayConfig ?? {}
    );
    const mappedDataCleaned = cleanObj(mappedData);
    return Object.assign({}, DEFAULT_VIEW_VALUES, mappedDataCleaned);
  }
  return thunkAPI.rejectWithValue({});
});

export const getPromiseToPayConfigurationBuilder = (
  builder: ActionReducerMapBuilder<ClientConfigPromiseToPayState>
) => {
  builder
    .addCase(getPromiseToPayConfiguration.pending, draftState => {
      draftState.loading = true;
    })
    .addCase(getPromiseToPayConfiguration.fulfilled, (draftState, action) => {
      draftState.previousConfig = action.payload;
      draftState.currentConfig = action.payload;
      draftState.loading = false;
    })
    .addCase(getPromiseToPayConfiguration.rejected, (draftState, action) => {
      draftState.loading = false;
    });
};
