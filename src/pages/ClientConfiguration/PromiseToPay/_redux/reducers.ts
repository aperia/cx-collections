import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { DEFAULT_VIEW_VALUES } from '../constants';
import { ClientConfigPromiseToPayState, PromiseToPayConfig } from '../types';
import {
  getPromiseToPayConfiguration,
  getPromiseToPayConfigurationBuilder
} from './getPromiseToPayConfig';
import {
  triggerSubmitPromiseToPayConfig,
  triggerSubmitPromiseToPayConfigBuilder
} from './submitPromiseToPayConfig';
import {
  triggerGetPromiseToPayConfig
} from './triggerGetPromiseToPayConfig';

export const initialState: ClientConfigPromiseToPayState = {
  previousConfig: DEFAULT_VIEW_VALUES,
  currentConfig: DEFAULT_VIEW_VALUES,
  isSelectAll: false,
  loading: false
};

const { actions, reducer } = createSlice({
  name: 'clientConfigPromiseToPay',
  initialState,
  reducers: {
    resetToPrevious: draftState => {
      draftState.currentConfig = { ...draftState.previousConfig };
      // DON'T refactor to draftState.currentConfig = draftState.previousConfig
      // it need to create new object to reset value of DOF form
    },
    setCurrentValue: (
      draftState,
      action: PayloadAction<PromiseToPayConfig>
    ) => {
      draftState.currentConfig = action.payload;
    }
  },
  extraReducers: builder => {
    triggerSubmitPromiseToPayConfigBuilder(builder);
    getPromiseToPayConfigurationBuilder(builder);
  }
});

const allActions = {
  ...actions,
  triggerSubmitPromiseToPayConfig,
  getPromiseToPayConfiguration,
  triggerGetPromiseToPayConfig
};

export { allActions as clientConfigPromiseToPayActions, reducer };
