export interface PromiseToPayItem {
  enable: boolean;
  rawValue: string;
}

export type PromiseToPayConfig = Record<string, PromiseToPayItem>;

export interface ClientConfigPromiseToPayState {
  previousConfig: PromiseToPayConfig;
  currentConfig: PromiseToPayConfig;
  loading: boolean;
  isSelectAll: boolean;
}
