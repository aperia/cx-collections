import React, { useEffect, useState } from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';
import { isInvalid } from 'redux-form';
import cloneDeep from 'lodash.clonedeep';

// redux
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { selectCurrentConfig, selectDataChange } from './_redux/selectors';
import { clientConfigPromiseToPayActions } from './_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// components
import { Button, SimpleBar } from 'app/_libraries/_dls/components';
import { View } from 'app/_libraries/_dof/core';
import ApiErrorForHeaderSection from '../ApiErrorSection/ApiErrorForHeaderSection';

// hooks
import { useSelectFormValue } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useListenChangingTabEvent } from '../hooks/useListenChangingTabEvent';
import { useListenClosingModal } from '../hooks/useListenClosingModal';

// constants & types & helpers
import { AppState } from 'storeConfig';
import {
  CLIENT_CONFIG_PROMISE_TO_PAY_VIEW,
  DEFAULT_VIEW_VALUES,
  I18N_CLIENT_CONFIG_PROMISE_TO_PAY
} from './constants';
import { PromiseToPayConfig } from './types';
import {
  dispatchCloseModalEvent,
  dispatchControlTabChangeEvent
} from '../helpers';
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';
import { handleWithRefOnFind } from 'app/helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';

const ClientConfigPromiseToPay = () => {
  const dispatch = useDispatch();

  const { t } = useTranslation();

  const isDataChange = useSelector(selectDataChange);

  const formValue = useSelectFormValue(CLIENT_CONFIG_PROMISE_TO_PAY_VIEW);

  const initialViewValues = useSelector(selectCurrentConfig);

  const isFormInValid = useSelector<AppState, boolean>(
    isInvalid(CLIENT_CONFIG_PROMISE_TO_PAY_VIEW)
  );

  const testId = 'clientConfig_promiseToPay';

  const [selectedAll, setSelectedAll] = useState(false);

  const status = useCheckClientConfigStatus();

  const promiseToPayFormRef = React.useRef<any>(null);

  const handleSelectAll = () => {
    const prevSelectAll = selectedAll;
    let newFormValues: PromiseToPayConfig = { ...DEFAULT_VIEW_VALUES };
    if (!prevSelectAll) {
      // enable all
      newFormValues = cloneDeep(formValue) as PromiseToPayConfig;
      Object.keys(DEFAULT_VIEW_VALUES).forEach(fieldId => {
        if (!newFormValues[fieldId]) return;
        newFormValues[fieldId].enable = !prevSelectAll;
      });
    }
    dispatch(
      clientConfigPromiseToPayActions.setCurrentValue(
        newFormValues as PromiseToPayConfig
      )
    );
    setSelectedAll(!prevSelectAll);
  };

  const handleSaveChanges = () => {
    dispatch(clientConfigPromiseToPayActions.triggerSubmitPromiseToPayConfig());
  };

  const handleResetToPrev = () => {
    if (!isDataChange) return;
    batch(() => {
      dispatch(clientConfigPromiseToPayActions.resetToPrevious());
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.RESET_TO_PREVIOUS_SUCCESS
        })
      );
    });
  };

  useListenChangingTabEvent(() => {
    if (!isDataChange) {
      dispatchControlTabChangeEvent();
      return;
    }
    dispatch(
      confirmModalActions.open({
        title: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.UNSAVED_CHANGE,
        body: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.DISCARD_CHANGE_BODY,
        cancelText: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.CONTINUE_EDITING,
        confirmText: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.DISCARD_CHANGE,
        confirmCallback: dispatchControlTabChangeEvent,
        variant: 'danger'
      })
    );
  });

  useListenClosingModal(() => {
    if (!isDataChange) {
      dispatchCloseModalEvent();
      return;
    }
    dispatch(
      confirmModalActions.open({
        title: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.UNSAVED_CHANGE,
        body: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.DISCARD_CHANGE_BODY,
        cancelText: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.CONTINUE_EDITING,
        confirmText: I18N_CLIENT_CONFIG_PROMISE_TO_PAY.DISCARD_CHANGE,
        confirmCallback: () => {
          dispatchCloseModalEvent();
        },
        variant: 'danger'
      })
    );
  });

  useEffect(() => {
    if (!formValue) return;
    const isAllChecked = Object.values(formValue).every(
      item => item?.enable === true
    );

    if (!isAllChecked) {
      setSelectedAll(false);
    } else {
      setSelectedAll(true);
    }
  }, [formValue]);

  useEffect(() => {
    const setImmediateInstance = setImmediate(() => {
      if (status && promiseToPayFormRef.current && formValue) {
        const promiseToPayFormKey = Object.keys(formValue);

        promiseToPayFormKey.forEach((item: string) => {
          const selectedDOF = handleWithRefOnFind(promiseToPayFormRef, item);
          selectedDOF?.props?.setRequired(false);
          selectedDOF?.props?.setOthers((prev: MagicKeyValue) => {
            selectedDOF.props?.setOthers({
              ...prev,
              checkboxProps: { readOnly: true }
            });
          });
        });
      }
    });
    return () => clearImmediate(setImmediateInstance);
  }, [status, formValue]);

  useEffect(() => {
    dispatch(clientConfigPromiseToPayActions.getPromiseToPayConfiguration());
  }, [dispatch]);

  return (
    <div className={'position-relative has-footer-button'}>
      <SimpleBar>
        <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
          <ApiErrorForHeaderSection
            className="mb-24"
            dataTestId={`${testId}_apiError`}
          />
          <h5 data-testid={genAmtId(testId, 'title', '')}>
            {t(I18N_CLIENT_CONFIG_PROMISE_TO_PAY.PROMISE_TO_PAY)}
          </h5>
          <p
            className="mt-16 fs-14 color-grey-d20"
            data-testid={genAmtId(testId, 'description', '')}
          >
            {t(
              I18N_CLIENT_CONFIG_PROMISE_TO_PAY.SELECT_AND_PROVIDE_INFO_TO_CREATE
            )}
          </p>
          <div className="ml-n8 my-12">
            <Button
              disabled={status}
              size="sm"
              variant="outline-primary"
              onClick={handleSelectAll}
              dataTestId={`${testId}_selectAll-deselectAll-btn`}
            >
              {t(
                selectedAll
                  ? I18N_CLIENT_CONFIG_PROMISE_TO_PAY.DESELECT_ALL
                  : I18N_CLIENT_CONFIG_PROMISE_TO_PAY.SELECT_ALL
              )}
            </Button>
          </div>
          <View
            formKey={CLIENT_CONFIG_PROMISE_TO_PAY_VIEW}
            descriptor={CLIENT_CONFIG_PROMISE_TO_PAY_VIEW}
            value={initialViewValues}
            ref={promiseToPayFormRef}
          />
        </div>
      </SimpleBar>
      <div className="group-button-footer d-flex justify-content-end">
        <Button
          disabled={status}
          onClick={handleResetToPrev}
          variant="secondary"
          dataTestId={`${testId}_resetToPreviousBtn`}
        >
          {t(I18N_CLIENT_CONFIG_PROMISE_TO_PAY.RESET_TO_PREVIOUS)}
        </Button>
        <Button
          onClick={handleSaveChanges}
          variant="primary"
          disabled={status || isFormInValid || !isDataChange}
          dataTestId={`${testId}_saveChangesBtn`}
        >
          {t(I18N_CLIENT_CONFIG_PROMISE_TO_PAY.SAVE_CHANGES)}
        </Button>
      </div>
    </div>
  );
};

export default ClientConfigPromiseToPay;
