export const I18N_CLIENT_CONFIG_PROMISE_TO_PAY = {
  PROMISE_TO_PAY: 'txt_promise_to_pay',
  SELECT_AND_PROVIDE_INFO_TO_CREATE:
    'txt_select_and_provide_information_to_create_client_configuration',
  SELECT_ALL: 'txt_select_all',
  DESELECT_ALL: 'txt_deselect_all',
  RESET_TO_PREVIOUS: 'txt_reset_to_previous',
  UNSAVED_CHANGE_BODY: 'txt_unsaved_change_body',
  DISCARD_CHANGE_BODY: 'txt_discard_change_body',
  UNSAVED_CHANGE: 'txt_unsaved_changes',
  CONTINUE_EDITING: 'txt_continue_editing',
  SAVE_AND_CONTINUE: 'txt_save_and_continue',
  DISCARD_CHANGE: 'txt_discard_change',
  SAVE_CHANGES: 'txt_save_changes',
  RESET_TO_PREVIOUS_SUCCESS: 'txt_promise_to_pay_reset_to_previous_success',
  SAVE_CHANGE_SUCCESS: 'txt_promise_to_pay_settings_updated',
  SAVE_CHANGE_FAILURE: 'txt_promise_to_pay_settings_failed_to_update'
};

export const CLIENT_CONFIG_PROMISE_TO_PAY_VIEW = 'clientConfigPromiseToPay';

export const DEFAULT_VIEW_VALUES = {
  earliestPromiseInDaysFromToday: { enable: false, rawValue: '' },
  latestPromiseInDaysFromToday: { enable: false, rawValue: '' },
  latestFirstPromiseInDaysFromToday: { enable: false, rawValue: '' },
  minAmountPerPromise: { enable: false, rawValue: '' },
  maxAmountPerPromise: { enable: false, rawValue: '' },
  minSumTotalPromiseAmount: { enable: false, rawValue: '' },
  maxSumTotalPromiseAmount: { enable: false, rawValue: '' },
  minDaysBetweenPromise: { enable: false, rawValue: '' },
  maxDaysBetweenPromise: { enable: false, rawValue: '' },
  minNumberPromise: { enable: false, rawValue: '' },
  maxNumberPromise: { enable: false, rawValue: '' }
};
