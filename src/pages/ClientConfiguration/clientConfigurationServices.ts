import { apiService } from 'app/utils/api.service';
import axios from 'axios';
import {
  ChangeStatusClientConfigurationRequest,
  ClientInfoByIdResponse,
  CreateClientConfigurationRequest,
  GetClientConfigurationRequest,
  GetClientInfoRequestBody,
  GetSecureInfoRequestBody,
  IGetAccessConfigArgs,
  IGetCollectionsClientConfigServiceArgs,
  ISaveCollectionsClientConfigServiceArgs,
  UpdateClientConfigurationRequest,
  UpdateClientInfoRequestBody,
  UpdateClientInfoResponse
} from './types';

export const clientConfigurationServices = {
  clientInfoGetList(requestBody: GetClientConfigurationRequest) {
    const url = window.appConfig?.api?.clientConfig?.clientInfoGetList || '';

    return apiService.post(url, requestBody);
  },
  getSectionList() {
    const url = window.appConfig?.api?.clientConfig?.getSectionList || '';
    return axios.get(url);
  },
  add(requestBody: CreateClientConfigurationRequest) {
    const url = window.appConfig?.api?.clientConfig?.add || '';
    return apiService.post(url, requestBody);
  },
  update(requestBody: UpdateClientConfigurationRequest) {
    const url = window.appConfig?.api?.clientConfig?.update || '';
    return apiService.post(url, requestBody);
  },
  addUpdateClientInfo(requestBody: UpdateClientInfoRequestBody) {
    const url = window.appConfig?.api?.clientConfig?.addUpdateClientInfo || '';
    return apiService.post<UpdateClientInfoResponse>(url, requestBody);
  },
  getClientInfoById(requestBody: GetClientInfoRequestBody) {
    const url = window.appConfig?.api?.clientConfig?.getClientInfoById || '';
    return apiService.post<ClientInfoByIdResponse>(url, requestBody);
  },
  getSecureInfoById(requestBody: GetSecureInfoRequestBody) {
    const url =
      window.appConfig?.api?.clientConfig?.getCollectionsClientConfig || '';
    return apiService.post(url, requestBody);
  },
  changeStatusClientConfig(
    requestBody: ChangeStatusClientConfigurationRequest
  ) {
    const url =
      window.appConfig?.api?.clientConfig?.changeStatusClientConfig || '';
    return apiService.post(url, requestBody);
  },
  getCollectionsClientConfig(
    requestBody: IGetCollectionsClientConfigServiceArgs
  ) {
    const url =
      window.appConfig.api?.clientConfig.getCollectionsClientConfig || '';

    return apiService.post(url, requestBody);
  },
  saveCollectionsClientConfig(
    requestBody: ISaveCollectionsClientConfigServiceArgs
  ) {
    const url =
      window.appConfig.api?.clientConfig.saveCollectionsClientConfig || '';

    return apiService.post(url, requestBody);
  },
  getAccessConfig(requestBody: IGetAccessConfigArgs) {
    const url = window.appConfig.api?.clientConfig.getAccessConfig || '';

    return apiService.post(url, requestBody);
  }
};
