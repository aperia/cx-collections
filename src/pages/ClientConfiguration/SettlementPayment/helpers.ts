import { SETTLEMENT_PAYMENT_MENTION_DATA } from './constants';

export const replaceMentionText = (message: string) => {
  for (const mentionItem of SETTLEMENT_PAYMENT_MENTION_DATA) {
    const mentionText = `{${mentionItem}}`;
    if (message.includes(mentionText)) {
      const regex = new RegExp(`\{${mentionItem}\}`, 'gi');
      message = message?.replace(regex, `<!${mentionItem}!>`);
    }
  }
  return message;
};

export const replaceSpecialValidChars = (message: string) => {
  if (
    message.includes('\\`') ||
    message.includes('\\~') ||
    message.includes('\\*') ||
    message.includes('\\#') ||
    message.includes('\\-') ||
    message.includes('\\[') ||
    message.includes('\\]')
  ) {
    message = message.replace(/\\/g, '');
  }

  return message;
};
