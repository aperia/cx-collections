// types
import { screen } from '@testing-library/dom';
import { fireEvent } from '@testing-library/react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import React from 'react';
import { act } from 'react-dom/test-utils';

// component
import SettlementPayment from '.';
import { CLIENT_CONFIG_STATUS_KEY, SECTION_TAB_EVENT } from '../constants';
import * as helpers from '../helpers';
import * as status from '../hooks/useCheckClientConfigStatus';
import {
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE,
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE,
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE,
  FORM_FIELDS,
  I18N_SETTLEMENT_PAYMENT
} from './constants';
import { clientConfigSettlementPaymentActions } from './_redux/reducers';

jest.mock('app/_libraries/_dls/components/TextEditor', () => {
  return {
    __esModule: true,
    ...jest.requireActual('app/_libraries/_dls/components/TextEditor'),
    ...jest.requireActual('app/test-utils/mocks/MockTextEditor')
  };
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockInitialState: Partial<RootState> = {
  clientConfiguration: {
    settings: {
      selectedConfig: {
        additionalFields: [
          {
            name: CLIENT_CONFIG_STATUS_KEY,
            active: true
          }
        ]
      }
    }
  },
  settlementPaymentConfig: {
    loading: false,
    currentValue: {
      agreementMessageWithConvenienceFee: 'test1',
      agreementMessageWithoutConvenienceFee: 'test2',
      agreementMessage: 'test3'
    },
    previousValue: {
      agreementMessageWithConvenienceFee: 'test10',
      agreementMessageWithoutConvenienceFee: 'test20',
      agreementMessage: 'test30'
    }
  }
};

const emitChangingTabEvent = () => {
  const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGING_TAB, {
    bubbles: true,
    cancelable: true
  });

  window.dispatchEvent(event);
};

const emitClosingModalEvent = () => {
  const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
    bubbles: true,
    cancelable: true
  });

  window.dispatchEvent(event);
};
let spy: jest.SpyInstance;
let spyAction: jest.SpyInstance;

const renderWrapper = (initialState: Partial<RootState>) =>
  renderMockStore(<SettlementPayment />, { initialState });

const testId = 'clientConfig_settlementPayment';

const settlementPaymentSpy = mockActionCreator(
  clientConfigSettlementPaymentActions
);
const triggerSaveChangeSpy = settlementPaymentSpy(
  'triggerSubmitSettlementPaymentConfig'
);

describe('SettlementPayment', () => {
  beforeEach(() => {
    jest.spyOn(status, 'useCheckClientConfigStatus').mockReturnValue(false);
  });

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();

    spyAction?.mockReset();
    spyAction?.mockRestore();
  });

  it('should render UI', () => {
    jest.useFakeTimers();

    renderWrapper(mockInitialState);

    jest.runAllTimers();

    expect(
      screen.getByText(I18N_SETTLEMENT_PAYMENT.SETTLEMENT_PAYMENT)
    ).toBeInTheDocument();
  });

  it('should click save button', () => {
    jest.useFakeTimers();

    renderWrapper(mockInitialState);

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_settlementPayment_agreementMessageWithConvenienceFee_setDefaultButton_dls-button'
        )
      );
    });
    jest.runAllTimers();

    fireEvent.click(
      screen.getByTestId(
        'clientConfig_settlementPayment_saveChangesBtn_dls-button'
      )
    );
    jest.runAllTimers();
    expect(triggerSaveChangeSpy).not.toBeCalled();
  });

  it('should click reset button', () => {
    renderWrapper(mockInitialState);

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_settlementPayment_agreementMessageWithConvenienceFee_setDefaultButton_dls-button'
        )
      );
    });
    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_settlementPayment_resetToPreviousBtn_dls-button'
        )
      );
    });
    jest.runAllTimers();
  });

  it('should click reset button > case 2', () => {
    renderWrapper(mockInitialState);

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_settlementPayment_resetToPreviousBtn_dls-button'
        )
      );
    });
    jest.runAllTimers();
  });
});

describe('payment agreement text editor event', () => {
  it('on blur editor agreement message with fee', () => {
    jest.useFakeTimers();

    renderWrapper(mockInitialState);

    jest.runAllTimers();

    const editor = screen.getByTestId(
      `${testId}_${FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITH_CONVENIENCE_FEE}_editor`
    );
    fireEvent.change(editor, { target: { value: 'message content %%%' } });
    fireEvent.click(editor);
    fireEvent.blur(editor);
    jest.runAllTimers();
  });

  it('on blur editor agreement message without fee', () => {
    jest.useFakeTimers();

    renderWrapper(mockInitialState);

    jest.runAllTimers();

    const editor = screen.getByTestId(
      `${testId}_${FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITHOUT_CONVENIENCE_FEE}_editor`
    );

    fireEvent.change(editor, { target: { value: 'message content' } });
    fireEvent.click(editor);
    fireEvent.blur(editor);
    jest.runAllTimers();
  });

  it('on blur editor agreement message', () => {
    jest.useFakeTimers();

    renderWrapper(mockInitialState);

    jest.runAllTimers();

    const editor = screen.getByTestId(
      `${testId}_${FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE}_editor`
    );
    fireEvent.change(editor, {
      target: { value: 'message content abc %";' }
    });
    fireEvent.click(editor);

    fireEvent.blur(editor);
    jest.runAllTimers();
  });

  // formatTextData empty

  it('on blur editor agreement message with fee when formatTextData empty', () => {
    jest.useFakeTimers();

    renderWrapper({
      ...mockInitialState,
      settlementPaymentConfig: {
        ...mockInitialState['settlementPaymentConfig'],
        currentValue: {
          agreementMessageWithConvenienceFee: '',
          agreementMessageWithoutConvenienceFee: 'test20',
          agreementMessage: 'test30'
        }
      }
    } as Partial<RootState>);

    jest.runAllTimers();

    const editor = screen.getByTestId(
      `${testId}_${FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITH_CONVENIENCE_FEE}_editor`
    );
    fireEvent.change(editor, { target: { value: 'message content %%%' } });
    fireEvent.click(editor);
    fireEvent.blur(editor);
    jest.runAllTimers();
  });

  it('on blur editor agreement message when formatTextData empty', () => {
    jest.useFakeTimers();

    renderWrapper({
      ...mockInitialState,
      settlementPaymentConfig: {
        ...mockInitialState['settlementPaymentConfig'],
        currentValue: {
          agreementMessageWithConvenienceFee: 'test10',
          agreementMessageWithoutConvenienceFee: 'test20',
          agreementMessage: ''
        }
      }
    } as Partial<RootState>);

    jest.runAllTimers();

    const editor = screen.getByTestId(
      `${testId}_${FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE}_editor`
    );
    fireEvent.change(editor, { target: { value: 'message content %%%' } });
    fireEvent.click(editor);
    fireEvent.blur(editor);
    jest.runAllTimers();
  });

  it('on blur editor agreement without fee message when formatTextData empty', () => {
    jest.useFakeTimers();

    renderWrapper({
      ...mockInitialState,
      settlementPaymentConfig: {
        ...mockInitialState['settlementPaymentConfig'],
        currentValue: {
          agreementMessageWithConvenienceFee: 'test10',
          agreementMessageWithoutConvenienceFee: '',
          agreementMessage: 'test30'
        }
      }
    } as Partial<RootState>);

    jest.runAllTimers();

    const editor = screen.getByTestId(
      `${testId}_${FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITHOUT_CONVENIENCE_FEE}_editor`
    );
    fireEvent.change(editor, { target: { value: 'message content %%%' } });
    fireEvent.click(editor);
    fireEvent.blur(editor);
    jest.runAllTimers();
  });
});

describe('Actions', () => {
  it('Set default values > case 1 empty', () => {
    jest.useFakeTimers();

    renderWrapper({
      ...mockInitialState,
      settlementPaymentConfig: {
        loading: false,
        currentValue: {
          agreementMessageWithConvenienceFee: '',
          agreementMessageWithoutConvenienceFee: 'test',
          agreementMessage: 'test'
        },
        previousValue: {
          agreementMessageWithConvenienceFee: 'test10',
          agreementMessageWithoutConvenienceFee: 'test20',
          agreementMessage: 'test30'
        }
      }
    });

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_settlementPayment_agreementMessageWithConvenienceFee_setDefaultButton_dls-button'
        )
      );
    });
    jest.runAllTimers();
  });

  it('Set default values > case 1', () => {
    jest.useFakeTimers();

    renderWrapper({
      ...mockInitialState,
      settlementPaymentConfig: {
        loading: false,
        currentValue: {
          agreementMessageWithConvenienceFee:
            DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE,
          agreementMessageWithoutConvenienceFee: 'test',
          agreementMessage: 'test'
        },
        previousValue: {
          agreementMessageWithConvenienceFee: 'test10',
          agreementMessageWithoutConvenienceFee: 'test20',
          agreementMessage: 'test30'
        }
      }
    });

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_settlementPayment_agreementMessageWithConvenienceFee_setDefaultButton_dls-button'
        )
      );
    });
    jest.runAllTimers();
  });

  it('Set default values > case 1 > return', () => {
    jest.useFakeTimers();

    renderWrapper({
      ...mockInitialState,
      settlementPaymentConfig: {
        loading: false,
        currentValue: {
          agreementMessageWithConvenienceFee: 'data 1',
          agreementMessageWithoutConvenienceFee: 'data 2',
          agreementMessage: 'data 3'
        },
        previousValue: {
          agreementMessageWithConvenienceFee: 'test10',
          agreementMessageWithoutConvenienceFee: 'test20',
          agreementMessage: 'test30'
        }
      }
    });

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_settlementPayment_agreementMessageWithConvenienceFee_setDefaultButton_dls-button'
        )
      );
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_settlementPayment_saveChangesBtn_dls-button'
        )
      );
    });
    jest.runAllTimers();
  });

  it('Set default values > case 2 empty', () => {
    jest.useFakeTimers();

    renderWrapper({
      ...mockInitialState,
      settlementPaymentConfig: {
        loading: false,
        currentValue: {
          agreementMessageWithConvenienceFee:
            DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE,
          agreementMessageWithoutConvenienceFee: '',
          agreementMessage: DEFAULT_PAYMENT_AGREEMENT_MESSAGE
        },
        previousValue: {
          agreementMessageWithConvenienceFee: 'test10',
          agreementMessageWithoutConvenienceFee: 'test20',
          agreementMessage: 'test30'
        }
      }
    });

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_settlementPayment_agreementMessageWithoutConvenienceFee_setDefaultButton_dls-button'
        )
      );
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_settlementPayment_saveChangesBtn_dls-button'
        )
      );
    });
    jest.runAllTimers();
  });

  it('Set default values > case 2', () => {
    jest.useFakeTimers();

    renderWrapper({
      ...mockInitialState,
      settlementPaymentConfig: {
        loading: false,
        currentValue: {
          agreementMessageWithConvenienceFee: 'test',
          agreementMessageWithoutConvenienceFee:
            DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE,
          agreementMessage: 'test'
        },
        previousValue: {
          agreementMessageWithConvenienceFee: 'test10',
          agreementMessageWithoutConvenienceFee: 'test20',
          agreementMessage: 'test30'
        }
      }
    });

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_settlementPayment_agreementMessageWithoutConvenienceFee_setDefaultButton_dls-button'
        )
      );
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_settlementPayment_saveChangesBtn_dls-button'
        )
      );
    });
    jest.runAllTimers();
  });

  it('Set default values > case 3 empty', () => {
    jest.useFakeTimers();

    renderWrapper({
      ...mockInitialState,
      settlementPaymentConfig: {
        loading: false,
        currentValue: {
          agreementMessageWithConvenienceFee: 'test',
          agreementMessageWithoutConvenienceFee: 'test',
          agreementMessage: ''
        },
        previousValue: {
          agreementMessageWithConvenienceFee: 'test10',
          agreementMessageWithoutConvenienceFee: 'test20',
          agreementMessage: 'test30'
        }
      }
    });

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_settlementPayment_agreementMessage_setDefaultButton_dls-button'
        )
      );
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_settlementPayment_saveChangesBtn_dls-button'
        )
      );
    });
    jest.runAllTimers();
  });

  it('Set default values > case 3', () => {
    jest.useFakeTimers();

    renderWrapper({
      ...mockInitialState,
      settlementPaymentConfig: {
        loading: false,
        currentValue: {
          agreementMessageWithConvenienceFee: 'test',
          agreementMessageWithoutConvenienceFee: 'test',
          agreementMessage: DEFAULT_PAYMENT_AGREEMENT_MESSAGE
        },
        previousValue: {
          agreementMessageWithConvenienceFee: 'test10',
          agreementMessageWithoutConvenienceFee: 'test20',
          agreementMessage: 'test30'
        }
      }
    });

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_settlementPayment_agreementMessage_setDefaultButton_dls-button'
        )
      );
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_settlementPayment_saveChangesBtn_dls-button'
        )
      );
    });
    jest.runAllTimers();
  });
});

describe('trigger useListenChangingTabEvent', () => {
  const mockDispatchControlTabChangeEvent = jest
    .spyOn(helpers, 'dispatchControlTabChangeEvent')
    .mockImplementationOnce(jest.fn);

  it('data not change', () => {
    renderWrapper(mockInitialState);

    emitChangingTabEvent();

    expect(mockDispatchControlTabChangeEvent).toBeCalled();
  });

  it('data was changed', () => {
    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });

    spy = jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);

    jest.useFakeTimers();

    renderWrapper(mockInitialState);

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_settlementPayment_agreementMessageWithConvenienceFee_setDefaultButton_dls-button'
        )
      );
    });
    jest.runAllTimers();

    emitChangingTabEvent();

    expect(mockDispatchControlTabChangeEvent).toBeCalled();
    expect(openConfirmModalAction).toBeCalled();
  });
});

describe('trigger useListenClosingModal', () => {
  const mockDispatchCloseModalEvent = jest
    .spyOn(helpers, 'dispatchCloseModalEvent')
    .mockImplementationOnce(jest.fn);

  it('data not change', () => {
    renderWrapper(mockInitialState);

    emitClosingModalEvent();

    expect(mockDispatchCloseModalEvent).toBeCalled();
  });

  it('data was changed', () => {
    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });

    spy = jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);
    renderWrapper(mockInitialState);

    jest.runAllTimers();

    act(() => {
      fireEvent.click(
        screen.getByTestId(
          'clientConfig_settlementPayment_agreementMessageWithConvenienceFee_setDefaultButton_dls-button'
        )
      );
    });
    jest.runAllTimers();

    emitClosingModalEvent();

    expect(mockDispatchCloseModalEvent).toBeCalled();
  });
});
