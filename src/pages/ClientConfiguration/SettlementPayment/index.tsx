import React, { useCallback, useEffect, useRef, useState } from 'react';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import { selectCurrentConfig } from './_redux/selectors';
import { clientConfigSettlementPaymentActions } from './_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// components
import { Button, SimpleBar } from 'app/_libraries/_dls/components';
import ApiErrorForHeaderSection from '../ApiErrorSection/ApiErrorForHeaderSection';
import TextEditor, {
  replaceMentionWith,
  revertMentionWith
} from 'app/_libraries/_dls/components/TextEditor';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useListenChangingTabEvent } from '../hooks/useListenChangingTabEvent';
import { useListenClosingModal } from '../hooks/useListenClosingModal';
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';

// redux
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';

// constants & types & helpers
import {
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE,
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE,
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE,
  FORM_FIELDS,
  I18N_SETTLEMENT_PAYMENT,
  MAX_LENGTH_CHARACTERS,
  SETTLEMENT_PAYMENT_MENTION_DATA
} from './constants';
import { SettlementPaymentItem } from './types';
import { TextEditorRef } from 'app/_libraries/_dls/components/TextEditor/types';

import isEmpty from 'lodash.isempty';
import isEqual from 'lodash.isequal';
import { genAmtId } from 'app/_libraries/_dls/utils';
import {
  dispatchCloseModalEvent,
  dispatchControlTabChangeEvent
} from '../helpers';
import { formatCommon } from 'app/helpers';
import { replaceMentionText, replaceSpecialValidChars } from './helpers';
import { REGEX_CHECK_SPECIAL_CHARS } from '../constants';

const SettlementPayment = () => {
  const dispatch = useDispatch();

  const testId = 'clientConfig_settlementPayment';

  const agreementMessageWithFeeEditorRef = useRef<TextEditorRef | null>(null);
  const agreementMessageWithoutFeeEditorRef =
    useRef<TextEditorRef | null>(null);
  const agreementMessageEditorRef = useRef<TextEditorRef | null>(null);

  const { t } = useTranslation();

  const currentValue = useSelector(selectCurrentConfig);
  const status = useCheckClientConfigStatus();

  const [agreementMessageWithFeeLength, setAgreementMessageWithFeeLength] =
    useState(0);
  const [
    agreementMessageWithoutFeeLength,
    setAgreementMessageWithoutFeeLength
  ] = useState(0);

  const [agreementMessageLength, setAgreementMessageLength] = useState(0);

  const [values, setValues] = useState<SettlementPaymentItem>({
    ...currentValue
  });
  const [errors, setErrors] = useState<SettlementPaymentItem>({});

  const isDataChanged = !isEqual({ ...currentValue }, { ...values });
  const handleSaveChanges = () => {
    const amWithCFMention = replaceMentionWith(
      agreementMessageWithFeeEditorRef.current!.getMarkdown()
    );
    const amWithoutCFMention = replaceMentionWith(
      agreementMessageWithoutFeeEditorRef.current!.getMarkdown()
    );
    const amMention = replaceMentionWith(
      agreementMessageEditorRef.current!.getMarkdown()
    );
    const requestBody = {
      agreementMessageWithConvenienceFee: amWithCFMention,
      agreementMessageWithoutConvenienceFee: amWithoutCFMention,
      agreementMessage: amMention
    };
    dispatch(
      clientConfigSettlementPaymentActions.triggerSubmitSettlementPaymentConfig(
        requestBody
      )
    );
  };

  const handleResetToPrev = () => {
    if (!isDataChanged) return;
    agreementMessageWithFeeEditorRef.current?.setMarkdown(
      revertMentionWith(currentValue.agreementMessageWithConvenienceFee!)
    );
    agreementMessageWithoutFeeEditorRef.current?.setMarkdown(
      revertMentionWith(currentValue.agreementMessageWithoutConvenienceFee!)
    );
    agreementMessageEditorRef.current?.setMarkdown(
      revertMentionWith(currentValue.agreementMessage!)
    );
    setValues({ ...currentValue });
    clearErrors('', true);
    batch(() => {
      dispatch(clientConfigSettlementPaymentActions.resetToPrevious());
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_SETTLEMENT_PAYMENT.RESET_MESSAGE_CONTENT
        })
      );
    });
  };

  const handleChangePaymentAgreementWithFee = (text: string) => {
    setAgreementMessageWithFeeLength(text?.length);
  };

  const handleChangePaymentAgreementWithoutFee = (text: string) => {
    setAgreementMessageWithoutFeeLength(text?.length);
  };

  const handleChangePaymentAgreement = (text: string) => {
    setAgreementMessageLength(text?.length);
  };

  const getCharactersLeft = useCallback(
    (current: number) => {
      const charsLeft = formatCommon(MAX_LENGTH_CHARACTERS - current).quantity;
      return `${charsLeft} ${t(I18N_SETTLEMENT_PAYMENT.CHARACTERS_LEFT)}`;
    },
    [t]
  );

  const handleChangingTab = () => {
    batch(() => {
      dispatchControlTabChangeEvent();
      dispatch(clientConfigSettlementPaymentActions.removeStore());
    });
  };

  const handleClosingModal = () => {
    batch(() => {
      dispatchCloseModalEvent();
      dispatch(clientConfigSettlementPaymentActions.removeStore());
    });
  };

  const validateCustom = (textData: string, editorId: string) => {
    clearErrors(editorId);

    const reg = new RegExp(REGEX_CHECK_SPECIAL_CHARS, 'g');
    const formatTextData = replaceSpecialValidChars(textData);

    if (formatTextData?.length === 0) {
      setErrors({
        ...errors,
        [editorId]: I18N_SETTLEMENT_PAYMENT.MESSAGE_IS_REQUIRED
      });
    }

    if (reg.test(formatTextData)) {
      setErrors({
        ...errors,
        [editorId]: I18N_SETTLEMENT_PAYMENT.NON_SPECIAL_CHARS
      });
    }
  };

  const clearErrors = (fieldId: string, isClearAll?: boolean) => {
    if (isClearAll) {
      setErrors({});
      return;
    }
    const cloneErrors = { ...errors };
    delete cloneErrors?.[fieldId as keyof SettlementPaymentItem];
    setErrors({ ...cloneErrors });
  };

  const handleBlurEditor = (editorId: string) => {
    switch (editorId) {
      case FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITH_CONVENIENCE_FEE:
        {
          const markdown = replaceMentionWith(
            agreementMessageWithFeeEditorRef.current?.getMarkdown() || ''
          ).trim();
          validateCustom(replaceMentionText(markdown), editorId);
          setValues({
            ...values,
            agreementMessageWithConvenienceFee: markdown
          });
        }
        break;
      case FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITHOUT_CONVENIENCE_FEE:
        {
          const markdown = replaceMentionWith(
            agreementMessageWithoutFeeEditorRef.current?.getMarkdown() || ''
          ).trim();
          validateCustom(replaceMentionText(markdown), editorId);
          setValues({
            ...values,
            agreementMessageWithoutConvenienceFee: markdown
          });
        }
        break;

      default:
        {
          const markdown = replaceMentionWith(
            agreementMessageEditorRef.current?.getMarkdown() || ''
          ).trim();
          validateCustom(replaceMentionText(markdown), editorId);

          setValues({
            ...values,
            agreementMessage: markdown
          });
        }
        break;
    }
  };

  const setDefaultMessage = (editorId: string) => {
    clearErrors(editorId);
    switch (editorId) {
      case FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITH_CONVENIENCE_FEE:
        {
          const markdown = replaceMentionWith(
            agreementMessageWithFeeEditorRef.current?.getMarkdown() || ''
          );

          if (isEqual(markdown, DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE)) {
            return;
          }
          agreementMessageWithFeeEditorRef.current?.setMarkdown(
            revertMentionWith(DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE)
          );
          setValues({
            ...values,
            agreementMessageWithConvenienceFee:
              DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE
          });
        }
        break;
      case FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITHOUT_CONVENIENCE_FEE:
        {
          const markdown = replaceMentionWith(
            agreementMessageWithoutFeeEditorRef.current?.getMarkdown() || ''
          );

          if (
            isEqual(markdown, DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE)
          ) {
            return;
          }
          agreementMessageWithoutFeeEditorRef.current?.setMarkdown(
            revertMentionWith(DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE)
          );
          setValues({
            ...values,
            agreementMessageWithoutConvenienceFee:
              DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE
          });
        }
        break;

      default:
        {
          const markdown = replaceMentionWith(
            agreementMessageEditorRef.current?.getMarkdown() || ''
          );

          if (isEqual(markdown, DEFAULT_PAYMENT_AGREEMENT_MESSAGE)) {
            return;
          }
          agreementMessageEditorRef.current?.setMarkdown(
            revertMentionWith(DEFAULT_PAYMENT_AGREEMENT_MESSAGE)
          );
          setValues({
            ...values,
            agreementMessage: DEFAULT_PAYMENT_AGREEMENT_MESSAGE
          });
        }
        break;
    }
  };

  useListenChangingTabEvent(() => {
    if (!isDataChanged) {
      handleChangingTab();
      return;
    }
    dispatch(
      confirmModalActions.open({
        title: 'txt_unsaved_changes',
        body: 'txt_discard_change_body',
        cancelText: 'txt_continue_editing',
        confirmText: 'txt_discard_change',
        confirmCallback: handleChangingTab,
        variant: 'danger'
      })
    );
  });

  useListenClosingModal(() => {
    if (!isDataChanged) {
      handleClosingModal();
      return;
    }
    dispatch(
      confirmModalActions.open({
        title: 'txt_unsaved_changes',
        body: 'txt_discard_change_body',
        cancelText: 'txt_continue_editing',
        confirmText: 'txt_discard_change',
        confirmCallback: handleClosingModal,
        variant: 'danger'
      })
    );
  });

  useEffect(() => {
    dispatch(
      clientConfigSettlementPaymentActions.getSettlementPaymentConfiguration()
    );
  }, [dispatch]);

  useEffect(() => {
    setValues({ ...currentValue });
  }, [currentValue]);

  useEffect(() => {
    const revertMarkdown = revertMentionWith(
      currentValue.agreementMessageWithConvenienceFee || ''
    );
    agreementMessageWithFeeEditorRef.current?.setMarkdown(revertMarkdown);
  }, [currentValue.agreementMessageWithConvenienceFee]);

  useEffect(() => {
    const revertMarkdown = revertMentionWith(
      currentValue.agreementMessageWithoutConvenienceFee || ''
    );
    agreementMessageWithoutFeeEditorRef.current?.setMarkdown(revertMarkdown);
  }, [currentValue.agreementMessageWithoutConvenienceFee]);

  useEffect(() => {
    const revertMarkdown = revertMentionWith(
      currentValue.agreementMessage || ''
    );
    agreementMessageEditorRef.current?.setMarkdown(revertMarkdown);
  }, [currentValue.agreementMessage]);

  const EDITORS = [
    {
      id: FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITH_CONVENIENCE_FEE,
      title: I18N_SETTLEMENT_PAYMENT.SETTLEMENT_PAYMENT_SUB_TITLE1,
      onPlainTextChange: handleChangePaymentAgreementWithFee,
      editorRef: agreementMessageWithFeeEditorRef,
      currentLength: agreementMessageWithFeeLength
    },
    {
      id: FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE_WITHOUT_CONVENIENCE_FEE,
      title: I18N_SETTLEMENT_PAYMENT.SETTLEMENT_PAYMENT_SUB_TITLE2,
      onPlainTextChange: handleChangePaymentAgreementWithoutFee,
      editorRef: agreementMessageWithoutFeeEditorRef,
      currentLength: agreementMessageWithoutFeeLength
    },
    {
      id: FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE,
      title: I18N_SETTLEMENT_PAYMENT.SETTLEMENT_PAYMENT_SUB_TITLE3,
      onPlainTextChange: handleChangePaymentAgreement,
      editorRef: agreementMessageEditorRef,
      currentLength: agreementMessageLength
    }
  ];

  return (
    <div className="position-relative has-footer-button">
      <>
        <SimpleBar>
          <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
            <>
              <ApiErrorForHeaderSection
                className="mb-24"
                dataTestId={`${testId}_apiError`}
              />
              <h5 data-testid={genAmtId(testId, 'title', '')}>
                {t(I18N_SETTLEMENT_PAYMENT.SETTLEMENT_PAYMENT)}
              </h5>
              <p
                className="mt-16"
                data-testid={genAmtId(testId, 'description', '')}
              >
                {t(I18N_SETTLEMENT_PAYMENT.SETTLEMENT_PAYMENT_DESCRIPTION)}
              </p>
              {EDITORS.map(item => (
                <div key={item.id}>
                  <div className="custom-editor-payment">
                    <h6 className="color-grey mt-16 mb-8">
                      {t(item.title)}
                      <span className="color-red"> *</span>
                    </h6>
                    <TextEditor
                      dataTestId={`${testId}_${item.id}_editor`}
                      onPlainTextChange={item.onPlainTextChange}
                      placeholder={t(I18N_SETTLEMENT_PAYMENT.ENTER_MESSAGE)}
                      maxPlainText={MAX_LENGTH_CHARACTERS}
                      mention
                      mentionData={SETTLEMENT_PAYMENT_MENTION_DATA}
                      onBlur={() => {
                        handleBlurEditor(item.id);
                      }}
                      ref={item.editorRef}
                      error={
                        errors[item.id as keyof SettlementPaymentItem]
                          ? {
                              status: true,
                              message: t(
                                errors[item.id as keyof SettlementPaymentItem]
                              )
                            }
                          : undefined
                      }
                    />
                    <div className="d-flex align-items-center justify-content-between mt-4">
                      <p
                        className="fs-12 color-grey-l24"
                        data-testid={genAmtId(
                          testId,
                          `${item.id}_character_left`,
                          ''
                        )}
                      >
                        {getCharactersLeft(item.currentLength)}
                      </p>
                      <Button
                        dataTestId={`${testId}_${item.id}_setDefaultButton`}
                        variant="outline-primary"
                        size="sm"
                        className="mr-n8"
                        onClick={() => setDefaultMessage(item.id)}
                      >
                        {t(I18N_SETTLEMENT_PAYMENT.SET_AS_DEFAULT_MESSAGE)}
                      </Button>
                    </div>
                  </div>
                  {item.id !== FORM_FIELDS.PAYMENT_AGREEMENT_MESSAGE ? (
                    <div className="divider-dashed mt-12"></div>
                  ) : (
                    ''
                  )}
                </div>
              ))}
            </>
          </div>
        </SimpleBar>
        <div className="group-button-footer d-flex justify-content-end">
          <Button
            disabled={status}
            onClick={handleResetToPrev}
            variant="secondary"
            dataTestId={`${testId}_resetToPreviousBtn`}
          >
            {t(I18N_SETTLEMENT_PAYMENT.RESET_TO_PREVIOUS)}
          </Button>
          <Button
            onClick={handleSaveChanges}
            type="submit"
            variant="primary"
            disabled={status || !isEmpty(errors) || !isDataChanged}
            dataTestId={`${testId}_saveChangesBtn`}
          >
            {t(I18N_SETTLEMENT_PAYMENT.SAVE_CHANGES)}
          </Button>
        </div>
      </>
    </div>
  );
};

export default SettlementPayment;
