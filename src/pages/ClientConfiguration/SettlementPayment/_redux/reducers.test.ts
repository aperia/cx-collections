import {
  clientConfigSettlementPaymentActions as actions,
  reducer
} from './reducers';

import { ClientConfigSettlementPaymentState } from '../types';

const initialState: ClientConfigSettlementPaymentState = {
  loading: false,
  currentValue: {
    agreementMessageWithConvenienceFee: 'text value 1',
    agreementMessageWithoutConvenienceFee: 'text value 3',
    agreementMessage: 'text value 3'
  },
  previousValue: {
    agreementMessageWithConvenienceFee: 'text value 1',
    agreementMessageWithoutConvenienceFee: 'text value 3',
    agreementMessage: 'text value 3'
  }
};

describe('CallResult Reducers', () => {
  it('setCurrentValue', () => {
    const state = reducer(
      initialState,
      actions.setCurrentValue({
        agreementMessageWithConvenienceFee: 'text value Input 1',
        agreementMessageWithoutConvenienceFee: 'text value Input 2',
        agreementMessage: 'text value Input 3'
      })
    );

    expect(state.currentValue?.agreementMessageWithConvenienceFee).toEqual(
      'text value Input 1'
    );
    expect(state.currentValue?.agreementMessageWithoutConvenienceFee).toEqual(
      'text value Input 2'
    );
    expect(state.currentValue?.agreementMessage).toEqual('text value Input 3');
  });

  it('resetToPrevious', () => {
    const state = reducer(initialState, actions.resetToPrevious());

    expect(state.currentValue).toEqual(initialState.previousValue);
  });

  it('removeStore', () => {
    const state = reducer(initialState, actions.removeStore());

    expect(state.currentValue).toBeUndefined();
  });
});
