import { createStore, Store } from '@reduxjs/toolkit';
import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator
} from 'app/test-utils';

import { rootReducer } from 'storeConfig';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

import { triggerSubmitSettlementPaymentConfig } from './submitSettlementPaymentConfig';
import { I18N_SETTLEMENT_PAYMENT } from '../constants';
import { reducer } from './reducers';
import { SettlementPaymentItem } from '../types';

const spyToast = mockActionCreator(actionsToast);

const mockSubmitArgs: SettlementPaymentItem = {
  agreementMessageWithConvenienceFee: 'test1',
  agreementMessageWithoutConvenienceFee: 'test2',
  agreementMessage: 'test3'
};

const mockUndefinedArgs: SettlementPaymentItem = {
  agreementMessageWithConvenienceFee: 'test1',
  agreementMessageWithoutConvenienceFee: 'test2',
  agreementMessage: undefined
};

const mockSubmitPayload: SettlementPaymentItem = {
  agreementMessageWithConvenienceFee: 'test1',
  agreementMessageWithoutConvenienceFee: 'test2',
  agreementMessage: 'test3'
};

describe('SettlementPayment > redux submitSettlementPaymentConfig', () => {
  const initialState: Partial<RootState> = {
    clientConfiguration: {
      settings: {
        selectedConfig: {
          id: '9999100020003000',
          agentId: '3000',
          clientId: '9999',
          systemId: '1000',
          principleId: '2000'
        }
      }
    },
    settlementPaymentConfig: {
      loading: false
    }
  };

  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, initialState);
  });

  it('fulfilled', () => {
    const nextState = reducer(
      initialState.settlementPaymentConfig,
      triggerSubmitSettlementPaymentConfig.fulfilled(
        mockSubmitPayload,
        'triggerSubmitSettlementPaymentConfig',
        mockSubmitArgs
      )
    );

    expect(nextState.loading).toEqual(false);
    expect(nextState.currentValue?.agreementMessage).toEqual('test3');
    expect(nextState.previousValue?.agreementMessage).toEqual('test3');
  });

  it('should return data', async () => {
    const response = await triggerSubmitSettlementPaymentConfig(mockSubmitArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('should show success message when triggerSubmitSettlementPaymentConfig fulfilled', async () => {
    const mockAddToast = spyToast('addToast');

    await triggerSubmitSettlementPaymentConfig(mockSubmitArgs)(
      byPassFulfilled(triggerSubmitSettlementPaymentConfig.fulfilled.type),
      store.getState,
      {}
    );

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_SETTLEMENT_PAYMENT.SETTLEMENT_PAYMENT_SETTING_UPDATED
    });
  });

  it('should show success message when triggerSubmitSettlementPaymentConfig fulfilled > with default', async () => {
    const mockAddToast = spyToast('addToast');

    await triggerSubmitSettlementPaymentConfig(mockUndefinedArgs)(
      byPassFulfilled(triggerSubmitSettlementPaymentConfig.fulfilled.type),
      store.getState,
      {}
    );

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_SETTLEMENT_PAYMENT.SETTLEMENT_PAYMENT_SETTING_UPDATED
    });
  });

  it('should show fail message when triggerSubmitSettlementPaymentConfig reject', async () => {
    const mockAddToast = spyToast('addToast');

    await triggerSubmitSettlementPaymentConfig({
      agreementMessageWithConvenienceFee: undefined,
      agreementMessageWithoutConvenienceFee: undefined,
      agreementMessage: undefined
    })(
      byPassRejected(triggerSubmitSettlementPaymentConfig.rejected.type),
      store.getState,
      {}
    );

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_SETTLEMENT_PAYMENT.SETTLEMENT_PAYMENT_SETTING_UPDATED_FAILED
    });
  });
});
