import { createStore, Store } from '@reduxjs/toolkit';
import { byPassFulfilled, byPassRejected } from 'app/test-utils';

import { rootReducer } from 'storeConfig';
import { getSettlementPaymentConfiguration } from './getSettlementPaymentConfig';
import { reducer } from './reducers';

const initialState: Partial<RootState> = {
  clientConfiguration: {
    settings: {
      selectedConfig: {
        id: '9999100020003000',
        agentId: '3000',
        clientId: '9999',
        systemId: '1000',
        principleId: '2000'
      }
    }
  },
  settlementPaymentConfig: {
    loading: false
  }
};

window.appConfig = {
  commonConfig: {
    user: 'user',
    app: 'app',
    org: 'org'
  } as CommonConfig
} as AppConfiguration;

describe('SettlementPayment > redux getSettlementPaymentConfig', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, initialState);
  });

  it('pending', () => {
    const nextState = reducer(
      initialState.settlementPaymentConfig,
      getSettlementPaymentConfiguration.pending(
        'getSettlementPaymentConfiguration',
        {}
      )
    );

    expect(nextState.loading).toEqual(true);
  });

  it('fulfilled', () => {
    const nextState = reducer(
      initialState.settlementPaymentConfig,
      getSettlementPaymentConfiguration.fulfilled(
        {
          agreementMessageWithConvenienceFee: 'test1',
          agreementMessageWithoutConvenienceFee: 'test2',
          agreementMessage: 'test3'
        },
        'getSettlementPaymentConfiguration',
        {}
      )
    );

    expect(nextState.loading).toEqual(false);
    expect(nextState.currentValue?.agreementMessage).toEqual('test3');
    expect(nextState.previousValue?.agreementMessage).toEqual('test3');
  });

  it('rejected', () => {
    const nextState = reducer(
      initialState.settlementPaymentConfig,
      getSettlementPaymentConfiguration.rejected(
        null,
        'getSettlementPaymentConfiguration',
        {}
      )
    );

    expect(nextState.loading).toEqual(false);
  });

  it('should fulfilled', async () => {
    const response = await getSettlementPaymentConfiguration({})(
      byPassFulfilled(getSettlementPaymentConfiguration.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      getSettlementPaymentConfiguration.fulfilled.type
    );
  });

  it('should be rejected', async () => {
    const response = await getSettlementPaymentConfiguration({})(
      byPassRejected(getSettlementPaymentConfiguration.rejected.type),
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      getSettlementPaymentConfiguration.rejected.type
    );
  });
});
