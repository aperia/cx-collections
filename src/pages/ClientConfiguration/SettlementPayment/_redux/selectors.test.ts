import { selectorWrapper } from 'app/test-utils';
import { DEFAULT_SETTLEMENT_PAYMENT } from '../constants';
import * as selectors from './selectors';

const store: Partial<RootState> = {
  settlementPaymentConfig: {
    loading: true,
    currentValue: DEFAULT_SETTLEMENT_PAYMENT,
    previousValue: DEFAULT_SETTLEMENT_PAYMENT
  }
};

describe('SettlementPayment Selectors', () => {
  it('selectLoading', () => {
    const { data } = selectorWrapper(store)(selectors.selectLoading);

    expect(data).toEqual(store.settlementPaymentConfig?.loading);
  });
  it('selectCurrentConfig', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectors.selectCurrentConfig
    );

    expect(data).toEqual(store.settlementPaymentConfig?.currentValue);
    expect(emptyData).toEqual(DEFAULT_SETTLEMENT_PAYMENT);
  });
  it('selectPreviousConfig', () => {
    const { data } = selectorWrapper(store)(selectors.selectPreviousConfig);

    expect(data).toEqual(store.settlementPaymentConfig?.previousValue);
  });
});
