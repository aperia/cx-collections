import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  PayloadAction
} from '@reduxjs/toolkit';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';
import { saveCollectionsClientConfig } from 'pages/ClientConfiguration/_redux/saveCollectionsClientConfig';

import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';

import { I18N_SETTLEMENT_PAYMENT } from '../constants';
import {
  ClientConfigSettlementPaymentState,
  SettlementPaymentArgs,
  SettlementPaymentPayLoad
} from '../types';

export const triggerSubmitSettlementPaymentConfig = createAsyncThunk<
  SettlementPaymentPayLoad,
  SettlementPaymentArgs,
  ThunkAPIConfig
>(
  'clientConfigSettlementPayment/triggerSubmitSettlementPaymentConfig',
  async (args, thunkAPI) => {
    const { dispatch, rejectWithValue, getState } = thunkAPI;

    const {
      agreementMessageWithConvenienceFee = '',
      agreementMessageWithoutConvenienceFee = '',
      agreementMessage = ''
    } = args;

    const currentData = getState().settlementPaymentConfig.currentValue;

    const response = await dispatch(
      saveCollectionsClientConfig({
        component: 'component_settlement_payment_config',
        jsonValue: {
          settlementPaymentConfig: {
            agreementMessageWithConvenienceFee,
            agreementMessageWithoutConvenienceFee,
            agreementMessage
          }
        }
      })
    );
    if (isFulfilled(response)) {
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_SETTLEMENT_PAYMENT.SETTLEMENT_PAYMENT_SETTING_UPDATED
          })
        );
        dispatch(
          changeHistoryActions.saveChangedClientConfig({
            action: 'UPDATE',
            changedCategory: 'settlementPayment',
            oldValue: {
              data: { ...currentData }
            },
            newValue: {
              data: {
                agreementMessageWithConvenienceFee,
                agreementMessageWithoutConvenienceFee,
                agreementMessage
              }
            }
          })
        );
      });
      return args;
    }

    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message:
          I18N_SETTLEMENT_PAYMENT.SETTLEMENT_PAYMENT_SETTING_UPDATED_FAILED
      })
    );
    dispatch(
      apiErrorNotificationAction.updateApiError({
        storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION,
        forSection: 'inClientConfigurationFeatureHeader',
        apiResponse: response.payload?.response
      })
    );
    return rejectWithValue({ errorMessage: response.error.message });
  }
);

export const triggerSubmitSettlementPaymentConfigBuilder = (
  builder: ActionReducerMapBuilder<ClientConfigSettlementPaymentState>
) => {
  builder
    .addCase(
      triggerSubmitSettlementPaymentConfig.pending,
      (draftState, action) => {
        draftState.loading = true;
      }
    )
    .addCase(
      triggerSubmitSettlementPaymentConfig.fulfilled,
      (draftState, action: PayloadAction<SettlementPaymentPayLoad>) => {
        draftState.previousValue = {...action.payload};
        draftState.currentValue = {...action.payload};
        draftState.loading = false;
      }
    )
    .addCase(
      triggerSubmitSettlementPaymentConfig.rejected,
      (draftState, action) => {
        draftState.loading = false;
      }
    );
};
