import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  ClientConfigSettlementPaymentState,
  SettlementPaymentItem
} from '../types';
import {
  getSettlementPaymentConfiguration,
  getSettlementPaymentConfigurationBuilder
} from './getSettlementPaymentConfig';
import {
  triggerSubmitSettlementPaymentConfig,
  triggerSubmitSettlementPaymentConfigBuilder
} from './submitSettlementPaymentConfig';

export const initialState: ClientConfigSettlementPaymentState = {
  loading: false
};

const { actions, reducer } = createSlice({
  name: 'clientConfigSettlementPayment',
  initialState,
  reducers: {
    resetToPrevious: draftState => {
      draftState.currentValue = {...draftState.previousValue};
    },
    setCurrentValue: (
      draftState,
      action: PayloadAction<SettlementPaymentItem>
    ) => {
      draftState.currentValue = action.payload;
    },
    removeStore: () => initialState
  },
  extraReducers: builder => {
    triggerSubmitSettlementPaymentConfigBuilder(builder);
    getSettlementPaymentConfigurationBuilder(builder);
  }
});

const clientConfigSettlementPaymentActions = {
  ...actions,
  triggerSubmitSettlementPaymentConfig,
  getSettlementPaymentConfiguration
};

export { clientConfigSettlementPaymentActions, reducer };
