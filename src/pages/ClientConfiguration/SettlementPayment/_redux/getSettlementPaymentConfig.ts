import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';

import { getCollectionsClientConfig } from 'pages/ClientConfiguration/_redux/getCollectionsClientConfig';
import {
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE,
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE,
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE
} from '../constants';
import {
  ClientConfigSettlementPaymentState,
  SettlementPaymentPayArgs,
  SettlementPaymentPayLoad
} from '../types';

export const getSettlementPaymentConfiguration = createAsyncThunk<
  SettlementPaymentPayLoad,
  SettlementPaymentPayArgs | undefined,
  ThunkAPIConfig
>(
  'clientConfigSettlementPayment/getSettlementPaymentConfig',
  async (args, { dispatch, rejectWithValue }) => {
    const response = await dispatch(
      getCollectionsClientConfig({
        component: 'component_settlement_payment_config',
        storeId: args?.storeId
      })
    );

    if (isFulfilled(response)) {
      const {
        agreementMessageWithConvenienceFee = DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE,
        agreementMessageWithoutConvenienceFee = DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE,
        agreementMessage = DEFAULT_PAYMENT_AGREEMENT_MESSAGE
      } = response.payload?.settlementPaymentConfig || {};
      return {
        agreementMessageWithConvenienceFee,
        agreementMessageWithoutConvenienceFee,
        agreementMessage
      };
    }
    return rejectWithValue({ errorMessage: response.error.message });
  }
);

export const getSettlementPaymentConfigurationBuilder = (
  builder: ActionReducerMapBuilder<ClientConfigSettlementPaymentState>
) => {
  builder
    .addCase(getSettlementPaymentConfiguration.pending, draftState => {
      draftState.loading = true;
    })
    .addCase(
      getSettlementPaymentConfiguration.fulfilled,
      (draftState, action) => {
        draftState.previousValue = { ...action.payload };
        draftState.currentValue = { ...action.payload };
        draftState.loading = false;
      }
    )
    .addCase(
      getSettlementPaymentConfiguration.rejected,
      (draftState, action) => {
        draftState.loading = false;
      }
    );
};
