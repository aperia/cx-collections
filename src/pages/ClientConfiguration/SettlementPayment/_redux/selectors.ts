import { createSelector } from '@reduxjs/toolkit';
import { DEFAULT_SETTLEMENT_PAYMENT } from '../constants';

const getSettlementPaymentConfigState = (state: RootState) => {
  return state.settlementPaymentConfig;
};

export const selectCurrentConfig = createSelector(
  getSettlementPaymentConfigState,
  data => data?.currentValue || DEFAULT_SETTLEMENT_PAYMENT
);

export const selectPreviousConfig = createSelector(
  getSettlementPaymentConfigState,
  data => data?.previousValue
);

export const selectLoading = createSelector(
  getSettlementPaymentConfigState,
  data => data?.loading
);

