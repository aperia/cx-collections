export interface SettlementPaymentItem {
  agreementMessageWithConvenienceFee?: string;
  agreementMessageWithoutConvenienceFee?: string;
  agreementMessage?: string;
}

export interface ClientConfigSettlementPaymentState {
  previousValue?: SettlementPaymentItem;
  currentValue?: SettlementPaymentItem;
  loading: boolean;
}

export interface SettlementPaymentPayLoad extends SettlementPaymentItem {}

export interface SettlementPaymentArgs extends SettlementPaymentItem {}

export interface SettlementPaymentPayArgs {
  storeId?: string;
}

export type ErrorControlValidation = {
  status?: boolean;
  message?: string;
};
