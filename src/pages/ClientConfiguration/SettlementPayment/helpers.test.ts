import * as helpers from './helpers';

describe('Test SettlementPayment helpers', () => {

  it('should replaceMentionText', () => {
    const result = helpers.replaceMentionText('abc {Payment Amount}');
    expect(result).toEqual('abc <!Payment Amount!>');
  });

  it('should replaceSpecialValidChars', () => {
    const result = helpers.replaceSpecialValidChars('abc {abc} {xyz} \\`');
    expect(result).toEqual('abc {abc} {xyz} `');
  });
  it('should replaceSpecialValidChars', () => {
    const result = helpers.replaceSpecialValidChars('abc {abc} {xyz} \\~');
    expect(result).toEqual('abc {abc} {xyz} ~');
  });
  it('should replaceSpecialValidChars', () => {
    const result = helpers.replaceSpecialValidChars('abc {abc} {xyz} \\*');
    expect(result).toEqual('abc {abc} {xyz} *');
  });
  it('should replaceSpecialValidChars', () => {
    const result = helpers.replaceSpecialValidChars('abc {abc} {xyz} \\#');
    expect(result).toEqual('abc {abc} {xyz} #');
  });
  it('should replaceSpecialValidChars', () => {
    const result = helpers.replaceSpecialValidChars('abc {abc} {xyz} \\-');
    expect(result).toEqual('abc {abc} {xyz} -');
  });
  it('should replaceSpecialValidChars', () => {
    const result = helpers.replaceSpecialValidChars('abc {abc} {xyz} \\[');
    expect(result).toEqual('abc {abc} {xyz} [');
  });
  it('should replaceSpecialValidChars', () => {
    const result = helpers.replaceSpecialValidChars('abc {abc} {xyz} \\]');
    expect(result).toEqual('abc {abc} {xyz} ]');
  });
  it('should replaceSpecialValidChars', () => {
    const result = helpers.replaceSpecialValidChars('abc {abc} {xyz}');
    expect(result).toEqual('abc {abc} {xyz}');
  });
});
