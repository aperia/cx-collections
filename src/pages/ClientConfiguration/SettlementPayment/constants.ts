import { SettlementPaymentItem } from './types';

export const I18N_SETTLEMENT_PAYMENT = {
  SETTLEMENT_PAYMENT: 'txt_settlement_payment',
  SETTLEMENT_PAYMENT_DESCRIPTION: 'txt_settlement_payment_description',
  SETTLEMENT_PAYMENT_SUB_TITLE1: 'txt_settlement_payment_sub_title1',
  SETTLEMENT_PAYMENT_SUB_TITLE2: 'txt_settlement_payment_sub_title2',
  SETTLEMENT_PAYMENT_SUB_TITLE3: 'txt_settlement_payment_sub_title3',
  SET_AS_DEFAULT_MESSAGE: 'txt_set_as_default_message',
  CHARACTERS_LEFT: 'txt_charactes_left',
  SETTLEMENT_PAYMENT_SETTING_UPDATED: 'txt_setting_updated',
  SETTLEMENT_PAYMENT_SETTING_UPDATED_FAILED: 'txt_setting_updated_failed',
  MESSAGE_IS_REQUIRED: 'txt_message_is_required',
  MESSAGE_CONTENT: 'txt_message_content',
  RESET_MESSAGE_CONTENT: 'txt_settlement_payment_reset_to_previous_success',
  NON_SPECIAL_CHARS: 'txt_settlement_payment_non_special_chars',
  SAVE_CHANGES: 'txt_save_changes',
  RESET_TO_PREVIOUS: 'txt_reset_to_previous',
  ENTER_MESSAGE: 'txt_enter_message'
};

export const MAX_LENGTH_CHARACTERS = 2000;

export const SETTLEMENT_PAYMENT_MENTION_DATA = [
  'Account Number',
  'Convenience Fee',
  'Payment Amount',
  'Payment Date'
];

export const PAYMENT_AGREEMENT_MAX_LENGTH = 2000;

export const FORM_FIELDS = {
  PAYMENT_AGREEMENT_MESSAGE_WITH_CONVENIENCE_FEE:
    'agreementMessageWithConvenienceFee',
  PAYMENT_AGREEMENT_MESSAGE_WITHOUT_CONVENIENCE_FEE:
    'agreementMessageWithoutConvenienceFee',
  PAYMENT_AGREEMENT_MESSAGE: 'agreementMessage'
};

export const DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE = `Before I process your payment, let me review your payment information with you. Today you are authorizing us to deduct the amount of {Payment Amount} from your account ending in {Account Number} via a one-time electronic funds transfer. You will be charged a {Convenience Fee} convenience fee. Your payment will be debited from your account on or after {Payment Date} .If you have questions or would like to revoke this authorization, please call 1-800-575-6457. Can you please confirm your authorization to proceed?`;

export const DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE = `Before I process your payment, let me review your payment information with you. Today you are authorizing us to deduct the amount of {Payment Amount} from your account ending in {Account Number} via a one-time electronic funds transfer. Your payment will be debited from your account on or after {Payment Date} .If you have questions or would like to revoke this authorization, please call 1-800-575-6457. Can you please confirm your authorization to proceed?`;

export const DEFAULT_PAYMENT_AGREEMENT_MESSAGE = `Before I process the collection form, let me review the information with you. Today you promise to pay the amount of {Payment Amount} on {Payment Date} .Can you please confirm the information to proceed?`;

export const DEFAULT_SETTLEMENT_PAYMENT: SettlementPaymentItem = {
  agreementMessageWithConvenienceFee: '',
  agreementMessageWithoutConvenienceFee: '',
  agreementMessage: ''
};
