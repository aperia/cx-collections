import React from 'react';

//mock test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

//components
import Actions from './Actions';

//actions
import { accountExternalStatusActions } from './_redux/reducers';

//constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

const mockAccountExternalActions = mockActionCreator(
  accountExternalStatusActions
);
describe('render actions buttons ', () => {
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<Actions data={{}} />, { initialState });
  };
  it('should render modal delete and call dispatch modal actions when click delete button', () => {
    const mockAction = mockAccountExternalActions('toggleReasonCode');

    renderWrapper({});

    const deleteButton = screen.getByRole('button', {
      name: I18N_COMMON_TEXT.DELETE
    });

    expect(deleteButton).toBeInTheDocument();

    userEvent.click(deleteButton);

    expect(mockAction).toBeCalledWith({
      modalType: 'delete',
      selectedCode: {}
    });
  });

  it('should render modal edit and call dispatch modal actions when click edit button', () => {
    const mockAction = mockAccountExternalActions('toggleReasonCode');

    renderWrapper({});

    const editButton = screen.getByRole('button', {
      name: I18N_COMMON_TEXT.EDIT
    });

    expect(editButton).toBeInTheDocument();

    userEvent.click(editButton);

    expect(mockAction).toBeCalledWith({ modalType: 'edit', selectedCode: {} });
  });
});
