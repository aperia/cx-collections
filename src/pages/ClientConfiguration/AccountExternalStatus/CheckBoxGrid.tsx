import React from 'react';

//types
import { CheckBoxGridState } from './types';

//components
import { CheckBox } from 'app/_libraries/_dls/components';
import { genAmtId } from 'app/_libraries/_dls/utils';

const CheckBoxGrid: React.FC<CheckBoxGridState> = ({
  columnId,
  data,
  onChange,
  disabledAll = false,
  status,
  dataTestId
}) => {
  const { checked = false } =
    (data.checkList && data.checkList[columnId]) || {};
  const id = `${columnId}_${data.value}`;
  const disabled = columnId === data.value;

  return (
    <CheckBox className="checkbox-center" dataTestId={genAmtId(dataTestId, '', 'CheckBoxGrid')}>
      <CheckBox.Input
        id={id}
        readOnly={status}
        checked={checked || false}
        disabled={disabled || disabledAll}
        onChange={onChange}
      />
    </CheckBox>
  );
};

export default CheckBoxGrid;
