import { apiService } from 'app/utils/api.service';

//types
import { ExternalStatusPayload, ReasonCodeRequest } from './types';

export const accountExternalStatusServices = {
  saveAccountExternalInfo(payload: ExternalStatusPayload) {
    const url =
      window.appConfig?.api?.clientConfigAccountExternalStatus
        ?.saveExternalStatus || '';
    return apiService.post(url, payload);
  },
  addReasonCodeConfigList(requestBody: ReasonCodeRequest) {
    const url =
      window.appConfig?.api?.clientConfigAccountExternalStatus?.addReasonCode ||
      '';
    return apiService.post(url, requestBody);
  },
  editReasonCodeConfigList(requestBody: ReasonCodeRequest) {
    const url =
      window.appConfig?.api?.clientConfigAccountExternalStatus
        ?.editReasonCode || '';
    return apiService.post(url, requestBody);
  },
  deleteReasonCodeConfigList(requestBody: ReasonCodeRequest) {
    const url =
      window.appConfig?.api?.clientConfigAccountExternalStatus
        ?.deleteReasonCode || '';
    return apiService.post(url, requestBody);
  }
};
