import React from 'react';
import { useSelector } from 'react-redux';
import isEmpty from 'lodash.isempty';
import { batch, useDispatch } from 'react-redux';
import { Button, SimpleBar } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';

//hooks
import { useListenChangingTabEvent } from '../hooks/useListenChangingTabEvent';
import { useListenClosingModal } from '../hooks/useListenClosingModal';

//constants
import { I18N_ACCOUNT_EXTERNAL_STATUS_TEXT } from './constants';

//components UI
import AccountExternalStatusGrid from './AccountExternalStatusGrid';
import ReasonCodeConfiguration from './ReasonCodeConfiguration';
import ReasonCodeModal from './ReasonCodeModal';
import AddReasonCodeButton from './AddReasonCodeButton';

//redux
import { accountExternalStatusActions } from './_redux/reducers';
import {
  reasonCodeModal,
  reasonCodeModalType,
  selectDataChange,
  selectedReasonCodeConfigurationData
} from './_redux/selectors';

// Helper
import {
  dispatchCloseModalEvent,
  dispatchControlTabChangeEvent
} from '../helpers';

//hooks
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';
import { genAmtId } from 'app/_libraries/_dls/utils';
import ApiErrorForHeaderSection from '../ApiErrorSection/ApiErrorForHeaderSection';

const AccountExternalStatus: React.FC = () => {
  const dispatch = useDispatch();

  const status = useCheckClientConfigStatus();

  const { t } = useTranslation();

  const testId = 'clientConfig_accountExternal';

  const isOpenModal = useSelector(reasonCodeModal);

  const modalType = useSelector(reasonCodeModalType);

  const isDataChange = useSelector(selectDataChange);

  const reasonCodeData = useSelector(selectedReasonCodeConfigurationData);

  const handleReset = () => {
    dispatch(
      accountExternalStatusActions.triggerResetToPrevious({
        toast: {
          show: true,
          type: 'success',
          message: t(
            I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.RESET_TO_PREVIOUS_SUCCESS
          )
        }
      })
    );
  };

  const handleOnSave = () => {
    dispatch(accountExternalStatusActions.triggerOnSaveData());
  };

  const handleToggleModal = () => {
    batch(() => {
      dispatch(accountExternalStatusActions.toggleReasonCode({ modalType }));
      dispatch(accountExternalStatusActions.setAddReasonCodeErrorMessage(''));
    });
  };

  useListenChangingTabEvent(() => {
    dispatch(
      accountExternalStatusActions.triggerChangeTabEvent({
        modalPayload: {
          title: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.UNSAVED_CHANGE,
          body: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.DISCARD_CHANGE_BODY,
          cancelText: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.CONTINUE_EDITING,
          confirmText: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.DISCARD_CHANGE,
          confirmCallback: dispatchControlTabChangeEvent,
          variant: 'danger'
        }
      })
    );
  });

  useListenClosingModal(() => {
    dispatch(
      accountExternalStatusActions.triggerCloseTabEvent({
        modalPayload: {
          title: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.UNSAVED_CHANGE,
          body: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.DISCARD_CHANGE_BODY,
          cancelText: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.CONTINUE_EDITING,
          confirmText: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.DISCARD_CHANGE,
          confirmCallback: dispatchCloseModalEvent,
          variant: 'danger'
        }
      })
    );
  });

  return (
    <div className={'position-relative has-footer-button'}>
      <SimpleBar>
        <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
          <ApiErrorForHeaderSection
            className="mb-24"
            dataTestId={`${testId}_apiError`}
          />
          <h5 className="mb-16" data-testid={genAmtId(testId, 'title', '')}>
            {t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.ACCOUNT_EXTERNAL_STATUS)}
          </h5>
          <strong
            className="mt-16"
            data-testid={genAmtId(testId, 'currentStatusTitle', '')}
          >
            {t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.CURRENT_STATUS_CONFIGURATION)}
          </strong>

          <AccountExternalStatusGrid status={status} />
          <div className="d-flex align-items-center justify-content-between mt-24">
            <strong data-testid={genAmtId(testId, 'reasonCodeConfig', '')}>
              {t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.REASON_CODE_CONFIGURATION)}
            </strong>
            {!isEmpty(reasonCodeData) && (
              <AddReasonCodeButton status={status} />
            )}
          </div>

          <ReasonCodeConfiguration status={status} />
        </div>
      </SimpleBar>
      <div className="group-button-footer d-flex justify-content-end">
        <Button
          disabled={status}
          onClick={handleReset}
          variant="secondary"
          dataTestId={`${testId}_resetToPreviousBtn`}
        >
          {t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.RESET_TO_PREVIOUS)}
        </Button>
        <Button
          disabled={status || !isDataChange}
          onClick={handleOnSave}
          variant="primary"
          dataTestId={`${testId}_saveChangesBtn`}
        >
          {t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.SAVE_CHANGES)}
        </Button>
      </div>

      {isOpenModal && (
        <ReasonCodeModal
          modalType={modalType}
          show={isOpenModal}
          toggle={handleToggleModal}
        />
      )}
    </div>
  );
};

export default AccountExternalStatus;
