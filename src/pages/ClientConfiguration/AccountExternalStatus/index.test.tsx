import React from 'react';

//test-utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

//components
import AccountExternalStatus from './';

//helpers
import * as clientConfigHelper from 'pages/ClientConfiguration/helpers';

//actions
import { accountExternalStatusActions } from './_redux/reducers';

//constants
import { I18N_ACCOUNT_EXTERNAL_STATUS_TEXT } from './constants';
import { CLIENT_CONFIG_STATUS_KEY, SECTION_TAB_EVENT } from '../constants';

import * as mockStatus from '../hooks/useCheckClientConfigStatus';

jest.mock('app/_libraries/_dls/utils/genAmtId', () => () => '');

jest.mock('./AccountExternalStatusGrid', () => () => (
  <div>AccountExternalStatusGrid</div>
));

jest.mock('./ReasonCodeConfiguration', () => () => (
  <div>ReasonCodeConfiguration</div>
));

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

jest.mock('./ReasonCodeModal', () => ({ toggle }: any) => {
  return (
    <div>
      <button onClick={toggle}>trigger toggle</button>
    </div>
  );
});

const mockAccountExternalActions = mockActionCreator(
  accountExternalStatusActions
);

const withReasonCodeList: Partial<RootState> = {
  accountExternalStatus: {
    accountExternalStatusList: {
      data: [
        {
          value: 'M',
          description: 'Description',
          checkList: {
            name: { checked: true }
          }
        }
      ],
      loading: false
    },
    data: {
      reasonCode: [],
      externalCode: []
    },
    isUpdating: false,
    reasonCodeList: {
      data: [
        {
          value: '01',
          description: 'rainy'
        },
        {
          value: '02',
          description: 'rainy'
        }
      ],
      modalErrorMessage: '',
      modalLoading: false,
      modalType: '',
      open: true
    }
  },
  clientConfiguration: {
    settings: {
      selectedConfig: {
        id: '2',
        additionalFields: [
          {
            name: CLIENT_CONFIG_STATUS_KEY,
            active: true
          }
        ]
      }
    }
  }
};

describe('render Account External Status tabs', () => {
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<AccountExternalStatus />, { initialState });
  };

  beforeEach(() => {
    jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(false);
  });

  it('should render footer button', () => {
    renderWrapper(withReasonCodeList);

    const saveChangeButton = screen.getByRole('button', {
      name: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.SAVE_CHANGES
    });

    const resetPreviousButton = screen.getByRole('button', {
      name: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.RESET_TO_PREVIOUS
    });

    expect(saveChangeButton).toBeInTheDocument();

    expect(resetPreviousButton).toBeInTheDocument();
  });

  it('should render reason code modal', () => {
    const mockToggleReasonCode = mockAccountExternalActions('toggleReasonCode');
    const mockSetAddReasonCode = mockAccountExternalActions(
      'setAddReasonCodeErrorMessage'
    );
    renderWrapper(withReasonCodeList);

    userEvent.click(screen.getByText('trigger toggle'));

    expect(mockToggleReasonCode).toBeCalledWith({
      modalType: ''
    });

    expect(mockSetAddReasonCode).toBeCalledWith('');
  });

  it('should render reason code modal > reasonCodeList is empty array ', () => {
    const mockSetAddReasonCode = mockAccountExternalActions(
      'setAddReasonCodeErrorMessage'
    );

    renderWrapper({
      ...withReasonCodeList,
      accountExternalStatus: {
        ...withReasonCodeList.accountExternalStatus,
        reasonCodeList: []
      }
    } as never);

    expect(mockSetAddReasonCode).not.toBeCalled();
  });

  it('should trigger handleReset when click reset button', () => {
    const mockTriggerResetToPrevious = mockAccountExternalActions(
      'triggerResetToPrevious'
    );

    renderWrapper(withReasonCodeList);

    const resetPreviousButton = screen.getByRole('button', {
      name: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.RESET_TO_PREVIOUS
    });

    userEvent.click(resetPreviousButton);

    expect(mockTriggerResetToPrevious).toBeCalledWith({
      toast: {
        show: true,
        type: 'success',
        message: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.RESET_TO_PREVIOUS_SUCCESS
      }
    });
  });

  it('should trigger handleOnSave when click save button', () => {
    const mockTriggerOnSaveData =
      mockAccountExternalActions('triggerOnSaveData');

    renderWrapper({
      accountExternalStatus: {
        accountExternalStatusList: {},
        reasonCodeList: {
          data: [
            {
              description: 'reasonCode description 1',
              value: 'value',
              checkList: {
                value: { checked: true }
              }
            }
          ],
          originData: [
            {
              description: 'reasonCode description 1',
              value: 'value',
              checkList: {
                value: { checked: false }
              }
            }
          ]
        }
      }
    } as any);

    const saveChangeButton = screen.getByRole('button', {
      name: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.SAVE_CHANGES
    });

    userEvent.click(saveChangeButton);

    expect(mockTriggerOnSaveData).toBeCalled();
  });

  describe('useListenChangingTabEvent', () => {
    let spy: jest.SpyInstance;

    afterEach(() => {
      spy.mockReset();
      spy.mockRestore();
    });

    it('should trigger useListenChangingTabEvent', () => {
      const openConfirmModalAction = jest.fn(x => {
        if (x.modalPayload.confirmCallback) {
          x.modalPayload.confirmCallback();
        }
        return { type: 'test' };
      });

      spy = jest
        .spyOn(accountExternalStatusActions, 'triggerChangeTabEvent')
        .mockImplementation(openConfirmModalAction as never);

      renderWrapper(withReasonCodeList);

      const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGING_TAB, {
        bubbles: true,
        cancelable: true
      });

      window.dispatchEvent(event);

      expect(openConfirmModalAction).toBeCalledWith({
        modalPayload: expect.objectContaining({
          title: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.UNSAVED_CHANGE,
          body: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.DISCARD_CHANGE_BODY,
          cancelText: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.CONTINUE_EDITING,
          confirmText: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.DISCARD_CHANGE,
          variant: 'danger'
        })
      });
    });
  });

  describe('useListenClosingModal', () => {
    let spy: jest.SpyInstance;

    afterEach(() => {
      spy.mockReset();
      spy.mockRestore();
    });

    const mockDispatchCloseModalEvent = jest.fn();
    it('should trigger useListenClosingModal', () => {
      spy = jest
        .spyOn(clientConfigHelper, 'dispatchCloseModalEvent')
        .mockImplementation(mockDispatchCloseModalEvent);
      const triggerCloseTab = mockAccountExternalActions(
        'triggerCloseTabEvent'
      );
      renderWrapper(withReasonCodeList);

      const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
        bubbles: true,
        cancelable: true
      });

      window.dispatchEvent(event);

      expect(triggerCloseTab).toBeCalled();
    });
  });
});
