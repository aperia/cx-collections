// Type
import { ReasonCodesDataType } from './types';

export const getCheckListData = (data?: ReasonCodesDataType[]) => {
  return data?.reduce((checkListData: Array<string>, item) => {
    const currentChecklist = item?.checkList || {};
    const nextData = Object.keys(currentChecklist).filter(
      itemCurrent => currentChecklist[itemCurrent].checked
    );
    checkListData = [...checkListData, ...nextData];
    return checkListData;
  }, []);
};

export const getValueAndDescription = (data: MagicKeyValue) => {
  if (data?.value === '' || data?.value === ' ') return data.description;

  return `${data.value} - ${data.description}`;
};
