import React from 'react';

//test-utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

//components
import ReasonCodeConfiguration from './ReasonCodeConfiguration';

//constants
import { I18N_ACCOUNT_EXTERNAL_STATUS_TEXT } from './constants';

// Hooks
import * as usePagination from 'app/hooks/usePagination';
import { codeToTextActions } from '../CodeToText/_redux/reducers';

const codeToTextActionSpy = mockActionCreator(codeToTextActions);

jest.mock('app/_libraries/_dls/utils/genAmtId', () => () => '');

beforeEach(() => {
  jest.spyOn(usePagination, 'usePagination').mockImplementation(
    () =>
      ({
        total: 200,
        currentPage: 1,
        pageSize: [10, 25, 50],
        currentPageSize: 10,
        gridData: [
          {
            id: '1',
            reasonCode: '01 - rainy',
            value: '01',
            description: 'rainy'
          },
          {
            id: '2',
            reasonCode: '02 - rainy',
            value: '02',
            description: 'rainy',
            checkList: { display: { checked: null } }
          }
        ],
        onPageChange: () => {},
        onPageSizeChange: () => {}
      } as any)
  );
});

describe('should render reasonCodeConfiguration', () => {
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<ReasonCodeConfiguration status={false} />, {
      initialState
    });
  };

  it('should render reasonCodeConfiguration no data reasonCodeList', async () => {
    const initialState: Partial<RootState> = {
      accountExternalStatus: {
        accountExternalStatusList: {
          data: [
            {
              value: 'M',
              description: 'Description',
              checkList: { A: { checked: true } }
            }
          ]
        },
        data: {
          reasonCode: [],
          externalCode: []
        },
        isUpdating: false,
        reasonCodeList: {
          data: [],
          modalErrorMessage: '',
          modalLoading: false,
          modalType: '',
          open: false
        }
      }
    };

    renderWrapper(initialState);

    expect(
      screen.getByText(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.NO_DATA)
    ).toBeInTheDocument();
  });

  it('should render reasonCodeConfiguration execute function handleChangeTabToCodeToText ', async () => {
    const setSelectedCodeToTextTypes = codeToTextActionSpy(
      'setSelectedCodeToTextTypes'
    );
    const selectedDropdown = codeToTextActionSpy('selectedDropdown');
    const initialState: Partial<RootState> = {
      accountExternalStatus: {
        accountExternalStatusList: {
          data: []
        },
        data: {
          reasonCode: [],
          externalCode: []
        },
        isUpdating: false,
        reasonCodeList: {
          data: [],
          modalErrorMessage: '',
          modalLoading: false,
          modalType: '',
          open: false
        }
      }
    };

    renderWrapper(initialState);

    const queryCodeToText = screen.queryByText(/txt_code_to_txt/);
    queryCodeToText!.click();

    expect(setSelectedCodeToTextTypes).toHaveBeenCalledWith([
      'accountExternalStatus'
    ]);
    expect(selectedDropdown).toHaveBeenCalledWith({
      data: [
        {
          code: 'accountExternalStatus',
          description: 'Account External Status'
        }
      ],
      listCode: ['accountExternalStatus']
    });
  });

  it('should render reasonCodeConfiguration  have data', async () => {
    const initialState: Partial<RootState> = {
      accountExternalStatus: {
        accountExternalStatusList: {
          data: [
            {
              description: '10',
              value: '10',
              checkList: {
                name: { checked: true }
              }
            }
          ]
        },
        data: {
          reasonCode: [],
          externalCode: []
        },
        isUpdating: false,
        reasonCodeList: {
          data: [
            {
              description: 'description',
              value: 'value',
              checkList: {
                name: { checked: true }
              }
            }
          ],
          modalErrorMessage: '',
          modalLoading: false,
          modalType: '',
          open: false
        }
      }
    };

    renderWrapper(initialState);

    const reasonCodeTable = screen.getByRole('table');

    expect(reasonCodeTable).toBeInTheDocument();

    userEvent.click(
      screen.getAllByText(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.REASON_CODE)[0]
    );
  });
});
