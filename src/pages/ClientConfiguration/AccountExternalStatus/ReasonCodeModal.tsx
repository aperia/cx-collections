import React, { useEffect, useRef } from 'react';

//components
import {
  Button,
  Modal,
  InlineMessage,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

//view
import { View, ExtraFieldProps } from 'app/_libraries/_dof/core';

//forms
import { Field } from 'redux-form';

//selectors
import {
  loadingReasonCodeConfiguration,
  selectedReasonCode
} from './_redux/selectors';

//redux
import { useSelector, useDispatch } from 'react-redux';

//actions
import { accountExternalStatusActions } from './_redux/reducers';

//hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSelectFormValue } from 'app/hooks/useSelectFormValue';
import { useBehaviorApi } from './hooks';
import { isValid } from 'redux-form';

//constants
import { I18N_ACCOUNT_EXTERNAL_STATUS_TEXT } from './constants';

//libs
import { debounce, isEmpty, isFunction } from 'app/_libraries/_dls/lodash';
import { useIsDirtyForm } from 'app/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import ModalBodyWithErrorClientConfig from '../ApiErrorSection/ModalBodyWithErrorClientConfig';

interface ReasonCodeModalType {
  modalType: string;
  show: boolean;
  toggle: () => void;
}

const DOF_REASON_CODE_NAME = 'reasonCodeClientConfigView';

const ReasonCodeModal: React.FC<ReasonCodeModalType> = ({
  modalType,
  show,
  toggle
}) => {
  const dispatch = useDispatch();
  const testId = 'clientConfig_accountExternal_reasonCodeModal';

  const { reasonCodeModalErrorMessage: errorMessage } = useBehaviorApi();

  const selectedCode = useSelector(selectedReasonCode);

  const [initialData] = React.useState({
    reasonCode: selectedCode?.value ?? '',
    description: selectedCode?.description ?? ''
  });

  const values = useSelectFormValue(DOF_REASON_CODE_NAME);

  const modalViewRef = useRef<any>(null);

  const formValid = useSelector(isValid(DOF_REASON_CODE_NAME));

  const isLoading = useSelector(loadingReasonCodeConfiguration);

  const isDirtyFrom = useIsDirtyForm([DOF_REASON_CODE_NAME]);

  const { t } = useTranslation();

  const dynamicProps: any = {
    add: {
      title: t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.ADD_REASON_CODE),
      variant: 'primary',
      footer: t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.SUBMIT)
    },
    edit: {
      title: t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.EDIT_REASON_CODE),
      variant: 'primary',
      footer: t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.SAVE)
    },
    delete: {
      title: t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.DELETE_REASON_CODE),
      footer: t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.DELETE),
      variant: 'danger',
      content: t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.MODAL_DELETE_CONTENT)
    }
  };
  const handleClickRef = useRef<Record<string, any>>({});
  handleClickRef.current.values = values;
  handleClickRef.current.modalType = modalType;
  handleClickRef.current.selectedCode = selectedCode;

  !isFunction(handleClickRef.current.handleClick) &&
    (handleClickRef.current.handleClick = debounce(() => {
      const {
        values: valuesRef,
        modalType: modalTypeRef,
        selectedCode: selectedCodeRef
      } = handleClickRef.current;

      if (modalTypeRef === 'add') {
        dispatch(accountExternalStatusActions.setAddReasonCodeErrorMessage(''));
        dispatch(
          accountExternalStatusActions.addReasonCode({
            reasonCode: valuesRef?.reasonCode,
            description: valuesRef?.description,
            checkList: { display: { checked: true } }
          })
        );
      }

      if (modalTypeRef === 'delete' && selectedCodeRef) {
        const { value } = selectedCodeRef;
        dispatch(
          accountExternalStatusActions.deleteReasonCode({
            id: value
          })
        );
        dispatch(accountExternalStatusActions.setAddReasonCodeErrorMessage(''));
      }

      if (modalTypeRef === 'edit' && selectedCodeRef) {
        const { value } = selectedCodeRef;
        dispatch(accountExternalStatusActions.setAddReasonCodeErrorMessage(''));
        dispatch(
          accountExternalStatusActions.editReasonCode({
            id: value,
            reasonCode: valuesRef?.reasonCode,
            description: valuesRef?.description
          })
        );
      }
    }, 500));

  const isDeleteContent = modalType === 'delete';

  const setViewProps = () => {
    const reasonCodeFields: Field<ExtraFieldProps> =
      modalViewRef?.current?.props?.onFind('reasonCode');

    if (errorMessage && isEmpty(values?.reasonCode)) return;

    if (errorMessage) {
      reasonCodeFields?.props?.props?.setOthers((prev: MagicKeyValue) => {
        reasonCodeFields.props?.props?.setOthers({
          ...prev,
          options: { ...prev.options, isError: true }
        });
      });
    }
  };

  useEffect(() => {
    if (modalViewRef.current) {
      return setViewProps();
    }
  });

  return (
    <>
      <Modal
        xs={isDeleteContent}
        loading={isLoading}
        sm={!isDeleteContent}
        show={show}
        dataTestId={testId}
      >
        <ModalHeader
          border
          closeButton
          onHide={toggle}
          dataTestId={`${testId}_header`}
        >
          <ModalTitle dataTestId={`${testId}_title`}>
            {dynamicProps[modalType].title}
          </ModalTitle>
        </ModalHeader>
        <ModalBodyWithErrorClientConfig
          apiErrorClassName="mb-24"
          dataTestId={`${testId}_body`}
        >
          {errorMessage && (
            <InlineMessage
              className="mb-24"
              variant="danger"
              dataTestId={`${testId}_errorMessage`}
            >
              {t(errorMessage)}
            </InlineMessage>
          )}
          {isDeleteContent ? (
            <>
              <div
                className="mb-6"
                data-testid={genAmtId(testId, 'content', '')}
              >
                {dynamicProps[modalType].content}
              </div>
              <div data-testid={genAmtId(testId, 'reasonCodeNormal', '')}>
                {t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.REASON_CODE_NORMAL)}:{' '}
                <strong>{`${selectedCode?.value} - ${selectedCode?.description}`}</strong>
              </div>
            </>
          ) : (
            <View
              id={DOF_REASON_CODE_NAME}
              formKey={DOF_REASON_CODE_NAME}
              descriptor={DOF_REASON_CODE_NAME}
              value={initialData}
              ref={modalViewRef}
            />
          )}
        </ModalBodyWithErrorClientConfig>
        <ModalFooter dataTestId={`${testId}_footer`}>
          <div className="d-flex align-items-right justify-content-end">
            <Button
              onClick={toggle}
              variant="secondary"
              dataTestId={`${testId}_cancelBtn`}
            >
              {t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.CANCEL)}
            </Button>
            <Button
              disabled={!isDeleteContent && (!formValid || !isDirtyFrom)}
              onClick={handleClickRef.current.handleClick}
              variant={dynamicProps[modalType].variant}
              dataTestId={`${testId}_okBtn`}
            >
              {dynamicProps[modalType].footer}
            </Button>
          </div>
        </ModalFooter>
      </Modal>
    </>
  );
};

export default ReasonCodeModal;
