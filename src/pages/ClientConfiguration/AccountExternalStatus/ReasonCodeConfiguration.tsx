import React, { useCallback, useMemo } from 'react';
import {
  Grid,
  Pagination,
  ColumnType,
  TruncateText
} from 'app/_libraries/_dls/components';

import { useTranslation } from 'app/_libraries/_dls/hooks';

// Components
import CheckBoxGrid from '../AccountExternalStatus/CheckBoxGrid';
import { NoDataGrid } from 'app/components';
import AddReasonCodeButton from './AddReasonCodeButton';
import Actions from '../AccountExternalStatus/Actions';

// Constants
import { I18N_ACCOUNT_EXTERNAL_STATUS_TEXT } from './constants';

// Actions

// Hooks
import { useDataGridCheckBox, useBehaviorApi } from './hooks';
import { usePagination } from 'app/hooks';

// Helper
import { ACC_EXTERNAL_STATUS, REASON_CODE } from '../helpers';
import { getValueAndDescription } from './helpers';
import { ReasonCodesDataType } from './types';
import { ClientConfigStatusProps } from '../types';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { codeToTextActions } from '../CodeToText/_redux/reducers';
import { useDispatch } from 'react-redux';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

const ReasonCodeConfiguration: React.FC<ClientConfigStatusProps> = ({
  status
}) => {
  const { reasonCodeList: listData, accountExternalStatusList } =
    useBehaviorApi();

  const { data: accExtSttData } = useDataGridCheckBox(
    accountExternalStatusList,
    ACC_EXTERNAL_STATUS
  );

  const { t } = useTranslation();
  const dispatch = useDispatch();
  const testId = 'clientConfig_accountExternal_reasonCodeConfig';

  const { handleChange } = useDataGridCheckBox(listData, REASON_CODE);

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(listData);

  const showPagination = total > pageSize[0];

  const handleChangeTabToCodeToText = useCallback(() => {
    // set multiple codetotext dropdown value
    const listCode = ['accountExternalStatus'];
    dispatch(
      codeToTextActions.selectedDropdown({
        data: [
          {
            code: 'accountExternalStatus',
            description: 'Account External Status'
          }
        ],
        listCode
      })
    );
    dispatch(codeToTextActions.setSelectedCodeToTextTypes(listCode));

    const codeToTextElm = document.querySelector('a[data-rb-event-key="CTT"]');
    (codeToTextElm as HTMLElement)?.click();
  }, [dispatch]);

  const columns = useMemo(() => {
    return [
      ...accountExternalStatusList.map(item => {
        return {
          id: item.value,
          Header: getValueAndDescription(item),
          cellProps: { className: 'text-center' },
          accessor: function accessor(data: ReasonCodesDataType, idx: number) {
            return (
              <CheckBoxGrid
                status={status}
                columnId={item.value}
                data={data}
                onChange={handleChange}
                disabledAll={
                  !data.checkList?.display || !data.checkList?.display?.checked
                }
                dataTestId={`${item.value}_${idx}_${testId}`}
              />
            );
          } as any,
          width: 161
        };
      }),
      {
        id: 'action',
        Header: t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.ACTIONS),
        width: 150,
        cellProps: { className: 'text-center' },
        cellBodyProps: { className: 'td-sm' },
        accessor: (data: MagicKeyValue, idx: number) => (
          <Actions
            status={status}
            data={data}
            dataTestId={`${idx}_${testId}`}
          />
        )
      }
    ];
  }, [accountExternalStatusList, handleChange, t, status]);

  const ReasonCodeColumns: ColumnType[] = useMemo(() => {
    const gridTemplate: ColumnType[] = [
      {
        id: 'reasonCode',
        className: 'text-break',
        Header: t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.REASON_CODE),
        accessor: acc => (
          <TruncateText
            lines={2}
            title={getValueAndDescription(acc)}
            resizable
            ellipsisMoreText={t(I18N_COMMON_TEXT.MORE)}
            ellipsisLessText={t(I18N_COMMON_TEXT.LESS)}
            className="text-break"
          >
            {getValueAndDescription(acc)}
          </TruncateText>
        ),
        isFixedLeft: true,
        autoWidth: true,
        width: 256
      },
      {
        id: 'display',
        Header: t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.DISPLAY),
        cellBodyProps: { className: 'text-center' },
        accessor: function accessor(data, idx) {
          return (
            <CheckBoxGrid
              status={status}
              columnId={'display'}
              data={data}
              onChange={handleChange}
              dataTestId={`${idx}_display_${testId}`}
            />
          );
        },
        isFixedLeft: true,
        width: 87
      }
    ];
    if (accExtSttData.length) {
      gridTemplate.push({
        id: 'selectedStatus',
        Header: t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.SELECTED_STATUS),
        accessor: 'selectedStatus',
        columns
      });
    } else {
      gridTemplate.push({
        id: 'selectedStatus',
        cellBodyProps: { className: 'bg-light-l20' },
        cellHeaderProps: { className: 'text-center' },
        Header: t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.SELECTED_STATUS),
        accessor: function accessor(data, idx) {
          if (idx === 0) {
            return (
              <p className="color-grey">
                {t('txt_no_data_config_the_acc')}
                <span
                  onClick={handleChangeTabToCodeToText}
                  className="mx-4 link text-decoration-none"
                >
                  {t('txt_code_to_txt')}.
                </span>
              </p>
            );
          }
          return ' '; // dont remove this space
        },
        width: 520
      });
      gridTemplate.push({
        id: 'action',
        Header: t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.ACTIONS),
        width: 135,
        cellProps: { className: 'text-center border-left' },
        cellBodyProps: { className: 'td-sm' },
        accessor: (data: MagicKeyValue, idx: number) => (
          <Actions
            status={status}
            data={data}
            dataTestId={`${idx}_${testId}`}
          />
        )
      });
    }
    return gridTemplate;
  }, [
    t,
    accExtSttData.length,
    status,
    handleChange,
    columns,
    handleChangeTabToCodeToText
  ]);

  if (!accExtSttData.length && !listData.length) {
    return (
      <NoDataGrid
        text={
          <p className="mt-20 color-grey">
            {t('txt_no_data_config_the_acc')}
            <span
              onClick={handleChangeTabToCodeToText}
              className="ml-4 link text-decoration-none"
            >
              {t('txt_code_to_txt')}.
            </span>
          </p>
        }
        dataTestId={`${testId}_noData`}
      />
    );
  }

  if (!listData.length) {
    return (
      <>
        <p
          className="mt-8 mb-16"
          data-testid={genAmtId(testId, 'reasonCodeDescription', '')}
        >
          {t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.REASON_CODE_DESCRIPTION)}
        </p>
        <NoDataGrid text={t('txt_no_data')} dataTestId={`${testId}_noData`}>
          <div className="mt-24 mr-10">
            <AddReasonCodeButton status={status} />
          </div>
        </NoDataGrid>
      </>
    );
  }

  return (
    <>
      <p
        className="mt-8 mb-16"
        data-testid={genAmtId(testId, 'reasonCodeDescription', '')}
      >
        {t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.REASON_CODE_DESCRIPTION)}
      </p>
      <Grid
        columns={ReasonCodeColumns}
        data={gridData}
        staticHeaderId="selectedStatus"
        alwaysBorderFixedColumn
        dataTestId={`${testId}_grid`}
      />
      {showPagination && (
        <div className="pt-16">
          <Pagination
            totalItem={total}
            pageSize={pageSize}
            pageNumber={currentPage}
            pageSizeValue={currentPageSize}
            compact
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
            dataTestId={`${testId}_pagination`}
          />
        </div>
      )}
    </>
  );
};

export default ReasonCodeConfiguration;
