import React, { useRef } from 'react';

import { renderMockStore, mockActionCreator } from 'app/test-utils';

//hooks
import useDataGridCheckBox from './useDataGridCheckBox';
import CheckBoxGrid from '../../AccountExternalStatus/CheckBoxGrid';
//types
import { ColumnType, Grid } from 'app/_libraries/_dls/components';
//react-testing-lib
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

//actions
import { accountExternalStatusActions } from '../_redux/reducers';

jest.mock('app/_libraries/_dls/utils/genAmtId', () => () => '');

const mockData = [
  { id: 'normal', value: 'value', currentStatus: 'Normal', checkList: {} },
  { id: 'id', currentStatus: 'Normal' }
];

const mockAction = mockActionCreator(accountExternalStatusActions);

const withData: Partial<RootState> = {
  accountExternalStatus: {
    data: {
      reasonCode: [],
      externalCode: []
    },
    isUpdating: false,
    accountExternalStatusList: {
      data: mockData,
      error: '',
      loading: false
    },
    reasonCodeList: {}
  }
};

describe('should testing useDataGridCheckBox', () => {
  it('useDataGridCheckBox with empty type', () => {
    mockAction('updateAccountExternalList');
    const Component = () => {
      const { data, handleChange } = useDataGridCheckBox(mockData, '');

      const AccountExternalStatusColumns: ColumnType[] = React.useMemo(
        () => [
          {
            id: 'currentStatus',
            Header: 'test',
            accessor: 'currentStatus',
            isFixedLeft: true,
            width: 256,
            autoWidth: true
          },
          {
            id: 'availableStatus',
            Header: 'test2',
            accessor: 'availableStatus',
            cellProps: { className: 'text-center' },
            columns: [{ id: 'normal', label: 'Normal', width: 92 }].map(
              ({ id, label, width }) => {
                return {
                  id,
                  Header: label,
                  cellProps: { className: 'text-center' },
                  accessor: function accessor(item) {
                    return (
                      <CheckBoxGrid
                        columnId={id}
                        data={item}
                        onChange={handleChange}
                      />
                    );
                  },
                  width
                };
              }
            )
          }
        ],
        [handleChange]
      );
      return (
        <div>
          <Grid
            columns={AccountExternalStatusColumns}
            data={data}
            staticHeaderId="availableStatus"
            alwaysBorderFixedColumn
          />
        </div>
      );
    };

    const renderWrapper = (initialState: Partial<RootState>) => {
      return renderMockStore(<Component />, { initialState });
    };

    renderWrapper(withData);

    const checkBox = screen.getAllByRole('checkbox');

    userEvent.click(checkBox[0]);

    expect(checkBox[0]).toBeInTheDocument();
  });

  it('useDataGridCheckBox with AccountExternalStatus type', () => {
    mockAction('updateAccountExternalList');
    const Component = () => {
      const { data, handleChange } = useDataGridCheckBox(mockData, 'AES');

      const AccountExternalStatusColumns: ColumnType[] = React.useMemo(
        () => [
          {
            id: 'currentStatus',
            Header: 'test',
            accessor: 'currentStatus',
            isFixedLeft: true,
            width: 256,
            autoWidth: true
          },
          {
            id: 'availableStatus',
            Header: 'test2',
            accessor: 'availableStatus',
            cellProps: { className: 'text-center' },
            columns: [{ id: 'normal', label: 'Normal', width: 92 }].map(
              ({ id, label, width }) => {
                return {
                  id,
                  Header: label,
                  cellProps: { className: 'text-center' },
                  accessor: function accessor(item) {
                    return (
                      <CheckBoxGrid
                        columnId={id}
                        data={item}
                        onChange={handleChange}
                      />
                    );
                  },
                  width
                };
              }
            )
          }
        ],
        [handleChange]
      );
      return (
        <div>
          <Grid
            columns={AccountExternalStatusColumns}
            data={data}
            staticHeaderId="availableStatus"
            alwaysBorderFixedColumn
          />
        </div>
      );
    };

    const renderWrapper = (initialState: Partial<RootState>) => {
      return renderMockStore(<Component />, { initialState });
    };

    renderWrapper(withData);

    const checkBox = screen.getAllByRole('checkbox');

    userEvent.click(checkBox[0]);

    expect(checkBox[0]).toBeInTheDocument();
  });

  it('useDataGridCheckBox with ReasonCode type', () => {
    mockAction('updateReasonCodeExternalList');
    const Component = () => {
      const { data, handleChange } = useDataGridCheckBox(mockData, 'RC');

      const AccountExternalStatusColumns: ColumnType[] = React.useMemo(
        () => [
          {
            id: 'currentStatus',
            Header: 'test',
            accessor: 'currentStatus',
            isFixedLeft: true,
            width: 256,
            autoWidth: true
          },
          {
            id: 'availableStatus',
            Header: 'test2',
            accessor: 'availableStatus',
            cellProps: { className: 'text-center' },
            columns: [{ id: 'normal', label: 'Normal', width: 92 }].map(
              ({ id, label, width }) => {
                return {
                  id,
                  Header: label,
                  cellProps: { className: 'text-center' },
                  accessor: function accessor(item) {
                    return (
                      <CheckBoxGrid
                        columnId={id}
                        data={item}
                        onChange={handleChange}
                      />
                    );
                  },
                  width
                };
              }
            )
          }
        ],
        [handleChange]
      );
      return (
        <Grid
          columns={AccountExternalStatusColumns}
          data={data}
          staticHeaderId="availableStatus"
          alwaysBorderFixedColumn
        />
      );
    };

    const renderWrapper = (initialState: Partial<RootState>) => {
      return renderMockStore(<Component />, { initialState });
    };

    renderWrapper(withData);

    const checkBox = screen.getAllByRole('checkbox');

    userEvent.click(checkBox[0]);

    expect(checkBox[0]).toBeInTheDocument();
  });

  it('useDataGridCheckBox with empty data ', () => {
    mockAction('updateReasonCodeExternalList');
    const Component = () => {
      const keepRef = useRef([]);
      const { data, handleChange } = useDataGridCheckBox(keepRef.current, 'RC');

      const AccountExternalStatusColumns: ColumnType[] = React.useMemo(
        () => [
          {
            id: 'currentStatus',
            Header: 'test',
            accessor: 'currentStatus',
            isFixedLeft: true,
            width: 256,
            autoWidth: true
          },
          {
            id: 'availableStatus',
            Header: 'test2',
            accessor: 'availableStatus',
            cellProps: { className: 'text-center' },
            columns: [{ id: 'normal', label: 'Normal', width: 92 }].map(
              ({ id, label, width }) => {
                return {
                  id,
                  Header: label,
                  cellProps: { className: 'text-center' },
                  accessor: function accessor(item) {
                    return (
                      <CheckBoxGrid
                        columnId={id}
                        data={item}
                        onChange={handleChange}
                      />
                    );
                  },
                  width
                };
              }
            )
          }
        ],
        [handleChange]
      );

      return (
        <Grid
          columns={AccountExternalStatusColumns}
          data={data}
          staticHeaderId="availableStatus"
          alwaysBorderFixedColumn
        />
      );
    };

    const renderWrapper = (initialState: Partial<RootState>) => {
      return renderMockStore(<Component />, { initialState });
    };

    renderWrapper(withData);

    const table = screen.getByRole('table');

    expect(table).toBeInTheDocument();
  });
});
