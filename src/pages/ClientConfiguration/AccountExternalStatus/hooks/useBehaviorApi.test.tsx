import React from 'react';
import { screen } from '@testing-library/dom';
import { renderMockStore } from 'app/test-utils';

import useBehaviorApi from './useBehaviorApi';

const Comp = () => {
  const behavior = useBehaviorApi();

  return (
    <div>
      <p data-testid="accountExternalStatusError">
        {JSON.stringify(behavior.accountExternalStatusError)}
      </p>
      <p data-testid="accountExternalStatusList">
        {JSON.stringify(behavior.accountExternalStatusList)}
      </p>
      <p data-testid="accountExternalStatusLoading">
        {JSON.stringify(behavior.accountExternalStatusLoading)}
      </p>
      <p data-testid="reasonCodeList">
        {JSON.stringify(behavior.reasonCodeList)}
      </p>
      <p data-testid="reasonCodeLoading">
        {JSON.stringify(behavior.reasonCodeLoading)}
      </p>
      <p data-testid="reasonCodeModalErrorMessage">
        {JSON.stringify(behavior.reasonCodeModalErrorMessage)}
      </p>
    </div>
  );
};

const initialState: Partial<RootState> = {
  accountExternalStatus: {
    reasonCodeList: { data: [], modalErrorMessage: 'error' },
    error: 'error',
    loading: true,
    accountExternalStatusList: { data: [] },
    isUpdating: true,
    data: {
      externalCode: [],
      reasonCode: []
    }
  }
};

const renderWrapper = (state: Partial<RootState>) => {
  return renderMockStore(<Comp />, { initialState: state });
};

describe('useBehaviorApi', () => {
  it('should return data', () => {
    renderWrapper(initialState);

    const account = initialState.accountExternalStatus!;
    expect(
      JSON.parse(screen.getByTestId('accountExternalStatusError').textContent!)
    ).toEqual(account.error);
    expect(
      JSON.parse(screen.getByTestId('accountExternalStatusList').textContent!)
    ).toEqual(account.accountExternalStatusList.data);
    expect(
      JSON.parse(
        screen.getByTestId('accountExternalStatusLoading').textContent!
      )
    ).toEqual(account.loading);
    expect(
      JSON.parse(screen.getByTestId('reasonCodeList').textContent!)
    ).toEqual(account.reasonCodeList.data);
    expect(
      JSON.parse(screen.getByTestId('reasonCodeLoading').textContent!)
    ).toEqual(account.loading);
    expect(
      JSON.parse(screen.getByTestId('reasonCodeModalErrorMessage').textContent!)
    ).toEqual(account.reasonCodeList.modalErrorMessage);
  });
});
