import cloneDeep from 'lodash.clonedeep';
import isEmpty from 'lodash.isempty';
import {
  ACC_EXTERNAL_STATUS,
  REASON_CODE
} from 'pages/ClientConfiguration/helpers';
import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { AccountExternalStatusDataType, ReasonCodesDataType } from '../types';

// Redux
import { accountExternalStatusActions } from '../_redux/reducers';

const useDataGridCheckBox = (
  initialData: Array<AccountExternalStatusDataType | ReasonCodesDataType>,
  type: string
) => {
  const dispatch = useDispatch();
  const [data, setData] = useState<
    Array<AccountExternalStatusDataType | ReasonCodesDataType>
  >([]);

  useEffect(() => {
    if (isEmpty(initialData)) return setData([]);
    setData(initialData);
  }, [initialData]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { id, checked } = event.target;

    const [columnId, rowId] = id.split('_');

    const updateData = data.map(row => {
      const newRow = cloneDeep(row);
      if (row.value === rowId) {
        newRow.checkList = {
          ...newRow.checkList,
          [columnId]: {
            checked: checked
          }
        };
      }
      return newRow;
    });
    setData(updateData);

    if (type === ACC_EXTERNAL_STATUS) {
      return dispatch(
        accountExternalStatusActions.updateAccountExternalList({
          data: updateData
        })
      );
    }
    if (type === REASON_CODE) {
      return dispatch(
        accountExternalStatusActions.updateReasonCodeExternalList({
          data: updateData
        })
      );
    }
  };

  return {
    data,
    handleChange
  };
};

export default useDataGridCheckBox;
