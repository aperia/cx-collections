import { useSelector } from 'react-redux';

//selectors
import {
  selectedReasonCodeConfigurationData,
  selectedAccountExternalStatusData,
  selectedReasonCodeConfigurationListModalErrorMessage,
  selectGetAccountExternalStatusLoading,
  selectedAccountExternalStatusListError,
  selectedReasonCodeConfigurationSortBy
} from '../_redux/selectors';

const useBehaviorApi = () => {
  const reasonCodeList = useSelector(selectedReasonCodeConfigurationData);
  const reasonCodeSortby = useSelector(selectedReasonCodeConfigurationSortBy);

  const reasonCodeLoading = useSelector(selectGetAccountExternalStatusLoading);

  const reasonCodeModalErrorMessage = useSelector(
    selectedReasonCodeConfigurationListModalErrorMessage
  );

  const accountExternalStatusList = useSelector(
    selectedAccountExternalStatusData
  );

  const accountExternalStatusLoading = useSelector(
    selectGetAccountExternalStatusLoading
  );

  const accountExternalStatusError = useSelector(
    selectedAccountExternalStatusListError
  );

  return {
    reasonCodeList,
    reasonCodeSortby,
    reasonCodeLoading,
    reasonCodeModalErrorMessage,
    accountExternalStatusList,
    accountExternalStatusLoading,
    accountExternalStatusError
  };
};

export default useBehaviorApi;
