import React from 'react';

//test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';

//components
import AccountExternalStatusGrid from './AccountExternalStatusGrid';

//actions
import { accountExternalStatusActions } from './_redux/reducers';
import { codeToTextActions } from '../CodeToText/_redux/reducers';

jest.mock('app/_libraries/_dls/utils/genAmtId', () => () => '');

const mockAccountExternalActions = mockActionCreator(
  accountExternalStatusActions
);

const mockCodeToTextActions = mockActionCreator(codeToTextActions);

describe('Render UI', () => {
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<AccountExternalStatusGrid status={false} />, {
      initialState
    });
  };

  it('should render table', () => {
    const mockGetAccountStatusCodeList = mockAccountExternalActions(
      'getAccountStatusCodeList'
    );

    const withData: Partial<RootState> = {
      accountExternalStatus: {
        data: {
          reasonCode: [],
          externalCode: []
        },
        accountExternalStatusList: {
          data: [
            {
              availableStatus: '11'
            }
          ]
        },
        isUpdating: false,
        reasonCodeList: {}
      }
    };

    renderWrapper(withData);
    expect(mockGetAccountStatusCodeList).toBeCalled();
  });

  it('handleReload', () => {
    const mockGetAccountStatusCodeList = mockAccountExternalActions(
      'getAccountStatusCodeList'
    );

    const withData: Partial<RootState> = {
      accountExternalStatus: {
        data: {
          reasonCode: [],
          externalCode: []
        },
        isUpdating: false,
        accountExternalStatusList: {
          data: [],
          loading: false
        },
        error: 'Error',
        reasonCodeList: {}
      }
    };

    renderWrapper(withData);

    const queryButton = screen.getByText('txt_reload');
    queryButton!.click();

    expect(queryButton).toBeInTheDocument();
    expect(mockGetAccountStatusCodeList).toBeCalled();
  });

  it('handleChangeTabToCodeToText', () => {
    const setSelectedCodeToTextTypes = mockCodeToTextActions(
      'setSelectedCodeToTextTypes'
    );

    const withData: Partial<RootState> = {
      accountExternalStatus: {
        data: {
          reasonCode: [],
          externalCode: []
        },
        isUpdating: false,
        accountExternalStatusList: {
          data: [],
          loading: false
        },
        reasonCodeList: {}
      }
    };

    const { wrapper } = renderWrapper(withData);
    const reloadBtn = wrapper.queryByText(/txt_code_to_txt/)!;
    reloadBtn!.click();

    expect(setSelectedCodeToTextTypes).toBeCalledWith([
      'accountExternalStatus'
    ]);
  });
});
