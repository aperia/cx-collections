import React from 'react';

// components
import { Button } from 'app/_libraries/_dls/components';

//utils
import { useDispatch } from 'react-redux';

//redux
import { useTranslation } from 'app/_libraries/_dls/hooks';

//reducers
import { accountExternalStatusActions } from '../AccountExternalStatus/_redux/reducers';

//constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

export type dataActionType = {
  id?: string;
  reasonCode?: string;
};

export interface ActionsProps extends DLSId {
  data: dataActionType;
  status: boolean;
}

const Actions: React.FC<ActionsProps> = ({ data, status, dataTestId }) => {
  const dispatch = useDispatch();

  const { t } = useTranslation();

  const handleEdit = () => {
    dispatch(
      accountExternalStatusActions.toggleReasonCode({
        modalType: 'edit',
        selectedCode: data
      })
    );
  };

  const handleDelete = () =>
    dispatch(
      accountExternalStatusActions.toggleReasonCode({
        modalType: 'delete',
        selectedCode: data
      })
    );

  return (
    <>
      <Button
        disabled={status}
        variant="outline-danger"
        size="sm"
        onClick={handleDelete}
        dataTestId={`${dataTestId}_deleteBtn`}
      >
        {t(I18N_COMMON_TEXT.DELETE)}
      </Button>
      <Button
        disabled={status}
        variant="outline-primary"
        size="sm"
        onClick={handleEdit}
        dataTestId={`${dataTestId}_editBtn`}
      >
        {t(I18N_COMMON_TEXT.EDIT)}
      </Button>
    </>
  );
};

export default Actions;
