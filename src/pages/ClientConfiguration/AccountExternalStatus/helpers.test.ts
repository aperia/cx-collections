import * as helpers from './helpers';
import { ReasonCodesDataType } from './types';

describe('AccountExternalStatus helpers', () => {
  it('getCheckListData', () => {
    const data: ReasonCodesDataType[] = [
      { value: 'value 1', description: 'description 1' },
      { value: 'value 2', description: 'description 2', checkList: { ky: {} } }
    ];

    const result = helpers.getCheckListData(data);

    expect(result).toEqual([]);
  });

  it('getValueAndDescription', () => {
    let result = helpers.getValueAndDescription({
      value: ' ',
      description: 'description'
    });
    expect(result).toEqual('description');

    result = helpers.getValueAndDescription({
      value: 'value',
      description: 'description'
    });
    expect(result).toEqual('value - description');
  });
});
