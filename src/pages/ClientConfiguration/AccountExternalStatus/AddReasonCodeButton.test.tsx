import React from 'react';

//test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

//components
import AddReasonCodeButton from './AddReasonCodeButton';

//actions
import { accountExternalStatusActions } from './_redux/reducers';

//constants
import { I18N_ACCOUNT_EXTERNAL_STATUS_TEXT } from './constants';

const mockAccountExternalActions = mockActionCreator(
  accountExternalStatusActions
);
describe('Render UI', () => {
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<AddReasonCodeButton />, { initialState });
  };

  it('should render add modal and dispatch action open add modal ', () => {
    const mockAction = mockAccountExternalActions('toggleReasonCode');

    renderWrapper({});

    const button = screen.getByRole('button', {
      name: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.ADD_REASON_CODE
    });

    expect(button).toBeInTheDocument();

    userEvent.click(button);

    expect(mockAction).toBeCalledWith({ modalType: 'add' });
  });
});
