import React from 'react';

//components
import { Button } from 'app/_libraries/_dls/components';

//hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

//constants
import { I18N_ACCOUNT_EXTERNAL_STATUS_TEXT } from './constants';

//actions
import { accountExternalStatusActions } from './_redux/reducers';

//redux
import { useDispatch } from 'react-redux';
import { ClientConfigStatusProps } from '../types';

const AddReasonCodeButton: React.FC<ClientConfigStatusProps> = ({ status }) => {
  const { t } = useTranslation();
  const testId = 'clientConfig_accountExternal_addReasonCodeBtn';

  const dispatch = useDispatch();

  return (
    <Button
      disabled={status}
      className="mr-n8"
      onClick={() =>
        dispatch(
          accountExternalStatusActions.toggleReasonCode({ modalType: 'add' })
        )
      }
      size="sm"
      variant="outline-primary"
      dataTestId={testId}
    >
      {t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.ADD_REASON_CODE)}
    </Button>
  );
};

export default AddReasonCodeButton;
