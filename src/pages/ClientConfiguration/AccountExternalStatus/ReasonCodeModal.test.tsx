import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import ReasonCodeModal from './ReasonCodeModal';

//actions
import { accountExternalStatusActions } from './_redux/reducers';

//constants
import { I18N_ACCOUNT_EXTERNAL_STATUS_TEXT } from './constants';

import * as useSelectFormValue from 'app/hooks/useSelectFormValue';

import * as reduxForm from 'redux-form';

jest.mock('app/_libraries/_dls/utils/genAmtId', () => () => '');

jest.mock('lodash.debounce', () => {
  const entries = [{ contentRect: {} }];

  return (fn: Function) => () => fn(entries);
});

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const mockAccountExternalActions = mockActionCreator(
  accountExternalStatusActions
);

describe('Render UI', () => {
  const withSelectedCode: Partial<RootState> = {
    accountExternalStatus: {
      data: {
        reasonCode: [],
        externalCode: []
      },
      isUpdating: false,
      accountExternalStatusList: {
        loading: true
      },
      error: '',
      reasonCodeList: {
        data: [],
        modalErrorMessage: '',
        modalLoading: false,
        modalType: '',
        open: false,
        selectedCode: {
          value: 'selectedCode',
          description: 'description'
        }
      }
    }
  };

  const withErrorMessage: Partial<RootState> = {
    accountExternalStatus: {
      data: {
        reasonCode: [],
        externalCode: []
      },
      isUpdating: false,
      accountExternalStatusList: {},
      reasonCodeList: {
        data: [],
        modalErrorMessage: 'txt_reason_code_duplicate_message',
        modalLoading: false,
        modalType: '',
        open: false,
        selectedCode: {
          value: 'selectedCode',
          description: 'description'
        }
      }
    }
  };
  const mockToggle = jest.fn();

  const renderWrapper = (
    initialState: Partial<RootState>,
    modalType: 'add' | 'edit' | 'delete'
  ) => {
    return renderMockStore(
      <ReasonCodeModal modalType={modalType} show={true} toggle={mockToggle} />,
      { initialState }
    );
  };

  it('render ui with reason code add modal', () => {
    renderWrapper({}, 'add');

    const addReasonCodeText = screen.getByText(
      I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.ADD_REASON_CODE
    );

    expect(addReasonCodeText).toBeInTheDocument();
  });

  it('render ui with reason code edit modal', () => {
    renderWrapper({}, 'edit');

    const editReasonCodeText = screen.getByText(
      I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.EDIT_REASON_CODE
    );

    expect(editReasonCodeText).toBeInTheDocument();
  });

  it('render ui with reason code delete modal', () => {
    renderWrapper({}, 'delete');

    const deleteReasonCodeText = screen.getByText(
      I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.DELETE_REASON_CODE
    );

    expect(deleteReasonCodeText).toBeInTheDocument();
  });

  it('when click cancel button in modal', () => {
    renderWrapper({}, 'add');

    const cancelButton = screen.getByRole('button', {
      name: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.CANCEL
    });

    expect(cancelButton).toBeInTheDocument();

    userEvent.click(cancelButton);

    expect(mockToggle).toHaveBeenCalled();
  });

  describe('handleClick', () => {
    const formData = {
      reasonCode: 'reasonCode',
      description: 'description',
      checkList: { display: { checked: true } }
    };

    let spy: jest.SpyInstance;
    beforeEach(() => {
      spy = jest
        .spyOn(useSelectFormValue, 'useSelectFormValue')
        .mockImplementation(() => formData);
    });

    afterEach(() => {
      spy?.mockReset();
      spy?.mockRestore();
    });

    it('when click submit button without filling form in modal', () => {
      const isDirtySpy = jest
        .spyOn(reduxForm, 'isDirty')
        .mockImplementation(() => () => true);
      const mockAction = mockAccountExternalActions('addReasonCode');

      renderWrapper({}, 'add');

      const editButton = screen.getByRole('button', {
        name: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.SUBMIT
      });

      expect(editButton).toBeInTheDocument();

      userEvent.click(editButton);

      expect(mockAction).toBeCalledWith({
        reasonCode: formData.reasonCode,
        description: formData.description,
        checkList: formData.checkList
      });
      isDirtySpy.mockReset();
      isDirtySpy.mockRestore();
    });

    it('when click submit button in delete modal', () => {
      const mockAction = mockAccountExternalActions('deleteReasonCode');

      const mockSetAddReasonCodeErrorMessage = mockAccountExternalActions(
        'setAddReasonCodeErrorMessage'
      );

      renderWrapper(withSelectedCode, 'delete');

      const deleteButton = screen.getByRole('button', {
        name: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.DELETE
      });

      expect(deleteButton).toBeInTheDocument();

      userEvent.click(deleteButton);

      expect(mockAction).toBeCalledWith({ id: 'selectedCode' });

      expect(mockSetAddReasonCodeErrorMessage).toBeCalledWith('');
    });

    it('when click submit button in edit modal', () => {
      const isDirtySpy = jest
        .spyOn(reduxForm, 'isDirty')
        .mockImplementation(() => () => true);
      const mockEditAction = mockAccountExternalActions('editReasonCode');

      renderWrapper(withSelectedCode, 'edit');

      const editButton = screen.getByRole('button', {
        name: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.SAVE
      });

      expect(editButton).toBeInTheDocument();

      userEvent.click(editButton);

      expect(mockEditAction).toBeCalled();
      isDirtySpy.mockReset();
      isDirtySpy.mockRestore();
    });

    it('should render when errorMessage and values is empty', () => {
      spy = jest
        .spyOn(useSelectFormValue, 'useSelectFormValue')
        .mockImplementation(() => ({}));

      const { rerender } = renderWrapper(withErrorMessage, 'add');

      // rerender to trigger useEffect
      rerender(
        <ReasonCodeModal modalType={'add'} show={true} toggle={mockToggle} />
      );

      expect(
        screen.getByText('txt_reason_code_duplicate_message')
      ).toBeInTheDocument();
    });

    it('should render errorMessage ', () => {
      const { rerender } = renderWrapper(withErrorMessage, 'add');

      // rerender to trigger useEffect
      rerender(
        <ReasonCodeModal modalType={'add'} show={true} toggle={mockToggle} />
      );

      expect(
        screen.getByText('txt_reason_code_duplicate_message')
      ).toBeInTheDocument();
    });

    it('should render without errorMessage ', () => {
      const { rerender } = renderWrapper({}, 'add');

      // rerender to trigger useEffect
      rerender(
        <ReasonCodeModal modalType={'add'} show={true} toggle={mockToggle} />
      );

      expect(
        screen.queryByText('txt_reason_code_duplicate_message')
      ).toBeNull();
    });
  });
});
