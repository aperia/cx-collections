// services
import { accountExternalStatusServices } from './accountExternalStatusServices';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import { ExternalStatusPayload, ReasonCodeRequest } from './types';

describe('accountExternalStatusServices', () => {
  describe('saveAccountExternalInfo', () => {
    const params: ExternalStatusPayload = {
      externalCode: [{ value: 'E7D14CXH', description: 'a' }],
      reasonCode: [{ value: 'E7D14CXH', description: 'b' }]
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      accountExternalStatusServices.saveAccountExternalInfo(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      accountExternalStatusServices.saveAccountExternalInfo(params);

      expect(mockService).toBeCalledWith(
        apiUrl.clientConfigAccountExternalStatus.saveExternalStatus,
        params
      );
    });
  });

  describe('addReasonCodeConfigList', () => {
    const params: ReasonCodeRequest = { id: 'E7D14CXH' };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      accountExternalStatusServices.addReasonCodeConfigList(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      accountExternalStatusServices.addReasonCodeConfigList(params);

      expect(mockService).toBeCalledWith(
        apiUrl.clientConfigAccountExternalStatus.addReasonCode,
        params
      );
    });
  });

  describe('editReasonCodeConfigList', () => {
    const params: ReasonCodeRequest = { id: 'E7D14CXH' };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      accountExternalStatusServices.editReasonCodeConfigList(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      accountExternalStatusServices.editReasonCodeConfigList(params);

      expect(mockService).toBeCalledWith(
        apiUrl.clientConfigAccountExternalStatus.editReasonCode,
        params
      );
    });
  });

  describe('deleteReasonCodeConfigList', () => {
    const params: ReasonCodeRequest = { id: 'E7D14CXH' };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      accountExternalStatusServices.deleteReasonCodeConfigList(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      accountExternalStatusServices.deleteReasonCodeConfigList(params);

      expect(mockService).toBeCalledWith(
        apiUrl.clientConfigAccountExternalStatus.deleteReasonCode,
        params
      );
    });
  });
});
