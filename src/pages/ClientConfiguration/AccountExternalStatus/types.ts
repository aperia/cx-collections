import { SortType } from 'app/_libraries/_dls/components';
import { ModalData } from 'pages/__commons/ConfirmModal/types';
import { ToastType } from 'pages/__commons/ToastNotifications/_redux';

export interface AccountExternalStatusState {
  loading?: boolean;
  accountExternalStatusList: {
    data?: AccountExternalStatusDataType[];
    originData?: AccountExternalStatusDataType[];
    loading?: boolean;
  };
  reasonCodeList: {
    modalType?: string;
    open?: boolean;
    selectedCode?: ReasonCodesDataType;
    data?: ReasonCodesDataType[];
    originData?: ReasonCodesDataType[];
    modalErrorMessage?: string;
    modalLoading?: boolean;
    sortBy?: SortType;
  };
  data: {
    externalCode: Array<RefDataValue>;
    reasonCode: Array<RefDataValue>;
  };
  error?: string;
  isUpdating: boolean;
}

export type SelectedRecord = {
  checked?: boolean;
};

export interface AccountExternalStatusDataType extends RefDataValue {
  checkList?: Record<string, SelectedRecord>;
}

export interface ReasonCodesDataType extends RefDataValue {
  checkList?: Record<string, SelectedRecord>;
}

export interface CheckBoxGridState extends DLSId {
  columnId: string;
  data: MagicKeyValue;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  disabledAll?: boolean;
  status: boolean;
}

export interface ReasonCodeRequest {
  id?: string;
  reasonCode: string;
  description: string;
}

export interface SaveChangeDataArgs {
  modalPayload: ModalData;
}

export interface PreviousDataArgs {
  toast: ToastType;
}

export interface ExternalStatusPayload {
  externalCode: Array<ReasonCodesDataType>;
  reasonCode: Array<ReasonCodesDataType>;
}

export interface SaveAccountExternalStatus {
  data: {
    externalCode: Array<ReasonCodesDataType>;
    reasonCode: Array<ReasonCodesDataType>;
  };
}

export interface AccountExternalStatus {
  externalStatusCode?: AccountExternalStatusDataType[];
  externalStatusReasonCode?: ReasonCodesDataType[];
}

export interface GetAccountExternalStatusPayload {
  data: AccountExternalStatus;
}
