import React, { useMemo, useEffect } from 'react';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { Grid, ColumnType, TruncateText } from 'app/_libraries/_dls/components';
import { useDispatch } from 'react-redux';

// Components
import CheckBoxGrid from '../AccountExternalStatus/CheckBoxGrid';
import { FailedApiReload, NoDataGrid } from 'app/components';

// Constants
import { I18N_ACCOUNT_EXTERNAL_STATUS_TEXT } from './constants';

// Actions
import { accountExternalStatusActions } from './_redux/reducers';

// Hooks
import { useDataGridCheckBox, useBehaviorApi } from './hooks';

// Helper
import { ACC_EXTERNAL_STATUS } from '../helpers';
import { getValueAndDescription } from './helpers';
import { ClientConfigStatusProps } from '../types';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { codeToTextActions } from '../CodeToText/_redux/reducers';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

const AccountExternalStatusGrid: React.FC<ClientConfigStatusProps> = ({
  status
}) => {
  const dispatch = useDispatch();
  const {
    accountExternalStatusList: listData,
    accountExternalStatusError: isError
  } = useBehaviorApi();
  const testId = 'clientConfig_accountExternal_grid';

  const { data, handleChange } = useDataGridCheckBox(
    listData,
    ACC_EXTERNAL_STATUS
  );

  const handleReload = () =>
    dispatch(accountExternalStatusActions.getAccountStatusCodeList());

  const { t } = useTranslation();

  const handleChangeTabToCodeToText = () => {
    // set multiple codetotext dropdown value
    const listCode = ['accountExternalStatus'];
    dispatch(
      codeToTextActions.selectedDropdown({
        data: [
          {
            code: 'accountExternalStatus',
            description: 'Account External Status'
          }
        ],
        listCode
      })
    );
    dispatch(codeToTextActions.setSelectedCodeToTextTypes(listCode));
    const codeToTextElm = document.querySelector('a[data-rb-event-key="CTT"]');
    (codeToTextElm as HTMLElement)?.click();
  };

  const AccountExternalStatusColumns: ColumnType[] = useMemo(() => {
    return [
      {
        id: 'currentStatus',
        Header: t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.CURRENT_STATUS),
        accessor: acc => (
          <TruncateText
            lines={2}
            title={getValueAndDescription(acc)}
            resizable
            ellipsisMoreText={t(I18N_COMMON_TEXT.MORE)}
            ellipsisLessText={t(I18N_COMMON_TEXT.LESS)}
            className="text-break"
          >
            {getValueAndDescription(acc)}
          </TruncateText>
        ),
        isFixedLeft: true,
        width: 256,
        autoWidth: true
      },
      {
        id: 'availableStatus',
        Header: t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.AVAILABLE_STATUS),
        accessor: 'availableStatus',
        cellProps: { className: 'text-center' },
        columns: data?.map(item => {
          return {
            id: item?.value,
            Header: getValueAndDescription(item),
            cellProps: { className: 'text-center' },
            accessor: function accessor(accessorItem, idx) {
              return (
                <CheckBoxGrid
                  status={status}
                  columnId={item?.value}
                  data={accessorItem}
                  onChange={handleChange}
                  dataTestId={`${item?.value}_${idx}_${testId}`}
                />
              );
            },
            width: 160
          };
        })
      }
    ];
  }, [t, handleChange, data, status]);

  useEffect(() => {
    dispatch(accountExternalStatusActions.getAccountStatusCodeList());
  }, [dispatch]);

  if (isError)
    return (
      <div className="p-24">
        <FailedApiReload
          id="aes-account-external-status-list-failed"
          className="mt-80"
          onReload={handleReload}
          dataTestId={`${testId}_failedApi`}
        />
      </div>
    );

  if (!data.length)
    return (
      <NoDataGrid
        text={
          <p className="mt-20 color-grey">
            {t('txt_no_data_config_the_acc')}
            <span
              onClick={handleChangeTabToCodeToText}
              className="ml-4 link text-decoration-none"
            >
              {t('txt_code_to_txt')}.
            </span>
          </p>
        }
        dataTestId={`${testId}_noData`}
      />
    );

  return (
    <>
      <p
        className="mt-8 mb-16"
        data-testid={genAmtId(testId, 'description', '')}
      >
        {t(I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.ACCOUNT_EXTERNAL_DESCRIPTION)}
      </p>
      <Grid
        columns={AccountExternalStatusColumns}
        data={data}
        staticHeaderId="availableStatus"
        alwaysBorderFixedColumn
        dataTestId={testId}
      />
    </>
  );
};

export default AccountExternalStatusGrid;
