import './reducers';

import { accountExternalStatusActions } from './reducers';

import { createStore } from '@reduxjs/toolkit';

import { addReasonCode } from './addReasonCode';

import { accountExternalStatusServices } from '../accountExternalStatusServices';

import { I18N_ACCOUNT_EXTERNAL_STATUS_TEXT } from '../constants';

//redux
import { rootReducer } from 'storeConfig';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

import {
  responseDefault,
  byPassFulfilled,
  mockActionCreator,
  byPassRejected
} from 'app/test-utils';

const mockArgs = {
  reasonCode: '02',
  description: 'test'
};

let spy: jest.SpyInstance;

const mockAccountExternalActions = mockActionCreator(
  accountExternalStatusActions
);
const mockClientConfiguration = mockActionCreator(clientConfigurationActions);

const mockToast = mockActionCreator(actionsToast);

describe('AccountExternalStatus > ReasonCodeList > addReasonCode', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('success', async () => {
    const store = createStore(rootReducer, {});

    const mockSetAddReasonCodeErrorMessage = mockAccountExternalActions(
      'setAddReasonCodeErrorMessage'
    );
    const mockSaveCollectionsClientConfig = mockClientConfiguration(
      'saveCollectionsClientConfig'
    );

    await addReasonCode(mockArgs)(jest.fn(), store.getState, {});

    expect(mockSetAddReasonCodeErrorMessage).not.toBeCalled();
    expect(mockSaveCollectionsClientConfig).toBeCalledWith({
      component: 'account-external-status-component',
      jsonValue: {
        accountExternalStatus: {
          externalStatusCode: [],
          externalStatusReasonCode: [
            {
              checkList: { display: { checked: true } },
              description: 'test',
              value: '02'
            }
          ]
        }
      }
    });
  });

  it('isFulfilled', async () => {
    const store = createStore(rootReducer, {});
    const toast = mockToast('addToast');

    spy = jest
      .spyOn(accountExternalStatusServices, 'addReasonCodeConfigList')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    const mockToggleReasonCode = mockAccountExternalActions('toggleReasonCode');

    const response = await addReasonCode(mockArgs)(
      byPassFulfilled(addReasonCode.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.type).toBe(addReasonCode.fulfilled.type);

    expect(toast).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.REASON_CODE_ADD_MESSAGE
    });

    expect(mockToggleReasonCode).toBeCalledWith({
      modalType: 'add'
    });
  });

  it('isRejected', async () => {
    const store = createStore(rootReducer, {});
    const toast = mockToast('addToast');

    const response = await addReasonCode(mockArgs)(
      byPassRejected(addReasonCode.rejected.type),
      store.getState,
      {}
    );

    expect(response.payload).toBeUndefined();

    expect(toast).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.REASON_CODE_ADD_SERVER_ERROR
    });
  });

  describe('error with http code 409', () => {
    it('when reasonCodeList is NOT empty', async () => {
      const mockSetAddReasonCodeErrorMessage = mockAccountExternalActions(
        'setAddReasonCodeErrorMessage'
      );
      const store = createStore(rootReducer, {
        accountExternalStatus: {
          reasonCodeList: {
            data: [
              {
                value: '02',
                description: 'test2'
              }
            ],
            originData: [
              {
                value: '02',
                description: 'test2'
              }
            ],
            modalErrorMessage: '',
            modalLoading: false
          },
          accountExternalStatusList: {}
        }
      });

      const response = await addReasonCode(mockArgs)(
        byPassRejected(addReasonCode.rejected.type, {
          response: { status: 409 }
        }),
        store.getState,
        {}
      );

      expect(response.payload).toBeUndefined();

      expect(mockSetAddReasonCodeErrorMessage).toBeCalledWith(
        'txt_reason_code_duplicate_message'
      );
    });

    it('when reasonCodeList is empty', async () => {
      const mockSetAddReasonCodeErrorMessage = mockAccountExternalActions(
        'setAddReasonCodeErrorMessage'
      );
      const store = createStore(rootReducer, {
        accountExternalStatus: {
          reasonCodeList: {},
          accountExternalStatusList: {}
        }
      });

      await addReasonCode(mockArgs)(
        byPassRejected(addReasonCode.rejected.type, {
          response: { status: 409 }
        }),
        store.getState,
        {}
      );

      expect(mockSetAddReasonCodeErrorMessage).not.toHaveBeenCalled();
    });
  });

  describe('should be addReasonCodeBuilder', () => {
    let state: RootState;

    const store = createStore(rootReducer, {
      accountExternalStatus: {
        reasonCodeList: {
          data: [
            {
              value: '02',
              description: 'test2'
            }
          ],
          modalErrorMessage: '',
          modalLoading: false
        }
      }
    });

    beforeEach(() => {
      state = store.getState();
    });

    afterEach(() => {
      spy?.mockReset();
      spy?.mockRestore();
    });

    it('should response editReasonCode pending', () => {
      const pendingAction = addReasonCode.pending(
        addReasonCode.pending.type,
        mockArgs
      );
      const actual = rootReducer(state, pendingAction);

      expect(actual.accountExternalStatus.reasonCodeList.modalLoading).toEqual(
        true
      );
    });

    it('should response addReasonCode fulfilled', () => {
      const fulfilled = addReasonCode.fulfilled(
        undefined,
        addReasonCode.pending.type,
        mockArgs
      );
      const actual = rootReducer(state, fulfilled);

      expect(actual.accountExternalStatus.reasonCodeList.modalLoading).toEqual(
        false
      );
    });

    it('should response addReasonCode rejected', () => {
      const rejected = addReasonCode.rejected(
        null,
        addReasonCode.rejected.type,
        mockArgs
      );
      const actual = rootReducer(state, rejected);

      expect(actual.accountExternalStatus.reasonCodeList.modalLoading).toEqual(
        false
      );
    });
  });
});
