import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// types
import { AccountExternalStatusState } from '../types';

//api
import { addReasonCode, addReasonCodeBuilder } from './addReasonCode';
import {
  getAccountStatusCodeList,
  getAccountExternalStatusListBuilder
} from './getAccountStatusCodeList';
import { deleteReasonCode, deleteReasonCodeBuilder } from './deleteReasonCode';
import { editReasonCode, editReasonCodeBuilder } from './editReasonCode';
import {
  triggerChangeTabEvent,
  triggerCloseTabEvent,
  triggerOnSaveData,
  saveAccountExternalStatus,
  saveAccountExternalStatusBuilder
} from './triggerSaveChangeData';
import { triggerResetToPrevious } from './triggerResetToPrevious';
import { SortType } from 'app/_libraries/_dls/components';

const { actions, reducer } = createSlice({
  name: 'accountExternalStatus',
  initialState: {
    accountExternalStatusList: {
      data: [],
      error: '',
      loading: false
    },
    reasonCodeList: {
      loading: false,
      error: '',
      data: [],
      open: false,
      modalErrorMessage: '',
      modalType: '',
      modalLoading: false
    },
    data: {
      externalCode: [],
      reasonCode: []
    },
    isUpdating: false
  } as AccountExternalStatusState,
  reducers: {
    toggleReasonCode: (draftState, action: PayloadAction<MagicKeyValue>) => {
      draftState.reasonCodeList = {
        ...draftState.reasonCodeList,
        modalType: action.payload?.modalType,
        selectedCode: action.payload.selectedCode,
        open: !draftState.reasonCodeList?.open,
        modalErrorMessage: ''
      };
    },
    setAddReasonCodeErrorMessage: (
      draftState,
      action: PayloadAction<string>
    ) => {
      draftState.reasonCodeList = {
        ...draftState.reasonCodeList,
        modalErrorMessage: action.payload
      };
    },
    resetToPrevious: (draftState, action: PayloadAction<undefined>) => {
      draftState.accountExternalStatusList.data = draftState.data.externalCode;
      draftState.reasonCodeList.data = draftState.data.reasonCode;
    },
    updateAccountExternalList: (
      draftState,
      action: PayloadAction<MagicKeyValue>
    ) => {
      draftState.accountExternalStatusList = {
        ...draftState.accountExternalStatusList,
        data: action.payload.data
      };
    },
    updateReasonCodeExternalList: (
      draftState,
      action: PayloadAction<MagicKeyValue>
    ) => {
      draftState.reasonCodeList = {
        ...draftState.reasonCodeList,
        data: action.payload.data
      };
    },
    updateOriginReasonCode: (
      draftState,
      action: PayloadAction<MagicKeyValue>
    ) => {
      draftState.reasonCodeList = {
        ...draftState.reasonCodeList,
        originData: action.payload.data
      };
      draftState.data = {
        ...draftState.data,
        reasonCode: action.payload.data
      };
    },
    changeReasonCodeExternalSortBy: (
      draftState,
      action: PayloadAction<SortType>
    ) => {
      draftState.reasonCodeList = {
        ...draftState.reasonCodeList,
        sortBy: action.payload
      };
    }
  },
  extraReducers: builder => {
    getAccountExternalStatusListBuilder(builder);
    addReasonCodeBuilder(builder);
    deleteReasonCodeBuilder(builder);
    editReasonCodeBuilder(builder);
    saveAccountExternalStatusBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getAccountStatusCodeList,
  addReasonCode,
  deleteReasonCode,
  editReasonCode,
  triggerChangeTabEvent,
  saveAccountExternalStatus,
  triggerOnSaveData,
  triggerCloseTabEvent,
  triggerResetToPrevious
};

export {
  allActions as accountExternalStatusActions,
  reducer as accountExternalStatus
};
