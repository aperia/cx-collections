import './reducers';

import { getAccountStatusCodeList } from './getAccountStatusCodeList';

import { createStore } from '@reduxjs/toolkit';

//redux
import { rootReducer } from 'storeConfig';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

import {
  byPassFulfilled,
  mockActionCreator,
  byPassRejected,
  storeId
} from 'app/test-utils';

const mockClientConfiguration = mockActionCreator(clientConfigurationActions);

describe('getAccountStatusCodeList', () => {
  const store = createStore(rootReducer, {});

  it('isFulfilled', async () => {
    const mockGetCollectionsClientConfig = mockClientConfiguration(
      'getCollectionsClientConfig'
    );

    const response = await getAccountStatusCodeList({ storeId })(
      byPassFulfilled(getAccountStatusCodeList.fulfilled.type, {
        accountExternalStatus: { loading: false }
      }),
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      data: {
        externalStatusCode: [],
        externalStatusReasonCode: []
      }
    });
    expect(mockGetCollectionsClientConfig).toBeCalledWith({
      storeId: storeId,
      component: 'account-external-status-component'
    });
  });

  it('isFulfilled without payload', async () => {
    const mockGetCollectionsClientConfig = mockClientConfiguration(
      'getCollectionsClientConfig'
    );

    const response = await getAccountStatusCodeList({ storeId })(
      byPassFulfilled(getAccountStatusCodeList.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      data: {
        externalStatusCode: [],
        externalStatusReasonCode: []
      }
    });
    expect(mockGetCollectionsClientConfig).toHaveBeenNthCalledWith(1, {
      storeId,
      component: 'account-external-status-component'
    });
    expect(mockGetCollectionsClientConfig).toHaveBeenNthCalledWith(2, {
      component: 'code_to_text_config'
    });
  });

  it('isFulfilled with payload', async () => {
    mockClientConfiguration(
      'getCollectionsClientConfig',
      (args: any, _: any, __: any) => {
        if (args.component === 'account-external-status-component') {
          return {
            meta: {
              arg: {},
              requestStatus: 'fulfilled',
              requestId: 'mockRequestID'
            },
            type: getAccountStatusCodeList.fulfilled.type,
            payload: {
              key: 'abc-123',
              accountExternalStatus: {
                externalStatusCode: [
                  {
                    value: 'test',
                    description: 'test',
                    checkList: {
                      test: {
                        checked: true
                      }
                    },
                    code: 'test'
                  },
                  {
                    value: 'test3',
                    description: 'test3',
                    code: 'test3'
                  }
                ],
                externalStatusReasonCode: [
                  {
                    value: 'test1',
                    description: 'test1',
                    checkList: {
                      test1: {
                        checked: true
                      },
                      display: {
                        checked: true
                      }
                    },
                    code: 'test1'
                  },
                  {
                    value: 'test2',
                    description: 'test2',
                    checkList: {
                      test2: {
                        checked: false
                      }
                    },
                    code: 'test2'
                  }
                ],
                code: 'test'
              }
            }
          };
        }

        if (args.component === 'code_to_text_config') {
          return {
            meta: {
              arg: {},
              requestStatus: 'fulfilled',
              requestId: 'mockRequestID'
            },
            type: getAccountStatusCodeList.fulfilled.type,
            payload: {
              key: 'abc-123',
              accountExternalStatus: [
                {
                  externalStatusCode: [
                    {
                      value: 'test',
                      description: 'test',
                      checkList: {
                        test1: {
                          checked: true
                        },
                        display: {
                          checked: true
                        }
                      }
                    }
                  ],
                  externalStatusReasonCode: [
                    {
                      value: 'test1',
                      description: 'test1',
                      checkList: {
                        test: {
                          checked: true
                        },
                        display: {
                          checked: true
                        }
                      }
                    }
                  ],
                  code: 'test'
                }
              ]
            }
          };
        }
      }
    );

    const response = await getAccountStatusCodeList({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      data: {
        externalStatusCode: [
          {
            checkList: {
              test: {
                checked: true
              }
            },
            description: undefined,
            value: 'test'
          }
        ],
        externalStatusReasonCode: [
          {
            checkList: {
              display: {
                checked: true
              },
              test: {
                checked: false
              }
            },
            code: 'test1',
            description: 'test1',
            value: 'test1'
          },
          {
            checkList: {
              test: {
                checked: false
              }
            },
            code: 'test2',
            description: 'test2',
            value: 'test2'
          }
        ]
      }
    });
  });

  it('isFulfilled with payload empty externalStatusReasonCode', async () => {
    mockClientConfiguration(
      'getCollectionsClientConfig',
      (args: any, _: any, __: any) => {
        if (args.component === 'account-external-status-component') {
          return {
            meta: {
              arg: {},
              requestStatus: 'fulfilled',
              requestId: 'mockRequestID'
            },
            type: getAccountStatusCodeList.fulfilled.type,
            payload: {
              key: 'abc-123',
              accountExternalStatus: {
                externalStatusCode: [
                  {
                    value: 'test',
                    description: 'test',
                    checkList: {
                      test: {
                        checked: true
                      }
                    },
                    code: 'test'
                  },
                  {
                    value: 'test3',
                    description: 'test3',
                    code: 'test3'
                  }
                ],
                externalStatusReasonCode: null,
                code: 'test'
              }
            }
          };
        }

        if (args.component === 'code_to_text_config') {
          return {
            meta: {
              arg: {},
              requestStatus: 'fulfilled',
              requestId: 'mockRequestID'
            },
            type: getAccountStatusCodeList.fulfilled.type,
            payload: {
              key: 'abc-123',
              accountExternalStatus: [
                {
                  externalStatusCode: [
                    {
                      value: 'test',
                      description: 'test',
                      checkList: {
                        test1: {
                          checked: true
                        },
                        display: {
                          checked: true
                        }
                      }
                    }
                  ],
                  externalStatusReasonCode: [
                    {
                      value: 'test1',
                      description: 'test1',
                      checkList: {
                        test: {
                          checked: true
                        },
                        display: {
                          checked: true
                        }
                      }
                    }
                  ],
                  code: 'test'
                }
              ]
            }
          };
        }
      }
    );

    const response = await getAccountStatusCodeList({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      data: {
        externalStatusCode: [
          {
            checkList: {
              test: {
                checked: true
              }
            },
            description: undefined,
            value: 'test'
          }
        ],
        externalStatusReasonCode: []
      }
    });
  });

  it('isRejected', async () => {
    const response = await getAccountStatusCodeList({ storeId })(
      byPassRejected(getAccountStatusCodeList.rejected.type),
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      data: {
        externalStatusCode: [],
        externalStatusReasonCode: []
      }
    });
  });

  it('pending', () => {
    const action = getAccountStatusCodeList.pending(
      getAccountStatusCodeList.pending.type,
      undefined
    );
    const actual = rootReducer(store.getState(), action).accountExternalStatus;

    expect(actual.loading).toEqual(true);
  });

  it('fulfilled', () => {
    const action = getAccountStatusCodeList.fulfilled(
      { data: {} },
      getAccountStatusCodeList.pending.type,
      undefined
    );
    const actual = rootReducer(store.getState(), action).accountExternalStatus;

    expect(actual.loading).toEqual(false);
  });

  it('rejected', () => {
    const action = getAccountStatusCodeList.rejected(
      null,
      getAccountStatusCodeList.pending.type,
      undefined
    );
    const actual = rootReducer(store.getState(), action).accountExternalStatus;

    expect(actual.loading).toEqual(false);
  });
});
