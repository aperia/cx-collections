import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';

//types
import { AccountExternalStatusState, ReasonCodeRequest } from '../types';

//constants
import { I18N_ACCOUNT_EXTERNAL_STATUS_TEXT } from '../constants';

//redux
import { batch } from 'react-redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

//reducers
import { accountExternalStatusActions } from './reducers';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

export interface AddReasonCodeArgs extends ReasonCodeRequest, MagicKeyValue {}

export const addReasonCode = createAsyncThunk<
  void,
  AddReasonCodeArgs,
  ThunkAPIConfig
>('clientConfigAccountExternalStatus/addReasonCode', async (args, thunkAPI) => {
  const { getState, dispatch } = thunkAPI;
  const state = getState();
  const { reasonCodeList, accountExternalStatusList } =
    state?.accountExternalStatus;
  const {
    originData: reasonCodeOriginData = [],
    data: reasonCodeChangedData = []
  } = reasonCodeList;

  const { originData: externalStatusCode = [] } = accountExternalStatusList;

  const { reasonCode, description } = args;

  if (
    reasonCodeOriginData.find(item => item.value === reasonCode?.toString())
  ) {
    dispatch(
      accountExternalStatusActions.setAddReasonCodeErrorMessage(
        I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.ERROR_REASON_CODE_DUPLICATE_MESSAGE
      )
    );

    return;
  }

  const valueReasonCode = {
    value: reasonCode,
    description: description,
    checkList: { display: { checked: true } }
  };

  const newExternalStatusReasonCode = [
    ...reasonCodeOriginData,
    {
      ...valueReasonCode
    }
  ];

  const response = await dispatch(
    clientConfigurationActions.saveCollectionsClientConfig({
      component: 'account-external-status-component',
      jsonValue: {
        accountExternalStatus: {
          externalStatusCode,
          externalStatusReasonCode: newExternalStatusReasonCode
        }
      }
    })
  );

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        accountExternalStatusActions.updateOriginReasonCode({
          data: newExternalStatusReasonCode
        })
      );
      dispatch(
        accountExternalStatusActions.updateReasonCodeExternalList({
          data: [
            ...reasonCodeChangedData,
            {
              ...valueReasonCode
            }
          ]
        })
      );
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.REASON_CODE_ADD_MESSAGE
        })
      );

      dispatch(
        changeHistoryActions.saveChangedClientConfig({
          action: 'ADD',
          newValue: {
            value: reasonCode,
            description: description
          },
          changedItem: { value: `${reasonCode} - ${description}` },
          changedCategory: 'accountExternalStatusReasonCode'
        })
      );

      dispatch(
        accountExternalStatusActions.toggleReasonCode({ modalType: 'add' })
      );
    });
    return;
  }

  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message:
            I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.REASON_CODE_ADD_SERVER_ERROR
        })
      );
    });
  }
});

export const addReasonCodeBuilder = (
  builder: ActionReducerMapBuilder<AccountExternalStatusState>
) => {
  builder
    .addCase(addReasonCode.pending, (draftState, action) => {
      draftState.reasonCodeList = {
        ...draftState.reasonCodeList,
        modalLoading: true
      };
    })
    .addCase(addReasonCode.fulfilled, (draftState, action) => {
      draftState.reasonCodeList = {
        ...draftState.reasonCodeList,
        modalLoading: false
      };
    })
    .addCase(addReasonCode.rejected, (draftState, action) => {
      draftState.reasonCodeList = {
        ...draftState.reasonCodeList,
        modalLoading: false
      };
    });
};
