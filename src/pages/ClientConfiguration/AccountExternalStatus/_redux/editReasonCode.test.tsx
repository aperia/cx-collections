import { accountExternalStatusActions } from './reducers';

import { createStore, Store } from '@reduxjs/toolkit';

import { editReasonCode, editReasonCodeAPI } from './editReasonCode';

import { accountExternalStatusServices } from '../accountExternalStatusServices';

//redux
import { rootReducer } from 'storeConfig';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

//constants
import { I18N_ACCOUNT_EXTERNAL_STATUS_TEXT } from '../constants';

import {
  mockActionCreator,
  responseDefault,
  byPassRejected
} from 'app/test-utils';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

const mockArgs = {
  id: 'aaa-001',
  reasonCode: '01',
  description: 'test'
};

let store: Store<RootState>;
let state: RootState;

const mockAccountExternalActions = mockActionCreator(
  accountExternalStatusActions
);
const mockClientConfigurationActions = mockActionCreator(
  clientConfigurationActions
);
const mockChangeHistoryActions = mockActionCreator(changeHistoryActions);

const mockToast = mockActionCreator(actionsToast);

const initialState: Partial<RootState> = {
  accountExternalStatus: {
    accountExternalStatusList: {},
    isUpdating: false,
    reasonCodeList: {
      data: [
        {
          checkList: {
            value: { checked: false }
          },
          description: 'description',
          value: '09'
        }
      ],
      originData: [
        {
          description: 'reasonCode description 1',
          value: '09',
          checkList: {
            value: { checked: false }
          }
        }
      ],
      modalLoading: false
    },
    data: {
      externalCode: [],
      reasonCode: []
    }
  }
};

beforeEach(() => {
  store = createStore(rootReducer, initialState);
  state = store.getState();
});

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();

  mockClientConfigurationActions('saveCollectionsClientConfig');
});

describe('should run editReasonCodeAPI', () => {
  it('Should return data', async () => {
    jest
      .spyOn(accountExternalStatusServices, 'editReasonCodeConfigList')
      .mockResolvedValue({ ...responseDefault });

    const response = await editReasonCodeAPI(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('should return error', async () => {
    jest
      .spyOn(accountExternalStatusServices, 'editReasonCodeConfigList')
      .mockRejectedValue({ ...responseDefault });

    const response = await editReasonCodeAPI(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toBeDefined();
  });
});

describe('should trigger editReasonCode', () => {
  it('Should editReasonCodeConfigList', async () => {
    store = createStore(rootReducer, {
      accountExternalStatus: {
        accountExternalStatusList: {},
        reasonCodeList: {}
      }
    });

    const mockAction = mockClientConfigurationActions(
      'saveCollectionsClientConfig'
    );

    await editReasonCode(mockArgs)(jest.fn(), store.getState, {});

    expect(mockAction).toBeCalledWith({
      component: 'account-external-status-component',
      jsonValue: {
        accountExternalStatus: {
          externalStatusCode: [],
          externalStatusReasonCode: []
        }
      }
    });
  });

  it('isFulfilled item.value !== id', async () => {
    store = createStore(rootReducer, {
      accountExternalStatus: {
        accountExternalStatusList: {},
        reasonCodeList: {
          data: [
            {
              description: 'reasonCode description 1',
              value: `${mockArgs.id}-diff`,
              checkList: {
                value: { checked: true }
              }
            }
          ],
          originData: [
            {
              description: 'reasonCode description 1',
              value: 'aaa-001',
              checkList: {
                value: { checked: false }
              }
            }
          ]
        }
      }
    });

    const mockToggleReasonCode = mockAccountExternalActions('toggleReasonCode');

    const mockSaveChangedClientConfig = mockChangeHistoryActions(
      'saveChangedClientConfig'
    );

    mockClientConfigurationActions('saveCollectionsClientConfig', () => ({
      meta: {
        arg: {},
        requestStatus: 'fulfilled',
        requestId: 'mockRequestID'
      },
      type: editReasonCode.fulfilled.type,
      payload: {}
    }));
    const toast = mockToast('addToast');

    const response = await editReasonCode(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toBe(editReasonCode.fulfilled.type);

    expect(toast).toBeCalledWith({
      message: 'txt_reason_code_edit_message',
      show: true,
      type: 'success'
    });

    expect(mockToggleReasonCode).toBeCalledWith({ modalType: 'edit' });
    expect(mockSaveChangedClientConfig).toBeCalled();
  });

  it('isFulfilled with item.value === id', async () => {
    store = createStore(rootReducer, {
      accountExternalStatus: {
        accountExternalStatusList: {},
        reasonCodeList: {
          data: [
            {
              description: 'reasonCode description 1',
              value: `${mockArgs.id}`,
              checkList: {
                value: { checked: true }
              }
            }
          ],
          originData: [
            {
              description: 'reasonCode description 1',
              value: 'aaa-002',
              checkList: {
                value: { checked: false }
              }
            }
          ]
        }
      }
    });

    const mockToggleReasonCode = mockAccountExternalActions('toggleReasonCode');

    const mockSaveChangedClientConfig = mockChangeHistoryActions(
      'saveChangedClientConfig'
    );
    mockClientConfigurationActions('saveCollectionsClientConfig', () => ({
      meta: {
        arg: {},
        requestStatus: 'fulfilled',
        requestId: 'mockRequestID'
      },
      type: editReasonCode.fulfilled.type,
      payload: {}
    }));
    const toast = mockToast('addToast');

    const response = await editReasonCode(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toBe(editReasonCode.fulfilled.type);

    expect(toast).toBeCalledWith({
      message: 'txt_reason_code_edit_message',
      show: true,
      type: 'success'
    });

    expect(mockToggleReasonCode).toBeCalledWith({ modalType: 'edit' });
    expect(mockSaveChangedClientConfig).toBeCalled();
  });

  it('isRejected', async () => {
    const toast = mockToast('addToast');

    const response = await editReasonCode({
      ...mockArgs,
      reasonCode: '09',
      id: '09'
    })(byPassRejected(editReasonCode.rejected.type), store.getState, {});

    expect(response.payload).toBeUndefined();

    expect(toast).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.REASON_CODE_EDIT_SERVER_ERROR
    });
  });

  it('error with http code 409', async () => {
    const mockSetAddReasonCodeErrorMessage = mockAccountExternalActions(
      'setAddReasonCodeErrorMessage'
    );

    const response = await editReasonCode({ ...mockArgs, reasonCode: '09' })(
      byPassRejected(editReasonCode.rejected.type, {
        response: { status: 409 }
      }),
      store.getState,
      {}
    );

    expect(response.payload).toBeUndefined();

    expect(mockSetAddReasonCodeErrorMessage).toBeCalledWith(
      I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.ERROR_REASON_CODE_DUPLICATE_MESSAGE
    );
  });

  describe('should be editReasonCodeBuilder', () => {
    it('should response editReasonCode pending', () => {
      const pendingAction = editReasonCode.pending(
        editReasonCode.pending.type,
        mockArgs
      );
      const actual = rootReducer(state, pendingAction);

      expect(actual.accountExternalStatus.reasonCodeList.modalLoading).toEqual(
        true
      );
    });

    it('should response editReasonCode fulfilled', () => {
      const fulfilled = editReasonCode.fulfilled(
        undefined,
        editReasonCode.pending.type,
        mockArgs
      );
      const actual = rootReducer(state, fulfilled);

      expect(actual.accountExternalStatus.reasonCodeList.modalLoading).toEqual(
        false
      );
    });

    it('should response editReasonCode rejected', () => {
      const rejected = editReasonCode.rejected(
        null,
        editReasonCode.rejected.type,
        mockArgs
      );
      const actual = rootReducer(state, rejected);

      expect(actual.accountExternalStatus.reasonCodeList.modalLoading).toEqual(
        false
      );
    });
  });
});
