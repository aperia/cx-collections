import { createStore, Store } from '@reduxjs/toolkit';

import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator
} from 'app/test-utils';

import { rootReducer } from 'storeConfig';

import {
  saveAccountExternalStatus,
  triggerChangeTabEvent,
  triggerCloseTabEvent,
  triggerOnSaveData
} from './triggerSaveChangeData';

// Actions
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

// Helper
import * as helpers from 'pages/ClientConfiguration/helpers';
import { I18N_ACCOUNT_EXTERNAL_STATUS_TEXT } from '../constants';

const spyConfirmModal = mockActionCreator(confirmModalActions);
const spyHelpers = mockActionCreator(helpers);
const spyToast = mockActionCreator(actionsToast);
const spyClientConfig = mockActionCreator(clientConfigurationActions);

let store: Store<RootState>;

beforeEach(() => {
  store = createStore(rootReducer, {});
});

it('saveAccountExternalStatus', async () => {
  const saveCollectionsClientConfig = spyClientConfig(
    'saveCollectionsClientConfig'
  );

  store = createStore(rootReducer, {
    accountExternalStatus: {
      reasonCodeList: {},
      accountExternalStatusList: {}
    }
  });

  await saveAccountExternalStatus()(store.dispatch, store.getState, {});

  expect(saveCollectionsClientConfig).toHaveBeenCalledWith({
    component: 'account-external-status-component',
    jsonValue: {
      accountExternalStatus: {
        externalStatusCode: [],
        externalStatusReasonCode: []
      }
    }
  });
});

describe('triggerChangeTabEvent', () => {
  it('Should call action when data was changed', async () => {
    const mockDispatchControlTabChangeEvent = spyHelpers(
      'dispatchControlTabChangeEvent'
    );
    const mockOpenModal = spyConfirmModal('open');
    store = createStore(rootReducer, {
      accountExternalStatus: {
        accountExternalStatusList: {},
        reasonCodeList: {
          data: [
            {
              description: 'reasonCode 1',
              value: 'value',
              checkList: {
                value: { checked: true }
              }
            }
          ],
          originData: [
            {
              description: 'reasonCode 1',
              value: 'value',
              checkList: {
                value: { checked: false }
              }
            }
          ]
        },
        data: {}
      }
    });
    await triggerChangeTabEvent({ modalPayload: {} })(
      () => true,
      store.getState,
      {}
    );

    expect(mockOpenModal).toBeCalled();
    expect(mockDispatchControlTabChangeEvent).not.toBeCalled();
  });

  it('Should not call action when data was not changed', async () => {
    const mockDispatchControlTabChangeEvent = spyHelpers(
      'dispatchControlTabChangeEvent'
    );
    const mockOpenModal = spyConfirmModal('open');

    await triggerChangeTabEvent({ modalPayload: {} })(
      () => false,
      store.getState,
      {}
    );

    expect(mockOpenModal).not.toBeCalled();
    expect(mockDispatchControlTabChangeEvent).toBeCalled();
  });
});

describe('triggerCloseTabEvent', () => {
  it('Should call action when data was changed', async () => {
    const mockDispatchCloseModalEvent = spyHelpers('dispatchCloseModalEvent');
    const mockOpenModal = spyConfirmModal('open');
    store = createStore(rootReducer, {
      accountExternalStatus: {
        accountExternalStatusList: {},
        reasonCodeList: {
          data: [
            {
              description: 'reasonCode 1',
              value: 'value',
              checkList: {
                value: { checked: true }
              }
            }
          ],
          originData: [
            {
              description: 'reasonCode 1',
              value: 'value',
              checkList: {
                value: { checked: false }
              }
            }
          ]
        }
      }
    });
    await triggerCloseTabEvent({ modalPayload: {} })(
      () => true,
      store.getState,
      {}
    );

    expect(mockOpenModal).toBeCalled();
    expect(mockDispatchCloseModalEvent).not.toBeCalled();
  });

  it('Should not call action when data was not changed', async () => {
    const mockDispatchCloseModalEvent = spyHelpers('dispatchCloseModalEvent');
    const mockOpenModal = spyConfirmModal('open');

    await triggerCloseTabEvent({ modalPayload: {} })(
      () => false,
      store.getState,
      {}
    );

    expect(mockOpenModal).not.toBeCalled();
    expect(mockDispatchCloseModalEvent).toBeCalled();
  });
});

describe('triggerOnSaveData', () => {
  it('async action', async () => {
    const mockAttToast = spyToast('addToast');

    store = createStore(rootReducer, {
      accountExternalStatus: {
        reasonCodeList: {},
        accountExternalStatusList: {}
      }
    });

    await triggerOnSaveData()(store.dispatch, store.getState, {});

    expect(mockAttToast).not.toBeCalled();
  });

  it('isFulfilled', async () => {
    const mockAttToast = spyToast('addToast');

    await triggerOnSaveData()(
      byPassFulfilled(triggerOnSaveData.fulfilled.type),
      store.getState,
      {}
    );

    expect(mockAttToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.SAVE_CHANGE_SUCCESS
    });
  });

  it('isRejected', async () => {
    const mockAttToast = spyToast('addToast');

    await triggerOnSaveData()(
      byPassRejected(triggerOnSaveData.fulfilled.type),
      store.getState,
      {}
    );

    expect(mockAttToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.SAVE_CHANGE_FAILURE
    });
  });
});
