import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';
import isArray from 'lodash.isarray';
import sortBy from 'lodash.sortby';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import {
  AccountExternalStatusState,
  GetAccountExternalStatusPayload,
  ReasonCodesDataType,
  SelectedRecord
} from '../types';

export interface GetAccountExternalStatusArgs {
  storeId?: string;
}

export const normalizeCheckList = (
  list: {
    value: string;
    description: string;
    checkList?: Record<string, SelectedRecord>;
  }[]
) => {
  const checkedNormalizer: Record<string, Set<string>> = {};
  list.forEach(status => {
    const checkListArr: string[] = [];
    if (status.checkList) {
      Object.entries(status.checkList).forEach(([k, v]) => {
        if (v.checked) {
          checkListArr.push(k);
        }
      });
    }
    checkedNormalizer[status.value] = new Set(checkListArr);
  });
  return checkedNormalizer;
};

export const getAccountStatusCodeList = createAsyncThunk<
  GetAccountExternalStatusPayload,
  GetAccountExternalStatusArgs | undefined,
  ThunkAPIConfig
>(
  'clientConfigAccountExternalStatus/getAccountStatusCodeList',
  async (args, thunkAPI) => {
    const { dispatch } = thunkAPI;
    const response = await dispatch(
      clientConfigurationActions.getCollectionsClientConfig({
        storeId: args?.storeId,
        component: 'account-external-status-component'
      })
    );

    let extSttCheckedNormalizer: Record<string, Set<string>> = {};
    let extSttReasonCheckedNormalizer: Record<string, Set<string>> = {};

    let originalReasonCode: ReasonCodesDataType[] = [];

    if (isFulfilled(response)) {
      const originalExtStt =
        response.payload?.accountExternalStatus?.externalStatusCode || [];
      originalReasonCode =
        response.payload?.accountExternalStatus?.externalStatusReasonCode || [];
      extSttCheckedNormalizer = normalizeCheckList(originalExtStt);
      extSttReasonCheckedNormalizer = normalizeCheckList(originalReasonCode);
    }
    const accExtSttResponse = await dispatch(
      clientConfigurationActions.getCollectionsClientConfig({
        component: 'code_to_text_config'
      })
    );

    if (isFulfilled(accExtSttResponse)) {
      let codeToTextRefData: { code: string; description: string }[] = [];
      if (isArray(accExtSttResponse.payload?.accountExternalStatus)) {
        codeToTextRefData = accExtSttResponse.payload!.accountExternalStatus;
      }
      const extReasonCode = originalReasonCode.map(reasonCode => {
        const checkList: Record<string, { checked: boolean }> = {};
        codeToTextRefData.forEach(refData => {
          checkList[refData.code] = {
            checked: extSttReasonCheckedNormalizer[reasonCode.value]?.has(
              refData.code
            )
          };
        });
        if (reasonCode?.checkList?.display) {
          checkList['display'] = reasonCode.checkList.display as {
            checked: boolean;
          };
        }
        return { ...reasonCode, checkList };
      });

      const extStt = codeToTextRefData.map(item => {
        const checkList: Record<string, { checked: boolean }> = {};
        codeToTextRefData.forEach(refData => {
          checkList[refData.code] = {
            checked: extSttCheckedNormalizer[item.code]?.has(refData.code)
          };
        });
        return { description: item.description, value: item.code, checkList };
      });
      return {
        data: {
          externalStatusCode: extStt,
          externalStatusReasonCode: extReasonCode
        }
      };
    }

    return {
      data: {
        externalStatusCode: [],
        externalStatusReasonCode: []
      }
    };
  }
);

export const getAccountExternalStatusListBuilder = (
  builder: ActionReducerMapBuilder<AccountExternalStatusState>
) => {
  builder.addCase(getAccountStatusCodeList.pending, draftState => {
    draftState.loading = true;
    draftState.accountExternalStatusList = {
      data: [],
      originData: []
    };
    draftState.reasonCodeList = {
      data: [],
      originData: []
    };
  });
  builder.addCase(getAccountStatusCodeList.fulfilled, (draftState, action) => {
    const externalCode = sortBy(
      action.payload.data?.externalStatusCode || [],
      'value'
    );
    const reasonCode = sortBy(
      action.payload.data?.externalStatusReasonCode || [],
      'value'
    );
    draftState.loading = false;
    draftState.accountExternalStatusList = {
      ...draftState.accountExternalStatusList,
      data: externalCode,
      originData: externalCode
    };
    draftState.reasonCodeList = {
      ...draftState.reasonCodeList,
      data: reasonCode,
      originData: reasonCode
    };
    draftState.data = {
      ...draftState.data,
      externalCode,
      reasonCode
    };
  });
  builder.addCase(getAccountStatusCodeList.rejected, (draftState, action) => {
    draftState.loading = false;
    draftState.error = action.payload?.errorMessage;
  });
};
