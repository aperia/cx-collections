import { createSelector } from '@reduxjs/toolkit';
import { isEqual } from 'app/_libraries/_dls/lodash';
import { isUndefined, orderBy } from 'lodash';
import { getCheckListData } from '../helpers';

const getReasonCodeConfiguration = (state: RootState) => {
  return state?.accountExternalStatus?.reasonCodeList;
};

const getAccountExternalStatusList = (state: RootState) => {
  return state?.accountExternalStatus?.accountExternalStatusList;
};

const getAccountExternalStatusInfo = (state: RootState) => {
  return state?.accountExternalStatus?.data;
};

const getAccountExternalUpdateStatus = (state: RootState) => {
  return state?.accountExternalStatus?.isUpdating;
};

const getAccountExternalStatusLoading = (state: RootState) => {
  return state?.accountExternalStatus?.loading || false;
};

export const selectGetAccountExternalStatusLoading = createSelector(
  getAccountExternalStatusLoading,
  data => data
);

export const selectedReasonCodeConfigurationSortBy = createSelector(
  getReasonCodeConfiguration,
  data => data?.sortBy || { id: 'reasonCode', order: undefined }
);

export const selectedReasonCodeConfigurationData = createSelector(
  [getReasonCodeConfiguration, selectedReasonCodeConfigurationSortBy],
  (data, sortBy) => {
    const dataList = data?.data || [];
    if (isUndefined(sortBy?.order))
      return orderBy(dataList, [item => item?.value?.toLowerCase()], ['asc']);

    return orderBy(
      dataList,
      [
        (item: any) =>
          sortBy?.id === 'reasonCode'
            ? item?.value?.toString()?.toLowerCase()
            : item?.checkList?.[sortBy?.id]?.toString()?.toLowerCase()
      ],
      [sortBy.order || false]
    );
  }
);

export const selectedReasonCodeConfigurationOriginData = createSelector(
  [getReasonCodeConfiguration, selectedReasonCodeConfigurationSortBy],
  (data, sortBy) => {
    const dataList = data?.originData || [];
    if (isUndefined(sortBy?.order)) return dataList;

    return orderBy(
      dataList,
      [
        (item: any) =>
          sortBy?.id === 'reasonCode'
            ? item?.value?.toString()?.toLowerCase()
            : item?.checkList?.[sortBy?.id]?.toString()?.toLowerCase()
      ],
      [sortBy.order || false]
    );
  }
);

export const selectedReasonCodeConfigurationListModalErrorMessage =
  createSelector(getReasonCodeConfiguration, data => data?.modalErrorMessage);

export const loadingReasonCodeConfiguration = createSelector(
  getReasonCodeConfiguration,
  data => data?.modalLoading
);

export const selectedAccountExternalStatusData = createSelector(
  getAccountExternalStatusList,
  data => data?.data || []
);

export const selectedAccountExternalStatusOriginData = createSelector(
  getAccountExternalStatusList,
  data => data?.originData || []
);

export const selectedAccountExternalStatusListLoading = createSelector(
  getAccountExternalStatusList,
  data => data?.loading
);

export const selectedAccountExternalStatusListError = createSelector(
  (state: RootState) => state?.accountExternalStatus?.error,
  data => data
);

export const reasonCodeModal = createSelector(
  getReasonCodeConfiguration,
  data => data?.open
);

export const reasonCodeModalType = createSelector(
  getReasonCodeConfiguration,
  data => data?.modalType || ''
);

export const selectedReasonCode = createSelector(
  getReasonCodeConfiguration,
  data => data?.selectedCode
);

export const takeAccountExternalStatus = createSelector(
  getAccountExternalStatusInfo,
  data => data
);

export const takeAccountExternalUpdateStatus = createSelector(
  getAccountExternalUpdateStatus,
  data => data
);

export const selectDataChange = createSelector(
  (state: RootState) => {
    const { reasonCodeList, accountExternalStatusList, data } =
      state?.accountExternalStatus ?? {
        reasonCodeList: {},
        accountExternalStatusList: {},
        data: {}
      };
    const { data: reasonData = [] } = reasonCodeList;
    const { data: externalData = [] } = accountExternalStatusList;

    const latestList = getCheckListData([
      ...(data?.reasonCode || []),
      ...(data?.externalCode || [])
    ]);

    const reasonChecklist = getCheckListData(reasonData);

    const externalChecklist = getCheckListData(externalData);

    const changedList = [...reasonChecklist!, ...externalChecklist!];

    return !isEqual(changedList, latestList);
  },
  data => data
);
