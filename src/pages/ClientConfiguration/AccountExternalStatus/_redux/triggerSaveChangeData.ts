import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { saveChangedClientConfig } from 'pages/ClientConfiguration/ChangeHistory/_redux/saveChangedClientConfig';

// Helper
import {
  dispatchCloseModalEvent,
  dispatchControlTabChangeEvent
} from 'pages/ClientConfiguration/helpers';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

// Actions
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import { I18N_ACCOUNT_EXTERNAL_STATUS_TEXT } from '../constants';

// Type
import { AccountExternalStatusState, SaveChangeDataArgs } from '../types';
import { accountExternalStatusActions } from './reducers';

// Selector
import { selectDataChange } from './selectors';

export const saveAccountExternalStatus = createAsyncThunk<
  void,
  undefined,
  ThunkAPIConfig
>(
  'clientConfigAccountExternalStatus/saveAccountExternalStatus',
  async (args, thunkAPI) => {
    const { getState, dispatch } = thunkAPI;
    const state = getState();
    const { reasonCodeList, accountExternalStatusList } =
      state?.accountExternalStatus;
    const { data: externalStatusReasonCode = [] } = reasonCodeList;
    const { data: externalStatusCode = [] } = accountExternalStatusList;

    await dispatch(
      clientConfigurationActions.saveCollectionsClientConfig({
        component: 'account-external-status-component',
        jsonValue: {
          accountExternalStatus: {
            externalStatusCode,
            externalStatusReasonCode
          }
        }
      })
    );
  }
);

export const saveAccountExternalStatusBuilder = (
  builder: ActionReducerMapBuilder<AccountExternalStatusState>
) => {
  builder.addCase(triggerOnSaveData.pending, (draftState, action) => {
    draftState.loading = true;
  });
  builder.addCase(triggerOnSaveData.rejected, (draftState, action) => {
    draftState.loading = false;
  });
};

export const triggerChangeTabEvent = createAsyncThunk<
  unknown,
  SaveChangeDataArgs,
  ThunkAPIConfig
>(
  'clientConfigAccountExternalStatus/triggerSaveChangeData',
  (args, thunkAPI) => {
    const { dispatch, getState } = thunkAPI;
    const isChanged = selectDataChange(getState());
    const { modalPayload } = args;

    if (isChanged) {
      dispatch(confirmModalActions.open(modalPayload));
    } else {
      dispatchControlTabChangeEvent();
    }
  }
);

export const triggerCloseTabEvent = createAsyncThunk<
  unknown,
  SaveChangeDataArgs,
  ThunkAPIConfig
>(
  'clientConfigAccountExternalStatus/triggerCloseTabEvent',
  (args, thunkAPI) => {
    const { dispatch, getState } = thunkAPI;
    const isChanged = selectDataChange(getState());
    const { modalPayload } = args;

    if (isChanged) {
      dispatch(confirmModalActions.open(modalPayload));
    } else {
      dispatchCloseModalEvent();
    }
  }
);

export const triggerOnSaveData = createAsyncThunk<
  unknown,
  undefined,
  ThunkAPIConfig
>(
  'clientConfigAccountExternalStatus/triggerOnSaveData',
  async (args, thunkAPI) => {
    const { getState, dispatch, rejectWithValue } = thunkAPI;
    const state = getState();
    const { reasonCodeList, accountExternalStatusList } =
      state?.accountExternalStatus;
    const {
      data: externalStatusReasonCode = [],
      originData: originExternalStatusReasonCode = []
    } = reasonCodeList;
    const {
      data: externalStatusCode = [],
      originData: originExternalStatusCode = []
    } = accountExternalStatusList;

    const response = await dispatch(
      clientConfigurationActions.saveCollectionsClientConfig({
        component: 'account-external-status-component',
        jsonValue: {
          accountExternalStatus: {
            externalStatusCode,
            externalStatusReasonCode
          }
        },
        updateAction: 'delete'
      })
    );

    if (isFulfilled(response)) {
      batch(() => {
        dispatch(
          saveChangedClientConfig({
            action: 'UPDATE',
            changedCategory: 'accountExternalStatus',
            oldValue: {
              externalStatusCode: originExternalStatusCode,
              externalStatusReasonCode: originExternalStatusReasonCode
            },
            newValue: {
              externalStatusCode,
              externalStatusReasonCode
            }
          })
        );
        dispatch(accountExternalStatusActions.getAccountStatusCodeList());
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.SAVE_CHANGE_SUCCESS
          })
        );
      });
    }

    if (isRejected(response)) {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.SAVE_CHANGE_FAILURE
        })
      );
      return rejectWithValue({});
    }
  }
);
