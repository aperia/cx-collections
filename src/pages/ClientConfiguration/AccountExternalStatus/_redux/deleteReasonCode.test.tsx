import './reducers';
import { responseDefault, mockActionCreator } from 'app/test-utils';
import thunk from 'redux-thunk';

import { applyMiddleware, createStore } from '@reduxjs/toolkit';

//redux
import { rootReducer } from 'storeConfig';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

import { deleteReasonCode } from './deleteReasonCode';
import { clientConfigurationServices } from 'pages/ClientConfiguration/clientConfigurationServices';
import { I18N_ACCOUNT_EXTERNAL_STATUS_TEXT } from '../constants';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

const mockArgs = { id: '01' };

const toastSpy = mockActionCreator(actionsToast);
const clientConfigurationSpy = mockActionCreator(clientConfigurationActions);

describe('deleteReasonCode', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterAll(() => {
    jest.runAllTimers();
  });

  const store = createStore(
    rootReducer,
    {
      accountExternalStatus: {
        reasonCodeList: {
          data: [
            {
              id: '01',
              value: '01',
              description: 'test2',
              reasonCode: '01 - test2'
            }
          ],
          originData: [
            {
              id: '01',
              value: '01',
              description: 'test2',
              reasonCode: '01 - test2'
            }
          ],
          modalErrorMessage: '',
          modalLoading: false
        },
        accountExternalStatusList: {
          data: [{ checkList: '123' }],
          loading: false
        }
      },
      clientConfiguration: {
        settings: {
          selectedConfig: {
            id: '',
            agentId: 'agentId',
            clientId: 'clientId',
            systemId: 'systemId',
            principleId: 'principleId',
            userId: 'userId'
          }
        }
      }
    } as any,
    applyMiddleware(thunk)
  );

  it('success', async () => {
    const newStore = createStore(rootReducer, {
      accountExternalStatus: {
        reasonCodeList: {},
        accountExternalStatusList: {}
      }
    });

    const mockSaveCollectionsClientConfig = clientConfigurationSpy(
      'saveCollectionsClientConfig'
    );

    await deleteReasonCode(mockArgs)(jest.fn(), newStore.getState, {});

    expect(mockSaveCollectionsClientConfig).toBeCalledWith({
      component: 'account-external-status-component',
      jsonValue: {
        accountExternalStatus: {
          externalStatusCode: [],
          externalStatusReasonCode: []
        }
      }
    });
  });

  it('Fulfilled', async () => {
    const spy = toastSpy('addToast');

    Object.defineProperty(window, 'appConfig', {
      value: {
        commonConfig: {
          org: 'org',
          app: 'app',
          user: 'user'
        }
      }
    });
    jest
      .spyOn(clientConfigurationServices, 'saveCollectionsClientConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          configs: '123'
        }
      });
    const response = await deleteReasonCode({ id: '222' })(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'clientConfigAccountExternalStatus/deleteReasonCode/fulfilled'
    );
    expect(response.payload).toBeUndefined();
    expect(spy).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.REASON_CODE_EDIT_MESSAGE
    });
  });

  it('Rejected', async () => {
    const spy = toastSpy('addToast');
    const response = await deleteReasonCode(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.payload).toBeUndefined();
    expect(spy).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.REASON_CODE_DELETE_SERVER_ERROR
    });
  });

  it('pending', () => {
    const action = deleteReasonCode.pending(
      deleteReasonCode.pending.type,
      mockArgs
    );
    const actual = rootReducer(store.getState(), action).accountExternalStatus
      .reasonCodeList;

    expect(actual.modalLoading).toEqual(true);
  });

  it('fulfilled', () => {
    const action = deleteReasonCode.fulfilled(
      undefined,
      deleteReasonCode.pending.type,
      mockArgs
    );
    const actual = rootReducer(store.getState(), action).accountExternalStatus
      .reasonCodeList;

    expect(actual.modalLoading).toEqual(false);
  });

  it('rejected', () => {
    const action = deleteReasonCode.rejected(
      null,
      deleteReasonCode.pending.type,
      mockArgs
    );
    const actual = rootReducer(store.getState(), action).accountExternalStatus
      .reasonCodeList;

    expect(actual.modalLoading).toEqual(false);
  });
});
