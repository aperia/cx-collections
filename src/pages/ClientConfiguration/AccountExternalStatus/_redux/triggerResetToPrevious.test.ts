import { createStore, Store } from '@reduxjs/toolkit';

import { mockActionCreator } from 'app/test-utils';

import { rootReducer } from 'storeConfig';

import { triggerResetToPrevious } from './triggerResetToPrevious';

// Actions
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { accountExternalStatusActions } from './reducers';

const spyAccountExternalStatus = mockActionCreator(
  accountExternalStatusActions
);
const spyToast = mockActionCreator(actionsToast);

describe('triggerResetToPrevious', () => {
  const mockArgs = { toast: {} };

  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {});
  });

  it('Should not call actions when data was not changed', async () => {
    const mockResetToPrevious = spyAccountExternalStatus('resetToPrevious');
    const mockAddToast = spyToast('addToast');

    await triggerResetToPrevious(mockArgs)(() => false, store.getState, {});

    expect(mockResetToPrevious).not.toBeCalled();
    expect(mockAddToast).not.toBeCalled();
  });

  it('Should call actions when data was changed', async () => {
    const mockResetToPrevious = spyAccountExternalStatus('resetToPrevious');
    const mockAddToast = spyToast('addToast');
    store = createStore(rootReducer, {
      accountExternalStatus: {
        accountExternalStatusList: {},
        reasonCodeList: {
          data: [
            {
              description: 'reasonCode description 1',
              value: 'value',
              checkList: {
                value: { checked: true }
              }
            }
          ],
          originData: [
            {
              description: 'reasonCode description 1',
              value: 'value',
              checkList: {
                value: { checked: false }
              }
            }
          ]
        }
      }
    });
    await triggerResetToPrevious(mockArgs)(() => true, store.getState, {});

    expect(mockResetToPrevious).toBeCalled();
    expect(mockAddToast).toBeCalledWith(mockArgs.toast);
  });
});
