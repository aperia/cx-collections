import { selectorWrapper } from 'app/test-utils';
import { orderBy } from 'lodash';
import { AccountExternalStatusState } from '../types';
import {
  selectGetAccountExternalStatusLoading,
  selectedAccountExternalStatusData,
  selectedAccountExternalStatusListError,
  selectedAccountExternalStatusListLoading,
  selectedReasonCodeConfigurationData,
  selectedReasonCode,
  selectedReasonCodeConfigurationListModalErrorMessage,
  loadingReasonCodeConfiguration,
  takeAccountExternalStatus,
  takeAccountExternalUpdateStatus,
  reasonCodeModal,
  reasonCodeModalType,
  selectedReasonCodeConfigurationOriginData,
  selectedAccountExternalStatusOriginData
} from './selectors';

const mock = [
  { value: 'value', description: 'description' },
  { value: 'value 2', description: 'description 2' }
];

describe('Test Client Configuration Selectors', () => {
  const store: Partial<RootState> = {
    accountExternalStatus: {
      accountExternalStatusList: {
        data: [],
        error: '',
        loading: false
      },
      reasonCodeList: {
        loading: false,
        error: '',
        data: [],
        open: false,
        modalErrorMessage: '',
        modalType: '',
        modalLoading: false,
        selectedCode: {
          value: 'value',
          description: 'description'
        }
      },
      data: {
        externalCode: [],
        reasonCode: []
      },
      error: '',
      isUpdating: false
    } as AccountExternalStatusState
  };

  it('selectGetAccountExternalStatusLoading ', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectGetAccountExternalStatusLoading
    );

    expect(data).toEqual(false);
    expect(emptyData).toEqual(false);
  });

  it('selectedAccountExternalStatusData', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedAccountExternalStatusData
    );

    expect(data).toEqual([]);
    expect(emptyData).toEqual([]);
  });

  it('selectedAccountExternalStatusListError', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedAccountExternalStatusListError
    );

    expect(data).toEqual('');
    expect(emptyData).toEqual(undefined);
  });

  it('selectedAccountExternalStatusListLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedAccountExternalStatusListLoading
    );

    expect(data).toEqual(false);
    expect(emptyData).toEqual(undefined);
  });

  it('selectedReasonCodeConfigurationData', () => {
    const { data, emptyData } = selectorWrapper({
      accountExternalStatus: {
        ...store.accountExternalStatus,
        reasonCodeList: {
          ...store.accountExternalStatus?.reasonCodeList,
          data: mock,
          sortBy: { id: 'reasonCode', order: undefined }
        }
      }
    } as any)(selectedReasonCodeConfigurationData);

    expect(data).toEqual([
      {
        description: 'description',
        value: 'value'
      },
      {
        description: 'description 2',
        value: 'value 2'
      }
    ]);
    expect(emptyData).toEqual([]);
  });

  it('selectedReasonCodeConfigurationData when order by reasonCode', () => {
    const { data, emptyData } = selectorWrapper({
      accountExternalStatus: {
        ...store.accountExternalStatus,
        reasonCodeList: {
          data: mock,
          sortBy: { id: 'reasonCode', order: 'desc' }
        }
      }
    } as any)(selectedReasonCodeConfigurationData);

    expect(data).toEqual(orderBy(mock, ['value'], ['desc']));
    expect(emptyData).toEqual([]);
  });

  it('selectedReasonCodeConfigurationData when order by checked field', () => {
    const { data, emptyData } = selectorWrapper({
      accountExternalStatus: {
        ...store.accountExternalStatus,
        reasonCodeList: {
          ...store.accountExternalStatus?.reasonCodeList,
          data: mock,
          sortBy: { id: 'checked', order: '' }
        }
      }
    } as any)(selectedReasonCodeConfigurationData);

    expect(data).toEqual(mock);
    expect(emptyData).toEqual([]);
  });

  it('selectedReasonCode', () => {
    const { data, emptyData } = selectorWrapper(store)(selectedReasonCode);

    expect(data).toEqual({
      value: 'value',
      description: 'description'
    });
    expect(emptyData).toEqual(undefined);
  });

  it('selectedReasonCodeConfigurationListModalErrorMessage', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedReasonCodeConfigurationListModalErrorMessage
    );

    expect(data).toEqual('');
    expect(emptyData).toEqual(undefined);
  });

  it('loadingReasonCodeConfiguration', () => {
    const { data, emptyData } = selectorWrapper(store)(
      loadingReasonCodeConfiguration
    );

    expect(data).toEqual(false);
    expect(emptyData).toEqual(undefined);
  });

  it('takeAccountExternalStatus', () => {
    const { data, emptyData } = selectorWrapper(store)(
      takeAccountExternalStatus
    );

    expect(data).toEqual({ externalCode: [], reasonCode: [] });
    expect(emptyData).toEqual(undefined);
  });

  it('takeAccountExternalUpdateStatus', () => {
    const { data, emptyData } = selectorWrapper(store)(
      takeAccountExternalUpdateStatus
    );

    expect(data).toEqual(false);
    expect(emptyData).toEqual(undefined);
  });

  it('reasonCodeModal', () => {
    const { data, emptyData } = selectorWrapper(store)(reasonCodeModal);

    expect(data).toEqual(false);
    expect(emptyData).toEqual(undefined);
  });

  it('reasonCodeModalType', () => {
    const { data, emptyData } = selectorWrapper(store)(reasonCodeModalType);

    expect(data).toEqual('');
    expect(emptyData).toEqual('');
  });

  it('selectedAccountExternalStatusOriginData', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedAccountExternalStatusOriginData
    );

    expect(data).toEqual([]);
    expect(emptyData).toEqual([]);
  });

  it('selectedReasonCodeConfigurationOriginData', () => {
    const { data } = selectorWrapper({
      accountExternalStatus: {
        ...store.accountExternalStatus,
        reasonCodeList: {
          ...store.accountExternalStatus?.reasonCodeList,
          originData: mock,
          sortBy: { id: 'checked', order: '' }
        }
      }
    } as any)(selectedReasonCodeConfigurationOriginData);
    expect(data).toEqual([
      {
        description: 'description',
        value: 'value'
      },
      {
        description: 'description 2',
        value: 'value 2'
      }
    ]);
  });
});
