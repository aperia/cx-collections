import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';

//types
import { AccountExternalStatusState } from '../types';

//constants
import { I18N_ACCOUNT_EXTERNAL_STATUS_TEXT } from '../constants';

//redux
import { batch } from 'react-redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

//reducers
import { accountExternalStatusActions } from './reducers';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

export interface DeleteReasonCodeArg {
  id: string;
}

export const deleteReasonCode = createAsyncThunk<
  void,
  DeleteReasonCodeArg,
  ThunkAPIConfig
>(
  'clientConfigAccountExternalStatus/deleteReasonCode',
  async (args, thunkAPI) => {
    const { getState, dispatch } = thunkAPI;
    const state = getState();
    const { reasonCodeList, accountExternalStatusList } =
      state?.accountExternalStatus;
    const {
      originData: reasonCodeOriginData = [],
      data: reasonCodeChangedData = []
    } = reasonCodeList;
    const { originData: externalStatusCode = [] } = accountExternalStatusList;

    const { id } = args;

    const newExternalStatusReasonCode = reasonCodeOriginData.filter(
      item => item.value !== id
    );

    const response = await dispatch(
      clientConfigurationActions.saveCollectionsClientConfig({
        component: 'account-external-status-component',
        jsonValue: {
          accountExternalStatus: {
            externalStatusCode,
            externalStatusReasonCode: newExternalStatusReasonCode
          }
        }
      })
    );
    if (isFulfilled(response)) {
      const reasonCode = reasonCodeOriginData.find(item => item.value === id);
      batch(() => {
        dispatch(
          accountExternalStatusActions.updateOriginReasonCode({
            data: newExternalStatusReasonCode
          })
        );
        dispatch(
          accountExternalStatusActions.updateReasonCodeExternalList({
            data: reasonCodeChangedData.filter(item => item.value !== id)
          })
        );

        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.REASON_CODE_EDIT_MESSAGE
          })
        );

        dispatch(
          changeHistoryActions.saveChangedClientConfig({
            action: 'DELETE',
            oldValue: {
              value: reasonCode?.value,
              description: reasonCode?.description
            },
            changedItem: {
              value: `${reasonCode?.value} - ${reasonCode?.description}`
            },
            changedCategory: 'accountExternalStatusReasonCode'
          })
        );

        dispatch(
          accountExternalStatusActions.toggleReasonCode({ modalType: 'edit' })
        );
      });
      return;
    }

    if (isRejected(response)) {
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'error',
            message:
              I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.REASON_CODE_DELETE_SERVER_ERROR
          })
        );
      });
    }
  }
);

export const deleteReasonCodeBuilder = (
  builder: ActionReducerMapBuilder<AccountExternalStatusState>
) => {
  builder
    .addCase(deleteReasonCode.pending, (draftState, action) => {
      draftState.reasonCodeList = {
        ...draftState.reasonCodeList,
        modalLoading: true
      };
    })
    .addCase(deleteReasonCode.fulfilled, (draftState, action) => {
      draftState.reasonCodeList = {
        ...draftState.reasonCodeList,
        modalLoading: false
      };
    })
    .addCase(deleteReasonCode.rejected, (draftState, action) => {
      draftState.reasonCodeList = {
        ...draftState.reasonCodeList,
        modalLoading: false
      };
    });
};
