import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';

//services
import { accountExternalStatusServices } from '../accountExternalStatusServices';

//types
import { AccountExternalStatusState, ReasonCodeRequest } from '../types';

//constants
import { I18N_ACCOUNT_EXTERNAL_STATUS_TEXT } from '../constants';

//redux
import { batch } from 'react-redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

//reducers
import { accountExternalStatusActions } from './reducers';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

export interface EditReasonCodeArg extends ReasonCodeRequest, MagicKeyValue {}

export const editReasonCodeAPI = createAsyncThunk<
  void,
  EditReasonCodeArg,
  ThunkAPIConfig
>(
  'clientConfigAccountExternalStatus/editReasonCodeAPI',
  async (args, thunkAPI) => {
    try {
      await accountExternalStatusServices.editReasonCodeConfigList(args);
    } catch (error) {
      return thunkAPI.rejectWithValue({ response: error });
    }
  }
);

export const editReasonCode = createAsyncThunk<
  void,
  EditReasonCodeArg,
  ThunkAPIConfig
>(
  'clientConfigAccountExternalStatus/editReasonCode',
  async (args, thunkAPI) => {
    const { getState, dispatch } = thunkAPI;
    const state = getState();
    const { reasonCodeList, accountExternalStatusList } =
      state?.accountExternalStatus;
    const {
      originData: reasonCodeOriginData = [],
      data: reasonCodeChangedData = []
    } = reasonCodeList;
    const { originData: externalStatusCodeOrigin = [] } =
      accountExternalStatusList;

    const { reasonCode, description, id } = args;

    if (
      id !== reasonCode &&
      reasonCodeOriginData?.find(item => item.value === reasonCode)
    ) {
      dispatch(
        accountExternalStatusActions.setAddReasonCodeErrorMessage(
          I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.ERROR_REASON_CODE_DUPLICATE_MESSAGE
        )
      );

      return;
    }

    const newReasonCodeOriginData = reasonCodeOriginData.map(item => {
      if (item.value === id) {
        return { ...item, value: reasonCode, description };
      }

      return item;
    });

    const response = await dispatch(
      clientConfigurationActions.saveCollectionsClientConfig({
        component: 'account-external-status-component',
        jsonValue: {
          accountExternalStatus: {
            externalStatusCode: externalStatusCodeOrigin,
            externalStatusReasonCode: newReasonCodeOriginData
          }
        }
      })
    );

    if (isFulfilled(response)) {
      const oldValue: MagicKeyValue | undefined = reasonCodeOriginData.find(
        item => item.value === id
      );

      batch(() => {
        dispatch(
          accountExternalStatusActions.updateOriginReasonCode({
            data: newReasonCodeOriginData
          })
        );
        const newReasonCodeChangedData = reasonCodeChangedData.map(item => {
          if (item.value === id) {
            return { ...item, value: reasonCode, description };
          }

          return item;
        });
        dispatch(
          accountExternalStatusActions.updateReasonCodeExternalList({
            data: newReasonCodeChangedData
          })
        );

        dispatch(
          changeHistoryActions.saveChangedClientConfig({
            action: 'UPDATE',
            oldValue: {
              value: oldValue?.value,
              description: oldValue?.description
            },
            newValue: {
              value: reasonCode,
              description
            },
            changedItem: {
              value: `${oldValue?.value} - ${oldValue?.description}`
            },
            changedCategory: 'accountExternalStatusReasonCode'
          })
        );

        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.REASON_CODE_EDIT_MESSAGE
          })
        );

        dispatch(
          accountExternalStatusActions.toggleReasonCode({ modalType: 'edit' })
        );
      });
      return;
    }

    if (isRejected(response)) {
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'error',
            message:
              I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.REASON_CODE_EDIT_SERVER_ERROR
          })
        );
      });
    }
  }
);

export const editReasonCodeBuilder = (
  builder: ActionReducerMapBuilder<AccountExternalStatusState>
) => {
  builder
    .addCase(editReasonCode.pending, (draftState, action) => {
      draftState.reasonCodeList = {
        ...draftState.reasonCodeList,
        modalLoading: true
      };
    })
    .addCase(editReasonCode.fulfilled, (draftState, action) => {
      draftState.reasonCodeList = {
        ...draftState.reasonCodeList,
        modalLoading: false
      };
    })
    .addCase(editReasonCode.rejected, (draftState, action) => {
      draftState.reasonCodeList = {
        ...draftState.reasonCodeList,
        modalLoading: false
      };
    });
};
