// types
import { SortType } from 'app/_libraries/_dls/components';
import { AccountExternalStatusState } from '../types';

import {
  accountExternalStatusActions,
  accountExternalStatus
} from './reducers';

describe('Test Client Configuration Reducers', () => {
  const initialState: AccountExternalStatusState = {
    accountExternalStatusList: {
      data: [],
      loading: false
    },
    reasonCodeList: {
      data: [],
      open: false,
      modalErrorMessage: '',
      modalType: '',
      modalLoading: false,
      selectedCode: {} as any
    },
    data: {
      externalCode: [],
      reasonCode: []
    },
    isUpdating: false
  };

  it('should toggle modal add to open', () => {
    const state = accountExternalStatus(
      initialState,
      accountExternalStatusActions.toggleReasonCode({ modalType: 'add' })
    );
    expect(state.reasonCodeList.modalType).toEqual('add');

    expect(state.reasonCodeList.open).toEqual(true);
  });
  it('should toggle modal delete to open', () => {
    const state = accountExternalStatus(
      initialState,
      accountExternalStatusActions.toggleReasonCode({ modalType: 'delete' })
    );
    expect(state.reasonCodeList.modalType).toEqual('delete');

    expect(state.reasonCodeList.open).toEqual(true);
  });
  it('should toggle modal edit to open', () => {
    const state = accountExternalStatus(
      initialState,
      accountExternalStatusActions.toggleReasonCode({ modalType: 'edit' })
    );
    expect(state.reasonCodeList.modalType).toEqual('edit');

    expect(state.reasonCodeList.open).toEqual(true);
  });

  it('should setAddReasonCodeErrorMessage empty', () => {
    const state = accountExternalStatus(
      initialState,
      accountExternalStatusActions.setAddReasonCodeErrorMessage('')
    );
    expect(state.reasonCodeList.modalErrorMessage).toEqual('');
  });

  it('should setAddReasonCodeErrorMessage to sth', () => {
    const state = accountExternalStatus(
      initialState,
      accountExternalStatusActions.setAddReasonCodeErrorMessage('TEST')
    );
    expect(state.reasonCodeList.modalErrorMessage).toEqual('TEST');
  });

  it('should resetToPrevious ', () => {
    const state = accountExternalStatus(
      initialState,
      accountExternalStatusActions.resetToPrevious()
    );

    expect(state.data.externalCode).toEqual([]);

    expect(state.data.reasonCode).toEqual([]);
  });

  it('should updateAccountExternalList ', () => {
    const state = accountExternalStatus(
      initialState,
      accountExternalStatusActions.updateAccountExternalList({
        data: ['1']
      })
    );

    expect(state.accountExternalStatusList.data).toEqual(['1']);
  });

  it('should updateReasonCodeExternalList ', () => {
    const state = accountExternalStatus(
      initialState,
      accountExternalStatusActions.updateReasonCodeExternalList({
        data: ['1']
      })
    );

    expect(state.reasonCodeList.data).toEqual(['1']);
  });

  it('should changeReasonCodeExternalSortBy ', () => {
    const sortBy: SortType = { id: 'test', order: 'desc' };
    const state = accountExternalStatus(
      initialState,
      accountExternalStatusActions.changeReasonCodeExternalSortBy(sortBy)
    );

    expect(state.reasonCodeList.sortBy).toEqual(sortBy);
  });
});
