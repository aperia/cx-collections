import { createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// Actions
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { accountExternalStatusActions } from './reducers';

// Type
import { PreviousDataArgs } from '../types';

// Selector
import { selectDataChange } from './selectors';

export const triggerResetToPrevious = createAsyncThunk<
  unknown,
  PreviousDataArgs,
  ThunkAPIConfig
>(
  'clientConfigAccountExternalStatus/triggerResetToPrevious',
  (args, thunkAPI) => {
    const { dispatch, getState } = thunkAPI;
    const isChanged = selectDataChange(getState());
    const { toast } = args;

    if (isChanged) {
      batch(() => {
        dispatch(accountExternalStatusActions.resetToPrevious());
        dispatch(actionsToast.addToast(toast));
      });
    }
  }
);
