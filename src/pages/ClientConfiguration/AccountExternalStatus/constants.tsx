//Account External Status
export const ACCOUNT_STATUS_GRID_COLUMNS = [
  ' ',
  'A',
  'B',
  'C',
  'E',
  'F',
  'I',
  'L',
  'U',
  'Z'
];
//REASON CODE CONFIG

export const REASON_CODE_LABELS = [
  { id: 'normal', label: 'Normal', width: 92 },
  { id: 'authProhibited', label: 'A - Auth Prohibited', width: 164 },
  { id: 'bankrupt', label: 'B - Bankrupt', width: 124 },
  { id: 'closed', label: 'C - Closed', width: 103 },
  { id: 'revoked', label: 'E - Revoked', width: 111 },
  { id: 'frozen', label: 'F - Frozen', width: 111 },
  {
    id: 'interestAccrualProhibited',
    label: 'I - Interest Accrual Prohibited',
    width: 238
  },
  { id: 'lost', label: 'L - Lost', width: 87 },
  { id: 'stolen', label: 'U - Stolen', width: 106 },
  { id: 'chargedOff', label: 'Z - Charged Off', width: 139 },
  { id: 'action', label: '', width: 0 }
];

//I18N ACCOUNT EXTERNAL STATUS & REASON CODE

export const I18N_ACCOUNT_EXTERNAL_STATUS_TEXT = {
  REASON_CODE_EDIT_SERVER_ERROR: 'txt_reason_code_edit_server_error',
  REASON_CODE_DELETE_SERVER_ERROR: 'txt_reason_code_delete_server_error',
  REASON_CODE_ADD_SERVER_ERROR: 'txt_reason_code_add_server_error',
  ACCOUNT_EXTERNAL_STATUS: 'txt_account_external_status',
  ACCOUNT_EXTERNAL_DESCRIPTION: 'txt_account_external_description',
  REASON_CODE_DESCRIPTION: 'txt_reason_code_description',
  ERROR_REASON_CODE_DUPLICATE_MESSAGE: 'txt_reason_code_duplicate_message',
  REASON_CODE_ADD_MESSAGE: 'txt_reason_code_add_message',
  REASON_CODE_DELETE_MESSAGE: 'txt_reason_code_delete_message',
  REASON_CODE_EDIT_MESSAGE: 'txt_reason_code_edit_message',
  CURRENT_STATUS_CONFIGURATION: 'txt_current_status_configuration',
  REASON_CODE_CONFIGURATION: 'txt_reason_code_configuration',
  ADD_REASON_CODE: 'txt_add_reason_code',
  EDIT_REASON_CODE: 'txt_edit_reason_code',
  DELETE_REASON_CODE: 'txt_delete_reason_code',
  MODAL_DELETE_CONTENT: 'txt_delete_modal_content',
  RESET_TO_PREVIOUS: 'txt_reset_to_previous',
  SAVE_CHANGES: 'txt_save_changes',
  CURRENT_STATUS: 'txt_current_status',
  AVAILABLE_STATUS: 'txt_available_status',
  REASON_CODE: 'txt_reason_code',
  REASON_CODE_NORMAL: 'txt_reason_code_normal',
  DISPLAY: 'txt_display',
  SELECTED_STATUS: 'txt_selected_status',
  ACTIONS: 'txt_actions',
  CANCEL: 'txt_cancel',
  SUBMIT: 'txt_submit',
  DELETE: 'txt_delete',
  SAVE: 'txt_save',
  NO_DATA: 'txt_no_data',
  UNSAVED_CHANGE_BODY: 'txt_unsaved_change_body',
  DISCARD_CHANGE_BODY: 'txt_discard_change_body',
  UNSAVED_CHANGE: 'txt_unsaved_changes',
  CONTINUE_EDITING: 'txt_continue_editing',
  SAVE_AND_CONTINUE: 'txt_save_and_continue',
  DISCARD_CHANGE: 'txt_discard_change',
  RESET_TO_PREVIOUS_SUCCESS: 'txt_reset_to_previous_success',
  SAVE_CHANGE_SUCCESS: 'txt_save_changes_success',
  SAVE_CHANGE_FAILURE: 'txt_save_changes_failure',
  REASON_IS_REQUIRED: 'txt_reason_is_required',
  EXTERNAL_STATUS_IS_REQUIRED: 'txt_external_status_is_required'
};
