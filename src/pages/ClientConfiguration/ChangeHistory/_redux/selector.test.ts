import { selectorWrapper } from 'app/test-utils';
import { initialFilterData } from '../constants';
import {
  selectedLoading,
  selectedFilter,
  selectedHistories,
  selectedChangedCategories,
  selectedCSPAList,
  selectedIsFilterReset,
  selectedDefaultFilter
} from './selectors';

const states: Partial<RootState> = {
  changeHistory: {
    defaultFilter: {},
    filterData: {
      changedCategoryList: [],
      cspaList: []
    },
    filter: initialFilterData,
    loading: true,
    isFiltered: false,
    isFilterReset: false,
    histories: [
      {
        date: 'date',
        time: 'time',
        changedBy: 'changedBy',
        cspa: 'cspa',
        action: 'action',
        changedCategory: 'changedCategory',
        changedSection: 'changedSection',
        changedItem: 'changedItem',
        before: 'before',
        after: 'after'
      }
    ],
    sortType: {
      id: 'dateTime',
      order: 'asc'
    }
  }
};

const selectorTrigger = selectorWrapper(states);

describe('Test selector', () => {
  it('Test empty change history', () => {
    const { data } = selectorWrapper({
      changeHistory: undefined
    })(selectedLoading);

    expect(data).toBeUndefined();
  });

  it('Test selectedLoading', () => {
    const { data } = selectorTrigger(selectedLoading);
    expect(data).toBeTruthy();
  });

  it('Test selectedFilter', () => {
    const { data } = selectorTrigger(selectedFilter);
    expect(data).toEqual(initialFilterData);
  });

  it('Test selectedHistories', () => {
    const { data } = selectorTrigger(selectedHistories);
    expect(data).toEqual(states.changeHistory?.histories);
  });

  it('Test selectedHistories', () => {
    const { data } = selectorWrapper({
      changeHistory: {
        histories: [
          {
            date: 'date',
            time: 'time',
            changedBy: 'changedBy',
            cspa: 'cspa',
            action: 'action',
            changedCategory: 'changedCategory',
            changedSection: 'changedSection',
            changedItem: 'changedItem',
            before: 'before',
            after: 'after'
          }
        ],
        sortType: {
          id: '123',
          order: 'asc'
        }
      }
    })(selectedHistories);
    expect(data).toEqual(states.changeHistory?.histories);
  });

  it('Test selectedChangedCategories', () => {
    const { data } = selectorTrigger(selectedChangedCategories);
    expect(data).toEqual(states.changeHistory?.filterData?.changedCategoryList);
  });

  it('Test selectedCSPAList', () => {
    const { data } = selectorTrigger(selectedCSPAList);
    expect(data).toEqual(states.changeHistory?.filterData?.cspaList);
  });

  it('Test selectedIsFilterReset', () => {
    const { data } = selectorTrigger(selectedIsFilterReset);
    expect(data).toBeFalsy();
  });

  it('Test selectedDefaultFilter', () => {
    const { data } = selectorTrigger(selectedDefaultFilter);
    expect(data).toEqual(states.changeHistory?.defaultFilter);
  });
});
