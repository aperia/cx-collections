import { responseDefault } from 'app/test-utils';
import * as getAllClientConfigList from 'pages/ClientConfiguration/_redux/getAllClientConfigList';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { changeHistoryServices } from '../changeHistoryServices';
import { ChangedCategory, CSPA } from '../types';
import { getFilterData } from './getFilterData';

describe('should test get filter data', () => {
  let spyGetChangeCategories: jest.SpyInstance;
  let spyGetCSPA: jest.SpyInstance;

  let store: Store<RootState>;
  let state: RootState;

  const mockGetAllClientConfigListApi = (
    responseStatus = 'fulfilled',
    payload?: any
  ) => {
    jest
      .spyOn(getAllClientConfigList, 'getAllClientConfigListApi')
      .mockReturnValue({
        type: 'type',
        meta: {
          arg: {},
          requestId: 'G81A4iyNpRXX5pukAn17G',
          requestStatus: responseStatus
        },
        payload: {
          data: {
            clientInfoList: []
          },
          ...payload
        },
        error: { message: 'error' }
      } as never);
  };

  beforeEach(() => {
    store = createStore(rootReducer, {
      changeHistory: {
        filterData: {
          changedCategoryList: [],
          cspaList: []
        }
      },
      mapping: {
        data: {
          changeHistoryFilter: {}
        }
      }
    });
    state = store.getState();
  });

  afterEach(() => {
    spyGetChangeCategories?.mockReset();
    spyGetChangeCategories?.mockRestore();

    spyGetCSPA?.mockReset();
    spyGetCSPA?.mockRestore();
  });

  it('should fetch data', async () => {
    spyGetChangeCategories = jest
      .spyOn(changeHistoryServices, 'getChangedCategories')
      .mockResolvedValue({ ...responseDefault });

    mockGetAllClientConfigListApi();

    const response = await getFilterData()(store.dispatch, store.getState, {});
    expect(response.payload).toEqual({
      changedCategories: [],
      cspaList: []
    });
  });

  it('should reject', async () => {
    spyGetChangeCategories = jest
      .spyOn(changeHistoryServices, 'getChangedCategories')
      .mockResolvedValue({ ...responseDefault });

    mockGetAllClientConfigListApi('rejected');

    const response = await getFilterData()(store.dispatch, store.getState, {});
    expect(response.payload).toEqual({ errorMessage: 'error' });
  });

  it('should fetch data without mapping', async () => {
    store = createStore(rootReducer, {
      changeHistory: {
        filterData: {
          changedCategoryList: [],
          cspaList: []
        }
      },
      mapping: {
        data: {}
      }
    });
    state = store.getState();

    spyGetChangeCategories = jest
      .spyOn(changeHistoryServices, 'getChangedCategories')
      .mockResolvedValue({ ...responseDefault });
    mockGetAllClientConfigListApi('fulfilled', {
      data: {}
    });

    const response = await getFilterData()(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({
      changedCategories: [],
      cspaList: []
    });
  });

  it('should response fulfilled', () => {
    const changedCategories: ChangedCategory[] = [],
      cspaList: CSPA[] = [];

    const fulfilled = getFilterData.fulfilled(
      { changedCategories, cspaList },
      getFilterData.fulfilled.type,
      undefined
    );

    const actual = rootReducer(state, fulfilled);

    expect(actual.changeHistory.filterData).toEqual({
      changedCategoryList: [],
      cspaList: []
    });
    expect(actual.changeHistory.filter).toEqual({
      rangeDate: undefined,
      changedCategoryList: [],
      cspaList: []
    });
    expect(actual.changeHistory.defaultFilter).toEqual({
      rangeDate: undefined,
      changedCategoryList: [],
      cspaList: []
    });
  });
});
