import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { getAllClientConfigListApi } from 'pages/ClientConfiguration/_redux/getAllClientConfigList';
import { changeHistoryServices } from '../changeHistoryServices';
import { ChangeHistoryState, GetFilterDataPayload } from '../types';

export const getFilterData = createAsyncThunk<
  GetFilterDataPayload,
  undefined,
  ThunkAPIConfig
>(
  'changeHistory/getFilterData',
  async (_, { getState, dispatch, rejectWithValue }) => {
    const state = getState();
    const [changedCategoriesResponse, CSPAListResponse] = await Promise.all([
      changeHistoryServices.getChangedCategories(),
      dispatch(getAllClientConfigListApi())
    ]);

    if (isFulfilled(CSPAListResponse)) {
      return {
        changedCategories: mappingDataFromArray(
          changedCategoriesResponse.data,
          state.mapping.data.changeHistoryCategoryFilter || {}
        ),
        cspaList: mappingDataFromArray(
          CSPAListResponse.payload.data.clientInfoList || [],
          state.mapping.data.changeHistoryCspaFilter || {}
        )
      };
    }

    return rejectWithValue({ errorMessage: CSPAListResponse.error.message });
  }
);

export const getFilterDataBuilder = (
  builder: ActionReducerMapBuilder<ChangeHistoryState>
) => {
  builder
    .addCase(getFilterData.fulfilled, (draftState, action) => {
      draftState.filterData = {
        changedCategoryList: action.payload.changedCategories,
        cspaList: action.payload.cspaList
      };
      draftState.filter = {
        rangeDate: undefined,
        changedCategoryList: action.payload.changedCategories,
        cspaList: action.payload.cspaList
      };
      draftState.defaultFilter = {
        rangeDate: undefined,
        changedCategoryList: action.payload.changedCategories,
        cspaList: action.payload.cspaList
      };
    })
    .addCase(getFilterData.rejected, (draftState, action) => {
      draftState.filterData = {
        changedCategoryList: [],
        cspaList: []
      };
    });
};
