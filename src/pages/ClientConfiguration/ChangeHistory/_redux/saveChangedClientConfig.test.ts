import { isFulfilled, isRejected } from '@reduxjs/toolkit';
import { responseDefault, byPassFulfilled } from 'app/test-utils';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { changeHistoryServices } from '../changeHistoryServices';
import { SaveChangedClientConfigArgs } from '../types';
import { saveChangedClientConfig } from './saveChangedClientConfig';
import set from 'lodash.set';
import cloneDeep from 'lodash.clonedeep';

const mockArgs: SaveChangedClientConfigArgs = {
  action: 'ADD',
  changedCategory: 'letterList',
  newValue: {
    letterId: '000'
  }
};

describe('should test save changed client config', () => {
  let store: Store<RootState>;
  let spySaveChangedHistory: jest.SpyInstance;
  let spySaveChangedClientConfig: jest.SpyInstance;

  beforeEach(() => {
    store = createStore(rootReducer, {
      clientConfiguration: {
        settings: {
          selectedConfig: {
            agentId: '',
            clientId: '',
            systemId: '',
            principleId: ''
          }
        }
      },
      changeHistory: {
        oldValue: {}
      }
    });

    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: {
        commonConfig: {
          operatorID: 'NSA1',
          privileges: '123'
        }
      }
    })
  });

  afterEach(() => {
    spySaveChangedHistory?.mockReset();
    spySaveChangedHistory?.mockRestore();
    spySaveChangedClientConfig?.mockReset();
    spySaveChangedClientConfig?.mockRestore();
  });

  it('should add data with empty config', async () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: {}
    })
    spySaveChangedClientConfig = jest
      .spyOn(changeHistoryServices, 'saveChangedHistory')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          status: 'success'
        }
      });

    const response = await saveChangedClientConfig(mockArgs)(
      byPassFulfilled(saveChangedClientConfig.fulfilled.type, {
        letterList: [
          {
            action: ['ADD']
          }
        ]
      }),
      store.getState,
      {}
    );

    expect(isFulfilled(response)).toBeFalsy();
  });

  it('should add data', async () => {
    spySaveChangedClientConfig = jest
      .spyOn(changeHistoryServices, 'saveChangedHistory')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          status: 'success'
        }
      });

    const response = await saveChangedClientConfig(mockArgs)(
      byPassFulfilled(saveChangedClientConfig.fulfilled.type, {
        letterList: [
          {
            action: ['ADD']
          }
        ]
      }),
      store.getState,
      {}
    );

    expect(isFulfilled(response)).toBeTruthy();
  });

  it('should add data with empty data', async () => {
    spySaveChangedClientConfig = jest
      .spyOn(changeHistoryServices, 'saveChangedHistory')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          status: 'success'
        }
      });

    const response = await saveChangedClientConfig(mockArgs)(
      byPassFulfilled(saveChangedClientConfig.fulfilled.type, {
        letterList: [
          {
            action: ['UPDATE']
          }
        ]
      }),
      store.getState,
      {}
    );

    expect(isRejected(response)).toBeTruthy();
  });

  it('should edit data', async () => {
    spySaveChangedClientConfig = jest
      .spyOn(changeHistoryServices, 'saveChangedHistory')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          status: 'success'
        }
      });

    const mockArgsWithDefault = cloneDeep(mockArgs);
    set(mockArgsWithDefault, ['action'], 'UPDATE');
    const response = await saveChangedClientConfig(mockArgsWithDefault)(
      byPassFulfilled(saveChangedClientConfig.fulfilled.type, {
        letterList: [
          {
            action: ['UPDATE']
          }
        ]
      }),
      store.getState,
      {}
    );

    expect(isFulfilled(response)).toBeTruthy();
  });

  it('should edit data with empty value', async () => {
    store = createStore(rootReducer, {
      changeHistory: {
        oldValue: ''
      }
    });
    spySaveChangedClientConfig = jest
      .spyOn(changeHistoryServices, 'saveChangedHistory')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          status: 'success'
        }
      });

    const mockArgsWithDefault = cloneDeep(mockArgs);
    set(mockArgsWithDefault, ['action'], 'UPDATE');
    set(mockArgsWithDefault, ['newValue'], '');
    const response = await saveChangedClientConfig(mockArgsWithDefault)(
      byPassFulfilled(saveChangedClientConfig.fulfilled.type, {
        letterList: [
          {
            action: ['UPDATE']
          }
        ]
      }),
      store.getState,
      {}
    );

    expect(isFulfilled(response)).toBeTruthy();
  });

  it('should delete data', async () => {
    spySaveChangedClientConfig = jest
      .spyOn(changeHistoryServices, 'saveChangedHistory')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          status: 'failed'
        }
      });

    const mockArgsWithDefault = cloneDeep(mockArgs);
    set(mockArgsWithDefault, ['action'], 'DELETE');
    set(mockArgsWithDefault, ['newValue'], '');
    const response = await saveChangedClientConfig(mockArgsWithDefault)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(isRejected(response)).toBeTruthy();
  });

  it('should delete data with empty data', async () => {
    spySaveChangedClientConfig = jest
      .spyOn(changeHistoryServices, 'saveChangedHistory')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          status: 'failed'
        }
      });

    const mockArgsWithDefault = cloneDeep(mockArgs);
    set(mockArgsWithDefault, ['action'], 'DELETE');
    set(mockArgsWithDefault, ['newValue'], '');
    const response = await saveChangedClientConfig(mockArgsWithDefault)(
      byPassFulfilled(saveChangedClientConfig.fulfilled.type),
      store.getState,
      {}
    );

    expect(isRejected(response)).toBeTruthy();
  });

  it('should reject', async () => {
    spySaveChangedClientConfig = jest
      .spyOn(changeHistoryServices, 'saveChangedHistory')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          status: 'failed'
        }
      });

    const mockArgsWithDefault = cloneDeep(mockArgs);
    set(mockArgsWithDefault, ['action'], 'UPDATE');
    const response = await saveChangedClientConfig(mockArgsWithDefault)(
      byPassFulfilled(saveChangedClientConfig.fulfilled.type, {
        letterList: [
          {
            action: ['UPDATE']
          }
        ]
      }),
      store.getState,
      {}
    );

    expect(isRejected(response)).toBeTruthy();
  });
});
