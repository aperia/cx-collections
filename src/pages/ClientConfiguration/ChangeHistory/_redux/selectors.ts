import { createSelector } from 'reselect';
import {
  ChangeHistoryState,
  HistoryFilter,
  ClientConfigurationHistory
} from '../types';
import orderBy from 'lodash.orderby';

const getChangeHistoryState = (states: RootState) => states.changeHistory || {};

const getChangeHistoryFilterData = (states: RootState) =>
  states.changeHistory?.filterData || {};

export const selectedLoading = createSelector(
  getChangeHistoryState,
  (data: ChangeHistoryState) => data.loading
);

export const selectedFilter = createSelector(
  getChangeHistoryState,
  (data: ChangeHistoryState) => data.filter || {}
);

export const selectedHistories = createSelector(
  getChangeHistoryState,
  (data: ChangeHistoryState) =>
    orderBy(
      data.histories,
      data.sortType.id === 'dateTime'
        ? [item => new Date(`${item.date} ${item.time}`)]
        : [
            item =>
              item[data.sortType.id as keyof ClientConfigurationHistory]
                ?.toString()
                .trim()
                .toLowerCase(),
            (item: any) => item.value?.toString().trim().toLowerCase()
          ],
      data.sortType.order
    )
);

export const selectedChangedCategories = createSelector(
  getChangeHistoryFilterData,
  (data: HistoryFilter) => data.changedCategoryList || []
);

export const selectedCSPAList = createSelector(
  getChangeHistoryFilterData,
  (data: HistoryFilter) => data.cspaList || []
);

export const selectedIsFilterReset = createSelector(
  getChangeHistoryState,
  (data: ChangeHistoryState) => data.isFilterReset || false
);

export const selectedDefaultFilter = createSelector(
  getChangeHistoryState,
  (data: ChangeHistoryState) => data.defaultFilter || {}
);
