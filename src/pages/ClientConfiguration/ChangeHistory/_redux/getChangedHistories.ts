import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isRejected
} from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { AxiosResponse } from 'axios';
import { format } from 'date-and-time';
import { changeHistoryServices } from '../changeHistoryServices';
import {
  ChangeHistoryState,
  ClientConfigurationHistory,
  GetChangedHistoriesApiArgs,
  GetChangedHistoriesArgs,
  GetChangedHistoriesPayload
} from '../types';

export const getChangedHistoriesApi = createAsyncThunk<
  AxiosResponse,
  GetChangedHistoriesArgs,
  ThunkAPIConfig
>('changeHistory/getChangedHistoriesApi', args => {
  const { privileges } = window.appConfig.commonConfig || {};
  const params: GetChangedHistoriesApiArgs = {
    common: {
      privileges
    }
  };

  // cspaList param
  if (args.cspaList) {
    const cspaList = [];
    for (const item of args.cspaList) {
      const cspa = `${item.clientId}-${item.systemId}-${item.principleId}-${item.agentId}`;
      cspaList.push(cspa);
    }
    params.cspaList = cspaList;
  }

  // changedCategoryList param
  if (args.changedCategoryList) {
    const changedCategoryList = [];
    for (const item of args.changedCategoryList) {
      changedCategoryList.push(item.value);
    }
    params.changedCategoryList = changedCategoryList;
  }

  // rangeDate param
  if (args.rangeDate) {
    params.startDate = format(args.rangeDate.start!, 'MM-DD-YYYY');
    params.endDate = format(args.rangeDate.end!, 'MM-DD-YYYY');
  }
  return changeHistoryServices.getChangedHistories(params);
});

export const getChangedHistories = createAsyncThunk<
  GetChangedHistoriesPayload,
  GetChangedHistoriesArgs,
  ThunkAPIConfig
>(
  'changeHistory/getChangedHistories',
  async (args, { getState, dispatch, rejectWithValue }) => {
    const state = getState();

    const response = await dispatch(getChangedHistoriesApi(args));

    if (isRejected(response))
      return rejectWithValue({ response: response.error });

    const historyDataMapped: ClientConfigurationHistory[] =
      mappingDataFromArray(
        response.payload.data.configChangeHistoryList,
        state.mapping?.data?.changeHistory || {}
      );

    return {
      histories: historyDataMapped
    };
  }
);

export const getChangedHistoriesBuilder = (
  builder: ActionReducerMapBuilder<ChangeHistoryState>
) => {
  builder
    .addCase(getChangedHistories.pending, draftState => {
      draftState.loading = true;
    })
    .addCase(getChangedHistories.fulfilled, (draftState, action) => {
      draftState.histories = action.payload.histories;
      draftState.loading = false;
    })
    .addCase(getChangedHistories.rejected, draftState => {
      draftState.loading = false;
    });
};
