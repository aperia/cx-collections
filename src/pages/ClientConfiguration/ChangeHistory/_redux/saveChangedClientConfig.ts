import { createAsyncThunk, isRejected } from '@reduxjs/toolkit';
import isEmpty from 'lodash.isempty';

import { changeHistoryServices } from '../changeHistoryServices';
import { generateHistoryItems, getCSPAConfig } from '../helpers';
import {
  SaveChangedClientConfigArgs,
  SaveChangedHistoryApiArgs
} from '../types';
import { getHistoryLabel } from './getHistoryLabel';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

export const saveChangedClientConfig = createAsyncThunk<
  undefined,
  SaveChangedClientConfigArgs,
  ThunkAPIConfig
>(
  'clientConfiguration/saveChangedClientConfig',
  async (args, { getState, rejectWithValue, dispatch }) => {
    try {
      const { action, changedCategory } = args;

      const cspaConfig = getCSPAConfig(args, getState());

      const { clientId, systemId, principleId, agentId } = cspaConfig;
      const cspa = `${clientId}-${systemId}-${principleId}-${agentId}`;
      const { privileges } = window.appConfig.commonConfig || {};
      // get historyLabel json
      const getHistoryLabelResponse = await dispatch(getHistoryLabel());

      if (
        isRejected(getHistoryLabelResponse) ||
        isEmpty(getHistoryLabelResponse.payload)
      )
        throw getHistoryLabelResponse;

      // find label list by action and section
      const historyLabelItem = getHistoryLabelResponse.payload?.[
        changedCategory
      ]?.find(item => item.action.indexOf(action) >= 0);

      if (!historyLabelItem) throw new Error('empty ref data');

      const configChangeHistoryList = generateHistoryItems({
        item: args,
        cspa,
        historyLabelItem
      });

      if (isEmpty(configChangeHistoryList)) return;
      // const { privileges } = window.appConfig.commonConfig;
      const requestBody: SaveChangedHistoryApiArgs = {
        common: {
          privileges
        },
        configChangeHistoryList
      };

      const response = await changeHistoryServices.saveChangedHistory(
        requestBody
      );

      if (response.data.status !== 'success') throw response;

      dispatch(
        clientConfigurationActions.setLastHistory(configChangeHistoryList[0])
      );
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);
