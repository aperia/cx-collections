import { responseDefault } from 'app/test-utils';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import { changeHistoryServices } from '../changeHistoryServices';
import { getChangedHistories } from './getChangedHistories';

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    org: 'org',
    app: 'app'
  }
};

describe('getChangedHistories', () => {
  const params = {
    rangeDate: {
      start: new Date(),
      end: new Date()
    },
    changedCategoryList: [
      {
        id: 'id',
        value: 'value',
        description: 'description'
      }
    ],
    userRole: 'CSPA',
    cspaList: [
      {
        id: 'id',
        value: 'value',
        clientId: 'clientId',
        systemId: 'systemId',
        principleId: 'principleId',
        agentId: 'agentId'
      }
    ]
  };

  it('should be error', async () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: undefined
    };
    jest.spyOn(changeHistoryServices, 'getChangedHistories').mockResolvedValue({
      ...responseDefault,
      data: {
        configChangeHistoryList: '123'
      }
    });

    const store = createStore(rootReducer, {}, applyMiddleware(thunk));

    const response = await getChangedHistories({})(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'changeHistory/getChangedHistories/fulfilled'
    );
  });

  it('should be fulfilled', async () => {
    jest.spyOn(changeHistoryServices, 'getChangedHistories').mockResolvedValue({
      ...responseDefault,
      data: {
        configChangeHistoryList: '123'
      }
    });

    const store = createStore(rootReducer, {}, applyMiddleware(thunk));

    const response = await getChangedHistories(params)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'changeHistory/getChangedHistories/fulfilled'
    );
  });

  it('should be fulfilled', async () => {
    jest.spyOn(changeHistoryServices, 'getChangedHistories').mockRejectedValue({
      ...responseDefault,
      data: null
    });

    const store = createStore(rootReducer, {}, applyMiddleware(thunk));

    const response = await getChangedHistories(params)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('changeHistory/getChangedHistories/rejected');
  });
});
