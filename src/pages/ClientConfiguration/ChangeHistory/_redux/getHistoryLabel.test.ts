import { isFulfilled, isRejected } from '@reduxjs/toolkit';
import { createStore } from 'redux';

import { rootReducer } from 'storeConfig';
import { responseDefault } from 'app/test-utils';
import { getHistoryLabel } from './getHistoryLabel';
import { changeHistoryServices } from '../changeHistoryServices';

describe('getHistoryLabel', () => {
  it('should be fulfilled', async () => {
    jest
      .spyOn(changeHistoryServices, 'getHistoryLabel')
      .mockResolvedValue(responseDefault);

    const store = createStore(rootReducer, {});

    const response = await getHistoryLabel()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(isFulfilled(response)).toBeTruthy();
  });

  it('should be rejected', async () => {
    jest
      .spyOn(changeHistoryServices, 'getHistoryLabel')
      .mockRejectedValue(responseDefault);

    const store = createStore(rootReducer, {});

    const response = await getHistoryLabel()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(isRejected(response)).toBeTruthy();
  });
});
