import { initialFilterData } from '../constants';
import { ChangeHistoryState, ClientConfigurationHistory } from '../types';
import { changeHistoryActions as actions, reducer } from './reducers';

const initialState: ChangeHistoryState = {
  filterData: initialFilterData,
  loading: true,
  isFiltered: false
};

const mockData: ClientConfigurationHistory[] = [
  {
    date: '01-01-2021',
    time: '00:00:00',
    action: 'ADD'
  },
  {
    date: '01-02-2021',
    time: '00:00:00',
    action: 'UPDATE'
  }
];

describe('should test change history reducers', () => {
  it('should test update filter', () => {
    const state = reducer(initialState, actions.updateFilter({}));

    expect(state.filter).toEqual({});
    expect(state.isFilterReset).toEqual(false);
  });

  it('should test reset filter', () => {
    const state = reducer(initialState, actions.resetFilter());

    expect(state.filter).toEqual({
      rangeDate: undefined,
      changedCategoryList: [],
      cspaList: []
    });
    expect(state.isFiltered).toEqual(false);
  });

  it('should test default sort by', () => {
    const state = reducer(initialState, actions.sortBy({ id: 'dateTime' }));

    expect(state.histories).toBeUndefined();
  });

  it('should test sort by date time', () => {
    const state = reducer(
      {
        ...initialState,
        histories: mockData
      },
      actions.sortBy({ id: 'dateTime', order: 'desc' })
    );

    expect(state.histories![0]).toEqual({
      action: 'ADD',
      date: '01-01-2021',
      time: '00:00:00'
    });
  });

  it('should test sort by other', () => {
    const state = reducer(
      {
        ...initialState,
        histories: mockData
      },
      actions.sortBy({ id: 'action', order: 'desc' })
    );

    expect(state.histories![0]).toEqual({
      action: 'ADD',
      date: '01-01-2021',
      time: '00:00:00'
    });
  });

  it('should test remove store', () => {
    const state = reducer(initialState, actions.reset());

    expect(state).toEqual({
      ...initialState, sortType: {
        id: 'dateTime',
        order: 'desc'
      }
    });
  });
});
