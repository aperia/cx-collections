import { createAsyncThunk } from '@reduxjs/toolkit';
import { changeHistoryServices } from '../changeHistoryServices';
import { GetHistoryLabelPayload } from '../types';

export const getHistoryLabel = createAsyncThunk<
  GetHistoryLabelPayload,
  undefined,
  ThunkAPIConfig
>('changeHistory/getHistoryLabel', async _ => {
  const response = await changeHistoryServices.getHistoryLabel();

  return response.data;
});
