import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { initialFilterData } from '../constants';
import { ChangeHistoryState, HistoryFilter } from '../types';
import { getFilterData, getFilterDataBuilder } from './getFilterData';
import {
  getChangedHistories,
  getChangedHistoriesBuilder
} from './getChangedHistories';
import { saveChangedClientConfig } from './saveChangedClientConfig';
import { SortType } from 'app/_libraries/_dls/components';

export const initialState: ChangeHistoryState = {
  filterData: initialFilterData,
  loading: true,
  isFiltered: false,
  sortType: {
    id: 'dateTime',
    order: 'desc'
  }
};

const { actions, reducer } = createSlice({
  name: 'changeHistory',
  initialState,
  reducers: {
    updateFilter: (draftState, action: PayloadAction<HistoryFilter>) => {
      draftState.filter = action.payload;
      draftState.isFilterReset = false;
    },
    resetFilter: draftState => {
      draftState.filter = {
        rangeDate: undefined,
        changedCategoryList: draftState.filterData?.changedCategoryList,
        cspaList: draftState.filterData?.cspaList
      };
      draftState.isFiltered = false;
      draftState.isFilterReset = true;
    },
    sortBy: (draftState, action: PayloadAction<SortType>) => {
      draftState.sortType = {
        ...action.payload,
        order: action.payload.order || 'desc'
      };
    },
    reset: () => initialState
  },
  extraReducers: builder => {
    getFilterDataBuilder(builder);
    getChangedHistoriesBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getFilterData,
  getChangedHistories,
  saveChangedClientConfig
};

export { allActions as changeHistoryActions, reducer };
