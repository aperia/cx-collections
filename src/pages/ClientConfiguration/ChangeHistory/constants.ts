import { HistoryFilterData } from './types';

export const initialFilterData: HistoryFilterData = {
  changedCategoryList: [],
  cspaList: []
};

export const FILTER_FORM_NAME = {
  DATE_RANGE: 'rangeDate',
  CHANGED_CATEGORY_LIST: 'changedCategoryList',
  CSPA_LIST: 'cspaList'
};
