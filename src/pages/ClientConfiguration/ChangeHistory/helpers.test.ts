import cloneDeep from 'lodash.clonedeep';

// utils
import {
  generateHistoryItems,
  mappingLabelFromObject,
  mappingLabelValueFromString,
  filterHistoryValidationSchema,
  getCSPAConfig,
  validateHistoryValue,
  checkTypeData,
  strWithMention,
  strIncludeMarkdown
} from './helpers';
import * as letterListTemplates from './template/letterList';
import * as authenticationTemplates from './template/authentication';
import * as adjustmentTransactionTemplates from './template/adjustmentTransaction';

// constants
import { GetFormattedHistoryData } from './types';

describe('Test helpers', () => {
  it('filterHistoryValidationSchema', () => {
    const validationSchema = filterHistoryValidationSchema();

    expect(validationSchema.fields.rangeDate.notRequired()).toBeTruthy();
    expect(validationSchema.fields.rangeDate.default(undefined)).toBeTruthy();
    expect(validationSchema.fields.rangeDate.fields.start.type).toEqual('date');
    expect(
      validationSchema.fields.rangeDate.fields.start.required()
    ).toBeTruthy();
    expect(validationSchema.fields.rangeDate.fields.end.type).toEqual('date');
    expect(
      validationSchema.fields.rangeDate.fields.end.required()
    ).toBeTruthy();
  });

  it('mappingLabelValueFromString', () => {
    const text = '{replaceMe}Text';
    const valueData: MagicKeyValue = {
      replaceMe: 'replacedWithThisString'
    };
    const expected = 'replacedWithThisStringText';

    expect(mappingLabelValueFromString(text, valueData)).toEqual(expected);
  });

  describe('mappingLabelFromObject', () => {
    it('inputs are empty', () => {
      expect(mappingLabelFromObject()).toEqual([]);
    });

    it('should return value', () => {
      const labelData: MagicKeyValue = { key: 'label' };
      const valueData: MagicKeyValue = { key: 'value' };
      const expected = ['label: value'];

      expect(mappingLabelFromObject(labelData, valueData)).toEqual(expected);
    });
  });

  describe('generateHistoryItems', () => {
    window.appConfig = {
      commonConfig: {}
    } as AppConfiguration;

    const args: GetFormattedHistoryData = {
      item: {
        action: 'ADD',
        changedCategory: 'timer' // default
      },
      historyLabelItem: {
        action: ['action'],
        category: 'category',
        section: 'section',
        item: 'item',
        value: { key: 'value' }
      },
      cspa: 'cspa'
    };

    it('should call getLetterListTemplate', () => {
      const _args = cloneDeep(args);
      _args.item.changedCategory = 'letterList';
      const spy = jest.spyOn(letterListTemplates, 'getLetterListTemplate');

      generateHistoryItems(_args);

      expect(spy).toHaveBeenCalledWith(_args);
    });

    it('should call getAuthenticationTemplate', () => {
      const _args = cloneDeep(args);
      _args.item.oldValue = {
        sourceConfig: [],
        questionConfig: []
      };
      _args.item.newValue = {
        sourceConfig: [],
        questionConfig: []
      };
      _args.item.changedCategory = 'authentication';
      const spy = jest.spyOn(
        authenticationTemplates,
        'getAuthenticationTemplate'
      );

      generateHistoryItems(_args);

      expect(spy).toHaveBeenCalledWith(_args);
    });

    it('should call getAdjustmentTransactionTemplate', () => {
      const _args = cloneDeep(args);
      _args.item.changedCategory = 'adjustmentTransaction';
      const spy = jest.spyOn(
        adjustmentTransactionTemplates,
        'getAdjustmentTransactionTemplate'
      );

      generateHistoryItems(_args);

      expect(spy).toHaveBeenCalledWith(_args);
    });
  });

  it('generateHistoryItems', () => {
    const data = {
      item: {
        action: 'ADD',
        changedCategory: 'accountExternalStatus',
        changedSection: {},
        changedItem: {},
        oldValue: [
          {
            value: 'value',
            description: 'des',
            checkList: []
          }
        ],
        newValue: [
          {
            value: 'value',
            description: 'des',
            checkList: []
          }
        ]
      },
      historyLabelItem: {
        action: ['ADD'],
        category: 'category',
        section: 'section',
        item: 'item',
        value: { key: '1' }
      },
      cspa: 'cspa'
    };
    generateHistoryItems(data);
    generateHistoryItems({
      ...data,
      item: {
        ...data.item,
        changedCategory: 'userRoleEntitlement'
      }
    });
    generateHistoryItems({
      ...data,
      item: {
        ...data.item,
        action: 'UPDATE',
        changedCategory: 'userRoleEntitlement'
      }
    });
    generateHistoryItems({
      ...data,
      item: {
        ...data.item,
        changedCategory: 'contactAccountStatusEntitlement'
      }
    });
    generateHistoryItems({
      ...data,
      item: {
        ...data.item,
        action: 'UPDATE',
        changedCategory: 'contactAccountStatusEntitlement'
      }
    });
    generateHistoryItems({
      ...data,
      item: {
        ...data.item,
        changedCategory: 'memos'
      }
    });
    generateHistoryItems({
      ...data,
      item: {
        ...data.item,
        changedCategory: 'promiseToPay'
      }
    });
    generateHistoryItems({
      ...data,
      item: {
        ...data.item,
        changedCategory: 'timer'
      }
    });
    generateHistoryItems({
      ...data,
      item: {
        ...data.item,
        changedCategory: ''
      }
    });
  });

  describe('getCSPAConfig', () => {
    const resultCompareGetCspaConfigFromValue = {
      clientId: '1111',
      systemId: '2222',
      principleId: '3333',
      agentId: '4444'
    };

    const argsAdd = {
      action: 'ADD',
      newValue: { cspa: '1111-2222-3333-4444' }
    } as any;

    const rootState = {
      configurationUserRoleEntitlement: {
        selectedCSPA: { id: '4444-3333-2222-1111' }
      },
      configurationContactAccountEntitlement: {
        selectedCSPA: { id: '4444-3333-2222-1111' }
      },
      clientConfiguration: { settings: {} }
    } as RootState;

    it('userRoleEntitlement', () => {
      const args = {
        ...argsAdd,
        changedCategory: 'userRoleEntitlement'
      };

      const resultAdd = getCSPAConfig(args, rootState);
      expect(resultAdd).toEqual(resultCompareGetCspaConfigFromValue);

      args.action = '';

      const result = getCSPAConfig(args, rootState);
      expect(result).toEqual(
        rootState.configurationUserRoleEntitlement.selectedCSPA
      );
    });

    it('userRoleEntitlement no newValue', () => {
      const args = {
        ...argsAdd,
        newValue: {},
        changedCategory: 'userRoleEntitlement'
      };

      const result = getCSPAConfig(args, rootState);
      expect(result).toEqual({
        clientId: undefined,
        systemId: undefined,
        principleId: undefined,
        agentId: undefined
      });
    });

    it('contactAccountStatusEntitlement', () => {
      const args = {
        ...argsAdd,
        changedCategory: 'contactAccountStatusEntitlement'
      };

      const resultAdd = getCSPAConfig(args, rootState);
      expect(resultAdd).toEqual(resultCompareGetCspaConfigFromValue);

      args.action = '';

      const result = getCSPAConfig(args, rootState);
      expect(result).toEqual(
        rootState.configurationUserRoleEntitlement.selectedCSPA
      );
    });

    it('clientConfiguration', () => {
      const args = {
        ...argsAdd,
        changedCategory: 'clientConfiguration'
      };

      const resultAdd = getCSPAConfig(args, rootState);
      expect(resultAdd).toEqual(resultCompareGetCspaConfigFromValue);
    });

    it('default', () => {
      const args = {
        ...argsAdd,
        action: '',
        changedCategory: 'clientConfiguration'
      };

      const result = getCSPAConfig(args, rootState);
      expect(result).toEqual({});
    });
  });

  it('validateHistoryValue ', () => {
    // Case 1
    const result = validateHistoryValue('123', '');
    expect(result).toEqual('123');

    // Case 2
    const result2 = validateHistoryValue('', '');
    expect(result2).toEqual(' ');
  });

  it('checkTypeData', () => {
    const result = checkTypeData('abc');
    expect(result).toEqual('NormalText');
  });

  it('checkTypeData > case 2', () => {
    const result = checkTypeData('## abc {Account Number}');
    expect(result).toEqual('MentionText');
  });

  it('checkTypeData > case 3', () => {
    const result = checkTypeData('## abc classes=""');
    expect(result).toEqual('MarkdownText');
  });

  it('strWithMention', () => {
    const result = strWithMention('abc {Account Number}');
    expect(result).toEqual(true);
  });

  it('strWithMention > case 2', () => {
    const result = strWithMention('abc');
    expect(result).toEqual(false);
  });

  it('strIncludeMarkdown', () => {
    const result = strIncludeMarkdown('aba ##abc');
    expect(result).toEqual(true);
  });
});
