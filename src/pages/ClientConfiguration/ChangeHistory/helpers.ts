import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import * as Yup from 'yup';
import { FILTER_FORM_NAME } from './constants';
import {
  ConfigChangeHistoryItem,
  GetCSPAConfig,
  GetFormattedHistoryData,
  TypeDataChangeHistory
} from './types';
import {
  getAdjustmentTransactionTemplate,
  getAuthenticationTemplate,
  getContactAccountEntitlementTemplate,
  getDefaultTemplate,
  getLetterListTemplate,
  getMemosTemplate,
  getPromiseToPayTemplate,
  getUserRoleEntitlementTemplate,
  getAccExternalStatusTemplate
} from './template';
import isEmpty from 'lodash.isempty';
import isNull from 'lodash.isnull';
import isUndefined from 'lodash.isundefined';
import { getTimerTemplate } from './template/timer';
import { SETTLEMENT_PAYMENT_MENTION_DATA } from '../SettlementPayment/constants';

export const filterHistoryValidationSchema = () => {
  return Yup.object().shape({
    [FILTER_FORM_NAME.DATE_RANGE]: Yup.object({
      start: Yup.date().required(
        I18N_COMMON_TEXT.RANGE_DATE_MUST_HAVE_FROM_TO_DATE
      ),
      end: Yup.date().required(
        I18N_COMMON_TEXT.RANGE_DATE_MUST_HAVE_FROM_TO_DATE
      )
    })
      .notRequired()
      .default(undefined)
  });
};

export const getCSPAConfig = (
  args: GetCSPAConfig,
  rootState: RootState
): CSPA => {
  const { action, changedCategory, newValue } = args;
  const {
    configurationContactAccountEntitlement,
    configurationUserRoleEntitlement,
    clientConfiguration
  } = rootState;

  const getCspaConfigFromValue = (value?: MagicKeyValue) => {
    const newCspa = value?.cspa?.split('-') || [];
    return {
      clientId: newCspa[0],
      systemId: newCspa[1],
      principleId: newCspa[2],
      agentId: newCspa[3]
    };
  };

  if (changedCategory === 'userRoleEntitlement') {
    if (action === 'ADD') {
      return getCspaConfigFromValue(newValue);
    }
    return configurationUserRoleEntitlement.selectedCSPA;
  } else if (changedCategory === 'contactAccountStatusEntitlement') {
    if (action === 'ADD') {
      return getCspaConfigFromValue(newValue);
    }
    return configurationContactAccountEntitlement.selectedCSPA;
  } else if (changedCategory === 'clientConfiguration' && action === 'ADD') {
    return getCspaConfigFromValue(newValue);
  }
  return clientConfiguration.settings?.selectedConfig || {};
};

export const mappingLabelValueFromString = (
  text: string,
  valueData: MagicKeyValue = {}
) => {
  Object.keys(valueData).forEach(key => {
    const replaceIndex = text.indexOf(`{${key}}`);
    replaceIndex >= 0 && (text = text.replace(`{${key}}`, valueData[key]));
  });
  return text;
};

export const mappingLabelFromObject = (
  labelData: MagicKeyValue = {},
  valueData: MagicKeyValue = {}
) => {
  if (isEmpty(labelData) || isEmpty(valueData)) return [];
  const sortedValue = Object.keys(valueData)
    .sort()
    .reduce((obj: MagicKeyValue, key) => {
      obj[key] = valueData[key];
      return obj;
    }, {});
  const responseValue: string[] = [];
  Object.keys(sortedValue).forEach(key => {
    labelData[key] &&
      responseValue.push(`${labelData[key]}: ${sortedValue[key]}`);
  });
  return responseValue;
};

export const validateHistoryValue = (
  originValue?: string,
  formatData?: string
) => {
  if (
    isUndefined(originValue) ||
    isNull(originValue) ||
    isEmpty(originValue.toString().trim())
  ) {
    return ' ';
  }
  return formatData || originValue;
};

export const generateHistoryItems = (
  args: GetFormattedHistoryData
): ConfigChangeHistoryItem[] => {
  const { changedCategory, action } = args.item;

  switch (changedCategory) {
    case 'letterList':
      return getLetterListTemplate(args);
    case 'authentication':
      return getAuthenticationTemplate(args);
    case 'adjustmentTransaction':
      return getAdjustmentTransactionTemplate(args);
    case 'userRoleEntitlement':
      return action === 'UPDATE'
        ? getUserRoleEntitlementTemplate(args)
        : getDefaultTemplate(args);
    case 'contactAccountStatusEntitlement':
      return action === 'UPDATE'
        ? getContactAccountEntitlementTemplate(args)
        : getDefaultTemplate(args);
    case 'memos':
      return getMemosTemplate(args);
    case 'promiseToPay':
      return getPromiseToPayTemplate(args);
    case 'accountExternalStatus':
      return getAccExternalStatusTemplate(args);
    case 'timer':
      return getTimerTemplate(args);
    default:
      return getDefaultTemplate(args);
  }
};

export const checkTypeData = (value: string) => {
  if (strIncludeMarkdown(value)) {
    // Mention Text
    if (strWithMention(value)) return TypeDataChangeHistory[1];
    // MarkDown Text
    return TypeDataChangeHistory[0];
  }
  // Normal Text
  return TypeDataChangeHistory[2];
};

export const strWithMention = (value: string) => {
  for (const mentionItem of SETTLEMENT_PAYMENT_MENTION_DATA) {
    const mentionText = `{${mentionItem}}`;
    if (value.includes(mentionText)) {
      return true;
    }
  }
  return false;
};

export const strIncludeMarkdown = (value: string) => {
  const reg = new RegExp(/(__|\*|\#)/, 'gm');
  return reg.test(value) || value.includes('{classes=');
};
