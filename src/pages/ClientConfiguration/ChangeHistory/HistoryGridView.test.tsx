import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import HistoryGridView from './HistoryGridView';
import { GridProps } from 'app/_libraries/_dls/components';
import { screen } from '@testing-library/dom';
import { changeHistoryActions } from './_redux/reducers';

jest.mock('app/_libraries/_dls/components/Grid', () => (props: GridProps) => (
  <div>
    <button
      data-testid="sortButton"
      onClick={() => props.onSortChange!({ id: 'id', order: 'asc' })}
    >
      sort
    </button>
  </div>
));

const mockChangeHistoryActions = mockActionCreator(changeHistoryActions);

describe('should test history grid view', () => {
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<HistoryGridView />, { initialState });
  };

  const changeHistoryState: Partial<RootState> = {
    changeHistory: {
      histories: [
        { date: '09', before: 'test', after: 'test' },
        { date: '09', before: 'true', after: 'false' },
        { date: '09' },
        { date: '09' },
        { date: '09' },
        { date: '09' },
        { date: '09' },
        { date: '09' },
        { date: '09' },
        { date: '09' },
        { date: '09' }
      ],
      sortType: {
        id: 'dateTime'
      }
    }
  };

  it('should render UI', () => {
    const mockSortActions = mockChangeHistoryActions('sortBy');

    renderWrapper(changeHistoryState);

    const sort = screen.getByTestId('sortButton');
    sort.click();

    expect(mockSortActions).toBeCalledWith({ id: 'id', order: 'asc' });
  });

  it('should render no data', () => {
    const { wrapper } = renderWrapper({
      changeHistory: {
        histories: [],
        loading: false,
        sortType: {
          id: 'dateTime'
        }
      }
    });

    expect(wrapper.queryByText('txt_no_data')).toBeInTheDocument();
  });
});
