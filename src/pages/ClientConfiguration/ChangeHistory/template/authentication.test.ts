import * as authentication from './authentication';
import { formatTime } from 'app/helpers';
import { getTime } from 'app/test-utils/getTimeFormatMock';

describe('Test authentication', () => {
  window.appConfig = {
    commonConfig: {
      operatorID: 'abc'
    }
  } as any;

  it('getAuthenticationTemplate', () => {
    const mockArgs = {
      item: {
        oldValue: {
          sourceConfig: ['search_result'],
          questionConfig: ['mothers_maiden_name']
        } as any,
        newValue: {
          sourceConfig: ['abc', 'xyz'],
          questionConfig: ['ques1, ques2']
        } as any
      } as any,
      historyLabelItem: {} as any,
      cspa: ''
    };
    const result = authentication.getAuthenticationTemplate(mockArgs);
    expect(result).toEqual([
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Authentication Source Configuration',
        changedSection: 'Authentication Source Configuration',
        cspaLevel: '',
        date: formatTime(new Date().toString()).dateWithHyphen(),
        newValue: 'Search Result: false',
        oldValue: 'Search Result: true',
        operatorId: 'abc',
        time: getTime()
      },
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Authentication Question Configuration',
        changedSection: 'Authentication Question Configuration',
        cspaLevel: '',
        date: formatTime(new Date().toString()).dateWithHyphen(),
        newValue: "Mother's Maiden Name: false",
        oldValue: "Mother's Maiden Name: true",
        operatorId: 'abc',
        time: getTime()
      }
    ]);
  });
});
