import * as accountExternalStatus from './accountExternalStatus';
import { formatTime } from 'app/helpers';
import { getTime } from 'app/test-utils/getTimeFormatMock';

describe('Test accountExternalStatus', () => {
  window.appConfig = {
    commonConfig: {
      operatorID: 'abc'
    }
  } as any;
  it('getCurrentStatusTemplate > empty data', () => {
    const mockArgs = {
      oldExternalStatus: [],
      newExternalStatus: [],
      commonData: {}
    };
    const result = accountExternalStatus.getCurrentStatusTemplate(
      mockArgs.oldExternalStatus,
      mockArgs.newExternalStatus,
      mockArgs.commonData
    );
    expect(result).toEqual([]);
  });

  it('getCurrentStatusTemplate > contain data', () => {
    const mockArgs = {
      oldExternalStatus: [
        {
          value: 'B',
          description: 'Bankrupt',
          checkList: {
            B: {
              checked: false
            }
          }
        }
      ] as any,

      newExternalStatus: [
        {
          value: 'B',
          description: 'Bankrupt',
          checkList: {
            B: {
              checked: true
            }
          }
        }
      ] as any,
      commonData: {}
    };
    const result = accountExternalStatus.getCurrentStatusTemplate(
      mockArgs.oldExternalStatus,
      mockArgs.newExternalStatus,
      mockArgs.commonData
    );
    expect(result).toEqual([
      {
        changedItem: 'Current Status: B - Bankrupt',
        changedSection: 'Current Status Configuration',
        newValue: 'B - Bankrupt: true',
        oldValue: 'B - Bankrupt: false'
      }
    ]);
  });

  it('getCurrentStatusTemplate > contain data > same data', () => {
    const mockArgs = {
      oldExternalStatus: [
        {
          value: 'B',
          description: 'Bankrupt',
          checkList: {
            B: {
              checked: false
            }
          }
        }
      ] as any,

      newExternalStatus: [
        {
          value: 'B',
          description: 'Bankrupt',
          checkList: {
            B: {
              checked: false
            }
          }
        }
      ] as any,
      commonData: {}
    };
    const result = accountExternalStatus.getCurrentStatusTemplate(
      mockArgs.oldExternalStatus,
      mockArgs.newExternalStatus,
      mockArgs.commonData
    );
    expect(result).toEqual([]);
  });

  it('getCurrentStatusTemplate > contain data > not contain value', () => {
    const mockArgs = {
      oldExternalStatus: [
        {
          value: ' ',
          description: 'Normal',
          checkList: {}
        },
        {
          value: 'B',
          description: 'Bankrupt',
          checkList: {
            B: {
              checked: true
            }
          }
        }
      ] as any,

      newExternalStatus: [
        {
          value: ' ',
          description: 'Normal',
          checkList: {}
        },
        {
          value: 'B',
          description: 'Bankrupt',
          checkList: {
            B: {
              checked: false
            }
          }
        }
      ] as any,
      commonData: {}
    };
    const result = accountExternalStatus.getCurrentStatusTemplate(
      mockArgs.oldExternalStatus,
      mockArgs.newExternalStatus,
      mockArgs.commonData
    );
    expect(result).toEqual([
      {
        changedItem: 'Current Status: B - Bankrupt',
        changedSection: 'Current Status Configuration',
        newValue: 'B - Bankrupt: false',
        oldValue: 'B - Bankrupt: true'
      }
    ]);
  });

  it('getReasonCodeTemplate > empty data', () => {
    const mockArgs = {
      oldReasonCode: [],
      newReasonCode: [],
      commonData: {}
    };
    const result = accountExternalStatus.getReasonCodeTemplate(
      mockArgs.oldReasonCode,
      mockArgs.newReasonCode,
      mockArgs.commonData
    );
    expect(result).toEqual([]);
  });

  it('getReasonCodeTemplate > contain data', () => {
    const mockArgs = {
      oldReasonCode: [
        {
          value: 'B',
          description: 'Bankrupt',
          checkList: {
            B: {
              checked: false
            }
          }
        }
      ] as any,
      newReasonCode: [
        {
          value: 'B',
          description: 'Bankrupt',
          checkList: {
            B: {
              checked: true
            }
          }
        }
      ] as any,
      commonData: {}
    };
    const result = accountExternalStatus.getReasonCodeTemplate(
      mockArgs.oldReasonCode,
      mockArgs.newReasonCode,
      mockArgs.commonData
    );
    expect(result).toEqual([
      {
        changedItem: 'Reason Code: B - Bankrupt',
        changedSection: 'Reason Code Configuration',
        newValue: 'B - Bankrupt: true',
        oldValue: 'B - Bankrupt: false'
      }
    ]);
  });

  it('getReasonCodeTemplate > contain data > same data', () => {
    const mockArgs = {
      oldReasonCode: [
        {
          value: 'B',
          description: 'Bankrupt',
          checkList: {
            B: {
              checked: false
            }
          }
        }
      ] as any,
      newReasonCode: [
        {
          value: 'B',
          description: 'Bankrupt',
          checkList: {
            B: {
              checked: false
            }
          }
        }
      ] as any,
      commonData: {}
    };
    const result = accountExternalStatus.getReasonCodeTemplate(
      mockArgs.oldReasonCode,
      mockArgs.newReasonCode,
      mockArgs.commonData
    );
    expect(result).toEqual([]);
  });

  it('getReasonCodeTemplate > contain data > key == display', () => {
    const mockArgs = {
      oldReasonCode: [
        {
          value: 'A',
          description: 'xyz',
          checkList: {
            display: {
              checked: false
            }
          }
        }
      ] as any,
      newReasonCode: [
        {
          value: 'A',
          description: 'xyz',
          checkList: {
            display: {
              checked: true
            }
          }
        }
      ] as any,
      commonData: {}
    };
    const result = accountExternalStatus.getReasonCodeTemplate(
      mockArgs.oldReasonCode,
      mockArgs.newReasonCode,
      mockArgs.commonData
    );
    expect(result).toEqual([
      {
        changedItem: 'Reason Code: A - xyz',
        changedSection: 'Reason Code Configuration',
        newValue: 'Display: true',
        oldValue: 'Display: false'
      }
    ]);
  });

  it('getAccExternalStatusTemplate', () => {
    const mockArgs = {
      item: {
        oldValue: {
          externalStatusCode: [
            {
              value: 'B',
              description: 'Bankrupt',
              checkList: {
                B: {
                  checked: false
                }
              }
            }
          ]
        },
        newValue: {
          externalStatusCode: [
            {
              value: 'B',
              description: 'Bankrupt',
              checkList: {
                B: {
                  checked: true
                }
              }
            }
          ]
        }
      } as any,
      historyLabelItem: {} as any,
      cspa: ''
    };
    const result = accountExternalStatus.getAccExternalStatusTemplate({
      ...mockArgs
    });
    const today = new Date();

    expect(result).toEqual([
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Current Status: B - Bankrupt',
        changedSection: 'Current Status Configuration',
        cspaLevel: '',
        date: formatTime(today.toString()).dateWithHyphen(),
        newValue: 'B - Bankrupt: true',
        oldValue: 'B - Bankrupt: false',
        operatorId: 'abc',
        time: getTime()
      }
    ]);
  });

  it('should getAccExternalStatusTemplate return data', () => {
    const mockArgs = {
      item: {
        oldValue: {
          externalStatusCode: [
            {
              value: 'B',
              description: 'Bankrupt',
              checkList: {
                B: {
                  checked: false
                }
              }
            }
          ]
        },
        newValue: {
          externalStatusCode: [
            {
              value: 'B',
              description: 'Bankrupt',
              checkList: {
                B: {
                  checked: true
                }
              }
            }
          ]
        }
      } as any,
      historyLabelItem: {} as any,
      cspa: ''
    };
    const result = accountExternalStatus.getAccExternalStatusTemplate({
      ...mockArgs
    });
    const today = new Date();

    expect(result).toEqual([
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Current Status: B - Bankrupt',
        changedSection: 'Current Status Configuration',
        cspaLevel: '',
        date: formatTime(today.toString()).dateWithHyphen(),
        newValue: 'B - Bankrupt: true',
        oldValue: 'B - Bankrupt: false',
        operatorId: 'abc',
        time: getTime()
      }
    ]);
  });

  it('getAccExternalStatusTemplate > empty data', () => {
    const mockArgs = {
      item: {
        oldValue: {
          externalStatusCode: null
        },
        newValue: {
          externalStatusCode: null
        }
      } as any,
      historyLabelItem: {} as any,
      cspa: ''
    };
    const result = accountExternalStatus.getAccExternalStatusTemplate({
      ...mockArgs
    });

    expect(result).toEqual([]);
  });
});
