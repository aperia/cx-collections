import * as adjustmentTransaction from './adjustmentTransaction';
import { formatTime } from 'app/helpers';
import { getTime } from 'app/test-utils/getTimeFormatMock';

describe('Test adjustmentTransaction', () => {
  window.appConfig = {
    commonConfig: {
      operatorID: 'abc'
    }
  } as any;
  it('getAdjustmentTransactionTemplate > contain data', () => {
    const mockArgs = {
      item: {
        oldValue: [
          { originalTransactionCode: 'Original Trans Code' },
          { adjustmentPostingTranCode: 'Adjustment Posting Transaction Code' }
        ] as any,
        newValue: [{ key2: '456' }] as any
      } as any,
      historyLabelItem: {} as any,
      cspa: ''
    };
    const result = adjustmentTransaction.getAdjustmentTransactionTemplate({
      ...mockArgs
    });
    const today = new Date();
    expect(result).toEqual([
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Original Trans Code: Original Trans Code',
        changedSection: undefined,
        cspaLevel: '',
        date: formatTime(today.toString()).dateWithHyphen(),
        newValue: ' ',
        oldValue: 'Original Trans Code: Original Trans Code',
        operatorId: 'abc',
        time: getTime()
      },
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: ' ',
        changedSection: undefined,
        cspaLevel: '',
        date: formatTime(today.toString()).dateWithHyphen(),
        newValue: ' ',
        oldValue:
          'Adjustment Posting Transaction Code: Adjustment Posting Transaction Code',
        operatorId: 'abc',
        time: getTime()
      }
    ]);
  });

  it('getAdjustmentTransactionTemplate > contain data > case 2', () => {
    const mockArgs = {
      item: {
        oldValue: [
          {
            originalTransactionCode: 'Original Trans Code',
            adjustmentPostingTranCode: 'Adjustment Posting Transaction Code',
            adjustmentDescription: 'Description',
            adjustmentPostingBatchCode: 'Adjustment Posting Batch Code'
          },
          { adjustmentPostingTranCode: 'Adjustment Posting Transaction Code' }
        ] as any,
        newValue: [
          {
            adjustmentDescription: 'Description',
            adjustmentPostingTranCode: null,
            originalTransactionCode: 'aaa'
          },
          { adjustmentPostingBatchCode: 'Adjustment Posting Batch Code' }
        ] as any
      } as any,
      historyLabelItem: {} as any,
      cspa: ''
    };
    const result = adjustmentTransaction.getAdjustmentTransactionTemplate({
      ...mockArgs
    });
    const today = new Date();
    const todayTime = getTime();
    expect(result).toEqual([
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Original Trans Code: Original Trans Code',
        changedSection: undefined,
        cspaLevel: '',
        date: formatTime(today.toString()).dateWithHyphen(),
        newValue: 'Original Trans Code: aaa',
        oldValue: 'Original Trans Code: Original Trans Code',
        operatorId: 'abc',
        time: todayTime
      },
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Original Trans Code: Original Trans Code',
        changedSection: undefined,
        cspaLevel: '',
        date: formatTime(today.toString()).dateWithHyphen(),
        newValue: ' ',
        oldValue:
          'Adjustment Posting Transaction Code: Adjustment Posting Transaction Code',
        operatorId: 'abc',
        time: todayTime
      },
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Original Trans Code: Original Trans Code',
        changedSection: undefined,
        cspaLevel: '',
        date: formatTime(today.toString()).dateWithHyphen(),
        newValue: ' ',
        oldValue:
          'Adjustment Posting Batch Code: Adjustment Posting Batch Code',
        operatorId: 'abc',
        time: todayTime
      },
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: ' ',
        changedSection: undefined,
        cspaLevel: '',
        date: formatTime(today.toString()).dateWithHyphen(),
        newValue: ' ',
        oldValue:
          'Adjustment Posting Transaction Code: Adjustment Posting Transaction Code',
        operatorId: 'abc',
        time: todayTime
      }
    ]);
  });

  it('getAdjustmentTransactionTemplate > contain data > oldValue is null', () => {
    const mockArgs = {
      item: {
        oldValue: null as any,
        newValue: [
          {
            originalTransactionCode: 'Original Trans Code',
            adjustmentPostingTranCode: 'Adjustment Posting Transaction Code',
            adjustmentDescription: 'Description',
            adjustmentPostingBatchCode: 'Adjustment Posting Batch Code'
          }
        ] as any
      } as any,
      historyLabelItem: {} as any,
      cspa: ''
    };
    const result = adjustmentTransaction.getAdjustmentTransactionTemplate({
      ...mockArgs
    });
    const today = new Date();
    expect(result).toEqual([
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Original Trans Code: Original Trans Code',
        changedSection: undefined,
        cspaLevel: '',
        date: formatTime(today.toString()).dateWithHyphen(),
        newValue: 'Original Trans Code: Original Trans Code',
        oldValue: ' ',
        operatorId: 'abc',
        time: getTime()
      },
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Original Trans Code: Original Trans Code',
        changedSection: undefined,
        cspaLevel: '',
        date: formatTime(today.toString()).dateWithHyphen(),
        newValue:
          'Adjustment Posting Transaction Code: Adjustment Posting Transaction Code',
        oldValue: ' ',
        operatorId: 'abc',
        time: getTime()
      },
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Original Trans Code: Original Trans Code',
        changedSection: undefined,
        cspaLevel: '',
        date: formatTime(today.toString()).dateWithHyphen(),
        newValue: 'Description: Description',
        oldValue: ' ',
        operatorId: 'abc',
        time: getTime()
      },
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Original Trans Code: Original Trans Code',
        changedSection: undefined,
        cspaLevel: '',
        date: formatTime(today.toString()).dateWithHyphen(),
        newValue:
          'Adjustment Posting Batch Code: Adjustment Posting Batch Code',
        oldValue: ' ',
        operatorId: 'abc',
        time: getTime()
      }
    ]);
  });

  it('getAdjustmentTransactionTemplate > contain data > oldValue is null, saveChangeHistoryFields not include newValue', () => {
    const mockArgs = {
      item: {
        oldValue: null as any,
        newValue: [
          {
            key: '123'
          }
        ] as any
      } as any,
      historyLabelItem: {} as any,
      cspa: ''
    };
    const result = adjustmentTransaction.getAdjustmentTransactionTemplate({
      ...mockArgs
    });

    expect(result).toEqual([]);
  });

  it('getAdjustmentTransactionTemplate > contain data > oldItem === newItem', () => {
    const mockArgs = {
      item: {
        oldValue: [{ key1: '123' }] as any,
        newValue: [{ key1: '123' }] as any
      } as any,
      historyLabelItem: {} as any,
      cspa: ''
    };
    const result = adjustmentTransaction.getAdjustmentTransactionTemplate({
      ...mockArgs
    });

    expect(result).toEqual([]);
  });

  it('getAdjustmentTransactionTemplate > contain null data', () => {
    const mockArgs = {
      item: {
        oldValue: null,
        newValue: null
      } as any,
      historyLabelItem: {} as any,
      cspa: ''
    };
    const result = adjustmentTransaction.getAdjustmentTransactionTemplate({
      ...mockArgs
    });

    expect(result).toEqual([]);
  });
});
