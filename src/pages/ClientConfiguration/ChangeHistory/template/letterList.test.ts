import * as letterList from './letterList';
import { formatTime } from 'app/helpers';
import { getTime } from 'app/test-utils/getTimeFormatMock';

describe('Test letterList', () => {
  window.appConfig = {
    commonConfig: {
      operatorID: 'abc'
    }
  } as any;

  it('getLetterListTemplate', () => {
    const mockArgs = {
      item: {
        oldValue: {
          captionMapping: {
            key: '123'
          }
        } as any,
        newValue: {
          captionMapping: {
            key: '456'
          }
        } as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result = letterList.getLetterListTemplate(mockArgs);
    expect(result).toEqual([
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'entitlement',
        changedSection: 'entitlement',
        cspaLevel: '',
        date: formatTime(new Date().toString()).dateWithHyphen(),
        newValue: 'Generate Letter Field Mapping 1: 456',
        oldValue: 'Generate Letter Field Mapping 1: 123',
        operatorId: 'abc',
        time: getTime()
      }
    ]);
  });

  it('getLetterListTemplate > case 2', () => {
    const mockArgs = {
      item: {
        oldValue: {
          captionMapping: {
            key: undefined
          }
        } as any,
        newValue: {
          captionMapping: {
            key: undefined
          }
        } as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result = letterList.getLetterListTemplate(mockArgs);
    expect(result).toEqual([]);
  });

  it('getLetterListTemplate > case 3', () => {
    const mockArgs = {
      item: {
        oldValue: {
          captionMapping: {
            key: false
          },
          letterId: 'letterId',
          letterCode: false,
          letterDescription: 'letterDescription',
          variableLetter: 'variableLetter'
        } as any,
        newValue: {
          captionMapping: {
            key: false
          },
          letterId: 'letterIdXXX',
          letterCode: null,
          letterDescription: 'letterDescriptionXXX',
          variableLetter: 'variableLetterXXX'
        } as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result = letterList.getLetterListTemplate(mockArgs);
    expect(result).toEqual([
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'entitlement',
        changedSection: 'entitlement',
        cspaLevel: '',
        date: formatTime(new Date().toString()).dateWithHyphen(),
        newValue: 'Letter Number: letterIdXXX',
        oldValue: 'Letter Number: letterId',
        operatorId: 'abc',
        time: getTime()
      },
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'entitlement',
        changedSection: 'entitlement',
        cspaLevel: '',
        date: formatTime(new Date().toString()).dateWithHyphen(),
        newValue: ' ',
        oldValue: ' ',
        operatorId: 'abc',
        time: getTime()
      },
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'entitlement',
        changedSection: 'entitlement',
        cspaLevel: '',
        date: formatTime(new Date().toString()).dateWithHyphen(),
        newValue: 'Description: letterDescriptionXXX',
        oldValue: 'Description: letterDescription',
        operatorId: 'abc',
        time: getTime()
      },
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'entitlement',
        changedSection: 'entitlement',
        cspaLevel: '',
        date: formatTime(new Date().toString()).dateWithHyphen(),
        newValue: 'Letter Variables: variableLetterXXX',
        oldValue: 'Letter Variables: variableLetter',
        operatorId: 'abc',
        time: getTime()
      }
    ]);
  });

  it('getLetterListTemplate > case 3: oldValue, newValue: undefined', () => {
    const mockArgs = {
      item: {
        oldValue: undefined as any,
        newValue: undefined as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result = letterList.getLetterListTemplate(mockArgs);
    expect(result).toEqual([]);
  });

  it('getLetterListTemplate > case 4: captionMapping is undefined', () => {
    const mockArgs = {
      item: {
        oldValue: {
          captionMapping: undefined
        } as any,
        newValue: {
          captionMapping: undefined
        } as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result = letterList.getLetterListTemplate(mockArgs);
    expect(result).toEqual([]);
  });
});
