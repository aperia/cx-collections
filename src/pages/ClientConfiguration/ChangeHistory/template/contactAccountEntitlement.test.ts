import * as contactAccountEntitlement from './contactAccountEntitlement';
import { formatTime } from 'app/helpers';
import { getTime } from 'app/test-utils/getTimeFormatMock';

describe('Test contactAccountEntitlement', () => {
  window.appConfig = {
    commonConfig: {
      operatorID: 'abc'
    }
  } as any;

  it('getContactAccountEntitlementTemplate', () => {
    const mockArgs = {
      item: {
        oldValue: [
          {
            criteriaCode: 'abc',
            items: [{ value: '', active: false, description: 'description' }]
          }
        ] as any,
        newValue: [
          {
            criteriaCode: 'abc',
            items: [{ value: '123', active: true, description: 'description' }]
          }
        ] as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result =
      contactAccountEntitlement.getContactAccountEntitlementTemplate(mockArgs);
    expect(result).toEqual([
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'abc - entitlement',
        changedSection: 'entitlement',
        cspaLevel: '',
        date: formatTime(new Date().toString()).dateWithHyphen(),
        newValue: '123 - description: true',
        oldValue: 'description: false',
        operatorId: 'abc',
        time: getTime()
      }
    ]);
  });

  it('getContactAccountEntitlementTemplate > case Equal', () => {
    const mockArgs = {
      item: {
        oldValue: [
          {
            criteriaCode: 'abc',
            items: [{ value: '', active: false, description: 'description' }]
          }
        ] as any,
        newValue: [
          {
            criteriaCode: 'abc',
            items: [{ value: '', active: false, description: 'description' }]
          }
        ] as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result =
      contactAccountEntitlement.getContactAccountEntitlementTemplate(mockArgs);
    expect(result).toEqual([]);
  });

  it('getContactAccountEntitlementTemplate > items is null', () => {
    const mockArgs = {
      item: {
        oldValue: [
          {
            criteriaCode: 'abc',
            items: null
          }
        ] as any,
        newValue: [
          {
            criteriaCode: 'abc',
            items: null
          }
        ] as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result =
      contactAccountEntitlement.getContactAccountEntitlementTemplate(mockArgs);
    expect(result).toEqual([]);
  });
});
