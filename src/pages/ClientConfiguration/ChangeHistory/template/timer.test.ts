import * as timer from './timer';
import { formatTime } from 'app/helpers';
import { getTime } from 'app/test-utils/getTimeFormatMock';

describe('Test timer', () => {
  window.appConfig = {
    commonConfig: {
      operatorID: 'abc'
    }
  } as any;

  it('getTimerTemplate', () => {
    const mockArgs = {
      item: {
        oldValue: {
          green: {},
          red: {},
          amber: {},
          message: ''
        } as any,
        newValue: {
          green: {},
          red: {},
          amber: {},
          message: ''
        } as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result = timer.getTimerTemplate(mockArgs);
    expect(result).toEqual([]);
  });

  it('getTimerTemplate > case 2', () => {
    const mockArgs = {
      item: {
        oldValue: {
          green: {},
          red: {},
          amber: {},
          message: ''
        } as any,
        newValue: {
          green: {
            hour: '01',
            minute: '02',
            second: '03'
          },
          red: {
            hour: '01',
            minute: '02',
            second: '03'
          },
          amber: {
            hour: '01',
            minute: '02',
            second: '03'
          },
          message: 'aaa'
        } as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result = timer.getTimerTemplate(mockArgs);
    expect(result).toEqual([
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Green',
        changedSection: 'entitlement',
        cspaLevel: '',
        date: formatTime(new Date().toString()).dateWithHyphen(),
        newValue: 'Start Time: 01:02:03',
        oldValue: 'Start Time: undefined:undefined:undefined',
        operatorId: 'abc',
        time: getTime()
      },
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Amber',
        changedSection: 'entitlement',
        cspaLevel: '',
        date: formatTime(new Date().toString()).dateWithHyphen(),
        newValue: 'Start Time: 01:02:03',
        oldValue: 'Start Time: undefined:undefined:undefined',
        operatorId: 'abc',
        time: getTime()
      },
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Red',
        changedSection: 'entitlement',
        cspaLevel: '',
        date: formatTime(new Date().toString()).dateWithHyphen(),
        newValue: 'Start Time: 01:02:03',
        oldValue: 'Start Time: undefined:undefined:undefined',
        operatorId: 'abc',
        time: getTime()
      },
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Red',
        changedSection: 'entitlement',
        cspaLevel: '',
        date: formatTime(new Date().toString()).dateWithHyphen(),
        newValue: 'Message: aaa',
        oldValue: ' ',
        operatorId: 'abc',
        time: getTime()
      }
    ]);
  });
});
