import { capitalizeWord } from 'app/helpers';
import { ConfigChangeHistoryItem, GetFormattedHistoryData } from '../types';
import { getCommonTemplate } from './common';
import isEqual from 'lodash.isequal';
import { validateHistoryValue } from '../helpers';

const promiseToPayLabel: MagicKeyValue = {
  enable: 'Active',
  rawValue: 'Value'
};

export const getPromiseToPayTemplate = (
  args: GetFormattedHistoryData
): ConfigChangeHistoryItem[] => {
  const commonData = getCommonTemplate(args);
  const data: ConfigChangeHistoryItem[] = [];

  const oldValue = args.item.oldValue as MagicKeyValue;
  const newValue = args.item.newValue as MagicKeyValue;

  for (const key in { ...oldValue, ...newValue }) {
    const changedItem = `Name: ${capitalizeWord(
      key.replace(/([A-Z])/g, ' $1').trim()
    )}`;
    for (const keyItem in oldValue?.[key]) {
      const oldItems = oldValue?.[key];
      const newItems = newValue?.[key];
      if (!isEqual(oldItems?.[keyItem], newItems?.[keyItem])) {
        data.push({
          ...commonData,
          changedItem,
          oldValue: validateHistoryValue(
            oldItems?.[keyItem],
            `${promiseToPayLabel[keyItem]}: ${oldItems[keyItem]}`
          ),
          newValue: validateHistoryValue(
            newItems?.[keyItem],
            `${promiseToPayLabel[keyItem]}: ${newItems[keyItem]}`
          )
        });
      }
    }
  }

  return data;
};
