import { ConfigChangeHistoryItem, GetFormattedHistoryData } from '../types';
import { getCommonTemplate } from './common';
import { validateHistoryValue } from '../helpers';
import isUndefined from 'lodash.isundefined';
import isEqual from 'lodash.isequal';

export const getDefaultTemplate = (
  args: GetFormattedHistoryData
): ConfigChangeHistoryItem[] => {
  const EMPTY_VALUE = ' ';
  const { item, historyLabelItem } = args;
  const { action, oldValue = {}, newValue = {} } = item;
  const commonData = getCommonTemplate(args);

  const generateAction = () => {
    if (action === 'ADD' || action === 'UPDATE' || action === 'DELETE')
      return action;
    return 'UPDATE';
  };

  const generateOldValue = (values: string) => {
    if (generateAction() === 'ADD') return EMPTY_VALUE;
    return values;
  };

  const generateNewValue = (value: string) => {
    if (generateAction() === 'DELETE') return EMPTY_VALUE;
    return value;
  };

  const data: ConfigChangeHistoryItem[] = [];
  const labelList = historyLabelItem.value;
  const formatValue = Object.keys({ ...oldValue, ...newValue }).map(key => {
    const label = labelList?.[key] ? `${labelList[key]}: ` : '';
    if (isEqual(oldValue[key], newValue[key])) return;
    return {
      old: validateHistoryValue(oldValue[key], `${label}${oldValue[key]}`),
      new: validateHistoryValue(newValue[key], `${label}${newValue[key]}`)
    };
  });

  formatValue.forEach(valueItem => {
    if (isUndefined(valueItem)) return;
    data.push({
      ...commonData,
      oldValue: generateOldValue(valueItem.old),
      newValue: generateNewValue(valueItem.new)
    });
  });

  return data;
};
