import { GetFormattedHistoryData, ConfigChangeHistoryItem } from './../types';
import { getCommonTemplate } from './common';
import { AdjustmentConfigurationItem } from 'pages/ClientConfiguration/AdjustmentTransaction/types';
import isEqual from 'lodash.isequal';
import { validateHistoryValue } from '../helpers';

const saveChangeHistoryFields: MagicKeyValue = {
  originalTransactionCode: 'Original Trans Code',
  adjustmentPostingTranCode: 'Adjustment Posting Transaction Code',
  adjustmentDescription: 'Description',
  adjustmentPostingBatchCode: 'Adjustment Posting Batch Code'
};

export const getAdjustmentTransactionTemplate = (
  args: GetFormattedHistoryData
): ConfigChangeHistoryItem[] => {
  const commonData = getCommonTemplate(args);

  const oldTransactionCode =
    (args.item.oldValue as AdjustmentConfigurationItem[]) || [];

  const newTransactionCode =
    (args.item.newValue as AdjustmentConfigurationItem[]) || [];

  const data: ConfigChangeHistoryItem[] = [];

  const maxLength = Math.max(
    oldTransactionCode.length,
    newTransactionCode.length
  );

  for (let i = 0; i < maxLength; i++) {
    const oldItem = oldTransactionCode[i];
    const newItem = newTransactionCode[i];

    if (!isEqual(oldItem, newItem)) {
      const target = oldItem || newItem;

      for (const key in target) {
        if (!Object.prototype.hasOwnProperty.call(saveChangeHistoryFields, key))
          continue;
        const label = saveChangeHistoryFields[key];
        const oldValue =
          oldItem?.[key as keyof AdjustmentConfigurationItem]?.toString();
        const newValue =
          newItem?.[key as keyof AdjustmentConfigurationItem]?.toString();

        if (!isEqual(oldValue, newValue)) {
          data.push({
            ...commonData,
            changedItem: validateHistoryValue(
              target?.originalTransactionCode,
              `Original Trans Code: ${target?.originalTransactionCode}`
            ),
            oldValue: oldValue ? `${label}: ${oldValue}` : ' ',
            newValue: newValue ? `${label}: ${newValue}` : ' '
          });
        }
      }
    }
  }

  return data;
};
