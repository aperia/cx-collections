import isEqual from 'lodash.isequal';
import {
  QUESTION_CONFIG,
  SOURCE_CONFIG
} from 'pages/ClientConfiguration/Authentication/constants';
import { ConfigChangeHistoryItem, GetFormattedHistoryData } from '../types';
import { getCommonTemplate } from './common';

const LABEL_VALUES: Record<string, string> = {
  txt_search_result: 'Search Result',
  txt_push_queue: 'Push Queue',
  txt_dialer_queue: 'Dialer Queue',
  txt_maiden_name_of_mother: "Mother's Maiden Name",
  txt_date_of_birth: 'Date of Birth',
  txt_last_4_ssn: 'Last 4 SSN',
  txt_last_payment_amount: 'Last Payment Amount',
  txt_credit_limit: 'Credit Limit',
  txt_home_phone: 'Home Phone',
  txt_work_phone: 'Work Phone',
  txt_mobile_phone: 'Mobile Phone',
  txt_fax_billing_address: 'Fax Billing Address',
  txt_miscellaneous: 'Miscellaneous',
  txt_permanent_billing_address: 'Address',
  txt_permanent_mailing_address: 'Permanent Mailing Address'
};

export const getAuthenticationTemplate = (
  args: GetFormattedHistoryData
): ConfigChangeHistoryItem[] => {
  const commonData = getCommonTemplate(args);

  const oldSourceConfig = args.item.oldValue?.sourceConfig as string[];
  const oldQuestionConfig = args.item.oldValue?.questionConfig as string[];

  const newSourceConfig = args.item.newValue?.sourceConfig as string[];
  const newQuestionConfig = args.item.newValue?.questionConfig as string[];

  const sourceConfigData = SOURCE_CONFIG.reduce((arr: MagicKeyValue, item) => {
    arr[item.id] = {
      old: `${LABEL_VALUES[item.title]}: ${
        oldSourceConfig.indexOf(item.id) >= 0
      }`,
      new: `${LABEL_VALUES[item.title]}: ${
        newSourceConfig.indexOf(item.id) >= 0
      }`
    };
    return arr;
  }, []);

  const questionConfigData = QUESTION_CONFIG.reduce(
    (arr: MagicKeyValue, item) => {
      arr[item.id] = {
        old: `${LABEL_VALUES[item.title]}: ${
          oldQuestionConfig.indexOf(item.id) >= 0
        }`,
        new: `${LABEL_VALUES[item.title]}: ${
          newQuestionConfig.indexOf(item.id) >= 0
        }`
      };
      return arr;
    },
    []
  );

  const data: ConfigChangeHistoryItem[] = [];

  for (const key in sourceConfigData) {
    if (!isEqual(sourceConfigData[key].old, sourceConfigData[key].new)) {
      data.push({
        ...commonData,
        changedSection: 'Authentication Source Configuration',
        changedItem: 'Authentication Source Configuration',
        oldValue: sourceConfigData[key].old,
        newValue: sourceConfigData[key].new
      });
    }
  }
  for (const key in questionConfigData) {
    if (!isEqual(questionConfigData[key].old, questionConfigData[key].new)) {
      data.push({
        ...commonData,
        changedSection: 'Authentication Question Configuration',
        changedItem: 'Authentication Question Configuration',
        oldValue: questionConfigData[key].old,
        newValue: questionConfigData[key].new
      });
    }
  }

  return data;
};
