import { ConfigChangeHistoryItem, GetFormattedHistoryData } from '../types';
import { getCommonTemplate } from './common';
import { ClientInfoLetterTemplate } from 'pages/ClientConfiguration/types';
import isEqual from 'lodash.isequal';
import isUndefined from 'lodash.isundefined';

const mappingLetterLabel = (letter: ClientInfoLetterTemplate) => {
  if (isUndefined(letter)) return {};
  const flattenLetter: MagicKeyValue = {
    'Letter Number': letter.letterId,
    'Letter Type Code': letter.letterCode,
    Description: letter.letterDescription,
    'Letter Variables': letter.variableLetter?.toString()
  };
  if (!isUndefined(letter.captionMapping)) {
    Object.keys(letter.captionMapping).forEach((key, index) => {
      flattenLetter[`Letter Caption Fields ${index + 1}`] = key;
      flattenLetter[`Generate Letter Field Mapping ${index + 1}`] =
        letter.captionMapping?.[key] || '';
    });
  }
  return flattenLetter;
};

export const getLetterListTemplate = (
  args: GetFormattedHistoryData
): ConfigChangeHistoryItem[] => {
  const commonData = getCommonTemplate(args);

  const oldLetter = args.item.oldValue as ClientInfoLetterTemplate;
  const flattenOldData: MagicKeyValue = mappingLetterLabel(oldLetter);

  const newLetter = args.item.newValue as ClientInfoLetterTemplate;
  const flattenNewData: MagicKeyValue = mappingLetterLabel(newLetter);

  const data: ConfigChangeHistoryItem[] = [];

  for (const key in { ...flattenOldData, ...flattenNewData }) {
    if (!isEqual(flattenOldData[key], flattenNewData[key])) {
      data.push({
        ...commonData,
        oldValue: flattenOldData[key] ? `${key}: ${flattenOldData[key]}` : ' ',
        newValue: flattenNewData[key] ? `${key}: ${flattenNewData[key]}` : ' '
      });
    }
  }

  return data;
};
