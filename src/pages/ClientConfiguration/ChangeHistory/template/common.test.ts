import * as common from './common';
import { formatTime } from 'app/helpers';
import { getTime } from 'app/test-utils/getTimeFormatMock';

describe('Test common', () => {
  window.appConfig = {
    commonConfig: {
      operatorID: 'abc'
    }
  } as any;

  it('getCommonTemplate', () => {
    const mockArgs = {
      item: {
        oldValue: {
          sourceConfig: ['abc', 'xyz'],
          questionConfig: ['ques1, ques2']
        } as any,
        newValue: {
          sourceConfig: ['abc', 'xyz'],
          questionConfig: ['ques1, ques2']
        } as any,
        action: 'ADD'
      } as any,
      historyLabelItem: {} as any,
      cspa: ''
    };
    const result = common.getCommonTemplate(mockArgs);
    const today = new Date();
    expect(result).toEqual({
      action: 'ADD',
      changedCategory: undefined,
      changedItem: undefined,
      changedSection: undefined,
      cspaLevel: '',
      date: formatTime(today.toString()).dateWithHyphen(),
      operatorId: 'abc',
      time: getTime()
    });
  });
});
