import * as userRoleEntitlement from './userRoleEntitlement';
import { formatTime } from 'app/helpers';

describe('Test userRoleEntitlement', () => {
  window.appConfig = {
    commonConfig: {
      operatorID: 'abc'
    }
  } as any;

  it('getUserRoleEntitlementTemplate', () => {
    const mockArgs = {
      item: {
        oldValue: [{ items: [{ label: 'label', checked: true }] }] as any,
        newValue: [{ items: [{ label: 'label2', checked: false }] }] as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result = userRoleEntitlement.getUserRoleEntitlementTemplate(mockArgs);
    expect(result[0]).toEqual(
      expect.objectContaining({
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'undefined - label',
        changedSection: undefined,
        cspaLevel: '',
        date: formatTime(new Date().toString()).dateWithHyphen(),
        newValue: 'false',
        oldValue: 'true',
        operatorId: 'abc'
      })
    );
  });

  it('getUserRoleEntitlementTemplate > case 2', () => {
    const mockArgs = {
      item: {
        oldValue: [{ items: [{ label: 'label', checked: true }] }] as any,
        newValue: [{ items: [{ label: 'label', checked: true }] }] as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result = userRoleEntitlement.getUserRoleEntitlementTemplate(mockArgs);
    expect(result).toEqual([]);
  });

  it('getUserRoleEntitlementTemplate > case 3', () => {
    const mockArgs = {
      item: {
        oldValue: [{ items: undefined }] as any,
        newValue: [{ items: undefined }] as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result = userRoleEntitlement.getUserRoleEntitlementTemplate(mockArgs);
    expect(result).toEqual([]);
  });
});
