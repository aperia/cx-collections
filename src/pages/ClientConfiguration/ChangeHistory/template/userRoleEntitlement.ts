import { ConfigChangeHistoryItem, GetFormattedHistoryData } from '../types';
import { TreeViewValue } from 'app/_libraries/_dls/components';
import isEqual from 'lodash.isequal';
import { getCommonTemplate } from './common';

export const getUserRoleEntitlementTemplate = (
  args: GetFormattedHistoryData
): ConfigChangeHistoryItem[] => {
  const oldValue = args.item.oldValue as TreeViewValue[];
  const newValue = args.item.newValue as TreeViewValue[];
  const userRole = args.item.changedSection?.userRole as string;
  const commonData = getCommonTemplate(args);

  const data: ConfigChangeHistoryItem[] = [];

  oldValue.forEach((oldEntitlement, index) => {
    const oldItems = oldEntitlement.items || [];
    const newItems = newValue[index].items || [];
    oldItems.forEach((oldItem, itemIndex) => {
      if (!isEqual(oldItem, newItems[itemIndex])) {
        data.push({
          ...commonData,
          changedSection: oldEntitlement.label,
          changedItem: `${userRole} - ${oldItem.label}`,
          oldValue: `${oldItem.checked}`,
          newValue: `${newItems[itemIndex].checked}`
        });
      }
    });
  });

  return data;
};
