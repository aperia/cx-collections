import { CriteriaConfig } from 'pages/ConfigurationEntitlement/ContactAndAccountEntitlement/types';
import { ConfigChangeHistoryItem, GetFormattedHistoryData } from '../types';
import { getCommonTemplate } from './common';
import isEqual from 'lodash.isequal';
import { validateHistoryValue } from '../helpers';

export const getContactAccountEntitlementTemplate = (
  args: GetFormattedHistoryData
): ConfigChangeHistoryItem[] => {
  const commonData = getCommonTemplate(args);
  const data: ConfigChangeHistoryItem[] = [];
  const oldEntitlements = args.item.oldValue as CriteriaConfig[];
  const newEntitlements = args.item.newValue as CriteriaConfig[];
  const entitlementName = args.item.changedItem?.entitlement as string;

  oldEntitlements.forEach((entitlement, index) => {
    const oldEntitlementData = entitlement.items || [];
    const newEntitlementData = newEntitlements[index].items || [];
    const criteriaCode = entitlement.criteriaCode;
    oldEntitlementData.forEach((item, itemIndex) => {
      const newItem = newEntitlementData[itemIndex];
      if (!isEqual(item, newItem)) {
        const itemCode = validateHistoryValue(item.value, `${item.value} - `);
        const newItemCode = validateHistoryValue(
          newItem.value,
          `${newItem.value} - `
        );
        data.push({
          ...commonData,
          changedItem: `${criteriaCode} - ${entitlementName}`,
          oldValue: `${itemCode}${item.description}: ${item.active}`.trim(),
          newValue:
            `${newItemCode}${newItem.description}: ${newItem.active}`.trim()
        });
      }
    });
  });

  return data;
};
