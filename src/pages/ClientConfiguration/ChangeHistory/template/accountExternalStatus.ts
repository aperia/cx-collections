import isEqual from 'lodash.isequal';
import isEmpty from 'lodash.isempty';
import {
  AccountExternalStatus,
  AccountExternalStatusDataType,
  ReasonCodesDataType
} from 'pages/ClientConfiguration/AccountExternalStatus/types';
import { ConfigChangeHistoryItem, GetFormattedHistoryData } from '../types';
import { getCommonTemplate } from './common';
import { validateHistoryValue } from '../helpers';

const CURRENT_STATUS: MagicKeyValue = {};

export const getCurrentStatusTemplate = (
  oldExternalStatus: AccountExternalStatusDataType[],
  newExternalStatus: AccountExternalStatusDataType[],
  commonData: ConfigChangeHistoryItem
): ConfigChangeHistoryItem[] => {
  if (isEmpty(oldExternalStatus) && isEmpty(newExternalStatus)) return [];

  const data: ConfigChangeHistoryItem[] = [];

  oldExternalStatus.forEach(item => {
    CURRENT_STATUS[item.value] = item.description;
  });

  oldExternalStatus.forEach((item, index) => {
    const itemValue = validateHistoryValue(item.value, `${item.value} - `);
    const changedItem =
      `Current Status: ${itemValue}${item.description}`.trim();
    const oldCheckList = item.checkList;
    const newCheckList = newExternalStatus[index].checkList;

    for (const key in { ...oldCheckList, ...newCheckList }) {
      const checkBoxKey = validateHistoryValue(key, `${key} - `);
      const label = `${checkBoxKey}${CURRENT_STATUS[key]}`.trim();

      if (!isEqual(oldCheckList?.[key], newCheckList?.[key])) {
        data.push({
          ...commonData,
          changedSection: 'Current Status Configuration',
          changedItem,
          oldValue: `${label}: ${oldCheckList?.[key]?.checked || false}`,
          newValue: `${label}: ${newCheckList?.[key]?.checked}`
        });
      }
    }
  });

  return data;
};

export const getReasonCodeTemplate = (
  oldReasonCode: ReasonCodesDataType[],
  newReasonCode: ReasonCodesDataType[],
  commonData: ConfigChangeHistoryItem
): ConfigChangeHistoryItem[] => {
  if (isEmpty(oldReasonCode) && isEmpty(newReasonCode)) return [];

  const data: ConfigChangeHistoryItem[] = [];

  oldReasonCode.forEach(item => {
    CURRENT_STATUS[item.value] = item.description;
  });

  oldReasonCode.forEach((item, index) => {
    const itemValue = validateHistoryValue(item.value, `${item.value} - `);
    const changedItem = `Reason Code: ${itemValue}${item.description}`.trim();
    const oldCheckList = item.checkList;
    const newCheckList = newReasonCode[index].checkList;
    for (const key in { ...oldCheckList, ...newCheckList }) {
      if (!isEqual(oldCheckList?.[key], newCheckList?.[key])) {
        if (key === 'display') {
          data.push({
            ...commonData,
            changedSection: 'Reason Code Configuration',
            changedItem,
            oldValue: `Display: ${oldCheckList?.[key]?.checked || false}`,
            newValue: `Display: ${newCheckList?.[key]?.checked}`
          });
        } else {
          const checkBoxKey = validateHistoryValue(key, `${key} - `);
          const label = `${checkBoxKey}${CURRENT_STATUS[key]}`.trim();
          data.push({
            ...commonData,
            changedSection: 'Reason Code Configuration',
            changedItem,
            oldValue: `${label}: ${oldCheckList?.[key]?.checked || false}`,
            newValue: `${label}: ${newCheckList?.[key]?.checked}`
          });
        }
      }
    }
  });

  return data;
};

export const getAccExternalStatusTemplate = (
  args: GetFormattedHistoryData
): ConfigChangeHistoryItem[] => {
  const commonData = getCommonTemplate(args);
  const oldValue = args.item.oldValue as AccountExternalStatus;
  const newValue = args.item.newValue as AccountExternalStatus;

  return [
    ...getCurrentStatusTemplate(
      oldValue.externalStatusCode || [],
      newValue.externalStatusCode || [],
      commonData
    ),
    ...getReasonCodeTemplate(
      oldValue.externalStatusReasonCode || [],
      newValue.externalStatusReasonCode || [],
      commonData
    )
  ];
};
