import { capitalizeWord } from 'app/helpers';
import { FormValues } from 'pages/ClientConfiguration/Timer/types';
import { ConfigChangeHistoryItem, GetFormattedHistoryData } from '../types';
import { getCommonTemplate } from './common';
import isEqual from 'lodash.isequal';
import isUndefined from 'lodash.isundefined';
import { validateHistoryValue } from '../helpers';

const formatTimerTime = (value?: any) => {
  if (isUndefined(value)) return '';
  if (typeof value === 'object')
    return `${value.hour}:${value.minute}:${value.second}`;
  return value.toString();
};

const saveChangedItem: Record<string, MagicKeyValue> = {
  green: {
    green: 'Start Time'
  },
  amber: {
    amber: 'Start Time'
  },
  red: {
    red: 'Start Time',
    showMessage: 'Show Message',
    message: 'Message'
  }
};

export const getTimerTemplate = (
  args: GetFormattedHistoryData
): ConfigChangeHistoryItem[] => {
  const commonData = getCommonTemplate(args);
  const data: ConfigChangeHistoryItem[] = [];

  const oldTimer = args.item.oldValue as FormValues;
  const newTimer = args.item.newValue as FormValues;

  for (const changedItem in saveChangedItem) {
    for (const key in saveChangedItem[changedItem]) {
      const label = saveChangedItem[changedItem][key];
      const oldValue = formatTimerTime(oldTimer[key as keyof FormValues]);
      const newValue = formatTimerTime(newTimer[key as keyof FormValues]);
      if (!isEqual(oldValue, newValue)) {
        data.push({
          ...commonData,
          changedItem: capitalizeWord(changedItem),
          oldValue: validateHistoryValue(oldValue, `${label}: ${oldValue}`),
          newValue: validateHistoryValue(newValue, `${label}: ${newValue}`)
        });
      }
    }
  }

  return data;
};
