import * as memos from './memos';
import { formatTime } from 'app/helpers';
import { getTime } from 'app/test-utils/getTimeFormatMock';

describe('Test memos', () => {
  window.appConfig = {
    commonConfig: {
      operatorID: 'abc'
    }
  } as any;

  it('getMemosTemplate', () => {
    const mockArgs = {
      item: {
        oldValue: {} as any,
        newValue: {} as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result = memos.getMemosTemplate(mockArgs);
    expect(result).toEqual([]);
  });

  it('getMemosTemplate > case 2', () => {
    const mockArgs = {
      item: {
        oldValue: {
          key: 'demo1'
        } as any,
        newValue: {
          key: 'demo2'
        } as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result = memos.getMemosTemplate(mockArgs);
    expect(result).toEqual([
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Key',
        changedSection: 'entitlement',
        cspaLevel: '',
        date: formatTime(new Date().toString()).dateWithHyphen(),
        newValue: 'demo2',
        oldValue: 'demo1',
        operatorId: 'abc',
        time: getTime()
      }
    ]);
  });

  it('getMemosTemplate > case 3', () => {
    const mockArgs = {
      item: {
        oldValue: {
          key: 'demo1'
        } as any,
        newValue: {
          key: 'demo1'
        } as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result = memos.getMemosTemplate(mockArgs);
    expect(result).toEqual([]);
  });
});
