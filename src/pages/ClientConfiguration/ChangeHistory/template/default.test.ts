import * as defaultObject from './default';
import { formatTime } from 'app/helpers';
import { getTime } from 'app/test-utils/getTimeFormatMock';

describe('Test contactAccountEntitlement', () => {
  window.appConfig = {
    commonConfig: {
      operatorID: 'abc'
    }
  } as any;

  it('getContactAccountEntitlementTemplate > oldValue, newValue is undefined', () => {
    const mockArgs = {
      item: {
        oldValue: undefined as any,
        newValue: undefined as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result = defaultObject.getDefaultTemplate(mockArgs);
    expect(result).toEqual([]);
  });

  it('getContactAccountEntitlementTemplate', () => {
    const mockArgs = {
      item: {
        oldValue: {
          criteriaCode: 'abc',
          items: [{ value: '', active: false, description: 'description' }]
        } as any,

        newValue: {
          criteriaCode: 'abc',
          items: [{ value: '123', active: true, description: 'description' }]
        } as any,

        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result = defaultObject.getDefaultTemplate(mockArgs);
    const today = new Date();
    expect(result).toEqual([
      expect.objectContaining({
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'entitlement',
        changedSection: 'entitlement',
        cspaLevel: '',
        date: formatTime(today.toString()).dateWithHyphen(),
        newValue: '[object Object]',
        oldValue: '[object Object]',
        operatorId: 'abc'
      })
    ]);
  });

  it('getContactAccountEntitlementTemplate > case 2', () => {
    const mockArgs = {
      item: {
        oldValue: [
          {
            criteriaCode: 'abc',
            items: [{ value: '', active: false, description: 'description' }]
          }
        ] as any,
        newValue: [
          {
            criteriaCode: 'abc',
            items: [{ value: '123', active: true, description: 'description' }]
          }
        ] as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        },
        action: 'ADD'
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result = defaultObject.getDefaultTemplate(mockArgs);
    const today = new Date();
    expect(result).toEqual([
      {
        action: 'ADD',
        changedCategory: undefined,
        changedItem: 'entitlement',
        changedSection: 'entitlement',
        cspaLevel: '',
        date: formatTime(today.toString()).dateWithHyphen(),
        newValue: '[object Object]',
        oldValue: ' ',
        operatorId: 'abc',
        time: getTime()
      }
    ]);
  });

  it('getContactAccountEntitlementTemplate > case 3', () => {
    const mockArgs = {
      item: {
        oldValue: {
          criteriaCode: '',
          items: [{ value: '', active: false, description: 'description' }]
        } as any,

        newValue: {
          criteriaCode: '',
          items: [{ value: '123', active: true, description: 'description' }]
        } as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        },
        action: 'DELETE'
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement',
        value: {
          criteriaCode: 'label1',
          items: 'label2',
          key: '123'
        }
      } as any,
      cspa: ''
    };
    const result = defaultObject.getDefaultTemplate(mockArgs);
    const today = new Date();
    expect(result).toEqual([
      {
        action: 'DELETE',
        changedCategory: undefined,
        changedItem: 'entitlement',
        changedSection: 'entitlement',
        cspaLevel: '',
        date: formatTime(today.toString()).dateWithHyphen(),
        newValue: ' ',
        oldValue: 'label2: [object Object]',
        operatorId: 'abc',
        time: getTime()
      }
    ]);
  });
});
