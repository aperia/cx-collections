import * as promiseToPay from './promiseToPay';
import { formatTime } from 'app/helpers';
import { getTime } from 'app/test-utils/getTimeFormatMock';

describe('Test promiseToPay', () => {
  window.appConfig = {
    commonConfig: {
      operatorID: 'abc'
    }
  } as any;

  it('getPromiseToPayTemplate', () => {
    const mockArgs = {
      item: {
        oldValue: {
          key: '123'
        } as any,
        newValue: {
          key: '456'
        } as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result = promiseToPay.getPromiseToPayTemplate(mockArgs);
    expect(result).toEqual([
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Name: Key',
        changedSection: 'entitlement',
        cspaLevel: '',
        date: formatTime(new Date().toString()).dateWithHyphen(),
        newValue: 'undefined: 4',
        oldValue: 'undefined: 1',
        operatorId: 'abc',
        time: getTime()
      },
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Name: Key',
        changedSection: 'entitlement',
        cspaLevel: '',
        date: formatTime(new Date().toString()).dateWithHyphen(),
        newValue: 'undefined: 5',
        oldValue: 'undefined: 2',
        operatorId: 'abc',
        time: getTime()
      },
      {
        action: 'UPDATE',
        changedCategory: undefined,
        changedItem: 'Name: Key',
        changedSection: 'entitlement',
        cspaLevel: '',
        date: formatTime(new Date().toString()).dateWithHyphen(),
        newValue: 'undefined: 6',
        oldValue: 'undefined: 3',
        operatorId: 'abc',
        time: getTime()
      }
    ]);
  });

  it('getPromiseToPayTemplate > case 2', () => {
    const mockArgs = {
      item: {
        oldValue: {
          key: '123'
        } as any,
        newValue: {
          key: '123'
        } as any,
        changedItem: {
          entitlement: 'entitlement'
        },
        changedSection: {
          entitlement: 'entitlement'
        }
      } as any,
      historyLabelItem: {
        section: 'entitlement',
        item: 'entitlement'
      } as any,
      cspa: ''
    };
    const result = promiseToPay.getPromiseToPayTemplate(mockArgs);
    expect(result).toEqual([]);
  });
});
