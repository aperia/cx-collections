import { capitalizeWord } from 'app/helpers';
import isEqual from 'lodash.isequal';
import { validateHistoryValue } from '../helpers';
import { ConfigChangeHistoryItem, GetFormattedHistoryData } from '../types';
import { getCommonTemplate } from './common';

export const getMemosTemplate = (
  args: GetFormattedHistoryData
): ConfigChangeHistoryItem[] => {
  const commonData = getCommonTemplate(args);
  const data: ConfigChangeHistoryItem[] = [];
  const oldValue = args.item.oldValue;
  const newValue = args.item.newValue;

  for (const key in { ...oldValue, ...newValue }) {
    if (!isEqual(oldValue?.[key], newValue?.[key])) {
      data.push({
        ...commonData,
        changedItem: capitalizeWord(key.replace(/([A-Z])/g, ' $1').trim()),
        oldValue: validateHistoryValue(oldValue?.[key]),
        newValue: validateHistoryValue(newValue?.[key])
      });
    }
  }

  return data;
};
