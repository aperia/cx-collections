import { formatTime } from 'app/helpers';
import { format2DigitNumber } from 'app/_libraries/_dls/components/TimePicker/helpers';
import { mappingLabelValueFromString } from '../helpers';
import { GetFormattedHistoryData } from '../types';

export const getCommonTemplate = (args: GetFormattedHistoryData) => {
  const today = new Date();
  const { item, cspa, historyLabelItem } = args;

  const { action, changedSection, changedItem } = item;
  const generateAction = () => {
    if (action === 'ADD' || action === 'UPDATE' || action === 'DELETE')
      return action;
    return 'UPDATE';
  };
  const getTime = () => {
    return `${format2DigitNumber(today.getHours())}:${format2DigitNumber(
      today.getMinutes()
    )}:${format2DigitNumber(today.getSeconds())}`;
  };

  return {
    date: formatTime(today.toString()).dateWithHyphen(),
    time: getTime(),
    action: generateAction(),
    operatorId: window.appConfig.commonConfig.operatorID,
    cspaLevel: cspa,
    changedCategory: historyLabelItem.category,
    changedSection: mappingLabelValueFromString(
      historyLabelItem.section,
      changedSection
    ),
    changedItem: mappingLabelValueFromString(historyLabelItem.item, changedItem)
  };
};
