import React, { useMemo } from 'react';

// components
import MultiSelect from 'app/_libraries/_dls/components/MultiSelect';
import { FormikProps } from 'formik';

// constants
import { HistoryFilter, ChangedCategory } from './types';
import { FILTER_FORM_NAME } from './constants';
import { I18N_CHANGE_HISTORY } from 'app/constants/i18n';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useSelector } from 'react-redux';
import { selectedChangedCategories } from './_redux/selectors';
import { getGroupFormatInputText } from 'app/helpers/formatMultiSelectText';

const ChangedCategoryMultiSelect: React.FC<FormikProps<HistoryFilter>> = ({
  values,
  handleChange,
  handleBlur
}) => {
  const { t } = useTranslation();
  const data = useSelector(selectedChangedCategories);
  const testId = 'clientConfig_changeHistory_filterChangedCategoryMultiSelect';

  const dropdownListItem = useMemo(() => {
    return data?.map((item: ChangedCategory) => (
      <MultiSelect.Item
        key={item.id}
        label={t(item.description)}
        value={item}
        variant="checkbox"
      />
    ));
  }, [data, t]);

  return (
    <div className="col-12 mt-16">
      <MultiSelect
        name={FILTER_FORM_NAME.CHANGED_CATEGORY_LIST}
        label={t(I18N_CHANGE_HISTORY.CHANGED_CATEGORY)}
        value={values?.changedCategoryList}
        onChange={handleChange}
        onBlur={handleBlur}
        textField="description"
        variant="group"
        checkAll
        checkAllLabel={t('txt_all')}
        searchBar
        dataTestId={testId}
        noResult={t('txt_no_results_found')}
        searchBarPlaceholder={t('txt_type_to_search')}
        groupFormatInputText={(...args) =>
          t(getGroupFormatInputText(...args), {
            selected: args[0],
            items: args[1]
          })
        }
      >
        {dropdownListItem}
      </MultiSelect>
    </div>
  );
};

export default ChangedCategoryMultiSelect;
