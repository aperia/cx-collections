import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import isEmpty from 'lodash.isempty';
import isEqual from 'lodash.isequal';
import classNames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { genAmtId } from 'app/_libraries/_dls/utils';

// components
import {
  ColumnType,
  Grid,
  Icon,
  Pagination,
  SortType,
  TruncateText
} from 'app/_libraries/_dls/components';
import ClearAndResetView from './ClearAndResetView';
import { markdownToHTML } from 'app/_libraries/_dls/components/TextEditor';

// constants
import { I18N_CHANGE_HISTORY, I18N_COMMON_TEXT } from 'app/constants/i18n';
import {
  ClientConfigurationHistory,
  HistoryFilter,
  TypeDataChangeHistory
} from './types';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { usePagination } from 'app/hooks';

// helpers
import { capitalizeWord, formatTime } from 'app/helpers';
import { decodeHtmlEntities } from '../ClientContactInformation/helpers';
import { checkTypeData } from './helpers';

// redux
import {
  selectedDefaultFilter,
  selectedFilter,
  selectedHistories,
  selectedIsFilterReset,
  selectedLoading
} from './_redux/selectors';
import { changeHistoryActions } from './_redux/reducers';

const HistoryGridView: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const testId = 'clientConfig_changeHistory_grid';

  const histories: ClientConfigurationHistory[] =
    useSelector(selectedHistories);
  const filter: HistoryFilter = useSelector(selectedFilter);
  const isLoading = useSelector(selectedLoading);
  const isFilterReset = useSelector(selectedIsFilterReset);
  const DEFAULT_FILTER = useSelector(selectedDefaultFilter);
  const divRef = useRef<HTMLDivElement | null>(null);
  const [sortBy, setSortBy] = useState<SortType[]>([]);

  useEffect(() => {
    if (!isFilterReset && !isEmpty(filter)) {
      dispatch(
        changeHistoryActions.getChangedHistories({
          ...filter
        })
      );
    }
  }, [dispatch, filter, isFilterReset]);

  const formatHtmlValue = (baseValue: string) => {
    try {
      if (
        baseValue === 'true' ||
        baseValue === 'false' ||
        !isNaN(Number(baseValue))
      )
        return baseValue;
      return decodeHtmlEntities(baseValue);
    } catch (error) {
      return baseValue;
    }
  };

  const HISTORY_GRID_COLUMNS: ColumnType<ClientConfigurationHistory>[] =
    useMemo(
      () => [
        {
          id: 'dateTime',
          Header: t(I18N_CHANGE_HISTORY.DATE_AND_TIME),
          isSort: true,
          width: 128,
          accessor: values => {
            return (
              <>
                <div>{formatTime(`${values.date} ${values.time}`).date}</div>
                <div className="color-grey">
                  {formatTime(`${values.date} ${values.time}`).fullTimeMeridiem}
                </div>
              </>
            );
          }
        },
        {
          id: 'changedBy',
          Header: t(I18N_CHANGE_HISTORY.CHANGED_BY),
          isSort: true,
          width: 120,
          accessor: 'changedBy'
        },
        {
          id: 'cspa',
          Header: t(I18N_CHANGE_HISTORY.CSPA),
          isSort: true,
          width: 216,
          accessor: 'cspa'
        },
        {
          id: 'action',
          Header: t(I18N_CHANGE_HISTORY.ACTION),
          isSort: true,
          width: 96,
          accessor: values => {
            if (values.action === 'UPDATE') return 'Edit';
            return capitalizeWord(values.action!);
          }
        },
        {
          id: 'changedCategory',
          Header: t(I18N_CHANGE_HISTORY.CHANGED_CATEGORY),
          isSort: true,
          width: 200,
          accessor: 'changedCategory'
        },
        {
          id: 'changedSection',
          Header: t(I18N_CHANGE_HISTORY.CHANGED_SECTION),
          isSort: true,
          width: 200,
          accessor: 'changedSection'
        },
        {
          id: 'changedItem',
          Header: t(I18N_CHANGE_HISTORY.CHANGED_ITEM),
          isSort: true,
          width: 240,
          accessor: values => (
            <TruncateText
              lines={2}
              title={values?.changedItem}
              resizable
              ellipsisMoreText={t(I18N_COMMON_TEXT.MORE)}
              ellipsisLessText={t(I18N_COMMON_TEXT.LESS)}
              className="text-break"
            >
              {values?.changedItem}
            </TruncateText>
          )
        },
        {
          id: 'before',
          Header: t(I18N_CHANGE_HISTORY.BEFORE),
          isSort: true,
          width: 240,
          accessor: values => {
            const formated = formatHtmlValue(values?.before || '');
            const typeData = checkTypeData(formated || '');

            return typeData === TypeDataChangeHistory[2] ? (
              <TruncateText
                lines={2}
                title={values?.before}
                resizable
                ellipsisMoreText={t(I18N_COMMON_TEXT.MORE)}
                ellipsisLessText={t(I18N_COMMON_TEXT.LESS)}
                className="text-break"
              >
                {formated}
              </TruncateText>
            ) : (
              <div
                id="before__text"
                className={classNames(
                  'form-group-static__text white-space-pre-line'
                )}
                dangerouslySetInnerHTML={{
                  __html:
                    typeData === TypeDataChangeHistory[0]
                      ? markdownToHTML(formated).html
                      : formated
                          .split('{')
                          .join('<span class="dls-editor-mention-decorator">')
                          .split('}')
                          .join('</span>')
                          ?.replace(/\\/g, '')
                }}
              ></div>
            );
          }
        },
        {
          id: 'after',
          Header: t(I18N_CHANGE_HISTORY.AFTER),
          isSort: true,
          width: 240,
          accessor: values => {
            const formated = formatHtmlValue(values?.after || '');
            const typeData = checkTypeData(formated || '');

            return typeData === TypeDataChangeHistory[2] ? (
              <TruncateText
                lines={2}
                title={values?.after}
                resizable
                ellipsisMoreText={t(I18N_COMMON_TEXT.MORE)}
                ellipsisLessText={t(I18N_COMMON_TEXT.LESS)}
                className="text-break"
              >
                {formated}
              </TruncateText>
            ) : (
              <div
                id="after__text"
                className={classNames(
                  'form-group-static__text white-space-pre-line'
                )}
                dangerouslySetInnerHTML={{
                  __html:
                    typeData === TypeDataChangeHistory[0]
                      ? markdownToHTML(formated).html
                      : formated
                          .split('{')
                          .join('<span class="dls-editor-mention-decorator">')
                          .split('}')
                          .join('</span>')
                          ?.replace(/\\/g, '')
                }}
              ></div>
            );
          }
        }
      ],
      [t]
    );

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange,
    setCurrentPage
  } = usePagination(histories);

  useLayoutEffect(() => {
    divRef.current &&
      (divRef.current.style.height = isLoading
        ? 'calc(100vh - 216px)'
        : 'auto');
  }, [isLoading]);

  useEffect(() => {
    setCurrentPage(1);
  }, [setCurrentPage, histories]);

  const handleSortChange = useCallback(
    (sortType: SortType) => {
      setSortBy([sortType]);
      dispatch(changeHistoryActions.sortBy(sortType));
    },
    [dispatch]
  );

  const GridView = useMemo(() => {
    if (isLoading) {
      return null;
    }
    if (isEmpty(gridData)) {
      return (
        <div
          className="my-80 text-center"
          data-testid={genAmtId(testId, 'noResult', '')}
        >
          <Icon name="file" className="fs-80 color-light-l12" />
          <p className="color-grey mt-20 mb-24">
            {isEqual(filter, DEFAULT_FILTER) && !isFilterReset
              ? t(I18N_COMMON_TEXT.NO_DATA)
              : t(I18N_CHANGE_HISTORY.NO_RESULTS_FOUND_SELECT_ANOTHER_FILTER)}
          </p>
          {isEqual(filter, DEFAULT_FILTER) && !isFilterReset ? null : (
            <ClearAndResetView />
          )}
        </div>
      );
    }
    const showPagination = total > pageSize[0];
    return (
      <>
        <Grid
          className="mt-24"
          columns={HISTORY_GRID_COLUMNS}
          data={gridData}
          onSortChange={handleSortChange}
          sortBy={sortBy}
          dataTestId={`${testId}_grid`}
        />
        {showPagination && (
          <div className="pt-16">
            <Pagination
              totalItem={total}
              pageSize={pageSize}
              pageNumber={currentPage}
              pageSizeValue={currentPageSize}
              compact
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
              dataTestId={`${testId}_pagination`}
            />
          </div>
        )}
      </>
    );
  }, [
    DEFAULT_FILTER,
    HISTORY_GRID_COLUMNS,
    currentPage,
    currentPageSize,
    filter,
    gridData,
    handleSortChange,
    isFilterReset,
    isLoading,
    onPageChange,
    onPageSizeChange,
    pageSize,
    sortBy,
    t,
    total
  ]);

  return (
    <div
      ref={divRef}
      className={classNames({
        loading: isLoading
      })}
    >
      {GridView}
    </div>
  );
};

export default HistoryGridView;
