import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import ChangeHistory from '.';
import { changeHistoryActions } from './_redux/reducers';
import { screen } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import * as FilterForm from './FilterForm';

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockChangeHistoryActions = mockActionCreator(changeHistoryActions);

describe('should test change history', () => {
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<ChangeHistory />, { initialState });
  };

  const defaultFilter = {
    rangeDate: {
      start: new Date(),
      end: new Date()
    },
    changedCategoryList: [],
    cspaList: []
  };

  const changeHistoryState: Partial<RootState> = {
    changeHistory: {
      histories: [
        {
          date: '1',
          time: '1',
          action: 'ADD',
          before: '<div></div>',
          after: '<div>1</div>'
        },
        {
          date: '2',
          time: '2',
          action: 'ADD',
          before: 'true',
          after: 'false'
        }
      ],
      sortType: {
        id: 'dateTime'
      },
      filter: defaultFilter,
      isFiltered: true,
      loading: false
    }
  };

  const changeHistoryState2: Partial<RootState> = {
    changeHistory: {
      histories: [
        {
          date: '1',
          time: '1',
          action: 'UPDATE'
        },
        {
          date: '2',
          time: '2',
          action: 'EDIT'
        }
      ],
      sortType: {
        id: 'dateTime'
      },
      filter: defaultFilter,
      isFiltered: true,
      loading: false
    }
  };

  const changeHistoryState3: Partial<RootState> = {
    changeHistory: {
      histories: [
        {
          date: '1',
          time: '1',
          action: 'ADD',
          before: 'test1 {Account Number}',
          after: 'test2 {Account Number}'
        },
        {
          date: '1',
          time: '1',
          action: 'ADD',
          before: '## ABC ** abc ***',
          after: '## XYZ'
        }
      ],
      sortType: {
        id: 'dateTime'
      },
      filter: defaultFilter,
      isFiltered: true,
      loading: false
    }
  };

  const changeHistoryState4: Partial<RootState> = {
    changeHistory: {
      histories: [
        {
          date: '1',
          time: '1',
          action: 'ADD',
          before: 'test1 ### XXX {Account Number}',
          after: 'test2 ### YYY {Account Number}'
        }
      ],
      sortType: {
        id: 'dateTime'
      },
      filter: defaultFilter,
      isFiltered: true,
      loading: false
    }
  };

  it('should render ui', () => {
    renderWrapper(changeHistoryState);

    const filterButton = screen.getByText('txt_filter');

    userEvent.click(filterButton);

    expect(screen.getByText('txt_apply')).toBeInTheDocument();
    expect(screen.getByText('txt_reset_to_default')).toBeInTheDocument();
  });

  it('should render ui > case 2', () => {
    renderWrapper(changeHistoryState3);

    const filterButton = screen.getByText('txt_filter');

    userEvent.click(filterButton);

    expect(screen.getByText('txt_apply')).toBeInTheDocument();
    expect(screen.getByText('txt_reset_to_default')).toBeInTheDocument();
  });

  it('should render ui > case 3', () => {
    renderWrapper(changeHistoryState4);

    const filterButton = screen.getByText('txt_filter');

    userEvent.click(filterButton);

    expect(screen.getByText('txt_apply')).toBeInTheDocument();
    expect(screen.getByText('txt_reset_to_default')).toBeInTheDocument();
  });

  it('should render ui with update action', () => {
    renderWrapper(changeHistoryState2);

    const filterButton = screen.getByText('txt_filter');

    userEvent.click(filterButton);

    expect(screen.getByText('txt_apply')).toBeInTheDocument();
    expect(screen.getByText('txt_reset_to_default')).toBeInTheDocument();
  });

  describe('should test with mock formik', () => {
    let spy: jest.SpyInstance;

    beforeEach(() => {
      spy = jest
        .spyOn(FilterForm, 'FilterForm')
        .mockImplementation(({ onSubmit }) => (
          <div>
            <button
              data-testid="applyButton"
              onClick={() => {
                onSubmit({});
              }}
            >
              apply
            </button>
          </div>
        ));
    });

    afterEach(() => {
      spy?.mockReset();
      spy?.mockRestore();
    });

    it('should handle submit button event', () => {
      const mockUpdateFilter = mockChangeHistoryActions('updateFilter');

      renderWrapper({
        changeHistory: {
          ...changeHistoryState.changeHistory,
          isFiltered: true,
          sortType: {
            id: 'dateTime'
          }
        }
      });

      const filterButton = screen.getByText('txt_filter');

      userEvent.click(filterButton);

      const submitButton = screen.getByTestId('applyButton');

      userEvent.click(submitButton);

      expect(mockUpdateFilter).toBeCalled();
    });

    it('should show clear and reset button', () => {
      renderWrapper({
        changeHistory: {
          ...changeHistoryState.changeHistory,
          isFiltered: false,
          isFilterReset: true,
          sortType: {
            id: 'dateTime'
          }
        }
      });

      expect(screen.getByText('txt_clear_and_reset')).toBeInTheDocument();
    });

    it('should hide clear and reset button', () => {
      renderWrapper({
        changeHistory: {
          isFilterReset: false,
          filter: {},
          defaultFilter: {},
          sortType: {
            id: 'dateTime'
          }
        }
      });

      expect(screen.queryByText('txt_clear_and_reset')).not.toBeInTheDocument();
    });
  });

  it('It should render ClearAndResetView ', () => {
    mockChangeHistoryActions('getFilterData');
    const initialState: Partial<RootState> = {
      changeHistory: {
        histories: [
          {
            date: '1',
            time: '1',
            action: 'ADD'
          }
        ],
        sortType: {
          id: 'dateTime'
        },
        isFiltered: false,
        isFilterReset: true,
        loading: false
      }
    };
    jest.useFakeTimers();
    renderWrapper(initialState);
    jest.runAllTimers();
    expect(screen.getByText('txt_clear_and_reset')).toBeInTheDocument();
  });
});
