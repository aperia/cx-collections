import React from 'react';
import { renderMockStore } from 'app/test-utils';
import CSPAMultiSelect from './CSPAMultiSelect';
import { FormikProps } from 'formik';
import { HistoryFilter } from './types';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import { act, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

HTMLCanvasElement.prototype.getContext = jest.fn();

const props = {} as FormikProps<HistoryFilter>;

describe('should test cspa multi select', () => {
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<CSPAMultiSelect {...props} />, { initialState });
  };

  it('should render UI', () => {
    jest.useFakeTimers();

    const { baseElement } = renderWrapper({
      changeHistory: {
        filterData: {
          cspaList: [
            {
              id: '1',
              value: '1234-2345-3456-4567',
              description: '1234-2345-3456-4567',
              clientId: '1234',
              systemId: '2345',
              principleId: '3456',
              agentId: '4567',
            }
          ]
        }
      }
    });

    const queryTextFieldContainer = queryByClass(
      baseElement as HTMLElement,
      'text-field-container'
    );

    const queryInputCombobox = queryByClass(queryTextFieldContainer!, 'input');

    act(() => {
      userEvent.click(queryInputCombobox!);
    });
    jest.runAllTimers();
    expect(screen.getByText('1234-2345-3456-4567')).toBeInTheDocument();
  });
});
