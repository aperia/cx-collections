import React from 'react';
import { useDispatch } from 'react-redux';
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';
import { useTranslation } from 'app/_libraries/_dls/hooks';

import { I18N_CLIENT_CONFIG } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';

export const ChangeHistoryNavigation = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { isAdminRole } = window.appConfig.commonConfig || {};

  const handleChangeHistoryTab = () => {
    dispatch(
      actionsTab.addTab({
        id: 'changeHistory',
        title: I18N_CLIENT_CONFIG.CHANGE_HISTORY,
        storeId: 'changeHistory',
        tabType: 'changeHistory',
        iconName: 'file'
      })
    );
  };

  return (
    <>
      {isAdminRole && (
        <span
          className="link-header"
          onClick={handleChangeHistoryTab}
          data-testid={genAmtId('header_change_history', 'title', '')}
        >
          {t(I18N_CLIENT_CONFIG.CHANGE_HISTORY)}
        </span>
      )}
    </>
  );
};
