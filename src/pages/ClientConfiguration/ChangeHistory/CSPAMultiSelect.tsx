import React, { useMemo } from 'react';

// components
import MultiSelect from 'app/_libraries/_dls/components/MultiSelect';
import { FormikProps } from 'formik';

// constants
import { HistoryFilter, ChangedCSPA } from './types';
import { FILTER_FORM_NAME } from './constants';
import { I18N_CHANGE_HISTORY } from 'app/constants/i18n';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useSelector } from 'react-redux';
import { selectedCSPAList } from './_redux/selectors';
import { getGroupFormatInputText } from 'app/helpers/formatMultiSelectText';

const CSPAMultiSelect: React.FC<FormikProps<HistoryFilter>> = ({
  values,
  handleChange,
  handleBlur
}) => {
  const { t } = useTranslation();
  const data = useSelector(selectedCSPAList);
  const testId = 'clientConfig_changeHistory_filterCSPAMultiSelect';

  const dropdownListItem = useMemo(() => {
    return data?.map((item: ChangedCSPA, index) => {
      return (
        <MultiSelect.Item
          key={`${item.id}-${index}`}
          label={item.value}
          value={item}
          variant="checkbox"
        />
      );
    });
  }, [data]);

  return (
    <div className="col-12 mt-16">
      <MultiSelect
        name={FILTER_FORM_NAME.CSPA_LIST}
        label={t(I18N_CHANGE_HISTORY.CSPA)}
        value={values?.cspaList}
        onChange={handleChange}
        onBlur={handleBlur}
        textField="value"
        variant="group"
        checkAll
        checkAllLabel={t('txt_all')}
        searchBar
        dataTestId={testId}
        noResult={t('txt_no_results_found')}
        searchBarPlaceholder={t('txt_type_to_search')}
        groupFormatInputText={(...args) =>
          t(getGroupFormatInputText(...args), {
            selected: args[0],
            items: args[1]
          })
        }
      >
        {dropdownListItem}
      </MultiSelect>
    </div>
  );
};

export default CSPAMultiSelect;
