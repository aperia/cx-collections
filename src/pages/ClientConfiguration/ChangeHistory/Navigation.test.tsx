import React from 'react';
import { renderMockStore } from 'app/test-utils';
import { ChangeHistoryNavigation } from './Navigation';
import { screen } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';

HTMLCanvasElement.prototype.getContext = jest.fn();

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    org: 'org',
    app: 'app',
    isAdminRole: true
  }
};

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

describe('should test Navigation', () => {
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<ChangeHistoryNavigation />, { initialState });
  };

  const initialState: Partial<RootState> = {};

  it('should render ui', () => {
    renderWrapper(initialState);
    const btn = screen.getByTestId('header_change_history_title');
    userEvent.click(btn);
    expect(screen.getByText('txt_change_history')).toBeInTheDocument();
  });

  it('should render ui > case 2', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: undefined
    };
    renderWrapper(initialState);
  });
});
