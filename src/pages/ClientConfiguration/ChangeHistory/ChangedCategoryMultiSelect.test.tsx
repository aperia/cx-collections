import React from 'react';
import { renderMockStore } from 'app/test-utils';
import { FormikProps } from 'formik';
import { HistoryFilter } from './types';
import ChangedCategoryMultiSelect from './ChangedCategoryMultiSelect';
import { screen } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import { act } from '@testing-library/react';

HTMLCanvasElement.prototype.getContext = jest.fn();

const props = {} as FormikProps<HistoryFilter>;

describe('should test changed category multi select', () => {
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<ChangedCategoryMultiSelect {...props} />, {
      initialState
    });
  };

  it('should render UI', () => {
    jest.useFakeTimers();

    const { baseElement } = renderWrapper({
      changeHistory: {
        filterData: {
          changedCategoryList: [
            { id: '1', value: 'categoryA', description: 'categoryA' }
          ]
        }
      }
    });

    const queryTextFieldContainer = queryByClass(
      baseElement as HTMLElement,
      'text-field-container'
    );

    const queryInputCombobox = queryByClass(queryTextFieldContainer!, 'input');

    act(() => {
      userEvent.click(queryInputCombobox!);
    });
    jest.runAllTimers();

    expect(screen.getByText('categoryA')).toBeInTheDocument();
  });
});
