import React from 'react';
import * as formik from 'formik';
import { renderMockStore } from 'app/test-utils';
import{ FilterForm } from './FilterForm';
import { screen } from '@testing-library/dom';
import { initialFilterData } from './constants';
import userEvent from '@testing-library/user-event';
import { I18N_CHANGE_HISTORY } from 'app/constants/i18n';
import { fireEvent, waitFor } from '@testing-library/react';

const mockSubmit = jest.fn();
HTMLCanvasElement.prototype.getContext = jest.fn();

const mockFormik =
  (values?: Record<string, any>) =>
  ({ onSubmit }: any) => {
    return (
      <>
        <button data-testid="submitBtn" onClick={() => onSubmit(values)}>
          run
        </button>
      </>
    );
  };

describe('should test filter form', () => {
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<FilterForm onSubmit={mockSubmit} />, {
      initialState
    });
  };

  it('should test form event', async () => {
    const spy = jest.spyOn(formik, 'Formik').mockImplementation(
      mockFormik({
        values: initialFilterData,
        touched: {
          rangeDate: true
        },
        error: {}
      })
    );
    renderWrapper({
      changeHistory: {
        filter: initialFilterData
      }
    });

    const submitBtn = screen.getByTestId('submitBtn');
    await waitFor(() => {
      userEvent.click(submitBtn);
    });

    spy.mockRestore();
    expect(mockSubmit).toBeCalled();
  });

  it('Test handleResetForm action', async () => {
    const { baseElement } = renderWrapper({
      changeHistory: {
        filter: initialFilterData
      }
    });

    const dateInput: HTMLInputElement | null = baseElement.querySelector(
      '.text-field-container input'
    );
    fireEvent.touchStart(dateInput!);
    const resetBtn = screen.getByText(I18N_CHANGE_HISTORY.RESET_TO_DEFAULT);
    await waitFor(() => {
      userEvent.click(resetBtn);
    });
    expect(dateInput?.value).toEqual('');
  });

  it('Test form error', async () => {
    const { baseElement } = renderWrapper({
      changeHistory: {
        filter: {
          ...initialFilterData,
          rangeDate: {
            start: new Date('2021-08-12T06:43:34.243Z')
          }
        }
      }
    });

    const dateInput: HTMLInputElement | null = baseElement.querySelector(
      '.text-field-container > input'
    );
    fireEvent.touchStart(dateInput!);
    const submitBtn = screen.getByText(I18N_CHANGE_HISTORY.APPLY);
    await waitFor(() => {
      userEvent.click(submitBtn);
    });

    expect(dateInput?.value).not.toEqual('');
  });
});
