import { DateRangePickerValue, SortType } from 'app/_libraries/_dls/components';

export interface ChangeHistoryState {
  histories?: ClientConfigurationHistory[];
  filter?: HistoryFilter;
  filterData?: HistoryFilterData;
  loading?: boolean;
  isFiltered?: boolean;
  isFilterReset?: boolean;
  defaultFilter?: HistoryFilter;
  oldValue?: MagicKeyValue | string[];
  sortType: SortType;
}

export interface ClientConfigurationHistory {
  date?: string;
  time?: string;
  changedBy?: string;
  cspa?: string;
  action?: string;
  changedCategory?: string;
  changedSection?: string;
  changedItem?: string;
  before?: string;
  after?: string;
}

export interface ChangedCategory {
  id: string;
  value: string;
  description: string;
}

export interface ChangedCSPA {
  id?: string;
  value?: string;
  description?: string;
  clientId?: string;
  systemId?: string;
  principleId?: string;
  agentId?: string;
}

export interface HistoryFilter {
  rangeDate?: DateRangePickerValue;
  changedCategoryList?: ChangedCategory[];
  cspaList?: ChangedCSPA[];
}

export interface HistoryFilterData {
  changedCategoryList?: ChangedCategory[];
  cspaList?: ChangedCSPA[];
}

export interface FilterFormProps {
  onSubmit: (values: HistoryFilter) => void;
}

export interface GetFilterDataPayload {
  changedCategories?: ChangedCategory[];
  cspaList?: ChangedCSPA[];
}

export interface GetChangedHistoriesPayload {
  histories?: ClientConfigurationHistory[];
}

export interface GetChangedHistoriesArgs extends HistoryFilter {
  lastHistory?: boolean;
}

export interface GetChangedHistoriesApiArgs {
  changedCategoryList?: string[];
  cspaList?: string[];
  startDate?: string;
  endDate?: string;
  common: {
    privileges: string[];
  };
}

export interface ChangedClientConfig {
  category?: string;
}

export type ClientConfigCategory = keyof GetHistoryLabelPayload;

export interface SaveChangedClientConfigArgs {
  action: 'ADD' | 'UPDATE' | 'DELETE' | 'ACTIVATE' | 'DEACTIVATE';
  changedCategory: ClientConfigCategory;
  changedSection?: MagicKeyValue;
  changedItem?: MagicKeyValue;
  oldValue?: MagicKeyValue;
  newValue?: MagicKeyValue;
}

export interface ConfigChangeHistoryItem {
  date?: string;
  time?: string;
  operatorId?: string;
  cspaLevel?: string;
  action?: 'ADD' | 'UPDATE' | 'DELETE' | 'ACTIVATE' | 'DEACTIVATE';
  changedCategory?: string;
  changedSection?: string;
  changedItem?: string;
  oldValue?: string;
  newValue?: string;
}

export interface SaveChangedHistoryApiArgs {
  common: {
    privileges: string[];
  };
  configChangeHistoryList: ConfigChangeHistoryItem[];
}

export interface HistoryLabelItem {
  action: string[];
  category: string;
  section: string;
  item: string;
  value: MagicKeyValue;
}

export interface GetHistoryLabelPayload {
  adjustmentTransaction?: HistoryLabelItem[];
  debtManagementCompanies?: HistoryLabelItem[];
  callResult?: HistoryLabelItem[];
  clientContactInformation?: HistoryLabelItem[];
  talkingPoints?: HistoryLabelItem[];
  clusterToQueueMapping?: HistoryLabelItem[];
  functionRuleMapping?: HistoryLabelItem[];
  letterList?: HistoryLabelItem[];
  payByPhone?: HistoryLabelItem[];
  settlementPayment?: HistoryLabelItem[];
  promiseToPay?: HistoryLabelItem[];
  accountExternalStatus?: HistoryLabelItem[];
  accountExternalStatusReasonCode?: HistoryLabelItem[];
  authentication?: HistoryLabelItem[];
  codeToText?: HistoryLabelItem[];
  memos?: HistoryLabelItem[];
  references?: HistoryLabelItem[];
  regulatory?: HistoryLabelItem[];
  timer?: HistoryLabelItem[];
  contactAccountStatusEntitlement?: HistoryLabelItem[];
  userRoleEntitlement?: HistoryLabelItem[];
  clientConfiguration?: HistoryLabelItem[];
}

export interface GetFormattedHistoryData {
  item: SaveChangedClientConfigArgs;
  historyLabelItem: HistoryLabelItem;
  cspa: string;
}

export interface GetCSPAConfig extends SaveChangedClientConfigArgs {}

export enum TypeDataChangeHistory {
  MarkdownText = 0,
  MentionText,
  NormalText
}