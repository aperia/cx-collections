import React from 'react';

// components
import { Button, DateRangePicker } from 'app/_libraries/_dls/components';
import { Form, Formik, FormikProps } from 'formik';
import ChangedCategoryMultiSelect from './ChangedCategoryMultiSelect';
import CSPAMultiSelect from './CSPAMultiSelect';

// constants
import { I18N_CHANGE_HISTORY } from 'app/constants/i18n';
import { FILTER_FORM_NAME } from './constants';
import { FilterFormProps, HistoryFilter } from './types';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useSelector } from 'react-redux';
import {
  selectedChangedCategories,
  selectedCSPAList,
  selectedFilter
} from './_redux/selectors';

// helpers
import { filterHistoryValidationSchema } from './helpers';
import isUndefined from 'lodash.isundefined';
import { genAmtId } from 'app/_libraries/_dls/utils';

const FilterForm: React.FC<FilterFormProps> = ({ onSubmit }) => {
  const { t } = useTranslation();
  const filter = useSelector(selectedFilter);
  const changedCategories = useSelector(selectedChangedCategories);
  const CSPAList = useSelector(selectedCSPAList);
  const testId = 'clientConfig_changeHistory_filter';

  return (
    <Formik
      initialValues={filter}
      validateOnChange
      validationSchema={filterHistoryValidationSchema}
      onSubmit={onSubmit}
    >
      {(props: FormikProps<HistoryFilter>) => {
        const { values, errors, touched, handleChange, handleBlur, resetForm } =
          props;

        const handleResetForm = () => {
          resetForm({
            values: {
              rangeDate: undefined,
              changedCategoryList: changedCategories,
              cspaList: CSPAList
            }
          });
        };

        const handleDateRangeError = () => {
          if (isUndefined(errors.rangeDate) || !touched.rangeDate)
            return undefined;
          else {
            const rangeDateError =
              (errors.rangeDate as any).start || (errors.rangeDate as any).end;
            return {
              status: true,
              message: t(rangeDateError)
            };
          }
        };

        return (
          <Form className="row">
            <div className="col-12">
              <h4 data-testid={genAmtId(testId, 'title', '')}>
                {t(I18N_CHANGE_HISTORY.FILTER)}
              </h4>
            </div>
            <div className="col-12 mt-24">
              <DateRangePicker
                id={FILTER_FORM_NAME.DATE_RANGE}
                name={FILTER_FORM_NAME.DATE_RANGE}
                label={t(I18N_CHANGE_HISTORY.DATE_RANGE)}
                value={values.rangeDate}
                onChange={handleChange}
                onBlur={handleBlur}
                maxDate={new Date()}
                error={handleDateRangeError()}
                dataTestId={`${testId}_dateRange`}
              />
            </div>
            <ChangedCategoryMultiSelect {...props} />
            <CSPAMultiSelect {...props} />
            <div className="col-12 mt-24 text-right">
              <Button
                variant="secondary"
                size="sm"
                onClick={handleResetForm}
                dataTestId={`${testId}_resetToDefault-btn`}
              >
                {t(I18N_CHANGE_HISTORY.RESET_TO_DEFAULT)}
              </Button>
              <Button
                type="submit"
                variant="primary"
                size="sm"
                dataTestId={`${testId}_apply-btn`}
              >
                {t(I18N_CHANGE_HISTORY.APPLY)}
              </Button>
            </div>
          </Form>
        );
      }}
    </Formik>
  );
};

export { FilterForm };
