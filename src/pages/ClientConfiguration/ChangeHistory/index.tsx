import React, { useEffect, useState } from 'react';

// components
import {
  Button,
  Icon,
  Popover,
  SimpleBar
} from 'app/_libraries/_dls/components';
import { FilterForm } from './FilterForm';
import HistoryGridView from './HistoryGridView';

// constants
import { I18N_CHANGE_HISTORY, I18N_CLIENT_CONFIG } from 'app/constants/i18n';
import { ClientConfigurationHistory, HistoryFilter } from './types';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { changeHistoryActions } from './_redux/reducers';
import {
  selectedDefaultFilter,
  selectedFilter,
  selectedHistories,
  selectedIsFilterReset,
  selectedLoading
} from './_redux/selectors';

// helpers
import isEqual from 'lodash.isequal';
import isUndefined from 'lodash.isundefined';
import isEmpty from 'lodash.isempty';
import ClearAndResetView from './ClearAndResetView';
import classNames from 'classnames';
import { genAmtId } from 'app/_libraries/_dls/utils';

const ChangeHistory: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const filter = useSelector(selectedFilter);
  const [openPopover, setOpenPopover] = useState(false);
  const DEFAULT_FILTER = useSelector(selectedDefaultFilter);
  const isFilterReset = useSelector(selectedIsFilterReset);
  const histories: ClientConfigurationHistory[] =
    useSelector(selectedHistories);
  const isFiltered = !isEqual(filter, DEFAULT_FILTER);
  const isLoading = useSelector(selectedLoading);

  const testId = 'clientConfig_changeHistory';

  const submitFilterForm = (values: HistoryFilter) => {
    dispatch(changeHistoryActions.updateFilter(values));
    setOpenPopover(false);
  };

  useEffect(() => {
    dispatch(changeHistoryActions.getFilterData());
  }, [dispatch]);

  return (
    <div className="h-100">
      <SimpleBar>
        <div className="max-width-lg mx-auto p-24">
          <div className="d-flex align-items-center justify-content-between">
            <h3 data-testid={genAmtId(testId, 'title', '')}>
              {t(I18N_CLIENT_CONFIG.CHANGE_HISTORY)}
            </h3>
            <Popover
              triggerClassName="mr-n8"
              size="md"
              element={<FilterForm onSubmit={submitFilterForm} />}
              placement="bottom-end"
              opened={openPopover}
              onVisibilityChange={setOpenPopover}
            >
              {(!isEmpty(histories) || (isEmpty(histories) && isFiltered)) && (
                <Button
                  className={classNames({
                    active: openPopover
                  })}
                  onClick={() => setOpenPopover(true)}
                  variant="outline-primary"
                  size="sm"
                  hidden={isUndefined(filter) || isLoading}
                  dataTestId={`${testId}_filter-btn`}
                >
                  {!isEqual(filter, DEFAULT_FILTER) && (
                    <div className="filter-button" />
                  )}
                  <Icon name="filter" />
                  {t(I18N_CHANGE_HISTORY.FILTER)}
                </Button>
              )}
            </Popover>
          </div>
          {!isEmpty(histories) &&
          (isFiltered || (!isFiltered && isFilterReset)) &&
          !isLoading ? (
            <div className="mt-12 mb-n12 text-right mr-n8">
              <ClearAndResetView />
            </div>
          ) : null}
          <HistoryGridView />
        </div>
      </SimpleBar>
    </div>
  );
};

export default ChangeHistory;
