import { apiService } from 'app/utils/api.service';
import { GetChangedHistoriesApiArgs, SaveChangedHistoryApiArgs } from './types';

export const changeHistoryServices = {
  getChangedCategories() {
    const url = window.appConfig?.api?.changeHistory.getChangedCategories || '';

    return apiService.get(url);
  },
  getChangedHistories(args: GetChangedHistoriesApiArgs) {
    const url = window.appConfig?.api?.changeHistory.getChangedHistories || '';

    return apiService.post(url, args);
  },
  saveChangedHistory(args: SaveChangedHistoryApiArgs) {
    const url = window.appConfig?.api?.changeHistory.addChangeHistories || '';

    return apiService.post(url, args);
  },
  getHistoryLabel() {
    const url = window.appConfig?.api?.changeHistory.getHistoryLabel || '';

    return apiService.get(url);
  }
};
