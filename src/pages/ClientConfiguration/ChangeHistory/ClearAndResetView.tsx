import React, { useCallback } from 'react';
import { Button } from 'app/_libraries/_dls/components';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import { selectedDefaultFilter } from './_redux/selectors';
import { changeHistoryActions } from './_redux/reducers';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_CHANGE_HISTORY } from 'app/constants/i18n';

const ClearAndResetView: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const testId = 'clientConfig_changeHistory_clearAndReset';

  const DEFAULT_FILTER = useSelector(selectedDefaultFilter);

  const clearFilterAndReset = useCallback(() => {
    batch(() => {
      dispatch(changeHistoryActions.resetFilter());
      dispatch(changeHistoryActions.updateFilter(DEFAULT_FILTER));
    });
  }, [DEFAULT_FILTER, dispatch]);

  return (
    <Button
      size="sm"
      variant="outline-primary"
      onClick={clearFilterAndReset}
      dataTestId={testId}
    >
      {t(I18N_CHANGE_HISTORY.CLEAR_AND_RESET)}
    </Button>
  );
};

export default ClearAndResetView;
