import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import ClearAndResetView from './ClearAndResetView';
import { screen } from '@testing-library/dom';
import { changeHistoryActions } from './_redux/reducers';

const mockChangeHistoryActions = mockActionCreator(changeHistoryActions);

describe('should test clear and reset view', () => {
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<ClearAndResetView />, { initialState });
  };

  it('should render UI', () => {
    const mockResetFilter = mockChangeHistoryActions('resetFilter');
    const mockUpdateFilter = mockChangeHistoryActions('updateFilter');

    renderWrapper({
      changeHistory: {
        defaultFilter: {}
      }
    });

    const clearAndResetButton = screen.getByText('txt_clear_and_reset');
    clearAndResetButton.click();

    expect(mockResetFilter).toBeCalled();
    expect(mockUpdateFilter).toBeCalledWith({});
  });
});
