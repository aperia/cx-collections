import { changeHistoryServices } from './changeHistoryServices';

// utils
import { mockApiServices } from 'app/test-utils';
import { apiUrl, mockAppConfigApi } from 'app/test-utils/mocks/mockProperty';
import { GetChangedHistoriesApiArgs, SaveChangedHistoryApiArgs } from './types';
import { ADMIN_ROLE } from 'pages/__commons/UserRole/constants';

describe('changeHistoryServices', () => {
  describe('getChangedCategories', () => {
    it('when url is not defined', () => {
      mockAppConfigApi.clear();
      const mockService = mockApiServices.get();
      changeHistoryServices.getChangedCategories();

      expect(mockService).toBeCalledWith('');
    });

    it('when url is defined', () => {
      mockAppConfigApi.setup();
      const mockService = mockApiServices.get();
      changeHistoryServices.getChangedCategories();

      expect(mockService).toBeCalledWith(
        apiUrl.changeHistory.getChangedCategories
      );
    });
  });

  describe('getChangedHistories', () => {
    const params = {
      changedCategoryList: [],
      cspaList: [],
      common: {
        privileges: []
      }
    } as GetChangedHistoriesApiArgs;

    it('when url is not defined', () => {
      mockAppConfigApi.clear();
      const mockService = mockApiServices.post();
      changeHistoryServices.getChangedHistories(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url is defined', () => {
      mockAppConfigApi.setup();
      const mockService = mockApiServices.post();
      changeHistoryServices.getChangedHistories(params);

      expect(mockService).toBeCalledWith(
        apiUrl.changeHistory.getChangedHistories,
        params
      );
    });
  });

  describe('saveChangedHistories', () => {
    const params = {
      common: {
        privileges: [ADMIN_ROLE]
      },
      configChangeHistoryList: [
        {
          action: 'UPDATE',
          changedCategory: 'Client Contact Information',
          changedSection: 'Client Contact Information',
          changedItem: 'Client Contact Information',
          newValue: ''
        }
      ]
    } as SaveChangedHistoryApiArgs;

    it('when url is not defined', () => {
      mockAppConfigApi.clear();
      const mockService = mockApiServices.post();
      changeHistoryServices.saveChangedHistory(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url is defined', () => {
      mockAppConfigApi.setup();
      const mockService = mockApiServices.post();
      changeHistoryServices.saveChangedHistory(params);

      expect(mockService).toBeCalledWith(
        apiUrl.changeHistory.addChangeHistories,
        params
      );
    });
  });

  describe('getHistoryLabel', () => {
    it('when url is not defined', () => {
      mockAppConfigApi.clear();
      const mockService = mockApiServices.get();
      changeHistoryServices.getHistoryLabel();

      expect(mockService).toBeCalledWith('');
    });

    it('when url is defined', () => {
      mockAppConfigApi.setup();
      const mockService = mockApiServices.get();
      changeHistoryServices.getHistoryLabel();

      expect(mockService).toBeCalledWith(apiUrl.changeHistory.getHistoryLabel);
    });
  });
});
