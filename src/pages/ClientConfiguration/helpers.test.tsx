import React, { Suspense } from 'react';
import { AccountDetailProvider } from 'app/hooks';
import { mockActionCreator, renderMockStore, storeId } from 'app/test-utils';
import { applyMiddleware, createStore, Store } from 'redux';
import * as helpers from './helpers';
import { ConfigData } from './types';
import { SORT_TYPE } from 'app/constants';
import { MemoConfigItem } from './Memos/types';
import { DEFAULT_MEMO_CONFIG } from './Memos/constants';
import { NAME_ACTION } from './constants';
import { clientConfigurationActions } from './_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { rootReducer } from 'storeConfig';
import thunk from 'redux-thunk';
import { configurationContactAccountEntitlementActions } from 'pages/ConfigurationEntitlement/ContactAndAccountEntitlement/_redux/reducer';
HTMLCanvasElement.prototype.getContext = jest.fn();
jest.mock('./AccountExternalStatus', () => () => (
  <div>AccountExternalStatus</div>
));
jest.mock('./AdjustmentTransaction', () => () => (
  <div>AdjustmentTransaction</div>
));
jest.mock('./CodeToText', () => () => <div>CodeToText</div>);
jest.mock('./CallResult', () => () => <div>CallResult</div>);
jest.mock('./TalkingPoint', () => () => <div>TalkingPoint</div>);
jest.mock('./Timer', () => () => <div>Timer</div>);
jest.mock('./Authentication', () => () => <div>Authentication</div>);
jest.mock('./Letter', () => () => <div>Letter</div>);
jest.mock('./PayByPhone', () => () => <div>PayByPhone</div>);
jest.mock('./PromiseToPay', () => () => <div>PromiseToPay</div>);
jest.mock('./ClientContactInformation', () => () => (
  <div>ClientContactInformation</div>
));
jest.mock('./DebtManageCompanies', () => () => <div>DebtManageCompanies</div>);
jest.mock('./ClusterToQueueMapping', () => () => (
  <div>ClusterToQueueMapping</div>
));
jest.mock('./References', () => () => <div>References</div>);
jest.mock('./FunctionRuleMapping', () => () => <div>FunctionRuleMapping</div>);
jest.mock('./SendLetter', () => () => <div>SendLetter</div>);
jest.mock('./Memos', () => () => <div>Memos</div>);

const queues = [
  {
    queueId: '160675456170',
    queueName: 'COLX_DEFAULT',
    queueGroupIdOwner: 'FD - Admin',
    userRoleName: [
      { code: 'CXCOL1', text: 'CXCOL1' },
      { code: 'CXCOL2', text: 'CXCOL2' }
    ],
    accessLevel: 'UPDATE'
  },
  {
    queueId: '160675456170',
    queueName: 'queueName',
    queueGroupIdOwner: 'FD - Admin',
    userRoleName: [
      { code: 'CXCOL1', text: 'CXCOL1' },
      { code: 'CXCOL2', text: 'CXCOL2' }
    ],
    accessLevel: 'UPDATE'
  }
];

describe('ClientConfiguration helpers', () => {
  let store: Store,
    toastSpy: any,
    mockAction: any,
    mockAction1: any,
    mockAction2: any;

  beforeEach(() => {
    store = createStore(rootReducer, {}, applyMiddleware(thunk));
    toastSpy = mockActionCreator(actionsToast);
    mockAction = mockActionCreator(clientConfigurationActions);
    mockAction1 = mockActionCreator(
      configurationContactAccountEntitlementActions
    );
    mockAction2 = mockActionCreator(
      configurationContactAccountEntitlementActions
    );
  });
  it('mapConfig', () => {
    const value = [storeId, storeId, storeId, storeId, 'descriptions'];
    expect(helpers.mapConfig(value)).toEqual({
      clientId: storeId,
      systemId: storeId,
      principleId: storeId,
      agentId: storeId,
      description: 'descriptions'
    });
  });

  it('dispatchTabChangingTabEvent', () => {
    expect(helpers.dispatchTabChangingTabEvent()).toBeUndefined();
  });

  it('dispatchControlTabChangeEvent', () => {
    expect(helpers.dispatchControlTabChangeEvent()).toBeUndefined();
  });

  it('dispatchClosingModalEvent', () => {
    expect(helpers.dispatchClosingModalEvent()).toBeUndefined();
  });

  it('dispatchCloseModalEvent', () => {
    expect(helpers.dispatchCloseModalEvent()).toBeUndefined();
  });

  describe('mapSectionList', () => {
    const renderComponent = (Component: React.FC) => {
      return renderMockStore(
        <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
          <Suspense fallback={<div />}>
            <Component />
          </Suspense>
        </AccountDetailProvider>,
        { initialState: {} }
      );
    };

    it('testRenderLazy', () => {
      const runTest = (ky: string) => {
        const sections = [{ value: ky, description: ky }];

        const { id, title, Component } = helpers.mapSectionList(sections)[0];

        const wrapper = renderComponent(Component);

        expect(wrapper).not.toBeNull();

        expect(id).toEqual(ky);
        expect(title).toEqual(ky);
      };

      runTest('');
      runTest(helpers.ACC_EXTERNAL_STATUS);
      runTest(helpers.ADJUSTMENT_TRANSACTION);
      runTest(helpers.CODE_TO_TEXT);
      runTest(helpers.CALL_RESULT);
      runTest(helpers.TALKING_POINT);
      runTest(helpers.TIMER);
      runTest(helpers.AUTHENTICATION);
      runTest(helpers.LETTER_LIST);
      runTest(helpers.PAY_BY_PHONE);
      runTest(helpers.PROMISE_TO_PAY);
      runTest(helpers.CLIENT_CONTACT_INFORMATION);
      runTest(helpers.DEBT_MANAGEMENT_COMPANIES);
      runTest(helpers.CLUSTER_TO_QUEUE_MAPPING);
      runTest(helpers.REFERENCES);
      runTest(helpers.FUNCTION_RULE_MAPPING);
      runTest(helpers.REGULATORY);
      runTest(helpers.MEMOS);
      runTest(helpers.SETTEMENT_PAYMENT);
    });
  });

  it('filterClientConfigList', () => {
    const data: ConfigData[] = [{ id: 'id' }];

    expect(helpers.filterClientConfigList(data, '')).toEqual([]);
  });

  it('sortClientConfigList', () => {
    const dataCase1 = [
      {
        id: '1',
        additionalFields: [
          {
            name: 'isDefaultClientInfoKey',
            active: true
          }
        ]
      },
      {
        id: '2',
        additionalFields: [
          {
            name: 'name',
            active: true
          }
        ]
      }
    ];

    const dataCase2 = [
      {
        id: '1',
        additionalFields: [
          {
            name: 'name',
            active: true
          }
        ]
      },
      {
        id: '2',
        additionalFields: [
          {
            name: 'isDefaultClientInfoKey',
            active: true
          }
        ]
      }
    ];

    const dataCase3 = [
      {
        id: '1',
        additionalFields: [
          {
            name: 'name',
            active: true
          }
        ]
      },
      {
        id: '2',
        additionalFields: [
          {
            name: 'name',
            active: true
          }
        ]
      }
    ];

    const dataCase4 = [
      {
        id: '1',
        additionalFields: [
          {
            name: 'name',
            active: false
          }
        ]
      },
      {
        id: '2',
        additionalFields: [
          {
            name: 'name',
            active: true
          }
        ]
      }
    ];

    const dataCase5 = [
      {
        id: '1',
        additionalFields: [
          {
            name: 'name',
            active: false
          }
        ]
      },
      {
        id: '2',
        additionalFields: [
          {
            name: 'name',
            active: true
          }
        ]
      }
    ];

    const dataCase6 = [
      {
        id: '1',
        additionalFields: [
          {
            name: 'name',
            active: false
          }
        ]
      },
      {
        id: '2',
        additionalFields: [
          {
            name: 'name',
            active: true
          }
        ]
      }
    ];

    const dataCase7 = [
      {
        id: '1',
        additionalFields: [
          {
            name: 'name',
            active: false
          }
        ]
      },
      {
        id: '2',
        additionalFields: [
          {
            name: 'name',
            active: true
          }
        ]
      }
    ];

    const dataCase8 = [
      {
        id: '1',
        additionalFields: [
          {
            name: 'isConfigActive',
            active: true
          }
        ]
      },
      {
        id: '2',
        additionalFields: [
          {
            name: 'isConfigActive',
            active: false
          }
        ]
      }
    ];

    const dataCase9 = [
      {
        id: '1',
        additionalFields: [
          {
            name: 'isConfigActive',
            active: true
          }
        ]
      },
      {
        id: '2',
        additionalFields: [
          {
            name: 'isConfigActive',
            active: false
          }
        ]
      }
    ];

    const dataCase10 = [
      {
        id: '1',
        additionalFields: [
          {
            name: 'name',
            active: false
          }
        ]
      },
      {
        id: '2',
        additionalFields: [
          {
            name: 'name',
            active: true
          }
        ]
      }
    ];

    // Case 1
    const resultCase1 = helpers.sortClientConfigList(dataCase1, {
      id: '',
      order: ''
    });
    expect(resultCase1).toEqual(dataCase1);

    // Case 2
    const resultCase2 = helpers.sortClientConfigList(dataCase2, {
      id: '',
      order: ''
    });
    expect(resultCase2).toEqual([
      {
        additionalFields: [
          {
            active: true,
            name: 'isDefaultClientInfoKey'
          }
        ],
        id: '2'
      },
      {
        additionalFields: [
          {
            active: true,
            name: 'name'
          }
        ],
        id: '1'
      }
    ]);

    // Case 3
    const resultCase3 = helpers.sortClientConfigList(dataCase3, {
      id: 'status',
      order: ''
    });
    expect(resultCase3).toEqual(dataCase3);

    // Case 4
    const resultCase4 = helpers.sortClientConfigList(dataCase3, {
      id: 'status',
      order: SORT_TYPE.DESC
    });
    expect(resultCase4).toEqual(dataCase3);

    // Case 5
    const resultCase5 = helpers.sortClientConfigList(dataCase4, {
      id: 'status',
      order: SORT_TYPE.DESC
    });
    expect(resultCase5).toEqual(dataCase4);

    // Case 6
    const resultCase6 = helpers.sortClientConfigList(dataCase5, {
      id: 'status',
      order: ''
    });
    expect(resultCase6).toEqual([
      {
        additionalFields: [
          {
            active: true,
            name: 'name'
          }
        ],
        id: '2'
      },
      {
        additionalFields: [
          {
            active: false,
            name: 'name'
          }
        ],
        id: '1'
      }
    ]);

    // Case 7
    const resultCase7 = helpers.sortClientConfigList(dataCase6, {
      id: '',
      order: ''
    });
    expect(resultCase7).toEqual([
      {
        additionalFields: [
          {
            active: true,
            name: 'name'
          }
        ],
        id: '2'
      },
      {
        additionalFields: [
          {
            active: false,
            name: 'name'
          }
        ],
        id: '1'
      }
    ]);

    // Case 8
    const resultCase8 = helpers.sortClientConfigList(dataCase6, {
      id: 'id',
      order: SORT_TYPE.DESC
    });
    expect(resultCase8).toEqual([
      {
        additionalFields: [
          {
            active: true,
            name: 'name'
          }
        ],
        id: '2'
      },
      {
        additionalFields: [
          {
            active: false,
            name: 'name'
          }
        ],
        id: '1'
      }
    ]);

    // Case 9
    const resultCase9 = helpers.sortClientConfigList(dataCase7, {
      id: '',
      order: SORT_TYPE.DESC
    });
    expect(resultCase9).toEqual([
      {
        additionalFields: [
          {
            active: true,
            name: 'name'
          }
        ],
        id: '2'
      },
      {
        additionalFields: [
          {
            active: false,
            name: 'name'
          }
        ],
        id: '1'
      }
    ]);

    // Case 10
    const resultCase10 = helpers.sortClientConfigList(dataCase8, {
      id: 'status',
      order: SORT_TYPE.DESC
    });
    expect(resultCase10).toEqual([
      {
        additionalFields: [
          {
            active: false,
            name: 'isConfigActive'
          }
        ],
        id: '2'
      },
      {
        additionalFields: [
          {
            active: true,
            name: 'isConfigActive'
          }
        ],
        id: '1'
      }
    ]);

    // Case 11
    const resultCase11 = helpers.sortClientConfigList(dataCase9, {
      id: 'status',
      order: SORT_TYPE.ASC
    });
    expect(resultCase11).toEqual(dataCase9);

    // Case 12
    const resultCase12 = helpers.sortClientConfigList(dataCase10, {
      id: 'id',
      order: SORT_TYPE.ASC
    });
    expect(resultCase12).toEqual([
      {
        additionalFields: [
          {
            active: true,
            name: 'name'
          }
        ],
        id: '2'
      },
      {
        additionalFields: [
          {
            active: false,
            name: 'name'
          }
        ],
        id: '1'
      }
    ]);
  });

  it('convertClientInfoId', () => {
    const clientId = 'clientId';
    const agentId = 'agentId';
    const systemId = 'systemId';
    const principleId = 'principleId';
    expect(
      helpers.convertClientInfoId(clientId, agentId, systemId, principleId)
    ).toEqual(`${clientId}-${agentId}-${systemId}-${principleId}`);
  });

  it('isDefaultClientConfig', () => {
    const data = {
      additionalFields: [
        {
          name: 'isDefaultClientInfoKey',
          active: true
        }
      ]
    };
    const result = helpers.isDefaultClientConfig(data);
    expect(result).toBeTruthy();
  });

  it('mappingMemoConfig', () => {
    const data: MemoConfigItem = {
      ...DEFAULT_MEMO_CONFIG,
      default: 'OKLA'
    };
    const result = helpers.mappingMemoConfig(data);
    expect(result).toEqual({
      ...DEFAULT_MEMO_CONFIG,
      default: 'OKLA'
    });
  });

  it('mappingMemoConfig without default data', () => {
    const emptyData = {
      accountDetails: '',
      payment: '',
      callerAuthentication: '',
      letters: '',
      cardHolderMaintenance: '',
      transactionAdjustment: '',
      reviewAdjustment: ''
    };
    const result = helpers.mappingMemoConfig(emptyData);
    expect(result).toEqual(DEFAULT_MEMO_CONFIG);
  });

  it('rejectApi > name action = editCspa', () => {
    const addToastAction = toastSpy('addToast');
    const mockActionError = mockAction('setEditClientConfigErrorMessage');

    helpers.rejectApi({
      errorMsgInline: '',
      toastMsg: '',
      nameAction: NAME_ACTION.editCspa
    })(store.dispatch, store.getState, {});

    expect(mockActionError).toBeCalledWith({ error: '', isRetry: true });
    expect(addToastAction).toBeCalledWith({
      message: '',
      show: true,
      type: 'error'
    });
  });

  it('rejectApi > name action = addCSPA', () => {
    const addToastAction = toastSpy('addToast');
    const mockActionError = mockAction('setAddClientConfigErrorMessage');
    const errorMsgInline = 'errorMsgInline';
    helpers.rejectApi({
      errorMsgInline,
      toastMsg: '',
      nameAction: NAME_ACTION.addCspa
    })(store.dispatch, store.getState, {});

    expect(mockActionError).toBeCalledWith({
      error: errorMsgInline,
      isRetry: true
    });
    expect(addToastAction).toHaveBeenCalled();
  });

  it('rejectApi > name action = addContact', () => {
    const addToastAction = toastSpy('addToast');
    const mockActionError = mockAction1('setAddClientConfigErrorMessage');

    helpers.rejectApi({
      errorMsgInline: '',
      toastMsg: '',
      nameAction: NAME_ACTION.addContact
    })(store.dispatch, store.getState, {});

    expect(mockActionError).toBeCalledWith({ error: '', isRetry: true });
    expect(addToastAction).toBeCalledWith({
      message: '',
      show: true,
      type: 'error'
    });
  });

  it('successApi > name action = editCspa', () => {
    const addToastAction = toastSpy('addToast');
    const mockActionEditModal = mockAction('toggleEditModal');

    helpers.successApi({
      toastMsg: '',
      nameAction: NAME_ACTION.editCspa
    })(store.dispatch, store.getState, {});

    expect(mockActionEditModal).toBeCalled();
    expect(addToastAction).toBeCalledWith({
      message: '',
      show: true,
      type: 'success'
    });
  });

  it('successApi > name action = addContact', () => {
    const addToastAction = toastSpy('addToast');
    const mockActionCloseModal = mockAction2('onCloseModalAddCSPA');
    const mockActionGetContactList = mockAction2('getContactAccListCSPA');

    helpers.successApi({
      toastMsg: '',
      nameAction: NAME_ACTION.addContact
    })(store.dispatch, store.getState, {});

    expect(mockActionCloseModal).toBeCalled();
    expect(mockActionGetContactList).toBeCalled();
    expect(addToastAction).toBeCalledWith({
      message: '',
      show: true,
      type: 'success'
    });
  });

  it('successApi > name action = addCspa', () => {
    const addToastAction = toastSpy('addToast');
    const mockActionToggleAddModal = mockAction('toggleAddModal');

    helpers.successApi({
      toastMsg: '',
      nameAction: NAME_ACTION.addCspa
    })(store.dispatch, store.getState, {});

    expect(mockActionToggleAddModal).toBeCalled();
    expect(addToastAction).toBeCalledWith({
      message: '',
      show: true,
      type: 'success'
    });
  });

  it('searchQueueName with textSearch', () => {
    const data = helpers.searchQueueName(queues, 'COLX_DEFAULT');

    expect(data).toEqual([queues[0]]);
  });

  it('searchQueueName with textSearch empty', () => {
    const data = helpers.searchQueueName(queues, '');

    expect(data).toEqual(queues);
  });
});
