import React from 'react';
import { renderMockStoreId } from 'app/test-utils';
import StatusColumn from './StatusColumn';
import { screen } from '@testing-library/react';
import { ACTIVE, CLIENT_CONFIG_STATUS_KEY, INACTIVE } from '../constants';

describe('Test StatusColumn component', () => {
  it('render inactive', () => {
    renderMockStoreId(<StatusColumn data={{ additionalFields: [] }} />);

    expect(screen.getByText(INACTIVE)).toBeInTheDocument();
  });

  it('render active view', () => {
    const data = {
      clientId: '1234',
      systemId: '1234',
      principleId: '1234',
      agentId: '1234',
      additionalFields: [
        {
          name: CLIENT_CONFIG_STATUS_KEY,
          active: true
        }
      ]
    };
    renderMockStoreId(<StatusColumn data={data} />);

    expect(screen.getByText(ACTIVE)).toBeInTheDocument();
  });
});
