import React from 'react';

// constants
import AddModal from '../AddModal';
import StatusModal from '../StatusModal';
import EditModal from '../EditModal';
import SettingsModal from '../Settings';
import ClientConfigGrid from './ClientConfigGrid';

const ClientConfigurationList: React.FC = () => {
  return (
    <>
      <ClientConfigGrid />
      <SettingsModal />
      <AddModal />
      <EditModal />
      <StatusModal />
    </>
  );
};

export default ClientConfigurationList;
