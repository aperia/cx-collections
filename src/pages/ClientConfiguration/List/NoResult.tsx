import { I18N_CLIENT_CONFIG } from 'app/constants/i18n';
import { Button, Icon } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import React, { useMemo } from 'react';

export interface NoResultPageProps {
  id?: string;
}

const NoResult: React.FC<NoResultPageProps> = ({
  id = 'No_Result',
  ...props
}) => {
  const { t } = useTranslation();
  const testId = 'clientConfig_grid_noResult';
  const element = useMemo(() => {
    return (
      <div className="p-24" data-testid={genAmtId(testId, '', '')}>
        <h3 data-testid={genAmtId(testId, 'title', '')}>{t(I18N_CLIENT_CONFIG.CLIENT_CONFIG_LIST)}</h3>
        <div id={`${id}_results`} className="text-center my-80">
          <Icon name="file" className="fs-80 color-light-l12" />
          <p id={`${id}_description_`} className="mt-20 color-grey">
            {t('txt_no_search_results_found')}
          </p>
          <Button variant="outline-primary" size="sm" className="mt-24">
            {t('txt_add_client_config')}
          </Button>
        </div>
      </div>
    );
  }, [t, id]);

  return element;
};

export default NoResult;
