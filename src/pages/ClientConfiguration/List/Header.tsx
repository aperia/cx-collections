import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { TextSearch, Button } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import React, {
  ChangeEvent,
  Dispatch,
  FC,
  SetStateAction,
  useCallback
} from 'react';
import { isEmpty, isUndefined } from 'app/_libraries/_dls/lodash';

// Redux
import { useDispatch, useSelector } from 'react-redux';
import { clientConfigurationActions } from '../_redux/reducers';
import {
  selectClientConfigList,
  selectDefaultSettingsClientConfig,
  selectIsNoRecords,
  selectIsNoResultsFound,
  selectSearchValue,
  selectTextSearchValue
} from '../_redux/selectors';

export interface HeaderProps {
  setCurrentPage: Dispatch<SetStateAction<number>>;
  setCurrentPageSize: Dispatch<SetStateAction<number>>;
  pageSize: number[];
}

const Header: FC<HeaderProps> = ({
  setCurrentPage,
  setCurrentPageSize,
  pageSize
}) => {
  const dispatch = useDispatch();

  const { t } = useTranslation();

  const data = useSelector(selectClientConfigList);

  const searchValues = useSelector(selectSearchValue);

  const textSearchValue = useSelector(selectTextSearchValue);

  const isNoRecords = useSelector(selectIsNoRecords);

  const isNoResultsFound = useSelector(selectIsNoResultsFound);
  const defaultClientConfig = useSelector(selectDefaultSettingsClientConfig);

  const isNotHaveDefault = isUndefined(defaultClientConfig);
  const testId = 'clientConfig_grid-header';

  const handleAdd = useCallback(() => {
    dispatch(clientConfigurationActions.toggleAddModal());
  }, [dispatch]);

  const handleClearAndReset = () => {
    setCurrentPageSize(pageSize[0]);
    dispatch(clientConfigurationActions.clearAndReset());
  };

  const handleSearch = (searchValue: string) => {
    setCurrentPage(1);
    dispatch(
      clientConfigurationActions.changeSearchValue({
        searchValue
      })
    );
  };

  const handleChangeTextSearch = (e: ChangeEvent<HTMLInputElement>) => {
    dispatch(
      clientConfigurationActions.changeTextSearchValue({
        textSearchValue: e.target.value
      })
    );
  };

  const handleClearTextSearch = () => {
    dispatch(
      clientConfigurationActions.changeTextSearchValue({
        textSearchValue: ''
      })
    );
  };

  return (
    <>
      <div className="d-flex align-items-center justify-content-between mb-16">
        <div className="d-flex">
          <h3 data-testid={genAmtId(testId, 'title', '')}>
            {t(I18N_CLIENT_CONFIG.CLIENT_CONFIG_LIST)}
          </h3>
        </div>
        {!isNoRecords && (
          <TextSearch
            value={textSearchValue}
            onChange={handleChangeTextSearch}
            onClear={handleClearTextSearch}
            onSearch={handleSearch}
            placeholder={t(I18N_COMMON_TEXT.TYPE_TO_SEARCH)}
            dataTestId={`${testId}_textSearch`}
          />
        )}
      </div>
      <div className="text-right mr-n8">
        {isNoResultsFound && (
          <Button
            variant="outline-primary"
            size="sm"
            onClick={handleAdd}
            disabled={isNotHaveDefault}
            dataTestId={`${testId}_addBtn`}
          >
            {t(I18N_CLIENT_CONFIG.ADD_CLIENT_CONFIG)}
          </Button>
        )}

        {!isEmpty(data) && (
          <>
            <Button
              variant="outline-primary"
              size="sm"
              onClick={handleAdd}
              disabled={isNotHaveDefault}
              dataTestId={`${testId}_addBtn`}
            >
              {t(I18N_CLIENT_CONFIG.ADD_CLIENT_CONFIG)}
            </Button>
            {searchValues && (
              <Button
                size="sm"
                variant="outline-primary"
                onClick={handleClearAndReset}
                dataTestId={`${testId}_clearAndResetBtn`}
              >
                {t(I18N_COMMON_TEXT.CLEAR_AND_RESET)}
              </Button>
            )}
          </>
        )}
      </div>
    </>
  );
};

export default Header;
