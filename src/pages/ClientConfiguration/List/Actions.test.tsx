import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import React from 'react';
import Actions from './Actions';
import { screen } from '@testing-library/react';
import { clientConfigurationActions } from '../_redux/reducers';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { CLIENT_CONFIG_STATUS_KEY } from '../constants';

const clientConfigurationActionsSpy = mockActionCreator(
  clientConfigurationActions
);

describe('Test Actions component', () => {
  it('render component', () => {
    renderMockStoreId(
      <Actions
        data={{
          clientId: '1234',
          systemId: '1234',
          principleId: '1234',
          agentId: '1234'
        }}
      />
    );

    expect(screen.getByText('txt_activate')).toBeInTheDocument();
    expect(screen.getByText('txt_edit')).toBeInTheDocument();
    expect(screen.getByText('txt_view_settings')).toBeInTheDocument();
  });

  it('clicks edit button', () => {
    const data = {
      clientId: '1234',
      systemId: '1234',
      principleId: '1234',
      agentId: '1234'
    };
    const toggleEditModal = clientConfigurationActionsSpy('toggleEditModal');
    const updateSelectedConfig = clientConfigurationActionsSpy(
      'updateSelectedConfig'
    );

    renderMockStoreId(<Actions data={data} />);

    const editBtn = screen.getByText('txt_edit');
    editBtn?.click();

    expect(toggleEditModal).toBeCalled();
    expect(updateSelectedConfig).toBeCalledWith({ data });
  });

  it('clicks delete button', () => {
    const data = {
      clientId: '1234',
      systemId: '1234',
      principleId: '1234',
      agentId: '1234'
    };
    const toggleStatusModal =
      clientConfigurationActionsSpy('toggleStatusModal');
    const updateSelectedConfig = clientConfigurationActionsSpy(
      'updateSelectedConfig'
    );

    renderMockStoreId(<Actions data={data} />);

    const deleteBtn = screen.getByText('txt_activate');
    deleteBtn?.click();

    expect(toggleStatusModal).toBeCalled();
    expect(updateSelectedConfig).toBeCalledWith({ data });
  });

  it('click view settings button', () => {
    const data = {
      clientId: '1234',
      systemId: '1234',
      principleId: '1234',
      agentId: '1234'
    };
    const toggleSettingsModal = clientConfigurationActionsSpy(
      'toggleSettingsModal'
    );
    const updateSelectedConfig = clientConfigurationActionsSpy(
      'updateSelectedConfig'
    );

    renderMockStoreId(<Actions data={data} />);

    const viewBtn = screen.getByText('txt_view_settings');
    viewBtn?.click();

    expect(toggleSettingsModal).toBeCalled();
    expect(updateSelectedConfig).toBeCalledWith({ data });
  });

  it('render active view', () => {
    const data = {
      clientId: '1234',
      systemId: '1234',
      principleId: '1234',
      agentId: '1234',
      additionalFields: [
        {
          name: CLIENT_CONFIG_STATUS_KEY,
          active: true
        }
      ]
    };
    renderMockStoreId(<Actions data={data} />);

    expect(screen.getByText(I18N_COMMON_TEXT.DEACTIVATE)).toBeInTheDocument();
  });
});
