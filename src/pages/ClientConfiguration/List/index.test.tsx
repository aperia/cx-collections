import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';

import ClientConfigurationList from '.';
import { screen } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';

import { clientConfigurationActions } from '../_redux/reducers';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import * as usePagination from 'app/hooks/usePagination';
import { act } from 'react-dom/test-utils';

jest.mock('app/_libraries/_dls/utils/genAmtId', () => () => {});
jest.mock('app/_libraries/_dls/hooks/useTranslation', () => () => ({
  t: (text: string) => text
}));

HTMLCanvasElement.prototype.getContext = jest.fn();

const initialState: Partial<RootState> = {
  clientConfiguration: {
    list: {
      loading: false,
      error: false,
      data: [
        { id: '2', clientId: '2' },
        {
          id: '1',
          clientId: '1',
          additionalFields: [
            {
              name: 'isDefaultClientInfoKey',
              value: 'true'
            } as any
          ]
        }
      ],
      sortBy: {
        id: 'clientId',
        order: 'desc'
      },
      searchValue: '1',
      textSearchValue: 'textSearchValue'
    }
  },
  accessConfig: {
    cspaList: [{ cspa: '9999-1000-2000-3000' }]
  }
};

const spyClientConfiguration = mockActionCreator(clientConfigurationActions);

beforeEach(() => {
  jest.spyOn(usePagination, 'usePagination').mockImplementation(
    (data: any[]) =>
      ({
        total: 200,
        currentPage: 1,
        pageSize: [10, 25, 50],
        currentPageSize: 10,
        gridData: data,
        onPageChange: () => {},
        onPageSizeChange: () => {},
        setCurrentPage: () => {},
        setCurrentPageSize: () => {}
      } as any)
  );

  spyClientConfiguration('getAllClientConfigList');
});

describe('Render', () => {
  it('Should render', () => {
    const mockAction = spyClientConfiguration('getAllClientConfigList');

    const { container } = renderMockStore(<ClientConfigurationList />, {
      initialState
    });

    expect(container.querySelector('.dls-grid')).toBeInTheDocument();
    expect(mockAction).toBeCalled();
  });

  it('Should render with empty list', () => {
    // const mockToggleAddModal = spyClientConfiguration('toggleAddModal');
    renderMockStore(<ClientConfigurationList />, {
      initialState: {
        clientConfiguration: {
          list: {
            loading: false,
            error: false,
            data: [],
            sortBy: {
              id: 'clientId',
              order: 'desc'
            },
            searchValue: '',
            textSearchValue: ''
          }
        }
      }
    });

    // userEvent.click(screen.getByText('txt_add_client_config'));

    // expect(
    //   screen.getByText('txt_no_client_config_to_display')
    // ).toBeInTheDocument();
    // expect(mockToggleAddModal).toBeCalled();
  });

  it('Should render with empty filtered data', () => {
    const mockAction = spyClientConfiguration('toggleAddModal');

    const { container } = renderMockStore(<ClientConfigurationList />, {
      initialState: {
        clientConfiguration: {
          list: {
            loading: false,
            error: false,
            data: [
              { id: '2', clientId: '2' },
              { id: '1', clientId: '1' }
            ],
            sortBy: {
              id: 'test',
              order: 'asc'
            },
            searchValue: '123',
            textSearchValue: 'textSearchValue'
          }
        }
      }
    });

    expect(container.querySelector('.dls-grid')).toBeNull();
    expect(mockAction).not.toBeCalled();
  });

  it('Should render with loading', () => {
    const mockAction = spyClientConfiguration('toggleAddModal');

    const { container } = renderMockStore(<ClientConfigurationList />, {
      initialState: {
        clientConfiguration: {
          list: {
            loading: true,
            error: false,
            data: [
              { id: '2', clientId: '2' },
              { id: '1', clientId: '1' }
            ],
            sortBy: {
              id: 'test',
              order: 'asc'
            },
            searchValue: '',
            textSearchValue: ''
          }
        }
      }
    });
    expect(container.querySelector('.dls-grid')).toBeNull();
    expect(mockAction).not.toBeCalled();
  });
});

describe('Actions', () => {
  it('handleReload', () => {
    const mockAction = spyClientConfiguration('getAllClientConfigList');

    renderMockStore(<ClientConfigurationList />, {
      initialState: {
        clientConfiguration: {
          list: {
            loading: false,
            error: true,
            data: [
              { id: '2', clientId: '2' },
              { id: '1', clientId: '1' }
            ],
            sortBy: {
              id: 'test',
              order: 'asc'
            },
            textSearchValue: 'textSearchValue'
          }
        }
      }
    });

    act(() => {
      userEvent.click(screen.getByText(I18N_COMMON_TEXT.RELOAD));
    });

    expect(mockAction).toBeCalled();
  });

  it('handleAdd', () => {
    const mockAction = spyClientConfiguration('getAllClientConfigList');

    renderMockStore(<ClientConfigurationList />, {
      initialState: {
        clientConfiguration: {
          list: {
            loading: false,
            error: false,
            data: [],
            sortBy: {
              id: 'test',
              order: 'asc'
            },
            textSearchValue: 'textSearchValue'
          }
        },
        accessConfig: {
          cspaList: [{ cspa: '9999-1000-2000-3000' }]
        }
      }
    });

    act(() => {
      userEvent.click(screen.getByText(I18N_CLIENT_CONFIG.ADD_CLIENT_CONFIG));
    });

    expect(mockAction).toBeCalled();
  });

  it('handleClearAndReset', () => {
    const mockAction = spyClientConfiguration('clearAndReset');

    renderMockStore(<ClientConfigurationList />, {
      initialState: {
        clientConfiguration: {
          list: {
            loading: false,
            error: false,
            data: [
              { id: '2', clientId: '2' },
              { id: '1', clientId: '1' }
            ],
            sortBy: {
              id: 'clientId',
              order: 'asc'
            },
            searchValue: '123',
            textSearchValue: 'textSearchValue'
          }
        }
      }
    });

    act(() => {
      userEvent.click(screen.getByText(I18N_COMMON_TEXT.CLEAR_AND_RESET));
    });

    expect(mockAction).toBeCalled();
  });

  describe('handleSortChange', () => {
    it('when state has shortBy', () => {
      const mockAction = spyClientConfiguration('updateSortBy');

      const { container } = renderMockStore(<ClientConfigurationList />, {
        initialState
      });

      act(() => {
        userEvent.click(container.querySelector('.dls-grid-head > tr > th')!);
      });

      expect(mockAction).toBeCalled();
    });

    it('when state has shortBy', () => {
      const mockAction = spyClientConfiguration('updateSortBy');

      const { container } = renderMockStore(<ClientConfigurationList />, {
        initialState: {
          clientConfiguration: {
            list: {
              loading: false,
              error: false,
              data: [
                { id: '2', clientId: '2' },
                { id: '1', clientId: '1' }
              ],
              searchValue: '1',
              textSearchValue: 'textSearchValue'
            }
          }
        }
      });

      act(() => {
        userEvent.click(container.querySelector('.dls-grid-head > tr > th')!);
      });

      expect(mockAction).toBeCalledWith({
        id: 'clientId',
        order: 'asc'
      });
    });
  });
});
