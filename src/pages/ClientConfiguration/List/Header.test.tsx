import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';

import Header from './Header';
import { screen } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';

import { clientConfigurationActions } from '../_redux/reducers';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { DEFAULT_CLIENT_CONFIG_KEY } from '../constants';

HTMLCanvasElement.prototype.getContext = jest.fn();

const initialState: Partial<RootState> = {
  clientConfiguration: {
    list: {
      loading: true,
      error: true,
      data: [
        { id: '2', clientId: '2' },
        { id: '1', clientId: '1' }
      ],
      sortBy: {
        id: 'test',
        order: 'asc'
      },
      searchValue: '1',
      textSearchValue: 'textSearchValue'
    }
  }
};

const spyClientConfiguration = mockActionCreator(clientConfigurationActions);

describe('Render', () => {
  it('Should render', () => {
    renderMockStore(
      <Header
        pageSize={[5, 10, 20]}
        setCurrentPage={jest.fn()}
        setCurrentPageSize={jest.fn()}
      />,
      { initialState }
    );

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.CLIENT_CONFIG_LIST)
    ).toBeInTheDocument();
  });

  it('Should render with empty state', () => {
    renderMockStore(
      <Header
        pageSize={[5, 10, 20]}
        setCurrentPage={jest.fn()}
        setCurrentPageSize={jest.fn()}
      />,
      { initialState }
    );

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.CLIENT_CONFIG_LIST)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleAdd', () => {
    const mockAction = spyClientConfiguration('toggleAddModal');

    renderMockStore(
      <Header
        pageSize={[5, 10, 20]}
        setCurrentPage={jest.fn()}
        setCurrentPageSize={jest.fn()}
      />,
      {
        initialState: {
          clientConfiguration: {
            list: {
              loading: true,
              error: true,
              data: [
                { id: '2', clientId: '2' },
                {
                  id: '1',
                  clientId: '1',
                  additionalFields: [
                    {
                      name: DEFAULT_CLIENT_CONFIG_KEY,
                      value: 'true'
                    } as any
                  ]
                }
              ],
              sortBy: {
                id: 'test',
                order: 'asc'
              },
              searchValue: '3',
              textSearchValue: '3'
            }
          }
        }
      }
    );

    userEvent.click(screen.getByText(I18N_CLIENT_CONFIG.ADD_CLIENT_CONFIG));

    expect(mockAction).toBeCalled();
  });

  it('handleClearAndReset', () => {
    const mockAction = spyClientConfiguration('clearAndReset');

    renderMockStore(
      <Header
        pageSize={[5, 10, 20]}
        setCurrentPage={jest.fn()}
        setCurrentPageSize={jest.fn()}
      />,
      { initialState }
    );

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.CLEAR_AND_RESET));

    expect(mockAction).toBeCalled();
  });

  it('handleSearch', () => {
    const mockAction = spyClientConfiguration('changeSearchValue');

    const { container } = renderMockStore(
      <Header
        pageSize={[5, 10, 20]}
        setCurrentPage={jest.fn()}
        setCurrentPageSize={jest.fn()}
      />,
      { initialState }
    );

    userEvent.click(container.querySelector(`.dls-text-search-search`)!);

    expect(mockAction).toBeCalled();
  });

  it('handleChangeTextSearch', () => {
    const mockAction = spyClientConfiguration('changeTextSearchValue');

    const { container } = renderMockStore(
      <Header
        pageSize={[5, 10, 20]}
        setCurrentPage={jest.fn()}
        setCurrentPageSize={jest.fn()}
      />,
      { initialState }
    );

    userEvent.type(container.querySelector(`.dls-input-container input`)!, 'a');

    expect(mockAction).toBeCalled();
  });

  it('handleClearTextSearch', () => {
    const mockAction = spyClientConfiguration('changeTextSearchValue');

    renderMockStore(
      <Header
        pageSize={[5, 10, 20]}
        setCurrentPage={jest.fn()}
        setCurrentPageSize={jest.fn()}
      />,
      { initialState }
    );

    const clearDiv = screen.getByTestId(
      'clientConfig_grid-header_textSearch_dls-text-search_clear-btn'
    ) as HTMLDivElement;

    userEvent.click(clearDiv!);

    expect(mockAction).toBeCalled();
  });
});
