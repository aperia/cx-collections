import React, { useCallback, useMemo } from 'react';
import { isEmpty } from 'lodash';
import classnames from 'classnames';

// Components
import {
  Button,
  Grid,
  Pagination,
  SortType,
  TruncateText
} from 'app/_libraries/_dls/components';
import { FailedApiReload, NoDataGrid } from 'app/components';
import Header from './Header';
import Actions from './Actions';
import { SimpleBar } from 'app/_libraries/_dls/components';
import { ConfigData } from '../types';
import StatusColumn from './StatusColumn';

// Const
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { DEFAULT_SORT_VALUE } from 'app/constants';

// Hooks
import { usePagination } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useDispatch, useSelector } from 'react-redux';

// Redux
import { clientConfigurationActions } from '../_redux/reducers';
import {
  selectClientConfigList,
  selectIsNoResultsFound,
  selectListError,
  selectLoadingClientConfigList,
  selectOriginData,
  selectSortBy
} from '../_redux/selectors';

import { genAmtId } from 'app/_libraries/_dls/utils';
import { selectCSPAFromAccessConfig } from 'pages/__commons/AccessConfig/_redux/selector';

const ClientConfigGrid: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const clientConfigData = useSelector<RootState, ConfigData[]>(
    selectClientConfigList
  );
  const sortBy = useSelector(selectSortBy);
  const isNoResultsFound = useSelector(selectIsNoResultsFound);
  const error = useSelector(selectListError);
  const loading = useSelector(selectLoadingClientConfigList);
  const originData = useSelector(selectOriginData);
  const testId = 'clientConfig_grid';
  const accessConfig = useSelector(selectCSPAFromAccessConfig);

  const CLIENT_CONFIG_LIST_COLUMNS = useMemo(
    () => [
      {
        id: 'clientId',
        Header: t(I18N_COMMON_TEXT.CLIENT_ID),
        isSort: true,
        width: 120,
        accessor: 'clientId'
      },
      {
        id: 'systemId',
        Header: t(I18N_CLIENT_CONFIG.SYSTEM_ID),
        isSort: true,
        width: 120,
        accessor: 'systemId'
      },
      {
        id: 'principleId',
        Header: t(I18N_CLIENT_CONFIG.PRINCIPLE_ID),
        isSort: true,
        width: 120,
        accessor: 'principleId'
      },
      {
        id: 'agentId',
        Header: t(I18N_CLIENT_CONFIG.AGENT_ID),
        isSort: true,
        width: 120,
        accessor: 'agentId'
      },
      {
        id: 'description',
        Header: t(I18N_COMMON_TEXT.DESCRIPTION),
        isSort: true,
        width: 331,
        accessor: (data: ConfigData) => (
          <TruncateText
            lines={2}
            title={data?.description}
            resizable
            ellipsisMoreText={t(I18N_COMMON_TEXT.MORE)}
            ellipsisLessText={t(I18N_COMMON_TEXT.LESS)}
            className="text-break"
          >
            {data?.description}
          </TruncateText>
        ),
        autoWidth: true
      },
      {
        id: 'status',
        Header: t(I18N_COMMON_TEXT.STATUS),
        isSort: true,
        width: 102,
        accessor: (data: ConfigData) => <StatusColumn data={data} />
      },
      {
        id: 'action',
        Header: t(I18N_COMMON_TEXT.ACTIONS),
        cellHeaderProps: { className: 'text-center' },
        isFixedRight: true,
        width: 274,
        cellBodyProps: { className: 'text-right td-sm' },
        accessor: (data: ConfigData, idx: number) => (
          <Actions data={data} dataTestId={`${idx}_action_${testId}`} />
        )
      }
    ],
    [t]
  );

  React.useEffect(() => {
    if (!isEmpty(accessConfig.clientNumber)) {
      dispatch(clientConfigurationActions.getAllClientConfigList());
    }
  }, [accessConfig.clientNumber, dispatch]);

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange,
    setCurrentPage,
    setCurrentPageSize
  } = usePagination(clientConfigData);

  const showPagination = total > pageSize[0];

  const handleClearAndReset = useCallback(() => {
    setCurrentPageSize(pageSize[0]);
    dispatch(clientConfigurationActions.clearAndReset());
  }, [dispatch, setCurrentPageSize, pageSize]);

  const handleSortChange = (newSortBy: SortType) => {
    dispatch(
      clientConfigurationActions.updateSortBy(
        newSortBy?.order ? newSortBy : DEFAULT_SORT_VALUE
      )
    );
  };

  const handleReload = () => {
    dispatch(clientConfigurationActions.getAllClientConfigList());
  };

  const handleAdd = () => {
    dispatch(clientConfigurationActions.toggleAddModal());
  };

  if (error) {
    return (
      <div className="p-24 max-width-lg mx-auto">
        <Header
          setCurrentPage={setCurrentPage}
          setCurrentPageSize={setCurrentPageSize}
          pageSize={pageSize}
        />
        <FailedApiReload
          id="client-config-list-failed"
          className="mt-80"
          onReload={handleReload}
          dataTestId={`${testId}_failedApi`}
        />
      </div>
    );
  }

  if (loading) return <div className={classnames('h-100', { loading })} />;

  if (isEmpty(originData)) {
    return (
      <div className="p-24">
        <h3 data-testid={genAmtId(testId, 'title', '')}>
          {t(I18N_CLIENT_CONFIG.CLIENT_CONFIG_LIST)}
        </h3>
        <NoDataGrid
          text="txt_no_client_config_to_display"
          dataTestId={`${testId}_noData`}
        >
          <div className="mt-24">
            <Button
              size="sm"
              variant="outline-primary"
              onClick={handleAdd}
              dataTestId={`${testId}_addConfigBtn`}
            >
              {t(I18N_CLIENT_CONFIG.ADD_CLIENT_CONFIG)}
            </Button>
          </div>
        </NoDataGrid>
      </div>
    );
  }

  if (isNoResultsFound) {
    return (
      <div className="p-24 max-width-lg mx-auto">
        <Header
          setCurrentPage={setCurrentPage}
          setCurrentPageSize={setCurrentPageSize}
          pageSize={pageSize}
        />
        <NoDataGrid
          text="txt_no_search_results_found"
          dataTestId={`${testId}_noResultsFound`}
        >
          <div className="mt-24">
            <Button
              size="sm"
              variant="outline-primary"
              onClick={handleClearAndReset}
              dataTestId={`${testId}_clearAndResetBtn`}
            >
              {t(I18N_COMMON_TEXT.CLEAR_AND_RESET)}
            </Button>
          </div>
        </NoDataGrid>
      </div>
    );
  }

  return (
    <>
      <SimpleBar>
        <div className="p-24 max-width-lg mx-auto">
          <Header
            setCurrentPage={setCurrentPage}
            setCurrentPageSize={setCurrentPageSize}
            pageSize={pageSize}
          />
          <div className="mt-16">
            <Grid
              columns={CLIENT_CONFIG_LIST_COLUMNS}
              data={gridData}
              sortBy={[sortBy]}
              onSortChange={handleSortChange}
              dataTestId={`${testId}_grid`}
            />
            {showPagination && (
              <div className="pt-16">
                <Pagination
                  totalItem={total}
                  pageSize={pageSize}
                  pageNumber={currentPage}
                  pageSizeValue={currentPageSize}
                  compact
                  onChangePage={onPageChange}
                  onChangePageSize={onPageSizeChange}
                  dataTestId={`${testId}_pagination`}
                />
              </div>
            )}
          </div>
        </div>
      </SimpleBar>
    </>
  );
};

export default ClientConfigGrid;
