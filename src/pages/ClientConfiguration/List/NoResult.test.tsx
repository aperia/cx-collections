import { renderMockStoreId } from 'app/test-utils';
import React from 'react';
import NoResult from './NoResult';
import { screen } from '@testing-library/react';

describe('Test NoResult component', () => {
  it('render component without props', () => {
    renderMockStoreId(<NoResult />);
    expect(screen.getByText('txt_no_search_results_found')).toBeInTheDocument();
    expect(screen.getByText('txt_add_client_config')).toBeInTheDocument();
  });
});
