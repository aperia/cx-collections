import React from 'react';
import { Button } from 'app/_libraries/_dls/components';
import { batch, useDispatch } from 'react-redux';

// Actions
import { clientConfigurationActions } from '../_redux/reducers';

// Helper
import { isDefaultClientConfig } from '../helpers';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Const
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { AdditionalFieldsMapType, ConfigData } from '../types';
import { CLIENT_CONFIG_STATUS_KEY } from '../constants';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface ActionsProps extends DLSId {
  data: ConfigData;
}

const Actions: React.FC<ActionsProps> = ({ data, dataTestId }) => {
  //query check client config status with "isConfigActive" key
  const isConfigActive = data?.additionalFields?.find(
    (item: AdditionalFieldsMapType) => item.name === CLIENT_CONFIG_STATUS_KEY
  );

  const isActive = isConfigActive?.active === true;

  const dispatch = useDispatch();
  const { t } = useTranslation();

  const handleEdit = () => {
    batch(() => {
      dispatch(clientConfigurationActions.toggleEditModal());
      dispatch(
        clientConfigurationActions.updateSelectedConfig({
          data
        })
      );
    });
  };

  const handleChangeStatus = () => {
    batch(() => {
      dispatch(
        clientConfigurationActions.updateSelectedConfig({
          data
        })
      );
      dispatch(clientConfigurationActions.toggleStatusModal());
    });
  };

  const handleViewSetting = () => {
    batch(() => {
      dispatch(clientConfigurationActions.setConfigWarningMessage(''));
      dispatch(clientConfigurationActions.toggleSettingsModal());
      dispatch(
        clientConfigurationActions.updateSelectedConfig({
          data
        })
      );
    });
  };

  const isModified = !isDefaultClientConfig(data);

  return (
    <>
      {isModified && (
        <Button
          variant={isActive ? 'outline-danger' : 'outline-primary'}
          size="sm"
          onClick={handleChangeStatus}
          dataTestId={genAmtId(dataTestId, 'activate-deactivate-btn', '')}
        >
          {isActive
            ? t(I18N_COMMON_TEXT.DEACTIVATE)
            : t(I18N_COMMON_TEXT.ACTIVATE)}
        </Button>
      )}
      <Button
        variant="outline-primary"
        size="sm"
        onClick={handleEdit}
        dataTestId={genAmtId(dataTestId, 'editBtn', '')}
      >
        {t(I18N_COMMON_TEXT.EDIT)}
      </Button>
      <Button
        variant="outline-primary"
        size="sm"
        onClick={handleViewSetting}
        dataTestId={genAmtId(dataTestId, 'viewSettingBtn', '')}
      >
        {t(I18N_COMMON_TEXT.VIEW_SETTINGS)}
      </Button>
    </>
  );
};

export default Actions;
