import { Badge } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import React from 'react';
import { ACTIVE, CLIENT_CONFIG_STATUS_KEY, INACTIVE } from '../constants';
import { AdditionalFieldsMapType, ConfigData } from '../types';

export interface ActionsProps {
  data: ConfigData;
  dataTestId?: string;
}
const StatusColumn: React.FC<ActionsProps> = ({ data, dataTestId }) => {
  const { t } = useTranslation();
  const isConfigActive = data?.additionalFields?.find(
    (item: AdditionalFieldsMapType) => item.name === CLIENT_CONFIG_STATUS_KEY
  );

  const isActive = isConfigActive?.active;
  return (
    <Badge
      color={isActive ? 'green' : 'grey'}
      textTransformNone
      dataTestId={dataTestId}
    >
      {t(isActive ? ACTIVE : INACTIVE)}
    </Badge>
  );
};

export default StatusColumn;
