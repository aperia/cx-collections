import React from 'react';

//actions
import { codeToTextActions } from './_redux/reducers';

//selector
import { selectedGridError, selectedCodeToTextTypes } from './_redux/selectors';

//helpers
import { useListenChangingTabEvent } from '../hooks/useListenChangingTabEvent';
import { useListenClosingModal } from '../hooks/useListenClosingModal';

//component
import GridItem from './GridItem';
import { FailedApiReload } from 'app/components';

//redux
import { batch, useDispatch, useSelector } from 'react-redux';

// helper
import {
  dispatchControlTabChangeEvent,
  dispatchCloseModalEvent
} from '../helpers';

//types
import { CodeToTextType } from './types';

const GridList: React.FC = () => {
  const dispatch = useDispatch();

  const testId = 'clientConfig_codeToText_grid';

  const isError = useSelector(selectedGridError);

  const selectedCodeToTextGroup = useSelector(selectedCodeToTextTypes);

  const divRef = React.useRef<HTMLDivElement | null>(null);

  const handleReload = () => {
    dispatch(codeToTextActions.getCodeToTextConfig());
  };

  useListenChangingTabEvent(() => {
    batch(() => {
      dispatch(codeToTextActions.resetCodeToText());
      dispatchControlTabChangeEvent();
    });
  });

  useListenClosingModal(() => {
    batch(() => {
      dispatch(codeToTextActions.resetCodeToText());
      dispatchCloseModalEvent();
    });
  });

  if (isError)
    return (
      <div>
        <FailedApiReload
          id="client-config-code-to-text-failed"
          className="mt-80"
          onReload={handleReload}
          dataTestId={`${testId}_failedApi`}
        />
      </div>
    );

  return (
    <div ref={divRef}>
      {selectedCodeToTextGroup?.map(item => (
        <GridItem
          key={item}
          codeToTextType={item as CodeToTextType}
          dataTestId={`${item}_${testId}`}
        />
      ))}
    </div>
  );
};

export default GridList;
