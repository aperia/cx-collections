import React from 'react';

//mock test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';

//components
import CodeToTextDropDown from './CodeToTextDropDown';

//actions
import { codeToTextActions } from './_redux/reducers';

//constant
import userEvent from '@testing-library/user-event';

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockCodeToTextActions = mockActionCreator(codeToTextActions);

describe('render actions buttons ', () => {
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<CodeToTextDropDown />, { initialState });
  };

  it('should render dropdown and action with data', () => {
    mockCodeToTextActions('getCodeToTextDropdownList');
    const blurAction = mockCodeToTextActions('setSelectedCodeToTextTypes');
    const changeAction = mockCodeToTextActions('selectedDropdown');

    const initialState: Partial<RootState> = {
      codeToText: {
        dropdownList: [
          { code: 'DR', description: 'DELINQUENCY REASON' },
          { code: 'AES', description: 'ACCOUNT EXTERNAL STATUS' },
          { code: 'CS', description: 'CARD STATUS' }
        ],
        loading: false,
        modal: {
          actionType: '',
          open: false,
          selectedItem: undefined,
          codeToTextType: ''
        },
        selectedDropdown: [
          { code: 'DR', description: 'DELINQUENCY REASON' },
          { code: 'AES', description: 'ACCOUNT EXTERNAL STATUS' },
          { code: 'CS', description: 'CARD STATUS' }
        ],
        selectedCodeToTextTypes: ['accountExternalStatus', 'delinquencyReason', 'cardStatus'],
        data: {}
      }
    };

    jest.useFakeTimers();
    const { container } = renderWrapper(initialState);

    const multiSelect = container.querySelector('.text-field-container')!;

    userEvent.click(multiSelect);
    userEvent.click(screen.getByText('ACCOUNT EXTERNAL STATUS'));
    userEvent.click(screen.getByText('DELINQUENCY REASON'));
    jest.runAllTimers();

    userEvent.click(multiSelect);
    jest.runAllTimers();

    expect(blurAction).toBeCalled();
    expect(changeAction).toBeCalled();
  });
});
