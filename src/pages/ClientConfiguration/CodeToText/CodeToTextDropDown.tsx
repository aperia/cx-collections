import React, { useEffect } from 'react';

//components
import MultiSelect from 'app/_libraries/_dls/components/MultiSelect'; //   MultiSelectProps

//utils
import isEmpty from 'lodash.isempty';

//redux
import { batch, useDispatch, useSelector } from 'react-redux';

//actions
import { codeToTextActions } from './_redux/reducers';

//selector
import { selectedDropdownList, selectedDropDown } from './_redux/selectors';

//types
import { DropdownBaseChangeEvent } from 'app/_libraries/_dls/components';
import { CodeToTextItems } from './types';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { I18N_REFERENCE } from 'app/constants/i18n';
import { getGroupFormatInputText } from 'app/helpers/formatMultiSelectText';

const CodeToTextDropDown: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const testId = 'clientConfig_codeToText_dropdown';

  const data = useSelector(selectedDropdownList).map(item => ({
    ...item,
    description: t(item.description)
  }));
  const selectedList = useSelector(selectedDropDown);

  const selectedValue = selectedList?.map(item =>
    data.find(list => list.code === item.code)
  );

  const dropdownListItem: any = React.useMemo(() => {
    return (
      !isEmpty(data) &&
      data?.map((item: CodeToTextItems) => (
        <MultiSelect.Item
          key={item.code}
          label={item.description}
          value={item}
          variant="checkbox"
        />
      ))
    );
  }, [data]);

  const handleOnBlur = (event: any) => {
    const items = event?.value?.map((item: CodeToTextItems) => item?.code);
    const listCode: string[] = Array.from(new Set(items));
    dispatch(codeToTextActions.setSelectedCodeToTextTypes(listCode));
  };

  const handleChange = (event: DropdownBaseChangeEvent) => {
    const listCode = event?.value?.map((item: CodeToTextItems) => item.code);
    dispatch(
      codeToTextActions.selectedDropdown({ data: event.target.value, listCode })
    );
  };

  useEffect(() => {
    batch(() => {
      dispatch(codeToTextActions.getCodeToTextDropdownList({}));
      dispatch(codeToTextActions.getCodeToTextConfig());
    });
  }, [dispatch]);

  return (
    <div className="w-320px mt-md-16 mt-lg-0">
      <MultiSelect
        name="codeToText"
        onBlur={handleOnBlur}
        value={selectedValue}
        textField="description"
        onChange={handleChange}
        label={t(I18N_REFERENCE.FIELDS)}
        variant="group"
        checkAll
        checkAllLabel={t('txt_all')}
        searchBar
        dataTestId={testId}
        noResult={t('txt_no_results_found')}
        searchBarPlaceholder={t('txt_type_to_search')}
        groupFormatInputText={(...args) =>
          t(getGroupFormatInputText(...args), {
            selected: args[0],
            items: args[1]
          })
        }
      >
        {dropdownListItem}
      </MultiSelect>
    </div>
  );
};

export default CodeToTextDropDown;
