export const CODE_TO_TEXT_DROPDOWN_ITEM_NAME = {
  ACCOUNT_EXTERNAL_STATUS_CONFIGURATION:
    'txt_account_external_status_configuration',
  CARD_STATUS_CONFIGURATION: 'txt_card_status_configuration',
  DELINQUENCY_REASON_CONFIGURATION: 'txt_delinquency_reason_configuration',
  LETTER_CODE_CONFIGURATION: 'txt_letter_code_configuration',
  TRANSACTION_CODE_CONFIGURATION: 'txt_transaction_code_configuration',
  ACCOUNT_INTERNAL_STATUS_CONFIGURATION:
    'txt_account_internal_status_configuration'
};
