import React, { useCallback, useRef } from 'react';

//components
import {
  Button,
  Modal,
  InlineMessage,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

//view
import { View, ExtraFieldProps } from 'app/_libraries/_dof/core';

//libs
import { isEmpty } from 'app/_libraries/_dls/lodash';

//forms
import { Field } from 'redux-form';

//redux
import { useSelector, useDispatch, batch } from 'react-redux';

//types
import { CodeToTextModalType, CodeToTextRefData } from './types';

//actions
import { codeToTextActions } from './_redux/reducers';

//hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSelectFormValue } from 'app/hooks/useSelectFormValue';
import { isValid } from 'redux-form';

//selector
import {
  selectedModalErrorMsg,
  selectedModalLoading
} from './_redux/selectors';

//constants
import { I18N_CODE_TO_TEXT } from 'app/constants/i18n';
import { useIsDirtyForm } from 'app/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import ModalBodyWithErrorClientConfig from '../ApiErrorSection/ModalBodyWithErrorClientConfig';
import { codeToTextName } from './helper';

const DOF_CODE_TO_TEXT_DESCRIPTOR = 'CodeToTextClientConfigView';

const CodeToTextModal: React.FC<CodeToTextModalType> = ({
  actionType,
  codeToTextType,
  selectedItem
}) => {
  const DOF_CODE_TO_TEXT_NAME = `${DOF_CODE_TO_TEXT_DESCRIPTOR}_${codeToTextType}`;
  const testId = 'clientConfig_codeToText_modal';

  const dispatch = useDispatch();

  const [initialData] = React.useState({
    code: selectedItem?.code || '',
    description: selectedItem?.description || ''
  });

  const modalViewRef = useRef<any>();

  const formValid = useSelector(isValid(DOF_CODE_TO_TEXT_NAME));

  const errorMsg = useSelector(selectedModalErrorMsg);

  const loading = useSelector(selectedModalLoading);

  const values = useSelectFormValue(DOF_CODE_TO_TEXT_NAME);

  const isNotChangeData = useIsDirtyForm([DOF_CODE_TO_TEXT_NAME]);

  const isDisabledButtonSubmit = actionType === 'edit' && !isNotChangeData;

  const { t } = useTranslation();

  const handleSubmit = useCallback(() => {
    batch(() => {
      dispatch(
        codeToTextActions.updateCodeToTextConfig({
          data:
            actionType === 'delete'
              ? selectedItem!
              : (values as CodeToTextRefData),
          codeToTextType,
          actionType,
          label: t(codeToTextName(codeToTextType))
        })
      );

      dispatch(codeToTextActions.setErrorMessage(''));
    });
  }, [dispatch, actionType, selectedItem, values, codeToTextType, t]);

  const handleCloseModal = () => {
    batch(() => {
      dispatch(
        codeToTextActions.toggleModal({
          actionType: '',
          selectedItem: undefined,
          codeToTextType: ''
        })
      );
      dispatch(codeToTextActions.setErrorMessage(''));
    });
  };

  const setViewProps = () => {
    const codeToTextFields: Field<ExtraFieldProps> =
      modalViewRef?.current?.props?.onFind('code');

    if (actionType === 'add') {
      codeToTextFields?.props?.props?.setRequired(true);
      codeToTextFields?.props?.props?.setReadOnly(false);
    }

    if (errorMsg && !isEmpty(values?.code)) {
      codeToTextFields?.props?.props?.setOthers((prev: MagicKeyValue) => {
        codeToTextFields.props?.props?.setOthers({
          ...prev,
          options: { ...prev.options, isError: true }
        });
      });
    }
  };

  React.useEffect(() => {
    if (modalViewRef.current) {
      return setViewProps();
    }
  });

  const dynamicProps: MagicKeyValue = {
    add: {
      title: t(I18N_CODE_TO_TEXT.ADD_CODE),
      variant: 'primary',
      footer: t(I18N_CODE_TO_TEXT.SUBMIT)
    },
    edit: {
      title: t(I18N_CODE_TO_TEXT.EDIT_CODE),
      variant: 'primary',
      footer: t(I18N_CODE_TO_TEXT.SAVE)
    },
    delete: {
      title: t(I18N_CODE_TO_TEXT.DELETE_CODE),
      footer: t(I18N_CODE_TO_TEXT.DELETE),
      variant: 'danger',
      content: t(I18N_CODE_TO_TEXT.DELETE_MODAL_CONTENT)
    }
  };

  const isDeleteContent = actionType === 'delete';

  return (
    <>
      <Modal
        xs={isDeleteContent}
        sm={!isDeleteContent}
        show
        loading={loading}
        dataTestId={testId}
      >
        <ModalHeader
          border
          closeButton
          onHide={handleCloseModal}
          dataTestId={`${testId}_header`}
        >
          <ModalTitle dataTestId={`${testId}_title`}>
            {dynamicProps[actionType].title}
          </ModalTitle>
        </ModalHeader>
        <ModalBodyWithErrorClientConfig
          apiErrorClassName="mb-24"
          dataTestId={`${testId}_body`}
        >
          {errorMsg && (
            <InlineMessage
              className="mb-24"
              variant="danger"
              dataTestId={`${testId}_errorMessage`}
            >
              {t(errorMsg)}
            </InlineMessage>
          )}
          {isDeleteContent ? (
            <>
              <div
                className="mb-6"
                data-testid={genAmtId(testId, 'description', '')}
              >
                {dynamicProps[actionType].content}
              </div>
              <div data-testid={genAmtId(testId, 'code', '')}>
                {t(I18N_CODE_TO_TEXT.CODE)}:{' '}
                <strong>{selectedItem?.code}</strong> -{' '}
                <strong>{selectedItem?.description}</strong>
              </div>
            </>
          ) : (
            <View
              id={DOF_CODE_TO_TEXT_NAME}
              formKey={DOF_CODE_TO_TEXT_NAME}
              descriptor={DOF_CODE_TO_TEXT_DESCRIPTOR}
              value={initialData}
              ref={modalViewRef}
            />
          )}
        </ModalBodyWithErrorClientConfig>
        <ModalFooter dataTestId={`${testId}_footer`}>
          <div className="d-flex align-items-right justify-content-end">
            <Button
              onClick={handleCloseModal}
              variant="secondary"
              dataTestId={`${testId}_cancelBtn`}
            >
              {t(I18N_CODE_TO_TEXT.CANCEL)}
            </Button>
            <Button
              disabled={!formValid || isDisabledButtonSubmit}
              onClick={handleSubmit}
              variant={dynamicProps[actionType].variant}
              dataTestId={`${testId}_okBtn`}
            >
              {dynamicProps[actionType].footer}
            </Button>
          </div>
        </ModalFooter>
      </Modal>
    </>
  );
};

export default CodeToTextModal;
