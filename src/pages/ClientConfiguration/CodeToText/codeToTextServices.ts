import { apiService } from 'app/utils/api.service';

//types
import { CodeToTextRefData } from './types';

export const codeToTextServices = {
  getCodeToTextDropdownList() {
    return apiService.get<CodeToTextRefData[]>('refData/codeToText.json');
  }
};
