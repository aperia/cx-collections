import React from 'react';

//mock test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';

//components
import CodeToText from '.';

//actions
import { codeToTextActions } from './_redux/reducers';

import { I18N_CODE_TO_TEXT } from 'app/constants/i18n';

import userEvent from '@testing-library/user-event';

//helpers
import * as clientConfigHelper from 'pages/ClientConfiguration/helpers';

//constant
import { SECTION_TAB_EVENT } from '../constants';
import { CODE_TO_TEXT_DROPDOWN_ITEM_NAME } from './constants';

jest.mock('./CodeToTextModal', () => () => <div />);

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockCodeToTextActions = mockActionCreator(codeToTextActions);

const initialState: Partial<RootState> = {
  codeToText: {
    dropdownList: [{ code: 'AES', description: 'ACCOUNT EXTERNAL STATUS' }],
    loading: false,
    modal: {
      actionType: '',
      open: true,
      selectedItem: undefined,
      codeToTextType: ''
    },
    selectedDropdown: [{ code: 'AES', description: 'ACCOUNT EXTERNAL STATUS' }],
    selectedCodeToTextTypes: ['accountExternalStatus']
  }
};

const selectedDropdownListState: Partial<RootState> = {
  codeToText: {
    dropdownList: [
      { code: 'DR', description: 'DELINQUENCY REASON' },
      { code: 'AES', description: 'ACCOUNT EXTERNAL STATUS' },
      { code: 'CS', description: 'CARD STATUS' }
    ],
    loading: false,
    modal: {
      actionType: '',
      open: false,
      selectedItem: undefined,
      codeToTextType: ''
    },
    selectedDropdown: [
      { code: 'DR', description: 'DELINQUENCY REASON' },
      { code: 'AES', description: 'ACCOUNT EXTERNAL STATUS' }
    ],
    selectedCodeToTextTypes: ['transactionCode', 'accountExternalStatus']
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(<CodeToText />, { initialState });
};
describe('render actions buttons ', () => {
  it('should render list grid with data ', () => {
    renderWrapper(initialState);

    const title = screen.getByText(
      CODE_TO_TEXT_DROPDOWN_ITEM_NAME.ACCOUNT_EXTERNAL_STATUS_CONFIGURATION
    );
    const description = screen.getByText(
      I18N_CODE_TO_TEXT.CODE_TO_TEXT_DESCRIPTION
    );
    const name = screen.getByText(I18N_CODE_TO_TEXT.CODE_TO_TEXT);

    expect(title).toBeInTheDocument();
    expect(description).toBeInTheDocument();
    expect(name).toBeInTheDocument();
  });

  it('should unmount ', () => {
    const { wrapper } = renderWrapper(initialState);
    const resetCodeToText = mockCodeToTextActions('resetCodeToText');
    wrapper.unmount();
    expect(resetCodeToText).toBeCalled();
  });

  it('should render list grid with data and trigger modal ', () => {
    const mockToggleModal = mockCodeToTextActions('toggleModal');

    const mockSetErrorMessage = mockCodeToTextActions('setErrorMessage');

    renderWrapper(initialState);

    const toggleModal = screen.getByText('txt_add_code');
    userEvent.click(toggleModal);

    expect(mockToggleModal).not.toBeCalledWith({
      actionType: 'add',
      selectedItem: undefined,
      codeToTextType: 'accountExternalStatus'
    });

    expect(mockSetErrorMessage).not.toBeCalledWith('');
  });
});

describe('useListenChangingTabEvent', () => {
  let spy: jest.SpyInstance;

  afterEach(() => {
    spy.mockReset();
    spy.mockRestore();
  });

  const mockDispatchEventChangeTab = jest.fn();

  it('should trigger useListenChangingTabEvent', () => {
    const mockResetCodeToTextActions = mockCodeToTextActions('resetCodeToText');

    spy = jest
      .spyOn(clientConfigHelper, 'dispatchControlTabChangeEvent')
      .mockImplementation(mockDispatchEventChangeTab);

    renderWrapper(selectedDropdownListState);

    const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGING_TAB, {
      bubbles: true,
      cancelable: true
    });

    window.dispatchEvent(event);

    expect(mockDispatchEventChangeTab).toBeCalled();
    expect(mockResetCodeToTextActions).toBeCalledWith();
  });
});

describe('useListenClosingModal', () => {
  let spy: jest.SpyInstance;

  afterEach(() => {
    spy.mockReset();
    spy.mockRestore();
  });

  const mockDispatchCloseModalEvent = jest.fn();
  it('should trigger useListenClosingModal', () => {
    const mockResetCodeToTextActions = mockCodeToTextActions('resetCodeToText');

    spy = jest
      .spyOn(clientConfigHelper, 'dispatchCloseModalEvent')
      .mockImplementation(mockDispatchCloseModalEvent);

    renderWrapper(selectedDropdownListState);

    const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
      bubbles: true,
      cancelable: true
    });

    window.dispatchEvent(event);

    expect(mockDispatchCloseModalEvent).toBeCalled();
    expect(mockResetCodeToTextActions).toBeCalledWith();
  });
});
