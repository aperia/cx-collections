import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { sortBy } from 'app/_libraries/_dls/lodash';

//services
import { codeToTextServices } from '../codeToTextServices';

//types
import { CodeToTextRefData, CodeToTextState } from '../types';

export interface GetCodeToTextDropDownListPayload {
  data?: CodeToTextRefData[];
}

export interface GetCodeToTextArgs {}

export const getCodeToTextDropdownList = createAsyncThunk<
  GetCodeToTextDropDownListPayload,
  GetCodeToTextArgs,
  ThunkAPIConfig
>(
  'clientConfigCodeToText/getCodeToTextDropdownList',
  async (args, thunkAPI) => {
    const { data } = await codeToTextServices.getCodeToTextDropdownList();

    const mappingData = {
      code: 'value',
      description: 'description'
    };
    const mappedData = mappingDataFromArray<CodeToTextRefData>(
      data,
      mappingData
    );

    const ascendingSortData = sortBy(mappedData, ['description']);

    return { data: ascendingSortData };
  }
);

export const getCodeToTextDropdownListBuilder = (
  builder: ActionReducerMapBuilder<CodeToTextState>
) => {
  builder.addCase(getCodeToTextDropdownList.pending, (draftState, action) => {
    draftState.loading = true;
  });
  builder.addCase(getCodeToTextDropdownList.fulfilled, (draftState, action) => {
    draftState.loading = false;
    draftState.dropdownList = action.payload?.data;
  });
  builder.addCase(getCodeToTextDropdownList.rejected, (draftState, action) => {
    draftState.loading = false;
  });
};
