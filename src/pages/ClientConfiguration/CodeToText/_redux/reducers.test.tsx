import { codeToTextActions as actions, codeToText as reducer } from './reducers';

import { CodeToTextState } from '../types';

const initialState = {} as CodeToTextState;

describe('CodeToText Reducers', () => {
  it('selectedDropdown', () => {
    const state = reducer(initialState, actions.selectedDropdown({ data: [] }));
    expect(state.selectedDropdown).toEqual([]);
  });

  it('setErrorMessage', () => {
    const state = reducer(initialState, actions.setErrorMessage('error'));
    expect(state.modal.error).toEqual('error');
  });

  it('resetCodeToText', () => {
    const state = reducer(initialState, actions.resetCodeToText());

    expect(state.selectedDropdown).toEqual([]);
    expect(state.selectedCodeToTextTypes).toEqual([]);
    expect(state.data).toEqual({});
  });

  it('toggleModal', () => {
    const state = reducer(initialState, actions.toggleModal({
      actionType: 'add',
      codeToTextType: 'accountExternalStatus',
      selectedItem: { code: 'code', description: 'description' }
    }));

    expect(state.modal.actionType).toEqual('add');
    expect(state.modal.open).toEqual(true);
    expect(state.modal.selectedItem).toEqual({ code: 'code', description: 'description' });
    expect(state.modal.codeToTextType).toEqual('accountExternalStatus');
  });

  it('setSelectedCodeToTextTypes', () => {
    const state = reducer(
      initialState,
      actions.setSelectedCodeToTextTypes(['accountExternalStatus'])
    );
    expect(state.selectedCodeToTextTypes).toEqual(['accountExternalStatus']);
  });

});
