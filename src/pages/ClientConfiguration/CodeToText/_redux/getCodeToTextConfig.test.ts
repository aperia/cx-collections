import { byPassRejected } from 'app/test-utils';
import { createStore, Store, isRejected } from '@reduxjs/toolkit';
import { mockActionCreator } from 'app/test-utils';

import { rootReducer } from 'storeConfig';
import { CodeToTextState } from '../types';

import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { getCodeToTextConfig } from './getCodeToTextConfig';
import { codeToText as reducer } from './reducers';

const spyClientConfigActions = mockActionCreator(clientConfigurationActions);
describe('CodeToText > redux getCodeToTextConfig', () => {
  let store: Store<RootState>;
  const initialState = {} as CodeToTextState;

  const mockData = { A: [{ code: 'a', description: 'b' }] };

  beforeEach(() => {
    store = createStore(rootReducer, {
      codeToText: initialState
    });
  });

  it('pending', () => {
    const nextState = reducer(
      initialState,
      getCodeToTextConfig.pending('getCodeToTextConfig', undefined)
    );
    expect(nextState.loading).toEqual(true);
    expect(nextState.error).toEqual(false);
  });

  it('fulfilled', () => {
    const nextState = reducer(
      initialState,
      getCodeToTextConfig.fulfilled(
        { data: mockData },
        'getCodeToTextConfig',
        undefined
      )
    );
    expect(nextState.loading).toEqual(false);
    expect(nextState.data).toEqual(mockData);
  });

  it('rejected', async () => {
    const response = await getCodeToTextConfig()(
      byPassRejected(getCodeToTextConfig.rejected.type),
      store.getState,
      {}
    );

    expect(isRejected(response)).toBeTruthy();
  });

  it('should return data', async () => {
    const response = await getCodeToTextConfig()(
      jest.fn().mockResolvedValue({
        type: clientConfigurationActions.getCollectionsClientConfig.fulfilled
          .type,
        payload: mockData,
        meta: {
          requestId: 'requestId',
          requestStatus: 'fulfilled'
        }
      }),
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      data: mockData
    });
  });

  it('should fail to getCollectionsClientConfig', async () => {
    spyClientConfigActions('getCollectionsClientConfig').mockRejectedValue({});

    const response = await getCodeToTextConfig()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(getCodeToTextConfig.rejected.type);
  });
});
