import { createStore, Store } from '@reduxjs/toolkit';

import { getCodeToTextDropdownList } from './getCodeToTextDropdownList';

import { codeToTextServices } from '../codeToTextServices';

import { responseDefault } from 'app/test-utils';

import { rootReducer } from 'storeConfig';

const mockArgs = {};

let store: Store<RootState>;
let state: RootState;

beforeEach(() => {
  store = createStore(rootReducer, {});
  state = store.getState();
});

let spy1: jest.SpyInstance;

afterEach(() => {
  spy1?.mockReset();
  spy1?.mockRestore();
});

describe('should have test getAccountStatusCodeList', () => {
  it('Should return data', async () => {
    spy1 = jest
      .spyOn(codeToTextServices, 'getCodeToTextDropdownList')
      .mockResolvedValue({
        ...responseDefault,
        data: [
          { value: 'DR', description: 'DELINQUENCY REASON' },
          { value: 'AES', description: 'ACCOUNT EXTERNAL STATUS' },
          { value: 'CS', description: 'CARD STATUS' }
        ] as any
      });

    const response = await getCodeToTextDropdownList(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      data: [
        { code: 'AES', description: 'ACCOUNT EXTERNAL STATUS' },
        { code: 'CS', description: 'CARD STATUS' },
        { code: 'DR', description: 'DELINQUENCY REASON' },
      ]
    });
  });

  it('should response getCodeToTextDropdownList pending', () => {
    const pendingAction = getCodeToTextDropdownList.pending(
      getCodeToTextDropdownList.pending.type,
      mockArgs
    );
    const actual = rootReducer(state, pendingAction);

    expect(actual?.codeToText?.loading).toEqual(true);
  });

  it('should response getCodeToTextDropdownList fulfilled', () => {
    const fulfilled = getCodeToTextDropdownList.fulfilled(
      {},
      getCodeToTextDropdownList.fulfilled.type,
      mockArgs
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual?.codeToText?.loading).toEqual(false);
    expect(actual?.codeToText?.dropdownList).toEqual(undefined);
  });

  it('should response getCodeToTextDropdownList rejected', () => {
    const rejected = getCodeToTextDropdownList.rejected(
      null,
      getCodeToTextDropdownList.rejected.type,
      mockArgs
    );
    const actual = rootReducer(state, rejected);

    expect(actual?.codeToText?.loading).toEqual(false);
  });
});
