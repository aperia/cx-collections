import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// async thunks
import {
  getCodeToTextDropdownList,
  getCodeToTextDropdownListBuilder
} from './getCodeToTextDropdownList';

import {
  getCodeToTextConfig,
  getCodeToTextConfigBuilder
} from './getCodeToTextConfig';

import {
  updateCodeToTextConfig,
  updateCodeToTextConfigBuilder
} from './updateCodeToTextConfig';

// types
import { CodeToTextModal, CodeToTextState } from '../types';

const { actions, reducer } = createSlice({
  name: 'codeToText',
  initialState: {} as CodeToTextState,
  reducers: {
    selectedDropdown: (draftState, action: PayloadAction<MagicKeyValue>) => {
      const { data } = action.payload;
      draftState.selectedDropdown = data;
    },
    setErrorMessage: (draftState, action: PayloadAction<string>) => {
      draftState.modal = {
        ...draftState.modal,
        error: action.payload
      };
    },
    resetCodeToText: draftState => {
      draftState.selectedDropdown = [];
      draftState.selectedCodeToTextTypes = [];
      draftState.data = {};
    },
    toggleModal: (draftState, action: PayloadAction<CodeToTextModal>) => {
      draftState.modal = {
        ...draftState.modal,
        actionType: action.payload?.actionType,
        open: !draftState?.modal?.open,
        selectedItem: action.payload?.selectedItem,
        codeToTextType: action.payload?.codeToTextType
      };
    },
    setSelectedCodeToTextTypes: (
      draftState,
      action: PayloadAction<string[]>
    ) => {
      draftState.selectedCodeToTextTypes = action.payload;
    }
  },
  extraReducers: builder => {
    getCodeToTextDropdownListBuilder(builder);
    getCodeToTextConfigBuilder(builder);
    updateCodeToTextConfigBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getCodeToTextDropdownList,
  getCodeToTextConfig,
  updateCodeToTextConfig
};

export { allActions as codeToTextActions, reducer as codeToText };
