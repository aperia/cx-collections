import { createStore, Store } from '@reduxjs/toolkit';
import { mockActionCreator } from 'app/test-utils';

import { rootReducer } from 'storeConfig';
import { CodeToTextState } from '../types';

import {
  codeToTextName,
  dynamicMsg,
  getFulfilledMessage,
  getRejectedMessage
} from '../helper';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { updateCodeToTextConfig } from './updateCodeToTextConfig';
import { codeToText as reducer, codeToTextActions } from './reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { I18N_CLIENT_CONFIG } from 'app/constants/i18n';

const spyToastActions = mockActionCreator(actionsToast);
const spyCodeToTextActions = mockActionCreator(codeToTextActions);
const spyClientConfigActions = mockActionCreator(clientConfigurationActions);

describe('CodeToText > redux getCodeToTextConfig', () => {
  let store: Store<RootState>;
  const initialState = {
    modal: {
      selectedItem: {}
    }
  } as CodeToTextState;
  const mockData = {
    accountExternalStatus: [{ code: 'a', description: 'aa' }],
    transactionCode: [{ code: 'b', description: 'bb' }]
  };

  beforeEach(() => {
    store = createStore(rootReducer, {
      codeToText: initialState
    } as any);
  });

  it('pending', () => {
    const nextState = reducer(
      initialState,
      updateCodeToTextConfig.pending('updateCodeToTextConfig', undefined as any)
    );
    expect(nextState.modal.loading).toEqual(true);
  });

  it('fulfilled', () => {
    const nextState = reducer(
      initialState,
      updateCodeToTextConfig.fulfilled(
        undefined as any,
        'updateCodeToTextConfig',
        undefined as any
      )
    );
    expect(nextState.modal.loading).toEqual(false);
  });

  it('rejected', () => {
    const nextState = reducer(
      initialState,
      updateCodeToTextConfig.rejected(
        null,
        'updateCodeToTextConfig',
        undefined as any
      )
    );
    expect(nextState.modal.loading).toEqual(false);
  });

  it('should add fail', async () => {
    spyClientConfigActions('getCollectionsClientConfig').mockReturnValue({
      type: 'clientConfiguration/getCollectionsClientConfig/rejected',
      meta: { requestId: 'requestId', requestStatus: 'rejected' }
    });
    const response = await updateCodeToTextConfig({
      data: {} as any,
      actionType: 'add',
      codeToTextType: 'accountExternalStatus',
      label: 'Txt_account_external_status_'
    })(
      (arg: any) => {
        if (arg.type) return Promise.resolve(arg);
      },
      store.getState,
      {}
    );

    expect(response.type).toEqual(updateCodeToTextConfig.rejected.type);
  });

  it('should add fail when duplicated', async () => {
    const setErrorMessage = spyCodeToTextActions('setErrorMessage');
    spyClientConfigActions('getCollectionsClientConfig').mockReturnValue({
      type: 'clientConfiguration/getCollectionsClientConfig/fulfilled',
      payload: mockData,
      meta: { requestId: 'requestId', requestStatus: 'fulfilled' }
    });

    const response = await updateCodeToTextConfig({
      data: { code: 'a', description: 'cc' },
      actionType: 'add',
      codeToTextType: 'accountExternalStatus',
      label: 'Txt_account_external_status_'
    })(
      (arg: any) => {
        if (arg.type) return Promise.resolve(arg);
      },
      store.getState,
      {}
    );

    expect(setErrorMessage).toHaveBeenCalledWith(
      I18N_CLIENT_CONFIG.CODE_ALREADY_EXISTS
    );
    expect(response.type).toEqual(updateCodeToTextConfig.rejected.type);
  });

  it('should add success', async () => {
    const toggleModal = spyCodeToTextActions('toggleModal');
    const addToast = spyToastActions('addToast');
    spyClientConfigActions('getCollectionsClientConfig').mockReturnValue({
      type: 'clientConfiguration/getCollectionsClientConfig/fulfilled',
      payload: mockData,
      meta: { requestId: 'requestId', requestStatus: 'fulfilled' }
    });
    spyClientConfigActions('saveCollectionsClientConfig').mockReturnValue({
      type: 'clientConfiguration/saveCollectionsClientConfig/fulfilled',
      meta: { requestId: 'requestId', requestStatus: 'fulfilled' }
    });

    const response = await updateCodeToTextConfig({
      data: { code: 'c', description: 'cc' },
      actionType: 'add',
      codeToTextType: 'accountExternalStatus',
      label: 'Txt_account_external_status_'
    })(
      (arg: any) => {
        if (arg.type) return Promise.resolve(arg);
      },
      store.getState,
      {}
    );

    expect(addToast).toHaveBeenCalledWith({
      show: true,
      type: 'success',
      message: getFulfilledMessage('add'),
      msgVariables: {
        name: `${dynamicMsg(codeToTextName('accountExternalStatus'))}`
      }
    });
    expect(toggleModal).toHaveBeenCalled();
    expect(response.type).toEqual(updateCodeToTextConfig.fulfilled.type);
  });

  it('should edit fail', async () => {
    const addToast = spyToastActions('addToast');
    spyClientConfigActions('getCollectionsClientConfig').mockReturnValue({
      type: 'clientConfiguration/getCollectionsClientConfig/fulfilled',
      payload: mockData,
      meta: { requestId: 'requestId', requestStatus: 'fulfilled' }
    });
    spyClientConfigActions('saveCollectionsClientConfig').mockReturnValue({
      type: 'clientConfiguration/saveCollectionsClientConfig/rejected',
      meta: { requestId: 'requestId', requestStatus: 'rejected' }
    });
    const response = await updateCodeToTextConfig({
      data: {} as any,
      actionType: 'edit',
      codeToTextType: 'accountExternalStatus',
      label: 'Txt_account_external_status_'
    })(
      (arg: any) => {
        if (arg.type) return Promise.resolve(arg);
      },
      store.getState,
      {}
    );

    expect(addToast).toHaveBeenCalledWith({
      show: true,
      type: 'error',
      message: getRejectedMessage('edit'),
      msgVariables: {
        name: `${dynamicMsg(codeToTextName('accountExternalStatus'))}`
      }
    });
    expect(response.type).toEqual(updateCodeToTextConfig.rejected.type);
  });

  it('should edit success', async () => {
    const toggleModal = spyCodeToTextActions('toggleModal');
    const addToast = spyToastActions('addToast');
    spyClientConfigActions('getCollectionsClientConfig').mockReturnValue({
      type: 'clientConfiguration/getCollectionsClientConfig/fulfilled',
      payload: mockData,
      meta: { requestId: 'requestId', requestStatus: 'fulfilled' }
    });
    spyClientConfigActions('saveCollectionsClientConfig').mockReturnValue({
      type: 'clientConfiguration/saveCollectionsClientConfig/fulfilled',
      meta: { requestId: 'requestId', requestStatus: 'fulfilled' }
    });

    const response = await updateCodeToTextConfig({
      data: { code: 'a', description: 'cc' },
      actionType: 'edit',
      codeToTextType: 'accountExternalStatus',
      label: 'Txt_account_external_status_'
    })(
      (arg: any) => {
        if (arg.type) return Promise.resolve(arg);
      },
      store.getState,
      {}
    );

    expect(addToast).toHaveBeenCalledWith({
      show: true,
      type: 'success',
      message: getFulfilledMessage('edit'),
      msgVariables: {
        name: `${dynamicMsg(codeToTextName('accountExternalStatus'))}`
      }
    });
    expect(toggleModal).toHaveBeenCalled();
    expect(response.type).toEqual(updateCodeToTextConfig.fulfilled.type);
  });

  it('should delete fail', async () => {
    spyClientConfigActions('getCollectionsClientConfig').mockReturnValue({
      type: 'clientConfiguration/getCollectionsClientConfig/fulfilled',
      payload: mockData,
      meta: { requestId: 'requestId', requestStatus: 'fulfilled' }
    });
    spyClientConfigActions('saveCollectionsClientConfig').mockReturnValue({
      type: 'clientConfiguration/saveCollectionsClientConfig/rejected'
    });
    const response = await updateCodeToTextConfig({
      data: {} as any,
      actionType: 'delete',
      codeToTextType: 'letterCode',
      label: 'Txt_account_external_status_'
    })(
      (arg: any) => {
        if (arg.type) return Promise.resolve(arg);
      },
      store.getState,
      {}
    );

    expect(response.type).toEqual(updateCodeToTextConfig.rejected.type);
  });

  it('should delete success', async () => {
    const toggleModal = spyCodeToTextActions('toggleModal');
    const addToast = spyToastActions('addToast');
    spyClientConfigActions('getCollectionsClientConfig').mockReturnValue({
      type: 'clientConfiguration/getCollectionsClientConfig/fulfilled',
      payload: mockData,
      meta: { requestId: 'requestId', requestStatus: 'fulfilled' }
    });
    spyClientConfigActions('saveCollectionsClientConfig').mockReturnValue({
      type: 'clientConfiguration/saveCollectionsClientConfig/fulfilled',
      meta: { requestId: 'requestId', requestStatus: 'fulfilled' }
    });

    const response = await updateCodeToTextConfig({
      data: { code: 'a', description: 'cc' },
      actionType: 'delete',
      codeToTextType: 'accountExternalStatus',
      label: 'Txt_account_external_status_'
    })(
      (arg: any) => {
        if (arg.type) return Promise.resolve(arg);
      },
      store.getState,
      {}
    );

    expect(addToast).toHaveBeenCalledWith({
      show: true,
      type: 'success',
      message: getFulfilledMessage('delete'),
      msgVariables: {
        name: `${dynamicMsg(codeToTextName('accountExternalStatus'))}`
      }
    });
    expect(toggleModal).toHaveBeenCalled();
    expect(response.type).toEqual(updateCodeToTextConfig.fulfilled.type);
  });

  it('should delete success > actionType invalid ', async () => {
    const toggleModal = spyCodeToTextActions('toggleModal');
    const addToast = spyToastActions('addToast');
    spyClientConfigActions('getCollectionsClientConfig').mockReturnValue({
      type: 'clientConfiguration/getCollectionsClientConfig/fulfilled',
      payload: mockData,
      meta: { requestId: 'requestId', requestStatus: 'fulfilled' }
    });
    spyClientConfigActions('saveCollectionsClientConfig').mockReturnValue({
      type: 'clientConfiguration/saveCollectionsClientConfig/fulfilled',
      meta: { requestId: 'requestId', requestStatus: 'fulfilled' }
    });

    const response = await updateCodeToTextConfig({
      data: { code: 'a', description: 'cc' },
      actionType: 'xxx' as never,
      codeToTextType: 'accountExternalStatus',
      label: 'Txt_account_external_status_'
    })(
      (arg: any) => {
        if (arg.type) return Promise.resolve(arg);
      },
      store.getState,
      {}
    );

    expect(addToast).toHaveBeenCalledWith({
      show: true,
      type: 'success',
      message: undefined,
      msgVariables: {
        name: `${dynamicMsg(codeToTextName('accountExternalStatus'))}`
      }
    });
    expect(toggleModal).toHaveBeenCalled();
    expect(response.type).toEqual(updateCodeToTextConfig.fulfilled.type);
  });
});
