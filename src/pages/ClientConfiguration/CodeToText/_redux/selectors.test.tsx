import { selectorWrapper } from 'app/test-utils';
import * as selectors from './selectors';

const store: Partial<RootState> = {
  codeToText: {
    dropdownList: [
      {
        description: 'description',
        code: 'code'
      }
    ],
    loading: false,
    modal: {
      actionType: '',
      open: false,
      selectedItem: {
        description: 'description',
        code: 'code'
      },
      codeToTextType: ''
    },
    selectedDropdown: [],
    selectedCodeToTextTypes: ['code'],
    data: {}
  }
};

describe('CodeToText Selectors', () => {
  it('selectedDropdownList', () => {
    const { data } = selectorWrapper(
      store,
      store
    )(selectors.selectedDropdownList);

    expect(data).toEqual(store.codeToText?.dropdownList);
  });

  it('selectedDropDown', () => {
    const { data } = selectorWrapper(store)(selectors.selectedDropDown);

    expect(data).toEqual(store.codeToText?.selectedDropdown);
  });

  it('selectedCodeToTextTypes', () => {
    const { data } = selectorWrapper(store)(selectors.selectedCodeToTextTypes);

    expect(data).toEqual(store.codeToText?.selectedCodeToTextTypes);
  });

  it('selectedGridList', () => {
    const { data } = selectorWrapper(store)(selectors.selectedGridList);

    expect(data).toEqual(store.codeToText?.data);
  });

  it('selectedGridLoading', () => {
    const { data } = selectorWrapper(store)(selectors.selectedGridLoading);

    expect(data).toEqual(store.codeToText?.loading);
  });

  it('selectedGridError', () => {
    const { data } = selectorWrapper(store)(selectors.selectedGridError);

    expect(data).toEqual(store.codeToText?.error);
  });

  it('selectedOpenModal', () => {
    const { data } = selectorWrapper(store)(selectors.selectedOpenModal);

    expect(data).toEqual(store.codeToText?.modal?.open);
  });

  it('selectedItemModal', () => {
    const { data } = selectorWrapper(store)(selectors.selectedItemModal);

    expect(data).toEqual(store.codeToText?.modal?.selectedItem);
  });

  it('selectedActionType', () => {
    const { data } = selectorWrapper(store)(selectors.selectedActionType);

    expect(data).toEqual(store.codeToText?.modal?.codeToTextType);
  });

  it('selectedCodeToTextType', () => {
    const { data } = selectorWrapper(store)(selectors.selectedCodeToTextType);

    expect(data).toEqual(store.codeToText?.modal?.codeToTextType);
  });

  it('selectedModalErrorMsg', () => {
    const { data } = selectorWrapper(store)(selectors.selectedModalErrorMsg);

    expect(data).toEqual('');
  });

  it('selectedModalLoading', () => {
    const { data } = selectorWrapper(store)(selectors.selectedModalLoading);

    expect(data).toEqual(store.codeToText?.modal?.loading);
  });

  it('selectGridDataByType', () => {
    const { data } = selectorWrapper(store)(selectors.selectGridDataByType);

    expect(data).toEqual([]);
  });
});
