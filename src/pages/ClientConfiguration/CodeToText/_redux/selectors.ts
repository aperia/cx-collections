import { createSelector } from '@reduxjs/toolkit';
import { EMPTY_ARRAY } from 'app/constants';
import { orderBy } from 'app/_libraries/_dls/lodash';
import { CodeToTextRefData, CodeToTextType } from '../types';

const getCodeToText = (states: RootState) => states.codeToText;

const getModal = (states: RootState) => states.codeToText?.modal;

const getSelectedDropdown = (states: RootState) =>
  states.codeToText?.selectedDropdown;

const getSelectedCodeToTextTypes = (states: RootState) =>
  states.codeToText?.selectedCodeToTextTypes;

export const selectedDropdownList = createSelector(getCodeToText, data => {
  return orderBy(
    data.dropdownList,
    [(item: CodeToTextRefData) => item['code']],
    'asc'
  );
});

export const selectedDropDown = createSelector(
  getSelectedDropdown,
  data => data
);

export const selectedCodeToTextTypes = createSelector(
  getSelectedCodeToTextTypes,
  data => orderBy(data, [item => item], 'asc')
);

export const selectedGridList = createSelector(
  getCodeToText,
  data => data?.data
);

export const selectedGridLoading = createSelector(
  getCodeToText,
  data => data?.loading
);

export const selectedGridError = createSelector(
  getCodeToText,
  data => data?.error
);

export const selectedOpenModal = createSelector(getModal, data => data?.open);

export const selectedItemModal = createSelector(
  getModal,
  data => data?.selectedItem
);

export const selectedActionType = createSelector(
  getModal,
  data => data?.actionType
);

export const selectedCodeToTextType = createSelector(
  getModal,
  data => data?.codeToTextType
);

export const selectedModalErrorMsg = createSelector(
  getModal,
  data => data?.error || ''
);

export const selectedModalLoading = createSelector(
  getModal,
  data => data?.loading
);

const getGridDataByType = (
  states: RootState,
  codeToTextType: CodeToTextType
) => {
  return states.codeToText?.data?.[codeToTextType];
};
export const selectGridDataByType = createSelector(
  getGridDataByType,
  gridData => gridData || EMPTY_ARRAY
);
