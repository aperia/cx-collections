import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

//types
import { CodeToTextRefData, CodeToTextState } from '../types';

export interface PostListDataPayload {
  data?: Record<string, CodeToTextRefData[]>;
}

export const getCodeToTextConfig = createAsyncThunk<
  PostListDataPayload,
  void,
  ThunkAPIConfig
>('clientConfigCodeToText/getCodeToTextConfig', async (args, thunkAPI) => {
  const { dispatch, rejectWithValue } = thunkAPI;

  const response = await dispatch(
    clientConfigurationActions.getCollectionsClientConfig({
      component: 'code_to_text_config'
    })
  );
  if (isFulfilled(response)) {
    const data = response.payload as Record<string, CodeToTextRefData[]>;
    return { data };
  }
  return rejectWithValue({});
});

export const getCodeToTextConfigBuilder = (
  builder: ActionReducerMapBuilder<CodeToTextState>
) => {
  builder.addCase(getCodeToTextConfig.pending, (draftState, action) => {
    draftState.loading = true;
    draftState.error = false;
  });
  builder.addCase(getCodeToTextConfig.fulfilled, (draftState, action) => {
    const { data } = action.payload;
    draftState.data = data;
    draftState.loading = false;
  });
  builder.addCase(getCodeToTextConfig.rejected, (draftState, action) => {
    draftState.loading = false;
    draftState.error = true;
  });
};
