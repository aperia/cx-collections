import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { I18N_CLIENT_CONFIG } from 'app/constants/i18n';
import { capitalizeWord } from 'app/helpers';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import findIndex from 'lodash.findindex';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import { dynamicMsg, getFulfilledMessage, getRejectedMessage } from '../helper';

//types
import {
  ActionType,
  CodeToTextRefData,
  CodeToTextState,
  CodeToTextType
} from '../types';
import { codeToTextActions } from './reducers';

export interface UpdateCodeToTextConfigArgs {
  data: CodeToTextRefData;
  codeToTextType: CodeToTextType;
  actionType: ActionType;
  label: string;
}

export const updateCodeToTextConfig = createAsyncThunk<
  undefined,
  UpdateCodeToTextConfigArgs,
  ThunkAPIConfig
>('clientConfigCodeToText/updateCodeToTextConfig', async (args, thunkAPI) => {
  const { dispatch, rejectWithValue, getState } = thunkAPI;
  const { data, actionType, codeToTextType, label } = args;

  const selectedItem = getState().codeToText.modal.selectedItem;

  const updateItem: CodeToTextRefData = {
    code: data?.code, // allow space code
    description: data?.description?.trim()
  };
  const getCodeToTextListResponse = await dispatch(
    clientConfigurationActions.getCollectionsClientConfig({
      component: 'code_to_text_config'
    })
  );
  if (isRejected(getCodeToTextListResponse)) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: getRejectedMessage(actionType),
        msgVariables: {
          name: `${dynamicMsg(label)}`
        }
      })
    );
    return thunkAPI.rejectWithValue({});
  }

  const allCodeToTextList = getCodeToTextListResponse.payload as Record<
    string,
    CodeToTextRefData[]
  >;

  const currentCodeToTextGroup = !isEmpty(allCodeToTextList?.[codeToTextType])
    ? [...allCodeToTextList[codeToTextType]]
    : [];

  switch (actionType) {
    case 'add':
      const isDuplicated = currentCodeToTextGroup.find(
        item => item.code.toLowerCase() === updateItem.code.toLowerCase()
      );
      if (isDuplicated) {
        dispatch(
          codeToTextActions.setErrorMessage(
            I18N_CLIENT_CONFIG.CODE_ALREADY_EXISTS
          )
        );
        return rejectWithValue({});
      }

      currentCodeToTextGroup.push(updateItem);
      break;
    case 'edit':
      const editIndex = findIndex(
        currentCodeToTextGroup,
        i => i.code === updateItem.code
      );
      currentCodeToTextGroup.splice(editIndex, 1, updateItem);
      break;
    case 'delete':
      const deleteIndex = findIndex(currentCodeToTextGroup, updateItem);
      currentCodeToTextGroup.splice(deleteIndex, 1);
      break;
  }

  const updateResponse = await dispatch(
    clientConfigurationActions.saveCollectionsClientConfig({
      component: 'code_to_text_config',
      jsonValue: {
        ...allCodeToTextList,
        [codeToTextType]: currentCodeToTextGroup
      }
    })
  );

  if (isFulfilled(updateResponse)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: getFulfilledMessage(actionType),
          msgVariables: {
            name: `${dynamicMsg(label)}`
          }
        })
      );

      dispatch(
        codeToTextActions.toggleModal({
          actionType,
          selectedItem: undefined,
          codeToTextType: ''
        })
      );

      dispatch(codeToTextActions.getCodeToTextConfig());

      if (actionType === 'add') {
        dispatch(
          changeHistoryActions.saveChangedClientConfig({
            action: 'ADD',
            changedCategory: 'codeToText',
            changedSection: {
              selectedOption: capitalizeWord(
                codeToTextType?.replace(/([A-Z])/g, ' $1').trim()
              )
            },
            changedItem: {
              code: updateItem?.code
            },
            newValue: {
              code: updateItem?.code,
              description: updateItem?.description
            }
          })
        );
        return;
      }
      if (actionType === 'edit') {
        dispatch(
          changeHistoryActions.saveChangedClientConfig({
            action: 'UPDATE',
            changedCategory: 'codeToText',
            changedSection: {
              selectedOption: capitalizeWord(
                codeToTextType?.replace(/([A-Z])/g, ' $1').trim()
              )
            },
            changedItem: {
              code: updateItem?.code
            },
            newValue: {
              code: updateItem?.code,
              description: updateItem?.description
            },
            oldValue: {
              code: selectedItem?.code,
              description: selectedItem?.description
            }
          })
        );
        return;
      }
      if (actionType === 'delete') {
        dispatch(
          changeHistoryActions.saveChangedClientConfig({
            action: 'DELETE',
            changedCategory: 'codeToText',
            changedSection: {
              selectedOption: capitalizeWord(
                codeToTextType?.replace(/([A-Z])/g, ' $1').trim()
              )
            },
            changedItem: {
              code: updateItem?.code
            },
            oldValue: {
              code: selectedItem?.code,
              description: selectedItem?.description
            }
          })
        );
        return;
      }
    });
    return;
  }
  if (isRejected(updateResponse)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: getRejectedMessage(actionType),
          msgVariables: {
            name: `${dynamicMsg(label)}`
          }
        })
      );
    });
  }
  return rejectWithValue({});
});

export const updateCodeToTextConfigBuilder = (
  builder: ActionReducerMapBuilder<CodeToTextState>
) => {
  builder.addCase(updateCodeToTextConfig.pending, (draftState, action) => {
    draftState.modal = {
      ...draftState.modal,
      loading: true
    };
  });
  builder.addCase(updateCodeToTextConfig.fulfilled, (draftState, action) => {
    draftState.modal = {
      ...draftState.modal,
      loading: false
    };
  });
  builder.addCase(updateCodeToTextConfig.rejected, (draftState, action) => {
    draftState.modal = {
      ...draftState.modal,
      loading: false
    };
  });
};
