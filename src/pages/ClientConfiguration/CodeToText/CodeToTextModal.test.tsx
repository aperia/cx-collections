import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import CodeToTextModal from './CodeToTextModal';

//actions
import { codeToTextActions } from './_redux/reducers';

//constants
import { I18N_CODE_TO_TEXT } from 'app/constants/i18n';

import * as useSelectFormValue from 'app/hooks/useSelectFormValue';
import { ActionType, CodeToTextRefData } from './types';
import isEmpty from 'lodash.isempty';
import { CODE_TO_TEXT_DROPDOWN_ITEM_NAME } from './constants';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const mockCodeToTextActions = mockActionCreator(codeToTextActions);
describe('Render UI', () => {
  const withSelectedCode: Partial<RootState> = {
    codeToText: {
      dropdownList: [],
      loading: false,
      modal: {
        actionType: '',
        open: false,
        selectedItem: undefined,
        codeToTextType: ''
      },
      selectedDropdown: [],
      selectedCodeToTextTypes: [],
      data: {}
    }
  };

  const withErrorMessage: Partial<RootState> = {
    codeToText: {
      dropdownList: [],
      loading: false,
      modal: {
        actionType: '',
        open: false,
        error: 'txt_error',
        selectedItem: undefined,
        codeToTextType: ''
      },
      selectedDropdown: [],
      selectedCodeToTextTypes: [],
      data: {}
    }
  };
  const renderWrapper = (
    initialState: Partial<RootState>,
    action: ActionType = 'add',
    selected: CodeToTextRefData | {} = {
      code: 'AES',
      description: 'AES 1'
    }
  ) => {
    const selectedItem = isEmpty(selected)
      ? undefined
      : {
          code: 'AES',
          description: 'AES 1'
        };

    return renderMockStore(
      <CodeToTextModal
        actionType={action}
        codeToTextType="accountExternalStatus"
        selectedItem={selectedItem}
      />,
      { initialState }
    );
  };

  it('render ui with code to text add modal', () => {
    const { rerender } = renderWrapper({});

    // rerender to trigger useEffect
    rerender(
      <CodeToTextModal
        actionType="add"
        codeToTextType="accountExternalStatus"
        selectedItem={{
          code: 'AES',
          description: 'AES 1'
        }}
      />
    );

    const addReasonCodeText = screen.getByText(I18N_CODE_TO_TEXT.ADD_CODE);

    expect(addReasonCodeText).toBeInTheDocument();
  });

  it('render ui with no selected code', () => {
    renderWrapper({}, 'add', {});

    const addReasonCodeText = screen.getByText(I18N_CODE_TO_TEXT.ADD_CODE);

    expect(addReasonCodeText).toBeInTheDocument();
  });

  it('render ui with code to text edit modal', () => {
    renderWrapper({}, 'edit');

    const editReasonCodeText = screen.getByText(I18N_CODE_TO_TEXT.EDIT_CODE);
    expect(editReasonCodeText).toBeInTheDocument();
  });

  it('render ui with code to text delete modal', () => {
    renderWrapper({}, 'delete');

    const deleteReasonCodeText = screen.getByText(
      I18N_CODE_TO_TEXT.DELETE_CODE
    );

    expect(deleteReasonCodeText).toBeInTheDocument();
  });

  it('when click cancel button in modal', () => {
    const mockAction = mockCodeToTextActions('toggleModal');
    const mockSetErrorMessage = mockCodeToTextActions('setErrorMessage');

    renderWrapper({});

    const cancelButton = screen.getByRole('button', {
      name: I18N_CODE_TO_TEXT.CANCEL
    });
    expect(cancelButton).toBeInTheDocument();

    userEvent.click(cancelButton);

    expect(mockAction).toHaveBeenCalled();
    expect(mockSetErrorMessage).toHaveBeenCalled();
  });

  describe('handleClick', () => {
    const formData = {
      code: 'code',
      description: 'description'
    };

    let spy: jest.SpyInstance;
    beforeEach(() => {
      spy = jest
        .spyOn(useSelectFormValue, 'useSelectFormValue')
        .mockImplementation(() => formData);
    });

    afterEach(() => {
      spy?.mockReset();
      spy?.mockRestore();
    });

    it('when click submit button without filling form in modal', () => {
      const mockAction = mockCodeToTextActions('updateCodeToTextConfig');
      const mockSetErrorMessage = mockCodeToTextActions('setErrorMessage');

      renderWrapper({});

      const editButton = screen.getByRole('button', {
        name: I18N_CODE_TO_TEXT.SUBMIT
      });
      expect(editButton).toBeInTheDocument();

      userEvent.click(editButton);
      expect(mockAction).toBeCalledWith({
        actionType: 'add',
        codeToTextType: 'accountExternalStatus',
        data: formData,
        label: CODE_TO_TEXT_DROPDOWN_ITEM_NAME.ACCOUNT_EXTERNAL_STATUS_CONFIGURATION
      });
      expect(mockSetErrorMessage).toBeCalledWith('');
    });

    it('when click submit button in delete modal', () => {
      const mockAction = mockCodeToTextActions('updateCodeToTextConfig');
      const mockSetErrorMessage = mockCodeToTextActions('setErrorMessage');

      renderWrapper(withSelectedCode, 'delete');

      const deleteButton = screen.getByRole('button', {
        name: I18N_CODE_TO_TEXT.DELETE
      });
      expect(deleteButton).toBeInTheDocument();

      userEvent.click(deleteButton);
      expect(mockAction).toBeCalledWith({
        actionType: 'delete',
        codeToTextType: 'accountExternalStatus',
        data: {
          code: 'AES',
          description: 'AES 1'
        },
        label: CODE_TO_TEXT_DROPDOWN_ITEM_NAME.ACCOUNT_EXTERNAL_STATUS_CONFIGURATION
      });
      expect(mockSetErrorMessage).toBeCalledWith('');
    });

    it('when click submit button in edit modal', () => {
      const mockEditAction = mockCodeToTextActions('updateCodeToTextConfig');
      const mockSetErrorMessage = mockCodeToTextActions('setErrorMessage');

      renderWrapper(withSelectedCode, 'edit');

      const editButton = screen.getByRole('button', {
        name: I18N_CODE_TO_TEXT.SAVE
      });
      expect(editButton).toBeInTheDocument();

      userEvent.click(editButton);
      expect(mockEditAction).not.toBeCalledWith({
        actionType: 'edit',
        codeToTextType: 'accountExternalStatus',
        data: formData,
        label: CODE_TO_TEXT_DROPDOWN_ITEM_NAME.ACCOUNT_EXTERNAL_STATUS_CONFIGURATION
      });
      expect(mockSetErrorMessage).not.toBeCalledWith('');
    });

    it('should render errorMessage', () => {
      const { rerender } = renderWrapper(withErrorMessage);

      // rerender to trigger useEffect
      rerender(
        <CodeToTextModal
          actionType="add"
          codeToTextType="accountExternalStatus"
          selectedItem={{
            code: 'AES',
            description: 'AES 1'
          }}
        />
      );

      expect(
        screen.getByText(withErrorMessage.codeToText!.modal.error!)
      ).toBeInTheDocument();
    });

    it('should render errorMessage when form values are empty', () => {
      spy = jest
        .spyOn(useSelectFormValue, 'useSelectFormValue')
        .mockImplementation(() => ({}));

      const { rerender } = renderWrapper(withErrorMessage, 'edit');

      // rerender to trigger useEffect
      rerender(
        <CodeToTextModal
          actionType="edit"
          codeToTextType="accountExternalStatus"
          selectedItem={{
            code: 'AES',
            description: 'AES 1'
          }}
        />
      );

      expect(
        screen.getByText(withErrorMessage.codeToText!.modal.error!)
      ).toBeInTheDocument();
    });
  });
});
