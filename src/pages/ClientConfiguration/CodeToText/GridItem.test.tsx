import React from 'react';

//mock test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';

//components
import GridItem from './GridItem';

//actions
import { codeToTextActions } from './_redux/reducers';
import userEvent from '@testing-library/user-event';
import { I18N_CODE_TO_TEXT } from 'app/constants/i18n';
import * as usePagination from 'app/hooks/usePagination';
import { CodeToTextType } from './types';
import { CODE_TO_TEXT_DROPDOWN_ITEM_NAME } from './constants';
import * as mockStatus from '../hooks/useCheckClientConfigStatus';

const mockCodeToTextActions = mockActionCreator(codeToTextActions);

const listGrid = {
  delinquencyReason: [
    { code: 'A', description: 'DR 1' },
    { code: 'B', description: 'DR 2' }
  ],
  cardStatus: [
    { code: 'A', description: 'CS 1' },
    { code: 'B', description: 'CS 2' }
  ],
  accountExternalStatus: [
    { code: 'A', description: 'AES 1' },
    { code: 'B', description: 'AES 2' }
  ]
};

const renderWrapper = (
  initialState: Partial<RootState>,
  codeToTextType: CodeToTextType
) => {
  return renderMockStore(<GridItem codeToTextType={codeToTextType} />, {
    initialState
  });
};

beforeEach(() => {
  jest.spyOn(usePagination, 'usePagination').mockImplementation(
    (data: any[]) =>
      ({
        total: 200,
        currentPage: 1,
        pageSize: [10, 25, 50],
        currentPageSize: 10,
        gridData: data,
        onPageChange: jest.fn(),
        onPageSizeChange: jest.fn(),
        setCurrentPage: jest.fn(),
        setCurrentPageSize: jest.fn()
      } as any)
  );
  jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(false);
});

describe('render actions buttons ', () => {
  beforeEach(() => {
    jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(false);
  });

  const initialState: Partial<RootState> = {
    codeToText: {
      dropdownList: [],
      loading: false,
      modal: {
        actionType: '',
        open: false,
        selectedItem: undefined,
        codeToTextType: ''
      },
      selectedDropdown: [],
      selectedCodeToTextTypes: [
        'accountExternalStatus',
        'delinquencyReason',
        'cardStatus'
      ],
      data: {}
    }
  };

  it('should render title and have data', () => {
    renderWrapper(
      {
        ...initialState,
        codeToText: {
          ...initialState.codeToText,
          data: listGrid
        } as any
      },
      'accountExternalStatus'
    );

    const title = screen.getByText(
      CODE_TO_TEXT_DROPDOWN_ITEM_NAME.ACCOUNT_EXTERNAL_STATUS_CONFIGURATION
    );
    expect(title).toBeInTheDocument();

    const table = screen.getByRole('table');
    expect(table).toBeInTheDocument();
  });

  it('should render title and empty data', () => {
    renderWrapper(initialState, 'accountExternalStatus');

    const emptyState = screen.getByText(I18N_CODE_TO_TEXT.NO_DATA);
    expect(emptyState).toBeInTheDocument();
  });

  it('should render title and empty data and toggle collapse expand section', () => {
    const mockToggleModal = mockCodeToTextActions('toggleModal');
    const mockSetErrorMessage = mockCodeToTextActions('setErrorMessage');

    renderWrapper(initialState, 'accountExternalStatus');

    const addCodeButton = screen.getByRole('button', {
      name: I18N_CODE_TO_TEXT.ADD_CODE
    });

    userEvent.click(addCodeButton);

    expect(mockToggleModal).toBeCalledWith({
      actionType: 'add',
      selectedItem: undefined,
      codeToTextType: 'accountExternalStatus'
    });

    expect(mockSetErrorMessage).toBeCalledWith('');
  });

  it('should render title and data and toggle collapse expand section and single item', () => {
    const mockToggleModal = mockCodeToTextActions('toggleModal');

    const mockSetErrorMessage = mockCodeToTextActions('setErrorMessage');

    renderWrapper(initialState, 'accountExternalStatus');

    const addCodeButton = screen.getByRole('button', {
      name: I18N_CODE_TO_TEXT.ADD_CODE
    });

    userEvent.click(addCodeButton);

    expect(mockToggleModal).toBeCalledWith({
      actionType: 'add',
      selectedItem: undefined,
      codeToTextType: 'accountExternalStatus'
    });

    expect(mockSetErrorMessage).toBeCalledWith('');
  });

  it('should render title and data and toggle collapse expand section and multiple item', () => {
    const mockToggleModal = mockCodeToTextActions('toggleModal');
    const mockSetErrorMessage = mockCodeToTextActions('setErrorMessage');

    renderWrapper(
      {
        ...initialState,
        codeToText: {
          ...initialState.codeToText,
          data: { accountExternalStatus: listGrid.accountExternalStatus }
        } as any
      },
      'accountExternalStatus'
    );

    userEvent.click(screen.getByText(I18N_CODE_TO_TEXT.CODE));

    const addCodeButton = screen.getByRole('button', {
      name: I18N_CODE_TO_TEXT.ADD_CODE
    });
    const toggleButton = screen.getAllByRole('button')[0];

    userEvent.click(toggleButton);
    userEvent.click(addCodeButton);

    expect(mockToggleModal).toBeCalledWith({
      actionType: 'add',
      selectedItem: undefined,
      codeToTextType: 'accountExternalStatus'
    });

    expect(mockSetErrorMessage).toBeCalledWith('');
  });

  it('should render have expand', () => {
    renderWrapper(
      {
        ...initialState,
        codeToText: {
          ...initialState.codeToText,
          selectedCodeToTextTypes: ['accountExternalStatus']
        } as any
      },
      'accountExternalStatus'
    );

    const title = screen.getByText(
      CODE_TO_TEXT_DROPDOWN_ITEM_NAME.ACCOUNT_EXTERNAL_STATUS_CONFIGURATION
    );
    expect(title).toBeInTheDocument();
  });
});
