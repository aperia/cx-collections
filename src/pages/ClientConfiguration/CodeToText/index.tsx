import React, { useEffect } from 'react';

//components
import { SimpleBar } from 'app/_libraries/_dls/components';
import CodeToTextDropDown from './CodeToTextDropDown';
import GridList from './GridList';

//constants
import { I18N_CODE_TO_TEXT } from 'app/constants/i18n';

//components
import CodeToTextModal from './CodeToTextModal';

//hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

//reducers
import { useDispatch, useSelector } from 'react-redux';

//selectors
import {
  selectedOpenModal,
  selectedActionType,
  selectedItemModal,
  selectedCodeToTextType
} from './_redux/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { codeToTextActions } from './_redux/reducers';
import ApiErrorForHeaderSection from '../ApiErrorSection/ApiErrorForHeaderSection';

const CodeToText: React.FC = () => {
  const { t } = useTranslation();
  const testId = 'clientConfig_codeToText';
  const dispatch = useDispatch();

  const isShowModal = useSelector(selectedOpenModal);

  const actionType = useSelector(selectedActionType);

  const selectedItem = useSelector(selectedItemModal);

  const codeToTextType = useSelector(selectedCodeToTextType);

  useEffect(() => {
    return () => {
      dispatch(codeToTextActions.resetCodeToText());
    };
  }, [dispatch]);

  const codeToTextDescription = () => {
    return (
      <div>
        <h5 data-testid={genAmtId(testId, 'title', '')}>
          {t(I18N_CODE_TO_TEXT.CODE_TO_TEXT)}
        </h5>
        <p className="mt-16" data-testid={genAmtId(testId, 'description', '')}>
          {t(I18N_CODE_TO_TEXT.CODE_TO_TEXT_DESCRIPTION)}
        </p>
      </div>
    );
  };

  return (
    <div className="position-relative h-100">
      <SimpleBar>
        <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
          <ApiErrorForHeaderSection
            className="mb-24"
            dataTestId={`${testId}_apiError`}
          />
          <div className="d-lg-flex align-items-center justify-content-between ">
            {codeToTextDescription()}
            <CodeToTextDropDown />
          </div>
          <GridList />
        </div>

        {isShowModal && (
          <CodeToTextModal
            actionType={actionType}
            codeToTextType={codeToTextType}
            selectedItem={selectedItem}
          />
        )}
      </SimpleBar>
    </div>
  );
};

export default CodeToText;
