//components
import { codeToTextName, dynamicMsg, getFulfilledMessage, getRejectedMessage } from './helper';

//constants
import { CODE_TO_TEXT_DROPDOWN_ITEM_NAME } from './constants';
import { I18N_CODE_TO_TEXT } from 'app/constants/i18n';

describe('CodeToText > codeToTextName', () => {
  it('codeToTextName with accountExternalStatus', () => {
    const value = codeToTextName('accountExternalStatus');

    expect(value).toEqual(
      CODE_TO_TEXT_DROPDOWN_ITEM_NAME.ACCOUNT_EXTERNAL_STATUS_CONFIGURATION
    );
  });
  it('codeToTextName with delinquencyReason', () => {
    const value = codeToTextName('delinquencyReason');

    expect(value).toEqual(
      CODE_TO_TEXT_DROPDOWN_ITEM_NAME.DELINQUENCY_REASON_CONFIGURATION
    );
  });
  it('codeToTextName with cardStatus', () => {
    const value = codeToTextName('cardStatus');

    expect(value).toEqual(
      CODE_TO_TEXT_DROPDOWN_ITEM_NAME.CARD_STATUS_CONFIGURATION
    );
  });
  it('codeToTextName with letterCode', () => {
    const value = codeToTextName('letterCode');

    expect(value).toEqual(
      CODE_TO_TEXT_DROPDOWN_ITEM_NAME.LETTER_CODE_CONFIGURATION
    );
  });
  it('codeToTextName with transactionCode', () => {
    const value = codeToTextName('transactionCode');

    expect(value).toEqual(
      CODE_TO_TEXT_DROPDOWN_ITEM_NAME.TRANSACTION_CODE_CONFIGURATION
    );
  });
  it('codeToTextName with accountInternalStatus', () => {
    const value = codeToTextName('accountInternalStatus');

    expect(value).toEqual(
      CODE_TO_TEXT_DROPDOWN_ITEM_NAME.ACCOUNT_INTERNAL_STATUS_CONFIGURATION
    );
  });
});

describe('CodeToText > dynamicMsg', () => {
  it('dynamicMsg without code', () => {
    const msg = dynamicMsg('Account External Status Configuration');

    expect(msg).toEqual('Account external status ');
  });

  it('dynamicMsg code letter', () => {
    const msg = dynamicMsg('Letter Code Configuration');

    expect(msg).toEqual('Letter  ');
  });
});

describe('CodeToText > getFulfilledMessage', () => {
  it('getFulfilledMessage on add', () => {
    const msg = getFulfilledMessage('add');
    expect(msg).toEqual(I18N_CODE_TO_TEXT.CODE_TO_TEXT_ADD_MESSAGE);
  });
  it('getFulfilledMessage on edit', () => {
    const msg = getFulfilledMessage('edit');
    expect(msg).toEqual(I18N_CODE_TO_TEXT.CODE_TO_TEXT_EDIT_MESSAGE);
  });
  it('getFulfilledMessage on delete', () => {
    const msg = getFulfilledMessage('delete');
    expect(msg).toEqual(I18N_CODE_TO_TEXT.CODE_TO_TEXT_DELETE_MESSAGE);
  });
});

describe('CodeToText > getRejectedMessage', () => {
  it('getRejectedMessage on add', () => {
    const msg = getRejectedMessage('add');
    expect(msg).toEqual(I18N_CODE_TO_TEXT.CODE_TO_TEXT_ADD_SERVER_ERROR);
  });
  it('getRejectedMessage on edit', () => {
    const msg = getRejectedMessage('edit');
    expect(msg).toEqual(I18N_CODE_TO_TEXT.CODE_TO_TEXT_EDIT_SERVER_ERROR);
  });
  it('getRejectedMessage on delete', () => {
    const msg = getRejectedMessage('delete');
    expect(msg).toEqual(I18N_CODE_TO_TEXT.CODE_TO_TEXT_DELETE_SERVER_ERROR);
  });
});
