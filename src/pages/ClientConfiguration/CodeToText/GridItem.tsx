import React, { useEffect } from 'react';

//components
import {
  Grid,
  ColumnType,
  Pagination,
  SortType,
  Button
} from 'app/_libraries/_dls/components';
import Actions from './Actions';
import SectionControl from 'app/components/SectionControl';
import { NoDataGrid } from 'app/components';
//constants
import { I18N_COMMON_TEXT, I18N_CODE_TO_TEXT } from 'app/constants/i18n';

//redux
import { useDispatch, batch, useSelector } from 'react-redux';

//hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { usePagination } from 'app/hooks';

//actions
import { codeToTextActions } from './_redux/reducers';

//helper
import { codeToTextName } from './helper';
import { orderBy } from 'app/_libraries/_dls/lodash';

//types
import { CodeToTextRefData, GridItemProps } from './types';

//selectors
import {
  selectedCodeToTextTypes,
  selectGridDataByType
} from './_redux/selectors';
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';
import { genAmtId } from 'app/_libraries/_dls/utils';

const GridItem: React.FC<GridItemProps> = ({ codeToTextType, dataTestId }) => {
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const selectGridData = useSelector<RootState, CodeToTextRefData[]>(
    (state: RootState) => selectGridDataByType(state, codeToTextType)
  );

  const selectedCodeToTextGroup = useSelector(selectedCodeToTextTypes);
  const isSingleItem = selectedCodeToTextGroup?.length === 1;

  const [sortBy, setSortBy] = React.useState<SortType>({ id: 'code' });

  const [isExpand, setIsExpand] = React.useState<boolean>(true);

  const status = useCheckClientConfigStatus();

  const handleAddCode = () => {
    batch(() => {
      dispatch(
        codeToTextActions.toggleModal({
          actionType: 'add',
          selectedItem: undefined,
          codeToTextType
        })
      );
      dispatch(codeToTextActions.setErrorMessage(''));
    });
  };

  const handleToggleExpand = () => setIsExpand(prev => !prev);

  const gridDataOrdered = React.useMemo(() => {
    return orderBy(
      selectGridData,
      [
        item => item[sortBy.id as keyof CodeToTextRefData]?.toLowerCase(),
        item => item['code']?.toLowerCase()
      ],
      [sortBy.order || false, 'asc']
    );
  }, [selectGridData, sortBy]);

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(gridDataOrdered);

  const isShowPagination = total > pageSize[0];

  const CODE_TO_TEXT_COLUMNS: ColumnType[] = React.useMemo(
    () => [
      {
        id: 'code',
        Header: t(I18N_CODE_TO_TEXT.CODE),
        isSort: true,
        width: 160,
        accessor: 'code'
      },
      {
        id: 'description',
        Header: t(I18N_CODE_TO_TEXT.DESCRIPTION),
        accessor: 'description',
        isSort: true,
        width: 680,
        autoWidth: true
      },
      {
        id: 'action',
        Header: (
          <div className="text-center">{t(I18N_COMMON_TEXT.ACTIONS)}</div>
        ),
        isFixedRight: true,
        width: 151,
        cellBodyProps: { className: 'text-center td-sm' },
        accessor: (dataAction: any, idx: number) => (
          <Actions
            data={dataAction}
            codeToTextType={codeToTextType}
            status={status}
            dataTestId={`${idx}_action_${dataTestId}`}
          />
        )
      }
    ],
    [t, codeToTextType, status, dataTestId]
  );

  useEffect(() => {
    if (selectedCodeToTextGroup?.length === 1) setIsExpand(true);
  }, [selectedCodeToTextGroup?.length]);

  const renderEmptyGrid = () => {
    return (
      <>
        <NoDataGrid
          text={
            <>
              <p className="mt-20 color-grey">{t(I18N_CODE_TO_TEXT.NO_DATA)}</p>
              {isSingleItem && (
                <div className="mx-auto mt-24">
                  <Button
                    disabled={status}
                    size="sm"
                    variant="outline-primary"
                    onClick={handleAddCode}
                    dataTestId={genAmtId(
                      dataTestId!,
                      'header-control_add-btn',
                      'HeaderControl'
                    )}
                  >
                    {t(I18N_CODE_TO_TEXT.ADD_CODE)}
                  </Button>
                </div>
              )}
            </>
          }
        />
      </>
    );
  };

  const handleSortChange = (sortType: SortType) => {
    setSortBy(sortType);
  };

  const renderGrid = () => {
    return (
      <Grid
        columns={CODE_TO_TEXT_COLUMNS}
        data={gridData}
        onSortChange={handleSortChange}
        sortBy={[sortBy]}
        dataTestId={genAmtId(dataTestId, 'grid', '')}
      />
    );
  };

  return (
    <>
      <SectionControl
        title={t(codeToTextName(codeToTextType))}
        classNames="mt-24"
        sectionKey={'key'}
        onToggle={handleToggleExpand}
        isExpand={isExpand}
        isAdd={
          isSingleItem && !gridData.length
            ? undefined
            : {
                enableSectionButton: isSingleItem,
                buttonLabel: t(I18N_CODE_TO_TEXT.ADD_CODE),
                enableToolTip: false,
                disableButton: status,
                handleAdd: handleAddCode
              }
        }
        dataTestId={dataTestId}
      >
        <div className="mt-16">
          {!gridData.length ? renderEmptyGrid() : renderGrid()}
          {isShowPagination && (
            <div className="pt-16">
              <Pagination
                totalItem={total}
                pageSize={pageSize}
                pageNumber={currentPage}
                pageSizeValue={currentPageSize}
                compact
                onChangePage={onPageChange}
                onChangePageSize={onPageSizeChange}
                dataTestId={genAmtId(dataTestId, 'pagination', '')}
              />
            </div>
          )}
        </div>
      </SectionControl>
    </>
  );
};

export default GridItem;
