import { I18N_CODE_TO_TEXT } from 'app/constants/i18n';
import { CODE_TO_TEXT_DROPDOWN_ITEM_NAME } from './constants';
import { ActionType } from './types';

export const codeToTextName = (name: string) => {
  switch (name) {
    case 'accountExternalStatus':
      return CODE_TO_TEXT_DROPDOWN_ITEM_NAME.ACCOUNT_EXTERNAL_STATUS_CONFIGURATION;
    case 'delinquencyReason':
      return CODE_TO_TEXT_DROPDOWN_ITEM_NAME.DELINQUENCY_REASON_CONFIGURATION;
    case 'cardStatus':
      return CODE_TO_TEXT_DROPDOWN_ITEM_NAME.CARD_STATUS_CONFIGURATION;
    case 'transactionCode':
      return CODE_TO_TEXT_DROPDOWN_ITEM_NAME.TRANSACTION_CODE_CONFIGURATION;
    case 'letterCode':
      return CODE_TO_TEXT_DROPDOWN_ITEM_NAME.LETTER_CODE_CONFIGURATION;
    case 'accountInternalStatus':
      return CODE_TO_TEXT_DROPDOWN_ITEM_NAME.ACCOUNT_INTERNAL_STATUS_CONFIGURATION;
  }
};

export const getFulfilledMessage = (type: ActionType) => {
  switch (type) {
    case 'add':
      return I18N_CODE_TO_TEXT.CODE_TO_TEXT_ADD_MESSAGE;
    case 'delete':
      return I18N_CODE_TO_TEXT.CODE_TO_TEXT_DELETE_MESSAGE;
    case 'edit':
      return I18N_CODE_TO_TEXT.CODE_TO_TEXT_EDIT_MESSAGE;
  }
};

export const getRejectedMessage = (type: ActionType) => {
  switch (type) {
    case 'add':
      return I18N_CODE_TO_TEXT.CODE_TO_TEXT_ADD_SERVER_ERROR;
    case 'delete':
      return I18N_CODE_TO_TEXT.CODE_TO_TEXT_DELETE_SERVER_ERROR;
    case 'edit':
      return I18N_CODE_TO_TEXT.CODE_TO_TEXT_EDIT_SERVER_ERROR;
  }
};

export const dynamicMsg = (name: any) => {
  const isHavingCode = name.includes('Code');
  const newString = name.toLowerCase().replace('code', '');
  const string = isHavingCode
    ? newString.replace('configuration', '')
    : name.toLowerCase().replace('configuration', '');

  return string.charAt(0).toUpperCase() + string.slice(1);
};
