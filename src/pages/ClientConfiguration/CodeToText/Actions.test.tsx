import React from 'react';

//mock test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

//components
import Actions from './Actions';

//actions
import { codeToTextActions } from './_redux/reducers';

//constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

const mockCodeToTextActions = mockActionCreator(codeToTextActions);
describe('render actions buttons ', () => {
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(
      <Actions
        data={{ code: 'A', description: 'AES 1' }}
        codeToTextType="accountExternalStatus"
      />,
      { initialState }
    );
  };

  it('should render modal delete and call dispatch modal actions when click delete button', () => {
    const mockAction = mockCodeToTextActions('toggleModal');

    renderWrapper({});

    const deleteButton = screen.getByRole('button', {
      name: I18N_COMMON_TEXT.DELETE
    });

    userEvent.click(deleteButton);

    expect(deleteButton).toBeInTheDocument();
    expect(mockAction).toBeCalledWith({
      actionType: 'delete',
      selectedItem: {
        code: "A",
        description: "AES 1",
      },
      codeToTextType: "accountExternalStatus",
    });
  });

  it('should render modal edit and call dispatch modal actions when click edit button', () => {
    const mockAction = mockCodeToTextActions('toggleModal');

    renderWrapper({});

    const editButton = screen.getByRole('button', {
      name: I18N_COMMON_TEXT.EDIT
    });

    userEvent.click(editButton);

    expect(editButton).toBeInTheDocument();
    expect(mockAction).toBeCalledWith({
      actionType: 'edit',
      selectedItem: {
        code: "A",
        description: "AES 1",
      },
      codeToTextType: "accountExternalStatus",
    });
  });
});
