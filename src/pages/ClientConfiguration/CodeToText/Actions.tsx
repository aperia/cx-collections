import React from 'react';

// components
import { Button } from 'app/_libraries/_dls/components';

//utils
import { batch, useDispatch } from 'react-redux';

//redux
import { useTranslation } from 'app/_libraries/_dls/hooks';

//reducers
import { codeToTextActions } from './_redux/reducers';

//constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

//types
import { ActionsProps } from './types';
import { genAmtId } from 'app/_libraries/_dls/utils';

const Actions: React.FC<ActionsProps> = ({
  data,
  codeToTextType,
  status = false,
  dataTestId
}) => {
  const dispatch = useDispatch();

  const { t } = useTranslation();

  const handleEdit = () => {
    batch(() => {
      dispatch(
        codeToTextActions.toggleModal({
          actionType: 'edit',
          selectedItem: data,
          codeToTextType
        })
      );
    });
  };

  const handleDelete = () => {
    batch(() => {
      dispatch(
        codeToTextActions.toggleModal({
          actionType: 'delete',
          selectedItem: data,
          codeToTextType
        })
      );
    });
  };

  return (
    <>
      <Button
        disabled={status}
        variant="outline-danger"
        size="sm"
        onClick={handleDelete}
        dataTestId={genAmtId(dataTestId, 'deleteBtn', '')}
      >
        {t(I18N_COMMON_TEXT.DELETE)}
      </Button>
      <Button
        disabled={status}
        variant="outline-primary"
        size="sm"
        onClick={handleEdit}
        dataTestId={genAmtId(dataTestId, 'editBtn', '')}
      >
        {t(I18N_COMMON_TEXT.EDIT)}
      </Button>
    </>
  );
};

export default Actions;
