import React from 'react';

//mock test utils
import { renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';

//components
import GridList from './GridList';

import { I18N_COMMON_TEXT } from 'app/constants/i18n';

import userEvent from '@testing-library/user-event';
import { CODE_TO_TEXT_DROPDOWN_ITEM_NAME } from './constants';

describe('render actions buttons ', () => {
  const initialState: Partial<RootState> = {
    codeToText: {
      dropdownList: [],
      loading: false,
      modal: {
        actionType: '',
        open: false,
        selectedItem: undefined,
        codeToTextType: ''
      },
      selectedDropdown: [],
      selectedCodeToTextTypes: [
        'accountExternalStatus',
        'delinquencyReason',
        'cardStatus'
      ],
      data: {
        delinquencyReason: [
          { code: 'A', description: 'DR 1' },
          { code: 'B', description: 'DR 2' }
        ],
        cardStatus: [
          { code: 'A', description: 'CS 1' },
          { code: 'B', description: 'CS 2' }
        ],
        accountExternalStatus: [
          { code: 'A', description: 'AES 1' },
          { code: 'B', description: 'AES 2' }
        ]
      }
    }
  };
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<GridList />, { initialState });
  };

  it('should render list grid with data', () => {
    renderWrapper(initialState);

    const title1 = screen.getByText(
      CODE_TO_TEXT_DROPDOWN_ITEM_NAME.ACCOUNT_EXTERNAL_STATUS_CONFIGURATION
    );
    const title2 = screen.getByText(
      CODE_TO_TEXT_DROPDOWN_ITEM_NAME.DELINQUENCY_REASON_CONFIGURATION
    );
    const title3 = screen.getByText(
      CODE_TO_TEXT_DROPDOWN_ITEM_NAME.CARD_STATUS_CONFIGURATION
    );

    expect(title1).toBeInTheDocument();
    expect(title2).toBeInTheDocument();
    expect(title3).toBeInTheDocument();
  });

  it('should render list grid with data and loading false and have error and handle reload', () => {
    renderWrapper({
      ...initialState,
      codeToText: {
        ...initialState.codeToText,
        error: true
      }
    } as any);

    expect(
      screen.getByText('txt_data_load_unsuccessful_click_reload_to_try_again')
    ).toBeInTheDocument();
    expect(screen.getByText('txt_failure_ocurred_on')).toBeInTheDocument();

    const reloadButton = screen.getByRole('button', {
      name: I18N_COMMON_TEXT.RELOAD
    });

    expect(reloadButton).toBeInTheDocument();
    userEvent.click(reloadButton);
  });
});
