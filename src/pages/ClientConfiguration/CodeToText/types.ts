export interface CodeToTextState {
  dropdownList?: CodeToTextRefData[];
  selectedDropdown?: CodeToTextRefData[];
  modal: CodeToTextModal;
  loading?: boolean;
  error?: boolean;
  selectedCodeToTextTypes?: string[];
  data?: Record<string, CodeToTextRefData[]>;
}

export interface CodeToTextModal extends CodeToTextModalType {
  loading?: boolean;
  error?: string;
  open?: boolean;
}

export interface CodeToTextItems extends CodeToTextRefData {}

export interface ActionsProps extends DLSId {
  data: CodeToTextRefData;
  codeToTextType: CodeToTextType;
  status?: boolean;
}

export interface CodeToTextModalType {
  actionType: ActionType;
  codeToTextType: CodeToTextType;
  selectedItem?: CodeToTextRefData;
}

export interface GridItemProps extends DLSId {
  codeToTextType: CodeToTextType;
}

export type CodeToTextType =
  | 'transactionCode'
  | 'letterCode'
  | 'accountExternalStatus'
  | 'delinquencyReason'
  | 'cardStatus'
  | 'accountInternalStatus'
  | '';

export type ActionType = 'add' | 'delete' | 'edit' | '';

export interface CodeToTextRefData {
  description: string;
  code: string;
}
