import { apiService } from 'app/utils/api.service';
import axios from 'axios';
import { IRequestUpdateDebtCompany } from './types';

const debtManageCompaniesService = {
  getCompany() {
    const url =
      window.appConfig?.api?.debtManagementCompanies?.getCompany || '';
    return apiService.post(url, {});
  },

  deleteCompany(id: string) {
    const url =
      window.appConfig?.api?.debtManagementCompanies?.deleteCompany || '';
    return apiService.post(url, { id });
  },

  updateCompany(data: IRequestUpdateDebtCompany) {
    const url =
      window.appConfig?.api?.debtManagementCompanies?.updateCompany || '';
    return apiService.post(url, data);
  },

  getState: async () => {
    const { data } = await axios.get('refData/state.json');

    return data || [];
  }
};

export default debtManageCompaniesService;
