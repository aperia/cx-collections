import { formatContactPhone } from 'app/helpers';
import isEmpty from 'lodash.isempty';
import { IDebtCompany } from './types';

export const renewDataForChangeHistory = (data: IDebtCompany) => {
  return {
    ...data,
    companyPhone: data.companyPhone
      ? formatContactPhone(data.companyPhone)
      : undefined,
    companyFax: data.companyFax
      ? formatContactPhone(data.companyFax)
      : undefined,
    companyState: isEmpty(data.companyState)
      ? ''
      : `${data.companyState.value} - ${data.companyState.description}`,
    companyTitle: undefined,
    active: undefined
  };
};
