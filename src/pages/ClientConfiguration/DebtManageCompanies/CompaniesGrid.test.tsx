import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { fireEvent, screen } from '@testing-library/react';
import CompaniesGrid from './CompaniesGrid';
import { clientConfigDebtCompaniesActions } from './_redux/reducer';
import { IInitialStateDebtCompanies } from './types';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import userEvent from '@testing-library/user-event';

jest.mock('app/_libraries/_dls/components/Pagination', () => {
  return () => <div>Pagination</div>;
});

const companies = {
  companyName: 'companyName',
  addressLineOne: 'addressLineOne',
  addressLineTwo: 'addressLineOne',
  companyCity: 'companyCity',
  companyState: {
    description: 'companyState',
    value: 'companyState'
  },
  companyZipCode: 'companyZipCode',
  companyContact: 'companyContact',
  companyPhone: 'companyPhone',
  companyFax: 'companyFax',
  companyEmail: 'companyEmail',
  active: true
};

describe('Test CompaniesGrid', () => {
  const mockActions = mockActionCreator(clientConfigDebtCompaniesActions);
  afterEach(() => {
    jest.restoreAllMocks();
  });

  const generateState = (data?: Partial<IInitialStateDebtCompanies>) => {
    return {
      clientConfigurationDebtCompanies: {
        companies: [],
        company: {},
        isLoading: true,
        isLoadingModal: false,
        isOpenModal: false,
        ...data
      }
    } as Partial<RootState>;
  };

  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<CompaniesGrid />, { initialState });
  };

  it('Should have render UI', () => {
    renderWrapper(
      generateState({
        companies: [companies]
      })
    );
    const queryName = screen.queryByText(I18N_COMMON_TEXT.NAME);
    const queryAddOne = screen.queryByText(I18N_COMMON_TEXT.ADDRESS_LINE_ONE);
    const queryAddTwo = screen.queryByText(I18N_COMMON_TEXT.ADDRESS_LINE_TWO);
    const queryCity = screen.queryByText(I18N_COMMON_TEXT.CITY);
    const queryActions = screen.queryByText(I18N_COMMON_TEXT.ACTIONS);
    const queryFax = screen.queryByText(I18N_COMMON_TEXT.FAX);
    const queryPhone = screen.queryByText(I18N_COMMON_TEXT.PHONE);
    const queryState = screen.queryByText(I18N_COMMON_TEXT.STATE);
    const queryZip = screen.queryByText(I18N_COMMON_TEXT.ZIP);
    const queryContact = screen.queryByText(I18N_COMMON_TEXT.CONTACT);
    const queryValuePhone = screen.queryByText('companyPhone');
    const queryValueFax = screen.queryByText('companyFax');

    expect(queryValuePhone).toBeInTheDocument();
    expect(queryValueFax).toBeInTheDocument();
    expect(queryName).toBeInTheDocument();
    expect(queryAddOne).toBeInTheDocument();
    expect(queryAddTwo).toBeInTheDocument();
    expect(queryCity).toBeInTheDocument();
    expect(queryActions).toBeInTheDocument();
    expect(queryFax).toBeInTheDocument();
    expect(queryPhone).toBeInTheDocument();
    expect(queryState).toBeInTheDocument();
    expect(queryZip).toBeInTheDocument();
    expect(queryContact).toBeInTheDocument();
  });

  it('Should have render pagination', () => {
    renderWrapper(
      generateState({
        companies: [
          companies,
          { ...companies, companyName: 'companyName2' },
          { ...companies, companyName: 'companyName3' },
          { ...companies, companyName: 'companyName4' },
          companies,
          companies,
          companies,
          companies,
          companies,
          companies,
          companies
        ]
      })
    );

    const queryPagination = screen.queryByText('Pagination');
    expect(queryPagination).toBeInTheDocument();
    

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.NAME));
    userEvent.click(screen.getByText(I18N_COMMON_TEXT.NAME));
    userEvent.click(screen.getByText(I18N_COMMON_TEXT.STATE));
    expect(screen.getByText(I18N_COMMON_TEXT.STATE).querySelector('.sort-icon')).toBeInTheDocument();
  });

  it('Should have not value phone and fax', () => {
    renderWrapper(
      generateState({
        companies: [{ ...companies, companyPhone: '', companyFax: '' }]
      })
    );

    const queryValuePhone = screen.queryByText('companyPhone');
    const queryValueFax = screen.queryByText('companyFax');

    expect(queryValuePhone).not.toBeInTheDocument();
    expect(queryValueFax).not.toBeInTheDocument();
  });

  it('Should have click Edit', () => {
    const onChangeOpenModal = mockActions('onChangeOpenModal');
    const onChangeCompany = mockActions('onChangeCompany');
    renderWrapper(
      generateState({
        companies: [companies]
      })
    );

    const queryEdit = screen.queryByText(I18N_COMMON_TEXT.EDIT);
    queryEdit!.click();

    expect(onChangeOpenModal).toHaveBeenCalled();
    expect(onChangeCompany).toHaveBeenCalled();
  });

  it('Should have click Delete', () => {
    const onChangeOpenModalDelete = mockActions('onChangeOpenModalDelete');
    const onChangeCompany = mockActions('onChangeCompany');

    renderWrapper(
      generateState({
        companies: [companies]
      })
    );

    const queryDelete = screen.queryByText(I18N_COMMON_TEXT.DELETE);
    fireEvent.click(queryDelete!);

    expect(onChangeOpenModalDelete).toHaveBeenCalled();
    expect(onChangeCompany).toHaveBeenCalled();
  });
});
