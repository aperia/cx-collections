import React from 'react';

// components
import { ModalHeader, ModalTitle } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useDispatch, useSelector } from 'react-redux';

// actions
import { clientConfigDebtCompaniesActions as actions } from '../_redux/reducer';

// i18n
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { selectorCompany } from '../_redux/selectors';
import { isEmpty } from 'app/_libraries/_dls/lodash';

const FormHeader: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const company = useSelector(selectorCompany);
  const handleCloseModal = () => {
    dispatch(actions.onChangeOpenModal());
  };
  const testId = 'clientConfig_debtManageCompany_form';

  return (
    <ModalHeader
      border
      closeButton
      onHide={handleCloseModal}
      dataTestId={`${testId}_header`}
    >
      <ModalTitle dataTestId={`${testId}_title`}>
        {t(
          isEmpty(company)
            ? I18N_COMMON_TEXT.ADD_COMPANY
            : I18N_COMMON_TEXT.EDIT_COMPANY
        )}
      </ModalTitle>
    </ModalHeader>
  );
};

export default FormHeader;
