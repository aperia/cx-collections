import React, { MutableRefObject } from 'react';
import {
  mockActionCreator,
  queryByClass,
  renderMockStore
} from 'app/test-utils';
import { screen } from '@testing-library/react';
import Form from './';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { clientConfigDebtCompaniesActions } from '../_redux/reducer';
import { IInitialStateDebtCompanies } from '../types';
import * as reduxForm from 'redux-form';
const mockActions = mockActionCreator(clientConfigDebtCompaniesActions);

describe('Test form of DebtManageCompany', () => {
  beforeEach(() => {
    jest.spyOn(reduxForm, 'isDirty').mockImplementation(() => () => true);
  });
  afterEach(() => {
    jest.restoreAllMocks();
  });

  const generateState = (data?: Partial<IInitialStateDebtCompanies>) => {
    return {
      clientConfigurationDebtCompanies: {
        companies: [],
        company: {},
        isLoading: true,
        isLoadingModalUpdate: false,
        isOpenModalUpdate: true,
        ...data
      } as IInitialStateDebtCompanies
    } as Partial<RootState>;
  };

  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<Form />, { initialState });
  };

  it('Should have render button submit ', () => {
    renderWrapper(generateState());

    const queryText = screen.queryByText(I18N_COMMON_TEXT.SUBMIT);
    expect(queryText).toBeInTheDocument();
  });

  it('Should have render button save ', () => {
    renderWrapper(generateState({ company: { id: '1' } }));

    const queryText = screen.queryByText(I18N_COMMON_TEXT.SAVE);

    expect(queryText).toBeInTheDocument();
  });

  it('Should have click button cancel', () => {
    const onChangeOpenModal = mockActions('onChangeOpenModal');
    renderWrapper(generateState());

    const queryButtonClose = screen.queryByText(I18N_COMMON_TEXT.CANCEL);
    queryButtonClose!.click();
    expect(onChangeOpenModal).toHaveBeenCalled();
  });

  it('Should have updateCompany', () => {
    const updateCompany = mockActions('updateCompany');
    const ref = {} as MutableRefObject<MagicKeyValue>;
    renderWrapper(generateState());

    const queryButtonClose = screen.queryByText(I18N_COMMON_TEXT.SUBMIT);
    queryButtonClose!.click();
    expect(updateCompany).toBeCalledWith({ ref });
  });

  it('Should have loading', () => {
    const { wrapper } = renderWrapper(
      generateState({ isLoadingModalUpdate: true })
    );

    const queryLoading = queryByClass(
      wrapper.baseElement as HTMLElement,
      /loading/
    );
    expect(queryLoading).toBeInTheDocument();
  });
});
