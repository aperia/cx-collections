import React, { useEffect, useMemo, useRef } from 'react';

// components
import { Modal, ModalFooter } from 'app/_libraries/_dls/components';
import Header from './Header';
import { View } from 'app/_libraries/_dof/core';

// redux
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useDispatch, useSelector } from 'react-redux';
import { getFormValues, isValid } from 'redux-form';
import { clientConfigDebtCompaniesActions as actions } from '../_redux/reducer';
import {
  selectorCompany,
  selectorLoadingModalUpdate,
  selectorOpenModalUpdate
} from '../_redux/selectors';

// i18n
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import isEmpty from 'lodash.isempty';
import ModalBodyWithErrorClientConfig from 'pages/ClientConfiguration/ApiErrorSection/ModalBodyWithErrorClientConfig';
import { useDisableSubmitButton } from 'app/hooks/useDisableSubmitButton';
import { IDebtCompany } from '../types';

const Form: React.FC = () => {
  const { t } = useTranslation();
  const ref = useRef<any>();
  const dispatch = useDispatch();
  const isOpen = useSelector(selectorOpenModalUpdate);
  const loading = useSelector(selectorLoadingModalUpdate);
  const company = useSelector(selectorCompany);
  const isFormValid = useSelector(isValid('clientConfigDebtCompaniesForm'));
  const testId = 'clientConfig_debtManageCompany_form';

  const formValues = useSelector(
    getFormValues('clientConfigDebtCompaniesForm')
  ) as IDebtCompany;

  const { disableSubmit, setPrimitiveValues } = useDisableSubmitButton({
    ...formValues,
    addressLineOne: formValues?.addressLineOne,
    addressLineTwo: formValues?.addressLineTwo || undefined,
    companyCity: formValues?.companyCity,
    companyContact: formValues?.companyContact || undefined,
    companyFax: formValues?.companyFax || undefined,
    companyName: formValues?.companyName,
    companyPhone: formValues?.companyPhone || undefined,
    companyZipCode: formValues?.companyZipCode,
    companyState: formValues?.companyState,
    active: formValues?.active
  } as IDebtCompany);

  useEffect(() => {
    setPrimitiveValues({
      ...company,
      companyTitle: t(I18N_COMMON_TEXT.ENTER_PHONE_FAX_OR_BOTH)
    } as IDebtCompany);
  }, [company, setPrimitiveValues, t]);
  const isDisabledButtonSubmit = !isEmpty(company) && disableSubmit;

  const handleCloseModal = () => {
    dispatch(actions.onChangeOpenModal());
  };

  const handleClick = () => {
    dispatch(
      actions.updateCompany({
        ref
      })
    );
  };

  const render = useMemo(() => {
    return (
      <View
        ref={ref}
        id={'clientConfigDebtCompaniesForm'}
        formKey={'clientConfigDebtCompaniesForm'}
        descriptor={'clientConfigDebtCompaniesForm'}
        value={{
          companyTitle: t(I18N_COMMON_TEXT.ENTER_PHONE_FAX_OR_BOTH),
          ...company
        }}
      />
    );
  }, [t, company]);

  return (
    <Modal loading={loading} sm show={isOpen} dataTestId={testId}>
      <Header />
      <ModalBodyWithErrorClientConfig
        apiErrorClassName="mb-24"
        dataTestId={`${testId}_body`}
      >
        {render}
      </ModalBodyWithErrorClientConfig>
      <ModalFooter
        dataTestId={`${testId}_footer`}
        cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
        okButtonText={t(
          isEmpty(company) ? I18N_COMMON_TEXT.SUBMIT : I18N_COMMON_TEXT.SAVE
        )}
        onCancel={handleCloseModal}
        onOk={handleClick}
        disabledOk={!isFormValid || isDisabledButtonSubmit}
      />
    </Modal>
  );
};

export default React.memo(Form);
