import React from 'react';
import {
  mockActionCreator,
  queryByClass,
  renderMockStore
} from 'app/test-utils';
import { screen } from '@testing-library/react';
import Header from './Header';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { clientConfigDebtCompaniesActions } from '../_redux/reducer';
import { IInitialStateDebtCompanies } from '../types';

const mockActions = mockActionCreator(clientConfigDebtCompaniesActions);

describe('Test header of DebtManageCompany', () => {
  afterEach(() => {
    jest.restoreAllMocks();
  });

  const generateState = (data?: Partial<IInitialStateDebtCompanies>) => {
    return {
      clientConfigurationDebtCompanies: {
        companies: [],
        company: {},
        isLoading: true,
        isLoadingModal: false,
        isOpenModal: false,
        ...data
      }
    } as Partial<RootState>;
  };

  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<Header />, { initialState });
  };

  it('Should have render Add ', () => {
    renderWrapper(generateState());

    const queryText = screen.queryByText(I18N_COMMON_TEXT.ADD_COMPANY);

    expect(queryText).toBeInTheDocument();
  });

  it('Should have render Edit ', () => {
    renderWrapper(generateState({ company: { id: '1' } }));

    const queryText = screen.queryByText(I18N_COMMON_TEXT.EDIT_COMPANY);

    expect(queryText).toBeInTheDocument();
  });

  it('Should have click button close', () => {
    const onChangeOpenModal = mockActions('onChangeOpenModal');
    const { container } = renderWrapper(
      generateState({ company: { id: 'id' } })
    );

    const queryIconClose = queryByClass(container, /icon icon-close/);
    queryIconClose!.click();
    expect(onChangeOpenModal).toHaveBeenCalled();
  });
});
