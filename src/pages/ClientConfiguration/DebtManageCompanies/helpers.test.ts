import * as helpers from './helpers';
import { IDebtCompany } from './types';

describe('Test helpers', () => {
  describe('renewDataForChangeHistory', () => {
    it('should return with formatted data', () => {
      const args: IDebtCompany = {
        companyName: 'companyName',
        addressLineOne: 'addressLineOne',
        addressLineTwo: 'addressLineTwo',
        companyCity: 'companyCity',
        companyState: {
          description: 'description',
          value: 'value'
        },
        companyZipCode: 'companyZipCode',
        companyContact: 'companyContact',
        companyPhone: 'companyPhone',
        companyFax: 'companyFax',
        active: true
      };

      expect(helpers.renewDataForChangeHistory(args)).toEqual({
        companyName: 'companyName',
        addressLineOne: 'addressLineOne',
        addressLineTwo: 'addressLineTwo',
        companyCity: 'companyCity',
        companyState: 'value - description',
        companyZipCode: 'companyZipCode',
        companyContact: 'companyContact',
        companyPhone: 'companyPhone',
        companyFax: 'companyFax',
        active: undefined,
        companyTitle: undefined
      });
    });

    it('should return with some undefined data', () => {
      const args: IDebtCompany = {
        companyName: 'companyName',
        addressLineOne: 'addressLineOne',
        addressLineTwo: 'addressLineTwo',
        companyCity: 'companyCity',
        companyState: {} as RefDataValue,
        companyZipCode: 'companyZipCode',
        companyContact: 'companyContact',
        companyPhone: '',
        companyFax: '',
        active: true
      };

      expect(helpers.renewDataForChangeHistory(args)).toEqual({
        companyName: 'companyName',
        addressLineOne: 'addressLineOne',
        addressLineTwo: 'addressLineTwo',
        companyCity: 'companyCity',
        companyState: '',
        companyZipCode: 'companyZipCode',
        companyContact: 'companyContact',
        companyPhone: undefined,
        companyFax: undefined,
        active: undefined,
        companyTitle: undefined
      });
    });
  });
});
