import React, { useCallback, useMemo, useRef } from 'react';

// components
import {
  ColumnType,
  Grid,
  GridRef,
  Pagination,
  SortType
} from 'app/_libraries/_dls/components';
import GridActions from 'pages/__commons/GridActions';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { batch, useDispatch, useSelector } from 'react-redux';
import { usePagination } from 'app/hooks';

// reducer
import { selectorCompanies } from './_redux/selectors';
import { clientConfigDebtCompaniesActions as actions } from './_redux/reducer';

// i18
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// types
import { IDebtCompany } from './types';
import { formatCommon } from 'app/helpers';
import ApiErrorForHeaderSection from '../ApiErrorSection/ApiErrorForHeaderSection';
import { ClientConfigStatusProps } from '../types';
import { orderBy } from 'lodash';

const CompaniesGrid: React.FC<ClientConfigStatusProps> = ({ status }) => {
  const gridRefs = useRef<GridRef>(null);
  const data = useSelector(selectorCompanies);
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const testId = 'clientConfig_debtManageCompany_grid';

  const [sortBy, setSortBy] = React.useState<SortType>({ id: 'companyName' });

  const handleDeleteCompany = useCallback(
    (company: IDebtCompany) => {
      batch(() => {
        dispatch(actions.onChangeOpenModalDelete());
        dispatch(actions.onChangeCompany({ company }));
      });
    },
    [dispatch]
  );

  const handleEdit = useCallback(
    (company: IDebtCompany) => {
      batch(() => {
        dispatch(actions.onChangeOpenModal());
        dispatch(actions.onChangeCompany({ company }));
      });
    },
    [dispatch]
  );

  const dataColumn = useMemo(() => {
    return [
      {
        id: 'companyName',
        Header: t(I18N_COMMON_TEXT.NAME),
        accessor: 'companyName',
        isSort: true,
        width: 160
      },
      {
        id: 'addressLineOne',
        Header: t(I18N_COMMON_TEXT.ADDRESS_LINE_ONE),
        accessor: 'addressLineOne',
        isSort: true,
        width: 160
      },
      {
        id: 'addressLineTwo',
        Header: t(I18N_COMMON_TEXT.ADDRESS_LINE_TWO),
        accessor: 'addressLineTwo',
        isSort: true,
        width: 160
      },
      {
        id: 'companyCity',
        Header: t(I18N_COMMON_TEXT.CITY),
        isSort: true,
        width: 126,
        accessor: 'companyCity'
      },
      {
        id: 'companyState',
        Header: t(I18N_COMMON_TEXT.STATE),
        isSort: true,
        width: 170,
        accessor: (item: IDebtCompany) => (
          <span className="form-group-static__text">
            {item.companyState?.description}
          </span>
        )
      },
      {
        id: 'companyZipCode',
        Header: t(I18N_COMMON_TEXT.ZIP),
        isSort: true,
        width: 120,
        accessor: 'companyZipCode'
      },
      {
        id: 'companyContact',
        Header: t(I18N_COMMON_TEXT.CONTACT),
        isSort: true,
        width: 160,
        accessor: 'companyContact'
      },
      {
        id: 'companyPhone',
        Header: t(I18N_COMMON_TEXT.PHONE),
        isSort: true,
        width: 150,
        accessor: (item: IDebtCompany) => (
          <span className="form-group-static__text">
            {item.companyPhone ? formatCommon(item.companyPhone).phone : ''}
          </span>
        )
      },
      {
        id: 'companyFax',
        Header: t(I18N_COMMON_TEXT.FAX),
        isSort: true,
        width: 150,
        accessor: (item: IDebtCompany) => (
          <span className="form-group-static__text">
            {item.companyFax ? formatCommon(item.companyFax).phone : ''}
          </span>
        )
      },
      {
        id: I18N_COMMON_TEXT.ACTIONS,
        Header: t(I18N_COMMON_TEXT.ACTIONS),
        accessor: (item: IDebtCompany, idx: number) => (
          <GridActions
            status={status}
            onClickEdit={() => handleEdit(item)}
            onClickDelete={() => handleDeleteCompany(item)}
            dataTestId={`${idx}_action_${testId}`}
          />
        ),
        width: 138,
        isFixedRight: true,
        cellProps: { className: 'text-center' },
        cellBodyProps: { className: 'text-center' },
        className: 'td-sm'
      }
    ];
  }, [t, handleDeleteCompany, handleEdit, status]);

  const dataOrdered = useMemo(() => {
    return orderBy(
      data,
      [
        item =>
          (sortBy.id === 'companyState'
            ? item.companyState?.description
            : item[sortBy.id as keyof IDebtCompany]
          )?.toString()?.toLowerCase(),
        item => item.companyName?.toLowerCase()
      ],
      [sortBy.order || false, 'asc']
    );
  }, [data, sortBy]);

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(dataOrdered);

  const handleSortChange = (sortType: SortType) => {
    setSortBy(sortType);
  };

  const showPagination = total > pageSize[0];

  return (
    <React.Fragment>
      <div className="mt-16">
        <ApiErrorForHeaderSection className="mb-24" dataTestId={`${testId}_apiError`} />
        <Grid
          ref={gridRefs}
          columns={dataColumn as ColumnType[]}
          data={gridData}
          sortBy={[sortBy]}
          onSortChange={handleSortChange}
          dataTestId={`${testId}_grid`}
        />
        {showPagination && (
          <div className="pt-16">
            <Pagination
              totalItem={total}
              pageSize={pageSize}
              pageNumber={currentPage}
              pageSizeValue={currentPageSize}
              compact
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
              dataTestId={`${testId}_pagination`}
            />
          </div>
        )}
      </div>
    </React.Fragment>
  );
};

export default CompaniesGrid;
