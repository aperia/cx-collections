import React from 'react';

// components
import { Button } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useDispatch, useSelector } from 'react-redux';

// i18
import {
  I18N_COMMON_TEXT,
  I18N_DEBT_COMPANY_TO_TEXT
} from 'app/constants/i18n';

// actions
import { clientConfigDebtCompaniesActions as actions } from './_redux/reducer';
import { selectorDisplayNoData } from './_redux/selectors';
import { ClientConfigStatusProps } from '../types';
import { genAmtId } from 'app/_libraries/_dls/utils';

const Title: React.FC<ClientConfigStatusProps> = ({ status }) => {
  const dispatch = useDispatch();
  const testId = 'clientConfig_debtManageCompany_header';

  const { t } = useTranslation();

  const isDisplayNoData = useSelector(selectorDisplayNoData);

  const handleOpenModal = () => {
    dispatch(actions.onChangeOpenModal());
  };

  return (
    <React.Fragment>
      <h5 data-testid={genAmtId(testId, 'title', '')}>
        {t(I18N_DEBT_COMPANY_TO_TEXT.DEBT_MANAGEMENT_COMPANIES)}
      </h5>
      <div className="d-flex justify-content-between align-items-center mt-16">
        <p className="fw-500" data-testid={genAmtId(testId, 'companyList', '')}>
          {t(I18N_COMMON_TEXT.COMPANY_LIST)}
        </p>
        {!isDisplayNoData && (
          <Button
            disabled={status}
            className="mr-n8"
            size="sm"
            onClick={handleOpenModal}
            variant="outline-primary"
            dataTestId={`${testId}_addCompanyBtn`}
          >
            {t(I18N_COMMON_TEXT.ADD_COMPANY)}
          </Button>
        )}
      </div>
    </React.Fragment>
  );
};

export default Title;
