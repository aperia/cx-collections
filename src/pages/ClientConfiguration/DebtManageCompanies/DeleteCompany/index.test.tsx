import React from 'react';
import {
  mockActionCreator,
  queryByClass,
  renderMockStore
} from 'app/test-utils';
import { screen } from '@testing-library/react';
import DeleteDebtCompany from './';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { clientConfigDebtCompaniesActions } from '../_redux/reducer';
import { IInitialStateDebtCompanies } from '../types';
const mockActions = mockActionCreator(clientConfigDebtCompaniesActions);

describe('Test UI Delete DebtManageCompany', () => {
  const company = {
    id: '1',
    companyName: 'companyName',
    addressLineOne: 'addressLineOne',
    addressLineTwo: 'addressLineOne',
    companyCity: 'companyCity',
    companyState: {
      description: 'companyState',
      value: 'companyState'
    },
    companyZipCode: 'companyZipCode',
    companyContact: 'companyContact',
    companyPhone: 'companyPhone',
    companyFax: 'companyFax'
  };
  const generateState = (data?: Partial<IInitialStateDebtCompanies>) => {
    return {
      clientConfigurationDebtCompanies: {
        companies: [],
        company,
        isLoading: true,
        isLoadingModalDelete: false,
        isOpenModalDelete: true,
        ...data
      } as IInitialStateDebtCompanies
    } as Partial<RootState>;
  };

  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<DeleteDebtCompany />, { initialState });
  };

  it('Should have render UI', () => {
    renderWrapper(generateState());

    const queryTitle = screen.queryByText(
      I18N_COMMON_TEXT.CONFIRM_DELETE_COMPANY
    );

    const queryTextWarning = screen.queryByText(
      I18N_COMMON_TEXT.TXT_ARE_YOU_WANT_DELETE_COMPANY
    );

    const queryTitleCompanyName = screen.queryByText('txt_company_name:');
    const queryCompanyName = screen.queryByText(company.companyName);

    expect(queryTitle).toBeInTheDocument();
    expect(queryTextWarning).toBeInTheDocument();
    expect(queryTitleCompanyName).toBeInTheDocument();
    expect(queryCompanyName).toBeInTheDocument();
  });

  it('Should have click button cancel', () => {
    const onChangeOpenModalDelete = mockActions('onChangeOpenModalDelete');
    renderWrapper(generateState());

    const queryButtonClose = screen.queryByText(I18N_COMMON_TEXT.CANCEL);
    queryButtonClose!.click();
    expect(onChangeOpenModalDelete).toHaveBeenCalled();
  });

  it('Should have DeleteCompany', () => {
    const deleteCompany = mockActions('deleteCompany');
    renderWrapper(generateState());

    const queryButtonClose = screen.queryByText(I18N_COMMON_TEXT.DELETE);
    queryButtonClose!.click();
    expect(deleteCompany).toHaveBeenCalled();
  });

  it('Should have loading', () => {
    const { wrapper } = renderWrapper(
      generateState({ isLoadingModalDelete: true })
    );

    const queryLoading = queryByClass(
      wrapper.baseElement as HTMLElement,
      /loading/
    );
    expect(queryLoading).toBeInTheDocument();
  });
});
