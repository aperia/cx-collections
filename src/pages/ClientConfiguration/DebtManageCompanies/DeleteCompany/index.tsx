import React from 'react';
import { useDispatch, useSelector } from 'react-redux';

// components
import {
  Button,
  Icon,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import {
  selectorCompanyName,
  selectorLoadingModalDelete,
  selectorOpenModalDelete
} from '../_redux/selectors';
import { clientConfigDebtCompaniesActions as actions } from '../_redux/reducer';

// constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';
import ModalBodyWithErrorClientConfig from 'pages/ClientConfiguration/ApiErrorSection/ModalBodyWithErrorClientConfig';

const DeleteDebtCompany: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const loading = useSelector(selectorLoadingModalDelete);
  const isOpened = useSelector(selectorOpenModalDelete);
  const companyName = useSelector(selectorCompanyName);
  const handleClose = () => {
    dispatch(actions.onChangeOpenModalDelete());
  };
  const testId = 'clientConfig_debtManageCompany_delete';

  const handleDelete = () => {
    dispatch(actions.deleteCompany());
  };

  return (
    <Modal xs loading={loading} show={isOpened} dataTestId={testId}>
      <ModalHeader border dataTestId={`${testId}_header`}>
        <ModalTitle dataTestId={`${testId}_title`}>
          <p>{t(I18N_COMMON_TEXT.CONFIRM_DELETE_COMPANY)}</p>
        </ModalTitle>
        <Button
          onClick={handleClose}
          variant="icon-secondary"
          dataTestId={`${testId}_closeBtn`}
        >
          <Icon name="close" size="5x" />
        </Button>
      </ModalHeader>
      <ModalBodyWithErrorClientConfig
        apiErrorClassName="mb-24"
        dataTestId={`${testId}_body`}
      >
        <p data-testid={genAmtId(testId, 'confirmDeleteMessage', '')}>
          {t(I18N_COMMON_TEXT.TXT_ARE_YOU_WANT_DELETE_COMPANY)}
        </p>
        <p className="mt-16" data-testid={genAmtId(testId, 'companyName', '')}>
          {t('txt_company_name')}:<span className="fw-500"> {companyName}</span>
        </p>
      </ModalBodyWithErrorClientConfig>
      <ModalFooter dataTestId={`${testId}_footer`}>
        <Button
          id="confirm-modal-cancel-btn"
          variant="secondary"
          onClick={handleClose}
          dataTestId={`${testId}_cancelBtn`}
        >
          {t(I18N_COMMON_TEXT.CANCEL)}
        </Button>
        <Button
          id="confirm-modal-confirm-btn"
          variant="danger"
          onClick={handleDelete}
          dataTestId={`${testId}_deleteBtn`}
        >
          {t(I18N_COMMON_TEXT.DELETE)}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default DeleteDebtCompany;
