import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { screen } from '@testing-library/react';
import DebtManageCompany from '.';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { clientConfigDebtCompaniesActions } from './_redux/reducer';
import { IInitialStateDebtCompanies } from './types';
import * as mockStatus from '../hooks/useCheckClientConfigStatus';

jest.mock('./CompaniesGrid.tsx', () => {
  return () => <div>CompaniesGrid</div>;
});

const mockActions = mockActionCreator(clientConfigDebtCompaniesActions);

describe('Test DebtManageCompany', () => {
  afterEach(() => {
    jest.restoreAllMocks();
  });

  const generateState = (data?: Partial<IInitialStateDebtCompanies>) => {
    return {
      clientConfigurationDebtCompanies: {
        companies: [],
        company: {},
        isLoading: true,
        isLoadingModal: false,
        isOpenModal: false,
        ...data
      }
    } as Partial<RootState>;
  };

  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<DebtManageCompany />, { initialState });
  };

  it('Should have useEffect', () => {
    const getCompanies = mockActions('getCompanies');
    const onResetReducer = mockActions('onResetReducer');

    const { wrapper } = renderWrapper(generateState());

    expect(getCompanies).toHaveBeenCalled();

    wrapper.unmount();

    expect(onResetReducer).toHaveBeenCalled();
  });

  it('Should have render no data', () => {
    jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(false);
    renderWrapper(generateState());

    const onChangeOpenModal = mockActions('onChangeOpenModal');

    const queryTitle = screen.queryByText(I18N_COMMON_TEXT.COMPANY_LIST);

    const queryTextNoData = screen.queryByText(
      I18N_COMMON_TEXT.NO_COMPANIES_TO_DISPLAY
    );
    const queryTextAddCompany = screen.queryByText(
      I18N_COMMON_TEXT.ADD_COMPANY
    );

    queryTextAddCompany!.click();

    expect(queryTitle).toBeInTheDocument();
    expect(queryTextNoData).toBeInTheDocument();
    expect(onChangeOpenModal).toHaveBeenCalled();
  });

  it('Should have data', () => {
    renderWrapper(
      generateState({
        companies: [
          {
            companyName: 'companyName',
            addressLineOne: 'addressLineOne',
            addressLineTwo: 'addressLineOne',
            companyCity: 'companyCity',
            companyState: {
              description: 'companyState',
              value: 'companyState'
            },
            companyZipCode: 'companyZipCode',
            companyContact: 'companyContact',
            companyPhone: 'companyPhone',
            companyFax: 'companyFax',
            companyEmail: 'companyEmail',
            active: true
          }
        ]
      })
    );
    const queryText = screen.queryByText('CompaniesGrid');
    expect(queryText).toBeInTheDocument();
  });
});
