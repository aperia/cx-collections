import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';
import { I18N_DEBT_COMPANY_TO_TEXT } from 'app/constants/i18n';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { clientConfigDebtCompaniesActions } from './reducer';
import { IDebtCompany, IInitialStateDebtCompanies } from '../types';
import isEmpty from 'lodash.isempty';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';
import { renewDataForChangeHistory } from '../helpers';

export const deleteCompany = createAsyncThunk<void, undefined, ThunkAPIConfig>(
  'clientConfigDebtCompanies/deleteCompany',
  async (args, thunkAPI) => {
    const { rejectWithValue, dispatch, getState } = thunkAPI;
    const company = getState().clientConfigurationDebtCompanies
      .company as IDebtCompany;

    const clientInfoResponse = await dispatch(
      clientConfigurationActions.getClientInfoData({
        dataType: 'debtMgmtCompanyInfo'
      })
    );
    if (!isFulfilled(clientInfoResponse)) return rejectWithValue({});

    const prevData = clientInfoResponse.payload.debtMgmtCompanyInfo || [];
    const removedCompany = prevData.find(
      c => c.companyName === company.companyName
    );

    if (!removedCompany) return rejectWithValue({});
    removedCompany.active = false;

    const response = await dispatch(
      clientConfigurationActions.updateClientInfo({
        debtMgmtCompanyInfo: prevData,
        updateAction: 'delete'
      })
    );

    if (isFulfilled(response)) {
      dispatch(
        changeHistoryActions.saveChangedClientConfig({
          action: 'DELETE',
          oldValue: renewDataForChangeHistory(company),
          changedItem: { value: company.companyName },
          changedCategory: 'debtManagementCompanies'
        })
      );

      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_DEBT_COMPANY_TO_TEXT.DELETE_DEBT_COMPANY_SUCCESS
        })
      );
      dispatch(clientConfigDebtCompaniesActions.getCompanies());
      return;
    }

    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_DEBT_COMPANY_TO_TEXT.DELETE_DEBT_COMPANY_FAIL
      })
    );
    return rejectWithValue({});
  }
);

export const deleteCompanyBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateDebtCompanies>
) => {
  builder
    .addCase(deleteCompany.pending, draftState => {
      draftState.isLoadingModalDelete = true;
    })
    .addCase(deleteCompany.fulfilled, draftState => {
      draftState.isLoadingModalDelete = false;
      draftState.isOpenModalDelete = false;
      if (!isEmpty(draftState.company)) {
        draftState.company = {};
      }
    })
    .addCase(deleteCompany.rejected, draftState => {
      draftState.isLoadingModalDelete = false;
      draftState.isOpenModalDelete = false;
    });
};
