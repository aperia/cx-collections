import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { IInitialStateDebtCompanies } from '../types';
import { clientConfigDebtCompaniesActions } from './reducer';

describe('Test Reducer DebtManagementCompanies', () => {
  const company = {
    id: '1',
    companyName: 'companyName',
    addressLineOne: 'addressLineOne',
    addressLineTwo: 'addressLineOne',
    companyCity: 'companyCity',
    companyState: {
      description: 'companyState',
      value: 'companyState'
    },
    companyZipCode: 'companyZipCode',
    companyContact: 'companyContact',
    companyPhone: 'companyPhone',
    companyFax: 'companyFax'
  };

  const clientConfigurationDebtCompanies = {
    companies: [],
    company: {},
    isLoading: false,
    isLoadingModalUpdate: false,
    isLoadingModalDelete: false,
    isOpenModalDelete: false,
    isOpenModalUpdate: false
  } as IInitialStateDebtCompanies;

  it('onChangeOpenModalUpdate', () => {
    const store = createStore(rootReducer, {
      clientConfigurationDebtCompanies
    });

    const { getState, dispatch } = store;

    dispatch(clientConfigDebtCompaniesActions.onChangeOpenModal());

    const dataExpect =
      getState().clientConfigurationDebtCompanies.isOpenModalUpdate;

    expect(dataExpect).toEqual(
      !clientConfigurationDebtCompanies.isOpenModalUpdate
    );
  });

  it('onChangeOpenModalUpdate', () => {
    const store = createStore(rootReducer, {
      clientConfigurationDebtCompanies: {
        ...clientConfigurationDebtCompanies,
        isOpenModalUpdate: true,
        company
      }
    });

    const { getState, dispatch } = store;

    dispatch(clientConfigDebtCompaniesActions.onChangeOpenModal());

    const dataExpect = getState().clientConfigurationDebtCompanies.company;

    const isOpenModalExpect =
      getState().clientConfigurationDebtCompanies.isOpenModalUpdate;

    expect(isOpenModalExpect).toEqual(false);
    expect(dataExpect).toEqual({});
  });

  it('onChangeOpenModalDelete', () => {
    const store = createStore(rootReducer, {
      clientConfigurationDebtCompanies
    });

    const { getState, dispatch } = store;

    dispatch(clientConfigDebtCompaniesActions.onChangeOpenModalDelete());

    const dataExpect =
      getState().clientConfigurationDebtCompanies.isOpenModalDelete;

    expect(dataExpect).toEqual(
      !clientConfigurationDebtCompanies.isOpenModalDelete
    );
  });

  it('onChangeOpenModalDelete', () => {
    const store = createStore(rootReducer, {
      clientConfigurationDebtCompanies: {
        ...clientConfigurationDebtCompanies,
        isOpenModalDelete: true,
        company
      }
    });

    const { getState, dispatch } = store;

    dispatch(clientConfigDebtCompaniesActions.onChangeOpenModalDelete());

    const dataExpect = getState().clientConfigurationDebtCompanies.company;

    const isOpenModalExpect =
      getState().clientConfigurationDebtCompanies.isOpenModalDelete;

    expect(isOpenModalExpect).toEqual(false);
    expect(dataExpect).toEqual({});
  });

  it('onChangeCompany', () => {
    const store = createStore(rootReducer, {
      clientConfigurationDebtCompanies
    });
    store.dispatch(
      clientConfigDebtCompaniesActions.onChangeCompany({ company })
    );
    const dataExpect =
      store.getState().clientConfigurationDebtCompanies.company;
    expect(dataExpect).toEqual(company);
  });

  it('onResetReducer', () => {
    const state = {
      companies: [],
      company: {},
      isLoading: false,
      isLoadingModalUpdate: false,
      isLoadingModalDelete: false,
      isOpenModalDelete: false,
      isOpenModalUpdate: false
    } as IInitialStateDebtCompanies;
    const store = createStore(rootReducer, {
      clientConfigurationDebtCompanies
    });
    store.dispatch(clientConfigDebtCompaniesActions.onResetReducer());
    const dataExpect = store.getState().clientConfigurationDebtCompanies;
    expect(dataExpect).toEqual(state);
  });
});
