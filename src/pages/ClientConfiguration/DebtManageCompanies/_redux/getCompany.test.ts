import { createStore } from '@reduxjs/toolkit';
import { clientConfigDebtCompaniesActions } from './reducer';
import clientConfigDebtCompaniesServices from '../debtManageCompaniesService';
import { rootReducer } from 'storeConfig';
import { mockActionCreator } from 'app/test-utils';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { getClientInfoData } from 'pages/ClientConfiguration/_redux/getClientInfoById';

const mockClientConfigurationActions = mockActionCreator(
  clientConfigurationActions
);

describe('Test getCompanies', () => {
  const companies = {
    companyName: 'companyName',
    addressLineOne: 'addressLineOne',
    addressLineTwo: 'addressLineOne',
    companyCity: 'companyCity',
    companyState: 'AK',
    companyZipCode: 'companyZipCode',
    companyContact: 'companyContact',
    companyPhone: 'companyPhone',
    companyFax: 'companyFax',
    companyEmail: 'companyEmail',
    active: true
  };

  const mappingData = {
    companyName: 'companyName',
    companyEmail: 'companyEmail',
    companyState: 'companyState',
    active: 'active'
  };

  const store = createStore(rootReducer);

  const mockGetClientInfoData = (isReject = false, payload = {}) => {
    mockClientConfigurationActions(
      'getClientInfoData',
      jest.fn().mockImplementation(() => ({
        type: isReject
          ? getClientInfoData.rejected.type
          : getClientInfoData.fulfilled.type,
        payload,
        meta: {
          requestId: 'mock-id',
          requestStatus: isReject ? 'rejected' : 'fulfilled'
        }
      }))
    );
  };

  const callApi = (isError = false) => {
    if (!isError) {
      jest
        .spyOn(clientConfigDebtCompaniesServices, 'getState')
        .mockResolvedValue([
          {
            description: 'Alaska',
            value: 'AK',
            countryCode: 'USA'
          }
        ]);
    } else {
      jest
        .spyOn(clientConfigDebtCompaniesServices, 'getState')
        .mockRejectedValue(new Error('Async Error'));
    }
  };

  it('Should have return data > has mapping & debt data', async () => {
    callApi();
    mockGetClientInfoData(false, {
      debtMgmtCompanyInfo: [companies]
    });
    const result = (await clientConfigDebtCompaniesActions.getCompanies()(
      store.dispatch,
      () => {
        return {
          ...store.getState(),
          mapping: {
            data: {
              clientConfigDebtMgmtCompanyInfo: mappingData
            }
          } as MagicKeyValue
        } as RootState;
      },
      {}
    )) as MagicKeyValue;
    expect(result.payload.companies.length).toEqual(1);
  });

  it('Should have return data > doesnt has mapping & debt data', async () => {
    callApi();
    mockGetClientInfoData(false);
    const result = (await clientConfigDebtCompaniesActions.getCompanies()(
      store.dispatch,
      store.getState,
      {}
    )) as MagicKeyValue;
    expect(result.payload.companies.length).toEqual(0);
  });

  it('Should have error', async () => {
    callApi();
    mockGetClientInfoData(true);
    const result = (await clientConfigDebtCompaniesActions.getCompanies()(
      store.dispatch,
      store.getState,
      {}
    )) as MagicKeyValue;
    expect(result.payload).toEqual({});
  });

  it('Should have fulfilled', async () => {
    const pendingAction =
      clientConfigDebtCompaniesActions.getCompanies.fulfilled(
        { companies },
        'clientConfigDebtCompanies/getCompanies',
        undefined
      );

    const actual = rootReducer(
      store.getState(),
      pendingAction
    ).clientConfigurationDebtCompanies;

    expect(actual.companies).toEqual(companies);
    expect(actual.isLoading).toEqual(false);
  });

  it('Should have pending', async () => {
    const pendingAction = clientConfigDebtCompaniesActions.getCompanies.pending(
      'clientConfigDebtCompanies/getCompanies',
      undefined
    );

    const actual = rootReducer(
      store.getState(),
      pendingAction
    ).clientConfigurationDebtCompanies;

    expect(actual.isLoading).toEqual(true);
  });

  it('Should have reject', async () => {
    const pendingAction =
      clientConfigDebtCompaniesActions.getCompanies.rejected(
        null,
        'clientConfigDebtCompanies/getCompanies',
        undefined
      );

    const actual = rootReducer(
      store.getState(),
      pendingAction
    ).clientConfigurationDebtCompanies;

    expect(actual.isLoading).toEqual(false);
  });
});
