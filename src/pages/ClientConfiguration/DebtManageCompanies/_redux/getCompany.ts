import { ActionReducerMapBuilder, createAsyncThunk, isFulfilled } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import clientConfigDebtCompaniesServices from '../debtManageCompaniesService';
import { IDebtCompany, IInitialStateDebtCompanies } from '../types';

export const getCompanies = createAsyncThunk<
  unknown,
  undefined,
  ThunkAPIConfig
>('clientConfigDebtCompanies/getCompanies', async (args, thunkAPI) => {
  const { dispatch, getState, rejectWithValue } = thunkAPI;
  const companyStates = await clientConfigDebtCompaniesServices.getState();
  const response = await dispatch(
    clientConfigurationActions.getClientInfoData({
      dataType: 'debtMgmtCompanyInfo'
    })
  );

  if (isFulfilled(response)) {
    const ruleMappingModel =
      getState()?.mapping?.data?.clientConfigDebtMgmtCompanyInfo || {};

    const data = response.payload.debtMgmtCompanyInfo || [];
    const companies = mappingDataFromArray(data, ruleMappingModel)
      .map(
        (c: any) => ({
          ...c,
          companyState: companyStates.find((s: any) => s.value === c.companyState)
        }) as IDebtCompany
      );

    return { companies };
  }

  return rejectWithValue({});
});

export const getCompaniesBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateDebtCompanies>
) => {
  builder
    .addCase(getCompanies.pending, (draftState, action) => {
      draftState.isLoading = true;
    })
    .addCase(getCompanies.fulfilled, (draftState, action) => {
      const { companies }: any = action.payload;
      draftState.isLoading = false;
      draftState.companies = companies;
    })
    .addCase(getCompanies.rejected, (draftState, action) => {
      draftState.isLoading = false;
    });
};
