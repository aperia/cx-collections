import { selectorWrapper } from 'app/test-utils';
import { IInitialStateDebtCompanies } from '../types';
import * as selectors from './selectors';

describe('Test DebtManageCompanies Selectors', () => {
  const companies = {
    companyName: 'companyName',
    addressLineOne: 'addressLineOne',
    addressLineTwo: 'addressLineOne',
    companyCity: 'companyCity',
    companyState: {
      description: 'companyState',
      value: 'companyState'
    },
    companyZipCode: 'companyZipCode',
    companyContact: 'companyContact',
    companyPhone: 'companyPhone',
    companyFax: 'companyFax',
    companyEmail: 'companyEmail',
    active: true
  };

  const clientConfigurationDebtCompanies = {
    companies: [],
    company: {},
    isLoading: true,
    isLoadingModalUpdate: false,
    isLoadingModalDelete: false,
    isOpenModalDelete: false,
    isOpenModalUpdate: false
  } as IInitialStateDebtCompanies;

  const generateState = (data?: Partial<IInitialStateDebtCompanies>) => {
    return {
      clientConfigurationDebtCompanies: {
        ...clientConfigurationDebtCompanies,
        ...data
      }
    } as Partial<RootState>;
  };

  const defaultState = generateState();

  it('selectorLoadingModalUpdate', () => {
    const { data } = selectorWrapper(defaultState, defaultState)(
      selectors.selectorLoadingModalUpdate
    );
    expect(data).toEqual(clientConfigurationDebtCompanies.isLoadingModalUpdate);
  });

  it('selectorCompanies', () => {
    const { data } = selectorWrapper(generateState({companies: [companies]}), defaultState)(
      selectors.selectorCompanies
    );

    expect(data.length).toEqual(1);
  });

  it('selectorCompany', () => {
    const { data } = selectorWrapper(defaultState, defaultState)(
      selectors.selectorCompany
    );

    expect(data).toEqual(clientConfigurationDebtCompanies.company);
  });

  it('selectorOpenModal', () => {
    const { data } = selectorWrapper(defaultState, defaultState)(
      selectors.selectorOpenModalUpdate
    );

    expect(data).toEqual(clientConfigurationDebtCompanies.isOpenModalUpdate);
  });

  it('selectorOpenModalDelete', () => {
    const { data } = selectorWrapper(defaultState, defaultState)(
      selectors.selectorOpenModalDelete
    );

    expect(data).toEqual(clientConfigurationDebtCompanies.isOpenModalDelete);
  });

  it('selectorLoadingModalDelete', () => {
    const { data } = selectorWrapper(defaultState, defaultState)(
      selectors.selectorLoadingModalDelete
    );

    expect(data).toEqual(clientConfigurationDebtCompanies.isLoadingModalDelete);
  });

  it('selectorRootLoading', () => {
    const { data } = selectorWrapper(defaultState, defaultState)(
      selectors.selectorRootLoading
    );

    expect(data).toEqual(clientConfigurationDebtCompanies.isLoading);
  });

  it('selectorDisplayNoData', () => {
    const { data } = selectorWrapper(defaultState, defaultState)(
      selectors.selectorDisplayNoData
    );

    expect(data).toEqual(true);
  });

  it('selectorDisplayNoData > has data', () => {
    const { data } = selectorWrapper(generateState({companies: [companies]}), defaultState)(
      selectors.selectorDisplayNoData
    );

    expect(data).toEqual(false);
  });

  it('selectorDisplayNoData > has data', () => {
    const { data } = selectorWrapper(generateState({company: companies}), defaultState)(
      selectors.selectorCompanyName
    );

    expect(data).toEqual('companyName');
  });
});
