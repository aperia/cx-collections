import { updateClientInfo } from 'pages/ClientConfiguration/_redux/updateClientInfo';
import { getClientInfoData } from 'pages/ClientConfiguration/_redux/getClientInfoById';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { createStore, Store } from '@reduxjs/toolkit';
import { clientConfigDebtCompaniesActions } from './reducer';
import clientConfigDebtCompaniesServices from '../debtManageCompaniesService';
import { rootReducer } from 'storeConfig';
import { responseDefault, mockActionCreator } from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { I18N_DEBT_COMPANY_TO_TEXT } from 'app/constants/i18n';
import * as handleWithRefOnFind from 'app/helpers/commons';
import { updateCompany } from './updateCompany';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

const mockToastActions = mockActionCreator(actionsToast);
const mockDebtCompanyActions = mockActionCreator(
  clientConfigDebtCompaniesActions
);
const mockClientConfigurationActions = mockActionCreator(
  clientConfigurationActions
);
const mockChangeHistoryActions = mockActionCreator(changeHistoryActions);

describe('Test getCompanies', () => {
  const company = {
    companyName: 'companyName',
    addressLineOne: 'addressLineOne',
    addressLineTwo: 'addressLineOne',
    companyCity: 'companyCity',
    companyState: {
      description: 'companyState',
      value: 'companyState'
    },
    companyZipCode: 'companyZipCode',
    companyContact: 'companyContact',
    companyPhone: 'companyPhone',
    companyFax: 'companyFax',
    companyEmail: 'companyEmail',
    active: true
  };

  let store: Store<RootState>;

  const callApi = ({
    valueCompany = {},
    valuesForm = {},
    isError = false,
    ref = {}
  }) => {
    const IInitialState = {
      form: {
        clientConfigDebtCompaniesForm: {
          values: {
            ...valuesForm
          }
        }
      } as any,
      clientConfigurationDebtCompanies: {
        ...valueCompany
      } as any
    } as Partial<RootState>;
    store = createStore(rootReducer, IInitialState);
    if (!isError) {
      jest
        .spyOn(clientConfigDebtCompaniesServices, 'updateCompany')
        .mockResolvedValue({ ...responseDefault });
    } else {
      jest
        .spyOn(clientConfigDebtCompaniesServices, 'updateCompany')
        .mockRejectedValue(new Error('Async Error'));
    }

    return clientConfigDebtCompaniesActions.updateCompany({ ref } as any)(
      store.dispatch,
      store.getState,
      {}
    );
  };

  const mockGetClientInfoData = (isReject = false, payload = {}) => {
    mockClientConfigurationActions(
      'getClientInfoData',
      jest.fn().mockImplementation(() => ({
        type: isReject
          ? getClientInfoData.rejected.type
          : getClientInfoData.fulfilled.type,
        payload,
        meta: {
          requestId: 'mock-id',
          requestStatus: isReject ? 'rejected' : 'fulfilled'
        }
      }))
    );
  };

  const mockUpdateClientInfo = (isReject = false) => {
    mockClientConfigurationActions(
      'updateClientInfo',
      jest.fn().mockImplementation(() => ({
        type: isReject
          ? updateClientInfo.rejected.type
          : updateClientInfo.fulfilled.type,
        payload: {},
        meta: {
          requestId: 'mock-id',
          requestStatus: isReject ? 'rejected' : 'fulfilled'
        }
      }))
    );
  };

  const mockSaveChangedClientConfig = () => {
    mockChangeHistoryActions(
      'saveChangedClientConfig',
      jest.fn().mockImplementation(() => {
        return {
          type: 'clientConfiguration/saveChangedClientConfig/fulfilled',
          payload: {},
          meta: {
            requestId: 'mock-id',
            requestStatus: 'fulfilled'
          }
        };
      })
    );
  };

  it('Should have call api add with success > debtMgmtCompanyInfo is undefined', async () => {
    const getCompanies = mockDebtCompanyActions('getCompanies');
    const addToast = mockToastActions('addToast');
    const setOthers = jest.fn(x => x());
    const setValue = jest.fn(x => x);
    const ref = jest
      .spyOn(handleWithRefOnFind, 'handleWithRefOnFind')
      .mockImplementation(() => ({ props: { setOthers, setValue } } as any));

    mockGetClientInfoData(false, {
      debtMgmtCompanyInfo: undefined
    });

    mockUpdateClientInfo(false);

    mockSaveChangedClientConfig();

    await callApi({
      valuesForm: { ...company, companyPhone: '' },
      valueCompany: { companies: [] },
      ref
    });

    expect(addToast).toHaveBeenCalledWith({
      show: true,
      type: 'success',
      message: I18N_DEBT_COMPANY_TO_TEXT.ADD_DEBT_COMPANY_SUCCESS
    });
    expect(getCompanies).toHaveBeenCalled();
  });

  it('Should have call api add with success', async () => {
    const getCompanies = mockDebtCompanyActions('getCompanies');
    const addToast = mockToastActions('addToast');
    const setOthers = jest.fn(x => x());
    const setValue = jest.fn(x => x);
    const ref = jest
      .spyOn(handleWithRefOnFind, 'handleWithRefOnFind')
      .mockImplementation(() => ({ props: { setOthers, setValue } } as any));

    mockGetClientInfoData(false, {
      debtMgmtCompanyInfo: [
        { ...company, active: false },
        { ...company, companyName: 'companyName2' }
      ]
    });

    mockUpdateClientInfo(false);

    mockSaveChangedClientConfig();

    await callApi({
      valuesForm: { ...company, companyPhone: '' },
      valueCompany: { companies: [] },
      ref
    });

    expect(addToast).toHaveBeenCalledWith({
      show: true,
      type: 'success',
      message: I18N_DEBT_COMPANY_TO_TEXT.ADD_DEBT_COMPANY_SUCCESS
    });
    expect(getCompanies).toHaveBeenCalled();
  });

  it('Should have call api add with failed', async () => {
    const getCompanies = mockDebtCompanyActions('getCompanies');
    const addToast = mockToastActions('addToast');
    const setOthers = jest.fn(x => x());
    const setValue = jest.fn(x => x);
    const ref = jest
      .spyOn(handleWithRefOnFind, 'handleWithRefOnFind')
      .mockImplementation(() => ({ props: { setOthers, setValue } } as any));

    mockGetClientInfoData(false, {
      debtMgmtCompanyInfo: [{ ...company }]
    });

    mockUpdateClientInfo(true);

    await callApi({
      valuesForm: { ...company, companyPhone: '' },
      valueCompany: { companies: [] },
      ref
    });

    expect(addToast).toHaveBeenCalledWith({
      show: true,
      type: 'error',
      message: I18N_DEBT_COMPANY_TO_TEXT.ADD_DEBT_COMPANY_FAIL
    });
    expect(getCompanies).not.toHaveBeenCalled();
  });

  it('Should have call api update with success ', async () => {
    const getCompanies = mockDebtCompanyActions('getCompanies');
    const addToast = mockToastActions('addToast');
    const setOthers = jest.fn(x => x());
    const setValue = jest.fn(x => x);
    const ref = jest
      .spyOn(handleWithRefOnFind, 'handleWithRefOnFind')
      .mockImplementation(() => ({ props: { setOthers, setValue } } as any));

    mockGetClientInfoData(false, {
      debtMgmtCompanyInfo: [{ ...company }]
    });

    mockUpdateClientInfo(false);

    mockSaveChangedClientConfig();

    await callApi({
      valuesForm: { ...company },
      valueCompany: { companies: [company], company },
      ref
    });

    expect(getCompanies).toHaveBeenCalled();
    expect(addToast).toHaveBeenCalledWith({
      show: true,
      type: 'success',
      message: I18N_DEBT_COMPANY_TO_TEXT.UPDATE_DEBT_COMPANY_SUCCESS
    });
  });

  it('Should have call api update with failed', async () => {
    const getCompanies = mockDebtCompanyActions('getCompanies');
    const addToast = mockToastActions('addToast');
    const setOthers = jest.fn(x => x());
    const setValue = jest.fn(x => x);
    const ref = jest
      .spyOn(handleWithRefOnFind, 'handleWithRefOnFind')
      .mockImplementation(() => ({ props: { setOthers, setValue } } as any));

    mockGetClientInfoData(false, {
      debtMgmtCompanyInfo: [{ ...company }]
    });

    mockUpdateClientInfo(true);

    await callApi({
      valuesForm: { ...company },
      valueCompany: { companies: [company], company },
      ref
    });

    expect(getCompanies).not.toHaveBeenCalled();
    expect(addToast).toHaveBeenCalledWith({
      show: true,
      type: 'error',
      message: I18N_DEBT_COMPANY_TO_TEXT.UPDATE_DEBT_COMPANY_FAIL
    });
  });

  it('Should have call api update failed when retry api get companies', async () => {
    const setOthers = jest.fn(x => x());
    const setValue = jest.fn(x => x);
    const ref = jest
      .spyOn(handleWithRefOnFind, 'handleWithRefOnFind')
      .mockImplementation(() => ({ props: { setOthers, setValue } } as any));

    mockGetClientInfoData(true);

    mockUpdateClientInfo(false);

    const result = await callApi({
      valuesForm: { ...company },
      valueCompany: { companies: [company], company },
      ref
    });

    expect(result.type).toEqual(updateCompany.rejected.type);
  });

  it('Should have call api update with success with some fields empty data', async () => {
    const dummyCompany = {
      ...company,
      addressLineTwo: '',
      companyContact: '',
      companyFax: '',
      companyEmail: ''
    };
    const addToast = mockToastActions('addToast');
    const setOthers = jest.fn(x => x());
    const setValue = jest.fn(x => x);
    const ref = jest
      .spyOn(handleWithRefOnFind, 'handleWithRefOnFind')
      .mockImplementation(() => ({ props: { setOthers, setValue } } as any));

    mockGetClientInfoData(false, {
      debtMgmtCompanyInfo: [dummyCompany]
    });

    mockUpdateClientInfo(false);

    mockSaveChangedClientConfig();

    await callApi({
      valuesForm: dummyCompany,
      valueCompany: {
        companies: [dummyCompany],
        company: dummyCompany
      },
      ref
    });

    expect(addToast).toHaveBeenCalledWith({
      show: true,
      type: 'success',
      message: I18N_DEBT_COMPANY_TO_TEXT.UPDATE_DEBT_COMPANY_SUCCESS
    });
  });

  it('Should have call condition is false with tax and phone is empty', async () => {
    const setOthers = jest.fn(x => x());
    const setValue = jest.fn(x => x);
    const ref = jest
      .spyOn(handleWithRefOnFind, 'handleWithRefOnFind')
      .mockImplementation(() => ({ props: { setOthers, setValue } } as any));

    const response: any = await callApi({
      valuesForm: { companyFax: '', companyPhone: '' },
      valueCompany: { companies: [] },
      ref
    });
    expect(response.meta?.condition).toEqual(true);
  });

  it('Should have call condition is false with company exist', async () => {
    const setOthers = jest.fn(x => x());
    const setValue = jest.fn(x => x);
    const ref = jest
      .spyOn(handleWithRefOnFind, 'handleWithRefOnFind')
      .mockImplementation(() => ({ props: { setOthers, setValue } } as any));

    const response: any = await callApi({
      valuesForm: { ...company, companyName: 'companyName2' },
      valueCompany: {
        companies: [company, { ...company, companyName: 'companyName2' }],
        company
      },
      ref
    });
    expect(response.meta?.condition).toEqual(true);
  });

  it('Should have fulfilled', async () => {
    const pendingAction =
      clientConfigDebtCompaniesActions.updateCompany.fulfilled(
        undefined,
        'clientConfigDebtCompanies/updateCompany',
        { ref: {} as any }
      );

    const actual = rootReducer(
      store.getState(),
      pendingAction
    ).clientConfigurationDebtCompanies;

    expect(actual.isOpenModalUpdate).toEqual(false);
    expect(actual.isLoadingModalUpdate).toEqual(false);
  });

  it('Should have pending', async () => {
    const pendingAction =
      clientConfigDebtCompaniesActions.updateCompany.pending(
        'clientConfigDebtCompanies/updateCompany',
        { ref: {} as any }
      );

    const actual = rootReducer(
      store.getState(),
      pendingAction
    ).clientConfigurationDebtCompanies;

    expect(actual.isLoadingModalUpdate).toEqual(true);
  });

  it('Should have reject', async () => {
    const pendingAction =
      clientConfigDebtCompaniesActions.updateCompany.rejected(
        null,
        'clientConfigDebtCompanies/updateCompany',
        { ref: {} as any }
      );

    const actual = rootReducer(
      store.getState(),
      pendingAction
    ).clientConfigurationDebtCompanies;

    expect(actual.isLoadingModalUpdate).toEqual(false);
  });
});
