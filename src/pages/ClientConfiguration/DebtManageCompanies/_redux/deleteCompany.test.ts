import { createStore } from '@reduxjs/toolkit';
import { clientConfigDebtCompaniesActions } from './reducer';
import { rootReducer } from 'storeConfig';
import { mockActionCreator } from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { I18N_DEBT_COMPANY_TO_TEXT } from 'app/constants/i18n';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { getClientInfoData } from 'pages/ClientConfiguration/_redux/getClientInfoById';
import { IInitialStateDebtCompanies } from '../types';
import { updateClientInfo } from 'pages/ClientConfiguration/_redux/updateClientInfo';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

const mockToastActions = mockActionCreator(actionsToast);
const mockDebtCompanyActions = mockActionCreator(
  clientConfigDebtCompaniesActions
);
const mockClientConfigurationActions = mockActionCreator(
  clientConfigurationActions
);
const mockChangeHistoryActions = mockActionCreator(changeHistoryActions);

describe('Test deleteCompany', () => {
  const company = {
    companyName: 'companyName',
    addressLineOne: 'addressLineOne',
    addressLineTwo: 'addressLineOne',
    companyCity: 'companyCity',
    companyState: {
      description: 'companyState',
      value: 'companyState'
    },
    companyZipCode: 'companyZipCode',
    companyContact: 'companyContact',
    companyPhone: 'companyPhone',
    companyFax: 'companyFax',
    companyEmail: 'companyEmail',
    active: true
  };

  const initialState: Partial<RootState> = {
    clientConfigurationDebtCompanies: {
      company
    }
  };

  const store = createStore(rootReducer, initialState);

  const mockGetClientInfoData = (isReject = false, payload = {}) => {
    mockClientConfigurationActions(
      'getClientInfoData',
      jest.fn().mockImplementation(() => ({
        type: isReject
          ? getClientInfoData.rejected.type
          : getClientInfoData.fulfilled.type,
        payload,
        meta: {
          requestId: 'mock-id',
          requestStatus: isReject ? 'rejected' : 'fulfilled'
        }
      }))
    );
  };

  const mockUpdateClientInfo = (isReject = false) => {
    mockClientConfigurationActions(
      'updateClientInfo',
      jest.fn().mockImplementation(() => ({
        type: isReject
          ? updateClientInfo.rejected.type
          : updateClientInfo.fulfilled.type,
        payload: {},
        meta: {
          requestId: 'mock-id',
          requestStatus: isReject ? 'rejected' : 'fulfilled'
        }
      }))
    );
  };

  const mockSaveChangedClientConfig = () => {
    mockChangeHistoryActions(
      'saveChangedClientConfig',
      jest.fn().mockImplementation(() => {
        return {
          type: 'clientConfiguration/saveChangedClientConfig/fulfilled',
          payload: {},
          meta: {
            requestId: 'mock-id',
            requestStatus: 'fulfilled'
          }
        };
      })
    );
  };

  it('Should call deleteCompany > dont have removedCompany', async () => {
    mockGetClientInfoData(true);
    let result = (await clientConfigDebtCompaniesActions.deleteCompany()(
      store.dispatch,
      () => {
        return {
          ...store.getState(),
          clientConfigurationDebtCompanies: {
            company
          } as IInitialStateDebtCompanies
        };
      },
      {}
    )) as MagicKeyValue;
    expect(result.payload).toEqual({});

    mockGetClientInfoData(false);
    result = await clientConfigDebtCompaniesActions.deleteCompany()(
      store.dispatch,
      () => {
        return {
          ...store.getState(),
          clientConfigurationDebtCompanies: {
            company
          } as IInitialStateDebtCompanies
        };
      },
      {}
    );
    expect(result.payload).toEqual({});
  });

  it('Should call deleteCompany > have removedCompany', async () => {
    const addToast = mockToastActions('addToast');
    const getCompaniesMock = mockDebtCompanyActions('getCompanies');

    mockGetClientInfoData(false, {
      debtMgmtCompanyInfo: [{ ...company }]
    });

    mockUpdateClientInfo(false);

    mockSaveChangedClientConfig();

    await clientConfigDebtCompaniesActions.deleteCompany()(
      store.dispatch,
      () => {
        return {
          ...store.getState(),
          clientConfigurationDebtCompanies: {
            company
          } as IInitialStateDebtCompanies
        };
      },
      {}
    );

    expect(addToast).toHaveBeenCalledWith({
      show: true,
      type: 'success',
      message: I18N_DEBT_COMPANY_TO_TEXT.DELETE_DEBT_COMPANY_SUCCESS
    });
    expect(getCompaniesMock).toHaveBeenCalled();
  });

  it('Should call deleteCompany > update not success', async () => {
    const addToast = mockToastActions('addToast');

    mockGetClientInfoData(false, {
      debtMgmtCompanyInfo: [{ ...company }]
    });

    mockUpdateClientInfo(true);

    await clientConfigDebtCompaniesActions.deleteCompany()(
      store.dispatch,
      () => {
        return {
          ...store.getState(),
          clientConfigurationDebtCompanies: {
            company
          } as IInitialStateDebtCompanies
        };
      },
      {}
    );

    expect(addToast).toHaveBeenCalledWith({
      show: true,
      type: 'error',
      message: I18N_DEBT_COMPANY_TO_TEXT.DELETE_DEBT_COMPANY_FAIL
    });
  });

  it('Should have fulfilled', async () => {
    const pendingAction =
      clientConfigDebtCompaniesActions.deleteCompany.fulfilled(
        undefined,
        'clientConfigDebtCompanies/deleteCompany',
        undefined
      );

    const actual = rootReducer(
      store.getState(),
      pendingAction
    ).clientConfigurationDebtCompanies;

    expect(actual.isOpenModalDelete).toEqual(false);
    expect(actual.isLoadingModalDelete).toEqual(false);
  });

  it('Should have pending', async () => {
    const pendingAction =
      clientConfigDebtCompaniesActions.deleteCompany.pending(
        'clientConfigDebtCompanies/deleteCompany',
        undefined
      );

    const actual = rootReducer(
      store.getState(),
      pendingAction
    ).clientConfigurationDebtCompanies;

    expect(actual.isLoadingModalDelete).toEqual(true);
  });

  it('Should have reject', async () => {
    const pendingAction =
      clientConfigDebtCompaniesActions.deleteCompany.rejected(
        null,
        'clientConfigDebtCompanies/deleteCompany',
        undefined
      );

    const actual = rootReducer(
      store.getState(),
      pendingAction
    ).clientConfigurationDebtCompanies;

    expect(actual.isLoadingModalDelete).toEqual(false);
  });
});
