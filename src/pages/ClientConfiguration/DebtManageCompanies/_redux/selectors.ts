import { createSelector } from '@reduxjs/toolkit';
import { IDebtCompany } from '../types';

export const selectorCompanies = createSelector(
  (state: RootState) =>
    state.clientConfigurationDebtCompanies.companies?.filter(c => c.active),
  (data: IDebtCompany[]) => data
);

export const selectorOpenModalUpdate = createSelector(
  (state: RootState) =>
    state.clientConfigurationDebtCompanies.isOpenModalUpdate,
  (data: boolean) => data
);

export const selectorLoadingModalUpdate = createSelector(
  (state: RootState) =>
    state.clientConfigurationDebtCompanies.isLoadingModalUpdate,
  (data: boolean) => data
);

export const selectorRootLoading = createSelector(
  (state: RootState) => state.clientConfigurationDebtCompanies.isLoading,
  (data: boolean) => data
);

export const selectorCompany = createSelector(
  (state: RootState) =>
    state.clientConfigurationDebtCompanies.company as IDebtCompany,
  (data: IDebtCompany) => data
);

export const selectorLoadingModalDelete = createSelector(
  (state: RootState) =>
    state.clientConfigurationDebtCompanies.isLoadingModalDelete,
  (data: boolean) => data
);

export const selectorDisplayNoData = createSelector(
  (state: RootState) => state.clientConfigurationDebtCompanies.companies?.filter(c => c.active),
  (data: IDebtCompany[]) => (!data || !data.length ? true : false)
);

export const selectorCompanyName = createSelector(
  (state: RootState) =>
    state.clientConfigurationDebtCompanies.company as IDebtCompany,
  (data: IDebtCompany) => data.companyName
);

export const selectorOpenModalDelete = createSelector(
  (state: RootState) =>
    state.clientConfigurationDebtCompanies.isOpenModalDelete,
  (data: boolean) => data
);
