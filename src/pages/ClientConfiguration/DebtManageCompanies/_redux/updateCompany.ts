import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';
import {
  I18N_COMMON_TEXT,
  I18N_DEBT_COMPANY_TO_TEXT
} from 'app/constants/i18n';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { clientConfigDebtCompaniesActions } from './reducer';
import {
  IDebtCompany,
  IInitialStateDebtCompanies,
  IArgsUpdateDebtCompany,
  IValueDebtCompanyFrom
} from '../types';
import { handleWithRefOnFind as onFind } from 'app/helpers';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { DebtManagementCompany } from 'pages/ClientConfiguration/types';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';
import { batch } from 'react-redux';
import { renewDataForChangeHistory } from '../helpers';

const dataField = {
  NAME: 'companyName',
  ADDRESS: 'addressLineOne',
  CITY: 'companyCity',
  STATE: 'companyState',
  ZIP: 'companyZipCode',
  PHONE: 'companyPhone',
  FAX: 'companyFax',
  ERROR_EXIST: 'companyErrorExist',
  ERROR: 'companyError'
};

export const updateCompany = createAsyncThunk<
  unknown,
  IArgsUpdateDebtCompany,
  ThunkAPIConfig
>(
  'clientConfigDebtCompanies/updateCompany',
  async (args, thunkAPI) => {
    const {
      ADD_DEBT_COMPANY_FAIL,
      ADD_DEBT_COMPANY_SUCCESS,
      UPDATE_DEBT_COMPANY_FAIL,
      UPDATE_DEBT_COMPANY_SUCCESS
    } = I18N_DEBT_COMPANY_TO_TEXT;
    const { rejectWithValue, dispatch, getState } = thunkAPI;
    const { form, clientConfigurationDebtCompanies } = getState();
    const editedCompany =
      clientConfigurationDebtCompanies.company as IDebtCompany;
    const isAdd = isEmpty(editedCompany);
    const values = form['clientConfigDebtCompaniesForm']
      ?.values as IValueDebtCompanyFrom;

    // DO NOT remove undefined if you want pass this UT coverage
    const nextValue = {
      companyAddressLine1: values?.addressLineOne,
      companyAddressLine2: values?.addressLineTwo || undefined,
      companyCity: values?.companyCity,
      companyContact: values?.companyContact || undefined,
      companyFax: values?.companyFax || undefined,
      companyName: values?.companyName,
      companyPhone: values?.companyPhone || undefined,
      companyZip: values?.companyZipCode,
      companyState: values?.companyState?.value,
      active: values?.active
    } as DebtManagementCompany;

    const clientInfoResponse = await dispatch(
      clientConfigurationActions.getClientInfoData({
        dataType: 'debtMgmtCompanyInfo'
      })
    );
    if (!isFulfilled(clientInfoResponse)) return rejectWithValue({});

    const editedCompare = editedCompany?.companyName?.trim();
    const nextCompare = nextValue?.companyName?.trim();
    const prevData = (
      clientInfoResponse.payload.debtMgmtCompanyInfo || []
    ).filter(c => {
      const currentCompare = c?.companyName?.trim();

      if (isAdd && !c.active && currentCompare === nextCompare) return false;

      if (!isAdd && currentCompare === editedCompare) return false;

      return true;
    });

    const response = await dispatch(
      clientConfigurationActions.updateClientInfo({
        debtMgmtCompanyInfo: [...prevData, nextValue]
      })
    );

    if (isFulfilled(response)) {
      batch(() => {
        dispatch(
          changeHistoryActions.saveChangedClientConfig({
            action: isAdd ? 'ADD' : 'UPDATE',
            oldValue: isAdd
              ? undefined
              : renewDataForChangeHistory(editedCompany),
            newValue: renewDataForChangeHistory(values),
            changedItem: {
              value: isAdd
                ? values.companyName
                : renewDataForChangeHistory(editedCompany).companyName
            },
            changedCategory: 'debtManagementCompanies'
          })
        );
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: isAdd
              ? ADD_DEBT_COMPANY_SUCCESS
              : UPDATE_DEBT_COMPANY_SUCCESS
          })
        );
        dispatch(clientConfigDebtCompaniesActions.getCompanies());
      });

      return;
    }
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: isAdd ? ADD_DEBT_COMPANY_FAIL : UPDATE_DEBT_COMPANY_FAIL
      })
    );
    return rejectWithValue({});
  },
  {
    // Condition of redux toolkit prevent call api if this condition return false
    condition: (args, thunkAPI) => {
      const { ENTER_AT_LEAST_ONE_FIELD, COMPANY_EXIST } = I18N_COMMON_TEXT;
      const { getState } = thunkAPI;
      const { ref } = args;
      const { form, clientConfigurationDebtCompanies } = getState();
      const companies = clientConfigurationDebtCompanies.companies.filter(
        c => c.active
      );
      const selectedCompany =
        clientConfigurationDebtCompanies.company as IDebtCompany;
      const values = form['clientConfigDebtCompaniesForm']
        .values as IValueDebtCompanyFrom;

      let isErrorExistCompany = false;
      let isErrorPhoneTax = false;
      let textExist = undefined;
      let textError = undefined;
      let isReturn = true;
      let dataForFilter: IDebtCompany[] = companies;

      if (!isEmpty(selectedCompany)) {
        const compareValueCompany = selectedCompany?.companyName?.trim();
        dataForFilter = companies.filter(item => {
          const combineDataFilter = item?.companyName?.trim();
          return combineDataFilter !== compareValueCompany;
        });
      }

      const compareValue = values?.companyName?.trim();
      const findDataExist = dataForFilter.find(item => {
        const combineDataFind = item?.companyName?.trim();
        return compareValue === combineDataFind;
      });

      if (findDataExist) {
        textExist = COMPANY_EXIST;
        isErrorExistCompany = true;
        isReturn = false;
      }

      if (!values?.companyPhone && !values?.companyFax) {
        textError = ENTER_AT_LEAST_ONE_FIELD;
        isReturn = false;
        isErrorPhoneTax = true;
      }

      const { ERROR_EXIST, NAME, PHONE, FAX, ERROR } = dataField;

      const refCompanyError = onFind(ref, ERROR);
      const refExist = onFind(ref, ERROR_EXIST);
      const refName = onFind(ref, NAME);
      const refPhone = onFind(ref, PHONE);
      const refTax = onFind(ref, FAX);

      const refsPhoneTax = [refPhone, refTax];

      refsPhoneTax.forEach(item =>
        item.props.setOthers((pre: MagicKeyValue) => ({
          ...pre,
          options: { isError: isErrorPhoneTax }
        }))
      );

      refName.props.setOthers((pre: MagicKeyValue) => ({
        ...pre,
        options: { isError: isErrorExistCompany }
      }));
      refExist.props.setValue(textExist);

      refCompanyError.props.setValue(textError);

      return isReturn;
    }
  }
);

export const updateCompanyBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateDebtCompanies>
) => {
  builder
    .addCase(updateCompany.pending, (draftState, action) => {
      draftState.isLoadingModalUpdate = true;
    })
    .addCase(updateCompany.fulfilled, (draftState, action) => {
      draftState.isLoadingModalUpdate = false;
      draftState.isOpenModalUpdate = false;
      if (!isEmpty(draftState.company)) {
        draftState.company = {};
      }
    })
    .addCase(updateCompany.rejected, (draftState, action) => {
      draftState.isLoadingModalUpdate = false;
    });
};
