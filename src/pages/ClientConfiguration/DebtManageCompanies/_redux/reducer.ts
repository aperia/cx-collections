import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { getCompanies, getCompaniesBuilder } from './getCompany';
import { deleteCompany, deleteCompanyBuilder } from './deleteCompany';
import { updateCompany, updateCompanyBuilder } from './updateCompany';
import { IDebtCompany, IInitialStateDebtCompanies } from '../types';
import { isEmpty } from 'app/_libraries/_dls/lodash';

const initialState: IInitialStateDebtCompanies = {
  companies: [],
  company: {},
  isLoading: false,
  isLoadingModalUpdate: false,
  isLoadingModalDelete: false,
  isOpenModalUpdate: false,
  isOpenModalDelete: false
};

const { actions, reducer } = createSlice({
  name: 'clientConfigDebtCompanies',
  initialState,
  reducers: {
    onChangeOpenModal: draftState => {
      draftState.isOpenModalUpdate = !draftState.isOpenModalUpdate;
      if (!draftState.isOpenModalUpdate && !isEmpty(draftState.company)) {
        draftState.company = {};
      }
    },
    onChangeOpenModalDelete: draftState => {
      draftState.isOpenModalDelete = !draftState.isOpenModalDelete;
      if (!draftState.isOpenModalDelete && !isEmpty(draftState.company)) {
        draftState.company = {};
      }
    },
    onChangeCompany: (
      draftState,
      action: PayloadAction<{ company: IDebtCompany }>
    ) => {
      draftState.company = action.payload.company;
    },
    onResetReducer: () => {
      return { ...initialState };
    }
  },
  extraReducers: builder => {
    getCompaniesBuilder(builder);
    deleteCompanyBuilder(builder);
    updateCompanyBuilder(builder);
  }
});

const allActions = {
  ...actions,
  updateCompany,
  deleteCompany,
  getCompanies
};

export { allActions as clientConfigDebtCompaniesActions, reducer };
