import { ExtraFieldProps } from 'app/_libraries/_dof/core';
import { MutableRefObject } from 'react';

export interface IDebtCompany {
  companyName: string;
  addressLineOne: string;
  addressLineTwo: string;
  companyCity: string;
  companyState: RefDataValue;
  companyZipCode: string;
  companyContact: string;
  companyPhone: string;
  companyFax: string;
  active: boolean;
}
export interface IValueDebtCompanyFrom extends IDebtCompany {
  companyError?: string;
  companyErrorExist?: string;
}
export interface IInitialStateDebtCompanies {
  companies: IDebtCompany[];
  company: IDebtCompany | {};
  isLoading: boolean;
  isLoadingModalUpdate: boolean;
  isLoadingModalDelete: boolean;
  isOpenModalDelete: boolean;
  isOpenModalUpdate: boolean;
}
export interface IArgsUpdateDebtCompany {
  ref: MutableRefObject<ExtraFieldProps>;
}
export interface IRequestUpdateDebtCompany {
  idCompany?: string;
  company: IDebtCompany;
}
