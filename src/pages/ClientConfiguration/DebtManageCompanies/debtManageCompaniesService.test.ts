import axios from 'axios';
// services
import debtManageCompaniesService from './debtManageCompaniesService';

// utils
import { mockApiServices, storeId } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import { IRequestUpdateDebtCompany } from './types';

describe('debtManageCompaniesService', () => {
  describe('getCompany', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      debtManageCompaniesService.getCompany();

      expect(mockService).toBeCalledWith('', {});
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      debtManageCompaniesService.getCompany();

      expect(mockService).toBeCalledWith(
        apiUrl.debtManagementCompanies.getCompany,
        {}
      );
    });
  });

  describe('deleteCompany', () => {
    const params: string = storeId;

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      debtManageCompaniesService.deleteCompany(params);

      expect(mockService).toBeCalledWith('', { id: params });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      debtManageCompaniesService.deleteCompany(params);

      expect(mockService).toBeCalledWith(
        apiUrl.debtManagementCompanies.deleteCompany,
        { id: params }
      );
    });
  });

  describe('updateCompany', () => {
    const params: IRequestUpdateDebtCompany = {
      company: {
        companyName: 'companyName',
        addressLineOne: '333 MENLAY',
        addressLineTwo: '',
        companyCity: '',
        companyState: { value: 'value', description: 'description' },
        companyZipCode: '',
        companyContact: '',
        companyPhone: '',
        companyFax: '',
        companyEmail: '',
        active: false
      }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      debtManageCompaniesService.updateCompany(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      debtManageCompaniesService.updateCompany(params);

      expect(mockService).toBeCalledWith(
        apiUrl.debtManagementCompanies.updateCompany,
        params
      );
    });
  });

  it('getStateRefData', async () => {
    const mockAxiosGet = jest.spyOn(axios, 'get').mockResolvedValue({});
    await debtManageCompaniesService.getState();

    expect(mockAxiosGet).toBeCalledWith('refData/state.json');
  });
});
