import React, { useEffect } from 'react';

// component
import CompaniesGrid from './CompaniesGrid';
import { SimpleBar, Button } from 'app/_libraries/_dls/components';
import { NoDataGrid } from 'app/components';
import Form from './Form';
import Title from './Title';
import DeleteDebtCompany from './DeleteCompany';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { selectorDisplayNoData } from './_redux/selectors';

import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { clientConfigDebtCompaniesActions as actions } from './_redux/reducer';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';

const DebtManageCompany: React.FC = () => {
  const dispatch = useDispatch();

  const { t } = useTranslation();

  const isDisplayNoData = useSelector(selectorDisplayNoData);

  const testId = 'clientConfig_debtManageCompany';

  const status = useCheckClientConfigStatus();

  useEffect(() => {
    dispatch(actions.getCompanies());
    return () => {
      dispatch(actions.onResetReducer());
    };
  }, [dispatch]);

  const handleOpenModal = () => {
    dispatch(actions.onChangeOpenModal());
  };

  const renderView = () => {
    if (!isDisplayNoData) {
      return <CompaniesGrid status={status} />;
    }
    return (
      <NoDataGrid
        text={I18N_COMMON_TEXT.NO_COMPANIES_TO_DISPLAY}
        dataTestId={`${testId}_noData`}
      >
        <Button
          disabled={status}
          onClick={handleOpenModal}
          className="mt-16"
          variant="outline-primary"
          size="sm"
          dataTestId={`${testId}_addCompanyBtn`}
        >
          {t(I18N_COMMON_TEXT.ADD_COMPANY)}
        </Button>
      </NoDataGrid>
    );
  };

  return (
    <div className={'position-relative'}>
      <SimpleBar>
        <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
          <Title status={status} />
          {renderView()}
        </div>
      </SimpleBar>
      <Form />
      <DeleteDebtCompany />
    </div>
  );
};

export default DebtManageCompany;
