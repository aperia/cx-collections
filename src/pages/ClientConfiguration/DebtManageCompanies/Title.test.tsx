import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { screen } from '@testing-library/react';
import Title from './Title';
import {
  I18N_COMMON_TEXT,
  I18N_DEBT_COMPANY_TO_TEXT
} from 'app/constants/i18n';
import { clientConfigDebtCompaniesActions } from './_redux/reducer';
import { IInitialStateDebtCompanies } from './types';

const mockActions = mockActionCreator(clientConfigDebtCompaniesActions);

describe('Test title of DebtManageCompany', () => {
  afterEach(() => {
    jest.restoreAllMocks();
  });

  const generateState = (data?: Partial<IInitialStateDebtCompanies>) => {
    return {
      clientConfigurationDebtCompanies: {
        companies: [],
        company: {},
        isLoading: true,
        isLoadingModal: false,
        isOpenModal: false,
        ...data
      }
    } as Partial<RootState>;
  };

  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<Title />, { initialState });
  };

  it('Should have render isDisplayNoData ', () => {
    renderWrapper(generateState());

    const queryTitle = screen.queryByText(
      I18N_DEBT_COMPANY_TO_TEXT.DEBT_MANAGEMENT_COMPANIES
    );

    const queryText = screen.queryByText(I18N_COMMON_TEXT.COMPANY_LIST);
    const queryTextAddCompany = screen.queryByText(
      I18N_COMMON_TEXT.ADD_COMPANY
    );

    expect(queryTitle).toBeInTheDocument();
    expect(queryText).toBeInTheDocument();
    expect(queryTextAddCompany).not.toBeInTheDocument();
  });

  it('Should have render !isDisplayNoData', () => {
    const onChangeOpenModal = mockActions('onChangeOpenModal');
    renderWrapper(
      generateState({
        companies: [
          {
            companyName: 'companyName',
            addressLineOne: 'addressLineOne',
            addressLineTwo: 'addressLineOne',
            companyCity: 'companyCity',
            companyState: {
              description: 'companyState',
              value: 'companyState'
            },
            companyZipCode: 'companyZipCode',
            companyContact: 'companyContact',
            companyPhone: 'companyPhone',
            companyFax: 'companyFax',
            companyEmail: 'companyEmail',
            active: true
          }
        ]
      })
    );

    const queryTitle = screen.queryByText(
      I18N_DEBT_COMPANY_TO_TEXT.DEBT_MANAGEMENT_COMPANIES
    );

    const queryText = screen.queryByText(I18N_COMMON_TEXT.COMPANY_LIST);
    const queryTextAddCompany = screen.queryByText(
      I18N_COMMON_TEXT.ADD_COMPANY
    );

    queryTextAddCompany!.click();
    expect(queryTitle).toBeInTheDocument();
    expect(queryText).toBeInTheDocument();
    expect(onChangeOpenModal).toHaveBeenCalled();
  });
});
