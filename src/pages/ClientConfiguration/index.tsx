import React from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useDispatch } from 'react-redux';
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';

// constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';

const ClientConfigurationNavigation: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const handleClientConfigurationTab = () => {
    dispatch(
      actionsTab.addTab({
        id: 'client-configuration',
        title: I18N_COMMON_TEXT.CLIENT_CONFIGURATION,
        storeId: 'client-configuration',
        tabType: 'clientConfiguration',
        iconName: 'file'
      })
    );
  };

  return (
    <span
      className="link-header"
      onClick={handleClientConfigurationTab}
      data-testid={genAmtId('header_client-configuration', 'title', '')}
    >
      {t(I18N_COMMON_TEXT.CLIENT_CONFIGURATION)}
    </span>
  );
};

export default ClientConfigurationNavigation;
