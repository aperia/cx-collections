import React, { useEffect, useRef } from 'react';
import isEmpty from 'lodash.isempty';

// constant
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { usePagination, useStoreIdSelector } from 'app/hooks';

// components
import {
  ColumnType,
  GridRef,
  Grid as GridDLS,
  Pagination,
  SortType,
  TruncateText
} from 'app/_libraries/_dls/components';
import { Actions } from './Actions';
import AddActionCode from './AddActionCode';

// hooks
import { useScreenType, useTranslation } from 'app/_libraries/_dls/hooks';
import { ScreenType } from 'app/_libraries/_dls/hooks/useScreenType/types';

// types
import { CallResult } from './types';
import { ConfigData } from '../types';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { selectConfig } from '../_redux/selectors';
import { clientConfigCallResultActions } from './_redux/reducers';
import { getSortBy, selectGridData } from './_redux/selector';
import { NoDataGrid } from 'app/components';
import { PAGE_SIZE_COMMON } from 'app/constants';
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';
import { genAmtId } from 'app/_libraries/_dls/utils';

const Grid = () => {
  const gridRefs = useRef<GridRef | null>(null);
  const testId = 'callResult_grid';

  const dispatch = useDispatch();

  const status = useCheckClientConfigStatus();

  const { t } = useTranslation();

  const screenType = useScreenType();

  const columns: ColumnType<CallResult>[] = [
    {
      id: 'value',
      Header: t(I18N_CLIENT_CONFIG.ACTION_CODE),
      accessor: 'value',
      isSort: true,
      width: screenType === ScreenType.TABLET_PORTRAIT ? 127 : 192
    },
    {
      id: 'description',
      Header: t(I18N_COMMON_TEXT.DESCRIPTION),
      accessor: (col: CallResult) => (
        <TruncateText
          lines={2}
          title="description"
          resizable
          ellipsisMoreText={t(I18N_COMMON_TEXT.MORE)}
          ellipsisLessText={t(I18N_COMMON_TEXT.LESS)}
          className="text-break"
        >
          {col?.description}
        </TruncateText>
      ),
      isSort: true,
      width: 420,
      autoWidth: true
    },
    {
      id: 'actionTypes',
      Header: t(I18N_CLIENT_CONFIG.CALL_RESULT_ACTION_TYPE_COUNT),
      accessor: (p: CallResult) => p.actionTypes?.length || 0,
      isSort: true,
      width: 160
    },
    {
      id: 'action',
      Header: t(I18N_COMMON_TEXT.ACTIONS),
      accessor: (col: CallResult, idx: number) => (
        <Actions
          status={status}
          record={col}
          dataTestId={`${idx}_action_${testId}`}
        />
      ),
      width: 217,
      isFixedRight: true,
      cellProps: { className: 'text-right' },
      cellHeaderProps: { className: 'text-center' },
      className: 'td-sm'
    }
  ];

  const callResultGridData = useSelector(selectGridData);
  const sortBy = useSelector(getSortBy);
  const selectedConfig = useStoreIdSelector<ConfigData>(selectConfig);

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(callResultGridData);

  const handleSort = (sort: SortType) => {
    dispatch(clientConfigCallResultActions.onChangeSort(sort));
  };

  // reset sort
  useEffect(
    () => () => {
      dispatch(clientConfigCallResultActions.onChangeSort({ id: '' }));
    },
    [dispatch]
  );

  const showPagination = total > PAGE_SIZE_COMMON[0];

  useEffect(() => {
    selectedConfig &&
      dispatch(clientConfigCallResultActions.getCallResultList());
  }, [dispatch, selectedConfig]);

  return (
    <>
      <div className="mt-16 d-flex justify-content-between align-items-center">
        <p className={'fs-14 fw-500 color-grey-d20'} data-testid={genAmtId(testId, 'title', '')}>
          {t(I18N_CLIENT_CONFIG.CLIENT_ACTION_CODE_CONFIGURATION)}
        </p>
        {!isEmpty(gridData) && (
          <div className="mr-n8">
            <AddActionCode status={status} />
          </div>
        )}
      </div>
      <div className="mt-16">
        {isEmpty(gridData) && (
          <NoDataGrid text={I18N_COMMON_TEXT.NO_DATA} dataTestId={`${testId}_noData`}>
            <div className="mt-24">
              <AddActionCode status={status} />
            </div>
          </NoDataGrid>
        )}
        {!isEmpty(gridData) && (
          <GridDLS
            ref={gridRefs}
            columns={columns}
            data={gridData!}
            onSortChange={handleSort}
            sortBy={[sortBy]}
            dataTestId={`${testId}_grid`}
          />
        )}
        {showPagination && (
          <div className="pt-16">
            <Pagination
              totalItem={total}
              pageSize={pageSize}
              pageNumber={currentPage}
              pageSizeValue={currentPageSize}
              compact
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
              dataTestId={`${testId}_pagination`}
            />
          </div>
        )}
      </div>
    </>
  );
};

export default Grid;
