import { I18N_CLIENT_CONFIG } from 'app/constants/i18n';
import { SimpleBar } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import React from 'react';
import ActivateModal from './ActivateModal';
import DeactivateModal from './DeactivateModal';
import Grid from './Grid';
import UpdateModal from './UpdateModal';

const CallResult = () => {
  const { t } = useTranslation();

  return (
    <div className={'position-relative h-100'}>
      <SimpleBar>
        <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
          <h5 data-testid="callResult_title">
            {t(I18N_CLIENT_CONFIG.CALL_RESULT_SINGULAR)}
          </h5>
          <Grid />
        </div>
      </SimpleBar>
      <UpdateModal />
      <ActivateModal />
      <DeactivateModal />
    </div>
  );
};

export default CallResult;
