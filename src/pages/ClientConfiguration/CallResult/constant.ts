import { CallResult } from './types';

export const GRID_COLUMN: Record<string, keyof CallResult> = {
  ID: 'id',
  ACTION_CODE: 'value',
  ACTION_CODE_DESCRIPTION: 'description',
  // NEXT_WORK_DATE: 'nextWorkDateVariable',
  // MEMO: 'memo',
  // LETTER_ID: 'letterId',
  ACTIVATED: 'activated'
};

export const MENTION_DATA = ['Contact Name', 'Contact Phone', 'Operator ID'];

export const EMPTY_STATE_EDITOR = ``;

export const FORM_NAME = 'clientConfigCallResultView';

export const ADD_FORM_KY = 'clientConfigCallResultView';

export const UPDATE_FORM_KY = 'clientConfigCallResultViewEdit';

export const UPDATE_FORM_KY_TYPE =
  'clientConfigCallResultActionCodeTypeViewEdit';

export const CONTACT_VALUE = {
  YES: '1',
  NO: '0'
};

export const MEMO_MAX_LENGTH = 1000;
