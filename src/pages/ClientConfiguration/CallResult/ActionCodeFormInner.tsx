import React, { MutableRefObject, useEffect } from 'react';
// Redux
import { useSelector } from 'react-redux';
import { FormInstance, getFormValues } from 'redux-form';
// Components
import { View } from 'app/_libraries/_dof/core';
// Constants
import { CallResultActionType } from './types';
import isEqual from 'lodash.isequal';
import isString from 'lodash.isstring';
import { genAmtId } from 'app/_libraries/_dls/utils';

const FormInner = React.forwardRef(
  (
    {
      id,
      descriptor,
      formKey,
      actionValues,
      onDirtyCallback,
      dataTestId
    }: {
      id: string;
      descriptor: string;
      formKey: string;
      actionValues?: CallResultActionType;
      onDirtyCallback?: (isDirty: boolean) => void;
      dataTestId?: string;
    },
    innerRef
  ) => {
    const formValues = useSelector(getFormValues(formKey)) as any;

    useEffect(() => {
      const externalStatusCode = isString(formValues?.externalStatusCode)
        ? formValues?.externalStatusCode
        : formValues?.externalStatusCode?.value;
      const externalStatusReason = isString(formValues?.externalStatusReason)
        ? formValues?.externalStatusReason
        : formValues?.externalStatusReason?.value;

      if (typeof onDirtyCallback === 'function') {
        if (
          !isEqual(
            formValues?.nextWorkDateVariable,
            actionValues?.nextWorkDateVariable
          ) ||
          !isEqual(formValues?.letterId, actionValues?.letterId) ||
          !isEqual(externalStatusCode, actionValues?.externalStatusCode) ||
          !isEqual(externalStatusReason, actionValues?.externalStatusReason)
        ) {
          onDirtyCallback(true);
        } else {
          onDirtyCallback(false);
        }
      }
    }, [
      actionValues?.externalStatusCode,
      actionValues?.externalStatusReason,
      actionValues?.letterId,
      actionValues?.nextWorkDateVariable,
      formValues,
      onDirtyCallback
    ]);

    return (
      <View
        id={id}
        descriptor={descriptor}
        formKey={formKey}
        value={actionValues}
        ref={innerRef as MutableRefObject<FormInstance<any, any, string>>}
        dataTestId={genAmtId(dataTestId, descriptor, '')}
      />
    );
  }
);

export default FormInner;
