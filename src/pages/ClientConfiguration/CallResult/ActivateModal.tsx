import React from 'react';

// app utils
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { useStoreIdSelector } from 'app/hooks';

// components
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

// hooks
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { useDispatch, useSelector } from 'react-redux';
import {
  selectLoadingUpdate,
  selectOpenActivateModal,
  selectRecordActivateModal
} from './_redux/selector';

// redux
import { clientConfigCallResultActions } from './_redux/reducers';

// types
import { CallResult } from './types';
import { genAmtId } from 'app/_libraries/_dls/utils';

const ActivateModal = () => {
  const { t } = useTranslation();
  const testId = 'callResult_activateModal';

  const dispatch = useDispatch();

  const isOpenModal = useStoreIdSelector<boolean>(selectOpenActivateModal);

  const isLoading = useStoreIdSelector<boolean>(selectLoadingUpdate);

  const selectedRecord: CallResult | undefined = useSelector(
    selectRecordActivateModal
  );

  const onSubmit = () => {
    if (!selectedRecord) return;

    dispatch(
      clientConfigCallResultActions.triggerActivateCallResult({
        data: {
          ...selectedRecord,
          activated: true
        },
        updateAction: 'activate'
      })
    );
  };

  const handleCloseModal = () => {
    dispatch(clientConfigCallResultActions.toggleActivateModal());
  };

  return (
    <Modal show={isOpenModal} loading={isLoading} xs dataTestId={testId}>
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId={`${testId}_header`}
      >
        <ModalTitle dataTestId={`${testId}_title`}>
          {t(I18N_CLIENT_CONFIG.ACTIVATE_CALL_RESULT)}
        </ModalTitle>
      </ModalHeader>
      <ModalBody dataTestId={`${testId}_body`}>
        <p data-testid={genAmtId(testId, 'resultConfirm', '')}>
          {t(I18N_CLIENT_CONFIG.ACTIVATE_CALL_RESULT_CONFIRM)}
        </p>
        <p className="mt-16">
          {`${t(I18N_CLIENT_CONFIG.ACTION_CODE)}: `}
          <b
            className="text-break"
            data-testid={genAmtId(testId, 'activeCode', '')}
          >
            {`${selectedRecord?.value} • ${selectedRecord?.description}`}
          </b>
        </p>
      </ModalBody>
      <ModalFooter
        cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
        okButtonText={t(I18N_COMMON_TEXT.ACTIVATE)}
        onCancel={handleCloseModal}
        onOk={onSubmit}
        dataTestId={`${testId}_footer`}
      />
    </Modal>
  );
};

export default ActivateModal;
