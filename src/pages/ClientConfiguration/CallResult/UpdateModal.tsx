// app utils
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { useStoreIdSelector } from 'app/hooks';
// components
import {
  Icon,
  InlineMessage,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  Radio,
  TextBox,
  TextBoxRef
} from 'app/_libraries/_dls/components';
import ModalBodyWithErrorClientConfig from '../ApiErrorSection/ModalBodyWithErrorClientConfig';
import TextEditor from 'app/_libraries/_dls/components/TextEditor';
import { TextEditorRef } from 'app/_libraries/_dls/components/TextEditor/types';
import { SectionControl } from 'app/components';

// hooks
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import React, {
  ChangeEvent,
  createRef,
  MutableRefObject,
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  selectLoadingUpdate,
  selectOpenUpdateModal,
  selectRecordUpdateModal,
  selectUpdateErrorMessage
} from './_redux/selector';
// redux
import { clientConfigCallResultActions } from './_redux/reducers';
import { View } from 'app/_libraries/_dof/core';
import { FormInstance, getFormValues } from 'redux-form';

// types
import { CallResult, CallResultActionType } from './types';

// constant
import {
  CONTACT_VALUE,
  MEMO_MAX_LENGTH,
  MENTION_DATA,
  UPDATE_FORM_KY,
  UPDATE_FORM_KY_TYPE
} from './constant';
import { useDisabledButtonState } from './useDisabledButtonState';

// helper
import { formatCommon } from 'app/helpers';

// libaries
import isString from 'lodash.isstring';
import FormInner from './ActionCodeFormInner';
import { genAmtId } from 'app/_libraries/_dls/utils';

const UpdateModal = () => {
  const { t } = useTranslation();
  const testId = 'callResult_updateModal';

  const dispatch = useDispatch();

  const viewRef = useRef(null);

  const [behaviourTypeValue, setBehaviourTypeValue] = useState<
    Record<string, string>
  >({});

  const [isExpand, setIsExpand] = useState<Record<string, boolean>>({});

  const [parantRefList, setParantRefList] = useState<
    MutableRefObject<HTMLDivElement | null>[]
  >([]);

  const [behaviourTypeRefList, setBehaviourTypeRefList] = useState<
    MutableRefObject<TextBoxRef | null>[]
  >([]);

  const [editorRefList, setEditorRefList] = useState<
    MutableRefObject<TextEditorRef | null>[]
  >([]);

  const [formActionCodeRefList, setFormActionCodeRefList] = useState<
    MutableRefObject<FormInstance<any, any, string> | null>[]
  >([]);

  const [contactValue, setContactValue] =
    useState<boolean | undefined>(undefined);

  const isOpenModal = useStoreIdSelector<boolean>(selectOpenUpdateModal);

  const isLoading = useStoreIdSelector<boolean>(selectLoadingUpdate);

  const errorMessage = useStoreIdSelector<string>(selectUpdateErrorMessage);

  const selectedRecord: CallResult | undefined = useSelector(
    selectRecordUpdateModal
  );

  const formValue = useSelector(getFormValues(UPDATE_FORM_KY)) as CallResult;

  useEffect(() => {
    if (formValue?.actionTypes?.length) {
      setBehaviourTypeRefList(elRefs =>
        Array(formValue.actionTypes.length)
          .fill(null)
          .map((_, i) => elRefs[i] || createRef())
      );
      setEditorRefList(elRefs =>
        Array(formValue.actionTypes.length)
          .fill(null)
          .map((_, i) => elRefs[i] || createRef())
      );
      setParantRefList(elRefs =>
        Array(formValue.actionTypes.length)
          .fill(null)
          .map((_, i) => elRefs[i] || createRef())
      );
      setFormActionCodeRefList(elRefs =>
        Array(formValue.actionTypes.length)
          .fill(null)
          .map((_, i) => elRefs[i] || createRef())
      );
    }
  }, [formValue?.actionTypes?.length]);

  const [memoLength, setMemoLength] = useState<Record<string, number>>({});

  const behaviourType = useMemo(() => {
    return (
      selectedRecord?.actionTypes
        .map(action => action?.chronicleBehaviorIdentifier || '')
        .filter(text => text !== '') || []
    );
  }, [selectedRecord?.actionTypes]);

  const memoText = useMemo(() => {
    return (
      selectedRecord?.actionTypes
        .map(action => action?.memoText || '')
        .filter(text => text !== '') || []
    );
  }, [selectedRecord?.actionTypes]);

  const hideCollapseActionType =
    selectedRecord?.actionTypes && selectedRecord?.actionTypes.length <= 1;

  const {
    isDisabledOk,
    handleBlurEditor,
    handleBlurBehaviourType,
    handleDirtyFormInner,
    errorEditor,
    isSpecialCharacter,
    errorBehaviours
  } = useDisabledButtonState(
    UPDATE_FORM_KY,
    editorRefList,
    viewRef,
    parantRefList,
    errorMessage,
    isOpenModal,
    contactValue,
    selectedRecord?.countContact,
    'Edit',
    behaviourTypeRefList,
    behaviourType,
    memoText
  );

  const onSubmit = () => {
    if (!selectedRecord) return;

    const actionCodes: CallResultActionType[] = [];
    if (formValue?.actionTypes?.length) {
      formValue?.actionTypes?.forEach((_, idx) => {
        const formValuesCodeType = formActionCodeRefList[idx].current?.values;
        const behaviourType =
          behaviourTypeRefList[idx].current?.inputElement?.value.trim();
        const editorText = editorRefList[idx].current?.getMarkdown();

        const obj = {
          ...formValuesCodeType,
          externalStatusCode: isString(formValuesCodeType?.externalStatusCode)
            ? formValuesCodeType?.externalStatusCode
            : formValuesCodeType?.externalStatusCode?.value,
          externalStatusReason: isString(
            formValuesCodeType?.externalStatusReason
          )
            ? formValuesCodeType?.externalStatusReason
            : formValuesCodeType?.externalStatusReason?.value,
          chronicleBehaviorIdentifier: behaviourType,
          memoText: editorText,
          nextWorkDateVariable: formValuesCodeType?.nextWorkDateVariable || null
        } as CallResultActionType;

        actionCodes.push(obj);
      });
    }

    dispatch(
      clientConfigCallResultActions.triggerUpdateCallResult({
        data: {
          ...formValue,
          actionTypes: actionCodes,
          countContact: contactValue
        }
      })
    );
  };

  const handleCloseModal = () => {
    dispatch(clientConfigCallResultActions.toggleUpdateModal());
    setContactValue(undefined);
  };

  // bind memo data
  useEffect(() => {
    if (isOpenModal) {
      selectedRecord?.actionTypes.forEach((action, index) => {
        let nextHTML = action?.memoText || '';
        nextHTML = nextHTML
          ?.split('{')
          .join('<span class="dls-editor-mention-decorator">');
        nextHTML = nextHTML.split('}').join('</span>');
        editorRefList[index]?.current?.setMarkdown?.(nextHTML);

        setBehaviourTypeValue(prev => ({
          ...prev,
          [index]: action?.chronicleBehaviorIdentifier
        }));
      });

      setContactValue(selectedRecord?.countContact);
    }
  }, [
    editorRefList,
    isOpenModal,
    selectedRecord?.actionTypes,
    selectedRecord?.countContact
  ]);

  const handlePlainTextChange = (nextPlainText: string, idx: string) => {
    setMemoLength(prev => ({
      ...prev,
      [idx]: nextPlainText?.length
    }));
  };

  const handleChangeContact = (event: React.ChangeEvent<HTMLInputElement>) => {
    setContactValue(event.target.value === '1' ? true : false);
  };

  const handleToggleExpand = useCallback(
    (idx: string, expand: boolean) =>
      setIsExpand(prev => ({
        ...prev,
        [idx]: !expand
      })),
    []
  );

  const handleOnChangeBehaviorType = (
    e: ChangeEvent<HTMLInputElement>,
    idx: number
  ) => {
    const { value } = e.target;

    setBehaviourTypeValue(prev => ({
      ...prev,
      [idx]: value
    }));
  };

  useEffect(() => {
    formValue?.actionTypes?.map((_, index) => {
      handleToggleExpand(`${index}`, false);
      setMemoLength(prev => ({
        ...prev,
        [index]: 0
      }));
    });
  }, [formValue?.actionTypes, handleToggleExpand]);

  return (
    <Modal show={isOpenModal} loading={isLoading} md dataTestId={testId}>
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId={`${testId}_header`}
      >
        <ModalTitle dataTestId={`${testId}_title`}>
          {t(I18N_CLIENT_CONFIG.EDIT_ACTION_CODE)}
        </ModalTitle>
      </ModalHeader>
      <ModalBodyWithErrorClientConfig
        apiErrorClassName="mb-24"
        dataTestId={`${testId}_body`}
      >
        {errorMessage && (
          <InlineMessage
            className="mb-24"
            variant="danger"
            dataTestId={`${testId}_errorMessage`}
          >
            {t(errorMessage)}
          </InlineMessage>
        )}
        <View
          id={UPDATE_FORM_KY}
          descriptor={UPDATE_FORM_KY}
          formKey={UPDATE_FORM_KY}
          value={selectedRecord}
          ref={viewRef}
        />
        <div className="ml-n24 mr-n24 mt-16 mb-16 px-24 pb-16 bg-light-l20 border-top border-bottom">
          <div>
            {formValue?.actionTypes?.map((action, index) => (
              <div key={action.actionTypeName}>
                {index > 0 && <div className="divider-dashed my-16" />}

                <SectionControl
                  title={action.actionTypeDescription}
                  classNames="mt-16"
                  sectionKey={`session-${index}`}
                  onToggle={() =>
                    handleToggleExpand(`${index}`, isExpand?.[index])
                  }
                  isAdd={{
                    enableSectionButton: hideCollapseActionType
                  }}
                  isExpand={isExpand?.[index]}
                  dataTestId={`${action.actionTypeName}_${testId}`}
                >
                  <FormInner
                    id={`${UPDATE_FORM_KY_TYPE}-${index}`}
                    descriptor={UPDATE_FORM_KY_TYPE}
                    formKey={`${UPDATE_FORM_KY_TYPE}-${index}`}
                    actionValues={selectedRecord?.actionTypes[index]}
                    ref={formActionCodeRefList[index]}
                    onDirtyCallback={handleDirtyFormInner(index)}
                    dataTestId={`${action.actionTypeName}_${testId}`}
                  />
                  <div className="d-flex mt-16 mb-8">
                    <h6
                      className="color-grey"
                      data-testid={genAmtId(
                        `${action.actionTypeName}_${testId}`,
                        'memo-title',
                        ''
                      )}
                    >
                      {t(I18N_COMMON_TEXT.MEMO_SINGULAR)}
                    </h6>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <TextBox
                        ref={behaviourTypeRefList[index]}
                        name={`chronicleBehaviorIdentifier-${index}`}
                        label={t('txt_behavior_type')}
                        onBlur={handleBlurBehaviourType}
                        onChange={e => handleOnChangeBehaviorType(e, index)}
                        value={behaviourTypeValue[index]}
                        className="mb-16"
                        error={{
                          status: errorBehaviours[index],
                          message: t(
                            I18N_CLIENT_CONFIG.BEHAVIOR_TYPE_IS_REQUIRED
                          )
                        }}
                        maxLength={10}
                        dataTestId={`${action.actionTypeName}_${testId}_behaviorType`}
                      />
                    </div>
                  </div>

                  <p
                    className="mb-16"
                    data-testid={genAmtId(
                      `${action.actionTypeName}_${testId}`,
                      'typeSlashToAddVariables-title',
                      ''
                    )}
                  >
                    {t(I18N_CLIENT_CONFIG.TYPE_SLASH_TO_ADD_VARIABLES)}
                  </p>
                  <div ref={parantRefList[index]} className="bg-white">
                    <TextEditor
                      ref={editorRefList[index]}
                      mention
                      mentionData={MENTION_DATA}
                      onBlur={handleBlurEditor}
                      maxPlainText={MEMO_MAX_LENGTH}
                      onPlainTextChange={(text: string) =>
                        handlePlainTextChange(text, `${index}`)
                      }
                      placeholder={t(I18N_CLIENT_CONFIG.ENTER_MEMO_HERE)}
                      error={{
                        status: errorEditor[index],
                        message: t(I18N_CLIENT_CONFIG.MEMO_IS_REQUIRED)
                      }}
                      dataTestId={`${action.actionTypeName}_${testId}_memo`}
                    />
                  </div>
                  <p
                    className="fs-12 color-grey-l24 mt-8"
                    data-testid={genAmtId(
                      `${action.actionTypeName}_${testId}`,
                      'memoCharaterLeft',
                      ''
                    )}
                  >
                    {`${
                      formatCommon(
                        MEMO_MAX_LENGTH - (memoLength?.[index] || 0) < 0
                          ? 0
                          : MEMO_MAX_LENGTH - (memoLength?.[index] || 0)
                      ).quantity
                    } ${t(I18N_CLIENT_CONFIG.MEMO_CHARACTERS_LEFT)}`}
                  </p>
                  {isSpecialCharacter?.[index] && (
                    <div
                      className="d-flex mt-16 align-items-center"
                      data-testid={genAmtId(
                        `${action.actionTypeName}_${testId}`,
                        'warningSpecialCharacter',
                        ''
                      )}
                    >
                      <Icon
                        name="warning"
                        size="4x"
                        className="color-orange-d08"
                      ></Icon>
                      <p className="fs-12 color-grey ml-8">
                        {t(I18N_CLIENT_CONFIG.ACTION_CODE_MEMO_NON_SPECIAL)}
                      </p>
                    </div>
                  )}
                </SectionControl>
              </div>
            ))}
          </div>
        </div>

        <div className="d-flex my-16">
          <h6
            className="color-grey"
            data-testid={genAmtId(testId, 'countAsAccount-title', '')}
          >
            {t(I18N_CLIENT_CONFIG.COUNT_AS_COUNT)}
          </h6>
          <span className="asterisk color-danger ml-4 mt-n4">*</span>
        </div>
        <Radio
          className="d-inline-block"
          dataTestId={`${testId}_contactValue-yes`}
        >
          <Radio.Input
            checked={contactValue}
            value={CONTACT_VALUE.YES}
            name="contact-yes"
            onChange={handleChangeContact}
          />
          <Radio.Label>{t(I18N_CLIENT_CONFIG.YES)}</Radio.Label>
        </Radio>
        <Radio
          className="d-inline-block ml-24"
          dataTestId={`${testId}_contactValue-no`}
        >
          <Radio.Input
            checked={contactValue === false}
            value={CONTACT_VALUE.NO}
            name="contact-no"
            onChange={handleChangeContact}
          />
          <Radio.Label>{t(I18N_CLIENT_CONFIG.NO)}</Radio.Label>
        </Radio>
      </ModalBodyWithErrorClientConfig>
      <ModalFooter
        cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
        okButtonText={t(I18N_COMMON_TEXT.SAVE)}
        disabledOk={isDisabledOk}
        onCancel={handleCloseModal}
        onOk={onSubmit}
        dataTestId={`${testId}_footer`}
      />
    </Modal>
  );
};

export default UpdateModal;
