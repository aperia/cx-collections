import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';

// component
import ActivateModal from './ActivateModal';

// redux
import { clientConfigCallResultActions } from './_redux/reducers';

// types
import { screen } from '@testing-library/dom';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import userEvent from '@testing-library/user-event';

const spyClientConfigCallResult = mockActionCreator(
  clientConfigCallResultActions
);

const initialState: Partial<RootState> = {
  clientConfigCallResult: {
    activate: {
      open: true,
      record: {
        id: 'id',
        value: 'value',
        description: 'description',
        actionTypes: [
          {
            actionTypeName: 'default',
            actionTypeDescription: 'default',
            chronicleBehaviorIdentifier: 'COLX',
            memoText: 'test'
          }
        ]
      }
    },
    sortBy: { id: 'id' }
  },
  clientConfiguration: { settings: { selectedConfig: { id: 'id' } } }
};

describe('Render', () => {
  it('should render UI', () => {
    renderMockStore(<ActivateModal />, { initialState });

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.ACTIVATE_CALL_RESULT)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleCloseModal', () => {
    const mockAction = spyClientConfigCallResult('toggleActivateModal');
    renderMockStore(<ActivateModal />, { initialState });

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));

    expect(mockAction).toBeCalled();
  });

  describe('onSubmit', () => {
    it('when has selected record', () => {
      const mockAction = spyClientConfigCallResult('triggerActivateCallResult');
      renderMockStore(<ActivateModal />, { initialState });

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.ACTIVATE));

      expect(mockAction).toBeCalledWith({
        data: {
          ...initialState.clientConfigCallResult?.activate?.record,
          activated: true
        },
        updateAction: 'activate'
      });
    });

    it('when has no selected record', () => {
      const mockAction = spyClientConfigCallResult('triggerActivateCallResult');
      renderMockStore(<ActivateModal />, {
        initialState: {
          clientConfigCallResult: {
            activate: { open: true },
            sortBy: { id: 'id' }
          }
        }
      });

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.ACTIVATE));

      expect(mockAction).not.toBeCalled();
    });
  });
});
