import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';

// component
import AddActionCode from './AddActionCode';

// redux
import { clientConfigCallResultActions } from './_redux/reducers';

// types
import { screen } from '@testing-library/dom';
import { I18N_CLIENT_CONFIG } from 'app/constants/i18n';
import userEvent from '@testing-library/user-event';

const spyClientConfigCallResult = mockActionCreator(
  clientConfigCallResultActions
);

const initialState: Partial<RootState> = {};

describe('Render', () => {
  it('should render UI', () => {
    renderMockStore(<AddActionCode />, { initialState });

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.ADD_ACTION_CODE)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleAdd', () => {
    const mockAction = spyClientConfigCallResult('toggleAddModal');
    renderMockStore(<AddActionCode />, { initialState });

    userEvent.click(screen.getByText(I18N_CLIENT_CONFIG.ADD_ACTION_CODE));

    expect(mockAction).toBeCalledWith({ open: true });
  });
});
