import React from 'react';
import { renderMockStore } from 'app/test-utils';

// component
import CallResult from '.';

// types
import { screen } from '@testing-library/dom';
import { I18N_CLIENT_CONFIG } from 'app/constants/i18n';

const initialState: Partial<RootState> = {};

describe('Render', () => {
  it('should render UI', () => {
    renderMockStore(<CallResult />, { initialState });

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.CALL_RESULT_SINGULAR)
    ).toBeInTheDocument();
  });
});
