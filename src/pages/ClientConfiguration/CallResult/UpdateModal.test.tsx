import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';

// component
import UpdateModal from './UpdateModal';

// redux
import { clientConfigCallResultActions } from './_redux/reducers';

// types
import { screen } from '@testing-library/dom';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import userEvent from '@testing-library/user-event';
import { EditorProps } from 'app/_libraries/_dls/components/Editor';
import { UPDATE_FORM_KY } from './constant';
import { queryAllByClass } from 'app/test-utils';
import { fireEvent } from '@testing-library/react';

jest.mock('./useDisabledButtonState', () => {
  return {
    useDisabledButtonState: () => ({
      isDisabledOk: false,
      errorEditor: {
        0: false
      },
      errorBehaviours: {
        0: false
      },
      handleBlurEditor: jest.fn(),
      handleBlurBehaviourType: jest.fn(),
      handleDirtyFormInner: jest.fn(),
      isSpecialCharacter: {
        0: true,
        1: false
      }
    })
  };
});

jest.mock('app/_libraries/_dls/utils/genAmtId', () => jest.fn());

jest.mock('app/_libraries/_dls/components/TextEditor', () => {
  return {
    __esModule: true,
    default: ({ onPlainTextChange, dataTestId }: EditorProps) => {
      return (
        <div
          data-testid={dataTestId}
          onClick={() => onPlainTextChange!(Array(2000).fill('a').join(''))}
        />
      );
    }
  };
});

jest.mock('app/_libraries/_dls/components/TextBox', () => {
  return {
    __esModule: true,
    default: ({ onChange, dataTestId }: any) => {
      return <input data-testid={dataTestId} onChange={onChange} />;
    }
  };
});

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

// jest.mock('react', () => ({
//   ...jest.requireActual('react'),
//   createRef: () => {
//     return {
//       current: {
//         values: {
//           externalStatusCode: 'A',
//           externalStatusReason: 'A'
//         },
//         getHTML: () => 'a'
//       }
//     };
//   }
// }));

HTMLCanvasElement.prototype.getContext = jest.fn();

const spyClientConfigCallResult = mockActionCreator(
  clientConfigCallResultActions
);

const initialState: Partial<RootState> = {
  clientConfigCallResult: {
    sortBy: { id: 'id' },
    update: {
      errorMessage: 'errorMessage',
      open: true,
      record: {
        id: 'id',
        value: 'value',
        description: 'description',
        actionTypes: [
          {
            actionTypeName: 'default',
            nextWorkDateVariable: '',
            letterId: '1',
            chronicleBehaviorIdentifier: undefined,
            memoText: undefined,
            externalStatusCode: 'A',
            externalStatusReason: 'A'
          },
          {
            actionTypeName: 'custom',
            nextWorkDateVariable: '1',
            letterId: '2',
            chronicleBehaviorIdentifier: undefined,
            memoText: undefined,
            externalStatusCode: { value: 'A', description: 'test' } as any,
            externalStatusReason: { value: 'A', description: 'test' } as any
          }
        ],
        countContact: undefined
      }
    }
  },
  form: {
    [UPDATE_FORM_KY]: {
      values: {
        id: 'id',
        value: 'value',
        description: 'description',
        actionTypes: [
          {
            actionTypeName: 'default',
            actionTypeDescription: 'description',
            nextWorkDateVariable: '',
            letterId: '1',
            chronicleBehaviorIdentifier: undefined,
            memoText: 'test'
          },
          {
            actionTypeName: 'custom',
            actionTypeDescription: 'description',
            nextWorkDateVariable: '1',
            letterId: '2',
            chronicleBehaviorIdentifier: undefined,
            memoText: undefined
          }
        ]
      }
    } as any
  },
  clientConfiguration: { settings: { selectedConfig: { id: 'id' } } }
};

describe('Render', () => {
  it('should render UI', () => {
    jest.useFakeTimers();
    renderMockStore(<UpdateModal />, { initialState });
    jest.runAllTimers();

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.EDIT_ACTION_CODE)
    ).toBeInTheDocument();
  });

  it('should render UI', () => {
    renderMockStore(<UpdateModal />, { initialState: {} });

    expect(screen.queryByText(I18N_CLIENT_CONFIG.EDIT_ACTION_CODE)).toBeNull();
  });
});

describe('Actions', () => {
  it('handleCloseModal', () => {
    const mockAction = spyClientConfigCallResult('toggleUpdateModal');
    renderMockStore(<UpdateModal />, { initialState });

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));

    expect(mockAction).toBeCalled();
  });

  describe('onSubmit', () => {
    it('when has selected record', () => {
      const mockAction = spyClientConfigCallResult('triggerUpdateCallResult');
      const { baseElement } = renderMockStore(<UpdateModal />, {
        initialState
      });

      fireEvent.change(
        screen.getByTestId('custom_callResult_updateModal_behaviorType'),
        {
          target: { value: '{}+}' }
        }
      );
      fireEvent.change(
        screen.getByTestId('custom_callResult_updateModal_behaviorType'),
        {
          target: { value: 'XCOLLS' }
        }
      );

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.SAVE));
      const queryIconMinus = queryAllByClass(baseElement, /icon icon-minus/);
      userEvent.click(screen.getByTestId('custom_callResult_updateModal_memo'));
      userEvent.click(queryIconMinus[0]!);

      expect(mockAction).toBeCalledWith({
        data: {
          actionTypes: [
            {
              actionTypeName: 'default',
              chronicleBehaviorIdentifier: undefined,
              externalStatusCode: 'A',
              externalStatusReason: 'A',
              letterId: '1',
              memoText: undefined,
              nextWorkDateVariable: null
            },
            {
              actionTypeName: 'custom',
              chronicleBehaviorIdentifier: undefined,
              externalStatusCode: 'A',
              externalStatusReason: 'A',
              letterId: '2',
              memoText: undefined,
              nextWorkDateVariable: '1'
            }
          ],
          countContact: undefined,
          description: 'description',
          id: 'id',
          value: 'value'
        }
      });
    });

    it('when has selected record with no actionTypes', () => {
      const mockAction = spyClientConfigCallResult('triggerUpdateCallResult');
      renderMockStore(<UpdateModal />, {
        initialState: {
          ...initialState,
          clientConfigCallResult: {
            sortBy: { id: 'id' },
            update: {
              errorMessage: 'errorMessage',
              open: true,
              record: {
                id: 'id',
                value: 'value',
                description: 'description',
                actionTypes: [],
                countContact: undefined
              }
            }
          },
          form: {
            [UPDATE_FORM_KY]: {
              values: {
                actionTypes: [],
                countContact: undefined,
                description: 'description',
                id: 'id',
                value: 'value'
              }
            } as any
          }
        }
      });

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.SAVE));

      expect(mockAction).toBeCalledWith({
        data: {
          actionTypes: [],
          countContact: undefined,
          description: 'description',
          id: 'id',
          value: 'value'
        }
      });
    });

    it('when has no selected record', () => {
      const mockAction = spyClientConfigCallResult('triggerUpdateCallResult');
      renderMockStore(<UpdateModal />, {
        initialState: {
          clientConfigCallResult: {
            update: { open: true },
            sortBy: { id: 'id' }
          },
          form: {
            [UPDATE_FORM_KY]: {
              values: {}
            } as any
          }
        }
      });

      userEvent.click(screen.getByText(I18N_CLIENT_CONFIG.NO));
      userEvent.click(screen.getByText(I18N_CLIENT_CONFIG.YES));

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.SAVE));

      expect(mockAction).not.toBeCalled();
    });
  });
});
