import { TextBoxControlProps } from 'app/components/_dof_controls/controls';
import { TextBoxRef } from 'app/_libraries/_dls/components';
import { ExtraFieldProps } from 'app/_libraries/_dof/core';
import isEqual from 'lodash.isequal';
import isEmpty from 'lodash.isempty';
import { MutableRefObject, useCallback, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Field, isDirty, isInvalid } from 'redux-form';
import { GRID_COLUMN } from './constant';
import { TextEditorRef } from 'app/_libraries/_dls/components/TextEditor/types';

export const useDisabledButtonState = (
  formName: string,
  editorRef: MutableRefObject<TextEditorRef | null>[],
  viewRef: MutableRefObject<any>,
  parantRef: MutableRefObject<HTMLDivElement | null>[],
  errorMessage: string,
  isOpenModal: boolean,
  contactValue: boolean | undefined,
  initContactValue: boolean | undefined,
  action: string,
  behaviourTypeRef: MutableRefObject<TextBoxRef | null>[],
  behaviours?: string[],
  memos?: string[]
) => {
  const [isDisabledOk, setDisabledOk] = useState(true);

  const [focusEditor, setFocusEditor] = useState(false);

  const [errorEditor, setErrorEditor] = useState<Record<string, boolean>>({});

  const [isSpecialCharacter, setIsSpecialCharacter] = useState<
    Record<string, boolean>
  >({});

  const isFormInvalid = useSelector(isInvalid(formName));
  const isFormDirty = useSelector(isDirty(formName));

  const [errorBehaviours, setErrorBehaviours] = useState<
    Record<string, boolean>
  >({});

  const [formInnerDirty, setFormInnerDirty] = useState<Record<string, boolean>>(
    {}
  );
  const [isBehaviorDirty, setIsBehaviorDirty] = useState(false);
  const [isEditorDirty, setIsEditorDirty] = useState(false);

  const handleDirtyFormInner = (idx: number) => (isDirty: boolean) => {
    if (!isEqual(formInnerDirty[idx], isDirty)) {
      setFormInnerDirty(prev => ({
        ...prev,
        [idx]: isDirty
      }));
    }
  };

  const handleBlurEditor = useCallback(() => {
    let isEditorFormDirty = false;
    parantRef.map((ref, idx) => {
      const contentEditor = ref.current?.querySelectorAll(
        '[contenteditable="true"]'
      );

      const textContent =
        (contentEditor &&
          contentEditor?.length > 0 &&
          contentEditor[0]?.textContent?.trim()) ||
        '';
      const specialCharacter = /["()+;\\%{}]/g.test(textContent);

      // if exits special charactor, disable submit button
      setIsSpecialCharacter(prev => ({
        ...prev,
        [idx]: specialCharacter
      }));

      // Check if values of editor item change
      if (
        !isEditorFormDirty &&
        action === 'Edit' &&
        memos &&
        !isEqual(memos[idx]?.replace(/(\} \{|\{|\})/g, ''), textContent)
      ) {
        isEditorFormDirty = true;
      }
    });
    setFocusEditor(true);
    setIsEditorDirty(isEditorFormDirty);
  }, [action, memos, parantRef]);

  const handleBlurBehaviourType = useCallback(() => {
    let isBehaviorFormDirty = false;
    behaviourTypeRef.map((ref, idx) => {
      const textContent = ref.current?.inputElement?.value?.trim() || '';

      // Check if values of editor item change
      if (
        !isBehaviorFormDirty &&
        action === 'Edit' &&
        behaviours &&
        !isEqual(behaviours[idx], textContent)
      ) {
        isBehaviorFormDirty = true;
      }
    });
    setIsBehaviorDirty(isBehaviorFormDirty);
  }, [action, behaviourTypeRef, behaviours]);

  // reset state when close modal
  useEffect(() => {
    if (isOpenModal) {
      setErrorEditor({});
      setIsSpecialCharacter({});
      setErrorBehaviours({});
      setFormInnerDirty({});
      setIsBehaviorDirty(false);
      setIsEditorDirty(false);
    }
  }, [isOpenModal]);

  // watch isEmpty memo to calculate the disable state
  useEffect(() => {
    if (!isEmpty(memos)) {
      setFocusEditor(true);
    }
  }, [memos]);

  // calculate disabled state
  useEffect(() => {
    setDisabledOk(
      isFormInvalid ||
        (!isFormDirty &&
          !!!Object.values(formInnerDirty).find(item => item) &&
          !isBehaviorDirty &&
          !isEditorDirty &&
          isEqual(contactValue, initContactValue)) ||
        Object.values(isSpecialCharacter).find(item => item) !== undefined ||
        contactValue === undefined
    );
  }, [
    focusEditor,
    isFormInvalid,
    isFormDirty,
    formInnerDirty,
    isBehaviorDirty,
    isEditorDirty,
    isSpecialCharacter,
    contactValue,
    initContactValue
  ]);

  useEffect(() => {
    if (!errorMessage) return;

    const actionCode: Field<ExtraFieldProps> = viewRef?.current?.props?.onFind(
      GRID_COLUMN.ACTION_CODE
    );

    actionCode?.props?.props?.setOthers((prev: TextBoxControlProps) => ({
      ...prev,
      options: {
        ...prev.options,
        isError: true
      }
    }));
  }, [errorMessage, viewRef]);

  return {
    isDisabledOk,
    handleBlurEditor,
    handleBlurBehaviourType,
    handleDirtyFormInner,
    errorEditor,
    isSpecialCharacter,
    errorBehaviours
  };
};
