import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  PayloadAction
} from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import accountDetailService from 'pages/AccountDetails/AccountDetailSnapshot/accountDetailService';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { CallResultState, GetCallResultListPayload } from '../types';

export const getCallResultList = createAsyncThunk<
  GetCallResultListPayload,
  undefined,
  ThunkAPIConfig
>('clientConfiguration/getCallResultList', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;

  const [
    { data: externalStatusCodeRefData },
    { data: externalStatusReasonRefData }
  ] = await Promise.all([
    accountDetailService.getExternalStatusRefData(),
    accountDetailService.getReasonRefData()
  ]);

  const response = await dispatch(
    clientConfigurationActions.getClientInfoData({
      dataType: 'actionCodes'
    })
  );

  if (isFulfilled(response)) {
    const actionCodesModel =
      getState().mapping.data?.clientConfigurationActionCodes || {};

    const actionCodes = response.payload.actionCodes || [];

    const list = mappingDataFromArray<any>(
      actionCodes,
      actionCodesModel
    ).map(item => {
      if(item.externalStatus === ""){
        return {...item, externalStatus: " "};
      }
      return item;
    });

    return {
      list,
      externalStatusCodeRefData,
      externalStatusReasonRefData
    };
  }

  return thunkAPI.rejectWithValue({});
});

export const getCallResultListBuilder = (
  builder: ActionReducerMapBuilder<CallResultState>
) => {
  builder.addCase(getCallResultList.pending, (draftState, action) => {
    draftState.loading = true;
  });
  builder.addCase(
    getCallResultList.fulfilled,
    (draftState, action: PayloadAction<GetCallResultListPayload>) => {
      const { list, externalStatusCodeRefData, externalStatusReasonRefData } =
        action.payload;
      draftState.list = list;
      draftState.externalStatusCodeRefData = externalStatusCodeRefData;
      draftState.externalStatusReasonRefData = externalStatusReasonRefData;
      draftState.loading = false;
    }
  );
  builder.addCase(getCallResultList.rejected, (draftState, action) => {
    draftState.loading = false;
    draftState.error = true;
  });
};
