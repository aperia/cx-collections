import { createStore, Store } from '@reduxjs/toolkit';
import { I18N_CLIENT_CONFIG } from 'app/constants/i18n';
import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator
} from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

import { rootReducer } from 'storeConfig';
import { clientConfigCallResultActions } from './reducers';

import { triggerAddCallResult } from './addCallResult';
import { CodeErrorType } from 'app/constants/enums';
import { UpdateCallResultConfigurationArgs } from '../types';

const spyToast = mockActionCreator(actionsToast);
const spyClientConfigCallResult = mockActionCreator(
  clientConfigCallResultActions
);

describe('should have test addCallResult', () => {
  const mockArgs: UpdateCallResultConfigurationArgs = {
    data: {
      id: 'id', value: 'value', description: 'description',
      actionTypes: []
    },
    updateAction: 'add'
  };

  let store: Store<RootState>;
  let spy: jest.SpyInstance;

  beforeEach(() => {
    store = createStore(rootReducer, {
      clientConfigClientContactInformation: {}
    });
  });

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('error', async () => {
    const mockAction = spyClientConfigCallResult('setAddErrorMessage');

    await triggerAddCallResult(mockArgs)(
      store.dispatch,
      () =>
      ({
        clientConfigCallResult: {
          list: [{ id: 'id', value: 'value', description: 'desc' }],
          sortBy: { id: 'id' }
        }
      } as RootState),
      {}
    );

    expect(mockAction).toBeCalledWith(
      I18N_CLIENT_CONFIG.ACTION_CODE_ALREADY_EXISTS
    );
  });

  it('isfullfiled', async () => {
    spyToast('addToast');
    spyClientConfigCallResult('toggleAddModal');
    spyClientConfigCallResult('getCallResultList');
    const mockAddToast = spyToast('addToast');
    const mockToggleAddModal = spyClientConfigCallResult('toggleAddModal');
    const mockGetCallResultList =
      spyClientConfigCallResult('getCallResultList');

    await triggerAddCallResult(mockArgs)(
      byPassFulfilled(triggerAddCallResult.fulfilled.type),
      store.getState,
      {}
    );

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_CLIENT_CONFIG.ACTION_CODE_ADDED
    });
    expect(mockToggleAddModal).toBeCalled();
    expect(mockGetCallResultList).toBeCalled();
  });

  describe('isRejected', () => {
    it('when duplicate record', async () => {
      const payload = {
        response: { data: { responseCode: CodeErrorType.DuplicateRecord } }
      };

      const mockAddToast = spyToast('addToast');

      await triggerAddCallResult(mockArgs)(
        byPassRejected(triggerAddCallResult.fulfilled.type, payload),
        store.getState,
        {}
      );

      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'error',
        message: I18N_CLIENT_CONFIG.ACTION_CODE_FAILED_TO_ADD
      });
    });

    it('when not duplicate record', async () => {
      const mockAddToast = spyToast('addToast');

      await triggerAddCallResult(mockArgs)(
        byPassRejected(triggerAddCallResult.fulfilled.type),
        store.getState,
        {}
      );

      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'error',
        message: I18N_CLIENT_CONFIG.ACTION_CODE_FAILED_TO_ADD
      });
    });
  });

  it('pending', () => {
    const action = triggerAddCallResult.pending(
      triggerAddCallResult.pending.type,
      { data: { id: 'id', value: '', description: '' } }
    );
    const actual = rootReducer(store.getState(), action).clientConfigCallResult
      .add;

    expect(actual!.loading).toEqual(true);
  });

  it('fulfilled', () => {
    const action = triggerAddCallResult.fulfilled(
      {} as unknown as void,
      triggerAddCallResult.fulfilled.type,
      { data: { id: 'id', value: '', description: '' } }
    );
    const actual = rootReducer(store.getState(), action).clientConfigCallResult
      .add;

    expect(actual!.loading).toEqual(false);
  });

  it('rejected', () => {
    const action = triggerAddCallResult.rejected(
      new Error(),
      triggerAddCallResult.rejected.type,
      { data: { id: 'id', value: '', description: '' } }
    );
    const actual = rootReducer(store.getState(), action).clientConfigCallResult
      .add;

    expect(actual!.loading).toEqual(false);
  });
});
