import { clientConfigCallResultActions as actions, reducer } from './reducers';

import { CallResultState } from '../types';

const initialState: CallResultState = {
  add: { open: true, errorMessage: '' },
  update: { open: true },
  activate: { open: true },
  deactivate: { open: true },
  sortBy: { id: 'id' }
};

describe('CallResult Reducers', () => {
  it('toggleAddModal', () => {
    const state = reducer(initialState, actions.toggleAddModal({ open: true }));

    expect(state.add).toEqual({
      open: true,
      errorMessage: ''
    });
  });

  it('toggleUpdateModal', () => {
    const state = reducer(
      initialState,
      actions.toggleUpdateModal({ open: true })
    );

    expect(state.update).toEqual({ open: true });
  });

  it('toggleActivateModal', () => {
    const state = reducer(
      initialState,
      actions.toggleActivateModal({ open: true })
    );

    expect(state.activate).toEqual({ open: true });
  });

  it('toggleDeactivateModal', () => {
    const state = reducer(
      initialState,
      actions.toggleDeactivateModal({ open: true })
    );

    expect(state.deactivate).toEqual({ open: true });
  });

  it('setAddErrorMessage', () => {
    const state = reducer(
      initialState,
      actions.setAddErrorMessage('errorMessage')
    );

    expect(state.add!.errorMessage).toEqual('errorMessage');
  });

  it('setUpdateErrorMessage', () => {
    const state = reducer(
      initialState,
      actions.setUpdateErrorMessage('errorMessage')
    );

    expect(state.update!.errorMessage).toEqual('errorMessage');
  });

  it('onChangeSort', () => {
    const state = reducer(
      initialState,
      actions.onChangeSort({ id: 'id', order: 'asc' })
    );

    expect(state.sortBy).toEqual({ id: 'id', order: 'asc' });
  });
});
