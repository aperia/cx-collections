import { PAGE_SIZE_COMMON } from 'app/constants';
import {
  triggerActivateCallResult,
  triggerDeactivateCallResult,
  triggerUpdateCallResult,
  updateCallResultBuilder
} from './updateCallResult';
import { addCallResultBuilder, triggerAddCallResult } from './addCallResult';
import {
  getCallResultListBuilder,
  getCallResultList
} from './getCallResultList';
import {
  CallResultState,
  ToggleActivatePayload,
  ToggleUpdatePayload
} from './../types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { SortType } from 'app/_libraries/_dls/components';

const { actions, reducer } = createSlice({
  name: 'clientConfigCallResult',
  initialState: {
    sortBy: { id: 'value', order: undefined },
    page: 1,
    pageSize: PAGE_SIZE_COMMON[0]
  } as CallResultState,
  reducers: {
    toggleAddModal: (
      draftState,
      action: PayloadAction<ToggleUpdatePayload | undefined>
    ) => {
      draftState.add = {
        open: action?.payload?.open,
        errorMessage: ''
      };
    },
    toggleUpdateModal: (
      draftState,
      action: PayloadAction<ToggleUpdatePayload | undefined>
    ) => {
      draftState.update = {
        open: action?.payload?.open,
        record: action?.payload?.record || draftState.update?.record
      };
    },
    toggleActivateModal: (
      draftState,
      action: PayloadAction<ToggleActivatePayload | undefined>
    ) => {
      draftState.activate = {
        open: action?.payload?.open,
        record: action?.payload?.record || draftState.activate?.record
      };
    },
    toggleDeactivateModal: (
      draftState,
      action: PayloadAction<ToggleActivatePayload | undefined>
    ) => {
      draftState.deactivate = {
        open: action?.payload?.open,
        record: action?.payload?.record || draftState.deactivate?.record
      };
    },
    setAddErrorMessage: (draftState, action: PayloadAction<string>) => {
      draftState.add = {
        ...draftState.add,
        errorMessage: action.payload
      };
    },
    setUpdateErrorMessage: (draftState, action: PayloadAction<string>) => {
      draftState.update = {
        ...draftState.update,
        errorMessage: action.payload
      };
    },
    onChangeSort: (draftState, action: PayloadAction<SortType>) => {
      draftState.sortBy = action.payload;
    }
  },
  extraReducers: builder => {
    getCallResultListBuilder(builder);
    addCallResultBuilder(builder);
    updateCallResultBuilder(builder);
  }
});

const clientConfigCallResultActions = {
  ...actions,
  getCallResultList,
  triggerAddCallResult,
  triggerUpdateCallResult,
  triggerActivateCallResult,
  triggerDeactivateCallResult
};

export { clientConfigCallResultActions, reducer };
