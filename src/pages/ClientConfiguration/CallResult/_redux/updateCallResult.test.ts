import { createStore, Store } from '@reduxjs/toolkit';
import { I18N_CLIENT_CONFIG } from 'app/constants/i18n';
import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator
} from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

import { rootReducer } from 'storeConfig';
import { clientConfigCallResultActions } from './reducers';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

import {
  updateCallResult,
  triggerUpdateCallResult,
  triggerActivateCallResult,
  triggerDeactivateCallResult
} from './updateCallResult';
import { CallResult } from '../types';

const spyToast = mockActionCreator(actionsToast);
const spyClientConfigCallResult = mockActionCreator(
  clientConfigCallResultActions
);
const spyClientConfiguration = mockActionCreator(clientConfigurationActions);

let store: Store<RootState>;
let spy: jest.SpyInstance;

beforeEach(() => {
  store = createStore(rootReducer, {
    clientConfigClientContactInformation: {}
  });
});

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('updateCallResult', () => {
  it('action add', async () => {
    const mockAction = spyClientConfiguration('updateClientInfo');

    const response = await updateCallResult({
      data: {
        id: 'id',
        value: 'value',
        description: 'desc',
        externalStatus: {},
        externalStatusReason: {}
      },
      updateAction: 'add'
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual(undefined);
    expect(mockAction).toBeCalled();
  });

  it('action activate', async () => {
    const mockAction = spyClientConfiguration('updateClientInfo');
    const response = await updateCallResult({
      data: {
        id: 'id',
        value: 'value',
        description: 'desc',
        externalStatus: {},
        externalStatusReason: {}
      },
      updateAction: 'activate'
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual(undefined);
    expect(mockAction).toBeCalled();
  });

  it('action deactivate', async () => {
    const mockAction = spyClientConfiguration('updateClientInfo');
    const response = await updateCallResult({
      data: {
        id: 'id',
        value: 'value',
        description: 'desc',
        externalStatus: {},
        externalStatusReason: {}
      },
      updateAction: 'deactivate'
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual(undefined);
    expect(mockAction).toBeCalled();
  });

  it('without action', async () => {
    const mockAction = spyClientConfiguration('updateClientInfo');
    const response = await updateCallResult({
      data: {
        id: 'id',
        value: 'value',
        description: 'desc',
        externalStatus: '' as unknown as CallResult['externalStatus'],
        externalStatusReason:
          '' as unknown as CallResult['externalStatusReason']
      },
      updateAction: undefined
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual(undefined);
    expect(mockAction).toBeCalled();
  });

  it('isRejected', async () => {
    const mockAction = spyClientConfiguration('updateClientInfo');
    const response = await updateCallResult({
      data: {
        id: 'id',
        value: 'value',
        description: 'desc'
      }
    })(byPassRejected(updateCallResult.rejected.type), store.getState, {});

    expect(response.payload).toEqual({});
    expect(mockAction).toBeCalled();
  });

  it('pending', () => {
    const pendingAction = updateCallResult.pending(
      updateCallResult.pending.type,
      { data: { id: 'id', value: 'value', description: 'desc' } }
    );
    const actual = rootReducer(store.getState(), pendingAction)
      .clientConfigCallResult.update;

    expect(actual!.loading).toEqual(true);
  });

  it('fulfilled', () => {
    const pendingAction = updateCallResult.fulfilled(
      undefined,
      updateCallResult.pending.type,
      { data: { id: 'id', value: 'value', description: 'desc' } }
    );
    const actual = rootReducer(store.getState(), pendingAction)
      .clientConfigCallResult.update;

    expect(actual!.loading).toEqual(false);
    expect(actual!.open).toEqual(false);
  });

  it('rejected', () => {
    const pendingAction = updateCallResult.rejected(
      new Error(),
      updateCallResult.pending.type,
      { data: { id: 'id', value: 'value', description: 'desc' } }
    );
    const actual = rootReducer(store.getState(), pendingAction)
      .clientConfigCallResult.update;

    expect(actual!.loading).toEqual(false);
  });
});

describe('triggerActivateCallResult', () => {
  it('isFulfilled', async () => {
    const mockAddToast = spyToast('addToast');
    const mockToggleActivateModal = spyClientConfigCallResult(
      'toggleActivateModal'
    );
    const mockGetCallResultList =
      spyClientConfigCallResult('getCallResultList');

    await triggerActivateCallResult({
      data: {
        id: 'id',
        value: 'value',
        description: 'desc'
      }
    })(byPassFulfilled(updateCallResult.rejected.type), store.getState, {});

    expect(mockGetCallResultList).toBeCalled();
    expect(mockToggleActivateModal).toBeCalled();
    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_CLIENT_CONFIG.ACTION_CODE_ACTIVATED
    });
  });

  it('isRejected', async () => {
    const mockAddToast = spyToast('addToast');

    await triggerActivateCallResult({
      data: {
        id: 'id',
        value: 'value',
        description: 'desc'
      }
    })(byPassRejected(updateCallResult.rejected.type), store.getState, {});

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_CLIENT_CONFIG.ACTION_CODE_FAILED_TO_ACTIVATE
    });
  });
});

describe('triggerDeactivateCallResult', () => {
  it('isFulfilled', async () => {
    const mockAddToast = spyToast('addToast');
    const mockToggleDeactivateModal = spyClientConfigCallResult(
      'toggleDeactivateModal'
    );
    const mockGetCallResultList =
      spyClientConfigCallResult('getCallResultList');

    await triggerDeactivateCallResult({
      data: {
        id: 'id',
        value: 'value',
        description: 'desc'
      }
    })(byPassFulfilled(updateCallResult.rejected.type), store.getState, {});

    expect(mockGetCallResultList).toBeCalled();
    expect(mockToggleDeactivateModal).toBeCalled();
    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_CLIENT_CONFIG.ACTION_CODE_DEACTIVATED
    });
  });

  it('isRejected', async () => {
    const mockAddToast = spyToast('addToast');

    await triggerDeactivateCallResult({
      data: {
        id: 'id',
        value: 'value',
        description: 'desc'
      }
    })(byPassRejected(updateCallResult.rejected.type), store.getState, {});

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_CLIENT_CONFIG.ACTION_CODE_FAILED_TO_DEACTIVATE
    });
  });
});

describe('triggerUpdateCallResult', () => {
  it('update error', async () => {
    const mockSetUpdateErrorMessage = spyClientConfigCallResult(
      'setUpdateErrorMessage'
    );

    await triggerUpdateCallResult({
      data: {
        id: 'id',
        value: 'value',
        description: 'desc',
        actionTypes: []
      },
      updateAction: 'add'
    })(
      byPassFulfilled(updateCallResult.rejected.type),
      () =>
      ({
        clientConfigClientContactInformation: {
          tooltip: { text: '', visible: false }
        },
        clientConfigCallResult: {
          list: [{ id: 'id', value: 'value', description: 'desc' }],
          sortBy: { id: 'id' }
        }
      } as RootState),
      {}
    );

    expect(mockSetUpdateErrorMessage).toBeCalledWith(
      I18N_CLIENT_CONFIG.ACTION_CODE_ALREADY_EXISTS
    );
  });

  it('isFulfilled', async () => {
    const mockAddToast = spyToast('addToast');
    const mockGetCallResultList =
      spyClientConfigCallResult('getCallResultList');

    await triggerUpdateCallResult({
      data: {
        id: 'id',
        value: 'value',
        description: 'desc',
        actionTypes: []
      },
      updateAction: 'add'
    })(byPassFulfilled(updateCallResult.rejected.type), store.getState, {});

    expect(mockGetCallResultList).toBeCalled();

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_CLIENT_CONFIG.ACTION_CODE_UPDATED
    });
  });

  it('isRejected', async () => {
    const mockAddToast = spyToast('addToast');

    await triggerUpdateCallResult({
      data: {
        id: 'id',
        value: 'value',
        description: 'desc',
        actionTypes: []
      },
      updateAction: 'add'
    })(byPassRejected(updateCallResult.rejected.type), store.getState, {});

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_CLIENT_CONFIG.ACTION_CODE_FAILED_TO_UPDATE
    });
  });
});
