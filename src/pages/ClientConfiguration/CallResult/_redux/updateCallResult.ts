import {
  CallResult,
  CallResultState,
  UpdateCallResultConfigurationArgs
} from './../types';
import { I18N_CLIENT_CONFIG } from 'app/constants/i18n';
import { batch } from 'react-redux';
import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isRejected,
  isFulfilled
} from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { clientConfigCallResultActions } from './reducers';
import { mappingDataFromArray } from 'app/helpers';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { CallResultProp } from 'pages/ClientConfiguration/types';
import findIndex from 'lodash.findindex';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';
// import { isString } from 'app/_libraries/_dls/lodash';

export const updateCallResult = createAsyncThunk<
  undefined,
  UpdateCallResultConfigurationArgs,
  ThunkAPIConfig
>('clientConfiguration/updateCallResult', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;
  const { data, updateAction } = args;

  const { clientConfigCallResult } = getState();

  const existingCallResultList = clientConfigCallResult.list;

  const actionCodesModel =
    getState().mapping.data?.clientConfigurationActionCodes || {};

  const newUpdateData = [...(existingCallResultList || [])];
  const prepareDate = {
    ...data
  } as CallResult;

  if (updateAction === 'add') {
    newUpdateData.push(prepareDate);
  } else {
    let prevRecord: CallResult | undefined;
    switch (updateAction) {
      case 'activate':
        prevRecord = clientConfigCallResult.activate?.record;
        break;
      case 'deactivate':
        prevRecord = clientConfigCallResult.deactivate?.record;
        break;
      default:
        prevRecord = clientConfigCallResult.update?.record;
    }

    const editingEditIndex = findIndex(existingCallResultList, prevRecord);
    newUpdateData.splice(editingEditIndex, 1, prepareDate);
  }

  const actionCodes = mappingDataFromArray<CallResultProp>(
    newUpdateData,
    actionCodesModel,
    true
  );

  const response = await dispatch(
    clientConfigurationActions.updateClientInfo({
      actionCodes
    })
  );
  if (isRejected(response)) return thunkAPI.rejectWithValue({});
});

export const triggerActivateCallResult = createAsyncThunk<
  void,
  UpdateCallResultConfigurationArgs,
  ThunkAPIConfig
>('clientConfiguration/triggerActivateCallResult', async (args, thunkAPI) => {
  const { dispatch } = thunkAPI;
  const response = await dispatch(updateCallResult(args));

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_CLIENT_CONFIG.ACTION_CODE_ACTIVATED
        })
      );
      dispatch(clientConfigCallResultActions.toggleActivateModal());
      dispatch(clientConfigCallResultActions.getCallResultList());
    });
  }

  if (isRejected(response)) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_CLIENT_CONFIG.ACTION_CODE_FAILED_TO_ACTIVATE
      })
    );
  }
});

export const triggerDeactivateCallResult = createAsyncThunk<
  void,
  UpdateCallResultConfigurationArgs,
  ThunkAPIConfig
>('clientConfiguration/triggerDeactivateCallResult', async (args, thunkAPI) => {
  const { dispatch } = thunkAPI;
  const response = await dispatch(updateCallResult(args));

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_CLIENT_CONFIG.ACTION_CODE_DEACTIVATED
        })
      );
      dispatch(clientConfigCallResultActions.toggleDeactivateModal());
      dispatch(clientConfigCallResultActions.getCallResultList());
    });
  }

  if (isRejected(response)) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_CLIENT_CONFIG.ACTION_CODE_FAILED_TO_DEACTIVATE
      })
    );
  }
});

export const triggerUpdateCallResult = createAsyncThunk<
  void,
  UpdateCallResultConfigurationArgs,
  ThunkAPIConfig
>('clientConfiguration/triggerUpdateCallResult', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;
  const { data } = args;
  const { clientConfigCallResult } = getState();
  const existingCallResultList = clientConfigCallResult.list;
  const prevRecord = clientConfigCallResult.update?.record;
  if (
    existingCallResultList?.find(
      item => item.value === data.value && prevRecord?.value !== data.value
    )
  ) {
    dispatch(
      clientConfigCallResultActions.setUpdateErrorMessage(
        I18N_CLIENT_CONFIG.ACTION_CODE_ALREADY_EXISTS
      )
    );
    return;
  }

  const response = await dispatch(updateCallResult(args));

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        changeHistoryActions.saveChangedClientConfig({
          action: 'UPDATE',
          changedCategory: 'callResult',
          changedItem: {
            value: prevRecord?.value
          },
          newValue: {
            value: data?.value,
            description: data?.description,
            actionTypeName: prevRecord?.actionTypes[0].actionTypeName,
            nextWorkDateVariable: data?.actionTypes[0]?.nextWorkDateVariable,
            externalStatus: data?.actionTypes[0]?.externalStatusCode,
            externalStatusReason: data?.actionTypes[0]?.externalStatusReason,
            letterId: data?.actionTypes[0]?.letterId,
            memo: data?.actionTypes[0]?.memoText,
            countContact: data?.countContact,
            behaviorType: data?.actionTypes[0]?.chronicleBehaviorIdentifier
          },
          oldValue: {
            value: prevRecord?.value,
            description: prevRecord?.description,
            actionTypeName: prevRecord?.actionTypes[0].actionTypeName,
            nextWorkDateVariable:
              prevRecord?.actionTypes[0]?.nextWorkDateVariable,
            externalStatus: prevRecord?.actionTypes[0]?.externalStatusCode,
            externalStatusReason:
              prevRecord?.actionTypes[0]?.externalStatusReason,
            letterId: prevRecord?.actionTypes[0]?.letterId,
            memo: prevRecord?.actionTypes[0]?.memoText,
            countContact: prevRecord?.countContact,
            behaviorType:
              prevRecord?.actionTypes[0]?.chronicleBehaviorIdentifier
          }
        })
      );
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_CLIENT_CONFIG.ACTION_CODE_UPDATED
        })
      );
      dispatch(clientConfigCallResultActions.getCallResultList());
    });
  }

  if (isRejected(response)) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_CLIENT_CONFIG.ACTION_CODE_FAILED_TO_UPDATE
      })
    );
  }
});

export const updateCallResultBuilder = (
  builder: ActionReducerMapBuilder<CallResultState>
) => {
  builder
    .addCase(updateCallResult.pending, (draftState, action) => {
      draftState.update = {
        ...draftState.update,
        loading: true
      };
    })
    .addCase(updateCallResult.fulfilled, (draftState, action) => {
      draftState.update = {
        ...draftState.update,
        loading: false,
        open: false
      };
    })
    .addCase(updateCallResult.rejected, (draftState, action) => {
      draftState.update = {
        ...draftState.update,
        loading: false
      };
    });
};
