import { createStore, Store } from '@reduxjs/toolkit';
import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator
} from 'app/test-utils';

import { rootReducer } from 'storeConfig';

import accountDetailService from 'pages/AccountDetails/AccountDetailSnapshot/accountDetailService';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { getCallResultList } from './getCallResultList';

const spyAccountDetailService = mockActionCreator(accountDetailService);
const spyClientConfiguration = mockActionCreator(clientConfigurationActions);

beforeEach(() => {
  spyAccountDetailService('getExternalStatusRefData');
  spyAccountDetailService('getReasonRefData');
  spyClientConfiguration('getClientInfoData');
});

describe('should have test updateClientContact', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {
      clientConfigClientContactInformation: {},
      mapping: {
        loading: false,
        data: {
          clientConfigurationActionCodes: {
            externalStatus: 'externalStatus'
          }
        }
      }
    });
  });

  it('should return undefined', async () => {
    const mockGetExternalStatusRefData = spyAccountDetailService(
      'getExternalStatusRefData'
    );
    const mockGetReasonRefData = spyAccountDetailService('getReasonRefData');
    const mockGetClientInfoData = spyClientConfiguration('getClientInfoData');

    store = createStore(rootReducer, {});

    const response = await getCallResultList()(
      byPassFulfilled(getCallResultList.fulfilled.type, {}),
      store.getState,
      {}
    );

    expect(mockGetExternalStatusRefData).toBeCalled();
    expect(mockGetReasonRefData).toBeCalled();
    expect(mockGetClientInfoData).toBeCalledWith({ dataType: 'actionCodes' });
    expect(response.payload).toEqual({
      externalStatusCodeRefData: undefined,
      externalStatusReasonRefData: undefined,
      list: []
    });
  });

  it('should return data', async () => {
    const mockGetExternalStatusRefData = spyAccountDetailService(
      'getExternalStatusRefData'
    );
    const mockGetReasonRefData = spyAccountDetailService('getReasonRefData');
    const mockGetClientInfoData = spyClientConfiguration('getClientInfoData');

    const response = await getCallResultList()(
      byPassFulfilled(getCallResultList.fulfilled.type, {
        actionCodes: [
          { externalStatus: '' },
          { externalStatus: 'externalStatus' }
        ]
      }),
      store.getState,
      {}
    );

    expect(mockGetExternalStatusRefData).toBeCalled();
    expect(mockGetReasonRefData).toBeCalled();
    expect(mockGetClientInfoData).toBeCalledWith({ dataType: 'actionCodes' });
    expect(response.payload).toEqual({
      externalStatusCodeRefData: undefined,
      externalStatusReasonRefData: undefined,
      list: [{ externalStatus: ' ' }, { externalStatus: 'externalStatus' }]
    });
  });

  it('error', async () => {
    const response = await getCallResultList()(
      byPassRejected(getCallResultList.rejected.type, {}),
      store.getState,
      {}
    );

    expect(response.payload).toEqual({});
  });

  it('pending', () => {
    const action = getCallResultList.pending(
      getCallResultList.pending.type,
      undefined
    );
    const actual = rootReducer(store.getState(), action).clientConfigCallResult;

    expect(actual.loading).toEqual(true);
  });

  it('fulfilled', () => {
    const action = getCallResultList.fulfilled(
      {
        list: [],
        externalStatusCodeRefData: [],
        externalStatusReasonRefData: []
      },
      getCallResultList.fulfilled.type,
      undefined
    );
    const actual = rootReducer(store.getState(), action).clientConfigCallResult;

    expect(actual.loading).toEqual(false);
  });

  it('rejected', () => {
    const action = getCallResultList.rejected(
      new Error(),
      getCallResultList.rejected.type,
      undefined
    );
    const actual = rootReducer(store.getState(), action).clientConfigCallResult;

    expect(actual.loading).toEqual(false);
    expect(actual.error).toEqual(true);
  });
});
