import * as selectors from './selector';

import { selectorWrapper } from 'app/test-utils';

const initialState: Partial<RootState> = {
  clientConfigCallResult: {
    loading: true,
    list: [{ id: 'id', value: 'value', description: 'desc' }],
    add: { open: true, errorMessage: 'errorMessage' },
    update: { open: true, errorMessage: 'errorMessage' },
    activate: { open: true },
    deactivate: { open: true },
    externalStatusReasonRefData: [{ value: 'value', description: 'desc' }],
    externalStatusCodeRefData: [{ value: 'value', description: 'desc' }],
    sortBy: { id: 'id', order: null }
  }
};

const selectorTrigger = selectorWrapper(initialState);

describe('Selector SharedInfo', () => {
  it('selectCallResultList', () => {
    const { data } = selectorTrigger(selectors.selectCallResultList);

    expect(data).toEqual(initialState.clientConfigCallResult!.list);
  });

  it('selectGridData', () => {
    const { data } = selectorTrigger(selectors.selectGridData);

    expect(data).toEqual(initialState.clientConfigCallResult!.list);
  });

  it('selectTotalRecords', () => {
    const { data } = selectorTrigger(selectors.selectTotalRecords);

    expect(data).toEqual(initialState.clientConfigCallResult!.list!.length);
  });

  it('selectLoading', () => {
    const { data } = selectorTrigger(selectors.selectLoading);

    expect(data).toEqual(initialState.clientConfigCallResult!.loading);
  });

  it('selectAddErrorMessage', () => {
    const { data } = selectorTrigger(selectors.selectAddErrorMessage);

    expect(data).toEqual(
      initialState.clientConfigCallResult!.add!.errorMessage
    );
  });

  it('selectLoadingAdd', () => {
    const { data } = selectorTrigger(selectors.selectLoadingAdd);

    expect(data).toEqual(initialState.clientConfigCallResult!.add!.loading);
  });

  it('selectOpenAddModal', () => {
    const { data } = selectorTrigger(selectors.selectOpenAddModal);

    expect(data).toEqual(initialState.clientConfigCallResult!.add!.open);
  });

  it('selectOpenUpdateModal', () => {
    const { data } = selectorTrigger(selectors.selectOpenUpdateModal);

    expect(data).toEqual(initialState.clientConfigCallResult!.update!.open);
  });

  it('selectLoadingUpdate', () => {
    const { data } = selectorTrigger(selectors.selectLoadingUpdate);

    expect(data).toEqual(initialState.clientConfigCallResult!.update!.loading);
  });

  it('selectUpdateErrorMessage', () => {
    const { data } = selectorTrigger(selectors.selectUpdateErrorMessage);

    expect(data).toEqual(
      initialState.clientConfigCallResult!.update!.errorMessage
    );
  });

  it('selectRecordUpdateModal', () => {
    const { data } = selectorTrigger(selectors.selectRecordUpdateModal);

    expect(data).toEqual(initialState.clientConfigCallResult!.update!.record);
  });

  it('selectOpenActivateModal', () => {
    const { data } = selectorTrigger(selectors.selectOpenActivateModal);

    expect(data).toEqual(initialState.clientConfigCallResult!.activate!.open);
  });

  it('selectLoadingActivate', () => {
    const { data } = selectorTrigger(selectors.selectLoadingActivate);

    expect(data).toEqual(
      initialState.clientConfigCallResult!.activate!.loading
    );
  });

  it('selectRecordActivateModal', () => {
    const { data } = selectorTrigger(selectors.selectRecordActivateModal);

    expect(data).toEqual(initialState.clientConfigCallResult!.activate!.record);
  });

  it('selectOpenDeactivateModal', () => {
    const { data } = selectorTrigger(selectors.selectOpenDeactivateModal);

    expect(data).toEqual(initialState.clientConfigCallResult!.deactivate!.open);
  });

  it('selectLoadingDeactivate', () => {
    const { data } = selectorTrigger(selectors.selectLoadingDeactivate);

    expect(data).toEqual(
      initialState.clientConfigCallResult!.deactivate!.loading
    );
  });

  it('selectRecordDeactivateModal', () => {
    const { data } = selectorTrigger(selectors.selectRecordDeactivateModal);

    expect(data).toEqual(
      initialState.clientConfigCallResult!.deactivate!.record
    );
  });

  it('selectExternalStatusCodeRefData', () => {
    const { data } = selectorTrigger(selectors.selectExternalStatusCodeRefData);

    expect(data).toEqual(
      initialState.clientConfigCallResult!.externalStatusCodeRefData
    );
  });

  it('selectExternalStatusReasonRefData', () => {
    const { data } = selectorTrigger(
      selectors.selectExternalStatusReasonRefData
    );

    expect(data).toEqual(
      initialState.clientConfigCallResult!.externalStatusReasonRefData
    );
  });
});
