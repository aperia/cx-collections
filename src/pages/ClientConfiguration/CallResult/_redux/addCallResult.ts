import { CallResultState, UpdateCallResultConfigurationArgs } from './../types';
import { I18N_CLIENT_CONFIG } from 'app/constants/i18n';
import { batch } from 'react-redux';
import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isRejected,
  isFulfilled
} from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { clientConfigCallResultActions } from './reducers';
import { updateCallResult } from './updateCallResult';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

export const triggerAddCallResult = createAsyncThunk<
  void,
  UpdateCallResultConfigurationArgs,
  ThunkAPIConfig
>('clientConfiguration/triggerAddCallResult', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;
  const { data } = args;
  const { clientConfigCallResult } = getState();
  const existingCallResultList = clientConfigCallResult.list;
  if (existingCallResultList?.find(item => item.value === data.value)) {
    dispatch(
      clientConfigCallResultActions.setAddErrorMessage(
        I18N_CLIENT_CONFIG.ACTION_CODE_ALREADY_EXISTS
      )
    );
    return;
  }

  const response = await dispatch(updateCallResult(args));

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        changeHistoryActions.saveChangedClientConfig({
          action: 'ADD',
          changedCategory: 'callResult',
          changedItem: {
            value: data?.value
          },
          newValue: {
            value: data?.value,
            description: data?.description,
            nextWorkDateVariable: data?.actionTypes[0]?.nextWorkDateVariable,
            externalStatus: data?.actionTypes[0]?.externalStatusCode,
            externalStatusReason: data?.actionTypes[0]?.externalStatusReason,
            letterId: data?.actionTypes[0]?.letterId,
            memo: data?.actionTypes[0]?.memoText,
            countContact: data?.countContact,
            behaviorType: data?.actionTypes[0]?.chronicleBehaviorIdentifier
          }
        })
      );
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_CLIENT_CONFIG.ACTION_CODE_ADDED
        })
      );
      dispatch(clientConfigCallResultActions.toggleAddModal());
      dispatch(clientConfigCallResultActions.getCallResultList());
    });
  }

  if (isRejected(response)) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_CLIENT_CONFIG.ACTION_CODE_FAILED_TO_ADD
      })
    );
  }
});

export const addCallResultBuilder = (
  builder: ActionReducerMapBuilder<CallResultState>
) => {
  builder
    .addCase(triggerAddCallResult.pending, (draftState, action) => {
      draftState.add = {
        ...draftState.add,
        loading: true
      };
    })
    .addCase(triggerAddCallResult.fulfilled, (draftState, action) => {
      draftState.add = {
        ...draftState.add,
        loading: false
      };
    })
    .addCase(triggerAddCallResult.rejected, (draftState, action) => {
      draftState.add = {
        ...draftState.add,
        loading: false
      };
    });
};
