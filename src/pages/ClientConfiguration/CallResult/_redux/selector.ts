import { createSelector } from '@reduxjs/toolkit';
import { SortType } from 'app/_libraries/_dls/components';
import { orderBy } from 'app/_libraries/_dls/lodash';
import { CallResult } from '../types';

const getCallResult = (states: RootState) => states.clientConfigCallResult;

const getCallResultList = (states: RootState) =>
  states.clientConfigCallResult?.list || [];

export const getSortBy = (state: RootState): SortType => {
  return state.clientConfigCallResult.sortBy as any;
};

export const selectCallResultList = createSelector(
  getCallResultList,
  (callResultList: CallResult[]) => callResultList
);

export const selectGridData = createSelector(
  [selectCallResultList, getSortBy],
  (data, sortBy) => {

    return orderBy(
      data,
      [
        (item: CallResult) =>
          item[sortBy?.id as keyof CallResult]?.toString().toLowerCase(),
        item => item.value?.toLowerCase()
      ],
      [sortBy.order || false, 'asc']
    );
  }
);

export const selectTotalRecords = createSelector(
  selectCallResultList,
  callResultList => callResultList?.length || 0
);

export const selectLoading = createSelector(
  getCallResult,
  callResultList => callResultList?.loading || false
);

export const selectAddErrorMessage = createSelector(
  getCallResult,
  callResultList => callResultList?.add?.errorMessage
);

export const selectLoadingAdd = createSelector(
  getCallResult,
  callResultList => callResultList?.add?.loading
);

export const selectOpenAddModal = createSelector(
  getCallResult,
  callResultList => callResultList?.add?.open
);

export const selectOpenUpdateModal = createSelector(
  getCallResult,
  callResultList => callResultList?.update?.open
);

export const selectLoadingUpdate = createSelector(
  getCallResult,
  callResultList => callResultList?.update?.loading
);

export const selectUpdateErrorMessage = createSelector(
  getCallResult,
  callResultList => callResultList?.update?.errorMessage
);

export const selectRecordUpdateModal = createSelector(
  getCallResult,
  callResultList => callResultList?.update?.record
);

export const selectOpenActivateModal = createSelector(
  getCallResult,
  callResultList => callResultList?.activate?.open
);

export const selectLoadingActivate = createSelector(
  getCallResult,
  callResultList => callResultList?.activate?.loading
);

export const selectRecordActivateModal = createSelector(
  getCallResult,
  callResultList => callResultList?.activate?.record
);

export const selectOpenDeactivateModal = createSelector(
  getCallResult,
  callResultList => callResultList?.deactivate?.open
);

export const selectLoadingDeactivate = createSelector(
  getCallResult,
  callResultList => callResultList?.deactivate?.loading
);

export const selectRecordDeactivateModal = createSelector(
  getCallResult,
  callResultList => callResultList?.deactivate?.record
);

export const selectExternalStatusCodeRefData = createSelector(
  getCallResult,
  callResultList => callResultList?.externalStatusCodeRefData || []
);

export const selectExternalStatusReasonRefData = createSelector(
  getCallResult,
  callResultList => callResultList?.externalStatusReasonRefData || []
);
