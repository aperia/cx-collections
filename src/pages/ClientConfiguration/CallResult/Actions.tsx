import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { Button, Popover } from 'app/_libraries/_dls/components';
import { replaceMentionWith } from 'app/_libraries/_dls/components/TextEditor';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { View } from 'app/_libraries/_dof/core';
import classNames from 'classnames';
import { isUndefined } from 'lodash';
import React, { useCallback, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CallResult } from './types';
import { clientConfigCallResultActions } from './_redux/reducers';
import {
  selectExternalStatusCodeRefData,
  selectExternalStatusReasonRefData
} from './_redux/selector';

export interface ActionsProps extends DLSId {
  record: CallResult;
  status: boolean;
}

export const Actions: React.FC<ActionsProps> = ({
  status,
  record,
  dataTestId
}) => {
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const [active, setActive] = useState<boolean>(false);
  const externalStatusCodeRefData = useSelector(
    selectExternalStatusCodeRefData
  );
  const externalStatusReasonRefData = useSelector(
    selectExternalStatusReasonRefData
  );
  const getExternalStatusCode = useCallback(
    (givenValue?: string) =>
      externalStatusCodeRefData.find(
        item => item.value === givenValue //action?.externalStatus
      ),
    [externalStatusCodeRefData]
  );

  const getExternalStatusReason = useCallback(
    (givenValue?: string) =>
      externalStatusReasonRefData.find(
        item => item.value === givenValue //action?.externalStatusReason
      ),
    [externalStatusReasonRefData]
  );

  const getViewElements = useCallback(() => {
    return record?.actionTypes?.map((action, index) => {
      const externalStatusCode = getExternalStatusCode(
        action?.externalStatusCode
      );
      const externalStatusReason = getExternalStatusReason(
        action?.externalStatusReason
      );

      const viewRecord = {
        actionTypeHeader: action?.actionTypeDescription,
        actionTypeName: action?.actionTypeName,
        nextWorkDateVariable: action?.nextWorkDateVariable,
        externalStatusCode: !isUndefined(action?.externalStatusCode)
          ? `${externalStatusCode?.value} - ${t(
              externalStatusCode?.description
            )}`
          : '',
        externalStatusReason: !isUndefined(action?.externalStatusReason)
          ? `${externalStatusReason?.value} - ${t(
              externalStatusReason?.description
            )}`
          : '',
        letterId: action?.letterId,
        behaviourType: action?.chronicleBehaviorIdentifier,
        memoText: replaceMentionWith(action?.memoText || '')
          .split('{')
          .join('<span class="dls-editor-mention-decorator">')
          .split('}')
          .join('</span>')
          ?.replace(/\\/g, '')
      };

      return (
        <div key={`clientConfigCallResultInfoView-${index}`}>
          {index > 0 && <div className="divider-dashed my-16" />}
          <View
            descriptor="clientConfigCallResultInfoView"
            formKey={`clientConfigCallResultInfoView-${index}`}
            value={viewRecord}
            dataTestId={genAmtId(
              dataTestId,
              'clientConfigCallResultInfoView',
              ''
            )}
          />
        </div>
      );
    });
  }, [
    dataTestId,
    getExternalStatusCode,
    getExternalStatusReason,
    record?.actionTypes,
    t
  ]);

  const handleActivateRequest = () => {
    dispatch(
      clientConfigCallResultActions.toggleActivateModal({
        open: true,
        record
      })
    );
  };

  const handleDeactivateRequest = () => {
    dispatch(
      clientConfigCallResultActions.toggleDeactivateModal({
        open: true,
        record
      })
    );
  };

  const handleEditRequest = () => {
    dispatch(
      clientConfigCallResultActions.toggleUpdateModal({
        open: true,
        record
      })
    );
  };

  const handleVisibilityChange = (opened: boolean) => setActive(opened);

  return (
    <>
      {record.activated ? (
        <Button
          disabled={status}
          onClick={handleDeactivateRequest}
          size="sm"
          variant={'outline-danger'}
          dataTestId={genAmtId(dataTestId, 'deactive', '')}
        >
          {t(I18N_COMMON_TEXT.DEACTIVATE)}
        </Button>
      ) : (
        <Button
          disabled={status}
          onClick={handleActivateRequest}
          size="sm"
          variant={'outline-primary'}
          dataTestId={genAmtId(dataTestId, 'activate', '')}
        >
          {t(I18N_COMMON_TEXT.ACTIVATE)}
        </Button>
      )}

      <Popover
        triggerClassName="d-inline-block ml-8"
        popoverInnerClassName="scroll-popover"
        size="xl"
        placement="bottom-end"
        onVisibilityChange={handleVisibilityChange}
        element={getViewElements()}
      >
        <Button
          size="sm"
          variant="outline-primary"
          className={classNames({ active })}
          dataTestId={genAmtId(dataTestId, 'view', '')}
        >
          {t(I18N_COMMON_TEXT.VIEW)}
        </Button>
      </Popover>
      <Button
        disabled={status}
        onClick={handleEditRequest}
        size="sm"
        variant="outline-primary"
        className="ml-8"
        dataTestId={genAmtId(dataTestId, 'edit', '')}
      >
        {t(I18N_COMMON_TEXT.EDIT)}
      </Button>
    </>
  );
};
