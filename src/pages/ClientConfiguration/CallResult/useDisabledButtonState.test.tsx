import React, { createRef, useEffect, useState } from 'react';
import { ADD_FORM_KY } from './constant';

import { useDisabledButtonState } from './useDisabledButtonState';
import { renderMockStore } from 'app/test-utils';
import { screen } from '@testing-library/dom';

const Comp = ({
  editorRefs,
  viewRef,
  errorMessage,
  isOpenModal,
  contactValue,
  action,
  behaviourTypeRef,
  behaviours,
  memos
}: MagicKeyValue) => {
  const [parentRefs, setParentRefs] = useState<
    React.MutableRefObject<HTMLDivElement | null>[]
  >([]);

  useEffect(() => {
    setParentRefs(elRefs =>
      Array(3)
        .fill(null)
        .map((_, i) => elRefs[i] || createRef())
    );
  }, []);
  const {
    isDisabledOk,
    handleBlurEditor,
    errorEditor,
    handleBlurBehaviourType,
    errorBehaviours,
    handleDirtyFormInner
  } = useDisabledButtonState(
    ADD_FORM_KY,
    editorRefs,
    viewRef,
    parentRefs,
    errorMessage,
    isOpenModal,
    contactValue,
    undefined,
    action || 'Add',
    behaviourTypeRef,
    behaviours,
    memos
  );

  return (
    <>
      <div ref={parentRefs[0]}>
        <div contentEditable={true}></div>
        <div data-testid="Comp_isErrorEditor">
          {errorEditor ? 'true' : 'false'}
        </div>
        <div data-testid="Comp_handleBlur" onClick={handleBlurEditor} />
        <div data-testid="Comp_isErrorBehaviour">
          {errorBehaviours ? 'true' : 'false'}
        </div>
        <div
          data-testid="Comp_handleDirty1"
          onClick={() => handleDirtyFormInner(0)(true)}
        />
        <div
          data-testid="Comp_handleDirty2"
          onClick={() => handleDirtyFormInner(0)(true)}
        />
        <div
          data-testid="Comp_handleBlurBehaviour"
          onClick={handleBlurBehaviourType}
        />
      </div>
      <div ref={parentRefs[1]}>
        <div contentEditable={true}>abc([])</div>
      </div>
      <div ref={parentRefs[2]}>
        <div contentEditable={true}>xyz</div>
      </div>
      <div data-testid="Comp_isDisabledOk">
        {isDisabledOk ? 'true' : 'false'}
      </div>
    </>
  );
};

const Comp1 = ({
  editorRefs,
  viewRef,
  errorMessage,
  isOpenModal,
  contactValue,
  action,
  behaviourTypeRef,
  behaviours,
  memos
}: MagicKeyValue) => {
  const [parentRefs, setParentRefs] = useState<
    React.MutableRefObject<HTMLDivElement | null>[]
  >([]);

  useEffect(() => {
    setParentRefs(elRefs =>
      Array(1)
        .fill(null)
        .map((_, i) => elRefs[i] || createRef())
    );
  }, []);

  const {
    isDisabledOk,
    handleBlurEditor,
    handleBlurBehaviourType,
    handleDirtyFormInner
  } = useDisabledButtonState(
    ADD_FORM_KY,
    editorRefs,
    viewRef,
    parentRefs,
    errorMessage,
    isOpenModal,
    contactValue,
    undefined,
    action || 'Add',
    behaviourTypeRef,
    behaviours,
    memos
  );

  return (
    <>
      <div ref={parentRefs[0]}>
        <div contentEditable={true}>abc</div>
        <div data-testid="Comp_handleBlur" onClick={handleBlurEditor} />
        <div
          data-testid="Comp_handleDirty1"
          onClick={() => handleDirtyFormInner(0)(true)}
        />
        <div
          data-testid="Comp_handleBlurBehaviour"
          onClick={handleBlurBehaviourType}
        />
      </div>
      <div data-testid="Comp_isDisabledOk">
        {isDisabledOk ? 'true' : 'false'}
      </div>
    </>
  );
};

describe('', () => {
  it('should render', () => {
    renderMockStore(<Comp memos={['memo']} behaviours={['test']} />, {
      initialState: {}
    });

    expect(screen.getByTestId('Comp_isDisabledOk').textContent).toEqual('true');
    expect(screen.getByTestId('Comp_isErrorEditor').textContent).toEqual(
      'true'
    );
    expect(screen.getByTestId('Comp_isErrorBehaviour').textContent).toEqual(
      'true'
    );
  });

  it('should render ưith edit mode', () => {
    renderMockStore(
      <Comp action="Edit" memos={['memo']} behaviours={['test']} />,
      {
        initialState: {}
      }
    );

    expect(screen.getByTestId('Comp_isDisabledOk').textContent).toEqual('true');
    expect(screen.getByTestId('Comp_isErrorEditor').textContent).toEqual(
      'true'
    );
    expect(screen.getByTestId('Comp_isErrorBehaviour').textContent).toEqual(
      'true'
    );
  });

  it('should render when open', () => {
    renderMockStore(<Comp isOpenModal />, { initialState: {} });

    expect(screen.getByTestId('Comp_isDisabledOk').textContent).toEqual('true');
    expect(screen.getByTestId('Comp_isErrorEditor').textContent).toEqual(
      'true'
    );
  });

  it('should render with error', () => {
    const setOthers = jest.fn();

    const viewRef = {
      current: {
        props: {
          onFind: () => ({
            props: {
              props: {
                setOthers: (action: Function) => {
                  action({ options: {} });
                  setOthers();
                }
              }
            }
          })
        }
      }
    };

    const behaviourRefs = [
      {
        current: {
          inputElement: {
            value: ''
          }
        }
      },
      {
        current: {
          inputElement: {
            value: 'test()'
          }
        }
      },
      {
        current: {
          inputElement: {
            value: 'abc'
          }
        }
      }
    ];

    renderMockStore(
      <Comp
        viewRef={viewRef}
        behaviourTypeRef={behaviourRefs}
        errorMessage="error"
        action="Edit"
        memos={['memo']}
        behaviours={['test']}
      />,
      {
        initialState: {}
      }
    );

    screen.getByTestId('Comp_handleBlurBehaviour').click();
    screen.getByTestId('Comp_handleBlur').click();
    screen.getByTestId('Comp_handleDirty1').click();
    screen.getByTestId('Comp_handleDirty2').click();

    expect(screen.getByTestId('Comp_isDisabledOk').textContent).toEqual('true');
    expect(screen.getByTestId('Comp_isErrorEditor').textContent).toEqual(
      'true'
    );
  });

  it('should render with disabledOK is true when contactValue is undefined', () => {
    const setOthers = jest.fn();

    const viewRef = {
      current: {
        props: {
          onFind: () => ({
            props: {
              props: {
                setOthers: (action: Function) => {
                  action({ options: {} });
                  setOthers();
                }
              }
            }
          })
        }
      }
    };

    const behaviourRefs = [
      {
        current: {
          inputElement: {
            value: ''
          }
        }
      },
      {
        current: {
          inputElement: {
            value: 'test()'
          }
        }
      },
      {
        current: {
          inputElement: {
            value: 'abc'
          }
        }
      }
    ];

    renderMockStore(
      <Comp1 viewRef={viewRef} behaviourTypeRef={behaviourRefs} />,
      {
        initialState: {}
      }
    );

    screen.getByTestId('Comp_handleBlur').click();
    screen.getByTestId('Comp_handleDirty1').click();

    expect(screen.getByTestId('Comp_isDisabledOk').textContent).toEqual('true');
  });
});

describe('Action', () => {
  it('handleBlur', () => {
    renderMockStore(<Comp />, { initialState: {} });

    screen.getByTestId('Comp_handleBlur').click();

    expect(screen.getByTestId('Comp_isDisabledOk').textContent).toEqual('true');
  });
});
