export interface CallResultActionType {
  nextWorkDateVariable?: string;
  externalStatusCode?: string;
  externalStatusReason?: string;
  memoText?: string;
  letterId?: string;
  actionTypeName?: string;
  actionTypeDescription?: string;
  chronicleBehaviorIdentifier?: string;
}

export interface CallResult {
  id: string;
  value: string;
  description: string;
  actionTypes: CallResultActionType[];
  countContact?: boolean;
  activated?: boolean;
}

export interface GetCallResultListPayload {
  list: CallResult[];
  externalStatusCodeRefData: RefDataValue[];
  externalStatusReasonRefData: RefDataValue[];
}
export interface UpdateCallResultConfigurationArgs {
  data: CallResult;
  updateAction?: 'add' | 'deactivate' | 'activate';
}

export interface CallResultState {
  list?: CallResult[];
  sortBy: MagicKeyValue;
  loading?: boolean;
  error?: boolean;
  add?: {
    loading?: boolean;
    open?: boolean;
    errorMessage?: string;
  };
  update?: {
    loading?: boolean;
    open?: boolean;
    errorMessage?: string;
    record?: CallResult;
  };
  activate?: {
    loading?: boolean;
    open?: boolean;
    record?: CallResult;
  };
  deactivate?: {
    loading?: boolean;
    open?: boolean;
    record?: CallResult;
  };
  externalStatusCodeRefData?: RefDataValue[];
  externalStatusReasonRefData?: RefDataValue[];
}

export interface ChangePageSize {
  pageSize: number;
}
export interface ChangePage {
  page: number;
}

export interface ToggleUpdatePayload {
  open?: boolean;
  record?: CallResult;
}

export interface ToggleActivatePayload {
  open?: boolean;
  record?: CallResult;
}

export interface ToggleDeactivatePayload {
  open?: boolean;
  record?: CallResult;
}

export interface ToggleAddPayload {
  open?: boolean;
  record?: CallResult;
}
