// app utils
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { useStoreIdSelector } from 'app/hooks';

// components
import {
  Icon,
  InlineMessage,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  Radio,
  TextBox,
  TextBoxRef
} from 'app/_libraries/_dls/components';
import ModalBodyWithErrorClientConfig from '../ApiErrorSection/ModalBodyWithErrorClientConfig';
import TextEditor from 'app/_libraries/_dls/components/TextEditor';
import { TextEditorRef } from 'app/_libraries/_dls/components/TextEditor/types';

// hooks
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import React, { ChangeEvent, useRef, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  selectAddErrorMessage,
  selectLoadingAdd,
  selectOpenAddModal
} from './_redux/selector';

// redux
import { clientConfigCallResultActions } from './_redux/reducers';
import { View } from 'app/_libraries/_dof/core';
import { getFormValues } from 'redux-form';
import { CallResult, CallResultActionType } from './types';

// constant
import {
  ADD_FORM_KY,
  CONTACT_VALUE,
  FORM_NAME,
  MEMO_MAX_LENGTH,
  MENTION_DATA
} from './constant';
import { useDisabledButtonState } from './useDisabledButtonState';
import { formatCommon } from 'app/helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';

const DEFAULT_BEHAVIOUR_TYPE = ['CXCOLL'];

const AddModal = () => {
  const { t } = useTranslation();
  const testId = 'callResult_addActionCodeModal';

  const dispatch = useDispatch();

  const editorRef = useRef<TextEditorRef | null>(null);

  const viewRef = useRef<any>(null);

  const parantRef = useRef<HTMLDivElement | null>(null);

  const behaviourTypeRef = useRef<TextBoxRef | null>(null);

  const [memoLength, setMemoLength] = useState(0);

  const [behaviourTypeValue, setBehaviourTypeValue] = useState<
    Record<string, string>
  >({});

  const [contactValue, setContactValue] =
    useState<boolean | undefined>(undefined);

  const isOpenModal = useStoreIdSelector<boolean>(selectOpenAddModal);

  const isLoading = useStoreIdSelector<boolean>(selectLoadingAdd);

  const errorMessage = useStoreIdSelector<string>(selectAddErrorMessage);

  const formValue = useSelector(getFormValues(ADD_FORM_KY)) as Exclude<
    CallResult,
    'actionTypes'
  > &
    Omit<
      CallResultActionType,
      'externalStatusCode' | 'externalStatusReason'
    > & {
      externalStatusCode: RefDataValue;
      externalStatusReason: RefDataValue;
    };

  const {
    isDisabledOk,
    handleBlurEditor,
    handleBlurBehaviourType,
    errorEditor,
    errorBehaviours,
    isSpecialCharacter
  } = useDisabledButtonState(
    ADD_FORM_KY,
    [editorRef],
    viewRef,
    [parantRef],
    errorMessage,
    isOpenModal,
    contactValue,
    undefined,
    'Add',
    [behaviourTypeRef],
    DEFAULT_BEHAVIOUR_TYPE
  );

  const onSubmit = () => {
    const behaviourType = behaviourTypeRef.current?.inputElement?.value.trim();
    const editorText = editorRef.current?.getMarkdown();

    dispatch(
      clientConfigCallResultActions.triggerAddCallResult({
        data: {
          value: formValue?.value,
          description: formValue?.description,
          actionTypes: [
            {
              actionTypeName: 'default',
              actionTypeDescription: formValue?.description,
              nextWorkDateVariable: formValue?.nextWorkDateVariable,
              externalStatusCode: formValue?.externalStatusCode?.value,
              externalStatusReason: formValue?.externalStatusReason?.value,
              letterId: formValue?.letterId,
              chronicleBehaviorIdentifier: behaviourType,
              memoText: editorText
            }
          ],
          countContact: contactValue,
          activated: true
        } as CallResult,
        updateAction: 'add'
      })
    );
  };

  const handleCloseModal = () => {
    dispatch(clientConfigCallResultActions.toggleAddModal());
    setContactValue(undefined);
    setBehaviourTypeValue(prev => ({
      ...prev,
      [0]: DEFAULT_BEHAVIOUR_TYPE[0]
    }));
  };

  const handlePlainTextChange = (nextPlainText: string) => {
    setMemoLength(nextPlainText?.length);
  };

  const handleChangeContact = (event: React.ChangeEvent<HTMLInputElement>) => {
    setContactValue(event.target.value === '1');
  };

  const handleOnChangeBehaviorType = (
    e: ChangeEvent<HTMLInputElement>,
    idx: number
  ) => {
    const { value } = e.target;

    setBehaviourTypeValue(prev => ({
      ...prev,
      [idx]: value
    }));
  };

  return (
    <Modal show={isOpenModal} loading={isLoading} sm dataTestId={testId}>
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId={`${testId}_header`}
      >
        <ModalTitle dataTestId={`${testId}_title`}>
          {t(I18N_CLIENT_CONFIG.ADD_ACTION_CODE)}
        </ModalTitle>
      </ModalHeader>
      <ModalBodyWithErrorClientConfig
        apiErrorClassName="mb-24"
        dataTestId={`${testId}_body`}
      >
        {errorMessage && (
          <InlineMessage
            className="mb-24"
            variant="danger"
            dataTestId={`${testId}_errorMessage`}
          >
            {t(errorMessage)}
          </InlineMessage>
        )}
        <View
          id={ADD_FORM_KY}
          descriptor={FORM_NAME}
          formKey={ADD_FORM_KY}
          ref={viewRef}
        />
        <div className="d-flex mt-16 mb-8">
          <h6
            className="color-grey"
            data-testid={genAmtId(testId, 'memoSingular', '')}
          >
            {t(I18N_COMMON_TEXT.MEMO_SINGULAR)}
          </h6>
        </div>
        <TextBox
          ref={behaviourTypeRef}
          name="chronicleBehaviorIdentifier"
          label={t('txt_behavior_type')}
          defaultValue={DEFAULT_BEHAVIOUR_TYPE[0]}
          onBlur={handleBlurBehaviourType}
          onChange={e => handleOnChangeBehaviorType(e, 0)}
          value={behaviourTypeValue[0]}
          className="mb-16"
          error={{
            status: errorBehaviours[0],
            message: t(I18N_CLIENT_CONFIG.BEHAVIOR_TYPE_IS_REQUIRED)
          }}
          maxLength={10}
          dataTestId={`${testId}_behaviourType`}
        />
        <p
          className="mb-16"
          data-testid={genAmtId(testId, 'typeSlashToAddVariables-title', '')}
        >
          {t(I18N_CLIENT_CONFIG.TYPE_SLASH_TO_ADD_VARIABLES)}
        </p>
        <div ref={parantRef}>
          <TextEditor
            ref={editorRef}
            onBlur={handleBlurEditor}
            mention
            mentionData={MENTION_DATA}
            maxPlainText={MEMO_MAX_LENGTH}
            onPlainTextChange={handlePlainTextChange}
            placeholder={t(I18N_CLIENT_CONFIG.ENTER_MEMO_HERE)}
            error={{
              status: errorEditor[0],
              message: t(I18N_CLIENT_CONFIG.MEMO_IS_REQUIRED)
            }}
            dataTestId={`${testId}_memo`}
          />
        </div>
        <p
          className="fs-12 color-grey-l24 mt-8"
          data-testid={genAmtId(testId, 'memoCharacterLeft', '')}
        >
          {`${
            formatCommon(
              MEMO_MAX_LENGTH - memoLength < 0
                ? 0
                : MEMO_MAX_LENGTH - memoLength
            ).quantity
          } ${t(I18N_CLIENT_CONFIG.MEMO_CHARACTERS_LEFT)}`}
        </p>
        {isSpecialCharacter?.[0] && (
          <div
            className="d-flex mt-16 align-items-center"
            data-testid={genAmtId(testId, 'warningSpecialCharacter', '')}
          >
            <Icon name="warning" size="4x" className="color-orange-d08"></Icon>
            <p className="fs-12 color-grey ml-8">
              {t(I18N_CLIENT_CONFIG.ACTION_CODE_MEMO_NON_SPECIAL)}
            </p>
          </div>
        )}
        <div className="d-flex my-16">
          <h6
            className="color-grey"
            data-testid={genAmtId(testId, 'countAsContact-title', '')}
          >
            {t(I18N_CLIENT_CONFIG.COUNT_AS_COUNT)}
          </h6>
          <span className="asterisk color-danger ml-4 mt-n4">*</span>
        </div>
        <Radio
          className="d-inline-block"
          dataTestId={`${testId}_countAsContact-yes`}
        >
          <Radio.Input
            checked={contactValue}
            value={CONTACT_VALUE.YES}
            name="contact-yes"
            onChange={handleChangeContact}
          />
          <Radio.Label>{t(I18N_CLIENT_CONFIG.YES)}</Radio.Label>
        </Radio>
        <Radio
          className="d-inline-block ml-24"
          dataTestId={`${testId}_countAsContact-no`}
        >
          <Radio.Input
            checked={contactValue === false}
            value={CONTACT_VALUE.NO}
            name="contact-no"
            onChange={handleChangeContact}
          />
          <Radio.Label>{t(I18N_CLIENT_CONFIG.NO)}</Radio.Label>
        </Radio>
      </ModalBodyWithErrorClientConfig>
      <ModalFooter
        cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
        okButtonText={t(I18N_COMMON_TEXT.SUBMIT)}
        disabledOk={isDisabledOk}
        onCancel={handleCloseModal}
        onOk={onSubmit}
        dataTestId={`${testId}_footer`}
      />
    </Modal>
  );
};

export default AddModal;
