import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import userEvent from '@testing-library/user-event';

// component
import AddModal from './AddModal';

// redux
import { clientConfigCallResultActions } from './_redux/reducers';

// types
import { fireEvent, screen } from '@testing-library/dom';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { EditorProps } from 'app/_libraries/_dls/components/Editor';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

jest.mock('./useDisabledButtonState', () => {
  return {
    useDisabledButtonState: () => ({
      isDisabledOk: false,
      errorEditor: {
        0: false
      },
      errorBehaviours: {
        0: false
      },
      handleBlurEditor: jest.fn(),
      handleBlurBehaviourType: jest.fn(),
      isSpecialCharacter: {
        0: true
      }
    })
  };
});

jest.mock('app/_libraries/_dls/utils/genAmtId', () => jest.fn());

jest.mock('app/_libraries/_dls/components/TextEditor', () => {
  return {
    __esModule: true,
    default: ({ onPlainTextChange }: EditorProps) => {
      return (
        <div
          data-testid="Editor_onPlainTextChange_2000_words"
          onClick={() => onPlainTextChange!(Array(2000).fill('a').join(''))}
        />
      );
    }
  };
});

jest.mock('app/_libraries/_dls/components/TextBox', () => {
  return {
    __esModule: true,
    default: ({ onChange }: any) => {
      return <input data-testid="TextBox_onChange" onChange={onChange} />;
    }
  };
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const spyClientConfigCallResult = mockActionCreator(
  clientConfigCallResultActions
);

const initialState: Partial<RootState> = {
  clientConfigCallResult: {
    sortBy: { id: 'id' },
    add: {
      open: true,
      errorMessage: 'errorMessage'
    }
  },
  clientConfiguration: { settings: { selectedConfig: { id: 'id' } } }
};

describe('Render', () => {
  it('should render UI', () => {
    renderMockStore(<AddModal />, { initialState });

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.ADD_ACTION_CODE)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleCloseModal', () => {
    const mockAction = spyClientConfigCallResult('toggleAddModal');
    renderMockStore(<AddModal />, { initialState });

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));

    expect(mockAction).toBeCalled();
  });

  it('onSubmit', () => {
    const mockAction = spyClientConfigCallResult('triggerAddCallResult');
    renderMockStore(<AddModal />, { initialState });

    userEvent.click(screen.getByTestId('Editor_onPlainTextChange_2000_words'));
    userEvent.click(screen.getByTestId('TextBox_onChange'));
    fireEvent.change(screen.getByTestId('TextBox_onChange'), {
      target: { value: '{}+}' }
    });
    fireEvent.change(screen.getByTestId('TextBox_onChange'), {
      target: { value: 'XCOLLS' }
    });
    userEvent.click(screen.getByText(I18N_CLIENT_CONFIG.YES));

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.SUBMIT));

    expect(mockAction).toBeCalled();
  });
});
