import React from 'react';
import { renderMockStore } from 'app/test-utils';

// component
import ActionCodeFormInner from './ActionCodeFormInner';

// redux

// types
import { fireEvent, screen } from '@testing-library/dom';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const initialState: Partial<RootState> = {
  clientConfigCallResult: {
    sortBy: { id: 'id' },
    update: {
      errorMessage: 'errorMessage',
      open: true,
      record: {
        id: 'id',
        value: 'value',
        description: 'description',
        actionTypes: [
          {
            actionTypeName: 'default',
            nextWorkDateVariable: '',
            letterId: '1',
            chronicleBehaviorIdentifier: undefined,
            memoText: undefined,
            externalStatusReason: 'A'
          },
          {
            actionTypeName: 'default',
            nextWorkDateVariable: '',
            letterId: '1',
            chronicleBehaviorIdentifier: undefined,
            memoText: undefined,
            externalStatusReason: 'B'
          }
        ],
        countContact: undefined
      }
    }
  },
  form: {
    FORM_NAME: {
      values: {
        actionTypeName: 'default',
        actionTypeDescription: 'description',
        nextWorkDateVariable: '',
        letterId: '1',
        chronicleBehaviorIdentifier: '',
        memoText: 'test',
        externalStatusReason: { value: 'B', description: 'test' }
      }
    } as any,
    FORM_NAME_1: {
      values: {
        actionTypeName: 'default',
        actionTypeDescription: 'description',
        nextWorkDateVariable: '',
        letterId: '1',
        chronicleBehaviorIdentifier: '',
        memoText: 'test',
        externalStatusCode: 'A',
        externalStatusReason: 'B'
      }
    } as any
  },
  clientConfiguration: { settings: { selectedConfig: { id: 'id' } } }
};

describe('Render', () => {
  const onDirtyCallback = jest.fn();

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('should render UI with callback dirty call true', () => {
    renderMockStore(
      <ActionCodeFormInner
        id="FORM_NAME"
        descriptor="FORM_NAME"
        formKey="FORM_NAME"
        actionValues={
          initialState.clientConfigCallResult?.update?.record?.actionTypes[0]
        }
        onDirtyCallback={onDirtyCallback}
      />,
      { initialState }
    );

    fireEvent.change(screen.getByTestId('FORM_NAME_onChange'), {
      target: {
        value: 'test field'
      }
    });
    fireEvent.blur(screen.getByTestId('FORM_NAME_onChange'));

    expect(onDirtyCallback).toBeCalledWith(true);
  });

  it('should render UI with callback dirty call false', () => {
    renderMockStore(
      <ActionCodeFormInner
        id="FORM_NAME"
        descriptor="FORM_NAME"
        formKey="FORM_NAME"
        actionValues={
          initialState.clientConfigCallResult?.update?.record?.actionTypes[1]
        }
        onDirtyCallback={onDirtyCallback}
      />,
      { initialState }
    );

    fireEvent.change(screen.getByTestId('FORM_NAME_onChange'), {
      target: {
        value: 'test field'
      }
    });
    fireEvent.blur(screen.getByTestId('FORM_NAME_onChange'));
    expect(onDirtyCallback).toBeCalledWith(false);
  });

  it('should render UI with callback dirty call true 2', () => {
    renderMockStore(
      <ActionCodeFormInner
        id="FORM_NAME_1"
        descriptor="FORM_NAME_1"
        formKey="FORM_NAME_1"
        actionValues={
          initialState.clientConfigCallResult?.update?.record?.actionTypes[1]
        }
        onDirtyCallback={onDirtyCallback}
      />,
      { initialState }
    );

    fireEvent.change(screen.getByTestId('FORM_NAME_1_onChange'), {
      target: {
        value: 'test field'
      }
    });
    fireEvent.blur(screen.getByTestId('FORM_NAME_1_onChange'));
    expect(onDirtyCallback).toBeCalledWith(true);
  });

  it('should render UI with not do anything when form change if not pass cb', () => {
    renderMockStore(
      <ActionCodeFormInner
        id="FORM_NAME_1"
        descriptor="FORM_NAME_1"
        formKey="FORM_NAME_1"
        actionValues={
          initialState.clientConfigCallResult?.update?.record?.actionTypes[0]
        }
      />,
      { initialState }
    );

    fireEvent.change(screen.getByTestId('FORM_NAME_1_onChange'), {
      target: {
        value: 'test field'
      }
    });
    fireEvent.blur(screen.getByTestId('FORM_NAME_1_onChange'));
    expect(onDirtyCallback).not.toBeCalled();
  });
});
