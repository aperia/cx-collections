import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import userEvent from '@testing-library/user-event';

// component
import DeactivateModal from './DeactivateModal';

// redux
import { clientConfigCallResultActions } from './_redux/reducers';

// types
import { screen } from '@testing-library/dom';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';

const spyClientConfigCallResult = mockActionCreator(
  clientConfigCallResultActions
);

const initialState: Partial<RootState> = {
  clientConfigCallResult: {
    deactivate: {
      open: true,
      record: {
        id: 'id',
        value: 'value',
        description: 'description',
        actionTypes: [
          {
            actionTypeName: 'default',
            actionTypeDescription: 'default',
            chronicleBehaviorIdentifier: 'COLX',
            memoText: 'test'
          }
        ]
      }
    },
    sortBy: { id: 'id' }
  },
  clientConfiguration: { settings: { selectedConfig: { id: 'id' } } }
};

describe('Render', () => {
  it('should render UI', () => {
    renderMockStore(<DeactivateModal />, { initialState });

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.DEACTIVATE_CALL_RESULT)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleCloseModal', () => {
    const mockAction = spyClientConfigCallResult('toggleDeactivateModal');
    renderMockStore(<DeactivateModal />, { initialState });

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));

    expect(mockAction).toBeCalled();
  });

  describe('onSubmit', () => {
    it('when has selected record', () => {
      const mockAction = spyClientConfigCallResult(
        'triggerDeactivateCallResult'
      );
      renderMockStore(<DeactivateModal />, { initialState });

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.DEACTIVATE));

      expect(mockAction).toBeCalledWith({
        data: {
          ...initialState.clientConfigCallResult?.deactivate?.record,
          activated: false
        },
        updateAction: 'deactivate'
      });
    });

    it('when has no selected record', () => {
      const mockAction = spyClientConfigCallResult(
        'triggerDeactivateCallResult'
      );
      renderMockStore(<DeactivateModal />, {
        initialState: {
          clientConfigCallResult: {
            deactivate: { open: true },
            sortBy: { id: 'id' }
          }
        }
      });

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.DEACTIVATE));

      expect(mockAction).not.toBeCalled();
    });
  });
});
