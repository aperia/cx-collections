import React from 'react';
import { Button } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { I18N_CLIENT_CONFIG } from 'app/constants/i18n';
import AddModal from './AddModal';
import { useDispatch } from 'react-redux';
import { clientConfigCallResultActions } from './_redux/reducers';
import { ClientConfigStatusProps } from '../types';

const AddActionCode: React.FC<ClientConfigStatusProps> = ({ status }) => {
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const handleAdd = () => {
    dispatch(clientConfigCallResultActions.toggleAddModal({ open: true }));
  };

  return (
    <>
      <Button
        disabled={status}
        className="ml-n8"
        variant="outline-primary"
        size="sm"
        onClick={handleAdd}
        dataTestId="callResult_addActionCode-btn"
      >
        {t(I18N_CLIENT_CONFIG.ADD_ACTION_CODE)}
      </Button>
      <AddModal />
    </>
  );
};

export default AddActionCode;
