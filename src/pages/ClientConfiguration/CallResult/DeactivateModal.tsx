// app utils
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { useStoreIdSelector } from 'app/hooks';
// components
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
// hooks
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { genAmtId } from 'app/_libraries/_dls/utils';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CallResult } from './types';
// redux
import { clientConfigCallResultActions } from './_redux/reducers';
import {
  selectLoadingUpdate,
  selectOpenDeactivateModal,
  selectRecordDeactivateModal
} from './_redux/selector';

const DeactivateModal = () => {
  const { t } = useTranslation();
  const testId = 'callResult_deactivateModal';

  const dispatch = useDispatch();

  const isOpenModal = useStoreIdSelector<boolean>(selectOpenDeactivateModal);

  const isLoading = useStoreIdSelector<boolean>(selectLoadingUpdate);

  const selectedRecord: CallResult | undefined = useSelector(
    selectRecordDeactivateModal
  );

  const onSubmit = () => {
    if (!selectedRecord) return;

    dispatch(
      clientConfigCallResultActions.triggerDeactivateCallResult({
        data: {
          ...selectedRecord,
          activated: false
        },
        updateAction: 'deactivate'
      })
    );
  };

  const handleCloseModal = () => {
    dispatch(clientConfigCallResultActions.toggleDeactivateModal());
  };

  return (
    <Modal show={isOpenModal} loading={isLoading} xs dataTestId={testId}>
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId={`${testId}_header`}
      >
        <ModalTitle dataTestId={`${testId}_title`}>
          {t(I18N_CLIENT_CONFIG.DEACTIVATE_CALL_RESULT)}
        </ModalTitle>
      </ModalHeader>
      <ModalBody dataTestId={`${testId}_body`}>
        <p data-testid={genAmtId(testId, 'deactivateConfirmation', '')}>
          {t(I18N_CLIENT_CONFIG.DEACTIVATE_CALL_RESULT_CONFIRM)}
        </p>
        <p className="mt-16">
          {`${t(I18N_CLIENT_CONFIG.ACTION_CODE)}: `}
          <b
            className="text-break"
            data-testid={genAmtId(testId, 'actionCode', '')}
          >
            {`${selectedRecord?.value} • ${selectedRecord?.description}`}
          </b>
        </p>
      </ModalBody>
      <ModalFooter dataTestId={`${testId}_footer`}>
        <Button
          variant="secondary"
          onClick={handleCloseModal}
          dataTestId={`${testId}_cancelBtn`}
        >
          {t(I18N_COMMON_TEXT.CANCEL)}
        </Button>
        <Button
          variant="danger"
          onClick={onSubmit}
          dataTestId={`${testId}_deactivateBtn`}
        >
          {t(I18N_COMMON_TEXT.DEACTIVATE)}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default DeactivateModal;
