import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';

// component
import { Actions } from './Actions';

// redux
import { clientConfigCallResultActions } from './_redux/reducers';

// types
import { CallResult } from './types';
import { screen } from '@testing-library/dom';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import userEvent from '@testing-library/user-event';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const record: CallResult = {
  id: 'id',
  value: 'value',
  description: 'description',
  actionTypes: [
    {
      actionTypeName: 'default',
      actionTypeDescription: 'default',
      externalStatusCode: 'value',
      externalStatusReason: 'value'
    },
    {
      actionTypeName: 'custom'
    }
  ]
};

const spyClientConfigCallResult = mockActionCreator(
  clientConfigCallResultActions
);

const initialState: Partial<RootState> = {
  clientConfigCallResult: {
    sortBy: { id: 'id' },
    externalStatusCodeRefData: [{ value: 'value', description: 'desc' }],
    externalStatusReasonRefData: [{ value: 'value', description: 'desc' }]
  }
};

describe('Render', () => {
  it('should render UI with empty state', () => {
    renderMockStore(
      <Actions
        record={{ id: 'id', value: 'value', description: 'desc' } as any}
      />,
      { initialState: {} }
    );

    expect(screen.getByText(I18N_COMMON_TEXT.ACTIVATE)).toBeInTheDocument();
  });

  it('should render UI when actived', () => {
    renderMockStore(<Actions record={{ ...record, activated: true }} />, {
      initialState
    });

    expect(screen.getByText(I18N_COMMON_TEXT.DEACTIVATE)).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('show popover', () => {
    renderMockStore(<Actions record={record} />, { initialState });

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.VIEW));

    expect(
      screen.getByText(I18N_COMMON_TEXT.VIEW).classList.contains('active')
    ).toBeTruthy();
  });

  it('handleActivateRequest', () => {
    const mockAction = spyClientConfigCallResult('toggleActivateModal');

    renderMockStore(<Actions record={record} />, { initialState });

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.ACTIVATE));

    expect(mockAction).toBeCalledWith({ open: true, record });
  });

  it('handleDeactivateRequest', () => {
    const mockAction = spyClientConfigCallResult('toggleDeactivateModal');

    renderMockStore(<Actions record={{ ...record, activated: true }} />, {
      initialState
    });

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.DEACTIVATE));

    expect(mockAction).toBeCalledWith({
      open: true,
      record: { ...record, activated: true }
    });
  });

  it('handleEditRequest', () => {
    const mockAction = spyClientConfigCallResult('toggleUpdateModal');

    renderMockStore(<Actions record={record} />, { initialState });

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.EDIT));

    expect(mockAction).toBeCalledWith({ open: true, record });
  });
});
