import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import userEvent from '@testing-library/user-event';

// component
import Grid from './Grid';

// redux
import { clientConfigCallResultActions } from './_redux/reducers';

// hooks
import * as useScreenType from 'app/_libraries/_dls/hooks/useScreenType';

// types
import { screen } from '@testing-library/dom';
import { I18N_CLIENT_CONFIG } from 'app/constants/i18n';
import { ScreenType } from 'app/_libraries/_dls/hooks/useScreenType/types';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

jest.mock('./useDisabledButtonState', () => {
  return {
    useDisabledButtonState: () => ({
      isDisabledOk: false,
      errorEditor: {
        0: false
      },
      errorBehaviours: {
        0: false
      },
      handleBlurEditor: jest.fn(),
      handleBlurBehaviourType: jest.fn(),
      isSpecialCharacter: {}
    })
  };
});

jest.mock('app/_libraries/_dls/utils/genAmtId', () => () => { });

HTMLCanvasElement.prototype.getContext = jest.fn();

const spyClientConfigCallResult = mockActionCreator(
  clientConfigCallResultActions
);

const initialState: Partial<RootState> = {
  clientConfigCallResult: {
    add: {
      open: true,
      errorMessage: 'errorMessage'
    },
    list: [
      { id: 'id_0', value: 'value_0', description: 'description_0' },
      { id: 'id_1', value: 'value_1', description: 'description_1' },
      { id: 'id_2', value: 'value_2', description: 'description_2' },
      { id: 'id_3', value: 'value_3', description: 'description_3' },
      { id: 'id_4', value: 'value_4', description: 'description_4' },
      { id: 'id_5', value: 'value_5', description: 'description_5' },
      { id: 'id_6', value: 'value_6', description: 'description_6' },
      { id: 'id_7', value: 'value_7', description: 'description_7' },
      { id: 'id_8', value: 'value_8', description: 'description_8' },
      { id: 'id_9', value: 'value_9', description: 'description_9' },
      { id: 'id_10', value: 'value_10', description: 'description_10' }
    ],
    sortBy: { id: I18N_CLIENT_CONFIG.ACTION_CODE }
  },
  clientConfiguration: { settings: { selectedConfig: { id: '' } } }
};

describe('Render', () => {
  it('should render UI', () => {
    const spy = jest
      .spyOn(useScreenType, 'default')
      .mockImplementation(() => ScreenType.TABLET_PORTRAIT);

    const mockAction = spyClientConfigCallResult('getCallResultList');

    renderMockStore(<Grid />, { initialState });

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.CLIENT_ACTION_CODE_CONFIGURATION)
    ).toBeInTheDocument();
    expect(mockAction).toBeCalled();

    spy.mockReset();
    spy.mockRestore();
  });

  it('should render UI with empty state', () => {
    renderMockStore(<Grid />, { initialState: {} });

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.CLIENT_ACTION_CODE_CONFIGURATION)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleSort', () => {
    const mockAction = spyClientConfigCallResult('onChangeSort');
    renderMockStore(<Grid />, { initialState });

    userEvent.click(screen.getByText(I18N_CLIENT_CONFIG.ACTION_CODE));

    expect(mockAction).toBeCalledWith({ id: 'value', order: 'asc' });
  });
});
