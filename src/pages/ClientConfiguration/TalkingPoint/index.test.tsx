import React from 'react';
import { screen } from '@testing-library/dom';

import { mockActionCreator, renderMockStore } from 'app/test-utils';

import TalkingPoint from '.';

import { configurationTalkingPointActions } from './_redux/reducers';
import { SECTION_TAB_EVENT } from '../constants';

jest.mock('app/_libraries/_dls/utils/genAmtId', () => () => {});

HTMLCanvasElement.prototype.getContext = jest.fn();

const initialState: Partial<RootState> = {};

const spyConfigurationTalkingPoint = mockActionCreator(
  configurationTalkingPointActions
);

describe('Render', () => {
  it('Should render', () => {
    renderMockStore(<TalkingPoint />, { initialState });

    expect(screen.getByText('txt_talking_points')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('close modal', () => {
    const mockAction = spyConfigurationTalkingPoint(
      'unsavedChangeConfigurationTalkingPoint'
    );

    renderMockStore(<TalkingPoint />, { initialState });

    window.dispatchEvent(new Event(SECTION_TAB_EVENT.CLOSING_MODAL));

    expect(mockAction).toBeCalled();
  });

  it('change tab', () => {
    const mockAction = spyConfigurationTalkingPoint(
      'unsavedChangeConfigurationTalkingPoint'
    );

    renderMockStore(<TalkingPoint />, { initialState });

    window.dispatchEvent(new Event(SECTION_TAB_EVENT.CHANGING_TAB));

    expect(mockAction).toBeCalled();
  });
});
