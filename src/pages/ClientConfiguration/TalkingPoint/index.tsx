import React, { useEffect } from 'react';
// components
import HeaderConfigTalkingPoint from './Header';
import BodyConfigTalkingPoint from './Body';
import FooterConfigTalkingPoint from './Footer';
import { SimpleBar } from 'app/_libraries/_dls/components';

// redux
import { useDispatch } from 'react-redux';
import { configurationTalkingPointActions as actions } from './_redux/reducers';

// hooks
import { useListenClosingModal } from '../hooks/useListenClosingModal';
import { useListenChangingTabEvent } from '../hooks/useListenChangingTabEvent';

import {
  dispatchCloseModalEvent,
  dispatchControlTabChangeEvent
} from 'pages/ClientConfiguration/helpers';
import { useCheckClientConfigStatus } from 'pages/ClientConfiguration/hooks/useCheckClientConfigStatus';
import ApiErrorForHeaderSection from '../ApiErrorSection/ApiErrorForHeaderSection';

const ConfigTalkingPoint: React.FC = () => {
  const dispatch = useDispatch();

  const testId = 'clientConfig_talkingPoint';

  const status = useCheckClientConfigStatus();

  useListenClosingModal(() => {
    dispatch(
      actions.unsavedChangeConfigurationTalkingPoint(dispatchCloseModalEvent)
    );
  });

  useListenChangingTabEvent(() => {
    dispatch(
      actions.unsavedChangeConfigurationTalkingPoint(
        dispatchControlTabChangeEvent
      )
    );
  });

  useEffect(() => {
    return () => {
      dispatch(actions.resetState());
    };
  }, [dispatch]);

  return (
    <div className={'position-relative has-footer-button'}>
      <SimpleBar>
        <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
          <ApiErrorForHeaderSection
            className="mb-24"
            dataTestId={`${testId}_apiError`}
          />
          <HeaderConfigTalkingPoint />
          <BodyConfigTalkingPoint status={status} />
        </div>
      </SimpleBar>

      <FooterConfigTalkingPoint status={status} />
    </div>
  );
};
export default ConfigTalkingPoint;
