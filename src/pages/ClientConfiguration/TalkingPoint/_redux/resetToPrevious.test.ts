import './reducers';

import { resetToPrevious } from './resetToPrevious';

import { createStore, Store } from '@reduxjs/toolkit';

//redux
import { rootReducer } from 'storeConfig';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { configurationTalkingPointActions } from './reducers';

import { mockActionCreator } from 'app/test-utils';
import { IConfigurationTalkingPoint } from '../types';

let store: Store<RootState>;

const clientConfigurationTalkingPoint: IConfigurationTalkingPoint = {
  criterion: {
    id: 'criterion_id',
    text: 'criterion_text',
    value: 'criterion_value'
  },
  isPrevious: true,
  isLoading: false,
  originalTalkingPoint: '',
  talkingPoint: ''
};

const spyConfigurationTalkingPointActions = mockActionCreator(
  configurationTalkingPointActions
);
const spyToast = mockActionCreator(actionsToast);

describe('Test resetToPrevious', () => {
  it('talkingPint is originalTalkingPoint', async () => {
    const mockAddToast = spyToast('addToast');
    const mockOnChangeTalkingPoint = spyConfigurationTalkingPointActions(
      'onChangeTalkingPoint'
    );
    const mockOnChangePrevious =
      spyConfigurationTalkingPointActions('onChangePrevious');

    store = createStore(rootReducer, { clientConfigurationTalkingPoint });

    await resetToPrevious({})(store.dispatch, store.getState, {});

    expect(mockAddToast).not.toBeCalled();
    expect(mockOnChangeTalkingPoint).not.toBeCalled();
    expect(mockOnChangePrevious).not.toBeCalled();
  });

  it('talkingPint is not originalTalkingPoint', async () => {
    const mockAddToast = spyToast('addToast');
    const mockOnChangeTalkingPoint = spyConfigurationTalkingPointActions(
      'onChangeTalkingPoint'
    );
    const mockOnChangePrevious =
      spyConfigurationTalkingPointActions('onChangePrevious');

    store = createStore(rootReducer, {
      clientConfigurationTalkingPoint: {
        ...clientConfigurationTalkingPoint,
        originalTalkingPoint: `<div />`
      }
    });
    await resetToPrevious({})(store.dispatch, store.getState, {});

    expect(mockAddToast).toBeCalledWith({
      type: 'success',
      show: true,
      message: 'txt_talking_point_reset_to_previous'
    });
    expect(mockOnChangeTalkingPoint).toBeCalled();
    expect(mockOnChangePrevious).toBeCalled();
  });
});
