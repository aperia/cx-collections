import {
  configurationTalkingPointActions as actions,
  reducer
} from './reducers';
import { IConfigurationTalkingPoint } from '../types';

const initialState: IConfigurationTalkingPoint = {
  criterion: '',
  isPrevious: false,
  isLoading: false,
  originalTalkingPoint: {},
  talkingPoint: {}
};

describe('TalkingPoint reducer', () => {
  it('should run onChangeCriteria', () => {
    const criterion = {
      id: 'id',
      text: 'text',
      value: 'value'
    };

    const state = reducer(initialState, actions.onChangeCriteria(criterion));

    expect(state.criterion).toEqual(criterion);
  });

  it('should run onChangeTalkingPoint', () => {
    const talkingPoint = '<div></div>';

    const state = reducer(
      initialState,
      actions.onChangeTalkingPoint(talkingPoint)
    );

    expect(state.talkingPoint.panelContentText).toEqual(talkingPoint);
  });

  it('should run resetState', () => {
    const state = reducer(initialState, actions.resetState());

    expect(state).toEqual({
      ...initialState
    });
  });

  it('should run onChangePrevious', () => {
    const state = reducer(initialState, actions.onChangePrevious());

    expect(state.isPrevious).toEqual(!initialState.isPrevious);
  });
});
