import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';

export const unsavedChangeConfigurationTalkingPoint: AppThunk =
  (confirmCallback: Function) => async (dispatch, getState) => {
    const state = getState().clientConfigurationTalkingPoint;
    const talkingPoint = state.talkingPoint?.panelContentText?.replace(/class=\"\"/g, '')
      .replace(/rel=\"noreferrer\"/g, '')
      .replace(/\"/g, "'");
    const originalTalkingPoint = state.originalTalkingPoint?.panelContentText?.replace(/class=\"\"/g, '')
      .replace(/rel=\"noreferrer\"/g, '')
      .replace(/\"/g, "'");
    if (talkingPoint !== originalTalkingPoint) {
      dispatch(
        confirmModalActions.open({
          title: 'txt_unsaved_changes',
          body: 'txt_discard_change_body',
          cancelText: 'txt_continue_editing',
          confirmText: 'txt_discard_change',
          confirmCallback: confirmCallback,
          variant: 'danger'
        })
      );
    } else {
      confirmCallback();
    }
  };
