import './reducers';

import { getPanelContent } from './getPanelContent';

import { createStore, Store } from '@reduxjs/toolkit';

import { clientTalkingPointServices } from '../talkingPointServices';

//redux
import { rootReducer } from 'storeConfig';

import { responseDefault } from 'app/test-utils';

let store: Store<RootState>;

const generateState = (hasSelectedConfig = true, hasPanelContent = true) => {
  const initialState: Partial<RootState> = {
    clientConfiguration: {
      settings: {
        selectedConfig: hasSelectedConfig
          ? {
              agentId: '000',
              clientId: '111',
              systemId: '222',
              principleId: '333'
            }
          : undefined
      }
    },
    mapping: {
      data: {
        panelContent: hasPanelContent
          ? {
              id: 'panelContentId',
              panelContentText: 'panelContentText',
              status: 'status'
            }
          : undefined
      }
    } as any
  };

  return initialState;
};

beforeEach(() => {
  store = createStore(rootReducer, generateState());
});

describe('Test getTalkingPoint', () => {
  window.appConfig = {
    commonConfig: {
      app: 'app',
      user: 'user',
      org: 'org'
    }
  } as AppConfiguration;

  it('Case fulfilled', async () => {
    jest
      .spyOn(clientTalkingPointServices, 'getPanelContent')
      .mockResolvedValue({
        data: {
          ...responseDefault,
          panelContentModelList: [
            {
              panelContentId: '1_TalkingPoint',
              status: 'APPROVED',
              panelContentText: 'AAA'
            },
            {
              panelContentId: '2_TalkingPoint',
              status: 'APPROVED',
              panelContentText: 'BBB'
            }
          ]
        }
      } as any);

    const response = await getPanelContent()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'clientConfiguration/talkingpoint/getPanelContent/fulfilled'
    );
    expect(response.payload).toEqual({
      panelContent: [
        {
          id: '1_TalkingPoint',
          panelContentText: 'AAA',
          status: 'APPROVED'
        },
        {
          id: '2_TalkingPoint',
          panelContentText: 'BBB',
          status: 'APPROVED'
        }
      ]
    });
  });

  it('Case rejected', async () => {
    store = createStore(rootReducer, generateState(false, false));
    jest
      .spyOn(clientTalkingPointServices, 'getPanelContent')
      .mockRejectedValue({
        ...responseDefault
      } as any);

    const response = await getPanelContent()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'clientConfiguration/talkingpoint/getPanelContent/rejected'
    );
  });
});
