import './reducers';

import { getClientConfigurationTalkingPoint } from './getTalkingPoint';

import { createStore, Store } from '@reduxjs/toolkit';

import { clientTalkingPointServices } from '../talkingPointServices';

//redux
import { rootReducer } from 'storeConfig';

import { responseDefault } from 'app/test-utils';
import { IConfigurationTalkingPoint } from '../types';

let store: Store<RootState>;

beforeEach(() => {
  store = createStore(rootReducer, {
    clientConfigurationTalkingPoint: {
      isLoading: false,
      criterion: 'criterion_text',
      isPrevious: true,
      originalTalkingPoint: '',
      talkingPoint: ''
    } as IConfigurationTalkingPoint
  });
});

describe('Test getTalkingPoint', () => {
  window.appConfig = {
    commonConfig: {}
  } as AppConfiguration;

  it('Should return data', async () => {
    jest
      .spyOn(clientTalkingPointServices, 'getTalkingPoint')
      .mockResolvedValue({ ...responseDefault });

    const response = await getClientConfigurationTalkingPoint()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ talkingPoint: {} });
  });

  it('should return error', async () => {
    jest
      .spyOn(clientTalkingPointServices, 'getTalkingPoint')
      .mockRejectedValue({ ...responseDefault });

    const response = await getClientConfigurationTalkingPoint()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual(responseDefault);
  });
});
