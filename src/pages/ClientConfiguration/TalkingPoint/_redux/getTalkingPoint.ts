import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { mappingDataFromObj } from 'app/helpers';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { clientTalkingPointServices } from '../talkingPointServices';
import {
  IConfigurationPayloadTalkingPoint,
  IConfigurationTalkingPoint
} from '../types';

export const getClientConfigurationTalkingPoint = createAsyncThunk<
  IConfigurationPayloadTalkingPoint,
  undefined,
  ThunkAPIConfig
>(
  'clientConfiguration/getClientConfigurationTalkingPoint',
  async (_, thunkAPI) => {
    const { rejectWithValue, getState, dispatch } = thunkAPI;
    const {
      mapping,
      clientConfiguration,
      clientConfigurationTalkingPoint: { criterion }
    } = getState();
    const panelContent = mapping?.data?.panelContent || {};

    try {
      const { commonConfig } = window.appConfig;
      const { agentId, clientId, systemId, principleId } =
        clientConfiguration.settings?.selectedConfig || {};
      const { data } = await clientTalkingPointServices.getTalkingPoint({
        common: {
          clientNumber: clientId,
          system: systemId,
          prin: principleId,
          agent: agentId,
          app: commonConfig.app,
          user: commonConfig.user,
          org: commonConfig.org,
          userPrivileges: {
            cspa: [`${clientId}-${systemId}-${principleId}-${agentId}`],
            groups: ['admin']
          }
        },
        panelContentId: criterion
      });
      const talkingPoint = mappingDataFromObj(
        data?.panelContentModel,
        panelContent
      );
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          forSection: 'inClientConfigurationFeatureHeader',
          storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION
        })
      );
      return {
        talkingPoint
      };
    } catch (error) {
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION,
          forSection: 'inClientConfigurationFeatureHeader',
          apiResponse: error
        })
      );
      return rejectWithValue(error);
    }
  }
);

export const getClientConfigurationTalkingPointBuilder = (
  builder: ActionReducerMapBuilder<IConfigurationTalkingPoint>
) => {
  builder
    .addCase(
      getClientConfigurationTalkingPoint.pending,
      (draftState, action) => {
        draftState.isLoading = true;
      }
    )
    .addCase(
      getClientConfigurationTalkingPoint.fulfilled,
      (draftState, action) => {
        const { talkingPoint } = action.payload;
        draftState.isLoading = false;
        draftState.originalTalkingPoint = talkingPoint;
        draftState.talkingPoint = talkingPoint;
      }
    )
    .addCase(
      getClientConfigurationTalkingPoint.rejected,
      (draftState, action) => {
        draftState.isLoading = false;
      }
    );
};
