import { PanelContent } from './../types';
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { clientTalkingPointServices } from '../talkingPointServices';
import {
  IConfigurationPayloadTalkingPointPanelContent,
  IConfigurationTalkingPoint
} from '../types';
import uniqBy from 'lodash.uniqby';
import { APPROVED_STATES } from '../constants';

export const getPanelContent = createAsyncThunk<
  IConfigurationPayloadTalkingPointPanelContent,
  undefined,
  ThunkAPIConfig
>('clientConfiguration/talkingpoint/getPanelContent', async (_, thunkAPI) => {
  const { rejectWithValue, getState } = thunkAPI;
  const { mapping, clientConfiguration } = getState();
  const panelContent = mapping?.data?.panelContent || {};

  try {
    const { commonConfig } = window.appConfig;
    const { agentId, clientId, systemId, principleId } =
      clientConfiguration.settings?.selectedConfig || {};
    const { data } = await clientTalkingPointServices.getPanelContent({
      common: {
        clientNumber: clientId,
        system: systemId,
        prin: principleId,
        agent: agentId,
        app: commonConfig.app,
        user: commonConfig.user,
        org: commonConfig.org,
        userPrivileges: {
          cspa: [`${clientId}-${systemId}-${principleId}-${agentId}`],
          groups: ['admin']
        }
      },
      fetchSize: '200'
    });

    const mappingData: PanelContent[] = mappingDataFromArray(
      data?.panelContentModelList,
      panelContent
    );

    // filter uniq, filter approved and published state, filter dropdown id contains pattern 'TalkingPoints'
    const filteredData = uniqBy(mappingData, 'id').filter(
      item =>
        item.status &&
        APPROVED_STATES.includes(item.status) &&
        item.id?.includes('TalkingPoint')
    );

    return {
      panelContent: filteredData
    };
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const getPanelContentBuilder = (
  builder: ActionReducerMapBuilder<IConfigurationTalkingPoint>
) => {
  builder
    .addCase(getPanelContent.pending, (draftState, action) => {
      draftState.isLoading = true;
    })
    .addCase(getPanelContent.fulfilled, (draftState, action) => {
      const { panelContent } = action.payload;
      draftState.isLoading = false;
      draftState.panelContent = panelContent;
    })
    .addCase(getPanelContent.rejected, (draftState, action) => {
      draftState.isLoading = false;
    });
};
