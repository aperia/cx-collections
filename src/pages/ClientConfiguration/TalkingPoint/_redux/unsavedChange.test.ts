import './reducers';

import { unsavedChangeConfigurationTalkingPoint } from './unsavedChange';

import { createStore, Store } from '@reduxjs/toolkit';

//redux
import { rootReducer } from 'storeConfig';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';

import { mockActionCreator } from 'app/test-utils';
import { IConfigurationTalkingPoint } from '../types';

let store: Store<RootState>;

const clientConfigurationTalkingPoint: IConfigurationTalkingPoint = {
  criterion: '',
  isPrevious: true,
  isLoading: false,
  originalTalkingPoint: {},
  talkingPoint: {}
};

const spyConfirmModal = mockActionCreator(confirmModalActions);

describe('Test unsavedChange', () => {
  it('talkingPint is originalTalkingPoint', async () => {
    const mockAction = spyConfirmModal('open');
    const confirmCallback = jest.fn();

    store = createStore(rootReducer, { clientConfigurationTalkingPoint });

    await unsavedChangeConfigurationTalkingPoint(confirmCallback)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(mockAction).not.toBeCalled();
    expect(confirmCallback).toBeCalled();
  });

  it('talkingPint is not originalTalkingPoint', async () => {
    const mockAction = spyConfirmModal('open');
    const confirmCallback = jest.fn();

    store = createStore(rootReducer, {
      clientConfigurationTalkingPoint: {
        ...clientConfigurationTalkingPoint,
        originalTalkingPoint: {
          id: 'originalTalkingPoint',
          panelContentText: ' panelContentText'
        }
      }
    });

    await unsavedChangeConfigurationTalkingPoint(confirmCallback)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(confirmCallback).not.toBeCalled();
    expect(mockAction).toBeCalledWith({
      title: 'txt_unsaved_changes',
      body: 'txt_discard_change_body',
      cancelText: 'txt_continue_editing',
      confirmText: 'txt_discard_change',
      confirmCallback: expect.any(Function),
      variant: 'danger'
    });
  });
});
