import { getPanelContentBuilder, getPanelContent } from './getPanelContent';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IConfigurationTalkingPoint } from '../types';

import {
  getClientConfigurationTalkingPoint,
  getClientConfigurationTalkingPointBuilder
} from './getTalkingPoint';

import {
  updateClientConfigurationTalkingPoint,
  updateClientConfigurationTalkingPointBuilder
} from './updateTalkingPoint';

import { resetToPrevious } from './resetToPrevious';

import { unsavedChangeConfigurationTalkingPoint } from './unsavedChange';

// default empty data of Component Editor
export const defaultTalkingPoint = `<div class=\"\"></div>`;

const initialState: IConfigurationTalkingPoint = {
  criterion: '',
  originalTalkingPoint: {},
  isPrevious: false,
  talkingPoint: {},
  isLoading: false
};

const { actions, reducer } = createSlice({
  name: 'clientConfigurationTalkingPoint',
  initialState,
  reducers: {
    onChangeCriteria: (draftState, action: PayloadAction<string>) => {
      draftState.criterion = action.payload;
    },
    onChangeTalkingPoint: (draftState, action: PayloadAction<string>) => {
      draftState.talkingPoint.panelContentText = action.payload;
    },
    resetState: () => {
      return { ...initialState };
    },
    onChangePrevious: draftState => {
      draftState.isPrevious = !draftState.isPrevious;
    }
  },
  extraReducers: builder => {
    getClientConfigurationTalkingPointBuilder(builder);
    updateClientConfigurationTalkingPointBuilder(builder);
    getPanelContentBuilder(builder);
  }
});

const allActions = {
  getClientConfigurationTalkingPoint,
  getPanelContent,
  updateClientConfigurationTalkingPoint,
  resetToPrevious,
  unsavedChangeConfigurationTalkingPoint,
  ...actions
};

export { allActions as configurationTalkingPointActions, reducer };
