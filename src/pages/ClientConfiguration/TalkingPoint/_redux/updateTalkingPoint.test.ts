import './reducers';

import { updateClientConfigurationTalkingPoint } from './updateTalkingPoint';

import { applyMiddleware, createStore, Store } from '@reduxjs/toolkit';

import { clientTalkingPointServices } from '../talkingPointServices';

//redux
import { rootReducer } from 'storeConfig';

import { mockActionCreator, responseDefault } from 'app/test-utils';
import thunk from 'redux-thunk';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { configurationTalkingPointActions } from './reducers';
let store: Store<RootState>;

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    org: 'org',
    app: 'app'
  }
};

const mockToastAction = mockActionCreator(actionsToast);
const mockApiErrorNotificationAction = mockActionCreator(
  apiErrorNotificationAction
);
const mockConfigurationTalkingPointActions = mockActionCreator(
  configurationTalkingPointActions
);

beforeEach(() => {
  store = createStore(
    rootReducer,
    {
      clientConfigurationTalkingPoint: {
        isLoading: false,
        criterion: 'criterion',
        isPrevious: false,
        originalTalkingPoint: {},
        talkingPoint: {
          panelContentText: ''
        },
        panelContent: []
      }
    },
    applyMiddleware(thunk)
  );
});

describe('Test updateClientConfigurationTalkingPoint', () => {
  it('Should show error when response status is not success', async () => {
    const addToast = mockToastAction('addToast');
    const updateApiError = mockApiErrorNotificationAction('updateApiError');
    jest
      .spyOn(clientTalkingPointServices, 'updateTalkingPoint')
      .mockResolvedValue({ ...responseDefault });

    await updateClientConfigurationTalkingPoint(undefined)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(addToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: 'txt_update_talking_point_failed'
    });
    expect(updateApiError).toHaveBeenCalled();
  });

  it('Should return data when response status is success', async () => {
    const addToast = mockToastAction('addToast');
    const getClientConfigurationTalkingPoint =
      mockConfigurationTalkingPointActions(
        'getClientConfigurationTalkingPoint'
      );
    jest
      .spyOn(clientTalkingPointServices, 'updateTalkingPoint')
      .mockResolvedValue({
        ...responseDefault,
        data: { responseStatus: 'success' }
      });

    await updateClientConfigurationTalkingPoint(undefined)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(addToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: 'txt_update_talking_point_success'
    });
    expect(getClientConfigurationTalkingPoint).toHaveBeenCalled();
  });

  it('Should return data when criterion is empty', async () => {
    jest
      .spyOn(clientTalkingPointServices, 'updateTalkingPoint')
      .mockResolvedValue({ ...responseDefault });

    store = createStore(rootReducer, {
      clientConfigurationTalkingPoint: {
        criterion: '',
        isPrevious: true,
        isLoading: false,
        originalTalkingPoint: {},
        talkingPoint: {}
      },
      clientConfiguration: {
        settings: {}
      }
    });

    const response = await updateClientConfigurationTalkingPoint({})(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });
});
