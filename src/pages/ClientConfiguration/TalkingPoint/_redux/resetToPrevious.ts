import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import {
  configurationTalkingPointActions as actions,
  defaultTalkingPoint
} from './reducers';
export const resetToPrevious: AppThunk = () => async (dispatch, getState) => {
  const state = getState().clientConfigurationTalkingPoint;
  const talkingPoint = state.talkingPoint;
  const originalTalkingPoint = state.originalTalkingPoint;
  if (talkingPoint !== originalTalkingPoint) {
    batch(() => {
      dispatch(
        actions.onChangeTalkingPoint(
          originalTalkingPoint?.panelContentText || defaultTalkingPoint
        )
      );
      dispatch(actions.onChangePrevious());
    });
    dispatch(
      actionsToast.addToast({
        type: 'success',
        show: true,
        message: 'txt_talking_point_reset_to_previous'
      })
    );
  }
};
