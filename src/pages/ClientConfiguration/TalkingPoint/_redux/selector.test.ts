import * as selectors from './selector';

import { selectorWrapper } from 'app/test-utils';
import { defaultTalkingPoint } from './reducers';

const initialState: Partial<RootState> = {
  clientConfigurationTalkingPoint: {
    criterion: '',
    isPrevious: true,
    isLoading: true,
    originalTalkingPoint: {},
    talkingPoint: {},
    panelContent: [{ id: '1', text: 'text' }]
  }
};

const selectorTrigger = selectorWrapper(initialState);

describe('TalkingPoint selectors', () => {
  it('selectCriterion', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectCriterion);

    expect(data).toEqual(
      initialState.clientConfigurationTalkingPoint?.criterion
    );
    expect(emptyData).toEqual('criterion');
  });

  it('selectOriginalTalkingPoint', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectOriginalTalkingPoint
    );

    expect(data).toEqual(
      initialState.clientConfigurationTalkingPoint?.originalTalkingPoint
    );
    expect(emptyData).toEqual({
      id: '1',
      panelContentText: defaultTalkingPoint
    });
  });

  it('selectLoading', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectLoading);

    expect(data).toEqual(
      initialState.clientConfigurationTalkingPoint?.isLoading
    );
    expect(emptyData).toEqual(false);
  });

  it('selectIsPrevious', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectIsPrevious);

    expect(data).toEqual(
      initialState.clientConfigurationTalkingPoint?.isPrevious
    );
    expect(emptyData).toEqual(false);
  });

  it('selectPanelContent', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectPanelContent);

    expect(data).toEqual(
      initialState.clientConfigurationTalkingPoint?.panelContent
    );
    expect(emptyData).toEqual([]);
  });

  it('selectCurrentPanelContent', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectCurrentPanelContent
    );

    expect(data).toEqual(
      initialState.clientConfigurationTalkingPoint?.talkingPoint
    );
    expect(emptyData).toEqual({
      id: '1',
      panelContentText: '<div class=""></div>'
    });
  });

  it('selectCurrentPanelContent > talkingPoint is undefined', () => {
    const initialStateCase2: Partial<RootState> = {
      clientConfigurationTalkingPoint: {
        talkingPoint: undefined as never
      } as never
    };

    const selectorTriggerCase2 = selectorWrapper(initialStateCase2);

    const { data, emptyData } = selectorTriggerCase2(
      selectors.selectCurrentPanelContent
    );

    expect(data).toEqual(
      initialState.clientConfigurationTalkingPoint?.talkingPoint
    );
    expect(emptyData).toEqual({
      id: '1',
      panelContentText: '<div class=""></div>'
    });
  });
});
