import { createSelector } from '@reduxjs/toolkit';
import { PanelContent } from '../types';

export const selectCriterion = createSelector(
  (state: RootState) => state.clientConfigurationTalkingPoint.criterion,
  (criterion: string) => criterion
);

export const selectOriginalTalkingPoint = createSelector(
  (state: RootState) =>
    state.clientConfigurationTalkingPoint.originalTalkingPoint,
  (originalTalkingPoint: PanelContent) => originalTalkingPoint
);

export const selectLoading = createSelector(
  (state: RootState) => state.clientConfigurationTalkingPoint.isLoading,
  (isLoading: boolean) => isLoading
);

export const selectIsPrevious = createSelector(
  (state: RootState) => state.clientConfigurationTalkingPoint.isPrevious,
  (isLoading: boolean) => isLoading
);

export const selectPanelContent = createSelector(
  (state: RootState) =>
    state.clientConfigurationTalkingPoint.panelContent || [],
  (data: PanelContent[]) => data
);

export const selectCurrentPanelContent = createSelector(
  (state: RootState) =>
    state.clientConfigurationTalkingPoint.talkingPoint || {},
  (data: PanelContent) => data
);

