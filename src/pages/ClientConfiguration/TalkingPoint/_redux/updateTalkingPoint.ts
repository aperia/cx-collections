import { mappingDataFromObj } from 'app/helpers/mappingData';
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import isEmpty from 'lodash.isempty';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { clientTalkingPointServices } from '../talkingPointServices';
import {
  IConfigurationPayloadTalkingPoint,
  IConfigurationTalkingPoint
} from '../types';
import { configurationTalkingPointActions } from './reducers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';
import { batch } from 'react-redux';
import {
  encodeHtmlEntities,
  removeAttribute
} from 'pages/ClientConfiguration/ClientContactInformation/helpers';

export const updateClientConfigurationTalkingPoint = createAsyncThunk<
  IConfigurationPayloadTalkingPoint,
  unknown,
  ThunkAPIConfig
>(
  'clientConfiguration/updateClientConfigurationTalkingPoint',
  async (args, thunkAPI) => {
    const { rejectWithValue, getState, dispatch } = thunkAPI;

    const {
      mapping,
      clientConfiguration,
      clientConfigurationTalkingPoint: {
        talkingPoint,
        criterion,
        originalTalkingPoint
      }
    } = getState();
    const panelContent = mapping?.data?.panelContent || {};

    const payload = {
      id: criterion,
      // API do not accept pattern class=""
      panelContentText: removeAttribute(talkingPoint?.panelContentText || ''),
      status: talkingPoint?.status
    };
    const mappingPayload = mappingDataFromObj(payload, panelContent, true);
    const { commonConfig } = window.appConfig;

    try {
      const { agentId, clientId, systemId, principleId } =
        clientConfiguration.settings?.selectedConfig || {};

      const response = await clientTalkingPointServices.updateTalkingPoint({
        common: {
          clientNumber: clientId,
          system: systemId,
          prin: principleId,
          agent: agentId,
          app: commonConfig.app,
          user: commonConfig.user,
          org: commonConfig.org,
          userPrivileges: {
            cspa: [`${clientId}-${systemId}-${principleId}-${agentId}`],
            groups: ['admin']
          }
        },
        ...mappingPayload,
        panelContentText: mappingPayload?.panelContentText?.replace(/"/g, '\\"')
      });

      if (response.data?.responseStatus?.toLowerCase() !== 'success')
        throw response;

      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: 'txt_update_talking_point_success'
          })
        );
        dispatch(
          configurationTalkingPointActions.getClientConfigurationTalkingPoint()
        );
        dispatch(
          changeHistoryActions.saveChangedClientConfig({
            action: 'UPDATE',
            changedCategory: 'talkingPoints',
            changedItem: {
              selectedOption: talkingPoint.id
            },
            oldValue: {
              panelContentText: encodeHtmlEntities(
                removeAttribute(originalTalkingPoint.panelContentText || '')
              )
            },
            newValue: {
              panelContentText: encodeHtmlEntities(
                removeAttribute(talkingPoint.panelContentText || '')
              )
            }
          })
        );
      });

      return { talkingPoint };
    } catch (error) {
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'error',
            message: 'txt_update_talking_point_failed'
          })
        );
        dispatch(
          apiErrorNotificationAction.updateApiError({
            storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION,
            forSection: 'inClientConfigurationFeatureHeader',
            apiResponse: error
          })
        );
      });
      return rejectWithValue(error);
    }
  },
  {
    // Condition of redux toolkit prevent call api if this condition return false
    condition: (args, thunkAPI) => {
      const { getState } = thunkAPI;
      const { criterion } = getState().clientConfigurationTalkingPoint;
      if (isEmpty(criterion)) return false;
    }
  }
);

export const updateClientConfigurationTalkingPointBuilder = (
  builder: ActionReducerMapBuilder<IConfigurationTalkingPoint>
) => {
  builder
    .addCase(
      updateClientConfigurationTalkingPoint.pending,
      (draftState, action) => {
        draftState.isLoading = true;
      }
    )
    .addCase(
      updateClientConfigurationTalkingPoint.fulfilled,
      (draftState, action) => {
        const { talkingPoint } = action.payload;
        draftState.originalTalkingPoint = talkingPoint;
      }
    )
    .addCase(
      updateClientConfigurationTalkingPoint.rejected,
      (draftState, action) => {
        draftState.isLoading = false;
      }
    );
};
