import { apiService } from 'app/utils/api.service';
import {
  PanelContentGetByIdRequest,
  PanelContentGetListRequest,
  UpdatePanelContentRequest
} from './types';

export const clientTalkingPointServices = {
  getPanelContent(data: PanelContentGetListRequest) {
    const url =
      window.appConfig?.api?.getClientConfigurationTalkingPoint.getPanelContent;
    return apiService.post(url!, data);
  },
  getTalkingPoint(data: PanelContentGetByIdRequest) {
    const url =
      window.appConfig?.api?.getClientConfigurationTalkingPoint
        .getClientConfigTalkingPoint;
    return apiService.post(url!, data);
  },
  updateTalkingPoint(data: UpdatePanelContentRequest) {
    const url =
      window.appConfig?.api?.getClientConfigurationTalkingPoint
        .updateClientConfigTalkingPoint;
    return apiService.post(url!, data);
  }
};
