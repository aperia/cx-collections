import React, { useEffect, useRef } from 'react';

// component
import TextEditor from 'app/_libraries/_dls/components/TextEditor';

// hooks
import { useStoreIdSelector } from 'app/hooks';

// redux
import {
  selectOriginalTalkingPoint,
  selectIsPrevious,
  selectCriterion
} from '../_redux/selector';
import { useDispatch } from 'react-redux';
import { configurationTalkingPointActions as actions } from '../_redux/reducers';

// lodash
import { PanelContent } from '../types';
import isEmpty from 'lodash.isempty';

import { ClientConfigStatusProps } from 'pages/ClientConfiguration/types';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { TextEditorRef } from 'app/_libraries/_dls/components/TextEditor/types';

const BodyConfigTalkingPoint: React.FC<ClientConfigStatusProps> = ({
  status
}) => {
  const originalTalkingPoint = useStoreIdSelector<PanelContent>(
    selectOriginalTalkingPoint
  );
  const ref = useRef<TextEditorRef | null>(null);
  const dispatch = useDispatch();
  const testId = 'clientConfig_talkingPoint_body';

  const isPrevious = useStoreIdSelector<string>(selectIsPrevious);

  const criterion = useStoreIdSelector<string>(selectCriterion);

  const onChange = () => {
    dispatch(actions.onChangeTalkingPoint(ref.current?.getMarkdown() || ''));
  };

  useEffect(() => {
    if (!isPrevious) return;
    ref.current &&
      ref.current.setMarkdown(originalTalkingPoint.panelContentText || '');
    ref.current && dispatch(actions.onChangePrevious());
  }, [dispatch, isPrevious, originalTalkingPoint.panelContentText]);

  useEffect(() => {
    ref.current &&
      ref.current.setMarkdown(originalTalkingPoint.panelContentText || '');
  }, [originalTalkingPoint.panelContentText]);

  if (isEmpty(criterion)) return null;

  return (
    <div className="mt-24">
      <p
        className="mb-16 fw-500"
        data-testid={genAmtId(testId, 'originalTalkingPointText', '')}
      >
        {originalTalkingPoint.text}
      </p>
      <div className="w-lg-50 custom-editor">
        <TextEditor
          readOnly={status}
          onBlur={onChange}
          ref={ref}
          dataTestId={`${testId}_editor`}
        />
      </div>
    </div>
  );
};
export default BodyConfigTalkingPoint;
