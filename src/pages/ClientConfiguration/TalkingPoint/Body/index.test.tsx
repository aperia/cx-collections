import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import Body from '.';
import { screen } from '@testing-library/dom';
import { configurationTalkingPointActions } from '../_redux/reducers';
import { IConfigurationTalkingPoint, PanelContent } from '../types';
import { fireEvent } from '@testing-library/react';
jest.mock('app/_libraries/_dls/utils/genAmtId', () => () => {});

jest.mock('app/_libraries/_dls/components/TextEditor', () => {
  return {
    __esModule: true,
    ...jest.requireActual('app/_libraries/_dls/components/TextEditor'),
    ...jest.requireActual('app/test-utils/mocks/MockPayByPhoneTextEditor')
  };
});

HTMLCanvasElement.prototype.getContext = jest.fn();
const initialState: Partial<RootState> = {
  clientConfigurationTalkingPoint: {
    criterion: 'criterion_text',
    isPrevious: true,
    originalTalkingPoint: {
      text: `originalTalkingPointText`,
      panelContentText: `originalTalkingPointPanelContentText`
    } as PanelContent
  } as IConfigurationTalkingPoint
};
const spyConfigurationTalkingPoint = mockActionCreator(
  configurationTalkingPointActions
);

describe('Render', () => {
  it('Should render', () => {
    renderMockStore(<Body status={true} />, { initialState });
    expect(screen.getByText('originalTalkingPointText')).toBeInTheDocument();
  });

  it('Should render without panelContentText', () => {
    const state: Partial<RootState> = {
      clientConfigurationTalkingPoint: {
        criterion: 'criterion_text',
        isPrevious: true,
        originalTalkingPoint: {
          text: `originalTalkingPointText`
        } as PanelContent
      } as IConfigurationTalkingPoint
    };
    renderMockStore(<Body status={true} />, { initialState: state });
    expect(screen.getByText('originalTalkingPointText')).toBeInTheDocument();
  });

  it('Should render with empty state', () => {
    const { container } = renderMockStore(<Body status={true} />, {
      initialState: {}
    });
    expect(container.querySelector('div')).toBeNull();
  });
});

describe('Actions', () => {
  it('handleChange', () => {
    const mockAction = spyConfigurationTalkingPoint('onChangeTalkingPoint');
    renderMockStore(<Body status={true} />, {
      initialState
    });

    const input = screen.getByTestId('clientConfig_talkingPoint_body_editor');

    fireEvent.blur(input);
    expect(mockAction).toBeCalled();
  });
});
