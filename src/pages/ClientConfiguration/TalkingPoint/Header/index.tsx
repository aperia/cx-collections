import React, { useEffect } from 'react';

// components
import {
  ComboBox,
  DropdownBaseChangeEvent
} from 'app/_libraries/_dls/components';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { configurationTalkingPointActions } from '../_redux/reducers';

// library
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';

import { PanelContent } from '../types';
import { unsavedChangeConfigurationTalkingPoint } from '../_redux/unsavedChange';
import { selectCriterion, selectPanelContent } from '../_redux/selector';
import isEmpty from 'lodash.isempty';
import { genAmtId } from 'app/_libraries/_dls/utils';

const HeaderConfigTalkingPoint: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const testId = 'clientConfig_talkingPoint_header';

  const contentPanel = useSelector(selectCriterion);
  const data: PanelContent[] = useSelector(selectPanelContent);

  const onChange = (event: DropdownBaseChangeEvent) => {
    const handleConfirmCallback = () => {
      dispatch(
        configurationTalkingPointActions.onChangeCriteria(event.target.value)
      );
      dispatch(
        configurationTalkingPointActions.getClientConfigurationTalkingPoint()
      );
    };

    dispatch(unsavedChangeConfigurationTalkingPoint(handleConfirmCallback));
  };

  useEffect(() => {
    dispatch(configurationTalkingPointActions.getPanelContent());
  }, [dispatch]);

  return (
    <div className="d-lg-flex justify-content-between align-items-center">
      <div>
        <h5 data-testid={genAmtId(testId, 'title', '')}>
          {t('txt_talking_points')}
        </h5>
        <p className="mt-16" data-testid={genAmtId(testId, 'description', '')}>
          {t('txt_select_content_panel_to_configure_talking_points')}
        </p>
      </div>
      <div className="w-320px mt-md-16 mt-lg-0">
        <ComboBox
          name="criteriaTalkingPoint"
          label={t('txt_content_panel')}
          onChange={onChange}
          textField="text"
          value={isEmpty(contentPanel) ? null : contentPanel}
          dataTestId={`${testId}_contentPanel`}
          noResult={t('txt_no_results_found')}
          checkAllLabel={t('txt_all')}
        >
          {data.map((item: PanelContent) => (
            <ComboBox.Item key={item.id} label={item.text} value={item.id} />
          ))}
        </ComboBox>
      </div>
    </div>
  );
};
export default HeaderConfigTalkingPoint;
