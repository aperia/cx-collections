import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';

import Header from '.';
import { screen } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';

import { configurationTalkingPointActions } from '../_redux/reducers';
import { ComboBoxProps } from 'app/_libraries/_dls/components';

jest.mock('app/_libraries/_dls/utils/genAmtId', () => () => {});

jest.mock('app/_libraries/_dls/components/ComboBox', () => {
  const comboboxPrototypes = jest.requireActual(
    'app/_libraries/_dls/components/ComboBox'
  );

  const Combobox = ({ onChange }: ComboBoxProps) => {
    return <div data-testid="ComboBox_onChange" onClick={e => onChange!(e)} />;
  };

  Combobox.Item = () => <div />;

  return {
    ...comboboxPrototypes,
    __esModule: true,
    default: Combobox
  };
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const initialState: Partial<RootState> = {
  clientConfigurationTalkingPoint: {
    criterion: {
      id: 'criterion_id',
      text: 'criterion_text',
      value: 'criterion_value'
    },
    isPrevious: true,
    isLoading: false,
    originalTalkingPoint: '',
    talkingPoint: ''
  }
};

const spyConfigurationTalkingPoint = mockActionCreator(
  configurationTalkingPointActions
);

describe('Render', () => {
  it('Should render', () => {
    renderMockStore(<Header />, { initialState });

    expect(screen.getByText('txt_talking_points')).toBeInTheDocument();
  });

  it('Should render with empty state', () => {
    renderMockStore(<Header />, { initialState: {} });

    expect(screen.getByText('txt_talking_points')).toBeInTheDocument();
  });

  it('Should render with data selectPanelContent', () => {
    renderMockStore(<Header />, {
      initialState: {
        clientConfigurationTalkingPoint: {
          panelContent: [
            { id: 'abc123', text: 'AAABBBCCC' },
            { id: 'abc124', text: 'AAABBBDDD' },
            { id: 'abc125', text: 'AAABBBEEE' }
          ]
        } as never
      }
    });
    expect(screen.getByText('txt_talking_points')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('onChange', () => {
    const mockOnChangeCriteria =
      spyConfigurationTalkingPoint('onChangeCriteria');
    const mockGetClientConfigurationTalkingPoint = spyConfigurationTalkingPoint(
      'getClientConfigurationTalkingPoint'
    );

    renderMockStore(<Header />, { initialState });

    userEvent.click(screen.getByTestId('ComboBox_onChange'));

    expect(mockOnChangeCriteria).toBeCalled();
    expect(mockGetClientConfigurationTalkingPoint).toBeCalled();
  });
});
