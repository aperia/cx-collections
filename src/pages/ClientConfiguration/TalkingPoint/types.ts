export interface IConfigurationTalkingPoint {
  isLoading: boolean;
  criterion: string;
  isPrevious: boolean;
  originalTalkingPoint: PanelContent;
  talkingPoint: PanelContent;
  panelContent?: PanelContent[];
}
export interface IConfigurationPayloadTalkingPoint {
  talkingPoint: PanelContent;
}

export interface PanelContent {
  id?: string;
  versionNumber?: string;
  text?: string;
  panelContentText?: string;
  activationDate?: string;
  status?: string;
}

export interface IConfigurationPayloadTalkingPointPanelContent {
  panelContent: PanelContent[];
}

export interface PanelContentGetByIdRequest
  extends CommonRequest,
    MagicKeyValue {}

export interface PanelContentGetListRequest
  extends CommonRequest,
    MagicKeyValue {}

export interface UpdatePanelContentRequest
  extends CommonRequest,
    MagicKeyValue {}
