// services
import { clientTalkingPointServices } from './talkingPointServices';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

describe('clientTalkingPointServices', () => {
  it('getTalkingPoint', () => {
    const params = {
      criterion: {
        id: 'id',
        text: 'text',
        value: 'value'
      }
    };

    mockAppConfigApi.setup();

    const mockService = mockApiServices.post();

    clientTalkingPointServices.getTalkingPoint(params);

    expect(mockService).toBeCalledWith(
      apiUrl.getClientConfigurationTalkingPoint.getClientConfigTalkingPoint,
      params
    );
  });

  it('getPanelContent', () => {
    const params = {
      criterion: {
        id: 'id',
        text: 'text',
        value: 'value'
      }
    };

    mockAppConfigApi.setup();

    const mockService = mockApiServices.post();

    clientTalkingPointServices.getPanelContent(params);

    expect(mockService).toBeCalledWith(
      apiUrl.getClientConfigurationTalkingPoint.getPanelContent,
      params
    );
  });

  it('updateTalkingPoint', () => {
    const params = {
      data: {
        id: 'id',
        text: 'text',
        value: 'value'
      }
    };

    mockAppConfigApi.setup();

    const mockService = mockApiServices.post();

    clientTalkingPointServices.updateTalkingPoint(params);

    expect(mockService).toBeCalledWith(
      apiUrl.getClientConfigurationTalkingPoint.updateClientConfigTalkingPoint,
      params
    );
  });
});
