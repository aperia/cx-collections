import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';

import Footer from '.';
import { screen } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';

import { configurationTalkingPointActions } from '../_redux/reducers';

jest.mock('app/_libraries/_dls/utils/genAmtId', () => () => {});

HTMLCanvasElement.prototype.getContext = jest.fn();

const initialState: Partial<RootState> = {
  clientConfigurationTalkingPoint: {
    criterion: 'not empty',
    isLoading: false,
    isPrevious: true,
    talkingPoint: {
      panelContentText: 'text content 1'
    },
    originalTalkingPoint: {
      panelContentText: 'text content'
    }
  }
};

const spyConfigurationTalkingPoint = mockActionCreator(
  configurationTalkingPointActions
);

describe('Actions', () => {
  it('onResetToPrevious', () => {
    const mockAction = spyConfigurationTalkingPoint('resetToPrevious');

    renderMockStore(<Footer status={false} />, { initialState });
    userEvent.click(screen.getByText('txt_reset_to_previous'));
    expect(mockAction).toBeCalled();
  });

  it('onSaveChange', () => {
    const mockAction = spyConfigurationTalkingPoint(
      'updateClientConfigurationTalkingPoint'
    );

    renderMockStore(<Footer status={false} />, {
      initialState
    });
    userEvent.click(screen.getByText('txt_save_changes'));
    expect(mockAction).toBeCalled();
  });
});
