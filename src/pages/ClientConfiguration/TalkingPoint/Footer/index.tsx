import React from 'react';

// components
import { Button } from 'app/_libraries/_dls/components';

// i18n
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';

// redux
import { useDispatch } from 'react-redux';
import {
  selectCriterion,
  selectOriginalTalkingPoint,
  selectCurrentPanelContent
} from '../_redux/selector';
import { useStoreIdSelector } from 'app/hooks';

// services
import { configurationTalkingPointActions as actions } from '../_redux/reducers';

// libaries
import { isEmpty, isEqual } from 'app/_libraries/_dls/lodash';
import { ClientConfigStatusProps } from 'pages/ClientConfiguration/types';
import { PanelContent } from '../types';
import { removeAttribute } from 'pages/ClientConfiguration/ClientContactInformation/helpers';

const FooterConfigTalkingPoint: React.FC<ClientConfigStatusProps> = ({
  status
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const testId = 'clientConfig_talkingPoint_footer';

  const originalTalkingPoint = useStoreIdSelector<PanelContent>(
    selectOriginalTalkingPoint
  );

  const currentPanelContent = useStoreIdSelector<PanelContent>(
    selectCurrentPanelContent
  );

  const isNotChangeData = isEqual(
    removeAttribute(originalTalkingPoint?.panelContentText || ''),
    removeAttribute(currentPanelContent?.panelContentText || '')
  );

  const criterion = useStoreIdSelector<string>(selectCriterion);

  const onSaveChange = () => {
    dispatch(actions.updateClientConfigurationTalkingPoint({}));
  };

  const onResetToPrevious = () => {
    dispatch(actions.resetToPrevious());
  };
  return (
    <div className="group-button-footer d-flex justify-content-end">
      <Button
        onClick={onResetToPrevious}
        variant="secondary"
        disabled={status ? status : isEmpty(criterion)}
        dataTestId={`${testId}_resetToPreviousBtn`}
      >
        {t('txt_reset_to_previous')}
      </Button>
      <Button
        onClick={onSaveChange}
        variant="primary"
        disabled={(status ? status : isEmpty(criterion)) || isNotChangeData}
        dataTestId={`${testId}_saveChangesBtn`}
      >
        {t('txt_save_changes')}
      </Button>
    </div>
  );
};

export default FooterConfigTalkingPoint;
