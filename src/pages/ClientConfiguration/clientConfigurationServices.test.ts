// mocks
import { mockAxiosResolve } from 'app/test-utils/mocks/mockAxiosResolve';

// services
import { clientConfigurationServices } from './clientConfigurationServices';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';
import {
  UpdateClientConfigurationRequest,
  CreateClientConfigurationRequest
} from './types';
import {
  BINSEQUENCES,
  DEFAULT_COMMON_REQUEST_CLIENT_CONFIG
} from './constants';

describe('clientConfigurationServices', () => {
  describe('getSectionList', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      clientConfigurationServices.getSectionList();

      expect(mockAxiosResolve).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      clientConfigurationServices.getSectionList();

      expect(mockAxiosResolve).toBeCalledWith(
        apiUrl.clientConfig.getSectionList
      );
    });
  });

  describe('add', () => {
    const requestBody: CreateClientConfigurationRequest = {
      ...DEFAULT_COMMON_REQUEST_CLIENT_CONFIG,
      agent: '1234',
      clientNumber: '1234',
      system: '1234',
      prin: '1234',
      clientInfoId: '1234123412341234',
      userId: 'FDESMAST',
      portfolioName: 'test',
      x500Id: 'AAAA1114',
      orgId: 'COLX',
      binSequences: BINSEQUENCES
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      clientConfigurationServices.add(requestBody);

      expect(mockService).toBeCalledWith('', requestBody);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      clientConfigurationServices.add(requestBody);

      expect(mockService).toBeCalledWith(apiUrl.clientConfig.add, requestBody);
    });
  });

  describe('update', () => {
    const requestBody: UpdateClientConfigurationRequest = {
      common: {
        clientNumber: '1111',
        system: '1111',
        prin: '1111',
        agent: '1111',
        app: 'cx'
      },
      clientInfoId: '1111111111111111',
      portfolioName: 'ahihi do ngoc',
      binSequences: BINSEQUENCES
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      clientConfigurationServices.update(requestBody);

      expect(mockService).toBeCalledWith('', requestBody);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      clientConfigurationServices.update(requestBody);

      expect(mockService).toBeCalledWith(
        apiUrl.clientConfig.update,
        requestBody
      );
    });
  });

  describe('clientInfoGetList', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();
      const requestBody: CreateClientConfigurationRequest = {
        ...DEFAULT_COMMON_REQUEST_CLIENT_CONFIG,
        agent: '1234',
        clientNumber: '1234',
        system: '1234',
        prin: '1234',
        clientInfoId: '1234123412341234',
        userId: 'FDESMAST',
        portfolioName: 'test',
        x500Id: 'AAAA1114',
        orgId: 'COLX',
        binSequences: BINSEQUENCES
      };
      const mockService = mockApiServices.post();

      clientConfigurationServices.clientInfoGetList(requestBody);

      expect(mockService).toBeCalledWith('', {
        ...DEFAULT_COMMON_REQUEST_CLIENT_CONFIG,
        agent: '1234',
        clientNumber: '1234',
        system: '1234',
        prin: '1234',
        clientInfoId: '1234123412341234',
        userId: 'FDESMAST',
        portfolioName: 'test',
        x500Id: 'AAAA1114',
        orgId: 'COLX',
        binSequences: BINSEQUENCES
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();
      const params = {
        common: {
          agent: 'agent',
          clientNumber: 'clientNumber',
          system: 'system',
          prin: 'prin',
          user: 'user',
          clientInfoId: '1234123412341234',
          app: 'app'
        }
      };
      clientConfigurationServices.clientInfoGetList(params);

      expect(mockService).toBeCalledWith(
        apiUrl.clientConfig.clientInfoGetList,
        params
      );
    });
  });

  describe('addUpdateClientInfo', () => {
    const params = { id: 'id' };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      clientConfigurationServices.addUpdateClientInfo(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      clientConfigurationServices.addUpdateClientInfo(params);

      expect(mockService).toBeCalledWith(
        apiUrl.clientConfig.addUpdateClientInfo,
        params
      );
    });
  });

  describe('getClientInfoById', () => {
    const params = { id: 'id' };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      clientConfigurationServices.getClientInfoById(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      clientConfigurationServices.getClientInfoById(params);

      expect(mockService).toBeCalledWith(
        apiUrl.clientConfig.getClientInfoById,
        params
      );
    });
  });

  describe('changeStatusClientConfig', () => {
    const params = {
      common: {
        agent: 'agent',
        clientNumber: 'clientNumber',
        system: 'system',
        prin: 'prin',
        app: 'app'
      },
      clientInfoId: 'clientInfoId',
      orgId: 'orgId',
      binSequences: {},
      additionalFields: []
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      clientConfigurationServices.changeStatusClientConfig(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      clientConfigurationServices.changeStatusClientConfig(params);

      expect(mockService).toBeCalledWith(
        apiUrl.clientConfig.changeStatusClientConfig,
        params
      );
    });
  });

  describe('getCollectionsClientConfig', () => {
    const params = { id: 'id' };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      clientConfigurationServices.getCollectionsClientConfig(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      clientConfigurationServices.getCollectionsClientConfig(params);

      expect(mockService).toBeCalledWith(
        apiUrl.clientConfig.getCollectionsClientConfig,
        params
      );
    });
  });

  describe('saveCollectionsClientConfig', () => {
    const params = { id: 'id' };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      clientConfigurationServices.saveCollectionsClientConfig(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      clientConfigurationServices.saveCollectionsClientConfig(params);

      expect(mockService).toBeCalledWith(
        apiUrl.clientConfig.saveCollectionsClientConfig,
        params
      );
    });
  });

  describe('getSecureInfoById', () => {
    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      clientConfigurationServices.getSecureInfoById({
        clientInfoId: '123-abc',
        enableFallback: false
      });

      expect(mockService).toBeCalledWith(
        apiUrl.clientConfig.getCollectionsClientConfig,
        {
          clientInfoId: '123-abc',
          enableFallback: false
        }
      );
    });
    it('when url not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      clientConfigurationServices.getSecureInfoById({
        clientInfoId: '123-abc',
        enableFallback: false
      });

      expect(mockService).toBeCalledWith('', {
        clientInfoId: '123-abc',
        enableFallback: false
      });
    });
  });
  describe('getAccessConfig', () => {
    it('when url not defined', () => {
      mockAppConfigApi.clear();
      const param = {
        common: {
          x500Id: 'string',
          privileges: ['ADMIN']
        }
      };
      const mockService = mockApiServices.post();

      clientConfigurationServices.getAccessConfig(param);

      expect(mockService).toBeCalledWith('', {
        common: { privileges: ['ADMIN'], x500Id: 'string' }
      });
    });

    it('getAccessConfig', () => {
      mockAppConfigApi.setup();
      const param = {
        common: {
          x500Id: 'string',
          privileges: ['ADMIN']
        }
      };
      const mockService = mockApiServices.post();

      clientConfigurationServices.getAccessConfig(param);

      expect(mockService).toBeCalledWith(apiUrl.clientConfig.getAccessConfig, {
        common: { privileges: ['ADMIN'], x500Id: 'string' }
      });
    });
  });
});
