import React from 'react';

//test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

//components
import DeleteModal from '.';

//actions
import { clientConfigurationActions } from '../_redux/reducers';

//constants
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { CLIENT_CONFIG_STATUS_KEY } from '../constants';

const mockClientConfiguration = mockActionCreator(clientConfigurationActions);

const initialState: Partial<RootState> = {
  clientConfiguration: {
    openStatusModal: true,
    settings: {
      selectedConfig: { id: 'id' }
    }
  }
};

describe('Actions', () => {
  it('render with deactive', () => {
    renderMockStore(<DeleteModal />, { initialState });
    expect(screen.getByText('txt_activate')).toBeInTheDocument();
  });

  it('handleCloseModal', () => {
    const mockAction = mockClientConfiguration('toggleStatusModal');

    renderMockStore(<DeleteModal />, {
      initialState: {
        ...initialState,
        clientConfiguration: {
          ...initialState.clientConfiguration,
          settings: {
            selectedConfig: {
              id: 'id',
              additionalFields: [
                { name: CLIENT_CONFIG_STATUS_KEY, active: true }
              ]
            }
          }
        }
      }
    });

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));

    expect(
      screen.getByText(
        I18N_CLIENT_CONFIG.CONFIRM_CHANGE_STATUS_CLIENT_CONFIGURATION
      )
    ).toBeInTheDocument();
    expect(mockAction).toBeCalled();
  });

  it('handleSubmit', () => {
    const mockAction = mockClientConfiguration(
      'triggerChangeStatusClientConfiguration'
    );

    renderMockStore(<DeleteModal />, { initialState });

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.ACTIVATE));

    expect(mockAction).toBeCalled();
  });
});
