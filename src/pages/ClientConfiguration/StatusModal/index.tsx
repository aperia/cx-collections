import React from 'react';
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { useStoreIdSelector } from 'app/hooks';
import {
  selectConfig,
  selectLoadingStatus,
  selectOpenStatusModal
} from '../_redux/selectors';
import { useDispatch } from 'react-redux';
import { clientConfigurationActions } from '../_redux/reducers';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { AdditionalFieldsMapType, ConfigData } from '../types';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { ACTIVATE, CLIENT_CONFIG_STATUS_KEY, DEACTIVATE } from '../constants';
import { genAmtId } from 'app/_libraries/_dls/utils';

const StatusModal = () => {
  const { t } = useTranslation();
  const testId = 'clientConfig_statusModal';

  const dispatch = useDispatch();

  const isOpenModal = useStoreIdSelector<boolean>(selectOpenStatusModal);

  const isLoading = useStoreIdSelector<boolean>(selectLoadingStatus);

  const selectedConfig = useStoreIdSelector<ConfigData>(selectConfig);

  const handleCloseModal = () => {
    dispatch(clientConfigurationActions.toggleStatusModal());
  };

  const handleSubmit = () => {
    dispatch(
      clientConfigurationActions.triggerChangeStatusClientConfiguration({
        id: selectedConfig.id!,
        additionalFields: selectedConfig.additionalFields,
        translateFn: t
      })
    );
  };

  const isConfigActive = selectedConfig?.additionalFields?.find(
    (item: AdditionalFieldsMapType) => item.name === CLIENT_CONFIG_STATUS_KEY
  );

  const isActive = isConfigActive?.active;

  const formattedClientConfigurationText = [
    selectedConfig?.clientId,
    selectedConfig?.systemId,
    selectedConfig?.principleId,
    selectedConfig?.agentId,
    selectedConfig?.description
  ].join(' - ');

  return (
    <Modal
      sm
      show={isOpenModal}
      animationDuration={500}
      loading={isLoading}
      dataTestId={testId}
    >
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId={`${testId}_header`}
      >
        <ModalTitle dataTestId={`${testId}_title`}>
          {t(I18N_CLIENT_CONFIG.CONFIRM_CHANGE_STATUS_CLIENT_CONFIGURATION, {
            status: `${isActive ? DEACTIVATE : ACTIVATE}`
          })}
        </ModalTitle>
      </ModalHeader>
      <ModalBody dataTestId={`${testId}_body`}>
        <p className="mb-16" data-testid={genAmtId(testId, 'description', '')}>
          {t(
            I18N_CLIENT_CONFIG.CONFIRM_CHANGE_STATUS_CLIENT_CONFIGURATION_TEXT,
            {
              status: `${t(
                isActive ? DEACTIVATE.toLowerCase() : ACTIVATE.toLowerCase()
              )}`
            }
          )}
        </p>
        <p data-testid={genAmtId(testId, 'clientConfigTest', '')}>
          {t(I18N_CLIENT_CONFIG.CLIENT_CONFIGURATION_LOWER_CASE)}:{' '}
          <span className="fw-500 text-break">
            {formattedClientConfigurationText}
          </span>
        </p>
      </ModalBody>
      <ModalFooter dataTestId={`${testId}_footer`}>
        <Button
          variant="secondary"
          onClick={handleCloseModal}
          dataTestId={`${testId}_cancelBtn`}
        >
          {t(I18N_COMMON_TEXT.CANCEL)}
        </Button>
        <Button
          variant={isActive ? 'danger' : 'primary'}
          onClick={handleSubmit}
          dataTestId={`${testId}_activate-deactivate-btn`}
        >
          {isActive
            ? t(I18N_COMMON_TEXT.DEACTIVATE)
            : t(I18N_COMMON_TEXT.ACTIVATE)}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default StatusModal;
