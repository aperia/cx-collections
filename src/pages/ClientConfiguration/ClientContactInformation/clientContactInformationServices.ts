import { apiService } from 'app/utils/api.service';
import { UpdateClientContactRequest } from './types';

export const clientContactInformationServices = {
  getPanelContent(requestBody: MagicKeyValue) {
    const url =
      window.appConfig?.api?.clientConfigClientContactInformation
        ?.getPanelContent || '';
    return apiService.post(url, requestBody);
  },
  updatePanelContent(requestBody: UpdateClientContactRequest) {
    const url =
      window.appConfig?.api?.clientConfigClientContactInformation
        ?.updatePanelContent || '';
    return apiService.post(url, requestBody);
  }
};
