export const ALLOWED_TAGS = [
  'h1',
  'h2',
  'h3',
  'h4',
  'h5',
  'a',
  'b',
  'i',
  'strong',
  'div',
  'p'
];

export const ALLOWED_CLASSES = {
  a: ['link']
};
export const I18N_CLIENT_CONFIG_CCT = {
  CLIENT_CONTACT_INFORMATION: 'txt_client_contact_information',
  RESET_TO_PREVIOUS: 'txt_reset_to_previous',
  SAVE_CHANGES: 'txt_save_changes',

  EXAMPLE: 'txt_example',
  CLIENT_CONTACT_INFORMATION_UPDATED: 'txt_client_contact_information_updated',
  CLIENT_CONTACT_INFORMATION_NOT_UPDATED:
    'txt_client_contact_information_failed_to_update',
  CLIENT_CONTACT_INFORMATION_REVERTED:
    'txt_client_contact_information_reverted',
  UNSAVED_CHANGE_BODY: 'txt_unsaved_change_body',
  DISCARD_CHANGE_BODY: 'txt_discard_change_body',
  UNSAVED_CHANGE: 'txt_unsaved_changes',
  CONTINUE_EDITING: 'txt_continue_editing',
  SAVE_AND_CONTINUE: 'txt_save_and_continue',
  DISCARD_CHANGE: 'txt_discard_change'
};

export const EXAMPLE_DATA = `**Client Name**\n\nPhone Number: (163) 498-5764\n\nAddress: 142 Vera Pass, Omaha, NE 12345\n\nHours of Operation: Mon - Fri • 8:00 AM - 5:00 PM\n\nFax: (163) 498-5764\n\nEmail: Acc@gmail.com\n\nWebsite: [www.abc.com](#)`;

export const DEFAULT_CLIENT_CONTACT_DATA = '<div></div>';

export const PANEL_CONTENT_ID = 'ClientInformationTemplate';
