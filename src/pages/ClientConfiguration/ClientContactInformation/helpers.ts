
export const encodeHtmlEntities = (html: any) => {
  if (!html) return '';
  return btoa(escape(encodeURIComponent(html)));
};

export const decodeHtmlEntities = (htmlEncode: string) => {
  if (!htmlEncode) return '';
  return decodeURIComponent(unescape(atob(htmlEncode)));
};

export const removeAttribute = (html: string): string => {
  if (!html) return '';
  return html
};
