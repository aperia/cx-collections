import { ModalData } from 'pages/__commons/ConfirmModal/types';

export interface ClientContactInformationState {
  data?: PanelContentModelPayload;
  tooltip: TooltipState;
  loading?: boolean;
  modifiedContentText?: string;
}

export interface TooltipState {
  visible: boolean;
  text: string;
}

export interface TriggerDataArgs {
  currentData?: string;
  modalPayload: ModalData;
}

export interface UpdateClientContactRequest extends CommonRequest {
  panelContentId?: string;
  panelContentText?: string;
  status?: string;
}

export interface GetPanelContentRequestBody extends CommonRequest {
  panelContentId: string;
}

export interface PanelContentModelPayload {
  panelContentId?: string;
  versionNumber?: string;
  panelContentPageName?: string;
  panelContentText?: string;
  activationDate?: string;
  status?: string;
}

