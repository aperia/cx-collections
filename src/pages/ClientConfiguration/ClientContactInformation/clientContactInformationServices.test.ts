// services
import { clientContactInformationServices } from './clientContactInformationServices';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import { UpdateClientContactRequest } from './types';

describe('clientContactInformationServices', () => {
  describe('getClientContact', () => {
    const params: MagicKeyValue = {};

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      clientContactInformationServices.getPanelContent(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      clientContactInformationServices.getPanelContent(params);

      expect(mockService).toBeCalledWith(
        apiUrl.clientConfigClientContactInformation.getPanelContent,
        params
      );
    });
  });

  describe('updateClientContact', () => {
    const params: UpdateClientContactRequest = {};

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      clientContactInformationServices.updatePanelContent(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      clientContactInformationServices.updatePanelContent(params);

      expect(mockService).toBeCalledWith(
        apiUrl.clientConfigClientContactInformation.updatePanelContent,
        params
      );
    });
  });
});
