import React, { useEffect, useRef, useState } from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';

// Constant
import {
  DEFAULT_CLIENT_CONTACT_DATA,
  I18N_CLIENT_CONFIG_CCT
} from './constants';

// Components
import ExampleDataForm from './ExampleDataForm';
import { Button, SimpleBar } from 'app/_libraries/_dls/components';
import ApiErrorForHeaderSection from '../ApiErrorSection/ApiErrorForHeaderSection';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Redux
import { clientContactInformationActions } from './_redux/reducers';
import {
  selectedClientContact,
  selectedModifiedData
} from './_redux/selectors';
import { useListenChangingTabEvent } from '../hooks/useListenChangingTabEvent';
import { useListenClosingModal } from '../hooks/useListenClosingModal';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// Helpers
import {
  dispatchCloseModalEvent,
  dispatchControlTabChangeEvent
} from '../helpers';
import isEqual from 'lodash.isequal';

// Types
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';
import { removeAttribute } from './helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';
import TextEditor from 'app/_libraries/_dls/components/TextEditor';
import { TextEditorRef } from 'app/_libraries/_dls/components/TextEditor/types';

const ClientContactInformation: React.FC = () => {
  const editorRef = useRef<TextEditorRef | null>(null);
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const preData = useSelector(selectedClientContact);

  const currentData = useSelector(selectedModifiedData);

  const testId = 'clientConfig_clientContactInfo';

  const status = useCheckClientConfigStatus();

  const [dataValue, setDataValue] = useState<string>('');

  const equalData = isEqual(
    removeAttribute(preData),
    removeAttribute(currentData)
  );

  const onSaveChanges = () => {
    dispatch(clientContactInformationActions.updateClientContact({}));
  };

  const onReset = () => {
    if (equalData) return;
    editorRef.current?.setMarkdown(preData);
    batch(() => {
      dispatch(clientContactInformationActions.modifyData(preData));
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_CLIENT_CONFIG_CCT.CLIENT_CONTACT_INFORMATION_REVERTED
        })
      );
    });
  };

  useEffect(() => {
    dispatch(clientContactInformationActions.getPanelContent());
  }, [dispatch]);

  useEffect(() => {
    editorRef.current?.setMarkdown(dataValue);
  }, [dataValue]);
  useEffect(() => {
    setDataValue(preData);
  }, [preData]);
  useListenChangingTabEvent(() => {
    dispatch(
      clientContactInformationActions.triggerChangeTabEvent({
        modalPayload: {
          title: I18N_CLIENT_CONFIG_CCT.UNSAVED_CHANGE,
          body: I18N_CLIENT_CONFIG_CCT.DISCARD_CHANGE_BODY,
          cancelText: I18N_CLIENT_CONFIG_CCT.CONTINUE_EDITING,
          confirmText: I18N_CLIENT_CONFIG_CCT.DISCARD_CHANGE,
          confirmCallback: () => {
            dispatchControlTabChangeEvent();
            dispatch(clientContactInformationActions.resetContactInformation());
          },
          variant: 'danger'
        }
      })
    );
  });

  useListenClosingModal(() => {
    dispatch(
      clientContactInformationActions.triggerCloseTabEvent({
        modalPayload: {
          title: I18N_CLIENT_CONFIG_CCT.UNSAVED_CHANGE,
          body: I18N_CLIENT_CONFIG_CCT.DISCARD_CHANGE_BODY,
          cancelText: I18N_CLIENT_CONFIG_CCT.CONTINUE_EDITING,
          confirmText: I18N_CLIENT_CONFIG_CCT.DISCARD_CHANGE,
          confirmCallback: () => {
            dispatchCloseModalEvent();
            dispatch(clientContactInformationActions.resetContactInformation());
          },
          variant: 'danger'
        }
      })
    );
  });

  const handleOnBlur = () => {
    let modifyData =
      editorRef.current?.getMarkdown() || DEFAULT_CLIENT_CONTACT_DATA;
    modifyData = removeAttribute(modifyData);

    dispatch(clientContactInformationActions.modifyData(modifyData));
  };

  return (
    <div className="position-relative has-footer-button">
      <SimpleBar>
        <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
          <ApiErrorForHeaderSection
            className="mb-24"
            dataTestId={`${testId}_apiError`}
          />
          <h5 className="mb-16" data-testid={genAmtId(testId, 'title', '')}>
            {t(I18N_CLIENT_CONFIG_CCT.CLIENT_CONTACT_INFORMATION)}
          </h5>
          <div className="custom-editor">
            <div className="d-flex flex-xl-row flex-md-column">
              <div className="order-xl-1 order-md-2">
                <TextEditor
                  readOnly={status}
                  onBlur={handleOnBlur}
                  ref={editorRef}
                  dataTestId={`${testId}_editor`}
                />
              </div>
              <div className="order-md-1">
                <ExampleDataForm />
              </div>
            </div>
          </div>
        </div>
      </SimpleBar>
      <div className="group-button-footer d-flex justify-content-end">
        <Button
          disabled={status}
          variant="secondary"
          onClick={onReset}
          dataTestId={`${testId}_resetToDefault-btn`}
        >
          {t(I18N_CLIENT_CONFIG_CCT.RESET_TO_PREVIOUS)}
        </Button>
        <Button
          disabled={status || equalData}
          variant="primary"
          onClick={onSaveChanges}
          dataTestId={`${testId}_saveChanges-btn`}
        >
          {t(I18N_CLIENT_CONFIG_CCT.SAVE_CHANGES)}
        </Button>
      </div>
    </div>
  );
};

export default ClientContactInformation;
