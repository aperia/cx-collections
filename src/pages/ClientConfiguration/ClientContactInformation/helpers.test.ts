import * as helpers from './helpers';

const html = '<div>First & Last</div>';
const htmlEncoded =
  'JTI1M0NkaXYlMjUzRUZpcnN0JTI1MjAlMjUyNiUyNTIwTGFzdCUyNTNDJTI1MkZkaXYlMjUzRQ==';

describe('test encodeHtmlEntites', () => {
  it('should return empty string if html does not exist', () => {
    const result = helpers.encodeHtmlEntities('');
    expect(result).toEqual('');
  });

  it('should return html encoded', () => {
    const result = helpers.encodeHtmlEntities(html);
    expect(result).toEqual(htmlEncoded);
  });
});

describe('test decodeHtmlEntites', () => {
  it('should return empty string if htmlEncode does not exist', () => {
    const result = helpers.decodeHtmlEntities('');
    expect(result).toEqual('');
  });

  it('should return html encoded', () => {
    const result = helpers.decodeHtmlEntities(htmlEncoded);
    expect(result).toEqual(html);
  });

  it('should return html when textContent does not exist', () => {
    DOMParser.prototype.parseFromString = () => '';

    const result = helpers.decodeHtmlEntities(htmlEncoded);
    expect(result).toEqual('<div>First & Last</div>');
  });
});

describe('test removeAttribute', () => {
  it('should return empty string if html does not exist', () => {
    const result = helpers.removeAttribute('');
    expect(result).toEqual('');
  });

  it('should return html encoded', () => {
    const result = helpers.removeAttribute(htmlEncoded);
    expect(result).toEqual(htmlEncoded);
  });
});
