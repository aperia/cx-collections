import React from 'react';
import { fireEvent, screen } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { mockActionCreator, renderMockStore } from 'app/test-utils';

// constants
import ClientContactInformation from '.';
import {
  I18N_CLIENT_CONFIG_CCT,
  DEFAULT_CLIENT_CONTACT_DATA
} from './constants';
import { CLIENT_CONFIG_STATUS_KEY, SECTION_TAB_EVENT } from '../constants';

// redux
import { clientContactInformationActions } from './_redux/reducers';

import * as mockStatus from '../hooks/useCheckClientConfigStatus';

import { TextEditorProps } from 'app/_libraries/_dls/components/TextEditor/types';

jest.mock(
  'app/_libraries/_dls/components/TextEditor',
  () => (props: TextEditorProps) =>
    (
      <>
        <input data-testid="handleBlur" onBlur={props.onBlur} />
      </>
    )
);

jest.mock(
  'pages/ClientConfiguration/ClientContactInformation/ExampleDataForm',
  () => () => <>example</>
);

const mockClientContactInformationActions = mockActionCreator(
  clientContactInformationActions
);

const withClientContactState: Partial<RootState> = {
  clientConfigClientContactInformation: {
    tooltip: {
      visible: false,
      text: 'copy'
    },
    loading: false,
    data: { panelContentText: DEFAULT_CLIENT_CONTACT_DATA },
    modifiedContentText: DEFAULT_CLIENT_CONTACT_DATA
  },
  clientConfiguration: {
    settings: {
      selectedConfig: {
        id: '2'
      }
    }
  }
};

const mockStateWithModifiedData: Partial<RootState> = {
  clientConfigClientContactInformation: {
    tooltip: {
      visible: false,
      text: 'copy'
    },
    loading: false,
    data: { panelContentText: DEFAULT_CLIENT_CONTACT_DATA },
    modifiedContentText: `${DEFAULT_CLIENT_CONTACT_DATA}1`
  },
  clientConfiguration: {
    settings: {
      selectedConfig: {
        id: '2'
      }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(<ClientContactInformation />, {
    initialState
  });
};

describe('Render UI', () => {
  beforeEach(() => {
    jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(false);
  });
  it('should get client contact data', () => {
    jest.useFakeTimers();
    const mockgetPanelContent =
      mockClientContactInformationActions('getPanelContent');

    renderWrapper(withClientContactState);
    jest.runAllTimers();

    expect(mockgetPanelContent).toBeCalled();
  });

  describe('should test submit button', () => {
    it('render submit button', () => {
      jest.useFakeTimers();
      renderWrapper(withClientContactState);
      jest.runAllTimers();

      const saveChangeButton = screen.getByRole('button', {
        name: I18N_CLIENT_CONFIG_CCT.SAVE_CHANGES
      });

      expect(saveChangeButton).toBeInTheDocument();
    });

    it('submit without change data', () => {
      jest.useFakeTimers();
      const mockSubmit = mockClientContactInformationActions(
        'updateClientContact'
      );

      renderWrapper(withClientContactState);
      jest.runAllTimers();

      const saveChangeButton = screen.getByRole('button', {
        name: I18N_CLIENT_CONFIG_CCT.SAVE_CHANGES
      });

      userEvent.click(saveChangeButton);

      expect(mockSubmit).not.toBeCalled();
    });
  });

  describe('should test reset to previous button', () => {
    it('render reset to previous button', () => {
      jest.useFakeTimers();
      renderWrapper(withClientContactState);
      jest.runAllTimers();

      const resetPreviousButton = screen.getByRole('button', {
        name: I18N_CLIENT_CONFIG_CCT.RESET_TO_PREVIOUS
      });

      expect(resetPreviousButton).toBeInTheDocument();
    });

    it('click without modify data', () => {
      jest.useFakeTimers();
      const mockResetAction = mockClientContactInformationActions('modifyData');

      renderWrapper(withClientContactState);
      jest.runAllTimers();

      const resetPreviousButton = screen.getByRole('button', {
        name: I18N_CLIENT_CONFIG_CCT.RESET_TO_PREVIOUS
      });

      userEvent.click(resetPreviousButton);

      expect(mockResetAction).not.toBeCalled();
    });
  });

  describe('should control event on close and change tab', () => {
    let spy: jest.SpyInstance;

    afterEach(() => {
      spy?.mockReset();
      spy?.mockRestore();
    });

    HTMLCanvasElement.prototype.getContext = jest.fn();

    it('should control change tab event', () => {
      jest.useFakeTimers();
      const openConfirmModalAction = jest.fn(x => {
        if (x.modalPayload.confirmCallback) {
          x.modalPayload.confirmCallback();
        }
        return { type: 'type' };
      });

      spy = jest
        .spyOn(clientContactInformationActions, 'triggerChangeTabEvent')
        .mockImplementation(openConfirmModalAction as never);

      renderWrapper(withClientContactState);
      jest.runAllTimers();

      const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGING_TAB, {
        bubbles: true,
        cancelable: true
      });

      window.dispatchEvent(event);

      expect(openConfirmModalAction).toBeCalledWith({
        modalPayload: expect.objectContaining({
          title: I18N_CLIENT_CONFIG_CCT.UNSAVED_CHANGE,
          body: I18N_CLIENT_CONFIG_CCT.DISCARD_CHANGE_BODY,
          cancelText: I18N_CLIENT_CONFIG_CCT.CONTINUE_EDITING,
          confirmText: I18N_CLIENT_CONFIG_CCT.DISCARD_CHANGE,
          variant: 'danger'
        })
      });
    });

    it('should control close tab event', () => {
      jest.useFakeTimers();
      const openConfirmModalAction = jest.fn(x => {
        if (x.modalPayload.confirmCallback) {
          x.modalPayload.confirmCallback();
        }
        return { type: 'type' };
      });

      spy = jest
        .spyOn(clientContactInformationActions, 'triggerCloseTabEvent')
        .mockImplementation(openConfirmModalAction as never);

      renderWrapper(withClientContactState);
      jest.runAllTimers();

      const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
        bubbles: true,
        cancelable: true
      });

      window.dispatchEvent(event);

      expect(openConfirmModalAction).toBeCalled();
    });
  });

  describe('should control editor', () => {
    it('should handle blur event of editor', () => {
      jest.useFakeTimers();
      const mockModifyDataAction =
        mockClientContactInformationActions('modifyData');

      renderWrapper(withClientContactState);
      jest.runAllTimers();

      const mockEditor = screen.getByTestId('handleBlur');
      expect(mockEditor).toBeInTheDocument();

      fireEvent.blur(mockEditor, { target: { value: '' } });

      expect(mockModifyDataAction).toBeCalled();
    });
  });
});

describe('Action', () => {
  it('onSaveChanges', () => {
    const mockUpdateClientContact = mockClientContactInformationActions(
      'updateClientContact'
    );
    jest.useFakeTimers();
    renderWrapper({
      ...mockStateWithModifiedData,
      clientConfiguration: {
        settings: {
          selectedConfig: {
            id: '2',
            additionalFields: [
              {
                name: CLIENT_CONFIG_STATUS_KEY,
                active: true
              }
            ]
          }
        }
      }
    });
    jest.runAllTimers();

    const saveChangeButton = screen.getByRole('button', {
      name: I18N_CLIENT_CONFIG_CCT.SAVE_CHANGES
    });
    saveChangeButton!.click();
    expect(mockUpdateClientContact).toBeCalled();
  });

  it('onReset with mockStateWithModifiedData', () => {
    const mockModifyData = mockClientContactInformationActions('modifyData');
    jest.useFakeTimers();
    renderWrapper({
      ...mockStateWithModifiedData,
      clientConfiguration: {
        settings: {
          selectedConfig: {
            id: '2',
            additionalFields: [
              {
                name: CLIENT_CONFIG_STATUS_KEY,
                active: true
              }
            ]
          }
        }
      }
    });
    jest.runAllTimers();

    const resetButton = screen.getByRole('button', {
      name: I18N_CLIENT_CONFIG_CCT.RESET_TO_PREVIOUS
    });
    resetButton!.click();
    expect(mockModifyData).toBeCalledWith(DEFAULT_CLIENT_CONTACT_DATA);
  });

  it('onReset with withClientContactState', () => {
    const mockModifyData = mockClientContactInformationActions('modifyData');
    jest.useFakeTimers();
    renderWrapper({
      ...withClientContactState,
      clientConfiguration: {
        settings: {
          selectedConfig: {
            id: '2',
            additionalFields: [
              {
                name: CLIENT_CONFIG_STATUS_KEY,
                active: true
              }
            ]
          }
        }
      }
    });
    jest.runAllTimers();

    const resetButton = screen.getByRole('button', {
      name: I18N_CLIENT_CONFIG_CCT.RESET_TO_PREVIOUS
    });
    resetButton!.click();
    expect(mockModifyData).not.toBeCalled();
  });
});
