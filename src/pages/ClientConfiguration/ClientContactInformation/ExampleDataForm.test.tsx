import React from 'react';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { DEFAULT_CLIENT_CONTACT_DATA } from './constants';
import ExampleDataForm from './ExampleDataForm';
import { clientContactInformationActions } from './_redux/reducers';

const currentEventLoopEnd = () => {
  return new Promise(resolve => setImmediate(resolve));
};

const mockClientContactInformationActions = mockActionCreator(
  clientContactInformationActions
);

describe('Render UI', () => {
  const withExampleDataFormState: Partial<RootState> = {
    clientConfigClientContactInformation: {
      tooltip: {
        visible: false,
        text: 'copy'
      },
      data: DEFAULT_CLIENT_CONTACT_DATA
    }
  };

  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<ExampleDataForm />, { initialState });
  };

  it('should render copy button and hover', () => {
    const mockAction = mockClientContactInformationActions('hoverCopyButton');

    renderWrapper(withExampleDataFormState);

    const copyButton = screen.getByRole('button', {
      pressed: true
    });

    userEvent.hover(copyButton);

    expect(mockAction).toBeCalledWith(false);

    userEvent.unhover(copyButton);

    expect(mockAction).toBeCalledWith(false);
  });

  it('should render copy button and dispatch action copy', async () => {
    const mockHoverOut = mockClientContactInformationActions('hoverCopyButton');
    const mockAction = mockClientContactInformationActions('toggleCopyData');

    renderWrapper({});

    const copyButton = screen.getByRole('button', {
      pressed: true
    });

    expect(copyButton).toBeInTheDocument();

    userEvent.click(copyButton);

    expect(mockHoverOut).toBeCalledWith(false);

    await currentEventLoopEnd();

    expect(mockAction).toBeCalledWith(
      '<p><strong>Client Name</strong></p><p>Phone Number: (163) 498-5764</p><p>Address: 142 Vera Pass, Omaha, NE 12345</p><p>Hours of Operation: Mon - Fri • 8:00 AM - 5:00 PM</p><p>Fax: (163) 498-5764</p><p>Email: Acc@gmail.com</p><p>Website: <a href="#">www.abc.com</a></p>'
    );
  });
});
