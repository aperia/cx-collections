import React, { useCallback, useMemo, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// Constants
import { EXAMPLE_DATA, I18N_CLIENT_CONFIG_CCT } from './constants';

// Components
import { Button, Icon, Tooltip } from 'app/_libraries/_dls/components';

// Hooks
import { useRenderElement, useTranslation } from 'app/_libraries/_dls/hooks';

// selectors
import { selectedTooltip } from './_redux/selectors';

// actions
import { clientContactInformationActions } from './_redux/reducers';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { markdownToHTML } from 'app/_libraries/_dls/components/TextEditor';

const ExampleDataForm: React.FC = () => {
  const { t } = useTranslation();
  const renderHTMLRef = useRef<HTMLDivElement>(null);
  const dispatch = useDispatch();
  const tooltip = useSelector(selectedTooltip);
  const testId = 'clientConfig_exampleDataForm';

  const handleCopyAction = useCallback(() => {
    dispatch(
      clientContactInformationActions.toggleCopyData(
        markdownToHTML(EXAMPLE_DATA).html
      )
    );
  }, [dispatch]);

  useRenderElement(
    renderHTMLRef,
    markdownToHTML(EXAMPLE_DATA).element as HTMLElement
  );

  const handleBlur = useCallback(() => {
    dispatch(clientContactInformationActions.hoverCopyButton(false));
  }, [dispatch]);

  const CopyButton: any = useMemo(() => {
    return (
      <Tooltip
        placement="top"
        variant="primary"
        element={t(tooltip.text)}
        triggerClassName="d-flex"
        displayOnClick={true}
      >
        <Button
          variant="icon-secondary"
          size="sm"
          onClick={handleCopyAction}
          onMouseOver={handleBlur}
          aria-pressed="true"
          className="mr-n2"
          dataTestId={`${testId}_copyBtn`}
        >
          <Icon name="product-info" />
        </Button>
      </Tooltip>
    );
  }, [t, tooltip.text, handleCopyAction, handleBlur]);

  return (
    <div className="editor-example">
      <div className="d-flex align-items-center justify-content-between">
        <h6 className="color-grey" data-testid={genAmtId(testId, 'title', '')}>
          {t(I18N_CLIENT_CONFIG_CCT.EXAMPLE)}
        </h6>
        {CopyButton}
      </div>
      <div
        className="mt-16"
        data-testid={genAmtId(testId, 'content', '')}
        ref={renderHTMLRef}
      ></div>
    </div>
  );
};

export default ExampleDataForm;
