import { Dispatch } from '@reduxjs/toolkit';
import isEqual from 'lodash.isequal';
import {
  dispatchCloseModalEvent,
  dispatchControlTabChangeEvent
} from 'pages/ClientConfiguration/helpers';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { TriggerDataArgs } from '../types';

export const triggerChangeTabEvent: AppThunk =
  (args: TriggerDataArgs) => async (dispatch: Dispatch, getState) => {
    const state = getState();
    const { data, modifiedContentText } =
      state?.clientConfigClientContactInformation;
    const { modalPayload } = args;
    if (!isEqual(data?.panelContentText, modifiedContentText)) {
      dispatch(confirmModalActions.open(modalPayload));
    } else {
      dispatchControlTabChangeEvent();
    }
  };

export const triggerCloseTabEvent: AppThunk =
  (args: TriggerDataArgs) => async (dispatch: Dispatch, getState) => {
    const state = getState();
    const { data, modifiedContentText } =
      state?.clientConfigClientContactInformation;
    const { modalPayload } = args;
    if (!isEqual(data?.panelContentText, modifiedContentText)) {
      dispatch(confirmModalActions.open(modalPayload));
    } else {
      dispatchCloseModalEvent();
    }
  };
