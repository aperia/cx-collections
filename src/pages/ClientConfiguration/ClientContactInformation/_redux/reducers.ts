import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Types
import { ClientContactInformationState } from '../types';

// Constants
import {
  updateClientContact,
  updateClientContactBuilder
} from './updateClientContact';
import { getPanelContent, getPanelContentBuilder } from './getClientContact';
import {
  triggerChangeTabEvent,
  triggerCloseTabEvent
} from './triggerSaveChangeData';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { DEFAULT_CLIENT_CONTACT_DATA } from '../constants';
import { copyToClipboard } from 'app/helpers/copyToClipboard';

const initialState = {
  data: {
    panelContentText: DEFAULT_CLIENT_CONTACT_DATA
  },
  tooltip: {
    visible: false,
    text: I18N_COMMON_TEXT.COPY
  },
  modifiedContentText: DEFAULT_CLIENT_CONTACT_DATA
} as ClientContactInformationState;

const { actions, reducer } = createSlice({
  name: 'clientContactInformation',
  initialState,
  reducers: {
    hoverCopyButton: (draftState, action: PayloadAction<boolean>) => {
      draftState.tooltip.visible = action.payload;
      if (action.payload === false) {
        draftState.tooltip.text = I18N_COMMON_TEXT.COPY;
      }
    },
    toggleCopyData: (draftState, action: PayloadAction<string>) => {
      draftState.tooltip.visible = true;
      draftState.tooltip.text = I18N_COMMON_TEXT.COPIED;
      copyToClipboard(action.payload);
    },
    modifyData: (draftState, action: PayloadAction<string>) => {
      draftState.modifiedContentText = action.payload;
    },
    resetContactInformation: draftState => {
      draftState = initialState;
    }
  },
  extraReducers: builder => {
    getPanelContentBuilder(builder);
    updateClientContactBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getPanelContent,
  updateClientContact,
  triggerChangeTabEvent,
  triggerCloseTabEvent
};

export { allActions as clientContactInformationActions, reducer };
