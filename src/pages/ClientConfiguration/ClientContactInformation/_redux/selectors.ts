import { createSelector } from 'reselect';
import { DEFAULT_CLIENT_CONTACT_DATA } from '../constants';
import { ClientContactInformationState, TooltipState } from '../types';

const getClientContactInformation = (states: RootState) => {
  return states?.clientConfigClientContactInformation;
};

const getTooltip = (states: RootState) => {
  return states?.clientConfigClientContactInformation?.tooltip;
};

export const selectedClientContact = createSelector(
  getClientContactInformation,
  (data: ClientContactInformationState) => data?.data?.panelContentText || DEFAULT_CLIENT_CONTACT_DATA
);

export const selectedModifiedData = createSelector(
  getClientContactInformation,
  (data: ClientContactInformationState) => data?.modifiedContentText || DEFAULT_CLIENT_CONTACT_DATA
);

export const selectedTooltip = createSelector(
  getTooltip,
  (data: TooltipState) => data
);

export const selectedLoadingClientContactInformation = createSelector(
  getClientContactInformation,
  (data: ClientContactInformationState) => data?.loading
);
