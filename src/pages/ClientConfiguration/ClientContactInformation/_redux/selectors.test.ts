import { selectorWrapper } from 'app/test-utils';
import { DEFAULT_CLIENT_CONTACT_DATA } from '../constants';
import {
  selectedTooltip,
  selectedClientContact,
  selectedModifiedData,
  selectedLoadingClientContactInformation
} from './selectors';

describe('Test Client Contact Information Selectors', () => {
  const store: Partial<RootState> = {
    clientConfigClientContactInformation: {
      tooltip: {
        visible: false,
        text: 'copy'
      },
      data: {
        panelContentText: DEFAULT_CLIENT_CONTACT_DATA
      },
      modifiedContentText: DEFAULT_CLIENT_CONTACT_DATA,
      loading: false
    }
  };

  it('selectedTooltip', () => {
    const { data, emptyData } = selectorWrapper(store)(selectedTooltip);

    expect(data).toEqual({
      visible: false,
      text: 'copy'
    });
    expect(emptyData).toEqual(undefined);
  });

  it('selectedClientContact', () => {
    const { data, emptyData } = selectorWrapper(store)(selectedClientContact);

    expect(data).toEqual(DEFAULT_CLIENT_CONTACT_DATA);
    expect(emptyData).toEqual('<div></div>');
  });

  it('selectedModifiedData', () => {
    const { data, emptyData } = selectorWrapper(store)(selectedModifiedData);

    expect(data).toEqual(DEFAULT_CLIENT_CONTACT_DATA);
    expect(emptyData).toEqual('<div></div>');
  });

  it('selectedModifiedData', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedLoadingClientContactInformation
    );

    expect(data).toEqual(false);
    expect(emptyData).toEqual(undefined);
  });
});
