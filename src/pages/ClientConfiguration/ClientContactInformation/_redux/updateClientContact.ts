import { batch } from 'react-redux';
import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';

// Services
import { clientContactInformationServices } from '../clientContactInformationServices';

// Toast
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// Constants
import { I18N_CLIENT_CONFIG_CCT, PANEL_CONTENT_ID } from '../constants';

// Types
import {
  ClientContactInformationState,
  UpdateClientContactRequest
} from '../types';

// Reducers
import { clientContactInformationActions } from './reducers';
import { AxiosResponse } from 'axios';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';
import { encodeHtmlEntities, removeAttribute } from '../helpers';

export interface updateClientContactArgs extends UpdateClientContactRequest {}

export const updateClientContactAPI = createAsyncThunk<
  AxiosResponse,
  updateClientContactArgs,
  ThunkAPIConfig
>(
  'clientConfigClientContactInformation/updateClientContactAPI',
  async (args, { getState, rejectWithValue }) => {
    const contactInformationData =
      getState().clientConfigClientContactInformation?.data || {};

    const { commonConfig } = window.appConfig;
    const { status } = contactInformationData;

    try {
      const { agentId, clientId, systemId, principleId } =
        getState().clientConfiguration.settings?.selectedConfig || {};
      const panelContent =
        getState().clientConfigClientContactInformation.modifiedContentText;

      const requestBody: UpdateClientContactRequest = {
        common: {
          clientNumber: clientId,
          system: systemId,
          prin: principleId,
          agent: agentId,
          app: commonConfig.app,
          user: commonConfig.user,
          org: commonConfig.org,
          userPrivileges: {
            cspa: [`${clientId}-${systemId}-${principleId}-${agentId}`],
            groups: ['admin']
          }
        },
        panelContentId: PANEL_CONTENT_ID,
        status: status ? status : 'REVIEW',
        panelContentText:
          panelContent?.replace(/\'/g, '')?.replace(/"/g, '\\"') || ''
      };
      const response =
        await clientContactInformationServices.updatePanelContent(requestBody);

      if (response.data.responseStatus?.toLowerCase() !== 'success')
        throw response;

      return response;
    } catch (error) {
      return rejectWithValue({ response: error });
    }
  }
);

export const updateClientContact = createAsyncThunk<
  void,
  updateClientContactArgs,
  ThunkAPIConfig
>(
  'clientConfigClientContactInformation/updateClientContact',
  async (args, { dispatch, getState }) => {
    const response = await dispatch(updateClientContactAPI(args));
    if (isFulfilled(response)) {
      const { data, modifiedContentText } =
        getState().clientConfigClientContactInformation;
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_CLIENT_CONFIG_CCT.CLIENT_CONTACT_INFORMATION_UPDATED
          })
        );
        dispatch(clientContactInformationActions.getPanelContent());
        dispatch(
          changeHistoryActions.saveChangedClientConfig({
            action: 'UPDATE',
            changedCategory: 'clientContactInformation',
            oldValue: {
              panelContentText: encodeHtmlEntities(
                removeAttribute(data?.panelContentText || '')
              )
            },
            newValue: {
              panelContentText: encodeHtmlEntities(
                removeAttribute(modifiedContentText || '')
              )
            }
          })
        );
      });
      return;
    }

    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_CLIENT_CONFIG_CCT.CLIENT_CONTACT_INFORMATION_NOT_UPDATED
      })
    );
    dispatch(
      apiErrorNotificationAction.updateApiError({
        storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION,
        forSection: 'inClientConfigurationFeatureHeader',
        apiResponse: response.payload?.response
      })
    );
  }
);

export const updateClientContactBuilder = (
  builder: ActionReducerMapBuilder<ClientContactInformationState>
) => {
  builder
    .addCase(updateClientContact.pending, (draftState, action) => {
      draftState.loading = true;
    })
    .addCase(updateClientContact.fulfilled, (draftState, action) => {
      draftState.loading = false;
    })
    .addCase(updateClientContact.rejected, (draftState, action) => {
      draftState.loading = false;
    });
};
