import { createStore, Store } from '@reduxjs/toolkit';
import { mockActionCreator, responseDefault } from 'app/test-utils';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { rootReducer } from 'storeConfig';
import { clientContactInformationServices } from '../clientContactInformationServices';
import { getPanelContent } from './getClientContact';

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    org: 'org',
    app: 'app'
  }
};
const mockApiErrorNotificationAction = mockActionCreator(
  apiErrorNotificationAction
);
describe('should have test getPanelContent', () => {
  let store: Store<RootState>;
  let state: RootState;

  let spy: jest.SpyInstance;

  beforeEach(() => {
    store = createStore(rootReducer, {
      clientConfigClientContactInformation: {},
      clientConfiguration: {
        settings: {
          selectedConfig: {
            id: '9999100020003000',
            agentId: '3000',
            clientId: '9999',
            systemId: '1000',
            principleId: '2000'
          }
        }
      }
    });
    state = store.getState();
  });

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('Should return data with responseStatus success', async () => {
    const mockClearApiErrorData =
      mockApiErrorNotificationAction('clearApiErrorData');
    const mockRes = {
      ...responseDefault,
      data: {
        responseStatus: 'success',
        panelContentModel: {
          panelContentText: '<div>abc</div>'
        }
      }
    };
    spy = jest
      .spyOn(clientContactInformationServices, 'getPanelContent')
      .mockResolvedValue(mockRes);

    const response = await getPanelContent()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toBeDefined();

    expect(mockClearApiErrorData).toHaveBeenCalled();
  });

  it('Should return data with responseStatus failure', async () => {
    const mockUpdateApiError = mockApiErrorNotificationAction('updateApiError');
    const mockRes = {
      ...responseDefault,
      data: {
        responseStatus: 'failure'
      }
    };
    spy = jest
      .spyOn(clientContactInformationServices, 'getPanelContent')
      .mockResolvedValue(mockRes);

    const response = await getPanelContent()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toBeDefined();
    expect(mockUpdateApiError).toHaveBeenCalled();
  });

  it('Should return data when has no config', async () => {
    store = createStore(rootReducer, {
      clientConfigClientContactInformation: {},
      clientConfiguration: {
        settings: {}
      }
    });
    state = store.getState();

    const mockUpdateApiError = mockApiErrorNotificationAction('updateApiError');
    const mockRes = {
      ...responseDefault,
      data: {
        responseStatus: 'failure'
      }
    };
    spy = jest
      .spyOn(clientContactInformationServices, 'getPanelContent')
      .mockResolvedValue(mockRes);

    const response = await getPanelContent()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toBeDefined();
    expect(mockUpdateApiError).toHaveBeenCalled();
  });

  it('Should response getPanelContent pending', () => {
    const pendingAction = getPanelContent.pending(
      getPanelContent.pending.type,
      undefined
    );

    const actual = rootReducer(state, pendingAction);
    expect(actual.clientConfigClientContactInformation.loading).toEqual(true);
  });

  it('Should response getPanelContent fulfilled', () => {
    const fulfilled = getPanelContent.fulfilled(
      {},
      getPanelContent.fulfilled.type,
      undefined
    );

    const actual = rootReducer(state, fulfilled);

    expect(actual.clientConfigClientContactInformation.loading).toEqual(false);
    expect(actual.clientConfigClientContactInformation.data).toEqual({
      panelContentText: undefined,
      status: undefined
    });
  });

  it('Should response getPanelContent rejected', () => {
    const rejected = getPanelContent.rejected(
      null,
      getPanelContent.rejected.type,
      undefined
    );

    const actual = rootReducer(state, rejected);

    expect(actual.clientConfigClientContactInformation.loading).toEqual(false);
  });
});
