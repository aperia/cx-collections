import { createStore, Store } from '@reduxjs/toolkit';
import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault
} from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

import { rootReducer } from 'storeConfig';
import { clientContactInformationServices } from '../clientContactInformationServices';

import { I18N_CLIENT_CONFIG_CCT } from '../constants';

import {
  updateClientContact,
  updateClientContactAPI,
  updateClientContactArgs
} from './updateClientContact';

const mockToast = mockActionCreator(actionsToast);
window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    org: 'org',
    app: 'app'
  }
};
describe('should have test updateClientContact', () => {
  const mockArgs: updateClientContactArgs = {
    panelContentId: 'ClientInformationTemplate',
    panelContentText: '<div></div>',
    status: 'REVIEW'
  };

  let store: Store<RootState>;
  let state: RootState;
  let spy: jest.SpyInstance;

  beforeEach(() => {
    store = createStore(rootReducer, {
      clientConfigClientContactInformation: {},
      clientConfiguration: {
        settings: {
          selectedConfig: {
            id: '9999100020003000',
            agentId: '3000',
            clientId: '9999',
            systemId: '1000',
            principleId: '2000'
          }
        }
      }
    });
    state = store.getState();
  });

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  describe('should run updateClientContactAPI', () => {
    it('should return data', async () => {
      const mockRes = {
        ...responseDefault,
        data: {
          responseStatus: 'success'
        }
      };
      spy = jest
        .spyOn(clientContactInformationServices, 'updatePanelContent')
        .mockResolvedValue(mockRes);

      const response = await updateClientContactAPI(mockArgs)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(response.payload).toBeDefined();
    });

    it('should return data when has no config', async () => {
      store = createStore(rootReducer, {
        clientConfigClientContactInformation: {
          data: { status: 'REVIEW' }
        },
        clientConfiguration: {
          settings: {}
        }
      });
      state = store.getState();
      const mockRes = {
        ...responseDefault,
        data: {
          responseStatus: 'success'
        }
      };
      spy = jest
        .spyOn(clientContactInformationServices, 'updatePanelContent')
        .mockResolvedValue(mockRes);

      const response = await updateClientContactAPI(mockArgs)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(response.payload).toBeDefined();
    });

    it('should return error', async () => {
      const mockRes = {
        ...responseDefault,
        data: {
          responseStatus: 'failure'
        }
      };
      spy = jest
        .spyOn(clientContactInformationServices, 'updatePanelContent')
        .mockResolvedValue(mockRes);

      const response = await updateClientContactAPI(mockArgs)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(response.payload).toBeDefined();
    });
  });

  describe('should trigger updateClientContact', () => {
    it('should updateClientContact', async () => {
      spy = jest
        .spyOn(clientContactInformationServices, 'updatePanelContent')
        .mockResolvedValue({
          ...responseDefault
        });

      const response = await updateClientContact(mockArgs)(
        store.dispatch,
        store.getState,
        undefined
      );

      expect(response.payload).toEqual(undefined);
    });

    it('should update successfully', async () => {
      const addToastAction = mockToast('addToast');

      const response = await updateClientContact(mockArgs)(
        byPassFulfilled(updateClientContact.fulfilled.type),
        store.getState,
        {}
      );

      expect(response.type).toBe(updateClientContact.fulfilled.type);

      expect(addToastAction).toBeCalledWith({
        show: true,
        type: 'success',
        message: I18N_CLIENT_CONFIG_CCT.CLIENT_CONTACT_INFORMATION_UPDATED
      });
    });

    it('should update failed', async () => {
      const addToastAction = mockToast('addToast');

      await updateClientContact(mockArgs)(
        byPassRejected(updateClientContact.fulfilled.type),
        store.getState,
        {}
      );

      expect(addToastAction).toBeCalledWith({
        show: true,
        type: 'error',
        message: I18N_CLIENT_CONFIG_CCT.CLIENT_CONTACT_INFORMATION_NOT_UPDATED
      });
    });
  });

  it('should response updateClientContact pending', () => {
    const pendingAction = updateClientContact.pending(
      updateClientContact.pending.type,
      mockArgs
    );
    const actual = rootReducer(state, pendingAction);

    expect(actual.clientConfigClientContactInformation.loading).toEqual(true);
  });

  it('should response updateClientContact fulfilled', () => {
    const fulfilled = updateClientContact.fulfilled(
      undefined,
      updateClientContact.fulfilled.type,
      mockArgs
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual.clientConfigClientContactInformation.loading).toEqual(false);
  });

  it('should response updateClientContact reject', () => {
    const rejected = updateClientContact.rejected(
      null,
      updateClientContact.rejected.type,
      mockArgs
    );
    const actual = rootReducer(state, rejected);

    expect(actual.clientConfigClientContactInformation.loading).toEqual(false);
  });
});
