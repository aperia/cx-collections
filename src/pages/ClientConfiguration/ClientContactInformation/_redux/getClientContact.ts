import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// Services
import { clientContactInformationServices } from '../clientContactInformationServices';

// Constants
import { PANEL_CONTENT_ID } from '../constants';
// Types
import {
  ClientContactInformationState,
  PanelContentModelPayload
} from '../types';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';

export interface GetClientContactPayload {
  data?: string;
}

export const getPanelContent = createAsyncThunk<
  PanelContentModelPayload,
  undefined,
  ThunkAPIConfig
>(
  'clientConfigClientContactInformation/getPanelContent',
  async (args, thunkAPI) => {
    const { getState, dispatch } = thunkAPI;
    const { commonConfig } = window.appConfig;

    try {
      const { agentId, clientId, systemId, principleId } =
        getState().clientConfiguration.settings?.selectedConfig || {};

      const requestBody: MagicKeyValue = {
        common: {
          clientNumber: clientId,
          system: systemId,
          prin: principleId,
          agent: agentId,
          app: commonConfig.app,
          user: commonConfig.user,
          org: commonConfig.org,
          clientName: 'firstdata'
        },
        panelContentId: PANEL_CONTENT_ID
      };
      const response = await clientContactInformationServices.getPanelContent(
        requestBody
      );

      if (response.data.responseStatus?.toLowerCase() !== 'success') {
        throw response;
      }

      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          forSection: 'inClientConfigurationFeatureHeader',
          storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION
        })
      );

      return response.data?.panelContentModel;
    } catch (error) {
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION,
          forSection: 'inClientConfigurationFeatureHeader',
          apiResponse: error
        })
      );
      return thunkAPI.rejectWithValue({ response: error });
    }
  }
);

export const getPanelContentBuilder = (
  builder: ActionReducerMapBuilder<ClientContactInformationState>
) => {
  builder
    .addCase(getPanelContent.pending, (draftState, action) => {
      draftState.loading = true;
    })
    .addCase(getPanelContent.fulfilled, (draftState, action) => {
      const { panelContentText, status } = action.payload;
      draftState.loading = false;
      draftState.modifiedContentText = panelContentText;
      draftState.data = {
        panelContentText: panelContentText,
        status: status
      };
    })
    .addCase(getPanelContent.rejected, (draftState, action) => {
      draftState.loading = false;
    });
};
