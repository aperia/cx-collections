import { DEFAULT_CLIENT_CONTACT_DATA } from '../constants';
import { ClientContactInformationState } from '../types';
import {
  clientContactInformationActions,
  reducer as clientContactInformation
} from './reducers';

jest.mock('app/helpers/copyToClipboard', () => ({
  copyToClipboard: jest.fn()
}));

describe('Test Client Contact Information Reducers', () => {
  const initialState: ClientContactInformationState = {
    tooltip: {
      visible: false,
      text: 'copy'
    },
    data: {
      panelContentText: DEFAULT_CLIENT_CONTACT_DATA
    },
    loading: false
  };

  it('should hover copy button', () => {
    const state = clientContactInformation(
      initialState,
      clientContactInformationActions.hoverCopyButton(true)
    );

    expect(state.tooltip.visible).toEqual(true);
  });

  it('should toggle copy button', () => {
    const state = clientContactInformation(
      initialState,
      clientContactInformationActions.toggleCopyData('mock data')
    );

    expect(state.tooltip.visible).toEqual(true);
  });

  it('should unhover copy button', () => {
    const state = clientContactInformation(
      initialState,
      clientContactInformationActions.hoverCopyButton(false)
    );

    expect(state.tooltip.visible).toEqual(false);
  });

  it('should set client contact data', () => {
    const state = clientContactInformation(
      initialState,
      clientContactInformationActions.resetContactInformation()
    );

    expect(state.data?.panelContentText).toEqual(DEFAULT_CLIENT_CONTACT_DATA);
  });

  it('should set modify data', () => {
    const state = clientContactInformation(
      initialState,
      clientContactInformationActions.modifyData(DEFAULT_CLIENT_CONTACT_DATA)
    );

    expect(state.modifiedContentText).toEqual(DEFAULT_CLIENT_CONTACT_DATA);
  });
});
