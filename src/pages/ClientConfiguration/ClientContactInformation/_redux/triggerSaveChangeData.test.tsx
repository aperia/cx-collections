import React from 'react';
import { Store, createStore } from '@reduxjs/toolkit';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { rootReducer } from 'storeConfig';
import { DEFAULT_CLIENT_CONTACT_DATA } from '../constants';
import {
  triggerChangeTabEvent,
  triggerCloseTabEvent
} from './triggerSaveChangeData';
import * as clientConfigHelper from 'pages/ClientConfiguration/helpers';
import { SECTION_TAB_EVENT } from 'pages/ClientConfiguration/constants';
import ClientContactInformation from '..';

const mockModalActions = mockActionCreator(confirmModalActions);

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('should have test triggerSaveChangeData', () => {
  let store: Store<RootState>;
  let spy: jest.SpyInstance;
  const initialState: Partial<RootState> = {
    clientConfigClientContactInformation: {
      tooltip: {
        visible: false,
        text: 'copy'
      }
    },
    clientConfiguration: {
      settings: {
        selectedConfig: {
          id: '2'
        }
      }
    }
  };

  beforeEach(() => {
    store = createStore(rootReducer, {
      clientConfigClientContactInformation: {
        data: { panelContentText: DEFAULT_CLIENT_CONTACT_DATA }
      }
    });
  });

  describe('triggerCloseTabEvent', () => {
    const mockDispatchCloseModalEvent = jest.fn();

    afterEach(() => {
      spy?.mockReset();
      spy?.mockRestore();
    });

    it('data was changed', () => {
      const mockOpenModal = mockModalActions('open');
      triggerCloseTabEvent({ currentData: `${DEFAULT_CLIENT_CONTACT_DATA}1` })(
        store.dispatch,
        store.getState,
        {}
      );

      expect(mockOpenModal).toHaveBeenCalled();
    });

    it('data has not changed', () => {
      jest.useFakeTimers();
      triggerCloseTabEvent({ currentData: DEFAULT_CLIENT_CONTACT_DATA })(
        store.dispatch,
        store.getState,
        {}
      );

      spy = jest
        .spyOn(clientConfigHelper, 'dispatchCloseModalEvent')
        .mockImplementation(mockDispatchCloseModalEvent);

      renderMockStore(<ClientContactInformation />, { initialState });
      jest.runAllTimers();

      const closingModalEvent = new CustomEvent<string>(
        SECTION_TAB_EVENT.CLOSING_MODAL,
        {
          bubbles: true,
          cancelable: true
        }
      );

      window.dispatchEvent(closingModalEvent);

      expect(mockDispatchCloseModalEvent).toBeCalled();
    });
  });

  describe('triggerChangeTabEvent', () => {
    const mockDispatchEventChangeTab = jest.fn();

    afterEach(() => {
      spy?.mockReset();
      spy?.mockRestore();
    });

    it('data was changed', () => {
      const mockOpenModal = mockModalActions('open');
      triggerChangeTabEvent({ currentData: `${DEFAULT_CLIENT_CONTACT_DATA}1` })(
        store.dispatch,
        store.getState,
        {}
      );

      expect(mockOpenModal).toHaveBeenCalled();
    });

    it('data has not changed', () => {
      jest.useFakeTimers();

      triggerChangeTabEvent({ currentData: DEFAULT_CLIENT_CONTACT_DATA })(
        store.dispatch,
        store.getState,
        {}
      );

      spy = jest
        .spyOn(clientConfigHelper, 'dispatchControlTabChangeEvent')
        .mockImplementation(mockDispatchEventChangeTab);

      renderMockStore(<ClientContactInformation />, { initialState });
      jest.runAllTimers();

      const changeTabEvent = new CustomEvent<string>(
        SECTION_TAB_EVENT.CHANGING_TAB,
        {
          bubbles: true,
          cancelable: true
        }
      );

      window.dispatchEvent(changeTabEvent);

      expect(mockDispatchEventChangeTab).toBeCalled();
    });
  });
});
