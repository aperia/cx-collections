import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { screen, fireEvent } from '@testing-library/react';

import ReferencesModal from './ReferencesModal';

//actions
import { referenceActions } from './_redux/reducers';

//constants
import { I18N_COMMON_TEXT, I18N_REFERENCE } from 'app/constants/i18n';

import * as useSelectFormValue from 'app/hooks/useSelectFormValue';
import { referenceName } from './helper';
import userEvent from '@testing-library/user-event';
import * as reduxForm from 'redux-form';
import { REFERENCE_DROPDOWN_ITEM_NAME } from './constants';
// import { ReferencesModalType } from './types';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const initialState: Partial<RootState> = {
  reference: {
    dropdownList: [
      {
        id: '1',
        code: 'AG',
        value: 'Adjustment Group',
        reference: ''
      },
      { id: '2', code: 'DC', value: 'Deceased Contact', reference: '' },
      { id: '3', code: 'DR', value: 'Delinquency Reason', reference: '' },
      { id: '4', code: 'MTQ', value: 'Move to Queue', reference: '' }
    ],
    gridList: {
      loading: false,
      error: '',
      data: []
    },
    listCode: [],
    loading: false,
    modal: {
      modalType: 'activate',
      open: true,
      selectedItem: {},
      referenceTypeCode: 'AG'
    },
    selectedDropdown: []
  } as any
};

const mockCodeToTextActions = mockActionCreator(referenceActions);

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(<ReferencesModal />, { initialState });
};

describe('Render UI', () => {
  it('when click cancel button in modal', () => {
    const mockAction = mockCodeToTextActions('closeReferenceModal');
    renderWrapper({
      reference: {
        ...initialState.reference,
        modal: {
          modalType: 'add',
          open: true,
          selectedItem: {},
          referenceTypeCode: 'AG'
        }
      }
    } as any);
    const cancelButton = screen.getByRole('button', {
      name: I18N_REFERENCE.CANCEL
    });
    expect(cancelButton).toBeInTheDocument();
    fireEvent.click(cancelButton);
    expect(mockAction).toHaveBeenCalled();
  });
});

describe('handleClick', () => {
  const formData = {
    code: 'code',
    description: 'description'
  };
  let spy: jest.SpyInstance;
  beforeEach(() => {
    spy = jest
      .spyOn(useSelectFormValue, 'useSelectFormValue')
      .mockImplementation(() => formData);
  });
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('when click submit button without filling form in modal > referenceTypeCode AG', () => {
    renderWrapper({
      reference: {
        ...initialState.reference,
        modal: {
          modalType: 'add',
          open: true,
          selectedItem: {},
          referenceTypeCode: 'AG'
        }
      }
    } as any);
    const submitButton = screen.getByRole('button', {
      name: I18N_REFERENCE.SUBMIT
    });
    expect(submitButton).toBeInTheDocument();
    fireEvent.click(submitButton);
    expect(submitButton).toHaveAttribute('disabled', '');
  });

  it('when click submit button without filling form in modal > referenceTypeCode MTQ', () => {
    renderWrapper({
      reference: {
        ...initialState.reference,
        modal: {
          modalType: 'add',
          open: true,
          selectedItem: {},
          referenceTypeCode: 'MTQ'
        }
      }
    } as any);
    const submitButton = screen.getByRole('button', {
      name: I18N_REFERENCE.SUBMIT
    });
    expect(submitButton).toBeInTheDocument();
    fireEvent.click(submitButton);
    expect(submitButton).toHaveAttribute('disabled', '');
  });

  it('when click submit button in add modal', () => {
    jest.spyOn(reduxForm, 'isDirty').mockImplementation(() => () => true);
    const mockAction = mockCodeToTextActions('addReference');
    const mockSetErrorMessage = mockCodeToTextActions('setErrorMessage');
    renderWrapper({
      reference: {
        ...initialState.reference,
        modal: {
          modalType: 'add',
          open: true,
          selectedItem: {
            code: 'AG 1',
            description: 'AG 1'
          },
          referenceTypeCode: 'AG'
        }
      }
    } as any);
    const submitButton = screen.getByRole('button', {
      name: I18N_REFERENCE.SUBMIT
    });
    expect(submitButton).toBeInTheDocument();
    fireEvent.click(submitButton);
    expect(mockAction).toBeCalledWith({
      data: { code: 'code', description: 'description' },
      label: REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP,
      referenceTypeCode: 'AG'
    });
    expect(mockSetErrorMessage).toBeCalledWith({ message: '' });
  });

  it('when click submit button in delete modal', () => {
    jest.spyOn(reduxForm, 'isDirty').mockImplementation(() => () => true);
    const mockAction = mockCodeToTextActions('deleteReference');
    renderWrapper({
      reference: {
        ...initialState.reference,
        modal: {
          modalType: 'delete',
          open: true,
          selectedItem: {
            id: 'AG 1',
            code: 'AG 1',
            description: 'AG 1',
            isActivate: true
          },
          referenceTypeCode: 'AG'
        }
      }
    } as any);
    const deleteButton = screen.getByRole('button', {
      name: I18N_REFERENCE.DELETE
    });
    expect(deleteButton).toBeInTheDocument();
    fireEvent.click(deleteButton);
    expect(mockAction).toBeCalledWith({
      referenceTypeCode: 'AG',
      label: REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP,
      data: {
        id: 'AG 1',
        code: 'AG 1',
        description: 'AG 1',
        isActivate: true
      }
    });
  });

  it('when click submit button in activate modal', () => {
    jest.spyOn(reduxForm, 'isDirty').mockImplementation(() => () => true);
    const mockAction = mockCodeToTextActions('activateReference');
    renderWrapper({
      reference: {
        ...initialState.reference,
        modal: {
          modalType: 'activate',
          open: true,
          selectedItem: {
            id: 'AG 1',
            code: 'AG 1',
            description: 'AG 1',
            isActivate: false
          },
          referenceTypeCode: 'AG'
        }
      }
    } as any);
    const activateButton = screen.getByRole('button', {
      name: I18N_COMMON_TEXT.ACTIVATE
    });
    expect(activateButton).toBeInTheDocument();
    fireEvent.click(activateButton);
    expect(mockAction).toBeCalledWith({
      referenceTypeCode: 'AG',
      label: REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP,
      data: {
        id: 'AG 1',
        code: 'AG 1',
        description: 'AG 1',
        isActivate: false
      }
    });
  });

  it('when click submit button in edit modal', () => {
    jest.spyOn(reduxForm, 'isDirty').mockImplementation(() => () => true);
    const mockEditAction = mockCodeToTextActions('editReference');
    renderWrapper({
      reference: {
        ...initialState.reference,
        modal: {
          modalType: 'edit',
          open: true,
          selectedItem: {
            id: 'AG 1',
            code: 'AG 1',
            description: 'AG 1',
            isActivate: true
          },
          referenceTypeCode: 'AG'
        }
      }
    } as any);
    const editButton = screen.getByRole('button', {
      name: I18N_REFERENCE.SAVE
    });
    expect(editButton).toBeInTheDocument();
    userEvent.click(editButton);
    expect(mockEditAction).toBeCalledWith({
      data: {
        code: 'AG 1',
        description: 'description',
        id: 'AG 1',
        isActivate: true
      },
      label: REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP,
      previousValue: {
        code: 'AG 1',
        description: 'AG 1',
        id: 'AG 1',
        isActivate: true
      },
      referenceTypeCode: 'AG'
    });
  });

  it('should render errorMessage', () => {
    jest.spyOn(reduxForm, 'isDirty').mockImplementation(() => () => true);
    // const mockAction = mockCodeToTextActions('addReference');
    // const mockSetErrorMessage = mockCodeToTextActions('setErrorMessage');
    const { rerender } = renderWrapper({
      reference: {
        ...initialState.reference,
        modal: {
          modalType: 'add',
          open: true,
          selectedItem: {},
          referenceTypeCode: 'AG',
          error: {
            message: 'txt_reference_code_duplicate_msg',
            msgVariables: {
              name: `${referenceName('AG')}`
            }
          }
        }
      }
    } as any);
    rerender(<ReferencesModal />);
    expect(
      screen.getByText('txt_reference_code_duplicate_msg')
    ).toBeInTheDocument();
  });
});
