//components
import { referenceName, dynamicMsg } from './helper';

//constants
import { REFERENCE_DROPDOWN_ITEM_NAME } from './constants';

describe('referenceName', () => {
  it('referenceName with AG', () => {
    const value = referenceName('AG');

    expect(value).toEqual(REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP);
  });
  it('referenceName with DR', () => {
    const value = referenceName('DR');

    expect(value).toEqual(REFERENCE_DROPDOWN_ITEM_NAME.DELINQUENCY_REASON);
  });
  it('referenceName with DC', () => {
    const value = referenceName('DC');

    expect(value).toEqual(REFERENCE_DROPDOWN_ITEM_NAME.DECEASED_CONTACT);
  });
  it('referenceName with MTQ', () => {
    const value = referenceName('MTQ');

    expect(value).toEqual(REFERENCE_DROPDOWN_ITEM_NAME.MOVE_TO_QUEUE);
  });

  it('referenceName with default', () => {
    const value = referenceName('');

    expect(value).toEqual('');
  });
});

describe('dynamicMsg', () => {
  it('dynamicMsg', () => {
    const msg = dynamicMsg('Adjustment Group');

    expect(msg).toEqual('Adjustment group');
  });
});
