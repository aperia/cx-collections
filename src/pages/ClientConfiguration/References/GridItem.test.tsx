import React from 'react';

//mock test utils
import {
  mockActionCreator,
  queryByClass,
  renderMockStore
} from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';

//components
import GridItem, { renderActions } from './GridItem';

//actions
import { referenceActions } from './_redux/reducers';
import userEvent from '@testing-library/user-event';
import { I18N_REFERENCE } from 'app/constants/i18n';
import { GridProps } from 'app/_libraries/_dls/components';
import * as mockStatus from '../hooks/useCheckClientConfigStatus';

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');

  return {
    ...dlsComponents,
    Grid: ({ onDataChange }: GridProps) => {
      return (
        <div data-testid="table">
          <div
            data-testid="Grid_onDataChange"
            onClick={() => onDataChange!([])}
          />
        </div>
      );
    }
  };
});

const mockReferenceActions = mockActionCreator(referenceActions);

describe('render actions buttons ', () => {
  const dataAG = [
    { id: '1', code: 'AG 1', description: 'AG 1', isActivate: true },
    { id: '2', code: 'AG 2', description: 'AG 2', isActivate: false },
    { id: '3', code: 'AG 3', description: 'AG 3', isActivate: true },
    { id: '3', code: 'AG 4', description: 'AG 4', isActivate: true },
    { id: '3', code: 'AG 5', description: 'AG 5', isActivate: true },
    { id: '3', code: 'AG 6', description: 'AG 6', isActivate: true },
    { id: '3', code: 'AG 7', description: 'AG 7', isActivate: true },
    { id: '3', code: 'AG 8', description: 'AG 8', isActivate: true },
    { id: '3', code: 'AG 9', description: 'AG 9', isActivate: true },
    {
      id: '3',
      code: 'AG 10',
      description: 'AG 10',
      isActivate: true
    },
    { id: '3', code: 'AG 11', description: 'AG 11', isActivate: true }
  ];
  const initialState: Partial<RootState> = {
    reference: {
      dropdownList: [],
      gridList: {
        loading: false,
        data: []
      },
      loading: false,
      modal: {
        modalType: '',
        open: false,
        selectedItem: {}
      },
      selectedDropdown: [],
      listCode: [],
      selectedReferenceCodes: ['AG']
    } as any
  };
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<GridItem referenceTypeCode="AG" />, {
      initialState
    });
  };

  it('should render title and have data', () => {
    const mockUpdateReferenceList = mockReferenceActions(
      'updateDragAndDropData'
    );

    const { baseElement } = renderWrapper({
      reference: {
        ...initialState.reference,
        data: {
          AG: {
            data: dataAG
          }
        },
        selectedReferenceCodes: ['AG', 'DC']
      } as any
    });

    const title = screen.getByText(I18N_REFERENCE.REFERENCE_SECTION_TITLE);

    expect(title).toBeInTheDocument();

    const table = screen.getByTestId('table');

    expect(table).toBeInTheDocument();

    const queryIconMinus = queryByClass(baseElement, /icon icon-minus/);

    userEvent.click(queryIconMinus!);

    userEvent.click(screen.getByTestId('Grid_onDataChange'));

    expect(mockUpdateReferenceList).toBeCalledWith({
      referenceTypeCode: 'AG',
      allUpdatedData: dataAG
    });
  });

  it('should render title and empty data', () => {
    renderWrapper(initialState);

    const emptyState = screen.getByText('txt_no_data');

    expect(emptyState).toBeInTheDocument();
  });

  it('should render title and empty data', () => {
    jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(false);

    const mockToggleModal = mockReferenceActions('openReferenceModal');

    const mockSetErrorMessage = mockReferenceActions('setErrorMessage');

    renderWrapper(initialState);

    const title = screen.getByText(I18N_REFERENCE.REFERENCE_SECTION_TITLE);

    expect(title).toBeInTheDocument();

    const emptyState = screen.getByText('txt_no_data');

    expect(emptyState).toBeInTheDocument();

    const addCodeButton = screen.getByRole('button', { name: 'txt_add_code' });

    userEvent.click(addCodeButton);

    expect(mockToggleModal).toBeCalledWith({
      modalType: 'add',
      selectedItem: {},
      referenceTypeCode: 'AG'
    });

    expect(mockSetErrorMessage).toBeCalledWith({ message: '' });
  });

  it('should render title and error api', () => {
    const mockGetList = mockReferenceActions('getListReference');

    renderWrapper({
      reference: {
        ...initialState.reference,
        data: {
          AG: {
            data: dataAG,
            error: 'text_error'
          }
        }
      } as any
    });

    userEvent.click(screen.getByText('txt_reload'));
    expect(mockGetList).toBeCalledWith({ referenceTypeCodes: 'AG' });
  });
});

describe('renderActions', () => {
  it('render UI', () => {
    renderMockStore(
      renderActions('AG')({
        id: '1',
        code: 'AG 1',
        description: 'AG 1',
        isActivate: true
      }),
      {}
    );

    const deleteText = screen.getByText('txt_delete');

    expect(deleteText).toBeInTheDocument();

    const deactiveText = screen.getByText('txt_deactivate');

    expect(deactiveText).toBeInTheDocument();

    const editText = screen.getByText('txt_edit');

    expect(editText).toBeInTheDocument();
  });
});
