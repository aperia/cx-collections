import React, { useEffect, useRef } from 'react';

//components
import {
  Button,
  Modal,
  InlineMessage,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

//view
import { View, ExtraFieldProps } from 'app/_libraries/_dof/core';

//libs
import { isEmpty } from 'app/_libraries/_dls/lodash';

//forms
import { Field } from 'redux-form';

//redux
import { useSelector, useDispatch, batch } from 'react-redux';

//actions
import { referenceActions } from './_redux/reducers';

//hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSelectFormValue } from 'app/hooks/useSelectFormValue';
import { isValid } from 'redux-form';

//selector
import {
  selectedItemModal,
  selectedModalErrorMsg,
  selectedModalIsLoading,
  selectedModalType,
  selectedReferenceTypeCode
} from './_redux/selectors';

//constants
import { I18N_REFERENCE, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { ReferenceItem } from './types';
import ModalBodyWithErrorClientConfig from '../ApiErrorSection/ModalBodyWithErrorClientConfig';
import { useIsDirtyForm } from 'app/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { referenceName } from './helper';

const DOF_REFERENCE_DESCRIPTOR = 'ReferenceClientConfigView';
const DOF_CUSTOM_REFERENCE_DESCRIPTOR = 'CustomReferenceClientConfigView';

const ReferencesModal: React.FC<{}> = () => {
  const testId = 'clientConfig_references_modal';

  const dispatch = useDispatch();
  const modalType = useSelector(selectedModalType);
  const selectedItem = useSelector(selectedItemModal);
  const referenceTypeCode = useSelector(selectedReferenceTypeCode);

  const [initialData] = React.useState({
    code: selectedItem?.code || '',
    description: selectedItem?.description || ''
  });

  const modalViewRef = useRef<any>();
  const DOF_REFERENCE_NAME = `${DOF_REFERENCE_DESCRIPTOR}_${referenceTypeCode}`;

  const formValid = useSelector(isValid(DOF_REFERENCE_NAME));

  const errorMsg = useSelector(selectedModalErrorMsg);

  const values = useSelectFormValue(DOF_REFERENCE_NAME) as ReferenceItem;

  const loading = useSelector(selectedModalIsLoading);

  const isDirtyForm = useIsDirtyForm([DOF_REFERENCE_NAME]);

  const { t } = useTranslation();

  const handleSubmit = React.useCallback(() => {
    if (modalType === 'add') {
      batch(() => {
        dispatch(referenceActions.setErrorMessage({ message: '' }));
        dispatch(
          referenceActions.addReference({
            referenceTypeCode,
            data: values,
            label: t(referenceName(referenceTypeCode))
          })
        );
      });
    }

    if (modalType === 'delete') {
      batch(() => {
        dispatch(
          referenceActions.deleteReference({
            referenceTypeCode,
            data: selectedItem,
            label: t(referenceName(referenceTypeCode))
          })
        );
      });
    }

    if (modalType === 'activate') {
      batch(() => {
        dispatch(
          referenceActions.activateReference({
            referenceTypeCode,
            data: selectedItem,
            label: t(referenceName(referenceTypeCode))
          })
        );
      });
    }

    if (modalType === 'edit') {
      const newValues = {
        ...selectedItem,
        description: values?.description?.trim()
      };

      batch(() => {
        dispatch(
          referenceActions.editReference({
            referenceTypeCode,
            data: newValues,
            previousValue: selectedItem,
            label: t(referenceName(referenceTypeCode))
          })
        );
      });
    }
  }, [modalType, dispatch, referenceTypeCode, values, t, selectedItem]);

  const handleCloseModal = () => {
    dispatch(referenceActions.closeReferenceModal({}));
  };

  const setViewProps = () => {
    const referenceFields: Field<ExtraFieldProps> =
      modalViewRef?.current?.props?.onFind('code');

    if (modalType === 'add') {
      referenceFields?.props?.props?.setRequired(true);
      referenceFields?.props?.props?.setReadOnly(false);
    }

    if (errorMsg?.message && !isEmpty(values?.code)) {
      referenceFields?.props?.props?.setOthers((prev: MagicKeyValue) => {
        referenceFields.props?.props?.setOthers({
          ...prev,
          options: { ...prev.options, isError: true }
        });
      });
    }
  };

  React.useEffect(() => {
    modalViewRef && setViewProps();
  });

  useEffect(() => {
    return () => {
      dispatch(referenceActions.setErrorMessage({ message: '' }));
    };
  }, [dispatch]);

  const dynamicProps: MagicKeyValue = {
    add: {
      title: t(I18N_REFERENCE.ADD_CODE),
      variant: 'primary',
      footer: t(I18N_REFERENCE.SUBMIT)
    },
    edit: {
      title: t(I18N_REFERENCE.EDIT_CODE),
      variant: 'primary',
      footer: t(I18N_REFERENCE.SAVE)
    },
    delete: {
      title: t(I18N_REFERENCE.DELETE_CODE),
      footer: t(I18N_REFERENCE.DELETE),
      variant: 'danger',
      content: t(I18N_REFERENCE.DELETE_MODAL_CONTENT)
    },
    activate: selectedItem?.isActivate
      ? {
          title: t(I18N_REFERENCE.DEACTIVATE),
          footer: t(I18N_COMMON_TEXT.DEACTIVATE),
          variant: 'danger',
          content: t(I18N_REFERENCE.DEACTIVATE_MODAL_CONTENT)
        }
      : {
          title: t(I18N_REFERENCE.ACTIVATE),
          footer: t(I18N_COMMON_TEXT.ACTIVATE),
          variant: 'primary',
          content: t(I18N_REFERENCE.ACTIVATE_MODAL_CONTENT)
        }
  };

  const isDeleteOrActivateContent =
    modalType === 'delete' || modalType === 'activate';

  return (
    <>
      <Modal
        xs={isDeleteOrActivateContent}
        sm={!isDeleteOrActivateContent}
        show={true}
        loading={loading}
        dataTestId={testId}
      >
        <ModalHeader
          border
          closeButton
          onHide={handleCloseModal}
          dataTestId={`${testId}_header`}
        >
          <ModalTitle dataTestId={`${testId}_title`}>
            {dynamicProps[modalType]?.title}
          </ModalTitle>
        </ModalHeader>
        <ModalBodyWithErrorClientConfig
          apiErrorClassName="mb-24"
          dataTestId={`${testId}_body`}
        >
          {errorMsg?.message && (
            <InlineMessage
              className="mb-24"
              variant="danger"
              dataTestId={`${testId}_errorMessage`}
            >
              {t(errorMsg.message, errorMsg.msgVariables)}
            </InlineMessage>
          )}
          {isDeleteOrActivateContent ? (
            <>
              <div
                className="mb-6"
                data-testid={genAmtId(testId, 'content', '')}
              >
                {dynamicProps[modalType].content}
              </div>
              <div data-testid={genAmtId(testId, 'referenceCode', '')}>
                {t(I18N_REFERENCE.CODE)}: <strong>{selectedItem?.code}</strong>{' '}
                -{' '}
                <strong className="text-break">
                  {selectedItem?.description}
                </strong>
              </div>
            </>
          ) : referenceTypeCode === 'MTQ' ? (
            <View
              id={DOF_REFERENCE_NAME}
              formKey={DOF_REFERENCE_NAME}
              descriptor={DOF_CUSTOM_REFERENCE_DESCRIPTOR}
              value={initialData}
              ref={modalViewRef}
            />
          ) : (
            <View
              id={DOF_REFERENCE_NAME}
              formKey={DOF_REFERENCE_NAME}
              descriptor={DOF_REFERENCE_DESCRIPTOR}
              value={initialData}
              ref={modalViewRef}
            />
          )}
        </ModalBodyWithErrorClientConfig>
        <ModalFooter dataTestId={`${testId}_footer`}>
          <div className="d-flex align-items-right justify-content-end">
            <Button
              onClick={handleCloseModal}
              variant="secondary"
              dataTestId={`${testId}_cancelBtn`}
            >
              {t(I18N_REFERENCE.CANCEL)}
            </Button>
            <Button
              disabled={
                !isDeleteOrActivateContent && (!formValid || !isDirtyForm)
              }
              onClick={handleSubmit}
              variant={dynamicProps[modalType]?.variant}
              dataTestId={`${testId}_okBtn`}
            >
              {dynamicProps[modalType]?.footer}
            </Button>
          </div>
        </ModalFooter>
      </Modal>
    </>
  );
};

export default ReferencesModal;
