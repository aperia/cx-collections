import React, { useEffect, useMemo, useState } from 'react';

//components
import MultiSelect from 'app/_libraries/_dls/components/MultiSelect'; //   MultiSelectProps

//utils
import isEmpty from 'lodash.isempty';

//redux
import { batch, useDispatch, useSelector } from 'react-redux';

//actions
import { referenceActions } from './_redux/reducers';

//hooks
import { useListenChangingTabEvent } from '../hooks/useListenChangingTabEvent';

// helper
import {
  dispatchCloseModalEvent,
  dispatchControlTabChangeEvent
} from '../helpers';

//selector
import { selectDropdownListData } from './_redux/selectors';

//types
import { DropdownBaseChangeEvent } from 'app/_libraries/_dls/components';
import { I18N_REFERENCE } from 'app/constants/i18n';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { ReferenceTypeCode } from './types';
import { useListenClosingModal } from '../hooks/useListenClosingModal';
import { getGroupFormatInputText } from 'app/helpers/formatMultiSelectText';

const ReferencesDropDown: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [selectedRefCode, setSelectedRefCode] = useState([]);
  const testId = 'clientConfig_references';

  const refData = useSelector(selectDropdownListData);
  const dropdownListData = useMemo(
    () =>
      refData.map(item => ({
        ...item,
        description: t(item.description)
      })),
    [refData, t]
  );

  const dropdownListItem: any = React.useMemo(() => {
    return (
      !isEmpty(dropdownListData) &&
      dropdownListData?.map(item => (
        <MultiSelect.Item
          key={item.value}
          label={item.description}
          value={item}
          variant="checkbox"
        />
      ))
    );
  }, [dropdownListData]);

  const handleOnBlur = (event: any) => {
    const items = event?.value?.map((item: RefDataValue) => item?.value);
    const listCode: ReferenceTypeCode[] = Array.from(new Set(items));
    dispatch(
      referenceActions.getListReference({ referenceTypeCodes: listCode })
    );
    dispatch(referenceActions.updateSelectedReferenceCodes(listCode));
  };

  const handleChange = (event: DropdownBaseChangeEvent) => {
    setSelectedRefCode(event.target.value);
  };

  useListenChangingTabEvent(() => {
    batch(() => {
      dispatch(referenceActions.resetReference());
      dispatchControlTabChangeEvent();
    });
  });

  useEffect(() => {
    // get when render
    dispatch(referenceActions.getReferenceDropdownList());
    return () => {
      dispatch(referenceActions.resetReference());
    };
  }, [dispatch]);

  useListenClosingModal(() => {
    batch(() => {
      dispatch(referenceActions.resetReference());
      dispatchCloseModalEvent();
    });
  });

  return (
    <div
      className="w-320px mt-md-16 mt-lg-0"
      data-testid="referenceListWrapper"
    >
      <MultiSelect
        name="reference"
        onBlur={handleOnBlur}
        value={selectedRefCode}
        textField="description"
        onChange={handleChange}
        label={t(I18N_REFERENCE.FIELDS)}
        variant="group"
        checkAll
        checkAllLabel={t('txt_all')}
        dataTestId={`${testId}_fieldsDropdown`}
        noResult={t('txt_no_results_found')}
        searchBarPlaceholder={t('txt_type_to_search')}
        groupFormatInputText={(...args) =>
          t(getGroupFormatInputText(...args), {
            selected: args[0],
            items: args[1]
          })
        }
      >
        {dropdownListItem}
      </MultiSelect>
    </div>
  );
};

export default ReferencesDropDown;
