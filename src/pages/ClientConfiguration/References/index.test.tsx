import React from 'react'; // ReactNode // ReactElement,

//mock test utils
import {
  // mockActionCreator,
  queryByClass,
  renderMockStore
} from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';

//components
import References from '.';

//actions
// import { referenceActions } from './_redux/reducers';

import { I18N_REFERENCE } from 'app/constants/i18n';

// import userEvent from '@testing-library/user-event';
import { PopoverProps } from 'app/_libraries/_dls/components';

jest.mock('./ReferencesModal', () => () => <div>Mocked ReferencesModal</div>);

jest.mock('app/_libraries/_dls/components', () => {
  const { Popover } = jest.requireActual('app/_libraries/_dls/components');
  return {
    ...jest.requireActual('app/_libraries/_dls/components'),
    Popover: (props: PopoverProps) => (
      <>
        <div
          data-testid="onVisibilityChange"
          onClick={() => props.onVisibilityChange!(true)}
        />
        <Popover opened {...props} />
      </>
    )
  };
});

HTMLCanvasElement.prototype.getContext = jest.fn();

// const mockReferenceActions = mockActionCreator(referenceActions);
describe('render actions buttons ', () => {
  const initialState = {
    reference: {
      dropdownList: [
        { id: '1', code: 'AG', value: 'Adjustment Group', reference: '' }
      ],
      gridList: {
        loading: false,
        error: '',
        data: [
          {
            id: '1',
            code: 'AG',
            data: [
              { id: '1', code: 'A', description: 'AG 1', isActivate: true },
              { id: '2', code: 'B', description: 'AG 2', isActivate: false }
            ]
          }
        ]
      },
      listCode: [],
      loading: false,
      modal: {
        modalType: 'activate' as any,
        open: false,
        selectedItem: {},
        referenceTypeCode: 'AG' as any
      },
      selectedDropdown: [{ id: '1', code: 'AG', value: 'Adjustment Group' }],
      selectedReferenceCodes: ['AG', 'DC', 'DR', 'MTQ'] as any,
      dropdownListData: [],
      data: {
        AG: {},
        DC: {},
        DR: {},
        MTQ: {}
      }
    }
  };
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<References />, { initialState });
  };

  it('should render list grid with data ', () => {
    renderWrapper(initialState);

    const description = screen.getByText(I18N_REFERENCE.REFERENCES_DESCRIPTION);

    const name = screen.getByText(I18N_REFERENCE.REFERENCES);

    expect(description).toBeInTheDocument();

    expect(name).toBeInTheDocument();
  });

  // it('should render list grid with data and trigger modal ', () => {
  //   const mockToggleModal = mockReferenceActions('toggleModal');

  //   const mockSetErrorMessage = mockReferenceActions('setErrorMessage');

  //   const { baseElement } = renderWrapper({
  //     ...initialState,
  //     reference: {
  //       ...initialState.reference!,
  //       modal: {
  //         modalType: 'add',
  //         open: true,
  //         selectedItem: {}
  //       }
  //     }
  //   });

  //   const queryIconInfo = queryByClass(baseElement, /icon icon-information/);

  //   userEvent.click(queryIconInfo!);

  //   const toggleModal = screen.getByRole('button', { name: 'txt_add_code' });

  //   userEvent.click(toggleModal);

  //   expect(mockToggleModal).toBeCalledWith({
  //     modalType: 'add',
  //     selectedItem: {},
  //     referenceGroupId: 'AG'
  //   });

  //   expect(mockSetErrorMessage).toBeCalledWith({ message: '' });
  // });

  describe('Actions', () => {
    it('onVisibilityChange', () => {
      const { baseElement } = renderWrapper({
        ...initialState,
        reference: {
          ...initialState.reference!,
          modal: {
            modalType: 'add',
            open: true,
            selectedItem: {},
            referenceTypeCode: 'AG'
          }
        }
      });

      // trigger onVisibilityChange(true)
      screen.getByTestId('onVisibilityChange').click();

      expect(queryByClass(baseElement, /active/)).toBeInTheDocument();
    });
  });
});
