import React, { useLayoutEffect } from 'react';

//selector
import {
  selectDropdownListData,
  selectedReferenceCodes
} from './_redux/selectors';

//component
import GridItem from './GridItem';
// import { FailedApiReload } from 'app/components';

//redux
import { useSelector } from 'react-redux';
import { ReferenceTypeCode } from './types';

const GridList: React.FC = () => {
  const testId = 'clientConfig_references_grid';

  const selectedDropDown = useSelector(selectedReferenceCodes);

  const dropdownListData = useSelector(selectDropdownListData);

  const divRef = React.useRef<HTMLDivElement | null>(null);

  const selectedReferences = dropdownListData.filter(ref =>
    selectedDropDown.includes(ref.value as ReferenceTypeCode)
  );

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = 'auto');
  }, []);

  return (
    <div ref={divRef}>
      {selectedReferences?.length > 0 &&
        selectedReferences?.map(refValue => (
          <div key={refValue.value}>
            <GridItem
              referenceTypeCode={refValue.value as ReferenceTypeCode}
              dataTestId={`${refValue.value}_${testId}`}
            />
          </div>
        ))}
    </div>
  );
};

export default GridList;
