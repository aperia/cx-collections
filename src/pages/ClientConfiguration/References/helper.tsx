import { REFERENCE_DROPDOWN_ITEM_NAME } from './constants';

export const referenceName = (name: string) => {
  switch (name) {
    case 'AG':
      return REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP;
    case 'DR':
      return REFERENCE_DROPDOWN_ITEM_NAME.DELINQUENCY_REASON;
    case 'DC':
      return REFERENCE_DROPDOWN_ITEM_NAME.DECEASED_CONTACT;
    case 'MTQ':
      return REFERENCE_DROPDOWN_ITEM_NAME.MOVE_TO_QUEUE;
    default:
      return '';
  }
};

export const dynamicMsg = (name: any) => {
  const newString = name.toLowerCase();
  return newString.charAt(0).toUpperCase() + newString.slice(1);
};
