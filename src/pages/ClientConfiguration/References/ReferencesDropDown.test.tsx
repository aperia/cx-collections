import React from 'react';

//mock test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';

//components
import ReferencesDropDown from './ReferencesDropDown';

//helpers
import * as clientConfigHelper from 'pages/ClientConfiguration/helpers';

//actions
import { referenceActions } from './_redux/reducers';

//constant
import { SECTION_TAB_EVENT } from '../constants';
import userEvent from '@testing-library/user-event';

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockReferenceActions = mockActionCreator(referenceActions);

const initialState: Partial<RootState> = {
  reference: {
    data: {
      AG: {
        data: [{ id: '1', code: 'AG', description: 'Adjustment Group' }]
      },
      DC: {
        data: [{ id: '2', code: 'DC', description: 'Deceased Contact' }]
      },
      DR: {
        data: [{ id: '3', code: 'DR', description: 'Delinquency Reason' }]
      },
      MTQ: {
        data: [{ id: '4', code: 'MTQ', description: 'Move to Queue' }]
      }
    },
    gridList: {
      loading: false,
      error: '',
      data: []
    },
    listCode: [],
    loading: false,
    modal: {
      modalType: '',
      open: false,
      selectedItem: {}
    },
    dropdownListData: [
      { value: 'AG', description: 'Adjustment Group' },
      { value: 'DC', description: 'Deceased Contact' }
    ]
  } as any
};

describe('render actions buttons ', () => {
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<ReferencesDropDown />, { initialState });
  };

  it('should render dropdown and action with data', () => {
    mockReferenceActions('getReferenceDropdownList');

    const blurAction = mockReferenceActions('getListReference');

    const changeAction = mockReferenceActions('updateSelectedReferenceCodes');

    const { container } = renderWrapper(initialState);

    const multiSelect = container.querySelector('.text-field-container')!;

    userEvent.click(multiSelect);

    userEvent.click(screen.getByText('Adjustment Group'));
    userEvent.click(screen.getByText('Deceased Contact'));

    userEvent.click(multiSelect);
    userEvent.click(multiSelect);

    expect(blurAction).toBeCalled();
    expect(changeAction).toBeCalled();
  });

  describe('useListenChangingTabEvent', () => {
    let spy: jest.SpyInstance;

    afterEach(() => {
      spy.mockReset();
      spy.mockRestore();
    });

    const mockDispatchEventChangeTab = jest.fn();

    it('should trigger useListenChangingTabEvent', () => {
      mockReferenceActions('getReferenceDropdownList');
      const mockResetReferenceActions = mockReferenceActions('resetReference');

      spy = jest
        .spyOn(clientConfigHelper, 'dispatchControlTabChangeEvent')
        .mockImplementation(mockDispatchEventChangeTab);

      renderWrapper(initialState);

      const event = new CustomEvent<string>(SECTION_TAB_EVENT.CHANGING_TAB, {
        bubbles: true,
        cancelable: true
      });

      window.dispatchEvent(event);

      expect(mockDispatchEventChangeTab).toBeCalled();
      expect(mockResetReferenceActions).toBeCalledWith();
    });
  });

  describe('useListenClosingModal', () => {
    let spy: jest.SpyInstance;

    afterEach(() => {
      spy.mockReset();
      spy.mockRestore();
    });

    const mockDispatchCloseModalEvent = jest.fn();
    it('should trigger useListenClosingModal', () => {
      mockReferenceActions('getReferenceDropdownList');
      const mockResetReferenceActions = mockReferenceActions('resetReference');

      spy = jest
        .spyOn(clientConfigHelper, 'dispatchCloseModalEvent')
        .mockImplementation(mockDispatchCloseModalEvent);

      renderWrapper(initialState);

      const event = new CustomEvent<string>(SECTION_TAB_EVENT.CLOSING_MODAL, {
        bubbles: true,
        cancelable: true
      });

      window.dispatchEvent(event);

      expect(mockDispatchCloseModalEvent).toBeCalled();
      expect(mockResetReferenceActions).toBeCalledWith();
    });
  });
});
