export type ReferenceTypeCode = 'AG' | 'DC' | 'DR' | 'MTQ';

// export interface ReferenceListCodes {}
export interface ReferenceState {
  dropdownListData: RefDataValue[];
  selectedReferenceCodes: ReferenceTypeCode[];
  modal: ReferenceModal;
  data: Record<ReferenceTypeCode, ReferenceGridList>;
  loading?: boolean;
  error?: boolean;
}

export interface ReferenceItem {
  id?: string;
  code?: string;
  description?: string;
  isActivate?: boolean;
}

export interface ReferenceGridList {
  data?: ReferenceItem[];
  error?: boolean;
}

export interface IReferenceRequest {
  id: ReferenceTypeCode;
  data: ReferenceItem;
}

export interface IReferenceListRequest {
  id?: string;
  data?: ReferenceItem[];
}

export interface IReferenceRefData {
  id: string;
  code: ReferenceTypeCode;
  value: string;
}

export type ModalType = 'add' | 'delete' | 'activate' | 'edit';
export interface ReferenceModalError {
  message?: string;
  msgVariables?: Record<string, string | number>;
}

export interface OpenModalPayloadAction {
  modalType: ModalType;
  selectedItem?: ReferenceItem;
  referenceTypeCode: ReferenceTypeCode;
}

export interface ActivateReferenceArgs extends IReferenceRequest {}

export interface ReferenceModal extends OpenModalPayloadAction {
  loading?: boolean;
  error?: ReferenceModalError;
  open?: boolean;
}

// Get Adjustment Group Code Type

export interface GetAdjustmentGroupCodesPayload {
  data: ReferenceItem[];
}
export interface GetAdjustmentGroupCodesArgs {
  referenceTypeCode: ReferenceTypeCode;
}

// -------------COMMON--------------------------------------
export interface CommonUpdateReferenceArgs {
  referenceTypeCode: ReferenceTypeCode;
  data: ReferenceItem;
}

export interface CommonAddReferenceArgs extends CommonUpdateReferenceArgs {
  label: string;
}
export interface CommonDeleteReferenceArgs extends CommonUpdateReferenceArgs {
  label: string;
}
export interface CommonEditReferenceArgs extends CommonUpdateReferenceArgs {
  previousValue: ReferenceItem;
  label: string;
}
export interface CommonActivateReferenceArgs extends CommonUpdateReferenceArgs {
  label: string;
}

export interface CommonGetListReferenceArgs {
  referenceTypeCodes: ReferenceTypeCode[] | ReferenceTypeCode;
}

export interface CommonUpdateDragAndDropArgs {
  referenceTypeCode: ReferenceTypeCode;
  allUpdatedData: ReferenceItem[];
}

// ---------------DROPDOWN LIST TYPE -------------------------------------
export interface GetReferencesRefDataPayload {
  data: RefDataValue[];
}

// -----------------ADJUSTMENT GROUP CODES------------------------------------------
export interface DeleteAdjustmentGroupsArgs extends CommonUpdateReferenceArgs {
  actionType: 'delete';
}
export interface UpdateAdjustmentGroupsArgs extends CommonUpdateReferenceArgs {
  actionType: ModalType | 'updateAllData';
  previousValue?: ReferenceItem;
  allUpdatedData?: ReferenceItem[];
}

// -----------------DECEASED DELINQUENCY REASON MOVE TO QUEUE----MOCK API----------------------------------

export interface GetListReferenceMock {
  clientInfoId: string;
  code: ReferenceTypeCode;
}

export interface GetOtherListReferencePayload {
  data: ReferenceItem[];
}

export interface GetOtherListReferenceArgs {
  referenceTypeCode: ReferenceTypeCode;
}
export type ActionType = ModalType | 'updateAllData';
