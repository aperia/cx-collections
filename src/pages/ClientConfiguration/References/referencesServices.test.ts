// services

import { referenceServices } from './referencesServices';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi } from 'app/test-utils/mocks/mockProperty';

describe('referenceServices', () => {
  describe('getReferenceDropdownList', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      referenceServices.getReferenceDropdownList();

      expect(mockService).toBeCalledWith('refData/referenceSessions.json');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      referenceServices.getReferenceDropdownList();

      expect(mockService).toBeCalledWith('refData/referenceSessions.json');
    });
  });
});
