import React, { useState, useMemo, useCallback, useEffect } from 'react';

//components
import { Grid, ColumnType, Pagination } from 'app/_libraries/_dls/components';
import Actions from './Actions';
import SectionControl from 'app/components/SectionControl';
import { FailedApiReload, NoDataGrid } from 'app/components';
//constants
import { I18N_COMMON_TEXT, I18N_REFERENCE } from 'app/constants/i18n';

//redux
import { useDispatch, batch, useSelector } from 'react-redux';

//hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { usePagination } from 'app/hooks';

//actions
import { referenceActions } from './_redux/reducers';

//helper
import { referenceName } from './helper';

//types
import { ReferenceItem, ReferenceTypeCode } from './types';
import {
  selectDataByReferenceCode,
  selectedReferenceCodes,
  selectErrorByReferenceCode
} from './_redux/selectors';
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface GridItemProps extends DLSId {
  referenceTypeCode: ReferenceTypeCode;
}

export const renderActions =
  (
    referenceTypeCode: ReferenceTypeCode,
    status: boolean,
    dataTestId?: string
  ) =>
  (dataAction: ReferenceItem, idx: number) =>
    (
      <Actions
        status={status}
        referenceItem={dataAction}
        referenceTypeCode={referenceTypeCode}
        dataTestId={genAmtId(`${idx}_${dataTestId}`, 'action', '')}
      />
    );

const GridItem: React.FC<GridItemProps> = ({
  referenceTypeCode,
  dataTestId
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const [isExpand, setIsExpand] = useState<boolean>(true);

  const allData = useSelector<RootState, ReferenceItem[]>((state: RootState) =>
    selectDataByReferenceCode(state, referenceTypeCode)
  );
  const isError = useSelector<RootState, boolean>((state: RootState) =>
    selectErrorByReferenceCode(state, referenceTypeCode)
  );
  const selectedCodes = useSelector(selectedReferenceCodes);

  const hideCollapseGrid = selectedCodes?.length <= 1;

  const status = useCheckClientConfigStatus();

  const handleAddCode = () => {
    batch(() => {
      dispatch(
        referenceActions.openReferenceModal({
          modalType: 'add',
          selectedItem: {},
          referenceTypeCode
        })
      );
      dispatch(referenceActions.setErrorMessage({ message: '' }));
    });
  };

  const handleToggleExpand = () => setIsExpand(prev => !prev);

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(allData);

  // Drap and Drop
  const handleOnDataChange = (draggedGridData: ReferenceItem[]) => {
    const allUpdatedData = [...allData];
    allUpdatedData.splice(
      (currentPage - 1) * currentPageSize,
      draggedGridData.length,
      ...draggedGridData
    );
    dispatch(
      referenceActions.updateDragAndDropData({
        referenceTypeCode,
        allUpdatedData
      })
    );
  };

  const handleReload = useCallback(() => {
    dispatch(
      referenceActions.getListReference({
        referenceTypeCodes: referenceTypeCode
      })
    );
  }, [dispatch, referenceTypeCode]);

  useEffect(() => {
    if (selectedCodes.length === 1) setIsExpand(true);
  }, [selectedCodes.length]);

  const isShowPagination = total > pageSize[0];

  const REFERENCE_COLUMNS: ColumnType[] = useMemo(
    () => [
      {
        id: 'code',
        Header: t(I18N_REFERENCE.CODE),
        width: 142,
        accessor: 'code'
      },
      {
        id: 'description',
        Header: t(I18N_REFERENCE.DESCRIPTION),
        accessor: 'description',
        width: 572,
        autoWidth: true
      },
      {
        id: 'action',
        Header: (
          <div className="text-center">{t(I18N_COMMON_TEXT.ACTIONS)}</div>
        ),
        isFixedRight: true,
        width: 226,
        cellBodyProps: { className: 'text-right td-sm' },
        accessor: renderActions(referenceTypeCode, status)
      }
    ],
    [referenceTypeCode, t, status]
  );

  const renderEmptyGrid = () => {
    return (
      <NoDataGrid
        text={I18N_REFERENCE.NO_DATA}
        dataTestId={genAmtId(dataTestId, 'noData', '')}
      />
    );
  };

  const renderGrid = () => {
    return (
      <Grid
        columns={REFERENCE_COLUMNS}
        data={gridData}
        variant={{
          id: `grid_dnd_${referenceTypeCode}`,
          type: 'dnd'
        }}
        dataItemKey={'id'}
        dnd
        dndReadOnly={status}
        onDataChange={handleOnDataChange}
        dataTestId={genAmtId(dataTestId, 'grid', '')}
      />
    );
  };

  const renderApiError = useCallback(() => {
    return (
      <div>
        <FailedApiReload
          id="client-config-code-to-text-failed"
          dataTestId={genAmtId(dataTestId, 'failedApi', '')}
          className="mt-80 mb-40"
          onReload={handleReload}
        />
      </div>
    );
  }, [dataTestId, handleReload]);

  return (
    <>
      <SectionControl
        title={t(I18N_REFERENCE.REFERENCE_SECTION_TITLE, {
          name: t(referenceName(referenceTypeCode))
        })}
        classNames="mt-24"
        sectionKey={'key'}
        onToggle={handleToggleExpand}
        isExpand={isExpand}
        isAdd={{
          enableSectionButton: hideCollapseGrid,
          buttonLabel: t(I18N_REFERENCE.ADD_CODE),
          enableToolTip: false,
          disableButton: status,
          handleAdd: handleAddCode
        }}
        dataTestId={dataTestId}
      >
        <div className="mt-16">
          {isError
            ? renderApiError()
            : !allData.length
            ? renderEmptyGrid()
            : renderGrid()}
          {isShowPagination && (
            <div className="pt-16">
              <Pagination
                totalItem={total}
                pageSize={pageSize}
                pageNumber={currentPage}
                pageSizeValue={currentPageSize}
                compact
                onChangePage={onPageChange}
                onChangePageSize={onPageSizeChange}
                dataTestId={genAmtId(dataTestId, 'pagination', '')}
              />
            </div>
          )}
        </div>
      </SectionControl>
    </>
  );
};

export default GridItem;
