import React from 'react';

// components
import { Button } from 'app/_libraries/_dls/components';

//utils
import { useDispatch } from 'react-redux';

//redux
import { useTranslation } from 'app/_libraries/_dls/hooks';

//reducers
import { referenceActions } from './_redux/reducers';

//constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { ReferenceItem, ReferenceTypeCode } from './types';
import { genAmtId } from 'app/_libraries/_dls/utils';

//types
export interface ActionsProps extends DLSId {
  referenceTypeCode: ReferenceTypeCode;
  referenceItem: ReferenceItem;
  status: boolean;
}

const Actions: React.FC<ActionsProps> = ({
  referenceTypeCode,
  referenceItem,
  status,
  dataTestId
}) => {
  const dispatch = useDispatch();

  const { t } = useTranslation();

  const handleEdit = () => {
    dispatch(
      referenceActions.openReferenceModal({
        modalType: 'edit',
        selectedItem: referenceItem,
        referenceTypeCode
      })
    );
  };

  const handleDelete = () => {
    dispatch(
      referenceActions.openReferenceModal({
        modalType: 'delete',
        selectedItem: referenceItem,
        referenceTypeCode
      })
    );
  };
  const handleDeActive = () => {
    dispatch(
      referenceActions.openReferenceModal({
        modalType: 'activate',
        selectedItem: referenceItem,
        referenceTypeCode
      })
    );
  };

  return (
    <>
      <Button
        disabled={status}
        variant={
          referenceItem?.isActivate ? 'outline-danger' : 'outline-primary'
        }
        size="sm"
        onClick={handleDeActive}
        dataTestId={genAmtId(dataTestId, 'activate-deactive-btn', '')}
      >
        {referenceItem?.isActivate
          ? t(I18N_COMMON_TEXT.DEACTIVATE)
          : t(I18N_COMMON_TEXT.ACTIVATE)}
      </Button>
      <Button
        disabled={status}
        variant="outline-danger"
        size="sm"
        onClick={handleDelete}
        dataTestId={genAmtId(dataTestId, 'delete-btn', '')}
      >
        {t(I18N_COMMON_TEXT.DELETE)}
      </Button>
      <Button
        disabled={status}
        variant="outline-primary"
        size="sm"
        onClick={handleEdit}
        dataTestId={genAmtId(dataTestId, 'edit-btn', '')}
      >
        {t(I18N_COMMON_TEXT.EDIT)}
      </Button>
    </>
  );
};

export default Actions;
