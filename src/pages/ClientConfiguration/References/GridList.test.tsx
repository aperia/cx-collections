import React from 'react';

//mock test utils
import { renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';

//components
import GridList from './GridList';

//actions
import { I18N_REFERENCE } from 'app/constants/i18n';

describe('render Grid List', () => {
  const initialState: Partial<RootState> = {
    reference: {
      dropdownList: [],
      data: {
        AG: {
          data: [
            { id: '1', code: 'A', description: 'AG 1', isActivate: true },
            { id: '2', code: 'B', description: 'AG 2', isActivate: false }
          ]
        },
        DC: {
          data: [
            { id: '1', code: 'A', description: 'DC 1', isActivate: true },
            { id: '2', code: 'B', description: 'DC 2', isActivate: false }
          ]
        }
      },
      listCode: [],
      loading: false,
      modal: {
        modalType: '',
        open: false,
        selectedItem: {}
      },
      dropdownListData: [
        { value: 'AG', description: 'Adjustment Group' },
        { value: 'DC', description: 'Deceased Contact' }
      ],
      selectedReferenceCodes: ['AG']
    } as any
  };
  const renderWrapper = (initialState: Partial<RootState>) => {
    return renderMockStore(<GridList />, { initialState });
  };

  it('should render list grid with data and loading false', () => {
    renderWrapper(initialState);

    expect(
      screen.queryByText(I18N_REFERENCE.REFERENCE_SECTION_TITLE)
    ).toBeInTheDocument();
  });
});
