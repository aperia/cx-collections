// types
import { ReferenceState } from '../types';

//reducers
import { referenceActions, reference } from './reducers';

describe('Test Client Configuration Code To Text Reducers', () => {
  const initialState: ReferenceState = {
    dropdownListData: [
      { value: 'AG', description: 'Adjustment Group' },
      { value: 'DC', description: 'Deceased Contact' }
    ],
    data: {
      AG: {
        data: [
          { id: '1', code: 'A', description: 'AG 1', isActivate: true },
          { id: '2', code: 'B', description: 'AG 2', isActivate: false }
        ]
      },
      DC: {
        data: [
          { id: '1', code: 'A', description: 'DC 1', isActivate: true },
          { id: '2', code: 'B', description: 'DC 2', isActivate: false }
        ]
      }
    } as any,
    loading: false,
    modal: {
      modalType: 'add',
      open: false,
      selectedItem: {}
    } as any,
    selectedReferenceCodes: ['AG', 'DC']
  };

  it('should openReferenceModal add', () => {
    const state = reference(
      initialState,
      referenceActions.openReferenceModal({
        modalType: 'add',
        selectedItem: {},
        referenceTypeCode: 'AG'
      })
    );
    expect(state?.modal?.modalType).toEqual('add');

    expect(state?.modal?.open).toEqual(true);
  });
  it('should openReferenceModal delete', () => {
    const state = reference(
      initialState,
      referenceActions.openReferenceModal({
        modalType: 'delete',
        referenceTypeCode: 'AG'
      })
    );
    expect(state?.modal?.modalType).toEqual('delete');

    expect(state?.modal?.open).toEqual(true);
  });
  it('should openReferenceModal edit', () => {
    const state = reference(
      initialState,
      referenceActions.openReferenceModal({
        modalType: 'edit',
        referenceTypeCode: 'AG',
        selectedItem: {
          id: '1',
          code: 'AG',
          description: 'Adjustment Group',
          isActivate: true
        }
      })
    );
    expect(state?.modal?.modalType).toEqual('edit');

    expect(state?.modal?.open).toEqual(true);

    expect(state?.modal?.selectedItem).toEqual({
      code: 'AG',
      description: 'Adjustment Group',
      id: '1',
      isActivate: true
    });

    expect(state?.modal?.referenceTypeCode).toEqual('AG');
  });

  it('should closeReferenceModal', () => {
    const state = reference(
      initialState,
      referenceActions.closeReferenceModal({})
    );

    expect(state?.modal?.open).toEqual(false);
  });

  it('should setErrorMessage empty', () => {
    const state = reference(initialState, referenceActions.setErrorMessage({}));
    expect(state.modal?.error).toEqual({});
  });

  it('should setErrorMessage to sth', () => {
    const state = reference(
      initialState,
      referenceActions.setErrorMessage({ message: 'TEST' })
    );
    expect(state.modal?.error).toEqual({ message: 'TEST' });
  });

  it('should updateSelectedReferenceCodes ', () => {
    const state = reference(
      initialState,
      referenceActions.updateSelectedReferenceCodes(['AG'])
    );

    expect(state.selectedReferenceCodes).toEqual(['AG']);
  });

  it('should resetReference ', () => {
    const state = reference(initialState, referenceActions.resetReference());

    expect(state.dropdownListData).toEqual([
      { description: 'Adjustment Group', value: 'AG' },
      { description: 'Deceased Contact', value: 'DC' }
    ]);

    expect(state.data).toEqual({ AG: {}, DC: {}, DR: {}, MTQ: {} });

    expect(state.selectedReferenceCodes).toEqual([]);
  });
});
