import { mockActionCreator } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { getListReference } from './commonGetListReference';
import * as getAdjustment from './adjustmentGroupCodes/getAdjustmentGroupCodes';
import * as getOtherReference from './otherReference/getOtherReference';

const spyGetAdjustment = mockActionCreator(getAdjustment);
const spyGetOtherReference = mockActionCreator(getOtherReference);

describe('getListReference', () => {
  it('success', async () => {
    const mockAction = spyGetAdjustment('getAdjustmentGroupCodes');

    const store = createStore(rootReducer, {});
    await getListReference({ referenceTypeCodes: 'AG' })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(mockAction).toBeCalledWith({ referenceTypeCode: 'AG' });
  });

  it('success with multi references', async () => {
    const mockGetAdjustmentGroupCodes = spyGetAdjustment(
      'getAdjustmentGroupCodes'
    );
    const mockGetOtherMockReferenceAPI = spyGetOtherReference(
      'getOtherMockReferenceAPI'
    );

    const store = createStore(rootReducer, {});
    await getListReference({ referenceTypeCodes: ['AG', 'DC'] })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(mockGetAdjustmentGroupCodes).toBeCalledWith({
      referenceTypeCode: 'AG'
    });
    expect(mockGetOtherMockReferenceAPI).toBeCalledWith({
      referenceTypeCode: 'DC'
    });
  });
});
