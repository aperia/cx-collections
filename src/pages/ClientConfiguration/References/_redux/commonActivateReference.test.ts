import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator
} from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { activateReference } from './commonActivateReference';
import * as updateAdjustment from './adjustmentGroupCodes/updateAdjustmentGroupCodes';
import * as updateOtherReference from './otherReference/updateOtherReference';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { referenceActions } from './reducers';
import { I18N_REFERENCE } from 'app/constants/i18n';
import { REFERENCE_DROPDOWN_ITEM_NAME } from '../constants';

const spyUpdateAdjustment = mockActionCreator(updateAdjustment);
const spyUpdateOtherReference = mockActionCreator(updateOtherReference);
const spyReferenceActions = mockActionCreator(referenceActions);
const spyActionsToast = mockActionCreator(actionsToast);

beforeEach(() => {
  spyUpdateAdjustment('updateAdjustmentGroup');
  spyUpdateOtherReference('updateOtherReferenceMockApi');
});

describe('activateReference', () => {
  const msgVariables = {
    name:
      REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP.charAt(0).toUpperCase() +
      REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP.slice(1)
  };
  it('success with code AG', async () => {
    const mockAction = spyUpdateAdjustment('updateAdjustmentGroup');

    const store = createStore(rootReducer, {});
    await activateReference({
      data: {},
      referenceTypeCode: 'AG',
      label: REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP
    })(store.dispatch, store.getState, {});

    expect(mockAction).toBeCalledWith({
      data: {},
      referenceTypeCode: 'AG',
      actionType: 'activate',
      label: REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP
    });
  });

  it('success with code DC', async () => {
    const mockAction = spyUpdateOtherReference('updateOtherReferenceMockApi');

    const store = createStore(rootReducer, {});
    await activateReference({
      data: {},
      referenceTypeCode: 'DC',
      label: REFERENCE_DROPDOWN_ITEM_NAME.DECEASED_CONTACT
    })(store.dispatch, store.getState, {});

    expect(mockAction).toBeCalledWith({
      data: {},
      referenceTypeCode: 'DC',
      actionType: 'activate',
      label: REFERENCE_DROPDOWN_ITEM_NAME.DECEASED_CONTACT
    });
  });

  it('isFulfilled when disactivated', async () => {
    const mockAddToast = spyActionsToast('addToast');
    const mockCloseReferenceModal = spyReferenceActions('closeReferenceModal');
    const mockGetListReference = spyReferenceActions('getListReference');

    const store = createStore(rootReducer, {});
    await activateReference({
      data: { isActivate: false },
      referenceTypeCode: 'AG',
      label: REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP
    })(byPassFulfilled(activateReference.fulfilled.type), store.getState, {});

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_REFERENCE.REFERENCE_ACTIVATE_MESSAGE,
      msgVariables
    });
    expect(mockCloseReferenceModal).toBeCalledWith({});
    expect(mockGetListReference).toBeCalledWith({ referenceTypeCodes: 'AG' });
  });

  it('isFulfilled when disactivated', async () => {
    const mockAddToast = spyActionsToast('addToast');
    const mockCloseReferenceModal = spyReferenceActions('closeReferenceModal');
    const mockGetListReference = spyReferenceActions('getListReference');

    const store = createStore(rootReducer, {});
    await activateReference({
      data: { isActivate: true },
      referenceTypeCode: 'AG',
      label: REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP
    })(byPassFulfilled(activateReference.fulfilled.type), store.getState, {});

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_REFERENCE.REFERENCE_DEACTIVATE_MESSAGE,
      msgVariables
    });
    expect(mockCloseReferenceModal).toBeCalledWith({});
    expect(mockGetListReference).toBeCalledWith({ referenceTypeCodes: 'AG' });
  });

  it('isRejected when activated', async () => {
    const mockAddToast = spyActionsToast('addToast');

    const store = createStore(rootReducer, {});
    await activateReference({
      data: { isActivate: true },
      referenceTypeCode: 'AG',
      label: REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP
    })(byPassRejected(activateReference.rejected.type), store.getState, {});

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_REFERENCE.REFERENCE_DEACTIVATE_SERVER_ERROR,
      msgVariables
    });
  });

  it('isRejected when activated', async () => {
    const mockAddToast = spyActionsToast('addToast');

    const store = createStore(rootReducer, {});
    await activateReference({
      data: { isActivate: false },
      referenceTypeCode: 'AG',
      label: REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP
    })(byPassRejected(activateReference.rejected.type), store.getState, {});

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_REFERENCE.REFERENCE_ACTIVATE_SERVER_ERROR,
      msgVariables
    });
  });

  it('pending', () => {
    const store = createStore(rootReducer, {});

    const action = activateReference.pending(activateReference.pending.type, {
      data: {},
      referenceTypeCode: 'AG',
      label: REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP
    });
    const actual = rootReducer(store.getState(), action).reference.modal;

    expect(actual.loading).toBe(true);
  });

  it('fulfilled', () => {
    const store = createStore(rootReducer, {});

    const action = activateReference.fulfilled(null, {}, '');
    const actual = rootReducer(store.getState(), action).reference.modal;

    expect(actual.loading).toBe(false);
  });

  it('rejected', () => {
    const store = createStore(rootReducer, {});

    const action = activateReference.rejected(null, '', {
      data: {},
      referenceTypeCode: 'AG',
      label: REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP
    });
    const actual = rootReducer(store.getState(), action).reference.modal;

    expect(actual.loading).toBe(false);
  });
});
