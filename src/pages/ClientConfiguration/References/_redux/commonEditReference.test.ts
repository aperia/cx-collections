import { rootReducer } from 'storeConfig';

// types
import { createStore } from '@reduxjs/toolkit';

//reducers
import { referenceActions } from './reducers';
import * as updateOtherReferenceMock from './otherReference/updateOtherReference';
import * as updateAdjustmentGroupCodesMock from './adjustmentGroupCodes/updateAdjustmentGroupCodes';
import { mockActionCreator } from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

const spyUpdateAdjustmentGroup = mockActionCreator(
  updateAdjustmentGroupCodesMock
);
const spyUpdateOtherReference = mockActionCreator(updateOtherReferenceMock);
const mockActionToast = mockActionCreator(actionsToast);
const mockActionReference = mockActionCreator(referenceActions);
const mockActionChangeHistory = mockActionCreator(changeHistoryActions);

describe('commonEditReference', () => {
  // it('dispatch a add function rules mapping request');

  describe('when commonEditReference success', () => {
    let store: any;

    beforeEach(() => {
      store = createStore(rootReducer, {});
    });

    test('dispatches success with referenceTypeCode AG', async () => {
      const args = {
        referenceTypeCode: 'AG' as any,
        data: {
          id: '1',
          code: 'AG',
          description: 'Adjustment Group 1',
          isActivate: true
        },
        previousValue: {
          id: '1',
          code: 'AG',
          description: 'Adjustment Group',
          isActivate: true
        },
        label: 'mock label'
      };

      spyUpdateAdjustmentGroup('updateAdjustmentGroup', () => ({
        payload: undefined,
        type: 'clientConfigReference/commonEditReference/fulfilled',
        meta: {
          arg: {
            referenceTypeCode: args.referenceTypeCode,
            previousValue: args.previousValue,
            actionType: 'edit'
          },
          requestId: 'test request id',
          requestStatus: 'fulfilled'
        }
      }));

      const mockAddToast = mockActionToast('addToast');
      const mockCloseReferenceModal = mockActionReference(
        'closeReferenceModal'
      );
      const mockGetListReference = mockActionReference('getListReference');
      const mockSaveChangedClientConfig = mockActionChangeHistory(
        'saveChangedClientConfig'
      );

      const res = await referenceActions.editReference(args)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(res.payload).toBeUndefined();
      expect(mockAddToast).toBeCalledWith({
        message: 'txt_reference_edit_msg',
        msgVariables: {
          name: 'Mock label'
        },
        show: true,
        type: 'success'
      });
      expect(mockCloseReferenceModal).toBeCalledWith({});
      expect(mockGetListReference).toBeCalledWith({
        referenceTypeCodes: 'AG'
      });
      expect(mockSaveChangedClientConfig).toBeCalledWith({
        action: 'UPDATE',
        changedCategory: 'references',
        changedItem: {
          code: 'AG'
        },
        changedSection: {
          selectedOption: 'txt_adjustment_group'
        },
        newValue: {
          code: 'AG',
          description: 'Adjustment Group 1'
        },
        oldValue: {
          code: undefined,
          description: undefined
        }
      });
    });

    test('dispatches success with referenceTypeCode differ from AG', async () => {
      const args = {
        referenceTypeCode: 'DC' as any,
        data: {
          id: '1',
          code: 'DC',
          description: 'Deceased Contact 1',
          isActivate: false
        },
        previousValue: {
          id: '1',
          code: 'DC',
          description: 'Deceased Contact',
          isActivate: false
        },
        label: 'mock label'
      };

      spyUpdateOtherReference('updateOtherReferenceMockApi', () => ({
        type: 'clientConfigReference/commonEditReference/fulfilled',
        meta: {
          arg: {
            referenceTypeCode: args.referenceTypeCode,
            previousValue: args.previousValue,
            actionType: 'edit'
          },
          requestId: 'test request id',
          requestStatus: 'fulfilled'
        }
      }));

      const mockAddToast = mockActionToast('addToast');
      const mockCloseReferenceModal = mockActionReference(
        'closeReferenceModal'
      );
      const mockGetListReference = mockActionReference('getListReference');
      const mockSaveChangedClientConfig = mockActionChangeHistory(
        'saveChangedClientConfig'
      );

      const res = await referenceActions.editReference(args)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(res.payload).toBeUndefined();
      expect(mockAddToast).toBeCalledWith({
        message: 'txt_reference_edit_msg',
        msgVariables: {
          name: 'Mock label'
        },
        show: true,
        type: 'success'
      });
      expect(mockCloseReferenceModal).toBeCalledWith({});
      expect(mockGetListReference).toBeCalledWith({
        referenceTypeCodes: 'DC'
      });
      expect(mockSaveChangedClientConfig).toBeCalledWith({
        action: 'UPDATE',
        changedCategory: 'references',
        changedItem: {
          code: 'DC'
        },
        changedSection: {
          selectedOption: 'txt_deceased_contact'
        },
        newValue: {
          code: 'DC',
          description: 'Deceased Contact 1'
        },
        oldValue: {
          code: undefined,
          description: undefined
        }
      });
    });

    test('dispatches error with rejected response', async () => {
      const args = {
        referenceTypeCode: 'AG' as any,
        data: {
          id: '1',
          code: 'AG',
          description: 'Adjustment Group 1',
          isActivate: false
        },
        previousValue: {
          id: '1',
          code: 'AG',
          description: 'Adjustment Group',
          isActivate: true
        },
        label: 'mock label'
      };

      spyUpdateAdjustmentGroup('updateAdjustmentGroup', () => ({
        payload: undefined,
        type: 'clientConfigReference/commonEditReference/rejected',
        meta: {
          arg: args,
          requestId: 'test request id',
          rejectedWithValue: true,
          requestStatus: 'rejected',
          aborted: true,
          condition: true
        },
        error: 'error'
      }));

      const mockAddToast = mockActionToast('addToast');

      const res = await referenceActions.editReference(args)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(res.payload).toBeUndefined();
      expect(mockAddToast).toBeCalledWith({
        message: 'txt_reference_edit_server_err',
        msgVariables: {
          name: 'Mock label'
        },
        show: true,
        type: 'error'
      });
    });

    test('dispatches nothing if response return empty', async () => {
      spyUpdateAdjustmentGroup('updateAdjustmentGroup', () => ({
        payload: undefined,
        type: 'clientConfigReference/commonEditReference/pending',
        meta: {
          arg: {},
          requestId: 'test request id'
        }
      }));

      const res = await referenceActions.editReference({
        referenceTypeCode: 'AG' as any,
        data: {
          id: '1',
          code: 'AG',
          description: 'Adjustment Group 1',
          isActivate: false
        }
      } as any)(store.dispatch, store.getState, {});

      expect(res.payload).toBeUndefined();
    });
  });
});

describe('Test getReferencesDropdownListBuilder', () => {
  const store = createStore(rootReducer);

  it('Should have fulfilled', async () => {
    const actual = rootReducer(store.getState(), {
      type: 'clientConfigReference/commonEditReference/fulfilled',
      meta: {
        arg: {
          referenceTypeCode: 'AG' as any,
          data: {
            id: '1',
            code: 'AG',
            description: 'Adjustment Group 1',
            isActivate: false
          },
          previousValue: {
            id: '1',
            code: 'AG',
            description: 'Adjustment Group',
            isActivate: true
          }
        }
      }
    }).reference;

    expect(actual.modal).toEqual({ loading: false });
  });

  it('Should have pending', async () => {
    const actual = rootReducer(store.getState(), {
      type: 'clientConfigReference/commonEditReference/pending'
    }).reference;

    expect(actual.modal).toEqual({ loading: true });
  });

  it('Should have reject', async () => {
    const actual = rootReducer(store.getState(), {
      type: 'clientConfigReference/commonEditReference/rejected',
      error: { message: 'test error' }
    }).reference;

    expect(actual.loading).toBeFalsy();
    expect(actual.modal).toEqual({ loading: false });
  });
});
