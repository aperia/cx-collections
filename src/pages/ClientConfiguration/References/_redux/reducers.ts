import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// async thunks
import {
  getReferenceDropdownList,
  getReferencesDropdownListBuilder
} from './getReferencesDropdownList';

import { addReference, addReferenceBuilder } from './commonAddReference';

import {
  getListReference,
  getAllListReferenceBuilder
} from './commonGetListReference';

import { editReference, editReferenceBuilder } from './commonEditReference';

import {
  deleteReference,
  deleteReferenceBuilder
} from './commonDeleteReference';

import {
  activateReference,
  activateReferenceBuilder
} from './commonActivateReference';

import {
  updateDragAndDropData,
  updateDragAndDropDataBuilder
} from './commonUpdateDragAndDropData';

// Adjustment Group code
import { getAdjustmentGroupCodesBuilder } from './adjustmentGroupCodes/getAdjustmentGroupCodes';

// types
import {
  ReferenceModalError,
  OpenModalPayloadAction,
  ReferenceState,
  ReferenceTypeCode
} from '../types';
import {
  getOtherMockReferenceAPIBuilder,
  getOtherMockReferenceAPI
} from './otherReference/getOtherReference';
import { updateOtherReferenceMockApi } from './otherReference/updateOtherReference';

const defaultData: ReferenceState['data'] = { AG: {}, DC: {}, DR: {}, MTQ: {} };

const { actions, reducer } = createSlice({
  name: 'clientConfigReferences',
  initialState: { data: defaultData } as ReferenceState,
  reducers: {
    updateSelectedReferenceCodes: (
      draftState,
      action: PayloadAction<ReferenceTypeCode[]>
    ) => {
      const data = action.payload;
      draftState.selectedReferenceCodes = data;
    },
    setErrorMessage: (
      draftState,
      action: PayloadAction<ReferenceModalError>
    ) => {
      const { message, msgVariables } = action.payload;

      draftState.modal = {
        ...draftState.modal,
        error: { message, msgVariables }
      };
    },
    resetReference: draftState => {
      draftState.data = defaultData;
      draftState.selectedReferenceCodes = [];
    },
    openReferenceModal: (
      draftState,
      action: PayloadAction<OpenModalPayloadAction>
    ) => {
      const {
        modalType,
        referenceTypeCode,
        selectedItem = {}
      } = action.payload;
      draftState.modal = {
        ...draftState.modal,
        modalType,
        open: true,
        selectedItem,
        referenceTypeCode
      };
    },
    closeReferenceModal: (draftState, action: PayloadAction<{}>) => {
      draftState.modal = {
        ...draftState.modal,
        open: false
      };
    }
  },
  extraReducers: builder => {
    getReferencesDropdownListBuilder(builder);
    getAllListReferenceBuilder(builder);
    addReferenceBuilder(builder);
    editReferenceBuilder(builder);
    deleteReferenceBuilder(builder);
    activateReferenceBuilder(builder);
    getAdjustmentGroupCodesBuilder(builder);
    updateDragAndDropDataBuilder(builder);
    getOtherMockReferenceAPIBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getReferenceDropdownList,
  getListReference,
  addReference,
  editReference,
  deleteReference,
  activateReference,
  updateDragAndDropData,
  getOtherMockReferenceAPI,
  updateOtherReferenceMockApi
};

export { allActions as referenceActions, reducer as reference };
