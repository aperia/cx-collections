import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import isArray from 'lodash.isarray';

//services

//types
import { CommonGetListReferenceArgs, ReferenceState } from '../types';
import { getAdjustmentGroupCodes } from './adjustmentGroupCodes/getAdjustmentGroupCodes';
import { getOtherMockReferenceAPI } from './otherReference/getOtherReference';

export const getListReference = createAsyncThunk<
  void,
  CommonGetListReferenceArgs,
  ThunkAPIConfig
>('clientConfigReference/commonGetListReference', async (args, thunkAPI) => {
  const { dispatch } = thunkAPI;
  const { referenceTypeCodes } = args;
  const refTypeCodes = isArray(referenceTypeCodes)
    ? referenceTypeCodes
    : [referenceTypeCodes];
  const listDispatch: Promise<any>[] = [];
  refTypeCodes.forEach(referenceTypeCode => {
    if (referenceTypeCode === 'AG') {
      listDispatch.push(
        dispatch(getAdjustmentGroupCodes({ referenceTypeCode }))
      );
    } else {
      listDispatch.push(
        dispatch(getOtherMockReferenceAPI({ referenceTypeCode }))
      );
    }
  });
  await Promise.all(listDispatch);
});

export const getAllListReferenceBuilder = (
  builder: ActionReducerMapBuilder<ReferenceState>
) => {
  builder.addCase(getListReference.pending, (draftState, action) => {
    draftState.loading = true;
  });
  builder.addCase(getListReference.fulfilled, (draftState, action) => {
    draftState.loading = false;
  });
};
