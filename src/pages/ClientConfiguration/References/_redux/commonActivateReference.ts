import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';

//types
import {
  CommonActivateReferenceArgs,
  // ActivateReferenceArgs,
  ReferenceState
} from '../types';

//helper
import { dynamicMsg } from '../helper';

//constants
import { I18N_REFERENCE } from 'app/constants/i18n';

//redux
import { batch } from 'react-redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

//reducers
import { referenceActions } from './reducers';
import { updateAdjustmentGroup } from './adjustmentGroupCodes/updateAdjustmentGroupCodes';
import { updateOtherReferenceMockApi } from './otherReference/updateOtherReference';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';
import { MAPPING_REFERENCE_CODE_AND_LABEL } from '../constants';
// import { updateReferenceAPI } from './updateReference';

export const activateReference = createAsyncThunk<
  void,
  CommonActivateReferenceArgs,
  ThunkAPIConfig
>('clientConfigReference/commonActivateReference', async (args, thunkAPI) => {
  const { dispatch } = thunkAPI;
  const { data, referenceTypeCode, label } = args;

  const addDispatch: Promise<any>[] = [];

  if (referenceTypeCode === 'AG') {
    addDispatch.push(
      dispatch(updateAdjustmentGroup({ ...args, actionType: 'activate' }))
    );
  } else {
    addDispatch.push(
      dispatch(updateOtherReferenceMockApi({ ...args, actionType: 'activate' }))
    );
  }

  const [response] = await Promise.all(addDispatch);

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: data?.isActivate
            ? I18N_REFERENCE.REFERENCE_DEACTIVATE_MESSAGE
            : I18N_REFERENCE.REFERENCE_ACTIVATE_MESSAGE,
          msgVariables: {
            name: `${dynamicMsg(label)}`
          }
        })
      );

      dispatch(referenceActions.closeReferenceModal({}));
      dispatch(
        referenceActions.getListReference({
          referenceTypeCodes: referenceTypeCode
        })
      );
      dispatch(
        changeHistoryActions.saveChangedClientConfig({
          action: 'UPDATE',
          changedCategory: 'references',
          changedSection: {
            selectedOption: MAPPING_REFERENCE_CODE_AND_LABEL[referenceTypeCode]
          },
          changedItem: {
            code: data.code
          },
          oldValue: {
            activate: data.isActivate
          },
          newValue: {
            activate: !data.isActivate
          }
        })
      );
    });
  }

  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: data?.isActivate
            ? I18N_REFERENCE.REFERENCE_DEACTIVATE_SERVER_ERROR
            : I18N_REFERENCE.REFERENCE_ACTIVATE_SERVER_ERROR,
          msgVariables: {
            name: `${dynamicMsg(label)}`
          }
        })
      );
    });
  }
});

export const activateReferenceBuilder = (
  builder: ActionReducerMapBuilder<ReferenceState>
) => {
  builder.addCase(activateReference.pending, (draftState, action) => {
    draftState.modal = {
      ...draftState.modal,
      loading: true
    };
  });

  builder.addCase(activateReference.fulfilled, (draftState, action) => {
    draftState.modal = {
      ...draftState.modal,
      loading: false
    };
  });

  builder.addCase(activateReference.rejected, (draftState, action) => {
    draftState.modal = {
      ...draftState.modal,
      loading: false
    };
  });
};
