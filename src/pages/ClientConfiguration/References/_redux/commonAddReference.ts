import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';

//types
import {
  ReferenceState,
  CommonAddReferenceArgs,
  ReferenceTypeCode,
  ReferenceItem
} from '../types';

//helper
import { dynamicMsg } from '../helper';

//constants
import { I18N_REFERENCE } from 'app/constants/i18n';

//redux
import { batch } from 'react-redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

//reducers
import { referenceActions } from './reducers';

//enums
import { updateAdjustmentGroup } from './adjustmentGroupCodes/updateAdjustmentGroupCodes';
import { updateOtherReferenceMockApi } from './otherReference/updateOtherReference';
import { selectDataByReferenceCode } from './selectors';
import find from 'lodash.find';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';
import { MAPPING_REFERENCE_CODE_AND_LABEL } from '../constants';

export const checkDuplicatedCode: AppThunk<boolean> =
  (
    referenceTypeCode: ReferenceTypeCode,
    addingItem: ReferenceItem,
    label: String
  ) =>
  (dispatch, getState) => {
    const allDataByCode = selectDataByReferenceCode(
      getState(),
      referenceTypeCode
    );
    const isDuplicatedCode = find(
      allDataByCode,
      refItem =>
        refItem?.code?.toLocaleLowerCase() ===
        addingItem?.code?.toLocaleLowerCase()
    );
    if (isDuplicatedCode) {
      dispatch(
        referenceActions.setErrorMessage({
          message: I18N_REFERENCE.REFERENCE_CODE_DUPLICATE_MESSAGE,
          msgVariables: { name: `${label}` }
        })
      );
      return true;
    } else {
      return false;
    }
  };

export const addReference = createAsyncThunk<
  void,
  CommonAddReferenceArgs,
  ThunkAPIConfig
>('clientConfigReference/addReference', async (args, thunkAPI) => {
  const { dispatch, getState, rejectWithValue } = thunkAPI;

  const { referenceTypeCode, data, label } = args;

  // check duplicate codes in here if duplicated  -> go to reject
  const isDuplicated = checkDuplicatedCode(referenceTypeCode, data, label)(
    dispatch,
    getState,
    {}
  );
  if (isDuplicated) return rejectWithValue({});

  const addDispatch: Promise<any>[] = [];

  if (referenceTypeCode === 'AG') {
    addDispatch.push(
      dispatch(updateAdjustmentGroup({ ...args, actionType: 'add' }))
    );
  } else {
    addDispatch.push(
      dispatch(updateOtherReferenceMockApi({ ...args, actionType: 'add' }))
    );
  }

  const [response] = await Promise.all(addDispatch);

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_REFERENCE.REFERENCE_ADD_MESSAGE,
          msgVariables: {
            name: `${dynamicMsg(label)}`
          }
        })
      );
      dispatch(referenceActions.closeReferenceModal({}));
      dispatch(
        referenceActions.getListReference({
          referenceTypeCodes: referenceTypeCode
        })
      );
      dispatch(
        changeHistoryActions.saveChangedClientConfig({
          action: 'ADD',
          changedCategory: 'references',
          changedSection: {
            selectedOption: MAPPING_REFERENCE_CODE_AND_LABEL[referenceTypeCode]
          },
          changedItem: {
            code: data.code
          },
          newValue: {
            code: data.code,
            description: data.description
          }
        })
      );
    });
  }

  if (isRejected(response)) {
    batch(() => {
      dispatch(referenceActions.setErrorMessage({ message: '' }));
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_REFERENCE.REFERENCE_ADD_SERVER_ERROR,
          msgVariables: {
            name: `${dynamicMsg(label)}`
          }
        })
      );
    });
    return rejectWithValue({});
  }
});

export const addReferenceBuilder = (
  builder: ActionReducerMapBuilder<ReferenceState>
) => {
  builder.addCase(addReference.pending, (draftState, action) => {
    draftState.modal = {
      ...draftState.modal,
      loading: true
    };
  });
  builder.addCase(addReference.fulfilled, (draftState, action) => {
    draftState.modal = {
      ...draftState.modal,
      loading: false
    };
  });
  builder.addCase(addReference.rejected, (draftState, action) => {
    draftState.modal = {
      ...draftState.modal,
      loading: false
    };
  });
};
