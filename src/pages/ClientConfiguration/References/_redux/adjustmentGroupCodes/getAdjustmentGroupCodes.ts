import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import {
  GetAdjustmentGroupCodesArgs,
  GetAdjustmentGroupCodesPayload,
  ReferenceItem,
  ReferenceState
} from '../../types';

export const getAdjustmentGroupCodes = createAsyncThunk<
  GetAdjustmentGroupCodesPayload,
  GetAdjustmentGroupCodesArgs,
  ThunkAPIConfig
>('clientConfigReference/getAdjustmentGroupCodes', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;
  const response = await dispatch(
    clientConfigurationActions.getClientInfoData({
      dataType: 'adjustments'
    })
  );

  if (isFulfilled(response)) {
    const adjustmentModel =
      getState().mapping.data?.clientConfigurationAdjustmentGroupCodes || {};

    const adjustmentsGroups = mappingDataFromArray<ReferenceItem>(
      response.payload.adjustments?.adjustmentGroups || [],
      adjustmentModel
    );
    return { data: adjustmentsGroups };
  }

  return thunkAPI.rejectWithValue({});
});

export const getAdjustmentGroupCodesBuilder = (
  builder: ActionReducerMapBuilder<ReferenceState>
) => {
  builder.addCase(getAdjustmentGroupCodes.fulfilled, (draftState, action) => {
    const { data } = action.payload;
    draftState.data.AG = { data };
  });
  builder.addCase(getAdjustmentGroupCodes.rejected, (draftState, action) => {
    draftState.data.AG = { error: true };
  });
};
