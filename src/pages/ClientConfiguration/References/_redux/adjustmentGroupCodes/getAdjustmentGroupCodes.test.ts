import { byPassFulfilled, byPassRejected } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { getAdjustmentGroupCodes } from './getAdjustmentGroupCodes';

describe('getAdjustmentGroupCodes', () => {
  it('isFulfilled', async () => {
    const store = createStore(rootReducer, {});
    const response = await getAdjustmentGroupCodes({ referenceTypeCode: 'AG' })(
      byPassFulfilled(getAdjustmentGroupCodes.fulfilled.type, {}),
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ data: [] });
  });

  it('isRejected', async () => {
    const store = createStore(rootReducer, {});
    const response = await getAdjustmentGroupCodes({ referenceTypeCode: 'AG' })(
      byPassRejected(getAdjustmentGroupCodes.fulfilled.type, {}),
      store.getState,
      {}
    );

    expect(response.payload).toEqual({});
  });

  it('fulfilled', () => {
    const store = createStore(rootReducer, {});

    const action = getAdjustmentGroupCodes.fulfilled({ data: [] }, '', {
      referenceTypeCode: 'AG'
    });

    const state = rootReducer(store.getState(), action).reference;
    expect(state.data.AG).toEqual({ data: [] });
  });

  it('rejected', () => {
    const store = createStore(rootReducer, {});

    const action = getAdjustmentGroupCodes.rejected(new Error(), '', {
      referenceTypeCode: 'AG'
    });

    const state = rootReducer(store.getState(), action).reference;
    expect(state.data.AG).toEqual({ error: true });
  });
});
