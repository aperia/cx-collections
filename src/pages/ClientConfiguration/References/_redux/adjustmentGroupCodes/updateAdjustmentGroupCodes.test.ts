import {
  byPassFulfilled,
  byPassRejectedSpecificAction,
  mockActionCreator,
  specificName
} from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { ModalType } from '../../types';
import { updateAdjustmentGroup } from './updateAdjustmentGroupCodes';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

const spyClientConfiguration = mockActionCreator(clientConfigurationActions);

describe('updateAdjustmentGroup', () => {
  it('isFulfilled', async () => {
    const store = createStore(rootReducer, {});
    const response = await updateAdjustmentGroup({
      data: {},
      actionType: '' as unknown as ModalType,
      referenceTypeCode: 'AG'
    })(
      byPassFulfilled(updateAdjustmentGroup.fulfilled.type, {}),
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('isFulfilled add', async () => {
    const store = createStore(rootReducer, {});
    const response = await updateAdjustmentGroup({
      data: {},
      actionType: 'add',
      referenceTypeCode: 'AG'
    })(
      byPassFulfilled(updateAdjustmentGroup.fulfilled.type, {}),
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('isFulfilled delete', async () => {
    const store = createStore(rootReducer, {});
    const response = await updateAdjustmentGroup({
      data: {},
      actionType: 'delete',
      referenceTypeCode: 'AG'
    })(
      byPassFulfilled(updateAdjustmentGroup.fulfilled.type, {}),
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('isFulfilled activate', async () => {
    const store = createStore(rootReducer, {});
    const response = await updateAdjustmentGroup({
      data: {},
      actionType: 'activate',
      referenceTypeCode: 'AG'
    })(
      byPassFulfilled(updateAdjustmentGroup.fulfilled.type, {}),
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('isFulfilled edit', async () => {
    const store = createStore(rootReducer, {});
    const response = await updateAdjustmentGroup({
      data: {},
      actionType: 'edit',
      referenceTypeCode: 'AG'
    })(
      byPassFulfilled(updateAdjustmentGroup.fulfilled.type, {}),
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('isFulfilled updateAllData when has data updated', async () => {
    const store = createStore(rootReducer, {});
    const response = await updateAdjustmentGroup({
      data: {},
      actionType: 'updateAllData',
      referenceTypeCode: 'AG',
      allUpdatedData: [{ id: 'id' }]
    })(
      byPassFulfilled(updateAdjustmentGroup.fulfilled.type, {}),
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('isFulfilled updateAllData when no data updated', async () => {
    const store = createStore(rootReducer, {});
    const response = await updateAdjustmentGroup({
      data: {},
      actionType: 'updateAllData',
      referenceTypeCode: 'AG'
    })(
      byPassFulfilled(updateAdjustmentGroup.fulfilled.type, {}),
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('isRejected getClientInfoData', async () => {
    spyClientConfiguration('getClientInfoData', () => specificName);

    const store = createStore(rootReducer, {});
    const response = await updateAdjustmentGroup({
      data: {},
      actionType: 'add',
      referenceTypeCode: 'AG'
    })(
      byPassRejectedSpecificAction(updateAdjustmentGroup.fulfilled.type, {}),
      store.getState,
      {}
    );

    expect(response.payload).toEqual({});
  });

  it('isRejected updateClientInfo', async () => {
    spyClientConfiguration('updateClientInfo', () => specificName);

    const store = createStore(rootReducer, {});
    const response = await updateAdjustmentGroup({
      data: {},
      actionType: 'add',
      referenceTypeCode: 'AG'
    })(
      byPassRejectedSpecificAction(updateAdjustmentGroup.fulfilled.type, {}),
      store.getState,
      {}
    );

    expect(response.payload).toEqual({});
  });
});
