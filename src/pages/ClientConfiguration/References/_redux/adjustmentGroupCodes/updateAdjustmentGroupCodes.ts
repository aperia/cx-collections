import { createAsyncThunk, isRejected } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import findIndex from 'lodash.findindex';
import omit from 'lodash.omit';
import { AdjustmentGroupItem } from 'pages/ClientConfiguration/types';

import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import {
  ActionType,
  ReferenceItem,
  UpdateAdjustmentGroupsArgs
} from '../../types';
import { selectDataByReferenceCode } from '../selectors';

export const updateAdjustmentGroup = createAsyncThunk<
  undefined,
  UpdateAdjustmentGroupsArgs,
  ThunkAPIConfig
>('clientConfigReference/updateAdjustmentGroup', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;
  const {
    data: adjustmentGroupItem,
    actionType,
    previousValue,
    allUpdatedData
  } = args;

  const currentAdjustmentGroups = selectDataByReferenceCode(getState(), 'AG');

  //  need to fetch adjustmentCodes data to attach to the api
  const getClientInfoResponse = await dispatch(
    clientConfigurationActions.getClientInfoData({ dataType: 'adjustments' })
  );

  if (isRejected(getClientInfoResponse)) return thunkAPI.rejectWithValue({});

  let updatedData = [...currentAdjustmentGroups];

  // Transformation Data base on Type Edit/Delete/Add
  switch (actionType) {
    case 'add':
      updatedData.push(adjustmentGroupItem);
      break;
    case 'delete':
      const deleteIndex = findIndex(
        currentAdjustmentGroups,
        adjustmentGroupItem
      );
      updatedData.splice(deleteIndex, 1);
      break;

    case 'activate':
      const newUpdateItem: ReferenceItem = {
        ...adjustmentGroupItem,
        isActivate: !adjustmentGroupItem.isActivate
      };

      const activateIndex = findIndex(
        currentAdjustmentGroups,
        adjustmentGroupItem
      );
      updatedData.splice(activateIndex, 1, newUpdateItem);
      break;
    case 'edit':
      const editIndex = findIndex(currentAdjustmentGroups, previousValue);
      updatedData.splice(editIndex, 1, adjustmentGroupItem);
      break;

    case 'updateAllData':
      if (!allUpdatedData) break;
      updatedData = [...allUpdatedData];
      break;

    default:
      break;
  }

  // Revert Mapping
  const adjustmentModel =
    getState().mapping.data?.clientConfigurationAdjustmentGroupCodes || {};

  const excludedIdFieldMappingModel = omit(adjustmentModel, 'id');

  const adjustmentGroupCodes = mappingDataFromArray<AdjustmentGroupItem>(
    updatedData,
    excludedIdFieldMappingModel,
    true
  );

  const adjustmentsData = getClientInfoResponse?.payload?.adjustments;

  const updateActionSameBehaviorDelete: ActionType[] = [
    'activate',
    'delete',
    'updateAllData'
  ];

  const response = await dispatch(
    clientConfigurationActions.updateClientInfo({
      adjustments: {
        ...adjustmentsData,
        adjustmentGroups: adjustmentGroupCodes
      },
      updateAction: updateActionSameBehaviorDelete.includes(actionType)
        ? 'delete'
        : undefined
    })
  );

  if (isRejected(response))
    return thunkAPI.rejectWithValue({ response: response.payload?.response });

  return undefined;
});
