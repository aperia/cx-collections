import {
  mockActionCreator
} from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { deleteReference } from './commonDeleteReference';
import * as updateAdjustment from './adjustmentGroupCodes/updateAdjustmentGroupCodes';
import * as updateOtherReference from './otherReference/updateOtherReference';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { referenceActions } from './reducers';
import { I18N_REFERENCE } from 'app/constants/i18n';

const spyUpdateAdjustment = mockActionCreator(updateAdjustment);
const spyUpdateOtherReference = mockActionCreator(updateOtherReference);
const spyReferenceActions = mockActionCreator(referenceActions);
const spyActionsToast = mockActionCreator(actionsToast);

beforeEach(() => {
  spyUpdateAdjustment('updateAdjustmentGroup');
  spyUpdateOtherReference('updateOtherReferenceMockApi');
});

describe('deleteReference', () => {
  it('success', async () => {
    const mockAction = spyUpdateOtherReference('updateOtherReferenceMockApi');

    const store = createStore(rootReducer, {});
    await deleteReference({ data: {}, referenceTypeCode: 'DC', label: 'mock label'})(
      store.dispatch,
      store.getState,
      {}
    );

    expect(mockAction).toBeCalledWith({
      data: {},
      referenceTypeCode: 'DC',
      actionType: 'delete',
      label: 'mock label'
    });
  });

  it('isFulfilled with code AG', async () => {
    const mockAddToast = spyActionsToast('addToast');
    const mockCloseReferenceModal = spyReferenceActions('closeReferenceModal');
    const mockGetListReference = spyReferenceActions('getListReference');

    spyUpdateAdjustment('updateAdjustmentGroup', () => ({
      payload: undefined,
      type: 'clientConfigReference/commonDeleteReference/fulfilled',
      meta: {
        arg: {
          actionType: 'edit'
        },
        requestId: 'test request id',
        requestStatus: 'fulfilled'
      }
    }));

    const store = createStore(rootReducer, {});
    await deleteReference({ data: {}, referenceTypeCode: 'AG', label: 'mock label' })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_REFERENCE.REFERENCE_DELETE_MESSAGE,
      msgVariables: { name: 'Mock label' }
    });
    expect(mockCloseReferenceModal).toBeCalledWith({});
    expect(mockGetListReference).toBeCalledWith({ referenceTypeCodes: 'AG' });
  });

  it('isRejected', async () => {
    const mockAddToast = spyActionsToast('addToast');
    spyUpdateAdjustment('updateAdjustmentGroup', () => ({
      payload: undefined,
      type: 'clientConfigReference/commonDeleteReference/rejected',
      meta: {
        arg: {},
        requestId: 'test request id',
        rejectedWithValue: true,
        requestStatus: 'rejected',
        aborted: true,
        condition: true
      },
      error: 'error'
    }));

    const store = createStore(rootReducer, {});
    await deleteReference({
      data: { isActivate: true },
      referenceTypeCode: 'AG',
      label: 'mock label'
    })(store.dispatch, store.getState, {});

    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_REFERENCE.REFERENCE_DELETE_SERVER_ERROR,
      msgVariables: { name: 'Mock label' }
    });
  });

  it('pending', () => {
    const store = createStore(rootReducer, {});

    const action = deleteReference.pending(deleteReference.pending.type, {
      data: {},
      referenceTypeCode: 'AG',
      label: 'mock label'
    });
    const actual = rootReducer(store.getState(), action).reference.modal;

    expect(actual.loading).toBe(true);
  });

  it('fulfilled', () => {
    const store = createStore(rootReducer, {});

    const action = deleteReference.fulfilled(null, {}, '');
    const actual = rootReducer(store.getState(), action).reference.modal;

    expect(actual.loading).toBe(false);
  });

  it('rejected', () => {
    const store = createStore(rootReducer, {});

    const action = deleteReference.rejected(null, '', {
      data: {},
      referenceTypeCode: 'AG',
      label: 'mock label'
    });
    const actual = rootReducer(store.getState(), action).reference.modal;

    expect(actual.loading).toBe(false);
  });
});
