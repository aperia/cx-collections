import { createSelector } from '@reduxjs/toolkit';
import { EMPTY_ARRAY } from 'app/constants';
import { ReferenceTypeCode } from '../types';

const getReference = (states: RootState) => states.reference;

const getDataByReferenceType = (
  states: RootState,
  referenceType: ReferenceTypeCode
) => {
  return states.reference.data?.[referenceType];
};

const getModal = (states: RootState) => states.reference?.modal;

export const selectDataByReferenceCode = createSelector(
  getDataByReferenceType,
  gridData => gridData?.data || EMPTY_ARRAY
);
export const selectErrorByReferenceCode = createSelector(
  getDataByReferenceType,
  gridData => gridData?.error || false
);

export const selectDropdownListData = createSelector(
  getReference,
  data => data?.dropdownListData || []
);

export const selectedReferenceCodes = createSelector(
  getReference,
  data => data?.selectedReferenceCodes || []
);

export const selectedReferenceLoading = createSelector(
  getReference,
  refState => refState?.loading
);

export const selectedOpenModal = createSelector(
  getModal,
  data => data?.open || false
);

export const selectedItemModal = createSelector(
  getModal,
  data => data?.selectedItem || {}
);

export const selectedModalType = createSelector(
  getModal,
  data => data?.modalType
);

export const selectedReferenceTypeCode = createSelector(
  getModal,
  data => data?.referenceTypeCode
);

export const selectedModalErrorMsg = createSelector(
  getModal,
  data => data?.error
);

export const selectedModalIsLoading = createSelector(
  getModal,
  data => data?.loading
);
