import { rootReducer } from 'storeConfig';

// types
import { createStore } from '@reduxjs/toolkit';

//reducers
import { referenceActions } from './reducers';
import * as updateOtherReferenceMock from './otherReference/updateOtherReference';
import * as updateAdjustmentGroupCodesMock from './adjustmentGroupCodes/updateAdjustmentGroupCodes';
import * as selectReference from './selectors';

// constants
import { REFERENCE_DROPDOWN_ITEM_NAME } from '../constants';

describe('commonAddReference', () => {
  describe('when commonAddReference success', () => {
    let dispatch: jest.SpyInstance;
    let getState: jest.SpyInstance;

    beforeEach(() => {
      dispatch = jest.fn(cb => cb);
      getState = jest.fn(_ => ({}));
    });

    test('dispatches success with referenceTypeCode AG', async () => {
      const args = {
        referenceTypeCode: 'AG' as any,
        data: {
          id: '1',
          code: 'AG',
          description: 'Adjustment Group 1',
          isActivate: true
        },
        label: REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP
      };

      jest.spyOn(selectReference, 'selectDataByReferenceCode').mockReturnValue([
        {
          id: '2',
          code: 'DC',
          description: 'Deceased Contact 1',
          isActivate: true
        }
      ]);

      jest
        .spyOn(updateAdjustmentGroupCodesMock, 'updateAdjustmentGroup')
        .mockResolvedValue({
          type: 'clientConfigReference/addReference/fulfilled',
          meta: {
            arg: {
              referenceTypeCode: args.referenceTypeCode,
              data: args.data,
              actionType: 'add'
            },
            requestId: 'test request id',
            requestStatus: 'fulfilled'
          }
        } as never);

      const res = await referenceActions.addReference(args)(
        dispatch as any,
        getState as any,
        {}
      );

      expect(res.payload).toBeUndefined();

      expect(dispatch).toHaveBeenNthCalledWith(3, {
        payload: {
          message: 'txt_reference_add_msg',
          msgVariables: {
            name: 'Txt_adjustment_group'
          },
          show: true,
          type: 'success'
        },
        type: 'toastNotifications/addToast'
      });
    });

    test('dispatches success with referenceTypeCode differ from AG', async () => {
      const args = {
        referenceTypeCode: 'DC' as any,
        data: {
          id: '1',
          code: 'DR',
          description: 'DR 1',
          isActivate: false
        },
        label: REFERENCE_DROPDOWN_ITEM_NAME.DELINQUENCY_REASON
      };

      jest.spyOn(selectReference, 'selectDataByReferenceCode').mockReturnValue([
        {
          id: '2',
          code: 'DC',
          description: 'Deceased Contact 1',
          isActivate: true
        }
      ]);

      jest
        .spyOn(updateOtherReferenceMock, 'updateOtherReferenceMockApi')
        .mockResolvedValue({
          type: 'clientConfigReference/addReference/fulfilled',
          meta: {
            arg: {
              referenceTypeCode: args.referenceTypeCode,
              data: args.data,
              actionType: 'add'
            },
            requestId: 'test request id',
            requestStatus: 'fulfilled'
          }
        } as never);

      const res = await referenceActions.addReference(args)(
        dispatch as any,
        getState as any,
        {}
      );

      expect(res.payload).toBeUndefined();

      expect(dispatch).toHaveBeenNthCalledWith(3, {
        payload: {
          message: 'txt_reference_add_msg',
          msgVariables: { name: 'Txt_delinquency_reason' },
          show: true,
          type: 'success'
        },
        type: 'toastNotifications/addToast'
      });
    });

    test('dispatches error if duplicate code', async () => {
      const args = {
        referenceTypeCode: 'AG' as any,
        data: {
          id: '1',
          code: 'AG',
          description: 'Txt_adjustment_group',
          isActivate: false
        },
        label: REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP
      };

      jest
        .spyOn(selectReference, 'selectDataByReferenceCode')
        .mockReturnValue([args.data]);

      const res = await referenceActions.addReference(args)(
        dispatch as any,
        getState as any,
        {}
      );

      expect(res.payload).toEqual({});
      expect(dispatch).toBeCalledTimes(3);
    });

    test('dispatches error with rejected response', async () => {
      const args = {
        referenceTypeCode: 'AG' as any,
        data: {
          id: '1',
          code: 'AG',
          description: 'Txt_adjustment_group',
          isActivate: false
        },
        label: REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP
      };

      jest.spyOn(selectReference, 'selectDataByReferenceCode').mockReturnValue([
        {
          id: '2',
          code: 'DC',
          description: 'Deceased Contact 1',
          isActivate: true
        }
      ]);

      jest
        .spyOn(updateAdjustmentGroupCodesMock, 'updateAdjustmentGroup')
        .mockResolvedValue({
          payload: undefined,
          type: 'clientConfigReference/addReference/rejected',
          meta: {
            arg: args,
            requestId: 'test request id',
            rejectedWithValue: true,
            requestStatus: 'rejected',
            aborted: true,
            condition: true
          },
          error: 'error'
        } as never);

      const res = await referenceActions.addReference(args)(
        dispatch as any,
        getState as any,
        {}
      );

      expect(res.payload).toEqual({});
      expect(dispatch).toBeCalledTimes(5);
    });
  });
});

describe('Test getReferencesDropdownListBuilder', () => {
  const store = createStore(rootReducer);

  it('Should have fulfilled', async () => {
    const actual = rootReducer(store.getState(), {
      type: 'clientConfigReference/addReference/fulfilled',
      meta: {
        arg: {
          referenceTypeCode: 'AG' as any,
          data: {
            id: '1',
            code: 'AG',
            description: 'Adjustment Group 1',
            isActivate: false
          },
          label: REFERENCE_DROPDOWN_ITEM_NAME.ADJUSTMENT_GROUP
        }
      }
    }).reference;

    expect(actual.modal).toEqual({ loading: false });
  });

  it('Should have pending', async () => {
    const actual = rootReducer(store.getState(), {
      type: 'clientConfigReference/addReference/pending'
    }).reference;

    expect(actual.modal).toEqual({ loading: true });
  });

  it('Should have reject', async () => {
    const actual = rootReducer(store.getState(), {
      type: 'clientConfigReference/addReference/rejected',
      error: { message: 'test error' }
    }).reference;

    expect(actual.loading).toBeFalsy();
    expect(actual.modal).toEqual({ loading: false });
  });
});
