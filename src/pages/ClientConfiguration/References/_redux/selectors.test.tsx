import { selectorWrapper } from 'app/test-utils';
import {
  selectedReferenceLoading,
  selectedItemModal,
  selectedModalErrorMsg,
  selectedModalType,
  selectedOpenModal,
  selectDataByReferenceCode,
  selectErrorByReferenceCode,
  selectDropdownListData,
  selectedReferenceCodes,
  selectedReferenceTypeCode,
  selectedModalIsLoading
} from './selectors';

describe('Test Client Configuration Code To Text Selectors', () => {
  const store: Partial<RootState> = {
    reference: {
      dropdownList: [
        { id: '1', code: 'AG', value: 'Adjustment Group' },
        { id: '2', code: 'DC', value: 'Deceased Contact' }
      ],
      gridList: {
        loading: false,
        error: 'txt_grid_error',
        data: []
      },
      listCode: [],
      loading: false,
      modal: {
        modalType: 'add',
        open: false,
        selectedItem: {},
        error: { message: 'txt_error' }
      },
      selectedDropdown: [],
      data: {
        AG: {
          data: [
            {
              id: 'AG 1',
              code: 'AG 1',
              description: 'AG 1',
              isActivate: false
            }
          ],
          error: false
        },
        DC: {
          data: [
            {
              id: 'DC 1',
              code: 'DC 1',
              description: 'DC 1',
              isActivate: false
            }
          ],
          error: true
        },
        DR: {
          data: [
            {
              id: 'DR 1',
              code: 'DR 1',
              description: 'DR 1',
              isActivate: false
            }
          ],
          error: false
        },
        MTQ: {
          data: [
            {
              id: 'MTQ 1',
              code: 'MTQ 1',
              description: 'MTQ 1',
              isActivate: false
            }
          ],
          error: false
        }
      }
    } as any
  };

  it('selectDataByReferenceCode', () => {
    const data = selectDataByReferenceCode(store as any, 'DC');

    expect(data).toEqual([
      {
        code: 'DC 1',
        description: 'DC 1',
        id: 'DC 1',
        isActivate: false
      }
    ]);

    const defaultData = selectDataByReferenceCode(
      {
        ...store,
        reference: {
          ...store.reference,
          data: {
            ...store.reference?.data,
            DC: {
              ...store.reference?.data.DC,
              data: undefined
            }
          }
        }
      } as RootState,
      'DC'
    );

    expect(defaultData).toEqual([]);
  });

  it('selectErrorByReferenceCode', () => {
    const isError = selectErrorByReferenceCode(store as any, 'AG');

    expect(isError).toEqual(false);
  });

  it('selectDropdownListData', () => {
    const { data, emptyData } = selectorWrapper(store)(selectDropdownListData);

    expect(data).toEqual([]);
    expect(emptyData).toEqual([]);
  });

  it('selectedReferenceLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedReferenceLoading
    );

    expect(data).toEqual(false);
    expect(emptyData).toEqual(undefined);
  });

  it('selectedReferenceCodes', () => {
    const { data, emptyData } = selectorWrapper(store)(selectedReferenceCodes);

    expect(data).toEqual([]);
    expect(emptyData).toEqual([]);
  });

  it('selectedReferenceTypeCode', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedReferenceTypeCode
    );

    expect(data).toEqual(undefined);
    expect(emptyData).toEqual(undefined);
  });

  it('selectedItemModal', () => {
    const { data, emptyData } = selectorWrapper(store)(selectedItemModal);

    expect(data).toEqual({});
    expect(emptyData).toEqual({});
  });

  it('selectedModalErrorMsg', () => {
    const { data, emptyData } = selectorWrapper(store)(selectedModalErrorMsg);

    expect(data).toEqual({ message: 'txt_error' });
    expect(emptyData).toEqual(undefined);
  });

  it('selectedModalIsLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(selectedModalIsLoading);

    expect(data).toEqual(undefined);
    expect(emptyData).toEqual(undefined);
  });

  it('selectedModalType', () => {
    const { data, emptyData } = selectorWrapper(store)(selectedModalType);

    expect(data).toEqual('add');
    expect(emptyData).toEqual(undefined);
  });

  it('selectedOpenModal', () => {
    const { data, emptyData } = selectorWrapper(store)(selectedOpenModal);

    expect(data).toEqual(false);
    expect(emptyData).toEqual(false);
  });
});
