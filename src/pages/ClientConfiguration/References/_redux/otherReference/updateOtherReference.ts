import { createAsyncThunk, isRejected } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers/mappingData';
import findIndex from 'lodash.findindex';
import { GetCollectionsClientConfigResponse } from 'pages/ClientConfiguration/types';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { MAPPING_REFERENCE_CODE_AND_API_KEY } from '../../constants';

import { ReferenceItem, UpdateAdjustmentGroupsArgs } from '../../types';
import { selectDataByReferenceCode } from '../selectors';

export const updateOtherReferenceMockApi = createAsyncThunk<
  void,
  UpdateAdjustmentGroupsArgs,
  ThunkAPIConfig
>('clientConfigReference/updateReferenceMockArgs', async (args, thunkAPI) => {
  const { getState, dispatch } = thunkAPI;
  const {
    data: updateItem,
    actionType,
    previousValue,
    allUpdatedData,
    referenceTypeCode
  } = args;

  const currentDataByCode = selectDataByReferenceCode(
    getState(),
    referenceTypeCode
  );

  const getReferenceListResponse = await dispatch(
    clientConfigurationActions.getCollectionsClientConfig({
      component: 'component_reference_list_config'
    })
  );

  if (isRejected(getReferenceListResponse)) return thunkAPI.rejectWithValue({});

  const allReferenceList = getReferenceListResponse.payload || {};
  const keyByCode = MAPPING_REFERENCE_CODE_AND_API_KEY[
    referenceTypeCode
  ] as keyof GetCollectionsClientConfigResponse;

  let updatedData = [...currentDataByCode];

  // Transformation Data base on Type Edit/Delete/Add
  switch (actionType) {
    case 'add':
      updatedData.push({ ...updateItem, isActivate: true });
      break;
    case 'delete':
      const deleteIndex = findIndex(currentDataByCode, updateItem);
      updatedData.splice(deleteIndex, 1);
      break;

    case 'activate':
      const newUpdateItem: ReferenceItem = {
        ...updateItem,
        isActivate: !updateItem.isActivate
      };

      const activateIndex = findIndex(currentDataByCode, updateItem);
      updatedData.splice(activateIndex, 1, newUpdateItem);
      break;
    case 'edit':
      const editIndex = findIndex(currentDataByCode, previousValue);
      updatedData.splice(editIndex, 1, updateItem);
      break;

    case 'updateAllData':
      if (!allUpdatedData) break;
      updatedData = [...allUpdatedData];
      break;

    default:
      break;
  }

  const mappingModel =
    getState().mapping.data?.clientConfigurationReferenceOtherMockApiFields ||
    {};

  const mappedData = mappingDataFromArray<ReferenceItem>(
    updatedData,
    mappingModel,
    true
  );

  const updateResponse = await dispatch(
    clientConfigurationActions.saveCollectionsClientConfig({
      component: 'component_reference_list_config',
      jsonValue: {
        ...allReferenceList,
        [keyByCode]: mappedData
      }
    })
  );
  if (isRejected(updateResponse)) return thunkAPI.rejectWithValue({});
});
