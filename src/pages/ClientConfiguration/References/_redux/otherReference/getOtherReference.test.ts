import { byPassFulfilled, byPassRejected } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { getOtherMockReferenceAPI } from './getOtherReference';

describe('getOtherMockReferenceAPI', () => {
  it('isFulfilled', async () => {
    const store = createStore(rootReducer, {});
    const response = await getOtherMockReferenceAPI({
      referenceTypeCode: 'AG'
    })(
      byPassFulfilled(getOtherMockReferenceAPI.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ data: [] });
  });

  it('isRejected', async () => {
    const store = createStore(rootReducer, {});
    const response = await getOtherMockReferenceAPI({
      referenceTypeCode: 'AG'
    })(
      byPassRejected(getOtherMockReferenceAPI.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ data: undefined });
  });

  it('fulfilled', () => {
    const store = createStore(rootReducer, {});

    const action = getOtherMockReferenceAPI.fulfilled({ data: {} }, '', {
      referenceTypeCode: 'AG'
    });
    const actual = rootReducer(store.getState(), action).reference.data;

    expect(actual['AG']).toEqual({ data: {} });
  });

  it('rejected', () => {
    const store = createStore(rootReducer, {});

    const action = getOtherMockReferenceAPI.rejected(null, '', {
      referenceTypeCode: 'AG'
    });
    const actual = rootReducer(store.getState(), action).reference.data;

    expect(actual['AG']).toEqual({ error: true });
  });
});
