import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { updateOtherReferenceMockApi } from './updateOtherReference';
import { ModalType } from '../../types';
import {
  byPassFulfilled,
  byPassRejectedSpecificAction,
  mockActionCreator,
  specificName
} from 'app/test-utils';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

const spyClientConfiguration = mockActionCreator(clientConfigurationActions);

describe('updateOtherReferenceMockApi', () => {
  it('success', async () => {
    const store = createStore(rootReducer, {});
    const response = await updateOtherReferenceMockApi({
      actionType: '' as unknown as ModalType,
      data: { id: 'id' },
      referenceTypeCode: 'AG'
    })(
      byPassFulfilled(updateOtherReferenceMockApi.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('success add', async () => {
    const store = createStore(rootReducer, {});
    const response = await updateOtherReferenceMockApi({
      actionType: 'add',
      data: { id: 'id' },
      referenceTypeCode: 'AG'
    })(
      byPassFulfilled(updateOtherReferenceMockApi.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('success delete', async () => {
    const store = createStore(rootReducer, {});
    const response = await updateOtherReferenceMockApi({
      actionType: 'delete',
      data: { id: 'id' },
      referenceTypeCode: 'AG'
    })(
      byPassFulfilled(updateOtherReferenceMockApi.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('success activate', async () => {
    const store = createStore(rootReducer, {});
    const response = await updateOtherReferenceMockApi({
      actionType: 'activate',
      data: { id: 'id' },
      referenceTypeCode: 'AG'
    })(
      byPassFulfilled(updateOtherReferenceMockApi.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('success edit', async () => {
    const store = createStore(rootReducer, {});
    const response = await updateOtherReferenceMockApi({
      actionType: 'edit',
      data: { id: 'id' },
      referenceTypeCode: 'AG'
    })(
      byPassFulfilled(updateOtherReferenceMockApi.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('success updateAllData with data updated', async () => {
    const store = createStore(rootReducer, {});
    const response = await updateOtherReferenceMockApi({
      actionType: 'updateAllData',
      data: { id: 'id' },
      referenceTypeCode: 'AG',
      allUpdatedData: [{ id: 'id' }]
    })(
      byPassFulfilled(updateOtherReferenceMockApi.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('success updateAllData with no data updated', async () => {
    const store = createStore(rootReducer, {});
    const response = await updateOtherReferenceMockApi({
      actionType: 'updateAllData',
      data: { id: 'id' },
      referenceTypeCode: 'AG'
    })(
      byPassFulfilled(updateOtherReferenceMockApi.fulfilled.type),
      store.getState,
      {}
    );

    expect(response.payload).toEqual(undefined);
  });

  it('isRejected getCollectionsClientConfig', async () => {
    spyClientConfiguration('getCollectionsClientConfig', () => specificName);

    const store = createStore(rootReducer, {});
    const response = await updateOtherReferenceMockApi({
      data: {},
      actionType: 'add',
      referenceTypeCode: 'AG'
    })(
      byPassRejectedSpecificAction(
        updateOtherReferenceMockApi.fulfilled.type,
        {}
      ),
      store.getState,
      {}
    );

    expect(response.payload).toEqual({});
  });

  it('isRejected saveCollectionsClientConfig', async () => {
    spyClientConfiguration('saveCollectionsClientConfig', () => specificName);

    const store = createStore(rootReducer, {});
    const response = await updateOtherReferenceMockApi({
      data: {},
      actionType: 'add',
      referenceTypeCode: 'AG'
    })(
      byPassRejectedSpecificAction(
        updateOtherReferenceMockApi.fulfilled.type,
        {}
      ),
      store.getState,
      {}
    );

    expect(response.payload).toEqual({});
  });
});
