import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import {
  GetOtherListReferenceArgs,
  GetOtherListReferencePayload,
  ReferenceItem,
  ReferenceState
} from '../../types';

import { mappingDataFromArray } from 'app/helpers';
import { MAPPING_REFERENCE_CODE_AND_API_KEY } from '../../constants';
import {
  GetCollectionsClientConfigResponse,
  IReferenceItem
} from 'pages/ClientConfiguration/types';

export const getOtherMockReferenceAPI = createAsyncThunk<
  GetOtherListReferencePayload,
  GetOtherListReferenceArgs,
  ThunkAPIConfig
>('clientConfigReference/getListReferenceAPI', async (args, thunkAPI) => {
  const { dispatch, getState, rejectWithValue } = thunkAPI;
  const { referenceTypeCode } = args;
  const response = await dispatch(
    clientConfigurationActions.getCollectionsClientConfig({
      component: 'component_reference_list_config'
    })
  );

  if (isFulfilled(response)) {
    const referenceListConfig = response.payload;
    const keyByCode = MAPPING_REFERENCE_CODE_AND_API_KEY[
      referenceTypeCode
    ] as keyof GetCollectionsClientConfigResponse;
    const dataByReferenceCode = referenceListConfig?.[
      keyByCode
    ] as IReferenceItem[];
    const mappingModel =
      getState().mapping.data?.clientConfigurationReferenceOtherMockApiFields ||
      {};

    const mappedData = mappingDataFromArray<ReferenceItem>(
      dataByReferenceCode || [],
      mappingModel
    );
    return { data: mappedData };
  }
  return rejectWithValue({});
});

export const getOtherMockReferenceAPIBuilder = (
  builder: ActionReducerMapBuilder<ReferenceState>
) => {
  builder.addCase(getOtherMockReferenceAPI.fulfilled, (draftState, action) => {
    const { data } = action.payload;
    const { referenceTypeCode } = action.meta.arg;
    draftState.data[referenceTypeCode] = { data };
  });
  builder.addCase(getOtherMockReferenceAPI.rejected, (draftState, action) => {
    const { referenceTypeCode } = action.meta.arg;
    draftState.data[referenceTypeCode] = {
      error: true
    };
  });
};
