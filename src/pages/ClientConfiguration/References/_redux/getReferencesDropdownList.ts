import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import sortBy from 'lodash.sortby';

//services
import { referenceServices } from '../referencesServices';

//types
import { GetReferencesRefDataPayload, ReferenceState } from '../types';

export interface GetReferenceArgs {}

export const getReferenceDropdownList = createAsyncThunk<
  GetReferencesRefDataPayload,
  void,
  ThunkAPIConfig
>('clientConfigReference/getReferenceDropdownList', async (args, thunkAPI) => {
  const { mapping } = thunkAPI.getState();
  const { data } = await referenceServices.getReferenceDropdownList();

  const mappingData = mapping?.data?.getReferencesDropdownList || {};
  const mappedData = mappingDataFromArray<RefDataValue>(data, mappingData);
  const ascendingSortData = sortBy(mappedData, ['description']);

  return { data: ascendingSortData };
});

export const getReferencesDropdownListBuilder = (
  builder: ActionReducerMapBuilder<ReferenceState>
) => {
  builder.addCase(getReferenceDropdownList.pending, (draftState, action) => {
    draftState.loading = true;
    draftState.error = false;
  });
  builder.addCase(getReferenceDropdownList.fulfilled, (draftState, action) => {
    draftState.loading = false;
    draftState.dropdownListData = action.payload.data;
  });
  builder.addCase(getReferenceDropdownList.rejected, (draftState, action) => {
    draftState.loading = false;
    draftState.error = true;
  });
};
