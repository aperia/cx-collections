import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';

//types
import { ReferenceState, CommonEditReferenceArgs } from '../types';

//constants
import { I18N_REFERENCE } from 'app/constants/i18n';

//redux
import { batch } from 'react-redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

//reducers
import { referenceActions } from './reducers';

//helpers
import { dynamicMsg } from '../helper';
import { updateAdjustmentGroup } from './adjustmentGroupCodes/updateAdjustmentGroupCodes';
import { updateOtherReferenceMockApi } from './otherReference/updateOtherReference';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';
import { MAPPING_REFERENCE_CODE_AND_LABEL } from '../constants';

export const editReference = createAsyncThunk<
  void,
  CommonEditReferenceArgs,
  ThunkAPIConfig
>('clientConfigReference/commonEditReference', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;
  const { referenceTypeCode, data, label } = args;
  const { selectedItem } = getState().reference.modal;

  const addDispatch: Promise<any>[] = [];

  if (referenceTypeCode === 'AG') {
    addDispatch.push(
      dispatch(updateAdjustmentGroup({ ...args, actionType: 'edit' }))
    );
  } else {
    addDispatch.push(
      dispatch(updateOtherReferenceMockApi({ ...args, actionType: 'edit' }))
    );
  }

  const [response] = await Promise.all(addDispatch);

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_REFERENCE.REFERENCE_EDIT_MESSAGE,
          msgVariables: {
            name: `${dynamicMsg(label)}`
          }
        })
      );

      dispatch(referenceActions.closeReferenceModal({}));
      dispatch(
        referenceActions.getListReference({
          referenceTypeCodes: referenceTypeCode
        })
      );
      dispatch(
        changeHistoryActions.saveChangedClientConfig({
          action: 'UPDATE',
          changedCategory: 'references',
          changedSection: {
            selectedOption: MAPPING_REFERENCE_CODE_AND_LABEL[referenceTypeCode]
          },
          changedItem: {
            code: data.code
          },
          oldValue: {
            code: selectedItem?.code,
            description: selectedItem?.description
          },
          newValue: {
            code: data.code,
            description: data.description
          }
        })
      );
    });

    return;
  }

  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_REFERENCE.REFERENCE_EDIT_SERVER_ERROR,
          msgVariables: {
            name: `${dynamicMsg(label)}`
          }
        })
      );
    });
  }
});

export const editReferenceBuilder = (
  builder: ActionReducerMapBuilder<ReferenceState>
) => {
  builder.addCase(editReference.pending, (draftState, action) => {
    draftState.modal = {
      ...draftState.modal,
      loading: true
    };
  });

  builder.addCase(editReference.fulfilled, (draftState, action) => {
    draftState.modal = {
      ...draftState.modal,
      loading: false
    };
  });

  builder.addCase(editReference.rejected, (draftState, action) => {
    draftState.modal = {
      ...draftState.modal,
      loading: false
    };
  });
};
