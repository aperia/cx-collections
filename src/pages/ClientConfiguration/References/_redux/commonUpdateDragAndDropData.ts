import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isRejected
} from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import { DRAG_AND_DROP_FAILED_MESSAGE } from '../constants';

//types
import { CommonUpdateDragAndDropArgs, ReferenceState } from '../types';
import { updateAdjustmentGroup } from './adjustmentGroupCodes/updateAdjustmentGroupCodes';
import { updateOtherReferenceMockApi } from './otherReference/updateOtherReference';

export const updateDragAndDropData = createAsyncThunk<
  void,
  CommonUpdateDragAndDropArgs,
  ThunkAPIConfig
>('clientConfigReference/updateDragAndDropData', async (args, thunkAPI) => {
  const { referenceTypeCode, allUpdatedData } = args;
  const { dispatch, rejectWithValue } = thunkAPI;

  const addDispatch: Promise<any>[] = [];

  if (referenceTypeCode === 'AG') {
    addDispatch.push(
      dispatch(
        updateAdjustmentGroup({
          referenceTypeCode,
          allUpdatedData,
          actionType: 'updateAllData',
          data: {}
        })
      )
    );
  } else {
    addDispatch.push(
      dispatch(
        updateOtherReferenceMockApi({
          ...args,
          actionType: 'updateAllData',
          data: {}
        })
      )
    );
  }
  const [response] = await Promise.all(addDispatch);
  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          message: DRAG_AND_DROP_FAILED_MESSAGE[referenceTypeCode],
          type: 'error'
        })
      );
    });
    return rejectWithValue({});
  }
});

export const updateDragAndDropDataBuilder = (
  builder: ActionReducerMapBuilder<ReferenceState>
) => {
  builder.addCase(updateDragAndDropData.pending, (draftState, action) => {
    draftState.loading = true;
  });

  builder.addCase(updateDragAndDropData.fulfilled, (draftState, action) => {
    const { allUpdatedData, referenceTypeCode } = action.meta.arg;
    draftState.loading = false;
    draftState.data = {
      ...draftState.data,
      [referenceTypeCode]: {
        data: allUpdatedData
      }
    };
  });

  builder.addCase(updateDragAndDropData.rejected, (draftState, action) => {
    draftState.loading = false;
  });
};
