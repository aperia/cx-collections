// types
import { AsyncThunkAction, createStore } from '@reduxjs/toolkit';
import { Dispatch } from 'react';
import { rootReducer } from 'storeConfig';
import { referenceServices } from '../referencesServices';

//reducers
import { getReferenceDropdownList } from './getReferencesDropdownList';

describe('getReferenceDropdownList', () => {
  describe('when getReferenceDropdownList success', () => {
    let dispatch: Dispatch<any>;
    let getState: () => unknown;
    let action: AsyncThunkAction<unknown, any, {}>;

    beforeEach(() => {
      dispatch = jest.fn(cb => cb);

      action = getReferenceDropdownList();
    });

    test('dispatches success', async () => {
      jest
        .spyOn(referenceServices, 'getReferenceDropdownList')
        .mockResolvedValue({
          data: [
            { value: 'AG', description: 'Adjustment Group' },
            { value: 'DC', description: 'Deceased Contact' }
          ]
        } as never);

      getState = jest.fn().mockImplementation(() => ({
        mapping: {
          data: {
            getReferencesDropdownList: {
              value: 'value',
              description: 'description'
            }
          }
        }
      }));

      const res = await action(dispatch, getState, {});

      expect(res.payload).toEqual({
        data: [
          { description: 'Adjustment Group', value: 'AG' },
          { description: 'Deceased Contact', value: 'DC' }
        ]
      });
      expect(dispatch).toBeCalledTimes(2);
    });

    test('dispatches success with empty data', async () => {
      jest
        .spyOn(referenceServices, 'getReferenceDropdownList')
        .mockResolvedValue({
          data: [
            { value: 'AG', description: 'Adjustment Group' },
            { value: 'DC', description: 'Deceased Contact' }
          ]
        } as never);

      getState = jest.fn().mockImplementation(() => ({
        mapping: {}
      }));

      const res = await action(dispatch, getState, {});

      expect(res.payload).toEqual({
        data: []
      });
      expect(dispatch).toBeCalledTimes(2);
    });
  });
});

describe('Test getReferencesDropdownListBuilder', () => {
  const store = createStore(rootReducer);

  it('Should have fulfilled', async () => {
    const actual = rootReducer(store.getState(), {
      type: 'clientConfigReference/getReferenceDropdownList/fulfilled',
      payload: {
        data: [
          { value: 'AG', description: 'Adjustment Group' },
          { value: 'DC', description: 'Deceased Contact' }
        ],
        callback: jest.fn()
      }
    }).reference;

    expect(actual.loading).toBeFalsy();
    expect(actual.dropdownListData).toEqual([
      { value: 'AG', description: 'Adjustment Group' },
      { value: 'DC', description: 'Deceased Contact' }
    ]);
  });

  it('Should have pending', async () => {
    const actual = rootReducer(store.getState(), {
      type: 'clientConfigReference/getReferenceDropdownList/pending'
    }).reference;

    expect(actual.loading).toBeTruthy();
    expect(actual.error).toBeFalsy();
  });

  it('Should have reject', async () => {
    const actual = rootReducer(store.getState(), {
      type: 'clientConfigReference/getReferenceDropdownList/rejected',
      error: { message: 'test error' }
    }).reference;

    expect(actual.loading).toBeFalsy();
    expect(actual.error).toBeTruthy();
  });
});
