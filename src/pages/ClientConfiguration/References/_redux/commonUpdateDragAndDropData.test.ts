import { Dispatch } from 'react';
import { rootReducer } from 'storeConfig';

// types
import { createStore } from '@reduxjs/toolkit';

//reducers
import { updateDragAndDropData } from './commonUpdateDragAndDropData';
import * as updateOtherReferenceMock from './otherReference/updateOtherReference';
import * as updateAdjustmentGroupCodesMock from './adjustmentGroupCodes/updateAdjustmentGroupCodes';

describe('commonUpdateDragAndDropData', () => {
  // it('dispatch a add function rules mapping request');

  describe('when commonUpdateDragAndDropData success', () => {
    let dispatch: Dispatch<any>;
    let getState: () => RootState;

    beforeEach(() => {
      dispatch = jest.fn(cb => cb);
      getState = jest.fn();
    });

    test('dispatches success with referenceTypeCode AG', async () => {
      const args = {
        referenceTypeCode: 'AG' as any,
        allUpdatedData: [
          {
            id: '1',
            code: 'AG',
            description: 'Adjustment Group',
            isActivate: true
          }
        ]
      };

      jest
        .spyOn(updateAdjustmentGroupCodesMock, 'updateAdjustmentGroup')
        .mockResolvedValue({
          payload: undefined,
          type: 'clientConfigReference/updateDragAndDropData/fulfilled',
          meta: {
            arg: {
              referenceTypeCode: args.referenceTypeCode,
              allUpdatedData: args.allUpdatedData,
              actionType: 'updateAllData',
              data: {}
            },
            requestId: 'test request id',
            requestStatus: 'fulfilled'
          }
        } as never);

      const res = await updateDragAndDropData(args)(dispatch, getState, {});

      expect(res.payload).toBeUndefined();
      expect(dispatch).toBeCalledTimes(3);
    });

    test('dispatches success with referenceTypeCode differ from AG', async () => {
      const args = {
        referenceTypeCode: 'DC' as any,
        allUpdatedData: []
      };

      jest
        .spyOn(updateOtherReferenceMock, 'updateOtherReferenceMockApi')
        .mockResolvedValue({
          type: 'clientConfigReference/updateDragAndDropData/fulfilled',
          meta: {
            arg: {
              actionType: 'updateAllData',
              data: {}
            },
            requestId: 'test request id',
            requestStatus: 'fulfilled'
          }
        } as never);

      const res = await updateDragAndDropData(args)(dispatch, getState, {});

      expect(res.payload).toBeUndefined();
      expect(dispatch).toBeCalledTimes(3);
    });

    test('dispatches error with rejected response', async () => {
      const args = {
        referenceTypeCode: 'AG' as any,
        allUpdatedData: [
          {
            id: '1',
            code: 'AG',
            description: 'Adjustment Group',
            isActivate: true
          }
        ]
      };

      jest
        .spyOn(updateAdjustmentGroupCodesMock, 'updateAdjustmentGroup')
        .mockResolvedValue({
          payload: undefined,
          type: 'clientConfigReference/updateDragAndDropData/rejected',
          meta: {
            arg: {},
            requestId: 'test request id',
            rejectedWithValue: true,
            requestStatus: 'rejected',
            aborted: true,
            condition: true
          },
          error: 'error'
        } as never);

      const res = await updateDragAndDropData(args)(dispatch, getState, {});

      expect(res.payload).toEqual({});
      expect(dispatch).toBeCalledTimes(4);
      expect(dispatch).toHaveBeenNthCalledWith(3, {
        payload: {
          message: 'txt_adjustment_group_code_failed_to_update',
          type: 'error'
        },
        type: 'toastNotifications/addToast'
      });
    });
  });
});

describe('Test getReferencesDropdownListBuilder', () => {
  const store = createStore(rootReducer);

  it('Should have fulfilled', async () => {
    const actual = rootReducer(store.getState(), {
      type: 'clientConfigReference/updateDragAndDropData/fulfilled',
      meta: {
        arg: {
          referenceTypeCode: 'AG',
          allUpdatedData: [
            { value: 'AG', description: 'Adjustment Group' },
            { value: 'AG1', description: 'Adjustment Group 1' }
          ]
        }
      }
    }).reference;

    expect(actual.loading).toBeFalsy();
    expect(actual.data).toEqual({
      AG: {
        data: [
          { description: 'Adjustment Group', value: 'AG' },
          { description: 'Adjustment Group 1', value: 'AG1' }
        ]
      },
      DC: {},
      DR: {},
      MTQ: {}
    });
  });

  it('Should have pending', async () => {
    const actual = rootReducer(store.getState(), {
      type: 'clientConfigReference/updateDragAndDropData/pending'
    }).reference;

    expect(actual.loading).toBeTruthy();
  });

  it('Should have reject', async () => {
    const actual = rootReducer(store.getState(), {
      type: 'clientConfigReference/updateDragAndDropData/rejected',
      error: { message: 'test error' }
    }).reference;

    expect(actual.loading).toBeFalsy();
  });
});
