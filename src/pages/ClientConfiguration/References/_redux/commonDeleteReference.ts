import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';

//types
import { ReferenceState, CommonDeleteReferenceArgs } from '../types';

//helper
import { dynamicMsg } from '../helper';

//constants
import { I18N_REFERENCE } from 'app/constants/i18n';

//redux
import { batch } from 'react-redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

//reducers
import { referenceActions } from './reducers';
import { updateAdjustmentGroup } from './adjustmentGroupCodes/updateAdjustmentGroupCodes';
import { updateOtherReferenceMockApi } from './otherReference/updateOtherReference';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';
import { MAPPING_REFERENCE_CODE_AND_LABEL } from '../constants';

export const deleteReference = createAsyncThunk<
  void,
  CommonDeleteReferenceArgs,
  ThunkAPIConfig
>('clientConfigReference/deleteReference', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;
  const { referenceTypeCode, label } = args;
  const { selectedItem } = getState().reference.modal;
  const addDispatch: Promise<any>[] = [];

  if (referenceTypeCode === 'AG') {
    addDispatch.push(
      dispatch(updateAdjustmentGroup({ ...args, actionType: 'delete' }))
    );
  } else {
    addDispatch.push(
      dispatch(updateOtherReferenceMockApi({ ...args, actionType: 'delete' }))
    );
  }

  const [response] = await Promise.all(addDispatch);

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_REFERENCE.REFERENCE_DELETE_MESSAGE,
          msgVariables: {
            name: `${dynamicMsg(label)}`
          }
        })
      );

      dispatch(referenceActions.closeReferenceModal({}));
      dispatch(
        referenceActions.getListReference({
          referenceTypeCodes: referenceTypeCode
        })
      );
      dispatch(
        changeHistoryActions.saveChangedClientConfig({
          action: 'DELETE',
          changedCategory: 'references',
          changedSection: {
            selectedOption: MAPPING_REFERENCE_CODE_AND_LABEL[referenceTypeCode]
          },
          changedItem: {
            code: selectedItem?.code
          },
          oldValue: {
            code: selectedItem?.code,
            description: selectedItem?.description
          }
        })
      );
    });
  }

  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_REFERENCE.REFERENCE_DELETE_SERVER_ERROR,
          msgVariables: {
            name: `${dynamicMsg(label)}`
          }
        })
      );
    });
  }
});

export const deleteReferenceBuilder = (
  builder: ActionReducerMapBuilder<ReferenceState>
) => {
  builder.addCase(deleteReference.pending, (draftState, action) => {
    draftState.modal = {
      ...draftState.modal,
      loading: true
    };
  });

  builder.addCase(deleteReference.fulfilled, (draftState, action) => {
    draftState.modal = {
      ...draftState.modal,
      loading: false
    };
  });

  builder.addCase(deleteReference.rejected, (draftState, action) => {
    draftState.modal = {
      ...draftState.modal,
      loading: false
    };
  });
};
