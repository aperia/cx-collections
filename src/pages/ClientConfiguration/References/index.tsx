import React, { useMemo, useState } from 'react';

//components
import {
  Button,
  Icon,
  Popover,
  SimpleBar
} from 'app/_libraries/_dls/components';
import ReferencesDropDown from './ReferencesDropDown';
import GridList from './GridList';

//constants
import { I18N_REFERENCE } from 'app/constants/i18n';

//components
import ReferencesModal from './ReferencesModal';

//hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

//reducers
import { useSelector } from 'react-redux';

//selectors
import { selectedOpenModal, selectedReferenceCodes } from './_redux/selectors';
import classNames from 'classnames';
import ApiErrorForHeaderSection from '../ApiErrorSection/ApiErrorForHeaderSection';
import { genAmtId } from 'app/_libraries/_dls/utils';

const References: React.FC = () => {
  const [showGuide, setShowGuide] = useState<boolean>(false);

  const { t } = useTranslation();
  const testId = 'clientConfig_references';

  const isShowModal = useSelector(selectedOpenModal);
  const selectedCodes = useSelector(selectedReferenceCodes);

  const referencesDescription = useMemo(() => {
    return (
      <div>
        <h5 data-testid={genAmtId(testId, 'title', '')}>
          {t(I18N_REFERENCE.REFERENCES)}
        </h5>
        <p className="mt-16" data-testid={genAmtId(testId, 'description', '')}>
          {t(I18N_REFERENCE.REFERENCES_DESCRIPTION)}
        </p>
      </div>
    );
  }, [t]);

  const referencesGuide = () => {
    return (
      <div className="d-flex mt-24">
        <div className="d-flex align-items-center bg-light-l20 py-12 px-16 br-radius-4">
          <p className="mr-8 text-grey" data-testid={genAmtId(testId, 'note', '')}>
            {t(I18N_REFERENCE.REFERENCES_NOTE)}
          </p>
          <Popover
            size="sm"
            onVisibilityChange={opened => setShowGuide(opened)}
            element={
              <img
                src="images/dragging.png"
                alt="dragging guide"
                className="mw-100"
              />
            }
          >
            <Button
              className={classNames({ active: showGuide })}
              variant="icon-secondary"
              dataTestId={`${testId}_infoBtn`}
            >
              <Icon name="information" size="5x"></Icon>
            </Button>
          </Popover>
        </div>
      </div>
    );
  };

  return (
    <div className="position-relative h-100">
      <SimpleBar>
        <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
          <ApiErrorForHeaderSection className="mb-24" dataTestId={`${testId}_apiError`}/>
          <div className="d-lg-flex align-items-center justify-content-between ">
            {referencesDescription}
            <ReferencesDropDown />
          </div>
          {Boolean(selectedCodes?.length) && referencesGuide()}
          <GridList />
        </div>

        {isShowModal && <ReferencesModal />}
      </SimpleBar>
    </div>
  );
};

export default References;
