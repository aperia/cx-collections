import { apiService } from 'app/utils/api.service';

export const referenceServices = {
  async getReferenceDropdownList() {
    return apiService.get<RefDataValue[]>('refData/referenceSessions.json');
  }
};
