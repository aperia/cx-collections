import React from 'react';

//mock test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

//components
import Actions, { ActionsProps } from './Actions';

//actions
import { referenceActions } from './_redux/reducers';

//constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

const mockReferenceActions = mockActionCreator(referenceActions);
const defaultAction: ActionsProps = {
  referenceItem: {},
  referenceTypeCode: 'AG'
};
describe('render actions buttons ', () => {
  const renderWrapper = (
    actionProp: ActionsProps,
    initialState: Partial<RootState>
  ) => {
    return renderMockStore(<Actions {...actionProp} />, { initialState });
  };
  it('should render modal delete and call dispatch modal actions when click delete button', () => {
    const mockAction = mockReferenceActions('openReferenceModal');

    renderWrapper(defaultAction, {});

    const deleteButton = screen.getByRole('button', {
      name: I18N_COMMON_TEXT.DELETE
    });

    userEvent.click(deleteButton);

    expect(deleteButton).toBeInTheDocument();

    expect(mockAction).toBeCalledWith({
      modalType: 'delete',
      selectedItem: {},
      referenceTypeCode: 'AG'
    });
  });

  it('should render modal edit and call dispatch modal actions when click edit button', () => {
    const mockAction = mockReferenceActions('openReferenceModal');

    renderWrapper(defaultAction, {});

    const editButton = screen.getByRole('button', {
      name: I18N_COMMON_TEXT.EDIT
    });

    userEvent.click(editButton);

    expect(editButton).toBeInTheDocument();

    expect(mockAction).toBeCalledWith({
      modalType: 'edit',
      selectedItem: {},
      referenceTypeCode: 'AG'
    });
  });

  it('should render modal activate and call dispatch modal actions when click activate button', () => {
    const mockAction = mockReferenceActions('openReferenceModal');

    renderWrapper(defaultAction, {});

    const editButton = screen.getByRole('button', {
      name: I18N_COMMON_TEXT.ACTIVATE
    });

    userEvent.click(editButton);

    expect(editButton).toBeInTheDocument();

    expect(mockAction).toBeCalledWith({
      modalType: 'activate',
      selectedItem: {},
      referenceTypeCode: 'AG'
    });
  });

  it('should render modal deactivate and call dispatch modal actions when click deactivate button', () => {
    const mockAction = mockReferenceActions('openReferenceModal');

    renderWrapper(
      { ...defaultAction, referenceItem: { isActivate: true } },
      {}
    );

    const editButton = screen.getByRole('button', {
      name: I18N_COMMON_TEXT.DEACTIVATE
    });

    userEvent.click(editButton);

    expect(editButton).toBeInTheDocument();

    expect(mockAction).toBeCalledWith({
      modalType: 'activate',
      selectedItem: { isActivate: true },
      referenceTypeCode: 'AG'
    });
  });
});
