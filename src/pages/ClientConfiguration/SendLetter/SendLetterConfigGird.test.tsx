import React from 'react';
import { fireEvent, screen } from '@testing-library/dom';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { sendLetterConfigActions } from './_redux/reducers';
import LetterConfigGrid from './SendLetterConfigGird';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { SendLetterConfigData } from './types';
import { SortType, GridProps } from 'app/_libraries/_dls/components';
import cloneDeep from 'lodash.clonedeep';
import { I18N_ADJUSTMENT_TRANSACTION } from '../AdjustmentTransaction/constants';
import * as mockStatus from '../hooks/useCheckClientConfigStatus';

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponent = jest.requireActual('app/_libraries/_dls/components');
  const { MockGrid } = jest.requireActual('app/test-utils/mocks/MockGrid');
  return {
    ...dlsComponent,
    Grid: (props: GridProps) => (
      <>
        <MockGrid {...props} />
        <button
          data-testid="mockSortGrid"
          onClick={() => props.onSortChange?.({ id: 'state' })}
        >
          Mock Sort Grid component
        </button>
      </>
    )
  };
});

jest.mock('./SendLetterModal', () => ({
  __esModule: true,
  default: () => <div>Mock Add Modal</div>
}));

jest.mock('./Actions', () => ({
  Actions: () => <div>Mock Actions component</div>
}));

const spySendLetterConfigActions = mockActionCreator(sendLetterConfigActions);

const initialState = {
  sendLetterConfig: {
    sendLetterConfigurationList: {
      loading: false,
      data: [
        {
          state: {
            description: 'mock description 1',
            value: 'mock value 1'
          },
          letterNumber: 'mock letter number 1',
          stateDesc: 'mock state 1'
        },
        {
          state: {
            description: 'mock description 2',
            value: 'mock value 2'
          },
          letterNumber: 'mock letter number 2',
          stateDesc: 'mock state 2'
        },
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {}
      ] as SendLetterConfigData[],
      error: undefined
    },
    sortBy: {} as SortType
  }
} as RootState;

describe('Test on Send letter config Grid UI component', () => {
  it('should render Grid UI > has data with pagination', () => {
    const mockGetData = spySendLetterConfigActions(
      'getSendLetterConfiguration'
    );

    renderMockStore(<LetterConfigGrid />, { initialState });

    expect(mockGetData).toHaveBeenCalled();
    expect(screen.getByText('mock letter number 1')).toBeInTheDocument();
    expect(screen.getByText('mock letter number 2')).toBeInTheDocument();
  });

  it('should render Grid UI > has loading', () => {
    const mockGetData = spySendLetterConfigActions(
      'getSendLetterConfiguration'
    );

    const { baseElement } = renderMockStore(<LetterConfigGrid />, {
      initialState: {
        sendLetterConfig: {
          sendLetterConfigurationList: {
            loading: true,
            data: [] as SendLetterConfigData[],
            error: undefined
          },
          sortBy: {} as SortType
        }
      } as RootState
    });

    expect(mockGetData).toHaveBeenCalled();
    expect(baseElement.querySelector('.vh-50')).toBeInTheDocument();
  });

  it('should render Grid UI > has error', () => {
    const mockGetData = spySendLetterConfigActions(
      'getSendLetterConfiguration'
    );

    renderMockStore(<LetterConfigGrid />, {
      initialState: {
        sendLetterConfig: {
          sendLetterConfigurationList: {
            loading: false,
            data: [] as SendLetterConfigData[],
            error: 'mock error'
          },
          sortBy: {} as SortType
        }
      } as RootState
    });

    expect(mockGetData).toHaveBeenCalled();
    mockGetData.mockClear();

    expect(screen.getByText(/^txt_failure_ocurred_on/i)).toBeInTheDocument();

    fireEvent.click(screen.getByText(I18N_COMMON_TEXT.RELOAD));
    expect(mockGetData).toHaveBeenCalled();
  });

  it('should render Grid UI > trigger handleSort', () => {
    spySendLetterConfigActions('getSendLetterConfiguration');
    const mockChangeSort = spySendLetterConfigActions('onChangeSort');

    renderMockStore(<LetterConfigGrid />, { initialState });

    fireEvent.click(screen.getByTestId('mockSortGrid'));

    expect(mockChangeSort).toHaveBeenCalled();
  });

  it('should render Grid UI > data is empty', () => {
    jest.spyOn(mockStatus, 'useCheckClientConfigStatus').mockReturnValue(false);
    spySendLetterConfigActions('getSendLetterConfiguration');
    const updateSendLetterConfigRequest = spySendLetterConfigActions(
      'updateSendLetterConfigRequest'
    );

    const _initialState = cloneDeep(initialState);
    _initialState.sendLetterConfig.sendLetterConfigurationList.data = [];

    renderMockStore(<LetterConfigGrid />, { initialState: _initialState });

    expect(
      screen.getByText(I18N_ADJUSTMENT_TRANSACTION.NO_DATA)
    ).toBeInTheDocument();

    fireEvent.click(screen.getByText('txt_letter_config_add_title'));
    expect(updateSendLetterConfigRequest).toHaveBeenCalledWith({
      actionType: 'add'
    });
  });
});
