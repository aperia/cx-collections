// services
import { sendLetterConfigService } from './sendLetterService';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi } from 'app/test-utils/mocks/mockProperty';
import { CallFrequencyConfigData } from './types';

describe('Test on sendLetterConfigService', () => {
  const mockFrequencyPostData = {
    state: {
      description: 'mock description',
      value: 'mock value',
      countryCode: 'mock code'
    },
    homePhone: {
      times: 0,
      days: 0
    },
    otherPhone: {
      times: 0,
      days: 0
    }
  } as CallFrequencyConfigData;

  it('should call getFrequencyConfiguration', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    sendLetterConfigService.getFrequencyConfiguration();

    expect(mockService).toHaveBeenCalled();
  });

  it('should call addFrequencyConfiguration', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    sendLetterConfigService.addFrequencyConfiguration(mockFrequencyPostData);

    expect(mockService).toHaveBeenCalledWith('', {
      data: mockFrequencyPostData
    });
  });

  it('should call deleteFrequencyConfiguration', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    sendLetterConfigService.deleteFrequencyConfiguration(mockFrequencyPostData);

    expect(mockService).toHaveBeenCalledWith('', {
      data: mockFrequencyPostData
    });
  });

  it('should call editFrequencyConfiguration', () => {
    mockAppConfigApi.clear();

    const mockService = mockApiServices.post();

    sendLetterConfigService.editFrequencyConfiguration({
      prevRecord: mockFrequencyPostData,
      newRecord: mockFrequencyPostData
    });

    expect(mockService).toHaveBeenCalledWith('', {
      data: {
        prevRecord: mockFrequencyPostData,
        newRecord: mockFrequencyPostData
      }
    });
  });
});
