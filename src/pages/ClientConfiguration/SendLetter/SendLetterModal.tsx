import React, { useRef, useState } from 'react';

// components
import {
  Button,
  InlineMessage,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { View } from 'app/_libraries/_dof/core';
import ModalBodyWithErrorClientConfig from '../ApiErrorSection/ModalBodyWithErrorClientConfig';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { sendLetterConfigActions } from './_redux/reducers';
import {
  selectedSendLetterModalLoading,
  selectFormData,
  selectLetterConfigurationData
} from './_redux/selectors';

// constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { I18N_SEND_LETTER, VIEW_NAME } from './constants';

// types
import { Field, isInvalid } from 'redux-form';
import { ExtraFieldProps } from 'app/_libraries/_dof/core';
import { DropDownControlProps } from 'app/components/_dof_controls/controls';
import { SendLetterConfigData, SendLetterModalType } from './types';

// helpers
import findIndex from 'lodash.findindex';
import { useIsDirtyForm } from 'app/hooks';

export interface MessageErrorType {
  state?: string | undefined;
  letterNumber?: string | undefined;
}

export interface SendLetterModal extends SendLetterModalType {
  isShowModal?: boolean;
}

const SendLetterModal: React.FC<SendLetterModal> = ({
  actionType,
  selectedItem,
  isShowModal = false
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const testId = 'clientConfig_sendLetter_modal';

  const isNotChangeData = useIsDirtyForm(['send_letter_config_client']);

  const isDisabledButtonSubmit = actionType === 'edit' && !isNotChangeData;

  const dynamicProps: MagicKeyValue = {
    add: {
      title: t(I18N_SEND_LETTER.ADD_TITLE),
      variant: 'primary',
      footer: t(I18N_COMMON_TEXT.SUBMIT)
    },
    edit: {
      title: t(I18N_SEND_LETTER.EDIT_TITLE),
      variant: 'primary',
      footer: t(I18N_COMMON_TEXT.SAVE)
    },
    delete: {
      title: t(I18N_SEND_LETTER.DELETE_TITLE),
      footer: t(I18N_COMMON_TEXT.DELETE),
      variant: 'danger',
      content: t(I18N_SEND_LETTER.DELETE_MODAL_CONTENT)
    }
  };

  const loading = useSelector(selectedSendLetterModalLoading);
  const data = useSelector(selectLetterConfigurationData);
  const formData = useSelector(selectFormData);

  const [isMessage, setIsMessage] = useState<boolean>(false);
  const viewRef = useRef<any>(null);

  const disabledOk = useSelector<RootState, boolean>(isInvalid(VIEW_NAME));

  const handleCancelModal = () => {
    dispatch(sendLetterConfigActions.cancelUpdateSendLetterConfig());
    setIsMessage(false);
  };

  const handleSubmit = () => {
    // check the item in the list already exists and is different from the selected item to edit
    const index = findIndex(
      data,
      item =>
        item?.state?.value === formData?.state?.value &&
        item?.state?.value !== selectedItem?.state?.value
    );

    if (index !== -1) {
      setIsMessage(true);

      const stateField: Field<ExtraFieldProps> =
        viewRef?.current?.props?.onFind('state');

      stateField.props.props.setOthers((prev: DropDownControlProps) => ({
        ...prev,
        options: {
          ...prev.options,
          isError: true
        }
      }));
    } else {
      setIsMessage(false);

      dispatch(
        sendLetterConfigActions.updateSendLetterConfig({
          data:
            actionType === 'delete'
              ? selectedItem!
              : (formData as SendLetterConfigData),
          actionType
        })
      );
    }
  };

  const isDeleteContent = actionType === 'delete';

  const renderDeleteBody = () => (
    <>
      <p className="fs-14">{t(dynamicProps[actionType].content)}</p>
      <p className="fs-14 mt-16">
        {`${t(I18N_SEND_LETTER.STATE)}:`}{' '}
        <b>
          {`${selectedItem?.state?.value}`} -{' '}
          {`${selectedItem?.state?.description}`}
        </b>
      </p>
    </>
  );

  return (
    <>
      <Modal
        xs={isDeleteContent}
        sm={!isDeleteContent}
        show={isShowModal}
        loading={loading}
        dataTestId={testId}
      >
        <ModalHeader border closeButton onHide={handleCancelModal} dataTestId={`${testId}_header`}>
          <ModalTitle dataTestId={`${testId}_title`}>{dynamicProps[actionType].title}</ModalTitle>
        </ModalHeader>
        <ModalBodyWithErrorClientConfig apiErrorClassName="mb-24" dataTestId={`${testId}_body`}>
          {isMessage && (
            <InlineMessage className="mb-24" variant="danger" dataTestId={`${testId}_errorMessage`}>
              {t(I18N_SEND_LETTER.STATE_EXIST_ERROR)}
            </InlineMessage>
          )}
          {isDeleteContent ? (
            renderDeleteBody()
          ) : (
            <View
              id="send_letter_config_client"
              formKey="send_letter_config_client"
              descriptor="sendLetterConfigFormView"
              ref={viewRef}
              value={selectedItem}
            />
          )}
        </ModalBodyWithErrorClientConfig>
        <ModalFooter dataTestId={`${testId}_footer`}>
          <div className="d-flex align-items-right justify-content-end">
            <Button
              onClick={handleCancelModal}
              variant="secondary"
              dataTestId={`${testId}_cancelBtn`}
            >
              {t(I18N_COMMON_TEXT.CANCEL)}
            </Button>
            <Button
              disabled={disabledOk || isDisabledButtonSubmit}
              onClick={handleSubmit}
              variant={dynamicProps[actionType].variant}
              dataTestId={`${testId}_okBtn`}
            >
              {dynamicProps[actionType].footer}
            </Button>
          </div>
        </ModalFooter>
      </Modal>
    </>
  );
};

export default SendLetterModal;
