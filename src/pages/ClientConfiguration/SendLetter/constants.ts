export const SEND_LETTER_GRID_COLUMN_NAME = {
  LETTER_STATE: 'stateValue',
  LETTER_NUMBER: 'letterNumber'
};

export const TYPE_FORM = {
  ADD: 'add',
  EDIT: 'edit'
};

export const FORM_FIELD = {
  ID: {
    name: 'state',
    label: 'txt_letter_config_id'
  },
  DESCRIPTION: {
    name: 'letterNumber',
    label: 'txt_letter_config_description',
    maxLength: 50
  }
};

export const VIEW_NAME = 'send_letter_config_client';

export const FREQUENCY_VIEW_NAME = 'call_frequency_config_client';

export const I18N_SEND_LETTER = {
  ADD_TITLE: 'txt_letter_config_add_title',
  EDIT_TITLE: 'txt_letter_config_edit_title',
  DELETE_TITLE: 'txt_send_letter_config_delete_confirm',
  DELETE_MODAL_CONTENT: 'txt_send_letter_config_delete_body',
  STATE: 'txt_send_letter_config_state',
  STATE_EXIST_ERROR: 'txt_send_letter_config_inline_message',
  LETTER_ADD_SUCCESS: 'txt_letter_config_add_success',
  LETTER_ADD_FAIL: 'txt_letter_config_add_fail',
  LETTER_EDIT_SUCCESS: 'txt_letter_config_edit_success',
  LETTER_EDIT_FAIL: 'txt_letter_config_edit_fail',
  LETTER_DELETE_SUCCESS: 'txt_letter_config_delete_success',
  LETTER_DELETE_FAIL: 'txt_letter_config_delete_fail'
};
