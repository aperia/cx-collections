import { createStore, Store } from '@reduxjs/toolkit';
import { mockActionCreator } from 'app/test-utils';

import { rootReducer } from 'storeConfig';

import {
  formatPostData,
  getFulfilledMessage,
  getRejectedMessage
} from './helpers';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { updateSendLetterConfig } from './updateSendLetterConfig';
import {
  reducer,
  initialState,
  sendLetterConfigActions as actions
} from './reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

const spyToastActions = mockActionCreator(actionsToast);
const spySendLetterConfigActions = mockActionCreator(actions);
const spyClientConfigActions = mockActionCreator(clientConfigurationActions);
describe('CodeToText > redux getCodeToTextConfig', () => {
  let store: Store<RootState>;
  const mockData = {
    state: {
      description: 'description 1',
      value: 'value 1',
      countryCode: 'code 1'
    },
    letterNumber: 'letter number 1 change'
  };

  it('pending', () => {
    const nextState = reducer(
      initialState,
      updateSendLetterConfig.pending('updateSendLetterConfig', undefined as any)
    );
    expect(nextState.updateSendLetterConfig.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      initialState,
      updateSendLetterConfig.fulfilled(
        undefined as any,
        'updateSendLetterConfig',
        undefined as any
      )
    );
    expect(nextState.updateSendLetterConfig.loading).toEqual(false);
    expect(nextState.updateSendLetterConfig.error).toEqual('');
    expect(nextState.updateSendLetterConfig.openModal).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      initialState,
      updateSendLetterConfig.rejected(
        null,
        'updateSendLetterConfig',
        undefined as any
      )
    );
    expect(nextState.updateSendLetterConfig.loading).toEqual(false);
  });

  describe('add', () => {
    beforeEach(() => {
      store = createStore(rootReducer, {
        sendLetterConfig: initialState
      });
    });

    it('should add success', async () => {
      const addToast = spyToastActions('addToast');
      const cancelUpdateSendLetterConfig = spySendLetterConfigActions(
        'cancelUpdateSendLetterConfig'
      );
      const getSendLetterConfiguration = spySendLetterConfigActions(
        'getSendLetterConfiguration'
      );
      const saveCollectionsClientConfig = spyClientConfigActions(
        'saveCollectionsClientConfig'
      ).mockReturnValue({
        type: 'fulfilled',
        meta: { requestId: 'id', requestStatus: 'fulfilled' }
      });

      const result = await updateSendLetterConfig({
        data: mockData,
        actionType: 'add'
      })(
        (arg: any) => {
          if (arg.type) return Promise.resolve(arg);
        },
        store.getState,
        {}
      );

      expect(saveCollectionsClientConfig).toBeCalledWith({
        component: 'regulatory-config',
        jsonValue: {
          ['send-letter']: [formatPostData(mockData)]
        }
      });

      expect(result.type).toEqual(updateSendLetterConfig.fulfilled.type);
      expect(cancelUpdateSendLetterConfig).toBeCalled();
      expect(getSendLetterConfiguration).toBeCalled();
      expect(addToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: getFulfilledMessage('add')
      });
    });

    it('should add success without action type', async () => {
      const addToast = spyToastActions('addToast');
      const cancelUpdateSendLetterConfig = spySendLetterConfigActions(
        'cancelUpdateSendLetterConfig'
      );
      const getSendLetterConfiguration = spySendLetterConfigActions(
        'getSendLetterConfiguration'
      );
      const saveCollectionsClientConfig = spyClientConfigActions(
        'saveCollectionsClientConfig'
      ).mockReturnValue({
        type: 'fulfilled',
        meta: { requestId: 'id', requestStatus: 'fulfilled' }
      });

      const result = await updateSendLetterConfig({
        data: mockData,
        actionType: ''
      })(
        (arg: any) => {
          if (arg.type) return Promise.resolve(arg);
        },
        store.getState,
        {}
      );

      expect(saveCollectionsClientConfig).toBeCalledWith({
        component: 'regulatory-config',
        jsonValue: {
          ['send-letter']: []
        }
      });

      expect(result.type).toEqual(updateSendLetterConfig.fulfilled.type);
      expect(cancelUpdateSendLetterConfig).toBeCalled();
      expect(getSendLetterConfiguration).toBeCalled();
      expect(addToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: undefined
      });
    });

    it('should add fail', async () => {
      const addToast = spyToastActions('addToast');
      const saveCollectionsClientConfig = spyClientConfigActions(
        'saveCollectionsClientConfig'
      ).mockReturnValue({
        type: 'rejected',
        meta: { requestId: 'id', requestStatus: 'rejected' }
      });

      const result = await updateSendLetterConfig({
        data: undefined,
        actionType: 'add'
      } as any)(
        (arg: any) => {
          if (arg.type) return Promise.resolve(arg);
        },
        store.getState,
        {}
      );

      expect(saveCollectionsClientConfig).toBeCalledWith({
        component: 'regulatory-config',
        jsonValue: {
          ['send-letter']: [
            formatPostData({
              state: {
                description: '',
                value: '',
                countryCode: ''
              },
              letterNumber: ''
            })
          ]
        }
      });

      expect(result.type).toEqual(updateSendLetterConfig.rejected.type);
      expect(addToast).toBeCalledWith({
        show: true,
        type: 'error',
        message: getRejectedMessage('add')
      });
    });
  });

  describe('edit', () => {
    beforeEach(() => {
      store = createStore(rootReducer, {
        sendLetterConfig: {
          ...initialState,
          sendLetterConfigurationList: {
            data: [
              {
                state: {
                  description: 'description 1',
                  value: 'value 1',
                  countryCode: 'code 1'
                },
                letterNumber: 'letter number 1 change',
                stateValue: 'state 1 change'
              }
            ]
          }
        }
      });
    });

    it('should edit success', async () => {
      const addToast = spyToastActions('addToast');
      const cancelUpdateSendLetterConfig = spySendLetterConfigActions(
        'cancelUpdateSendLetterConfig'
      );
      const getSendLetterConfiguration = spySendLetterConfigActions(
        'getSendLetterConfiguration'
      );
      const saveCollectionsClientConfig = spyClientConfigActions(
        'saveCollectionsClientConfig'
      ).mockReturnValue({
        type: 'fulfilled',
        meta: { requestId: 'id', requestStatus: 'fulfilled' }
      });

      const result = await updateSendLetterConfig({
        data: mockData,
        actionType: 'edit'
      })(
        (arg: any) => {
          if (arg.type) return Promise.resolve(arg);
        },
        store.getState,
        {}
      );

      expect(saveCollectionsClientConfig).toBeCalledWith({
        component: 'regulatory-config',
        jsonValue: {
          ['send-letter']: [formatPostData(mockData)]
        }
      });

      expect(result.type).toEqual(updateSendLetterConfig.fulfilled.type);
      expect(cancelUpdateSendLetterConfig).toBeCalled();
      expect(getSendLetterConfiguration).toBeCalled();
      expect(addToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: getFulfilledMessage('edit')
      });
    });

    it('should edit fail when saveCollectionsClientConfig not rejected', async () => {
      const saveCollectionsClientConfig = spyClientConfigActions(
        'saveCollectionsClientConfig'
      ).mockReturnValue({
        type: 'rejected',
        meta: { requestStatus: 'rejected' }
      });

      const result = await updateSendLetterConfig({
        data: mockData,
        actionType: 'edit'
      } as any)(
        (arg: any) => {
          if (arg.type) return Promise.resolve(arg);
        },
        store.getState,
        {}
      );

      expect(saveCollectionsClientConfig).toBeCalledWith({
        component: 'regulatory-config',
        jsonValue: {
          ['send-letter']: [formatPostData(mockData)]
        }
      });

      expect(result.type).toEqual(updateSendLetterConfig.rejected.type);
    });

    it('should edit fail', async () => {
      const addToast = spyToastActions('addToast');
      const saveCollectionsClientConfig = spyClientConfigActions(
        'saveCollectionsClientConfig'
      ).mockReturnValue({
        type: 'rejected',
        meta: { requestId: 'id', requestStatus: 'rejected' }
      });

      const result = await updateSendLetterConfig({
        data: mockData,
        actionType: 'edit'
      } as any)(
        (arg: any) => {
          if (arg.type) return Promise.resolve(arg);
        },
        store.getState,
        {}
      );

      expect(saveCollectionsClientConfig).toBeCalledWith({
        component: 'regulatory-config',
        jsonValue: {
          ['send-letter']: [formatPostData(mockData)]
        }
      });

      expect(result.type).toEqual(updateSendLetterConfig.rejected.type);
      expect(addToast).toBeCalledWith({
        show: true,
        type: 'error',
        message: getRejectedMessage('edit')
      });
    });
  });

  describe('delete', () => {
    beforeEach(() => {
      store = createStore(rootReducer, {
        sendLetterConfig: {
          ...initialState,
          sendLetterConfigurationList: {
            data: [
              {
                state: {
                  description: 'description 1',
                  value: 'value 1',
                  countryCode: 'code 1'
                },
                letterNumber: 'letter number 1 change',
                stateValue: 'state 1 change'
              }
            ]
          }
        }
      });
    });

    it('should delete success', async () => {
      const addToast = spyToastActions('addToast');
      const cancelUpdateSendLetterConfig = spySendLetterConfigActions(
        'cancelUpdateSendLetterConfig'
      );
      const getSendLetterConfiguration = spySendLetterConfigActions(
        'getSendLetterConfiguration'
      );
      const saveCollectionsClientConfig = spyClientConfigActions(
        'saveCollectionsClientConfig'
      ).mockReturnValue({
        type: 'fulfilled',
        meta: { requestId: 'id', requestStatus: 'fulfilled' }
      });

      const result = await updateSendLetterConfig({
        data: mockData,
        actionType: 'delete'
      })(
        (arg: any) => {
          if (arg.type) return Promise.resolve(arg);
        },
        store.getState,
        {}
      );

      expect(saveCollectionsClientConfig).toBeCalledWith({
        component: 'regulatory-config',
        jsonValue: {
          ['send-letter']: []
        }
      });

      expect(result.type).toEqual(updateSendLetterConfig.fulfilled.type);
      expect(cancelUpdateSendLetterConfig).toBeCalled();
      expect(getSendLetterConfiguration).toBeCalled();
      expect(addToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: getFulfilledMessage('delete')
      });
    });

    it('should delete fail', async () => {
      const addToast = spyToastActions('addToast');
      const saveCollectionsClientConfig = spyClientConfigActions(
        'saveCollectionsClientConfig'
      ).mockReturnValue({
        type: 'rejected',
        meta: { requestId: 'id', requestStatus: 'rejected' }
      });

      const result = await updateSendLetterConfig({
        data: mockData,
        actionType: 'delete'
      } as any)(
        (arg: any) => {
          if (arg.type) return Promise.resolve(arg);
        },
        store.getState,
        {}
      );

      expect(saveCollectionsClientConfig).toBeCalledWith({
        component: 'regulatory-config',
        jsonValue: {
          ['send-letter']: []
        }
      });

      expect(result.type).toEqual(updateSendLetterConfig.rejected.type);
      expect(addToast).toBeCalledWith({
        show: true,
        type: 'error',
        message: getRejectedMessage('delete')
      });
    });
  });
});
