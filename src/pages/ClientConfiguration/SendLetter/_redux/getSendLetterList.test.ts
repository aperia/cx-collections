import { createStore } from '@reduxjs/toolkit';
import { rootReducer } from 'storeConfig';
import { reducer, initialState } from './reducers';

import { mockActionCreator } from 'app/test-utils';
import { SendLetterConfigData, SendLetterState } from '../types';
import { getSendLetterConfiguration } from './getSendLetterList';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

const initReducer = {
  sendLetterConfigurationList: {
    loading: false,
    data: [] as SendLetterConfigData[],
    error: undefined
  }
} as SendLetterState;

const mockData: SendLetterConfigData[] = [
  {
    state: {
      description: 'description',
      value: 'value',
      countryCode: 'code'
    },
    letterNumber: 'letter number',
    stateValue: 'state'
  }
];

const spyClientConfigurationActions = mockActionCreator(clientConfigurationActions);
describe('Send letter redux  > getSendLetterConfiguration', () => {
  const store = createStore(rootReducer, {
    sendLetterConfig: initialState
  });
  it('should call thunk action successful, with data', async () => {
    const getCollectionsClientConfig = spyClientConfigurationActions('getCollectionsClientConfig')
      .mockReturnValue({
        payload: { 'send-letter': mockData },
        type: 'fulfilled',
        meta: { requestId: 'id', requestStatus: 'fulfilled' }
      });

    const response = await getSendLetterConfiguration()(
      (arg: any) => { if (arg.type) return Promise.resolve(arg) },
      store.getState,
      {}
    );

    const mockFormatData = mockData.map((item: SendLetterConfigData) => {
      return {
        ...item,
        stateValue: item?.state?.value
      };
    });

    expect(response.payload).toEqual({ data: mockFormatData });
    expect(getCollectionsClientConfig).toHaveBeenCalled();
  });
  it('should call thunk action successful, with empty data', async () => {
    const getCollectionsClientConfig = spyClientConfigurationActions('getCollectionsClientConfig')
      .mockReturnValue({
        payload: { },
        type: 'fulfilled',
        meta: { requestId: 'id', requestStatus: 'fulfilled' }
      });

    const response = await getSendLetterConfiguration()(
      (arg: any) => { if (arg.type) return Promise.resolve(arg) },
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ data: [] });
    expect(getCollectionsClientConfig).toHaveBeenCalled();
  });
  it('should call thunk action failure', async () => {
    const getCollectionsClientConfig = spyClientConfigurationActions('getCollectionsClientConfig')
      .mockReturnValue({});

    const response = await getSendLetterConfiguration()(
      (arg: any) => { if (arg.type) return Promise.resolve(arg) },
      store.getState,
      {}
    );

    expect(response.type).toEqual(getSendLetterConfiguration.rejected.type);
    expect(getCollectionsClientConfig).toHaveBeenCalled();
  });

  describe('test getSendLetterConfigurationBuilder', () => {
    it('pending', () => {
      const nextState = reducer(
        initReducer,
        getSendLetterConfiguration.pending(
          getSendLetterConfiguration.pending.type,
          undefined
        )
      );

      expect(nextState.sendLetterConfigurationList.loading).toEqual(true);
    });

    it('fulfilled', () => {
      const nextState = reducer(
        initReducer,
        getSendLetterConfiguration.fulfilled(
          { data: mockData },
          getSendLetterConfiguration.fulfilled.type,
          undefined
        )
      );

      expect(nextState.sendLetterConfigurationList.loading).toEqual(false);
      expect(nextState.sendLetterConfigurationList.data).toEqual(mockData);
      expect(nextState.sendLetterConfigurationList.error).toEqual('');
    });

    it('rejected', () => {
      const nextState = reducer(
        initReducer,
        getSendLetterConfiguration.rejected(
          new Error('mock message'),
          getSendLetterConfiguration.rejected.type,
          undefined
        )
      );

      expect(nextState.sendLetterConfigurationList.loading).toEqual(false);
      expect(nextState.sendLetterConfigurationList.error).toEqual(
        'mock message'
      );
    });
  });
});
