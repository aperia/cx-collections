import { createSelector } from '@reduxjs/toolkit';

// types
import { SortType } from 'app/_libraries/_dls/components';
import { SendLetterConfigData } from '../types';

// helpers
import orderBy from 'lodash.orderby';

// constants
import { FREQUENCY_VIEW_NAME, VIEW_NAME } from '../constants';

const getFormData = (state: RootState) => {
  return state.form[VIEW_NAME]?.values;
};

const getSortBy = (states: RootState): SortType => {
  return states.sendLetterConfig.sortBy;
};

export const selectFormData = createSelector(getFormData, data => data);

const getLetterConfigurationList = (state: RootState) => {
  return state.sendLetterConfig.sendLetterConfigurationList;
};

export const selectLetterConfigurationLoading = createSelector(
  getLetterConfigurationList,
  data => data?.loading || false
);

export const selectLetterConfigurationData = createSelector(
  [getLetterConfigurationList, getSortBy],
  (data, sortBy) => {
    return orderBy(
      data.data,
      [
        item => (item[sortBy.id as keyof SendLetterConfigData] as string)?.toLowerCase(),
        item => item.state?.value?.toLowerCase()
      ],
      [sortBy.order || false, 'asc']
    );
  }
);

export const selectLetterConfigurationError = createSelector(
  getLetterConfigurationList,
  data => data?.error || ''
);

export const getSendLetterConfig = (state: RootState) => {
  return state.sendLetterConfig?.updateSendLetterConfig || {};
};

export const selectSendLetterModalOpen = createSelector(
  getSendLetterConfig,
  data => data?.openModal || false
);

export const selectedActionType = createSelector(
  getSendLetterConfig,
  data => data?.actionType || ''
);

export const selectedItemModal = createSelector(
  getSendLetterConfig,
  data => data?.selectedItem || {}
);

export const selectedSendLetterModalLoading = createSelector(
  getSendLetterConfig,
  data => data?.loading || false
);

// call frequency

const getFormDataFrequency = (state: RootState) => {
  return state.form[FREQUENCY_VIEW_NAME]?.values;
};

export const selectFormDataFrequency = createSelector(
  getFormDataFrequency,
  data => data
);

const getFrequencyConfigurationList = (state: RootState) => {
  return state.sendLetterConfig.callFrequencyConfigurationList;
};

export const selectFrequencyConfigurationLoading = createSelector(
  getFrequencyConfigurationList,
  data => data.loading
);

export const selectFrequencyConfigurationData = createSelector(
  getFrequencyConfigurationList,
  data => data.data
);

export const selectFrequencyConfigurationError = createSelector(
  getFrequencyConfigurationList,
  data => data.error
);

export const getDeleteFrequencyConfiguration = (state: RootState) => {
  return state.sendLetterConfig.deleteCallFrequencyConfiguration;
};

export const selectDeleteFrequencyModalOpen = createSelector(
  getDeleteFrequencyConfiguration,
  data => data.openModal
);

export const selectDeleteFrequencyLoading = createSelector(
  getDeleteFrequencyConfiguration,
  data => data.loading
);

export const selectDeleteFrequencyRecord = createSelector(
  getDeleteFrequencyConfiguration,
  data => data.currentRecord
);

const getAddFrequencyConfiguration = (state: RootState) => {
  return state.sendLetterConfig.addCallFrequencyConfiguration;
};

export const selectAddFrequencyModalOpen = createSelector(
  getAddFrequencyConfiguration,
  data => data.openModal
);

export const selectAddFrequencyLoadingModalOpen = createSelector(
  getAddFrequencyConfiguration,
  data => data.loading
);

const getEditFrequencyConfiguration = (state: RootState) => {
  return state.sendLetterConfig.editCallFrequencyConfiguration;
};

export const selectEditFrequencyModalOpen = createSelector(
  getEditFrequencyConfiguration,
  data => data.openModal
);

export const selectEditFrequencyLoadingModalOpen = createSelector(
  getEditFrequencyConfiguration,
  data => data.loading
);

export const selectEditFrequencyCurrentRecord = createSelector(
  getEditFrequencyConfiguration,
  data => data.currentRecord
);
