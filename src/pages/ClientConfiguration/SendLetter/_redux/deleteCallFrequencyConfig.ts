import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { AxiosResponse } from 'axios';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';

import { sendLetterConfigService } from '../sendLetterService';
import { SendLetterState } from '../types';
import { sendLetterConfigActions } from './reducers';

export const deleteFrequencyConfiguration = createAsyncThunk<
  AxiosResponse,
  undefined,
  ThunkAPIConfig
>('deleteFrequencyConfiguration', async (_, { getState, rejectWithValue }) => {
  const state = getState();
  const requestBody =
    state.sendLetterConfig.deleteCallFrequencyConfiguration.currentRecord || {};
  return await sendLetterConfigService.deleteFrequencyConfiguration(
    requestBody
  );
});

export const triggerDeleteFrequencyConfiguration = createAsyncThunk<
  unknown,
  undefined,
  ThunkAPIConfig
>('triggerDeleteLetterConfiguration', async (_, thunkAPI) => {
  const { dispatch } = thunkAPI;
  const response = await dispatch(deleteFrequencyConfiguration());

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'txt_frequency_config_delete_success'
        })
      );
      dispatch(sendLetterConfigActions.getFrequencyConfiguration());
    });
  }

  if (isRejected(response)) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: 'txt_frequency_config_delete_fail'
      })
    );
  }
});

export const deleteFrequencyConfigurationBuilder = (
  builder: ActionReducerMapBuilder<SendLetterState>
) => {
  builder
    .addCase(deleteFrequencyConfiguration.pending, (draftState, action) => {
      draftState.deleteCallFrequencyConfiguration = {
        ...draftState.deleteCallFrequencyConfiguration,
        loading: true
      };
    })
    .addCase(deleteFrequencyConfiguration.fulfilled, (draftState, action) => {
      draftState.deleteCallFrequencyConfiguration = {
        ...draftState.deleteCallFrequencyConfiguration,
        loading: false,
        error: '',
        openModal: false
      };
    })
    .addCase(deleteFrequencyConfiguration.rejected, (draftState, action) => {
      draftState.deleteCallFrequencyConfiguration = {
        ...draftState.deleteCallFrequencyConfiguration,
        loading: false,
        error: action.error.message
      };
    });
};
