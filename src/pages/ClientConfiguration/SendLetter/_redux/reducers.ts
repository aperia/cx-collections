import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// sendLetter
import {
  getSendLetterConfiguration,
  getSendLetterConfigurationBuilder
} from './getSendLetterList';
import {
  updateSendLetterConfig,
  updateSendLetterConfigBuilder
} from './updateSendLetterConfig';

// frequency
import {
  getFrequencyConfiguration,
  getFrequencyConfigurationBuilder
} from './getCallFrequencyList';
import {
  addFrequencyConfiguration,
  triggerAddFrequencyConfiguration,
  addFrequencyConfigurationBuilder
} from './addCallFrequencyConfig';
import {
  deleteFrequencyConfiguration,
  triggerDeleteFrequencyConfiguration,
  deleteFrequencyConfigurationBuilder
} from './deleteCallFrequencyConfig';
import {
  triggerEditFrequencyConfiguration,
  editFrequencyConfiguration,
  editFrequencyConfigurationBuilder
} from './editCallFrequencyConfig';

// types
import { SortType } from 'app/_libraries/_dls/components';
import {
  CallFrequencyConfigData,
  SendLetterState,
  SendLetterModalType
} from '../types';

// constants
import { SEND_LETTER_GRID_COLUMN_NAME } from '../constants';

// helpers
import { formatEditData } from './helpers';

export const initialState: SendLetterState = {
  sortBy: { id: SEND_LETTER_GRID_COLUMN_NAME.LETTER_STATE, order: undefined },
  sendLetterConfigurationList: { loading: false, data: [], error: '' },
  updateSendLetterConfig: {
    openModal: false,
    loading: false,
    actionType: ''
  },
  callFrequencyConfigurationList: { loading: false, data: [], error: '' },
  addCallFrequencyConfiguration: {
    loading: false,
    openModal: false
  },
  deleteCallFrequencyConfiguration: {
    openModal: false,
    loading: false
  },
  editCallFrequencyConfiguration: {
    openModal: false,
    loading: false
  }
};

const { actions, reducer } = createSlice({
  name: 'sendLetterConfiguration',
  initialState,
  reducers: {
    // sendLetter
    updateSendLetterConfigRequest: (
      draftState,
      action: PayloadAction<SendLetterModalType>
    ) => {
      const { actionType, selectedItem } = action.payload;
      // format to mapping value and desc to show on the state dropdown for edit case
      const formattedEditData = selectedItem
        ? formatEditData(selectedItem)
        : selectedItem;

      draftState.updateSendLetterConfig = {
        ...draftState.updateSendLetterConfig,
        openModal: true,
        actionType: actionType || '',
        selectedItem: formattedEditData || {}
      };
    },
    cancelUpdateSendLetterConfig: draftState => {
      draftState.updateSendLetterConfig = {
        ...draftState.updateSendLetterConfig,
        openModal: false,
        actionType: '',
        selectedItem: {}
      };
    },

    // call frequency
    cancelDeleteFrequencyConfig: draftState => {
      draftState.deleteCallFrequencyConfiguration = {
        ...draftState.deleteCallFrequencyConfiguration,
        openModal: false,
        currentRecord: undefined
      };
    },
    deleteFrequencyConfigRequest: (draftState, action) => {
      draftState.deleteCallFrequencyConfiguration = {
        ...draftState.deleteCallFrequencyConfiguration,
        openModal: true,
        currentRecord: action.payload
      };
    },
    addFrequencyConfigRequest: draftState => {
      draftState.addCallFrequencyConfiguration = {
        ...draftState.addCallFrequencyConfiguration,
        openModal: true
      };
    },
    cancelAddFrequencyConfig: draftState => {
      draftState.addCallFrequencyConfiguration = {
        ...draftState.addCallFrequencyConfiguration,
        openModal: false
      };
    },
    editFrequencyConfigRequest: (draftState, action) => {
      draftState.editCallFrequencyConfiguration = {
        ...draftState.editCallFrequencyConfiguration,
        openModal: true,
        currentRecord: formatEditData(action.payload) as CallFrequencyConfigData
      };
    },
    cancelEditFrequencyConfig: draftState => {
      draftState.editCallFrequencyConfiguration = {
        ...draftState.editCallFrequencyConfiguration,
        openModal: false
      };
    },
    onChangeSort: (draftState, action: PayloadAction<SortType>) => {
      draftState.sortBy = action.payload;
    }
  },
  extraReducers: builder => {
    getSendLetterConfigurationBuilder(builder);
    getFrequencyConfigurationBuilder(builder);
    addFrequencyConfigurationBuilder(builder);
    deleteFrequencyConfigurationBuilder(builder);
    editFrequencyConfigurationBuilder(builder);
    updateSendLetterConfigBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getSendLetterConfiguration,
  getFrequencyConfiguration,
  addFrequencyConfiguration,
  deleteFrequencyConfiguration,
  triggerAddFrequencyConfiguration,
  triggerDeleteFrequencyConfiguration,
  triggerEditFrequencyConfiguration,
  editFrequencyConfiguration,
  updateSendLetterConfig
};

export { allActions as sendLetterConfigActions, reducer };
