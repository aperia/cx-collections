import { I18N_SEND_LETTER } from './../constants';

// types
import { ActionType } from './../types';
import { SendLetterConfigData } from '../types';

export const formatPostData = (data: SendLetterConfigData) => {
  return {
    ...data,
    state: {
      ...data?.state,
      description: data?.state?.description?.split(' - ')[1]
    }
  };
};

export const formatEditData = (data: SendLetterConfigData) => {
  return {
    ...data,
    state: {
      ...data?.state,
      description: `${data?.state?.value} - ${data?.state?.description}`
    }
  };
};

export const getFulfilledMessage = (type: ActionType) => {
  switch (type) {
    case 'add':
      return I18N_SEND_LETTER.LETTER_ADD_SUCCESS;
    case 'delete':
      return I18N_SEND_LETTER.LETTER_DELETE_SUCCESS;
    case 'edit':
      return I18N_SEND_LETTER.LETTER_EDIT_SUCCESS;
  }
};

export const getRejectedMessage = (type: ActionType) => {
  switch (type) {
    case 'add':
      return I18N_SEND_LETTER.LETTER_ADD_FAIL;
    case 'delete':
      return I18N_SEND_LETTER.LETTER_DELETE_FAIL;
    case 'edit':
      return I18N_SEND_LETTER.LETTER_EDIT_FAIL;
  }
};
