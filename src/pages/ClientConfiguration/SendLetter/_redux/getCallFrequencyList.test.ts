import { responseDefault } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { sendLetterConfigService } from '../sendLetterService';
import { getFrequencyConfiguration } from './getCallFrequencyList';

describe('Test getFrequencyConfiguration', () => {
  it('getFrequencyConfiguration > fulfilled', async () => {
    const store = createStore(rootReducer, {});

    jest
      .spyOn(sendLetterConfigService, 'getFrequencyConfiguration')
      .mockResolvedValue({ ...responseDefault });

    const result = await getFrequencyConfiguration()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(getFrequencyConfiguration.fulfilled.type);
  });

  it('getFrequencyConfiguration > rejected', async () => {
    const store = createStore(rootReducer, {});

    jest
      .spyOn(sendLetterConfigService, 'getFrequencyConfiguration')
      .mockRejectedValue({ ...responseDefault });

    const result = await getFrequencyConfiguration()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(getFrequencyConfiguration.rejected.type);
  });
});
