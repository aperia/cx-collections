import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { sendLetterConfigService } from '../sendLetterService';
import { CallFrequencyConfigData, SendLetterState } from '../types';

export const getFrequencyConfiguration = createAsyncThunk<
  CallFrequencyConfigData[],
  undefined,
  ThunkAPIConfig
>('getFrequencyConfiguration', async (args, thunkAPI) => {
  const { data } = await sendLetterConfigService.getFrequencyConfiguration();

  return data;
});

export const getFrequencyConfigurationBuilder = (
  builder: ActionReducerMapBuilder<SendLetterState>
) => {
  builder
    .addCase(getFrequencyConfiguration.pending, draftState => {
      draftState.callFrequencyConfigurationList = {
        ...draftState.callFrequencyConfigurationList,
        loading: true,
        data: []
      };
    })
    .addCase(getFrequencyConfiguration.fulfilled, (draftState, action) => {
      draftState.callFrequencyConfigurationList = {
        ...draftState.callFrequencyConfigurationList,
        loading: false,
        data: action.payload,
        error: ''
      };
    })
    .addCase(getFrequencyConfiguration.rejected, (draftState, action) => {
      draftState.callFrequencyConfigurationList = {
        ...draftState.callFrequencyConfigurationList,
        loading: false,
        error: action.error.message
      };
    });
};
