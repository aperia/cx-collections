import { selectorWrapper } from 'app/test-utils';
import {
  FREQUENCY_VIEW_NAME,
  SEND_LETTER_GRID_COLUMN_NAME,
  VIEW_NAME
} from '../constants';
import * as selectors from './selectors';

const states: Partial<RootState> = {
  sendLetterConfig: {
    sortBy: { id: SEND_LETTER_GRID_COLUMN_NAME.LETTER_STATE, order: 'desc' },
    sendLetterConfigurationList: {
      loading: true,
      data: [{ stateValue: 'A' }, { stateValue: 'B' }],
      error: 'Error'
    },
    updateSendLetterConfig: {
      openModal: false,
      loading: false,
      actionType: '',
      selectedItem: {
        letterNumber: '123'
      }
    },
    callFrequencyConfigurationList: {
      loading: true,
      data: [
        {
          state: {
            description: 'des',
            value: 'val',
            countryCode: 'count'
          }
        }
      ],
      error: 'Error'
    },
    addCallFrequencyConfiguration: {
      loading: true,
      openModal: true
    },
    deleteCallFrequencyConfiguration: {
      openModal: true,
      loading: true,
      currentRecord: {
        state: {
          description: 'des',
          value: 'val',
          countryCode: 'count'
        }
      }
    },
    editCallFrequencyConfiguration: {
      openModal: true,
      loading: true,
      currentRecord: {
        state: {
          description: 'des',
          value: 'val',
          countryCode: 'count'
        }
      }
    }
  },
  form: {
    [VIEW_NAME]: {
      values: {
        test: true
      },
      registeredFields: []
    },
    [FREQUENCY_VIEW_NAME]: {
      values: {
        test: true
      },
      registeredFields: []
    }
  }
};

describe('Test ClientConfiguration SendLetter selectors', () => {
  it('selectFormData', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selectors.selectFormData
    );

    expect(data).toEqual({ test: true });
    expect(emptyData).toEqual(undefined);
  });

  it('selectLetterConfigurationLoading', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selectors.selectLetterConfigurationLoading
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectLetterConfigurationData', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selectors.selectLetterConfigurationData
    );

    expect(data).toEqual([{ stateValue: 'B' }, { stateValue: 'A' }]);
    expect(emptyData).toEqual([]);
  });

  it('selectLetterConfigurationError', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selectors.selectLetterConfigurationError
    );

    expect(data).toEqual('Error');
    expect(emptyData).toEqual('');
  });

  it('selectFormDataFrequency', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selectors.selectFormDataFrequency
    );

    expect(data).toEqual({ test: true });
    expect(emptyData).toEqual(undefined);
  });

  it('selectFrequencyConfigurationLoading', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selectors.selectFrequencyConfigurationLoading
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectFrequencyConfigurationData', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selectors.selectFrequencyConfigurationData
    );

    expect(data).toEqual([
      {
        state: {
          description: 'des',
          value: 'val',
          countryCode: 'count'
        }
      }
    ]);
    expect(emptyData).toEqual([]);
  });

  it('selectFrequencyConfigurationError', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selectors.selectFrequencyConfigurationError
    );

    expect(data).toEqual('Error');
    expect(emptyData).toEqual('');
  });

  it('selectDeleteFrequencyModalOpen', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selectors.selectDeleteFrequencyModalOpen
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectDeleteFrequencyLoading', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selectors.selectDeleteFrequencyLoading
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectDeleteFrequencyRecord', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selectors.selectDeleteFrequencyRecord
    );

    expect(data).toEqual({
      state: {
        description: 'des',
        value: 'val',
        countryCode: 'count'
      }
    });
    expect(emptyData).toEqual(undefined);
  });

  it('selectAddFrequencyModalOpen', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selectors.selectAddFrequencyModalOpen
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectAddFrequencyLoadingModalOpen', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selectors.selectAddFrequencyLoadingModalOpen
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectEditFrequencyModalOpen', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selectors.selectEditFrequencyModalOpen
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectEditFrequencyLoadingModalOpen', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selectors.selectEditFrequencyLoadingModalOpen
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectEditFrequencyCurrentRecord', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selectors.selectEditFrequencyCurrentRecord
    );

    expect(data).toEqual({
      state: {
        description: 'des',
        value: 'val',
        countryCode: 'count'
      }
    });
    expect(emptyData).toEqual(undefined);
  });

  it('getSendLetterConfig ', () => {
    const { data } = selectorWrapper(states)(selectors.getSendLetterConfig);

    expect(data).toEqual({
      actionType: '',
      loading: false,
      openModal: false,
      selectedItem: { letterNumber: '123' }
    });
  });

  it('getSendLetterConfig with empty data', () => {
    const { data } = selectorWrapper({
      sendLetterConfig: {
        updateSendLetterConfig: null
      }
    })(selectors.getSendLetterConfig);

    expect(data).toEqual({});
  });

  it('selectSendLetterModalOpen', () => {
    const { data } = selectorWrapper(states)(
      selectors.selectSendLetterModalOpen
    );

    expect(data).toBeFalsy();
  });

  it('selectedActionType', () => {
    const { data } = selectorWrapper(states)(selectors.selectedActionType);

    expect(data).toEqual('');
  });

  it('selectedItemModal', () => {
    const { data } = selectorWrapper(states)(selectors.selectedItemModal);

    expect(data).toEqual({
      letterNumber: '123'
    });
  });

  it('selectedSendLetterModalLoading', () => {
    const { data } = selectorWrapper(states)(
      selectors.selectedSendLetterModalLoading
    );

    expect(data).toBeFalsy();
  });
});
