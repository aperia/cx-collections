import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

// helpers
import orderBy from 'lodash.orderby';

// types
import { SendLetterState, SendLetterConfigData } from '../types';

export const getSendLetterConfiguration = createAsyncThunk<
  any,
  undefined,
  ThunkAPIConfig
>('clientConfigRegulatory/getLetterConfiguration', async (args, thunkAPI) => {
  const { dispatch, rejectWithValue } = thunkAPI;

  const response = await dispatch(
    clientConfigurationActions.getCollectionsClientConfig({
      component: 'regulatory-config'
    })
  );

  if (isFulfilled(response)) {
    const data =
      (response.payload as Record<string, SendLetterConfigData[]>)[
        'send-letter'
      ] || [];

    const mappedData = data.map((item: SendLetterConfigData) => ({
      ...item,
      stateValue: item?.state?.value
    }));

    return {
      data: orderBy(mappedData, item => item.state?.value, 'asc')
    };
  }

  return rejectWithValue({});
});

export const getSendLetterConfigurationBuilder = (
  builder: ActionReducerMapBuilder<SendLetterState>
) => {
  builder
    .addCase(getSendLetterConfiguration.pending, draftState => {
      draftState.sendLetterConfigurationList = {
        ...draftState.sendLetterConfigurationList,
        loading: true,
        data: []
      };
    })
    .addCase(getSendLetterConfiguration.fulfilled, (draftState, action) => {
      const { data } = action.payload;

      draftState.sendLetterConfigurationList = {
        ...draftState.sendLetterConfigurationList,
        loading: false,
        data,
        error: ''
      };
    })
    .addCase(getSendLetterConfiguration.rejected, (draftState, action) => {
      draftState.sendLetterConfigurationList = {
        ...draftState.sendLetterConfigurationList,
        loading: false,
        error: action.error.message
      };
    });
};
