import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';

import { sendLetterConfigService } from '../sendLetterService';
import { SendLetterState } from '../types';
import { formatPostData } from './helpers';
import { sendLetterConfigActions } from './reducers';

export const addFrequencyConfiguration = createAsyncThunk<
  undefined,
  MagicKeyValue,
  ThunkAPIConfig
>('addFrequencyConfiguration', async (args, thunkAPI) => {
  const { postData } = args;
  const { data } = await sendLetterConfigService.addFrequencyConfiguration(
    postData
  );
  return data;
});

export const triggerAddFrequencyConfiguration = createAsyncThunk<
  unknown,
  MagicKeyValue,
  ThunkAPIConfig
>('triggerAddFrequencyConfiguration', async (args, thunkAPI) => {
  const { dispatch } = thunkAPI;
  const { postData } = args;
  const format = formatPostData(postData);
  const response = await dispatch(
    addFrequencyConfiguration({ postData: format })
  );

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'txt_frequency_config_add_success'
        })
      );
      dispatch(sendLetterConfigActions.getFrequencyConfiguration());
    });
  }

  if (isRejected(response)) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: 'txt_frequency_config_add_fail'
      })
    );
  }
});

export const addFrequencyConfigurationBuilder = (
  builder: ActionReducerMapBuilder<SendLetterState>
) => {
  builder
    .addCase(addFrequencyConfiguration.pending, (draftState, action) => {
      draftState.addCallFrequencyConfiguration = {
        ...draftState.addCallFrequencyConfiguration,
        loading: true
      };
    })
    .addCase(addFrequencyConfiguration.fulfilled, (draftState, action) => {
      draftState.addCallFrequencyConfiguration = {
        ...draftState.addCallFrequencyConfiguration,
        loading: false,
        error: '',
        openModal: false
      };
    })
    .addCase(addFrequencyConfiguration.rejected, (draftState, action) => {
      draftState.addCallFrequencyConfiguration = {
        ...draftState.addCallFrequencyConfiguration,
        loading: false,
        error: action.error.message
      };
    });
};
