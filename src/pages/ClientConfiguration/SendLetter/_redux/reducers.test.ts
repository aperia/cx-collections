import { SEND_LETTER_GRID_COLUMN_NAME } from '../constants';
import { SendLetterState } from '../types';
import { formatEditData } from './helpers';
import { sendLetterConfigActions, reducer } from './reducers';

const initialState: SendLetterState = {
  sortBy: { id: SEND_LETTER_GRID_COLUMN_NAME.LETTER_STATE, order: undefined },
  sendLetterConfigurationList: { loading: false, data: [], error: '' },
  updateSendLetterConfig: {
    openModal: false,
    loading: false,
    actionType: ''
  },
  callFrequencyConfigurationList: { loading: false, data: [], error: '' },
  addCallFrequencyConfiguration: {
    loading: false,
    openModal: false
  },
  deleteCallFrequencyConfiguration: {
    openModal: false,
    loading: false
  },
  editCallFrequencyConfiguration: {
    openModal: false,
    loading: false
  }
};

describe('Test ClientConfiguration SendLetter reducers', () => {
  it('updateSendLetterConfigRequest > add', () => {
    const actionType = 'add';
    const selectedItem = undefined;

    const nextState = reducer(
      initialState,
      sendLetterConfigActions.updateSendLetterConfigRequest({ actionType, selectedItem })
    );

    expect(nextState.updateSendLetterConfig.openModal).toEqual(true);
    expect(nextState.updateSendLetterConfig.actionType).toEqual(actionType);
    expect(nextState.updateSendLetterConfig.selectedItem).toEqual({});
  });
  it('updateSendLetterConfigRequest > edit', () => {
    const actionType = 'edit';
    const selectedItem = {
      state: {
        description: 'description 1',
        value: 'value 1',
        countryCode: 'code 1'
      },
      letterNumber: 'letter number 1 change',
      stateValue: 'state 1 change'
    };

    const nextState = reducer(
      initialState,
      sendLetterConfigActions.updateSendLetterConfigRequest({ actionType, selectedItem })
    );

    expect(nextState.updateSendLetterConfig.openModal).toEqual(true);
    expect(nextState.updateSendLetterConfig.actionType).toEqual(actionType);
    expect(nextState.updateSendLetterConfig.selectedItem).toEqual(formatEditData(selectedItem));
  });
  it('updateSendLetterConfigRequest > empty', () => {
    const actionType = '';
    const selectedItem = undefined;

    const nextState = reducer(
      initialState,
      sendLetterConfigActions.updateSendLetterConfigRequest({ actionType, selectedItem })
    );
    expect(nextState.updateSendLetterConfig.openModal).toEqual(true);
    expect(nextState.updateSendLetterConfig.actionType).toEqual(actionType);
    expect(nextState.updateSendLetterConfig.selectedItem).toEqual({});
  });
  it('cancelUpdateSendLetterConfig', () => {
    const nextState = reducer(
      initialState,
      sendLetterConfigActions.cancelUpdateSendLetterConfig()
    );
    expect(nextState.updateSendLetterConfig.openModal).toEqual(false);
    expect(nextState.updateSendLetterConfig.actionType).toEqual('');
    expect(nextState.updateSendLetterConfig.selectedItem).toEqual({});
  });
  it('cancelDeleteFrequencyConfig', () => {
    const nextState = reducer(
      initialState,
      sendLetterConfigActions.cancelDeleteFrequencyConfig()
    );

    expect(nextState.deleteCallFrequencyConfiguration.openModal).toEqual(false);
    expect(nextState.deleteCallFrequencyConfiguration.currentRecord).toEqual(
      undefined
    );
  });

  it('deleteFrequencyConfigRequest', () => {
    const nextState = reducer(
      initialState,
      sendLetterConfigActions.deleteFrequencyConfigRequest({ letterNumber: 1 })
    );

    expect(nextState.deleteCallFrequencyConfiguration.openModal).toEqual(true);
    expect(nextState.deleteCallFrequencyConfiguration.currentRecord).toEqual({
      letterNumber: 1
    });
  });

  it('addFrequencyConfigRequest', () => {
    const nextState = reducer(
      initialState,
      sendLetterConfigActions.addFrequencyConfigRequest()
    );

    expect(nextState.addCallFrequencyConfiguration.openModal).toEqual(true);
  });

  it('cancelAddFrequencyConfig', () => {
    const nextState = reducer(
      initialState,
      sendLetterConfigActions.cancelAddFrequencyConfig()
    );

    expect(nextState.addCallFrequencyConfiguration.openModal).toEqual(false);
  });

  it('editFrequencyConfigRequest', () => {
    const nextState = reducer(
      initialState,
      sendLetterConfigActions.editFrequencyConfigRequest({ letterNumber: 1 })
    );

    expect(nextState.editCallFrequencyConfiguration.openModal).toEqual(true);
    expect(
      nextState.editCallFrequencyConfiguration.currentRecord?.state
    ).toEqual({
      description: 'undefined - undefined'
    });
  });

  it('cancelEditFrequencyConfig', () => {
    const nextState = reducer(
      initialState,
      sendLetterConfigActions.cancelEditFrequencyConfig()
    );

    expect(nextState.editCallFrequencyConfiguration.openModal).toEqual(false);
  });

  it('onChangeSort', () => {
    const nextState = reducer(
      initialState,
      sendLetterConfigActions.onChangeSort({
        id: 'letterNumber',
        order: 'desc'
      })
    );

    expect(nextState.sortBy).toEqual({
      id: 'letterNumber',
      order: 'desc'
    });
  });
});
