import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { batch } from 'react-redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { sendLetterConfigActions } from './reducers';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';

// helpers
import findIndex from 'lodash.findindex';
import isEmpty from 'lodash.isempty';
import {
  getFulfilledMessage,
  getRejectedMessage,
  formatPostData
} from './helpers';

// types
import { SendLetterConfigData, ActionType, SendLetterState } from './../types';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

export interface UpdateSendLetterConfigArgs {
  data: SendLetterConfigData;
  actionType: ActionType;
}

export const updateSendLetterConfig = createAsyncThunk<
  undefined,
  UpdateSendLetterConfigArgs,
  ThunkAPIConfig
>('clientConfigRegulatory/updateSendLetterConfig', async (args, thunkAPI) => {
  const { dispatch, rejectWithValue, getState } = thunkAPI;
  const { sendLetterConfig } = getState();
  const { data, actionType } = args;
  const { state, letterNumber } = data || {};

  const updateItem: SendLetterConfigData = {
    state: {
      description: state?.description?.trim() || '',
      value: state?.value?.trim() || '',
      countryCode: state?.countryCode?.trim() || ''
    },
    letterNumber: letterNumber?.trim() || ''
  };
  const formattedUpdateItem = formatPostData(
    updateItem
  ) as SendLetterConfigData;
  const getSendLetterData = !isEmpty(
    sendLetterConfig?.sendLetterConfigurationList?.data
  )
    ? [...sendLetterConfig?.sendLetterConfigurationList?.data]
    : [];

  switch (actionType) {
    case 'add':
      getSendLetterData.push(formattedUpdateItem);
      break;
    case 'edit':
      // get selectedItem from store to check correctly with the case of editing the whole state
      const selectedItem =
        sendLetterConfig?.updateSendLetterConfig?.selectedItem;

      const editIndex = findIndex(
        getSendLetterData,
        item => item?.state?.value === selectedItem?.state?.value
      );

      getSendLetterData.splice(editIndex, 1, formattedUpdateItem);
      break;
    case 'delete':
      const deleteIndex = findIndex(
        getSendLetterData,
        item => item?.state?.value === formattedUpdateItem?.state?.value
      );

      getSendLetterData.splice(deleteIndex, 1);
      break;
  }

  const updateResponse = await dispatch(
    clientConfigurationActions.saveCollectionsClientConfig({
      component: 'regulatory-config',
      jsonValue: {
        ['send-letter']: getSendLetterData
      }
    })
  );

  if (isFulfilled(updateResponse)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: getFulfilledMessage(actionType)
        })
      );

      dispatch(sendLetterConfigActions.cancelUpdateSendLetterConfig());

      dispatch(sendLetterConfigActions.getSendLetterConfiguration());

      if (!isEmpty(actionType)) {
        dispatch(
          changeHistoryActions.saveChangedClientConfig({
            action:
              actionType === 'edit'
                ? 'UPDATE'
                : (actionType.toUpperCase() as 'ADD' | 'DELETE'),
            changedCategory: 'regulatory',
            changedItem: {
              state: `${formattedUpdateItem.state?.description}`
            },
            oldValue:
              actionType.toUpperCase() !== 'ADD'
                ? {
                    letterId:
                      sendLetterConfig?.updateSendLetterConfig?.selectedItem
                        ?.letterNumber
                  }
                : {},
            newValue:
              actionType.toUpperCase() !== 'DELETE'
                ? {
                    letterId: formattedUpdateItem.letterNumber
                  }
                : {}
          })
        );
      }
    });

    return;
  }

  if (isRejected(updateResponse)) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: getRejectedMessage(actionType)
      })
    );
  }

  return rejectWithValue({});
});

export const updateSendLetterConfigBuilder = (
  builder: ActionReducerMapBuilder<SendLetterState>
) => {
  builder
    .addCase(updateSendLetterConfig.pending, (draftState, action) => {
      draftState.updateSendLetterConfig = {
        ...draftState.updateSendLetterConfig,
        loading: true
      };
    })
    .addCase(updateSendLetterConfig.fulfilled, (draftState, action) => {
      draftState.updateSendLetterConfig = {
        ...draftState.updateSendLetterConfig,
        loading: false,
        error: '',
        openModal: false
      };
    })
    .addCase(updateSendLetterConfig.rejected, (draftState, action) => {
      draftState.updateSendLetterConfig = {
        ...draftState.updateSendLetterConfig,
        loading: false,
        error: action.error.message
      };
    });
};
