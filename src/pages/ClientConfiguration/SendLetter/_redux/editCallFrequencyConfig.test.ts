import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault
} from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { sendLetterConfigService } from '../sendLetterService';
import {
  editFrequencyConfiguration,
  triggerEditFrequencyConfiguration
} from './editCallFrequencyConfig';

const actionsToastSpy = mockActionCreator(actionsToast);

describe('Test editFrequencyConfiguration', () => {
  it('editFrequencyConfiguration > fulfilled', async () => {
    const store = createStore(rootReducer, {});

    jest
      .spyOn(sendLetterConfigService, 'editFrequencyConfiguration')
      .mockResolvedValue({ ...responseDefault });

    const result = await editFrequencyConfiguration({ postData: {} })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(editFrequencyConfiguration.fulfilled.type);
  });

  it('editFrequencyConfiguration > rejected', async () => {
    const store = createStore(rootReducer, {});

    jest
      .spyOn(sendLetterConfigService, 'editFrequencyConfiguration')
      .mockRejectedValue({ ...responseDefault });

    const result = await editFrequencyConfiguration({ postData: {} })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(editFrequencyConfiguration.rejected.type);
  });
});

describe('Test triggerEditFrequencyConfiguration', () => {
  it('triggerEditFrequencyConfiguration > fulfilled', async () => {
    const store = createStore(rootReducer, {});

    const addToast = actionsToastSpy('addToast');

    await triggerEditFrequencyConfiguration({ postData: {} })(
      byPassFulfilled(triggerEditFrequencyConfiguration.fulfilled.type),
      store.getState,
      {}
    );

    expect(addToast).toBeCalled();
  });

  it('triggerEditFrequencyConfiguration > rejected', async () => {
    const store = createStore(rootReducer, {});

    const addToast = actionsToastSpy('addToast');

    await triggerEditFrequencyConfiguration({ postData: {} })(
      byPassRejected(triggerEditFrequencyConfiguration.rejected.type),
      store.getState,
      {}
    );

    expect(addToast).toBeCalled();
  });
});
