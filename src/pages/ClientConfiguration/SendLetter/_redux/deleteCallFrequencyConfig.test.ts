import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault
} from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { sendLetterConfigService } from '../sendLetterService';
import {
  deleteFrequencyConfiguration,
  triggerDeleteFrequencyConfiguration
} from './deleteCallFrequencyConfig';

const actionsToastSpy = mockActionCreator(actionsToast);

describe('Test deleteFrequencyConfiguration', () => {
  const store = createStore(rootReducer, {
    sendLetterConfig: {
      deleteCallFrequencyConfiguration: {
        currentRecord: {
          state: {
            description: 'description',
            value: 'value',
            countryCode: 'countryCode'
          }
        }
      }
    }
  });

  it('deleteFrequencyConfiguration > fulfilled', async () => {
    const store = createStore(rootReducer, {});

    jest
      .spyOn(sendLetterConfigService, 'deleteFrequencyConfiguration')
      .mockResolvedValue({ ...responseDefault });

    const result = await deleteFrequencyConfiguration()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(deleteFrequencyConfiguration.fulfilled.type);
  });

  it('deleteFrequencyConfiguration > fulfilled', async () => {
    jest
      .spyOn(sendLetterConfigService, 'deleteFrequencyConfiguration')
      .mockResolvedValue({ ...responseDefault });

    const result = await deleteFrequencyConfiguration()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(deleteFrequencyConfiguration.fulfilled.type);
  });

  it('deleteFrequencyConfiguration > rejected', async () => {
    jest
      .spyOn(sendLetterConfigService, 'deleteFrequencyConfiguration')
      .mockRejectedValue({ ...responseDefault });

    const result = await deleteFrequencyConfiguration()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(deleteFrequencyConfiguration.rejected.type);
  });
});

describe('Test triggerDeleteFrequencyConfiguration', () => {
  it('triggerDeleteFrequencyConfiguration > fulfilled', async () => {
    const store = createStore(rootReducer, {});

    const addToast = actionsToastSpy('addToast');

    await triggerDeleteFrequencyConfiguration()(
      byPassFulfilled(triggerDeleteFrequencyConfiguration.fulfilled.type),
      store.getState,
      {}
    );

    expect(addToast).toBeCalled();
  });

  it('triggerDeleteFrequencyConfiguration > rejected', async () => {
    const store = createStore(rootReducer, {});

    const addToast = actionsToastSpy('addToast');

    await triggerDeleteFrequencyConfiguration()(
      byPassRejected(triggerDeleteFrequencyConfiguration.rejected.type),
      store.getState,
      {}
    );

    expect(addToast).toBeCalled();
  });
});
