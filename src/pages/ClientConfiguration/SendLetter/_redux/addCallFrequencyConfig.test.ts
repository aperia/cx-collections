import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault
} from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { sendLetterConfigService } from '../sendLetterService';
import {
  addFrequencyConfiguration,
  triggerAddFrequencyConfiguration
} from './addCallFrequencyConfig';

const actionsToastSpy = mockActionCreator(actionsToast);

describe('Test addFrequencyConfiguration', () => {
  it('addFrequencyConfiguration > fulfilled', async () => {
    const store = createStore(rootReducer, {});

    jest
      .spyOn(sendLetterConfigService, 'addFrequencyConfiguration')
      .mockResolvedValue({ ...responseDefault });

    const result = await addFrequencyConfiguration({ postData: {} })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(addFrequencyConfiguration.fulfilled.type);
  });

  it('addFrequencyConfiguration > rejected', async () => {
    const store = createStore(rootReducer, {});

    jest
      .spyOn(sendLetterConfigService, 'addFrequencyConfiguration')
      .mockRejectedValue({ ...responseDefault });

    const result = await addFrequencyConfiguration({ postData: {} })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(addFrequencyConfiguration.rejected.type);
  });
});

describe('Test triggerAddFrequencyConfiguration', () => {
  it('triggerAddFrequencyConfiguration > fulfilled', async () => {
    const store = createStore(rootReducer, {});

    const addToast = actionsToastSpy('addToast');

    await triggerAddFrequencyConfiguration({ postData: {} })(
      byPassFulfilled(triggerAddFrequencyConfiguration.fulfilled.type),
      store.getState,
      {}
    );

    expect(addToast).toBeCalled();
  });

  it('triggerAddFrequencyConfiguration > rejected', async () => {
    const store = createStore(rootReducer, {});

    const addToast = actionsToastSpy('addToast');

    await triggerAddFrequencyConfiguration({ postData: {} })(
      byPassRejected(triggerAddFrequencyConfiguration.rejected.type),
      store.getState,
      {}
    );

    expect(addToast).toBeCalled();
  });
});
