import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';

import { sendLetterConfigService } from '../sendLetterService';
import { EditFrequencyPayload, SendLetterState } from '../types';
import { formatPostData } from './helpers';
import { sendLetterConfigActions } from './reducers';

export const editFrequencyConfiguration = createAsyncThunk<
  undefined,
  EditFrequencyPayload,
  ThunkAPIConfig
>('editFrequencyConfiguration', async (args, thunkAPI) => {
  const { postData } = args;
  const { data } = await sendLetterConfigService.editFrequencyConfiguration(
    postData
  );
  return data;
});

export const triggerEditFrequencyConfiguration = createAsyncThunk<
  unknown,
  EditFrequencyPayload,
  ThunkAPIConfig
>('triggerEditSendLetterConfiguration', async (args, thunkAPI) => {
  const { dispatch } = thunkAPI;
  const { postData } = args;
  const format = {
    ...postData,
    newRecord: formatPostData(postData?.newRecord as MagicKeyValue)
  };
  const response = await dispatch(
    editFrequencyConfiguration({ postData: format })
  );

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'txt_frequency_config_edit_success'
        })
      );
      dispatch(sendLetterConfigActions.getFrequencyConfiguration());
    });
  }

  if (isRejected(response)) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: 'txt_frequency_config_edit_fail'
      })
    );
  }
});

export const editFrequencyConfigurationBuilder = (
  builder: ActionReducerMapBuilder<SendLetterState>
) => {
  builder
    .addCase(editFrequencyConfiguration.pending, (draftState, action) => {
      draftState.editCallFrequencyConfiguration = {
        ...draftState.editCallFrequencyConfiguration,
        loading: true
      };
    })
    .addCase(editFrequencyConfiguration.fulfilled, (draftState, action) => {
      draftState.editCallFrequencyConfiguration = {
        ...draftState.editCallFrequencyConfiguration,
        loading: false,
        error: '',
        openModal: false
      };
    })
    .addCase(editFrequencyConfiguration.rejected, (draftState, action) => {
      draftState.editCallFrequencyConfiguration = {
        ...draftState.editCallFrequencyConfiguration,
        loading: false,
        error: action.error.message
      };
    });
};
