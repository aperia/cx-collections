import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';

// components
import { FailedApiReload, NoDataGrid } from 'app/components';
import {
  Button,
  ColumnType,
  Grid,
  GridRef,
  Pagination,
  SortType
} from 'app/_libraries/_dls/components';
import { Actions } from './Actions';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { usePagination } from 'app/hooks';

// helpers
import classNames from 'classnames';

// redux
import { useDispatch, useSelector } from 'react-redux';
import {
  selectLetterConfigurationData,
  selectLetterConfigurationError,
  selectLetterConfigurationLoading
} from './_redux/selectors';
import { sendLetterConfigActions } from './_redux/reducers';

// constants
import { SEND_LETTER_GRID_COLUMN_NAME } from './constants';
import { I18N_ADJUSTMENT_TRANSACTION } from '../AdjustmentTransaction/constants';

// types
import { SendLetterConfigData } from './types';
import { useCheckClientConfigStatus } from '../hooks/useCheckClientConfigStatus';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface LetterConfigGridProps { }

const Header = ({ className }: { className?: string }) => {
  const { t } = useTranslation();
  const testId = 'clientConfig_sendLetter_grid';
  return (
    <p
      className={classNames('fs-14 fw-500 color-grey-d20', className)}
      data-testid={genAmtId(testId, 'header', '')}
    >
      {t('txt_send_letter_config_title_header')}
    </p>
  );
};

const LetterConfigGrid: React.FC<LetterConfigGridProps> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const gridRefs = useRef<GridRef>(null);
  const testId = 'clientConfig_sendLetter_grid';

  const isLoading = useSelector(selectLetterConfigurationLoading);
  const isError = useSelector(selectLetterConfigurationError);
  const data = useSelector(selectLetterConfigurationData);

  const status = useCheckClientConfigStatus();

  const [sortBy, setSortBy] = useState<SortType[]>([]);

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(data);

  const handleSort = (sort: SortType) => {
    setSortBy([sort]);
    dispatch(sendLetterConfigActions.onChangeSort(sort));
  };

  const handleReload = useCallback(() => {
    dispatch(sendLetterConfigActions.getSendLetterConfiguration());
  }, [dispatch]);

  useEffect(() => {
    handleReload();
  }, [handleReload]);
  // reset sort
  useEffect(() => () => {
    dispatch(sendLetterConfigActions.onChangeSort({ id: '' }));
  }, [dispatch]);

  const showPagination = total > pageSize[0];

  const dataColumn = useMemo(() => {
    return [
      {
        id: SEND_LETTER_GRID_COLUMN_NAME.LETTER_STATE,
        Header: t('txt_send_letter_config_state'),
        accessor: (col: SendLetterConfigData) => (
          <span>
            {col?.state?.value} - {col?.state?.description}
          </span>
        ),
        isSort: true,
        width: 257
      },
      {
        id: SEND_LETTER_GRID_COLUMN_NAME.LETTER_NUMBER,
        Header: t('txt_send_letter_config_number'),
        accessor: SEND_LETTER_GRID_COLUMN_NAME.LETTER_NUMBER,
        isSort: true,
        autoWidth: true,
        width: 160
      },
      {
        id: 'action',
        Header: t('txt_actions'),
        accessor: (col: SendLetterConfigData, idx: number) => (
          <Actions status={status} record={col} dataTestId={`${idx}_action_${testId}`} />
        ),
        width: 144,
        isFixedRight: true,
        cellProps: { className: 'text-center' },
        cellBodyProps: { className: 'text-center' },
        className: 'td-sm'
      }
    ];
  }, [t, status]);

  const handleOpenModal = () => {
    dispatch(
      sendLetterConfigActions.updateSendLetterConfigRequest({
        actionType: 'add'
      })
    );
  };

  if (isLoading) return <div className="vh-50" />;

  if (isError)
    return (
      <>
        <Header className="mt-16" />
        <FailedApiReload
          id="send-letter-config-request-fail"
          className="mt-80"
          onReload={handleReload}
          dataTestId={`${testId}_failedApi`}
        />
      </>
    );
  if (!data.length)
    return (
      <>
        <Header className="mt-16" />
        <NoDataGrid
          text={t(I18N_ADJUSTMENT_TRANSACTION.NO_DATA)}
          dataTestId={`${testId}_noData`}
        >
          <div className="mt-24">
            <Button
              disabled={status}
              onClick={handleOpenModal}
              size="sm"
              variant="outline-primary"
              dataTestId={`${testId}_addBtn`}
            >
              {t('txt_letter_config_add_title')}
            </Button>
          </div>
        </NoDataGrid>
      </>
    );

  return (
    <>
      <div className="mt-16 d-flex justify-content-between align-items-center">
        <Header />
        <div className="mr-n8">
          <Button
            disabled={status}
            onClick={handleOpenModal}
            size="sm"
            variant="outline-primary"
            dataTestId={`${testId}_addBtn`}
          >
            {t('txt_letter_config_add_title')}
          </Button>
        </div>
      </div>
      <p className="mt-8" data-testid={genAmtId(testId, 'description', '')}>
        {t('txt_send_letter_config_title_header_desc')}
      </p>
      <div className="mt-16">
        <Grid
          ref={gridRefs}
          onSortChange={handleSort}
          columns={dataColumn as unknown as ColumnType[]}
          data={gridData}
          sortBy={sortBy}
          dataTestId={`${testId}_grid`}
        />
        {showPagination && (
          <div className="pt-16">
            <Pagination
              totalItem={total}
              pageSize={pageSize}
              pageNumber={currentPage}
              pageSizeValue={currentPageSize}
              compact
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
              dataTestId={`${testId}_pagination`}
            />
          </div>
        )}
      </div>
    </>
  );
};

export default LetterConfigGrid;
