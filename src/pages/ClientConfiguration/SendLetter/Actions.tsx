import React from 'react';

// components
import { Button } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useDispatch } from 'react-redux';
import { sendLetterConfigActions } from './_redux/reducers';

// types
import { SendLetterConfigData } from './types';

// constant
import { I18N_ADJUSTMENT_TRANSACTION } from '../AdjustmentTransaction/constants';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface ActionsProps extends DLSId {
  record: SendLetterConfigData;
  status: boolean;
}

export const Actions: React.FC<ActionsProps> = ({ record, status, dataTestId }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const handleRequest = (actionType: 'delete' | 'edit') => {
    dispatch(
      sendLetterConfigActions.updateSendLetterConfigRequest({
        actionType,
        selectedItem: record
      })
    );
  };

  return (
    <>
      <Button
        disabled={status}
        onClick={() => handleRequest('delete')}
        size="sm"
        variant="outline-danger"
        dataTestId={genAmtId(dataTestId, 'deleteBtn', '')}
      >
        {t(I18N_ADJUSTMENT_TRANSACTION.DELETE)}
      </Button>
      <Button
        disabled={status}
        onClick={() => handleRequest('edit')}
        size="sm"
        variant="outline-primary"
        dataTestId={genAmtId(dataTestId, 'editBtn', '')}
      >
        {t(I18N_ADJUSTMENT_TRANSACTION.EDIT)}
      </Button>
    </>
  );
};
