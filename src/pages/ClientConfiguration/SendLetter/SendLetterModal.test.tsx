import React from 'react';
import { fireEvent, screen } from '@testing-library/dom';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { sendLetterConfigActions } from './_redux/reducers';
import SendLetterModal from './SendLetterModal';
import { SendLetterConfigData } from './types';
import { SortType } from 'app/_libraries/_dls/components';
import { I18N_SEND_LETTER } from './constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

const spySendLetterConfigActions = mockActionCreator(sendLetterConfigActions);

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const formValues = {
  state: {
    description: 'description 1',
    value: 'value 1',
    countryCode: 'code 1'
  },
  letterNumber: 'letter number 1 change',
  stateValue: 'state 1 change'
};
const initialState = {
  sendLetterConfig: {
    sendLetterConfigurationList: {
      loading: false,
      data: [
        {
          state: {
            description: 'description 1',
            value: 'value 1',
            countryCode: 'code 1'
          },
          letterNumber: 'letter number 1',
          stateValue: 'state 1'
        },
        {
          state: {
            description: 'description 2',
            value: 'value 2',
            countryCode: 'code 2'
          },
          letterNumber: 'letter number 2',
          stateValue: 'state 2'
        }
      ] as SendLetterConfigData[],
      error: undefined
    },
    sortBy: {} as SortType
  },
  form: {
    send_letter_config_client: {
      values: formValues
    }
  } as any
} as RootState;

describe('SendLetterModal', () => {
  const mockSelectedItem = (number: number) => ({
    state: {
      description: `description ${number}`,
      value: `value ${number}`,
      countryCode: `code ${number}`
    },
    letterNumber: `letter number ${number}`,
    stateValue: `state ${number}`
  });

  describe(' > add', () => {
    it(' > should hide modal', () => {
      renderMockStore(
        <SendLetterModal actionType="add" selectedItem={undefined} />,
        { initialState }
      );
      expect(document.body.textContent).toEqual('');
    });

    it(' > should render modal', () => {
      renderMockStore(
        <SendLetterModal
          actionType="add"
          selectedItem={undefined}
          isShowModal
        />,
        { initialState }
      );
      expect(screen.getByText(I18N_SEND_LETTER.ADD_TITLE)).toBeInTheDocument();
    });

    it(' > should render error message', () => {
      renderMockStore(
        <SendLetterModal
          actionType="add"
          selectedItem={undefined}
          isShowModal
        />,
        { initialState }
      );
      fireEvent.click(screen.getByText(I18N_COMMON_TEXT.SUBMIT));
      expect(
        screen.getByText(I18N_SEND_LETTER.STATE_EXIST_ERROR)
      ).toBeInTheDocument();
    });

    it(' > should render error message', () => {
      renderMockStore(
        <SendLetterModal
          actionType="add"
          selectedItem={undefined}
          isShowModal
        />,
        { initialState }
      );
      fireEvent.click(screen.getByText(I18N_COMMON_TEXT.SUBMIT));
      expect(
        screen.getByText(I18N_SEND_LETTER.STATE_EXIST_ERROR)
      ).toBeInTheDocument();
    });
  });

  describe(' > edit', () => {
    it(' > should close when click cancel', () => {
      const cancelUpdateSendLetterConfig = spySendLetterConfigActions(
        'cancelUpdateSendLetterConfig'
      );
      renderMockStore(
        <SendLetterModal
          actionType="edit"
          selectedItem={mockSelectedItem(1)}
          isShowModal
        />,
        { initialState }
      );
      fireEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));
      expect(cancelUpdateSendLetterConfig).toBeCalled();
    });

    it(' > should edit success', () => {
      const updateSendLetterConfig = spySendLetterConfigActions(
        'updateSendLetterConfig'
      );
      const selectedItem = mockSelectedItem(1);
      renderMockStore(
        <SendLetterModal
          actionType="edit"
          selectedItem={selectedItem}
          isShowModal
        />,
        { initialState }
      );
      fireEvent.click(screen.getByText(I18N_COMMON_TEXT.SAVE));
      expect(updateSendLetterConfig).toBeCalledWith({
        actionType: 'edit',
        data: formValues
      });
    });
  });

  describe(' > delete', () => {
    it(' > should delete success', () => {
      const updateSendLetterConfig = spySendLetterConfigActions(
        'updateSendLetterConfig'
      );
      const selectedItem = mockSelectedItem(1);
      renderMockStore(
        <SendLetterModal
          actionType="delete"
          selectedItem={selectedItem}
          isShowModal
        />,
        { initialState }
      );
      fireEvent.click(screen.getByText(I18N_COMMON_TEXT.DELETE));
      expect(updateSendLetterConfig).toBeCalledWith({
        actionType: 'delete',
        data: selectedItem
      });
    });
  });
});
