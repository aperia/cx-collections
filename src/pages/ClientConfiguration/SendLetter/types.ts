import { SortType } from 'app/_libraries/_dls/components';

export interface SendLetterConfigData {
  state?: {
    description?: string;
    value?: string;
    countryCode?: string;
  };
  letterNumber?: string;
  stateValue?: string;
}

export interface CallFrequencyConfigData {
  state?: {
    description: string;
    value: string;
    countryCode: string;
  };
  homePhone?: {
    times: number;
    days: number;
  };
  otherPhone?: {
    times: number;
    days: number;
  };
}

export interface EditSendLetterPayload {
  postData: {
    prevRecord?: MagicKeyValue;
    newRecord?: MagicKeyValue;
  };
}

export interface EditFrequencyPayload {
  postData: {
    prevRecord?: MagicKeyValue;
    newRecord?: MagicKeyValue;
  };
}

export type ActionType = 'add' | 'delete' | 'edit' | '';

export interface SendLetterModalType {
  actionType: ActionType;
  selectedItem?: SendLetterConfigData;
}

export interface UpdateSendLetterConfigType extends SendLetterModalType {
  loading: boolean;
  error?: string;
  openModal: boolean;
}

export interface SendLetterState {
  sortBy: SortType;
  sendLetterConfigurationList: {
    loading: boolean;
    data: SendLetterConfigData[];
    error?: string;
  };
  updateSendLetterConfig: UpdateSendLetterConfigType;

  callFrequencyConfigurationList: {
    loading: boolean;
    data: CallFrequencyConfigData[];
    error?: string;
  };
  addCallFrequencyConfiguration: {
    loading: boolean;
    error?: string;
    openModal: boolean;
  };
  deleteCallFrequencyConfiguration: {
    loading: boolean;
    error?: string;
    openModal: boolean;
    currentRecord?: CallFrequencyConfigData;
  };
  editCallFrequencyConfiguration: {
    loading: boolean;
    error?: string;
    openModal: boolean;
    currentRecord?: CallFrequencyConfigData;
  };
}
