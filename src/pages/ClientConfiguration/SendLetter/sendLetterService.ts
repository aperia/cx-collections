import { apiService } from 'app/utils/api.service';
import { CallFrequencyConfigData } from './types';

export const sendLetterConfigService = {
  getFrequencyConfiguration() {
    const url =
      window.appConfig?.api?.sendLetterConfig?.getFrequencyConfiguration || '';
    return apiService.post(url, {});
  },

  addFrequencyConfiguration(data: CallFrequencyConfigData) {
    const url =
      window.appConfig?.api?.sendLetterConfig?.addFrequencyConfiguration || '';
    return apiService.post(url, { data });
  },

  deleteFrequencyConfiguration(data: CallFrequencyConfigData) {
    const url =
      window.appConfig?.api?.sendLetterConfig?.deleteFrequencyConfiguration ||
      '';
    return apiService.post(url, { data });
  },

  editFrequencyConfiguration(data: {
    prevRecord?: CallFrequencyConfigData;
    newRecord?: CallFrequencyConfigData;
  }) {
    const url =
      window.appConfig?.api?.sendLetterConfig?.editFrequencyConfiguration || '';
    return apiService.post(url, { data });
  }
};
