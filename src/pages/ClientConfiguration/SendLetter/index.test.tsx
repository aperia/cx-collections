import React from 'react';
import { screen } from '@testing-library/dom';
import { renderMockStore } from 'app/test-utils';
import SendLetterConfig from './index';

jest.mock('./SendLetterConfigGird', () => ({
  __esModule: true,
  default: () => <div>Mock Letter Grid</div>
}));

jest.mock('./SendLetterModal', () => ({
  __esModule: true,
  default: () => <div>Mock Add Modal</div>
}));

const initialState = {
  sendLetterConfig: {
    sendLetterConfigurationList: {
      loading: false,
      data: [] as any[]
    }
  }
} as RootState;

describe('Test on index component', () => {
  it('should render Send Letter UI ', () => {
    renderMockStore(<SendLetterConfig />, { initialState });

    expect(
      screen.getByText('txt_send_letter_config_title')
    ).toBeInTheDocument();
    expect(screen.getByText('Mock Letter Grid')).toBeInTheDocument();
  });
  it('should render Send Letter UI with add modal', () => {
    renderMockStore(
      <SendLetterConfig />,
      {
        initialState: {
          ...initialState,
          sendLetterConfig: {
            ...initialState.sendLetterConfig,
            updateSendLetterConfig: {
              openModal: true
            } as any
          }
        }
      });

    expect(
      screen.getByText('txt_send_letter_config_title')
    ).toBeInTheDocument();
    expect(screen.getByText('Mock Add Modal')).toBeInTheDocument();
  });
});
