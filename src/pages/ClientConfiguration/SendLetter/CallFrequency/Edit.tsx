import React from 'react';

// components
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { sendLetterConfigActions } from '../_redux/reducers';
import {
  selectEditFrequencyLoadingModalOpen,
  selectEditFrequencyCurrentRecord,
  selectEditFrequencyModalOpen,
  selectFormDataFrequency
} from '../_redux/selectors';

// constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { View } from 'app/_libraries/_dof/core';
import { isInvalid } from 'redux-form';
import { FREQUENCY_VIEW_NAME } from '../constants';

export interface DataFormType {
  letterId?: string;
  letterDescription?: string;
}

const Edit: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const testId = 'clientConfig_sendLetter_callFrequencyEdit';

  const modalOpened = useSelector(selectEditFrequencyModalOpen);
  const loading = useSelector(selectEditFrequencyLoadingModalOpen);
  const dataEdit = useSelector(selectEditFrequencyCurrentRecord);
  const formData = useSelector(selectFormDataFrequency);
  const disabledOk = useSelector<RootState, boolean>(
    isInvalid(FREQUENCY_VIEW_NAME)
  );

  const handleCancelModal = () => {
    dispatch(sendLetterConfigActions.cancelEditFrequencyConfig());
  };

  const handleSubmit = () => {
    dispatch(
      sendLetterConfigActions.triggerEditFrequencyConfiguration({
        postData: {
          prevRecord: dataEdit,
          newRecord: formData
        }
      })
    );
  };

  return (
    <>
      <Modal sm show={modalOpened} loading={loading} dataTestId={testId}>
        <ModalHeader border closeButton onHide={handleCancelModal} dataTestId={`${testId}_header`}>
          <ModalTitle dataTestId={`${testId}_title`}>{t('txt_call_frequency_edit_title')}</ModalTitle>
        </ModalHeader>
        <ModalBody dataTestId={`${testId}_body`}>
          <View
            id="call_frequency_config_client"
            formKey="call_frequency_config_client"
            descriptor="callFrequencyConfigFormView"
            value={dataEdit}
          />
        </ModalBody>
        <ModalFooter
          dataTestId={`${testId}_footer`}
          className="pt-32 pb-24"
          cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
          onCancel={handleCancelModal}
          okButtonText={t(I18N_COMMON_TEXT.SAVE)}
          onOk={handleSubmit}
          disabledOk={disabledOk}
        />
      </Modal>
    </>
  );
};

export default Edit;
