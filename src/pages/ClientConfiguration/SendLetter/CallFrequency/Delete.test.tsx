import React from 'react';
import userEvent from '@testing-library/user-event';

import { Delete } from './Delete';
import { CallFrequencyConfigData } from '../types';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { sendLetterConfigActions } from '../_redux/reducers';
import { mockActionCreator, renderMockStore } from 'app/test-utils';

describe('Test component Delete', () => {
  const initialState = {
    sendLetterConfig: {
      deleteCallFrequencyConfiguration: {
        loading: false,
        error: '',
        openModal: true,
        currentRecord: {
          state: {
            description: 'mock description',
            value: 'mock value',
            countryCode: 'mock code'
          },
          homePhone: {
            times: 0,
            days: 0
          },
          otherPhone: {
            times: 0,
            days: 0
          }
        } as CallFrequencyConfigData
      }
    }
  } as RootState;
  const sendLetterConfigActionsSpy = mockActionCreator(sendLetterConfigActions);

  it('should render component', () => {
    const { wrapper } = renderMockStore(<Delete />, { initialState });
    const currentRecordState =
      initialState.sendLetterConfig.deleteCallFrequencyConfiguration
        .currentRecord!.state!;

    expect(wrapper).toBeTruthy();
    expect(
      wrapper.getByText('txt_frequency_config_delete_confirm')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_frequency_config_delete_body')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText('txt_send_letter_config_state:')
    ).toBeInTheDocument();
    expect(
      wrapper.getByText(
        `${currentRecordState.countryCode} - ${currentRecordState.description}`
      )
    ).toBeInTheDocument();
    expect(wrapper.getByText(I18N_COMMON_TEXT.CANCEL)).toBeInTheDocument();
    expect(wrapper.getByText(I18N_COMMON_TEXT.DELETE)).toBeInTheDocument();
  });

  it('handleClose', () => {
    const mockCloseAction = sendLetterConfigActionsSpy(
      'cancelDeleteFrequencyConfig'
    );

    const { wrapper } = renderMockStore(<Delete />, { initialState });

    userEvent.click(wrapper.getByText(I18N_COMMON_TEXT.CANCEL));

    expect(mockCloseAction).toHaveBeenCalled();
  });

  it('handleDelete', () => {
    const mockDeleteAction = sendLetterConfigActionsSpy(
      'triggerDeleteFrequencyConfiguration'
    );

    const { wrapper } = renderMockStore(<Delete />, { initialState });

    userEvent.click(wrapper.getByText(I18N_COMMON_TEXT.DELETE));

    expect(mockDeleteAction).toHaveBeenCalled();
  });
});
