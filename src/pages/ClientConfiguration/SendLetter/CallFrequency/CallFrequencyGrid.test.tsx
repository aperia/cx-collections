import React from 'react';
import cloneDeep from 'lodash.clonedeep';

import { CallFrequencyConfigData } from '../types';
import CallFrequencyGird from './CallFrequencyGrid';
import { sendLetterConfigActions } from '../_redux/reducers';
import { renderMockStore, mockActionCreator } from 'app/test-utils';
import { I18N_ADJUSTMENT_TRANSACTION } from 'pages/ClientConfiguration/AdjustmentTransaction/constants';

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponent = jest.requireActual('app/_libraries/_dls/components');
  const { MockGrid } = jest.requireActual('app/test-utils/mocks/MockGrid');
  return {
    ...dlsComponent,
    Grid: MockGrid
  };
});

jest.mock('./Add', () => ({
  __esModule: true,
  default: () => <div>Mock Add Modal</div>
}));

jest.mock('./Action', () => ({
  Actions: () => <div>Mock Actions component</div>
}));

describe('Test CallFrequencyGrid component', () => {
  const initialState = {
    sendLetterConfig: {
      addCallFrequencyConfiguration: {
        loading: false,
        openModal: true,
        error: undefined
      },
      callFrequencyConfigurationList: {
        loading: false,
        data: [
          {
            state: {
              description: 'mock description',
              value: 'mock value',
              countryCode: 'mock code'
            },
            homePhone: {
              times: 0,
              days: 0
            },
            otherPhone: {
              times: 0,
              days: 0
            }
          } as CallFrequencyConfigData
        ],
        error: ''
      }
    }
  } as RootState;
  const sendLetterConfigActionsSpy = mockActionCreator(sendLetterConfigActions);

  it('should render component', () => {
    sendLetterConfigActionsSpy('getFrequencyConfiguration');

    jest.useFakeTimers();
    const { wrapper } = renderMockStore(<CallFrequencyGird />, {
      initialState
    });
    jest.runAllTimers();

    expect(wrapper).toBeTruthy();
    expect(
      wrapper.getByText('txt_call_frequency_config_title_header')
    ).toBeInTheDocument();
  });

  it('should render component with pagination', () => {
    sendLetterConfigActionsSpy('getFrequencyConfiguration');
    const cloneInitialState = cloneDeep(initialState);
    cloneInitialState.sendLetterConfig.callFrequencyConfigurationList.data =
      Array(11).fill(
        initialState.sendLetterConfig.callFrequencyConfigurationList.data
      );

    jest.useFakeTimers();
    const { wrapper } = renderMockStore(<CallFrequencyGird />, {
      initialState: cloneInitialState
    });
    jest.runAllTimers();

    expect(wrapper.getByText('1')).toBeTruthy();
  });

  it('should render component error', () => {
    sendLetterConfigActionsSpy('getFrequencyConfiguration');
    const cloneInitialState = cloneDeep(initialState);
    cloneInitialState.sendLetterConfig.callFrequencyConfigurationList.error =
      'mock error';

    jest.useFakeTimers();
    const { baseElement } = renderMockStore(<CallFrequencyGird />, {
      initialState: cloneInitialState
    });
    jest.runAllTimers();

    expect(
      baseElement.querySelector('#send-letter-config-request-fail_Button')
    ).toBeInTheDocument();
  });

  it('should render component with empty data', () => {
    sendLetterConfigActionsSpy('getFrequencyConfiguration');
    const cloneInitialState = cloneDeep(initialState);
    cloneInitialState.sendLetterConfig.callFrequencyConfigurationList.data = [];

    jest.useFakeTimers();
    const { wrapper } = renderMockStore(<CallFrequencyGird />, {
      initialState: cloneInitialState
    });
    jest.runAllTimers();

    expect(
      wrapper.getByText(I18N_ADJUSTMENT_TRANSACTION.NO_DATA)
    ).toBeInTheDocument();
  });

  it('should render component being loading', () => {
    sendLetterConfigActionsSpy('getFrequencyConfiguration');
    const cloneInitialState = cloneDeep(initialState);
    cloneInitialState.sendLetterConfig.callFrequencyConfigurationList.loading =
      true;

    jest.useFakeTimers();
    const { baseElement } = renderMockStore(<CallFrequencyGird />, {
      initialState: cloneInitialState
    });
    jest.runAllTimers();

    expect(baseElement.getElementsByClassName('vh-50').length).toEqual(1);
  });
});
