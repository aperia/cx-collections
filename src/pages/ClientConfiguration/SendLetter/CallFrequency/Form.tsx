import { View } from 'app/_libraries/_dof/core';
import React from 'react';

export interface FormProps {
  viewRef?: any;
}

const Form: React.FC<FormProps> = ({ viewRef }) => {
  return (
    <>
      <View
        id="call_frequency_config_client"
        formKey="call_frequency_config_client"
        descriptor="callFrequencyConfigFormView"
        ref={viewRef}
      />
    </>
  );
};

export default Form;
