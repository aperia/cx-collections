import React, { useRef, useState } from 'react';

// components
import {
  Button,
  InlineMessage,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import Form from './Form';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { sendLetterConfigActions } from '../_redux/reducers';
import {
  selectAddFrequencyModalOpen,
  selectAddFrequencyLoadingModalOpen,
  selectFormDataFrequency,
  selectFrequencyConfigurationData
} from '../_redux/selectors';

// constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { Field, isInvalid } from 'redux-form';
import { FREQUENCY_VIEW_NAME } from '../constants';
import findIndex from 'lodash.findindex';
import { ExtraFieldProps } from 'app/_libraries/_dof/core';
import { DropDownControlProps } from 'app/components/_dof_controls/controls';

export interface MessageErrorType {
  state?: string | undefined;
  letterNumber?: string | undefined;
}

const Add: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const testId = 'clientConfig_sendLetter_callFrequencyAdd';

  const modalOpened = useSelector(selectAddFrequencyModalOpen);
  const loading = useSelector(selectAddFrequencyLoadingModalOpen);
  const data = useSelector(selectFrequencyConfigurationData);
  const formData = useSelector(selectFormDataFrequency);

  const [isMessage, setIsMessage] = useState<boolean>(false);
  const viewRef = useRef<any>(null);

  const disabledOk = useSelector<RootState, boolean>(
    isInvalid(FREQUENCY_VIEW_NAME)
  );

  const handleOpenModal = () => {
    dispatch(sendLetterConfigActions.addFrequencyConfigRequest());
  };

  const handleCancelModal = () => {
    dispatch(sendLetterConfigActions.cancelAddFrequencyConfig());
    setIsMessage(false);
  };

  const handleSubmit = () => {
    const index = findIndex(
      data,
      item => item?.state?.value === formData?.state?.value
    );
    if (index !== -1) {
      setIsMessage(true);
      const stateField: Field<ExtraFieldProps> =
        viewRef?.current?.props?.onFind('state');

      stateField.props.props.setOthers((prev: DropDownControlProps) => ({
        ...prev,
        options: {
          ...prev.options,
          isError: true
        }
      }));
    } else {
      setIsMessage(false);
      dispatch(
        sendLetterConfigActions.triggerAddFrequencyConfiguration({
          postData: formData
        })
      );
    }
  };

  return (
    <>
      <Button
        onClick={handleOpenModal}
        size="sm"
        variant="outline-primary"
        dataTestId={`${testId}_addBtn`}
      >
        {t('txt_frequency_add_title')}
      </Button>
      <Modal sm show={modalOpened} loading={loading} dataTestId={testId}>
        <ModalHeader border closeButton onHide={handleCancelModal} dataTestId={`${testId}_header`}>
          <ModalTitle dataTestId={`${testId}_title`}>{t('txt_frequency_add_title')}</ModalTitle>
        </ModalHeader>
        <ModalBody dataTestId={`${testId}_body`}>
          <>
            {isMessage && (
              <InlineMessage className="mb-24" variant="danger" dataTestId={`${testId}_errorMessage`}>
                {t('txt_send_letter_config_inline_message')}
              </InlineMessage>
            )}
            <Form viewRef={viewRef} />
          </>
        </ModalBody>
        <ModalFooter
          dataTestId={`${testId}_footer`}
          className="pt-32 pb-24"
          cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
          onCancel={handleCancelModal}
          okButtonText={t(I18N_COMMON_TEXT.SUBMIT)}
          onOk={handleSubmit}
          disabledOk={disabledOk}
        />
      </Modal>
    </>
  );
};

export default Add;
