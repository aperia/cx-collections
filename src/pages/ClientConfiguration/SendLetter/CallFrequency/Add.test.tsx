import React from 'react';
import cloneDeep from 'lodash.clonedeep';
import { FormStateMap } from 'redux-form';
import userEvent from '@testing-library/user-event';

import Add from './Add';
import { FREQUENCY_VIEW_NAME } from '../constants';
import { CallFrequencyConfigData } from '../types';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { sendLetterConfigActions } from '../_redux/reducers';
import { renderMockStore, mockActionCreator } from 'app/test-utils';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

describe('Test Add component', () => {
  const initialState = {
    sendLetterConfig: {
      addCallFrequencyConfiguration: {
        loading: false,
        openModal: true,
        error: undefined
      },
      callFrequencyConfigurationList: {
        loading: false,
        data: [
          {
            state: {
              description: 'mock description',
              value: 'mock value',
              countryCode: 'mock code'
            },
            homePhone: {
              times: 0,
              days: 0
            },
            otherPhone: {
              times: 0,
              days: 0
            }
          } as CallFrequencyConfigData
        ],
        error: ''
      }
    },
    form: {
      [FREQUENCY_VIEW_NAME]: {
        values: {
          state: {
            description: 'mock description',
            value: 'mock value',
            countryCode: 'mock code'
          },
          homePhone: {
            times: 0,
            days: 0
          },
          otherPhone: {
            times: 0,
            days: 0
          }
        } as CallFrequencyConfigData
      } as any
    } as FormStateMap
  } as RootState;
  const sendLetterConfigActionsSpy = mockActionCreator(sendLetterConfigActions);

  it('should render Modal UI', () => {
    const { wrapper } = renderMockStore(<Add />, { initialState });
    expect(
      wrapper.getAllByText('txt_frequency_add_title').length
    ).toBeGreaterThan(0);
    expect(wrapper.getByText(I18N_COMMON_TEXT.CANCEL)).toBeInTheDocument();
    expect(wrapper.getByText(I18N_COMMON_TEXT.SUBMIT)).toBeInTheDocument();
  });

  it('handleOpenModal', () => {
    const cloneInitialState = cloneDeep(initialState);
    cloneInitialState.sendLetterConfig.addCallFrequencyConfiguration.openModal =
      false;
    const { wrapper } = renderMockStore(<Add />, {
      initialState: cloneInitialState
    });
    const mockOpenModalAction = sendLetterConfigActionsSpy(
      'addFrequencyConfigRequest'
    );

    userEvent.click(wrapper.getByText('txt_frequency_add_title'));

    expect(mockOpenModalAction).toHaveBeenCalled();
  });

  it('handleCancelModal', () => {
    const { wrapper } = renderMockStore(<Add />, {
      initialState
    });
    const mockCancelModalAction = sendLetterConfigActionsSpy(
      'cancelAddFrequencyConfig'
    );

    userEvent.click(wrapper.getByText(I18N_COMMON_TEXT.CANCEL));

    expect(mockCancelModalAction).toHaveBeenCalled();
  });

  describe('handleSubmit', () => {
    it('should be able to submit with data not including form state value', () => {
      const cloneInitialState = cloneDeep(initialState);
      cloneInitialState.form[FREQUENCY_VIEW_NAME]!.values!.state.value =
        'another mock value';
      const mockSubmitAction = sendLetterConfigActionsSpy(
        'triggerAddFrequencyConfiguration'
      );
      const { wrapper } = renderMockStore(<Add />, {
        initialState: cloneInitialState
      });

      userEvent.click(wrapper.getByText(I18N_COMMON_TEXT.SUBMIT));

      expect(mockSubmitAction).toHaveBeenCalledWith({
        postData: cloneInitialState.form[FREQUENCY_VIEW_NAME].values
      });
    });

    it('should be able to submit with data including form state value', () => {
      const { wrapper } = renderMockStore(<Add />, {
        initialState
      });

      userEvent.click(wrapper.getByText(I18N_COMMON_TEXT.SUBMIT));

      expect(
        wrapper.getByText('txt_send_letter_config_inline_message')
      ).toBeInTheDocument();
    });
  });
});
