import React from 'react';
import userEvent from '@testing-library/user-event';

import { Actions } from './Action';
import { CallFrequencyConfigData } from '../types';
import { sendLetterConfigActions } from '../_redux/reducers';
import { renderMockStoreId, mockActionCreator } from 'app/test-utils';
import { I18N_ADJUSTMENT_TRANSACTION } from '../../AdjustmentTransaction/constants';

describe('Test Action component', () => {
  const mockRecord: CallFrequencyConfigData = {
    state: {
      description: 'mock description',
      value: 'mock value',
      countryCode: 'mock code'
    },
    homePhone: {
      times: 0,
      days: 0
    },
    otherPhone: {
      times: 0,
      days: 0
    }
  };
  const sendLetterConfigActionsSpy = mockActionCreator(sendLetterConfigActions);

  it('should render the component', () => {
    const { wrapper } = renderMockStoreId(
      <Actions record={{}} />,
      { initialState: {} },
      false,
      true
    );
    expect(wrapper).toBeTruthy();
    expect(
      wrapper.getByText(I18N_ADJUSTMENT_TRANSACTION.DELETE)
    ).toBeInTheDocument();
    expect(
      wrapper.getByText(I18N_ADJUSTMENT_TRANSACTION.EDIT)
    ).toBeInTheDocument();
  });

  it('handleDeleteRequest', () => {
    const { wrapper } = renderMockStoreId(
      <Actions record={mockRecord} />,
      { initialState: {} },
      true,
      true
    );
    const mockDeleteAction = sendLetterConfigActionsSpy(
      'deleteFrequencyConfigRequest'
    );

    userEvent.click(wrapper.getByText(I18N_ADJUSTMENT_TRANSACTION.DELETE));

    expect(mockDeleteAction).toHaveBeenCalledWith(mockRecord);
  });

  it('handleEditRequest', () => {
    const { wrapper } = renderMockStoreId(
      <Actions record={mockRecord} />,
      { initialState: {} },
      true,
      true
    );
    const mockEditAction = sendLetterConfigActionsSpy(
      'editFrequencyConfigRequest'
    );

    userEvent.click(wrapper.getByText(I18N_ADJUSTMENT_TRANSACTION.EDIT));

    expect(mockEditAction).toHaveBeenCalledWith(mockRecord);
  });
});
