import React from 'react';
import { FormStateMap } from 'redux-form';
import userEvent from '@testing-library/user-event';

import Edit from './Edit';
import { FREQUENCY_VIEW_NAME } from '../constants';
import { CallFrequencyConfigData } from '../types';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { sendLetterConfigActions } from '../_redux/reducers';
import { mockActionCreator, renderMockStore } from 'app/test-utils';

describe('Test Edit component', () => {
  const initialState = {
    sendLetterConfig: {
      editCallFrequencyConfiguration: {
        loading: false,
        error: '',
        openModal: true,
        currentRecord: {
          state: {
            description: 'mock description',
            value: 'mock value',
            countryCode: 'mock code'
          },
          homePhone: {
            times: 0,
            days: 0
          },
          otherPhone: {
            times: 0,
            days: 0
          }
        } as CallFrequencyConfigData
      }
    },
    form: {
      [FREQUENCY_VIEW_NAME]: {
        values: {
          state: {
            description: 'mock description',
            value: 'mock value',
            countryCode: 'mock code'
          },
          homePhone: {
            times: 0,
            days: 0
          },
          otherPhone: {
            times: 0,
            days: 0
          }
        }
      } as any
    } as FormStateMap
  } as RootState;
  const sendLetterConfigActionsSpy = mockActionCreator(sendLetterConfigActions);

  it('should render component', () => {
    const { wrapper } = renderMockStore(<Edit />, { initialState });

    expect(wrapper).toBeTruthy();
    expect(
      wrapper.getByText('txt_call_frequency_edit_title')
    ).toBeInTheDocument();
    expect(wrapper.getByText(I18N_COMMON_TEXT.CANCEL)).toBeInTheDocument();
    expect(wrapper.getByText(I18N_COMMON_TEXT.SAVE)).toBeInTheDocument();
  });

  it('handleCancelModal', () => {
    const mockCancelModalAction = sendLetterConfigActionsSpy(
      'cancelEditFrequencyConfig'
    );
    const { wrapper } = renderMockStore(<Edit />, { initialState });

    userEvent.click(wrapper.getByText(I18N_COMMON_TEXT.CANCEL));

    expect(mockCancelModalAction).toHaveBeenCalled();
  });

  it('handleSubmit', () => {
    const mockSubmitModalAction = sendLetterConfigActionsSpy(
      'triggerEditFrequencyConfiguration'
    );
    const { wrapper } = renderMockStore(<Edit />, { initialState });

    userEvent.click(wrapper.getByText(I18N_COMMON_TEXT.SAVE));

    expect(mockSubmitModalAction).toHaveBeenCalledWith({
      postData: {
        prevRecord:
          initialState.sendLetterConfig.editCallFrequencyConfiguration
            .currentRecord,
        newRecord: initialState.form[FREQUENCY_VIEW_NAME].values
      }
    });
  });
});
