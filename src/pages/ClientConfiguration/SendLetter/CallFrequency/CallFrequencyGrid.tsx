import React, { useCallback, useEffect } from 'react';

// components
import Add from './Add';
import { Actions } from './Action';

// redux
import { useDispatch, useSelector } from 'react-redux';
import {
  selectFrequencyConfigurationData,
  selectFrequencyConfigurationError,
  selectFrequencyConfigurationLoading
} from '../_redux/selectors';

// hooks
import { usePagination } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import classNames from 'classnames';
import { FailedApiReload, NoDataGrid } from 'app/components';
import { I18N_ADJUSTMENT_TRANSACTION } from 'pages/ClientConfiguration/AdjustmentTransaction/constants';
import { Pagination } from 'app/_libraries/_dls/components';
import { sendLetterConfigActions } from '../_redux/reducers';
import { genAmtId } from 'app/_libraries/_dls/utils';

const CallFrequencyGird: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const testId = 'clientConfig_sendLetter_callFrequencyGird';

  const isLoading = useSelector(selectFrequencyConfigurationLoading);
  const isError = useSelector(selectFrequencyConfigurationError);
  const data = useSelector(selectFrequencyConfigurationData);

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(data);

  const showPagination = total > pageSize[0];

  const handleReload = useCallback(() => {
    dispatch(sendLetterConfigActions.getFrequencyConfiguration());
  }, [dispatch]);

  useEffect(() => {
    handleReload();
  }, [handleReload]);

  const Header = ({ className }: { className?: string }) => {
    const { t } = useTranslation();
    return (
      <p
        className={classNames('fs-14 fw-500 color-grey-d20', className)}
        data-testid={genAmtId(testId, 'header', '')}
      >
        {t('txt_call_frequency_config_title_header')}
      </p>
    );
  };

  if (isLoading) return <div className="vh-50" />;

  if (isError)
    return (
      <>
        <Header className="mt-16" />
        <FailedApiReload
          id="send-letter-config-request-fail"
          className="mt-80"
          onReload={handleReload}
        />
      </>
    );
  if (!gridData.length)
    return (
      <>
        <Header className="mt-16" />
        <NoDataGrid text={t(I18N_ADJUSTMENT_TRANSACTION.NO_DATA)}>
          <div className="mt-24">
            <Add />
          </div>
        </NoDataGrid>
      </>
    );

  return (
    <>
      <div className="mt-24 d-flex justify-content-between align-items-center">
        <Header />
        <div className="mr-n8">
          <Add />
        </div>
      </div>
      {data.map((item, index) => {
        return (
          <div
            className="d-flex w-100 border br-light-l04 br-radius-8 px-24 py-16 justify-content-between align-items-xl-center mt-16"
            key={index}
          >
            <div className="call-address d-lg-flex">
              <div className="w-180px mr-xxl-40">
                <p
                  className="fs-11 text-uppercase color-grey-l16 mb-4"
                  data-testid={genAmtId(`${index}_${testId}`, 'configStateTitle', '')}
                >
                  {t('txt_send_letter_config_state')}
                </p>
                <p data-testid={genAmtId(`${index}_${testId}`, 'configState', '')}>
                  {item?.state?.value} - {item?.state?.description}
                </p>
              </div>
              <div className="call-phone d-xl-flex d-md-block ml-lg-24 mt-md-16 mt-lg-0 ml-xxl-40">
                <div className="border-xl-right border-md-none pr-lg-24 mr-lg-24 mb-md-16 mb-xl-0">
                  <p
                    className="fs-11 text-uppercase color-grey-l16 mb-4"
                    data-testid={genAmtId(`${index}_${testId}`, 'homePhoneTitle', '')}
                  >
                    {t('txt_frequency_label_home_phone_maximum_time')}
                  </p>
                  <p data-testid={genAmtId(`${index}_${testId}`, 'homePhone', '')}>
                    {item?.homePhone?.times} times over {item?.homePhone?.days}{' '}
                    days
                  </p>
                </div>
                <div>
                  <p
                    className="fs-11 text-uppercase color-grey-l16 mb-4"
                    data-testid={genAmtId(`${index}_${testId}`, 'otherPhoneTitle', '')}
                  >
                    {t('txt_frequency_label_other_phone_maximum_time')}
                  </p>
                  <p data-testid={genAmtId(`${index}_${testId}`, 'otherPhone', '')}>
                    {item?.otherPhone?.times} times over{' '}
                    {item?.otherPhone?.days} days
                  </p>
                </div>
              </div>
            </div>
            <div className="mr-n8">
              <Actions record={item} dataTestId={`${index}_action_${testId}`} />
            </div>
          </div>
        );
      })}
      {showPagination && (
        <div className="mt-16">
          <Pagination
            totalItem={total}
            pageSize={pageSize}
            pageNumber={currentPage}
            pageSizeValue={currentPageSize}
            compact
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
            dataTestId={`${testId}_pagination`}
          />
        </div>
      )}
    </>
  );
};

export default CallFrequencyGird;
