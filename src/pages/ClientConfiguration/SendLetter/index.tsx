import React from 'react';

// components
import { SimpleBar } from 'app/_libraries/_dls/components';
import LetterGrid from './SendLetterConfigGird';
import SendLetterModal from './SendLetterModal';
import ApiErrorForHeaderSection from '../ApiErrorSection/ApiErrorForHeaderSection';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useSelector } from 'react-redux';
import {
  selectedActionType,
  selectSendLetterModalOpen,
  selectedItemModal
} from './_redux/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';

const SendLetterConfig = () => {
  const { t } = useTranslation();

  const testId = 'clientConfig_sendLetter';

  const isShowModal = useSelector(selectSendLetterModalOpen);

  const actionType = useSelector(selectedActionType);

  const selectedItem = useSelector(selectedItemModal);

  return (
    <div className={'position-relative h-100'}>
      <SimpleBar>
        <div className="mx-auto px-24 pb-24 pt-22 max-width-lg">
          <ApiErrorForHeaderSection
            className="mb-24"
            dataTestId={`${testId}_apiError`}
          />
          <h5 data-testid={genAmtId(testId, 'title', '')}>
            {t('txt_send_letter_config_title')}
          </h5>
          <LetterGrid />
        </div>
        {isShowModal && (
          <SendLetterModal
            actionType={actionType}
            selectedItem={selectedItem}
            isShowModal={isShowModal}
          />
        )}
      </SimpleBar>
    </div>
  );
};

export default SendLetterConfig;
