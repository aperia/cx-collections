import React from 'react';
import { fireEvent, screen } from '@testing-library/dom';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { sendLetterConfigActions } from './_redux/reducers';
import { Actions } from './Actions';
import { SendLetterConfigData } from './types';
import { I18N_ADJUSTMENT_TRANSACTION } from '../AdjustmentTransaction/constants';

const spySendLetterConfigActions = mockActionCreator(sendLetterConfigActions);

describe('Test on Actions component', () => {
  it('should render UI with Edit & Delete buttons', () => {
    renderMockStore(<Actions record={{} as SendLetterConfigData} />, {
      initialState: {}
    });

    expect(
      screen.getByText(I18N_ADJUSTMENT_TRANSACTION.DELETE)
    ).toBeInTheDocument();
    expect(
      screen.getByText(I18N_ADJUSTMENT_TRANSACTION.EDIT)
    ).toBeInTheDocument();
  });

  it('should call actions after clicking on buttons', () => {
    const mockRecord = {
      state: {
        description: 'mock description',
        value: 'mock value',
        countryCode: 'mock code'
      },
      letterNumber: 'mock letter number',
      stateDesc: 'mock state'
    };

    const mockPostData = {
      actionType: 'delete',
      selectedItem: {
        letterNumber: 'mock letter number',
        state: {
          countryCode: 'mock code',
          description: 'mock description',
          value: 'mock value'
        },
        stateDesc: 'mock state'
      }
    };

    const mockUpdateAction = spySendLetterConfigActions(
      'updateSendLetterConfigRequest'
    );

    renderMockStore(<Actions record={mockRecord} />, { initialState: {} });

    fireEvent.click(screen.getByText(I18N_ADJUSTMENT_TRANSACTION.DELETE));
    expect(mockUpdateAction).toHaveBeenCalledWith(mockPostData);

    fireEvent.click(screen.getByText(I18N_ADJUSTMENT_TRANSACTION.EDIT));
    expect(mockUpdateAction).toHaveBeenCalledWith({
      ...mockPostData,
      actionType: 'edit'
    });
  });
});
