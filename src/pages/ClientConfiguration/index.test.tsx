import React from 'react';
import { AccountDetailProvider } from 'app/hooks';
import { mockActionCreator, renderMockStore, storeId } from 'app/test-utils';

import ClientConfigurationNavigation from '.';
import userEvent from '@testing-library/user-event';
import { screen } from '@testing-library/dom';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// redux
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';

const spyActionsTab = mockActionCreator(actionsTab);

describe('Actions', () => {
  it('handleClientConfigurationTab', () => {
    const mockAction = spyActionsTab('addTab');

    renderMockStore(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <ClientConfigurationNavigation />
      </AccountDetailProvider>,
      { initialState: {} }
    );

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.CLIENT_CONFIGURATION));

    expect(mockAction).toBeCalledWith({
      id: 'client-configuration',
      title: I18N_COMMON_TEXT.CLIENT_CONFIGURATION,
      storeId: 'client-configuration',
      tabType: 'clientConfiguration',
      iconName: 'file'
    });
  });
});
