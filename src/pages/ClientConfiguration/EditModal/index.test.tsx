import React from 'react';

//test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

// redux
import { clientConfigurationActions } from '../_redux/reducers';

//components
import EditModal from '.';
import { screen } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';

import * as useClientConfigForm from '../hooks/useClientConfigForm';
import { CODE_ALL } from '../constants';

jest.mock('app/_libraries/_dls/utils/genAmtId', () => () => {});

const initialState: Partial<RootState> = {
  clientConfiguration: {
    openEditModal: true,
    settings: {
      selectedConfig: {
        id: 'id',
        clientId: 'clientId',
        systemId: 'systemId',
        principleId: 'principleId',
        agentId: 'agentId'
      }
    },
    update: {
      errorMessage: 'error',
      isRetry: false
    }
  }
};

const renderWrapper = (state: Partial<RootState>) => {
  return renderMockStore(<EditModal />, { initialState: state });
};

const mockConfigForm = {
  handleChangeRadioOptionAll: jest.fn(),
  handleChangeRadioOptionCustom: jest.fn(),
  handleChangeTextBox: jest.fn(),
  setFieldValue: jest.fn(),
  setValues: jest.fn(),
  handleReset: jest.fn(),
  handleChange: jest.fn(),
  handleBlur: jest.fn(),
  validateForm: jest.fn()
};

const spyClientConfiguration = mockActionCreator(clientConfigurationActions);

HTMLCanvasElement.prototype.getContext = jest.fn();

let spyUseClientConfigForm: jest.SpyInstance;

afterEach(() => {
  spyUseClientConfigForm?.mockReset();
  spyUseClientConfigForm?.mockRestore();
});

describe('Render UI', () => {
  it('should render with empty state', () => {
    const setValues = jest.fn();

    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: false },
          systemRadio: { all: false, custom: false },
          principleRadio: { all: false, custom: false },
          agentRadio: { all: false, custom: false },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: {},
          touched: {},
          setValues,
          handleSubmit: () => onSubmit(values)
        };
      });

    renderWrapper({});

    expect(
      screen.queryByText(I18N_CLIENT_CONFIG.EDIT_CLIENT_CONFIG)
    ).toBeNull();
    expect(setValues).not.toBeCalled();
  });

  it('should render with all checked id', () => {
    const setValues = jest.fn();

    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: true, custom: false },
          systemRadio: { all: true, custom: false },
          principleRadio: { all: true, custom: false },
          agentRadio: { all: true, custom: false },
          clientId: CODE_ALL,
          systemId: CODE_ALL,
          principleId: CODE_ALL,
          agentId: CODE_ALL,
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: {},
          touched: {},
          setValues,
          handleSubmit: () => onSubmit(values)
        };
      });

    renderWrapper({
      clientConfiguration: {
        openEditModal: true,
        settings: {
          selectedConfig: {
            id: 'id',
            clientId: CODE_ALL,
            systemId: CODE_ALL,
            principleId: CODE_ALL,
            agentId: CODE_ALL
          }
        },
        update: {
          errorMessage: 'error'
        }
      }
    });

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.EDIT_CLIENT_CONFIG)
    ).toBeInTheDocument();
    expect(setValues).toBeCalledWith({
      agentId: '',
      agentRadio: {
        all: true,
        custom: false
      },
      clientId: '',
      clientRadio: {
        all: true,
        custom: false
      },
      description: undefined,
      principleId: '',
      principleRadio: {
        all: true,
        custom: false
      },
      systemId: '',
      systemRadio: {
        all: true,
        custom: false
      }
    });
  });

  it('should render', () => {
    const setValues = jest.fn();

    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: false },
          systemRadio: { all: false, custom: false },
          principleRadio: { all: false, custom: false },
          agentRadio: { all: false, custom: false },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: {},
          touched: {},
          setValues,
          handleSubmit: () => onSubmit(values)
        };
      });

    renderWrapper(initialState);

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.EDIT_CLIENT_CONFIG)
    ).toBeInTheDocument();
    expect(setValues).toBeCalledWith({
      agentId: 'agentId',
      agentRadio: {
        all: false,
        custom: true
      },
      clientId: 'clientId',
      clientRadio: {
        all: false,
        custom: true
      },
      description: undefined,
      principleId: 'principleId',
      principleRadio: {
        all: false,
        custom: true
      },
      systemId: 'systemId',
      systemRadio: {
        all: false,
        custom: true
      }
    });
  });

  it('should render when has error description', () => {
    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: false },
          systemRadio: { all: false, custom: false },
          principleRadio: { all: false, custom: false },
          agentRadio: { all: false, custom: false },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: { description: 'error' },
          touched: { description: true },
          handleSubmit: () => onSubmit(values)
        };
      });

    renderWrapper(initialState);

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.EDIT_CLIENT_CONFIG)
    ).toBeInTheDocument();
  });

  it('should render when has error agent', () => {
    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: false },
          systemRadio: { all: false, custom: false },
          principleRadio: { all: false, custom: false },
          agentRadio: { all: false, custom: true },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: { agentId: 'error' },
          touched: { agentId: true },
          handleSubmit: () => onSubmit(values)
        };
      });

    renderWrapper(initialState);

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.EDIT_CLIENT_CONFIG)
    ).toBeInTheDocument();
  });

  it('should render when has error principle', () => {
    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: false },
          systemRadio: { all: false, custom: false },
          principleRadio: { all: false, custom: true },
          agentRadio: { all: false, custom: false },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: { principleId: 'error' },
          touched: { principleId: true },
          handleSubmit: () => onSubmit(values)
        };
      });

    renderWrapper(initialState);

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.EDIT_CLIENT_CONFIG)
    ).toBeInTheDocument();
  });

  it('should render when has error system', () => {
    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: false },
          systemRadio: { all: false, custom: true },
          principleRadio: { all: false, custom: false },
          agentRadio: { all: false, custom: false },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: { systemId: 'error' },
          touched: { systemId: true },
          handleSubmit: () => onSubmit(values)
        };
      });

    renderWrapper(initialState);

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.EDIT_CLIENT_CONFIG)
    ).toBeInTheDocument();
  });

  it('should render when has error client', () => {
    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: true },
          systemRadio: { all: false, custom: false },
          principleRadio: { all: false, custom: false },
          agentRadio: { all: false, custom: false },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: { clientId: 'error' },
          touched: { clientId: true },
          handleSubmit: () => onSubmit(values)
        };
      });

    renderWrapper(initialState);

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.EDIT_CLIENT_CONFIG)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('close modal', () => {
    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: false },
          systemRadio: { all: false, custom: false },
          principleRadio: { all: false, custom: false },
          agentRadio: { all: false, custom: false },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: {},
          touched: {},
          handleSubmit: () => onSubmit(values)
        };
      });

    const mockToggleUpdateModal = spyClientConfiguration('toggleEditModal');

    renderWrapper(initialState);

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));

    expect(mockToggleUpdateModal).toBeCalled();
  });

  it('close modal with isRetry is true', () => {
    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: false },
          systemRadio: { all: false, custom: false },
          principleRadio: { all: false, custom: false },
          agentRadio: { all: false, custom: false },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: {},
          touched: {},
          handleSubmit: () => onSubmit(values)
        };
      });

    const mockToggleUpdateModal = spyClientConfiguration('toggleEditModal');

    renderWrapper({
      clientConfiguration: {
        ...initialState.clientConfiguration,
        update: {
          ...initialState.clientConfiguration?.update,
          isRetry: true
        }
      }
    });

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));

    expect(mockToggleUpdateModal).toBeCalled();
  });

  it('handleClickRetry', () => {
    const setValues = jest.fn();

    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: false },
          systemRadio: { all: false, custom: false },
          principleRadio: { all: false, custom: false },
          agentRadio: { all: false, custom: false },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: {},
          touched: {},
          setValues,
          handleSubmit: () => onSubmit(values)
        };
      });

    renderWrapper({
      clientConfiguration: {
        ...initialState.clientConfiguration,
        update: {
          errorMessage: 'error',
          isRetry: true
        }
      }
    });

    const element = screen.queryByTestId('client-config-retry-text')!;
    userEvent.click(element);
    expect(true).toBeTruthy();
  });

  describe('submit modal', () => {
    it('when radio custom is of', () => {
      spyUseClientConfigForm = jest
        .spyOn(useClientConfigForm, 'useClientConfigForm')
        .mockImplementation((onSubmit: Function) => {
          const values = {
            clientRadio: { all: false, custom: false },
            systemRadio: { all: false, custom: false },
            principleRadio: { all: false, custom: false },
            agentRadio: { all: false, custom: false },
            clientId: 'clientId',
            systemId: 'systemId',
            principleId: 'principleId',
            agentId: 'agentId',
            description: 'description'
          };

          return {
            ...mockConfigForm,
            values,
            errors: {},
            touched: {},
            handleSubmit: () => onSubmit(values)
          };
        });

      const mockAction = spyClientConfiguration(
        'triggerUpdateClientConfiguration'
      );

      renderWrapper(initialState);

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.SAVE));

      expect(mockAction).toBeCalledWith({
        id: 'id',
        agentId: 'agentId',
        clientId: 'clientId',
        defaultCspa: '',
        principleId: 'principleId',
        systemId: 'systemId',
        description: 'description'
      });
    });

    it('when client radio custom turn on', () => {
      spyUseClientConfigForm = jest
        .spyOn(useClientConfigForm, 'useClientConfigForm')
        .mockImplementation((onSubmit: Function) => {
          const values = {
            clientRadio: { all: false, custom: true },
            systemRadio: { all: false, custom: false },
            principleRadio: { all: false, custom: false },
            agentRadio: { all: false, custom: false },
            clientId: 'clientId',
            systemId: 'systemId',
            principleId: 'principleId',
            agentId: 'agentId',
            description: 'description'
          };

          return {
            ...mockConfigForm,
            values,
            errors: {},
            touched: {},
            handleSubmit: () => onSubmit(values)
          };
        });

      const mockAction = spyClientConfiguration(
        'triggerUpdateClientConfiguration'
      );

      renderWrapper(initialState);

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.SAVE));

      expect(mockAction).toBeCalledWith({
        id: 'id',
        agentId: 'agentId',
        defaultCspa: '',
        clientId: 'clientId',
        principleId: 'principleId',
        systemId: 'systemId',
        description: 'description'
      });
    });

    it('when system radio custom turn on', () => {
      spyUseClientConfigForm = jest
        .spyOn(useClientConfigForm, 'useClientConfigForm')
        .mockImplementation((onSubmit: Function) => {
          const values = {
            clientRadio: { all: false, custom: false },
            systemRadio: { all: false, custom: true },
            principleRadio: { all: false, custom: false },
            agentRadio: { all: false, custom: false },
            clientId: 'clientId',
            systemId: 'systemId',
            principleId: 'principleId',
            agentId: 'agentId',
            description: 'description'
          };

          return {
            ...mockConfigForm,
            values,
            errors: {},
            touched: {},
            handleSubmit: () => onSubmit(values)
          };
        });

      const mockAction = spyClientConfiguration(
        'triggerUpdateClientConfiguration'
      );

      renderWrapper(initialState);

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.SAVE));

      expect(mockAction).toBeCalledWith({
        id: 'id',
        agentId: 'agentId',
        clientId: 'clientId',
        defaultCspa: '',
        principleId: 'principleId',
        systemId: 'systemId',
        description: 'description'
      });
    });

    it('when principle radio custom turn on', () => {
      spyUseClientConfigForm = jest
        .spyOn(useClientConfigForm, 'useClientConfigForm')
        .mockImplementation((onSubmit: Function) => {
          const values = {
            clientRadio: { all: false, custom: false },
            systemRadio: { all: false, custom: false },
            principleRadio: { all: false, custom: true },
            agentRadio: { all: false, custom: false },
            clientId: 'clientId',
            systemId: 'systemId',
            principleId: 'principleId',
            agentId: 'agentId',
            description: 'description'
          };

          return {
            ...mockConfigForm,
            values,
            errors: {},
            touched: {},
            handleSubmit: () => onSubmit(values)
          };
        });

      const mockAction = spyClientConfiguration(
        'triggerUpdateClientConfiguration'
      );

      renderWrapper(initialState);

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.SAVE));

      expect(mockAction).toBeCalledWith({
        id: 'id',
        agentId: 'agentId',
        clientId: 'clientId',
        defaultCspa: '',
        principleId: 'principleId',
        systemId: 'systemId',
        description: 'description'
      });
    });

    it('when principle radio custom turn on', () => {
      spyUseClientConfigForm = jest
        .spyOn(useClientConfigForm, 'useClientConfigForm')
        .mockImplementation((onSubmit: Function) => {
          const values = {
            clientRadio: { all: false, custom: false },
            systemRadio: { all: false, custom: false },
            principleRadio: { all: false, custom: false },
            agentRadio: { all: false, custom: true },
            clientId: 'clientId',
            systemId: 'systemId',
            principleId: 'principleId',
            agentId: 'agentId',
            description: 'description'
          };

          return {
            ...mockConfigForm,
            values,
            errors: {},
            touched: {},
            handleSubmit: () => onSubmit(values)
          };
        });

      const mockAction = spyClientConfiguration(
        'triggerUpdateClientConfiguration'
      );

      renderWrapper(initialState);

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.SAVE));

      expect(mockAction).toBeCalledWith({
        id: 'id',
        agentId: 'agentId',
        clientId: 'clientId',
        defaultCspa: '',
        principleId: 'principleId',
        systemId: 'systemId',
        description: 'description'
      });
    });
  });
});
