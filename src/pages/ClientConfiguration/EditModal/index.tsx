import React, { useEffect } from 'react';
import {
  Button,
  Icon,
  InlineMessage,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  Popover,
  Radio,
  TextBox,
  TransDLS
} from 'app/_libraries/_dls/components';
import { useStoreIdSelector } from 'app/hooks';
import {
  selectConfig,
  selectDefaultSettingsClientConfig,
  selectLoadingEdit,
  selectOpenEditModal,
  selectUpdateErrorMessage,
  selectUpdateRetry
} from '../_redux/selectors';
import { batch, useDispatch } from 'react-redux';
import { clientConfigurationActions } from '../_redux/reducers';
import isEmpty from 'lodash.isempty';
import {
  CLIENT_CONFIG_FORM_DESCRIPTION_MAX_LENGTH,
  CLIENT_CONFIG_FORM_MAX_LENGTH,
  CODE_ALL
} from '../constants';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { ConfigData } from '../types';
import { FormValue } from '../types';
import { useClientConfigForm } from '../hooks/useClientConfigForm';
import { useDisableSubmitButton } from 'app/hooks/useDisableSubmitButton';
import { ApiErrorSection } from 'pages/ApiErrorNotification/types';
import { takeApiErrorDetailOpenModalForData } from 'pages/ApiErrorNotification/_redux/selectors';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { genAmtId } from 'app/_libraries/_dls/utils';
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';

const EditModal = () => {
  const { t } = useTranslation();
  const testId = 'clientConfig_editModal';

  const dispatch = useDispatch();

  const isOpenModal = useStoreIdSelector<boolean>(selectOpenEditModal);

  const isLoading = useStoreIdSelector<boolean>(selectLoadingEdit);

  const selectedConfig = useStoreIdSelector<ConfigData>(selectConfig);

  const errorMessage = useStoreIdSelector<boolean>(selectUpdateErrorMessage);

  const defaultClientConfig = useStoreIdSelector<ConfigData>(
    selectDefaultSettingsClientConfig
  );

  const errorModalOpened = useStoreIdSelector<
    Record<ApiErrorSection, MagicKeyValue>
  >(takeApiErrorDetailOpenModalForData, API_ERROR_STORE_ID.ENTITLEMENT);

  const isRetry = useStoreIdSelector<boolean>(selectUpdateRetry);

  const defaultCspa = defaultClientConfig?.cspaId || '';

  const onSubmit = (values: FormValue) => {
    batch(() => {
      dispatch(
        clientConfigurationActions.setEditClientConfigErrorMessage({
          error: '',
          isRetry: false
        })
      );
      dispatch(
        clientConfigurationActions.triggerUpdateClientConfiguration({
          id: selectedConfig.id!,
          clientId: selectedConfig.clientId!,
          systemId: selectedConfig.systemId!,
          principleId: selectedConfig.principleId!,
          agentId: selectedConfig.agentId!,
          description: values.description!.toString(),
          defaultCspa
        })
      );
    });
  };

  const {
    handleChangeRadioOptionAll,
    handleChangeRadioOptionCustom,
    handleChangeDescription,
    handleChangeTextBox,
    values,
    setValues,
    handleReset,
    handleBlur,
    handleSubmit,
    validateForm,
    errors,
    touched
  } = useClientConfigForm(onSubmit);

  const handleCloseModal = () => {
    batch(() => {
      isRetry && dispatch(clientConfigurationActions.getAllClientConfigList());
      dispatch(clientConfigurationActions.toggleEditModal());
      dispatch(
        clientConfigurationActions.setEditClientConfigErrorMessage({
          error: '',
          isRetry: false
        })
      );
    });
    handleReset({});
  };

  const handleClickRetry = () => {
    dispatch(
      clientConfigurationActions.triggerUpdateClientConfiguration({
        id: selectedConfig.id!,
        clientId: selectedConfig.clientId!,
        systemId: selectedConfig.systemId!,
        principleId: selectedConfig.principleId!,
        agentId: selectedConfig.agentId!,
        description: values.description!.toString(),
        defaultCspa
      })
    );
  };

  const { disableSubmit, setPrimitiveValues } = useDisableSubmitButton(values);

  // bind values returned from API
  useEffect(() => {
    if (!selectedConfig || !isOpenModal) return;

    const newValues = {
      clientRadio: {
        all: selectedConfig?.clientId === CODE_ALL,
        custom: selectedConfig?.clientId !== CODE_ALL
      },
      clientId:
        selectedConfig?.clientId === CODE_ALL ? '' : selectedConfig?.clientId,
      systemRadio: {
        all: selectedConfig?.systemId === CODE_ALL,
        custom: selectedConfig?.systemId !== CODE_ALL
      },
      systemId:
        selectedConfig?.systemId === CODE_ALL ? '' : selectedConfig?.systemId,
      principleRadio: {
        all: selectedConfig?.principleId === CODE_ALL,
        custom: selectedConfig?.principleId !== CODE_ALL
      },
      principleId:
        selectedConfig?.principleId === CODE_ALL
          ? ''
          : selectedConfig?.principleId,
      agentRadio: {
        all: selectedConfig?.agentId === CODE_ALL,
        custom: selectedConfig?.agentId !== CODE_ALL
      },
      agentId:
        selectedConfig?.agentId === CODE_ALL ? '' : selectedConfig?.agentId,
      description: selectedConfig?.description
    };

    setValues(newValues);
    setPrimitiveValues(newValues);
    validateForm(newValues);
  }, [
    selectedConfig,
    setValues,
    validateForm,
    setPrimitiveValues,
    isOpenModal
  ]);

  return (
    <Modal
      sm
      show={isEmpty(errorModalOpened) && isOpenModal}
      loading={isLoading}
      dataTestId={testId}
    >
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId={`${testId}_header`}
      >
        <ModalTitle dataTestId={`${testId}_title`}>
          {t(I18N_CLIENT_CONFIG.EDIT_CLIENT_CONFIG)}
        </ModalTitle>
      </ModalHeader>
      <ModalBodyWithApiError
        apiErrorClassName="mb-24"
        storeId={API_ERROR_STORE_ID.ENTITLEMENT}
        dataTestId="client-configuration-error"
      >
        {errorMessage && (
          <InlineMessage
            className="mb-24 mt-n8"
            variant="danger"
            dataTestId={`${testId}_errorMessage`}
          >
            <span>
              {t(errorMessage)}{' '}
              {isRetry && (
                <a
                  onClick={handleClickRetry}
                  className="link text-decoration-none ml-4"
                  data-testid={'client-config-retry-text'}
                >
                  {t('txt_retry')}
                </a>
              )}
            </span>
          </InlineMessage>
        )}
        <div className="d-flex align-items-center mb-16">
          <span
            className="mr-10"
            dât-testid={genAmtId(testId, 'provideHierachy', '')}
          >
            {t(I18N_CLIENT_CONFIG.PROVIDE_HIERARCHY)}
          </span>
          <Popover
            triggerClassName="d-inline-block"
            placement="top"
            size="md"
            element={
              <TransDLS
                keyTranslation={I18N_CLIENT_CONFIG.PROVIDE_HIERARCHY_TOOLTIP}
              >
                <p className="mb-8">
                  The All selection enables you to set the System, Principle,
                  and Agent to include all the entities within that level and
                  below.
                </p>
                <p>
                  For example, when you set the Agent to All, all of your
                  organization’s Agent Identifiers will have the workflow
                  settings you create for the parameter set.
                </p>
              </TransDLS>
            }
          >
            <Button
              variant="icon-secondary"
              size="sm"
              dataTestId={`${testId}_infoBtn`}
            >
              <Icon name="information"></Icon>
            </Button>
          </Popover>
        </div>
        <form>
          <div className="d-flex align-items-center mb-16">
            <div className="d-flex w-227px">
              <p
                className="w-65px color-grey fw-500"
                data-testid={genAmtId(testId, 'clientTitle', '')}
              >
                {t(I18N_CLIENT_CONFIG.CLIENT)}:
              </p>
            </div>
            <div className="flex-1 ml-24">
              <TextBox
                name="clientId"
                label={t(I18N_CLIENT_CONFIG.CLIENT_ID)}
                value={values.clientId}
                readOnly={values.clientRadio.custom}
                type="text"
                dataTestId={`${testId}_clientId`}
              />
            </div>
          </div>
          <div className="mb-16 divider-dashed" />
          <div className="d-flex align-items-center mb-16">
            <div className="d-flex w-227px">
              <p
                className="w-65px color-grey fw-500"
                data-testid={genAmtId(testId, 'systemTitle', '')}
              >
                {t(I18N_CLIENT_CONFIG.SYSTEM)}:
              </p>
            </div>
            <div className="flex-1 ml-24">
              <TextBox
                errorTooltipProps={{
                  triggerClassName: 'w-100'
                }}
                name="systemId"
                label={t(I18N_CLIENT_CONFIG.SYSTEM_ID)}
                value={values.systemRadio.all ? CODE_ALL : values.systemId}
                onChange={handleChangeTextBox}
                onBlur={handleBlur}
                readOnly
                required={values.systemRadio.custom}
                error={{
                  status: Boolean(
                    values.systemRadio.custom &&
                      errors.systemId &&
                      touched.systemId
                  ),
                  message: errors.systemId!
                }}
                type="text"
                maxLength={CLIENT_CONFIG_FORM_MAX_LENGTH}
                dataTestId={`${testId}_systemId`}
              />
            </div>
          </div>
          <div className="mb-16 divider-dashed" />
          <div className="d-flex align-items-center mb-16">
            <div className="d-flex">
              <p
                className="w-65px color-grey fw-500"
                data-testid={genAmtId(testId, 'principleTitle', '')}
              >
                {t(I18N_CLIENT_CONFIG.PRINCIPLE)}:
              </p>
              <Radio className="ml-24" dataTestId={`${testId}_principleAll`}>
                <Radio.Input
                  name="principleRadio"
                  readOnly
                  checked={values.principleRadio.all}
                  onChange={handleChangeRadioOptionAll}
                />
                <Radio.Label>{t(I18N_CLIENT_CONFIG.ALL)}</Radio.Label>
              </Radio>
              <Radio className="ml-24" dataTestId={`${testId}_principleCustom`}>
                <Radio.Input
                  name="principleRadio"
                  checked={values.principleRadio.custom}
                  onChange={handleChangeRadioOptionCustom}
                  readOnly
                />
                <Radio.Label>{t(I18N_CLIENT_CONFIG.CUSTOM)}</Radio.Label>
              </Radio>
            </div>
            <div className="flex-1 ml-24">
              <TextBox
                errorTooltipProps={{
                  triggerClassName: 'w-100'
                }}
                name="principleId"
                label={t(I18N_CLIENT_CONFIG.PRINCIPLE_ID)}
                value={
                  values.principleRadio.all ? CODE_ALL : values.principleId
                }
                onChange={handleChangeTextBox}
                onBlur={handleBlur}
                readOnly
                required={values.principleRadio.custom}
                error={{
                  status: Boolean(
                    values.principleRadio.custom &&
                      errors.principleId &&
                      touched.principleId
                  ),
                  message: errors.principleId!
                }}
                type="text"
                maxLength={CLIENT_CONFIG_FORM_MAX_LENGTH}
                dataTestId={`${testId}_principleId`}
              />
            </div>
          </div>
          <div className="mb-16 divider-dashed" />
          <div className="d-flex align-items-center mb-16">
            <div className="d-flex">
              <p
                className="w-65px color-grey fw-500"
                data-testid={genAmtId(testId, 'agentTitle', '')}
              >
                {t(I18N_CLIENT_CONFIG.AGENT)}:
              </p>
              <Radio className="ml-24" dataTestId={`${testId}_agentAll`}>
                <Radio.Input
                  name="agentRadio"
                  readOnly
                  checked={values.agentRadio.all}
                  onChange={handleChangeRadioOptionAll}
                />
                <Radio.Label>{t(I18N_CLIENT_CONFIG.ALL)}</Radio.Label>
              </Radio>
              <Radio className="ml-24" dataTestId={`${testId}_agentCustom`}>
                <Radio.Input
                  name="agentRadio"
                  checked={values.agentRadio.custom}
                  onChange={handleChangeRadioOptionCustom}
                  readOnly
                />
                <Radio.Label>{t(I18N_CLIENT_CONFIG.CUSTOM)}</Radio.Label>
              </Radio>
            </div>
            <div className="flex-1 ml-24">
              <TextBox
                errorTooltipProps={{
                  triggerClassName: 'w-100'
                }}
                name="agentId"
                label={t(I18N_CLIENT_CONFIG.AGENT_ID)}
                value={values.agentRadio.all ? CODE_ALL : values.agentId}
                onChange={handleChangeTextBox}
                onBlur={handleBlur}
                readOnly
                required={values.agentRadio.custom}
                error={{
                  status: Boolean(
                    values.agentRadio.custom &&
                      errors.agentId &&
                      touched.agentId
                  ),
                  message: errors.agentId!
                }}
                type="text"
                maxLength={CLIENT_CONFIG_FORM_MAX_LENGTH}
                dataTestId={`${testId}_agentId`}
              />
            </div>
          </div>
          <div className="mb-16 divider-dashed" />
          <TextBox
            name="description"
            label={t(I18N_COMMON_TEXT.DESCRIPTION)}
            value={values.description}
            onChange={handleChangeDescription}
            onBlur={handleBlur}
            required
            readOnly={isRetry}
            error={{
              status: Boolean(errors.description && touched.description),
              message: errors.description as string
            }}
            maxLength={CLIENT_CONFIG_FORM_DESCRIPTION_MAX_LENGTH}
            dataTestId={`${testId}_description`}
          />
        </form>
      </ModalBodyWithApiError>
      <ModalFooter
        dataTestId={`${testId}_footer`}
        cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
        okButtonText={t(I18N_COMMON_TEXT.SAVE)}
        disabledOk={isRetry || !isEmpty(errors) || disableSubmit}
        onCancel={handleCloseModal}
        onOk={handleSubmit}
      />
    </Modal>
  );
};

export default EditModal;
