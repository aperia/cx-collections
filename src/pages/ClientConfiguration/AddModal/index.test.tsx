import React from 'react';

//test utils
import { mockActionCreator, renderMockStore, storeId } from 'app/test-utils';

// redux
import { clientConfigurationActions } from '../_redux/reducers';

//components
import AddModal from '.';
import { screen } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';

import * as useClientConfigForm from '../hooks/useClientConfigForm';
import { CODE_ALL } from '../constants';

jest.mock('app/_libraries/_dls/utils/genAmtId', () => () => {});

const initialState: Partial<RootState> = {
  clientConfiguration: {
    openAddModal: true,
    add: {
      errorMessage: 'error'
    }
  },
  apiErrorNotification: {
    [storeId]: {
      openModalFor: 'inMemoFlyOut'
    }
  }
};

const retryState: Partial<RootState> = {
  clientConfiguration: {
    openAddModal: true,
    add: {
      errorMessage: 'error',
      isRetry: true
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(<AddModal />, { initialState });
};

const mockConfigForm = {
  handleChangeRadioOptionAll: jest.fn(),
  handleChangeRadioOptionCustom: jest.fn(),
  handleChangeTextBox: jest.fn(),
  setFieldValue: jest.fn(),
  setValues: jest.fn(),
  handleReset: jest.fn(),
  handleChange: jest.fn(),
  handleBlur: jest.fn(),
  validateForm: jest.fn()
};

const spyClientConfiguration = mockActionCreator(clientConfigurationActions);

HTMLCanvasElement.prototype.getContext = jest.fn();

let spyUseClientConfigForm: jest.SpyInstance;

afterEach(() => {
  spyUseClientConfigForm?.mockReset();
  spyUseClientConfigForm?.mockRestore();
});

describe('Render UI', () => {
  it('should render with empty state', () => {
    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: false },
          systemRadio: { all: false, custom: false },
          principleRadio: { all: false, custom: false },
          agentRadio: { all: false, custom: false },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: {},
          touched: {},
          handleSubmit: () => onSubmit(values)
        };
      });

    renderWrapper({});

    expect(screen.queryByText(I18N_CLIENT_CONFIG.ADD_CLIENT_CONFIG)).toBeNull();
  });

  it('should render with all check all are on', () => {
    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: true, custom: false },
          systemRadio: { all: true, custom: false },
          principleRadio: { all: true, custom: false },
          agentRadio: { all: true, custom: false },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: {},
          touched: {},
          handleSubmit: () => onSubmit(values)
        };
      });

    renderWrapper({});

    expect(screen.queryByText(I18N_CLIENT_CONFIG.ADD_CLIENT_CONFIG)).toBeNull();
  });

  it('should render', () => {
    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: false },
          systemRadio: { all: false, custom: false },
          principleRadio: { all: false, custom: false },
          agentRadio: { all: false, custom: false },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: {},
          touched: {},
          handleSubmit: () => onSubmit(values)
        };
      });

    renderWrapper(initialState);

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.ADD_CLIENT_CONFIG)
    ).toBeInTheDocument();
  });

  it('should render when has error description', () => {
    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: false },
          systemRadio: { all: false, custom: false },
          principleRadio: { all: false, custom: false },
          agentRadio: { all: false, custom: false },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: { description: 'error' },
          touched: { description: true },
          handleSubmit: () => onSubmit(values)
        };
      });

    renderWrapper(initialState);

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.ADD_CLIENT_CONFIG)
    ).toBeInTheDocument();
  });

  it('should render when has error agent', () => {
    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: false },
          systemRadio: { all: false, custom: false },
          principleRadio: { all: false, custom: false },
          agentRadio: { all: false, custom: true },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: { agentId: 'error' },
          touched: { agentId: true },
          handleSubmit: () => onSubmit(values)
        };
      });

    renderWrapper(initialState);

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.ADD_CLIENT_CONFIG)
    ).toBeInTheDocument();
  });

  it('should render when has error principle', () => {
    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: false },
          systemRadio: { all: false, custom: false },
          principleRadio: { all: false, custom: true },
          agentRadio: { all: false, custom: false },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: { principleId: 'error' },
          touched: { principleId: true },
          handleSubmit: () => onSubmit(values)
        };
      });

    renderWrapper(initialState);

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.ADD_CLIENT_CONFIG)
    ).toBeInTheDocument();
  });

  it('should render when has error system', () => {
    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: false },
          systemRadio: { all: false, custom: true },
          principleRadio: { all: false, custom: false },
          agentRadio: { all: false, custom: false },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: { systemId: 'error' },
          touched: { systemId: true },
          handleSubmit: () => onSubmit(values)
        };
      });

    renderWrapper(initialState);

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.ADD_CLIENT_CONFIG)
    ).toBeInTheDocument();
  });

  it('should render when has error client', () => {
    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: true },
          systemRadio: { all: false, custom: false },
          principleRadio: { all: false, custom: false },
          agentRadio: { all: false, custom: false },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: { clientId: 'error' },
          touched: { clientId: true },
          handleSubmit: () => onSubmit(values)
        };
      });

    renderWrapper(initialState);

    expect(
      screen.getByText(I18N_CLIENT_CONFIG.ADD_CLIENT_CONFIG)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('close modal', () => {
    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: false },
          systemRadio: { all: false, custom: false },
          principleRadio: { all: false, custom: false },
          agentRadio: { all: false, custom: false },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: {},
          touched: {},
          handleSubmit: () => onSubmit(values)
        };
      });

    const mockToggleAddModal = spyClientConfiguration('toggleAddModal');
    const mockSetAddClientConfigErrorMessage = spyClientConfiguration(
      'setAddClientConfigErrorMessage'
    );
    renderWrapper(initialState);

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));

    expect(mockToggleAddModal).toBeCalled();
    expect(mockSetAddClientConfigErrorMessage).toBeCalledWith({
      error: '',
      isRetry: false
    });
  });

  it('close modal with retry', () => {
    spyUseClientConfigForm = jest
      .spyOn(useClientConfigForm, 'useClientConfigForm')
      .mockImplementation((onSubmit: Function) => {
        const values = {
          clientRadio: { all: false, custom: false },
          systemRadio: { all: false, custom: false },
          principleRadio: { all: false, custom: false },
          agentRadio: { all: false, custom: false },
          clientId: 'clientId',
          systemId: 'systemId',
          principleId: 'principleId',
          agentId: 'agentId',
          description: 'description'
        };

        return {
          ...mockConfigForm,
          values,
          errors: {},
          touched: {},
          handleSubmit: () => onSubmit(values)
        };
      });

    const mockToggleAddModal = spyClientConfiguration('toggleAddModal');
    const mockSetAddClientConfigErrorMessage = spyClientConfiguration(
      'setAddClientConfigErrorMessage'
    );
    renderWrapper(retryState);

    userEvent.click(screen.getByText(I18N_COMMON_TEXT.CANCEL));

    expect(mockToggleAddModal).toBeCalled();
    expect(mockSetAddClientConfigErrorMessage).toBeCalledWith({
      error: '',
      isRetry: false
    });
  });

  describe('submit modal', () => {
    it('when radio custom is of', () => {
      spyUseClientConfigForm = jest
        .spyOn(useClientConfigForm, 'useClientConfigForm')
        .mockImplementation((onSubmit: Function) => {
          const values = {
            clientRadio: { all: false, custom: false },
            systemRadio: { all: false, custom: false },
            principleRadio: { all: false, custom: false },
            agentRadio: { all: false, custom: false },
            clientId: 'clientId',
            systemId: 'systemId',
            principleId: 'principleId',
            agentId: 'agentId',
            description: 'description'
          };

          return {
            ...mockConfigForm,
            values,
            errors: {},
            touched: {},
            handleSubmit: () => onSubmit(values)
          };
        });

      const mockAction = spyClientConfiguration(
        'triggerAddClientConfiguration'
      );

      renderWrapper(initialState);

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.SUBMIT));

      expect(mockAction).toBeCalledWith({
        agentId: CODE_ALL,
        clientId: '',
        defaultCspa: '',
        principleId: CODE_ALL,
        systemId: 'systemId',
        description: 'description'
      });
    });

    it('when client radio custom is on', () => {
      spyUseClientConfigForm = jest
        .spyOn(useClientConfigForm, 'useClientConfigForm')
        .mockImplementation((onSubmit: Function) => {
          const values = {
            clientRadio: { all: false, custom: true },
            systemRadio: { all: false, custom: false },
            principleRadio: { all: false, custom: false },
            agentRadio: { all: false, custom: false },
            clientId: 'clientId',
            systemId: 'systemId',
            principleId: 'principleId',
            agentId: 'agentId',
            description: 'description'
          };

          return {
            ...mockConfigForm,
            values,
            errors: {},
            touched: {},
            handleSubmit: () => onSubmit(values)
          };
        });

      const mockAction = spyClientConfiguration(
        'triggerAddClientConfiguration'
      );

      renderWrapper(initialState);

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.SUBMIT));

      expect(mockAction).toBeCalledWith({
        agentId: CODE_ALL,
        clientId: '',
        defaultCspa: '',
        principleId: CODE_ALL,
        systemId: 'systemId',
        description: 'description'
      });
    });

    it('when system radio custom is on', () => {
      spyUseClientConfigForm = jest
        .spyOn(useClientConfigForm, 'useClientConfigForm')
        .mockImplementation((onSubmit: Function) => {
          const values = {
            clientRadio: { all: false, custom: false },
            systemRadio: { all: false, custom: true },
            principleRadio: { all: false, custom: false },
            agentRadio: { all: false, custom: false },
            clientId: 'clientId',
            systemId: 'systemId',
            principleId: 'principleId',
            agentId: 'agentId',
            description: 'description'
          };

          return {
            ...mockConfigForm,
            values,
            errors: {},
            touched: {},
            handleSubmit: () => onSubmit(values)
          };
        });

      const mockAction = spyClientConfiguration(
        'triggerAddClientConfiguration'
      );

      renderWrapper(initialState);

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.SUBMIT));

      expect(mockAction).toBeCalledWith({
        agentId: CODE_ALL,
        clientId: '',
        defaultCspa: '',
        principleId: CODE_ALL,
        systemId: 'systemId',
        description: 'description'
      });
    });

    it('when principle radio custom is on', () => {
      spyUseClientConfigForm = jest
        .spyOn(useClientConfigForm, 'useClientConfigForm')
        .mockImplementation((onSubmit: Function) => {
          const values = {
            clientRadio: { all: false, custom: false },
            systemRadio: { all: false, custom: false },
            principleRadio: { all: false, custom: true },
            agentRadio: { all: false, custom: false },
            clientId: 'clientId',
            systemId: 'systemId',
            principleId: 'principleId',
            agentId: 'agentId',
            description: 'description'
          };

          return {
            ...mockConfigForm,
            values,
            errors: {},
            touched: {},
            handleSubmit: () => onSubmit(values)
          };
        });

      const mockAction = spyClientConfiguration(
        'triggerAddClientConfiguration'
      );

      renderWrapper(initialState);

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.SUBMIT));

      expect(mockAction).toBeCalledWith({
        agentId: CODE_ALL,
        clientId: '',
        defaultCspa: '',
        principleId: 'principleId',
        systemId: 'systemId',
        description: 'description'
      });
    });

    it('when principle radio custom is on', () => {
      spyUseClientConfigForm = jest
        .spyOn(useClientConfigForm, 'useClientConfigForm')
        .mockImplementation((onSubmit: Function) => {
          const values = {
            clientRadio: { all: false, custom: false },
            systemRadio: { all: false, custom: false },
            principleRadio: { all: false, custom: false },
            agentRadio: { all: false, custom: true },
            clientId: 'clientId',
            systemId: 'systemId',
            principleId: 'principleId',
            agentId: 'agentId',
            description: 'description'
          };

          return {
            ...mockConfigForm,
            values,
            errors: {},
            touched: {},
            handleSubmit: () => onSubmit(values)
          };
        });

      const mockAction = spyClientConfiguration(
        'triggerAddClientConfiguration'
      );

      renderWrapper(initialState);

      userEvent.click(screen.getByText(I18N_COMMON_TEXT.SUBMIT));

      expect(mockAction).toBeCalledWith({
        agentId: 'agentId',
        clientId: '',
        defaultCspa: '',
        principleId: CODE_ALL,
        systemId: 'systemId',
        description: 'description'
      });
    });

    it('handleClickRetry ', () => {
      const spy = spyClientConfiguration('triggerAddClientConfiguration');
      renderWrapper(retryState);

      const button = screen.getByText('txt_retry')!;
      button.click();
      expect(spy).toBeCalledWith({
        clientId: '',
        systemId: undefined,
        principleId: undefined,
        agentId: undefined,
        description: undefined,
        defaultCspa: ''
      });
    });
  });
});
