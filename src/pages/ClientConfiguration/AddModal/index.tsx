import React, { useEffect } from 'react';
import isEmpty from 'lodash.isempty';

// components
import {
  Button,
  Icon,
  InlineMessage,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  Popover,
  Radio,
  TextBox,
  TransDLS
} from 'app/_libraries/_dls/components';

// hooks
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { batch, useDispatch } from 'react-redux';
import { useStoreIdSelector } from 'app/hooks';
import { useClientConfigForm } from '../hooks/useClientConfigForm';

// type
import { ConfigData, FormValue } from '../types';

// constant
import {
  CLIENT_CONFIG_FORM_DESCRIPTION_MAX_LENGTH,
  CLIENT_CONFIG_FORM_MAX_LENGTH,
  CODE_ALL,
  CLIENT_CONFIG_FORM_FIELDS
} from '../constants';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';
const {
  SYSTEM_ID,
  PRINCIPLE_ID,
  PRINCIPLE_RADIO,
  AGENT_ID,
  AGENT_RADIO,
  DESCRIPTION
} = CLIENT_CONFIG_FORM_FIELDS;

// redux
import { clientConfigurationActions } from '../_redux/reducers';
import {
  selectAddErrorMessage,
  selectAddRetry,
  selectDefaultSettingsClientConfig,
  selectLoadingAdd,
  selectOpenAddModal
} from '../_redux/selectors';
import { ApiErrorSection } from 'pages/ApiErrorNotification/types';
import { takeApiErrorDetailOpenModalForData } from 'pages/ApiErrorNotification/_redux/selectors';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { genAmtId } from 'app/_libraries/_dls/utils';
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';

const AddModal = () => {
  const { t } = useTranslation();

  const dispatch = useDispatch();
  const testId = 'clientConfig_addModal';

  const isOpenModal = useStoreIdSelector<boolean>(selectOpenAddModal);

  const isLoading = useStoreIdSelector<boolean>(selectLoadingAdd);

  const errorMessage = useStoreIdSelector<boolean>(selectAddErrorMessage);

  const isRetry = useStoreIdSelector<boolean>(selectAddRetry);

  const defaultClientConfig = useStoreIdSelector<ConfigData>(
    selectDefaultSettingsClientConfig
  );

  const errorModalOpened = useStoreIdSelector<
    Record<ApiErrorSection, MagicKeyValue>
  >(takeApiErrorDetailOpenModalForData, API_ERROR_STORE_ID.ENTITLEMENT);

  const defaultCspa = defaultClientConfig?.cspaId || '';

  const defaultClientId = defaultClientConfig?.clientId || '';

  const [CSPA, setCSPA] = React.useState<MagicKeyValue>({});

  const onSubmit = (eValues: FormValue) => {
    setCSPA({
      systemId: eValues?.systemId!.toString(),
      principleId: eValues?.principleRadio?.custom
        ? eValues.principleId!.toString()
        : CODE_ALL,
      agentId: eValues?.agentRadio?.custom
        ? eValues.agentId!.toString()
        : CODE_ALL,
      description: eValues.description!.toString()
    });

    batch(() => {
      dispatch(
        clientConfigurationActions.setAddClientConfigErrorMessage({
          error: '',
          isRetry: false
        })
      );
      dispatch(
        clientConfigurationActions.triggerAddClientConfiguration({
          clientId: defaultClientId,
          systemId: eValues.systemId!.toString(),
          principleId: eValues?.principleRadio?.custom
            ? eValues.principleId!.toString()
            : CODE_ALL,
          agentId: eValues?.agentRadio?.custom
            ? eValues.agentId!.toString()
            : CODE_ALL,
          description: eValues.description!.toString(),
          defaultCspa
        })
      );
    });
  };

  const {
    handleChangeRadioOptionAll,
    handleChangeRadioOptionCustom,
    handleChangeTextBox,
    handleChangeDescription,
    values,
    handleReset,
    handleBlur,
    handleSubmit,
    validateForm,
    errors,
    touched
  } = useClientConfigForm(onSubmit);

  const handleCloseModal = () => {
    batch(() => {
      isRetry && dispatch(clientConfigurationActions.getAllClientConfigList());
      dispatch(clientConfigurationActions.toggleAddModal());
      dispatch(
        clientConfigurationActions.setAddClientConfigErrorMessage({
          error: '',
          isRetry: false
        })
      );
    });
  };

  const handleClickRetry = () => {
    dispatch(
      clientConfigurationActions.triggerAddClientConfiguration({
        clientId: defaultClientId,
        systemId: CSPA.systemId,
        principleId: CSPA.principleId,
        agentId: CSPA.agentId,
        description: CSPA.description,
        defaultCspa
      })
    );
  };

  // revalidate form after close modal
  useEffect(() => {
    if (!isOpenModal) return;
    handleReset({});
    validateForm();
  }, [isOpenModal, validateForm, handleReset]);

  return (
    <Modal
      sm
      show={isEmpty(errorModalOpened) && isOpenModal}
      loading={isLoading}
      dataTestId={testId}
    >
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId={`${testId}_header`}
      >
        <ModalTitle dataTestId={`${testId}_title`}>
          {t(I18N_CLIENT_CONFIG.ADD_CLIENT_CONFIG)}
        </ModalTitle>
      </ModalHeader>
      <ModalBodyWithApiError
        apiErrorClassName="mb-24"
        storeId={API_ERROR_STORE_ID.ENTITLEMENT}
        dataTestId="client-configuration-error"
      >
        {errorMessage && (
          <InlineMessage
            className="mb-24 mt-n8"
            variant="danger"
            dataTestId={`${testId}_errorMessage`}
          >
            <span>
              {t(errorMessage)}{' '}
              {isRetry && (
                <a
                  onClick={handleClickRetry}
                  className="link text-decoration-none ml-4"
                  data-testid={'client-config-retry-text'}
                >
                  {t('txt_retry')}
                </a>
              )}
            </span>
          </InlineMessage>
        )}
        <div className="mb-16 d-flex align-items-center">
          <span
            className="mr-10"
            data-testid={genAmtId(testId, 'provideHierachyTitle', '')}
          >
            {t(I18N_CLIENT_CONFIG.PROVIDE_HIERARCHY)}
          </span>
          <Popover
            triggerClassName="d-inline-block"
            placement="top"
            size="md"
            element={
              <TransDLS
                keyTranslation={I18N_CLIENT_CONFIG.PROVIDE_HIERARCHY_TOOLTIP}
              >
                <p className="mb-8">{t('txt_hierarchy_tooltip_title')}</p>
                <p>{t('txt_hierarchy_tooltip_sub_title')}</p>
              </TransDLS>
            }
          >
            <Button
              variant="icon-secondary"
              size="sm"
              dataTestId={`${testId}_infoBtn`}
            >
              <Icon name="information"></Icon>
            </Button>
          </Popover>
        </div>
        <form>
          <div className="d-flex align-items-center mb-16">
            <div className="d-flex w-227px">
              <p
                className="w-65px color-grey fw-500"
                data-testid={genAmtId(testId, 'clientTitle', '')}
              >
                {t(I18N_CLIENT_CONFIG.CLIENT)}:
              </p>
            </div>
            <div className="flex-1 ml-24">
              <TextBox
                label={t(I18N_CLIENT_CONFIG.CLIENT_ID)}
                value={defaultClientId}
                readOnly={isRetry ? isRetry : values.clientRadio.custom}
                type="text"
                dataTestId={`${testId}_clientId`}
              />
            </div>
          </div>
          <div className="mb-16 divider-dashed" />
          <div className="d-flex align-items-center mb-16">
            <div className="d-flex w-227px">
              <p
                className="w-65px color-grey fw-500"
                data-testid={genAmtId(testId, 'systemTitle', '')}
              >
                {t(I18N_CLIENT_CONFIG.SYSTEM)}:
              </p>
            </div>
            <div className="flex-1 ml-24">
              <TextBox
                errorTooltipProps={{
                  triggerClassName: 'w-100'
                }}
                name={SYSTEM_ID}
                label={t(I18N_CLIENT_CONFIG.SYSTEM_ID)}
                value={values.systemId}
                onChange={handleChangeTextBox}
                onBlur={handleBlur}
                readOnly={isRetry ? isRetry : values.systemRadio.all}
                required={values.systemRadio.custom}
                error={{
                  status: Boolean(
                    values.systemRadio.custom &&
                      errors.systemId &&
                      touched.systemId
                  ),
                  message: errors.systemId!
                }}
                type="text"
                maxLength={CLIENT_CONFIG_FORM_MAX_LENGTH}
                dataTestId={`${testId}_systemId`}
              />
            </div>
          </div>
          <div className="mb-16 divider-dashed" />
          <div className="d-flex align-items-center mb-16">
            <div className="d-flex">
              <p
                className="w-65px color-grey fw-500"
                data-testid={genAmtId(testId, 'principleTitle', '')}
              >
                {t(I18N_CLIENT_CONFIG.PRINCIPLE)}:
              </p>
              <Radio className="ml-24" dataTestId={`${testId}_principle`}>
                <Radio.Input
                  name={PRINCIPLE_RADIO}
                  checked={values.principleRadio.all}
                  onChange={handleChangeRadioOptionAll}
                  readOnly={isRetry}
                />
                <Radio.Label>{t(I18N_CLIENT_CONFIG.ALL)}</Radio.Label>
              </Radio>
              <Radio className="ml-24">
                <Radio.Input
                  name={PRINCIPLE_RADIO}
                  checked={values.principleRadio.custom}
                  onChange={handleChangeRadioOptionCustom}
                  disabled={isRetry ? isRetry : values.systemRadio.all}
                />
                <Radio.Label>{t(I18N_CLIENT_CONFIG.CUSTOM)}</Radio.Label>
              </Radio>
            </div>

            <div className="flex-1 ml-24">
              <TextBox
                errorTooltipProps={{
                  triggerClassName: 'w-100'
                }}
                name={PRINCIPLE_ID}
                label={t(I18N_CLIENT_CONFIG.PRINCIPLE_ID)}
                value={
                  values.principleRadio.all ? CODE_ALL : values.principleId
                }
                onChange={handleChangeTextBox}
                onBlur={handleBlur}
                readOnly={isRetry ? isRetry : values.principleRadio.all}
                required={values.principleRadio.custom}
                error={{
                  status: Boolean(
                    values.principleRadio.custom &&
                      errors.principleId &&
                      touched.principleId
                  ),
                  message: errors.principleId!
                }}
                type="text"
                maxLength={CLIENT_CONFIG_FORM_MAX_LENGTH}
                dataTestId={`${testId}_principleId`}
              />
            </div>
          </div>
          <div className="mb-16 divider-dashed" />
          <div className="d-flex align-items-center mb-16">
            <div className="d-flex">
              <p
                className="w-65px color-grey fw-500"
                data-testid={genAmtId(testId, 'agentTitle', '')}
              >
                {t(I18N_CLIENT_CONFIG.AGENT)}:
              </p>
              <Radio className="ml-24" dataTestId={`${testId}_agent`}>
                <Radio.Input
                  name={AGENT_RADIO}
                  checked={values.agentRadio.all}
                  onChange={handleChangeRadioOptionAll}
                  readOnly={isRetry}
                />
                <Radio.Label>{t(I18N_CLIENT_CONFIG.ALL)}</Radio.Label>
              </Radio>
              <Radio className="ml-24">
                <Radio.Input
                  name={AGENT_RADIO}
                  checked={values.agentRadio.custom}
                  onChange={handleChangeRadioOptionCustom}
                  disabled={isRetry ? isRetry : values.principleRadio.all}
                />
                <Radio.Label>{t(I18N_CLIENT_CONFIG.CUSTOM)}</Radio.Label>
              </Radio>
            </div>
            <div className="flex-1 ml-24">
              <TextBox
                errorTooltipProps={{
                  triggerClassName: 'w-100'
                }}
                name={AGENT_ID}
                label={t(I18N_CLIENT_CONFIG.AGENT_ID)}
                value={values.agentRadio.all ? CODE_ALL : values.agentId}
                onChange={handleChangeTextBox}
                onBlur={handleBlur}
                readOnly={isRetry ? isRetry : values.agentRadio.all}
                required={values.agentRadio.custom}
                error={{
                  status: Boolean(
                    values.agentRadio.custom &&
                      errors.agentId &&
                      touched.agentId
                  ),
                  message: errors.agentId!
                }}
                type="text"
                maxLength={CLIENT_CONFIG_FORM_MAX_LENGTH}
                dataTestId={`${testId}_agentId`}
              />
            </div>
          </div>
          <div className="mb-16 divider-dashed" />
          <TextBox
            name={DESCRIPTION}
            label={t(I18N_COMMON_TEXT.DESCRIPTION)}
            value={values.description}
            onChange={handleChangeDescription}
            onBlur={handleBlur}
            required
            readOnly={isRetry}
            error={{
              status: Boolean(errors.description && touched.description),
              message: errors.description!
            }}
            maxLength={CLIENT_CONFIG_FORM_DESCRIPTION_MAX_LENGTH}
            dataTestId={`${testId}_description`}
          />
        </form>
      </ModalBodyWithApiError>
      <ModalFooter
        cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
        okButtonText={t(I18N_COMMON_TEXT.SUBMIT)}
        disabledOk={isRetry || !isEmpty(errors)}
        onCancel={handleCloseModal}
        onOk={handleSubmit}
        dataTestId={`${testId}_footer`}
      />
    </Modal>
  );
};

export default AddModal;
