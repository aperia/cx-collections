import { AdditionalFieldsMapType } from './../types';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { AxiosResponse } from 'axios';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { clientConfigurationServices } from '../clientConfigurationServices';
import {
  CollectionsClientConfigGroup,
  ISaveCollectionsClientConfigArgs,
  ISaveCollectionsClientConfigServiceArgs
} from '../types';
import { CLIENT_CONFIG_STATUS_KEY } from '../constants';

export const saveCollectionsClientConfig = createAsyncThunk<
  AxiosResponse,
  ISaveCollectionsClientConfigArgs,
  ThunkAPIConfig
>('clientConfiguration/saveCollectionsClientConfig', async (args, thunkAPI) => {
  const { component, jsonValue, updateAction } = args;
  const { dispatch, getState } = thunkAPI;
  const state = getState();

  try {
    const {
      agentId,
      clientId,
      systemId,
      principleId,
      description,
      additionalFields
    } = state.clientConfiguration.settings?.selectedConfig || {};
    const cspa = `${clientId}-${systemId}-${principleId}-${agentId}`;
    let groupKey = CollectionsClientConfigGroup.CodeToText;

    const activeConfig = additionalFields?.find(
      (item: AdditionalFieldsMapType) => item.name === CLIENT_CONFIG_STATUS_KEY
    );

    if (
      args.component === 'component_authentication' ||
      args.component === 'account-external-status-component'
    ) {
      groupKey = CollectionsClientConfigGroup.ClientConfig;
    }

    const { user, app, org, privileges } = window.appConfig.commonConfig;

    const requestBody: ISaveCollectionsClientConfigServiceArgs = {
      common: {
        agent: agentId,
        clientNumber: clientId,
        system: systemId,
        prin: principleId,
        user,
        app,
        org,
        privileges
      },
      [groupKey]: {
        cspas: [
          {
            cspa,
            components: [
              {
                component,
                jsonValue
              }
            ],
            description,
            active: activeConfig?.active
          }
        ]
      }
    };

    const response =
      await clientConfigurationServices.saveCollectionsClientConfig(
        requestBody
      );

    if (!response.data.configs) throw response;

    if (updateAction === 'delete') {
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION,
          forSection: 'inClientConfigurationFeatureHeader'
        })
      );
    } else {
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION,
          forSection: 'inModalBody'
        })
      );
    }

    return response;
  } catch (error) {
    if (updateAction === 'delete') {
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION,
          forSection: 'inClientConfigurationFeatureHeader',
          apiResponse: error
        })
      );
    } else {
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION,
          forSection: 'inModalBody',
          apiResponse: error
        })
      );
    }

    return thunkAPI.rejectWithValue({ response: error });
  }
});
