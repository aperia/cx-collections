import { createAsyncThunk, isFulfilled } from '@reduxjs/toolkit';
import { clientConfigurationActions } from './reducers';

export const triggerUpdateSelectedConfiguration = createAsyncThunk<
  void,
  {
    clientId: string;
    systemId: string;
    principleId: string;
    agentId: string;
  },
  ThunkAPIConfig
>(
  'clientConfiguration/triggerUpdateSelectedConfig',
  async (
    { clientId, systemId, principleId, agentId },
    { rejectWithValue, dispatch }
  ) => {
    try {
      const response = await dispatch(
        clientConfigurationActions.getAllClientConfigList()
      );
      if (isFulfilled(response)) {
        // trigger update

        dispatch(
          clientConfigurationActions.updateSelectedConfig({
            data: {
              id: `${clientId}${systemId}${principleId}${agentId}`,
              clientId,
              systemId,
              principleId,
              agentId
            }
          })
        );
        return;
      }
      return;
    } catch (error) {
      return rejectWithValue({ response: error });
    }
  }
);
