import { createStore, Store } from '@reduxjs/toolkit';
import { byPassFulfilled, byPassRejected } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import { clientConfigurationServices } from '../clientConfigurationServices';
import './reducers';
import { triggerUpdateSelectedConfiguration } from './triggerUpdateSelectedConfiguration';

let store: Store<RootState>;

let spy: jest.SpyInstance;

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    user: 'user',
    org: 'org',
    app: 'app'
  }
};

describe('Test triggerUpdateSelectedConfiguration async thunk', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });
  beforeEach(() => {
    store = createStore(rootReducer, {});
  });

  it('FulFilled', async () => {
    jest
      .spyOn(clientConfigurationServices, 'clientInfoGetList')
      .mockReturnValue({});
    const res = await triggerUpdateSelectedConfiguration({} as never)(
      byPassFulfilled(
        'clientConfiguration/getAllClientConfigListApi/fulfilled',
        {
          data: [
            {
              id: '123',
              clientId: '0000',
              systemId: '1111',
              principleId: '2222',
              agentId: '3333'
            }
          ]
        }
      ),
      store.getState,
      {}
    );
    expect(res.type).toEqual(
      'clientConfiguration/triggerUpdateSelectedConfig/fulfilled'
    );
  });

  it('Rejected > case 1', async () => {
    jest
      .spyOn(clientConfigurationServices, 'clientInfoGetList')
      .mockReturnValue({});
    const res = await triggerUpdateSelectedConfiguration({} as never)(
      byPassRejected('clientConfiguration/getAllClientConfigListApi/rejected', {
        data: undefined
      }),
      store.getState,
      {}
    );
    expect(res.type).toEqual(
      'clientConfiguration/triggerUpdateSelectedConfig/fulfilled'
    );
  });

  it('Rejected > case 2', async () => {
    jest
      .spyOn(clientConfigurationServices, 'clientInfoGetList')
      .mockRejectedValue({});

    const res = await triggerUpdateSelectedConfiguration({} as never)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(res.type).toEqual(
      'clientConfiguration/triggerUpdateSelectedConfig/rejected'
    );
  });
});
