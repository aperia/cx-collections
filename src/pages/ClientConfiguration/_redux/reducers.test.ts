import { ClientConfigurationState } from '../types';
import { clientConfigurationActions, reducer } from './reducers';

describe('Test Client Configuration Reducers', () => {
  const initialState: ClientConfigurationState = {
    openAddModal: false,
    openEditModal: false,
    openDeleteModal: false,
    loadingAddModal: false,
    loadingEditModal: false,
    loadingDeleteModal: false
  };

  it('toggleSettingsModal', () => {
    const state = reducer(
      initialState,
      clientConfigurationActions.toggleSettingsModal()
    );
    expect(state.settings?.isOpen).toEqual(true);
  });

  it('updateSelectedConfig > should set data to selectedConfig', () => {
    const data = { test: true };
    const state = reducer(
      initialState,
      clientConfigurationActions.updateSelectedConfig({ data })
    );
    expect(state.settings?.selectedConfig).toEqual(data);
  });

  it('toggleAddModal > should be true', () => {
    const state = reducer(
      initialState,
      clientConfigurationActions.toggleAddModal()
    );
    expect(state.openAddModal).toEqual(true);
  });

  it('toggleEditModal > should be true', () => {
    const state = reducer(
      initialState,
      clientConfigurationActions.toggleEditModal()
    );
    expect(state.openEditModal).toEqual(true);
  });

  it('toggleStatusModal > should be true', () => {
    const state = reducer(
      initialState,
      clientConfigurationActions.toggleStatusModal()
    );
    expect(state.openStatusModal).toEqual(true);
  });

  it('updateSortBy > should set data to sortBy', () => {
    const state = reducer(
      initialState,
      clientConfigurationActions.updateSortBy({ id: 'test', order: 'asc' })
    );
    expect(state.list?.sortBy).toEqual({ id: 'test', order: 'asc' });
  });

  it('setAddClientConfigErrorMessage > should set data to errorMessage in add', () => {
    const state = reducer(
      initialState,
      clientConfigurationActions.setAddClientConfigErrorMessage('Error')
    );
    expect(state.add?.errorMessage).toBeUndefined();
  });

  it('setEditClientConfigErrorMessage', () => {
    const state = reducer(
      initialState,
      clientConfigurationActions.setEditClientConfigErrorMessage({
        error: 'Error',
        isRetry: true
      })
    );
    expect(state.update?.errorMessage).toEqual('Error');
  });

  it('setConfigWarningMessage', () => {
    const state = reducer(
      initialState,
      clientConfigurationActions.setConfigWarningMessage('Error')
    );
    expect(state.warningMessage).toEqual('Error');
  });

  it('toggleSetting > should be true', () => {
    const state = reducer(
      initialState,
      clientConfigurationActions.toggleSetting(true)
    );
    expect(state.settings?.toggle).toEqual(true);
  });
  it('changeSearchValue', () => {
    const state = reducer(
      initialState,
      clientConfigurationActions.changeSearchValue({
        searchValue: 'searchValue'
      })
    );
    expect(state.list?.searchValue).toEqual('searchValue');
  });
  it('changeTextSearchValue', () => {
    const state = reducer(
      initialState,
      clientConfigurationActions.changeTextSearchValue({
        textSearchValue: 'textSearchValue'
      })
    );
    expect(state.list?.textSearchValue).toEqual('textSearchValue');
  });
  it('clearAndReset', () => {
    const state = reducer(
      initialState,
      clientConfigurationActions.clearAndReset()
    );
    expect(state.list?.textSearchValue).toEqual('');
    expect(state.list?.searchValue).toEqual('');
  });
  it('removeStore', () => {
    const state = reducer(
      initialState,
      clientConfigurationActions.removeStore()
    );

    expect(state.list).toEqual(undefined);
  });
  it('setLastHistory', () => {
    const state = reducer(
      initialState,
      clientConfigurationActions.setLastHistory({})
    );

    expect(state.lastHistory).toEqual({});
  });
});
