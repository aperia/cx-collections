import { createStore, Store } from '@reduxjs/toolkit';
import { rootReducer } from 'storeConfig';
import { clientConfigurationServices } from '../clientConfigurationServices';
import './reducers';
import { triggerSaveClientConfiguration } from './triggerSaveClientConfiguration';

let store: Store<RootState>;

let spy: jest.SpyInstance;

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    user: 'user',
    org: 'org',
    app: 'app'
  }
};

describe('Test triggerSaveClientConfiguration async thunk', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });
  beforeEach(() => {
    store = createStore(rootReducer, {});
  });

  it('FulFilled', async () => {
    jest
      .spyOn(clientConfigurationServices, 'saveCollectionsClientConfig')
      .mockReturnValue({});
    const res = await triggerSaveClientConfiguration({} as never)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(res.type).toEqual(
      'clientConfiguration/triggerSaveClientConfiguration/fulfilled'
    );
  });

  it('Rejected', async () => {
    jest
      .spyOn(clientConfigurationServices, 'saveCollectionsClientConfig')
      .mockRejectedValue({});

    const res = await triggerSaveClientConfiguration({} as never)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(res.type).toEqual(
      'clientConfiguration/triggerSaveClientConfiguration/rejected'
    );
  });
});
