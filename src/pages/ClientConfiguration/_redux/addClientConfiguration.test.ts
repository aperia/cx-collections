import { clientConfigurationServices } from './../clientConfigurationServices';
import {
  mockActionCreator,
  responseDefault,
  storeId,
  byPassFulfilled
} from 'app/test-utils';
import {
  addClientConfiguration,
  triggerAddClientConfiguration
} from './addClientConfiguration';
import { applyMiddleware, createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';

import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import {
  CreateClientConfigurationRequest,
  AddClientConfigurationArgs
} from '../types';
import {
  BINSEQUENCES,
  DEFAULT_COMMON_REQUEST_CLIENT_CONFIG
} from '../constants';
import { clientConfigurationActions } from './reducers';
import thunk from 'redux-thunk';
import * as saveChangedClientConfigMock from '../ChangeHistory/_redux/saveChangedClientConfig';
import * as helpersMock from '../helpers';

const requestActionData: AddClientConfigurationArgs = {
  clientId: '',
  systemId: '',
  principleId: '',
  agentId: '',
  description: '',
  defaultCspa: 'defaultCspa'
};

const requestBody: CreateClientConfigurationRequest = {
  ...DEFAULT_COMMON_REQUEST_CLIENT_CONFIG,
  agent: '1234',
  clientNumber: '1234',
  system: '1234',
  prin: '1234',
  userId: 'FDESMAST',
  portfolioName: 'test',
  x500Id: 'AAAA1114',
  orgId: 'COLX',
  binSequences: BINSEQUENCES
};

let spy: jest.SpyInstance;

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    user: 'user',
    org: 'org',
    app: 'app',
    x500id: 'x500id'
  }
};

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

const mockAction = mockActionCreator(clientConfigurationActions);
const mockActionToast = mockActionCreator(actionsToast);

describe('ClientConfiguration > addClientConfiguration', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {}, applyMiddleware(thunk));
  });

  it('success', async () => {
    spy = jest.spyOn(clientConfigurationServices, 'add').mockResolvedValue({
      ...responseDefault,
      data: { data: { message: 'success' } }
    });

    const response = await addClientConfiguration(requestBody)(
      store.dispatch,
      store.getState,
      undefined
    );

    expect(response.payload).toEqual({
      ...responseDefault,
      data: { data: { message: 'success' } }
    });
  });

  it('error', async () => {
    window.appConfig = {} as AppConfiguration;

    spy = jest.spyOn(clientConfigurationServices, 'add').mockRejectedValue({
      ...responseDefault,
      data: { data: { message: 'failed' } }
    });

    const response = await addClientConfiguration(requestBody)(
      store.dispatch,
      store.getState,
      undefined
    );

    expect(response.type).toBe(addClientConfiguration.rejected.type);
  });
});

describe('ClientConfiguration > triggerAddClientConfiguration', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(
      rootReducer,
      { clientConfiguration: { add: { isRetry: true } } },
      applyMiddleware(thunk)
    );
  });

  describe('success', () => {
    it('fulfilled', async () => {
      spy = jest.spyOn(clientConfigurationServices, 'add').mockResolvedValue({
        ...responseDefault,
        data: { data: { message: 'success' } }
      });

      const mockAddToast = jest
        .fn()
        .mockImplementation(() => ({ type: 'type' }));
      jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);

      await triggerAddClientConfiguration(requestActionData)(
        jest.fn().mockResolvedValue({
          meta: {
            arg: {
              storeId: storeId
            },
            requestStatus: 'fulfilled',
            requestId: 'mockRequestID'
          },
          type: 'clientConfiguration/triggerAddClientConfiguration/fulfilled',
          payload: {
            data: {}
          }
        }),
        store.getState,
        {}
      );
    });

    it('resolved on response 2', async () => {
      store = createStore(
        rootReducer,
        {
          clientConfiguration: {
            add: {
              isRetry: false
            }
          }
        },
        applyMiddleware(thunk)
      );

      spy = jest.spyOn(clientConfigurationServices, 'add').mockResolvedValue({
        ...responseDefault,
        data: { data: { message: 'success' } }
      });

      mockAction('triggerSaveClientConfiguration', () => ({
        type: 'clientConfiguration/triggerSaveClientConfiguration/fulfilled',
        meta: {
          arg: {},
          requestId: 'test request id',
          requestStatus: 'fulfilled'
        }
      }));

      const spySaveChangedClientConfig = mockActionCreator(
        saveChangedClientConfigMock
      );
      const spyHelpers = mockActionCreator(helpersMock);
      const action = spySaveChangedClientConfig('saveChangedClientConfig');
      const mockSuccessApi = spyHelpers('successApi');

      const mockAddToast = jest
        .fn()
        .mockImplementation(() => ({ type: 'type' }));
      jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);

      await triggerAddClientConfiguration(requestActionData)(
        store.dispatch,
        store.getState,
        undefined
      );

      expect(action).toBeCalledWith({
        action: 'ADD',
        changedCategory: 'clientConfiguration',
        newValue: {
          cspa: '---',
          description: ''
        }
      });

      expect(mockSuccessApi).toBeCalledWith({
        nameAction: 'addCSPA',
        toastMsg: 'txt_save_client_config_message'
      });
    });

    it('rejected despite fulfilled response1', async () => {
      store = createStore(
        rootReducer,
        {
          clientConfiguration: {
            add: {
              isRetry: false
            }
          }
        },
        applyMiddleware(thunk)
      );
      jest.spyOn(clientConfigurationServices, 'add').mockResolvedValue({
        ...responseDefault,
        data: { errors: { message: 'error' } }
      });

      const mockAddClientConfigErrMessage = mockAction(
        'setAddClientConfigErrorMessage'
      );

      const mockAddToast = jest
        .fn()
        .mockImplementation(() => ({ type: 'type' }));
      jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);

      await triggerAddClientConfiguration(requestActionData)(
        store.dispatch,
        store.getState,
        undefined
      );

      expect(mockAddClientConfigErrMessage).toBeCalledWith({
        error: 'txt_client_configuration_already_exists',
        isRetry: false
      });
    });

    it('rejected on response 2', async () => {
      store = createStore(
        rootReducer,
        {
          clientConfiguration: {
            add: {
              isRetry: false
            }
          }
        },
        applyMiddleware(thunk)
      );

      spy = jest.spyOn(clientConfigurationServices, 'add').mockResolvedValue({
        ...responseDefault,
        data: { data: { message: 'success' } }
      });

      const mockAddToast = mockActionToast('addToast');

      mockAction('triggerSaveClientConfiguration', () => ({
        type: 'clientConfiguration/triggerSaveClientConfiguration/rejected',
        payload: undefined,
        meta: {
          arg: {},
          requestId: 'test request id',
          rejectedWithValue: true,
          requestStatus: 'rejected',
          aborted: true,
          condition: true
        },
        error: 'error'
      }));

      await triggerAddClientConfiguration(requestActionData)(
        store.dispatch,
        store.getState,
        undefined
      );

      expect(mockAddToast).toHaveBeenNthCalledWith(1, {
        message: 'txt_cspa_add_case_management_success',
        show: true,
        type: 'success'
      });

      expect(mockAddToast).toHaveBeenNthCalledWith(2, {
        message: 'txt_cspa_add_secure_case_management_failed',
        show: true,
        type: 'error'
      });
    });

    it('rejected on response 1', async () => {
      store = createStore(
        rootReducer,
        {
          clientConfiguration: {
            add: {
              isRetry: false
            }
          }
        },
        applyMiddleware(thunk)
      );
      jest.spyOn(clientConfigurationServices, 'add').mockRejectedValue({
        data: { error: { message: 'error' } },
        status: 400,
        statusText: '',
        headers: {},
        config: {}
      });

      const mockAddToast = jest
        .fn()
        .mockImplementation(() => ({ type: 'type' }));
      jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);

      await triggerAddClientConfiguration(requestActionData)(
        store.dispatch,
        store.getState,
        undefined
      );

      expect(mockAddToast).nthReturnedWith(1, { type: 'type' });
    });

    it('not do anything', async () => {
      store = createStore(
        rootReducer,
        {
          clientConfiguration: {
            add: {
              isRetry: false
            }
          }
        },
        applyMiddleware(thunk)
      );

      await triggerAddClientConfiguration(requestActionData)(
        jest.fn().mockResolvedValue({
          meta: {
            arg: {
              storeId: storeId
            },
            requestStatus: 'pending',
            requestId: 'mockRequestID'
          },
          type: 'clientConfiguration/triggerAddClientConfiguration/pending',
          payload: {
            data: {}
          }
        }),
        store.getState,
        {}
      );
    });
  });

  describe('should show error when duplicate record add client configuration ', () => {
    it('rejected code 409', async () => {
      const mockActionError = mockAction('setAddClientConfigErrorMessage');
      spy = jest.spyOn(clientConfigurationServices, 'add').mockResolvedValue({
        ...responseDefault,
        data: { data: { message: 'success' } }
      });

      await triggerAddClientConfiguration(requestActionData)(
        byPassFulfilled(triggerAddClientConfiguration.fulfilled.type, {
          data: { errors: '40123123' }
        }),
        store.getState,
        undefined
      );

      expect(mockActionError).toBeCalled();
    });
  });

  it('error', async () => {
    spy = jest.spyOn(clientConfigurationServices, 'add').mockRejectedValue({
      ...responseDefault,
      data: { data: { message: 'failed' } }
    });

    const response = await triggerAddClientConfiguration(requestActionData)(
      store.dispatch,
      store.getState,
      undefined
    );

    expect(response.type).toBe(triggerAddClientConfiguration.rejected.type);
  });
});
