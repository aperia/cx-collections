import { createAsyncThunk } from '@reduxjs/toolkit';
import { AxiosResponse } from 'axios';
import { clientConfigurationServices } from '../clientConfigurationServices';
import {
  CollectionsClientConfigGroup,
  ICollectionsClientConfigs,
  IGetCollectionsClientConfigArgs,
  IGetCollectionsClientConfigPayload,
  IGetCollectionsClientConfigServiceArgs
} from '../types';

export const getCollectionsClientConfig = createAsyncThunk<
  IGetCollectionsClientConfigPayload,
  IGetCollectionsClientConfigArgs,
  ThunkAPIConfig
>('clientConfiguration/getCollectionsClientConfig', async (args, thunkAPI) => {
  const state = thunkAPI.getState();
  try {
    const { agentId, clientId, systemId, principleId } =
      state.clientConfiguration.settings?.selectedConfig || {};
    const { privileges } = window.appConfig.commonConfig || {};
    const { storeId } = args;
    const {
      clientIdentifier,
      systemIdentifier,
      principalIdentifier,
      agentIdentifier
    } = (storeId && state.accountDetail[storeId]?.rawData) || {};

    const cspa = clientId
      ? `${clientId}-${systemId}-${principleId}-${agentId}`
      : `${clientIdentifier}-${systemIdentifier}-${principalIdentifier}-${agentIdentifier}`;

    let groupKey = CollectionsClientConfigGroup.CodeToText;

    if (
      args.component === 'component_authentication' ||
      args.component === 'ods-code-dictionary-admin-component' ||
      args.component === 'account-external-status-component'
    ) {
      groupKey = CollectionsClientConfigGroup.ClientConfig;
    }

    const requestBody: IGetCollectionsClientConfigServiceArgs = {
      common: {
        privileges
      },
      selectConfig: groupKey,
      selectCspa: cspa,
      selectComponent: args.component
    };

    const response: AxiosResponse<ICollectionsClientConfigs> =
      await clientConfigurationServices.getCollectionsClientConfig(requestBody);

    const group = response.data?.configs?.[groupKey];
    const getByCSPA = group?.cspas?.find(item => item.cspa === cspa);
    const getByComponent = getByCSPA?.components?.find(
      item => item.component === args.component
    );

    return getByComponent?.jsonValue || {};
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});
