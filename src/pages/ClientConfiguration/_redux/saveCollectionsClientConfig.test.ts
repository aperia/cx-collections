import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault
} from 'app/test-utils';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { clientConfigurationServices } from '../clientConfigurationServices';
import { CLIENT_CONFIG_STATUS_KEY } from '../constants';
import { saveCollectionsClientConfig } from './saveCollectionsClientConfig';

const mockApiErrorNotificationAction = mockActionCreator(
  apiErrorNotificationAction
);

describe('should test save collections client config', () => {
  let spy: jest.SpyInstance;
  let store: Store<RootState>;

  const cspa = '9999-1000-2000-3000';

  const mockResponseData = {
    configs: {
      clientConfig: {
        cspas: [
          {
            cspa,
            components: [
              {
                component: 'component_authentication',
                jsonValue: {
                  searchResult: false,
                  pushQueue: true,
                  dialerQueue: true
                }
              }
            ]
          }
        ]
      },
      codeToText: {
        cspas: [
          {
            cspa,
            components: [
              {
                component: 'component_pay_by_phone_config',
                jsonValue: {
                  maxDay: '100'
                }
              }
            ]
          }
        ]
      }
    }
  };

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  window.appConfig = {
    commonConfig: {
      user: 'user',
      app: 'app',
      org: 'org'
    } as CommonConfig
  } as AppConfiguration;

  test('should return empty data', async () => {
    const initialState: Partial<RootState> = {
      clientConfiguration: {
        settings: {
          selectedConfig: {
            id: '9999100020003000',
            agentId: '3000',
            clientId: '9999',
            systemId: '1000',
            principleId: '2000',
            additionalFields: [{ active: true, name: CLIENT_CONFIG_STATUS_KEY }]
          }
        }
      }
    };

    store = createStore(rootReducer, initialState);

    spy = jest
      .spyOn(clientConfigurationServices, 'saveCollectionsClientConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    const responseClientConfig = await saveCollectionsClientConfig({
      component: 'component_authentication',
      jsonValue: {}
    })(
      byPassFulfilled(saveCollectionsClientConfig.fulfilled.type),
      store.getState,
      {}
    );

    expect(responseClientConfig.type).toEqual(
      saveCollectionsClientConfig.rejected.type
    );
  });

  test('should return data', async () => {
    const mockClearApiErrorData =
      mockApiErrorNotificationAction('clearApiErrorData');

    const initialState: Partial<RootState> = {
      clientConfiguration: {
        settings: {
          selectedConfig: {
            id: '9999100020003000',
            agentId: '3000',
            clientId: '9999',
            systemId: '1000',
            principleId: '2000',
            additionalFields: [
              {
                name: 'isConfigActive',
                active: true
              }
            ]
          }
        }
      }
    };

    store = createStore(rootReducer, initialState);

    spy = jest
      .spyOn(clientConfigurationServices, 'saveCollectionsClientConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: mockResponseData
      });

    const responseClientConfig = await saveCollectionsClientConfig({
      component: 'component_authentication',
      jsonValue: {}
    })(
      byPassFulfilled(saveCollectionsClientConfig.fulfilled.type),
      store.getState,
      {}
    );

    const responseCodeToText = await saveCollectionsClientConfig({
      component: 'component_pay_by_phone_config',
      jsonValue: {},
      updateAction: 'delete'
    })(
      byPassFulfilled(saveCollectionsClientConfig.fulfilled.type),
      store.getState,
      {}
    );

    expect(responseClientConfig.type).toEqual(
      saveCollectionsClientConfig.fulfilled.type
    );
    expect(responseCodeToText.type).toEqual(
      saveCollectionsClientConfig.fulfilled.type
    );
    expect(mockClearApiErrorData).toBeCalled();
  });

  test('should be error', async () => {
    const mockUpdateApiError = mockApiErrorNotificationAction('updateApiError');

    const initialState: Partial<RootState> = {
      clientConfiguration: {
        settings: {}
      }
    };

    store = createStore(rootReducer, initialState);

    spy = jest
      .spyOn(clientConfigurationServices, 'saveCollectionsClientConfig')
      .mockRejectedValue({
        ...responseDefault
      });

    const responseClientConfig = await saveCollectionsClientConfig({
      component: 'component_authentication',
      jsonValue: {}
    })(
      byPassRejected(saveCollectionsClientConfig.rejected.type),
      store.getState,
      {}
    );

    const responseCodeToText = await saveCollectionsClientConfig({
      component: 'component_pay_by_phone_config',
      jsonValue: {},
      updateAction: 'delete'
    })(
      byPassRejected(saveCollectionsClientConfig.rejected.type),
      store.getState,
      {}
    );

    expect(responseClientConfig.type).toEqual(
      saveCollectionsClientConfig.rejected.type
    );
    expect(responseCodeToText.type).toEqual(
      saveCollectionsClientConfig.rejected.type
    );
    expect(mockUpdateApiError).toBeCalled();
  });
});
