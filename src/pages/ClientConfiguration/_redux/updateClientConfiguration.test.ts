import { clientConfigurationServices } from './../clientConfigurationServices';
import {
  byPassRejected,
  mockActionCreator,
  responseDefault,
  storeId
} from 'app/test-utils';
import {
  updateClientConfiguration,
  triggerUpdateClientConfiguration
} from './updateClientConfiguration';
import { applyMiddleware, createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';

import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import thunk from 'redux-thunk';
import { clientConfigurationActions } from './reducers';
import * as helpersMock from '../helpers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import * as saveChangedClientConfig from '../ChangeHistory/_redux/saveChangedClientConfig';

const requestActionData = {
  common: {
    agent: 'agent',
    clientNumber: 'clientNumber',
    system: 'system',
    prin: 'prin',
    app: 'app'
  },
  clientInfoId: 'clientInfoId',
  orgId: 'orgId',
  portfolioName: 'portfolioName',
  binSequences: [{}],
  additionalFields: []
};

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    user: 'user',
    org: 'org',
    app: 'app'
  }
};

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

const mockAction = mockActionCreator(clientConfigurationActions);
const mockActionToast = mockActionCreator(actionsToast);
const spyApiErrorNotificationAction = mockActionCreator(
  apiErrorNotificationAction
);

describe('ClientConfiguration > updateClientConfiguration', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {});
  });

  it('success', async () => {
    spy = jest.spyOn(clientConfigurationServices, 'update').mockResolvedValue({
      ...responseDefault,
      data: { data: { message: 'success' } }
    });

    const response = await updateClientConfiguration(requestActionData)(
      store.dispatch,
      store.getState,
      undefined
    );

    expect(response.payload).toEqual({
      ...responseDefault,
      data: { data: { message: 'success' } }
    });
  });

  it('error', async () => {
    spy = jest.spyOn(clientConfigurationServices, 'update').mockRejectedValue({
      ...responseDefault,
      data: { data: { message: 'failed' } }
    });

    const response = await updateClientConfiguration(requestActionData)(
      store.dispatch,
      store.getState,
      undefined
    );

    expect(response.type).toBe(updateClientConfiguration.rejected.type);
  });
});

describe('ClientConfiguration > triggerUpdateClientConfiguration', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {});
  });

  describe('success', () => {
    it('fulfilled', async () => {
      spy = jest
        .spyOn(clientConfigurationServices, 'update')
        .mockResolvedValue({
          ...responseDefault,
          data: { data: { message: 'success' } }
        });

      const mockAddToast = jest
        .fn()
        .mockImplementation(() => ({ type: 'type' }));
      jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);

      await triggerUpdateClientConfiguration(requestActionData as any)(
        jest.fn().mockResolvedValue({
          meta: {
            arg: {
              storeId: storeId
            },
            requestStatus: 'fulfilled',
            requestId: 'mockRequestID'
          },
          type: 'clientConfiguration/triggerUpdateClientConfiguration/fulfilled',
          payload: null
        }),
        store.getState,
        {}
      );
    });

    it('fulfilled > isRetry', async () => {
      store = createStore(
        rootReducer,
        {
          clientConfiguration: {
            update: {
              isRetry: true
            }
          }
        },
        applyMiddleware(thunk)
      );
      spy = jest
        .spyOn(clientConfigurationServices, 'update')
        .mockResolvedValue({
          ...responseDefault,
          data: { data: { message: 'success' } }
        });

      const mockEditClientConfigErrorMessage = mockAction(
        'setEditClientConfigErrorMessage'
      );
      const spyHelpers = mockActionCreator(helpersMock);
      const mockSuccessApi = spyHelpers('successApi');
      const mockSaveChangedClientConfig = mockActionCreator(
        saveChangedClientConfig
      )('saveChangedClientConfig');

      mockAction('triggerSaveClientConfiguration', () => ({
        type: 'clientConfiguration/triggerSaveClientConfiguration/fulfilled',
        meta: {
          arg: {},
          requestId: 'test request id',
          requestStatus: 'fulfilled'
        }
      }));

      await triggerUpdateClientConfiguration(requestActionData as any)(
        store.dispatch,
        store.getState,
        undefined
      );

      expect(mockEditClientConfigErrorMessage).toBeCalledWith({
        error: '',
        isRetry: false
      });

      expect(mockSaveChangedClientConfig).toBeCalled();

      expect(mockSuccessApi).toBeCalledWith({
        nameAction: 'editCSPA',
        toastMsg: 'txt_cspa_edit_secure_case_management_success'
      });
    });

    it('rejected > isRetry', async () => {
      store = createStore(rootReducer, {
        clientConfiguration: { update: { isRetry: true } }
      });
      spy = jest
        .spyOn(clientConfigurationServices, 'update')
        .mockRejectedValue({
          ...responseDefault,
          data: { data: { message: 'success' } }
        });

      const mockAddToast = jest
        .fn()
        .mockImplementation(() => ({ type: 'type' }));
      jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);
      const spyHelpers = mockActionCreator(helpersMock);
      const mockRejectApi = spyHelpers('rejectApi');
      const mockUpdateThunkApiError = spyApiErrorNotificationAction(
        'updateThunkApiError'
      );

      await triggerUpdateClientConfiguration(requestActionData as any)(
        byPassRejected(triggerUpdateClientConfiguration.rejected.type),
        store.getState,
        undefined
      );

      expect(mockUpdateThunkApiError).toBeCalled();

      expect(mockRejectApi).toBeCalledWith({
        errorMsgInline: 'txt_cspa_inline_failed_secure_case_management',
        nameAction: 'editCSPA',
        toastMsg: 'txt_cspa_edit_secure_case_management_failed'
      });
    });

    it('resolved on response 2', async () => {
      store = createStore(
        rootReducer,
        {
          clientConfiguration: {
            add: {
              isRetry: false
            }
          }
        },
        applyMiddleware(thunk)
      );

      spy = jest
        .spyOn(clientConfigurationServices, 'update')
        .mockResolvedValue({
          ...responseDefault,
          data: { data: { message: 'success' } }
        });

      mockAction('triggerSaveClientConfiguration', () => ({
        type: 'clientConfiguration/triggerSaveClientConfiguration/fulfilled',
        meta: {
          arg: {},
          requestId: 'test request id',
          requestStatus: 'fulfilled'
        }
      }));

      const spyHelpers = mockActionCreator(helpersMock);
      const mockSuccessApi = spyHelpers('successApi');

      const mockAddToast = jest
        .fn()
        .mockImplementation(() => ({ type: 'type' }));
      jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);

      await triggerUpdateClientConfiguration(requestActionData as any)(
        store.dispatch,
        store.getState,
        undefined
      );

      expect(mockSuccessApi).toBeCalledWith({
        nameAction: 'editCSPA',
        toastMsg: 'txt_edit_client_config_message'
      });
    });

    it('rejected on response 2', async () => {
      store = createStore(
        rootReducer,
        {
          clientConfiguration: {
            add: {
              isRetry: false
            }
          }
        },
        applyMiddleware(thunk)
      );

      spy = jest
        .spyOn(clientConfigurationServices, 'update')
        .mockResolvedValue({
          ...responseDefault,
          data: { data: { message: 'success' } }
        });

      const mockAddToast = mockActionToast('addToast');
      const spyHelpers = mockActionCreator(helpersMock);
      const mockRejectApi = spyHelpers('rejectApi');
      const mockUpdateThunkApiError = spyApiErrorNotificationAction(
        'updateThunkApiError'
      );

      mockAction('triggerSaveClientConfiguration', () => ({
        type: 'clientConfiguration/triggerSaveClientConfiguration/rejected',
        payload: undefined,
        meta: {
          arg: {},
          requestId: 'test request id',
          rejectedWithValue: true,
          requestStatus: 'rejected',
          aborted: true,
          condition: true
        },
        error: 'error'
      }));

      await triggerUpdateClientConfiguration(requestActionData as any)(
        store.dispatch,
        store.getState,
        undefined
      );

      expect(mockAddToast).toBeCalledWith({
        message: 'txt_cspa_edit_case_management_success',
        show: true,
        type: 'success'
      });

      expect(mockUpdateThunkApiError).toBeCalled();

      expect(mockRejectApi).toBeCalledWith({
        errorMsgInline: 'txt_cspa_inline_failed_secure_case_management',
        nameAction: 'editCSPA',
        toastMsg: 'txt_cspa_edit_secure_case_management_failed'
      });
    });

    it('not do anything', async () => {
      store = createStore(
        rootReducer,
        {
          clientConfiguration: {
            add: {
              isRetry: false
            }
          }
        },
        applyMiddleware(thunk)
      );

      await triggerUpdateClientConfiguration(requestActionData as any)(
        jest.fn().mockResolvedValue({
          meta: {
            arg: {
              storeId: storeId
            },
            requestStatus: 'pending',
            requestId: 'mockRequestID'
          },
          type: 'clientConfiguration/triggerUpdateClientConfiguration/pending',
          payload: {
            data: {}
          }
        }),
        store.getState,
        {}
      );
    });

    it('rejected code 409', async () => {
      spy = jest
        .spyOn(clientConfigurationServices, 'update')
        .mockResolvedValue({
          ...responseDefault,
          data: { data: { message: 'success' } }
        });

      const mockAddToast = jest
        .fn()
        .mockImplementation(() => ({ type: 'type' }));
      jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);

      await triggerUpdateClientConfiguration(requestActionData as any)(
        jest.fn().mockResolvedValue({
          meta: {
            arg: {
              storeId: storeId
            },
            requestStatus: 'rejected',
            requestId: 'mockRequestID'
          },
          type: 'clientConfiguration/triggerUpdateClientConfiguration/fulfilled',
          payload: {
            response: {
              data: {
                responseCode: 409
              }
            }
          }
        }),
        store.getState,
        undefined
      );

      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'error',
        message: 'txt_cspa_failed_to_edit_case_and_secure_management'
      });
    });
  });

  it('error', async () => {
    spy = jest.spyOn(clientConfigurationServices, 'update').mockRejectedValue({
      ...responseDefault,
      data: { data: { message: 'failed' } }
    });

    const response = await triggerUpdateClientConfiguration(
      requestActionData as any
    )(store.dispatch, store.getState, undefined);

    expect(response.type).toBe(triggerUpdateClientConfiguration.rejected.type);
  });
});
