import { clientConfigurationServices } from './../clientConfigurationServices';
import { byPassFulfilled, responseDefault } from 'app/test-utils';
import { getClientInfoData } from './getClientInfoById';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { GetClientInfoArgs } from '../types';

const requestActionData: GetClientInfoArgs = {
  dataType: {
    emailTemplates: {},
    clientInfoLetterTemplate: [],
    actionCodes: [],
    adjustments: {
      adjustmentCodes: [],
      adjustmentGroups: []
    },
    debtMgmtCompanyInfo: [],
    clusterQueueMappings: [],
    promiseConfigList: [],
    rulesetsConfig: [],
    rulesets: []
  },
  fieldsToQuery: []
};

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    org: 'org',
    app: 'app'
  }
};

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('ClientConfiguration > getClientInfoData', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {});
  });
  it('success', async () => {
    spy = jest
      .spyOn(clientConfigurationServices, 'getClientInfoById')
      .mockResolvedValue({
        ...responseDefault,
        data: { message: 'success', responseStatus: 'success' }
      });

    const response = await getClientInfoData(requestActionData)(
      byPassFulfilled(getClientInfoData.fulfilled.type),
      store.getState,
      undefined
    );
    const responseFieldsToQueryObject = await getClientInfoData({
      ...requestActionData,
      fieldsToQuery: 'emailTemplates'
    })(
      byPassFulfilled(getClientInfoData.fulfilled.type),
      store.getState,
      undefined
    );
    const responseDataType = await getClientInfoData({
      ...requestActionData,
      dataType: 'actionCodes',
      fieldsToQuery: undefined
    })(
      byPassFulfilled(getClientInfoData.fulfilled.type),
      store.getState,
      undefined
    );

    expect(response.type).toEqual(getClientInfoData.fulfilled.type);
    expect(responseFieldsToQueryObject.type).toEqual(
      getClientInfoData.fulfilled.type
    );
    expect(responseDataType.type).toEqual(getClientInfoData.fulfilled.type);
  });

  it('error', async () => {
    spy = jest
      .spyOn(clientConfigurationServices, 'getClientInfoById')
      .mockResolvedValue({
        ...responseDefault,
        data: { message: 'error', responseStatus: 'error' }
      });

    const response = await getClientInfoData(requestActionData)(
      byPassFulfilled(getClientInfoData.rejected.type),
      store.getState,
      undefined
    );
    const responseDelete = await getClientInfoData({
      ...requestActionData,
      updateAction: 'delete'
    })(
      byPassFulfilled(getClientInfoData.rejected.type),
      store.getState,
      undefined
    );

    expect(response.type).toEqual(getClientInfoData.rejected.type);
    expect(responseDelete.type).toEqual(getClientInfoData.rejected.type);
  });
});
