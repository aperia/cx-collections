import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';
import { clientConfigurationServices } from '../clientConfigurationServices';
import { mappingDataFromObj } from 'app/helpers';

import {
  ClientConfigurationState,
  ConfigData,
  GetClientConfigListPayload,
  GetClientConfigurationRequest
} from '../types';
import { AxiosResponse } from 'axios';
import { buildListDefaultSort } from 'pages/ConfigurationEntitlement/helpers';

export const getAllClientConfigListApi = createAsyncThunk<
  AxiosResponse,
  undefined,
  ThunkAPIConfig
>(
  'clientConfiguration/getAllClientConfigListApi',
  async (_, { rejectWithValue, getState }) => {
    const { accessConfig } = getState();
    const { user, app, org } = window.appConfig.commonConfig;
    const [clientNumber = '', system = '$ALL', prin = '$ALL', agent = '$ALL'] =
      accessConfig?.cspaList?.[0]?.cspa?.split('-') || [];
    const requestBody: GetClientConfigurationRequest = {
      common: {
        clientName: 'firstdata',
        clientNumber,
        agent,
        system,
        prin,
        user,
        app
      },
      orgId: org,
      fetchSize: '500'
    };

    try {
      return await clientConfigurationServices.clientInfoGetList(requestBody);
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const getAllClientConfigList = createAsyncThunk<
  GetClientConfigListPayload,
  undefined,
  ThunkAPIConfig
>(
  'clientConfiguration/getAllClientConfigList',
  async (_, { dispatch, rejectWithValue, getState }) => {
    try {
      const response = await dispatch(getAllClientConfigListApi());

      let payload: { data?: ConfigData[] } = {};

      if (isFulfilled(response)) {
        const { mapping } = getState();
        if (response?.payload.data?.clientInfoList) {
          const mappingData = mapping?.data?.clientConfig || {};

          const data = response?.payload.data?.clientInfoList.map(
            (item: MagicKeyValue) => {
              return {
                ...mappingDataFromObj(item, mappingData)
              };
            }
          );

          payload = {
            data: buildListDefaultSort(data)
          };
        }
        return payload;
      }
      throw response;
    } catch (error) {
      return rejectWithValue({ response: error });
    }
  }
);

export const getAllClientConfigListBuilder = (
  builder: ActionReducerMapBuilder<ClientConfigurationState>
) => {
  builder.addCase(getAllClientConfigList.pending, (draftState, action) => {
    draftState.list = {
      ...draftState?.list,
      loading: true,
      error: false
    };
  });
  builder.addCase(getAllClientConfigList.fulfilled, (draftState, action) => {
    const payload = action.payload;

    draftState.list = {
      ...draftState?.list,
      loading: false,
      error: false,
      data: payload?.data
    };
  });
  builder.addCase(getAllClientConfigList.rejected, (draftState, action) => {
    draftState.list = {
      ...draftState?.list,
      loading: false,
      error: true,
      data: []
    };
  });
};
