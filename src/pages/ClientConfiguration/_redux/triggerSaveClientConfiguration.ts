import { clientConfigurationServices } from './../clientConfigurationServices';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

import {
  ClientConfigurationState,
  IGetCollectionsClientConfigServiceArgs
} from '../types';
import { AxiosResponse } from 'axios';

export const triggerSaveClientConfiguration = createAsyncThunk<
  AxiosResponse,
  IGetCollectionsClientConfigServiceArgs,
  ThunkAPIConfig
>(
  'clientConfiguration/triggerSaveClientConfiguration',
  async (args, { rejectWithValue }) => {
    try {
      return await clientConfigurationServices.saveCollectionsClientConfig(
        args
      );
    } catch (error) {
      return rejectWithValue({ response: error });
    }
  }
);

export const triggerSaveClientConfigurationBuilder = (
  builder: ActionReducerMapBuilder<ClientConfigurationState>
) => {
  builder
    .addCase(triggerSaveClientConfiguration.pending, (draftState, action) => {
      draftState.loadingAddModal = true;
      draftState.loadingEditModal = true;
    })
    .addCase(triggerSaveClientConfiguration.fulfilled, (draftState, action) => {
      draftState.loadingAddModal = false;
      draftState.loadingEditModal = false;
    })
    .addCase(triggerSaveClientConfiguration.rejected, (draftState, action) => {
      draftState.loadingAddModal = false;
      draftState.loadingEditModal = false;
    });
};
