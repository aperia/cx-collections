import { NAME_ACTION } from './../constants';
import { I18N_CLIENT_CONFIG } from 'app/constants/i18n';
import { clientConfigurationServices } from './../clientConfigurationServices';
import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { clientConfigurationActions } from './reducers';
import {
  ClientConfigurationState,
  TriggerSaveClientConfigRequest,
  UpdateClientConfigurationArgs,
  UpdateClientConfigurationRequest
} from '../types';
import { AxiosResponse } from 'axios';
import { convertClientInfoId } from '../helpers';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

import { rejectApi, successApi } from 'pages/ClientConfiguration/helpers';
import { saveChangedClientConfig } from '../ChangeHistory/_redux/saveChangedClientConfig';
import { batch } from 'react-redux';

export const updateClientConfiguration = createAsyncThunk<
  AxiosResponse,
  UpdateClientConfigurationRequest,
  ThunkAPIConfig
>('clientConfiguration/updateClientConfiguration', async (args, thunkAPI) => {
  try {
    return await clientConfigurationServices.update(args);
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerUpdateClientConfiguration = createAsyncThunk<
  void,
  UpdateClientConfigurationArgs,
  ThunkAPIConfig
>(
  'clientConfiguration/triggerUpdateClientConfiguration',
  async (args, thunkAPI) => {
    const { dispatch, getState } = thunkAPI;
    const {
      description,
      id,
      clientId,
      systemId,
      principleId,
      agentId,
      defaultCspa
    } = args;
    const { user, app, org, privileges } = window?.appConfig?.commonConfig;

    const cspa = convertClientInfoId(clientId, systemId, principleId, agentId);

    const { accessConfig, clientConfiguration } = getState();
    const isRetry = clientConfiguration.update?.isRetry;
    const oldDescription =
      clientConfiguration.settings?.selectedConfig?.description;
    const [clientNumber = '', system = '$ALL', prin = '$ALL', agent = '$ALL'] =
      accessConfig?.cspaList?.[0]?.cspa?.split('-') || [];

    const requestBody1: UpdateClientConfigurationRequest = {
      common: {
        agent,
        clientNumber,
        system,
        prin,
        user,
        app
      },
      clientInfoId: id,
      portfolioName: description
    };

    const requestBody2: TriggerSaveClientConfigRequest = {
      common: {
        user,
        app,
        org,
        privileges
      },
      defaultCspa,
      callerAccountEntitlements: {
        cspas: [
          {
            cspa,
            description,
            active: true
          }
        ]
      },
      clientConfig: {
        cspas: [
          {
            cspa,
            description,
            active: true
          }
        ]
      },
      codeToText: {
        cspas: [
          {
            cspa,
            description,
            active: true
          }
        ]
      }
    };

    if (isRetry) {
      const retryResponse = await dispatch(
        clientConfigurationActions.triggerSaveClientConfiguration(requestBody2)
      );
      if (isFulfilled(retryResponse)) {
        dispatch(
          clientConfigurationActions.setEditClientConfigErrorMessage({
            error: '',
            isRetry: false
          })
        );
        dispatch(
          successApi({
            toastMsg: 'txt_cspa_edit_secure_case_management_success',
            nameAction: NAME_ACTION.editCspa
          })
        );
        dispatch(
          saveChangedClientConfig({
            action: 'UPDATE',
            changedCategory: 'clientConfiguration',
            oldValue: {
              description: oldDescription
            },
            newValue: {
              description
            }
          })
        );
      }
      if (isRejected(retryResponse)) {
        dispatch(
          apiErrorNotificationAction.updateThunkApiError({
            storeId: API_ERROR_STORE_ID.ENTITLEMENT,
            forSection: 'inModalBody',
            rejectData: {
              ...retryResponse,
              payload: retryResponse.payload?.response
            }
          })
        );
        dispatch(
          rejectApi({
            errorMsgInline: 'txt_cspa_inline_failed_secure_case_management',
            toastMsg: 'txt_cspa_edit_secure_case_management_failed',
            nameAction: NAME_ACTION.editCspa
          })
        );
      }
      return;
    }

    const response1 = await dispatch(updateClientConfiguration(requestBody1));

    if (isRejected(response1)) {
      dispatch(
        apiErrorNotificationAction.updateThunkApiError({
          storeId: API_ERROR_STORE_ID.ENTITLEMENT,
          forSection: 'inModalBody',
          rejectData: {
            ...response1,
            payload: response1.payload?.response
          }
        })
      );
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: 'txt_cspa_failed_to_edit_case_and_secure_management'
        })
      );
      return;
    }

    if (isFulfilled(response1)) {
      const response2 = await dispatch(
        clientConfigurationActions.triggerSaveClientConfiguration(requestBody2)
      );

      if (isFulfilled(response2)) {
        batch(() => {
          dispatch(
            successApi({
              toastMsg: I18N_CLIENT_CONFIG.EDIT_CLIENT_CONFIG_MESSAGE,
              nameAction: NAME_ACTION.editCspa
            })
          );
          dispatch(
            saveChangedClientConfig({
              action: 'UPDATE',
              changedCategory: 'clientConfiguration',
              oldValue: {
                description: oldDescription
              },
              newValue: {
                description
              }
            })
          );
        });
      }

      if (isRejected(response2)) {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: 'txt_cspa_edit_case_management_success'
          })
        );

        dispatch(
          apiErrorNotificationAction.updateThunkApiError({
            storeId: API_ERROR_STORE_ID.ENTITLEMENT,
            forSection: 'inModalBody',
            rejectData: {
              ...response2,
              payload: response2.payload?.response
            }
          })
        );

        dispatch(
          rejectApi({
            errorMsgInline: 'txt_cspa_inline_failed_secure_case_management',
            toastMsg: 'txt_cspa_edit_secure_case_management_failed',
            nameAction: NAME_ACTION.editCspa
          })
        );
      }
    }
  }
);

export const updateClientConfigurationBuilder = (
  builder: ActionReducerMapBuilder<ClientConfigurationState>
) => {
  builder
    .addCase(updateClientConfiguration.pending, (draftState, action) => {
      draftState.loadingEditModal = true;
    })
    .addCase(updateClientConfiguration.fulfilled, (draftState, action) => {
      draftState.loadingEditModal = false;
    })
    .addCase(updateClientConfiguration.rejected, (draftState, action) => {
      draftState.loadingEditModal = false;
    });
};
