import { selectorWrapper } from 'app/test-utils';
import {
  selectAddErrorMessage,
  selectClientConfigList,
  selectConfig,
  selectIsNoRecords,
  selectIsNoResultsFound,
  selectListError,
  selectLoadingAdd,
  selectLoadingClientConfigList,
  selectOpenStatusModal,
  selectLoadingEdit,
  selectOpenAddModal,
  selectLoadingStatus,
  selectOpenEditModal,
  selectOpenSettings,
  selectSearchValue,
  selectSections,
  selectSortBy,
  selectTextSearchValue,
  selectToggleSettings,
  selectUpdateErrorMessage,
  selectLoadingSaveConfig,
  selectDefaultSettingsClientConfig,
  selectOriginData,
  takeWarningMsg,
  selectAddRetry,
  selectUpdateRetry,
  selectLastHistory
} from './selectors';

describe('Test Client Configuration Selectors', () => {
  const store: Partial<RootState> = {
    clientConfiguration: {
      openStatusModal: true,
      loadingStatusModal: true,
      openAddModal: true,
      openEditModal: true,
      openDeleteModal: true,
      loadingAddModal: true,
      loadingEditModal: true,
      loadingDeleteModal: true,
      loadingSaveClientConfig: true,
      warningMessage: 'warning',
      add: {
        errorMessage: 'Test',
        isRetry: true
      },
      update: {
        errorMessage: 'Test',
        isRetry: true
      },
      list: {
        loading: true,
        error: true,
        data: [
          { id: '2', clientId: '2' },
          { id: '1', clientId: '1' },
          {
            id: '3',
            clientId: '3',
            additionalFields: [
              {
                name: 'isDefaultClientInfoKey',
                value: 'true'
              }
            ]
          }
        ],
        sortBy: {
          id: 'test',
          order: 'asc'
        },
        searchValue: '1',
        textSearchValue: 'textSearchValue'
      },
      settings: {
        toggle: true,
        isOpen: true,
        selectedConfig: {
          test: true,
          additionalFields: [
            {
              name: 'isConfigActive',
              active: true
            }
          ]
        },
        sections: [{ value: 'value', description: 'description' }]
      },
      lastHistory: {
        date: '01/01/2021',
        time: '9:00',
        changedBy: 'test',
        cspa: '',
        action: '',
        changedCategory: '',
        changedSection: '',
        changedItem: '',
        before: '',
        after: ''
      }
    }
  };

  it('selectToggleSettings', () => {
    const { data, emptyData } = selectorWrapper(store)(selectToggleSettings);

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectOpenSettings', () => {
    const { data, emptyData } = selectorWrapper(store)(selectOpenSettings);

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectConfig', () => {
    const { data, emptyData } = selectorWrapper(store)(selectConfig);

    expect(data).toEqual({
      additionalFields: [
        {
          name: 'isConfigActive',
          active: true
        }
      ],
      status: true,
      test: true
    });
    expect(emptyData.status).toEqual(undefined);
  });

  it('selectSections', () => {
    const { data, emptyData } = selectorWrapper(store)(selectSections);

    expect(data).toEqual([{ value: 'value', description: 'description' }]);
    expect(emptyData).toEqual([]);
  });

  it('selectLoadingClientConfigList', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectLoadingClientConfigList
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectClientConfigList', () => {
    const { data, emptyData } = selectorWrapper(store)(selectClientConfigList);

    expect(data).toEqual([{ id: '1', clientId: '1' }]);
    expect(emptyData).toEqual([]);
  });

  it('selectSortBy', () => {
    const { data, emptyData } = selectorWrapper(store)(selectSortBy);

    expect(data).toEqual({
      id: 'test',
      order: 'asc'
    });
    expect(emptyData).toEqual({ id: 'status', order: undefined });
  });

  it('selectListError', () => {
    const { data, emptyData } = selectorWrapper(store)(selectListError);

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectOpenAddModal', () => {
    const { data, emptyData } = selectorWrapper(store)(selectOpenAddModal);

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectOpenEditModal', () => {
    const { data, emptyData } = selectorWrapper(store)(selectOpenEditModal);

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectLoadingStatus ', () => {
    const { data, emptyData } = selectorWrapper(store)(selectLoadingStatus);

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectLoadingAdd', () => {
    const { data, emptyData } = selectorWrapper(store)(selectLoadingAdd);

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectLoadingEdit', () => {
    const { data, emptyData } = selectorWrapper(store)(selectLoadingEdit);

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectOpenStatusModal ', () => {
    const { data, emptyData } = selectorWrapper(store)(selectOpenStatusModal);

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectAddErrorMessage', () => {
    const { data, emptyData } = selectorWrapper(store)(selectAddErrorMessage);

    expect(data).toEqual('Test');
    expect(emptyData).toEqual('');
  });

  it('selectUpdateErrorMessage', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectUpdateErrorMessage
    );

    expect(data).toEqual('Test');
    expect(emptyData).toEqual('');
  });

  it('selectSearchValue', () => {
    const { data, emptyData } = selectorWrapper(store)(selectSearchValue);

    expect(data).toEqual('1');
    expect(emptyData).toEqual('');
  });

  it('selectTextSearchValue', () => {
    const { data, emptyData } = selectorWrapper(store)(selectTextSearchValue);

    expect(data).toEqual('textSearchValue');
    expect(emptyData).toEqual('');
  });

  it('selectIsNoResultsFound', () => {
    const { data, emptyData } = selectorWrapper(store)(selectIsNoResultsFound);

    expect(data).toEqual(false);
    expect(emptyData).toEqual(false);
  });

  it('selectIsNoRecords', () => {
    const { data, emptyData } = selectorWrapper(store)(selectIsNoRecords);

    expect(data).toEqual(false);
    expect(emptyData).toEqual(true);
  });

  it('takeWarningMsg', () => {
    const { data, emptyData } = selectorWrapper(store)(takeWarningMsg);

    expect(data).toEqual('warning');
    expect(emptyData).toEqual('');
  });

  it('selectAddRetry', () => {
    const { data, emptyData } = selectorWrapper(store)(selectAddRetry);

    expect(data).toBeTruthy();
    expect(emptyData).toBeFalsy();
  });

  it('selectUpdateRetry ', () => {
    const { data, emptyData } = selectorWrapper(store)(selectUpdateRetry);

    expect(data).toBeTruthy();
    expect(emptyData).toBeFalsy();
  });

  it('selectLoadingSaveConfig', () => {
    const { data, emptyData } = selectorWrapper(store)(selectLoadingSaveConfig);

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectDefaultSettingsClientConfig', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectDefaultSettingsClientConfig
    );

    expect(data).toEqual({
      additionalFields: [{ name: 'isDefaultClientInfoKey', value: 'true' }],
      clientId: '3',
      id: '3'
    });
    expect(emptyData).toEqual(undefined);
  });

  it('selectOriginData', () => {
    const { data, emptyData } = selectorWrapper(store)(selectOriginData);

    expect(data).toEqual([
      { clientId: '2', id: '2' },
      { clientId: '1', id: '1' },
      {
        additionalFields: [{ name: 'isDefaultClientInfoKey', value: 'true' }],
        clientId: '3',
        id: '3'
      }
    ]);
    expect(emptyData).toEqual([]);
  });

  it('selectLastHistory', () => {
    const { data, emptyData } = selectorWrapper(store)(selectLastHistory);

    expect(data).toEqual({
      action: '',
      after: '',
      before: '',
      changedBy: 'test',
      changedCategory: '',
      changedItem: '',
      changedSection: '',
      cspa: '',
      date: '01/01/2021',
      time: '9:00'
    });
    expect(emptyData).toBeUndefined();
  });
});
