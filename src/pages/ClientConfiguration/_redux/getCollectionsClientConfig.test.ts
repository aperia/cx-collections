import {
  byPassFulfilled,
  byPassRejected,
  responseDefault,
  storeId
} from 'app/test-utils';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { clientConfigurationServices } from '../clientConfigurationServices';
import { getCollectionsClientConfig } from './getCollectionsClientConfig';

describe('should test get collections client config', () => {
  let spy: jest.SpyInstance;
  let store: Store<RootState>;

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  window.appConfig = {
    commonConfig: {
      user: 'user',
      app: 'app',
      org: 'org'
    } as CommonConfig
  } as AppConfiguration;

  const cspa = '9999-1000-2000-3000';

  const mockResponseData = {
    configs: {
      clientConfig: {
        cspas: [
          {
            cspa,
            components: [
              {
                component: 'component_authentication',
                jsonValue: {
                  searchResult: false,
                  pushQueue: true,
                  dialerQueue: true
                }
              }
            ]
          }
        ]
      },
      codeToText: {
        cspas: [
          {
            cspa,
            components: [
              {
                component: 'component_pay_by_phone_config',
                jsonValue: {
                  maxDay: '100'
                }
              }
            ]
          }
        ]
      }
    }
  };

  it('should return data in client configuration', async () => {
    const initialState: Partial<RootState> = {
      clientConfiguration: {
        settings: {
          selectedConfig: {
            id: '9999100020003000',
            agentId: '3000',
            clientId: '9999',
            systemId: '1000',
            principleId: '2000'
          }
        }
      }
    };

    store = createStore(rootReducer, initialState);

    spy = jest
      .spyOn(clientConfigurationServices, 'getCollectionsClientConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: mockResponseData
      });

    const responseClientConfig = await getCollectionsClientConfig({
      component: 'component_authentication'
    })(
      byPassFulfilled(getCollectionsClientConfig.fulfilled.type),
      store.getState,
      {}
    );

    const responseCodeToText = await getCollectionsClientConfig({
      component: 'component_pay_by_phone_config'
    })(
      byPassFulfilled(getCollectionsClientConfig.fulfilled.type),
      store.getState,
      {}
    );

    expect(responseClientConfig.type).toEqual(
      getCollectionsClientConfig.fulfilled.type
    );
    expect(responseCodeToText.type).toEqual(
      getCollectionsClientConfig.fulfilled.type
    );
  });

  it('should return data in account detail', async () => {
    const initialState: Partial<RootState> = {
      accountDetail: {
        [storeId]: {
          rawData: {
            clientIdentifier: '9999',
            systemIdentifier: '1000',
            principalIdentifier: '2000',
            agentIdentifier: '3000'
          }
        }
      }
    };

    store = createStore(rootReducer, initialState);

    spy = jest
      .spyOn(clientConfigurationServices, 'getCollectionsClientConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: mockResponseData
      });

    const responseClientConfig = await getCollectionsClientConfig({
      component: 'component_authentication',
      storeId
    })(
      byPassFulfilled(getCollectionsClientConfig.fulfilled.type),
      store.getState,
      {}
    );

    const responseCodeToText = await getCollectionsClientConfig({
      component: 'component_pay_by_phone_config'
    })(
      byPassFulfilled(getCollectionsClientConfig.fulfilled.type),
      store.getState,
      {}
    );

    expect(responseClientConfig.type).toEqual(
      getCollectionsClientConfig.fulfilled.type
    );
    expect(responseCodeToText.type).toEqual(
      getCollectionsClientConfig.fulfilled.type
    );
  });

  it('should be error', async () => {
    window.appConfig = {
      commonConfig: undefined as CommonConfig
    };
    
    const initialState: Partial<RootState> = {
      clientConfiguration: {
        settings: {}
      }
    };

    store = createStore(rootReducer, initialState);

    spy = jest
      .spyOn(clientConfigurationServices, 'getCollectionsClientConfig')
      .mockRejectedValue({
        ...responseDefault
      });

    const responseClientConfig = await getCollectionsClientConfig({
      component: 'component_authentication'
    })(
      byPassRejected(getCollectionsClientConfig.rejected.type),
      store.getState,
      {}
    );

    const responseCodeToText = await getCollectionsClientConfig({
      component: 'component_pay_by_phone_config'
    })(
      byPassRejected(getCollectionsClientConfig.rejected.type),
      store.getState,
      {}
    );

    expect(responseClientConfig.type).toEqual(
      getCollectionsClientConfig.rejected.type
    );
    expect(responseCodeToText.type).toEqual(
      getCollectionsClientConfig.rejected.type
    );
  });
});
