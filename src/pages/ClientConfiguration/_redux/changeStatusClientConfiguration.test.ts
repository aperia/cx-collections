import { clientConfigurationServices } from '../clientConfigurationServices';
import {
  responseDefault,
  byPassFulfilled,
  byPassRejected
} from 'app/test-utils';
import {
  changeStatusClientConfiguration,
  triggerChangeStatusClientConfiguration
} from './changeStatusClientConfiguration';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';

import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

const translateFn = (text: string) => text;

const requestActionData = {
  id: '1',
  clientId: '',
  systemId: '',
  principleId: '',
  agentId: '',
  description: '',
  additionalFields: [
    {
      name: 'mock name',
      active: false,
      value: 'aaa',
      description: 'aaa'
    },
    {
      name: 'isConfigActive',
      active: true,
      value: 'aaa',
      description: 'aaa'
    }
  ],
  clientInfoId: '',
  binSequences: [],
  translateFn
};

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

let store: Store<RootState>;

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    user: 'user',
    org: 'org',
    app: 'app'
  }
};

describe('ClientConfiguration > changeStatusClientConfiguration', () => {
  beforeEach(() => {
    store = createStore(rootReducer, {});
  });

  it('success', async () => {
    spy = jest
      .spyOn(clientConfigurationServices, 'changeStatusClientConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: { data: { message: 'success' } }
      });

    const response = await changeStatusClientConfiguration(requestActionData)(
      store.dispatch,
      store.getState,
      undefined
    );

    expect(response.payload).toEqual({
      ...responseDefault,
      data: { data: { message: 'success' } }
    });
  });

  it('error', async () => {
    spy = jest
      .spyOn(clientConfigurationServices, 'changeStatusClientConfig')
      .mockRejectedValue({
        ...responseDefault,
        data: { data: { message: 'failed' } }
      });

    const response = await changeStatusClientConfiguration(requestActionData)(
      store.dispatch,
      store.getState,
      undefined
    );

    expect(response.type).toBe(changeStatusClientConfiguration.rejected.type);
  });
});

describe('ClientConfiguration > triggerChangeStatusClientConfiguration', () => {
  beforeEach(() => {
    store = createStore(rootReducer, {});
  });

  describe('success', () => {
    it('fulfilled', async () => {
      const mockAddToast = jest
        .fn()
        .mockImplementation(() => ({ type: 'type' }));
      jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);

      await triggerChangeStatusClientConfiguration(requestActionData)(
        byPassFulfilled(
          'clientConfiguration/changeStatusClientConfiguration/fulfilled',
          {
            data: {
              messages: [{ status: 'success' }]
            }
          }
        ),
        store.getState,
        {}
      );

      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: 'txt_client_configuration_change_status',
        msgVariables: {
          status: 'txt_deactivated'
        }
      });
    });

    it('rejected', async () => {
      spy = jest
        .spyOn(clientConfigurationServices, 'changeStatusClientConfig')
        .mockResolvedValue({
          ...responseDefault,
          data: { data: { message: 'success' } }
        });

      const mockAddToast = jest
        .fn()
        .mockImplementation(() => ({ type: 'type' }));
      jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);

      await triggerChangeStatusClientConfiguration(requestActionData)(
        byPassRejected(
          'clientConfiguration/changeStatusClientConfiguration/rejected',
          {
            data: {
              messages: [{ status: 'success' }]
            }
          }
        ),
        store.getState,
        undefined
      );

      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'error',
        message: 'txt_client_configuration_failed_to_change_status',
        msgVariables: {
          status: 'txt_deactivate'
        }
      });
    });

    it('fulfilled > not Active', async () => {
      const mockAddToast = jest
        .fn()
        .mockImplementation(() => ({ type: 'type' }));
      jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);

      await triggerChangeStatusClientConfiguration({
        ...requestActionData,
        additionalFields: [
          {
            name: 'isConfigActive',
            active: false,
            value: 'aaa',
            description: 'aaa'
          }
        ]
      })(
        byPassFulfilled(
          'clientConfiguration/changeStatusClientConfiguration/fulfilled',
          {
            data: {
              messages: [{ status: 'success' }]
            }
          }
        ),
        store.getState,
        {}
      );

      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: 'txt_client_configuration_change_status',
        msgVariables: {
          status: 'txt_activated'
        }
      });
    });

    it('rejected > not Active', async () => {
      spy = jest
        .spyOn(clientConfigurationServices, 'changeStatusClientConfig')
        .mockResolvedValue({
          ...responseDefault,
          data: { data: { message: 'success' } }
        });

      const mockAddToast = jest
        .fn()
        .mockImplementation(() => ({ type: 'type' }));
      jest.spyOn(actionsToast, 'addToast').mockImplementation(mockAddToast);

      await triggerChangeStatusClientConfiguration({
        ...requestActionData,
        additionalFields: [
          {
            name: 'isConfigActive',
            active: false,
            value: 'aaa',
            description: 'aaa'
          }
        ]
      })(
        byPassRejected(
          'clientConfiguration/changeStatusClientConfiguration/rejected',
          {
            data: {
              messages: [{ status: 'success' }]
            }
          }
        ),
        store.getState,
        undefined
      );

      expect(mockAddToast).toBeCalledWith({
        show: true,
        type: 'error',
        message: 'txt_client_configuration_failed_to_change_status',
        msgVariables: {
          status: 'txt_activate'
        }
      });
    });
  });
});
