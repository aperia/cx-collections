import { clientConfigurationServices } from './../clientConfigurationServices';
import { byPassFulfilled, responseDefault } from 'app/test-utils';
import { updateClientInfo } from './updateClientInfo';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';

const requestActionData = {
  common: {
    agent: 'agent',
    clientNumber: 'clientNumber',
    system: 'system',
    prin: 'prin',
    app: 'app'
  },
  clientInfoId: 'clientInfoId',
  orgId: 'orgId',
  portfolioName: 'portfolioName',
  binSequences: [{}],
  additionalFields: [],

  rulesets: [],
  actionCodes: [],
  adjustments: {
    adjustmentCodes: [],
    adjustmentGroups: []
  },
  clusterQueueMappings: [],

  debtMgmtCompanyInfo: [],
  promiseConfigList: [],
  clientInfoLetterTemplate: []
};

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    org: 'org',
    app: 'app'
  }
};

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('ClientConfiguration > updateClientInfo', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {});
  });
  it('success', async () => {
    spy = jest
      .spyOn(clientConfigurationServices, 'addUpdateClientInfo')
      .mockResolvedValue({
        ...responseDefault,
        data: { message: 'success', responseStatus: 'success' }
      });

    const response = await updateClientInfo(requestActionData)(
      byPassFulfilled(updateClientInfo.fulfilled.type),
      store.getState,
      undefined
    );
    const responseDelete = await updateClientInfo({
      ...requestActionData,
      updateAction: 'delete'
    })(
      byPassFulfilled(updateClientInfo.fulfilled.type),
      store.getState,
      undefined
    );

    expect(response.type).toEqual(updateClientInfo.fulfilled.type);
    expect(responseDelete.type).toEqual(updateClientInfo.fulfilled.type);
  });

  it('error', async () => {
    spy = jest
      .spyOn(clientConfigurationServices, 'addUpdateClientInfo')
      .mockResolvedValue({
        ...responseDefault,
        data: { message: 'error', responseStatus: 'error' }
      });

    const response = await updateClientInfo(requestActionData)(
      byPassFulfilled(updateClientInfo.rejected.type),
      store.getState,
      undefined
    );
    const responseDelete = await updateClientInfo({
      ...requestActionData,
      updateAction: 'delete'
    })(
      byPassFulfilled(updateClientInfo.rejected.type),
      store.getState,
      undefined
    );

    expect(response.type).toEqual(updateClientInfo.rejected.type);
    expect(responseDelete.type).toEqual(updateClientInfo.rejected.type);
  });
});
