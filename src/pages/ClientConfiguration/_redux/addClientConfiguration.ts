import { I18N_CLIENT_CONFIG } from 'app/constants/i18n';
import { clientConfigurationServices } from './../clientConfigurationServices';

import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isRejected,
  isFulfilled
} from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { clientConfigurationActions } from './reducers';

import {
  AddClientConfigurationArgs,
  ClientConfigurationState,
  CreateClientConfigurationRequest,
  TriggerSaveClientConfigRequest
} from '../types';
import {
  BINSEQUENCES,
  CLIENT_CONFIG_STATUS_KEY,
  NAME_ACTION
} from '../constants';

import { AxiosResponse } from 'axios';
import { convertClientInfoId, rejectApi, successApi } from '../helpers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { saveChangedClientConfig } from '../ChangeHistory/_redux/saveChangedClientConfig';
import { batch } from 'react-redux';

export const addClientConfiguration = createAsyncThunk<
  AxiosResponse,
  CreateClientConfigurationRequest,
  ThunkAPIConfig
>('clientConfiguration/addClientConfiguration', async (args, thunkAPI) => {
  try {
    return await clientConfigurationServices.add(args);
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerAddClientConfiguration = createAsyncThunk<
  void,
  AddClientConfigurationArgs,
  ThunkAPIConfig
>(
  'clientConfiguration/triggerAddClientConfiguration',
  async (args, { dispatch, getState }) => {
    const {
      clientId,
      agentId,
      systemId,
      principleId,
      description,
      defaultCspa
    } = args;

    const isRetry = getState().clientConfiguration.add?.isRetry;

    const {
      user = '',
      app = '',
      org = '',
      x500id = '',
      privileges
    } = window?.appConfig?.commonConfig || {};

    const cspa = convertClientInfoId(clientId, systemId, principleId, agentId);

    const requestBody1: CreateClientConfigurationRequest = {
      common: {
        agent: agentId,
        clientNumber: clientId,
        system: systemId,
        prin: principleId,
        user,
        app
      },
      agent: agentId,
      clientNumber: clientId,
      system: systemId,
      prin: principleId,
      userId: user,
      portfolioName: description,
      x500Id: x500id,
      orgId: org,
      //need to hardcore binSequences
      binSequences: BINSEQUENCES,
      additionalFields: [
        {
          name: CLIENT_CONFIG_STATUS_KEY,
          active: true
        }
      ],
      copyDefaultConfig: true
    };

    const requestBody2: TriggerSaveClientConfigRequest = {
      common: {
        user,
        app,
        org,
        privileges
      },
      defaultCspa,
      callerAccountEntitlements: {
        cspas: [
          {
            cspa,
            description,
            active: true
          }
        ]
      },
      clientConfig: {
        cspas: [
          {
            cspa,
            description,
            active: true
          }
        ]
      },
      codeToText: {
        cspas: [
          {
            cspa,
            description,
            active: true
          }
        ]
      }
    };

    if (isRetry) {
      const retryResponse = await dispatch(
        clientConfigurationActions.triggerSaveClientConfiguration(requestBody2)
      );
      if (isFulfilled(retryResponse)) {
        batch(() => {
          dispatch(
            clientConfigurationActions.setAddClientConfigErrorMessage({
              error: '',
              isRetry: false
            })
          );
          dispatch(
            successApi({
              toastMsg: 'txt_cspa_add_secure_case_management_success',
              nameAction: NAME_ACTION.addCspa
            })
          );
          dispatch(
            saveChangedClientConfig({
              action: 'ADD',
              changedCategory: 'clientConfiguration',
              newValue: {
                cspa: `${clientId}-${systemId}-${principleId}-${agentId}`,
                description
              }
            })
          );
        });
      }
      if (isRejected(retryResponse)) {
        dispatch(
          apiErrorNotificationAction.updateThunkApiError({
            storeId: API_ERROR_STORE_ID.ENTITLEMENT,
            forSection: 'inModalBody',
            rejectData: {
              ...retryResponse,
              payload: retryResponse.payload?.response
            }
          })
        );
        dispatch(
          rejectApi({
            errorMsgInline: 'txt_cspa_inline_failed_secure_case_management',
            toastMsg: 'txt_cspa_add_secure_case_management_failed',
            nameAction: NAME_ACTION.addCspa
          })
        );
      }
      return;
    }

    const response1 = await dispatch(addClientConfiguration(requestBody1));

    if (isRejected(response1)) {
      dispatch(
        apiErrorNotificationAction.updateThunkApiError({
          storeId: API_ERROR_STORE_ID.ENTITLEMENT,
          forSection: 'inModalBody',
          rejectData: {
            ...response1,
            payload: response1.payload?.response
          }
        })
      );
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: 'txt_cspa_failed_to_add_case_and_secure_management'
        })
      );
      return;
    }

    if (isFulfilled(response1)) {
      if (response1?.payload?.data?.errors) {
        dispatch(
          clientConfigurationActions.setAddClientConfigErrorMessage({
            error: I18N_CLIENT_CONFIG.CLIENT_CONFIGURATION_ALREADY_EXISTS,
            isRetry: false
          })
        );
        return;
      }

      const response2 = await dispatch(
        clientConfigurationActions.triggerSaveClientConfiguration(requestBody2)
      );

      if (isFulfilled(response2)) {
        batch(() => {
          dispatch(
            successApi({
              toastMsg: I18N_CLIENT_CONFIG.SAVE_CLIENT_CONFIG_MESSAGE,
              nameAction: NAME_ACTION.addCspa
            })
          );
          dispatch(
            saveChangedClientConfig({
              action: 'ADD',
              changedCategory: 'clientConfiguration',
              newValue: {
                cspa: `${clientId}-${systemId}-${principleId}-${agentId}`,
                description
              }
            })
          );
        });
      }

      if (isRejected(response2)) {
        dispatch(
          apiErrorNotificationAction.updateThunkApiError({
            storeId: API_ERROR_STORE_ID.ENTITLEMENT,
            forSection: 'inModalBody',
            rejectData: {
              ...response2,
              payload: response2.payload?.response
            }
          })
        );

        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: 'txt_cspa_add_case_management_success'
          })
        );

        dispatch(
          rejectApi({
            errorMsgInline: 'txt_cspa_inline_failed_secure_case_management',
            toastMsg: 'txt_cspa_add_secure_case_management_failed',
            nameAction: NAME_ACTION.addCspa
          })
        );
      }
    }
  }
);

export const addClientConfigurationBuilder = (
  builder: ActionReducerMapBuilder<ClientConfigurationState>
) => {
  builder
    .addCase(addClientConfiguration.pending, (draftState, action) => {
      draftState.loadingAddModal = true;
    })
    .addCase(addClientConfiguration.fulfilled, (draftState, action) => {
      draftState.loadingAddModal = false;
    })
    .addCase(addClientConfiguration.rejected, (draftState, action) => {
      draftState.loadingAddModal = false;
    });
};
