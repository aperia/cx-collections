import { mockActionCreator } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import * as getChangedHistoriesMock from '../ChangeHistory/_redux/getChangedHistories';
import { getLastHistory } from './getLastHistory';

const spyGetChangedHistories = mockActionCreator(getChangedHistoriesMock);

describe('Test getLastHistory async thunk', () => {
  it('fulfilled', async () => {
    spyGetChangedHistories(
      'getChangedHistoriesApi',
      () =>
        ({
          type: getLastHistory.fulfilled.type,
          meta: {
            arg: {},
            requestId: 'test request id',
            requestStatus: 'fulfilled'
          },
          payload: {
            data: {
              configChangeHistoryList: [
                {
                  date: '01/01/2021',
                  time: '9:00',
                  changedBy: 'test',
                  cspa: '',
                  action: '',
                  changedCategory: '',
                  changedSection: '',
                  changedItem: '',
                  before: '',
                  after: ''
                }
              ]
            }
          }
        } as any)
    );
    const store = createStore(rootReducer, {
      mapping: {
        data: {
          changeHistory: {
            date: 'date',
            time: 'time',
            changedBy: 'changedBy',
            cspa: '',
            action: 'action',
            changedCategory: '',
            changedSection: '',
            changedItem: '',
            before: 'before',
            after: 'after'
          }
        }
      }
    });

    const res = await getLastHistory({})(store.dispatch, store.getState, {});

    expect(res.type).toEqual('clientConfiguration/getLastHistory/fulfilled');
    expect(res.payload).toEqual({
      action: '',
      after: '',
      before: '',
      changedBy: 'test',
      date: '01/01/2021',
      time: '9:00'
    });
  });

  it('fulfilled2', async () => {
    spyGetChangedHistories(
      'getChangedHistoriesApi',
      () =>
        ({
          type: getLastHistory.fulfilled.type,
          meta: {
            arg: {},
            requestId: 'test request id',
            requestStatus: 'fulfilled'
          },
          payload: {
            data: {
              configChangeHistoryList: []
            }
          }
        } as any)
    );
    const store = createStore(rootReducer, {
      mapping: {}
    });

    const res = await getLastHistory({})(store.dispatch, store.getState, {});

    expect(res.type).toEqual('clientConfiguration/getLastHistory/fulfilled');
    expect(res.payload).toBeUndefined();
  });

  it('rejected', async () => {
    spyGetChangedHistories('getChangedHistoriesApi', () => ({
      type: getLastHistory.rejected.type,
      payload: undefined,
      meta: {
        arg: {},
        requestId: 'test request id',
        rejectedWithValue: true,
        requestStatus: 'rejected',
        aborted: true,
        condition: true
      },
      error: 'error'
    }));
    const store = createStore(rootReducer, {});

    const res = await getLastHistory({})(store.dispatch, store.getState, {});

    expect(res.type).toEqual('clientConfiguration/getLastHistory/rejected');
  });
});
