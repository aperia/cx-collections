import { ConfigData, AdditionalFieldsMapType } from './../types';
import { createSelector } from '@reduxjs/toolkit';
import { SortType } from 'app/_libraries/_dls/components';
import { DEFAULT_SORT_VALUE } from 'app/constants';
import {
  CLIENT_CONFIG_STATUS_KEY,
  DEFAULT_CLIENT_CONFIG_KEY
} from '../constants';
import isEmpty from 'lodash.isempty';
import { searchCSPA, sortCSPA } from 'pages/ConfigurationEntitlement/helpers';

const getIsLoadingList = (states: RootState) =>
  states.clientConfiguration?.list?.loading || false;

const getListData = (states: RootState) =>
  states.clientConfiguration?.list?.data || [];

const getSortByOfClientConfigList = (states: RootState) =>
  states.clientConfiguration?.list?.sortBy || DEFAULT_SORT_VALUE;

const getListError = (states: RootState) =>
  states.clientConfiguration?.list?.error || false;

const getSelectedConfig = (states: RootState) =>
  states.clientConfiguration?.settings?.selectedConfig || undefined;

const getSettingSections = (states: RootState) =>
  states.clientConfiguration?.settings?.sections || [];

const getIsOpenSettings = (states: RootState) =>
  states.clientConfiguration?.settings?.isOpen || false;

const getToggleSettings = (states: RootState) =>
  states.clientConfiguration?.settings?.toggle || false;

const getSearchValue = (states: RootState) =>
  states.clientConfiguration?.list?.searchValue?.trim() || '';

const getConfigWarningMsg = (states: RootState) =>
  states.clientConfiguration?.warningMessage || '';

const getLastHistory = (states: RootState) =>
  states.clientConfiguration?.lastHistory;

export const takeWarningMsg = createSelector(
  getConfigWarningMsg,
  (data: string) => data
);

export const selectToggleSettings = createSelector(
  getToggleSettings,
  (toggle: boolean) => toggle
);

export const selectOpenSettings = createSelector(
  getIsOpenSettings,
  (open: boolean) => open
);

export const selectConfig = createSelector(
  getSelectedConfig,
  (data: MagicKeyValue | undefined) => {
    const cspaStatus = data?.additionalFields?.find(
      (item: AdditionalFieldsMapType) => item.name === CLIENT_CONFIG_STATUS_KEY
    );

    return {
      ...data,
      status: cspaStatus?.active
    };
  }
);

export const selectSections = createSelector(
  getSettingSections,
  (data: RefDataValue[]) => data
);

export const selectLoadingClientConfigList = createSelector(
  getIsLoadingList,
  (isLoading: boolean) => isLoading
);

export const selectOriginData = createSelector(getListData, data => data);

export const selectClientConfigList = createSelector(
  getListData,
  getSortByOfClientConfigList,
  getSearchValue,
  (data: ConfigData[], sortBy: SortType, searchValue: string) => {
    return sortCSPA(searchCSPA(data, searchValue), sortBy);
  }
);

export const selectSortBy = createSelector(
  getSortByOfClientConfigList,
  (sortBy: SortType) => sortBy
);

export const selectListError = createSelector(
  getListError,
  (error: boolean) => error
);

export const selectOpenAddModal = createSelector(
  (states: RootState) => states.clientConfiguration?.openAddModal || false,
  (open: boolean) => open
);

export const selectOpenEditModal = createSelector(
  (states: RootState) => states.clientConfiguration?.openEditModal || false,
  (open: boolean) => open
);

export const selectOpenStatusModal = createSelector(
  (states: RootState) => states.clientConfiguration?.openStatusModal || false,
  (open: boolean) => open
);

export const selectLoadingAdd = createSelector(
  (states: RootState) => states.clientConfiguration?.loadingAddModal || false,
  (open: boolean) => open
);

export const selectLoadingEdit = createSelector(
  (states: RootState) => states.clientConfiguration?.loadingEditModal || false,
  (open: boolean) => open
);

export const selectLoadingSaveConfig = createSelector(
  (states: RootState) =>
    states.clientConfiguration?.loadingSaveClientConfig || false,
  (open: boolean) => open
);

export const selectLoadingStatus = createSelector(
  (states: RootState) =>
    states.clientConfiguration?.loadingStatusModal || false,
  (open: boolean) => open
);

export const selectAddErrorMessage = createSelector(
  (states: RootState) => states.clientConfiguration?.add?.errorMessage || '',
  (errorMessage: string) => errorMessage
);

export const selectAddRetry = createSelector(
  (states: RootState) => states.clientConfiguration?.add?.isRetry || false,
  (isRetry: boolean) => isRetry
);

export const selectUpdateErrorMessage = createSelector(
  (states: RootState) => states.clientConfiguration?.update?.errorMessage || '',
  (errorMessage: string) => errorMessage
);

export const selectUpdateRetry = createSelector(
  (states: RootState) => states.clientConfiguration?.update?.isRetry || false,
  (isRetry: boolean) => isRetry
);

export const selectSearchValue = createSelector(
  getSearchValue,
  (searchValue: string) => searchValue
);

export const selectTextSearchValue = createSelector(
  (states: RootState) =>
    states.clientConfiguration?.list?.textSearchValue || '',
  (textSearchValue: string) => textSearchValue
);

export const selectIsNoResultsFound = createSelector(
  getListData,
  selectClientConfigList,
  (data: ConfigData[], filteredData: ConfigData[]) => {
    return !isEmpty(data) && isEmpty(filteredData);
  }
);

export const selectIsNoRecords = createSelector(
  getListData,
  (data: ConfigData[]) => {
    return isEmpty(data);
  }
);

export const selectDefaultSettingsClientConfig = createSelector(
  getListData,
  (data: ConfigData[]) => {
    return data.find((item: ConfigData) =>
      item.additionalFields?.find(
        (el: AdditionalFieldsMapType) =>
          el.name === DEFAULT_CLIENT_CONFIG_KEY && el.value === 'true'
      )
    );
  }
);

export const selectLastHistory = createSelector(getLastHistory, data => data);
