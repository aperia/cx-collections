import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isRejected
} from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { ClientConfigurationHistory } from '../ChangeHistory/types';
import { getChangedHistoriesApi } from '../ChangeHistory/_redux/getChangedHistories';
import {
  ClientConfigurationState,
  GetLastHistoryArgs,
  GetLastHistoryPayload
} from '../types';
import orderBy from 'lodash.orderby';

export const getLastHistory = createAsyncThunk<
  GetLastHistoryPayload,
  GetLastHistoryArgs,
  ThunkAPIConfig
>(
  'clientConfiguration/getLastHistory',
  async (args, { dispatch, getState, rejectWithValue }) => {
    const { mapping } = getState();
    const { cspaList } = args;

    const response = await dispatch(
      getChangedHistoriesApi({
        cspaList
      })
    );

    if (isRejected(response))
      return rejectWithValue({ response: response.error });

    const historyDataMapped: ClientConfigurationHistory[] =
      mappingDataFromArray(
        response.payload.data.configChangeHistoryList,
        mapping?.data?.changeHistory || {}
      );

    return orderBy(
      historyDataMapped,
      [item => new Date(`${item.date} ${item.time}`)],
      'desc'
    )[0];
  }
);

export const getLastHistoryBuilder = (
  builder: ActionReducerMapBuilder<ClientConfigurationState>
) => {
  builder
    .addCase(getLastHistory.pending, draftState => {
      draftState.lastHistory = undefined;
    })
    .addCase(getLastHistory.fulfilled, (draftState, action) => {
      draftState.lastHistory = action.payload;
    })
    .addCase(getLastHistory.rejected, draftState => {
      draftState.lastHistory = undefined;
    });
};
