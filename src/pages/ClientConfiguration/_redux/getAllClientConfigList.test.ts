import { createStore, Store } from '@reduxjs/toolkit';
import { byPassFulfilled, byPassRejected } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import {
  getAllClientConfigList,
  getAllClientConfigListApi
} from './getAllClientConfigList';
import './reducers';

let store: Store<RootState>;
let state: RootState;

let spy: jest.SpyInstance;

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    user: 'user',
    org: 'org',
    app: 'app'
  }
};

describe('Test getAllClientConfigListApi', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });
  beforeEach(() => {
    store = createStore(rootReducer, {});
    state = store.getState();
  });

  it('Case 1', async () => {
    const response = await getAllClientConfigListApi()(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'clientConfiguration/getAllClientConfigListApi/rejected'
    );
  });
});

describe('should getAllClientConfigListBuilder', () => {
  beforeEach(() => {
    store = createStore(rootReducer, {});
  });

  it('should response getAllClientConfigListList pending', () => {
    const pendingAction = getAllClientConfigList.pending(
      getAllClientConfigList.pending.type,
      undefined
    );
    const actual = rootReducer(state, pendingAction);

    expect(actual?.clientConfiguration?.list?.loading).toEqual(true);
  });

  it('should response getAllClientConfigList fulfilled', () => {
    const fulfilled = getAllClientConfigList.fulfilled(
      {},
      getAllClientConfigList.fulfilled.type,
      undefined
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual?.clientConfiguration?.list?.loading).toEqual(false);
    expect(actual?.clientConfiguration?.list?.data).toEqual(undefined);
  });

  it('should response getAllClientConfigList rejected', () => {
    const rejected = getAllClientConfigList.rejected(
      null,
      getAllClientConfigList.rejected.type,
      undefined
    );
    const actual = rootReducer(state, rejected);

    expect(actual?.clientConfiguration?.list?.loading).toEqual(false);
  });

  it('Fulfilled', async () => {
    store = createStore(rootReducer, {
      mapping: {
        data: {
          clientConfig: { id: 'id_mock' }
        }
      } as never
    });
    const res = await getAllClientConfigList()(
      byPassFulfilled(
        'clientConfiguration/getAllClientConfigListApi/fulfilled',
        {
          data: {
            messages: [{ status: 'success' }],
            clientInfoList: [{ id_mock: '01' }]
          }
        }
      ),
      store.getState,
      {}
    );

    expect(res.payload).toEqual({ data: [{ id: '01' }] });
  });

  it('Fulfilled > store not contain clientConfig', async () => {
    store = createStore(rootReducer, {
      mapping: {
        data: {}
      } as never
    });
    const res = await getAllClientConfigList()(
      byPassFulfilled(
        'clientConfiguration/getAllClientConfigListApi/fulfilled',
        {
          data: {
            messages: [{ status: 'success' }],
            clientInfoList: [{ id_mock: '01' }]
          }
        }
      ),
      store.getState,
      {}
    );

    expect(res.payload).toEqual({ data: [{}] });
  });

  it('Fulfilled > response not contain clientInfoList', async () => {
    store = createStore(rootReducer, {
      mapping: {
        data: {}
      } as never
    });
    const res = await getAllClientConfigList()(
      byPassFulfilled(
        'clientConfiguration/getAllClientConfigListApi/fulfilled',
        {
          data: {
            messages: [{ status: 'success' }]
          }
        }
      ),
      store.getState,
      {}
    );
    expect(res.payload).toEqual({});
  });

  it('Rejected', async () => {
    const res: any = await getAllClientConfigList()(
      byPassRejected('clientConfiguration/getAllClientConfigList/rejected', {}),
      store.getState,
      {}
    );
    expect(res.type).toEqual(
      'clientConfiguration/getAllClientConfigList/rejected'
    );
  });
});
