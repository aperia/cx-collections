import { responseDefault } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { clientConfigurationServices } from '../clientConfigurationServices';
import { getSectionList } from './getSectionList';

let spy: jest.SpyInstance;

describe('Test getSectionList async thunk', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('fulfilled', async () => {
    spy = jest
      .spyOn(clientConfigurationServices, 'getSectionList')
      .mockResolvedValue({
        ...responseDefault
      });
    const store = createStore(rootReducer, {});

    const res = await getSectionList({})(store.dispatch, store.getState, {});

    expect(res.type).toEqual('clientConfiguration/getSectionList/fulfilled');
    expect(res.payload).toEqual({ data: null });
  });

  it('rejected', async () => {
    spy = jest
      .spyOn(clientConfigurationServices, 'getSectionList')
      .mockRejectedValue({});
    const store = createStore(rootReducer, {});

    const res = await getSectionList({})(store.dispatch, store.getState, {});

    expect(res.type).toEqual('clientConfiguration/getSectionList/rejected');
  });
});
