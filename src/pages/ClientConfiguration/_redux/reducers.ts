import {
  addClientConfigurationBuilder,
  triggerAddClientConfiguration
} from './addClientConfiguration';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
// async thunks

import {
  getAllClientConfigList,
  getAllClientConfigListBuilder
} from './getAllClientConfigList';
import { getSectionList, getSectionListBuilder } from './getSectionList';
// types
import {
  ChangeSearchValue,
  ChangeTextSearchValue,
  ClientConfigurationState,
  ConfigData
} from '../types';
import { SortType } from 'app/_libraries/_dls/components';
import {
  updateClientConfigurationBuilder,
  triggerUpdateClientConfiguration
} from './updateClientConfiguration';
import {
  changeStatusClientConfigurationBuilder,
  triggerChangeStatusClientConfiguration
} from './changeStatusClientConfiguration';
import { getLastHistory, getLastHistoryBuilder } from './getLastHistory';

import { getClientInfoData } from './getClientInfoById';
import { updateClientInfo } from './updateClientInfo';

import {
  triggerSaveClientConfigurationBuilder,
  triggerSaveClientConfiguration
} from './triggerSaveClientConfiguration';

import { getCollectionsClientConfig } from './getCollectionsClientConfig';
import { saveCollectionsClientConfig } from './saveCollectionsClientConfig';
import { checkSecureClientConfig } from './checkSecureClientConfig';
import { ClientConfigurationHistory } from '../ChangeHistory/types';
import {
  triggerUpdateSelectedConfiguration,
} from './triggerUpdateSelectedConfiguration';

const { actions, reducer } = createSlice({
  name: 'clientConfiguration',
  initialState: {
    list: {
      loading: true
    }
  } as ClientConfigurationState,
  reducers: {
    toggleSettingsModal: draftState => {
      const { isOpen = false } = draftState.settings || {};
      draftState.settings = {
        isOpen: !isOpen
      };
    },
    updateSelectedConfig: (
      draftState,
      action: PayloadAction<{ data: ConfigData }>
    ) => {
      const data = action.payload?.data;
      draftState.settings = {
        ...draftState.settings,
        selectedConfig: data
      };
    },
    toggleAddModal: draftState => {
      draftState.openAddModal = !draftState.openAddModal;
    },
    toggleEditModal: draftState => {
      draftState.openEditModal = !draftState.openEditModal;
    },
    toggleStatusModal: draftState => {
      draftState.openStatusModal = !draftState.openStatusModal;
    },
    updateSortBy: (draftState, action: PayloadAction<SortType>) => {
      const data = action.payload;
      draftState.list = {
        ...draftState.list,
        sortBy: data
      };
    },
    setAddClientConfigErrorMessage: (
      draftState,
      action: PayloadAction<MagicKeyValue>
    ) => {
      const { error, isRetry } = action.payload;
      draftState.add = {
        ...draftState.add,
        errorMessage: error,
        isRetry
      };
    },
    setEditClientConfigErrorMessage: (
      draftState,
      action: PayloadAction<MagicKeyValue>
    ) => {
      const { error, isRetry } = action.payload;
      draftState.update = {
        ...draftState.update,
        errorMessage: error,
        isRetry
      };
    },
    setConfigWarningMessage: (draftState, action: PayloadAction<string>) => {
      draftState.warningMessage = action.payload;
    },
    toggleSetting: (draftState, action: PayloadAction<boolean>) => {
      const data = action.payload;
      draftState.settings = {
        ...draftState.settings,
        toggle: data
      };
    },
    changeSearchValue: (
      draftState,
      action: PayloadAction<ChangeSearchValue>
    ) => {
      const { searchValue } = action.payload;

      draftState.list = {
        ...draftState.list,
        searchValue
      };
    },
    changeTextSearchValue: (
      draftState,
      action: PayloadAction<ChangeTextSearchValue>
    ) => {
      const { textSearchValue } = action.payload;

      draftState.list = {
        ...draftState.list,
        textSearchValue
      };
    },
    clearAndReset: draftState => {
      draftState.list = {
        ...draftState.list,
        textSearchValue: '',
        searchValue: '',
        sortBy: undefined
      };
    },
    removeStore: draftState => {
      draftState.list = undefined;
    },
    setLastHistory: (
      draftState,
      action: PayloadAction<ClientConfigurationHistory>
    ) => {
      draftState.lastHistory = action.payload;
    }
  },
  extraReducers: builder => {
    getSectionListBuilder(builder);
    addClientConfigurationBuilder(builder);
    updateClientConfigurationBuilder(builder);
    changeStatusClientConfigurationBuilder(builder);
    getAllClientConfigListBuilder(builder);
    triggerSaveClientConfigurationBuilder(builder);
    getLastHistoryBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getSectionList,
  triggerAddClientConfiguration,
  triggerUpdateClientConfiguration,
  getAllClientConfigList,
  getClientInfoData,
  updateClientInfo,
  triggerChangeStatusClientConfiguration,
  getCollectionsClientConfig,
  saveCollectionsClientConfig,
  triggerSaveClientConfiguration,
  checkSecureClientConfig,
  getLastHistory,
  triggerUpdateSelectedConfiguration
};

export { allActions as clientConfigurationActions, reducer };
