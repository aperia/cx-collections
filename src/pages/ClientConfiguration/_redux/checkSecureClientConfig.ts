import { createAsyncThunk } from '@reduxjs/toolkit';
import { AxiosResponse } from 'axios';
import isEmpty from 'lodash.isempty';
import { clientConfigurationServices } from '../clientConfigurationServices';
import {
  CollectionsClientConfigGroup,
  ICollectionsClientConfigs,
  IGetCollectionsClientConfigServiceArgs
} from '../types';
import { clientConfigurationActions } from './reducers';

export const checkSecureClientConfig = createAsyncThunk<
  unknown,
  undefined,
  ThunkAPIConfig
>('clientConfiguration/checkSecureClientConfig', async (args, thunkAPI) => {
  const state = thunkAPI.getState();
  const dispatch = thunkAPI.dispatch;
  try {
    const { agentId, clientId, systemId, principleId } =
      state.clientConfiguration.settings?.selectedConfig || {};
    const { privileges } = window.appConfig.commonConfig || {};

    const cspa = `${clientId}-${systemId}-${principleId}-${agentId}`;

    const requestBody: IGetCollectionsClientConfigServiceArgs = {
      common: {
        privileges
      },
      selectConfig: CollectionsClientConfigGroup.ClientConfig,
      selectCspa: cspa
    };

    const response: AxiosResponse<ICollectionsClientConfigs> =
      await clientConfigurationServices.getCollectionsClientConfig(requestBody);

    const {
      data: {
        configs: { clientConfig }
      }
    } = response;

    const data = clientConfig;

    if (isEmpty(data)) {
      dispatch(
        clientConfigurationActions.setConfigWarningMessage(
          'txt_client_config_warning_msg'
        )
      );
    }
    return { data };
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});
