import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { clientConfigurationServices } from '../clientConfigurationServices';
import {
  ClientConfigurationState,
  GetSectionListArgs,
  GetSectionListPayload
} from '../types';

export const getSectionList = createAsyncThunk<
  GetSectionListPayload,
  GetSectionListArgs,
  ThunkAPIConfig
>('clientConfiguration/getSectionList', async () => {
  const { data } = await clientConfigurationServices.getSectionList();
  return { data };
});

export const getSectionListBuilder = (
  builder: ActionReducerMapBuilder<ClientConfigurationState>
) => {
  builder.addCase(getSectionList.pending, (draftState, action) => {
    draftState.settings = {
      ...draftState?.settings,
      loading: true
    };
  });
  builder.addCase(getSectionList.fulfilled, (draftState, action) => {
    const payload = action.payload;

    draftState.settings = {
      ...draftState?.settings,
      sections: payload?.data
    };
  });
  builder.addCase(getSectionList.rejected, (draftState, action) => {
    draftState.settings = {
      ...draftState?.settings,
      loading: false
    };
  });
};
