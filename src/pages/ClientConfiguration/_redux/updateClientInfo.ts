import { clientConfigurationServices } from './../clientConfigurationServices';
import { createAsyncThunk } from '@reduxjs/toolkit';

import { UpdateClientInfoArgs, UpdateClientInfoRequestBody } from '../types';

import { AxiosResponse } from 'axios';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export const updateClientInfo = createAsyncThunk<
  AxiosResponse,
  UpdateClientInfoArgs,
  ThunkAPIConfig
>('clientConfiguration/updateClientInfo', async (args, thunkAPI) => {
  const { getState, dispatch } = thunkAPI;
  const { updateAction, ...data } = args;
  try {
    const {
      id: clientInfoId = '',
      agentId,
      clientId,
      systemId,
      principleId
    } = getState().clientConfiguration.settings?.selectedConfig || {};
    const { commonConfig } = window.appConfig;

    const requestBody: UpdateClientInfoRequestBody = {
      common: {
        clientNumber: clientId,
        system: systemId,
        prin: principleId,
        agent: agentId,
        app: commonConfig.app,
        user: commonConfig.user,
        org: commonConfig.org
      },
      clientInfoId,
      ...data
    };
    const response = await clientConfigurationServices.addUpdateClientInfo(
      requestBody
    );

    if (response.data.responseStatus !== 'success') throw response;

    if (updateAction === 'delete') {
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION,
          forSection: 'inClientConfigurationFeatureHeader'
        })
      );
    } else {
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION,
          forSection: 'inModalBody'
        })
      );
    }
    return response;
  } catch (error) {
    if (updateAction === 'delete') {
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION,
          forSection: 'inClientConfigurationFeatureHeader',
          apiResponse: error
        })
      );
    } else {
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId: API_ERROR_STORE_ID.CLIENT_CONFIGURATION,
          forSection: 'inModalBody',
          apiResponse: error
        })
      );
    }

    return thunkAPI.rejectWithValue({ response: error });
  }
});
