import { responseDefault } from 'app/test-utils';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import { clientConfigurationServices } from '../clientConfigurationServices';
import { checkSecureClientConfig } from './checkSecureClientConfig';

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    org: 'org',
    app: 'app'
  }
};

describe('checkSecureClientConfig', () => {
  it('with data', async () => {
    jest
      .spyOn(clientConfigurationServices, 'getCollectionsClientConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          configs: {
            clientConfig: '123'
          }
        }
      });
    const store = createStore(
      rootReducer,
      {
        clientConfiguration: {
          settings: {
            selectedConfig: {
              agentId: 'agentId',
              clientId: 'clientId',
              systemId: 'systemId',
              principleId: 'principleId '
            }
          }
        },
        userRole: {
          role: 'admin'
        }
      },
      applyMiddleware(thunk)
    );

    const response = await checkSecureClientConfig()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'clientConfiguration/checkSecureClientConfig/fulfilled'
    );
  });

  it('with error', async () => {
    jest
      .spyOn(clientConfigurationServices, 'getCollectionsClientConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });
    const store = createStore(
      rootReducer,
      {
        clientConfiguration: {
          settings: {}
        }
      },
      applyMiddleware(thunk)
    );

    const response = await checkSecureClientConfig()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'clientConfiguration/checkSecureClientConfig/rejected'
    );
  });

  it('with empty data', async () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: undefined
    };
    jest
      .spyOn(clientConfigurationServices, 'getCollectionsClientConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          configs: {
            clientConfig: null
          }
        }
      });
    const store = createStore(
      rootReducer,
      {
        clientConfiguration: {
          settings: {
            selectedConfig: {
              agentId: 'agentId',
              clientId: 'clientId',
              systemId: 'systemId',
              principleId: 'principleId '
            }
          }
        },
        userRole: {
          role: 'admin'
        }
      },
      applyMiddleware(thunk)
    );

    const response = await checkSecureClientConfig()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'clientConfiguration/checkSecureClientConfig/fulfilled'
    );
  });
});
