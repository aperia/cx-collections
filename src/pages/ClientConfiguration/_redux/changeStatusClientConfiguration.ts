import { I18N_CLIENT_CONFIG } from 'app/constants/i18n';
import { clientConfigurationServices } from '../clientConfigurationServices';
import { batch } from 'react-redux';
import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isRejected,
  isFulfilled
} from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { clientConfigurationActions } from './reducers';
import {
  AdditionalFieldsMapType,
  ChangeStatusClientConfigurationArgs,
  ChangeStatusClientConfigurationRequest,
  ClientConfigurationState,
  TriggerSaveClientConfigRequest
} from '../types';
import { AxiosResponse } from 'axios';
import {
  ACTIVATE,
  ACTIVATED,
  BINSEQUENCES,
  CLIENT_CONFIG_STATUS_KEY,
  DEACTIVATE,
  DEACTIVATED
} from '../constants';
import { saveChangedClientConfig } from '../ChangeHistory/_redux/saveChangedClientConfig';
import { convertClientInfoId } from '../helpers';

export const changeStatusClientConfiguration = createAsyncThunk<
  AxiosResponse,
  ChangeStatusClientConfigurationRequest,
  ThunkAPIConfig
>(
  'clientConfiguration/changeStatusClientConfiguration',
  async (args, thunkAPI) => {
    try {
      return await clientConfigurationServices.changeStatusClientConfig(args);
    } catch (error) {
      return thunkAPI.rejectWithValue({ response: error });
    }
  }
);

export const triggerChangeStatusClientConfiguration = createAsyncThunk<
  void,
  ChangeStatusClientConfigurationArgs,
  ThunkAPIConfig
>(
  'clientConfiguration/triggerChangeStatusClientConfiguration',
  async (args, thunkAPI) => {
    const { dispatch, getState } = thunkAPI;
    const { id, additionalFields, translateFn } = args;

    const isConfigStatus = additionalFields?.find(
      (item: AdditionalFieldsMapType) => item.name === CLIENT_CONFIG_STATUS_KEY
    );
    const { user, app, org, privileges } = window.appConfig.commonConfig;
    const { accessConfig, clientConfiguration } = getState();
    const {
      agentId = '',
      clientId = '',
      systemId = '',
      principleId = '',
      description = ''
    } = clientConfiguration?.settings?.selectedConfig || {};
    const cspa = convertClientInfoId(clientId, systemId, principleId, agentId);

    const [clientNumber = '', system = '$ALL', prin = '$ALL', agent = '$ALL'] =
      accessConfig?.cspaList?.[0]?.cspa?.split('-') || [];

    const isActive = isConfigStatus?.active === true;

    const requestBody: ChangeStatusClientConfigurationRequest = {
      common: {
        agent,
        clientNumber,
        system,
        prin,
        user,
        app
      },
      clientInfoId: id,
      binSequences: BINSEQUENCES,
      additionalFields: [
        {
          name: CLIENT_CONFIG_STATUS_KEY,
          active: !isActive
        }
      ]
    };
    const response = await dispatch(
      changeStatusClientConfiguration(requestBody)
    );

    const secureConfigItem = {
      cspas: [
        {
          cspa,
          description,
          active: !isActive
        }
      ]
    };

    const saveSecureInfoRequestBody: TriggerSaveClientConfigRequest = {
      common: {
        user,
        app,
        org,
        privileges
      },
      callerAccountEntitlements: secureConfigItem,
      clientConfig: secureConfigItem,
      codeToText: secureConfigItem
    };

    if (isFulfilled(response)) {
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_CLIENT_CONFIG.CLIENT_CONFIGURATION_CHANGE_STATUS,
            msgVariables: {
              status: `${translateFn(
                isActive ? DEACTIVATED.toLowerCase() : ACTIVATED.toLowerCase()
              )}`
            }
          })
        );
        dispatch(clientConfigurationActions.toggleStatusModal());
        dispatch(clientConfigurationActions.getAllClientConfigList());
        dispatch(
          saveChangedClientConfig({
            action: isActive ? 'DEACTIVATE' : 'ACTIVATE',
            changedCategory: 'clientConfiguration',
            oldValue: {
              active: isActive
            },
            newValue: {
              active: !isActive
            }
          })
        );
        dispatch(
          clientConfigurationActions.triggerSaveClientConfiguration(
            saveSecureInfoRequestBody
          )
        );
      });
    }

    if (isRejected(response)) {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message:
            I18N_CLIENT_CONFIG.CLIENT_CONFIGURATION_FAILED_TO_CHANGE_STATUS,
          msgVariables: {
            status: `${translateFn(
              isActive ? DEACTIVATE.toLowerCase() : ACTIVATE.toLowerCase()
            )}`
          }
        })
      );
    }
  }
);

export const changeStatusClientConfigurationBuilder = (
  builder: ActionReducerMapBuilder<ClientConfigurationState>
) => {
  builder
    .addCase(changeStatusClientConfiguration.pending, (draftState, action) => {
      draftState.loadingStatusModal = true;
    })
    .addCase(
      changeStatusClientConfiguration.fulfilled,
      (draftState, action) => {
        draftState.loadingStatusModal = false;
      }
    )
    .addCase(changeStatusClientConfiguration.rejected, (draftState, action) => {
      draftState.loadingStatusModal = false;
    });
};
