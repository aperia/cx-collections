import { clientConfigurationServices } from '../clientConfigurationServices';
import { createAsyncThunk } from '@reduxjs/toolkit';

import {
  GetClientInfoArgs,
  GetClientInfoRequestBody,
  GetClientInfoReturned
} from '../types';
import isArray from 'lodash.isarray';

export const getClientInfoData = createAsyncThunk<
  GetClientInfoReturned,
  GetClientInfoArgs,
  ThunkAPIConfig
>('clientConfiguration/getClientInfoById', async (args, thunkAPI) => {
  const { getState } = thunkAPI;
  const { dataType, fieldsToQuery } = args;
  const fieldsToInclude = isArray(fieldsToQuery)
    ? fieldsToQuery
    : [fieldsToQuery || dataType];

  const { commonConfig } = window.appConfig;

  try {
    const {
      id: clientInfoId = '',
      agentId,
      clientId,
      systemId,
      principleId
    } = getState().clientConfiguration.settings?.selectedConfig || {};

    const requestBody: GetClientInfoRequestBody = {
      common: {
        clientNumber: clientId,
        system: systemId,
        prin: principleId,
        agent: agentId,
        app: commonConfig.app,
        user: commonConfig.user,
        org: commonConfig.org
      },

      clientInfoId,
      fieldsToInclude,
      enableFallback: false
    };
    const response = await clientConfigurationServices.getClientInfoById(
      requestBody
    );
    if (response.data.responseStatus !== 'success') throw response;
    return { [dataType]: response.data.clientInfo?.[dataType] };
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});
