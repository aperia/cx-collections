import { FormatTime } from 'app/constants/enums';
import { formatTimeDefault } from 'app/helpers';
import { Company } from './CompanyList/types';
import { CCCS_ACTIONS } from './constants';
import * as helpers from './helpers';
import { ICurrentResultCCCS } from './types';

const gridDataAsc = [
  {
    id: 1,
    name: 'A'
  },
  {
    id: 2,
    name: 'B'
  }
];

const gridDataDesc = [
  {
    id: 2,
    name: 'B'
  },
  {
    id: 1,
    name: 'A'
  }
];

describe('helpers', () => {
  it('should not break when have no order', () => {
    const expectedOutput = [
      {
        id: 1,
        name: 'A'
      },
      {
        id: 2,
        name: 'B'
      }
    ];
    expect(helpers.sortGridData(gridDataAsc, { id: 'name' })).toEqual(
      expectedOutput
    );
  });
  it('sortGridDataAsc to asc', () => {
    const expectedOutput = [
      {
        id: 1,
        name: 'A'
      },
      {
        id: 2,
        name: 'B'
      }
    ];
    expect(
      helpers.sortGridData(gridDataAsc, { id: 'name', order: 'asc' })
    ).toEqual(expectedOutput);
  });
  it('sortGridDataAsc to desc', () => {
    const expectedOutput = [
      {
        id: 2,
        name: 'B'
      },
      {
        id: 1,
        name: 'A'
      }
    ];
    expect(
      helpers.sortGridData(gridDataAsc, { id: 'name', order: 'desc' })
    ).toEqual(expectedOutput);
  });
  it('sortGridDataDesc to asc', () => {
    const expectedOutput = [
      {
        id: 1,
        name: 'A'
      },
      {
        id: 2,
        name: 'B'
      }
    ];
    expect(
      helpers.sortGridData(gridDataDesc, { id: 'name', order: 'asc' })
    ).toEqual(expectedOutput);
  });
  it('sortGridDataDesc to desc', () => {
    const expectedOutput = [
      {
        id: 2,
        name: 'B'
      },
      {
        id: 1,
        name: 'A'
      }
    ];

    expect(
      helpers.sortGridData(gridDataDesc, { id: 'name', order: 'desc' })
    ).toEqual(expectedOutput);
  });
});

describe('prepareCompanyData', () => {
  it('should return empty data', () => {
    expect(helpers.prepareCompanyData({})).toEqual({
      address: '',
      companyName: undefined,
      contact: '',
      fax: undefined,
      phone: undefined
    });
  });
  it('should return correct data', () => {
    const companyData = {
      companyName: 'companyName',
      contact: 'contact',
      fax: '01202',
      phone: '012312',
      city: 'city',
      addressLineOne: 'addressLineOne',
      addressLineTwo: 'addressLineTwo',
      state: 'New York',
      zip: '12312'
    };
    expect(helpers.prepareCompanyData(companyData)).toEqual({
      address: 'addressLineOne, addressLineTwo, city, New York 12312',
      companyName: 'companyName',
      contact: 'contact',
      fax: '01202',
      phone: '012312'
    });
  });
});

describe('filterCompany', () => {
  const data: Company[] = [
    {
      companyName: 'companyName',
      contact: 'contact',
      fax: '01202',
      phone: '012312',
      city: 'city',
      addressLineOne: 'addressLineOne',
      addressLineTwo: 'addressLineTwo',
      state: {
        value: 'New York',
        description: ''
      },
      zip: '12312'
    }
  ];

  it('should return empty data', () => {
    expect(helpers.filterCompany([], '')).toEqual([]);
  });

  it('should return data when search value is undefined', () => {
    expect(helpers.filterCompany(data, undefined as unknown as string)).toEqual(
      data
    );
  });

  it('should return correct data', () => {
    expect(helpers.filterCompany(data, data[0].contact!)).toEqual([data[0]]);
  });
});

it('prepareUpdateDataCCCS', () => {
  const data: MagicKeyValue = {
    companyName: 'companyName',
    contact: 'contact',
    fax: '01202',
    phone: '012312',
    city: 'city',
    addressLineOne: 'addressLineOne',
    addressLineTwo: 'addressLineTwo',
    state: {
      value: 'New York',
      description: ''
    },
    zip: '12312',
    action: CCCS_ACTIONS.UPDATE_PLAN
  };

  expect(helpers.prepareUpdateDataCCCS([])).toEqual({
    clientSince: undefined,
    dateReceived: undefined,
    endDate: undefined,
    reagePaymentPeriod: undefined,
    sentVia: undefined,
    startDate: undefined,
    state: undefined
  });

  expect(helpers.prepareUpdateDataCCCS(data)).toEqual({
    action: 'ACCEPT',
    addressLineOne: 'addressLineOne',
    addressLineTwo: 'addressLineTwo',
    cccsStatusDeclineReason: undefined,
    cccsStatusDiscontinueReason: undefined,
    city: 'city',
    clientSince: undefined,
    companyName: 'companyName',
    contact: 'contact',
    dateReceived: undefined,
    endDate: undefined,
    fax: '01202',
    phone: '012312',
    reagePaymentPeriod: undefined,
    sentVia: undefined,
    startDate: undefined,
    state: 'New York',
    zip: '12312'
  });

  expect(
    helpers.prepareUpdateDataCCCS({
      ...data,
      action: CCCS_ACTIONS.DECLINE_PROPOSAL,
      declineProposalOption: {
        value: 'other',
        description: 'Other'
      }
    })
  ).toEqual({
    action: 'DECLINE',
    addressLineOne: 'addressLineOne',
    addressLineTwo: 'addressLineTwo',
    cccsStatusDeclineReason: undefined,
    cccsStatusDiscontinueReason: undefined,
    city: 'city',
    clientSince: undefined,
    companyName: 'companyName',
    contact: 'contact',
    dateReceived: undefined,
    endDate: undefined,
    fax: '01202',
    phone: '012312',
    reagePaymentPeriod: undefined,
    sentVia: undefined,
    startDate: undefined,
    state: 'New York',
    zip: '12312',
    declineProposalOption: {
      value: 'other',
      description: 'Other'
    }
  });

  expect(
    helpers.prepareUpdateDataCCCS({
      ...data,
      action: CCCS_ACTIONS.DECLINE_PROPOSAL,
      actionDeclineProposalText: 'actionDeclineProposalText',
      declineProposalOption: {
        value: 'IP',
        description: 'Invalid Proposal'
      }
    })
  ).toEqual({
    action: 'DECLINE',
    addressLineOne: 'addressLineOne',
    addressLineTwo: 'addressLineTwo',
    cccsStatusDeclineReason: 'Invalid Proposal',
    cccsStatusDiscontinueReason: undefined,
    city: 'city',
    clientSince: undefined,
    companyName: 'companyName',
    contact: 'contact',
    dateReceived: undefined,
    endDate: undefined,
    fax: '01202',
    phone: '012312',
    reagePaymentPeriod: undefined,
    sentVia: undefined,
    startDate: undefined,
    state: 'New York',
    zip: '12312',
    actionDeclineProposalText: 'actionDeclineProposalText',
    declineProposalOption: {
      value: 'IP',
      description: 'Invalid Proposal'
    }
  });

  expect(
    helpers.prepareUpdateDataCCCS({
      ...data,
      action: CCCS_ACTIONS.DISCONTINUE_PLAN,
      actionDiscontinuePlanText: 'actionDiscontinuePlanText',
      discontinuePlanOption: {
        value: 'IP',
        description: 'Invalid Proposal'
      }
    })
  ).toEqual({
    action: 'DISCONTINUE',
    addressLineOne: 'addressLineOne',
    addressLineTwo: 'addressLineTwo',
    cccsStatusDeclineReason: undefined,
    cccsStatusDiscontinueReason: 'Invalid Proposal',
    city: 'city',
    clientSince: undefined,
    companyName: 'companyName',
    contact: 'contact',
    dateReceived: undefined,
    endDate: undefined,
    fax: '01202',
    phone: '012312',
    reagePaymentPeriod: undefined,
    sentVia: undefined,
    startDate: undefined,
    state: 'New York',
    zip: '12312',
    actionDiscontinuePlanText: 'actionDiscontinuePlanText',
    discontinuePlanOption: {
      value: 'IP',
      description: 'Invalid Proposal'
    }
  });

  expect(
    helpers.prepareUpdateDataCCCS({
      ...data,
      action: CCCS_ACTIONS.DISCONTINUE_PLAN,
      actionDiscontinuePlanText: 'actionDiscontinuePlanText',
      discontinuePlanOption: {
        value: 'other',
        description: 'Other'
      }
    })
  ).toEqual({
    action: 'DISCONTINUE',
    addressLineOne: 'addressLineOne',
    addressLineTwo: 'addressLineTwo',
    cccsStatusDeclineReason: undefined,
    cccsStatusDiscontinueReason: 'actionDiscontinuePlanText',
    city: 'city',
    clientSince: undefined,
    companyName: 'companyName',
    contact: 'contact',
    dateReceived: undefined,
    endDate: undefined,
    fax: '01202',
    phone: '012312',
    reagePaymentPeriod: undefined,
    sentVia: undefined,
    startDate: undefined,
    state: 'New York',
    zip: '12312',
    actionDiscontinuePlanText: 'actionDiscontinuePlanText',
    discontinuePlanOption: {
      value: 'other',
      description: 'Other'
    }
  });

  expect(
    helpers.prepareUpdateDataCCCS({
      ...data,
      reagePaymentPeriod: { value: 0 }
    })
  ).toEqual({
    action: 'ACCEPT',
    addressLineOne: 'addressLineOne',
    addressLineTwo: 'addressLineTwo',
    cccsStatusDeclineReason: undefined,
    cccsStatusDiscontinueReason: undefined,
    city: 'city',
    clientSince: undefined,
    dateReceived: undefined,
    endDate: undefined,
    companyName: 'companyName',
    contact: 'contact',
    fax: '01202',
    phone: '012312',
    sentVia: undefined,
    startDate: undefined,
    state: 'New York',
    zip: '12312'
  });
});

describe('isCompanyEqual', () => {
  const companyA: Company = {
    companyName: 'companyA',
    contact: 'contact',
    fax: '01202',
    phone: '012312',
    city: 'city',
    addressLineOne: 'addressLineOne',
    addressLineTwo: 'addressLineTwo',
    state: {
      value: 'New York',
      description: ''
    },
    zip: '12312'
  };
  const companyB: Company = {
    companyName: 'companyB',
    contact: 'contact',
    fax: '01202',
    phone: '012312',
    city: 'city',
    addressLineOne: 'addressLineOne',
    addressLineTwo: 'addressLineTwo',
    state: {
      value: 'New York',
      description: ''
    },
    zip: '12312'
  };

  it('should return data', () => {
    expect(helpers.isCompanyEqual(companyA, companyB)).toEqual(false);
  });
});

describe('prepareReturnDataCCCS', () => {
  const company: ICurrentResultCCCS = {
    companyName: 'companyA',
    contact: 'contact',
    fax: '01202',
    phone: '012312',
    city: 'city',
    addressLineOne: 'addressLineOne',
    addressLineTwo: 'addressLineTwo',
    state: 'New York',
    zip: '12312'
  };

  it('should return data', () => {
    expect(helpers.prepareReturnDataCCCS(company)).toEqual({
      ...company,
      reagePaymentPeriod: {
        description: '',
        value: '0'
      }
    });
  });

  it('should return data when existed active cccs and reagePaymentPeriod', () => {
    expect(
      helpers.prepareReturnDataCCCS({
        ...company,
        activeCccsExists: true,
        reagePaymentPeriod: '5',
        sentVia: '5'
      } as MagicKeyValue)
    ).toEqual({
      ...company,
      reagePaymentPeriod: {
        description: '5 Months',
        value: '5'
      },
      sentVia: {
        description: '5',
        value: '5'
      },
      activeCccsExists: true,
      minimumAmount: undefined,
      balancePercentage: undefined
    });
  });

  it('should return data when existed active cccs ', () => {
    expect(
      helpers.prepareReturnDataCCCS({
        ...company,
        reagePaymentPeriod: '5',
        activeCccsExists: true
      } as MagicKeyValue)
    ).toEqual({
      ...company,
      reagePaymentPeriod: {
        description: '5 Months',
        value: '5'
      },
      activeCccsExists: true,
      minimumAmount: undefined,
      balancePercentage: undefined
    });
  });
});

describe('preparePostCompanyData', () => {
  const company: Company = {
    companyName: 'companyA',
    fax: '01202',
    phone: '012312',
    city: 'city',
    addressLineOne: 'addressLineOne',
    zip: '12312'
  };

  it('should return data', () => {
    expect(helpers.preparePostCompanyData(company)).toEqual({
      ...company,
      addressLineTwo: ' ',
      contact: ' '
    });
  });
});

describe('formatCCCSNumeric', () => {
  it('should return data when  input is numeric', () => {
    expect(helpers.formatCCCSNumeric({ dmTotalDebt: 4 })).toEqual({
      dmTotalDebt: '4'
    });
  });

  it('should return data when  input is not numeric', () => {
    expect(helpers.formatCCCSNumeric({ dmTotalDebt: 'test' })).toEqual({
      dmTotalDebt: 'test'
    });
  });

  it('should return data when  not dmTotalDebt', () => {
    expect(helpers.formatCCCSNumeric({ dmTotalDebtNull: 'test' })).toEqual({
      dmTotalDebtNull: 'test'
    });
  });
});

const isReAgePaymentPeriodChangeFromNoneToOtherValueTestCases = [
  {
    caseName:
      "should return true if from null to {value: 3, description: '3 Months'}",
    previous: null,
    current: { value: 3, description: '3 Months' },
    expected: true
  },
  {
    caseName:
      "should return true if from {value: 0, description: 'None'} to {value: 3, description: '3 Months'}",
    previous: { value: 0, description: 'None' },
    current: { value: 3, description: '3 Months' },
    expected: true
  },
  {
    caseName:
      "should return true if from {value: '0', description: 'None'} to {value: 3, description: '3 Months'}",
    previous: { value: '0', description: 'None' },
    current: { value: 3, description: '3 Months' },
    expected: true
  },
  {
    caseName:
      "should return true if from 0 to {value: 3, description: '3 Months'}",
    previous: 0,
    current: { value: 3, description: '3 Months' },
    expected: true
  },
  {
    caseName:
      "should return true if from '0' to {value: 3, description: '3 Months'}",
    previous: '0',
    current: { value: 3, description: '3 Months' },
    expected: true
  },
  {
    caseName:
      "should return false if from {value: 4, description: '4 Months'} to {value: 3, description: '3 Months'}",
    previous: { value: 4, description: '4 Months' },
    current: { value: 3, description: '3 Months' },
    expected: false
  },
  {
    caseName:
      "should return false if from {value: 3, description: '3 Months'} to {value: 3, description: '3 Months'}",
    previous: { value: 3, description: '3 Months' },
    current: { value: 3, description: '3 Months' },
    expected: false
  }
];

describe.each(isReAgePaymentPeriodChangeFromNoneToOtherValueTestCases)(
  'isReAgePaymentPeriodChangeFromNoneToOtherValue',
  ({ caseName, previous, current, expected }) => {
    it(caseName, () => {
      expect(
        helpers.isReAgePaymentPeriodChangeFromNoneToOtherValue(
          previous,
          current
        )
      ).toEqual(expected);
    });
  }
);

const isDiffEndDateTestCases = [
  {
    caseName: 'should return false if invalid dates',
    date: new Date('invalid date'),
    anotherDate: new Date('invalid date'),
    expected: false
  },
  {
    caseName: 'should return false if mmddyyyy is same',
    date: new Date(),
    anotherDate: new Date(),
    expected: false
  },
  {
    caseName: 'should return true if mmddyyyy is diff',
    date: new Date(),
    anotherDate: new Date(new Date().setMonth(new Date().getMonth() + 1)),
    expected: true
  }
];

describe.each(isDiffEndDateTestCases)(
  'isDiffEndDate',
  ({ caseName, date, anotherDate, expected }) => {
    it(caseName, () => {
      expect(helpers.isDiffEndDate(date, anotherDate)).toEqual(expected);
    });
  }
);

const prepareDateTestCases = [
  {
    caseName: `should set startDate as dmStartDate`,
    cccsData: {
      startDate: '08252021',
      endDate: '09252021',
      reagePaymentPeriod: '0',
      action: CCCS_ACTIONS.NEW_PROPOSAL
    },
    changesAction: CCCS_ACTIONS.PENDING_PROPOSAL
  },
  {
    caseName: `should set startDate as today`,
    cccsData: {
      startDate: '08252021',
      endDate: '09252021',
      reagePaymentPeriod: '0',
      action: CCCS_ACTIONS.PENDING_PROPOSAL
    },
    changesAction: CCCS_ACTIONS.ACCEPT_PROPOSAL
  },
  {
    caseName: `should set startDate as dmStartDate`,
    cccsData: {
      startDate: '08252021',
      endDate: '09252021',
      reagePaymentPeriod: '0',
      action: CCCS_ACTIONS.PENDING_PROPOSAL
    },
    changesAction: CCCS_ACTIONS.DECLINE_PROPOSAL
  },
  {
    caseName: `should set startDate as dmStartDate`,
    cccsData: {
      startDate: '08252021',
      endDate: '09252021',
      reagePaymentPeriod: '0',
      action: CCCS_ACTIONS.ACCEPT_PROPOSAL
    },
    changesAction: CCCS_ACTIONS.UPDATE_PLAN
  },
  {
    caseName: `should set startDate as dmStartDate`,
    cccsData: {
      startDate: '08252021',
      endDate: '09252021',
      reagePaymentPeriod: '0',
      action: CCCS_ACTIONS.ACCEPT_PROPOSAL
    },
    changesAction: CCCS_ACTIONS.DISCONTINUE_PLAN
  }
];

describe.each(prepareDateTestCases)(
  'prepareDate',
  ({ caseName, cccsData, changesAction }) => {
    it(`${caseName} if user update action from ${cccsData.action} to ${changesAction}`, () => {
      const today = formatTimeDefault(new Date(), FormatTime.DefaultPostDate);
      const dmStartDate = cccsData.startDate;

      const startDate = formatTimeDefault(
        helpers.prepareDate(cccsData, changesAction).startDate,
        FormatTime.DefaultPostDate
      );

      if (
        cccsData.action === CCCS_ACTIONS.PENDING_PROPOSAL &&
        changesAction === CCCS_ACTIONS.ACCEPT_PROPOSAL
      ) {
        expect(startDate).toEqual(today);
      } else {
        expect(startDate).toEqual(dmStartDate);
      }
    });
  }
);

const trimLeadingZeroNumericTestCases = [
  {
    caseName: `should return 123`,
    numeric: '000123.00',
    expected: '123'
  },
  {
    caseName: `should return 326`,
    numeric: '326',
    expected: '326'
  },
  {
    caseName: `should return 326.10`,
    numeric: '326.1',
    expected: '326.10'
  },
  {
    caseName: `should return 326.16`,
    numeric: '326.16',
    expected: '326.16'
  },
  {
    caseName: `should return 326.123`,
    numeric: '326.123',
    expected: '326.12'
  }
];

describe.each(trimLeadingZeroNumericTestCases)(
  'trimLeadingZeroNumeric',
  ({ caseName, numeric, expected }) => {
    it(caseName, () => {
      expect(helpers.trimLeadingZeroNumeric(numeric)).toEqual(expected);
    });
  }
);
