import companyService, {
  GetCompanyPostData,
  AddCompanyPostData,
  UpdateCompanyPostData
} from './api-service';

// utils
import { mockApiServices, storeId } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

describe('companyService', () => {
  describe('getCallerAuthentication', () => {
    const params: GetCompanyPostData = { common: { accountId: storeId } };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      companyService.getCompanyList(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      companyService.getCompanyList(params);

      expect(mockService).toBeCalledWith(apiUrl.company.getCompanyList, params);
    });
  });

  describe('addCompany', () => {
    const params: AddCompanyPostData = { common: { accountId: storeId } };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      companyService.addCompany(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      companyService.addCompany(params);

      expect(mockService).toBeCalledWith(apiUrl.company.addCompany, params);
    });
  });

  describe('updateCompany', () => {
    const params: UpdateCompanyPostData = { common: { accountId: storeId } };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      companyService.updateCompany(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      companyService.updateCompany(params);

      expect(mockService).toBeCalledWith(apiUrl.company.updateCompany, params);
    });
  });
});
