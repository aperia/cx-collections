// i18n
import { I18N_COLLECTION_FORM, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { Button, Icon, TextSearch } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import classNames from 'classnames';
import isEmpty from 'lodash.isempty';
import { companyActions } from 'pages/CCCS/CompanyList/_redux/reducers';
import {
  selectCheckList,
  selectIsNoCompanies,
  selectIsNoResultFound,
  selectLoading,
  selectOpenAddModal,
  selectOpenEditModal,
  selectSearchValue,
  selectTextSearchValue,
  selectTotalCompanies
} from 'pages/CCCS/CompanyList/_redux/selectors';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import ClearAndReset from 'pages/__commons/ClearAndReset';
import React, { ChangeEvent, useCallback, useEffect } from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';
import { isDirty } from 'redux-form';
import AddCompany from './AddCompany';
import CompanyGrid from './CompanyGrid';
import EditCompany from './EditCompany';

const CompanyList = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { storeId } = useAccountDetail();

  const totalCompanies = useStoreIdSelector<number>(selectTotalCompanies);
  const isNoResultFound = useStoreIdSelector<number>(selectIsNoResultFound);
  const isNoCompanies = useStoreIdSelector<number>(selectIsNoCompanies);
  const isOpenAddModal = useStoreIdSelector<number>(selectOpenAddModal);
  const isOpenEditModal = useStoreIdSelector<boolean>(selectOpenEditModal);
  const searchValue = useStoreIdSelector<boolean>(selectSearchValue);
  const textSearchValue = useStoreIdSelector<string>(selectTextSearchValue);
  const loading = useStoreIdSelector<boolean>(selectLoading);
  const selectedCompanyId = useStoreIdSelector<string[]>(selectCheckList);
  const isCCCSFormDirty = useSelector(isDirty(`${storeId}-enterCCCS`));

  const testId = 'collectionForm_CCCS-form_company-list';

  const handleGetCompanyList = useCallback(() => {
    dispatch(
      companyActions.getCompanyList({
        storeId: storeId
      })
    );
  }, [dispatch, storeId]);

  const handleClearAndReset = () => {
    batch(() => {
      dispatch(companyActions.changeSearchValue({ storeId, searchValue: '' }));
      dispatch(
        companyActions.changeTextSearchValue({ storeId, textSearchValue: '' })
      );
    });
  };

  const handleSearch = (value: string) => {
    batch(() => {
      dispatch(companyActions.changePage({ storeId, pageNumber: 1 }));
      dispatch(
        companyActions.changeSearchValue({
          storeId,
          searchValue: value
        })
      );
    });
  };

  const handleAddCompany = () => {
    dispatch(companyActions.toggleAddModal({ storeId }));
  };

  const handleChangeTextSearch = (e: ChangeEvent<HTMLInputElement>) => {
    dispatch(
      companyActions.changeTextSearchValue({
        storeId,
        textSearchValue: e.target.value
      })
    );
  };

  const handleClearTextSearch = () => {
    dispatch(
      companyActions.changeTextSearchValue({
        storeId,
        textSearchValue: ''
      })
    );
  };

  useEffect(() => {
    handleGetCompanyList();
  }, [handleGetCompanyList, dispatch, storeId]);

  useEffect(() => {
    dispatch(
      collectionFormActions.changeDisabledOk({
        storeId,
        disabledOk: isEmpty(selectedCompanyId) || !isCCCSFormDirty
      })
    );
  }, [selectedCompanyId, dispatch, storeId, isCCCSFormDirty]);

  useEffect(() => {
    return () => {
      dispatch(
        companyActions.removeStore({
          storeId
        })
      );
    };
  }, [dispatch, storeId]);

  return (
    <div
      className={classNames({ loading })}
      data-testid={genAmtId(testId, 'container', '')}
    >
      <div className="custom-cccs d-flex align-items-center justify-content-between mb-16 mb-md-0">
        <div className="d-flex">
          <h6
            className="color-grey mb-md-16"
            data-testid={genAmtId(testId, 'title', '')}
          >
            {t(I18N_COLLECTION_FORM.DEBT_MANAGEMENT_COMPANY_LIST)}
          </h6>
          <span className="asterisk color-danger ml-4 mt-n4">*</span>
        </div>
        {!isNoCompanies && (
          <div className="mb-md-16">
            <TextSearch
              value={textSearchValue}
              onChange={handleChangeTextSearch}
              onClear={handleClearTextSearch}
              onSearch={handleSearch}
              placeholder={t(I18N_COMMON_TEXT.TYPE_TO_SEARCH)}
              dataTestId={genAmtId(testId, 'search', '')}
            />
          </div>
        )}
      </div>
      {!isNoCompanies && (
        <div className="d-flex align-items-center justify-content-between pb-16 mr-n8">
          {
            <p
              className="fs-14"
              data-testid={genAmtId(testId, 'total-companies', '')}
            >
              {!isNoResultFound &&
                !isNoCompanies &&
                t(I18N_COLLECTION_FORM.COMPANY_FOUND, {
                  count: totalCompanies
                })}
            </p>
          }
          <div>
            <Button
              size="sm"
              variant="outline-primary"
              onClick={handleAddCompany}
              dataTestId={genAmtId(testId, 'add-company-btn', '')}
            >
              {t(I18N_COLLECTION_FORM.ADD_COMPANY)}
            </Button>
            {!isNoResultFound && searchValue && (
              <Button
                size="sm"
                variant="outline-primary"
                onClick={handleClearAndReset}
                dataTestId={genAmtId(testId, 'clear-and-reset-btn', '')}
              >
                {t(I18N_COMMON_TEXT.CLEAR_AND_RESET)}
              </Button>
            )}
          </div>
        </div>
      )}
      {isNoResultFound && (
        <ClearAndReset
          className="mt-24 mb-16"
          onClearAndReset={handleClearAndReset}
          testId={`${testId}_no-result-found`}
        />
      )}
      {isNoCompanies && (
        <div className="text-center mt-80 mb-16">
          <Icon name="file" className="fs-80 color-light-l12" />
          <p
            className="mt-20 mb-24 color-grey"
            data-testid={genAmtId(testId, 'no-data-to-display', '')}
          >
            {t(I18N_COLLECTION_FORM.NO_COMPANIES_TO_DISPLAY)}
          </p>
          <Button
            size="sm"
            variant="outline-primary"
            onClick={handleAddCompany}
            dataTestId={genAmtId(testId, 'add-company-btn', '')}
          >
            {t(I18N_COLLECTION_FORM.ADD_COMPANY)}
          </Button>
        </div>
      )}
      {!isNoResultFound && !isNoCompanies && <CompanyGrid />}
      {isOpenAddModal && <AddCompany />}
      {isOpenEditModal && <EditCompany />}
    </div>
  );
};

export default CompanyList;
