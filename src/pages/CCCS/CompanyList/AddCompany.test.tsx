import React from 'react';
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import AddCompany from './AddCompany';
import { screen } from '@testing-library/react';
import { companyActions } from './_redux/reducers';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

const mockCompanyActions = mockActionCreator(companyActions);

beforeEach(() => {
  Element.prototype.scrollTo = jest.fn();
  Object.defineProperty(Element.prototype, 'scrollTop', {
    value: 0
  });
});
describe('AddCompany component', () => {
  it('should click cancel button', () => {
    const spy = mockCompanyActions('toggleAddModal');

    renderMockStoreId(<AddCompany />);

    const cancelButton = screen.getByText(I18N_COMMON_TEXT.CANCEL);
    cancelButton.click();

    expect(spy).toBeCalled();
  });

  it('should show inline error', () => {
    renderMockStoreId(<AddCompany />, {
      initialState: {
        company: {
          [storeId]: {
            add: { errorMessage: 'mock error' },
            editingCompany: {},
            loading: false
          }
        },
        apiErrorNotification: {
          [storeId]: {
            errorDetail: {
              inModalBody: { request: '' }
            }
          }
        }
      }
    });

    expect(screen.getByText('mock error')).toBeInTheDocument();
  });

  it('should click add button', () => {
    const spy = mockCompanyActions('triggerAddCompany');

    renderMockStoreId(<AddCompany />);

    const cancelButton = screen.getByText(I18N_COMMON_TEXT.SUBMIT);
    cancelButton.click();

    expect(spy).toBeCalled();
  });
});
