import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import React from 'react';
import CompanyGrid from './CompanyGrid';
import { screen } from '@testing-library/react';
import {
  GridProps,
  PaginationWrapperProps
} from 'app/_libraries/_dls/components';
import { companyActions } from './_redux/reducers';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { CollectionForm } from 'pages/CollectionForm/types';

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponent = jest.requireActual('app/_libraries/_dls/components');
  const { Grid } = dlsComponent;
  return {
    ...dlsComponent,
    Grid: (props: GridProps) => {
      return (
        <>
          <div data-testid="grid-onCheck" onClick={e => props.onCheck!([])} />
          <div
            data-testid="grid-onSortChange"
            onClick={() => props.onSortChange!({ id: 'id', order: 'asc' })}
          />
          <Grid {...props} />
        </>
      );
    },
    Pagination: (props: PaginationWrapperProps) => {
      return (
        <>
          <div
            data-testid="pagination-onChangePage"
            onClick={() => props.onChangePage!(1)}
          />
          <div
            data-testid="pagination-onChangePageSize"
            onClick={() => props.onChangePageSize!({ page: 1, size: 10 })}
          />
        </>
      );
    }
  };
});

const mockCollectionFormActions = mockActionCreator(collectionFormActions);
const mockCompanyActions = mockActionCreator(companyActions);

describe('CompanyGrid component', () => {
  it('should render component', () => {
    renderMockStoreId(<CompanyGrid />);
    expect(screen.getByText('txt_company_name')).toBeInTheDocument();
  });

  it('should call on sort change', () => {
    const updateSortBySpy = mockCompanyActions('updateSortBy');

    renderMockStoreId(<CompanyGrid />);

    const divElement = screen.getByTestId('grid-onSortChange');
    divElement.click();

    expect(updateSortBySpy).toBeCalled();
  });

  it('should call on check', () => {
    const setDirtyFormSpy = mockCollectionFormActions('setDirtyForm');
    const changeCheckListSpy = mockCompanyActions('changeCheckList');

    renderMockStoreId(<CompanyGrid />, {
      initialState: {
        collectionForm: {
          [storeId]: {
            disabledOk: false
          } as CollectionForm
        }
      }
    });

    const divElement = screen.getByTestId('grid-onCheck');
    divElement.click();

    expect(setDirtyFormSpy).toBeCalled();
    expect(changeCheckListSpy).toBeCalled();
  });

  it('should call change page of Pagination', () => {
    const changePageSpy = mockCompanyActions('changePage');
    const initialState = {
      company: {
        [storeId]: {
          companyList: [
            {
              companyName: 'a',
              phone: '',
              fax: '',
              contact: 'contact'
            }
          ]
        }
      }
    };

    renderMockStoreId(<CompanyGrid />, { initialState });

    const divElement = screen.getByTestId('pagination-onChangePage');
    divElement.click();

    expect(changePageSpy).toBeCalled();
  });

  it('should call change page size of Pagination', () => {
    const selectEditingCompanySpy = mockCompanyActions('selectEditingCompany');
    const changePageSpy = mockCompanyActions('changePage');
    const changePageSizeSpy = mockCompanyActions('changePageSize');
    const initialState = {
      company: {
        [storeId]: {
          companyList: [
            {
              companyName: 'a',
              phone: '(123) 123-1234',
              fax: '(123) 123-1234',
              contact: 'contact'
            }
          ],
          sortBy: {
            id: 'id'
          }
        }
      }
    };

    renderMockStoreId(<CompanyGrid />, { initialState });

    const editButton = screen.getByText('txt_edit');
    editButton.click();

    const divElement = screen.getByTestId('pagination-onChangePageSize');
    divElement.click();

    expect(selectEditingCompanySpy).toBeCalled();
    expect(changePageSpy).toBeCalled();
    expect(changePageSizeSpy).toBeCalled();
  });
});
