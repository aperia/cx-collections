import { apiService } from 'app/utils/api.service';

export interface GetCompanyPostData extends CommonAccountIdRequestBody {
  fieldsToInclude: string[];
  clientInfoId: string;
  enableFallback: boolean;
}

export interface AddCompanyPostData
  extends CommonAccountIdRequestBody,
    MagicKeyValue {}

export interface UpdateCompanyPostData
  extends CommonAccountIdRequestBody,
    MagicKeyValue {}

const companyService = {
  getCompanyList(data: GetCompanyPostData) {
    const url = window.appConfig?.api?.company?.getCompanyList || '';
    return apiService.post(url, data);
  },
  addCompany(data: AddCompanyPostData) {
    const url = window.appConfig?.api?.company?.addCompany || '';
    return apiService.post(url, data);
  },
  updateCompany(data: UpdateCompanyPostData) {
    const url = window.appConfig?.api?.company?.updateCompany || '';
    return apiService.post(url, data);
  }
};

export default companyService;
