import React from 'react';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { View } from 'app/_libraries/_dof/core';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import {
  InlineMessage,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

import { useDispatch, useSelector } from 'react-redux';
import { companyActions } from 'pages/CCCS/CompanyList/_redux/reducers';
import { isInvalid } from 'redux-form';
import {
  selectAddErrorMessage,
  selectLoading
} from 'pages/CCCS/CompanyList/_redux/selectors';

// constant
import { I18N_COLLECTION_FORM, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';

const AddCompany = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { storeId, accEValue } = useAccountDetail();

  const loading = useStoreIdSelector<boolean>(selectLoading);
  const errorMessage = useStoreIdSelector<string>(selectAddErrorMessage);

  const testId = 'collectionForm_CCCS-form_company-list_add-modal';

  const handleCloseModal = () => {
    dispatch(companyActions.toggleAddModal({ storeId }));
  };

  const handleSubmit = () => {
    dispatch(
      companyActions.triggerAddCompany({ storeId, accountId: accEValue })
    );
  };

  const isFormInvalid = useSelector(isInvalid(`${storeId}-companyViews`));

  return (
    <>
      <Modal
        sm
        show
        loading={loading}
        dataTestId={genAmtId(testId, 'container', '')}
      >
        <ModalHeader
          border
          closeButton
          onHide={handleCloseModal}
          dataTestId={genAmtId(testId, 'header', '')}
        >
          <ModalTitle dataTestId={genAmtId(testId, 'header-title', '')}>
            {t(I18N_COLLECTION_FORM.ADD_COMPANY)}
          </ModalTitle>
        </ModalHeader>
        <ModalBodyWithApiError
          storeId={storeId}
          apiErrorClassName="mb-16"
          dataTestId={genAmtId(testId, 'body-api-error', '')}
        >
          {errorMessage && (
            <InlineMessage
              className="mb-16"
              variant="danger"
              dataTestId={genAmtId(testId, 'body-error-message', '')}
            >
              {t(errorMessage)}
            </InlineMessage>
          )}
          <View
            id={`${storeId}-companyViews`}
            formKey={`${storeId}-companyViews`}
            descriptor="companyViews"
          />
        </ModalBodyWithApiError>
        <ModalFooter
          okButtonText={t(I18N_COMMON_TEXT.SUBMIT)}
          cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
          onCancel={handleCloseModal}
          disabledOk={isFormInvalid}
          onOk={handleSubmit}
          dataTestId={genAmtId(testId, 'footer', '')}
        />
      </Modal>
    </>
  );
};

export default AddCompany;
