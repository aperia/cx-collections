import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { batch } from 'react-redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { AxiosPromise } from 'axios';
import { CompanyListState, CompanyListArgs } from '../types';
import { companyActions } from './reducers';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import companyService from '../api-service';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { mappingDataFromArray } from 'app/helpers';
import { isCompanyEqual, preparePostCompanyData } from 'pages/CCCS/helpers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export const addCompany = createAsyncThunk<
  AxiosPromise | MagicKeyValue,
  CompanyListArgs,
  ThunkAPIConfig
>('company/addCompany', async (args, thunkAPI) => {
  const { storeId } = args;
  const { org, app } = window.appConfig?.commonConfig || {};
  const { getState } = thunkAPI;
  const { form, company, accountDetail, mapping } = getState();

  const currentCompanyList = company[storeId].companyList || [];
  const {
    clientIdentifier,
    systemIdentifier,
    principalIdentifier,
    agentIdentifier
  } = accountDetail[storeId]?.rawData || {};
  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;
  const companyMapping = mapping?.data?.company || {};
  const companyToBeAdded = form[`${storeId}-companyViews`].values || {};

  const allCompaniesCombined = [{ ...companyToBeAdded }, ...currentCompanyList];
  const companyPostData = allCompaniesCombined.map(item =>
    preparePostCompanyData(item)
  );

  const mappingCompanyToBeAdded = mappingDataFromArray(
    companyPostData,
    companyMapping,
    true
  );

  const clientInfoId = `${clientIdentifier}${systemIdentifier}${principalIdentifier}${agentIdentifier}`;

  try {
    return await companyService.addCompany({
      common: {
        accountId: eValueAccountId,
        org,
        app,
        agent: agentIdentifier,
        clientNumber: clientIdentifier,
        system: systemIdentifier,
        prin: principalIdentifier
      },
      clientInfoId,
      debtMgmtCompanyInfo: mappingCompanyToBeAdded
    });
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerAddCompany = createAsyncThunk<
  void,
  CompanyListArgs,
  ThunkAPIConfig
>('company/triggerAddCompany', async (args, { dispatch, getState }) => {
  const { storeId } = args;
  const { form, company } = getState();
  const currentCompanyList = company[storeId].companyList || [];
  const companyToBeAdded = form[`${storeId}-companyViews`].values || {};

  const isExistCompany =
    currentCompanyList.findIndex(companyItem =>
      isCompanyEqual(companyItem, companyToBeAdded)
    ) !== -1;

  if (isExistCompany) {
    dispatch(
      companyActions.setAddErrorMessage({
        storeId,
        errorMessage: 'txt_company_already_exists'
      })
    );

    return;
  }
  const response = await dispatch(addCompany(args));

  if (isFulfilled(response)) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'success',
        message: I18N_COLLECTION_FORM.COMPANY_ADDED_SUCCESS
      })
    );
    await dispatch(
      companyActions.getCompanyList({
        storeId
      })
    );
    batch(() => {
      dispatch(
        collectionFormActions.setDirtyForm({
          storeId,
          callResult: CALL_RESULT_CODE.CCCS,
          value: true
        })
      );
      dispatch(
        collectionFormActions.changeDisabledOk({
          storeId,
          disabledOk: false
        })
      );
      dispatch(
        companyActions.changeCheckList({
          storeId,
          checkList: [0]
        })
      );
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inModalBody'
        })
      );
    });
  }

  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_COLLECTION_FORM.COMPANY_ADDED_FAILURE
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inModalBody',
          apiResponse: response?.payload?.response
        })
      );
    });
  }
});

export const addCompanyBuilder = (
  builder: ActionReducerMapBuilder<CompanyListState>
) => {
  builder
    .addCase(addCompany.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        loading: true
      };
    })
    .addCase(addCompany.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId].loading = false;
      draftState[storeId].error = false;
      draftState[storeId].openAddModal = false;
      draftState[storeId].textSearchValue = '';
      draftState[storeId].searchValue = '';
      draftState[storeId].sortBy = undefined;
    })
    .addCase(addCompany.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId].loading = false;
      draftState[storeId].error = true;
    });
};
