import { selectorWrapper, storeId } from 'app/test-utils';
import * as selector from './selectors';

const states: Partial<RootState> = {
  company: {
    [storeId]: {
      openAddModal: true,
      openEditModal: true,
      loading: true,
      error: true,
      sortBy: { id: 'id', order: 'asc' },
      editingCompany: { id: 0 },
      textSearchValue: 'text-search-value',
      searchValue: 'search-value',
      pageNumber: 2,
      pageSize: 25,
      companyList: [{ id: 0 }, { id: 1 }, { id: 2 }],
      totalResults: 3,
      checkedList: ['0'],
      add: {
        errorMessage: 'error'
      },
      update: {
        errorMessage: 'error'
      }
    }
  },
  cccs: {} as MagicKeyValue
};

const emptyStates: Partial<RootState> = {
  company: {
    [storeId]: {
      checkedList: []
    }
  },
  cccs: {
    [storeId]: {
      data: {
        checkedList: ['0']
      }
    }
  } as MagicKeyValue
};

const selectorTrigger = selectorWrapper(states, emptyStates);

describe('CompanyList selectors', () => {
  it('should run selectOpenAddModal', () => {
    const { data, emptyData } = selectorTrigger(selector.selectOpenAddModal);
    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('should run selectOpenEditModal', () => {
    const { data, emptyData } = selectorTrigger(selector.selectOpenEditModal);
    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('should run selectEditingCompany', () => {
    const { data, emptyData } = selectorTrigger(selector.selectEditingCompany);
    expect(data).toEqual({ id: 0 });
    expect(emptyData).toEqual({});
  });

  it('should run selectLoading', () => {
    const { data, emptyData } = selectorTrigger(selector.selectLoading);
    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('should run selectError', () => {
    const { data, emptyData } = selectorTrigger(selector.selectError);
    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('should run selectSortBy', () => {
    const { data, emptyData } = selectorTrigger(selector.selectSortBy);
    expect(data).toEqual({ id: 'id', order: 'asc' });
    expect(emptyData).toEqual(undefined);
  });

  it('should run selectTextSearchValue', () => {
    const { data, emptyData } = selectorTrigger(selector.selectTextSearchValue);
    expect(data).toEqual('text-search-value');
    expect(emptyData).toEqual('');
  });

  it('should run selectSearchValue', () => {
    const { data, emptyData } = selectorTrigger(selector.selectSearchValue);
    expect(data).toEqual('search-value');
    expect(emptyData).toEqual('');
  });

  it('should run selectPageNumber', () => {
    const { data, emptyData } = selectorTrigger(selector.selectPageNumber);
    expect(data).toEqual(2);
    expect(emptyData).toEqual(1);
  });

  it('should run selectPageSize', () => {
    const { data, emptyData } = selectorTrigger(selector.selectPageSize);
    expect(data).toEqual(25);
    expect(emptyData).toEqual(5);
  });

  describe('should run selectCheckList', () => {
    it('with empty check list', () => {
      const { data, emptyData } = selectorTrigger(selector.selectCheckList);
      expect(data).toEqual(['0']);
      expect(emptyData).toEqual([]);
    });

    it('with check list', () => {
      const newState: Partial<RootState> = {
        cccs: { [storeId]: { checkedList: ['0'] } } as MagicKeyValue,
        company: {}
      };

      const newSelectorTrigger = selectorWrapper(newState, emptyStates);
      const { data: newData, emptyData: newEmptyData } = newSelectorTrigger(
        selector.selectCheckList
      );
      expect(newData).toEqual([]);
      expect(newEmptyData).toEqual([]);
    });

    it('with company name', () => {
      const newState: Partial<RootState> = {
        cccs: {
          [storeId]: { data: { companyName: 'company name' } }
        } as MagicKeyValue,
        company: {
          [storeId]: { companyList: [{ companyName: 'company name' }] }
        }
      };

      const newSelectorTrigger = selectorWrapper(newState, emptyStates);
      const { data: newData, emptyData: newEmptyData } = newSelectorTrigger(
        selector.selectCheckList
      );
      expect(newData).toEqual([0]);
      expect(newEmptyData).toEqual([]);
    });
  });

  it('should run selectCompanyList', () => {
    const { data, emptyData } = selectorTrigger(selector.selectCompanyList);
    expect(data).toEqual([{ id: 0 }, { id: 1 }, { id: 2 }]);
    expect(emptyData).toEqual([]);
  });

  it('should run selectSortedCompanyList', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectCompanyListGridData
    );
    expect(data).toEqual([]);
    expect(emptyData).toEqual([]);
  });

  it('should run selectTotalCompanies', () => {
    const { data, emptyData } = selectorTrigger(selector.selectTotalCompanies);
    expect(data).toEqual(0);
    expect(emptyData).toEqual(0);
  });

  it('should run selectIsNoCompanies', () => {
    const { data, emptyData } = selectorTrigger(selector.selectIsNoCompanies);
    expect(data).toEqual(false);
    expect(emptyData).toEqual(true);
  });

  it('should run selectIsNoResultFound', () => {
    const { data, emptyData } = selectorTrigger(selector.selectIsNoResultFound);
    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('should run selectAddErrorMessage', () => {
    const { data, emptyData } = selectorTrigger(selector.selectAddErrorMessage);
    expect(data).toEqual('error');
    expect(emptyData).toEqual(undefined);
  });

  it('should run selectUpdateErrorMessage', () => {
    const { data, emptyData } = selectorTrigger(
      selector.selectUpdateErrorMessage
    );
    expect(data).toEqual('error');
    expect(emptyData).toEqual(undefined);
  });
});
