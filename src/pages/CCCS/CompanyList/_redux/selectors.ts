import { SortType } from 'app/_libraries/_dls/components';
import { createSelector } from '@reduxjs/toolkit';
import isEmpty from 'lodash.isempty';
import { Company } from '../types';
import { PAGE_SIZE } from '../constants';
import { filterCompany, sortGridData } from '../../helpers';

export const selectOpenAddModal = createSelector(
  (states: RootState, storeId: string) =>
    states.company[storeId]?.openAddModal || false,
  (openAddModal: boolean) => openAddModal
);

export const selectOpenEditModal = createSelector(
  (states: RootState, storeId: string) =>
    states.company[storeId]?.openEditModal || false,
  (openEditModal: boolean) => openEditModal
);

export const selectEditingCompany = createSelector(
  (states: RootState, storeId: string) =>
    states.company[storeId]?.editingCompany || {},
  (editingCompany: Company) => editingCompany
);

export const selectLoading = createSelector(
  (states: RootState, storeId: string) =>
    states.company[storeId]?.loading || false,
  (loading: boolean) => loading
);

export const selectError = createSelector(
  (states: RootState, storeId: string) =>
    states.company[storeId]?.error || false,
  (error: boolean) => error
);

export const selectSortBy = createSelector(
  (states: RootState, storeId: string) => states.company[storeId]?.sortBy,
  (sortBy: SortType | undefined) => sortBy
);

export const selectTextSearchValue = createSelector(
  (states: RootState, storeId: string) =>
    states.company[storeId]?.textSearchValue || '',
  (textSearchValue: string) => textSearchValue
);

export const selectSearchValue = createSelector(
  (states: RootState, storeId: string) =>
    states.company[storeId]?.searchValue || '',
  searchValue => searchValue
);

export const selectPageNumber = createSelector(
  (states: RootState, storeId: string) =>
    states.company[storeId]?.pageNumber || 1,
  (pageNumber: number) => pageNumber
);

export const selectPageSize = createSelector(
  (states: RootState, storeId: string) =>
    states.company[storeId]?.pageSize || PAGE_SIZE[0],
  (pageSize: number) => pageSize
);

export const selectCheckList = createSelector(
  [
    (states: RootState, storeId: string) => states.cccs[storeId]?.data,
    (states: RootState, storeId: string) => states.company[storeId]
  ],
  (cccsData, company) => {
    if (!isEmpty(company?.checkedList)) {
      return company?.checkedList;
    }
    if (cccsData?.companyName) {
      const selectedCompanyIndex = company?.companyList?.findIndex(
        item => item?.companyName === cccsData?.companyName
      );
      return [selectedCompanyIndex];
    }
    return [];
  }
);

export const selectCompanyList: CustomStoreIdSelector<Company[]> =
  createSelector(
    (states: RootState, storeId: string) =>
      states.company[storeId]?.companyList,
    companyList => companyList || []
  );

export const selectCompanyListGridData = createSelector(
  [
    selectCompanyList,
    selectSortBy,
    selectSearchValue,
    selectPageNumber,
    selectPageSize
  ],
  (companyList, sortBy, searchValue, pageNumber, pageSize) => {
    const sortedData = sortGridData(companyList, sortBy);
    const filteredData = filterCompany(sortedData, searchValue);
    return filteredData.slice(
      pageSize * (pageNumber - 1),
      pageSize * pageNumber
    );
  }
);

export const selectTotalCompanies: CustomStoreIdSelector<number | undefined> =
  createSelector(
    [selectCompanyList, selectSearchValue],
    (totalCompanies, searchValue) =>
      filterCompany(totalCompanies, searchValue)?.length
  );

export const selectIsNoCompanies = createSelector(
  [selectTotalCompanies, selectSearchValue],
  (totalCompanies, searchValue) => totalCompanies === 0 && isEmpty(searchValue)
);

export const selectIsNoResultFound = createSelector(
  [selectTotalCompanies, selectSearchValue],
  (totalCompanies, searchValue) => totalCompanies === 0 && !isEmpty(searchValue)
);

export const selectAddErrorMessage = createSelector(
  (states: RootState, storeId: string) =>
    states.company[storeId]?.add?.errorMessage,
  errorMessage => errorMessage
);

export const selectUpdateErrorMessage = createSelector(
  (states: RootState, storeId: string) =>
    states.company[storeId]?.update?.errorMessage,
  errorMessage => errorMessage
);
