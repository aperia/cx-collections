import { companyActions } from './reducers';
import { addCompany, triggerAddCompany } from './addCompany';
import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault,
  storeId
} from 'app/test-utils';
import companyService from '../api-service';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';

describe('addCompany action', () => {
  let spy: jest.SpyInstance;

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('should is fulfilled', async () => {
    const store = createStore(rootReducer, {
      form: {
        [`${storeId}-companyViews`]: {}
      }
    });

    spy = jest
      .spyOn(companyService, 'addCompany')
      .mockResolvedValue({ ...responseDefault });

    const result = await addCompany({
      accountId: storeId,
      storeId
    })(store.dispatch, store.getState, {});

    expect(result.type).toEqual('company/addCompany/fulfilled');
  });

  it('should is rejected', async () => {
    const store = createStore(rootReducer, {
      form: {
        [`${storeId}-companyViews`]: {
          values: {}
        }
      }
    });

    spy = jest.spyOn(companyService, 'addCompany').mockRejectedValue({});

    const result = await addCompany({
      accountId: storeId,
      storeId
    })(store.dispatch, store.getState, {});

    expect(result.type).toEqual('company/addCompany/rejected');
  });
});

describe('triggerAddCompany action', () => {
  const initialState: Partial<RootState> = {
    company: { [storeId]: {} },
    form: {
      [`${storeId}-companyViews`]: { registeredFields: [] }
    }
  };
  const store = createStore(rootReducer, initialState);
  const toastSpy = mockActionCreator(actionsToast);
  const companyActionSpy = mockActionCreator(companyActions);

  it('should pass isRejected', async () => {
    const mockAction = toastSpy('addToast');

    await triggerAddCompany({
      accountId: storeId,
      storeId
    })(byPassRejected(addCompany.rejected.type), store.getState, {});

    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_COLLECTION_FORM.COMPANY_ADDED_FAILURE
    });
  });

  it('should not pass isFulfilled when company is existed', async () => {
    const initialState: Partial<RootState> = {
      company: {
        [storeId]: {
          companyList: [{ id: 0 }]
        }
      },
      form: {
        [`${storeId}-companyViews`]: { registeredFields: [] }
      }
    };
    const store = createStore(rootReducer, initialState);

    const setAddErrorMessage = companyActionSpy('setAddErrorMessage');

    await triggerAddCompany({
      accountId: storeId,
      storeId
    })(byPassFulfilled(addCompany.fulfilled.type), store.getState, {});

    expect(setAddErrorMessage).toBeCalledWith({
      storeId,
      errorMessage: 'txt_company_already_exists'
    });
  });

  it('should pass isFulfilled', async () => {
    const mockAction = toastSpy('addToast');
    const getCompanyListAction = companyActionSpy('getCompanyList');
    const changeCheckListAction = companyActionSpy('changeCheckList');

    await triggerAddCompany({
      accountId: storeId,
      storeId
    })(byPassFulfilled(addCompany.fulfilled.type), store.getState, {});

    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.COMPANY_ADDED_SUCCESS
    });
    expect(getCompanyListAction).toBeCalledWith({
      storeId
    });
    expect(changeCheckListAction).toBeCalledWith({
      storeId,
      checkList: [0]
    });
  });
});
