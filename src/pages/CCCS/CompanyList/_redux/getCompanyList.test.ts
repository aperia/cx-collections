import { CompanyListArgs } from '../types';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { getCompanyList } from './getCompanyList';
import { storeId } from 'app/test-utils';
import companyService from '../api-service';
import { AxiosResponse } from 'axios';

const requestData: CompanyListArgs = {
  storeId,
  accountId: storeId
};

describe('getCompanyList', () => {
  it('success without mapping data', async () => {
    jest.spyOn(companyService, 'getCompanyList').mockResolvedValue({
      data: { companyList: [], pageNumber: 1 }
    } as AxiosResponse);

    const store = createStore(rootReducer, {});
    const response = await getCompanyList({
      ...requestData,
      searchValue: '123'
    })(store.dispatch, store.getState, {});

    expect(response?.type).toBe(getCompanyList.fulfilled.type);
  });

  it('success with mapping data', async () => {
    jest.spyOn(companyService, 'getCompanyList').mockResolvedValue({
      data: {
        companyList: [],
        pageNumber: 1,
        clientInfo: {
          debtMgmtCompanyInfo: [
            {
              companyId: 'id'
            }
          ]
        }
      }
    } as AxiosResponse);

    const store = createStore(rootReducer, {
      mapping: {
        loading: false,
        data: { company: { companyId: 'companyId' } }
      },
      refData: { stateRefData: { data: [{ value: 'value', description: '' }] } }
    });

    const response = await getCompanyList({
      ...requestData,
      searchValue: '123'
    })(store.dispatch, store.getState, {});

    expect(response?.type).toBe(getCompanyList.fulfilled.type);
  });
  it('error', async () => {
    jest
      .spyOn(companyService, 'getCompanyList')
      .mockRejectedValue({} as AxiosResponse);

    const store = createStore(rootReducer, {});
    const response = await getCompanyList(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response?.type).toBe(getCompanyList.rejected.type);
  });
});
