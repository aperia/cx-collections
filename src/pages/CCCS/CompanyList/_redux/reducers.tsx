import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  ChangeCheckList,
  ChangePage,
  ChangePageSize,
  ChangeSearchValue,
  ChangeTextSearchValue,
  SelectEditingCompany,
  UpdateSortBy,
  CompanyListState,
  SetErrorMessage
} from '../types';
import { getCompanyList, getCompanyListBuilder } from './getCompanyList';
import { addCompanyBuilder, triggerAddCompany } from './addCompany';
import { updateCompanyBuilder, triggerUpdateCompany } from './updateCompany';
import { DEFAULT_PAGE_SIZE } from '../constants';

const { actions, reducer } = createSlice({
  name: 'company',
  initialState: {} as CompanyListState,
  reducers: {
    updateSortBy: (draftState, action: PayloadAction<UpdateSortBy>) => {
      const { storeId, sortBy } = action.payload;

      draftState[storeId].sortBy = sortBy;
    },
    changePage: (draftState, action: PayloadAction<ChangePage>) => {
      const { storeId, pageNumber } = action.payload;

      draftState[storeId].pageNumber = pageNumber;
    },
    changePageSize: (draftState, action: PayloadAction<ChangePageSize>) => {
      const { storeId, pageSize } = action.payload;

      draftState[storeId] = { ...draftState[storeId], pageSize };
    },
    changeSearchValue: (
      draftState,
      action: PayloadAction<ChangeSearchValue>
    ) => {
      const { storeId, searchValue } = action.payload;

      draftState[storeId] = { ...draftState[storeId], searchValue };
    },
    changeTextSearchValue: (
      draftState,
      action: PayloadAction<ChangeTextSearchValue>
    ) => {
      const { storeId, textSearchValue } = action.payload;

      draftState[storeId] = { ...draftState[storeId], textSearchValue };
    },
    changeCheckList: (draftState, action: PayloadAction<ChangeCheckList>) => {
      const { storeId, checkList } = action.payload;

      draftState[storeId].checkedList = checkList;
      draftState[storeId].checkedCompany = draftState[
        storeId
      ].companyList?.find(company => company.id === checkList[0]);
    },
    toggleAddModal: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;

      draftState[storeId].openAddModal = !draftState[storeId].openAddModal;
      draftState[storeId].add = {
        errorMessage: undefined
      };
    },
    toggleEditModal: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;

      draftState[storeId].openEditModal = !draftState[storeId].openEditModal;
      draftState[storeId].update = {
        errorMessage: undefined
      };
    },
    selectEditingCompany: (
      draftState,
      action: PayloadAction<SelectEditingCompany>
    ) => {
      const { storeId, editingCompany } = action.payload;

      draftState[storeId].openEditModal = !draftState[storeId].openEditModal;
      draftState[storeId].editingCompany = editingCompany;
    },
    clearAndReset: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;

      draftState[storeId].searchValue = '';
      draftState[storeId].textSearchValue = '';
      draftState[storeId].pageNumber = 1;
      draftState[storeId].pageSize = DEFAULT_PAGE_SIZE;
      draftState[storeId].sortBy = undefined;
    },
    setAddErrorMessage: (
      draftState,
      action: PayloadAction<SetErrorMessage>
    ) => {
      const { storeId } = action.payload;
      draftState[storeId].add = {
        ...draftState[storeId].add,
        errorMessage: action.payload?.errorMessage
      };
    },
    setUpdateErrorMessage: (
      draftState,
      action: PayloadAction<SetErrorMessage>
    ) => {
      const { storeId } = action.payload;
      draftState[storeId].update = {
        ...draftState[storeId].update,
        errorMessage: action.payload?.errorMessage
      };
    },
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    }
  },

  extraReducers: builder => {
    getCompanyListBuilder(builder);
    addCompanyBuilder(builder);
    updateCompanyBuilder(builder);
  }
});

const companyActions = {
  ...actions,
  getCompanyList,
  triggerAddCompany,
  triggerUpdateCompany
};

export { companyActions, reducer };
