import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import companyService from '../api-service';
import { PAGE_SIZE } from '../constants';
import {
  CompanyListState,
  CompanyListPayload,
  CompanyListArgs
} from '../types';

export const getCompanyList = createAsyncThunk<
  CompanyListPayload,
  CompanyListArgs,
  ThunkAPIConfig
>('company/getCompanyList', async (args, thunkAPI) => {
  const { storeId } = args;
  const { org, app } = window.appConfig?.commonConfig || {};
  const state = thunkAPI.getState();
  const { accountDetail } = state;
  const {
    clientIdentifier,
    systemIdentifier,
    principalIdentifier,
    agentIdentifier
  } = accountDetail[storeId]?.rawData || {};
  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;
  const clientInfoId = `${clientIdentifier}${systemIdentifier}${principalIdentifier}${agentIdentifier}`;

  const { data } = await companyService.getCompanyList({
    common: {
      accountId: eValueAccountId!,
      org,
      app,
      agent: agentIdentifier,
      clientNumber: clientIdentifier,
      system: systemIdentifier,
      prin: principalIdentifier
    },
    fieldsToInclude: ['debtMgmtCompanyInfo'],
    clientInfoId,
    enableFallback: true
  });

  const companyMapping = state.mapping?.data?.company || {};
  const mappingData = mappingDataFromArray<MagicKeyValue>(
    data?.clientInfo?.debtMgmtCompanyInfo,
    companyMapping
  );

  const mappingCompanyWithId = mappingData.map((item, index) => {
    const stateRefData = state.refData.stateRefData?.data?.find(
      stateItem => item.state === stateItem.value
    );

    return {
      ...item,
      id: index,
      state: stateRefData
    };
  });

  return {
    data: mappingCompanyWithId
  };
});

export const getCompanyListBuilder = (
  builder: ActionReducerMapBuilder<CompanyListState>
) => {
  builder
    .addCase(getCompanyList.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        loading: true,
        pageSize: draftState[storeId]?.pageSize || PAGE_SIZE[0],
        pageNumber: draftState[storeId]?.pageNumber || 1
      };
    })
    .addCase(getCompanyList.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId].companyList = action.payload.data;
      draftState[storeId].loading = false;
      draftState[storeId].error = false;
      draftState[storeId].totalResults = action.payload.data?.length;
    })
    .addCase(getCompanyList.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId].loading = false;
      draftState[storeId].error = true;
    });
};
