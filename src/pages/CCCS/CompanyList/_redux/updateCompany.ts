import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { companyActions } from './reducers';
import { batch } from 'react-redux';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { AxiosPromise } from 'axios';
import companyService from '../api-service';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { CompanyListState, CompanyListArgs } from '../types';
import { DEFAULT_PAGE_SIZE } from '../constants';

import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { mappingDataFromArray } from 'app/helpers';
import { isCompanyEqual, preparePostCompanyData } from 'pages/CCCS/helpers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export const updateCompany = createAsyncThunk<
  AxiosPromise | MagicKeyValue,
  CompanyListArgs,
  ThunkAPIConfig
>('company/updateCompany', async (args, thunkAPI) => {
  const { storeId } = args;
  const { org, app } = window.appConfig?.commonConfig || {};
  const { getState } = thunkAPI;
  const { form, company, accountDetail, mapping } = getState();
  const currentCompanyList = company[storeId].companyList || [];
  const {
    clientIdentifier,
    systemIdentifier,
    principalIdentifier,
    agentIdentifier
  } = accountDetail[storeId]?.rawData || {};
  const companyMapping = mapping?.data?.company || {};
  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;
  const companyToBeAdded = form[`${storeId}-companyViews`].values || {};

  // take the item to be updated outta the list and push to top
  const companiesWithoutUpdatedItem = currentCompanyList.filter(companyItem => {
    return companyItem.id !== companyToBeAdded.id;
  });
  const allCompanies = [
    { ...companyToBeAdded },
    ...companiesWithoutUpdatedItem
  ];
  const companyPostData = allCompanies.map(item =>
    preparePostCompanyData(item)
  );

  const mappingCompanyToBeAdded = mappingDataFromArray(
    companyPostData,
    companyMapping,
    true
  );

  const clientInfoId = `${clientIdentifier}${systemIdentifier}${principalIdentifier}${agentIdentifier}`;

  try {
    return await companyService.addCompany({
      common: {
        accountId: eValueAccountId,
        org,
        app,
        agent: agentIdentifier,
        clientNumber: clientIdentifier,
        system: systemIdentifier,
        prin: principalIdentifier
      },
      clientInfoId,
      debtMgmtCompanyInfo: mappingCompanyToBeAdded
    });
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerUpdateCompany = createAsyncThunk<
  void,
  CompanyListArgs,
  ThunkAPIConfig
>('company/triggerUpdateCompany', async (args, { dispatch, getState }) => {
  const { storeId } = args;
  const { form, company } = getState();
  const currentCompanyList = company[storeId].companyList || [];
  const companyToBeUpdated = form[`${storeId}-companyViews`].values || {};

  const filteredCompany = currentCompanyList.filter(
    companyItem => companyItem.id !== companyToBeUpdated.id
  );
  const isExistCompany =
    filteredCompany.findIndex(companyItem =>
      isCompanyEqual(companyItem, companyToBeUpdated)
    ) !== -1;

  if (isExistCompany) {
    dispatch(
      companyActions.setUpdateErrorMessage({
        storeId,
        errorMessage: 'txt_company_already_exists'
      })
    );

    return;
  }
  const response = await dispatch(updateCompany(args));

  if (isFulfilled(response)) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'success',
        message: I18N_COLLECTION_FORM.COMPANY_UPDATED_SUCCESS
      })
    );

    await dispatch(
      companyActions.getCompanyList({
        storeId,
        pageNumber: 1,
        searchValue: ''
      })
    );
    batch(() => {
      dispatch(
        collectionFormActions.setDirtyForm({
          storeId,
          callResult: CALL_RESULT_CODE.CCCS,
          value: true
        })
      );
      dispatch(
        collectionFormActions.changeDisabledOk({
          storeId,
          disabledOk: false
        })
      );
      dispatch(
        companyActions.changeCheckList({
          storeId,
          checkList: [0]
        })
      );
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inModalBody'
        })
      );
    });
  }

  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_COLLECTION_FORM.COMPANY_UPDATED_FAILED
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inModalBody',
          apiResponse: response?.payload?.response
        })
      );
    });
  }
});

export const updateCompanyBuilder = (
  builder: ActionReducerMapBuilder<CompanyListState>
) => {
  builder
    .addCase(updateCompany.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        loading: true
      };
    })
    .addCase(updateCompany.fulfilled, (draftState, action) => {
      const { storeId, id } = action.meta.arg;
      draftState[storeId].loading = false;
      draftState[storeId].error = false;
      draftState[storeId].openEditModal = false;
      draftState[storeId].checkedList = [id];
      draftState[storeId].pageNumber = 1;
      draftState[storeId].pageSize = DEFAULT_PAGE_SIZE;
      draftState[storeId].textSearchValue = '';
      draftState[storeId].searchValue = '';
      draftState[storeId].sortBy = undefined;
    })
    .addCase(updateCompany.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId].loading = false;
      draftState[storeId].error = true;
    });
};
