import { CompanyListState } from '../types';
import { companyActions, reducer } from './reducers';
import { storeId } from 'app/test-utils';
import { DEFAULT_PAGE_SIZE } from '../constants';

const {
  updateSortBy,
  changePage,
  changePageSize,
  changeSearchValue,
  changeTextSearchValue,
  changeCheckList,
  toggleAddModal,
  toggleEditModal,
  selectEditingCompany,
  clearAndReset,
  setAddErrorMessage,
  setUpdateErrorMessage,
  removeStore
} = companyActions;

const initialState: CompanyListState = {
  [storeId]: {
    openAddModal: false,
    openEditModal: false,
    companyList: [{ id: 0 }, { id: 1 }, { id: 2 }]
  }
};

describe('Company reducer', () => {
  it('should run updateSortBy action', () => {
    const expectValue = { id: 'id', order: 'asc' };
    const state = reducer(
      initialState,
      updateSortBy({ storeId, sortBy: { id: 'id', order: 'asc' } })
    );
    expect(state[storeId].sortBy).toEqual(expectValue);
  });

  it('should run changePage action', () => {
    const expectValue = 2;
    const state = reducer(initialState, changePage({ storeId, pageNumber: 2 }));
    expect(state[storeId].pageNumber).toEqual(expectValue);
  });

  it('should run changePageSize action', () => {
    const expectValue = 20;
    const state = reducer(
      initialState,
      changePageSize({ storeId, pageSize: 20 })
    );
    expect(state[storeId].pageSize).toEqual(expectValue);
  });

  it('should run changeSearchValue action', () => {
    const expectValue = 'search-value';
    const state = reducer(
      initialState,
      changeSearchValue({ storeId, searchValue: 'search-value' })
    );
    expect(state[storeId].searchValue).toEqual(expectValue);
  });

  it('should run changeTextSearchValue action', () => {
    const expectValue = 'text-search-value';
    const state = reducer(
      initialState,
      changeTextSearchValue({ storeId, textSearchValue: 'text-search-value' })
    );
    expect(state[storeId].textSearchValue).toEqual(expectValue);
  });

  it('should run changeCheckList action', () => {
    const expectValue = ['0', '1'];
    const state = reducer(
      initialState,
      changeCheckList({ storeId, checkList: ['0', '1'] })
    );
    expect(state[storeId].checkedList).toEqual(expectValue);
    expect(state[storeId].checkedCompany).toEqual(undefined);
  });

  it('should run toggleAddModal action', () => {
    const expectValue = true;
    const state = reducer(initialState, toggleAddModal({ storeId }));
    expect(state[storeId].openAddModal).toEqual(expectValue);
  });

  it('should run toggleEditModal action', () => {
    const expectValue = true;
    const state = reducer(initialState, toggleEditModal({ storeId }));
    expect(state[storeId].openEditModal).toEqual(expectValue);
  });

  it('should run selectEditingCompany action', () => {
    const state = reducer(
      initialState,
      selectEditingCompany({ storeId, editingCompany: { id: 0 } })
    );
    expect(state[storeId].openEditModal).toEqual(true);
    expect(state[storeId].editingCompany).toEqual({ id: 0 });
  });

  it('should run clearAndReset action', () => {
    const state = reducer(initialState, clearAndReset({ storeId }));
    expect(state[storeId].searchValue).toEqual('');
    expect(state[storeId].textSearchValue).toEqual('');
    expect(state[storeId].pageNumber).toEqual(1);
    expect(state[storeId].pageSize).toEqual(DEFAULT_PAGE_SIZE);
    expect(state[storeId].sortBy).toEqual(undefined);
  });

  it('should run setAddErrorMessage action', () => {
    const state = reducer(
      initialState,
      setAddErrorMessage({ storeId, errorMessage: 'error' })
    );
    expect(state[storeId].add!.errorMessage).toEqual('error');
  });

  it('should run setUpdateErrorMessage action', () => {
    const state = reducer(
      initialState,
      setUpdateErrorMessage({ storeId, errorMessage: 'error' })
    );
    expect(state[storeId].update!.errorMessage).toEqual('error');
  });

  it('should run removeStore action', () => {
    const state = reducer(initialState, removeStore({ storeId }));
    expect(state[storeId]).toEqual(undefined);
  });
});
