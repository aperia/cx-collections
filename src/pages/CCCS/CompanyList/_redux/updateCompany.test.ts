import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault,
  storeId
} from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import companyService from '../api-service';
import { companyActions } from './reducers';
import { triggerUpdateCompany, updateCompany } from './updateCompany';

describe('CompanyList updateCompany action', () => {
  let spy: jest.SpyInstance;

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('should is fulfilled', async () => {
    spy = jest
      .spyOn(companyService, 'addCompany')
      .mockResolvedValue({ ...responseDefault });

    const store = createStore(rootReducer, {
      company: { [storeId]: { companyList: [{ companyName: 'name' }] } },
      form: {
        [`${storeId}-companyViews`]: { registeredFields: [] }
      }
    });

    const result = await updateCompany({ accountId: storeId, storeId })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual('company/updateCompany/fulfilled');
  });

  it('should is reject', async () => {
    spy = jest.spyOn(companyService, 'addCompany').mockRejectedValue({});

    const store = createStore(rootReducer, {
      form: {
        [`${storeId}-companyViews`]: { registeredFields: [] }
      }
    });

    const result = await updateCompany({ accountId: storeId, storeId })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual('company/updateCompany/rejected');
  });
});

describe('CompanyList triggerUpdateCompany action', () => {
  const initialState: Partial<RootState> = {
    company: {
      [storeId]: {}
    },
    form: {
      [`${storeId}-companyViews`]: { registeredFields: [] }
    }
  };
  const store = createStore(rootReducer, initialState);
  const toastSpy = mockActionCreator(actionsToast);
  const companyActionSpy = mockActionCreator(companyActions);

  it('should not pass isFulfilled when company is existed', async () => {
    const newState: Partial<RootState> = {
      company: {
        [storeId]: {
          companyList: [{ id: 0 }]
        }
      },
      form: {
        [`${storeId}-companyViews`]: { registeredFields: [] }
      }
    };
    const newStore = createStore(rootReducer, newState);

    const setUpdateErrorMessage = companyActionSpy('setUpdateErrorMessage');

    await triggerUpdateCompany({
      accountId: storeId,
      storeId
    })(byPassFulfilled(updateCompany.fulfilled.type), newStore.getState, {});

    expect(setUpdateErrorMessage).toBeCalledWith({
      storeId,
      errorMessage: 'txt_company_already_exists'
    });
  });

  it('should pass isFulfilled', async () => {
    const mockAction = toastSpy('addToast');
    const getCompanyListAction = companyActionSpy('getCompanyList');
    const changeCheckListAction = companyActionSpy('changeCheckList');

    await triggerUpdateCompany({
      accountId: storeId,
      storeId
    })(byPassFulfilled(updateCompany.fulfilled.type), store.getState, {});

    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.COMPANY_UPDATED_SUCCESS
    });
    expect(getCompanyListAction).toBeCalledWith({
      storeId,
      pageNumber: 1,
      searchValue: ''
    });
    expect(changeCheckListAction).toBeCalledWith({
      storeId,
      checkList: [0]
    });
  });

  it('should pass isRejected', async () => {
    const mockAction = toastSpy('addToast');

    await triggerUpdateCompany({
      accountId: storeId,
      storeId
    })(byPassRejected(updateCompany.rejected.type), store.getState, {});

    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_COLLECTION_FORM.COMPANY_UPDATED_FAILED
    });
  });
});
