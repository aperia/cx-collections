import React, { useMemo } from 'react';
import {
  Button,
  Grid,
  Pagination,
  SortType
} from 'app/_libraries/_dls/components';
import { formatCommon, joinAddress, joinStateAndPostalCode } from 'app/helpers';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { batch, useDispatch } from 'react-redux';
import { companyActions } from 'pages/CCCS/CompanyList/_redux/reducers';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import {
  selectCheckList,
  selectPageNumber,
  selectPageSize,
  selectSortBy,
  selectCompanyListGridData,
  selectTotalCompanies
} from 'pages/CCCS/CompanyList/_redux/selectors';
import { PAGE_SIZE } from './constants';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// i18n
import { I18N_COLLECTION_FORM, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { Company } from './types';
import { genAmtId } from 'app/_libraries/_dls/utils';

const CompanyGrid = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const { storeId } = useAccountDetail();

  const companyList = useStoreIdSelector<Record<string, string>[]>(
    selectCompanyListGridData
  );

  const totalCompanies = useStoreIdSelector<number>(selectTotalCompanies);
  const sortBy = useStoreIdSelector<SortType>(selectSortBy);
  const pageNumber = useStoreIdSelector<number>(selectPageNumber);
  const checkedList = useStoreIdSelector<string[]>(selectCheckList);
  const pageSize = useStoreIdSelector<number>(selectPageSize);

  const testId = 'collectionForm_CCCS-form_company-list';

  const columns = useMemo(
    () => [
      {
        id: 'companyName',
        Header: t(I18N_COLLECTION_FORM.COMPANY_NAME),
        accessor: 'companyName',
        isSort: true,
        width: 270,
        cellProps: { className: 'text-break' }
      },
      {
        id: 'address',
        Header: t(I18N_COMMON_TEXT.ADDRESS),
        accessor: (data: Company, index: number) => (
          <span
            className="form-group-static__text"
            data-testid={genAmtId(`${index}_${testId}`, 'address', '')}
          >
            {joinAddress(
              data?.addressLineOne,
              data?.addressLineTwo,
              data?.city,
              joinStateAndPostalCode(data?.state?.value, data?.zip)
            )}
          </span>
        ),
        width: 360,
        cellProps: { className: 'text-break' }
      },
      {
        id: 'contact',
        Header: t(I18N_COMMON_TEXT.CONTACT),
        accessor: (data: Company, index: number) => (
          <span
            className="form-group-static__text"
            data-testid={genAmtId(`${index}_${testId}`, 'contact', '')}
          >
            {data?.contact}
          </span>
        ),
        isSort: true,
        width: 160,
        cellProps: { className: 'text-break' }
      },
      {
        id: 'phone',
        Header: t(I18N_COMMON_TEXT.PHONE),
        accessor: (data: Company, index: number) => (
          <span
            className="form-group-static__text"
            data-testid={genAmtId(`${index}_${testId}`, 'phone', '')}
          >
            {data?.phone ? formatCommon(data?.phone).phone : ''}
          </span>
        ),
        width: 140
      },
      {
        id: 'fax',
        Header: t(I18N_COMMON_TEXT.FAX),
        accessor: (data: Company, index: number) => (
          <span
            className="form-group-static__text"
            data-testid={genAmtId(`${index}_${testId}`, 'fax', '')}
          >
            {data?.fax ? formatCommon(data?.fax).phone : ''}
          </span>
        ),
        width: 140
      },

      {
        id: 'action',
        Header: t(I18N_COMMON_TEXT.ACTION),
        accessor: (item: Company, index: number) => {
          return (
            <Button
              size="sm"
              className="m-auto"
              variant="outline-primary"
              onClick={() =>
                dispatch(
                  companyActions.selectEditingCompany({
                    storeId,
                    editingCompany: item
                  })
                )
              }
              dataTestId={genAmtId(`${index}_${testId}`, 'edit-btn', '')}
            >
              {t(I18N_COMMON_TEXT.EDIT)}
            </Button>
          );
        },
        width: 95,
        cellProps: { className: 'text-center' },
        cellBodyProps: { className: 'td-sm' }
      }
    ],
    [dispatch, storeId, t]
  );

  const handleSort = (e: SortType) => {
    dispatch(companyActions.updateSortBy({ storeId, sortBy: e }));
  };

  const handleOnCheck = (dataKeyList: string[]) => {
    batch(() => {
      dispatch(
        collectionFormActions.setDirtyForm({
          storeId,
          callResult: CALL_RESULT_CODE.CCCS,
          value: true
        })
      );
      dispatch(
        companyActions.changeCheckList({ storeId, checkList: dataKeyList })
      );
      dispatch(
        collectionFormActions.changeDisabledOk({
          storeId,
          disabledOk: false
        })
      );
    });
  };

  const handleChangePage = (pageNumberValue: number) => {
    dispatch(
      companyActions.changePage({ storeId, pageNumber: pageNumberValue })
    );
  };

  const handleChangePageSize = ({ size }: { size: number }) => {
    batch(() => {
      dispatch(companyActions.changePageSize({ storeId, pageSize: size }));
      dispatch(companyActions.changePage({ storeId, pageNumber: 1 }));
    });
  };

  return (
    <div>
      <Grid
        columns={columns}
        data={companyList}
        variant={{
          id: 'checkbox_id',
          type: 'radio',
          isFixedLeft: true
        }}
        dataItemKey={'id'}
        checkedList={checkedList}
        onCheck={handleOnCheck}
        sortBy={sortBy ? [sortBy] : []}
        onSortChange={handleSort}
        dataTestId={genAmtId(testId, 'grid', '')}
      />
      <div className="pt-16">
        <Pagination
          totalItem={totalCompanies}
          pageSize={PAGE_SIZE}
          pageNumber={pageNumber}
          pageSizeValue={pageSize}
          onChangePage={handleChangePage}
          onChangePageSize={handleChangePageSize}
          dataTestId={genAmtId(testId, 'pagination', '')}
        />
      </div>
    </div>
  );
};

export default CompanyGrid;
