import React from 'react';
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import EditCompany from './EditCompany';
import { screen } from '@testing-library/react';
import { companyActions } from './_redux/reducers';
import userEvent from '@testing-library/user-event';
import * as reduxForm from 'redux-form';

const initialState: Partial<RootState> = {
  company: { [storeId]: { editingCompany: {}, loading: false } },
  accountDetail: {
    [storeId]: {}
  }
};

const mockCompanyActions = mockActionCreator(companyActions);

beforeEach(() => {
  Element.prototype.scrollTo = jest.fn();
  Object.defineProperty(Element.prototype, 'scrollTop', {
    value: 0
  });
});
describe('EditCompany component', () => {
  it('should render component', () => {
    renderMockStoreId(<EditCompany />, { initialState });

    expect(screen.getByText('txt_edit_company')).toBeInTheDocument();
  });

  it('should click error detail button', () => {
    renderMockStoreId(<EditCompany />, {
      initialState: {
        company: {
          [storeId]: {
            update: { errorMessage: 'error' },
            editingCompany: {},
            loading: false
          }
        },
        apiErrorNotification: {
          [storeId]: {
            errorDetail: {
              inModalBody: { request: '' }
            }
          }
        }
      }
    });
    expect(screen.getByText('txt_edit_company')).toBeInTheDocument();
  });

  it('should click cancel button', () => {
    const spy = mockCompanyActions('toggleEditModal');

    renderMockStoreId(<EditCompany />, { initialState });

    expect(screen.getByText('txt_cancel')).toBeInTheDocument();

    const cancelButton = screen.getByText('txt_cancel');
    userEvent.click(cancelButton);

    expect(spy).toBeCalled();
  });

  it('should click submit button', () => {
    const spy = mockCompanyActions('triggerUpdateCompany');
    jest.spyOn(reduxForm, 'isDirty').mockImplementation(() => () => true);

    renderMockStoreId(<EditCompany />, { initialState });

    expect(screen.getByText('txt_save')).toBeInTheDocument();

    const editButton = screen.getByText('txt_save');
    expect(editButton).toBeInTheDocument();
    userEvent.click(editButton);

    expect(spy).toBeCalled();
  });
});
