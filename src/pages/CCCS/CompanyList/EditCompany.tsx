import {
  useAccountDetail,
  useIsDirtyForm,
  useStoreIdSelector
} from 'app/hooks';
import { View } from 'app/_libraries/_dof/core';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import {
  InlineMessage,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { companyActions } from 'pages/CCCS/CompanyList/_redux/reducers';
import { isInvalid } from 'redux-form';
import { Company } from './types';
import {
  selectEditingCompany,
  selectLoading,
  selectUpdateErrorMessage
} from 'pages/CCCS/CompanyList/_redux/selectors';

// i18n
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';

const EditCompany = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { storeId, accEValue } = useAccountDetail();

  const company = useStoreIdSelector<Company>(selectEditingCompany);
  const loading = useStoreIdSelector<boolean>(selectLoading);
  const errorMessage = useStoreIdSelector<string>(selectUpdateErrorMessage);
  const isFormInvalid = useSelector(isInvalid(`${storeId}-companyViews`));
  const isNotChangeData = useIsDirtyForm([`${storeId}-companyViews`]);

  const testId = 'collectionForm_CCCS-form_company-list_edit-modal';

  const handleCloseModal = () => {
    dispatch(companyActions.toggleEditModal({ storeId }));
  };

  const handleSubmit = () => {
    dispatch(
      companyActions.triggerUpdateCompany({
        storeId,
        accountId: accEValue,
        id: company.id
      })
    );
  };

  return (
    <>
      <Modal
        sm
        show
        loading={loading}
        dataTestId={genAmtId(testId, 'container', '')}
      >
        <ModalHeader
          border
          closeButton
          onHide={handleCloseModal}
          dataTestId={genAmtId(testId, 'header', '')}
        >
          <ModalTitle dataTestId={genAmtId(testId, 'header-title', '')}>
            {t(I18N_COMMON_TEXT.EDIT_COMPANY)}
          </ModalTitle>
        </ModalHeader>
        <ModalBodyWithApiError
          dataTestId={genAmtId(testId, 'body-api-error', '')}
          apiErrorClassName="mb-16"
          storeId={storeId}
        >
          {errorMessage && (
            <InlineMessage
              className="mb-16"
              variant="danger"
              dataTestId={genAmtId(testId, 'body-error-message', '')}
            >
              {t(errorMessage)}
            </InlineMessage>
          )}
          <View
            id={`${storeId}-companyViews`}
            formKey={`${storeId}-companyViews`}
            descriptor="companyViews"
            value={company}
          />
        </ModalBodyWithApiError>
        <ModalFooter
          okButtonText={t(I18N_COMMON_TEXT.SAVE)}
          cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
          onCancel={handleCloseModal}
          disabledOk={isFormInvalid || !isNotChangeData}
          onOk={handleSubmit}
          dataTestId={genAmtId(testId, 'footer', '')}
        />
      </Modal>
    </>
  );
};

export default EditCompany;
