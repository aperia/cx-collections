import React from 'react';
import { fireEvent, screen } from '@testing-library/react';

// hooks
import { AccountDetailProvider } from 'app/hooks';

// helpers
import {
  mockActionCreator,
  queryAllByClass,
  renderMockStore,
  renderMockStoreId,
  storeId
} from 'app/test-utils';
import CompanyList from '.';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { GridProps } from 'app/_libraries/_dls/components';
import { companyActions } from './_redux/reducers';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import userEvent from '@testing-library/user-event';
import { Company } from './types';
import { queryByClass } from 'app/_libraries/_dls/test-utils';

jest.mock('./AddCompany', () => () => <div>AddCompany</div>);
jest.mock('./EditCompany', () => () => <div>EditCompany</div>);

export const companyList: Company[] = [
  {
    id: 1,
    companyName: '5TH VENTURE CATALYST',
    phone: '5355260021',
    fax: '5355260021',
    contact: 'Thomas Cesar',
    addressLineOne: '3350 Manley',
    city: 'Wyoming',
    zip: '49519'
  },
  {
    id: 2,
    companyName: 'EVIGT FOOD SERVICES',
    phone: '5355260021',
    fax: '5355260021',
    contact: 'Blaine Roberts',
    addressLineOne: '285 Pepper Wood',
    city: 'Nashville',
    zip: '37209'
  },
  {
    id: 3,
    companyName: 'ALPINE PLASTIC',
    phone: '5355260021',
    fax: '5355260021',
    contact: 'Aliya Montgomery',
    addressLineOne: '750 Academy Drive',
    city: 'Mobile',
    zip: '36691'
  },
  {
    id: 4,
    companyName: 'RELIANCE LIFE INSURANCE',
    phone: '5355265678',
    fax: '5355678021',
    contact: 'Kyng Hinton',
    addressLineOne: '2181 Pelham Pkwy',
    city: 'Hampton',
    zip: '23666'
  },
  {
    id: 5,
    companyName: 'RG RETAIL',
    phone: '4565260021',
    fax: '4565260021',
    contact: 'Chandler Rosario',
    addressLineOne: '626 Olive Street Sw',
    city: 'Billings',
    zip: '59101'
  }
];

const initialState: Partial<RootState> = {
  collectionForm: { [storeId]: { uploadFiles: {} } },
  company: {
    [storeId]: {
      companyList,
      searchValue: '5TH VENTURE CATALYST',
      totalResults: 10
    }
  },
  cccs: {
    [storeId]: {
      data: { companyName: 'RG RETAIL', currentBalance: 'currentBalance' }
    }
  }
};

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');
  const { Grid } = dlsComponents;
  return {
    ...dlsComponents,
    Grid: (props: GridProps) => {
      return (
        <>
          <input
            data-testid="grid-onExpand"
            onChange={e => props.onExpand!([e.target.value])}
          />
          <input
            data-testid="grid-onSortChange"
            onChange={(e: any) => props.onSortChange!(e.target.value)}
          />
          <Grid {...props} />
        </>
      );
    }
  };
});

// New
const mockCompanyActions = mockActionCreator(companyActions);
const mockCollectionFormActions = mockActionCreator(collectionFormActions);

beforeEach(() => {
  mockCompanyActions('removeStore');
  mockCompanyActions('getCompanyList');
  mockCollectionFormActions('changeDisabledOk');
});

const renderWrapper = (state: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <CompanyList />
    </AccountDetailProvider>,
    { initialState: state }
  );
};

describe('Render view', () => {
  it('Render UI with no company found', () => {
    renderWrapper({
      collectionForm: { [storeId]: { uploadFiles: {} } },
      company: {
        [storeId]: { companyList, searchValue: 'abc', totalResults: 0 }
      }
    });

    expect(screen.getByText('txt_memos_no_results_found')).toBeInTheDocument();
  });

  it('Render UI with no company', () => {
    renderWrapper({
      collectionForm: { [storeId]: { uploadFiles: {} } },
      company: {
        [storeId]: { companyList: [], searchValue: '', totalResults: 0 }
      }
    });

    expect(
      screen.getByText(I18N_COLLECTION_FORM.NO_COMPANIES_TO_DISPLAY)
    ).toBeInTheDocument();
  });

  it('Render UI', () => {
    renderWrapper(initialState);

    expect(
      screen.getByText(I18N_COLLECTION_FORM.DEBT_MANAGEMENT_COMPANY_LIST)
    ).toBeInTheDocument();
  });
});
describe('Render modal', () => {
  it('Render edit modal', () => {
    renderWrapper({
      collectionForm: { [storeId]: { uploadFiles: {} } },
      company: {
        [storeId]: {
          companyList,
          searchValue: '',
          totalResults: 10,
          openEditModal: true,
          editingCompany: {
            id: 4,
            companyName: 'RELIANCE LIFE INSURANCE',
            phone: '5355265678',
            fax: '5355678021',
            contact: 'Kyng Hinton',
            addressLineOne: '2181 Pelham Pkwy',
            city: 'Hampton',
            zip: '23666'
          }
        }
      }
    });

    expect(screen.getByText('EditCompany')).toBeInTheDocument();
  });
  it('Render add modal', () => {
    renderWrapper({
      collectionForm: { [storeId]: { uploadFiles: {} } },
      company: {
        [storeId]: {
          companyList,
          searchValue: '',
          totalResults: 10,
          openAddModal: true
        }
      }
    });

    expect(screen.getByText('AddCompany')).toBeInTheDocument();
  });
});

describe('CompanyList component', () => {
  it('should call handleClearAndReset', () => {
    const changeSearchValue = mockCompanyActions('changeSearchValue');
    const changeTextSearchValue = mockCompanyActions('changeTextSearchValue');

    renderMockStoreId(<CompanyList />, { initialState });

    const button = screen.getByText('txt_clear_and_reset');
    userEvent.click(button);

    expect(changeSearchValue).toBeCalled();
    expect(changeTextSearchValue).toBeCalled();
  });

  it('should call handleChangeTextSearch', () => {
    const spy = mockCompanyActions('changeTextSearchValue');

    renderMockStoreId(<CompanyList />, { initialState });

    const input = screen.getAllByRole('textbox');
    fireEvent.change(input[0], {
      target: { value: 'change-text' }
    });

    expect(spy).toBeCalled();
  });

  it('should call handleAdd', () => {
    const spy = mockCompanyActions('toggleAddModal');

    renderMockStoreId(<CompanyList />, { initialState });

    const button = screen.getByText(I18N_COLLECTION_FORM.ADD_COMPANY);
    userEvent.click(button);

    expect(spy).toBeCalled();
  });

  it('should call handleClearTextSearch ', () => {
    const spy = mockCompanyActions('changeTextSearchValue');

    const { wrapper } = renderMockStoreId(<CompanyList />, { initialState });

    const button = queryAllByClass(wrapper.container, /icon-close/)[0];
    userEvent.click(button);

    expect(spy).toBeCalled();
  });

  it('should call handleSearch', () => {
    const spy = mockCompanyActions('changeSearchValue');

    const { wrapper } = renderMockStoreId(<CompanyList />, {
      initialState: {
        ...initialState,
        company: {
          ...initialState.company,
          [storeId]: {
            ...initialState.company![storeId],
            textSearchValue: '5TH VENTURE CATALYST'
          }
        }
      }
    });
    const button = queryByClass(wrapper.container, /icon icon-search/);
    userEvent.click(button!);
    expect(spy).toBeCalled();
  });
});
