import { SortType } from 'app/_libraries/_dls/components';

export interface Company {
  id?: number;
  companyName?: string;
  addressLineOne?: string;
  addressLineTwo?: string;
  city?: string;
  state?: RefDataValue;
  zip?: string;
  contact?: string;
  phone?: string;
  fax?: string;
  companyEmail?: string;
}

export interface CompanyListState {
  [storeId: string]: {
    companyList?: Company[];
    totalResults?: number;
    loading?: boolean;
    error?: boolean;
    sortBy?: SortType;
    pageNumber?: number;
    pageSize?: number;
    searchValue?: string;
    textSearchValue?: string;
    checkedList?: number[] | string[];
    checkedCompany?: Company;
    openAddModal?: boolean;
    openEditModal?: boolean;
    editingCompany?: Company;
    add?: {
      errorMessage?: string;
    };
    update?: {
      errorMessage?: string;
    };
  };
}

export interface UpdateSortBy {
  storeId: string;
  sortBy: SortType | undefined;
}

export interface ChangePage {
  storeId: string;
  pageNumber: number;
}

export interface ChangePageSize {
  storeId: string;
  pageSize: number;
}

export interface ChangeCheckList {
  storeId: string;
  checkList: number[] | string[];
}

export interface SelectEditingCompany {
  storeId: string;
  editingCompany: Company;
}

export interface ChangeSearchValue {
  storeId: string;
  searchValue?: string;
}

export interface ChangeTextSearchValue {
  storeId: string;
  textSearchValue?: string;
}

export interface CompanyListArgs extends MagicKeyValue {
  storeId: string;
  searchValue?: string;
  pageNumber?: number;
  pageSize?: number;
  checkedList?: number[] | string[];
}

export interface CompanyListPayload {
  data: Company[];
}

export interface SetErrorMessage extends StoreIdPayload {
  errorMessage?: string;
}
