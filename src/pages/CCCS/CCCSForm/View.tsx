import { IRadio } from 'app/components/_dof_controls/controls';
import {
  isReAgePaymentPeriodNone,
  updateEndDateByReAgePaymentPeriod
} from 'app/components/_dof_controls/custom-functions/onChangeReagePaymentPeriod';
import { formatCommon, isValidDate } from 'app/helpers';
// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { isEqual } from 'app/_libraries/_dls/lodash';
// component
import { ExtraFieldProps, View } from 'app/_libraries/_dof/core';
import {
  dispatchDelayProgress,
  dispatchDestroyDelayProgress
} from 'pages/CollectionForm/helpers';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { setStatusNFPAInit } from 'pages/HardShip/helpers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import React, {
  useCallback,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { useDispatch } from 'react-redux';
import { Field } from 'redux-form';
import { CCCS_ACTIONS, ENABLE_RADIO_RULES, I18N_CCCS } from '../constants';
import {
  isDiffEndDate,
  isReAgePaymentPeriodChangeFromNoneToOtherValue,
  prepareDate
} from '../helpers';
// redux
import { ICurrentResultCCCS } from '../types';
import { cccsActions } from '../_redux/reducers';
import { selectCCCSData } from '../_redux/selectors';

const ViewUpdateCCCS: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [showActionsOptions, setShowActionsOptions] = useState<boolean>(false);
  const { storeId, accEValue } = useAccountDetail();

  const formKy = `${storeId}-enterCCCS`;
  const formActionCCCSKy = `${storeId}-selectReasonCCCSView`;

  const cccsData = useStoreIdSelector<ICurrentResultCCCS>(selectCCCSData);

  const ref = useRef<any>(null);
  const actionRef = useRef<any>(null);
  const reasonRef = useRef<HTMLDivElement | null>(null);

  const setFieldDisabled = useCallback((name: string, value: boolean) => {
    ref?.current?.props?.onFind(name)?.props?.props?.setReadOnly(value);
  }, []);

  const setFieldValue = useCallback((name: string, value: any) => {
    const targetFieldShadow = ref?.current?.props?.onFind(name);
    const targetActionsField = actionRef?.current?.props?.onFind(name);
    targetFieldShadow?.props?.props?.setValue(value);
    targetActionsField?.props?.props?.setValue(value);
  }, []);

  const setFieldValidationRules = useCallback((name: string, rules: any[]) => {
    ref?.current?.props
      ?.onFind(name)
      ?.props?.props?.setOthers((pre: MagicKeyValue) => {
        // const { validationRules: currentRules } = pre;

        return {
          ...pre,
          validationRules: [
            // ...(Array.isArray(currentRules) ? currentRules : []),
            ...rules
          ]
        };
      });
  }, []);

  useEffect(() => {
    dispatchDestroyDelayProgress(storeId);

    return () => {
      dispatchDelayProgress(
        cccsActions.getCCCSDetail({
          storeId
        }),
        storeId
      );
    };
  }, [dispatch, storeId, accEValue]);

  /**
   * Add form constraint logic
   */
  useEffect(() => {
    const setImmediateInstance = setImmediate(() => {
      if (!ref?.current || !cccsData) return;

      const nonDelinquentMpdAmountErrMsg = t(
        I18N_CCCS.NON_DELINQUENT_MPD_AMOUNT_ERR_MSG,
        {
          nonDelinquentMpdAmount: formatCommon(
            cccsData?.nonDelinquentMpdAmount || '0'
          ).currency(2)
        }
      );

      setFieldValidationRules('nonDelinquentMpdAmount', [
        {
          ruleName: 'LessThanOrEqual',
          errorMsg: nonDelinquentMpdAmountErrMsg,
          ruleOptions: {
            otherValue: cccsData?.nonDelinquentMpdAmount
          }
        }
      ]);

      const paymentAmountErrMsg = t(I18N_CCCS.PAYMENT_AMOUNT_ERR_MSG, {
        minimumAmount: formatCommon(cccsData?.paymentAmount || '0').currency(2),
        currentBalanceAmount: formatCommon(
          cccsData?.currentBalance || '0'
        ).currency(2)
      });

      setFieldValidationRules('paymentAmount', [
        {
          ruleName: 'GreaterThanOrEqual',
          errorMsg: paymentAmountErrMsg,
          ruleOptions: {
            otherValue: cccsData?.paymentAmount
          }
        },
        {
          ruleName: 'CompareNumber',
          errorMsg: paymentAmountErrMsg,
          ruleOptions: {
            operator: 'LessEqual',
            isReturnFieldCompare: true,
            fieldType: 'currency',
            format: 'c2',
            fieldToCompare: 'cccs_currentBalance'
          }
        }
      ]);

      const { endDate } = prepareDate(cccsData);

      updateEndDateByReAgePaymentPeriod(
        ref?.current?.props?.onFind('endDate')?.props,
        cccsData?.reagePaymentPeriod,
        false,
        endDate
      );

      if (isReAgePaymentPeriodNone(cccsData?.reagePaymentPeriod)) {
        setFieldValue('reagePaymentPeriod', null);
        setFieldDisabled('endDate', false);
      } else {
        setFieldDisabled('endDate', true);
      }

      // handle logic enable radio option
      const action = cccsData?.action || CCCS_ACTIONS.NEW_PROPOSAL;
      const actionsRadio = ref.current.props.onFind('action');
      actionsRadio?.props?.props?.setOthers((prev: MagicKeyValue) => ({
        ...prev,
        radios: prev.radios.map((item: IRadio) => {
          const isOptionDisabled = !ENABLE_RADIO_RULES[action]?.includes?.(
            item.value as CCCS_ACTIONS
          );
          return {
            ...item,
            disabled: isOptionDisabled
          };
        })
      }));

      if (cccsData?.action === CCCS_ACTIONS.ACCEPT_PROPOSAL) {
        setFieldDisabled('nonDelinquentMpdAmount', true);
        setFieldDisabled('reagePaymentPeriod', true);
        setFieldDisabled('endDate', true);
        setFieldDisabled('paymentAmount', true);
      } else {
        setFieldDisabled('nonDelinquentMpdAmount', false);
      }
    });

    return () => clearImmediate(setImmediateInstance);
  }, [cccsData, setFieldDisabled, setFieldValidationRules, t, setFieldValue]);

  // set error for NFPA after load data
  useEffect(() => {
    const immediateId = setImmediate(() => {
      if (!ref?.current || !cccsData) return;

      const field: Field<ExtraFieldProps> =
        ref.current?.props?.onFind('paymentAmount');

      setStatusNFPAInit(field, {
        newFixedPaymentAmount: cccsData?.paymentAmount,
        minimumNewFixedPaymentAmount: cccsData?.currentBalance
      });
    });

    return () => {
      clearImmediate(immediateId);
    };
  }, [cccsData]);

  const value = useMemo(() => {
    const { startDate, endDate } = prepareDate(cccsData);

    return {
      ...cccsData,
      startDate,
      endDate
    };
  }, [cccsData]);

  const setFieldRequired = useCallback((name: string, required: boolean) => {
    const targetFieldShadow = ref?.current?.props?.onFind(name);
    targetFieldShadow?.props?.props?.setRequired(required);
    targetFieldShadow?.props?.props?.setVisible(required);
    const targetActionsField = actionRef?.current?.props?.onFind(name);
    targetActionsField?.props?.props?.setRequired(required);
    targetActionsField?.props?.props?.setVisible(required);
  }, []);

  const handleActionsFieldsRequired = useCallback(
    (values: MagicKeyValue) => {
      if (values?.action === CCCS_ACTIONS.DECLINE_PROPOSAL) {
        setFieldRequired('declineProposalOption', true);
      } else {
        setFieldValue('declineProposalOption', undefined);
        setFieldRequired('declineProposalOption', false);
      }

      if (values?.action === CCCS_ACTIONS.DISCONTINUE_PLAN) {
        setFieldRequired('discontinuePlanOption', true);
      } else {
        setFieldValue('discontinuePlanOption', undefined);
        setFieldRequired('discontinuePlanOption', false);
      }

      if (
        values?.action === CCCS_ACTIONS.DISCONTINUE_PLAN ||
        values?.action === CCCS_ACTIONS.DECLINE_PROPOSAL
      ) {
        setShowActionsOptions(true);
        process.nextTick(() => {
          reasonRef.current &&
            reasonRef.current.scrollIntoView({
              behavior: 'smooth'
            });
        });
      } else {
        setShowActionsOptions(false);
      }
      if (cccsData?.action === CCCS_ACTIONS.PENDING_PROPOSAL) {
        dispatch(
          collectionFormActions.changeDisabledOk({
            storeId,
            disabledOk: values?.action === CCCS_ACTIONS.PENDING_PROPOSAL
          })
        );
      }
    },
    [setFieldRequired, setFieldValue, dispatch, storeId, cccsData]
  );

  const handleOptionsFieldsRequired = useCallback(
    (values: MagicKeyValue) => {
      setFieldValue('declineProposalOption', values.declineProposalOption);

      setFieldValue('discontinuePlanOption', values.discontinuePlanOption);

      setFieldValue(
        'actionDeclineProposalText',
        values.actionDeclineProposalText
      );

      setFieldValue(
        'actionDiscontinuePlanText',
        values.actionDiscontinuePlanText
      );

      if (values?.declineProposalOption?.value === 'other') {
        setFieldRequired('actionDeclineProposalText', true);
      } else {
        setFieldRequired('actionDeclineProposalText', false);
      }

      if (values?.discontinuePlanOption?.value === 'other') {
        setFieldRequired('actionDiscontinuePlanText', true);
      } else {
        setFieldRequired('actionDiscontinuePlanText', false);
      }
    },
    [setFieldValue, setFieldRequired]
  );

  const handleShowWarningEndDateChanges = useCallback(
    (options: {
      previousReAgePaymentPeriodValue: RefDataValue | null;
      currentReAgePaymentPeriodValue: RefDataValue | null;
      previousEndDateValue: Date;
      currentEndDateValue: Date;
      isDirtyForm: boolean;
    }) => {
      const {
        previousReAgePaymentPeriodValue,
        currentReAgePaymentPeriodValue,
        previousEndDateValue,
        currentEndDateValue,
        isDirtyForm
      } = options;

      if (
        !isDirtyForm ||
        !isValidDate(previousEndDateValue) ||
        !isValidDate(currentEndDateValue)
      )
        return;

      if (
        isReAgePaymentPeriodChangeFromNoneToOtherValue(
          previousReAgePaymentPeriodValue,
          currentReAgePaymentPeriodValue
        )
      ) {
        if (isDiffEndDate(previousEndDateValue, currentEndDateValue)) {
          dispatch(
            actionsToast.addToast({
              show: true,
              type: 'attention',
              message: I18N_CCCS.WARNING_CHANGING_END_DATE_MSG
            })
          );
        }
      }
    },
    [dispatch]
  );

  const handleChangeStartDate = useCallback(
    (options: {
      previousActionValue: CCCS_ACTIONS;
      currentActionValue: CCCS_ACTIONS;
      currentReAgePaymentPeriodValue: RefDataValue | null;
      isDirtyForm: boolean;
    }) => {
      const {
        previousActionValue,
        currentActionValue,
        isDirtyForm,
        currentReAgePaymentPeriodValue
      } = options;

      if (!isDirtyForm) return;
      if (isEqual(previousActionValue, currentActionValue)) return;
      if (cccsData?.action !== CCCS_ACTIONS.PENDING_PROPOSAL) return;

      const { startDate, endDate } = prepareDate(
        cccsData,
        currentActionValue,
        currentReAgePaymentPeriodValue
      );

      setFieldValue('startDate', startDate);
      setFieldValue('endDate', endDate);
    },
    [cccsData, setFieldValue]
  );

  const handleOnChangeView = useCallback(
    (values: MagicKeyValue, _, props, previousValues) => {
      const {
        reagePaymentPeriod: previousReAgePaymentPeriodValue,
        endDate: previousEndDateValue,
        action: previousActionValue
      } = previousValues;
      const {
        reagePaymentPeriod: currentReAgePaymentPeriodValue,
        endDate: currentEndDateValue,
        action: currentActionValue
      } = values;

      const { dirty: isDirtyForm } = props;

      handleChangeStartDate({
        previousActionValue,
        currentActionValue,
        currentReAgePaymentPeriodValue,
        isDirtyForm
      });

      handleShowWarningEndDateChanges({
        previousReAgePaymentPeriodValue,
        currentReAgePaymentPeriodValue,
        previousEndDateValue,
        currentEndDateValue,
        isDirtyForm
      });

      handleActionsFieldsRequired(values);
    },
    [
      handleActionsFieldsRequired,
      handleShowWarningEndDateChanges,
      handleChangeStartDate
    ]
  );

  const handleOnActionChangeView = useCallback(
    (values: MagicKeyValue, _, __, ___) => {
      handleOptionsFieldsRequired(values);
    },
    [handleOptionsFieldsRequired]
  );

  return (
    <>
      <div className="px-24 pb-24 mx-auto max-width-lg">
        <View
          id={formKy}
          formKey={formKy}
          descriptor="enterCCCSView"
          value={value}
          ref={ref}
          onChange={handleOnChangeView}
        />
      </div>

      <div
        ref={reasonRef}
        className={`mt-n8 pb-24 pt-8 border-top bg-light-l20 ${
          showActionsOptions ? '' : 'd-none'
        }`}
      >
        <div className="px-24 mx-auto max-width-lg">
          <View
            id={formActionCCCSKy}
            formKey={formActionCCCSKy}
            descriptor="selectReasonCCCSView"
            value={value}
            ref={actionRef}
            onChange={handleOnActionChangeView}
          />
        </div>
      </div>
    </>
  );
};

export default ViewUpdateCCCS;
