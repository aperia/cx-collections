import React from 'react';
import ViewUpdateCCCS from './index';
import { renderMockStoreId, storeId, mockActionCreator } from 'app/test-utils';
import * as useSubmitHook from 'pages/CollectionForm/hooks/useSubmit';
import { cccsActions } from '../_redux/reducers';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { CCCS_ACTIONS } from '../constants';
import { screen } from '@testing-library/dom';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import {
  HARDSHIP_COMPLETE,
  HARDSHIP_IN_PROGRESS
} from 'pages/HardShip/constants';

jest.mock('./Badge', () => () => <div>BadgeCCCS</div>);
jest.mock('../CompanyList', () => () => <div>CompanyList</div>);
jest.mock('./View', () => () => <div>ViewUpdateCCCS</div>);

const companyData = {
  checkedCompany: {
    addressLineOne: '3350 Manley',
    city: 'Wyoming',
    companyName: '5TH VENTURE CATALYST',
    contact: 'Thomas Cesar',
    fax: '5355260021',
    id: 1,
    lastUpdated: '3-10-2021',
    phone: '5355260021',
    state: 'MI',
    zip: '49519'
  },
  companyList: [
    {
      addressLineOne: '3350 Manley',
      city: 'Wyoming',
      companyName: '5TH VENTURE CATALYST',
      contact: 'Thomas Cesar',
      fax: '5355260021',
      id: 1,
      lastUpdated: '3-10-2021',
      phone: '5355260021',
      state: 'MI',
      zip: '49519'
    }
  ]
};

const cccsData = {
  action: 'Pending Proposal',
  address: '91715 SunnySide Drive, Hampton, VA 23667',
  addressLineOne: '2181 Pelham Pkwy',
  balancePercentage: 50,
  checkedList: [4],
  city: 'Hampton',
  company: 'MONEY NETWORK',
  companyName: 'RELIANCE LIFE INSURANCE',
  contact: 'Thomas Cesar',
  cycleToDatePaymentAmount: 200,
  dateReceived: null,
  fax: '5355260021',
  id: 4,
  lastUpdated: '3-10-2021',
  minimumAmount: 100,
  nonDelinquentMpdAmount: 100,
  numberOfCreditors: '23',
  paymentAmount: 100,
  phone: '5355260021',
  reagePaymentPeriod: { value: 3, description: '3 Months' },
  startDate: '2021-03-19T03:12:05.398Z',
  state: 'VA',
  totalDebt: '0.00',
  zip: '23666'
};

const initialState: Partial<RootState> = {
  company: { [storeId]: { checkedCompany: { ...companyData } } },
  collectionForm: {
    [storeId]: {
      collectionCurrentData: { ...cccsData, action: '' }
    }
  },
  accountDetail: { [storeId]: { data: { monetary: {} } } },
  hardship: {
    [storeId]: {
      data: { processStatus: 'status' }
    }
  }
};

const cccsActionsSpy = mockActionCreator(cccsActions);
const collectionFormActionsSpy = mockActionCreator(collectionFormActions);

beforeEach(() => {
  collectionFormActionsSpy('setDirtyForm');
  cccsActionsSpy('getCCCSDetail');
});

describe('Test Update CCCS', () => {
  it('Test submit', () => {
    jest.spyOn(useSubmitHook, 'useSubmit').mockImplementation((props: any) => {
      props();
    });
    const mockAction = cccsActionsSpy('triggerUpdateCCCS');
    renderMockStoreId(<ViewUpdateCCCS />, {
      initialState
    });
    expect(mockAction).toBeCalledWith({
      storeId
    });
  });

  it('Test confirm when hardship status is COMPLETE', () => {
    jest.spyOn(useSubmitHook, 'useSubmit').mockImplementation((props: any) => {
      props();
    });
    const mockAction = cccsActionsSpy('toggleConfirmModal');
    renderMockStoreId(<ViewUpdateCCCS />, {
      initialState: {
        ...initialState,
        hardship: { [storeId]: { data: { processStatus: HARDSHIP_COMPLETE } } },
        form: {
          [`${storeId}-enterCCCS`]: {
            registeredFields: [],
            values: {
              action: CCCS_ACTIONS.ACCEPT_PROPOSAL
            }
          }
        }
      }
    });
    expect(mockAction).toBeCalledWith({
      storeId,
      opened: true
    });
  });

  it('Test confirm when hardship status is IN_PROGRESS', () => {
    jest.spyOn(useSubmitHook, 'useSubmit').mockImplementation((props: any) => {
      props();
    });
    const mockAction = cccsActionsSpy('toggleConfirmModal');
    renderMockStoreId(<ViewUpdateCCCS />, {
      initialState: {
        ...initialState,
        hardship: {
          [storeId]: { data: { processStatus: HARDSHIP_IN_PROGRESS } }
        },
        form: {
          [`${storeId}-enterCCCS`]: {
            registeredFields: [],
            values: {
              action: CCCS_ACTIONS.ACCEPT_PROPOSAL
            }
          }
        }
      }
    });
    expect(mockAction).toBeCalledWith({
      storeId,
      opened: true
    });
  });

  it('Test close modal', () => {
    const mockAction = cccsActionsSpy('toggleConfirmModal');
    renderMockStoreId(<ViewUpdateCCCS />, {
      initialState: {
        ...initialState,
        cccs: { [storeId]: { showConfirmModal: true } }
      }
    });

    screen.getByText(I18N_COMMON_TEXT.CANCEL).click();
    expect(mockAction).toBeCalledWith({
      storeId,
      opened: false
    });
  });
});
