import { I18N_COLLECTION_FORM, I18N_COMMON_TEXT } from 'app/constants/i18n';
// hooks
import {
  useAccountDetail,
  useIsDirtyForm,
  useStoreIdSelector
} from 'app/hooks';
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
// classnames
import classNames from 'classnames';
// components
import CompanyList from 'pages/CCCS/CompanyList';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { useSaveViewName } from 'pages/CollectionForm/hooks/useSaveViewName';
import { useSubmit } from 'pages/CollectionForm/hooks/useSubmit';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import {
  selectIsHavingValidHardshipData,
  selectMemoSectionCollapsed
} from 'pages/CollectionForm/_redux/selectors';
import React, { useCallback, useEffect } from 'react';
// redux
import { useDispatch, useSelector } from 'react-redux';
import { getFormValues } from 'redux-form';
import { CCCS_ACTIONS } from '../constants';
import { ICurrentResultCCCS } from '../types';
import { cccsActions } from '../_redux/reducers';
import { selectIsLoading, selectIsOpenConfirmModal } from '../_redux/selectors';
import BadgeCCCS from './Badge';
import ViewUpdateCCCS from './View';

export interface UpdateCCCSProps {}

const UpdateCCCS: React.FC<UpdateCCCSProps> = () => {
  const { storeId } = useAccountDetail();

  const { t } = useTranslation();

  const dispatch = useDispatch();

  const viewName = `${storeId}-enterCCCS`;
  const testId = 'collectionForm_CCCS-form';

  const isMemoSectionCollapsed = useStoreIdSelector<MagicKeyValue>(
    selectMemoSectionCollapsed
  );

  const isHavingValidHardshipData = useStoreIdSelector(
    selectIsHavingValidHardshipData
  );

  const isDirty = useIsDirtyForm([viewName]);

  const isLoading = useStoreIdSelector<boolean>(selectIsLoading);

  const isOpenModal = useStoreIdSelector<boolean>(selectIsOpenConfirmModal);

  const data = useSelector(
    getFormValues(`${storeId}-enterCCCS`)
  ) as ICurrentResultCCCS;

  const handleSubmit = useCallback(() => {
    dispatch(
      cccsActions.triggerUpdateCCCS({
        storeId
      })
    );
  }, [dispatch, storeId]);

  const handleConfirm = useCallback(() => {
    dispatch(
      cccsActions.toggleConfirmModal({
        storeId,
        opened: true
      })
    );
  }, [dispatch, storeId]);

  const handleCloseModal = () => {
    dispatch(
      cccsActions.toggleConfirmModal({
        storeId,
        opened: false
      })
    );
  };

  const shouldShowConfirm =
    isHavingValidHardshipData && data?.action === CCCS_ACTIONS.ACCEPT_PROPOSAL;

  useSubmit(shouldShowConfirm ? handleConfirm : handleSubmit);
  useSaveViewName([viewName]);

  // set dirty
  useEffect(() => {
    dispatch(
      collectionFormActions.setDirtyForm({
        storeId,
        callResult: CALL_RESULT_CODE.CCCS,
        value: isDirty
      })
    );

    dispatch(
      collectionFormActions.changeDisabledOk({
        storeId,
        disabledOk: !isDirty
      })
    );
  }, [dispatch, isDirty, storeId]);

  useEffect(() => {
    cccsActions.getCCCSDetail({
      storeId
    });
  }, [dispatch, storeId]);

  const memoExtend = !isMemoSectionCollapsed;

  return (
    <>
      <div
        className={classNames({
          'memos-extend': memoExtend,
          loading: isLoading
        })}
      >
        <div className="px-24 pb-24 mx-auto max-width-lg">
          <BadgeCCCS />
          <div className="mt-24">
            <CompanyList />
          </div>
        </div>
        <ViewUpdateCCCS />
      </div>
      <Modal
        show={isOpenModal}
        loading={isLoading}
        xs
        dataTestId={genAmtId(testId, 'confirm-submit-modal', '')}
      >
        <ModalHeader
          border
          closeButton
          onHide={handleCloseModal}
          dataTestId={genAmtId(testId, 'confirm-submit-modal_header', '')}
        >
          <ModalTitle
            dataTestId={genAmtId(
              testId,
              'confirm-submit-modal_header_title',
              ''
            )}
          >
            {t(I18N_COLLECTION_FORM.CONFIRM_SUBMIT_CALL_RESULT)}
          </ModalTitle>
        </ModalHeader>
        <ModalBody>
          <p data-testid={genAmtId(testId, 'confirm-submit-modal_body', '')}>
            {t(I18N_COLLECTION_FORM.CONFIRM_SUBMIT_CCCS_BODY)}
          </p>
        </ModalBody>
        <ModalFooter
          cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
          okButtonText={t(I18N_COMMON_TEXT.SUBMIT)}
          onCancel={handleCloseModal}
          onOk={handleSubmit}
          dataTestId={genAmtId(testId, 'confirm-submit-modal_footer', '')}
        />
      </Modal>
    </>
  );
};

export default UpdateCCCS;
