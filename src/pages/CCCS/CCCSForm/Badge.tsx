import React from 'react';

// components
import { Badge, Icon } from 'app/_libraries/_dls/components';

// constants
import {
  CCCS_ACTIONS,
  CCCS_STATUS_COLOR,
  CCCS_STATUS_TEXT
} from '../constants';

// hooks
import { useStoreIdSelector } from 'app/hooks';

// selectors
import { selectCCCSData } from '../_redux/selectors';

// types
import { ICurrentResultCCCS } from '../types';
import { useTranslation } from 'react-i18next';
import { genAmtId } from 'app/_libraries/_dls/utils';

const BadgeCCCS: React.FC = () => {
  const { t } = useTranslation();
  const testId = 'collectionForm_CCCS-form';

  const statusCCCS = useStoreIdSelector<ICurrentResultCCCS>(selectCCCSData);

  const statusCCCSAction = statusCCCS?.action || CCCS_ACTIONS.NEW_PROPOSAL;
  const badgeColor = CCCS_STATUS_COLOR[statusCCCSAction];
  const badgeText = CCCS_STATUS_TEXT[statusCCCSAction];

  return (
    <div className="row mt-n24" data-testid={genAmtId(testId, 'badge', '')}>
      <div className="col-extend-xxl-3 col-xl-3 col-extend-xl-4 col-lg-4 col-extend-lg-6 col-extend-md-12 col-md-6">
        <div className="p-8 bg-light-l16 d-flex align-items-center rounded-lg">
          <div
            className="bubble-icon"
            data-testid={genAmtId(testId, 'badge-icon', '')}
          >
            <Icon name="megaphone" />
          </div>
          <p
            className="mx-12"
            data-testid={genAmtId(testId, 'badge-label', '')}
          >
            CCCS
          </p>
          <div className="pr-8 ml-auto">
            <Badge
              color={badgeColor}
              dataTestId={genAmtId(testId, 'badge-text', '')}
            >
              {t(badgeText)}
            </Badge>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BadgeCCCS;
