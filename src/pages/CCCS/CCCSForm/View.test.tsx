import React from 'react';
import {
  queryById,
  renderMockStore,
  storeId,
  accEValue,
  mockActionCreator
} from 'app/test-utils';
import { fireEvent, screen } from '@testing-library/react';
import ViewUpdateCCCS from './View';
import { AccountDetailProvider } from 'app/hooks';

import { storeIdViewNull } from 'app/test-utils/mocks/MockView';
import { ICurrentResultCCCS } from '../types';
import { CCCS_ACTIONS, I18N_CCCS } from '../constants';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import * as FormatTimeModule from 'app/helpers/formatTime';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

HTMLElement.prototype.scrollIntoView = jest.fn();

const data: ICurrentResultCCCS = {
  clientId: storeId,
  clientSince: 'clientSince',
  sentVia: 'sentVia',
  action: CCCS_ACTIONS.ACCEPT_PROPOSAL,
  address: '91715 SunnySide Drive, Hampton, VA 23667',
  balancePercentage: 50,
  checkedList: [4],
  company: 'MONEY NETWORK',
  contact: 'Thomas Cesar',
  cycleToDatePaymentAmount: 200,
  dateReceived: 'dateReceived',
  fax: '5355260021',
  minimumAmount: 100,
  nonDelinquentMpdAmount: 100,
  numberOfCreditors: '23',
  paymentAmount: 100,
  phone: '5355260021',
  reagePaymentPeriod: { value: '3', description: '3 Months' },
  startDate: '2021-03-19T03:12:05.398Z',
  endDate: '2021-03-19T03:12:05.398Z',
  totalDebt: '10.00'
};

const otherData = { ...data, action: undefined };

const declineData = { ...data, action: CCCS_ACTIONS.DECLINE_PROPOSAL };

const discontinueData = { ...data, action: CCCS_ACTIONS.DISCONTINUE_PLAN };

const pendingData = {
  ...data,
  action: CCCS_ACTIONS.PENDING_PROPOSAL,
  reagePaymentPeriod: { value: '0', description: '3 Months' }
};

const dateData = {
  ...data,
  action: CCCS_ACTIONS.DISCONTINUE_PLAN,
  startDate: '',
  endDate: '',
  nonDelinquentMpdAmount: '',
  paymentAmount: ''
};

const initState: Partial<RootState> = {
  cccs: {
    [storeId]: {
      data
    }
  },
  accountDetail: {
    [storeId]: { data: { monetary: {} } },
    [storeIdViewNull]: { data: { monetary: {} } }
  },
  form: {
    [`${storeId}-enterCCCS`]: {
      values: {
        action: '1'
      },
      registeredFields: []
    },
    [`${storeId}-selectReasonCCCSView`]: {
      values: {
        declineProposalOption: null,
        actionDeclineProposalText: null,
        discontinuePlanOption: null,
        actionDiscontinuePlanText: null
      },
      registeredFields: []
    }
  }
};

const otherState: Partial<RootState> = {
  cccs: {
    [storeId]: {
      data: otherData
    }
  },
  accountDetail: { [storeId]: { data: { monetary: {} } } }
};

const declineState: Partial<RootState> = {
  cccs: {
    [storeId]: {
      data: declineData
    }
  },
  accountDetail: { [storeId]: { data: { monetary: {} } } }
};

const discontinueState: Partial<RootState> = {
  cccs: {
    [storeId]: {
      data: discontinueData
    }
  },
  accountDetail: { [storeId]: { data: { monetary: {} } } }
};

const pendingState: Partial<RootState> = {
  cccs: {
    [storeId]: {
      data: pendingData
    }
  },
  accountDetail: { [storeId]: { data: { monetary: {} } } }
};

const emptyDateState: Partial<RootState> = {
  cccs: {
    [storeId]: {
      data: dateData
    }
  },
  accountDetail: { [storeId]: { data: { monetary: {} } } }
};

const generateWrapper = (id = storeId, initialState = initState) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId: id, accEValue }}>
      <ViewUpdateCCCS />
    </AccountDetailProvider>,
    { initialState }
  );
};

const spyCollectionForm = mockActionCreator(collectionFormActions);

describe('ViewUpdateCCCS component', () => {
  it('should not render', () => {
    jest.useFakeTimers();
    const { wrapper } = generateWrapper(storeIdViewNull);
    jest.runAllTimers();
    const ele = queryById(wrapper.container, `${storeId}-enterCCCS`);
    expect(ele).not.toBeInTheDocument();
  });
  it('should render Accept Proposal Action', () => {
    jest.useFakeTimers();
    generateWrapper();
    jest.runAllTimers();
    expect(screen.getByTestId('enterCCCSView')).toBeInTheDocument();
  });
  it('should render other Action', () => {
    jest.useFakeTimers();
    generateWrapper(storeId, otherState);
    jest.runAllTimers();
    expect(screen.getByTestId('enterCCCSView')).toBeInTheDocument();
  });
  it('should run setOthers', () => {
    jest.useFakeTimers();
    generateWrapper(storeId, otherState);
    jest.runAllTimers();
    expect(screen.getByTestId('enterCCCSView')).toBeInTheDocument();
  });
  it('should render Decline Proposal when trigger checbox DECLINE, trigger some form event enterCCCS, selectReasonCCCSView', () => {
    jest.useFakeTimers();
    generateWrapper(storeId, declineState);
    jest.runAllTimers();
    expect(screen.getByTestId('enterCCCSView')).toBeInTheDocument();

    const input = screen.getByTestId('enterCCCSView_onChange');

    fireEvent.change(input!, {
      target: {
        value: 'some value',
        values: { ...declineData, action: CCCS_ACTIONS.DISCONTINUE_PLAN },
        previousValues: {
          ...declineData,
          action: CCCS_ACTIONS.DISCONTINUE_PLAN
        },
        viewProps: { dirty: true }
      }
    });
    jest.runAllTimers();

    const input2 = screen.getByTestId('selectReasonCCCSView_onChange');

    fireEvent.change(input2!, {
      target: {
        value: 'some value',
        values: {
          discontinuePlanOption: {
            value: 'other',
            description: 'Other'
          }
        }
      }
    });

    expect(
      screen.getByTestId('selectReasonCCCSView_onChange')
    ).toBeInTheDocument();
  });
  it('should render Decline Proposal when trigger checbox DISCONTINUE, trigger some form event enterCCCS, selectReasonCCCSView', () => {
    jest.useFakeTimers();
    generateWrapper(storeId, discontinueState);
    jest.runAllTimers();
    expect(screen.getByTestId('enterCCCSView')).toBeInTheDocument();

    const input = screen.getByTestId('enterCCCSView_onChange');

    fireEvent.change(input!, {
      target: {
        value: 'some value',
        values: { ...declineData, action: CCCS_ACTIONS.DECLINE_PROPOSAL },
        previousValues: {
          ...declineData,
          reagePaymentPeriod: { value: '0', description: '3 Months' },
          endDate: '2020-03-19T03:12:05.398Z',
          action: CCCS_ACTIONS.DECLINE_PROPOSAL
        },
        viewProps: { dirty: true }
      }
    });

    const input2 = screen.getByTestId('selectReasonCCCSView_onChange');

    fireEvent.change(input2!, {
      target: {
        value: 'some value',
        values: {
          declineProposalOption: {
            value: 'other',
            description: 'Other'
          }
        },
        previousValues: {
          declineProposalOption: {
            value: 'other',
            description: 'Other'
          }
        },
        viewProps: { dirty: true }
      }
    });

    fireEvent.change(input!, {
      target: {
        value: 'some value 2',
        values: { ...declineData, action: CCCS_ACTIONS.PENDING_PROPOSAL },
        previousValues: {
          ...declineData,
          action: CCCS_ACTIONS.PENDING_PROPOSAL
        },
        viewProps: { dirty: true }
      }
    });

    expect(
      screen.getByTestId('selectReasonCCCSView_onChange')
    ).toBeInTheDocument();
  });
  it('should render Decline Proposal when trigger checbox PENDING, trigger some form event enterCCCS, selectReasonCCCSView', () => {
    const mockAction = spyCollectionForm('changeDisabledOk');

    jest.useFakeTimers();
    generateWrapper(storeId, pendingState);
    jest.runAllTimers();
    expect(screen.getByTestId('enterCCCSView')).toBeInTheDocument();

    const input = screen.getByTestId('enterCCCSView_onChange');

    fireEvent.change(input!, {
      target: {
        value: 'some value',
        values: {
          ...declineData,
          reagePaymentPeriod: { value: '0', description: '3 Months' },
          action: CCCS_ACTIONS.PENDING_PROPOSAL
        },
        previousValues: {
          ...declineData,
          action: CCCS_ACTIONS.PENDING_PROPOSAL
        },
        viewProps: { dirty: true }
      }
    });

    expect(
      screen.getByTestId('selectReasonCCCSView_onChange')
    ).toBeInTheDocument();
    expect(mockAction).toBeCalledWith({ storeId, disabledOk: true });
  });

  it('should render with empty date', () => {
    jest.useFakeTimers();
    generateWrapper(storeId, emptyDateState);
    jest.runAllTimers();
    expect(screen.getByTestId('enterCCCSView')).toBeInTheDocument();

    const input = screen.getByTestId('enterCCCSView_onChange');

    fireEvent.change(input!, {
      target: {
        value: 'some value',
        values: {
          ...declineData,
          reagePaymentPeriod: { value: '0', description: '3 Months' },
          action: CCCS_ACTIONS.PENDING_PROPOSAL
        },
        previousValues: {
          ...declineData,
          action: CCCS_ACTIONS.PENDING_PROPOSAL
        },
        viewProps: { dirty: false }
      }
    });

    expect(
      screen.getByTestId('selectReasonCCCSView_onChange')
    ).toBeInTheDocument();
  });

  it('should warning end date has been changed if user changes reagePaymentPeriod from none to other value', () => {
    const actionsToastSpy = mockActionCreator(actionsToast);
    const addToastSpy = actionsToastSpy('addToast');

    spyCollectionForm('changeDisabledOk');

    const startDate = new Date();

    const previousEndDate = new Date(
      new Date().setMonth(new Date().getMonth() + 1)
    );

    const currentEndDate = new Date(
      new Date().setMonth(new Date().getMonth() + 3)
    );

    jest.useFakeTimers();
    generateWrapper(storeId, pendingState);
    jest.runAllTimers();
    expect(screen.getByTestId('enterCCCSView')).toBeInTheDocument();

    const input = screen.getByTestId('enterCCCSView_onChange');

    fireEvent.change(input!, {
      target: {
        value: 'some value',
        values: {
          ...data,
          reagePaymentPeriod: { value: '3', description: '3 Months' },
          startDate: startDate,
          endDate: currentEndDate
        },
        previousValues: {
          ...data,
          reagePaymentPeriod: { value: '0', description: 'None' },
          startDate: startDate,
          endDate: previousEndDate
        },
        viewProps: { dirty: true }
      }
    });

    expect(addToastSpy).toBeCalledWith({
      show: true,
      type: 'attention',
      message: I18N_CCCS.WARNING_CHANGING_END_DATE_MSG
    });
  });

  it('should not display warning if changing end date is same previous', () => {
    const actionsToastSpy = mockActionCreator(actionsToast);
    const addToastSpy = actionsToastSpy('addToast');

    spyCollectionForm('changeDisabledOk');

    const startDate = new Date();

    const previousEndDate = new Date(
      new Date().setMonth(new Date().getMonth() + 1)
    );

    jest.useFakeTimers();
    generateWrapper(storeId, pendingState);
    jest.runAllTimers();
    expect(screen.getByTestId('enterCCCSView')).toBeInTheDocument();

    const input = screen.getByTestId('enterCCCSView_onChange');

    fireEvent.change(input!, {
      target: {
        value: 'some value',
        values: {
          ...data,
          reagePaymentPeriod: { value: '3', description: '3 Months' },
          startDate: startDate,
          endDate: previousEndDate
        },
        previousValues: {
          ...data,
          reagePaymentPeriod: { value: '0', description: 'None' },
          startDate: startDate,
          endDate: previousEndDate
        },
        viewProps: { dirty: true }
      }
    });

    expect(addToastSpy).not.toBeCalled();
  });

  it('should use current date as default if startDate, endDate are invalid dates', () => {
    spyCollectionForm('changeDisabledOk');
    const formatTimeDefaultMock = jest.spyOn(
      FormatTimeModule,
      'formatTimeDefault'
    );

    formatTimeDefaultMock.mockReturnValue(undefined);

    jest.useFakeTimers();
    generateWrapper(storeId);
    jest.runAllTimers();
    expect(screen.getByTestId('enterCCCSView')).toBeInTheDocument();
  });

  it(`should not call handleChangeStartDate if update action from ${CCCS_ACTIONS.PENDING_PROPOSAL} to ${CCCS_ACTIONS.ACCEPT_PROPOSAL}`, () => {
    spyCollectionForm('changeDisabledOk');

    jest.useFakeTimers();
    generateWrapper(storeId, pendingState);
    jest.runAllTimers();
    expect(screen.getByTestId('enterCCCSView')).toBeInTheDocument();

    const input = screen.getByTestId('enterCCCSView_onChange');

    fireEvent.change(input!, {
      target: {
        value: 'some value',
        values: {
          ...data,
          action: CCCS_ACTIONS.PENDING_PROPOSAL
        },
        previousValues: {
          ...data,
          action: CCCS_ACTIONS.ACCEPT_PROPOSAL
        },
        viewProps: { dirty: true }
      }
    });
  });

  it('should not call handleChangeStartDate if cccs action is undefined', () => {
    spyCollectionForm('changeDisabledOk');

    jest.useFakeTimers();
    generateWrapper(storeId, otherState);
    jest.runAllTimers();
    expect(screen.getByTestId('enterCCCSView')).toBeInTheDocument();

    const input = screen.getByTestId('enterCCCSView_onChange');

    fireEvent.change(input!, {
      target: {
        value: 'some value',
        values: {
          ...otherData
        },
        previousValues: {
          ...data,
          action: CCCS_ACTIONS.PENDING_PROPOSAL
        },
        viewProps: { dirty: true }
      }
    });
  });
});
