import React from 'react';
import { queryById, renderMockStore, storeId, accEValue } from 'app/test-utils';
import BadgeCCCS from './Badge';
import { AccountDetailProvider } from 'app/hooks';

import { storeIdViewNull } from 'app/test-utils/mocks/MockView';

const initState: Partial<RootState> = {
  cccs: {
    [storeId]: {
      data: {}
    }
  }
};

const generateWrapper = (id = storeId, initialState = initState) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId: id, accEValue }}>
      <BadgeCCCS />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('BadgeCCCS component', () => {
  it('should render', () => {
    const { wrapper } = generateWrapper(storeIdViewNull);

    const ele = queryById(wrapper.container, `${storeId}-enterCCCS`);
    expect(ele).not.toBeInTheDocument();
  });
});
