import { isReAgePaymentPeriodNone } from 'app/components/_dof_controls/custom-functions/onChangeReagePaymentPeriod';
import { FormatTime } from 'app/constants/enums';
import { formatTimeDefault, joinAddress } from 'app/helpers';
import { SortType } from 'app/_libraries/_dls/components';
import isEqual from 'lodash.isequal';
import { CCCS_ACTIONS } from 'pages/CCCS/constants';
import {
  capitalizeWord,
  joinStateAndPostalCode
} from './../../app/helpers/commons';
import { Company } from './CompanyList/types';
import { ICurrentResultCCCS } from './types';

export const sortGridData = (
  gridData: MagicKeyValue[],
  sortBy: SortType | undefined
) => {
  if (!sortBy?.order) return gridData;

  const result = [...gridData];

  return result.sort((a, b) => {
    const valueToCompareA = a[sortBy.id];
    const valueToCompareB = b[sortBy.id];

    if (sortBy.order === 'desc') {
      return valueToCompareB > valueToCompareA ? 1 : -1;
    } else return valueToCompareA > valueToCompareB ? 1 : -1;
  });
};

export const prepareCompanyData = (data: ICurrentResultCCCS) => {
  return {
    companyName: data?.companyName,
    phone: data?.phone,
    fax: data?.fax,
    contact: data?.contact || '',
    address: joinAddress(
      data?.addressLineOne,
      data?.addressLineTwo,
      data?.city,
      joinStateAndPostalCode(data?.state, data?.zip)
    ),
    reagePaymentPeriod: data?.reagePaymentPeriod?.description,
    sentVia: data?.sentVia?.description
  };
};

export const filterCompany = (data: Company[], searchValue: string) => {
  const isIncludedSearchValue = (str?: string) =>
    str?.toLowerCase().includes(searchValue?.toLowerCase() || '');

  return data.filter(
    company =>
      isIncludedSearchValue(company?.companyName) ||
      isIncludedSearchValue(company?.fax) ||
      isIncludedSearchValue(company?.phone) ||
      isIncludedSearchValue(company?.addressLineOne) ||
      isIncludedSearchValue(company?.addressLineTwo) ||
      isIncludedSearchValue(company?.zip) ||
      isIncludedSearchValue(company?.city) ||
      isIncludedSearchValue(company?.state?.value) ||
      isIncludedSearchValue(company?.contact)
  );
};

export const prepareUpdateDataCCCS = (updateData: MagicKeyValue) => {
  const result: MagicKeyValue = {
    ...updateData,
    state: updateData.state?.value,
    reagePaymentPeriod: updateData.reagePaymentPeriod?.value?.toString?.(),
    sentVia: updateData.sentVia?.value,
    dateReceived: formatTimeDefault(
      updateData?.dateReceived,
      FormatTime.DefaultPostDate
    ),
    clientSince: formatTimeDefault(
      updateData?.clientSince,
      FormatTime.DefaultPostDate
    ),
    startDate: formatTimeDefault(
      updateData?.startDate,
      FormatTime.DefaultPostDate
    ),
    endDate: formatTimeDefault(updateData?.endDate, FormatTime.DefaultPostDate),
    cccsStatusDeclineReason:
      updateData?.action === CCCS_ACTIONS.DECLINE_PROPOSAL
        ? updateData.declineProposalOption.value === 'other'
          ? updateData.actionDeclineProposalText
          : updateData.declineProposalOption.description
        : undefined,
    cccsStatusDiscontinueReason:
      updateData?.action === CCCS_ACTIONS.DISCONTINUE_PLAN
        ? updateData.discontinuePlanOption.value === 'other'
          ? updateData.actionDiscontinuePlanText
          : updateData.discontinuePlanOption.description
        : undefined
  };

  if (updateData?.action === CCCS_ACTIONS.UPDATE_PLAN) {
    result.action = CCCS_ACTIONS.ACCEPT_PROPOSAL;
  }

  if (isReAgePaymentPeriodNone(result.reagePaymentPeriod)) {
    delete result['reagePaymentPeriod'];
  }

  return result;
};

export const isCompanyEqual = (companyA: Company, companyB: Company) => {
  return isEqual(companyA.companyName?.trim(), companyB.companyName?.trim());
};

export const prepareReturnDataCCCS = (data: ICurrentResultCCCS) => {
  const { reagePaymentPeriod, sentVia } = data;

  let preparedData = {
    ...data,
    reagePaymentPeriod: {
      value: reagePaymentPeriod ? reagePaymentPeriod.toString() : '0',
      description: reagePaymentPeriod ? `${reagePaymentPeriod} Months` : ''
    }
  };

  if (sentVia) {
    preparedData['sentVia'] = {
      value: sentVia.toString(),
      description: capitalizeWord(sentVia.toString())
    };
  }

  if (data?.activeCccsExists) {
    preparedData = {
      ...preparedData,
      minimumAmount: data?.dmMinAmount,
      balancePercentage: data?.dmPercentOfBal
    };
  }
  return preparedData;
};

export const preparePostCompanyData = (data: Company) => {
  return {
    ...data,
    addressLineTwo: data?.addressLineTwo || ' ',
    contact: data?.contact || ' ',
    state: data?.state?.value
  };
};

export const isFormatNumericField = (fieldKey: string) => {
  const formattedList = ['dmTotalDebt'];

  return fieldKey.includes('Amount') || formattedList.includes(fieldKey);
};

export const trimLeadingZeroNumeric = (numeric: any) => {
  if (isNaN(numeric)) return numeric;

  return Number.isInteger(numeric * 1)
    ? parseInt(numeric).toString()
    : parseFloat(numeric).toFixed(2).toString();
};

export const formatCCCSNumeric = (updateData: any) => {
  const formattedData = { ...updateData };

  Object.keys(formattedData).forEach(fieldKey => {
    if (isFormatNumericField(fieldKey)) {
      formattedData[fieldKey] = trimLeadingZeroNumeric(formattedData[fieldKey]);
    }
  });

  return formattedData;
};

export const isReAgePaymentPeriodChangeFromNoneToOtherValue = (
  previous: any,
  current: any
) => {
  return (
    isReAgePaymentPeriodNone(previous) && !isReAgePaymentPeriodNone(current)
  );
};

export const isDiffEndDate = (date: Date, anotherDate: Date) => {
  const dateStr = formatTimeDefault(date, FormatTime.DefaultPostDate);
  const anotherDateStr = formatTimeDefault(
    anotherDate,
    FormatTime.DefaultPostDate
  );

  return dateStr !== anotherDateStr;
};

export const prepareDate = (
  cccsData: any,
  changesAction?: CCCS_ACTIONS,
  currentReAgePaymentPeriodValue?: RefDataValue | null
) => {
  let dmStartDate = cccsData?.startDate;
  let dmEndDate = cccsData?.endDate;
  const action = cccsData?.action || CCCS_ACTIONS.NEW_PROPOSAL;

  const today = new Date();
  today.setHours(0, 0, 0, 0);

  let startDate = new Date(today.getTime());

  if (dmStartDate) {
    dmStartDate = formatTimeDefault(dmStartDate, FormatTime.Date);
    if (dmStartDate) startDate = new Date(dmStartDate);
  }

  if (changesAction === CCCS_ACTIONS.ACCEPT_PROPOSAL) {
    startDate = new Date(today.getTime());
  }

  let endDate = new Date(today.getTime());

  if (action === CCCS_ACTIONS.ACCEPT_PROPOSAL && dmEndDate) {
    dmEndDate = formatTimeDefault(dmEndDate, FormatTime.Date);
    if (dmEndDate) endDate = new Date(dmEndDate);
  } else {
    const reagePaymentPeriodValue =
      currentReAgePaymentPeriodValue?.value ||
      cccsData?.reagePaymentPeriod?.value ||
      '0';

    const copiedStartDate = new Date(startDate.getTime());

    endDate = new Date(
      copiedStartDate.setMonth(
        isReAgePaymentPeriodNone(reagePaymentPeriodValue)
          ? copiedStartDate.getMonth() + 1
          : copiedStartDate.getMonth() +
              Number.parseInt(reagePaymentPeriodValue)
      )
    );
  }

  startDate.setHours(0, 0, 0, 0);
  endDate.setHours(0, 0, 0, 0);

  return { startDate, endDate };
};
