import { CCCS_ACTIONS } from './constants';
export interface CCCSState {
  [storeId: string]: {
    data: ICurrentResultCCCS;
    showConfirmModal?: boolean;
    checkedList?: number[] | string[];
    update?: {
      loading?: boolean;
    };
  };
}

export interface ICurrentResultCCCS {
  activeCccsExists?: boolean;
  companyName?: string;
  phone?: string;
  fax?: string;
  contact?: string;
  addressLineOne?: string;
  addressLineTwo?: string;
  city?: string;
  state?: string;
  zip?: string;
  action?: CCCS_ACTIONS;
  balancePercentage?: number | string;
  clientId?: string;
  clientSince?: Date | string;
  cycleToDatePaymentAmount?: number | string;
  dateReceived?: Date | string;
  minimumAmount?: number | string;
  nonDelinquentMpdAmount?: number | string;
  numberOfCreditors?: string;
  paymentAmount?: number | string;
  reagePaymentPeriod?: RefDataValue;
  sentVia?: RefDataValue;
  startDate?: Date | string;
  endDate?: Date | string;
  totalDebt?: number | string;
  processStatus?: string;
  dmPercentOfBal?: string;
  dmMinAmount?: string;
  declineProposalOption?: Record<string, any>;
  discontinuePlanOption?: Record<string, any>;
  actionDeclineProposalText?: string;
  actionDiscontinuePlanText?: string;
  currentBalance: number | string;
}

export interface IUpdateCCCSArgs {
  storeId: string;
}

export interface UpdateDataPayloadAction {
  storeId: string;
  data: ICurrentResultCCCS;
}

export interface UpdateDataCCCS extends ICurrentResultCCCS {}

export interface ToggleConfirmModalPayloadAction {
  storeId: string;
  opened?: boolean;
}

export interface GetCCCSArgs {
  storeId: string;
}

export interface GetCCCSPayload {
  data: ICurrentResultCCCS;
}
