import React from 'react';
import classNames from 'classnames';

// component
import { View } from 'app/_libraries/_dof/core';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// types
import { ICurrentResultCCCS } from './types';

// selectors
import { selectCCCSData } from './_redux/selectors';
import { CCCS_ACTIONS } from './constants';
import { prepareCompanyData } from './helpers';

interface CCCSProps {}

const CCCS: React.FC<CCCSProps> = () => {
  const { storeId } = useAccountDetail();

  const data: ICurrentResultCCCS = useStoreIdSelector(selectCCCSData);

  if (data?.action === CCCS_ACTIONS.DISCONTINUE_PLAN) return null;

  return (
    <div className={classNames('mt-16')}>
      <View
        id={`${storeId}-infoCCCSView`}
        formKey={`${storeId}-infoCCCSView`}
        descriptor="infoCCCSView"
        value={{ ...data, ...prepareCompanyData(data) }}
      />
    </div>
  );
};

export default CCCS;
