import { cccsService } from './api-services';

// utils
import { mockApiServices, storeId } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

describe('cccsService', () => {
  describe('getCCCSDetail', () => {
    const params = { accountId: storeId };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cccsService.getCCCSDetail(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cccsService.getCCCSDetail(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.getCCCSDetail,
        params
      );
    });
  });

  describe('updatePendingCCCS', () => {
    const params = { accountId: storeId };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cccsService.updatePendingCCCS(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cccsService.updatePendingCCCS(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.updatePendingCCCS,
        params
      );
    });
  });

  describe('updateAcceptProposalCCCS', () => {
    const params = { accountId: storeId };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cccsService.updateAcceptProposalCCCS(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cccsService.updateAcceptProposalCCCS(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.updateAcceptProposalCCCS,
        params
      );
    });
  });

  describe('updateDeclineProposalCCCS', () => {
    const params = { accountId: storeId };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cccsService.updateDeclineProposalCCCS(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cccsService.updateDeclineProposalCCCS(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.updateDeclineProposalCCCS,
        params
      );
    });
  });

  describe('updateDiscontinuePlanCCCS', () => {
    const params = { accountId: storeId };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cccsService.updateDiscontinuePlanCCCS(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cccsService.updateDiscontinuePlanCCCS(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.updateDiscontinuePlanCCCS,
        params
      );
    });
  });
});
