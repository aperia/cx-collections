import React from 'react';
import CCCS from './index';
import { renderMockStoreId, storeId } from 'app/test-utils';
import { screen } from '@testing-library/react';
// types
import { ICurrentResultCCCS } from './types';
import { CCCS_ACTIONS } from './constants';

jest.mock('app/_libraries/_dof/core/View', () => () => {
  return <div data-testid="dls-View" />;
});

const data: ICurrentResultCCCS = {
  clientId: storeId,
  clientSince: 'clientSince',
  sentVia: 'sentVia',
  action: CCCS_ACTIONS.PENDING_PROPOSAL,
  address: '91715 SunnySide Drive, Hampton, VA 23667',
  balancePercentage: 50,
  checkedList: [4],
  company: 'MONEY NETWORK',
  contact: 'Thomas Cesar',
  cycleToDatePaymentAmount: 200,
  dateReceived: 'dateReceived',
  fax: '5355260021',
  minimumAmount: 100,
  nonDelinquentMpdAmount: 100,
  numberOfCreditors: '23',
  paymentAmount: 100,
  phone: '5355260021',
  reagePaymentPeriod: { value: '3', description: '3 Months' },
  startDate: '2021-03-19T03:12:05.398Z',
  totalDebt: '10.00'
};

describe('Render UI', () => {
  it('Render UI with discontinue plan action', () => {
    renderMockStoreId(<CCCS />, {
      initialState: {
        cccs: {
          [storeId]: {
            data: { ...data, action: CCCS_ACTIONS.DISCONTINUE_PLAN }
          }
        }
      }
    });

    expect(screen.queryByTestId('dls-View')).toBeNull();
  });

  it('Render UI with totalDebt', () => {
    renderMockStoreId(<CCCS />, {
      initialState: { cccs: { [storeId]: { data } } }
    });

    expect(screen.getByTestId('dls-View')).toBeInTheDocument();
  });

  it('Render UI without totalDebt', () => {
    renderMockStoreId(<CCCS />, {
      initialState: { cccs: { [storeId]: { data } } }
    });

    expect(screen.getByTestId('dls-View')).toBeInTheDocument();
  });
});
