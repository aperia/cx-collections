import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { CCCSState, ToggleConfirmModalPayloadAction } from '../types';
import { triggerUpdateCCCS, updateCCCSBuilder } from './updateCCCS';
import { getCCCSDetail, getCCCSDetailBuilder } from './getCCCSDetail';

const { actions, reducer } = createSlice({
  name: 'cccs',
  initialState: {} as CCCSState,
  reducers: {
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },

    toggleConfirmModal: (
      draftState,
      action: PayloadAction<ToggleConfirmModalPayloadAction>
    ) => {
      const { storeId, opened } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        showConfirmModal: opened
      };
    }
  },
  extraReducers: builder => {
    updateCCCSBuilder(builder);
    getCCCSDetailBuilder(builder);
  }
});

const allActions = {
  ...actions,
  triggerUpdateCCCS,
  getCCCSDetail
};

export { allActions as cccsActions, reducer };
