import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { CCCS_ACTIONS } from './../constants';
import { createStore, isRejected, Store } from '@reduxjs/toolkit';
import { updateCCCS, triggerUpdateCCCS } from './updateCCCS';

import { cccsService } from '../api-services';
import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault,
  storeId
} from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

const initialState: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      method: 'edit'
    }
  },
  form: {
    [`${storeId}-enterCCCS`]: {
      values: {}
    }
  } as any,
  company: {
    [storeId]: {
      companyList: [{ id: 'com_id' }]
    }
  },
  cccs: {
    [storeId]: {
      checkedList: ['com_id']
    }
  }
};

let store: Store<RootState>;

const mockArgs = {
  storeId,
  postData: { accountId: storeId }
};

const toastSpy = mockActionCreator(actionsToast);
const collectionFormActionSpy = mockActionCreator(collectionFormActions);

beforeEach(() => {
  store = createStore(rootReducer, initialState);
});

describe('Test updateCCCS', () => {
  it('Should return data with type PENDING_PROPOSAL', async () => {
    jest
      .spyOn(cccsService, 'updatePendingCCCS')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const response = await updateCCCS({
      ...mockArgs,
      type: CCCS_ACTIONS.PENDING_PROPOSAL
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({ ...responseDefault, data: {} });
  });

  it('Should return data with type ACCEPT_PROPOSAL', async () => {
    jest
      .spyOn(cccsService, 'updateAcceptProposalCCCS')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const response = await updateCCCS({
      ...mockArgs,
      type: CCCS_ACTIONS.ACCEPT_PROPOSAL
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({ ...responseDefault, data: {} });
  });

  it('Should return data with type UPDATE_PLAN', async () => {
    jest
      .spyOn(cccsService, 'updateAcceptProposalCCCS')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const response = await updateCCCS({
      ...mockArgs,
      type: CCCS_ACTIONS.UPDATE_PLAN
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({ ...responseDefault, data: {} });
  });

  it('Should return data with type DECLINE_PROPOSAL', async () => {
    jest
      .spyOn(cccsService, 'updateDeclineProposalCCCS')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const response = await updateCCCS({
      ...mockArgs,
      type: CCCS_ACTIONS.DECLINE_PROPOSAL
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({ ...responseDefault, data: {} });
  });

  it('Should return data with type DISCONTINUE_PLAN', async () => {
    jest
      .spyOn(cccsService, 'updateDiscontinuePlanCCCS')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const response = await updateCCCS({
      ...mockArgs,
      type: CCCS_ACTIONS.UPDATE_PLAN
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual(undefined);
  });

  it('Should return data with type DISCONTINUE_PLAN', async () => {
    jest
      .spyOn(cccsService, 'updateAcceptProposalCCCS')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const response = await updateCCCS({
      ...mockArgs,
      type: CCCS_ACTIONS.DISCONTINUE_PLAN
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual(undefined);
  });

  it('Should return data without type', async () => {
    jest
      .spyOn(cccsService, 'updateAcceptProposalCCCS')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const response = await updateCCCS({
      ...mockArgs
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({});
  });

  it('Should error', async () => {
    jest
      .spyOn(cccsService, 'updatePendingCCCS')
      .mockRejectedValue({ ...responseDefault, data: {} });

    const response = await updateCCCS({
      ...mockArgs,
      type: CCCS_ACTIONS.PENDING_PROPOSAL
    })(store.dispatch, store.getState, {});

    expect(isRejected(response)).toBeTruthy();
  });
});

describe('Test triggerUpdateCCCS', () => {
  describe('isFulfilled', () => {
    it('case normal', async () => {
      jest
        .spyOn(cccsService, 'updateDiscontinuePlanCCCS')
        .mockResolvedValue({ ...responseDefault, data: {} });

      const mockAddToast = toastSpy('addToast');
      const mockToggleModal = collectionFormActionSpy('toggleModal');
      const mockGetLatestActionRequest = collectionFormActionSpy(
        'getLatestActionRequest'
      );
      const mockToggleCallResultInProgressStatus = collectionFormActionSpy(
        'toggleCallResultInProgressStatus'
      );

      await triggerUpdateCCCS({
        storeId,
        postData: { accountId: storeId }
      })(byPassFulfilled(triggerUpdateCCCS.fulfilled.type), store.getState, {});

      expect(mockAddToast).toBeCalledWith({
        message: 'txt_cccs_collection_form_updated',
        type: 'success',
        show: true
      });
      expect(mockToggleModal).toBeCalled();
      expect(mockGetLatestActionRequest).toBeCalled();
      expect(mockToggleCallResultInProgressStatus).not.toBeCalled();
    });

    it('fulfilled with dmLastAction', async () => {
      store = createStore(rootReducer, {
        ...initialState,
        collectionForm: {
          [storeId]: {
            method: 'create'
          }
        },
        cccs: {
          [storeId]: {
            checkedList: ['com_id'],
            data: {
              action: 'ACCEPT'
            }
          }
        } as any,
        company: {
          [storeId]: {
            companyList: [{ id: 423 }],
            checkedCompany: { id: 1 }
          }
        },
        mapping: {
          loading: false,
          data: {
            updateCCCS: {
              action: 'dmLastAction'
            }
          }
        }
      });
      jest
        .spyOn(cccsService, 'updateDiscontinuePlanCCCS')
        .mockResolvedValue({ ...responseDefault, data: {} });

      const mockAddToast = toastSpy('addToast');
      const mockToggleModal = collectionFormActionSpy('toggleModal');
      const mockGetLatestActionRequest = collectionFormActionSpy(
        'getLatestActionRequest'
      );
      const mockToggleCallResultInProgressStatus = collectionFormActionSpy(
        'toggleCallResultInProgressStatus'
      );

      await triggerUpdateCCCS({
        storeId,
        postData: { accountId: storeId }
      })(byPassFulfilled(triggerUpdateCCCS.fulfilled.type), store.getState, {});

      expect(mockAddToast).toBeCalledWith({
        message: 'txt_cccs_collection_form_submitted',
        type: 'success',
        show: true
      });
      expect(mockToggleModal).toBeCalled();
      expect(mockGetLatestActionRequest).toBeCalled();
      expect(mockToggleCallResultInProgressStatus).toBeCalled();
    });
  });

  it('isRejected', async () => {
    jest
      .spyOn(cccsService, 'updateDiscontinuePlanCCCS')
      .mockResolvedValue({ ...responseDefault, data: {} });
    const mockAddToast = toastSpy('addToast');

    await triggerUpdateCCCS({
      storeId,
      postData: { accountId: storeId }
    })(byPassRejected(triggerUpdateCCCS.rejected.type), store.getState, {});

    expect(mockAddToast).toBeCalledWith({
      message: 'txt_cccs_collection_form_failed',
      show: true,
      type: 'error'
    });
  });
});
