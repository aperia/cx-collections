import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { createStore, Store } from '@reduxjs/toolkit';
import { getCCCSDetail } from './getCCCSDetail';

import { cccsService } from '../api-services';
import { mockActionCreator, responseDefault, storeId } from 'app/test-utils';
import { rootReducer } from 'storeConfig';

const initialState: Partial<RootState> = {
  collectionForm: {
    [storeId]: {
      inProgressInfo: { value: '', dmLastAction: 'action' },
      isInProgress: true
    }
  },
  mapping: {
    loading: false,
    data: {
      cccs: {
        sentVia: 'sentVia',
        action: 'action'
      }
    }
  }
};

let store: Store<RootState>;

const mockArgs = {
  storeId,
  postData: { accountId: storeId }
};

const collectionFormActionSpy = mockActionCreator(collectionFormActions);

beforeEach(() => {
  store = createStore(rootReducer, initialState);
});

describe('Test getCCCSDetail', () => {
  it('Should return data with empty state', async () => {
    const newStore = createStore(rootReducer, {});
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app'
      }
    };

    jest
      .spyOn(cccsService, 'getCCCSDetail')
      .mockResolvedValue({ ...responseDefault, data: {} });
    const mockAction = collectionFormActionSpy(
      'toggleCallResultInProgressStatus'
    );

    const response = await getCCCSDetail({
      ...mockArgs
    })(newStore.dispatch, newStore.getState, {});

    expect(response.payload).toEqual({
      data: {
        reagePaymentPeriod: {
          description: '',
          value: '0'
        }
      }
    });
    expect(mockAction).not.toBeCalled();
  });

  it('Should return data with null commonConfig', async () => {
    const newStore = createStore(rootReducer, {});
    window.appConfig = {
      ...window.appConfig,
      commonConfig: null
    };

    jest
      .spyOn(cccsService, 'getCCCSDetail')
      .mockResolvedValue({ ...responseDefault, data: {} });
    const mockAction = collectionFormActionSpy(
      'toggleCallResultInProgressStatus'
    );

    const response = await getCCCSDetail({
      ...mockArgs
    })(newStore.dispatch, newStore.getState, {});

    expect(response.payload).toEqual({
      data: {
        reagePaymentPeriod: {
          description: '',
          value: '0'
        }
      }
    });
    expect(mockAction).not.toBeCalled();
  });

  it('Should return data with state', async () => {
    jest.spyOn(cccsService, 'getCCCSDetail').mockResolvedValue({
      ...responseDefault,
      data: { activeCccsExists: true, action: 'action', sentVia: 'sentVia' }
    });
    const mockAction = collectionFormActionSpy(
      'toggleCallResultInProgressStatus'
    );

    const response = await getCCCSDetail({
      ...mockArgs
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({
      data: {
        action: 'action',
        reagePaymentPeriod: {
          description: '',
          value: '0'
        },
        sentVia: {
          description: 'Sentvia',
          value: 'sentVia'
        }
      }
    });
    expect(mockAction).toBeCalled();
  });

  it('Should error', async () => {
    jest
      .spyOn(cccsService, 'getCCCSDetail')
      .mockRejectedValue({ ...responseDefault, data: {} });
    const mockAction = collectionFormActionSpy(
      'toggleCallResultInProgressStatus'
    );

    const response = await getCCCSDetail({
      ...mockArgs
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({ ...responseDefault, data: {} });
    expect(mockAction).not.toBeCalled();
  });
});
