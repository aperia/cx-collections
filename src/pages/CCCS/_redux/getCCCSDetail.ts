import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromObj } from 'app/helpers';
import { isEqual } from 'app/_libraries/_dls/lodash';
import isEmpty from 'lodash.isempty';
import { dispatchDestroyDelayProgress } from 'pages/CollectionForm/helpers';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
// Service
import { cccsService } from '../api-services';
import { prepareReturnDataCCCS } from '../helpers';
// Type
import { CCCSState, GetCCCSArgs, GetCCCSPayload } from '../types';

export const getCCCSDetail = createAsyncThunk<
  GetCCCSPayload,
  GetCCCSArgs,
  ThunkAPIConfig
>('cccs/getCCCSDetail', async (args, thunkAPI) => {
  const { storeId } = args;
  const { dispatch } = thunkAPI;
  const { collectionForm, accountDetail } = thunkAPI.getState();
  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;

  const {
    callingApplicationTestA,
    operatorID: operatorCode,
    org,
    app
  } = window.appConfig?.commonConfig || {};

  const { inProgressInfo = {}, isInProgress } = collectionForm[storeId] || {};

  try {
    const { data } = await cccsService.getCCCSDetail({
      common: {
        accountId: eValueAccountId,
        org,
        app
      },
      callingApplication: callingApplicationTestA,
      operatorCode
    });

    const state = thunkAPI.getState();
    const cccsMapping = state.mapping?.data?.cccs || {};

    const mappingData = mappingDataFromObj(data, cccsMapping);

    if (
      !isEmpty(inProgressInfo?.dmLastAction) &&
      isEqual(inProgressInfo.dmLastAction, mappingData?.action) &&
      isInProgress
    ) {
      dispatch(
        collectionFormActions.toggleCallResultInProgressStatus({
          storeId,
          info: data
        })
      );
      dispatchDestroyDelayProgress(storeId);
    }

    const preparedData = prepareReturnDataCCCS(mappingData);

    return {
      data: preparedData
    };
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const getCCCSDetailBuilder = (
  builder: ActionReducerMapBuilder<CCCSState>
) => {
  builder
    .addCase(getCCCSDetail.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        update: {
          loading: true
        }
      };
    })
    .addCase(getCCCSDetail.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        update: {
          loading: false
        },
        data: action.payload.data
      };
    })
    .addCase(getCCCSDetail.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        update: {
          loading: false
        }
      };
    });
};
