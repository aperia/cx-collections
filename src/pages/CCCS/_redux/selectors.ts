import { createSelector } from '@reduxjs/toolkit';
import { ICurrentResultCCCS } from '../types';

export const selectCCCSData = createSelector(
  (states: RootState, storeId: string) => states.cccs?.[storeId]?.data,
  (data: ICurrentResultCCCS) => data
);

export const selectIsOpenConfirmModal = createSelector(
  (states: RootState, storeId: string) =>
    states.cccs[storeId]?.showConfirmModal || false,
  (data: boolean) => data
);

export const selectIsLoading = createSelector(
  (states: RootState, storeId: string) =>
    states.cccs[storeId]?.update?.loading || false,
  (data: boolean) => data
);

export const selectCCCSProcessStatus = createSelector(
  (states: RootState, storeId: string) => states.cccs?.[storeId]?.data?.processStatus || "",
  (data: string) => data
);

export const selectCCCSAction = createSelector(
  (states: RootState, storeId: string) => states.cccs?.[storeId]?.data?.action || "",
  (data: string) => data
);
