import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { mappingDataFromObj } from 'app/helpers';
import { AxiosResponse } from 'axios';
import isEqual from 'lodash.isequal';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import {
  CALL_RESULT_CODE,
  COLLECTION_METHOD
} from 'pages/CollectionForm/constants';
import {
  dispatchDelayProgress,
  setDelayProgress
} from 'pages/CollectionForm/helpers';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
// actions
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
// helpers
import { cccsService } from '../api-services';
import { prepareUpdateDataCCCS } from '../helpers';
// types
import { CCCSState, IUpdateCCCSArgs } from '../types';
import { CCCS_ACTIONS } from './../constants';
import { formatCCCSNumeric } from './../helpers';
import { cccsActions } from './reducers';

export const updateCCCS = createAsyncThunk<
  AxiosResponse,
  MagicKeyValue,
  ThunkAPIConfig
>('cccs/updateCCCS', async (args, thunkAPI) => {
  try {
    const payload = args.postData;
    switch (args.type) {
      case CCCS_ACTIONS.PENDING_PROPOSAL:
        return await cccsService.updatePendingCCCS(payload);
      case CCCS_ACTIONS.ACCEPT_PROPOSAL:
      case CCCS_ACTIONS.UPDATE_PLAN:
        return await cccsService.updateAcceptProposalCCCS(payload);
      case CCCS_ACTIONS.DECLINE_PROPOSAL:
        return await cccsService.updateDeclineProposalCCCS(payload);
      case CCCS_ACTIONS.DISCONTINUE_PLAN:
        return await cccsService.updateDiscontinuePlanCCCS(payload);
      default:
        return thunkAPI.rejectWithValue({});
    }
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerUpdateCCCS = createAsyncThunk<
  void,
  IUpdateCCCSArgs,
  ThunkAPIConfig
>('cccs/triggerUpdateCCCS', async (args, thunkAPI) => {
  const { storeId } = args;

  const { dispatch } = thunkAPI;
  const { form, company, cccs, collectionForm, mapping, accountDetail } =
    thunkAPI.getState();
  const cccsMapping = mapping?.data?.updateCCCS || {};
  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;
  const {
    callingApplicationTestA,
    operatorID: operatorCode,
    org,
    app
  } = window.appConfig?.commonConfig || {};

  const viewName = `${storeId}-enterCCCS`;
  const dataFromView = form[viewName].values;

  let checkedCompany = company[storeId].checkedCompany;

  if (!checkedCompany) {
    checkedCompany = company[storeId].companyList?.find(
      item => item.companyName === cccs[storeId]?.data?.companyName
    );
  }

  let updateData: MagicKeyValue = {
    ...cccs[storeId]?.data,
    ...dataFromView,
    ...checkedCompany
  };

  updateData = prepareUpdateDataCCCS(updateData);

  // clear data when discontinue plan, wait for confirm
  // if (dataFromView?.action === CCCS_ACTIONS.DISCONTINUE_PLAN) {
  //   updateData = {
  //     action: dataFromView?.action,
  //     cycleToDatePaymentAmount: dataFromView?.cycleToDatePaymentAmount,
  //     startDate: dataFromView?.startDate,
  //     endDate: dataFromView?.endDate,
  //     balancePercentage: dataFromView?.balancePercentage,
  //     minimumAmount: dataFromView?.minimumAmount,
  //     paymentAmount: dataFromView?.minimumAmount,
  //     nonDelinquentMpdAmount:
  //       Number(dataFromView?.cycleToDatePaymentAmount) -
  //       Number(dataFromView?.minimumAmount)
  //   };
  // }

  updateData = mappingDataFromObj(updateData, cccsMapping, true);

  updateData = formatCCCSNumeric(updateData);

  const type = dataFromView?.action;
  const dataRequest = {
    common: { accountId: eValueAccountId, app, org },
    callingApplication: callingApplicationTestA,
    operatorCode,
    cardholderContactedName:
      accountDetail[storeId]?.selectedAuthenticationData
        ?.customerNameForCreateWorkflow,
    ...updateData
  };

  const response = await dispatch(
    updateCCCS({ storeId, type, postData: dataRequest })
  );

  if (isFulfilled(response)) {
    const responseCCCSDetail = await dispatch(
      cccsActions.getCCCSDetail({
        storeId
      })
    );

    batch(() => {
      const isSubmit =
        collectionForm[storeId].method === COLLECTION_METHOD.CREATE;
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: isSubmit
            ? I18N_COLLECTION_FORM.CCCS_SUBMIT_SUCCESS
            : I18N_COLLECTION_FORM.CCCS_UPDATE_SUCCESS
        })
      );

      dispatch(
        collectionFormActions.getLatestActionRequest({
          storeId
        })
      );

      dispatch(collectionFormActions.toggleModal({ storeId }));
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inCollectionFormModal'
        })
      );

      if (
        !isEqual(
          updateData?.dmLastAction,
          (responseCCCSDetail?.payload as MagicKeyValue)?.data?.dmLastAction
        )
      ) {
        dispatch(
          collectionFormActions.toggleCallResultInProgressStatus({
            storeId,
            info: updateData,
            lastDelayCallResult: CALL_RESULT_CODE.CCCS
          })
        );
        dispatchDelayProgress(
          cccsActions.getCCCSDetail({
            storeId
          }),
          storeId
        );

        setDelayProgress(storeId, CALL_RESULT_CODE.CCCS, updateData);
      }
    });
  }
  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_COLLECTION_FORM.CCCS_FAILED
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inCollectionFormModal',
          apiResponse: response.payload?.response
        })
      );
    });
  }
});

export const updateCCCSBuilder = (
  builder: ActionReducerMapBuilder<CCCSState>
) => {
  builder
    .addCase(updateCCCS.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        update: {
          loading: true
        }
      };
    })

    .addCase(updateCCCS.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        update: {
          loading: false
        },
        showConfirmModal: false
      };
    })

    .addCase(updateCCCS.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        update: {
          loading: false
        },
        showConfirmModal: false
      };
    });
};
