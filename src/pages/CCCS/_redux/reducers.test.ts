import { cccsActions, reducer } from './reducers';
import { storeId } from 'app/test-utils';
import { CCCSState } from '../types';

const { removeStore, toggleConfirmModal } = cccsActions;

const initialState: CCCSState = {
  [storeId]: {
    data: {
      balancePercentage: '',
      clientId: '',
      clientSince: '',
      cycleToDatePaymentAmount: '',
      dateReceived: '',
      minimumAmount: '',
      nonDelinquentMpdAmount: '',
      numberOfCreditors: '',
      paymentAmount: '',
      sentVia: '',
      startDate: '',
      totalDebt: ''
    },
    checkedList: []
  }
};

describe('CCCS reducer', () => {
  it('should run removeStore', () => {
    const state = reducer(initialState, removeStore({ storeId }));
    expect(state[storeId]).toBeUndefined();
  });

  it('should run toggleConfirmModal', () => {
    const state = reducer(
      initialState,
      toggleConfirmModal({ storeId, opened: true })
    );
    expect(state[storeId].showConfirmModal).toBeTruthy();
  });
});
