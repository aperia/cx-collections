import { CCCS_ACTIONS } from 'pages/CCCS/constants';
import * as selectors from './selectors';

import { selectorWrapper, storeId } from 'app/test-utils';

const initialState: Partial<RootState> = {
  cccs: {
    [storeId]: {
      data: {
        clientId: storeId,
        activeCccsExists: true,
        processStatus: 'status',
        action: CCCS_ACTIONS.PENDING_PROPOSAL
      },
      showConfirmModal: true,
      update: {
        loading: true
      }
    }
  }
};

const selectorTrigger = selectorWrapper(initialState);

describe('Selector CCCS', () => {
  it('selectCCCSData', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectCCCSData);
    expect(data).toEqual(initialState.cccs![storeId].data);
    expect(emptyData).toEqual(undefined);
  });

  it('selectIsOpenConfirmModal', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectIsOpenConfirmModal
    );
    expect(data).toEqual(initialState.cccs![storeId].showConfirmModal);
    expect(emptyData).toEqual(false);
  });

  it('selectIsLoading', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectIsLoading);
    expect(data).toEqual(initialState.cccs![storeId].update!.loading);
    expect(emptyData).toEqual(false);
  });

  it('selectIsLoading', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectIsLoading);
    expect(data).toEqual(initialState.cccs![storeId].update!.loading);
    expect(emptyData).toEqual(false);
  });

  it('selectCCCSProcessStatus', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectCCCSProcessStatus
    );
    expect(data).toEqual(initialState.cccs![storeId].data!.processStatus);
    expect(emptyData).toEqual('');
  });

  it('selectCCCSAction', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectCCCSAction
    );
    expect(data).toEqual(initialState.cccs![storeId].data!.action);
    expect(emptyData).toEqual('');
  });
});
