import { apiService } from 'app/utils/api.service';

export const cccsService = {
  getCCCSDetail(data: any) {
    const url = window.appConfig?.api?.collectionForm?.getCCCSDetail || '';
    return apiService.post(url, data);
  },
  updatePendingCCCS(data: any) {
    const url = window.appConfig?.api?.collectionForm?.updatePendingCCCS || '';
    return apiService.post(url, data);
  },
  updateAcceptProposalCCCS(data: any) {
    const url =
      window.appConfig?.api?.collectionForm?.updateAcceptProposalCCCS || '';
    return apiService.post(url, data);
  },
  updateDeclineProposalCCCS(data: any) {
    const url =
      window.appConfig?.api?.collectionForm?.updateDeclineProposalCCCS || '';
    return apiService.post(url, data);
  },
  updateDiscontinuePlanCCCS(data: any) {
    const url =
      window.appConfig?.api?.collectionForm?.updateDiscontinuePlanCCCS || '';
    return apiService.post(url, data);
  }
};
