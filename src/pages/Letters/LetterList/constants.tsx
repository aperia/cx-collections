// types
import { SortType } from 'app/_libraries/_dls/components';

export const SORT_TYPE = {
  letterId: 'letterId',
  letterDescription: 'letterDescription'
};

export const SORT_DEFAULT: SortType = {
  id: SORT_TYPE.letterId,
  order: undefined
};

export const disabledSendBasedDestinationCode = 'W';
