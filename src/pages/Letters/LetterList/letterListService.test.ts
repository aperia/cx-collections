import letterListService from './letterListService';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import { SendLetterPostData } from './types';

describe('letterListService', () => {
  describe('getLetters', () => {
    const params: SendLetterPostData = {
      common: { org: 'org', app: 'app' }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      letterListService.getLetters(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      letterListService.getLetters(params);

      expect(mockService).toBeCalledWith(apiUrl.sendLetter.getLetters, params);
    });
  });
});
