import cloneDeep from 'lodash.clonedeep';
import * as selector from './selectors';

// mocks
import { useSelectorMock } from 'app/test-utils/useSelectorMock';

// utils
import { storeId } from 'app/test-utils';
import { SORT_DEFAULT, SORT_TYPE } from '../constants';

const store: Partial<RootState> = {
  letterList: {
    [storeId]: {
      openModal: true,
      letters: {
        loading: true,
        total: 10,
        page: 1,
        pageSize: 25,
        sortBy: { id: SORT_TYPE.letterId, order: 'asc' },
        error: true,
        noData: true,
        data: [
          {
            letterCode: '2',
            letterDescription: 'description 2',
            letterId: 'id2',
            variableLetter: false
          },
          {
            letterCode: '3',
            letterDescription: 'description 3',
            letterId: 'id3',
            variableLetter: false
          },
          {
            letterCode: '4',
            letterDescription: 'description 4',
            letterId: 'id4',
            variableLetter: false
          },
          {
            letterCode: '1',
            letterDescription: 'description 1',
            letterId: 'id1',
            variableLetter: false
          }
        ]
      },
      activeKey: 'activeKey'
    }
  }
};

const storeEmpty: Partial<RootState> = {
  letterList: { [storeId]: {} }
};

describe('redux-store > letter > selector', () => {
  it('selectLoadingLetter', () => {
    const data = useSelectorMock(selector.selectLoadingLetter, store, storeId);
    const expected = store.letterList![storeId]?.letters?.loading;
    expect(data).toEqual(expected);
  });

  it('selectLoadingLetter empty state', () => {
    const data = useSelectorMock(
      selector.selectLoadingLetter,
      storeEmpty,
      storeId
    );
    expect(data).toEqual(true);
  });

  it('selectSortByLetter', () => {
    const data = useSelectorMock(selector.selectSortByLetter, store, storeId);
    const expected = store.letterList![storeId]?.letters?.sortBy;
    expect(data).toEqual(expected);
  });

  it('selectSortByLetter empty state', () => {
    const data = useSelectorMock(
      selector.selectSortByLetter,
      storeEmpty,
      storeId
    );
    expect(data).toEqual(SORT_DEFAULT);
  });

  describe('selectErrorLetter', () => {
    it('when letters have error', () => {
      const data = useSelectorMock(selector.selectErrorLetter, store, storeId);
      expect(data).toBeTruthy();
    });

    it('when letters have no error', () => {
      const _store = cloneDeep(store);
      _store.letterList![storeId].letters!.error = undefined;
      const data = useSelectorMock(selector.selectErrorLetter, _store, storeId);
      expect(data).toBeFalsy();
    });
  });

  it('selectLetterData', () => {
    const data = useSelectorMock(selector.selectLetterData, store, storeId);
    expect(data.length).toEqual(4);
  });

  it('selectLetterData empty state', () => {
    const data = useSelectorMock(
      selector.selectLetterData,
      storeEmpty,
      storeId
    );
    expect(data.length).toEqual(0);
  });

  it('selectLetterData empty sortby', () => {
    const storeSortBy: Partial<RootState> = {
      letterList: {
        [storeId]: {
          letters: {
            sortBy: { id: undefined as any, order: undefined }
          }
        }
      }
    };

    const data = useSelectorMock(
      selector.selectLetterData,
      storeSortBy,
      storeId
    );
    expect(data.length).toEqual(0);
  });
});
