import { responseDefault, storeId } from 'app/test-utils';
import { clientConfigurationServices } from 'pages/ClientConfiguration/clientConfigurationServices';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import { getLetter } from './getLetter';

describe('getLetter action', () => {
  it('should be fulfilled', async () => {
    window.appConfig = {
      commonConfig: {
        app: 'app',
        user: 'user'
      }
    };
    jest
      .spyOn(clientConfigurationServices, 'getClientInfoById')
      .mockResolvedValue({
        data: {
          responseStatus: 'success',
          clientInfo: {
            clientInfoLetterTemplate: ['emailTemplates']
          }
        }
      });

    const store = createStore(
      rootReducer,
      {
        accountDetail: {
          [storeId]: {
            rawData: {
              systemIdentifier: '123',
              clientIdentifier: '123',
              principalIdentifier: '123',
              agentIdentifier: '123'
            }
          }
        },
        mapping: {
          data: {
            clientConfigurationLetterList: {
              letterId: 'letterId'
            }
          }
        }
      },
      applyMiddleware(thunk)
    );

    const response = await getLetter({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('getLetter/fulfilled');
    expect(response.payload).toEqual([{ letterId: undefined }]);
  });

  it('should be fulfilled with payload and store state empty', async () => {
    window.appConfig = {
      commonConfig: {
        app: 'app',
        user: 'user'
      }
    };
    jest
      .spyOn(clientConfigurationServices, 'getClientInfoById')
      .mockResolvedValue({
        data: {
          responseStatus: 'success',
          clientInfo: {}
        }
      });

    const store = createStore(
      rootReducer,
      {
        accountDetail: {
          [storeId]: {
            rawData: {
              systemIdentifier: '123',
              clientIdentifier: '123',
              principalIdentifier: '123',
              agentIdentifier: '123'
            }
          }
        },
        mapping: {
          data: {}
        }
      },
      applyMiddleware(thunk)
    );

    const response = await getLetter({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('getLetter/fulfilled');
    expect(response.payload).toEqual([]);
  });

  it('should be reject', async () => {
    window.appConfig = {
      commonConfig: {
        app: 'app',
        user: 'user'
      }
    };
    jest
      .spyOn(clientConfigurationServices, 'getClientInfoById')
      .mockRejectedValue({
        ...responseDefault,
        error: {
          message: 'error'
        }
      });

    const store = createStore(rootReducer, {}, applyMiddleware(thunk));

    const response = await getLetter({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('getLetter/rejected');
    expect(response.payload).toEqual({ errorMessage: 'Rejected' });
  });
});
