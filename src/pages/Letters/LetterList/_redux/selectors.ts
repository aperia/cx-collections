import { createSelector } from '@reduxjs/toolkit';
import { SORT_DEFAULT } from 'pages/Letters/LetterList/constants';
import { LetterDetailData, Letters } from '../types';
import orderBy from 'lodash.orderby';
import isUndefined from 'lodash.isundefined';

const getLetters = (states: RootState, storeId: string) =>
  states.letterList[storeId]?.letters || {};

export const selectLoadingLetter = createSelector(
  getLetters,
  (letters: Letters) => (isUndefined(letters.loading) ? true : letters.loading)
);

export const selectSortByLetter = createSelector(
  getLetters,
  (letters: Letters) => letters.sortBy || SORT_DEFAULT
);

export const selectLetterData = createSelector(
  getLetters,
  (letters: Letters) => {
    const sortBy = letters.sortBy || { id: 'letterId', order: 'asc' };
    return orderBy(
      letters.data || [],
      [(letter: LetterDetailData) => letter[sortBy.id]?.toLowerCase()],
      sortBy.order || 'asc'
    )
  }
);

export const selectErrorLetter = createSelector(
  getLetters,
  (letters: Letters) => letters.error || false
);
