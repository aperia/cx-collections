import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { getLetter, getLetterBuilder } from './getLetter';

// types
import { LetterListInitialState, UpdateSortBy } from '../types';

const { actions, reducer } = createSlice({
  name: 'sendLetter',
  initialState: {} as LetterListInitialState,
  reducers: {
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    updateSortBy: (draftState, action: PayloadAction<UpdateSortBy>) => {
      const { storeId, sortBy } = action.payload;
      draftState[storeId].letters = {
        ...draftState[storeId].letters,
        sortBy
      };
    }
  },
  extraReducers: builder => {
    getLetterBuilder(builder);
  }
});

const letterListAction = {
  ...actions,
  getLetter
};

export { letterListAction, reducer };
