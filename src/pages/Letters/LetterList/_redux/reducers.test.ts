import { letterListAction, reducer } from './reducers';

// utils
import { storeId } from 'app/test-utils';

const store = { [storeId]: {} };

describe('Test reducer', () => {
  it('removeStore action', () => {
    const state = reducer(store, letterListAction.removeStore({ storeId }));

    expect(state).toEqual({});
  });

  it('updateSortBy action', () => {
    const state = reducer(
      store,
      letterListAction.updateSortBy({
        storeId,
        sortBy: { id: 'id' }
      })
    );

    expect(state).toEqual({
      [storeId]: { letters: { sortBy: { id: 'id' } } }
    });
  });
});
