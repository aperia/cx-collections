import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isFulfilled
} from '@reduxjs/toolkit';

// types
import {
  LetterListInitialState,
  SendLetterArg,
  LetterDetailData
} from '../types';

//service
import { mappingDataFromArray } from 'app/helpers';
import { orderBy } from 'lodash';
import { getClientInfoData } from 'pages/AccountDetails/_redux/getClientInfoById';

export const getLetter = createAsyncThunk<
  LetterDetailData[],
  SendLetterArg,
  ThunkAPIConfig
>('getLetter', async (args, { getState, dispatch, rejectWithValue }) => {
  const response = await dispatch(
    getClientInfoData({
      dataType: 'clientInfoLetterTemplate',
      storeId: args.storeId
    })
  );

  if (isFulfilled(response)) {
    return orderBy(
      mappingDataFromArray(
        response.payload.clientInfoLetterTemplate || [],
        getState().mapping.data.clientConfigurationLetterList || {}
      ),
      'letterId',
      'asc'
    );
  }

  return rejectWithValue({ errorMessage: response.error.message });
});

export const getLetterBuilder = (
  builder: ActionReducerMapBuilder<LetterListInitialState>
) => {
  builder
    .addCase(getLetter.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...(draftState[storeId] || {}),
        letters: {
          loading: true
        }
      };
    })
    .addCase(getLetter.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].letters = {
        ...draftState[storeId]?.letters,
        loading: false,
        error: false,
        data: action.payload,
        total: action.payload.length
      };
    })
    .addCase(getLetter.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].letters = {
        ...draftState[storeId]?.letters,
        loading: false,
        error: false,
        data: []
      };
    });
};
