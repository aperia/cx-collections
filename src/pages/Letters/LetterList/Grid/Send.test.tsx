import React from 'react';
import { AccountDetailProvider } from 'app/hooks';
import { fireEvent, screen } from '@testing-library/react';

// utils
import { LettersData } from 'pages/Letters/LetterList/types';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { renderMockStore, setupWrapper, storeId } from 'app/test-utils';

// components
import Send from './Send';

// redux
import { sendLetterActions as actions } from 'pages/Letters/SendLetter/_redux/reducers';

const data: LettersData = { description: 'description', letterNumber: '1' };
const newData: LettersData = { description: 'description', letterNumber: '2' };

const initialState: Partial<RootState> = {
  sendLetter: {}
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <Send data={data} />
    </AccountDetailProvider>,
    { initialState }
  );
};

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper(initialState);

    expect(screen.getByText(I18N_COMMON_TEXT.SEND)).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleChangePage', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    jest.spyOn(actions, 'triggerSendLetter').mockImplementation(mockAction);

    const { wrapper } = renderWrapper(initialState);

    // simulate
    fireEvent.click(screen.getByText(I18N_COMMON_TEXT.SEND));

    expect(mockAction).toBeCalledWith({
      storeId,
      letter: data,
      common: { accountId: storeId }
    });

    // rerender with new data
    const { wrapper: preWrapper } = setupWrapper(
      <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
        <Send data={newData} />
      </AccountDetailProvider>,
      { initialState }
    );
    wrapper.rerender(preWrapper);

    // simulate
    fireEvent.click(screen.getByText(I18N_COMMON_TEXT.SEND));

    expect(mockAction).toBeCalledWith({
      storeId,
      letter: newData,
      common: { accountId: storeId }
    });
  });
});
