import React from 'react';
import { AccountDetailProvider } from 'app/hooks';
import { fireEvent, screen } from '@testing-library/react';

// utils
import {
  renderMockStore,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';

// components
import LetterGird, { renderActions } from './index';

// redux
import { PaginationWrapperProps } from 'app/_libraries/_dls/components';
import { LetterDetailData } from '../types';
import { mockActionCreator } from 'app/test-utils';
import { letterListAction } from '../_redux/reducers';

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');

  return {
    ...dlsComponents,
    Pagination: (props: PaginationWrapperProps) => {
      const {
        onChangePage = () => undefined,
        onChangePageSize = () => undefined
      } = props;

      return (
        <div>
          <input
            data-testid="Pagination_onChangePage"
            onChange={(e: any) => onChangePage(e.target.value)}
          />
          <input
            data-testid="Pagination_onChangePageSize"
            onChange={(e: any) => onChangePageSize(e.target)}
          />
        </div>
      );
    }
  };
});

const letterListActionSpy = mockActionCreator(letterListAction);

const multipleData = [
  {
    letterId: 'mock id 1',
    letterCode: '123',
    letterDescription: 'des 1',
    variableLetter: false
  },
  {
    letterId: 'mock id 2',
    letterCode: '123',
    letterDescription: 'des 2',
    variableLetter: true
  },
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {},
  {}
] as LetterDetailData[];
const initialState: Partial<RootState> = {
  letterList: {
    [storeId]: {
      letters: {
        total: 250,
        page: 1,
        pageSize: 50,
        data: [],
        loading: true
      }
    }
  }
};
const initialMultipleDataState: Partial<RootState> = {
  letterList: {
    [storeId]: {
      letters: {
        total: 250,
        page: 1,
        pageSize: 50,
        data: multipleData
      }
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <LetterGird />
    </AccountDetailProvider>,
    {
      initialState
    }
  );
};

describe('Render with loading', () => {
  it('Render UI', () => {
    const { baseElement } = renderWrapper(initialState);

    expect(baseElement.firstChild).toBeEmptyDOMElement();
  });

  it('Render UI with no data', () => {
    const updateState = {
      letterList: {
        [storeId]: {
          letters: {
            total: 250,
            page: 1,
            pageSize: 50,
            data: [],
            loading: false
          }
        }
      }
    };
    renderWrapper(updateState);

    expect(screen.getByText('txt_no_letters_to_display')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleChangePage', () => {
    // mock
    const mockAction = letterListActionSpy('updateSortBy');

    // render
    renderWrapper(initialMultipleDataState);

    // simulate
    fireEvent.click(
      screen.getByTestId('letterGrid_dls-grid_header-txt_letter_number')
    );

    // assert
    expect(mockAction).toBeCalledWith({
      storeId,
      sortBy: {
        id: 'letterId',
        order: 'asc'
      }
    });
  });
});

describe('renderActions', () => {
  it('render UI', () => {
    renderMockStore(
      renderActions(
        {
          letterId: 'mock id 2',
          letterCode: '123',
          letterDescription: 'des 2',
          variableLetter: true
        },
        0
      ),
      {}
    );

    const sendText = screen.getByText('txt_send');

    expect(sendText).toBeInTheDocument();
  });
});
