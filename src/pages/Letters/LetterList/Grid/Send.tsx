import React, { Fragment } from 'react';
import isEqual from 'lodash.isequal';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useDispatch } from 'react-redux';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// Components
import { Button } from 'app/_libraries/_dls/components';

// Redux
import { sendLetterActions } from 'pages/Letters/SendLetter/_redux/reducers';
import { takeDestinationCode } from 'pages/AccountDetails/_redux/selectors';

// Const
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { disabledSendBasedDestinationCode } from '../constants';

// types
import { LetterDetailData } from '../types';

export interface SendProps {
  data: LetterDetailData;
  dataTestId?: string;
}

const Send: React.FC<SendProps> = React.memo(
  ({ data, dataTestId }) => {
    const dispatch = useDispatch();
    const { t } = useTranslation();
    const { accEValue, storeId } = useAccountDetail();
    const destinationCode = useStoreIdSelector(takeDestinationCode);

    const handleSendLetter = () => {
      dispatch(
        sendLetterActions.triggerSendLetter({
          common: {
            accountId: accEValue
          },
          letter: data,
          storeId
        })
      );
    };

    return (
      <Fragment>
        <Button
          dataTestId={dataTestId}
          variant="outline-primary"
          size="sm"
          onClick={handleSendLetter}
          disabled={destinationCode === disabledSendBasedDestinationCode}
        >
          {t(I18N_COMMON_TEXT.SEND)}
        </Button>
      </Fragment>
    );
  },
  (prevProps, nextProps) => isEqual(prevProps.data, nextProps.data)
);

export default Send;
