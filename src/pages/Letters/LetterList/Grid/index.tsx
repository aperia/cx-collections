import React, { useMemo } from 'react';

// constant
import { SORT_TYPE } from '../constants';

// redux
import { useDispatch } from 'react-redux';
import {
  selectLetterData,
  selectLoadingLetter,
  selectSortByLetter
} from '../_redux/selectors';
import { letterListAction } from '../_redux/reducers';

// hooks
import { useAccountDetail, usePagination, useStoreIdSelector } from 'app/hooks';

// components/types
import { LetterDetailData, LettersData } from 'pages/Letters/LetterList/types';
import {
  ColumnType,
  Grid,
  Icon,
  Pagination,
  SortType
} from 'app/_libraries/_dls/components';
import Send from './Send';
import { I18N_COMMON_TEXT, I18N_LETTER_CONFIG } from 'app/constants/i18n';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { isEmpty } from 'lodash';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

export interface LetterGridProps {}

export const renderActions = (values: LetterDetailData, index: number) => (
  <Send data={values} dataTestId={`${index}_send-btn`} />
);

const LetterGrid: React.FC<LetterGridProps> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();

  const letters = useStoreIdSelector<LettersData[]>(selectLetterData);
  const sortBy = useStoreIdSelector<SortType>(selectSortByLetter);
  const loading = useStoreIdSelector<boolean>(selectLoadingLetter);

  const handleChangeSort = (sort: SortType) => {
    dispatch(letterListAction.updateSortBy({ storeId, sortBy: sort }));
  };

  const columns = useMemo(() => {
    const allColumns: ColumnType<LetterDetailData>[] = [
      {
        id: SORT_TYPE.letterId,
        Header: t(I18N_LETTER_CONFIG.LETTER_NUMBER),
        isSort: true,
        accessor: 'letterId',
        width: 240
      },
      {
        id: SORT_TYPE.letterDescription,
        Header: t(I18N_LETTER_CONFIG.LETTER_CONFIG_DESCRIPTION),
        isSort: true,
        accessor: 'letterDescription'
      },
      {
        id: 'action',
        Header: <div className="text-center">{t(I18N_COMMON_TEXT.ACTION)}</div>,
        accessor: renderActions,
        width: 93,
        cellProps: { className: 'text-center' },
        cellBodyProps: { className: 'td-sm' }
      }
    ];
    return allColumns.filter(column => {
      if (column.id === 'action') {
        return checkPermission(PERMISSIONS.LETTER_SEND_LETTER, storeId);
      }
      return true;
    });
  }, [storeId, t]);

  const {
    total,
    gridData,
    pageSize,
    currentPage,
    onPageChange,
    onPageSizeChange
  } = usePagination(letters);

  const showPagination = total > pageSize[0];

  if (isEmpty(letters)) {
    if (loading) return null;
    return (
      <div className="text-center my-80">
        <Icon name="file" className="fs-80 color-light-l12" />
        <p
          className="mt-20 text-secondary"
          data-testid={genAmtId('letterList', 'no-letter', '')}
        >
          {t(I18N_COMMON_TEXT.NO_LETTERS_TO_DISPLAY)}
        </p>
      </div>
    );
  }

  return (
    <>
      <Grid
        dataTestId="letterGrid"
        columns={columns}
        data={gridData}
        onSortChange={handleChangeSort}
        sortBy={[sortBy]}
      />
      {showPagination && (
        <div className="pt-16">
          <Pagination
            dataTestId="letterGrid_pagination"
            totalItem={total}
            pageSize={pageSize}
            pageNumber={currentPage}
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
          />
        </div>
      )}
    </>
  );
};

export default LetterGrid;
