import { apiService } from 'app/utils/api.service';

// types
import { SendLetterPostData } from './types';

const letterListService = {
  getLetters: (postData: SendLetterPostData) => {
    const url = window.appConfig?.api?.sendLetter?.getLetters || '';

    return apiService.post(url, { ...postData });
  }
};

export default letterListService;
