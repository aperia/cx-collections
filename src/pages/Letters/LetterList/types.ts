import { SortType } from 'app/_libraries/_dls/components';
import { Letter } from '../SendLetter/types';

export interface UpdateSortBy {
  storeId: string;
  sortBy: SortType;
}

export interface SendLetterPostData {
  common: {
    org: string;
    app: string;
  };
}

export interface SendLetterArg {
  storeId: string;
}

export interface LettersData {
  letterId: string;
  letterDescription: string;
}

export interface SendLetterPayload {
  data: {
    letters: LettersData[];
  };
}

export interface LetterDetailData extends Letter, MagicKeyValue {}

export interface LetterListData {
  letterGroup: string;
  letterDetails: LetterDetailData[];
}

export interface Letters {
  loading?: boolean;
  error?: boolean;
  noData?: boolean;
  data?: LetterDetailData[];
  total?: number;
  pageSize?: number;
  page?: number;
  sortBy?: SortType;
}

export interface LetterList {
  openModal?: boolean;
  activeKey?: string;
  letters?: Letters;
}

export interface LetterListInitialState {
  [storeId: string]: LetterList;
}
