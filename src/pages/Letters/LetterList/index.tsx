import React, { useEffect, useLayoutEffect, useRef } from 'react';
import classNames from 'classnames';

// components
import { InlineMessage } from 'app/_libraries/_dls/components';
import LetterGrid from './Grid';
import { FailedApiReload } from 'app/components';

// Const
import { disabledSendBasedDestinationCode } from './constants';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { I18N_PENDING_LETTER } from 'app/constants/i18n';

// redux
import { useDispatch } from 'react-redux';
import { letterListAction } from './_redux/reducers';
import { selectErrorLetter, selectLoadingLetter } from './_redux/selectors';
import { takeLoadingVariablesStatus } from 'pages/Letters/SendLetter/_redux/selectors';
import { takeDestinationCode } from 'pages/AccountDetails/_redux/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';
import ApiErrorForHeaderSection from 'pages/ClientConfiguration/ApiErrorSection/ApiErrorForHeaderSection';

export interface LetterListProps {}

const LetterList: React.FC<LetterListProps> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId, accEValue } = useAccountDetail();
  const divRef = useRef<HTMLDivElement | null>(null);

  const loading = useStoreIdSelector<boolean>(selectLoadingLetter);
  const loadingVariables = useStoreIdSelector<boolean>(
    takeLoadingVariablesStatus
  );
  const isError = useStoreIdSelector<boolean>(selectErrorLetter);
  const destinationCode = useStoreIdSelector(takeDestinationCode);

  const handleReload = () =>
    dispatch(
      letterListAction.getLetter({
        storeId
      })
    );

  useEffect(() => {
    dispatch(
      letterListAction.getLetter({
        storeId
      })
    );
  }, [accEValue, dispatch, storeId]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.minHeight = '500px');
  }, []);

  return (
    <div
      className={classNames(
        { loading: loading || loadingVariables },
        'px-24 pt-24'
      )}
      ref={divRef}
    >
      <h5 className="pb-16" data-testid={genAmtId('letterList', 'title', '')}>
        {t('txt_letter_list')}
      </h5>
      <ApiErrorForHeaderSection className="mt-24 ml-24" />
      {destinationCode === disabledSendBasedDestinationCode ? (
        <InlineMessage
          variant="warning"
          withIcon
          dataTestId="letterList_warning"
        >
          {t(I18N_PENDING_LETTER.RETURN_MAIL_FLAG)}
        </InlineMessage>
      ) : null}
      {isError ? (
        <FailedApiReload
          id="account-detail-overview-fail"
          dataTestId="letterList_fail"
          className="mt-80"
          onReload={handleReload}
        />
      ) : (
        <LetterGrid />
      )}
    </div>
  );
};

export default LetterList;
