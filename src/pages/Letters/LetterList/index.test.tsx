import React from 'react';
import { AccountDetailProvider } from 'app/hooks';
import { screen } from '@testing-library/react';

// utils
import {
  renderMockStore,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';

// components
import LetterList from './index';

// redux
import { letterListAction as actions } from './_redux/reducers';

const initialState: Partial<RootState> = {
  letterList: {
    [storeId]: {
      letters: {}
    }
  },
  accountDetail: {
    [storeId]: {
      data: {
        nextHeldStatementDestinationCode: 'W'
      }
    }
  }
};

const errorState: Partial<RootState> = {
  letterList: { [storeId]: { letters: { error: true, noData: true } } }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <LetterList />
    </AccountDetailProvider>,
    {
      initialState
    }
  );
};

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper(initialState);

    expect(screen.getByText('txt_letter_list')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleReload', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    jest.spyOn(actions, 'getLetter').mockImplementation(mockAction);

    renderWrapper(errorState);
    const reloadBtn = screen.getByRole('button');
    reloadBtn.click();

    expect(mockAction).toBeCalledTimes(2);
  });
});
