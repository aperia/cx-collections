import React from 'react';
import { render, screen } from '@testing-library/react';

import SendLetter from './index';

import { ViewProps } from 'app/_libraries/_dof/core';
import { sendLetterActions } from 'pages/Letters/SendLetter/_redux/reducers';
import * as sendLetterSelectors from 'pages/Letters/SendLetter/_redux/selectors';
import { SEND_LETTER } from './constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

const mockStoreId = 'mock-store-id';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => () => ({
  t: jest.fn((x, options) => {
    if (options?.count > 1) return `${x}_plural`;
    return x;
  })
}));

jest.mock('react-redux', () => {
  return {
    ...(jest.requireActual('react-redux') as object),
    useSelector: (x: Function) => jest.fn().mockImplementation(() => x()),
    useDispatch: () => (x: Function) => jest.fn().mockImplementation(() => x())
  };
});

jest.mock('redux-form', () => ({
  ...(jest.requireActual('redux-form') as object),
  isValid: jest.fn().mockImplementation(() => true),
  getFormValues: jest.fn().mockResolvedValue({})
}));

jest.mock(
  'app/_libraries/_dof/core/View',
  () =>
    ({ descriptor }: ViewProps) =>
      <div>{descriptor}</div>
);

jest.mock('app/hooks', () => ({
  useAccountDetail: () => ({ storeId: mockStoreId }),
  useStoreIdSelector: (x: Function) => x()
}));

jest.mock('./SendLetterContent', () => ({
  ...(jest.requireActual('./SendLetterContent') as object),
  SendLetterContent: () => <div>SendLetterContent</div>
}));

beforeAll(() => {
  jest.spyOn(sendLetterSelectors, 'takeLoadingSendLetterStatus');
  jest.spyOn(sendLetterSelectors, 'takeOpenStatus');
});

const Component = <SendLetter />;

describe('Send Letter', () => {
  it('should render null when no variables', () => {
    const { container } = render(Component);
    expect(container.firstChild).toBe(null);
  });

  it('should render modal with SEND_LETTER.LETTER_CAPTION title', () => {
    jest
      .spyOn(sendLetterSelectors, 'takeOpenStatus')
      .mockImplementation(() => true);
    render(Component);
    expect(screen.getByText(SEND_LETTER.LETTER_CAPTION)).toBeInTheDocument();
  });

  it('should close modal', () => {
    const mockToggleSendLetter = jest.fn();
    jest
      .spyOn(sendLetterActions, 'toggleSendLetter')
      .mockImplementation(mockToggleSendLetter);
    jest
      .spyOn(sendLetterSelectors, 'takeOpenStatus')
      .mockImplementation(() => true);
    render(Component);
    const closeButton = screen.getByText(I18N_COMMON_TEXT.CLOSE);
    closeButton.click();
    expect(mockToggleSendLetter).toBeCalled();
  });

  it('should trigger send letter', () => {
    const mockInvokeSendingLetter = jest.fn();
    jest
      .spyOn(sendLetterActions, 'invokeSendingLetter')
      .mockImplementation(mockInvokeSendingLetter);
    jest
      .spyOn(sendLetterSelectors, 'takeOpenStatus')
      .mockImplementation(() => true);
    render(Component);
    const okButton = screen.getByText(I18N_COMMON_TEXT.SEND);
    okButton.click();
    expect(mockInvokeSendingLetter).toBeCalled();
  });
});
