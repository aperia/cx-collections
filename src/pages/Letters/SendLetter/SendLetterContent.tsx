import React, { Fragment, useEffect } from 'react';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// Components/types
import Label from 'app/components/Label';
import { Letter } from './types';
import {
  App,
  fieldRegistry,
  View,
  viewRegistry
} from 'app/_libraries/_dof/core';

// I18n
import { SEND_LETTER, viewName } from './constants';

// Redux
import {
  takeLetter,
  takeVariables,
  takeVariablesData
} from 'pages/Letters/SendLetter/_redux/selectors';
import { useDispatch } from 'react-redux';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { useApiErrorNotification } from 'pages/ApiErrorNotification/hooks/useApiErrorNotification';

export interface SendLetterContentProps {}

export const SendLetterContent: React.FC<SendLetterContentProps> = React.memo(
  () => {
    const dispatch = useDispatch();
    const { storeId } = useAccountDetail();
    const { t } = useTranslation();
    const ApiErrorDetail = useApiErrorNotification({
      storeId,
      forSection: 'inSendLetterWithVariableModal'
    });

    const letter = useStoreIdSelector<Letter>(takeLetter);
    const variablesData = useStoreIdSelector<MagicKeyValue>(takeVariablesData);
    const variables = useStoreIdSelector<Array<MagicKeyValue>>(takeVariables);

    const renderSnapshot = () => {
      return (
        <div className="bg-light-l20 p-24 border-bottom">
          <div className="row ">
            <div className="col-3">
              <Label
                id={`sentLetter-letter-snapshot-letterNumber-${storeId}`}
                dataTestId="sendLetterContent_letter-number"
                value={letter.letterId}
                label={t(SEND_LETTER.LETTER_NUMBER)}
              />
            </div>
            <div className="col-9">
              <Label
                id={`sentLetter-letter-snapshot-description-${storeId}`}
                dataTestId="sendLetterContent_description"
                value={letter.letterDescription}
                label={t(SEND_LETTER.DESCRIPTION)}
              />
            </div>
          </div>
        </div>
      );
    };

    const renderVariables = () => {
      if (!variables?.length) return null;
      return (
        <div className="px-24 mt-24">
          <h5
            data-testid={genAmtId(
              'sendLetterContent',
              'variables',
              'SendLetterContent'
            )}
          >
            {t(SEND_LETTER.VARIABLES)}
          </h5>
          <App urlConfigs={[]}>
            <View
              id={`${viewName}_${storeId}`}
              formKey={`${viewName}_${storeId}`}
              descriptor={viewName}
              value={variablesData}
            />
          </App>
        </div>
      );
    };

    useEffect(() => {
      // clear api error when close modal
      return () => {
        dispatch(
          apiErrorNotificationAction.clearApiErrorData({
            forSection: 'inSendLetterWithVariableModal',
            storeId
          })
        );
      };
    }, [dispatch, storeId]);

    useEffect(() => {
      if (!variables?.length) return;

      const views: MagicKeyValue = {
        name: viewName,
        isCached: false,
        layout: {
          name: 'BootstrapGridLayout'
        },
        fields: variables
      };

      fieldRegistry.unregisterField(variables as never);

      // Register field and view into DOF
      fieldRegistry.registerFields(variables as never);
      viewRegistry.registerView(viewName, views);
    }, [variables, variablesData]);

    return (
      <Fragment>
        {renderSnapshot()}
        <ApiErrorDetail
          dataTestId="sendLetterContent_load-error"
          className="my-24 ml-24"
        />
        {renderVariables()}
      </Fragment>
    );
  }
);
