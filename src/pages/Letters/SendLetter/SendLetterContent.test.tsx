import React from 'react';

// components
import { SendLetterContent } from './SendLetterContent';

// RTL
import { screen, waitFor, fireEvent } from '@testing-library/react';

// helpers
import { renderMockStoreId, storeId, mockActionCreator } from 'app/test-utils';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { Letter } from './types';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => () => ({
  t: jest.fn((x, options) => {
    if (options?.count > 1) return `${x}_plural`;
    return x;
  })
}));

jest.mock('app/helpers', () => {
  const helpers = jest.requireActual('app/helpers');
  return {
    ...helpers,
    getScrollParent: () => ({
      scrollTo: jest.fn()
    })
  };
});

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<SendLetterContent />, { initialState });
};

const initialState: Partial<RootState> = {
  apiErrorNotification: {
    [storeId]: {
      errorDetail: {
        inSendLetterWithVariableModal: {
          response: '500',
          request: '500'
        }
      }
    }
  },
  sendLetter: {
    [storeId]: {
      letter: {
        letterId: '123456',
        letterDescription: 'des'
      } as Letter,
      variablesData: {
        key: '1',
        value: '123'
      },
      variables: [
        {
          key: '1',
          value: '123'
        },
        {
          key: '1',
          value: '123'
        }
      ]
    }
  }
};

const apiErrorNotificationActionSpy = mockActionCreator(
  apiErrorNotificationAction
);

describe('Send Letter', () => {
  beforeEach(() => {
    jest.useFakeTimers();
    apiErrorNotificationActionSpy('openApiDetailErrorModal');
  });

  afterEach(() => {
    jest.runAllTimers();
    jest.clearAllMocks();
  });

  it('should render component', () => {
    renderWrapper({
      sendLetter: {
        [storeId]: {
          letter: {
            letterId: '123456',
            letterDescription: 'des'
          } as Letter
        }
      }
    });

    expect(screen.getByText('txt_letter_number')).toBeInTheDocument();
    expect(screen.getByText('txt_description')).toBeInTheDocument();
  });

  it('should render component with empty variables', () => {
    renderWrapper({
      apiErrorNotification: {
        [storeId]: {
          errorDetail: {
            inSendLetterWithVariableModal: {
              response: '500',
              request: '500'
            }
          }
        }
      },
      sendLetter: {
        [storeId]: {
          letter: {
            letterId: '123456',
            letterDescription: 'des'
          } as Letter,
          variablesData: {
            key: '1',
            value: '123'
          },
          variables: []
        }
      }
    });

    expect(screen.getByText('txt_letter_number')).toBeInTheDocument();
    expect(screen.getByText('txt_description')).toBeInTheDocument();
  });

  it('render component with data', () => {
    waitFor(() => {
      const { wrapper } = renderWrapper(initialState);
      expect(wrapper.getByText('txt_letter_number')).toBeInTheDocument();
    });
  });

  it('handleClickErrorDetail', () => {
    const openApiDetailErrorModalSpy = apiErrorNotificationActionSpy(
      'openApiDetailErrorModal'
    );
    waitFor(() => {
      renderWrapper(initialState);
    });

    const button = screen.getByText('txt_view_detail');

    fireEvent.click(button!);
    // //Trigger Button
    expect(openApiDetailErrorModalSpy).toBeCalledWith({
      forSection: 'inSendLetterWithVariableModal',
      storeId
    });
  });
});
