import { SendLetterConfiguration } from 'app/global-types/letter';

export interface GetVariablesArgs
  extends StoreIdPayload,
    CommonAccountIdRequestBody {
  letter?: Letter;
}

export interface SendLetterArgs
  extends StoreIdPayload,
    CommonAccountIdRequestBody {
  letter?: Letter;
  data?: MagicKeyValue;
}

export interface TriggerSendLetterArgs
  extends StoreIdPayload,
    CommonAccountIdRequestBody {
  letter?: Letter;
}

export interface ToggleSendLetterPayload extends StoreIdPayload {
  letterNumber?: string;
}
export interface SaveLetterData extends StoreIdPayload {
  letter?: Letter;
}

export interface SendLetterState {
  letter?: Letter;
  variablesData?: MagicKeyValue;
  variables?: Array<MagicKeyValue>;
  isOpenLetter?: boolean;
  isLoadingVariables?: boolean;
  isLoadingSendLetter?: boolean;
  isError?: boolean;
}

export interface SendLetterStateResult {
  [storeId: string]: SendLetterState;
}

export interface Letter {
  letterId: string;
  letterDescription: string;
  letterCode: string;
  variableLetter: boolean;
  captionMapping?: MagicKeyValue[];
}

export interface SendLetterRequest
  extends CommonAccountIdRequestBody,
    MagicKeyValue {
  letterIdentifier?: string;
}

export interface GetLetterVariablesRequest extends CommonAccountIdRequestBody {
  letterNumber?: string;
  letterTypeCode?: string;
  customerInquiryFields?: {
    selectFields: Array<string>;
  };
  universalAddressInquiryFields?: {
    selectFields?: Array<string>;
  };
}

export type SendLetterPopulateData = Record<string, string>;

// Letter variables
export interface LetterCaption {
  optionalNameOrAddressCode?: string;
  alternateAddressOneRequestIndicator?: string;
  alternateAddressThreeRequestIndicator?: string;
  optionalVariableCaptionOne?: string;
  optionalVariableCaptionTwo?: string;
  optionalVariableCaptionThree?: string;
  optionalVariableCaptionFive?: string;
  optionalVariableCaptionSix?: string;
  optionalVariableCaptionSeven?: string;
  optionalVariableCaptionEight?: string;
  optionalVariableCaptionNine?: string;
  optionalVariableCaptionTen?: string;
}

export type LetterField = keyof LetterCaption;

export type LetterCaptionsList = Array<LetterCaption>;

// Customer
export type ListCustomer = Array<SendLetterPopulateData>;

// Address
export type ListAddress = Array<SendLetterPopulateData>;

// Response
export interface GetLetterVariablesResponse {
  letterCaptionsResponse?: {
    letterCaptionsList?: LetterCaptionsList;
  };
  customerInquiryResponse?: {
    customers?: Array<MagicKeyValue>;
  };
  universalAddressInquiryResponse?: {
    primary?: Array<MagicKeyValue>;
  };
  letterConfig: SendLetterConfiguration;
  status?: string;
  message?: string;
}

export interface DynamicVariable {
  dataField?: string;
  label?: string;
  name?: string;
  mappingField?: string;
  props?: { required?: boolean; readOnly?: boolean };
  options?: MagicKeyValue;
  validationRules?: Array<MagicKeyValue>;
  type?: string;
  layoutProps?: { className?: string };
  format?: string;
  isAmount?: boolean;
}
