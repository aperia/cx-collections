import isUndefined from 'lodash.isundefined';
import dateTime from 'date-and-time';

// Helper
import { capitalizeWord } from 'app/helpers';

// Constant
import {
  FIELD_TYPE,
  isEnableFieldNameOrAddress,
  kyMapping,
  RULE_NAME
} from './constants';
import { FormatTime } from 'app/constants/enums';
import { EMPTY_OBJECT, EMPTY_STRING, SLASH, SPACE } from 'app/constants';

// Type
import {
  LetterCaptionsList,
  LetterCaption,
  LetterField,
  DynamicVariable
} from './types';
import { Field, SendLetterConfiguration } from 'app/global-types/letter';

const isNumberVariable = (
  labelField: string,
  fieldType: SendLetterConfiguration['fieldType'] = {}
): boolean => {
  return !!fieldType?.currency?.find(fieldItem =>
    labelField.toLocaleUpperCase().includes(fieldItem)
  );
};

const isDateVariable = (
  labelField: string,
  fieldType: SendLetterConfiguration['fieldType'] = {}
): boolean => {
  return !!fieldType?.date?.find(fieldItem =>
    labelField.toLocaleUpperCase().includes(fieldItem)
  );
};

/**
 * Generate variables based on API response,
 * we create list control to generate dynamic field
 * @param captions list field return by API
 * @returns list dynamic variables
 */
export const generateLetterVariables = (
  captions: LetterCaptionsList,
  sendLetterConfig: SendLetterConfiguration = {}
): DynamicVariable[] => {
  let fieldsData: Array<DynamicVariable> = [];

  // Get configuration on json file
  const {
    populateFields = [],
    combineTwoFields = [],
    additionalOptionsConfig = [],
    mappingDynamicKys = {}
  } = sendLetterConfig;

  const getAdditionalOptionsConfig = (label: string) => {
    return (
      additionalOptionsConfig.find(item =>
        label?.includes(item?.optionsName || EMPTY_STRING)
      ) || {}
    );
  };

  /**
   * Get mapping keys which is using in payload in case optionalNameOrAddressCode === Y
   * If there is has any special case config in mapping field, it will take key in config
   * @param label
   */
  const getPopulateField = (label: string): Field => {
    const { fieldName = EMPTY_STRING, ky: populateFieldName = EMPTY_STRING } =
      populateFields.find(item => item.includesLabel?.includes(label)) || {};

    const { additionalOptions = {}, additionalValidations = [] } =
      getAdditionalOptionsConfig(label);

    return {
      fieldName,
      populateFieldName,
      additionalOptions,
      additionalValidations
    };
  };

  /**
   * Get mapping keys which is using in payload in case optionalNameOrAddressCode === N
   * @param label
   * @param captionName
   * @returns
   */
  const getDynamicField = (label: string, captionName: string): Field => {
    const { additionalOptions = {}, additionalValidations = [] } =
      combineTwoFields.includes(label) ? {} : getAdditionalOptionsConfig(label);
    return {
      fieldName: mappingDynamicKys[captionName],
      populateFieldName: mappingDynamicKys[captionName],
      additionalOptions,
      additionalValidations
    };
  };

  /**
   * Create list dynamic field for each item of letter caption
   * @param item data return by API
   */
  const createFieldFromCaption = (
    item: LetterCaption,
    isEnableField: boolean
  ): Array<DynamicVariable> => {
    return Object.keys(item).reduce(
      (fields: Array<DynamicVariable>, captionId: string) => {
        const captionName = captionId as LetterField;
        const labelField =
          item[captionName]?.trim()?.toLocaleUpperCase() || EMPTY_STRING;

        if (
          !labelField.length ||
          labelField === item.optionalNameOrAddressCode
        ) {
          return fields;
        }

        /**
         * If the flag - optionalNameOrAddressCode is 'Y' we will using the mapping populate field,
         * otherwise we using mapping dynamic key and not populate data
         */
        const {
          fieldName,
          populateFieldName,
          additionalValidations,
          additionalOptions
        } = isEnableField
          ? getPopulateField(labelField)
          : getDynamicField(labelField, captionName);

        // Check if the label contain the combine field, then split it to two field
        if (combineTwoFields?.includes(labelField) && isEnableField) {
          // Dissection the combine field based on slash character
          const listField = labelField.includes(SLASH)
            ? labelField.split(SLASH)
            : labelField.split(SPACE);

          const combineFields = listField.map(name =>
            createFieldFromCaption(
              {
                [`${populateFieldName}${kyMapping}${name}`]: name
              },
              isEnableField
            )
          );

          return fields.concat(...combineFields);
        }

        // Capitalize for the sentence support for the label of field on UI
        const customLabel = capitalizeWord(labelField);

        let field: DynamicVariable = {
          dataField: populateFieldName,
          label: customLabel,
          name: fieldName,
          mappingField: populateFieldName,
          props: {
            required: true
          },
          options: additionalOptions,
          validationRules: [
            {
              errorMsg: `${customLabel} is required.`,
              ruleName: RULE_NAME.REQUIRED
            },
            ...additionalValidations!
          ],
          type: FIELD_TYPE.TEXT,
          isAmount: false,
          layoutProps: { className: 'col-lg-4 col-6 mt-16' }
        };

        if (isNumberVariable(labelField, sendLetterConfig?.fieldType)) {
          field = { ...field, isAmount: true };
        }

        if (isDateVariable(labelField, sendLetterConfig?.fieldType)) {
          field = {
            ...field,
            type: FIELD_TYPE.DATE
          };
        }

        return fields.concat(field);
      },
      []
    );
  };

  captions.forEach(caption => {
    const isEnableNameAddress =
      caption.optionalNameOrAddressCode === isEnableFieldNameOrAddress;

    fieldsData = [
      ...fieldsData,
      ...createFieldFromCaption(caption, isEnableNameAddress)
    ];
  });

  return fieldsData;
};

/**
 * Function generate populate data for dynamic variables
 * @param variables an array dynamic variables
 * @param customers the data for customer
 * @param addresses the data for address
 * @returns populate data
 */
export const generateLetterData = (
  variables: Array<DynamicVariable>,
  customer: MagicKeyValue | undefined = EMPTY_OBJECT,
  address: MagicKeyValue | undefined = EMPTY_OBJECT
) => {
  // Populate data for dynamic variables
  const generateData = (item: Record<string, string>) => {
    return Object.keys(item).reduce(
      (fields: MagicKeyValue, captionName: string) => {
        // Find the variable that matched with the caption name based on label key
        const field = variables.find(fieldItem => {
          const { mappingField = EMPTY_STRING } = fieldItem;
          return mappingField === captionName;
        });

        if (isUndefined(field)) {
          return fields;
        }

        const { dataField = EMPTY_STRING } = field;

        // Remove whitespace and check if value of item is empty or not, if empty return it to undefined
        const value =
          item[captionName].trim().length > 0
            ? item[captionName].trim()
            : undefined;

        fields = { ...fields, [dataField]: value };

        return fields;
      },
      {}
    );
  };

  return {
    ...generateData(customer),
    ...generateData(address)
  };
};

/**
 * Build custom data, merge combine field to one field business
 * @param data
 * @returns custom data
 */
export const buildCustomData = (
  data: MagicKeyValue,
  variables: Array<DynamicVariable>,
  sendLetterConfig: SendLetterConfiguration = {}
) => {
  const customData: Record<string, string> = {};

  const listKeyCombine: Record<string, Array<string>> = Object.keys(
    data
  ).reduce((group: Record<string, Array<string>>, item: string) => {
    let value = data[item];
    const currentVariable = variables.find(variable => variable.name === item);
    if (
      isDateVariable(
        currentVariable?.label || EMPTY_STRING,
        sendLetterConfig?.fieldType
      )
    ) {
      value = dateTime.format(new Date(value), FormatTime.DefaultPostDate);
    }

    if (
      isNumberVariable(
        currentVariable?.label || EMPTY_STRING,
        sendLetterConfig?.fieldType
      )
    ) {
      value = Number(value);
    }

    if (!item.includes(kyMapping)) {
      // If item not include the key mapping
      customData[item] = value;
      return group;
    }

    const [originalKy] = item.split(kyMapping);
    if (group[originalKy]) {
      group[originalKy] = [...group[originalKy], value];
    } else {
      group[originalKy] = [value];
    }

    return group;
  }, {});

  Object.keys(listKeyCombine).forEach(item => {
    customData[item] = listKeyCombine[item].join(SLASH);
  });

  return customData;
};
