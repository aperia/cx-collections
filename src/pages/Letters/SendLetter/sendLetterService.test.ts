import sendLetterService from './sendLetterService';

// utils
import { mockApiServices, storeId } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import { GetLetterVariablesRequest, SendLetterRequest } from './types';

describe('sendLetterService', () => {
  describe('sendLetter', () => {
    const params: SendLetterRequest = {
      letterIdentifier: 'id'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      sendLetterService.sendLetter(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      sendLetterService.sendLetter(params);

      expect(mockService).toBeCalledWith(apiUrl.letter.sendLetter, params);
    });
  });

  describe('getLetterWithVariables', () => {
    const params: GetLetterVariablesRequest = {
      common: { accountId: storeId },
      letterNumber: '6464'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      sendLetterService.getLetterWithVariables(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      sendLetterService.getLetterWithVariables(params);

      expect(mockService).toBeCalledWith(apiUrl.letter.letterInquiry, params);
    });
  });
});
