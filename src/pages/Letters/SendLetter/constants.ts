export enum SEND_LETTER {
  LETTER_NUMBER = 'txt_letter_number',
  DESCRIPTION = 'txt_description',
  VARIABLES = 'txt_variables',
  LETTER_CAPTION = 'txt_letter_captions',
  SENT_SUCCESS = 'txt_letter_sent_success',
  SENT_FAIL = 'txt_letter_sent_fail'
}

export enum FIELD_TYPE {
  NUMERIC = 'NumericControl',
  DATE = 'DatePickerControl',
  TEXT = 'TextBoxControl'
}

export enum RULE_NAME {
  REQUIRED = 'Required'
}

export enum SEND_LETTER_LANG {
  REQUIRED_MSG = 'txt_required_send_letter_msg'
}

export const letterTypeCode = 'C';

export const whiteSpaceRegex = /\s+/g;
export const isEnableFieldNameOrAddress = 'Y';
export const kyMapping = '_with_';
export const CONFIG_SEND_LETTER_URL = 'config/sendLetter.json';
export const viewName = 'letterCaptionWithVariables';
