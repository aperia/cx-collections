import { Letter } from '../types';
import {
  takeOpenStatus,
  takeLetter,
  takeVariablesData,
  takeVariables,
  takeLoadingSendLetterStatus,
  takeLoadingVariablesStatus
} from './selectors';

const mockStoreId = 'mock-store-id';
const mockLetterActive = 'mock-letter-active';
const mockLetter = 'mock-letter' as Letter;
const mockVariablesData = ('mock-variables-data' as unknown) as MagicKeyValue;
const mockVariables = 'mock-variables';
const mockLoadingSendLetterStatus = true;
const mockLoadingVariablesStatus = true;
const mockIsOpenLetter = false;

const storeFixture = ({
  sendLetter: {
    [mockStoreId]: {
      isOpenLetter: mockIsOpenLetter,
      letter: mockLetter,
      variablesData: mockVariablesData,
      letterActive: mockLetterActive,
      variables: mockVariables,
      isLoadingSendLetter: mockLoadingSendLetterStatus,
      isLoadingVariables: mockLoadingVariablesStatus
    }
  }
} as unknown) as RootState;

describe('selectors', () => {
  it('takeOpenStatus', () => {
    const results = takeOpenStatus(storeFixture, mockStoreId);
    expect(results).toEqual(mockIsOpenLetter);
  });

  it('take Letter', () => {
    const results = takeLetter(storeFixture, mockStoreId);
    expect(results).toEqual(mockLetter);
  });

  it('takeVariablesData', () => {
    const results = takeVariablesData(storeFixture, mockStoreId);
    expect(results).toEqual(mockVariablesData);
  });

  it('takeVariablesData', () => {
    const results = takeVariables(storeFixture, mockStoreId);
    expect(results).toEqual(mockVariables);
  });

  it('takeLoadingSendLetterStatus', () => {
    const results = takeLoadingSendLetterStatus(storeFixture, mockStoreId);
    expect(results).toEqual(mockLoadingSendLetterStatus);
  });

  it('takeLoadingVariablesStatus', () => {
    const results = takeLoadingVariablesStatus(storeFixture, mockStoreId);
    expect(results).toEqual(mockLoadingVariablesStatus);
  });
});
