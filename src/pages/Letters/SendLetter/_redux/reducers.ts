import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import isUndefined from 'lodash.isundefined';

// Types
import { SendLetterStateResult, ToggleSendLetterPayload } from '../types';

// Thunk
import {
  sendLetterConfig,
  sendLetterBuilder,
  triggerSendLetter,
  invokeSendingLetter
} from './sendLetter';
import {
  getLetterWithVariables,
  getLetterWithVariablesBuilder
} from './getLetterWithVariables';

const initialState: SendLetterStateResult = {};
const { actions, reducer } = createSlice({
  initialState,
  name: 'sendLetter',
  reducers: {
    toggleSendLetter: (
      draftState,
      action: PayloadAction<ToggleSendLetterPayload>
    ) => {
      const { storeId } = action.payload;
      const { isOpenLetter } = draftState[storeId];
      const status = isUndefined(isOpenLetter) ? true : !isOpenLetter;
      draftState[storeId] = {
        ...draftState[storeId],
        isOpenLetter: status
      };
    },
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    }
  },
  extraReducers: builder => {
    getLetterWithVariablesBuilder(builder);
    sendLetterBuilder(builder);
  }
});

const sendLetterActions = {
  ...actions,
  getLetterWithVariables,
  sendLetterConfig,
  triggerSendLetter,
  invokeSendingLetter
};

export { sendLetterActions, reducer };
