import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// Types
import { GetLetterVariablesResponse } from '../types';
import { GetVariablesArgs, SendLetterStateResult } from '../types';
import { SendLetterConfiguration } from 'app/global-types/letter';

// Service
import sendLetterService from '../sendLetterService';

// Helper
import { generateLetterData, generateLetterVariables } from '../helpers';
import { getFeatureConfig } from 'app/helpers';

// Constant
import { CONFIG_SEND_LETTER_URL } from '../constants';
import { CUSTOMER_ROLE_CODE, EMPTY_OBJECT, FAILURE_MSG } from 'app/constants';

import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export const getLetterWithVariables = createAsyncThunk<
  GetLetterVariablesResponse,
  GetVariablesArgs,
  ThunkAPIConfig
>('letter/getLetterWithVariables', async (args, thunkAPI) => {
  const { common, letter, storeId } = args;
  const { dispatch } = thunkAPI;
  try {
    const letterConfig = await getFeatureConfig<SendLetterConfiguration>(
      CONFIG_SEND_LETTER_URL
    );

    // Get configuration on json file
    const { customerSelectFields = [], addressSelectFields = [] } =
      letterConfig || {};

    const response = await sendLetterService.getLetterWithVariables({
      common,
      customerInquiryFields: {
        selectFields: customerSelectFields
      },
      universalAddressInquiryFields: {
        selectFields: addressSelectFields
      },
      letterNumber: letter?.letterId,
      letterTypeCode: letter?.letterCode
    });

    const { data } = response;

    if (data?.status === FAILURE_MSG) {
      throw response;
    }

    dispatch(
      apiErrorNotificationAction.clearApiErrorData({
        storeId,
        forSection: 'inModalBody'
      })
    );

    return {
      ...data,
      letterConfig
    };
  } catch (error) {
    dispatch(
      apiErrorNotificationAction.updateApiError({
        storeId,
        forSection: 'inModalBody',
        apiResponse: error
      })
    );
    return thunkAPI.rejectWithValue(EMPTY_OBJECT);
  }
});

export const getLetterWithVariablesBuilder = (
  builder: ActionReducerMapBuilder<SendLetterStateResult>
) => {
  builder
    .addCase(getLetterWithVariables.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        variablesData: EMPTY_OBJECT,
        isLoadingVariables: true
      };
    })
    .addCase(getLetterWithVariables.fulfilled, (draftState, action) => {
      const { storeId, letter } = action.meta.arg;

      const {
        letterCaptionsResponse,
        customerInquiryResponse,
        universalAddressInquiryResponse,
        letterConfig = {}
      } = action.payload;
      const { letterCaptionsList = [] } = letterCaptionsResponse || {};
      const { customers = [] } = customerInquiryResponse || {};
      const { primary: primaries = [] } = universalAddressInquiryResponse || {};

      const customer = customers.find(
        item => item?.customerRoleTypeCode === CUSTOMER_ROLE_CODE.primary
      );
      const primary = primaries.find(
        item => item?.customerRoleTypeCode === CUSTOMER_ROLE_CODE.primary
      );

      letterConfig.mappingDynamicKys = letter?.captionMapping;

      const variables = generateLetterVariables(
        letterCaptionsList,
        letterConfig
      );

      const variablesData = generateLetterData(variables, customer, primary);

      draftState[storeId] = {
        ...draftState[storeId],
        variables,
        variablesData,
        isLoadingVariables: false,
        letter: letter,
        isError: false
      };
    })
    .addCase(getLetterWithVariables.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingVariables: false,
        isError: true
      };
    });
};
