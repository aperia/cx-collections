import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import {
  invokeSendingLetter,
  triggerSendLetter,
  sendLetterConfig
} from './sendLetter';
import { sendLetterActions } from './reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import sendLetterService from '../sendLetterService';
import {
  accNbr,
  mockActionCreator,
  responseDefault,
  storeId
} from 'app/test-utils';
import * as commonActions from 'app/helpers/commons';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { FIELD_TYPE, SEND_LETTER } from '../constants';
import { SendLetterState } from '../types';

import * as sendLetterHelpers from '../helpers';

const mockData = {
  storeId,
  common: {
    accountId: accNbr
  },
  inquiryFields: {
    memberSequenceIdentifier: 'member',
    customerRoleTypeCode: 'type'
  },
  selectFields: ['123', '123'],
  isCallRealApi: true,
  data: {},
  sendLetter: {
    [storeId]: {
      letter: {
        letterNumber: '123',
        description: 'des'
      },
      variablesData: {
        key: '1',
        value: '123'
      },
      variables: [
        {
          key: '1',
          value: '123'
        },
        {
          key: '1',
          value: '123',
          isAmount: true
        },
        {
          dataField: 'field1',
          type: FIELD_TYPE.DATE
        }
      ]
    }
  }
};

const store = createStore(
  rootReducer,
  {
    sendLetter: {
      [storeId]: {
        isOpenLetter: true,
        variablesData: {
          key: '1',
          value: '123'
        },
        variables: [
          {
            key: '1',
            value: '123'
          },
          {
            key: '1',
            value: '123',
            dataField: 'amountField',
            isAmount: true
          },
          {
            dataField: 'field1',
            type: FIELD_TYPE.DATE
          }
        ]
      }
    },
    accountDetail: {
      [storeId]: {
        data: {
          primaryName: 'name'
        }
      }
    }
  },
  applyMiddleware(thunk)
);

const errorStore = createStore(
  rootReducer,
  {
    sendLetter: {
      [storeId]: {
        variablesData: undefined
      }
    }
  },
  applyMiddleware(thunk)
);

const toastSpy = mockActionCreator(actionsToast);
const memoSpy = mockActionCreator(memoActions);
const apiErrorNotificationActionSpy = mockActionCreator(
  apiErrorNotificationAction
);
const sendLetterSpy = mockActionCreator(sendLetterActions);

describe('Test SendLetter', () => {
  beforeEach(() => {
    jest.spyOn(commonActions, 'getFeatureConfig').mockResolvedValue({
      ...responseDefault,
      customerSelectFields: []
    });
    jest.useFakeTimers();
  });

  afterEach(() => {
    jest.runAllTimers();
    jest.clearAllMocks();
  });

  it('sendLetterConfig', async () => {
    jest.spyOn(sendLetterHelpers, 'buildCustomData').mockReturnValue({
      field1: '123',
      textField: '123',
      numericField: '123',
      amountField: '111'
    });
    jest
      .spyOn(sendLetterService, 'sendLetter')
      .mockResolvedValue({ ...responseDefault, data: {} });
    jest.spyOn(commonActions, 'getFeatureConfig').mockResolvedValue({
      ...responseDefault,
      selectFields: 'selectFields',
      defaultFields: 'defaultFields'
    });

    const response = await sendLetterConfig({
      ...mockData,
      letter: { letterId: 'mock id' } as any
    })(
      store.dispatch,
      () => {
        return {
          sendLetter: {
            [storeId]: {
              variables: [
                {
                  dataField: 'textField',
                  type: FIELD_TYPE.TEXT
                },
                {
                  dataField: 'numericField',
                  type: FIELD_TYPE.NUMERIC
                },
                {
                  dataField: 'field1',
                  type: FIELD_TYPE.DATE
                }
              ]
            }
          } as SendLetterState
        } as RootState;
      },
      {}
    );

    expect(response.type).toEqual(sendLetterConfig.fulfilled.type);
    expect(response.payload).toEqual({});
  });

  it('sendLetterConfig empty data', async () => {
    jest
      .spyOn(sendLetterService, 'sendLetter')
      .mockResolvedValue({ ...responseDefault, data: {} });
    jest.spyOn(commonActions, 'getFeatureConfig').mockResolvedValue({
      ...responseDefault,
      selectFields: 'selectFields',
      defaultFields: 'defaultFields'
    });

    const response = await sendLetterConfig({
      ...mockData,
      data: undefined
    })(
      store.dispatch,
      () => {
        return {
          sendLetter: {
            [storeId]: {}
          } as SendLetterState
        } as RootState;
      },
      {}
    );

    expect(response.type).toEqual(sendLetterConfig.fulfilled.type);
    expect(response.payload).toEqual({});
  });

  it('sendLetterConfig with undefined variable date time data', async () => {
    jest.spyOn(sendLetterHelpers, 'buildCustomData').mockReturnValue({
      field1: undefined as any,
      amountField: '111'
    });
    jest
      .spyOn(sendLetterService, 'sendLetter')
      .mockResolvedValue({ ...responseDefault, data: {} });
    jest.spyOn(commonActions, 'getFeatureConfig').mockResolvedValue({
      ...responseDefault,
      selectFields: 'selectFields',
      defaultFields: 'defaultFields'
    });

    const response = await sendLetterConfig({
      ...mockData,
      data: undefined
    })(
      store.dispatch,
      () => {
        return {
          sendLetter: {
            [storeId]: {
              variables: [
                {
                  dataField: 'field1',
                  type: FIELD_TYPE.DATE
                },
                {
                  dataField: 'amountField',
                  type: FIELD_TYPE.TEXT,
                  isAmount: true
                }
              ]
            }
          } as SendLetterState
        } as RootState;
      },
      {}
    );

    expect(response.type).toEqual(sendLetterConfig.fulfilled.type);
    expect(response.payload).toEqual({});
  });

  it('sendLetterConfig with error', async () => {
    jest
      .spyOn(sendLetterService, 'sendLetter')
      .mockRejectedValue({ ...responseDefault, data: {} });

    const response = await sendLetterConfig(mockData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(sendLetterConfig.rejected.type);
  });

  it('invokeSendingLetter ', async () => {
    jest.spyOn(sendLetterService, 'sendLetter').mockResolvedValue({
      ...responseDefault,
      data: {
        test: '1111'
      }
    });

    const actionsToast = toastSpy('addToast');
    const actionsMomo = memoSpy('postMemo');
    const actionAccount = apiErrorNotificationActionSpy('clearApiErrorData');

    await invokeSendingLetter({
      ...mockData,
      letter: { letterId: 'mock id' } as any
    })(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          sendLetter: {
            [storeId]: {
              isOpenLetter: true
            }
          }
        } as RootState),
      {}
    );

    expect(actionsToast).toHaveBeenCalledWith({
      message: 'txt_letter_sent_success',
      show: true,
      type: 'success'
    });
    expect(actionAccount).toHaveBeenCalledWith({
      storeId,
      forSection: 'inSendLetterWithVariableModal'
    });
    expect(actionsMomo).toHaveBeenCalledWith('SEND_LETTER_WITH_VARIABLES', {
      accEValue: '323*****322',
      cardholderContactedName: 'name',
      letterDescription: undefined,
      chronicleKey: 'letters',
      letterId: 'mock id',
      operatorId: '',
      storeId: '123456789'
    });
  });

  it('invokeSendingLetter, isOpenLetter is false ', async () => {
    jest.spyOn(sendLetterService, 'sendLetter').mockResolvedValue({
      ...responseDefault,
      data: {
        test: '1111'
      }
    });

    const actionsToast = toastSpy('addToast');
    memoSpy('postMemo');
    apiErrorNotificationActionSpy('clearApiErrorData');

    await invokeSendingLetter(mockData)(
      store.dispatch,
      () =>
        ({
          ...store.getState(),
          sendLetter: {
            [storeId]: {
              isOpenLetter: false
            }
          }
        } as RootState),
      {}
    );

    expect(actionsToast).toHaveBeenCalledWith({
      message: 'txt_letter_sent_success',
      show: true,
      type: 'success'
    });
  });

  it('invokeSendingLetter with error', async () => {
    const actionsToast = toastSpy('addToast');

    await invokeSendingLetter(mockData)(store.dispatch, store.getState, {});

    expect(actionsToast).toHaveBeenCalledWith({
      message: 'txt_letter_sent_fail',
      show: true,
      type: 'error'
    });
  });

  it('triggerSendLetter with error', async () => {
    const actionsToast = toastSpy('addToast');
    await triggerSendLetter({
      storeId,
      common: {
        accountId: accNbr
      },
      letter: {
        letterId: '123',
        letterCode: '123',
        letterDescription: 'des',
        variableLetter: false
      }
    })(errorStore.dispatch, errorStore.getState, {});

    expect(actionsToast).toHaveBeenCalledWith({
      show: true,
      type: 'error',
      message: SEND_LETTER.SENT_FAIL
    });
  });

  it('triggerSendLetter', async () => {
    jest.spyOn(sendLetterService, 'getLetterWithVariables').mockResolvedValue({
      ...responseDefault,
      data: {
        letterConfig: {}
      }
    });

    const memoAction = memoSpy('postMemo');
    const actionsToast = toastSpy('addToast');
    await triggerSendLetter(mockData)(store.dispatch, store.getState, {});

    expect(memoAction).toHaveBeenCalledWith('SEND_LETTER_WITHOUT_VARIABLES', {
      accEValue: '323*****322',
      cardholderContactedName: 'name',
      chronicleKey: 'letters',
      letterDescription: undefined,
      letterId: undefined,
      operatorId: '',
      storeId: '123456789'
    });

    expect(actionsToast).toHaveBeenCalledWith({
      message: 'txt_letter_sent_success',
      show: true,
      type: 'success'
    });
  });

  it('triggerSendLetter with more data', async () => {
    jest.spyOn(sendLetterService, 'getLetterWithVariables').mockResolvedValue({
      ...responseDefault,
      data: {
        letterConfig: {},
        universalAddressInquiryResponse: {},
        customerInquiryResponse: {}
      }
    });

    const actionsLetter = sendLetterSpy('toggleSendLetter');
    await triggerSendLetter(mockData)(store.dispatch, store.getState, {});

    expect(actionsLetter).toHaveBeenCalledWith({
      storeId: '123456789'
    });
  });
});
