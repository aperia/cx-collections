import {
  reducer,
  sendLetterActions
} from 'pages/Letters/SendLetter/_redux/reducers';
import { SendLetterStateResult } from 'pages/Letters/SendLetter/types';

const mockStoreId = 'mock-store-id';

const initReducer: SendLetterStateResult = {
  [mockStoreId]: {}
};

describe('redux-store > send letter ', () => {
  it('toggleSendLetter', () => {
    const nextState = reducer(
      initReducer,
      sendLetterActions.toggleSendLetter({
        letterNumber: '1231231',
        storeId: mockStoreId
      })
    );
    expect(nextState[mockStoreId].isOpenLetter).toEqual(true);
  });

  it('removeStore', () => {
    const nextState = reducer(
      initReducer,
      sendLetterActions.removeStore({
        storeId: mockStoreId
      })
    );
    expect(nextState[mockStoreId]).toBeUndefined();
  });
});
