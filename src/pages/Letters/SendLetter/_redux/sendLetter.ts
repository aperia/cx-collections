import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import isUndefined from 'lodash.isundefined';
import { batch } from 'react-redux';

// I18n
import { CONFIG_SEND_LETTER_URL, FIELD_TYPE, SEND_LETTER } from '../constants';

// Redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { sendLetterActions } from './reducers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { memoActions } from 'pages/Memos/_redux/reducers';

// Type
import {
  SendLetterArgs,
  SendLetterStateResult,
  TriggerSendLetterArgs
} from '../types';
import { SendLetterConfiguration } from 'app/global-types/letter';

// Service
import sendLetterService from '../sendLetterService';

// Helper
import { buildCustomData } from '../helpers';
import { formatTimeDefault, getFeatureConfig } from 'app/helpers';

// Const
import { EMPTY_ARRAY, EMPTY_OBJECT } from 'app/constants';
import { LETTER_ACTIONS } from 'pages/Memos/constants';
import { CHRONICLE_LETTERS_KEY } from 'pages/ClientConfiguration/Memos/constants';
import { FormatTime } from 'app/constants/enums';

export const sendLetterConfig = createAsyncThunk<
  unknown,
  SendLetterArgs,
  ThunkAPIConfig
>('letter/sendLetter', async (args, thunkAPI) => {
  const { getState } = thunkAPI;
  const { letter, data, common, storeId } = args;
  const { sendLetter } = getState();
  let currentLetter = letter;
  const variables = sendLetter[storeId]?.variables || EMPTY_ARRAY;

  if (isUndefined(letter)) {
    currentLetter = sendLetter[storeId]?.letter;
  }
  try {
    const letterConfig = await getFeatureConfig<SendLetterConfiguration>(
      CONFIG_SEND_LETTER_URL
    );

    const datePickerField = [];
    const amountField = [];

    for (let i = 0; i < variables.length; i++) {
      if (variables[i].type === FIELD_TYPE.DATE) {
        datePickerField.push(variables[i].dataField);
      } else if (variables[i].isAmount) {
        amountField.push(variables[i].dataField);
      }
    }

    const customData = buildCustomData(
      data || EMPTY_OBJECT,
      variables,
      letterConfig
    );

    for (const fieldItem in customData) {
      if (datePickerField.includes(fieldItem) && !isUndefined(fieldItem)) {
        customData[fieldItem] =
          formatTimeDefault(
            customData[fieldItem],
            FormatTime.DefaultPostDate
          ) || customData[fieldItem];
      } else if (amountField.includes(fieldItem) && !isUndefined(fieldItem)) {
        customData[fieldItem] = (
          Number.parseFloat(customData[fieldItem]) * 100
        ).toString();
      }
    }

    const response = await sendLetterService.sendLetter({
      common,
      letterIdentifier: currentLetter?.letterId,
      ...customData
    });

    return response.data;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const invokeSendingLetter = createAsyncThunk<
  unknown,
  SendLetterArgs,
  ThunkAPIConfig
>('letter/invokeSendingLetter', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;
  const { letter, common, storeId } = args;
  const { sendLetter, accountDetail } = getState();
  let currentLetter = letter;
  const { operatorID = '' } = window?.appConfig?.commonConfig || {};

  if (isUndefined(letter)) {
    currentLetter = sendLetter[storeId]?.letter;
  }

  const response = await dispatch(sendLetterActions.sendLetterConfig(args));

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: SEND_LETTER.SENT_SUCCESS
        })
      );

      // Post memo send letter with variables
      dispatch(
        memoActions?.postMemo(LETTER_ACTIONS.SEND_LETTER_WITH_VARIABLES, {
          storeId,
          accEValue: common?.accountId,
          chronicleKey: CHRONICLE_LETTERS_KEY,
          letterId: currentLetter?.letterId,
          letterDescription: currentLetter?.letterDescription,
          cardholderContactedName: accountDetail[storeId]?.data?.primaryName,
          operatorId: operatorID
        })
      );

      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inSendLetterWithVariableModal'
        })
      );

      if (sendLetter[storeId]?.isOpenLetter)
        dispatch(sendLetterActions.toggleSendLetter({ storeId }));
    });
  }

  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: SEND_LETTER.SENT_FAIL
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inSendLetterWithVariableModal',
          apiResponse: response.payload?.response
        })
      );
    });
  }
});

export const triggerSendLetter = createAsyncThunk<
  unknown,
  TriggerSendLetterArgs,
  ThunkAPIConfig
>('letter/triggerSendLetter', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;
  const { letter, common, storeId } = args;
  const { operatorID = '' } = window?.appConfig?.commonConfig || {};

  const response = await dispatch(
    sendLetterActions.getLetterWithVariables({
      common,
      storeId,
      letter
    })
  );

  const { accountDetail } = getState();

  if (isFulfilled(response)) {
    const { payload } = response;

    /**
     * if the letter does not contains variables, then immediately stop generate letter
     * else open the sendLetter modal to continue the process
     */
    if (
      !payload?.universalAddressInquiryResponse &&
      !payload.customerInquiryResponse
    ) {
      // Post memo send letter without variables
      dispatch(
        memoActions?.postMemo(LETTER_ACTIONS.SEND_LETTER_WITHOUT_VARIABLES, {
          storeId,
          accEValue: common?.accountId,
          chronicleKey: CHRONICLE_LETTERS_KEY,
          letterId: letter?.letterId,
          letterDescription: letter?.letterDescription,
          cardholderContactedName: accountDetail[storeId]?.data?.primaryName,
          operatorId: operatorID
        })
      );
      return dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: SEND_LETTER.SENT_SUCCESS
        })
      );
    }

    dispatch(
      sendLetterActions.toggleSendLetter({
        storeId
      })
    );
  }

  if (isRejected(response)) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: SEND_LETTER.SENT_FAIL
      })
    );
  }
});

export const sendLetterBuilder = (
  builder: ActionReducerMapBuilder<SendLetterStateResult>
) => {
  builder
    .addCase(sendLetterConfig.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingSendLetter: true
      };
    })
    .addCase(sendLetterConfig.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingSendLetter: false,
        variablesData: EMPTY_OBJECT
      };
    })
    .addCase(sendLetterConfig.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingSendLetter: false
      };
    });
};
