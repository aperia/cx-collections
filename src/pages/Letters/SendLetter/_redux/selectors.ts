import { createSelector } from '@reduxjs/toolkit';

export const getOpenStatus = (state: RootState, storeId: string) =>
  state.sendLetter[storeId]?.isOpenLetter;

export const takeOpenStatus = createSelector(getOpenStatus, data => data);

export const getLetter = (state: RootState, storeId: string) =>
  state.sendLetter[storeId]?.letter;

export const takeLetter = createSelector(getLetter, data => data);

export const getVariablesData = (state: RootState, storeId: string) =>
  state.sendLetter[storeId]?.variablesData;

export const takeVariablesData = createSelector(getVariablesData, data => data);

export const getVariables = (state: RootState, storeId: string) =>
  state.sendLetter[storeId]?.variables;

export const takeVariables = createSelector(getVariables, data => data);

export const getLoadingSendLetterStatus = (state: RootState, storeId: string) =>
  state.sendLetter[storeId]?.isLoadingSendLetter;

export const takeLoadingSendLetterStatus = createSelector(
  getLoadingSendLetterStatus,
  data => data
);

export const getLoadingVariablesStatus = (state: RootState, storeId: string) =>
  state.sendLetter[storeId]?.isLoadingVariables;

export const takeLoadingVariablesStatus = createSelector(
  getLoadingVariablesStatus,
  data => data
);
