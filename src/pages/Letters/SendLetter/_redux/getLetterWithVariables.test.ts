import {
  accNbr,
  mockActionCreator,
  responseDefault,
  storeId
} from 'app/test-utils';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import sendLetterService from '../sendLetterService';
import * as commonActions from 'app/helpers/commons';
import { getLetterWithVariables } from './getLetterWithVariables';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { GetVariablesArgs } from '../types';

const mockData = {
  storeId,
  common: {
    accountId: accNbr
  },
  inquiryFields: {
    memberSequenceIdentifier: 'member',
    customerRoleTypeCode: 'type'
  },
  letter: {
    letterId: 'mock id',
    letterCode: '123',
    letterDescription: 'des'
  }
} as GetVariablesArgs;

const store = createStore(rootReducer, {}, applyMiddleware(thunk));

const apiErrorNotificationActionSpy = mockActionCreator(
  apiErrorNotificationAction
);
const toastSpy = mockActionCreator(actionsToast);

describe('getLetterWithVariables', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterEach(() => {
    jest.runAllTimers();
    jest.clearAllMocks();
  });

  it('with data', async () => {
    jest.spyOn(commonActions, 'getFeatureConfig').mockResolvedValue({
      ...responseDefault,
      customerSelectFields: [
        { key: '1', value: 'abc' },
        { key: '2', value: 'abc' }
      ],
      addressSelectFields: [
        { key: '1', value: 'abc' },
        { key: '2', value: 'abc' }
      ]
    });
    jest.spyOn(sendLetterService, 'getLetterWithVariables').mockResolvedValue({
      ...responseDefault,
      data: {
        letterConfig: {},
        letterCaptionsResponse: {
          letterCaptionsList: [{}]
        },
        customerInquiryResponse: {
          customers: [
            { customerRoleTypeCode: '01' },
            { customerRoleTypeCode: '02' }
          ]
        },
        universalAddressInquiryResponse: {
          primary: [
            { customerRoleTypeCode: '01' },
            { customerRoleTypeCode: '02' }
          ]
        }
      }
    });
    const spy = apiErrorNotificationActionSpy('clearApiErrorData');
    await getLetterWithVariables(mockData)(store.dispatch, store.getState, {});
    expect(spy).toHaveBeenCalledWith({
      storeId,
      forSection: 'inModalBody'
    });
  });

  it('with getFeatureConfig return  empty data', async () => {
    jest.spyOn(commonActions, 'getFeatureConfig').mockResolvedValue(undefined);
    jest.spyOn(sendLetterService, 'getLetterWithVariables').mockResolvedValue({
      ...responseDefault,
      data: {
        letterConfig: {},
        letterCaptionsResponse: undefined,
        customerInquiryResponse: undefined,
        universalAddressInquiryResponse: undefined
      }
    });
    const spy = apiErrorNotificationActionSpy('clearApiErrorData');
    await getLetterWithVariables(mockData)(store.dispatch, store.getState, {});
    expect(spy).toHaveBeenCalledWith({
      storeId,
      forSection: 'inModalBody'
    });
  });

  it('with empty data', async () => {
    jest.spyOn(commonActions, 'getFeatureConfig').mockResolvedValue({
      ...responseDefault,
      customerSelectFields: [
        { key: '1', value: 'abc' },
        { key: '2', value: 'abc' }
      ],
      addressSelectFields: [
        { key: '1', value: 'abc' },
        { key: '2', value: 'abc' }
      ]
    });
    jest.spyOn(sendLetterService, 'getLetterWithVariables').mockResolvedValue({
      ...responseDefault,
      data: {
        letterConfig: {},
        letterCaptionsResponse: undefined,
        customerInquiryResponse: undefined,
        universalAddressInquiryResponse: undefined
      }
    });
    const spy = apiErrorNotificationActionSpy('clearApiErrorData');
    await getLetterWithVariables(mockData)(store.dispatch, store.getState, {});
    expect(spy).toHaveBeenCalledWith({
      storeId,
      forSection: 'inModalBody'
    });
  });

  it('with data error message', async () => {
    jest.spyOn(commonActions, 'getFeatureConfig').mockResolvedValue({
      ...responseDefault,
      customerSelectFields: [
        { key: '1', value: 'abc' },
        { key: '2', value: 'abc' }
      ],
      addressSelectFields: [
        { key: '1', value: 'abc' },
        { key: '2', value: 'abc' }
      ]
    });
    jest.spyOn(sendLetterService, 'getLetterWithVariables').mockResolvedValue({
      ...responseDefault,
      data: {
        status: 'failure',
        letterConfig: {}
      }
    });
    const spy = apiErrorNotificationActionSpy('clearApiErrorData');
    await getLetterWithVariables(mockData)(store.dispatch, store.getState, {});
    expect(spy).toHaveBeenCalledTimes(0);
  });

  it('with error', async () => {
    jest
      .spyOn(commonActions, 'getFeatureConfig')
      .mockResolvedValue(responseDefault);
    jest.spyOn(sendLetterService, 'getLetterWithVariables').mockResolvedValue({
      ...responseDefault,
      data: {
        status: 'failure',
        message: ';{"errorCode": "500", "message": "Error"}',
        letterConfig: {}
      }
    });
    const spy = toastSpy('addToast');
    await getLetterWithVariables(mockData)(store.dispatch, store.getState, {});
    expect(spy).not.toHaveBeenCalledWith({
      message: 'Error',
      show: true,
      type: 'error'
    });
  });
});
