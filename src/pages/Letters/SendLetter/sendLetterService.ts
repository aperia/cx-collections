import { apiService } from 'app/utils/api.service';

// types
import {
  GetLetterVariablesRequest,
  GetLetterVariablesResponse,
  SendLetterRequest
} from './types';

const sendLetterService = {
  sendLetter(data: SendLetterRequest) {
    const url = window.appConfig?.api?.letter?.sendLetter || '';
    return apiService.post(url, data);
  },
  getLetterWithVariables(data: GetLetterVariablesRequest) {
    const url = window.appConfig?.api?.letter?.letterInquiry || '';
    return apiService.post<GetLetterVariablesResponse>(url, data);
  }
};

export default sendLetterService;
