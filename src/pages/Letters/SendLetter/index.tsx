import React from 'react';
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { useDispatch, useSelector } from 'react-redux';
import { getFormValues, isValid } from 'redux-form';

// Hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// I18n
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { SEND_LETTER } from './constants';

// Redux
import { sendLetterActions } from './_redux/reducers';
import {
  takeLoadingSendLetterStatus,
  takeOpenStatus
} from './_redux/selectors';
import { SendLetterContent } from './SendLetterContent';

export const viewName = 'letterCaptionWithVariables';

export interface SendLetterProps {}
const SendLetter: React.FC<SendLetterProps> = () => {
  const { storeId, accEValue } = useAccountDetail();
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const isLoading = useStoreIdSelector<boolean>(takeLoadingSendLetterStatus);
  const formValue: MagicKeyValue = useSelector(
    getFormValues(`${viewName}_${storeId}`)
  );
  const formValid = useSelector(isValid(`${viewName}_${storeId}`));
  const isOpenLetter = useStoreIdSelector<boolean>(takeOpenStatus);

  const handleCloseModal = () => {
    dispatch(sendLetterActions.toggleSendLetter({ storeId }));
  };

  const handleSendLetter = () => {
    dispatch(
      sendLetterActions.invokeSendingLetter({
        common: {
          accountId: accEValue
        },
        data: formValue,
        storeId
      })
    );
  };

  return (
    <Modal
      show={isOpenLetter}
      md
      loading={isLoading}
      dataTestId="letterCaptions_modal"
    >
      <ModalHeader
        dataTestId="letterCaptions_header"
        border
        closeButton
        onHide={handleCloseModal}
      >
        <ModalTitle dataTestId="letterCaptions_header_title">
          {t(SEND_LETTER.LETTER_CAPTION)}
        </ModalTitle>
      </ModalHeader>
      <ModalBody noPadding>
        <SendLetterContent />
      </ModalBody>
      <ModalFooter
        dataTestId="letterCaptions_footer"
        okButtonText={t(I18N_COMMON_TEXT.SEND)}
        cancelButtonText={t(I18N_COMMON_TEXT.CLOSE)}
        onCancel={handleCloseModal}
        onOk={handleSendLetter}
        disabledOk={!formValid}
      />
    </Modal>
  );
};

export default SendLetter;
