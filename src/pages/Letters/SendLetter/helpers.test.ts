import { kyMapping } from './constants';
import {
  generateLetterVariables,
  generateLetterData,
  buildCustomData
} from './helpers';
import { DynamicVariable } from './types';

const variable = [
  {
    dataField: 'dataField',
    label: 'label',
    name: 'aObj1_with_state',
    mappingField: 'mapping',
    props: { required: true, readOnly: true },
    options: { key: '1', value: 'value' },
    validationRules: [
      { key: '1', value: 'value' },
      { key: '2', value: 'value' }
    ],
    type: 'type'
  },
  {
    dataField: 'dataField',
    label: '123',
    name: 'aObj1_with_post',
    mappingField: 'mapping',
    props: { required: true, readOnly: true },
    options: { key: '1', value: 'value' },
    validationRules: [
      { key: '1', value: 'value' },
      { key: '2', value: 'value' }
    ],
    type: 'type'
  }
];
const map = {
  fieldType: {
    currency: ['123', '12'],
    date: ['123', '12']
  },
  populateFields: [
    {
      ky: 'ky',
      fieldName: 'name',
      includesLabel: ['123', '12', '1234']
    }
  ],
  additionalOptionsConfig: [{ name: 'name' }],
  combineTwoFields: ['123', '12', '1234'],
  customerSelectFields: ['123', '12', '1234'],
  addressSelectFields: ['123', '12', '1234']
};

const defaultConfig = {
  populateFields: [
    {
      includesLabel: ['NAME', 'CARDHOLDER NAME', 'PRIMARY', 'PRIMARY NAME'],
      ky: 'customerName',
      fieldName: 'alternateAddressName',
      additionalOptions: {
        maxLength: 30
      }
    },
    {
      includesLabel: ['ADDRESS LINE #1', 'ADDRESS LINE 1', 'ADDRESS LINE ONE'],
      ky: 'addressContinuationLine1Text',
      fieldName: 'alternateAddressLineOneText',
      additionalOptions: {
        maxLength: 35
      }
    },
    {
      includesLabel: ['ADDRESS LINE #2', 'ADDRESS LINE 2', 'ADDRESS LINE TWO'],
      ky: 'addressContinuationLine2Text',
      fieldName: 'alternateAddressLineTwoText',
      additionalOptions: {
        maxLength: 35
      }
    },
    {
      includesLabel: ['STATE'],
      ky: 'addressSubdivisionOneText',
      fieldName: 'countrySubDivisionFirstIdentifier',
      additionalValidations: [
        {
          ruleName: 'MinLength',
          errorMsg: 'Invalid format',
          ruleOptions: { minLength: 2 }
        }
      ],
      additionalOptions: {
        maxLength: 2
      }
    },
    {
      includesLabel: ['CITY'],
      ky: 'cityName',
      fieldName: 'alternateAddressCityName',
      additionalOptions: {
        maxLength: 18
      }
    },
    {
      includesLabel: ['ZIP', 'ZIP CODE'],
      ky: 'postalCode',
      fieldName: 'alternateAddressPostalCode',
      additionalOptions: {
        maxLength: 10,
        minLength: 5
      }
    },
    {
      includesLabel: ['DAY OF MONTH'],
      additionalOptions: {
        numberOnly: true
      }
    }
  ],
  fieldType: {
    currency: ['AMOUNT', 'AMT'],
    date: ['DATE']
  },
  combineTwoFields: [
    'CITY STATE',
    'CITY/STATE',
    'CITY / STATE',
    'CITY/STATE/ZIP',
    'CITY / STATE / ZIP',
    'STATE/ZIP',
    'STATE / ZIP',
    'STATE/ZIP CODE',
    'STATE / ZIP CODE'
  ],
  customerSelectFields: ['customerName', 'primaryCustomerName'],
  addressSelectFields: [
    'cityName',

    'postalCode',

    'addressContinuationLine1Text',

    'addressContinuationLine2Text',

    'addressContinuationLine3Text',

    'addressContinuationLine4Text',

    'addressSubdivisionOneText',

    'addressSubdivisionTwoText'
  ],
  additionalOptionsConfig: [
    {
      optionsName: 'STATE',
      additionalOptions: {
        key: '1',
        value: 'value'
      },
      additionalValidations: [
        {
          key: '1',
          value: 'value'
        },
        {
          key: '2',
          value: 'value'
        }
      ]
    },
    {},
    {
      optionsName: 'test'
    }
  ]
};

describe('SendLetter helpers', () => {
  it('should run generateLetterVariables', () => {
    const result = generateLetterVariables(
      [
        {
          optionalNameOrAddressCode: 'Y',
          optionalVariableCaptionOne: 'NAME',
          optionalVariableCaptionTwo: 'state/zip',
          optionalVariableCaptionThree: 'city state'
        }
      ],
      defaultConfig
    );

    expect(result.length).toEqual(5);
  });

  it('should run generateLetterVariables with empty sendLetterConfig', () => {
    const result = generateLetterVariables([
      {
        optionalNameOrAddressCode: 'Y',
        undefined: ''
      } as any
    ]);

    expect(result.length).toEqual(0);
  });

  it('should run generateLetterVariables with empty sendLetterConfig 2', () => {
    const result = generateLetterVariables([
      {
        optionalNameOrAddressCode: 'address',
        optionalVariableCaptionOne: 'another'
      } as any
    ]);

    expect(result.length).toEqual(2);
  });

  it('should run generateLetterVariables with empty sendLetterConfig', () => {
    const result = generateLetterVariables(
      [
        {
          optionalNameOrAddressCode: 'N',
          optionalVariableCaptionOne: 'NAME',
          optionalVariableCaptionTwo: 'STATE/ZIP'
        } as any
      ],
      defaultConfig
    );

    expect(result.length).toEqual(2);
  });

  it('should run generateLetterVariables with amount field', () => {
    const result = generateLetterVariables(
      [
        {
          optionalNameOrAddressCode: 'Y',
          optionalVariableCaptionOne: 'AMT',
          optionalVariableCaptionTwo: 'state/zip'
        }
      ],
      defaultConfig
    );

    expect(result.length).toEqual(3);
  });

  it('should run generateLetterVariables with date field', () => {
    const result = generateLetterVariables(
      [
        {
          optionalNameOrAddressCode: 'Y',
          optionalVariableCaptionOne: 'Date',
          optionalVariableCaptionTwo: 'state/zip'
        }
      ],
      defaultConfig
    );

    expect(result.length).toEqual(3);
  });

  it('should run generateLetterVariables with date field & empty defaultConfig', () => {
    const result = generateLetterVariables([
      {
        optionalNameOrAddressCode: 'Y',
        optionalVariableCaptionOne: 'Date',
        optionalVariableCaptionTwo: 'state/zip'
      }
    ]);

    expect(result.length).toEqual(2);
  });

  it('should run generateLetterVariables with date field & empty additionalOptionsConfig', () => {
    const emptyOptionalNameAdditionalOptionsConfig = {
      ...defaultConfig,
      additionalOptionsConfig: [
        {
          additionalOptions: {
            key: '1',
            value: 'value'
          },
          additionalValidations: [
            {
              key: '1',
              value: 'value'
            },
            {
              key: '2',
              value: 'value'
            }
          ]
        }
      ]
    };
    const result = generateLetterVariables(
      [
        {
          optionalNameOrAddressCode: 'Y',
          optionalVariableCaptionOne: 'Date',
          optionalVariableCaptionTwo: 'state/zip'
        }
      ],
      emptyOptionalNameAdditionalOptionsConfig
    );

    expect(result.length).toEqual(3);
  });

  it('should run generateLetterData > return empty object', () => {
    const variables: Array<DynamicVariable> = [
      {
        dataField: 'optionalVariableCaptionOne',
        label: 'Name',
        name: 'optionalVariableCaptionOne',
        mappingField: 'customerName',
        props: { readOnly: false, required: true },
        options: { maxLength: 30 },
        validationRules: [[Object]],
        type: 'TextBoxControl',
        layoutProps: { className: 'col-lg-4 col-6 mt-16' }
      }
    ];

    const customers = [
      {
        customerName: 'Smith'
      }
    ];
    const addresses = [
      {
        addressContinuationLine1Text: 'LA'
      }
    ];
    const result = generateLetterData(variables, customers, addresses);
    expect(result).toEqual({});
  });

  it('should run generateLetterData with value', () => {
    const customers = {
      customerName: 'Smith'
    };
    const addresses = {
      addressContinuationLine1Text: 'LA',
      mapping: 'mapping'
    };
    const result = generateLetterData(variable, customers, addresses);
    expect(result).toEqual({ dataField: 'mapping' });
  });

  it('should run generateLetterData with value 2', () => {
    const customers = {
      customerName: 'Smith'
    };
    const addresses = {
      addressContinuationLine1Text: 'LA',
      ['']: 'mapping'
    };
    const result = generateLetterData(
      [
        {
          label: 'label',
          name: 'aObj1_with_state',
          props: { required: true, readOnly: true },
          options: { key: '1', value: 'value' },
          validationRules: [
            { key: '1', value: 'value' },
            { key: '2', value: 'value' }
          ],
          type: 'type'
        }
      ],
      customers,
      addresses
    );
    expect(result).toEqual({ '': 'mapping' });
  });

  it('should run generateLetterData with value 3', () => {
    const customers = {};
    const addresses = {
      mapping: ''
    };
    const result = generateLetterData(variable, customers, addresses);
    expect(result).toEqual({});
  });

  it('should run generateLetterData with value 4', () => {
    const result = generateLetterData(variable);
    expect(result).toEqual({});
  });

  it('should run buildCustomData > return empty object', () => {
    const result = buildCustomData({}, []);
    expect(result).toEqual({});
  });

  it('should run buildCustomData', () => {
    const data = {
      [`aObj1${kyMapping}state`]: `aObj1_`,
      [`aObj1${kyMapping}post`]: 'aObj1_ky'
    };

    const result = buildCustomData(data, []);
    expect(result).toEqual({
      aObj1: 'aObj1_/aObj1_ky'
    });
  });

  it('should run buildCustomData', () => {
    const data = {
      [`aObj1${kyMapping}state`]: `aObj1_`,
      [`aObj1${kyMapping}post`]: 'aObj1_ky',
      [`aObj1_post`]: 'aObj1_ky'
    };
    const result = buildCustomData(data, variable, map as any);
    expect(result).toEqual({ aObj1: 'aObj1_/NaN', aObj1_post: 'aObj1_ky' });
  });

  it('generateLetterVariables with data', () => {
    const data = [
      {
        optionalNameOrAddressCode: 'Y',
        optionalVariableCaptionOne: 'caption one',
        optionalVariableCaptionTwo: 'caption two',
        optionalVariableCaptionThree: 'caption three',
        optionalVariableCaptionFive: 'caption five',
        optionalVariableCaptionSix: 'caption six',
        optionalVariableCaptionSeven: 'caption seven',
        optionalVariableCaptionEight: 'caption eight',
        optionalVariableCaptionNine: 'caption nine',
        optionalVariableCaptionTen: 'caption ten'
      },
      {
        optionalNameOrAddressCode: 'address',
        optionalVariableCaptionOne: 'one',
        optionalVariableCaptionTwo: 'two',
        optionalVariableCaptionThree: 'three',
        optionalVariableCaptionFive: 'five',
        optionalVariableCaptionSix: 'six',
        optionalVariableCaptionSeven: 'seven',
        optionalVariableCaptionEight: 'eight',
        optionalVariableCaptionNine: 'nine',
        optionalVariableCaptionTen: 'ten'
      }
    ];
    const result = generateLetterVariables(data, defaultConfig);
    expect(result[0]?.dataField).toEqual('');
  });
});
