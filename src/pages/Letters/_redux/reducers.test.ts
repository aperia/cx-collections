import { letterAction, reducer } from './reducers';
import { LetterInitialState } from '../types';
import { storeId } from 'app/test-utils';

const { removeStore, updateOpenModal, updateSelectTab } = letterAction;

const initialState: LetterInitialState = {};

describe('Letter reducer', () => {
  it('should run removeStore action', () => {
    const params = { storeId };
    const state = reducer(initialState, removeStore(params));
    expect(state[storeId]).toBeUndefined();
  });

  it('should run updateOpenModal action', () => {
    const params = { storeId, openModal: true };
    const state = reducer(initialState, updateOpenModal(params));
    expect(state[storeId]).toEqual(
      expect.objectContaining({ openModal: true })
    );
  });

  it('should run updateSelectTab action', () => {
    const params = { storeId, activeKey: 'active-key' };
    const state = reducer(initialState, updateSelectTab(params));
    expect(state[storeId]).toEqual(
      expect.objectContaining({ activeKey: 'active-key' })
    );
  });
});
