import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// types
import { LetterInitialState, UpdateOpenModal, UpdateSelectTab } from '../types';

const { actions, reducer } = createSlice({
  name: 'letter',
  initialState: {} as LetterInitialState,
  reducers: {
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    updateOpenModal: (draftState, action: PayloadAction<UpdateOpenModal>) => {
      const { storeId, openModal } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        openModal
      };
    },
    updateSelectTab: (draftState, action: PayloadAction<UpdateSelectTab>) => {
      const { storeId, activeKey } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        activeKey
      };
    }
  }
});

const letterAction = {
  ...actions
};

export { letterAction, reducer };
