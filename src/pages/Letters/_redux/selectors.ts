import { createSelector } from '@reduxjs/toolkit';

export const selectOpenModal = createSelector(
  (states: RootState, storeId: string) =>
    states.letter[storeId]?.openModal || false,
  (openModal: boolean) => openModal
);

export const selectActiveKey = createSelector(
  (states: RootState, storeId: string) =>
    states.letter[storeId]?.activeKey,
  (activeKey?: string) => activeKey
);
