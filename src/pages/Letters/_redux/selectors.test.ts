import { selectorWrapper, storeId } from 'app/test-utils';
import * as selector from './selectors';

const initialState: Partial<RootState> = {
  letter: {
    [storeId]: {
      openModal: true,
      activeKey: 'activeKey'
    }
  }
};

const emptyState: Partial<RootState> = {
  letter: {
    [storeId]: {}
  }
};

const selectorTrigger = selectorWrapper(initialState, emptyState);

describe('Letter selectors', () => {
  it('should run selectOpenModal', () => {
    const { data, emptyData } = selectorTrigger(selector.selectOpenModal);
    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });
});
