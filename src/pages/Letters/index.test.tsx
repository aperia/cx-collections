import React from 'react';
import { AccountDetailProvider } from 'app/hooks';
import { fireEvent, screen } from '@testing-library/react';

// utils
import {
  renderMockStore,
  storeId,
  accEValue
} from 'app/test-utils/renderComponentWithMockStore';

// components
import SendLetter from './index';

// redux
import { letterAction } from './_redux/reducers';
import {
  HorizontalTabsProps,
  ModalHeaderProps
} from 'app/_libraries/_dls/components';

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponent = jest.requireActual('app/_libraries/_dls/components');
  return {
    ...dlsComponent,
    HorizontalTabs: (props: HorizontalTabsProps) => {
      const { onSelect = () => undefined } = props;

      return (
        <>
          <input
            data-testid="HorizontalTabs_onSelect"
            onChange={(e: any) => onSelect(e.target.value, e)}
          />
          <button
            data-testid="HorizontalTabs_onSelect_null"
            onClick={(e: any) => onSelect(null, e)}
          />
        </>
      );
    },
    ModalHeader: (props: ModalHeaderProps) => {
      const { onHide = () => undefined } = props;

      return <button data-testid="ModalHeader_onHide" onClick={onHide} />;
    }
  };
});

const generateMockAction = () =>
  jest.fn().mockImplementation(() => ({ type: 'type' }));

const initialState: Partial<RootState> = {
  letter: { [storeId]: { openModal: true } }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <SendLetter />
    </AccountDetailProvider>,
    {
      initialState
    }
  );
};

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper(initialState);

    expect(screen.getByRole('dialog')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleClose', () => {
    // mocks
    const mockAction = generateMockAction();

    renderWrapper(initialState);

    fireEvent.click(screen.getByTestId('ModalHeader_onHide'));

    jest.spyOn(letterAction, 'removeStore').mockImplementation(mockAction);
  });

  describe('handleSelect', () => {
    it('with value', () => {
      const activeTab = 'tab_1';

      // mocks
      const mockAction = generateMockAction();

      jest
        .spyOn(letterAction, 'updateSelectTab')
        .mockImplementation(mockAction);

      // render
      renderWrapper(initialState);

      // simulate
      fireEvent.change(screen.getByTestId('HorizontalTabs_onSelect'), {
        target: { value: activeTab }
      });

      // assertion
      expect(mockAction).lastCalledWith({ storeId, activeKey: activeTab });
    });

    it('with null', () => {
      // mocks
      const mockAction = generateMockAction();
      jest
        .spyOn(letterAction, 'updateSelectTab')
        .mockImplementation(mockAction);

      // render
      renderWrapper(initialState);

      fireEvent.click(screen.getByTestId('HorizontalTabs_onSelect_null'));

      // assertion
      expect(mockAction).lastCalledWith({ storeId, activeKey: '' });
    });
  });
});
