export interface UpdateSelectTab {
  storeId: string;
  activeKey: string;
}

export interface UpdateOpenModal {
  storeId: string;
  openModal: boolean;
}

export interface Letter {
  openModal?: boolean;
  activeKey?: string;
}
export interface LetterInitialState {
  [storeId: string]: Letter;
}
