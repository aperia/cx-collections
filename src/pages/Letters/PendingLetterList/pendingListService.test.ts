import pendingListService from './pendingListService';

// utils
import { mockApiServices, storeId } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import { DeleteLetterPostData, SendPendingLetterPostData } from './types';

describe('pendingListService', () => {
  describe('getPendingLetters', () => {
    const params: SendPendingLetterPostData = {
      common: { accountId: storeId },
      letterOriginCode: 'code',
      selectFields: ['field_1']
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      pendingListService.getPendingLetters(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      pendingListService.getPendingLetters(params);

      expect(mockService).toBeCalledWith(
        apiUrl.sendLetter.getPendingLetters,
        params
      );
    });
  });

  describe('deletePendingLetter', () => {
    const params: DeleteLetterPostData = {
      common: { accountId: storeId },
      letterIdentifier: 'id',
      originationCode: '',
      originationDate: '',
      originationTime: ''
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      pendingListService.deletePendingLetter(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      pendingListService.deletePendingLetter(params);

      expect(mockService).toBeCalledWith(
        apiUrl.sendLetter.deletePendingLetter,
        params
      );
    });
  });
});
