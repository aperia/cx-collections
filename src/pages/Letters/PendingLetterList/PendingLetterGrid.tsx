import React, { useCallback, useEffect, useState } from 'react';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// components
import { Button, ColumnType, Grid } from 'app/_libraries/_dls/components';
import DeleteModal from './DeleteModal';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// redux
import { selectPendingData } from './_redux/selectors';

// types
import { PendingLetterData, ToggleDeleteModal } from './types';

// helpers
import { formatStringTime } from './helpers';
import { convertAPIDateToView } from 'app/helpers/formatTime';

// redux
import { useDispatch } from 'react-redux';
import { pendingAction } from './_redux/reducers';

// Const
import { EMPTY_STRING } from 'app/constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

export interface PendingLetterGridProps {}

const PendingLetterGrid: React.FC<PendingLetterGridProps> = () => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const data = useStoreIdSelector<PendingLetterData[]>(selectPendingData);

  const { t } = useTranslation();

  const [columns, setColumns] = useState<ColumnType[]>([]);

  const handleDeleteLetter = useCallback(
    code => {
      dispatch(pendingAction.toggleDeleteModal({ storeId, code }));
    },
    [dispatch, storeId]
  );

  useEffect(() => {
    const allColumns = [
      {
        id: 'letterNumber',
        Header: t('txt_letter_number'),
        accessor: 'letterNumber',
        width: 140
      },
      {
        id: 'hostOperatorID',
        Header: t('txt_host_operator_id'),
        accessor: 'operatorTerminalIdentification',
        width: 154
      },
      {
        id: 'description',
        Header: t('txt_description'),
        accessor: 'letterDescription',
        width: 210
      },
      {
        id: 'origin',
        Header: t('txt_origin'),
        accessor: 'letterOriginCode',
        autoWidth: true,
        width: 450
      },
      {
        id: 'dateTime',
        Header: t('txt_date_&_time'),
        accessor: (value: Partial<PendingLetterData>, index: number) => (
          <>
            <p data-testid={genAmtId(`${index}_pendingLetterGrid`, 'date', '')}>
              {convertAPIDateToView(value?.inputDate || EMPTY_STRING)}
            </p>
            <p
              className="color-grey"
              data-testid={genAmtId(`${index}_pendingLetterGrid`, 'time', '')}
            >
              {formatStringTime(value?.inputTime || EMPTY_STRING)}
            </p>
          </>
        ),
        width: 150
      },
      {
        id: 'view',
        Header: t('txt_action'),
        accessor: ({ code }: Partial<ToggleDeleteModal>, index: number) => {
          return (
            <Button
              dataTestId={`${index}_pendingLetterGrid_delete-btn`}
              size="sm"
              variant="outline-danger"
              onClick={() => handleDeleteLetter(code)}
            >
              {t(I18N_COMMON_TEXT.DELETE)}
            </Button>
          );
        },
        cellProps: { className: 'text-center' },
        cellBodyProps: { className: 'td-sm ' },
        width: 92
      }
    ];
    const filteredColumns = allColumns.filter(column => {
      if (column.id === 'view') {
        return checkPermission(
          PERMISSIONS.LETTER_DELETE_PENDING_LETTER,
          storeId
        );
      }
      return true;
    });
    setColumns(filteredColumns);
  }, [handleDeleteLetter, t, storeId]);

  return (
    <>
      <Grid columns={columns} data={data} dataTestId="pendingLetterGrid" />
      <DeleteModal />
    </>
  );
};

export default PendingLetterGrid;
