export const CONFIG_DEFAULT_BODY = {
  selectFields: [
    'letterNumber',
    'letterDescription',
    'operatorTerminalIdentification',
    'inputDate',
    'inputTime',
    'letterOriginCode'
  ],
  letterOriginCode: 'S'
};
