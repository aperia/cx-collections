export interface UpdateCheckedList {
  storeId: string;
  checkedList: string[];
}

export interface UpdateReload {
  storeId: string;
  reload: boolean;
}

export interface SendLetterPostData {
  accountId: string;
}

export interface SendLetterArg {
  storeId: string;
  postData: SendLetterPostData;
}

export interface DeleteLetterPostData {
  common: {
    accountId: string;
  },
  letterIdentifier: string;
  originationCode: string;
  originationDate: string;
  originationTime: string;
}

export interface SendPendingLetterPostData {
  common: {
    accountId: string;
  };
  selectFields: string[];
  letterOriginCode: string;
}

export interface ToggleDeleteModal {
  storeId: string;
  code: string;
}

export interface DeleteLetterArg {
  storeId: string;
  postData: {
    accountId: string;
    code: string;
  };
}

export interface PendingLetterData {
  inputDate: string;
  inputTime: string;
  letterDescription: string;
  letterNumber: string;
  letterOriginCode: string;
  operatorTerminalIdentification: string;
}

export interface SendPendingLetterArg {
  storeId: string;
  postData: {
    common: {
      accountId: string;
    };
  };
}

export interface SendPendingLetterPayload {
  data: {
    letterPendingFields: PendingLetterData[];
  };
}

export interface PendingLetters {
  loading?: boolean;
  error?: boolean;
  noData?: boolean;
  data?: PendingLetterData[];
  openDeleteModal?: boolean;
  reload?: boolean;
  loadingModal?: boolean;
  code?: string;
}

export interface SendLetterInitialState {
  [storeId: string]: PendingLetters;
}
