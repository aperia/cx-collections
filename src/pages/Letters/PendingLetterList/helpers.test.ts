import { formatStringTime } from './helpers';

describe('Test helpers', () => {
  describe('formatStringTime', () => {
    it('should return PM', () => {
      const input = '130000';
      const expected = '01:00:00 PM';
      expect(formatStringTime(input)).toEqual(expected);
    });

    it('should return AM', () => {
      const input = '010000';
      const expected = '01:00:00 AM';
      expect(formatStringTime(input)).toEqual(expected);
    });

    it('coverage', () => {
      const input = '230000';
      const expected = '11:00:00 PM';
      expect(formatStringTime(input)).toEqual(expected);
    });
  });
});
