import React from 'react';
import { AccountDetailProvider } from 'app/hooks';
import { screen } from '@testing-library/react';

// utils
import {
  renderMockStore,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';

// components
import DeleteModal from './DeleteModal';

// redux
import { pendingAction } from './_redux/reducers';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

const initialState: Partial<RootState> = {
  pendingLetter: {
    [storeId]: { openDeleteModal: true }
  }
};

const emptyState: Partial<RootState> = {
  pendingLetter: { [storeId]: {} }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <DeleteModal />
    </AccountDetailProvider>,
    {
      initialState
    }
  );
};

const generateMockAction = () =>
  jest.fn().mockImplementation(() => ({ type: 'type' }));

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper(initialState);

    expect(screen.getByRole('dialog')).toBeInTheDocument();
  });

  it('Render UI with empty state', () => {
    renderWrapper(emptyState);

    expect(screen.queryByRole('dialog')).toBeNull();
  });
});

describe('Actions', () => {
  it('handleCloseModal', () => {
    const mockAction = generateMockAction();
    jest
      .spyOn(pendingAction, 'toggleDeleteModal')
      .mockImplementation(mockAction);

    renderWrapper(initialState);

    const cancelBtn = screen.getByText(I18N_COMMON_TEXT.CANCEL);
    cancelBtn.click();

    expect(mockAction).toBeCalledWith({ storeId, code: '' });
  });

  it('handleDelete', () => {
    const mockAction = generateMockAction();
    jest
      .spyOn(pendingAction, 'triggerDeletePendingLetter')
      .mockImplementation(mockAction);

    renderWrapper(initialState);

    const deleteBtn = screen.getByText(I18N_COMMON_TEXT.DELETE);
    deleteBtn.click();

    expect(mockAction).toBeCalledWith({
      storeId,
      postData: { accountId: storeId, code: '' }
    });
  });
});
