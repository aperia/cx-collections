import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// helpers
import isEmpty from 'lodash.isempty';
import isArray from 'lodash.isarray';
import { formatTimeDefault } from 'app/helpers';

// types
import {
  SendLetterInitialState,
  SendPendingLetterArg,
  SendPendingLetterPayload
} from '../types';

// service
import pendingLetterService from '../pendingListService';

// constants
import { CONFIG_DEFAULT_BODY } from '../constants';
import { FormatTime } from 'app/constants/enums';

export const getPendingLetter = createAsyncThunk<
  SendPendingLetterPayload,
  SendPendingLetterArg,
  ThunkAPIConfig
>('getPendingLetter', async (args, thunkAPI) => {
  try {
    const { postData } = args;
    const data = await pendingLetterService.getPendingLetters({
      ...postData,
      ...CONFIG_DEFAULT_BODY
    });
    return data;
  } catch (error) {
    return thunkAPI.rejectWithValue({
      errorMessage: `${error?.status || error?.response?.status}`
    });
  }
});

export const getPendingLetterBuilder = (
  builder: ActionReducerMapBuilder<SendLetterInitialState>
) => {
  builder
    .addCase(getPendingLetter.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: true
      };
    })
    .addCase(getPendingLetter.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;
      const pendingLetter = isArray(data?.letterPendingFields)
        ? data?.letterPendingFields.map(letter => {
            const {
              operatorTerminalIdentification,
              inputDate,
              letterNumber,
              inputTime,
              letterDescription
            } = letter;
            const formatedDate = formatTimeDefault(
              inputDate,
              FormatTime.UTCDate
            );

            return {
              ...letter,
              operatorTerminalIdentification: operatorTerminalIdentification
                ?.trim()
                ?.substring(operatorTerminalIdentification?.length - 4),
              code: `${letterNumber}_${formatedDate}_${inputTime}_${letterDescription}`
            };
          })
        : [];
      draftState[storeId] = {
        ...draftState[storeId],
        loading: false,
        error: false,
        data: pendingLetter,
        noData: isEmpty(pendingLetter)
      };
    })
    .addCase(getPendingLetter.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: false,
        error: action.payload?.errorMessage !== '455',
        noData: action.payload?.errorMessage === '455'
      };
    });
};
