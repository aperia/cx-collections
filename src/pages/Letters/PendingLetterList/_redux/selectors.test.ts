import * as selector from './selectors';

// mocks
import { useSelectorMock } from 'app/test-utils/useSelectorMock';

// utils
import { storeId } from 'app/test-utils';

const store: Partial<RootState> = {
  pendingLetter: {
    [storeId]: {
      loading: true,
      data: [
        {
          letterNumber: ' a',
          inputDate: '123',
          inputTime: '12',
          letterDescription: 'des',
          letterOriginCode: 'DES',
          operatorTerminalIdentification: 'des'
        },
        {
          letterNumber: ' a',
          inputDate: '123',
          inputTime: '12',
          letterDescription: 'des',
          letterOriginCode: 'DES',
          operatorTerminalIdentification: 'des'
        },
        {
          letterNumber: ' b',
          inputDate: '456',
          inputTime: '15',
          letterDescription: 'des',
          letterOriginCode: 'DES',
          operatorTerminalIdentification: 'des'
        },
        {
          letterNumber: ' b',
          inputDate: '456',
          inputTime: '15',
          letterDescription: 'des',
          letterOriginCode: 'DES',
          operatorTerminalIdentification: 'des'
        }
      ],
      loadingModal: true,
      reload: true,
      error: true,
      noData: true,
      code: 'code'
    }
  }
};

const storeEmpty: Partial<RootState> = {
  pendingLetter: { [storeId]: {} }
};

describe('redux-store > letter > selector', () => {
  it('selectLoadingPendingLetter', () => {
    const data = useSelectorMock(
      selector.selectLoadingPendingLetter,
      store,
      storeId
    );
    const expected = store.pendingLetter![storeId].loading;
    expect(data).toEqual(expected);
  });

  it('selectLoadingPendingLetter empty state', () => {
    const data = useSelectorMock(
      selector.selectLoadingPendingLetter,
      storeEmpty,
      storeId
    );
    expect(data).toEqual(false);
  });

  it('selectPendingData', () => {
    const data = useSelectorMock(selector.selectPendingData, store, storeId);
    expect(data).toEqual([
      {
        letterNumber: ' b',
        inputDate: '456',
        inputTime: '15',
        letterDescription: 'des',
        letterOriginCode: 'DES',
        operatorTerminalIdentification: 'des'
      },
      {
        letterNumber: ' b',
        inputDate: '456',
        inputTime: '15',
        letterDescription: 'des',
        letterOriginCode: 'DES',
        operatorTerminalIdentification: 'des'
      },
      {
        letterNumber: ' a',
        inputDate: '123',
        inputTime: '12',
        letterDescription: 'des',
        letterOriginCode: 'DES',
        operatorTerminalIdentification: 'des'
      },
      {
        letterNumber: ' a',
        inputDate: '123',
        inputTime: '12',
        letterDescription: 'des',
        letterOriginCode: 'DES',
        operatorTerminalIdentification: 'des'
      }
    ]);
  });

  it('selectPendingData empty state', () => {
    const data = useSelectorMock(
      selector.selectPendingData,
      storeEmpty,
      storeId
    );
    expect(data).toEqual([]);
  });

  it('selectOpenModalPending empty state', () => {
    const data = useSelectorMock(
      selector.selectOpenModalPending,
      storeEmpty,
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('selectLoadingDelete', () => {
    const data = useSelectorMock(selector.selectLoadingDelete, store, storeId);
    const expected = store.pendingLetter![storeId].loadingModal;
    expect(data).toEqual(expected);
  });

  it('selectLoadingDelete empty state', () => {
    const data = useSelectorMock(
      selector.selectLoadingDelete,
      storeEmpty,
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('selectReload', () => {
    const data = useSelectorMock(selector.selectReload, store, storeId);
    const expected = store.pendingLetter![storeId].reload;
    expect(data).toEqual(expected);
  });

  it('selectReload  empty state', () => {
    const data = useSelectorMock(selector.selectReload, storeEmpty, storeId);
    expect(data).toBeFalsy();
  });

  it('selectErrorPendingLetter', () => {
    const data = useSelectorMock(
      selector.selectErrorPendingLetter,
      store,
      storeId
    );
    const expected = store.pendingLetter![storeId].error;
    expect(data).toEqual(expected);
  });

  it('selectErrorPendingLetter empty state', () => {
    const data = useSelectorMock(
      selector.selectErrorPendingLetter,
      storeEmpty,
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('selectNoPendingLetter', () => {
    const data = useSelectorMock(
      selector.selectNoPendingLetter,
      store,
      storeId
    );
    const expected = store.pendingLetter![storeId].noData;
    expect(data).toEqual(expected);
  });

  it('selectNoPendingLetter empty state', () => {
    const data = useSelectorMock(
      selector.selectNoPendingLetter,
      storeEmpty,
      storeId
    );
    expect(data).toBeFalsy();
  });

  it('selectCodePendingLetter', () => {
    const data = useSelectorMock(
      selector.selectCodePendingLetter,
      store,
      storeId
    );
    const expected = store.pendingLetter![storeId].code;
    expect(data).toEqual(expected);
  });

  it('selectCodePendingLetter empty state', () => {
    const data = useSelectorMock(
      selector.selectCodePendingLetter,
      storeEmpty,
      storeId
    );
    expect(data).toBeFalsy();
  });
});
