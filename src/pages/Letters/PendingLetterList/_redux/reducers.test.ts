import { pendingAction, reducer } from './reducers';

// utils
import { storeId } from 'app/test-utils';
import { SendLetterInitialState } from '../types';

const store: SendLetterInitialState = { [storeId]: { openDeleteModal: true } };

describe('Test reducer', () => {
  it('removeStore action', () => {
    const state = reducer(store, pendingAction.removeStore({ storeId }));

    expect(state).toEqual({});
  });

  it('toggleDeleteModal action', () => {
    const state = reducer(
      store,
      pendingAction.toggleDeleteModal({ storeId, code: 'code' })
    );

    expect(state[storeId]).toEqual({
      code: 'code',
      openDeleteModal: !store[storeId].openDeleteModal
    });
  });

  it('updateReload action', () => {
    const state = reducer(
      store,
      pendingAction.updateReload({ storeId, reload: true })
    );

    expect(state).toEqual({
      [storeId]: {
        reload: true,
        openDeleteModal: store[storeId].openDeleteModal
      }
    });
  });
});
