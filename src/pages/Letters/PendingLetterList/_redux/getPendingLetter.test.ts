import './reducers';
import { getPendingLetter } from './getPendingLetter';

// types
import { PhoneArg } from 'pages/AccountDetails/PhoneAndAddress/types';

// redux
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';

// utils
import { responseDefault, storeId } from 'app/test-utils';
import { PendingLetterData } from '../types';

// service
import pendingLetterService from '../pendingListService';

const requestData: PhoneArg = {
  storeId,
  postData: {
    common: { accountId: storeId }
  }
};

const letterPendingFields: PendingLetterData[] = [
  {
    inputDate: '123',
    inputTime: '123',
    letterDescription: '123',
    letterNumber: '123',
    letterOriginCode: '123',
    operatorTerminalIdentification: '123'
  }
];

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('redux-store > getPendingLetter', () => {
  it('async thunk', async () => {
    spy = jest
      .spyOn(pendingLetterService, 'getPendingLetters')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          letterPendingFields
        }
      });

    const store = createStore(rootReducer, {});
    const response = await getPendingLetter(requestData)(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig['extra']
    );

    expect(response.payload).toEqual({
      ...responseDefault,
      data: {
        letterPendingFields
      }
    });
  });

  it('async thunk empty data', async () => {
    spy = jest
      .spyOn(pendingLetterService, 'getPendingLetters')
      .mockResolvedValue({ ...responseDefault });

    const store = createStore(rootReducer, {});
    const response = await getPendingLetter(requestData)(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig['extra']
    );

    expect(response.payload).toEqual({ ...responseDefault });
  });

  it('reject', async () => {
    spy = jest
      .spyOn(pendingLetterService, 'getPendingLetters')
      .mockResolvedValue({ ...responseDefault });

    const store = createStore(rootReducer, {});
    const response = await getPendingLetter(requestData)(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig['extra']
    );

    expect(response.payload).toEqual({ ...responseDefault });
  });
});
