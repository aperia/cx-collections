import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { getPendingLetter, getPendingLetterBuilder } from './getPendingLetter';
import {
  deletePendingLetter,
  deletePendingLetterBuilder,
  triggerDeletePendingLetter
} from './deletePendingLetter';

// types
import {
  SendLetterInitialState,
  ToggleDeleteModal,
  UpdateReload
} from '../types';

const { actions, reducer } = createSlice({
  name: 'pendingLetter',
  initialState: {} as SendLetterInitialState,
  reducers: {
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    toggleDeleteModal: (draftState, action: PayloadAction<ToggleDeleteModal>) => {
      const { storeId, code } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        openDeleteModal: !draftState[storeId].openDeleteModal,
        code
      };
    },
    updateReload: (draftState, action: PayloadAction<UpdateReload>) => {
      const { storeId, reload } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        reload
      };
    }
  },
  extraReducers: builder => {
    getPendingLetterBuilder(builder);
    deletePendingLetterBuilder(builder);
  }
});

const pendingAction = {
  ...actions,
  getPendingLetter,
  deletePendingLetter,
  triggerDeletePendingLetter
};

export { pendingAction, reducer };
