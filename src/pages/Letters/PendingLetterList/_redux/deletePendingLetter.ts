import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';

// types
import { SendLetterInitialState, DeleteLetterArg } from '../types';

// service
import pendingLetterService from '../pendingListService';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { LETTER_ACTIONS } from 'pages/Memos/constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { CHRONICLE_LETTERS_KEY } from 'pages/ClientConfiguration/Memos/constants';

export const deletePendingLetter = createAsyncThunk<
  MagicKeyValue,
  DeleteLetterArg,
  ThunkAPIConfig
>('pendingLetter/delete', async (args, thunkAPI) => {
  const { postData } = args;
  const code = postData?.code.split('_');
  const originationDate = code[1]
    .split('-')
    .reduce((char, current) => char + current, '');
  const body = {
    common: {
      accountId: postData?.accountId
    },
    letterIdentifier: code[0],
    originationCode: 'S',
    originationDate: originationDate,
    originationTime: code[2]
  };
  try {
    return await pendingLetterService.deletePendingLetter(body);
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerDeletePendingLetter = createAsyncThunk<
  void,
  DeleteLetterArg,
  ThunkAPIConfig
>('triggerDelete', async (args, { dispatch }) => {
  const {
    storeId,
    postData: { accountId, code }
  } = args;
  const { operatorID = '' } = window?.appConfig?.commonConfig || {};

  const response = await dispatch(deletePendingLetter(args));

  // delete success
  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_COMMON_TEXT.DELETE_LETTER_SUCCESS
        })
      );

      dispatch(
        memoActions.postMemo(LETTER_ACTIONS.DELETE_PENDING_LETTER, {
          storeId,
          accEValue: accountId,
          chronicleKey: CHRONICLE_LETTERS_KEY,
          letterId: code.split('_')[0],
          letterDescription: code.split('_')[3],
          operatorId: operatorID
        })
      );
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          forSection: 'inModalBody',
          storeId
        })
      );
    });
  }

  // delete error
  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_COMMON_TEXT.DELETE_LETTER_FAIL
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          forSection: 'inModalBody',
          storeId,
          apiResponse: response.payload?.response
        })
      );
    });
  }
});

export const deletePendingLetterBuilder = (
  builder: ActionReducerMapBuilder<SendLetterInitialState>
) => {
  builder
    .addCase(deletePendingLetter.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loadingModal: true
      };
    })
    .addCase(deletePendingLetter.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        loadingModal: false,
        reload: true,
        openDeleteModal: false
      };
    })
    .addCase(deletePendingLetter.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loadingModal: false,
        openDeleteModal: false
      };
    });
};
