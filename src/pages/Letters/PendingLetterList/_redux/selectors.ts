import { createSelector } from '@reduxjs/toolkit';
import { EMPTY_ARRAY } from 'app/constants';
import { PendingLetterData } from '../types';

export const selectLoadingPendingLetter = createSelector(
  (states: RootState, storeId: string) =>
    states.pendingLetter[storeId]?.loading || false,
  (loading: boolean) => loading
);

export const selectPendingData = createSelector(
  (states: RootState, storeId: string) =>
    states.pendingLetter[storeId]?.data || [],
  (data: PendingLetterData[]) => {
    const groupData = [...data].reduce(
      (group: Record<string, Array<PendingLetterData>>, item) => {
        const currentGroup = group[item.inputDate] || EMPTY_ARRAY;
        group[item.inputDate] = [...currentGroup, item];

        return group;
      },
      {}
    );

    const groupKys = Object.keys(groupData);
    const sortGroupDataCallback = (current: string, next: string) => {
      return new Date(next).getTime() - new Date(current).getTime();
    };

    const sortResultCallback = (
      left: PendingLetterData,
      right: PendingLetterData
    ): number => {
      return parseInt(right.inputTime) - parseInt(left.inputTime);
    };

    return [...groupKys]
      .sort(sortGroupDataCallback)
      .reduce((sortData: Array<PendingLetterData>, groupName) => {
        groupData[groupName].sort(sortResultCallback);

        sortData = [...sortData, ...groupData[groupName]];

        return sortData;
      }, []);
  }
);

export const selectOpenModalPending = createSelector(
  (states: RootState, storeId: string) =>
    states.pendingLetter[storeId]?.openDeleteModal || false,
  (openModal: boolean) => openModal
);

export const selectLoadingDelete = createSelector(
  (states: RootState, storeId: string) =>
    states.pendingLetter[storeId]?.loadingModal || false,
  (loading: boolean) => loading
);

export const selectReload = createSelector(
  (states: RootState, storeId: string) =>
    states.pendingLetter[storeId]?.reload || false,
  (reload: boolean) => reload
);

export const selectErrorPendingLetter = createSelector(
  (states: RootState, storeId: string) =>
    states.pendingLetter[storeId]?.error || false,
  (error: boolean) => error
);

export const selectNoPendingLetter = createSelector(
  (states: RootState, storeId: string) =>
    states.pendingLetter[storeId]?.noData || false,
  (noData: boolean) => noData
);

export const selectCodePendingLetter = createSelector(
  (states: RootState, storeId: string) =>
    states.pendingLetter[storeId]?.code || '',
  (code: string) => code
);
