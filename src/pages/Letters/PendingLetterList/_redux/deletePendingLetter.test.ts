import './reducers';
import { isRejected, isFulfilled } from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import {
  deletePendingLetter,
  triggerDeletePendingLetter
} from './deletePendingLetter';

// types
import { DeleteLetterArg, PendingLetterData } from '../types';

// redux
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';

// utils
import { responseDefault, storeId } from 'app/test-utils';
import { Dispatch } from 'react';

// service
import pendingLetterService from '../pendingListService';

jest.mock('pages/__commons/ToastNotifications/_redux', () => ({
  ...jest.requireActual('pages/__commons/ToastNotifications/_redux'),
  actionsToast: { addToast: jest.fn() }
}));

jest.mock('@reduxjs/toolkit', () => ({
  ...jest.requireActual('@reduxjs/toolkit'),
  isRejected: jest.fn(),
  isFulfilled: jest.fn()
}));

const requestData: DeleteLetterArg = {
  storeId,
  postData: { accountId: storeId, code: 'per_code-mock' }
};

const lettersData: PendingLetterData[] = [
  {
    letterNumber: '1',
    inputDate: '',
    inputTime: '',
    letterDescription: '',
    letterOriginCode: '',
    operatorTerminalIdentification: ''
  }
];

let spy: jest.SpyInstance;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('redux-store > deletePendingLetter', () => {
  it('async thunk', async () => {
    spy = jest
      .spyOn(pendingLetterService, 'deletePendingLetter')
      .mockResolvedValue({ ...responseDefault, data: lettersData });

    const store = createStore(rootReducer, {
      pendingLetter: { [storeId]: {} }
    });

    const response = await deletePendingLetter(requestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ ...responseDefault, data: lettersData });
  });

  it('reject', async () => {
    spy = jest
      .spyOn(pendingLetterService, 'deletePendingLetter')
      .mockRejectedValue({});

    const store = createStore(rootReducer, {
      pendingLetter: { [storeId]: {} }
    });
    const response = await deletePendingLetter(requestData)(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig['extra']
    );

    expect(isRejected(response)).toBeTruthy;
  });
});

describe('redux-store > triggerDeletePendingLetter', () => {
  it('async thunk', async () => {
    const store = createStore(rootReducer, {});
    await triggerDeletePendingLetter(requestData)(
      (() => null as unknown) as Dispatch<object>,
      store.getState,
      undefined as unknown as ThunkAPIConfig['extra']
    );

    expect(actionsToast.addToast).not.toBeCalled();
  });

  it('async thunk isFulfilled and isRejected', async () => {
    (isFulfilled as unknown as jest.Mock).mockImplementation(() => true);
    (isRejected as unknown as jest.Mock).mockImplementation(() => true);

    // with 1 [code]
    const newRequestData = { ...requestData };
    newRequestData.postData.code = 'code_1';

    const store = createStore(rootReducer, {});
    await triggerDeletePendingLetter(requestData)(
      (() => null as unknown) as Dispatch<object>,
      store.getState,
      undefined as unknown as ThunkAPIConfig['extra']
    );

    expect(actionsToast.addToast).toBeCalled();
  });
});
