import React from 'react';
import cloneDeep from 'lodash.clonedeep';
import { AccountDetailProvider } from 'app/hooks';
import { screen } from '@testing-library/react';

// utils
import {
  renderMockStore,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';

// components
import PendingLetterGrid from './PendingLetterGrid';

// redux
import { pendingAction } from './_redux/reducers';
import { PendingLetterData } from './types';
import { formatStringTime } from './helpers';
import * as entitlementsHelpers from 'app/entitlements/helpers';

const initialState: Partial<RootState> = {
  pendingLetter: {
    [storeId]: {
      data: [
        {
          letterDescription: '',
          letterOriginCode: 'id_3',
          letterNumber: '1',
          inputDate: '04-14-2021',
          inputTime: '08:00',
          operatorTerminalIdentification: '',
          view: { code: 1 }
        } as unknown as PendingLetterData
      ]
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <PendingLetterGrid />
    </AccountDetailProvider>,
    {
      initialState
    }
  );
};

const EMPTY_STRING = '000000';

jest.mock('app/constants', () => {
  const constants = jest.requireActual('app/constants');
  return {
    ...constants,
    EMPTY_STRING
  };
});

jest.mock('app/helpers/formatTime', () => {
  const helpers = jest.requireActual('app/helpers/formatTime');
  return {
    ...helpers,
    convertAPIDateToView: () => EMPTY_STRING
  };
});


beforeEach(() => {
  jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
});

describe('Test PendingLetterGrid component', () => {
  describe('Render UI', () => {
    it('with inputDate and inputTime provided', () => {
      renderWrapper(initialState);

      expect(screen.getByRole('table')).toBeInTheDocument();
    });

    it('with inputDate and inputTime not provided', () => {
      const _initialState = cloneDeep(initialState);
      _initialState.pendingLetter![storeId].data![0].inputDate = '';
      _initialState.pendingLetter![storeId].data![0].inputTime = '';
      renderWrapper(_initialState);

      // check inputDate
      expect(screen.getByText(EMPTY_STRING)).toBeInTheDocument();
      // check inputTime
      expect(
        screen.getByText(formatStringTime(EMPTY_STRING))
      ).toBeInTheDocument();
    });
  });

  describe('Actions', () => {
    it('handleDeleteLetter', () => {
      const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
      jest
        .spyOn(pendingAction, 'toggleDeleteModal')
        .mockImplementation(mockAction);

      renderWrapper(initialState);
      const lastPageBtn = screen.getByText('txt_delete');

      lastPageBtn.click();

      expect(mockAction).toBeCalledWith({ storeId });
    });
  });
});
