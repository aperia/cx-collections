import { apiService } from 'app/utils/api.service';

// types
import { DeleteLetterPostData, SendPendingLetterPostData } from './types';

const pendingListService = {
  getPendingLetters: (postData: SendPendingLetterPostData) => {
    const url = window.appConfig?.api?.sendLetter?.getPendingLetters || '';

    return apiService.post(url, { ...postData });
  },

  deletePendingLetter: (postData: DeleteLetterPostData) => {
    const url = window.appConfig?.api?.sendLetter?.deletePendingLetter || '';

    return apiService.post(url, { ...postData });
  }
};

export default pendingListService;
