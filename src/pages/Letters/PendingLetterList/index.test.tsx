import React from 'react';
import { AccountDetailProvider } from 'app/hooks';
import { screen } from '@testing-library/react';

// utils
import {
  renderMockStore,
  storeId
} from 'app/test-utils/renderComponentWithMockStore';

// components
import PendingLetterList from './index';

// redux
import { pendingAction } from './_redux/reducers';

const initialState: Partial<RootState> = {
  pendingLetter: { [storeId]: {} }
};

const errorState: Partial<RootState> = {
  pendingLetter: { [storeId]: { error: true, noData: true } }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue: storeId }}>
      <PendingLetterList />
    </AccountDetailProvider>,
    {
      initialState
    }
  );
};

const generateMockAction = () =>
  jest.fn().mockImplementation(() => ({ type: 'type' }));

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper(initialState);

    expect(screen.getByText('txt_pending_letter_list')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('handleReload', () => {
    const mockAction = generateMockAction();
    jest
      .spyOn(pendingAction, 'getPendingLetter')
      .mockImplementation(mockAction);

    renderWrapper(errorState);

    const reloadBtn = screen.getByRole('button');
    reloadBtn.click();

    expect(mockAction).toBeCalledWith({
      storeId,
      postData: {
        common: {
          accountId: storeId
        }
      }
    });
  });
});
