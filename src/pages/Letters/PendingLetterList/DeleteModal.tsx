import React from 'react';

// components
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

// redux
import { useDispatch } from 'react-redux';
import { pendingAction } from './_redux/reducers';
import {
  selectCodePendingLetter,
  selectLoadingDelete,
  selectOpenModalPending
} from './_redux/selectors';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// i18n
import { I18N_COMMON_TEXT, I18N_PENDING_LETTER } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';

const DeleteModal: React.FC = () => {
  const dispatch = useDispatch();
  const { storeId, accEValue } = useAccountDetail();
  const { t } = useTranslation();

  const openModal = useStoreIdSelector<boolean>(selectOpenModalPending);
  const loading = useStoreIdSelector<boolean>(selectLoadingDelete);
  const code = useStoreIdSelector<string>(selectCodePendingLetter);

  const handleCloseModal = () =>
    dispatch(pendingAction.toggleDeleteModal({ storeId, code: '' }));

  const handleDelete = () =>
    dispatch(
      pendingAction.triggerDeletePendingLetter({
        storeId,
        postData: { accountId: accEValue, code }
      })
    );

  return (
    <Modal
      xs
      show={openModal}
      loading={loading}
      dataTestId="pendingLetterDeleteModal"
    >
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId="pendingLetterDeleteModal_header"
      >
        <ModalTitle dataTestId="pendingLetterDeleteModal_header-title">
          {t(I18N_PENDING_LETTER.CONFIRM_HEADER)}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <p
          data-testid={genAmtId(
            'pendingLetterDeleteModal_body',
            'description',
            ''
          )}
        >
          {t(I18N_PENDING_LETTER.CONFIRM_BODY)}
        </p>
      </ModalBody>
      <ModalFooter dataTestId="pendingLetterDeleteModal_footer">
        <Button
          variant="secondary"
          onClick={handleCloseModal}
          dataTestId="pendingLetterDeleteModal_footer-cancel-btn"
        >
          {t(I18N_COMMON_TEXT.CANCEL)}
        </Button>
        <Button
          variant="danger"
          onClick={handleDelete}
          dataTestId="pendingLetterDeleteModal_footer-delete-btn"
        >
          {t(I18N_COMMON_TEXT.DELETE)}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default DeleteModal;
