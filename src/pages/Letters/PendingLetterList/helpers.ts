export const formatStringTime = (time: string) => {
  // input string '034202'
  // output string '03:42:02 AM'
  const h = time.slice(0, 2);
  const m = time.slice(2, 4);
  const s = time.slice(4, 6);
  const ap = parseInt(h) > 12 ? 'PM' : 'AM';
  const temp = parseInt(h) % 12;
  const parseH = parseInt(h) < 12 ? h : temp >= 10 ? temp : `0${temp}`;
  return `${parseH}:${m}:${s} ${ap}`;
};