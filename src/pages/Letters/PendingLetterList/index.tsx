import React, { useEffect, useLayoutEffect, useRef } from 'react';

// components
import { Icon } from 'app/_libraries/_dls/components';
import FailedApiReload from 'app/components/FailedApiReload';
import PendingLetterGrid from './PendingLetterGrid';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// redux
import { batch, useDispatch } from 'react-redux';
import { pendingAction } from './_redux/reducers';
import {
  selectErrorPendingLetter,
  selectLoadingPendingLetter,
  selectNoPendingLetter,
  selectReload
} from './_redux/selectors';

// helpers
import classNames from 'classnames';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface PendingLetterListProps {}

const PendingLetterList: React.FC<PendingLetterListProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId, accEValue } = useAccountDetail();
  const divRef = useRef<HTMLDivElement | null>(null);

  const loading = useStoreIdSelector<boolean>(selectLoadingPendingLetter);
  const reload = useStoreIdSelector<boolean>(selectReload);
  const isError = useStoreIdSelector<boolean>(selectErrorPendingLetter);
  const noPendingLetter = useStoreIdSelector<boolean>(selectNoPendingLetter);

  const handleReload = () =>
    dispatch(pendingAction.updateReload({ storeId, reload: true }));

  // get api
  useEffect(() => {
    dispatch(
      pendingAction.getPendingLetter({
        storeId,
        postData: {
          common: {
            accountId: accEValue
          }
        }
      })
    );
  }, [accEValue, dispatch, storeId]);

  // reload
  useEffect(() => {
    if (!reload) return;
    batch(() => {
      dispatch(
        pendingAction.getPendingLetter({
          storeId,
          postData: {
            common: {
              accountId: accEValue
            }
          }
        })
      );
      dispatch(pendingAction.updateReload({ storeId, reload: false }));
    });
  }, [accEValue, dispatch, reload, storeId]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.minHeight = '500px');
  }, []);

  return (
    <div className={classNames({ loading }, 'px-24 pt-24')} ref={divRef}>
      <h5
        className="pb-16"
        data-testid={genAmtId('pendingLetterList', 'title', '')}
      >
        {t('txt_pending_letter_list')}
      </h5>
      {isError && (
        <FailedApiReload
          id="account-detail-overview-fail"
          dataTestId="pendingLetterList-fail"
          className="mt-80"
          onReload={handleReload}
        />
      )}
      {noPendingLetter && (
        <div className="text-center my-80">
          <Icon name="file" className="fs-80 color-light-l12" />
          <p
            className="mt-20 text-secondary"
            data-testid={genAmtId('pendingLetterList', 'no-pending-letter', '')}
          >
            {t('txt_no_pending_letter_to_display')}
          </p>
        </div>
      )}
      {!isError && !noPendingLetter && <PendingLetterGrid />}
    </div>
  );
};

export default PendingLetterList;
