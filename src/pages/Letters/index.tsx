import React from 'react';

// components/types
import {
  HorizontalTabs,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import LetterList from './LetterList';
import PendingLetterList from './PendingLetterList';
import { SelectCallback } from 'react-bootstrap/esm/helpers';
import SendLetter from 'pages/Letters/SendLetter';
import TimerWorking from 'pages/TimerWorking';
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { batch, useDispatch } from 'react-redux';
import { letterAction } from 'pages/Letters/_redux/reducers';
import { letterListAction } from './LetterList/_redux/reducers';
import { pendingAction } from './PendingLetterList/_redux/reducers';
import {
  selectActiveKey,
  selectOpenModal
} from 'pages/Letters/_redux/selectors';

// constant
import { KEY } from './constants';
import { sendLetterActions } from './SendLetter/_redux/reducers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

export interface LetterProps {}

const Letter: React.FC<LetterProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();

  const show = useStoreIdSelector<boolean>(selectOpenModal);
  const activeKey =
    useStoreIdSelector<string>(selectActiveKey) ||
    (checkPermission(PERMISSIONS.LETTER_VIEW_LETTER_LIST, storeId)
      ? KEY.letters
      : KEY.pending);

  const handleClose = () => {
    batch(() => {
      dispatch(letterAction.removeStore({ storeId }));
      dispatch(letterListAction.removeStore({ storeId }));
      dispatch(pendingAction.removeStore({ storeId }));
      dispatch(sendLetterActions.removeStore({ storeId }));
    });
  };

  const handleSelect: SelectCallback = activeTab => {
    batch(() => {
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inModalBody'
        })
      );
      dispatch(
        letterAction.updateSelectTab({ storeId, activeKey: activeTab || '' })
      );
    });
  };

  return (
    <Modal dataTestId="sendLetter_modal" full show={show} enforceFocus={false}>
      <ModalHeader
        dataTestId="sendLetter_header"
        border
        closeButton
        onHide={handleClose}
      >
        <ModalTitle
          className="d-flex align-items-center"
          dataTestId="sendLetter_header_title"
        >
          {t('txt_send_letter')} <TimerWorking className="ml-16" />
        </ModalTitle>
      </ModalHeader>
      <ModalBodyWithApiError
        dataTestId="sendLetter_body_load-error"
        className="no-padding"
        apiErrorClassName="mb-24 ml-24 mt-24"
        storeId={storeId}
      >
        <HorizontalTabs
          dataTestId="sendLetter_tab"
          onSelect={handleSelect}
          activeKey={activeKey}
          mountOnEnter
          level2
        >
          {checkPermission(PERMISSIONS.LETTER_VIEW_LETTER_LIST, storeId) ? (
            <HorizontalTabs.Tab
              key={`${storeId}-${KEY.letters}`}
              eventKey={KEY.letters}
              title={t('txt_letters')}
            >
              <LetterList />
            </HorizontalTabs.Tab>
          ) : (
            <></>
          )}
          {checkPermission(PERMISSIONS.LETTER_VIEW_PENDING_LIST, storeId) ? (
            <HorizontalTabs.Tab
              key={`${storeId}-${KEY.pending}`}
              eventKey={KEY.pending}
              title={t('txt_pending_letters')}
            >
              <PendingLetterList />
            </HorizontalTabs.Tab>
          ) : (
            <></>
          )}
        </HorizontalTabs>
        <SendLetter />
      </ModalBodyWithApiError>
      <ModalFooter
        dataTestId="sendLetter_footer"
        border
        cancelButtonText={t('txt_close')}
        onCancel={handleClose}
      />
    </Modal>
  );
};

export default Letter;
