import React from 'react';

// helpers
import { renderMockStoreId } from 'app/test-utils';
import { waitFor } from '@testing-library/react';

// components
import ApiErrorNotification from '.';

describe('Test Api Error Notification', () => {
  it('render to UI', () => {
    waitFor(() => {
      const { wrapper } = renderMockStoreId(<ApiErrorNotification />);

      expect(wrapper.container.innerHTML).toEqual('');
    });
  });
});
