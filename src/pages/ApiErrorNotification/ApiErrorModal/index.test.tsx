import React from 'react';
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import ApiErrorModal from './index';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { apiErrorNotificationAction } from '../_redux/reducers';
import { TextAreaProps } from 'app/_libraries/_dls/components';

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');

  return {
    ...dlsComponents,
    TextArea: ({ value }: TextAreaProps) => <div>{value}</div>
  };
});

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<ApiErrorModal />, {
    initialState
  });
};

const apiErrorNotificationActionSpy = mockActionCreator(
  apiErrorNotificationAction
);
Object.defineProperty(window.navigator, 'clipboard', {
  value: {
    writeText: (inputStr: string) => Promise.resolve()
  }
});

describe('Api Error Modal', () => {
  it('should not open when there is no Error', () => {
    renderWrapper({});

    expect(
      screen.queryByText(I18N_COMMON_TEXT.API_ERROR_DETAIL)
    ).not.toBeInTheDocument();
  });
  it('should not open when there is no Error', () => {
    renderWrapper({
      apiErrorNotification: {
        [storeId]: {
          openModalFor: undefined
        }
      }
    });

    expect(
      screen.queryByText(I18N_COMMON_TEXT.API_ERROR_DETAIL)
    ).not.toBeInTheDocument();
  });

  it('should open Modal when there is  Error', () => {
    renderWrapper({
      apiErrorNotification: {
        [storeId]: {
          openModalFor: 'inModalBody',
          errorDetail: {
            inModalBody: {
              request: 'request error'
            }
          }
        }
      }
    });

    expect(
      screen.queryByText(I18N_COMMON_TEXT.API_ERROR_DETAIL)
    ).toBeInTheDocument();
    expect(screen.queryByText(/request error/)).toBeInTheDocument();
  });

  it('should open Modal when there is  Error', () => {
    renderWrapper({
      apiErrorNotification: {
        [storeId]: {
          openModalFor: 'inModalBody',
          errorDetail: {}
        }
      }
    });
    expect(
      screen.queryByText(I18N_COMMON_TEXT.API_ERROR_DETAIL)
    ).toBeInTheDocument();
  });

  it('handleCopyAction', () => {
    const { wrapper } = renderWrapper({
      apiErrorNotification: {
        [storeId]: {
          openModalFor: 'inModalBody',
          errorDetail: {
            inModalBody: {
              request: '500',
              response: '500'
            }
          }
        }
      }
    });
    const icon = wrapper.baseElement.querySelector('.icon-product-info')!;
    userEvent.click(icon.closest('button')!);
    expect(wrapper.getByText('txt_copied')).toBeInTheDocument();
  });

  it('handleCloseModal', () => {
    const spy = apiErrorNotificationActionSpy('closeApiDetailErrorModal');
    const { wrapper } = renderWrapper({
      apiErrorNotification: {
        [storeId]: {
          openModalFor: 'inModalBody',
          errorDetail: {
            inModalBody: {
              request: '500',
              response: '500'
            }
          }
        }
      }
    });

    const button = wrapper.baseElement.querySelector('.btn-icon-secondary')!;
    userEvent.click(button);
    expect(spy).toHaveBeenCalledWith({
      storeId
    });
  });
});
