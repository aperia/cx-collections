import React, { useMemo, useRef, useState } from 'react';

// components
import {
  Button,
  Icon,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TextArea,
  Tooltip
} from 'app/_libraries/_dls/components';

import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useDispatch, useSelector } from 'react-redux';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import isEmpty from 'lodash.isempty';
import { ApiErrorByStoreId, ApiErrorNotificationState } from '../types';
import { apiErrorNotificationAction } from '../_redux/reducers';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface ApiDetailErrorModalProp {}

const ApiErrorModal: React.FC<ApiDetailErrorModalProp> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [tooltipValue, setTooltipValue] = useState(I18N_COMMON_TEXT.COPY);
  const errorTextBoxRef = useRef<HTMLTextAreaElement | null>(null);

  const testId = 'apiErrorModal';

  const apiErrorState = useSelector<RootState, ApiErrorNotificationState>(
    state => state.apiErrorNotification
  );

  const storeIdHasErrorModal = useMemo<string | undefined>(() => {
    if (isEmpty(apiErrorState)) return undefined;
    const storeIds = Object.keys(apiErrorState);
    return (
      storeIds.find(storeId => apiErrorState?.[storeId]?.openModalFor) || ''
    );
  }, [apiErrorState]);

  const openModalForSectionData = useMemo<ApiErrorByStoreId | undefined>(() => {
    if (!storeIdHasErrorModal) return undefined;
    return apiErrorState[storeIdHasErrorModal];
  }, [apiErrorState, storeIdHasErrorModal]);

  const handleCloseModal = () => {
    dispatch(
      apiErrorNotificationAction.closeApiDetailErrorModal({
        storeId: storeIdHasErrorModal!
      })
    );
  };

  const handleCopyAction = () => {
    setTooltipValue(I18N_COMMON_TEXT.COPIED);
    navigator.clipboard.writeText(errorTextBoxRef.current?.value || '');
  };

  const handleBlur = () => {
    setTooltipValue(I18N_COMMON_TEXT.COPY);
  };

  const copyButton = useMemo(() => {
    return (
      <Tooltip
        placement="top"
        variant="primary"
        element={t(tooltipValue)}
        displayOnClick={true}
      >
        <Button
          variant="icon-secondary"
          onClick={handleCopyAction}
          onMouseOver={handleBlur}
          aria-pressed="true"
          className="mr-n4"
          dataTestId={`${testId}_copyButton`}
        >
          <Icon name="product-info" />
        </Button>
      </Tooltip>
    );
  }, [t, tooltipValue]);

  if (!openModalForSectionData) return null;

  const { errorDetail, openModalFor } = openModalForSectionData;

  const { request = '', response = '' } = errorDetail?.[openModalFor!] || {};

  const errorValue = response + '\n\n' + request;

  return (
    <Modal show={Boolean(openModalFor)} sm dataTestId={testId}>
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId={`${testId}_header`}
      >
        <ModalTitle dataTestId={`${testId}_title`}>
          {t(I18N_COMMON_TEXT.API_ERROR_DETAIL)}
        </ModalTitle>
      </ModalHeader>
      <ModalBody dataTestId={`${testId}_body`}>
        <div className="d-flex justify-content-between mb-16">
          <h5 data-testid={genAmtId(testId, 'payload_title', '')}>
            {t(I18N_COMMON_TEXT.API_ERROR_DETAIL_RESPONSE_REQUEST_PAYLOAD)}
          </h5>
          {copyButton}
        </div>
        <TextArea
          id={`${storeIdHasErrorModal}_api-error-modal`}
          value={errorValue}
          className="resize-none h-300px"
          ref={errorTextBoxRef}
          dataTestId={`${testId}_errorValue`}
        />
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_close')}
        onCancel={handleCloseModal}
        dataTestId={`${testId}_footer`}
      />
    </Modal>
  );
};

export default ApiErrorModal;
