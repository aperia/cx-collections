import React from 'react';
import ApiErrorModal from './ApiErrorModal';

const ApiErrorNotification: React.FC<{}> = () => {
  return <ApiErrorModal />;
};

export default ApiErrorNotification;
