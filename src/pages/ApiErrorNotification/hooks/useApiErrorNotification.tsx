import React, { useCallback, useEffect, useMemo, useRef } from 'react';
import { useStoreIdSelector } from 'app/hooks/useStoreIdSelector';
import { useDispatch } from 'react-redux';
import { ApiErrorSection } from '../types';
import { apiErrorNotificationAction } from '../_redux/reducers';
import { takeApiErrorDetailData } from '../_redux/selectors';
import ApiError from 'app/components/ApiError';
import isEmpty from 'lodash.isempty';
import { getScrollParent } from 'app/helpers';

interface ApiErrorReturned {
  className?: string;
  dataTestId?: string;
}
export type UseApiErrorNotification = (args: {
  storeId: string;
  forSection: ApiErrorSection;
}) => React.FC<ApiErrorReturned>;

export const useApiErrorNotification: UseApiErrorNotification = ({
  forSection,
  storeId
}) => {
  const dispatch = useDispatch();
  const apiErrorRef = useRef<HTMLDivElement | null>(null);
  const handleClickErrorDetail = useCallback(() => {
    dispatch(
      apiErrorNotificationAction.openApiDetailErrorModal({
        forSection,
        storeId
      })
    );
  }, [dispatch, forSection, storeId]);
  const apiErrorDetailData = useStoreIdSelector<
    Record<ApiErrorSection, MagicKeyValue>
  >(takeApiErrorDetailData, storeId);

  const apiErrorData = apiErrorDetailData?.[forSection];
  const hasApiError = !isEmpty(apiErrorData);

  useEffect(() => {
    if (isEmpty(apiErrorData)) return;
    // auto scroll to top when there is an error
    const scrollParentRef = getScrollParent(apiErrorRef.current);
    if (scrollParentRef && typeof scrollParentRef.scrollTo === 'function')
      scrollParentRef.scrollTo({ top: 0, behavior: 'smooth' });
  }, [apiErrorData]);

  const ApiErrorDetail: React.FC<ApiErrorReturned> = useMemo(() => {
    return props => {
      if (!hasApiError) return null;
      return (
        <ApiError
          onClickViewErrorDetail={handleClickErrorDetail}
          {...props}
          ref={apiErrorRef}
        />
      );
    };
  }, [handleClickErrorDetail, hasApiError]);

  return ApiErrorDetail;
};
