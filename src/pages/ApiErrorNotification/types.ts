import { AxiosResponse } from 'axios';

export type ApiErrorSection =
  | 'inModalBody'
  | 'inSnapshot'
  | 'inCollectionFormFlyOut'
  | 'inSendLetterWithVariableModal'
  | 'inAddAndEditPaymentMethodModal'
  | 'inCollectionFormModal'
  | 'inMemoFlyOut'
  | 'inAddressCardholderFlyOut'
  | 'inClientConfigurationFeatureHeader';

export interface ErrorDetail {
  response?: string;
  request?: string;
}

export type ApiStoreId = string | 'clientConfigurationStoreId';

export interface ApiErrorByStoreId {
  openModalFor?: ApiErrorSection;
  errorDetail?: {
    [K in ApiErrorSection]?: ErrorDetail;
  };
}

export interface ApiErrorNotificationState {
  [storeId: string]: ApiErrorByStoreId;
}

export interface ClearAPiErrorPayload {
  storeId: ApiStoreId;
  forSection: ApiErrorSection;
}

export interface OpenApiDetailErrorModalPayload extends ClearAPiErrorPayload {}

export interface UpdateAPiErrorPayload<T = any> extends ClearAPiErrorPayload {
  apiResponse?: AxiosResponse | T;
}

export interface UpdateThunkAPiErrorPayload extends ClearAPiErrorPayload {
  rejectData: RejectedAPIPayload;
}

export interface CloseApiDetailErrorModalPayload {
  storeId: ApiStoreId;
}
