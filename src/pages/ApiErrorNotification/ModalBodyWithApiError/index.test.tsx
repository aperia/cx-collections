import React from 'react';
import { renderMockStoreId, storeId } from 'app/test-utils';

import ModalBodyWithApiError from '.';

import { mockScrollToFn } from 'app/test-utils/mocks/mockProperty';

mockScrollToFn(jest.fn());

describe('Render', () => {
  it('should render with errorModalOpened', () => {
    renderMockStoreId(<ModalBodyWithApiError storeId={storeId} />, {
      initialState: {
        apiErrorNotification: {
          [storeId]: {
            errorDetail: { inModalBody: { request: '' } },
            openModalFor: { x: '1' }
          }
        }
      }
    });
  });

  it('should render without errorModalOpened', () => {
    renderMockStoreId(<ModalBodyWithApiError storeId={storeId} />, {
      initialState: {
        apiErrorNotification: {
          [storeId]: {
            errorDetail: {},
            openModalFor: {}
          }
        }
      }
    });
  });
});
