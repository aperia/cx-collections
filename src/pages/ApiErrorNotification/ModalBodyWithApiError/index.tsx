import React, { useEffect } from 'react';
import { ModalBody, ModalBodyProps } from 'app/_libraries/_dls/components';

import { useDispatch } from 'react-redux';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { useApiErrorNotification } from '../hooks/useApiErrorNotification';
import { useStoreIdSelector } from 'app/hooks';
import { ApiErrorSection } from '../types';
import { takeApiErrorDetailOpenModalForData } from '../_redux/selectors';
import { isEmpty } from 'lodash';

export interface ModalBodyWithApiErrorProp extends ModalBodyProps {
  apiErrorClassName?: string;
  storeId: string;
  dataTestId?: string;
}

const ModalBodyWithApiError: React.FC<ModalBodyWithApiErrorProp> = ({
  children,
  apiErrorClassName,
  storeId,
  dataTestId,
  ...props
}) => {
  const ApiErrorDetail = useApiErrorNotification({
    storeId,
    forSection: 'inModalBody'
  });
  const dispatch = useDispatch();

  const testId = genAmtId(
    dataTestId!,
    'modal-body-with-api-error',
    'ModalBodyWithApiError'
  );

  const errorModalOpened = useStoreIdSelector<
    Record<ApiErrorSection, MagicKeyValue>
  >(takeApiErrorDetailOpenModalForData, storeId);

  useEffect(() => {
    if (isEmpty(errorModalOpened)) {
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inModalBody'
        })
      );
    }
  }, [dispatch, storeId, errorModalOpened]);

  return (
    <ModalBody {...props}>
      <ApiErrorDetail className={`${apiErrorClassName}`} dataTestId={testId} />
      {children}
    </ModalBody>
  );
};

export default ModalBodyWithApiError;
