import { ApiErrorNotificationState, ApiErrorSection } from '../types';
import { apiErrorNotificationAction, reducer } from './reducers';
import { storeId } from 'app/test-utils';
import { AxiosResponse } from 'axios';

const initialState: ApiErrorNotificationState = {};

describe('test reducer', () => {
  describe('test action updateApiError', () => {
    it('errorDetail.inMemoFlyOut should be empty object if apiResponse is undefined', () => {
      const params = {
        storeId,
        apiResponse: undefined,
        forSection: 'inMemoFlyOut' as ApiErrorSection
      };
      const state = reducer(
        initialState,
        apiErrorNotificationAction.updateApiError(params)
      );
      expect(state).toEqual({
        [storeId]: {
          errorDetail: {
            inMemoFlyOut: {}
          }
        }
      });
    });

    it('errorDetail.inMemoFlyOut should be valid object if apiResponse is exists', () => {
      const params = {
        storeId,
        apiResponse: {} as AxiosResponse,
        forSection: 'inMemoFlyOut' as ApiErrorSection
      };
      const state = reducer(
        initialState,
        apiErrorNotificationAction.updateApiError(params)
      );
      expect(state).toEqual({
        [storeId]: {
          errorDetail: {
            inMemoFlyOut: {
              request: undefined,
              response: undefined
            }
          }
        }
      });
    });
  });

  describe('test action updateApiError', () => {
    it('errorDetail.inMemoFlyOut should be empty object if rejectData.payload is undefined', () => {
      const state = reducer(
        initialState,
        apiErrorNotificationAction.updateThunkApiError({
          rejectData: { payload: undefined },
          storeId,
          forSection: 'inMemoFlyOut'
        })
      );
      expect(state).toEqual({
        [storeId]: {
          errorDetail: {
            inMemoFlyOut: {}
          }
        }
      });
    });

    it('errorDetail.inMemoFlyOut should be valid object if rejectData.payload is exists', () => {
      const state = reducer(
        initialState,
        apiErrorNotificationAction.updateThunkApiError({
          rejectData: { payload: { config: { data: undefined } } },
          storeId,
          forSection: 'inMemoFlyOut'
        })
      );
      expect(state).toEqual({
        [storeId]: {
          errorDetail: {
            inMemoFlyOut: {
              request: undefined,
              response: '{"config":{}}'
            }
          }
        }
      });
    });
  });

  describe('other actions', () => {
    it('clearApiErrorData', () => {
      const params = {
        storeId,
        forSection: 'inMemoFlyOut' as ApiErrorSection
      };
      const state = reducer(
        initialState,
        apiErrorNotificationAction.clearApiErrorData(params)
      );
      expect(state).toEqual({
        123456789: {
          errorDetail: {
            inMemoFlyOut: undefined
          }
        }
      });
    });

    it('closeApiDetailErrorModal', () => {
      const params = {
        storeId
      };
      const state = reducer(
        initialState,
        apiErrorNotificationAction.closeApiDetailErrorModal(params)
      );
      expect(state).toEqual({
        123456789: {
          openModalFor: undefined
        }
      });
    });

    it('openApiDetailErrorModal', () => {
      const params = {
        storeId,
        forSection: 'inMemoFlyOut' as ApiErrorSection
      };
      const state = reducer(
        initialState,
        apiErrorNotificationAction.openApiDetailErrorModal(params)
      );
      expect(state).toEqual({
        123456789: {
          openModalFor: 'inMemoFlyOut'
        }
      });
    });
  });
});
