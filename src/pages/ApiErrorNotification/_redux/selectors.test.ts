import { storeId, selectorWrapper } from 'app/test-utils';
import * as selectors from './selectors';

const selectorTrigger = selectorWrapper(
  {
    apiErrorNotification: {
      [storeId]: {
        errorDetail: {},
        openModalFor: 'inAddAndEditPaymentMethodModal'
      }
    }
  },
  { apiErrorNotification: { [storeId]: {} } }
);

describe('Test selectors', () => {
  it('takeApiErrorDetailOpenModalForData', () => {
    const result = selectorTrigger(
      selectors.takeApiErrorDetailOpenModalForData
    );
    expect(result).toEqual({
      data: 'inAddAndEditPaymentMethodModal',
      emptyData: undefined
    });
  });
});
