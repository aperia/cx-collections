import { createSelector } from '@reduxjs/toolkit';

export const getApiErrorSection = (state: RootState, storeId: string) =>
  state.apiErrorNotification[storeId];

export const takeApiErrorDetailData = createSelector(
  getApiErrorSection,
  data => data?.errorDetail
);

export const takeApiErrorDetailOpenModalForData = createSelector(
  getApiErrorSection,
  data => data?.openModalFor
);
