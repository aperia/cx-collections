import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Type
import {
  ApiErrorNotificationState,
  ClearAPiErrorPayload,
  CloseApiDetailErrorModalPayload,
  OpenApiDetailErrorModalPayload,
  UpdateAPiErrorPayload,
  UpdateThunkAPiErrorPayload
} from '../types';

const { actions, reducer } = createSlice({
  name: 'apiErrorNotification',
  initialState: {} as ApiErrorNotificationState,
  reducers: {
    updateApiError: (
      draftState,
      action: PayloadAction<UpdateAPiErrorPayload>
    ) => {
      const { storeId, apiResponse, forSection } = action.payload;
      const request = (apiResponse?.config?.data || apiResponse?.response?.config?.data) as string;
      const response = (apiResponse?.data || apiResponse?.response?.data);
      const errorDetail = apiResponse
        ? {
          request: request,
          response: JSON.stringify(response)
        }
        : {};
      draftState[storeId] = {
        ...draftState[storeId],
        errorDetail: {
          ...(draftState[storeId]?.errorDetail || {}),
          [forSection]: errorDetail
        }
      };
    },
    updateThunkApiError: (
      draftState,
      action: PayloadAction<UpdateThunkAPiErrorPayload>
    ) => {
      const { rejectData, storeId, forSection } = action.payload;
      const { payload } = rejectData;
      const request = payload?.config?.data;
      const errorDetail = payload
        ? {
          request: request,
          response: JSON.stringify(payload)
        }
        : {};

      draftState[storeId] = {
        ...draftState[storeId],
        errorDetail: {
          ...(draftState[storeId]?.errorDetail || {}),
          [forSection]: errorDetail
        }
      };
    },
    clearApiErrorData: (
      draftState,
      action: PayloadAction<ClearAPiErrorPayload>
    ) => {
      const { storeId, forSection } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        errorDetail: {
          ...(draftState[storeId]?.errorDetail || {}),
          [forSection]: undefined
        }
      };
    },
    openApiDetailErrorModal: (
      draftState,
      action: PayloadAction<OpenApiDetailErrorModalPayload>
    ) => {
      const { storeId, forSection } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        openModalFor: forSection
      };
    },
    closeApiDetailErrorModal: (
      draftState,
      action: PayloadAction<CloseApiDetailErrorModalPayload>
    ) => {
      const { storeId } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        openModalFor: undefined
      };
    }
  }
});

export { actions as apiErrorNotificationAction, reducer };
