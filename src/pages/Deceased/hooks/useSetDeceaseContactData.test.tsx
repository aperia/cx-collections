import React from 'react';
import { render, screen } from '@testing-library/react';

import useSetDeceaseContactData, {
  DeceasedContactType
} from './useSetDeceaseContactData';

const MockComponent: React.FC<{
  contactViewRef?: any;
  deceasedContact?: DeceasedContactType[];
  selectedValue?: RefDataValue;
  deceasedParty?: MagicKeyValue;
}> = props => {
  useSetDeceaseContactData(props as Required<typeof props>);

  return <div>nothing to expect</div>;
};

const mockContactViewRef = {
  current: {
    props: {
      onFind: () => ({
        props: {
          props: {
            setData: () => undefined,
            setValue: () => undefined
          }
        }
      })
    }
  }
};

describe('Test useSetDeceaseContactData hook', () => {
  beforeEach(() => jest.useFakeTimers());

  it('should not run useEffect', () => {
    render(<MockComponent />);
    expect(screen.queryByText('nothing to expect')).toBeInTheDocument();
  });

  it('should run useEffect with deceasedContact', () => {
    render(
      <MockComponent
        contactViewRef={mockContactViewRef}
        deceasedContact={[{ active: true }]}
      />
    );
    jest.runAllTimers();
    expect(screen.queryByText('nothing to expect')).toBeInTheDocument();
  });

  it('should run useEffect with empty deceasedContact', () => {
    render(
      <MockComponent contactViewRef={mockContactViewRef} deceasedContact={[]} />
    );
    jest.runAllTimers();
    expect(screen.queryByText('nothing to expect')).toBeInTheDocument();
  });

  it('should return if customSelectedValue falsy', () => {
    render(
      <MockComponent
        contactViewRef={mockContactViewRef}
        deceasedContact={[
          {
            code: 0
          }
        ]}
      />
    );
    jest.runAllTimers();
    expect(screen.queryByText('nothing to expect')).toBeInTheDocument();
  });
});
