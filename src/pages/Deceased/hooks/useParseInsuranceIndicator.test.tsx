import {
  INSURANCE_INDICATOR_CHARGE_CODE_MIN,
  INSURANCE_INDICATOR_CHARGE_DESCRIPTION,
  INSURANCE_INDICATOR_NOT_CHARGE_CODE,
  INSURANCE_INDICATOR_NOT_CHARGE_DESCRIPTION
} from '../constants';
import useParseInsuranceIndicator from './useParseInsuranceIndicator';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

describe('Test useParseInsuranceIndicator hook', () => {
  it('return empty', () => {
    const result = useParseInsuranceIndicator('');

    expect(result).toEqual('');
  });

  it('return empty > invalid value', () => {
    const result = useParseInsuranceIndicator('invalid');

    expect(result).toEqual('');
  });

  it('return empty > invalid value', () => {
    const result = useParseInsuranceIndicator('1000');

    expect(result).toEqual('');
  });

  it('return value > INSURANCE_INDICATOR_NOT_CHARGE_CODE', () => {
    const result = useParseInsuranceIndicator(
      `${INSURANCE_INDICATOR_NOT_CHARGE_CODE}`
    );

    expect(result).toEqual(`0 - ${INSURANCE_INDICATOR_NOT_CHARGE_DESCRIPTION}`);
  });

  it('return value > INSURANCE_INDICATOR_CHARGE_CODE_MIN', () => {
    const result = useParseInsuranceIndicator(
      `${INSURANCE_INDICATOR_CHARGE_CODE_MIN}`
    );

    expect(result).toEqual(`1 - ${INSURANCE_INDICATOR_CHARGE_DESCRIPTION}`);
  });
});
