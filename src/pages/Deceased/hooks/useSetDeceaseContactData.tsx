import { useEffect } from 'react';
import { ExtraFieldProps } from 'app/_libraries/_dof/core';
import { Field } from 'redux-form';

export interface DeceasedContactType {
  code: string;
  description: string;
  active: boolean;
}

const useSetDeceaseContactData = ({
  contactViewRef,
  deceasedContact,
  selectedValue,
  deceasedParty
}: {
  contactViewRef: any;
  deceasedContact: DeceasedContactType[];
  selectedValue: RefDataValue;
  deceasedParty: MagicKeyValue;
}) => {
  useEffect(() => {
    if (!contactViewRef?.current || !deceasedContact) return;
    const setImmediateInstance = setImmediate(() => {
      const filerName: Field<ExtraFieldProps> =
        contactViewRef.current?.props?.onFind('deceasedContact');

      const convertData = deceasedContact
        .map(item => ({
          value: item.code,
          description: item.description,
          active: item.active
        }))
        .filter(item => item.active);

      filerName?.props?.props?.setData(convertData);

      let customSelectedValue =
        convertData?.filter(item => item.value === selectedValue?.value)[0] ||
        '';

      if (customSelectedValue) {
        customSelectedValue = {
          ...customSelectedValue,
          description: `${customSelectedValue.value} - ${customSelectedValue.description}`
        };
      }
      filerName?.props?.props?.setValue(customSelectedValue);
    });
    return () => clearImmediate(setImmediateInstance);
  }, [contactViewRef, deceasedContact, deceasedParty?.value, selectedValue]);
};

export default useSetDeceaseContactData;
