import { useMemo } from 'react';

export interface DeceasedContactType {
  code: string;
  description: string;
  active: boolean;
}

const useSetDeceaseInfoData = ({
  selectedValue,
  deceasedContact
}: {
  selectedValue?: string;
  deceasedContact: DeceasedContactType[];
}) => {
  return useMemo(() => {
    if (!selectedValue) return null;

    const convertData =
      deceasedContact
        .map(item => ({
          value: item.code,
          description: item.description
        }))
        .filter(item => item.value === selectedValue)[0] || null;

    return convertData
      ? `${convertData?.value} - ${convertData?.description}`
      : null;
  }, [selectedValue, deceasedContact]);
};

export default useSetDeceaseInfoData;
