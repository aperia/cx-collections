import { useTranslation } from 'app/_libraries/_dls/hooks';
import {
  INSURANCE_INDICATOR_CHARGE_CODE_MAX,
  INSURANCE_INDICATOR_CHARGE_CODE_MIN,
  INSURANCE_INDICATOR_CHARGE_DESCRIPTION,
  INSURANCE_INDICATOR_NOT_CHARGE_CODE,
  INSURANCE_INDICATOR_NOT_CHARGE_DESCRIPTION
} from '../constants';

const useParseInsuranceIndicator = (insuranceIndicator: string) => {
  const { t } = useTranslation();
  if (!insuranceIndicator) return '';
  const codeValue = parseInt(insuranceIndicator);
  if (isNaN(codeValue)) return '';
  if (codeValue === INSURANCE_INDICATOR_NOT_CHARGE_CODE) {
    return `${codeValue} - ${t(INSURANCE_INDICATOR_NOT_CHARGE_DESCRIPTION)}`;
  }
  if (
    INSURANCE_INDICATOR_CHARGE_CODE_MIN <= codeValue &&
    codeValue <= INSURANCE_INDICATOR_CHARGE_CODE_MAX
  ) {
    return `${codeValue} - ${t(INSURANCE_INDICATOR_CHARGE_DESCRIPTION)}`;
  }
  return '';
};

export default useParseInsuranceIndicator;
