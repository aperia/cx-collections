import React from 'react';
import { render, screen } from '@testing-library/react';

import useSetDeceaseInfoData, {
  DeceasedContactType
} from './useSetDeceaseInfoData';

const MockComponent: React.FC<{
  selectedValue?: string;
  deceasedContact?: DeceasedContactType[];
}> = props => {
  const result = useSetDeceaseInfoData(props as Required<typeof props>);
  return <div>{JSON.stringify(result)}</div>;
};

describe('Test useSetDeceaseInfoData hook', () => {
  it('should return null', () => {
    render(<MockComponent />);
    expect(screen.queryByText('"1 - undefined"')).not.toBeInTheDocument();
  });

  it('convertData null', () => {
    render(<MockComponent deceasedContact={[{ code: 1 }]} selectedValue={2} />);
    expect(screen.queryByText('"1 - undefined"')).not.toBeInTheDocument();
  });

  it('should return convertData', () => {
    render(<MockComponent deceasedContact={[{ code: 1 }]} selectedValue={1} />);
    expect(screen.queryByText('"1 - undefined"')).toBeInTheDocument();
  });
});
