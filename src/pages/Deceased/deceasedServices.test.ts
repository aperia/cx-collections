// services
import { deceasedService } from './deceasedServices';

// utils
import { mockApiServices, storeId } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import { IRequestRemoveDeceased } from './types';

describe('deceasedService', () => {
  describe('deceasedInformation', () => {
    const params = {
      commonData: { app: 'app', org: 'org', accountId: storeId },
      data: { callingApplication: '' }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      deceasedService.deceasedInformation(params.commonData, params.data);

      expect(mockService).toBeCalledWith('', {
        common: params.commonData,
        ...params.data
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      deceasedService.deceasedInformation(params.commonData, params.data);

      expect(mockService).toBeCalledWith(apiUrl.deceased.deceasedInformation, {
        common: params.commonData,
        ...params.data
      });
    });
  });

  describe('deceasedCreateWorkflow', () => {
    const params = {
      commonData: { app: 'app', org: 'org', accountId: storeId },
      data: { callingApplication: '' }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      deceasedService.deceasedCreateWorkflow(params.commonData, params.data);

      expect(mockService).toBeCalledWith('', {
        common: params.commonData,
        ...params.data
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      deceasedService.deceasedCreateWorkflow(params.commonData, params.data);

      expect(mockService).toBeCalledWith(
        apiUrl.deceased.deceasedCreateWorkflow,
        { common: params.commonData, ...params.data }
      );
    });
  });

  describe('removeStatusCallResult', () => {
    const params: IRequestRemoveDeceased = {
      common: { accountId: storeId },
      callResultType: 'string'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      deceasedService.removeStatusCallResult(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      deceasedService.removeStatusCallResult(params);

      expect(mockService).toBeCalledWith(
        apiUrl.collectionForm.removeStatusCallResult,
        params
      );
    });
  });
});
