import { apiService } from 'app/utils/api.service';
import {
  DeceasedCreateWorkflowRequestData,
  DeceasedCommonRequestData,
  DeceasedInformationRequestData,
  IRequestRemoveDeceased,
  DeceasedInformationResponse
} from './types';

export const deceasedService = {
  deceasedInformation(
    commonData: DeceasedCommonRequestData,
    data: DeceasedInformationRequestData
  ) {
    const url = window.appConfig?.api?.deceased?.deceasedInformation || '';
    const requestBody = {
      common: {
        ...commonData
      },
      ...data
    };
    return apiService.post<DeceasedInformationResponse>(url, requestBody);
  },
  deceasedCreateWorkflow(
    commonData: DeceasedCommonRequestData,
    data: DeceasedCreateWorkflowRequestData
  ) {
    const url = window.appConfig?.api?.deceased?.deceasedCreateWorkflow || '';
    const requestBody = {
      common: {
        ...commonData
      },
      ...data
    };
    return apiService.post(url, requestBody);
  },
  removeStatusCallResult(data: IRequestRemoveDeceased) {
    const url =
      window.appConfig?.api?.collectionForm?.removeStatusCallResult || '';
    return apiService.post(url, data);
  }
};
