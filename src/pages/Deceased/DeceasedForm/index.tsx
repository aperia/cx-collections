import React, { useCallback, useEffect, useMemo } from 'react';

// components
import DeceasedPartyMainInfo from './DeceasedPartyForms';
import DeceasedPartyDropdownList from '../DeceasedPartyDropdownList';

// hooks
import {
  useAccountDetail,
  useIsDirtyForm,
  useStoreIdSelector
} from 'app/hooks';
import { useSaveViewName } from 'pages/CollectionForm/hooks/useSaveViewName';

// redux
import { useDispatch } from 'react-redux';
import { deceasedActions } from '../_redux/reducers';
import { selectMemoSectionCollapsed } from 'pages/CollectionForm/_redux/selectors';
import {
  selectDeceasedParty,
  selectOriginDeceasedParty
} from '../_redux/selectors';

// utils
import classNames from 'classnames';
import {
  DECEASED_ENTRY_CODE,
  DECEASED_PARTY_VALUE,
  PRIMARY_DECEASED_PARTY_INFORMATION_VIEW,
  PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW,
  SECONDARY_DECEASED_PARTY_INFORMATION_VIEW,
  SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW,
  SETTLEMENT_INFORMATION_VIEW
} from '../constants';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { useSubmit } from 'pages/CollectionForm/hooks/useSubmit';

const DeceasedForm: React.FC = () => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();

  const isMemoSectionCollapsed = useStoreIdSelector<MagicKeyValue>(
    selectMemoSectionCollapsed
  );

  const deceasedParty = useStoreIdSelector<RefDataValue>(selectDeceasedParty);

  const originDeceasedParty = useStoreIdSelector<string>(
    selectOriginDeceasedParty
  );

  const viewList = useMemo(() => {
    if (deceasedParty.value === DECEASED_PARTY_VALUE.PRIMARY) {
      return [
        `${storeId}_${PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`,
        `${storeId}_${PRIMARY_DECEASED_PARTY_INFORMATION_VIEW}`,
        `${storeId}_${SETTLEMENT_INFORMATION_VIEW}`
      ];
    } else if (deceasedParty.value === DECEASED_PARTY_VALUE.SECONDARY) {
      return [
        `${storeId}_${SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`,
        `${storeId}_${SECONDARY_DECEASED_PARTY_INFORMATION_VIEW}`,
        `${storeId}_${SETTLEMENT_INFORMATION_VIEW}`
      ];
    } else if (deceasedParty.value === DECEASED_PARTY_VALUE.BOTH) {
      return [
        `${storeId}_${PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`,
        `${storeId}_${PRIMARY_DECEASED_PARTY_INFORMATION_VIEW}`,
        `${storeId}_${SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`,
        `${storeId}_${SECONDARY_DECEASED_PARTY_INFORMATION_VIEW}`,
        `${storeId}_${SETTLEMENT_INFORMATION_VIEW}`
      ];
    } else {
      return [];
    }
  }, [deceasedParty, storeId]);

  const isDirty = useIsDirtyForm(viewList);

  const handleSubmit = useCallback(() => {
    dispatch(
      deceasedActions.triggerDeceasedConfirmed({
        storeId,
        entryCode: DECEASED_ENTRY_CODE.DECEASED_CONFIRMED
      })
    );
  }, [dispatch, storeId]);

  useSaveViewName(viewList);
  useSubmit(handleSubmit);

  useEffect(() => {
    dispatch(deceasedActions.deceasedInformationRequest({ storeId }));
  }, [storeId, dispatch]);

  useEffect(() => {
    return () => {
      dispatch(deceasedActions.resetDeceasedParty({ storeId }));
    };
  }, [dispatch, storeId]);

  // set dirty
  useEffect(() => {
    dispatch(
      collectionFormActions.setDirtyForm({
        storeId,
        callResult: CALL_RESULT_CODE.DECEASE,
        value: isDirty
      })
    );
    dispatch(
      collectionFormActions.changeDisabledOk({
        storeId,
        disabledOk:
          !isDirty &&
          originDeceasedParty?.toLowerCase() ===
            deceasedParty?.value?.toLowerCase()
      })
    );
  }, [deceasedParty?.value, dispatch, isDirty, originDeceasedParty, storeId]);

  return (
    <div
      className={classNames('px-24 pb-24 mx-auto max-width-lg', {
        'memos-extend': !isMemoSectionCollapsed
      })}
    >
      <DeceasedPartyDropdownList />
      <DeceasedPartyMainInfo />
    </div>
  );
};

export default DeceasedForm;
