import React from 'react';

// RTL
import { screen } from '@testing-library/react';

// helpers
import { renderMockStoreId, storeId } from 'app/test-utils';

// component
import DeceasedForm from './index';
import { DECEASED_PARTIES } from '../constants';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('pages/CollectionForm/hooks/useSubmit', () => ({
  useSubmit: (callback: Function) => {
    callback();
  }
}));

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text || '' });
});


describe('Test DeceasedForm component', () => {
  it('render component', () => {
    renderMockStoreId(<DeceasedForm />);

    expect(screen.getByText('txt_deceased_party')).toBeInTheDocument();
  });

  it('Secondary', () => {
    renderMockStoreId(<DeceasedForm />, {
      initialState: {
        deceased: {
          [storeId]: {
            isJointAcc: true,
            deceasedParty: DECEASED_PARTIES[1]
          }
        }
      }
    });

    expect(screen.getByText('txt_secondary')).toBeInTheDocument();
  });

  it('Both', () => {
    renderMockStoreId(<DeceasedForm />, {
      initialState: {
        deceased: {
          [storeId]: {
            isJointAcc: true,
            deceasedParty: DECEASED_PARTIES[2]
          }
        }
      }
    });

    expect(screen.getByText('txt_both')).toBeInTheDocument();
  });

  it('Both', () => {
    renderMockStoreId(<DeceasedForm />, {
      initialState: {
        deceased: {
          [storeId]: {
            isJointAcc: true,
            deceasedParty: {
              value: 'other'
            }
          }
        }
      }
    });

    expect(screen.getByText('txt_deceased_party')).toBeInTheDocument();
  });
});
