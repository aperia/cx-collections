import React from 'react';

// RTL
import { screen } from '@testing-library/react';

// components
import DeceasedPartyMainInfo from './DeceasedPartyForms';

// helpers
import { renderMockStoreId, storeId } from 'app/test-utils';
import {
  DECEASED_PARTIES,
  PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW,
  SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW
} from '../constants';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

describe('Test DeceasedPartyForms component', () => {
  it('render component', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(<DeceasedPartyMainInfo />, {
      initialState: {
        deceased: {
          [storeId]: {
            isJointAcc: true,
            deceasedParty: {
              value: 'other'
            }
          }
        }
      }
    });
    jest.runAllTimers();

    expect(wrapper.container.innerHTML).toEqual('');
  });

  describe('PRIMARY', () => {
    it('render component', () => {
      jest.useFakeTimers();
      renderMockStoreId(<DeceasedPartyMainInfo />, {
        initialState: {
          deceased: {
            [storeId]: {
              isJointAcc: true,
              deceasedParty: DECEASED_PARTIES[0],
              deceasedInformation: {
                deceasedPrimaryInformation: {
                  deceasedDatePrimary: '07-01-2021',
                  deceasedDateNoticeReceivedPrimary: '07-02-2021',
                  deceasedContactStatePrimary: 'TX',
                  deceasedStatePrimary: 'TX'
                }
              },
              deceasedSettlementInformation: {
                settlementAmount: '100'
              },
              deathCertificates: {
                primary: [{ name: 'File A', url: 'url' }]
              }
            }
          }
        }
      });
      jest.runAllTimers();

      expect(
        screen.getByTestId('primaryDeceasedPartyMainInformationView')
      ).toBeInTheDocument();
      expect(
        screen.getByTestId('deceasedPartyInformationView')
      ).toBeInTheDocument();
      expect(
        screen.getByTestId('settlementInformationView')
      ).toBeInTheDocument();
    });

    it('handlePrimaryDateOfDeathChange > dateOfDeath having value', () => {
      jest.useFakeTimers();
      renderMockStoreId(<DeceasedPartyMainInfo />, {
        initialState: {
          deceased: {
            [storeId]: {
              isJointAcc: true,
              deceasedParty: DECEASED_PARTIES[0],
              deceasedInformation: {},
              deceasedSettlementInformation: {
                settlementAmount: '100'
              },
              deathCertificates: {
                primary: [{ name: 'File A', url: 'url' }]
              }
            }
          },
          form: {
            [`${storeId}_${PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`]: {
              registeredFields: [],
              values: {
                deceasedDateOfDeath: '07-01-2021'
              }
            }
          }
        } as Partial<RootState>
      });
      jest.runAllTimers();

      expect(
        screen.getByTestId('deceasedPartyInformationView')
      ).toBeInTheDocument();
      expect(
        screen.getByTestId('settlementInformationView')
      ).toBeInTheDocument();
    });

    it('handlePrimaryDateOfDeathChange > dateOfDeath null', () => {
      jest.useFakeTimers();
      renderMockStoreId(<DeceasedPartyMainInfo />, {
        initialState: {
          deceased: {
            [storeId]: {
              isJointAcc: true,
              deceasedParty: DECEASED_PARTIES[0],
              deceasedInformation: {},
              deceasedSettlementInformation: {
                settlementAmount: '100'
              },
              deathCertificates: {
                primary: [{ name: 'File A', url: 'url' }]
              }
            }
          },
          form: {
            [`${storeId}_${PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`]: {
              registeredFields: [],
              values: {
                deceasedDateOfDeath: null
              }
            }
          }
        } as Partial<RootState>
      });
      jest.runAllTimers();

      expect(
        screen.getByTestId('deceasedPartyInformationView')
      ).toBeInTheDocument();
      expect(
        screen.getByTestId('settlementInformationView')
      ).toBeInTheDocument();
    });
  });

  describe('SECONDARY', () => {
    it('render component', () => {
      jest.useFakeTimers();
      renderMockStoreId(<DeceasedPartyMainInfo />, {
        initialState: {
          deceased: {
            [storeId]: {
              isJointAcc: true,
              deceasedParty: DECEASED_PARTIES[1],
              deceasedInformation: {
                deceasedSecondaryInformation: {
                  deceasedDateSecondary: '07-01-2021',
                  deceasedDateNoticeReceivedSecondary: '07-02-2021',
                  deceasedContactStateSecondary: 'TX',
                  deceasedStateSecondary: 'TX'
                }
              }
            }
          }
        }
      });
      jest.runAllTimers();

      expect(
        screen.getByTestId('secondaryDeceasedPartyMainInformationView')
      ).toBeInTheDocument();
      expect(
        screen.getByTestId('deceasedPartyInformationView')
      ).toBeInTheDocument();
      expect(
        screen.getByTestId('settlementInformationView')
      ).toBeInTheDocument();
    });

    it('handleSecondaryDateOfDeathChange > dateOfDeath having value', () => {
      jest.useFakeTimers();
      renderMockStoreId(<DeceasedPartyMainInfo />, {
        initialState: {
          deceased: {
            [storeId]: {
              isJointAcc: true,
              deceasedParty: DECEASED_PARTIES[1],
              deceasedInformation: {
                deceasedSecondaryInformation: {
                  deceasedDateSecondary: '07-01-2021',
                  deceasedDateNoticeReceivedSecondary: '07-02-2021',
                  deceasedContactStateSecondary: 'TX',
                  deceasedStateSecondary: 'TX'
                }
              }
            }
          },
          form: {
            [`${storeId}_${SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`]: {
              registeredFields: [],
              values: {
                deceasedDateOfDeath: '07-01-2021'
              }
            }
          }
        } as Partial<RootState>
      });
      jest.runAllTimers();

      expect(
        screen.getByTestId('secondaryDeceasedPartyMainInformationView')
      ).toBeInTheDocument();
    });

    it('handleSecondaryDateOfDeathChange > dateOfDeath null', () => {
      jest.useFakeTimers();
      renderMockStoreId(<DeceasedPartyMainInfo />, {
        initialState: {
          deceased: {
            [storeId]: {
              isJointAcc: true,
              deceasedParty: DECEASED_PARTIES[1],
              deceasedInformation: {
                deceasedSecondaryInformation: {
                  deceasedDateSecondary: '07-01-2021',
                  deceasedDateNoticeReceivedSecondary: '07-02-2021',
                  deceasedContactStateSecondary: 'TX',
                  deceasedStateSecondary: 'TX'
                }
              },
              deathCertificates: {
                secondary: [{ name: 'File A', url: 'url' }]
              }
            }
          },
          form: {
            [`${storeId}_${SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`]: {
              registeredFields: [],
              values: {
                deceasedDateOfDeath: null
              }
            }
          }
        } as Partial<RootState>
      });
      jest.runAllTimers();

      expect(
        screen.getByTestId('secondaryDeceasedPartyMainInformationView')
      ).toBeInTheDocument();
    });
  });

  describe('BOTH', () => {
    it('render component', () => {
      jest.useFakeTimers();
      renderMockStoreId(<DeceasedPartyMainInfo />, {
        initialState: {
          deceased: {
            [storeId]: {
              isJointAcc: true,
              deceasedParty: DECEASED_PARTIES[2],
              deceasedInformation: {
                deceasedPrimaryInformation: {
                  deceasedDatePrimary: '07-01-2021',
                  deceasedDateNoticeReceivedPrimary: '07-02-2021',
                  deceasedContactStatePrimary: 'TX',
                  deceasedStatePrimary: 'TX'
                },
                deceasedSecondaryInformation: {
                  deceasedDateSecondary: '07-01-2021',
                  deceasedDateNoticeReceivedSecondary: '07-02-2021',
                  deceasedContactStateSecondary: 'TX',
                  deceasedStateSecondary: 'TX'
                }
              },
              deathCertificates: {
                secondary: [{ name: 'File A', url: 'url' }]
              }
            }
          }
        }
      });
      jest.runAllTimers();

      expect(
        screen.getByTestId('primaryDeceasedPartyMainInformationView')
      ).toBeInTheDocument();
      expect(
        screen.getAllByTestId('deceasedPartyInformationView').length
      ).toEqual(2);
      expect(
        screen.getByTestId('secondaryDeceasedPartyMainInformationView')
      ).toBeInTheDocument();
      expect(
        screen.getByTestId('settlementInformationView')
      ).toBeInTheDocument();
    });

    it('render component', () => {
      jest.useFakeTimers();
      renderMockStoreId(<DeceasedPartyMainInfo />, {
        initialState: {
          deceased: {
            [storeId]: {
              isJointAcc: true,
              deceasedParty: DECEASED_PARTIES[2],
              deceasedInformation: {
                deceasedPrimaryInformation: {
                  deceasedDatePrimary: '07-01-2021',
                  deceasedDateNoticeReceivedPrimary: '07-02-2021',
                  deceasedContactStatePrimary: 'TX',
                  deceasedStatePrimary: 'TX'
                },
                deceasedSecondaryInformation: {
                  deceasedDateSecondary: '07-01-2021',
                  deceasedDateNoticeReceivedSecondary: '07-02-2021',
                  deceasedContactStateSecondary: 'TX',
                  deceasedStateSecondary: 'TX'
                }
              },
              deathCertificates: {
                secondary: [{ name: 'File A', url: 'url' }]
              }
            }
          },
          form: {
            [`${storeId}_${PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`]: {
              registeredFields: [],
              values: {
                deceasedDateOfDeath: null
              }
            },
            [`${storeId}_${SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`]: {
              registeredFields: [],
              values: {
                deceasedDateOfDeath: null
              }
            }
          }
        }
      });
      jest.runAllTimers();

      expect(
        screen.getByTestId('primaryDeceasedPartyMainInformationView')
      ).toBeInTheDocument();
      expect(
        screen.getAllByTestId('deceasedPartyInformationView').length
      ).toEqual(2);
      expect(
        screen.getByTestId('secondaryDeceasedPartyMainInformationView')
      ).toBeInTheDocument();
      expect(
        screen.getByTestId('settlementInformationView')
      ).toBeInTheDocument();
    });

    it('render component', () => {
      jest.useFakeTimers();
      renderMockStoreId(<DeceasedPartyMainInfo />, {
        initialState: {
          deceased: {
            [storeId]: {
              isJointAcc: true,
              deceasedParty: DECEASED_PARTIES[2],
              deceasedInformation: {
                deceasedPrimaryInformation: {
                  deceasedDatePrimary: '07-01-2021',
                  deceasedDateNoticeReceivedPrimary: '07-02-2021',
                  deceasedContactStatePrimary: 'TX',
                  deceasedStatePrimary: 'TX'
                },
                deceasedSecondaryInformation: {
                  deceasedDateSecondary: '07-01-2021',
                  deceasedDateNoticeReceivedSecondary: '07-02-2021',
                  deceasedContactStateSecondary: 'TX',
                  deceasedStateSecondary: 'TX'
                }
              },
              deathCertificates: {
                secondary: [{ name: 'File A', url: 'url' }]
              }
            }
          },
          form: {
            [`${storeId}_${PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`]: {
              registeredFields: [],
              values: {
                deceasedDateOfDeath: '07-01-2021'
              }
            },
            [`${storeId}_${SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`]: {
              registeredFields: [],
              values: {
                deceasedDateOfDeath: '07-01-2021'
              }
            }
          }
        }
      });
      jest.runAllTimers();

      expect(
        screen.getByTestId('primaryDeceasedPartyMainInformationView')
      ).toBeInTheDocument();
      expect(
        screen.getAllByTestId('deceasedPartyInformationView').length
      ).toEqual(2);
      expect(
        screen.getByTestId('secondaryDeceasedPartyMainInformationView')
      ).toBeInTheDocument();
      expect(
        screen.getByTestId('settlementInformationView')
      ).toBeInTheDocument();
    });
  });
});
