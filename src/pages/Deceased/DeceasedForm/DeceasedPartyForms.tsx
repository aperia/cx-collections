import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
// components
import { View } from 'app/_libraries/_dof/core';
import { getConfigReferenceData } from 'pages/CollectionForm/CollectionActivities/GeneralInformation/_redux/selectors';
import React, { useCallback, useEffect, useMemo, useRef } from 'react';
import { useSelector } from 'react-redux';
import { getFormValues } from 'redux-form';
// constants
import {
  DECEASED_PARTY,
  DECEASED_PARTY_VALUE,
  PRIMARY_DECEASED_PARTY_INFORMATION_VIEW,
  PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW,
  SECONDARY_DECEASED_PARTY_INFORMATION_VIEW,
  SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW,
  SETTLEMENT_INFORMATION_VIEW
} from '../constants';
import useParseInsuranceIndicator from '../hooks/useParseInsuranceIndicator';
import useSetDeceaseContactData from '../hooks/useSetDeceaseContactData';
import {
  DeceasedPrimaryMainInfoForm,
  DeceasedSecondaryMainInfoForm,
  DeceasedSettlementInformation,
  IDeceasedPartyInfo,
  IValueDeceased,
  ReferenceData
} from '../types';
// redux
import {
  selectCreditLifeCode,
  selectDeceasedParty,
  selectDeceasedSettlement,
  selectValueDeceasedMainPrimary,
  selectValueDeceasedMainSecond,
  selectValueDeceasedPrimary,
  selectValueDeceasedSecond
} from '../_redux/selectors';

interface DeceasedPartyMainInfoProp {}

const DeceasedPartyMainInfo: React.FC<DeceasedPartyMainInfoProp> = ({}) => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();

  const deceasedParty = useStoreIdSelector<ReferenceData>(selectDeceasedParty);

  const creditLifeCode = useStoreIdSelector<string>(selectCreditLifeCode);

  const { deceasedContact } = useStoreIdSelector<any>(getConfigReferenceData);

  const deceasedMainPrimaryValue = useStoreIdSelector<IValueDeceased>(
    selectValueDeceasedMainPrimary
  );

  const deceasedMainSecondaryValue = useStoreIdSelector<IValueDeceased>(
    selectValueDeceasedMainSecond
  );

  const deceasedSettlementValue =
    useStoreIdSelector<DeceasedSettlementInformation>(selectDeceasedSettlement);

  const valueDeceasedMainPrimary = useStoreIdSelector<IDeceasedPartyInfo>(
    selectValueDeceasedPrimary
  );

  const valueDeceasedMainSecond = useStoreIdSelector<IDeceasedPartyInfo>(
    selectValueDeceasedSecond
  );

  const primaryForm: DeceasedPrimaryMainInfoForm = useSelector(
    getFormValues(`${storeId}_${PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`)
  );

  const secondaryForm: DeceasedSecondaryMainInfoForm = useSelector(
    getFormValues(
      `${storeId}_${SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`
    )
  );

  const primaryViewRef = useRef<any>(null);
  const secondaryViewRef = useRef<any>(null);
  const contactPrimaryViewRef = useRef<any>(null);
  const contactSecondaryViewRef = useRef<any>(null);
  const settlementViewRef = useRef<any>(null);

  const headingPrimaryView = t(
    I18N_COLLECTION_FORM.PRIMARY_DECEASED_PARTY_MAIN
  );
  const headingSecondView = t(I18N_COLLECTION_FORM.SECOND_DECEASED_PARTY_MAIN);
  const settlementView = t(I18N_COLLECTION_FORM.SETTLEMENT_DECEASED_HEADER);

  const insuranceIndicator = useParseInsuranceIndicator(creditLifeCode);

  const renderPrimaryView = useMemo(() => {
    return (
      <>
        <View
          id={`${storeId}_${PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`}
          formKey={`${storeId}_${PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`}
          ref={primaryViewRef}
          descriptor="primaryDeceasedPartyMainInformationView"
          value={{
            deceasedPartyMainHeadingControl: headingPrimaryView,
            ...deceasedMainPrimaryValue
          }}
        />
        <View
          id={`${storeId}_${PRIMARY_DECEASED_PARTY_INFORMATION_VIEW}`}
          formKey={`${storeId}_${PRIMARY_DECEASED_PARTY_INFORMATION_VIEW}`}
          descriptor="deceasedPartyInformationView"
          ref={contactPrimaryViewRef}
          value={{
            ...valueDeceasedMainPrimary,
            deceasedInsuranceIndicator: insuranceIndicator
          }}
        />

        <View
          id={`${storeId}_${SETTLEMENT_INFORMATION_VIEW}`}
          formKey={`${storeId}_${SETTLEMENT_INFORMATION_VIEW}`}
          ref={settlementViewRef}
          descriptor="settlementInformationView"
          value={{
            deceasedSettlementHeadingControl: settlementView,
            ...deceasedSettlementValue
          }}
        />
      </>
    );
  }, [
    storeId,
    headingPrimaryView,
    deceasedMainPrimaryValue,
    valueDeceasedMainPrimary,
    insuranceIndicator,
    settlementView,
    deceasedSettlementValue
  ]);

  const renderSecondView = useMemo(() => {
    return (
      <>
        <View
          id={`${storeId}_${SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`}
          formKey={`${storeId}_${SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`}
          descriptor="secondaryDeceasedPartyMainInformationView"
          ref={secondaryViewRef}
          value={{
            deceasedPartyMainHeadingControl: headingSecondView,
            ...deceasedMainSecondaryValue
          }}
        />
        <View
          id={`${storeId}_${SECONDARY_DECEASED_PARTY_INFORMATION_VIEW}`}
          formKey={`${storeId}_${SECONDARY_DECEASED_PARTY_INFORMATION_VIEW}`}
          descriptor="deceasedPartyInformationView"
          ref={contactSecondaryViewRef}
          value={{
            ...valueDeceasedMainSecond,
            deceasedInsuranceIndicator: insuranceIndicator
          }}
        />

        <View
          id={`${storeId}_${SETTLEMENT_INFORMATION_VIEW}`}
          formKey={`${storeId}_${SETTLEMENT_INFORMATION_VIEW}`}
          ref={settlementViewRef}
          descriptor="settlementInformationView"
          value={{
            deceasedSettlementHeadingControl: settlementView,
            ...deceasedSettlementValue
          }}
        />
      </>
    );
  }, [
    storeId,
    headingSecondView,
    deceasedMainSecondaryValue,
    valueDeceasedMainSecond,
    insuranceIndicator,
    settlementView,
    deceasedSettlementValue
  ]);

  const renderBothView = useMemo(() => {
    return (
      <>
        <View
          id={`${storeId}_${PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`}
          formKey={`${storeId}_${PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`}
          ref={primaryViewRef}
          descriptor="primaryDeceasedPartyMainInformationView"
          value={{
            deceasedPartyMainHeadingControl: headingPrimaryView,
            ...deceasedMainPrimaryValue
          }}
        />
        <View
          id={`${storeId}_${PRIMARY_DECEASED_PARTY_INFORMATION_VIEW}`}
          formKey={`${storeId}_${PRIMARY_DECEASED_PARTY_INFORMATION_VIEW}`}
          descriptor="deceasedPartyInformationView"
          ref={contactPrimaryViewRef}
          value={{
            ...valueDeceasedMainPrimary,
            deceasedInsuranceIndicator: insuranceIndicator
          }}
        />

        <View
          id={`${storeId}_${SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`}
          formKey={`${storeId}_${SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`}
          descriptor="secondaryDeceasedPartyMainInformationView"
          ref={secondaryViewRef}
          value={{
            deceasedPartyMainHeadingControl: headingSecondView,
            ...deceasedMainSecondaryValue
          }}
        />
        <View
          id={`${storeId}_${SECONDARY_DECEASED_PARTY_INFORMATION_VIEW}`}
          formKey={`${storeId}_${SECONDARY_DECEASED_PARTY_INFORMATION_VIEW}`}
          descriptor="deceasedPartyInformationView"
          ref={contactSecondaryViewRef}
          value={{
            ...valueDeceasedMainSecond,
            deceasedInsuranceIndicator: insuranceIndicator
          }}
        />
        <View
          id={`${storeId}_${SETTLEMENT_INFORMATION_VIEW}`}
          formKey={`${storeId}_${SETTLEMENT_INFORMATION_VIEW}`}
          ref={settlementViewRef}
          descriptor="settlementInformationView"
          value={{
            deceasedSettlementHeadingControl: settlementView,
            ...deceasedSettlementValue
          }}
        />
      </>
    );
  }, [
    storeId,
    headingPrimaryView,
    deceasedMainPrimaryValue,
    valueDeceasedMainPrimary,
    insuranceIndicator,
    headingSecondView,
    deceasedMainSecondaryValue,
    valueDeceasedMainSecond,
    settlementView,
    deceasedSettlementValue
  ]);

  const renderLayout = useMemo(() => {
    if (deceasedParty.value === DECEASED_PARTY_VALUE.PRIMARY)
      return renderPrimaryView;
    if (deceasedParty.value === DECEASED_PARTY_VALUE.SECONDARY)
      return renderSecondView;
    if (deceasedParty.value === DECEASED_PARTY_VALUE.BOTH)
      return renderBothView;
    return null;
  }, [renderPrimaryView, deceasedParty, renderSecondView, renderBothView]);

  const handlePrimaryDateOfDeathChange = useCallback(() => {
    const settlementDateField =
      settlementViewRef?.current?.props?.onFind('settlementDate');

    const primaryDateNoticeField = primaryViewRef?.current?.props?.onFind(
      'deceasedDateNoticeReceived'
    );

    const dateOfDeath = primaryForm?.deceasedDateOfDeath;

    if (dateOfDeath) {
      settlementDateField?.props?.props?.setEnable(true);
      primaryDateNoticeField?.props?.props?.setEnable(true);
      primaryDateNoticeField?.props?.props?.setOthers(
        (previousValue: MagicKeyValue) => ({
          ...previousValue,
          options: {
            ...previousValue?.options,
            min: dateOfDeath
          }
        })
      );
    } else {
      settlementDateField?.props?.props?.setEnable(false);
      primaryDateNoticeField?.props?.props?.setEnable(false);
    }
    if (dateOfDeath === null) {
      primaryDateNoticeField?.props?.props?.setValue(null);
      settlementDateField?.props?.props?.setValue(null);
    }
  }, [primaryForm?.deceasedDateOfDeath]);

  const handleSecondaryDateOfDeathChange = useCallback(() => {
    const settlementDateField =
      settlementViewRef?.current?.props?.onFind('settlementDate');

    const dateNoticeField = secondaryViewRef?.current?.props?.onFind(
      'deceasedDateNoticeReceived'
    );

    const dateOfDeath = secondaryForm?.deceasedDateOfDeath;

    if (dateOfDeath) {
      settlementDateField?.props?.props?.setEnable(true);
      dateNoticeField?.props?.props?.setEnable(true);
      dateNoticeField?.props?.props?.setOthers(
        (previousValue: MagicKeyValue) => ({
          ...previousValue,
          options: {
            ...previousValue?.options,
            min: dateOfDeath
          }
        })
      );
    } else {
      settlementDateField?.props?.props?.setEnable(false);
      dateNoticeField?.props?.props?.setEnable(false);
    }
    if (dateOfDeath === null) {
      dateNoticeField?.props?.props?.setValue(null);
      settlementDateField?.props?.props?.setValue(null);
    }
  }, [secondaryForm?.deceasedDateOfDeath]);

  const handleBoth = useCallback(() => {
    const settlementDateField =
      settlementViewRef?.current?.props?.onFind('settlementDate');
    const primaryDateNoticeField = primaryViewRef?.current?.props?.onFind(
      'deceasedDateNoticeReceived'
    );
    const secondaryDateNoticeField = secondaryViewRef?.current?.props?.onFind(
      'deceasedDateNoticeReceived'
    );

    const primaryDateOfDeathValue = primaryForm?.deceasedDateOfDeath;
    const secondaryDateOfDeathValue = secondaryForm?.deceasedDateOfDeath;

    // Handle enable
    primaryDateNoticeField?.props?.props?.setEnable(!!primaryDateOfDeathValue);
    secondaryDateNoticeField?.props?.props?.setEnable(
      !!secondaryDateOfDeathValue
    );
    settlementDateField?.props?.props?.setEnable(
      !!primaryDateOfDeathValue && !!secondaryDateOfDeathValue
    );
    // Handle range date
    primaryDateOfDeathValue &&
      primaryDateNoticeField?.props?.props?.setOthers(
        (previousValue: MagicKeyValue) => ({
          ...previousValue,
          options: {
            ...previousValue?.options,
            min: primaryDateOfDeathValue
          }
        })
      );
    secondaryDateOfDeathValue &&
      secondaryDateNoticeField?.props?.props?.setOthers(
        (previousValue: MagicKeyValue) => ({
          ...previousValue,
          options: {
            ...previousValue?.options,
            min: secondaryDateOfDeathValue
          }
        })
      );
    // Handle date value
    if (primaryDateOfDeathValue === null) {
      primaryDateNoticeField?.props?.props?.setValue(null);
      settlementDateField?.props?.props?.setValue(null);
    }

    if (secondaryDateOfDeathValue === null) {
      secondaryDateNoticeField?.props?.props?.setValue(null);
      settlementDateField?.props?.props?.setValue(null);
    }
  }, [primaryForm?.deceasedDateOfDeath, secondaryForm?.deceasedDateOfDeath]);

  useSetDeceaseContactData({
    contactViewRef: contactPrimaryViewRef,
    deceasedContact,
    deceasedParty,
    selectedValue: valueDeceasedMainPrimary.deceasedContact
  });

  useSetDeceaseContactData({
    contactViewRef: contactSecondaryViewRef,
    deceasedContact,
    deceasedParty,
    selectedValue: valueDeceasedMainSecond.deceasedContact
  });

  useEffect(() => {
    setImmediate(() => {
      if (deceasedParty?.value === DECEASED_PARTY.PRIMARY) {
        handlePrimaryDateOfDeathChange();
      }
      if (deceasedParty?.value === DECEASED_PARTY.SECONDARY) {
        handleSecondaryDateOfDeathChange();
      }
      if (deceasedParty?.value === DECEASED_PARTY.BOTH) {
        handleBoth();
      }
    });
  }, [
    deceasedParty?.value,
    handlePrimaryDateOfDeathChange,
    handleSecondaryDateOfDeathChange,
    handleBoth
  ]);

  return <>{renderLayout}</>;
};

export default DeceasedPartyMainInfo;
