import { isFulfilled, isRejected } from '@reduxjs/toolkit';
import { accEValue, responseDefault, storeId } from 'app/test-utils';
import { AxiosResponse } from 'axios';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { deceasedService } from '../deceasedServices';
import { deceasedCreateWorkflowRequest } from './deceasedCreateWorkflow';

export const successResponseDefault: AxiosResponse<any> = {
  data: { messages: [{ status: 'success' }] },
  status: 200,
  statusText: '',
  headers: {},
  config: {}
};

let spy: jest.SpyInstance;

jest.mock('pages/__commons/ToastNotifications/_redux', () => ({
  ...(jest.requireActual(
    'pages/__commons/ToastNotifications/_redux'
  ) as object),
  actionsToast: { addToast: jest.fn() }
}));

describe('deceasedCreateWorkflow', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  describe('deceasedCreateWorkflowRequest async thunk', () => {
    it('should return payload when request success', async () => {
      spy = jest
        .spyOn(deceasedService, 'deceasedCreateWorkflow')
        .mockResolvedValue({
          ...successResponseDefault
        });
      const store = createStore(rootReducer, {});
      const response = await deceasedCreateWorkflowRequest({
        storeId,
        commonData: {
          app: 'cx',
          org: 'COLX',
          accountId: accEValue
        },
        data: {}
      })(store.dispatch, store.getState, {});
      expect(response.payload).toEqual(successResponseDefault.data);
      expect(isFulfilled(response)).toBeTruthy;
    });

    it('should return reject when response has no status success ', async () => {
      spy = jest
        .spyOn(deceasedService, 'deceasedCreateWorkflow')
        .mockResolvedValue({
          ...responseDefault
        });
      const store = createStore(rootReducer, {});
      const response = await deceasedCreateWorkflowRequest({
        storeId,
        commonData: {
          app: 'cx',
          org: 'COLX',
          accountId: accEValue
        },
        data: {}
      })(store.dispatch, store.getState, {});
      expect(response.payload).toEqual({ response: responseDefault });
      expect(isRejected(response)).toBeTruthy;
    });

    it('should return reject when call API is failed ', async () => {
      spy = jest
        .spyOn(deceasedService, 'deceasedCreateWorkflow')
        .mockRejectedValue({});
      const store = createStore(rootReducer, {});
      const response = await deceasedCreateWorkflowRequest({
        storeId,
        commonData: {
          app: 'cx',
          org: 'COLX',
          accountId: accEValue
        },
        data: {}
      })(store.dispatch, store.getState, {});
      expect(isRejected(response)).toBeTruthy;
    });
  });
});
