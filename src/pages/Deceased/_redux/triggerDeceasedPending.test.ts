import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import {
  accEValue,
  createStoreWithDefaultMiddleWare,
  mockActionCreator,
  storeId
} from 'app/test-utils';
import { AxiosResponse } from 'axios';
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { deceasedService } from '../deceasedServices';
import { DECEASED_PARTIES } from '../constants';
import { triggerDeceasedPending } from './triggerDeceasedPending';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';

export const successResponseDefault: AxiosResponse<any> = {
  data: { messages: [{ status: 'success' }] },
  status: 200,
  statusText: '',
  headers: {},
  config: {}
};

const toastSpy = mockActionCreator(actionsToast);
const collectionFormSpy = mockActionCreator(collectionFormActions);

let spy: jest.SpyInstance;

describe('triggerDeceasedPending async thunk when success', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isCollector: true
    }
  };
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  const runWhenAPISuccess = async (initialState: MagicKeyValue) => {
    spy = jest
      .spyOn(deceasedService, 'deceasedCreateWorkflow')
      .mockResolvedValue({ ...successResponseDefault });
    const store = createStoreWithDefaultMiddleWare(initialState);
    return triggerDeceasedPending({
      storeId,
      accEValue,
      entryCode: 'DP'
    })(store.dispatch, store.getState, {});
  };

  const runWhenAPIFailed = async (initialState: MagicKeyValue) => {
    spy = jest
      .spyOn(deceasedService, 'deceasedCreateWorkflow')
      .mockRejectedValue({});
    const store = createStoreWithDefaultMiddleWare(initialState);
    return triggerDeceasedPending({
      storeId,
      accEValue,
      entryCode: 'DP'
    })(store.dispatch, store.getState, {});
  };

  it('should be role Admin', async () => {
    const mockAction = toastSpy('addToast');
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: { commonConfig: { isAdminRole: true, org: 'org', app: 'app' } }
    });
    await runWhenAPISuccess({
      collectionForm: {
        [storeId]: {
          viewName: ['PRIMARY'],
          method: COLLECTION_METHOD.CREATE
        }
      },
      form: {
        ['PRIMARY']: {
          values: {}
        }
      },
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          updateDeceasedLoading: false
        }
      }
    });
    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
    });
  });

  it('should be fulfilled when submit form > case PRIMARY', async () => {
    const mockAction = toastSpy('addToast');
    await runWhenAPISuccess({
      collectionForm: {
        [storeId]: {
          viewName: ['PRIMARY'],
          method: COLLECTION_METHOD.CREATE
        }
      },
      form: {
        ['PRIMARY']: {
          values: {}
        }
      },
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          updateDeceasedLoading: false
        }
      }
    });
    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
    });
  });

  it('should be fulfilled when submit form > case PRIMARY error', async () => {
    const mockAction = toastSpy('addToast');
    await runWhenAPIFailed({
      collectionForm: {
        [storeId]: {
          viewName: ['PRIMARY'],
          method: COLLECTION_METHOD.CREATE
        }
      },
      form: {
        ['PRIMARY']: {
          values: {}
        }
      },
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          updateDeceasedLoading: false
        }
      }
    });
    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_COLLECTION_FORM.TOAST_DECEASED_SUBMIT_FAILURE
    });
  });

  it('should be fulfilled when submit form > case SECONDARY', async () => {
    const mockAction = toastSpy('addToast');
    await runWhenAPISuccess({
      collectionForm: {
        [storeId]: {
          viewName: ['SECONDARY'],
          method: COLLECTION_METHOD.CREATE
        }
      },
      form: {
        ['SECONDARY']: {
          values: {}
        }
      },
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          updateDeceasedLoading: false
        }
      }
    });
    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
    });
  });

  it('should be fulfilled when submit form > case BOTH', async () => {
    const mockAction = toastSpy('addToast');
    await runWhenAPISuccess({
      collectionForm: {
        [storeId]: {
          viewName: ['BOTH'],
          method: COLLECTION_METHOD.CREATE
        }
      },
      form: {
        ['BOTH']: {
          values: {}
        }
      },
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          updateDeceasedLoading: false
        }
      }
    });

    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
    });
  });

  it('should be fulfilled when update form', async () => {
    const mockAction = toastSpy('addToast');
    await runWhenAPISuccess({
      collectionForm: {
        [storeId]: {
          viewName: ['PRIMARY'],
          method: COLLECTION_METHOD.EDIT
        }
      },
      form: {
        ['PRIMARY']: {
          values: {}
        }
      },
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          updateDeceasedLoading: false
        }
      }
    });

    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATED
    });
  });

  it('should be fulfilled when submit form > case PRIMARY', async () => {
    const mockAction = toastSpy('addToast');
    const mockOpenUploadFileModal = collectionFormSpy('openUploadFileModal');

    await runWhenAPISuccess({
      userRole: {
        role: 'CXCOL1'
      },
      collectionForm: {
        [storeId]: {
          viewName: ['PRIMARY'],
          method: COLLECTION_METHOD.CREATE
        }
      },
      form: {
        ['PRIMARY']: {
          values: {}
        }
      },
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          updateDeceasedLoading: false
        }
      }
    });
    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
    });
    expect(mockOpenUploadFileModal).toBeCalledWith({
      storeId,
      type: 'deathCertificate'
    });
  });

  it('should be rejected when submit form', async () => {
    const mockAction = toastSpy('addToast');
    await runWhenAPIFailed({
      collectionForm: {
        [storeId]: {
          viewName: ['PRIMARY'],
          method: COLLECTION_METHOD.CREATE
        }
      },
      form: {
        ['PRIMARY']: {
          values: {}
        }
      },
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          updateDeceasedLoading: false
        }
      }
    });

    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_COLLECTION_FORM.TOAST_DECEASED_SUBMIT_FAILURE
    });
  });

  it('should be rejected when update form', async () => {
    const mockAction = toastSpy('addToast');
    await runWhenAPIFailed({
      collectionForm: {
        [storeId]: {
          viewName: ['PRIMARY'],
          method: COLLECTION_METHOD.EDIT
        }
      },
      form: {
        ['PRIMARY']: {
          values: {}
        }
      },
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          updateDeceasedLoading: false
        }
      }
    });
    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_COLLECTION_FORM.TOAST_DECEASED_UPDATE_FAILURE
    });
  });

  it('should show error', async () => {
    const mockAction = toastSpy('addToast');
    await runWhenAPIFailed({
      collectionForm: {
        [storeId]: {
          viewName: ['PRIMARY'],
          method: COLLECTION_METHOD.CREATE
        }
      },
      form: {
        ['PRIMARY']: {
          values: {}
        }
      },
      deceased: {
        [storeId]: {
          updateDeceasedLoading: false
        }
      }
    });
    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_COLLECTION_FORM.TOAST_DECEASED_SUBMIT_FAILURE
    });
  });
});
