import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
// Service
import { deceasedService } from '../deceasedServices';

// Type
import {
  DeceasedCreateWorkflowArgs,
  DeceasedCreateWorkflowPayload,
  DeceasedState
} from '../types';

export const deceasedCreateWorkflowRequest = createAsyncThunk<
  DeceasedCreateWorkflowPayload,
  DeceasedCreateWorkflowArgs,
  ThunkAPIConfig
>('deceased/deceasedCreateWorkflowRequest', async (args, thunkAPI) => {
  const { commonData, data } = args;

  try {
    const response = await deceasedService.deceasedCreateWorkflow(
      commonData,
      data
    );
    const responseMessage = response?.data?.messages?.[0]?.status;

    if (responseMessage !== 'success') {
      throw response;
    }
    return response.data;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const deceasedCreateWorkflowRequestBuilder = (
  builder: ActionReducerMapBuilder<DeceasedState>
) => {
  builder
    .addCase(deceasedCreateWorkflowRequest.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        updateDeceasedLoading: true
      };
    })
    .addCase(deceasedCreateWorkflowRequest.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        updateDeceasedLoading: false
      };
    })
    .addCase(deceasedCreateWorkflowRequest.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        updateDeceasedLoading: false
      };
    });
};
