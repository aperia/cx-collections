import { createAsyncThunk, isFulfilled, isRejected } from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import {
  DeceasedCommonRequestData,
  RemoveDeceasedStatusArgs,
  RemoveDeceasedStatusPayload
} from '../types';
import { deceasedCreateWorkflowRequest } from './deceasedCreateWorkflow';
import { prepareCommonData } from '../helpers';
import { batch } from 'react-redux';
import { deceasedInformationRequest } from './deceasedInformation';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import {
  dispatchDelayProgress,
  setDelayProgress
} from 'pages/CollectionForm/helpers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export const triggerRemoveDeceasedStatus = createAsyncThunk<
  RemoveDeceasedStatusPayload,
  RemoveDeceasedStatusArgs,
  ThunkAPIConfig
>('deceased/triggerRemoveDeceasedStatusRequest', async (args, thunkAPI) => {
  const { storeId, entryCode } = args;
  const { dispatch } = thunkAPI;
  const { deceased, accountDetail } = thunkAPI.getState();
  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;
  const { operatorID: operatorCode } = window.appConfig?.commonConfig;

  const showError = () => {
    const message = I18N_COLLECTION_FORM.REMOVE_DECEASED_FAILURE;
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message
      })
    );
  };

  const commonData: DeceasedCommonRequestData = prepareCommonData(
    eValueAccountId!
  );

  const response = await dispatch(
    deceasedCreateWorkflowRequest({
      storeId,
      commonData,
      data: {
        callingApplication:
          window.appConfig?.commonConfig?.callingApplication || '',
        operatorCode,
        entryCode,
        deceasedInformation: deceased[storeId].deceasedInformation
      }
    })
  );

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_COLLECTION_FORM.REMOVE_DECEASED_SUCCESS
        })
      );
      dispatch(deceasedInformationRequest({ storeId }));
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inCollectionFormFlyOut'
        })
      );
      const info = {
        callResultType: CALL_RESULT_CODE.NOT_DECEASED
      };
      dispatch(
        collectionFormActions.toggleCallResultInProgressStatus({
          storeId,
          info,
          lastDelayCallResult: CALL_RESULT_CODE.NOT_DECEASED
        })
      );
      dispatchDelayProgress(deceasedInformationRequest({ storeId }), storeId);

      setDelayProgress(storeId, CALL_RESULT_CODE.NOT_DECEASED, info);
    });
  }
  if (isRejected(response)) {
    showError();
    dispatch(
      apiErrorNotificationAction.updateApiError({
        storeId,
        forSection: 'inCollectionFormFlyOut',
        apiResponse: response.payload?.response
      })
    );
  }
});
