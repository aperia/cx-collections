import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Type
import {
  DeceasedResultTab,
  DeceasedState,
  IPayLoadChangeDeceased,
  SetIsJointAccPayload
} from '../types';

// Const
import {
  DECEASED_PARTIES,
  DECEASED_PARTIES_OF_NOT_JOINT_ACC
} from '../constants';

import {
  deceasedInformationBuilder,
  deceasedInformationRequest
} from './deceasedInformation';
import {
  deceasedCreateWorkflowRequest,
  deceasedCreateWorkflowRequestBuilder
} from './deceasedCreateWorkflow';
import { triggerRemoveDeceasedStatus } from './removeDeceasedStatus';
import { triggerDeceasedPending } from './triggerDeceasedPending';
import { triggerDeceasedConfirmed } from './triggerDeceasedConfirmed';

const { actions, reducer } = createSlice({
  name: 'deceased',
  initialState: {} as DeceasedState,
  reducers: {
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    changeDeceasedParty: (
      draftState,
      action: PayloadAction<IPayLoadChangeDeceased>
    ) => {
      const { storeId, deceasedParty } = action.payload;
      const [defaultDeceasedParty] = draftState[storeId]?.isJointAcc
        ? DECEASED_PARTIES
        : DECEASED_PARTIES_OF_NOT_JOINT_ACC;
      draftState[storeId] = {
        ...draftState[storeId],
        deceasedParty: deceasedParty || defaultDeceasedParty
      };
    },
    resetDeceasedParty: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      const [defaultDeceasedParty] = draftState[storeId]?.isJointAcc
        ? DECEASED_PARTIES
        : DECEASED_PARTIES_OF_NOT_JOINT_ACC;
      draftState[storeId] = {
        ...draftState[storeId],
        deceasedParty: defaultDeceasedParty
      };
      const deceasedPartyName =
        draftState[storeId]?.deceasedInformation?.deceasedPartyName;
      if (deceasedPartyName) {
        const party = DECEASED_PARTIES.find(
          item => item.value === deceasedPartyName.toLowerCase()
        );
        draftState[storeId] = {
          ...draftState[storeId],
          deceasedParty: party || defaultDeceasedParty
        };
      }
    },
    setIsJointAcc: (
      draftState,
      action: PayloadAction<SetIsJointAccPayload>
    ) => {
      const { storeId, isJointAcc } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isJointAcc
      };
    },
    updateSelectedDeceasedResultTab: (
      draftState,
      action: PayloadAction<{ storeId: string; tabKey: DeceasedResultTab }>
    ) => {
      const { storeId, tabKey } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        selectedDeceasedResultTab: tabKey
      };
    }
  },
  extraReducers: builder => {
    deceasedInformationBuilder(builder);
    deceasedCreateWorkflowRequestBuilder(builder);
  }
});

const allActions = {
  ...actions,
  deceasedInformationRequest,
  deceasedCreateWorkflowRequest,
  triggerRemoveDeceasedStatus,
  triggerDeceasedPending,
  triggerDeceasedConfirmed
};

export { allActions as deceasedActions, reducer };
