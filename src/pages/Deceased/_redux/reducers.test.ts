import { DeceasedResultTab, DeceasedState } from '../types';
import { deceasedActions, reducer } from './reducers';
import { storeId } from 'app/test-utils';
import { DECEASED_PARTIES } from '../constants';

describe('Test Deceased Reducers', () => {
  const initialState: DeceasedState = {
    [storeId]: {
      deceasedInformation: {
        deceasedPartyName: 'SECONDARY'
      }
    },
    ['other-store-id']: {
      deceasedInformation: {
        deceasedPartyName: 'wrong'
      }
    },
    ['empty']: {}
  };

  const initialStateForJointAcc: DeceasedState = {
    [storeId]: {
      isJointAcc: true,
      deceasedInformation: {
        deceasedPartyName: 'PRIMARY'
      }
    },
    ['empty']: {}
  };

  describe('removeStore', () => {
    it('should remove store', () => {
      const state = reducer(
        initialState,
        deceasedActions.removeStore({ storeId })
      );
      expect(state[storeId]).toBeUndefined();
    });
  });

  describe('changeDeceasedParty', () => {
    it('should return default deceasedParty', () => {
      const state = reducer(
        initialState,
        deceasedActions.changeDeceasedParty({ storeId })
      );
      expect(state[storeId].deceasedParty).toEqual(DECEASED_PARTIES[0]);
    });

    it('should return new deceasedParty', () => {
      const state = reducer(
        initialState,
        deceasedActions.changeDeceasedParty({
          storeId,
          deceasedParty: DECEASED_PARTIES[1]
        })
      );
      expect(state[storeId].deceasedParty).toEqual(DECEASED_PARTIES[1]);
    });

    it('should return default deceasedParty > isJointAcc', () => {
      const state = reducer(
        initialStateForJointAcc,
        deceasedActions.changeDeceasedParty({ storeId })
      );
      expect(state[storeId].deceasedParty).toEqual(DECEASED_PARTIES[0]);
    });
  });

  describe('resetDeceasedParty', () => {
    it('should set default value to deceasedParty > not having value', () => {
      const state = reducer(
        initialState,
        deceasedActions.resetDeceasedParty({ storeId: 'empty' })
      );
      expect(state['empty'].deceasedParty).toEqual(DECEASED_PARTIES[0]);
    });

    it('should set default value to deceasedParty > having value', () => {
      const state = reducer(
        initialState,
        deceasedActions.resetDeceasedParty({ storeId: 'other-store-id' })
      );
      expect(state['other-store-id'].deceasedParty).toEqual(
        DECEASED_PARTIES[0]
      );
    });

    it('should reset deceasedParty to initial value', () => {
      const state = reducer(
        initialState,
        deceasedActions.resetDeceasedParty({ storeId })
      );
      expect(state[storeId].deceasedParty).toEqual(DECEASED_PARTIES[1]);
    });

    it('should set default value to deceasedParty for joint acc', () => {
      const state = reducer(
        initialStateForJointAcc,
        deceasedActions.resetDeceasedParty({ storeId })
      );
      expect(state[storeId].deceasedParty).toEqual(DECEASED_PARTIES[0]);
    });
  });

  describe('setIsJointAcc', () => {
    it('set data success', () => {
      const state = reducer(
        initialStateForJointAcc,
        deceasedActions.setIsJointAcc({ storeId: 'empty', isJointAcc: true })
      );
      expect(state['empty'].isJointAcc).toEqual(true);
    });
  });

  describe('updateSelectedDeceasedResultTab', () => {
    it('set data success', () => {
      const state = reducer(
        initialStateForJointAcc,
        deceasedActions.updateSelectedDeceasedResultTab({
          storeId,
          tabKey: DeceasedResultTab.DeceasedInfo
        })
      );
      expect(state[storeId].selectedDeceasedResultTab).toEqual(
        DeceasedResultTab.DeceasedInfo
      );
    });
  });
});
