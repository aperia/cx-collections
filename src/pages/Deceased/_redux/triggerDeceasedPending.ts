import { createAsyncThunk, isFulfilled, isRejected } from '@reduxjs/toolkit';
import { batch } from 'react-redux';
// helpers
import { prepareCommonData, prepareDeceasedPendingData } from '../helpers';
// types
import {
  DeceasedCommonRequestData,
  TriggerDeceasedPendingArgs,
  TriggerDeceasedPendingPayload,
  ViewDataInfo
} from '../types';
// actions
import { deceasedCreateWorkflowRequest } from './deceasedCreateWorkflow';
import { deceasedInformationRequest } from './deceasedInformation';
import { showErrorToast } from '../helpers';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
// constants
import {
  DECEASED_PARTY_VALUE,
  PRIMARY_VIEW_KY,
  SECONDARY_VIEW_KY
} from '../constants';
import {
  CALL_RESULT_CODE,
  COLLECTION_METHOD
} from 'pages/CollectionForm/constants';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import {
  dispatchDelayProgress,
  setDelayProgress
} from 'pages/CollectionForm/helpers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export const triggerDeceasedPending = createAsyncThunk<
  TriggerDeceasedPendingPayload,
  TriggerDeceasedPendingArgs,
  ThunkAPIConfig
>('deceased/triggerDeceasedPending', async (args, thunkAPI) => {
  const { storeId, entryCode } = args;
  const { form, deceased, collectionForm, accountDetail } = thunkAPI.getState();
  const dispatch = thunkAPI.dispatch;

  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;

  try {
    // Prepare request data
    const deceasedPartyName =
      deceased[storeId]?.deceasedParty?.value || DECEASED_PARTY_VALUE.PRIMARY;

    const insuranceIndicator =
      accountDetail[storeId]?.rawData?.creditLifeCode || '';

    const {
      callingApplication,
      operatorID = '',
      isAdminRole,
      isCollector
    } = window.appConfig?.commonConfig;

    // Mapped data
    let viewDataInfo: ViewDataInfo = {};
    collectionForm[storeId]?.viewName?.forEach(item => {
      if (item.includes(PRIMARY_VIEW_KY)) {
        viewDataInfo = {
          ...viewDataInfo,
          primary: {
            ...viewDataInfo?.primary,
            ...form[item]?.values,
            deceasedInsuranceIndicator: insuranceIndicator
          }
        };
      } else if (item.includes(SECONDARY_VIEW_KY)) {
        viewDataInfo = {
          ...viewDataInfo,
          secondary: {
            ...viewDataInfo?.secondary,
            ...form[item]?.values,
            deceasedInsuranceIndicator: insuranceIndicator
          }
        };
      }
    });

    const commonData: DeceasedCommonRequestData = prepareCommonData(
      eValueAccountId!
    );
    const requestData = prepareDeceasedPendingData(
      deceasedPartyName,
      viewDataInfo,
      entryCode,
      callingApplication,
      operatorID
    );

    const response = await dispatch(
      deceasedCreateWorkflowRequest({ storeId, commonData, data: requestData })
    );

    if (isFulfilled(response)) {
      batch(() => {
        const isSubmit =
          collectionForm[storeId].method === COLLECTION_METHOD.CREATE;
        const message = isSubmit
          ? I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
          : I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATED;

        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message
          })
        );
        dispatch(deceasedInformationRequest({ storeId }));
        dispatch(collectionFormActions.toggleModal({ storeId }));
        dispatch(
          collectionFormActions.getLatestActionRequest({
            storeId
          })
        );
        dispatch(
          apiErrorNotificationAction.clearApiErrorData({
            storeId,
            forSection: 'inCollectionFormModal'
          })
        );

        if (isSubmit) {
          const roleViewDeathCertificate = isCollector || isAdminRole;
          const info = {
            callResultType: CALL_RESULT_CODE.DECEASE_PENDING
          };

          // Upload files when success CXC-3845
          roleViewDeathCertificate &&
            dispatch(
              collectionFormActions.openUploadFileModal({
                storeId,
                type: 'deathCertificate'
              })
            );
          dispatch(
            collectionFormActions.toggleCallResultInProgressStatus({
              storeId,
              info,
              lastDelayCallResult: CALL_RESULT_CODE.DECEASE_PENDING
            })
          );
          dispatchDelayProgress(
            deceasedInformationRequest({ storeId }),
            storeId
          );

          setDelayProgress(storeId, CALL_RESULT_CODE.DECEASE_PENDING, info);
        }
      });
    }

    if (isRejected(response)) {
      dispatch(showErrorToast(storeId));
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inCollectionFormModal',
          apiResponse: response.payload?.response
        })
      );
    }
  } catch (error) {}
});
