import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import {
  accEValue,
  byPassFulfilled,
  createStoreWithDefaultMiddleWare,
  mockActionCreator,
  storeId
} from 'app/test-utils';
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { deceasedCreateWorkflowRequest } from './deceasedCreateWorkflow';
import { triggerDeceasedConfirmed } from './triggerDeceasedConfirmed';
import { deceasedService } from '../deceasedServices';

let spy: jest.SpyInstance;

const toastSpy = mockActionCreator(actionsToast);
const collectionFormSpy = mockActionCreator(collectionFormActions);

jest.mock('pages/__commons/ToastNotifications/_redux', () => ({
  ...(jest.requireActual(
    'pages/__commons/ToastNotifications/_redux'
  ) as object),
  actionsToast: { addToast: jest.fn() }
}));
describe('triggerDeceasedConfirmed', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isCollector: true
    }
  };
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('should isAdminRole', async () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: { commonConfig: { isAdminRole: true, org: 'org', app: 'app' } }
    });
    const mockAction = toastSpy('addToast');
    const store = createStoreWithDefaultMiddleWare({
      collectionForm: {
        [storeId]: {
          viewName: ['PRIMARY'],
          method: COLLECTION_METHOD.CREATE
        }
      },
      form: {
        ['PRIMARY']: {
          values: {}
        }
      },
      deceased: {
        [storeId]: {
          updateDeceasedLoading: false
        }
      }
    } as MagicKeyValue);

    await triggerDeceasedConfirmed({
      storeId,
      accEValue,
      entryCode: 'DC'
    })(
      byPassFulfilled(deceasedCreateWorkflowRequest.fulfilled.type, {
        data: {
          messages: [{ status: 'success' }]
        }
      }),
      store.getState,
      {}
    );

    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
    });
  });

  it('should show success toast message > PRIMARY', async () => {
    const mockAction = toastSpy('addToast');
    const store = createStoreWithDefaultMiddleWare({
      collectionForm: {
        [storeId]: {
          viewName: ['PRIMARY'],
          method: COLLECTION_METHOD.CREATE
        }
      },
      form: {
        ['PRIMARY']: {
          values: {}
        }
      },
      deceased: {
        [storeId]: {
          updateDeceasedLoading: false
        }
      }
    });

    await triggerDeceasedConfirmed({
      storeId,
      accEValue,
      entryCode: 'DC'
    })(
      byPassFulfilled(deceasedCreateWorkflowRequest.fulfilled.type, {
        data: {
          messages: [{ status: 'success' }]
        }
      }),
      store.getState,
      {}
    );

    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
    });
  });

  it('should show success toast message > SECONDARY', async () => {
    const mockAction = toastSpy('addToast');

    const store = createStoreWithDefaultMiddleWare({
      collectionForm: {
        [storeId]: {
          viewName: ['SECONDARY'],
          method: COLLECTION_METHOD.EDIT
        }
      },
      form: {
        ['SECONDARY']: {
          values: {}
        }
      },
      deceased: {
        [storeId]: {
          updateDeceasedLoading: false
        }
      }
    });

    await triggerDeceasedConfirmed({
      storeId,
      accEValue,
      entryCode: 'DC'
    })(
      byPassFulfilled(deceasedCreateWorkflowRequest.fulfilled.type, {
        data: {
          messages: [{ status: 'success' }]
        }
      }),
      store.getState,
      {}
    );

    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATED
    });
  });

  it('should show success toast message > BOTH', async () => {
    const mockAction = toastSpy('addToast');

    const store = createStoreWithDefaultMiddleWare({
      collectionForm: {
        [storeId]: {
          viewName: ['BOTH'],
          method: COLLECTION_METHOD.EDIT
        }
      },
      form: {
        ['BOTH']: {
          values: {}
        }
      },
      deceased: {
        [storeId]: {
          updateDeceasedLoading: false
        }
      }
    });

    await triggerDeceasedConfirmed({
      storeId,
      accEValue,
      entryCode: 'DC'
    })(
      byPassFulfilled(deceasedCreateWorkflowRequest.fulfilled.type, {
        data: {
          messages: [{ status: 'success' }]
        }
      }),
      store.getState,
      {}
    );

    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_UPDATED
    });
  });

  it('should show error toast message > submit failed', async () => {
    const mockAction = toastSpy('addToast');
    spy = jest
      .spyOn(deceasedService, 'deceasedCreateWorkflow')
      .mockRejectedValue({});

    const store = createStoreWithDefaultMiddleWare({
      collectionForm: {
        [storeId]: {
          viewName: ['BOTH'],
          method: COLLECTION_METHOD.CREATE
        }
      },
      form: {
        ['BOTH']: {
          values: {}
        }
      },
      deceased: {
        [storeId]: {
          updateDeceasedLoading: false
        }
      }
    });

    await triggerDeceasedConfirmed({
      storeId,
      accEValue,
      entryCode: 'DC'
    })(store.dispatch, store.getState, {});

    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_COLLECTION_FORM.TOAST_DECEASED_SUBMIT_FAILURE
    });
  });

  it('should open upload modal if has permission', async () => {
    const mockAction = toastSpy('addToast');
    const mockOpenUploadFileModal = collectionFormSpy('openUploadFileModal');

    const store = createStoreWithDefaultMiddleWare({
      collectionForm: {
        [storeId]: {
          viewName: ['PRIMARY'],
          method: COLLECTION_METHOD.CREATE
        }
      },
      form: {
        ['PRIMARY']: {
          values: {}
        }
      },
      deceased: {
        [storeId]: {
          updateDeceasedLoading: false
        }
      }
    });

    await triggerDeceasedConfirmed({
      storeId,
      accEValue,
      entryCode: 'DC'
    })(
      byPassFulfilled(deceasedCreateWorkflowRequest.fulfilled.type, {
        data: {
          messages: [{ status: 'success' }]
        }
      }),
      store.getState,
      {}
    );

    expect(mockAction).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
    });
    expect(mockOpenUploadFileModal).toBeCalledWith({
      storeId,
      type: 'deathCertificate'
    });
  });
});
