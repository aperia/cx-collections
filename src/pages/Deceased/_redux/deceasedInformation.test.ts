import { responseDefault, storeId } from 'app/test-utils';
import accountDetailService from 'pages/AccountDetails/accountDetailService';
import { collectionFormService } from 'pages/CollectionForm/collectionFormServices';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import { ERR_CANNOT_PROCESS } from '../constants';
import { deceasedService } from '../deceasedServices';
import { deceasedInformationRequest } from './deceasedInformation';

describe('Test deceasedInformationRequest async action', () => {
  it('fulfilled', async () => {
    const store = createStore(rootReducer, {
      deceased: {
        [storeId]: {
          isJointAcc: true
        }
      }
    });

    jest.spyOn(deceasedService, 'deceasedInformation').mockResolvedValue({
      ...responseDefault,
      data: {
        deceasedInformation: {
          deceasedPartyName: 'PRIMARY'
        }
      }
    });

    const args = {
      storeId,
      accEValue: storeId
    };
    const result = await deceasedInformationRequest(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'deceased/deceasedInformationRequest/fulfilled'
    );
  });

  it('fulfilled > pass isInProgress', async () => {
    const store = createStore(
      rootReducer,
      {
        deceased: {
          [storeId]: {
            isJointAcc: true
          }
        },
        collectionForm: {
          [storeId]: {
            isInProgress: true,
            inProgressInfo: {
              callResultType: 'DC'
            }
          }
        }
      },
      applyMiddleware(thunk)
    );

    jest.spyOn(collectionFormService, 'getLatestAction').mockResolvedValue({
      ...responseDefault,
      data: {
        actionEntryFields: [{ actionEntryPrefixCode: 'DC' }]
      }
    });

    jest.spyOn(deceasedService, 'deceasedInformation').mockResolvedValue({
      ...responseDefault,
      data: {
        deceasedInformation: {
          deceasedPartyName: 'PRIMARY'
        }
      }
    });

    const args = {
      storeId,
      accEValue: storeId
    };
    const result = await deceasedInformationRequest(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'deceased/deceasedInformationRequest/fulfilled'
    );
  });

  it('fulfilled > pass isJointAcc', async () => {
    const store = createStore(rootReducer, {
      deceased: {
        [storeId]: {}
      }
    });

    jest.spyOn(accountDetailService, 'getAccountDetails').mockResolvedValue({
      ...responseDefault,
      data: {
        customerInquiry: [
          { customerRoleTypeCode: '01' },
          { customerRoleTypeCode: '02' }
        ]
      }
    });

    jest.spyOn(deceasedService, 'deceasedInformation').mockResolvedValue({
      ...responseDefault,
      data: {
        deceasedInformation: {
          deceasedPartyName: 'PRIMARY'
        }
      }
    });

    const args = {
      storeId,
      accEValue: storeId
    };
    const result = await deceasedInformationRequest(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'deceased/deceasedInformationRequest/fulfilled'
    );
  });

  it('fulfilled > pass isJointAcc - coverage extra reducer', async () => {
    const store = createStore(rootReducer, {
      deceased: {
        [storeId]: {}
      }
    });

    jest.spyOn(accountDetailService, 'getAccountDetails').mockResolvedValue({
      ...responseDefault,
      data: {
        customerInquiry: [{ customerRoleTypeCode: '01' }]
      }
    });

    jest.spyOn(deceasedService, 'deceasedInformation').mockResolvedValue({
      ...responseDefault,
      data: {
        deceasedInformation: {
          deceasedPartyName: 'PRIMARY'
        }
      }
    });

    const args = {
      storeId,
      accEValue: storeId
    };
    const result = await deceasedInformationRequest(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'deceased/deceasedInformationRequest/fulfilled'
    );
  });

  it('fulfilled > Not Deceased', async () => {
    const store = createStore(rootReducer, {
      deceased: {
        [storeId]: {
          isJointAcc: true
        }
      },
      collectionForm: {
        [storeId]: {
          lastFollowUpData: {
            callResultType: 'ND'
          }
        }
      }
    });

    jest.spyOn(deceasedService, 'deceasedInformation').mockResolvedValue({
      ...responseDefault,
      data: {
        deceasedInformation: {
          deceasedPartyName: 'PRIMARY'
        }
      }
    });

    const args = {
      storeId,
      accEValue: storeId
    };
    const result = await deceasedInformationRequest(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual(
      'deceased/deceasedInformationRequest/fulfilled'
    );
  });

  it('rejected', async () => {
    const store = createStore(rootReducer, {
      deceased: {
        [storeId]: {
          isJointAcc: true
        }
      }
    });

    jest.spyOn(deceasedService, 'deceasedInformation').mockResolvedValue({
      ...responseDefault,
      data: {
        statusCode: ERR_CANNOT_PROCESS
      }
    });

    const args = {
      storeId,
      accEValue: storeId
    };
    const result = await deceasedInformationRequest(args)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual('deceased/deceasedInformationRequest/rejected');
  });
});
