// mocks
import { useSelectorMock } from 'app/test-utils/useSelectorMock';

// utils
import { selectorWrapper, storeId } from 'app/test-utils';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import {
  DECEASED_PARTIES,
  DECEASED_PARTY_DESCRIPTION,
  PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW
} from '../constants';
import {
  selectDateNoticeReceivedPrimaryForm,
  selectDeceasedParty,
  selectDeceasedSettlement,
  selectIsShowPrimaryMainInfo,
  selectIsShowSecondaryMainInfo,
  selectIsShowSettlementInfo,
  selectPrimaryMainInfo,
  selectPrimaryPartyInfo,
  selectSecondaryMainInfo,
  selectSecondaryPartyInfo,
  selectSettlementInfo,
  selectValueDeceasedMainPrimary,
  selectValueDeceasedMainSecond,
  selectValueDeceasedParty,
  selectValueDeceasedPrimary,
  selectValueDeceasedSecond,
  selectPrimaryInfo,
  selectPrimaryName,
  selectInsuranceIndicator,
  selectSecondaryInfo,
  selectSecondaryName,
  selectStateRef,
  selectUpdateDeceasedLoading,
  selectDeathCertificatesError,
  selectDeathCertificatesFiles,
  selectDeathCertificatesLoading,
  selectedDeceasedResultTab,
  selectRawAccountDetail,
  selectCreditLifeCode,
  selectIsJointAcc,
  selectGetDeceasedInfoLoading,
  selectDeceasedPartyDescription,
  selectOriginDeceasedParty
} from './selectors';
import { REF_DATA_TYPE } from 'pages/AccountSearch/Home/types';
import { CollectionFormState } from 'pages/CollectionForm/types';

describe('Deceased Selectors', () => {
  const primaryDeceasedPartyStore: Partial<RootState> = {
    deceased: {
      [storeId]: {
        deceasedParty: DECEASED_PARTIES[0],
        loading: true,
        deathCertificates: {},
        deceasedInformation: {
          insuranceIndicator: '0',
          deceasedPrimaryInformation: {
            deceasedDatePrimary: '2021/03/25'
          },
          deceasedPartyName: 'name'
        },
        uploadCertificateLoading: {
          primary: true
        },
        removeCertificateLoading: {
          primary: true
        },
        updateDeceasedLoading: true,
        isJointAcc: true
      }
    },
    refData: {
      [REF_DATA_TYPE.USA_STATE]: {
        data: [{ value: 'TX', description: 'Texas' }]
      }
    },
    accountDetail: {
      [storeId]: {
        rawData: {
          customerName: 'primaryName',
          creditLifeCode: {}
        }
      }
    }
  };

  const secondaryDeceasedPartyStore: Partial<RootState> = {
    deceased: {
      [storeId]: {
        deceasedParty: DECEASED_PARTIES[1],
        loading: false,
        deceasedInformation: {
          deceasedSecondaryInformation: {
            deceasedDateSecondary: '2021/03/25'
          }
        },
        uploadCertificateLoading: {
          secondary: true
        },
        removeCertificateLoading: {
          secondary: true
        },
        isJointAcc: false
      }
    },
    accountDetail: {
      [storeId]: {
        rawData: {
          secondaryName: 'SecondaryName'
        }
      }
    }
  };

  const bothDeceasedPartyStore: Partial<RootState> = {
    deceased: {
      [storeId]: {
        deceasedParty: DECEASED_PARTIES[2],
        loading: false
      }
    },
    accountDetail: {
      [storeId]: {
        rawData: {
          customerName: 'primaryName',
          secondaryName: 'SecondaryName'
        }
      }
    }
  };

  const mockCollectionFormState: CollectionFormState = {
    [storeId]: {
      lastFollowUpData: {
        callResultType: undefined
      },
      last10Actions: {
        loading: false,
        data: {},
        error: false,
        isOpen: false
      },
      uploadFiles: {}
    }
  };

  describe('selectValueDeceasedParty', () => {
    it('should return primary', () => {
      const { data, emptyData } = selectorWrapper(primaryDeceasedPartyStore)(
        selectValueDeceasedParty
      );
      expect(data).toEqual('primary');
      expect(emptyData).toEqual('primary');
    });
  });

  describe('selectIsShowPrimaryMainInfo', () => {
    it('should return TRUE when deceased party is primary', () => {
      const data = useSelectorMock(
        selectIsShowPrimaryMainInfo,
        primaryDeceasedPartyStore,
        storeId
      );
      const expected = true;
      expect(data).toEqual(expected);
    });

    it('should return TRUE when deceased party is both', () => {
      const data = useSelectorMock(
        selectIsShowPrimaryMainInfo,
        bothDeceasedPartyStore,
        storeId
      );
      const expected = true;
      expect(data).toEqual(expected);
    });

    it('should return FALSE when deceased party is secondary', () => {
      const data = useSelectorMock(
        selectIsShowPrimaryMainInfo,
        secondaryDeceasedPartyStore,
        storeId
      );
      const expected = false;
      expect(data).toEqual(expected);
    });
  });

  describe('selectIsShowSecondaryMainInfo', () => {
    it('should return TRUE when deceased party is secondary', () => {
      const data = useSelectorMock(
        selectIsShowSecondaryMainInfo,
        secondaryDeceasedPartyStore,
        storeId
      );
      const expected = true;
      expect(data).toEqual(expected);
    });

    it('should return TRUE when deceased party is both', () => {
      const data = useSelectorMock(
        selectIsShowSecondaryMainInfo,
        bothDeceasedPartyStore,
        storeId
      );
      const expected = true;
      expect(data).toEqual(expected);
    });

    it('should return FALSE when deceased party is primary', () => {
      const data = useSelectorMock(
        selectIsShowSecondaryMainInfo,
        primaryDeceasedPartyStore,
        storeId
      );
      const expected = false;
      expect(data).toEqual(expected);
    });
  });

  describe('selectIsShowSettlementInfo', () => {
    const store: Partial<RootState> = {
      collectionForm: {
        [storeId]: {
          ...mockCollectionFormState[storeId],
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.DECEASE
          }
        }
      }
    };
    const emptyStore: Partial<RootState> = {
      collectionForm: {
        [storeId]: {
          ...mockCollectionFormState[storeId],
          lastFollowUpData: {
            callResultType: undefined
          }
        }
      }
    };
    const deceasedPendingStore: Partial<RootState> = {
      collectionForm: {
        [storeId]: {
          ...mockCollectionFormState[storeId],
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.DECEASE_PENDING
          }
        }
      }
    };

    it('should return FALSE when call result type of collection form is DECEASE_PENDING', () => {
      const data = useSelectorMock(
        selectIsShowSettlementInfo,
        deceasedPendingStore,
        storeId
      );
      const expected = false;
      expect(data).toEqual(expected);
    });

    it('should return FALSE when call result type of collection form is empty', () => {
      const data = useSelectorMock(
        selectIsShowSettlementInfo,
        emptyStore,
        storeId
      );
      const expected = false;
      expect(data).toEqual(expected);
    });

    it('should return TRUE when call result type of collection form is different from DECEASE', () => {
      const data = useSelectorMock(selectIsShowSettlementInfo, store, storeId);
      const expected = true;
      expect(data).toEqual(expected);
    });
  });

  describe('selectPrimaryMainInfo', () => {
    const store: Partial<RootState> = {
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false,
          deceasedInformation: {
            deceasedPrimaryInformation: {
              deceasedDatePrimary: '2021/03/25',
              deceasedDateNoticeReceivedPrimary: undefined
            }
          }
        }
      }
    };
    const emptyStore: Partial<RootState> = {
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false
        }
      }
    };
    const emptyDataStore: Partial<RootState> = {
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false,
          deceasedInformation: {
            deceasedPrimaryInformation: {
              deceasedDatePrimary: ''
            }
          }
        }
      }
    };

    it('should return NULL with empty store', () => {
      const data = useSelectorMock(selectPrimaryMainInfo, emptyStore, storeId);
      const expected = null;
      expect(data).toEqual(expected);
    });

    it('should return NULL when empty data', () => {
      const data = useSelectorMock(
        selectPrimaryMainInfo,
        emptyDataStore,
        storeId
      );
      const expected = null;
      expect(data).toEqual(expected);
    });

    it('should return data', () => {
      const data = useSelectorMock(selectPrimaryMainInfo, store, storeId);
      const expected = data;
      expect(data).toEqual(expected);
    });
  });

  describe('selectSecondaryMainInfo', () => {
    const store: Partial<RootState> = {
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false,
          deceasedInformation: {
            deceasedSecondaryInformation: {
              deceasedDateSecondary: '2021/03/25'
            }
          }
        }
      }
    };
    const emptyStore: Partial<RootState> = {
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false
        }
      }
    };
    const emptyDataStore: Partial<RootState> = {
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false,
          deceasedInformation: {
            deceasedSecondaryInformation: {
              deceasedDateSecondary: ''
            }
          }
        }
      }
    };

    it('should return data', () => {
      const data = useSelectorMock(selectSecondaryMainInfo, store, storeId);
      const expected = {
        deceasedDateSecondary: '2021/03/25'
      };
      expect(data).toEqual(expected);
    });
    it('should return null when empty store', () => {
      const data = useSelectorMock(
        selectSecondaryMainInfo,
        emptyStore,
        storeId
      );
      expect(data).toEqual(null);
    });
    it('should return null when empty data', () => {
      const data = useSelectorMock(
        selectSecondaryMainInfo,
        emptyDataStore,
        storeId
      );
      expect(data).toEqual(null);
    });
  });

  describe('selectSettlementInfo', () => {
    const store: Partial<RootState> = {
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false,
          deceasedSettlementInformation: {
            settlementAmount: '10'
          }
        }
      }
    };
    const emptyStore: Partial<RootState> = {
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false
        }
      }
    };
    const emptyDataStore: Partial<RootState> = {
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false,
          deceasedSettlementInformation: {
            settlementAmount: ''
          }
        }
      }
    };

    it('should return data when having data', () => {
      const data = useSelectorMock(selectSettlementInfo, store, storeId);
      expect(data).toEqual({
        settlementAmount: '10'
      });
    });

    it('should return data when empty store', () => {
      const data = useSelectorMock(selectSettlementInfo, emptyStore, storeId);
      expect(data).toEqual(null);
    });

    it('should return data when empty data store', () => {
      const data = useSelectorMock(
        selectSettlementInfo,
        emptyDataStore,
        storeId
      );
      expect(data).toEqual(null);
    });
  });

  describe('selectPrimaryPartyInfo', () => {
    const store: Partial<RootState> = {
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false,
          deceasedInformation: {
            insuranceIndicator: '0',
            deceasedPrimaryInformation: {
              deceasedDatePrimary: '2021/04/26'
            }
          }
        }
      },
      accountDetail: {
        [storeId]: {
          rawData: {
            customerName: 'primaryName'
          }
        }
      }
    };
    const emptyStore: Partial<RootState> = {
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false
        }
      }
    };

    it('should return data when having data', () => {
      const data = useSelectorMock(selectPrimaryPartyInfo, store, storeId);
      const expectData = {
        insuranceIndicator: '0',
        deceasedDatePrimary: '2021/04/26',
        primaryCustomerName: 'primaryName'
      };
      expect(data).toEqual(expectData);
    });

    it('should return default value when empty store', () => {
      const data = useSelectorMock(selectPrimaryPartyInfo, emptyStore, storeId);
      const expectData = {
        primaryCustomerName: '',
        insuranceIndicator: ''
      };
      expect(data).toEqual(expectData);
    });
  });

  describe('selectSecondaryPartyInfo', () => {
    const store: Partial<RootState> = {
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false,
          deceasedInformation: {
            secondaryName: 'name',
            insuranceIndicator: '0',
            deceasedSecondaryInformation: {
              deceasedDateSecondary: '2021/04/26'
            }
          }
        }
      },
      accountDetail: {
        [storeId]: {
          rawData: {
            secondaryName: 'secondaryName'
          }
        }
      }
    };
    const emptyStore: Partial<RootState> = {
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false
        }
      }
    };

    it('should return data when having data', () => {
      const data = useSelectorMock(selectSecondaryPartyInfo, store, storeId);
      const expectData = {
        insuranceIndicator: '0',
        deceasedDateSecondary: '2021/04/26',
        secondaryName: 'secondaryName'
      };
      expect(data).toEqual(expectData);
    });

    it('should return default value when empty store', () => {
      const data = useSelectorMock(
        selectSecondaryPartyInfo,
        emptyStore,
        storeId
      );
      const expectData = {
        secondaryName: '',
        insuranceIndicator: ''
      };
      expect(data).toEqual(expectData);
    });
  });

  describe('selectDeceasedParty', () => {
    const store: Partial<RootState> = {
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[1],
          loading: false,
          isJointAcc: true
        }
      }
    };
    const emptyStore: Partial<RootState> = {
      deceased: {
        [storeId]: {
          deceasedParty: {},
          loading: false
        }
      }
    };

    it('should return data', () => {
      const data = useSelectorMock(selectDeceasedParty, store, storeId);
      const expectData = DECEASED_PARTIES[1];
      expect(data).toEqual(expectData);
    });

    it('should return default data', () => {
      const data = useSelectorMock(selectDeceasedParty, emptyStore, storeId);
      const expectData = DECEASED_PARTIES[0];
      expect(data).toEqual(expectData);
    });
  });

  describe('selectValueDeceasedMainPrimary', () => {
    const store: Partial<RootState> = {
      collectionForm: {
        [storeId]: {
          ...mockCollectionFormState[storeId],
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.DECEASE_PENDING
          }
        }
      },
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false,
          deceasedInformation: {
            deceasedPrimaryInformation: {
              deceasedDatePrimary: '2021/04/26',
              deceasedDateNoticeReceivedPrimary: '2021/04/27'
            }
          }
        }
      }
    };

    const notDeceasedStatus: Partial<RootState> = {
      collectionForm: {
        [storeId]: {
          ...mockCollectionFormState[storeId],
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.NOT_DECEASED
          }
        }
      },
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false,
          deceasedInformation: {}
        }
      }
    };

    it('should return data', () => {
      const { data, emptyData } = selectorWrapper(store)(
        selectValueDeceasedMainPrimary
      );
      expect(data).toEqual({
        deceasedDateNoticeReceived: '2021/04/27',
        deceasedDateOfDeath: '2021/04/26'
      });
      expect(emptyData).toEqual({
        deceasedDateNoticeReceived: '',
        deceasedDateOfDeath: undefined
      });
    });

    it('should return data', () => {
      const data = useSelectorMock(
        selectValueDeceasedMainPrimary,
        store,
        storeId
      );
      const expectData = {
        deceasedDateNoticeReceived: '2021/04/27',
        deceasedDateOfDeath: '2021/04/26'
      };
      expect(data).toEqual(expectData);
    });

    it('should return null when status is not deceased', () => {
      const data = useSelectorMock(
        selectValueDeceasedMainPrimary,
        notDeceasedStatus,
        storeId
      );
      const expectData = null;
      expect(data).toEqual(expectData);
    });
  });

  describe('selectValueDeceasedMainSecond', () => {
    const store: Partial<RootState> = {
      collectionForm: {
        [storeId]: {
          ...mockCollectionFormState[storeId],
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.DECEASE_PENDING
          }
        }
      },
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false,
          deceasedInformation: {
            deceasedSecondaryInformation: {
              deceasedDateSecondary: '2021/04/26',
              deceasedDateNoticeReceivedSecondary: '2021/04/27'
            }
          }
        }
      }
    };

    const notDeceasedStatus: Partial<RootState> = {
      collectionForm: {
        [storeId]: {
          ...mockCollectionFormState[storeId],
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.NOT_DECEASED
          }
        }
      },
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false,
          deceasedInformation: {}
        }
      }
    };

    it('should return data', () => {
      const data = useSelectorMock(
        selectValueDeceasedMainSecond,
        store,
        storeId
      );
      const expectData = {
        deceasedDateNoticeReceived: '2021/04/27',
        deceasedDateOfDeath: '2021/04/26'
      };
      expect(data).toEqual(expectData);
    });

    it('should return null when status is not deceased', () => {
      const data = useSelectorMock(
        selectValueDeceasedMainSecond,
        notDeceasedStatus,
        storeId
      );
      const expectData = null;
      expect(data).toEqual(expectData);
    });
  });

  describe('selectValueDeceasedPrimary', () => {
    const store: Partial<RootState> = {
      collectionForm: {
        [storeId]: {
          ...mockCollectionFormState[storeId],
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.DECEASE_PENDING
          }
        }
      },
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false,
          deceasedInformation: {
            deceasedPrimaryInformation: {
              deceasedCountyPrimary: 'county'
            }
          }
        }
      },
      mapping: {
        loading: false,
        data: {
          deceasedPartyPendingResponsePrimary: {
            deceasedCardholderName: 'primaryCustomerName',
            deceasedCounty: 'deceasedCountyPrimary'
          }
        }
      },
      accountDetail: {
        [storeId]: {
          rawData: {
            customerName: 'primaryName'
          }
        }
      }
    };
    const notDeceasedStatus: Partial<RootState> = {
      collectionForm: {
        [storeId]: {
          ...mockCollectionFormState[storeId],
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.NOT_DECEASED
          }
        }
      },
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false,
          deceasedInformation: {}
        }
      }
    };

    it('should return data', () => {
      const data = useSelectorMock(selectValueDeceasedPrimary, store, storeId);
      expect(data).toEqual({
        deceasedCounty: 'county',
        deceasedCardholderName: 'primaryName'
      });
    });

    it('should return empty object when status is not deceased', () => {
      const data = useSelectorMock(
        selectValueDeceasedPrimary,
        notDeceasedStatus,
        storeId
      );
      expect(data).toEqual({});
    });
  });

  describe('selectValueDeceasedSecond', () => {
    const store: Partial<RootState> = {
      collectionForm: {
        [storeId]: {
          ...mockCollectionFormState[storeId],
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.DECEASE_PENDING
          }
        }
      },
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false,
          deceasedInformation: {
            secondaryName: 'name',
            deceasedSecondaryInformation: {
              deceasedCountySecondary: 'county'
            }
          }
        }
      },
      mapping: {
        loading: false,
        data: {
          deceasedPartyPendingResponseSecond: {
            deceasedCardholderName: 'secondaryName',
            deceasedCounty: 'deceasedCountySecondary'
          }
        }
      }
    };
    const notDeceasedStatus: Partial<RootState> = {
      collectionForm: {
        [storeId]: {
          ...mockCollectionFormState[storeId],
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.NOT_DECEASED
          }
        }
      },
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: false,
          deceasedInformation: {}
        }
      }
    };

    it('should return data', () => {
      const data = useSelectorMock(selectValueDeceasedSecond, store, storeId);
      expect(data).toEqual({
        deceasedCounty: 'county',
        deceasedCardholderName: ''
      });
    });

    it('should return empty object when status is not deceased', () => {
      const data = useSelectorMock(
        selectValueDeceasedSecond,
        notDeceasedStatus,
        storeId
      );
      expect(data).toEqual({});
    });
  });

  describe('selectDeceasedSettlement', () => {
    const store: Partial<RootState> = {
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: true,
          deceasedSettlementInformation: {
            settlementAmount: '10'
          }
        }
      },
      collectionForm: {
        [storeId]: {
          ...mockCollectionFormState[storeId],
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.DECEASE_PENDING
          }
        }
      }
    };
    const emptyStore: Partial<RootState> = {
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: true
        }
      },
      collectionForm: {
        [storeId]: {
          ...mockCollectionFormState[storeId],
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.DECEASE_PENDING
          }
        }
      }
    };
    const emptyData: Partial<RootState> = {
      deceased: {
        [storeId]: {
          deceasedParty: DECEASED_PARTIES[0],
          loading: true
        }
      },
      collectionForm: {
        [storeId]: {
          ...mockCollectionFormState[storeId],
          lastFollowUpData: {
            callResultType: CALL_RESULT_CODE.NOT_DECEASED
          }
        }
      }
    };

    it('should return data', () => {
      const data = useSelectorMock(selectDeceasedSettlement, store, storeId);
      expect(data).toEqual({
        settlementAmount: '10'
      });
    });

    it('should return data', () => {
      const data = useSelectorMock(
        selectDeceasedSettlement,
        emptyData,
        storeId
      );
      expect(data).toEqual(null);
    });

    it('should return null', () => {
      const data = useSelectorMock(
        selectDeceasedSettlement,
        emptyStore,
        storeId
      );
      expect(data).toEqual(null);
    });
  });

  describe('selectDateNoticeReceivedPrimaryForm', () => {
    const store: Partial<RootState> = {
      form: {
        [`${storeId}_${PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`]: {
          registeredFields: [],
          values: {
            deceasedDateNoticeReceived: '2021/04/26'
          }
        }
      }
    };

    it('should return data', () => {
      const data = useSelectorMock(
        selectDateNoticeReceivedPrimaryForm,
        store,
        storeId
      );
      expect(data).toEqual('2021/04/26');
    });
  });

  describe('selectPrimaryInfo', () => {
    it('should return data', () => {
      const { data, emptyData } = selectorWrapper(primaryDeceasedPartyStore)(
        selectPrimaryInfo
      );
      expect(data).toEqual({ deceasedDatePrimary: '2021/03/25' });
      expect(emptyData).toBeNull();
    });
  });

  describe('selectPrimaryName', () => {
    it('should return data', () => {
      const { data, emptyData } = selectorWrapper(primaryDeceasedPartyStore)(
        selectPrimaryName
      );
      expect(data).toEqual('primaryName');
      expect(emptyData).toEqual('');
    });
  });

  describe('selectInsuranceIndicator', () => {
    it('should return data', () => {
      const { data, emptyData } = selectorWrapper(primaryDeceasedPartyStore)(
        selectInsuranceIndicator
      );
      expect(data).toEqual('0');
      expect(emptyData).toEqual('');
    });
  });

  describe('selectSecondaryInfo', () => {
    it('should return data', () => {
      const { data, emptyData } = selectorWrapper(secondaryDeceasedPartyStore)(
        selectSecondaryInfo
      );
      expect(data).toEqual({ deceasedDateSecondary: '2021/03/25' });
      expect(emptyData).toBeNull();
    });
  });

  describe('selectSecondaryName', () => {
    it('should return data', () => {
      const { data, emptyData } = selectorWrapper(secondaryDeceasedPartyStore)(
        selectSecondaryName
      );
      expect(data).toEqual('SecondaryName');
      expect(emptyData).toEqual('');
    });
  });

  describe('selectStateRef', () => {
    it('should return data', () => {
      const { data, emptyData } = selectorWrapper(primaryDeceasedPartyStore)(
        selectStateRef
      );

      expect(data).toEqual([{ value: 'TX', description: 'Texas' }]);
      expect(emptyData).toEqual([]);
    });
  });

  describe('selectUpdateDeceasedLoading', () => {
    it('should return data', () => {
      const { data, emptyData } = selectorWrapper(primaryDeceasedPartyStore)(
        selectUpdateDeceasedLoading
      );

      expect(data).toEqual(true);
      expect(emptyData).toEqual(false);
    });
  });

  // selectDeathCertificatesLoading,
  // selectedDeceasedResultTab

  describe('DEATH CERTIFICATE', () => {
    const storeHasData = {
      collectionForm: {
        [storeId]: {
          caseDocumentFiles: {
            deathCertificate: {
              loading: true,
              error: true,
              files: [{ id: 'id' }]
            }
          }
        } as any
      },
      deceased: {
        [storeId]: {
          selectedDeceasedResultTab: '01'
        } as any
      }
    };
    it('selectDeathCertificatesError', () => {
      const { data, emptyData } = selectorWrapper(storeHasData)(
        selectDeathCertificatesError
      );

      expect(data).toEqual(true);
      expect(emptyData).toEqual(undefined);
    });

    it('selectDeathCertificatesFiles', () => {
      const { data, emptyData } = selectorWrapper(storeHasData)(
        selectDeathCertificatesFiles
      );

      expect(data).toEqual([{ id: 'id' }]);
      expect(emptyData).toEqual(undefined);
    });

    it('selectDeathCertificatesLoading', () => {
      const { data, emptyData } = selectorWrapper(storeHasData)(
        selectDeathCertificatesLoading
      );

      expect(data).toEqual(true);
      expect(emptyData).toEqual(undefined);
    });
    it('selectedDeceasedResultTab ', () => {
      const { data, emptyData } = selectorWrapper(storeHasData)(
        selectedDeceasedResultTab
      );

      expect(data).toEqual('01');
      expect(emptyData).toEqual(undefined);
    });
  });

  describe('selectRawAccountDetail', () => {
    it('should return data', () => {
      const { data, emptyData } = selectorWrapper(primaryDeceasedPartyStore)(
        selectRawAccountDetail
      );

      expect(data).toEqual({
        creditLifeCode: {},
        customerName: 'primaryName'
      });
      expect(emptyData).toEqual(undefined);
    });
  });

  describe('selectCreditLifeCode ', () => {
    it('should return data', () => {
      const { data, emptyData } = selectorWrapper(primaryDeceasedPartyStore)(
        selectCreditLifeCode
      );

      expect(data).toEqual({});
      expect(emptyData).toEqual('');
    });

    it('should return default data', () => {
      const { data, emptyData } = selectorWrapper(secondaryDeceasedPartyStore)(
        selectCreditLifeCode
      );

      expect(data).toEqual('');
      expect(emptyData).toEqual('');
    });
  });

  describe('selectIsJointAcc', () => {
    it('should return data', () => {
      const { data, emptyData } = selectorWrapper(primaryDeceasedPartyStore)(
        selectIsJointAcc
      );

      expect(data).toEqual(true);
      expect(emptyData).toEqual(false);
    });
  });

  describe('selectGetDeceasedInfoLoading', () => {
    it('should return data', () => {
      const { data, emptyData } = selectorWrapper(primaryDeceasedPartyStore)(
        selectGetDeceasedInfoLoading
      );

      expect(data).toEqual(true);
      expect(emptyData).toEqual(false);
    });
  });

  describe('selectDeceasedPartyDescription', () => {
    it('should return data', () => {
      const { data, emptyData } = selectorWrapper(primaryDeceasedPartyStore)(
        selectDeceasedPartyDescription
      );

      expect(data).toEqual(DECEASED_PARTY_DESCRIPTION.PRIMARY);
      expect(emptyData).toEqual(undefined);
    });
  });

  describe('selectOriginDeceasedParty', () => {
    it('should return data', () => {
      const { data, emptyData } = selectorWrapper(primaryDeceasedPartyStore)(
        selectOriginDeceasedParty
      );

      expect(data).toEqual('name');
      expect(emptyData).toEqual('');
    });
  });
});
