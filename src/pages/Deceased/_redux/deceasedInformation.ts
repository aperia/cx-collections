import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled
} from '@reduxjs/toolkit';

// Types
import {
  DeceasedCommonRequestData,
  DeceasedInformationArgs,
  DeceasedInformationPayload,
  DeceasedInformationRequestData,
  DeceasedState
} from '../types';

// Services
import { deceasedService } from '../deceasedServices';

// Constants
import {
  DECEASED_PARTIES,
  DECEASED_PARTIES_OF_NOT_JOINT_ACC,
  DEFAULT_DECEASED_PARTY,
  ERR_CANNOT_PROCESS
} from '../constants';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';

// Helpers
import { prepareCommonData } from '../helpers';
import accountDetailService from 'pages/AccountDetails/accountDetailService';
import { CUSTOMER_ROLE_CODE } from 'app/constants';
import { deceasedActions } from './reducers';
import isEqual from 'lodash.isequal';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { dispatchDestroyDelayProgress } from 'pages/CollectionForm/helpers';

export const deceasedInformationRequest = createAsyncThunk<
  DeceasedInformationPayload,
  DeceasedInformationArgs,
  ThunkAPIConfig
>('deceased/deceasedInformationRequest', async (args, thunkAPI) => {
  const { storeId } = args;
  const states = thunkAPI.getState();
  const dispatch = thunkAPI.dispatch;
  const {
    inProgressInfo = {},
    isInProgress,
    lastFollowUpData
  } = states.collectionForm[storeId] || {};

  const eValueAccountId = states.accountDetail[storeId]?.data?.eValueAccountId;

  if (isInProgress) {
    const actionEntryResponse = await dispatch(
      collectionFormActions.getLatestActionRequest({
        storeId
      })
    );

    const callResultType =
      isFulfilled(actionEntryResponse) &&
      actionEntryResponse.payload.latestCallResultCode;

    const isCloseInlineMessage =
      isEqual(lastFollowUpData, inProgressInfo) ||
      isEqual(inProgressInfo, {
        callResultType
      });

    isCloseInlineMessage &&
      dispatch(
        collectionFormActions.toggleCallResultInProgressStatus({
          storeId,
          info: {}
        })
      );

    isCloseInlineMessage && dispatchDestroyDelayProgress(storeId);
  }

  // Check acc type
  // Joint Acc is having primary and secondary acc
  let isJointAcc = states.deceased[storeId]?.isJointAcc;
  if (isJointAcc === undefined) {
    const requestBody = {
      common: {
        accountId: eValueAccountId
      },
      selectFields: ['customerInquiry.customerRoleTypeCode']
    };
    const res = await accountDetailService.getAccountDetails(requestBody);
    const accTypes = res?.data?.customerInquiry?.reduce(
      (result, current) => {
        if (current.customerRoleTypeCode === CUSTOMER_ROLE_CODE.primary) {
          result.isHavingPrimary = true;
        }
        if (current.customerRoleTypeCode === CUSTOMER_ROLE_CODE.secondary) {
          result.isHavingSecondary = true;
        }

        return result;
      },
      {
        isHavingPrimary: false,
        isHavingSecondary: false
      }
    );
    isJointAcc = Boolean(
      accTypes?.isHavingPrimary && accTypes?.isHavingSecondary
    );
    dispatch(deceasedActions.setIsJointAcc({ storeId, isJointAcc }));
  }

  const commonData: DeceasedCommonRequestData = prepareCommonData(
    eValueAccountId!
  );
  const requestData: DeceasedInformationRequestData = {
    callingApplication: window.appConfig?.commonConfig?.callingApplication || ''
  };

  const { data } = await deceasedService.deceasedInformation(
    commonData,
    requestData
  );

  if (data?.statusCode === ERR_CANNOT_PROCESS) {
    return thunkAPI.rejectWithValue({
      errorMessage: data?.statusMessage
    });
  }

  if (lastFollowUpData?.callResultType === CALL_RESULT_CODE.NOT_DECEASED) {
    return {
      ...data,
      isNotDeceasedStatus: true,
      isJointAcc
    };
  }

  return { ...data, isJointAcc };
});

export const deceasedInformationBuilder = (
  builder: ActionReducerMapBuilder<DeceasedState>
) => {
  builder
    .addCase(deceasedInformationRequest.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: true
      };
    })
    .addCase(deceasedInformationRequest.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const deceasedPartyName =
        action?.payload?.deceasedInformation?.deceasedPartyName;

      let party;
      if (action?.payload?.isJointAcc) {
        party =
          deceasedPartyName &&
          DECEASED_PARTIES.find(
            item => item.value === deceasedPartyName.toLowerCase()
          );
      } else {
        party =
          deceasedPartyName &&
          DECEASED_PARTIES_OF_NOT_JOINT_ACC.find(
            item => item.value === deceasedPartyName.toLowerCase()
          );
      }
      if (action?.payload?.isNotDeceasedStatus) {
        party = DEFAULT_DECEASED_PARTY;
      }

      draftState[storeId] = {
        ...draftState[storeId],
        ...action.payload,
        deceasedParty: party,
        loading: false
      };
    })
    .addCase(deceasedInformationRequest.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        ...action.payload,
        loading: false
      };
    });
};
