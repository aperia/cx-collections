import {
  mockActionCreator,
  storeId,
  accEValue,
  createStoreWithDefaultMiddleWare,
  responseDefault
} from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { deceasedService } from '../deceasedServices';
import { deceasedActions } from './reducers';
import { triggerRemoveDeceasedStatus } from './removeDeceasedStatus';

export const successResponseDefault = {
  ...responseDefault,
  data: { messages: [{ status: 'success' }] }
};

let spy: jest.SpyInstance;
const deceasedActionsSpy = mockActionCreator(deceasedActions);
const toastSpy = mockActionCreator(actionsToast);

window.appConfig = {
  commonConfig: {
    operatorID: 'ACB'
  }
}

describe('removeDeceasedStatus', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('should be fulfilled', async () => {
    spy = jest
      .spyOn(deceasedService, 'deceasedCreateWorkflow')
      .mockResolvedValue({
        ...successResponseDefault
      });

    const store = createStoreWithDefaultMiddleWare({
      deceased: {
        [storeId]: {
          deceasedInformation: {}
        }
      },
      collectionForm: {
        [storeId]: {}
      }
    });

    const result = await triggerRemoveDeceasedStatus({
      storeId,
      accEValue,
      entryCode: 'ND'
    })(store.dispatch, store.getState, {});

    expect(result.type).toEqual(
      'deceased/triggerRemoveDeceasedStatusRequest/fulfilled'
    );
  });

  it('should be rejected', async () => {
    const store = createStoreWithDefaultMiddleWare({
      deceased: {
        [storeId]: {
          deceasedInformation: {}
        }
      },
      collectionForm: {
        [storeId]: {}
      }
    });

    deceasedActionsSpy(
      'deceasedCreateWorkflowRequest',
      jest.fn().mockRejectedValue({})
    );
    const addToastSpy = toastSpy('addToast');

    await triggerRemoveDeceasedStatus({
      storeId,
      accEValue,
      entryCode: 'ND'
    })(store.dispatch, store.getState, {});

    expect(addToastSpy).toBeCalled();
  });
});
