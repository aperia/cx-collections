import { createSelector } from '@reduxjs/toolkit';
// Helper
import { mappingDataFromObj } from 'app/helpers';
import isEmpty from 'lodash.isempty';
// Type
import {
  CommonRefData,
  RefStateData,
  REF_DATA_TYPE
} from 'pages/AccountSearch/Home/types';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { CollectionCurrentData } from 'pages/CollectionForm/types';
// Helper
import {
  DECEASED_PARTIES,
  DECEASED_PARTIES_OF_NOT_JOINT_ACC,
  DECEASED_PARTY_VALUE,
  DEFAULT_DECEASED_PARTY_PRIMARY_VALUE,
  PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW
} from '../constants';
import { IDeceasedPartyInfo, ReferenceData } from '../types';

const checkEmptyData = (data: MagicKeyValue) => {
  let count = 0,
    error = 0;
  for (const key in data) {
    count += 1;
    if (!data[key]) error += 1;
  }
  return count === error;
};

const emptyObject = {};

const getCallResultType = (state: RootState, storeId: string) =>
  state.collectionForm[storeId]?.lastFollowUpData?.callResultType || '';

const getPrimaryInformation = (state: RootState, storeId: string) =>
  state.deceased[storeId]?.deceasedInformation?.deceasedPrimaryInformation ||
  emptyObject;

const getSecondaryInformation = (state: RootState, storeId: string) =>
  state.deceased[storeId]?.deceasedInformation?.deceasedSecondaryInformation ||
  emptyObject;

// for primaryName and secondaryName get from AccountDetail as the following ticket https://jira.aperia.local/browse/CXC-4062
const getPrimaryCustomerName = (state: RootState, storeId: string) =>
  state.accountDetail?.[storeId]?.rawData?.customerName || '';

const getSecondaryCustomerName = (state: RootState, storeId: string) =>
  state.accountDetail?.[storeId]?.rawData?.secondaryName || '';

const getInsuranceIndicator = (state: RootState, storeId: string) =>
  state.deceased[storeId]?.deceasedInformation?.insuranceIndicator || '';

const getOriginDeceasedParty = (state: RootState, storeId: string) =>
  state.deceased[storeId]?.deceasedInformation?.deceasedPartyName;

const getDeceasedParty = (state: RootState, storeId: string) =>
  state.deceased[storeId]?.deceasedParty;

const getDeceasedSettlementInfo = (state: RootState, storeId: string) =>
  state.deceased[storeId]?.deceasedSettlementInformation;

const getDeceasedInfoLoading = (state: RootState, storeId: string) =>
  state.deceased[storeId]?.loading || false;

const getUpdateDeceasedInfoLoading = (state: RootState, storeId: string) =>
  state.deceased[storeId]?.updateDeceasedLoading || false;

const getDeceasedPartyPendingResponsePrimaryMapping = (
  state: RootState,
  storeId: string
) => state.mapping?.data?.deceasedPartyPendingResponsePrimary || {};

const getDeceasedPartyPendingResponseSecond = (
  state: RootState,
  storeId: string
) => state.mapping?.data?.deceasedPartyPendingResponseSecond || {};

const getStateRef = (state: RootState) =>
  state.refData[REF_DATA_TYPE.USA_STATE]?.data || [];

const getIsJointAcc = (state: RootState, storeId: string) =>
  state.deceased[storeId]?.isJointAcc || false;

const getCreditLifeCode = (state: RootState, storeId: string) =>
  state.accountDetail?.[storeId]?.rawData?.creditLifeCode || '';

const getRawAccountDetailData = (state: RootState, storeId: string) =>
  state.accountDetail?.[storeId]?.rawData;

export const selectCreditLifeCode = createSelector(
  getCreditLifeCode,
  (data: string) => data
);

export const selectIsJointAcc = createSelector(
  getIsJointAcc,
  (data: boolean) => data
);

export const selectStateRef = createSelector(
  getStateRef,
  (data: Array<RefStateData | CommonRefData | undefined>) => data
);

export const selectValueDeceasedParty = createSelector(
  getDeceasedParty,
  data => data?.value || DEFAULT_DECEASED_PARTY_PRIMARY_VALUE
);

export const selectOriginDeceasedParty = createSelector(
  getOriginDeceasedParty,
  data => data || ''
);

export const selectIsShowPrimaryMainInfo = createSelector(
  getDeceasedParty,
  data => {
    const deceasedPartyValue = data?.value;
    return (
      deceasedPartyValue === DECEASED_PARTY_VALUE.PRIMARY ||
      deceasedPartyValue === DECEASED_PARTY_VALUE.BOTH
    );
  }
);

export const selectIsShowSecondaryMainInfo = createSelector(
  getDeceasedParty,
  data => {
    const deceasedPartyValue = data?.value;

    return (
      deceasedPartyValue === DECEASED_PARTY_VALUE.SECONDARY ||
      deceasedPartyValue === DECEASED_PARTY_VALUE.BOTH
    );
  }
);

export const selectIsShowSettlementInfo = createSelector(
  getCallResultType,
  data => {
    if (!data) return false;
    return data !== CALL_RESULT_CODE.DECEASE_PENDING;
  }
);

export const selectPrimaryMainInfo = createSelector(
  getPrimaryInformation,
  data => {
    if (checkEmptyData(data)) return null;
    return data;
  }
);

export const selectSecondaryMainInfo = createSelector(
  getSecondaryInformation,
  data => {
    if (checkEmptyData(data)) return null;
    return data;
  }
);

export const selectSettlementInfo = createSelector(
  getDeceasedSettlementInfo,
  data => {
    const info = data || {};
    if (checkEmptyData(info)) return null;
    return data;
  }
);

export const selectPrimaryInfo = createSelector(getPrimaryInformation, data => {
  if (checkEmptyData(data)) return null;
  return data;
});

export const selectPrimaryName = createSelector(
  getPrimaryCustomerName,
  data => data
);

export const selectInsuranceIndicator = createSelector(
  getInsuranceIndicator,
  data => data
);

export const selectPrimaryPartyInfo = createSelector(
  [getPrimaryInformation, getPrimaryCustomerName, getInsuranceIndicator],
  (
    data?: object,
    primaryCustomerName?: string,
    insuranceIndicator?: string
  ) => {
    return {
      ...data,
      primaryCustomerName,
      insuranceIndicator
    };
  }
);

export const selectSecondaryInfo = createSelector(
  getSecondaryInformation,
  data => {
    if (checkEmptyData(data)) return null;
    return data;
  }
);

export const selectSecondaryName = createSelector(
  getSecondaryCustomerName,
  data => data
);

export const selectSecondaryPartyInfo = createSelector(
  [getSecondaryInformation, getSecondaryCustomerName, getInsuranceIndicator],
  (data, secondaryName?: string, insuranceIndicator?: string) => {
    return {
      ...data,
      secondaryName,
      insuranceIndicator
    };
  }
);

export const selectDeceasedParty = createSelector(
  getDeceasedParty,
  getIsJointAcc,
  (data?: ReferenceData, isJointAcc?: boolean) => {
    const [defaultParty] = isJointAcc
      ? DECEASED_PARTIES
      : DECEASED_PARTIES_OF_NOT_JOINT_ACC;
    return isEmpty(data) ? defaultParty : data;
  }
);

export const selectDeceasedPartyDescription = createSelector(
  getDeceasedParty,
  data => data?.description
);

export const selectValueDeceasedMainPrimary = createSelector(
  [getPrimaryInformation, getCallResultType],
  (data: CollectionCurrentData, type: string) => {
    if (type === CALL_RESULT_CODE.NOT_DECEASED) return null;

    return {
      deceasedDateNoticeReceived: data?.deceasedDateNoticeReceivedPrimary || '',
      deceasedDateOfDeath: data?.deceasedDatePrimary || undefined
    };
  }
);

export const selectValueDeceasedMainSecond = createSelector(
  [getSecondaryInformation, getCallResultType],
  (data: CollectionCurrentData, type: string) => {
    if (type === CALL_RESULT_CODE.NOT_DECEASED) return null;

    return {
      deceasedDateNoticeReceived: data?.deceasedDateNoticeReceivedSecondary,
      deceasedDateOfDeath: data?.deceasedDateSecondary
    };
  }
);

export const selectValueDeceasedPrimary = createSelector(
  [
    getPrimaryInformation,
    getPrimaryCustomerName,
    getInsuranceIndicator,
    getCallResultType,
    getDeceasedPartyPendingResponsePrimaryMapping
  ],
  (
    data: CollectionCurrentData,
    primaryCustomerName: string,
    insuranceIndicator: string,
    type: string,
    deceasedPartyPendingResponsePrimary
  ) => {
    let dataReturn: Partial<IDeceasedPartyInfo> = {};

    if (type === CALL_RESULT_CODE.NOT_DECEASED) {
      dataReturn = mappingDataFromObj(
        { primaryCustomerName, insuranceIndicator },
        deceasedPartyPendingResponsePrimary
      );
      return dataReturn;
    }

    dataReturn = mappingDataFromObj(
      { ...data, primaryCustomerName, insuranceIndicator },
      deceasedPartyPendingResponsePrimary
    );

    return dataReturn;
  }
);

export const selectValueDeceasedSecond = createSelector(
  [
    getSecondaryInformation,
    getSecondaryCustomerName,
    getInsuranceIndicator,
    getCallResultType,
    getDeceasedPartyPendingResponseSecond
  ],
  (
    data: CollectionCurrentData,
    secondaryName: string,
    insuranceIndicator: string,
    type: string,
    deceasedPartyPendingResponseSecond
  ) => {
    let dataReturn: Partial<IDeceasedPartyInfo> = {};

    if (type === CALL_RESULT_CODE.NOT_DECEASED) {
      dataReturn = mappingDataFromObj(
        { secondaryName, insuranceIndicator },
        deceasedPartyPendingResponseSecond
      );
      return dataReturn;
    }

    dataReturn = mappingDataFromObj(
      { ...data, secondaryName, insuranceIndicator },
      deceasedPartyPendingResponseSecond
    );

    return dataReturn;
  }
);

export const selectGetDeceasedInfoLoading = createSelector(
  getDeceasedInfoLoading,
  (data: boolean) => data
);
export const selectUpdateDeceasedLoading = createSelector(
  getUpdateDeceasedInfoLoading,
  (data: boolean) => data
);

export const selectDeceasedSettlement = createSelector(
  getDeceasedSettlementInfo,
  getCallResultType,
  (data, type) => {
    if (type === CALL_RESULT_CODE.NOT_DECEASED) return null;
    return data || null;
  }
);

export const selectDateNoticeReceivedPrimaryForm = createSelector(
  (state: RootState, storeId: string) =>
    state.form[`${storeId}_${PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`]
      ?.values?.deceasedDateNoticeReceived,
  data => data
);

export const selectRawAccountDetail = createSelector(
  getRawAccountDetailData,
  data => data
);

// ------------------------DEATH CERTIFICATE------------------------------//

export const getDeathCertificateDocuments = (
  state: RootState,
  storeId: string
) => state.collectionForm[storeId]?.caseDocumentFiles?.deathCertificate;

export const getSelectedDeceasedResultTab = (
  state: RootState,
  storeId: string
) => state.deceased[storeId]?.selectedDeceasedResultTab;

export const selectDeathCertificatesFiles = createSelector(
  getDeathCertificateDocuments,
  data => data?.files
);
export const selectDeathCertificatesLoading = createSelector(
  getDeathCertificateDocuments,
  data => data?.loading
);

export const selectDeathCertificatesError = createSelector(
  getDeathCertificateDocuments,
  data => data?.error
);
export const selectedDeceasedResultTab = createSelector(
  getSelectedDeceasedResultTab,
  tabIndex => tabIndex
);
