import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import React from 'react';
import DeceasedInfoHeader from './DeceasedInfoHeader';
import { screen } from '@testing-library/react';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';

const collectionFormActionsSpy = mockActionCreator(collectionFormActions);
describe('should render', () => {
  it('should render', () => {
    renderMockStoreId(<DeceasedInfoHeader />);
  });
  it('actions', () => {
    renderMockStoreId(<DeceasedInfoHeader />);
    const mockActionTriggerEditCollectionForm = collectionFormActionsSpy(
      'triggerEditCollectionForm'
    );
    const mockActionChangeMethod = collectionFormActionsSpy('changeMethod');

    const btn = screen.getByRole('button');
    btn.click();

    expect(mockActionTriggerEditCollectionForm).toBeCalledWith({ storeId });
    expect(mockActionChangeMethod).toBeCalledWith({
      storeId,
      method: COLLECTION_METHOD.EDIT
    });
  });
});
