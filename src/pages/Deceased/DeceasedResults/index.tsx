import React from 'react';
import DeceasedInfo from './DeceasedInfo';
import Attachment from './DeathCertificatesResult';
import { HorizontalTabs, Icon, Tooltip } from 'app/_libraries/_dls/components';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { selectedDeceasedResultTab } from '../_redux/selectors';
import { DeceasedResultTab } from '../types';
import { deceasedActions } from '../_redux/reducers';
import { useDispatch } from 'react-redux';
import { selectViewDeathCertificate } from 'pages/CollectionForm/_redux/selectors';
import { useTranslation } from 'app/_libraries/_dls/hooks';

const DeceasedResult = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const handleOnSelect = (eventKey: any) => {
    dispatch(
      deceasedActions.updateSelectedDeceasedResultTab({
        storeId,
        tabKey: eventKey as DeceasedResultTab
      })
    );
  };

  const selectedTab = useStoreIdSelector<DeceasedResultTab>(
    selectedDeceasedResultTab
  );

  const viewDeathCertificateMode = useStoreIdSelector<boolean>(
    selectViewDeathCertificate
  );

  if (!viewDeathCertificateMode) {
    return <DeceasedInfo />;
  }
  return (
    <div>
      <HorizontalTabs
        level2
        unmountOnExit={false}
        onSelect={handleOnSelect}
        activeKey={selectedTab}
        dataTestId="deceasedResultTabs"
      >
        <HorizontalTabs.Tab
          title={
            <Tooltip
              element={t('txt_main_information')}
              placement="top"
              variant="primary"
            >
              <Icon name="file" size="6x" />
            </Tooltip>
          }
          key={DeceasedResultTab.DeceasedInfo}
          eventKey={DeceasedResultTab.DeceasedInfo}
        >
          <DeceasedInfo viewDeathCertificateMode />
        </HorizontalTabs.Tab>
        <HorizontalTabs.Tab
          title={
            <Tooltip
              element={t('txt_death_certificate')}
              placement="top"
              variant="primary"
            >
              <Icon name="attachment" size="6x" />
            </Tooltip>
          }
          key={DeceasedResultTab.DeathCertificate}
          eventKey={DeceasedResultTab.DeathCertificate}
        >
          <Attachment />
        </HorizontalTabs.Tab>
      </HorizontalTabs>
    </div>
  );
};

export default DeceasedResult;
