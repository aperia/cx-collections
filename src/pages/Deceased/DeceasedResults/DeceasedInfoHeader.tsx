import React from 'react';

// Components
import { Tooltip, Button, Icon } from 'app/_libraries/_dls/components';

// Hooks
import { useDispatch } from 'react-redux';
import { useAccountDetail } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// i18n/constant
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { genAmtId } from 'app/_libraries/_dls/utils';

const CallResultHeader: React.FC = () => {
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();

  const handleEditButton = () => {
    dispatch(collectionFormActions.triggerEditCollectionForm({ storeId }));
    dispatch(
      collectionFormActions.changeMethod({
        storeId,
        method: COLLECTION_METHOD.EDIT
      })
    );
  };

  return (
    <div className="d-flex justify-content-between align-items-center my-16">
      <p
        className=" fs-14 fw-500 color-grey"
        data-testid={genAmtId('deceasedInfoHeader', 'main-information', '')}
      >
        {t('txt_main_information')}
      </p>
      <Tooltip
        element={t(I18N_COLLECTION_FORM.EDIT_INFORMATION)}
        placement="top"
        variant="primary"
        triggerClassName="mr-n4"
        dataTestId="deceasedInfoHeader_edit"
      >
        <Button
          variant="icon-secondary"
          onClick={handleEditButton}
          dataTestId="deceasedInfoHeader_edit-btn"
        >
          <Icon name="edit" />
        </Button>
      </Tooltip>
    </div>
  );
};

export default CallResultHeader;
