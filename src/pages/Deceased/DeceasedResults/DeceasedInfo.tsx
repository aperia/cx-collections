// components
import { ViewWrapper } from 'app/components';
// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import classnames from 'classnames';
import { getConfigReferenceData } from 'pages/CollectionForm/CollectionActivities/GeneralInformation/_redux/selectors';
import {
  useDelayProgress,
  useTriggerDelayProgress
} from 'pages/CollectionForm/hooks/useDelayProgress';
import React, { useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import DataSection from '../../__commons/DataSection';
import useParseInsuranceIndicator from '../hooks/useParseInsuranceIndicator';
import useSetDeceaseInfoData from '../hooks/useSetDeceaseInfoData';
// helpers
import {
  PrimaryDeceasedPartyResult,
  SecondaryDeceasedPartyResult
} from '../types';
// redux
import { deceasedActions } from '../_redux/reducers';
import {
  selectCreditLifeCode,
  selectDeceasedPartyDescription,
  selectGetDeceasedInfoLoading,
  selectIsShowPrimaryMainInfo,
  selectIsShowSecondaryMainInfo,
  selectIsShowSettlementInfo,
  selectPrimaryInfo,
  selectPrimaryName,
  selectSecondaryInfo,
  selectSecondaryName,
  selectSettlementInfo,
  selectStateRef
} from '../_redux/selectors';
import DeceasedHeader from './DeceasedInfoHeader';

interface DeceasedCallResultsProp {
  viewDeathCertificateMode?: boolean;
}

const DeceasedCallResults: React.FC<DeceasedCallResultsProp> = ({
  viewDeathCertificateMode
}) => {
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();

  const loading = useStoreIdSelector<boolean>(selectGetDeceasedInfoLoading);
  const primaryName = useStoreIdSelector<string>(selectPrimaryName);
  const secondaryName = useStoreIdSelector<string>(selectSecondaryName);
  const settlementInfo = useStoreIdSelector<object>(selectSettlementInfo);
  const stateRef = useSelector(selectStateRef);

  const deceasedPartyDescription = useStoreIdSelector<string>(
    selectDeceasedPartyDescription
  );

  const isShowPrimaryMainInfo = useStoreIdSelector<boolean>(
    selectIsShowPrimaryMainInfo
  );

  const isShowSecondaryMainInfo = useStoreIdSelector<boolean>(
    selectIsShowSecondaryMainInfo
  );

  const isShowSettlementInfo = useStoreIdSelector<boolean>(
    selectIsShowSettlementInfo
  );

  const { deceasedContact } = useStoreIdSelector<any>(getConfigReferenceData);

  const primaryPartyInfo =
    useStoreIdSelector<PrimaryDeceasedPartyResult>(selectPrimaryInfo);

  const secondaryPartyInfo =
    useStoreIdSelector<SecondaryDeceasedPartyResult>(selectSecondaryInfo);

  const creditLifeCode = useStoreIdSelector<string>(selectCreditLifeCode);
  const insuranceIndicator = useParseInsuranceIndicator(creditLifeCode);

  const deceasedContactPrimary = useSetDeceaseInfoData({
    selectedValue: primaryPartyInfo?.deceasedContactPrimary,
    deceasedContact
  });

  const deceasedContactSecondary = useSetDeceaseInfoData({
    selectedValue: secondaryPartyInfo?.deceasedContactSecondary,
    deceasedContact
  });

  const deceasedContactStatePrimary = useMemo(() => {
    if (primaryPartyInfo?.deceasedContactStatePrimary) {
      const deceasedContactState = stateRef.find(
        item => item?.value === primaryPartyInfo?.deceasedContactStatePrimary
      );
      return `${deceasedContactState?.value} - ${deceasedContactState?.description}`;
    }
    return '';
  }, [primaryPartyInfo?.deceasedContactStatePrimary, stateRef]);

  const deceasedStatePrimary = useMemo(() => {
    if (primaryPartyInfo?.deceasedStatePrimary) {
      const deceasedState = stateRef.find(
        item => item?.value === primaryPartyInfo?.deceasedStatePrimary
      );
      return `${deceasedState?.value} - ${deceasedState?.description}`;
    }
    return '';
  }, [primaryPartyInfo?.deceasedStatePrimary, stateRef]);

  const deceasedContactStateSecondary = useMemo(() => {
    if (secondaryPartyInfo?.deceasedContactStateSecondary) {
      const deceasedContactState = stateRef.find(
        item =>
          item?.value === secondaryPartyInfo?.deceasedContactStateSecondary
      );
      return `${deceasedContactState?.value} - ${deceasedContactState?.description}`;
    }
    return '';
  }, [secondaryPartyInfo?.deceasedContactStateSecondary, stateRef]);

  const deceasedStateSecondary = useMemo(() => {
    if (secondaryPartyInfo?.deceasedStateSecondary) {
      const deceasedState = stateRef.find(
        item => item?.value === secondaryPartyInfo?.deceasedStateSecondary
      );
      return `${deceasedState?.value} - ${deceasedState?.description}`;
    }
    return '';
  }, [secondaryPartyInfo?.deceasedStateSecondary, stateRef]);

  useDelayProgress();
  useTriggerDelayProgress();

  useEffect(() => {
    dispatch(deceasedActions.deceasedInformationRequest({ storeId }));
  }, [storeId, dispatch]);

  return (
    <div className={classnames({ loading })}>
      {viewDeathCertificateMode && <DeceasedHeader />}
      <div className="mt-n16">
        <ViewWrapper
          id={`${storeId}-deceasedPartyResultView`}
          formKey={`${storeId}-deceasedPartyResultView`}
          descriptor="deceasedPartyResultView"
          value={{ deceasedParty: deceasedPartyDescription }}
        />
      </div>

      <div className="mt-16">
        {isShowPrimaryMainInfo ? (
          <>
            <div
              className={classnames({
                'mt-24': isShowPrimaryMainInfo
              })}
            >
              <div
                className={classnames(
                  isShowPrimaryMainInfo && 'divider-dashed'
                )}
              />
              <div
                className={classnames({
                  'mt-24': isShowPrimaryMainInfo
                })}
              >
                <DataSection
                  dataTestId="deceasedPrimaryPartyInfo"
                  label="txt_primary_deceased_party"
                  isNoData={!primaryPartyInfo}
                  component={
                    <ViewWrapper
                      id={`${storeId}-deceasedPrimaryPartyInfo`}
                      formKey={`${storeId}-deceasedPrimaryPartyInfo`}
                      descriptor="deceasedPrimaryPartyInfo"
                      value={{
                        ...primaryPartyInfo,
                        primaryCustomerName: primaryName,
                        insuranceIndicator,
                        deceasedStatePrimary,
                        deceasedContactStatePrimary,
                        deceasedContactPrimary
                      }}
                    />
                  }
                />
              </div>
            </div>
          </>
        ) : null}

        {isShowSecondaryMainInfo ? (
          <>
            <div
              className={classnames({
                'mt-24': isShowSecondaryMainInfo
              })}
            >
              <div
                className={classnames(
                  isShowSecondaryMainInfo && 'divider-dashed'
                )}
              />
              <div
                className={classnames({
                  'mt-24': isShowSecondaryMainInfo
                })}
              >
                <DataSection
                  dataTestId="deceasedSecondaryPartyInfo"
                  label="txt_secondary_deceased_party"
                  isNoData={!secondaryPartyInfo}
                  component={
                    <ViewWrapper
                      id={`${storeId}-deceasedSecondaryPartyInfo`}
                      formKey={`${storeId}-deceasedSecondaryPartyInfo`}
                      descriptor="deceasedSecondaryPartyInfo"
                      value={{
                        ...secondaryPartyInfo,
                        secondaryName,
                        insuranceIndicator,
                        deceasedStateSecondary,
                        deceasedContactStateSecondary,
                        deceasedContactSecondary
                      }}
                    />
                  }
                />
              </div>
            </div>
          </>
        ) : null}

        {isShowSettlementInfo ? (
          <>
            <div
              className={classnames({
                'mt-24': isShowSecondaryMainInfo || isShowPrimaryMainInfo
              })}
            >
              <div
                className={classnames({
                  'divider-dashed':
                    isShowPrimaryMainInfo || isShowSecondaryMainInfo
                })}
              />
              <div
                className={classnames({
                  'mt-24': isShowPrimaryMainInfo || isShowSecondaryMainInfo
                })}
              >
                <DataSection
                  dataTestId="deceasedSettlementInfo"
                  label="txt_settlement_information"
                  isNoData={!settlementInfo}
                  component={
                    <ViewWrapper
                      id={`${storeId}-deceasedSettlementInfo`}
                      formKey={`${storeId}-deceasedSettlementInfo`}
                      descriptor="deceasedSettlementInfo"
                      value={settlementInfo}
                    />
                  }
                />
              </div>
            </div>
          </>
        ) : null}
      </div>
    </div>
  );
};

export default DeceasedCallResults;
