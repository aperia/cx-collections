import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import {
  selectDeathCertificatesFiles,
  selectDeathCertificatesLoading
} from '../_redux/selectors';
import classnames from 'classnames';
import DocumentList, { FileDetail } from 'app/components/FileList';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { Button, Icon, Tooltip } from 'app/_libraries/_dls/components';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface DeathCertificatesResultsProps {}

const DeathCertificatesResults: React.FC<DeathCertificatesResultsProps> =
  ({}) => {
    const dispatch = useDispatch();
    const { storeId } = useAccountDetail();
    const { t } = useTranslation();

    const deathCertificates = useStoreIdSelector<FileDetail[]>(
      selectDeathCertificatesFiles
    );

    const loading = useStoreIdSelector<boolean>(selectDeathCertificatesLoading);

    const handleDownloadFile = (file: FileDetail) => {
      dispatch(
        collectionFormActions.downloadCaseDocumentFile({
          storeId,
          postData: {
            fileName: file?.documentName,
            uuid: file?.documentObjectId
          }
        })
      );
    };

    const handleUpLoadFile = () => {
      dispatch(
        collectionFormActions.openUploadFileModal({
          storeId,
          type: 'deathCertificate'
        })
      );
    };

    useEffect(() => {
      dispatch(
        collectionFormActions.getListCaseDocument({
          storeId,
          type: 'deathCertificate'
        })
      );
    }, [dispatch, storeId]);

    return (
      <div className={classnames('dls-upload mt-16', { loading })}>
        <div className="d-flex justify-content-between align-items-center my-16">
          <p
            className=" fs-14 fw-500 color-grey"
            data-testid={genAmtId('deathCertificates', 'title', '')}
          >
            {t(I18N_COLLECTION_FORM.DEATH_CERTIFICATE)}
          </p>
          <Tooltip
            element={t(I18N_COLLECTION_FORM.UPLOAD_DEATH_CERTIFICATE)}
            placement="top"
            variant="primary"
            triggerClassName="mr-n4"
            dataTestId={genAmtId('deathCertificates', 'upload-file', '')}
          >
            <Button
              variant="icon-secondary"
              onClick={handleUpLoadFile}
              dataTestId={genAmtId('deathCertificates', 'upload-file-btn', '')}
            >
              <Icon name="add-file" />
            </Button>
          </Tooltip>
        </div>
        <DocumentList
          dataTestId="deathCertificates"
          files={deathCertificates}
          onDownload={handleDownloadFile}
        />
      </div>
    );
  };

export default DeathCertificatesResults;
