import React from 'react';

// RTL
import { screen } from '@testing-library/react';

// helpers
import { renderMockStoreId, storeId } from 'app/test-utils';

// components
import DeceasedCallResults from './index';

// constants
import { DECEASED_PARTIES } from '../constants';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import userEvent from '@testing-library/user-event';
import { queryByClass } from 'app/_libraries/_dls/test-utils';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

describe('Test DeceasedCallResults component', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isCollector: true
    }
  };
  it('render component', () => {
    renderMockStoreId(<DeceasedCallResults />);

    expect(screen.getByTestId('deceasedPartyResultView')).toBeInTheDocument();
  });

  it('show primary info', () => {
    renderMockStoreId(<DeceasedCallResults />, {
      initialState: {
        refData: {
          states: {
            data: [
              {
                value: 'TX',
                description: 'Texas'
              }
            ]
          }
        },
        deceased: {
          [storeId]: {
            isJointAcc: true,
            deceasedParty: DECEASED_PARTIES[0],
            deceasedInformation: {
              deceasedPrimaryInformation: {
                deceasedDatePrimary: '07-01-2021',
                deceasedDateNoticeReceivedPrimary: '07-02-2021',
                deceasedContactStatePrimary: 'TX',
                deceasedStatePrimary: 'TX'
              }
            }
          }
        }
      }
    });

    expect(screen.getByTestId('deceasedPartyResultView')).toBeInTheDocument();
    expect(screen.getByTestId('deceasedPrimaryPartyInfo')).toBeInTheDocument();
  });

  it('show secondary info', () => {
    renderMockStoreId(<DeceasedCallResults />, {
      initialState: {
        refData: {
          states: {
            data: [
              {
                value: 'TX',
                description: 'Texas'
              }
            ]
          }
        },
        deceased: {
          [storeId]: {
            isJointAcc: true,
            deceasedParty: DECEASED_PARTIES[1],
            deceasedInformation: {
              deceasedSecondaryInformation: {
                deceasedDateSecondary: '07-01-2021',
                deceasedDateNoticeReceivedSecondary: '07-02-2021',
                deceasedContactStateSecondary: 'TX',
                deceasedStateSecondary: 'TX'
              }
            }
          }
        }
      }
    });

    expect(screen.getByTestId('deceasedPartyResultView')).toBeInTheDocument();
    expect(
      screen.getByTestId('deceasedSecondaryPartyInfo')
    ).toBeInTheDocument();
  });

  it('show settlement info', () => {
    renderMockStoreId(<DeceasedCallResults />, {
      initialState: {
        collectionForm: {
          [storeId]: {
            lastFollowUpData: {
              callResultType: CALL_RESULT_CODE.DECEASE
            }
          } as any
        }
      }
    });

    expect(screen.getByTestId('deceasedPartyResultView')).toBeInTheDocument();
    expect(screen.getByText('txt_settlement_information')).toBeInTheDocument();
  });

  it('show settlement info', () => {
    renderMockStoreId(<DeceasedCallResults />, {
      initialState: {
        collectionForm: {
          [storeId]: {
            lastFollowUpData: {
              callResultType: CALL_RESULT_CODE.DECEASE
            }
          } as any
        }
      }
    });

    expect(screen.getByTestId('deceasedPartyResultView')).toBeInTheDocument();
    expect(screen.getByText('txt_settlement_information')).toBeInTheDocument();
  });
  it('change tab', () => {
    const { wrapper } = renderMockStoreId(<DeceasedCallResults />, {
      initialState: {
        collectionForm: {
          [storeId]: {
            lastFollowUpData: {
              callResultType: CALL_RESULT_CODE.DECEASE
            }
          } as any
        }
      }
    });

    userEvent.click(queryByClass(wrapper.container, /icon icon-attachment/)!);
    expect(screen.queryByText('txt_main_information')).toBeInTheDocument();
  });
});
