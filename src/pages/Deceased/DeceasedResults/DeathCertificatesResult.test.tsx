import React from 'react';

// RTL
import { screen } from '@testing-library/react';

// helpers
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';

// components
import DeathCertificatesResults from './DeathCertificatesResult';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import userEvent from '@testing-library/user-event';

const collectionFormActionsMock = mockActionCreator(collectionFormActions);

const renderComponent = () => {
  return renderMockStoreId(<DeathCertificatesResults />, {
    initialState: {
      collectionForm: {
        [storeId]: {
          caseDocumentFiles: {
            deathCertificate: {
              files: [
                {
                  documentName: 'searchAccount.png',
                  documentObjectId: '123-456-789'
                }
              ]
            }
          }
        } as any
      }
    }
  });
};

describe('Test DeathCertificatesResults component', () => {
  it('render component', () => {
    const getListCaseDocument = collectionFormActionsMock(
      'getListCaseDocument'
    );
    renderComponent();

    expect(getListCaseDocument).toHaveBeenCalled();
    expect(screen.queryByText('searchAccount.png')).toBeInTheDocument();
  });

  it('handleUpLoadFile', () => {
    const openUploadFileModal = collectionFormActionsMock(
      'openUploadFileModal'
    );
    const getListCaseDocument = collectionFormActionsMock(
      'getListCaseDocument'
    );
    const { wrapper } = renderComponent();

    userEvent.click(queryByClass(wrapper.container, /icon icon-add-file/)!);
    expect(getListCaseDocument).toHaveBeenCalled();
    expect(openUploadFileModal).toHaveBeenCalled();
  });

  it('downloadCaseDocumentFile', () => {
    const downloadCaseDocumentFile = collectionFormActionsMock(
      'downloadCaseDocumentFile'
    );
    const getListCaseDocument = collectionFormActionsMock(
      'getListCaseDocument'
    );
    const { wrapper } = renderComponent();

    userEvent.click(queryByClass(wrapper.container, /icon icon-download/)!);
    expect(getListCaseDocument).toHaveBeenCalled();
    expect(downloadCaseDocumentFile).toHaveBeenCalled();
  });
});
