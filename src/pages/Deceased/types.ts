export interface Action {
  description: string;
  value: 'primary' | 'secondary' | 'both';
}

export interface CommonPayload {
  common: {
    accountId: string;
  };
}

export interface DeceasedState {
  [storeId: string]: DeceasedData;
}

export interface DeceasedData extends MagicKeyValue {
  deceasedParty?: ReferenceData;
  loading?: boolean;
  deceasedInformation?: DeceasedInformation;
  deceasedSettlementInformation?: DeceasedSettlement;
  isJointAcc?: boolean;
  selectedDeceasedResultTab?: DeceasedResultTab;
}

export interface DeceasedInformation extends MagicKeyValue {
  deceasedConfirmation?: string;
  deceasedPartyName?: string;
  insuranceIndicator?: string;
  primaryCustomerName?: string;
  secondaryName?: string;
  deceasedPrimaryInformation?: {
    deceasedCityPrimary?: string;
    deceasedContactAddressPrimary?: string;
    deceasedContactNamePrimary?: string;
    deceasedContactPhoneNumberPrimary?: string;
    deceasedContactPrimary?: string;
    deceasedContactRelationshipPrimary?: string;
    deceasedCountyPrimary?: string;
    deceasedDateNoticeReceivedPrimary?: string;
    deceasedDatePrimary?: string;
    deceasedFaxPrimary?: string;
    deceasedStatePrimary?: string;
    deceasedZipPrimary?: string;
    deceasedContactStatePrimary?: string;
  };
  deceasedSecondaryInformation?: {
    deceasedCitySecondary?: string;
    deceasedContactAddressSecondary?: string;
    deceasedContactNameSecondary?: string;
    deceasedContactPhoneNumberSecondary?: string;
    deceasedContactRelationshipSecondary?: string;
    deceasedContactSecondary?: string;
    deceasedCountySecondary?: string;
    deceasedDateNoticeReceivedSecondary?: string;
    deceasedDateSecondary?: string;
    deceasedFaxSecondary?: string;
    deceasedStateSecondary?: string;
    deceasedZipSecondary?: string;
    deceasedContactStateSecondary?: string;
  };
}

export interface PrimaryDeceasedPartyResult {
  deceasedCityPrimary?: string;
  deceasedContactAddressPrimary?: string;
  deceasedContactNamePrimary?: string;
  deceasedContactPhoneNumberPrimary?: string;
  deceasedContactPrimary?: string;
  deceasedContactRelationshipPrimary?: string;
  deceasedContactStatePrimary?: string;
  deceasedCountyPrimary?: string;
  deceasedDateNoticeReceivedPrimary?: string;
  deceasedDatePrimary?: string;
  deceasedFaxPrimary?: string;
  deceasedStatePrimary?: string;
  deceasedZipPrimary?: string;
  insuranceIndicator?: string;
  primaryCustomerName?: string;
}

export interface SecondaryDeceasedPartyResult {
  deceasedCitySecondary?: string;
  deceasedContactAddressSecondary?: string;
  deceasedContactNameSecondary?: string;
  deceasedContactPhoneNumberSecondary?: string;
  deceasedContactRelationshipSecondary?: string;
  deceasedContactSecondary?: string;
  deceasedContactStateSecondary?: string;
  deceasedCountySecondary?: string;
  deceasedDateNoticeReceivedSecondary?: string;
  deceasedDateSecondary?: string;
  deceasedFaxSecondary?: string;
  deceasedStateSecondary?: string;
  deceasedZipSecondary?: string;
  insuranceIndicator?: string;
  secondaryName?: string;
}

export interface DeceasedSettlement {
  settlementAmount?: string;
  settlementDate?: string;
  settlementPaymentMethod?: string;
}

export interface ReferenceData {
  value?: string;
  description?: string;
}

export interface DeceasedUpdateDataAction extends StoreIdPayload {
  data: MagicKeyValue;
}

export interface RemoveDeceasedStatusArgs extends StoreIdPayload {
  entryCode: string;
}

export interface RemoveDeceasedStatusPayload {}

export interface IDeceased {
  deceasedParty?: ReferenceData;
}

export interface IPayLoadChangeDeceased extends StoreIdPayload, IDeceased {}

export interface DeceasedUpdatePayload {}

export interface SetIsJointAccPayload extends StoreIdPayload {
  isJointAcc: boolean;
}

export interface DeceasedResults {
  callResultType?: string;
  deceasedParty?: {
    descriptions?: string;
    value?: string;
  };
  data?: {
    primaryMainInfo?: {
      dateOfDeath?: string;
      dateNoticeReceived?: string;
    };
    secondaryMainInfo?: {
      dateOfDeath?: string;
      dateNoticeReceived?: string;
    };
    settlementInfo?: {
      amount?: string;
      method?: string;
      date?: string;
    };
    primaryPartyInfo?: {
      cardholderName?: string;
      deceasedState?: string;
      deceasedCounty?: string;
      insuranceIndicator?: string;
      contact?: string;
      relationship?: string;
      contactName?: string;
      contactPhone?: string;
      address?: string;
      city?: string;
      state?: string;
      zip?: string;
      fax?: string;
    };
    secondaryPartyInfo?: {
      cardholderName?: string;
      deceasedState?: string;
      deceasedCounty?: string;
      insuranceIndicator?: string;
      contact?: string;
      relationship?: string;
      contactName?: string;
      contactPhone?: string;
      address?: string;
      city?: string;
      state?: string;
      zip?: string;
      fax?: string;
    };
  };
}

export interface IDataRequestDeceasedPending
  extends IDeceasedMainSecondPartyInfo,
    IDeceasedMainPrimaryPartyInfo,
    Partial<IDeceasedPrimaryPartyInfo>,
    Partial<IDeceasedSecondPartyInfo>,
    IDeceased {}

export interface IDeceasedMainSecondPartyInfo {
  dateNoticeReceivedMainSecond?: string;
  deceasedDateOfDeathMainSecond?: string;
}

export interface IDeceasedMainPrimaryPartyInfo {
  deceasedDatePrimary?: string;
  deceasedDateNoticeReceivedPrimary?: string;
}

export interface IDeceasedPrimaryPartyInfo {
  primaryCustomerName?: string;
  deceasedCountyPrimary?: string;
  insuranceIndicator?: string;
  deceasedContactPrimary?: string;
  deceasedContactRelationshipPrimary?: string;
  deceasedContactNamePrimary?: string;
  deceasedContactPhoneNumberPrimary?: string;
  deceasedContactAddressPrimary?: string;
  deceasedCityPrimary?: string;
  deceasedStatePrimary?: string;
  deceasedZipPrimary?: string;
  deceasedFaxPrimary?: string;
}

export interface IDeceasedSecondPartyInfo {
  deceasedSecondPartyHeadingControl: string;
  deceasedSecondCardholderName: string;
  deceasedSecondAtState: RefData;
  deceasedSecondCountry: string;
  deceasedSecondInsuranceIndicator: string;
  deceasedSecondContact: RefDataValue;
  deceasedSecondRelationship: string;
  deceasedSecondContactName: string;
  deceasedSecondContactPhone: string;
  deceasedSecondAddress: string;
  deceasedSecondCity: string;
  deceasedSecondState: RefData;
  deceasedSecondZip: string;
  deceasedSecondFax: string;
}

export interface IRequestRemoveDeceased {
  common: { accountId: string };
  callResultType: string;
}

export interface IValueDeceased {
  deceasedDateNoticeReceived: string;
  deceasedDateOfDeath?: string;
}

export interface PrimaryPartyMainInfo {
  deceasedDatePrimary: string;
  deceasedDateNoticeReceivedPrimary: string;
}

export interface SecondaryPartyMainInfo {
  deceasedDateSecondary: string;
  deceasedDateNoticeReceivedSecondary: string;
}

export interface IDeceasedPartyInfo {
  deceasedPartyHeadingControl: string;
  deceasedCardholderName: string;
  deceasedAtState: RefData;
  deceasedCounty: string;
  deceasedInsuranceIndicator: string;
  deceasedContact: RefDataValue;
  deceasedRelationShip: string;
  deceasedContactName: string;
  deceasedContractPhone: string;
  deceasedAddress: string;
  deceasedCity: string;
  deceasedState: RefData;
  deceasedZip: string;
  deceasedFax: string;
}

export interface IArgsUpdateDeceasedPending extends StoreIdPayload {
  postData: {
    accountId: string;
  };
}

export interface DeceasedCommonRequestData {
  app: string;
  org: string;
}

export interface DeceasedInformationRequestData {
  callingApplication: string;
}

export interface DeceasedCreateWorkflowRequestData extends MagicKeyValue {}

export interface DeceasedInformationData extends MagicKeyValue {
  isNotDeceasedStatus?: boolean;
  isJointAcc?: boolean;
}

export type DeceasedInformationPayload = DeceasedInformationData;

export interface DeceasedInformationArgs {
  storeId: string;
}

export interface MessageResponse {
  status?: string;
  statusCode?: string;
  statusMessage?: string;
}

export interface DeceasedCreateWorkflow {
  caseId?: string;
  messages?: Array<MessageResponse>;
}

export type DeceasedCreateWorkflowPayload = DeceasedCreateWorkflow;

export interface DeceasedCreateWorkflowArgs {
  storeId: string;
  commonData: DeceasedCommonRequestData;
  data: MagicKeyValue;
}

export interface TriggerDeceasedPendingArgs {
  storeId: string;
  entryCode: string;
}

export interface TriggerDeceasedPending {}

export type TriggerDeceasedPendingPayload =
  | TriggerDeceasedPending
  | null
  | undefined;

export interface TriggerDeceasedConfirmedArgs {
  storeId: string;
  entryCode: string;
}

export interface TriggerDeceasedConfirmedPayload {}

export interface DeceasedSettlementInformation {
  settlementAmount: string;
  settlementDate: string;
  settlementPaymentMethod: string;
}

export interface ViewDataInfo {
  primary?: MagicKeyValue;
  secondary?: MagicKeyValue;
  settlementAmount?: string;
  settlementDate?: string;
  settlementPaymentMethod?: string;
}

export interface RemoveDeathCertificateArgs extends StoreIdPayload {
  postData: RemoveDeathCertificateRequest;
  onSuccess: () => void;
  onFailed: () => void;
  partyType: 'primary' | 'secondary';
}

export interface RemoveDeathCertificateRequest {
  accountId: string;
  fileIds: string[];
  uploadType: string;
}

export interface GetDeathCertificatePayload {
  files: File[];
}

export interface DeceasedPrimaryMainInfoForm {
  deceasedDateOfDeath?: string | Date;
  dateNoticeReceivedMainPrimary?: string | Date;
}

export interface DeceasedSecondaryMainInfoForm {
  deceasedDateOfDeath?: string | Date;
  dateNoticeReceivedMainSecond?: string | Date;
}

export interface DeceasedInformationResponse {
  deceasedInformation?: DeceasedInformation;
  deceasedSettlementInformation?: DeceasedSettlement;
  statusCode?: string;
  statusMessage?: string;
}

export enum DeceasedResultTab {
  DeceasedInfo = '01',
  DeathCertificate = '02'
}
