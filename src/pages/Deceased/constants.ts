import { Action } from './types';

export const DECEASED_PARTY_VALUE = {
  BOTH: 'both',
  PRIMARY: 'primary',
  SECONDARY: 'secondary'
};

export const DECEASED_PARTY_DESCRIPTION = {
  PRIMARY: 'txt_primary',
  SECONDARY: 'txt_secondary',
  BOTH: 'txt_both'
};

export const DECEASED_PARTIES = [
  {
    value: DECEASED_PARTY_VALUE.PRIMARY,
    description: DECEASED_PARTY_DESCRIPTION.PRIMARY
  },
  {
    value: DECEASED_PARTY_VALUE.SECONDARY,
    description: DECEASED_PARTY_DESCRIPTION.SECONDARY
  },
  {
    value: DECEASED_PARTY_VALUE.BOTH,
    description: DECEASED_PARTY_DESCRIPTION.BOTH
  }
];

export const DECEASED_PARTIES_OF_NOT_JOINT_ACC = [
  {
    value: DECEASED_PARTY_VALUE.PRIMARY,
    description: DECEASED_PARTY_DESCRIPTION.PRIMARY
  }
];

export const DEFAULT_DECEASED_PARTY_PRIMARY_VALUE =
  DECEASED_PARTY_VALUE.PRIMARY;

export const DEFAULT_DECEASED_PARTY = {
  value: DECEASED_PARTY_VALUE.PRIMARY,
  description: DECEASED_PARTY_DESCRIPTION.PRIMARY
};

export const PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW =
  'PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW';

export const SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW =
  'SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW';

export const PRIMARY_DECEASED_PARTY_INFORMATION_VIEW =
  'PRIMARY_DECEASED_PARTY_INFORMATION_VIEW';

export const SECONDARY_DECEASED_PARTY_INFORMATION_VIEW =
  'SECONDARY_DECEASED_PARTY_INFORMATION_VIEW';

export const BOTH_DECEASED_INFORMATION_VIEW = 'BOTH_DECEASED_INFORMATION_VIEW';

export const SETTLEMENT_INFORMATION_VIEW = 'SETTLEMENT_INFORMATION_VIEW';

export enum DECEASED_PARTY {
  PRIMARY = 'primary',
  SECONDARY = 'secondary',
  BOTH = 'both'
}

export enum DECEASED_FIELDS {
  PRIMARY_DATE_OF_DEATH = 'deceasedDateOfDeathMainPrimary',
  SECONDARY_DATE_OF_DEATH = 'deceasedDateOfDeathMainSecond',
  PRIMARY_DATE_NOTICE_RECEIVED = 'dateNoticeReceivedMainPrimary',
  SECOND_DATE_NOTICE_RECEIVED = 'dateNoticeReceivedMainSecond',
  SETTLEMENT_DATE = 'settlementDate',
  SETTLEMENT_METHOD = 'settlementMethod',
  SETTLEMENT_AMOUNT = 'settlementAmount'
}

export const actions: Action[] = [
  {
    description: 'Primary',
    value: DECEASED_PARTY.PRIMARY
  },
  {
    description: 'Secondary',
    value: DECEASED_PARTY.SECONDARY
  },
  {
    description: 'Both',
    value: DECEASED_PARTY.BOTH
  }
];

export const DECEASED_ENTRY_CODE = {
  DECEASED_PENDING: 'DP',
  DECEASED_CONFIRMED: 'DC',
  NOT_DECEASED: 'ND'
};

export const INSURANCE_INDICATOR_NOT_CHARGE_DESCRIPTION =
  'txt_insurance_indicator_not_charge';
export const INSURANCE_INDICATOR_CHARGE_DESCRIPTION =
  'txt_insurance_indicator_charge';
export const PRIMARY_VIEW_KY = 'PRIMARY';
export const SECONDARY_VIEW_KY = 'SECONDARY';
export const ERR_CANNOT_PROCESS = 'C-000';
export const INSURANCE_INDICATOR_NOT_CHARGE_CODE = 0;
export const INSURANCE_INDICATOR_CHARGE_CODE_MIN = 1;
export const INSURANCE_INDICATOR_CHARGE_CODE_MAX = 9;
