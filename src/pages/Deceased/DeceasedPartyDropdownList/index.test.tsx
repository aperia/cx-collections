import React from 'react';
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import DeceasedPartyDropdownList from '.';
import { fireEvent, screen } from '@testing-library/react';
import { deceasedActions } from '../_redux/reducers';

jest.mock('app/_libraries/_dls/components/DropdownList', () =>
  jest.requireActual('app/test-utils/mocks/MockDropdownList')
);

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockDeceasedActions = mockActionCreator(deceasedActions);

describe('DeceasedPartyDropdownList component', () => {
  it('should render component', () => {
    const spy = mockDeceasedActions('changeDeceasedParty');

    renderMockStoreId(<DeceasedPartyDropdownList />);

    fireEvent.change(screen.getByTestId('DropdownList.onChange'), {
      target: { value: 'undefined', valueObject: { id: 1, value: 'Test' } }
    });

    expect(spy).toBeCalled();
  });
});
