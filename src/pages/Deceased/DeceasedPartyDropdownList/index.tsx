import React, { useMemo } from 'react';

// components
import {
  DropdownButton,
  DropdownList,
  DropdownBaseChangeEvent
} from 'app/_libraries/_dls/components';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { deceasedActions } from '../_redux/reducers';
import { selectDeceasedParty, selectIsJointAcc } from '../_redux/selectors';

// constants
import {
  DECEASED_PARTIES,
  DECEASED_PARTIES_OF_NOT_JOINT_ACC
} from '../constants';

const DeceasedPartyDropdownList: React.FC = () => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const deceasedParty = useStoreIdSelector<RefDataValue>(selectDeceasedParty);
  const isJointAcc = useStoreIdSelector<boolean>(selectIsJointAcc);

  const dropdownList = (
    isJointAcc ? DECEASED_PARTIES : DECEASED_PARTIES_OF_NOT_JOINT_ACC
  ).map(item => ({
    ...item,
    description: t(item.description)
  }));

  const dropdownListItem = useMemo(() => {
    return dropdownList.map(item => (
      <DropdownButton.Item
        key={item.value}
        label={item.description}
        value={item}
      />
    ));
  }, [dropdownList]);

  const onChangeDeceasedParty = (event: DropdownBaseChangeEvent) => {
    const selectedParty = event.target.value;
    const party = dropdownList.find(item => item.value === selectedParty.value);
    dispatch(
      deceasedActions.changeDeceasedParty({
        storeId,
        deceasedParty: party
      })
    );
  };

  return (
    <div className="row">
      <div className="col-extend-md-12 col-md-4 col-extend-lg-6 col-xl-3 col-extend-xl-4 col-extend-xxl-3">
        <DropdownList
          dataTestId="deceased-deceasedParty"
          textField="description"
          name="deceasedParty"
          label={t('txt_deceased_party')}
          id="value"
          value={{
            ...deceasedParty,
            description: t(deceasedParty.description)
          }}
          required
          onChange={onChangeDeceasedParty}
          noResult={t('txt_no_results_found')}
        >
          {dropdownListItem}
        </DropdownList>
      </div>
    </div>
  );
};

export default DeceasedPartyDropdownList;
