import isEmpty from 'lodash.isempty';
import isString from 'lodash.isstring';
import { DECEASED_ENTRY_CODE, DECEASED_PARTY_VALUE } from './constants';
import { EMPTY_STRING } from 'app/constants';
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import dateTime from 'date-and-time';

export const prepareCommonData = (accId: string) => {
  const { app = EMPTY_STRING, org = EMPTY_STRING } =
    window.appConfig?.commonConfig || {};
  return {
    app,
    org,
    accountId: accId
  };
};

export const prepareDeceasedPendingData = (
  deceasedParty: string,
  viewData: MagicKeyValue,
  entryCode: string,
  callingApplication: string,
  operatorCode: string
) => {
  let mappedData: MagicKeyValue = {};
  if (
    deceasedParty === DECEASED_PARTY_VALUE.PRIMARY ||
    deceasedParty === DECEASED_PARTY_VALUE.BOTH
  ) {
    let primaryState = '';
    if (isString(viewData?.primary?.deceasedAtState)) {
      primaryState = viewData?.primary?.deceasedAtState;
    } else if (viewData?.primary?.deceasedAtState?.value) {
      primaryState = viewData?.primary?.deceasedAtState?.value;
    }

    let deceasedContact = '';
    if (isString(viewData?.primary?.deceasedContact)) {
      deceasedContact = viewData?.primary?.deceasedContact;
    } else if (viewData?.primary?.deceasedContact?.value) {
      deceasedContact = viewData?.primary?.deceasedContact?.value;
    }

    mappedData = {
      deceasedPrimaryInformation: {
        deceasedDatePrimary:
          formatDeceasedDate(viewData?.primary?.deceasedDateOfDeath) || '',
        deceasedDateNoticeReceivedPrimary:
          formatDeceasedDate(viewData?.primary?.deceasedDateNoticeReceived) ||
          '',
        primaryCustomerName: viewData?.primary?.deceasedCardholderName || '',
        deceasedStatePrimary: primaryState || '',
        deceasedCountyPrimary: viewData?.primary?.deceasedCounty || '',
        deceasedContactPrimary: deceasedContact || '',
        deceasedContactRelationshipPrimary:
          viewData?.primary?.deceasedRelationship || '',
        deceasedContactNamePrimary:
          viewData?.primary?.deceasedContactName || '',
        deceasedContactPhoneNumberPrimary:
          viewData?.primary?.deceasedContactPhone || '',
        deceasedContactAddressPrimary: viewData?.primary?.deceasedAddress || '',
        deceasedCityPrimary: viewData?.primary?.deceasedCity || '',
        deceasedZipPrimary: viewData?.primary?.deceasedZip || '',
        deceasedFaxPrimary: viewData?.primary?.deceasedFax || '',
        insuranceIndicator: viewData?.primary?.deceasedInsuranceIndicator || '',
        deceasedContactStatePrimary:
          viewData?.primary?.deceasedState?.value ||
          viewData?.primary?.deceasedState ||
          ''
      }
    };

    for (const key in mappedData.deceasedPrimaryInformation) {
      if (
        (key === 'deceasedDatePrimary' ||
          key === 'deceasedDateNoticeReceivedPrimary') &&
        isEmpty(mappedData.deceasedPrimaryInformation[`${key}`])
      ) {
        delete mappedData.deceasedPrimaryInformation[key];
      }
    }
  }
  if (
    deceasedParty === DECEASED_PARTY_VALUE.SECONDARY ||
    deceasedParty === DECEASED_PARTY_VALUE.BOTH
  ) {
    let secondaryState = '';
    if (isString(viewData?.secondary?.deceasedAtState)) {
      secondaryState = viewData?.secondary?.deceasedAtState;
    } else if (viewData?.secondary?.deceasedAtState?.value) {
      secondaryState = viewData?.secondary?.deceasedAtState?.value;
    }

    let deceasedContact = '';
    if (isString(viewData?.secondary?.deceasedContact)) {
      deceasedContact = viewData?.secondary?.deceasedContact;
    } else if (viewData?.secondary?.deceasedContact?.value) {
      deceasedContact = viewData?.secondary?.deceasedContact?.value;
    }

    mappedData = {
      ...mappedData,
      deceasedSecondaryInformation: {
        deceasedDateSecondary:
          formatDeceasedDate(viewData?.secondary?.deceasedDateOfDeath) || '',
        deceasedDateNoticeReceivedSecondary:
          formatDeceasedDate(viewData?.secondary?.deceasedDateNoticeReceived) ||
          '',
        secondaryName: viewData?.secondary?.deceasedCardholderName || '',
        deceasedStateSecondary: secondaryState || '',
        deceasedCountySecondary: viewData?.secondary?.deceasedCounty || '',
        deceasedContactSecondary: deceasedContact || '',
        deceasedContactRelationshipSecondary:
          viewData?.secondary?.deceasedRelationship || '',
        deceasedContactNameSecondary:
          viewData?.secondary?.deceasedContactName || '',
        deceasedContactPhoneNumberSecondary:
          viewData?.secondary?.deceasedContactPhone || '',
        deceasedContactAddressSecondary:
          viewData?.secondary?.deceasedAddress || '',
        deceasedCitySecondary: viewData?.secondary?.deceasedCity || '',
        deceasedZipSecondary: viewData?.secondary?.deceasedZip || '',
        deceasedFaxSecondary: viewData?.secondary?.deceasedFax || '',
        insuranceIndicator:
          viewData?.secondary?.deceasedInsuranceIndicator || '',
        deceasedContactStateSecondary:
          viewData?.secondary?.deceasedState?.value ||
          viewData?.secondary?.deceasedState ||
          ''
      }
    };

    for (const key in mappedData.deceasedSecondaryInformation) {
      if (
        (key === 'deceasedDateSecondary' ||
          key === 'deceasedDateNoticeReceivedSecondary') &&
        isEmpty(mappedData.deceasedSecondaryInformation[`${key}`])
      ) {
        delete mappedData.deceasedSecondaryInformation[key];
      }
    }
  }

  return {
    callingApplication,
    operatorCode,
    entryCode,
    deceasedInformation: {
      deceasedPartyName: deceasedParty.toUpperCase(),
      deceasedConfirmation: '',
      ...mappedData
    }
  };
};

export const prepareDeceasedConfirmedData = (
  deceasedParty: string,
  viewData: MagicKeyValue,
  entryCode: string,
  callingApplication: string,
  operatorCode: string
) => {
  let mappedData = {};
  if (deceasedParty === DECEASED_PARTY_VALUE.PRIMARY) {
    let primaryState = '';
    if (isString(viewData?.primary?.deceasedAtState)) {
      primaryState = viewData?.primary?.deceasedAtState;
    } else if (viewData?.primary?.deceasedAtState?.value) {
      primaryState = viewData?.primary?.deceasedAtState?.value;
    }

    let deceasedContact = '';
    if (isString(viewData?.primary?.deceasedContact)) {
      deceasedContact = viewData?.primary?.deceasedContact;
    } else if (viewData?.primary?.deceasedContact?.value) {
      deceasedContact = viewData?.primary?.deceasedContact?.value;
    }

    mappedData = {
      deceasedPrimaryInformation: {
        deceasedDatePrimary:
          formatDeceasedDate(viewData?.primary?.deceasedDateOfDeath) || '',
        deceasedDateNoticeReceivedPrimary:
          formatDeceasedDate(viewData?.primary?.deceasedDateNoticeReceived) ||
          '',
        primaryCustomerName: viewData?.primary?.deceasedCardholderName || '',
        deceasedStatePrimary: primaryState || '',
        deceasedCountyPrimary: viewData?.primary?.deceasedCounty || '',
        deceasedContactPrimary: deceasedContact || '',
        deceasedContactRelationshipPrimary:
          viewData?.primary?.deceasedRelationship || '',
        deceasedContactNamePrimary:
          viewData?.primary?.deceasedContactName || '',
        deceasedContactPhoneNumberPrimary:
          viewData?.primary?.deceasedContactPhone || '',
        deceasedContactAddressPrimary: viewData?.primary?.deceasedAddress || '',
        deceasedCityPrimary: viewData?.primary?.deceasedCity || '',
        deceasedZipPrimary: viewData?.primary?.deceasedZip || '',
        deceasedFaxPrimary: viewData?.primary?.deceasedFax || '',
        deceasedContactStatePrimary:
          viewData?.primary?.deceasedState?.value ||
          viewData?.primary?.deceasedState ||
          '',
        insuranceIndicator: viewData?.primary?.deceasedInsuranceIndicator || ''
      }
    };

    let finalSettlement = '';
    if (viewData?.settlementPaymentMethod?.value) {
      finalSettlement = viewData?.settlementPaymentMethod?.value;
    } else if (isString(viewData?.settlementPaymentMethod)) {
      finalSettlement = viewData?.settlementPaymentMethod;
    }

    return {
      callingApplication,
      operatorCode,
      entryCode,
      settlementAmount: viewData?.settlementAmount?.split('.')[0] || '',
      settlementDate: formatDeceasedDate(viewData?.settlementDate) || '',
      settlementPaymentMethod: finalSettlement || '',
      deceasedInformation: {
        deceasedPartyName: deceasedParty.toUpperCase(),
        deceasedConfirmation: '',
        ...mappedData
      }
    };
  }
  if (deceasedParty === DECEASED_PARTY_VALUE.SECONDARY) {
    let secondaryState = '';
    if (isString(viewData?.secondary?.deceasedAtState)) {
      secondaryState = viewData?.secondary?.deceasedAtState;
    } else if (viewData?.secondary?.deceasedAtState?.value) {
      secondaryState = viewData?.secondary?.deceasedAtState?.value;
    }

    let deceasedContact = '';
    if (isString(viewData?.secondary?.deceasedContact)) {
      deceasedContact = viewData?.secondary?.deceasedContact;
    } else if (viewData?.secondary?.deceasedContact?.value) {
      deceasedContact = viewData?.secondary?.deceasedContact?.value;
    }

    mappedData = {
      ...mappedData,
      deceasedSecondaryInformation: {
        deceasedDateSecondary:
          formatDeceasedDate(viewData?.secondary?.deceasedDateOfDeath) || '',
        deceasedDateNoticeReceivedSecondary:
          formatDeceasedDate(viewData?.secondary?.deceasedDateNoticeReceived) ||
          '',
        secondaryName: viewData?.secondary?.deceasedCardholderName || '',
        deceasedStateSecondary: secondaryState || '',
        deceasedCountySecondary: viewData?.secondary?.deceasedCounty || '',
        deceasedContactSecondary: deceasedContact || '',
        deceasedContactRelationshipSecondary:
          viewData?.secondary?.deceasedRelationship || '',
        deceasedContactNameSecondary:
          viewData?.secondary?.deceasedContactName || '',
        deceasedContactPhoneNumberSecondary:
          viewData?.secondary?.deceasedContactPhone || '',
        deceasedContactAddressSecondary:
          viewData?.secondary?.deceasedAddress || '',
        deceasedCitySecondary: viewData?.secondary?.deceasedCity || '',
        deceasedZipSecondary: viewData?.secondary?.deceasedZip || '',
        deceasedFaxSecondary: viewData?.secondary?.deceasedFax || '',
        deceasedContactStateSecondary:
          viewData?.secondary?.deceasedState?.value ||
          viewData?.secondary?.deceasedState ||
          '',
        insuranceIndicator:
          viewData?.secondary?.deceasedInsuranceIndicator || ''
      }
    };

    let finalSettlement = '';
    if (viewData?.settlementPaymentMethod?.value) {
      finalSettlement = viewData?.settlementPaymentMethod?.value;
    } else if (isString(viewData?.settlementPaymentMethod)) {
      finalSettlement = viewData?.settlementPaymentMethod;
    }

    return {
      callingApplication,
      operatorCode,
      entryCode,
      settlementAmount: viewData?.settlementAmount?.split('.')[0] || '',
      settlementDate: formatDeceasedDate(viewData?.settlementDate) || '',
      settlementPaymentMethod: finalSettlement || '',
      deceasedInformation: {
        deceasedPartyName: deceasedParty.toUpperCase(),
        deceasedConfirmation: '',
        ...mappedData
      }
    };
  }

  if (entryCode === DECEASED_ENTRY_CODE.DECEASED_CONFIRMED) {
    let primaryState = '';
    if (isString(viewData?.primary?.deceasedAtState)) {
      primaryState = viewData?.primary?.deceasedAtState;
    } else if (viewData?.primary?.deceasedAtState?.value) {
      primaryState = viewData?.primary?.deceasedAtState?.value;
    }

    let primaryDeceasedContact = '';
    if (isString(viewData?.primary?.deceasedContact)) {
      primaryDeceasedContact = viewData?.primary?.deceasedContact;
    } else if (viewData?.primary?.deceasedContact?.value) {
      primaryDeceasedContact = viewData?.primary?.deceasedContact?.value;
    }

    let secondaryState = '';
    if (isString(viewData?.secondary?.deceasedAtState)) {
      secondaryState = viewData?.secondary?.deceasedAtState;
    } else if (viewData?.secondary?.deceasedAtState?.value) {
      secondaryState = viewData?.secondary?.deceasedAtState?.value;
    }

    let secondaryDeceasedContact = '';
    if (isString(viewData?.secondary?.deceasedContact)) {
      secondaryDeceasedContact = viewData?.secondary?.deceasedContact;
    } else if (viewData?.secondary?.deceasedContact?.value) {
      secondaryDeceasedContact = viewData?.secondary?.deceasedContact?.value;
    }

    mappedData = {
      deceasedPrimaryInformation: {
        deceasedDatePrimary:
          formatDeceasedDate(viewData?.primary?.deceasedDateOfDeath) || '',
        deceasedDateNoticeReceivedPrimary:
          formatDeceasedDate(viewData?.primary?.deceasedDateNoticeReceived) ||
          '',
        primaryCustomerName: viewData?.primary?.deceasedCardholderName || '',
        deceasedStatePrimary: primaryState || '',
        deceasedCountyPrimary: viewData?.primary?.deceasedCounty || '',
        deceasedContactPrimary: primaryDeceasedContact || '',
        deceasedContactRelationshipPrimary:
          viewData?.primary?.deceasedRelationship || '',
        deceasedContactNamePrimary:
          viewData?.primary?.deceasedContactName || '',
        deceasedContactPhoneNumberPrimary:
          viewData?.primary?.deceasedContactPhone || '',
        deceasedContactAddressPrimary: viewData?.primary?.deceasedAddress || '',
        deceasedCityPrimary: viewData?.primary?.deceasedCity || '',
        deceasedZipPrimary: viewData?.primary?.deceasedZip || '',
        deceasedFaxPrimary: viewData?.primary?.deceasedFax || '',
        deceasedContactStatePrimary:
          viewData?.primary?.deceasedState?.value ||
          viewData?.primary?.deceasedState ||
          '',
        insuranceIndicator: viewData?.primary?.deceasedInsuranceIndicator || ''
      }
    };

    mappedData = {
      ...mappedData,
      deceasedSecondaryInformation: {
        deceasedDateSecondary:
          formatDeceasedDate(viewData?.secondary?.deceasedDateOfDeath) || '',
        deceasedDateNoticeReceivedSecondary:
          formatDeceasedDate(viewData?.secondary?.deceasedDateNoticeReceived) ||
          '',
        secondaryName: viewData?.secondary?.deceasedCardholderName || '',
        deceasedStateSecondary: secondaryState || '',
        deceasedCountySecondary: viewData?.secondary?.deceasedCounty || '',
        deceasedContactSecondary: secondaryDeceasedContact || '',
        deceasedContactRelationshipSecondary:
          viewData?.secondary?.deceasedRelationship || '',
        deceasedContactNameSecondary:
          viewData?.secondary?.deceasedContactName || '',
        deceasedContactPhoneNumberSecondary:
          viewData?.secondary?.deceasedContactPhone || '',
        deceasedContactAddressSecondary:
          viewData?.secondary?.deceasedAddress || '',
        deceasedCitySecondary: viewData?.secondary?.deceasedCity || '',
        deceasedZipSecondary: viewData?.secondary?.deceasedZip || '',
        deceasedFaxSecondary: viewData?.secondary?.deceasedFax || '',
        deceasedContactStateSecondary:
          viewData?.secondary?.deceasedState?.value ||
          viewData?.secondary?.deceasedState ||
          '',
        insuranceIndicator:
          viewData?.secondary?.deceasedInsuranceIndicator || ''
      }
    };

    let finalSettlement = '';
    if (viewData?.settlementPaymentMethod?.value) {
      finalSettlement = viewData?.settlementPaymentMethod?.value;
    } else if (isString(viewData?.settlementPaymentMethod)) {
      finalSettlement = viewData?.settlementPaymentMethod;
    }

    return {
      callingApplication,
      operatorCode,
      entryCode,
      settlementAmount: viewData?.settlementAmount?.split('.')[0] || '',
      settlementDate: formatDeceasedDate(viewData?.settlementDate) || '',
      settlementPaymentMethod: finalSettlement || '',
      deceasedInformation: {
        deceasedPartyName: deceasedParty.toUpperCase(),
        deceasedConfirmation: '',
        ...mappedData
      }
    };
  }

  return {
    callingApplication,
    operatorCode,
    entryCode,
    deceasedInformation: {
      deceasedPartyName: deceasedParty.toUpperCase(),
      deceasedConfirmation: '',
      ...mappedData
    }
  };
};

// API returns Deceased Date Format: YYYY-MM-DD
export const formatDeceasedDate = (data: string) => {
  if (!data) return '';
  let year, month, day;

  // Date Object from Datepicter
  if (typeof data === 'object') {
    return dateTime.format(new Date(data), 'YYYY-MM-DD');
  }

  if (data?.includes('GMT')) {
    const value = new Date(data);
    year = value.getFullYear();
    month = value.getMonth() + 1;
    day = value.getDate();
  } else {
    return data;
  }

  return `${year}-${month >= 10 ? month : `0${month}`}-${
    day >= 10 ? day : `0${day}`
  }`;
};

export const showErrorToast: AppThunk =
  (storeId: string) => (dispatch, getState) => {
    const { collectionForm } = getState();
    const message =
      collectionForm[storeId].method === COLLECTION_METHOD.CREATE
        ? I18N_COLLECTION_FORM.TOAST_DECEASED_SUBMIT_FAILURE
        : I18N_COLLECTION_FORM.TOAST_DECEASED_UPDATE_FAILURE;
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message
      })
    );
  };
