import React from 'react';

// RTL
import { screen } from '@testing-library/react';

// helpers
import {
  mockActionCreator,
  queryByClass,
  renderMockStoreId,
  storeId
} from 'app/test-utils';

// components
import DeceasedPendingForm from './index';

// constants
import { DECEASED_PARTIES } from '../constants';
import { deceasedActions } from '../_redux/reducers';

HTMLCanvasElement.prototype.getContext = jest.fn();

const deceasedActionsSpy = mockActionCreator(deceasedActions);

jest.mock('pages/CollectionForm/hooks/useSubmit', () => ({
  useSubmit: (callback: Function) => {
    callback();
  }
}));

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text || '' });
});

describe('Test DeceasedPendingForm component', () => {
  it('render component', () => {
    deceasedActionsSpy('triggerDeceasedPending');
    renderMockStoreId(<DeceasedPendingForm />);

    expect(screen.getByText('txt_deceased_party')).toBeInTheDocument();
  });

  it('render primary option', () => {
    renderMockStoreId(<DeceasedPendingForm />, {
      initialState: {
        deceased: {
          [storeId]: {
            isJointAcc: true,
            deceasedParty: DECEASED_PARTIES[0],
            deceasedInformation: {
              deceasedPrimaryInformation: {
                deceasedDatePrimary: '07-01-2021',
                deceasedDateNoticeReceivedPrimary: '07-02-2021'
              }
            }
          }
        }
      }
    });

    expect(screen.getByText('txt_primary')).toBeInTheDocument();
  });

  it('render secondary option', () => {
    renderMockStoreId(<DeceasedPendingForm />, {
      initialState: {
        deceased: {
          [storeId]: {
            isJointAcc: true,
            deceasedParty: DECEASED_PARTIES[1],
            deceasedInformation: {
              deceasedSecondaryInformation: {
                deceasedDateSecondary: '07-01-2021',
                deceasedDateNoticeReceivedSecondary: '07-02-2021'
              }
            }
          }
        }
      }
    });

    expect(screen.getByText('txt_secondary')).toBeInTheDocument();
  });

  it('render secondary option', () => {
    renderMockStoreId(<DeceasedPendingForm />, {
      initialState: {
        deceased: {
          [storeId]: {
            isJointAcc: true,
            deceasedParty: DECEASED_PARTIES[2]
          }
        }
      }
    });

    expect(screen.getByText('txt_both')).toBeInTheDocument();
  });

  it('render other option', () => {
    const { wrapper } = renderMockStoreId(<DeceasedPendingForm />, {
      initialState: {
        deceased: {
          [storeId]: {
            isJointAcc: true,
            deceasedParty: { value: 'other' }
          }
        }
      }
    });

    expect(queryByClass(wrapper.container, 'input')?.innerHTML).toEqual(
      '<span class="dls-ellipsis"></span>'
    );
  });
});
