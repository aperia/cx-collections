import { handleDateNoticeReceivedField } from 'app/components/_dof_controls/custom-functions/deceasedDateOfDeath';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
// components
import { ExtraFieldProps, View } from 'app/_libraries/_dof/core';
import { getConfigReferenceData } from 'pages/CollectionForm/CollectionActivities/GeneralInformation/_redux/selectors';
import React, { useEffect, useMemo, useRef } from 'react';
// constants
import {
  DECEASED_PARTY_VALUE,
  PRIMARY_DECEASED_PARTY_INFORMATION_VIEW,
  PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW,
  SECONDARY_DECEASED_PARTY_INFORMATION_VIEW,
  SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW
} from '../constants';
import useParseInsuranceIndicator from '../hooks/useParseInsuranceIndicator';
import useSetDeceaseContactData from '../hooks/useSetDeceaseContactData';
import { IDeceasedPartyInfo, IValueDeceased, ReferenceData } from '../types';
// redux
import {
  selectCreditLifeCode,
  selectDeceasedParty,
  selectValueDeceasedMainPrimary,
  selectValueDeceasedMainSecond,
  selectValueDeceasedPrimary,
  selectValueDeceasedSecond
} from '../_redux/selectors';

interface DeceasedPendingPartyMainInfoProp {}

const DeceasedPendingPartyMainInfo: React.FC<DeceasedPendingPartyMainInfoProp> =
  () => {
    const { t } = useTranslation();
    const { storeId } = useAccountDetail();

    const refDeceasedPartySecondary = useRef<any>(null);
    const refDeceasedPartyPrimary = useRef<any>(null);
    const refDeceasedContactPrimary = useRef<any>(null);
    const refDeceasedContactSecondary = useRef<any>(null);

    const primaryTitle = t(I18N_COLLECTION_FORM.PRIMARY_DECEASED_PARTY_MAIN);
    const secondaryTitle = t(I18N_COLLECTION_FORM.SECOND_DECEASED_PARTY_MAIN);

    const deceasedParty =
      useStoreIdSelector<ReferenceData>(selectDeceasedParty);

    const creditLifeCode = useStoreIdSelector<string>(selectCreditLifeCode);

    const valueDeceasedPrimary = useStoreIdSelector<IValueDeceased>(
      selectValueDeceasedMainPrimary
    );
    const valueDeceasedMainPrimary = useStoreIdSelector<IDeceasedPartyInfo>(
      selectValueDeceasedPrimary
    );

    const valueDeceasedSecondary = useStoreIdSelector<IValueDeceased>(
      selectValueDeceasedMainSecond
    );

    const valueDeceasedMainSecondary = useStoreIdSelector<IDeceasedPartyInfo>(
      selectValueDeceasedSecond
    );

    const { deceasedContact } = useStoreIdSelector<any>(getConfigReferenceData);

    const insuranceIndicator = useParseInsuranceIndicator(creditLifeCode);

    const primaryDeceasedParty = useMemo(() => {
      return (
        <>
          <View
            id={`${storeId}_${PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`}
            formKey={`${storeId}_${PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`}
            ref={refDeceasedPartyPrimary}
            descriptor="deceasedPartyMainInformationView"
            value={{
              ...valueDeceasedPrimary,
              deceasedPartyMainHeadingControl: primaryTitle
            }}
          />
          <View
            id={`${storeId}_${PRIMARY_DECEASED_PARTY_INFORMATION_VIEW}`}
            formKey={`${storeId}_${PRIMARY_DECEASED_PARTY_INFORMATION_VIEW}`}
            ref={refDeceasedContactPrimary}
            descriptor="deceasedPartyInformationView"
            value={{
              ...valueDeceasedMainPrimary,
              deceasedInsuranceIndicator: insuranceIndicator
            }}
          />
        </>
      );
    }, [
      storeId,
      valueDeceasedPrimary,
      primaryTitle,
      valueDeceasedMainPrimary,
      insuranceIndicator
    ]);

    const secondaryDeceasedParty = useMemo(() => {
      return (
        <>
          <View
            id={`${storeId}_${SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`}
            formKey={`${storeId}_${SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`}
            descriptor="deceasedPartyMainInformationView"
            ref={refDeceasedPartySecondary}
            value={{
              ...valueDeceasedSecondary,
              deceasedPartyMainHeadingControl: secondaryTitle
            }}
          />
          <View
            id={`${storeId}_${SECONDARY_DECEASED_PARTY_INFORMATION_VIEW}`}
            formKey={`${storeId}_${SECONDARY_DECEASED_PARTY_INFORMATION_VIEW}`}
            ref={refDeceasedContactSecondary}
            descriptor="deceasedPartyInformationView"
            value={{
              ...valueDeceasedMainSecondary,
              deceasedInsuranceIndicator: insuranceIndicator
            }}
          />
        </>
      );
    }, [
      storeId,
      valueDeceasedSecondary,
      secondaryTitle,
      valueDeceasedMainSecondary,
      insuranceIndicator
    ]);

    const renderLayout = useMemo(() => {
      if (deceasedParty.value === DECEASED_PARTY_VALUE.PRIMARY)
        return primaryDeceasedParty;
      if (deceasedParty.value === DECEASED_PARTY_VALUE.SECONDARY)
        return secondaryDeceasedParty;
      if (deceasedParty.value === DECEASED_PARTY_VALUE.BOTH)
        return (
          <>
            {primaryDeceasedParty}
            {secondaryDeceasedParty}
          </>
        );
      return null;
    }, [deceasedParty, primaryDeceasedParty, secondaryDeceasedParty]);

    useSetDeceaseContactData({
      contactViewRef: refDeceasedContactPrimary,
      deceasedContact,
      deceasedParty,
      selectedValue: valueDeceasedMainPrimary.deceasedContact
    });

    useSetDeceaseContactData({
      contactViewRef: refDeceasedContactSecondary,
      deceasedContact,
      deceasedParty,
      selectedValue: valueDeceasedMainSecondary.deceasedContact
    });

    useEffect(() => {
      if (valueDeceasedPrimary?.deceasedDateOfDeath) {
        const deceasedDateNoticeReceived =
          refDeceasedPartyPrimary?.current?.props?.onFind(
            'deceasedDateNoticeReceived'
          );

        const deceasedDateNoticeReceivedProps: ExtraFieldProps =
          deceasedDateNoticeReceived?.props;

        handleDateNoticeReceivedField(
          valueDeceasedPrimary?.deceasedDateOfDeath,
          valueDeceasedPrimary?.deceasedDateNoticeReceived,
          deceasedDateNoticeReceivedProps,
          !!valueDeceasedPrimary?.deceasedDateOfDeath
        );
      }
    }, [valueDeceasedPrimary, refDeceasedPartyPrimary, deceasedParty]);

    useEffect(() => {
      if (valueDeceasedSecondary?.deceasedDateOfDeath) {
        const deceasedDateNoticeReceived =
          refDeceasedPartySecondary?.current?.props?.onFind(
            'deceasedDateNoticeReceived'
          );

        const deceasedDateNoticeReceivedProps: ExtraFieldProps =
          deceasedDateNoticeReceived?.props;

        handleDateNoticeReceivedField(
          valueDeceasedSecondary?.deceasedDateOfDeath,
          valueDeceasedSecondary?.deceasedDateNoticeReceived,
          deceasedDateNoticeReceivedProps,
          !!valueDeceasedSecondary.deceasedDateOfDeath
        );
      }
    }, [valueDeceasedSecondary, refDeceasedPartySecondary, deceasedParty]);

    return renderLayout;
  };

export default DeceasedPendingPartyMainInfo;
