import React from 'react';

// RTL
import { screen } from '@testing-library/react';

// components
import DeceasedPendingPartyMainInfo from './DeceasedPendingPartyForms';

// helpers
import { renderMockStoreId, storeId } from 'app/test-utils';

// constants
import { DECEASED_PARTIES } from '../constants';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

describe('Test DeceasedPendingPartyMainInfo component', () => {
  it('not render component', () => {
    const { wrapper } = renderMockStoreId(<DeceasedPendingPartyMainInfo />, {
      initialState: {
        deceased: {
          [storeId]: {
            isJointAcc: true,
            deceasedParty: {
              value: 'other'
            }
          }
        }
      }
    });

    expect(wrapper.container.innerHTML).toEqual('');
  });

  it('render primary form', () => {
    renderMockStoreId(<DeceasedPendingPartyMainInfo />, {
      initialState: {
        deceased: {
          [storeId]: {
            isJointAcc: true,
            deceasedParty: DECEASED_PARTIES[0],
            deceasedInformation: {
              deceasedPrimaryInformation: {
                deceasedDatePrimary: '07-01-2021',
                deceasedDateNoticeReceivedPrimary: '07-02-2021'
              }
            }
          }
        }
      }
    });

    expect(
      screen.getByTestId('deceasedPartyMainInformationView')
    ).toBeInTheDocument();
    expect(
      screen.getByTestId('deceasedPartyInformationView')
    ).toBeInTheDocument();
  });

  it('render secondary form', () => {
    renderMockStoreId(<DeceasedPendingPartyMainInfo />, {
      initialState: {
        deceased: {
          [storeId]: {
            isJointAcc: true,
            deceasedParty: DECEASED_PARTIES[1],
            deceasedInformation: {
              deceasedSecondaryInformation: {
                deceasedDateSecondary: '07-01-2021',
                deceasedDateNoticeReceivedSecondary: '07-02-2021'
              }
            }
          }
        }
      }
    });

    expect(
      screen.getByTestId('deceasedPartyMainInformationView')
    ).toBeInTheDocument();
    expect(
      screen.getByTestId('deceasedPartyInformationView')
    ).toBeInTheDocument();
  });

  it('render both form', () => {
    renderMockStoreId(<DeceasedPendingPartyMainInfo />, {
      initialState: {
        deceased: {
          [storeId]: {
            isJointAcc: true,
            deceasedParty: DECEASED_PARTIES[2]
          }
        }
      }
    });

    expect(
      screen.getAllByTestId('deceasedPartyMainInformationView').length
    ).toEqual(2);
    expect(
      screen.getAllByTestId('deceasedPartyInformationView').length
    ).toEqual(2);
  });

  it('render both form', () => {
    renderMockStoreId(<DeceasedPendingPartyMainInfo />, {
      initialState: {
        deceased: {
          [storeId]: {
            isJointAcc: true,
            deceasedParty: DECEASED_PARTIES[2]
          }
        }
      }
    });

    expect(
      screen.getAllByTestId('deceasedPartyMainInformationView').length
    ).toEqual(2);
    expect(
      screen.getAllByTestId('deceasedPartyInformationView').length
    ).toEqual(2);
  });
});
