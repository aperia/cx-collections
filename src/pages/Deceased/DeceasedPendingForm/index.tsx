import React, { useEffect, useCallback, useMemo } from 'react';
import classNames from 'classnames';

// components
import DeceasedPartyDropdownList from '../DeceasedPartyDropdownList';
import DeceasedPendingPartyMainInfo from './DeceasedPendingPartyForms';

// redux
import { useDispatch } from 'react-redux';
import { deceasedActions } from '../_redux/reducers';
import {
  selectDeceasedParty,
  selectOriginDeceasedParty
} from '../_redux/selectors';
import { selectMemoSectionCollapsed } from 'pages/CollectionForm/_redux/selectors';

// hooks
import {
  useAccountDetail,
  useIsDirtyForm,
  useStoreIdSelector
} from 'app/hooks';
import { useSaveViewName } from 'pages/CollectionForm/hooks/useSaveViewName';

// constants
import {
  DECEASED_ENTRY_CODE,
  DECEASED_PARTY_VALUE,
  PRIMARY_DECEASED_PARTY_INFORMATION_VIEW,
  PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW,
  SECONDARY_DECEASED_PARTY_INFORMATION_VIEW,
  SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW
} from '../constants';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { useSubmit } from 'pages/CollectionForm/hooks/useSubmit';

const DeceasedPendingForm: React.FC = () => {
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();

  const isMemoSectionCollapsed = useStoreIdSelector<MagicKeyValue>(
    selectMemoSectionCollapsed
  );

  const currentDeceasedParty =
    useStoreIdSelector<RefDataValue>(selectDeceasedParty);

  const originDeceasedParty = useStoreIdSelector<string>(
    selectOriginDeceasedParty
  );

  const viewList = useMemo(() => {
    if (currentDeceasedParty.value === DECEASED_PARTY_VALUE.PRIMARY) {
      return [
        `${storeId}_${PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`,
        `${storeId}_${PRIMARY_DECEASED_PARTY_INFORMATION_VIEW}`
      ];
    } else if (currentDeceasedParty.value === DECEASED_PARTY_VALUE.SECONDARY) {
      return [
        `${storeId}_${SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`,
        `${storeId}_${SECONDARY_DECEASED_PARTY_INFORMATION_VIEW}`
      ];
    } else if (currentDeceasedParty.value === DECEASED_PARTY_VALUE.BOTH) {
      return [
        `${storeId}_${PRIMARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`,
        `${storeId}_${SECONDARY_DECEASED_PARTY_MAIN_INFORMATION_VIEW}`,
        `${storeId}_${PRIMARY_DECEASED_PARTY_INFORMATION_VIEW}`,
        `${storeId}_${SECONDARY_DECEASED_PARTY_INFORMATION_VIEW}`
      ];
    } else {
      return [];
    }
  }, [currentDeceasedParty, storeId]);

  const isDirty = useIsDirtyForm(viewList);

  const handleSubmit = useCallback(() => {
    dispatch(
      deceasedActions.triggerDeceasedPending({
        storeId,
        entryCode: DECEASED_ENTRY_CODE.DECEASED_PENDING
      })
    );
  }, [dispatch, storeId]);

  useSaveViewName(viewList);
  useSubmit(handleSubmit);

  useEffect(() => {
    dispatch(deceasedActions.deceasedInformationRequest({ storeId }));
  }, [storeId, dispatch]);

  useEffect(() => {
    return () => {
      dispatch(deceasedActions.resetDeceasedParty({ storeId }));
    };
  }, [dispatch, storeId]);

  // set dirty
  useEffect(() => {
    dispatch(
      collectionFormActions.setDirtyForm({
        storeId,
        callResult: CALL_RESULT_CODE.DECEASE_PENDING,
        value: isDirty
      })
    );
    dispatch(
      collectionFormActions.changeDisabledOk({
        storeId,
        disabledOk:
          !isDirty &&
          originDeceasedParty?.toLowerCase() ===
            currentDeceasedParty?.value?.toLowerCase()
      })
    );
  }, [
    currentDeceasedParty?.value,
    dispatch,
    isDirty,
    originDeceasedParty,
    storeId
  ]);

  return (
    <div
      className={classNames('px-24 pb-24 mx-auto max-width-lg', {
        'memos-extend': !isMemoSectionCollapsed
      })}
    >
      <DeceasedPartyDropdownList />
      <DeceasedPendingPartyMainInfo />
    </div>
  );
};

export default DeceasedPendingForm;
