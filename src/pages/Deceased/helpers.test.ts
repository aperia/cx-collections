import { DECEASED_PARTY_VALUE } from './constants';
import {
  prepareCommonData,
  prepareDeceasedPendingData,
  prepareDeceasedConfirmedData,
  formatDeceasedDate,
  showErrorToast
} from './helpers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { mockActionCreator, storeId } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';

const generateExpectResult = (data?: MagicKeyValue) => {
  return {
    callingApplication: '0002',
    operatorCode: 'ABC',
    entryCode: 'DP',
    deceasedInformation: {
      deceasedConfirmation: '',
      ...data
    }
  };
};

const generateDeceasedConfirmedExpectResult = (data?: MagicKeyValue) => {
  return {
    callingApplication: '0002',
    operatorCode: 'ABC',
    entryCode: 'DC',
    settlementAmount: '',
    settlementDate: '',
    settlementPaymentMethod: '',
    ...data
  };
};

describe('Test Deceased helpers', () => {
  const callingApplication = '0002';
  const operatorCode = 'ABC';

  describe('Function prepareDeceasedPendingData', () => {
    const entryCode = 'DP';

    it('PRIMARY > default', () => {
      const deceasedParty = DECEASED_PARTY_VALUE.PRIMARY;
      const viewData = {};
      const expectResult = generateExpectResult({
        deceasedPartyName: 'PRIMARY',
        deceasedPrimaryInformation: {
          primaryCustomerName: '',
          deceasedStatePrimary: '',
          deceasedCountyPrimary: '',
          deceasedContactPrimary: '',
          deceasedContactRelationshipPrimary: '',
          deceasedContactNamePrimary: '',
          deceasedContactPhoneNumberPrimary: '',
          deceasedContactAddressPrimary: '',
          deceasedCityPrimary: '',
          deceasedZipPrimary: '',
          deceasedFaxPrimary: '',
          insuranceIndicator: '',
          deceasedContactStatePrimary: ''
        }
      });

      const result = prepareDeceasedPendingData(
        deceasedParty,
        viewData,
        entryCode,
        callingApplication,
        operatorCode
      );

      expect(result).toEqual(expectResult);
    });

    it('PRIMARY > type of deceasedAtState and deceasedContact is string', () => {
      const deceasedParty = DECEASED_PARTY_VALUE.PRIMARY;
      const viewData = {
        primary: {
          deceasedAtState: 'TX',
          deceasedContact: 'Contact'
        }
      };
      const expectResult = generateExpectResult({
        deceasedPartyName: 'PRIMARY',
        deceasedPrimaryInformation: {
          primaryCustomerName: '',
          deceasedStatePrimary: 'TX',
          deceasedCountyPrimary: '',
          deceasedContactPrimary: 'Contact',
          deceasedContactRelationshipPrimary: '',
          deceasedContactNamePrimary: '',
          deceasedContactPhoneNumberPrimary: '',
          deceasedContactAddressPrimary: '',
          deceasedCityPrimary: '',
          deceasedZipPrimary: '',
          deceasedFaxPrimary: '',
          insuranceIndicator: '',
          deceasedContactStatePrimary: ''
        }
      });

      const result = prepareDeceasedPendingData(
        deceasedParty,
        viewData,
        entryCode,
        callingApplication,
        operatorCode
      );

      expect(result).toEqual(expectResult);
    });

    it('PRIMARY > type of deceasedAtState and deceasedContact is object', () => {
      const deceasedParty = DECEASED_PARTY_VALUE.PRIMARY;
      const viewData = {
        primary: {
          deceasedAtState: {
            value: 'TX'
          },
          deceasedContact: {
            value: 'Contact'
          }
        }
      };
      const expectResult = generateExpectResult({
        deceasedPartyName: 'PRIMARY',
        deceasedPrimaryInformation: {
          primaryCustomerName: '',
          deceasedStatePrimary: 'TX',
          deceasedCountyPrimary: '',
          deceasedContactPrimary: 'Contact',
          deceasedContactRelationshipPrimary: '',
          deceasedContactNamePrimary: '',
          deceasedContactPhoneNumberPrimary: '',
          deceasedContactAddressPrimary: '',
          deceasedCityPrimary: '',
          deceasedZipPrimary: '',
          deceasedFaxPrimary: '',
          insuranceIndicator: '',
          deceasedContactStatePrimary: ''
        }
      });

      const result = prepareDeceasedPendingData(
        deceasedParty,
        viewData,
        entryCode,
        callingApplication,
        operatorCode
      );

      expect(result).toEqual(expectResult);
    });

    it('SECONDARY > default', () => {
      const deceasedParty = DECEASED_PARTY_VALUE.SECONDARY;
      const viewData = {};
      const expectResult = generateExpectResult({
        deceasedPartyName: 'SECONDARY',
        deceasedSecondaryInformation: {
          secondaryName: '',
          deceasedStateSecondary: '',
          deceasedCountySecondary: '',
          deceasedContactSecondary: '',
          deceasedContactRelationshipSecondary: '',
          deceasedContactNameSecondary: '',
          deceasedContactPhoneNumberSecondary: '',
          deceasedContactAddressSecondary: '',
          deceasedCitySecondary: '',
          deceasedZipSecondary: '',
          deceasedFaxSecondary: '',
          insuranceIndicator: '',
          deceasedContactStateSecondary: ''
        }
      });

      const result = prepareDeceasedPendingData(
        deceasedParty,
        viewData,
        entryCode,
        callingApplication,
        operatorCode
      );

      expect(result).toEqual(expectResult);
    });

    it('SECONDARY > type of deceasedAtState and deceasedContact is string', () => {
      const deceasedParty = DECEASED_PARTY_VALUE.SECONDARY;
      const viewData = {
        secondary: {
          deceasedAtState: 'TX',
          deceasedContact: 'Contact'
        }
      };
      const expectResult = generateExpectResult({
        deceasedPartyName: 'SECONDARY',
        deceasedSecondaryInformation: {
          secondaryName: '',
          deceasedStateSecondary: 'TX',
          deceasedCountySecondary: '',
          deceasedContactSecondary: 'Contact',
          deceasedContactRelationshipSecondary: '',
          deceasedContactNameSecondary: '',
          deceasedContactPhoneNumberSecondary: '',
          deceasedContactAddressSecondary: '',
          deceasedCitySecondary: '',
          deceasedZipSecondary: '',
          deceasedFaxSecondary: '',
          insuranceIndicator: '',
          deceasedContactStateSecondary: ''
        }
      });

      const result = prepareDeceasedPendingData(
        deceasedParty,
        viewData,
        entryCode,
        callingApplication,
        operatorCode
      );

      expect(result).toEqual(expectResult);
    });

    it('SECONDARY > type of deceasedAtState and deceasedContact is object', () => {
      const deceasedParty = DECEASED_PARTY_VALUE.SECONDARY;
      const viewData = {
        secondary: {
          deceasedAtState: {
            value: 'TX'
          },
          deceasedContact: {
            value: 'Contact'
          }
        }
      };
      const expectResult = generateExpectResult({
        deceasedPartyName: 'SECONDARY',
        deceasedSecondaryInformation: {
          secondaryName: '',
          deceasedStateSecondary: 'TX',
          deceasedCountySecondary: '',
          deceasedContactSecondary: 'Contact',
          deceasedContactRelationshipSecondary: '',
          deceasedContactNameSecondary: '',
          deceasedContactPhoneNumberSecondary: '',
          deceasedContactAddressSecondary: '',
          deceasedCitySecondary: '',
          deceasedZipSecondary: '',
          deceasedFaxSecondary: '',
          insuranceIndicator: '',
          deceasedContactStateSecondary: ''
        }
      });

      const result = prepareDeceasedPendingData(
        deceasedParty,
        viewData,
        entryCode,
        callingApplication,
        operatorCode
      );

      expect(result).toEqual(expectResult);
    });
  });

  describe('Function prepareDeceasedConfirmedData', () => {
    const entryCode = 'DC';

    it('PRIMARY > default', () => {
      const deceasedParty = DECEASED_PARTY_VALUE.PRIMARY;
      const viewData = {};
      const expectResult = generateDeceasedConfirmedExpectResult({
        deceasedInformation: {
          deceasedPartyName: 'PRIMARY',
          deceasedConfirmation: '',
          deceasedPrimaryInformation: {
            deceasedDatePrimary: '',
            deceasedDateNoticeReceivedPrimary: '',
            primaryCustomerName: '',
            deceasedStatePrimary: '',
            deceasedCountyPrimary: '',
            deceasedContactPrimary: '',
            deceasedContactRelationshipPrimary: '',
            deceasedContactNamePrimary: '',
            deceasedContactPhoneNumberPrimary: '',
            deceasedContactAddressPrimary: '',
            deceasedCityPrimary: '',
            deceasedZipPrimary: '',
            deceasedFaxPrimary: '',
            deceasedContactStatePrimary: '',
            insuranceIndicator: ''
          }
        }
      });

      const result = prepareDeceasedConfirmedData(
        deceasedParty,
        viewData,
        entryCode,
        callingApplication,
        operatorCode
      );

      expect(result).toEqual(expectResult);
    });

    it('PRIMARY > type of deceasedAtState, deceasedContact, settlementPaymentMethod is string', () => {
      const deceasedParty = DECEASED_PARTY_VALUE.PRIMARY;
      const viewData = {
        primary: {
          deceasedAtState: 'TX',
          deceasedContact: 'Contact'
        },
        settlementPaymentMethod: 'CHECK'
      };
      const expectResult = generateDeceasedConfirmedExpectResult({
        settlementPaymentMethod: 'CHECK',
        deceasedInformation: {
          deceasedPartyName: 'PRIMARY',
          deceasedConfirmation: '',
          deceasedPrimaryInformation: {
            deceasedDatePrimary: '',
            deceasedDateNoticeReceivedPrimary: '',
            primaryCustomerName: '',
            deceasedStatePrimary: 'TX',
            deceasedCountyPrimary: '',
            deceasedContactPrimary: 'Contact',
            deceasedContactRelationshipPrimary: '',
            deceasedContactNamePrimary: '',
            deceasedContactPhoneNumberPrimary: '',
            deceasedContactAddressPrimary: '',
            deceasedCityPrimary: '',
            deceasedZipPrimary: '',
            deceasedFaxPrimary: '',
            deceasedContactStatePrimary: '',
            insuranceIndicator: ''
          }
        }
      });

      const result = prepareDeceasedConfirmedData(
        deceasedParty,
        viewData,
        entryCode,
        callingApplication,
        operatorCode
      );

      expect(result).toEqual(expectResult);
    });

    it('PRIMARY > type of deceasedAtState, deceasedContact, settlementPaymentMethod is object', () => {
      const deceasedParty = DECEASED_PARTY_VALUE.PRIMARY;
      const viewData = {
        primary: {
          deceasedAtState: {
            value: 'TX'
          },
          deceasedContact: {
            value: 'Contact'
          }
        },
        settlementPaymentMethod: {
          value: 'CHECK'
        }
      };
      const expectResult = generateDeceasedConfirmedExpectResult({
        settlementPaymentMethod: 'CHECK',
        deceasedInformation: {
          deceasedPartyName: 'PRIMARY',
          deceasedConfirmation: '',
          deceasedPrimaryInformation: {
            deceasedDatePrimary: '',
            deceasedDateNoticeReceivedPrimary: '',
            primaryCustomerName: '',
            deceasedStatePrimary: 'TX',
            deceasedCountyPrimary: '',
            deceasedContactPrimary: 'Contact',
            deceasedContactRelationshipPrimary: '',
            deceasedContactNamePrimary: '',
            deceasedContactPhoneNumberPrimary: '',
            deceasedContactAddressPrimary: '',
            deceasedCityPrimary: '',
            deceasedZipPrimary: '',
            deceasedFaxPrimary: '',
            deceasedContactStatePrimary: '',
            insuranceIndicator: ''
          }
        }
      });

      const result = prepareDeceasedConfirmedData(
        deceasedParty,
        viewData,
        entryCode,
        callingApplication,
        operatorCode
      );

      expect(result).toEqual(expectResult);
    });

    it('SECONDARY > default', () => {
      const deceasedParty = DECEASED_PARTY_VALUE.SECONDARY;
      const viewData = {};
      const expectResult = generateDeceasedConfirmedExpectResult({
        deceasedInformation: {
          deceasedPartyName: 'SECONDARY',
          deceasedConfirmation: '',
          deceasedSecondaryInformation: {
            deceasedDateSecondary: '',
            deceasedDateNoticeReceivedSecondary: '',
            secondaryName: '',
            deceasedStateSecondary: '',
            deceasedCountySecondary: '',
            deceasedContactSecondary: '',
            deceasedContactRelationshipSecondary: '',
            deceasedContactNameSecondary: '',
            deceasedContactPhoneNumberSecondary: '',
            deceasedContactAddressSecondary: '',
            deceasedCitySecondary: '',
            deceasedZipSecondary: '',
            deceasedFaxSecondary: '',
            deceasedContactStateSecondary: '',
            insuranceIndicator: ''
          }
        }
      });

      const result = prepareDeceasedConfirmedData(
        deceasedParty,
        viewData,
        entryCode,
        callingApplication,
        operatorCode
      );

      expect(result).toEqual(expectResult);
    });

    it('SECONDARY > type of deceasedAtState, deceasedContact, settlementPaymentMethod is string', () => {
      const deceasedParty = DECEASED_PARTY_VALUE.SECONDARY;
      const viewData = {
        secondary: {
          deceasedAtState: 'TX',
          deceasedContact: 'Contact'
        },
        settlementPaymentMethod: 'CHECK'
      };
      const expectResult = generateDeceasedConfirmedExpectResult({
        settlementPaymentMethod: 'CHECK',
        deceasedInformation: {
          deceasedPartyName: 'SECONDARY',
          deceasedConfirmation: '',
          deceasedSecondaryInformation: {
            deceasedDateSecondary: '',
            deceasedDateNoticeReceivedSecondary: '',
            secondaryName: '',
            deceasedStateSecondary: 'TX',
            deceasedCountySecondary: '',
            deceasedContactSecondary: 'Contact',
            deceasedContactRelationshipSecondary: '',
            deceasedContactNameSecondary: '',
            deceasedContactPhoneNumberSecondary: '',
            deceasedContactAddressSecondary: '',
            deceasedCitySecondary: '',
            deceasedZipSecondary: '',
            deceasedFaxSecondary: '',
            deceasedContactStateSecondary: '',
            insuranceIndicator: ''
          }
        }
      });

      const result = prepareDeceasedConfirmedData(
        deceasedParty,
        viewData,
        entryCode,
        callingApplication,
        operatorCode
      );

      expect(result).toEqual(expectResult);
    });

    it('SECONDARY > type of deceasedAtState, deceasedContact, settlementPaymentMethod is object', () => {
      const deceasedParty = DECEASED_PARTY_VALUE.SECONDARY;
      const viewData = {
        secondary: {
          deceasedAtState: {
            value: 'TX'
          },
          deceasedContact: {
            value: 'Contact'
          }
        },
        settlementPaymentMethod: {
          value: 'CHECK'
        }
      };
      const expectResult = generateDeceasedConfirmedExpectResult({
        settlementPaymentMethod: 'CHECK',
        deceasedInformation: {
          deceasedPartyName: 'SECONDARY',
          deceasedConfirmation: '',
          deceasedSecondaryInformation: {
            deceasedDateSecondary: '',
            deceasedDateNoticeReceivedSecondary: '',
            secondaryName: '',
            deceasedStateSecondary: 'TX',
            deceasedCountySecondary: '',
            deceasedContactSecondary: 'Contact',
            deceasedContactRelationshipSecondary: '',
            deceasedContactNameSecondary: '',
            deceasedContactPhoneNumberSecondary: '',
            deceasedContactAddressSecondary: '',
            deceasedCitySecondary: '',
            deceasedZipSecondary: '',
            deceasedFaxSecondary: '',
            deceasedContactStateSecondary: '',
            insuranceIndicator: ''
          }
        }
      });

      const result = prepareDeceasedConfirmedData(
        deceasedParty,
        viewData,
        entryCode,
        callingApplication,
        operatorCode
      );

      expect(result).toEqual(expectResult);
    });

    it('BOTH > type of deceasedAtState, deceasedContact, settlementPaymentMethod is string', () => {
      const deceasedParty = DECEASED_PARTY_VALUE.BOTH;
      const viewData = {
        primary: {
          deceasedAtState: 'TX',
          deceasedContact: 'Contact'
        },
        secondary: {
          deceasedAtState: 'TX',
          deceasedContact: 'Contact'
        },
        settlementPaymentMethod: 'CHECK'
      };
      const expectResult = generateDeceasedConfirmedExpectResult({
        settlementPaymentMethod: 'CHECK',
        deceasedInformation: {
          deceasedPartyName: 'BOTH',
          deceasedConfirmation: '',
          deceasedPrimaryInformation: {
            deceasedDatePrimary: '',
            deceasedDateNoticeReceivedPrimary: '',
            primaryCustomerName: '',
            deceasedStatePrimary: 'TX',
            deceasedCountyPrimary: '',
            deceasedContactPrimary: 'Contact',
            deceasedContactRelationshipPrimary: '',
            deceasedContactNamePrimary: '',
            deceasedContactPhoneNumberPrimary: '',
            deceasedContactAddressPrimary: '',
            deceasedCityPrimary: '',
            deceasedZipPrimary: '',
            deceasedFaxPrimary: '',
            deceasedContactStatePrimary: '',
            insuranceIndicator: ''
          },
          deceasedSecondaryInformation: {
            deceasedDateSecondary: '',
            deceasedDateNoticeReceivedSecondary: '',
            secondaryName: '',
            deceasedStateSecondary: 'TX',
            deceasedCountySecondary: '',
            deceasedContactSecondary: 'Contact',
            deceasedContactRelationshipSecondary: '',
            deceasedContactNameSecondary: '',
            deceasedContactPhoneNumberSecondary: '',
            deceasedContactAddressSecondary: '',
            deceasedCitySecondary: '',
            deceasedZipSecondary: '',
            deceasedFaxSecondary: '',
            deceasedContactStateSecondary: '',
            insuranceIndicator: ''
          }
        }
      });

      const result = prepareDeceasedConfirmedData(
        deceasedParty,
        viewData,
        entryCode,
        callingApplication,
        operatorCode
      );

      expect(result).toEqual(expectResult);
    });

    it('BOTH > type of deceasedAtState, deceasedContact, settlementPaymentMethod is object', () => {
      const deceasedParty = DECEASED_PARTY_VALUE.BOTH;
      const viewData = {
        primary: {
          deceasedAtState: {
            value: 'TX'
          },
          deceasedContact: {
            value: 'Contact'
          }
        },
        secondary: {
          deceasedAtState: {
            value: 'TX'
          },
          deceasedContact: {
            value: 'Contact'
          }
        },
        settlementPaymentMethod: {
          value: 'CHECK'
        }
      };
      const expectResult = generateDeceasedConfirmedExpectResult({
        settlementPaymentMethod: 'CHECK',
        deceasedInformation: {
          deceasedPartyName: 'BOTH',
          deceasedConfirmation: '',
          deceasedPrimaryInformation: {
            deceasedDatePrimary: '',
            deceasedDateNoticeReceivedPrimary: '',
            primaryCustomerName: '',
            deceasedStatePrimary: 'TX',
            deceasedCountyPrimary: '',
            deceasedContactPrimary: 'Contact',
            deceasedContactRelationshipPrimary: '',
            deceasedContactNamePrimary: '',
            deceasedContactPhoneNumberPrimary: '',
            deceasedContactAddressPrimary: '',
            deceasedCityPrimary: '',
            deceasedZipPrimary: '',
            deceasedFaxPrimary: '',
            deceasedContactStatePrimary: '',
            insuranceIndicator: ''
          },
          deceasedSecondaryInformation: {
            deceasedDateSecondary: '',
            deceasedDateNoticeReceivedSecondary: '',
            secondaryName: '',
            deceasedStateSecondary: 'TX',
            deceasedCountySecondary: '',
            deceasedContactSecondary: 'Contact',
            deceasedContactRelationshipSecondary: '',
            deceasedContactNameSecondary: '',
            deceasedContactPhoneNumberSecondary: '',
            deceasedContactAddressSecondary: '',
            deceasedCitySecondary: '',
            deceasedZipSecondary: '',
            deceasedFaxSecondary: '',
            deceasedContactStateSecondary: '',
            insuranceIndicator: ''
          }
        }
      });

      const result = prepareDeceasedConfirmedData(
        deceasedParty,
        viewData,
        entryCode,
        callingApplication,
        operatorCode
      );

      expect(result).toEqual(expectResult);
    });

    it('BOTH > default', () => {
      const deceasedParty = '';
      const viewData = {};
      const expectResult = generateDeceasedConfirmedExpectResult({
        deceasedInformation: {
          deceasedPartyName: '',
          deceasedConfirmation: '',
          deceasedPrimaryInformation: {
            deceasedDatePrimary: '',
            deceasedDateNoticeReceivedPrimary: '',
            primaryCustomerName: '',
            deceasedStatePrimary: '',
            deceasedCountyPrimary: '',
            deceasedContactPrimary: '',
            deceasedContactRelationshipPrimary: '',
            deceasedContactNamePrimary: '',
            deceasedContactPhoneNumberPrimary: '',
            deceasedContactAddressPrimary: '',
            deceasedCityPrimary: '',
            deceasedZipPrimary: '',
            deceasedFaxPrimary: '',
            deceasedContactStatePrimary: '',
            insuranceIndicator: ''
          },
          deceasedSecondaryInformation: {
            deceasedDateSecondary: '',
            deceasedDateNoticeReceivedSecondary: '',
            secondaryName: '',
            deceasedStateSecondary: '',
            deceasedCountySecondary: '',
            deceasedContactSecondary: '',
            deceasedContactRelationshipSecondary: '',
            deceasedContactNameSecondary: '',
            deceasedContactPhoneNumberSecondary: '',
            deceasedContactAddressSecondary: '',
            deceasedCitySecondary: '',
            deceasedZipSecondary: '',
            deceasedFaxSecondary: '',
            deceasedContactStateSecondary: '',
            insuranceIndicator: ''
          }
        }
      });

      const result = prepareDeceasedConfirmedData(
        deceasedParty,
        viewData,
        entryCode,
        callingApplication,
        operatorCode
      );

      expect(result).toEqual(expectResult);
    });

    it('default', () => {
      const deceasedParty = '';
      const viewData = {};

      const result = prepareDeceasedConfirmedData(
        deceasedParty,
        viewData,
        '',
        callingApplication,
        operatorCode
      );

      expect(result).toEqual({
        callingApplication: '0002',
        operatorCode: 'ABC',
        entryCode: '',
        deceasedInformation: { deceasedPartyName: '', deceasedConfirmation: '' }
      });
    });
  });

  describe('Function prepareCommonData', () => {
    it('return value', () => {
      const result = prepareCommonData('ABC');

      expect(result).toEqual({ app: '', org: '', accountId: 'ABC' });
    });
  });

  describe('Function formatDeceasedDate', () => {
    it('return empty', () => {
      const result = formatDeceasedDate('');
      expect(result).toEqual('');
    });

    it('return value', () => {
      const result = formatDeceasedDate(
        'Sat Jun 05 2021 17:01:28 GMT+0700 (Indochina Time)'
      );
      expect(result).toEqual('2021-06-05');
    });

    it('return value', () => {
      const result = formatDeceasedDate(
        'Sat Oct 15 2021 17:01:28 GMT+0700 (Indochina Time)'
      );
      expect(result).toEqual('2021-10-15');
    });

    it('return value', () => {
      const result = formatDeceasedDate('2021-06-05');
      expect(result).toEqual('2021-06-05');
    });

    it('with input object', () => {
      const a = new Date('2021/06/05');
      const result = formatDeceasedDate(a as any);
      expect(result).toEqual('2021-06-05');
    });
  });

  describe('Function showErrorToast', () => {
    const spyActionToast = mockActionCreator(actionsToast);

    it('method is create', () => {
      const mockAddToast = spyActionToast('addToast');
      const store = createStore(rootReducer, {
        collectionForm: {
          [storeId]: {
            method: COLLECTION_METHOD.CREATE
          }
        }
      });

      showErrorToast(storeId)(store.dispatch, store.getState, {});

      expect(mockAddToast).toHaveBeenCalledWith({
        show: true,
        type: 'error',
        message: I18N_COLLECTION_FORM.TOAST_DECEASED_SUBMIT_FAILURE
      });
    });

    it('method is NOT create', () => {
      const mockAddToast = spyActionToast('addToast');
      const store = createStore(rootReducer, {
        collectionForm: {
          [storeId]: {
            method: COLLECTION_METHOD.EDIT
          }
        }
      });

      showErrorToast(storeId)(store.dispatch, store.getState, {});

      expect(mockAddToast).toHaveBeenCalledWith({
        show: true,
        type: 'error',
        message: I18N_COLLECTION_FORM.TOAST_DECEASED_UPDATE_FAILURE
      });
    });
  });
});
