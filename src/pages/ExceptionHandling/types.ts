import { DateRangePickerValue, SortType } from 'app/_libraries/_dls/components';

export interface ExceptionHandlingProps { }

export const CSPA_DEFAULT_SORT_VALUE: SortType = {
  id: 'clientId',
  order: undefined
};

export interface CSPAData {
  id?: string;
  clientId?: string;
  systemId?: string;
  principleId?: string;
  agentId?: string;
  userId?: string;
  description?: string;
}

export interface CSPAState {
  loading?: boolean;
  error?: boolean;
  cspaList?: CSPAData[];
  sortBy?: SortType;
  searchValue?: string;
  textSearchValue?: string;
  caseDetail?: CSPAData;
}

export interface CaseSearchRecord {
  caseNumber?: string;
  accountNumber?: string;
  caseStatus?: string;
  updatedDate?: Date;
  operatorId?: string;
  promiseType?: string;
  dataFieldElements?: FieldElement[];
  archiveIds?: string[];
}

export interface CaseDetailFilter {
  filterType?: 'caseNumber' | 'accountNumber' | 'caseStatusAndUpdateDate';
  caseNumber?: string;
  accountNumber?: string;
  caseStatus?: CaseStatusRef;
  updateDateRanger?: DateRangePickerValue;
}

export interface CaseDetailSearchState {
  loading?: boolean;
  error?: boolean;
  caseSearchList?: CaseSearchRecord[];
  caseStatuses?: CaseStatusRef[];
  filter?: CaseDetailFilter;
  lastFilter?: CaseDetailFilter;
  isMounted?: boolean;
  isCollapsed?: boolean;
  caseDetailRecord?: CaseSearchRecord;
  taskError: TaskErrorState;
}

export interface TaskErrorState {
  loading?: boolean;
  error?: boolean;
  reprocessError?: string;
  taskErrorList?: CaseTaskError[];
}

export interface CaseSearchArg {
  cspaData: CSPAData;
  filter?: CaseDetailFilter;
}

export interface CaseSearchRequest {
  common: {
    agent: string;
    clientNumber: string;
    system: string;
    prin: string;
    user: string;
    org?: string;
    app: string;
  };
  excludeCloseCases: boolean;
  caseId?: string;
  customer?: string;
  caseStatus?: string;
  updateDateFrom?: string;
  updateDateTo?: string;
  pagingKey?: string;
  fetchSize?: string;
}

export interface SecureArchiveRequest {
  common: {
    agent: string;
    clientNumber: string;
    system: string;
    prin: string;
    user: string;
    app: string;
    org?: string;
  };
  caseId?: string;
  requestType?: string;
  archiveId?: string;
  bypassCaseIdCheck?: boolean;
  fetchSize?: string;
}

export interface ReprocessCaseRequest {
  common: {
    agent: string;
    clientNumber: string;
    system: string;
    prin: string;
    user: string;
    app: string;
    org?: string;
  };
  caseId?: string;
  moduleName?: string;
  requestType?: string;
}

export interface CaseStatusRef {
  fieldID: string;
  fieldValue: string;
  description: string;
}

export interface DataFieldElement {
  fieldName?: string;
  fieldValue?: string;
}

export interface FieldElement {
  category?: string;
  dataFieldElements?: DataFieldElement[];
}

export interface CaseTaskError {
  requestFromCallingApplication?: string;
  requestedService?: string;
  errorCode?: string;
  errorDate?: string;
  errorMessage?: string;
  retryCount?: number;
  callingApplication?: string;
}

export interface SecureArchiveArgs {
  caseId: string;
  archiveIds?: string[];
}


export interface ReprocessCaseArgs {
  caseId: string;
  requestType?: string;
  caseStatus?: string;
}