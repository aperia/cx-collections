import { CaseDetailFilter } from './types';

export const I18N_EXCEPTION = {
  EXCEPTION_TITLE: 'txt_exception_handling_title',
  CSPA_LIST: 'txt_cspa_list',
  CASE_LIST: 'txt_case_list',
  MODIFY_CLIENT_CONFIG: 'txt_modify_client_configuration',
  VIEW_REPORT: 'txt_view_report',
  CASE_NUMBER: 'txt_case_number',
  CASE_STATUS_AND_DATE_RANGE: 'txt_case_status_and_date_range',
  CASE_STATUS: 'txt_case_status',
  LAST_UPDATE_DATE: 'txt_update_date_range',
  CASE_NUMBER_IS_REQUIRED: 'txt_case_number_is_required',
  ACCOUNT_NUMBER_IS_REQUIRED: 'txt_account_number_is_required',
  CASE_STATUS_IS_REQUIRED: 'txt_case_status_is_required',
  LAST_UPDATE_DATE_IS_REQUIRED: 'txt_last_update_date_range_is_required',
  CASE_DETAIL: 'txt_case_detail',
  TASK_ERROR_DETAILS: 'txt_task_error_details',
  FAILED_TASK: 'txt_failed_task',
  ERROR_CODE: 'txt_error_code',
  ERROR_DATE: 'txt_error_date',
  ERROR_MESSAGE: 'txt_error_message',
  NUMBER_OF_RETRY: 'txt_number_of_retry',
  REPROCESS: 'txt_reprocess',
  CONFIRM_REPROCESS_TITLE: 'txt_confirm_reporcess_title',
  CONFIRM_REPROCESS_MESSAGE: 'txt_confirm_reporcess_message',
  CASE_STATUS_NO_FUTHER_ACTION: 'txt_case_status_no_futher_action',
  CASE_STATUS_ACCESS_NOT_AVAILABLE: 'txt_case_status_access_not_available'
};

export const CASE_STATUS_MAPPING: Record<string, string> = {
  '20': 'txt_promise_to_pay',
  '21': 'txt_bankruptcy',
  '22': 'txt_deceased',
  '23': 'txt_action_entry',
  '24': 'txt_hardship',
  '25': 'txt_cccs'
};

export const DEFAULT_CASE_FILTER: CaseDetailFilter = {
  filterType: 'caseNumber'
};

export const FORM_NAMES = {
  FILTER_TYPE: 'filterType',
  CASE_NUMBER: 'caseNumber',
  ACCOUNT_NUMBER: 'accountNumber',
  CASE_STATUS: 'caseStatus',
  UPDATE_DATE_RANGE: 'updateDateRanger'
};

export enum FILTER_TYPE_VALUE {
  CASE_NUMBER = 'caseNumber',
  ACCOUNT_NUMBER = 'accountNumber',
  CASE_STATUS_AND_UPDATE_DATE = 'caseStatusAndUpdateDate'
}

export const EXCEPTION_PAGE_STORE_ID = 'exception-handling-page';
