import { Button, SimpleBar } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import React, { useCallback, useEffect, useLayoutEffect, useRef } from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';
import { I18N_EXCEPTION } from '../constants';
import { cspaActions } from '../CSPAList/_redux/reducers';
import { selectedCaseDetail } from '../CSPAList/_redux/selectors';
import { CaseDetailFilter } from '../types';
import { FilterForm } from './FilterForm';
import SearchResult from './SearchResult';
import { caseSearchActions } from './_redux/reducers';
import {
  selectedCaseDetailRecord,
  selectedLastFilter
} from './_redux/selectors';
import isEqual from 'lodash.isequal';
import CaseDetailModal from './CaseDetailModal';
import { genAmtId } from 'app/_libraries/_dls/utils';

const CSPAList: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const caseDetail = useSelector(selectedCaseDetail);
  const lastFilter = useSelector(selectedLastFilter);
  const caseDetailRecord = useSelector(selectedCaseDetailRecord);
  const rightRef = useRef<HTMLDivElement | null>(null);
  const caseSearchRef = useRef<HTMLDivElement | null>(null);
  const testId = 'exceptionHandling-caseDetailSearch';

  useEffect(() => {
    dispatch(caseSearchActions.getCaseStatus());
    return () => {
      batch(() => {
        dispatch(caseSearchActions.resetToDefault());
        dispatch(cspaActions.setCaseDetail(undefined));
      });
    };
  }, [dispatch]);

  useLayoutEffect(() => {
    rightRef.current && (rightRef.current.style.height = `calc(100vh - 193px)`);
  }, []);

  const handleBackToList = useCallback(() => {
    dispatch(cspaActions.setCaseDetail(undefined));
  }, [dispatch]);

  const handleSubmit = useCallback(
    (updatedValues: CaseDetailFilter) => {
      batch(() => {
        if (!isEqual(updatedValues, lastFilter)) {
          dispatch(caseSearchActions.changeFilter(updatedValues));
          dispatch(caseSearchActions.changeLastFilter(updatedValues));
          dispatch(caseSearchActions.caseDetailSearch());
        }
      });
    },
    [dispatch, lastFilter]
  );

  return (
    <>
      <SimpleBar>
        <div ref={caseSearchRef} className="px-24 pt-24">
          <div className="d-flex align-items-center pb-24 mr-n8">
            <h3 data-testid={genAmtId(testId, 'title', '')}>
              {t(I18N_EXCEPTION.EXCEPTION_TITLE)}
            </h3>
            {caseDetail && (
              <span
                className="fs-16 color-grey-d20 px-12 py-4 bg-light-l16 br-radius-8 ml-8 fw-500"
                data-testid={genAmtId(testId, 'cspa', '')}
              >
                {`CSPA: ${caseDetail.id}`}
              </span>
            )}
            <Button
              className="ml-auto"
              variant="outline-primary"
              size="sm"
              onClick={handleBackToList}
              dataTestId={genAmtId(testId, 'cspa-list-btn', '')}
            >
              {t(I18N_EXCEPTION.CSPA_LIST)}
            </Button>
          </div>
          <div className="d-flex border-top mx-n24">
            <FilterForm onSubmit={handleSubmit} />
            <div ref={rightRef} className="flex-1">
              <SimpleBar>
                <SearchResult />
              </SimpleBar>
            </div>
          </div>
        </div>
      </SimpleBar>
      {caseDetailRecord && <CaseDetailModal />}
    </>
  );
};

export default CSPAList;
