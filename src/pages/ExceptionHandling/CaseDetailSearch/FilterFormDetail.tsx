import { ToggleButton } from 'app/components';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import {
  Button,
  ComboBox,
  DateRangePicker,
  Radio,
  SimpleBar,
  TextBox
} from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import classNames from 'classnames';
import { FormikProps, isString } from 'formik';
import React, { useCallback, useLayoutEffect, useMemo, useRef } from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';
import {
  DEFAULT_CASE_FILTER,
  FILTER_TYPE_VALUE,
  FORM_NAMES,
  I18N_EXCEPTION
} from '../constants';
import { CaseDetailFilter } from '../types';
import { caseSearchActions } from './_redux/reducers';
import { selectedIsCollapsed, selectedCaseStatuses } from './_redux/selectors';

const FilterFormDetail = (props: FormikProps<CaseDetailFilter>) => {
  const {
    values,
    errors,
    isValid,
    touched,
    setValues,
    submitForm,
    handleChange,
    handleBlur,
    validateForm,
    resetForm
  } = props;

  const dispatch = useDispatch();
  const { t } = useTranslation();
  const leftRef = useRef<HTMLDivElement | null>(null);
  const collapsed = useSelector(selectedIsCollapsed);
  const caseStatusList = useSelector(selectedCaseStatuses);
  const testId = 'exceptionHandling-caseDetailSearch-filterFormDetail';

  useLayoutEffect(() => {
    leftRef.current && (leftRef.current.style.height = `calc(100vh - 262px)`);
  }, []);

  const handleToggleButtonEvent = useCallback(() => {
    dispatch(caseSearchActions.setToggleSettingsSection(!collapsed));
  }, [dispatch, collapsed]);

  const handleResetForm = useCallback(() => {
    resetForm({
      values: DEFAULT_CASE_FILTER
    });
    validateForm(DEFAULT_CASE_FILTER);
    batch(() => {
      dispatch(caseSearchActions.changeFilter(DEFAULT_CASE_FILTER));
      dispatch(caseSearchActions.resetToDefault());
    });
  }, [dispatch, resetForm, validateForm]);

  const handleFilterChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      resetForm({
        values: DEFAULT_CASE_FILTER
      });
      validateForm(DEFAULT_CASE_FILTER);
      switch (e.target.value) {
        case FILTER_TYPE_VALUE.CASE_NUMBER:
          setValues({
            ...values,
            accountNumber: undefined,
            caseStatus: undefined,
            updateDateRanger: undefined
          });
          break;
        case FILTER_TYPE_VALUE.ACCOUNT_NUMBER:
          setValues({
            ...values,
            caseNumber: undefined,
            caseStatus: undefined,
            updateDateRanger: undefined
          });
          break;
        case FILTER_TYPE_VALUE.CASE_STATUS_AND_UPDATE_DATE:
          setValues({
            ...values,
            caseNumber: undefined,
            accountNumber: undefined
          });
          break;
      }

      handleChange(e);
    },
    [handleChange, setValues, values, resetForm, validateForm]
  );

  const handleNumericChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const value = event.target.value;
      const allowCallOnChange = /^\d+$/.test(value);
      if (!allowCallOnChange && value) {
        return;
      }
      handleChange(event);
    },
    [handleChange]
  );

  const caseNumberRadio = useMemo(
    () => (
      <div className="row">
        <div className="custom-group-radio col-12">
          <Radio dataTestId={genAmtId(testId, 'case-number', '')}>
            <Radio.Input
              id={'radio-case-number'}
              name={FORM_NAMES.FILTER_TYPE}
              checked={values?.filterType === FILTER_TYPE_VALUE.CASE_NUMBER}
              value={FILTER_TYPE_VALUE.CASE_NUMBER}
              onChange={handleFilterChange}
            ></Radio.Input>
            <Radio.Label>{t(I18N_EXCEPTION.CASE_NUMBER)}</Radio.Label>
          </Radio>
        </div>
        {values?.filterType === FILTER_TYPE_VALUE.CASE_NUMBER && (
          <div className="col-12 mt-16 pl-36">
            <TextBox
              required
              id={FORM_NAMES.CASE_NUMBER}
              name={FORM_NAMES.CASE_NUMBER}
              label={t(I18N_EXCEPTION.CASE_NUMBER)}
              value={values?.caseNumber || ''}
              onChange={handleNumericChange}
              onBlur={handleBlur}
              maxLength={19}
              error={
                touched?.caseNumber
                  ? {
                      status: isString(errors?.caseNumber),
                      message: t(errors?.caseNumber)
                    }
                  : undefined
              }
              dataTestId={genAmtId(testId, 'case-number', '')}
            />
          </div>
        )}
      </div>
    ),
    [
      errors?.caseNumber,
      handleBlur,
      handleNumericChange,
      handleFilterChange,
      t,
      touched?.caseNumber,
      values?.caseNumber,
      values?.filterType
    ]
  );

  const accountNumberRadio = useMemo(
    () => (
      <div className="row">
        <div className="custom-group-radio col-12">
          <Radio dataTestId={genAmtId(testId, 'account-number', '')}>
            <Radio.Input
              id={'radio-account-number'}
              name={FORM_NAMES.FILTER_TYPE}
              checked={values?.filterType === FILTER_TYPE_VALUE.ACCOUNT_NUMBER}
              value={FILTER_TYPE_VALUE.ACCOUNT_NUMBER}
              onChange={handleFilterChange}
            ></Radio.Input>
            <Radio.Label>{t(I18N_COMMON_TEXT.ACCOUNT_NUMBER)}</Radio.Label>
          </Radio>
        </div>
        {values?.filterType === FILTER_TYPE_VALUE.ACCOUNT_NUMBER && (
          <div className="col-12 mt-16 pl-36">
            <TextBox
              required
              id={FORM_NAMES.ACCOUNT_NUMBER}
              name={FORM_NAMES.ACCOUNT_NUMBER}
              label={t(I18N_COMMON_TEXT.ACCOUNT_NUMBER)}
              value={values?.accountNumber || ''}
              onChange={handleNumericChange}
              onBlur={handleBlur}
              maxLength={16}
              error={
                touched?.accountNumber
                  ? {
                      status: isString(errors?.accountNumber),
                      message: t(errors?.accountNumber)
                    }
                  : undefined
              }
              dataTestId={genAmtId(testId, 'account-number', '')}
            />
          </div>
        )}
      </div>
    ),
    [
      errors?.accountNumber,
      handleBlur,
      handleNumericChange,
      handleFilterChange,
      t,
      touched?.accountNumber,
      values?.accountNumber,
      values?.filterType
    ]
  );

  const comboBoxItems = useMemo(() => {
    return caseStatusList?.map((item, index) => (
      <ComboBox.Item
        key={`${index}-${item.fieldID}`}
        label={`${t(item.description)} - ${item.fieldID}`}
        value={item}
        variant="text"
      />
    ));
  }, [caseStatusList, t]);

  const handleShowDateRangePickerError = useCallback(
    (
      dateRangePickerError: MagicKeyValue
    ): { status: boolean; message: string } | undefined => {
      if (dateRangePickerError?.start) {
        return {
          status: isString(dateRangePickerError.start),
          message: t(dateRangePickerError.start)
        };
      }
      if (dateRangePickerError?.end) {
        return {
          status: isString(dateRangePickerError.end),
          message: t(dateRangePickerError.end)
        };
      }
      return undefined;
    },
    [t]
  );

  const prepareStartDate = (startDate?: Date) => {
    if (!startDate) return;

    const copiedStartDate = new Date(startDate);
    copiedStartDate.setHours(0, 0, 0, 0);

    return copiedStartDate;
  };

  const prepareEndDate = (startDate?: Date) => {
    if (!startDate) return;

    const days = 6;
    const copiedStartDate = new Date(startDate);
    copiedStartDate.setHours(0, 0, 0, 0);

    copiedStartDate.setDate(copiedStartDate.getDate() + days);

    const endDate = new Date(copiedStartDate);
    endDate.setHours(0, 0, 0, 0);

    return endDate;
  };

  const caseStatusRadio = useMemo(
    () => (
      <div className="row">
        <div className="custom-group-radio col-12">
          <Radio
            dataTestId={genAmtId(testId, 'case-status-and-date-range', '')}
          >
            <Radio.Input
              id={'radio-case-status-and-date-range'}
              name={FORM_NAMES.FILTER_TYPE}
              checked={
                values?.filterType ===
                FILTER_TYPE_VALUE.CASE_STATUS_AND_UPDATE_DATE
              }
              value={FILTER_TYPE_VALUE.CASE_STATUS_AND_UPDATE_DATE}
              onChange={handleFilterChange}
            ></Radio.Input>
            <Radio.Label>
              {t(I18N_EXCEPTION.CASE_STATUS_AND_DATE_RANGE)}
            </Radio.Label>
          </Radio>
        </div>
        {values?.filterType ===
          FILTER_TYPE_VALUE.CASE_STATUS_AND_UPDATE_DATE && (
          <div className="col-12 mt-16 pl-36">
            <ComboBox
              value={values?.caseStatus}
              required
              textField="description"
              label={t(I18N_EXCEPTION.CASE_STATUS)}
              name={FORM_NAMES.CASE_STATUS}
              onChange={handleChange}
              onBlur={handleBlur}
              error={
                touched?.caseStatus
                  ? {
                      status: isString(errors?.caseStatus),
                      message: t(errors?.caseStatus)
                    }
                  : undefined
              }
              dataTestId={genAmtId(testId, 'case-status', '')}
              noResult={t('txt_no_results_found')}
              checkAllLabel={t('txt_all')}
            >
              {comboBoxItems}
            </ComboBox>
            <DateRangePicker
              required
              minDate={prepareStartDate(values?.updateDateRanger?.start)}
              maxDate={prepareEndDate(values?.updateDateRanger?.start)}
              className={'mt-16'}
              id={FORM_NAMES.UPDATE_DATE_RANGE}
              name={FORM_NAMES.UPDATE_DATE_RANGE}
              label={t(I18N_EXCEPTION.LAST_UPDATE_DATE)}
              value={values?.updateDateRanger}
              onChange={handleChange}
              onBlur={handleBlur}
              error={
                touched?.updateDateRanger
                  ? handleShowDateRangePickerError(
                      errors?.updateDateRanger as any
                    )
                  : undefined
              }
              dataTestId={genAmtId(testId, 'update-date-range', '')}
            />
          </div>
        )}
      </div>
    ),
    [
      errors?.caseStatus,
      handleBlur,
      handleChange,
      handleFilterChange,
      t,
      touched?.caseStatus,
      values?.caseStatus,
      values?.filterType,
      errors?.updateDateRanger,
      touched?.updateDateRanger,
      values?.updateDateRanger,
      handleShowDateRangePickerError,
      comboBoxItems
    ]
  );

  return (
    <form
      onSubmit={e => {
        e.preventDefault();
        submitForm();
      }}
      className={classNames('reporting-section', {
        collapsed
      })}
    >
      <ToggleButton
        collapseMessage={t(I18N_COMMON_TEXT.EXPAND)}
        expandMessage={t(I18N_COMMON_TEXT.COLLAPSE)}
        collapsed={!collapsed}
        onToggleCollapsed={handleToggleButtonEvent}
        dataTestId={genAmtId(testId, 'expand-btn', '')}
      />
      <div ref={leftRef}>
        <SimpleBar>
          <div className="px-24 pb-24 pt-16">
            <h5 className="mb-16" data-testid={genAmtId(testId, 'search', '')}>
              {t(I18N_COMMON_TEXT.SEARCH)}
            </h5>
            <h6 data-testid={genAmtId(testId, 'search-by', '')}>
              {t(I18N_COMMON_TEXT.SEARCH_BY)}
            </h6>
            {caseNumberRadio}
            {accountNumberRadio}
            {caseStatusRadio}
          </div>
        </SimpleBar>
      </div>

      <div className="d-flex justify-content-end py-16 px-24 mr-0 ml-n24 border-top">
        <Button
          variant="secondary"
          onClick={handleResetForm}
          dataTestId={genAmtId(testId, 'reset-btn', '')}
        >
          {t(I18N_COMMON_TEXT.RESET_TO_DEFAULT)}
        </Button>
        <Button
          type="submit"
          variant="primary"
          disabled={!isValid}
          dataTestId={genAmtId(testId, 'apply-btn', '')}
        >
          {t(I18N_COMMON_TEXT.APPLY)}
        </Button>
      </div>
    </form>
  );
};

export default FilterFormDetail;
