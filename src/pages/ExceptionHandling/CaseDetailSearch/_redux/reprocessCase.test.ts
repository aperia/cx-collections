
import { byPassFulfilled, byPassRejected, mockActionCreator, responseDefault } from 'app/test-utils';
import { exceptionHandlingServices } from 'pages/ExceptionHandling/exceptionHandlingService';
import { reprocessCase, triggerReprocessCase } from './reprocessCase';

//redux
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';

import { ReprocessCaseArgs } from 'pages/ExceptionHandling/types';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';



const mockToastAction = mockActionCreator(actionsToast);
const mockApiErrorAction = mockActionCreator(apiErrorNotificationAction);

const mockResponePayload = {
  responseStatus: 'success'
} as any;

describe('Test', () => {
  describe('Test reprocessCase thunk', () => {

    it('it should run successfully', async () => {
      const store = createStore(rootReducer, {
        cspa: {
          caseDetail: {},
        }
      })
      const params = {
        caseId: 'caseId',
        requestType: 'requestType',
        caseStatus: 'caseStatues',
      } as ReprocessCaseArgs;

      jest.spyOn(exceptionHandlingServices, 'reprocessCase').mockResolvedValue({
        ...responseDefault, data: mockResponePayload
      });

      const response = await reprocessCase(params)(store.dispatch, store.getState, {});
      expect(response.payload).toBeUndefined();
    });


    it('it should run failed', async () => {
      const store = createStore(rootReducer, {
        cspa: {
          caseDetail: {},
        }
      })
      const params = {
        caseId: 'caseId',
        requestType: 'requestType',
        caseStatus: 'caseStatues',
      } as ReprocessCaseArgs;

      jest.spyOn(exceptionHandlingServices, 'reprocessCase').mockResolvedValue({
        ...responseDefault, data: { responseStatus: 'failed' }
      });

      const response = await reprocessCase(params)(store.dispatch, store.getState, {});
      expect(response.payload).toBeDefined();
    });

    it('it should run reject action', async () => {
      const store = createStore(rootReducer, {
        cspa: {}
      })
      const params = {
        caseId: 'caseId',
        requestType: 'requestType',
        caseStatus: 'caseStatues',
      } as ReprocessCaseArgs;

      jest.spyOn(exceptionHandlingServices, 'reprocessCase').mockRejectedValue({
        ...responseDefault,
      });

      const response = await reprocessCase(params)(store.dispatch, store.getState, {});

      expect(response.payload).toBeDefined();
    });

  });

  describe('Test triggerReprocessCase thunk', () => {

    it('it should run triggerReprocessCase successfully', async () => {
      const store = createStore(rootReducer);

      const addToastAction = mockToastAction('addToast');
      const clearApiErrorAction = mockApiErrorAction('clearApiErrorData');

      const params = {
        caseId: 'caseId',
        requestType: 'requestType',
        caseStatus: 'caseStatues',
      } as ReprocessCaseArgs;

      await triggerReprocessCase(params)(
        byPassFulfilled(triggerReprocessCase.fulfilled.type, { 'message': 'txt_task_reprocessed' }),
        store.getState, {});

      expect(addToastAction).toBeCalledWith({
        show: true,
        type: 'success',
        message: 'txt_task_reprocessed'
      });
      expect(clearApiErrorAction).toBeCalled();

    });

    it('It should run triggerReprocessCase failed', async () => {
      const store = createStore(rootReducer, {
        cspa: {
          caseDetail: {},
        }
      });
      const addToastAction = mockToastAction('addToast');
      const clearUpdateApiError = mockApiErrorAction('updateApiError');

      const params = {
        caseId: 'caseId',
        requestType: 'requestType',
        caseStatus: 'caseStatues',
      } as ReprocessCaseArgs;

      await triggerReprocessCase(params)(
        byPassRejected(triggerReprocessCase.rejected.type),
        store.getState, {});

      expect(addToastAction).toBeCalledWith({
        show: true,
        type: 'error',
        message: 'txt_task_failed_to_reprocess'
      });
      expect(clearUpdateApiError).toBeCalled();

    });
  })
});




