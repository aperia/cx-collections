import { createSelector } from '@reduxjs/toolkit';
import { DEFAULT_CASE_FILTER } from 'pages/ExceptionHandling/constants';
import { CaseSearchRecord } from 'pages/ExceptionHandling/types';

const getCaseSearchList = (states: RootState) =>
  states?.caseDetailSearch?.caseSearchList || [];
const getLoading = (states: RootState) => states?.caseDetailSearch?.loading;
const getError = (states: RootState) => states?.caseDetailSearch?.error;

export const selectedCaseSearchList = createSelector(
  getCaseSearchList,
  (data: CaseSearchRecord[]) => data
);

export const selectedLoading = createSelector(getLoading, data => data);

export const selectedError = createSelector(getError, data => data);

const getFilter = (states: RootState) =>
  states?.caseDetailSearch?.filter || DEFAULT_CASE_FILTER;
const getLastFilter = (states: RootState) =>
  states?.caseDetailSearch?.lastFilter || DEFAULT_CASE_FILTER;
const getFilterType = (states: RootState) =>
  states?.caseDetailSearch?.filter?.filterType ||
  DEFAULT_CASE_FILTER.filterType;

export const selectedFilter = createSelector(getFilter, data => data);
export const selectedLastFilter = createSelector(getLastFilter, data => data);

export const selectedFilterType = createSelector(getFilterType, data => data);

const getIsMounted = (states: RootState) => states?.caseDetailSearch?.isMounted;

export const selectedIsMounted = createSelector(getIsMounted, data => data);

const getIsCollapsed = (states: RootState) =>
  states?.caseDetailSearch?.isCollapsed;

export const selectedIsCollapsed = createSelector(getIsCollapsed, data => data);

const getCaseStatuses = (states: RootState) =>
  states?.caseDetailSearch?.caseStatuses || [];

export const selectedCaseStatuses = createSelector(
  getCaseStatuses,
  data => data
);

const getCaseDetailRecord = (states: RootState) =>
  states?.caseDetailSearch?.caseDetailRecord;

export const selectedCaseDetailRecord = createSelector(
  getCaseDetailRecord,
  data => data
);

const getTaskErrorList = (states: RootState) =>
  states?.caseDetailSearch?.taskError.taskErrorList || [];
const getTaskErrorLoading = (states: RootState) =>
  states?.caseDetailSearch?.taskError.loading || false;
const getTaskErrorError = (states: RootState) =>
  states?.caseDetailSearch?.taskError.error || false;

export const selectedTaskErrorList = createSelector(
  getTaskErrorList,
  data => data
);
export const selectedTaskErrorLoading = createSelector(
  getTaskErrorLoading,
  data => data
);
export const selectedTaskErrorError = createSelector(
  getTaskErrorError,
  data => data
);

const getTaskErrorDetail = (states: RootState) =>
  states?.caseDetailSearch?.taskError.reprocessError || undefined;

export const selectedTaskErrorDetail = createSelector(
  getTaskErrorDetail,
  data => data
);
