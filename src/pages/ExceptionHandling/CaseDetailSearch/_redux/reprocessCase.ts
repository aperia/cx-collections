import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { EXCEPTION_PAGE_STORE_ID } from 'pages/ExceptionHandling/constants';
import { exceptionHandlingServices } from 'pages/ExceptionHandling/exceptionHandlingService';
import {
  CODE_ALL,
  getModuleFromRequestService
} from 'pages/ExceptionHandling/helpers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import {
  CaseDetailSearchState,
  ReprocessCaseArgs,
  ReprocessCaseRequest
} from '../../types';
import { caseSearchActions } from './reducers';

export const reprocessCase = createAsyncThunk<
  unknown,
  ReprocessCaseArgs,
  ThunkAPIConfig
>('caseSearch/reprocessCase', async (args, thunkAPI) => {
  try {
    const state = thunkAPI.getState();
    const cspaData = state.cspa.caseDetail || {};
    const moduleName = getModuleFromRequestService(args);

    const request: ReprocessCaseRequest = {
      common: {
        agent: cspaData.agentId || CODE_ALL,
        clientNumber: cspaData.clientId || CODE_ALL,
        system: cspaData.systemId || CODE_ALL,
        prin: cspaData.principleId || CODE_ALL,
        user: cspaData.userId || CODE_ALL,
        org: window.appConfig?.commonConfig?.org,
        app: 'cx'
      },
      caseId: args.caseId,
      requestType: args.requestType,
      moduleName
    };

    const response = await exceptionHandlingServices.reprocessCase(request);

    if (response.data?.responseStatus?.toLowerCase() !== 'success') {
      return thunkAPI.rejectWithValue({ response });
    }
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerReprocessCase = createAsyncThunk<
  unknown,
  ReprocessCaseArgs,
  ThunkAPIConfig
>('caseSearch/triggerReprocessCase', async (args, thunkAPI) => {
  const { dispatch } = thunkAPI;
  const response = await dispatch(reprocessCase(args));
  const state = thunkAPI.getState();
  const { caseNumber: caseId = '', archiveIds } =
    state.caseDetailSearch?.caseDetailRecord || {};

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'txt_task_reprocessed'
        })
      );
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId: EXCEPTION_PAGE_STORE_ID,
          forSection: 'inModalBody'
        })
      );
    });
  }

  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: 'txt_task_failed_to_reprocess'
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId: EXCEPTION_PAGE_STORE_ID,
          forSection: 'inModalBody',
          apiResponse: response.payload?.response
        })
      );
    });
  }

  dispatch(caseSearchActions.secureArchiveById({ caseId, archiveIds }));
});

export const reprocessCaseBuilder = (
  builder: ActionReducerMapBuilder<CaseDetailSearchState>
) => {
  builder
    .addCase(reprocessCase.pending, (draftState, action) => {
      draftState.taskError.loading = true;
    })
    .addCase(reprocessCase.fulfilled, (draftState, action) => {
      draftState.taskError.reprocessError = undefined;
      draftState.taskError.loading = false;
    })
    .addCase(reprocessCase.rejected, (draftState, action) => {
      draftState.taskError.loading = false;
      draftState.taskError.reprocessError = action.error.message;
    });
};
