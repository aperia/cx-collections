import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import {
  CASE_STATUS_MAPPING,
  DEFAULT_CASE_FILTER
} from 'pages/ExceptionHandling/constants';
import { exceptionHandlingServices } from 'pages/ExceptionHandling/exceptionHandlingService';
import { getFieldValue, getFieldValues } from 'pages/ExceptionHandling/helpers';
import { CaseSearchRecord, CaseDetailSearchState } from '../../types';

const DEFAULT_CATEGORY = 'default';
const SECURE_CATEGORY = 'securearchiveid';
const OPERATOR_FIELD = 'OperatorCode';
const PROMISE_TYPE_FIELD = 'PromiseToPayType';
const APP_ERROR_TYPE_FIELD = 'ApplicationErrorResponse';

export const caseDetailSearch = createAsyncThunk<
  CaseSearchRecord[],
  undefined,
  ThunkAPIConfig
>('caseSearch/caseDetailSearch', async (args, thunkAPI) => {
  const state = thunkAPI.getState();
  const cspaData = state.cspa.caseDetail || {};
  const filter = state.caseDetailSearch.filter || DEFAULT_CASE_FILTER;
  const mappingModel = state.mapping?.data?.caseSearchList;

  const response = await exceptionHandlingServices.caseDetailsSearch(
    cspaData,
    filter
  );

  if (response.data?.responseStatus?.toLowerCase() !== 'success') {
    throw new Error('Status is not success');
  }
  const rawMappingData = mappingDataFromArray<CaseSearchRecord>(
    response.data?.caseSearchRecordList,
    mappingModel!
  );

  const caseSearchList = rawMappingData?.map(
    c =>
      ({
        ...c,
        caseStatus: CASE_STATUS_MAPPING[`${c.caseStatus}`] || c.caseStatus,
        operatorId: getFieldValue(
          c.dataFieldElements,
          DEFAULT_CATEGORY,
          OPERATOR_FIELD
        ),
        promiseType: getFieldValue(
          c.dataFieldElements,
          DEFAULT_CATEGORY,
          PROMISE_TYPE_FIELD
        ),
        archiveIds: getFieldValues(
          c.dataFieldElements,
          SECURE_CATEGORY,
          APP_ERROR_TYPE_FIELD
        )
      } as CaseSearchRecord)
  );
  return caseSearchList;
});

export const caseDetailSearchBuilder = (
  builder: ActionReducerMapBuilder<CaseDetailSearchState>
) => {
  builder
    .addCase(caseDetailSearch.pending, (draftState, action) => {
      draftState.loading = true;
      draftState.error = false;
    })
    .addCase(caseDetailSearch.fulfilled, (draftState, action) => {
      draftState.caseSearchList = action.payload;
      draftState.loading = false;
    })
    .addCase(caseDetailSearch.rejected, (draftState, action) => {
      draftState.loading = false;
      draftState.error = true;
    });
};
