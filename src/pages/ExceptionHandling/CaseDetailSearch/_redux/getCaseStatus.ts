import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { exceptionHandlingServices } from 'pages/ExceptionHandling/exceptionHandlingService';
import { CaseDetailSearchState, CaseStatusRef } from '../../types';

export const getCaseStatus = createAsyncThunk<
  CaseStatusRef[],
  undefined,
  ThunkAPIConfig
>('caseSearch/getCaseStatus', async (args, thunkAPI) => {
  const response = await exceptionHandlingServices.getCaseStatuses();

  const caseStatusList = response.data;

  return caseStatusList;
});

export const getCaseStatusBuilder = (
  builder: ActionReducerMapBuilder<CaseDetailSearchState>
) => {
  builder
    .addCase(getCaseStatus.fulfilled, (draftState, action) => {
      draftState.caseStatuses = action.payload;
    })
    .addCase(getCaseStatus.rejected, (draftState, action) => {
      draftState.caseStatuses = undefined;
    });
};
