import * as selectors from './selectors';
import { selectorWrapper } from 'app/test-utils';
import { DEFAULT_CASE_FILTER } from 'pages/ExceptionHandling/constants';

const SAMPLE_DATA_CASE_DETAIL_SEARCH = {
  caseSearchList: [
    {
      caseStatus: "20",
      dataFieldElements: [
        {
          category: "default",
          dataFieldElements: [
            {
              fieldName: "BankruptcyAction",
              fieldValue: "ADD/UPDATE",
            },
            {
              fieldName: "BankruptcyOperationDate",
              fieldValue: "2021-08-02",
            },
            {
              fieldName: "BankruptcyStatus",
              fieldValue: "ACTIVE",
            },
            {
              fieldName: "LastActionPerformed",
              fieldValue: "BANKRUPTCY",
            }
          ]
        },

      ],
    }, {
      caseStatus: "29",
      dataFieldElements: [
        {
          category: "default",
          dataFieldElements: [
            {
              fieldName: "BankruptcyAction",
              fieldValue: "ADD/UPDATE",
            },
            {
              fieldName: "BankruptcyOperationDate",
              fieldValue: "2021-08-02",
            },
            {
              fieldName: "BankruptcyStatus",
              fieldValue: "ACTIVE",
            },
            {
              fieldName: "LastActionPerformed",
              fieldValue: "BANKRUPTCY",
            }
          ]
        },

      ],
    }
  ],
  filter: DEFAULT_CASE_FILTER,
  loading: false,
  error: false,
  caseStatuses: [{
    fieldID: 'fieldID',
    fieldValue: 'fieldValue',
    description: 'description'
  }],
  caseDetailRecord: {
    caseStatus: "20",
    dataFieldElements: [
      {
        category: "default",
        dataFieldElements: [
          {
            fieldName: "BankruptcyAction",
            fieldValue: "ADD/UPDATE",
          }
        ]
      },
    ],
  },
  taskErrorList: [],
};

const statesCaseDetailSearch: Partial<RootState> = {
  caseDetailSearch: {
    taskError: {
      error: false,
      reprocessError: ''
    },
    caseSearchList: [...SAMPLE_DATA_CASE_DETAIL_SEARCH.caseSearchList],
    loading: false,
    error: false,
    isMounted: false,
    isCollapsed: false,
    filter: { ...SAMPLE_DATA_CASE_DETAIL_SEARCH.filter },
    lastFilter: { ...SAMPLE_DATA_CASE_DETAIL_SEARCH.filter },
    caseStatuses: [...SAMPLE_DATA_CASE_DETAIL_SEARCH.caseStatuses],
    caseDetailRecord: { ...SAMPLE_DATA_CASE_DETAIL_SEARCH.caseDetailRecord }
  }
}

const selectorTrigger = selectorWrapper(statesCaseDetailSearch);

describe('Test CaseDetailSearch selector', () => {

  it('selectedCaseSearchList', () => {
    const { data } = selectorTrigger(selectors.selectedCaseSearchList);
    expect(data).toEqual(SAMPLE_DATA_CASE_DETAIL_SEARCH.caseSearchList);
  });

  it('selectedFilter', () => {
    const { data } = selectorTrigger(selectors.selectedFilter);
    expect(data).toEqual(SAMPLE_DATA_CASE_DETAIL_SEARCH.filter)
  });

  it('selectedLastFilter', () => {
    const { data } = selectorTrigger(selectors.selectedLastFilter);
    expect(data).toEqual(SAMPLE_DATA_CASE_DETAIL_SEARCH.filter)
  });

  it('selectedLoading', () => {
    const { data } = selectorTrigger(selectors.selectedLoading);
    expect(data).toBeFalsy();
  });


  it('selectedError', () => {
    const { data } = selectorTrigger(selectors.selectedError);
    expect(data).toBeFalsy();
  });


  it('selectedFilterType', () => {
    const { data } = selectorTrigger(selectors.selectedFilterType);
    expect(data).toEqual(SAMPLE_DATA_CASE_DETAIL_SEARCH.filter.filterType)
  });

  it('selectedIsMounted', () => {
    const { data } = selectorTrigger(selectors.selectedIsMounted);
    expect(data).toBeFalsy()
  });

  it('selectedIsCollapsed', () => {
    const { data } = selectorTrigger(selectors.selectedIsCollapsed);
    expect(data).toBeFalsy()
  });

  it('selectedCaseStatuses', () => {
    const { data } = selectorTrigger(selectors.selectedCaseStatuses);
    expect(data).toEqual(SAMPLE_DATA_CASE_DETAIL_SEARCH.caseStatuses)
  });

  it('selectedCaseDetailRecord', () => {
    const { data } = selectorTrigger(selectors.selectedCaseDetailRecord);
    expect(data).toEqual(SAMPLE_DATA_CASE_DETAIL_SEARCH.caseDetailRecord)
  });

  it('selectedTaskErrorList', () => {
    const { data } = selectorTrigger(selectors.selectedTaskErrorList);
    expect(data).toEqual(SAMPLE_DATA_CASE_DETAIL_SEARCH.taskErrorList)
  });

  it('selectedTaskErrorLoading', () => {
    const { data } = selectorTrigger(selectors.selectedTaskErrorLoading);
    expect(data).toBeFalsy()
  });

  it('selectedTaskErrorError', () => {
    const { data } = selectorTrigger(selectors.selectedTaskErrorError);
    expect(data).toBeFalsy()
  });

  it('selectedTaskErrorDetail', () => {
    const { data } = selectorTrigger(selectors.selectedTaskErrorDetail);
    expect(data).toBeUndefined()
  });

});