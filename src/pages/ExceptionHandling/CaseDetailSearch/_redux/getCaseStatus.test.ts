import { createStore, Store } from '@reduxjs/toolkit';

import { responseDefault } from 'app/test-utils';
import { exceptionHandlingServices } from 'pages/ExceptionHandling/exceptionHandlingService';
import { CaseSearchRecord, CaseStatusRef } from '../../types';
import { getCaseStatus } from './getCaseStatus';

//redux
import { rootReducer } from 'storeConfig';

let store: Store<RootState>;
let state: RootState;

const mockResponse: CaseSearchRecord[] = [
  {
    caseStatus: "20",
    dataFieldElements: [
      {
        category: "default",
        dataFieldElements: [
          {
            fieldName: "BankruptcyAction",
            fieldValue: "ADD/UPDATE",
          },
          {
            fieldName: "BankruptcyOperationDate",
            fieldValue: "2021-08-02",
          },
          {
            fieldName: "BankruptcyStatus",
            fieldValue: "ACTIVE",
          },
          {
            fieldName: "LastActionPerformed",
            fieldValue: "BANKRUPTCY",
          }
        ]
      },

    ],
  }
]

const mockPayload: CaseStatusRef[] = [{
  fieldID: 'fieldID',
  fieldValue: 'fieldValue',
  description: 'description'
}]

describe('Test getCaseStatuses thunk', () => {

  beforeEach(() => {
    store = createStore(rootReducer, {});
    state = store.getState();
  });

  it('it should work correctly', async () => {
    jest.spyOn(exceptionHandlingServices, 'getCaseStatuses').mockResolvedValue({
      ...responseDefault, data: mockResponse
    })
    const response = await getCaseStatus()(store.dispatch, store.getState, undefined);
    expect(response.payload).toEqual(mockResponse)
  });


})


describe('Test getCaseStatusBuilder', () => {
  it('it should be fulfilled', () => {
    const fulfilled = getCaseStatus.fulfilled(mockPayload, getCaseStatus.fulfilled.type, undefined);
    const actual = rootReducer(state, fulfilled);
    expect(actual.caseDetailSearch.caseStatuses).toEqual(mockPayload);
  })

  it('it should be rejected', () => {
    const rejected = getCaseStatus.rejected(null, getCaseStatus.rejected.type, undefined);
    const actual = rootReducer(state, rejected);
    expect(actual.caseDetailSearch.caseStatuses).toBeUndefined();
  })
})