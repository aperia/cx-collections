import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { DEFAULT_CASE_FILTER } from 'pages/ExceptionHandling/constants';
import {
  CaseDetailSearchState,
  CaseDetailFilter,
  CaseSearchRecord
} from '../../types';
import { caseDetailSearch, caseDetailSearchBuilder } from './caseDetailSearch';
import { getCaseStatus, getCaseStatusBuilder } from './getCaseStatus';
import {
  secureArchiveById,
  secureArchiveByIdBuilder
} from './secureArchiveById';
import {
  triggerReprocessCase,
  reprocessCaseBuilder
} from './reprocessCase';

export const initialState: CaseDetailSearchState = {
  taskError: {}
};

const { actions, reducer } = createSlice({
  name: 'caseDetailSearch',
  initialState: initialState,
  reducers: {
    changeFilter: (draftState, action: PayloadAction<CaseDetailFilter>) => {
      draftState.filter = action.payload;
      draftState.isMounted = true;
    },
    changeLastFilter: (draftState, action: PayloadAction<CaseDetailFilter>) => {
      draftState.lastFilter = action.payload;
      draftState.isMounted = true;
    },
    setToggleSettingsSection: (draftState, action: PayloadAction<boolean>) => {
      draftState.isCollapsed = action.payload;
    },
    openCaseDetailModal: (
      draftState,
      action: PayloadAction<CaseSearchRecord>
    ) => {
      draftState.caseDetailRecord = action.payload;
    },
    closeCaseDetailModal: draftState => {
      draftState.caseDetailRecord = undefined;
    },
    resetToDefault: draftState => {
      draftState.caseSearchList = undefined;
      draftState.caseDetailRecord = undefined;
      draftState.filter = DEFAULT_CASE_FILTER;
      draftState.lastFilter = DEFAULT_CASE_FILTER;
      draftState.loading = undefined;
      draftState.error = undefined;
    },
    cleanupTaskError: draftState => {
      draftState.taskError = {};
    },
    cleanup: () => initialState
  },
  extraReducers: builder => {
    caseDetailSearchBuilder(builder);
    getCaseStatusBuilder(builder);
    secureArchiveByIdBuilder(builder);
    reprocessCaseBuilder(builder);
  }
});

const allActions = {
  ...actions,
  caseDetailSearch,
  getCaseStatus,
  secureArchiveById,
  triggerReprocessCase
};

export { allActions as caseSearchActions, reducer };
