import { responseDefault } from 'app/test-utils';
import { createStore, Store } from '@reduxjs/toolkit';
import { rootReducer } from 'storeConfig';
import { exceptionHandlingServices } from 'pages/ExceptionHandling/exceptionHandlingService';
import { caseDetailSearch } from './caseDetailSearch';
import { CaseSearchRecord } from 'pages/ExceptionHandling/types';

let store: Store<RootState> = createStore(rootReducer, {
  cspa: {
    caseDetail: {
      id: 'id',
      clientId: 'clientId',
      systemId: 'systemId',
      principleId: 'principleId',
      agentId: 'agentId',
      userId: 'userId',
      description: 'description'
    }
  },
  mapping: {
    data: {
      caseSearchList: {
        accountNumber: 'customer',
        caseNumber: 'caseId',
        caseStatus: 'caseStatus',
        dataFieldElements: 'dataFieldElements',
        updatedDate: 'updateDate'
      }
    }
  }
} as any);

const mockCaseSearchRecord: CaseSearchRecord[] = [
  {
    caseStatus: '20',
    dataFieldElements: [
      {
        category: 'default',
        dataFieldElements: [
          {
            fieldName: 'BankruptcyAction',
            fieldValue: 'ADD/UPDATE'
          }
        ]
      }
    ]
  },
  {
    caseStatus: '29',
    dataFieldElements: [
      {
        category: 'default',
        dataFieldElements: [
          {
            fieldName: 'BankruptcyAction',
            fieldValue: 'ADD/UPDATE'
          }
        ]
      }
    ]
  }
];

const expectRes: CaseSearchRecord[] = [
  {
    accountNumber: undefined,
    caseNumber: undefined,
    caseStatus: 'txt_promise_to_pay',
    dataFieldElements: [
      {
        category: 'default',
        dataFieldElements: [
          {
            fieldName: 'BankruptcyAction',
            fieldValue: 'ADD/UPDATE'
          }
        ]
      }
    ],
    updatedDate: undefined,
    operatorId: '',
    promiseType: '',
    archiveIds: []
  },
  {
    accountNumber: undefined,
    caseNumber: undefined,
    caseStatus: '29',
    dataFieldElements: [
      {
        category: 'default',
        dataFieldElements: [
          {
            fieldName: 'BankruptcyAction',
            fieldValue: 'ADD/UPDATE'
          }
        ]
      }
    ],
    updatedDate: undefined,
    operatorId: '',
    promiseType: '',
    archiveIds: []
  }
];

const mockRes = {
  ...responseDefault,
  data: {
    responseStatus: 'success',
    caseSearchRecordList: mockCaseSearchRecord
  }
};

describe('Test caseDetailSearch thunk', () => {
  it('it should run correctly', async () => {
    store = createStore(rootReducer, {
      cspa: {},
      mapping: {
        data: {
          caseSearchList: {
            accountNumber: 'customer',
            caseNumber: 'caseId',
            caseStatus: 'caseStatus',
            dataFieldElements: 'dataFieldElements',
            updatedDate: 'updateDate'
          }
        }
      }
    } as any);

    jest
      .spyOn(exceptionHandlingServices, 'caseDetailsSearch')
      .mockResolvedValue(mockRes);

    const response = await caseDetailSearch()(
      store.dispatch,
      store.getState,
      undefined
    );
    expect(response.payload).toEqual(expectRes);
  });

  it('it should run failed ', async () => {
    jest
      .spyOn(exceptionHandlingServices, 'caseDetailsSearch')
      .mockResolvedValue({ ...mockRes, data: { responseStatus: 'failed' } });
    const response = await caseDetailSearch()(
      store.dispatch,
      store.getState,
      undefined
    );
    expect(response.payload).toBeUndefined();
  });

  it('it should run correctly with caseDetail is undefined ', async () => {
    jest
      .spyOn(exceptionHandlingServices, 'caseDetailsSearch')
      .mockResolvedValue(mockRes);

    const response = await caseDetailSearch()(
      store.dispatch,
      store.getState,
      undefined
    );
    expect(response.payload).toEqual(expectRes);
  });
});

describe('Test caseDetailSearch', () => {
  const state = store.getState();

  it('it should pending', async () => {
    const pending = caseDetailSearch.pending('', undefined);
    const actual = rootReducer(state, pending);
    expect(actual.caseDetailSearch.error).toBeFalsy();
    expect(actual.caseDetailSearch.loading).toBeTruthy();
  });

  it('it should fulfilled', async () => {
    const fulfilled = caseDetailSearch.fulfilled(
      mockCaseSearchRecord,
      caseDetailSearch.fulfilled.type,
      undefined
    );
    const actual = rootReducer(state, fulfilled);
    expect(actual.caseDetailSearch.caseSearchList).toEqual(
      mockCaseSearchRecord
    );
    expect(actual.caseDetailSearch.loading).toBeFalsy();
  });

  it('it should rejected', async () => {
    const rejected = caseDetailSearch.rejected(
      null,
      caseDetailSearch.rejected.type,
      undefined
    );
    const actual = rootReducer(state, rejected);
    expect(actual.caseDetailSearch.error).toBeTruthy();
    expect(actual.caseDetailSearch.loading).toBeFalsy();
  });
});
