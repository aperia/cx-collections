import { createStore, Store } from '@reduxjs/toolkit';
import { exceptionHandlingServices } from 'pages/ExceptionHandling/exceptionHandlingService';

import { responseDefault } from 'app/test-utils';

let store: Store<RootState>;
let state: RootState;

//redux
import { rootReducer } from 'storeConfig';
import { secureArchiveById } from './secureArchiveById';
import { CaseTaskError, SecureArchiveArgs } from 'pages/ExceptionHandling/types';

describe('test secureArchiveById thunk', () => {

  beforeEach(() => {
    store = createStore(rootReducer, {});
    state = store.getState();
  });

  it('it should run secureArchiveById', async () => {

    const childObj: any = { errorCode: "errorCode", errorMessage: "errorMessage" };
    const jsonData: any = {
      requestFromCallingApplication: "value",
      requestedService: 'value',
      retryCount: "value",
      errorDate: "errorDate",
      responseStatus: childObj,
      callingApplication: "callingApplication"
    }
    const expectResult = [
      {
        requestFromCallingApplication: 'value',
        requestedService: 'value',
        retryCount: 'value',
        errorDate: 'errorDate',
        errorCode: 'errorCode',
        errorMessage: 'errorMessage',
        callingApplication: 'callingApplication'
      },
      {
        requestFromCallingApplication: 'value',
        requestedService: 'value',
        retryCount: 'value',
        errorDate: 'errorDate',
        errorCode: 'errorCode',
        errorMessage: 'errorMessage',
        callingApplication: 'callingApplication'
      }
    ]
    jest
      .spyOn(exceptionHandlingServices, 'secureArchiveRetrievalByCaseId')
      .mockResolvedValue(
        {
          ...responseDefault, data: {
            jsonData: JSON.stringify(jsonData)
          }
        }
      );

    const response = await secureArchiveById({
      caseId: '39',
      archiveIds: ['abc', 'cde'],
    })(store.dispatch, store.getState, undefined);

    expect(response.type).toEqual(secureArchiveById.fulfilled.type);
    expect(response.payload).toEqual(expectResult);
  })

  it('it should run secureArchiveById and data is empty', async () => {
    jest
      .spyOn(exceptionHandlingServices, 'secureArchiveRetrievalByCaseId')
      .mockResolvedValue(
        {
          ...responseDefault, data: {}
        }
      );

    const response = await secureArchiveById({
      caseId: '39',
      archiveIds: ['abc', 'cde'],
    })(store.dispatch, store.getState, undefined);

    expect(response.type).toEqual(secureArchiveById.fulfilled.type);
    expect(response.payload).toEqual([]);
  })

  it('it should run secureArchiveById with archiveIds is empty', async () => {

    const response = await secureArchiveById({
      caseId: '39',
      archiveIds: [],
    })(store.dispatch, store.getState, undefined);

    expect(response.type).toEqual(secureArchiveById.fulfilled.type);
    expect(response.payload).toEqual([]);
  })
})

describe('test secureArchiveByIdBuilder', () => {

  const mockPayload = [{
    requestFromCallingApplication: 'requestFromCallingApplication',
    requestedService: 'requestedService',
    errorCode: 'errorCode',
    errorDate: 'errorDate',
    errorMessage: 'errorMessage',
    retryCount: 10,
    callingApplication: 'callingApplication',
  }] as CaseTaskError[];

  const mockSecureIdArgs = { caseId: 'caseId', archiveIds: [] } as SecureArchiveArgs;

  it('it should be pending', () => {
    const pending = secureArchiveById.pending('', { caseId: 'caseId', archiveIds: [] });
    const actual = rootReducer(state, pending);
    expect(actual.caseDetailSearch.taskError.loading).toBeTruthy();
  })

  it('it should be fulfilled', () => {
    const fulfilled = secureArchiveById.fulfilled(mockPayload, secureArchiveById.fulfilled.type, mockSecureIdArgs);
    const actual = rootReducer(state, fulfilled);
    expect(actual.caseDetailSearch.taskError.taskErrorList).toEqual(mockPayload);
    expect(actual.caseDetailSearch.taskError.loading).toBeFalsy();
  })

  it('it should be rejected', () => {
    const rejected = secureArchiveById.rejected(null, secureArchiveById.rejected.type, mockSecureIdArgs);
    const actual = rootReducer(state, rejected);
    expect(actual.caseDetailSearch.taskError.loading).toBeFalsy();
    expect(actual.caseDetailSearch.taskError.error).toBeTruthy();
  })
})