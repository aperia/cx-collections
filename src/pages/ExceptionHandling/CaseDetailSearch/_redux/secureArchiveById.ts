import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { exceptionHandlingServices } from 'pages/ExceptionHandling/exceptionHandlingService';
import { convertSecureArchiveJsonData } from 'pages/ExceptionHandling/helpers';
import {
  CaseDetailSearchState,
  CaseTaskError,
  SecureArchiveArgs
} from '../../types';

export const secureArchiveById = createAsyncThunk<
  CaseTaskError[],
  SecureArchiveArgs,
  ThunkAPIConfig
>('caseSearch/secureArchiveById', async (args, thunkAPI) => {
  const state = thunkAPI.getState();
  const cspaData = state.cspa.caseDetail || {};

  if (!args.archiveIds || args.archiveIds.length === 0) return [];

  const results = await Promise.all(
    args.archiveIds.map(archiveId => {
      return exceptionHandlingServices.secureArchiveRetrievalByCaseId(
        cspaData,
        args.caseId,
        archiveId
      );
    })
  );
  const secureArchiveList = [] as CaseTaskError[];

  results.forEach(rs => {
    const data = convertSecureArchiveJsonData(rs?.data?.jsonData);
    if (data) secureArchiveList.push(data);
  });

  return secureArchiveList;
});

export const secureArchiveByIdBuilder = (
  builder: ActionReducerMapBuilder<CaseDetailSearchState>
) => {
  builder
    .addCase(secureArchiveById.pending, (draftState, action) => {
      draftState.taskError.loading = true;
    })
    .addCase(secureArchiveById.fulfilled, (draftState, action) => {
      draftState.taskError.taskErrorList = action.payload;
      draftState.taskError.loading = false;
      draftState.taskError.error = false;
    })
    .addCase(secureArchiveById.rejected, (draftState, action) => {
      draftState.taskError.loading = false;
      draftState.taskError.error = true;
    });
};
