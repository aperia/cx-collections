import { caseSearchActions, reducer, initialState } from './reducers';

import {
  CaseSearchRecord
} from '../../types';
import { DEFAULT_CASE_FILTER } from 'pages/ExceptionHandling/constants';

const {
  changeFilter,
  changeLastFilter,
  setToggleSettingsSection,
  openCaseDetailModal,
  closeCaseDetailModal,
  resetToDefault,
  cleanupTaskError,
  cleanup
} = caseSearchActions;

describe('CaseDetailSearch reducer', () => {

  const caseSearchRecord: CaseSearchRecord = {
    caseNumber: '20',
    accountNumber: 'accountNumber',
    caseStatus: 'caseStatus',
    operatorId: 'operatorId',
    promiseType: 'promiseType',
    dataFieldElements: [],
    archiveIds: ['accountNumber'],
  }

  it('it should run changeFilter action', () => {
    const state = reducer(initialState, changeFilter(DEFAULT_CASE_FILTER));
    expect(state.filter).toEqual(DEFAULT_CASE_FILTER);
  });

  it('it should run changeLastFilter action', () => {
    const state = reducer(initialState, changeLastFilter(DEFAULT_CASE_FILTER));
    expect(state.lastFilter).toEqual(DEFAULT_CASE_FILTER);
    expect(state.isMounted).toBeTruthy();
  });

  it('it should run setToggleSettingsSection action', () => {
    const isToggle = true;
    const state = reducer(initialState, setToggleSettingsSection(isToggle));
    expect(state.isCollapsed).toEqual(isToggle);
  });

  it('it should run openCaseDetailModal action', () => {
    const state = reducer(initialState, openCaseDetailModal(caseSearchRecord));
    expect(state.caseDetailRecord).toEqual(caseSearchRecord);
  });

  it('it should run closeCaseDetailModal action', () => {
    const state = reducer(initialState, closeCaseDetailModal());
    expect(state.caseDetailRecord).toBeUndefined();
  });

  it('it should run resetToDefault action', () => {
    const state = reducer(initialState, resetToDefault());
    expect(state.caseSearchList).toBeUndefined();
    expect(state.caseDetailRecord).toBeUndefined();
    expect(state.filter).toEqual(DEFAULT_CASE_FILTER);
    expect(state.lastFilter).toEqual(DEFAULT_CASE_FILTER);
    expect(state.loading).toBeUndefined();
    expect(state.error).toBeUndefined();
  });

  it('it should run cleanupTaskError action', () => {
    const state = reducer(initialState, cleanupTaskError());
    expect(state.taskError).toEqual({});
  });

  it('it should run cleanup action', () => {
    const state = reducer(initialState, cleanup());
    expect(state).toEqual(initialState);
  });

})