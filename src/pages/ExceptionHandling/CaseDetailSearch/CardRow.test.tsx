import React from 'react';
import { fireEvent, screen } from '@testing-library/react';
// utils
import { renderMockStore } from 'app/test-utils/renderComponentWithMockStore';

// components
import CardRow from './CardRow';
import * as customHooks from 'app/hooks/useDesktopScreenSize';
import * as selectors from './_redux/selectors';
import { CaseSearchRecord } from '../types';
import { CASE_STATUS_MAPPING } from '../constants';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const mockCaseSearchRecord: CaseSearchRecord = {
  caseStatus: CASE_STATUS_MAPPING['20'],
  dataFieldElements: [
    {
      category: 'default',
      dataFieldElements: [
        {
          fieldName: 'BankruptcyAction',
          fieldValue: 'ADD/UPDATE'
        },
        {
          fieldName: 'BankruptcyOperationDate',
          fieldValue: '2021-08-02'
        },
        {
          fieldName: 'BankruptcyStatus',
          fieldValue: 'ACTIVE'
        },
        {
          fieldName: 'LastActionPerformed',
          fieldValue: 'BANKRUPTCY'
        }
      ]
    }
  ]
};

const mockCaseSearchRecordReadOnly: CaseSearchRecord = {
  caseStatus: '11',
  dataFieldElements: [
    {
      category: 'default',
      dataFieldElements: [
        {
          fieldName: 'BankruptcyAction',
          fieldValue: 'ADD/UPDATE'
        },
        {
          fieldName: 'BankruptcyOperationDate',
          fieldValue: '2021-08-02'
        },
        {
          fieldName: 'BankruptcyStatus',
          fieldValue: 'ACTIVE'
        },
        {
          fieldName: 'LastActionPerformed',
          fieldValue: 'BANKRUPTCY'
        }
      ]
    }
  ]
};

describe('render UI', () => {
  it('should render component', () => {
    jest.spyOn(selectors, 'selectedIsCollapsed').mockReturnValue(true);
    renderMockStore(
      <CardRow data={mockCaseSearchRecord} dataTestId={'mock-testId'} />
    );
    const openModalBtn = screen.getByTestId('mock-testId_card');
    fireEvent.click(openModalBtn);
  });

  it('should render component > caseStatus undefined', () => {
    jest.spyOn(selectors, 'selectedIsCollapsed').mockReturnValue(true);
    renderMockStore(
      <CardRow
        data={{ ...mockCaseSearchRecord, caseStatus: undefined }}
        dataTestId={'mock-testId'}
      />
    );
    const openModalBtn = screen.getByTestId('mock-testId_card');
    fireEvent.click(openModalBtn);
  });

  it('should render component when smallCardView is false', () => {
    jest.spyOn(selectors, 'selectedIsCollapsed').mockReturnValue(false);
    jest.spyOn(customHooks, 'useDesktopScreenSize').mockReturnValue(false);
    renderMockStore(
      <CardRow data={mockCaseSearchRecord} dataTestId={'mock-testId'} />
    );
    const openModalBtn = screen.getByTestId('mock-testId_card');
    fireEvent.click(openModalBtn);
  });

  it('should render component read only', () => {
    jest.spyOn(selectors, 'selectedIsCollapsed').mockReturnValue(true);
    renderMockStore(
      <CardRow data={mockCaseSearchRecordReadOnly} dataTestId={'mock-testId'} />
    );
    const openModalBtn = screen.getByTestId('mock-testId_card');
    fireEvent.click(openModalBtn);
  });
});
