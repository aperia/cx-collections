import React from 'react';
import {  renderMockStore, mockActionCreator } from 'app/test-utils';
import { screen } from '@testing-library/dom';
import { DEFAULT_CASE_FILTER, I18N_EXCEPTION } from '../constants';
import { caseSearchActions } from './_redux/reducers';
import { cspaActions } from '../CSPAList/_redux/reducers';
import { fireEvent, waitFor } from '@testing-library/react';
import CSPAList from './index';
import * as FilterForm from './FilterForm';


const initalState: Partial<RootState> = {
  cspa: {
    caseDetail: {
      id: '',
      clientId: '',
      systemId: '',
      principleId: '',
      agentId: '',
      userId: '',
      description: '',
    },
  },
  caseDetailSearch: {
    filter: DEFAULT_CASE_FILTER,
    lastFilter: DEFAULT_CASE_FILTER,
    taskError: {},
    caseDetailRecord: {
      caseStatus: "20",
      dataFieldElements: [
        {
          category: "default",
          dataFieldElements: [
            {
              fieldName: "BankruptcyAction",
              fieldValue: "ADD/UPDATE",
            }
          ]
        },
      ],
    },
  }
}
jest.mock('./SearchResult', () => () => <div>SearchResult</div>)
jest.mock('./CaseDetailModal', () => () => <div>CaseDetailModal</div>)
jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockCaseDetailSearchAction = mockActionCreator(
  caseSearchActions
);
const mockCspaActions = mockActionCreator(cspaActions);

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(<CSPAList />, {
    initialState
  });
};

describe('Test render UI', () => {

  it('should render correctly', () => {
    jest.useFakeTimers();
    renderWrapper(initalState);
    jest.runAllTimers()
    const title = screen.getByText(I18N_EXCEPTION.EXCEPTION_TITLE);
    expect(title).toBeInTheDocument();
  });

  it('should run submit action', () => {
    const spy = jest
      .spyOn(FilterForm, 'FilterForm')
      .mockImplementation(({ onSubmit }) => (
        <div>
          <button data-testid="submitBtn"
            onClick={() => onSubmit({ caseNumber: 'accountNumber' })}
          >
            run
          </button>
        </div>
      ));

    const changeFilterAction = mockCaseDetailSearchAction('changeFilter');
    const changeLastFilterAction = mockCaseDetailSearchAction('changeLastFilter');
    const caseDetailSearchAction = mockCaseDetailSearchAction('caseDetailSearch');

    jest.useFakeTimers();
    
    renderWrapper(initalState);
    
    jest.runAllTimers();
    const submitBtn = screen.getByTestId('submitBtn');
    waitFor(()=> {
      fireEvent.click(submitBtn);
    })
    expect(changeFilterAction).toBeCalled();
    expect(changeLastFilterAction).toBeCalled();
    expect(caseDetailSearchAction).toBeCalled();

    spy.mockReset();
    spy.mockRestore();
  });

  it('should not run submit action', () => {
    const spy = jest
      .spyOn(FilterForm, 'FilterForm')
      .mockImplementation(({ onSubmit }) => (
        <div>
          <button data-testid="submitBtn"
            onClick={() => onSubmit(DEFAULT_CASE_FILTER)}
          >
            run
          </button>
        </div>
      ));

    const changeFilterAction = mockCaseDetailSearchAction('changeFilter');
    const changeLastFilterAction = mockCaseDetailSearchAction('changeLastFilter');
    const caseDetailSearchAction = mockCaseDetailSearchAction('caseDetailSearch');

    jest.useFakeTimers();
    
    renderWrapper(initalState);
    
    jest.runAllTimers();
    const submitBtn = screen.getByTestId('submitBtn');
    waitFor(()=> {
      fireEvent.click(submitBtn);
    })
    expect(changeFilterAction).not.toBeCalled();
    expect(changeLastFilterAction).not.toBeCalled();
    expect(caseDetailSearchAction).not.toBeCalled();

    spy.mockReset();
    spy.mockRestore();
  });

  it('should run backToList action', () => {

    const setCaseDetailAction = mockCspaActions('setCaseDetail');

    jest.useFakeTimers();
    
    renderWrapper(initalState);

    jest.runAllTimers();

    const cspaListBtn = screen.getByText(I18N_EXCEPTION.CSPA_LIST);
    fireEvent.click(cspaListBtn);
    expect(setCaseDetailAction).toBeCalled();

  });
});