
import React from 'react';
import { screen } from '@testing-library/react';
import { renderMockStoreId } from 'app/test-utils';
import { FilterForm} from './FilterForm';

const mockOnSubmit = jest.fn();
jest.mock('./FilterFormDetail', () => () => <div>FilterFormDetail</div>);

const initialState: Partial<RootState> = {
  caseDetailSearch: { 
    filter: {},
    taskError: {},
    isMounted: true,
  }
}

const generateWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<FilterForm onSubmit={mockOnSubmit} />, { initialState });
}

describe('Test FilterForm UI', () => {
  it('should render UI', () => {
    generateWrapper(initialState);
   const FilterFormDetail = screen.getByText('FilterFormDetail');
    expect(FilterFormDetail).toBeInTheDocument();
  })
});
