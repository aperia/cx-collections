import React from 'react';
import { queryByClass, renderMockStore } from 'app/test-utils';
import { screen } from '@testing-library/dom';
import SearchResult from './SearchResult';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

const initalState: Partial<RootState> = {
  caseDetailSearch: {
    caseSearchList: [
      {
        caseNumber: '90',
        caseStatus: "20",
        dataFieldElements: [
          {
            category: "default",
            dataFieldElements: [
              {
                fieldName: "BankruptcyAction",
                fieldValue: "ADD/UPDATE",
              }
            ]
          },
        ],
      }, {
        caseNumber: '91',
        caseStatus: "29",
        dataFieldElements: [
          {
            category: "default",
            dataFieldElements: [
              {
                fieldName: "BankruptcyAction",
                fieldValue: "ADD/UPDATE",
              },
            ]
          },
        ],
      },
      {},
      {},
      {},
      {},
      {},
      {},
      {},
      {},
      {}
    ],
    taskError: {},
    loading: false,
    error: false,
  }
}

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(<SearchResult />, {
    initialState
  });
};

describe('Test render SearchResult', () => {
  it('should render correctly', () => {
    renderWrapper(initalState);
    expect(screen.getByText(`${initalState.caseDetailSearch?.caseSearchList?.length} ${I18N_COMMON_TEXT.RESULT_FOUND_PLURAL}`)).toBeInTheDocument();
  });

  it('should render with no data', () => {
    const caseListNotFound = {
      caseDetailSearch: {
        caseSearchList: [],
        taskError: {},
      }
    }
    renderWrapper(caseListNotFound);
    expect(screen.getByText(I18N_COMMON_TEXT.NO_DATA)).toBeInTheDocument();
  });

  it('should render with 1 data', () => {
    const caseListOneData = {
      caseDetailSearch: {
        caseSearchList: [
          {
            caseNumber: '90',
            caseStatus: "20",
            dataFieldElements: [
              {
                category: "default",
                dataFieldElements: [
                  {
                    fieldName: "BankruptcyAction",
                    fieldValue: "ADD/UPDATE",
                  }
                ]
              },
            ],
          }
        ],
        taskError: {},
      }
    }
    renderWrapper(caseListOneData);
    expect(screen.getByText(`${caseListOneData.caseDetailSearch?.caseSearchList?.length} ${I18N_COMMON_TEXT.RESULT_FOUND}`)).toBeInTheDocument();
  });

  it('should render with 1 data without status', () => {
    const caseListOneData = {
      caseDetailSearch: {
        caseSearchList: [
          {
            caseNumber: '95',
            caseStatus: undefined,
            dataFieldElements: [
              {
                category: "default",
                dataFieldElements: [
                  {
                    fieldName: "BankruptcyAction",
                    fieldValue: "ADD/UPDATE",
                  }
                ]
              },
            ],
          }
        ],
        taskError: {},
      }
    }
    renderWrapper(caseListOneData);
    expect(screen.getByText(`${caseListOneData.caseDetailSearch?.caseSearchList?.length} ${I18N_COMMON_TEXT.RESULT_FOUND}`)).toBeInTheDocument();
  });

  it('should render loading', () => {
    const caseListLoading = {
      caseDetailSearch: {
        caseSearchList: [
        ],
        taskError: {},
        loading: true,
      }
    }
    const { baseElement } = renderWrapper(caseListLoading);
    const loadingElement = queryByClass(baseElement, /loading/);
    expect(loadingElement).toBeInTheDocument();
  });
});