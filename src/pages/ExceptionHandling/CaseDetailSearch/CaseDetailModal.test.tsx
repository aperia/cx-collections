import React from 'react';
import { fireEvent, screen } from '@testing-library/react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { I18N_EXCEPTION } from '../constants';
import CaseDetailModal from './CaseDetailModal';
import { caseSearchActions } from './_redux/reducers'
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { CaseTaskError } from '../types';

const initialState: Partial<RootState> = {
  caseDetailSearch: {
    caseDetailRecord: {
      promiseType: 'promiseType'
    },
    taskError: {
      loading: false,
      taskErrorList: [{
        requestFromCallingApplication: "value",
        requestedService: 'value',
        retryCount: 9,
        errorDate: "errorDate",
        callingApplication: "callingApplication",
      }],
    }
  }
};

const mockActions = mockActionCreator(caseSearchActions);

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <CaseDetailModal />,
    {
      initialState
    }
  );
}

describe('should render CaseDetailModal', () => {

  it('It should render UI', () => {
    renderWrapper(initialState);
    const modalTitle = screen.getByText(I18N_EXCEPTION.CASE_DETAIL)
    expect(modalTitle).toBeInTheDocument()
  });

  it('Should render UI with caseDetailRecord', () => {
    const statesWithDetailRecord: Partial<RootState> = {
      caseDetailSearch: {
        caseDetailRecord: {
          caseStatus: "20",
          dataFieldElements: [
            {
              category: "default",
              dataFieldElements: [
                {
                  fieldName: "BankruptcyAction",
                  fieldValue: "ADD/UPDATE",
                },
              ]
            },
          ],
        },
        taskError: {
          taskErrorList: [{
            requestFromCallingApplication: "",
            requestedService: 'value',
            retryCount: 9,
            errorDate: "errorDate",
            callingApplication: "callingApplication"
          }],
        },
      }
    };
    mockActions('secureArchiveById')([{
      requestFromCallingApplication: "value",
      requestedService: 'value',
      retryCount: 9,
      errorDate: "errorDate",
      callingApplication: "callingApplication"
    }])
    renderWrapper(statesWithDetailRecord);
  });

  it('It should render UI with loading', () => {
    const statesLoading: Partial<RootState> = {
      caseDetailSearch: {
        taskError: {
          loading: true,
        }
      }
    };

    const { baseElement } = renderWrapper(statesLoading);
    const loadingEle = queryByClass(baseElement, /loading/);
    expect(loadingEle).toBeInTheDocument()

  });

  it('It should render UI with no data', () => {
    const statesLoading: Partial<RootState> = {
      caseDetailSearch: {
        taskError: {
          loading: false,
          taskErrorList: [],
        }
      }
    };

    renderWrapper(statesLoading);
    const noData = screen.getByText('txt_no_data');
    expect(noData).toBeInTheDocument()
  });

  it('Test handleClose action', () => {
    const closeModalAction = mockActions('closeCaseDetailModal')
    renderWrapper(initialState);
    const btnClose = screen.getByText('txt_close');
    fireEvent.click(btnClose);
    expect(closeModalAction).toBeCalled()
  });

  it('Test handleReprocess action', () => {

    const taskErrorPayload = [{
      requestFromCallingApplication: "",
      requestedService: 'value',
      retryCount: 9,
      errorDate: "",
      callingApplication: "callingApplication",
    }] as CaseTaskError[];

    const statesWithDetailRecord: Partial<RootState> = {
      caseDetailSearch: {
        caseDetailRecord: {
          caseStatus: "20",
          promiseType: 'promiseType',
          dataFieldElements: [
            {
              category: "default",
              dataFieldElements: [
                {
                  fieldName: "BankruptcyAction",
                  fieldValue: "ADD/UPDATE",
                }
              ]
            },
          ],
        },
        taskError: {
          taskErrorList: taskErrorPayload,
        },
      }
    };

    const confirmCallbackAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });

    jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(confirmCallbackAction as never);

    mockActions('secureArchiveById')(taskErrorPayload);

    const triggerReprocessCaseAction = mockActions('triggerReprocessCase');

    renderWrapper(statesWithDetailRecord);
    const btnReProcess = screen.getByText(I18N_EXCEPTION.REPROCESS);
    fireEvent.click(btnReProcess);
    expect(triggerReprocessCaseAction).toBeCalled();

  });

  it('Test handleReprocess action with requestFromCallingApplication is empty', () => {
    const taskErrorPayload = [
      {
        requestFromCallingApplication: '',
        requestedService: '',
        retryCount: 9,
        errorDate: '',
        callingApplication: 'callingApplication'
      }
    ] as CaseTaskError[];

    const statesWithDetailRecord: Partial<RootState> = {
      caseDetailSearch: {
        caseDetailRecord: {
          caseStatus: '20',
          promiseType: 'promiseType',
          dataFieldElements: [
            {
              category: 'default',
              dataFieldElements: [
                {
                  fieldName: 'BankruptcyAction',
                  fieldValue: 'ADD/UPDATE'
                }
              ]
            }
          ]
        },
        taskError: {
          taskErrorList: taskErrorPayload
        }
      }
    };

    const confirmCallbackAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });

    jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(confirmCallbackAction as never);

    mockActions('secureArchiveById')(taskErrorPayload);
    const triggerReprocessCaseAction = mockActions('triggerReprocessCase');

    renderWrapper(statesWithDetailRecord);
    const btnReProcess = screen.getByText(I18N_EXCEPTION.REPROCESS);
    fireEvent.click(btnReProcess);
    expect(triggerReprocessCaseAction).toBeCalled();
  });
});