import React from 'react';
import { screen } from '@testing-library/dom';
import { fireEvent, act } from '@testing-library/react';
import FilterFormDetail from './FilterFormDetail';

import { FormikProps } from 'formik';

import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { CaseDetailFilter } from '../types';
import {
  DEFAULT_CASE_FILTER,
  FILTER_TYPE_VALUE,
  I18N_EXCEPTION
} from '../constants';
import { caseSearchActions } from './_redux/reducers';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { queryByClass } from 'app/_libraries/_dls/test-utils';

const mockCaseSearchActions = mockActionCreator(caseSearchActions);
const mockFilterFormProps = {
  values: {},
  isValid: true,
  setValues: value => {},
  submitForm: () => {},
  handleChange: (value: any) => {},
  validateForm: value => {},
  resetForm: value => {}
} as FormikProps<CaseDetailFilter>;

const initialState: Partial<RootState> = {
  caseDetailSearch: {
    caseStatuses: [
      {
        fieldID: 'fieldID',
        fieldValue: 'fieldValue',
        description: 'description'
      }
    ],
    isCollapsed: true,
    taskError: {}
  }
};

HTMLCanvasElement.prototype.getContext = jest.fn();

const renderWrapper = (
  initialState: Partial<RootState>,
  formikProps?: FormikProps<CaseDetailFilter>
) => {
  return renderMockStore(
    <FilterFormDetail {...{ ...mockFilterFormProps, ...formikProps }} />,
    {
      initialState
    }
  );
};

describe('should test filter form detail', () => {
  it('it should render UI', () => {
    jest.useFakeTimers();

    renderWrapper(initialState);

    jest.runAllTimers();
    const caseNumberText = screen.getByText(I18N_EXCEPTION.CASE_NUMBER);
    expect(caseNumberText).toBeInTheDocument();
  });
});

describe('Test action', () => {
  it('should test toggle button', () => {
    const { wrapper } = renderWrapper(initialState);
    const mockToggleCollapse = mockCaseSearchActions(
      'setToggleSettingsSection'
    );

    const button = queryByClass(wrapper.container, 'collapse-button')!;
    button.click();
    expect(mockToggleCollapse).toBeCalledWith(
      !initialState.caseDetailSearch?.isCollapsed
    );
  });

  it('should test reset action', () => {
    renderWrapper(initialState);

    const mockChangeFilter = mockCaseSearchActions('changeFilter');
    const mockResetToDefault = mockCaseSearchActions('resetToDefault');

    const resetBtn = screen.getByText(I18N_COMMON_TEXT.RESET_TO_DEFAULT);
    act(() => {
      fireEvent.click(resetBtn);
    });

    expect(mockChangeFilter).toBeCalledWith(DEFAULT_CASE_FILTER);
    expect(mockResetToDefault).toBeCalled();
  });

  it('should test handleChange with case number', () => {
    renderWrapper(initialState);
    const caseNumberRadio = screen.getByText(I18N_EXCEPTION.CASE_NUMBER);
    fireEvent.click(caseNumberRadio);
    const caseNumberTextbox = screen.getByLabelText(I18N_EXCEPTION.CASE_NUMBER);
    expect(caseNumberTextbox).toBeInTheDocument();
  });

  it('should show case number textbox', () => {
    const formikProps = {
      values: {
        filterType: FILTER_TYPE_VALUE.CASE_NUMBER,
        updateDateRanger: {
          start: new Date(),
          end: new Date()
        }
      }
    } as FormikProps<CaseDetailFilter>;
    renderWrapper(initialState, formikProps);

    const caseNumberTextbox = screen.getByLabelText(I18N_EXCEPTION.CASE_NUMBER);
    expect(caseNumberTextbox).toBeInTheDocument();
  });
  it('should show case number textbox with error', () => {
    const formikProps = {
      values: {
        filterType: FILTER_TYPE_VALUE.CASE_NUMBER,
        updateDateRanger: {
          start: new Date(),
          end: new Date()
        }
      },
      touched: { caseNumber: true },
      errors: { caseNumber: 'error' }
    } as FormikProps<CaseDetailFilter>;

    renderWrapper(initialState, formikProps);

    const caseNumberTextbox = screen.getByLabelText(I18N_EXCEPTION.CASE_NUMBER);
    expect(caseNumberTextbox).toBeInTheDocument();
  });

  it('should test handleChange with account number', () => {
    renderWrapper(initialState);
    const caseAccountNumberRadio = screen.getByText(
      I18N_COMMON_TEXT.ACCOUNT_NUMBER
    );
    fireEvent.click(caseAccountNumberRadio);
    const caseAccountTextbox = screen.getByLabelText(
      I18N_COMMON_TEXT.ACCOUNT_NUMBER
    );
    expect(caseAccountTextbox).toBeInTheDocument();
  });

  it('should show account number textbox', () => {
    const formikProps = {
      values: {
        filterType: FILTER_TYPE_VALUE.ACCOUNT_NUMBER
      }
    } as FormikProps<CaseDetailFilter>;

    const { baseElement } = renderWrapper(initialState, formikProps);
    const input: HTMLInputElement | null = baseElement.querySelector(
      '.dls-text-box > input'
    );

    act(() => {
      fireEvent.change(input!, { target: { value: 1 } });
    });

    expect(input!).toBeInTheDocument();
  });

  it('should show account number textbox when value is not numeric', () => {
    const formikProps = {
      values: {
        filterType: FILTER_TYPE_VALUE.ACCOUNT_NUMBER
      }
    } as FormikProps<CaseDetailFilter>;

    const { baseElement } = renderWrapper(initialState, formikProps);

    const input: HTMLInputElement | null = baseElement.querySelector(
      '.dls-text-box > input'
    );

    act(() => {
      fireEvent.change(input!, { target: { value: 'abc' } });
    });

    expect(input!).toBeInTheDocument();
  });

  it('should show account number textbox with error', () => {
    const formikProps = {
      values: {
        filterType: FILTER_TYPE_VALUE.ACCOUNT_NUMBER
      },
      touched: {
        accountNumber: true
      },
      errors: {
        accountNumber: 'error'
      }
    } as FormikProps<CaseDetailFilter>;
    renderWrapper(initialState, formikProps);

    const caseAccountTextbox = screen.getByLabelText(
      I18N_COMMON_TEXT.ACCOUNT_NUMBER
    );
    expect(caseAccountTextbox).toBeInTheDocument();
  });

  it('should test handleChange with case status and range', () => {
    const formikProps = {
      touched: { updateDateRanger: true }
    } as FormikProps<CaseDetailFilter>;

    renderWrapper(initialState, formikProps);
    const dateRangeRadio = screen.getByText(
      I18N_EXCEPTION.CASE_STATUS_AND_DATE_RANGE
    );
    act(() => {
      fireEvent.click(dateRangeRadio);
    });

    const caseStatusTextbox = screen.getByLabelText(
      I18N_EXCEPTION.CASE_STATUS_AND_DATE_RANGE
    );
    expect(caseStatusTextbox).toBeInTheDocument();
  });

  it('it should show case status and range', () => {
    const formikProps = {
      values: {
        filterType: FILTER_TYPE_VALUE.CASE_STATUS_AND_UPDATE_DATE,
        updateDateRanger: {
          start: new Date(),
          end: new Date()
        }
      }
    } as FormikProps<CaseDetailFilter>;

    renderWrapper(initialState, formikProps);
    const caseStatusTextbox = screen.getByLabelText(
      I18N_EXCEPTION.CASE_STATUS_AND_DATE_RANGE
    );

    expect(caseStatusTextbox).toBeInTheDocument();
  });

  it('it should show case status and range with start error', () => {
    const formikProps = {
      touched: { updateDateRanger: true },
      errors: {
        updateDateRanger: {
          start: 'error'
        } as any
      },
      values: {
        filterType: FILTER_TYPE_VALUE.CASE_STATUS_AND_UPDATE_DATE
      }
    } as FormikProps<CaseDetailFilter>;

    renderWrapper(initialState, formikProps);

    const caseStatusTextbox = screen.getByLabelText(
      I18N_EXCEPTION.CASE_STATUS_AND_DATE_RANGE
    );
    expect(caseStatusTextbox).toBeInTheDocument();
  });

  it('it should show case status and date range with end error ', () => {
    const formikProps = {
      touched: { updateDateRanger: true },
      errors: {
        updateDateRanger: {
          start: undefined,
          end: 'error'
        } as any
      },
      values: {
        filterType: FILTER_TYPE_VALUE.CASE_STATUS_AND_UPDATE_DATE
      }
    } as FormikProps<CaseDetailFilter>;

    renderWrapper(initialState, formikProps);
    const caseStatusTextbox = screen.getByText('txt_update_date_range');
    expect(caseStatusTextbox).toBeInTheDocument();
  });

  it('it should show case status and date range with undefined ', () => {
    const formikProps = {
      touched: { updateDateRanger: true },
      errors: {
        updateDateRanger: {
          start: undefined,
          end: undefined
        } as any
      },
      values: {
        filterType: FILTER_TYPE_VALUE.CASE_STATUS_AND_UPDATE_DATE
      }
    } as FormikProps<CaseDetailFilter>;

    renderWrapper(initialState, formikProps);
    const caseStatusTextbox = screen.getByText('txt_update_date_range');
    expect(caseStatusTextbox).toBeInTheDocument();
  });

  it('it should show combobox', () => {
    const formikProps = {
      touched: { caseStatus: true },
      values: {
        filterType: FILTER_TYPE_VALUE.CASE_STATUS_AND_UPDATE_DATE,
        caseStatus: {
          fieldID: 'fieldID',
          fieldValue: 'fieldValue',
          description: 'description'
        }
      }
    } as FormikProps<CaseDetailFilter>;

    renderWrapper(initialState, formikProps);
    const caseStatusTextbox = screen.getByText(I18N_EXCEPTION.CASE_STATUS);
    expect(caseStatusTextbox).toBeInTheDocument();
  });

  it('it should show cobobox with error', () => {
    const formikProps = {
      touched: { caseStatus: true },
      errors: {
        caseStatus: 'error'
      },
      values: {
        filterType: FILTER_TYPE_VALUE.CASE_STATUS_AND_UPDATE_DATE,
        caseStatus: {
          fieldID: 'fieldID',
          fieldValue: 'fieldValue',
          description: 'description'
        }
      }
    } as FormikProps<CaseDetailFilter>;
    renderWrapper(initialState, formikProps);
    const caseStatusTextbox = screen.getByText(I18N_EXCEPTION.CASE_STATUS);
    expect(caseStatusTextbox).toBeInTheDocument();
  });

  it('test submit action', () => {
    renderWrapper(initialState);
    const caseNumberRadio = screen.getByText(I18N_EXCEPTION.CASE_NUMBER);
    act(() => {
      fireEvent.click(caseNumberRadio);
    });
    const submitBtn = screen.getByText(I18N_COMMON_TEXT.APPLY);
    act(() => {
      fireEvent.click(submitBtn);
    });
  });
});
