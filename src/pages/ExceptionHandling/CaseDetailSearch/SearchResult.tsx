import { NoDataGrid } from 'app/components';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { usePagination } from 'app/hooks';
import { InlineMessage, Pagination } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import React from 'react';
import { useSelector } from 'react-redux';
import { I18N_EXCEPTION } from '../constants';
import { getCaseStatusWarningMessage } from '../helpers';
import { CaseSearchRecord } from '../types';
import CardRow from './CardRow';
import {
  selectedCaseSearchList,
  selectedError,
  selectedLoading
} from './_redux/selectors';

const SearchResult: React.FC = () => {
  const { t } = useTranslation();

  const caseList = useSelector(selectedCaseSearchList);
  const loading = useSelector(selectedLoading);
  const error = useSelector(selectedError);
  const testId = 'exceptionHandling-caseDetailSearch-searchResult';

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(caseList);

  const showPagination = total > pageSize[0];
  const warningMessage =
    caseList?.length > 0 &&
    getCaseStatusWarningMessage(caseList[0].caseStatus || '');

  if ((caseList?.length === 0 && !loading) || error)
    return (
      <div>
        <NoDataGrid text="txt_no_data"></NoDataGrid>
      </div>
    );

  if (loading)
    return (
      <div className="mt-40 pt-40">
        <div className="loading mt-30" />
      </div>
    );

  return (
    <div className={`card-primary px-24 pb-24 pt-16 max-width-lg mx-auto`}>
      <h5 className="mb-16" data-testid={genAmtId(testId, 'title', '')}>
        {t(I18N_EXCEPTION.CASE_LIST)}
      </h5>
      {warningMessage && (
        <InlineMessage
          variant="warning"
          dataTestId={genAmtId(testId, 'warning', '')}
        >
          {t(warningMessage, { caseStatus: caseList[0].caseStatus })}
        </InlineMessage>
      )}
      <p className="mb-20" data-testid={genAmtId(testId, 'result-found', '')}>
        {`${caseList.length} ${t(
          caseList.length === 1
            ? I18N_COMMON_TEXT.RESULT_FOUND
            : I18N_COMMON_TEXT.RESULT_FOUND_PLURAL
        )}`}
      </p>
      {gridData?.map((c: CaseSearchRecord, index: number) => (
        <CardRow
          key={c.caseNumber}
          data={c}
          dataTestId={genAmtId('case-search-result', `${index}_item`, '')}
        />
      ))}
      {showPagination && (
        <div className="pt-16">
          <Pagination
            totalItem={total}
            pageSize={pageSize}
            pageNumber={currentPage}
            pageSizeValue={currentPageSize}
            compact
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
            dataTestId={genAmtId(testId, 'pagination', '')}
          />
        </div>
      )}
    </div>
  );
};

export default SearchResult;
