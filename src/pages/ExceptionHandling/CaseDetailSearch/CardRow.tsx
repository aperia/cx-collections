import { useDesktopScreenSize } from 'app/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { View } from 'app/_libraries/_dof/core';
import classNames from 'classnames';
import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getCaseStatusWarningMessage } from '../helpers';
import { CaseSearchRecord } from '../types';
import { caseSearchActions } from './_redux/reducers';
import { selectedIsCollapsed } from './_redux/selectors';

export interface CardRowProps {
  data: CaseSearchRecord;
  dataTestId?: string;
}

const CardRow: React.FC<CardRowProps> = ({ data, dataTestId }) => {
  const dispatch = useDispatch();

  const isDesktop = useDesktopScreenSize();

  const isCollasped = useSelector(selectedIsCollapsed);

  const smallCardView = !isDesktop && !isCollasped;

  const isReadOnly = !!getCaseStatusWarningMessage(data.caseStatus || '');

  const handleCardClicked = useCallback(() => {
    !isReadOnly && dispatch(caseSearchActions.openCaseDetailModal(data));
  }, [dispatch, data, isReadOnly]);

  return (
    <div
      className={classNames(
        'card-item p-16',
        isReadOnly && 'bg-light-l16 cursor-unset'
      )}
      data-testid={genAmtId(dataTestId, 'card', '')}
      onClick={handleCardClicked}
    >
      <p
        className="fs-14 fw-500 color-grey-d20"
        data-testid={genAmtId(dataTestId, 'caseNumber', '')}
      >
        {data.caseNumber}
      </p>
      <View
        id={`case_search_result_views-id-${data.caseNumber}`}
        formKey={`case_search_result_views-id-${data.caseNumber}_view`}
        descriptor={
          smallCardView
            ? 'case_search_result_views--small'
            : 'case_search_result_views'
        }
        value={data}
        dataTestId={genAmtId(dataTestId, 'view', '')}
      />
    </div>
  );
};

export default CardRow;
