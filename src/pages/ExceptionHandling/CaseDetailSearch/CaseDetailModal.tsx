import { NoDataGrid } from 'app/components';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { formatCommon } from 'app/helpers';
import {
  Button,
  ColumnType,
  Grid,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TruncateText
} from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { View } from 'app/_libraries/_dof/core';
import { useApiErrorNotification } from 'pages/ApiErrorNotification/hooks/useApiErrorNotification';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import React, { useCallback, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { EXCEPTION_PAGE_STORE_ID, I18N_EXCEPTION } from '../constants';
import { CaseTaskError } from '../types';
import { caseSearchActions } from './_redux/reducers';
import {
  selectedCaseDetailRecord,
  selectedTaskErrorList,
  selectedTaskErrorLoading
} from './_redux/selectors';

const CaseDetailModal: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const caseDetailRecord = useSelector(selectedCaseDetailRecord);
  const data = useMemo(() => {
    return caseDetailRecord
      ? {
          caseNumber: caseDetailRecord.caseNumber,
          accountNumber: caseDetailRecord.accountNumber,
          caseStatus: caseDetailRecord.caseStatus,
          updatedDate: caseDetailRecord.updatedDate,
          operatorId: caseDetailRecord.operatorId,
          promiseType: caseDetailRecord.promiseType?.toLowerCase()
        }
      : {};
  }, [caseDetailRecord]);

  const loading = useSelector(selectedTaskErrorLoading);
  const taskErrorList = useSelector(selectedTaskErrorList);
  const testId = 'exceptionHandling-caseDetailModal';

  const handleClose = useCallback(() => {
    dispatch(caseSearchActions.closeCaseDetailModal());
  }, [dispatch]);

  const handleReprocess = useCallback(
    (
      caseId: string,
      requestedService?: string,
      requestFromCallingApplication?: string,
      caseStatus?: string
    ) => {
      dispatch(
        confirmModalActions.open({
          title: I18N_EXCEPTION.CONFIRM_REPROCESS_TITLE,
          body: (
            <>
              <p className="mb-16">
                {t(I18N_EXCEPTION.CONFIRM_REPROCESS_MESSAGE)}
              </p>
              <p className="word-break-all">
                {t(I18N_EXCEPTION.FAILED_TASK)}:{' '}
                <strong>{requestedService || ''}</strong>
              </p>
            </>
          ),
          cancelText: I18N_COMMON_TEXT.CANCEL,
          confirmText: I18N_EXCEPTION.REPROCESS,
          confirmCallback: () => {
            dispatch(
              caseSearchActions.triggerReprocessCase({
                caseId,
                requestType: requestFromCallingApplication,
                caseStatus
              })
            );
          },
          variant: 'primary'
        })
      );
    },
    [dispatch, t]
  );

  const TASK_LIST_COLUMNS: ColumnType[] = useMemo(
    () => [
      {
        id: 'requestedService',
        Header: t(I18N_EXCEPTION.FAILED_TASK),
        width: 331,
        accessor: (data: CaseTaskError) => (
          <div className="text-break">{data.requestedService}</div>
        )
      },
      {
        id: 'errorCode',
        Header: t(I18N_EXCEPTION.ERROR_CODE),
        width: 182,
        accessor: 'errorCode'
      },
      {
        id: 'errorDate',
        Header: t(I18N_EXCEPTION.ERROR_DATE),
        width: 121,
        accessor: (data: CaseTaskError) => (
          <div>{formatCommon(data.errorDate || '').time.date}</div>
        )
      },
      {
        id: 'errorMessage',
        Header: t(I18N_EXCEPTION.ERROR_MESSAGE),
        width: 333,
        autoWidth: true,
        accessor: (data: CaseTaskError, index: number) => (
          <TruncateText
            lines={2}
            title={data?.errorMessage}
            resizable
            ellipsisMoreText={t(I18N_COMMON_TEXT.MORE)}
            ellipsisLessText={t(I18N_COMMON_TEXT.LESS)}
            className="text-break"
            dataTestId={genAmtId(testId, `${index}-errorMessage`, '')}
          >
            {data.errorMessage}
          </TruncateText>
        )
      },
      {
        id: 'retryCount',
        Header: t(I18N_EXCEPTION.NUMBER_OF_RETRY),
        cellProps: { className: 'text-right' },
        width: 149,
        accessor: (data: CaseTaskError) => (
          <div className="text-right">{data.retryCount}</div>
        )
      },
      {
        id: 'action',
        Header: <div className="text-center">{t(I18N_COMMON_TEXT.ACTION)}</div>,
        isFixedRight: true,
        width: 110,
        cellBodyProps: { className: 'text-center td-sm' },
        accessor: (data: CaseTaskError, index: number) => (
          <Button
            variant="outline-primary"
            size="sm"
            onClick={() =>
              handleReprocess(
                caseDetailRecord?.caseNumber || '',
                data.requestedService,
                data.requestFromCallingApplication,
                caseDetailRecord?.caseStatus
              )
            }
            dataTestId={genAmtId(testId, `${index}-action`, '')}
          >
            {t(I18N_EXCEPTION.REPROCESS)}
          </Button>
        )
      }
    ],
    [t, handleReprocess, caseDetailRecord]
  );

  useEffect(() => {
    caseDetailRecord &&
      dispatch(
        caseSearchActions.secureArchiveById({
          caseId: caseDetailRecord.caseNumber || '',
          archiveIds: caseDetailRecord.archiveIds || []
        })
      );
    return () => {
      dispatch(caseSearchActions.cleanupTaskError());
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId: EXCEPTION_PAGE_STORE_ID,
          forSection: 'inModalBody'
        })
      );
    };
  }, [dispatch, caseDetailRecord]);

  const ApiErrorDetail = useApiErrorNotification({
    storeId: EXCEPTION_PAGE_STORE_ID,
    forSection: 'inModalBody'
  });

  return (
    <>
      <Modal full show enforceFocus={false} dataTestId={testId}>
        <ModalHeader
          border
          closeButton
          onHide={handleClose}
          dataTestId={genAmtId(testId, 'header', '')}
        >
          <ModalTitle
            className="d-flex align-items-center"
            dataTestId={genAmtId(testId, 'title', '')}
          >
            {t(I18N_EXCEPTION.CASE_DETAIL)}
          </ModalTitle>
        </ModalHeader>
        <ModalBody noPadding>
          <div className="bg-light-l20 border-bottom">
            <div className="max-width-lg mx-auto px-24 pb-24 pt-8">
              <View
                id={`case-record-detail-id-${data?.accountNumber}`}
                formKey={`case-record-detail-id-id-${data?.accountNumber}_view`}
                descriptor={
                  data?.promiseType
                    ? 'case_detail_record_views--with_promise_type'
                    : 'case_detail_record_views'
                }
                value={data}
              />
            </div>
          </div>
          <div className="max-width-lg mx-auto p-24">
            <ApiErrorDetail
              className="mb-16"
              dataTestId={genAmtId(testId, 'api-error', '')}
            />
            <h5>{t(I18N_EXCEPTION.TASK_ERROR_DETAILS)}</h5>
            {loading ? (
              <div className="mt-40 pt-40">
                <div className="loading mt-30"></div>
              </div>
            ) : taskErrorList.length === 0 ? (
              <NoDataGrid
                text="txt_no_data"
                dataTestId={genAmtId(testId, 'no-data', '')}
              ></NoDataGrid>
            ) : (
              <Grid
                className="mt-16"
                columns={TASK_LIST_COLUMNS}
                data={taskErrorList}
                dataTestId={genAmtId(testId, 'grid', '')}
              />
            )}
          </div>
        </ModalBody>
        <ModalFooter
          border
          cancelButtonText={t(I18N_COMMON_TEXT.CLOSE)}
          onCancel={handleClose}
          dataTestId={genAmtId(testId, 'footer', '')}
        />
      </Modal>
    </>
  );
};

export default CaseDetailModal;
