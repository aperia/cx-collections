import { Formik } from 'formik';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { DEFAULT_CASE_FILTER } from '../constants';
import { caseSeachFormValidationSchema } from '../helpers';
import { CaseDetailFilter } from '../types';
import FilterFormDetail from './FilterFormDetail';
import { selectedFilter, selectedIsMounted } from './_redux/selectors';

interface FilterFormProps {
  onSubmit: (updatedValues: CaseDetailFilter) => void;
}

export const FilterForm: React.FC<FilterFormProps> = ({ onSubmit }) => {
  const filters = useSelector(selectedFilter);
  const isMounted = useSelector(selectedIsMounted);

  const [formFilter, setFormFilter] =
    useState<CaseDetailFilter>(DEFAULT_CASE_FILTER);

  useEffect(() => {
    setFormFilter(filters);
  }, [filters]);

  return (
    <Formik
      initialValues={formFilter}
      validationSchema={caseSeachFormValidationSchema}
      validateOnChange={false}
      validateOnBlur={true}
      validateOnMount={!isMounted}
      onSubmit={onSubmit}
    >
      {props => <FilterFormDetail {...props} />}
    </Formik>
  );
};
