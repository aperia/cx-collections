import datetime from 'date-and-time';
import { FormatTime } from 'app/constants/enums';
import { getStartOfDay } from 'app/helpers';
import { apiService } from 'app/utils/api.service';
import { GetClientConfigurationRequest } from 'pages/ClientConfiguration/types';
import { CODE_ALL } from './helpers';
import {
  CaseDetailFilter,
  CaseSearchRequest,
  CSPAData,
  ReprocessCaseRequest,
  SecureArchiveRequest
} from './types';

const FETCH_SIZE = '10000';

export const exceptionHandlingServices = {
  getCSPAList(requestBody: GetClientConfigurationRequest) {
    const url = window.appConfig?.api?.clientConfig?.clientInfoGetList || '';
    return apiService.post(url, requestBody);
  },
  getCaseStatuses() {
    const url = window.appConfig?.api?.exceptionHandling.getCaseStatuses || '';
    return apiService.get(url);
  },
  caseDetailsSearch(common: CSPAData, filter: CaseDetailFilter) {
    const requestBody: CaseSearchRequest = {
      common: {
        agent: common.agentId || CODE_ALL,
        clientNumber: common.clientId || CODE_ALL,
        system: common.systemId || CODE_ALL,
        prin: common.principleId || CODE_ALL,
        user: common.userId || CODE_ALL,
        app: 'cx',
        org: window.appConfig?.commonConfig?.org
      },
      excludeCloseCases: false
    };

    switch (filter.filterType) {
      case 'accountNumber':
        requestBody.customer = filter.accountNumber;
        break;
      case 'caseNumber':
        requestBody.caseId = filter.caseNumber;
        break;
      case 'caseStatusAndUpdateDate':
        requestBody.caseStatus = filter.caseStatus?.fieldID;
        requestBody.updateDateFrom = getStartOfDay(
          filter.updateDateRanger?.start,
          FormatTime.UTCDate
        );
        requestBody.updateDateTo = getStartOfDay(
          datetime.addDays(filter.updateDateRanger?.end as Date, 1),
          FormatTime.UTCDate
        );
        requestBody.fetchSize = FETCH_SIZE;
        break;
    }

    const url =
      window.appConfig?.api?.exceptionHandling.caseDetailsSearch || '';
    return apiService.post(url, requestBody);
  },
  secureArchiveRetrievalByCaseId(
    common: CSPAData,
    caseId: string,
    archiveId: string
  ) {
    const requestBody: SecureArchiveRequest = {
      common: {
        agent: common.agentId || CODE_ALL,
        clientNumber: common.clientId || CODE_ALL,
        system: common.systemId || CODE_ALL,
        prin: common.principleId || CODE_ALL,
        user: common.userId || CODE_ALL,
        app: 'cx',
        org: window.appConfig?.commonConfig?.org
      },
      caseId,
      requestType: 'Response',
      archiveId,
      bypassCaseIdCheck: true
    };
    const url =
      window.appConfig?.api?.exceptionHandling.secureArchiveRetrievalByCaseId ||
      '';
    return apiService.post(url, requestBody);
  },

  reprocessCase(request: ReprocessCaseRequest) {
    const url = window.appConfig?.api?.exceptionHandling.reprocessCase || '';
    return apiService.post(url, request);
  }
};
