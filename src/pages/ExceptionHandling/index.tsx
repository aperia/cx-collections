import React from 'react';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Constants
import { I18N_EXCEPTION } from './constants';

// Redux
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';
import { useDispatch } from 'react-redux';
import { genAmtId } from 'app/_libraries/_dls/utils';

const ExceptionHandlingNavigation: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const handleExceptionTab = () => {
    dispatch(
      actionsTab.addTab({
        id: 'exception-handling',
        title: I18N_EXCEPTION.EXCEPTION_TITLE,
        storeId: 'exception-handling',
        tabType: 'exceptionHandling',
        iconName: 'file'
      })
    );
  };

  return (
    <span
      className="link-header"
      onClick={handleExceptionTab}
      data-testid={genAmtId('header_exception', 'title', '')}
    >
      {t(I18N_EXCEPTION.EXCEPTION_TITLE)}
    </span>
  );
};

export default ExceptionHandlingNavigation;
