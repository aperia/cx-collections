import { SORT_TYPE } from 'app/constants';
import {
  CaseTaskError,
  CSPAData,
  FieldElement,
  ReprocessCaseArgs
} from './types';
import * as Yup from 'yup';
import {
  CASE_STATUS_MAPPING,
  FILTER_TYPE_VALUE,
  FORM_NAMES,
  I18N_EXCEPTION
} from './constants';
import get from 'lodash.get';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

export const CODE_ALL = '$ALL';

export const filterCSPAList = (data: CSPAData[], searchValue: string) => {
  return data.filter(
    item =>
      item?.agentId?.toLowerCase().includes(searchValue) ||
      item?.clientId?.toLowerCase().includes(searchValue) ||
      item?.systemId?.toLowerCase().includes(searchValue) ||
      item?.principleId?.toLowerCase().includes(searchValue) ||
      item?.description?.toLowerCase().includes(searchValue)
  );
};

export const sortCSPAList = (data: CSPAData[], sortBy: MagicKeyValue) => {
  const sortCallback = (a: CSPAData, b: CSPAData) => {
    if (isDefaultCSPA(a)) {
      return -1;
    }
    if (isDefaultCSPA(b)) {
      return 1;
    }
    const valueToCompareA = a[sortBy.id as keyof CSPAData] || '';
    const valueToCompareB = b[sortBy.id as keyof CSPAData] || '';
    if (sortBy.order === SORT_TYPE.DESC) {
      return valueToCompareB > valueToCompareA ? 1 : -1;
    }

    return valueToCompareA > valueToCompareB ? 1 : -1;
  };

  data.sort(sortCallback);

  return data;
};

export const isDefaultCSPA = (cspa: CSPAData) => {
  return (
    cspa.systemId === CODE_ALL &&
    cspa.principleId === CODE_ALL &&
    cspa.agentId === CODE_ALL
  );
};

export const convertCSPAId = (
  clientId: string,
  agentId: string,
  systemId: string,
  principleId: string
) => {
  return `${clientId} - ${systemId} - ${principleId} - ${agentId}`;
};

export const caseSeachFormValidationSchema = () => {
  return Yup.object().shape({
    [FORM_NAMES.CASE_NUMBER]: Yup.string().when(FORM_NAMES.FILTER_TYPE, {
      is: FILTER_TYPE_VALUE.CASE_NUMBER,
      then: Yup.string()
        .required(I18N_EXCEPTION.CASE_NUMBER_IS_REQUIRED)
        .min(16, I18N_COMMON_TEXT.INVALID_FORMAT)
        .max(19, I18N_COMMON_TEXT.INVALID_FORMAT)
        .trim(),
      otherwise: Yup.string().optional()
    }),
    [FORM_NAMES.ACCOUNT_NUMBER]: Yup.string().when(FORM_NAMES.FILTER_TYPE, {
      is: FILTER_TYPE_VALUE.ACCOUNT_NUMBER,
      then: Yup.string()
        .required(I18N_EXCEPTION.ACCOUNT_NUMBER_IS_REQUIRED)
        .min(15, I18N_COMMON_TEXT.INVALID_FORMAT)
        .max(16, I18N_COMMON_TEXT.INVALID_FORMAT)
        .trim(),
      otherwise: Yup.string().optional()
    }),
    [FORM_NAMES.CASE_STATUS]: Yup.object().when(FORM_NAMES.FILTER_TYPE, {
      is: FILTER_TYPE_VALUE.CASE_STATUS_AND_UPDATE_DATE,
      then: Yup.object().required(I18N_EXCEPTION.CASE_STATUS_IS_REQUIRED),
      otherwise: Yup.object().optional()
    }),
    [FORM_NAMES.UPDATE_DATE_RANGE]: Yup.object().when(FORM_NAMES.FILTER_TYPE, {
      is: FILTER_TYPE_VALUE.CASE_STATUS_AND_UPDATE_DATE,
      then: Yup.object({
        start: Yup.date().required(I18N_EXCEPTION.LAST_UPDATE_DATE_IS_REQUIRED),
        end: Yup.date().required(I18N_EXCEPTION.LAST_UPDATE_DATE_IS_REQUIRED)
      }).required(I18N_EXCEPTION.LAST_UPDATE_DATE_IS_REQUIRED),
      otherwise: Yup.object().optional()
    })
  });
};

export const getFieldValue = (
  fieldElements?: FieldElement[],
  category?: string,
  fieldName?: string
) => {
  if (!fieldElements) return '';
  const fieldByCategory = fieldElements.find(
    f => f.category === category
  )?.dataFieldElements;
  return (
    fieldByCategory?.find(f => f.fieldName === fieldName)?.fieldValue || ''
  );
};

export const getFieldValues = (
  fieldElements?: FieldElement[],
  category?: string,
  fieldName?: string
) => {
  if (!fieldElements) return '';
  const fieldByCategory = fieldElements.find(
    f => f.category === category
  )?.dataFieldElements;
  return (
    fieldByCategory
      ?.filter(f => f.fieldName === fieldName)
      ?.map(f => f.fieldValue || '') || []
  );
};

export const convertSecureArchiveJsonData = (
  jsonData?: string
): CaseTaskError | undefined => {
  if (!jsonData) return undefined;
  const result: CaseTaskError = {};

  try {
    const parseJson = JSON.parse(jsonData);
    result.requestFromCallingApplication = get(
      parseJson,
      'requestFromCallingApplication'
    );
    result.requestedService = get(parseJson, 'requestedService');
    result.retryCount = get(parseJson, 'retryCount');
    result.errorDate = get(parseJson, 'errorDate');
    result.errorCode = get(parseJson, 'responseStatus.errorCode');
    result.errorMessage = get(parseJson, 'responseStatus.errorMessage');
    result.callingApplication = get(parseJson, 'callingApplication');
  } catch (error) {
    return undefined;
  }

  return result;
};

export const getModuleFromRequestService = ({
  requestType,
  caseStatus
}: ReprocessCaseArgs) => {
  let moduleName = '';
  const caseStatusKey = Object.keys(CASE_STATUS_MAPPING).find(
    k => CASE_STATUS_MAPPING[k] === caseStatus
  );

  switch (caseStatusKey) {
    case '20':
      moduleName = 'CX_PTP';
      break;
    case '21':
      moduleName = 'CX_BANKRUPTCY';
      break;
    case '22':
      moduleName = 'CX_DECEASED';
      break;
    case '24':
      moduleName = 'CX_HARDSHIP';
      break;
    case '25':
      moduleName = 'CX_CCCS';
      break;
    case '23':
      switch (requestType) {
        case 'MEWorkflowRequest':
          moduleName = 'CX_LONG_TERM_MEDICAL';
          break;
        case 'FDWorkflowRequest':
          moduleName = 'CX_INCARCERATION';
          break;
        default:
          moduleName = 'CX_ACTION_ENTRY';
          break;
      }
  }

  return moduleName;
};

export const getCaseStatusWarningMessage = (caseStatus: string): string => {
  // Mapped case status return empty
  if (Object.values(CASE_STATUS_MAPPING).includes(caseStatus)) return '';

  const caseStatusNumber = Number.parseInt(caseStatus);
  if (!isNaN(caseStatusNumber)) {
    if (
      (caseStatusNumber >= 0 && caseStatusNumber <= 9) ||
      caseStatusNumber === 11 ||
      (caseStatusNumber >= 13 && caseStatusNumber <= 19) ||
      (caseStatusNumber >= 90 && caseStatusNumber <= 100)
    ) {
      return I18N_EXCEPTION.CASE_STATUS_NO_FUTHER_ACTION;
    } else {
      return I18N_EXCEPTION.CASE_STATUS_ACCESS_NOT_AVAILABLE;
    }
  }
  return '';
};
