import { SORT_TYPE } from 'app/constants';
import { CSPAData, FieldElement, ReprocessCaseArgs } from './types';
import { CASE_STATUS_MAPPING, FORM_NAMES, I18N_EXCEPTION } from './constants';
import {
  convertCSPAId,
  filterCSPAList,
  sortCSPAList,
  caseSeachFormValidationSchema,
  getFieldValue,
  getFieldValues,
  convertSecureArchiveJsonData,
  getModuleFromRequestService,
  getCaseStatusWarningMessage
} from './helpers';

export const CODE_ALL = '$ALL';

const cspaData: CSPAData[] = [
  {
    clientId: '9999',
    systemId: '1000',
    principleId: '1000',
    agentId: '3000',
    userId: 'FDESMAST',
    description: 'updated CSPA active'
  },
  {
    clientId: '8888',
    systemId: '1001',
    principleId: '2002',
    agentId: '3003',
    userId: 'FDESMAST',
    description: 'Description'
  }
];

describe('ExpectionHandling helpers', () => {
  describe('filterCSPAList', () => {
    it('it should be empty', () => {
      const data = filterCSPAList(cspaData, '00000');
      expect(data.length).toEqual(0);
    });

    it('clientId > has data', () => {
      const searchText = '99';
      const data = filterCSPAList(cspaData, searchText);
      expect(data[0].clientId).toContain(searchText);
    });
    it('systemId > has data', () => {
      const searchText = '10';
      const data = filterCSPAList(cspaData, searchText);
      expect(data[0].systemId).toContain(searchText);
    });
    it('principleId > has data', () => {
      const searchText = '10';
      const data = filterCSPAList(cspaData, searchText);
      expect(data[0].principleId).toContain(searchText);
    });
    it('description > has data', () => {
      const searchText = 'updated';
      const data = filterCSPAList(cspaData, searchText);
      expect(data[0].description).toContain(searchText);
    });
  });

  describe('sortCSPAList', () => {
    const sortBy = {
      id: 'clientId',
      order: SORT_TYPE.ASC
    };

    it('Default current CSPA List', () => {
      const cspaDefault: CSPAData[] = [
        {
          id: '001',
          clientId: CODE_ALL,
          systemId: CODE_ALL,
          principleId: CODE_ALL,
          agentId: CODE_ALL,
          userId: CODE_ALL
        },
        {
          id: '002',
          clientId: 'value',
          systemId: 'value',
          principleId: 'value',
          agentId: 'value',
          userId: 'value'
        }
      ];
      const result = sortCSPAList(cspaDefault, sortBy);
      expect(result).toEqual(cspaDefault);
    });

    it('Default next CSPA List', () => {
      const cspaDefault: CSPAData[] = [
        {
          clientId: 'value',
          systemId: 'value',
          principleId: 'value',
          agentId: 'value',
          userId: 'value'
        },
        {
          clientId: CODE_ALL,
          systemId: CODE_ALL,
          principleId: CODE_ALL,
          agentId: CODE_ALL,
          userId: CODE_ALL
        }
      ];
      const result = sortCSPAList(cspaDefault, sortBy);
      expect(result).toEqual(cspaDefault);
    });

    it('CSPA List current < next and order is asc', () => {
      const sortBy = {
        id: 'clientId',
        order: SORT_TYPE.ASC
      };

      const cspaList: CSPAData[] = [
        {
          id: '001',
          clientId: '9999',
          systemId: '1000',
          principleId: '1000',
          agentId: '3000',
          userId: 'FDESMAST',
          description: 'updated CSPA active'
        },
        {
          id: '002',
          clientId: '8888',
          systemId: '1001',
          principleId: '2002',
          agentId: '3003',
          userId: 'FDESMAST',
          description: 'Description'
        }
      ];
      const result = sortCSPAList(cspaList, sortBy);
      expect(result).toEqual(cspaList);
    });

    it('CSPA List current < next and order is desc', () => {
      const sortBy = {
        id: 'clientId',
        order: SORT_TYPE.DESC
      };

      const cspaList: CSPAData[] = [
        {
          id: '001',
          clientId: '9999',
          systemId: '1000',
          principleId: '1000',
          agentId: '3000',
          userId: 'FDESMAST',
          description: 'updated CSPA active'
        },
        {
          id: '002',
          clientId: '8888',
          systemId: '1001',
          principleId: '2002',
          agentId: '3003',
          userId: 'FDESMAST',
          description: 'Description'
        }
      ];
      const result = sortCSPAList([...cspaList], sortBy);
      expect(result).toEqual(cspaList);
    });

    it('CSPA List current > next and order is asc', () => {
      const sortBy = {
        id: 'clientId',
        order: SORT_TYPE.ASC
      };

      const cspaList: CSPAData[] = [
        {
          id: '001',
          clientId: 'a',
          systemId: '1000',
          principleId: '1000',
          agentId: '3000',
          userId: 'FDESMAST',
          description: 'updated CSPA active'
        },
        {
          id: '002',
          clientId: 'b',
          systemId: '1001',
          principleId: '2002',
          agentId: '3003',
          userId: 'FDESMAST',
          description: 'Description'
        }
      ];

      const result = sortCSPAList([...cspaList], sortBy);
      expect(result).toEqual(cspaList);
    });

    it('CSPA List current > next', () => {
      const sortBy = {
        id: 'clientId',
        order: SORT_TYPE.DESC
      };

      const cspaList: CSPAData[] = [
        {
          id: '001',
          clientId: 'a',
          systemId: '1000',
          principleId: '1000',
          agentId: '3000',
          userId: 'FDESMAST',
          description: 'updated CSPA active'
        },
        {
          id: '002',
          clientId: 'b',
          systemId: '1001',
          principleId: '2002',
          agentId: '3003',
          userId: 'FDESMAST',
          description: 'Description'
        }
      ];

      const result = sortCSPAList([...cspaList], sortBy);
      expect(result).toEqual([...cspaList].reverse());
    });

    it('CSPA List order is empty', () => {
      const sortBy = {};

      const cspaList: CSPAData[] = [
        {
          id: '001',
          clientId: 'a',
          systemId: '1000',
          principleId: '1000',
          agentId: '3000',
          userId: 'FDESMAST',
          description: 'updated CSPA active'
        },
        {
          id: '002',
          clientId: 'b',
          systemId: '1001',
          principleId: '2002',
          agentId: '3003',
          userId: 'FDESMAST',
          description: 'Description'
        }
      ];

      const result = sortCSPAList([...cspaList], sortBy);
      expect(result).toEqual([...cspaList].reverse());
    });
  });

  describe('convertCSPAId', () => {
    it('It should work correctly', () => {
      const clientId = 'clientId';
      const agentId = 'agentId';
      const systemId = 'systemId';
      const principleId = 'systemId';

      const result = convertCSPAId(clientId, agentId, systemId, principleId);
      expect(result).toEqual(
        `${clientId} - ${systemId} - ${principleId} - ${agentId}`
      );
    });
  });

  describe('caseSeachFormValidationSchema', () => {
    it('it should work correctly', () => {
      const validationSchema = caseSeachFormValidationSchema();
      expect(
        validationSchema.fields[FORM_NAMES.CASE_NUMBER].required
      ).toBeTruthy();
      expect(
        validationSchema.fields[FORM_NAMES.ACCOUNT_NUMBER].required
      ).toBeTruthy();
      expect(
        validationSchema.fields[FORM_NAMES.CASE_STATUS].required
      ).toBeTruthy();
      expect(
        validationSchema.fields[FORM_NAMES.CASE_NUMBER].required
      ).toBeTruthy();
      expect(validationSchema.fields.updateDateRanger.required()).toBeTruthy();
    });
  });

  describe('getFieldValue', () => {
    const fieldElements: FieldElement[] = [
      {
        category: 'catogeory a',
        dataFieldElements: [
          {
            fieldName: 'field name 1',
            fieldValue: 'field name 1 value'
          }
        ]
      }
    ];

    it('fieldElements is undefined', () => {
      const result = getFieldValue();
      expect(result).toEqual('');
    });

    it('fieldElements is not empty, category is empty', () => {
      const category = '';
      const fieldName = '';
      const expectValue = '';
      const result = getFieldValue(fieldElements, category, fieldName);
      expect(result).toEqual(expectValue);
    });

    it('it should work correctly', () => {
      const category = 'catogeory a';
      const fieldName = 'field name 1';
      const expectValue = 'field name 1 value';
      const result = getFieldValue(fieldElements, category, fieldName);
      expect(result).toEqual(expectValue);
    });
  });

  describe('getFieldValues', () => {
    const fieldElements: FieldElement[] = [
      {
        category: 'catogeory a',
        dataFieldElements: [
          {
            fieldName: 'field name 1',
            fieldValue: 'field name 1 value'
          },
          {
            fieldName: 'field name 1',
            fieldValue: 'field name 2 value'
          }
        ]
      }
    ];

    it('fieldElements is undefined', () => {
      const expectValue = '';
      const result = getFieldValues();
      expect(result).toEqual(expectValue);
    });

    it('fieldElements is not empty, category is empty', () => {
      const category = '';
      const fieldName = '';
      const expectValue: any[] = [];
      const result = getFieldValues(fieldElements, category, fieldName);
      expect(result).toEqual(expectValue);
    });

    it('it should work correctly', () => {
      const category = 'catogeory a';
      const fieldName = 'field name 1';
      const expectValue: string[] = [
        'field name 1 value',
        'field name 2 value'
      ];
      const result = getFieldValues(fieldElements, category, fieldName);
      expect(result).toEqual(expectValue);
    });

    it('it should work correctly with empty fieldValue', () => {
      const fieldElements: FieldElement[] = [
        {
          category: 'catogeory a',
          dataFieldElements: [
            {
              fieldName: 'field name 1'
            }
          ]
        }
      ];
      const category = 'catogeory a';
      const fieldName = 'field name 1';
      const expectValue: string[] = [''];
      const result = getFieldValues(fieldElements, category, fieldName);
      expect(result).toEqual(expectValue);
    });
  });

  describe('convertSecureArchiveJsonData', () => {
    const childObj: any = {
      errorCode: 'errorCode',
      errorMessage: 'errorMessage'
    };

    const jsonData: any = {
      requestFromCallingApplication: 'value',
      requestedService: 'value',
      retryCount: 'value',
      errorDate: 'errorDate',
      responseStatus: childObj,
      callingApplication: 'callingApplication'
    };

    it('jsonData is empty', () => {
      const jsonData = '';
      const result = convertSecureArchiveJsonData(jsonData);
      expect(result).toBeUndefined();
    });

    it('it should throw exception', () => {
      const jsonData = 'abc';
      const result = convertSecureArchiveJsonData(
        JSON.parse(JSON.stringify(jsonData))
      );
      expect(result).toBeUndefined();
    });

    it('it should work correctly ', () => {
      const expectResult: any = {
        ...jsonData,
        ...childObj,
        responseStatus: undefined
      };
      const result = convertSecureArchiveJsonData(JSON.stringify(jsonData));
      expect(result).toEqual(expectResult);
    });
  });

  describe('Test getModuleFromRequestService', () => {
    it('it should return empty', () => {
      const reprocessCaseArgs = {
        requestType: '',
        caseStatus: ''
      } as ReprocessCaseArgs;

      const result = getModuleFromRequestService(reprocessCaseArgs);
      expect(result).toEqual('');
    });

    it('when caseStatus is Promise to Pay - 20', () => {
      const reprocessCaseArgs = {
        requestType: '',
        caseStatus: 'txt_promise_to_pay'
      } as ReprocessCaseArgs;

      const result = getModuleFromRequestService(reprocessCaseArgs);
      expect(result).toEqual('CX_PTP');
    });

    it('it should return Bankruptcy - 21', () => {
      const reprocessCaseArgs = {
        requestType: '',
        caseStatus: 'txt_bankruptcy'
      } as ReprocessCaseArgs;

      const result = getModuleFromRequestService(reprocessCaseArgs);
      expect(result).toEqual('CX_BANKRUPTCY');
    });

    it('it should return empty', () => {
      const reprocessCaseArgs = {
        requestType: '',
        caseStatus: 'txt_bankruptcy'
      } as ReprocessCaseArgs;

      const result = getModuleFromRequestService(reprocessCaseArgs);
      expect(result).toEqual('CX_BANKRUPTCY');
    });

    it('it should return Deceased - 22', () => {
      const reprocessCaseArgs = {
        requestType: '',
        caseStatus: 'txt_deceased'
      } as ReprocessCaseArgs;

      const result = getModuleFromRequestService(reprocessCaseArgs);
      expect(result).toEqual('CX_DECEASED');
    });

    it('it should return Hardship - 24', () => {
      const reprocessCaseArgs = {
        requestType: '',
        caseStatus: 'txt_hardship'
      } as ReprocessCaseArgs;

      const result = getModuleFromRequestService(reprocessCaseArgs);
      expect(result).toEqual('CX_HARDSHIP');
    });

    it('it should return CCCS - 25', () => {
      const reprocessCaseArgs = {
        requestType: '',
        caseStatus: 'txt_cccs'
      } as ReprocessCaseArgs;

      const result = getModuleFromRequestService(reprocessCaseArgs);
      expect(result).toEqual('CX_CCCS');
    });

    it('it should return CX_CCCS', () => {
      const reprocessCaseArgs = {
        requestType: 'MEWorkflowRequest',
        caseStatus: 'txt_action_entry'
      } as ReprocessCaseArgs;

      const result = getModuleFromRequestService(reprocessCaseArgs);
      expect(result).toEqual('CX_LONG_TERM_MEDICAL');
    });

    it('it should return CX_INCARCERATION', () => {
      const reprocessCaseArgs = {
        requestType: 'FDWorkflowRequest',
        caseStatus: 'txt_action_entry'
      } as ReprocessCaseArgs;

      const result = getModuleFromRequestService(reprocessCaseArgs);
      expect(result).toEqual('CX_INCARCERATION');
    });

    it('it should return CX_ACTION_ENTRY', () => {
      const reprocessCaseArgs = {
        requestType: '',
        caseStatus: 'txt_action_entry'
      } as ReprocessCaseArgs;

      const result = getModuleFromRequestService(reprocessCaseArgs);
      expect(result).toEqual('CX_ACTION_ENTRY');
    });

    it('it should return empty string if the status is mapped', () => {
      const promiseToPayStatus = CASE_STATUS_MAPPING['20'];
      const result = getCaseStatusWarningMessage(promiseToPayStatus);
      expect(result).toEqual('');
    });

    it('it should return empty string if the status is NaN', () => {
      const promiseToPayStatus = 'NaN';
      const result = getCaseStatusWarningMessage(promiseToPayStatus);
      expect(result).toEqual('');
    });

    it('it should return CASE_STATUS_NO_FUTHER_ACTION', () => {
      const promiseToPayStatus = '0';
      const result = getCaseStatusWarningMessage(promiseToPayStatus);
      expect(result).toEqual(I18N_EXCEPTION.CASE_STATUS_NO_FUTHER_ACTION);
    });

    it('it should return CASE_STATUS_NO_FUTHER_ACTION', () => {
      const promiseToPayStatus = '95';
      const result = getCaseStatusWarningMessage(promiseToPayStatus);
      expect(result).toEqual(I18N_EXCEPTION.CASE_STATUS_NO_FUTHER_ACTION);
    });

    it('it should return CASE_STATUS_NO_FUTHER_ACTION', () => {
      const promiseToPayStatus = '11';
      const result = getCaseStatusWarningMessage(promiseToPayStatus);
      expect(result).toEqual(I18N_EXCEPTION.CASE_STATUS_NO_FUTHER_ACTION);
    });

    it('it should return CASE_STATUS_NO_FUTHER_ACTION', () => {
      const promiseToPayStatus = '13';
      const result = getCaseStatusWarningMessage(promiseToPayStatus);
      expect(result).toEqual(I18N_EXCEPTION.CASE_STATUS_NO_FUTHER_ACTION);
    });

    it('it should return CASE_STATUS_ACCESS_NOT_AVAILABLE', () => {
      const promiseToPayStatus = '10';
      const result = getCaseStatusWarningMessage(promiseToPayStatus);
      expect(result).toEqual(I18N_EXCEPTION.CASE_STATUS_ACCESS_NOT_AVAILABLE);
    });
  });
});
