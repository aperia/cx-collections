import React from 'react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';
import ExceptionHandlingNavigation from '.';
import { screen } from '@testing-library/dom';
// Constants
import { I18N_EXCEPTION } from './constants';

const mockTabBarActions = mockActionCreator(actionsTab);

describe('should test reporting navigation', () => {
  const renderWrapper = () => {
    return renderMockStore(<ExceptionHandlingNavigation />);
  };

  test('should render UI', () => {
    jest.useFakeTimers();

    const mockAddTab = mockTabBarActions('addTab');

    renderWrapper();

    jest.runAllTimers();

    const reportingButton = screen.getByText(I18N_EXCEPTION.EXCEPTION_TITLE);

    expect(reportingButton).toBeInTheDocument();

    reportingButton.click();

    expect(mockAddTab).toBeCalledWith({
      id: 'exception-handling',
      title: I18N_EXCEPTION.EXCEPTION_TITLE,
      storeId: 'exception-handling',
      tabType: 'exceptionHandling',
      iconName: 'file'
    });
  });
});
