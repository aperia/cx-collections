import { exceptionHandlingServices } from './exceptionHandlingService';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';
import { GetClientConfigurationRequest } from 'pages/ClientConfiguration/types';

import { CODE_ALL } from './helpers';
import { getStartOfDay } from 'app/helpers';
import { FormatTime } from 'app/constants/enums';
import { FILTER_TYPE_VALUE } from './constants';
import {
  CaseDetailFilter,
  CaseSearchRequest,
  CSPAData,
  ReprocessCaseRequest,
  SecureArchiveRequest
} from './types';
import datetime from 'date-and-time';

const getRequestBody = (
  requestBody: CaseSearchRequest,
  filter: CaseDetailFilter
) => {
  const parseReq = { ...requestBody };

  switch (filter.filterType) {
    case FILTER_TYPE_VALUE.ACCOUNT_NUMBER:
      parseReq.customer = filter.accountNumber;
      break;
    case FILTER_TYPE_VALUE.CASE_NUMBER:
      parseReq.caseId = filter.caseNumber;
      break;
    case FILTER_TYPE_VALUE.CASE_STATUS_AND_UPDATE_DATE:
      parseReq.caseStatus = filter.caseStatus?.fieldID;
      parseReq.updateDateFrom = getStartOfDay(
        filter.updateDateRanger?.start,
        FormatTime.UTCDate
      );
      parseReq.updateDateTo = getStartOfDay(
        datetime.addDays(filter.updateDateRanger?.end as Date, 1),
        FormatTime.UTCDate
      );
      break;
  }

  return parseReq;
};

// mock request param

const common: CSPAData = {
  id: 'id',
  clientId: 'clientId',
  systemId: 'systemId',
  principleId: 'principleId',
  agentId: 'agentId',
  userId: 'userId',
  description: 'description'
};

describe('exceptionHandlingService', () => {
  beforeEach(() => {
    mockAppConfigApi.clear();
  });

  describe('getCSPAList', () => {
    const params: GetClientConfigurationRequest = {
      common: {
        agent: 'agent',
        clientNumber: 'clientNumber',
        system: 'system',
        prin: 'prin',
        user: 'user',
        app: 'app',
        org: 'org'
      }
    };

    it('when url was not defined', () => {
      const mockService = mockApiServices.post();
      exceptionHandlingServices.getCSPAList(params);
      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();
      const mockService = mockApiServices.post();
      exceptionHandlingServices.getCSPAList(params);
      expect(mockService).toBeCalledWith(
        apiUrl.clientConfig.clientInfoGetList,
        params
      );
    });

    it('when url was defined and default parameters', () => {
      mockAppConfigApi.setup();
      const mockService = mockApiServices.post();
      exceptionHandlingServices.getCSPAList({});
      expect(mockService).toBeCalledWith(
        apiUrl.clientConfig.clientInfoGetList,
        {}
      );
    });
  });

  describe('getCaseStatuses', () => {
    it('when url was not defined', () => {
      const mockService = mockApiServices.get();
      exceptionHandlingServices.getCaseStatuses();
      expect(mockService).toBeCalled();
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();
      const mockService = mockApiServices.get();
      exceptionHandlingServices.getCaseStatuses();
      expect(mockService).toBeCalledWith(
        apiUrl.exceptionHandling.getCaseStatuses
      );
    });
  });

  describe('caseDetailsSearch', () => {
    const requestBody: CaseSearchRequest = {
      common: {
        agent: common.agentId || CODE_ALL,
        clientNumber: common.clientId || CODE_ALL,
        system: common.systemId || CODE_ALL,
        prin: common.principleId || CODE_ALL,
        user: common.userId || CODE_ALL,
        app: 'cx',
        org: undefined
      },
      excludeCloseCases: false
    };

    it('when url was not defined', () => {
      const request = getRequestBody(requestBody, {
        filterType: FILTER_TYPE_VALUE.ACCOUNT_NUMBER
      });
      const mockService = mockApiServices.post();
      exceptionHandlingServices.caseDetailsSearch(common, {
        filterType: FILTER_TYPE_VALUE.ACCOUNT_NUMBER
      });
      expect(mockService).toBeCalledWith('', request);
    });

    it('when url was defined and default parameters', () => {
      mockAppConfigApi.setup();
      const mockService = mockApiServices.post();
      exceptionHandlingServices.caseDetailsSearch(
        {},
        { filterType: FILTER_TYPE_VALUE.ACCOUNT_NUMBER }
      );
      expect(mockService).toBeCalledWith(
        apiUrl.exceptionHandling.caseDetailsSearch,
        {
          common: {
            agent: '$ALL',
            app: 'cx',
            clientNumber: '$ALL',
            org: undefined,
            prin: '$ALL',
            system: '$ALL',
            user: '$ALL'
          },
          customer: undefined,
          excludeCloseCases: false
        }
      );
    });

    it('when url was defined and filterType is accountNumber', () => {
      const request = getRequestBody(requestBody, {
        filterType: FILTER_TYPE_VALUE.ACCOUNT_NUMBER
      });
      mockAppConfigApi.setup();
      const mockService = mockApiServices.post();
      exceptionHandlingServices.caseDetailsSearch(common, {
        filterType: FILTER_TYPE_VALUE.ACCOUNT_NUMBER
      });
      expect(mockService).toBeCalledWith(
        apiUrl.exceptionHandling.caseDetailsSearch,
        request
      );
    });

    it('when url was defined and filterType is caseNumber', () => {
      const request = getRequestBody(requestBody, {
        filterType: FILTER_TYPE_VALUE.CASE_NUMBER
      });
      mockAppConfigApi.setup();
      const mockService = mockApiServices.post();
      exceptionHandlingServices.caseDetailsSearch(common, {
        filterType: FILTER_TYPE_VALUE.CASE_NUMBER
      });
      expect(mockService).toBeCalledWith(
        apiUrl.exceptionHandling.caseDetailsSearch,
        request
      );
    });

    it('when url was defined and filterType is case Status and update date', () => {
      mockAppConfigApi.setup();
      const mockService = mockApiServices.post();
      exceptionHandlingServices.caseDetailsSearch(common, {
        filterType: FILTER_TYPE_VALUE.CASE_STATUS_AND_UPDATE_DATE,
        updateDateRanger: {
          end: new Date()
        }
      });
      expect(mockService).toBeCalled();
    });
  });

  describe('secureArchiveRetrievalByCaseId', () => {
    const caseId = 'caseId';
    const archiveId = 'archiveId';

    const requestBody: SecureArchiveRequest = {
      common: {
        agent: common.agentId || CODE_ALL,
        clientNumber: common.clientId || CODE_ALL,
        system: common.systemId || CODE_ALL,
        prin: common.principleId || CODE_ALL,
        user: common.userId || CODE_ALL,
        org: undefined,
        app: 'cx'
      },
      caseId,
      requestType: 'Response',
      archiveId
    };

    it('when url was defined', () => {
      mockAppConfigApi.setup();
      const mockService = mockApiServices.post();
      exceptionHandlingServices.secureArchiveRetrievalByCaseId(
        common,
        caseId,
        archiveId
      );
      expect(mockService).toBeCalledWith(
        apiUrl.exceptionHandling.secureArchiveRetrievalByCaseId,
        { ...requestBody, bypassCaseIdCheck: true }
      );
    });

    it('when url was not defined', () => {
      const mockService = mockApiServices.post();
      exceptionHandlingServices.secureArchiveRetrievalByCaseId(
        common,
        caseId,
        archiveId
      );
      expect(mockService).toBeCalledWith('', {
        ...requestBody,
        bypassCaseIdCheck: true
      });
    });

    it('when url was defined and default parameters', () => {
      mockAppConfigApi.setup();
      const mockService = mockApiServices.post();
      exceptionHandlingServices.secureArchiveRetrievalByCaseId(
        {},
        caseId,
        archiveId
      );
      expect(mockService).toBeCalledWith(
        apiUrl.exceptionHandling.secureArchiveRetrievalByCaseId,
        {
          ...requestBody,
          common: {
            agent: CODE_ALL,
            clientNumber: CODE_ALL,
            system: CODE_ALL,
            prin: CODE_ALL,
            user: CODE_ALL,
            app: 'cx',
            org: undefined
          },
          bypassCaseIdCheck: true
        }
      );
    });
  });

  describe('reprocessCase', () => {
    const caseId = 'caseId';

    const requestBody: ReprocessCaseRequest = {
      common: {
        agent: common.agentId || CODE_ALL,
        clientNumber: common.clientId || CODE_ALL,
        system: common.systemId || CODE_ALL,
        prin: common.principleId || CODE_ALL,
        user: common.userId || CODE_ALL,
        app: 'cx'
      },
      caseId,
      requestType: 'Response'
    };

    it('when url was not defined', () => {
      const mockService = mockApiServices.post();
      exceptionHandlingServices.reprocessCase(requestBody);
      expect(mockService).toBeCalledWith('', requestBody);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();
      const mockService = mockApiServices.post();
      exceptionHandlingServices.reprocessCase(requestBody);
      expect(mockService).toBeCalledWith(
        apiUrl.exceptionHandling.reprocessCase,
        requestBody
      );
    });
  });
});
