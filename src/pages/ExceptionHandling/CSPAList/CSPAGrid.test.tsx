import React from 'react';
import CSPAGrid from './CSPAGrid';
import {
  mockActionCreator,
  queryById,
  renderMockStoreId
} from 'app/test-utils';
import { cspaActions } from './_redux/reducers';
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';
import { I18N_EXCEPTION } from '../constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import userEvent from '@testing-library/user-event';
import { GridProps, TextSearchProps } from 'app/_libraries/_dls/components';

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');
  const { MockGrid } = jest.requireActual('app/test-utils/mocks/MockGrid');
  return {
    ...dlsComponents,
    Grid: (props: GridProps) => (
      <>
        <MockGrid {...props} />
        <button
          data-testid="mockSortGrid"
          onClick={() => props.onSortChange?.({ id: 'state' })}
        >
          Mock Sort Grid component
        </button>
      </>
    ),
    TextSearch: (props: TextSearchProps) => {
      return (
        <>
          <input
            data-testid="textSearch-onChange"
            onChange={(e: any) => props.onChange?.(e)}
          />
          <button
            data-testid="textSearch-onClear"
            onClick={() => props.onClear?.()}
          >
            Mock Clear Text Search
          </button>
          <button
            data-testid="textSearch-onSearch"
            onClick={() => props.onSearch?.('abc')}
          >
            Mock Search Text Search
          </button>
        </>
      );
    }
  };
});

let spy: jest.SpyInstance;
const tabActionSpy = mockActionCreator(actionsTab);
const cspaActionSpy = mockActionCreator(cspaActions);

const initialState: Partial<RootState> = {
  cspa: {
    cspaList: [
      { id: '1', description: 'abc', agentId: '1', clientId: '1', userId: '1' },
      { id: '2', description: 'bnc', agentId: '2', clientId: '2', userId: '2' },
      { id: '3', description: 'abc', agentId: '3', clientId: '3', userId: '3' },
      { id: '4', description: 'dnk', agentId: '4', clientId: '4', userId: '4' },
      { id: '5', description: 'abc', agentId: '5', clientId: '5', userId: '5' },
      { id: '6', description: 'abc', agentId: '6', clientId: '6', userId: '6' },
      { id: '7', description: 'abc', agentId: '7', clientId: '7', userId: '7' },
      { id: '8', description: 'dnk', agentId: '8', clientId: '8', userId: '8' },
      { id: '9', description: 'bnc', agentId: '9', clientId: '9', userId: '9' },
      {
        id: '10',
        description: 'bnc',
        agentId: '10',
        clientId: '10',
        userId: '10'
      },
      {
        id: '11',
        description: 'abc',
        agentId: '11',
        clientId: '11',
        userId: '11'
      }
    ],
    loading: false,
    error: false,
    searchValue: '',
    // textSearchValue: '',
    sortBy: { id: 'id', order: 'desc' },
    caseDetail: {
      id: '2',
      description: 'bnc',
      agentId: '2',
      clientId: '2',
      userId: '2'
    }
  },
  accessConfig: {
    cspaList: [{ cspa: '9999-1000-2000-3000' }]
  }
};

afterEach(() => {
  if (spy) {
    spy.mockReset();
    spy.mockRestore();
  }
});

describe('Render UI', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isCollector: true,
      isAdminRole: true
    }
  };

  it('Should render is loading', () => {
    const { wrapper } = renderMockStoreId(<CSPAGrid />, {
      initialState: {
        ...initialState,
        cspa: { ...initialState.cspa, loading: true }
      }
    });
    expect(wrapper.container.querySelector('.loading')).toBeInTheDocument();
  });

  it('Should render is empty', () => {
    const { wrapper } = renderMockStoreId(<CSPAGrid />, { initialState: {} });
    expect(
      wrapper.getByText(I18N_EXCEPTION.MODIFY_CLIENT_CONFIG)
    ).toBeInTheDocument();
  });

  it('Should render is error', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy = jest.spyOn(cspaActions, 'cspaGetList').mockImplementation(mockAction);
    const { wrapper } = renderMockStoreId(<CSPAGrid />, {
      initialState: { ...initialState, cspa: { error: true, loading: false } }
    });
    expect(
      queryById(wrapper.container, /client-config-list-failed/)
    ).toBeInTheDocument();
    expect(mockAction).toBeCalled();
  });

  it('Should render', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy = jest.spyOn(cspaActions, 'cspaGetList').mockImplementation(mockAction);
    const { wrapper } = renderMockStoreId(<CSPAGrid />, { initialState });
    expect(
      wrapper.getAllByText(I18N_EXCEPTION.VIEW_REPORT)[0]
    ).toBeInTheDocument();
    expect(mockAction).toBeCalled();
  });

  it('Should render not found data', () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy = jest.spyOn(cspaActions, 'cspaGetList').mockImplementation(mockAction);
    const { wrapper } = renderMockStoreId(<CSPAGrid />, {
      initialState: {
        ...initialState,
        cspa: { ...initialState.cspa, searchValue: '123' }
      }
    });
    expect(
      wrapper.getByText(I18N_COMMON_TEXT.CLEAR_AND_RESET)
    ).toBeInTheDocument();
    expect(mockAction).toBeCalled();
  });
});

describe('Action', () => {
  it('handleClearAndReset', () => {
    const mockData = jest.fn().mockImplementation(() => ({ type: 'type' }));
    const spyClearAndReset = cspaActionSpy('clearAndReset');
    spy = jest.spyOn(cspaActions, 'cspaGetList').mockImplementation(mockData);
    const { wrapper } = renderMockStoreId(<CSPAGrid />, {
      initialState: {
        ...initialState,
        cspa: { ...initialState.cspa, searchValue: '123' }
      }
    });
    wrapper.getByText(I18N_COMMON_TEXT.CLEAR_AND_RESET).click();
    expect(spyClearAndReset).toBeCalled();
  });

  it('handleSortChange without sortBy', () => {
    const mockData = jest.fn().mockImplementation(() => ({ type: 'type' }));
    const spyUpdateSortBy = cspaActionSpy('updateSortBy');
    spy = jest.spyOn(cspaActions, 'cspaGetList').mockImplementation(mockData);
    const { wrapper } = renderMockStoreId(<CSPAGrid />, { initialState });
    userEvent.click(wrapper.getByTestId('mockSortGrid'));
    expect(spyUpdateSortBy).toBeCalled();
  });

  it('handleSortChange', () => {
    const mockData = jest.fn().mockImplementation(() => ({ type: 'type' }));
    const spyUpdateSortBy = cspaActionSpy('updateSortBy');
    spy = jest.spyOn(cspaActions, 'cspaGetList').mockImplementation(mockData);
    const { wrapper } = renderMockStoreId(<CSPAGrid />, {
      initialState: { ...initialState }
    });
    userEvent.click(wrapper.getByTestId('mockSortGrid'));
    userEvent.click(
      wrapper.container.querySelector('.dls-grid-head > tr > th')!
    );
    expect(spyUpdateSortBy).toBeCalled();
  });

  it('handleReload', () => {
    const mockData = jest.fn().mockImplementation(() => ({ type: 'type' }));
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy = jest.spyOn(cspaActions, 'cspaGetList').mockImplementation(mockData);
    const { wrapper } = renderMockStoreId(<CSPAGrid />, {
      initialState: { ...initialState, cspa: { error: true, loading: false } }
    });
    spy.mockReset();
    spy.mockRestore();
    spy = jest.spyOn(cspaActions, 'cspaGetList').mockImplementation(mockAction);
    wrapper.getByText(I18N_COMMON_TEXT.RELOAD).click();
    expect(mockAction).toBeCalled();
  });

  it('handleAdd', () => {
    const mockAddTab = tabActionSpy('addTab');
    const { wrapper } = renderMockStoreId(<CSPAGrid />, {
      initialState: {}
    });

    const queryButton = wrapper.getByText(I18N_EXCEPTION.MODIFY_CLIENT_CONFIG);
    queryButton!.click();
    expect(mockAddTab).toBeCalledWith({
      id: 'client-configuration',
      title: I18N_COMMON_TEXT.CLIENT_CONFIGURATION,
      storeId: 'client-configuration',
      tabType: 'clientConfiguration',
      iconName: 'file'
    });
  });
});
