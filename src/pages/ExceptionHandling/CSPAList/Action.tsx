import React, { useCallback } from 'react';
import { I18N_EXCEPTION } from '../constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { Button } from 'app/_libraries/_dls/components';
import { useDispatch } from 'react-redux';
import { CSPAData } from '../types';
import { cspaActions } from './_redux/reducers';

export interface ActionProps {
  cspaData?: CSPAData;
  dataTestId?: string;
}

const Action: React.FC<ActionProps> = ({ cspaData, dataTestId }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const handleViewSetting = useCallback(() => {
    dispatch(cspaActions.setCaseDetail(cspaData));
  }, [dispatch, cspaData]);

  return (
    <>
      <Button variant="outline-primary" size="sm" onClick={handleViewSetting} dataTestId={dataTestId}>
        {t(I18N_EXCEPTION.VIEW_REPORT)}
      </Button>
    </>
  );
};

export default Action;
