import {
  renderMockStoreId
} from 'app/test-utils';
import CSPAList from './index';
import React from 'react';

jest.mock('pages/ExceptionHandling/CSPAList/CSPAGrid', () => () => (
  <div>CSPAGrid</div>
));

jest.mock('pages/ExceptionHandling/CaseDetailSearch', () => () => (
  <div>CaseDetailSearch</div>
));

describe('Render UI', () => {
  it('Render UI > CSPAGrid', () => {
    const { wrapper } = renderMockStoreId(<CSPAList />);
    expect(wrapper.getByText('CSPAGrid')).toBeInTheDocument();
  });

  it('Render UI > CaseDetailSearch', () => {
    const initialState: Partial<RootState> = {
      cspa: {
        caseDetail: {
          id: '1',
        }
      }
    };
    const { wrapper } = renderMockStoreId(<CSPAList />, {initialState});
    expect(wrapper.getByText('CaseDetailSearch')).toBeInTheDocument();
  });

});