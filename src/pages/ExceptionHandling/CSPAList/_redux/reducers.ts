import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { SortType } from 'app/_libraries/_dls/components';
import { convertCSPAId } from 'pages/ExceptionHandling/helpers';
import { CSPAData, CSPAState } from '../../types';
import { cspaGetList, cspaGetListBuilder } from './cspaGetList';

export const initialState: CSPAState = {};

const { actions, reducer } = createSlice({
  name: 'cspa',
  initialState: initialState,
  reducers: {
    changeSearchValue: (draftState, action: PayloadAction<string>) => {
      const searchValue = action.payload;

      draftState.searchValue = searchValue;
    },
    changeTextSearchValue: (draftState, action: PayloadAction<string>) => {
      const textSearchValue = action.payload;

      draftState.textSearchValue = textSearchValue;
    },
    clearAndReset: draftState => {
      draftState.textSearchValue = '';
      draftState.searchValue = '';
      draftState.sortBy = undefined;
    },
    updateSortBy: (draftState, action: PayloadAction<SortType>) => {
      const data = action.payload;
      draftState.sortBy = data;
    },
    setCaseDetail: (
      draftState,
      action: PayloadAction<CSPAData | undefined>
    ) => {
      const data = action.payload ? { ...action.payload } : undefined;
      if (data) {
        const {
          clientId = '',
          agentId = '',
          systemId = '',
          principleId = ''
        } = data;
        data.id = convertCSPAId(clientId, agentId, systemId, principleId);
      }
      draftState.caseDetail = data;
    },
    cleanup: () => initialState
  },
  extraReducers: builder => {
    cspaGetListBuilder(builder);
  }
});

const allActions = {
  ...actions,
  cspaGetList
};

export { allActions as cspaActions, reducer };
