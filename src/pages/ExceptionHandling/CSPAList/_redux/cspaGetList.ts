import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
import { GetClientConfigurationRequest } from 'pages/ClientConfiguration/types';
import { exceptionHandlingServices } from 'pages/ExceptionHandling/exceptionHandlingService';
import { CSPAData, CSPAState } from '../../types';

export const cspaGetList = createAsyncThunk<
  CSPAData[],
  undefined,
  ThunkAPIConfig
>('cspa/cspaGetList', async (args, { getState, rejectWithValue }) => {
  try {
    const { accessConfig, mapping } = getState();
    const mappingModel = mapping?.data?.cspaList;

    const { user, app, org } = window.appConfig.commonConfig;
    const [clientNumber = '', system = '$ALL', prin = '$ALL', agent = '$ALL'] =
      accessConfig?.cspaList?.[0]?.cspa?.split('-') || [];

    const requestBody: GetClientConfigurationRequest = {
      common: {
        clientName: 'firstdata',
        agent,
        clientNumber,
        system,
        prin,
        user,
        app
      },
      orgId: org,
      fetchSize: '500'
    };

    const response = await exceptionHandlingServices.getCSPAList(requestBody);

    if (response.data?.responseStatus?.toLowerCase() !== 'success') {
      throw new Error('Status is not success');
    }

    return mappingDataFromArray<CSPAData>(
      response.data?.clientInfoList,
      mappingModel!
    );
  } catch (error) {
    return rejectWithValue({ response: error });
  }
});

export const cspaGetListBuilder = (
  builder: ActionReducerMapBuilder<CSPAState>
) => {
  builder
    .addCase(cspaGetList.pending, (draftState, action) => {
      draftState.loading = true;
      draftState.error = false;
    })
    .addCase(cspaGetList.fulfilled, (draftState, action) => {
      draftState.cspaList = action.payload;
      draftState.loading = false;
    })
    .addCase(cspaGetList.rejected, (draftState, action) => {
      draftState.loading = false;
      draftState.error = true;
    });
};
