import { selectorWrapper } from 'app/test-utils';
import { DOFAppState } from 'storeConfig';
import * as selectors from './selectors';
import {
  CSPAData,
  CSPA_DEFAULT_SORT_VALUE
} from 'pages/ExceptionHandling/types';


const state: Partial<DOFAppState> = {
  cspa: {
    cspaList: [
      { id: '1', description: 'abc' },
      { id: '2', description: '' },
    ],
    loading: false,
    error: false,
    searchValue: 'abc',
    textSearchValue: 'abc',
    sortBy: { id: 'id', order: 'asc' },
    caseDetail: {
      id: '2',
    }
  }
}

const triggerSelector = selectorWrapper(state)

describe('Test selectors', () => {
  it('selectedOriginData', () => {
    const { data, emptyData } = triggerSelector(selectors.selectedOriginData);
    expect(data).toEqual(state.cspa?.cspaList);
    expect(emptyData).toEqual([]);
  });

  it('selectedCspaList', () => {
    const { data, emptyData } = triggerSelector(selectors.selectedCspaList);
    const cspaList = [{ id: '1', description: 'abc' }] as CSPAData[];
    expect(data).toEqual(cspaList);
    expect(emptyData).toEqual([]);
  });

  it('selectedLoading', () => {
    const { data, emptyData } = triggerSelector(selectors.selectedLoading);
    expect(data).toBeFalsy();
    expect(emptyData).toEqual(undefined);
  });

  it('selectedError', () => {
    const { data, emptyData } = triggerSelector(selectors.selectedError);
    expect(data).toBeFalsy();
    expect(emptyData).toEqual(undefined);
  });

  it('selectedSearchValue', () => {
    const { data, emptyData } = triggerSelector(selectors.selectedSearchValue);
    expect(data).toEqual(state.cspa?.searchValue);
    expect(emptyData).toEqual('');
  });

  it('selectedTextSearchValue', () => {
    const { data, emptyData } = triggerSelector(selectors.selectedTextSearchValue);
    expect(data).toEqual(state.cspa?.textSearchValue);
    expect(emptyData).toEqual('');
  });

  it('selectedSortBy', () => {
    const { data, emptyData } = triggerSelector(selectors.selectedSortBy);
    expect(data).toEqual(state.cspa?.sortBy);
    expect(emptyData).toEqual(CSPA_DEFAULT_SORT_VALUE);
  });

  it('selectedCaseDetail', () => {
    const { data, emptyData } = triggerSelector(selectors.selectedCaseDetail);
    expect(data).toEqual(state.cspa?.caseDetail);
    expect(emptyData).toEqual(undefined);
  });
});