import { exceptionHandlingServices } from 'pages/ExceptionHandling/exceptionHandlingService';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { responseDefault } from 'app/test-utils';
import { cspaGetList } from './cspaGetList';
import { CSPAData } from 'pages/ExceptionHandling/types';

let spy: jest.SpyInstance;

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    org: 'org',
    app: 'app',
    user: 'user'
  }
};

describe('Test cspaGetList', () => {
  afterEach(() => {
    spy.mockReset();
    spy.mockRestore();
  });

  const mockApiServiceError = (response?: MagicKeyValue) => {
    spy = jest
      .spyOn(exceptionHandlingServices, 'getCSPAList')
      .mockResolvedValue({
        ...responseDefault,
        ...response
      });
  };

  const mockApiService = (response?: MagicKeyValue) => {
    spy = jest
      .spyOn(exceptionHandlingServices, 'getCSPAList')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          responseStatus: 'success',
          clientInfoList: [
            {
              clientId: '9999',
              agentId: '3000',
              systemId: '1000',
              principleId: '2000'
            } as CSPAData
          ]
        }
      });
  };

  it('should fulfilled', async () => {
    mockApiService();

    const store = createStore(rootReducer, {
      mapping: {
        data: {
          cspaList: {
            id: 'clientInfoId',
            clientId: 'clientId',
            systemId: 'systemId',
            principleId: 'principleId',
            agentId: 'agentId',
            userId: 'userId',
            description: 'portfolioName'
          }
        }
      }
    });
    const response = await cspaGetList()(store.dispatch, store.getState, {});
    expect(response.type).toEqual('cspa/cspaGetList/fulfilled');
    expect(response?.payload).toEqual([
      {
        clientId: '9999',
        systemId: '1000',
        principleId: '2000',
        agentId: '3000'
      }
    ]);
  });

  it('should rejected', async () => {
    mockApiServiceError();

    const store = createStore(rootReducer, {});
    const response = await cspaGetList()(store.dispatch, store.getState, {});
    expect(response.type).toEqual('cspa/cspaGetList/rejected');
    expect(response?.payload).toEqual({
      response: Error('Status is not success')
    });
  });
});
