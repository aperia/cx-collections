import { cspaActions, reducer } from './reducers';
import { CSPAData, CSPAState } from '../../types';
import { SortType } from 'app/_libraries/_dls/components';
import { convertCSPAId } from 'pages/ExceptionHandling/helpers';

const initialState: CSPAState = {};

describe('Test reducer', () => {
  it('changeSearchValue', () => {
    const param = 'abc';
    const state = reducer(
      initialState,
      cspaActions.changeSearchValue(param)
    );
    expect(state).toEqual({ searchValue: param });
  });

  it('changeTextSearchValue', () => {
    const param = 'abc';
    const state = reducer(
      initialState,
      cspaActions.changeTextSearchValue(param)
    );
    expect(state).toEqual({ textSearchValue: param });
  });

  it('clearAndReset', () => {
    const state = reducer(
      initialState,
      cspaActions.clearAndReset
    );
    expect(state).toEqual({ searchValue: '', textSearchValue: '', sortBy: undefined });
  });

  it('updateSortBy', () => {
    const param = { id: 'id', order: 'asc' } as SortType;
    const state = reducer(
      initialState,
      cspaActions.updateSortBy(param)
    );
    expect(state).toEqual({sortBy: param});
  });

  it('setCaseDetail with param', () => {
    const param = {
      clientId: '9999',
      agentId: '3000',
      systemId: '1000',
      principleId: '2000'
    } as CSPAData;
    param.id = convertCSPAId(param.clientId as string, param.agentId as string, param.systemId as string, param.principleId as string);
    const state = reducer(
      initialState,
      cspaActions.setCaseDetail(param)
    );
    expect(state).toEqual({ caseDetail: param });
  });

  it('setCaseDetail with undefined', () => {
    const state = reducer(
      initialState,
      cspaActions.setCaseDetail(undefined)
    );
    expect(state).toEqual({ caseDetail: undefined });
  });

  it('setCaseDetail with empty', () => {
    const state = reducer(
      initialState,
      cspaActions.setCaseDetail({})
    );
    expect(state).toEqual({ caseDetail: { id: ' -  -  - ' } });
  });

  it('cleanup', () => {
    const state = reducer(
      initialState,
      cspaActions.cleanup
    );
    expect(state).toEqual(initialState);
  });
});

