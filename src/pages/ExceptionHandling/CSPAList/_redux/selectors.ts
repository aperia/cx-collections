import { createSelector } from '@reduxjs/toolkit';
import { SortType } from 'app/_libraries/_dls/components';
import { filterCSPAList, sortCSPAList } from 'pages/ExceptionHandling/helpers';
import {
  CSPAData,
  CSPA_DEFAULT_SORT_VALUE
} from 'pages/ExceptionHandling/types';

const getCspaList = (states: RootState) => states.cspa?.cspaList || [];
const getSortBy = (states: RootState) =>
  states.cspa?.sortBy || CSPA_DEFAULT_SORT_VALUE;
const getSearchValue = (states: RootState) =>
  states.cspa?.searchValue?.trim() || '';

export const selectedOriginData = createSelector(
  getCspaList,
  (data: CSPAData[]) => data
);

export const selectedCspaList = createSelector(
  getCspaList,
  getSortBy,
  getSearchValue,
  (data: CSPAData[], sortBy: SortType, searchValue: string) => {
    const filteredList = filterCSPAList(data, searchValue?.toLowerCase());

    return sortCSPAList(filteredList, sortBy);
  }
);

const getCspaStatus = (states: RootState) => ({
  loading: states.cspa?.loading,
  error: states.cspa?.error
});

export const selectedLoading = createSelector(
  getCspaStatus,
  (data: MagicKeyValue) => data.loading
);

export const selectedError = createSelector(
  getCspaStatus,
  (data: MagicKeyValue) => data.error
);

export const selectedSearchValue = createSelector(
  getSearchValue,
  (value: string) => value
);

export const selectedTextSearchValue = createSelector(
  (states: RootState) => states.cspa?.textSearchValue || '',
  (value: string) => value
);

export const selectedSortBy = createSelector(
  getSortBy,
  (value: SortType) => value
);

const getCaseDetail = (states: RootState) => states.cspa?.caseDetail;

export const selectedCaseDetail = createSelector(getCaseDetail, data => data);
