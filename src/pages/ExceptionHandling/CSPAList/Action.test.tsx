import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import { CSPAData } from '../types';
import Action from './Action';
import React from 'react';
import { I18N_EXCEPTION } from '../constants';
import { cspaActions } from './_redux/reducers';


describe('Render UI', () => {
  it('Render UI > Action without props', () => {
    const { wrapper } = renderMockStoreId(<Action />);
    expect(wrapper.getByText(I18N_EXCEPTION.VIEW_REPORT)).toBeInTheDocument();
  });

  it('Render UI > Action props', () => {
    const cspaActionSpy = mockActionCreator(cspaActions);
    const mockSetCaseDetail = cspaActionSpy('setCaseDetail');
    const data = { id: '2' } as CSPAData
    const initialState: Partial<RootState> = {
      cspa: {
        caseDetail: {
          id: '1',
        }
      }
    };
    const { wrapper } = renderMockStoreId(<Action cspaData={data} />, { initialState });
    wrapper.getByText(I18N_EXCEPTION.VIEW_REPORT).click();
    expect(mockSetCaseDetail).toBeCalledWith({id: '2'});
    mockSetCaseDetail.mockClear();
  });

});