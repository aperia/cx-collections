//components
import React, { useCallback, useEffect, useMemo } from 'react';
import { FailedApiReload, NoDataGrid } from 'app/components';
import Header from './Header';
import Action from './Action';
import classnames from 'classnames';
import {
  Button,
  ColumnType,
  Grid,
  Pagination,
  SortType,
  TruncateText
} from 'app/_libraries/_dls/components';
import { usePagination } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import isEmpty from 'lodash.isempty';

// constants
import { I18N_EXCEPTION } from '../constants';
import { CSPAData, CSPA_DEFAULT_SORT_VALUE } from '../types';
import { I18N_CLIENT_CONFIG, I18N_COMMON_TEXT } from 'app/constants/i18n';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { cspaActions } from './_redux/reducers';
import {
  selectedCspaList,
  selectedError,
  selectedLoading,
  selectedSortBy,
  selectedSearchValue,
  selectedOriginData
} from './_redux/selectors';
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { selectCSPAFromAccessConfig } from 'pages/__commons/AccessConfig/_redux/selector';

const CSPAGrid: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const cspaListData = useSelector(selectedCspaList);
  const sortBy = useSelector(selectedSortBy);
  const error = useSelector(selectedError);
  const loading = useSelector(selectedLoading);
  const searchValue = useSelector(selectedSearchValue);
  const originData = useSelector(selectedOriginData);
  const accessConfig = useSelector(selectCSPAFromAccessConfig);

  const { isAdminRole: isAdmin } = window.appConfig?.commonConfig;

  const isNoResultsFound =
    !loading && !error && cspaListData.length === 0 && !!searchValue?.trim();

  const testId = 'exceptionHandling-CSPAGrid';

  const CSPA_LIST_COLUMNS: ColumnType[] = useMemo(
    () => [
      {
        id: 'clientId',
        Header: t(I18N_COMMON_TEXT.CLIENT_ID),
        isSort: true,
        width: 160,
        accessor: 'clientId'
      },
      {
        id: 'systemId',
        Header: t(I18N_CLIENT_CONFIG.SYSTEM_ID),
        isSort: true,
        width: 160,
        accessor: 'systemId'
      },
      {
        id: 'principleId',
        Header: t(I18N_CLIENT_CONFIG.PRINCIPLE_ID),
        isSort: true,
        width: 160,
        accessor: 'principleId'
      },
      {
        id: 'agentId',
        Header: t(I18N_CLIENT_CONFIG.AGENT_ID),
        isSort: true,
        width: 160,
        accessor: 'agentId'
      },
      {
        id: 'description',
        Header: t(I18N_COMMON_TEXT.DESCRIPTION),
        isSort: true,
        width: 428,
        autoWidth: true,
        accessor: (data: CSPAData, index: number) => (
          <TruncateText
            lines={2}
            title={data?.description}
            resizable
            ellipsisMoreText={t(I18N_COMMON_TEXT.MORE)}
            ellipsisLessText={t(I18N_COMMON_TEXT.LESS)}
            className="text-break"
            dataTestId={genAmtId(testId, `description-${index}`, '')}
          >
            {data?.description}
          </TruncateText>
        )
      },
      {
        id: 'action',
        Header: (
          <div className="text-center">{t(I18N_COMMON_TEXT.ACTIONS)}</div>
        ),
        width: 162,
        cellBodyProps: { className: 'text-center td-sm' },
        isFixedRight: true,
        accessor: (data: CSPAData, index: number) => (
          <Action
            cspaData={data}
            dataTestId={genAmtId(testId, `action-${index}`, '')}
          />
        )
      }
    ],
    [t]
  );

  useEffect(() => {
    if (!accessConfig.clientNumber) return;
    dispatch(cspaActions.cspaGetList());
    return () => {
      dispatch(cspaActions.clearAndReset());
    };
  }, [accessConfig.clientNumber, dispatch]);

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange,
    setCurrentPage,
    setCurrentPageSize
  } = usePagination(cspaListData);

  const showPagination = total > pageSize[0];

  const handleClearAndReset = useCallback(() => {
    setCurrentPageSize(pageSize[0]);
    dispatch(cspaActions.clearAndReset());
  }, [dispatch, setCurrentPageSize, pageSize]);

  const handleSortChange = (newSortBy: SortType) => {
    dispatch(
      cspaActions.updateSortBy(
        newSortBy?.order ? newSortBy : CSPA_DEFAULT_SORT_VALUE
      )
    );
  };

  const handleReload = () => {
    dispatch(cspaActions.cspaGetList());
  };

  const handleAdd = useCallback(() => {
    isAdmin &&
      dispatch(
        actionsTab.addTab({
          id: 'client-configuration',
          title: I18N_COMMON_TEXT.CLIENT_CONFIGURATION,
          storeId: 'client-configuration',
          tabType: 'clientConfiguration',
          iconName: 'file'
        })
      );
  }, [dispatch, isAdmin]);

  if (error) {
    return (
      <>
        <Header
          setCurrentPage={setCurrentPage}
          setCurrentPageSize={setCurrentPageSize}
          pageSize={pageSize}
        />
        <FailedApiReload
          id="client-config-list-failed"
          className="mt-80"
          onReload={handleReload}
          dataTestId={genAmtId(testId, 'failed', '')}
        />
      </>
    );
  }

  if (isEmpty(originData)) {
    return (
      <>
        <Header
          setCurrentPage={setCurrentPage}
          setCurrentPageSize={setCurrentPageSize}
          pageSize={pageSize}
        />
        <NoDataGrid
          text="txt_no_CSPA_to_display"
          dataTestId={genAmtId(testId, 'no-CSPA', '')}
        >
          <div className="mt-24">
            <Button
              size="sm"
              variant="outline-primary"
              onClick={handleAdd}
              dataTestId={genAmtId(testId, 'modify-btn', '')}
            >
              {t(I18N_EXCEPTION.MODIFY_CLIENT_CONFIG)}
            </Button>
          </div>
        </NoDataGrid>
      </>
    );
  }

  if (isNoResultsFound)
    return (
      <>
        <Header
          setCurrentPage={setCurrentPage}
          setCurrentPageSize={setCurrentPageSize}
          pageSize={pageSize}
        />
        <NoDataGrid
          text="txt_no_search_results_found"
          dataTestId={genAmtId(testId, 'no-search-results-found', '')}
        >
          <div className="mt-24">
            <Button
              size="sm"
              variant="outline-primary"
              onClick={handleClearAndReset}
              dataTestId={genAmtId(testId, 'reset-btn', '')}
            >
              {t(I18N_COMMON_TEXT.CLEAR_AND_RESET)}
            </Button>
          </div>
        </NoDataGrid>
      </>
    );

  if (loading)
    return (
      <div className="mt-40 pt-40">
        <div className={classnames('mt-30', { loading })} />
      </div>
    );

  return (
    <div>
      <Header
        setCurrentPage={setCurrentPage}
        setCurrentPageSize={setCurrentPageSize}
        pageSize={pageSize}
      />
      <div className="mt-16">
        <Grid
          columns={CSPA_LIST_COLUMNS}
          data={gridData}
          sortBy={[sortBy]}
          onSortChange={handleSortChange}
          dataTestId={genAmtId(testId, 'grid', '')}
        />
        {showPagination && (
          <div className="pt-16">
            <Pagination
              totalItem={total}
              pageSize={pageSize}
              pageNumber={currentPage}
              pageSizeValue={currentPageSize}
              compact
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
              dataTestId={genAmtId(testId, 'pagination', '')}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default CSPAGrid;
