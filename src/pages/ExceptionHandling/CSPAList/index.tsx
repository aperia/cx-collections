import { SimpleBar } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import React from 'react';
import { I18N_EXCEPTION } from '../constants';
import { selectedCaseDetail } from './_redux/selectors';
import CSPAGrid from './CSPAGrid';
import { useSelector } from 'react-redux';
import CaseDetailSearch from '../CaseDetailSearch';
import { genAmtId } from 'app/_libraries/_dls/utils';

const CSPAList: React.FC = () => {
  const { t } = useTranslation();

  const caseDetail = useSelector(selectedCaseDetail);

  return (
    <>
      {caseDetail && caseDetail.id ? (
        <CaseDetailSearch />
      ) : (
        <SimpleBar>
          <div className="max-width-lg mx-auto p-24">
            <h3
              className="pb-24"
              data-testid={genAmtId('exceptionHandling-CSPAList', 'title', '')}
            >
              {t(I18N_EXCEPTION.EXCEPTION_TITLE)}
            </h3>
            <CSPAGrid />
          </div>
        </SimpleBar>
      )}
    </>
  );
};

export default CSPAList;
