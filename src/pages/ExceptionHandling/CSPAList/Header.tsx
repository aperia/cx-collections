import React, {
  ChangeEvent,
  Dispatch,
  SetStateAction,
  useCallback
} from 'react';
import { I18N_EXCEPTION } from '../constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { Button, TextSearch } from 'app/_libraries/_dls/components';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { useDispatch, useSelector } from 'react-redux';
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';
import {
  selectedCspaList,
  selectedError,
  selectedSearchValue,
  selectedTextSearchValue
} from './_redux/selectors';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { cspaActions } from './_redux/reducers';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface HeaderProps {
  setCurrentPage: Dispatch<SetStateAction<number>>;
  setCurrentPageSize: Dispatch<SetStateAction<number>>;
  pageSize: number[];
}

const Header: React.FC<HeaderProps> = ({
  setCurrentPage,
  setCurrentPageSize,
  pageSize
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const { isAdminRole: isAdmin } = window.appConfig?.commonConfig;

  const data = useSelector(selectedCspaList);
  const searchValues = useSelector(selectedSearchValue);
  const textSearchValue = useSelector(selectedTextSearchValue);

  const isNoRecords = isEmpty(data) && !searchValues?.trim();
  const error = useSelector(selectedError);
  const testId = 'exceptionHandling-CSPAList-header';

  const handleSearch = (searchValue: string) => {
    setCurrentPage(1);
    dispatch(cspaActions.changeSearchValue(searchValue));
  };

  const handleChangeTextSearch = (e: ChangeEvent<HTMLInputElement>) => {
    dispatch(cspaActions.changeTextSearchValue(e.target.value));
  };

  const handleClearTextSearch = () => {
    dispatch(cspaActions.changeTextSearchValue(''));
  };

  const handleAdd = useCallback(() => {
    isAdmin &&
      dispatch(
        actionsTab.addTab({
          id: 'client-configuration',
          title: I18N_COMMON_TEXT.CLIENT_CONFIGURATION,
          storeId: 'client-configuration',
          tabType: 'clientConfiguration',
          iconName: 'file'
        })
      );
  }, [dispatch, isAdmin]);

  const handleClearAndReset = () => {
    setCurrentPageSize(pageSize[0]);
    dispatch(cspaActions.clearAndReset());
  };

  if (error) {
    return null;
  }

  return (
    <>
      <div className="d-flex align-items-start justify-content-between pb-16">
        <div className="d-flex">
          <h5 data-testid={genAmtId(testId, 'title', '')}>
            {t(I18N_EXCEPTION.CSPA_LIST)}
          </h5>
        </div>
        {!isNoRecords && (
          <TextSearch
            value={textSearchValue}
            onChange={handleChangeTextSearch}
            onClear={handleClearTextSearch}
            onSearch={handleSearch}
            placeholder={t(I18N_COMMON_TEXT.TYPE_TO_SEARCH)}
            dataTestId={genAmtId(testId, 'search', '')}
          />
        )}
      </div>
      <div className="text-right mr-n8">
        {!isEmpty(data) && (
          <>
            <Button
              variant="outline-primary"
              size="sm"
              onClick={handleAdd}
              dataTestId={genAmtId(testId, 'modify-btn', '')}
            >
              {t(I18N_EXCEPTION.MODIFY_CLIENT_CONFIG)}
            </Button>
            {searchValues && (
              <Button
                size="sm"
                variant="outline-primary"
                onClick={handleClearAndReset}
                dataTestId={genAmtId(testId, 'reset-btn', '')}
              >
                {t(I18N_COMMON_TEXT.CLEAR_AND_RESET)}
              </Button>
            )}
          </>
        )}
      </div>
    </>
  );
};

export default Header;
