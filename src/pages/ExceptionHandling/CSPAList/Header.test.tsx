import React from 'react';
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import Header from './Header';
import { I18N_EXCEPTION } from '../constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { cspaActions } from './_redux/reducers';
import userEvent from '@testing-library/user-event';
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';
import { TextSearchProps } from 'app/_libraries/_dls/components';

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');
  return {
    ...dlsComponents,
    TextSearch: (props: TextSearchProps) => {
      return (
        <>
          <input
            data-testid="textSearch-onChange"
            onChange={(e: any) => props.onChange?.(e)}
          />
          <button
            data-testid="textSearch-onClear"
            onClick={() => props.onClear?.()}
          >
            Mock Clear Text Search
          </button>
          <button
            data-testid="textSearch-onSearch"
            onClick={() => props.onSearch?.('abc')}
          >
            Mock Search Text Search
          </button>
        </>
      );
    }
  };
});

const initialState: Partial<RootState> = {
  cspa: {
    cspaList: [
      { id: '1', description: 'abc', agentId: '1', clientId: '1', userId: '1' },
      { id: '2', description: '', agentId: '2', clientId: '2', userId: '2' }
    ],
    loading: false,
    error: false,
    searchValue: 'abc',
    textSearchValue: 'abc',
    sortBy: { id: 'id', order: 'asc' },
    caseDetail: {
      id: '2',
      description: '',
      agentId: '2',
      clientId: '2',
      userId: '2'
    }
  }
};

const cspaActionSpy = mockActionCreator(cspaActions);
const tabActionSpy = mockActionCreator(actionsTab);

describe('Render UI', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isCollector: true,
      isAdminRole: true
    }
  };
  it('Should render UI', () => {
    const { wrapper } = renderMockStoreId(
      <Header
        pageSize={[5, 10, 20]}
        setCurrentPage={jest.fn()}
        setCurrentPageSize={jest.fn()}
      />,
      { initialState }
    );
    expect(
      wrapper.getByText(I18N_EXCEPTION.MODIFY_CLIENT_CONFIG)
    ).toBeInTheDocument();
    expect(
      wrapper.getByText(I18N_COMMON_TEXT.CLEAR_AND_RESET)
    ).toBeInTheDocument();
  });

  it('Should render UI with empty data', () => {
    const { wrapper } = renderMockStoreId(
      <Header
        pageSize={[5, 10, 15]}
        setCurrentPage={jest.fn()}
        setCurrentPageSize={jest.fn()}
      />,
      { initialState: {} }
    );
    expect(wrapper.getByText(I18N_EXCEPTION.CSPA_LIST)).toBeInTheDocument();
  });
});

describe('Action', () => {
  it('handleClearAndReset', () => {
    const mockChangeSearchValue = cspaActionSpy('clearAndReset');
    const { wrapper } = renderMockStoreId(
      <Header
        pageSize={[5, 10, 20]}
        setCurrentPage={jest.fn()}
        setCurrentPageSize={jest.fn()}
      />,
      { initialState }
    );
    wrapper.getByText(I18N_COMMON_TEXT.CLEAR_AND_RESET).click();
    expect(mockChangeSearchValue).toBeCalled();
    mockChangeSearchValue.mockClear();
  });

  it('handleAdd', () => {
    const mockAddTab = tabActionSpy('addTab');
    const { wrapper } = renderMockStoreId(
      <Header
        pageSize={[5, 10, 20]}
        setCurrentPage={jest.fn()}
        setCurrentPageSize={jest.fn()}
      />,
      { initialState }
    );
    wrapper.getByText(I18N_EXCEPTION.MODIFY_CLIENT_CONFIG).click();
    expect(mockAddTab).toBeCalled();
    mockAddTab.mockClear();
  });

  it('handleClearTextSearch', () => {
    const mockChangeTextSearchValue = cspaActionSpy('changeTextSearchValue');
    const { wrapper } = renderMockStoreId(
      <Header
        pageSize={[5, 10, 20]}
        setCurrentPage={jest.fn()}
        setCurrentPageSize={jest.fn()}
      />,
      { initialState }
    );
    userEvent.click(wrapper.getByTestId(`textSearch-onClear`)!);
    expect(mockChangeTextSearchValue).toBeCalled();
    mockChangeTextSearchValue.mockClear();
  });

  it('handleChangeTextSearch', () => {
    const mockChangeTextSearchValue = cspaActionSpy('changeTextSearchValue');
    const { wrapper } = renderMockStoreId(
      <Header
        pageSize={[5, 10, 20]}
        setCurrentPage={jest.fn()}
        setCurrentPageSize={jest.fn()}
      />,
      { initialState }
    );
    userEvent.type(wrapper.getByTestId(`textSearch-onChange`)!, 'a');
    expect(mockChangeTextSearchValue).toBeCalled();
    mockChangeTextSearchValue.mockClear();
  });

  it('handleSearch', () => {
    const mockchangeSearchValue = cspaActionSpy('changeSearchValue');
    const { wrapper } = renderMockStoreId(
      <Header
        pageSize={[5, 10, 20]}
        setCurrentPage={jest.fn()}
        setCurrentPageSize={jest.fn()}
      />,
      { initialState }
    );
    userEvent.click(wrapper.getByTestId(`textSearch-onSearch`)!);
    expect(mockchangeSearchValue).toBeCalled();
    mockchangeSearchValue.mockClear();
  });
});
