import React from 'react';
import { generateRecurringPromisesData, RecurringPromisesGrid } from './index';
import { render, screen } from '@testing-library/react';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

const fixtureDate = new Date('2021/04/14');

describe('generateRecurringPromisesData', () => {
  it('not count', () => {
    const result = generateRecurringPromisesData({
      amount: '12',
      paymentMethod: 'Check',
      startDate: fixtureDate,
      interval: 'WEEKLY',
      count: ''
    });
    expect(result).toEqual([]);
  });

  it('count < 1', () => {
    const result = generateRecurringPromisesData({
      amount: '12',
      paymentMethod: 'Check',
      startDate: fixtureDate,
      interval: 'WEEKLY',
      count: '0.7'
    });
    expect(result).toEqual([]);
  });

  it('WEEKLY', () => {
    const result = generateRecurringPromisesData({
      amount: '12',
      paymentMethod: 'Check',
      startDate: fixtureDate,
      interval: 'WEEKLY',
      count: '2'
    });

    expect(result).toEqual([
      {
        paymentMethod: 'Check',
        paymentDate: '04/14/2021',
        amount: '12'
      },
      {
        paymentMethod: 'Check',
        paymentDate: '04/21/2021',
        amount: '12'
      }
    ]);
  });

  it('BI-WEEKLY', () => {
    const result = generateRecurringPromisesData({
      amount: '12',
      paymentMethod: 'Check',
      startDate: fixtureDate,
      interval: 'BIWEEKLY',
      count: '2'
    });
    expect(result).toEqual([
      {
        paymentMethod: 'Check',
        paymentDate: '04/14/2021',
        amount: '12'
      },
      {
        paymentMethod: 'Check',
        paymentDate: '04/28/2021',
        amount: '12'
      }
    ]);
  });

  it('MONTHLY', () => {
    const result = generateRecurringPromisesData({
      amount: '12',
      paymentMethod: 'Check',
      startDate: fixtureDate,
      interval: 'MONTHLY',
      count: '2'
    });
    expect(result).toEqual([
      {
        paymentMethod: 'Check',
        paymentDate: '04/14/2021',
        amount: '12'
      },
      {
        paymentMethod: 'Check',
        paymentDate: '05/14/2021',
        amount: '12'
      }
    ]);
  });
  it('SOME THING ELSE', () => {
    const result = generateRecurringPromisesData({
      amount: '12',
      paymentMethod: 'Check',
      startDate: fixtureDate,
      interval: 'SomeThingElse' as never,
      count: '2'
    });
    expect(result).toEqual([
      {
        paymentMethod: 'Check',
        paymentDate: fixtureDate,
        amount: '12'
      },
      {
        paymentMethod: 'Check',
        paymentDate: fixtureDate,
        amount: '12'
      }
    ]);
  });
});

describe('RecurringPromisesGrid', () => {
  it('Return null when has no data', () => {
    const { container } = render(<RecurringPromisesGrid data={[]} />, {});
    expect(container.firstChild).toBeNull();
  });

  it('render correctly', () => {
    const data = [
      { amount: '12', paymentMethod: 'Check', paymentDate: fixtureDate }
    ];
    render(<RecurringPromisesGrid data={data} />);
    expect(screen.getAllByText('$12.00').length).toEqual(1);
  });
});
