import React, { useMemo } from 'react';
// helpers
import { formatCommon } from 'app/helpers';
import { addDays, addMonths } from 'app/helpers/commons';
// components
import { ColumnType, Grid } from 'app/_libraries/_dls/components';
// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
// types & constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { RecurringGridItem } from '../types';

export const generateRecurringPromisesData = ({
  amount,
  paymentMethod,
  startDate,
  interval,
  count
}: {
  amount?: string;
  startDate?: string | Date;
  interval?: string;
  paymentMethod?: string;
  count?: string;
}) => {
  if (!amount || !paymentMethod || !startDate || !interval || !count) return [];
  const newCount = parseInt(count, 10);
  if (newCount < 1) return [];
  const result = [];
  let newDate = startDate;

  for (let i = 0; i < newCount; i++) {
    switch (interval) {
      case 'WEEKLY':
        newDate = addDays(startDate, 7 * i);
        break;
      case 'BIWEEKLY':
        newDate = addDays(startDate, 14 * i);
        break;
      case 'MONTHLY':
        newDate = addMonths(startDate, i);
        break;
      default:
        break;
    }
    result.push({ paymentMethod, paymentDate: newDate, amount });
  }
  return result;
};

export interface RecurringPromisesGridProps {
  data: RecurringGridItem[];
  titleElement?: React.ReactNode;
}

export const RecurringPromisesGrid: React.FC<RecurringPromisesGridProps> =
  props => {
    const { t } = useTranslation();
    const { data, titleElement } = props;
    const dataColumn = useMemo(() => {
      return [
        {
          id: 'paymentMethodDate',
          Header: `${t(I18N_COMMON_TEXT.PAYMENT_METHOD)} • ${t(
            I18N_COMMON_TEXT.DATE
          )}`,
          accessor: ({ paymentMethod, paymentDate }: any) =>
            `${t(paymentMethod)} • ${formatCommon(paymentDate).time.date}`,
          width: 219
        },
        {
          id: 'Amount',
          Header: 'Amount',
          accessor: ({ amount }: any) => formatCommon(amount).currency(),
          width: 139,
          cellProps: { className: 'text-right' }
        }
      ];
    }, [t]);

    if (!data.length) return null;

    return (
      <div>
        {titleElement}
        <Grid
          dataTestId="recurringPromisesGrid"
          maxHeight={482}
          columns={dataColumn as unknown as ColumnType[]}
          data={data}
        />
      </div>
    );
  };
