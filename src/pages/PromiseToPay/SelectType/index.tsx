import React from 'react';

// components
import { Radio } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { PromiseToPayType } from '../types';

export interface SelectTypeProps {
  uniqueId?: string;
  type: string;
  setType: (value: PromiseToPayType) => void;
}

export const SelectType: React.FC<SelectTypeProps> = ({
  uniqueId,
  type,
  setType
}) => {
  const { t } = useTranslation();

  const handleChangeRadio = (e: React.ChangeEvent<HTMLInputElement>) => {
    setType(e.target.value as PromiseToPayType);
  };

  return (
    <div className="d-flex mt-16">
      <p className="mr-24 color-grey fs-14 fw-500">
        {t(I18N_COLLECTION_FORM.TYPE)}:
      </p>
      <Radio dataTestId="promiseToPay-Single" className="mr-24">
        <Radio.Input
          onChange={handleChangeRadio}
          checked={type === PromiseToPayType.SINGLE}
          value={PromiseToPayType.SINGLE}
          name={`promise-to-pay-type__${uniqueId}`}
          id={`${uniqueId}-${PromiseToPayType.SINGLE}`}
        />
        <Radio.Label>{t(I18N_COLLECTION_FORM.SINGLE)}</Radio.Label>
      </Radio>
      <Radio dataTestId="promiseToPay-Recurring" className="mr-24">
        <Radio.Input
          onChange={handleChangeRadio}
          checked={type === PromiseToPayType.RECURRING}
          value={PromiseToPayType.RECURRING}
          name={`promise-to-pay-type__${uniqueId}`}
          id={`${uniqueId}-${PromiseToPayType.RECURRING}`}
        />
        <Radio.Label>{t(I18N_COLLECTION_FORM.RECURRING)}</Radio.Label>
      </Radio>
      <Radio dataTestId="promiseToPay-Custom">
        <Radio.Input
          onChange={handleChangeRadio}
          checked={type === PromiseToPayType.MULTIPLE}
          value={PromiseToPayType.MULTIPLE}
          name={`promise-to-pay-type__${uniqueId}`}
          id={`${uniqueId}-${PromiseToPayType.MULTIPLE}`}
        />
        <Radio.Label>{t(I18N_COLLECTION_FORM.CUSTOM)}</Radio.Label>
      </Radio>
    </div>
  );
};
