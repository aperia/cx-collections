import {
  mappingSingleDetail,
  mappingRecurringDetail,
  mappingNonRecurringDetail
} from './helper';
import { PromiseToPayDetailsResponse } from './types';

describe('test mappingSingleDetail', () => {
  it('it should be return undefined', () => {
    // For coverage test case
    expect(mappingSingleDetail()).toEqual(undefined);
  });

  it('it should be return data correctly', () => {
    const dataFromApi = {
      singlePromiseData: {
        promiseAmount: '10',
        promiseDueDate: '10/10/2020'
      }
    } as PromiseToPayDetailsResponse['latestSinglePtpDetail'];
    const data = mappingSingleDetail(dataFromApi);
    expect(data).toEqual({
      amount: '10',
      paymentDate: '10/10/2020'
    });
  });
});

describe('mappingRecurringDetail', () => {
  it('it should be return undefined', () => {
    // For coverage test case
    expect(mappingRecurringDetail()).toEqual(undefined);
  });

  it('it should be return data correctly', () => {
    const dataFromApi = {
      recurringPromiseDataList: [
        {
          promiseId: '1',
          promiseDueDate: '10/10/2020',
          promiseAmount: '10',
          promiseStatus: 'test',
          promiseAmountPaid: '100',
          paymentMethod: 'test',
          lastPaymentDate: '10/11/2020'
        }
      ]
    } as PromiseToPayDetailsResponse['latestRecurringPtpDetail'];
    const response = mappingRecurringDetail(dataFromApi, [
      { description: 'test', value: 'test' }
    ]);
    expect(response).toEqual({
      dataList: [
        { paymentDate: '10/10/2020', paymentMethod: 'test', amount: '10' }
      ],
      interval: undefined,
      count: 1,
      amount: '10',
      paymentDate: '10/10/2020',
      method: { description: 'test', value: 'test' },
      endDate: '10/10/2020',
      startDate: '10/10/2020'
    });
    // for coverage because methodRefData is required
    const responseWithoutRefData = mappingRecurringDetail(dataFromApi);
    expect(responseWithoutRefData).toEqual({
        dataList: [
            { paymentDate: '10/10/2020', paymentMethod: '', amount: '10' }
          ],
          interval: undefined,
          count: 1,
          amount: '10',
          paymentDate: '10/10/2020',
          endDate: '10/10/2020',
          startDate: '10/10/2020',
          method: undefined
    })
  });
});

describe('test mappingNonRecurringDetail', () => {
  it('should be return undefined', () => {
    // For coverage test case
    expect(mappingNonRecurringDetail()).toEqual(undefined);
  });

  it('should be return correct value', () => {
    const dataFromApi = {
      nonRecurringPromiseDataList: [
        {
          promiseDueDate: '10/10/2020',
          promiseAmount: '100'
        },
        {
          promiseDueDate: '11/10/2020',
          promiseAmount: '1000'
        }
      ]
    } as PromiseToPayDetailsResponse['latestNonRecurringPtpDetail'];
    expect(mappingNonRecurringDetail(dataFromApi)).toEqual([
      { amount: '100', paymentDate: '10/10/2020' },
      { amount: '1000', paymentDate: '11/10/2020' }
    ]);
  });
});
