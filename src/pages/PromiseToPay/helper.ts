import sortBy from 'lodash.sortby';
import { INTERVAL_OPTIONS } from './constants';
import { PromiseToPayDetailsResponse } from './types';

function mappingSingleDetail(
  dataFromApi: PromiseToPayDetailsResponse['latestSinglePtpDetail'],
  methodRefData: RefDataValue[]
) {
  if (!dataFromApi) return undefined;
  return {
    amount: dataFromApi.singlePromiseData.promiseAmount,
    method: methodRefData
      ? methodRefData.find(
          refData =>
            refData.value === dataFromApi.singlePromiseData.paymentMethod
        )
      : undefined,
    paymentDate: dataFromApi?.singlePromiseData.promiseDueDate
  };
}

function mappingRecurringDetail(
  dataFromApi: PromiseToPayDetailsResponse['latestRecurringPtpDetail'],
  methodRefData: RefDataValue[]
) {
  if (!dataFromApi) return undefined;
  const dataList = sortBy(
    dataFromApi.recurringPromiseDataList,
    item => new Date(item.promiseDueDate)
  );
  return {
    dataList: dataList.map(item => ({
      paymentDate: item.promiseDueDate,
      paymentMethod: methodRefData
        ? methodRefData.find(refData => refData.value === item.paymentMethod)
            ?.description
        : '',
      amount: item.promiseAmount
    })),
    interval: INTERVAL_OPTIONS.find(
      option => option.value === dataFromApi.recurringInterval
    ),
    count: dataList.length,
    amount: dataList[0].promiseAmount,
    paymentDate: dataList[0].promiseDueDate,
    method: methodRefData
      ? methodRefData.find(
          refData => refData.value === dataList[0].paymentMethod
        )
      : undefined,
    endDate: dataList[dataList.length - 1].promiseDueDate,
    startDate: dataList[0].promiseDueDate
  };
}

function mappingNonRecurringDetail(
  dataFromApi: PromiseToPayDetailsResponse['latestNonRecurringPtpDetail'],
  methodRefData: RefDataValue[]
) {
  if (!dataFromApi) return undefined;
  const dataList = sortBy(
    dataFromApi.nonRecurringPromiseDataList,
    item => new Date(item.promiseDueDate)
  );
  return dataList.map(item => ({
    amount: item.promiseAmount,
    paymentDate: item.promiseDueDate,
    method: methodRefData
      ? methodRefData.find(refData => refData.value === item.paymentMethod)
      : undefined
  }));
}

export {
  mappingSingleDetail,
  mappingRecurringDetail,
  mappingNonRecurringDetail
};
