import './reducers';

//redux
import { createStore, Store } from '@reduxjs/toolkit';
import { rootReducer } from 'storeConfig';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

//api
import {
  triggerUpdatePromiseToPay,
  updatePromiseToPayRequest
} from './updatePromiseToPay';

//services
import promiseToPayService from '../promiseToPayService';

import {
  responseDefault,
  byPassFulfilled,
  mockActionCreator,
  byPassRejected,
  storeId,
  accEValue
} from 'app/test-utils';
import { PromiseToPayType } from '../types';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';
import { promiseToPayActions } from './reducers';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

let spy: jest.SpyInstance;

let store: Store<RootState>;
let state: RootState;

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

const mockDay = new Date('2099-12-31');

const mockData = {
  amount: '30',
  method: { value: 'check', description: 'check' },
  paymentDate: mockDay
};

describe('updatePromiseToPayRequest', () => {
  const mockPayloadSuccess = { message: 'success' };
  const mockPayloadFailure = { status: 'failure' };
  describe('PromiseToPayType Single', () => {
    it('call promise to pay single success with all default value', async () => {
      store = createStore(rootReducer, {
        promiseToPay: { [storeId]: {} },
        accountDetail: {
          [storeId]: {
            data: {
              cardHolderSecondaryName: 'test'
            }
          }
        }
      });
      state = store.getState();
      jest
        .spyOn(promiseToPayService, 'addSinglePromiseToPay')
        .mockResolvedValue({ ...responseDefault, data: mockPayloadSuccess });
      const response = await updatePromiseToPayRequest({
        storeId,
        postData: {
          common: { accountId: accEValue },
          data: {
            amount: undefined, // For coverage
            method: mockData.method
          },
          promiseToPayType: PromiseToPayType.SINGLE
        }
      })(store.dispatch, store.getState, {});

      expect(response.type).toEqual(
        'promiseToPay/updatePromiseToPayRequest/fulfilled'
      );
    });

    it('call promise to pay single success', async () => {
      store = createStore(rootReducer, {
        promiseToPay: { [storeId]: {} },
        accountDetail: {}
      });
      state = store.getState();
      jest
        .spyOn(promiseToPayService, 'addSinglePromiseToPay')
        .mockResolvedValue({ ...responseDefault, data: mockPayloadSuccess });
      const response = await updatePromiseToPayRequest({
        storeId,
        postData: {
          common: { accountId: accEValue },
          data: {
            amount: mockData.amount,
            paymentDate: mockData.paymentDate,
            method: mockData.method
          },
          promiseToPayType: PromiseToPayType.SINGLE
        }
      })(store.dispatch, store.getState, {});

      expect(response.type).toEqual(
        'promiseToPay/updatePromiseToPayRequest/fulfilled'
      );
    });

    it('call promise to pay single fail', async () => {
      store = createStore(rootReducer, {
        promiseToPay: { [storeId]: {} },
        accountDetail: {}
      });
      state = store.getState();
      jest
        .spyOn(promiseToPayService, 'addSinglePromiseToPay')
        .mockResolvedValue({ ...responseDefault, data: mockPayloadFailure });
      const response = await updatePromiseToPayRequest({
        storeId,
        postData: {
          common: { accountId: accEValue },
          data: {
            amount: mockData.amount,
            paymentDate: mockData.paymentDate,
            method: mockData.method
          },
          promiseToPayType: PromiseToPayType.SINGLE
        }
      })(store.dispatch, store.getState, {});

      expect(response.type).toEqual(
        'promiseToPay/updatePromiseToPayRequest/rejected'
      );
    });
  });

  describe('PromiseToPayType Recurring', () => {
    const postData = {
      common: { accountId: accEValue },
      data: {
        paymentDate: mockData.paymentDate,
        amount: mockData.amount,
        method: mockData.method,
        interval: { value: '123', description: '123' },
        count: { value: '123', description: '123' }
      },
      promiseToPayType: PromiseToPayType.RECURRING
    };

    it('call promise to pay Recurring success with all default value', async () => {
      store = createStore(rootReducer, {
        promiseToPay: { [storeId]: {} },
        accountDetail: {
          [storeId]: {}
        }
      });
      state = store.getState();
      jest
        .spyOn(promiseToPayService, 'addRecurringPromiseToPay')
        .mockResolvedValue({ ...responseDefault, data: mockPayloadSuccess });
      const responseData = {
        ...postData,
        data: { ...postData.data, paymentDate: undefined, amount: undefined }
      };

      const response = await updatePromiseToPayRequest({
        storeId,
        postData: responseData
      })(store.dispatch, store.getState, {});

      expect(response.type).toEqual(
        'promiseToPay/updatePromiseToPayRequest/fulfilled'
      );
    });

    it('call promise to pay Recurring success', async () => {
      store = createStore(rootReducer, {
        promiseToPay: { [storeId]: {} },
        accountDetail: {}
      });
      state = store.getState();
      jest
        .spyOn(promiseToPayService, 'addRecurringPromiseToPay')
        .mockResolvedValue({ ...responseDefault, data: mockPayloadSuccess });
      const response = await updatePromiseToPayRequest({
        storeId,
        postData
      })(store.dispatch, store.getState, {});
      expect(response.type).toEqual(
        'promiseToPay/updatePromiseToPayRequest/fulfilled'
      );
    });

    it('call promise to pay Recurring fail', async () => {
      store = createStore(rootReducer, {
        promiseToPay: { [storeId]: {} },
        accountDetail: {}
      });
      state = store.getState();
      jest
        .spyOn(promiseToPayService, 'addRecurringPromiseToPay')
        .mockResolvedValue({ ...responseDefault, data: mockPayloadFailure });
      const response = await updatePromiseToPayRequest({
        storeId,
        postData
      })(store.dispatch, store.getState, {});

      expect(response.type).toEqual(
        'promiseToPay/updatePromiseToPayRequest/rejected'
      );
    });
  });

  describe('PromiseToPayType Custom', () => {
    const postData = {
      common: { accountId: accEValue },
      data: [
        {
          paymentDate: mockData.paymentDate,
          amount: mockData.amount,
          method: mockData.method
        }
      ],
      promiseToPayType: PromiseToPayType.MULTIPLE
    };

    it('call promise to pay Recurring success with all default value', async () => {
      store = createStore(rootReducer, {
        promiseToPay: { [storeId]: {} },
        accountDetail: {
          [storeId]: {}
        }
      });
      state = store.getState();
      jest
        .spyOn(promiseToPayService, 'addCustomPromiseToPay')
        .mockResolvedValue({ ...responseDefault, data: mockPayloadSuccess });
      const responseData = {
        ...postData,
        data: [
          { paymentDate: undefined, amount: undefined, method: mockData.method }
        ]
      };

      const response = await updatePromiseToPayRequest({
        storeId,
        postData: responseData
      })(store.dispatch, store.getState, {});

      expect(response.type).toEqual(
        'promiseToPay/updatePromiseToPayRequest/fulfilled'
      );
    });
    it('call promise to pay Custom success', async () => {
      store = createStore(rootReducer, {
        promiseToPay: { [storeId]: {} },
        accountDetail: {}
      });
      state = store.getState();
      jest
        .spyOn(promiseToPayService, 'addCustomPromiseToPay')
        .mockResolvedValue({ ...responseDefault, data: mockPayloadSuccess });
      const response = await updatePromiseToPayRequest({
        storeId,
        postData
      })(store.dispatch, store.getState, {});
      expect(response.type).toEqual(
        'promiseToPay/updatePromiseToPayRequest/fulfilled'
      );
    });

    it('call promise to pay Recurring fail', async () => {
      store = createStore(rootReducer, {
        promiseToPay: { [storeId]: {} },
        accountDetail: {}
      });
      state = store.getState();
      jest
        .spyOn(promiseToPayService, 'addCustomPromiseToPay')
        .mockResolvedValue({ ...responseDefault, data: mockPayloadFailure });
      const response = await updatePromiseToPayRequest({
        storeId,
        postData
      })(store.dispatch, store.getState, {});

      expect(response.type).toEqual(
        'promiseToPay/updatePromiseToPayRequest/rejected'
      );
    });
  });
});

describe('triggerUpdatePromiseToPay', () => {
  const mActionsToast = mockActionCreator(actionsToast);
  const mCollectionFormActions = mockActionCreator(collectionFormActions);
  const mAccountDetailActions = mockActionCreator(accountDetailActions);
  const mApiErrorNotificationAction = mockActionCreator(
    apiErrorNotificationAction
  );
  const mPromiseToPayActions = mockActionCreator(promiseToPayActions);
  it('single fulfilled', async () => {
    const mockAddToast = mActionsToast('addToast');
    mCollectionFormActions('setCurrentView');
    mCollectionFormActions('selectCallResult');
    mAccountDetailActions('getAccountDetail');
    mApiErrorNotificationAction('clearApiErrorData');
    mCollectionFormActions('toggleCallResultInProgressStatus');
    mPromiseToPayActions('getPromiseToPayData');
    await triggerUpdatePromiseToPay({
      storeId,
      postData: {
        accountId: accEValue,
        data: {
          amount: mockData.amount,
          method: mockData.method,
          paymentDate: mockData.paymentDate
        },
        promiseToPayType: PromiseToPayType.SINGLE,
        memberSequenceIdentifier: '',
        socialSecurityIdentifier: ''
      }
    })(
      byPassFulfilled(updatePromiseToPayRequest.fulfilled.type),
      store.getState,
      {}
    );
    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'success',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
    });
  });

  it('multiple rejected', async () => {
    const mockAddToast = mActionsToast('addToast');
    mApiErrorNotificationAction('updateApiError');

    await triggerUpdatePromiseToPay({
      storeId,
      postData: {
        accountId: accEValue,
        data: [
          {
            amount: mockData.amount,
            method: mockData.method,
            paymentDate: mockData.paymentDate
          }
        ],
        promiseToPayType: PromiseToPayType.MULTIPLE,
        memberSequenceIdentifier: '',
        socialSecurityIdentifier: ''
      }
    })(
      byPassRejected(updatePromiseToPayRequest.rejected.type),
      store.getState,
      {}
    );
    expect(mockAddToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMIT_FAILED
    });
  });
});

describe('should be triggerUpdatePromiseToPayBuilder', () => {
  it('should response triggerUpdatePromiseToPay pending', () => {
    const pendingAction = triggerUpdatePromiseToPay.pending(
      triggerUpdatePromiseToPay.pending.type,
      {
        storeId,
        postData: {
          data: mockData,
          accountId: accEValue,
          promiseToPayType: PromiseToPayType.SINGLE,
          memberSequenceIdentifier: '',
          socialSecurityIdentifier: ''
        }
      }
    );
    const actual = rootReducer(state, pendingAction);

    expect(actual.promiseToPay?.[storeId].loading).toEqual(true);
  });

  it('should response triggerUpdatePromiseToPay fulfilled', () => {
    const fulfilled = triggerUpdatePromiseToPay.fulfilled(
      null,
      triggerUpdatePromiseToPay.pending.type,
      {
        storeId,
        postData: {
          data: mockData,
          accountId: accEValue,
          promiseToPayType: PromiseToPayType.SINGLE,
          memberSequenceIdentifier: '',
          socialSecurityIdentifier: ''
        }
      }
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual.promiseToPay?.[storeId].loading).toEqual(false);
  });

  it('should response triggerUpdatePromiseToPay rejected', () => {
    const rejected = triggerUpdatePromiseToPay.rejected(
      null,
      triggerUpdatePromiseToPay.rejected.type,
      {
        storeId,
        postData: {
          data: mockData,
          accountId: accEValue,
          promiseToPayType: PromiseToPayType.SINGLE,
          memberSequenceIdentifier: '',
          socialSecurityIdentifier: ''
        }
      }
    );
    const actual = rootReducer(state, rejected);

    expect(actual.promiseToPay?.[storeId].loading).toEqual(false);
  });
});
