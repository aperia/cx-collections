import { createSelector } from '@reduxjs/toolkit';
import { EMPTY_OBJECT } from 'app/constants';
import { PromiseToPayPayload } from '../types';

export const selectPromiseToPayInfo = createSelector(
  (states: RootState, storeId: string) => {
    const data = states.promiseToPay[storeId]?.data as PromiseToPayPayload;
    return data;
  },
  (data: object) => data
);

export const selectPromiseToPayLoading = createSelector(
  (states: RootState, storeId: string) => {
    return states.promiseToPay[storeId]?.loading ?? false;
  },
  (data: boolean) => data
);

export const selectPromiseToPayDirtyForm = createSelector(
  (states: RootState, storeId: string) => {
    return states.promiseToPay[storeId]?.isFormDirty ?? false;
  },
  (data: boolean) => data
);

export const selectPromiseToPayValidForm = createSelector(
  (states: RootState, storeId: string) => {
    return states.promiseToPay[storeId]?.isFormValid ?? false;
  },
  (data: boolean) => data
);

export const selectPromiseToPayMethodRefData = createSelector(
  (states: RootState, storeId: string) => {
    return states.promiseToPay[storeId]?.methodRefData ?? [];
  },
  (data: RefDataValue[]) => data
);

export const selectPromiseToPayDetailType = createSelector(
  (states: RootState, storeId: string) => {
    return states.promiseToPay[storeId]?.promiseToPayData?.type ?? null;
  },
  data => data
);

export const selectPromiseToPayDetailData = createSelector(
  (states: RootState, storeId: string) => {
    return states.promiseToPay[storeId]?.promiseToPayData?.data;
  },
  data => data
);

export const selectPromiseToPayConfig = createSelector(
  (states: RootState, storeId: string) => {
    return states.promiseToPay[storeId]?.config;
  },
  data => data ?? EMPTY_OBJECT
);
