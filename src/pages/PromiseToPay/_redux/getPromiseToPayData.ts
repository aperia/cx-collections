import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import isEqual from 'lodash.isequal';
import isEmpty from 'lodash.isempty';
// Service
import promiseToPayService from '../promiseToPayService';

// Type
import {
  PromiseToPayDetailData,
  PromiseToPayDetailsResponse,
  PromiseToPayDetailType,
  PromiseToPayNonRecurringDetail,
  PromiseToPayRecurringDetail,
  PromiseToPaySingleDetail,
  PromiseToPayState
} from '../types';

// Helpers
import { dispatchDestroyDelayProgress } from 'pages/CollectionForm/helpers';

// Const
import { EMPTY_STRING } from 'app/constants';

// Actions
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import {
  mappingNonRecurringDetail,
  mappingRecurringDetail,
  mappingSingleDetail
} from '../helper';
import { mappingDataFromObj } from 'app/helpers';

export const getPromiseToPayData = createAsyncThunk<
  {
    promiseToPayType: PromiseToPayDetailType;
    promiseToPayData: PromiseToPayDetailData;
  },
  StoreIdPayload,
  ThunkAPIConfig
>('promiseToPay/getPromiseToPayData', async (args, thunkAPI) => {
  const { getState, dispatch } = thunkAPI;
  const { collectionForm, mapping } = getState();

  const { storeId } = args;
  const { commonConfig } = window.appConfig || {};
  const { inProgressInfo = {}, isInProgress } = collectionForm[storeId];
  const { accountDetail } = thunkAPI.getState();
  const selectedCaller = accountDetail[storeId]?.selectedCaller;

  const eValueAccountId = accountDetail[storeId]?.data?.eValueAccountId;

  const requestBody = {
    common: {
      org: commonConfig?.org || EMPTY_STRING,
      app: commonConfig?.app || EMPTY_STRING,
      accountId: eValueAccountId!
    },
    callingApplication: commonConfig?.callingApplication || EMPTY_STRING,
    operatorCode: commonConfig?.operatorID || EMPTY_STRING,
    cardholderContactedName: selectedCaller?.customerName ?? ''
  };

  const { data: methodRefData } =
    await promiseToPayService.getPromiseToPayMethod();

  const { data } = await promiseToPayService.getPromiseToPayData(requestBody);
  const mappedData = mappingDataFromObj<PromiseToPayDetailsResponse>(
    data,
    mapping?.data?.promiseToPay ?? {}
  );

  const {
    latestSinglePromiseDetailStatus,
    latestNonRecurringPromiseDetailStatus,
    latestRecurringPromiseDetailStatus,
    latestSinglePtpDetail,
    latestRecurringPtpDetail,
    latestNonRecurringPtpDetail
  } = mappedData;
  const promiseToPayType: PromiseToPayDetailType = (() => {
    if (latestSinglePromiseDetailStatus === 'ACTIVE') return 'Single';
    if (latestRecurringPromiseDetailStatus === 'ACTIVE') return 'Recurring';
    if (latestNonRecurringPromiseDetailStatus === 'ACTIVE')
      return 'NonRecurring';
    return null;
  })();
  const promiseToPayData:
    | PromiseToPaySingleDetail
    | PromiseToPayRecurringDetail
    | PromiseToPayNonRecurringDetail
    | undefined = (() => {
    switch (promiseToPayType) {
      case 'Single':
        return mappingSingleDetail(latestSinglePtpDetail, methodRefData);
      case 'Recurring':
        return mappingRecurringDetail(latestRecurringPtpDetail, methodRefData);
      case 'NonRecurring':
        return mappingNonRecurringDetail(
          latestNonRecurringPtpDetail,
          methodRefData
        );

      default:
        return undefined;
    }
  })();

  if (
    isEqual({ promiseToPayType }, inProgressInfo) &&
    !isEmpty(inProgressInfo) &&
    isInProgress
  ) {
    dispatch(
      collectionFormActions.toggleCallResultInProgressStatus({
        storeId,
        info: { promiseToPayType }
      })
    );
    dispatchDestroyDelayProgress(storeId);
  }

  return { promiseToPayType, promiseToPayData };
});

export const getPromiseToPayDataBuilder = (
  builder: ActionReducerMapBuilder<PromiseToPayState>
) => {
  builder
    .addCase(getPromiseToPayData.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: true
      };
    })
    .addCase(getPromiseToPayData.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { promiseToPayType, promiseToPayData } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: false,
        promiseToPayData: {
          type: promiseToPayType,
          data: promiseToPayData
        }
      };
    })
    .addCase(getPromiseToPayData.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = { ...draftState[storeId], loading: false };
    });
};
