import { createStore } from '@reduxjs/toolkit';
import { rootReducer } from 'storeConfig';
import { getPromiseToPayConfig } from './getPromiseToPayConfig';
import { accEValue, responseDefault, storeId } from 'app/test-utils';
import promiseToPayService from '../promiseToPayService';
import { DEFAULT_PROMISE_TO_PAY_CONFIG } from '../constants';

const mockRequestData = {
  accId: accEValue,
  storeId
};

const initialState: Partial<RootState> = {
  mapping: {
    loading: false,
    data: {}
  }
};

describe('getPromiseToPayConfig', () => {
  const data = {
    clientInfo: {
      promiseConfigList: [
        { name: 'name_1', value: 'value_1', active: true },
        { name: 'name_2', value: 'value_2', active: false }
      ]
    }
  };

  it('call api', async () => {
    jest
      .spyOn(promiseToPayService, 'getPromiseToPayConfig')
      .mockResolvedValue({ ...responseDefault, data });

    const store = createStore(rootReducer, initialState);

    const response = await getPromiseToPayConfig(mockRequestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({});
  });

  it('call with api return empty clientInfo', async () => {
    jest
      .spyOn(promiseToPayService, 'getPromiseToPayConfig')
      .mockResolvedValue({ ...responseDefault, data: { clientInfo: {} } });

    const store = createStore(rootReducer, initialState);

    const response = await getPromiseToPayConfig(mockRequestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({});
  });
});

describe('getPromiseToPayConfigBuilder ', () => {
  let store;
  let state: RootState;
  const mockPayload = {};
  const generateStoreState = (initialState?: Partial<RootState>) => {
    store = createStore(rootReducer, initialState ?? {});
    state = store.getState();
  };

  it('pending', () => {
    generateStoreState();
    const payload = getPromiseToPayConfig.pending(
      getPromiseToPayConfig.pending.type,
      mockRequestData
    );
    const actual = rootReducer(state, payload);

    expect(actual.promiseToPay[storeId].loading).toEqual(true);
  });

  it('fulfilled', () => {
    generateStoreState({
      promiseToPay: { [storeId]: { loading: true } }
    });
    const payload = getPromiseToPayConfig.fulfilled(
      mockPayload,
      getPromiseToPayConfig.fulfilled.type,
      mockRequestData
    );
    const actual = rootReducer(state, payload);
    expect(actual.promiseToPay[storeId].loading).toEqual(false);
    expect(actual.promiseToPay[storeId].config).toEqual(
      DEFAULT_PROMISE_TO_PAY_CONFIG
    );
  });

  it('rejected', () => {
    generateStoreState({
      promiseToPay: { [storeId]: { loading: true } }
    });
    const payload = getPromiseToPayConfig.rejected(
      { name: 'ErrorName', message: 'error message' },
      getPromiseToPayConfig.rejected.type,
      mockRequestData
    );
    const actual = rootReducer(state, payload);
    expect(actual.promiseToPay[storeId].loading).toEqual(false);
  });
});
