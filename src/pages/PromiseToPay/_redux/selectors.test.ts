import { selectorWrapper, storeId } from 'app/test-utils';
import {
  selectPromiseToPayConfig,
  selectPromiseToPayDirtyForm,
  selectPromiseToPayInfo,
  selectPromiseToPayLoading,
  selectPromiseToPayMethodRefData,
  selectPromiseToPayDetailData,
  selectPromiseToPayValidForm,
  selectPromiseToPayDetailType
} from './selectors';
import { EMPTY_OBJECT } from 'app/constants';

const mockPayload = {
  amount: '12',
  method: { value: 'check', description: 'check' },
  paymentDate: new Date()
};

const states: Partial<RootState> = {
  promiseToPay: {
    [storeId]: {
      data: mockPayload,
      loading: true,
      isFormDirty: true,
      isFormValid: true,
      methodRefData: [{ value: '123', description: '123' }],
      config: { minAmountPerPromise: 10, maxAmountPerPromise: 10 },
      promiseToPayData: {type: 'Single', data: []}
    }
  }
};

const selectorTrigger = selectorWrapper(states);

describe('Promise to pay selector', () => {
  it('selectPromiseToPayInfo', () => {
    const { data, emptyData } = selectorTrigger(selectPromiseToPayInfo);
    expect(emptyData).toEqual(undefined);
    expect(data).toEqual(mockPayload);
  });

  it('selectPromiseToPayLoading', () => {
    const { data, emptyData } = selectorTrigger(selectPromiseToPayLoading);
    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('select form dirty', () => {
    const { data, emptyData } = selectorTrigger(selectPromiseToPayDirtyForm);
    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('select form valid', () => {
    const { data, emptyData } = selectorTrigger(selectPromiseToPayValidForm);
    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('select promise to pay method', () => {
    const { data, emptyData } = selectorTrigger(
      selectPromiseToPayMethodRefData
    );
    expect(data).toEqual([{ value: '123', description: '123' }]);
    expect(emptyData).toEqual([]);
  });

  it('select selectPromiseToPayConfig ', () => {
    const { data, emptyData } = selectorTrigger(selectPromiseToPayConfig);
    expect(data).toEqual({ minAmountPerPromise: 10, maxAmountPerPromise: 10 });
    expect(emptyData).toEqual(EMPTY_OBJECT);
  });

  it('select selectPromiseToPayDetailData ', () => {
    const { data, emptyData } = selectorTrigger(selectPromiseToPayDetailData);
    expect(data).toEqual([]);
    expect(emptyData).toEqual(undefined);
  });

  it('select selectPromiseToPayDetailType ', () => {
    const { data, emptyData } = selectorTrigger(selectPromiseToPayDetailType);
    expect(data).toEqual('Single');
    expect(emptyData).toEqual(null);
  });
});
