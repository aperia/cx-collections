import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// Service
import promiseToPayService from '../promiseToPayService';

// Type
import { PromiseToPayState } from '../types';

export const getPromiseToPayMethodRefData = createAsyncThunk<
  RefDataValue[],
  StoreIdPayload,
  ThunkAPIConfig
>('promiseToPay/getPromiseToPayMethodRefData', async (args, thunkAPI) => {
  const { data } = await promiseToPayService.getPromiseToPayMethod();

  return data;
});

export const getPromiseToPayMethodRefDataBuilder = (
  builder: ActionReducerMapBuilder<PromiseToPayState>
) => {
  builder
    .addCase(getPromiseToPayMethodRefData.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: true,
        methodRefData: []
      };
    })
    .addCase(getPromiseToPayMethodRefData.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].loading = false;
      draftState[storeId].methodRefData = action.payload;
    })
    .addCase(getPromiseToPayMethodRefData.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].methodRefData = [];
      draftState[storeId].loading = false;
    });
};
