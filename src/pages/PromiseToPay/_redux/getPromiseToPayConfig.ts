import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { cleanObj, mappingDataFromObj } from 'app/helpers';
import { selectClientInfoGetByIdRequest } from 'pages/__commons/AccountDetailTabs/_redux';
import { DEFAULT_PROMISE_TO_PAY_CONFIG } from '../constants';

// Service
import promiseToPayService from '../promiseToPayService';

// Type
import { PromiseToPayState } from '../types';

export const getPromiseToPayConfig = createAsyncThunk<
  Record<string, number>,
  StoreIdPayload,
  ThunkAPIConfig
>('promiseToPay/getPromiseToPayConfig', async (args, thunkAPI) => {
  const { getState } = thunkAPI;
  const { mapping } = getState();

  const commonClientInfoRequest = selectClientInfoGetByIdRequest(
    getState(),
    args.storeId
  );

  const { data } = await promiseToPayService.getPromiseToPayConfig({
    ...commonClientInfoRequest,
    fieldsToInclude: ['promiseConfigList'],
    enableFallback: true
  });

  const returnedPromiseConfigList = data.clientInfo.promiseConfigList;
  if (!returnedPromiseConfigList) return {};

  /* Example
   * format from:
   * [{name: "Min Amount per Promise", value: "32", active: true}]
   * to:
   * {"Min Amount per Promise": 32}
   **/
  const promiseConfigListNormalized: Record<string, number | undefined> = {};
  returnedPromiseConfigList.forEach(
    (config: { name: string; value: string; active: boolean }) => {
      promiseConfigListNormalized[config.name] =
        config.value && config.active ? parseInt(config.value, 10) : undefined;
    }
  );
  const mappedData = mappingDataFromObj(
    promiseConfigListNormalized,
    mapping.data.promiseToPayConfig ?? {}
  );
  return cleanObj(mappedData);
});

export const getPromiseToPayConfigBuilder = (
  builder: ActionReducerMapBuilder<PromiseToPayState>
) => {
  builder
    .addCase(getPromiseToPayConfig.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: true
      };
    })
    .addCase(getPromiseToPayConfig.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].loading = false;
      draftState[storeId].config = Object.assign(
        {},
        DEFAULT_PROMISE_TO_PAY_CONFIG,
        action.payload
      );
    })
    .addCase(getPromiseToPayConfig.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].config = DEFAULT_PROMISE_TO_PAY_CONFIG;
      draftState[storeId].loading = false;
    });
};
