import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// Service
import promiseToPayService from '../promiseToPayService';

// Types
import {
  PromiseToPayRecurringData,
  PromiseToPaySingleData,
  PromiseToPayState,
  PromiseToPayType,
  TriggerPromiseToPayUpdateArg,
  UpdatePromiseToPayRequestArg,
  UpdatePromiseToPayRequestPayload
} from '../types';

// Const
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { EMPTY_STRING } from 'app/constants';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { FormatTime } from 'app/constants/enums';

// Helper
import { formatTimeDefault } from 'app/helpers';
import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';
import {
  dispatchDelayProgress,
  setDelayProgress
} from 'pages/CollectionForm/helpers';

// Actions
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { promiseToPayActions } from './reducers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

export const updatePromiseToPayRequest = createAsyncThunk<
  UpdatePromiseToPayRequestPayload,
  UpdatePromiseToPayRequestArg,
  ThunkAPIConfig
>('promiseToPay/updatePromiseToPayRequest', async (args, thunkAPI) => {
  const { storeId } = args;
  const { promiseToPayType, ...requestData } = args.postData;
  const { accountDetail } = thunkAPI.getState();
  const selectedCaller = accountDetail[storeId]?.selectedCaller;
  const { commonConfig } = window.appConfig || {};

  try {
    const commonRequest = {
      common: {
        app: commonConfig?.app || EMPTY_STRING,
        org: commonConfig?.org || EMPTY_STRING,
        accountId: requestData.common.accountId
      },
      callingApplication: commonConfig?.callingApplication || EMPTY_STRING,
      operatorCode: commonConfig?.operatorID || EMPTY_STRING,
      cardholderContactedName: selectedCaller?.customerName ?? ''
    };
    if (promiseToPayType === PromiseToPayType.SINGLE) {
      const { paymentDate, amount, method } =
        requestData.data as PromiseToPaySingleData;
      const response = await promiseToPayService.addSinglePromiseToPay({
        ...commonRequest,
        promiseToPayDate:
          formatTimeDefault(paymentDate!, FormatTime.UTCDate) ?? '',
        promiseToPayAmount: `${Math.floor(+(amount ?? 0))}`,
        cardholderEntityType:
          accountDetail[storeId]?.data?.separateEntityIndicator,
        promiseToPayPaymentMethod: method!.value
      });

      if (response?.data.status === 'failure') {
        throw response;
      }
      return response.data;
    }

    if (promiseToPayType === PromiseToPayType.RECURRING) {
      const { amount, method, paymentDate, interval, count } =
        requestData.data as PromiseToPayRecurringData;
      const response = await promiseToPayService.addRecurringPromiseToPay({
        ...commonRequest,
        promiseToPayStartDate:
          formatTimeDefault(paymentDate!, FormatTime.UTCDate) ?? '',
        promiseToPayAmount: `${Math.floor(+(amount ?? 0))}`,
        promiseToPayPaymentMethod: method!.value,
        promiseToPayPaymentInterval: interval!.value,
        promiseToPayPaymentCount: count!.value
      });

      if (response?.data.status === 'failure') {
        throw response;
      }
      return response.data;
    }

    // custom promise to pay
    const res = await promiseToPayService.addCustomPromiseToPay({
      ...commonRequest,
      promiseList: (requestData.data as PromiseToPaySingleData[]).map(
        promiseData => {
          const { paymentDate, amount, method } = promiseData;
          return {
            promiseToPayDate:
              formatTimeDefault(paymentDate!, FormatTime.UTCDate) ?? '',
            promiseToPayAmount: `${Math.floor(+(amount ?? 0))}`,
            promiseToPayPaymentMethod: method!.value
          };
        }
      )
    });

    if (res?.data.status === 'failure') {
      throw res;
    }
    return res.data;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerUpdatePromiseToPay = createAsyncThunk<
  unknown,
  TriggerPromiseToPayUpdateArg,
  ThunkAPIConfig
>('promiseToPay/triggerUpdatePromiseToPay', async (args, thunkAPI) => {
  const {
    storeId,

    postData: {
      data,
      accountId,
      promiseToPayType,
      memberSequenceIdentifier,
      socialSecurityIdentifier
    }
  } = args;
  const { dispatch, getState } = thunkAPI;

  const eValueAccountId =
    getState().accountDetail[storeId]?.data?.eValueAccountId;

  const response = await dispatch(
    updatePromiseToPayRequest({
      storeId,
      postData: {
        common: {
          accountId: eValueAccountId!
        },
        callResultType: CALL_RESULT_CODE.PROMISE_TO_PAY,
        data,
        promiseToPayType
      }
    })
  );

  if (isFulfilled(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMITTED
        })
      );
      //
      dispatch(collectionFormActions.setCurrentView({ storeId }));
      dispatch(
        collectionFormActions.selectCallResult({ storeId, item: undefined })
      );

      // reload accountDetail Data to get newest nextWorkDate
      dispatch(
        accountDetailActions.getAccountDetail({
          storeId,
          postData: {
            common: {
              accountId
            },
            inquiryFields: {
              memberSequenceIdentifier
            }
          },
          socialSecurityIdentifier,
          isReload: true
        })
      );

      // API Error
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inCollectionFormFlyOut'
        })
      );
    });

    dispatch(
      collectionFormActions.toggleCallResultInProgressStatus({
        storeId,
        info: { promiseToPayType },
        lastDelayCallResult: CALL_RESULT_CODE.PROMISE_TO_PAY
      })
    );

    dispatchDelayProgress(
      promiseToPayActions.getPromiseToPayData({
        storeId
      }),
      storeId
    );

    setDelayProgress(storeId, CALL_RESULT_CODE.PROMISE_TO_PAY, {
      promiseToPayType
    });
  }

  if (isRejected(response)) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_COLLECTION_FORM.TOAST_COLLECTION_FORM_SUBMIT_FAILED
        })
      );
      // API Error
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inCollectionFormFlyOut',
          apiResponse: response?.payload?.response
        })
      );
    });
  }
});

export const triggerUpdatePromiseToPayBuilder = (
  builder: ActionReducerMapBuilder<PromiseToPayState>
) => {
  builder
    .addCase(triggerUpdatePromiseToPay.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: true
      };
    })
    .addCase(triggerUpdatePromiseToPay.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].loading = false;
    })
    .addCase(triggerUpdatePromiseToPay.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId].loading = false;
    });
};
