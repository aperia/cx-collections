import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { PromiseToPayState, UpdateDataAction } from '../types';
import {
  triggerUpdatePromiseToPay,
  triggerUpdatePromiseToPayBuilder,
  updatePromiseToPayRequest
} from './updatePromiseToPay';

import {
  getPromiseToPayData,
  getPromiseToPayDataBuilder
} from './getPromiseToPayData';
import {
  getPromiseToPayConfig,
  getPromiseToPayConfigBuilder
} from './getPromiseToPayConfig';
import {
  getPromiseToPayMethodRefData,
  getPromiseToPayMethodRefDataBuilder
} from './getPromiseToPayMethodRefData';

const initialState: PromiseToPayState = {};

const { actions, reducer } = createSlice({
  name: 'promiseToPay',
  initialState,
  reducers: {
    updateData: (draftState, action: PayloadAction<UpdateDataAction>) => {
      const { storeId, data } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        ...data
      };
    },
    setValidForm: (
      draftState,
      action: PayloadAction<StoreIdPayload & { isValid: boolean }>
    ) => {
      const { storeId, isValid } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isFormValid: isValid
      };
    },
    setDirtyForm: (
      draftState,
      action: PayloadAction<StoreIdPayload & { dirty: boolean }>
    ) => {
      const { storeId, dirty } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isFormDirty: dirty
      };
    },
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    }
  },
  extraReducers: builder => {
    triggerUpdatePromiseToPayBuilder(builder);
    getPromiseToPayDataBuilder(builder);
    getPromiseToPayConfigBuilder(builder);
    getPromiseToPayMethodRefDataBuilder(builder);
  }
});

const allActions = {
  ...actions,
  updatePromiseToPayRequest,
  triggerUpdatePromiseToPay,
  getPromiseToPayData,
  getPromiseToPayConfig,
  getPromiseToPayMethodRefData
};

export { allActions as promiseToPayActions, reducer };
