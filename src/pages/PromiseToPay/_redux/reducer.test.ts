import { promiseToPayActions, reducer } from './reducers';

// utils
import { storeId } from 'app/test-utils';

describe('Test reducer', () => {
  const mockPayload = {
    amount: '12',
    method: { value: 'check', description: 'check' },
    paymentDate: new Date()
  };
  it('updateData', () => {
    const state = reducer(
      {},
      promiseToPayActions.updateData({ storeId, data: mockPayload })
    );

    expect(state).toEqual({
      [storeId]: { ...mockPayload }
    });
  });

  it('removeStore', () => {
    const state = reducer(
      {
        [storeId]: { data: mockPayload }
      },
      promiseToPayActions.removeStore({ storeId })
    );

    expect(state).toEqual({});
  });

  it('setValidForm', () => {
    const state = reducer(
      {
        [storeId]: { data: mockPayload }
      },
      promiseToPayActions.setValidForm({ storeId, isValid: true })
    );

    expect(state).toEqual({
      [storeId]: {
        data: mockPayload,
        isFormValid: true
      }
    });
  });

  it('setDirtyForm', () => {
    const state = reducer(
      {
        [storeId]: { data: mockPayload }
      },
      promiseToPayActions.setDirtyForm({ storeId, dirty: true })
    );

    expect(state).toEqual({
      [storeId]: {
        data: mockPayload,
        isFormDirty: true
      }
    });
  });
});
