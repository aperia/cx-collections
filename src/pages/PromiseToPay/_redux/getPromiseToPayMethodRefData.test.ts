import { createStore } from '@reduxjs/toolkit';
import { rootReducer } from 'storeConfig';
import { getPromiseToPayMethodRefData } from './getPromiseToPayMethodRefData';
import { responseDefault, storeId } from 'app/test-utils';
import promiseToPayService from '../promiseToPayService';

const mockRequestData = {
  storeId
};

describe('getPromiseToPayMethodRefData', () => {
  it('call api', async () => {
    jest
      .spyOn(promiseToPayService, 'getPromiseToPayMethod')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const store = createStore(rootReducer, {});

    const response = await getPromiseToPayMethodRefData(mockRequestData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({});
  });
});

describe('getPromiseToPayMethodRefDataBuilder ', () => {
  let store;
  let state: RootState;
  const mockPayload = [{ value: 'check', description: 'check' }];
  const generateStoreState = (initialState?: Partial<RootState>) => {
    store = createStore(rootReducer, initialState ?? {});
    state = store.getState();
  };

  it('pending', () => {
    generateStoreState();
    const payload = getPromiseToPayMethodRefData.pending(
      getPromiseToPayMethodRefData.pending.type,
      mockRequestData
    );
    const actual = rootReducer(state, payload);

    expect(actual.promiseToPay[storeId].loading).toEqual(true);
  });

  it('fulfilled', () => {
    generateStoreState({
      promiseToPay: { [storeId]: { loading: true } }
    });
    const payload = getPromiseToPayMethodRefData.fulfilled(
      mockPayload,
      getPromiseToPayMethodRefData.fulfilled.type,
      mockRequestData
    );
    const actual = rootReducer(state, payload);
    expect(actual.promiseToPay[storeId].loading).toEqual(false);
    expect(actual.promiseToPay[storeId].promiseToPaySingleData).toEqual(
      undefined
    );
  });

  it('rejected', () => {
    generateStoreState({
      promiseToPay: { [storeId]: { loading: true } }
    });
    const payload = getPromiseToPayMethodRefData.rejected(
      { name: 'ErrorName', message: 'error message' },
      getPromiseToPayMethodRefData.rejected.type,
      mockRequestData
    );
    const actual = rootReducer(state, payload);
    expect(actual.promiseToPay[storeId].loading).toEqual(false);
  });
});
