import { createStore } from '@reduxjs/toolkit';
import { rootReducer } from 'storeConfig';
import { getPromiseToPayData } from './getPromiseToPayData';
import {
  accEValue,
  responseDefault,
  storeId,
  mockActionCreator
} from 'app/test-utils';
import promiseToPayService from '../promiseToPayService';
import * as commonConfig from 'app/helpers/commons';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { AxiosResponse } from 'axios';
import * as helpers from 'pages/CollectionForm/helpers';
import {
  PromiseToPayDetailType,
  PromiseToPayDetailData,
  GetPromiseToPayArgs
} from '../types';

const mockRequestData = {
  storeId,
  postData: { accountId: accEValue }
};

describe('async thunk', () => {
  const mockActions = mockActionCreator(collectionFormActions);
  const dataFromAPICommon = {
    amount: 'amount',
    method: 'method',
    paymentDate: 'paymentDate'
  };
  const dataMapped = {
    ...dataFromAPICommon,
    latestSinglePromiseDetailStatus: 'latestSinglePromiseDetailStatus',
    latestSinglePtpDetail: 'latestSinglePtpDetail',
    latestRecurringPromiseDetailStatus: 'latestRecurringPromiseDetailStatus',
    latestRecurringPtpDetail: 'latestRecurringPtpDetail',
    latestNonRecurringPtpDetail: 'latestNonRecurringPtpDetail',
    latestNonRecurringPromiseDetailStatus:
      'latestNonRecurringPromiseDetailStatus'
  };
  const dataRef = [{ value: 'method' }];

  describe('call api', () => {
    jest
      .spyOn(commonConfig, 'getFeatureConfig')
      .mockResolvedValue({ selectFields: [] });
    it('with promiseToPayType is Single', async () => {
      const dataReturnFromAPI = {
        ...dataFromAPICommon,
        latestSinglePromiseDetailStatus: 'ACTIVE',
        latestSinglePtpDetail: {
          singlePromiseData: {
            promiseAmount: '10',
            promiseDueDate: '10/10/2020'
          }
        }
      };
      jest
        .spyOn(promiseToPayService, 'getPromiseToPayMethod')
        .mockResolvedValue({
          ...responseDefault,
          data: dataRef
        } as AxiosResponse);
      jest.spyOn(promiseToPayService, 'getPromiseToPayData').mockResolvedValue({
        ...responseDefault,
        data: dataReturnFromAPI
      } as AxiosResponse);

      const store = createStore(rootReducer, {
        collectionForm: { [storeId]: {} },
        accountDetail: {
          [storeId]: {
            data: {
              cardHolderSecondaryName: 'test'
            }
          }
        },
        mapping: {
          data: {
            promiseToPay: { ...dataMapped }
          }
        }
      });

      const response = await getPromiseToPayData(mockRequestData)(
        store.dispatch,
        store.getState,
        {}
      );
      expect(response.payload).toEqual({
        promiseToPayData: {
          amount: '10',
          paymentDate: '10/10/2020'
        },
        promiseToPayType: 'Single'
      });
    });

    it('with promiseToPayType is Recurring', async () => {
      const dataReturnFromAPI = {
        ...dataFromAPICommon,
        latestRecurringPromiseDetailStatus: 'ACTIVE',
        latestRecurringPtpDetail: {
          recurringInterval: 'WEEKLY',
          recurringPromiseDataList: [
            {
              promiseDueDate: '10/10/2020',
              promiseAmount: '10',
              paymentMethod: 'method'
            }
          ]
        }
      };
      jest
        .spyOn(promiseToPayService, 'getPromiseToPayMethod')
        .mockResolvedValue({
          ...responseDefault,
          data: dataRef
        } as AxiosResponse);
      jest.spyOn(promiseToPayService, 'getPromiseToPayData').mockResolvedValue({
        ...responseDefault,
        data: dataReturnFromAPI
      } as AxiosResponse);

      const store = createStore(rootReducer, {
        collectionForm: { [storeId]: {} },
        accountDetail: {
          [storeId]: {
            data: {}
          }
        },
        mapping: {
          data: {
            promiseToPay: { ...dataMapped }
          }
        }
      });

      const response = await getPromiseToPayData(mockRequestData)(
        store.dispatch,
        store.getState,
        {}
      );
      expect(response.payload).toEqual({
        promiseToPayType: 'Recurring',
        promiseToPayData: {
          dataList: [
            {
              amount: '10',
              paymentDate: '10/10/2020',
              paymentMethod: undefined
            }
          ],
          interval: { value: 'WEEKLY', description: 'Weekly' },
          count: 1,
          amount: '10',
          paymentDate: '10/10/2020',
          method: { value: 'method' },
          endDate: '10/10/2020',
          startDate: '10/10/2020'
        }
      });
    });

    it('with promiseToPayType is NonRecurring', async () => {
      const dataReturnFromAPI = {
        ...dataFromAPICommon,
        latestNonRecurringPromiseDetailStatus: 'ACTIVE',
        latestNonRecurringPtpDetail: {
          nonRecurringPromiseDataList: [
            {
              promiseAmount: '10',
              promiseDueDate: '10/10/2020'
            }
          ]
        }
      };
      jest
        .spyOn(promiseToPayService, 'getPromiseToPayMethod')
        .mockResolvedValue({
          ...responseDefault,
          data: dataRef
        } as AxiosResponse);
      jest.spyOn(promiseToPayService, 'getPromiseToPayData').mockResolvedValue({
        ...responseDefault,
        data: dataReturnFromAPI
      } as AxiosResponse);

      const store = createStore(rootReducer, {
        collectionForm: { [storeId]: {} },
        accountDetail: {
          [storeId]: {
            data: {
              cardHolderSecondaryName: 'test'
            }
          }
        },
        mapping: {
          data: {
            promiseToPay: { ...dataMapped }
          }
        }
      });

      const response = await getPromiseToPayData(mockRequestData)(
        store.dispatch,
        store.getState,
        {}
      );
      expect(response.payload).toEqual({
        promiseToPayData: [
          {
            amount: '10',
            paymentDate: '10/10/2020'
          }
        ],
        promiseToPayType: 'NonRecurring'
      });
    });

    it('with promiseToPayType is another', async () => {
      const dataReturnFromAPI = {
        ...dataFromAPICommon
      };
      jest
        .spyOn(promiseToPayService, 'getPromiseToPayMethod')
        .mockResolvedValue({
          ...responseDefault,
          data: dataRef
        } as AxiosResponse);
      jest.spyOn(promiseToPayService, 'getPromiseToPayData').mockResolvedValue({
        ...responseDefault,
        data: dataReturnFromAPI
      } as AxiosResponse);

      const store = createStore(rootReducer, {
        collectionForm: { [storeId]: {} },
        accountDetail: {
          [storeId]: {}
        },
        mapping: {
          data: {}
        }
      });

      const response = await getPromiseToPayData(mockRequestData)(
        store.dispatch,
        store.getState,
        {}
      );
      expect(response.payload).toEqual({
        promiseToPayData: undefined,
        promiseToPayType: null
      });
    });

    it('call api with raw value equal progress info', async () => {
      const dispatchDestroyDelayProgressSpy = jest.spyOn(
        helpers,
        'dispatchDestroyDelayProgress'
      );

      const dataReturnFromAPI = {
        ...dataFromAPICommon,
        latestSinglePromiseDetailStatus: 'ACTIVE',
        latestSinglePtpDetail: {
          singlePromiseData: {
            promiseAmount: '10',
            promiseDueDate: '10/10/2020'
          }
        }
      };
      jest
        .spyOn(promiseToPayService, 'getPromiseToPayMethod')
        .mockResolvedValue({
          ...responseDefault,
          data: dataRef
        } as AxiosResponse);
      jest.spyOn(promiseToPayService, 'getPromiseToPayData').mockResolvedValue({
        ...responseDefault,
        data: dataReturnFromAPI
      } as AxiosResponse);

      const mockToggleCallResultInProgressStatus = mockActions(
        'toggleCallResultInProgressStatus'
      );
      const store = createStore(rootReducer, {
        collectionForm: {
          [storeId]: {
            inProgressInfo: {
              promiseToPayType: 'Single'
            },
            isInProgress: true
          }
        },
        mapping: {
          data: {
            promiseToPay: { ...dataMapped }
          }
        }
      });

      await getPromiseToPayData(mockRequestData)(
        store.dispatch,
        store.getState,
        {}
      );
      expect(mockToggleCallResultInProgressStatus).toBeCalledWith({
        storeId,
        info: { promiseToPayType: 'Single' }
      });
      expect(dispatchDestroyDelayProgressSpy).toBeCalledWith(storeId);
    });
  });

  describe('Test builder', () => {
    const store = createStore(rootReducer, {
      promiseToPay: {
        [storeId]: {
          loading: false
        }
      }
    });
    const state = store.getState();
    const mockPayload: {
      promiseToPayType: PromiseToPayDetailType;
      promiseToPayData: PromiseToPayDetailData;
    } = {
      promiseToPayType: 'Single',
      promiseToPayData: {
        amount: '10'
      }
    };

    const mockArg: GetPromiseToPayArgs = {
      storeId,
      postData: {
        accountId: storeId
      }
    };

    it('builder > fulfilled', () => {
      const fulfilled = getPromiseToPayData.fulfilled(mockPayload, '', mockArg);
      const { promiseToPay } = rootReducer(state, fulfilled);
      expect(promiseToPay).toEqual({
        [storeId]: {
          loading: false,
          promiseToPayData: {
            type: 'Single',
            data: {
              amount: '10'
            }
          }
        }
      });
    });

    it('builder > pending', () => {
      const pending = getPromiseToPayData.pending('', mockArg);
      const { promiseToPay } = rootReducer(state, pending);
      expect(promiseToPay).toEqual({
        [storeId]: {
          loading: true
        }
      });
    });
    
    it('builder > rejected', () => {
      const rejected = getPromiseToPayData.rejected(null, '', mockArg);
      const { promiseToPay } = rootReducer(state, rejected);
      expect(promiseToPay).toEqual({
        [storeId]: {
          loading: false
        }
      });
    });
  });
});
