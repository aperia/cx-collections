import { apiService } from 'app/utils/api.service';
import axios from 'axios';
import {
  AddCustomPromiseToPayPayload,
  AddRecurringPromiseToPayPayload,
  AddSinglePromiseToPayPayload,
  GetPromiseToPayPayload,
  PromiseToPayDetailsResponse
} from './types';

const promiseToPayService = {
  addSinglePromiseToPay(data: AddSinglePromiseToPayPayload) {
    const url =
      window.appConfig?.api?.collectionForm.addSinglePromiseToPay || '';
    return apiService.post(url, { promiseToPayAction: 'ADD', ...data });
  },

  addRecurringPromiseToPay(
    data: Omit<AddRecurringPromiseToPayPayload, 'promiseToPayType'>
  ) {
    const url =
      window.appConfig?.api?.collectionForm?.addRecurringPromiseToPay || '';
    return apiService.post(url, data);
  },

  addCustomPromiseToPay(
    data: Omit<AddCustomPromiseToPayPayload, 'promiseToPayType'>
  ) {
    const url =
      window.appConfig?.api?.collectionForm?.addCustomPromiseToPay || '';
    return apiService.post(url, data);
  },

  getPromiseToPayData(requestBody: GetPromiseToPayPayload) {
    const url = window.appConfig?.api?.collectionForm.getPromiseToPay || '';
    return apiService.post<PromiseToPayDetailsResponse>(url, requestBody);
  },

  getPromiseToPayConfig(
    requestBody: CommonAccountIdRequestBody & {
      clientInfoId: string;
      fieldsToInclude: string[];
      enableFallback: boolean;
    }
  ) {
    const url =
      window.appConfig?.api?.collectionForm?.getPromiseToPayConfig || '';
    return apiService.post(url, requestBody);
  },

  getPromiseToPayMethod() {
    const url =
      window.appConfig?.api?.collectionForm?.getPromiseToPayMethod || '';
    return axios.get(url);
  }
};

export default promiseToPayService;
