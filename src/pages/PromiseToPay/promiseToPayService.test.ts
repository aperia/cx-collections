import { AxiosRequestConfig } from 'axios';
import promiseToPayService from './promiseToPayService';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi } from 'app/test-utils/mocks/mockProperty';

// types
import {
  AddSinglePromiseToPayPayload,
  GetPromiseToPayPayload,
  AddRecurringPromiseToPayPayload,
  AddCustomPromiseToPayPayload
} from './types';

jest.mock('axios', () => {
  const axios = jest.requireActual('axios');
  const mockAction = (params?: AxiosRequestConfig) => {
    return Promise.resolve({ data: params });
  };
  const axiosMock = (params?: AxiosRequestConfig) => mockAction(params);
  axiosMock.get = (params?: AxiosRequestConfig) => mockAction(params);
  axiosMock.create = axios.create;
  return {
    ...axios,
    __esModule: true,
    default: axiosMock,
    defaults: axiosMock
  };
});

describe('promiseToPayService', () => {
  it('addSinglePromiseToPay work correctly', () => {
    const mockService = mockApiServices.post();
    const params = {} as AddSinglePromiseToPayPayload;
    mockAppConfigApi.clear();
    promiseToPayService.addSinglePromiseToPay(params);
    expect(mockService).toBeCalledWith('', { promiseToPayAction: 'ADD' });
    mockAppConfigApi.setup();
    promiseToPayService.addSinglePromiseToPay(params);
    expect(mockService).toBeCalledWith(
      'fs/collectionsPtp/v1/ptpCreateWorkflow',
      { promiseToPayAction: 'ADD' }
    );
  });

  it('addRecurringPromiseToPay work correctly', () => {
    const mockService = mockApiServices.post();
    const params = {} as Omit<
      AddRecurringPromiseToPayPayload,
      'promiseToPayType'
    >;
    mockAppConfigApi.clear();
    promiseToPayService.addRecurringPromiseToPay(params);
    expect(mockService).toBeCalledWith('', params);
    mockAppConfigApi.setup();

    promiseToPayService.addRecurringPromiseToPay(params);
    expect(mockService).toBeCalledWith(
      'fs/collectionsPtp/v1/ptpCreateRecurringWorkflow',
      params
    );
  });

  it('addCustomPromiseToPay work correctly', () => {
    const mockService = mockApiServices.post();
    const params = {} as Omit<AddCustomPromiseToPayPayload, 'promiseToPayType'>;
    mockAppConfigApi.clear();
    promiseToPayService.addCustomPromiseToPay(params);
    expect(mockService).toBeCalledWith('', params);
    mockAppConfigApi.setup();
    promiseToPayService.addCustomPromiseToPay(params);
    expect(mockService).toBeCalledWith(
      'fs/collectionsPtp/v1/ptpCreateNonRecurringWorkflow',
      params
    );
  });

  it('getPromiseToPayConfig work correctly', () => {
    const mockService = mockApiServices.post();
    const params = {} as CommonAccountIdRequestBody & {
      clientInfoId: string;
      fieldsToInclude: string[];
      enableFallback: boolean;
    };
    mockAppConfigApi.clear();
    promiseToPayService.getPromiseToPayConfig(params);
    expect(mockService).toBeCalledWith('', params);
    mockAppConfigApi.setup();

    promiseToPayService.getPromiseToPayConfig(params);
    expect(mockService).toBeCalledWith(
      'fs/caseManagement/v1/clientInfoGetById',
      params
    );
  });

  it('getPromiseToPayMethod work correctly', async () => {
    mockAppConfigApi.clear();
    const responseWithoutUrl =
      await promiseToPayService.getPromiseToPayMethod();

    expect(responseWithoutUrl.data).toEqual('');

    mockAppConfigApi.setup();
    const responseWithUrl = await promiseToPayService.getPromiseToPayMethod();
    expect(responseWithUrl.data).toEqual('refData/promiseToPayMethod.json');
  });

  it('getPromiseToPayData work correctly', () => {
    const mockService = mockApiServices.post();
    const params = {} as GetPromiseToPayPayload;
    mockAppConfigApi.clear();
    promiseToPayService.getPromiseToPayData(params);
    expect(mockService).toBeCalledWith('', params);
    mockAppConfigApi.setup();

    promiseToPayService.getPromiseToPayData(params);
    expect(mockService).toBeCalledWith(
      'fs/collectionsPtp/v1/ptpAgreementDetails',
      params
    );
  });
});
