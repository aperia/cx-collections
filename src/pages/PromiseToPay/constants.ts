export const I18N_PROMISE_TO_PAY = {
  RECURRING_PROMISES: 'txt_recurring_promises'
};

export const INTERVAL_OPTIONS = [
  {
    value: 'WEEKLY',
    description: 'Weekly'
  },
  {
    value: 'BIWEEKLY',
    description: 'Bi-weekly'
  },
  {
    value: 'MONTHLY',
    description: 'Monthly'
  }
];

export const DEFAULT_PROMISE_TO_PAY_CONFIG = {
  earliestPromiseInDaysFromToday: undefined,
  latestPromiseInDaysFromToday: undefined,
  latestFirstPromiseInDaysFromToday: undefined,
  minAmountPerPromise: undefined,
  maxAmountPerPromise: undefined,
  minSumTotalPromiseAmount: undefined,
  maxSumTotalPromiseAmount: undefined,
  minDaysBetweenPromise: undefined,
  maxDaysBetweenPromise: undefined,
  minNumberPromise: undefined,
  maxNumberPromise: undefined
};

export const MIN_NUMBER_RECURRING_PROMISE_TO_PAY = 2;
export const MAX_NUMBER_RECURRING_PROMISE_TO_PAY = 26;
export const MIN_NUMBER_CUSTOM_PROMISE_TO_PAY = 2;
export const MAX_NUMBER_CUSTOM_PROMISE_TO_PAY = 26;
