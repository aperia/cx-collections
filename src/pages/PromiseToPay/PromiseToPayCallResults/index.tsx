import React from 'react';

// components
import { View } from 'app/_libraries/_dof/core';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// types
import {
  PromiseToPayDetailData,
  PromiseToPayDetailType,
  PromiseToPayRecurringDetail
} from '../types';

// helpers
import isArray from 'lodash.isarray';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import {
  selectPromiseToPayDetailData,
  selectPromiseToPayDetailType,
  selectPromiseToPayLoading
} from '../_redux/selectors';
import { I18N_PROMISE_TO_PAY } from '../constants';
import { RecurringPromisesGrid } from '../RecurringPromisesGrid';
import classNames from 'classnames';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface PromiseToPayProps {}

const PromiseToPay: React.FC<PromiseToPayProps> = () => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();

  const loading = useStoreIdSelector<boolean>(selectPromiseToPayLoading);

  const promiseToPayDetailType = useStoreIdSelector<PromiseToPayDetailType>(
    selectPromiseToPayDetailType
  );

  const promiseToPayDetailData = useStoreIdSelector<PromiseToPayDetailData>(
    selectPromiseToPayDetailData
  );

  const testId = 'promiseToPayCallResults';

  const generatePromiseToPayType = () => {
    if (promiseToPayDetailType === 'Recurring')
      return t(I18N_COLLECTION_FORM.RECURRING);
    if (promiseToPayDetailType === 'NonRecurring')
      return t(I18N_COLLECTION_FORM.CUSTOM);
    return t(I18N_COLLECTION_FORM.SINGLE);
  };

  const renderSinglePromiseToPayView = promiseToPayDetailType === 'Single' && (
    <View
      id={`${storeId}-promiseToPayDetail`}
      formKey={`${storeId}-promiseToPayDetail`}
      descriptor="promiseToPayDetailSingleViews"
      value={promiseToPayDetailData}
    />
  );

  const renderRecurringPromiseToPayView = promiseToPayDetailType ===
    'Recurring' && (
    <>
      <View
        id={`${storeId}-promiseToPayDetail`}
        formKey={`${storeId}-promiseToPayDetail`}
        descriptor="promiseToPayDetailRecurringViews"
        value={promiseToPayDetailData}
      />
      <div className="my-24 divider-dashed" />
      <RecurringPromisesGrid
        data={(promiseToPayDetailData as PromiseToPayRecurringDetail).dataList}
        titleElement={
          <h6
            className="color-grey mb-16"
            data-testid={genAmtId(testId, 'recurring-promises', '')}
          >
            {t(I18N_PROMISE_TO_PAY.RECURRING_PROMISES)}
          </h6>
        }
      />
    </>
  );

  const renderMultiplePromiseToPayView = (
    <>
      {promiseToPayDetailType === 'NonRecurring' &&
        promiseToPayDetailData &&
        isArray(promiseToPayDetailData) &&
        promiseToPayDetailData.map((viewInfo, idx) => {
          return (
            <React.Fragment key={`${idx}`}>
              {idx > 0 && <div className="divider-dashed my-24" />}
              <h6
                className="mb-16 color-grey"
                data-testid={genAmtId(testId, `promise-${idx}`, '')}
              >
                {t(I18N_COLLECTION_FORM.PROMISE)} {idx + 1}
              </h6>
              <View
                id={`${storeId}-promiseToPayDetail-${idx}`}
                formKey={`${storeId}-promiseToPayDetail-${idx}`}
                descriptor="promiseToPayDetailSingleViews"
                value={viewInfo}
              />
            </React.Fragment>
          );
        })}
    </>
  );

  return (
    <div className={classNames({ loading })}>
      {promiseToPayDetailType && (
        <>
          <span
            className="form-group-static__label"
            data-testid={genAmtId(testId, 'type', '')}
          >
            {t('txt_type')}
          </span>
          <div
            className="form-group-static__text"
            data-testid={genAmtId(testId, 'type-value', '')}
          >
            {generatePromiseToPayType()}
          </div>
          <div className="divider-dashed my-24" />
        </>
      )}
      {renderSinglePromiseToPayView}
      {renderRecurringPromiseToPayView}
      {renderMultiplePromiseToPayView}
    </div>
  );
};

export default PromiseToPay;
