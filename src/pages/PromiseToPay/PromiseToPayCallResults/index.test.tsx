import React from 'react';
import { screen } from '@testing-library/react';

import PromiseToPayCallResults from './index';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { promiseToPayActions } from '../_redux/reducers';
import {
  mockActionCreator,
  renderMockStore,
  storeId,
  accEValue
} from 'app/test-utils';
import { AccountDetailProvider } from 'app/hooks';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { PromiseToPayDetailType } from '../types';

jest.mock('pages/PromiseToPay/RecurringPromisesGrid', () => {
  return {
    __esModule: true,
    RecurringPromisesGrid: () => (
      <div>
        <h6 className="color-grey mb-16">txt_recurring_promise</h6>
      </div>
    )
  };
});

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const mockDataPromiseToPay = [
  {
    amount: '12',
    method: { value: 'CHECK', description: 'Check' },
    paymentDate: '10/10/2020',
    dataList: []
  },
  {
    amount: '24',
    method: { value: 'CHECK', description: 'Check' },
    paymentDate: '17/10/2020',
    dataList: []
  }
];

const generateState = (
  callResultType: CALL_RESULT_CODE,
  promiseToPayDataType?: PromiseToPayDetailType
): Partial<RootState> => {
  return {
    collectionForm: {
      [storeId]: {
        lastFollowUpData: { callResultType },
        uploadFiles: {}
      }
    },
    promiseToPay: {
      [storeId]: {
        promiseToPayData: {
          type: promiseToPayDataType || null,
          data: mockDataPromiseToPay
        }
      }
    }
  };
};

mockActionCreator(collectionFormActions);
mockActionCreator(promiseToPayActions);

const renderWrapper = (
  callResultType: CALL_RESULT_CODE,
  promiseToPayDataType?: PromiseToPayDetailType
) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <PromiseToPayCallResults />
    </AccountDetailProvider>,
    { initialState: generateState(callResultType, promiseToPayDataType) }
  );
};

describe('Render', () => {
  it('Render Promise to pay single', async () => {
    renderWrapper(CALL_RESULT_CODE.PROMISE_TO_PAY, 'Single');
    expect(screen.getByText('txt_single')).toBeInTheDocument();
    expect(
      screen.getByTestId('promiseToPayDetailSingleViews')
    ).toBeInTheDocument();
  });

  it('Render Promise to pay recurring', async () => {
    renderWrapper(CALL_RESULT_CODE.PROMISE_TO_PAY_RECURRING, 'Recurring');
    expect(screen.getByText('txt_recurring_promise')).toBeInTheDocument();
    expect(
      screen.getByTestId('promiseToPayDetailRecurringViews')
    ).toBeInTheDocument();
  });

  it('Render Promise to pay multiple', async () => {
    renderWrapper(CALL_RESULT_CODE.PROMISE_TO_PAY_MULTIPLE, 'NonRecurring');
    expect(
      screen.getAllByTestId('promiseToPayDetailSingleViews')
    ).toHaveLength(2);
  });
});
