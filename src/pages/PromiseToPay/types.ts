export interface PromiseToPayState {
  [storeId: string]: PromiseToPayData;
}

export type PromiseToPayDetailType =
  | 'Single'
  | 'Recurring'
  | 'NonRecurring'
  | null;

export type PromiseToPayDetailData =
  | PromiseToPaySingleDetail
  | PromiseToPayRecurringDetail
  | PromiseToPayNonRecurringDetail
  | undefined;

export interface PromiseToPayData {
  callResultType?: string;
  data?: PromiseToPayPayload | PromiseToPayPayload[];
  loading?: boolean;
  config?: PromiseToPayConfig;
  promiseToPayData?: {
    type: PromiseToPayDetailType;
    data: PromiseToPayDetailData;
  };
  isFormValid?: boolean;
  isFormDirty?: boolean;
  methodRefData?: RefDataValue[];
}

export interface PromiseToPayConfig {
  earliestPromiseInDaysFromToday?: number;
  latestPromiseInDaysFromToday?: number;
  latestFirstPromiseInDaysFromToday?: number;
  minAmountPerPromise?: number;
  maxAmountPerPromise?: number;
  minSumTotalPromiseAmount?: number;
  maxSumTotalPromiseAmount?: number;
  minDaysBetweenPromise?: number;
  maxDaysBetweenPromise?: number;
  minNumberPromise?: number;
  maxNumberPromise?: number;
}

export enum TRIGGER_TYPE {
  CREATE = 'CREATE',
  UPDATE = 'UPDATE'
}

export enum PromiseToPayType {
  SINGLE = 'Single',
  MULTIPLE = 'NonRecurring',
  RECURRING = 'Recurring'
}

export interface TriggerPromiseToPayUpdateArg extends StoreIdPayload {
  postData: {
    data: PromiseToPayRequestData;
    accountId: string;
    promiseToPayType: PromiseToPayType;
    memberSequenceIdentifier: string;
    socialSecurityIdentifier: string;
  };
}

export interface PromiseToPayPayload {
  amount?: string;
  count?: RefDataValue;
  interval?: RefDataValue;
  method?: RefDataValue;
  paymentDate?: Date;
  recurring?: boolean;
  startDate?: Date;
  endDate?: Date;
}

export interface UpdatePromiseToPayRequestPayload {}

export interface GetPromiseToPayPayload {
  common: {
    app: string;
    org: string;
    accountId: string;
  };
  callingApplication: string;
  operatorCode: string;
  cardholderContactedName: string;
}

export interface UpdatePromiseToPayRequestArg extends StoreIdPayload {
  postData: UpdatePromiseToPayPayload;
}

export interface CommonPayload {
  common: {
    accountId: string;
  };
}

export interface UpdatePromiseToPayPayload extends CommonPayload {
  callResultType?: string;
  data: PromiseToPayRequestData;
  promiseToPayType: PromiseToPayType;
}

export interface CommonRequestPayload {
  common: {
    app: string;
    org: string;
    accountId: string;
  };
  callingApplication: string;
  operatorCode: string;
  cardholderContactedName?: string;
}

export interface AddSinglePromiseToPayPayload extends CommonRequestPayload {
  promiseToPayDate: string;
  promiseToPayAmount: string;
  promiseToPayPaymentMethod: string;
  cardholderEntityType?: string;
}

export interface AddRecurringPromiseToPayPayload extends CommonRequestPayload {
  promiseToPayStartDate: string;
  promiseToPayAmount: string;
  promiseToPayPaymentMethod: string;
  promiseToPayPaymentInterval: string;
  promiseToPayPaymentCount: string;
}

export interface AddCustomPromiseToPayPayload extends CommonRequestPayload {
  promiseList: {
    promiseToPayDate: string;
    promiseToPayAmount: string;
    promiseToPayPaymentMethod: string;
  }[];
}

export interface UpdateDataAction extends StoreIdPayload {
  data: PromiseToPayPayload;
}

export interface PromiseToPaySingleData {
  amount: string;
  method?: RefDataValue;
  paymentDate?: Date;
}

export interface RecurringGridItem {
  paymentMethod: string;
  amount: string;
  paymentDate: string | Date;
}

export interface PromiseToPayRecurringData extends PromiseToPaySingleData {
  interval?: RefDataValue;
  count?: RefDataValue;
  startDate?: string;
  endDate?: string;
  listRecurringData?: MagicKeyValue
}

export type PromiseToPayRequestData =
  | PromiseToPaySingleData
  | PromiseToPaySingleData[]
  | PromiseToPayRecurringData;

export type PromiseToPayInterval = 'Weekly' | 'Bi-weekly' | 'Monthly';

export type ActiveStatus = 'ACTIVE' | 'INACTIVE';

export interface PromiseToPaySingleDetail {
  amount: string;
  paymentDate?: string;
  method?: RefDataValue;
}

export interface PromiseToPayRecurringDetail extends PromiseToPaySingleDetail {
  count?: string;
  interval?: RefDataValue;
  dataList: RecurringGridItem[];
}

export type PromiseToPayNonRecurringDetail = PromiseToPaySingleDetail[];

export interface PromiseToPayDetailsResponse {
  latestSinglePromiseDetailStatus?: ActiveStatus;
  latestRecurringPromiseDetailStatus?: ActiveStatus;
  latestNonRecurringPromiseDetailStatus?: ActiveStatus;
  latestSinglePtpDetail?: {
    id: string;
    common: Record<string, string>;
    active: boolean;
    singlePromiseData: {
      promiseId: string;
      promiseDueDate: string;
      promiseAmount: string;
      promiseStatus: string;
      promiseAmountPaid: string;
      paymentMethod: string;
      lastPaymentDate: string;
      lastPaymentAmount: string;
    };
  };
  latestRecurringPtpDetail?: {
    common: Record<string, string>;
    id: string;
    active: string;
    recurringInterval: string;
    totalPromiseAmount: string;
    totalPromiseAmountPaid: string;
    lastPaymentDate: string;
    lastPaymentAmount: string;
    recurringPromiseDataList: {
      promiseId: string;
      promiseDueDate: string;
      promiseAmount: string;
      promiseStatus: string;
      promiseAmountPaid: string;
      paymentMethod: string;
      lastPaymentDate: string;
      lastPaymentAmount?: string;
    }[];
  };
  latestNonRecurringPtpDetail?: {
    common: Record<string, string>;
    id: string;
    active: string;
    totalPromiseAmount: string;
    totalPromiseAmountPaid: string;
    lastPaymentDate: string;
    lastPaymentAmount: string;
    nonRecurringPromiseDataList: {
      promiseId: string;
      promiseDueDate: string;
      promiseAmount: string;
      promiseStatus: string;
      promiseAmountPaid: string;
      paymentMethod: string;
      lastPaymentDate: string;
      lastPaymentAmount?: string;
    }[];
  };
}

export interface PromiseToPayRecurringResponse {
  interval?: string;
  listRecurringData?: MagicKeyValue;
}