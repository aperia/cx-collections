import React, { FC, useRef, useState } from 'react';
import { FieldArray, Form, Formik, FormikProps } from 'formik';
import * as yup from 'yup';

import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useStoreIdSelector } from 'app/hooks';

import { FormikDatePickerControl } from 'app/components/_formik_controls/FormikDatePickerControl';
import { FormikDropdownControl } from 'app/components/_formik_controls/FormikDropdownControl';
import { FormikNumericControl } from 'app/components/_formik_controls/FormikNumericControl';
import {
  Button,
  Icon,
  InlineMessage,
  Tooltip
} from 'app/_libraries/_dls/components';
import { FormikContext } from './FormikContext';

import { selectPromiseToPayMethodRefData } from '../_redux/selectors';

import { I18N_COLLECTION_FORM, I18N_COMMON_TEXT } from 'app/constants/i18n';
import { PromiseToPayRequestData, PromiseToPaySingleData } from '../types';
import { checkEnableAndReturnValue, findDateErrorIndex } from './helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { formatCommon } from 'app/helpers';
import { selectCurrentConfig } from 'pages/ClientConfiguration/PromiseToPay/_redux/selectors';
import { PromiseToPayConfig } from 'pages/ClientConfiguration/PromiseToPay/types';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { DEFAULT_CONFIG_VALUE } from './constants';

const EMPTY_VALUE = {
  amount: '',
  method: undefined,
  paymentDate: undefined
};

interface CustomPromiseToPayProps {
  formId: string;
  acceptedBalanceAmount?: string;
  onSubmit: (values: PromiseToPayRequestData) => void;
}

export const CustomPromiseToPay: FC<CustomPromiseToPayProps> = ({
  formId,
  acceptedBalanceAmount,
  onSubmit
}) => {
  const { t } = useTranslation();
  const addBtnRef = useRef<HTMLDivElement>(null);
  const methodRefData = useStoreIdSelector<RefDataValue[]>(
    selectPromiseToPayMethodRefData
  );
  const {
    maxAmountPerPromise,
    minAmountPerPromise,
    earliestPromiseInDaysFromToday,
    latestPromiseInDaysFromToday,
    latestFirstPromiseInDaysFromToday,
    minDaysBetweenPromise,
    maxDaysBetweenPromise,
    minNumberPromise,
    maxNumberPromise,
    minSumTotalPromiseAmount,
    maxSumTotalPromiseAmount
  } = useStoreIdSelector<PromiseToPayConfig>(selectCurrentConfig);

  const [inlineErrMsg, setInlineErrMsg] = useState('');
  const [totalAmountError, setTotalAmountError] = useState(false);
  const [daysError, setDaysError] = useState<number[]>([]);

  const maxAmountPerPromiseNumber = checkEnableAndReturnValue(
    maxAmountPerPromise,
    DEFAULT_CONFIG_VALUE.maxAmountPerPromise
  );
  const minAmountPerPromiseNumber = checkEnableAndReturnValue(
    minAmountPerPromise,
    DEFAULT_CONFIG_VALUE.minAmountPerPromise
  );

  const earliestPromiseInDaysFromTodayNumber = checkEnableAndReturnValue(
    earliestPromiseInDaysFromToday,
    DEFAULT_CONFIG_VALUE.earliestPromiseInDaysFromToday
  );

  const latestPromiseInDaysFromTodayNumber = checkEnableAndReturnValue(
    latestPromiseInDaysFromToday,
    DEFAULT_CONFIG_VALUE.latestPromiseInDaysFromToday
  );

  const latestFirstPromiseInDaysFromTodayNumber = checkEnableAndReturnValue(
    latestFirstPromiseInDaysFromToday,
    DEFAULT_CONFIG_VALUE.latestFirstPromiseInDaysFromToday
  );
    
  const minSumTotalPromiseAmountNumber = checkEnableAndReturnValue(
    minSumTotalPromiseAmount,
    DEFAULT_CONFIG_VALUE.minSumTotalPromiseAmount
  );
  const maxSumTotalPromiseAmountNumber = checkEnableAndReturnValue(
    maxSumTotalPromiseAmount,
    DEFAULT_CONFIG_VALUE.maxSumTotalPromiseAmount
  );

  const minDaysBetweenPromiseNumber = checkEnableAndReturnValue(
    minDaysBetweenPromise,
    DEFAULT_CONFIG_VALUE.minDaysBetweenPromise
  );
  const maxDaysBetweenPromiseNumber = checkEnableAndReturnValue(
    maxDaysBetweenPromise,
    DEFAULT_CONFIG_VALUE.maxDaysBetweenPromise
  );

  const minNumberPromiseNumber = checkEnableAndReturnValue(
    minNumberPromise,
    DEFAULT_CONFIG_VALUE.minNumberPromise
  );
  const maxNumberPromiseNumber = checkEnableAndReturnValue(
    maxNumberPromise,
    DEFAULT_CONFIG_VALUE.maxNumberPromise
  );

  const validateSchema = (() => {
    if (acceptedBalanceAmount === undefined) {
      if (!isEmpty(minAmountPerPromise) && !isEmpty(maxAmountPerPromise)) {
        return yup.object().shape({
          custom: yup.array().of(
            yup.object().shape({
              amount: yup
                .number()
                .required(t('txt_amount_is_required'))
                .max(
                  maxAmountPerPromiseNumber,
                  t('txt_amount_must_be_less_than_or_equal_to_', {
                    count: formatCommon(maxAmountPerPromiseNumber).quantity
                  })
                )
                .min(
                  minAmountPerPromiseNumber,
                  t('txt_amount_must_be_equal_to_or_greater_than_', {
                    count: formatCommon(minAmountPerPromiseNumber).quantity
                  })
                ),
              method: yup.object().required(t('txt_method_is_required')),
              paymentDate: yup
                .date()
                .required(t('txt_payment_date_is_required'))
            })
          )
        });
      }
      if (!isEmpty(maxAmountPerPromise)) {
        return yup.object().shape({
          custom: yup.array().of(
            yup.object().shape({
              amount: yup
                .number()
                .required(t('txt_amount_is_required'))
                .max(
                  maxAmountPerPromiseNumber,
                  t('txt_amount_must_be_less_than_or_equal_to_', {
                    count: formatCommon(maxAmountPerPromiseNumber).quantity
                  })
                ),
              method: yup.object().required(t('txt_method_is_required')),
              paymentDate: yup
                .date()
                .required(t('txt_payment_date_is_required'))
            })
          )
        });
      }

      if (!isEmpty(minAmountPerPromise)) {
        return yup.object().shape({
          custom: yup.array().of(
            yup.object().shape({
              amount: yup
                .number()
                .required(t('txt_amount_is_required'))
                .min(
                  minAmountPerPromiseNumber,
                  t('txt_amount_must_be_equal_to_or_greater_than_', {
                    count: formatCommon(minAmountPerPromiseNumber).quantity
                  })
                ),
              method: yup.object().required(t('txt_method_is_required')),
              paymentDate: yup
                .date()
                .required(t('txt_payment_date_is_required'))
            })
          )
        });
      }
      return;
    }

    return yup.object().shape({
      custom: yup.array().of(
        yup.object().shape({
          amount: yup.number().required(t('txt_amount_is_required')),
          method: yup.object().required(t('txt_method_is_required')),
          paymentDate: yup.date().required(t('txt_payment_date_is_required'))
        })
      )
    });
  })();

  const handleSubmit = (values: { custom: PromiseToPaySingleData[] }) => {
    // resetState
    setDaysError([]);
    setInlineErrMsg('');
    setTotalAmountError(false);
    const customValue = values.custom as Array<
      Required<PromiseToPaySingleData>
    >;
    const totalAmount = customValue.reduce(
      (prevTotal, item) => prevTotal + parseInt(item.amount, 10),
      0
    );

    if (
      acceptedBalanceAmount !== undefined &&
      totalAmount !== Number(acceptedBalanceAmount)
    ) {
      setTotalAmountError(true);
      return setInlineErrMsg(t('txt_total_promise_amount_error'));
    }
    if (
      minSumTotalPromiseAmount !== undefined &&
      totalAmount < minSumTotalPromiseAmountNumber
    ) {
      setTotalAmountError(true);
      return setInlineErrMsg(
        t('txt_total_promise_amount_must_be_equal_to_or_greater_than_', {
          count: minSumTotalPromiseAmount
        })
      );
    }
    if (
      maxSumTotalPromiseAmount !== undefined &&
      totalAmount > maxSumTotalPromiseAmountNumber
    ) {
      setTotalAmountError(true);
      return setInlineErrMsg(
        t('txt_total_promise_amount_must_be_less_than_or_equal_to_', {
          count: maxSumTotalPromiseAmountNumber
        })
      );
    }

    const { minDaysError, maxDaysError } = findDateErrorIndex(
      customValue,
      minDaysBetweenPromiseNumber,
      maxDaysBetweenPromiseNumber
    );
    if (minDaysError.length) {
      setDaysError(minDaysError);
      return setInlineErrMsg(
        t('txt_payments_date_must_be_at_least__day_apart', {
          count: minDaysBetweenPromiseNumber
        })
      );
    }
    if (maxDaysError.length) {
      setDaysError(maxDaysError);
      return setInlineErrMsg(
        t('txt_payments_date_must_be_no_more_than__day_apart', {
          count: maxDaysBetweenPromiseNumber
        })
      );
    }
    onSubmit(customValue);
  };

  return (
    <div>
      {inlineErrMsg && (
        <InlineMessage
          className="mt-16 mb-0"
          variant="danger"
          dataTestId="customPromiseToPay-error"
        >
          {inlineErrMsg}
        </InlineMessage>
      )}
      <Formik
        initialValues={{
          custom: (function () {
            const result: PromiseToPaySingleData[] = [];
            for (let i = 0; i < minNumberPromiseNumber; i++) {
              result.push({ ...EMPTY_VALUE });
            }
            return result;
          })()
        }}
        validateOnBlur
        validateOnMount
        validationSchema={validateSchema}
        onSubmit={handleSubmit}
      >
        {(
          props: FormikProps<{
            custom: PromiseToPaySingleData[];
          }>
        ) => {
          const { values } = props;

          const showRemove = values.custom.length > minNumberPromiseNumber;
          const enableAddBtn = values.custom.length < maxNumberPromiseNumber;

          return (
            <Form id={formId}>
              <FieldArray name="custom">
                {({ remove, push }) => (
                  <>
                    {values.custom.map((_, index) => {
                      const testId = `customPromiseToPay-promise-${index + 1}`;
                      return (
                        <div
                          key={`${index}`}
                          className="mt-16 p-16 border br-light-l04 rounded bg-light-l20"
                          data-testid={genAmtId(testId, 'container', '')}
                        >
                          <div className="d-flex justify-content-between align-items-center">
                            <h6
                              className="color-grey"
                              data-testid={genAmtId(testId, 'title', '')}
                            >{`${t(I18N_COLLECTION_FORM.PROMISE)} ${
                              index + 1
                            }`}</h6>
                            {showRemove && (
                              <Tooltip
                                placement="top"
                                element={t(I18N_COMMON_TEXT.REMOVE)}
                                variant="primary"
                                dataTestId={`${testId}-remove-tooltip`}
                              >
                                <Button
                                  dataTestId={`${testId}-close-btn`}
                                  variant="icon-danger"
                                  size="sm"
                                  onClick={() => {
                                    remove(index);
                                  }}
                                >
                                  <Icon name="close" />
                                </Button>
                              </Tooltip>
                            )}
                          </div>
                          <div className="row">
                            <FormikNumericControl
                              name={`custom.${index}.amount`}
                              id={`${formId}-amount-${index}`}
                              dataTestId={`${testId}-amount`}
                              label={t('txt_amount')}
                              format="c0"
                              required
                              forceError={totalAmountError}
                              className="mt-16 col-12"
                            />
                            <FormikDropdownControl
                              data={methodRefData}
                              name={`custom.${index}.method`}
                              id={`${formId}-method-${index}`}
                              dataTestId={`${testId}-method`}
                              label={t('txt_method')}
                              required
                              className="mt-16 col-12"
                            />
                            <FormikDatePickerControl
                              name={`custom.${index}.paymentDate`}
                              id={`${formId}-paymentDate-${index}`}
                              dataTestId={`${testId}-paymentDate`}
                              label={t('txt_payment_date')}
                              required
                              forceError={daysError.includes(index)}
                              minDaysFromToday={
                                earliestPromiseInDaysFromTodayNumber
                              }
                              maxDaysFromToday={
                                index === 0
                                  ? latestFirstPromiseInDaysFromTodayNumber
                                  : latestPromiseInDaysFromTodayNumber
                              }
                              pastDateDisabledText={t(
                                'txt_payment_date_cannot_be_past_date'
                              )}
                              minDateDisabledText={t(
                                'txt_payment_date_must_be_at_least__days_from_today',
                                { count: earliestPromiseInDaysFromTodayNumber }
                              )}
                              maxDateDisabledText={t(
                                'txt_payment_date_must_be_less_than__days_from_today',
                                {
                                  count:
                                    index === 0
                                      ? latestFirstPromiseInDaysFromTodayNumber
                                      : latestPromiseInDaysFromTodayNumber
                                }
                              )}
                              className="mt-16 col-12"
                            />
                          </div>
                        </div>
                      );
                    })}
                    <div className="mt-16 mb-24 ml-n8" ref={addBtnRef}>
                      <Button
                        dataTestId="customPromiseToPay-add"
                        size="sm"
                        disabled={!enableAddBtn}
                        variant="outline-primary"
                        onClick={() => {
                          push({ ...EMPTY_VALUE });
                          process.nextTick(() => {
                            addBtnRef.current &&
                              addBtnRef.current.scrollIntoView({
                                behavior: 'smooth'
                              });
                          });
                        }}
                      >
                        {t(I18N_COLLECTION_FORM.ADD_PROMISE_TO_PAY)}
                      </Button>
                    </div>
                  </>
                )}
              </FieldArray>
              <FormikContext />
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};
