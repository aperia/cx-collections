import { isEmpty } from 'lodash';
import { PromiseToPayItem } from './../../ClientConfiguration/PromiseToPay/types';
import dateTime from 'date-and-time';
import { sortBy } from 'lodash';
import { PromiseToPaySingleData } from '../types';

export function findDateErrorIndex(
  promiseToPayCustom: Array<Required<PromiseToPaySingleData>>,
  minDaysBetweenPromise?: number,
  maxDaysBetweenPromise?: number
) {
  const promiseToPayCustomNormalized = promiseToPayCustom.map(
    (item, index) => ({ ...item, index })
  ); // keep index to recognize
  const promiseToPayCustomSorted = sortBy(
    promiseToPayCustomNormalized,
    'paymentDate'
  );
  const minDaysError = new Set<number>();
  const maxDaysError = new Set<number>();
  for (let i = 1; i < promiseToPayCustomSorted.length; i++) {
    const diffDays = dateTime
      .subtract(
        promiseToPayCustomSorted[i].paymentDate,
        promiseToPayCustomSorted[i - 1].paymentDate
      )
      .toDays();
    if (
      minDaysBetweenPromise !== undefined &&
      diffDays < minDaysBetweenPromise
    ) {
      minDaysError.add(promiseToPayCustomSorted[i].index);
      minDaysError.add(promiseToPayCustomSorted[i - 1].index);
    }
    if (
      maxDaysBetweenPromise !== undefined &&
      diffDays > maxDaysBetweenPromise
    ) {
      maxDaysError.add(promiseToPayCustomSorted[i].index);
      maxDaysError.add(promiseToPayCustomSorted[i - 1].index);
    }
  }
  return {
    minDaysError: Array.from(minDaysError),
    maxDaysError: Array.from(maxDaysError)
  };
}

export const checkEnableAndReturnValue = (originValue: PromiseToPayItem, defaultValue: number) => {
  if (isEmpty(originValue) || !originValue.enable) {
    return defaultValue;
  }
  return Number(originValue.rawValue);
}