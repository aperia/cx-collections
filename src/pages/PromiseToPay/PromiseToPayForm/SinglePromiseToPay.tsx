import React, { FC } from 'react';
import { Form, Formik, FormikProps } from 'formik';
import * as yup from 'yup';
import { isEmpty } from 'app/_libraries/_dls/lodash';

import { useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

import { FormikDatePickerControl } from 'app/components/_formik_controls/FormikDatePickerControl';
import { FormikDropdownControl } from 'app/components/_formik_controls/FormikDropdownControl';
import { FormikNumericControl } from 'app/components/_formik_controls/FormikNumericControl';
import { FormikContext } from './FormikContext';
import { SinglePromiseToPayContext } from './SinglePromiseToPayContext';

import { selectPromiseToPayMethodRefData } from '../_redux/selectors';
import { selectCurrentConfig } from 'pages/ClientConfiguration/PromiseToPay/_redux/selectors';

import { PromiseToPayRequestData, PromiseToPaySingleData } from '../types';
import { PromiseToPayConfig } from 'pages/ClientConfiguration/PromiseToPay/types';
import { DEFAULT_CONFIG_VALUE } from './constants';

import { formatCommon } from 'app/helpers';
import { checkEnableAndReturnValue } from './helpers';

export interface SinglePromiseToPayProps {
  formId: string;
  acceptedBalanceAmount?: string;
  onSubmit: (values: PromiseToPayRequestData) => void;
}

export const SinglePromiseToPay: FC<SinglePromiseToPayProps> = ({
  formId,
  acceptedBalanceAmount,
  onSubmit
}) => {
  const { t } = useTranslation();
  const methodRefData = useStoreIdSelector<RefDataValue[]>(
    selectPromiseToPayMethodRefData
  );
  const {
    maxAmountPerPromise,
    minAmountPerPromise,
    earliestPromiseInDaysFromToday,
    latestPromiseInDaysFromToday
  } = useStoreIdSelector<PromiseToPayConfig>(selectCurrentConfig);

  const testId = 'SinglePromiseToPay';

  const earliestPromiseInDaysFromTodayNumber = checkEnableAndReturnValue(
    earliestPromiseInDaysFromToday,
    DEFAULT_CONFIG_VALUE.earliestPromiseInDaysFromToday
  );

  const latestPromiseInDaysFromTodayNumber = checkEnableAndReturnValue(
    latestPromiseInDaysFromToday,
    DEFAULT_CONFIG_VALUE.latestPromiseInDaysFromToday
  );

  const validateSchema = (() => {
    const maxAmountPerPromiseNumber = checkEnableAndReturnValue(
      maxAmountPerPromise,
      DEFAULT_CONFIG_VALUE.maxAmountPerPromise
    );
    const minAmountPerPromiseNumber = checkEnableAndReturnValue(
      minAmountPerPromise,
      DEFAULT_CONFIG_VALUE.minAmountPerPromise
    );

    if (!isEmpty(minAmountPerPromise) && !isEmpty(maxAmountPerPromise)) {
      return yup.object().shape({
        amount: yup
          .number()
          .required(t('txt_amount_is_required'))
          .max(
            maxAmountPerPromiseNumber,
            t('txt_amount_must_be_less_than_or_equal_to_', {
              count: formatCommon(maxAmountPerPromiseNumber).quantity
            })
          )
          .min(
            minAmountPerPromiseNumber,
            t('txt_amount_must_be_equal_to_or_greater_than_', {
              count: formatCommon(minAmountPerPromiseNumber).quantity
            })
          ),
        method: yup.object().required(t('txt_method_is_required')),
        paymentDate: yup.date().required(t('txt_payment_date_is_required'))
      });
    }
    if (!isEmpty(maxAmountPerPromise)) {
      return yup.object().shape({
        amount: yup
          .number()
          .required(t('txt_amount_is_required'))
          .max(
            maxAmountPerPromiseNumber,
            t('txt_amount_must_be_less_than_or_equal_to_', {
              count: formatCommon(maxAmountPerPromiseNumber).quantity
            })
          ),
        method: yup.object().required(t('txt_method_is_required')),
        paymentDate: yup.date().required(t('txt_payment_date_is_required'))
      });
    }

    if (!isEmpty(minAmountPerPromise)) {
      return yup.object().shape({
        amount: yup
          .number()
          .required(t('txt_amount_is_required'))
          .min(
            minAmountPerPromiseNumber,
            t('txt_amount_must_be_equal_to_or_greater_than_', {
              count: formatCommon(minAmountPerPromiseNumber).quantity
            })
          ),
        method: yup.object().required(t('txt_method_is_required')),
        paymentDate: yup.date().required(t('txt_payment_date_is_required'))
      });
    }

    return yup.object().shape({
      amount: yup.number().required(t('txt_amount_is_required')),
      method: yup.object().required(t('txt_method_is_required')),
      paymentDate: yup.date().required(t('txt_payment_date_is_required'))
    });
  })();

  const validateSchemaWithAcceptedBalance = yup.object().shape({
    amount: yup.number().optional(),
    method: yup.object().required(t('txt_method_is_required')),
    paymentDate: yup.date().required(t('txt_payment_date_is_required'))
  });

  return (
    <div>
      <Formik
        initialValues={{
          amount: '',
          method: undefined,
          paymentDate: undefined
        }}
        validationSchema={
          acceptedBalanceAmount === undefined
            ? validateSchema
            : validateSchemaWithAcceptedBalance
        }
        onSubmit={onSubmit}
        validateOnBlur
        validateOnMount
      >
        {(props: FormikProps<PromiseToPaySingleData>) => (
          <Form id={formId} className="row">
            <FormikNumericControl
              name="amount"
              id={`${formId}-amount`}
              dataTestId={`${testId}-amount`}
              label={t('txt_amount')}
              format="c0"
              required={acceptedBalanceAmount === undefined}
              readOnly={acceptedBalanceAmount !== undefined}
              className="mt-16 col-12"
            />
            <FormikDropdownControl
              data={methodRefData}
              id={`${formId}-method`}
              dataTestId={`${testId}-method`}
              name="method"
              label={t('txt_method')}
              required
              className="mt-16 col-12"
            />
            <FormikDatePickerControl
              name="paymentDate"
              id={`${formId}-paymentDate`}
              dataTestId={`${testId}-paymentDate`}
              label={t('txt_payment_date')}
              required
              minDaysFromToday={earliestPromiseInDaysFromTodayNumber}
              maxDaysFromToday={latestPromiseInDaysFromTodayNumber}
              pastDateDisabledText={t('txt_payment_date_cannot_be_past_date')}
              minDateDisabledText={t(
                'txt_payment_date_must_be_at_least__days_from_today',
                { count: earliestPromiseInDaysFromTodayNumber }
              )}
              maxDateDisabledText={t(
                'txt_payment_date_must_be_less_than__days_from_today',
                {
                  count: latestPromiseInDaysFromTodayNumber
                }
              )}
              className="mt-16 col-12"
            />
            <FormikContext />
            <SinglePromiseToPayContext
              acceptedBalanceAmount={acceptedBalanceAmount}
            />
          </Form>
        )}
      </Formik>
    </div>
  );
};

export default SinglePromiseToPay;
