import React from 'react';
import * as formik from 'formik';
import { screen } from '@testing-library/react';
import { renderMockStoreId } from 'app/test-utils';

import { RecurringPromiseToPay } from './RecurringPromiseToPay';

const mFormId = 'mockFormId';
const mOnSubmit = jest.fn();

const promiseToPayConfig = {
  earliestPromiseInDaysFromToday: { enable: true, rawValue: '1' },
  latestFirstPromiseInDaysFromToday: { enable: true, rawValue: '2' },
  latestPromiseInDaysFromToday: { enable: true, rawValue: '2' },
  maxAmountPerPromise: { enable: true, rawValue: '1000' },
  maxDaysBetweenPromise: { enable: true, rawValue: '6' },
  maxNumberPromise: { enable: true, rawValue: '26' },
  maxSumTotalPromiseAmount: { enable: true, rawValue: '20000' },
  minAmountPerPromise: { enable: true, rawValue: '20' },
  minDaysBetweenPromise: { enable: true, rawValue: '0' },
  minNumberPromise: { enable: true, rawValue: '1' },
  minSumTotalPromiseAmount: { enable: true, rawValue: '40' }
} as any;

const defaultValues = {
  amount: 20,
  method: { value: 'CHECK', description: 'check' },
  paymentDate: new Date(),
  interval: { value: 'Weekly', description: 'Weekly' },
  count: { value: '2', description: '2' }
};

const mockFormik =
  (values: Record<string, any> | undefined) =>
  ({ onSubmit }: any) => {
    return (
      <>
        <button data-testid="submitBtn" onClick={() => onSubmit(values)}>
          Btn
        </button>
      </>
    );
  };

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('test RecurringPromiseToPay', () => {
  describe('render', () => {
    it('render correctly', () => {
      renderMockStoreId(
        <RecurringPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
        {
          initialState: {
            promiseToPayConfig: {
              currentConfig: {
                earliestPromiseInDaysFromToday: { enable: true, rawValue: '1' },
                latestFirstPromiseInDaysFromToday: {
                  enable: true,
                  rawValue: '6'
                },
                latestPromiseInDaysFromToday: { enable: true, rawValue: '2' },
                maxAmountPerPromise: { enable: true, rawValue: '1000' },
                maxDaysBetweenPromise: { enable: true, rawValue: '33' },
                maxNumberPromise: { enable: true, rawValue: '26' },
                maxSumTotalPromiseAmount: { enable: true, rawValue: '20000' },
                minAmountPerPromise: { enable: true, rawValue: '20' },
                minDaysBetweenPromise: { enable: true, rawValue: '31' },
                minNumberPromise: { enable: true, rawValue: '1' },
                minSumTotalPromiseAmount: { enable: true, rawValue: '20' }
              }
            } as any
          }
        }
      );
      expect(screen.getByText('txt_interval')).toBeInTheDocument();
    });

    it('render > case 2', () => {
      renderMockStoreId(
        <RecurringPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
        {
          initialState: {
            promiseToPayConfig: {
              currentConfig: {
                ...promiseToPayConfig,
                minAmountPerPromise: undefined
              }
            } as any
          }
        }
      );
      expect(screen.getByText('txt_interval')).toBeInTheDocument();
    });

    it('render > case 3', () => {
      renderMockStoreId(
        <RecurringPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
        {
          initialState: {
            promiseToPayConfig: {
              currentConfig: {
                ...promiseToPayConfig,
                maxAmountPerPromise: undefined
              }
            } as any
          }
        }
      );
      expect(screen.getByText('txt_interval')).toBeInTheDocument();
    });

    it('render > case 4', () => {
      renderMockStoreId(
        <RecurringPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
        {
          initialState: {
            promiseToPayConfig: {
              currentConfig: {
                ...promiseToPayConfig,
                maxAmountPerPromise: undefined,
                earliestPromiseInDaysFromToday: undefined,
                minNumberPromise: undefined,
                maxNumberPromise: undefined,
                minDaysBetweenPromise: undefined,
                maxDaysBetweenPromise: undefined,
                latestFirstPromiseInDaysFromToday: {
                  enable: false,
                  rawValue: ''
                }
              }
            } as any
          }
        }
      );
      expect(screen.getByText('txt_interval')).toBeInTheDocument();
    });

    it('render > case 5', () => {
      renderMockStoreId(
        <RecurringPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
        {
          initialState: {
            promiseToPayConfig: {
              currentConfig: {
                ...promiseToPayConfig,
                maxAmountPerPromise: undefined,
                minAmountPerPromise: undefined
              }
            } as any
          }
        }
      );
      expect(screen.getByText('txt_interval')).toBeInTheDocument();
    });
  });

  describe('actions', () => {
    it('should be error when total amount less than min sum total amount', () => {
      jest.spyOn(formik, 'Formik').mockImplementation(
        mockFormik({
          ...defaultValues,
          amount: 10
        })
      );
      renderMockStoreId(
        <RecurringPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
        {
          initialState: {
            promiseToPayConfig: {
              currentConfig: {
                ...promiseToPayConfig,
                minSumTotalPromiseAmount: { enable: true, rawValue: '50' }
              }
            } as any
          }
        }
      );
      const submitBtn = screen.getByTestId('submitBtn');
      submitBtn.click();
      expect(
        screen.getByText(
          'txt_total_promise_amount_must_be_equal_to_or_greater_than_'
        )
      ).toBeInTheDocument();
    });

    it('should be error when total amount greater than max sum total amount', () => {
      jest.spyOn(formik, 'Formik').mockImplementation(
        mockFormik({
          ...defaultValues,
          amount: 50
        })
      );
      renderMockStoreId(
        <RecurringPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
        {
          initialState: {
            promiseToPayConfig: {
              currentConfig: {
                ...promiseToPayConfig,
                maxSumTotalPromiseAmount: { enable: true, rawValue: '70' }
              }
            } as any
          }
        }
      );
      const submitBtn = screen.getByTestId('submitBtn');
      submitBtn.click();
      expect(
        screen.getByText(
          'txt_total_promise_amount_must_be_less_than_or_equal_to_'
        )
      ).toBeInTheDocument();
    });

    it('should be call submit', () => {
      jest.spyOn(formik, 'Formik').mockImplementation(
        mockFormik({
          ...defaultValues
        })
      );
      renderMockStoreId(
        <RecurringPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
        {
          initialState: {
            promiseToPayConfig: {
              currentConfig: {
                ...promiseToPayConfig,
                maxSumTotalPromiseAmount: { enable: true, rawValue: '70' }
              }
            } as any
          }
        }
      );
      const submitBtn = screen.getByTestId('submitBtn');
      submitBtn.click();
      expect(mOnSubmit).toBeCalledWith(defaultValues);
    });

    it('should be call submit > acceptedBalanceAmount is defined', () => {
      jest.spyOn(formik, 'Formik').mockImplementation(
        mockFormik({
          ...defaultValues
        })
      );
      renderMockStoreId(
        <RecurringPromiseToPay
          formId={mFormId}
          onSubmit={mOnSubmit}
          acceptedBalanceAmount="0"
        />,
        {
          initialState: {
            promiseToPayConfig: {
              currentConfig: {
                ...promiseToPayConfig,
                maxSumTotalPromiseAmount: { enable: true, rawValue: '70' }
              }
            } as any
          }
        }
      );
      const submitBtn = screen.getByTestId('submitBtn');
      submitBtn.click();
      expect(mOnSubmit).not.toBeCalled();
    });
  });
});
