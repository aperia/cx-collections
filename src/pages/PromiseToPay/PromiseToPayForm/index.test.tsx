import React from 'react';
import { fireEvent, screen, waitFor } from '@testing-library/react';
import {
  accEValue,
  mockActionCreator,
  renderMockStoreId,
  storeId
} from 'app/test-utils';
import PromiseToPayForm from './index';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';
import userEvent from '@testing-library/user-event';
import { promiseToPayActions } from '../_redux/reducers';

import * as SinglePTP from './SinglePromiseToPay';

import { PromiseToPayType } from '../types';
import { AccountDetailProvider } from 'app/hooks';
import { actionsLiveVox } from 'app/livevox-dialer/_redux/reducers';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockCollectionFormActions = mockActionCreator(collectionFormActions);
const mockPromiseToPayActions = mockActionCreator(promiseToPayActions);
const mockLiveVoxDialerActions = mockActionCreator(actionsLiveVox);
const mockConfirmModalActions = mockActionCreator(confirmModalActions);
beforeEach(() => {
  mockCollectionFormActions('getDetailCollectionForm');
  mockPromiseToPayActions('getPromiseToPayConfig');
  mockPromiseToPayActions('getPromiseToPayMethodRefData');
});

describe('test Promise To Pay Form Component', () => {
  describe('render', () => {
    it('should render single Promise to pay', async () => {
      jest.useFakeTimers();
      renderMockStoreId(<PromiseToPayForm />);
      jest.runAllTimers();
      const singleRadioBtn = screen.getByText('txt_single');
      waitFor(() => {
        fireEvent.click(singleRadioBtn);
      });
      expect(screen.getByText('txt_amount')).toBeInTheDocument();
      expect(screen.getByText('txt_method')).toBeInTheDocument();
      expect(screen.getByText('txt_payment_date')).toBeInTheDocument();
    });

    it('should render recurring promise to pay', async () => {
      jest.useFakeTimers();
      renderMockStoreId(<PromiseToPayForm />, {
        initialState: { promiseToPay: { [storeId]: { loading: true } } }
      });
      jest.runAllTimers();
      const recurringRadioBtn = screen.getByText('txt_recurring');
      waitFor(() => {
        fireEvent.click(recurringRadioBtn);
      });
      expect(screen.getByText('txt_amount')).toBeInTheDocument();
      expect(screen.getByText('txt_method')).toBeInTheDocument();
      expect(screen.getByText('txt_payment_date')).toBeInTheDocument();
    });

    it('should render custom promise to pay', async () => {
      jest.useFakeTimers();
      renderMockStoreId(<PromiseToPayForm />);
      jest.runAllTimers();
      const customRadioBtn = screen.getByText('txt_custom');
      waitFor(() => {
        fireEvent.click(customRadioBtn);
      });
    });
  });

  describe('actions', () => {
    it('should be cancel custom promise to pay', async () => {
      const mOpenModal = mockConfirmModalActions('open');
      const initialState: Partial<RootState> = {
        promiseToPayConfig: {
          currentConfig: {
            earliestPromiseInDaysFromToday: { enable: true, rawValue: '1' },
            latestFirstPromiseInDaysFromToday: {
              enable: true,
              rawValue: '6'
            } as any,
            latestPromiseInDaysFromToday: { enable: true, rawValue: '2' },
            maxAmountPerPromise: { enable: true, rawValue: '1000' },
            maxDaysBetweenPromise: { enable: true, rawValue: '33' },
            maxNumberPromise: { enable: true, rawValue: '26' },
            maxSumTotalPromiseAmount: { enable: true, rawValue: '20000' },
            minAmountPerPromise: { enable: true, rawValue: '2' },
            minDaysBetweenPromise: { enable: true, rawValue: '31' },
            minNumberPromise: { enable: true, rawValue: '1' },
            minSumTotalPromiseAmount: { enable: true, rawValue: '20' }
          } as any
        },
        promiseToPay: {
          [storeId]: {
            config: {
              earliestPromiseInDaysFromToday: 0,
              latestFirstPromiseInDaysFromToday: 365,
              latestPromiseInDaysFromToday: 365,
              maxAmountPerPromise: 50000,
              maxDaysBetweenPromise: undefined,
              maxNumberPromise: undefined,
              maxSumTotalPromiseAmount: undefined,
              minAmountPerPromise: 10,
              minDaysBetweenPromise: undefined,
              minNumberPromise: undefined,
              minSumTotalPromiseAmount: 10
            },
            isFormValid: true,
            isFormDirty: true
          }
        }
      };

      renderMockStoreId(<PromiseToPayForm />, { initialState });

      const customRadioBtn = screen.getByText('txt_custom');
      const cancelBtn = screen.getByText('txt_cancel');

      waitFor(() => {
        fireEvent.click(customRadioBtn);
      });

      const findTextBox = screen.queryByTestId(
        /customPromiseToPay-promise-1-amount_dls-numeric-textbox-input/
      );

      fireEvent.change(findTextBox!, {
        target: { value: '0' }
      });

      waitFor(() => {
        fireEvent.click(cancelBtn);
      });

      expect(mOpenModal).toHaveBeenCalled();
    });

    it('should be cancel without form dirty', async () => {
      const mSetCurrentView = mockCollectionFormActions('setCurrentView');
      const mSelectCallResult = mockCollectionFormActions('selectCallResult');
      const mChangeMethod = mockCollectionFormActions('changeMethod');
      renderMockStoreId(<PromiseToPayForm />);
      const customRadioBtn = screen.getByText('txt_custom');
      customRadioBtn.click();
      const cancelBtn = screen.getByText('txt_cancel');
      cancelBtn.click();

      waitFor(() => {
        fireEvent.click(customRadioBtn);
        fireEvent.click(cancelBtn);
      });
      expect(mSetCurrentView).toBeCalledWith({ storeId });
      expect(mSelectCallResult).toBeCalledWith({ storeId, item: undefined });
      expect(mChangeMethod).toBeCalledWith({
        storeId,
        method: COLLECTION_METHOD.CREATE
      });
    });

    it('should be cancel with form dirty', async () => {
      const setCurrentViewSpy = mockCollectionFormActions('setCurrentView');
      const selectCallResultSpy = mockCollectionFormActions('selectCallResult');
      const changeMethodSpy = mockCollectionFormActions('changeMethod');
      renderMockStoreId(
        <AccountDetailProvider
          value={{
            storeId,
            accEValue
          }}
        >
          <PromiseToPayForm />
        </AccountDetailProvider>,
        {},
        false
      );
      const customRadioBtn = screen.getByText('txt_custom');
      const amountText = screen.getAllByText('txt_amount');
      const cancelBtn = screen.getByText('txt_cancel');
      waitFor(() => {
        fireEvent.click(customRadioBtn);
        userEvent.type(amountText[0], '2');
        fireEvent.click(cancelBtn);
      });

      expect(setCurrentViewSpy).toBeCalledWith({ storeId });
      expect(selectCallResultSpy).toBeCalledWith({ storeId, item: undefined });
      expect(changeMethodSpy).toBeCalledWith({
        storeId,
        method: COLLECTION_METHOD.CREATE
      });
    });

    it('should be submit', async () => {
      const mockData = { value: 'value' } as never;
      const spy = jest
        .spyOn(SinglePTP, 'SinglePromiseToPay')
        .mockImplementation(({ onSubmit, formId }) => (
          <div id={formId}>
            <button
              data-testid="submitBtn"
              onClick={() => onSubmit(mockData)}
            />
          </div>
        ));
      const mockLiveVoxDialerTermCodesThunk = mockLiveVoxDialerActions(
        'liveVoxDialerTermCodesThunk'
      );
      const mTriggerUpdatePromiseToPay = mockPromiseToPayActions(
        'triggerUpdatePromiseToPay'
      );
      renderMockStoreId(<PromiseToPayForm />, {
        initialState: {
          liveVoxReducer: {
            sessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
            isAgentAuthenticated: true,
            lineNumber: 'ACD',
            accountNumber: '2345',
            lastInCallAccountNumber: '2345',
            inCallAccountNumber: '2345',
            call: {
              accountNumber: '5166480500018901',
              accountNumberRequired: true,
              callCenterId: 75,
              callRecordingStarted: true,
              callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
              callTransactionId: '126650403531',
              phoneNumber: '1234567890',
              serviceId: 1094568
            }
          },
          collectionForm: {
            [storeId]: {
              selectedCallResult: {
                code: 'PP',
                description: 'Promise to Pay'
              }
            }
          },
          promiseToPay: { [storeId]: { loading: true } }
        }
      });
      const submitBtn = screen.getByTestId('submitBtn');
      submitBtn.click();
      expect(mTriggerUpdatePromiseToPay).toBeCalledWith({
        storeId,
        postData: {
          accountId: accEValue,
          data: mockData,
          promiseToPayType: PromiseToPayType.SINGLE,
          memberSequenceIdentifier: 'memberSequenceIdentifier',
          socialSecurityIdentifier: 'socialSecurityIdentifier'
        }
      });
      expect(mockLiveVoxDialerTermCodesThunk).toBeCalledWith({
        liveVoxDialerSessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
        liveVoxDialerLineNumber: 'ACD',
        selectedCallResult: {
          code: 'PP',
          description: 'Promise to Pay'
        },
        serviceId: 1094568,
        callTransactionId: '126650403531',
        callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
        account: '5166480500018901',
        phoneDialed: '1234567890',
        moveAgentToNotReady: false
      });
      spy.mockReset();
      spy.mockRestore();
    });

    it('should not dispatch DialerTermCodes when no accountNumber', async () => {
      const mockData = { value: 'value' } as never;
      const spy = jest
        .spyOn(SinglePTP, 'SinglePromiseToPay')
        .mockImplementation(({ onSubmit, formId }) => (
          <div id={formId}>
            <button
              data-testid="submitBtn"
              onClick={() => onSubmit(mockData)}
            />
          </div>
        ));
      const mockLiveVoxDialerTermCodesThunk = mockLiveVoxDialerActions(
        'liveVoxDialerTermCodesThunk'
      );
      const mTriggerUpdatePromiseToPay = mockPromiseToPayActions(
        'triggerUpdatePromiseToPay'
      );
      renderMockStoreId(<PromiseToPayForm />, {
        initialState: {
          liveVoxReducer: {
            sessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
            isAgentAuthenticated: true,
            lineNumber: 'ACD',
            accountNumber: '',
            lastInCallAccountNumber: '',
            inCallAccountNumber: '',
            call: {
              accountNumber: '',
              accountNumberRequired: true,
              callCenterId: 75,
              callRecordingStarted: true,
              callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
              callTransactionId: '126650403531',
              phoneNumber: '1234567890',
              serviceId: 1094568
            }
          },
          collectionForm: {
            [storeId]: {
              selectedCallResult: {
                code: 'PP',
                description: 'Promise to Pay'
              }
            }
          },
          promiseToPay: { [storeId]: { loading: true } }
        }
      });
      const submitBtn = screen.getByTestId('submitBtn');
      submitBtn.click();
      expect(mTriggerUpdatePromiseToPay).toBeCalledWith({
        storeId,
        postData: {
          accountId: accEValue,
          data: mockData,
          promiseToPayType: PromiseToPayType.SINGLE,
          memberSequenceIdentifier: 'memberSequenceIdentifier',
          socialSecurityIdentifier: 'socialSecurityIdentifier'
        }
      });
      expect(mockLiveVoxDialerTermCodesThunk).not.toBeCalled();
      spy.mockReset();
      spy.mockRestore();
    });
  });
});
