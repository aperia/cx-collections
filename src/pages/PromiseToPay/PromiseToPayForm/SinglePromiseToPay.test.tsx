import React from 'react';
import * as formik from 'formik';
import { renderMockStoreId, storeId } from 'app/test-utils';

import { SinglePromiseToPay } from './SinglePromiseToPay';

const mFormId = 'mockFormId';
const mOnSubmit = jest.fn();

const defaultConfig = {
  minAmountPerPromise: 20,
  maxAmountPerPromise: 1000
};

const promiseToPayConfig = {
  earliestPromiseInDaysFromToday: { enable: true, rawValue: '4' },
  latestFirstPromiseInDaysFromToday: { enable: true, rawValue: '2' },
  latestPromiseInDaysFromToday: { enable: true, rawValue: '2' },
  maxAmountPerPromise: { enable: true, rawValue: '25' },
  maxDaysBetweenPromise: { enable: true, rawValue: '15' },
  maxNumberPromise: { enable: true, rawValue: '90' },
  maxSumTotalPromiseAmount: { enable: false, rawValue: '' },
  minAmountPerPromise: { enable: true, rawValue: '10' },
  minDaysBetweenPromise: { enable: true, rawValue: '3' },
  minNumberPromise: { enable: true, rawValue: '3' },
  minSumTotalPromiseAmount: { enable: false, rawValue: '' }
} as any;

HTMLCanvasElement.prototype.getContext = jest.fn();
HTMLDivElement.prototype.scrollIntoView = jest.fn();

const generateFormikValue = (amounts: number[], paymentDate?: Date[]) => {
  return {
    custom: amounts.map((amount, i) => ({
      amount,
      method: { value: 'CHECK', description: 'check' },
      paymentDate: paymentDate ? paymentDate[i] : new Date()
    }))
  };
};

const mockFormik =
  (values: Record<string, any> | undefined) =>
  ({ onSubmit }: any) => {
    return (
      <>
        <button data-testid="submitBtn" onClick={() => onSubmit(values)}>
          Btn
        </button>
      </>
    );
  };

describe('test SinglePromiseToPay', () => {
  it('render correctly', () => {
    const { wrapper } = renderMockStoreId(
      <SinglePromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
      {
        initialState: {
          promiseToPayConfig: {
            currentConfig: {
              ...promiseToPayConfig
            }
          } as any
        }
      }
    );

    expect(wrapper.getByText('txt_amount')).toBeInTheDocument();
    expect(wrapper.getByText('txt_method')).toBeInTheDocument();
    expect(wrapper.getByText('txt_payment_date')).toBeInTheDocument();
  });

  it('render correctly > case 2', () => {
    const { wrapper } = renderMockStoreId(
      <SinglePromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
      {
        initialState: {
          promiseToPayConfig: {
            currentConfig: {
              maxAmountPerPromise: { enable: true, rawValue: '25' }
            }
          } as any
        }
      }
    );

    expect(wrapper.getByText('txt_amount')).toBeInTheDocument();
    expect(wrapper.getByText('txt_method')).toBeInTheDocument();
    expect(wrapper.getByText('txt_payment_date')).toBeInTheDocument();
  });

  it('render correctly > case 3', () => {
    const { wrapper } = renderMockStoreId(
      <SinglePromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
      {
        initialState: {
          promiseToPayConfig: {
            currentConfig: {
              minAmountPerPromise: { enable: true, rawValue: '10' }
            }
          } as any
        }
      }
    );

    expect(wrapper.getByText('txt_amount')).toBeInTheDocument();
    expect(wrapper.getByText('txt_method')).toBeInTheDocument();
    expect(wrapper.getByText('txt_payment_date')).toBeInTheDocument();
  });

  it('render correctly > case 4', () => {
    const { wrapper } = renderMockStoreId(
      <SinglePromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
      {
        initialState: {
          promiseToPayConfig: {
            currentConfig: {}
          } as any
        }
      }
    );

    expect(wrapper.getByText('txt_amount')).toBeInTheDocument();
    expect(wrapper.getByText('txt_method')).toBeInTheDocument();
    expect(wrapper.getByText('txt_payment_date')).toBeInTheDocument();
  });

  describe('actions', () => {
    it('should validate have not minAmountPerPromise', () => {
      jest
        .spyOn(formik, 'Formik')
        .mockImplementation(mockFormik(generateFormikValue([15, 20])));
      renderMockStoreId(
        <SinglePromiseToPay
          formId={mFormId}
          onSubmit={mOnSubmit}
          acceptedBalanceAmount="0"
        />,
        {
          initialState: {
            promiseToPay: {
              [storeId]: {
                config: Object.assign(
                  {},
                  { ...defaultConfig, minAmountPerPromise: 0 },
                  {
                    minSumTotalPromiseAmount: 50
                  }
                )
              }
            }
          }
        }
      );
    });

    it('should validate have not maxAmountPerPromise', () => {
      jest
        .spyOn(formik, 'Formik')
        .mockImplementation(mockFormik(generateFormikValue([15, 20])));
      renderMockStoreId(
        <SinglePromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
        {
          initialState: {
            promiseToPay: {
              [storeId]: {
                config: Object.assign(
                  {},
                  { ...defaultConfig, maxAmountPerPromise: 0 },
                  {
                    minSumTotalPromiseAmount: 50
                  }
                )
              }
            }
          }
        }
      );
    });

    it('should validate have not maxAmountPerPromise & minAmountPerPromise', () => {
      jest
        .spyOn(formik, 'Formik')
        .mockImplementation(mockFormik(generateFormikValue([15, 20])));
      renderMockStoreId(
        <SinglePromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
        {
          initialState: {
            promiseToPay: {
              [storeId]: {
                config: Object.assign(
                  {},
                  {
                    ...defaultConfig,
                    maxAmountPerPromise: 0,
                    minAmountPerPromise: 0
                  },
                  {
                    minSumTotalPromiseAmount: 50
                  }
                )
              }
            }
          }
        }
      );
    });
  });
});
