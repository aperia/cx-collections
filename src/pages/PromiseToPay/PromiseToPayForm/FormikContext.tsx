import { useEffect } from 'react';
import { useFormikContext } from 'formik';
import { useDispatch } from 'react-redux';
import { useAccountDetail } from 'app/hooks';
import { promiseToPayActions } from '../_redux/reducers';

export const FormikContext = () => {
  const { storeId } = useAccountDetail();
  const { isValid, dirty } = useFormikContext();
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(promiseToPayActions.setValidForm({ storeId, isValid }));
  }, [dispatch, isValid, storeId]);
  useEffect(() => {
    dispatch(promiseToPayActions.setDirtyForm({ storeId, dirty }));
  }, [dispatch, dirty, storeId]);
  return null;
};
