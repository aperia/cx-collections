export const DEFAULT_CONFIG_VALUE = {
  maxAmountPerPromise: 99,
  minAmountPerPromise: 0,
  earliestPromiseInDaysFromToday: 0,
  latestPromiseInDaysFromToday: 999,
  latestFirstPromiseInDaysFromToday: 999,
  minDaysBetweenPromise: 0,
  maxDaysBetweenPromise: 31,
  minNumberPromise: 2,
  maxNumberPromise: 26,
  minSumTotalPromiseAmount: 0,
  maxSumTotalPromiseAmount: 999
};
