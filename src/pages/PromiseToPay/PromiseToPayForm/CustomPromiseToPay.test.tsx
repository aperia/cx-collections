import React from 'react';
import * as formik from 'formik';
import dateTime from 'date-and-time';
import { fireEvent, screen } from '@testing-library/react';
import { renderMockStoreId } from 'app/test-utils';

import { CustomPromiseToPay } from './CustomPromiseToPay';
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';

const today = new Date();
today.setHours(0, 0, 0, 0);

const mFormId = 'mockFormId';
const mOnSubmit = jest.fn();

const promiseToPayConfig = {
  earliestPromiseInDaysFromToday: { enable: true, rawValue: '1' },
  latestFirstPromiseInDaysFromToday: { enable: true, rawValue: '2' },
  latestPromiseInDaysFromToday: { enable: true, rawValue: '2' },
  maxAmountPerPromise: { enable: true, rawValue: '1000' },
  maxDaysBetweenPromise: { enable: true, rawValue: '6' },
  maxNumberPromise: { enable: true, rawValue: '26' },
  maxSumTotalPromiseAmount: { enable: true, rawValue: '20000' },
  minAmountPerPromise: { enable: true, rawValue: '20' },
  minDaysBetweenPromise: { enable: true, rawValue: '0' },
  minNumberPromise: { enable: true, rawValue: '1' },
  minSumTotalPromiseAmount: { enable: true, rawValue: '40' }
} as any;

const generateFormikValue = (amounts: number[], paymentDate?: Date[]) => {
  return {
    custom: amounts.map((amount, i) => ({
      amount,
      method: { value: 'CHECK', description: 'check' },
      paymentDate: paymentDate ? paymentDate[i] : new Date()
    }))
  };
};

const mockFormik =
  (values: Record<string, any> | undefined) =>
  ({ onSubmit }: any) => {
    return (
      <>
        <button data-testid="submitBtn" onClick={() => onSubmit(values)}>
          Btn
        </button>
      </>
    );
  };

HTMLCanvasElement.prototype.getContext = jest.fn();
HTMLDivElement.prototype.scrollIntoView = jest.fn();

describe('test CustomPromiseToPay', () => {
  it('render correctly', () => {
    const { wrapper } = renderMockStoreId(
      <CustomPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
      {
        initialState: {
          promiseToPayConfig: {
            currentConfig: {
              ...promiseToPayConfig
            }
          } as any
        }
      }
    );
    expect(screen.getAllByText('txt_amount').length).toEqual(1);
    const addBtn = screen.getByText(I18N_COLLECTION_FORM.ADD_PROMISE_TO_PAY);
    jest.useFakeTimers();
    addBtn.click();
    jest.runAllTimers();
    expect(screen.getAllByText('txt_amount').length).toEqual(2);
    const removeFormBtn =
      wrapper.baseElement.getElementsByClassName('btn-icon-danger');
    fireEvent.click(removeFormBtn[1]);
    expect(screen.getAllByText('txt_amount').length).toEqual(1);
  });

  it('render > case 2', () => {
    const { wrapper } = renderMockStoreId(
      <CustomPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
      {
        initialState: {
          promiseToPayConfig: {
            currentConfig: {
              ...promiseToPayConfig,
              maxAmountPerPromise: undefined
            }
          } as any
        }
      }
    );

    expect(screen.getAllByText('txt_amount').length).toEqual(1);
    const addBtn = screen.getByText(I18N_COLLECTION_FORM.ADD_PROMISE_TO_PAY);
    jest.useFakeTimers();
    addBtn.click();
    jest.runAllTimers();
    expect(screen.getAllByText('txt_amount').length).toEqual(2);
    const removeFormBtn =
      wrapper.baseElement.getElementsByClassName('btn-icon-danger');
    fireEvent.click(removeFormBtn[1]);
    expect(screen.getAllByText('txt_amount').length).toEqual(1);
  });

  it('render > case 3', () => {
    const { wrapper } = renderMockStoreId(
      <CustomPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
      {
        initialState: {
          promiseToPayConfig: {
            currentConfig: {
              ...promiseToPayConfig,
              minAmountPerPromise: undefined
            }
          } as any
        }
      }
    );

    expect(screen.getAllByText('txt_amount').length).toEqual(1);
    const addBtn = screen.getByText(I18N_COLLECTION_FORM.ADD_PROMISE_TO_PAY);
    jest.useFakeTimers();
    addBtn.click();
    jest.runAllTimers();
    expect(screen.getAllByText('txt_amount').length).toEqual(2);
    const removeFormBtn =
      wrapper.baseElement.getElementsByClassName('btn-icon-danger');
    fireEvent.click(removeFormBtn[1]);
    expect(screen.getAllByText('txt_amount').length).toEqual(1);
  });

  it('render > case 4', () => {
    const { wrapper } = renderMockStoreId(
      <CustomPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
      {
        initialState: {
          promiseToPayConfig: {
            currentConfig: {
              ...promiseToPayConfig,
              maxAmountPerPromise: undefined,
              minAmountPerPromise: undefined,
              minDaysBetweenPromise: undefined,
              maxDaysBetweenPromise: undefined,
              minNumberPromise: undefined,
              maxNumberPromise: undefined,
              earliestPromiseInDaysFromToday: undefined,
              latestFirstPromiseInDaysFromToday: undefined
            }
          } as any
        }
      }
    );

    expect(screen.getAllByText('txt_amount').length).toEqual(2);
    const addBtn = screen.getByText(I18N_COLLECTION_FORM.ADD_PROMISE_TO_PAY);
    jest.useFakeTimers();
    addBtn.click();
    jest.runAllTimers();
    expect(screen.getAllByText('txt_amount').length).toEqual(3);
    const removeFormBtn =
      wrapper.baseElement.getElementsByClassName('btn-icon-danger');
    fireEvent.click(removeFormBtn[1]);
    expect(screen.getAllByText('txt_amount').length).toEqual(2);
  });

  describe('actions', () => {
    it('should be error when total amount less than min sum total amount', () => {
      jest
        .spyOn(formik, 'Formik')
        .mockImplementation(mockFormik(generateFormikValue([15, 20])));
      renderMockStoreId(
        <CustomPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
        {
          initialState: {
            promiseToPayConfig: {
              currentConfig: {
                ...promiseToPayConfig,
                minSumTotalPromiseAmount: { enable: true, rawValue: '50' }
              }
            } as any
          }
        }
      );
      const submitBtn = screen.getByTestId('submitBtn');
      submitBtn.click();
      expect(
        screen.getByText(
          'txt_total_promise_amount_must_be_equal_to_or_greater_than_'
        )
      ).toBeInTheDocument();
    });

    it('should be error when total amount greater than max sum total amount', () => {
      jest
        .spyOn(formik, 'Formik')
        .mockImplementation(mockFormik(generateFormikValue([50, 60])));
      renderMockStoreId(
        <CustomPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
        {
          initialState: {
            promiseToPayConfig: {
              currentConfig: {
                ...promiseToPayConfig,
                maxSumTotalPromiseAmount: { enable: true, rawValue: '70' }
              }
            } as any
          }
        }
      );
      const submitBtn = screen.getByTestId('submitBtn');
      submitBtn.click();
      expect(
        screen.getByText(
          'txt_total_promise_amount_must_be_less_than_or_equal_to_'
        )
      ).toBeInTheDocument();
    });

    it('should be error when have not minAmountPerPromise', () => {
      jest
        .spyOn(formik, 'Formik')
        .mockImplementation(mockFormik(generateFormikValue([50, 60])));
      renderMockStoreId(
        <CustomPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
        {
          initialState: {
            promiseToPayConfig: {
              currentConfig: {
                ...promiseToPayConfig,
                maxSumTotalPromiseAmount: { enable: true, rawValue: '70' },
                minAmountPerPromise: { enable: true, rawValue: '0' }
              }
            } as any
          }
        }
      );
      const submitBtn = screen.getByTestId('submitBtn');
      submitBtn.click();
      expect(
        screen.getByText(
          'txt_total_promise_amount_must_be_less_than_or_equal_to_'
        )
      ).toBeInTheDocument();
    });

    it('should be error when have not maxAmountPerPromise', () => {
      jest
        .spyOn(formik, 'Formik')
        .mockImplementation(mockFormik(generateFormikValue([50, 60])));
      renderMockStoreId(
        <CustomPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
        {
          initialState: {
            promiseToPayConfig: {
              currentConfig: {
                ...promiseToPayConfig,
                maxSumTotalPromiseAmount: { enable: true, rawValue: '70' },
                maxAmountPerPromise: { enable: true, rawValue: '0' }
              }
            } as any
          }
        }
      );
      const submitBtn = screen.getByTestId('submitBtn');
      submitBtn.click();
      expect(
        screen.getByText(
          'txt_total_promise_amount_must_be_less_than_or_equal_to_'
        )
      ).toBeInTheDocument();
    });

    it('should be error when payment date less than minDaysBetweenPromise', () => {
      jest
        .spyOn(formik, 'Formik')
        .mockImplementation(
          mockFormik(
            generateFormikValue([20, 30], [today, dateTime.addDays(today, 10)])
          )
        );
      renderMockStoreId(
        <CustomPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
        {
          initialState: {
            promiseToPayConfig: {
              currentConfig: {
                ...promiseToPayConfig,
                maxSumTotalPromiseAmount: { enable: true, rawValue: '70' },
                minDaysBetweenPromise: { enable: true, rawValue: '12' }
              }
            } as any
          }
        }
      );
      const submitBtn = screen.getByTestId('submitBtn');
      submitBtn.click();
      expect(
        screen.getByText('txt_payments_date_must_be_at_least__day_apart')
      ).toBeInTheDocument();
    });

    it('should be error when payment date greater than maxDaysBetweenPromise', () => {
      jest
        .spyOn(formik, 'Formik')
        .mockImplementation(
          mockFormik(
            generateFormikValue([20, 30], [today, dateTime.addDays(today, 10)])
          )
        );
      renderMockStoreId(
        <CustomPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
        {
          initialState: {
            promiseToPayConfig: {
              currentConfig: {
                ...promiseToPayConfig,
                maxSumTotalPromiseAmount: { enable: true, rawValue: '70' },
                maxDaysBetweenPromise: { enable: true, rawValue: '8' }
              }
            } as any
          }
        }
      );
      const submitBtn = screen.getByTestId('submitBtn');
      submitBtn.click();
      expect(
        screen.getByText('txt_payments_date_must_be_no_more_than__day_apart')
      ).toBeInTheDocument();
    });

    it('should be call submit', () => {
      const formikValue = generateFormikValue([20, 30]);
      jest.spyOn(formik, 'Formik').mockImplementation(mockFormik(formikValue));
      renderMockStoreId(
        <CustomPromiseToPay formId={mFormId} onSubmit={mOnSubmit} />,
        {
          initialState: {
            promiseToPayConfig: {
              currentConfig: {
                ...promiseToPayConfig,
                maxSumTotalPromiseAmount: { enable: true, rawValue: '70' }
              }
            } as any
          }
        }
      );
      const submitBtn = screen.getByTestId('submitBtn');
      submitBtn.click();
      expect(mOnSubmit).toBeCalledWith(formikValue.custom);
    });

    it('should be call submit > acceptedBalanceAmount is defined', () => {
      const formikValue = generateFormikValue([20, 30]);
      jest.spyOn(formik, 'Formik').mockImplementation(mockFormik(formikValue));
      renderMockStoreId(
        <CustomPromiseToPay
          formId={mFormId}
          onSubmit={mOnSubmit}
          acceptedBalanceAmount="0"
        />,
        {
          initialState: {
            promiseToPayConfig: {
              currentConfig: {
                ...promiseToPayConfig,
                maxSumTotalPromiseAmount: { enable: true, rawValue: '70' }
              }
            } as any
          }
        }
      );
      const submitBtn = screen.getByTestId('submitBtn');
      submitBtn.click();
      expect(mOnSubmit).not.toBeCalled();
    });
  });
});
