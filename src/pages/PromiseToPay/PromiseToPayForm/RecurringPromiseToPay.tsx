import React, { FC, useMemo, useState } from 'react';
import { Form, Formik, FormikProps } from 'formik';
import * as yup from 'yup';
import { isEmpty } from 'app/_libraries/_dls/lodash';

import { useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

import { FormikDatePickerControl } from 'app/components/_formik_controls/FormikDatePickerControl';
import { FormikDropdownControl } from 'app/components/_formik_controls/FormikDropdownControl';
import { FormikNumericControl } from 'app/components/_formik_controls/FormikNumericControl';
import { FormikContext } from './FormikContext';
import {
  generateRecurringPromisesData,
  RecurringPromisesGrid
} from '../RecurringPromisesGrid';
import { InlineMessage } from 'app/_libraries/_dls/components';

import { selectPromiseToPayMethodRefData } from '../_redux/selectors';
import { selectCurrentConfig } from 'pages/ClientConfiguration/PromiseToPay/_redux/selectors';

import { I18N_PROMISE_TO_PAY } from '../constants';
import { DEFAULT_CONFIG_VALUE } from './constants';
import { PromiseToPayRecurringData, PromiseToPayRequestData } from '../types';
import { PromiseToPayConfig } from 'pages/ClientConfiguration/PromiseToPay/types';

import { formatCommon } from 'app/helpers';
import { checkEnableAndReturnValue } from './helpers';
export interface RecurringPromiseToPayProps {
  formId: string;
  acceptedBalanceAmount?: string;
  onSubmit: (values: PromiseToPayRequestData) => void;
}

export const RecurringPromiseToPay: FC<RecurringPromiseToPayProps> = ({
  formId,
  acceptedBalanceAmount,
  onSubmit
}) => {
  const { t } = useTranslation();
  const methodRefData = useStoreIdSelector<RefDataValue[]>(
    selectPromiseToPayMethodRefData
  );
  const {
    maxAmountPerPromise,
    minAmountPerPromise,
    earliestPromiseInDaysFromToday,
    latestPromiseInDaysFromToday,

    latestFirstPromiseInDaysFromToday,
    minNumberPromise,
    maxNumberPromise,
    minDaysBetweenPromise,
    maxDaysBetweenPromise,
    minSumTotalPromiseAmount,
    maxSumTotalPromiseAmount
  } = useStoreIdSelector<PromiseToPayConfig>(selectCurrentConfig);

  const [inlineErrMsg, setInlineErrMsg] = useState('');
  const [totalAmountError, setTotalAmountError] = useState(false);

  const maxAmountPerPromiseNumber = checkEnableAndReturnValue(
    maxAmountPerPromise,
    DEFAULT_CONFIG_VALUE.maxAmountPerPromise
  );
  const minAmountPerPromiseNumber = checkEnableAndReturnValue(
    minAmountPerPromise,
    DEFAULT_CONFIG_VALUE.minAmountPerPromise
  );

  const earliestPromiseInDaysFromTodayNumber = checkEnableAndReturnValue(
    earliestPromiseInDaysFromToday,
    DEFAULT_CONFIG_VALUE.earliestPromiseInDaysFromToday
  );

  const latestPromiseInDaysFromTodayNumber = checkEnableAndReturnValue(
    latestPromiseInDaysFromToday,
    DEFAULT_CONFIG_VALUE.latestPromiseInDaysFromToday
  );

  const latestFirstPromiseInDaysFromTodayNumber = checkEnableAndReturnValue(
    latestFirstPromiseInDaysFromToday,
    DEFAULT_CONFIG_VALUE.latestFirstPromiseInDaysFromToday
  );

  const minSumTotalPromiseAmountNumber = checkEnableAndReturnValue(
    minSumTotalPromiseAmount,
    DEFAULT_CONFIG_VALUE.minSumTotalPromiseAmount
  );
  const maxSumTotalPromiseAmountNumber = checkEnableAndReturnValue(
    maxSumTotalPromiseAmount,
    DEFAULT_CONFIG_VALUE.maxSumTotalPromiseAmount
  );

  const countOptions: RefDataValue[] = useMemo(() => {
    const minNumberPromiseNumber = checkEnableAndReturnValue(
      minNumberPromise,
      DEFAULT_CONFIG_VALUE.minNumberPromise
    );
    const maxNumberPromiseNumber = checkEnableAndReturnValue(
      maxNumberPromise,
      DEFAULT_CONFIG_VALUE.maxNumberPromise
    );

    const result: RefDataValue[] = [];
    for (
      let number = minNumberPromiseNumber;
      number <= maxNumberPromiseNumber;
      number++
    ) {
      result.push({ value: `${number}`, description: `${number}` });
    }
    return result;
  }, [maxNumberPromise, minNumberPromise]);

  const testId = `recurringPromiseToPay`;

  const intervalOptions: RefDataValue[] = useMemo(() => {
    const minDaysBetweenPromiseNumber = checkEnableAndReturnValue(
      minDaysBetweenPromise,
      DEFAULT_CONFIG_VALUE.minDaysBetweenPromise
    );
    const maxDaysBetweenPromiseNumber = checkEnableAndReturnValue(
      maxDaysBetweenPromise,
      DEFAULT_CONFIG_VALUE.maxDaysBetweenPromise
    );

    const weekly = {
      value: 'WEEKLY',
      description: t('txt_weekly')
    };
    const biWeekly = {
      value: 'BIWEEKLY',
      description: t('txt_bi-weekly')
    };
    const monthly = {
      value: 'MONTHLY',
      description: t('txt_monthly')
    };
    let result: RefDataValue[] = [];
    if (minDaysBetweenPromiseNumber <= 7) result.push(weekly);
    if (minDaysBetweenPromiseNumber <= 14) result.push(biWeekly);
    if (minDaysBetweenPromiseNumber <= 30) result.push(monthly);
    if (maxDaysBetweenPromiseNumber < 30) {
      result = result.filter(item => item !== monthly);
    }
    if (maxDaysBetweenPromiseNumber < 14) {
      result = result.filter(item => item !== biWeekly);
    }
    if (maxDaysBetweenPromiseNumber < 7) {
      result = result.filter(item => item !== weekly);
    }
    return result;
  }, [maxDaysBetweenPromise, minDaysBetweenPromise, t]);

  const validateSchema = (() => {
    if (acceptedBalanceAmount === undefined) {
      if (!isEmpty(minAmountPerPromise) && !isEmpty(maxAmountPerPromise)) {
        return yup.object().shape({
          amount: yup
            .number()
            .required(t('txt_amount_is_required'))
            .max(
              maxAmountPerPromiseNumber,
              t('txt_amount_must_be_less_than_or_equal_to_', {
                count: formatCommon(maxAmountPerPromiseNumber).quantity
              })
            )
            .min(
              minAmountPerPromiseNumber,
              t('txt_amount_must_be_equal_to_or_greater_than_', {
                count: formatCommon(minAmountPerPromiseNumber).quantity
              })
            ),
          method: yup.object().required(t('txt_method_is_required')),
          paymentDate: yup.date().required(t('txt_payment_date_is_required')),
          interval: yup.object().required(t('txt_interval_is_required')),
          count: yup.object().required(t('txt_count_is_required'))
        });
      }
      if (!isEmpty(maxAmountPerPromise)) {
        return yup.object().shape({
          amount: yup
            .number()
            .required(t('txt_amount_is_required'))
            .max(
              maxAmountPerPromiseNumber,
              t('txt_amount_must_be_less_than_or_equal_to_', {
                count: formatCommon(maxAmountPerPromiseNumber).quantity
              })
            ),
          method: yup.object().required(t('txt_method_is_required')),
          paymentDate: yup.date().required(t('txt_payment_date_is_required')),
          interval: yup.object().required(t('txt_interval_is_required')),
          count: yup.object().required(t('txt_count_is_required'))
        });
      }

      if (!isEmpty(minAmountPerPromise)) {
        return yup.object().shape({
          amount: yup
            .number()
            .required(t('txt_amount_is_required'))
            .min(
              minAmountPerPromiseNumber,
              t('txt_amount_must_be_equal_to_or_greater_than_', {
                count: formatCommon(minAmountPerPromiseNumber).quantity
              })
            ),
          method: yup.object().required(t('txt_method_is_required')),
          paymentDate: yup.date().required(t('txt_payment_date_is_required')),
          interval: yup.object().required(t('txt_interval_is_required')),
          count: yup.object().required(t('txt_count_is_required'))
        });
      }
      return;
    }

    return yup.object().shape({
      amount: yup.number().required(t('txt_amount_is_required')),
      method: yup.object().required(t('txt_method_is_required')),
      paymentDate: yup.date().required(t('txt_payment_date_is_required')),
      interval: yup.object().required(t('txt_interval_is_required')),
      count: yup.object().required(t('txt_count_is_required'))
    });
  })();

  const handleSubmit = (values: PromiseToPayRecurringData) => {
    // resetState
    setInlineErrMsg('');
    setTotalAmountError(false);
    const totalAmount =
      parseInt(values.amount, 10) * parseInt(values.count!.value, 10);

    if (
      acceptedBalanceAmount !== undefined &&
      totalAmount !== Number(acceptedBalanceAmount)
    ) {
      setTotalAmountError(true);
      return setInlineErrMsg(t('txt_total_promise_amount_error'));
    }
    if (
      minSumTotalPromiseAmount !== undefined &&
      totalAmount < minSumTotalPromiseAmountNumber
    ) {
      setTotalAmountError(true);
      return setInlineErrMsg(
        t('txt_total_promise_amount_must_be_equal_to_or_greater_than_', {
          count: minSumTotalPromiseAmountNumber
        })
      );
    }
    if (
      maxSumTotalPromiseAmount !== undefined &&
      totalAmount > maxSumTotalPromiseAmountNumber
    ) {
      setTotalAmountError(true);
      return setInlineErrMsg(
        t('txt_total_promise_amount_must_be_less_than_or_equal_to_', {
          count: maxSumTotalPromiseAmountNumber
        })
      );
    }
    return onSubmit(values);
  };

  return (
    <div className="mb-24">
      {inlineErrMsg && (
        <InlineMessage
          className="mt-16 mb-0"
          variant="danger"
          dataTestId="recurringPromiseToPay-error"
        >
          {inlineErrMsg}
        </InlineMessage>
      )}
      <Formik
        initialValues={{
          amount: '',
          method: undefined,
          paymentDate: undefined,
          interval: undefined,
          count: undefined
        }}
        validationSchema={validateSchema}
        onSubmit={handleSubmit}
        validateOnBlur
        validateOnMount
      >
        {(props: FormikProps<PromiseToPayRecurringData>) => {
          const gridData = generateRecurringPromisesData({
            amount: props.values?.amount,
            startDate: props.values?.paymentDate ?? '',
            interval: props.values?.interval?.value,
            count: props.values?.count?.value,
            paymentMethod: props.values?.method?.description
          });
          return (
            <>
              <Form id={formId} className="row">
                <FormikNumericControl
                  name="amount"
                  id={`${formId}-amount`}
                  dataTestId={`${testId}-amount`}
                  label={t('txt_amount')}
                  format="c0"
                  required
                  forceError={totalAmountError}
                  className="mt-16 col-12"
                />
                <FormikDropdownControl
                  data={methodRefData}
                  name="method"
                  id={`${formId}-method`}
                  dataTestId={`${testId}-method`}
                  label={t('txt_method')}
                  required
                  className="mt-16 col-12"
                />
                <FormikDatePickerControl
                  name="paymentDate"
                  id={`${formId}-paymentDate`}
                  dataTestId={`${testId}-paymentDate`}
                  label={t('txt_payment_date')}
                  required
                  minDaysFromToday={earliestPromiseInDaysFromTodayNumber}
                  maxDaysFromToday={
                    !isEmpty(latestFirstPromiseInDaysFromToday?.rawValue)
                      ? latestFirstPromiseInDaysFromTodayNumber
                      : latestPromiseInDaysFromTodayNumber
                  }
                  pastDateDisabledText={t(
                    'txt_payment_date_cannot_be_past_date'
                  )}
                  minDateDisabledText={t(
                    'txt_payment_date_must_be_at_least__days_from_today',
                    { count: earliestPromiseInDaysFromTodayNumber }
                  )}
                  maxDateDisabledText={t(
                    'txt_payment_date_must_be_less_than__days_from_today',
                    {
                      count: !isEmpty(
                        latestFirstPromiseInDaysFromToday?.rawValue
                      )
                        ? latestFirstPromiseInDaysFromTodayNumber
                        : latestPromiseInDaysFromTodayNumber
                    }
                  )}
                  className="mt-16 col-12"
                />
                <FormikDropdownControl
                  data={intervalOptions}
                  name="interval"
                  id={`${formId}-interval`}
                  dataTestId={`${testId}-interval`}
                  label={t('txt_interval')}
                  required
                  className="mt-16 col-12"
                />
                <FormikDropdownControl
                  data={countOptions}
                  name="count"
                  id={`${formId}-count`}
                  dataTestId={`${testId}-count`}
                  label={t('txt_count')}
                  required
                  forceError={totalAmountError}
                  className="mt-16 col-12"
                />
                <FormikContext />
              </Form>
              <RecurringPromisesGrid
                data={gridData}
                titleElement={
                  <p className="mt-24 mb-16 fs-14 color-grey-d20 fw-500">
                    {t(I18N_PROMISE_TO_PAY.RECURRING_PROMISES)}
                  </p>
                }
              />
            </>
          );
        }}
      </Formik>
    </div>
  );
};
