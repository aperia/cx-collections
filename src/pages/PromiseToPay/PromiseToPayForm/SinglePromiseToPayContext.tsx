import { FC, useEffect } from 'react';
import { useFormikContext } from 'formik';

export interface SinglePromiseToPayContextProps {
  acceptedBalanceAmount?: string;
}

export const SinglePromiseToPayContext: FC<SinglePromiseToPayContextProps> = ({
  acceptedBalanceAmount
}) => {
  const { setFieldValue } = useFormikContext();

  useEffect(() => {
    if (acceptedBalanceAmount !== undefined) {
      setFieldValue('amount', acceptedBalanceAmount);
    }
  }, [acceptedBalanceAmount, setFieldValue]);

  return null;
};
