import React, {
  useState,
  useEffect,
  useRef,
  useLayoutEffect,
  Fragment
} from 'react';

// components
import { Button, Icon } from 'app/_libraries/_dls/components';
import SimpleBarWithApiError from 'pages/CollectionForm/SimpleBarWithApiError';

// redux
import { useDispatch, batch, useSelector } from 'react-redux';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// i18n
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// constants
import { I18N_COLLECTION_FORM } from 'app/constants/i18n';
import { COLLECTION_METHOD } from 'pages/CollectionForm/constants';

// types
import { SelectType } from '../SelectType';
import { PromiseToPayRequestData, PromiseToPayType } from '../types';

// Actions
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { promiseToPayActions } from '../_redux/reducers';
import * as liveVoxSelectors from 'app/livevox-dialer/_redux/selectors';
import { actionsLiveVox } from 'app/livevox-dialer/_redux/reducers';

// Selectors
import {
  selectPromiseToPayDirtyForm,
  selectPromiseToPayValidForm
} from '../_redux/selectors';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { SinglePromiseToPay } from './SinglePromiseToPay';
import { CustomPromiseToPay } from './CustomPromiseToPay';
import { RecurringPromiseToPay } from './RecurringPromiseToPay';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { LiveVoxCallResultArgs } from 'app/livevox-dialer/types';
import { CallResult } from 'pages/CollectionForm/types';
import { takeSelectedCallResult } from 'pages/CollectionForm/_redux/selectors';
import  isEmpty  from 'lodash.isempty';

export interface PromiseToPayProps {}

export enum MultiplePromiseToPayAction {
  ADD = 'ADD',
  REMOVE = 'REMOVE'
}

const PromiseToPay: React.FC<PromiseToPayProps> = () => {
  const { t } = useTranslation();
  const {
    storeId,
    accEValue,
    memberSequenceIdentifier = '',
    socialSecurityIdentifier = ''
  } = useAccountDetail();
  const dispatch = useDispatch();
  const divRef = useRef<HTMLDivElement | null>(null);

  const [promiseToPayType, setPromiseToPayType] = useState<PromiseToPayType>(
    PromiseToPayType.SINGLE
  );

  const isFormDirty = useStoreIdSelector<boolean>(selectPromiseToPayDirtyForm);

  const isFormValid = useStoreIdSelector<boolean>(selectPromiseToPayValidForm);

  const isSingleActive = promiseToPayType === PromiseToPayType.SINGLE;
  const isRecurringActive = promiseToPayType === PromiseToPayType.RECURRING;
  const isMultipleActive = promiseToPayType === PromiseToPayType.MULTIPLE;


  const callResultSelected = useStoreIdSelector<CallResult>(
    takeSelectedCallResult
  );

  // LiveVox
  const liveVoxDialerSessionId = useSelector(liveVoxSelectors.getLiveVoxDialerSessionId);
  const liveVoxDialerLineNumber = useSelector(liveVoxSelectors.getLiveVoxDialerLineNumber);
  const liveVoxDialerLineActiveAndReadyData = useSelector(liveVoxSelectors.getLiveVoxDialerLineActiveAndReadyData);

  const handleCancel = () => {
    if (!isMultipleActive || !isFormDirty) return handleProcessCancel();
    dispatch(
      confirmModalActions.open({
        title: 'txt_unsaved_changes',
        body: 'txt_discard_change_body',
        cancelText: 'txt_continue_editing',
        confirmText: 'txt_discard_change',
        confirmCallback: handleProcessCancel,
        variant: 'danger'
      })
    );
  };

  const handleProcessCancel = () => {
    batch(() => {
      dispatch(collectionFormActions.setCurrentView({ storeId }));
      dispatch(
        collectionFormActions.selectCallResult({ storeId, item: undefined })
      );
      dispatch(
        collectionFormActions.changeMethod({
          storeId,
          method: COLLECTION_METHOD.CREATE
        })
      );
    });
  };

  const handleSubmit = (values: PromiseToPayRequestData) => {
    dispatch(
      promiseToPayActions.triggerUpdatePromiseToPay({
        storeId,
        postData: {
          accountId: accEValue,
          data: values,
          promiseToPayType: promiseToPayType,
          memberSequenceIdentifier,
          socialSecurityIdentifier
        }
      })
    );

    if (liveVoxDialerLineActiveAndReadyData && !isEmpty(liveVoxDialerLineActiveAndReadyData.accountNumber)) {
      const liveVoxTermCodesArgs: LiveVoxCallResultArgs = {
        liveVoxDialerSessionId: liveVoxDialerSessionId,
        liveVoxDialerLineNumber: liveVoxDialerLineNumber,
        selectedCallResult: callResultSelected,
        serviceId: liveVoxDialerLineActiveAndReadyData.serviceId,
        callTransactionId: liveVoxDialerLineActiveAndReadyData.callTransactionId,
        callSessionId: liveVoxDialerLineActiveAndReadyData.callSessionId,
        account: liveVoxDialerLineActiveAndReadyData.accountNumber,
        phoneDialed: liveVoxDialerLineActiveAndReadyData.phoneNumber,
        moveAgentToNotReady: false
      };
      dispatch(actionsLiveVox.liveVoxDialerTermCodesThunk(liveVoxTermCodesArgs));
    }
  };

  const handleChangeType = (type: PromiseToPayType) => {
    setPromiseToPayType(type);
  };

  const typePromiseToPay = (
    <SelectType
      uniqueId={storeId}
      type={promiseToPayType}
      setType={handleChangeType}
    />
  );

  useEffect(() => {
    // get config
    dispatch(promiseToPayActions.getPromiseToPayConfig({ storeId }));
  }, [dispatch, storeId]);

  // Hide next actions - float dropdown button
  useEffect(() => {
    document.body.classList.add('hide-float-btn');
    return () => {
      document.body.classList.remove('hide-float-btn');
    };
  }, []);

  useEffect(() => {
    dispatch(promiseToPayActions.getPromiseToPayMethodRefData({ storeId }));
  }, [dispatch, storeId]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100vh - 258px)`);
  }, []);

  const formId = `${storeId}-promiseToPay`;

  return (
    <Fragment>
      <div ref={divRef}>
        <SimpleBarWithApiError className="mt-24 ml-24">
          <div className="pt-24 px-24">
            <h5
              data-testid={genAmtId(
                'promiseToPay',
                'callResult',
                'PromiseToPayForm'
              )}
              className="mr-auto"
            >
              {t(I18N_COLLECTION_FORM.CALL_RESULT)}
            </h5>
            <div className="mt-16 p-8 bg-light-l16 d-flex align-items-center rounded-lg">
              <div className="bubble-icon">
                <Icon name="megaphone" />
              </div>

              <p
                data-testid={genAmtId(
                  'promiseToPay',
                  'promiseToPay',
                  'PromiseToPayForm'
                )}
                className="ml-12"
              >
                {t(I18N_COMMON_TEXT.PROMISE_TO_PAY)}
              </p>
            </div>
            {typePromiseToPay}
            <div>
              {isSingleActive && (
                <SinglePromiseToPay formId={formId} onSubmit={handleSubmit} />
              )}
              {isRecurringActive && (
                <RecurringPromiseToPay
                  formId={formId}
                  onSubmit={handleSubmit}
                />
              )}
              {isMultipleActive && (
                <CustomPromiseToPay formId={formId} onSubmit={handleSubmit} />
              )}
            </div>
          </div>
        </SimpleBarWithApiError>
      </div>
      <div className="group-button-footer d-flex justify-content-end">
        <Button
          dataTestId="promiseToPay-cancel-btn"
          variant="secondary"
          onClick={handleCancel}
        >
          {t(I18N_COMMON_TEXT.CANCEL)}
        </Button>
        <Button
          dataTestId="promiseToPay-submit-btn"
          type="submit"
          form={formId}
          disabled={!isFormValid}
        >
          {t(I18N_COMMON_TEXT.SUBMIT)}
        </Button>
      </div>
    </Fragment>
  );
};

export default PromiseToPay;
