import React from 'react';
import { renderMockStore } from 'app/test-utils';
import { SinglePromiseToPayContext } from './SinglePromiseToPayContext';
import * as formik from 'formik';

const mockSetFieldValue = jest.fn();

describe('should test single promise to pay context', () => {
  it('acceptedBalanceAmount is undefined', () => {
    jest
      .spyOn(formik, 'useFormikContext')
      .mockReturnValue({ setFieldValue: mockSetFieldValue } as any);

    renderMockStore(<SinglePromiseToPayContext />, {
      initialState: {}
    });

    expect(mockSetFieldValue).not.toBeCalled();
  });
  it('acceptedBalanceAmount is defined', () => {
    jest
      .spyOn(formik, 'useFormikContext')
      .mockReturnValue({ setFieldValue: mockSetFieldValue } as any);

    renderMockStore(<SinglePromiseToPayContext acceptedBalanceAmount="0" />, {
      initialState: {}
    });

    expect(mockSetFieldValue).toBeCalled();
  });
});
