import { getByText, render } from '@testing-library/react';
import { InputProps } from 'app/_libraries/_dls/components/Radio/Input';
import React from 'react';

import { SelectType } from './SelectType';
import { PromiseToPayType } from './types';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => () => ({
  t: jest.fn(x => x)
}));

jest.mock(
  'app/_libraries/_dls/components/Radio/Input',
  () => ({ onChange, value }: InputProps) => (
    <button
      onClick={e =>
        onChange &&
        onChange((e as unknown) as React.ChangeEvent<HTMLInputElement>)
      }
    >
      {value}
    </button>
  )
);

describe('test SelectType components', () => {
  it('should be call setType func when change radio input', () => {
    const mockSetType = jest.fn();

    const { container } = render(
      <SelectType
        uniqueId="id"
        type={PromiseToPayType.SINGLE}
        setType={mockSetType}
      />
    );

    const multipleInput = getByText(container, PromiseToPayType.MULTIPLE);
    multipleInput.click();
    expect(mockSetType).toBeCalled();
  });
});
