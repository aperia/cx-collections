export interface CollectionHistoryResponse {
  collectionsEntryDate?: string;
  collectionsExitDate?: string;
  cyclesDelinquent?: string;
  collectionDayWord?: string;
  highestAccountBalance?: string;
  highestDaysDelinquent?: string;
  highestDelinquentAmount?: string;
}
