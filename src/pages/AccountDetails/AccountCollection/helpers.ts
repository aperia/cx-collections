import isArray from 'lodash.isarray';
import { CUSTOMER_ROLE_CODE } from 'app/constants';
import { CollectionHistoryResponse } from './types';

interface Item {
  customerRoleTypeCode: string;
}

interface AccountDetailResponse {
  customerInquiry?: Item | Item[];
  collectionHistory?: CollectionHistoryResponse[];
}

export const getData = (data: AccountDetailResponse) => {
  const customerInquiry =
    isArray(data?.customerInquiry) && data?.customerInquiry.length > 0
      ? data?.customerInquiry.find(
          ({ customerRoleTypeCode }) =>
            customerRoleTypeCode === CUSTOMER_ROLE_CODE.primary
        )
      : data?.customerInquiry;
  const collectionHistory = isArray(data.collectionHistory)
    ? data.collectionHistory[0]
    : {};
  if (
    collectionHistory?.collectionsEntryDate &&
    collectionHistory?.collectionsExitDate
  ) {
    const oneDay = 24 * 60 * 60 * 1000;
    const endDate = new Date(collectionHistory.collectionsExitDate);
    const startDate = new Date(collectionHistory.collectionsEntryDate);
    collectionHistory.collectionDayWord = Math.round(
      (endDate.getTime() - startDate.getTime()) / oneDay
    ).toString();
  }
  return {
    ...customerInquiry,
    collectionHistory
  };
};
