// Components
import React from 'react';
import AccountCollection from './index';
import { TogglePinProps } from 'pages/__commons/InfoBar/TogglePin';
import { actionsInfoBar, InfoBarSection } from 'pages/__commons/InfoBar/_redux';
import { I18N_ACCOUNT_COLLECTION } from './constants';
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

jest.mock(
  'pages/__commons/InfoBar/TogglePin',
  () => (props: TogglePinProps) => {
    return <div onClick={props.onToggle}>TogglePin</div>;
  }
);

const initialState: Partial<RootState> = {
  accountCollection: {
    [storeId]: {
      loading: false,
      error: false,
      data: {
        delinquent: {
          delinquentAmount: '0000000000000540',
          externalStatusReasonCode: '91',
          delinquentStartDate: '06-07-2018',
          daysDelinquentCount: '591'
        }
      }
    }
  },
  infoBar: {
    [storeId]: {
      pinned: InfoBarSection.CollectionForm,
      isExpand: true,
      recover: InfoBarSection.ClientContactInfo
    }
  }
};

const errorState: Partial<RootState> = {
  accountCollection: {
    [storeId]: {
      loading: false,
      error: true,
      data: {}
    }
  }
};

const mockActionsInfoBar = mockActionCreator(actionsInfoBar);

describe('Testing Account Collection', () => {
  it('render component', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(<AccountCollection />, {
      initialState
    });
    jest.runAllTimers();

    expect(
      wrapper.queryByText(I18N_ACCOUNT_COLLECTION.COLLECTION_INFORMATION)
    ).toBeInTheDocument();
  });

  it('render with delinquent data', () => {
    const { wrapper } = renderMockStoreId(<AccountCollection />, {
      initialState
    });
    expect(
      wrapper.queryByText(I18N_ACCOUNT_COLLECTION.DELINQUENT)
    ).toBeInTheDocument();
  });
  it('render with error data', () => {
    const { wrapper } = renderMockStoreId(<AccountCollection />, {
      initialState: errorState
    });

    expect(wrapper.queryByText(I18N_COMMON_TEXT.RELOAD)).toBeInTheDocument();
  });

  it('render with empty data', () => {
    const { wrapper } = renderMockStoreId(<AccountCollection />, {
      initialState: {}
    });
    expect(
      wrapper.queryByText(I18N_ACCOUNT_COLLECTION.DELINQUENT)
    ).not.toBeInTheDocument();
  });

  it(' trigger action Pin with exist Collection Information Pin', () => {
    const { wrapper } = renderMockStoreId(<AccountCollection />, {
      initialState
    });
    const spy = mockActionsInfoBar('updateRecover');
    wrapper.queryByText('TogglePin')!.click();
    expect(spy).toHaveBeenCalledWith({ storeId, recover: null });
  });

  it('trigger action toggle Pin', () => {
    const { wrapper } = renderMockStoreId(<AccountCollection />, {
      initialState: {
        ...initialState,
        infoBar: {
          [storeId]: {
            pinned: InfoBarSection.CollectionInformation,
            isExpand: true,
            recover: InfoBarSection.CardholderMaintenance
          }
        }
      }
    });

    const spy = mockActionsInfoBar('updatePinned');
    wrapper.queryByText('TogglePin')!.click();
    expect(spy).toHaveBeenCalledWith({ storeId, pinned: null });
  });

  it('mapping modal is null', () => {
    const { wrapper } = renderMockStoreId(<AccountCollection />, {
      initialState,
      mapping: {}
    });
    expect(
      wrapper.queryByText(I18N_ACCOUNT_COLLECTION.COLLECTION_INFORMATION)
    ).toBeInTheDocument();
  });
});
