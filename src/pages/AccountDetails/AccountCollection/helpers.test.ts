import { getData } from './helpers';

describe('testing helper getData', () => {
  it('empty Object input', () => {
    const input = {};
    const outPut = getData(input);
    expect(outPut).toEqual({
      collectionHistory: {}
    });
  });

  it('having multiple record from API, only get roleType 01', () => {
    const input = {
      customerInquiry: [
        {
          customerRoleTypeCode: '01',
          cycledOverlimitCount: '00',
          daysDelinquentCount: '591'
        },
        {
          customerRoleTypeCode: '02',
          cycledOverlimitCount: '00',
          daysDelinquentCount: '591'
        }
      ]
    };
    const outPut = getData(input);
    expect(outPut).toEqual({
      customerRoleTypeCode: '01',
      cycledOverlimitCount: '00',
      daysDelinquentCount: '591',
      collectionHistory: {}
    });
  });

  it('should test with collection history', () => {
    const input = {
      customerInquiry: [
        {
          customerRoleTypeCode: '01',
          cycledOverlimitCount: '00',
          daysDelinquentCount: '591'
        },
        {
          customerRoleTypeCode: '02',
          cycledOverlimitCount: '00',
          daysDelinquentCount: '591'
        }
      ],
      collectionHistory: [
        {
          collectionsEntryDate: '08-01-2021',
          collectionsExitDate: '08-02-2021'
        }
      ]
    };
    const outPut = getData(input);
    expect(outPut).toEqual({
      customerRoleTypeCode: '01',
      cycledOverlimitCount: '00',
      daysDelinquentCount: '591',
      collectionHistory: {
        collectionsEntryDate: '08-01-2021',
        collectionsExitDate: '08-02-2021',
        collectionDayWord: '1'
      }
    });
  });
});
