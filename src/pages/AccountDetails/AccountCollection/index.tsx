import React, { useCallback, useEffect, useMemo, useRef } from 'react';

// components
import { FailedApiReload, GroupView } from 'app/components';
import { SimpleBar } from 'app/_libraries/_dls/components';
import TogglePin from 'pages/__commons/InfoBar/TogglePin';

// redux store
import { batch, useDispatch, useSelector } from 'react-redux';
import { accountCollection } from './_redux';
import {
  selectIsError,
  selectData,
  selectLoading,
  selectMapping
} from './_redux/selector';

// hooks - helpers
import { useAccountDetail } from 'app/hooks';
import { getGroupMappingData } from 'app/helpers';
import classNames from 'classnames';
import { FIELDS, I18N_ACCOUNT_COLLECTION } from './constants';
import { selectPinned } from 'pages/__commons/InfoBar/_redux/selectors';
import { actionsInfoBar, InfoBarSection } from 'pages/__commons/InfoBar/_redux';
import { useStoreIdSelector } from 'app/hooks/useStoreIdSelector';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { PromiseToPayHistory } from '../PromiseToPayHistory';
import { selectPromiseToPayHistoryLoading } from '../PromiseToPayHistory/_redux/selector';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface CollectionInformationProps {}

const AccountCollection: React.FC<CollectionInformationProps> = () => {
  const { storeId, accEValue } = useAccountDetail();
  const { t } = useTranslation();
  const testId = 'infoBar_collectionInfo';

  const dispatch = useDispatch();
  const ref = useRef<HTMLDivElement | null>(null);

  const accCollectionLoading = useSelector<RootState, boolean>(
    selectLoading(storeId)
  );
  const promiseToPayLoading = useStoreIdSelector<boolean>(
    selectPromiseToPayHistoryLoading
  );
  const isError = useSelector<RootState, boolean>(selectIsError(storeId));
  const data = useSelector<RootState, Record<string, object> | undefined>(
    selectData(storeId)
  );
  const mapping = useSelector<RootState, MagicKeyValue>(selectMapping());
  const pinned = useStoreIdSelector(selectPinned);
  const isPinned = pinned === InfoBarSection.CollectionInformation;

  const groupKeys = useMemo(() => {
    if (!mapping || !mapping.accountCollection) return {};

    return getGroupMappingData(mapping?.accountCollection);
  }, [mapping]);

  const handleReload = useCallback(() => {
    dispatch(
      accountCollection.getAccountCollection({
        storeId,
        postData: { accountNumber: accEValue }
      })
    );
  }, [storeId, accEValue, dispatch]);

  const loading = accCollectionLoading || promiseToPayLoading;

  const content = useMemo(() => {
    if (isError) {
      return (
        <FailedApiReload
          id="overview-account-information-fail"
          className="w-100 mt-24"
          onReload={handleReload}
          dataTestId={`${testId}_failed-api`}
        />
      );
    }

    /* render View DOF when data load done */
    if (!data) return null;

    return FIELDS.map(({ descriptor, groupKey, ...rest }) => {
      return (
        <GroupView
          key={descriptor}
          descriptor={descriptor}
          data={data[groupKey]}
          fields={groupKeys[groupKey]}
          {...rest}
        />
      );
    });
  }, [data, groupKeys, handleReload, isError]);

  useEffect(() => handleReload(), [handleReload]);

  const handleTogglePin = () => {
    batch(() => {
      const thisPinned = isPinned ? null : InfoBarSection.CollectionInformation;

      dispatch(
        actionsInfoBar.updatePinned({
          storeId,
          pinned: thisPinned
        })
      );

      if (thisPinned) {
        dispatch(actionsInfoBar.updateRecover({ storeId, recover: null }));
      }

      dispatch(actionsInfoBar.updateIsExpand({ storeId, isExpand: !isPinned }));
    });
  };

  return (
    <div className={classNames('h-100', { loading })}>
      <SimpleBar>
        <div ref={ref} className="p-24 overlap-next-action">
          <div className="d-inline-flex">
            <h4
              className="pb-16 mr-8"
              data-testid={genAmtId(testId, 'title', '')}
            >
              {t(I18N_ACCOUNT_COLLECTION.COLLECTION_INFORMATION)}
            </h4>
            <TogglePin
              isPinned={isPinned}
              onToggle={handleTogglePin}
              dataTestId={testId}
            />
          </div>
          {content}
          <PromiseToPayHistory />
        </div>
      </SimpleBar>
    </div>
  );
};

export default AccountCollection;
