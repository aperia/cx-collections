import accountCollectionService from './accountCollectionService';

// utils
import { storeId, mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// constants
import { ACCOUNT_OVERVIEW_COLLECTION } from './constants';

describe('accountCollectionService', () => {
  describe('getAccountCollectionData', () => {
    const params = storeId;

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      accountCollectionService.getAccountCollectionData(params);

      expect(mockService).toBeCalledWith('', {
        common: { accountId: params },
        selectFields: ACCOUNT_OVERVIEW_COLLECTION
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      accountCollectionService.getAccountCollectionData(params);

      expect(mockService).toBeCalledWith(apiUrl.account.getOverviewCollection, {
        common: { accountId: params },
        selectFields: ACCOUNT_OVERVIEW_COLLECTION
      });
    });
  });
});
