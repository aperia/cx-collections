// actions
import { accountCollection, reducer } from './index';

// utils
import { storeId } from 'app/test-utils';

const initialState = {};

const mockData = {
  storeId: 'storeId',
  postData: { accountId: 'accountId', accountNumber: 'accNum' }
};

describe('getAccountCollection', () => {
  it('removeStore', () => {
    const params = { storeId };
    const state = reducer(initialState, accountCollection.removeStore(params));
    expect(state[storeId]).toBeUndefined();
  })

  it('pending', () => {
    const action = accountCollection.getAccountCollection.pending(
      'getAccountCollection',
      mockData
    );
    const nextState = reducer(initialState, action);
    expect(nextState.storeId.loading).toEqual(true);
  });

  it('fulfilled > empty data', () => {
    const action = accountCollection.getAccountCollection.fulfilled(
      {
        data: {}
      },
      'account/getAccountCollection',
      mockData
    );
    const nextState = reducer(initialState, action);
    expect(nextState.storeId.data).toEqual({});
  });

  it('fulfilled > has data', () => {
    const action = accountCollection.getAccountCollection.fulfilled(
      {
        data: { customer: [1] }
      },
      'account/getAccountCollection',
      mockData
    );
    const nextState = reducer(initialState, action);
    expect(nextState.storeId.data).toEqual({ customer: [1] });
  });

  it('reject', () => {
    const pendingAction = accountCollection.getAccountCollection.rejected(
      null,
      'account/getAccountCollection',
      mockData
    );
    const nextState = reducer(initialState, pendingAction);
    expect(nextState.storeId.error).toEqual(true);
  });

});
