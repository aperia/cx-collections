import { responseDefault, storeId, accNbr } from 'app/test-utils';
import { getAccountCollection } from 'pages/AccountDetails/AccountCollection/_redux/getAccountCollection';
import accountCollectionService from '../accountCollectionService';
import { createStore, applyMiddleware } from 'redux';
import { rootReducer } from 'storeConfig';
import thunk from 'redux-thunk';

let spy: jest.SpyInstance;

const mockData = {
  storeId,
  postData: { accountId: 'accountId', accountNumber: accNbr }
};

describe('Test getNextAccount async actions', () => {
  const store = createStore(
    rootReducer,
    {
      mapping: {
        data: {
          accountCollection: {
            delinquentAmount: 'delinquentAmount',
            noDelinquentReason: ' ',
            delinquentStartDate: 'delinquentStartDate',
            daysDelinquentCount: 'daysDelinquentCount',
            lifetimeOneCycleDelinquentCount: 'lifetimeOneCycleDelinquentCount',
            lifetimeTwoCyclesDelinquentCount:
              'lifetimeTwoCyclesDelinquentCount',
            lifetimeThreeCyclesDelinquentCount:
              'lifetimeThreeCyclesDelinquentCount',
            cacsCollectorCode: 'cacsCollectorCode',
            noOtherQueueName: 'noOtherQueueName',
            workoutPlanStartDate: 'workoutPlanStartDate',
            fixPaymentAmount: 'fixPaymentAmount',
            cycledOverlimitCount: 'cycledOverlimitCount',
            oneCycleDelinquentAmount: 'oneCycleDelinquentAmount',
            twoCycleDelinquentAmount: 'twoCycleDelinquentAmount',
            threeCycleDelinquentAmount: 'threeCycleDelinquentAmount',
            fourCycleDelinquentAmount: 'fourCycleDelinquentAmount',
            fiveCycleDelinquentAmount: 'fiveCycleDelinquentAmount',
            sixCycleDelinquentAmount: 'sixCycleDelinquentAmount',
            sevenCycleDelinquentAmount: 'sevenCycleDelinquentAmount',
            collectionsEntryDate: 'collectionsEntryDate',
            noCollectionHistoryExitDate: ' ',
            noHighCyclesDelinquency: ' ',
            noCollectionDaysWork: ' ',
            noHighDaysDelinquent: ' ',
            highBalanceAmount: 'highBalanceAmount',
            noHighDelinquentAmount: ' ',

            'tabPromise.rows.amount1': 'noKeptAmount',
            'tabPromise.rows.number1': 'noKeptNumber',
            'tabPromise.rows.amount2': 'noBrokenAmount',
            'tabPromise.rows.number2': 'noBrokenNumber',
            'tabPromise.total.amount': 'noTotalAmount',
            'tabPromise.total.number': 'noTotalNumber',
            noChanged: 'noChanged',
            noPaymentReceived: 'noPaymentReceived'
          }
        }
      }
    },
    applyMiddleware(thunk)
  );

  afterEach(() => {
    spy.mockReset();
    spy.mockRestore();
  });

  it('should be fulfilled > empty data', async () => {
    spy = jest
      .spyOn(accountCollectionService, 'getAccountCollectionData')
      .mockResolvedValue({ ...responseDefault });

    const response = await getAccountCollection(mockData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ data: {} });
    expect(response.type).toEqual('account/getAccountCollection/fulfilled');
  });

  it('should be rejected', async () => {
    spy = jest
      .spyOn(accountCollectionService, 'getAccountCollectionData')
      .mockRejectedValue({
        data: null,
        status: 500
      });

    const response = await getAccountCollection(mockData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('account/getAccountCollection/rejected');
  });

  it('should be fulfilled > with data', async () => {
    spy = jest
      .spyOn(accountCollectionService, 'getAccountCollectionData')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          customerInquiry: [
            {
              cacsCollectorCode: '    ',
              collectionsEntryDate: '00000000',
              customerExternalIdentifier: 'C03098090011800710071012',
              customerRoleTypeCode: '01',
              cycledOverlimitCount: '00',
              daysDelinquentCount: '595',
              delinquentAmount: '0000000000000555',
              delinquentStartDate: '06-07-2018',
              externalStatusReasonCode: '85',
              fiveCycleDelinquentAmount: '0000000000000015.00',
              fixPaymentAmount: '0000000000000015.00',
              fourCycleDelinquentAmount: '0000000000000015.00',
              highBalanceAmount: '0000000000001080',
              lifetimeOneCycleDelinquentCount: '002',
              lifetimeThreeCyclesDelinquentCount: '002',
              lifetimeTwoCyclesDelinquentCount: '002',
              memberSequenceIdentifier: '00001',
              oneCycleDelinquentAmount: '0000000000000015.00',
              plasticReplacementIndicator: 'N',
              presentationInstrumentIdentifier:
                '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
              presentationInstrumentReplacementSequenceNumber: '0000',
              presentationInstrumentTypeCode: '01',
              sevenCycleDelinquentAmount: '0000000000000465.00',
              sixCycleDelinquentAmount: '0000000000000015.00',
              threeCycleDelinquentAmount: '0000000000000015.00',
              twoCycleDelinquentAmount: '0000000000000015.00',
              workoutPlanStartDate: '03-10-2010'
            }
          ]
        }
      });

    const response = await getAccountCollection(mockData)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      data: {
        delinquentAmount: '0000000000000555',
        noDelinquentReason: undefined,
        delinquentStartDate: '06-07-2018',
        daysDelinquentCount: '595',
        lifetimeOneCycleDelinquentCount: '002',
        lifetimeTwoCyclesDelinquentCount: '002',
        lifetimeThreeCyclesDelinquentCount: '002',
        cacsCollectorCode: '',
        noOtherQueueName: undefined,
        workoutPlanStartDate: '03-10-2010',
        fixPaymentAmount: '0000000000000015.00',
        cycledOverlimitCount: '00',
        oneCycleDelinquentAmount: '0000000000000015.00',
        twoCycleDelinquentAmount: '0000000000000015.00',
        threeCycleDelinquentAmount: '0000000000000015.00',
        fourCycleDelinquentAmount: '0000000000000015.00',
        fiveCycleDelinquentAmount: '0000000000000015.00',
        sixCycleDelinquentAmount: '0000000000000015.00',
        sevenCycleDelinquentAmount: '0000000000000465.00',
        collectionsEntryDate: '00000000',
        noCollectionHistoryExitDate: undefined,
        noHighCyclesDelinquency: undefined,
        noCollectionDaysWork: undefined,
        noHighDaysDelinquent: undefined,
        highBalanceAmount: '0000000000001080',
        noHighDelinquentAmount: undefined,
        tabPromise: {
          rows: {
            amount1: undefined,
            number1: undefined,
            amount2: undefined,
            number2: undefined
          },
          total: { amount: undefined, number: undefined }
        },
        noChanged: undefined,
        noPaymentReceived: undefined
      }
    });
    expect(response.type).toEqual('account/getAccountCollection/fulfilled');
  });

  it('Async Thunk work correctly with default data', async () => {
    const store = createStore(rootReducer, {});
    spy = jest
      .spyOn(accountCollectionService, 'getAccountCollectionData')
      .mockResolvedValue({ ...responseDefault });

    const response = await getAccountCollection(mockData)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual('account/getAccountCollection/fulfilled');
  });
});
