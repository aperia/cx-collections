import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// helpers
import isEmpty from 'lodash.isempty';
import { mappingDataFromObj } from 'app/helpers';
import { getData } from '../helpers';

// types
import { Args, InitialState, Payload } from './types';

import accountCollectionService from '../accountCollectionService';

/**
 * get account detail info
 */
export const getAccountCollection = createAsyncThunk<
  Payload,
  Args,
  ThunkAPIConfig
>('account/getAccountCollection', async (args, thunkAPI) => {
  const { accountNumber } = args.postData;
  const state = thunkAPI.getState();
  const accountMapping = state?.mapping?.data?.accountCollection || {};
  const { data } = await accountCollectionService.getAccountCollectionData(
    accountNumber
  );

  if (isEmpty(data)) return { data: {} };

  const customers = getData(data) as MagicKeyValue;
  const mappedData = mappingDataFromObj(customers, accountMapping);

  return { data: mappedData };
});

/**
 * - get search account list extra reducers builder
 */
export const getAccountCollectionBuilder = (
  builder: ActionReducerMapBuilder<InitialState>
) => {
  builder
    .addCase(getAccountCollection.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        loading: true
      };
    })
    .addCase(getAccountCollection.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        loading: false,
        error: false,
        data
      };
    })
    .addCase(getAccountCollection.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        loading: false,
        error: true
      };
    });
};
