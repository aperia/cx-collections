import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import {
  getAccountCollection,
  getAccountCollectionBuilder
} from './getAccountCollection';

// types
import { InitialState } from './types';

const { actions, reducer } = createSlice({
  name: 'accountCollection',
  initialState: {} as InitialState,
  reducers: {
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    }
  },
  extraReducers: builder => {
    getAccountCollectionBuilder(builder);
  }
});

const accountCollection = {
  ...actions,
  getAccountCollection
};

export { accountCollection, reducer };
