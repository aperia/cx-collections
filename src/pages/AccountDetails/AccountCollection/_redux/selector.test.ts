import { storeId, selectorWrapper } from 'app/test-utils';
import { MappingState } from 'pages/__commons/MappingProvider/types';

import * as selectors from './selector';
import { AccountDetailOverview } from './types';

const SAMPLE_DATA: AccountDetailOverview = {
  data: {},
  error: true,
  loading: true
};

const SAMPLE_MAPPING: MappingState = {
  loading: true,
  data: {}
};

const selectorTrigger = selectorWrapper(
  {
    accountCollection: {
      [storeId]: {
        ...SAMPLE_DATA
      }
    },
    mapping: {
      ...SAMPLE_MAPPING
    }
  },
  {
    accountCollection: { [storeId]: {} },
  }
);


describe('Test selectors', () => {
  it('selectData', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectData(storeId));
    expect(data).toEqual(SAMPLE_DATA.data);
    expect(emptyData).toEqual(undefined);
  });

  it('selectIsError', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectIsError(storeId));
    expect(data).toEqual(SAMPLE_DATA.error);
    expect(emptyData).toEqual(false);
  });
  it('selectLoading', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectLoading(storeId));
    expect(data).toEqual(SAMPLE_DATA.loading);
    expect(emptyData).toEqual(false);
  });
  it('selectMapping', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectMapping());
    expect(data).toEqual(SAMPLE_MAPPING.data);
    expect(emptyData).toEqual(undefined);
  });
});
