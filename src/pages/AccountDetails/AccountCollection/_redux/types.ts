export interface AccountDetailOverview {
  loading?: boolean;
  error?: boolean;
  data?: MagicKeyValue;
}

export interface InitialState {
  [storeId: string]: AccountDetailOverview;
}

export interface Payload {
  data: MagicKeyValue;
}

export interface Args {
  storeId: string;
  postData: { accountNumber: string };
}
