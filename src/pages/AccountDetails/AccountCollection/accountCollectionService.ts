import { apiService } from 'app/utils/api.service';
// constants
import { ACCOUNT_OVERVIEW_COLLECTION } from './constants';

const accountCollectionService = {
  getAccountCollectionData: (accountNumber: string) => {
    const url = window.appConfig?.api?.account?.getOverviewCollection || '';
    const requestBody = {
      common: { accountId: accountNumber },
      selectFields: ACCOUNT_OVERVIEW_COLLECTION
    };
    return apiService.post(url, requestBody);
  }
};

export default accountCollectionService;
