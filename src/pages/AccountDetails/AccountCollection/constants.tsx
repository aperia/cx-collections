import { I18N_COMMON_TEXT } from 'app/constants/i18n';

export const I18N_ACCOUNT_COLLECTION = {
  DELINQUENT: 'txt_delinquent',
  DELINQUENCY_HISTORY: 'txt_delinquency_history',
  NUMBER_OF_TIMES_CUSTOMER_DELINQUENT:
    'txt_numberOfTimes_a_customer_delinquent',
  OTHER: 'txt_other',
  DELINQUENCY_CYCLE_SUMMARY: 'txt_delinquency_cycle_summary',
  AMOUNT_THAT_A_CUSTOMER_HAS_BEEN_DELINQUENT:
    'txt_amount_that_a_customer_has_been_delinquent',
  COLLECTION_HISTORY: 'txt_collection_history',
  PROMISE_DETAILS: 'txt_promise_details',
  COLLECTION_INFORMATION: 'txt_collection_information'
};

export const FIELDS = [
  {
    groupKey: 'delinquent',
    descriptor: 'accountCollectionDelinquent',
    header: I18N_ACCOUNT_COLLECTION.DELINQUENT
  },
  {
    groupKey: 'delinquencyHistory',
    descriptor: 'accountCollectionDelinquencyHistory',
    header: {
      label: I18N_ACCOUNT_COLLECTION.DELINQUENCY_HISTORY,
      tooltip: I18N_ACCOUNT_COLLECTION.NUMBER_OF_TIMES_CUSTOMER_DELINQUENT
    }
  },
  {
    groupKey: 'other',
    descriptor: 'accountCollectionOther',
    header: I18N_COMMON_TEXT.OTHER
  },
  {
    groupKey: 'delinquencyCycleSummary',
    descriptor: 'accountCollectionDelinquencyCycleSummary',
    header: {
      label: I18N_ACCOUNT_COLLECTION.DELINQUENCY_CYCLE_SUMMARY,
      tooltip:
        I18N_ACCOUNT_COLLECTION.AMOUNT_THAT_A_CUSTOMER_HAS_BEEN_DELINQUENT
    }
  },
  {
    groupKey: 'collectionHistory',
    descriptor: 'accountCollectionCollectionHistory',
    header: I18N_ACCOUNT_COLLECTION.COLLECTION_HISTORY
  }
];

export const ACCOUNT_OVERVIEW_COLLECTION = [
  'customerInquiry.delinquentAmount',
  'customerInquiry.externalStatusReasonCode',
  'customerInquiry.delinquentStartDate',
  'customerInquiry.daysDelinquentCount',
  'customerInquiry.totalNumberOfTimesOneCycleDelinquentCount',
  'customerInquiry.totalNumberOfTimesTwoCyclesDelinquentCount',
  'customerInquiry.totalNumberOfTimesThreeCyclesDelinquentCount',
  'customerInquiry.collectorCode',
  'customerInquiry.workoutPlanStartDate',
  'customerInquiry.fixPaymentAmount',
  'customerInquiry.cycledOverlimitCount',
  'customerInquiry.oneCycleDelinquentAmount',
  'customerInquiry.twoCycleDelinquentAmount',
  'customerInquiry.threeCycleDelinquentAmount',
  'customerInquiry.fourCycleDelinquentAmount',
  'customerInquiry.fiveCycleDelinquentAmount',
  'customerInquiry.sixCycleDelinquentAmount',
  'customerInquiry.sevenCycleDelinquentAmount',
  'customerInquiry.collectionsEntryDate',
  'customerInquiry.highBalanceAmount',
  'collectionStatus.namePrincipalCardholder',
  'collectionHistory.collectionsEntryDate',
  'collectionHistory.collectionsExitDate',
  'collectionHistory.cyclesDelinquent',
  'collectionHistory.highestDaysDelinquent',
  'collectionHistory.highestAccountBalance',
  'collectionHistory.highestDelinquentAmount'
];
