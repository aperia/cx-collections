import React, { useEffect } from 'react';
import {
  DropdownList,
  Modal,
  ModalHeader,
  ModalTitle,
  ModalFooter,
  InlineMessage,
  DropdownBaseChangeEvent
} from 'app/_libraries/_dls/components';
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';
import { useFormik } from 'formik';
import isEmpty from 'lodash.isempty';
import { batch, useDispatch } from 'react-redux';

// hooks- helpers
import { useStoreIdSelector, useAccountDetail } from 'app/hooks';

// redux
import { accountDetailSnapshotActions } from '../_redux';
import {
  takeExternalStatusRefDataLoadingStatus,
  takeErrorBusinessMessage,
  takeExternalStatusCodeConfig,
  takeReasonCodeConfig,
  getCustomerInquiryData,
  getCustomerRoleTypeData
} from '../_redux/selector';
import { takeCurrentExternalStatus } from 'pages/AccountDetails/_redux/selectors';

// hooks- helpers
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { SNAPSHOT_LANG } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';

import {
  AccountExternalStatusDataType,
  ReasonCodesDataType
} from 'pages/ClientConfiguration/AccountExternalStatus/types';
import { getValueAndDescription } from 'pages/ClientConfiguration/AccountExternalStatus/helpers';
import { useDisableSubmitButton } from 'app/hooks/useDisableSubmitButton';
import { I18N_ACCOUNT_EXTERNAL_STATUS_TEXT } from 'pages/ClientConfiguration/AccountExternalStatus/constants';
import { takeAccessToken } from 'pages/__commons/AccessToken/_redux/selector';

interface EditExternalStatusModalProps {
  onCloseModal: () => void;
  isOpenModal: boolean;
}

interface ValuesProps {
  externalStatus?: RefDataValue;
  reasonCode?: RefDataValue;
}

const EditExternalStatusModal: React.FC<EditExternalStatusModalProps> = ({
  onCloseModal,
  isOpenModal
}) => {
  const dispatch = useDispatch();
  const {
    accEValue,
    storeId,
    memberSequenceIdentifier,
    socialSecurityIdentifier
  } = useAccountDetail();

  const { t } = useTranslation();

  const { operatorID } = window.appConfig.commonConfig;

  const STATUS_NORMAL = ' ';
  const currentExternalStatusCode =
    useStoreIdSelector<string>(takeCurrentExternalStatus) || STATUS_NORMAL;

  const externalStatusRefData = useStoreIdSelector<
    AccountExternalStatusDataType[]
  >(takeExternalStatusCodeConfig);

  const getCustomerInquiry = useStoreIdSelector<MagicKeyValue[]>(
    getCustomerInquiryData
  );

  const getCustomerRoleType = useStoreIdSelector<string>(
    getCustomerRoleTypeData
  );

  const reasonRefData =
    useStoreIdSelector<ReasonCodesDataType[]>(takeReasonCodeConfig);

  const isLoading = useStoreIdSelector<boolean>(
    takeExternalStatusRefDataLoadingStatus
  );

  const errorBusinessMessage = useStoreIdSelector<string>(
    takeErrorBusinessMessage
  );

  const accountToken = useStoreIdSelector<any>(takeAccessToken);

  const testId = 'edit-external-status-modal';

  const checkList = externalStatusRefData?.find(
    item => item?.value === currentExternalStatusCode
  )?.checkList;

  const getFilterReasonCode = getCustomerInquiry?.filter(
    (item: MagicKeyValue) => item.customerRoleTypeCode === getCustomerRoleType
  )[0];

  const {
    errors,
    values,
    setValues,
    isValid,
    submitForm,
    handleChange,
    setFieldValue,
    handleBlur,
    touched
  } = useFormik<ValuesProps>({
    initialValues: {},
    enableReinitialize: true,
    onSubmit: ({ externalStatus, reasonCode }) => {
      batch(() => {
        dispatch(
          accountDetailSnapshotActions.triggerUpdateAccountExternalStatusCode({
            storeId,
            postData: {
              accountId: accEValue,
              externalStatusCode: externalStatus?.value || '',
              externalStatusDescription: externalStatus?.description || '',
              reasonCode: reasonCode?.value || '',
              reasonDescription: reasonCode?.description || '',
              accountToken: accountToken || '',
              operatorID: operatorID || ''
            },
            memberSequenceIdentifier,
            socialSecurityIdentifier
          })
        );
      });
    },
    validateOnBlur: true,
    validateOnChange: true,
    validate: ({ reasonCode, externalStatus }) => {
      const validateError: Record<string, string> | undefined = {};

      !reasonCode &&
        (validateError['reasonCode'] = t(
          I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.REASON_IS_REQUIRED
        ));
      !externalStatus &&
        (validateError['externalStatus'] = t(
          I18N_ACCOUNT_EXTERNAL_STATUS_TEXT.EXTERNAL_STATUS_IS_REQUIRED
        ));
      return validateError;
    }
  });

  const { disableSubmit, setPrimitiveValues } = useDisableSubmitButton(values);

  useEffect(() => {
    if (isEmpty(externalStatusRefData)) return;

    const defaultExternalStatus = externalStatusRefData.find(
      item => item.value === currentExternalStatusCode
    ) as RefDataValue;

    const getDataExternalStatusByCode = reasonRefData?.find(
      item => item.value === getFilterReasonCode?.externalStatusReasonCode
    ) as RefDataValue;

    const changeValue = {
      externalStatus: defaultExternalStatus,
      reasonCode: getDataExternalStatusByCode
    };
    setValues(changeValue);
    setPrimitiveValues(changeValue);
  }, [
    setValues,
    externalStatusRefData,
    reasonRefData,
    getFilterReasonCode?.externalStatusReasonCode,
    currentExternalStatusCode,
    setPrimitiveValues
  ]);

  useEffect(() => {
    dispatch(accountDetailSnapshotActions.getExternalConfig({ storeId }));
  }, [dispatch, storeId]);

  if (!isOpenModal) return null;

  return (
    <Modal
      sm
      show={isOpenModal}
      loading={isLoading}
      dataTestId={genAmtId(testId, '', '')}
    >
      <ModalHeader
        border
        closeButton
        onHide={onCloseModal}
        dataTestId={genAmtId(testId, 'header', '')}
      >
        <ModalTitle dataTestId={genAmtId(testId, 'header-title', '')}>
          {t(SNAPSHOT_LANG.EDIT_EXTERNAL_STATUS)}
        </ModalTitle>
      </ModalHeader>
      <ModalBodyWithApiError
        apiErrorClassName="mb-24"
        storeId={storeId}
        dataTestId={genAmtId(testId, 'body', '')}
      >
        {errorBusinessMessage && (
          <InlineMessage
            variant="danger"
            dataTestId={genAmtId(testId, 'body-bussiness-message', '')}
          >
            {errorBusinessMessage}
          </InlineMessage>
        )}
        <div className="row">
          <div className="col-6">
            <DropdownList
              name="externalStatus"
              textFieldRender={(itemValue: ReasonCodesDataType) => {
                if (itemValue.value === ' ') return itemValue.description;
                return `${itemValue.value} - ${itemValue.description}` as any;
              }}
              label={t('txt_external_status')}
              onBlur={handleBlur}
              value={values.externalStatus}
              onChange={(e: DropdownBaseChangeEvent) => {
                handleChange(e), setFieldValue('reasonCode', undefined);
              }}
              errorTooltipProps={
                errorBusinessMessage ? { opened: false } : undefined
              }
              required
              error={{
                message: errors.externalStatus as string,
                status:
                  (touched.externalStatus && !!errors.externalStatus) ||
                  !!errorBusinessMessage
              }}
              dataTestId={genAmtId(testId, 'body-external-status', '')}
              noResult={t('txt_no_results_found')}
            >
              {externalStatusRefData
                ?.filter(
                  item =>
                    checkList?.[item.value]?.checked ||
                    item.value === currentExternalStatusCode
                )
                ?.map(item => (
                  <DropdownList.Item
                    key={item.value}
                    label={getValueAndDescription(item)}
                    value={item}
                  />
                ))}
            </DropdownList>
          </div>
          <div className="col-12 mt-16">
            <DropdownList
              name="reasonCode"
              textFieldRender={(itemValue: ReasonCodesDataType) => {
                if (itemValue.value === ' ') return itemValue.description;
                return `${itemValue.value} - ${itemValue.description}` as any;
              }}
              label={t('txt_reason')}
              onBlur={handleBlur}
              value={values.reasonCode}
              onChange={handleChange}
              required
              errorTooltipProps={
                errorBusinessMessage ? { opened: false } : undefined
              }
              error={{
                message: errors.reasonCode as string,
                status: touched.reasonCode && !!errors.reasonCode
              }}
              dataTestId={genAmtId(testId, 'body-reason-code', '')}
              noResult={t('txt_no_results_found')}
            >
              {reasonRefData
                ?.filter(
                  item =>
                    values.externalStatus?.value &&
                    item.checkList?.[values.externalStatus?.value]?.checked &&
                    item.checkList?.display?.checked
                )
                ?.map(item => (
                  <DropdownList.Item
                    key={item.value}
                    label={getValueAndDescription(item)}
                    value={item}
                  />
                ))}
            </DropdownList>
          </div>
        </div>
      </ModalBodyWithApiError>
      <ModalFooter
        okButtonText={t('txt_save')}
        cancelButtonText={t('txt_cancel')}
        onCancel={onCloseModal}
        onOk={submitForm}
        disabledOk={isEmpty(externalStatusRefData) || !isValid || disableSubmit}
        dataTestId={genAmtId(testId, 'footer', '')}
      />
    </Modal>
  );
};

export default EditExternalStatusModal;
