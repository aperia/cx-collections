import React from 'react';
import { screen } from '@testing-library/react';
import * as formik from 'formik';
import userEvent from '@testing-library/user-event';

import {
  accEValue,
  mockActionCreator,
  renderMockStoreId,
  storeId
} from 'app/test-utils';

// components
import EditExternalStatusModal from '.';
import * as useDisableSubmit from 'app/hooks/useDisableSubmitButton';
import {
  DropdownBaseChangeEvent,
  ModalProps
} from 'app/_libraries/_dls/components';

// redux
import { accountDetailSnapshotActions } from '../_redux';
import { accountExternalStatusActions } from 'pages/ClientConfiguration/AccountExternalStatus/_redux/reducers';
import { act } from 'react-dom/test-utils';

HTMLCanvasElement.prototype.getContext = jest.fn();

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');

  return {
    ...dlsComponents,
    Modal: (props: ModalProps) => <div>{props.children}</div>
  };
});

window.appConfig = {
  commonConfig: {
    operatorID: 'ACB'
  }
} as AppConfiguration;

const emptyState: Partial<RootState> = {
  accountDetail: {
    [storeId]: {
      data: {}
    }
  },
  accountDetailSnapshot: {
    [storeId]: {
      editExternalStatus: {
        errorBusinessMessage: 'true',
        isLoading: false,
        isOpenModal: false,
        externalStatusRefData: [],
        reasonRefData: []
      }
    }
  },
  accessToken: {
    [storeId]: {
      accountId: '022009001002624',
      accountInfo: {
        token: '99f0ecae-dbf0-4c6c-b76b-2fbf97927674'
      }
    }
  }
};

const initialState: Partial<RootState> = {
  clientConfiguration: {
    settings: {
      selectedConfig: {
        id: '9999100020003000',
        agentId: '3000',
        clientId: '9999',
        systemId: '1000',
        principleId: '2000'
      }
    }
  },
  accountDetail: {
    [storeId]: {
      data: {
        externalStatusCode: 'value',
        customerRoleTypeCode: 'value'
      },
      customerInquiry: [
        {
          customerRoleTypeCode: 'value',
          externalStatusReasonCode: 'value'
        }
      ],
      rawData: {
        clientIdentifier: '9999',
        systemIdentifier: '1000',
        principalIdentifier: '2000',
        agentIdentifier: '3000'
      }
    }
  },
  accountExternalStatus: {
    accountExternalStatusList: {
      data: [
        {
          description: 'accountExternalStatus description 1',
          value: ' ',
          checkList: {
            value: { checked: false }
          }
        },
        {
          description: 'accountExternalStatus description 2',
          value: 'value',
          checkList: {
            value: { checked: false }
          }
        },
        {
          description: 'accountExternalStatus description 3',
          value: 'value',
          checkList: {
            value: { checked: true }
          }
        },
        {
          description: 'accountExternalStatus description 4',
          value: '',
          checkList: {
            value: { checked: true }
          }
        }
      ]
    },
    data: {
      reasonCode: [
        {
          description: 'des',
          value: 'value'
        },
        {
          description: 'des',
          value: ''
        }
      ],
      externalCode: [
        {
          description: 'des',
          value: 'value'
        },
        {
          description: 'des',
          value: ''
        }
      ]
    },
    isUpdating: false,
    reasonCodeList: {
      data: [
        {
          description: 'reasonCode description 1',
          value: 'value',
          checkList: {
            value: { checked: true },
            display: { checked: true }
          }
        },
        {
          description: 'reasonCode description 2',
          value: 'value',
          checkList: {
            value: { checked: true },
            display: { checked: true }
          }
        },
        {
          description: 'reasonCode description 3',
          value: ' ',
          checkList: {
            value: { checked: true },
            display: { checked: true }
          }
        },
        {
          description: 'reasonCode description 4',
          value: '',
          checkList: {
            value: { checked: true },
            display: { checked: true }
          }
        }
      ],
      modalErrorMessage: '',
      modalLoading: false,
      modalType: '',
      open: false
    }
  },
  accountDetailSnapshot: {
    [storeId]: {
      editExternalStatus: {
        reasonRefData: [
          { description: 'test', value: 'test' },
          { description: 'test-2', value: '' }
        ],
        externalStatusRefData: [],
        errorBusinessMessage: '',
        externalConfig: {
          externalStatusCode: [
            {
              description: 'accountExternalStatus description 1',
              value: ' ',
              checkList: {
                value: { checked: false }
              }
            },
            {
              description: 'accountExternalStatus description 2',
              value: 'value',
              checkList: {
                value: { checked: false }
              }
            },
            {
              description: 'accountExternalStatus description 3',
              value: 'value',
              checkList: {
                value: { checked: true }
              }
            },
            {
              description: 'accountExternalStatus description 4',
              value: '',
              checkList: {
                value: { checked: true }
              }
            }
          ],
          externalStatusReasonCode: [
            {
              description: 'reasonCode description 1',
              value: 'value',
              checkList: {
                value: { checked: true },
                display: { checked: true }
              }
            },
            {
              description: 'reasonCode description 2',
              value: 'value',
              checkList: {
                value: { checked: true },
                display: { checked: true }
              }
            },
            {
              description: 'reasonCode description 3',
              value: ' ',
              checkList: {
                value: { checked: true },
                display: { checked: true }
              }
            },
            {
              description: 'reasonCode description 4',
              value: '',
              checkList: {
                value: { checked: true },
                display: { checked: true }
              }
            }
          ]
        }
      }
    }
  }
};

const accountDetailSnapshotActionsSpy = mockActionCreator(
  accountDetailSnapshotActions
);
const accountExternalStatusActionsSpy = mockActionCreator(
  accountExternalStatusActions
);

beforeEach(() => {
  accountExternalStatusActionsSpy('getAccountStatusCodeList');
  accountDetailSnapshotActionsSpy('getExternalConfig');
  jest
    .spyOn(useDisableSubmit, 'useDisableSubmitButton')
    .mockImplementation(() => ({
      disableSubmit: false,
      setPrimitiveValues: jest.fn()
    }));
});

beforeEach(() => {
  jest.spyOn(formik, 'useFormik').mockImplementation((({
    validate,
    onSubmit
  }: {
    validate: Function;
    onSubmit: Function;
  }) => {
    return {
      setFieldValue: jest.fn(),
      setValues: jest.fn(),
      handleReset: jest.fn(),
      handleChange: jest.fn(),
      handleBlur: jest.fn(),
      isValid: true,
      submitForm: onSubmit,
      validateForm: validate,
      errors: {},
      touched: { externalStatus: {} },
      values: {
        externalStatus: { value: ' ', description: 'desc' },
        reasonCode: { value: ' ', description: 'desc' }
      }
    };
  }) as any);
});

describe('Test EditExternalStatusModal', () => {
  window.appConfig = {
    commonConfig: {}
  } as AppConfiguration;

  it('Render UI with isOpenModal false', () => {
    const { wrapper } = renderMockStoreId(
      <EditExternalStatusModal isOpenModal={false} onCloseModal={jest.fn()} />,
      { initialState: {} }
    );

    expect(wrapper.container.innerHTML).toEqual('');
  });

  it('Render with error data', () => {
    const { wrapper } = renderMockStoreId(
      <EditExternalStatusModal isOpenModal={true} onCloseModal={jest.fn()} />,
      { initialState: emptyState }
    );

    expect(wrapper.getByText('txt_edit_external_status')).toBeInTheDocument();
  });

  it('customHandleChange', () => {
    const { wrapper } = renderMockStoreId(
      <EditExternalStatusModal isOpenModal={true} onCloseModal={jest.fn()} />,
      { initialState }
    );

    const icon = wrapper.baseElement.querySelectorAll('.icon-chevron-down');

    jest.useFakeTimers();
    userEvent.click(icon[0]);
    jest.runAllTimers();

    //SelectItem
    const item = wrapper.getByText(
      'value - accountExternalStatus description 2'
    );

    act(() => {
      userEvent.click(item);
    });
  });

  it('handleSubmit', () => {
    const mockAction = accountDetailSnapshotActionsSpy(
      'triggerUpdateAccountExternalStatusCode'
    );

    jest.spyOn(formik, 'useFormik').mockImplementation((({
      validate,
      onSubmit
    }: {
      validate: Function;
      onSubmit: Function;
    }) => {
      return {
        setFieldValue: jest.fn(),
        setValues: jest.fn(),
        handleReset: jest.fn(),
        handleChange: ({ value }: DropdownBaseChangeEvent) => {
          validate(value.value === ' ' ? {} : { reasonCode: value });
        },
        handleBlur: jest.fn(),
        isValid: true,
        submitForm: onSubmit,
        validateForm: validate,
        errors: {},
        touched: { reasonCode: {} },
        values: {
          externalStatus: {
            value: 'value',
            description: 'desc'
          } as RefDataValue,
          reasonCode: { value: 'value', description: 'desc' }
        }
      };
    }) as any);

    const {
      wrapper: { container }
    } = renderMockStoreId(
      <EditExternalStatusModal isOpenModal={true} onCloseModal={jest.fn()} />,
      { initialState }
    );

    // show dropdown reason
    act(() => {
      userEvent.click(container.querySelectorAll('.text-field-container')[1]!);
    });

    // select reasonCode with value is ' '
    act(() => {
      userEvent.click(screen.getByText(/reasonCode description 3/));
    });

    // select reasonCode with value is 'value'
    act(() => {
      userEvent.click(screen.getByText(/reasonCode description 2/));
    });

    // click submit
    userEvent.click(screen.getByText('txt_save'));

    expect(mockAction).toBeCalledWith({
      storeId,
      postData: {
        accountId: accEValue,
        externalStatusCode: '',
        externalStatusDescription: '',
        reasonCode: '',
        accountToken: '',
        reasonDescription: '',
        operatorID: ''
      },
      memberSequenceIdentifier: 'memberSequenceIdentifier',
      socialSecurityIdentifier: 'socialSecurityIdentifier'
    });
  });
});
