import { createSelector } from '@reduxjs/toolkit';

export const getExternalStatusData = (state: RootState, storeId: string) => {
  return state.accountDetailSnapshot[storeId]?.editExternalStatus;
};

export const getAccountDetailData = (state: RootState, storeId: string) => {
  return state.accountDetail[storeId]?.data;
};

export const getCustomerInquiry = (state: RootState, storeId: string) => {
  return state.accountDetail[storeId]?.customerInquiry;
};

export const getCustomerRoleType = (state: RootState, storeId: string) => {
  return state.accountDetail[storeId]?.data?.customerRoleTypeCode;
};

// External data
const normalExternalStatus = ' ';
export const takeCurrentExternalStatus = createSelector(
  getAccountDetailData,
  data => data?.externalStatusCode || normalExternalStatus
);

export const takeExternalStatusRefData = createSelector(
  getExternalStatusData,
  data => data?.externalStatusRefData
);

export const getCustomerRoleTypeData = createSelector(
  getCustomerRoleType,
  data => data
);

export const getCustomerInquiryData = createSelector(
  getCustomerInquiry,
  data => data
);

export const takeReasonRefData = createSelector(
  getExternalStatusData,
  data => data?.reasonRefData
);

const DEFAULT_EXTERNAL = {
  externalStatusCode: [],
  externalStatusReasonCode: []
};
export const takeExternalStatusConfig = createSelector(
  (states: RootState, storeId: string) =>
    states.accountDetailSnapshot[storeId]?.editExternalStatus?.externalConfig ??
    DEFAULT_EXTERNAL,
  data => data
);

export const takeExternalStatusCodeConfig = createSelector(
  (states: RootState, storeId: string) =>
    states.accountDetailSnapshot[storeId]?.editExternalStatus?.externalConfig,
  data => data?.externalStatusCode || DEFAULT_EXTERNAL.externalStatusCode
);

export const takeReasonCodeConfig = createSelector(
  (states: RootState, storeId: string) =>
    states.accountDetailSnapshot[storeId]?.editExternalStatus?.externalConfig,
  data =>
    data?.externalStatusReasonCode || DEFAULT_EXTERNAL.externalStatusReasonCode
);

export const takeExternalStatusRefDataLoadingStatus = createSelector(
  getExternalStatusData,
  data => data?.isLoading
);

export const takeErrorBusinessMessage = createSelector(
  getExternalStatusData,
  data => data?.errorBusinessMessage
);

export const takeExternalStatusDataOpenModalStatus = createSelector(
  getExternalStatusData,
  data => data?.isOpenModal
);
