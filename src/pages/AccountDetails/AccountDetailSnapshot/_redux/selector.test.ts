import { storeId } from 'app/test-utils';
import { rootReducer } from 'storeConfig';

import * as selectors from './selector';
import { createStore } from 'redux';

const store = createStore(rootReducer);
const state = store.getState();

state.accountDetail = {
  [storeId]: {
    data: {
      externalStatusCode: 'F'
    }
  }
};

state.accountDetailSnapshot = {
  [storeId]: {
    editExternalStatus: {
      errorBusinessMessage: 'errorBusinessMessage',
      isLoading: false,
      isOpenModal: false,
      reasonRefData: [],
      externalStatusRefData: [],
      externalConfig: {
        externalStatusCode: [
          {
            value: 'test',
            description: 'test',
            checkList: {
              A: {
                checked: true
              }
            }
          }
        ],
        externalStatusReasonCode: [
          {
            value: 'test',
            description: 'test',
            checkList: {
              A: {
                checked: true
              }
            }
          }
        ]
      }
    }
  }
};

describe('redux-store > account-detail-snapshot > selector', () => {
  it('takeCurrentExternalStatus', () => {
    const result = selectors.takeCurrentExternalStatus(state, storeId);
    expect(result).toEqual(
      state.accountDetail[storeId]?.data?.externalStatusCode
    );

    const emptyResult = selectors.takeCurrentExternalStatus(
      createStore(rootReducer, {
        accountDetail: {
          [storeId]: {
            data: {
              externalStatusCode: undefined
            }
          }
        }
      }).getState(),
      storeId
    );
    expect(emptyResult).toEqual(' ');
  });

  it('takeExternalStatusRefData', () => {
    const result = selectors.takeReasonRefData(state, storeId);
    expect(result).toEqual(
      state.accountDetailSnapshot[storeId].editExternalStatus
        ?.externalStatusRefData
    );
  });

  it('takeExternalStatusConfig', () => {
    const result = selectors.takeExternalStatusConfig(state, storeId);
    expect(result).toEqual(
      state.accountDetailSnapshot[storeId].editExternalStatus?.externalConfig
    );

    const emptyResult = selectors.takeExternalStatusConfig(
      createStore(rootReducer, {
        accountDetailSnapshot: {
          [storeId]: {
            editExternalStatus: {}
          }
        }
      }).getState(),
      storeId
    );

    expect(emptyResult).toEqual({
      externalStatusCode: [],
      externalStatusReasonCode: []
    });
  });

  it('takeReasonRefData', () => {
    const result = selectors.takeReasonRefData(state, storeId);
    expect(result).toEqual(
      state.accountDetailSnapshot[storeId].editExternalStatus?.reasonRefData
    );
  });

  it('takeExternalStatusRefDataLoadingStatus', () => {
    const result = selectors.takeExternalStatusRefDataLoadingStatus(
      state,
      storeId
    );
    expect(result).toEqual(
      state.accountDetailSnapshot[storeId].editExternalStatus!.isLoading
    );
  });

  it('takeErrorBusinessMessage', () => {
    const result = selectors.takeErrorBusinessMessage(state, storeId);
    expect(result).toEqual(
      state.accountDetailSnapshot[storeId].editExternalStatus
        ?.errorBusinessMessage
    );
  });

  it('takeExternalStatusDataOpenModalStatus', () => {
    const result = selectors.takeExternalStatusDataOpenModalStatus(
      state,
      storeId
    );
    expect(result).toEqual(
      state.accountDetailSnapshot[storeId]?.editExternalStatus?.isOpenModal
    );
  });

  it('takeErrorBusinessMessage', () => {
    const result = selectors.takeErrorBusinessMessage(state, storeId);
    expect(result).toEqual(
      state.accountDetailSnapshot[storeId].editExternalStatus
        ?.errorBusinessMessage
    );
  });

  it('takeExternalStatusRefData', () => {
    const result = selectors.takeExternalStatusRefData(state, storeId);
    expect(result).toEqual(
      state.accountDetailSnapshot[storeId].editExternalStatus
        ?.externalStatusRefData
    );
  });
});
