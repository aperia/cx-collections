import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { mappingDataFromArray } from 'app/helpers';
// service
import accountDetailService from '../accountDetailService';
// helpers
import {
  AccountDetailSnapshotInitialState,
  GetExternalStatusRefDataArgs,
  GetExternalStatusRefDataPayload
} from '../types';

const mappingFullDescription = {
  value: 'value',
  description: '{value} - {description}'
};

/**
 * get get External Status RefData
 */
export const getExternalStatusRefData = createAsyncThunk<
  GetExternalStatusRefDataPayload,
  GetExternalStatusRefDataArgs,
  ThunkAPIConfig
>('account/getExternalStatusRefData', async (args, { getState }) => {
  const { accountId } = args.postData;

  const [{ data: externalStatusRefData }, { data: reasonRefData }] =
    await Promise.all([
      accountDetailService.getExternalStatusRefData(accountId),
      accountDetailService.getReasonRefData(accountId)
    ]);
  if (!externalStatusRefData || !reasonRefData)
    return {
      externalStatusRefData: [],
      reasonRefData: []
    };

  const state = getState();
  const currentExternalStatusCode =
    state.accountDetail[args.storeId]?.data?.externalStatusCode;

  let currentExternalStatus: RefDataValue | undefined;

  if (currentExternalStatusCode) {
    currentExternalStatus = externalStatusRefData.find(
      (e: RefDataValue) => e.value === currentExternalStatusCode
    );
  }

  return {
    externalStatusRefData,
    reasonRefData: mappingDataFromArray(reasonRefData, mappingFullDescription),
    currentExternalStatus
  };
});

/**
 * - get search account list extra reducers builder
 */
export const getExternalStatusRefDataBuilder = (
  builder: ActionReducerMapBuilder<AccountDetailSnapshotInitialState>
) => {
  builder
    .addCase(getExternalStatusRefData.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...(draftState[storeId] || {}),
        editExternalStatus: {
          ...(draftState[storeId]?.editExternalStatus || {}),
          isLoading: true
        }
      };
    })
    .addCase(getExternalStatusRefData.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { externalStatusRefData, reasonRefData, currentExternalStatus } =
        action.payload;

      draftState[storeId].editExternalStatus = {
        ...draftState[storeId]?.editExternalStatus,
        isLoading: false,
        externalStatusRefData,
        reasonRefData,
        currentExternalStatus
      };
    })
    .addCase(getExternalStatusRefData.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].editExternalStatus = {
        ...draftState[storeId]?.editExternalStatus,
        isLoading: false
      };
    });
};
