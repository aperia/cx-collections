import './index';
import { getExternalConfig } from './getExternalConfig';
import accountDetailService from '../accountDetailService';
import { responseDefault, storeId } from 'app/test-utils';
import { EXTERNAL_STATUS_COMPONENT_ID } from 'app/constants';
import { createStore, Store } from '@reduxjs/toolkit';
import { rootReducer } from 'storeConfig';

let store: Store<RootState>;

beforeEach(() => {
  store = createStore(rootReducer, {
    accountDetailSnapshot: { [storeId]: {} }
  });
});

describe('async thunk', () => {
  it('return data', async () => {
    const mReturnValue = {
      accountExternalStatus: {
        externalStatusCode: [],
        externalStatusReasonCode: []
      }
    };
    const spy = jest
      .spyOn(accountDetailService, 'getExternalConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          clientConfig: {
            components: [
              {
                component: EXTERNAL_STATUS_COMPONENT_ID,
                jsonValue: mReturnValue
              }
            ]
          }
        }
      });
    const response = await getExternalConfig({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.payload).toEqual(mReturnValue);
    spy.mockReset();
    spy.mockRestore();
  });

  it('return no data', async () => {
    const spy = jest
      .spyOn(accountDetailService, 'getExternalConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          clientConfig: {
            components: [
              {
                component: EXTERNAL_STATUS_COMPONENT_ID,
                jsonValue: {}
              }
            ]
          }
        }
      });
    const response = await getExternalConfig({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.payload).toEqual({});
    spy.mockReset();
    spy.mockRestore();
  });

  it('reject with no data', async () => {
    const spy = jest
      .spyOn(accountDetailService, 'getExternalConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          clientConfig: {
            components: []
          }
        }
      });
    const response = await getExternalConfig({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.payload).toEqual({ errorMessage: 'no data' });
    expect(response.type).toEqual(getExternalConfig.rejected.type);
    spy.mockReset();
    spy.mockRestore();
  });
});
