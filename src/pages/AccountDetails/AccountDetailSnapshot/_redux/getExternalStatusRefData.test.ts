// actions
import { accountDetailSnapshotActions } from './index';
import accountDetailService from '../accountDetailService';
import { storeId } from 'app/test-utils';
import { createStore, Store } from 'redux';
import { rootReducer } from 'storeConfig';
import { AxiosResponse } from 'axios';

const requestDataArgs = {
  storeId,
  postData: { accountId: 'accountId' }
};

const responseDefault: AxiosResponse<any> = {
  data: null,
  status: 200,
  statusText: '',
  headers: {},
  config: {}
};

let spy1: jest.SpyInstance;
let spy2: jest.SpyInstance;

describe('fetching data using getExternalStatusRefData', () => {
  let store: Store<RootState>;
  beforeEach(() => {
    store = createStore(rootReducer, { accountDetailSnapshot: {} });
  });

  afterEach(() => {
    if (spy1) {
      spy1.mockReset();
      spy1.mockRestore();
    }
    if (spy2) {
      spy2.mockReset();
      spy2.mockRestore();
    }
  });

  
  it('fetching data with currentExternalStatusCode having returned data', async () => {
    const mockRefData = [{ value: '00' }];
    spy1 = jest
      .spyOn(accountDetailService, 'getExternalStatusRefData')
      .mockResolvedValue({ ...responseDefault, data: mockRefData });
    spy2 = jest
      .spyOn(accountDetailService, 'getReasonRefData')
      .mockResolvedValue({ ...responseDefault, data: mockRefData });



    await accountDetailSnapshotActions.getExternalStatusRefData(
      requestDataArgs
    )(store.dispatch,() =>  ({...store.getState(), accountDetail: {[storeId]: {data: {externalStatusCode: 'f'}}}}), {});

    expect(
      store.getState().accountDetailSnapshot[storeId].editExternalStatus
        ?.externalStatusRefData
    ).toEqual(mockRefData);
  });

  it('fetching data having returned data', async () => {
    const mockRefData = [{ value: '00' }];
    spy1 = jest
      .spyOn(accountDetailService, 'getExternalStatusRefData')
      .mockResolvedValue({ ...responseDefault, data: mockRefData });
    spy2 = jest
      .spyOn(accountDetailService, 'getReasonRefData')
      .mockResolvedValue({ ...responseDefault, data: mockRefData });

    await accountDetailSnapshotActions.getExternalStatusRefData(
      requestDataArgs
    )(store.dispatch, store.getState, {});

    expect(
      store.getState().accountDetailSnapshot[storeId].editExternalStatus
        ?.externalStatusRefData
    ).toEqual(mockRefData);
  });

  it('fetching data with nodata return', async () => {
    const mockRefData = undefined;
    const defaultRefData = [] as any[];
    spy1 = jest
      .spyOn(accountDetailService, 'getExternalStatusRefData')
      .mockResolvedValue({ ...responseDefault, data: mockRefData });
    spy2 = jest
      .spyOn(accountDetailService, 'getReasonRefData')
      .mockResolvedValue({ ...responseDefault, data: mockRefData });

    await accountDetailSnapshotActions.getExternalStatusRefData(
      requestDataArgs
    )(store.dispatch, store.getState, {});

    expect(
      store.getState().accountDetailSnapshot[storeId].editExternalStatus
        ?.externalStatusRefData
    ).toEqual(defaultRefData);
  });

  it('fetching data with error : lost network', async () => {
    spy1 = jest
      .spyOn(accountDetailService, 'getExternalStatusRefData')
      .mockRejectedValue({ ...responseDefault });
    spy2 = jest
      .spyOn(accountDetailService, 'getReasonRefData')
      .mockRejectedValue({ ...responseDefault });

    await accountDetailSnapshotActions.getExternalStatusRefData(
      requestDataArgs
    )(store.dispatch, store.getState, {});

    expect(
      store.getState().accountDetailSnapshot[storeId].editExternalStatus
        ?.isLoading
    ).toEqual(false);
  });
});
