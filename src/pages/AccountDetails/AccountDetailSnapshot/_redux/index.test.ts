import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { accountDetailSnapshotActions } from 'pages/AccountDetails/AccountDetailSnapshot/_redux';
import { AccountDetailInitialState } from 'pages/AccountDetails/AccountDetailSnapshot/_redux/types';

const store = createStore(rootReducer);

const state = store.getState();

const storeId = 'storeId';

describe('Unit test account search list reducer', () => {
  it('removeStore', () => {
    const action = accountDetailSnapshotActions.removeStore({ storeId });
    const actual = rootReducer(state, action);

    expect(actual.accountSearchList[storeId]).toBeUndefined();
  });

  it('toggleExternalStatusModal', () => {
    state.accountDetailSnapshot = {
      [storeId]: {
        loading: false,
        error: false,
        data: null,
        isEnd: false,
        showMoreRowIds: {}
      }
    } as AccountDetailInitialState;
    const action = accountDetailSnapshotActions.toggleExternalStatusModal({
      storeId
    });
    const actual = rootReducer(state, action);

    expect(
      actual.accountDetailSnapshot[storeId]?.editExternalStatus?.isOpenModal
    ).toEqual(true);
  });
});
