import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Actions
import {
  getExternalStatusRefData,
  getExternalStatusRefDataBuilder
} from './getExternalStatusRefData';

import {
  triggerUpdateAccountExternalStatusCode,
  updateAccountExternalStatusCodeBuilder
} from './updateAccountExternalStatusCode';

import {
  getExternalConfig,
  getExternalConfigBuilder
} from './getExternalConfig';

// types
import { AccountDetailSnapshotInitialState } from '../types';

const { actions, reducer } = createSlice({
  name: 'accountDetailSnapshot',
  initialState: {} as AccountDetailSnapshotInitialState,
  reducers: {
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    toggleExternalStatusModal: (
      draftState,
      action: PayloadAction<StoreIdPayload>
    ) => {
      const { storeId } = action.payload;
      const currentOpenModal =
        draftState[storeId]?.editExternalStatus?.isOpenModal;

      draftState[storeId] = {
        ...draftState[storeId],
        editExternalStatus: {
          ...draftState[storeId]?.editExternalStatus,
          isOpenModal: !currentOpenModal
        }
      };
    }
  },
  extraReducers: builder => {
    getExternalStatusRefDataBuilder(builder);
    updateAccountExternalStatusCodeBuilder(builder);
    getExternalConfigBuilder(builder);
  }
});

const accountDetailSnapshotActions = {
  ...actions,
  triggerUpdateAccountExternalStatusCode,
  getExternalStatusRefData,
  getExternalConfig
};

export { accountDetailSnapshotActions, reducer as accountDetailSnapshot };
