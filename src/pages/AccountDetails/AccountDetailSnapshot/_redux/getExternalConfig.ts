import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { EXTERNAL_STATUS_COMPONENT_ID } from 'app/constants';
import accountDetailService from '../accountDetailService';

import { selectClientInfoGetByIdRequest } from 'pages/__commons/AccountDetailTabs/_redux';

import { AccountDetailSnapshotInitialState, ExternalConfig } from '../types';
import isEmpty from 'lodash.isempty';

export const getExternalConfig = createAsyncThunk<
  { accountExternalStatus: ExternalConfig },
  StoreIdPayload,
  ThunkAPIConfig
>('account/getExternalConfig', async (args, thunkAPI) => {
  const { getState } = thunkAPI;
  const commonClientInfoRequest = selectClientInfoGetByIdRequest(
    getState(),
    args.storeId
  );

  const { data } = await accountDetailService.getExternalConfig({
    common: { ...commonClientInfoRequest.common },
    selectConfig: 'clientConfig',
    selectComponent: EXTERNAL_STATUS_COMPONENT_ID
  });

  const filterConfig = (
    data.clientConfig.components as CollectionsClientConfigItem['components']
  ).find(item => item.component === EXTERNAL_STATUS_COMPONENT_ID);

  if (!filterConfig) {
    return thunkAPI.rejectWithValue({ errorMessage: 'no data' });
  }
  return filterConfig.jsonValue as { accountExternalStatus: ExternalConfig };
});

export const getExternalConfigBuilder = (
  builder: ActionReducerMapBuilder<AccountDetailSnapshotInitialState>
) => {
  builder.addCase(getExternalConfig.pending, (draftState, action) => {
    const { storeId } = action.meta.arg;
    draftState[storeId].editExternalStatus = {
      ...draftState[storeId].editExternalStatus,
      isLoading: true,
      externalConfig: { externalStatusCode: [], externalStatusReasonCode: [] }
    };
  });
  builder.addCase(getExternalConfig.fulfilled, (draftState, action) => {
    const { storeId } = action.meta.arg;
    const { accountExternalStatus } = action.payload;
    draftState[storeId].editExternalStatus = {
      ...draftState[storeId].editExternalStatus,
      externalConfig: !isEmpty(accountExternalStatus)
        ? accountExternalStatus
        : {
            externalStatusCode: [],
            externalStatusReasonCode: []
          },
      isLoading: false
    };
  });
  builder.addCase(getExternalConfig.rejected, (draftState, action) => {
    const { storeId } = action.meta.arg;
    draftState[storeId].editExternalStatus = {
      ...draftState[storeId].editExternalStatus,
      externalConfig: { externalStatusCode: [], externalStatusReasonCode: [] },
      isLoading: false
    };
  });
};
