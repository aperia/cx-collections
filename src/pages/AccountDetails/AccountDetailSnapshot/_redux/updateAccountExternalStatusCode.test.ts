// actions
import { accountDetailSnapshotActions } from './index';
import { updateAccountExternalStatusCode } from './updateAccountExternalStatusCode';
import { rootReducer } from 'storeConfig';
import { ExternalBodyRequestArgs } from '../types';
import { createStore } from 'redux';
import accountDetailService from '../accountDetailService';
import { AxiosResponse } from 'axios';
import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  storeId
} from 'app/test-utils';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { ACCOUNT_DETAILS_ACTIONS } from 'pages/Memos/constants';
import { NORMAL_CODE } from '../constants';
import { ReasonCodesDataType } from 'pages/ClientConfiguration/AccountExternalStatus/types';
const requestData: ExternalBodyRequestArgs = {
  storeId,
  postData: {
    accountId: storeId,
    externalStatusCode: 'HNB',
    reasonCode: 'APM',
    externalStatusDescription: '',
    reasonDescription: ''
  }
};

const responseDefault: AxiosResponse<any> = {
  data: null,
  status: 200,
  statusText: '',
  headers: {},
  config: {}
};

let spy1: jest.SpyInstance;

afterEach(() => {
  spy1?.mockReset();
  spy1?.mockRestore();
});

describe('UpdateAccountExternalStatusCode', () => {
  const memoSpy = mockActionCreator(memoActions);

  afterEach(() => {
    if (spy1) {
      spy1.mockReset();
      spy1.mockRestore();
    }
  });

  it('it should be not call postMemo UPDATE_EXTERNAL_REASON_CD', async () => {
    const store = createStore(rootReducer, {
      accountDetailSnapshot: {
        [storeId]: {
          editExternalStatus: {
            reasonRefData: [{ value: 'test-2', description: 'test-2' }]
          }
        }
      },
      accountDetail: {
        [storeId]: {
          rawData: {
            externalStatusReasonCode: 'test-2'
          }
        }
      },
      accountDetailSnapshot: {
        [storeId]: {
          editExternalStatus: {
            externalConfig: {
              externalStatusCode: [],
              externalStatusReasonCode: [{ value: 'test-2', description: 'des' }]
            }
          }
        }
      }
    });
    const postMemoSpy = memoSpy('postMemo');

    const newParams = { ...requestData };
    newParams.postData.reasonCode = 'test-2';
    await accountDetailSnapshotActions.triggerUpdateAccountExternalStatusCode(
      newParams
    )(
      byPassFulfilled(updateAccountExternalStatusCode.fulfilled.type),
      store.getState,
      {}
    );
    expect(postMemoSpy).not.toBeCalledWith(
      ACCOUNT_DETAILS_ACTIONS.UPDATE_EXTERNAL_REASON_CD
    );
  });

  it('it should be not call postMemo UPDATE_EXTERNAL_ACC_STATUS', async () => {
    const store = createStore(rootReducer, {
      accountDetailSnapshot: {
        [storeId]: {
          editExternalStatus: {
            externalStatusRefData: [
              { value: 'test', description: 'test description' }
            ]
          }
        }
      },
      accountDetail: {
        [storeId]: {
          rawData: {
            externalStatusCode: 'HNB'
          }
        }
      }
    });
    const postMemoSpy = memoSpy('postMemo');

    await accountDetailSnapshotActions.triggerUpdateAccountExternalStatusCode(
      requestData
    )(
      byPassFulfilled(updateAccountExternalStatusCode.fulfilled.type),
      store.getState,
      {}
    );
    expect(postMemoSpy).not.toBeCalledWith(
      ACCOUNT_DETAILS_ACTIONS.UPDATE_EXTERNAL_ACC_STATUS
    );
  });

  it('it should be call postMemo UPDATE_EXTERNAL_ACC_STATUS correctly', async () => {
    const postMemoSpy = memoSpy('postMemo');
    const store = createStore(rootReducer, {
      accountDetailSnapshot: {
        [storeId]: {
          editExternalStatus: {
            externalStatusRefData: [
              { value: 'test', description: 'test description' }
            ],
            externalConfig: {
              externalStatusCode: [
                {
                  value: 'test',
                  description: 'test',
                  checkList: {
                    A: {
                      checked: true
                    }
                  }
                }
              ],
              externalStatusReasonCode: []
            }
          }
        }
      },
      accountDetail: {
        [storeId]: {
          rawData: {
            externalStatusCode: 'test'
          }
        }
      }
    });
    await accountDetailSnapshotActions.triggerUpdateAccountExternalStatusCode(
      requestData
    )(
      byPassFulfilled(updateAccountExternalStatusCode.fulfilled.type),
      store.getState,
      {}
    );
    expect(postMemoSpy).toBeCalled();
  });

  it('it should be call postMemo UPDATE_EXTERNAL_ACC_STATUS with existingStatusCode = NORMAL_CODE correctly', async () => {
    const postMemoSpy = memoSpy('postMemo');
    const store = createStore(rootReducer, {
      accountDetailSnapshot: {
        [storeId]: {
          editExternalStatus: {
            externalStatusRefData: [
              { value: 'test', description: 'test description' }
            ]
          }
        }
      },
      accountDetail: {
        [storeId]: {
          rawData: {
            externalStatusCode: NORMAL_CODE
          }
        }
      },
      accountExternalStatus: {
        reasonCodeList: {
          data: [
            {
              value: 'test',
              description: 'test',
              checkList: {
                A: {
                  checked: true
                }
              }
            }
          ] as ReasonCodesDataType[]
        }
      }
    });

    await accountDetailSnapshotActions.triggerUpdateAccountExternalStatusCode(
      requestData
    )(
      byPassFulfilled(updateAccountExternalStatusCode.fulfilled.type),
      store.getState,
      {}
    );
    expect(postMemoSpy).toBeCalled();
  });

  it('it should be call postMemo UPDATE_EXTERNAL_ACC_STATUS with externalStatusCode = NORMAL_CODE correctly', async () => {
    const postMemoSpy = memoSpy('postMemo');
    const store = createStore(rootReducer, {
      accountDetailSnapshot: {
        [storeId]: {
          editExternalStatus: {
            externalStatusRefData: [
              { value: 'test', description: 'test description' }
            ]
          }
        }
      },
      accountDetail: {
        [storeId]: {
          rawData: {
            externalStatusCode: 'test'
          }
        }
      }
    });

    const newRequestData = { ...requestData };
    newRequestData.postData.externalStatusCode = NORMAL_CODE;

    await accountDetailSnapshotActions.triggerUpdateAccountExternalStatusCode(
      newRequestData
    )(
      byPassFulfilled(updateAccountExternalStatusCode.fulfilled.type),
      store.getState,
      {}
    );
    expect(postMemoSpy).toBeCalled();
  });

  it('action thunk > isFullFilled', async () => {
    const store = createStore(rootReducer);
    const toastSpy = mockActionCreator(actionsToast);
    const addToastAction = toastSpy('addToast');

    await accountDetailSnapshotActions.triggerUpdateAccountExternalStatusCode(
      requestData
    )(
      byPassFulfilled(updateAccountExternalStatusCode.fulfilled.type),
      store.getState,
      {}
    );

    expect(addToastAction).toBeCalledWith({
      show: true,
      type: 'success',
      message: 'txt_toast_external_status_updated'
    });

    await accountDetailSnapshotActions.triggerUpdateAccountExternalStatusCode(
      requestData
    )(
      byPassRejected(updateAccountExternalStatusCode.fulfilled.type),
      store.getState,
      {}
    );
  });

  it('action thunk > isRejected', async () => {
    const store = createStore(rootReducer);
    const toastSpy = mockActionCreator(actionsToast);
    const addToastAction = toastSpy('addToast');

    await accountDetailSnapshotActions.triggerUpdateAccountExternalStatusCode(
      requestData
    )(
      byPassRejected(updateAccountExternalStatusCode.fulfilled.type),
      store.getState,
      {}
    );

    expect(addToastAction).toBeCalledWith({
      show: true,
      type: 'error',
      message: 'txt_toast_external_status_failed_to_update'
    });
  });

  it('action thunk > success', async () => {
    const store = createStore(rootReducer);
    spy1 = jest
      .spyOn(accountDetailService, 'updateExternalStatusCode')
      .mockResolvedValue({ ...responseDefault, data: {} });

    await updateAccountExternalStatusCode(requestData)(
      store.dispatch,
      store.getState,
      {}
    );
  });

  it('action thunk > reject', async () => {
    const store = createStore(rootReducer);
    spy1 = jest
      .spyOn(accountDetailService, 'updateExternalStatusCode')
      .mockRejectedValue({ ...responseDefault, data: {} });

    await updateAccountExternalStatusCode(requestData)(
      store.dispatch,
      store.getState,
      {}
    );
  });
});
