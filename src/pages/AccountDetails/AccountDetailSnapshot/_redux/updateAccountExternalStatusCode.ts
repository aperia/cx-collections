import {
  createAsyncThunk,
  ActionReducerMapBuilder,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// service
import accountDetailService from '../accountDetailService';

// helpers
import { batch } from 'react-redux';
import {
  AccountDetailSnapshotInitialState,
  ExternalBodyRequestArgs,
  ExternalBodyRequestPayload
} from '../types';

// Actions
import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';
import { accountDetailSnapshotActions } from '.';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { ACCOUNT_DETAILS_ACTIONS } from 'pages/Memos/constants';
import { NORMAL_CODE, NORMAL_CODE_DESCRIPTION } from '../constants';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { CHRONICLE_ACCOUNT_DETAILS_KEY } from 'pages/ClientConfiguration/Memos/constants';
import { accessTokenActions } from 'pages/__commons/AccessToken/_redux/reducer';

/**
 * update Account External Status Code
 */
export const updateAccountExternalStatusCode = createAsyncThunk<
  ExternalBodyRequestPayload,
  ExternalBodyRequestArgs,
  ThunkAPIConfig
>('account/updateAccountExternalStatusCode', async (args, thunkAPI) => {
  const { postData } = args;
  const { accountId, externalStatusCode, reasonCode } = postData;

  try {
    const requestBody = {
      common: {
        accountId
      },
      externalStatusCode,
      externalStatusReasonCode: reasonCode
    };
    const { data } = await accountDetailService.updateExternalStatusCode(
      requestBody
    );
    return data;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const triggerUpdateAccountExternalStatusCode = createAsyncThunk<
  void,
  ExternalBodyRequestArgs,
  ThunkAPIConfig
>(
  'account/triggerUpdateAccountExternalStatusCode',
  async (args, { dispatch, getState }) => {
    const { accountDetail, accountDetailSnapshot } = getState();

    const response = await dispatch(updateAccountExternalStatusCode(args));

    if (isFulfilled(response)) {
      const {
        storeId,
        memberSequenceIdentifier,
        socialSecurityIdentifier,
        postData: {
          accountId,
          externalStatusCode,
          reasonCode,
          externalStatusDescription,
          reasonDescription,
          accountToken,
          operatorID
        }
      } = args;
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: 'txt_toast_external_status_updated'
          })
        );
        dispatch(
          accountDetailSnapshotActions.toggleExternalStatusModal({ storeId })
        );
        dispatch(
          accountDetailActions.getAccountDetail({
            storeId,
            postData: {
              common: {
                accountId
              },
              inquiryFields: {
                memberSequenceIdentifier
              }
            },
            socialSecurityIdentifier,
            isReload: true
          })
        );

        dispatch(
          apiErrorNotificationAction.clearApiErrorData({
            storeId: response.meta.arg.storeId,
            forSection: 'inModalBody'
          })
        );

        dispatch(
          accessTokenActions.updateCollectionAccount({
            accountToken,
            agentId: operatorID,
            extStatus: externalStatusCode,
            statusRcd: reasonCode
          })
        );

        // Post memo when status changes
        const existingStatusCode =
          accountDetail[storeId]?.rawData?.externalStatusCode;

        const externalStatusList =
          accountDetailSnapshot[storeId]?.editExternalStatus?.externalConfig
            ?.externalStatusCode;

        const existingStatusDescription =
          externalStatusList?.find(item => item?.value === existingStatusCode)
            ?.description || '';

        if (existingStatusCode !== externalStatusCode) {
          dispatch(
            memoActions.postMemo(
              ACCOUNT_DETAILS_ACTIONS.UPDATE_EXTERNAL_ACC_STATUS,
              {
                storeId,
                accEValue: accountId,
                chronicleKey: CHRONICLE_ACCOUNT_DETAILS_KEY,
                cardholderContactedName:
                  accountDetail[storeId]?.data?.primaryName,
                existingValue: `${
                  existingStatusCode === NORMAL_CODE
                    ? NORMAL_CODE_DESCRIPTION
                    : existingStatusCode
                } - ${existingStatusDescription}`,
                newValue: `${
                  externalStatusCode === NORMAL_CODE
                    ? NORMAL_CODE_DESCRIPTION
                    : externalStatusCode
                } - ${externalStatusDescription}`
              }
            )
          );
        }
        // Post memo when reason code changes

        const reasonCodeList =
          accountDetailSnapshot[storeId]?.editExternalStatus?.externalConfig
            ?.externalStatusReasonCode;

        const existingReasonCode =
          accountDetail[storeId]?.rawData?.externalStatusReasonCode;

        const existingReasonValue = `${existingReasonCode} - ${
          reasonCodeList?.find(item => item?.value === existingReasonCode)
            ?.description || ''
        }`;

        if (existingReasonCode !== reasonCode) {
          dispatch(
            memoActions.postMemo(
              ACCOUNT_DETAILS_ACTIONS.UPDATE_EXTERNAL_REASON_CD,
              {
                storeId,
                accEValue: accountId,
                chronicleKey: CHRONICLE_ACCOUNT_DETAILS_KEY,
                cardholderContactedName:
                  accountDetail[storeId]?.data?.primaryName,
                existingValue: existingReasonValue,
                newValue: `${reasonCode} - ${reasonDescription}`
              }
            )
          );
        }
      });
    }

    if (isRejected(response)) {
      batch(() => {
        dispatch(
          apiErrorNotificationAction.updateApiError({
            storeId: response.meta.arg.storeId,
            apiResponse: response.payload?.response,
            forSection: 'inModalBody'
          })
        );

        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'error',
            message: 'txt_toast_external_status_failed_to_update'
          })
        );
      });
    }
  }
);

/**
 * - get search account list extra reducers builder
 */
export const updateAccountExternalStatusCodeBuilder = (
  builder: ActionReducerMapBuilder<AccountDetailSnapshotInitialState>
) => {
  builder
    .addCase(updateAccountExternalStatusCode.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...(draftState[storeId] || {}),
        editExternalStatus: {
          ...(draftState[storeId]?.editExternalStatus || {}),
          isLoading: true
        }
      };
    })
    .addCase(
      updateAccountExternalStatusCode.fulfilled,
      (draftState, action) => {
        const { storeId } = action.meta.arg;
        draftState[storeId].editExternalStatus = {
          ...draftState[storeId]?.editExternalStatus,
          isLoading: false,
          errorBusinessMessage: undefined
        };
      }
    )
    .addCase(updateAccountExternalStatusCode.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].editExternalStatus = {
        ...draftState[storeId].editExternalStatus,
        isLoading: false
      };
    });
};
