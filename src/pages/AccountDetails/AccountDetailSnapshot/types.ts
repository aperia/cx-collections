import {
  AccountExternalStatusDataType,
  ReasonCodesDataType
} from 'pages/ClientConfiguration/AccountExternalStatus/types';

export interface AccountDetailSnapshot {
  editExternalStatus?: {
    errorBusinessMessage?: string;
    isLoading?: boolean;
    isOpenModal?: boolean;
    reasonRefData?: RefDataValue[];
    externalStatusRefData?: RefDataValue[];
    currentExternalStatus?: RefDataValue;
    externalConfig?: ExternalConfig;
  };
}

export type ExternalConfig = {
  externalStatusCode: AccountExternalStatusDataType[];
  externalStatusReasonCode: ReasonCodesDataType[];
};

export interface AccountDetailSnapshotInitialState {
  [storeId: string]: AccountDetailSnapshot;
}

export interface GetExternalStatusRefDataPayload {
  externalStatusRefData: RefDataValue[];
  reasonRefData: RefDataValue[];
  currentExternalStatus?: RefDataValue;
}

export interface GetExternalStatusRefDataArgs {
  storeId: string;
  postData: { accountId: string };
}

export interface ExternalBodyRequestPayload { }
export interface ExternalBodyRequestArgs {
  storeId: string;
  memberSequenceIdentifier?: string;
  socialSecurityIdentifier?: string;
  postData: {
    accountId: string;
    externalStatusCode: string;
    externalStatusDescription: string;
    reasonCode: string;
    reasonDescription: string;
    accountToken: string;
    operatorID: string;
  };
}
export interface ExternalStatusRequestData extends CommonAccountIdRequestBody {
  externalStatusCode: string;
  externalStatusReasonCode: string;
}
