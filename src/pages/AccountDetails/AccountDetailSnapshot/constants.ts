export const VIEW_NAME = {
  account: 'accountDetailSnapshotAccountInfo'
};

export const VIEW_TITLE = {
  account: 'Account Information'
};

export const SNAPSHOT_CONFIG = 'config/snapshot.json';

export const NORMAL_CODE = ' ';
export const NORMAL_CODE_DESCRIPTION = 'BLANK';
