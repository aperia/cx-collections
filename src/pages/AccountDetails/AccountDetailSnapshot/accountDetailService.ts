import { apiService } from 'app/utils/api.service';

// Type
import { ExternalStatusRequestData } from './types';

const accountDetailService = {
  getExternalStatusRefData: (accountId?: string) => {
    const url = window.appConfig?.api?.account?.getExternalStatusRefData || '';
    return apiService.get(url);
  },

  getExternalConfig(requestBody: {
    common: Record<string, string>;
    selectConfig: string;
    selectComponent: string;
  }) {
    const url = window.appConfig?.api?.account?.getExternalConfig || '';
    return apiService.post(url, requestBody);
  },

  getReasonRefData: (accountId?: string) => {
    const url =
      window.appConfig?.api?.account?.getExternalStatusReasonRefData || '';
    return apiService.get(url);
  },

  updateExternalStatusCode: (data: ExternalStatusRequestData) => {
    const url =
      window.appConfig?.api?.account?.updateAccountExternalStatusCode || '';
    return apiService.post(url, data);
  }
};
export default accountDetailService;
