import accountDetailService from './accountDetailService';

// utils
import { storeId, mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import { ExternalStatusRequestData } from './types';

describe('accountDetailService', () => {
  describe('getExternalStatusRefData', () => {
    const params = storeId;

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      accountDetailService.getExternalStatusRefData(params);

      expect(mockService).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      accountDetailService.getExternalStatusRefData(params);

      expect(mockService).toBeCalledWith(
        apiUrl.account.getExternalStatusRefData
      );
    });
  });

  describe('getReasonRefData', () => {
    const params = storeId;

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      accountDetailService.getReasonRefData(params);

      expect(mockService).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      accountDetailService.getReasonRefData(params);

      expect(mockService).toBeCalledWith(
        apiUrl.account.getExternalStatusReasonRefData
      );
    });
  });

  describe('updateExternalStatusCode', () => {
    const params: ExternalStatusRequestData = {
      externalStatusCode: 'SE',
      externalStatusReasonCode: 'DE'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      accountDetailService.updateExternalStatusCode(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      accountDetailService.updateExternalStatusCode(params);

      expect(mockService).toBeCalledWith(
        apiUrl.account.updateAccountExternalStatusCode,
        params
      );
    });
  });
});
