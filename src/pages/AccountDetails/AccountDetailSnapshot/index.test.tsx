import React from 'react';

// redux
import { accountDetailActions } from '../_redux/reducer';
import { accountDetailSnapshotActions } from './_redux';

// components
import AccountDetailSnapshot from '.';

// helpers
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { mockScrollToFn } from 'app/test-utils/mocks/mockProperty';
import { PERMISSIONS } from 'app/entitlements/constants';
import * as entitlements from 'app/entitlements/helpers';

window.appConfig = {
  commonConfig: {
    operatorID: 'ACB'
  }
} as AppConfiguration;

const initialState: Partial<RootState> = {
  accountDetail: {
    [storeId]: {
      data: {
        externalStatusCode: 'F'
      }
    }
  },
  accountDetailSnapshot: {
    [storeId]: {
      editExternalStatus: {
        isOpenModal: true
      }
    }
  }
};

const errorState: Partial<RootState> = {
  accountDetail: {
    [storeId]: {
      data: {
        externalStatusCode: '01'
      },
      isError: true
    }
  }
};

const apiErrorState: Partial<RootState> = {
  accountDetail: {
    [storeId]: {
      data: undefined
    }
  },
  apiErrorNotification: {
    [storeId]: {
      errorDetail: {
        inSnapshot: {
          request: 'request',
          response: 'response'
        }
      }
    }
  }
};

const mockAccountDetailAction = mockActionCreator(accountDetailActions);
const mockAccountDetailSnapshotActions = mockActionCreator(
  accountDetailSnapshotActions
);

HTMLCanvasElement.prototype.getContext = jest.fn();
mockScrollToFn(jest.fn());

describe('Test Account Detail Overview', () => {
  beforeEach(() => {
    jest.useFakeTimers();
  });

  afterEach(() => {
    jest.runAllTimers();
  });

  it('Render UI', () => {
    jest.spyOn(entitlements, 'checkPermission').mockImplementation(() => {
      return true;
    });
    const spy = mockAccountDetailSnapshotActions('toggleExternalStatusModal');
    const { wrapper } = renderMockStoreId(<AccountDetailSnapshot />, {
      initialState
    });

    // OnClick handleToggleExternalStatusModal
    const button = wrapper.queryByTestId(
      'edit-external-status-modal_header_dls-modal-header_close-btn_dls-button'
    )!;
    button.click();

    expect(spy).toBeCalledWith({
      storeId
    });
  });

  it('Render UI with error', () => {
    const spy = mockAccountDetailAction('getAccountDetail');
    const { wrapper } = renderMockStoreId(<AccountDetailSnapshot />, {
      initialState: errorState
    });

    expect(wrapper.getByText('txt_failure_ocurred_on')).toBeInTheDocument();

    // Onclick Reload
    const button = wrapper.getByText(I18N_COMMON_TEXT.RELOAD)!;
    button.click();

    expect(spy).toBeCalledWith({
      storeId,
      postData: {
        common: {
          accountId: 'accEValue'
        },
        inquiryFields: {
          memberSequenceIdentifier: 'memberSequenceIdentifier'
        }
      },
      socialSecurityIdentifier: 'socialSecurityIdentifier',
      isReload: true
    });
  });

  it('Render with error api', () => {
    jest
      .spyOn(entitlements, 'checkPermission')
      .mockImplementation(permission => {
        if (permission === PERMISSIONS.ACCOUNT_SNAPSHOT_VIEW) return true;

        return false;
      });
    const { wrapper } = renderMockStoreId(<AccountDetailSnapshot />, {
      initialState: apiErrorState
    });

    expect(wrapper.getByText('txt_view_detail')).toBeInTheDocument();
  });
});
