import { FailedApiReload } from 'app/components';
import { ZERO } from 'app/constants';
import { SNAPSHOT_LANG } from 'app/constants/i18n';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';
// Hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { Button } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { ExtraFieldProps, View } from 'app/_libraries/_dof/core';
import classNames from 'classnames';
import isUndefined from 'lodash.isundefined';
import { useApiErrorNotification } from 'pages/ApiErrorNotification/hooks/useApiErrorNotification';
import TimerWorking from 'pages/TimerWorking';
import AlertGroup from 'pages/__commons/AlertGroup';
import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef
} from 'react';
import { useDispatch } from 'react-redux';
import { Field } from 'redux-form';
import { RELATED_ACCOUNT_FIELD_NAME } from '../RelatedAccount/constants';
// Types
import { AccountInfo } from '../types';
import { accountDetailActions } from '../_redux/reducer';
import {
  takeAccountDetail,
  takeAccountDetailErrorStatus
} from '../_redux/selectors';
// Constants
import { VIEW_NAME } from './constants';
// Components
import EditExternalStatusModal from './EditExternalStatusModal';
// Selector & Action
import { accountDetailSnapshotActions } from './_redux';
import { takeExternalStatusDataOpenModalStatus } from './_redux/selector';

export interface AccountDetailSnapshotProps {}

const AccountDetailSnapshot: React.FC<AccountDetailSnapshotProps> = () => {
  const dispatch = useDispatch();
  const {
    storeId,
    accEValue,
    memberSequenceIdentifier,
    socialSecurityIdentifier
  } = useAccountDetail();
  const { t } = useTranslation();
  const snapshotViewRef = useRef<any>(null);
  const divRef = useRef<HTMLDivElement | null>(null);
  const ApiErrorDetail = useApiErrorNotification({
    forSection: 'inSnapshot',
    storeId
  });

  const snapshotData = useStoreIdSelector<AccountInfo>(takeAccountDetail);

  const isError = useStoreIdSelector<boolean>(takeAccountDetailErrorStatus);

  const isOpenModal = useStoreIdSelector<boolean>(
    takeExternalStatusDataOpenModalStatus
  );

  const testId = 'account-detail-snapshot';

  const handleReload = useCallback(() => {
    dispatch(
      accountDetailActions.getAccountDetail({
        storeId,
        postData: {
          common: {
            accountId: accEValue
          },
          inquiryFields: {
            memberSequenceIdentifier
          }
        },
        socialSecurityIdentifier,
        isReload: true
      })
    );
  }, [
    dispatch,
    storeId,
    accEValue,
    memberSequenceIdentifier,
    socialSecurityIdentifier
  ]);

  const handleToggleExternalStatusModal = useCallback(() => {
    dispatch(
      accountDetailSnapshotActions.toggleExternalStatusModal({
        storeId
      })
    );
  }, [dispatch, storeId]);

  useEffect(() => {
    if (isUndefined(snapshotData)) return;
    setImmediate(() => {
      const relatedField: Field<ExtraFieldProps> =
        snapshotViewRef.current?.props?.onFind(RELATED_ACCOUNT_FIELD_NAME);

      const isDisabledRelatedField = snapshotData.relatedAccounts === ZERO;

      relatedField?.props?.props?.setEnable(!isDisabledRelatedField);
    });
  }, [snapshotData]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = '252px');
  }, []);

  const EditExternalStatusButton = useMemo(() => {
    if (
      !checkPermission(
        PERMISSIONS.ACCOUNT_SNAPSHOT_EDIT_EXTERNAL_CODE,
        storeId
      ) ||
      !checkPermission(PERMISSIONS.ACCOUNT_SNAPSHOT_VIEW, storeId)
    ) {
      return null;
    }
    return (
      <Button
        size="sm"
        id={`${accEValue}-external-status`}
        className="mr-n8"
        variant="outline-primary"
        onClick={handleToggleExternalStatusModal}
        dataTestId={`${testId}_external-status`}
      >
        {t(SNAPSHOT_LANG.EDIT_EXTERNAL_STATUS)}
      </Button>
    );
  }, [accEValue, handleToggleExternalStatusModal, storeId, t]);

  if (isError) {
    return (
      <div
        ref={divRef}
        className={classNames('bg-light-l20 d-flex align-items-center')}
      >
        <FailedApiReload
          id="account-detail-snapshot-failed"
          className="w-100"
          onReload={handleReload}
          dataTestId={`${testId}_api-reload-failed`}
        />
      </div>
    );
  }

  return (
    <div className="bg-light-l20 p-24">
      <ApiErrorDetail dataTestId={`${testId}_api-error`} className="mb-24" />
      <AlertGroup dataTestId={`${testId}_alerts`} />
      <div className="d-flex align-items-center justify-content-between">
        <div className="d-flex">
          <h3 data-testid={genAmtId(testId, 'primaryName', '')}>
            {snapshotData?.primaryName}
          </h3>
          <TimerWorking
            className="ml-14"
            dataTestId={genAmtId(testId, 'timmer', '')}
          />
        </div>
        {EditExternalStatusButton}
      </div>

      <div className="mt-8">
        {checkPermission(PERMISSIONS.ACCOUNT_SNAPSHOT_VIEW, storeId) && (
          <View
            id={`${accEValue}_${VIEW_NAME.account}`}
            formKey={`${accEValue}_${VIEW_NAME.account}_view`}
            descriptor={VIEW_NAME.account}
            value={snapshotData}
            ref={snapshotViewRef}
          />
        )}
      </div>

      {isOpenModal && (
        <EditExternalStatusModal
          onCloseModal={handleToggleExternalStatusModal}
          isOpenModal={isOpenModal}
        />
      )}
    </div>
  );
};

export default AccountDetailSnapshot;
