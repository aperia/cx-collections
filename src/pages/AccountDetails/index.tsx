import React, { useEffect, useMemo, useRef } from 'react';
import classNames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';
import { SimpleBar } from 'app/_libraries/_dls/components';
import isUndefined from 'lodash.isundefined';

// Components
import AccountDetailSnapshot from 'pages/AccountDetails/AccountDetailSnapshot';
import { InfoBar, TransactionTab } from 'pages/__commons';
import NextActionsButton from './NextActions';

// Hooks
import { AccountDetailProvider } from 'app/hooks';

// Selector
import {
  takeAccountDetailLoadingStatus,
  takeAccountDetailReloadingStatus,
  takeRefreshAcc
} from './_redux/selectors';
import { triggerGetAccountDetail } from './_redux/triggerGetAccountDetail';
import { takeContactEntitlementLoadingStatus } from 'pages/__commons/Entitlement/_redux/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';
import AccountLockTime from './AccountLockTime';
import { selectQueueId } from './NextActions/_redux/selector';
import { PERMISSIONS } from 'app/entitlements/constants';
import { checkPermission } from 'app/entitlements';
import TimerWorking from 'pages/TimerWorking';
import { NoDataGrid } from 'app/components';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface AccountDetailPageProps {
  primaryName: string;
  storeId: string;
  relatedStoreId?: string;
  accEValue: string;
  accNbr?: string;
  memberSequenceIdentifier?: string;
  socialSecurityIdentifier?: string;
}

const AccountDetailPage: React.FC<AccountDetailPageProps> = ({
  primaryName,
  memberSequenceIdentifier,
  socialSecurityIdentifier,
  storeId,
  relatedStoreId,
  accEValue,
  accNbr
}) => {
  const testId = 'account-detail';
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const contextValueRef = useRef({
    storeId,
    accEValue,
    accNbr,
    memberSequenceIdentifier,
    socialSecurityIdentifier
  });

  const { queueId } = useSelector(selectQueueId(accEValue)) || {};

  const isAllowView = [
    PERMISSIONS.ACCOUNT_SNAPSHOT_VIEW,
    PERMISSIONS.OVERVIEW_HISTORICAL_INFORMATION_VIEW,
    PERMISSIONS.OVERVIEW_MONETARY_INFORMATION_VIEW,
    PERMISSIONS.PAYMENT_LIST_VIEW,
    PERMISSIONS.PENDING_AUTHORIZATION_VIEW,
    PERMISSIONS.STATEMENT_VIEW
  ].find(item => checkPermission(item, storeId) === true);

  // watching the changes of storeId, accountId
  contextValueRef.current.storeId = storeId;
  contextValueRef.current.accEValue = accEValue;
  contextValueRef.current.accNbr = accNbr;
  contextValueRef.current.memberSequenceIdentifier = memberSequenceIdentifier;
  contextValueRef.current.socialSecurityIdentifier = socialSecurityIdentifier;

  const isLoadingAccountDetail = useSelector((state: RootState) =>
    takeAccountDetailLoadingStatus(state, storeId)
  );

  const isReloadingAccountDetail = useSelector((state: RootState) =>
    takeAccountDetailReloadingStatus(state, storeId)
  );

  const isLoadingContactEntitlement = useSelector((state: RootState) =>
    takeContactEntitlementLoadingStatus(state, storeId)
  );

  const isRefreshAcc = useSelector((state: RootState) =>
    takeRefreshAcc(state, storeId)
  );

  const isRefreshRelatedAcc = useSelector((state: RootState) =>
    takeRefreshAcc(state, relatedStoreId!)
  );

  useEffect(() => {
    if (relatedStoreId === storeId) return;
    dispatch(
      triggerGetAccountDetail({
        storeId,
        relatedStoreId,
        accEValue,
        memberSequenceIdentifier,
        socialSecurityIdentifier,
        queueId
      })
    );
  }, [
    dispatch,
    accEValue,
    storeId,
    relatedStoreId,
    memberSequenceIdentifier,
    socialSecurityIdentifier,
    queueId,
    isRefreshAcc,
    isRefreshRelatedAcc
  ]);

  const nonEntitlementsAreSetupView = useMemo(
    () => (
      <div className="info-bar-main">
        <SimpleBar>
          <div className="bg-light-l20 p-24 border-bottom">
            <div className="d-flex">
              <h3 data-testid={genAmtId(testId, 'primaryName', '')}>
                {primaryName}
              </h3>
              <TimerWorking
                className="ml-14"
                dataTestId={genAmtId(testId, 'timer', '')}
              />
            </div>
          </div>
          <NoDataGrid text="">
            <p
              className="mt-20 color-grey"
              data-testid={genAmtId(testId, 'no-data-grid_text', 'NoDataGrid')}
            >
              {t(I18N_COMMON_TEXT.NO_ENTITLEMENT_ARE_SETUP)}
              <br></br>
              {t(I18N_COMMON_TEXT.CONTACT_ADMIN_FOR_MORE_DETAILS)}
            </p>
          </NoDataGrid>
        </SimpleBar>
      </div>
    ),
    [primaryName, t]
  );

  const accountDetailView = useMemo(
    () => (
      <>
        <div className="info-bar-main">
          <SimpleBar>
            <AccountDetailSnapshot />
            <TransactionTab />
          </SimpleBar>
        </div>
        <InfoBar primaryName={primaryName} />
        <NextActionsButton />
        <AccountLockTime
          queueId={queueId || ''}
          storeId={storeId}
          accountId={accEValue}
        />
      </>
    ),
    [accEValue, primaryName, queueId, storeId]
  );

  if (
    isUndefined(isLoadingAccountDetail) ||
    isLoadingAccountDetail ||
    isUndefined(isLoadingContactEntitlement) ||
    isLoadingContactEntitlement
  ) {
    return (
      <div
        className={classNames('h-100', {
          loading: isLoadingAccountDetail || isLoadingContactEntitlement
        })}
        data-testid={genAmtId('accountDetails', 'tab', '')}
      />
    );
  }

  return (
    <AccountDetailProvider value={contextValueRef.current}>
      <div
        className={classNames('info-bar-wrapper', {
          loading: isReloadingAccountDetail
        })}
      >
        {isAllowView ? accountDetailView : nonEntitlementsAreSetupView}
      </div>
    </AccountDetailProvider>
  );
};

export default AccountDetailPage;
