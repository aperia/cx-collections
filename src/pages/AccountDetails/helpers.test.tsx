import { EMPTY_OBJECT, EMPTY_STRING } from 'app/constants';
import {
  parseLastSSNAccNbr,
  parseRelationshipSearchText,
  mappingRefData
} from './helpers';

const stringJsonValue = '{"maskedValue":"****1","eValue":"1"}';
const stringJsonSSN = '{"maskedValue":"0-1-ssn","eValue":"ssn"}';
describe('helper Account detail', () => {
  it('parseLastSSNAccNbr', () => {
    const data = {
      accNbr: stringJsonValue,
      ssn: stringJsonSSN
    };
    const result = parseLastSSNAccNbr(data);

    expect(result.accNbr).toEqual('****1');
    expect(result.lastSSN).toEqual('ssn');

    const resultIncorrectFormatJsonString = parseLastSSNAccNbr({
      accNbr: '123{',
      ssn: '123}'
    });
    expect(resultIncorrectFormatJsonString).toEqual(EMPTY_OBJECT);

    const resultEmptyString = parseLastSSNAccNbr({});

    expect(resultEmptyString.accNbr).toEqual(EMPTY_STRING);
    expect(resultEmptyString.lastSSN).toEqual(EMPTY_STRING);
  });

  it('parseRelationshipSearchText', () => {
    const result = parseRelationshipSearchText(stringJsonValue);

    expect(result).toEqual('1');

    const resultIncorrectFormatJsonString = parseRelationshipSearchText('123{');
    expect(resultIncorrectFormatJsonString).toEqual(EMPTY_STRING);

    const resultEmptyString = parseRelationshipSearchText('');
    expect(resultEmptyString).toEqual(EMPTY_STRING);
  });
});

describe('mappingRefData', () => {
  it('data has a empty value', async () => {
    const object = { name: 'test' };
    const data = ['addressCategory'];
    const result = await mappingRefData(object, data);
    expect(result).toEqual({
      ...object,
      addressCategory: {}
    });
  });

  it('data has a value', async () => {
    const object = { addressCategory: 'P' };
    const data = ['addressCategory'];
    const result = await mappingRefData(object, data);
    expect(result).toEqual({
      ...object,
      addressCategory: {
        description: 'txt_p_permanent',
        fieldID: 'P',
        fieldValue: 'Permanent'
      }
    });
  });
});
