import { apiService } from 'app/utils/api.service';

// Const
import { EMPTY_STRING } from 'app/constants';

// Type
import { AccountDetailResponse } from './types';

const accountDetailService = {
  getAccountDetails: (bodyRequest: CommonAccountIdRequestBody) => {
    const url =
      window.appConfig?.api?.account?.getAccountDetails || EMPTY_STRING;
    return apiService.post<AccountDetailResponse>(url, bodyRequest);
  },

  getAuthenticationConfig: (requestBody: {
    common: Record<string, string>;
  }) => {
    const url =
      window.appConfig?.api?.account?.getAuthenticationConfig || EMPTY_STRING;
    return apiService.post(url, requestBody);
  }
};
export default accountDetailService;
