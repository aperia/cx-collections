import { CallerAuthenticationData } from 'pages/CallerAuthenticate/types';

// Response
export interface AccountDetailResponse {
  customerInquiry?: Array<CustomerInquiry>;
  cardholderInfo?: Array<CardholderInfo>;
  collectionStatus?: CollectionStatus;
  collectionHistory?: CollectionHistory[];
}

export interface CollectionHistory {
  promiseToPayAmt?: string;
  promiseToPayCount?: string;
}

export interface CollectionStatus {
  promiseToPayAmount?: string;
  promiseToPayDate?: string;
}

export interface CardholderInfo {
  namePrincipalCardholder?: string;
  secondaryName?: string;
}

export interface CustomerInquiry {
  accountIdentifier?: string;
  agentBankName?: string;
  agentIdentifier?: string;
  availableCreditAmount?: string;
  behaviorScoreIdentifier?: string;
  billingCycleCode?: string;
  clientIdentifier?: string;
  collectionsEntryDate?: string;
  creditBureauScoreNumber?: string;
  creditLimitAmount?: string;
  currentBalanceAmount?: string;
  currentMinimumPaymentDueAmount?: string;
  customerExternalIdentifier?: string;
  customerRoleTypeCode?: string;
  daysDelinquentCount?: string;
  delinquentAmount?: number;
  externalStatusCode?: string;
  internalStatusCode?: string;
  lastMonetaryDate?: string;
  lastMonetaryType?: string;
  lastPaymentAmount?: string;
  lastPaymentDate?: string;
  lastReageDate?: string;
  lastStatementDate?: string;
  lastStatusCodeChangeDate?: string;
  memberSequenceIdentifier?: string;
  nextHeldStatementDestinationCode?: string;
  openDate?: string;
  paymentDueDate?: string;
  plasticReplacementIndicator?: string;
  presentationInstrumentIdentifier?: string;
  presentationInstrumentReplacementSequenceNumber?: string;
  presentationInstrumentTypeCode?: string;
  primaryCustomerName?: string;
  principalIdentifier?: string;
  reageCount?: string;
  separateEntityIndicator?: string;
  socialSecurityIdentifier?: string;
  systemIdentifier?: string;
  customerName?: string;
}

// Data type

export interface AccountInfo {
  accNbr?: string;
  eValueAccountId?: string;
  agentId?: string;
  clientID?: string;
  cycleCode?: string;
  cycleDate?: string;
  externalStatusCode?: string;
  currentBalanceAmount?: string;
  institutionName?: string;
  internalStatusCode?: string;
  ssn?: string;
  lastSSN?: string;
  primaryName?: string;
  principalId?: string;
  secondaryName?: string;
  statusDate?: string;
  sysId?: string;
  sysPrinAgent?: string;
  nextHeldStatementDestinationCode?: string;
  relatedAccounts?: number;
  cardHolderSecondaryName?: string;
  namePrincipalCardholder?: string;
  separateEntityIndicator?: string;
  memberSequenceIdentifier?: string;
  customerRoleTypeCode?: string;
  socialSecurityIdentifier?: string;
  historical?: HistoricalData;
  monetary?: MonetaryData;
  promiseToPayDate?: string | Date | null;
}

export interface MonetaryData {
  amountDelinquent?: string;
  amountDue?: string;
  availableCredit?: string;
  creditLimit?: string;
  currentBalance?: string;
  daysDelinquent?: string;
  existingPromiseToPay?: string;
  existingPromiseToPayAmount?: string;
  paymentDueDate?: string;
}

export interface HistoricalData {
  behaviorScore?: string;
  collectionEntryDate?: string;
  creditBureauScore?: string;
  lastMonetaryDate?: string;
  lastMonetaryType?: string;
  lastPaymentAmount?: string;
  lastPaymentDate?: string;
  lastReageDate?: string;
  openDate?: string;
  reageCount?: string;
}

// Arguments
export interface GetAccountDetailPayload {
  data: AccountInfo;
  rawData: MagicKeyValue;
  customerInquiry: MagicKeyValue[];
}

export interface GetAccountDetailArgs extends StoreIdPayload {
  postData: CommonAccountIdRequestBody;
  socialSecurityIdentifier?: string;
  isReload?: boolean;
  triggerRenewToken?: boolean;
}

// Reducer
export interface AccountDetail {
  data?: AccountInfo;
  rawData?: MagicKeyValue;
  isLoading?: boolean;
  isReloading?: boolean;
  isError?: boolean;
  isLoadingRenewRelated?: boolean;
  /** include all data of the account was transferred after authenticate*/
  rawAccountDetailData?: MagicKeyValue;
  /** only include data of combination of accountId and memberSequenceNumber*/
  accountDetailBySeqNumber?: AccountInfo;
  numberPhoneAuthenticated?: string;
  selectedAuthenticationData?: SelectedAuthenticationData;
  selectedCaller?: CallerAuthenticationData;
  customerInquiry?: MagicKeyValue[];
  sharedInfoTimer?: MagicKeyValue;
  isRefreshAccDetail?: boolean;
  secureInfo?: MagicKeyValue;
  accountCountTime?: {
    toggleExtendModal?: boolean;
    warningUnlockMsg?: string;
  };
}

export interface AccountDetailInitialState {
  [storeId: string]: AccountDetail | undefined;
}

export interface SelectedAuthenticationData {
  customerName?: string;
  /**CXC-3719 for more detail*/
  customerNameForCreateWorkflow?: string;
}

export interface SaveSelectedCallerPayload extends StoreIdPayload {
  data: CallerAuthenticationData;
  authData: {
    customerName?: string;
    /**CXC-3719 for more AuthCaller*/
    customerNameForCreateWorkflow?: string;
  };
}
