import React from 'react';
import { COMMON_STATUS } from 'app/constants';
import {
  Badge,
  ColumnType,
  Grid,
  TruncateText
} from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { PromiseDetailGridItem } from './types';
import { formatCommon } from 'app/helpers';

export interface PromiseDetailGridProps {
  gridData: PromiseDetailGridItem[];
}

export const PromiseDetailGrid: React.FC<PromiseDetailGridProps> = ({
  gridData
}) => {
  const { t } = useTranslation();
  const testId = 'infoBar_collectionInfo_promiseToPayGrid';

  const columns: ColumnType<PromiseDetailGridItem>[] = [
    {
      id: 'promiseId',
      Header: t('txt_promise_id'),
      accessor: 'promiseId',
      isSort: true,
      width: 124
    },
    {
      id: 'promiseDate',
      Header: t('txt_promise_date'),
      accessor: (p: PromiseDetailGridItem) =>
        formatCommon(p.promiseDueDate).time.date,
      isSort: true,
      width: 125,
      autoWidth: true
    },
    {
      id: 'promiseAmount',
      Header: t('txt_promise_amount'),
      accessor: (p: PromiseDetailGridItem) =>
        formatCommon(p.promiseAmount).currency(2),
      isSort: true,
      width: 234,
      cellProps: { className: 'text-right' },
      autoWidth: true
    },
    {
      id: 'paidAmount',
      Header: t('txt_paid_amount'),
      accessor: (p: PromiseDetailGridItem) =>
        formatCommon(p.promiseAmountPaid).currency(2),
      isSort: true,
      width: 234,
      cellProps: { className: 'text-right' },
      autoWidth: true
    },
    {
      id: 'method',
      Header: t('txt_method'),
      accessor: (p: PromiseDetailGridItem) => t(p.paymentMethod?.description),
      isSort: true,
      width: 148
    },
    {
      id: 'status',
      Header: t('txt_status'),
      accessor: (p: PromiseDetailGridItem) => (
        <Badge color={COMMON_STATUS[p.promiseStatus]} textTransformNone>
          <TruncateText title={p.promiseStatus}>{p.promiseStatus}</TruncateText>
        </Badge>
      ),
      isFixedRight: true,
      isSort: true,
      width: 102
    }
  ];

  return (
    <Grid columns={columns} data={gridData} id={testId} dataTestId={testId} />
  );
};
