import React, { useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { View } from 'app/_libraries/_dof/core';
import {
  Button,
  GroupButton,
  Icon,
  Tooltip
} from 'app/_libraries/_dls/components';
import { PromiseDetail } from './PromiseDetail';
import { useDispatch } from 'react-redux';
import { promiseToPayHistoryActions } from './_redux/reducers';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import {
  NonRecurringPromiseViewData,
  RecurringPromiseViewData,
  SinglePromiseViewData
} from './types';
import {
  selectPromiseToPayHistoryNonRecurring,
  selectPromiseToPayHistoryRecurring,
  selectPromiseToPayHistorySingle
} from './_redux/selector';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { genAmtId } from 'app/_libraries/_dls/utils';

export type CurrentView = 'single' | 'recurring' | 'nonRecurring' | '';

const GROUP_BUTTON_DATA: { name: string; id: CurrentView }[] = [
  { name: 'txt_single', id: 'single' },
  { name: 'txt_recurring', id: 'recurring' },
  { name: 'txt_custom', id: 'nonRecurring' }
];

export const PromiseToPayHistory: React.FC = () => {
  const { t } = useTranslation();
  const { storeId, accEValue } = useAccountDetail();
  const dispatch = useDispatch();
  const testId = 'infoBar_collectionInfo_promiseToPayHistory';

  const singlePromise = useStoreIdSelector<SinglePromiseViewData>(
    selectPromiseToPayHistorySingle
  );
  const recurringPromise = useStoreIdSelector<RecurringPromiseViewData>(
    selectPromiseToPayHistoryRecurring
  );
  const nonRecurringPromise = useStoreIdSelector<NonRecurringPromiseViewData>(
    selectPromiseToPayHistoryNonRecurring
  );

  const activePromiseIndex = useMemo(
    () =>
      [singlePromise, recurringPromise, nonRecurringPromise].map(item =>
        Boolean(!isEmpty(item))
      ),
    [nonRecurringPromise, recurringPromise, singlePromise]
  );

  const groupButtonDataFiltered = useMemo(
    () => GROUP_BUTTON_DATA.filter((_, index) => activePromiseIndex[index]),
    [activePromiseIndex]
  );

  const [currentView, setCurrentView] = useState<CurrentView>('');
  const [opened, setOpened] = useState<boolean>(false);

  const handleCloseModal = () => {
    setOpened(false);
  };

  const renderHeaderText = (() => {
    switch (currentView) {
      case 'single':
        return t('txt_single_promise');
      case 'recurring':
        return t('txt_recurring_promises');
      case 'nonRecurring':
        return t('txt_custom_promises');
      default:
        return '';
    }
  })();

  useEffect(() => {
    dispatch(
      promiseToPayHistoryActions.getPromiseToPayHistory({
        storeId,
        postData: { accountId: accEValue }
      })
    );
  }, [accEValue, dispatch, storeId]);

  useEffect(() => {
    setCurrentView(groupButtonDataFiltered[0]?.id ?? '');
  }, [groupButtonDataFiltered]);

  if (!groupButtonDataFiltered.length)
    return (
      <>
        <h5 data-testid={genAmtId(testId, 'title', '')}>
          {t('txt_most_recent_promises')}
        </h5>
        <p
          className="color-grey mt-16"
          data-testid={genAmtId(testId, 'no-data-content', '')}
        >
          {t('txt_no_data')}
        </p>
      </>
    );

  return (
    <>
      <PromiseDetail
        opened={opened}
        onClose={handleCloseModal}
        type={currentView}
      />

      <h5 data-testid={genAmtId(testId, 'title', '')}>
        {t('txt_most_recent_promises')}
      </h5>
      {groupButtonDataFiltered.length > 1 && (
        <GroupButton className="mt-16" dataTestId={`${testId}_filter`}>
          {groupButtonDataFiltered.map(item => {
            return (
              <Button
                className={`${item}`}
                key={item.id}
                size="sm"
                selected={item.id === currentView}
                onClick={() => setCurrentView(item.id)}
                variant="secondary"
                dataTestId={`${item.id}_${testId}_filterItem`}
              >
                {t(item.name)}
              </Button>
            );
          })}
        </GroupButton>
      )}
      <div className="my-16 d-flex justify-content-between align-items-center">
        <p className="fw-500" data-testid={genAmtId(testId, 'helpText', '')}>
          {renderHeaderText}
        </p>
        {currentView !== 'single' && (
          <Tooltip
            variant="primary"
            element={t('txt_view_details')}
            placement="top"
          >
            <Button
              onClick={() => setOpened(true)}
              variant="icon-secondary"
              id={`${testId}-open-detail-promise-history`}
              dataTestId={`${testId}_viewDetails`}
            >
              <Icon name="file"></Icon>
            </Button>
          </Tooltip>
        )}
      </div>

      <div>
        {currentView === 'single' && (
          <View
            formKey={`${storeId}-promiseToPayHistorySingle`}
            descriptor="ptpHistorySingle"
            value={singlePromise}
            id={`${testId}-single-promise-history`}
          />
        )}
        {currentView === 'recurring' && (
          <View
            formKey={`${storeId}-promiseToPayHistoryRecurring`}
            descriptor="ptpHistoryRecurring"
            value={recurringPromise}
            id={`${testId}-recurring-promise-history`}
          />
        )}
        {currentView === 'nonRecurring' && (
          <View
            formKey={`${storeId}-promiseToPayHistoryNonRecurring`}
            descriptor="ptpHistoryNonRecurring"
            value={nonRecurringPromise}
            id={`${testId}-nonRecurring-promise-history`}
          />
        )}
      </div>
    </>
  );
};
