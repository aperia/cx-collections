import React from 'react';

import { PromiseDetailGrid } from './PromiseDetailGrid';
import { mockUseTranslate, renderMockStoreId } from 'app/test-utils';

mockUseTranslate();

const mDataGrid = [
  {
    lastPaymentDate: '',
    paymentMethod: 'CHECK',
    promiseAmount: '10',
    promiseAmountPaid: '0',
    promiseDueDate: '2021-07-10',
    promiseId: '1',
    promiseStatus: 'Kept'
  },
  {
    lastPaymentDate: '',
    paymentMethod: 'ACH',
    promiseAmount: '15',
    promiseAmountPaid: '0',
    promiseDueDate: '2021-07-10',
    promiseId: '2',
    promiseStatus: 'Broken'
  }
];

describe('test PromiseDetailGrid', () => {
  it('should be render', () => {
    const { wrapper } = renderMockStoreId(
      <PromiseDetailGrid gridData={mDataGrid as never} />
    );
    expect(wrapper.getByText('Kept')).toBeInTheDocument();
  });
});
