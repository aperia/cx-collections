import sortBy from 'lodash.sortby';
import { INTERVAL_OPTIONS } from 'pages/PromiseToPay/constants';
import {
  NonRecurringPromisePayload,
  RecurringPromisePayload,
  SinglePromisePayload
} from './types';

const normalizeStatus = (status: string) => {
  if (!status) return '';
  if (status.toLowerCase().includes('kept')) return 'Kept';
  if (status.toLowerCase().includes('broken')) return 'Broken';
  return 'Pending';
};

export const generateSinglePromiseHistory = (
  data: SinglePromisePayload,
  methodRefData: RefDataValue[],
  latestSinglePromiseDetailStatus?: string
) => {
  if (!data || !data.singlePromiseData) return {};
  const { singlePromiseData } = data;
  const status = (() => {
    if (latestSinglePromiseDetailStatus === 'ACTIVE') return 'Pending';
    return normalizeStatus(singlePromiseData.promiseStatus);
  })();
  const {
    promiseDueDate,
    promiseAmount,
    paymentMethod,
    lastPaymentDate,
    lastPaymentAmount
  } = singlePromiseData;
  return {
    promiseStatus: status,
    promiseDueDate,
    promiseAmount,
    paymentMethod: methodRefData
      ? methodRefData.find(refData => refData.value === paymentMethod)
      : undefined,
    lastPaymentDate,
    lastPaymentAmount
  };
};

export const generateRecurringPromiseHistory = (
  data: RecurringPromisePayload,
  methodRefData: RefDataValue[]
) => {
  if (!data || !data.recurringPromiseDataList) return {};
  const promiseData = data.recurringPromiseDataList.map(item => ({
    ...item,
    paymentMethod: methodRefData
      ? methodRefData.find(refData => refData.value === item.paymentMethod)
      : undefined,
    promiseStatus: normalizeStatus(item.promiseStatus)
  }));

  const promiseDataSorted = sortBy(promiseData, o => o.promiseId);

  const keptPromise = promiseDataSorted.filter(
    item => item.promiseStatus === 'Kept'
  );
  const brokenPromise = promiseDataSorted.filter(
    item => item.promiseStatus === 'Broken'
  );
  const pendingPromise = promiseDataSorted.filter(
    item => item.promiseStatus !== 'Kept' && item.promiseStatus !== 'Broken'
  );

  const recurring = {
    promiseStatus:
      promiseDataSorted[promiseDataSorted.length - 1].promiseStatus,
    promiseCount: promiseDataSorted.length.toString(),
    recurringInterval: INTERVAL_OPTIONS.find(
      item => item.value === data.recurringInterval
    ),
    countInterval: `${promiseDataSorted.length.toString()} • ${
      INTERVAL_OPTIONS.find(item => item.value === data.recurringInterval)
        ?.description
    }`,
    totalPromiseAmount: data.totalPromiseAmount,
    lastPaymentDate: data.lastPaymentDate,
    lastPaymentAmount: data.lastPaymentAmount,
    keptPromiseCount: keptPromise.length.toString(),
    keptPromiseAmount: keptPromise
      .reduce((prev, item) => prev + +item.promiseAmount, 0)
      .toString(),
    brokenPromiseCount: brokenPromise.length.toString(),
    brokenPromiseAmount: brokenPromise
      .reduce((prev, item) => prev + +item.promiseAmount, 0)
      .toString(),
    pendingPromiseCount: pendingPromise.length.toString(),
    pendingPromiseAmount: pendingPromise
      .reduce((prev, item) => prev + +item.promiseAmount, 0)
      .toString(),
    totalPromiseCount: promiseDataSorted.length.toString()
  };
  return { recurring, recurringGrid: promiseDataSorted };
};

export const generateNonRecurringPromiseHistory = (
  data: NonRecurringPromisePayload,
  methodRefData: RefDataValue[]
) => {
  if (!data || !data.nonRecurringPromiseDataList) return {};
  const promiseData = data.nonRecurringPromiseDataList.map(item => ({
    ...item,
    promiseStatus: normalizeStatus(item.promiseStatus),
    paymentMethod: methodRefData
      ? methodRefData.find(refData => refData.value === item.paymentMethod)
      : undefined
  }));

  const promiseDataSorted = sortBy(promiseData, o => o.promiseId);

  const keptPromise = promiseDataSorted.filter(
    item => item.promiseStatus === 'Kept'
  );
  const brokenPromise = promiseDataSorted.filter(
    item => item.promiseStatus === 'Broken'
  );
  const pendingPromise = promiseDataSorted.filter(
    item => item.promiseStatus !== 'Kept' && item.promiseStatus !== 'Broken'
  );

  const nonRecurring = {
    promiseStatus:
      promiseDataSorted[promiseDataSorted.length - 1].promiseStatus,
    promiseCount: promiseDataSorted.length.toString(),
    totalPromiseAmount: data.totalPromiseAmount,
    lastPaymentDate: data.lastPaymentDate,
    lastPaymentAmount: data.lastPaymentAmount,
    keptPromiseCount: keptPromise.length.toString(),
    keptPromiseAmount: keptPromise
      .reduce((prev, item) => prev + +item.promiseAmount, 0)
      .toString(),
    brokenPromiseCount: brokenPromise.length.toString(),
    brokenPromiseAmount: brokenPromise
      .reduce((prev, item) => prev + +item.promiseAmount, 0)
      .toString(),
    pendingPromiseCount: pendingPromise.length.toString(),
    pendingPromiseAmount: pendingPromise
      .reduce((prev, item) => prev + +item.promiseAmount, 0)
      .toString(),
    totalPromiseCount: promiseDataSorted.length.toString()
  };
  return { nonRecurring, nonRecurringGrid: promiseDataSorted };
};
