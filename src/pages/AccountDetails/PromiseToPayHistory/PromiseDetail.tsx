import React from 'react';
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { View } from 'app/_libraries/_dof/core';

import { useTranslation } from 'app/_libraries/_dls/hooks';
import { CurrentView } from '.';
import { PromiseDetailGrid } from './PromiseDetailGrid';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import {
  NonRecurringPromiseViewData,
  PromiseDetailGridItem,
  RecurringPromiseViewData
} from './types';
import {
  selectPromiseToPayHistoryNonRecurring,
  selectPromiseToPayHistoryNonRecurringGrid,
  selectPromiseToPayHistoryRecurring,
  selectPromiseToPayHistoryRecurringGrid
} from './_redux/selector';
import { formatCommon } from 'app/helpers';
import classNames from 'classnames';
import { genAmtId } from 'app/_libraries/_dls/utils';

interface PromiseDetailProps {
  opened: boolean;
  onClose: () => void;
  type: CurrentView;
}

const RENDER_DATA_INFO = {
  ['recurring' as CurrentView]: {
    descriptor: 'ptpHistoryRecurringSnapshot',
    text: 'txt_recurring_promises'
  },
  ['nonRecurring' as CurrentView]: {
    descriptor: 'ptpHistoryNonRecurringSnapshot',
    text: 'txt_custom_promises'
  }
};

export const InlineField = ({
  label,
  count,
  amount,
  className,
  dataTestId
}: {
  label: string;
  count?: string;
  amount?: string;
  className?: string;
  dataTestId?: string;
}) => {
  const genId = genAmtId(dataTestId, 'field', 'PromiseDetail.InlineField');
  return (
    <div
      className={classNames('col-md-3', className)}
      data-testid={genId}
      id={genId}
    >
      <span
        id={`${genId}-label`}
        className="fw-500 text-capitalize"
      >{`${label.toLowerCase()}:`}</span>
      <span id={`${genId}-count-data`} className="ml-8">
        {count ?? ''}
      </span>
      <span className="color-light-l12 mx-8">|</span>
      <span id={`${genId}-amount-data`}>
        {formatCommon(amount ?? '').currency(2)}
      </span>
    </div>
  );
};

export const PromiseDetail: React.FC<PromiseDetailProps> = ({
  opened,
  onClose,
  type
}) => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const testId = 'infoBar_collectionInfo_promiseToPayDetail';

  const recurringPromise = useStoreIdSelector<RecurringPromiseViewData>(
    selectPromiseToPayHistoryRecurring
  );
  const nonRecurringPromise = useStoreIdSelector<NonRecurringPromiseViewData>(
    selectPromiseToPayHistoryNonRecurring
  );

  const recurringGrid = useStoreIdSelector<PromiseDetailGridItem[]>(
    selectPromiseToPayHistoryRecurringGrid
  );
  const nonRecurringGrid = useStoreIdSelector<PromiseDetailGridItem[]>(
    selectPromiseToPayHistoryNonRecurringGrid
  );

  const snapshotData =
    type === 'recurring'
      ? { type: t('txt_recurring'), ...recurringPromise }
      : { type: t('txt_custom'), ...nonRecurringPromise };

  return (
    <Modal lg show={opened} dataTestId={testId}>
      <ModalHeader
        border
        closeButton
        onHide={onClose}
        dataTestId={`${testId}_header`}
      >
        <ModalTitle dataTestId={`${testId}_title`}>
          {t('txt_promise_details')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody className="p-0" dataTestId={`${testId}_body`}>
        {!!RENDER_DATA_INFO[type] && (
          <>
            <div className="p-24 border-bottom bg-light-l20">
              <View
                formKey={`${storeId}-${type}-promiseDetail`}
                descriptor={RENDER_DATA_INFO[type].descriptor}
                value={snapshotData}
                id={`${testId}-promise-details`}
              />
            </div>
            <div className="px-24 pt-24">
              <h5 data-testid={genAmtId(testId, 'description', '')}>
                {t(RENDER_DATA_INFO[type].text)}
              </h5>
              <div className="row mt-16">
                <InlineField
                  label={t('txt_kept')}
                  count={snapshotData.keptPromiseCount}
                  amount={snapshotData.keptPromiseAmount}
                  dataTestId={`${testId}_kept`}
                />
                <InlineField
                  label={t('txt_broken')}
                  count={snapshotData.brokenPromiseCount}
                  amount={snapshotData.brokenPromiseAmount}
                  dataTestId={`${testId}_broken`}
                />
                <InlineField
                  label={t('txt_pending')}
                  count={snapshotData.pendingPromiseCount}
                  amount={snapshotData.pendingPromiseAmount}
                  dataTestId={`${testId}_pending`}
                />
                <InlineField
                  label={t('txt_total')}
                  count={snapshotData.totalPromiseCount}
                  amount={snapshotData.totalPromiseAmount}
                  dataTestId={`${testId}_total`}
                />
              </div>
              <div className="mt-16">
                <PromiseDetailGrid
                  gridData={
                    type === 'recurring' ? recurringGrid : nonRecurringGrid
                  }
                />
              </div>
            </div>
          </>
        )}
      </ModalBody>
      <ModalFooter dataTestId={`${testId}_footer`}>
        <Button
          variant="secondary"
          onClick={onClose}
          dataTestId={`${testId}_close-btn`}
        >
          {t('txt_close')}
        </Button>
      </ModalFooter>
    </Modal>
  );
};
