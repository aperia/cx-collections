import { createStore, Store } from '@reduxjs/toolkit';

import { getPromiseToPayHistory } from './getPromiseToPayHistory';

import { accEValue, responseDefault, storeId } from 'app/test-utils';

import { rootReducer } from 'storeConfig';
import promiseToPayService from 'pages/PromiseToPay/promiseToPayService';

let store: Store<RootState>;
let state: RootState;

const mMethod = [
  { value: 'ACH', description: 'ACH' },
  { value: 'CHECK', description: 'Check' },
  { value: 'DEBIT', description: 'Debit Card' }
];

const mReturnData = {
  status: 'success',
  latestSinglePromiseDetailStatus: 'INACTIVE',
  latestRecurringPromiseDetailStatus: 'INACTIVE',
  latestNonRecurringPromiseDetailStatus: 'ACTIVE',
  latestSinglePtpDetail: {
    common: {},
    id: '1',
    active: false,
    dateTimeCreated: '2021-07-10T00:32:36.488+0000',
    singlePromiseData: {
      promiseId: '1',
      promiseDueDate: '2021-07-10',
      promiseAmount: '20',
      promiseStatus: 'Promise Kept',
      promiseAmountPaid: '25.0',
      paymentMethod: 'DEBIT',
      lastPaymentDate: '2021-07-10',
      lastPaymentAmount: '25'
    }
  },
  latestRecurringPtpDetail: {
    common: {},
    id: '1',
    active: false,
    dateTimeCreated: '2021-07-10T00:34:19.062+0000',
    recurringInterval: 'WEEKLY',
    totalPromiseAmount: '50.0',
    totalPromiseAmountPaid: '50.0',
    lastPaymentAmount: '50',
    lastPaymentDate: '2021-07-10',
    recurringPromiseDataList: [
      {
        promiseId: '2',
        promiseDueDate: '2021-07-17',
        promiseAmount: '25',
        promiseStatus: 'Promise Kept',
        promiseAmountPaid: '0',
        paymentMethod: 'ACH',
        lastPaymentDate: ''
      },
      {
        promiseId: '1',
        promiseDueDate: '2021-07-10',
        promiseAmount: '25',
        promiseStatus: 'Promise Kept',
        promiseAmountPaid: '50.0',
        paymentMethod: 'ACH',
        lastPaymentDate: '2021-07-10',
        lastPaymentAmount: '50'
      }
    ]
  },
  latestNonRecurringPtpDetail: {
    common: {},
    id: '1',
    active: true,
    dateTimeCreated: '2021-07-10T00:46:40.279+0000',
    totalPromiseAmount: '25',
    totalPromiseAmountPaid: '0',
    lastPaymentAmount: '50',
    lastPaymentDate: '2021-07-10',
    nonRecurringPromiseDataList: [
      {
        promiseId: '2',
        promiseDueDate: '2021-07-10',
        promiseAmount: '15',
        promiseStatus: 'Future Promise',
        promiseAmountPaid: '0',
        paymentMethod: 'ACH',
        lastPaymentDate: ''
      },
      {
        promiseId: '1',
        promiseDueDate: '2021-07-10',
        promiseAmount: '10',
        promiseStatus: 'Waiting for Host Update',
        promiseAmountPaid: '0',
        paymentMethod: 'CHECK',
        lastPaymentDate: ''
      }
    ]
  }
};

let spy1: jest.SpyInstance;
let spy2: jest.SpyInstance;

afterEach(() => {
  spy1?.mockReset();
  spy1?.mockRestore();
  spy2?.mockReset();
  spy2?.mockRestore();
});

describe('getPromiseToPayHistory', () => {
  it('Should return data and has no mapping', async () => {
    store = createStore(rootReducer, {});
    spy1 = jest
      .spyOn(promiseToPayService, 'getPromiseToPayMethod')
      .mockResolvedValue({
        ...responseDefault,
        data: mMethod
      });

    spy2 = jest
      .spyOn(promiseToPayService, 'getPromiseToPayData')
      .mockResolvedValue({
        ...responseDefault,
        data: mReturnData as never
      });

    const response = await getPromiseToPayHistory({
      postData: { accountId: accEValue },
      storeId
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({
      nonRecurring: undefined,
      nonRecurringGrid: undefined,
      recurring: undefined,
      recurringGrid: undefined,
      single: {}
    });
  });

  it('Should return data with has cardHolderSecondaryName', async () => {
    store = createStore(rootReducer, {
      accountDetail: {
        [storeId]: {
          data: { cardHolderSecondaryName: 'John' }
        }
      },
      mapping: {
        data: {
          promiseToPay: {
            promiseAmount: 'promiseToPayAmount',
            promiseDueDate: 'promiseToPayDate',
            latestSinglePromiseDetailStatus: 'latestSinglePromiseDetailStatus',
            latestRecurringPromiseDetailStatus:
              'latestRecurringPromiseDetailStatus',
            latestNonRecurringPromiseDetailStatus:
              'latestNonRecurringPromiseDetailStatus',
            latestSinglePtpDetail: 'latestSinglePtpDetail',
            latestRecurringPtpDetail: 'latestRecurringPtpDetail',
            latestNonRecurringPtpDetail: 'latestNonRecurringPtpDetail'
          }
        }
      }
    });
    spy1 = jest
      .spyOn(promiseToPayService, 'getPromiseToPayMethod')
      .mockResolvedValue({
        ...responseDefault,
        data: mMethod
      });

    spy2 = jest
      .spyOn(promiseToPayService, 'getPromiseToPayData')
      .mockResolvedValue({
        ...responseDefault,
        data: mReturnData as never
      });

    const response = await getPromiseToPayHistory({
      postData: { accountId: accEValue },
      storeId
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({
      nonRecurring: {
        brokenPromiseAmount: '0',
        brokenPromiseCount: '0',
        keptPromiseAmount: '0',
        keptPromiseCount: '0',
        lastPaymentAmount: '50',
        lastPaymentDate: '2021-07-10',
        pendingPromiseAmount: '25',
        pendingPromiseCount: '2',
        promiseCount: '2',
        promiseStatus: 'Pending',
        totalPromiseAmount: '25',
        totalPromiseCount: '2'
      },
      nonRecurringGrid: [
        {
          lastPaymentDate: '',
          paymentMethod: {
            description: 'Check',
            value: 'CHECK'
          },
          promiseAmount: '10',
          promiseAmountPaid: '0',
          promiseDueDate: '2021-07-10',
          promiseId: '1',
          promiseStatus: 'Pending'
        },
        {
          lastPaymentDate: '',
          paymentMethod: {
            description: 'ACH',
            value: 'ACH'
          },
          promiseAmount: '15',
          promiseAmountPaid: '0',
          promiseDueDate: '2021-07-10',
          promiseId: '2',
          promiseStatus: 'Pending'
        }
      ],
      recurring: {
        brokenPromiseAmount: '0',
        brokenPromiseCount: '0',
        countInterval: '2 • Weekly',
        keptPromiseAmount: '50',
        keptPromiseCount: '2',
        lastPaymentAmount: '50',
        lastPaymentDate: '2021-07-10',
        pendingPromiseAmount: '0',
        pendingPromiseCount: '0',
        promiseCount: '2',
        promiseStatus: 'Kept',
        recurringInterval: {
          description: 'Weekly',
          value: 'WEEKLY'
        },
        totalPromiseAmount: '50.0',
        totalPromiseCount: '2'
      },
      recurringGrid: [
        {
          lastPaymentAmount: '50',
          lastPaymentDate: '2021-07-10',
          paymentMethod: {
            description: 'ACH',
            value: 'ACH'
          },
          promiseAmount: '25',
          promiseAmountPaid: '50.0',
          promiseDueDate: '2021-07-10',
          promiseId: '1',
          promiseStatus: 'Kept'
        },
        {
          lastPaymentDate: '',
          paymentMethod: {
            description: 'ACH',
            value: 'ACH'
          },
          promiseAmount: '25',
          promiseAmountPaid: '0',
          promiseDueDate: '2021-07-17',
          promiseId: '2',
          promiseStatus: 'Kept'
        }
      ],
      single: {
        lastPaymentAmount: '25',
        lastPaymentDate: '2021-07-10',
        paymentMethod: {
          description: 'Debit Card',
          value: 'DEBIT'
        },
        promiseAmount: '20',
        promiseDueDate: '2021-07-10',
        promiseStatus: 'Kept'
      }
    });
  });

  it('should response getPromiseToPayHistory rejected', () => {
    store = createStore(rootReducer, {
      promiseToPayHistory: { [storeId]: { loading: true } }
    });
    state = store.getState();
    const rejected = getPromiseToPayHistory.rejected(
      null,
      getPromiseToPayHistory.rejected.type,
      {
        postData: { accountId: accEValue },
        storeId
      }
    );
    const actual = rootReducer(state, rejected);

    expect(actual?.promiseToPayHistory[storeId]?.loading).toEqual(false);
  });
});
