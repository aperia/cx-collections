import { createSelector } from '@reduxjs/toolkit';

const getPromiseToPayHistoryLoading = (state: RootState, storeId: string) => {
  return state.promiseToPayHistory[storeId]?.loading ?? false;
};

export const selectPromiseToPayHistoryLoading = createSelector(
  getPromiseToPayHistoryLoading,
  data => data
);

const getPromiseToPayHistorySingle = (state: RootState, storeId: string) => {
  return state.promiseToPayHistory[storeId]?.single;
};

export const selectPromiseToPayHistorySingle = createSelector(
  getPromiseToPayHistorySingle,
  data => data
);

const getPromiseToPayHistoryRecurring = (state: RootState, storeId: string) => {
  return state.promiseToPayHistory[storeId]?.recurring;
};

export const selectPromiseToPayHistoryRecurring = createSelector(
  getPromiseToPayHistoryRecurring,
  data => data
);

const getPromiseToPayHistoryNonRecurring = (
  state: RootState,
  storeId: string
) => {
  return state.promiseToPayHistory[storeId]?.nonRecurring;
};

export const selectPromiseToPayHistoryNonRecurring = createSelector(
  getPromiseToPayHistoryNonRecurring,
  data => data
);

const getPromiseToPayHistoryRecurringGrid = (
  state: RootState,
  storeId: string
) => {
  return state.promiseToPayHistory[storeId]?.recurringGrid;
};

export const selectPromiseToPayHistoryRecurringGrid = createSelector(
  getPromiseToPayHistoryRecurringGrid,
  data => data
);

const getPromiseToPayHistoryNonRecurringGrid = (
  state: RootState,
  storeId: string
) => {
  return state.promiseToPayHistory[storeId]?.nonRecurringGrid;
};

export const selectPromiseToPayHistoryNonRecurringGrid = createSelector(
  getPromiseToPayHistoryNonRecurringGrid,
  data => data
);
