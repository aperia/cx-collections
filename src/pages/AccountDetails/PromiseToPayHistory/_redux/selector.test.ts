import { selectorWrapper, storeId } from 'app/test-utils';
import {
  selectPromiseToPayHistoryLoading,
  selectPromiseToPayHistoryNonRecurring,
  selectPromiseToPayHistoryNonRecurringGrid,
  selectPromiseToPayHistoryRecurring,
  selectPromiseToPayHistoryRecurringGrid,
  selectPromiseToPayHistorySingle
} from './selector';

const mState = {
  loading: false,
  single: { promiseStatus: 'Kept' },
  recurring: { promiseStatus: 'Kept' },
  nonRecurring: { promiseStatus: 'Kept' },
  recurringGrid: [{ promiseStatus: 'Kept' }],
  nonRecurringGrid: [{ promiseStatus: 'Kept' }]
};

describe('Test Promise to Pay history Selectors', () => {
  const store: Partial<RootState> = {
    promiseToPayHistory: {
      [storeId]: mState as never
    }
  };

  it('selectPromiseToPayHistoryLoading', () => {
    const { data } = selectorWrapper(store)(selectPromiseToPayHistoryLoading);
    expect(data).toEqual(mState.loading);
  });

  it('selectPromiseToPayHistorySingle', () => {
    const { data } = selectorWrapper(store)(selectPromiseToPayHistorySingle);
    expect(data).toEqual(mState.single);
  });

  it('selectPromiseToPayHistoryRecurring', () => {
    const { data } = selectorWrapper(store)(selectPromiseToPayHistoryRecurring);
    expect(data).toEqual(mState.recurring);
  });

  it('selectPromiseToPayHistoryNonRecurring', () => {
    const { data } = selectorWrapper(store)(
      selectPromiseToPayHistoryNonRecurring
    );
    expect(data).toEqual(mState.nonRecurring);
  });

  it('selectPromiseToPayHistoryRecurringGrid', () => {
    const { data } = selectorWrapper(store)(
      selectPromiseToPayHistoryRecurringGrid
    );
    expect(data).toEqual(mState.recurringGrid);
  });

  it('selectPromiseToPayHistoryNonRecurringGrid', () => {
    const { data } = selectorWrapper(store)(
      selectPromiseToPayHistoryNonRecurringGrid
    );
    expect(data).toEqual(mState.nonRecurringGrid);
  });
});
