import { createSlice } from '@reduxjs/toolkit';
import {
  getPromiseToPayHistory,
  getPromiseToPayHistoryBuilder
} from './getPromiseToPayHistory';
import { PromiseToPayHistoryState } from '../types';

const initialState: PromiseToPayHistoryState = {};

export const { actions, reducer } = createSlice({
  initialState,
  name: 'promiseToPayHistory',
  reducers: {},
  extraReducers: builder => {
    getPromiseToPayHistoryBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getPromiseToPayHistory
};

export {
  allActions as promiseToPayHistoryActions,
  reducer as promiseToPayHistory
};
