import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { EMPTY_STRING } from 'app/constants';
import { mappingDataFromObj } from 'app/helpers';
import promiseToPayService from 'pages/PromiseToPay/promiseToPayService';
import { PromiseToPayDetailsResponse } from 'pages/PromiseToPay/types';
import {
  generateNonRecurringPromiseHistory,
  generateRecurringPromiseHistory,
  generateSinglePromiseHistory
} from '../helpers';
import {
  GetPromiseToPayHistoryArgs,
  GetPromiseToPayHistoryPayload,
  PromiseToPayHistoryState
} from '../types';

export const getPromiseToPayHistory = createAsyncThunk<
  GetPromiseToPayHistoryPayload,
  GetPromiseToPayHistoryArgs,
  ThunkAPIConfig
>('promiseToPayHistory/getPromiseToPayHistory', async (args, thunkAPI) => {
  const {
    postData: { accountId },
    storeId
  } = args;
  const { commonConfig } = window.appConfig || {};
  const { accountDetail, mapping } = thunkAPI.getState();
  const selectedCaller = accountDetail[storeId]?.selectedCaller;

  const requestBody = {
    common: {
      org: commonConfig?.org || EMPTY_STRING,
      app: commonConfig?.app || EMPTY_STRING,
      accountId
    },
    callingApplication: commonConfig?.callingApplication || EMPTY_STRING,
    operatorCode: commonConfig?.operatorID || EMPTY_STRING,
    cardholderContactedName: selectedCaller?.customerName ?? ''
  };
  const { data: methodRefData } =
    await promiseToPayService.getPromiseToPayMethod();

  const { data } = await promiseToPayService.getPromiseToPayData(requestBody);
  const mappedData = mappingDataFromObj<PromiseToPayDetailsResponse>(
    data,
    mapping?.data?.promiseToPay ?? {}
  );
  const single = generateSinglePromiseHistory(
    mappedData.latestSinglePtpDetail,
    methodRefData,
    mappedData.latestSinglePromiseDetailStatus
  );

  const { recurring, recurringGrid } = generateRecurringPromiseHistory(
    mappedData.latestRecurringPtpDetail,
    methodRefData
  );

  const { nonRecurring, nonRecurringGrid } = generateNonRecurringPromiseHistory(
    mappedData.latestNonRecurringPtpDetail,
    methodRefData
  );
  return {
    single,
    recurring,
    nonRecurring,
    recurringGrid,
    nonRecurringGrid
  };
});

export const getPromiseToPayHistoryBuilder = (
  builder: ActionReducerMapBuilder<PromiseToPayHistoryState>
) => {
  builder
    .addCase(getPromiseToPayHistory.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: true
      };
    })
    .addCase(getPromiseToPayHistory.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const {
        single,
        recurring,
        nonRecurring,
        recurringGrid,
        nonRecurringGrid
      } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: false,
        single,
        recurring,
        nonRecurring,
        recurringGrid,
        nonRecurringGrid
      };
    })
    .addCase(getPromiseToPayHistory.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId].loading = false;
    });
};
