import {
  generateSinglePromiseHistory,
  generateRecurringPromiseHistory,
  generateNonRecurringPromiseHistory
} from './helpers';

const mMethodRefData = [
  { value: 'ACH', description: 'ACH' },
  { value: 'CHECK', description: 'Check' },
  { value: 'DEBIT', description: 'Debit Card' }
];

describe('test generateSinglePromiseHistory', () => {
  it('should be return empty object when data arg is undefined', () => {
    const result = generateSinglePromiseHistory(
      {
        common: {},
        id: '1',
        active: true
      } as never,
      mMethodRefData,
      'ACTIVE'
    );
    expect(result).toEqual({});
  });

  it('should be return result with status equal pending', () => {
    const result = generateSinglePromiseHistory(
      {
        common: {},
        id: '1',
        active: true,
        singlePromiseData: {
          promiseId: '1',
          promiseDueDate: '2021-07-10',
          promiseAmount: '200',
          promiseStatus: 'Waiting for Host Update',
          promiseAmountPaid: '0',
          paymentMethod: 'DEBIT',
          lastPaymentDate: '',
          lastPaymentAmount: ''
        }
      },
      mMethodRefData,
      'ACTIVE'
    );
    expect(result).toEqual({
      lastPaymentAmount: '',
      lastPaymentDate: '',
      paymentMethod: {
        description: 'Debit Card',
        value: 'DEBIT'
      },
      promiseAmount: '200',
      promiseDueDate: '2021-07-10',
      promiseStatus: 'Pending'
    });
  });

  it('should be return result with status not equal pending', () => {
    const result = generateSinglePromiseHistory(
      {
        common: {},
        id: '1',
        active: true,
        singlePromiseData: {
          promiseId: '1',
          promiseDueDate: '2021-07-10',
          promiseAmount: '200',
          promiseStatus: 'Kept Promise',
          promiseAmountPaid: '0',
          paymentMethod: 'DEBIT',
          lastPaymentDate: '',
          lastPaymentAmount: ''
        }
      },
      undefined as never,
      'INACTIVE'
    );
    expect(result).toEqual({
      lastPaymentAmount: '',
      lastPaymentDate: '',
      paymentMethod: undefined,
      promiseAmount: '200',
      promiseDueDate: '2021-07-10',
      promiseStatus: 'Kept'
    });
  });
});

describe('test generateRecurringPromiseHistory', () => {
  it('should be return empty object when data arg is undefined', () => {
    const result = generateRecurringPromiseHistory(
      {
        common: {},
        id: '1',
        active: true
      } as never,
      mMethodRefData
    );
    expect(result).toEqual({});
  });

  it('should be return result', () => {
    const result = generateRecurringPromiseHistory(
      {
        common: {},
        id: '1',
        active: 'true',
        recurringInterval: 'WEEKLY',
        totalPromiseAmount: '50.0',
        totalPromiseAmountPaid: '0.0',
        lastPaymentAmount: '25',
        lastPaymentDate: '2021-07-17',
        recurringPromiseDataList: [
          {
            promiseId: '1',
            promiseDueDate: '2021-07-10',
            promiseAmount: '25',
            promiseStatus: 'Active Promise',
            promiseAmountPaid: '0',
            paymentMethod: 'ACH',
            lastPaymentDate: ''
          },
          {
            promiseId: '2',
            promiseDueDate: '2021-07-17',
            promiseAmount: '25',
            promiseStatus: 'Future Promise',
            promiseAmountPaid: '0',
            paymentMethod: 'ACH',
            lastPaymentDate: ''
          }
        ]
      },
      mMethodRefData
    );
    expect(result).toEqual({
      recurring: {
        brokenPromiseAmount: '0',
        brokenPromiseCount: '0',
        countInterval: '2 • Weekly',
        keptPromiseAmount: '0',
        keptPromiseCount: '0',
        lastPaymentAmount: '25',
        lastPaymentDate: '2021-07-17',
        pendingPromiseAmount: '50',
        pendingPromiseCount: '2',
        promiseCount: '2',
        promiseStatus: 'Pending',
        recurringInterval: {
          description: 'Weekly',
          value: 'WEEKLY'
        },
        totalPromiseAmount: '50.0',
        totalPromiseCount: '2'
      },
      recurringGrid: [
        {
          lastPaymentDate: '',
          paymentMethod: {
            description: 'ACH',
            value: 'ACH'
          },
          promiseAmount: '25',
          promiseAmountPaid: '0',
          promiseDueDate: '2021-07-10',
          promiseId: '1',
          promiseStatus: 'Pending'
        },
        {
          lastPaymentDate: '',
          paymentMethod: {
            description: 'ACH',
            value: 'ACH'
          },
          promiseAmount: '25',
          promiseAmountPaid: '0',
          promiseDueDate: '2021-07-17',
          promiseId: '2',
          promiseStatus: 'Pending'
        }
      ]
    });
  });

  it('should be return empty object when data arg is undefined', () => {
    const result = generateRecurringPromiseHistory(
      {
        common: {},
        id: '1',
        active: true
      } as never,
      mMethodRefData
    );
    expect(result).toEqual({});
  });

  it('should be return result with kept and broken exists', () => {
    const result = generateRecurringPromiseHistory(
      {
        common: {},
        id: '1',
        active: 'true',
        recurringInterval: 'WEEKLY',
        totalPromiseAmount: '50.0',
        totalPromiseAmountPaid: '0.0',
        lastPaymentAmount: '25',
        lastPaymentDate: '2021-07-17',
        recurringPromiseDataList: [
          {
            promiseId: '1',
            promiseDueDate: '2021-07-10',
            promiseAmount: '25',
            promiseStatus: 'Kept Promise',
            promiseAmountPaid: '0',
            paymentMethod: 'ACH',
            lastPaymentDate: ''
          },
          {
            promiseId: '2',
            promiseDueDate: '2021-07-17',
            promiseAmount: '25',
            promiseStatus: 'Broken Promise',
            promiseAmountPaid: '0',
            paymentMethod: 'ACH',
            lastPaymentDate: ''
          }
        ]
      },
      undefined as never
    );
    expect(result).toEqual({
      recurring: {
        brokenPromiseAmount: '25',
        brokenPromiseCount: '1',
        countInterval: '2 • Weekly',
        keptPromiseAmount: '25',
        keptPromiseCount: '1',
        lastPaymentAmount: '25',
        lastPaymentDate: '2021-07-17',
        pendingPromiseAmount: '0',
        pendingPromiseCount: '0',
        promiseCount: '2',
        promiseStatus: 'Broken',
        recurringInterval: {
          description: 'Weekly',
          value: 'WEEKLY'
        },
        totalPromiseAmount: '50.0',
        totalPromiseCount: '2'
      },
      recurringGrid: [
        {
          lastPaymentDate: '',
          paymentMethod: undefined,
          promiseAmount: '25',
          promiseAmountPaid: '0',
          promiseDueDate: '2021-07-10',
          promiseId: '1',
          promiseStatus: 'Kept'
        },
        {
          lastPaymentDate: '',
          paymentMethod: undefined,
          promiseAmount: '25',
          promiseAmountPaid: '0',
          promiseDueDate: '2021-07-17',
          promiseId: '2',
          promiseStatus: 'Broken'
        }
      ]
    });
  });
});

describe('test generateNonRecurringPromiseHistory', () => {
  it('should be return empty object when data arg is undefined', () => {
    const result = generateNonRecurringPromiseHistory(
      {
        common: {},
        id: '1',
        active: true
      } as never,
      mMethodRefData
    );
    expect(result).toEqual({});
  });

  it('should be return result', () => {
    const result = generateNonRecurringPromiseHistory(
      {
        common: {},
        id: '1',
        active: 'true',
        totalPromiseAmount: '25',
        totalPromiseAmountPaid: '0',
        lastPaymentAmount: '15',
        lastPaymentDate: '2021-07-10',
        nonRecurringPromiseDataList: [
          {
            promiseId: '2',
            promiseDueDate: '2021-07-10',
            promiseAmount: '15',
            promiseStatus: 'Future Promise',
            promiseAmountPaid: '0',
            paymentMethod: 'ACH',
            lastPaymentDate: ''
          },
          {
            promiseId: '1',
            promiseDueDate: '2021-07-10',
            promiseAmount: '10',
            promiseStatus: '',
            promiseAmountPaid: '0',
            paymentMethod: 'CHECK',
            lastPaymentDate: ''
          }
        ]
      },
      mMethodRefData
    );
    expect(result).toEqual({
      nonRecurring: {
        brokenPromiseAmount: '0',
        brokenPromiseCount: '0',
        keptPromiseAmount: '0',
        keptPromiseCount: '0',
        lastPaymentAmount: '15',
        lastPaymentDate: '2021-07-10',
        pendingPromiseAmount: '25',
        pendingPromiseCount: '2',
        promiseCount: '2',
        promiseStatus: 'Pending',
        totalPromiseAmount: '25',
        totalPromiseCount: '2'
      },
      nonRecurringGrid: [
        {
          lastPaymentDate: '',
          paymentMethod: {
            description: 'Check',
            value: 'CHECK'
          },
          promiseAmount: '10',
          promiseAmountPaid: '0',
          promiseDueDate: '2021-07-10',
          promiseId: '1',
          promiseStatus: ''
        },
        {
          lastPaymentDate: '',
          paymentMethod: {
            description: 'ACH',
            value: 'ACH'
          },
          promiseAmount: '15',
          promiseAmountPaid: '0',
          promiseDueDate: '2021-07-10',
          promiseId: '2',
          promiseStatus: 'Pending'
        }
      ]
    });
  });

  it('should be return empty object when data arg is undefined', () => {
    const result = generateNonRecurringPromiseHistory(
      {
        common: {},
        id: '1',
        active: true
      } as never,
      mMethodRefData
    );
    expect(result).toEqual({});
  });

  it('should be return result with kept and broken exists', () => {
    const result = generateNonRecurringPromiseHistory(
      {
        common: {},
        id: '1',
        active: 'true',
        totalPromiseAmount: '25',
        totalPromiseAmountPaid: '0',
        lastPaymentAmount: '15',
        lastPaymentDate: '2021-07-10',
        nonRecurringPromiseDataList: [
          {
            promiseId: '2',
            promiseDueDate: '2021-07-10',
            promiseAmount: '15',
            promiseStatus: 'Broken Promise',
            promiseAmountPaid: '0',
            paymentMethod: 'ACH',
            lastPaymentDate: '2021-07-10'
          },
          {
            promiseId: '1',
            promiseDueDate: '2021-07-10',
            promiseAmount: '10',
            promiseStatus: 'Kept Promise',
            promiseAmountPaid: '0',
            paymentMethod: 'CHECK',
            lastPaymentDate: '2021-07-10'
          }
        ]
      },
      undefined as never
    );
    expect(result).toEqual({
      nonRecurring: {
        brokenPromiseAmount: '15',
        brokenPromiseCount: '1',
        keptPromiseAmount: '10',
        keptPromiseCount: '1',
        lastPaymentAmount: '15',
        lastPaymentDate: '2021-07-10',
        pendingPromiseAmount: '0',
        pendingPromiseCount: '0',
        promiseCount: '2',
        promiseStatus: 'Broken',
        totalPromiseAmount: '25',
        totalPromiseCount: '2'
      },
      nonRecurringGrid: [
        {
          lastPaymentDate: '2021-07-10',
          paymentMethod: undefined,
          promiseAmount: '10',
          promiseAmountPaid: '0',
          promiseDueDate: '2021-07-10',
          promiseId: '1',
          promiseStatus: 'Kept'
        },
        {
          lastPaymentDate: '2021-07-10',
          paymentMethod: undefined,
          promiseAmount: '15',
          promiseAmountPaid: '0',
          promiseDueDate: '2021-07-10',
          promiseId: '2',
          promiseStatus: 'Broken'
        }
      ]
    });
  });
});
