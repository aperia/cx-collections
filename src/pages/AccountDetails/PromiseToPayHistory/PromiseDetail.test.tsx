import { screen } from '@testing-library/react';
import { renderMockStoreId } from 'app/test-utils';
import React from 'react';
import * as PromiseGrid from './PromiseDetailGrid';

import { PromiseDetail } from './PromiseDetail';

beforeEach(() => {
  jest
    .spyOn(PromiseGrid, 'PromiseDetailGrid')
    .mockImplementation(() => <div>PromiseDetailGrid</div>);
});

describe('Test PromiseDetail component', () => {
  it('should render recurring detail', () => {
    renderMockStoreId(
      <PromiseDetail opened={true} onClose={() => {}} type="recurring" />
    );
    expect(screen.getByText('txt_recurring_promises')).toBeInTheDocument();
  });

  it('should render non recurring detail', () => {
    renderMockStoreId(
      <PromiseDetail opened={true} onClose={() => {}} type="nonRecurring" />
    );
    expect(screen.getByText('txt_custom_promises')).toBeInTheDocument();
  });
});
