import { queryByClass, renderMockStoreId, storeId } from 'app/test-utils';
import React from 'react';
import { screen } from '@testing-library/react';

import { PromiseToPayHistory } from './index';
import * as PromiseDetail from './PromiseDetail';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

beforeEach(() => {
  jest
    .spyOn(PromiseDetail, 'PromiseDetail')
    .mockImplementation(({ onClose }) => {
      return (
        <button onClick={onClose} data-testid="onClosePromiseDetail"></button>
      );
    });
});

describe('Test PromiseToPayHistory Component', () => {
  it('should render with no data', () => {
    renderMockStoreId(<PromiseToPayHistory />);
    expect(screen.getByText('txt_no_data')).toBeInTheDocument();
  });
  it('should render single Promise', () => {
    renderMockStoreId(<PromiseToPayHistory />, {
      initialState: {
        promiseToPayHistory: {
          [storeId]: { loading: false, single: { promiseStatus: 'Kept' } }
        }
      }
    });
    expect(screen.getByTestId('ptpHistorySingle')).toBeInTheDocument();
  });
  it('should render single and Recurring', () => {
    renderMockStoreId(<PromiseToPayHistory />, {
      initialState: {
        promiseToPayHistory: {
          [storeId]: {
            loading: false,
            single: { promiseStatus: 'Kept' },
            recurring: { promiseStatus: 'Kept' }
          }
        }
      }
    });
    const recurringBtn = screen.getByText('txt_recurring');
    recurringBtn.click();
    expect(screen.getByTestId('ptpHistoryRecurring')).toBeInTheDocument();
  });
  it('should render custom', () => {
    const { wrapper } = renderMockStoreId(<PromiseToPayHistory />, {
      initialState: {
        promiseToPayHistory: {
          [storeId]: { loading: false, nonRecurring: { promiseStatus: 'Kept' } }
        }
      }
    });
    const detailIcon = queryByClass(
      wrapper.container,
      /btn btn-icon-secondary/
    );
    detailIcon?.click();
    const onCloseBtn = screen.getByTestId('onClosePromiseDetail');
    onCloseBtn.click();
    expect(screen.getByTestId('ptpHistoryNonRecurring')).toBeInTheDocument();
  });
});
