import { PromiseToPayDetailsResponse } from 'pages/PromiseToPay/types';

export interface PromiseDetailGridItem {
  promiseStatus: string;
  paymentMethod: RefDataValue | undefined;
  promiseId: string;
  promiseDueDate: string;
  promiseAmount: string;
  promiseAmountPaid: string;
  lastPaymentDate: string;
}

export type SinglePromisePayload =
  PromiseToPayDetailsResponse['latestSinglePtpDetail'];

export type RecurringPromisePayload =
  PromiseToPayDetailsResponse['latestRecurringPtpDetail'];

export type NonRecurringPromisePayload =
  PromiseToPayDetailsResponse['latestNonRecurringPtpDetail'];

export interface SinglePromiseViewData {
  promiseStatus?: string;
  promiseDueDate?: string;
  promiseAmount?: string;
  paymentMethod?: RefDataValue;
  lastPaymentDate?: string;
  lastPaymentAmount?: string;
}

export interface RecurringPromiseViewData {
  promiseStatus?: string;
  promiseCount?: string;
  recurringInterval?: RefDataValue;
  totalPromiseAmount?: string;
  lastPaymentDate?: string;
  lastPaymentAmount?: string;
  keptPromiseCount?: string;
  keptPromiseAmount?: string;
  brokenPromiseCount?: string;
  brokenPromiseAmount?: string;
  pendingPromiseCount?: string;
  pendingPromiseAmount?: string;
  totalPromiseCount?: string;
}

export interface NonRecurringPromiseViewData {
  promiseStatus?: string;
  promiseCount?: string;
  totalPromiseAmount?: string;
  lastPaymentDate?: string;
  lastPaymentAmount?: string;
  keptPromiseCount?: string;
  keptPromiseAmount?: string;
  brokenPromiseCount?: string;
  brokenPromiseAmount?: string;
  pendingPromiseCount?: string;
  pendingPromiseAmount?: string;
  totalPromiseCount?: string;
}

export interface PromiseToPayHistoryStateItem {
  loading: boolean;
  single?: SinglePromiseViewData;
  recurring?: RecurringPromiseViewData;
  nonRecurring?: NonRecurringPromiseViewData;
  recurringGrid?: PromiseDetailGridItem[];
  nonRecurringGrid?: PromiseDetailGridItem[];
}

export type PromiseToPayHistoryState = Record<
  string,
  PromiseToPayHistoryStateItem
>;

export interface GetPromiseToPayHistoryArgs extends StoreIdPayload {
  postData: { accountId: string };
}

export interface GetPromiseToPayHistoryPayload {
  single?: SinglePromiseViewData;
  recurring?: RecurringPromiseViewData;
  nonRecurring?: NonRecurringPromiseViewData;
  recurringGrid?: PromiseDetailGridItem[];
  nonRecurringGrid?: PromiseDetailGridItem[];
}
