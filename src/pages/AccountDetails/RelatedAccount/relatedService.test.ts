import relatedService from './relatedService';

// utils
import { storeId, mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import { RelatedAccountRequest } from './types';

describe('relatedService', () => {
  describe('getRelatedAccount', () => {
    const params: RelatedAccountRequest = {
      common: { accountId: storeId }
    };

    const mockService = mockApiServices.post();

    relatedService.getRelatedAccount(params);

    expect(mockService).toBeCalledWith('', params);

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      relatedService.getRelatedAccount(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      relatedService.getRelatedAccount(params);

      expect(mockService).toBeCalledWith(
        apiUrl.account.getRelatedAccounts,
        params
      );
    });
  });

  describe('getAccountDetails', () => {
    const params: CommonAccountIdRequestBody = {
      common: { accountId: storeId }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      relatedService.getAccountDetails(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      relatedService.getAccountDetails(params);

      expect(mockService).toBeCalledWith(
        apiUrl.account.getAccountDetails,
        params
      );
    });
  });
});
