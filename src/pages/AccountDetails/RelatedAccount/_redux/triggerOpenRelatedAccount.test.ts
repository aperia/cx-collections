import { mockActionCreator } from 'app/test-utils';
import { tabActions } from 'pages/__commons/TabBar/_redux';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { triggerOpenRelatedAccount } from './triggerOpenRelatedAccount';

const tabActionsSpy = mockActionCreator(tabActions);

describe('Test triggerOpenRelatedAccount async thunk', () => {
  it('should be fulfilled', async () => {
    const addTabSpy = tabActionsSpy('addTab');

    const store = createStore(rootReducer, {});
    const result = await triggerOpenRelatedAccount({
      translateFn: jest
        .fn()
        .mockImplementation(() => ({ t: (text: string) => text }))
    })(store.dispatch, store.getState, {});

    expect(result.type).toEqual('account/triggerOpenRelatedAccount/fulfilled');
    expect(addTabSpy).toBeCalledWith(
      expect.objectContaining({
        storeId: 'home__store_ky__',
        accEValue: '',
        tabType: 'accountRelated',
        iconName: 'account-details',
        props: {
          accEValue: '',
          accNbr: undefined,
          memberSequenceIdentifier: undefined,
          primaryName: '',
          socialSecurityIdentifier: undefined,
          storeId: 'home__store_ky__'
        }
      })
    );
  });
});
