import * as selectors from './selectors';
import { selectorWrapper, storeId, accNbr } from 'app/test-utils';
import { DELINQUENT_OVER_LIMIT_STATUS, DELINQUENT_STATUS } from '../constants';

describe('Test Related Account Selectors', () => {
  const relatedAccData = {
    accountId: '123456',
    memberSequenceIdentifier: '0001',
    accNbr
  };

  const initialState: Partial<RootState> = {
    relatedAccount: {
      [storeId]: {
        isLoading: true,
        relatedAcc: [relatedAccData]
      }
    }
  };

  it('gets data and default data from takeLoadingRelatedAccStatus selector', () => {
    const { data, emptyData } = selectorWrapper(initialState)(
      selectors.takeLoadingRelatedAccStatus
    );

    expect(data).toEqual(true);
    expect(emptyData).toBeUndefined();
  });

  it('gets data and default data from takeRelatedAccount selector', () => {
    const { data, emptyData } = selectorWrapper(initialState)(
      selectors.takeRelatedAccount
    );

    expect(data).toEqual([relatedAccData]);
    expect(emptyData).toEqual([]);
  });

  describe('Test takeRelatedAccountDelinquentStatus', () => {
    it('internalStatusCode is undefined', () => {
      const { data, emptyData } = selectorWrapper(initialState)(
        selectors.takeRelatedAccountDelinquentStatus
      );

      expect(data).toEqual(false);
      expect(emptyData).toEqual(false);
    });

    it('internalStatusCode is DELINQUENT_STATUS', () => {
      const initialStateHavingDelinquentStatus: Partial<RootState> = {
        relatedAccount: {
          [storeId]: {
            isLoading: true,
            relatedAcc: [{ internalStatusCode: DELINQUENT_STATUS }]
          }
        }
      };

      const { data, emptyData } = selectorWrapper(
        initialStateHavingDelinquentStatus
      )(selectors.takeRelatedAccountDelinquentStatus);

      expect(data).toEqual(true);
      expect(emptyData).toEqual(false);
    });

    it('internalStatusCode is DELINQUENT_OVER_LIMIT_STATUS', () => {
      const initialStateHavingDelinquentOverLimit: Partial<RootState> = {
        relatedAccount: {
          [storeId]: {
            isLoading: true,
            relatedAcc: [{ internalStatusCode: DELINQUENT_OVER_LIMIT_STATUS }]
          }
        }
      };

      const { data, emptyData } = selectorWrapper(
        initialStateHavingDelinquentOverLimit
      )(selectors.takeRelatedAccountDelinquentStatus);

      expect(data).toEqual(true);
      expect(emptyData).toEqual(false);
    });
  });
});
