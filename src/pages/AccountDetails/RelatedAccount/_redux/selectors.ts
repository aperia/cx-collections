import { createSelector } from '@reduxjs/toolkit';
import isUndefined from 'lodash.isundefined';

// Const
import { DELINQUENT_STATUS, DELINQUENT_OVER_LIMIT_STATUS } from '../constants';

export const getLoadingRelatedAccStatus = (state: RootState, storeId: string) =>
  state.relatedAccount[storeId]?.isLoading;

export const takeLoadingRelatedAccStatus = createSelector(
  getLoadingRelatedAccStatus,
  data => data
);

// Related account
export const getRelatedAccount = (state: RootState, storeId: string) =>
  state.relatedAccount[storeId]?.relatedAcc || [];

export const takeRelatedAccount = createSelector(
  getRelatedAccount,
  data => data
);

// Delinquent status
export const takeRelatedAccountDelinquentStatus = createSelector(
  getRelatedAccount,
  data => {
    const account = data.find(
      item =>
        item.internalStatusCode === DELINQUENT_STATUS ||
        item.internalStatusCode === DELINQUENT_OVER_LIMIT_STATUS
    );
    return !isUndefined(account);
  }
);
