import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Types
import {
  RelatedAccountStateResult,
  SetRelatedSelectedCallerArgs
} from '../types';

// Action
import { triggerOpenRelatedAccount } from './triggerOpenRelatedAccount';
import {
  getRelatedAccount,
  getRelatedAccountBuilder
} from './getRelatedAccount';

const { actions, reducer } = createSlice({
  name: 'relatedAccount',
  initialState: {} as RelatedAccountStateResult,
  reducers: {
    removeRelatedAccountBasedOnStoreId: (
      draftState,
      action: PayloadAction<StoreIdPayload>
    ) => {
      const { storeId } = action.payload;

      delete draftState[storeId];
    },
    setRelatedSelectedCaller: (
      draftState,
      action: PayloadAction<SetRelatedSelectedCallerArgs>
    ) => {
      const { storeId, data } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        selectedCaller: data
      };
    }
  },
  extraReducers: builder => {
    getRelatedAccountBuilder(builder);
  }
});

const allActions = {
  ...actions,
  triggerOpenRelatedAccount,
  getRelatedAccount
};

export { allActions as relatedAccountActions, reducer as relatedAccount };
