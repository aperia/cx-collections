// Const
import { CUSTOMER_ROLE_CODE, MAPPING_STORE_KY } from 'app/constants';
import { contactEntitlement } from 'app/entitlements';

// Redux
import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';
import { entitlementActions } from 'pages/__commons/Entitlement/_redux/reducers';

export interface TriggerGetRelatedAccDetail {
  storeId: string;
  memberSequenceIdentifier: string;
  socialSecurityIdentifier: string;
  accEValue: string;
}

export const triggerGetRelatedAccDetail: AppThunk<Promise<unknown>> =
  ({
    storeId,
    accEValue,
    memberSequenceIdentifier,
    socialSecurityIdentifier
  }: TriggerGetRelatedAccDetail) =>
  async (dispatch, getState) => {
    const { accountDetail } = getState();
    const [originalStore] = storeId.split(MAPPING_STORE_KY);
    const { selectedCaller = {} } = accountDetail[originalStore] || {};
    const {
      clientIdentifier = '',
      systemIdentifier = '',
      principalIdentifier = '',
      agentIdentifier = '',
      customerRoleTypeCode = '',
      isByPass
    } = selectedCaller;

    contactEntitlement.removeRegister(storeId);

    await dispatch<Promise<unknown>>(
      accountDetailActions.getAccountDetail({
        storeId,
        postData: {
          common: {
            accountId: accEValue
          },
          inquiryFields: {
            memberSequenceIdentifier
          }
        },
        socialSecurityIdentifier,
        triggerRenewToken: true
      })
    );

    await dispatch<Promise<unknown>>(
      entitlementActions.getContactEntitlement({
        agent: agentIdentifier,
        clientNumber: clientIdentifier,
        prin: principalIdentifier,
        system: systemIdentifier,
        accountId: accEValue,
        customerRoleTypeCode: isByPass
          ? CUSTOMER_ROLE_CODE.primary
          : customerRoleTypeCode,
        storeId
      })
    );
  };
