import { accEValue } from './../../../../app/test-utils/renderComponentWithMockStore';
import { mockActionCreator, storeId } from 'app/test-utils';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import { triggerGetRelatedAccDetail } from './triggerReleatedAcc';
import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';

const tabActionsSpy = mockActionCreator(accountDetailActions);

describe('Test triggerGetRelatedAccDetail async thunk', () => {
  it('should be fulfilled', async () => {
    const spy = tabActionsSpy('getAccountDetail');

    const store = createStore(
      rootReducer,
      {
        accountDetail: {
          [storeId]: {
            selectedCaller: {
              clientIdentifier: '123',
              systemIdentifier: '123',
              principalIdentifier: '123',
              agentIdentifier: '123',
              customerRoleTypeCode: '123',
              isByPass: true
            }
          }
        }
      },
      applyMiddleware(thunk)
    );

    await triggerGetRelatedAccDetail({
      storeId,
      accEValue,
      memberSequenceIdentifier: '',
      socialSecurityIdentifier: ''
    })(store.dispatch, store.getState, {});

    expect(spy).toBeCalledWith({
      storeId,
      postData: {
        common: {
          accountId: accEValue
        },
        inquiryFields: {
          memberSequenceIdentifier: ''
        }
      },
      socialSecurityIdentifier: '',
      triggerRenewToken: true
    });
  });

  it('should be fulfilled with empty state', async () => {
    const spy = tabActionsSpy('getAccountDetail');

    const store = createStore(
      rootReducer,
      {},
      applyMiddleware(thunk)
    );

    await triggerGetRelatedAccDetail({
      storeId,
      accEValue,
      memberSequenceIdentifier: '',
      socialSecurityIdentifier: ''
    })(store.dispatch, store.getState, {});

    expect(spy).toBeCalledWith({
      storeId,
      postData: {
        common: {
          accountId: accEValue
        },
        inquiryFields: {
          memberSequenceIdentifier: ''
        }
      },
      socialSecurityIdentifier: '',
      triggerRenewToken: true
    });
  });
});
