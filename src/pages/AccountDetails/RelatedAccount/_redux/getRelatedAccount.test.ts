import { accEValue, responseDefault, storeId } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import relatedService from '../relatedService';
import { getRelatedAccount } from './getRelatedAccount';
import * as commons from 'app/helpers/commons';

let spy: jest.SpyInstance;
let spy1: jest.SpyInstance;

describe('Test getRelatedAccount async thunk', () => {
  afterEach(() => {
    spy.mockReset();
    spy.mockRestore();

    spy1.mockReset();
    spy1.mockRestore();
  });

  describe('should be fulfilled', () => {
    beforeEach(() => {
      jest.spyOn(commons, 'getFeatureConfig').mockResolvedValue({
        selectFields: ['"accountIdentifier"'],
        defaultFields: {
          endSequence: 500,
          odsNaming: false,
          relationshipTypeCode: 'S',
          startSequence: 1
        },
        selectFieldDetail: ['customerInquiry.accountIdentifier']
      });
    });

    it('full data', async () => {
      spy = jest.spyOn(relatedService, 'getRelatedAccount').mockResolvedValue({
        ...responseDefault,
        data: { accounts: [{ accountIdentifier: '' }] }
      });

      spy1 = jest.spyOn(relatedService, 'getAccountDetails').mockResolvedValue({
        ...responseDefault,
        data: {
          cardholderInfo: [
            {
              namePrincipalCardholder: 'ADAMS,HENRY J             '
            }
          ],
          customerInquiry: [
            {
              accountIdentifier: '{"maskedValue":"022002******1611", eValue:"" '
            }
          ]
        }
      });

      const store = createStore(rootReducer, {
        tab: {
          tabStoreIdSelected: storeId
        },
        accountDetail: {
          [storeId]: {
            data: {
              accNbr: '123456',
              primaryName: 'SMITH'
            },
            isLoading: false
          }
        }
      });

      const result = await getRelatedAccount({ storeId, accountId: accEValue })(
        store.dispatch,
        store.getState,
        {}
      );

      expect(result.type).toEqual('account/getRelatedAccount/fulfilled');
      expect(result.payload).toEqual({ accounts: [] });
    });

    it('no inject data', async () => {
      spy = jest.spyOn(relatedService, 'getRelatedAccount').mockResolvedValue({
        ...responseDefault,
        data: { accounts: [{ accountIdentifier: '' }] }
      });

      spy1 = jest.spyOn(relatedService, 'getAccountDetails').mockResolvedValue({
        ...responseDefault,
        data: {}
      });

      const store = createStore(rootReducer, {});

      const result = await getRelatedAccount({ storeId, accountId: accEValue })(
        store.dispatch,
        store.getState,
        {}
      );

      expect(result.type).toEqual('account/getRelatedAccount/fulfilled');
    });

    it('getAccountDetails failed', async () => {
      spy = jest.spyOn(relatedService, 'getRelatedAccount').mockResolvedValue({
        ...responseDefault,
        data: { accounts: [{ accountIdentifier: '' }] }
      });

      spy1 = jest.spyOn(relatedService, 'getAccountDetails').mockRejectedValue({
        ...responseDefault,
        data: {}
      });

      const store = createStore(rootReducer, {});

      const result = await getRelatedAccount({ storeId, accountId: accEValue })(
        store.dispatch,
        store.getState,
        {}
      );

      expect(result.type).toEqual('account/getRelatedAccount/fulfilled');
    });
  });

  it('should be rejected', async () => {
    spy = jest.spyOn(relatedService, 'getRelatedAccount').mockResolvedValue({
      ...responseDefault,
      data: { accounts: [{ accountIdentifier: '' }] }
    });

    spy1 = jest.spyOn(relatedService, 'getAccountDetails').mockRejectedValue({
      ...responseDefault,
      data: {}
    });

    const store = createStore(rootReducer, {});

    const result = await getRelatedAccount({ storeId, accountId: accEValue })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(result.type).toEqual('account/getRelatedAccount/rejected');
  });
});
