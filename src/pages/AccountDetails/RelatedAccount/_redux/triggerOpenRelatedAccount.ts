import { createAsyncThunk } from '@reduxjs/toolkit';

// Actions
import { tabActions } from 'pages/__commons/TabBar/_redux';

// Const
import { RELATED_ACCOUNT_LANG } from 'app/constants/i18n';
import { EMPTY_STRING, MAPPING_STORE_KY } from 'app/constants';

// Type
import { TriggerOpenRelatedAccountArgs } from '../types';
import { relatedAccountActions } from './reducer';
import { batch } from 'react-redux';

export const triggerOpenRelatedAccount = createAsyncThunk<
  unknown,
  TriggerOpenRelatedAccountArgs,
  ThunkAPIConfig
>('account/triggerOpenRelatedAccount', async (args, thunkAPI) => {
  const { getState, dispatch } = thunkAPI;
  const { translateFn } = args;
  const { tab, accountDetail } = getState();
  const { tabStoreIdSelected, accEValueSelected = EMPTY_STRING } = tab;
  const storeId = `${tabStoreIdSelected}${MAPPING_STORE_KY}${''}`;
  const { data: accountInfo, selectedCaller = {} } =
    accountDetail[tabStoreIdSelected] || {};
  const {
    primaryName = EMPTY_STRING,
    accNbr,
    memberSequenceIdentifier,
    socialSecurityIdentifier
  } = accountInfo || {};

  batch(() => {
    dispatch(
      tabActions.addTab({
        title: translateFn(RELATED_ACCOUNT_LANG.TITLE, {
          name: primaryName
        }),
        storeId,
        accEValue: accEValueSelected,
        tabType: 'accountRelated',
        iconName: 'account-details',
        props: {
          accEValue: accEValueSelected,
          storeId,
          primaryName,
          accNbr,
          memberSequenceIdentifier,
          socialSecurityIdentifier
        }
      })
    );

    dispatch(
      relatedAccountActions.setRelatedSelectedCaller({
        storeId,
        data: selectedCaller
      })
    );
  });
});
