import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// Helper
import { getFeatureConfig, trimData } from 'app/helpers';
import { handleMapRelatedAccounts } from '../helpers';

// Const
import { RELATED_ACCOUNT_CONFIG } from '../constants';
import { RelatedAccountConfig } from 'app/global-types/relatedAccount';
import {
  CUSTOMER_ROLE_CODE,
  EMPTY_ARRAY,
  EMPTY_STRING_OBJECT,
  EMPTY_STRING,
  FULFILLED
} from 'app/constants';

// Service
import relatedService from '../relatedService';

// Types
import {
  GetRelatedAccountArgs,
  GetRelatedAccountPayload,
  ListRelatedPromise,
  RelatedAccount,
  RelatedAccountStateResult
} from '../types';

export const getRelatedAccount = createAsyncThunk<
  GetRelatedAccountPayload,
  GetRelatedAccountArgs,
  ThunkAPIConfig
>('account/getRelatedAccount', async (args, thunkAPI) => {
  const { socialSecurityIdentifier } = args;
  const { getState } = thunkAPI;
  const { mapping, accountDetail, tab } = getState();
  const { relatedAccounts = {} } = mapping?.data || {};
  const { data: accountDetailData } =
    accountDetail[tab?.tabStoreIdSelected] || {};

  const { selectFields, defaultFields, selectFieldDetail } =
    await getFeatureConfig<RelatedAccountConfig>(RELATED_ACCOUNT_CONFIG);

  // Get relationship search text from social security number
  const { eValue: relationshipSearchText = EMPTY_STRING } = JSON.parse(
    socialSecurityIdentifier || EMPTY_STRING_OBJECT
  ) as CommonIdentifier;

  const { data } = await relatedService.getRelatedAccount({
    ...defaultFields,
    relationshipClientIdentifier: accountDetailData?.clientID,
    relationshipSearchText,
    selectFields
  });

  const accounts: Array<RelatedAccount> = trimData(data.accounts);

  const listAccountsPromise = accounts.map(item => {
    return relatedService.getAccountDetails({
      common: {
        accountId: item.relationshipAccountIdentifier
      },
      inquiryFields: {
        memberSequenceIdentifier: CUSTOMER_ROLE_CODE.primary
      },
      selectFields: selectFieldDetail
    });
  }) as ListRelatedPromise;

  const response = await Promise.allSettled(listAccountsPromise);

  let listAccounts: Array<RelatedAccount> = EMPTY_ARRAY;
  response.forEach(item => {
    if (item.status === FULFILLED) {
      const [accountData] = item.value.data.customerInquiry || EMPTY_ARRAY;
      const [infoData] = item.value.data.cardholderInfo || EMPTY_ARRAY;
      listAccounts = [...listAccounts, { ...accountData, ...infoData }];
    }
  });

  return {
    accounts: handleMapRelatedAccounts(listAccounts, relatedAccounts)
  };
});

export const getRelatedAccountBuilder = (
  builder: ActionReducerMapBuilder<RelatedAccountStateResult>
) => {
  builder
    .addCase(getRelatedAccount.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true
      };
    })
    .addCase(getRelatedAccount.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { accounts } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        relatedAcc: accounts
      };
    })
    .addCase(getRelatedAccount.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false
      };
    });
};
