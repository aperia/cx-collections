import { relatedAccountActions, relatedAccount } from './reducer';
import { storeId } from 'app/test-utils';

describe('Test Related Account reducer', () => {
  const initialState = {
    [storeId]: {
      relatedAcc: [],
      isLoading: true
    }
  };

  it('runs removeRelatedAccountBasedOnStoreId', () => {
    const state = relatedAccount(
      initialState,
      relatedAccountActions.removeRelatedAccountBasedOnStoreId({ storeId })
    );

    expect(state).toEqual({});
  });
});
