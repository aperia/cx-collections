import React, { useEffect, useLayoutEffect, useRef } from 'react';
import classNames from 'classnames';
import isEmpty from 'lodash.isempty';
import isUndefined from 'lodash.isundefined';

// Hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useDispatch, useSelector } from 'react-redux';

// Types
import { RelatedAccountData } from '../types';

// Const
import { RELATED_ACCOUNT_LIST_VIEW } from '../constants';
import {
  DASH,
  EMPTY_STRING_OBJECT,
  EMPTY_STRING,
  MAPPING_STORE_KY
} from 'app/constants';

// Components
import { View } from 'app/_libraries/_dof/core';
import { InlineMessage } from 'app/_libraries/_dls/components';

// Selectors
import {
  takeLoadingRelatedAccStatus,
  takeRelatedAccount,
  takeRelatedAccountDelinquentStatus
} from '../_redux/selectors';
import { relatedAccountActions } from '../_redux/reducer';
import { takeAccountDetailLoadingStatus } from 'pages/AccountDetails/_redux/selectors';
import { tabActions } from 'pages/__commons/TabBar/_redux';
import { genAmtId } from 'app/_libraries/_dls/utils';

const WrappedList: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId, accEValue, socialSecurityIdentifier } = useAccountDetail();

  const relatedAccounts =
    useStoreIdSelector<Array<RelatedAccountData>>(takeRelatedAccount);
  const isHaveDelinquent = useStoreIdSelector<boolean>(
    takeRelatedAccountDelinquentStatus
  );
  const isLoadingRelatedAcc = useStoreIdSelector<boolean>(
    takeLoadingRelatedAccStatus
  );
  const divRef = useRef<HTMLDivElement | null>(null);
  const testId = 'related-account-details-tab-list';

  const handleClickViewAccount = (account: RelatedAccountData) => {
    const {
      accNbr,
      primaryName = EMPTY_STRING,
      memberSequenceIdentifier,
      customerRoleTypeCode,
      id,
      socialSecurityIdentifier: accSocialSecurityIdentifier
    } = account;
    const title = `${primaryName} ${DASH} ${accNbr}`;
    const { eValue: reAccEValue = account.accountId } = JSON.parse(
      account?.accountId || EMPTY_STRING_OBJECT
    );
    const newStoreId = `${reAccEValue}${DASH}${memberSequenceIdentifier}`;
    const [relatedStoreId] = storeId.split(MAPPING_STORE_KY);

    dispatch(
      tabActions.addTab({
        title,
        storeId: newStoreId,
        accEValue: reAccEValue,
        customerRoleTypeCode,
        customerExternalIdentifier: id,
        tabType: 'accountDetail',
        iconName: 'account-details',
        props: {
          primaryName: primaryName,
          accEValue: reAccEValue,
          storeId: newStoreId,
          accNbr: accNbr ?? '',
          memberSequenceIdentifier,
          socialSecurityIdentifier: accSocialSecurityIdentifier,
          relatedStoreId
        }
      })
    );
  };

  const renderCardRow = () => {
    if (isEmpty(relatedAccounts)) return null;
    return relatedAccounts.map(item => {
      const { id } = item;
      return (
        <div key={id} className="pb-16">
          <div
            id={`${id}_card_container`}
            onClick={() => handleClickViewAccount(item)}
            data-testid={genAmtId(`${id}_${testId}`, 'card-container', '')}
          >
            <div className="card-primary">
              <div className="card-item p-16">
                <h5
                  id={`${id}__primary-name`}
                  data-testid={genAmtId(`${id}_${testId}`, 'primary-name', '')}
                >
                  {item?.primaryName}
                </h5>
                <View
                  id={`${storeId}_${id}_${RELATED_ACCOUNT_LIST_VIEW}`}
                  formKey={`${storeId}_${id}_${RELATED_ACCOUNT_LIST_VIEW}`}
                  dataTestId={`${id}_${testId}_${RELATED_ACCOUNT_LIST_VIEW}`}
                  descriptor={RELATED_ACCOUNT_LIST_VIEW}
                  value={item}
                />
              </div>
            </div>
          </div>
        </div>
      );
    });
  };

  useEffect(() => {
    dispatch(
      relatedAccountActions.getRelatedAccount({
        storeId,
        socialSecurityIdentifier,
        accountId: accEValue
      })
    );
  }, [dispatch, accEValue, storeId, socialSecurityIdentifier]);

  useLayoutEffect(() => {
    divRef.current &&
      (divRef.current.style.height = isLoadingRelatedAcc
        ? 'calc(100vh - 370px)'
        : 'auto');
  }, [isLoadingRelatedAcc]);

  return (
    <div
      className={classNames({
        loading: isLoadingRelatedAcc
      })}
      ref={divRef}
    >
      <div
        className={'px-24 pt-24 pb-8'}
        id={`${storeId}-related-account-results`}
        data-testid={genAmtId(testId, 'results-list', '')}
      >
        <h4
          className="mb-16"
          data-testid={genAmtId(testId, 'account-list-title', '')}
        >
          {t('txt_related_account_list')}
        </h4>
        {isHaveDelinquent ? (
          <InlineMessage
            variant="danger"
            dataTestId={`${testId}_sub-title-status`}
          >
            {t('txt_subtitle_one_related_account_status')}
          </InlineMessage>
        ) : null}
        {renderCardRow()}
      </div>
    </div>
  );
};

export const List: React.FC = () => {
  const { storeId } = useAccountDetail();
  const isLoading = useSelector((state: RootState) =>
    takeAccountDetailLoadingStatus(state, storeId)
  );

  if (isUndefined(isLoading) || isLoading) {
    return null;
  }

  return <WrappedList />;
};

export default List;
