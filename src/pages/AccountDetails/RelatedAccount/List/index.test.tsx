import {
  mockActionCreator,
  queryByClass,
  renderMockStoreId,
  storeId
} from 'app/test-utils';
import React from 'react';
import List from './index';
import { fireEvent, screen } from '@testing-library/react';
import { relatedAccountActions } from '../_redux/reducer';
import { tabActions } from 'pages/__commons/TabBar/_redux';
import { DELINQUENT_STATUS } from '../constants';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const relatedAccountActionsSpy = mockActionCreator(relatedAccountActions);
const tabActionsSpy = mockActionCreator(tabActions);

const mockAccountDetail = {
  [storeId]: {
    data: {
      accNbr: '123456',
      primaryName: 'SMITH'
    },
    isLoading: false
  }
};

describe('Test List component', () => {
  it('not render component when loading', () => {
    relatedAccountActionsSpy('getRelatedAccount');

    const { wrapper } = renderMockStoreId(<List />, {
      initialState: {
        accountDetail: {
          [storeId]: {
            data: {
              accNbr: '123456',
              primaryName: 'SMITH'
            },
            isLoading: true
          }
        }
      }
    });

    expect(wrapper.container.innerHTML).toEqual('');
  });

  it('not render card component', () => {
    relatedAccountActionsSpy('getRelatedAccount');

    const { wrapper } = renderMockStoreId(<List />, {
      initialState: {
        accountDetail: {
          [storeId]: {
            data: {
              accNbr: '123456',
              primaryName: 'SMITH'
            },
            isLoading: false
          }
        }
      }
    });

    expect(
      queryByClass(wrapper.container, 'card-primary')
    ).not.toBeInTheDocument();

    expect(screen.getByText('txt_related_account_list')).toBeInTheDocument();
  });

  it('render card component > having loading', () => {
    relatedAccountActionsSpy('getRelatedAccount');

    const { wrapper } = renderMockStoreId(<List />, {
      initialState: {
        relatedAccount: {
          [storeId]: {
            relatedAcc: [
              {
                id: '1',
                accNbr: '022002******1611'
              }
            ],
            isLoading: true
          }
        },
        accountDetail: {
          [storeId]: {
            data: {
              accNbr: '123456',
              primaryName: 'SMITH'
            },
            isLoading: false
          }
        }
      }
    });

    expect(queryByClass(wrapper.container, 'loading')).toBeInTheDocument();
  });

  it('render card component', () => {
    relatedAccountActionsSpy('getRelatedAccount');

    renderMockStoreId(<List />, {
      initialState: {
        relatedAccount: {
          [storeId]: {
            relatedAcc: [
              {
                id: '1',
                accNbr: '022002******1611',
                accountId: '{"maskedValue":"022002******1611","eValue":""}',
                address: {
                  addressLineOne: 'C/O FIRST DATA RESOURCES',
                  addressLineTwo: '7301 PACIFIC STREET'
                }
              },
              {
                id: '2',
                internalStatusCode: DELINQUENT_STATUS
              }
            ]
          }
        },
        accountDetail: mockAccountDetail
      }
    });

    expect(screen.getByText('txt_related_account_list')).toBeInTheDocument();
    expect(
      screen.getByText('txt_subtitle_one_related_account_status')
    ).toBeInTheDocument();
    expect(screen.getAllByTestId('relatedAccountListView').length).toEqual(2);
  });

  it('clicks card component', () => {
    relatedAccountActionsSpy('getRelatedAccount');
    const addTab = tabActionsSpy('addTab');

    renderMockStoreId(<List />, {
      initialState: {
        relatedAccount: {
          [storeId]: {
            relatedAcc: [
              {
                id: '1',
                accountId: '{"maskedValue":"022002******1611","eValue":""}',
                address: {
                  addressLineOne: 'C/O FIRST DATA RESOURCES',
                  addressLineTwo: '7301 PACIFIC STREET'
                }
              }
            ]
          }
        },
        accountDetail: mockAccountDetail
      }
    });

    const card = screen.getByTestId('relatedAccountListView');
    fireEvent.click(card);

    expect(addTab).toBeCalled();
  });

  it('clicks card component > dispatch with default values', () => {
    relatedAccountActionsSpy('getRelatedAccount');
    const addTab = tabActionsSpy('addTab');

    renderMockStoreId(<List />, {
      initialState: {
        relatedAccount: {
          [storeId]: {
            relatedAcc: [
              {
                id: '1',
                accNbr: '022002******1611',
                address: {
                  addressLineOne: 'C/O FIRST DATA RESOURCES',
                  addressLineTwo: '7301 PACIFIC STREET'
                }
              }
            ]
          }
        },
        accountDetail: mockAccountDetail
      }
    });

    const card = screen.getByTestId('relatedAccountListView');
    fireEvent.click(card);

    expect(addTab).toBeCalled();
  });
});
