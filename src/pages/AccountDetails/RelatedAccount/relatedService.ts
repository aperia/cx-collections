import { apiService } from 'app/utils/api.service';

// Type
import {
  AccountRelatedResponse,
  GetRelatedAccountResponse,
  RelatedAccountRequest
} from './types';

const relatedService = {
  getRelatedAccount: (body: RelatedAccountRequest) => {
    const url = window.appConfig?.api?.account?.getRelatedAccounts || '';
    return apiService.post<GetRelatedAccountResponse>(url, body);
  },
  getAccountDetails: (bodyRequest: CommonAccountIdRequestBody) => {
    const url = window.appConfig?.api?.account?.getAccountDetails || '';
    return apiService.post<AccountRelatedResponse>(url, { ...bodyRequest });
  }
};

export default relatedService;
