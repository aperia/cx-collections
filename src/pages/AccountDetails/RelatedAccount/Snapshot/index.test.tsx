import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';
import React from 'react';
import Snapshot from './index';
import { fireEvent, screen } from '@testing-library/react';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const accountDetailActionsSpy = mockActionCreator(accountDetailActions);

describe('Test Snapshot component', () => {
  it('render component', () => {
    renderMockStoreId(<Snapshot />, {
      initialState: {
        accountDetail: {
          [storeId]: {
            data: {
              accNbr: '123456',
              primaryName: 'SMITH'
            },
            isLoading: false
          }
        }
      }
    });

    expect(screen.getByText('SMITH')).toBeInTheDocument();
    expect(screen.getByTestId('relatedAccountSnapshot')).toBeInTheDocument();
  });

  it('render component when error occurs', () => {
    const getAccountDetail = accountDetailActionsSpy('getAccountDetail');
    renderMockStoreId(<Snapshot />, {
      initialState: {
        accountDetail: {
          [storeId]: {
            data: {
              accNbr: '123456',
              primaryName: 'SMITH'
            },
            isLoading: false,
            isError: true
          }
        }
      }
    });

    const button = screen.getByText('txt_reload');
    fireEvent.click(button);

    expect(getAccountDetail).toBeCalled();
    expect(
      screen.getByText('txt_data_load_unsuccessful_click_reload_to_try_again')
    ).toBeInTheDocument();
    expect(screen.getByText('txt_failure_ocurred_on')).toBeInTheDocument();
  });
});
