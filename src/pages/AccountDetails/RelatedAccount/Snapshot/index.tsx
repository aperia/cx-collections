import React, { useCallback, useLayoutEffect, useRef } from 'react';
import classNames from 'classnames';
import { useDispatch } from 'react-redux';
import { View } from 'app/_libraries/_dof/core';

// Types
import { AccountInfo } from '../types';

// Components
import { FailedApiReload } from 'app/components';

// Const
import { RELATED_ACCOUNT_SNAPSHOT_VIEW } from '../constants';

// Hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// Selectors
import {
  takeAccountDetail,
  takeAccountDetailErrorStatus,
  takeAccountDetailLoadingStatus
} from 'pages/AccountDetails/_redux/selectors';
import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';
import { genAmtId } from 'app/_libraries/_dls/utils';

const Snapshot: React.FC = () => {
  const {
    storeId,
    accEValue,
    memberSequenceIdentifier,
    socialSecurityIdentifier
  } = useAccountDetail();
  const dispatch = useDispatch();

  const snapshotData = useStoreIdSelector<AccountInfo>(takeAccountDetail);
  const isError = useStoreIdSelector<boolean>(takeAccountDetailErrorStatus);
  const isLoading = useStoreIdSelector<boolean>(takeAccountDetailLoadingStatus);
  const divRef = useRef<HTMLDivElement | null>(null);
  const testId = 'related-account-detail-snapshot';

  const handleReloadData = useCallback(() => {
    dispatch(
      accountDetailActions.getAccountDetail({
        storeId,
        postData: {
          common: {
            accountId: accEValue
          },
          inquiryFields: {
            memberSequenceIdentifier
          }
        },
        socialSecurityIdentifier,
        isReload: true
      })
    );
  }, [
    dispatch,
    accEValue,
    storeId,
    memberSequenceIdentifier,
    socialSecurityIdentifier
  ]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = '252px');
  }, []);

  if (isError) {
    return (
      <div
        ref={divRef}
        className={classNames(
          { isLoading },
          'bg-light-l20 d-flex align-items-center'
        )}
      >
        <FailedApiReload
          id="account-detail-snapshot-failed"
          className="w-100"
          onReload={handleReloadData}
          dataTestId={`${testId}_api-reload-failed`}
        />
      </div>
    );
  }

  return (
    <div className={classNames('bg-light-l20 p-24 border-bottom')}>
      <h3 data-testid={genAmtId(testId, 'title', '')}>{snapshotData?.primaryName}</h3>
      <div className="mt-8">
        <View
          id={`${storeId}_${RELATED_ACCOUNT_SNAPSHOT_VIEW}`}
          formKey={`${storeId}_${RELATED_ACCOUNT_SNAPSHOT_VIEW}`}
          descriptor={RELATED_ACCOUNT_SNAPSHOT_VIEW}
          value={snapshotData}
        />
      </div>
    </div>
  );
};

export default Snapshot;
