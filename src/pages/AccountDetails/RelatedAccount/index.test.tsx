import {
  renderMockStoreId,
  storeId,
  accEValue,
  queryByClass,
  mockActionCreator
} from 'app/test-utils';
import React from 'react';
import RelatedAccount from './index';
import { accountDetailActions } from '../_redux/reducer';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const accountDetailActionsSpy = mockActionCreator(accountDetailActions);

describe('Test RelatedAccount component', () => {
  it('render loading', () => {
    const getAccountDetail = accountDetailActionsSpy('getAccountDetail');

    const { wrapper } = renderMockStoreId(
      <RelatedAccount
        primaryName="SMITH"
        storeId={storeId}
        accEValue={accEValue}
      />,
      {
        initialState: {
          accountDetail: {
            [storeId]: {
              isLoading: true
            }
          }
        }
      }
    );

    expect(
      queryByClass(wrapper.container, 'h-100 loading')
    ).toBeInTheDocument();
    expect(getAccountDetail).toBeCalled();
  });

  it('render component', () => {
    accountDetailActionsSpy('getAccountDetail');

    const { wrapper } = renderMockStoreId(
      <RelatedAccount
        primaryName="SMITH"
        storeId={storeId}
        accEValue={accEValue}
      />,
      {
        initialState: {
          accountDetail: {
            [storeId]: {
              data: {
                accNbr: '123456',
                primaryName: 'SMITH'
              },
              isLoading: false
            }
          }
        }
      }
    );
    expect(queryByClass(wrapper.container, 'h-100')).toBeInTheDocument();
  });
});
