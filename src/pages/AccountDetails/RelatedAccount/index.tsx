import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import classNames from 'classnames';
import { SimpleBar } from 'app/_libraries/_dls/components';
import isUndefined from 'lodash.isundefined';

// Const
import { MAPPING_STORE_KY } from 'app/constants';

// Hooks
import { AccountDetailProvider } from 'app/hooks';

// Types
import { AccountDetailPageProps } from '..';

// Components
import Snapshot from './Snapshot';
import List from './List';

// Redux
import {
  takeAccountDetailLoadingStatus,
  takeRefreshAcc
} from '../_redux/selectors';
import { triggerGetRelatedAccDetail } from './_redux/triggerReleatedAcc';
import { takeContactEntitlementLoadingStatus } from 'pages/__commons/Entitlement/_redux/selectors';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface RelatedAccountProps extends AccountDetailPageProps {}

const RelatedAccount: React.FC<RelatedAccountProps> = props => {
  const {
    memberSequenceIdentifier,
    storeId,
    accEValue,
    accNbr,
    socialSecurityIdentifier
  } = props;
  const contextValueRef = useRef({
    storeId,
    accEValue,
    accNbr,
    memberSequenceIdentifier,
    socialSecurityIdentifier
  });

  const dispatch = useDispatch();

  // watching the changes of storeId, accountId
  contextValueRef.current.storeId = storeId;
  contextValueRef.current.accEValue = accEValue;
  contextValueRef.current.accNbr = accNbr;
  contextValueRef.current.memberSequenceIdentifier = memberSequenceIdentifier;
  contextValueRef.current.socialSecurityIdentifier = socialSecurityIdentifier;

  const [originalStore] = storeId.split(MAPPING_STORE_KY);

  const isLoadingAccountDetail = useSelector((state: RootState) =>
    takeAccountDetailLoadingStatus(state, storeId)
  );

  const isLoadingContactEntitlement = useSelector((state: RootState) =>
    takeContactEntitlementLoadingStatus(state, storeId)
  );

  const isRefreshAcc = useSelector((state: RootState) =>
    takeRefreshAcc(state, originalStore)
  );

  useEffect(() => {
    dispatch(
      triggerGetRelatedAccDetail({
        storeId,
        accEValue,
        memberSequenceIdentifier,
        socialSecurityIdentifier
      })
    );
  }, [
    dispatch,
    accEValue,
    memberSequenceIdentifier,
    socialSecurityIdentifier,
    storeId,
    isRefreshAcc
  ]);

  if (
    isUndefined(isLoadingAccountDetail) ||
    isLoadingAccountDetail ||
    isUndefined(isLoadingContactEntitlement) ||
    isLoadingContactEntitlement
  ) {
    return (
      <div
        className={classNames('h-100', {
          loading: isLoadingAccountDetail || isLoadingContactEntitlement
        })}
        data-testid={genAmtId('relatedAccount', 'tab', '')}
      />
    );
  }

  return (
    <AccountDetailProvider value={contextValueRef.current}>
      <SimpleBar>
        <Snapshot />
        <List />
      </SimpleBar>
    </AccountDetailProvider>
  );
};

export default RelatedAccount;
