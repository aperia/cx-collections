export const RELATED_ACCOUNT_SNAPSHOT_VIEW = 'relatedAccountSnapshot';
export const RELATED_ACCOUNT_LIST_VIEW = 'relatedAccountListView';

export const RELATED_ACCOUNT_CONFIG = 'config/relatedAccount.json';

export const RELATED_ACCOUNT_FIELD_NAME = 'relatedAccounts';

export const DELINQUENT_STATUS = 'D';
export const DELINQUENT_OVER_LIMIT_STATUS = 'X';
