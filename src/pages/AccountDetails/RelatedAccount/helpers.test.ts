import { handleMapRelatedAccounts } from './helpers';

describe('Test Related Account helpers', () => {
  const mapping = {
    accountId: 'accountIdentifier',
    'address.addressLineOne': 'addressContinuationLine1Text',
    'address.addressLineTwo': 'addressContinuationLine2Text',
    availableCreditAmount: 'availableCreditAmount',
    balanceAmount: 'currentBalanceAmount',
    cardNbr: 'presentationInstrumentIdentifier',
    cityName: 'cityName',
    creditLimitAmount: 'creditLimitAmount',
    customerRoleTypeCode: 'customerRoleTypeCode',
    cycleCode: 'billingCycleCode',
    cycleCounts: 'lifetimeDelinquentCycleCount',
    daysDelinquent: 'daysDelinquentCount',
    delinquentAmount: 'delinquentAmount',
    externalStatusCode: 'externalStatusCode',
    id: 'customerExternalIdentifier',
    internalStatusCode: 'internalStatusCode',
    lastSSN: 'socialSecurityNumber',
    memberSequenceIdentifier: 'memberSequenceIdentifier',
    nextPaymentDueDate: 'nextPaymentDueDate',
    openDate: '{openDayDate}-{openDate}',
    paymentDueAmount: 'currentMinimumPaymentDueAmount',
    primaryName: 'customerName',
    secondaryName: 'secondaryName',
    socialSecurityIdentifier: 'socialSecurityIdentifier'
  };

  it('returns empty > mapping is empty', () => {
    const customers = [{ accountIdentifier: 'accId1' }];

    const result = handleMapRelatedAccounts(customers, {});

    expect(result).toEqual([]);
  });

  it('returns empty > return from catch', () => {
    const customers = [{ accountIdentifier: 'accId1' }];

    const result = handleMapRelatedAccounts(customers, mapping);

    expect(result).toEqual([]);
  });

  it('returns data with accountId is empty', () => {
    const customers = [{ accountIdentifier: '' }];

    const result = handleMapRelatedAccounts(customers, mapping);

    expect(result).toEqual([
      {
        accountId: '',
        address: { addressLineOne: undefined, addressLineTwo: undefined },
        availableCreditAmount: undefined,
        balanceAmount: undefined,
        cardNbr: undefined,
        cityName: undefined,
        creditLimitAmount: undefined,
        customerRoleTypeCode: undefined,
        cycleCode: undefined,
        cycleCounts: undefined,
        daysDelinquent: undefined,
        delinquentAmount: undefined,
        externalStatusCode: undefined,
        id: undefined,
        internalStatusCode: undefined,
        lastSSN: undefined,
        memberSequenceIdentifier: undefined,
        nextPaymentDueDate: undefined,
        openDate: '-',
        paymentDueAmount: undefined,
        primaryName: undefined,
        secondaryName: undefined,
        socialSecurityIdentifier: undefined,
        accNbr: undefined
      }
    ]);
  });

  it('returns data with accountIdentifier and socialSecurityNumber are having maskedValue', () => {
    const customers = [
      {
        accountIdentifier: JSON.stringify({ maskedValue: 'abc' }),
        socialSecurityNumber: [JSON.stringify({ maskedValue: '123-456-7890' })]
      }
    ];

    const result = handleMapRelatedAccounts(customers, mapping);

    expect(result).toEqual([
      {
        accountId: '{"maskedValue":"abc"}',
        address: { addressLineOne: undefined, addressLineTwo: undefined },
        availableCreditAmount: undefined,
        balanceAmount: undefined,
        cardNbr: undefined,
        cityName: undefined,
        creditLimitAmount: undefined,
        customerRoleTypeCode: undefined,
        cycleCode: undefined,
        cycleCounts: undefined,
        daysDelinquent: undefined,
        delinquentAmount: undefined,
        externalStatusCode: undefined,
        id: undefined,
        internalStatusCode: undefined,
        lastSSN: '7890',
        memberSequenceIdentifier: undefined,
        nextPaymentDueDate: undefined,
        openDate: '-',
        paymentDueAmount: undefined,
        primaryName: undefined,
        secondaryName: undefined,
        socialSecurityIdentifier: undefined,
        accNbr: 'abc'
      }
    ]);
  });

  it('returns data with > socialSecurityNumber not having maskedValue', () => {
    const customers = [
      {
        accountIdentifier: JSON.stringify({ maskedValue: 'abc' }),
        socialSecurityNumber: [JSON.stringify({ eValue: 'abc' })]
      }
    ];

    const result = handleMapRelatedAccounts(customers, mapping);

    expect(result).toEqual([
      {
        accountId: '{"maskedValue":"abc"}',
        address: { addressLineOne: undefined, addressLineTwo: undefined },
        availableCreditAmount: undefined,
        balanceAmount: undefined,
        cardNbr: undefined,
        cityName: undefined,
        creditLimitAmount: undefined,
        customerRoleTypeCode: undefined,
        cycleCode: undefined,
        cycleCounts: undefined,
        daysDelinquent: undefined,
        delinquentAmount: undefined,
        externalStatusCode: undefined,
        id: undefined,
        internalStatusCode: undefined,
        lastSSN: ['{"eValue":"abc"}'],
        memberSequenceIdentifier: undefined,
        nextPaymentDueDate: undefined,
        openDate: '-',
        paymentDueAmount: undefined,
        primaryName: undefined,
        secondaryName: undefined,
        socialSecurityIdentifier: undefined,
        accNbr: 'abc'
      }
    ]);
  });
});
