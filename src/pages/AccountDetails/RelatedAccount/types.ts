import { AxiosResponse } from 'axios';
import { CallerAuthenticationData } from 'pages/CallerAuthenticate/types';

export interface RelatedAccountData {
  accountId?: string;
  memberSequenceIdentifier?: string;
  accNbr?: string | null;
  address?: {
    addressLineOne?: string;
    addressLineTwo?: string;
  };
  cityName?: string;
  id?: string;
  lastSSN?: string | null;
  primaryName?: string;
  secondaryName?: string;
  clientId?: string;
  customerRoleTypeCode?: string;
  cycleCounts?: number;
  delinquentAmount?: number;
  internalStatusCode?: string;
  externalStatusCode?: string;
  daysDelinquent?: number;
  openDate?: string;
  balanceAmount?: string;
  availableCreditAmount?: string;
  paymentDueAmount?: string;
  cycleCode?: string;
  creditLimitAmount?: string;
  paymentDueDate?: string;
  socialSecurityIdentifier?: string;
}

export interface RelatedAccountState {
  relatedAcc?: Array<RelatedAccountData>;
  isLoading?: boolean;
  selectedCaller?: CallerAuthenticationData;
}

export interface RelatedAccountStateResult {
  [storeId: string]: RelatedAccountState | undefined;
}

// Trigger open related account
export interface TriggerOpenRelatedAccountArgs {
  translateFn: Function;
}

export interface SetRelatedSelectedCallerArgs extends StoreIdPayload {
  data: CallerAuthenticationData;
}

// Related account
export interface RelatedAccountRequest extends CommonAccountIdRequestBody {
  relationshipSearchText?: string;
  relationshipClientIdentifier?: string;
}

// Get related account
export interface GetRelatedAccountArgs extends StoreIdPayload {
  socialSecurityIdentifier?: string;
  accountId: string;
}

export interface GetRelatedAccountPayload {
  accounts?: Array<RelatedAccountData>;
}

export interface AccountInfo {
  accNbr?: string;
  agentId?: string;
  clientID?: string;
  cycleCode?: string;
  cycleDate?: string;
  externalStatusCode?: string;
  institutionName?: string;
  internalStatusCode?: string;
  ssn?: string;
  lastSSN?: string;
  primaryName?: string;
  principalId?: string;
  secondaryName?: string;
  statusDate?: string;
  sysId?: string;
  sysPrinAgent?: string;
  nextHeldStatementDestinationCode?: string;
  relatedAccounts?: number;
  cardHolderSecondaryName?: string;
  namePrincipalCardholder?: string;
  separateEntityIndicator?: string;
  memberSequenceIdentifier?: string;
  customerRoleTypeCode?: string;
  socialSecurityIdentifier?: string;
}

export interface AccountRelatedResponse {
  customerInquiry?: Array<CustomerInquiry>;
  cardholderInfo?: Array<CardholderInfo>;
  collectionStatus?: CollectionStatus;
}

export interface CollectionStatus {
  promiseToPayAmount?: string;
  promiseToPayDate?: string;
}

export interface CardholderInfo {
  namePrincipalCardholder?: string;
  secondaryName?: string;
}

export interface CustomerInquiry {
  accountIdentifier?: string;
  agentBankName?: string;
  agentIdentifier?: string;
  availableCreditAmount?: string;
  behaviorScoreIdentifier?: string;
  billingCycleCode?: string;
  clientIdentifier?: string;
  collectionsEntryDate?: string;
  creditBureauScoreNumber?: string;
  creditLimitAmount?: string;
  currentBalanceAmount?: string;
  currentMinimumPaymentDueAmount?: string;
  customerExternalIdentifier?: string;
  customerRoleTypeCode?: string;
  daysDelinquentCount?: string;
  delinquentAmount?: number;
  externalStatusCode?: string;
  internalStatusCode?: string;
  lastMonetaryDate?: string;
  lastMonetaryType?: string;
  lastPaymentAmount?: string;
  lastPaymentDate?: string;
  lastReageDate?: string;
  lastStatementDate?: string;
  lastStatusCodeChangeDate?: string;
  memberSequenceIdentifier?: string;
  nextHeldStatementDestinationCode?: string;
  openDate?: string;
  paymentDueDate?: string;
  plasticReplacementIndicator?: string;
  presentationInstrumentIdentifier?: string;
  presentationInstrumentReplacementSequenceNumber?: string;
  presentationInstrumentTypeCode?: string;
  primaryCustomerName?: string;
  principalIdentifier?: string;
  reageCount?: string;
  separateEntityIndicator?: string;
  socialSecurityIdentifier?: string;
  systemIdentifier?: string;
  customerName?: string;
}

// Service
export interface RelatedAccount {
  accountIdentifier?: string;
  convertedEnteredIdentifier?: string;
  enteredIdentifier?: string;
  accountNumber?: string;
  agentIdentifier?: string;
  lastUpdateDate?: string;
  principalIdentifier?: string;
  relationshipAccountIdentifier?: string;
  relationshipAccountNumber?: string;
  relationshipClientIdentifier?: string;
  relationshipClientNumber?: string;
  relationshipSearchText?: string;
  relationshipTypeCode?: string;
  systemIdentifier?: string;
}

export interface GetRelatedAccountResponse {
  accounts?: Array<RelatedAccount>;
}

// Get detail of related account type
export type ResponseRelatedResult = PromiseSettledResult<
  AxiosResponse<AccountRelatedResponse>
>;
export type ResponseRelated = AxiosResponse<AccountRelatedResponse>;
export type ListRelatedPromise = Array<Promise<ResponseRelated>>;
