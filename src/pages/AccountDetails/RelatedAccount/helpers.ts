// Const
import { DASH, EMPTY_STRING_OBJECT, EMPTY_STRING } from 'app/constants';

// Helpers
import { mappingDataFromArray } from 'app/helpers';

// Types
import { AccountData } from 'pages/AccountSearch/AccountSearchList/types';
import { CustomerInquiry } from '../types';

export const handleMapRelatedAccounts = (
  customers: CustomerInquiry[],
  mappingData: Record<string, string>
) => {
  try {
    const listCustomerMapped = mappingDataFromArray<AccountData>(
      customers,
      mappingData
    );

    listCustomerMapped.forEach(customerMapped => {
      const { maskedValue } = JSON.parse(
        customerMapped?.accountId || EMPTY_STRING_OBJECT
      ) as CommonIdentifier;

      customerMapped.accNbr = maskedValue;

      if (customerMapped?.lastSSN?.length) {
        const { maskedValue: ssn = EMPTY_STRING } = JSON.parse(
          customerMapped?.lastSSN
        ) as CommonIdentifier;
        const [, , lastSSN = customerMapped.lastSSN] = ssn.split(DASH);
        customerMapped.lastSSN = lastSSN;
      }
    });

    return listCustomerMapped;
  } catch (e) {
    return [];
  }
};
