import React, { useCallback } from 'react';
import { View } from 'app/_libraries/_dof/core';
import FailedApiReload from 'app/components/FailedApiReload';

// hooks
import { useStoreIdSelector, useAccountDetail } from 'app/hooks';

// redux
import { useDispatch } from 'react-redux';
import { accountDetailActions } from '../_redux/reducer';

// selector
import {
  takeMonetaryData,
  takeHistoricalData,
  takeAccountDetailErrorStatus
} from '../_redux/selectors';

// type
import { MonetaryData, HistoricalData } from '../types';

// constants
import { VIEW_NAME } from './constants';

// helper
import { formatCommon } from 'app/helpers';
import classNames from 'classnames';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

const Overview: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const {
    storeId,
    accEValue,
    memberSequenceIdentifier,
    socialSecurityIdentifier
  } = useAccountDetail();

  const monetaryData = useStoreIdSelector<MonetaryData>(takeMonetaryData);
  const historicalData = useStoreIdSelector<HistoricalData>(takeHistoricalData);
  const isError = useStoreIdSelector<Boolean>(takeAccountDetailErrorStatus);
  const testId = 'account-detail-overview-tab';

  const handleReload = useCallback(() => {
    dispatch(
      accountDetailActions.getAccountDetail({
        storeId,
        postData: {
          common: {
            accountId: accEValue
          },
          inquiryFields: {
            memberSequenceIdentifier
          }
        },
        socialSecurityIdentifier,
        isReload: true
      })
    );
  }, [
    dispatch,
    storeId,
    accEValue,
    memberSequenceIdentifier,
    socialSecurityIdentifier
  ]);

  if (isError) {
    return (
      <FailedApiReload
        id="account-detail-overview-fail"
        className={classNames('mt-80')}
        onReload={handleReload}
        dataTestId={`${testId}_api-reload-failed`}
      />
    );
  }

  return (
    <div className={classNames('p-24')}>
      <h4 data-testid={genAmtId(testId, 'title', '')}>{t('txt_overview')}</h4>
      {checkPermission(
        PERMISSIONS.OVERVIEW_MONETARY_INFORMATION_VIEW,
        storeId
      ) && (
        <>
          <h5
            className="mt-16"
            data-testid={genAmtId(testId, 'monetary-information_title', '')}
          >
            {t('txt_monetary_information')}
          </h5>
          <div className="border br-light-l04 bg-light-l20 br-radius-8 p-16 mt-16">
            <div className="row">
              <div className="col-xl-4 col-md-6">
                <strong data-testid={genAmtId(testId, 'amountDue_label', '')}>
                  {t('txt_amount_due')}:{' '}
                </strong>
                <span
                  className="content-empty"
                  data-testid={genAmtId(testId, 'amountDue_value', '')}
                >
                  {formatCommon(monetaryData?.amountDue ?? '').currency()}
                </span>
              </div>
              <div className="col-xl-4 col-md-6">
                <strong
                  data-testid={genAmtId(testId, 'daysDelinquent_label', '')}
                >
                  {t('txt_days_delinquent')}:{' '}
                </strong>
                <span
                  className="content-empty"
                  data-testid={genAmtId(testId, 'daysDelinquent_value', '')}
                >
                  {formatCommon(monetaryData?.daysDelinquent ?? '').number}
                </span>
              </div>
              <div className="col-xl-4 col-md-12 mt-md-8 mt-xl-0">
                <strong
                  data-testid={genAmtId(testId, 'paymentDueDate_label', '')}
                >
                  {t('txt_paymentDueDate')}:{' '}
                </strong>
                <span
                  className="content-empty"
                  data-testid={genAmtId(testId, 'paymentDueDate_value', '')}
                >
                  {formatCommon(monetaryData?.paymentDueDate ?? '').time.date}
                </span>
              </div>
            </div>
          </div>
          <View
            id={`${storeId}_${VIEW_NAME.monetary}`}
            formKey={`${storeId}_${VIEW_NAME.monetary}_view`}
            descriptor="overviewInfomation"
            value={{
              ...monetaryData,
              existingPromiseToPayAmount:
                Number(monetaryData?.existingPromiseToPayAmount) < 0
                  ? undefined
                  : monetaryData?.existingPromiseToPayAmount
            }}
          />
        </>
      )}
      {checkPermission(
        PERMISSIONS.OVERVIEW_HISTORICAL_INFORMATION_VIEW,
        storeId
      ) && (
        <>
          <h5 data-testid={genAmtId(testId, 'historical-info_title', '')}>
            {t('txt_historical_information')}
          </h5>
          <View
            id={`${storeId}_${VIEW_NAME.historical}`}
            formKey={`${storeId}_${VIEW_NAME.historical}_view`}
            descriptor={VIEW_NAME.historical}
            value={historicalData}
          />
        </>
      )}
    </div>
  );
};

export default Overview;
