import { apiService } from 'app/utils/api.service';
// constants

const accountDetailService = {
  getAccountDetails: (bodyRequest: MagicKeyValue) => {
    const url = window.appConfig?.api?.account?.getAccountDetails || '';
    return apiService.post(url, { ...bodyRequest });
  }
};
export default accountDetailService;
