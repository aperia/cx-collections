import accountDetailService from './accountDetailService';

// utils
import { storeId, mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

describe('accountDetailService', () => {
  describe('getAccountDetails', () => {
    const params: MagicKeyValue = { storeId };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      accountDetailService.getAccountDetails(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      accountDetailService.getAccountDetails(params);

      expect(mockService).toBeCalledWith(
        apiUrl.account.getAccountDetails,
        params
      );
    });
  });
});
