import React from 'react';

import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import Overview from './index';
import { accountDetailActions } from '../_redux/reducer';

jest.mock('app/entitlements', () => ({
  checkPermission: () => true
}));

const actionSpy = mockActionCreator(accountDetailActions);

describe('Test Account Detail Overview', () => {
  const initialState: Partial<RootState> = {
    accountDetail: {
      [storeId]: {
        isError: false,
        data: {
          monetary: {
            amountDelinquent: 'amountDelinquent',
            amountDue: 'amountDue',
            availableCredit: 'availableCredit',
            creditLimit: 'creditLimit',
            currentBalance: 'currentBalance',
            daysDelinquent: 'daysDelinquent',
            existingPromiseToPay: 'existingPromiseToPay',
            existingPromiseToPayAmount: '-10',
            paymentDueDate: '11/11/2020'
          },
          historical: {
            behaviorScore: 'behaviorScore',
            collectionEntryDate: 'collectionEntryDate',
            creditBureauScore: 'creditBureauScore',
            lastMonetaryDate: 'lastMonetaryDate',
            lastMonetaryType: 'lastMonetaryType',
            lastPaymentAmount: 'lastPaymentAmount',
            lastPaymentDate: 'lastPaymentDate',
            lastReageDate: 'lastReageDate',
            openDate: 'openDate',
            reageCount: 'reageCount'
          }
        }
      }
    }
  };

  it('render UI', () => {
    const { wrapper } = renderMockStoreId(<Overview />, { initialState });
    expect(wrapper.getByText('txt_overview')).toBeTruthy();
  });

  it('render UI with empty monetary', () => {
    const { wrapper } = renderMockStoreId(<Overview />, {
      initialState: {
        accountDetail: {
          [storeId]: {
            isError: false,
            data: {
              monetary: {},
              historical: {
                behaviorScore: 'behaviorScore',
                collectionEntryDate: 'collectionEntryDate',
                creditBureauScore: 'creditBureauScore',
                lastMonetaryDate: 'lastMonetaryDate',
                lastMonetaryType: 'lastMonetaryType',
                lastPaymentAmount: 'lastPaymentAmount',
                lastPaymentDate: 'lastPaymentDate',
                lastReageDate: 'lastReageDate',
                openDate: 'openDate',
                reageCount: 'reageCount'
              }
            }
          }
        }
      }
    });
    expect(wrapper.getByText('txt_overview')).toBeInTheDocument();
  });

  it('handleReload', () => {
    const spy = actionSpy('getAccountDetail');
    const { wrapper } = renderMockStoreId(<Overview />, {
      initialState: {
        accountDetail: {
          [storeId]: {
            isError: true,
            data: {}
          }
        }
      }
    });

    expect(wrapper.getByText('txt_failure_ocurred_on')).toBeInTheDocument();

    const button = wrapper.getByText('txt_reload')!;

    button.click();

    expect(spy).toBeCalledWith({
      storeId,
      postData: {
        common: {
          accountId: 'accEValue'
        },
        inquiryFields: {
          memberSequenceIdentifier: 'memberSequenceIdentifier'
        }
      },
      socialSecurityIdentifier: 'socialSecurityIdentifier',
      isReload: true
    });
  });
});
