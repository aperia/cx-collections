export const VIEW_NAME = {
  monetary: 'overviewInfomation',
  historical: 'accountDetailSnapshotHistoricalInformation'
};

export const OVERVIEW_CONFIG = 'config/overview.json';
