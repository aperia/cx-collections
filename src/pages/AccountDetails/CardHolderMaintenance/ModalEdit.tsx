import React from 'react';

// components
import {
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';
import { View } from 'app/_libraries/_dof/core';

// redux
import { useDispatch, useSelector } from 'react-redux';
import {
  selectCardholderId,
  selectEmailInfo,
  selectLoadingModal,
  selectModalEdit,
  selectTelePhoneFormInfo,
  selectTypeEdit
} from 'pages/AccountDetails/CardHolderMaintenance/_redux/selector';
import { cardholderMaintenanceActions } from 'pages/AccountDetails/CardHolderMaintenance/_redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import {
  useAccountDetail,
  useIsDirtyForm,
  useStoreIdSelector
} from 'app/hooks';

// helpers
import isEmpty from 'lodash.isempty';
import { formatPostData } from './TelePhone/helpers';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import classNames from 'classnames';

// constants
import { TYPE_EDIT } from './TelePhone/constants';
import {
  VIEW_NAME_CARD_MAINTENANCE,
  I18N_CARDHOLDER_MAINTENANCE
} from './constants';

// types
import { ICardholderId } from './types';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { isDirty } from 'redux-form';

export interface ModalEditProps {}

const ModalEdit: React.FC = () => {
  const dispatch = useDispatch();
  const { storeId, accEValue } = useAccountDetail();
  const { t } = useTranslation();

  const isTelephoneDirty = useSelector(
    isDirty(`${storeId}_telephone_edit_view`)
  );

  const show = useStoreIdSelector<boolean>(selectModalEdit);
  const type = useStoreIdSelector<string>(selectTypeEdit);
  const dataTelephone = useStoreIdSelector(selectTelePhoneFormInfo);
  const dataEmail = useStoreIdSelector(selectEmailInfo);
  const cardholderId = useStoreIdSelector<ICardholderId>(selectCardholderId);
  const loading = useStoreIdSelector<boolean>(selectLoadingModal);

  const formKey = `${storeId}_${type}_edit_view`;

  const isDirtyForm = useIsDirtyForm([formKey]);

  const formData = useSelector<RootState, MagicKeyValue>(
    state => state?.form[formKey]
  );

  const title =
    type === TYPE_EDIT.PHONE
      ? t(I18N_CARDHOLDER_MAINTENANCE.EDIT_PHONE)
      : t(I18N_CARDHOLDER_MAINTENANCE.EDIT_EMAIL);

  const onCloseModal = () =>
    dispatch(
      cardholderMaintenanceActions.updateModalEdit({
        storeId,
        open: false,
        type
      })
    );

  const onInvokeCloseModal = () => {
    if (type === TYPE_EDIT.PHONE && isTelephoneDirty) {
      return dispatch(
        confirmModalActions.open({
          title: 'txt_unsaved_changes',
          body: 'txt_discard_change_body',
          cancelText: 'txt_continue_editing',
          confirmText: 'txt_discard_change',
          confirmCallback: onCloseModal,
          variant: 'danger'
        })
      );
    }
    onCloseModal();
  };

  const submitForm = () => {
    const postData =
      type === TYPE_EDIT.EMAIL
        ? formData?.values
        : formatPostData(formData?.values);

    dispatch(
      cardholderMaintenanceActions.triggerUpdateCardHolder({
        storeId,
        accEValue,
        type,
        postData: {
          common: {
            accountId: accEValue,
            externalCustomerId: cardholderId?.id
          },
          data: {
            ...postData
          }
        }
      })
    );
  };

  const nameView =
    type === TYPE_EDIT.PHONE
      ? VIEW_NAME_CARD_MAINTENANCE.EDIT_PHONE
      : VIEW_NAME_CARD_MAINTENANCE.EDIT_EMAIL;

  const value = type === TYPE_EDIT.PHONE ? dataTelephone : dataEmail;

  const phone = type === TYPE_EDIT.PHONE;
  const email = type === TYPE_EDIT.EMAIL;

  return (
    <Modal
      lg={phone}
      sm={email}
      className={classNames(phone && 'window-lg-full')}
      show={show}
      loading={loading}
      dataTestId={`${nameView}_modal`}
    >
      <ModalHeader
        border
        closeButton
        onHide={onInvokeCloseModal}
        dataTestId={`${nameView}_modal-header`}
      >
        <ModalTitle dataTestId={`${nameView}_modal-title`}>{title}</ModalTitle>
      </ModalHeader>
      <ModalBodyWithApiError
        storeId={storeId}
        apiErrorClassName="mb-24"
        dataTestId={`${nameView}_modal-body`}
      >
        <View
          id={`${storeId}_${type}_edit_view`}
          formKey={`${storeId}_${type}_edit_view`}
          descriptor={nameView}
          value={value}
        />
      </ModalBodyWithApiError>
      <ModalFooter
        okButtonText={t(I18N_COMMON_TEXT.SAVE)}
        cancelButtonText={t(I18N_COMMON_TEXT.CANCEL)}
        onCancel={onInvokeCloseModal}
        onOk={submitForm}
        disabledOk={!isEmpty(formData?.syncErrors) || !isDirtyForm}
        dataTestId={`${nameView}_modal-footer`}
      />
    </Modal>
  );
};

export default ModalEdit;
