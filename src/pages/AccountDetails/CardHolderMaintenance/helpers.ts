import { formatTimeDefault } from 'app/helpers';
import {
  IAddressInfo,
  ICardholderInfo,
  StandardAddressForCompare
} from './types';
import { Dispatch } from '@reduxjs/toolkit';
import {
  ExpandState,
  IChangeExpand
} from 'pages/AccountDetails/CardHolderMaintenance/types';
import { cardholderMaintenanceActions } from './_redux';
import { CARD_ROLE } from 'app/constants';
import { FormatTime } from 'app/constants/enums';
import isEmpty from 'lodash.isempty';

export const handleToggle: AppThunk =
  (expandState: ExpandState, storeId: string) => async (dispatch: Dispatch) => {
    const data: IChangeExpand = {
      expandState,
      storeId
    };
    dispatch(cardholderMaintenanceActions.changeExpand(data));
  };

export const prepareCardholderInfoViewData = (
  data: ICardholderInfo,
  t: Function
) => ({
  ...data,
  cardholderRole: data?.cardholderRole ? t(CARD_ROLE[data?.cardholderRole]) : ''
});

export const generateLabel = (
  value: IAddressInfo,
  isDisplayDate = false,
  translateFn: Function
) => {
  const formatEffectiveFrom = formatTimeDefault(
    value?.effectiveFrom,
    FormatTime.Date
  );
  const formatEffectiveTo = formatTimeDefault(
    value?.effectiveTo,
    FormatTime.Date
  );
  // txt_p_permanent => txt_permanent
  const translateData = (text?: string) => {
    if (!text) return '';
    return translateFn(
      text
        .split('_')
        .filter((_, index) => index !== 1)
        .join('_')
    );
  };
  const result = `${translateData(
    value?.addressCategory?.description
  )} ${translateData(value?.addressType?.description)}`;

  if (!isDisplayDate) return result;

  return `${result} • ${formatEffectiveFrom} - ${formatEffectiveTo}`;
};

export const handleGenerateValuesForMemo = (
  oldValues: StandardAddressForCompare,
  newValues: StandardAddressForCompare
) => {
  const result = [];

  if (oldValues?.addressLineOne !== newValues?.addressLineOne) {
    const oldValue = oldValues?.addressLineOne;
    const newValue = newValues?.addressLineOne;

    result.push(getStringMemo(oldValue, newValue, 'Address Line 1'));
  }
  if (oldValues?.addressLineTwo !== newValues?.addressLineTwo) {
    const oldValue = oldValues?.addressLineTwo;
    const newValue = newValues?.addressLineTwo;

    result.push(getStringMemo(oldValue, newValue, 'Address Line 2'));
  }
  if (oldValues?.cityName !== newValues?.city) {
    const oldValue = oldValues?.cityName;
    const newValue = newValues?.city;

    result.push(getStringMemo(oldValue, newValue, 'City'));
  }
  if (
    oldValues?.stateSubdivision?.description !==
    newValues?.stateSubdivision?.description
  ) {
    const oldValue = oldValues?.stateSubdivision?.description;
    const newValue = newValues?.stateSubdivision?.description;

    result.push(getStringMemo(oldValue, newValue, 'State/Subdivision'));
  }
  if (oldValues?.postalCode !== newValues?.postalCode) {
    const oldValue = oldValues?.postalCode;
    const newValue = newValues?.postalCode;

    result.push(getStringMemo(oldValue, newValue, 'Postal Code'));
  }

  return result.join(', ');
};

export const generateMemoAddressValue = (address?: IAddressInfo) => {
  if (isEmpty(address) || !address) return '';
  let textMemo = `CATEGORY ${address.addressCategory.fieldID} - ${
    address.addressCategory.fieldValue
  }, ADDRESS TYPE ${address.addressType.fieldID} - ${
    address.addressType.fieldValue
  }, EFFECTIVE FROM ${formatTimeDefault(
    address.effectiveFrom,
    FormatTime.Date
  )}, EFFECTIVE TO ${formatTimeDefault(
    address.effectiveTo,
    FormatTime.Date
  )}, COUNTRY ${address.countryISO.description ?? ''}`;

  if (!isEmpty(address?.attention)) {
    textMemo = textMemo + `, ATTENTION ${address.attention}`;
  }
  if (!isEmpty(address?.houseBuilding)) {
    textMemo = textMemo + `, HOUSE/BUILDING ${address.houseBuilding}`;
  }
  if (!isEmpty(address?.streetName)) {
    textMemo = textMemo + `, STREET NAME ${address.streetName}`;
  }
  if (!isEmpty(address?.houseNumber)) {
    textMemo = textMemo + `, HOUSE NUMBER ${address?.houseNumber}`;
  }
  if (!isEmpty(address?.POBox)) {
    textMemo = textMemo + `, PO BOX ${address.POBox}`;
  }
  textMemo =
    textMemo +
    `, ADDRESS RELATIONSHIP ${address.addressRelationship.fieldID} - ${address.addressRelationship.fieldValue}, ADDRESS LINE 1 ${address.addressLineOne}`;

  if (!isEmpty(address?.addressLineTwo)) {
    textMemo = textMemo + `, ADDRESS LINE 2 ${address.addressLineTwo}`;
  }
  if (!isEmpty(address?.addressLineThree)) {
    textMemo = textMemo + `, ADDRESS LINE 3 ${address.addressLineThree}`;
  }
  if (!isEmpty(address?.addressLineFour)) {
    textMemo = textMemo + `, ADDRESS LINE 4 ${address.addressLineFour}`;
  }
  textMemo = textMemo + `, CITY ${address.cityName}`;
  if (!isEmpty(address.stateSubdivision)) {
    textMemo = textMemo + `, STATE ${address.stateSubdivision?.description}`;
  }
  textMemo = textMemo + `, ZIP CODE ${address.postalCode}`;
  return textMemo;
};

const getStringMemo = (
  oldValue?: string,
  newValue?: string,
  label?: string
) => {
  const _newValue = isEmpty(newValue) ? '' : newValue;
  const _oldValue = isEmpty(oldValue) ? '' : oldValue;
  return `${label} changed from ${_oldValue} to ${_newValue}`;
};

export const generateMemoEditAddressValue = (address: MagicKeyValue) => {
  if (isEmpty(address) || !address) return '';
  const result = [];

  if (address?.addressCategory) {
    const newValue = address?.addressCategory?.new?.description;
    const oldValue = address?.addressCategory?.old?.description;

    result.push(getStringMemo(oldValue, newValue, 'CATEGORY'));
  }
  if (address?.addressType) {
    const newValue = address?.addressType?.new?.description;
    const oldValue = address?.addressType?.old?.description;
    result.push(getStringMemo(oldValue, newValue, 'Address Type'));
  }
  if (address?.effectiveFrom) {
    const newValue = formatTimeDefault(
      address?.effectiveFrom?.new,
      FormatTime.Date
    );
    const oldValue = formatTimeDefault(
      address?.effectiveFrom?.old,
      FormatTime.Date
    );
    result.push(getStringMemo(oldValue, newValue, 'Effective From'));
  }
  if (address?.effectiveTo) {
    const newValue = formatTimeDefault(
      address?.effectiveTo?.new,
      FormatTime.Date
    );
    const oldValue = formatTimeDefault(
      address?.effectiveTo?.old,
      FormatTime.Date
    );
    result.push(getStringMemo(oldValue, newValue, 'Effective To'));
  }
  if (address?.countryISO) {
    const newValue = address?.countryISO?.new?.description;
    const oldValue = address?.countryISO?.old?.description;

    result.push(getStringMemo(oldValue, newValue, 'COUNTRY'));
  }
  if (address?.attention) {
    const newValue = address?.attention?.new;
    const oldValue = address?.attention?.old;

    result.push(getStringMemo(oldValue, newValue, 'Attention'));
  }
  if (address?.houseBuilding) {
    const newValue = address?.houseBuilding?.new;
    const oldValue = address?.houseBuilding?.old;

    result.push(getStringMemo(oldValue, newValue, 'HOUSE/BUILDING'));
  }
  if (address?.streetName) {
    const newValue = address?.streetName?.new;
    const oldValue = address?.streetName?.old;

    result.push(getStringMemo(oldValue, newValue, 'Street Name'));
  }
  if (address?.houseNumber) {
    const newValue = address?.houseNumber?.new;
    const oldValue = address?.houseNumber?.old;

    result.push(getStringMemo(oldValue, newValue, 'House Number'));
  }
  if (address?.POBox) {
    const newValue = address?.POBox?.new;
    const oldValue = address?.POBox?.old;

    result.push(getStringMemo(oldValue, newValue, 'PO Box'));
  }
  if (address?.addressRelationship) {
    const newValue = address?.addressRelationship?.new?.description;
    const oldValue = address?.addressRelationship?.old?.description;

    result.push(getStringMemo(oldValue, newValue, 'Address Relationship'));
  }
  if (address?.addressLineOne) {
    const newValue = address?.addressLineOne?.new;
    const oldValue = address?.addressLineOne?.old;

    result.push(getStringMemo(oldValue, newValue, 'Address Line 1'));
  }
  if (address?.addressLineTwo) {
    const newValue = address?.addressLineTwo?.new;
    const oldValue = address?.addressLineTwo?.old;

    result.push(getStringMemo(oldValue, newValue, 'Address Line 2'));
  }
  if (address?.addressLineThree) {
    const newValue = address?.addressLineThree?.new;
    const oldValue = address?.addressLineThree?.old;

    result.push(getStringMemo(oldValue, newValue, 'Address Line 3'));
  }
  if (address?.addressLineFour) {
    const newValue = address?.addressLineFour?.new;
    const oldValue = address?.addressLineFour?.old;

    result.push(getStringMemo(oldValue, newValue, 'Address Line 4'));
  }
  if (address?.cityName) {
    const newValue = address?.cityName?.new;
    const oldValue = address?.cityName?.old;

    result.push(getStringMemo(oldValue, newValue, 'City'));
  }
  if (address?.stateSubdivision) {
    const newValue = address?.stateSubdivision?.new?.description;
    const oldValue = address?.stateSubdivision?.old?.description;

    result.push(getStringMemo(oldValue, newValue, 'STATE'));
  }
  if (address?.postalCode) {
    const newValue = address?.postalCode?.new;
    const oldValue = address?.postalCode?.old;

    result.push(getStringMemo(oldValue, newValue, 'ZIP CODE'));
  }

  return result.join(', ');
};

export const translateAddressInfo = (
  addressInfo: IAddressInfo,
  translateFn: Function
): IAddressInfo => {
  if (!addressInfo) return addressInfo;
  return {
    ...addressInfo,
    addressCategory: {
      ...addressInfo.addressCategory,
      description: translateFn(addressInfo.addressCategory?.description)
    },
    addressType: {
      ...addressInfo.addressType,
      description: translateFn(addressInfo.addressType?.description)
    },
    addressRelationship: {
      ...addressInfo.addressRelationship,
      description: translateFn(addressInfo.addressRelationship?.description)
    }
  };
};
