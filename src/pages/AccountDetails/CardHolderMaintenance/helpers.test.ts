import { createStore } from '@reduxjs/toolkit';
import { FormatTime } from 'app/constants/enums';
import { formatTimeDefault } from 'app/helpers';
import { mockActionCreator, storeId } from 'app/test-utils';
import {
  generateLabel,
  generateMemoAddressValue,
  handleGenerateValuesForMemo,
  handleToggle,
  generateMemoEditAddressValue,
  prepareCardholderInfoViewData
} from './helpers';
import { IAddressInfo, ICardholderInfo } from './types';
import { rootReducer } from 'storeConfig';
import { cardholderMaintenanceActions } from './_redux';

const spyActions = mockActionCreator(cardholderMaintenanceActions);

const translateFn = (value: string) => value;

const value: ICardholderInfo = {
  authCBRFlag: 'Do not report',
  cardNumber: '603530******0985',
  cardholderRole: '01',
  dateOfBirth: '********0001',
  embossingName: '0000000000000000',
  firstName: '',
  lastName: '',
  memberSequence: '00002',
  middleName: '',
  prefix: '',
  qualification: '',
  salutation: 'Unknown Gender',
  solicitation: 'Solicitation Allowed',
  ssnTaxId: '',
  suffix: '',
  title: ''
};

const addressInfo: IAddressInfo = {
  POBox: '',
  addressCategory: {
    fieldValue: 'Permanent',
    fieldID: 'P',
    description: 'txt_p_permanent'
  },
  addressLineFour: '',
  addressLineOne: '333 MENLAY',
  addressLineThree: '',
  addressLineTwo: '',
  addressRelationship: {
    fieldValue: 'Unknown',
    fieldID: 'U',
    description: 'U - Unknown'
  },
  addressType: {
    fieldValue: 'Billing',
    fieldID: 'BLL1',
    description: 'txt_bll1_billing'
  },
  attention: '',
  cityName: 'OMAHA',
  countryISO: {
    fieldValue: 'United States of America',
    fieldID: 'USA',
    description: 'USA - United States of America'
  },
  effectiveFrom: '2021/05/18',
  effectiveTo: '9999/12/31',
  formattedAddressCode: 'F',
  houseBuilding: '',
  houseNumber: '',
  id: '00001-P-BLL1-2021-05-18-9999-12-31',
  memberSequence: '00001',
  postalCode: '10003',
  stateSubdivision: '' as any,
  streetName: ''
};

const addressInfo1: IAddressInfo = {
  POBox: '11111',
  addressCategory: {
    fieldValue: 'Permanent',
    fieldID: 'P',
    description: 'P - Permanent'
  },
  addressLineFour: '1212',
  addressLineOne: '333 MENLAY',
  addressLineThree: '12121',
  addressLineTwo: '12121',
  addressRelationship: {
    fieldValue: 'Unknown',
    fieldID: 'U',
    description: 'U - Unknown'
  },
  addressType: {
    fieldValue: 'Billing',
    fieldID: 'BLL1',
    description: 'BLL1 - Billing'
  },
  attention: '12121',
  cityName: 'OMAHA',
  countryISO: {
    fieldValue: 'United States of America',
    fieldID: 'USA',
    description: 'USA - United States of America'
  },
  effectiveFrom: '2021/05/18',
  effectiveTo: '9999/12/31',
  formattedAddressCode: 'F',
  houseBuilding: '12121',
  houseNumber: '12121',
  id: '00001-P-BLL1-2021-05-18-9999-12-31',
  memberSequence: '00001',
  postalCode: '10003',
  stateSubdivision: {
    fieldValue: 'New York',
    fieldID: 'NY',
    description: 'NY - New York'
  },
  streetName: '121212'
};

describe('Test Helpers', () => {
  it('Test prepareCardholderInfoViewData should have cardholderRole Primary', () => {
    const dataReturn = prepareCardholderInfoViewData(value, translateFn);
    expect(dataReturn?.cardholderRole).toEqual('txt_primary');
  });

  it('Test prepareCardholderInfoViewData should have cardholderRole Secondary', () => {
    const dataReturn = prepareCardholderInfoViewData(
      {
        ...value,
        cardholderRole: '02'
      },
      translateFn
    );
    expect(dataReturn?.cardholderRole).toEqual('txt_secondary');
  });

  it('Test prepareCardholderInfoViewData should have cardholderRole Authorized', () => {
    const dataReturn = prepareCardholderInfoViewData(
      {
        ...value,
        cardholderRole: '03'
      },
      translateFn
    );
    expect(dataReturn?.cardholderRole).toEqual('txt_authorized');
  });

  it('Test prepareCardholderInfoViewData should have cardholderRole empty', () => {
    const dataReturn = prepareCardholderInfoViewData(
      {
        ...value,
        cardholderRole: ''
      },
      translateFn
    );
    expect(dataReturn?.cardholderRole).toEqual('');
  });

  it('Test generateLabel isDisplayDate = false', () => {
    const dataExpect = `txt_permanent txt_billing`;
    const dataReturn = generateLabel(addressInfo, undefined, translateFn);
    expect(dataExpect).toEqual(dataReturn);
  });

  it('Test generateLabel isDisplayDate = true', () => {
    const formatEffectiveFrom = formatTimeDefault(
      addressInfo.effectiveFrom,
      FormatTime.Date
    );
    const formatEffectiveTo = formatTimeDefault(
      addressInfo.effectiveTo,
      FormatTime.Date
    );
    const dataExpect = `txt_permanent txt_billing • ${formatEffectiveFrom} - ${formatEffectiveTo}`;
    const dataReturn = generateLabel(addressInfo, true, translateFn);
    expect(dataExpect).toEqual(dataReturn);
  });

  it('generateMemoAddressValue without data', () => {
    const dataReturn = generateMemoAddressValue();
    expect(dataReturn).toEqual('');
  });

  it('generateMemoAddressValue with data', () => {
    const dataReturn = generateMemoAddressValue(addressInfo);
    expect(dataReturn).toBeTruthy();
  });

  it('generateMemoAddressValue with data', () => {
    const dataReturn = generateMemoAddressValue(addressInfo1);
    expect(dataReturn).toBeTruthy();
  });

  it('generateMemoAddressValue with wrong Data', () => {
    const dataReturn = generateMemoAddressValue({
      ...addressInfo,
      addressCategory: { fieldID: 'Test', fieldValue: 'Test' },
      addressType: { fieldID: 'Test', fieldValue: 'Test' },
      countryISO: { fieldID: 'Test', fieldValue: 'Test' },
      stateSubdivision: { fieldID: 'Test', fieldValue: 'Test' },
      addressRelationship: { fieldID: 'Test', fieldValue: 'Test' }
    });
    expect(dataReturn).toBeTruthy();
  });

  it('handleToggle', () => {
    const store = createStore(rootReducer, {});
    const changeExpand = spyActions('changeExpand');
    handleToggle('isExpandAddress', storeId)(
      store.dispatch,
      store.getState,
      {}
    );
    const data = {
      expandState: 'isExpandAddress',
      storeId
    };
    expect(changeExpand).toHaveBeenCalledWith(data);
  });

  it('handleGenerateValuesForMemo with data', () => {
    const oldValue = {
      addressLineOne: 'addressLineOne',
      addressLineTwo: 'addressLineTwo',
      cityName: 'cityName',
      city: 'city',
      stateSubdivision: {
        fieldId: 'fieldId',
        fieldValue: 'fieldValue',
        description: 'description'
      },
      postalCode: 'postalCode'
    };
    const newValue = {
      addressLineOne: 'addressLineOneNew',
      addressLineTwo: 'addressLineTwoNew',
      cityName: 'cityNameNew',
      city: 'cityNew',
      stateSubdivision: {
        fieldId: 'fieldIdNew',
        fieldValue: 'fieldValueNew',
        description: 'descriptionNew'
      },
      postalCode: 'postalCodeNew'
    };
    const returnData = handleGenerateValuesForMemo(oldValue, newValue);

    expect(returnData).toEqual(
      'Address Line 1 changed from addressLineOne to addressLineOneNew, Address Line 2 changed from addressLineTwo to addressLineTwoNew, City changed from cityName to cityNew, State/Subdivision changed from description to descriptionNew, Postal Code changed from postalCode to postalCodeNew'
    );
  });

  it('handleGenerateValuesForMemo with wrong data', () => {
    const returnData = handleGenerateValuesForMemo({}, {});
    expect(returnData).toEqual('');
  });

  describe('generateMemoEditAddressValue', () => {
    it('generateMemoEditAddressValue ', () => {
      const data = {
        addressCategory: {
          new: { description: 'addressCategoryNew', old: 'addressCategoryOld' }
        },
        addressType: {
          new: { description: 'addressTypeNew', old: 'addressTypeOld' }
        },
        effectiveFrom: { new: 'effectiveFromNew', old: 'effectiveFromOld' },
        effectiveTo: { new: 'effectiveToNew', old: 'effectiveToOld' },
        countryISO: {
          new: { description: 'countryISONew', old: 'countryISOOld' }
        },
        attention: { new: 'attentionNew', old: 'attentionOld' },
        houseBuilding: { new: 'houseBuildingNew', old: 'houseBuildingOld' },
        streetName: { new: 'streetNameNew', old: 'streetNameOld' },
        houseNumber: { new: 'houseNumberNew', old: 'houseNumberOld' },
        POBox: { new: 'POBoxNew', old: 'POBoxOld' },
        addressRelationship: {
          new: {
            description: 'addressRelationshipNew',
            old: 'addressRelationshipOld'
          }
        },
        addressLineOne: { new: 'addressLineOneNew', old: 'addressLineOneOld' },
        addressLineTwo: { new: 'addressLineTwoNew', old: 'addressLineTwoOld' },
        addressLineThree: {
          new: 'addressLineThreeNew',
          old: 'addressLineThreeOld'
        },
        addressLineFour: {
          new: 'addressLineFourNew',
          old: 'addressLineFourOld'
        },
        cityName: { new: 'cityNameNew', old: 'cityNameOld' },
        stateSubdivision: {
          new: {
            description: 'stateSubdivisionNew',
            old: 'stateSubdivisionOld'
          }
        },
        postalCode: { new: 'postalCodeNew', old: 'postalCodeOld' }
      };

      const result = generateMemoEditAddressValue(data);

      expect(result).toEqual(
        'CATEGORY changed from  to addressCategoryNew, Address Type changed from  to addressTypeNew, Effective From changed from  to , Effective To changed from  to , COUNTRY changed from  to countryISONew, Attention changed from attentionOld to attentionNew, HOUSE/BUILDING changed from houseBuildingOld to houseBuildingNew, Street Name changed from streetNameOld to streetNameNew, House Number changed from houseNumberOld to houseNumberNew, PO Box changed from POBoxOld to POBoxNew, Address Relationship changed from  to addressRelationshipNew, Address Line 1 changed from addressLineOneOld to addressLineOneNew, Address Line 2 changed from addressLineTwoOld to addressLineTwoNew, Address Line 3 changed from addressLineThreeOld to addressLineThreeNew, Address Line 4 changed from addressLineFourOld to addressLineFourNew, City changed from cityNameOld to cityNameNew, STATE changed from  to stateSubdivisionNew, ZIP CODE changed from postalCodeOld to postalCodeNew'
      );
    });

    it('empty value ', () => {
      const data = {};

      const result = generateMemoEditAddressValue(data);

      expect(result).toEqual('');
    });

    it('should test with wrong data', () => {
      const result = generateMemoEditAddressValue({
        key: 'test mock data'
      } as MagicKeyValue);
      expect(result).toEqual('');
    });
  });
});
