import React from 'react';
import { render, screen } from '@testing-library/react';
import DropdownItem, { ItemDropdownProps } from './DropdownItem';
import { queryByClass } from 'app/test-utils';
import { COMMON_STATUS } from 'app/constants';

const props: ItemDropdownProps = {
  cardholderName: 'cardholderName',
  cardholderRole: 'cardholderRole',
  status: 'Closed',
  statusCode: 'C'
};

describe('Render', () => {
  it('Render UI', () => {
    const { baseElement } = render(<DropdownItem {...props} />);

    expect(screen.getByText(props.status)).toBeInTheDocument();
    expect(
      queryByClass(
        baseElement as HTMLElement,
        `badge badge-${COMMON_STATUS[props.statusCode]}`
      )
    ).toBeInTheDocument();
  });

  it('Render UI > without status', () => {
    const { baseElement } = render(
      <DropdownItem {...props} status={undefined as unknown as string} />
    );

    expect(screen.getByText('txt_normal')).toBeInTheDocument();
    expect(
      queryByClass(
        baseElement as HTMLElement,
        `badge badge-${COMMON_STATUS[props.statusCode]}`
      )
    ).toBeInTheDocument();
  });

  it('Render UI > without statusCode', () => {
    const { baseElement } = render(
      <DropdownItem {...props} statusCode={undefined as unknown as string} />
    );

    expect(screen.getByText(props.status)).toBeInTheDocument();
    expect(
      queryByClass(baseElement as HTMLElement, /badge-green/)
    ).toBeInTheDocument();
  });
});
