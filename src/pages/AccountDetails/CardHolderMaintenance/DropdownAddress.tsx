import React, { useMemo } from 'react';

// components
import {
  DropdownList,
  DropdownBaseChangeEvent
} from 'app/_libraries/_dls/components';

// redux
import { useStoreIdSelector, useAccountDetail } from 'app/hooks';
import {
  selectCardholderAddressTypes,
  selectCardholderAddressType
} from 'pages/AccountDetails/CardHolderMaintenance/_redux/selector';
import { useDispatch } from 'react-redux';
import { cardholderMaintenanceActions } from 'pages/AccountDetails/CardHolderMaintenance/_redux';

// type
import { useTranslation } from 'app/_libraries/_dls/hooks';
import {
  IAddressInfo,
  IChangeAddressType
} from 'pages/AccountDetails/CardHolderMaintenance/types';
import { generateLabel, translateAddressInfo } from './helpers';
import isEmpty from 'lodash.isempty';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface CardHolderMaintenanceProps {
  isDisplayDate?: boolean;
  dataTestId?: string;
}

const DropdownAddress: React.FC<CardHolderMaintenanceProps> = ({
  isDisplayDate = false,
  dataTestId
}) => {
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();
  const addressTypes: IAddressInfo[] = useStoreIdSelector(
    selectCardholderAddressTypes
  );
  const addressTypeData: IAddressInfo = useStoreIdSelector(
    selectCardholderAddressType
  );

  const addressType: IAddressInfo = translateAddressInfo(addressTypeData, t);

  const handleTextFieldRender = (value: IAddressInfo) => {
    const formatValue: IAddressInfo = {
      ...value,
      addressCategory: {
        ...value.addressCategory,
        description: value.addressCategory.description?.split('-')[1]?.trim()
      },
      addressType: {
        ...value.addressType,
        description: value.addressType.description?.split('-')[1]?.trim()
      }
    };
    return <>{generateLabel(formatValue, isDisplayDate, t)}</>;
  };

  const dropdownListItems = useMemo(() => {
    if (!addressTypes || !addressTypes.length) return [];
    const items = addressTypes.map(item => {
      return (
        <DropdownList.Item
          key={item.id}
          label={generateLabel(item, isDisplayDate, t)}
          value={item}
        />
      );
    });

    return items;
  }, [addressTypes, isDisplayDate, t]);

  const handleChange = (event: DropdownBaseChangeEvent) => {
    const data: IChangeAddressType = {
      addressType: event.target.value,
      storeId
    };
    dispatch(cardholderMaintenanceActions.changeAddressType(data));
  };

  return (
    <div className="mt-16">
      <DropdownList
        name="addressType"
        textFieldRender={handleTextFieldRender}
        label={t('txt_address_type')}
        value={isEmpty(addressType) ? null : addressType}
        onChange={handleChange}
        popupBaseProps={{
          popupBaseClassName: 'inside-infobar'
        }}
        dataTestId={genAmtId(
          dataTestId!,
          'dropdown-address',
          'CardHolderMaintenance.DropdownAddress'
        )}
        noResult={t('txt_no_results_found')}
      >
        {dropdownListItems}
      </DropdownList>
    </div>
  );
};

export default DropdownAddress;
