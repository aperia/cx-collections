import React from 'react';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// components
import { View } from 'app/_libraries/_dof/core';
import SectionControl from 'app/components/SectionControl';

// redux
import {
  selectCardholderInfo,
  selectIsExpandInfo
} from 'pages/AccountDetails/CardHolderMaintenance/_redux/selector';

// action
import { handleToggle, prepareCardholderInfoViewData } from '../helpers';
import {
  I18N_CARDHOLDER_MAINTENANCE,
  VIEW_NAME_CARD_MAINTENANCE
} from '../constants';
import { ICardholderInfo } from '../types';

export interface CardholderInfoProps {}

const CardholderInfo: React.FC<CardholderInfoProps> = ({}) => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const isExpandInfo = useStoreIdSelector<boolean>(selectIsExpandInfo);
  const cardholder = useStoreIdSelector<ICardholderInfo>(selectCardholderInfo);
  const testId = 'infobar_cardholder_cardholder-info';

  const changeToggle = () => {
    dispatch(handleToggle('isExpandInfo', storeId));
  };
  return (
    <SectionControl
      title={t(I18N_CARDHOLDER_MAINTENANCE.CARDHOLDER_INFO)}
      classNamesView="ml-20"
      sectionKey={VIEW_NAME_CARD_MAINTENANCE.CARD_INFO}
      onToggle={changeToggle}
      isExpand={isExpandInfo}
      dataTestId={testId}
    >
      <View
        id={`cardholderMaintenanceInfo_${storeId}`}
        descriptor={VIEW_NAME_CARD_MAINTENANCE.CARD_INFO}
        formKey={`cardholderMaintenanceInfo_${storeId}`}
        value={prepareCardholderInfoViewData(cardholder, t)}
      />
    </SectionControl>
  );
};

export default CardholderInfo;
