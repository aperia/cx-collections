import React from 'react';
import CardholderInfo from './';
import { queryByClass, renderMockStoreId, storeId } from 'app/test-utils';
import { fireEvent } from '@testing-library/react';
import { defaultStateCardholder } from '../_redux';
import { ICardholderId } from '../types';

const cardholderId: ICardholderId = {
  cardNumberValue: 'VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)',
  cardholderName: 'ENDY2,HARRYY T',
  cardholderRole: '01',
  customerExternalIdentifier: 'C20273234913858500085018',
  customerRoleTypeCode: '01',
  customerType: '01',
  id: '01',
  memberSequence: '00001',
  status: '',
  statusCode: ''
};

const initialState: Partial<RootState> = {
  cardholderMaintenance: {
    [storeId]: {
      ...defaultStateCardholder,
      cardholdersId: [cardholderId],
      cardholderId: cardholderId,
      isLoading: true
    }
  }
};

describe('Test CardholderInfo', () => {
  it('Should Have Collapse', () => {
    const { wrapper } = renderMockStoreId(<CardholderInfo />, {
      initialState
    });

    const queryClass = queryByClass(wrapper.container, /icon icon-minus/);
    expect(queryClass).toBeInTheDocument();
  });

  it('Should Have Expanded', () => {
    const { wrapper } = renderMockStoreId(<CardholderInfo />, {
      initialState
    });

    const queryClassIconMinus = queryByClass(
      wrapper.container,
      /icon icon-minus/
    );

    fireEvent.click(queryClassIconMinus!);

    const queryClassIconPlus = queryByClass(
      wrapper.container,
      /icon icon-plus/
    );
    expect(queryClassIconPlus).toBeInTheDocument();
  });
});
