import React, { useMemo, useState } from 'react';

// components/types
import {
  DropdownList,
  Modal,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TextBox
} from 'app/_libraries/_dls/components';
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';
import { IAddressInfo, ICardholderId } from '../types';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useFormik } from 'formik';

// redux
import { useDispatch } from 'react-redux';
import {
  getCardholderId,
  selectCardholderAddressType,
  selectIsOpenModalStandardEditAddress,
  selectLoadingEditStandardAddress
} from '../_redux/selector';
import { cardholderMaintenanceActions } from 'pages/AccountDetails/CardHolderMaintenance/_redux';

// utils
import isEmpty from 'lodash.isempty';

// fake data
import statesByCountry from './tempStatesByCountry.json';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { translateAddressInfo } from '../helpers';

export interface EditProps {}

const Edit: React.FC<EditProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId, accEValue } = useAccountDetail();
  const [isDirty, setFormDirty] = useState(true);
  const cardholderId = useStoreIdSelector<ICardholderId>(getCardholderId);

  const addressInfoData = useStoreIdSelector<IAddressInfo>(
    selectCardholderAddressType
  );

  const addressInfo: IAddressInfo = useMemo(
    () => translateAddressInfo(addressInfoData, t),
    [addressInfoData, t]
  );

  const isOpenModalStandardEditAddress = useStoreIdSelector<boolean>(
    selectIsOpenModalStandardEditAddress
  );

  const isLoading = useStoreIdSelector<boolean>(
    selectLoadingEditStandardAddress
  );

  const testId = 'infobar_cardholder_standard-address_edit-modal';

  const originalAddress = {
    addressLineOne: addressInfo?.addressLineOne,
    addressLineTwo: addressInfo?.addressLineTwo,
    city: addressInfo?.cityName,
    stateSubdivision: statesByCountry.find(
      state => state.fieldId === addressInfo?.stateSubdivision.fieldID
    ),
    postalCode: addressInfo?.postalCode
  };

  const {
    values,
    handleChange,
    handleBlur,
    handleSubmit,
    errors,

    dirty,
    touched
  } = useFormik({
    initialValues: {
      ...originalAddress
    },
    onSubmit: values => {
      dispatch(
        cardholderMaintenanceActions.triggerEditStandardAddress({
          storeId,
          accEValue,
          cardholderId: cardholderId.id,
          ...values
        } as any)
      );
    },
    validateOnBlur: true,
    validateOnChange: false,
    validate: ({ addressLineOne, postalCode, city, stateSubdivision }) => {
      setFormDirty(!dirty);
      const errors: Record<string, string> | undefined = {};
      const regex = /[&',-/.]/;
      if (!addressLineOne) {
        errors['addressLineOne'] = 'txt_address_line_one_is_required';
      }
      if (!city) errors['city'] = 'txt_city_is_required';
      if (!stateSubdivision) {
        errors['stateSubdivision'] = 'txt_state_subdivision_required';
      }
      if (!postalCode) {
        errors['postalCode'] = 'txt_postal_code_required';
      }
      if (city && regex.test(city)) {
        errors['city'] = `txt_city_not_contain`;
      }
      if (postalCode && regex.test(postalCode)) {
        errors['postalCode'] = `txt_postal_code_not_contain`;
      }
      return errors;
    }
  });

  const handleCloseModal = () => {
    dispatch(
      cardholderMaintenanceActions.toggleModalStandardEditAddress({
        storeId,
        isOpen: false
      })
    );
  };

  return (
    <Modal
      loading={isLoading}
      md
      show={isOpenModalStandardEditAddress}
      dataTestId={testId}
    >
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId={`${testId}-header`}
      >
        <ModalTitle dataTestId={`${testId}-header-title`}>
          {t('txt_edit_address')}
        </ModalTitle>
      </ModalHeader>
      <ModalBodyWithApiError
        storeId={storeId}
        apiErrorClassName="mb-24"
        dataTestId={`${testId}-body`}
      >
        <form>
          <div className="row mb-16">
            <div className="col-6">
              <TextBox
                name="addressLineOne"
                label={t('txt_address_line_one')}
                value={values.addressLineOne}
                onChange={handleChange}
                onBlur={handleBlur}
                required
                error={{
                  status: Boolean(
                    errors.addressLineOne && touched.addressLineOne
                  ),
                  message: t(errors.addressLineOne) as string
                }}
                maxLength={26}
                dataTestId={`${testId}-body_addressLineOne`}
              />
            </div>
            <div className="col-6">
              <TextBox
                name="addressLineTwo"
                label={t('txt_address_line_two')}
                value={values.addressLineTwo}
                onChange={handleChange}
                onBlur={handleBlur}
                maxLength={26}
                dataTestId={`${testId}-body_addressLineTwo`}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-4">
              <TextBox
                name="city"
                label={t('txt_city')}
                value={values.city}
                onChange={handleChange}
                onBlur={handleBlur}
                required
                error={{
                  status: Boolean(errors.city && touched.city),
                  message: t(errors.city) as string
                }}
                maxLength={25}
                dataTestId={`${testId}-body_city`}
              />
            </div>
            <div className="col-4">
              <DropdownList
                name="stateSubdivision"
                label={t('txt_state_subdivision')}
                value={values.stateSubdivision}
                onChange={handleChange}
                onBlur={handleBlur}
                textField="description"
                required
                error={{
                  status: Boolean(
                    errors.stateSubdivision && touched.stateSubdivision
                  ),
                  message: t(errors.stateSubdivision) as string
                }}
                dataTestId={`${testId}-body_stateSubdivision`}
                noResult={t('txt_no_results_found')}
              >
                {statesByCountry.map(
                  (item: {
                    fieldId: string;
                    fieldValue: string;
                    description: string;
                  }) => {
                    return (
                      <DropdownList.Item
                        key={item.fieldId}
                        label={item.description}
                        value={item}
                      />
                    );
                  }
                )}
              </DropdownList>
            </div>
            <div className="col-4">
              <TextBox
                name="postalCode"
                label={t('txt_postal_code')}
                required
                value={values.postalCode}
                onChange={handleChange}
                onBlur={handleBlur}
                error={{
                  status: Boolean(errors.postalCode && touched.postalCode),
                  message: t(errors.postalCode) as string
                }}
                maxLength={5}
                dataTestId={`${testId}-body_postalCode`}
              />
            </div>
          </div>
        </form>
      </ModalBodyWithApiError>
      <ModalFooter
        cancelButtonText={t('txt_cancel')}
        okButtonText={t('txt_save')}
        disabledOk={!isEmpty(errors) || isDirty}
        onCancel={handleCloseModal}
        onOk={handleSubmit}
        dataTestId={`${testId}-footer`}
      />
    </Modal>
  );
};

export default Edit;
