import React from 'react';
import { fireEvent, waitFor } from '@testing-library/react';

// Component
import Edit from './Edit';

// Helper
import { mockActionCreator, storeId, renderMockStoreId } from 'app/test-utils';

// redux
import { cardholderMaintenanceActions } from '../_redux';
import userEvent from '@testing-library/user-event';

HTMLCanvasElement.prototype.getContext = jest.fn();

const initialState: Partial<RootState> = {
  cardholderMaintenance: {
    [storeId]: {
      cardholderId: {
        id: 'cardID',
        cardNumberValue: 'value',
        cardholderName: 'nameCard',
        cardholderRole: '01',
        customerType: 'type',
        customerExternalIdentifier: 'externalId',
        customerRoleTypeCode: 'custom role',
        memberSequence: 'member',
        status: 'success',
        statusCode: '200'
      },
      addressType: {
        id: 'addId',
        addressLineOne: '12345',
        addressLineTwo: '12345',
        cityName: '12345',
        stateSubdivision: {
          description: 'AA - APO/FPO Florida',
          fieldID: 'AA',
          fieldValue: 'APO/FPO Florida'
        },
        postalCode: '12345'
      },
      standardAddress: {
        isOpenModalEdit: true
      }
    }
  }
};

const cardHolderSpy = mockActionCreator(cardholderMaintenanceActions);

describe('Test Edit Address', () => {
  it('Render UI with invalid data', () => {
    const spy = cardHolderSpy('toggleModalStandardEditAddress');
    const { wrapper } = renderMockStoreId(<Edit />, {
      initialState: {
        ...initialState,
        cardholderMaintenance: {
          ...initialState.cardholderMaintenance,
          [storeId]: {
            ...initialState.cardholderMaintenance[storeId],
            addressType: {
              id: 'addId',
              addressLineOne: '',
              addressLineTwo: '',
              cityName: '',
              stateSubdivision: {
                description: '',
                fieldID: '',
                fieldValue: ''
              },
              postalCode: ''
            }
          }
        }
      }
    });

    const input = wrapper.queryByTestId(
      'infobar_cardholder_standard-address_edit-modal-body_city_dls-text-box_input'
    )!;
    fireEvent.blur(input);
    expect(wrapper.getByText('txt_edit_address')).toBeInTheDocument();

    // Close Modal
    const buttonCancel = wrapper.getByText('txt_cancel')!;
    buttonCancel.click();

    expect(spy).toBeCalledWith({
      storeId,
      isOpen: false
    });
  });

  it('Render UI with incorrect data', () => {
    const { wrapper } = renderMockStoreId(<Edit />, {
      initialState: {
        ...initialState,
        cardholderMaintenance: {
          ...initialState.cardholderMaintenance,
          [storeId]: {
            ...initialState.cardholderMaintenance[storeId],
            addressType: {
              id: 'addId',
              addressLineOne: '',
              addressLineTwo: '',
              cityName: '123-',
              stateSubdivision: {
                description: '',
                fieldID: '',
                fieldValue: ''
              },
              postalCode: '123-'
            }
          }
        }
      }
    });

    const input = wrapper.queryByTestId(
      'infobar_cardholder_standard-address_edit-modal-body_city_dls-text-box_input'
    )!;
    fireEvent.blur(input);
    expect(wrapper.getByText('txt_edit_address')).toBeInTheDocument();
  });

  describe('Submit', () => {
    it('Render UI with valid data', () => {
      waitFor(() => {
        const spy = cardHolderSpy('triggerEditStandardAddress');
        const { wrapper } = renderMockStoreId(<Edit />, {
          initialState
        });

        const button = wrapper.getByText('txt_save')!;

        const input = wrapper.queryByTestId(
          'infobar_cardholder_standard-address_edit-modal-body_city_dls-text-box_input'
        )!;

        userEvent.type(input, '123bs');

        button.click();

        expect(spy).toBeCalledWith({
          storeId,
          accEValue: storeId,
          cardholderId: 'cardID'
        });
      });
    });
  });
});
