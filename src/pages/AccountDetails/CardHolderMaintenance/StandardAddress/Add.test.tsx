import React from 'react';
import { fireEvent, waitFor, screen } from '@testing-library/react';

// Component
import Add from './Add';

// Helper
import { mockActionCreator, storeId, renderMockStoreId } from 'app/test-utils';

// redux
import {
  cardholderMaintenanceActions,
  defaultStateCardholder
} from '../_redux';
import userEvent from '@testing-library/user-event';

HTMLCanvasElement.prototype.getContext = jest.fn();

const initialState: Partial<RootState> = {
  cardholderMaintenance: {
    [storeId]: {
      ...defaultStateCardholder,
      expandedAddress: {
        histories: [],
        openModal: '',
        isLoading: false,
        isEndLoadMore: false
      },
      cardholderId: {
        cardNumberValue: 'VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)',
        cardholderName: 'ENDY2,HARRYY T',
        cardholderRole: '01',
        customerExternalIdentifier: 'C20273234913858500085018',
        customerRoleTypeCode: '01',
        customerType: '01',
        id: 'C20273234913858500085018',
        memberSequence: '0001',
        status: '',
        statusCode: ''
      },
      standardAddress: { isOpenModalAdd: true },
      isLoading: true
    }
  }
};

const cardHolderSpy = mockActionCreator(cardholderMaintenanceActions);

describe('Test Add Address', () => {
  it('onChange valid data', () => {
    const { wrapper } = renderMockStoreId(<Add />, {
      initialState
    });

    const inputAdr1 = wrapper.getByTestId(
      'infobar_cardholder_standard-address_add-modal-body_addressLineOne_dls-text-box_input'
    )!;
    const inputAdr2 = wrapper.getByTestId(
      'infobar_cardholder_standard-address_add-modal-body_addressLineTwo_dls-text-box_input'
    )!;
    const inputPostalCode = wrapper.getByTestId(
      'infobar_cardholder_standard-address_add-modal-body_postalCode_dls-text-box_input'
    )!;
    const inputCity = wrapper.getByTestId(
      'infobar_cardholder_standard-address_add-modal-body_city_dls-text-box_input'
    )!;

    const button = wrapper.getByText('txt_add')!;

    // Case 1 with invalid value
    fireEvent.change(inputPostalCode, { target: { value: '123-' } });
    fireEvent.change(inputCity, { target: { value: '123-' } });

    button.click();

    expect(inputPostalCode).toHaveValue('123-');
    expect(inputCity).toHaveValue('123-');

    // Case 2 with valid value
    fireEvent.change(inputAdr1, { target: { value: 'adr123' } });
    fireEvent.change(inputAdr2, { target: { value: 'adr123' } });
    fireEvent.change(inputCity, { target: { value: '12345' } });
    fireEvent.change(inputPostalCode, { target: { value: '12345' } });

    const icon = wrapper.baseElement.querySelector('.icon-chevron-down')!;
    userEvent.click(icon);

    const item = wrapper.getByText('AA - APO/FPO Florida')!;
    item.click();

    waitFor(() => {
      button.click();
    });
  });

  it('Should have handleCloseModal with button cancel', () => {
    waitFor(() => {
      const mockAction = cardHolderSpy('toggleModalStandardAddAddress');

      renderMockStoreId(<Add />, {
        initialState
      });

      const button = screen.getByText(/txt_cancel/);
      waitFor(() => {
        button.click();
      });

      expect(mockAction).toBeCalledWith({ storeId, isOpen: false });
    });
  });
});
