import React from 'react';
import {
  fireEvent,
  queryByText,
  act,
  waitFor,
  screen
} from '@testing-library/react';

// Helper
import {
  mockActionCreator,
  queryAllByClass,
  queryByClass,
  renderMockStoreId,
  storeId
} from 'app/test-utils';

// redux
import {
  cardholderMaintenanceActions,
  defaultStateCardholder
} from '../_redux';
import StandardAddress from './index';
import { IAddressInfo } from '../types';
import userEvent from '@testing-library/user-event';
import * as entitlementsHelpers from 'app/entitlements/helpers';
import { PERMISSIONS } from 'app/entitlements/constants';

HTMLCanvasElement.prototype.getContext = jest.fn();
const addressInfo = {
  POBox: '',
  addressCategory: {
    fieldValue: 'Permanent',
    fieldID: 'P',
    description: 'txt_p_permanent'
  },
  addressLineFour: '',
  addressLineOne: '333 MENLAY',
  addressLineThree: '',
  addressLineTwo: '',
  addressRelationship: {
    fieldValue: 'Unknown',
    fieldID: 'U',
    description: 'txt_u_unknown'
  },
  addressType: {
    fieldValue: 'Billing',
    fieldID: 'BLL1',
    description: 'txt_bll1_billing'
  },
  attention: '',
  cityName: 'OMAHA',
  countryISO: {
    fieldValue: 'United States of America',
    fieldID: 'USA',
    description: 'USA - United States of America'
  },
  effectiveFrom: '2021-05-18',
  effectiveTo: '9999-12-31',
  formattedAddressCode: 'F',
  houseBuilding: '',
  houseNumber: '',
  id: '00001-P-BLL1-2021-06-18-9999-12-31',
  memberSequence: '00001',
  postalCode: '10003',
  stateSubdivision: {
    fieldValue: 'New York',
    fieldID: 'NY',
    description: 'NY - New York'
  },
  streetName: ''
};

const cardHolderSpy = mockActionCreator(cardholderMaintenanceActions);

const initialState: Partial<RootState> = {
  cardholderMaintenance: {
    [storeId]: {
      ...defaultStateCardholder,
      addressType: {} as IAddressInfo,
      isLoading: true
    }
  }
};

beforeEach(() => {
  jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
});

describe('Test StandardAddress', () => {
  it('Should have Render UI with no addressType and section is collapsing', () => {
    const { wrapper } = renderMockStoreId(<StandardAddress />, {
      initialState
    });
    const queryTextNoData = queryByText(wrapper.container, 'txt_no_data');
    const queryClassCollapse = queryByClass(
      wrapper.container,
      /icon icon-plus/
    );
    expect(queryClassCollapse).toBeInTheDocument();
    expect(queryTextNoData).toBeInTheDocument();
  });

  it('Should have Render UI section is expanding with class icon-minus', () => {
    const { wrapper } = renderMockStoreId(<StandardAddress />, {
      initialState
    });

    const queryClassPlus = queryByClass(wrapper.container, /icon icon-plus/);
    fireEvent.click(queryClassPlus!);
    const queryClassMinus = queryByClass(wrapper.container, /icon icon-minus/);
    expect(queryClassMinus).toBeInTheDocument();
  });

  it('Should have Render UI section is expanding with action', () => {
    const { wrapper } = renderMockStoreId(<StandardAddress />, {
      initialState
    });

    const spyHandleToggle = cardHolderSpy('changeExpand');
    const queryClassPlus = queryByClass(wrapper.container, /icon icon-plus/);
    fireEvent.click(queryClassPlus!);

    expect(spyHandleToggle).toBeCalledWith({
      expandState: 'isExpandAddress',
      storeId
    });
  });

  it('Should have Render UI with data addressType and Type is BLL1', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(<StandardAddress />, {
      initialState: {
        ...initialState,
        cardholderMaintenance: {
          [storeId]: {
            ...initialState.cardholderMaintenance![storeId],
            addressType: addressInfo
          }
        }
      } as Partial<RootState>
    });
    jest.runAllTimers();
    const queryTextNoData = queryByText(
      wrapper.container,
      'txt_cardholder_address_details'
    );

    screen.debug();

    const queryClassIconEdit = queryByClass(
      wrapper.container,
      /icon icon-edit/
    );

    expect(queryTextNoData).toBeInTheDocument();
    expect(queryClassIconEdit).toBeInTheDocument();
  });

  it('Should have Render UI with data addressType and Type difference BLL1', () => {
    const { wrapper } = renderMockStoreId(<StandardAddress />, {
      initialState: {
        ...initialState,
        cardholderMaintenance: {
          [storeId]: {
            ...initialState.cardholderMaintenance![storeId],
            addressType: {
              ...addressInfo,
              addressType: {
                fieldValue: 'Plastic',
                fieldID: 'PLS',
                description: 'PLS - Plastic'
              }
            }
          }
        }
      } as Partial<RootState>
    });

    const queryTextNoData = queryByText(
      wrapper.container,
      'txt_cardholder_address_details'
    );

    const queryClassIconEdit = queryByClass(
      wrapper.container,
      /icon icon-edit/
    );

    expect(queryTextNoData).toBeInTheDocument();
    expect(queryClassIconEdit).not.toBeInTheDocument();
  });

  it('Should have Render Modal Edit', () => {
    const { wrapper } = renderMockStoreId(<StandardAddress />, {
      initialState: {
        ...initialState,
        cardholderMaintenance: {
          [storeId]: {
            ...initialState.cardholderMaintenance![storeId],
            addressType: addressInfo
          }
        }
      } as Partial<RootState>
    });
    const queryClassPlus = queryByClass(wrapper.container, /icon icon-edit/);
    fireEvent.click(queryClassPlus!);
    const queryClassModal = queryByClass(
      wrapper.baseElement as HTMLElement,
      /modal-backdrop/
    );
    const queryText = queryByText(
      wrapper.baseElement as HTMLElement,
      'txt_edit_address'
    );
    expect(queryClassModal).toBeInTheDocument();
    expect(queryText).toBeInTheDocument();
  });

  it('Should have Render Modal Add', () => {
    const { wrapper } = renderMockStoreId(<StandardAddress />, {
      initialState: {
        ...initialState,
        cardholderMaintenance: {
          [storeId]: {
            ...initialState.cardholderMaintenance![storeId],
            standardAddress: {
              isOpenModalAdd: true
            }
          }
        }
      } as Partial<RootState>
    });

    const queryText = queryByText(
      wrapper.baseElement as HTMLElement,
      'txt_add_address'
    );
    expect(queryText).toBeInTheDocument();
  });

  it('Should have Click Dropdown Modal History And Click Close Modal HISTORY', () => {
    waitFor(() => {
      jest.useFakeTimers();
      const { wrapper } = renderMockStoreId(<StandardAddress />, {
        initialState: {
          ...initialState,
          cardholderMaintenance: {
            [storeId]: {
              ...initialState.cardholderMaintenance![storeId],
              addressType: addressInfo
            }
          }
        } as Partial<RootState>
      });

      const queryClassIconVertical = queryByClass(
        wrapper.baseElement as HTMLElement,
        /btn btn-icon-secondary dls-dropdown-button/
      );
      jest.runAllTimers();
      act(() => {
        userEvent.click(queryClassIconVertical!);
      });

      const queryDropdownBaseContainer = queryByClass(
        wrapper.baseElement as HTMLElement,
        /dls-dropdown-base-container/
      );

      const queryItemDropdown = queryAllByClass(
        queryDropdownBaseContainer as HTMLElement,
        /item/
      );

      act(() => {
        userEvent.click(queryItemDropdown[1]!);
      });

      const queryFooterModal = queryByClass(
        wrapper.baseElement as HTMLElement,
        /dls-modal-footer modal-footer/
      );

      const queryButtonClose = queryByClass(
        queryFooterModal as HTMLElement,
        /btn btn-secondary/
      );

      fireEvent.click(queryButtonClose!);
    });
  });

  it('Should have Click Dropdown Modal Add and spy action', () => {
    waitFor(() => {
      jest.useFakeTimers();
      const spyToggleModalStandardAddAddress = cardHolderSpy(
        'toggleModalStandardAddAddress'
      );

      const { wrapper } = renderMockStoreId(<StandardAddress />, {
        initialState: {
          ...initialState,
          cardholderMaintenance: {
            [storeId]: {
              ...initialState.cardholderMaintenance![storeId],
              addressType: addressInfo
            }
          }
        } as Partial<RootState>
      });

      const queryClassIconVertical = queryByClass(
        wrapper.baseElement as HTMLElement,
        /btn btn-icon-secondary dls-dropdown-button/
      );
      jest.runAllTimers();
      act(() => {
        userEvent.click(queryClassIconVertical!);
      });

      const queryDLSDropdownBaseContainer = queryByClass(
        wrapper.baseElement as HTMLElement,
        /dls-dropdown-base-container/
      );

      const queryItemDropdown = queryAllByClass(
        queryDLSDropdownBaseContainer as HTMLElement,
        /item/
      );

      act(() => {
        userEvent.click(queryItemDropdown[0]!);
      });

      expect(spyToggleModalStandardAddAddress).toBeCalledWith({
        storeId,
        isOpen: true
      });
    });
  });

  it('Should have Click Dropdown Modal History and spy action', () => {
    waitFor(() => {
      jest.useFakeTimers();
      const spyToggleModalStandardAddAddress = cardHolderSpy(
        'changeModalAddEditHistoryAddress'
      );

      const { wrapper } = renderMockStoreId(<StandardAddress />, {
        initialState: {
          ...initialState,
          cardholderMaintenance: {
            [storeId]: {
              ...initialState.cardholderMaintenance![storeId],
              addressType: addressInfo
            }
          }
        } as Partial<RootState>
      });
      const queryClassIconVertical = queryByClass(
        wrapper.baseElement as HTMLElement,
        /btn btn-icon-secondary dls-dropdown-button/
      );
      jest.runAllTimers();
      act(() => {
        userEvent.click(queryClassIconVertical!);
      });

      const queryDLSDropdownBaseContainer = queryByClass(
        wrapper.baseElement as HTMLElement,
        /dls-dropdown-base-container/
      );

      const queryItemDropdown = queryAllByClass(
        queryDLSDropdownBaseContainer as HTMLElement,
        /item/
      );

      act(() => {
        userEvent.click(queryItemDropdown[1]!);
      });

      expect(spyToggleModalStandardAddAddress).toBeCalledWith({
        storeId,
        type: 'HISTORY'
      });
    });
  });

  it('Should have Click Dropdown Modal Add Address and spy action', () => {
    const spyToggleModalStandardAddAddress = cardHolderSpy(
      'toggleModalStandardAddAddress'
    );
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);

    const { wrapper, rerender } = renderMockStoreId(<StandardAddress />, {
      initialState: {
        ...initialState,
        cardholderMaintenance: {
          [storeId]: {
            ...initialState.cardholderMaintenance![storeId],
            expandedAddress: {
              histories: [],
              openModal: 'HISTORY',
              isLoading: true,
              isEndLoadMore: true
            },
            addressType: addressInfo
          }
        }
      } as Partial<RootState>
    });
    const queryClassIconVertical = queryByClass(
      wrapper.baseElement as HTMLElement,
      /btn btn-icon-secondary dls-dropdown-button/
    );

    userEvent.click(queryClassIconVertical!);

    rerender(<StandardAddress />);

    const queryAddAddress = screen.getByText('txt_add_address');

    act(() => {
      userEvent.click(queryAddAddress);
    });

    expect(spyToggleModalStandardAddAddress).toBeCalledWith({
      storeId,
      isOpen: true
    });
  });

  it('Should have Click Dropdown Modal History and spy action 2', () => {
    const spyToggleModalAddEditHistoryAddress = cardHolderSpy(
      'changeModalAddEditHistoryAddress'
    );
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);

    const { wrapper, rerender } = renderMockStoreId(<StandardAddress />, {
      initialState: {
        ...initialState,
        cardholderMaintenance: {
          [storeId]: {
            ...initialState.cardholderMaintenance![storeId],
            expandedAddress: {
              histories: [],
              openModal: 'HISTORY',
              isLoading: true,
              isEndLoadMore: true
            },
            addressType: addressInfo
          }
        }
      } as Partial<RootState>
    });
    const queryClassIconVertical = queryByClass(
      wrapper.baseElement as HTMLElement,
      /btn btn-icon-secondary dls-dropdown-button/
    );

    userEvent.click(queryClassIconVertical!);

    rerender(<StandardAddress />);

    const queryViewAddressHistory = screen.getByText(
      'txt_view_address_history'
    );

    act(() => {
      userEvent.click(queryViewAddressHistory);
    });

    expect(spyToggleModalAddEditHistoryAddress).toBeCalledWith({
      storeId,
      type: 'HISTORY'
    });

    const queryClassIconClose = queryByClass(
      wrapper.baseElement as HTMLElement,
      /icon icon-close/
    );

    act(() => {
      userEvent.click(queryClassIconClose!);
    });

    expect(spyToggleModalAddEditHistoryAddress).toBeCalledWith({
      storeId,
      type: ''
    });
  });

  it('should render UI with only have permission add address', () => {
    const spyToggleModalStandardAddAddress = cardHolderSpy(
      'toggleModalStandardAddAddress'
    );

    jest
      .spyOn(entitlementsHelpers, 'checkPermission')
      .mockImplementation(permission => {
        return permission === PERMISSIONS.CARDHOLDER_MAINTENANCE_ADD_ADDRESS;
      });

    renderMockStoreId(<StandardAddress />, {
      initialState
    });

    screen.getByText('txt_add_address').click();
    expect(spyToggleModalStandardAddAddress).toBeCalled();
  });

  it('should render UI without any permission', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(false);

    renderMockStoreId(<StandardAddress />, {
      initialState
    });

    expect(screen.queryByText('txt_add_address')).not.toBeInTheDocument();
    expect(
      screen.queryByText('txt_view_address_history')
    ).not.toBeInTheDocument();
  });
});
