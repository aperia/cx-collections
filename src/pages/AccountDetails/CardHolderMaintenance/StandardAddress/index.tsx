import React, { useCallback, useMemo } from 'react';

// components
import { View } from 'app/_libraries/_dof/core';
import SectionControl, {
  SectionControlProps
} from 'app/components/SectionControl';
import { Button, Icon, Tooltip } from 'app/_libraries/_dls/components';
import History from '../HistoryAddress';
import Add from './Add';
import Edit from './Edit';

// helpers
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import {
  I18N_CARDHOLDER_MAINTENANCE,
  VIEW_NAME_CARD_MAINTENANCE
} from '../constants';
import { handleToggle, translateAddressInfo } from '../helpers';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// i18n
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// redux
import { useDispatch } from 'react-redux';
import {
  selectCardholderAddressType,
  selectIsExpandAddress,
  selectIsOpenModalStandardAddAddress,
  selectIsOpenModalStandardEditAddress,
  selectModalExpandedAddress
} from '../_redux/selector';
import { cardholderMaintenanceActions } from 'pages/AccountDetails/CardHolderMaintenance/_redux';

// type
import { IAddressInfo, ModalExpandedAddress } from '../types';
import isEmpty from 'lodash.isempty';
import DropdownAddress from '../DropdownAddress';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

export interface StandardAddressProps {}

const StandardAddress: React.FC<StandardAddressProps> = () => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const isExpandAddress = useStoreIdSelector<boolean>(selectIsExpandAddress);

  const addressInfoData = useStoreIdSelector<IAddressInfo>(
    selectCardholderAddressType
  );
  const addressInfo: IAddressInfo = translateAddressInfo(addressInfoData, t);

  const isOpenModalStandardAddAddress = useStoreIdSelector<boolean>(
    selectIsOpenModalStandardAddAddress
  );

  const isOpenModalStandardEditAddress = useStoreIdSelector<boolean>(
    selectIsOpenModalStandardEditAddress
  );

  const openModal = useStoreIdSelector<ModalExpandedAddress>(
    selectModalExpandedAddress
  );

  const testId = 'infobar_cardholder_standard-address';

  const changeToggle = () => {
    dispatch(handleToggle('isExpandAddress', storeId));
  };

  const onChangeHistoryModal = useCallback(
    (type?: ModalExpandedAddress) => {
      dispatch(
        cardholderMaintenanceActions.changeModalAddEditHistoryAddress({
          storeId,
          type: type || ''
        })
      );
    },
    [dispatch, storeId]
  );

  const dropDownItems = useMemo(
    () => [
      {
        text: 'txt_add_address',
        value: 'addAddress',
        entitlementCode: PERMISSIONS.CARDHOLDER_MAINTENANCE_ADD_ADDRESS,
        handleClick: () => {
          dispatch(
            cardholderMaintenanceActions.toggleModalStandardAddAddress({
              storeId,
              isOpen: true
            })
          );
        }
      },
      {
        text: 'txt_view_address_history',
        value: 'viewAddressHistory',
        entitlementCode:
          PERMISSIONS.CARDHOLDER_MAINTENANCE_VIEW_ADDRESS_HISTORY,
        handleClick: () => {
          onChangeHistoryModal('HISTORY');
        }
      }
    ],
    [dispatch, onChangeHistoryModal, storeId]
  );

  const isShowDropDownButton = useMemo(
    () =>
      dropDownItems.filter(
        item => checkPermission(item.entitlementCode, storeId) === true
      ).length === 2,
    [dropDownItems, storeId]
  );

  const dropdownButton = useMemo<SectionControlProps['dropdownButton']>(() => {
    if (!isShowDropDownButton) return;
    return {
      onSelect: event => {
        dropDownItems
          .find(item => item.value === event.target.value)
          ?.handleClick();
      },
      tooltipText: 'txt_actions',
      placementDropdown: 'bottom-end',
      items: dropDownItems
    };
  }, [dropDownItems, isShowDropDownButton]);

  const ActionButton = useMemo(() => {
    if (isShowDropDownButton) return;
    const enableButton = dropDownItems.find(item =>
      checkPermission(item.entitlementCode, storeId)
    );
    if (!enableButton) return;
    return {
      buttonLabel: t(enableButton.text),
      handleAdd: enableButton.handleClick,
      className: 'h5 ml-4'
    };
  }, [dropDownItems, isShowDropDownButton, storeId, t]);

  const EditButton = useMemo(() => {
    if (
      !checkPermission(
        PERMISSIONS.CARDHOLDER_MAINTENANCE_EDIT_ADDRESS,
        storeId
      ) ||
      addressInfo?.addressType?.fieldID !== 'BLL1'
    ) {
      return null;
    }
    const handleShowStandardEditAddress = () => {
      dispatch(
        cardholderMaintenanceActions.toggleModalStandardEditAddress({
          storeId,
          isOpen: true
        })
      );
    };
    return (
      <Tooltip
        triggerClassName="ml-auto mr-n4"
        placement="top"
        variant={'primary'}
        element={t('txt_edit_address')}
      >
        <Button
          onClick={handleShowStandardEditAddress}
          variant="icon-secondary"
          dataTestId={`${testId}_edit-btn`}
        >
          <Icon name="edit" />
        </Button>
      </Tooltip>
    );
  }, [addressInfo?.addressType?.fieldID, dispatch, storeId, t]);

  return (
    <>
      <SectionControl
        title={t(I18N_COMMON_TEXT.ADDRESS)}
        classNamesView="ml-20"
        sectionKey={VIEW_NAME_CARD_MAINTENANCE.CARD_INFO}
        onToggle={changeToggle}
        isExpand={isExpandAddress}
        dropdownButton={dropdownButton}
        dataTestId={testId}
        isAdd={ActionButton}
      >
        {isEmpty(addressInfoData) ? (
          <p
            className="color-grey mt-16"
            data-testid={genAmtId(testId!, 'no-data', 'StandardAddress')}
          >
            {t('txt_no_data')}
          </p>
        ) : (
          <>
            <DropdownAddress dataTestId={`${testId}_addressType`} />
            <div className="d-flex align-items-center mt-16">
              <p
                className="fw-500"
                data-testid={genAmtId(testId, 'address-details-title', '')}
              >
                {t(I18N_CARDHOLDER_MAINTENANCE.ADDRESS_DETAILS)}
              </p>
              {EditButton}
            </div>
            <View
              id={`cardholderMaintenanceStandardAddress_${storeId}`}
              descriptor={VIEW_NAME_CARD_MAINTENANCE.CARD_STANDARD_ADDRESS}
              formKey={`cardholderMaintenanceStandardAddress_${storeId}`}
              value={addressInfo}
            />
          </>
        )}
      </SectionControl>
      {openModal === 'HISTORY' && (
        <History
          isExpandedAddress={false}
          onCloseModal={onChangeHistoryModal}
          dataTestId={`${testId}_standard-address`}
        />
      )}
      {isOpenModalStandardAddAddress && <Add />}
      {isOpenModalStandardEditAddress && <Edit />}
    </>
  );
};

export default StandardAddress;
