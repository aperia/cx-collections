export const TELEPHONE_VIEW_LABEL = {
  HOME_PHONE: 'txt_home_phone',
  WORK_PHONE: 'txt_work_phone',
  MOBILE_PHONE: 'txt_mobile_phone',
  FAX: 'txt_fax',
  MISCELLANEOUS: 'txt_miscellaneous'
};

export const TYPE_EDIT = {
  PHONE: 'telephone',
  EMAIL: 'email'
};

export const DATA_TYPE = {
  HOME: 'editPhoneHomeTextBox',
  WORK: 'editPhoneWorkTextBox',
  MOBILE: 'editPhoneMobileTextBox',
  FAX: 'editPhoneFaxTextBox',
  MISCELLANEOUS: 'editPhoneMiscellaneousTextBox'
};

export const DATA_FIELD = {
  HOME: 'homePhone.less',
  WORK: 'workPhone.less',
  MOBILE: 'mobilePhone.less',
  FAX: 'fax.less',
  MISCELLANEOUS: 'miscellaneous.less'
};

export const NAME_VIEW = {
  HOME: 'telephoneMoreHomeView',
  WORK: 'telephoneMoreWorkView',
  MOBILE: 'telephoneMoreMobileView',
  FAX: 'telephoneMoreFaxView',
  MISCELLANEOUS: 'telephoneMoreMiscellaneousView'
};
