import React from 'react';

// components
import { Popover } from 'app/_libraries/_dls/components';
import { View } from 'app/_libraries/_dof/core';

// redux
import { useAccountDetail } from 'app/hooks';

// type
import { TelePhoneInformation } from 'pages/AccountDetails/CardHolderMaintenance/types';

// helper
import { formatCommon } from 'app/helpers';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import isEmpty from 'lodash.isempty';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface RenderViewPhone {
  data?: TelePhoneInformation;
  label: string;
  nameView: string;
}

const ViewPhone: React.FC<RenderViewPhone> = ({
  data = {},
  label,
  nameView
}) => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();

  return (
    <div className="form-group-static">
      <span
        className="form-group-static__label"
        data-testid={genAmtId(nameView!, 'label', 'ViewPhone')}
      >
        <span>{label}</span>
      </span>
      <div className="form-group-static__text">
        {!isEmpty(data?.less) && (
          <>
            <span
              className="mr-4"
              data-testid={genAmtId(nameView!, 'value', 'ViewPhone')}
            >
              {formatCommon(data.less!).phone}
            </span>
            <Popover
              triggerClassName="d-inline-block"
              containerClassName="inside-infobar"
              placement="top"
              size="sm"
              element={
                <View
                  id={`${storeId}_${nameView}_telephone_more`}
                  formKey={`${storeId}_${nameView}_telephone_more`}
                  descriptor={nameView}
                  value={data?.more}
                />
              }
            >
              <span
                className="link text-decoration-none"
                data-testid={genAmtId(nameView!, 'more-btn', 'ViewPhone')}
              >
                {t(I18N_COMMON_TEXT.MORE)}
              </span>
            </Popover>
          </>
        )}
      </div>
    </div>
  );
};

export default ViewPhone;
