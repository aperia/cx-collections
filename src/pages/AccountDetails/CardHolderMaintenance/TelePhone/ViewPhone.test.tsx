import React from 'react';
import { screen } from '@testing-library/react';

// utils
import {
  renderMockStore,
  storeId,
  accEValue
} from 'app/test-utils/renderComponentWithMockStore';

// hook
import { AccountDetailProvider } from 'app/hooks';

// components
import ViewPhone from './ViewPhone';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// services
import { TelePhoneInformation } from '../types';

let spy1: jest.SpyInstance;

afterEach(() => {
  if (spy1) {
    spy1.mockReset();
    spy1.mockRestore();
  }
});

const data: TelePhoneInformation = {
  less: 'less'
};

const renderWrapper = (data?: TelePhoneInformation) => {
  return renderMockStore(
    <AccountDetailProvider value={{ storeId, accEValue }}>
      <ViewPhone data={data} label="label" nameView="nameView" />
    </AccountDetailProvider>
  );
};

describe('Render', () => {
  it('Render UI', () => {
    renderWrapper(data);

    expect(screen.getByText('label')).toBeInTheDocument();
    expect(screen.getByText(I18N_COMMON_TEXT.MORE)).toBeInTheDocument();
  });

  it('Render UI with empty data', () => {
    renderWrapper();

    expect(screen.getByText('label')).toBeInTheDocument();
    expect(screen.queryByText(I18N_COMMON_TEXT.MORE)).toBeNull();
  });
});
