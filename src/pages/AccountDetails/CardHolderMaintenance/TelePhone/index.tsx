import React from 'react';

// component
import SectionControl from 'app/components/SectionControl';
import ViewPhone from './ViewPhone';

// helper
import {
  VIEW_NAME_CARD_MAINTENANCE,
  I18N_CARDHOLDER_MAINTENANCE
} from '../constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import isEmpty from 'lodash.isempty';

// constants
import { NAME_VIEW, TELEPHONE_VIEW_LABEL, TYPE_EDIT } from './constants';

// action
import { handleToggle } from '../helpers';

// redux
import {
  selectIsExpandTelePhone,
  selectTelePhoneInfo
} from 'pages/AccountDetails/CardHolderMaintenance/_redux/selector';

// type
import { TelePhoneData } from 'pages/AccountDetails/CardHolderMaintenance/types';

// hooks
import { useDispatch } from 'react-redux';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

export interface TelePhoneProps {}

const TelePhone: React.FC<TelePhoneProps> = () => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const telephoneInfo = useStoreIdSelector<TelePhoneData>(selectTelePhoneInfo);
  const selectExpand = useStoreIdSelector<boolean>(selectIsExpandTelePhone);

  const testId = 'infobar_cardholder_tele-phone';

  const noData =
    isEmpty(telephoneInfo?.homePhone?.less) &&
    isEmpty(telephoneInfo?.workPhone?.less) &&
    isEmpty(telephoneInfo?.miscellaneous?.less) &&
    isEmpty(telephoneInfo?.mobilePhone?.less) &&
    isEmpty(telephoneInfo?.fax?.less);

  const changeToggle = () => {
    dispatch(handleToggle('isExpandTelePhone', storeId));
  };

  return (
    <SectionControl
      title={t(I18N_COMMON_TEXT.TELEPHONE)}
      classNamesView="ml-20"
      sectionKey={VIEW_NAME_CARD_MAINTENANCE.CARD_TELEPHONE}
      onToggle={changeToggle}
      isExpand={selectExpand}
      isEdit={checkPermission(
        PERMISSIONS.CARDHOLDER_MAINTENANCE_EDIT_PHONE,
        storeId
      )}
      typeEdit={TYPE_EDIT.PHONE}
      textTooltipEdit={I18N_CARDHOLDER_MAINTENANCE.EDIT_PHONE}
      dataTestId={testId}
    >
      {noData ? (
        <p
          className="color-grey mt-16"
          data-testid={genAmtId(testId!, 'no-data', 'TelePhone')}
        >
          {t(I18N_COMMON_TEXT.NO_DATA)}
        </p>
      ) : (
        <div className="row">
          <div className="col-6">
            <ViewPhone
              data={telephoneInfo?.homePhone}
              label={t(TELEPHONE_VIEW_LABEL.HOME_PHONE)}
              nameView={NAME_VIEW.HOME}
            />
          </div>
          <div className="col-6">
            <ViewPhone
              data={telephoneInfo?.workPhone}
              label={t(TELEPHONE_VIEW_LABEL.WORK_PHONE)}
              nameView={NAME_VIEW.WORK}
            />
          </div>
          <div className="col-6">
            <ViewPhone
              data={telephoneInfo?.mobilePhone}
              label={t(TELEPHONE_VIEW_LABEL.MOBILE_PHONE)}
              nameView={NAME_VIEW.MOBILE}
            />
          </div>
          <div className="col-6">
            <ViewPhone
              data={telephoneInfo?.fax}
              label={t(TELEPHONE_VIEW_LABEL.FAX)}
              nameView={NAME_VIEW.FAX}
            />
          </div>
          <div className="col-6">
            <ViewPhone
              data={telephoneInfo?.miscellaneous}
              label={t(TELEPHONE_VIEW_LABEL.MISCELLANEOUS)}
              nameView={NAME_VIEW.MISCELLANEOUS}
            />
          </div>
        </div>
      )}
    </SectionControl>
  );
};

export default TelePhone;
