import { dataField, formatPostData } from './helpers';
import { DATA_FIELD, DATA_TYPE } from './constants';
const values = {
  fax: {
    less: '4234234234',
    more: {
      deviceType: { value: 'U', description: 'U - Unknown Type' },
      flag: {
        value: 'Y',
        description: 'Y - Number Is Valid; Manual Dial Only'
      },
      lastUpdated: '04/29/2021'
    }
  },
  homePhone: {
    less: '0895565655',
    more: {
      deviceType: { value: 'I', description: 'I - Invalid Number' },
      flag: {
        value: 'C',
        description: 'C - Do Not Contact Cease And Desist'
      },
      lastUpdated: '05/04/2021'
    }
  },
  miscellaneous: {
    less: '7987979797',
    more: {
      deviceType: { value: 'L', description: 'L - Land Line' },
      flag: { value: 'V', description: 'V - Number Is Not Verified' },
      lastUpdated: '04/28/2021'
    }
  },
  mobilePhone: {
    less: '3232323232',
    more: {
      deviceType: { value: 'I', description: 'I - Invalid Number' },
      flag: {
        value: 'B',
        description: 'B - Consent For SMS Text Messages And Voice'
      },
      lastUpdated: '05/07/2021'
    }
  },
  workPhone: {
    less: '',
    more: {
      deviceType: null,
      flag: null,
      lastUpdated: null
    }
  }
};
describe('Test Action dataField', () => {
  it('Should have DATA_TYPE and DATA_FIELD is HOME', async () => {
    const data = dataField(DATA_TYPE.HOME);
    expect(data).toEqual(DATA_FIELD.HOME);
  });

  it('Should have DATA_TYPE and DATA_FIELD is WORK', async () => {
    const data = dataField(DATA_TYPE.WORK);
    expect(data).toEqual(DATA_FIELD.WORK);
  });

  it('Should have DATA_TYPE and DATA_FIELD is MOBILE', async () => {
    const data = dataField(DATA_TYPE.MOBILE);
    expect(data).toEqual(DATA_FIELD.MOBILE);
  });

  it('Should have DATA_TYPE and DATA_FIELD is FAX', async () => {
    const data = dataField(DATA_TYPE.FAX);
    expect(data).toEqual(DATA_FIELD.FAX);
  });

  it('Should have DATA_TYPE and DATA_FIELD is MISCELLANEOUS', async () => {
    const data = dataField(DATA_TYPE.MISCELLANEOUS);
    expect(data).toEqual(DATA_FIELD.MISCELLANEOUS);
  });

  it('Should have DATA_TYPE and DATA_FIELD is HOME with input empty ', async () => {
    const data = dataField('');
    expect(data).toEqual(DATA_FIELD.HOME);
  });

  it('Should have formatPostData ', async () => {
    const data = formatPostData(values as any);
    expect(data).toEqual({
      primaryCustomerHomePhoneIdentifier: '0895565655',
      homePhoneDeviceCode: 'I',
      homePhoneLastUpdatedDate: '05/04/2021',
      homePhoneStatusCode: 'C',
      primaryCustomerSecondPhoneIdentifier: ' ',
      businessPhoneDeviceCode: undefined,
      businessPhoneLastUpdatedDate: null,
      businessTelephoneFlagCode: undefined,
      mobilePhoneIdentifier: '3232323232',
      mobilePhoneDeviceCode: 'I',
      mobilePhoneLastUpdatedDate: '05/07/2021',
      mobilePhoneFlagCode: 'B',
      facsimilePhoneIdentifier: '4234234234',
      facsimilePhoneDeviceCode: 'U',
      facsimileLastUpdatedDate: '04/29/2021',
      facsimilePhoneFlagCode: 'Y',
      clientControlledPhoneIdentifier: '7987979797',
      clientControlledPhoneDeviceCode: 'L',
      clientControlledPhoneLastUpdatedDate: '04/28/2021',
      clientControlledPhoneFlagIndicator: 'V'
    });
  });
});
