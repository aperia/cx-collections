import React from 'react';
import { screen } from '@testing-library/react';

// utils
import {
  storeId,
  renderMockStoreId
} from 'app/test-utils/renderComponentWithMockStore';

// components
import TelePhone from 'pages/AccountDetails/CardHolderMaintenance/TelePhone';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// services
import * as services from '../helpers';
import { defaultStateCardholder } from '../_redux';

let spy1: jest.SpyInstance;

afterEach(() => {
  if (spy1) {
    spy1.mockReset();
    spy1.mockRestore();
  }
});

const initialState: Partial<RootState> = {
  cardholderMaintenance: {
    [storeId]: {
      ...defaultStateCardholder,
      cardholderId: {
        cardNumberValue: 'VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)',
        cardholderName: 'ENDY2,HARRYY T',
        cardholderRole: '01',
        customerExternalIdentifier: 'C20273234913858500085018',
        customerRoleTypeCode: '01',
        customerType: '01',
        id: 'C20273234913858500085018',
        memberSequence: '1',
        status: 'status',
        statusCode: 'statusCode'
      },
      cardholders: [
        {
          cardholderId: 'C20273234913858500085018',
          cardholderInfo: {
            authCBRFlag: 'Do not report',
            cardNumber: '603530******0985',
            cardholderRole: '02',
            dateOfBirth: '********0001',
            embossingName: '0000000000000000',
            firstName: '',
            lastName: '',
            memberSequence: '00002',
            middleName: '',
            prefix: '',
            qualification: '',
            salutation: 'Unknown Gender',
            solicitation: 'Solicitation Allowed',
            ssnTaxId: '',
            suffix: '',
            title: ''
          },
          email: {},
          telephone: {
            homePhone: { less: 'homePhone less' },
            workPhone: { less: 'workPhone less' },
            mobilePhone: { less: 'mobilePhone less' },
            miscellaneous: { less: 'miscellaneous less' },
            fax: { less: 'fax less' }
          }
        }
      ],
      isLoading: false
    }
  }
};

describe('Test TelePhone', () => {
  it('Render UI', () => {
    renderMockStoreId(<TelePhone />, { initialState });
  });

  it('Render UI with empty data', () => {
    renderMockStoreId(<TelePhone />, {});

    expect(screen.getByText(I18N_COMMON_TEXT.NO_DATA)).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('changeToggle', async () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy1 = jest.spyOn(services, 'handleToggle').mockImplementation(mockAction);

    renderMockStoreId(<TelePhone />, { initialState });
    screen.getAllByRole('button')[0].click();

    expect(mockAction).toBeCalledWith('isExpandTelePhone', storeId);
  });
});
