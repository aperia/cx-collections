import { FormatPostData } from '../types';
import { DATA_FIELD, DATA_TYPE } from './constants';

export const dataField = (name: string) => {
  switch (name) {
    case DATA_TYPE.HOME:
      return DATA_FIELD.HOME;
    case DATA_TYPE.WORK:
      return DATA_FIELD.WORK;
    case DATA_TYPE.MOBILE:
      return DATA_FIELD.MOBILE;
    case DATA_TYPE.FAX:
      return DATA_FIELD.FAX;
    case DATA_TYPE.MISCELLANEOUS:
      return DATA_FIELD.MISCELLANEOUS;
    default:
      return DATA_FIELD.HOME;
  }
};

export const formatPostData = (data: FormatPostData) => {
  const { homePhone, workPhone, mobilePhone, fax, miscellaneous } = data;
  return {
    primaryCustomerHomePhoneIdentifier: homePhone?.less || ' ',
    homePhoneDeviceCode: homePhone?.more?.deviceType?.value,
    homePhoneLastUpdatedDate: homePhone?.more?.lastUpdated,
    homePhoneStatusCode: homePhone?.more?.flag?.value,
    primaryCustomerSecondPhoneIdentifier: workPhone?.less || ' ',
    businessPhoneDeviceCode: workPhone?.more?.deviceType?.value,
    businessPhoneLastUpdatedDate: workPhone?.more?.lastUpdated,
    businessTelephoneFlagCode: workPhone?.more?.flag?.value,
    mobilePhoneIdentifier: mobilePhone?.less || ' ',
    mobilePhoneDeviceCode: mobilePhone?.more?.deviceType?.value,
    mobilePhoneLastUpdatedDate: mobilePhone?.more?.lastUpdated,
    mobilePhoneFlagCode: mobilePhone?.more?.flag?.value,
    facsimilePhoneIdentifier: fax?.less || ' ',
    facsimilePhoneDeviceCode: fax?.more?.deviceType?.value,
    facsimileLastUpdatedDate: fax?.more?.lastUpdated,
    facsimilePhoneFlagCode: fax?.more?.flag?.value,
    clientControlledPhoneIdentifier: miscellaneous?.less || ' ',
    clientControlledPhoneDeviceCode: miscellaneous?.more?.deviceType?.value,
    clientControlledPhoneLastUpdatedDate: miscellaneous?.more?.lastUpdated,
    clientControlledPhoneFlagIndicator: miscellaneous?.more?.flag?.value
  };
};
