import React, { useEffect, useMemo, useState } from 'react';

// components/types
import {
  Grid,
  GridRef,
  Icon,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { IAddressInfo, ModalExpandedAddress } from '../types';

// utils
import { isArray, isFunction, debounce } from 'app/_libraries/_dls/lodash';
import { MODAL_ADDRESS_STANDARD } from '../ExpandedAddress/constants';

// redux
import { cardholderMaintenanceActions } from '../_redux';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useDispatch } from 'react-redux';
import {
  isEndLoadMore,
  isLoadingExpandedAddress,
  selectExpandedAddressHistory,
  selectModalExpandedAddress
} from '../_redux/selector';
import { formatTimeDefault, isArrayHasValue } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { FormatTime } from 'app/constants/enums';
export interface HistoryProps {
  onCloseModal: () => void;
  isExpandedAddress?: boolean;
  dataTestId?: string;
}

const History: React.FC<HistoryProps> = ({
  onCloseModal,
  isExpandedAddress = true,
  dataTestId
}) => {
  const { storeId, accEValue } = useAccountDetail();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const expandedAddressHistory = useStoreIdSelector<IAddressInfo[]>(
    selectExpandedAddressHistory
  );
  const isLoading = useStoreIdSelector<boolean>(isLoadingExpandedAddress);
  const isEnd = useStoreIdSelector<boolean>(isEndLoadMore);
  const openModal = useStoreIdSelector<ModalExpandedAddress>(
    selectModalExpandedAddress
  );

  const testId = genAmtId(
    dataTestId!,
    'history-address-modal',
    'CardholderMaintenance.HistoryAddress'
  );
  const step = 25;
  const [startSequence, setStartSequence] = useState(1);
  const [endSequence, setEndSequence] = useState(step);
  const [gridRef, setGridRef] = useState<GridRef | null>(null);
  const [height, setHeight] = useState(0);

  const expandedHistoryGridData = useMemo(() => {
    if (!isArrayHasValue(expandedAddressHistory)) return [];

    return expandedAddressHistory.map((addressInfo: IAddressInfo) => {
      let addressCombined = `${addressInfo.houseBuilding}, ${addressInfo.houseNumber}, ${addressInfo.streetName}, ${addressInfo.POBox}, ${addressInfo?.addressLineOne}, ${addressInfo?.addressLineTwo}, ${addressInfo.addressLineThree}, ${addressInfo.addressLineFour}, ${addressInfo?.cityName},
        ${addressInfo?.stateSubdivision?.fieldID} ${addressInfo?.postalCode}, ${addressInfo?.countryISO.description}`;

      addressCombined = addressCombined
        .split('undefined')
        .join('')
        .split(',')
        .filter(str => str.trim())
        .join(',');

      return {
        addressCategory: addressInfo?.addressCategory?.description,
        addressType: addressInfo?.addressType?.description,
        effectiveFrom: addressInfo?.effectiveFrom,
        effectiveTo: addressInfo?.effectiveTo,
        addressRelationship: addressInfo?.addressRelationship.description,
        attention: addressInfo?.attention,
        address: addressCombined
      };
    });
  }, [expandedAddressHistory]);

  const columns = useMemo(() => {
    const data = [
      {
        id: 'addressCategory',
        Header: t('txt_address_category'),
        accessor: 'addressCategory',
        width: 158
      },
      {
        id: 'addressType',
        Header: t('txt_address_type'),
        accessor: 'addressType',
        width: 160
      },
      {
        id: 'effectiveFrom',
        Header: t('txt_effective_from'),
        width: 140,
        accessor: (item: IAddressInfo) => {
          return formatTimeDefault(item.effectiveFrom, FormatTime.Date);
        }
      },
      {
        id: 'effectiveTo',
        Header: t('txt_effective_to'),
        width: 120,
        accessor: (item: IAddressInfo) => {
          return formatTimeDefault(item.effectiveTo, FormatTime.Date);
        }
      },
      {
        id: 'addressRelationship',
        Header: t('txt_address_relation_ship'),
        accessor: 'addressRelationship',
        width: 190
      },
      {
        id: 'attention',
        Header: t('txt_attention'),
        accessor: 'attention',
        width: 141
      },
      {
        id: 'address',
        Header: t('txt_address'),
        accessor: 'address',
        width: 300,
        autoWidth: true
      }
    ];

    return isExpandedAddress ? data : [...data.slice(0, 4), ...data.slice(6)];
  }, [t, isExpandedAddress]);

  useEffect(() => {
    dispatch(
      cardholderMaintenanceActions.getAddressHistory({
        storeId,
        accEValue,
        startSequence,
        endSequence,
        ignoreInitialState: true
      })
    );
  }, [dispatch, storeId, accEValue, endSequence, startSequence]);

  const handleLoadMore = () => {
    if (isEnd) return;
    setStartSequence(state => state + step);
    setEndSequence(state => state + step);
  };

  const handleScrollToTop = () => {
    gridRef?.gridBodyElement?.scrollTo({ top: 0 });
  };

  const handleCloseModal = () => {
    dispatch(
      cardholderMaintenanceActions.clearExpandedAddressHistory({
        storeId
      })
    );
    isFunction(onCloseModal) && onCloseModal();
  };

  useEffect(() => {
    const resizeEvent = debounce(
      () => {
        setHeight(window.innerHeight - 253);
      },
      250,
      { leading: false, trailing: true }
    );

    resizeEvent();

    window.addEventListener('resize', resizeEvent);
    return () => window.removeEventListener('resize', resizeEvent);
  }, []);

  return (
    <Modal
      full
      show={openModal === MODAL_ADDRESS_STANDARD.VIEW}
      animationDuration={500}
      loading={isLoading}
      dataTestId={testId}
    >
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModal}
        dataTestId={`${testId}-header`}
      >
        <ModalTitle dataTestId={`${testId}-header-title`}>
          {t('txt_address_history')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <>
          <h5 data-testid={genAmtId(testId!, 'body_address-list-title', '')}>
            {t('txt_address_list')}
          </h5>
          {isArray(expandedAddressHistory) && expandedAddressHistory.length ? (
            <>
              <p
                className="my-16"
                data-testid={genAmtId(
                  testId!,
                  'body_total',
                  'CardholderMaintenance.HistoryAddress'
                )}
              >
                {t('txt_total_address_list', {
                  count: expandedHistoryGridData?.length
                })}
              </p>
              <Grid
                maxHeight={height}
                ref={setGridRef}
                columns={columns}
                data={expandedHistoryGridData}
                onLoadMore={handleLoadMore}
                dataTestId={`${testId}-body_address-list`}
                loadElement={
                  <>
                    {isLoading && (
                      <tr
                        className="row-loading"
                        data-testid={genAmtId(
                          testId!,
                          'body_loading-more',
                          'CardholderMaintenance.HistoryAddress'
                        )}
                      >
                        <td>
                          <span className="loading" />
                          <span>{t('txt_loading_more_results')}</span>
                        </td>
                      </tr>
                    )}
                    {isEnd && (
                      <tr className="text-center">
                        <td>
                          <span className="mr-4">
                            {t('txt_end_of_address_history')}
                          </span>
                          <a
                            className="link text-decoration-none"
                            onClick={handleScrollToTop}
                            data-testid={genAmtId(
                              testId!,
                              'back-to-top',
                              'CardholderMaintenance.HistoryAddress'
                            )}
                          >
                            {t('txt_back_to_top')}
                          </a>
                        </td>
                      </tr>
                    )}
                  </>
                }
              />
            </>
          ) : !isLoading ? (
            <div
              className="text-center my-80"
              data-testid={genAmtId(
                testId!,
                'body_no-data',
                'CardholderMaintenance.HistoryAddress'
              )}
            >
              <Icon name="file" className="fs-80 color-light-l12" />
              <p className="mt-20 text-secondary">
                {t('txt_no_address_history_to_display')}
              </p>
            </div>
          ) : null}
        </>
      </ModalBody>
      <ModalFooter
        border
        cancelButtonText={t('txt_close')}
        onCancel={handleCloseModal}
        dataTestId={`${testId}-footer`}
      />
    </Modal>
  );
};

export default History;
