import React from 'react';
import { fireEvent, screen } from '@testing-library/react';

// Component
import History from './index';

// Helper
import {
  storeId,
  renderMockStoreId,
  queryByClass,
  queryAllBy,
  mockActionCreator
} from 'app/test-utils';

// redux
import {
  cardholderMaintenanceActions,
  defaultStateCardholder
} from '../_redux';

jest.mock('resize-observer-polyfill', () =>
  jest.requireActual('app/test-utils/mocks/resizeObserver.ts')
);

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponent = jest.requireActual('app/_libraries/_dls/components');
  const { MockGrid } = jest.requireActual('app/test-utils/mocks/MockGrid');

  return {
    ...dlsComponent,
    Grid: MockGrid
  };
});

const histories = [
  {
    POBox: '',
    addressCategory: {
      fieldValue: 'Permanent',
      fieldID: 'P',
      description: 'P - Permanent'
    },
    addressLineFour: '',
    addressLineOne: '111',
    addressLineThree: '',
    addressLineTwo: '',
    addressRelationship: {
      fieldValue: 'Unknown',
      fieldID: 'U',
      description: 'U - Unknown'
    },
    addressType: {
      fieldValue: 'Billing',
      fieldID: 'BLL1',
      description: 'BLL1 - Billing'
    },
    attention: '',
    cityName: 'PHOENIX',
    countryISO: {
      fieldValue: 'United States of America',
      fieldID: 'USA',
      description: 'USA - United States of America'
    },
    effectiveFrom: '2021-05-19',
    effectiveTo: '2021-05-20',
    formattedAddressCode: 'F',
    houseBuilding: undefined,
    houseNumber: '',
    id: '00001-P-BLL1-2021-05-19-2021-05-20',
    memberSequence: '00001',
    postalCode: '71601',
    stateSubdivision: {
      fieldValue: 'Arkansas',
      fieldID: 'AR',
      countryCode: 'USA',
      description: 'AR - Arkansas'
    },
    streetName: ''
  }
];

const generateState = (data?: any) => {
  return {
    cardholderMaintenance: {
      [storeId]: {
        ...defaultStateCardholder,
        expandedAddress: {
          histories: [],
          openModal: 'HISTORY',
          isLoading: false,
          isEndLoadMore: false,
          ...data
        },
        cardholderId: {
          cardNumberValue: 'VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)',
          cardholderName: 'ENDY2,HARRYY T',
          cardholderRole: '01',
          customerExternalIdentifier: 'C20273234913858500085018',
          customerRoleTypeCode: '01',
          customerType: '01',
          id: 'C20273234913858500085018',
          memberSequence: '0001',
          status: '',
          statusCode: ''
        },
        isLoading: true
      }
    }
  } as Partial<RootState>;
};

const cardHolderSpy = mockActionCreator(cardholderMaintenanceActions);

Object.defineProperties(window.HTMLTableSectionElement.prototype, {
  offsetHeight: { get: () => 100 }
});

Object.defineProperty(window, 'innerHeight', {
  writable: true,
  value: 303
});

jest.mock('lodash.debounce', () => {
  return (fn: Function) => () => fn();
});

describe('Test History', () => {
  it('Should have isLoading', () => {
    cardHolderSpy('getAddressHistory');
    const { wrapper } = renderMockStoreId(<History onCloseModal={() => {}} />, {
      initialState: generateState({ isLoading: true })
    });
    const queryByLoading = queryByClass(
      wrapper.baseElement as HTMLElement,
      'modal-content loading'
    );
    expect(queryByLoading).toBeInTheDocument();
  });

  it('Should have empty data', () => {
    cardHolderSpy('getAddressHistory');
    renderMockStoreId(<History onCloseModal={() => {}} />, {
      initialState: generateState()
    });
    const queryByText = screen.queryByText('txt_total_address_list');
    expect(queryByText).not.toBeInTheDocument();
  });

  it('Should have empty data with txt_no_address_history_to_display', () => {
    cardHolderSpy('getAddressHistory');
    renderMockStoreId(<History onCloseModal={() => {}} />, {
      initialState: generateState({ isLoading: false })
    });
    const queryByText = screen.queryByText('txt_no_address_history_to_display');
    expect(queryByText).toBeInTheDocument();
  });

  it('Should have data', () => {
    cardHolderSpy('getAddressHistory');
    renderMockStoreId(<History onCloseModal={() => {}} />, {
      initialState: generateState({ histories })
    });
    const queryByText = screen.queryByText('txt_total_address_list');
    expect(queryByText).toBeInTheDocument();
  });

  it('Should have Column isExpandedAddress = true', () => {
    cardHolderSpy('getAddressHistory');
    const { wrapper } = renderMockStoreId(<History onCloseModal={() => {}} />, {
      initialState: generateState({ histories })
    });
    const queryHeader = queryAllBy(
      wrapper.baseElement as HTMLElement,
      'role',
      'columnheader'
    );
    expect(queryHeader.length).toEqual(7);
  });

  it('Should have Column isExpandedAddress = false', () => {
    cardHolderSpy('getAddressHistory');
    const { wrapper } = renderMockStoreId(
      <History isExpandedAddress={false} onCloseModal={() => {}} />,
      {
        initialState: generateState({ histories })
      }
    );
    const queryHeader = queryAllBy(
      wrapper.baseElement as HTMLElement,
      'role',
      'columnheader'
    );
    expect(queryHeader.length).toEqual(5);
  });

  it('Should have click close button close', () => {
    cardHolderSpy('getAddressHistory');
    const mockClearAddressHistory = cardHolderSpy(
      'clearExpandedAddressHistory'
    );
    const onCloseModal = jest.fn();
    renderMockStoreId(<History onCloseModal={onCloseModal} />, {
      initialState: generateState({ histories })
    });
    const queryButtonClose = screen.queryByText('txt_close');
    queryButtonClose!.click();
    expect(mockClearAddressHistory).toBeCalledWith({ storeId });
    expect(onCloseModal).toHaveBeenCalled();
  });

  it('Should have click close icon close', () => {
    cardHolderSpy('getAddressHistory');
    const mockClearAddressHistory = cardHolderSpy(
      'clearExpandedAddressHistory'
    );
    const onCloseModal = jest.fn();
    const { wrapper } = renderMockStoreId(
      <History onCloseModal={onCloseModal} />,
      {
        initialState: generateState({ histories })
      }
    );
    const queryIconClose = queryByClass(
      wrapper.baseElement as HTMLElement,
      /icon icon-close/
    );
    queryIconClose!.click();
    expect(mockClearAddressHistory).toBeCalledWith({ storeId });
    expect(onCloseModal).toHaveBeenCalled();
  });

  it('Should have handleScrollToTop', () => {
    cardHolderSpy('getAddressHistory');

    Object.defineProperty(window, 'innerHeight', {
      writable: true,
      value: 303
    });

    renderMockStoreId(
      <History isExpandedAddress={false} onCloseModal={() => {}} />,
      {
        initialState: generateState({
          histories,
          isLoading: false,
          isEndLoadMore: true
        })
      }
    );
    const queryTextEndOfHistory = screen.queryByText(
      'txt_end_of_address_history'
    );
    const queryBackToTop = screen.queryByText('txt_back_to_top');
    queryBackToTop!.click();
    expect(queryTextEndOfHistory).toBeInTheDocument();
  });

  it('Should have without load more', () => {
    const getAddressHistory = cardHolderSpy('getAddressHistory');
    Object.defineProperty(window, 'innerHeight', {
      writable: true,
      value: 453
    });
    const { wrapper } = renderMockStoreId(
      <History isExpandedAddress={false} onCloseModal={() => {}} />,
      {
        initialState: generateState({
          histories,
          isLoading: false,
          isEndLoadMore: true
        })
      }
    );
    fireEvent.scroll(
      queryByClass(wrapper.baseElement as HTMLElement, /dls-grid-body/)!,
      {
        target: {},
        currentTarget: { scrollTop: 1000 }
      }
    );
    expect(getAddressHistory).toHaveBeenCalledTimes(1);
  });

  it('Should have load more', () => {
    const getAddressHistory = cardHolderSpy('getAddressHistory');
    Object.defineProperty(window, 'innerHeight', {
      writable: true,
      value: 453
    });
    const { wrapper } = renderMockStoreId(<History onCloseModal={() => {}} />, {
      initialState: generateState({
        histories,
        isLoading: true,
        isEndLoadMore: false
      })
    });
    const textLoadMore = screen.queryByText('txt_loading_more_results');
    const queryRowLoading = queryByClass(
      wrapper.baseElement as HTMLElement,
      'row-loading'
    );
    fireEvent.scroll(
      queryByClass(wrapper.baseElement as HTMLElement, /dls-grid-body/)!,
      {
        target: {},
        currentTarget: { scrollTop: 1000 }
      }
    );

    expect(textLoadMore).toBeInTheDocument();
    expect(queryRowLoading).toBeInTheDocument();
    expect(getAddressHistory).toHaveBeenCalledTimes(2);
  });
});
