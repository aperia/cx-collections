// constants
import { CARD_DEVICE_TYPE, CARD_FLAG_TYPE, CARD_ROLE } from 'app/constants';
import { isEmpty } from 'lodash';
import isUndefined from 'lodash.isundefined';
import { CHRONICLE_CARD_HOLDER_MAINTENANCE_KEY } from 'pages/ClientConfiguration/Memos/constants';
import { CARDHOLDER_MAINTENANCE_ACTIONS } from 'pages/Memos/constants';

// actions
import { memoActions } from 'pages/Memos/_redux/reducers';

// types
import { UpdateCardHolderArg } from '../types';

export const postMemoForPhone: AppThunk =
  (data: UpdateCardHolderArg, storeId: string) =>
  async (dispatch, getState) => {
    const { postData } = data;
    const { cardholderMaintenance, accountDetail } = getState();
    const cardholder = cardholderMaintenance[storeId]?.cardholderId;
    const cardholderList = cardholderMaintenance[storeId]?.cardholders;
    const allData = cardholderList.find(
      item => item.cardholderId === cardholder.id
    );
    const cardholderContactedName =
      cardholderMaintenance[storeId].cardholderId?.cardholderName;
    const telephone = allData?.telephone;
    const { homePhone, workPhone, mobilePhone, fax, miscellaneous } =
      telephone || {};

    // CASE: HOME PHONE CHANGES
    const {
      primaryCustomerHomePhoneIdentifier,
      homePhoneDeviceCode,
      homePhoneStatusCode
    } = postData?.data;

    const isPhoneChange =
      !isUndefined(primaryCustomerHomePhoneIdentifier) &&
      primaryCustomerHomePhoneIdentifier !== homePhone?.less;
    const isDeviceCodeChange =
      !isUndefined(homePhoneDeviceCode) &&
      homePhoneDeviceCode !== homePhone?.more?.deviceType?.value;
    const isStatusCodeChange =
      !isUndefined(homePhoneStatusCode) &&
      homePhoneStatusCode !== homePhone?.more?.flag?.value;

    if (isPhoneChange || isDeviceCodeChange || isStatusCodeChange) {
      const result = [];
      if (isPhoneChange) {
        result.push(`HOME PHONE CHANGED FROM ${homePhone?.less} TO ${primaryCustomerHomePhoneIdentifier}`);
        if (isEmpty(primaryCustomerHomePhoneIdentifier)) {
          result.push(`HOME DEVICE TYPE CHANGED FROM ${homePhone?.more?.deviceType?.description} TO `);
          result.push(`HOME FLAG CHANGED FROM ${homePhone?.more?.flag?.description} TO `);
        }
      }

      if (isDeviceCodeChange) {
        result.push(`HOME DEVICE TYPE CHANGED FROM ${homePhone?.more?.deviceType?.description} TO ${CARD_DEVICE_TYPE[homePhoneDeviceCode]}`);
      }

      if (isStatusCodeChange) {
        result.push(`HOME FLAG CHANGED FROM ${homePhone?.more?.flag?.description} TO ${CARD_FLAG_TYPE[homePhoneStatusCode]}`);
      }

      const contentEdit = result.join(', ');

      dispatch(
        memoActions.postMemo(
          CARDHOLDER_MAINTENANCE_ACTIONS.UPDATE_PHONE_NUMBER,
          {
            storeId,
            accEValue: postData?.common?.accountId,
            chronicleKey: CHRONICLE_CARD_HOLDER_MAINTENANCE_KEY,
            cardholderMaintenance: {
              cardholderContactedName,
              cardholderContactedNameAuth:
                accountDetail[storeId]?.selectedAuthenticationData
                  ?.customerName,
              role: CARD_ROLE[cardholder.cardholderRole],
              contentEdit
            }
          }
        )
      );
    }

    // CASE: WORK PHONE CHANGES
    const {
      primaryCustomerSecondPhoneIdentifier,
      businessPhoneDeviceCode,
      businessTelephoneFlagCode
    } = postData?.data;

    const isWorkPhoneChange =
      !isUndefined(primaryCustomerSecondPhoneIdentifier) &&
      primaryCustomerSecondPhoneIdentifier !== workPhone?.less;
    const isWorkDeviceCodeChange =
      !isUndefined(businessPhoneDeviceCode) &&
      businessPhoneDeviceCode !== workPhone?.more?.deviceType?.value;
    const isWorkStatusCodeChange =
      !isUndefined(businessTelephoneFlagCode) &&
      businessTelephoneFlagCode !== workPhone?.more?.flag?.value;

    if (isWorkPhoneChange || isWorkDeviceCodeChange || isWorkStatusCodeChange) {
      const result = [];

      if (isWorkPhoneChange) {
        result.push(`WORK PHONE CHANGED FROM ${workPhone?.less} TO ${primaryCustomerSecondPhoneIdentifier}`);
        if (isEmpty(primaryCustomerSecondPhoneIdentifier)) {
          result.push(`WORK DEVICE TYPE CHANGED FROM ${workPhone?.more?.deviceType?.description} TO `);
          result.push(`WORK FLAG CHANGED FROM ${workPhone?.more?.flag?.description} TO `);
        }
      }

      if (isWorkDeviceCodeChange) {
        result.push(`WORK DEVICE TYPE CHANGED FROM ${workPhone?.more?.deviceType?.description} TO ${CARD_DEVICE_TYPE[businessPhoneDeviceCode]}`);
      }

      if (isWorkStatusCodeChange) {
        result.push(`WORK FLAG CHANGED FROM ${workPhone?.more?.flag?.description} TO ${CARD_FLAG_TYPE[businessTelephoneFlagCode]}`);
      }

      const contentEdit = result.join(', ');

      dispatch(
        memoActions.postMemo(
          CARDHOLDER_MAINTENANCE_ACTIONS.UPDATE_PHONE_NUMBER,
          {
            storeId,
            accEValue: postData?.common?.accountId,
            chronicleKey: CHRONICLE_CARD_HOLDER_MAINTENANCE_KEY,
            cardholderMaintenance: {
              cardholderContactedName,
              cardholderContactedNameAuth:
                accountDetail[storeId]?.selectedAuthenticationData
                  ?.customerName,
              role: CARD_ROLE[cardholder.cardholderRole],
              contentEdit
            }
          }
        )
      );
    }

    // CASE: MOBILE PHONE CHANGES
    const {
      mobilePhoneIdentifier,
      mobilePhoneDeviceCode,
      mobilePhoneFlagCode
    } = postData?.data;

    const isMobilePhoneChange =
      !isUndefined(mobilePhoneIdentifier) &&
      mobilePhoneIdentifier !== mobilePhone?.less;
    const isMobileDeviceCodeChange =
      !isUndefined(mobilePhoneDeviceCode) &&
      mobilePhoneDeviceCode !== mobilePhone?.more?.deviceType?.value;
    const isMobileStatusCodeChange =
      !isUndefined(mobilePhoneFlagCode) &&
      mobilePhoneFlagCode !== mobilePhone?.more?.flag?.value;

    if (
      isMobilePhoneChange ||
      isMobileDeviceCodeChange ||
      isMobileStatusCodeChange
    ) {
      const result = [];

      if (isMobilePhoneChange) {
        result.push(`MOBILE PHONE CHANGED FROM ${mobilePhone?.less} TO ${mobilePhoneIdentifier}`);
        if (isEmpty(mobilePhoneIdentifier)) {
          result.push(`MOBILE DEVICE TYPE CHANGED FROM ${mobilePhone?.more?.deviceType?.description} TO `);
          result.push(`MOBILE FLAG CHANGED FROM ${mobilePhone?.more?.flag?.description} TO `);
        }
      }

      if (isMobileDeviceCodeChange) {
        result.push(`MOBILE DEVICE TYPE CHANGED FROM ${mobilePhone?.more?.deviceType?.description} TO ${CARD_DEVICE_TYPE[mobilePhoneDeviceCode]}`);
      }

      if (isMobileStatusCodeChange) {
        result.push(`MOBILE FLAG CHANGED FROM ${mobilePhone?.more?.flag?.description} TO ${CARD_FLAG_TYPE[mobilePhoneFlagCode]}`);
      }

      const contentEdit = result.join(', ');

      dispatch(
        memoActions.postMemo(
          CARDHOLDER_MAINTENANCE_ACTIONS.UPDATE_PHONE_NUMBER,
          {
            storeId,
            accEValue: postData?.common?.accountId,
            chronicleKey: CHRONICLE_CARD_HOLDER_MAINTENANCE_KEY,
            cardholderMaintenance: {
              cardholderContactedName,
              cardholderContactedNameAuth:
                accountDetail[storeId]?.selectedAuthenticationData
                  ?.customerName,
              role: CARD_ROLE[cardholder.cardholderRole],
              contentEdit
            }
          }
        )
      );
    }

    // CASE: FAX CHANGES
    const {
      facsimilePhoneIdentifier,
      facsimilePhoneDeviceCode,
      facsimilePhoneFlagCode
    } = postData?.data;

    const isFaxChange =
      !isUndefined(facsimilePhoneIdentifier) &&
      facsimilePhoneIdentifier !== fax?.less;
    const isFaxDeviceCodeChange =
      !isUndefined(facsimilePhoneDeviceCode) &&
      facsimilePhoneDeviceCode !== fax?.more?.deviceType?.value;
    const isFaxStatusCodeChange =
      !isUndefined(facsimilePhoneFlagCode) &&
      facsimilePhoneFlagCode !== fax?.more?.flag?.value;

    if (isFaxChange || isFaxDeviceCodeChange || isFaxStatusCodeChange) {
      const result = [];

      if (isFaxChange) {
        result.push(`FAX CHANGED FROM ${fax?.less} TO ${facsimilePhoneIdentifier}`);
        if (isEmpty(facsimilePhoneIdentifier)) {
          result.push(`FAX DEVICE TYPE CHANGED FROM ${fax?.more?.deviceType?.description} TO `);
          result.push(`FAX FLAG CHANGED FROM ${fax?.more?.flag?.description} TO `);
        }
      }

      if (isFaxDeviceCodeChange) {
        result.push(`FAX DEVICE TYPE CHANGED FROM ${fax?.more?.deviceType?.description} TO ${CARD_DEVICE_TYPE[facsimilePhoneDeviceCode]}`);
      }

      if (isFaxStatusCodeChange) {
        result.push(`FAX FLAG CHANGED FROM ${fax?.more?.flag?.description} TO ${CARD_FLAG_TYPE[facsimilePhoneFlagCode]}`);
      }

      const contentEdit = result.join(', ');

      dispatch(
        memoActions.postMemo(
          CARDHOLDER_MAINTENANCE_ACTIONS.UPDATE_PHONE_NUMBER,
          {
            storeId,
            accEValue: postData?.common?.accountId,
            chronicleKey: CHRONICLE_CARD_HOLDER_MAINTENANCE_KEY,
            cardholderMaintenance: {
              cardholderContactedName,
              cardholderContactedNameAuth:
                accountDetail[storeId]?.selectedAuthenticationData
                  ?.customerName,
              role: CARD_ROLE[cardholder.cardholderRole],
              contentEdit
            }
          }
        )
      );
    }

    // CASE: MISCELLANEOUS
    const {
      clientControlledPhoneIdentifier,
      clientControlledPhoneDeviceCode,
      clientControlledPhoneFlagIndicator
    } = postData?.data;

    const isMiscellaneousChange =
      !isUndefined(clientControlledPhoneIdentifier) &&
      clientControlledPhoneIdentifier !== miscellaneous?.less;
    const isMiscellaneousDeviceCodeChange =
      !isUndefined(clientControlledPhoneDeviceCode) &&
      clientControlledPhoneDeviceCode !==
        miscellaneous?.more?.deviceType?.value;
    const isMiscellaneousStatusCodeChange =
      !isUndefined(clientControlledPhoneFlagIndicator) &&
      clientControlledPhoneFlagIndicator !== miscellaneous?.more?.flag?.value;

    if (
      isMiscellaneousChange ||
      isMiscellaneousDeviceCodeChange ||
      isMiscellaneousStatusCodeChange
    ) {
      const result = [];

      if (isMiscellaneousChange) {
        result.push(`MISCELLANEOUS CHANGED FROM ${miscellaneous?.less} TO ${clientControlledPhoneIdentifier}`);
        if (isEmpty(clientControlledPhoneIdentifier)) {
          result.push(`MISCELLANEOUS DEVICE TYPE CHANGED FROM ${miscellaneous?.more?.deviceType?.description} TO `);
          result.push(`MISCELLANEOUS FLAG CHANGED FROM ${miscellaneous?.more?.flag?.description} TO `);
        }
      }

      if (isMiscellaneousDeviceCodeChange) {
        result.push(`MISCELLANEOUS DEVICE TYPE CHANGED FROM ${miscellaneous?.more?.deviceType?.description} TO ${CARD_DEVICE_TYPE[clientControlledPhoneDeviceCode]}`);
      }

      if (isMiscellaneousStatusCodeChange) {
        result.push(`MISCELLANEOUS FLAG CHANGED FROM ${miscellaneous?.more?.flag?.description} TO ${CARD_FLAG_TYPE[clientControlledPhoneFlagIndicator]}`);
      }

      const contentEdit = result.join(', ');

      dispatch(
        memoActions.postMemo(
          CARDHOLDER_MAINTENANCE_ACTIONS.UPDATE_PHONE_NUMBER,
          {
            storeId,
            accEValue: postData?.common?.accountId,
            chronicleKey: CHRONICLE_CARD_HOLDER_MAINTENANCE_KEY,
            cardholderMaintenance: {
              cardholderContactedName,
              cardholderContactedNameAuth:
                accountDetail[storeId]?.selectedAuthenticationData
                  ?.customerName,
              role: CARD_ROLE[cardholder.cardholderRole],
              contentEdit
            }
          }
        )
      );
    }
  };
