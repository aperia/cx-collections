import { I18N_CARDHOLDER_MAINTENANCE } from './../constants';
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import cardholdersMaintenanceService from '../cardholdersMaintenanceService';
import {
  DeleteExpandedAddressArgs,
  IInitStateCardholder,
  IRequestDeleteExpandedAddress
} from '../types';
import { formatTimeDefault } from 'app/helpers';
import { FormatTime } from 'app/constants/enums';
import { cardholderMaintenanceActions } from '.';
import { selectCardholderAddressInfo } from './selector';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { CARDHOLDER_MAINTENANCE_ACTIONS } from 'pages/Memos/constants';
import { generateMemoAddressValue } from '../helpers';
import { batch } from 'react-redux';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { CARD_ROLE } from 'app/constants';
import { CHRONICLE_CARD_HOLDER_MAINTENANCE_KEY } from 'pages/ClientConfiguration/Memos/constants';

export const triggerDeleteExpandedAddress = createAsyncThunk<
  void,
  DeleteExpandedAddressArgs,
  ThunkAPIConfig
>(
  'cardholderMaintenance/triggerDeleteExpandedAddress',
  async (args, thunkAPI) => {
    const { dispatch } = thunkAPI;
    const { storeId, accEValue } = args;
    const { cardholderMaintenance, accountDetail } = thunkAPI.getState();
    const cardholderId = cardholderMaintenance[storeId].cardholderId;
    const addressType = cardholderMaintenance[storeId].addressType;
    const { deleteAddressExpandedAddress } = cardholdersMaintenanceService;
    const { getAddressInfo } = cardholderMaintenanceActions;
    const data: IRequestDeleteExpandedAddress = {
      common: {
        externalCustomerId: cardholderId.customerExternalIdentifier,
        accountId: cardholderId.cardNumberValue
      },
      memberSequenceIdentifier: cardholderId.memberSequence,
      customerType: cardholderId.customerType,
      addressType: addressType.addressType?.fieldID,
      categoryCode: addressType.addressCategory?.fieldID,
      effectiveFromDate: formatTimeDefault(
        addressType.effectiveFrom,
        FormatTime.DefaultPostDate
      ),
      effectiveToDate: formatTimeDefault(
        addressType.effectiveTo,
        FormatTime.DefaultPostDate
      )
    };

    try {
      await deleteAddressExpandedAddress({
        ...data
      });
      // auto memo
      const addressData = selectCardholderAddressInfo(
        thunkAPI.getState(),
        storeId
      );

      dispatch(
        memoActions.postMemo(
          CARDHOLDER_MAINTENANCE_ACTIONS.DELETE_EXPANDED_ADDRESS,
          {
            storeId,
            accEValue: accEValue,
            chronicleKey: CHRONICLE_CARD_HOLDER_MAINTENANCE_KEY,
            cardholderMaintenance: {
              cardholderContactedName:
                cardholderMaintenance[storeId].cardholderId?.cardholderName,
              cardholderContactedNameAuth:
                accountDetail[storeId]?.selectedAuthenticationData
                  ?.customerName,
              role: CARD_ROLE[
                cardholderMaintenance[storeId]?.cardholderId?.cardholderRole
              ],
              deleteValue: generateMemoAddressValue(addressData)
            }
          }
        )
      );
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_CARDHOLDER_MAINTENANCE.ADDRESS_DELETED
        })
      );
      dispatch(getAddressInfo({ storeId }));
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inAddressCardholderFlyOut'
        })
      );
    } catch (error) {
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'error',
            message: I18N_CARDHOLDER_MAINTENANCE.ADDRESS_FAILED_TO_DELETE
          })
        );
        dispatch(
          apiErrorNotificationAction.updateApiError({
            storeId,
            forSection: 'inAddressCardholderFlyOut',
            apiResponse: error
          })
        );
      });
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const deleteExpandedAddressBuilder = (
  builder: ActionReducerMapBuilder<IInitStateCardholder>
) => {
  builder
    .addCase(triggerDeleteExpandedAddress.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].expandedAddress.isLoading = true;
    })
    .addCase(triggerDeleteExpandedAddress.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].expandedAddress.isLoading = false;
      draftState[storeId].expandedAddress.openModal = '';
    })
    .addCase(triggerDeleteExpandedAddress.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].expandedAddress.isLoading = false;
      draftState[storeId].expandedAddress.openModal = '';
    });
};
