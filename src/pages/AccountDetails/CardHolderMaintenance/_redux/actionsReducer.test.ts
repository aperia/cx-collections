import { cardholderMaintenanceActions } from './index';
import { storeId, mockCreateStore } from 'app/test-utils';
import { IAddressType, ICardholderId } from '../types';

describe('Test Actions CardholderMaintenance', () => {
  const store = mockCreateStore({
    cardholderMaintenance: {
      [storeId]: {
        expandState: '',
        addressTypes: [],
        addressType: {},
        isExpandAddress: false,
        isExpandInfo: true
      }
    }
  });
  it('Test changeExpand', () => {
    const expandState = 'isExpandAddress';
    store.dispatch(
      cardholderMaintenanceActions.changeExpand({ storeId, expandState })
    );
    const state = store.getState();
    expect(state.cardholderMaintenance[storeId][expandState]).toEqual(true);
  });
  it('Test changeAddressTypes', () => {
    const addressTypes: IAddressType[] = [];
    store.dispatch(
      cardholderMaintenanceActions.changeAddressTypes({ storeId, addressTypes })
    );
    const state = store.getState();
    expect(state.cardholderMaintenance[storeId].addressTypes).toEqual(
      addressTypes
    );
  });
  it('Test changeAddressType', () => {
    const addressType = {
      id: '1',
      addressType: '1',
      fromDate: new Date(),
      toDate: new Date()
    } as IAddressType;
    store.dispatch(
      cardholderMaintenanceActions.changeAddressType({ storeId, addressType })
    );
    const state = store.getState();
    expect(state.cardholderMaintenance[storeId].addressType).toEqual(
      addressType
    );
  });

  it('Test changeCardholder', () => {
    const cardholderId = {
      id: '1',
      cardholderName: 'Smith Jones',
      cardholderRole: 'Primary',
      status: 'Close',
      statusCode: 'C'
    } as ICardholderId;
    store.dispatch(
      cardholderMaintenanceActions.changeCardholder({ storeId, cardholderId })
    );
    const state = store.getState();
    expect(state.cardholderMaintenance[storeId].cardholderId).toEqual(
      cardholderId
    );
  });
});
