import { createStore, Store } from '@reduxjs/toolkit';
import { FormatTime } from 'app/constants/enums';
import { formatTimeDefault, mappingDataFromArray } from 'app/helpers';
import { storeId } from 'app/test-utils';
import sortBy from 'lodash.sortby';
import { mappingRefData } from 'pages/AccountDetails/helpers';
import { rootReducer } from 'storeConfig';
//service
import apiService from '../cardholdersMaintenanceService';
import { TYPE_ADDRESS_MATCH_WITH_API } from '../ExpandedAddress/constants';
import { IAddressInfo } from '../types';
import { cardholderMaintenanceActions, defaultStateCardholder } from './index';
import { getExpandedAddress } from './mapping.json';

const responseData = {
  data: {
    primary: [
      {
        accountIdentifier:
          '{"maskedValue":"022002******5078","eValue":"VOL(Y1R1MH5uLyRqOzo1UFI/ew==)"}',
        accountNumber:
          '{"maskedValue":"022002******5078","eValue":"VOL(Y1R1MH5uLyRqOzo1UFI/ew==)"}',
        addressAttentionLineText:
          '                                                  ',
        addressContinuationLine4Text:
          '                                                  ',
        addressContinuationLine1Text:
          '111                                               ',
        addressContinuationLine3Text:
          '                                                  ',
        addressContinuationLine2Text:
          '                                                  ',
        addressCategoryCode: 'P',
        addressFormatCode: 'F',
        addressRelationshipTypeCode: 'U',
        addressSubdivisionOneText: 'AR                       ',
        addressSubdivisionTwoText: '                         ',
        addressTypeCode: 'BLL1',
        addressValidationCode: 'Y',
        alternateAccountIdentifier: '                   ',
        cardholderAccountNumber:
          '{"maskedValue":"022002******5078","eValue":"VOL(Y1R1MH5uLyRqOzo1UFI/ew==)"}',
        changeTypeCode: ' ',
        cityName: 'PHOENIX                  ',
        companyName: '                                                  ',
        convertedEnteredIdentifier:
          '{"maskedValue":"022002******5078","eValue":"VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)"}',
        countryCode: 'USA',
        customerExternalIdentifier: '                        ',
        customerRoleTypeCode: '01',
        customerTypeCode: '01',
        deliveryPointCode: '  ',
        effectiveFromDate: '2021-05-19',
        effectiveToDate: '9999-12-31',
        enteredIdentifier:
          '{"maskedValue":"022002******5078","eValue":"VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)"}',
        housebuildingName: '                                                  ',
        houseIdentifier: '          ',
        mailCode: '000',
        mailUpdateCode: '0',
        memberSequenceIdentifier: '00001',
        postOfficeBoxIdentifier: '          ',
        postalCode: '71601     ',
        streetName: '                                       ',
        validAddressIndicator: 'Y',
        addressChangedTimestamp: '2021-05-19-08.54.12.440001'
      }
    ],
    secondary: [
      {
        accountIdentifier:
          '{"maskedValue":"022002******5078","eValue":"VOL(Y1R1MH5uLyRqOzo1UFI/ew==)"}',
        accountNumber:
          '{"maskedValue":"022002******5078","eValue":"VOL(Y1R1MH5uLyRqOzo1UFI/ew==)"}',
        addressAttentionLineText:
          '                                                  ',
        addressContinuationLine4Text:
          '                                                  ',
        addressContinuationLine1Text:
          '333MENLEY                                         ',
        addressContinuationLine3Text:
          '                                                  ',
        addressContinuationLine2Text:
          '                                                  ',
        addressCategoryCode: 'P',
        addressFormatCode: 'F',
        addressRelationshipTypeCode: 'U',
        addressSubdivisionOneText: 'NY                       ',
        addressSubdivisionTwoText: '                         ',
        addressTypeCode: 'BLL1',
        addressValidationCode: 'Y',
        alternateAccountIdentifier: '                   ',
        cardholderAccountNumber:
          '{"maskedValue":"022002******5078","eValue":"VOL(Y1R1MH5uLyRqOzo1UFI/ew==)"}',
        changeTypeCode: ' ',
        cityName: 'OMAHA                    ',
        companyName: '                                                  ',
        convertedEnteredIdentifier:
          '{"maskedValue":"022002******5078","eValue":"VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)"}',
        countryCode: 'USA',
        customerExternalIdentifier: '                        ',
        customerRoleTypeCode: '02',
        customerTypeCode: '02',
        deliveryPointCode: '  ',
        effectiveFromDate: '2021-05-20',
        effectiveToDate: '9999-12-31',
        enteredIdentifier:
          '{"maskedValue":"022002******5078","eValue":"VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)"}',
        housebuildingName: '                                                  ',
        houseIdentifier: '          ',
        mailCode: '000',
        mailUpdateCode: '0',
        memberSequenceIdentifier: '00002',
        postOfficeBoxIdentifier: '          ',
        postalCode: '10003     ',
        streetName: '                                       ',
        validAddressIndicator: 'Y',
        addressChangedTimestamp: '2021-05-17-22.12.15.450001'
      }
    ],
    additionalUsers: [
      {
        accountIdentifier:
          '{"maskedValue":"022002******5078","eValue":"VOL(Y1R1MH5uLyRqOzo1UFI/ew==)"}',
        accountNumber:
          '{"maskedValue":"022002******5078","eValue":"VOL(Y1R1MH5uLyRqOzo1UFI/ew==)"}',
        addressAttentionLineText:
          ' AAAA ATTENTION                                   ',
        addressContinuationLine4Text:
          '                                                  ',
        addressContinuationLine1Text:
          '12 STR                                            ',
        addressContinuationLine3Text:
          '                                                  ',
        addressContinuationLine2Text:
          '                                                  ',
        addressCategoryCode: 'P',
        addressFormatCode: 'F',
        addressRelationshipTypeCode: 'U',
        addressSubdivisionOneText: 'NY                       ',
        addressSubdivisionTwoText: '                         ',
        addressTypeCode: 'BLL1',
        addressValidationCode: 'Y',
        alternateAccountIdentifier: '                   ',
        cardholderAccountNumber:
          '{"maskedValue":"022002******5078","eValue":"VOL(Y1R1MH5uLyRqOzo1UFI/ew==)"}',
        changeTypeCode: ' ',
        cityName: 'CITY                     ',
        companyName: '                                                  ',
        convertedEnteredIdentifier:
          '{"maskedValue":"022002******5078","eValue":"VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)"}',
        countryCode: 'USA',
        customerExternalIdentifier: '                        ',
        customerRoleTypeCode: '03',
        customerTypeCode: '03',
        deliveryPointCode: '  ',
        effectiveFromDate: '2021-05-14',
        effectiveToDate: '9999-12-31',
        enteredIdentifier:
          '{"maskedValue":"022002******5078","eValue":"VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)"}',
        housebuildingName: '                                                  ',
        houseIdentifier: '          ',
        mailCode: '000',
        mailUpdateCode: '0',
        memberSequenceIdentifier: '00008',
        postOfficeBoxIdentifier: '          ',
        postalCode: '10003     ',
        streetName: '                                       ',
        validAddressIndicator: 'Y',
        addressChangedTimestamp: '2021-05-13-21.46.37.550001'
      }
    ]
  }
} as any;

const data: IAddressInfo = {
  POBox: '',
  addressCategory: {
    description: 'P - Permanent',
    fieldID: 'P',
    fieldValue: 'Permanent'
  },
  addressLineFour: '',
  addressLineOne: '111',
  addressLineThree: '',
  addressLineTwo: '',
  addressRelationship: {
    description: 'U - Unknown',
    fieldID: 'U',
    fieldValue: 'Unknown'
  },
  addressType: {
    description: 'BLL1 - Billing',
    fieldID: 'BLL1',
    fieldValue: 'Billing'
  },
  attention: '',
  cityName: 'PHOENIX',
  countryISO: {
    description: 'USA - United States of America',
    fieldID: 'USA',
    fieldValue: 'United States of America'
  },
  effectiveFrom: '2021/05/19',
  effectiveTo: '9999/12/31',
  formattedAddressCode: 'F',
  houseBuilding: '',
  houseNumber: '',
  id: '00001-P-BLL1-2021-05-19-9999-12-31',
  memberSequence: '00001',
  postalCode: '71601',
  stateSubdivision: {
    description: 'AR - Arkansas',
    fieldID: 'AR',
    fieldValue: 'Arkansas'
  },
  streetName: ''
};

const { ADDITIONAL_USERS, PRIMARY_MATCH_API, SECOND_MATCH_API } =
  TYPE_ADDRESS_MATCH_WITH_API;

const convertData = async (cardholderRole: string, cardholderId: string) => {
  const mappingAddress: IAddressInfo[] = mappingDataFromArray(
    responseData.data[cardholderRole],
    getExpandedAddress
  );

  const filterAddress = mappingAddress.filter(
    (item: IAddressInfo) => item.memberSequence === cardholderId
  );

  let addressTypes: IAddressInfo[] = [];

  for (const index in filterAddress) {
    addressTypes = [
      ...addressTypes,
      {
        ...(await mappingRefData(filterAddress[index], [
          'countryISO',
          'stateSubdivision',
          'addressCategory',
          'addressType',
          'addressRelationship'
        ]))
      }
    ];
  }
  const sortAddress: IAddressInfo[] = sortBy(addressTypes, [
    'addressCategory.fieldID',
    'addressType.fieldID'
  ]);
  return sortAddress;
};

const initialState: Partial<RootState> = {
  cardholderMaintenance: {
    [storeId]: {
      ...defaultStateCardholder,
      cardholderId: {
        cardNumberValue: 'VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)',
        cardholderName: 'ENDY2,HARRYY T',
        cardholderRole: '01',
        customerExternalIdentifier: 'C20273234913858500085018',
        customerRoleTypeCode: '01',
        customerType: '01',
        id: 'C20273234913858500085018',
        memberSequence: '00001',
        status: 'status',
        statusCode: 'statusCode'
      },
      isLoading: true
    }
  }
};

describe('Test Api GetAddressInfo ', () => {
  let store: Store<RootState>;
  beforeEach(() => {
    store = createStore(rootReducer, initialState);
  });

  afterEach(() => {
    store = {} as any;
  });

  const callApi = async (isError = false, res = responseData) => {
    if (!isError) {
      jest.spyOn(apiService, 'getAddressInfo').mockResolvedValue(res);
    } else {
      jest
        .spyOn(apiService, 'getAddressInfo')
        .mockRejectedValue(new Error('Async'));
    }
    const data = await cardholderMaintenanceActions.getAddressInfo({
      storeId
    })(store.dispatch, store.getState, {});
    return data;
  };

  it('Should Have Call API with Cardholder Primary', async () => {
    store = createStore(rootReducer, initialState);
    await callApi();
    const dataEqual = await convertData(PRIMARY_MATCH_API, '00001');
    const dataExpect =
      store.getState().cardholderMaintenance[storeId].addressTypes;
    expect(dataExpect).toEqual(dataEqual);
  });

  it('Should Have Call API with Cardholder SECONDARY', async () => {
    store = createStore(rootReducer, {
      ...initialState,
      cardholderMaintenance: {
        [storeId]: {
          ...initialState.cardholderMaintenance![storeId],
          cardholderId: {
            ...initialState.cardholderMaintenance![storeId].cardholderId,
            cardholderRole: '02',
            customerRoleTypeCode: '02',
            customerType: '02',
            memberSequence: '00002'
          }
        }
      }
    });
    await callApi();
    const dataEqual = await convertData(SECOND_MATCH_API, '00002');
    const dataExpect =
      store.getState().cardholderMaintenance[storeId].addressTypes;
    expect(dataExpect).toEqual(dataEqual);
  });

  it('Should Have Call API with Cardholder AUTHORIZED', async () => {
    store = createStore(rootReducer, {
      ...initialState,
      cardholderMaintenance: {
        [storeId]: {
          ...initialState.cardholderMaintenance![storeId],
          cardholderId: {
            ...initialState.cardholderMaintenance![storeId].cardholderId,
            cardholderRole: '03',
            customerRoleTypeCode: '03',
            customerType: '03',
            memberSequence: '00003'
          }
        }
      }
    });
    const dataExpect =
      store.getState().cardholderMaintenance[storeId].addressTypes;
    const dataEqual = await convertData(ADDITIONAL_USERS, '00003');
    expect(dataExpect).toEqual(dataEqual);
  });

  it('Should Haven"t Call API with Cardholder AUTHORIZED', async () => {
    store = createStore(rootReducer, {
      ...initialState,
      cardholderMaintenance: {
        [storeId]: {
          ...initialState.cardholderMaintenance![storeId],
          cardholderId: {
            ...initialState.cardholderMaintenance![storeId].cardholderId,
            cardholderRole: '03',
            customerRoleTypeCode: '03',
            customerType: '03',
            memberSequence: '00003'
          }
        }
      }
    });
    const responseDataCustom = {
      data: { primary: responseData.data.primary[0] }
    };
    await callApi(false, responseDataCustom);
    const dataExpect =
      store.getState().cardholderMaintenance[storeId].addressTypes;
    expect(dataExpect).toEqual([]);
  });

  it('Should Have Call API with Failed', async () => {
    store = createStore(rootReducer, initialState);
    await callApi(true);
    const isLoading = store.getState().cardholderMaintenance[storeId].isLoading;
    expect(isLoading).toEqual(false);
  });

  it('Should Have Call API with Pending', async () => {
    const pendingAction = cardholderMaintenanceActions.getAddressInfo.pending(
      'cardholderMaintenance/getAddressInfo',
      {
        storeId,
        isSetRootLoading: true
      }
    );
    const actual = rootReducer(store.getState(), pendingAction);

    expect(actual.cardholderMaintenance[storeId].isLoading).toEqual(true);
  });

  it('Should Have Call API with FullField with idAddress', async () => {
    const effectiveFromIdAddress = formatTimeDefault(
      data.effectiveFrom,
      FormatTime.YearMonthDayWithHyphen
    );
    const effectiveToIdAddress = formatTimeDefault(
      data.effectiveTo,
      FormatTime.YearMonthDayWithHyphen
    );
    const idAddress = `${data.memberSequence}-${data.addressCategory.fieldID}-${data.addressType.fieldID}-${effectiveFromIdAddress}-${effectiveToIdAddress}`;
    const addressTypes = [data];
    const pendingAction = cardholderMaintenanceActions.getAddressInfo.fulfilled(
      { addressTypes },
      'cardholderMaintenance/getAddressInfo',
      {
        storeId,
        isSetRootLoading: true,
        idAddress
      }
    );

    const actual = rootReducer(store.getState(), pendingAction);

    expect(actual.cardholderMaintenance[storeId].addressTypes).toEqual(
      addressTypes
    );
    expect(actual.cardholderMaintenance[storeId].addressType).toEqual(data);
    expect(actual.cardholderMaintenance[storeId].isLoading).toEqual(false);
  });

  it('Should Have Call API with FullField with idAddress but haven"t addressTypes', async () => {
    const effectiveFromIdAddress = formatTimeDefault(
      data.effectiveFrom,
      FormatTime.YearMonthDayWithHyphen
    );
    const effectiveToIdAddress = formatTimeDefault(
      data.effectiveTo,
      FormatTime.YearMonthDayWithHyphen
    );
    const idAddress = `${data.memberSequence}-${data.addressCategory.fieldID}-${data.addressType.fieldID}-${effectiveFromIdAddress}-${effectiveToIdAddress}`;
    const pendingAction = cardholderMaintenanceActions.getAddressInfo.fulfilled(
      { addressTypes: [] },
      'cardholderMaintenance/getAddressInfo',
      {
        storeId,
        isSetRootLoading: true,
        idAddress
      }
    );

    const actual = rootReducer(store.getState(), pendingAction);

    expect(actual.cardholderMaintenance[storeId].addressTypes).toEqual([]);
    expect(actual.cardholderMaintenance[storeId].addressType).toEqual({});
    expect(actual.cardholderMaintenance[storeId].isLoading).toEqual(false);
  });

  it('Should Have Call API with FullField with idAddress and addressTypes exists', async () => {
    const effectiveFromIdAddress = formatTimeDefault(
      data.effectiveFrom,
      FormatTime.YearMonthDayWithHyphen
    );
    const effectiveToIdAddress = formatTimeDefault(
      data.effectiveTo,
      FormatTime.YearMonthDayWithHyphen
    );
    const idAddress = `${data.memberSequence}-${data.addressCategory.fieldID}-${data.addressType.fieldID}-${effectiveFromIdAddress}-${effectiveToIdAddress}`;
    const addressTypeMock = { ...data, id: idAddress };
    const addressTypesMock = [addressTypeMock];
    const pendingAction = cardholderMaintenanceActions.getAddressInfo.fulfilled(
      { addressTypes: addressTypesMock },
      'cardholderMaintenance/getAddressInfo',
      {
        storeId,
        isSetRootLoading: true,
        idAddress
      }
    );

    const actual = rootReducer(store.getState(), pendingAction);

    expect(actual.cardholderMaintenance[storeId].addressTypes).toEqual(
      addressTypesMock
    );
    expect(actual.cardholderMaintenance[storeId].addressType).toEqual(
      addressTypeMock
    );
    expect(actual.cardholderMaintenance[storeId].isLoading).toEqual(false);
  });
});
