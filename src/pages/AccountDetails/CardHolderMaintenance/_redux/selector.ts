import { createSelector } from '@reduxjs/toolkit';
import { formatCommon } from 'app/helpers';
import isEmpty from 'lodash.isempty';
import { VIEW_NAME_ADDRESS } from '../ExpandedAddress/constants';
import {
  ICardholderId,
  ICardholder,
  IAddressInfo,
  ModalExpandedAddress
} from '../types';
import { ADDRESS_EXPANSION_CODE } from '../constants';

export const getRootLoading: CustomStoreIdSelector<boolean> = (
  states,
  storeId
) => {
  return states.cardholderMaintenance[storeId]?.isLoading;
};

export const getExpansionAddress: CustomStoreIdSelector<string> = (
  states,
  storeId
) => {
  return states.cardholderMaintenance[storeId]?.expansionAddressCode;
};

export const getCardholderAddressTypes: CustomStoreIdSelector<IAddressInfo[]> =
  (states, storeId) => {
    return states.cardholderMaintenance[storeId]?.addressTypes;
  };

export const getCardholderAddressType: CustomStoreIdSelector<IAddressInfo> = (
  states,
  storeId
) => {
  return states.cardholderMaintenance[storeId]?.addressType;
};

export const getCardholderId: CustomStoreIdSelector<ICardholderId> = (
  states,
  storeId
) => {
  return states.cardholderMaintenance[storeId]?.cardholderId;
};

export const getCardholdersId: CustomStoreIdSelector<ICardholderId[]> = (
  states,
  storeId
) => {
  return states.cardholderMaintenance[storeId]?.cardholdersId;
};

export const getCardholders: CustomStoreIdSelector<ICardholder[]> = (
  states,
  storeId
) => {
  return states.cardholderMaintenance[storeId]?.cardholders;
};

export const getCardholder: CustomStoreIdSelector<ICardholder | undefined> = (
  states,
  storeId
) => {
  const cardholderId = getCardholderId(states, storeId);
  const cardholders = getCardholders(states, storeId);
  if (!cardholders || !cardholders.length || !cardholderId || !cardholderId.id)
    return;
  return cardholders.find(item => item.cardholderId === cardholderId.id);
};

export const selectIsExpandedAddress = createSelector(
  getExpansionAddress,
  (expansionAddressCode: string) =>
    expansionAddressCode === ADDRESS_EXPANSION_CODE.EXPANDED
);

export const selectCardholdersId = createSelector(
  getCardholdersId,
  (data: ICardholderId[]) => data
);

export const selectCardholderId = createSelector(
  getCardholderId,
  (data: ICardholderId) => data
);

export const selectCardholderInfo = createSelector(
  getCardholder,
  (data?: ICardholder) => {
    return data?.cardholderInfo;
  }
);

export const selectCardholderAddressTypes = createSelector(
  getCardholderAddressTypes,
  (data: IAddressInfo[]) => data
);

export const selectCardholderAddressType = createSelector(
  getCardholderAddressType,
  (data: IAddressInfo) => data
);

export const selectExpandedAddressHistory = createSelector(
  (states: RootState, storeId: string) =>
    states.cardholderMaintenance[storeId]?.expandedAddress.histories,
  (data: any[]) => data
);

export const selectCardholderStandardAddressHistory = createSelector(
  getCardholder,
  cardholder => cardholder?.standardAddressInfoHistory || []
);

export const selectIsExpandInfo = createSelector(
  (states: RootState, storeId: string) =>
    states.cardholderMaintenance[storeId]?.isExpandInfo,
  (data: boolean) => data
);

export const selectRootLoading = createSelector(
  getRootLoading,
  (isRootLoading: boolean) => {
    return isRootLoading;
  }
);

export const selectIsExpandTelePhone = createSelector(
  (states: RootState, storeId: string) =>
    states.cardholderMaintenance[storeId]?.isExpandTelePhone,
  (expand: boolean) => expand
);

export const selectIsExpandEmail = createSelector(
  (states: RootState, storeId: string) =>
    states.cardholderMaintenance[storeId]?.isExpandEmail,
  (expand: boolean) => expand
);

export const selectTelePhoneInfo = createSelector(
  getCardholder,
  data => data?.telephone
);

export const selectTelePhoneFormInfo = createSelector(getCardholder, data => {
  const telephoneInfo = data?.telephone;
  return {
    ...telephoneInfo,
    homePhone: {
      ...telephoneInfo?.homePhone,
      more: {
        ...telephoneInfo?.homePhone?.more,
        deviceType: isEmpty(telephoneInfo?.homePhone?.more?.deviceType?.value)
          ? null
          : telephoneInfo?.homePhone?.more?.deviceType,
        flag: isEmpty(telephoneInfo?.homePhone?.more?.flag?.value)
          ? null
          : telephoneInfo?.homePhone?.more?.flag,
        lastUpdated: isEmpty(telephoneInfo?.homePhone?.less)
          ? null
          : formatCommon(telephoneInfo?.homePhone?.more?.lastUpdated || '').time
              .date
      }
    },
    workPhone: {
      ...telephoneInfo?.workPhone,
      more: {
        ...telephoneInfo?.workPhone?.more,
        deviceType: isEmpty(telephoneInfo?.workPhone?.more?.deviceType?.value)
          ? null
          : telephoneInfo?.workPhone?.more?.deviceType,
        flag: isEmpty(telephoneInfo?.workPhone?.more?.flag?.value)
          ? null
          : telephoneInfo?.workPhone?.more?.flag,
        lastUpdated: isEmpty(telephoneInfo?.workPhone?.less)
          ? null
          : formatCommon(telephoneInfo?.workPhone?.more?.lastUpdated || '').time
              .date
      }
    },
    mobilePhone: {
      ...telephoneInfo?.mobilePhone,
      more: {
        ...telephoneInfo?.mobilePhone?.more,
        deviceType: isEmpty(telephoneInfo?.mobilePhone?.more?.deviceType?.value)
          ? null
          : telephoneInfo?.mobilePhone?.more?.deviceType,
        flag: isEmpty(telephoneInfo?.mobilePhone?.more?.flag?.value)
          ? null
          : telephoneInfo?.mobilePhone?.more?.flag,
        lastUpdated: isEmpty(telephoneInfo?.mobilePhone?.less)
          ? null
          : formatCommon(telephoneInfo?.mobilePhone?.more?.lastUpdated || '')
              .time.date
      }
    },
    fax: {
      ...telephoneInfo?.fax,
      more: {
        ...telephoneInfo?.fax?.more,
        deviceType: isEmpty(telephoneInfo?.fax?.more?.deviceType?.value)
          ? null
          : telephoneInfo?.fax?.more?.deviceType,
        flag: isEmpty(telephoneInfo?.fax?.more?.flag?.value)
          ? null
          : telephoneInfo?.fax?.more?.flag,
        lastUpdated: isEmpty(telephoneInfo?.fax?.less)
          ? null
          : formatCommon(telephoneInfo?.fax?.more?.lastUpdated || '').time.date
      }
    },
    miscellaneous: {
      ...telephoneInfo?.miscellaneous,
      more: {
        ...telephoneInfo?.miscellaneous?.more,
        deviceType: isEmpty(
          telephoneInfo?.miscellaneous?.more?.deviceType?.value
        )
          ? null
          : telephoneInfo?.miscellaneous?.more?.deviceType,
        flag: isEmpty(telephoneInfo?.miscellaneous?.more?.flag?.value)
          ? null
          : telephoneInfo?.miscellaneous?.more?.flag,
        lastUpdated: isEmpty(telephoneInfo?.miscellaneous?.less)
          ? null
          : formatCommon(telephoneInfo?.miscellaneous?.more?.lastUpdated || '')
              .time.date
      }
    }
  };
});

export const selectEmailInfo = createSelector(
  getCardholder,
  data => data?.email
);

export const selectModalEdit = createSelector(
  (states: RootState, storeId: string) =>
    states.cardholderMaintenance[storeId]?.isOpenModalPhoneEmail,
  (open: boolean) => open
);

export const selectTypeEdit = createSelector(
  (states: RootState, storeId: string) =>
    states.cardholderMaintenance[storeId]?.typeEdit,
  (type: string) => type
);

export const selectReload = createSelector(
  (states: RootState, storeId: string) =>
    states.cardholderMaintenance[storeId]?.reload,
  (reload: boolean) => reload
);

export const selectLoadingModal = createSelector(
  (states: RootState, storeId: string) =>
    states.cardholderMaintenance[storeId]?.loadingEdit,
  (loading: boolean) => loading
);

export const selectIsOpenModalStandardAddAddress = createSelector(
  (states: RootState, storeId: string) =>
    states.cardholderMaintenance[storeId]?.standardAddress?.isOpenModalAdd ||
    false,
  (open: boolean) => open
);

export const selectIsOpenModalStandardEditAddress = createSelector(
  (states: RootState, storeId: string) =>
    states.cardholderMaintenance[storeId]?.standardAddress?.isOpenModalEdit ||
    false,
  (open: boolean) => open
);

export const selectModalExpandedAddress = createSelector(
  (states: RootState, storeId: string) =>
    states.cardholderMaintenance[storeId]?.expandedAddress?.openModal,
  (openModal: ModalExpandedAddress) => openModal
);

export const selectAddressCategoryInForm = createSelector(
  (states: RootState, storeId: string) =>
    states.form[`${storeId}-${VIEW_NAME_ADDRESS.ADD_ADDRESS}`]?.values
      ?.addressCategory?.fieldID,
  (fieldID: string) => fieldID
);

export const selectIsExpandAddress = createSelector(
  (states: RootState, storeId: string) =>
    states.cardholderMaintenance[storeId]?.isExpandAddress,
  (data: boolean) => data
);

export const selectAddressTypeInForm = createSelector(
  (states: RootState, storeId: string) =>
    states.form[`${storeId}-${VIEW_NAME_ADDRESS.ADD_ADDRESS}`]?.values
      ?.addressType?.fieldID,
  (type: string) => type
);

export const selectEffectiveFromInForm = createSelector(
  (states: RootState, storeId: string) =>
    states.form[`${storeId}-${VIEW_NAME_ADDRESS.ADD_ADDRESS}`]?.values
      ?.effectiveFrom,
  (effectiveFrom: Date | string) => effectiveFrom
);

export const selectEffectiveToInForm = createSelector(
  (states: RootState, storeId: string) =>
    states.form[`${storeId}-${VIEW_NAME_ADDRESS.ADD_ADDRESS}`]?.values
      ?.effectiveTo,
  (effectiveTo: Date | string) => effectiveTo
);

export const selectCountryISOInForm = createSelector(
  (states: RootState, storeId: string) =>
    states.form[`${storeId}-${VIEW_NAME_ADDRESS.ADD_ADDRESS}`]?.values
      ?.countryISO?.fieldID,
  (fieldID: string) => fieldID
);

export const isLoadingExpandedAddress = createSelector(
  (states: RootState, storeId: string) =>
    states.cardholderMaintenance[storeId]?.expandedAddress?.isLoading,
  (isLoading: boolean) => isLoading
);

export const isEndLoadMore = createSelector(
  (states: RootState, storeId: string) =>
    states.cardholderMaintenance[storeId]?.expandedAddress?.isEndLoadMore,
  (data: boolean) => data
);

export const selectLoadingAddStandardAddress = createSelector(
  (states: RootState, storeId: string) =>
    states.cardholderMaintenance[storeId]?.standardAddress
      ?.isLoadingAddAddress || false,
  (isLoadingAddStandardAddress: boolean) => isLoadingAddStandardAddress
);

export const selectLoadingEditStandardAddress = createSelector(
  (states: RootState, storeId: string) =>
    states.cardholderMaintenance[storeId]?.standardAddress
      ?.isLoadingEditAddress || false,
  (isLoadingEditAddress: boolean) => isLoadingEditAddress
);

export const selectCardholderAddressInfo = createSelector(
  getCardholderAddressType,
  (addressType?: IAddressInfo) => addressType
);
