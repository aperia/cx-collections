import { defaultStateCardholder } from '.';
import { mockActionCreator, storeId } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { postMemoForPhone } from './postMemoForPhone';
import { ICardholder } from '../types';
import { memoActions } from 'pages/Memos/_redux/reducers';
import cardholdersMaintenanceService from '../cardholdersMaintenanceService';

const phoneDeviceTypeList = [
  { value: 'I', description: 'I - Invalid Number' },
  { value: 'L', description: 'L - Land Line' },
  { value: 'U', description: 'U - Unknown Type' },
  { value: 'W', description: 'W - Cell Phone' }
];

const phoneFlagList = [
  { value: 'C', description: 'C - Do Not Contact Cease And Desist' },
  { value: 'N', description: 'N - Number Not Valid' },
  { value: 'D', description: 'D - Disconnected' },
  { value: 'E', description: 'E - No Longer At This Number' },
  { value: 'G', description: 'G - Consent For Voice Contact' },
  { value: 'H', description: 'H - Do Not Contact' },
  { value: 'B', description: 'B - Consent For SMS Text Messages And Voice' },
  { value: 'S', description: 'S - Consent for SMS Text Messages Only' },
  { value: 'U', description: 'U - Number Is Unlisted, Do Not Contact' },
  { value: 'V', description: 'V - Number Is Not Verified' },
  { value: 'X', description: 'X - Do Not Contact The Customer' },
  { value: 'Y', description: 'Y - Number Is Valid; Manual Dial Only' }
];

const generatePostData = (data?: MagicKeyValue) => {
  return {
    accEValue: 'VOL(UzhueTEwXGV8bW0uOyQkO2Q2Xg==)',
    postData: {
      common: {
        accountId: 'VOL(UzhueTEwXGV8bW0uOyQkO2Q2Xg==)',
        externalCustomerId: 'C21075084643994570045717'
      },
      data: { ...data },
      storeId: 'VOL(UzhueTEwXGV8bW0uOyQkO2Q2Xg==)-00001',
      type: 'telephone'
    }
  };
};

const spyCardholdersMaintenance = mockActionCreator(
  cardholdersMaintenanceService
);

const spyActionPostMemo = mockActionCreator(memoActions);

const cardholders = {
  cardholderId: 'C21075084643994570045717',
  cardholderInfo: {
    authCBRFlag: 'Do not report',
    cardNumber: '435583******2509',
    cardholderRole: '01',
    dateOfBirth: '********1980',
    embossingName: 'VINTAGE PHOTOGRAPHY',
    firstName: '',
    lastName: '',
    memberSequence: '00001',
    middleName: '',
    prefix: '',
    qualification: '',
    salutation: 'Unknown Gender',
    solicitation: 'Solicitation Allowed',
    ssnTaxId: '1968',
    suffix: '',
    title: ''
  },
  email: {
    electronicMailHomeAddressText: 'ABC@GMAIL.COM',
    electronicMailHomeSolicitIndicator: 'Y',
    electronicMailHomeStatusIndicator: 'N',
    electronicMailWorkAddressText: 'TRUST@GMAIL.COM',
    electronicMailWorkSolicitIndicator: 'N',
    electronicMailWorkStatusIndicator: 'Y',
    homeEmailStatusDesc: 'N - Invalid',
    homeSolicitFlagDesc: 'Y - Solicit allowed',
    workEmailStatusDesc: 'Y - Valid',
    workSolicitFlagDesc: 'N - Do not solicit'
  },
  telephone: {
    fax: {
      less: '8477771111',
      more: {
        deviceType: { value: 'U', description: 'U - Unknown Type' },
        flag: {
          value: 'Y',
          description: 'Y - Number Is Valid; Manual Dial Only'
        },
        lastUpdated: '03-16-2021'
      }
    },
    homePhone: {
      less: '8989898981',
      more: {
        deviceType: { value: 'U', description: 'U - Unknown Type' },
        flag: {
          value: 'Y',
          description: 'Y - Number Is Valid; Manual Dial Only'
        },
        lastUpdated: '07-02-2021'
      }
    },
    miscellaneous: {
      less: '2343243241',
      more: {
        deviceType: { value: 'U', description: 'U - Unknown Type' },
        flag: {
          value: 'Y',
          description: 'Y - Number Is Valid; Manual Dial Only'
        },
        lastUpdated: '03-16-2021'
      }
    },
    mobilePhone: {
      less: '9785456142',
      more: {
        deviceType: { value: 'U', description: 'U - Unknown Type' },
        flag: {
          value: 'Y',
          description: 'Y - Number Is Valid; Manual Dial Only'
        },
        lastUpdated: '03-16-2021'
      }
    },
    workPhone: {
      less: '8328881111',
      more: {
        deviceType: { value: 'U', description: 'U - Unknown Type' },
        flag: {
          value: 'Y',
          description: 'Y - Number Is Valid; Manual Dial Only'
        },
        lastUpdated: '03-16-2021'
      }
    }
  }
} as ICardholder;

const initialState: Partial<RootState> = {
  accountDetail: {
    [storeId]: {
      selectedAuthenticationData: {
        customerName: 'customerName',
        customerNameForCreateWorkflow: 'customerNameForCreateWorkflow'
      }
    }
  },
  cardholderMaintenance: {
    [storeId]: {
      ...defaultStateCardholder,
      cardholders: [cardholders],
      cardholderId: {
        cardNumberValue: 'VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)',
        cardholderName: 'ENDY2,HARRYY T',
        cardholderRole: '01',
        customerExternalIdentifier: 'C20273234913858500085018',
        customerRoleTypeCode: '01',
        customerType: '01',
        id: 'C20273234913858500085018',
        memberSequence: '00001',
        status: 'status',
        statusCode: 'statusCode'
      },
      isLoading: true
    }
  }
};

describe('Test postMemoForPhone', () => {
  beforeEach(() => {
    spyCardholdersMaintenance('getPhoneDeviceTypeRefData', () => {
      return Promise.resolve(phoneDeviceTypeList);
    });
    spyCardholdersMaintenance('getPhoneFlagRefData', () => {
      return Promise.resolve(phoneFlagList);
    });
  });

  it('Should have isPhoneChange, isDeviceCodeChange, isStatusCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        homePhoneDeviceCode: 'U',
        homePhoneLastUpdatedDate: '07/03/2021',
        homePhoneStatusCode: 'Y',
        primaryCustomerHomePhoneIdentifier: '8989898982'
      }),
      storeId
    )(store.dispatch, store.getState, {});
    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have isPhoneChange, isDeviceCodeChange, isStatusCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        homePhoneDeviceCode: '',
        homePhoneLastUpdatedDate: '07/03/2021',
        homePhoneStatusCode: '',
        primaryCustomerHomePhoneIdentifier: ''
      }),
      storeId
    )(store.dispatch, store.getState, {});
    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have !isPhoneChange ', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        homePhoneDeviceCode: 'U',
        homePhoneLastUpdatedDate: '07/03/2021',
        homePhoneStatusCode: 'Y'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have !isDeviceCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        homePhoneLastUpdatedDate: '07/03/2021',
        homePhoneStatusCode: 'Y',
        primaryCustomerHomePhoneIdentifier: '8989898982'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have !isStatusCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');

    await postMemoForPhone(
      generatePostData({
        homePhoneDeviceCode: 'U',
        homePhoneLastUpdatedDate: '07/03/2021',
        primaryCustomerHomePhoneIdentifier: '8989898982'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have isFaxChange, isFaxDeviceCodeChange, isFaxStatusCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        facsimilePhoneIdentifier: 'facsimilePhoneIdentifier',
        facsimilePhoneDeviceCode: 'facsimilePhoneDeviceCode',
        facsimilePhoneFlagCode: 'facsimilePhoneFlagCode'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have isFaxChange, isFaxDeviceCodeChange, isFaxStatusCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        facsimilePhoneIdentifier: '',
        facsimilePhoneDeviceCode: '',
        facsimilePhoneFlagCode: ''
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have isFaxChange, isFaxDeviceCodeChange, isFaxStatusCodeChange > facsimilePhoneIdentifier is null ', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        facsimilePhoneIdentifier: null,
        facsimilePhoneDeviceCode: 'facsimilePhoneDeviceCode',
        facsimilePhoneFlagCode: 'facsimilePhoneFlagCode'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have !isFaxStatusCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        facsimilePhoneIdentifier: 'facsimilePhoneIdentifier',
        facsimilePhoneDeviceCode: 'facsimilePhoneDeviceCode'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have !isFaxDeviceCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        facsimilePhoneIdentifier: 'facsimilePhoneIdentifier',
        facsimilePhoneFlagCode: 'facsimilePhoneFlagCode'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have !isFaxChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        facsimilePhoneDeviceCode: 'facsimilePhoneDeviceCode',
        facsimilePhoneFlagCode: 'facsimilePhoneFlagCode'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have isWorkPhoneChange, isWorkDeviceCodeChange, isWorkStatusCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        primaryCustomerSecondPhoneIdentifier:
          'primaryCustomerSecondPhoneIdentifier',
        businessPhoneDeviceCode: 'businessPhoneDeviceCode',
        businessTelephoneFlagCode: 'businessTelephoneFlagCode'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have isWorkPhoneChange, isWorkDeviceCodeChange, isWorkStatusCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        primaryCustomerSecondPhoneIdentifier:
          '',
        businessPhoneDeviceCode: '',
        businessTelephoneFlagCode: ''
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have !isWorkStatusCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        primaryCustomerSecondPhoneIdentifier:
          'primaryCustomerSecondPhoneIdentifier',
        businessPhoneDeviceCode: 'businessPhoneDeviceCode'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have !isWorkDeviceCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        primaryCustomerSecondPhoneIdentifier:
          'primaryCustomerSecondPhoneIdentifier',
        businessTelephoneFlagCode: 'businessTelephoneFlagCode'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have !isWorkPhoneChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        businessPhoneDeviceCode: 'businessPhoneDeviceCode',
        businessTelephoneFlagCode: 'businessTelephoneFlagCode'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have isMiscellaneousChange, isMiscellaneousDeviceCodeChange, isMiscellaneousStatusCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        clientControlledPhoneIdentifier: 'clientControlledPhoneIdentifier',
        clientControlledPhoneDeviceCode: 'clientControlledPhoneDeviceCode',
        clientControlledPhoneFlagIndicator: 'clientControlledPhoneFlagIndicator'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have isMiscellaneousChange, isMiscellaneousDeviceCodeChange, isMiscellaneousStatusCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        clientControlledPhoneIdentifier: '',
        clientControlledPhoneDeviceCode: '',
        clientControlledPhoneFlagIndicator: ''
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have !isMiscellaneousChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        clientControlledPhoneDeviceCode: 'clientControlledPhoneDeviceCode',
        clientControlledPhoneFlagIndicator: 'clientControlledPhoneFlagIndicator'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have !isMiscellaneousDeviceCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        clientControlledPhoneIdentifier: 'clientControlledPhoneIdentifier',
        clientControlledPhoneFlagIndicator: 'clientControlledPhoneFlagIndicator'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have !isMiscellaneousStatusCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        clientControlledPhoneIdentifier: 'clientControlledPhoneIdentifier',
        clientControlledPhoneDeviceCode: 'clientControlledPhoneDeviceCode'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have isMobilePhoneChange, isMobileDeviceCodeChange, isMobileStatusCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        mobilePhoneIdentifier: 'mobilePhoneIdentifier',
        mobilePhoneDeviceCode: 'mobilePhoneDeviceCode',
        mobilePhoneFlagCode: 'mobilePhoneFlagCode'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have isMobilePhoneChange, isMobileDeviceCodeChange, isMobileStatusCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        mobilePhoneIdentifier: '',
        mobilePhoneDeviceCode: '',
        mobilePhoneFlagCode: ''
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have !isMobilePhoneChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        mobilePhoneDeviceCode: 'mobilePhoneDeviceCode',
        mobilePhoneFlagCode: 'mobilePhoneFlagCode'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have !isMobileDeviceCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        mobilePhoneIdentifier: 'mobilePhoneIdentifier',
        mobilePhoneFlagCode: 'mobilePhoneFlagCode'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });

  it('Should have !isMobileStatusCodeChange', async () => {
    const store = createStore(rootReducer, initialState);
    const postMemo = spyActionPostMemo('postMemo');
    await postMemoForPhone(
      generatePostData({
        mobilePhoneIdentifier: 'mobilePhoneIdentifier',
        mobilePhoneDeviceCode: 'mobilePhoneDeviceCode'
      }),
      storeId
    )(store.dispatch, store.getState, {});

    expect(postMemo).toHaveBeenCalled();
  });
});
