import { mockActionCreator, responseDefault, storeId } from 'app/test-utils';
import { rootReducer } from 'storeConfig';

import { cardholderMaintenanceActions, defaultStateCardholder } from './index';
import { createStore, Store } from '@reduxjs/toolkit';

//service
import apiService from '../cardholdersMaintenanceService';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { VIEW_NAME_ADDRESS } from '../ExpandedAddress/constants';
import { formatTimeDefault } from 'app/helpers/formatTime';
import { FormatTime } from 'app/constants/enums';
import { memoActions } from 'pages/Memos/_redux/reducers';

const toastSpy = mockActionCreator(actionsToast);

const spyMemoActions = mockActionCreator(memoActions);

const cardholderMaintenanceSpy = mockActionCreator(
  cardholderMaintenanceActions
);

const addressInfo = {
  POBox: '',
  addressCategory: {
    fieldValue: 'Permanent',
    fieldID: 'P',
    description: 'P - Permanent'
  },
  addressLineFour: '',
  addressLineOne: '333 MENLAY',
  addressLineThree: '',
  addressLineTwo: '',
  addressRelationship: {
    fieldValue: 'Unknown',
    fieldID: 'U',
    description: 'U - Unknown'
  },
  addressType: {
    fieldValue: 'Billing',
    fieldID: 'BLL1',
    description: 'BLL1 - Billing'
  },
  attention: '',
  cityName: 'OMAHA',
  countryISO: {
    fieldValue: 'United States of America',
    fieldID: 'USA',
    description: 'USA - United States of America'
  },
  effectiveFrom: '2021-05-18',
  effectiveTo: '9999-12-31',
  formattedAddressCode: 'F',
  houseBuilding: '',
  houseNumber: '',
  id: '00001-P-BLL1-2021-06-18-9999-12-31',
  memberSequence: '00001',
  postalCode: '10003',
  stateSubdivision: {
    fieldValue: 'New York',
    fieldID: 'NY',
    description: 'NY - New York'
  },
  streetName: ''
};

const responseData = {
  ...responseDefault,
  data: 'Success'
};

const initialState: Partial<RootState> = {
  form: {
    [`${storeId}-${VIEW_NAME_ADDRESS.ADD_ADDRESS}`]: {
      values: {
        POBox: '',
        addressCategory: {
          fieldValue: 'Permanent',
          fieldID: 'P',
          description: 'P - Permanent'
        },
        addressLineFour: '',
        addressLineOne: '213213213',
        addressLineThree: '',
        addressLineTwo: '',
        addressRelationship: {
          fieldValue: 'Unknown',
          fieldID: 'U',
          description: 'U - Unknown'
        },
        addressType: {
          fieldValue: 'Billing',
          fieldID: 'BLL1',
          description: 'BLL1 - Billing'
        },
        attention: '',
        cityName: '12321321',
        countryISO: {
          fieldValue: 'United States of America',
          fieldID: 'USA',
          description: 'USA - United States of America'
        },
        effectiveFrom: '2021-05-19',
        effectiveTo: '9999-12-31',
        formattedAddressCode: 'F',
        houseBuilding: '',
        houseNumber: '',
        id: '00001-P-BLL1-2021-05-19-9999-12-31',
        memberSequence: '00001',
        postalCode: '10003',
        stateSubdivision: {
          fieldValue: 'New York',
          fieldID: 'NY',
          countryCode: 'USA',
          description: 'NY - New York'
        },
        streetName: ''
      }
    } as any
  },
  cardholderMaintenance: {
    [storeId]: {
      ...defaultStateCardholder,
      expandedAddress: {
        histories: [],
        openModal: 'ADD',
        isLoading: false,
        isEndLoadMore: false
      },
      addressType: addressInfo,
      cardholderId: {
        cardNumberValue: 'VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)',
        cardholderName: 'ENDY2,HARRYY T',
        cardholderRole: '01',
        customerExternalIdentifier: 'C20273234913858500085018',
        customerRoleTypeCode: '01',
        customerType: '01',
        id: 'C20273234913858500085018',
        memberSequence: '1',
        status: '',
        statusCode: ''
      },
      isLoading: true
    }
  }
};

const data =
  initialState.form![`${storeId}-${VIEW_NAME_ADDRESS.ADD_ADDRESS}`].values;

const effectiveFromIdAddress = formatTimeDefault(
  data?.effectiveFrom,
  FormatTime.YearMonthDayWithHyphen
);
const effectiveToIdAddress = formatTimeDefault(
  data?.effectiveTo,
  FormatTime.YearMonthDayWithHyphen
);

const memberSequence =
  initialState?.cardholderMaintenance![storeId].cardholderId.memberSequence;

const idAddress = `${memberSequence}-${data!.addressCategory.fieldID}-${
  data!.addressType.fieldID
}-${effectiveFromIdAddress}-${effectiveToIdAddress}`;

describe('Test Api AddAndUpdateExpandedAddress', () => {
  describe.only('Test Api Add', () => {
    let store: Store<RootState>;
    beforeEach(() => {
      store = createStore(rootReducer, initialState);
    });

    const callApi = async (isError = false) => {
      if (!isError) {
        jest
          .spyOn(apiService, 'addAndUpdateExpandedAddress')
          .mockResolvedValue(responseData);
      } else {
        jest
          .spyOn(apiService, 'addAndUpdateExpandedAddress')
          .mockRejectedValue(new Error('Async'));
      }
      const response =
        await cardholderMaintenanceActions.addUpdateExpandedAddress({
          storeId,
          postData: { accountId: storeId }
        })(store.dispatch, store.getState, {});

      return response;
    };

    it('Should Have Call API Pending Add', async () => {
      const pendingAction =
        cardholderMaintenanceActions.addUpdateExpandedAddress.pending(
          'cardholderMaintenance/addUpdateExpandedAddress',
          { storeId, postData: { accountId: storeId } }
        );
      const actual = rootReducer(store.getState(), pendingAction)
        .cardholderMaintenance[storeId].expandedAddress;
      expect(actual.isLoading).toEqual(true);
    });

    it('Should Have Call API', async () => {
      const showToast = toastSpy('addToast');
      const getAddressInfo = cardholderMaintenanceSpy('getAddressInfo');
      const postMemo = spyMemoActions('postMemo');
      await callApi();

      const state =
        store.getState().cardholderMaintenance[storeId].expandedAddress;

      expect(postMemo).toHaveBeenCalled();
      expect(getAddressInfo).toBeCalledWith({
        storeId,
        idAddress,
        isSetRootLoading: true
      });
      expect(showToast).toBeCalledWith({
        message: 'txt_expanded_address_submitted',
        show: true,
        type: 'success'
      });
      expect(state.isLoading).toEqual(false);
      expect(state.openModal).toEqual('');
    });

    it('Should Have Call API Rejected Add', async () => {
      const showToast = toastSpy('addToast');
      await callApi(true);
      const state =
        store.getState().cardholderMaintenance[storeId].expandedAddress
          .isLoading;
      expect(showToast).toBeCalledWith({
        message: 'txt_expanded_address_submitted_failed',
        show: true,
        type: 'error'
      });
      expect(state).toEqual(false);
    });
  });

  describe.only('Test Api Add, with mapping data', () => {
    let store: Store<RootState>;
    beforeEach(() => {
      const updatedState = {
        ...initialState,
        form: {
          [`${storeId}-${VIEW_NAME_ADDRESS.ADD_ADDRESS}`]: {
            values: {
              ...initialState.form![
                `${storeId}-${VIEW_NAME_ADDRESS.ADD_ADDRESS}`
              ].values,
              houseBuilding: 'mock building',
              addressLineFour: 'mock addr line 4',
              addressLineThree: 'mock addr line 3',
              addressLineTwo: 'mock addr line 2',
              attention: 'mock attention',
              streetName: 'mock street name',
              POBox: 'mock PO Box',
              houseNumber: 'mock house'
            }
          }
        }
      };

      store = createStore(rootReducer, updatedState);
    });

    const callApi = async (isError = false) => {
      if (!isError) {
        jest
          .spyOn(apiService, 'addAndUpdateExpandedAddress')
          .mockResolvedValue(responseData);
      } else {
        jest
          .spyOn(apiService, 'addAndUpdateExpandedAddress')
          .mockRejectedValue(new Error('Async'));
      }
      const response =
        await cardholderMaintenanceActions.addUpdateExpandedAddress({
          storeId,
          postData: { accountId: storeId }
        })(store.dispatch, store.getState, {});

      return response;
    };

    it('Should Have Call API with mapping data', async () => {
      const showToast = toastSpy('addToast');
      const getAddressInfo = cardholderMaintenanceSpy('getAddressInfo');
      const postMemo = spyMemoActions('postMemo');

      await callApi();

      const state =
        store.getState().cardholderMaintenance[storeId].expandedAddress;

      expect(postMemo).toHaveBeenCalled();
      expect(getAddressInfo).toBeCalledWith({
        storeId,
        idAddress,
        isSetRootLoading: true
      });
      expect(showToast).toBeCalledWith({
        message: 'txt_expanded_address_submitted',
        show: true,
        type: 'success'
      });
      expect(state.isLoading).toEqual(false);
      expect(state.openModal).toEqual('');
    });
  });

  describe.only('Test Api Update', () => {
    let store: Store<RootState>;
    beforeEach(() => {
      store = createStore(rootReducer, {
        ...initialState,
        cardholderMaintenance: {
          [storeId]: {
            ...defaultStateCardholder,

            expandedAddress: {
              histories: [],
              openModal: 'EDIT',
              isLoading: false,
              isEndLoadMore: false
            },
            cardholderId: {
              cardNumberValue: 'VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)',
              cardholderName: 'ENDY2,HARRYY T',
              cardholderRole: '01',
              customerExternalIdentifier: 'C20273234913858500085018',
              customerRoleTypeCode: '01',
              customerType: '01',
              id: 'C20273234913858500085018',
              memberSequence: '1',
              status: '',
              statusCode: ''
            },
            isLoading: true
          }
        }
      });
    });

    const callApi = async (isError = false) => {
      if (!isError) {
        jest
          .spyOn(apiService, 'addAndUpdateExpandedAddress')
          .mockResolvedValue(responseData);
      } else {
        jest
          .spyOn(apiService, 'addAndUpdateExpandedAddress')
          .mockRejectedValue(new Error('Async'));
      }
      const response =
        await cardholderMaintenanceActions.addUpdateExpandedAddress({
          storeId,
          postData: { accountId: storeId }
        })(store.dispatch, store.getState, {});

      return response;
    };
    it('Should Have Call API Pending Update', async () => {
      const pendingAction =
        cardholderMaintenanceActions.addUpdateExpandedAddress.pending(
          'cardholderMaintenance/addUpdateExpandedAddress',
          { storeId, postData: { accountId: storeId } }
        );

      const actual = rootReducer(store.getState(), pendingAction)
        .cardholderMaintenance[storeId].expandedAddress;

      expect(actual.isLoading).toEqual(true);
    });

    it('Should Have Call API ', async () => {
      const showToast = toastSpy('addToast');
      const getAddressInfo = cardholderMaintenanceSpy('getAddressInfo');
      const postMemo = spyMemoActions('postMemo');
      await callApi();

      const state =
        store.getState().cardholderMaintenance[storeId].expandedAddress;

      expect(postMemo).toHaveBeenCalled();
      expect(getAddressInfo).toBeCalledWith({
        storeId,
        idAddress,
        isSetRootLoading: true
      });
      expect(showToast).toBeCalledWith({
        message: 'txt_expanded_address_updated',
        show: true,
        type: 'success'
      });
      expect(state.isLoading).toEqual(false);
      expect(state.openModal).toEqual('');
    });

    it('Should Have Call API Rejected Update', async () => {
      const showToast = toastSpy('addToast');
      await callApi(true);
      const state =
        store.getState().cardholderMaintenance[storeId].expandedAddress
          .isLoading;
      expect(showToast).toBeCalledWith({
        message: 'txt_expanded_address_updated_failed',
        show: true,
        type: 'error'
      });
      expect(state).toEqual(false);
    });
  });
});
