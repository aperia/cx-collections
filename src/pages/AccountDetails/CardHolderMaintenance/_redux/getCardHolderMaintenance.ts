import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import {
  IGetCardHolderPayload,
  IGetCardHolderArgs,
  IInitStateCardholder,
  ICardholderId,
  ICardholder
} from '../types';
import { cardholderMaintenanceActions, defaultStateCardholder } from '.';
import {
  mappingDataFromArray,
  isArrayHasValue,
  parseJSONString
} from 'app/helpers';

//service
import apiService from '../cardholdersMaintenanceService';

// constant
import { FIELD_REQUEST } from '../constants';
import {
  AUTH_CBR_FLAG,
  CARD_DEVICE_TYPE,
  CARD_FLAG_TYPE,
  CARD_SALUTATION,
  CARD_SOLICITATION,
  EMAIL_STATUS,
  EXTERNAL_STATUS,
  SOLICIT_FLAG
} from 'app/constants';
import isEmpty from 'lodash.isempty';

export const getCardHolderMaintenance = createAsyncThunk<
  IGetCardHolderPayload,
  IGetCardHolderArgs,
  ThunkAPIConfig
>('cardholderMaintenance/getCardholderMaintenance', async (args, thunkAPI) => {
  const { mapping } = thunkAPI.getState();
  const { dispatch } = thunkAPI;
  const { accEValue, isCallCheckType, storeId, translateFn: t } = args;
  const requestData = {
    common: { accountId: accEValue },
    selectFields: FIELD_REQUEST
  };
  try {
    const { data } = await apiService.getCardholderMaintenance(requestData);
    const { customerInquiry } = data;
    const formatData = customerInquiry.map(item => {
      const socialSecurityIdentifier = parseJSONString(
        item?.socialSecurityIdentifier
      );
      const length = socialSecurityIdentifier?.maskedValue?.length;
      const solicitationCode = isEmpty(item?.solicitationCode?.trim())
        ? t('txt_solicitation_allowed')
        : isEmpty(CARD_SOLICITATION[item?.solicitationCode])
        ? item?.solicitationCode
        : t(CARD_SOLICITATION[item?.solicitationCode]);
      return {
        ...item,
        birthDate: parseJSONString(item?.birthDate),
        presentationInstrumentIdentifier: parseJSONString(
          item?.presentationInstrumentIdentifier
        ),
        socialSecurityIdentifier: {
          ...socialSecurityIdentifier,
          maskedValue: !isEmpty(socialSecurityIdentifier?.maskedValue)
            ? socialSecurityIdentifier?.maskedValue?.slice(length - 4, length)
            : ''
        },
        presentationInstrumentStatusCodeDesc: t(
          EXTERNAL_STATUS[item?.presentationInstrumentStatusCode]
        ),
        customerRole: item?.customerRoleTypeCode,
        customerRoleTypeCode: item?.customerRoleTypeCode,
        electronicMailHomeStatusIndicatorDesc: t(
          EMAIL_STATUS[item?.electronicMailHomeStatusIndicator]
        ),
        electronicMailHomeSolicitIndicatorDesc: t(
          SOLICIT_FLAG[item?.electronicMailHomeSolicitIndicator]
        ),
        electronicMailWorkStatusIndicatorDesc: t(
          EMAIL_STATUS[item?.electronicMailWorkStatusIndicator]
        ),
        electronicMailWorkSolicitIndicatorDesc: t(
          SOLICIT_FLAG[item?.electronicMailWorkSolicitIndicator]
        ),
        homePhoneDeviceCodeDesc: t(CARD_DEVICE_TYPE[item?.homePhoneDeviceCode]),
        homePhoneStatusCodeDesc: t(CARD_FLAG_TYPE[item?.homePhoneStatusCode]),
        businessPhoneDeviceCodeDesc: t(
          CARD_DEVICE_TYPE[item?.businessPhoneDeviceCode]
        ),
        businessTelephoneFlagCodeDesc: t(
          CARD_FLAG_TYPE[item?.businessTelephoneFlagCode]
        ),
        mobilePhoneDeviceCodeDesc: t(
          CARD_DEVICE_TYPE[item?.mobilePhoneDeviceCode]
        ),
        mobilePhoneFlagCodeDesc: t(CARD_FLAG_TYPE[item?.mobilePhoneFlagCode]),
        facsimilePhoneDeviceCodeDesc: t(
          CARD_DEVICE_TYPE[item?.facsimilePhoneDeviceCode]
        ),
        facsimilePhoneFlagCodeDesc: t(
          CARD_FLAG_TYPE[item?.facsimilePhoneFlagCode]
        ),
        clientControlledPhoneDeviceCodeDesc: t(
          CARD_DEVICE_TYPE[item?.clientControlledPhoneDeviceCode]
        ),
        clientControlledPhoneFlagIndicatorDesc: t(
          CARD_FLAG_TYPE[item?.clientControlledPhoneFlagIndicator]
        ),
        creditBureauReportCode: t(AUTH_CBR_FLAG[item?.creditBureauReportCode]),
        salutationCode: t(CARD_SALUTATION[item?.salutationCode]),
        solicitationCode,
        externalStatusCode: item?.presentationInstrumentStatusCode,
        externalStatusDesc: t(
          EXTERNAL_STATUS[item?.presentationInstrumentStatusCode]
        )
      };
    });
    const mappingCardholdersId =
      mapping?.data?.getCardholderMaintenance?.cardholdersId;
    const cardholdersId: ICardholderId[] = mappingDataFromArray(
      formatData,
      mappingCardholdersId!
    );

    const mappingCardholders =
      mapping?.data?.getCardholderMaintenance?.cardholders;

    const cardholders: ICardholder[] = mappingDataFromArray(
      formatData,
      mappingCardholders!
    );
    isCallCheckType &&
      dispatch(
        cardholderMaintenanceActions.checkTypeAddress({
          storeId,
          accEValue: cardholdersId[0].cardNumberValue
        })
      );
    return {
      cardholdersId,
      cardholders
    };
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const getCardHolderMaintenanceBuilder = (
  builder: ActionReducerMapBuilder<IInitStateCardholder>
) => {
  builder
    .addCase(getCardHolderMaintenance.pending, (draftState, action) => {
      const { storeId, ignoreInitialState } = action.meta.arg;

      if (!ignoreInitialState) {
        draftState[storeId] = {
          ...draftState[storeId],
          ...defaultStateCardholder
        };
      }

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true
      };
    })
    .addCase(getCardHolderMaintenance.fulfilled, (draftState, action) => {
      const { storeId, ignoreInitialState, isSetRootLoading } = action.meta.arg;
      const { cardholdersId, cardholders } = action.payload;

      draftState[storeId].cardholdersId = cardholdersId;
      draftState[storeId].cardholders = cardholders;
      if (isArrayHasValue(cardholdersId) && !ignoreInitialState) {
        draftState[storeId].cardholderId = cardholdersId[0];
      }
      if (isSetRootLoading) {
        draftState[storeId].isLoading = false;
      }
    })
    .addCase(getCardHolderMaintenance.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false
      };
    });
};
