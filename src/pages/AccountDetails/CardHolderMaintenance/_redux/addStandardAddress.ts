import { batch } from 'react-redux';
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import {
  AddStandardAddressRequest,
  IInitStateCardholder,
  IPostStandardAddressArgs
} from '../types';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// service
import apiService from '../cardholdersMaintenanceService';
import { cardholderMaintenanceActions } from '.';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { CARDHOLDER_MAINTENANCE_ACTIONS } from 'pages/Memos/constants';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { CARD_ROLE } from 'app/constants';
import { TYPE_ADDRESS } from '../ExpandedAddress/constants';
import { CHRONICLE_CARD_HOLDER_MAINTENANCE_KEY } from 'pages/ClientConfiguration/Memos/constants';
import isEmpty from 'lodash.isempty';

export const triggerAddStandardAddress = createAsyncThunk<
  void,
  IPostStandardAddressArgs,
  ThunkAPIConfig
>('cardholderMaintenance/addStandardAddress', async (args, thunkAPI) => {
  const { dispatch, rejectWithValue } = thunkAPI;
  const { storeId } = args;
  const { accountDetail } = thunkAPI.getState();
  try {
    const {
      accEValue,
      addressLineOne,
      addressLineTwo,
      city,
      stateSubdivision,
      postalCode
    } = args;

    let requestData: AddStandardAddressRequest = {
      common: { accountId: accEValue },
      city,
      addressLineOne,
      addressLineTwo,
      state: stateSubdivision?.fieldId,
      zipCode: postalCode
    };

    if (addressLineTwo) {
      requestData = {
        ...requestData,
        letter: 'Y',
        propagate: 'Y',
        bypass: 'B'
      };
    }

    await apiService.addStandardAddress(requestData);
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'txt_standard_address_submitted'
        })
      );

      dispatch(
        cardholderMaintenanceActions.getAddressInfo({
          storeId,
          isSetRootLoading: true
        })
      );
      // API ERROR
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inModalBody'
        })
      );
      // Post memo
      // only primary can add Address
      const addValue = `ADDRESS LINE 1 ${addressLineOne}${
        !isEmpty(addressLineTwo) ? `, ADDRESS LINE 2 ${addressLineTwo}` : ''
      }, CITY ${city}, STATE ${
        stateSubdivision?.description
      }, ZIP CODE ${postalCode}`;
      dispatch(
        memoActions.postMemo(
          CARDHOLDER_MAINTENANCE_ACTIONS.ADD_STANDARD_ADDRESS,
          {
            storeId,
            accEValue,
            chronicleKey: CHRONICLE_CARD_HOLDER_MAINTENANCE_KEY,
            cardholderMaintenance: {
              cardholderContactedName:
                accountDetail[storeId]?.selectedAuthenticationData
                  ?.customerNameForCreateWorkflow,
              cardholderContactedNameAuth:
                accountDetail[storeId]?.selectedAuthenticationData
                  ?.customerName,
              role: CARD_ROLE[TYPE_ADDRESS.PRIMARY],
              addValue
            }
          }
        )
      );
    });
  } catch (error) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: 'txt_standard_address_submitted_failed'
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inModalBody',
          apiResponse: error
        })
      );
    });

    return rejectWithValue(error);
  }
});

export const addStandardAddressBuilder = (
  builder: ActionReducerMapBuilder<IInitStateCardholder>
) => {
  builder
    .addCase(triggerAddStandardAddress.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].standardAddress.isLoadingAddAddress = true;
    })
    .addCase(triggerAddStandardAddress.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].standardAddress.isLoadingAddAddress = false;
      draftState[storeId].standardAddress.isOpenModalAdd = false;
    })
    .addCase(triggerAddStandardAddress.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].standardAddress.isLoadingAddAddress = false;
    });
};
