import {
  cardholderMaintenanceActions,
  defaultStateCardholder,
  reducer
} from '.';
import { storeId } from 'app/test-utils';
import {
  ExpandState,
  ICardholderId,
  IChangeCardholder,
  IInitStateCardholder,
  ModalExpandedAddress
} from '../types';
import { TYPE_EDIT } from '../TelePhone/constants';

const initialState: IInitStateCardholder = {
  [storeId]: {
    ...defaultStateCardholder,
    isLoading: true
  }
};

const addressType = {
  POBox: '',
  addressCategory: {
    fieldValue: 'Permanent',
    fieldID: 'P',
    description: 'P - Permanent'
  },
  addressLineFour: '',
  addressLineOne: '333 MENLAY',
  addressLineThree: '',
  addressLineTwo: '',
  addressRelationship: {
    fieldValue: 'Unknown',
    fieldID: 'U',
    description: 'U - Unknown'
  },
  addressType: {
    fieldValue: 'Billing',
    fieldID: 'BLL1',
    description: 'BLL1 - Billing'
  },
  attention: '',
  city: 'OMAHA',
  countryISO: {
    fieldValue: 'United States of America',
    fieldID: 'USA',
    description: 'USA - United States of America'
  },
  effectiveFrom: '2021-05-18',
  effectiveTo: '9999-12-31',
  formattedAddressCode: 'F',
  houseBuilding: '',
  houseNumber: '',
  id: '00001-P-BLL1-2021-06-18-9999-12-31',
  memberSequence: '00001',
  postalCode: '10003',
  stateSubdivision: {
    fieldValue: 'New York',
    fieldID: 'NY',
    description: 'NY - New York'
  },
  streetName: ''
};

describe('CardholderMaintenance > reducer', () => {
  it('removeStore', () => {
    // invoke
    const params = { storeId };
    const state = reducer(
      initialState,
      cardholderMaintenanceActions.removeStore(params)
    );

    // expect
    expect(state[storeId]).toBeUndefined();
  });

  it('changeCardholder', () => {
    const cardholderId: ICardholderId = {
      cardNumberValue: 'VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)',
      cardholderName: 'ENDY2,HARRYY T',
      cardholderRole: '01',
      customerExternalIdentifier: 'C20273234913858500085018',
      customerRoleTypeCode: '01',
      customerType: '01',
      id: 'C20273234913858500085018',
      memberSequence: '00001',
      status: '',
      statusCode: ''
    };
    // invoke
    const params: IChangeCardholder = { storeId, cardholderId };
    const state = reducer(
      initialState,
      cardholderMaintenanceActions.changeCardholder(params)
    );

    // expect
    expect(state[storeId]).toMatchObject({
      ...defaultStateCardholder,
      cardholderId,
      isExpandInfo: true,
      isExpandAddress: false,
      isExpandEmail: false,
      isExpandTelePhone: false
    });
  });

  it('changeAddressType', () => {
    // invoke
    const params = { storeId, addressType };
    const state = reducer(
      initialState,
      cardholderMaintenanceActions.changeAddressType(params)
    );

    // expect
    expect(state[storeId].addressType).toEqual(addressType);
  });

  it('changeAddressTypes', () => {
    // invoke
    const params = { storeId, addressTypes: [addressType] };
    const state = reducer(
      initialState,
      cardholderMaintenanceActions.changeAddressTypes(params)
    );

    // expect
    expect(state[storeId].addressTypes).toEqual(params.addressTypes);
  });

  it('changeExpand', () => {
    // invoke
    const expandState: ExpandState = 'isExpandAddress';
    const params = { storeId, expandState };
    const state = reducer(
      initialState,
      cardholderMaintenanceActions.changeExpand(params)
    );

    // expect
    expect(state[storeId].isExpandAddress).toEqual(true);
  });

  it('updateReload', () => {
    // invoke
    const reload = true;
    const params = { storeId, reload };
    const state = reducer(
      initialState,
      cardholderMaintenanceActions.updateReload(params)
    );

    // expect
    expect(state[storeId].reload).toEqual(reload);
  });

  it('type is null', () => {
    // invoke
    const params = { storeId, open: true };
    const state = reducer(
      initialState,
      cardholderMaintenanceActions.updateModalEdit(params)
    );

    // expect
    expect(state[storeId].isOpenModalPhoneEmail).toEqual(true);
    expect(state[storeId].typeEdit).toEqual(TYPE_EDIT.PHONE);
  });

  it('type is null', () => {
    // invoke
    const params = { storeId, open: true, type: TYPE_EDIT.EMAIL };
    const state = reducer(
      initialState,
      cardholderMaintenanceActions.updateModalEdit(params)
    );

    // expect
    expect(state[storeId].isOpenModalPhoneEmail).toEqual(true);
    expect(state[storeId].typeEdit).toEqual(TYPE_EDIT.EMAIL);
  });

  it('toggleModalStandardAddressHistory', () => {
    // invoke
    const isOpen = true;
    const params = { storeId, isOpen };
    const state = reducer(
      initialState,
      cardholderMaintenanceActions.toggleModalStandardAddressHistory(params)
    );

    // expect
    expect(state[storeId].standardAddress.isOpenModalAddressHistory).toEqual(
      isOpen
    );
  });

  it('toggleModalStandardAddAddress', () => {
    // invoke
    const isOpen = true;
    const params = { storeId, isOpen };
    const state = reducer(
      initialState,
      cardholderMaintenanceActions.toggleModalStandardAddAddress(params)
    );

    // expect
    expect(state[storeId].standardAddress.isOpenModalAdd).toEqual(isOpen);
  });

  it('toggleModalStandardEditAddress', () => {
    // invoke
    const isOpen = true;
    const params = { storeId, isOpen };
    const state = reducer(
      initialState,
      cardholderMaintenanceActions.toggleModalStandardEditAddress(params)
    );

    // expect
    expect(state[storeId].standardAddress.isOpenModalEdit).toEqual(isOpen);
  });
  it('changeModalAddEditHistoryAddress', () => {
    // invoke
    const type: ModalExpandedAddress = 'EDIT';
    const params = { storeId, type };
    const state = reducer(
      initialState,
      cardholderMaintenanceActions.changeModalAddEditHistoryAddress(params)
    );

    // expect
    expect(state[storeId].expandedAddress.openModal).toEqual(type);
  });

  it('clearExpandedAddressHistory', () => {
    // invoke
    const params = { storeId };
    const state = reducer(
      initialState,
      cardholderMaintenanceActions.clearExpandedAddressHistory(params)
    );

    // expect
    expect(state[storeId].expandedAddress.histories).toEqual([]);
  });
});
