import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { IInitStateCardholder, CheckTypeAddressArgs } from '../types';

//service
import apiService from '../cardholdersMaintenanceService';
import { cardholderMaintenanceActions } from '.';

export const checkTypeAddress = createAsyncThunk<
  MagicKeyValue,
  CheckTypeAddressArgs,
  ThunkAPIConfig
>('checkTypeofAddress', async (args, thunkAPI) => {
  const { accEValue, storeId } = args;
  const requestData = {
    common: {
      user: 'string',
      cycle: '',
      debug: false,
      org: 'string',
      processor: false,
      contentType: 'string',
      retry: 0,
      timeout: 0,
      cid: 'string',
      eventOrigin: 'string',
      eventOriginInputName: 'string',
      externalCustomerId: 'string',
      presentationId: 'string',
      accountId: accEValue
    },
    selectFields: ['expansionAddressCode'],
    odsNaming: false
  };
  try {
    const { data } = await apiService.checkTypeofAddress(requestData);
    const { expansionAddressCode } = data;
    const { dispatch } = thunkAPI;
    dispatch(
      cardholderMaintenanceActions.getAddressInfo({
        storeId
      })
    );
    return {
      expansionAddressCode
    };
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const checkTypeAddressBuilder = (
  builder: ActionReducerMapBuilder<IInitStateCardholder>
) => {
  builder
    .addCase(checkTypeAddress.pending, (draftState, action) => {})
    .addCase(checkTypeAddress.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { expansionAddressCode } = action.payload;
      draftState[storeId].expansionAddressCode = expansionAddressCode;
    })
    .addCase(checkTypeAddress.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].isLoading = false;
    });
};
