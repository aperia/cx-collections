import * as selectors from './selector';
import { storeId, selectorWrapper } from 'app/test-utils';
import isEmpty from 'lodash.isempty';

//service
import { ICardholder, ICardholderId, ICardholderReducer } from '../types';
import { VIEW_NAME_ADDRESS } from '../ExpandedAddress/constants';
import { ADDRESS_EXPANSION_CODE } from '../constants';
import { TYPE_EDIT } from '../TelePhone/constants';

const cardholderId: ICardholderId = {
  id: '1',
  cardholderName: 'Smith Williams',
  cardholderRole: '01',
  status: 'Closed',
  statusCode: 'C',
  cardNumberValue: '',
  memberSequence: '',
  customerRoleTypeCode: '',
  customerType: '',
  customerExternalIdentifier: ''
};

const addressType = {
  POBox: '',
  addressCategory: {
    fieldValue: 'Permanent',
    fieldID: 'P',
    description: 'P - Permanent'
  },
  addressLineFour: '',
  addressLineOne: '333 MENLAY',
  addressLineThree: '',
  addressLineTwo: '',
  addressRelationship: {
    fieldValue: 'Unknown',
    fieldID: 'U',
    description: 'U - Unknown'
  },
  addressType: {
    fieldValue: 'Billing',
    fieldID: 'BLL1',
    description: 'BLL1 - Billing'
  },
  attention: '',
  city: 'OMAHA',
  countryISO: {
    fieldValue: 'United States of America',
    fieldID: 'USA',
    description: 'USA - United States of America'
  },
  effectiveFrom: '2021-05-18',
  effectiveTo: '9999-12-31',
  formattedAddressCode: 'F',
  houseBuilding: '',
  houseNumber: '',
  id: '00001-P-BLL1-2021-06-18-9999-12-31',
  memberSequence: '00001',
  postalCode: '10003',
  stateSubdivision: {
    fieldValue: 'New York',
    fieldID: 'NY',
    description: 'NY - New York'
  },
  streetName: ''
};

const letephoneProp = {
  less: 'less',
  more: {
    deviceType: { value: 'deviceType' },
    flag: { value: 'flag' },
    lastUpdated: undefined
  }
};

const cardholder: ICardholder = {
  cardholderId: '1',
  cardholderInfo: {
    memberSequence: '0001',
    cardNumber: '041200••••••3412',
    lastName: 'Smith',
    firstName: 'Williams',
    middleName: 'Chandra',
    cardholderRole: '01 - Primary',
    authCBRFlag: '1 - Individual',
    suffix: 'Suffix',
    prefix: 'Prefix',
    title: '',
    qualification: '',
    embossingName: '',
    salutation: 'U - Unknown',
    dateOfBirth: '10/10/2020',
    ssnTaxId: '',
    solicitation: 'S - Do not solicit this cardholder'
  },
  email: {},
  telephone: {
    fax: letephoneProp,
    homePhone: letephoneProp,
    miscellaneous: letephoneProp,
    mobilePhone: letephoneProp,
    workPhone: letephoneProp
  },
  standardAddressInfoHistory: []
};

const initialState: Partial<RootState> = {
  form: {
    [`${storeId}-${VIEW_NAME_ADDRESS.ADD_ADDRESS}`]: {
      registeredFields: [],
      values: {
        addressCategory: { fieldID: 'addressCategory' },
        addressType: { fieldID: 'addressType' },
        effectiveFrom: {},
        effectiveTo: {},
        countryISO: { fieldID: 'countryISO' }
      }
    }
  },
  cardholderMaintenance: {
    [storeId]: {
      cardholderId: cardholderId,
      cardholdersId: [cardholderId],
      cardholders: [cardholder],
      addressType,
      addressTypes: [addressType],
      expandedAddress: {
        histories: [],
        openModal: '',
        isLoading: false,
        isEndLoadMore: false
      },
      standardAddress: {
        isOpenModalAdd: true,
        isOpenModalEdit: true,
        isOpenModalAddressHistory: true,
        isLoadingAddAddress: true,
        isLoadingEditAddress: true
      },
      isLoading: false,
      isExpandAddress: false,
      isExpandEmail: false,
      isExpandTelePhone: false,
      isExpandInfo: true,
      isOpenModalPhoneEmail: false,
      reload: false,
      loadingEdit: false,
      expansionAddressCode: ADDRESS_EXPANSION_CODE.EXPANDED,
      typeEdit: TYPE_EDIT.PHONE
    } as ICardholderReducer
  }
};

const selectorTrigger = selectorWrapper(initialState);

describe('CardholderMaintenance Selector', () => {
  it('selectRootLoading ', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectRootLoading);

    expect(data).toEqual(
      initialState.cardholderMaintenance![storeId].isLoading
    );
    expect(emptyData).toBeUndefined();
  });

  it('getCardholder', () => {
    const { data, emptyData } = selectorTrigger(selectors.getCardholder);

    expect(data).toEqual(cardholder);
    expect(emptyData).toBeUndefined();
  });

  it('getCardholderAddressTypes', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.getCardholderAddressTypes
    );

    expect(data).toEqual([addressType]);
    expect(emptyData).toBeUndefined();
  });

  it('getCardholderAddressType', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.getCardholderAddressType
    );

    expect(data).toEqual(addressType);
    expect(emptyData).toBeUndefined();
  });

  it('getCardholdersId', () => {
    const { data, emptyData } = selectorTrigger(selectors.getCardholdersId);

    expect(data).toEqual([cardholderId]);
    expect(emptyData).toBeUndefined();
  });

  it('selectIsExpandedAddress', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectIsExpandedAddress
    );

    expect(data).toEqual(
      isEmpty(
        initialState.cardholderMaintenance![storeId].expansionAddressCode ===
          ADDRESS_EXPANSION_CODE.EXPANDED
      )
    );
    expect(emptyData).toBeFalsy();
  });

  it('selectCardholdersId', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectCardholdersId);

    expect(data).toEqual([cardholderId]);
    expect(emptyData).toBeUndefined();
  });

  it('selectCardholderId', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectCardholderId);

    expect(data).toEqual(cardholderId);
    expect(emptyData).toBeUndefined();
  });

  it('selectCardholderInfo', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectCardholderInfo);

    expect(data).toEqual(cardholder.cardholderInfo);
    expect(emptyData).toBeUndefined();
  });

  it('selectCardholderAddressTypes', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectCardholderAddressTypes
    );

    expect(data).toEqual([addressType]);
    expect(emptyData).toBeUndefined();
  });

  it('selectCardholderAddressType', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectCardholderAddressType
    );

    expect(data).toEqual(addressType);
    expect(emptyData).toBeUndefined();
  });

  it('selectCardholderAddressInfo', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectCardholderAddressInfo
    );

    expect(data).toEqual(addressType);
    expect(emptyData).toBeUndefined();
  });

  it('selectExpandedAddressHistory', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectExpandedAddressHistory
    );

    expect(data).toEqual(
      initialState.cardholderMaintenance![storeId].expandedAddress.histories
    );
    expect(emptyData).toBeUndefined();
  });

  it('selectCardholderStandardAddressHistory', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectCardholderStandardAddressHistory
    );

    expect(data).toEqual(cardholder.standardAddressInfoHistory);
    expect(emptyData).toEqual([]);
  });

  it('selectIsExpandAddress', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectIsExpandAddress
    );

    expect(data).toEqual(
      initialState.cardholderMaintenance![storeId].isExpandAddress
    );
    expect(emptyData).toBeUndefined();
  });

  it('selectIsExpandInfo', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectIsExpandInfo);

    expect(data).toEqual(
      initialState.cardholderMaintenance![storeId].isExpandInfo
    );
    expect(emptyData).toBeUndefined();
  });

  it('selectIsExpandTelePhone', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectIsExpandTelePhone
    );

    expect(data).toEqual(
      initialState.cardholderMaintenance![storeId].isExpandTelePhone
    );
    expect(emptyData).toBeUndefined();
  });

  it('selectIsExpandEmail', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectIsExpandEmail);

    expect(data).toEqual(
      initialState.cardholderMaintenance![storeId].isExpandEmail
    );
    expect(emptyData).toBeUndefined();
  });

  it('selectTelePhoneInfo', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectTelePhoneInfo);

    expect(data).toEqual(cardholder.telephone);
    expect(emptyData).toBeUndefined();
  });

  it('selectTelePhoneFormInfo', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectTelePhoneFormInfo
    );

    const more = {
      deviceType: { value: letephoneProp.more.deviceType.value },
      flag: { value: letephoneProp.more.flag.value },
      lastUpdated: undefined
    };

    const moreEmpty = { deviceType: null, flag: null, lastUpdated: null };

    expect(data).toEqual({
      homePhone: { less: 'less', more },
      workPhone: { less: 'less', more },
      mobilePhone: { less: 'less', more },
      fax: { less: 'less', more },
      miscellaneous: { less: 'less', more }
    });
    expect(emptyData).toEqual({
      homePhone: { more: moreEmpty },
      workPhone: { more: moreEmpty },
      mobilePhone: { more: moreEmpty },
      fax: { more: moreEmpty },
      miscellaneous: { more: moreEmpty }
    });
  });

  it('selectEmailInfo', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectEmailInfo);

    expect(data).toEqual(cardholder.email);
    expect(emptyData).toBeUndefined();
  });

  it('selectEmailInfo', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectEmailInfo);

    expect(data).toEqual(cardholder.email);
    expect(emptyData).toBeUndefined();
  });

  it('selectModalEdit', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectModalEdit);

    expect(data).toEqual(
      initialState.cardholderMaintenance![storeId].isOpenModalPhoneEmail
    );
    expect(emptyData).toBeUndefined();
  });

  it('selectTypeEdit', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectTypeEdit);

    expect(data).toEqual(initialState.cardholderMaintenance![storeId].typeEdit);
    expect(emptyData).toBeUndefined();
  });

  it('selectReload', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectReload);

    expect(data).toEqual(initialState.cardholderMaintenance![storeId].reload);
    expect(emptyData).toBeUndefined();
  });

  it('selectLoadingModal', () => {
    const { data, emptyData } = selectorTrigger(selectors.selectLoadingModal);

    expect(data).toEqual(
      initialState.cardholderMaintenance![storeId].loadingEdit
    );
    expect(emptyData).toBeUndefined();
  });

  it('selectIsOpenModalStandardAddAddress', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectIsOpenModalStandardAddAddress
    );

    expect(data).toEqual(
      initialState.cardholderMaintenance![storeId].standardAddress
        .isOpenModalAdd
    );
    expect(emptyData).toBeFalsy();
  });

  it('selectIsOpenModalStandardEditAddress', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectIsOpenModalStandardEditAddress
    );

    expect(data).toEqual(
      initialState.cardholderMaintenance![storeId].standardAddress
        .isOpenModalEdit
    );
    expect(emptyData).toBeFalsy();
  });

  it('selectModalExpandedAddress', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectModalExpandedAddress
    );

    expect(data).toEqual(
      initialState.cardholderMaintenance![storeId].expandedAddress.openModal
    );
    expect(emptyData).toBeUndefined();
  });

  it('selectAddressCategoryInForm', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectAddressCategoryInForm
    );

    expect(data).toEqual(
      initialState.form![`${storeId}-${VIEW_NAME_ADDRESS.ADD_ADDRESS}`].values
        ?.addressCategory?.fieldID
    );
    expect(emptyData).toBeUndefined();
  });

  it('selectAddressTypeInForm', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectAddressTypeInForm
    );

    expect(data).toEqual(
      initialState.form![`${storeId}-${VIEW_NAME_ADDRESS.ADD_ADDRESS}`].values
        ?.addressType?.fieldID
    );
    expect(emptyData).toBeUndefined();
  });

  it('selectEffectiveFromInForm', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectEffectiveFromInForm
    );

    expect(data).toEqual(
      initialState.form![`${storeId}-${VIEW_NAME_ADDRESS.ADD_ADDRESS}`].values
        ?.effectiveFrom
    );
    expect(emptyData).toBeUndefined();
  });

  it('selectEffectiveToInForm', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectEffectiveToInForm
    );

    expect(data).toEqual(
      initialState.form![`${storeId}-${VIEW_NAME_ADDRESS.ADD_ADDRESS}`].values
        ?.effectiveTo
    );
    expect(emptyData).toBeUndefined();
  });

  it('selectCountryISOInForm', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.selectCountryISOInForm
    );

    expect(data).toEqual(
      initialState.form![`${storeId}-${VIEW_NAME_ADDRESS.ADD_ADDRESS}`].values
        ?.countryISO?.fieldID
    );
    expect(emptyData).toBeUndefined();
  });

  it('isLoadingExpandedAddress', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.isLoadingExpandedAddress
    );

    expect(data).toEqual(
      initialState.cardholderMaintenance![storeId]?.expandedAddress?.isLoading
    );
    expect(emptyData).toBeUndefined();
  });

  it('isEndLoadMore', () => {
    const { data, emptyData } = selectorTrigger(selectors.isEndLoadMore);

    expect(data).toEqual(
      initialState.cardholderMaintenance![storeId]?.expandedAddress
        ?.isEndLoadMore
    );
    expect(emptyData).toBeUndefined();
  });

  it('selectLoadingAddStandardAddress', () => {
    const { data } = selectorTrigger(selectors.selectLoadingAddStandardAddress);

    expect(data).toEqual(
      initialState.cardholderMaintenance![storeId]?.standardAddress
        ?.isLoadingAddAddress
    );
  });

  it('selectLoadingEditStandardAddress', () => {
    const { data } = selectorTrigger(
      selectors.selectLoadingEditStandardAddress
    );

    expect(data).toEqual(
      initialState.cardholderMaintenance![storeId]?.standardAddress
        ?.isLoadingAddAddress
    );
  });
});
