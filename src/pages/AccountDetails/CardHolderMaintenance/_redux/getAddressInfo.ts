import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import sortBy from 'lodash.sortby';

// Type
import {
  IAddressInfo,
  IArgsExpandedAddressInfo,
  IInitStateCardholder,
  IPayLoadExpandedAddressInfo
} from '../types';

// Const
import {
  FIELD_EXPANDED_ADDRESS_INFO,
  TYPE_ADDRESS_MATCH_WITH_API,
  TYPE_ADDRESS
} from '../ExpandedAddress/constants';

// Service
import apiService from '../cardholdersMaintenanceService';

// Helper
import {
  formatPostalCodeUSA,
  isArrayHasValue,
  mappingDataFromArray
} from 'app/helpers';
import { getExpandedAddress } from './mapping.json';
import { mappingRefData } from 'pages/AccountDetails/helpers';
import { isEmpty } from 'app/_libraries/_dls/lodash';

export const getAddressInfo = createAsyncThunk<
  IPayLoadExpandedAddressInfo,
  IArgsExpandedAddressInfo,
  ThunkAPIConfig
>('cardholderMaintenance/getAddressInfo', async (args, thunkAPI) => {
  const { storeId } = args;
  const { cardholderId } = thunkAPI?.getState()?.cardholderMaintenance[storeId];
  try {
    const requestData = {
      common: {
        accountId: cardholderId.cardNumberValue
      },
      selectFields: FIELD_EXPANDED_ADDRESS_INFO
    };

    const { data } = await apiService.getAddressInfo(requestData);

    const { AUTHORIZED, PRIMARY } = TYPE_ADDRESS;
    const { ADDITIONAL_USERS, PRIMARY_MATCH_API, SECOND_MATCH_API } =
      TYPE_ADDRESS_MATCH_WITH_API;
    let cardholderRole = '';
    if (cardholderId.cardholderRole === AUTHORIZED) {
      cardholderRole = ADDITIONAL_USERS;
    } else if (cardholderId.cardholderRole === PRIMARY) {
      cardholderRole = PRIMARY_MATCH_API;
    } else {
      cardholderRole = SECOND_MATCH_API;
    }

    if (!data[cardholderRole] || !isArrayHasValue(data[cardholderRole])) {
      return {
        addressTypes: []
      };
    }

    const slicePostCode = data[cardholderRole].map((item: MagicKeyValue) => {
      const { countryCode, postalCode } = item;
      return {
        ...item,
        postalCode: formatPostalCodeUSA(countryCode, postalCode)
      };
    });

    const mappingAddress: IAddressInfo[] = mappingDataFromArray(
      slicePostCode,
      getExpandedAddress
    );

    const filterAddress = mappingAddress.filter(
      (item: IAddressInfo) =>
        item.memberSequence === cardholderId.memberSequence
    );

    let addressTypes: IAddressInfo[] = [];

    for (const index in filterAddress) {
      addressTypes = [
        ...addressTypes,
        {
          ...(await mappingRefData(filterAddress[index], [
            'countryISO',
            'stateSubdivision',
            'addressCategory',
            'addressType',
            'addressRelationship'
          ]))
        }
      ];
    }

    const sortAddress: IAddressInfo[] = sortBy(addressTypes, [
      'addressCategory.fieldID',
      'addressType.fieldID'
    ]);

    return {
      addressTypes: sortAddress
    };
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const getAddressInfoBuilder = (
  builder: ActionReducerMapBuilder<IInitStateCardholder>
) => {
  builder
    .addCase(getAddressInfo.pending, (draftState, action) => {
      const { storeId, isSetRootLoading } = action.meta.arg;
      if (isSetRootLoading) {
        draftState[storeId].isLoading = true;
      }
    })
    .addCase(getAddressInfo.fulfilled, (draftState, action) => {
      const { storeId, idAddress } = action.meta.arg;

      const { addressTypes } = action.payload;
      if (!idAddress) {
        draftState[storeId].addressType = addressTypes[0] || {};
      } else {
        const findAddress: IAddressInfo = addressTypes.find(
          (item: IAddressInfo) => item.id === idAddress
        )!;
        if (!isEmpty(findAddress)) {
          draftState[storeId].addressType = findAddress;
        } else {
          draftState[storeId].addressType = addressTypes[0] || {};
        }
      }

      draftState[storeId].addressTypes = addressTypes;
      draftState[storeId].isLoading = false;
    })
    .addCase(getAddressInfo.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].isLoading = false;
    });
};
