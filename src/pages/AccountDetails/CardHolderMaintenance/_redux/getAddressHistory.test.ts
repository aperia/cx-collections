import { storeId } from 'app/test-utils';
import { rootReducer } from 'storeConfig';

import { cardholderMaintenanceActions, defaultStateCardholder } from './index';
import { createStore, Store } from '@reduxjs/toolkit';

//service
import apiService from '../cardholdersMaintenanceService';

const responseData = {
  data: {
    addressHistory: [
      {
        accountIdentifier:
          '{"maskedValue":"022002******5078","eValue":"VOL(Y1R1MH5uLyRqOzo1UFI/ew==)"}',
        accountNumber:
          '{"maskedValue":"022002******5078","eValue":"VOL(Y1R1MH5uLyRqOzo1UFI/ew==)"}',
        addressAttentionLineText:
          '                                                  ',
        addressCategoryCode: 'R',
        addressChangedTimestamp: '2021-05-12-01.31.34.040001',
        addressContinuationLine1Text:
          '111 STR                                           ',
        addressContinuationLine2Text:
          '                                                  ',
        addressContinuationLine3Text:
          '                                                  ',
        addressContinuationLine4Text:
          '                                                  ',
        addressFormatCode: 'F',
        addressHistoryRetentionCode: ' ',
        addressRelationshipTypeCode: 'U',
        addressSubdivisionOneText: 'NY                       ',
        addressSubdivisionTwoText: '                         ',
        addressTypeCode: 'BLL1',
        addressValidationCode: 'Y',
        cardholderAccountNumber:
          '{"maskedValue":"022002******5078","eValue":"VOL(Y1R1MH5uLyRqOzo1UFI/ew==)"}',
        changeTypeCode: 'D',
        cityName: 'CITY                     ',
        companyName: '                                                  ',
        convertedEnteredIdentifier:
          '{"maskedValue":"022002******5078","eValue":"VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)"}',
        countryCode: 'USA',
        customerExternalIdentifier: '                        ',
        customerRoleTypeCode: '01',
        customerTypeCode: '01',
        deliveryPointCode: '  ',
        effectiveFromDate: '2021-05-12',
        effectiveToDate: '2021-05-12',
        enteredIdentifier:
          '{"maskedValue":"022002******5078","eValue":"VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)"}',
        houseBuildingName: '                                                  ',
        houseIdentifier: '          ',
        mailCode: '000',
        mailUpdateCode: '0',
        memberSequenceIdentifier: '00001',
        postOfficeBoxIdentifier: '          ',
        postalCode: '10003     ',
        streetName: '                                       ',
        validAddressIndicator: ' '
      }
    ]
  }
} as any;

const initialState: Partial<RootState> = {
  cardholderMaintenance: {
    [storeId]: {
      ...defaultStateCardholder,
      expandedAddress: {
        histories: [],
        openModal: 'HISTORY',
        isLoading: false,
        isEndLoadMore: false
      },
      cardholderId: {
        cardNumberValue: 'VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)',
        cardholderName: 'ENDY2,HARRYY T',
        cardholderRole: '01',
        customerExternalIdentifier: 'C20273234913858500085018',
        customerRoleTypeCode: '01',
        customerType: '01',
        id: 'C20273234913858500085018',
        memberSequence: '00001',
        status: '',
        statusCode: ''
      },
      isLoading: true
    }
  }
};

describe('Test Api getExpandedAddressHistory', () => {
  let store: Store<RootState>;
  beforeEach(() => {
    store = createStore(rootReducer, initialState);
  });

  const callApi = async ({ isError = false, isEmpty = false }) => {
    if (!isError) {
      jest
        .spyOn(apiService, 'getExpandedAddressHistory')
        .mockResolvedValue(!isEmpty ? responseData : null);
    } else {
      jest
        .spyOn(apiService, 'getExpandedAddressHistory')
        .mockRejectedValue(new Error('Async'));
    }
    return await cardholderMaintenanceActions.getAddressHistory({
      storeId,
      accEValue: storeId,
      endSequence: 1,
      startSequence: 50,
      ignoreInitialState: true
    })(store.dispatch, store.getState, {});
  };

  it('Should Have Call API Pending', async () => {
    const pendingAction =
      cardholderMaintenanceActions.triggerDeleteExpandedAddress.pending(
        'cardholderMaintenance/getExpandedAddressHistory',
        { storeId, accEValue: storeId }
      );
    const actual = rootReducer(store.getState(), pendingAction)
      .cardholderMaintenance[storeId].expandedAddress;

    expect(actual.isLoading).toEqual(true);
    expect(actual.openModal).toEqual('HISTORY');
  });

  it('Should Have Call API Full Filed', async () => {
    await callApi({});
    const state =
      store.getState().cardholderMaintenance[storeId].expandedAddress;
    expect(state.isLoading).toEqual(false);
    expect(state.histories.length).toEqual(
      responseData?.data?.addressHistory?.length
    );
  });

  it('Should Have Call API Rejected', async () => {
    await callApi({ isError: true });
    const state =
      store.getState().cardholderMaintenance[storeId].expandedAddress;
    expect(state.isLoading).toEqual(false);
    expect(state.isEndLoadMore).toEqual(true);
  });
});
