import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { TYPE_EDIT } from 'pages/AccountDetails/CardHolderMaintenance/TelePhone/constants';
import {
  getCardHolderMaintenance,
  getCardHolderMaintenanceBuilder
} from './getCardHolderMaintenance';
import {
  updateCardHolderBuilder,
  triggerUpdateCardHolder
} from './updateCardHolder';
import {
  triggerAddStandardAddress,
  addStandardAddressBuilder
} from './addStandardAddress';
import {
  triggerEditStandardAddress,
  editStandardAddressBuilder
} from './editStandardAddress';
import {
  addUpdateExpandedAddress,
  addUpdateExpandedAddressBuilder
} from './addUpdateExpandedAddress';
import {
  getAddressHistory,
  getAddressHistoryBuilder
} from './getAddressHistory';
import { getAddressInfo, getAddressInfoBuilder } from './getAddressInfo';
import {
  ICardholderReducer,
  IInitStateCardholder,
  IChangeCardholder,
  IChangeAddressType,
  IChangeExpand,
  IChangeAddressTypes,
  UpdateModalEdit,
  UpdateReload,
  IToggleModal,
  ModalExpandedAddress
} from '../types';

import {
  triggerDeleteExpandedAddress,
  deleteExpandedAddressBuilder
} from './deleteExpandedAddress';

import {
  checkTypeAddress,
  checkTypeAddressBuilder
} from './checkTypeofAddress';

const initialState: IInitStateCardholder = {};

const { actions, reducer } = createSlice({
  name: 'cardholderMaintenance',
  initialState,
  reducers: {
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    changeCardholder: (
      draftState,
      action: PayloadAction<IChangeCardholder>
    ) => {
      const { storeId, cardholderId } = action.payload;
      draftState[storeId].isLoading = true;
      draftState[storeId].cardholderId = cardholderId;
      draftState[storeId].isExpandInfo = true;
      draftState[storeId].isExpandAddress = false;
      draftState[storeId].isExpandEmail = false;
      draftState[storeId].isExpandTelePhone = false;
    },
    changeAddressType: (
      draftState,
      action: PayloadAction<IChangeAddressType>
    ) => {
      const { storeId, addressType } = action.payload;
      draftState[storeId].addressType = addressType;
    },
    changeAddressTypes: (
      draftState,
      action: PayloadAction<IChangeAddressTypes>
    ) => {
      const { storeId, addressTypes } = action.payload;
      draftState[storeId].addressTypes = addressTypes;
    },
    changeExpand: (draftState, action: PayloadAction<IChangeExpand>) => {
      const { storeId, expandState } = action.payload;
      draftState[storeId][expandState] = !draftState[storeId][expandState];
    },
    updateReload: (draftState, action: PayloadAction<UpdateReload>) => {
      const { storeId, reload } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        reload
      };
    },
    updateModalEdit: (draftState, action: PayloadAction<UpdateModalEdit>) => {
      const { storeId, open, type } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isOpenModalPhoneEmail: open,
        typeEdit: type ? type : TYPE_EDIT.PHONE
      };
    },
    toggleModalStandardAddressHistory: (
      draftState,
      action: PayloadAction<IToggleModal>
    ) => {
      const { storeId, isOpen } = action.payload;
      draftState[storeId].standardAddress.isOpenModalAddressHistory = isOpen;
    },
    toggleModalStandardAddAddress: (
      draftState,
      action: PayloadAction<IToggleModal>
    ) => {
      const { storeId, isOpen } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        standardAddress: {
          ...draftState[storeId]?.standardAddress,
          isOpenModalAdd: isOpen
        }
      };
    },
    toggleModalStandardEditAddress: (
      draftState,
      action: PayloadAction<IToggleModal>
    ) => {
      const { storeId, isOpen } = action.payload;
      draftState[storeId].standardAddress.isOpenModalEdit = isOpen;
    },
    changeModalAddEditHistoryAddress: (
      draftState,
      action: PayloadAction<{ storeId: string; type: ModalExpandedAddress }>
    ) => {
      const { storeId, type } = action.payload;
      draftState[storeId].expandedAddress.openModal = type;
    },
    clearExpandedAddressHistory: (
      draftState,
      action: PayloadAction<{ storeId: string }>
    ) => {
      const { storeId } = action.payload;
      draftState[storeId].expandedAddress.histories = [];
    }
  },
  extraReducers: builder => {
    getCardHolderMaintenanceBuilder(builder);
    updateCardHolderBuilder(builder);
    addStandardAddressBuilder(builder);
    editStandardAddressBuilder(builder);
    addUpdateExpandedAddressBuilder(builder);
    getAddressHistoryBuilder(builder);
    checkTypeAddressBuilder(builder);
    getAddressInfoBuilder(builder);
    deleteExpandedAddressBuilder(builder);
  }
});

const allActions = {
  getCardHolderMaintenance,
  triggerUpdateCardHolder,
  triggerAddStandardAddress,
  triggerEditStandardAddress,
  addUpdateExpandedAddress,
  triggerDeleteExpandedAddress,
  getAddressHistory,
  checkTypeAddress,
  getAddressInfo,
  getAddressInfoBuilder,
  ...actions
};

export { allActions as cardholderMaintenanceActions, reducer };

export const defaultStateCardholder: Omit<ICardholderReducer, 'isLoading'> = {
  expansionAddressCode: '',
  cardholdersId: [],
  cardholderId: {
    id: '',
    cardholderName: '',
    cardholderRole: '',
    status: '',
    statusCode: '',
    cardNumberValue: '',
    memberSequence: '',
    customerRoleTypeCode: '',
    customerType: '',
    customerExternalIdentifier: ''
  },
  cardholders: [],
  addressTypes: [],
  addressType: {
    id: '',
    addressCategory: { description: '', fieldID: '', fieldValue: '' },
    addressType: { description: '', fieldID: '', fieldValue: '' },
    effectiveFrom: '',
    effectiveTo: '',
    countryISO: { description: '', fieldID: '', fieldValue: '' },
    houseNumber: '',
    houseBuilding: '',
    streetName: '',
    POBox: '',
    attention: '',
    addressLineOne: '',
    addressLineTwo: '',
    addressLineThree: '',
    addressLineFour: '',
    cityName: '',
    stateSubdivision: { description: '', fieldID: '', fieldValue: '' },
    postalCode: '',
    addressRelationship: { description: '', fieldID: '', fieldValue: '' },
    formattedAddressCode: ''
  },
  expandedAddress: {
    histories: [],
    openModal: '',
    isLoading: false,
    isEndLoadMore: false
  },
  standardAddress: {
    isLoadingAddAddress: false,
    isLoadingEditAddress: false,
    isOpenModalAdd: false,
    isOpenModalEdit: false,
    isOpenModalAddressHistory: false
  },
  isExpandAddress: false,
  isExpandEmail: false,
  isExpandTelePhone: false,
  isExpandInfo: true,
  isOpenModalPhoneEmail: false,
  typeEdit: TYPE_EDIT.PHONE,
  reload: false,
  loadingEdit: false
};
