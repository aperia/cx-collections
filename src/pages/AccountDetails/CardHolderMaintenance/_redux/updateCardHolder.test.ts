import {
  mockActionCreator,
  responseDefault,
  storeId,
  accEValue
} from 'app/test-utils';
import { rootReducer } from 'storeConfig';

import { cardholderMaintenanceActions, defaultStateCardholder } from './index';
import { createStore, Store } from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { TYPE_EDIT } from '../TelePhone/constants';
import { memoActions } from 'pages/Memos/_redux/reducers';
import apiService from '../cardholdersMaintenanceService';

const spyMemoActions = mockActionCreator(memoActions);
const toastSpy = mockActionCreator(actionsToast);

const responseData = {
  ...responseDefault,
  data: 'Success'
};

const postDataPhone = {
  businessPhoneDeviceCode: 'I',
  businessPhoneLastUpdatedDate: '05/06/2019',
  businessTelephoneFlagCode: 'B',
  clientControlledPhoneDeviceCode: 'I',
  clientControlledPhoneFlagIndicator: 'S',
  clientControlledPhoneIdentifier: '1212121212',
  clientControlledPhoneLastUpdatedDate: '04/23/2021',
  common: {
    accountId: 'VOL(dDNnXH5kSmxEN3c7bS9mOy4wXg==)',
    externalCustomerId: 'E19126151549146169061767'
  },
  facsimileLastUpdatedDate: '05/10/2021',
  facsimilePhoneDeviceCode: 'I',
  facsimilePhoneFlagCode: 'S',
  facsimilePhoneIdentifier: '2121211212',
  homePhoneDeviceCode: 'I',
  homePhoneLastUpdatedDate: '05/20/2021',
  homePhoneStatusCode: 'B',
  mobilePhoneDeviceCode: 'W',
  mobilePhoneFlagCode: 'Y',
  mobilePhoneIdentifier: '2323232323',
  mobilePhoneLastUpdatedDate: '05/06/2021',
  primaryCustomerHomePhoneIdentifier: '8960125471',
  primaryCustomerSecondPhoneIdentifier: '8960125473'
};

const postDataEmail = {
  electronicMailHomeAddressText: 'SIMULATE.EXPERIA1N@CITI.COM',
  electronicMailHomeSolicitIndicator: 'Y',
  electronicMailHomeStatusIndicator: 'Y',
  electronicMailWorkAddressText: 'TEST@APERIA.COM',
  electronicMailWorkSolicitIndicator: 'N',
  electronicMailWorkStatusIndicator: 'N',
  homeEmailStatusDesc: 'Y - Valid',
  homeSolicitFlagDesc: 'Y - Solicit allowed',
  workEmailStatusDesc: 'N - Invalid',
  workSolicitFlagDesc: 'N - Do not solicit'
};

const requestData = {
  storeId,
  accEValue,
  type: 'email',
  postData: {
    common: {
      accountId: storeId,
      externalCustomerId: storeId
    },
    data: postDataEmail
  }
};

const initialState: Partial<RootState> = {
  cardholderMaintenance: {
    [storeId]: {
      ...defaultStateCardholder,
      cardholderId: {
        cardNumberValue: 'VOL(M35ETnouVGl6Y0ghb0RzRGo0fA==)',
        cardholderName: 'GUNTUR,KRISHNA P',
        cardholderRole: '01',
        customerExternalIdentifier: 'C02136212149537508764041',
        customerRoleTypeCode: '01',
        customerType: '01',
        id: 'C02136212149537508764041',
        memberSequence: '00001',
        status: '',
        statusCode: ''
      },
      cardholders: [
        {
          cardholderId: 'C02136212149537508764041',
          cardholderInfo: {} as any,
          email: {
            electronicMailHomeAddressText: 'SIMULATE.EXPERIA1N@CITI.COM',
            electronicMailHomeSolicitIndicator: 'Y',
            electronicMailHomeStatusIndicator: 'Y',
            electronicMailWorkAddressText: 'TEST@APERIA.COM',
            electronicMailWorkSolicitIndicator: 'N',
            electronicMailWorkStatusIndicator: 'N',
            homeEmailStatusDesc: 'Y - Valid',
            homeSolicitFlagDesc: 'Y - Solicit allowed',
            workEmailStatusDesc: 'N - Invalid',
            workSolicitFlagDesc: 'N - Do not solicit'
          } as any
        } as any
      ],
      isLoading: true
    }
  }
};

describe('Test Api UpdateCardHolder with', () => {
  let store: Store<RootState>;
  beforeEach(() => {
    store = createStore(rootReducer, initialState);
  });

  const callApi = async ({ type = TYPE_EDIT.EMAIL, isError = false }) => {
    if (!isError) {
      jest
        .spyOn(apiService, 'updateCardHolder')
        .mockResolvedValue(responseData);
    } else {
      jest
        .spyOn(apiService, 'updateCardHolder')
        .mockRejectedValue(new Error('Async'));
    }

    return await cardholderMaintenanceActions.triggerUpdateCardHolder({
      ...requestData,
      postData: {
        ...requestData.postData,
        data:
          type === TYPE_EDIT.EMAIL ? requestData.postData.data : postDataPhone
      },
      type
    })(store.dispatch, store.getState, {});
  };

  describe('Test Api Update Email', () => {
    it('Should Have Call API Success', async () => {
      const postMemo = spyMemoActions('postMemo');
      const spyToast = toastSpy('addToast');
      await callApi({});

      expect(postMemo).not.toHaveBeenCalled();
      expect(spyToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: 'txt_cardholder_update_email_success'
      });
    });

    it('Should Have Call API Failed', async () => {
      const spyToast = toastSpy('addToast');
      await callApi({ isError: true });

      expect(spyToast).toBeCalledWith({
        show: true,
        type: 'error',
        message: 'txt_cardholder_update_email_fail'
      });
    });
  });

  describe('Test Api Update Phone', () => {
    it('Should Have Call API Success', async () => {
      const spyToast = toastSpy('addToast');
      await callApi({ type: TYPE_EDIT.PHONE });

      expect(spyToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: 'txt_cardholder_update_phone_success'
      });
    });

    it('Should Have Call API Failed', async () => {
      const spyToast = toastSpy('addToast');
      await callApi({ type: TYPE_EDIT.PHONE, isError: true });

      expect(spyToast).toBeCalledWith({
        show: true,
        type: 'error',
        message: 'txt_cardholder_update_phone_fail'
      });
    });
  });

  it('Should Have Call Pending', async () => {
    const pendingAction =
      cardholderMaintenanceActions.triggerUpdateCardHolder.pending(
        'cardholderMaintenance/updateCardholder',
        requestData
      );
    const actual = rootReducer(store.getState(), pendingAction);

    expect(actual.cardholderMaintenance[storeId].loadingEdit).toEqual(true);
  });

  it('Should Have Call FullField', async () => {
    const pendingAction =
      cardholderMaintenanceActions.triggerUpdateCardHolder.fulfilled(
        undefined,
        'cardholderMaintenance/updateCardholder',
        requestData
      );
    const actual = rootReducer(store.getState(), pendingAction);

    expect(actual.cardholderMaintenance[storeId].loadingEdit).toEqual(false);
    expect(actual.cardholderMaintenance[storeId].isOpenModalPhoneEmail).toEqual(
      false
    );
    expect(actual.cardholderMaintenance[storeId].reload).toEqual(true);
  });

  it('Should Have Call Reject', async () => {
    const pendingAction =
      cardholderMaintenanceActions.triggerUpdateCardHolder.rejected(
        null,
        'cardholderMaintenance/updateCardholder',
        requestData
      );

    const actual = rootReducer(store.getState(), pendingAction);

    expect(actual.cardholderMaintenance[storeId].loadingEdit).toEqual(false);
  });
});

describe('Test Api UpdateCardHolder postMemo', () => {
  const emailHome = {
    electronicMailHomeAddressText: 'SIMULATE.EXPERIA1N@CITI.COM',
    electronicMailHomeSolicitIndicator: 'Y',
    electronicMailHomeStatusIndicator: 'Y',
    electronicMailWorkAddressText: '',
    electronicMailWorkSolicitIndicator: '',
    electronicMailWorkStatusIndicator: ''
  };
  const emailWork = {
    electronicMailHomeAddressText: '',
    electronicMailHomeSolicitIndicator: '',
    electronicMailHomeStatusIndicator: '',
    electronicMailWorkAddressText: 'TEST@APERIA.COM',
    electronicMailWorkSolicitIndicator: 'N',
    electronicMailWorkStatusIndicator: 'N'
  };

  const callApi = async ({
    type = TYPE_EDIT.EMAIL,
    typeEmail = 'emailHome'
  }) => {
    const store = createStore(rootReducer, {
      ...initialState,
      cardholderMaintenance: {
        [storeId]: {
          ...defaultStateCardholder,
          cardholderId: {
            cardNumberValue: 'VOL(M35ETnouVGl6Y0ghb0RzRGo0fA==)',
            cardholderName: 'GUNTUR,KRISHNA P',
            cardholderRole: '01',
            customerExternalIdentifier: 'C02136212149537508764041',
            customerRoleTypeCode: '01',
            customerType: '01',
            id: 'C02136212149537508764041',
            memberSequence: '00001',
            status: '',
            statusCode: ''
          },
          cardholders: [
            {
              cardholderId: 'C02136212149537508764041',
              cardholderInfo: {} as any,
              email:
                typeEmail === 'emailHome'
                  ? ({ ...emailHome } as any)
                  : ({ ...emailWork } as any)
            }
          ]
        }
      }
    });

    return await cardholderMaintenanceActions.triggerUpdateCardHolder({
      ...requestData,
      postData: {
        ...requestData.postData,
        data:
          type === TYPE_EDIT.EMAIL ? requestData.postData.data : postDataPhone
      },
      type
    })(store.dispatch, store.getState, {});
  };

  it('Should Have Call Post Memo Home Email', async () => {
    const postMemo = spyMemoActions('postMemo');
    await callApi({});
    expect(postMemo).toHaveBeenCalledTimes(1);
  });

  it('Should Have Call Post Work Email', async () => {
    const postMemo = spyMemoActions('postMemo');
    await callApi({ typeEmail: 'workEmail' });
    expect(postMemo).toHaveBeenCalledTimes(1);
  });
});

describe('Test case postDataEmail', () => {
  const emailHome = {
    electronicMailHomeAddressText: 'SIMULATE.EXPERIA1N@CITI.COM',
    electronicMailHomeSolicitIndicator: 'Y',
    electronicMailHomeStatusIndicator: 'Y',
    electronicMailWorkAddressText: '',
    electronicMailWorkSolicitIndicator: '',
    electronicMailWorkStatusIndicator: ''
  };

  const dataEmail = {
    electronicMailHomeAddressText: 'SIMULATE.EXPERIA1N@CITI.COM',
    electronicMailHomeSolicitIndicator: 'Y',
    electronicMailHomeStatusIndicator: 'Y',
    electronicMailWorkAddressText: 'TEST@APERIA.COM',
    electronicMailWorkSolicitIndicator: 'N',
    electronicMailWorkStatusIndicator: 'N',
    homeEmailStatusDesc: 'Y - Valid',
    homeSolicitFlagDesc: 'Y - Solicit allowed',
    workEmailStatusDesc: 'N - Invalid',
    workSolicitFlagDesc: 'N - Do not solicit'
  };

  const mockCallApi = async ({
    type = TYPE_EDIT.EMAIL,
    isError = false,
    data = {},
    infoEmail = {}
  }) => {
    const store = createStore(rootReducer, {
      ...initialState,
      cardholderMaintenance: {
        [storeId]: {
          ...defaultStateCardholder,
          cardholderId: {
            cardNumberValue: 'VOL(M35ETnouVGl6Y0ghb0RzRGo0fA==)',
            cardholderName: 'GUNTUR,KRISHNA P',
            cardholderRole: '01',
            customerExternalIdentifier: 'C02136212149537508764041',
            customerRoleTypeCode: '01',
            customerType: '01',
            id: 'C02136212149537508764041',
            memberSequence: '00001',
            status: '',
            statusCode: ''
          },
          cardholders: [
            {
              cardholderId: 'C02136212149537508764041',
              cardholderInfo: {} as any,
              email: { ...infoEmail } as any
            }
          ]
        }
      }
    });

    if (!isError) {
      jest
        .spyOn(apiService, 'updateCardHolder')
        .mockResolvedValue(responseData);
    } else {
      jest
        .spyOn(apiService, 'updateCardHolder')
        .mockRejectedValue(new Error('Async'));
    }

    return await cardholderMaintenanceActions.triggerUpdateCardHolder({
      ...requestData,
      postData: {
        ...requestData.postData,
        data: data
      },
      type
    })(store.dispatch, store.getState, {});
  };

  describe('Test Api Update Email > Case 2', () => {
    it('Should Have Call API Success > empty info Email', async () => {
      const postMemo = spyMemoActions('postMemo');
      const spyToast = toastSpy('addToast');
      await mockCallApi({});

      expect(postMemo).not.toHaveBeenCalled();
      expect(spyToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: 'txt_cardholder_update_email_success'
      });
    });

    it('Should Have Call API Success > with info Email', async () => {
      const postMemo = spyMemoActions('postMemo');
      const spyToast = toastSpy('addToast');
      await mockCallApi({ infoEmail: { ...emailHome } });

      expect(postMemo).toHaveBeenCalled();
      expect(spyToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: 'txt_cardholder_update_email_success'
      });
    });

    it('Should Have Call API Success > empty data', async () => {
      const postMemo = spyMemoActions('postMemo');
      const spyToast = toastSpy('addToast');
      await mockCallApi({});

      expect(postMemo).not.toHaveBeenCalled();
      expect(spyToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: 'txt_cardholder_update_email_success'
      });
    });

    it('Should Have Call API Success > with data', async () => {
      const postMemo = spyMemoActions('postMemo');
      const spyToast = toastSpy('addToast');
      await mockCallApi({ data: dataEmail });

      expect(postMemo).toHaveBeenCalled();
      expect(spyToast).toBeCalledWith({
        show: true,
        type: 'success',
        message: 'txt_cardholder_update_email_success'
      });
    });

    it('Should Have Call API Failed', async () => {
      const spyToast = toastSpy('addToast');
      await mockCallApi({ isError: true });

      expect(spyToast).toBeCalledWith({
        show: true,
        type: 'error',
        message: 'txt_cardholder_update_email_fail'
      });
    });
  });
});
