import { mockActionCreator, responseDefault, storeId } from 'app/test-utils';
import { rootReducer } from 'storeConfig';

import { cardholderMaintenanceActions, defaultStateCardholder } from './index';
import { createStore, Store } from '@reduxjs/toolkit';

//service
import apiService from '../cardholdersMaintenanceService';

const cardholderMaintenanceSpy = mockActionCreator(
  cardholderMaintenanceActions
);

const expansionAddressCode: '1' | '0' = '1';

const responseData = {
  ...responseDefault,
  data: { expansionAddressCode }
};

const initialState: Partial<RootState> = {
  cardholderMaintenance: {
    [storeId]: {
      ...defaultStateCardholder,
      isLoading: true
    }
  }
};

describe('Test Api CheckTypeOfAddress', () => {
  let store: Store<RootState>;
  beforeEach(() => {
    store = createStore(rootReducer, initialState);
  });

  const callApi = async (isError = false) => {
    if (!isError) {
      jest
        .spyOn(apiService, 'checkTypeofAddress')
        .mockResolvedValue(responseData);
    } else {
      jest
        .spyOn(apiService, 'checkTypeofAddress')
        .mockRejectedValue(new Error('Async'));
    }
    return await cardholderMaintenanceActions.checkTypeAddress({
      storeId,
      accEValue: storeId
    })(store.dispatch, store.getState, {});
  };

  it('Should Have Call API Pending', async () => {
    const state = store.getState().cardholderMaintenance[storeId];
    cardholderMaintenanceActions.checkTypeAddress.pending(
      'checkTypeofAddress',
      { storeId, accEValue: storeId }
    );
    expect(state.isLoading).toEqual(true);
  });

  it('Should Have Call API Full Filed', async () => {
    const mockAction = cardholderMaintenanceSpy('getAddressInfo');
    await callApi();
    const state = store.getState().cardholderMaintenance[storeId];
    expect(mockAction).toBeCalledWith({ storeId });
    expect(state.expansionAddressCode).toEqual(expansionAddressCode);
  });

  it('Should Have Call API Rejected', async () => {
    await callApi(true);
    const state = store.getState().cardholderMaintenance[storeId];
    expect(state.isLoading).toEqual(false);
  });
});
