import { mockActionCreator, responseDefault, storeId } from 'app/test-utils';
import { rootReducer } from 'storeConfig';

import { cardholderMaintenanceActions } from './index';
import { createStore, Store } from '@reduxjs/toolkit';
import { ICardholderId, ICardholder, ICardholderReducer } from '../types';

//service
import apiService from '../cardholdersMaintenanceService';
import { CARD_SOLICITATION } from 'app/constants';

const cardholderMaintenanceSpy = mockActionCreator(
  cardholderMaintenanceActions
);

const responseData = {
  ...responseDefault,
  data: {
    customerInquiry: [
      {
        birthDate:
          '{"maskedValue":"********1986","eValue":"VOL(bGRrazslVWx7ZC8tO0k/)"}',
        businessPhoneDeviceCode: 'U',
        businessPhoneLastUpdatedDate: '02-19-2013',
        businessTelephoneFlagCode: 'G',
        clientControlledPhoneDeviceCode: 'W',
        clientControlledPhoneFlagIndicator: 'H',
        clientControlledPhoneIdentifier: '6565656565         ',
        clientControlledPhoneLastUpdatedDate: '04-26-2021',
        creditBureauReportCode: '0',
        customerExternalIdentifier: 'C03098090011800710071012',
        customerName: 'SMITH,ADELE               ',
        customerRoleTypeCode: '01',
        electronicMailHomeAddressText:
          'HOOPSWISHSCORE11@AOL.COM                          ',
        electronicMailHomeSolicitIndicator: 'Y',
        electronicMailHomeStatusIndicator: 'Y',
        electronicMailWorkAddressText:
          '                                                  ',
        electronicMailWorkSolicitIndicator: ' ',
        electronicMailWorkStatusIndicator: ' ',
        facsimileLastUpdatedDate: '04-26-2021',
        facsimilePhoneDeviceCode: 'I',
        facsimilePhoneFlagCode: 'D',
        facsimilePhoneIdentifier: '2323232323         ',
        firstName: 'ADELE                                   ',
        homePhoneDeviceCode: 'U',
        homePhoneLastUpdatedDate: '07-29-2015',
        homePhoneStatusCode: 'S',
        lastName: 'SMITH                                   ',
        memberSequenceIdentifier: '00001',
        middleName: '                                        ',
        mobilePhoneDeviceCode: 'U',
        mobilePhoneFlagCode: 'E',
        mobilePhoneIdentifier: '4235213215         ',
        mobilePhoneLastUpdatedDate: '02-19-2013',
        personalizedEmbossingText: '                          ',
        plasticReplacementIndicator: 'N',
        prefixName: '                    ',
        presentationInstrumentIdentifier:
          '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
        presentationInstrumentReplacementSequenceNumber: '0000',
        presentationInstrumentStatusCode: ' ',
        presentationInstrumentTypeCode: '01',
        primaryCustomerHomePhoneIdentifier: '5063836069         ',
        primaryCustomerSecondPhoneIdentifier: '4236589652         ',
        qualificationName: '                    ',
        salutationCode: 'U',
        socialSecurityIdentifier:
          '{"maskedValue":"***-**-1111","eValue":"VOL(UTVqRzYhcWlVc0laZTpe)"}',
        solicitationCode: ' ',
        suffixName: '                    ',
        titleName: '                    '
      }
    ]
  }
};

const initialState: Partial<RootState> = {
  mapping: {
    loading: false,
    data: {
      getCardholderMaintenance: {
        cardholdersId: {
          id: 'customerExternalIdentifier',
          cardholderName: 'customerName',
          cardholderRole: 'customerRole',
          status: 'externalStatusDesc',
          statusCode: 'externalStatusCode',
          cardNumberValue: 'presentationInstrumentIdentifier.eValue',
          memberSequence: 'memberSequenceIdentifier',
          customerRoleTypeCode: 'customerRoleTypeCode',
          customerType: 'customerRoleTypeCode',
          customerExternalIdentifier: 'customerExternalIdentifier'
        } as unknown as ICardholderId[],
        cardholders: {
          cardholderId: 'customerExternalIdentifier',
          'cardholderInfo.memberSequence': 'memberSequenceIdentifier',
          'cardholderInfo.cardNumber':
            'presentationInstrumentIdentifier.maskedValue',
          'cardholderInfo.lastName': 'lastName',
          'cardholderInfo.firstName': 'firstName',
          'cardholderInfo.middleName': 'middleName',
          'cardholderInfo.cardholderRole': 'customerRoleTypeCode',
          'cardholderInfo.authCBRFlag': 'creditBureauReportCode',
          'cardholderInfo.suffix': 'suffixName',
          'cardholderInfo.prefix': 'prefixName',
          'cardholderInfo.title': 'titleName',
          'cardholderInfo.qualification': 'qualificationName',
          'cardholderInfo.embossingName': 'personalizedEmbossingText',
          'cardholderInfo.salutation': 'salutationCode',
          'cardholderInfo.dateOfBirth': 'birthDate.maskedValue',
          'cardholderInfo.ssnTaxId': 'socialSecurityIdentifier.maskedValue',
          'cardholderInfo.solicitation': 'solicitationCode',
          'email.electronicMailHomeAddressText':
            'electronicMailHomeAddressText',
          'email.electronicMailHomeStatusIndicator':
            'electronicMailHomeStatusIndicator',
          'email.homeEmailStatusDesc': 'electronicMailHomeStatusIndicatorDesc',
          'email.electronicMailHomeSolicitIndicator':
            'electronicMailHomeSolicitIndicator',
          'email.homeSolicitFlagDesc': 'electronicMailHomeSolicitIndicatorDesc',
          'email.electronicMailWorkAddressText':
            'electronicMailWorkAddressText',
          'email.electronicMailWorkStatusIndicator':
            'electronicMailWorkStatusIndicator',
          'email.workEmailStatusDesc': 'electronicMailWorkStatusIndicatorDesc',
          'email.electronicMailWorkSolicitIndicator':
            'electronicMailWorkSolicitIndicator',
          'email.workSolicitFlagDesc': 'electronicMailWorkSolicitIndicatorDesc',
          'telephone.homePhone.less': 'primaryCustomerHomePhoneIdentifier',
          'telephone.homePhone.more.deviceType.value': 'homePhoneDeviceCode',
          'telephone.homePhone.more.deviceType.description':
            'homePhoneDeviceCodeDesc',
          'telephone.homePhone.more.lastUpdated': 'homePhoneLastUpdatedDate',
          'telephone.homePhone.more.flag.value': 'homePhoneStatusCode',
          'telephone.homePhone.more.flag.description':
            'homePhoneStatusCodeDesc',
          'telephone.workPhone.less': 'primaryCustomerSecondPhoneIdentifier',
          'telephone.workPhone.more.deviceType.value':
            'businessPhoneDeviceCode',
          'telephone.workPhone.more.deviceType.description':
            'businessPhoneDeviceCodeDesc',
          'telephone.workPhone.more.lastUpdated':
            'businessPhoneLastUpdatedDate',
          'telephone.workPhone.more.flag.value': 'businessTelephoneFlagCode',
          'telephone.workPhone.more.flag.description':
            'businessTelephoneFlagCodeDesc',
          'telephone.mobilePhone.less': 'mobilePhoneIdentifier',
          'telephone.mobilePhone.more.deviceType.value':
            'mobilePhoneDeviceCode',
          'telephone.mobilePhone.more.deviceType.description':
            'mobilePhoneDeviceCodeDesc',
          'telephone.mobilePhone.more.lastUpdated':
            'mobilePhoneLastUpdatedDate',
          'telephone.mobilePhone.more.flag.value': 'mobilePhoneFlagCode',
          'telephone.mobilePhone.more.flag.description':
            'mobilePhoneFlagCodeDesc',
          'telephone.fax.less': 'facsimilePhoneIdentifier',
          'telephone.fax.more.deviceType.value': 'facsimilePhoneDeviceCode',
          'telephone.fax.more.deviceType.description':
            'facsimilePhoneDeviceCodeDesc',
          'telephone.fax.more.lastUpdated': 'facsimileLastUpdatedDate',
          'telephone.fax.more.flag.value': 'facsimilePhoneFlagCode',
          'telephone.fax.more.flag.description': 'facsimilePhoneFlagCodeDesc',
          'telephone.miscellaneous.less': 'clientControlledPhoneIdentifier',
          'telephone.miscellaneous.more.deviceType.value':
            'clientControlledPhoneDeviceCode',
          'telephone.miscellaneous.more.deviceType.description':
            'clientControlledPhoneDeviceCodeDesc',
          'telephone.miscellaneous.more.lastUpdated':
            'clientControlledPhoneLastUpdatedDate',
          'telephone.miscellaneous.more.flag.value':
            'clientControlledPhoneFlagIndicator',
          'telephone.miscellaneous.more.flag.description':
            'clientControlledPhoneFlagIndicatorDesc'
        } as unknown as ICardholder[]
      }
    }
  }
};

describe('Test Api getCardholderMaintenance', () => {
  let store: Store<RootState>;
  beforeEach(() => {
    store = createStore(rootReducer, initialState);
  });

  const callApi = async ({
    responseData = {} as any,
    ignoreInitialState = false,
    isSetRootLoading = false,
    isCallCheckType = false
  }) => {
    jest
      .spyOn(apiService, 'getCardholderMaintenance')
      .mockResolvedValue(responseData);
    return await cardholderMaintenanceActions.getCardHolderMaintenance({
      storeId,
      accEValue: storeId,
      ignoreInitialState,
      isSetRootLoading,
      isCallCheckType,
      translateFn: (text: string) => text
    })(store.dispatch, store.getState, {});
  };

  it('Should Have Call API', async () => {
    await callApi({ responseData });
    const state = store.getState().cardholderMaintenance[storeId];
    expect(state.cardholderId).not.toBeNull();
    expect(state.cardholdersId.length).toEqual(
      responseData.data.customerInquiry.length
    );
    expect(state.cardholders.length).toEqual(
      responseData.data.customerInquiry.length
    );
  });

  it('Should Have Call API with isSetRootLoading', async () => {
    await callApi({ responseData, isSetRootLoading: true });
    const state = store.getState().cardholderMaintenance[storeId];
    expect(state.cardholderId).not.toBeNull();
    expect(state.cardholdersId.length).toEqual(
      responseData.data.customerInquiry.length
    );
    expect(state.cardholders.length).toEqual(
      responseData.data.customerInquiry.length
    );
    expect(state.isLoading).toEqual(false);
  });

  it('Should Have Call API solicitationCode', async () => {
    const customResponseData = {
      ...responseDefault,
      data: {
        customerInquiry: [
          { ...responseData.data.customerInquiry[0], solicitationCode: 'S' }
        ]
      }
    };
    await callApi({ responseData: customResponseData });
    const state: ICardholderReducer =
      store.getState().cardholderMaintenance[storeId];
    expect(state.cardholders[0].cardholderInfo.solicitation).toEqual(
      CARD_SOLICITATION.S
    );
  });

  it('Should Have Call API with solicitationCode not exist', async () => {
    const checkTypeAddress = cardholderMaintenanceSpy('checkTypeAddress');

    const customResponseData = {
      ...responseDefault,
      data: {
        customerInquiry: [
          {
            ...responseData.data.customerInquiry[0],
            solicitationCode: 'Mock',
            socialSecurityIdentifier: '{}'
          }
        ]
      }
    };
    await callApi({
      responseData: customResponseData,
      ignoreInitialState: false,
      isSetRootLoading: false,
      isCallCheckType: true
    });

    expect(checkTypeAddress).toBeCalledWith({
      storeId,
      accEValue: 'VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)'
    });
  });

  it('Should Have ignoreInitialState', async () => {
    await callApi({ responseData, ignoreInitialState: true });
    const state = store.getState().cardholderMaintenance[storeId];
    expect(state.cardholderId).toBeUndefined();
  });

  it('Should Have Rejected', async () => {
    await callApi({ responseData: new Error('Async') });
    const state = store.getState().cardholderMaintenance[storeId];
    expect(state.isLoading).toEqual(false);
  });

  it('should have pending', () => {
    callApi({ responseData });
    cardholderMaintenanceActions.getCardHolderMaintenance.pending(
      'cardholderMaintenance/getCardholderMaintenance',
      { storeId, accEValue: storeId, translateFn: (text: string) => text }
    );
    const state = store.getState().cardholderMaintenance[storeId];
    expect(state.isLoading).toEqual(true);
  });

  it('should have pending IgnoreState', () => {
    callApi({ responseData, ignoreInitialState: true });
    cardholderMaintenanceActions.getCardHolderMaintenance.pending(
      'cardholderMaintenance/getCardholderMaintenance',
      { storeId, accEValue: storeId, translateFn: (text: string) => text }
    );
    const state = store.getState().cardholderMaintenance[storeId];
    expect(state.cardholders).toBeUndefined();
  });
});
