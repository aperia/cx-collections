import { batch } from 'react-redux';
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import {
  AddStandardAddressRequest,
  IInitStateCardholder,
  IPostStandardAddressArgs
} from '../types';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// service
import apiService from '../cardholdersMaintenanceService';
import { cardholderMaintenanceActions } from '.';
import { CARDHOLDER_MAINTENANCE_ACTIONS } from 'pages/Memos/constants';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { handleGenerateValuesForMemo } from '../helpers';
import isEmpty from 'lodash.isempty';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { CARD_ROLE } from 'app/constants';
import { TYPE_ADDRESS } from '../ExpandedAddress/constants';
import { CHRONICLE_CARD_HOLDER_MAINTENANCE_KEY } from 'pages/ClientConfiguration/Memos/constants';

export const triggerEditStandardAddress = createAsyncThunk<
  void,
  IPostStandardAddressArgs,
  ThunkAPIConfig
>('cardholderMaintenance/editStandardAddress', async (args, thunkAPI) => {
  const { dispatch, rejectWithValue } = thunkAPI;
  const { storeId } = args;
  const { cardholderMaintenance, accountDetail } = thunkAPI.getState();
  try {
    const {
      accEValue,
      addressLineOne,
      addressLineTwo,
      city,
      stateSubdivision,
      postalCode
    } = args;

    let requestData: AddStandardAddressRequest = {
      common: { accountId: accEValue },
      city,
      addressLineOne,
      addressLineTwo,
      state: stateSubdivision?.fieldId,
      zipCode: postalCode
    };

    if (addressLineTwo) {
      requestData = {
        ...requestData,
        letter: 'Y',
        propagate: 'Y',
        bypass: 'B'
      };
    }

    await apiService.editStandardAddress(requestData);
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: 'txt_standard_address_updated'
        })
      );
      dispatch(
        cardholderMaintenanceActions.getAddressInfo({
          storeId,
          isSetRootLoading: true
        })
      );
      // API Error Detail
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inModalBody'
        })
      );

      // POST memo
      const contentEdit = handleGenerateValuesForMemo(
        cardholderMaintenance[storeId]?.addressType,
        args
      );
      !isEmpty(contentEdit) &&
        dispatch(
          memoActions.postMemo(
            CARDHOLDER_MAINTENANCE_ACTIONS.EDIT_STANDARD_ADDRESS,
            {
              storeId,
              accEValue,
              chronicleKey: CHRONICLE_CARD_HOLDER_MAINTENANCE_KEY,
              cardholderMaintenance: {
                cardholderContactedName:
                  // primary
                  accountDetail[storeId]?.selectedAuthenticationData
                    ?.customerNameForCreateWorkflow,
                cardholderContactedNameAuth:
                  accountDetail[storeId]?.selectedAuthenticationData
                    ?.customerName,
                role: CARD_ROLE[TYPE_ADDRESS.PRIMARY],
                contentEdit: contentEdit
              }
            }
          )
        );
    });
  } catch (error) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: 'txt_standard_address_updated_failed'
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inModalBody',
          apiResponse: error
        })
      );
    });
    return rejectWithValue(error);
  }
});

export const editStandardAddressBuilder = (
  builder: ActionReducerMapBuilder<IInitStateCardholder>
) => {
  builder
    .addCase(triggerEditStandardAddress.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].standardAddress.isLoadingEditAddress = true;
    })
    .addCase(triggerEditStandardAddress.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].standardAddress.isLoadingEditAddress = false;
      draftState[storeId].standardAddress.isOpenModalEdit = false;
    })
    .addCase(triggerEditStandardAddress.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].standardAddress.isLoadingEditAddress = false;
    });
};
