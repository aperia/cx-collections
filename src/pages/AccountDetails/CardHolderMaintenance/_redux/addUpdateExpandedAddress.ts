import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { batch } from 'react-redux';
import {
  MODAL_ADDRESS_STANDARD,
  VIEW_NAME_ADDRESS
} from '../ExpandedAddress/constants';
import apiService from '../cardholdersMaintenanceService';
import {
  CardholderAddress,
  IAddressInfo,
  IInitStateCardholder,
  IRequestDataExpandedAddress
} from '../types';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import {
  formatTimeDefault,
  getPropertyDifference,
  mappingDataFromObj
} from 'app/helpers';
import { FormatTime } from 'app/constants/enums';
import { addAndEditExpandedAddress } from './mapping.json';
import { cardholderMaintenanceActions } from '.';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { CARDHOLDER_MAINTENANCE_ACTIONS } from 'pages/Memos/constants';
import { selectCardholderAddressInfo } from './selector';
import {
  generateMemoAddressValue,
  generateMemoEditAddressValue
} from '../helpers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { mapDataToView } from '../ExpandedAddress/AddAndUpdate/helper';
import { CARD_ROLE } from 'app/constants';
import { CHRONICLE_CARD_HOLDER_MAINTENANCE_KEY } from 'pages/ClientConfiguration/Memos/constants';

export const addUpdateExpandedAddress = createAsyncThunk<
  MagicKeyValue,
  ArgsCommon,
  ThunkAPIConfig
>('cardholderMaintenance/addUpdateExpandedAddress', async (args, thunkAPI) => {
  const { EDIT } = MODAL_ADDRESS_STANDARD;
  const { form, cardholderMaintenance, accountDetail } = thunkAPI.getState();
  const { dispatch } = thunkAPI;
  const { storeId, postData } = args;

  const cardholder = cardholderMaintenance[storeId].cardholderId;
  const openModal = cardholderMaintenance[storeId].expandedAddress.openModal;
  const data = form[`${storeId}-${VIEW_NAME_ADDRESS.ADD_ADDRESS}`]
    ?.values as IAddressInfo;

  const effectiveFrom = formatTimeDefault(
    data?.effectiveFrom,
    FormatTime.DefaultPostDate
  );
  const effectiveTo = formatTimeDefault(
    data?.effectiveTo,
    FormatTime.DefaultPostDate
  );

  const effectiveFromIdAddress = formatTimeDefault(
    data?.effectiveFrom,
    FormatTime.YearMonthDayWithHyphen
  );
  const effectiveToIdAddress = formatTimeDefault(
    data?.effectiveTo,
    FormatTime.YearMonthDayWithHyphen
  );

  const isUpdate = openModal === EDIT ? true : false;
  const idAddress = `${cardholder.memberSequence}-${data.addressCategory.fieldID}-${data.addressType.fieldID}-${effectiveFromIdAddress}-${effectiveToIdAddress}`;

  const mappingData = mappingDataFromObj<CardholderAddress>(
    {
      ...data,
      customerRoleTypeCode: cardholder.customerRoleTypeCode,
      customerType: cardholder.customerRoleTypeCode,
      memberSequence: cardholder.memberSequence,
      formattedAddressCode: 'F',
      effectiveFrom,
      effectiveTo
    },
    addAndEditExpandedAddress
  );

  let response = null;
  const requestData: IRequestDataExpandedAddress = {
    common: {
      accountId: cardholder.cardNumberValue,
      externalCustomerId: cardholder.customerExternalIdentifier
    },
    ...mappingData,
    houseBuildingName: mappingData.houseBuildingName
      ? mappingData.houseBuildingName
      : ' ',
    addressContinuationLine3Text: mappingData.addressContinuationLine3Text
      ? mappingData.addressContinuationLine3Text
      : ' ',
    addressContinuationLine2Text: mappingData.addressContinuationLine2Text
      ? mappingData.addressContinuationLine2Text
      : ' ',
    addressContinuationLine4Text: mappingData.addressContinuationLine4Text
      ? mappingData.addressContinuationLine4Text
      : ' ',
    addressAttentionLineText: mappingData.addressAttentionLineText
      ? mappingData.addressAttentionLineText
      : ' ',
    streetName: mappingData.streetName ? mappingData.streetName : ' ',
    postOfficeBoxIdentifier: mappingData.postOfficeBoxIdentifier
      ? mappingData.postOfficeBoxIdentifier
      : ' ',
    houseIdentifier: mappingData.houseIdentifier
      ? mappingData.houseIdentifier
      : ' '
  };

  try {
    response = await apiService.addAndUpdateExpandedAddress(requestData);
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: !isUpdate
            ? 'txt_expanded_address_submitted'
            : 'txt_expanded_address_updated'
        })
      );
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inModalBody'
        })
      );
    });

    const initAddressViewData = selectCardholderAddressInfo(
      thunkAPI.getState(),
      storeId
    );

    const memoValue = generateMemoAddressValue(data);
    const contentEdit = generateMemoEditAddressValue(
      getPropertyDifference(mapDataToView(initAddressViewData!), data)
    );

    dispatch(
      memoActions.postMemo(
        isUpdate
          ? CARDHOLDER_MAINTENANCE_ACTIONS.EDIT_EXPANDED_ADDRESS
          : CARDHOLDER_MAINTENANCE_ACTIONS.ADD_EXPANDED_ADDRESS,
        {
          storeId,
          accEValue: postData.accountId,
          chronicleKey: CHRONICLE_CARD_HOLDER_MAINTENANCE_KEY,
          cardholderMaintenance: {
            cardholderContactedName:
              cardholderMaintenance[storeId].cardholderId?.cardholderName,
            cardholderContactedNameAuth:
              accountDetail[storeId]?.selectedAuthenticationData?.customerName,
            role: CARD_ROLE[
              cardholderMaintenance[storeId]?.cardholderId?.cardholderRole
            ],
            addValue: memoValue,
            contentEdit: contentEdit
          }
        }
      )
    );
    dispatch(
      cardholderMaintenanceActions.getAddressInfo({
        storeId,
        idAddress,
        isSetRootLoading: true
      })
    );
    return response;
  } catch (error) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: !isUpdate
            ? 'txt_expanded_address_submitted_failed'
            : 'txt_expanded_address_updated_failed'
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inModalBody',
          apiResponse: error
        })
      );
    });
    return thunkAPI.rejectWithValue(error);
  }
});

export const addUpdateExpandedAddressBuilder = (
  builder: ActionReducerMapBuilder<IInitStateCardholder>
) => {
  builder
    .addCase(addUpdateExpandedAddress.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].expandedAddress.isLoading = true;
    })
    .addCase(addUpdateExpandedAddress.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].expandedAddress.isLoading = false;
      draftState[storeId].expandedAddress.openModal = '';
    })
    .addCase(addUpdateExpandedAddress.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].expandedAddress.isLoading = false;
    });
};
