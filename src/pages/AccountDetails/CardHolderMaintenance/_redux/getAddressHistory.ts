import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import {
  IAddressInfo,
  IArgGetExpandedAddressHistory,
  IInitStateCardholder,
  IPayLoadGetExpandedAddressHistory
} from '../types';
import { getExpandedAddress } from './mapping.json';

// service
import apiService from '../cardholdersMaintenanceService';
import { FIELD_HISTORY_EXPANDED_ADDRESS_INFO } from '../ExpandedAddress/constants';
import { mappingDataFromArray } from 'app/helpers';
import { mappingRefData } from 'pages/AccountDetails/helpers';

export const getAddressHistory = createAsyncThunk<
  IPayLoadGetExpandedAddressHistory,
  IArgGetExpandedAddressHistory,
  ThunkAPIConfig
>('cardholderMaintenance/getExpandedAddressHistory', async (args, thunkAPI) => {
  const { accEValue, startSequence, endSequence, storeId } = args;
  const requestData = {
    common: { accountId: accEValue },
    startSequence,
    endSequence,
    selectFields: FIELD_HISTORY_EXPANDED_ADDRESS_INFO
  };
  const { cardholderId } = thunkAPI?.getState()?.cardholderMaintenance[storeId];
  try {
    const { data } = await apiService.getExpandedAddressHistory(requestData);

    const mappingAddress: IAddressInfo[] = mappingDataFromArray(
      data?.addressHistory,
      getExpandedAddress
    );

    const filterHistory = mappingAddress.filter(
      (item: IAddressInfo) =>
        item?.memberSequence === cardholderId.memberSequence
    );

    let historyAddress: IAddressInfo[] = [];

    for (const index in filterHistory) {
      historyAddress = [
        ...historyAddress,
        {
          ...(await mappingRefData(filterHistory[index], [
            'countryISO',
            'stateSubdivision',
            'addressCategory',
            'addressType',
            'addressRelationship'
          ]))
        }
      ];
    }

    return {
      expandedAddressHistory: historyAddress
    };
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const getAddressHistoryBuilder = (
  builder: ActionReducerMapBuilder<IInitStateCardholder>
) => {
  builder
    .addCase(getAddressHistory.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].expandedAddress.isLoading = true;
    })
    .addCase(getAddressHistory.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { expandedAddressHistory } = action.payload;
      draftState[storeId].expandedAddress.histories = expandedAddressHistory;
      draftState[storeId].expandedAddress.isLoading = false;
    })
    .addCase(getAddressHistory.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].expandedAddress.isLoading = false;
      draftState[storeId].expandedAddress.isEndLoadMore = true;
    });
};
