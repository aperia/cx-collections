import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { batch } from 'react-redux';
// actions
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { postMemoForPhone } from './postMemoForPhone';

// types
import { IInitStateCardholder, UpdateCardHolderArg } from '../types';

// service
import apiService from '../cardholdersMaintenanceService';

// constant
import { TYPE_EDIT } from '../TelePhone/constants';
import { selectEmailInfo } from './selector';
import { isEqual, pick } from 'lodash';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { CARDHOLDER_MAINTENANCE_ACTIONS } from 'pages/Memos/constants';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { CARD_ROLE } from 'app/constants';
import { getPropertyDifference } from 'app/helpers';
import { CHRONICLE_CARD_HOLDER_MAINTENANCE_KEY } from 'pages/ClientConfiguration/Memos/constants';

export const triggerUpdateCardHolder = createAsyncThunk<
  void,
  UpdateCardHolderArg,
  ThunkAPIConfig
>('cardholderMaintenance/updateCardholder', async (args, thunkAPI) => {
  const { type, storeId, accEValue } = args;
  const state = thunkAPI.getState();
  const { dispatch, rejectWithValue } = thunkAPI;
  const { accountDetail, cardholderMaintenance } = state;

  const success =
    type === TYPE_EDIT.EMAIL
      ? 'txt_cardholder_update_email_success'
      : 'txt_cardholder_update_phone_success';

  const fail =
    type === TYPE_EDIT.EMAIL
      ? 'txt_cardholder_update_email_fail'
      : 'txt_cardholder_update_phone_fail';

  try {
    const { postData } = args;

    await apiService.updateCardHolder(postData);

    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: success
        })
      );
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          storeId,
          forSection: 'inModalBody'
        })
      );
    });

    // auto memo for email
    if (type === TYPE_EDIT.EMAIL) {
      const originalEmail = selectEmailInfo(state, storeId);
      const homeEmailField = [
        'electronicMailHomeAddressText',
        'electronicMailHomeSolicitIndicator',
        'electronicMailHomeStatusIndicator'
      ];
      const workEmailField = [
        'electronicMailWorkAddressText',
        'electronicMailWorkSolicitIndicator',
        'electronicMailWorkStatusIndicator'
      ];
      const originalHomeEmail = pick(originalEmail, homeEmailField);
      const newHomeEmail = pick(postData.data, homeEmailField);
      const originalWorkEmail = pick(originalEmail, workEmailField);
      const newWorkEmail = pick(postData.data, workEmailField);
      const isHomeEmailChange = !isEqual(originalHomeEmail, newHomeEmail);
      const isWorkEmailChange = !isEqual(originalWorkEmail, newWorkEmail);
      const statusDict = {
        N: 'N - Invalid',
        Y: 'Y - Valid'
      } as never;
      const solicitDict = {
        N: 'N - Do not solicit',
        Y: 'Y - Solicit allowed'
      } as never;

      const postMemo = (contentEdit: string) => {
        dispatch(
          memoActions.postMemo(
            CARDHOLDER_MAINTENANCE_ACTIONS.UPDATE_EMAIL_INFORMATION,
            {
              storeId,
              accEValue,
              chronicleKey: CHRONICLE_CARD_HOLDER_MAINTENANCE_KEY,
              cardholderMaintenance: {
                cardholderContactedName:
                  cardholderMaintenance[storeId]?.cardholderId?.cardholderName,
                cardholderContactedNameAuth:
                  accountDetail[storeId]?.selectedAuthenticationData
                    ?.customerName,
                role: CARD_ROLE[
                  cardholderMaintenance[storeId]?.cardholderId.cardholderRole
                ],
                contentEdit
              }
            }
          )
        );
      };
      if (isHomeEmailChange) {
        const generateHomeValue = (emailData: MagicKeyValue) => {
          const result = [];

          if (emailData?.electronicMailHomeAddressText) {
            const { old: oldValue, new: newValue } =
              emailData?.electronicMailHomeAddressText;
            result.push(`HOME EMAIL CHANGED FROM ${oldValue} to ${newValue}`);
          }
          if (emailData?.electronicMailHomeStatusIndicator) {
            const { old: oldValue, new: newValue } =
              emailData?.electronicMailHomeStatusIndicator;
            result.push(
              `HOME EMAIL STATUS CHANGED FROM ${
                statusDict[oldValue] || ''
              } to ${statusDict[newValue] || ''}`
            );
          }
          if (emailData?.electronicMailHomeSolicitIndicator) {
            const { old: oldValue, new: newValue } =
              emailData?.electronicMailHomeSolicitIndicator;
            result.push(
              `HOME EMAIL SOLICIT FLAG CHANGED FROM ${
                solicitDict[oldValue] || ''
              } to ${solicitDict[newValue] || ''}`
            );
          }
          return result.join(', ');
        };
        const contentEdit = generateHomeValue(
          getPropertyDifference(originalHomeEmail, newHomeEmail)
        );

        postMemo(contentEdit);
      }
      if (isWorkEmailChange) {
        const generateWorkValue = (emailData: MagicKeyValue) => {
          const result = [];

          if (emailData.electronicMailWorkAddressText) {
            const { old: oldValue, new: newValue } =
              emailData.electronicMailWorkAddressText;
            result.push(`WORK EMAIL CHANGED FROM ${oldValue} to ${newValue}`);
          }
          if (emailData.electronicMailWorkStatusIndicator) {
            const { old: oldValue, new: newValue } =
              emailData.electronicMailWorkStatusIndicator;
            result.push(
              `WORK EMAIL STATUS CHANGED FROM ${
                statusDict[oldValue] || ''
              } to ${statusDict[newValue] || ''}`
            );
          }
          if (emailData.electronicMailWorkSolicitIndicator) {
            const { old: oldValue, new: newValue } =
              emailData.electronicMailWorkSolicitIndicator;
            result.push(
              `WORK EMAIL SOLICIT FLAG CHANGED FROM ${
                solicitDict[oldValue] || ''
              } to ${solicitDict[newValue] || ''}`
            );
          }
          return result.join(', ');
        };
        const contentEdit = generateWorkValue(
          getPropertyDifference(originalWorkEmail, newWorkEmail)
        );

        postMemo(contentEdit);
      }
    }
    if (type === TYPE_EDIT.PHONE) {
      dispatch(postMemoForPhone(args, storeId));
    }
  } catch (error) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: fail
        })
      );
      dispatch(
        apiErrorNotificationAction.updateApiError({
          storeId,
          forSection: 'inModalBody',
          apiResponse: error
        })
      );
    });
    return rejectWithValue(error);
  }
});

export const updateCardHolderBuilder = (
  builder: ActionReducerMapBuilder<IInitStateCardholder>
) => {
  builder
    .addCase(triggerUpdateCardHolder.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loadingEdit: true
      };
    })
    .addCase(triggerUpdateCardHolder.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        reload: true,
        isOpenModalPhoneEmail: false,
        loadingEdit: false
      };
    })
    .addCase(triggerUpdateCardHolder.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loadingEdit: false
      };
    });
};
