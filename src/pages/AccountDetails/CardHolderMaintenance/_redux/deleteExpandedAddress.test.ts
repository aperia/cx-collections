import {
  mockActionCreator,
  responseDefault,
  storeId,
  accEValue
} from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import { cardholderMaintenanceActions, defaultStateCardholder } from './index';
import { createStore, Store } from '@reduxjs/toolkit';
import apiService from '../cardholdersMaintenanceService';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { I18N_CARDHOLDER_MAINTENANCE } from '../constants';
import { memoActions } from 'pages/Memos/_redux/reducers';

const spyMemoActions = mockActionCreator(memoActions);
const toastSpy = mockActionCreator(actionsToast);
const cardholderMaintenanceSpy = mockActionCreator(
  cardholderMaintenanceActions
);

const responseData = {
  ...responseDefault,
  data: 'Success'
};

const initialState: Partial<RootState> = {
  accountDetail: {
    [storeId]: {
      data: {
        primaryName: 'primaryName'
      }
    }
  },
  cardholderMaintenance: {
    [storeId]: {
      ...defaultStateCardholder,
      expandedAddress: {
        histories: [],
        openModal: 'DELETE',
        isLoading: false,
        isEndLoadMore: false
      },
      addressType: {
        POBox: '',
        addressCategory: {
          description: 'P - Permanent',
          fieldID: 'P',
          fieldValue: 'Permanent'
        },
        addressLineFour: '',
        addressLineOne: '111 NOEL ST',
        addressLineThree: '',
        addressLineTwo: '',
        addressRelationship: {
          fieldValue: 'Other',
          fieldID: 'O',
          description: 'O - Other'
        },
        addressType: {
          fieldValue: 'Billing',
          fieldID: 'BLL1',
          description: 'BLL1 - Billing'
        },
        attention: 'AAA',
        cityName: 'SSSSS',
        countryISO: {
          fieldValue: 'Canada',
          fieldID: 'CAN',
          description: 'CAN - Canada'
        },
        effectiveFrom: '2021-04-28',
        effectiveTo: '2021-05-24',
        formattedAddressCode: 'F',
        houseBuilding: '',
        houseNumber: '',
        id: '00001-P-BLL1-2021-04-28-2021-05-24',
        memberSequence: '00001',
        postalCode: 'T7S 0K7',
        stateSubdivision: {
          fieldValue: 'Alberta',
          fieldID: 'AB',
          description: 'AB - Alberta'
        },
        streetName: ''
      },
      cardholderId: {
        cardNumberValue: 'VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)',
        cardholderName: 'ENDY2,HARRYY T',
        cardholderRole: '01',
        customerExternalIdentifier: 'C20273234913858500085018',
        customerRoleTypeCode: '01',
        customerType: '01',
        id: 'C20273234913858500085018',
        memberSequence: '1',
        status: '',
        statusCode: ''
      },
      isLoading: true
    }
  }
};

describe('Test Api DeleteExpandedAddress', () => {
  let store: Store<RootState>;
  beforeEach(() => {
    store = createStore(rootReducer, initialState);
  });

  const callApi = async (isError = false) => {
    if (!isError) {
      jest
        .spyOn(apiService, 'deleteAddressExpandedAddress')
        .mockResolvedValue(responseData);
    } else {
      jest
        .spyOn(apiService, 'deleteAddressExpandedAddress')
        .mockRejectedValue(new Error('Async'));
    }
    return await cardholderMaintenanceActions.triggerDeleteExpandedAddress({
      storeId,
      accEValue
    })(store.dispatch, store.getState, {});
  };

  it('Should Have Call API Pending', async () => {
    const pendingAction =
      cardholderMaintenanceActions.triggerDeleteExpandedAddress.pending(
        'cardholderMaintenance/triggerDeleteExpandedAddress',
        { storeId, accEValue }
      );
    const actual = rootReducer(store.getState(), pendingAction)
      .cardholderMaintenance[storeId].expandedAddress;

    expect(actual.isLoading).toEqual(true);
    expect(actual.openModal).toEqual('DELETE');
  });

  it('Should Have Call', async () => {
    const showToast = toastSpy('addToast');
    const getAddressInfo = cardholderMaintenanceSpy('getAddressInfo');
    const postMemo = spyMemoActions('postMemo');
    await callApi();

    const state =
      store.getState().cardholderMaintenance[storeId].expandedAddress;

    expect(postMemo).toHaveBeenCalled();
    expect(getAddressInfo).toBeCalledWith({ storeId });
    expect(showToast).toBeCalledWith({
      message: I18N_CARDHOLDER_MAINTENANCE.ADDRESS_DELETED,
      show: true,
      type: 'success'
    });
    expect(state.isLoading).toEqual(false);
    expect(state.openModal).toEqual('');
  });

  it('Should Have Call API Rejected', async () => {
    const showToast = toastSpy('addToast');

    await callApi(true);

    const state =
      store.getState().cardholderMaintenance[storeId].expandedAddress.isLoading;

    expect(showToast).toBeCalledWith({
      message: I18N_CARDHOLDER_MAINTENANCE.ADDRESS_FAILED_TO_DELETE,
      show: true,
      type: 'error'
    });
    expect(state).toEqual(false);
  });
});
