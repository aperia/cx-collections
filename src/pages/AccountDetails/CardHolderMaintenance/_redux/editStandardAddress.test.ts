import { mockActionCreator, responseDefault, storeId } from 'app/test-utils';
import { rootReducer } from 'storeConfig';

import { cardholderMaintenanceActions, defaultStateCardholder } from './index';
import { createStore, Store } from '@reduxjs/toolkit';

//service
import apiService from '../cardholdersMaintenanceService';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { memoActions } from 'pages/Memos/_redux/reducers';

const spyMemoActions = mockActionCreator(memoActions);

const cardholderMaintenanceSpy = mockActionCreator(
  cardholderMaintenanceActions
);

const toastSpy = mockActionCreator(actionsToast);

const argsRequest = {
  storeId,
  accEValue: 'accEValue',
  addressLineOne: 'addressLineOne',
  addressLineTwo: 'addressLineTwo',
  city: 'city',
  postalCode: 'postalCode',
  memberSequence: 'memberSequence',
  customerRoleTypeCode: 'customerRoleTypeCode',
  customerType: 'customerType',
  cardholderId: 'cardholderId',
  stateSubdivision: {
    fieldId: 'fieldId',
    fieldValue: 'fieldValue',
    description: 'description'
  }
};

const responseData = {
  ...responseDefault,
  data: 'Success'
};

const initialState: Partial<RootState> = {
  cardholderMaintenance: {
    [storeId]: {
      ...defaultStateCardholder,
      isLoading: true
    }
  }
};

describe('Test Api EditStandardAddress', () => {
  let store: Store<RootState>;
  beforeEach(() => {
    store = createStore(rootReducer, initialState);
  });

  const callApi = async (isError = false, customArgs?: Record<string, any>) => {
    if (!isError) {
      jest
        .spyOn(apiService, 'editStandardAddress')
        .mockResolvedValue(responseData);
    } else {
      jest
        .spyOn(apiService, 'editStandardAddress')
        .mockRejectedValue(new Error('Async'));
    }
    return await cardholderMaintenanceActions.triggerEditStandardAddress({
      ...argsRequest,
      ...customArgs
    })(store.dispatch, store.getState, {});
  };

  it('Should Have Call API Pending', async () => {
    const pendingAction =
      cardholderMaintenanceActions.triggerEditStandardAddress.pending(
        'cardholderMaintenance/editStandardAddress',
        { ...argsRequest }
      );
    const actual = rootReducer(store.getState(), pendingAction);

    expect(
      actual.cardholderMaintenance[storeId].standardAddress.isLoadingEditAddress
    ).toBe(true);
  });

  it('Should Have Call API', async () => {
    const showToast = toastSpy('addToast');
    const getAddressInfo = cardholderMaintenanceSpy('getAddressInfo');
    const postMemo = spyMemoActions('postMemo');
    const state =
      store.getState().cardholderMaintenance[storeId].standardAddress
        .isLoadingEditAddress;
    await callApi();
    expect(postMemo).toHaveBeenCalled();
    expect(getAddressInfo).toBeCalledWith({ storeId, isSetRootLoading: true });
    expect(showToast).toBeCalledWith({
      message: 'txt_standard_address_updated',
      show: true,
      type: 'success'
    });
    expect(state).toEqual(false);
  });

  it('Should Have Call API with addressLineTwo = undefined', async () => {
    const state =
      store.getState().cardholderMaintenance[storeId].standardAddress
        .isLoadingAddAddress;
    await callApi(false, {
      addressLineTwo: undefined
    });
    expect(state).toEqual(false);
  });

  it('Should Have Call API Pending', async () => {
    const pendingAction =
      cardholderMaintenanceActions.triggerEditStandardAddress.fulfilled(
        undefined,
        'cardholderMaintenance/editStandardAddress',
        { ...argsRequest }
      );
    const actual = rootReducer(store.getState(), pendingAction);
    const standardAddress =
      actual.cardholderMaintenance[storeId].standardAddress;

    expect(standardAddress.isLoadingEditAddress).toBe(false);
    expect(standardAddress.isOpenModalEdit).toBe(false);
  });

  it('Should Have Call API Rejected', async () => {
    const showToast = toastSpy('addToast');
    const state =
      store.getState().cardholderMaintenance[storeId].standardAddress
        .isLoadingEditAddress;
    await callApi(true);
    expect(showToast).toBeCalledWith({
      message: 'txt_standard_address_updated_failed',
      show: true,
      type: 'error'
    });
    expect(state).toEqual(false);
  });
});
