import React, { useEffect, useLayoutEffect, useMemo, useRef } from 'react';
import { batch, useDispatch } from 'react-redux';

// components
import { SimpleBar } from 'app/_libraries/_dls/components';
import CardholderInfo from './CardholderInfo';
import CardholderDropdown from './CardholderDropdown';
import TelePhone from './TelePhone';
import Email from './Email';
import ModalEdit from './ModalEdit';
import StandardAddress from './StandardAddress';
import ExpandedAddress from './ExpandedAddress';

// helpers
import classNames from 'classnames';

// redux
import { cardholderMaintenanceActions } from 'pages/AccountDetails/CardHolderMaintenance/_redux';
import {
  selectRootLoading,
  selectReload,
  selectModalEdit,
  selectIsExpandedAddress
} from 'pages/AccountDetails/CardHolderMaintenance/_redux/selector';

// Hook
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useApiErrorNotification } from 'pages/ApiErrorNotification/hooks/useApiErrorNotification';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface CardHolderMaintenanceProps {}

const CardHolderMaintenance: React.FC<CardHolderMaintenanceProps> = ({}) => {
  const { storeId, accEValue } = useAccountDetail();
  const dispatch = useDispatch();
  const divRef = useRef<HTMLDivElement | null>(null);
  const { t } = useTranslation();

  const reload = useStoreIdSelector<boolean>(selectReload);
  const show = useStoreIdSelector<boolean>(selectModalEdit);
  const isExpandedAddress = useStoreIdSelector<boolean>(
    selectIsExpandedAddress
  );
  const isLoading = useStoreIdSelector(selectRootLoading);

  const ApiErrorDetail = useApiErrorNotification({
    storeId,
    forSection: 'inAddressCardholderFlyOut'
  });

  useEffect(() => {
    dispatch(
      cardholderMaintenanceActions.getCardHolderMaintenance({
        storeId,
        accEValue,
        isCallCheckType: true,
        translateFn: t
      })
    );
  }, [dispatch, storeId, accEValue, t]);

  // reload
  useEffect(() => {
    if (!reload) return;

    batch(() => {
      dispatch(
        cardholderMaintenanceActions.getCardHolderMaintenance({
          accEValue,
          storeId,
          isSetRootLoading: true,
          ignoreInitialState: true,
          translateFn: t
        })
      );
      dispatch(
        cardholderMaintenanceActions.updateReload({
          storeId,
          reload: false
        })
      );
    });
  }, [dispatch, accEValue, reload, storeId, t]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = 'calc(100% - 130px)');
  }, []);

  const AddressView = useMemo(() => {
    if (
      !checkPermission(PERMISSIONS.CARDHOLDER_MAINTENANCE_VIEW_ADDRESS, storeId)
    )
      return null;

    return (
      <>
        <hr />
        {isExpandedAddress ? <ExpandedAddress /> : <StandardAddress />}
      </>
    );
  }, [isExpandedAddress, storeId]);

  return (
    <div className={classNames('h-100', { loading: isLoading })}>
      <CardholderDropdown />
      <div ref={divRef}>
        <SimpleBar>
          <div className="p-24 overlap-next-action">
            <ApiErrorDetail
              className="mb-24"
              dataTestId="infobar_cardholder_cardholder-info_api-error"
            />
            <CardholderInfo />
            {AddressView}
            {checkPermission(
              PERMISSIONS.CARDHOLDER_MAINTENANCE_VIEW_PHONE,
              storeId
            ) && (
              <>
                <hr />
                <TelePhone />
              </>
            )}
            {checkPermission(
              PERMISSIONS.CARDHOLDER_MAINTENANCE_VIEW_EMAIL,
              storeId
            ) && (
              <>
                <hr />
                <Email />
              </>
            )}
          </div>
        </SimpleBar>
        {show && <ModalEdit />}
      </div>
    </div>
  );
};

export default CardHolderMaintenance;
