import axios from 'axios';
import { apiService } from 'app/utils/api.service';
import {
  IDataRequest,
  PostDataUpdate,
  APIGetCardHolderPayload,
  AddStandardAddressRequest,
  IRequestDeleteExpandedAddress
} from './types';
import {
  IRequestDataExpandedAddress,
  GetStandardAddressHistoryRequest
} from './types';

const cardholdersMaintenanceService = {
  checkTypeofAddress: (data: MagicKeyValue) => {
    const url =
      window.appConfig?.api?.cardholdersMaintenance?.checkTypeAddress || '';
    return apiService.post<MagicKeyValue>(url, data);
  },

  getCardholderMaintenance: (data: IDataRequest) => {
    const url =
      window.appConfig?.api?.cardholdersMaintenance?.getCardholderMaintenance ||
      '';
    return apiService.post<APIGetCardHolderPayload>(url, data);
  },

  updateCardHolder(data: PostDataUpdate) {
    const url = window.appConfig?.api?.cardholdersMaintenance?.update || '';
    return apiService.post(url, { common: data?.common, ...data?.data });
  },

  addStandardAddress: (data: AddStandardAddressRequest) => {
    const url =
      window.appConfig?.api?.cardholdersMaintenance.addStandardAddress || '';
    return apiService.post<string>(url, data);
  },

  editStandardAddress: (data: AddStandardAddressRequest) => {
    const url =
      window.appConfig?.api?.cardholdersMaintenance.editStandardAddress || '';
    return apiService.post<string>(url, data);
  },

  addAndUpdateExpandedAddress(data: IRequestDataExpandedAddress) {
    const url =
      window.appConfig?.api?.cardholdersMaintenance
        ?.addAndUpdateExpandedAddress || '';
    return apiService.post(url, data);
  },

  deleteAddressExpandedAddress(data: IRequestDeleteExpandedAddress) {
    const url =
      window.appConfig?.api?.cardholdersMaintenance
        ?.deleteAddressExpandedAddress || '';
    return apiService.post(url, data);
  },

  getExpandedAddressHistory: (data: any) => {
    const url =
      window.appConfig?.api?.cardholdersMaintenance
        ?.getExpandedAddressHistory || '';
    return apiService.post<any>(url, data);
  },

  getAddressInfo: (data: any) => {
    const url =
      window.appConfig?.api?.cardholdersMaintenance?.getAddressInfo || '';
    return apiService.post<any>(url, data);
  },

  getStandardAddressHistory: (data: GetStandardAddressHistoryRequest) => {
    const url =
      window.appConfig?.api?.cardholdersMaintenance.getStandardAddressHistory ||
      '';
    return apiService.post<any>(url, data);
  },

  getPhoneDeviceTypeRefData: async () => {
    const { data } = await axios({
      url: 'refData/phoneDeviceType.json',
      method: 'GET'
    });
    return data;
  },

  getPhoneFlagRefData: async () => {
    const { data } = await axios({
      url: 'refData/phoneFlag.json',
      method: 'GET'
    });

    return data;
  }
};

export default cardholdersMaintenanceService;
