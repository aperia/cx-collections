import React from 'react';
import {
  mockActionCreator,
  renderMockStoreId,
  storeId,
  accEValue,
  queryByClass
} from 'app/test-utils';
import CardHolderMaintenance from './index';
import { screen } from '@testing-library/dom';

import {
  cardholderMaintenanceActions,
  defaultStateCardholder
} from 'pages/AccountDetails/CardHolderMaintenance/_redux';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { mockScrollToFn } from 'app/test-utils/mocks/mockProperty';
import { TogglePinProps } from 'pages/__commons/InfoBar/TogglePin';
import * as entitlementsHelpers from 'app/entitlements/helpers';

jest.mock(
  'pages/__commons/InfoBar/TogglePin',
  () => (props: TogglePinProps) => {
    return <div onClick={props.onToggle}>TogglePin</div>;
  }
);

beforeEach(() => {
  jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
});

HTMLCanvasElement.prototype.getContext = jest.fn();
const initialState: Partial<RootState> = {
  cardholderMaintenance: {
    [storeId]: {
      ...defaultStateCardholder,
      expandedAddress: {
        histories: [],
        openModal: '',
        isLoading: false,
        isEndLoadMore: false
      },
      cardholderId: {
        cardNumberValue: 'VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)',
        cardholderName: 'ENDY2,HARRYY T',
        cardholderRole: '01',
        customerExternalIdentifier: 'C20273234913858500085018',
        customerRoleTypeCode: '01',
        customerType: '01',
        id: 'C20273234913858500085018',
        memberSequence: '1',
        status: '',
        statusCode: ''
      },
      isLoading: true
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<CardHolderMaintenance />, {
    initialState
  });
};

const cardholderMaintenanceSpy = mockActionCreator(
  cardholderMaintenanceActions
);
const spyApiErrorNotificationAction = mockActionCreator(
  apiErrorNotificationAction
);

describe('Test CardholderMaintenance UI', () => {
  it('Render UI > reload', () => {
    const updateReloadMock = cardholderMaintenanceSpy('updateReload');
    const getCardHolderMaintenanceMock = cardholderMaintenanceSpy(
      'getCardHolderMaintenance'
    );

    renderWrapper({
      cardholderMaintenance: {
        [storeId]: {
          ...defaultStateCardholder,
          reload: true,
          isLoading: true
        }
      }
    });

    expect(screen.getByText('txt_cardholder')).toBeInTheDocument();
    expect(updateReloadMock).toBeCalledWith({ storeId, reload: false });
    expect(getCardHolderMaintenanceMock).toBeCalledWith({
      accEValue,
      storeId,
      // ignoreInitialState: true,
      // isSetRootLoading: true,
      isCallCheckType: true,
      translateFn: expect.any(Function)
    });
  });

  it('Render UI', () => {
    const getCardHolderMaintenanceMock = cardholderMaintenanceSpy(
      'getCardHolderMaintenance'
    );

    renderWrapper(initialState);

    expect(screen.getByText('txt_cardholder')).toBeInTheDocument();
    expect(getCardHolderMaintenanceMock).toBeCalledWith({
      accEValue,
      isCallCheckType: true,
      storeId,
      translateFn: expect.any(Function)
    });
  });

  it('Render UI > with permission and render StandardAddress View', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);

    renderWrapper(initialState);

    expect(screen.getByText('txt_cardholder')).toBeInTheDocument();
  });

  it('Render UI > with permission and render ExpandedAddress View', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);

    renderWrapper({
      cardholderMaintenance: {
        [storeId]: {
          ...defaultStateCardholder,
          expansionAddressCode: '1',
          expandedAddress: {
            histories: [],
            openModal: '',
            isLoading: false,
            isEndLoadMore: false
          },
          cardholderId: {
            cardNumberValue: 'VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)',
            cardholderName: 'ENDY2,HARRYY T',
            cardholderRole: '01',
            customerExternalIdentifier: 'C20273234913858500085018',
            customerRoleTypeCode: '01',
            customerType: '01',
            id: 'C20273234913858500085018',
            memberSequence: '1',
            status: '',
            statusCode: ''
          },
          isLoading: true
        }
      }
    });

    expect(screen.getByText('txt_cardholder')).toBeInTheDocument();
  });

  it('Render Expanded Address', () => {
    const getCardHolderMaintenanceMock = cardholderMaintenanceSpy(
      'getCardHolderMaintenance'
    );
    renderWrapper({
      cardholderMaintenance: {
        [storeId]: {
          ...defaultStateCardholder,
          expansionAddressCode: '1',
          isLoading: true
        }
      }
    });
    expect(getCardHolderMaintenanceMock).toBeCalledWith({
      accEValue,
      isCallCheckType: true,
      storeId,
      translateFn: expect.any(Function)
    });
  });

  it('Render Modal Edit', () => {
    const getCardHolderMaintenanceMock = cardholderMaintenanceSpy(
      'getCardHolderMaintenance'
    );
    const { wrapper } = renderWrapper({
      cardholderMaintenance: {
        [storeId]: {
          ...defaultStateCardholder,
          isOpenModalPhoneEmail: true,
          isLoading: true
        }
      }
    });
    const queryClass = queryByClass(
      wrapper.baseElement as HTMLElement,
      /modal-dialog dls-modal-dialog dls-animation/
    );
    expect(getCardHolderMaintenanceMock).toBeCalledWith({
      accEValue,
      isCallCheckType: true,
      storeId,
      translateFn: expect.any(Function)
    });
    expect(queryClass).toBeInTheDocument();
  });

  it('handleClickErrorDetail ', () => {
    mockScrollToFn(jest.fn());
    const mockAction = spyApiErrorNotificationAction('openApiDetailErrorModal');
    const { wrapper } = renderWrapper({
      apiErrorNotification: {
        [storeId]: {
          errorDetail: {
            inAddressCardholderFlyOut: {
              request: '500',
              response: '500'
            }
          }
        }
      }
    });

    wrapper.getByText('txt_view_detail').click();

    expect(mockAction).toBeCalledWith({
      forSection: 'inAddressCardholderFlyOut',
      storeId
    });
  });
});

describe('Test CardholderMaintenance UI > no permission', () => {
  beforeEach(() => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(false);
  });

  it('Render Expanded Address > no permission', () => {
    const getCardHolderMaintenanceMock = cardholderMaintenanceSpy(
      'getCardHolderMaintenance'
    );
    renderWrapper({
      cardholderMaintenance: {
        [storeId]: {
          ...defaultStateCardholder,
          expansionAddressCode: '1',
          isLoading: true
        }
      }
    });
    expect(getCardHolderMaintenanceMock).toBeCalledWith({
      accEValue,
      isCallCheckType: true,
      storeId,
      translateFn: expect.any(Function)
    });
  });
});
