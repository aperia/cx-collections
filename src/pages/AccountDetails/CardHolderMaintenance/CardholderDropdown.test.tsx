import React from 'react';
import CardholderDropdown from './CardholderDropdown';
import {
  mockActionCreator,
  queryAllByClass,
  queryByClass,
  renderMockStoreId,
  storeId
} from 'app/test-utils';
import { fireEvent, act } from '@testing-library/react';
import { defaultStateCardholder } from './_redux';
import userEvent from '@testing-library/user-event';
import { ICardholderId } from './types';

import * as useScreenType from '../../../app/_libraries/_dls/hooks/useScreenType';
import { ScreenType } from 'app/_libraries/_dls/hooks/useScreenType/types';
import {
  actionsInfoBar,
  InfoBarSection,
  InfoBarState
} from 'pages/__commons/InfoBar/_redux';

jest.mock('../../../app/_libraries/_dls/hooks/useScreenType');

const mockActionInfoBar = mockActionCreator(actionsInfoBar);

const cardholderId: ICardholderId = {
  cardNumberValue: 'VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)',
  cardholderName: 'ENDY2,HARRYY T',
  cardholderRole: '01',
  customerExternalIdentifier: 'C20273234913858500085018',
  customerRoleTypeCode: '01',
  customerType: '01',
  id: '01',
  memberSequence: '00001',
  status: '',
  statusCode: ''
};

const initialState: Partial<RootState> = {
  cardholderMaintenance: {
    [storeId]: {
      ...defaultStateCardholder,
      cardholdersId: [{ ...cardholderId }, { ...cardholderId, id: '02' }],
      cardholderId: cardholderId,
      isLoading: true
    }
  },
  infoBar: {
    [storeId]: {
      pinned: null,
      isExpand: false
    } as InfoBarState
  }
};

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('Test CardholderDropdown', () => {
  it('Render with no data', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(<CardholderDropdown />);
    const { container, baseElement } = wrapper;
    const queryClass = queryByClass(container, /text-field-container/);
    act(() => {
      userEvent.click(queryClass!);
    });
    jest.runAllTimers();
    const queryClassResultNoFound = queryByClass(
      baseElement as HTMLElement,
      /dls-no-result-found/
    );
    expect(queryClassResultNoFound).toBeInTheDocument();
  });

  it('Render with data', () => {
    jest.useFakeTimers();
    const { wrapper, store } = renderMockStoreId(<CardholderDropdown />, {
      initialState
    });
    const { container, baseElement } = wrapper;
    const queryClass = queryByClass(container, /text-field-container/);
    jest.runAllTimers();
    act(() => {
      userEvent.click(queryClass!);
    });

    const queryDropdownBase = queryByClass(
      baseElement as HTMLElement,
      /dls-dropdown-base-scroll-container/
    );

    const queryDropdownItem = queryAllByClass(
      queryDropdownBase as HTMLElement,
      /dropdown-list--text/
    );

    act(() => {
      fireEvent.click(queryDropdownItem[1]!);
    });

    const state =
      store.getState().cardholderMaintenance[storeId].cardholdersId[1].id;
    expect(state).toEqual('02');
    expect(queryDropdownItem.length).toEqual(
      initialState.cardholderMaintenance![storeId].cardholdersId.length
    );
  });

  it('Should Have Pin with class unpin', () => {
    jest.useFakeTimers();
    jest
      .spyOn(useScreenType, 'default')
      .mockImplementation(() => ScreenType.DESKTOP);

    const { wrapper } = renderMockStoreId(<CardholderDropdown />);

    const { container } = wrapper;

    const queryClassPin = queryByClass(container, /icon icon-pin/);
    jest.runAllTimers();
    act(() => {
      userEvent.click(queryClassPin!);
    });

    const queryClassUnpin = queryByClass(container, /icon icon-unpin/);
    expect(queryClassUnpin).toBeInTheDocument();
  });

  it('Should Have UnPin with class pin', () => {
    jest.useFakeTimers();
    jest
      .spyOn(useScreenType, 'default')
      .mockImplementation(() => ScreenType.DESKTOP);

    const { wrapper } = renderMockStoreId(<CardholderDropdown />, {
      initialState
    });

    const { container } = wrapper;

    const queryClassUnpin = queryByClass(container, /icon icon-pin/);
    jest.runAllTimers();
    act(() => {
      userEvent.click(queryClassUnpin!);
    });

    const queryClassPin = queryByClass(container, /icon icon-unpin/);
    expect(queryClassPin).toBeInTheDocument();
  });

  it('Should Have Pin with expect action', () => {
    jest.useFakeTimers();
    jest
      .spyOn(useScreenType, 'default')
      .mockImplementation(() => ScreenType.DESKTOP);

    const spyUpdateRecover = mockActionInfoBar('updateRecover');
    const spyUpdateIsExpand = mockActionInfoBar('updateIsExpand');
    const spyUpdatePinned = mockActionInfoBar('updatePinned');
    const { wrapper } = renderMockStoreId(<CardholderDropdown />, {
      initialState
    });

    const { container } = wrapper;

    const queryClassPin = queryByClass(container, /icon icon-pin/);
    jest.runAllTimers();
    act(() => {
      userEvent.click(queryClassPin!);
    });

    expect(spyUpdateRecover).toBeCalledWith({ storeId, recover: null });
    expect(spyUpdatePinned).toBeCalledWith({
      pinned: 'Cardholder Maintenance',
      storeId
    });
    expect(spyUpdateIsExpand).toBeCalledWith({ storeId, isExpand: true });
  });

  it('Should Have unPin with expect action', () => {
    jest.useFakeTimers();
    jest
      .spyOn(useScreenType, 'default')
      .mockImplementation(() => ScreenType.DESKTOP);

    const spyUpdateIsExpand = mockActionInfoBar('updateIsExpand');
    const spyUpdatePinned = mockActionInfoBar('updatePinned');
    const { wrapper } = renderMockStoreId(<CardholderDropdown />, {
      initialState: {
        ...initialState,
        infoBar: {
          [storeId]: {
            pinned: InfoBarSection.CardholderMaintenance,
            isExpand: true
          } as InfoBarState
        }
      } as Partial<RootState>
    });
    jest.runAllTimers();
    const { container } = wrapper;

    const queryClassPin = queryByClass(container, /icon icon-unpin/);

    act(() => {
      userEvent.click(queryClassPin!);
    });

    expect(spyUpdatePinned).toBeCalledWith({
      pinned: null,
      storeId
    });
    expect(spyUpdateIsExpand).toBeCalledWith({ storeId, isExpand: false });
  });
});
