export declare type ExpandState =
  | 'isExpandAddress'
  | 'isExpandInfo'
  | 'isExpandEmail'
  | 'isExpandTelePhone';

export declare type ModalExpandedAddress =
  | 'EDIT'
  | 'ADD'
  | 'HISTORY'
  | 'DELETE'
  | '';

export interface IGetCardHolderArgs {
  accEValue: string;
  storeId: string;
  idAddressType?: string;
  ignoreInitialState?: boolean;
  isSetRootLoading?: boolean;
  isCallCheckType?: boolean;
  translateFn: Function;
}

export interface CheckTypeAddressArgs {
  storeId: string;
  accEValue: string;
}

export interface ICardholderId {
  id: string;
  cardholderName: string;
  cardholderRole: '01' | '02' | '03' | '';
  status: string;
  statusCode: string;
  cardNumberValue: string;
  memberSequence: string;
  customerRoleTypeCode: string;
  customerType: string;
  customerExternalIdentifier: string;
}
export interface IPayLoadGetExpandedAddressHistory {
  expandedAddressHistory: IAddressInfo[];
}

export interface IArgGetExpandedAddressHistory {
  storeId: string;
  accEValue: string;
  startSequence: string | number;
  endSequence: string | number;
  ignoreInitialState: boolean;
}
export interface IRequestDataExpandedAddress
  extends IDataRequest,
    CardholderAddress {}
export interface UpdateModalEdit {
  storeId: string;
  open: boolean;
  type?: string;
}
export interface IToggleModal extends StoreIdPayload {
  isOpen: boolean;
}
export interface IPostStandardAddressArgs extends StoreIdPayload {
  accEValue: string;
  cardholderId: string;
  addressLineOne: string;
  addressLineTwo?: string;
  city: string;
  stateSubdivision?: {
    fieldId: string;
    fieldValue: string;
    description: string;
  };
  postalCode: string;
}
export interface IChangeExpand extends StoreIdPayload {
  expandState: ExpandState;
}
export interface IChangeCardholder extends StoreIdPayload {
  cardholderId: ICardholderId;
}
export interface IChangeAddressType extends StoreIdPayload {
  addressType: IAddressInfo;
}
export interface IChangeAddressTypes extends StoreIdPayload {
  addressTypes: IAddressInfo[];
}
export interface EmailData {
  electronicMailHomeAddressText?: string;
  electronicMailHomeStatusIndicator?: string;
  electronicMailHomeSolicitIndicator?: string;
  electronicMailWorkAddressText?: string;
  electronicMailWorkStatusIndicator?: string;
  electronicMailWorkSolicitIndicator?: string;
}
export interface IPayLoadExpandedAddressInfo {
  addressTypes: IAddressInfo[];
}
export interface IArgsExpandedAddressInfo extends StoreIdPayload {
  idAddress?: string | null;
  isSetRootLoading?: boolean;
}
export interface TelePhoneInformation {
  less?: string;
  more?: {
    deviceType?: MagicKeyValue;
    lastUpdated?: string;
    flag?: MagicKeyValue;
  };
}

export interface FormatTelephoneInformation {
  less?: string;
  more?: {
    deviceType?: {
      value: string;
      description: string;
    };
    lastUpdated?: string;
    flag?: {
      value: string;
      description: string;
    };
  };
}
export interface TelePhoneData {
  homePhone?: TelePhoneInformation;
  workPhone?: TelePhoneInformation;
  mobilePhone?: TelePhoneInformation;
  fax?: TelePhoneInformation;
  miscellaneous?: TelePhoneInformation;
}

export interface IAddressInfo extends MagicKeyValue {
  id: string;
  addressCategory: IRefData;
  addressType: IRefData;
  effectiveFrom: Date | string;
  effectiveTo: Date | string;
  countryISO: IRefData;
  houseNumber: string;
  houseBuilding: string;
  streetName: string;
  POBox: string;
  attention: string;
  addressLineOne: string;
  addressLineTwo: string;
  addressLineThree: string;
  addressLineFour: string;
  cityName: string;
  stateSubdivision: IRefData;
  postalCode: string;
  addressRelationship: IRefData;
  formattedAddressCode: string;
}

export interface ICardholderInfo {
  cardNumber: string;
  lastName: string;
  firstName: string;
  middleName: string;
  cardholderRole: string;
  authCBRFlag: string;
  suffix: string;
  prefix: string;
  title: string;
  memberSequence: string;
  qualification: string;
  embossingName: string;
  salutation: string;
  dateOfBirth: string;
  ssnTaxId: string;
  solicitation: string;
}

export interface StandardAddressInfosData {
  addressCategory: {
    fieldId: string;
    fieldValue: string;
    description: string;
  } | null;
  addressType: {
    fieldId: string;
    fieldValue: string;
    description: string;
  } | null;
  effectiveFrom?: Date;
  effectiveTo?: Date;
  countryISO: {
    fieldId: string;
    fieldValue: string;
    description: string;
  } | null;
  addressLineOne?: string;
  addressLineTwo?: string;
  city?: string;
  stateSubdivision: {
    fieldId: string;
    fieldValue: string;
    description: string;
  } | null;
  postalCode?: string;
  expansionAddressCode: string;
}

export interface ICardholder {
  cardholderId: string;
  cardholderInfo: ICardholderInfo;
  standardAddressInfo?: StandardAddressInfosData;
  standardAddressInfoHistory?: StandardAddressInfosData[];
  email: EmailData;
  telephone: TelePhoneData;
}

export interface IGetCardHolderPayload {
  cardholdersId: ICardholderId[];
  cardholders: ICardholder[];
}

export interface APIGetCardHolderPayload {
  customerInquiry: MagicKeyValue[];
}

export interface IInitStateCardholder extends MagicKeyValue {
  [storeId: string]: ICardholderReducer;
}

export interface ICardholderReducer {
  expansionAddressCode: string;
  cardholdersId: ICardholderId[];
  cardholderId: ICardholderId;
  cardholders: ICardholder[];
  addressTypes: IAddressInfo[];
  addressType: IAddressInfo;
  expandedAddress: {
    histories: IAddressInfo[];
    openModal: ModalExpandedAddress;
    isLoading: boolean;
    isEndLoadMore: boolean;
  };
  standardAddress: {
    addresses?: GetAddressPayload;
    addressHistory?: StandardAddressHistory[];
    isLoadingAddressHistory?: boolean;
    isLoadingAddress?: boolean;
    isLoadMoreAddressHistory?: boolean;
    isOpenModalAddressHistory?: boolean;
    isOpenModalAdd?: boolean;
    isOpenModalEdit?: boolean;
    isLoadingAddAddress?: boolean;
    isLoadingEditAddress?: boolean;
  };
  isExpandAddress: boolean;
  isExpandInfo: boolean;
  isExpandTelePhone: boolean;
  isExpandEmail: boolean;
  isLoading: boolean;
  isOpenModalPhoneEmail: boolean;
  typeEdit: string;
  reload: boolean;
  loadingEdit: boolean;
  ignoreInitialState?: boolean;
}

export interface PostDataUpdate {
  common: {
    accountId: string;
    externalCustomerId: string;
  };
  data: MagicKeyValue;
}

export interface UpdateCardHolderArg {
  storeId: string;
  accEValue: string;
  type: string;
  postData: PostDataUpdate;
}

export interface UpdateReload {
  storeId: string;
  reload: boolean;
}

export interface FormatPostData {
  homePhone?: FormatTelephoneInformation;
  workPhone?: FormatTelephoneInformation;
  mobilePhone?: FormatTelephoneInformation;
  fax?: FormatTelephoneInformation;
  miscellaneous?: FormatTelephoneInformation;
}

export interface IDataRequest {
  common: {
    externalCustomerId?: string;
    accountId: string;
  };
}

export interface DeleteExpandedAddress {
  cardholderId?: string;
  idAddressType?: string;
}

export interface IDeleteExpandedAddress extends CommonAccountIdRequestBody {
  data: DeleteExpandedAddress;
}

export interface DeleteExpandedAddressArgs {
  storeId: string;
  accEValue: string;
}

export interface DeleteExpandedAddressPayload {
  data: MagicKeyValue;
}

export interface IDisableRange {
  fromDate: Date | string;
  toDate: Date | string;
}

export interface GetAddressRequest extends CommonAccountIdRequestBody {}

export interface CardholderAddress {
  accountIdentifier?: string;
  accountNumber?: string;
  addressAttentionLineText?: string;
  addressContinuationLine4Text?: string;
  addressContinuationLine1Text?: string;
  addressContinuationLine3Text?: string;
  addressContinuationLine2Text?: string;
  addressCategoryCode?: ADDRESS_CATEGORY_MAP_KY;
  addressFormatCode?: string;
  addressRelationshipTypeCode?: string;
  addressSubdivisionOneText?: string;
  addressSubdivisionTwoText?: string;
  addressTypeCode?: ADDRESS_TYPE_MAP_KY;
  addressValidationCode?: string;
  alternateAccountIdentifier?: string;
  cardholderAccountNumber?: string;
  changeTypeCode?: string;
  cityName?: string;
  companyName?: string;
  convertedEnteredIdentifier?: string;
  countryCode?: string;
  customerExternalIdentifier?: string;
  customerRoleTypeCode?: string;
  customerTypeCode?: string;
  deliveryPointCode?: string;
  effectiveFromDate?: string;
  effectiveToDate?: string;
  enteredIdentifier?: string;
  housebuildingName?: string;
  houseBuildingName?: string;
  houseIdentifier?: string;
  mailCode?: string;
  mailUpdateCode?: string;
  memberSequenceIdentifier?: string;
  postOfficeBoxIdentifier?: string;
  postalCode?: string;
  streetName?: string;
  validAddressIndicator?: string;
  addressChangedTimestamp?: string;
}

export interface GetAddressPayload {
  primary?: CardholderAddress[];
  secondary?: CardholderAddress[];
  additionalUsers?: CardholderAddress[];
}
export type AddressRoleTypes = keyof GetAddressPayload;

export interface GetAddressArgs extends StoreIdPayload {
  accEValue: string;
}

export interface GetStandardAddressHistoryArgs
  extends StoreIdPayload,
    CommonAccountIdRequestBody {
  startSequence: number;
  endSequence: number;
}

export interface GetStandardAddressHistoryRequest
  extends CommonAccountIdRequestBody {
  startSequence?: number;
  endSequence?: number;
}

export type ADDRESS_CATEGORY_MAP_KY = 'P' | 'R' | 'T';

export type ADDRESS_TYPE_MAP_KY = 'BLL1' | 'PLST' | 'LTTR' | 'RFRN';

export interface StandardAddressHistory {
  addressContinuationLine1Text: string;
  addressContinuationLine2Text?: string;
  addressCategoryCode?: ADDRESS_CATEGORY_MAP_KY;
  addressTypeCode?: ADDRESS_TYPE_MAP_KY;
  effectiveFromDate?: string;
  effectiveToDate?: string;
  cityName?: string;
  addressSubdivisionOneText?: string;
  postalCode?: string;
}

export interface StandardAddressHistoryGrid {
  address?: string;
  addressCategory?: string;
  addressType?: string;
  effectiveFromDate?: string;
  effectiveToDate?: string;
}

export interface GetStandardAddressHistoryPayload {
  addressHistory: StandardAddressHistory[];
}

export interface AddStandardAddressRequest extends CommonAccountIdRequestBody {
  bypass?: string;
  letter?: string;
  propagate?: string;
  addressLineOne?: string;
  addressLineTwo?: string;
  city?: string;
  state?: string;
  zipCode?: string;
  memberSequenceIndefitier?: string;
  customerRoleTypeCode?: string;
  customerType?: string;
}

export interface StandardAddressConfig {
  selectFields: string[];
}

export interface StandardAddressHistoryConfig {
  selectFields: string[];
}
export interface IAddAddress extends IAddressInfo {
  customerRoleTypeCode?: string;
  customerType?: string;
  memberSequenceIndefitier?: string;
}

export interface IRequestDeleteExpandedAddress {
  common: {
    externalCustomerId: string;
    accountId: string;
  };
  memberSequenceIdentifier: string;
  customerType: string;
  addressType: string | undefined;
  categoryCode: string | undefined;
  effectiveFromDate: string | undefined;
  effectiveToDate: string | undefined;
}

export interface StandardAddressForCompare extends MagicKeyValue {
  addressLineOne?: string;
  addressLineTwo?: string;
  cityName?: string;
  city?: string;
  stateSubdivision?: {
    fieldId?: string;
    fieldValue?: string;
    description?: string;
  };
  postalCode?: string;
}
