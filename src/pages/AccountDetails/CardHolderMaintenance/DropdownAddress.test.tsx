import React from 'react';
import DropdownAddress from './DropdownAddress';
import {
  queryAllByClass,
  queryByClass,
  renderMockStoreId,
  storeId
} from 'app/test-utils';
import { fireEvent, act, waitFor } from '@testing-library/react';
import { defaultStateCardholder } from './_redux';
import userEvent from '@testing-library/user-event';

const addressInfo = {
  POBox: '',
  addressCategory: {
    fieldValue: 'Permanent',
    fieldID: 'P',
    description: 'P - Permanent'
  },
  addressLineFour: '',
  addressLineOne: '333 MENLAY',
  addressLineThree: '',
  addressLineTwo: '',
  addressRelationship: {
    fieldValue: 'Unknown',
    fieldID: 'U',
    description: 'U - Unknown'
  },
  addressType: {
    fieldValue: 'Billing',
    fieldID: 'BLL1',
    description: 'BLL1 - Billing'
  },
  attention: '',
  cityName: 'OMAHA',
  countryISO: {
    fieldValue: 'United States of America',
    fieldID: 'USA',
    description: 'USA - United States of America'
  },
  effectiveFrom: '2021-05-18',
  effectiveTo: '9999-12-31',
  formattedAddressCode: 'F',
  houseBuilding: '',
  houseNumber: '',
  id: '00001-P-BLL1-2021-06-18-9999-12-31',
  memberSequence: '00001',
  postalCode: '10003',
  stateSubdivision: {
    fieldValue: 'New York',
    fieldID: 'NY',
    description: 'NY - New York'
  },
  streetName: ''
};
HTMLCanvasElement.prototype.getContext = jest.fn();
const initialState: Partial<RootState> = {
  cardholderMaintenance: {
    [storeId]: {
      ...defaultStateCardholder,
      addressType: addressInfo,
      addressTypes: [
        { ...addressInfo },
        {
          ...addressInfo,
          id: '00001-P-BLL1-2021-05-18-9999-12-31',
          addressType: {
            fieldValue: 'Plastics',
            fieldID: 'PLST'
          }
        }
      ],
      isLoading: true
    }
  }
};
describe('Test DropdownAddress', () => {
  it('Render with no data', () => {
    waitFor(() => {
      jest.useFakeTimers();
      const { wrapper } = renderMockStoreId(<DropdownAddress />);
      const { container, baseElement } = wrapper;
      const queryClass = queryByClass(container, /text-field-container/);
      jest.runAllTimers();
      act(() => {
        userEvent.click(queryClass!);
      });
      const queryClassResultNoFound = queryByClass(
        baseElement as HTMLElement,
        /dls-no-result-found/
      );
      expect(queryClassResultNoFound).toBeInTheDocument();
    })
  });

  it('Render with data', () => {
    waitFor(() => {
      jest.useFakeTimers();
      const { wrapper, store } = renderMockStoreId(
        <DropdownAddress isDisplayDate={true} />,
        {
          initialState
        }
      );
      const { container, baseElement } = wrapper;
      const queryClass = queryByClass(container, /text-field-container/);
      jest.runAllTimers();
      act(() => {
        userEvent.click(queryClass!);
      });

      const queryDropdownBase = queryByClass(
        baseElement as HTMLElement,
        /dls-dropdown-base-scroll-container/
      );

      const queryDropdownItem = queryAllByClass(
        queryDropdownBase as HTMLElement,
        /item/
      );

      fireEvent.click(queryDropdownItem[1]!);
      const state =
        store.getState().cardholderMaintenance[storeId].addressTypes[1]
          .addressType;
      expect(state).toEqual({
        fieldValue: 'Plastics',
        fieldID: 'PLST'
      });
      expect(queryDropdownItem.length).toEqual(
        initialState.cardholderMaintenance![storeId].addressTypes.length
      );
    })
  });
});
