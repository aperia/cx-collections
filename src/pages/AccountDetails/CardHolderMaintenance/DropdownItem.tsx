import React from 'react';

// components
import { Badge } from 'app/_libraries/_dls/components';

// i18next
import { COMMON_STATUS } from 'app/constants';

import cn from 'classnames';
import isEmpty from 'lodash.isempty';
import isUndefined from 'lodash.isundefined';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface CardHolderDropdownProps {}

export interface ItemDropdownProps {
  isDropdownItem?: boolean;
  cardholderName: string;
  cardholderRole: string;
  status: string;
  statusCode: string;
}

const DropdownItem: React.FC<ItemDropdownProps> = ({
  cardholderName,
  cardholderRole,
  status,
  statusCode,
  isDropdownItem = true
}) => {
  const { t } = useTranslation();
  return (
    <div
      className="d-flex align-items-center justify-content-between"
      data-testid={genAmtId(
        'cardholder-item',
        `${cardholderName}-${cardholderRole}`,
        ''
      )}
    >
      <div className="dropdown-list--text">
        {cardholderName && cardholderRole && (
          <p
            className="d-flex"
            title={` ${cardholderName} • ${t(cardholderRole)}`}
          >
            <span>{cardholderName}</span>&nbsp;•&nbsp;
            <span className="flex-1"> {t(cardholderRole)}</span>
          </p>
        )}
      </div>
      <div
        className={cn('dropdown-list--badge', {
          'dropdown-list-item--badge': isDropdownItem,
          'mt-n16': !isDropdownItem
        })}
        title={t(isUndefined(status) ? 'txt_normal' : status)}
      >
        <Badge
          color={
            isUndefined(COMMON_STATUS[statusCode])
              ? 'green'
              : COMMON_STATUS[statusCode]
          }
          dataTestId={genAmtId(
            'cardholder-item',
            `${cardholderName}-${cardholderRole}_status`,
            ''
          )}
        >
          {t(isEmpty(status) ? 'txt_normal' : status)}
        </Badge>
      </div>
    </div>
  );
};

export default DropdownItem;
