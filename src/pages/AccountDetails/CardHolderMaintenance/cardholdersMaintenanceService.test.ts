// mocks
import { mockAxiosResolve } from 'app/test-utils/mocks/mockAxiosResolve';

// services
import cardholdersMaintenanceService from './cardholdersMaintenanceService';

// utils
import { storeId, mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import {
  IAddressInfo,
  IDataRequest,
  PostDataUpdate,
  AddStandardAddressRequest,
  IRequestDeleteExpandedAddress,
  IRequestDataExpandedAddress,
  GetStandardAddressHistoryRequest
} from './types';

const addressInfo: IAddressInfo = {
  POBox: '',
  addressCategory: {
    fieldValue: 'Permanent',
    fieldID: 'P',
    description: 'P - Permanent'
  },
  addressLineFour: '',
  addressLineOne: '333 MENLAY',
  addressLineThree: '',
  addressLineTwo: '',
  addressRelationship: {
    fieldValue: 'Unknown',
    fieldID: 'U',
    description: 'U - Unknown'
  },
  addressType: {
    fieldValue: 'Billing',
    fieldID: 'BLL1',
    description: 'BLL1 - Billing'
  },
  attention: '',
  cityName: 'OMAHA',
  countryISO: {
    fieldValue: 'United States of America',
    fieldID: 'USA',
    description: 'USA - United States of America'
  },
  effectiveFrom: '2021-05-18',
  effectiveTo: '9999-12-31',
  formattedAddressCode: 'F',
  houseBuilding: '',
  houseNumber: '',
  id: '00001-P-BLL1-2021-05-18-9999-12-31',
  memberSequence: '00001',
  postalCode: '10003',
  stateSubdivision: {
    fieldValue: 'New York',
    fieldID: 'NY',
    description: 'NY - New York'
  },
  streetName: ''
};

describe('cardholdersMaintenanceService', () => {
  describe('checkTypeofAddress', () => {
    const params: MagicKeyValue = { id: 'id', data: [] };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.checkTypeofAddress(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.checkTypeofAddress(params);

      expect(mockService).toBeCalledWith(
        apiUrl.cardholdersMaintenance.checkTypeAddress,
        params
      );
    });
  });

  describe('getCardholderMaintenance', () => {
    const params: IDataRequest = { common: { accountId: storeId } };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.getCardholderMaintenance(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.getCardholderMaintenance(params);

      expect(mockService).toBeCalledWith(
        apiUrl.cardholdersMaintenance.getCardholderMaintenance,
        params
      );
    });
  });

  describe('updateCardHolder', () => {
    const params: PostDataUpdate = {
      common: { accountId: storeId, externalCustomerId: storeId },
      data: { id: 'id', data: [] }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.updateCardHolder(params);

      expect(mockService).toBeCalledWith('', {
        common: params.common,
        ...params.data
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.updateCardHolder(params);

      expect(mockService).toBeCalledWith(apiUrl.cardholdersMaintenance.update, {
        common: params.common,
        ...params.data
      });
    });
  });

  describe('addStandardAddress', () => {
    const params: AddStandardAddressRequest = {
      common: { accountId: storeId },
      bypass: 'yes'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.addStandardAddress(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.addStandardAddress(params);

      expect(mockService).toBeCalledWith(
        apiUrl.cardholdersMaintenance.addStandardAddress,
        params
      );
    });
  });

  describe('editStandardAddress', () => {
    const params: AddStandardAddressRequest = {
      common: { accountId: storeId },
      bypass: 'yes'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.editStandardAddress(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.editStandardAddress(params);

      expect(mockService).toBeCalledWith(
        apiUrl.cardholdersMaintenance.editStandardAddress,
        params
      );
    });
  });

  describe('addAndUpdateExpandedAddress', () => {
    const params: IRequestDataExpandedAddress = {
      common: { accountId: storeId },
      ...addressInfo
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.addAndUpdateExpandedAddress(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.addAndUpdateExpandedAddress(params);

      expect(mockService).toBeCalledWith(
        apiUrl.cardholdersMaintenance.addAndUpdateExpandedAddress,
        params
      );
    });
  });

  describe('deleteAddressExpandedAddress', () => {
    const params: IRequestDeleteExpandedAddress = {
      common: { accountId: storeId, externalCustomerId: storeId },
      memberSequenceIdentifier: 'id',
      customerType: 'id',
      addressType: 'string',
      categoryCode: 'SE',
      effectiveFromDate: '01/04/2021',
      effectiveToDate: '02/03/2021'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.deleteAddressExpandedAddress(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.deleteAddressExpandedAddress(params);

      expect(mockService).toBeCalledWith(
        apiUrl.cardholdersMaintenance?.deleteAddressExpandedAddress,
        params
      );
    });
  });

  describe('getExpandedAddressHistory', () => {
    const params = {
      common: { accountId: storeId, externalCustomerId: storeId }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.getExpandedAddressHistory(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.getExpandedAddressHistory(params);

      expect(mockService).toBeCalledWith(
        apiUrl.cardholdersMaintenance?.getExpandedAddressHistory,
        params
      );
    });
  });

  describe('getAddressInfo', () => {
    const params = {
      common: { accountId: storeId, externalCustomerId: storeId }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.getAddressInfo(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.getAddressInfo(params);

      expect(mockService).toBeCalledWith(
        apiUrl.cardholdersMaintenance.getAddressInfo,
        params
      );
    });
  });

  describe('getStandardAddressHistory', () => {
    const params: GetStandardAddressHistoryRequest = {
      common: { accountId: storeId }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.getStandardAddressHistory(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      cardholdersMaintenanceService.getStandardAddressHistory(params);

      expect(mockService).toBeCalledWith(
        apiUrl.cardholdersMaintenance.getStandardAddressHistory,
        params
      );
    });
  });

  it('getPhoneDeviceTypeRefData', async () => {
    await cardholdersMaintenanceService.getPhoneDeviceTypeRefData();

    expect(mockAxiosResolve).toBeCalledWith({
      url: 'refData/phoneDeviceType.json',
      method: 'GET'
    });
  });

  it('getPhoneFlagRefData', async () => {
    await cardholdersMaintenanceService.getPhoneFlagRefData();

    expect(mockAxiosResolve).toBeCalledWith({
      url: 'refData/phoneFlag.json',
      method: 'GET'
    });
  });
});
