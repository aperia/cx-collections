import React from 'react';
import { screen } from '@testing-library/react';

// utils
import {
  storeId,
  renderMockStoreId
} from 'app/test-utils/renderComponentWithMockStore';

// hook

// components
import Email from 'pages/AccountDetails/CardHolderMaintenance/Email';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// services
import * as services from '../helpers';
import { VIEW_NAME_CARD_MAINTENANCE } from '../constants';
import { defaultStateCardholder } from '../_redux';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

let spy1: jest.SpyInstance;

afterEach(() => {
  if (spy1) {
    spy1.mockReset();
    spy1.mockRestore();
  }
});

const initialState: Partial<RootState> = {
  cardholderMaintenance: {
    [storeId]: {
      ...defaultStateCardholder,
      cardholderId: {
        cardNumberValue: 'VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)',
        cardholderName: 'ENDY2,HARRYY T',
        cardholderRole: '01',
        customerExternalIdentifier: 'C20273234913858500085018',
        customerRoleTypeCode: '01',
        customerType: '01',
        id: 'C20273234913858500085018',
        memberSequence: '1',
        status: 'status',
        statusCode: 'statusCode'
      },
      cardholders: [
        {
          cardholderId: 'C20273234913858500085018',
          cardholderInfo: {
            authCBRFlag: 'Do not report',
            cardNumber: '603530******0985',
            cardholderRole: '02',
            dateOfBirth: '********0001',
            embossingName: '0000000000000000',
            firstName: '',
            lastName: '',
            memberSequence: '00002',
            middleName: '',
            prefix: '',
            qualification: '',
            salutation: 'Unknown Gender',
            solicitation: 'Solicitation Allowed',
            ssnTaxId: '',
            suffix: '',
            title: ''
          },
          email: {
            electronicMailHomeAddressText: 'electronicMailHomeAddressText',
            electronicMailWorkAddressText: 'electronicMailWorkAddressText'
          },
          telephone: {
            homePhone: { less: 'homePhone less' },
            workPhone: { less: 'workPhone less' },
            mobilePhone: { less: 'mobilePhone less' },
            miscellaneous: { less: 'miscellaneous less' },
            fax: { less: 'fax less' }
          }
        }
      ],
      isLoading: false
    }
  }
};

describe('Test View Cardholder Email', () => {
  it('Render UI', () => {
    renderMockStoreId(<Email />, { initialState });

    expect(
      screen.getByTestId(VIEW_NAME_CARD_MAINTENANCE.CARD_EMAIL)
    ).toBeInTheDocument();
  });

  it('Render UI with empty data', () => {
    renderMockStoreId(<Email />, {});

    expect(screen.getByText(I18N_COMMON_TEXT.NO_DATA)).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('changeToggle', async () => {
    const mockAction = jest.fn().mockImplementation(() => ({ type: 'type' }));
    spy1 = jest.spyOn(services, 'handleToggle').mockImplementation(mockAction);

    renderMockStoreId(<Email />, { initialState });
    screen.getAllByRole('button')[0].click();

    expect(mockAction).toBeCalledWith('isExpandEmail', storeId);
  });
});
