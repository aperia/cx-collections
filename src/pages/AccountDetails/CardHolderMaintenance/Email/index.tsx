import React from 'react';

// components
import SectionControl from 'app/components/SectionControl';
import { View } from 'app/_libraries/_dof/core';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// i18n
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// action
import { handleToggle } from '../helpers';

// redux
import { useDispatch } from 'react-redux';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import {
  selectEmailInfo,
  selectIsExpandEmail
} from 'pages/AccountDetails/CardHolderMaintenance/_redux/selector';

// constants
import {
  VIEW_NAME_CARD_MAINTENANCE,
  I18N_CARDHOLDER_MAINTENANCE
} from '../constants';
import { TYPE_EDIT } from '../TelePhone/constants';

// types
import { EmailData } from '../types';

// helpers
import isEmpty from 'lodash.isempty';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { checkPermission } from 'app/entitlements';
import { PERMISSIONS } from 'app/entitlements/constants';

export interface EmailProps {}

const Email: React.FC<EmailProps> = () => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const value = useStoreIdSelector<EmailData>(selectEmailInfo);
  const selectExpand = useStoreIdSelector<boolean>(selectIsExpandEmail);

  const testId = 'infobar_cardholder_email';

  const changeToggle = () => {
    dispatch(handleToggle('isExpandEmail', storeId));
  };

  const noData =
    isEmpty(value?.electronicMailHomeAddressText) &&
    isEmpty(value?.electronicMailWorkAddressText);

  return (
    <SectionControl
      title={t(I18N_COMMON_TEXT.EMAIL)}
      classNamesView="ml-20"
      sectionKey={VIEW_NAME_CARD_MAINTENANCE.CARD_EMAIL}
      onToggle={changeToggle}
      isExpand={selectExpand}
      isEdit={checkPermission(
        PERMISSIONS.CARDHOLDER_MAINTENANCE_EDIT_EMAIL,
        storeId
      )}
      typeEdit={TYPE_EDIT.EMAIL}
      textTooltipEdit={I18N_CARDHOLDER_MAINTENANCE.EDIT_EMAIL}
      dataTestId={testId}
    >
      {noData ? (
        <p
          className="color-grey mt-16"
          data-testid={genAmtId(testId, 'no-data', 'CardHolderEmail')}
        >
          {t(I18N_COMMON_TEXT.NO_DATA)}
        </p>
      ) : (
        <View
          id={`cardholderMaintenanceEmail_${storeId}`}
          descriptor={VIEW_NAME_CARD_MAINTENANCE.CARD_EMAIL}
          formKey={`cardholderMaintenanceEmail_${storeId}`}
          value={value}
        />
      )}
    </SectionControl>
  );
};

export default Email;
