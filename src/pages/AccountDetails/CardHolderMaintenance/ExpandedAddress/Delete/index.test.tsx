import React from 'react';
import DeleteAddress from './index';
import {
  mockActionCreator,
  queryByClass,
  renderMockStoreId,
  storeId,
  accEValue
} from 'app/test-utils';
import { fireEvent } from '@testing-library/react';
import { cardholderMaintenanceActions } from '../../_redux';

const mockAction = mockActionCreator(cardholderMaintenanceActions);

describe('Test DeleteAddress', () => {
  it('Should have Render UI', () => {
    const { wrapper } = renderMockStoreId(
      <DeleteAddress onCloseModal={() => {}} />
    );
    const { baseElement } = wrapper;
    const queryClassModal = queryByClass(
      baseElement as HTMLElement,
      /modal-backdrop/
    );
    expect(queryClassModal).toBeInTheDocument();
  });

  it('Should have onCloseModal with icon close', () => {
    const onCloseModal = jest.fn();

    const { wrapper } = renderMockStoreId(
      <DeleteAddress onCloseModal={onCloseModal} />
    );
    const { baseElement } = wrapper;

    const queryHeader = queryByClass(
      baseElement as HTMLElement,
      /dls-modal-header modal-header/
    );

    const queryButtonIconClose = queryByClass(
      queryHeader as HTMLElement,
      /btn btn-icon-secondary/
    );

    fireEvent.click(queryButtonIconClose!);

    expect(onCloseModal).toHaveBeenCalled();
  });

  it('Should have onCloseModal with button cancel', () => {
    const onCloseModal = jest.fn();
    const { wrapper } = renderMockStoreId(
      <DeleteAddress onCloseModal={onCloseModal} />
    );
    const { baseElement } = wrapper;

    const queryFooter = queryByClass(
      baseElement as HTMLElement,
      /dls-modal-footer modal-footer/
    );

    const queryButtonCancel = queryByClass(
      queryFooter as HTMLElement,
      /btn btn-secondary/
    );

    fireEvent.click(queryButtonCancel!);

    expect(onCloseModal).toHaveBeenCalled();
  });

  it('Should have click Delete', () => {
    const onCloseModal = jest.fn();

    const triggerDeleteExpandedAddress = mockAction(
      'triggerDeleteExpandedAddress'
    );
    const { wrapper } = renderMockStoreId(
      <DeleteAddress onCloseModal={onCloseModal} />
    );

    const { baseElement } = wrapper;
    const queryFooter = queryByClass(
      baseElement as HTMLElement,
      /dls-modal-footer modal-footer/
    );

    const queryButtonDelete = queryByClass(
      queryFooter as HTMLElement,
      /btn btn-danger/
    );

    fireEvent.click(queryButtonDelete!);

    expect(triggerDeleteExpandedAddress).toHaveBeenCalledWith({
      storeId,
      accEValue
    });
  });
});
