import React, { useMemo } from 'react';

// redux
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { I18N_CARDHOLDER_MAINTENANCE } from '../../constants';
import {
  isLoadingExpandedAddress,
  selectCardholderAddressType
} from '../../_redux/selector';
import { useDispatch } from 'react-redux';

// action
import { useTranslation } from 'app/_libraries/_dls/hooks';

// types
import { IAddressInfo } from '../../types';
import { generateLabel, translateAddressInfo } from '../../helpers';
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { cardholderMaintenanceActions } from '../../_redux';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface DeleteAddressProps {
  onCloseModal: () => void;
}

const DeleteAddress: React.FC<DeleteAddressProps> = ({ onCloseModal }) => {
  const { storeId, accEValue } = useAccountDetail();
  const { t } = useTranslation();

  const dispatch = useDispatch();
  const isLoading = useStoreIdSelector<boolean>(isLoadingExpandedAddress);
  const addressTypeData = useStoreIdSelector<IAddressInfo>(
    selectCardholderAddressType
  );
  const addressType: IAddressInfo = useMemo(
    () => translateAddressInfo(addressTypeData, t),
    [addressTypeData, t]
  );
  const testId = 'cardholder_expanded-address_delete';

  const handleDeleteExpandedAddress = () => {
    dispatch(
      cardholderMaintenanceActions.triggerDeleteExpandedAddress({
        storeId,
        accEValue
      })
    );
  };

  return (
    <Modal
      xs
      loading={isLoading}
      show={true}
      enforceFocus={false}
      dataTestId={testId}
    >
      <ModalHeader
        border
        closeButton
        onHide={onCloseModal}
        dataTestId={`${testId}_header`}
      >
        <ModalTitle dataTestId={`${testId}_header-title`}>
          {t(I18N_CARDHOLDER_MAINTENANCE.CONFIRM_DELETE_ADDRESS)}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <p
          data-testid={genAmtId(
            testId!,
            'confirm-delete-message',
            'ExpandedAddress.DeleteAddress'
          )}
        >
          {t(I18N_CARDHOLDER_MAINTENANCE.CONFIRM_DELETE_ADDRESS_BODY)}
        </p>
        <p
          className="mt-16"
          data-testid={genAmtId(
            testId!,
            'address-type',
            'ExpandedAddress.DeleteAddress'
          )}
        >
          {t(I18N_CARDHOLDER_MAINTENANCE.ADDRESS_TYPE)}
          {': '}
          <span
            className="fw-500"
            data-testid={genAmtId(
              testId!,
              'address-type-value',
              'ExpandedAddress.DeleteAddress'
            )}
          >
            {generateLabel(addressType, true, t)}
          </span>
        </p>
      </ModalBody>

      <ModalFooter>
        <Button
          variant="secondary"
          onClick={onCloseModal}
          dataTestId={`${testId}_footer_close-btn`}
        >
          {t(I18N_COMMON_TEXT.CANCEL)}
        </Button>
        <Button
          variant="danger"
          onClick={handleDeleteExpandedAddress}
          dataTestId={`${testId}_footer_delete-btn`}
        >
          {t(I18N_COMMON_TEXT.DELETE)}
        </Button>
      </ModalFooter>
    </Modal>
  );
};
export default DeleteAddress;
