import React from 'react';
import { screen } from '@testing-library/react';
import {
  storeId,
  renderMockStoreId,
  queryByClass,
  mockActionCreator
} from 'app/test-utils';
import { defaultStateCardholder } from '../../_redux';
import AddAddress from './';
import { formatTimeDefault } from 'app/helpers';
import { FormatTime } from 'app/constants/enums';
import { IAddressInfo } from '../../types';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import * as reduxForm from 'redux-form';

const spyAction = mockActionCreator(confirmModalActions);

const nameForm = `${storeId}-addExpandedAddressView`;

const addressCategoryPlastic = {
  addressCategory: {
    fieldValue: 'Plastic',
    fieldID: 'PLST',
    description: 'Plastic - Billing'
  }
};

const addressInfo = {
  POBox: '',
  addressCategory: {
    fieldValue: 'Permanent',
    fieldID: 'P',
    description: 'P - Permanent'
  },
  addressLineFour: '',
  addressLineOne: '333 MENLAY',
  addressLineThree: '',
  addressLineTwo: '',
  addressRelationship: {
    fieldValue: 'Unknown',
    fieldID: 'U',
    description: 'U - Unknown'
  },
  addressType: {
    fieldValue: 'Billing',
    fieldID: 'BLL1',
    description: 'BLL1 - Billing'
  },
  attention: '',
  cityName: 'OMAHA',
  countryISO: {
    fieldValue: 'United States of America',
    fieldID: 'USA',
    description: 'USA - United States of America'
  },
  effectiveFrom: '2021-05-18',
  effectiveTo: '9999-12-31',
  formattedAddressCode: 'F',
  houseBuilding: '',
  houseNumber: '',
  id: '00001-P-BLL1-2021-06-18-9999-12-31',
  memberSequence: '00001',
  postalCode: '10003',
  stateSubdivision: {
    fieldValue: 'New York',
    fieldID: 'NY',
    description: 'NY - New York'
  },
  streetName: ''
} as IAddressInfo;

const form = {
  [nameForm]: {
    values: {
      ...addressInfo
    }
  }
};

const generateState = ({
  openModal = 'ADD',
  isLoading = false,
  addressType = {} as IAddressInfo,
  addressTypes = [] as IAddressInfo[],
  form = {} as any
}) => {
  return {
    form,
    cardholderMaintenance: {
      [storeId]: {
        ...defaultStateCardholder,
        expandedAddress: {
          histories: [],
          openModal,
          isLoading,
          isEndLoadMore: true
        },
        addressTypes: addressTypes,
        addressType,
        isLoading: false
      }
    }
  } as Partial<RootState>;
};

describe('Test AddExpandedAddress', () => {
  it('Should have Loading', () => {
    const { wrapper } = renderMockStoreId(
      <AddAddress onCloseModal={() => undefined} />,
      {
        initialState: generateState({ isLoading: true })
      }
    );

    const queryLoading = queryByClass(
      wrapper.baseElement as HTMLElement,
      /loading/
    );

    expect(queryLoading).toBeInTheDocument();
  });

  it('Should title address detail', () => {
    renderMockStoreId(<AddAddress onCloseModal={() => undefined} />, {
      initialState: generateState({})
    });

    const queryTitle = screen.queryByText('txt_cardholder_address_details');

    expect(queryTitle).toBeInTheDocument();
  });

  it('Should have click icon close', () => {
    const onCloseModal = jest.fn();
    const confirmModalActionsOpen = spyAction('open');

    const { wrapper } = renderMockStoreId(
      <AddAddress onCloseModal={onCloseModal} />,
      {
        initialState: generateState({ openModal: 'EDIT' })
      }
    );

    const queryClose = queryByClass(
      wrapper.baseElement as HTMLElement,
      /icon icon-close/
    );

    queryClose!.click();
    expect(confirmModalActionsOpen).not.toHaveBeenCalled();
    expect(onCloseModal).toHaveBeenCalled();
  });

  it('Should have click button close', () => {
    const onCloseModal = jest.fn();
    const confirmModalActionsOpen = spyAction('open');
    renderMockStoreId(<AddAddress onCloseModal={onCloseModal} />, {
      initialState: generateState({ openModal: 'EDIT' })
    });

    const queryClose = screen.queryByText(/txt_cancel/);
    queryClose!.click();
    expect(confirmModalActionsOpen).not.toHaveBeenCalled();
    expect(onCloseModal).toHaveBeenCalled();
  });

  it('Should have render form add', () => {
    const onCloseModal = jest.fn();
    const { store } = renderMockStoreId(
      <AddAddress onCloseModal={onCloseModal} />,
      {
        initialState: generateState({})
      }
    );
    const values = store.getState().form[nameForm].values;
    expect(values).toEqual({
      addressRelationship: {
        description: 'U - Unknown',
        fieldID: 'U',
        fieldValue: 'Unknown'
      }
    });
  });

  it('Should have render form edit with type Permanent', () => {
    jest.useFakeTimers();
    const onCloseModal = jest.fn();
    const { store } = renderMockStoreId(
      <AddAddress onCloseModal={onCloseModal} />,
      {
        initialState: generateState({
          form,
          openModal: 'EDIT',
          addressType: addressInfo
        })
      }
    );
    jest.runAllTimers();
    const values = store.getState().form[nameForm].values;
    expect(values).toEqual({
      ...addressInfo,
      effectiveFrom: formatTimeDefault(
        addressInfo.effectiveFrom,
        FormatTime.Date
      ),
      effectiveTo: formatTimeDefault(addressInfo.effectiveTo, FormatTime.Date)
    });
  });

  it('Should have render form edit with helperHandleEffectiveTo', () => {
    jest.useFakeTimers();
    const onCloseModal = jest.fn();
    const { store } = renderMockStoreId(
      <AddAddress onCloseModal={onCloseModal} />,
      {
        initialState: generateState({
          addressTypes: [addressInfo, addressInfo],
          openModal: 'EDIT',
          addressType: {
            ...addressInfo,
            addressCategory: {
              fieldValue: 'Plastic',
              fieldID: 'PLST',
              description: 'Plastic - Billing'
            }
          }
        })
      }
    );
    jest.runAllTimers();
    const values = store.getState().form[nameForm].values;
    expect(values).toEqual({
      ...addressInfo,
      addressCategory: {
        fieldValue: 'Plastic',
        fieldID: 'PLST',
        description: 'Plastic - Billing'
      },
      effectiveFrom: formatTimeDefault(
        addressInfo.effectiveFrom,
        FormatTime.Date
      ),
      effectiveTo: formatTimeDefault(addressInfo.effectiveTo, FormatTime.Date)
    });
  });

  it('Should have render form edit with type is Permanent', () => {
    const onCloseModal = jest.fn();
    const { store } = renderMockStoreId(
      <AddAddress onCloseModal={onCloseModal} />,
      {
        initialState: generateState({
          form,
          openModal: 'EDIT',
          addressTypes: [addressInfo, { ...addressInfo, id: '1' }],
          addressType: addressInfo
        })
      }
    );
    const values = store.getState().form[nameForm].values;
    expect(values).toEqual({
      ...addressInfo,
      effectiveFrom: formatTimeDefault(
        addressInfo.effectiveFrom,
        FormatTime.Date
      ),
      effectiveTo: formatTimeDefault(addressInfo.effectiveTo, FormatTime.Date)
    });
  });

  it('Should have render if (idAddressCategory === PERMANENT)', () => {
    const onCloseModal = jest.fn();
    const { store } = renderMockStoreId(
      <AddAddress onCloseModal={onCloseModal} />,
      {
        initialState: generateState({
          form,
          openModal: 'EDIT',
          addressTypes: [
            addressInfo,
            { ...addressInfo, id: '1' },
            { ...addressInfo, id: '2' }
          ],
          addressType: addressInfo
        })
      }
    );
    const values = store.getState().form[nameForm].values;
    expect(values).toEqual({
      ...addressInfo,
      effectiveFrom: formatTimeDefault(
        addressInfo.effectiveFrom,
        FormatTime.Date
      ),
      effectiveTo: formatTimeDefault(addressInfo.effectiveTo, FormatTime.Date)
    });
  });

  it('Should have openModal === EDIT (idAddressCategory !== PERMANENT) (effectiveFromInForm && isArrayHasValue(reduceAddress)) (addressGreaterDateFrom)', () => {
    const onCloseModal = jest.fn();
    const { store } = renderMockStoreId(
      <AddAddress onCloseModal={onCloseModal} />,
      {
        initialState: generateState({
          form: {
            [nameForm]: {
              values: {
                ...addressInfo,
                ...addressCategoryPlastic
              }
            }
          },
          openModal: 'EDIT',
          addressTypes: [
            addressInfo,
            {
              ...addressInfo,
              ...addressCategoryPlastic,
              effectiveFrom: '2021-06-18',
              id: '1'
            },
            {
              ...addressInfo,
              ...addressCategoryPlastic,
              id: '2'
            }
          ],
          addressType: {
            ...addressInfo,
            ...addressCategoryPlastic
          }
        })
      }
    );
    const values = store.getState().form[nameForm].values;
    expect(values).toEqual({
      ...addressInfo,
      addressCategory: {
        description: 'Plastic - Billing',
        fieldID: 'PLST',
        fieldValue: 'Plastic'
      },
      effectiveFrom: formatTimeDefault(
        addressInfo.effectiveFrom,
        FormatTime.Date
      ),
      effectiveTo: formatTimeDefault(addressInfo.effectiveTo, FormatTime.Date)
    });
  });

  it('Should have openModal === ADD (idAddressCategory !== PERMANENT) !(effectiveFromInForm && isArrayHasValue(reduceAddress)) (addressGreaterDateFrom) (formatDate < effectiveFromInFormFormat) !(formatDate < effectiveFromInFormFormat)', () => {
    const onCloseModal = jest.fn();
    const { store } = renderMockStoreId(
      <AddAddress onCloseModal={onCloseModal} />,
      {
        initialState: generateState({
          form: {
            [nameForm]: {
              values: {
                addressType: {
                  fieldValue: 'Billing',
                  fieldID: 'BLL1',
                  description: 'BLL1 - Billing'
                },
                ...addressCategoryPlastic,
                effectiveFrom: '2021-05-18'
              }
            }
          },
          addressTypes: [
            addressInfo,
            {
              ...addressInfo,
              ...addressCategoryPlastic,
              effectiveFrom: '2021-06-18',
              effectiveTo: '2021-07-18',
              id: '1'
            },
            {
              ...addressInfo,
              ...addressCategoryPlastic,
              effectiveFrom: '2021-04-18',
              effectiveTo: '2021-05-17',
              id: '2'
            }
          ],
          addressType: {
            ...addressInfo,
            ...addressCategoryPlastic
          }
        })
      }
    );
    const values = store.getState().form[nameForm].values;
    expect(values).toEqual({
      addressRelationship: {
        description: 'U - Unknown',
        fieldID: 'U',
        fieldValue: 'Unknown'
      }
    });
  });

  it('Should have openModal === ADD (idAddressCategory !== PERMANENT) !(effectiveFromInForm && isArrayHasValue(reduceAddress)) (addressGreaterDateFrom) (formatDate < effectiveFromInFormFormat) !(formatDate < effectiveFromInFormFormat)', () => {
    const onCloseModal = jest.fn();
    const { store } = renderMockStoreId(
      <AddAddress onCloseModal={onCloseModal} />,
      {
        initialState: generateState({
          form: {
            [nameForm]: {
              values: {
                addressType: {
                  fieldValue: 'Billing',
                  fieldID: 'BLL1',
                  description: 'BLL1 - Billing'
                },
                ...addressCategoryPlastic
              }
            }
          },
          addressTypes: [
            addressInfo,
            {
              ...addressInfo,
              ...addressCategoryPlastic,
              effectiveFrom: '2021-06-18',
              effectiveTo: '2021-07-18',
              id: '1'
            },
            {
              ...addressInfo,
              ...addressCategoryPlastic,
              effectiveFrom: '2021-04-18',
              effectiveTo: '2021-05-17',
              id: '2'
            }
          ],
          addressType: {
            ...addressInfo,
            ...addressCategoryPlastic
          }
        })
      }
    );
    const values = store.getState().form[nameForm].values;
    expect(values).toEqual({
      addressRelationship: {
        description: 'U - Unknown',
        fieldID: 'U',
        fieldValue: 'Unknown'
      }
    });
  });

  it('Should have (idAddressCategory !== PERMANENT) !(effectiveFromInForm && isArrayHasValue(reduceAddress))', () => {
    const onCloseModal = jest.fn();
    const { store } = renderMockStoreId(
      <AddAddress onCloseModal={onCloseModal} />,
      {
        initialState: generateState({
          form: {
            [nameForm]: {
              values: {
                ...addressInfo,
                ...addressCategoryPlastic
              }
            }
          },
          openModal: 'EDIT',
          addressTypes: [
            addressInfo,
            {
              ...addressInfo,
              ...addressCategoryPlastic,
              id: '1'
            },
            {
              ...addressInfo,
              ...addressCategoryPlastic,
              id: '2'
            }
          ],
          addressType: {
            ...addressInfo,
            ...addressCategoryPlastic
          }
        })
      }
    );
    const values = store.getState().form[nameForm].values;
    expect(values).toEqual({
      ...addressInfo,
      addressCategory: {
        description: 'Plastic - Billing',
        fieldID: 'PLST',
        fieldValue: 'Plastic'
      },
      effectiveFrom: formatTimeDefault(
        addressInfo.effectiveFrom,
        FormatTime.Date
      ),
      effectiveTo: formatTimeDefault(addressInfo.effectiveTo, FormatTime.Date)
    });
  });

  it('Should have modal confirm', () => {
    jest.spyOn(reduxForm, 'isDirty').mockImplementation(() => () => true);
    const onCloseModal = () => {};
    const confirmModalActionsOpen = spyAction('open');
    renderMockStoreId(<AddAddress onCloseModal={onCloseModal} />, {
      initialState: generateState({
        openModal: 'ADD',
        addressTypes: [addressInfo],
        addressType: addressInfo
      })
    });

    const queryTextCancel = screen.queryByText('txt_cancel');
    queryTextCancel!.click();
    expect(confirmModalActionsOpen).toHaveBeenCalledWith({
      title: 'txt_unsaved_changes',
      body: 'txt_discard_change_body',
      cancelText: 'txt_continue_editing',
      confirmText: 'txt_discard_change',
      confirmCallback: onCloseModal,
      variant: 'danger'
    });
    jest.resetAllMocks();
    jest.clearAllMocks();
  });
});
