import React from 'react';
import { screen } from '@testing-library/react';

// Component
import Footer from './Footer';

// Helper
import {
  mockActionCreator,
  storeId,
  renderMockStoreId,
  queryByClass
} from 'app/test-utils';

// redux
import {
  cardholderMaintenanceActions,
  defaultStateCardholder
} from '../../_redux';

// constant
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

import * as reduxForm from 'redux-form';

const initialState: Partial<RootState> = {
  form: {
    [`${storeId}-addExpandedAddressView`]: {
      values: {
        POBox: null,
        addressCategory: {
          fieldID: 'R',
          fieldValue: 'Repeating',
          description: 'R - Repeating'
        },
        addressLineFour: null,
        addressLineOne: '123213213',
        addressLineThree: null,
        addressRelationship: {
          description: 'U - Unknown',
          fieldID: 'U',
          fieldValue: 'Unknown'
        },
        addressType: {
          fieldID: 'BLL1',
          fieldValue: 'Billing',
          description: 'BLL1 - Billing'
        },
        cityName: '3123213',
        countryISO: {
          fieldID: 'USA',
          fieldValue: 'United States of America',
          description: 'USA - United States of America'
        },
        effectiveFrom:
          'Fri May 21 2021 00:00:00 GMT-0500 (Central Daylight Time)',
        effectiveTo:
          'Sat May 29 2021 00:00:00 GMT-0500 (Central Daylight Time)',
        houseBuilding: null,
        houseNumber: null,
        postalCode: '21321',
        stateSubdivision: {
          fieldID: 'AA',
          fieldValue: 'APO/FPO Florida',
          description: 'AA - APO/FPO Florida'
        },
        streetName: null
      }
    } as any
  },
  cardholderMaintenance: {
    [storeId]: {
      ...defaultStateCardholder,
      expandedAddress: {
        histories: [],
        openModal: 'EDIT',
        isLoading: true,
        isEndLoadMore: true
      },
      isLoading: false
    }
  }
};

const cardHolderSpy = mockActionCreator(cardholderMaintenanceActions);

describe('Test Footer AddAndUpdate ExpandedAddress', () => {
  it('Should have Render UI > Save', () => {
    renderMockStoreId(<Footer onCloseModal={() => undefined} />, {
      initialState
    });
    expect(screen.getByText(I18N_COMMON_TEXT.SAVE)).toBeInTheDocument();
  });

  it('Should have Render UI > Add', () => {
    renderMockStoreId(<Footer onCloseModal={() => undefined} />, {
      initialState: {
        ...initialState,
        cardholderMaintenance: {
          [storeId]: {
            ...initialState.cardholderMaintenance![storeId],
            expandedAddress: {
              histories: [],
              openModal: 'ADD',
              isLoading: true,
              isEndLoadMore: true
            }
          }
        }
      } as Partial<RootState>
    });

    expect(screen.getByText(I18N_COMMON_TEXT.ADD)).toBeInTheDocument();
  });

  it('Should have submit form', () => {
    const mockAction = cardHolderSpy('addUpdateExpandedAddress');

    renderMockStoreId(<Footer onCloseModal={() => undefined} />, {
      initialState
    });

    screen.getByText(I18N_COMMON_TEXT.SAVE).click();

    expect(mockAction).toBeCalledWith({
      storeId,
      postData: { accountId: 'accEValue' }
    });
  });

  it('Should have invalid form', () => {
    jest.spyOn(reduxForm, 'isValid').mockImplementation(() => () => false);
    const { wrapper } = renderMockStoreId(
      <Footer onCloseModal={() => undefined} />,
      {
        initialState
      }
    );
    const queryButtonSave = queryByClass(wrapper.container, /btn btn-primary/);
    expect(queryButtonSave).toHaveAttribute('disabled');
  });
});
