import React from 'react';
import Header from './Header';
import { screen } from '@testing-library/react';
import { storeId, renderMockStoreId, queryByClass } from 'app/test-utils';
import { defaultStateCardholder } from '../../_redux';
import { I18N_CARD_MAINTENANCE } from 'app/constants/i18n';
const generateState = (openModal = 'EDIT') => {
  return {
    cardholderMaintenance: {
      [storeId]: {
        ...defaultStateCardholder,
        expandedAddress: {
          histories: [],
          openModal,
          isLoading: true,
          isEndLoadMore: true
        },
        isLoading: false
      }
    }
  } as Partial<RootState>;
};

describe('Test Header AddAndUpdate ExpandedAddress', () => {
  it('Should have Render Modal Edit', () => {
    renderMockStoreId(<Header onCloseModal={() => undefined} />, {
      initialState: generateState()
    });
    const queryTextEdit = screen.getByText(I18N_CARD_MAINTENANCE.EDIT_ADDRESS);
    expect(queryTextEdit).toBeInTheDocument();
  });

  it('Should have Render Add Edit', () => {
    renderMockStoreId(<Header onCloseModal={() => undefined} />, {
      initialState: generateState('ADD')
    });
    const queryTextAdd = screen.getByText(I18N_CARD_MAINTENANCE.ADD_ADDRESS);
    expect(queryTextAdd).toBeInTheDocument();
  });

  it('Should have onCloseModal', () => {
    const onCloseModal = jest.fn();
    const { wrapper } = renderMockStoreId(
      <Header onCloseModal={onCloseModal} />,
      {
        initialState: generateState('ADD')
      }
    );
    const queryClass = queryByClass(
      wrapper.baseElement as HTMLElement,
      /icon icon-close/
    );
    queryClass!.click();
    expect(onCloseModal).toHaveBeenCalled();
  });
});
