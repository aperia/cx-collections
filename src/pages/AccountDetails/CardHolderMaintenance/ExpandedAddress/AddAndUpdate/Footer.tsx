import React from 'react';

// components
import { ModalFooter } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'react-i18next';
import {
  useAccountDetail,
  useIsDirtyForm,
  useStoreIdSelector
} from 'app/hooks';
import { useDispatch, useSelector } from 'react-redux';

// i18n
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { isValid } from 'redux-form';

// constants
import { MODAL_ADDRESS_STANDARD, VIEW_NAME_ADDRESS } from '../constants';
import { ModalExpandedAddress } from '../../types';
import { selectModalExpandedAddress } from '../../_redux/selector';
import { cardholderMaintenanceActions } from '../../_redux';

export interface FooterAddAddressProps {
  onCloseModal: () => void;
}

const FooterAddAddress: React.FC<FooterAddAddressProps> = ({
  onCloseModal
}) => {
  const { ADD_ADDRESS } = VIEW_NAME_ADDRESS;
  const { SAVE, ADD, CANCEL } = I18N_COMMON_TEXT;
  const { storeId, accEValue } = useAccountDetail();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const isFormValid = useSelector(isValid(`${storeId}-${ADD_ADDRESS}`));
  const isDirtyForm = useIsDirtyForm([`${storeId}-${ADD_ADDRESS}`]);

  const openModal = useStoreIdSelector<ModalExpandedAddress>(
    selectModalExpandedAddress
  );

  const isDisable = openModal === MODAL_ADDRESS_STANDARD.EDIT && !isDirtyForm;

  const title = openModal === MODAL_ADDRESS_STANDARD.ADD ? ADD : SAVE;

  const handleSave = () => {
    dispatch(
      cardholderMaintenanceActions.addUpdateExpandedAddress({
        storeId,
        postData: { accountId: accEValue }
      })
    );
  };

  return (
    <ModalFooter
      okButtonText={t(title)}
      cancelButtonText={t(CANCEL)}
      onCancel={onCloseModal}
      disabledOk={!isFormValid || isDisable}
      onOk={handleSave}
      dataTestId="infobar_cardholder_expanded-address_add-modal-footer"
    />
  );
};

export default FooterAddAddress;
