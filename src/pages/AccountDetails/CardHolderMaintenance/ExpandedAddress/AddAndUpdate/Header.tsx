import React from 'react';

// components
import { ModalTitle, ModalHeader } from 'app/_libraries/_dls/components';

// constants
import { ModalExpandedAddress } from '../../types';

// hooks
import { useStoreIdSelector } from 'app/hooks';

// redux
import { selectModalExpandedAddress } from '../../_redux/selector';

// i18n
import { useTranslation } from 'react-i18next';
import { I18N_CARD_MAINTENANCE } from 'app/constants/i18n';
import { MODAL_ADDRESS_STANDARD } from '../constants';

export interface HeaderAddAddressProps {
  onCloseModal: () => void;
}

const HeaderAddAddress: React.FC<HeaderAddAddressProps> = ({
  onCloseModal
}) => {
  const { t } = useTranslation();
  const { EDIT_ADDRESS, ADD_ADDRESS } = I18N_CARD_MAINTENANCE;
  const openModal = useStoreIdSelector<ModalExpandedAddress>(
    selectModalExpandedAddress
  );
  const title =
    openModal === MODAL_ADDRESS_STANDARD.ADD ? ADD_ADDRESS : EDIT_ADDRESS;
  return (
    <ModalHeader
      border
      closeButton
      onHide={onCloseModal}
      dataTestId="cardholder_expanded-address_add-modal-header"
    >
      <ModalTitle dataTestId="cardholder_expanded-address_add-modal-header-title">
        {t(title)}
      </ModalTitle>
    </ModalHeader>
  );
};

export default HeaderAddAddress;
