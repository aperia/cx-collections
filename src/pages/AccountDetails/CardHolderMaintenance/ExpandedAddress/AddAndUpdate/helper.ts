import { formatTimeDefault } from 'app/helpers';
import { FormatTime } from 'app/constants/enums';

export const mapDataToView = (data: MagicKeyValue) => {
  return {
    ...data,
    effectiveFrom: formatTimeDefault(data?.effectiveFrom, FormatTime.Date),
    effectiveTo: formatTimeDefault(data?.effectiveTo, FormatTime.Date)
  };
};
