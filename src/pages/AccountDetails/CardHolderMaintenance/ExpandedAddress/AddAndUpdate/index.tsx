import React, { Fragment, useEffect, useMemo, useRef } from 'react';

// components
import { Modal } from 'app/_libraries/_dls/components';
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';
import FooterAddAddress from './Footer';
import HeaderAddAddress from './Header';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// types
import { IAddressInfo, IDisableRange, ModalExpandedAddress } from '../../types';

// redux
import {
  isLoadingExpandedAddress,
  selectAddressCategoryInForm,
  selectAddressTypeInForm,
  selectCardholderAddressInfo,
  selectCardholderAddressTypes,
  selectCountryISOInForm,
  selectEffectiveFromInForm,
  selectEffectiveToInForm,
  selectModalExpandedAddress
} from '../../_redux/selector';

// constants
import {
  ID_ADDRESS_TYPE,
  MODAL_ADDRESS_STANDARD,
  VIEW_NAME_ADDRESS
} from '../constants';
import { View } from 'app/_libraries/_dof/core';

// i18n
import { useTranslation } from 'react-i18next';
import {
  handleOverlapPermanent,
  helperDisabledDefault,
  helperDisabledWithValueFormat,
  helperGetSubdivision,
  helperHandleCalculateDate,
  helperHandleDisableRange,
  helperHandleEffectiveTo
} from '../helper';

import { formatTimeDefault, isArrayHasValue } from 'app/helpers';
import { FormatTime } from 'app/constants/enums';
import { I18N_CARDHOLDER_MAINTENANCE } from '../../constants';
import isEmpty from 'lodash.isempty';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import { useDispatch, useSelector } from 'react-redux';
import { isDirty } from 'redux-form';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { mapDataToView } from './helper';
export interface AddAddressProps {
  onCloseModal: () => void;
}

const AddAddress: React.FC<AddAddressProps> = ({ onCloseModal }) => {
  const ref = useRef<any>();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { ADD_ADDRESS } = VIEW_NAME_ADDRESS;
  const { PERMANENT } = ID_ADDRESS_TYPE;
  const { EDIT } = MODAL_ADDRESS_STANDARD;
  const { storeId } = useAccountDetail();
  const title = t(I18N_CARDHOLDER_MAINTENANCE.ADDRESS_DETAILS);

  const openModal = useStoreIdSelector<ModalExpandedAddress>(
    selectModalExpandedAddress
  );

  const isLoading = useStoreIdSelector<boolean>(isLoadingExpandedAddress);

  const addressTypes = useStoreIdSelector<IAddressInfo[]>(
    selectCardholderAddressTypes
  );

  const effectiveFromInForm = useStoreIdSelector<Date | string>(
    selectEffectiveFromInForm
  );

  const effectiveToInForm = useStoreIdSelector<Date | string>(
    selectEffectiveToInForm
  );

  const idAddressCategory = useStoreIdSelector<string>(
    selectAddressCategoryInForm
  );

  const idCountryISO = useStoreIdSelector<string>(selectCountryISOInForm);

  const idAddressType = useStoreIdSelector<string>(selectAddressTypeInForm);

  const data = useStoreIdSelector<IAddressInfo>(selectCardholderAddressInfo);

  const isFormDirty = useSelector(isDirty(`${storeId}-${ADD_ADDRESS}`));

  useEffect(() => {
    if (!isArrayHasValue(addressTypes)) return;
    if (!idAddressCategory && !effectiveFromInForm) return;
    let filterAddress: IAddressInfo[] = [];
    if (openModal === EDIT) {
      filterAddress = addressTypes.filter(item => {
        return (
          item.addressCategory?.fieldID === idAddressCategory &&
          item.addressType?.fieldID === idAddressType &&
          item.id !== data.id
        );
      });
    } else {
      filterAddress = addressTypes.filter(item => {
        return (
          item.addressCategory?.fieldID === idAddressCategory &&
          item.addressType?.fieldID === idAddressType
        );
      });
    }

    if (!isArrayHasValue(filterAddress)) {
      return helperHandleDisableRange(ref, [], [], idAddressCategory);
    }

    if (idAddressCategory === PERMANENT) {
      return handleOverlapPermanent(filterAddress, ref);
    }

    const reduceAddress = filterAddress.reduce((pre, current) => {
      return [
        ...pre,
        {
          fromDate: current.effectiveFrom!,
          toDate: current.effectiveTo!
        }
      ];
    }, [] as IDisableRange[]);

    const effectiveFromInFormFormat = formatTimeDefault(
      effectiveFromInForm?.toString(),
      FormatTime.YearMonthDay
    )!;

    let dateRangeDisableFrom = reduceAddress;
    let dateRangeDisableTo = reduceAddress;

    if (effectiveFromInForm && isArrayHasValue(reduceAddress)) {
      // take range data Greater EffectiveFrom
      const addressGreaterDateFrom = filterAddress.find(item => {
        const formatDate = formatTimeDefault(
          item.effectiveFrom!.toString(),
          FormatTime.YearMonthDay
        )!;
        return formatDate > effectiveFromInFormFormat;
      });

      // take range data Less EffectiveFrom
      if (addressGreaterDateFrom) {
        const addressLessDateFrom = filterAddress.reduce((pre, current) => {
          const formatDate = formatTimeDefault(
            current.effectiveTo!.toString(),
            FormatTime.YearMonthDay
          )!;
          if (formatDate < effectiveFromInFormFormat) {
            return [
              ...pre,
              {
                fromDate: current.effectiveFrom!,
                toDate: current.effectiveTo!
              }
            ];
          }
          return pre;
        }, [] as IDisableRange[]);

        dateRangeDisableFrom = reduceAddress;
        dateRangeDisableTo = [
          ...addressLessDateFrom,
          {
            fromDate: addressGreaterDateFrom.effectiveFrom!,
            toDate: '9999/12/31'
          }
        ];
      }
    }

    helperHandleDisableRange(
      ref,
      dateRangeDisableFrom,
      dateRangeDisableTo,
      idAddressCategory
    );
  }, [
    openModal,
    EDIT,
    idAddressCategory,
    effectiveFromInForm,
    effectiveToInForm,
    addressTypes,
    data.id,
    PERMANENT,
    idAddressType
  ]);

  useEffect(() => {
    if (!idCountryISO) return;
    setTimeout(() => {
      helperDisabledWithValueFormat(ref, idCountryISO, data);
    }, 0);
  }, [idCountryISO, data]);

  useEffect(() => {
    if (openModal !== EDIT || isEmpty(data)) return;
    setTimeout(() => {
      if (data.addressCategory.fieldID === PERMANENT) {
        helperHandleEffectiveTo(ref, data.addressCategory.fieldID);
      }
      helperGetSubdivision(ref, data.countryISO.fieldID);
      helperHandleCalculateDate(ref, data);
      helperDisabledDefault(ref, data.countryISO.fieldID);
    }, 0);
  }, [openModal, EDIT, data, PERMANENT]);

  const handleInvokeCancel = () => {
    if (openModal !== EDIT && isFormDirty) {
      return dispatch(
        confirmModalActions.open({
          title: 'txt_unsaved_changes',
          body: 'txt_discard_change_body',
          cancelText: 'txt_continue_editing',
          confirmText: 'txt_discard_change',
          confirmCallback: onCloseModal,
          variant: 'danger'
        })
      );
    }
    onCloseModal();
  };

  const renderView = useMemo(() => {
    let dataView: MagicKeyValue = {
      addressRelationship: {
        description: 'U - Unknown',
        fieldID: 'U',
        fieldValue: 'Unknown'
      }
    };

    dataView = mapDataToView(openModal === EDIT ? data : dataView);
    return (
      <Fragment>
        <h5
          className="mb-16"
          data-testid={genAmtId(
            'cardholder_expanded-address_add-modal',
            'body_title',
            ''
          )}
        >
          {title}
        </h5>
        <View
          id={`${storeId}-${ADD_ADDRESS}`}
          formKey={`${storeId}-${ADD_ADDRESS}`}
          descriptor={ADD_ADDRESS}
          ref={ref}
          value={dataView}
        />
      </Fragment>
    );
  }, [ADD_ADDRESS, title, EDIT, data, openModal, storeId]);

  return (
    <Modal
      md
      loading={isLoading}
      show={true}
      enforceFocus={false}
      dataTestId="cardholder_expanded-address_add-modal"
    >
      <HeaderAddAddress onCloseModal={handleInvokeCancel} />
      <ModalBodyWithApiError
        storeId={storeId}
        apiErrorClassName="mb-24"
        dataTestId="infobar_cardholder_expanded-address_add-modal-body"
      >
        {renderView}
      </ModalBodyWithApiError>
      <FooterAddAddress onCloseModal={handleInvokeCancel} />
    </Modal>
  );
};

export default AddAddress;
