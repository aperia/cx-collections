import React from 'react';
import { screen, act, waitFor } from '@testing-library/react';

// Component
import ExpandedAddress from '.';

// Helper
import {
  storeId,
  renderMockStoreId,
  mockActionCreator,
  queryByClass,
  queryAllByClass
} from 'app/test-utils';

// redux
import {
  cardholderMaintenanceActions,
  defaultStateCardholder
} from '../_redux';

// constant
import { IAddressInfo } from '../types';
import userEvent from '@testing-library/user-event';

import * as entitlementsHelpers from 'app/entitlements/helpers';
import { PERMISSIONS } from 'app/entitlements/constants';

HTMLCanvasElement.prototype.getContext = jest.fn();

beforeEach(() => {
  jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
});

const addressInfo = {
  POBox: '',
  addressCategory: {
    fieldValue: 'Permanent',
    fieldID: 'P',
    description: 'P - Permanent'
  },
  addressLineFour: '',
  addressLineOne: '333 MENLAY',
  addressLineThree: '',
  addressLineTwo: '',
  addressRelationship: {
    fieldValue: 'Unknown',
    fieldID: 'U',
    description: 'U - Unknown'
  },
  addressType: {
    fieldValue: 'Billing',
    fieldID: 'BLL1',
    description: 'BLL1 - Billing'
  },
  attention: '',
  cityName: 'OMAHA',
  countryISO: {
    fieldValue: 'United States of America',
    fieldID: 'USA',
    description: 'USA - United States of America'
  },
  effectiveFrom: '2021-05-18',
  effectiveTo: '9999-12-31',
  formattedAddressCode: 'F',
  houseBuilding: '',
  houseNumber: '',
  id: '00001-P-BLL1-2021-06-18-9999-12-31',
  memberSequence: '00001',
  postalCode: '10003',
  stateSubdivision: {
    fieldValue: 'New York',
    fieldID: 'NY',
    description: 'NY - New York'
  },
  streetName: ''
} as IAddressInfo;

const mockAction = mockActionCreator(cardholderMaintenanceActions);

const generateState = ({
  openModal = 'EDIT',
  addressType = {},
  addressTypes = []
}: {
  openModal?: string;
  addressType?: {};
  addressTypes?: IAddressInfo[];
}) => {
  return {
    cardholderMaintenance: {
      [storeId]: {
        ...defaultStateCardholder,
        expandedAddress: {
          histories: [],
          openModal,
          isLoading: true,
          isEndLoadMore: true
        },
        addressTypes,
        addressType: {
          ...addressType
        } as IAddressInfo,
        isLoading: false
      }
    }
  } as Partial<RootState>;
};

describe('Test ExpandedAddress', () => {
  it('Should have Render without modal', () => {
    waitFor(() => {
      jest.useFakeTimers();
      const { wrapper } = renderMockStoreId(<ExpandedAddress />, {
        initialState: generateState({ openModal: '' })
      });
      jest.runAllTimers();
      const queryModal = queryByClass(
        wrapper.baseElement as HTMLElement,
        /modal-content/
      );
      expect(queryModal).not.toBeInTheDocument();
    });
  });

  it('Should have open modal delete', () => {
    waitFor(() => {
      jest.useFakeTimers();
      const changeModalAddEditHistoryAddress = mockAction(
        'changeModalAddEditHistoryAddress'
      );

      const { wrapper } = renderMockStoreId(<ExpandedAddress />, {
        initialState: generateState({
          openModal: '',
          addressTypes: [addressInfo],
          addressType: {
            ...addressInfo,
            addressType: {
              fieldValue: 'Plastic',
              fieldID: 'PLTS',
              description: 'PLTS - Plastic'
            }
          }
        })
      });
      jest.runAllTimers();
      const queryButtonMore = queryAllByClass(
        wrapper.container as HTMLElement,
        'icon icon-more-vertical'
      );

      act(() => {
        userEvent.click(queryButtonMore[1]);
      });

      const queryTextDelete = screen.queryByText('txt_delete_address');

      queryTextDelete!.click();

      expect(changeModalAddEditHistoryAddress).toHaveBeenCalledWith({
        storeId,
        type: 'DELETE'
      });
    });
  });

  it("Should haven't open modal delete", () => {
    waitFor(() => {
      jest.useFakeTimers();
      const changeModalAddEditHistoryAddress = mockAction(
        'changeModalAddEditHistoryAddress'
      );

      const { wrapper } = renderMockStoreId(<ExpandedAddress />, {
        initialState: generateState({
          openModal: '',
          addressType: addressInfo,
          addressTypes: [addressInfo]
        })
      });
      jest.runAllTimers();
      const queryButtonMore = queryAllByClass(
        wrapper.container as HTMLElement,
        'icon icon-more-vertical'
      );

      act(() => {
        userEvent.click(queryButtonMore[1]);
      });

      const queryTextDelete = screen.queryByText('txt_delete_address');

      queryTextDelete!.click();

      expect(changeModalAddEditHistoryAddress).not.toHaveBeenCalledWith({
        storeId,
        type: 'DELETE'
      });
    });
  });

  it('Should have open modal edit', () => {
    waitFor(() => {
      jest.useFakeTimers();
      const changeModalAddEditHistoryAddress = mockAction(
        'changeModalAddEditHistoryAddress'
      );
      const { wrapper } = renderMockStoreId(<ExpandedAddress />, {
        initialState: generateState({
          openModal: '',
          addressType: addressInfo,
          addressTypes: [addressInfo]
        })
      });
      const queryButtonMore = queryAllByClass(
        wrapper.container as HTMLElement,
        'icon icon-more-vertical'
      );
      jest.runAllTimers();

      act(() => {
        userEvent.click(queryButtonMore[1]);
      });

      const queryTextEdit = screen.queryByText('txt_edit_address');

      queryTextEdit!.click();

      expect(changeModalAddEditHistoryAddress).toHaveBeenCalledWith({
        storeId,
        type: 'EDIT'
      });
    });
  });

  it('Should have open modal history', () => {
    waitFor(() => {
      jest.useFakeTimers();
      const changeModalAddEditHistoryAddress = mockAction(
        'changeModalAddEditHistoryAddress'
      );
      const { wrapper } = renderMockStoreId(<ExpandedAddress />, {
        initialState: generateState({ openModal: '', addressType: addressInfo })
      });
      const queryButtonMore = queryAllByClass(
        wrapper.container as HTMLElement,
        'icon icon-more-vertical'
      );
      jest.runAllTimers();
      act(() => {
        userEvent.click(queryButtonMore[0]);
      });

      const queryTextHistory = screen.queryByText('txt_view_address_history');

      queryTextHistory!.click();

      expect(changeModalAddEditHistoryAddress).toHaveBeenCalledWith({
        storeId,
        type: 'HISTORY'
      });
    });
  });

  it('Should have open modal add', () => {
    waitFor(() => {
      jest.useFakeTimers();
      const changeModalAddEditHistoryAddress = mockAction(
        'changeModalAddEditHistoryAddress'
      );
      const { wrapper } = renderMockStoreId(<ExpandedAddress />, {
        initialState: generateState({
          openModal: '',
          addressType: addressInfo,
          addressTypes: [addressInfo] as IAddressInfo[]
        })
      });
      const queryButtonMore = queryAllByClass(
        wrapper.container as HTMLElement,
        'icon icon-more-vertical'
      );

      jest.runAllTimers();
      act(() => {
        userEvent.click(queryButtonMore[0]);
      });

      const queryTextAddAddress = screen.queryByText('txt_add_address');

      queryTextAddAddress!.click();

      expect(changeModalAddEditHistoryAddress).toHaveBeenCalledWith({
        storeId,
        type: 'ADD'
      });
    });
  });

  it('Should have render without data', () => {
    waitFor(() => {
      renderMockStoreId(<ExpandedAddress />, {
        initialState: generateState({ openModal: '' })
      });
      const queryTextNoData = screen.getByText('txt_no_data');
      expect(queryTextNoData).toBeInTheDocument();
    });
  });

  it('Should have Render with click toggle', () => {
    waitFor(() => {
      const changeExpand = mockAction('changeExpand');
      const { wrapper } = renderMockStoreId(<ExpandedAddress />, {
        initialState: generateState({})
      });
      const queryClassIconMinus = queryByClass(
        wrapper.baseElement as HTMLElement,
        /icon icon-plus/
      );
      queryClassIconMinus!.click();
      expect(changeExpand).toHaveBeenCalledWith({
        expandState: 'isExpandAddress',
        storeId
      });
    });
  });

  it('Should have Render with click icon close Modal', () => {
    waitFor(() => {
      const changeModalAddEditHistoryAddress = mockAction(
        'changeModalAddEditHistoryAddress'
      );
      const { wrapper } = renderMockStoreId(<ExpandedAddress />, {
        initialState: generateState({})
      });
      const queryClassIconClose = queryByClass(
        wrapper.baseElement as HTMLElement,
        /icon icon-close/
      );
      queryClassIconClose!.click();
      expect(changeModalAddEditHistoryAddress).toHaveBeenCalledWith({
        storeId,
        type: ''
      });
    });
  });

  it('Should have Render with click button close Modal', () => {
    waitFor(() => {
      const changeModalAddEditHistoryAddress = mockAction(
        'changeModalAddEditHistoryAddress'
      );
      renderMockStoreId(<ExpandedAddress />, {
        initialState: generateState({})
      });
      const queryClassButtonCancel = screen.queryByText('txt_cancel');
      queryClassButtonCancel!.click();
      expect(changeModalAddEditHistoryAddress).toHaveBeenCalledWith({
        storeId,
        type: ''
      });
    });
  });

  it('Should have Render with modal Edit', () => {
    renderMockStoreId(<ExpandedAddress />, {
      initialState: generateState({})
    });
    const queryTextEdit = screen.getByText('txt_no_data');
    expect(queryTextEdit).toBeInTheDocument();
  });

  it('Should have Render with modal Add', () => {
    renderMockStoreId(<ExpandedAddress />, {
      initialState: generateState({ openModal: 'ADD' })
    });
    const queryTextAdd = screen.getByText('txt_no_data');
    expect(queryTextAdd).toBeInTheDocument();
  });

  it('Should have Render with modal History', () => {
    renderMockStoreId(<ExpandedAddress />, {
      initialState: generateState({ openModal: 'HISTORY' })
    });
    const queryTextHistory = screen.getByText('txt_no_data');
    expect(queryTextHistory).toBeInTheDocument();
  });

  it('Should have Render with modal DELETE', () => {
    renderMockStoreId(<ExpandedAddress />, {
      initialState: generateState({ openModal: 'DELETE' })
    });
    const queryTextDelete = screen.getByText('txt_no_data');
    waitFor(() => {
      expect(queryTextDelete).toBeInTheDocument();
    });
  });

  it('Should have Render with modal EDIT when click more actions and choose edit option', () => {
    const changeModalAddEditHistoryAddress = mockAction(
      'changeModalAddEditHistoryAddress'
    );
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);

    const { rerender } = renderMockStoreId(<ExpandedAddress />, {
      initialState: generateState({
        openModal: '',
        addressType: addressInfo,
        addressTypes: [addressInfo]
      })
    });

    const queryMoreActionBtn = screen.getByTestId(
      'infobar_cardholder_expanded-address_more-actions_dls-button'
    );

    act(() => {
      userEvent.click(queryMoreActionBtn);
    });

    rerender(<ExpandedAddress />);

    const queryEditAddress = screen.getByText('txt_edit_address');

    act(() => {
      userEvent.click(queryEditAddress);
    });

    expect(changeModalAddEditHistoryAddress).toHaveBeenCalledWith({
      storeId,
      type: 'EDIT'
    });
  });
});

describe('Test Render UI > permission', () => {
  it('Render UI without any permission', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(false);
    waitFor(() => {
      renderMockStoreId(<ExpandedAddress />, {
        initialState: generateState({ openModal: '' })
      });
      const queryTextNoData = screen.getByText('txt_no_data');
      expect(queryTextNoData).toBeInTheDocument();
    });
  });

  it('Render UI > with all permissions are allowed', () => {
    jest.spyOn(entitlementsHelpers, 'checkPermission').mockReturnValue(true);
    waitFor(() => {
      renderMockStoreId(<ExpandedAddress />, {
        initialState: generateState({ openModal: '' })
      });
      const queryTextNoData = screen.getByText('txt_no_data');
      expect(queryTextNoData).toBeInTheDocument();
    });
  });

  it('Render UI have delete address permission', () => {
    const changeModalAddEditHistoryAddress = mockAction(
      'changeModalAddEditHistoryAddress'
    );

    jest
      .spyOn(entitlementsHelpers, 'checkPermission')
      .mockImplementation(permission =>
        permission === PERMISSIONS.CARDHOLDER_MAINTENANCE_EDIT_ADDRESS ||
        permission !== PERMISSIONS.CARDHOLDER_MAINTENANCE_DELETE_ADDRESS
          ? true
          : false
      );

    const { wrapper } = renderMockStoreId(<ExpandedAddress />, {
      initialState: generateState({
        openModal: '',
        addressTypes: [addressInfo]
      })
    });

    const queryClassIconEdit = queryByClass(
      wrapper.container,
      /icon icon-edit/
    );
    queryClassIconEdit!.click();
    expect(changeModalAddEditHistoryAddress).toBeCalled();
  });

  it('Render UI have edit address permission', () => {
    const changeModalAddEditHistoryAddress = mockAction(
      'changeModalAddEditHistoryAddress'
    );

    jest
      .spyOn(entitlementsHelpers, 'checkPermission')
      .mockImplementation(permission =>
        permission !== PERMISSIONS.CARDHOLDER_MAINTENANCE_EDIT_ADDRESS &&
        permission === PERMISSIONS.CARDHOLDER_MAINTENANCE_DELETE_ADDRESS
          ? true
          : false
      );

      const { wrapper } = renderMockStoreId(<ExpandedAddress />, {
      initialState: generateState({
        openModal: '',
        addressTypes: [addressInfo]
      })
    });
    
    const queryClassIconEdit = queryByClass(
      wrapper.container,
      /icon icon-delete/
    );
    queryClassIconEdit!.click();
    expect(changeModalAddEditHistoryAddress).toBeCalled();
  });

  it('Render UI Case 5', () => {
    jest
      .spyOn(entitlementsHelpers, 'checkPermission')
      .mockImplementation(permission =>
        permission === PERMISSIONS.CARDHOLDER_MAINTENANCE_VIEW_ADDRESS_HISTORY
          ? true
          : false
      );
    waitFor(() => {
      renderMockStoreId(<ExpandedAddress />, {
        initialState: generateState({ openModal: '' })
      });

      const queryTextEdit = screen.queryByText('txt_view_address_history');

      queryTextEdit!.click();
      const queryTextNoData = screen.getByText('txt_no_data');

      expect(queryTextNoData).toBeInTheDocument();
    });
  });
});
