import { getStateSubdivision } from './getStateSubdivision';
jest.mock('axios', () => {
  return () => {
    return new Promise((resolve, reject) => {
      resolve({
        data: {
          USA: {
            fieldID: 'AA',
            fieldValue: 'APO/FPO Florida',
            description: 'AA - APO/FPO Florida'
          }
        }
      });
      reject(() => new Error('1'));
    });
  };
});
describe('Test getStateSubdivision', () => {
  it('Should get data', async () => {
    const data = await getStateSubdivision('USA');
    expect(data).toEqual({
      fieldID: 'AA',
      fieldValue: 'APO/FPO Florida',
      description: 'AA - APO/FPO Florida'
    });
  });

  it('Should get empty', async () => {
    const data = await getStateSubdivision('CAN');
    expect(data).toEqual(null);
  });
});
