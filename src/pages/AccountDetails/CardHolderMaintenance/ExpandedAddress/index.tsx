import React, { Fragment, useCallback, useMemo } from 'react';

// components
import { View } from 'app/_libraries/_dof/core';
import AddAddress from './AddAndUpdate';
import History from '../HistoryAddress';
import SectionControl from 'app/components/SectionControl';
import DeleteAddress from './Delete';

// redux
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { VIEW_NAME_CARD_MAINTENANCE } from '../constants';
import {
  selectCardholderAddressType,
  selectCardholderAddressTypes,
  selectIsExpandAddress,
  selectModalExpandedAddress
} from '../_redux/selector';
import { useDispatch } from 'react-redux';
import { cardholderMaintenanceActions } from '../_redux';
import DropdownAddress from '../DropdownAddress';
import MoreAction, { ActionValue } from 'app/components/MoreActions';

// action
import { handleToggle, translateAddressInfo } from '../helpers';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// i18n
import { I18N_CARD_MAINTENANCE, I18N_COMMON_TEXT } from 'app/constants/i18n';

// types
import { IAddressInfo, ModalExpandedAddress } from '../types';
import { MODAL_ADDRESS_STANDARD } from './constants';
import {
  Button,
  DropdownButtonSelectEvent,
  Icon
} from 'app/_libraries/_dls/components';
import { isActiveAddress } from './helper';
import { isArrayHasValue } from 'app/helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { PERMISSIONS } from 'app/entitlements/constants';
import { checkPermission } from 'app/entitlements';

export interface CardHolderMaintenanceAddressProps {}

const ExpandedAddress: React.FC<CardHolderMaintenanceAddressProps> = ({}) => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const isExpandAddress = useStoreIdSelector<boolean>(selectIsExpandAddress);

  const addressTypeData = useStoreIdSelector<IAddressInfo>(
    selectCardholderAddressType
  );

  const addressType: IAddressInfo = useMemo(
    () => translateAddressInfo(addressTypeData, t),
    [addressTypeData, t]
  );

  const addressTypes = useStoreIdSelector<IAddressInfo[]>(
    selectCardholderAddressTypes
  );

  const openModal = useStoreIdSelector<ModalExpandedAddress>(
    selectModalExpandedAddress
  );
  const testId = 'infobar_cardholder_expanded-address';

  const changeToggle = () => {
    dispatch(handleToggle('isExpandAddress', storeId));
  };

  const dispatchChangeModal = useCallback(
    (type: ModalExpandedAddress) => {
      dispatch(
        cardholderMaintenanceActions.changeModalAddEditHistoryAddress({
          storeId,
          type
        })
      );
    },
    [dispatch, storeId]
  );

  const handleSelectActionsDropdownButton = (e: DropdownButtonSelectEvent) => {
    dispatchChangeModal(e.value as ModalExpandedAddress);
  };

  const handleSelectActions = useCallback(
    (e: ActionValue) => {
      dispatchChangeModal(e.uniqueId as ModalExpandedAddress);
    },
    [dispatchChangeModal]
  );

  const handleCloseModal = () => {
    dispatchChangeModal('');
  };

  const renderModal = () => {
    const { ADD, EDIT, VIEW, DELETE } = MODAL_ADDRESS_STANDARD;

    switch (openModal) {
      case EDIT:
      case ADD:
        return <AddAddress onCloseModal={handleCloseModal} />;
      case VIEW:
        return (
          <History
            onCloseModal={handleCloseModal}
            dataTestId={`${testId}_history`}
          />
        );
      case DELETE:
        return <DeleteAddress onCloseModal={handleCloseModal} />;
      default:
        return null;
    }
  };

  const renderMoreAction = useMemo(() => {
    const actions: ActionValue[] = [
      {
        label: I18N_CARD_MAINTENANCE.EDIT_ADDRESS,
        uniqueId: MODAL_ADDRESS_STANDARD.EDIT
      },
      {
        label: I18N_CARD_MAINTENANCE.DELETE_ADDRESS,
        uniqueId: MODAL_ADDRESS_STANDARD.DELETE,
        disabled: isActiveAddress(addressType)
      }
    ];
    if (
      checkPermission(PERMISSIONS.CARDHOLDER_MAINTENANCE_EDIT_ADDRESS, storeId)
    ) {
      if (
        checkPermission(
          PERMISSIONS.CARDHOLDER_MAINTENANCE_DELETE_ADDRESS,
          storeId
        )
      ) {
        return (
          <MoreAction
            size="lg"
            placementDropdown="bottom-end"
            actions={actions}
            onSelect={handleSelectActions}
            dataTestId={`${testId}_more-actions`}
          />
        );
      }
      return (
        <Button
          id={actions[0].uniqueId}
          variant="icon-secondary"
          onClick={() => {
            handleSelectActions(actions[0]);
          }}
          dataTestId={`${testId}_edit-address-btn`}
        >
          <Icon name="edit" />
        </Button>
      );
    } else if (
      checkPermission(
        PERMISSIONS.CARDHOLDER_MAINTENANCE_DELETE_ADDRESS,
        storeId
      )
    ) {
      return (
        <Button
          id={actions[1].uniqueId}
          variant="icon-secondary"
          disabled={isActiveAddress(addressType)}
          onClick={() => {
            handleSelectActions(actions[1]);
          }}
          dataTestId={`${testId}_delete-address-btn`}
        >
          <Icon name="delete" />
        </Button>
      );
    }
    return null;
  }, [addressType, handleSelectActions, storeId]);

  const dropDownItems = [
    {
      text: I18N_CARD_MAINTENANCE.ADD_ADDRESS,
      value: MODAL_ADDRESS_STANDARD.ADD,
      entitlement: PERMISSIONS.CARDHOLDER_MAINTENANCE_ADD_ADDRESS
    },
    {
      text: I18N_CARD_MAINTENANCE.VIEW_ADD_ADDRESS_HISTORY,
      value: MODAL_ADDRESS_STANDARD.VIEW,
      entitlement: PERMISSIONS.CARDHOLDER_MAINTENANCE_VIEW_ADDRESS_HISTORY
    }
  ];

  const isShowDropDownButton = () => {
    return (
      dropDownItems.filter(
        item => checkPermission(item.entitlement, storeId) === true
      ).length === 2
    );
  };

  const renderActionButton = () => {
    if (isShowDropDownButton()) return;
    const enableButton = dropDownItems.find(item =>
      checkPermission(item.entitlement, storeId)
    );
    if (!enableButton) return;
    return {
      buttonLabel: t(enableButton.text),
      handleAdd: () => {
        handleSelectActionsDropdownButton(enableButton as any);
      },
      className: 'h5 ml-4'
    };
  };

  return (
    <Fragment>
      <SectionControl
        title={t(I18N_COMMON_TEXT.ADDRESS)}
        classNamesView="ml-20"
        sectionKey={VIEW_NAME_CARD_MAINTENANCE.CARD_INFO}
        onToggle={changeToggle}
        isExpand={isExpandAddress}
        dropdownButton={
          isShowDropDownButton()
            ? {
                placementDropdown: 'bottom-end',
                tooltipText: 'txt_actions',
                onSelect: handleSelectActionsDropdownButton,
                items: dropDownItems
              }
            : undefined
        }
        isAdd={renderActionButton()}
        dataTestId={testId}
      >
        {!isArrayHasValue(addressTypes) ? (
          <p
            className="color-grey mt-16"
            data-testid={genAmtId(testId, 'no-data', 'ExpandedAddress')}
          >
            {t('txt_no_data')}
          </p>
        ) : (
          <React.Fragment>
            <DropdownAddress
              isDisplayDate={true}
              dataTestId={genAmtId(testId, 'addressType', 'ExpandedAddress')}
            />
            <div className="mt-16 d-flex justify-content-between mr-n4">
              <p
                className="fw-500"
                data-testid={genAmtId(testId, 'address-detail-title', '')}
              >
                {t(I18N_CARD_MAINTENANCE.ADDRESS_DETAIL)}
              </p>
              {renderMoreAction}
            </div>
            <View
              id={`cardholderMaintenanceAddress_${storeId}`}
              descriptor={VIEW_NAME_CARD_MAINTENANCE.CARD_ADDRESS}
              formKey={`cardholderMaintenanceAddress_${storeId}`}
              value={addressType}
            />
          </React.Fragment>
        )}
        {renderModal()}
      </SectionControl>
    </Fragment>
  );
};
export default ExpandedAddress;
