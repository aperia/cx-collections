import { isEmpty } from 'app/_libraries/_dls/lodash';
import axios from 'axios';
export const getStateSubdivision = async (countryID: string) => {
  const { data } = await axios({
    url: 'refData/stateByCountry.json',
    method: 'GET'
  });

  return isEmpty(data[countryID]) ? null : data[countryID];
};
