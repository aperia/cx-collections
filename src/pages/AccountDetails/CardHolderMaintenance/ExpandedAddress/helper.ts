import { MutableRefObject } from 'react';
import dateTime from 'date-and-time';

// Const
import { COUNTRY_ID } from 'app/constants';
import { FormatTime } from 'app/constants/enums';
import { ADDRESS_CATEGORY_ID, ID_ADDRESS_TYPE } from './constants';

// Helper
import { formatTimeDefault, handleWithRefOnFind } from 'app/helpers';
import { handleMaxDayAddAddress } from 'app/components/_dof_controls/custom-functions/onChangeDateExpandedAddress';

// Type
import { IAddressInfo, IDisableRange } from '../types';

// Action
import { getStateSubdivision } from './getStateSubdivision';

export const helperHandleDisableRange = (
  ref: MutableRefObject<MagicKeyValue>,
  disableRangeFrom: IDisableRange[],
  disableRangeTo: IDisableRange[],
  idAddressCategory: string
) => {
  let effectiveFromText = '';
  let effectiveToText = '';
  const effectiveFrom = handleWithRefOnFind(ref, 'effectiveFrom');
  const effectiveTo = handleWithRefOnFind(ref, 'effectiveTo');
  const { REPEATING, TEMPORARY } = ID_ADDRESS_TYPE;

  if (idAddressCategory === TEMPORARY) {
    effectiveFromText = 'txt_disabled_range_from_temporary';
    effectiveToText = 'txt_disabled_range_to_temporary';
  }

  if (idAddressCategory === REPEATING) {
    effectiveFromText = 'txt_disabled_range_from_repeating';
    effectiveToText = 'txt_disabled_range_to_repeating';
  }

  const getFromDateToDay = (range: IDisableRange) => {
    return {
      fromDate: new Date(formatTimeDefault(range.fromDate, FormatTime.Date)!),
      toDate: new Date(formatTimeDefault(range.toDate, FormatTime.Date)!)
    };
  };

  const disableEffectiveFrom: IDisableRange[] = disableRangeFrom.map(range =>
    getFromDateToDay(range)
  );

  const disableEffectiveTo: IDisableRange[] = disableRangeTo.map(range =>
    getFromDateToDay(range)
  );

  effectiveFrom?.props?.setOthers((pre: MagicKeyValue) => ({
    ...pre,
    options: {
      ...pre?.options,
      disableRange: disableEffectiveFrom,
      disableDateRangeText: effectiveFromText
    }
  }));
  effectiveTo?.props?.setOthers((pre: MagicKeyValue) => ({
    ...pre,
    options: {
      ...pre?.options,
      disableRange: disableEffectiveTo,
      disableDateRangeText: effectiveToText
    }
  }));
};

export const helperHandleEffectiveTo = (
  ref: MutableRefObject<MagicKeyValue>,
  addressTypeID: string
) => {
  if (addressTypeID !== ID_ADDRESS_TYPE.PERMANENT) return;
  const effectiveTo = handleWithRefOnFind(ref, 'effectiveTo');
  effectiveTo?.props?.setReadOnly(true);
};

export const helperHandleCalculateDate = (
  ref: MutableRefObject<MagicKeyValue>,
  data: IAddressInfo
) => {
  if (data.addressCategory.fieldID === ID_ADDRESS_TYPE.PERMANENT) return;
  const effectiveTo = handleWithRefOnFind(ref, 'effectiveTo');

  const effectFromFormat = formatTimeDefault(
    data.effectiveFrom,
    FormatTime.YearMonthDay
  )!;

  const newDate = formatTimeDefault(new Date(), FormatTime.YearMonthDay)!;

  const date = new Date(effectFromFormat);

  const calculateMinDate =
    effectFromFormat >= newDate
      ? new Date(date.setDate(date.getDate() + 1))
      : newDate;

  const currentDateFormat = formatTimeDefault(
    new Date(),
    FormatTime.YearMonthDay
  )!;

  effectiveTo?.props.setOthers((pre: MagicKeyValue) => ({
    ...pre,
    options: {
      ...pre?.options,
      min: formatTimeDefault(new Date(calculateMinDate), FormatTime.Date),
      minDateDisabledText:
        effectFromFormat >= currentDateFormat
          ? 'txt_effective_to_cannot_before_effective_from'
          : 'txt_effective_to_not_past_date'
    }
  }));
  handleMaxDayAddAddress(
    data.addressCategory.fieldID,
    effectiveTo,
    data.effectiveFrom as Date,
    data.effectiveTo as Date
  );
};

export const helperDisabledWithValueFormat = (
  ref: MutableRefObject<MagicKeyValue>,
  countryID: string,
  data: IAddressInfo
) => {
  let readOnly = false;
  const houseBuilding = handleWithRefOnFind(ref, 'houseBuilding');
  const streetName = handleWithRefOnFind(ref, 'streetName');
  const houseNumber = handleWithRefOnFind(ref, 'houseNumber');
  const POBox = handleWithRefOnFind(ref, 'POBox');
  const addressLineThree = handleWithRefOnFind(ref, 'addressLineThree');
  const addressLineFour = handleWithRefOnFind(ref, 'addressLineFour');
  const list = [
    houseBuilding,
    addressLineThree,
    addressLineFour,
    streetName,
    POBox,
    houseNumber
  ];
  if (countryID === COUNTRY_ID.USA) {
    readOnly = true;
    list.forEach(item => item?.props?.setValue(null));
  } else {
    list.forEach(item => item?.props?.setValue(data[item.name]));
  }
  list.forEach(item => item?.props?.setReadOnly(readOnly));
};

export const helperDisabledDefault = (
  ref: MutableRefObject<MagicKeyValue>,
  idCountryISO: string
) => {
  let maxLength = 5;
  const addressCategory = handleWithRefOnFind(ref, 'addressCategory');
  const addressType = handleWithRefOnFind(ref, 'addressType');
  const effectiveFrom = handleWithRefOnFind(ref, 'effectiveFrom');
  const postalCode = handleWithRefOnFind(ref, 'postalCode');
  const list = [addressCategory, addressType, effectiveFrom];

  let validation = "^[^&',-/.]+$";
  let errorMsg = 'txt_postal_code_not_contain';

  if (idCountryISO === 'CAN') {
    maxLength = 10;
    validation = "^[^&',/.]+$";
    errorMsg = 'txt_postal_code_contain_hyphen';
  }

  postalCode?.props.setOthers((pre: MagicKeyValue) => ({
    ...pre,
    options: {
      ...pre?.options,
      maxLength
    },
    validationRules: [
      { ...pre?.validationRules[0] },
      {
        ruleName: 'RegExp',
        errorMsg,
        ruleOptions: {
          regExp: validation
        }
      }
    ]
  }));

  list.forEach(item => item?.props?.setReadOnly(true));
};

export const helperGetSubdivision = async (
  ref: MutableRefObject<MagicKeyValue>,
  countryID: string
) => {
  const addressStateSubdivision = handleWithRefOnFind(ref, 'stateSubdivision');
  try {
    const data = await getStateSubdivision(countryID);
    addressStateSubdivision.props.setData(data);
    addressStateSubdivision.props.setReadOnly(false);
  } catch (error) {}
};

export const handleOverlapPermanent = (
  filterAddress: IAddressInfo[],
  ref: React.MutableRefObject<MagicKeyValue>
) => {
  const findMaxYear = filterAddress.find(item => {
    return item.effectiveTo?.toString()?.includes('9999');
  });

  const effectiveFromRef = handleWithRefOnFind(ref, 'effectiveFrom');
  const effectiveToRef = handleWithRefOnFind(ref, 'effectiveTo');

  effectiveFromRef?.props?.setOthers((pre: MagicKeyValue) => ({
    ...pre,
    options: {
      ...pre?.options,
      disableRange: [],
      disableDateRangeText: ''
    }
  }));

  effectiveToRef?.props?.setOthers((pre: MagicKeyValue) => ({
    ...pre,
    options: {
      ...pre?.options,
      disableRange: []
    }
  }));

  if (!findMaxYear) return;

  const currentDate = formatTimeDefault(new Date(), FormatTime.YearMonthDay);
  const formatEffectiveTo = formatTimeDefault(
    findMaxYear.effectiveFrom?.toString(),
    FormatTime.YearMonthDay
  )!;
  if (formatEffectiveTo >= currentDate!) {
    const newDate = new Date(formatEffectiveTo);

    const calculateMinDate = new Date(newDate.setDate(newDate.getDate() + 2));
    effectiveFromRef?.props?.setOthers((pre: MagicKeyValue) => ({
      ...pre,
      options: {
        ...pre?.options,
        disableRange: [
          { fromDate: new Date(currentDate!), toDate: calculateMinDate }
        ],
        disableDateRangeText: 'txt_disabled_range_from_permanent'
      }
    }));
  }
};

export const isActiveAddress = (address: IAddressInfo) => {
  if (!address?.effectiveTo || !address?.effectiveFrom) {
    return false;
  }

  if (
    address.addressCategory?.fieldID !== ID_ADDRESS_TYPE.PERMANENT ||
    address.addressType?.fieldID !== ADDRESS_CATEGORY_ID.BILLING
  )
    return false;

  if (dateTime.isSameDay(new Date(), new Date(address?.effectiveTo)))
    return true;

  if (dateTime.isSameDay(new Date(), new Date(address?.effectiveFrom)))
    return true;

  return (
    new Date() < new Date(address?.effectiveTo) &&
    new Date() > new Date(address?.effectiveFrom)
  );
};
