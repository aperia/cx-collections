export const VIEW_NAME_ADDRESS = { ADD_ADDRESS: 'addExpandedAddressView' };

export const TYPE_ADDRESS = {
  AUTHORIZED: '03',
  PRIMARY: '01',
  SECOND: '02'
};

// additionalUsers = authorized

export const TYPE_ADDRESS_MATCH_WITH_API = {
  ADDITIONAL_USERS: 'additionalUsers',
  PRIMARY_MATCH_API: 'primary',
  SECOND_MATCH_API: 'secondary'
};

export const MODAL_ADDRESS_STANDARD = {
  ADD: 'ADD',
  EDIT: 'EDIT',
  DELETE: 'DELETE',
  VIEW: 'HISTORY'
};

export const ID_ADDRESS_TYPE = {
  PERMANENT: 'P',
  TEMPORARY: 'T',
  REPEATING: 'R'
};

export const ADDRESS_CATEGORY_ID = {
  BILLING: 'BLL1',
  TEMPORARY: 'T',
  REPEATING: 'R'
};

export const ID_FORMATTED_ADDRESS = {
  FORMATTED: 'F',
  UNFORMATTED: 'T',
  STANDARD_ADDRESS: 'BLANK'
};

export const FIELD_EXPANDED_ADDRESS_INFO = [
  'cityName',
  'postalCode',
  'addressContinuationLine1Text',
  'addressContinuationLine2Text',
  'addressContinuationLine3Text',
  'addressContinuationLine4Text',
  'addressSubdivisionOneText',
  'addressSubdivisionTwoText',
  '*'
];

export const FIELD_HISTORY_EXPANDED_ADDRESS_INFO = [
  'cityName',
  'postalCode',
  'addressContinuationLine1Text',
  'addressContinuationLine2Text',
  'addressContinuationLine3Text',
  'addressContinuationLine4Text',
  'addressSubdivisionOneText',
  'addressSubdivisionTwoText',
  '*'
];
