import { AddressRoleTypes } from './types';

export const VIEW_NAME_CARD_MAINTENANCE = {
  CARD_INFO: 'cardholderMaintenanceInfo',
  CARD_ADDRESS: 'cardholderMaintenanceAddress',
  CARD_STANDARD_ADDRESS: 'cardholderMaintenanceStandardAddress',
  CARD_TELEPHONE: 'cardholderMaintenanceTelephone',
  CARD_EMAIL: 'cardholderMaintenanceEmail',
  EDIT_PHONE: 'editPhoneFormView',
  EDIT_EMAIL: 'editEmailFormView'
};

export const I18N_CARDHOLDER_MAINTENANCE = {
  CARDHOLDER_MAINTENANCE: 'txt_cardholder_maintenance',
  CARDHOLDER_INFO: 'txt_cardholder_information',
  EDIT_EMAIL: 'txt_cardholder_edit_email',
  EDIT_PHONE: 'txt_cardholder_edit_phone',
  UPDATE_PHONE_SUCCESS: 'txt_cardholder_update_phone_success',
  UPDATE_PHONE_FAIL: 'txt_cardholder_update_phone_fail',
  ADDRESS_DETAILS: 'txt_cardholder_address_details',
  ADDRESS_TYPE: 'txt_address_type',
  CONFIRM_DELETE_ADDRESS: 'txt_confirm_delete_address',
  CONFIRM_DELETE_ADDRESS_BODY: 'txt_confirm_delete_address_body',
  ADDRESS_DELETED: 'txt_address_deleted',
  ADDRESS_FAILED_TO_DELETE: 'txt_address_failed_to_delete'
};

export enum ID_GRID_ADDRESS_HISTORY {
  ADDRESS_CATEGORY = 'addressCategory',
  ADDRESS_TYPE = 'addressType',
  EFFECTIVE_FROM = 'effectiveFromDate',
  EFFECTIVE_TO = 'effectiveToDate',
  ADDRESS = 'address'
}

export const FIELD_REQUEST = [
  'customerInquiry.memberSequenceIdentifier',
  'customerInquiry.customerName',
  'customerInquiry.customerRoleTypeCode',
  'customerInquiry.presentationInstrumentStatusCode',
  'customerInquiry.presentationInstrumentIdentifier',
  'customerInquiry.firstName',
  'customerInquiry.middleName',
  'customerInquiry.lastName',
  'customerInquiry.customerRoleTypeCode',
  'customerInquiry.creditBureauReportCode',
  'customerInquiry.salutationCode',
  'customerInquiry.titleName',
  'customerInquiry.suffixName',
  'customerInquiry.prefixName',
  'customerInquiry.qualificationName',
  'customerInquiry.personalizedEmbossingText',
  'customerInquiry.birthDate',
  'customerInquiry.socialSecurityIdentifier',
  'customerInquiry.solicitationCode',
  // telephone information
  'customerInquiry.primaryCustomerHomePhoneIdentifier',
  'customerInquiry.homePhoneDeviceCode',
  'customerInquiry.homePhoneLastUpdatedDate',
  'customerInquiry.homePhoneStatusCode',
  'customerInquiry.primaryCustomerSecondPhoneIdentifier',
  'customerInquiry.businessPhoneDeviceCode',
  'customerInquiry.businessPhoneLastUpdatedDate',
  'customerInquiry.businessTelephoneFlagCode',
  'customerInquiry.mobilePhoneIdentifier',
  'customerInquiry.mobilePhoneDeviceCode',
  'customerInquiry.mobilePhoneLastUpdatedDate',
  'customerInquiry.mobilePhoneFlagCode',
  'customerInquiry.facsimilePhoneIdentifier',
  'customerInquiry.facsimilePhoneDeviceCode',
  'customerInquiry.facsimileLastUpdatedDate',
  'customerInquiry.facsimilePhoneFlagCode',
  'customerInquiry.clientControlledPhoneIdentifier',
  'customerInquiry.clientControlledPhoneDeviceCode',
  'customerInquiry.clientControlledPhoneLastUpdatedDate',
  'customerInquiry.clientControlledPhoneFlagIndicator',
  // email information
  'customerInquiry.electronicMailHomeAddressText',
  'customerInquiry.electronicMailHomeStatusIndicator',
  'customerInquiry.electronicMailHomeSolicitIndicator',
  'customerInquiry.electronicMailWorkAddressText',
  'customerInquiry.electronicMailWorkStatusIndicator',
  'customerInquiry.electronicMailWorkSolicitIndicator'
];

export const ADDRESS_ROLE_MAP: Record<string, AddressRoleTypes> = {
  '01': 'primary',
  '02': 'secondary',
  '03': 'additionalUsers'
};

export const ADDRESS_EXPANSION_CODE = {
  STANDARD: '0',
  EXPANDED: '1'
};
