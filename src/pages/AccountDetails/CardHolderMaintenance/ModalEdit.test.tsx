import React from 'react';
import { screen } from '@testing-library/react';
import {
  storeId,
  renderMockStoreId
} from 'app/test-utils/renderComponentWithMockStore';
import ModalEdit from './ModalEdit';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { TYPE_EDIT } from './TelePhone/constants';
import { mockActionCreator } from 'app/test-utils';
import { confirmModalActions } from 'pages/__commons/ConfirmModal/_redux';
import {
  cardholderMaintenanceActions,
  defaultStateCardholder
} from 'pages/AccountDetails/CardHolderMaintenance/_redux';
import * as reduxForm from 'redux-form';

const cardholderMaintenanceSpy = mockActionCreator(
  cardholderMaintenanceActions
);

const generateState = (typeEdit = TYPE_EDIT.EMAIL) => {
  return {
    form: {
      [`${storeId}_telephone_edit_view`]: {
        values: {
          fax: {
            less: '4234234234',
            more: {
              deviceType: { value: 'U', description: 'U - Unknown Type' },
              flag: {
                value: 'Y',
                description: 'Y - Number Is Valid; Manual Dial Only'
              },
              lastUpdated: '04/29/2021'
            }
          },
          homePhone: {
            less: '0895565655',
            more: {
              deviceType: { value: 'I', description: 'I - Invalid Number' },
              flag: {
                value: 'C',
                description: 'C - Do Not Contact Cease And Desist'
              },
              lastUpdated: '05/04/2021'
            }
          },
          miscellaneous: {
            less: '7987979797',
            more: {
              deviceType: { value: 'L', description: 'L - Land Line' },
              flag: { value: 'V', description: 'V - Number Is Not Verified' },
              lastUpdated: '04/28/2021'
            }
          },
          mobilePhone: {
            less: '3232323232',
            more: {
              deviceType: { value: 'I', description: 'I - Invalid Number' },
              flag: {
                value: 'B',
                description: 'B - Consent For SMS Text Messages And Voice'
              },
              lastUpdated: '05/07/2021'
            }
          },
          workPhone: {
            less: '',
            more: {
              deviceType: null,
              flag: null,
              lastUpdated: null
            }
          }
        }
      }
    } as any,
    cardholderMaintenance: {
      [storeId]: {
        ...defaultStateCardholder,
        isLoading: true,
        isOpenModalPhoneEmail: true,
        typeEdit,
        cardholderId: {
          cardNumberValue: 'VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)',
          cardholderName: 'ENDY2,HARRYY T',
          cardholderRole: '01',
          customerExternalIdentifier: 'C20273234913858500085018',
          customerRoleTypeCode: '01',
          customerType: '01',
          id: '01',
          memberSequence: '00001',
          status: '',
          statusCode: ''
        },
        cardholders: [
          {
            cardholderId: 'id',
            cardholderInfo: {
              authCBRFlag: 'Individual',
              cardNumber: '022002******5078',
              cardholderRole: '01',
              dateOfBirth: '********2020',
              embossingName: 'Y',
              firstName: 'HARRYY',
              lastName: 'ENDY2',
              memberSequence: '00001',
              middleName: 'TOMMY',
              prefix: 'TT111E',
              qualification: 'Y',
              salutation: 'Unknown Gender',
              solicitation: 'Solicitation Allowed',
              ssnTaxId: '4286',
              suffix: 'TYT',
              title: 'Y'
            },
            email: {},
            telephone: {
              homePhone: { less: 'homePhone less' },
              workPhone: { less: 'workPhone less' },
              mobilePhone: { less: 'mobilePhone less' },
              miscellaneous: { less: 'miscellaneous less' },
              fax: { less: 'fax less' }
            }
          }
        ]
      }
    }
  } as Partial<RootState>;
};

describe('Actions', () => {
  it('onCloseModal', () => {
    const initialState = generateState();
    renderMockStoreId(<ModalEdit />, { initialState });
    const mockAction = cardholderMaintenanceSpy('updateModalEdit');

    screen.getByText(I18N_COMMON_TEXT.CANCEL).click();

    expect(mockAction).toBeCalledWith({
      storeId,
      open: false,
      type: TYPE_EDIT.EMAIL
    });
  });

  it('onCloseModal with confirmModalActions', () => {
    jest.spyOn(reduxForm, 'isDirty').mockImplementation(() => () => true);
    const openConfirmModalAction = jest.fn(x => {
      if (x.confirmCallback) {
        x.confirmCallback();
      }
      return { type: 'ok' };
    });

    const spy = jest
      .spyOn(confirmModalActions, 'open')
      .mockImplementation(openConfirmModalAction as never);

    const initialState = generateState(TYPE_EDIT.PHONE);

    renderMockStoreId(<ModalEdit />, { initialState });

    screen.getByText(I18N_COMMON_TEXT.CANCEL).click();
    expect(openConfirmModalAction).toBeCalledWith(
      expect.objectContaining({
        title: 'txt_unsaved_changes',
        body: 'txt_discard_change_body',
        cancelText: 'txt_continue_editing',
        confirmText: 'txt_discard_change',
        variant: 'danger'
      })
    );
    spy.mockRestore();
    spy.mockReset();
  });

  describe('submitForm', () => {
    it('Email', () => {
      jest.spyOn(reduxForm, 'isDirty').mockImplementation(() => () => true);
      const initialState = generateState();
      renderMockStoreId(<ModalEdit />, { initialState });
      const mockAction = cardholderMaintenanceSpy('triggerUpdateCardHolder');
      screen.getByText(I18N_COMMON_TEXT.SAVE).click();
      expect(mockAction).toHaveBeenCalled();
    });

    it('Phone', () => {
      jest.spyOn(reduxForm, 'isDirty').mockImplementation(() => () => true);
      const initialState = generateState(TYPE_EDIT.PHONE);
      renderMockStoreId(<ModalEdit />, { initialState });
      const mockAction = cardholderMaintenanceSpy('triggerUpdateCardHolder');
      screen.getByText(I18N_COMMON_TEXT.SAVE).click();
      expect(mockAction).toHaveBeenCalled();
    });
  });
});
