import React, { useMemo } from 'react';

// components
import {
  DropdownList,
  DropdownBaseChangeEvent
} from 'app/_libraries/_dls/components';
import DropdownItem from './DropdownItem';
import TogglePin from 'pages/__commons/InfoBar/TogglePin';

// redux
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { batch, useDispatch } from 'react-redux';
import {
  selectCardholderId,
  selectCardholdersId
} from 'pages/AccountDetails/CardHolderMaintenance/_redux/selector';
import { actionsInfoBar, InfoBarSection } from 'pages/__commons/InfoBar/_redux';
import { selectPinned } from 'pages/__commons/InfoBar/_redux/selectors';
import { cardholderMaintenanceActions } from './_redux';

// types
import {
  ICardholderId,
  IChangeCardholder
} from 'pages/AccountDetails/CardHolderMaintenance/types';

// i18next
import { I18N_CARDHOLDER_MAINTENANCE } from './constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constant
import { CARD_ROLE } from 'app/constants';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface CardHolderDropdownProps {}

const CardholderDropdown: React.FC<CardHolderDropdownProps> = ({}) => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const cardholderId = useStoreIdSelector<ICardholderId>(selectCardholderId);
  const cardholderIds =
    useStoreIdSelector<ICardholderId[]>(selectCardholdersId);
  const pinned = useStoreIdSelector(selectPinned);
  const isPinned = pinned === InfoBarSection.CardholderMaintenance;

  const renderItem = useMemo(() => {
    if (!cardholderIds || !cardholderIds.length) return [];
    return cardholderIds.map((item: ICardholderId) => (
      <DropdownList.Item
        key={item.id}
        label={item.cardholderName}
        value={item}
        variant="custom"
      >
        <DropdownItem
          cardholderName={item.cardholderName}
          cardholderRole={CARD_ROLE[item.cardholderRole]}
          status={item.status}
          statusCode={item.statusCode}
        />
      </DropdownList.Item>
    ));
  }, [cardholderIds]);

  const handleChange = (e: DropdownBaseChangeEvent) => {
    const data: IChangeCardholder = {
      cardholderId: e.target.value,
      storeId
    };
    dispatch(cardholderMaintenanceActions.changeCardholder(data));
    dispatch(
      cardholderMaintenanceActions.checkTypeAddress({
        storeId,
        accEValue: data.cardholderId.cardNumberValue
      })
    );
  };

  const handleTogglePin = () => {
    batch(() => {
      const thisPinned = isPinned ? null : InfoBarSection.CardholderMaintenance;

      dispatch(
        actionsInfoBar.updatePinned({
          storeId,
          pinned: thisPinned
        })
      );

      if (thisPinned) {
        dispatch(actionsInfoBar.updateRecover({ storeId, recover: null }));
      }

      dispatch(actionsInfoBar.updateIsExpand({ storeId, isExpand: !isPinned }));
    });
  };

  return (
    <div className="border-bottom bg-light-l20 px-24 pt-24 pb-16">
      <div className="d-inline-flex">
        <h4
          className="mb-16 mr-8"
          data-testid={genAmtId('infobar_cardholder-header', 'title', '')}
        >
          {t(I18N_CARDHOLDER_MAINTENANCE.CARDHOLDER_MAINTENANCE)}
        </h4>
        <TogglePin
          isPinned={isPinned}
          onToggle={handleTogglePin}
          dataTestId="infobar_cardholder-header_pin-btn"
        />
      </div>
      <DropdownList
        value={cardholderIds?.find(item => item.id === cardholderId.id)}
        name="cardholder"
        textField="cardholderName"
        label={t('txt_cardholder')}
        onChange={handleChange}
        dataTestId="infobar_cardholder_cardholder-name"
        textFieldRender={value =>
          value && (
            <DropdownItem
              isDropdownItem={false}
              cardholderName={value.cardholderName}
              cardholderRole={CARD_ROLE[value.cardholderRole]}
              status={value.status}
              statusCode={value.statusCode}
            />
          )
        }
        popupBaseProps={{
          popupBaseClassName: 'inside-infobar'
        }}
        noResult={t('txt_no_results_found')}
      >
        {renderItem}
      </DropdownList>
    </div>
  );
};

export default CardholderDropdown;
