import React from 'react';
import { screen } from '@testing-library/dom';

// helpers
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';

// components
import WebsiteLauncher from '.';

import { InfoBarSection, actionsInfoBar } from 'pages/__commons/InfoBar/_redux';

import { USER_TOKEN_ID } from 'app/constants';
import { waitFor } from '@testing-library/react';

window.appConfig = {
  commonConfig: {
    solutionBuilderUrl: 'token'
  }
} as any;

const initialState: Partial<RootState> = {
  renewToken: {
    [USER_TOKEN_ID]: {
      token: 'token'
    }
  },
  infoBar: {
    [storeId]: {
      pinned: InfoBarSection.websiteLauncher
    }
  }
};

jest.mock(
  'pages/__commons/InfoBar/TogglePin',
  () => (props: TogglePinProps) => {
    return <div onClick={props.onToggle}>TogglePin</div>;
  }
);

const spyActionsInfoBar = mockActionCreator(actionsInfoBar);

describe('Render', () => {
  it('render UI', () => {
    waitFor(() => {
      renderMockStoreId(<WebsiteLauncher />, {
        initialState
      });

      expect(screen.getByText('txt_website_launcher')).toBeInTheDocument();
    });
  });

  it('handleTogglePin', () => {
    const spy = spyActionsInfoBar('updatePinned');
    const { wrapper } = renderMockStoreId(<WebsiteLauncher />, {
      initialState
    });

    const button = wrapper.getByText('TogglePin');
    button.click();

    expect(spy).toBeCalled();
  });

  it('handleTogglePin with empty isPinned', () => {
    const spy = spyActionsInfoBar('updatePinned');
    const { wrapper } = renderMockStoreId(<WebsiteLauncher />, {
      initialState: {
        renewToken: {
          [USER_TOKEN_ID]: {
            token: 'token'
          }
        },
        infoBar: {
          [storeId]: {
            pinned: InfoBarSection.CollectionForm
          }
        }
      }
    });

    const button = wrapper.getByText('TogglePin');
    button.click();

    expect(spy).toBeCalled();
  });
});
