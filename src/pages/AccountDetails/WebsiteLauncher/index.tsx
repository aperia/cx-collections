import React from 'react';

// components
import TogglePin from 'pages/__commons/InfoBar/TogglePin';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';

// redux
import { batch, useDispatch } from 'react-redux';
import { selectPinned } from 'pages/__commons/InfoBar/_redux/selectors';
import { actionsInfoBar, InfoBarSection } from 'pages/__commons/InfoBar/_redux';
import { takeUserToken } from 'pages/__commons/RenewToken/_redux/selectors';

// helpers
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface WebsiteLauncherProps {}

const WebsiteLauncher: React.FC<WebsiteLauncherProps> = () => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const pinned = useStoreIdSelector(selectPinned);
  const userToken = useStoreIdSelector<string>(takeUserToken);
  const isPinned = pinned === InfoBarSection.websiteLauncher;

  const testId = 'infoBar_websiteLauncher';

  const handleTogglePin = () => {
    batch(() => {
      const thisPinned = isPinned ? null : InfoBarSection.websiteLauncher;

      dispatch(
        actionsInfoBar.updatePinned({
          storeId,
          pinned: thisPinned
        })
      );

      if (thisPinned) {
        dispatch(actionsInfoBar.updateRecover({ storeId, recover: null }));
      }

      dispatch(actionsInfoBar.updateIsExpand({ storeId, isExpand: !isPinned }));
    });
  };

  const solutionBuilderUrl =
    window.appConfig?.commonConfig?.solutionBuilderUrl.replace(
      'token',
      userToken
    );

  return (
    <div className="h-100">
      <div className="d-flex justify-content-between py-24 px-24 pb-16">
        <div className="d-inline-flex">
          <h4 data-testid={genAmtId(testId, 'title', '')} className="mr-8">
            {t('txt_website_launcher')}
          </h4>
          <TogglePin
            isPinned={isPinned}
            onToggle={handleTogglePin}
            dataTestId={testId}
          />
        </div>
      </div>
      <div className="px-24">
        <a
          href={solutionBuilderUrl}
          className="link-external text-decoration-none"
          target="_blank"
          rel="noreferrer"
          data-testid={genAmtId(testId, 'solution_builder-link', '')}
        >
          {t('txt_solution_builder')}
        </a>
      </div>
    </div>
  );
};

export default WebsiteLauncher;
