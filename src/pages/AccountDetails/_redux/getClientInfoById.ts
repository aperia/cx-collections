import { createAsyncThunk } from '@reduxjs/toolkit';
import isArray from 'lodash.isarray';
import { selectClientInfoGetByIdRequest } from 'pages/__commons/AccountDetailTabs/_redux';
import {
  GetClientInfoArgs,
  GetClientInfoRequestBody,
  GetClientInfoReturned
} from 'pages/ClientConfiguration/types';
import { clientConfigurationServices } from 'pages/ClientConfiguration/clientConfigurationServices';

export interface GetClientInfoDataArgs
  extends GetClientInfoArgs,
    StoreIdPayload {}

export const getClientInfoData = createAsyncThunk<
  GetClientInfoReturned,
  GetClientInfoDataArgs,
  ThunkAPIConfig
>('accountDetail/getClientInfoById', async (args, thunkAPI) => {
  const { getState } = thunkAPI;
  const { dataType, fieldsToQuery, storeId } = args;
  const fieldsToInclude = isArray(fieldsToQuery)
    ? fieldsToQuery
    : [fieldsToQuery || dataType];

  try {
    const commonClientInfoRequest = selectClientInfoGetByIdRequest(
      getState(),
      storeId
    );

    const requestBody: GetClientInfoRequestBody = {
      ...commonClientInfoRequest,
      fieldsToInclude,
      enableFallback: true
    };

    const response = await clientConfigurationServices.getClientInfoById(
      requestBody
    );
    if (response.data.responseStatus !== 'success') throw response;
    return { [dataType]: response.data.clientInfo?.[dataType] };
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});
