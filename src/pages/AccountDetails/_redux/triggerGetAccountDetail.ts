import { CUSTOMER_ROLE_CODE } from 'app/constants';
import { contactEntitlement } from 'app/entitlements';
import { dashboardActions } from 'pages/AccountSearch/Home/Dashboard/_redux/reducers';
import { bankruptcyActions } from 'pages/Bankruptcy/_redux/reducers';
import { cccsActions } from 'pages/CCCS/_redux/reducers';
import { deceasedActions } from 'pages/Deceased/_redux/reducers';
import { hardshipActions } from 'pages/HardShip/_redux/reducers';
import { accessTokenActions } from 'pages/__commons/AccessToken/_redux/reducer';
import { entitlementActions } from 'pages/__commons/Entitlement/_redux/reducers';
import { batch } from 'react-redux';
import { GetAccountDetailPayload } from '../types';
// Redux
import { accountDetailActions } from './reducer';

interface TriggerGetAccountDetailArgs {
  storeId: string;
  relatedStoreId?: string;
  accEValue: string;
  memberSequenceIdentifier: string;
  socialSecurityIdentifier: string;
  queueId?: string;
}

interface GetAccountDetailResponse {
  payload: GetAccountDetailPayload;
}
export const triggerGetAccountDetail: AppThunk<Promise<unknown>> =
  ({
    storeId,
    relatedStoreId = '',
    accEValue,
    memberSequenceIdentifier,
    socialSecurityIdentifier,
    queueId
  }: TriggerGetAccountDetailArgs) =>
  async (dispatch, getState) => {
    const { accountDetail } = getState();
    const selectedCaller =
      accountDetail[storeId]?.selectedCaller ||
      accountDetail[relatedStoreId]?.selectedCaller ||
      {};
    const { isByPass } = selectedCaller;

    contactEntitlement.removeRegister(storeId);

    const res = await dispatch<Promise<unknown>>(
      accountDetailActions.getAccountDetail({
        storeId,
        postData: {
          common: {
            accountId: accEValue
          },
          inquiryFields: {
            memberSequenceIdentifier
          }
        },
        socialSecurityIdentifier,
        triggerRenewToken: true
      })
    );

    const { data } = (res as GetAccountDetailResponse).payload || {};

    await dispatch<Promise<unknown>>(
      entitlementActions.getContactEntitlement({
        agent: data?.agentId ?? '',
        clientNumber: data?.clientID ?? '',
        prin: data?.principalId ?? '',
        system: data?.sysId ?? '',
        accountId: accEValue,
        customerRoleTypeCode: isByPass
          ? CUSTOMER_ROLE_CODE.primary
          : selectedCaller?.customerRoleTypeCode,
        storeId
      })
    );

    batch(() => {
      dispatch(deceasedActions.deceasedInformationRequest({ storeId }));

      dispatch(
        bankruptcyActions.getBankruptcyInformation({
          storeId
        })
      );

      dispatch(
        hardshipActions.getHardshipDebtManagementDetails({
          postData: {
            storeId
          }
        })
      );

      dispatch(
        cccsActions.getCCCSDetail({
          storeId
        })
      );

      dispatch(
        accountDetailActions.getSecureInfoData({
          storeId
        })
      );

      dispatch(
        accessTokenActions.getAccessToken({
          storeId,
          postData: {
            accountId: accEValue
          }
        })
      );

      queueId &&
        dispatch(
          dashboardActions.getAccountLockStatusInquiry({
            storeId,
            accEValue,
            relatedStoreId
          })
        );
    });
  };
