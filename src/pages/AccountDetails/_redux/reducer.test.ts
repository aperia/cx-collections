import { AccountDetailInitialState } from '../types';
import { accountDetail, accountDetailActions } from './reducer';
import { storeId } from 'app/test-utils';

const initialState: AccountDetailInitialState = {};

describe('Test reducer', () => {
  it('setLoadingRenewToken', () => {
    const params = { storeId };
    const state = accountDetail(
      initialState,
      accountDetailActions.setLoadingRenewToken(params)
    );
    expect(state).toEqual({ 123456789: { isLoadingRenewRelated: true } });
  });

  it('setRefreshAcc', () => {
    const params = { storeId };
    const state = accountDetail(
      initialState,
      accountDetailActions.setRefreshAcc(params)
    );
    expect((state as any)[storeId].isRefreshAccDetail).toBeTruthy();
  });

  it('transferDataFromAuthentication', () => {
    const params = {
      storeId,
      rawAccountDetailData: undefined,
      accountDetailBySeqNumber: undefined
    };
    const state = accountDetail(
      initialState,
      accountDetailActions.transferDataFromAuthentication(params)
    );
    expect(state).toEqual({
      123456789: {
        accountDetailBySeqNumber: undefined,
        rawAccountDetailData: undefined
      }
    });
  });

  it('removeStore', () => {
    const params = {
      storeId
    };
    const state = accountDetail(
      initialState,
      accountDetailActions.removeStore(params)
    );
    expect(state).toEqual({});
  });

  it('toggle loading', () => {
    const params = {
      storeId
    };
    const state = accountDetail(
      initialState,
      accountDetailActions.toggleLoading(params)
    );
    expect(state).toEqual({
      [storeId]: {
        isLoading: true
      }
    });
  });

  it('saveSelectedCaller', () => {
    const params = {
      storeId,
      data: {
        customerName: 'customerName',
        customerNameForCreateWorkflow: 'customerNameForCreateWorkflow'
      },
      authData: {
        customerName: 'customerName'
      }
    };
    const state = accountDetail(
      initialState,
      accountDetailActions.saveSelectedCaller(params)
    );
    expect(state).toEqual({
      123456789: {
        selectedAuthenticationData: {
          customerName: 'customerName'
        },
        selectedCaller: {
          customerName: 'customerName',
          customerNameForCreateWorkflow: 'customerNameForCreateWorkflow'
        }
      }
    });
  });
});
