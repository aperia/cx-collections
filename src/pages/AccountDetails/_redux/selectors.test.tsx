import React from 'react';
import { storeId, selectorWrapper, renderMockStore } from 'app/test-utils';
import * as selectors from './selectors';
import { AccountDetail } from '../types';

const SAMPLE_DATA: AccountDetail = {
  data: {
    historical: {
      openDate: '10/10/2020'
    },
    externalStatusCode: 'true',
    nextHeldStatementDestinationCode: '123',
    monetary: {
      currentBalance: '100'
    },
    internalStatusCode: '123',
    primaryName: '123',
    secondaryName: '123'
  },
  isLoading: true,
  isReloading: true,
  isError: true,
  customerInquiry: [{ customerRoleTypeCode: '01', delinquentAmount: '1' }],
  isRefreshAccDetail: true
};

const selectorTrigger = selectorWrapper(
  {
    accountDetail: {
      [storeId]: {
        ...SAMPLE_DATA
      }
    },
    refData: {}
  },
  { accountDetail: { [storeId]: {} } }
);

describe('Test selectors', () => {
  it('takeAccountDetail', () => {
    const { data, emptyData } = selectorTrigger(selectors.takeAccountDetail);
    expect(data).toEqual(SAMPLE_DATA.data);
    expect(emptyData).toEqual(undefined);
  });

  it('takeAccountDetailLoadingStatus', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.takeAccountDetailLoadingStatus
    );
    expect(data).toEqual(SAMPLE_DATA.isLoading);
    expect(emptyData).toEqual(undefined);
  });
  it('takeAccountDetailReloadingStatus', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.takeAccountDetailReloadingStatus
    );
    expect(data).toEqual(SAMPLE_DATA.isReloading);
    expect(emptyData).toEqual(undefined);
  });
  it('takeAccountDetailErrorStatus', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.takeAccountDetailErrorStatus
    );
    expect(data).toEqual(SAMPLE_DATA.isError);
    expect(emptyData).toEqual(undefined);
  });
  it('takeCurrentExternalStatus', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.takeCurrentExternalStatus
    );
    expect(data).toEqual(SAMPLE_DATA.data?.externalStatusCode);
    expect(emptyData).toEqual(undefined);
  });
  it('takeDestinationCode', () => {
    const { data, emptyData } = selectorTrigger(selectors.takeDestinationCode);
    expect(data).toEqual(SAMPLE_DATA.data?.nextHeldStatementDestinationCode);
    expect(emptyData).toEqual(undefined);
  });
  it('takeMonetaryData', () => {
    const { data, emptyData } = selectorTrigger(selectors.takeMonetaryData);
    expect(data).toEqual(SAMPLE_DATA.data?.monetary);
    expect(emptyData).toEqual(undefined);
  });
  it('takeHistoricalData', () => {
    const { data, emptyData } = selectorTrigger(selectors.takeHistoricalData);
    expect(data).toEqual(SAMPLE_DATA.data?.historical);
    expect(emptyData).toEqual(undefined);
  });
  it('getAccountDetailInternalStatusCode', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.getAccountDetailInternalStatusCode
    );
    expect(data).toEqual(SAMPLE_DATA.data?.internalStatusCode);
    expect(emptyData).toEqual(undefined);
  });
  it('takeHasOnlyPrimaryCardholder', () => {
    const { data, emptyData } = selectorTrigger(
      selectors.takeHasOnlyPrimaryCardholder
    );
    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });
  it('useFilerNameDropdownData', () => {
    const initialState: Partial<RootState> = {
      accountDetail: {
        [storeId]: {
          data: {
            primaryName: 'aaa',
            secondaryName: 'bbb'
          }
        }
      }
    };
    let resultData,
      resultEmptyData = null;
    const Component: React.FC<any> = () => {
      const { data, emptyData } = selectorTrigger(
        selectors.useFilerNameDropdownData
      );
      resultData = data;
      resultEmptyData = emptyData;
      return <div />;
    };
    renderMockStore(<Component />, {
      initialState
    });
    expect(resultData).toEqual([]);
    expect(resultEmptyData).toEqual([]);
  });

  it('refData', () => {
    const { data, emptyData } = selectorTrigger(selectors.refData);
    expect(data).toEqual({});
    expect(emptyData).toEqual(undefined);
  });

  it('takeRefreshAcc', () => {
    const { data } = selectorTrigger(selectors.takeRefreshAcc);
    expect(data).toBeTruthy();
  });

  it('takeRefreshAcc with undefined store', () => {
    const result = selectors.getRefreshStatus({} as any, undefined);
    expect(result).toBeUndefined();
  });
});
