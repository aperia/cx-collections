import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Const
import { EMPTY_OBJECT } from 'app/constants';
import { getSessionStorageWithUF8 } from 'app/helpers';

// Types
import { AccountDetailInitialState, SaveSelectedCallerPayload } from '../types';

// Actions
import { getAccountDetail, getAccountDetailBuilder } from './getAccountDetail';
import { getClientInfoData } from './getClientInfoById';
import { getSecureInfoBuilder, getSecureInfoData } from './getSecureInfoById';

const saveAccountDetailData = getSessionStorageWithUF8('accountDetail');

const initialState: AccountDetailInitialState =
  saveAccountDetailData || EMPTY_OBJECT;

export const { actions, reducer } = createSlice({
  initialState,
  name: 'accountDetail',
  reducers: {
    setLoadingRenewToken: (
      draftState,
      action: PayloadAction<StoreIdPayload>
    ) => {
      const { storeId } = action.payload;
      const { isLoadingRenewRelated = false } = draftState[storeId] || {};

      draftState[storeId] = {
        ...draftState[storeId],
        isLoadingRenewRelated: !isLoadingRenewRelated
      };
    },
    setRefreshAcc: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      const { isRefreshAccDetail = false } = draftState[storeId] || {};

      draftState[storeId] = {
        ...draftState[storeId],
        isRefreshAccDetail: !isRefreshAccDetail
      };
    },
    transferDataFromAuthentication: (
      draftState,
      action: PayloadAction<{
        numberPhoneAuthenticated?: string;
        rawAccountDetailData?: MagicKeyValue;
        accountDetailBySeqNumber?: MagicKeyValue;
        sharedInfoTimer?: MagicKeyValue;
        storeId: string;
      }>
    ) => {
      const {
        storeId,
        rawAccountDetailData,
        accountDetailBySeqNumber,
        numberPhoneAuthenticated,
        sharedInfoTimer
      } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        numberPhoneAuthenticated,
        rawAccountDetailData,
        accountDetailBySeqNumber,
        sharedInfoTimer
      };
    },
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    saveSelectedCaller: (
      draftState,
      action: PayloadAction<SaveSelectedCallerPayload>
    ) => {
      const { storeId, data, authData } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        selectedCaller: data,
        selectedAuthenticationData: authData
      };
    },
    toggleLoading: (draftState, action: PayloadAction<StoreIdPayload>) => {
      draftState[action.payload.storeId] = {
        ...draftState[action.payload.storeId],
        isLoading: !draftState[action.payload.storeId]?.isLoading
      };
    }
  },
  extraReducers: builder => {
    getAccountDetailBuilder(builder);
    getSecureInfoBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getAccountDetail,
  getClientInfoData,
  getSecureInfoData
};

export { allActions as accountDetailActions, reducer as accountDetail };
