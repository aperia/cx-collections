import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// Helper
import { getFeatureConfig, mappingDataFromObj } from 'app/helpers';
import { parseRelationshipSearchText, prepareReturnData } from '../helpers';

// Service
import accountDetailService from '../accountDetailService';
import relatedService from '../RelatedAccount/relatedService';

// Const
import { EMPTY_OBJECT, EMPTY_STRING, ZERO } from 'app/constants';
import { RELATED_ACCOUNT_CONFIG } from '../RelatedAccount/constants';

import { SELECTED_FIELDS_ACCOUNT_DETAIL_REQUEST } from '../constants';

// Types
import {
  GetAccountDetailPayload,
  GetAccountDetailArgs,
  AccountDetailInitialState,
  AccountInfo
} from '../types';
import { RelatedAccountConfig } from 'app/global-types/relatedAccount';

// Actions
import { timerWorkingActions } from 'pages/TimerWorking/_redux/reducers';
import { renewTokenActions } from 'pages/__commons/RenewToken/_redux/reducer';
import { getDefaultTokenBasedOnHostName } from 'app/helpers/setupInterceptor';
import { tabActions } from 'pages/__commons/TabBar/_redux';

export const getAccountDetail = createAsyncThunk<
  GetAccountDetailPayload,
  GetAccountDetailArgs,
  ThunkAPIConfig
>('account/getAccountDetailInfo', async (args, thunkAPI) => {
  const { mapping } = thunkAPI.getState();
  const { commonConfig } = window.appConfig;

  const { dispatch } = thunkAPI;
  const {
    postData,
    socialSecurityIdentifier = EMPTY_STRING,
    storeId,
    triggerRenewToken
  } = args;

  const { common } = postData;

  if (triggerRenewToken) {
    const defaultToken = dispatch(getDefaultTokenBasedOnHostName());
    await dispatch(
      renewTokenActions.getRenewToken({
        storeId,
        payload: {
          jwt: defaultToken || EMPTY_STRING,
          common: {
            applicationId: commonConfig?.app || EMPTY_STRING,
            accountId: common?.accountId || ''
          }
        }
      })
    );
  }

  const { accountDetail: accountDetailMapping = EMPTY_OBJECT } = mapping?.data;

  const {
    selectFields: relatedAccSelectFields,
    defaultFields: relatedAccDefaultFields
  } = await getFeatureConfig<RelatedAccountConfig>(RELATED_ACCOUNT_CONFIG);

  // Get relationship search text from social security number
  const relationshipSearchText = parseRelationshipSearchText(
    socialSecurityIdentifier
  );

  const accountDetailResponse = await accountDetailService.getAccountDetails({
    common,
    selectFields: SELECTED_FIELDS_ACCOUNT_DETAIL_REQUEST
  });

  const { data: accountDetailData } = accountDetailResponse;

  // start timer
  dispatch(timerWorkingActions.startWorking(storeId));

  const [customerInquiry] = accountDetailData?.customerInquiry || [];
  const [cardholderInfo] = accountDetailData?.cardholderInfo || [];
  const collectionStatus = accountDetailData?.collectionStatus || {};
  const collectionHistory = accountDetailData?.collectionHistory || [];

  let rawData: MagicKeyValue = {
    ...customerInquiry,
    ...cardholderInfo,
    promiseToPayDate: collectionStatus?.promiseToPayDate,
    promiseToPayAmount: collectionStatus?.promiseToPayAmount,
    secondaryName: cardholderInfo?.secondaryName,
    nextHeldStatementDestinationCode:
      customerInquiry?.nextHeldStatementDestinationCode,
    collectionHistory,
    collectionStatus
  };

  try {
    const relatedAccountResponse = await relatedService.getRelatedAccount({
      ...relatedAccDefaultFields,
      relationshipClientIdentifier: customerInquiry?.clientIdentifier,
      relationshipSearchText,
      selectFields: relatedAccSelectFields
    });

    const { data: relatedAccountData } = relatedAccountResponse;

    rawData = {
      ...rawData,
      relatedAccounts: relatedAccountData?.accounts?.length || ZERO
    };
  } catch (error) {
    rawData = {
      ...rawData,
      relatedAccounts: ZERO
    };
  }

  const mappedData = mappingDataFromObj<AccountInfo>(
    rawData,
    accountDetailMapping
  );

  const preparedData = prepareReturnData(mappedData);
  dispatch(
    tabActions.updateTabTitle({
      storeId,
      primaryName: preparedData.primaryName!
    })
  );

  return {
    data: preparedData,
    rawData,
    customerInquiry: accountDetailData?.customerInquiry || []
  };
});

/**
 * - get search account list extra reducers builder
 */
export const getAccountDetailBuilder = (
  builder: ActionReducerMapBuilder<AccountDetailInitialState>
) => {
  builder
    .addCase(getAccountDetail.pending, (draftState, action) => {
      const { storeId, isReload } = action.meta.arg;

      if (isReload) {
        draftState[storeId] = {
          ...draftState[storeId],
          isReloading: true,
          isLoading: false,
          isError: false
        };

        return;
      }

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true,
        isError: false
      };
    })
    .addCase(getAccountDetail.fulfilled, (draftState, action) => {
      const { storeId, isReload } = action.meta.arg;
      const { data, rawData, customerInquiry } = action.payload;

      if (isReload) {
        draftState[storeId] = {
          ...draftState[storeId],
          isReloading: false,
          isLoading: false,
          isError: false,
          data,
          rawData,
          customerInquiry
        };

        return;
      }

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: false,
        data,
        rawData,
        customerInquiry
      };
    })
    .addCase(getAccountDetail.rejected, (draftState, action) => {
      const { storeId, isReload } = action.meta.arg;

      if (isReload) {
        draftState[storeId] = {
          ...draftState[storeId],
          isReloading: false,
          isLoading: false,
          isError: true
        };

        return;
      }

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: true
      };
    });
};
