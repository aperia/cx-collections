import { applyMiddleware, createStore } from 'redux';

import { rootReducer } from 'storeConfig';
import { storeId, responseDefault } from 'app/test-utils';
import { getSecureInfoData } from './getSecureInfoById';
import thunk from 'redux-thunk';
import accountDetailService from '../accountDetailService';

const requestBody = {
  storeId
};

describe('Test getSecureInfoById async thunk', () => {
  it('fulfilled', async () => {
    const store = createStore(
      rootReducer,
      {
        accountDetail: {
          [storeId]: {
            rawData: {
              clientIdentifier: 'a',
              systemIdentifier: 'a',
              principalIdentifier: 'a',
              agentIdentifier: 'a'
            }
          }
        }
      },
      applyMiddleware(thunk)
    );

    jest
      .spyOn(accountDetailService, 'getAuthenticationConfig')
      .mockResolvedValueOnce({
        ...responseDefault,
        data: {
          responseStatus: 'success',
          configs: {
            codeToText: {
              cspas: [
                {
                  cspa: 'a-a-a-a',
                  components: {
                    key: '1'
                  }
                }
              ]
            }
          }
        }
      });

    const response = await getSecureInfoData(requestBody)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      configs: {
        codeToText: { cspas: [{ components: { key: '1' }, cspa: 'a-a-a-a' }] }
      },
      responseStatus: 'success'
    });
    expect(response.type).toEqual('accountDetail/getSecureInfoById/fulfilled');
  });

  it('rejected', async () => {
    const store = createStore(rootReducer, {}, applyMiddleware(thunk));

    jest
      .spyOn(accountDetailService, 'getAuthenticationConfig')
      .mockRejectedValue({
        ...responseDefault,
        data: {
          responseStatus: 'success'
        }
      });

    const response = await getSecureInfoData(requestBody)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      response: {
        config: {},
        data: { responseStatus: 'success' },
        headers: {},
        status: 200,
        statusText: ''
      }
    });
    expect(response.type).toEqual('accountDetail/getSecureInfoById/rejected');
  });
});
