import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import {
  GetSecureInfoReturned
} from 'pages/ClientConfiguration/types';
import { selectClientInfoGetByIdRequest } from 'pages/__commons/AccountDetailTabs/_redux';
import accountDetailService from '../accountDetailService';
import { AccountDetailInitialState } from '../types';

export interface GetSecureInfoDataArgs
  extends StoreIdPayload { }

export const getSecureInfoData = createAsyncThunk<
  GetSecureInfoReturned,
  GetSecureInfoDataArgs,
  ThunkAPIConfig
>('accountDetail/getSecureInfoById', async (args, thunkAPI) => {
  const { getState } = thunkAPI;
  const { storeId } = args;

  try {
    const commonSecureInfoRequest = selectClientInfoGetByIdRequest(
      getState(),
      storeId
    );

    const { data } = await accountDetailService.getAuthenticationConfig(
      commonSecureInfoRequest
    );

    return data;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const getSecureInfoBuilder = (
  builder: ActionReducerMapBuilder<AccountDetailInitialState>
) => {
  builder
    .addCase(getSecureInfoData.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true
      };
    })
    .addCase(getSecureInfoData.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const data = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: false,
        secureInfo: data,
      };
    })
    .addCase(getSecureInfoData.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: true
      };
    });
};
