import { storeId, accEValue, mockActionCreator } from 'app/test-utils';
import { entitlementActions } from 'pages/__commons/Entitlement/_redux/reducers';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import { accountDetailActions } from './reducer';
import { triggerGetAccountDetail } from './triggerGetAccountDetail';

const params = {
  storeId,
  accEValue,
  memberSequenceIdentifier: 'memberSequenceIdentifier',
  socialSecurityIdentifier: 'socialSecurityIdentifier',
  queueId: '123'
};

const accountSpy = mockActionCreator(accountDetailActions);
const entiSpy = mockActionCreator(entitlementActions);

describe('triggerGetAccountDetail', () => {
  it('triggerGetAccountDetail', async () => {
    const spy1 = accountSpy('getAccountDetail');
    const spy2 = entiSpy('getContactEntitlement');
    const store = createStore(
      rootReducer,
      {
        accountDetail: {
          [storeId]: {
            selectedCaller: {
              isByPass: true,
            }
          }
        }
      },
      applyMiddleware(thunk)
    );

    await triggerGetAccountDetail(params)(store.dispatch, store.getState, {});

    expect(spy1).toBeCalledWith({
      postData: {
        common: {
          accountId: 'accEValue'
        },
        inquiryFields: {
          memberSequenceIdentifier: 'memberSequenceIdentifier'
        }
      },
      socialSecurityIdentifier: 'socialSecurityIdentifier',
      storeId: '123456789',
      triggerRenewToken: true
    });
    expect(spy2).toBeCalledWith({
      accountId: 'accEValue',
      agent: '',
      clientNumber: '',
      customerRoleTypeCode: '01',
      prin: '',
      storeId: '123456789',
      system: ''
    });
  });

  it('triggerGetAccountDetail with empty data', async () => {
    const spy1 = accountSpy('getAccountDetail');
    const spy2 = entiSpy('getContactEntitlement');
    const store = createStore(
      rootReducer,
      {
        accountDetail: {}
      },
      applyMiddleware(thunk)
    );

    await triggerGetAccountDetail(params)(store.dispatch, store.getState, {});

    expect(spy1).toBeCalledWith({
      postData: {
        common: {
          accountId: 'accEValue'
        },
        inquiryFields: {
          memberSequenceIdentifier: 'memberSequenceIdentifier'
        }
      },
      socialSecurityIdentifier: 'socialSecurityIdentifier',
      storeId: '123456789',
      triggerRenewToken: true
    });
    expect(spy2).toBeCalledWith({
      accountId: 'accEValue',
      agent: '',
      clientNumber: '',
      customerRoleTypeCode: undefined,
      prin: '',
      storeId: '123456789',
      system: ''
    });
  });
});
