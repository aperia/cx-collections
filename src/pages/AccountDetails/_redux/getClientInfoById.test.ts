import { createStore } from 'redux';
import { isFulfilled, isRejected } from '@reduxjs/toolkit';

import { rootReducer } from 'storeConfig';
import { storeId, responseDefault } from 'app/test-utils';
import { ClientInfoByIdResponse } from 'pages/ClientConfiguration/types';
import { getClientInfoData, GetClientInfoDataArgs } from './getClientInfoById';
import { clientConfigurationServices } from 'pages/ClientConfiguration/clientConfigurationServices';

describe('Test getClientInfoById async thunk', () => {
  describe('should be fulilled', () => {
    it('when fieldsToQuery argument is an array', async () => {
      const dataType = 'emailTemplates';
      const store = createStore(rootReducer, {});
      const fieldsToQuery = ['emailTemplates', 'clientInfoLetterTemplate'];
      const requestBody = {
        storeId,
        dataType,
        fieldsToQuery
      } as GetClientInfoDataArgs;

      jest
        .spyOn(clientConfigurationServices, 'getClientInfoById')
        .mockResolvedValueOnce({
          ...responseDefault,
          data: {
            responseStatus: 'success',
            clientInfo: {
              emailTemplates: []
            } as unknown as ClientInfoByIdResponse['clientInfo']
          }
        });

      const response = await getClientInfoData(requestBody)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(isFulfilled(response)).toBeTruthy();
      expect(response.payload).toEqual({
        [dataType]: []
      });
    });

    it('when fieldsToQuery argument is NOT an array', async () => {
      const dataType = 'emailTemplates';
      const fieldsToQuery = 'emailTemplates';
      const store = createStore(rootReducer, {});
      const requestBody = {
        storeId,
        dataType,
        fieldsToQuery
      } as GetClientInfoDataArgs;

      jest
        .spyOn(clientConfigurationServices, 'getClientInfoById')
        .mockResolvedValueOnce({
          ...responseDefault,
          data: {
            responseStatus: 'success',
            clientInfo: {
              emailTemplates: []
            } as unknown as ClientInfoByIdResponse['clientInfo']
          }
        });

      const response = await getClientInfoData(requestBody)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(isFulfilled(response)).toBeTruthy();
      expect(response.payload).toEqual({
        [dataType]: []
      });
    });

    it('when fieldsToQuery argument is NOT defnied', async () => {
      const dataType = 'emailTemplates';
      const requestBody = {
        storeId,
        dataType
      } as GetClientInfoDataArgs;
      const store = createStore(rootReducer, {});

      jest
        .spyOn(clientConfigurationServices, 'getClientInfoById')
        .mockResolvedValueOnce({
          ...responseDefault,
          data: {
            responseStatus: 'success',
            clientInfo: {
              emailTemplates: []
            } as unknown as ClientInfoByIdResponse['clientInfo']
          }
        });

      const response = await getClientInfoData(requestBody)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(isFulfilled(response)).toBeTruthy();
      expect(response.payload).toEqual({
        [dataType]: []
      });
    });

    it('when API service does NOT include clientInfo field', async () => {
      const dataType = 'emailTemplates';
      const store = createStore(rootReducer, {});
      const fieldsToQuery = ['emailTemplates', 'clientInfoLetterTemplate'];
      const requestBody = {
        storeId,
        dataType,
        fieldsToQuery
      } as GetClientInfoDataArgs;

      jest
        .spyOn(clientConfigurationServices, 'getClientInfoById')
        .mockResolvedValueOnce({
          ...responseDefault,
          data: {
            responseStatus: 'success'
          } as ClientInfoByIdResponse
        });

      const response = await getClientInfoData(requestBody)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(isFulfilled(response)).toBeTruthy();
      expect(response.payload).toEqual({
        [dataType]: undefined
      });
    });
  });

  describe('should be rejected', () => {
    it('when API service failed to call', async () => {
      const store = createStore(rootReducer, {});
      const requestBody = {} as GetClientInfoDataArgs;

      jest
        .spyOn(clientConfigurationServices, 'getClientInfoById')
        .mockRejectedValueOnce({});

      const response = await getClientInfoData(requestBody)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(isRejected(response)).toBeTruthy();
    });

    it('when API service return error in response', async () => {
      const dataType = 'emailTemplates';
      const store = createStore(rootReducer, {});
      const fieldsToQuery = ['emailTemplates', 'clientInfoLetterTemplate'];
      const requestBody = {
        storeId,
        dataType,
        fieldsToQuery
      } as GetClientInfoDataArgs;

      jest
        .spyOn(clientConfigurationServices, 'getClientInfoById')
        .mockResolvedValueOnce({
          ...responseDefault,
          data: {
            responseStatus: 'failure'
          } as ClientInfoByIdResponse
        });

      const response = await getClientInfoData(requestBody)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(isRejected(response)).toBeTruthy();
    });
  });
});
