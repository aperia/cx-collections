import { CUSTOMER_ROLE_CODE } from 'app/constants/index';
import { createSelector } from '@reduxjs/toolkit';
import { useMemo } from 'react';
import { useStoreIdSelector } from 'app/hooks';
import { isUndefined } from 'lodash';

export const getAccountDetail = (state: RootState, storeId: string) => {
  return state.accountDetail[storeId]?.data;
};

export const getRefreshStatus = (state: RootState, storeId: string) => {
  if (isUndefined(storeId)) return undefined;
  return state.accountDetail[storeId]?.isRefreshAccDetail;
};

export const getMonetaryData = (state: RootState, storeId: string) =>
  state.accountDetail[storeId]?.data?.monetary;

export const getHistoricalData = (state: RootState, storeId: string) =>
  state.accountDetail[storeId]?.data?.historical;

export const getAccountDetailLoadingStatus = (
  state: RootState,
  storeId: string
) => {
  return state.accountDetail[storeId]?.isLoading;
};

export const getAccountDetailReloadingStatus = (
  state: RootState,
  storeId: string
) => {
  return state.accountDetail[storeId]?.isReloading;
};

export const getAccountDetailErrorStatus = (
  state: RootState,
  storeId: string
) => {
  return state.accountDetail[storeId]?.isError;
};

export const getAccountDetailInternalStatusCode = (
  state: RootState,
  storeId: string
) => {
  return state.accountDetail[storeId]?.data?.internalStatusCode;
};

// ----------Selector----------------------------
export const takeAccountDetail = createSelector(getAccountDetail, data => data);

export const takeRefreshAcc = createSelector(getRefreshStatus, data => data);

export const takeAccountDetailLoadingStatus = createSelector(
  getAccountDetailLoadingStatus,
  data => data
);

export const takeAccountDetailReloadingStatus = createSelector(
  getAccountDetailReloadingStatus,
  data => data
);

export const takeAccountDetailErrorStatus = createSelector(
  getAccountDetailErrorStatus,
  data => data
);

export const takeCurrentExternalStatus = createSelector(
  getAccountDetail,
  data => data?.externalStatusCode
);

export const takeDestinationCode = createSelector(
  getAccountDetail,
  data => data?.nextHeldStatementDestinationCode
);

export const takeMonetaryData = createSelector(getMonetaryData, data => data);

export const takeHistoricalData = createSelector(
  getHistoricalData,
  data => data
);

export const getHasOnlyPrimaryCardholder = (
  state: RootState,
  storeId: string
) => {
  return (
    state.accountDetail[storeId]?.customerInquiry?.findIndex(
      customer => customer.customerRoleTypeCode === CUSTOMER_ROLE_CODE.secondary
    ) === -1
  );
};

export const takeHasOnlyPrimaryCardholder = createSelector(
  getHasOnlyPrimaryCardholder,
  data => data
);

export const takeAccountPrimaryName = createSelector(
  getAccountDetail,
  data => data?.primaryName
);
const takeAccounSecondaryName = createSelector(
  getAccountDetail,
  data => data?.secondaryName
);

export const useFilerNameDropdownData = () => {
  const primaryName = useStoreIdSelector(takeAccountPrimaryName);
  const secondaryName = useStoreIdSelector(takeAccounSecondaryName);

  const result = useMemo(
    () =>
      [
        {
          value: primaryName,
          description: `Primary • ${primaryName}`
        },
        {
          value: secondaryName,
          description: `Secondary • ${secondaryName}`
        }
      ].filter(a => a.value) as RefDataValue[],
    [primaryName, secondaryName]
  );

  return result;
};

export const refData = createSelector(
  (states: RootState) => states.refData,
  (data: any) => data
);
