import {
  responseDefault,
  storeId,
  createStoreWithDefaultMiddleWare,
  mockActionCreator
} from 'app/test-utils';
import { getAccountDetail } from './getAccountDetail';
import accountDetailService from '../accountDetailService';
import { SELECTED_FIELDS_ACCOUNT_DETAIL_REQUEST } from '../constants';
import * as commonActions from 'app/helpers/commons';
import relatedService from '../RelatedAccount/relatedService';
import { isFulfilled, isRejected } from '@reduxjs/toolkit';
import { renewTokenActions } from 'pages/__commons/RenewToken/_redux/reducer';

const mockRenewTokenActions = mockActionCreator(renewTokenActions);

describe('Test getAccountDetail', () => {
  beforeEach(() => {
    jest.spyOn(commonActions, 'getFeatureConfig').mockResolvedValue({
      ...responseDefault,
      selectFields: 'selectFields',
      defaultFields: 'defaultFields'
    });
  });

  it('should be fulfilled triggerRenewToken false', async () => {
    const requestBody = {
      storeId,
      postData: {
        common: {}
      },
      isReload: true,
      triggerRenewToken: true,
      selectFields: SELECTED_FIELDS_ACCOUNT_DETAIL_REQUEST
    };
    const getRenewToken = mockRenewTokenActions('getRenewToken');
    window.appConfig = {} as AppConfiguration;
    jest
      .spyOn(accountDetailService, 'getAccountDetails')
      .mockResolvedValue({ ...responseDefault, data: {} });
    jest
      .spyOn(relatedService, 'getRelatedAccount')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const store = createStoreWithDefaultMiddleWare({
      mapping: { data: { accountDetail: {} }, loading: false }
    });

    const response = await getAccountDetail(requestBody)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(isFulfilled(response)).toBeTruthy();
    expect(getRenewToken).toHaveBeenCalled();
  });

  it('should be fulfilled triggerRenewToken false', async () => {
    const requestBody = {
      storeId,
      postData: {
        common: {}
      },
      isReload: false,
      triggerRenewToken: false,
      selectFields: SELECTED_FIELDS_ACCOUNT_DETAIL_REQUEST
    };
    window.appConfig = {} as AppConfiguration;
    const getRenewToken = mockRenewTokenActions('getRenewToken');

    jest
      .spyOn(accountDetailService, 'getAccountDetails')
      .mockResolvedValue({ ...responseDefault, data: {} });
    jest.spyOn(relatedService, 'getRelatedAccount').mockRejectedValue({});

    const store = createStoreWithDefaultMiddleWare({
      mapping: { data: {}, loading: false },
      tab: {
        tabStoreIdSelected: '1111',
        tabs: [
          {
            storeId: '1111',
            title: '1111',
            tabType: 'accountDetail',
            className: '1111'
          }
        ]
      }
    });

    const response = await getAccountDetail(requestBody)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(isFulfilled(response)).toBeTruthy();
    expect(getRenewToken).not.toHaveBeenCalled();
  });

  it('should be rejected isReload false', async () => {
    const requestBody = {
      storeId,
      postData: {
        common: {}
      },
      isReload: false,
      triggerRenewToken: false,
      selectFields: SELECTED_FIELDS_ACCOUNT_DETAIL_REQUEST
    };
    window.appConfig = {} as AppConfiguration;

    jest.spyOn(accountDetailService, 'getAccountDetails').mockRejectedValue({});
    jest.spyOn(relatedService, 'getRelatedAccount').mockRejectedValue({});

    const store = createStoreWithDefaultMiddleWare({
      mapping: { data: {}, loading: false },
      tab: {
        tabStoreIdSelected: '1111',
        tabs: []
      }
    });

    const response = await getAccountDetail(requestBody)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(isRejected(response)).toBeTruthy();
  });
  it('should be rejected isReload true', async () => {
    const requestBody = {
      storeId,
      postData: {
        common: {}
      },
      isReload: true,
      triggerRenewToken: false,
      selectFields: SELECTED_FIELDS_ACCOUNT_DETAIL_REQUEST
    };
    window.appConfig = {} as AppConfiguration;

    jest.spyOn(accountDetailService, 'getAccountDetails').mockRejectedValue({});
    jest.spyOn(relatedService, 'getRelatedAccount').mockRejectedValue({});

    const store = createStoreWithDefaultMiddleWare({
      mapping: { data: {}, loading: false }
    });

    const response = await getAccountDetail(requestBody)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(isRejected(response)).toBeTruthy();
  });
});
