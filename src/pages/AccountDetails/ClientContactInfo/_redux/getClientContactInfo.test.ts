import { rootReducer } from 'storeConfig';
import { createStore } from 'redux';

import './index';
import { getClientContactInfo } from './getClientContactInfo';
import { AxiosResponse } from 'axios';
import { storeId } from 'app/test-utils';
import clientContactService from '../clientContactService';
import { clientPhoneAddressAction, initialStateClientContact } from './';

const requestData = {
  storeId,
  postData: { accountNumber: storeId }
};

const responseDefault: AxiosResponse<any> = {
  data: null,
  status: 200,
  statusText: '',
  headers: {},
  config: {}
};

const store = createStore(rootReducer, {
  accountDetail: {
    [storeId]: {
      data: {
        clientID: '',
        sysPrinAgent: '1111/1111/1111'
      }
    }
  },
  clientContactInfo: {
    [storeId]: {}
  }
});

describe('redux-store > getClientContactInfo', () => {
  it('async thunk', async () => {
    jest.spyOn(clientContactService, 'getClientContactInfo').mockResolvedValue({
      ...responseDefault,
      data: {
        panelContentModel: {
          panelContentText: 'panelContentText'
        }
      }
    });
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app'
      }
    };
    const response = await getClientContactInfo(requestData)(
      store.dispatch,
      store.getState,
      { storeId }
    );
    expect(response.payload).toEqual({
      clientContactInfo: 'panelContentText'
    });

    expect(response.type).toEqual(
      'cardholderMaintenance/getClientContactInfo/fulfilled'
    );
  });

  it('Should have pending', async () => {
    const action = clientPhoneAddressAction.getClientContactInfo.pending(
      'cardholderMaintenance/getClientContactInfo',
      { storeId }
    );
    const state = rootReducer(store.getState(), action).clientContactInfo;
    expect(state[storeId]).toEqual(initialStateClientContact);
  });

  it('Should have fulfilled', async () => {
    const action = clientPhoneAddressAction.getClientContactInfo.fulfilled(
      { clientContactInfo: 'clientContactInfo' },
      'cardholderMaintenance/getClientContactInfo',
      { storeId }
    );
    const state = rootReducer(store.getState(), action).clientContactInfo;
    expect(state[storeId].isLoading).toEqual(false);
    expect(state[storeId].clientContactInfo).toEqual('clientContactInfo');
  });

  it('Should have rejected', async () => {
    const action = clientPhoneAddressAction.getClientContactInfo.rejected(
      null,
      'cardholderMaintenance/getClientContactInfo',
      { storeId }
    );
    const state = rootReducer(store.getState(), action).clientContactInfo;
    expect(state[storeId].isLoading).toEqual(false);
  });
});
