import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

// types
import { IClientContactInitialState } from '../types';

// service
import clientPhoneAddressService from '../clientContactService';
import { initialStateClientContact } from '.';
import { EMPTY_STRING } from 'app/constants';

export const getClientContactInfo = createAsyncThunk<any, any, ThunkAPIConfig>(
  'cardholderMaintenance/getClientContactInfo',
  async (args, thunkAPI) => {
    const { storeId } = args;
    const states = thunkAPI.getState();
    const { clientID, sysPrinAgent } =
      states.accountDetail[storeId]?.data ?? {};
    const [system, prin, agent] = (sysPrinAgent ?? '0000/0000/0000').split('/');
    const { commonConfig } = window.appConfig || {};
    const requestData = {
      common: {
        clientNumber: clientID,
        agent,
        prin,
        system,
        app: commonConfig?.app || EMPTY_STRING,
        clientName: 'firstdata'
      },
      panelContentId: 'ClientInformationTemplate'
    };
    try {
      const { data } = await clientPhoneAddressService.getClientContactInfo(
        requestData
      );
      return { clientContactInfo: data?.panelContentModel?.panelContentText };
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const getClientContactInfoBuilder = (
  builder: ActionReducerMapBuilder<IClientContactInitialState>
) => {
  builder
    .addCase(getClientContactInfo.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = initialStateClientContact;
    })
    .addCase(getClientContactInfo.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { clientContactInfo } = action.payload;
      draftState[storeId].isLoading = false;
      draftState[storeId].clientContactInfo = clientContactInfo;
    })
    .addCase(getClientContactInfo.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId].isLoading = false;
    });
};
