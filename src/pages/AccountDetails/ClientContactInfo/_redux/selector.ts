import { createSelector } from '@reduxjs/toolkit';

export const selectLoading = createSelector(
  (states: RootState, storeId: string) =>
    states.clientContactInfo[storeId]?.isLoading,
  (isLoading: boolean) => isLoading
);

export const selectClientContactInfo = createSelector(
  (states: RootState, storeId: string) =>
    states.clientContactInfo[storeId]?.clientContactInfo,
  (clientContactInfo: string) => clientContactInfo
);
