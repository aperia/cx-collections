import { createSlice } from '@reduxjs/toolkit';

import {
  getClientContactInfo,
  getClientContactInfoBuilder
} from './getClientContactInfo';
// types
import { IClientContactInitialState, IClientContactState } from '../types';

const { actions, reducer } = createSlice({
  name: 'clientContact',
  initialState: {} as IClientContactInitialState,
  reducers: {},
  extraReducers: builder => {
    getClientContactInfoBuilder(builder);
  }
});

const clientPhoneAddressAction = {
  ...actions,
  getClientContactInfo
};

export const initialStateClientContact = {
  isLoading: true,
  clientContactInfo: ''
} as IClientContactState;

export { clientPhoneAddressAction, reducer };
