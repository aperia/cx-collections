export interface IClientContactState {
  isLoading: boolean;
  clientContactInfo: string;
}

export interface IClientContactInitialState {
  [storeId: string]: IClientContactState;
}
