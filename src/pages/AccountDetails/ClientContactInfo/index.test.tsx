import React from 'react';
import {
  mockActionCreator,
  queryByClass,
  renderMockStoreId,
  storeId
} from 'app/test-utils';
import PhoneAndAddressList from './';
import { screen } from '@testing-library/dom';
import { InfoBarSection, actionsInfoBar } from 'pages/__commons/InfoBar/_redux';
import { waitFor } from '@testing-library/react';
import { TogglePinProps } from 'pages/__commons/InfoBar/TogglePin';

const renderWrapper = (initialState: Partial<RootState>) => {
  const { wrapper } = renderMockStoreId(<PhoneAndAddressList />, {
    initialState
  });

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement
  };
};

const infoBarSpy = mockActionCreator(actionsInfoBar);

jest.mock(
  'pages/__commons/InfoBar/TogglePin',
  () => (props: TogglePinProps) => {
    return <div onClick={props.onToggle}>TogglePin</div>;
  }
);

describe('ClientContactInfo component', (): void => {
  it('Render UI', () => {
    const initialState = {
      clientContactInfo: {
        [storeId]: { isLoading: true, clientContactInfo: '' }
      }
    };
    const { wrapper } = renderMockStoreId(<PhoneAndAddressList />, {
      initialState
    });

    expect(
      queryByClass(wrapper.container, /loading h-100/)
    ).toBeInTheDocument();
    expect(wrapper.getByText(/txt_client_contact_info/)).toBeInTheDocument();
  });

  describe('Action', () => {
    it('Toggle unpin', () => {
      const initialState = {
        clientContactInfo: {
          [storeId]: { isLoading: false, clientContactInfo: '' }
        },
        infoBar: { [storeId]: { pinned: InfoBarSection.ClientContactInfo } }
      };

      const updatePinnedSpy = infoBarSpy('updatePinned');
      const updateIsExpandSpy = infoBarSpy('updateIsExpand');

      jest.useFakeTimers();
      const { wrapper } = renderWrapper(initialState);
      jest.runAllTimers();

      expect(wrapper.getByText(/txt_client_contact_info/)).toBeInTheDocument();

      const toggleElement = screen.getByText('TogglePin');
      waitFor(() => {
        toggleElement.click();
      });

      expect(updatePinnedSpy).toBeCalledWith({ storeId, pinned: null });
      expect(updateIsExpandSpy).toBeCalledWith({ storeId, isExpand: false });
    });

    it('Toggle unpin', () => {
      const initialState = {
        clientContactInfo: {
          [storeId]: { isLoading: false, clientContactInfo: '' }
        },
        infoBar: { [storeId]: { pinned: null } }
      };

      const updatePinnedSpy = infoBarSpy('updatePinned');
      const updateRecoverSpy = infoBarSpy('updateRecover');
      const updateIsExpandSpy = infoBarSpy('updateIsExpand');

      jest.useFakeTimers();
      const { wrapper } = renderWrapper(initialState);
      jest.runAllTimers();

      expect(wrapper.getByText(/txt_client_contact_info/)).toBeInTheDocument();

      const toggleElement = screen.getByText('TogglePin');
      waitFor(() => {
        toggleElement.click();
      });

      expect(updatePinnedSpy).toBeCalledWith({
        storeId,
        pinned: InfoBarSection.ClientContactInfo
      });
      expect(updateRecoverSpy).toBeCalledWith({ storeId, recover: null });
      expect(updateIsExpandSpy).toBeCalledWith({ storeId, isExpand: true });
    });
  });
});
