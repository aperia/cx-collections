import clientContactService from './clientContactService';

// utils
import { storeId, mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

describe('clientContactService', () => {
  describe('getClientContactInfo', () => {
    const params = { storeId };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      clientContactService.getClientContactInfo(params);

      expect(mockService).toBeCalledWith(undefined, params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      clientContactService.getClientContactInfo(params);

      expect(mockService).toBeCalledWith(
        apiUrl.clientContactInfo.getClientContactInfo,
        params
      );
    });
  });
});
