import React, { useEffect, useRef } from 'react';

// components
import TogglePin from 'pages/__commons/InfoBar/TogglePin';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { batch, useDispatch } from 'react-redux';
import {
  selectClientContactInfo,
  selectLoading
} from 'pages/AccountDetails/ClientContactInfo/_redux/selector';
import { clientPhoneAddressAction } from 'pages/AccountDetails/ClientContactInfo/_redux';
import { selectPinned } from 'pages/__commons/InfoBar/_redux/selectors';
import { actionsInfoBar, InfoBarSection } from 'pages/__commons/InfoBar/_redux';

// helpers
import classNames from 'classnames';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { useRenderElement } from 'app/_libraries/_dls/hooks';
import { markdownToHTML } from 'app/_libraries/_dls/components/TextEditor';

export interface PhoneAndAddressListProps {}
export interface PhoneAndAddressListProps {}

const PhoneAndAddressList: React.FC<PhoneAndAddressListProps> = () => {
  const dispatch = useDispatch();
  const elementRef = useRef<HTMLDivElement | null>(null);
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const testId = 'infobar_clientContactinfo';

  const isLoading = useStoreIdSelector<boolean>(selectLoading);
  const clientContactInfo = useStoreIdSelector<string>(selectClientContactInfo);
  const pinned = useStoreIdSelector(selectPinned);
  const isPinned = pinned === InfoBarSection.ClientContactInfo;

  const handleTogglePin = () => {
    batch(() => {
      const thisPinned = isPinned ? null : InfoBarSection.ClientContactInfo;

      dispatch(
        actionsInfoBar.updatePinned({
          storeId,
          pinned: thisPinned
        })
      );

      if (thisPinned) {
        dispatch(actionsInfoBar.updateRecover({ storeId, recover: null }));
      }

      dispatch(actionsInfoBar.updateIsExpand({ storeId, isExpand: !isPinned }));
    });
  };

  useEffect(() => {
    dispatch(clientPhoneAddressAction.getClientContactInfo({ storeId }));
  }, [dispatch, storeId]);

  useRenderElement(
    elementRef,
    markdownToHTML(clientContactInfo).element as HTMLElement
  );

  return (
    <div
      data-testid={genAmtId(testId, 'container', '')}
      className={classNames({ loading: isLoading }, 'h-100')}
    >
      <div className="d-flex justify-content-between pt-24 px-24 pb-16">
        <div className="d-inline-flex">
          <h4 className="mr-8" data-testid={genAmtId(testId, 'title', '')}>
            {t('txt_client_contact_info')}
          </h4>
          <TogglePin
            isPinned={isPinned}
            onToggle={handleTogglePin}
            dataTestId={testId}
          />
        </div>
      </div>

      <div
        className="px-24"
        ref={elementRef}
        data-testid={genAmtId(testId, 'content', '')}
      ></div>
    </div>
  );
};

export default PhoneAndAddressList;
