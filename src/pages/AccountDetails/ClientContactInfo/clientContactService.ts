import { apiService } from 'app/utils/api.service';

const clientContactService = {
  getClientContactInfo: (postData: any) => {
    const url = window.appConfig?.api?.clientContactInfo?.getClientContactInfo;

    return apiService.post(url!, { ...postData });
  }
};

export default clientContactService;
