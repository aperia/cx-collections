import {
  Modal,
  ModalTitle,
  ModalBody,
  ModalFooter,
  ModalHeader
} from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import React from 'react';

const testId = 'account-extend-lock-modal';
interface AccountExtendLockModalType {
  extendTime: number | undefined;
  time: string[];
  show: boolean;
  toggle: () => void;
  onSubmit: () => void;
}
const AccountExtendLockModal: React.FC<AccountExtendLockModalType> = ({
  extendTime,
  time,
  show,
  toggle,
  onSubmit
}) => {
  const { t } = useTranslation();
  const [toStringMin = '', toStringSec = ''] = time;

  if (!show) return null;
  return (
    <Modal xs show={show} dataTestId={genAmtId(testId, '', '')}>
      <ModalHeader border onHide={toggle}>
        <ModalTitle dataTestId={genAmtId(testId, 'header-title', '')}>
          {t('txt_confirm_account_lock_title')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody dataTestId={genAmtId(testId, 'body', '')}>
        <div className="count-down">
          <div className="count-down-box">
            <span className="count-down-time">{toStringMin}</span>
            <span className="count-down-title">minutes</span>
          </div>
          <div className="count-down-box">
            <span className="count-down-time">{toStringSec}</span>
            <span className="count-down-title">seconds</span>
          </div>
        </div>
        <h5 className="text-center mt-24 mb-16 color-orange-d16">
          {t('txt_confirm_account_lock_header')}
        </h5>
        <p className="text-center">
          {t('txt_confirm_account_lock_body', { time: extendTime })}
        </p>
      </ModalBody>
      <ModalFooter
        okButtonText={t('txt_extend_lock')}
        cancelButtonText={t('txt_no')}
        onCancel={toggle}
        onOk={onSubmit}
        dataTestId={genAmtId(testId, 'footer', '')}
      />
    </Modal>
  );
};

export default AccountExtendLockModal;
