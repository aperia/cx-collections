import React from 'react';
import { screen } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';
import {
  accEValue,
  mockActionCreator,
  renderMockStoreId,
  storeId
} from 'app/test-utils';

import AccountLockTime from '.';

import { dashboardActions } from 'pages/AccountSearch/Home/Dashboard/_redux/reducers';

const mockDashboardAction = mockActionCreator(dashboardActions);

describe('Account Extend Lock Time', () => {
  it('should render modal and click cancel without extendMsg', () => {
    jest.useFakeTimers();
    const mockToggleExtendModal = mockDashboardAction('toggleExtendModal');
    const initialState = {
      dashboard: {
        accountLockTime: 1,
        accountCountTime: {
          [storeId]: {
            toggleExtendModal: true
          }
        }
      },
      nextActions: {
        [accEValue]: {
          data: '123'
        }
      }
    };
    renderMockStoreId(
      <AccountLockTime queueId="1" accountId="2" storeId={storeId} />,
      {
        initialState
      }
    );

    const noButton = screen.getByText('txt_no');

    userEvent.click(noButton);

    expect(mockToggleExtendModal).toBeCalledWith({ storeId });

    expect(noButton).toBeInTheDocument();
  });
  it('should not render modal and without lockTime', () => {
    jest.useFakeTimers();
    const initialState = {
      dashboard: {
        accountLockTime: 0,
        accountCountTime: {
          [storeId]: {
            toggleExtendModal: true
          }
        }
      },
      nextActions: {
        [accEValue]: {
          data: '123'
        }
      }
    };
    renderMockStoreId(
      <AccountLockTime queueId="1" accountId="2" storeId={storeId} />,
      {
        initialState
      }
    );
  });

  it('should not render modal and without lockTime and without accountId', () => {
    jest.useFakeTimers();
    const initialState = {
      dashboard: {
        accountLockTime: 0,
        accountCountTime: {
          [storeId]: {
            toggleExtendModal: true
          }
        }
      },
      nextActions: {
        [accEValue]: {
          data: '123'
        }
      }
    };
    renderMockStoreId(
      <AccountLockTime queueId="1" accountId="" storeId={storeId} />,
      {
        initialState
      }
    );
  });
  it('should render modal and click cancel', () => {
    jest.useFakeTimers();
    const mockToggleExtendModal = mockDashboardAction('toggleExtendModal');
    const initialState = {
      dashboard: {
        accountLockTime: 1,
        accountCountTime: {
          [storeId]: {
            toggleExtendModal: true
          }
        }
      },
      nextActions: {
        [accEValue]: {
          data: '123'
        }
      }
    };
    renderMockStoreId(
      <AccountLockTime queueId="1" accountId="2" storeId={storeId} />,
      {
        initialState
      }
    );

    const noButton = screen.getByText('txt_no');

    userEvent.click(noButton);

    expect(mockToggleExtendModal).toBeCalledWith({ storeId });

    expect(noButton).toBeInTheDocument();
  });

  it('should render modal and click submit with warningUnlockMsg', () => {
    jest.useFakeTimers();

    const mockGetAccountExtendLockTime = mockDashboardAction(
      'getAccountExtendLockTime'
    );

    const initialState: Partial<RootState> = {
      dashboard: {
        accountLockTime: 1,
        accountCountTime: {
          [storeId]: {
            toggleExtendModal: true
          }
        }
      },
      nextActions: {
        [accEValue]: {
          data: '123'
        }
      }
    };
    renderMockStoreId(
      <AccountLockTime queueId="" accountId="2" storeId={storeId} />,
      {
        initialState
      }
    );

    const extendLockButton = screen.getByText('txt_extend_lock');

    userEvent.click(extendLockButton);

    expect(mockGetAccountExtendLockTime).toBeCalledWith({
      accountId: '2',
      storeId
    });
  });
});
