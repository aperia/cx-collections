import React from 'react';
import AccountExtendLockModal from './AccountExtendLockModal';
import { render, screen } from '@testing-library/react';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => () => ({
  t: (text: string) => text
}));

describe('AccountExtendLockModal', () => {
  const mockOnSubmit = jest.fn();
  const mockToggle = jest.fn();

  it('should render AccountExtendLockModal', () => {
    render(
      <AccountExtendLockModal
        show={true}
        time={['02', '02']}
        extendTime="30"
        onSubmit={mockOnSubmit}
        toggle={mockToggle}
      />
    );

    const text = screen.getByText('txt_confirm_account_lock_title');

    expect(text).toBeInTheDocument();
  });

  it('should render AccountExtendLockModal with default props', () => {
    render(
      <AccountExtendLockModal
        show={true}
        time={[]}
        extendTime=""
        onSubmit={mockOnSubmit}
        toggle={mockToggle}
      />
    );

    const text = screen.getByText('txt_confirm_account_lock_title');

    expect(text).toBeInTheDocument();
  });

  it('should render AccountExtendLockModal with show props false', () => {
    const { baseElement } = render(
      <AccountExtendLockModal
        show={false}
        time={[]}
        extendTime=""
        onSubmit={mockOnSubmit}
        toggle={mockToggle}
      />
    );

    expect(baseElement).toBeTruthy();
  });
});
