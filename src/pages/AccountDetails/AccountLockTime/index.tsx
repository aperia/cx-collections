import useCountDownTimer from 'pages/AccountSearch/Home/Dashboard/hooks/useCountDownTimer';
import { dashboardActions } from 'pages/AccountSearch/Home/Dashboard/_redux/reducers';
import {
  takeAccountExtendLockTime,
  takeAccountLockTime,
  takeAccountResetTime
} from 'pages/AccountSearch/Home/Dashboard/_redux/selectors';

import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import AccountExtendLockModal from './AccountExtendLockModal';

interface AccountLockTimeType {
  queueId: string;
  storeId: string;
  accountId: string;
}
const AccountLockTime: React.FC<AccountLockTimeType> = ({
  queueId,
  storeId,
  accountId
}) => {
  const accountLockTime = useSelector(takeAccountLockTime);
  const accountExtendLockTime = useSelector(takeAccountExtendLockTime);
  const isReset = useSelector(takeAccountResetTime);

  const dispatch = useDispatch();
  const { toStringMin, toStringSec, openCountDownModal } = useCountDownTimer(
    accountLockTime,
    accountExtendLockTime,
    queueId,
    storeId,
    isReset
  );

  useEffect(() => {
    dispatch(dashboardActions.resetTime(false));
  }, [dispatch]);

  const toggleExtendModal = React.useCallback(() => {
    dispatch(dashboardActions.toggleExtendModal({ storeId }));
  }, [dispatch, storeId]);

  const handleSubmit = React.useCallback(() => {
    dispatch(dashboardActions.toggleExtendModal({ storeId }));
    dispatch(dashboardActions.getAccountExtendLockTime({ storeId, accountId }));
  }, [dispatch, storeId, accountId]);

  return (
    <>
      <AccountExtendLockModal
        extendTime={accountLockTime}
        time={[toStringMin, toStringSec]}
        show={openCountDownModal}
        toggle={toggleExtendModal}
        onSubmit={handleSubmit}
      />
    </>
  );
};

export default AccountLockTime;
