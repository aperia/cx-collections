import { createSelector } from '@reduxjs/toolkit';

export const selectData = (storeId: string) => {
  return createSelector(
    (states: RootState) => states.accountInformation[storeId]?.data,
    (data: Record<string, object>) => data
  );
};

export const selectIsError = (storeId: string) => {
  return createSelector(
    (states: RootState) => !!states.accountInformation[storeId]?.error,
    (error: boolean) => error
  );
};

export const selectLoading = (storeId: string) => {
  return createSelector(
    (states: RootState) => !!states.accountInformation[storeId]?.loading,
    (loading: boolean) => loading
  );
};

export const selectMapping = () => {
  return createSelector(
    (states: RootState) => states.mapping?.data,
    (data: Object) => data
  );
};
