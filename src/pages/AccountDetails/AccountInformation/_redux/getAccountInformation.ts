import { prepareDateStringFormat } from './../../../../app/helpers/formatTime';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import {
  convertMaskAccount,
  formatDisplayDate,
  mappingDataFromObj,
  parseJSONString
} from 'app/helpers';
// helpers
import isEmpty from 'lodash.isempty';
// service
import accountService from '../accountInformationService';
import { buildAccountActivityHistory, getData } from '../helpers';
// Type
import { Args, GetAccountInformationPayload, InitialState } from '../types';

/**
 * get account detail info
 */
export const getAccountInformation = createAsyncThunk<
  GetAccountInformationPayload,
  Args,
  ThunkAPIConfig
>('account/getAccountInformation', async (args, thunkAPI) => {
  try {
    const state = thunkAPI.getState();
    const { accountNumber } = args.postData;

    const accountMapping = state.mapping?.data?.accountInformation || {};
    const { data } = await accountService.getAccountInformation(accountNumber);

    if (isEmpty(data)) return { data: {} };

    const customers = getData(data);
    let mappedData = mappingDataFromObj<MagicKeyValue>(
      customers,
      accountMapping
    );

    const maskSavingAccount = parseJSONString(
      mappedData.accounts.savingsAccountIdentifier
    )?.maskedValue;

    const maskCheckingAccount = parseJSONString(
      mappedData.accounts.checkingAccount
    )?.maskedValue;

    const mostRecentDisputeDate = formatDisplayDate(
      prepareDateStringFormat(mappedData?.disputed?.mostRecentDisputeDate || '')
    );

    mappedData = {
      ...mappedData,
      accounts: {
        ...mappedData.accounts,
        savingsAccountIdentifier: convertMaskAccount(maskSavingAccount),
        checkingAccount: convertMaskAccount(maskCheckingAccount)
      },
      activityHistory: {
        paymentHistoryCode: buildAccountActivityHistory(
          mappedData?.activityHistory?.paymentHistoryCode,
          args.translateFn
        )
      },
      disputed: {
        ...mappedData.disputed,
        mostRecentDisputeDate
      }
    };

    try {
      for (const key in mappedData?.crossReference) {
        const acc = mappedData?.crossReference[key];
        if (acc?.accInfo) {
          const parsedAccInfo = JSON.parse(acc.accInfo);
          acc.accMaskedValue = parsedAccInfo.maskedValue;
          acc.accEValue = parsedAccInfo.eValue;
        }
      }
    } catch (error) {
      // Do nothing
    }

    return { data: mappedData };
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

/**
 * - get search account list extra reducers builder
 */
export const getAccountInformationBuilder = (
  builder: ActionReducerMapBuilder<InitialState>
) => {
  builder
    .addCase(getAccountInformation.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: true
      };
    })
    .addCase(getAccountInformation.fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        loading: false,
        error: false,
        data
      };
    })
    .addCase(getAccountInformation.rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        loading: false,
        error: true
      };
    });
};
