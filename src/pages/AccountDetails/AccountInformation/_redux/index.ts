import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  getAccountInformation,
  getAccountInformationBuilder
} from './getAccountInformation';

// types
import { InitialState } from '../types';

const { actions, reducer } = createSlice({
  name: 'accountInformation',
  initialState: {} as InitialState,
  reducers: {
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    }
  },
  extraReducers: builder => {
    getAccountInformationBuilder(builder);
  }
});

const accountInformation = {
  ...actions,
  getAccountInformation
};

export { accountInformation, reducer };
