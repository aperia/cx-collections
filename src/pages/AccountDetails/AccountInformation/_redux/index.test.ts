import { accountInformation, reducer } from './index';

// utils
import { storeId } from 'app/test-utils';

const { removeStore } = accountInformation;

describe('Test reducer', () => {
  it('removeStore action', () => {
    const state = reducer({}, removeStore({ storeId }));

    expect(state).toEqual({});
  });
});
