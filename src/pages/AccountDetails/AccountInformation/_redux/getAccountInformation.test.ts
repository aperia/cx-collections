import { rootReducer } from 'storeConfig';
import { createStore } from 'redux';

// redux-store
import './index';
import { getAccountInformation } from './getAccountInformation';

// types
import { Args } from '../types';
import { AxiosResponse } from 'axios';

// utils
import { storeId } from 'app/test-utils';

// service
import accountService from '../accountInformationService';
import { isRejected } from '@reduxjs/toolkit';

const requestData: Args = {
  storeId,
  postData: { accountNumber: storeId },
  translateFn: (value: string) => value
};

const responseDefault: AxiosResponse<any> = {
  data: null,
  status: 200,
  statusText: '',
  headers: {},
  config: {}
};

const mockResponseData = {
  customerInquiry: [
    {
      activityCycleToDateAmount: '0000000000000000.00',
      additionalCrossReferenceNumber1Identifier: '                ',
      additionalCrossReferenceNumber2Identifier:
        '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
      authorizationCode: 'S',
      creditBureauCode1: '3',
      creditLifeCode: '0',
      creditLifeStatusCode: 'N',
      creditLineChangeCode: 'M',
      crossReferenceIdentifier:
        '{"maskedValue":"022002******5300","eValue":"VOL(XS1RKlxxKjI9VE9nZUI0KA==)"}',
      currentCreditLineChangeDate: '04-26-2018',
      customerExternalIdentifier: 'C03098090011800710071012',
      customerRoleTypeCode: '01',
      cycleToDateReturnedCheckCount: '000',
      expirationDate: '04-2007',
      lastNonMonetaryDate: '07-01-2021',
      lastNonMonetaryTypeCode: ' 795',
      memberSequenceIdentifier: '00001',
      monthsGrossActiveCount: '000',
      mostRecentDisputeDate: '00000',
      numberReturnChecksCount: '0',
      paymentHistoryCode: '177777777777',
      plasticReplacementIndicator: 'N',
      presentationInstrumentExpirationDate: '04-30-2007',
      presentationInstrumentIdentifier:
        '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
      presentationInstrumentReplacementSequenceNumber: '0000',
      presentationInstrumentTypeCode: '01',
      savingsAccountIdentifier: '12456            ',
      socialSecurityIdentifier:
        '{"maskedValue":"***-**-1111","eValue":"VOL(UTVqRzYhcWlVc0laZTpe)"}',
      timesInDisputeCount: '000',
      totalDisputeAmount: '0000000000000000.00',
      totalNumberOfPlasticsIssuedCount: '004'
    },
    {
      activityCycleToDateAmount: '0000000000000000.00',
      additionalCrossReferenceNumber1Identifier: '                ',
      additionalCrossReferenceNumber2Identifier:
        '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
      authorizationCode: ' ',
      creditBureauCode1: ' ',
      creditLifeCode: '0',
      creditLifeStatusCode: 'N',
      creditLineChangeCode: 'M',
      crossReferenceIdentifier:
        '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
      currentCreditLineChangeDate: '04-26-2018',
      customerExternalIdentifier: 'C03143165404434086408614',
      customerRoleTypeCode: '02',
      cycleToDateReturnedCheckCount: '000',
      expirationDate: '04-2007',
      lastNonMonetaryDate: '07-01-2021',
      lastNonMonetaryTypeCode: ' 795',
      memberSequenceIdentifier: '00002',
      monthsGrossActiveCount: '000',
      mostRecentDisputeDate: '00000',
      numberReturnChecksCount: '0',
      paymentHistoryCode: '177777777777',
      plasticReplacementIndicator: 'N',
      presentationInstrumentExpirationDate: '01-01-0001',
      presentationInstrumentIdentifier:
        '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
      presentationInstrumentReplacementSequenceNumber: '0000',
      presentationInstrumentTypeCode: '01',
      savingsAccountIdentifier: '12456            ',
      socialSecurityIdentifier: '         ',
      timesInDisputeCount: '000',
      totalDisputeAmount: '0000000000000000.00',
      totalNumberOfPlasticsIssuedCount: '004'
    },
    {
      activityCycleToDateAmount: '0000000000000000.00',
      additionalCrossReferenceNumber1Identifier: '                ',
      additionalCrossReferenceNumber2Identifier:
        '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
      authorizationCode: ' ',
      creditBureauCode1: ' ',
      creditLifeCode: '0',
      creditLifeStatusCode: 'N',
      creditLineChangeCode: 'M',
      crossReferenceIdentifier:
        '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
      currentCreditLineChangeDate: '04-26-2018',
      customerExternalIdentifier: 'C03098090011800710071020',
      customerRoleTypeCode: '03',
      cycleToDateReturnedCheckCount: '000',
      expirationDate: '04-2007',
      lastNonMonetaryDate: '07-01-2021',
      lastNonMonetaryTypeCode: ' 795',
      memberSequenceIdentifier: '00005',
      monthsGrossActiveCount: '000',
      mostRecentDisputeDate: '00000',
      numberReturnChecksCount: '0',
      paymentHistoryCode: '177777777777',
      plasticReplacementIndicator: 'N',
      presentationInstrumentExpirationDate: '04-30-2007',
      presentationInstrumentIdentifier:
        '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
      presentationInstrumentReplacementSequenceNumber: '0000',
      presentationInstrumentTypeCode: '01',
      savingsAccountIdentifier: '12456            ',
      socialSecurityIdentifier: '         ',
      timesInDisputeCount: '000',
      totalDisputeAmount: '0000000000000000.00',
      totalNumberOfPlasticsIssuedCount: '004'
    },
    {
      activityCycleToDateAmount: '0000000000000000.00',
      additionalCrossReferenceNumber1Identifier: '                ',
      additionalCrossReferenceNumber2Identifier:
        '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
      authorizationCode: ' ',
      creditBureauCode1: ' ',
      creditLifeCode: '0',
      creditLifeStatusCode: 'N',
      creditLineChangeCode: 'M',
      crossReferenceIdentifier:
        '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
      currentCreditLineChangeDate: '04-26-2018',
      customerExternalIdentifier: 'C03143165326034772477215',
      customerRoleTypeCode: '03',
      cycleToDateReturnedCheckCount: '000',
      expirationDate: '04-2007',
      lastNonMonetaryDate: '07-01-2021',
      lastNonMonetaryTypeCode: ' 795',
      memberSequenceIdentifier: '00003',
      monthsGrossActiveCount: '000',
      mostRecentDisputeDate: '00000',
      numberReturnChecksCount: '0',
      paymentHistoryCode: '177777777777',
      plasticReplacementIndicator: 'N',
      presentationInstrumentExpirationDate: '01-01-0001',
      presentationInstrumentIdentifier:
        '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
      presentationInstrumentReplacementSequenceNumber: '0000',
      presentationInstrumentTypeCode: '01',
      savingsAccountIdentifier: '12456            ',
      socialSecurityIdentifier: '         ',
      timesInDisputeCount: '000',
      totalDisputeAmount: '0000000000000000.00',
      totalNumberOfPlasticsIssuedCount: '004'
    }
  ]
};

const expectData = {
  accounts: {
    checkingAccount: '',
    savingsAccountIdentifier: ''
  },
  activityHistory: {
    paymentHistoryCode: [
      { cycle: 1, history: 'txt_no_postings_during_the_cycle' },
      { cycle: 2, history: 'txt_no_postings_during_the_cycle' },
      { cycle: 3, history: 'txt_no_postings_during_the_cycle' },
      { cycle: 4, history: 'txt_no_postings_during_the_cycle' },
      { cycle: 5, history: 'txt_no_postings_during_the_cycle' },
      { cycle: 6, history: 'txt_no_postings_during_the_cycle' },
      { cycle: 7, history: 'txt_no_postings_during_the_cycle' },
      { cycle: 8, history: 'txt_no_postings_during_the_cycle' },
      { cycle: 9, history: 'txt_no_postings_during_the_cycle' },
      { cycle: 10, history: 'txt_no_postings_during_the_cycle' },
      { cycle: 11, history: 'txt_no_postings_during_the_cycle' },
      { cycle: 12, history: 'txt_no_postings_during_the_cycle' }
    ]
  },
  cardInformation: {
    presentationInstrumentExpirationDate: '04-30-2007',
    totalNumberOfPlasticsIssuedCount: '004'
  },
  creditLife: {
    creditLifeCode: '0',
    creditLifeStatusCode: 'N'
  },
  creditLineChange: {
    creditLineChangeCode: 'M',
    creditLineChangeDate: '04-26-2018'
  },
  crossReference: {
    account1Info: {
      accEValue: 'VOL(XS1RKlxxKjI9VE9nZUI0KA==)',
      accInfo:
        '{"maskedValue":"022002******5300","eValue":"VOL(XS1RKlxxKjI9VE9nZUI0KA==)"}',
      accMaskedValue: '022002******5300',
      memberSequenceIdentifier: '00001',
      socialSecurityIdentifier:
        '{"maskedValue":"***-**-1111","eValue":"VOL(UTVqRzYhcWlVc0laZTpe)"}'
    },
    account2Info: {
      accInfo: '',
      accMaskedValue: '',
      accEValue: '',
      memberSequenceIdentifier: '00001',
      socialSecurityIdentifier:
        '{"maskedValue":"***-**-1111","eValue":"VOL(UTVqRzYhcWlVc0laZTpe)"}'
    },
    account3Info: {
      accEValue: 'VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)',
      accInfo:
        '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
      accMaskedValue: '022009******2624',
      memberSequenceIdentifier: '00001',
      socialSecurityIdentifier:
        '{"maskedValue":"***-**-1111","eValue":"VOL(UTVqRzYhcWlVc0laZTpe)"}'
    }
  },
  cycleToDate: {
    cycleToDateTotalCreditAmount: '0000000000000000.00',
    cycleToDateReturnedCheckCount: '000'
  },
  disputed: {
    totalDisputeAmount: '0000000000000000.00',
    timesInDisputeCount: '000',
    mostRecentDisputeDate: ''
  },
  flags: { authorizationCode: 'S', creditBureauCode1: '3' },
  history: { monthsGrossActiveCount: '000' },
  lastNonMonetary: {
    lastNonMonetaryTransactionDate: '07-01-2021',
    lastNonMonetaryTypeCode: '795'
  },
  other: { numberReturnChecksCount: '0' }
};

describe('redux-store > getAccountInformation', () => {
  const store = createStore(rootReducer, {
    mapping: {
      data: {
        accountInformation: {
          'creditLineChange.creditLineChangeDate':
            'currentCreditLineChangeDate',
          'creditLineChange.creditLineChangeCode': 'creditLineChangeCode',
          'lastNonMonetary.lastNonMonetaryTransactionDate':
            'lastNonMonetaryDate',
          'lastNonMonetary.lastNonMonetaryTypeCode': 'lastNonMonetaryTypeCode',
          'crossReference.account1Info.accInfo': 'crossReferenceIdentifier',
          'crossReference.account1Info.accMaskedValue':
            'crossReferenceIdentifier',
          'crossReference.account1Info.accEValue': 'crossReferenceIdentifier',
          'crossReference.account1Info.memberSequenceIdentifier':
            'memberSequenceIdentifier',
          'crossReference.account1Info.socialSecurityIdentifier':
            'socialSecurityIdentifier',
          'crossReference.account2Info.accInfo':
            'additionalCrossReferenceNumber1Identifier',
          'crossReference.account2Info.accMaskedValue':
            'additionalCrossReferenceNumber1Identifier',
          'crossReference.account2Info.accEValue':
            'additionalCrossReferenceNumber1Identifier',
          'crossReference.account2Info.memberSequenceIdentifier':
            'memberSequenceIdentifier',
          'crossReference.account2Info.socialSecurityIdentifier':
            'socialSecurityIdentifier',
          'crossReference.account3Info.accInfo':
            'additionalCrossReferenceNumber2Identifier',
          'crossReference.account3Info.accMaskedValue':
            'additionalCrossReferenceNumber2Identifier',
          'crossReference.account3Info.accEValue':
            'additionalCrossReferenceNumber2Identifier',
          'crossReference.account3Info.memberSequenceIdentifier':
            'memberSequenceIdentifier',
          'crossReference.account3Info.socialSecurityIdentifier':
            'socialSecurityIdentifier',
          'creditLife.creditLifeCode': 'creditLifeCode',
          'creditLife.creditLifeStatusCode': 'creditLifeStatusCode',
          'flags.authorizationCode': 'authorizationCode',
          'flags.creditBureauCode1': 'creditBureauCode1',
          'other.numberReturnChecksCount': 'numberReturnChecksCount',
          'disputed.totalDisputeAmount': 'totalDisputeAmount',
          'disputed.timesInDisputeCount': 'timesInDisputeCount',
          'disputed.mostRecentDisputeDate': 'mostRecentDisputeDate',
          'cycleToDate.cycleToDateTotalCreditAmount':
            'activityCycleToDateAmount',
          'cycleToDate.cycleToDateReturnedCheckCount':
            'cycleToDateReturnedCheckCount',
          'accounts.checkingAccount': 'demandDepositAccountIdentifier',
          'accounts.savingsAccountIdentifier': 'savingsAccountIdentifier',
          'cardInformation.totalNumberOfPlasticsIssuedCount':
            'totalNumberOfPlasticsIssuedCount',
          'cardInformation.presentationInstrumentExpirationDate':
            'presentationInstrumentExpirationDate',
          'history.monthsGrossActiveCount': 'monthsGrossActiveCount',
          'activityHistory.paymentHistoryCode': 'paymentHistoryCode'
        }
      }
    }
  });

  describe('success', () => {
    it('async thunk empty data', async () => {
      jest
        .spyOn(accountService, 'getAccountInformation')
        .mockResolvedValue({ ...responseDefault, data: [] });
      const response = await getAccountInformation(requestData)(
        store.dispatch,
        store.getState,
        {}
      );
      expect(response.payload).toEqual({ data: {} });
    });

    it('fulfilled data', async () => {
      jest.spyOn(accountService, 'getAccountInformation').mockResolvedValue({
        ...responseDefault,
        data: mockResponseData
      });

      const response = await getAccountInformation(requestData)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(response.type).toEqual('account/getAccountInformation/fulfilled');
      expect(response.payload).toEqual({
        data: expectData
      });
    });

    it('fulfilled data > mapping data null', async () => {
      jest.spyOn(accountService, 'getAccountInformation').mockResolvedValue({
        ...responseDefault,
        data: mockResponseData
      });

      const storeMappingDataNull = createStore(rootReducer, {
        mapping: {
          data: {}
        }
      });

      const response = await getAccountInformation(requestData)(
        storeMappingDataNull.dispatch,
        storeMappingDataNull.getState,
        {}
      );
      expect(isRejected(response)).toBeTruthy;
    });

    it('fulfilled data > customers null', async () => {
      jest.spyOn(accountService, 'getAccountInformation').mockResolvedValue({
        ...responseDefault,
        data: { customerInquiry: undefined }
      });

      const response = await getAccountInformation(requestData)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(isRejected(response)).toBeTruthy;
    });
  });

  describe('error', () => {
    it('455 error', async () => {
      jest
        .spyOn(accountService, 'getAccountInformation')
        .mockRejectedValue({ ...responseDefault, status: 455 });

      const response = await getAccountInformation(requestData)(
        store.dispatch,
        store.getState,
        {}
      );

      expect(response.payload).toEqual({
        ...responseDefault,
        status: 455
      });
    });

    it('reject', async () => {
      jest.spyOn(accountService, 'getAccountInformation').mockRejectedValue({});
      const response = await getAccountInformation(requestData)(
        store.dispatch,
        store.getState,
        {}
      );
      expect(response.payload).toEqual({});
    });
  });
});
