import { storeId } from 'app/test-utils';
import { rootReducer } from 'storeConfig';

import * as selector from './selector';
import { createStore } from 'redux';

const store = createStore(rootReducer);
const state = store.getState();

state.accountInformation = {
  [storeId]: {
    collection: [],
    data: {},
    loading: false,
    error: false
  }
};

describe('redux-store > account-information > selector', () => {
  it('selectData', () => {
    const result = selector.selectData(storeId)(state);
    expect(result).toEqual(state.accountInformation[storeId].data);
  });

  it('selectIsError', () => {
    const result = selector.selectIsError(storeId)(state);
    expect(result).toEqual(state.accountInformation[storeId].error);
  });

  it('selectLoading', () => {
    const result = selector.selectLoading(storeId)(state);
    expect(result).toEqual(state.accountInformation[storeId].loading);
  });

  it('selectMapping', () => {
    const result = selector.selectMapping()(state);
    expect(result).toEqual(state.mapping.data);
  })
});
