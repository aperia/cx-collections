import { CUSTOMER_ROLE_CODE } from 'app/constants';
import { ACTIVITY_HISTORY } from './constants';
import { buildAccountActivityHistory, getData } from './helpers';

const mockTranslate = (text: string) => text;

describe('Helper Account Information', () => {
  it('getData with array item', () => {
    const input = {
      customerInquiry: [
        { id: 1, customerRoleTypeCode: CUSTOMER_ROLE_CODE.secondary },
        { id: 2, customerRoleTypeCode: CUSTOMER_ROLE_CODE.primary }
      ]
    };
    const result = getData(input);
    expect(result).toEqual(input.customerInquiry[1]);
  });

  it('getData with item', () => {
    const input = {
      customerInquiry: {
        id: 1,
        customerRoleTypeCode: CUSTOMER_ROLE_CODE.primary
      }
    };
    const result = getData(input);
    expect(result).toEqual(input.customerInquiry);
  });

  it('buildAccountActivityHistory with no data', () => {
    const result = buildAccountActivityHistory('', mockTranslate);
    expect(result).toEqual([]);
  });

  it('buildAccountActivityHistory with data', () => {
    const input = '01234567ZABCDEFGHIJKLMNOPQRSTUVWX%#+-';
    const listInput = input.trim().split('');

    const output = listInput.map((item, index) => ({
      cycle: index + 1,
      history: ACTIVITY_HISTORY[item]
    }));

    const result = buildAccountActivityHistory(input, mockTranslate);
    expect(result).toEqual(output);
  });
});
