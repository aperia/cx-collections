import React from 'react';

export const FIELDS = [
  {
    groupKey: 'creditLineChange',
    descriptor: 'accountInformationCreditLineChange',
    header: 'txt_credit_line_change'
  },
  {
    groupKey: 'lastNonMonetary',
    descriptor: 'accountInformationLastNonMonetary',
    header: 'txt_last_non_monetary'
  },
  {
    groupKey: 'crossReference',
    descriptor: 'accountInformationCrossReference',
    header: 'txt_cross_reference'
  },
  {
    groupKey: 'creditLife',
    descriptor: 'accountInformationCreditLife',
    header: 'txt_credit_life'
  },
  {
    groupKey: 'flags',
    descriptor: 'accountInformationFlags',
    header: 'txt_flags'
  },
  {
    groupKey: 'other',
    descriptor: 'accountInformationOther',
    header: 'txt_other'
  },
  {
    groupKey: 'disputed',
    descriptor: 'accountInformationDisputed',
    header: 'txt_disputed'
  },
  {
    groupKey: 'cycleToDate',
    descriptor: 'accountInformationCycleToDate',
    header: 'txt_cycle_to_date'
  },
  {
    groupKey: 'accounts',
    descriptor: 'accountInformationAccounts',
    header: 'txt_accounts'
  },
  {
    groupKey: 'cardInformation',
    descriptor: 'accountInformationCardInformation',
    header: 'txt_card_information'
  },
  {
    groupKey: 'history',
    descriptor: 'accountInformationHistory',
    header: 'txt_history'
  },
  {
    groupKey: 'activityHistory',
    descriptor: 'accountInformationActivityHistory',
    header: 'txt_activity_history',
    footer: <></>
  }
];
export const ACCOUNT_OVERVIEW_INFORMATION = [
  'cardholderInfo.savingsAccountNumber',
  'customerInquiry.currentCreditLineChangeDate',
  'customerInquiry.creditLineChangeCode',
  'customerInquiry.lastNonMonetaryDate',
  'customerInquiry.lastNonMonetaryTypeCode',
  'customerInquiry.crossReferenceIdentifier',
  'customerInquiry.additionalCrossReferenceNumber1Identifier',
  'customerInquiry.additionalCrossReferenceNumber2Identifier',
  'customerInquiry.creditLifeCode',
  'customerInquiry.creditLifeStatusCode',
  'customerInquiry.authorizationCode',
  'customerInquiry.creditBureauCode1',
  'customerInquiry.numberReturnChecksCount',
  'customerInquiry.totalDisputeAmount',
  'customerInquiry.timesInDisputeCount',
  'customerInquiry.mostRecentDisputeDate',
  'customerInquiry.activityCycleToDateAmount',
  'customerInquiry.cycleToDateReturnedCheckCount',
  'customerInquiry.checkingAccount',
  'customerInquiry.demandDepositAccountIdentifier',
  'customerInquiry.totalNumberOfPlasticsIssuedCount',
  'customerInquiry.presentationInstrumentExpirationDate',
  'customerInquiry.expirationDate',
  'customerInquiry.monthsGrossActiveCount',
  'customerInquiry.socialSecurityIdentifier',
  'customerInquiry.paymentHistoryCode'
];

export const ACTIVITY_HISTORY: Record<string, string> = {
  0: 'txt_no_postings_during_the_cycle',
  1: 'txt_no_postings_during_the_cycle',
  2: 'txt_no_postings_during_the_cycle',
  3: 'txt_no_postings_during_the_cycle',
  4: 'txt_no_postings_during_the_cycle',
  5: 'txt_no_postings_during_the_cycle',
  6: 'txt_no_postings_during_the_cycle',
  7: 'txt_no_postings_during_the_cycle',
  Z: 'txt_statement_not_generated',
  A: 'txt_account_not_delinquent',
  B: 'txt_account_1_cycle_delinquent',
  C: 'txt_account_2_cycles_delinquent',
  D: 'txt_account_3_cycles_delinquent',
  E: 'txt_account_4_cycles_delinquent',
  F: 'txt_account_5_cycles_delinquent',
  G: 'txt_account_6_cycles_delinquent',
  H: 'txt_account_7_cycles_delinquent',
  I: 'txt_account_not_delinquent',
  J: 'txt_account_1_cycle_delinquent',
  K: 'txt_account_2_cycles_delinquent',
  L: 'txt_account_3_cycles_delinquent',
  M: 'txt_account_4_cycles_delinquent',
  N: 'txt_account_5_cycles_delinquent',
  O: 'txt_account_6_cycles_delinquent',
  P: 'txt_account_7_cycles_delinquent',
  Q: 'txt_account_not_delinquent',
  R: 'txt_account_1_cycle_delinquent',
  S: 'txt_account_2_cycles_delinquent',
  T: 'txt_account_3_cycles_delinquent',
  U: 'txt_account_4_cycles_delinquent',
  V: 'txt_account_5_cycles_delinquent',
  W: 'txt_account_6_cycles_delinquent',
  X: 'txt_account_7_cycles_delinquent',
  '%': 'txt_credit_balance_with_no_activity',
  '#': 'txt_credit_balance_with_debit_credit_activity',
  '+': 'txt_credit_balance_with_debit_activity',
  '-': 'txt_credit_balance_with_credit_activity'
};
