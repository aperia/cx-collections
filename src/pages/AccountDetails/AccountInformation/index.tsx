import React, { useCallback, useEffect, useMemo, useRef } from 'react';

// components
import { SimpleBar } from 'app/_libraries/_dls/components';
import { FailedApiReload, GroupView } from 'app/components';
import TogglePin from 'pages/__commons/InfoBar/TogglePin';

// hooks - helpers
import { useAccountDetail } from 'app/hooks';
import {
  formatDisplayDate,
  getGroupMappingData,
  prepareDateStringFormat
} from 'app/helpers';

// redux store
import { batch, useDispatch, useSelector } from 'react-redux';
import { accountInformation } from './_redux';
import {
  selectMapping,
  selectData,
  selectLoading,
  selectIsError
} from './_redux/selector';
import { actionsInfoBar, InfoBarSection } from 'pages/__commons/InfoBar/_redux';
import { selectPinned } from 'pages/__commons/InfoBar/_redux/selectors';

// helpers
import classNames from 'classnames';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { FIELDS } from './constants';

// types
import { GetAccountInformation } from './types';
import { useStoreIdSelector } from 'app/hooks/useStoreIdSelector';
import { EMPTY_OBJECT } from 'app/constants';
import isEmpty from 'lodash.isempty';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface AccountInformationProps {
  primaryName: string;
}

const AccountInformation: React.FC<AccountInformationProps> = ({
  primaryName
}) => {
  const ref = useRef<HTMLDivElement | null>(null);
  const { t } = useTranslation();
  const testId = 'accountDetail_accountInformation';

  const dispatch = useDispatch();
  const { storeId, accEValue } = useAccountDetail();
  const loading = useSelector<RootState, boolean>(selectLoading(storeId));
  const isError = useSelector<RootState, boolean>(selectIsError(storeId));
  const data = useSelector<RootState, GetAccountInformation>(
    selectData(storeId)
  );
  const mapping = useSelector<RootState, MagicKeyValue>(selectMapping());
  const pinned = useStoreIdSelector(selectPinned);
  const isPinned = pinned === InfoBarSection.AccountInformation;

  const groupKeys = useMemo(() => {
    if (!mapping || !mapping.accountCollection) return {};

    return getGroupMappingData(mapping?.accountInformation);
  }, [mapping]);

  const handleReload = useCallback(() => {
    dispatch(
      accountInformation.getAccountInformation({
        storeId,
        postData: { accountNumber: accEValue },
        translateFn: t
      })
    );
  }, [dispatch, storeId, accEValue, t]);

  const renderContent = () => {
    if (isError) {
      return (
        <FailedApiReload
          id="overview-account-information-fail"
          className="w-100 mt-24"
          onReload={handleReload}
          dataTestId={`${testId}_failed-api`}
        />
      );
    }

    if (isEmpty(data)) return null;

    const {
      account1Info = EMPTY_OBJECT,
      account2Info = EMPTY_OBJECT,
      account3Info = EMPTY_OBJECT
    } = data?.crossReference || {};

    const { presentationInstrumentExpirationDate = '' } =
      data?.cardInformation || {};

    const dataView = {
      ...data,
      crossReference: {
        account1Info: {
          primaryName,
          ...account1Info
        },
        account2Info: {
          primaryName,
          ...account2Info
        },
        account3Info: {
          primaryName,
          ...account3Info
        }
      },
      cardInformation: {
        ...data.cardInformation,
        presentationInstrumentExpirationDate: formatDisplayDate(
          prepareDateStringFormat(presentationInstrumentExpirationDate)
        )
      },
      activityHistory: {
        ...data.activityHistory,
        paymentHistoryCode: data.activityHistory?.paymentHistoryCode?.map(
          item => ({
            ...item,
            history: t(item.history)
          })
        )
      }
    } as MagicKeyValue;

    const onlyFields = {
      crossReference: [
        'account1Info.accMaskedValue',
        'account2Info.accMaskedValue',
        'account3Info.accMaskedValue'
      ]
    } as MagicKeyValue;

    return FIELDS.map(({ descriptor, groupKey, ...rest }) => (
      <GroupView
        key={descriptor}
        descriptor={descriptor}
        data={dataView[groupKey]}
        fields={groupKeys[groupKey]}
        onlyFields={onlyFields[groupKey]}
        {...rest}
      />
    ));
  };

  const handleTogglePin = () => {
    batch(() => {
      const thisPinned = isPinned ? null : InfoBarSection.AccountInformation;

      dispatch(
        actionsInfoBar.updatePinned({
          storeId,
          pinned: thisPinned
        })
      );

      if (thisPinned) {
        dispatch(actionsInfoBar.updateRecover({ storeId, recover: null }));
      }

      dispatch(actionsInfoBar.updateIsExpand({ storeId, isExpand: !isPinned }));
    });
  };

  useEffect(() => handleReload(), [handleReload]);

  return (
    <div className={classNames('h-100', { loading })}>
      <SimpleBar>
        <div ref={ref} className="p-24 overlap-next-action">
          <div className="d-inline-flex">
            <h4
              className="pb-16 mr-8"
              data-testid={genAmtId(testId, 'title', '')}
            >
              {t('txt_account_information')}
            </h4>
            <TogglePin
              isPinned={isPinned}
              onToggle={handleTogglePin}
              dataTestId={testId}
            />
          </div>
          {renderContent()}
        </div>
      </SimpleBar>
    </div>
  );
};

export default AccountInformation;
