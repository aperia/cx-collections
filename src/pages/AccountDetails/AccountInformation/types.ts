export interface AccountDetailOverview {
  loading?: boolean;
  error?: boolean;
  collection: MagicKeyValue;
  data: MagicKeyValue;
}

export interface InitialState {
  [storeId: string]: AccountDetailOverview;
}

export interface GetAccountInformationPayload {
  data: GetAccountInformation;
}

export interface ActivityHistory {
  cycle: number;
  history: string;
}

export interface GetAccountInformation {
  accounts?: {
    checkingAccount?: string;
    savingsAccountIdentifier?: string;
  };
  activityHistory?: {
    paymentHistoryCode?: Array<ActivityHistory>;
  };
  cardInformation?: {
    presentationInstrumentExpirationDate?: string;
    totalNumberOfPlasticsIssuedCount?: string;
  };
  creditLife?: {
    creditLifeStatusCode?: string;
    creditLifeCode?: string;
  };
  creditLineChange?: {
    creditLineChangeCode?: string;
    creditLineChangeDate?: string;
  };
  crossReference?: {
    account1Info?: {
      accEValue?: string;
      accInfo?: string;
      accMaskedValue?: string;
      memberSequenceIdentifier?: string;
      socialSecurityIdentifier?: string;
    };
    account2Info?: {
      accEValue?: string;
      accInfo?: string;
      accMaskedValue?: string;
      memberSequenceIdentifier?: string;
      socialSecurityIdentifier?: string;
    };
    account3Info?: {
      accEValue?: string;
      accInfo?: string;
      accMaskedValue?: string;
      memberSequenceIdentifier?: string;
      socialSecurityIdentifier?: string;
    };
  };
  cycleToDate?: {
    cycleToDateReturnedCheckCount?: string;
    cycleToDateTotalCreditAmount?: string;
  };
  disputed?: {
    totalDisputeAmount?: string;
    timesInDisputeCount?: string;
    mostRecentDisputeDate?: string;
  };
  flags?: {
    authorizationCode?: string;
    creditBureauCode1?: string;
  };
  history?: {
    monthsGrossActiveCount?: string;
  };
  lastNonMonetary?: {
    lastNonMonetaryTransactionDate?: string;
    lastNonMonetaryTypeCode?: string;
  };
  other?: {
    numberReturnChecksCount?: string;
  };
}

export interface Args {
  storeId: string;
  postData: { accountNumber: string };
  translateFn: Function;
}
