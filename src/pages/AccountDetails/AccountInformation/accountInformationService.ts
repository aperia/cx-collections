import { apiService } from 'app/utils/api.service';
import { ACCOUNT_OVERVIEW_INFORMATION } from './constants';

// constants

const accountInformationService = {
  getAccountInformation: (
    accountNumber: string,
    selectFields = ACCOUNT_OVERVIEW_INFORMATION
  ) => {
    const url = window.appConfig?.api?.account?.getOverviewAccount || '';
    const requestBody = {
      common: { accountId: accountNumber },
      selectFields
    };
    return apiService.post(url, requestBody);
  }
};
export default accountInformationService;
