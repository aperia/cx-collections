import React from 'react';

// Component
import AccountInformation from './index';

// Helper
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';

// redux
import { actionsInfoBar, InfoBarSection } from 'pages/__commons/InfoBar/_redux';

// types
import { MappingStateData } from 'pages/__commons/MappingProvider/types';
import { TogglePinProps } from 'pages/__commons/InfoBar/TogglePin';

const renderWrapper = (
  initialState: Partial<RootState>,
  mapping?: Partial<MappingStateData>
) => {
  return renderMockStoreId(<AccountInformation primaryName="name" />, {
    initialState,
    mapping
  });
};

jest.mock('app/_libraries/_dls/hooks', () => ({
  ...jest.requireActual('app/_libraries/_dls/hooks'),
  useTranslation: () => ({ t: (text: string) => text })
}));

jest.mock(
  'pages/__commons/InfoBar/TogglePin',
  () => (props: TogglePinProps) => {
    return <div onClick={props.onToggle}>TogglePin</div>;
  }
);

const actionsInfoBarSpy = mockActionCreator(actionsInfoBar);

describe('Render', () => {
  it('Render UI', () => {
    const initialState: object = {
      accountInformation: { [storeId]: { data: {} } }
    };
    const { wrapper } = renderWrapper(initialState);
    expect(wrapper.getByText('txt_account_information')).toBeInTheDocument();
  });
  it('Render UI without data', () => {
    const initialState: object = {
      accountInformation: { [storeId]: {} }
    };
    const { wrapper } = renderWrapper(initialState, {});
    expect(wrapper.getByText('txt_account_information')).toBeInTheDocument();
  });
  it('Render UI with error', () => {
    const initialState: object = {
      accountInformation: { [storeId]: { error: true } }
    };
    const { wrapper } = renderWrapper(initialState);
    expect(
      wrapper.getByText('txt_data_load_unsuccessful_click_reload_to_try_again')
    ).toBeInTheDocument();
    expect(wrapper.getByRole('button')).toBeInTheDocument();
  });
  it('Render UI without mapping', () => {
    const initialState: object = {
      accountInformation: { [storeId]: {} }
    };
    const { wrapper } = renderWrapper(initialState);
    expect(wrapper.getByText('txt_account_information')).toBeInTheDocument();
  });
  it('Render UI with crossReference', () => {
    const initialState: object = {
      accountInformation: {
        [storeId]: {
          data: { crossReference: {} }
        }
      }
    };
    const { wrapper } = renderWrapper(initialState);
    expect(wrapper.getByText('txt_account_information')).toBeInTheDocument();
  });
});

describe('Pin', () => {
  const initialState: object = {
    accountInformation: {
      [storeId]: {
        data: {
          accounts: {
            checkingAccount: undefined,
            savingsAccountIdentifier: '12456'
          },
          activityHistory: {
            paymentHistoryCode: [
              { cycle: 1, history: 'No postings during the cycle' },
              { cycle: 2, history: 'No postings during the cycle' },
              { cycle: 3, history: 'No postings during the cycle' },
              { cycle: 4, history: 'No postings during the cycle' },
              { cycle: 5, history: 'No postings during the cycle' },
              { cycle: 6, history: 'No postings during the cycle' },
              { cycle: 7, history: 'No postings during the cycle' },
              { cycle: 8, history: 'No postings during the cycle' },
              { cycle: 9, history: 'No postings during the cycle' },
              { cycle: 10, history: 'No postings during the cycle' },
              { cycle: 11, history: 'No postings during the cycle' },
              { cycle: 12, history: 'No postings during the cycle' }
            ]
          },
          cardInformation: {
            presentationInstrumentExpirationDate: '04-30-2007',
            totalNumberOfPlasticsIssuedCount: '004'
          },
          creditLife: {
            creditLifeCode: '0',
            creditLifeStatusCode: 'N'
          },
          creditLineChange: {
            creditLineChangeCode: 'M',
            creditLineChangeDate: '04-26-2018'
          },
          crossReference: {
            account1Info: {
              accEValue: 'VOL(XS1RKlxxKjI9VE9nZUI0KA==)',
              accInfo:
                '{"maskedValue":"022002******5300","eValue":"VOL(XS1RKlxxKjI9VE9nZUI0KA==)"}',
              accMaskedValue: '022002******5300',
              memberSequenceIdentifier: '00001',
              socialSecurityIdentifier:
                '{"maskedValue":"***-**-1111","eValue":"VOL(UTVqRzYhcWlVc0laZTpe)"}'
            },
            account2Info: {
              accInfo: '',
              accMaskedValue: '',
              accEValue: '',
              memberSequenceIdentifier: '00001',
              socialSecurityIdentifier:
                '{"maskedValue":"***-**-1111","eValue":"VOL(UTVqRzYhcWlVc0laZTpe)"}'
            },
            account3Info: {
              accEValue: 'VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)',
              accInfo:
                '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
              accMaskedValue: '022009******2624',
              memberSequenceIdentifier: '00001',
              socialSecurityIdentifier:
                '{"maskedValue":"***-**-1111","eValue":"VOL(UTVqRzYhcWlVc0laZTpe)"}'
            }
          },
          cycleToDate: {
            cycleToDateTotalCreditAmount: '0000000000000000.00',
            cycleToDateReturnedCheckCount: '000'
          },
          disputed: {
            totalDisputeAmount: '0000000000000000.00',
            timesInDisputeCount: '000',
            mostRecentDisputeDate: '00000'
          },
          flags: { authorizationCode: 'S', creditBureauCode1: '3' },
          history: { monthsGrossActiveCount: '000' },
          lastNonMonetary: {
            lastNonMonetaryTransactionDate: '07-01-2021',
            lastNonMonetaryTypeCode: '795'
          },
          other: { numberReturnChecksCount: '0' }
        }
      }
    },
    mapping: {
      data: undefined
    },
    infoBar: {
      [storeId]: {
        pinned: true
      }
    }
  };

  it('render with error data', () => {
    const initialStateNullData: object = {
      accountInformation: {
        [storeId]: {
          data: { crossReference: undefined }
        }
      },
      mapping: {
        data: undefined
      },
      infoBar: {
        [storeId]: {
          pinned: true
        }
      }
    };

    const { wrapper } = renderMockStoreId(
      <AccountInformation primaryName="name" />,
      {
        initialState: initialStateNullData
      }
    );

    expect(wrapper.queryByText('txt_account_information')).toBeInTheDocument();
  });

  it(' trigger action Pin with exist Account Information Pin', () => {
    const { wrapper } = renderMockStoreId(
      <AccountInformation primaryName="name" />,
      {
        initialState
      }
    );
    const spy = actionsInfoBarSpy('updateRecover');
    wrapper.queryByText('TogglePin')!.click();
    expect(spy).toHaveBeenCalledWith({ storeId, recover: null });
  });

  it('trigger action toggle Pin', () => {
    const { wrapper } = renderMockStoreId(
      <AccountInformation primaryName="name" />,
      {
        initialState: {
          ...initialState,
          infoBar: {
            [storeId]: {
              pinned: InfoBarSection.AccountInformation,
              isExpand: true,
              recover: InfoBarSection.AccountInformation
            }
          }
        }
      }
    );

    const spy = actionsInfoBarSpy('updatePinned');
    wrapper.queryByText('TogglePin')!.click();
    expect(spy).toHaveBeenCalledWith({ storeId, pinned: null });
  });
});
