import isArray from 'lodash.isarray';

// Const
import { CUSTOMER_ROLE_CODE } from 'app/constants';
import { ACTIVITY_HISTORY } from './constants';

// Type
import { ActivityHistory } from './types';

interface Item {
  customerRoleTypeCode: string;
}

interface ItemCardholder {
  savingsAccountNumber?: string;
}

interface AccountDetailResponse {
  customerInquiry?: Item | Item[];
  cardholderInfo?: ItemCardholder[];
}

export const getData = (data: AccountDetailResponse) => {
  const customerInquiry =
    isArray(data?.customerInquiry) && data?.customerInquiry.length > 0
      ? data?.customerInquiry.find(
          ({ customerRoleTypeCode }) =>
            customerRoleTypeCode === CUSTOMER_ROLE_CODE.primary
        )
      : data?.customerInquiry;
  return {
    ...customerInquiry,
    savingsAccountIdentifier: data?.cardholderInfo?.[0]?.savingsAccountNumber
  };
};

export const buildAccountActivityHistory = (
  paymentHistoryCode: string,
  translateFn: Function
) => {
  const listActivityHistory = paymentHistoryCode?.trim()?.split('') || [];

  if (!listActivityHistory.length) return listActivityHistory;

  return listActivityHistory.reduce(
    (nextData: Array<ActivityHistory>, item, index) => {
      nextData = [
        ...nextData,
        {
          ['cycle']: index + 1,
          ['history']: translateFn(ACTIVITY_HISTORY[item])
        }
      ];
      return nextData;
    },
    []
  );
};
