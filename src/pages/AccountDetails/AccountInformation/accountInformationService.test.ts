import accountInformationService from './accountInformationService';

// utils
import { storeId, mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import { ACCOUNT_OVERVIEW_INFORMATION } from './constants';

describe('accountInformationService', () => {
  describe('getAccountInformation', () => {
    const params = storeId;

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      accountInformationService.getAccountInformation(params);

      expect(mockService).toBeCalledWith('', {
        common: { accountId: params },
        selectFields: ACCOUNT_OVERVIEW_INFORMATION
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      accountInformationService.getAccountInformation(params);

      expect(mockService).toBeCalledWith(apiUrl.account.getOverviewAccount, {
        common: { accountId: params },
        selectFields: ACCOUNT_OVERVIEW_INFORMATION
      });
    });
  });
});
