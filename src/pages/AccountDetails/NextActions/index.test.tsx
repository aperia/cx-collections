import {
  accEValue,
  mockActionCreator,
  queryByClass,
  renderMockStoreId,
  storeId
} from 'app/test-utils';
import React from 'react';
import { act, fireEvent, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
// component
import NextActions from './index';
// actions
import * as removeStore from 'app/helpers/removeStore';
import { tabActions } from 'pages/__commons/TabBar/_redux';
// constants
import {
  NEXT_ACCOUNT,
  PREVIOUS_ACCOUNT,
  QUEUE_DETAILS,
  QUEUE_LIST,
  SEARCH_PAGE
} from './constants';
import { dashboardActions } from 'pages/AccountSearch/Home/Dashboard/_redux/reducers';
import { QueueTypeEnum } from 'pages/AccountSearch/Home/Dashboard/QueueDetail/types';
import { QueueListState } from 'pages/QueueList/types';
import { queueAccountsActions } from 'pages/AccountSearch/Home/Dashboard/QueueDetail/_redux';

jest.mock('react-redux', () => {
  return {
    ...jest.requireActual('react-redux'),
    useDispatch: () => (x: Function) => jest.fn().mockImplementation(() => x())
  };
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const queueAccountsActionsSpy = mockActionCreator(queueAccountsActions);
const dashboardActionsSpy = mockActionCreator(dashboardActions);
const tabActionsSpy = mockActionCreator(tabActions);
const removeStoreReduxSpy = jest
  .spyOn(removeStore, 'removeStoreRedux')
  .mockImplementation((args: MagicKeyValue[]) => () => ({ type: 'type' }));

describe('Test NextActions component', () => {
  it('renders component', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(<NextActions />);
    jest.runAllTimers();

    expect(
      queryByClass(wrapper.container, 'dls-floating-dropdown-button')
    ).toBeInTheDocument();

    expect(
      queryByClass(wrapper.container, 'text-field-container')?.innerHTML
    ).toEqual(
      `<div class="input"><span class="dls-ellipsis">${QUEUE_LIST}</span></div>`
    );

    const button = screen.getByRole('button');
    // button having arrow right icon
    expect(button?.innerHTML).toEqual('<i class="icon icon-arrow-right"></i>');
  });

  it('selects NEXT_ACCOUNT option with Push_Queue', () => {
    const triggerNextAccountWorkOnQueue = dashboardActionsSpy(
      'triggerNextAccountWorkOnQueue'
    );

    jest.useFakeTimers();
    renderMockStoreId(<NextActions />, {
      initialState: {
        callerAuthentication: {
          selectedAccountDetailData: {
            queueId: 'primaryName'
          }
        },
        nextActions: {
          [accEValue]: { queueId: '04', queueType: QueueTypeEnum.PUSH_QUEUE }
        } as never
      }
    });
    jest.runAllTimers();

    expect(screen.getByText(NEXT_ACCOUNT)).toBeInTheDocument();

    const button = screen.getByRole('button');
    fireEvent.click(button);

    expect(triggerNextAccountWorkOnQueue).toBeCalledWith({
      queueId: '04',
      inAccountDetail: true,
      storeId,
      accountNumber: undefined,
      inQueueDetail: true,
      queueType: QueueTypeEnum.PUSH_QUEUE
    });
  });

  it('selects NEXT_ACCOUNT option with Pull_Queue', () => {
    const triggerNextAccountWorkOnQueue = dashboardActionsSpy(
      'triggerNextAccountWorkOnQueue'
    );

    jest.useFakeTimers();
    renderMockStoreId(<NextActions />, {
      initialState: {
        callerAuthentication: {
          selectedAccountDetailData: {
            queueId: 'primaryName'
          }
        },
        nextActions: {
          [accEValue]: { queueId: '04', queueType: QueueTypeEnum.PULL_QUEUE }
        } as never
      }
    });
    jest.runAllTimers();

    expect(screen.getByText(NEXT_ACCOUNT)).toBeInTheDocument();

    const button = screen.getByRole('button');
    fireEvent.click(button);

    expect(triggerNextAccountWorkOnQueue).toBeCalledWith({
      queueId: '04',
      inAccountDetail: true,
      storeId,
      accountNumber: undefined,
      inQueueDetail: true,
      queueType: QueueTypeEnum.PULL_QUEUE
    });
  });

  it('selects PREVIOUS_ACCOUNT option', () => {
    const getQueueAccountDetail = queueAccountsActionsSpy(
      'getQueueAccountDetail'
    );

    const mockAccounts = [
      {
        id: '1',
        lastName: 'Smith',
        firstName: 'Jackie',
        currentBalance: '1000',
        accountNumber: '1234',
        lastWorkDate: '01-05-2022',
        daysDelinquent: '358',
        amountDelinquent: '1964.20',
        lastPaymentDate: '12-23-2021',
        timeZone: 'CST'
      },
      {
        id: '2',
        lastName: 'Harley',
        firstName: 'Murphy',
        currentBalance: '2000',
        accountNumber: '4567',
        lastWorkDate: '01-10-2022',
        daysDelinquent: '123',
        amountDelinquent: '1964.20',
        lastPaymentDate: '12-23-2021',
        timeZone: 'CST'
      }
    ];

    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(<NextActions />, {
      initialState: {
        callerAuthentication: {
          selectedAccountDetailData: {
            queueId: 'primaryName'
          }
        },
        accountDetail: {
          [storeId]: {
            data: {
              accNbr: '123456',
              primaryName: 'SMITH'
            },
            isLoading: false
          }
        },
        queueList: {
          data: {
            queueDetails: [
              {
                queueId: '04',
                queueName: 'queueName',
                assignedTo: 'Nate Nash',
                totalNumbersOfAccount: '4',
                totalOutstandingBalanceAmount: '100000'
              }
            ]
          },
          loading: false,
          error: '',
          sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
          orderBy: { description: 'Descending', value: 'desc' },
          searchBy: [],
          queueType: { pull: true, push: true }
        } as QueueListState,
        queueAccounts: {
          '04': {
            isLoading: false,
            isError: false,
            data: mockAccounts
          }
        },
        dashboard: {
          queueAccount: {
            previousAccount: mockAccounts[0],
            currentAccount: mockAccounts[1]
          }
        },
        nextActions: {
          [accEValue]: { queueId: '04', queueType: QueueTypeEnum.PULL_QUEUE }
        }
      }
    });
    jest.runAllTimers();

    const dropdownElement = queryByClass(
      wrapper.container,
      'text-field-container'
    );
    act(() => {
      userEvent.click(dropdownElement!);
      jest.runAllTimers();
    });

    userEvent.click(screen.getAllByText(PREVIOUS_ACCOUNT)[0]);
    jest.runAllTimers();

    expect(screen.getAllByText(PREVIOUS_ACCOUNT)[0]).toBeInTheDocument();

    const button = screen.getByRole('button');
    fireEvent.click(button);

    expect(getQueueAccountDetail).toBeCalledWith({
      accountNumber: mockAccounts[0].accountNumber,
      queueType: QueueTypeEnum.PULL_QUEUE,
      queueId: '04'
    });
  });

  it('selects QUEUE_DETAILS page option, open new tab', () => {
    const addTabSpy = tabActionsSpy('addTab');

    jest.useFakeTimers();
    const mockQueueDetailsData = {
      iconName: 'file',
      props: {
        data: {
          assignedTo: 'Nate Nash',
          createdDate: '11/23/2020',
          queueId: '04',
          queueName: 'queueName',
          remainingAccounts: '3 | $60,000.00',
          totalAccounts: '4 | $100,000.00',
          workedAccounts: '1 | $40,000.00'
        }
      },
      storeId: '04-queueName',
      tabType: 'queueDetail',
      title: '04 - queueName'
    };
    const { wrapper } = renderMockStoreId(<NextActions />, {
      initialState: {
        queueList: {
          data: {
            queueDetails: [
              {
                queueId: '04',
                queueName: 'queueName',
                assignedTo: 'Nate Nash',
                totalNumbersOfAccount: '4',
                totalOutstandingBalanceAmount: '100000'
              }
            ]
          },
          loading: false,
          error: '',
          sortBy: { description: 'Last Work Date', value: 'lastWorkDate' },
          orderBy: { description: 'Descending', value: 'desc' },
          searchBy: [],
          queueType: { pull: true, push: true }
        } as QueueListState,
        nextActions: {
          [accEValue]: { queueId: '04', queueType: QueueTypeEnum.PULL_QUEUE }
        }
      }
    });
    jest.runAllTimers();

    const dropdownElement = queryByClass(
      wrapper.container,
      'text-field-container'
    );
    act(() => {
      userEvent.click(dropdownElement!);
      jest.runAllTimers();
    });

    userEvent.click(screen.getAllByText(QUEUE_DETAILS)[0]);
    jest.runAllTimers();

    expect(screen.getAllByText(QUEUE_DETAILS)[0]).toBeInTheDocument();

    const button = screen.getByRole('button');
    fireEvent.click(button);

    expect(addTabSpy).toBeCalledWith(
      expect.objectContaining(mockQueueDetailsData)
    );

    expect(removeStoreReduxSpy).toBeCalled();
  });

  it('selects QUEUE_DETAILS page option, open exists tab', () => {
    const selectTabSpy = tabActionsSpy('selectTab');

    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(<NextActions />, {
      initialState: {
        nextActions: {
          [accEValue]: { queueId: '04', queueType: QueueTypeEnum.PULL_QUEUE }
        },
        tab: {
          accEValueSelected: 'accEValue',
          tabStoreIdSelected: 'anotherTabStoreId',
          tabs: [
            {
              title: QUEUE_DETAILS,
              storeId: 'queueDetail',
              accEValue: 'accEValue',
              iconName: 'file',
              tabType: 'queueDetail'
            }
          ]
        }
      }
    });
    jest.runAllTimers();

    const dropdownElement = queryByClass(
      wrapper.container,
      'text-field-container'
    );
    act(() => {
      userEvent.click(dropdownElement!);
      jest.runAllTimers();
    });

    userEvent.click(screen.getAllByText(QUEUE_DETAILS)[0]);
    jest.runAllTimers();

    expect(screen.getAllByText(QUEUE_DETAILS)[0]).toBeInTheDocument();

    const button = screen.getByRole('button');
    fireEvent.click(button);

    expect(selectTabSpy).toBeCalledWith(
      expect.objectContaining({
        storeId: 'queueDetail',
        accEValue: 'accEValue'
      })
    );

    expect(removeStoreReduxSpy).toBeCalled();
  });

  it('selects QUEUE_LIST page option, open new tab', () => {
    const addTabSpy = tabActionsSpy('addTab');

    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(<NextActions />);
    jest.runAllTimers();

    const dropdownElement = queryByClass(
      wrapper.container,
      'text-field-container'
    );
    act(() => {
      userEvent.click(dropdownElement!);
      jest.runAllTimers();
    });

    userEvent.click(screen.getAllByText(QUEUE_LIST)[0]);
    jest.runAllTimers();

    expect(screen.getAllByText(QUEUE_LIST)[0]).toBeInTheDocument();

    const button = screen.getByRole('button');
    fireEvent.click(button);

    expect(addTabSpy).toBeCalledWith(
      expect.objectContaining({
        title: QUEUE_LIST,
        accEValue: 'accEValue',
        iconName: 'file',
        tabType: 'queueList'
      })
    );

    expect(removeStoreReduxSpy).toBeCalled();
  });

  it('selects QUEUE_LIST page option, open exists tab', () => {
    const selectTabSpy = tabActionsSpy('selectTab');

    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(<NextActions />, {
      initialState: {
        tab: {
          accEValueSelected: 'accEValue',
          tabStoreIdSelected: 'anotherTabStoreId',
          tabs: [
            {
              title: 'Queue List',
              storeId: 'queueList',
              accEValue: 'accEValue',
              iconName: 'file',
              tabType: 'queueList'
            }
          ]
        }
      }
    });
    jest.runAllTimers();

    const dropdownElement = queryByClass(
      wrapper.container,
      'text-field-container'
    );
    act(() => {
      userEvent.click(dropdownElement!);
      jest.runAllTimers();
    });

    userEvent.click(screen.getAllByText(QUEUE_LIST)[0]);
    jest.runAllTimers();

    expect(screen.getAllByText(QUEUE_LIST)[0]).toBeInTheDocument();

    const button = screen.getByRole('button');
    fireEvent.click(button);

    expect(selectTabSpy).toBeCalledWith(
      expect.objectContaining({
        storeId: 'queueList',
        accEValue: 'accEValue'
      })
    );

    expect(removeStoreReduxSpy).toBeCalled();
  });

  it('selects search page option', () => {
    const addTabSpy = tabActionsSpy('selectTab');

    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(<NextActions />);
    jest.runAllTimers();

    const dropdownElement = queryByClass(
      wrapper.container,
      'text-field-container'
    );
    act(() => {
      userEvent.click(dropdownElement!);
      jest.runAllTimers();
    });

    userEvent.click(screen.getByText(SEARCH_PAGE));
    jest.runAllTimers();

    expect(screen.getByText(SEARCH_PAGE)).toBeInTheDocument();

    const button = screen.getByRole('button');
    fireEvent.click(button);

    expect(addTabSpy).toBeCalledWith(
      expect.objectContaining({
        storeId: 'home'
      })
    );

    expect(removeStoreReduxSpy).toBeCalled();
  });
});
