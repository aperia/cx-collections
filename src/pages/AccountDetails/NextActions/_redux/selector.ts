import { NextActionsData } from './../types';
import { createSelector } from '@reduxjs/toolkit';

export const selectQueueId = (eValue: string) => {
  return createSelector(
    (state: RootState) => {
      return state.nextActions[eValue] as unknown as
        | NextActionsData
        | undefined;
    },
    (data?: NextActionsData) => data
  );
};
