import { QueueTypeEnum } from 'pages/AccountSearch/Home/Dashboard/QueueDetail/types';
import { nextActions, reducer } from './reducers';
import { accEValue } from 'app/test-utils';
import { NextActionsState } from '../types';

const initialState: NextActionsState = {};

describe('Test reducer', () => {
  it('setQueueId', () => {
    const params = {
      eValue: accEValue,
      queueId: 'queueId',
      queueType: QueueTypeEnum.PULL_QUEUE
    };
    const state = reducer(initialState, nextActions.setQueueId(params));

    expect(state[accEValue]).toEqual(
      expect.objectContaining({
        queueId: 'queueId',
        queueType: QueueTypeEnum.PULL_QUEUE
      })
    );
  });
});
