import { getNextAccount, triggerGetNextAccount } from './getNextAccount';
import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault,
  storeId
} from 'app/test-utils';
import { nextActionsServices } from '../nextActionsServices';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { callerAuthenticationActions } from 'pages/CallerAuthenticate/_redux/reducers';
import { timerWorkingActions } from 'pages/TimerWorking/_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { WorkType } from 'pages/TimerWorking/types';

let spy: jest.SpyInstance;

const callerAuthenticationActionsSpy = mockActionCreator(
  callerAuthenticationActions
);
const timerWorkingActionsSpy = mockActionCreator(timerWorkingActions);
const actionsToastSpy = mockActionCreator(actionsToast);

describe('Test getNextAccount async actions', () => {
  const store = createStore(rootReducer, {});

  afterEach(() => {
    spy.mockReset();
    spy.mockRestore();
  });

  describe('Test getNextAccount async action', () => {
    it('should be fulfilled', async () => {
      spy = jest
        .spyOn(nextActionsServices, 'getNextAccount')
        .mockResolvedValue({ ...responseDefault });

      const response = await getNextAccount({ storeId })(
        store.dispatch,
        store.getState,
        {}
      );

      expect(response.type).toEqual('nextActions/getNextAccount/fulfilled');
      expect(response?.payload).toEqual(
        expect.objectContaining({ status: 200 })
      );
    });

    it('should be rejected', async () => {
      spy = jest
        .spyOn(nextActionsServices, 'getNextAccount')
        .mockRejectedValue({ ...responseDefault, status: 500 });

      const response = await getNextAccount({ storeId })(
        store.dispatch,
        store.getState,
        {}
      );

      expect(response.type).toEqual('nextActions/getNextAccount/rejected');
      expect(response?.payload).toBeUndefined();
    });
  });

  describe('Test triggerGetNextAccount async action', () => {
    it('should be fulfilled', async () => {
      const toggleModalCallerAuthenticationSpy = callerAuthenticationActionsSpy(
        'toggleModalCallerAuthentication'
      );
      const setAuthenticateDataSpy = callerAuthenticationActionsSpy(
        'setAuthenticateData'
      );
      const setWorkTypeSpy = timerWorkingActionsSpy('setWorkType');

      await triggerGetNextAccount({ storeId })(
        byPassFulfilled(getNextAccount.fulfilled.type, {
          data: {
            accId: JSON.stringify({
              maskedValue: 'maskedValue',
              eValue: 'eValue'
            }),
            customerName: 'customerName',
            memberSequenceIdentifier: '0001',
            socialSecurityIdentifier: '1'
          }
        }),
        store.getState,
        {}
      );

      expect(toggleModalCallerAuthenticationSpy).toBeCalled();
      expect(setAuthenticateDataSpy).toBeCalledWith({
        accountDetailData: {
          eValue: 'eValue',
          primaryName: 'customerName',
          memberSequenceIdentifier: '0001',
          maskedValue: 'maskedValue',
          socialSecurityIdentifier: '1'
        }
      });
      expect(setWorkTypeSpy).toBeCalledWith({
        eValue: 'eValue',
        workType: WorkType.QUEUE
      });
    });

    it('should be rejected', async () => {
      const addToastSpy = actionsToastSpy('addToast');

      await triggerGetNextAccount({ storeId })(
        byPassRejected(getNextAccount.rejected.type),
        store.getState,
        {}
      );

      expect(addToastSpy).toBeCalledWith({
        show: true,
        type: 'error',
        message: 'txt_failed_to_get_next_acc'
      });
    });

    it('should be rejected in catch', async () => {
      const addToastSpy = actionsToastSpy('addToast');

      await triggerGetNextAccount({ storeId })(
        byPassFulfilled(getNextAccount.fulfilled.type),
        store.getState,
        {}
      );

      expect(addToastSpy).toBeCalledWith({
        show: true,
        type: 'error',
        message: 'txt_failed_to_get_next_acc'
      });
    });
  });
});
