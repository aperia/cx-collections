import { QueueTypeEnum } from 'pages/AccountSearch/Home/Dashboard/QueueDetail/types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { NextActionsState } from '../types';
import {
  getNextAccount,
  getNextAccountBuilder,
  triggerGetNextAccount
} from './getNextAccount';

const { actions, reducer } = createSlice({
  name: 'nextActions',
  initialState: {} as NextActionsState,
  reducers: {
    setQueueId: (
      draftState,
      action: PayloadAction<{
        eValue: string;
        queueId?: string;
        queueType?: QueueTypeEnum;
      }>
    ) => {
      const { eValue, queueId, queueType } = action.payload;
      draftState[eValue] = {
        queueId,
        queueType
      };
    }
  },
  extraReducers: builder => {
    getNextAccountBuilder(builder);
  }
});

const nextActions = {
  ...actions,
  getNextAccount,
  triggerGetNextAccount
};

export { nextActions, reducer };
