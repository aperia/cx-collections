import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  isFulfilled,
  isRejected
} from '@reduxjs/toolkit';

// Service
import { nextActionsServices } from '../nextActionsServices';

// Types
import {
  GetNextAccountArgs,
  GetNextAccountPayload,
  NextActionsState,
  TriggerGetNextAccountArgs,
  TriggerGetNextAccountPayload
} from '../types';

// Actions
import { actions as tabActions } from 'pages/__commons/TabBar/_redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// Component
import { batch } from 'react-redux';
import { callerAuthenticationActions } from 'pages/CallerAuthenticate/_redux/reducers';
import { timerWorkingActions } from 'pages/TimerWorking/_redux/reducers';
import { WorkType } from 'pages/TimerWorking/types';
import { removeStoreRedux } from 'app/helpers';

export const getNextAccount = createAsyncThunk<
  GetNextAccountPayload,
  GetNextAccountArgs,
  ThunkAPIConfig
>('nextActions/getNextAccount', async (args, thunkAPI) => {
  const { storeId } = args;
  const response = await nextActionsServices.getNextAccount(storeId);
  return response;
});

export const getNextAccountBuilder = (
  builder: ActionReducerMapBuilder<NextActionsState>
) => {
  builder.addCase(getNextAccount.pending, (draftState, action) => {
    const { storeId } = action.meta.arg;
    draftState[storeId] = {
      ...draftState[storeId]
    };
  });
  builder.addCase(getNextAccount.fulfilled, (draftState, action) => {
    const { storeId } = action.meta.arg;
    const payload = action.payload;
    draftState[storeId] = {
      ...draftState[storeId],
      ...payload
    };
  });
  builder.addCase(getNextAccount.rejected, (draftState, action) => {
    const { storeId } = action.meta.arg;
    draftState[storeId] = {
      ...draftState[storeId]
    };
  });
};

export const triggerGetNextAccount = createAsyncThunk<
  TriggerGetNextAccountPayload,
  TriggerGetNextAccountArgs,
  ThunkAPIConfig
>('nextActions/getNextAccount', async (args, thunkAPI) => {
  const { tab } = thunkAPI.getState();
  const dispatch = thunkAPI.dispatch;
  const { storeId } = args;

  try {
    dispatch(tabActions.removeTab({ storeId }));
    // Remove data from store based on store id
    const removeTab = tab.tabs.find(item => item.storeId === storeId);
    dispatch(
      removeStoreRedux(removeTab?.tabType, storeId, removeTab?.accEValue)
    );
    const result = await dispatch(getNextAccount({ storeId }));

    if (isFulfilled(result)) {
      const data = (result?.payload as MagicKeyValue)?.data;
      const cus = JSON.parse(data.accId);

      // authenticate
      batch(() => {
        dispatch(callerAuthenticationActions.toggleModalCallerAuthentication());
        dispatch(
          timerWorkingActions.setWorkType({
            eValue: cus.eValue,
            workType: WorkType.QUEUE
          })
        );
        dispatch(
          callerAuthenticationActions.setAuthenticateData({
            accountDetailData: {
              eValue: cus.eValue,
              primaryName: data.customerName,
              memberSequenceIdentifier: data?.memberSequenceIdentifier,
              maskedValue: cus.maskedValue,
              socialSecurityIdentifier: data.socialSecurityIdentifier
            }
          })
        );
      });
      // end of authenticate
    }
    if (isRejected(result)) {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: 'txt_failed_to_get_next_acc'
        })
      );
    }
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: 'txt_failed_to_get_next_acc'
      })
    );
    return thunkAPI.rejectWithValue(error);
  }
});
