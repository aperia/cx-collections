import { QueueTypeEnum } from "pages/AccountSearch/Home/Dashboard/QueueDetail/types";

export interface NextActionsState {
  [storeId: string]: NextActionsData;
}

export interface NextActionsData {
  queueId?: string;
  queueType?: QueueTypeEnum;
  accountNumber?: string;
}

export interface GetNextAccountArgs {
  storeId: string;
}

export interface GetNextAccountPayload {}

export interface TriggerGetNextAccountArgs {
  storeId: string;
}

export interface TriggerGetNextAccountPayload {}
