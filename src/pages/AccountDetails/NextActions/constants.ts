export const NEXT_ACCOUNT = 'txt_next_account';
export const PREVIOUS_ACCOUNT = 'txt_previous_account';
export const QUEUE_DETAILS = 'txt_queue_details';
export const QUEUE_LIST = 'txt_queue_list';
export const SEARCH_PAGE = 'txt_search_page';

export const DEFAULT_NEXT_ACTIONS = [QUEUE_LIST, SEARCH_PAGE];
