import FloatingDropdownButton from 'app/_libraries/_dls/components/FloatingDropdownButton';
import React, { useMemo, useState } from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';
import { actions as tabActions } from 'pages/__commons/TabBar/_redux';
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { formatCommon, removeStoreRedux } from 'app/helpers';
import { takeTabData } from 'pages/__commons/TabBar/_redux/selectors';
import { TabState } from 'pages/__commons/TabBar/types';
import {
  DEFAULT_NEXT_ACTIONS,
  NEXT_ACCOUNT,
  PREVIOUS_ACCOUNT,
  QUEUE_DETAILS,
  QUEUE_LIST,
  SEARCH_PAGE
} from './constants';
import { dashboardActions } from 'pages/AccountSearch/Home/Dashboard/_redux/reducers';
import { selectQueueId } from './_redux/selector';
import { DASH } from 'app/constants';
import { selectedQueueList } from 'pages/QueueList/_redux/selectors';
import { QueueTypeEnum } from 'pages/AccountSearch/Home/Dashboard/QueueDetail/types';
import { isEqual } from 'lodash';
import { selectQueueAccountsData } from 'pages/AccountSearch/Home/Dashboard/QueueDetail/_redux/selectors';
import { takeQueueAccount } from 'pages/AccountSearch/Home/Dashboard/_redux/selectors';
import { queueAccountsActions } from 'pages/AccountSearch/Home/Dashboard/QueueDetail/_redux';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface NextActionsProps {}

const NextActions: React.FC<NextActionsProps> = () => {
  const dispatch = useDispatch();
  const { storeId, accEValue, accNbr = '' } = useAccountDetail();

  const { t } = useTranslation();

  const { tabs } = useStoreIdSelector<TabState>(takeTabData);

  const { queueId = '', queueType } =
    useSelector(selectQueueId(accEValue)) || {};

  const accounts = useSelector(selectQueueAccountsData(queueId));

  const { accountNumber } =
    accounts.find(acc => acc.accountNumber.slice(-4) === accNbr.slice(-4)) ||
    {};

  const defaultActions = useMemo(
    () =>
      DEFAULT_NEXT_ACTIONS.map((item, index) => ({
        id: index + 1,
        value: item
      })),
    []
  );

  const [action, setAction] = useState(defaultActions[0]);

  const [actionList, setActionList] = useState(defaultActions);

  const selectedTabDetail = tabs.find(item => item.storeId === storeId);

  const { previousAccount } = useSelector(takeQueueAccount) || {};

  const dropdownItems = useMemo(
    () =>
      actionList.map(item => {
        return (
          <FloatingDropdownButton.Item
            key={item.id}
            label={t(item.value)}
            value={item}
          />
        );
      }),
    [actionList, t]
  );

  React.useEffect(() => {
    let buttons = [...DEFAULT_NEXT_ACTIONS] as string[];

    if (queueId) {
      if (queueType === QueueTypeEnum.PULL_QUEUE) {
        buttons = [
          NEXT_ACCOUNT,
          ...(previousAccount ? [PREVIOUS_ACCOUNT] : []),
          QUEUE_DETAILS,
          ...buttons
        ];
      } else {
        buttons = [NEXT_ACCOUNT, ...buttons];
      }

      const newActionList = buttons.map((value, index) => ({
        id: index + 1,
        value: t(value)
      }));

      if (!isEqual(actionList, newActionList)) {
        setActionList(newActionList);
        setAction(newActionList[0]);
      }
    }
  }, [queueId, actionList, queueType, t, previousAccount]);

  const handleRemoveTab = () => {
    dispatch(tabActions.removeTab({ storeId }));
    // Remove data from store based on store id

    dispatch(
      removeStoreRedux(
        selectedTabDetail?.tabType,
        storeId,
        selectedTabDetail?.accEValue
      )
    );
  };

  const queueDetails = useSelector(selectedQueueList);

  const {
    queueName = '',
    assignedTo,
    totalNumbersOfAccount,
    totalOutstandingBalanceAmount
  } = queueDetails?.find(queueDetail => queueDetail.queueId === queueId) || {};

  const dataSnapshot = useMemo(() => {
    const workedAccountsCount = '1';
    const workedAccountsAmount = '40000';
    const remainingAccountsCount = `${
      +totalNumbersOfAccount - +workedAccountsCount
    }`;
    const remainingAccountsAmount = `${
      Number.parseFloat(totalOutstandingBalanceAmount) -
      Number.parseFloat(workedAccountsAmount)
    }`;

    return {
      queueId,
      queueName,
      assignedTo,
      createdDate: '11/23/2020',
      workedAccounts: `${
        +totalNumbersOfAccount - +remainingAccountsCount
      } | ${formatCommon(workedAccountsAmount).currency(2)}`,

      remainingAccounts: `${
        +totalNumbersOfAccount - +workedAccountsCount
      } | ${formatCommon(remainingAccountsAmount).currency(2)}`,

      totalAccounts: `${totalNumbersOfAccount} | ${formatCommon(
        totalOutstandingBalanceAmount
      ).currency(2)}`
    };
  }, [
    assignedTo,
    queueId,
    queueName,
    totalNumbersOfAccount,
    totalOutstandingBalanceAmount
  ]);

  const handleOnClick = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    if (action.value === t(NEXT_ACCOUNT) && queueId) {
      batch(() => {
        dispatch(dashboardActions.resetTime(true));
        dispatch(
          dashboardActions.triggerNextAccountWorkOnQueue({
            queueId,
            inAccountDetail: true,
            inQueueDetail: true,
            storeId,
            queueType,
            accountNumber
          })
        );
        dispatch(
          dashboardActions.getAccountLockTimeUpdate({
            acctWorkStatus: '0',
            storeId
          })
        );
      });
      handleRemoveTab();
    }

    if (previousAccount && action.value === t(PREVIOUS_ACCOUNT) && queueId) {
      const previousAccountNumber = previousAccount.accountNumber;

      batch(() => {
        dispatch(dashboardActions.resetTime(true));
        dispatch(
          dashboardActions.setQueueAccount({
            previousAccount: undefined,
            currentAccount: previousAccount
          })
        );
        dispatch(
          queueAccountsActions.getQueueAccountDetail({
            queueId,
            accountNumber: previousAccountNumber,
            queueType: QueueTypeEnum.PULL_QUEUE
          })
        );
        dispatch(
          dashboardActions.getAccountLockTimeUpdate({
            acctWorkStatus: '0',
            storeId
          })
        );
      });
      handleRemoveTab();
    }

    if (action.value === t(QUEUE_DETAILS)) {
      const queueDetailTab = tabs.find(tab => tab.tabType === 'queueDetail');

      if (queueDetailTab) {
        dispatch(
          tabActions.selectTab({
            storeId: queueDetailTab.storeId,
            accEValue: queueDetailTab.accEValue
          })
        );
      } else {
        const title = `${queueId} ${DASH} ${queueName}`;
        const newStoreId = `${queueId}${DASH}${queueName}`;

        dispatch(
          tabActions.addTab({
            title,
            storeId: newStoreId,
            iconName: 'file',
            tabType: 'queueDetail',
            props: {
              data: dataSnapshot
            }
          })
        );
      }

      handleRemoveTab();
    }

    if (action.value === t(QUEUE_LIST)) {
      const queueListTab = tabs.find(tab => tab.tabType === 'queueList');

      if (queueListTab) {
        dispatch(
          tabActions.selectTab({
            storeId: queueListTab.storeId,
            accEValue: queueListTab.accEValue
          })
        );
      } else {
        const newStoreId = new Date().valueOf().toString();

        dispatch(
          tabActions.addTab({
            title: 'txt_queue_list',
            storeId: newStoreId,
            accEValue: 'accEValue',
            iconName: 'file',
            tabType: 'queueList'
          })
        );
      }

      handleRemoveTab();
    }

    if (action.value === t(SEARCH_PAGE)) {
      dispatch(
        tabActions.selectTab({
          storeId: 'home'
        })
      );
      handleRemoveTab();
    }
  };

  return (
    <FloatingDropdownButton
      value={{
        ...action,
        value: t(action.value)
      }}
      textField="value"
      textFieldRender={value => value.FieldValue}
      popupBaseProps={{
        popupBaseClassName: 'inside-infobar'
      }}
      onChange={event => setAction(event.target.value)}
      onClickNavigation={event => {
        handleOnClick(event);
      }}
      dataTestId="account-detail-next-actions"
    >
      {dropdownItems}
    </FloatingDropdownButton>
  );
};

export default NextActions;
