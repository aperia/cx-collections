import { apiService } from 'app/utils/api.service';

export const nextActionsServices = {
  getNextAccount(accountId: string) {
    const url = window.appConfig?.api?.nextActions?.getNextAccount || '';
    const requestBody = { accountId };
    return apiService.post(url, requestBody);
  }
};
