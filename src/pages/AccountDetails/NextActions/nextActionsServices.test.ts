import { nextActionsServices } from './nextActionsServices';

// utils
import { storeId, mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

describe('nextActionsServices', () => {
  describe('getNextAccount', () => {
    const params = storeId;

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      nextActionsServices.getNextAccount(params);

      expect(mockService).toBeCalledWith('', { accountId: params });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      nextActionsServices.getNextAccount(params);

      expect(mockService).toBeCalledWith(apiUrl.nextActions.getNextAccount, {
        accountId: params
      });
    });
  });
});
