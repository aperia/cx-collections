import React from 'react';

// helpers
import { accEValue, renderMockStoreId, storeId } from 'app/test-utils';

// components
import AccountDetailPage from 'pages/AccountDetails';

import * as entitlementHelpers from 'app/entitlements/helpers';

const initialState: Partial<RootState> = {
  accountDetail: {
    [storeId]: {
      isLoading: false,
      isReloading: true,
      rawData: {
        externalStatusReasonCode: '123'
      }
    }
  },
  entitlement: {
    contactEntitlements: {
      [storeId]: {
        isLoading: false
      }
    }
  },
  accountDetailSnapshot: {
    [storeId]: {
      editExternalStatus: {
        externalConfig: {
          externalStatusCode: [],
          externalStatusReasonCode: [{ value: '123', description: 'des' }]
        }
      }
    }
  },
  nextActions: {
    [accEValue]: {
      data: '123'
    }
  },
  accessToken: {
    [storeId]: {
      accountInfo: {
        token: '123'
      }
    }
  }
};

HTMLCanvasElement.prototype.getContext = jest.fn();
const { getComputedStyle } = global.window;
window.getComputedStyle = (elm, select) => getComputedStyle(elm, select);

jest.mock('app/_libraries/_dls/hooks/useScreenType', () => () => 0);

describe('Test Account Detail Overview', () => {
  it('render to UI', () => {
    const { wrapper } = renderMockStoreId(
      <AccountDetailPage
        accEValue={accEValue}
        primaryName="John Han"
        storeId={storeId}
      />,
      { initialState }
    );

    expect(wrapper.container.innerHTML).toEqual(
      '<div class="h-100 loading" data-testid="accountDetails_tab"></div>'
    );
  });

  it('render to UI without queueId', () => {
    jest.spyOn(entitlementHelpers, 'checkPermission').mockReturnValue(true);

    const mockWithQueueId = {
      ...initialState,
      dashboard: {
        recentWorkQueue: {
          data: { collectorRecentWorkQueueList: [{ queueName: 'queueName' }] }
        }
      },
      nextActions: {
        [storeId]: {
          data: {}
        }
      }
    };
    const { wrapper } = renderMockStoreId(
      <AccountDetailPage
        accEValue={accEValue}
        primaryName="John Han"
        storeId={storeId}
      />,
      { initialState: mockWithQueueId }
    );

    expect(wrapper.container.innerHTML).toEqual(
      '<div class="h-100 loading" data-testid="accountDetails_tab"></div>'
    );
  });

  it('render to UI with relatedStoreId', () => {
    jest.spyOn(entitlementHelpers, 'checkPermission').mockReturnValue(true);

    const { wrapper } = renderMockStoreId(
      <AccountDetailPage
        primaryName="John Han"
        accEValue={accEValue}
        storeId={storeId}
        relatedStoreId={storeId}
      />,
      {
        initialState: {
          accountDetail: {
            [storeId]: {
              isReloading: true
            }
          },
          entitlement: {
            contactEntitlements: {
              [storeId]: {
                isLoading: false
              }
            }
          },
          accessToken: {
            [storeId]: {
              accountInfo: {
                token: '123'
              }
            }
          }
        }
      }
    );

    expect(wrapper.container.innerHTML).toEqual(
      '<div class="h-100" data-testid="accountDetails_tab"></div>'
    );
  });
});
