// Const
import {
  DASH,
  EMPTY_OBJECT,
  EMPTY_STRING,
  EMPTY_STRING_OBJECT
} from 'app/constants';
import { FormatTime } from 'app/constants/enums';
import { convertToDD_MM_YYYY, formatTimeDefault } from 'app/helpers';
import isUndefined from 'lodash.isundefined';
import cloneDeep from 'lodash.clonedeep';

// Type
import { AccountInfo } from './types';

export const mappingRefData = async <T extends MagicKeyValue>(
  object: T,
  data: string[]
) => {
  let dataReturn: T = cloneDeep(object);
  const importCallback = (name: string) => (resultImport: any) =>
    resultImport[name];
  for (const name of data) {
    const arrays = await import(`app/constants/${name}.ts`).then(
      importCallback(name)
    );
    const findItem: IRefData = arrays.find(
      (item: IRefData) => item.fieldID === object[name]
    );
    const getReturnDataItem = () => {
      if (isUndefined(findItem)) return {};
      const description = [
        'addressCategory',
        'addressType',
        'addressRelationship'
      ].includes(name)
        ? findItem.description
        : `${findItem?.fieldID} - ${findItem?.fieldValue}`;
      return {
        ...findItem,
        description
      };
    };

    dataReturn = {
      ...dataReturn,
      [name]: getReturnDataItem()
    };
  }
  return dataReturn;
};

export const parseLastSSNAccNbr = (mappedData: AccountInfo): AccountInfo => {
  try {
    const parseData = { ...mappedData };

    const { maskedValue = EMPTY_STRING, eValue } = JSON.parse(
      parseData?.accNbr || EMPTY_STRING_OBJECT
    ) as CommonIdentifier;

    const { maskedValue: maskedData = EMPTY_STRING } = JSON.parse(
      parseData?.ssn || EMPTY_STRING_OBJECT
    ) as CommonIdentifier;

    const [, , lastSSN = EMPTY_STRING] = maskedData?.split(DASH);

    parseData.accNbr = maskedValue;
    parseData.lastSSN = lastSSN;
    parseData.eValueAccountId = eValue;

    return parseData;
  } catch (error) {
    return EMPTY_OBJECT;
  }
};

/**
 * Get relationship search text from social security number
 */
export const parseRelationshipSearchText = (
  socialSecurityIdentifier: string
) => {
  try {
    const { eValue: relationshipSearchText = EMPTY_STRING } = JSON.parse(
      socialSecurityIdentifier || EMPTY_STRING_OBJECT
    ) as CommonIdentifier;

    return relationshipSearchText;
  } catch (e) {
    return EMPTY_STRING;
  }
};

export const prepareReturnData = (mappedData: AccountInfo): AccountInfo => {
  const parsedData = parseLastSSNAccNbr(mappedData);

  return {
    ...parsedData,
    monetary: {
      ...parsedData.monetary,
      existingPromiseToPay:
        formatTimeDefault(
          parsedData?.monetary?.existingPromiseToPay || '',
          FormatTime.Date
        ) ?? ''
    },
    historical: {
      ...parsedData?.historical,
      openDate: convertToDD_MM_YYYY(parsedData?.historical?.openDate || '')
    }
  };
};
