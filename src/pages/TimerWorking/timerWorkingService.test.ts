import { timerWorkingService } from './timerWorkingService';

// utils
import { mockApiServices, storeId } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import { LogWorkingTimeRequestBody } from './types';

describe('timerWorkingService', () => {
  describe('logWorkingTime', () => {
    const params: LogWorkingTimeRequestBody = {
      common: { accountId: storeId }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      timerWorkingService.logWorkingTime(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      timerWorkingService.logWorkingTime(params);

      expect(mockService).toBeCalledWith(
        apiUrl.timerWorking.logWorkingTime,
        params
      );
    });
  });

  describe('getTimerConfig', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      timerWorkingService.getTimerConfig({
        common: {
          app: 'app',
          org: 'org',
          accountId: 'accountId'
        }
      });

      expect(mockService).toBeCalledWith('', {
        common: {
          accountId: "accountId",
          app: "app",
          org: "org",
        },
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      timerWorkingService.getTimerConfig({
        common: {
          app: 'app',
          org: 'org',
          accountId: 'accountId'
        }
      });

      expect(mockService).toBeCalledWith(
        'fs/collectionsClientConfig/v1/user',
        {
          common: {
            app: 'app',
            org: 'org',
            accountId: 'accountId'
          }
        }
      );
    });
  });
});
