import { apiService } from 'app/utils/api.service';
import { LogWorkingTimeRequestBody } from './types';

export const timerWorkingService = {
  logWorkingTime(requestBody: LogWorkingTimeRequestBody) {
    const url = window.appConfig?.api?.timerWorking?.logWorkingTime || '';
    return apiService.post(url, requestBody);
  },
  getTimerConfig(requestBody: { common: Record<string, string> }) {
    const url = window.appConfig?.api?.timerWorking?.getTimerConfig || '';
    return apiService.post(url, requestBody);
  }
};
