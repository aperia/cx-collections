export const COLOR_CODE: Record<string, string> = {
  green: '#62D821',
  amber: '#FFB900',
  red: '#F72D1D'
};

export const DEFAULT_CONFIG = {
  green: [0, 179],
  amber: [180, 359],
  red: [360, 35999]
};

export const INFINITE_TIME = 35999;

export const COLLECTION_CODE_DEFAULT = '';
