import { timerWorkingActions, reducer } from './reducers';

// utils
import { storeId, accEValue } from 'app/test-utils';
import { WorkType } from '../types';

describe('Test reducer', () => {
  const defaultTimerWorkingState = {
    workingFromRecord: {},
    authenticateFromRecord: {},
    warningMessage: { [storeId]: 'Warning message' },
    config: {},
    workLogged: {},
    contactPerson: {},
    workTypeRecord: {},
    lastCollectionCodeRecord: {}
  };
  const mockDateNow = 1619600237447;
  describe('startWorking', () => {
    it('startWorking', () => {
      const oldDateNow = global.Date.now;
      global.Date.now = () => mockDateNow;
      const state = reducer(
        defaultTimerWorkingState,
        timerWorkingActions.startWorking(storeId)
      );

      expect(state).toEqual({
        ...defaultTimerWorkingState,
        workingFromRecord: { [storeId]: Date.now() },
        workLogged: {
          [storeId]: true
        }
      });
      global.Date.now = oldDateNow;
    });
    it('do nothing', () => {
      const existStoreIdState = {
        workingFromRecord: { [storeId]: mockDateNow },
        authenticateFromRecord: {},
        warningMessage: {},
        config: {},
        workLogged: { [storeId]: true },
        contactPerson: {},
        workTypeRecord: {},
        lastCollectionCodeRecord: {}
      };
      const state = reducer(
        existStoreIdState,
        timerWorkingActions.startWorking(storeId)
      );
      expect(state).toEqual(existStoreIdState);
    });
  });

  it('startAuthenticate', () => {
    const oldDateNow = global.Date.now;
    global.Date.now = () => mockDateNow;
    const state = reducer(
      defaultTimerWorkingState,
      timerWorkingActions.startAuthenticate(accEValue)
    );

    expect(state).toEqual({
      ...defaultTimerWorkingState,
      authenticateFromRecord: { [accEValue]: Date.now() }
    });
    global.Date.now = oldDateNow;
  });

  it('startAuthenticate with timeAuthenticate has already exists', () => {
    const oldDateNow = global.Date.now;
    global.Date.now = () => mockDateNow;
    const state = reducer(
      {
        ...defaultTimerWorkingState,
        authenticateFromRecord: { [accEValue]: mockDateNow - 100 }
      },
      timerWorkingActions.startAuthenticate(accEValue)
    );

    expect(state).toEqual({
      ...defaultTimerWorkingState,
      authenticateFromRecord: { [accEValue]: mockDateNow - 100 }
    });
    global.Date.now = oldDateNow;
  });

  it('setContactPersonName', () => {
    const mockPersonName = 'MockName';
    const state = reducer(
      defaultTimerWorkingState,
      timerWorkingActions.setContactPersonName({
        eValue: accEValue,
        contactPerson: mockPersonName
      })
    );

    expect(state).toEqual({
      ...defaultTimerWorkingState,
      contactPerson: { [accEValue]: mockPersonName }
    });
  });

  it('setWorkType', () => {
    const mockWorkType = WorkType.QUEUE;
    const state = reducer(
      defaultTimerWorkingState,
      timerWorkingActions.setWorkType({
        eValue: accEValue,
        workType: mockWorkType
      })
    );

    expect(state).toEqual({
      ...defaultTimerWorkingState,
      workTypeRecord: { [accEValue]: mockWorkType }
    });
  });

  it('setCllResultCode', () => {
    const mockLastCollectionForm = 'PP';
    const state = reducer(
      defaultTimerWorkingState,
      timerWorkingActions.setCallResultCode({
        storeId,
        lastCollectionCode: mockLastCollectionForm
      })
    );

    expect(state).toEqual({
      ...defaultTimerWorkingState,
      lastCollectionCodeRecord: { [storeId]: mockLastCollectionForm }
    });
  });

  it('reset Warning message', () => {
    const state = reducer(
      defaultTimerWorkingState,
      timerWorkingActions.resetWarningMsg(storeId)
    );
    expect(state.warningMessage[storeId]).toEqual('');
  });
});
