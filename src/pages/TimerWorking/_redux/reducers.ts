import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import {
  triggerStopWorking,
  triggerStopWorkingBuilder
} from './triggerStopWorking';

import {
  getTimerWorkingBuilder,
  getTimerWorkingConfig
} from './getTimerWorkingConfig';
import { TimerWorkingState, WorkType } from '../types';
import { getSessionStorage } from 'app/helpers';

const savedTimerWorking = getSessionStorage('timerWorking');

const initialState: TimerWorkingState =
  (savedTimerWorking as TimerWorkingState) || {
    workingFromRecord: {},
    authenticateFromRecord: {},
    warningMessage: {},
    config: {},
    workLogged: {},
    contactPerson: {},
    workTypeRecord: {},
    lastCollectionCodeRecord: {}
  };

const { actions, reducer } = createSlice({
  name: 'timerWorking',
  initialState,
  reducers: {
    startWorking: (draftState, action: PayloadAction<string>) => {
      const storeId = action.payload;
      if (draftState.workLogged[storeId]) return;
      draftState.workingFromRecord[action.payload] = Date.now();
      draftState.workLogged = {
        ...draftState.workLogged,
        [storeId]: true
      };
    },
    startAuthenticate: (draftState, action: PayloadAction<string>) => {
      if (!draftState.authenticateFromRecord[action.payload]) {
        draftState.authenticateFromRecord[action.payload] = Date.now();
      }
    },
    resetWarningMsg: (draftState, action: PayloadAction<string>) => {
      const storeId = action.payload;
      draftState.warningMessage[storeId] = '';
    },
    setContactPersonName: (
      draftState,
      action: PayloadAction<{ eValue: string; contactPerson: string }>
    ) => {
      const { eValue, contactPerson } = action.payload;
      draftState.contactPerson[eValue] = contactPerson;
    },
    setWorkType: (
      draftState,
      action: PayloadAction<{ eValue: string; workType: WorkType }>
    ) => {
      const { eValue, workType } = action.payload;
      draftState.workTypeRecord[eValue] = workType;
    },
    setCallResultCode: (
      draftState,
      action: PayloadAction<{ storeId: string; lastCollectionCode: string }>
    ) => {
      const { storeId, lastCollectionCode } = action.payload;
      draftState.lastCollectionCodeRecord[storeId] = lastCollectionCode;
    }
  },
  extraReducers: builder => {
    getTimerWorkingBuilder(builder);
    triggerStopWorkingBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getTimerWorkingConfig,
  triggerStopWorking
};

export { allActions as timerWorkingActions, reducer };
