import { responseDefault, storeId, accEValue } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { timerWorkingService } from '../timerWorkingService';
import { WorkType } from '../types';
import { triggerStopWorking } from './triggerStopWorking';

describe('triggerStopWorking', () => {
  const mockDateNow = 1619600237447;
  const initialState: Partial<RootState> = {
    timerWorking: {
      workingFromRecord: { [storeId]: mockDateNow - 1000 },
      authenticateFromRecord: { [accEValue]: mockDateNow - 2000 },
      contactPerson: 'SmithJohn',
      workTypeRecord: { [accEValue]: WorkType.QUEUE },
      lastCollectionCodeRecord: {
        [storeId]: 'AAA'
      }
    },
    collectionForm: {
      [storeId]: {
        customCallResult: [{ code: 'AAA' }]
      } as any
    }
  };

  it('async thunk', async () => {
    const oldDateNow = global.Date.now;
    global.Date.now = () => mockDateNow;
    const mockResolvedData = { status: 'OK' };
    const spy = jest
      .spyOn(timerWorkingService, 'logWorkingTime')
      .mockResolvedValue({ ...responseDefault, data: mockResolvedData });
    const state: Partial<RootState> = {
      timerWorking: {
        workingFromRecord: { [storeId]: mockDateNow - 1000 },
        authenticateFromRecord: { [accEValue]: mockDateNow - 2000 },
        contactPerson: 'SmithJohn',
        workTypeRecord: { [accEValue]: WorkType.QUEUE },
        lastCollectionCodeRecord: {}
      },
      collectionForm: {
        [storeId]: {
          customCallResult: [{ code: 'AAA' }]
        } as any
      }
    };
    const store = createStore(rootReducer, state);
    const response = await triggerStopWorking({ storeId, accEValue })(
      store.dispatch,
      store.getState,
      initialState
    );

    expect(response.payload).toEqual(mockResolvedData);
    spy.mockReset();
    spy.mockRestore();
    global.Date.now = oldDateNow;
  });

  it('async thunk > case 2', async () => {
    const oldDateNow = global.Date.now;
    global.Date.now = () => mockDateNow;
    const mockResolvedData = { status: 'OK' };
    const spy = jest
      .spyOn(timerWorkingService, 'logWorkingTime')
      .mockResolvedValue({ ...responseDefault, data: mockResolvedData });
    const store = createStore(rootReducer, { ...initialState });
    const response = await triggerStopWorking({ storeId, accEValue })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual(mockResolvedData);
    spy.mockReset();
    spy.mockRestore();
    global.Date.now = oldDateNow;
  });
});

describe('trigger Stop working builder', () => {
  let store;
  let state: RootState;
  beforeEach(() => {
    store = createStore(rootReducer, {
      timerWorking: {
        workingFromRecord: { [storeId]: 1111 },
        config: {},
        warningMessage: {},
        workLogged: {},
        contactPerson: {},
        authenticateFromRecord: {},
        workTypeRecord: {},
        lastCollectionCodeRecord: {}
      }
    });
    state = store.getState();
  });
  it('should be fulfilled', () => {
    const fulfilled = triggerStopWorking.fulfilled(
      undefined,
      triggerStopWorking.fulfilled.type,
      { storeId, accEValue }
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual.timerWorking.workingFromRecord).toEqual({});
  });

  it('should be rejected', () => {
    const rejected = triggerStopWorking.rejected(
      new Error('rejected'),
      triggerStopWorking.rejected.type,
      { storeId, accEValue }
    );
    const actual = rootReducer(state, rejected);

    expect(actual.timerWorking.workingFromRecord).toEqual({});
  });
});
