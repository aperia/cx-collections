import { selectorWrapper, storeId } from 'app/test-utils';
import { selectWorkingFrom, selectWorkingConfig, selectWaringMessage } from './selector';

const mockConfig = {
  green: [0, 599],
  amber: [600, 1199],
  red: [1200, 35999]
};

const mockDateNow = 1619600237447;

const mockWarningMessage = 'mock test mockWarningMessage'

const states: Partial<RootState> = {
  timerWorking: {
    workingFromRecord: {
      [storeId]: mockDateNow
    },
    warningMessage: {
      [storeId]: mockWarningMessage
    },
    config: { [storeId]: mockConfig }
  }
};

const selectorTrigger = selectorWrapper(states);

describe('Timer working selector', () => {
  it('selectWorkingFrom', () => {
    const { data } = selectorTrigger(selectWorkingFrom);
    expect(data).toEqual(mockDateNow);
  });

  it('selectWorkingConfig', () => {
    const { data } = selectorTrigger(selectWorkingConfig);
    expect(data).toEqual(mockConfig);
  });
  
  it('selectWaringMessage', () => {
    const { data } = selectorTrigger(selectWaringMessage);
    expect(data).toEqual(mockWarningMessage);
  });
});
