import { createSelector } from '@reduxjs/toolkit';
import { WorkingConfig } from '../types';

export const selectWorkingFrom = createSelector(
  (states: RootState, storeId: string) => {
    return states.timerWorking.workingFromRecord[storeId];
  },
  (data: number) => data
);

export const selectWorkingConfig = createSelector(
  (states: RootState, storeId: string) => {
    return states.timerWorking.config[storeId];
  },
  (data: WorkingConfig) => data
);

export const selectWaringMessage = createSelector(
  (states: RootState, storeId: string) =>
    states.timerWorking.warningMessage[storeId],
  data => data
);
