import { createStore } from '@reduxjs/toolkit';
import { responseDefault, storeId } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import { DEFAULT_CONFIG } from '../constants';
import { timerWorkingService } from '../timerWorkingService';
import { getTimerWorkingConfig } from './getTimerWorkingConfig';

const mockConfig = {
  green: { hour: '00', minute: '00', second: '00' },
  amber: { hour: '00', minute: '03', second: '00' },
  red: { hour: '00', minute: '06', second: '00' },
  codeToText: {
    components: [
      {
        id: 1,
        component: 'component_timer_config',
        jsonValue: {
          green: { hour: '00', minute: '00', second: '00' },
          amber: { hour: '00', minute: '03', second: '00' },
          red: { hour: '00', minute: '06', second: '00' }
        }
      }
    ]
  }
};

describe('getTimerWorkingConfig', () => {
  it('async thunk', async () => {
    const spy = jest
      .spyOn(timerWorkingService, 'getTimerConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: mockConfig
      });

    const store = createStore(rootReducer, {});
    const response = await getTimerWorkingConfig({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      amber: { hour: '00', minute: '03', second: '00' },
      green: { hour: '00', minute: '00', second: '00' },
      red: { hour: '00', minute: '06', second: '00' }
    });
    spy.mockReset();
    spy.mockRestore();
  });

  it('async thunk case 2 > not have filterConfig', async () => {
    const spy = jest
      .spyOn(timerWorkingService, 'getTimerConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          ...mockConfig,
          codeToText: {
            components: [{ id: 1, component: 'component_timer_configxx', jsonValue: {} }]
          }
        }
      });

    const store = createStore(rootReducer, {});
    const response = await getTimerWorkingConfig({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ errorMessage: 'no data' });

    spy.mockReset();
    spy.mockRestore();
  });
});

describe('getTimerWorkingBuilder ', () => {
  let store;
  let state: RootState;
  beforeEach(() => {
    store = createStore(rootReducer, { timerWorking: {} });
    state = store.getState();
  });
  it('fulfilled', () => {
    const fulfilled = getTimerWorkingConfig.fulfilled(
      {
        ...mockConfig,
        showMessage: true,
        message: 'Warning message'
      },
      getTimerWorkingConfig.fulfilled.type,
      { storeId }
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual.timerWorking.config).toEqual({
      [storeId]: { amber: [180, 359], green: [0, 179], red: [360, 35999] }
    });
  });

  it('fulfilled with empty data', () => {
    const fulfilled = getTimerWorkingConfig.fulfilled(
      mockConfig,
      getTimerWorkingConfig.fulfilled.type,
      { storeId }
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual.timerWorking.config).toEqual({
      [storeId]: { amber: [180, 359], green: [0, 179], red: [360, 35999] }
    });
  });

  it('rejected', () => {
    const rejected = getTimerWorkingConfig.rejected(
      new Error('rejected'),
      getTimerWorkingConfig.rejected.type,
      { storeId }
    );
    const actual = rootReducer(state, rejected);

    expect(actual.timerWorking.config[storeId]).toEqual(DEFAULT_CONFIG);
    expect(actual.timerWorking.warningMessage[storeId]).toEqual('');
  });
});
