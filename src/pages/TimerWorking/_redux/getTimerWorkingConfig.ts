import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { TIMER_COMPONENT_ID } from 'app/constants';
import { timeToSecond } from 'app/helpers';
import { FormValues } from 'pages/ClientConfiguration/Timer/types';
import { selectClientInfoGetByIdRequest } from 'pages/__commons/AccountDetailTabs/_redux';
import { DEFAULT_CONFIG, INFINITE_TIME } from '../constants';
import { timerWorkingService } from '../timerWorkingService';

import { TimerWorkingState } from '../types';

export const getTimerWorkingConfig = createAsyncThunk<
  FormValues,
  StoreIdPayload,
  ThunkAPIConfig
>('timerWorking/getConfig', async (args, thunkAPI) => {
  const { getState } = thunkAPI;
  const commonClientInfoRequest = selectClientInfoGetByIdRequest(
    getState(),
    args.storeId
  );

  const { data } = await timerWorkingService.getTimerConfig({
    common: { ...commonClientInfoRequest.common }
  });

  const filterConfig = (
    data.codeToText.components as CollectionsClientConfigItem['components']
  ).find(item => item.component === TIMER_COMPONENT_ID);

  if (!filterConfig) {
    return thunkAPI.rejectWithValue({ errorMessage: 'no data' });
  }
  return filterConfig.jsonValue as FormValues;
});

export const getTimerWorkingBuilder = (
  builder: ActionReducerMapBuilder<TimerWorkingState>
) => {
  builder.addCase(getTimerWorkingConfig.fulfilled, (draftState, action) => {
    const { storeId } = action.meta.arg;
    const { red, amber, showMessage, message } = action.payload;
    const amberPoint = timeToSecond(amber, 179);
    const redPoint = timeToSecond(red, 359);
    draftState.config = {
      ...draftState.config,
      [storeId]: {
        green: [0, amberPoint - 1],
        amber: [amberPoint, redPoint - 1],
        red: [redPoint, INFINITE_TIME]
      }
    };
    draftState.warningMessage = {
      ...draftState.warningMessage,
      [storeId]: showMessage && message ? message : ''
    };
  });
  builder.addCase(getTimerWorkingConfig.rejected, (draftState, action) => {
    const { storeId } = action.meta.arg;
    draftState.config = { ...draftState.config, [storeId]: DEFAULT_CONFIG };
    draftState.warningMessage = { ...draftState.warningMessage, [storeId]: '' };
  });
};
