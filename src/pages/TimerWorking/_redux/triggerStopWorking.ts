import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { diffTimeToHumanTime } from '../helpers';
import dateTime from 'date-and-time';
import { timerWorkingService } from '../timerWorkingService';
import { FormatTime } from 'app/constants/enums';
import { LogWorkingTimeRequestBody, TimerWorkingState } from '../types';
import { COLLECTION_CODE_DEFAULT } from '../constants';
import { takeUserToken } from 'pages/__commons/RenewToken/_redux/selectors';

export const triggerStopWorking = createAsyncThunk<
  undefined,
  { storeId: string; accEValue: string },
  ThunkAPIConfig
>('timerWorking/triggerStopWorking', async (args, thunkAPI) => {
  const globalState = thunkAPI.getState();
  const { timerWorking, sharedInfo, accountDetail, collectionForm } =
    globalState;
  const authToken = takeUserToken(globalState);
  const { storeId, accEValue } = args;
  const {
    workingFromRecord,
    authenticateFromRecord,
    workTypeRecord,
    contactPerson: contactPersonRecord,
    lastCollectionCodeRecord
  } = timerWorking;
  const workingFrom = workingFromRecord[storeId];
  const authenFrom = authenticateFromRecord[accEValue];
  const callResultCodeDescription =
    lastCollectionCodeRecord[storeId] ?? COLLECTION_CODE_DEFAULT;
  const callResultRefData = collectionForm[storeId].customCallResult;
  const actionCode = callResultRefData?.find(
    item => item.code === callResultCodeDescription
  );
  const workType = workTypeRecord[accEValue];
  const diffTime = Date.now() - workingFrom;
  const dialPopTime = Math.abs(
    Math.floor((workingFrom - authenFrom) / 1000)
  ).toString();
  const callDuration = diffTimeToHumanTime(diffTime);
  const { data: sharedInfoData } = sharedInfo;
  const { commonConfig } = window.appConfig || {};

  const { phoneType, stateCode, zipCode } =
    accountDetail[storeId]?.sharedInfoTimer || {};

  const requestBody: LogWorkingTimeRequestBody = {
    common: {
      app: commonConfig?.app ?? 'cx',
      accountId: accEValue,
      auth: authToken ?? ''
    },
    callingApplication: 'APR01', // hard code
    callDate: dateTime.format(new Date(), FormatTime.DateWithHyphen),
    callDuration,
    callResultCodeDescription,
    cardholderContactedName:
      accountDetail[storeId]?.selectedAuthenticationData
        ?.customerNameForCreateWorkflow ?? '',
    dialPopTime,
    endofCallAction: sharedInfoData.endofCallAction,
    operatorCode: commonConfig?.operatorID ?? '',
    phoneType,
    stateCode,
    zipCode,
    workType: workType as string,
    phoneNumber: accountDetail[storeId]?.numberPhoneAuthenticated,
    cardholderContactType: contactPersonRecord[accEValue],
    countAsContact: actionCode?.countAsContact ?? ''
  };
  const { data } = await timerWorkingService.logWorkingTime(requestBody);
  return data;
});

export const triggerStopWorkingBuilder = (
  builder: ActionReducerMapBuilder<TimerWorkingState>
) => {
  builder.addCase(triggerStopWorking.fulfilled, (draftState, action) => {
    const { storeId, accEValue } = action.meta.arg;
    delete draftState.workingFromRecord?.[storeId];
    delete draftState.config?.[storeId];
    delete draftState.warningMessage?.[storeId];
    delete draftState.workLogged?.[storeId];
    delete draftState.contactPerson?.[accEValue];
    delete draftState.authenticateFromRecord?.[accEValue];
    delete draftState.workTypeRecord?.[accEValue];
    delete draftState.lastCollectionCodeRecord?.[storeId];
  });
  builder.addCase(triggerStopWorking.rejected, (draftState, action) => {
    const { storeId, accEValue } = action.meta.arg;
    delete draftState.workingFromRecord?.[storeId];
    delete draftState.config?.[storeId];
    delete draftState.warningMessage?.[storeId];
    delete draftState.workLogged?.[storeId];
    delete draftState.contactPerson?.[accEValue];
    delete draftState.authenticateFromRecord?.[accEValue];
    delete draftState.workTypeRecord?.[accEValue];
    delete draftState.lastCollectionCodeRecord?.[storeId];
  });
};
