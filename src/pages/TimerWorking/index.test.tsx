import React from 'react';
import { screen } from '@testing-library/react';
import {
  renderMockStore,
  storeId,
  accEValue,
  mockActionCreator
} from 'app/test-utils';

import { AccountDetailProvider } from 'app/hooks';

import TimeWorking from './index';
import { WorkType } from './types';
import { timerWorkingActions } from './_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { act } from 'react-dom/test-utils';

const mockAction = mockActionCreator(timerWorkingActions);
const mockToast = mockActionCreator(actionsToast);

const generateState = (
  initTime?: number,
  config?: Record<string, number[]>
) => {
  const state: Partial<RootState> = {
    timerWorking: {
      workingFromRecord: initTime
        ? {
            [storeId]: initTime
          }
        : {},
      config: config
        ? { [storeId]: config }
        : {
            [storeId]: {
              green: [0, 599],
              amber: [600, 1199],
              red: [1200, 35999]
            }
          },
      authenticateFromRecord: initTime
        ? {
            [accEValue]: initTime - 1000
          }
        : {},
      workLogged: initTime ? { [storeId]: true } : {},
      contactPerson: { [storeId]: 'JohnSmith' },
      workTypeRecord: { [accEValue]: WorkType.QUEUE },
      lastCollectionCodeRecord: { [storeId]: '' },
      warningMessage: { [storeId]: 'Warning message' }
    }
  };
  return state;
};

beforeEach(() => {
  mockAction('getTimerWorkingConfig');
});

describe('TimeWorking', () => {
  const renderMockStoreId = (
    initTime?: number,
    config?: Record<string, number[]>
  ) => {
    return renderMockStore(
      <AccountDetailProvider value={{ storeId, accEValue }}>
        <TimeWorking />
      </AccountDetailProvider>,
      { initialState: generateState(initTime, config) }
    );
  };
  it('return null when timerWorking == 0', () => {
    const { container } = renderMockStoreId();
    expect(container.firstChild).toBeNull();
  });

  it('return null when has no config', () => {
    jest.useFakeTimers();
    const { container } = renderMockStoreId(Date.now() - 1000, {});
    jest.advanceTimersByTime(1000);
    expect(container.firstChild).toBeNull();
  });

  it('count after 1s', () => {
    jest.useFakeTimers();
    renderMockStoreId(Date.now() - 1000);
    act(() => {
      jest.advanceTimersByTime(1000);
    });
    expect(screen.getByText('00:00:01')).toBeInTheDocument();
  });

  it('show warning toast', () => {
    const mAddToast = mockToast('addToast');
    const mockResetWarning = mockAction('resetWarningMsg');
    jest.useFakeTimers();
    renderMockStoreId(Date.now() - 1200 * 1000);
    act(() => {
      jest.advanceTimersByTime(1200 * 1000);
    });
    expect(mAddToast).toBeCalled();
    expect(mockResetWarning).toBeCalledWith(storeId);
  });
});
