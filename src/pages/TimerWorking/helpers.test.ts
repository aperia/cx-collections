import { diffTimeToHumanTime } from './helpers';

describe('diffTimeToHumanTime', () => {
  it('its working correctly', () => {
    const result = diffTimeToHumanTime(500000);
    expect(result).toEqual('00:08:20');
  });
});
