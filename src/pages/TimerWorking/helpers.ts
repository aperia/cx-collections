export const diffTimeToHumanTime = (diff: number): string => {
  const seconds = Math.floor((diff / 1000) % 60);
  const minutes = Math.floor((diff / (1000 * 60)) % 60);
  const hours = Math.floor((diff / (1000 * 60 * 60)) % 24);
  const result = [hours, minutes, seconds].map(item =>
    item < 10 ? `0${item}` : `${item}`
  );
  return result.join(':');
};
