export type WorkingRecord = Record<string, number>;

export type AuthenticateTimeRecord = Record<string, number>;

export type WorkingConfig = Record<string, number[]> | undefined;

export enum WorkType {
  ACCOUNT_SEARCH = 'AccountSearch',
  QUEUE = 'Queue',
  DIALER = 'Dialer'
}

export interface TimerWorkingState {
  workingFromRecord: WorkingRecord;
  authenticateFromRecord: AuthenticateTimeRecord;
  config: Record<string, WorkingConfig>;
  warningMessage: Record<string, string>;
  workLogged: Record<string, boolean>;
  contactPerson: Record<string, string>;
  workTypeRecord: Record<string, WorkType>;
  lastCollectionCodeRecord: Record<string, string>;
}

export interface LogWorkingTimeRequestBody {
  common: {
    app: string;
    accountId: string;
    auth: string;
  };

  callingApplication: string;
  callDuration: string;
  callDate: string;
  callResultCodeDescription: string;
  operatorCode: string;
  dialPopTime: string;
  cardholderContactedName: string;
  endofCallAction: string;
  stateCode: string;
  zipCode: string;
  phoneType: string;
  workType?: string;
  phoneNumber?: string;
  cardholderContactType: string;
  countAsContact: boolean | string;
}
