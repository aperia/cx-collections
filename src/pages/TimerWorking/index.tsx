import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useState
} from 'react';
import { batch, useDispatch } from 'react-redux';
import classNames from 'classnames';
import isEmpty from 'lodash.isempty';
// components
import TimeIcon from 'pages/__commons/TimeIcon';
// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
// redux
import {
  selectWaringMessage,
  selectWorkingConfig,
  selectWorkingFrom
} from './_redux/selector';
import { timerWorkingActions } from './_redux/reducers';
// constants
import { COLOR_CODE } from './constants';

// helpers
import { diffTimeToHumanTime } from './helpers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { WorkingConfig } from './types';

import { genAmtId } from 'app/_libraries/_dls/utils';
export interface TimerProps {
  dataTestId?: string;
  className?: string;
}

export const TimerWorking: React.FC<TimerProps> = ({
  dataTestId,
  className
}) => {
  const { storeId } = useAccountDetail();

  const dispatch = useDispatch();
  const [timerElement, setTimerElement] = useState<HTMLDivElement | null>(null);

  const workingFrom = useStoreIdSelector<number>(selectWorkingFrom);
  const workingConfig = useStoreIdSelector<WorkingConfig>(selectWorkingConfig);
  const warningMsg = useStoreIdSelector<string>(selectWaringMessage);

  const [workingTime, setWorkingTime] = useState(0);
  const [variant, setVariant] = useState('');

  const getVariant = useCallback(
    (diffTime: number) => {
      if (!workingConfig || isEmpty(workingConfig)) return '';
      let result = '';
      Object.entries(workingConfig).forEach(([ky, value]) => {
        if (value[0] <= diffTime && value[1] >= diffTime) {
          result = COLOR_CODE[ky];
        }
      });
      return result;
    },
    [workingConfig]
  );

  useLayoutEffect(() => {
    if (!timerElement) return;
    timerElement.style.borderColor = variant;
  }, [variant, timerElement]);

  useEffect(() => {
    if (isEmpty(workingConfig)) {
      dispatch(timerWorkingActions.getTimerWorkingConfig({ storeId }));
    }
  }, [dispatch, storeId, workingConfig]);

  useEffect(() => {
    if (!workingFrom) return;
    const workingTimeId = setInterval(() => {
      const diff = Date.now() - workingFrom;
      const variantResult = getVariant(
        Math.floor((Date.now() - workingFrom) / 1000)
      );
      if (!variantResult) return;
      setVariant(variantResult);
      setWorkingTime(diff);
    }, 1000);

    return () => {
      clearInterval(workingTimeId);
    };
  }, [getVariant, workingFrom]);

  useEffect(() => {
    if (warningMsg && variant === COLOR_CODE.red) {
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'warning',
            message: warningMsg
          })
        );
        dispatch(timerWorkingActions.resetWarningMsg(storeId));
      });
    }
  }, [dispatch, variant, warningMsg, storeId]);

  if (!workingTime) return null;

  return (
    <div
      className={classNames('timer-working', className)}
      ref={setTimerElement}
    >
      <div className="d-flex align-items-center px-8">
        <TimeIcon color={variant} />
        <span
          className="color-grey-d20 ml-8 fs-14 py-6 fw-400"
          data-testid={genAmtId(
            dataTestId!,
            'timer-working_time-counter',
            'TimerWorking'
          )}
        >
          {diffTimeToHumanTime(workingTime)}
        </span>
      </div>
    </div>
  );
};

export default TimerWorking;
