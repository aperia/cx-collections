import React from 'react';
import ConfirmCannotAuthentication from './ConfirmCannotAuthenticationModal';
import AuthenticateModal from './AuthenticateModal';

const CallerAuthenticationWrapper: React.FC<{}> = () => {
  return (
    <>
      <AuthenticateModal />
      <ConfirmCannotAuthentication />
    </>
  );
};

export default CallerAuthenticationWrapper;
