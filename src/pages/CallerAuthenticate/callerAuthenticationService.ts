import { apiService } from 'app/utils/api.service';
import { CallerAuthenticationPostData } from './types';

export const callerAuthenticationService = {
  getCallerAuthentication(
    bodyRequest: CallerAuthenticationPostData,
    token?: string
  ) {
    const url =
      window.appConfig?.api?.callerAuthentication?.getCallerAuthentication ||
      '';
    return apiService.post(url, bodyRequest, {
      headers: { Authorization: token }
    });
  },

  updateCallerAuthentication() {
    return new Promise<any>((resolve, reject) => {
      setTimeout(() => {
        resolve({ data: { message: 'Update successfully' } });
      }, 400);
    });
  },
  getAddressInfo(bodyRequest: CallerAuthenticationPostData, token?: string) {
    const url =
      window.appConfig?.api?.callerAuthentication?.getAddressInfo || '';
    return apiService.post(url, bodyRequest, {
      headers: { Authorization: token }
    });
  },
  triggerRenewToken(bodyRequest: RenewTokenPayload) {
    const url = window.appConfig?.api?.common?.renewToken || '';
    return apiService.post(url, bodyRequest);
  },
  checkTypeofAddress: (data: MagicKeyValue, token?: string) => {
    const url =
      window.appConfig?.api?.callerAuthentication?.checkTypeAddress || '';
    return apiService.post(url, data, {
      headers: { Authorization: token }
    });
  },
  getPostalCodeList() {
    const url = window.appConfig?.api?.callerAuthentication?.getPostalCodeList;
    return apiService.get(url!);
  }
};
