import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import React from 'react';
import AuthenticateModal from './AuthenticateModal';
import { fireEvent, screen } from '@testing-library/react';
import { callerAuthenticationActions } from './_redux/reducers';

HTMLCanvasElement.prototype.getContext = jest.fn();

const callerAuthenticationActionsSpy = mockActionCreator(
  callerAuthenticationActions
);

describe('Test AuthenticateModal component', () => {
  it('not render modal', () => {
    const { wrapper } = renderMockStoreId(<AuthenticateModal />);

    expect(wrapper.container.innerHTML).toEqual('');
  });

  it('render modal', () => {
    callerAuthenticationActionsSpy('getCallerAuthentication');

    jest.useFakeTimers();
    renderMockStoreId(<AuthenticateModal />, {
      initialState: {
        callerAuthentication: {
          isOpenModal: true
        }
      }
    });
    jest.runAllTimers();

    expect(screen.getByText('txt_contact_authentication')).toBeInTheDocument();
    expect(screen.getByText('txt_bypass_authentication')).toBeInTheDocument();
    expect(screen.getByText('txt_cannot_authenticate')).toBeInTheDocument();
    expect(screen.getByText('txt_authenticate')).toBeInTheDocument();
    expect(
      screen.getByText('txt_select_a_contact_to_authenticate')
    ).toBeInTheDocument();
    expect(
      screen.getByText('txt_security_verification_required_before_proceeding')
    ).toBeInTheDocument();
  });

  it('handles to close modal', () => {
    callerAuthenticationActionsSpy('getCallerAuthentication');
    const toggleModalCallerAuthenticationSpy = callerAuthenticationActionsSpy(
      'toggleModalCallerAuthentication'
    );

    jest.useFakeTimers();
    renderMockStoreId(<AuthenticateModal />, {
      initialState: {
        callerAuthentication: {
          isOpenModal: true
        }
      }
    });
    jest.runAllTimers();

    const closeBtnList = screen.getAllByRole('button');
    fireEvent.click(closeBtnList[0]);

    expect(toggleModalCallerAuthenticationSpy).toBeCalled();
  });
});
