import React from 'react';
import { render } from '@testing-library/react';
import { formatCommon } from 'app/helpers';
import { QUESTION_CONFIG_ID } from 'pages/ClientConfiguration/Authentication/constants';

import { AuthenticationInfoView } from './AuthenticationInfoView';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => () => ({
  t: (text: string) => text
}));

const mockMotherMaidenName = 'Maiden name';
const mockLastSSN = '1234';
const mockCreditLimitAmount = '30';

describe('test Authentication InfoView component', () => {
  it('should be render', () => {
    const { getByText, queryByText } = render(
      <AuthenticationInfoView
        data={{
          mothersMaidenName: mockMotherMaidenName,
          socialSecurityIdentifier: mockLastSSN,
          creditLimitAmount: mockCreditLimitAmount
        }}
        questionConfig={[
          QUESTION_CONFIG_ID.MOTHERS_MAIDEN_NAME,
          QUESTION_CONFIG_ID.CREDIT_LIMIT
        ]}
      />
    );
    expect(getByText(mockMotherMaidenName)).toBeInTheDocument();
    expect(
      getByText(formatCommon(mockCreditLimitAmount).currency(2))
    ).toBeInTheDocument();
    expect(queryByText(mockLastSSN)).toEqual(null);
  });

  it('should be render without data', () => {
    const { queryByText } = render(
      <AuthenticationInfoView
        data={undefined as never}
        questionConfig={[QUESTION_CONFIG_ID.MOTHERS_MAIDEN_NAME]}
      />
    );
    expect(queryByText(mockMotherMaidenName)).toEqual(null);
  });
});
