import React from 'react';

// redux
import { callerAuthenticationActions } from './_redux/reducers';

// Component
import ConfirmCannotAuthenticationModal from './ConfirmCannotAuthenticationModal';

// Helper
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';

// Hooks
import { screen } from '@testing-library/react';

const initialState: Partial<RootState> = {
  callerAuthentication: {
    modalConfirmOpen: true,
    authenticationInfo: {
      data: [{ customerName: 'customer name', accountIdentifier: '' }]
    }
  }
};

const callerAuthenticationSpy = mockActionCreator(callerAuthenticationActions);

describe('Test ConfirmCannotAuthenticationModal component', () => {
  it('not render modal', () => {
    const { wrapper } = renderMockStoreId(
      <ConfirmCannotAuthenticationModal />,
      {
        initialState: {
          callerAuthentication: {
            modalConfirmOpen: false
          }
        }
      }
    );

    expect(wrapper.container.innerHTML).toEqual('');
  });

  it('render modal', () => {
    renderMockStoreId(<ConfirmCannotAuthenticationModal />, { initialState });

    expect(screen.getByText('txt_cannot_authenticate')).toBeInTheDocument();
    expect(
      screen.getByText('txt_cannot_authenticate_description_confirm')
    ).toBeInTheDocument();
    expect(screen.getByText('txt_cancel')).toBeInTheDocument();
    expect(screen.getByText('txt_continue')).toBeInTheDocument();
  });

  it('click cancel button', () => {
    const toggleConfirmModalSpy = callerAuthenticationSpy('toggleConfirmModal');

    renderMockStoreId(<ConfirmCannotAuthenticationModal />, { initialState });

    const cancelButton = screen.getByText('txt_cancel');
    cancelButton.click();

    expect(toggleConfirmModalSpy).toBeCalled();
  });

  it('click continue button', () => {
    const triggerUpdateCallerAuthenticationSpy = callerAuthenticationSpy(
      'triggerUpdateCallerAuthentication'
    );

    renderMockStoreId(<ConfirmCannotAuthenticationModal />, { initialState });

    const continueButton = screen.getByText('txt_continue');
    continueButton.click();

    expect(triggerUpdateCallerAuthenticationSpy).toBeCalled();
  });
});
