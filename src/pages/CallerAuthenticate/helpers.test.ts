import { PhoneType } from './constants';
import {
  checkPostalCodeDisallowed,
  handleNumberPhoneAuthenticated,
  mapRoleType
} from './helpers';
import { CallerAuthenticationData } from './types';

describe('CallerAuthenticate > helpers', () => {
  it('mapRoleType', () => {
    expect(mapRoleType('01')).toEqual('txt_primary');
    expect(mapRoleType('02')).toEqual('txt_secondary');
    expect(mapRoleType('03')).toEqual('txt_authorized');
    expect(mapRoleType()).toEqual('');
  });
  it('checkPostalCodeDisallowed', () => {
    expect(checkPostalCodeDisallowed({})).toEqual(false);
    expect(checkPostalCodeDisallowed({ item: ['C'] }, 'S')).toEqual(false);
    expect(checkPostalCodeDisallowed({ item: ['S'] }, 'S')).toEqual(true);
  });
});

const handleNumberPhoneAuthenticatedTestCases = [
  {
    phoneSelected: '',
    expected: {
      phoneType: ''
    }
  },
  {
    phoneSelected: PhoneType.HOME_PHONE,
    expected: {
      phoneType: 'primaryCustomerHomePhoneIdentifier'
    }
  },
  {
    phoneSelected: PhoneType.MOBILE_PHONE,
    expected: {
      phoneType: 'mobilePhoneIdentifier'
    }
  },
  {
    phoneSelected: PhoneType.WORK_PHONE,
    expected: {
      phoneType: 'primaryCustomerSecondPhoneIdentifier'
    }
  }
];

describe.each(handleNumberPhoneAuthenticatedTestCases)(
  'handleNumberPhoneAuthenticated',
  ({ phoneSelected, expected }) => {
    it(`should return data['${expected.phoneType}'] if phoneSelected === ${
      phoneSelected || "''"
    }`, () => {
      const mockData = {
        primaryCustomerHomePhoneIdentifier:
          'primaryCustomerHomePhoneIdentifier',
        mobilePhoneIdentifier: 'mobilePhoneIdentifier',
        primaryCustomerSecondPhoneIdentifier:
          'primaryCustomerSecondPhoneIdentifier'
      } as CallerAuthenticationData;
      if (mockData[expected.phoneType] === undefined) {
        expect(handleNumberPhoneAuthenticated(mockData, phoneSelected)).toEqual(
          '0000000000'
        );
      } else {
        expect(handleNumberPhoneAuthenticated(mockData, phoneSelected)).toEqual(
          mockData[expected.phoneType]
        );
      }
    });
  }
);
