import React, { useEffect, useState } from 'react';

// Component
import {
  CheckBox,
  conformToMask,
  InlineMessage,
  MaskedTextBox,
  Radio
} from 'app/_libraries/_dls/components';
import { AuthenticationInfoView } from './AuthenticationInfoView';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useDispatch, useSelector } from 'react-redux';

// Const
import { I18N_AUTHENTICATION, PhoneType } from './constants';
import {
  selectorIsRenderLawMsg,
  selectorPhoneType,
  selectorSelectedCaller,
  selectQuestionConfig
} from './_redux/selectors';

// helpers
import { genAmtId } from 'app/_libraries/_dls/utils';
import { callerAuthenticationActions } from './_redux/reducers';
import isEmpty from 'lodash.isempty';
import { PHONE_MASK_INPUT_REGEX } from 'pages/AccountSearch/constants';
import { convertStringToNumberOnly } from 'app/helpers';
import { placeHolderPhone } from 'app/constants';

export interface SelectPhoneTypeProps {
  confirmAuthentication: boolean;
  setConfirmAuthentication: (data: boolean) => void;
}

const InputPhone: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [value, setValue] = useState('');
  const [message, setMessage] = useState('txt_phone_number_is_required');
  const [isError, setError] = useState(false);

  useEffect(() => {
    dispatch(callerAuthenticationActions.setNumberPhoneAuthenticated({}));
  }, [dispatch]);

  const handleChange = (e: MagicKeyValue) => {
    setValue(convertStringToNumberOnly(e.target?.value));
  };

  const handleOnblur = (e: MagicKeyValue) => {
    const numberPhone: string = convertStringToNumberOnly(e.target?.value);
    const lengthPhone = 10;
    const isError = numberPhone?.length < lengthPhone;

    setMessage(
      !numberPhone
        ? 'txt_phone_number_is_required'
        : 'txt_error_msg_phone_number'
    );
    setError(!!isError);
    dispatch(
      callerAuthenticationActions.setNumberPhoneAuthenticated({
        numberPhone
      })
    );
  };

  return (
    <div className="row">
      <div className="col-6">
        <MaskedTextBox
          required={true}
          mask={PHONE_MASK_INPUT_REGEX}
          label={t('txt_phone_number')}
          placeholder={placeHolderPhone}
          id={'caller-authentication-input-phone'}
          className="mt-16"
          onChange={handleChange}
          onBlur={handleOnblur}
          value={
            value
              ? conformToMask(value!, PHONE_MASK_INPUT_REGEX).conformedValue
              : value
          }
          error={
            isError
              ? {
                  message: t(message),
                  status: true
                }
              : undefined
          }
        />
      </div>
    </div>
  );
};

const SelectPhoneType: React.FC<SelectPhoneTypeProps> = ({
  confirmAuthentication,
  setConfirmAuthentication
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const isRenderNYStateLawMsg = useSelector(selectorIsRenderLawMsg);
  const questionConfig = useSelector(selectQuestionConfig);
  const selectedCaller = useSelector(selectorSelectedCaller);
  const selectedPhoneType = useSelector(selectorPhoneType);

  useEffect(() => {
    if (
      !selectedPhoneType ||
      selectedPhoneType === PhoneType.OTHER_PHONE ||
      isEmpty(selectedCaller)
    )
      return;

    dispatch(callerAuthenticationActions.setNumberPhoneAuthenticated({}));
  }, [dispatch, selectedCaller, selectedPhoneType]);

  const handleChangePhoneType = (e: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(callerAuthenticationActions.setPhoneType(e.target.value));
  };

  const renderAuthenticationInfo = () => (
    <>
      <AuthenticationInfoView
        data={selectedCaller || {}}
        questionConfig={questionConfig}
      />
      <CheckBox
        id="caller-authenticate__authenticate-modal__verifyAuthentication"
        className="mt-24"
        dataTestId="authenticateModal_verify-authentication"
      >
        <CheckBox.Input
          id="verifyAuthentication"
          checked={confirmAuthentication}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
            setConfirmAuthentication(e.target.checked)
          }
        />
        <CheckBox.Label
          id="caller-authenticate__authenticate-modal__verify-authentication"
          htmlFor="verifyAuthentication"
        >
          {t(I18N_AUTHENTICATION.SECURITY_VERIFICATION_REQUIRED)}
        </CheckBox.Label>
      </CheckBox>
      <div className="divider-dashed my-16"></div>
      <p data-testid={genAmtId('authenticateModal', 'select-phone-type', '')}>
        {t(I18N_AUTHENTICATION.SELECT_PHONE_TYPE)}
        <span className="ml-4 color-danger">*</span>
      </p>
      <div className="mt-16">
        <Radio
          id="caller-authenticate-work-phone-rdo"
          className="d-inline-block mr-24"
          dataTestId="authenticateModal_work-phone"
        >
          <Radio.Input
            id="caller-authenticate-work-phone-input"
            checked={selectedPhoneType === PhoneType.WORK_PHONE}
            value={PhoneType.WORK_PHONE}
            name="work-phone"
            onChange={handleChangePhoneType}
          />
          <Radio.Label id="caller-authenticate-work-phone-lbl">
            {t(I18N_AUTHENTICATION.WORK_PHONE)}
          </Radio.Label>
        </Radio>
        <Radio
          id="caller-authenticate-home-phone-rdo"
          className="d-inline-block mr-24"
          dataTestId="authenticateModal_home-phone"
        >
          <Radio.Input
            id="caller-authenticate-home-phone-input"
            name="home-phone"
            checked={selectedPhoneType === PhoneType.HOME_PHONE}
            value={PhoneType.HOME_PHONE}
            onChange={handleChangePhoneType}
          />
          <Radio.Label id="caller-authenticate-home-phone-lbl">
            {t(I18N_AUTHENTICATION.HOME_PHONE)}
          </Radio.Label>
        </Radio>
        <Radio
          id="caller-authenticate-mobile-phone-rdo"
          className="d-inline-block mr-24"
          dataTestId="authenticateModal_mobile-phone"
        >
          <Radio.Input
            id="caller-authenticate__authenticate-modal__mobile-phone-input"
            name="mobile-phone"
            checked={selectedPhoneType === PhoneType.MOBILE_PHONE}
            value={PhoneType.MOBILE_PHONE}
            onChange={handleChangePhoneType}
          />
          <Radio.Label id="caller-authenticate__authenticate-modal__mobile-phone-lbl">
            {t(I18N_AUTHENTICATION.MOBILE_PHONE)}
          </Radio.Label>
        </Radio>

        <Radio
          id="caller-authenticate__authenticate-modal__mobile-phone-rdo"
          className="d-inline-block mr-24"
          dataTestId="authenticateModal_mobile-phone"
        >
          <Radio.Input
            id="caller-authenticate-other-phone-input"
            name="other-phone"
            checked={selectedPhoneType === PhoneType.OTHER_PHONE}
            value={PhoneType.OTHER_PHONE}
            onChange={handleChangePhoneType}
          />
          <Radio.Label id="caller-authenticate-other-phone-lbl">
            {t('txt_other')}
          </Radio.Label>
        </Radio>
        {PhoneType.OTHER_PHONE === selectedPhoneType && <InputPhone />}
      </div>
    </>
  );

  const renderNYStateLaw = () => (
    <InlineMessage
      id="caller-authenticate__authenticate-modal__new-york-state-law-message"
      className="mb-0"
      variant="danger"
      dataTestId="authenticateModal_new-york-state-law-message"
    >
      {t(I18N_AUTHENTICATION.NEW_YORK_STATE_LAW_MSG)}
    </InlineMessage>
  );

  return (
    <div className="px-24 pt-24">
      {!isRenderNYStateLawMsg ? renderAuthenticationInfo() : renderNYStateLaw()}
    </div>
  );
};

export default React.memo(SelectPhoneType);
