import { CUSTOMER_ROLE_CODE, DASH } from 'app/constants';
import { formatMaskNumber } from 'app/helpers';
import { ItemTab } from 'pages/__commons/TabBar/types';
import { CallerAuthenticationData } from '../types';
import { triggerAddNewTab } from './triggerAddNewTab';

const getCustomerPrimaryName = (
  authenticationData: CallerAuthenticationData[]
) => {
  return authenticationData.find(
    item => item.customerRoleTypeCode === CUSTOMER_ROLE_CODE.primary
  )?.customerName;
};

export const handleOpenNewAccountDetailTab: AppThunk =
  isByPass => (dispatch, getState) => {
    const {
      authenticationInfo,
      selectedAccountDetailData,
      selectedCustomerName,
      selectedCaller
    } = getState().callerAuthentication;

    const {
      maskedValue,
      eValue,
      primaryName,
      memberSequenceIdentifier,
      socialSecurityIdentifier,
      queueId
    } = selectedAccountDetailData!;

    if (!eValue || !maskedValue) return;

    const mask = formatMaskNumber(maskedValue);
    const maskedValueFormat =
      mask && mask?.firstText + mask?.maskText + mask?.lastText;
    const title = `${primaryName} ${DASH} ${maskedValueFormat}`;
    const newStoreId = `${eValue}${DASH}${memberSequenceIdentifier}`;
    const customerName = isByPass
      ? getCustomerPrimaryName(authenticationInfo?.data || [])
      : selectedCustomerName;

    const newTab: ItemTab = {
      title,
      storeId: newStoreId,
      accEValue: eValue,
      tabType: 'accountDetail',
      iconName: 'account-details',
      props: {
        primaryName,
        accEValue: eValue,
        storeId: newStoreId,
        accNbr: maskedValue,
        memberSequenceIdentifier,
        socialSecurityIdentifier,
        queueId
      }
    };

    dispatch(
      triggerAddNewTab({
        storeId: newStoreId,
        tabItem: newTab,
        customerName,
        customerNameForCreateWorkflow: selectedCustomerName,
        selectedCaller: { ...selectedCaller, isByPass }
      })
    );
  };
