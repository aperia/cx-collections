import * as selector from './selectors';
import { selectorWrapper } from 'app/test-utils';
import { PhoneType } from '../constants';

const states: Partial<RootState> = {
  callerAuthentication: {
    isOpenModal: true,
    modalConfirmOpen: true,
    authenticationInfo: {
      isLoading: true,
      data: [{ customerName: 'Unit Test' }]
    },
    selectedAccountDetailData: {
      primaryName: 'Primary Name'
    },
    updateAuthentication: {
      loading: true
    },
    selectedCustomerName: 'Secondary Name',
    postalCodeDisallowBySpouse: {
      data: { code: 's' }
    },
    getConfigLoading: true,
    questionConfig: ['questionConfig']
  }
};

describe('CallerAuthenticate > selectors', () => {
  it('selectLoading', () => {
    const { data, emptyData } = selectorWrapper(states)(selector.selectLoading);

    expect(data).toEqual(true);
    expect(emptyData).toBeFalsy();
  });

  it('selectQuestionConfig', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectQuestionConfig
    );

    expect(data).toEqual(['questionConfig']);
    expect(emptyData).toEqual([]);
  });

  it('selectAuthData', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectAuthData
    );

    expect(data).toEqual([{ customerName: 'Unit Test' }]);
    expect(emptyData).toEqual([]);
  });

  it('selectOpenModal', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectOpenModal
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectConfirmModalOpen', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectConfirmModalOpen
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectAccountDetailData', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectAccountDetailData
    );

    expect(data).toEqual({ primaryName: 'Primary Name' });
    expect(emptyData).toEqual({});
  });

  it('selectUpdateAuthenticationLoading', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectUpdateAuthenticationLoading
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectAuthenConfigLoading', () => {
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectAuthenConfigLoading
    );

    expect(data).toEqual(states.callerAuthentication!.getConfigLoading);
    expect(emptyData).toEqual(false);
  });

  //new selectors

  it('selectorSelectedCaller', () => {
    const states: Partial<RootState> = {
      callerAuthentication: {
        selectedCaller: {
          test: 'test'
        }
      }
    };
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectorSelectedCaller
    );

    expect(data).toEqual(states?.callerAuthentication?.selectedCaller);
    expect(emptyData).toEqual(undefined);
  });

  it('selectorPhoneType', () => {
    const states: Partial<RootState> = {
      callerAuthentication: {
        phoneType: PhoneType.OTHER_PHONE
      }
    };
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectorPhoneType
    );

    expect(data).toEqual(states.callerAuthentication?.phoneType);
    expect(emptyData).toEqual(undefined);
  });

  it('selectorErrorPhoneType with empty phoneType', () => {
    const noPhoneStates: Partial<RootState> = {
      callerAuthentication: {
        numberPhoneAuthenticated: ''
      }
    };

    const { data, emptyData } = selectorWrapper(noPhoneStates)(
      selector.selectorErrorPhoneType
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(true);
  });

  it('selectorErrorPhoneType with phoneType is otherPhone', () => {
    const otherPhoneStates: Partial<RootState> = {
      callerAuthentication: {
        phoneType: PhoneType.OTHER_PHONE,
        numberPhoneAuthenticated: '000000000000'
      }
    };

    const { data, emptyData } = selectorWrapper(otherPhoneStates)(
      selector.selectorErrorPhoneType
    );

    expect(data).toEqual(false);
    expect(emptyData).toEqual(true);
  });

  it('selectorErrorPhoneType with phoneType is work_phone', () => {
    const notOtherPhoneStates: Partial<RootState> = {
      callerAuthentication: {
        phoneType: PhoneType.WORK_PHONE,
        numberPhoneAuthenticated: '000000000000'
      }
    };

    const { data, emptyData } = selectorWrapper(notOtherPhoneStates)(
      selector.selectorErrorPhoneType
    );

    expect(data).toEqual(false);
    expect(emptyData).toEqual(true);
  });

  it('selectorErrorPhoneType with numberPhoneAuthenticated empty', () => {
    const notOtherPhoneStates: Partial<RootState> = {
      callerAuthentication: {
        phoneType: PhoneType.OTHER_PHONE,
        numberPhoneAuthenticated: ''
      }
    };

    const { data, emptyData } = selectorWrapper(notOtherPhoneStates)(
      selector.selectorErrorPhoneType
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(true);
  });

  it('selectorIsRenderLawMsg', () => {
    const states: Partial<RootState> = {
      callerAuthentication: {
        selectedCaller: {
          fullCustomerName: 'Spouse',
          isSpouseConfirm: true
        }
      }
    };
    const { data, emptyData } = selectorWrapper(states)(
      selector.selectorIsRenderLawMsg
    );

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });
});
