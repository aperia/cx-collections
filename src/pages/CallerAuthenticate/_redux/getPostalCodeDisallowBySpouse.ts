import { SPOUSE_FULL } from './../constants';
import { CallerAuthenticationState } from './../types';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { callerAuthenticationService } from '../callerAuthenticationService';
import { GetPostalCodeListArgs, GetPostalCodeListPayload } from '../types';

export const getPostalCodeDisallowBySpouse = createAsyncThunk<
  GetPostalCodeListPayload,
  GetPostalCodeListArgs,
  ThunkAPIConfig
>('callerAuthentication/getPostalCodeList', async () => {
  const { data } = await callerAuthenticationService.getPostalCodeList();

  return { data: data[SPOUSE_FULL.toLowerCase()] };
});

export const getPostalCodeDisallowBySpouseBuilder = (
  builder: ActionReducerMapBuilder<CallerAuthenticationState>
) => {
  builder.addCase(getPostalCodeDisallowBySpouse.pending, draftState => {
    draftState.postalCodeDisallowBySpouse = {
      ...draftState?.postalCodeDisallowBySpouse,
      loading: true
    };
  });
  builder.addCase(
    getPostalCodeDisallowBySpouse.fulfilled,
    (draftState, action) => {
      const { data } = action.payload;

      draftState.postalCodeDisallowBySpouse = {
        ...draftState?.postalCodeDisallowBySpouse,
        error: false,
        loading: false,
        data
      };
    }
  );
  builder.addCase(getPostalCodeDisallowBySpouse.rejected, draftState => {
    draftState.postalCodeDisallowBySpouse = {
      ...draftState?.postalCodeDisallowBySpouse,
      loading: false,
      error: true
    };
  });
};
