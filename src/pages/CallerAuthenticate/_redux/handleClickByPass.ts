import { DASH, DEFAULT_HOME_PHONE } from 'app/constants';
import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';
import { CHRONICLE_CALL_AUTHENTICATION_KEY } from 'pages/ClientConfiguration/Memos/constants';
import { CALLER_AUTHENTICATION_ACTIONS } from 'pages/Memos/constants';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import { I18N_AUTHENTICATION } from '../constants';
import { callerAuthenticationActions } from './reducers';

export const handleClickByPass: AppThunk = () => (dispatch, getState) => {
  const { selectedCaller, selectedAccountDetailData, phoneType } =
    getState().callerAuthentication;

  const {
    clientIdentifier: clientNumber,
    systemIdentifier: system,
    principalIdentifier: prin,
    agentIdentifier: agent,
    state = '',
    postalCode = ''
  } = selectedCaller as MagicKeyValue;

  const { eValue, memberSequenceIdentifier } = selectedAccountDetailData!;
  const newStoreId = `${eValue}${DASH}${memberSequenceIdentifier}`;

  batch(() => {
    dispatch(callerAuthenticationActions.handleStartTimer({ isByPass: true }));
    dispatch(callerAuthenticationActions.toggleModalCallerAuthentication());
    dispatch(
      actionsToast.addToast({
        message: I18N_AUTHENTICATION.TOAST_AUTHENTICATION_BYPASSED,
        type: 'success'
      })
    );
    dispatch(callerAuthenticationActions.handleOpenNewAccountDetailTab(true));
    dispatch(
      memoActions.postMemo(
        CALLER_AUTHENTICATION_ACTIONS.BYPASS_AUTHENTICATION,
        {
          cspa: { clientNumber, system, prin, agent },
          storeId: eValue,
          chronicleKey: CHRONICLE_CALL_AUTHENTICATION_KEY,
          accEValue: eValue
        }
      )
    );
    dispatch(
      accountDetailActions.transferDataFromAuthentication({
        storeId: newStoreId,
        numberPhoneAuthenticated: DEFAULT_HOME_PHONE,
        sharedInfoTimer: {
          stateCode: state,
          zipCode: postalCode,
          phoneType
        }
      })
    );
    dispatch(callerAuthenticationActions.resetReducer());
  });
};
