import './reducers';
import { applyMiddleware, createStore } from '@reduxjs/toolkit';
import { rootReducer } from 'storeConfig';
import { callerAuthenticationActions } from './reducers';
import thunk from 'redux-thunk';
import * as actionAddNewTab from './triggerAddNewTab';

const initState: Partial<RootState> = {
  callerAuthentication: {
    selectedCustomerName: 'selectedCustomerName',
    selectedCaller: {},
    selectedAccountDetailData: {
      eValue: 'eValue',
      maskedValue: 'maskedValue',
      primaryName: 'primaryName',
      memberSequenceIdentifier: 'memberSequenceIdentifier',
      socialSecurityIdentifier: 'socialSecurityIdentifier',
      queueId: 'queueId'
    } as MagicKeyValue
  }
};

describe('Test handleOpenNewAccountDetailTab', () => {
  const callAction = ({ data = initState, isByPass = false }) => {
    const store = createStore(rootReducer, data, applyMiddleware(thunk));
    return callerAuthenticationActions.handleOpenNewAccountDetailTab(isByPass)(
      store.dispatch,
      store.getState,
      {}
    );
  };

  it('Should have execute handleOpenNewAccountDetailTab', async () => {
    const actionAddNewTabSpy = jest.spyOn(actionAddNewTab, 'triggerAddNewTab');
    callAction({});

    expect(actionAddNewTabSpy).toHaveBeenCalledWith({
      customerName: 'selectedCustomerName',
      customerNameForCreateWorkflow: 'selectedCustomerName',
      selectedCaller: {
        isByPass: false
      },
      storeId: 'eValue-memberSequenceIdentifier',
      tabItem: {
        accEValue: 'eValue',
        iconName: 'account-details',
        props: {
          accEValue: 'eValue',
          accNbr: 'maskedValue',
          memberSequenceIdentifier: 'memberSequenceIdentifier',
          primaryName: 'primaryName',
          socialSecurityIdentifier: 'socialSecurityIdentifier',
          storeId: 'eValue-memberSequenceIdentifier',
          queueId: 'queueId'
        },
        storeId: 'eValue-memberSequenceIdentifier',
        tabType: 'accountDetail',
        title: 'primaryName - ···········'
      }
    });

    actionAddNewTabSpy.mockReset();
    actionAddNewTabSpy.mockRestore();
  });

  it('Should haven"t execute handleOpenNewAccountDetailTab', async () => {
    const actionAddNewTabSpy = jest.spyOn(actionAddNewTab, 'triggerAddNewTab');

    callAction({
      data: {
        ...initState,
        callerAuthentication: {
          ...initState.callerAuthentication,
          selectedAccountDetailData: {
            ...initState.callerAuthentication!.selectedAccountDetailData,
            eValue: undefined
          }
        }
      }
    });

    expect(actionAddNewTabSpy).not.toHaveBeenCalled();

    actionAddNewTabSpy.mockReset();
    actionAddNewTabSpy.mockRestore();
  });

  it('Should haven"t execute handleOpenNewAccountDetailTab', async () => {
    const actionAddNewTabSpy = jest.spyOn(actionAddNewTab, 'triggerAddNewTab');

    callAction({
      data: {
        ...initState,
        callerAuthentication: {
          ...initState.callerAuthentication,
          selectedAccountDetailData: {
            ...initState.callerAuthentication!.selectedAccountDetailData,
            maskedValue: undefined
          }
        }
      }
    });

    expect(actionAddNewTabSpy).not.toHaveBeenCalled();

    actionAddNewTabSpy.mockReset();
    actionAddNewTabSpy.mockRestore();
  });
});

describe('Test handleOpenNewAccountDetailTab with isByPass', () => {
  const callAction = ({ data = initState, isByPass = false }) => {
    const store = createStore(rootReducer, data, applyMiddleware(thunk));
    return callerAuthenticationActions.handleOpenNewAccountDetailTab(isByPass)(
      store.dispatch,
      store.getState,
      {}
    );
  };

  it('Should haven execute handleOpenNewAccountDetailTab', async () => {
    const actionAddNewTabSpy = jest.spyOn(actionAddNewTab, 'triggerAddNewTab');

    callAction({
      data: {
        ...initState,
        callerAuthentication: {
          ...initState.callerAuthentication,
          authenticationInfo: {
            data: [{ customerRoleTypeCode: '01', customerName: 'customerName' }]
          }
        }
      },
      isByPass: true
    });

    expect(actionAddNewTabSpy).toHaveBeenCalledWith(
      expect.objectContaining({ customerName: 'customerName' })
    );

    actionAddNewTabSpy.mockReset();
    actionAddNewTabSpy.mockRestore();
  });

  it('Should haven execute handleOpenNewAccountDetailTab with authenticationInfo null ', async () => {
    const actionAddNewTabSpy = jest.spyOn(actionAddNewTab, 'triggerAddNewTab');

    callAction({
      data: {
        ...initState,
        callerAuthentication: {
          ...initState.callerAuthentication
        }
      },
      isByPass: true
    });

    expect(actionAddNewTabSpy).toHaveBeenCalled();

    actionAddNewTabSpy.mockReset();
    actionAddNewTabSpy.mockRestore();
  });
});
