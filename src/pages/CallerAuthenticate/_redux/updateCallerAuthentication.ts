import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import {
  CallerAuthenticationState,
  UpdateAuthenticationCaller
} from '../types';

//service
import { batch } from 'react-redux';
import { callerAuthenticationService } from '../callerAuthenticationService';
import { TOAST_MESSAGE } from '../constants';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { callerAuthenticationActions } from './reducers';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { CALLER_AUTHENTICATION_ACTIONS } from 'pages/Memos/constants';
import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';
import { DASH, DEFAULT_HOME_PHONE } from 'app/constants';
import { CHRONICLE_CALL_AUTHENTICATION_KEY } from 'pages/ClientConfiguration/Memos/constants';

export const triggerUpdateCallerAuthentication = createAsyncThunk<
  void,
  UpdateAuthenticationCaller,
  ThunkAPIConfig
>(
  'callerAuthentication/triggerUpdateCallerAuthentication',
  async (args, thunkAPI) => {
    const { dispatch, getState } = thunkAPI;
    const {
      postData: { authenticationType }
    } = args;

    const { callerAuthentication } = getState();

    const {
      selectedCaller: accountDetailData,
      selectedAccountDetailData,
      selectedCustomerName: cardholderContactedName,
      phoneType
    } = callerAuthentication;

    const { eValue = '', memberSequenceIdentifier } =
      selectedAccountDetailData || {};

    const accountId = eValue;

    const {
      clientIdentifier: clientNumber,
      systemIdentifier: system,
      principalIdentifier: prin,
      agentIdentifier: agent,
      state = '',
      postalCode = ''
    } = accountDetailData!;

    const cspa = { clientNumber, system, prin, agent };

    await callerAuthenticationService.updateCallerAuthentication();

    const cardholderName = cardholderContactedName?.split('•')[0];

    batch(() => {
      dispatch(
        actionsToast.addToast({
          message: TOAST_MESSAGE[authenticationType].SUCCESS,
          type: 'success'
        })
      );

      if (authenticationType === 'cannotAuthentication') {
        dispatch(callerAuthenticationActions.toggleConfirmModal());
        dispatch(
          memoActions.postMemo(CALLER_AUTHENTICATION_ACTIONS.FAILED_TO_VERIFY, {
            cspa,
            storeId: accountId,
            accEValue: accountId,
            chronicleKey: CHRONICLE_CALL_AUTHENTICATION_KEY,
            cardholderContactedName: cardholderName
          })
        );
      } else {
        dispatch(callerAuthenticationActions.toggleModalCallerAuthentication());

        dispatch(callerAuthenticationActions.handleOpenNewAccountDetailTab());

        dispatch(
          memoActions.postMemo(CALLER_AUTHENTICATION_ACTIONS.VERIFY_SUCCESS, {
            cspa,
            storeId: accountId,
            accEValue: accountId,
            chronicleKey: CHRONICLE_CALL_AUTHENTICATION_KEY,
            cardholderContactedName: cardholderName
          })
        );
      }
      // transfer data of AccountDetail to accountDetail Tab

      const rawAccountDetailData =
        callerAuthentication.authenticationInfo?.rawAccountDetailData;

      const numberPhoneAuthenticated =
        callerAuthentication.numberPhoneAuthenticated ?? DEFAULT_HOME_PHONE;

      if (authenticationType === 'cannotAuthentication') {
        return;
      }

      const nextStoreId = `${eValue}${DASH}${memberSequenceIdentifier}`;

      const accountDetailBySeqNumber =
        rawAccountDetailData?.customerInquiry?.find(
          item => item?.memberSequenceIdentifier === memberSequenceIdentifier
        ) || [];

      const infoTimer = {
        stateCode: state?.trim(),
        zipCode: postalCode?.trim(),
        phoneType
      };

      dispatch(
        accountDetailActions.transferDataFromAuthentication({
          storeId: nextStoreId,
          rawAccountDetailData,
          accountDetailBySeqNumber,
          sharedInfoTimer: infoTimer,
          numberPhoneAuthenticated
        })
      );

      authenticationType === 'authentication' &&
        dispatch(callerAuthenticationActions.resetReducer());
    });
  }
);

export const updateCallerAuthenticationBuilder = (
  builder: ActionReducerMapBuilder<CallerAuthenticationState>
) => {
  const { pending, fulfilled, rejected } = triggerUpdateCallerAuthentication;

  builder
    .addCase(pending, (draftState, action) => {
      draftState.updateAuthentication = {
        ...draftState.updateAuthentication,
        loading: true
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      draftState.updateAuthentication = {
        ...draftState.updateAuthentication,
        loading: false,
        error: false
      };
    })
    .addCase(rejected, (draftState, action) => {
      draftState.updateAuthentication = {
        ...draftState.updateAuthentication,
        loading: false,
        error: true
      };
    });
};
