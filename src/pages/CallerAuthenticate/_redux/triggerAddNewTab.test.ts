import { createStore } from 'redux';

import { rootReducer } from 'storeConfig';
import { triggerAddNewTab, TriggerAddNewTabArgs } from './triggerAddNewTab';
import { mockActionCreator, storeId } from 'app/test-utils';
import { tabActions } from 'pages/__commons/TabBar/_redux';
import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';

describe('Test async thunk triggerAddNewTab', () => {
  const spyTabActions = mockActionCreator(tabActions);
  const spyAccountDetailActions = mockActionCreator(accountDetailActions);

  const mockArgs: TriggerAddNewTabArgs = {
    storeId,
    tabItem: {
      storeId,
      title: 'title',
      tabType: 'home'
    },
    customerName: 'customerName',
    customerNameForCreateWorkflow: 'customerNameForCreateWorkflow',
    selectedCaller: {}
  };

  it('should fulfill with existed storeId', async () => {
    const addTab = spyTabActions('addTab');
    const setRefreshAcc = spyAccountDetailActions('setRefreshAcc');
    const saveSelectedCaller = spyAccountDetailActions('saveSelectedCaller');

    const store = createStore(rootReducer, {
      tab: {
        tabs: [
          {
            storeId
          }
        ]
      }
    });

    await triggerAddNewTab(mockArgs)(store.dispatch, store.getState, {});

    expect(addTab).toHaveBeenCalledWith(mockArgs.tabItem);
    expect(setRefreshAcc).toHaveBeenCalledWith({ storeId });
    expect(saveSelectedCaller).toHaveBeenCalledWith({
      storeId,
      data: mockArgs.selectedCaller,
      authData: {
        customerName: mockArgs.customerName,
        customerNameForCreateWorkflow: mockArgs.customerNameForCreateWorkflow
      }
    });
  });

  it('should fulfill with storeId not existed', async () => {
    const addTab = spyTabActions('addTab');
    const saveSelectedCaller = spyAccountDetailActions('saveSelectedCaller');

    const store = createStore(rootReducer, {
      tab: {
        tabs: []
      }
    });

    await triggerAddNewTab(mockArgs)(store.dispatch, store.getState, {});

    expect(addTab).toHaveBeenCalledWith(mockArgs.tabItem);
    expect(saveSelectedCaller).toHaveBeenCalledWith({
      storeId,
      data: mockArgs.selectedCaller,
      authData: {
        customerName: mockArgs.customerName,
        customerNameForCreateWorkflow: mockArgs.customerNameForCreateWorkflow
      }
    });
  });
});
