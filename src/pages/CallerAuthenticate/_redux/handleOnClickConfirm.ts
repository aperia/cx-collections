import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';
import { callerAuthenticationActions } from './reducers';

export const handleOnClickConfirm: AppThunk = () => (dispatch, getState) => {
  const { selectedAccountDetailData } = getState().callerAuthentication;
  dispatch(
    callerAuthenticationActions.triggerActionEntryCreateWorkFlow({
      postData: {
        accountId: selectedAccountDetailData!.eValue!,
        cardholderContactedName: selectedAccountDetailData!.primaryName!,
        actionCode: CALL_RESULT_CODE.REGULATORY_SPOUSE_CONTACT
      }
    })
  );
};
