import { createSelector } from '@reduxjs/toolkit';
import { EMPTY_ARRAY } from 'app/constants';
import { PhoneType, SPOUSE_TEXTS } from '../constants';
import { CallerAuthenticationData } from '../types';

const getCallerAuthentication = (state: RootState) => {
  return state.callerAuthentication;
};

const getQuestionConfig = (state: RootState) => {
  return state.callerAuthentication.questionConfig;
};

const getCallerAuthenticationInfo = (state: RootState) => {
  return state.callerAuthentication?.authenticationInfo;
};

// get caller authentication info

export const selectorSelectedCaller = createSelector(
  (state: RootState) => state.callerAuthentication.selectedCaller,
  (data: CallerAuthenticationData | undefined) => data
);

export const selectorPhoneType = createSelector(
  (state: RootState) => state.callerAuthentication.phoneType,
  (data: string | undefined) => data
);

export const selectorErrorPhoneType = createSelector(
  (state: RootState) => state.callerAuthentication.phoneType,
  (state: RootState) => state.callerAuthentication.numberPhoneAuthenticated,
  (phoneType: string | undefined, numberPhone: string | undefined) => {
    if (!phoneType) return true;
    if (phoneType !== PhoneType.OTHER_PHONE) return false;
    if (!numberPhone) return true;
    const lengthNumberPhone = 10;
    const isValidPhone = numberPhone.length < lengthNumberPhone;
    return !!isValidPhone;
  }
);

export const selectorIsRenderLawMsg = createSelector(
  (state: RootState) => state.callerAuthentication.selectedCaller,
  (selectedCaller: CallerAuthenticationData | undefined) => {
    const isSpouseItem = SPOUSE_TEXTS.includes(
      selectedCaller?.fullCustomerName?.toLowerCase() ?? ''
    );

    // isSpouseConfirm just appear when selecting spouse contact item
    const isPostalCodeDisallowed = selectedCaller?.isSpouseConfirm;

    if (isSpouseItem && isPostalCodeDisallowed) return true;
    return false;
  }
);

export const selectLoading = createSelector(
  getCallerAuthenticationInfo,
  authentication => authentication?.isLoading || false
);

export const selectQuestionConfig = createSelector(
  getQuestionConfig,
  config => config || EMPTY_ARRAY
);

export const selectAuthData = createSelector(
  getCallerAuthenticationInfo,
  authentication => authentication?.data || []
);

// modal authentication
export const selectOpenModal = createSelector(
  getCallerAuthentication,
  authentication => authentication?.isOpenModal || false
);

export const selectConfirmModalOpen = createSelector(
  getCallerAuthentication,
  authentication => authentication?.modalConfirmOpen || false
);

export const selectAccountDetailData = createSelector(
  getCallerAuthentication,
  authentication => authentication?.selectedAccountDetailData || {}
);

// update authentication
export const selectUpdateAuthenticationLoading = createSelector(
  getCallerAuthentication,
  authentication => authentication?.updateAuthentication?.loading || false
);

export const selectAuthenConfigLoading = createSelector(
  (state: RootState) => state.callerAuthentication?.getConfigLoading ?? false,
  data => data
);
