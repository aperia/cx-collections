import './reducers';
import { createStore } from '@reduxjs/toolkit';
import { mockActionCreator } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import { callerAuthenticationActions } from './reducers';
import { CALL_RESULT_CODE } from 'pages/CollectionForm/constants';

const initState: Partial<RootState> = {
  callerAuthentication: {
    selectedAccountDetailData: {
      eValue: 'eValue',
      primaryName: 'cardholderContactedName'
    } as MagicKeyValue
  }
};

const callerAuthenticationActionSpy = mockActionCreator(
  callerAuthenticationActions
);

describe('Test handleOnClickConfirm', () => {
  const callAction = (data = initState) => {
    const store = createStore(rootReducer, data);
    return callerAuthenticationActions.handleOnClickConfirm()(
      store.dispatch,
      store.getState,
      {}
    );
  };

  it('Should have execute handleOnClickConfirm', async () => {
    const trigger = callerAuthenticationActionSpy(
      'triggerActionEntryCreateWorkFlow'
    );

    callAction();

    expect(trigger).toHaveBeenCalledWith({
      postData: {
        accountId: 'eValue',
        cardholderContactedName: 'cardholderContactedName',
        actionCode: CALL_RESULT_CODE.REGULATORY_SPOUSE_CONTACT
      }
    });
  });
});
