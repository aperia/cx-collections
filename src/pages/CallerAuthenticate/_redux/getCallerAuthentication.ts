import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';

//types
import {
  CallerAuthenticationData,
  CallerAuthenticationPayLoad,
  CallerAuthenticationPostData,
  CallerAuthenticationState,
  GetAddressRequest,
  AddressInfo,
  GetTypeAddress
} from '../types';

//service
import { callerAuthenticationService } from '../callerAuthenticationService';

//constants
import {
  ADDRESS_MAP,
  ADDRESS_REQUEST_SELECT_FIELDS,
  AUTHENTICATION_SELECTED_FIELDS,
  SPOUSE_FULL,
  STATE
} from '../constants';
import { SELECTED_FIELDS_ACCOUNT_DETAIL_REQUEST } from 'pages/AccountDetails/constants';
import {
  CUSTOMER_ROLE_CODE,
  DEFAULT_CALLER,
  EMPTY_STRING,
  SPOUSE
} from 'app/constants';

//helpers
import { checkPostalCodeDisallowed, mapRoleType } from '../helpers';
import {
  formatPostalCodeUSA,
  getMaskedValue,
  joinAddress,
  trimData
} from 'app/helpers';
import { getDefaultTokenBasedOnHostName } from 'app/helpers/setupInterceptor';

export const getCallerAuthentication = createAsyncThunk<
  CallerAuthenticationPayLoad,
  undefined,
  ThunkAPIConfig
>('callerAuthentication/getCallerAuthentication', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;
  const accountId =
    getState().callerAuthentication.selectedAccountDetailData?.eValue || '';

  const defaultTokenByRole = dispatch(getDefaultTokenBasedOnHostName());

  // get renewToken to get Account Detail data
  const { commonConfig } = window.appConfig || {};
  const { data } = await callerAuthenticationService.triggerRenewToken({
    common: { accountId, applicationId: commonConfig?.app || EMPTY_STRING },
    jwt: defaultTokenByRole
  });

  const authRequest: CallerAuthenticationPostData = {
    common: { accountId },
    selectFields: [
      ...AUTHENTICATION_SELECTED_FIELDS,
      ...SELECTED_FIELDS_ACCOUNT_DETAIL_REQUEST
    ]
  };

  const addressRequest: GetAddressRequest = {
    common: { accountId },
    selectFields: ADDRESS_REQUEST_SELECT_FIELDS
  };

  const checkTypeAddressRequest: GetTypeAddress = {
    common: { accountId },
    selectFields: ['expansionAddressCode']
  };

  const result = await Promise.allSettled([
    callerAuthenticationService.getCallerAuthentication(authRequest, data?.jwt),
    callerAuthenticationService.getAddressInfo(addressRequest, data?.jwt),
    callerAuthenticationService.checkTypeofAddress(
      checkTypeAddressRequest,
      data?.jwt
    ),
    callerAuthenticationService.getPostalCodeList()
  ]);

  const [
    authenticateResponse,
    addressResponse,
    typeAddressResponse,
    postalCodeDisallowBySpouse
  ] = result;

  let authData;
  let addressData;
  let typeAddressCode;
  let postalCodeBySpouse: MagicKeyValue;
  let selectPrimaryAddress: AddressInfo | undefined;
  let selectSpouseAddress: AddressInfo[] | undefined;

  if (authenticateResponse.status === 'fulfilled') {
    authData = authenticateResponse.value.data;
  }

  if (addressResponse.status === 'fulfilled') {
    addressData = addressResponse.value.data;
  }

  if (typeAddressResponse.status === 'fulfilled') {
    typeAddressCode = typeAddressResponse.value.data;
  }

  // get postal code by spouse contact
  if (postalCodeDisallowBySpouse.status === 'fulfilled') {
    postalCodeBySpouse =
      postalCodeDisallowBySpouse.value.data[SPOUSE_FULL.toLowerCase()];
  }

  const primaryAddress = (addressData?.primary || []) as AddressInfo[];

  const secondaryAddress = ((addressData?.secondary &&
    addressData?.secondary[0]) ||
    null) as AddressInfo;
  const authorizedAddressArray = (addressData?.additionalUsers ||
    []) as AddressInfo[];

  const concatPrimaryAndSecondaryAddress = primaryAddress.concat(
    addressData?.secondary || ([] as AddressInfo[])
  );

  // check the current date is effective date
  const checkEffectiveDate = (fromDate: Date, toDate: Date) => {
    const currentDate = new Date().setHours(0, 0, 0, 0);
    const convertedFromDate = new Date(fromDate).setHours(0, 0, 0, 0);
    const convertedToDate = new Date(toDate).setHours(0, 0, 0, 0);

    const isCheckedFormDate = convertedFromDate <= currentDate;
    const isCheckToDate = currentDate <= convertedToDate;

    return isCheckedFormDate && isCheckToDate;
  };

  const checkTypeAndCategoryCode = (
    typeCode: string,
    categoryCode: string,
    checkLetter?: boolean
  ) => {
    const isPermanent = categoryCode.trim() === ADDRESS_MAP.PERMANENT;
    const isBilling = typeCode.trim() === ADDRESS_MAP.BILLING;
    const isLetter = typeCode.trim() === ADDRESS_MAP.LETTER_ADDRESS;

    if (checkLetter) {
      return (isBilling || isLetter) && isPermanent;
    }

    return isBilling && isPermanent;
  };

  if (typeAddressCode?.expansionAddressCode === ADDRESS_MAP.EXPAND_ADDRESS) {
    selectPrimaryAddress = primaryAddress.find(
      item =>
        checkTypeAndCategoryCode(
          item?.addressTypeCode || '',
          item?.addressCategoryCode || ''
        ) &&
        checkEffectiveDate(
          item?.effectiveFromDate as Date,
          item?.effectiveToDate as Date
        )
    );
  } else {
    selectPrimaryAddress = primaryAddress.find(item =>
      checkTypeAndCategoryCode(
        item?.addressTypeCode || '',
        item?.addressCategoryCode || ''
      )
    );
  }

  // get the Primary and Secondary of the account Permanent Billing or Permanent Letter Address
  if (typeAddressCode?.expansionAddressCode === ADDRESS_MAP.EXPAND_ADDRESS) {
    selectSpouseAddress = concatPrimaryAndSecondaryAddress?.filter(
      item =>
        checkTypeAndCategoryCode(
          item?.addressTypeCode || '',
          item?.addressCategoryCode || '',
          true
        ) &&
        checkEffectiveDate(
          item?.effectiveFromDate as Date,
          item?.effectiveToDate as Date
        )
    );
  } else {
    selectSpouseAddress = concatPrimaryAndSecondaryAddress?.filter(item =>
      checkTypeAndCategoryCode(
        item?.addressTypeCode || '',
        item?.addressCategoryCode || '',
        true
      )
    );
  }

  const formatAddress = (selectAddress?: AddressInfo) => {
    if (!selectAddress) return {};
    return {
      addressLineOne: selectAddress?.addressContinuationLine1Text,
      addressLineTwo: selectAddress?.addressContinuationLine2Text,
      addressLineThree: selectAddress?.addressContinuationLine3Text,
      addressLineFour: selectAddress?.addressContinuationLine4Text,
      addressLineFive: joinAddress(
        selectAddress?.cityName?.trim(),
        selectAddress?.addressSubdivisionOneText?.trim(),
        formatPostalCodeUSA(
          selectAddress?.countryCode,
          selectAddress?.postalCode
        )
      )
    };
  };

  const getStatePrimary = (
    customerRoleTypeValue: string,
    memberSequenceId?: string
  ) => {
    if (customerRoleTypeValue?.toLowerCase() === 'txt_primary')
      return selectPrimaryAddress?.addressSubdivisionOneText?.trim();
    if (customerRoleTypeValue?.toLowerCase() === 'txt_secondary')
      return secondaryAddress?.addressSubdivisionOneText?.trim();
    if (customerRoleTypeValue?.toLowerCase() === 'txt_authorized') {
      const authorizedAddress = authorizedAddressArray.find(
        address => address?.memberSequenceIdentifier === memberSequenceId
      );
      return authorizedAddress?.addressSubdivisionOneText?.trim();
    }
    return EMPTY_STRING;
  };

  const getAddressBaseOnRoleType = (
    customerRoleTypeValue: string,
    memberSequenceId?: string
  ) => {
    if (customerRoleTypeValue === 'txt_primary')
      return formatAddress(selectPrimaryAddress);
    if (customerRoleTypeValue === 'txt_secondary')
      return formatAddress(secondaryAddress);
    if (customerRoleTypeValue === 'txt_authorized') {
      const authorizedAddress = authorizedAddressArray.find(
        address => address?.memberSequenceIdentifier === memberSequenceId
      );
      return formatAddress(authorizedAddress);
    }
    return [];
  };

  const getPostalCodeByRoleType = (
    customerRoleTypeValue: string,
    memberSequenceId?: string
  ) => {
    if (customerRoleTypeValue?.toLowerCase() === 'txt_primary')
      return formatPostalCodeUSA(
        selectPrimaryAddress?.countryCode,
        selectPrimaryAddress?.postalCode
      );
    if (customerRoleTypeValue?.toLowerCase() === 'txt_secondary')
      return formatPostalCodeUSA(
        secondaryAddress?.countryCode,
        secondaryAddress?.postalCode
      );
    if (customerRoleTypeValue?.toLowerCase() === 'txt_authorized') {
      const authorizedAddress = authorizedAddressArray.find(
        address => address?.memberSequenceIdentifier === memberSequenceId
      );
      return formatPostalCodeUSA(
        authorizedAddress?.countryCode,
        authorizedAddress?.postalCode
      );
    }
    return EMPTY_STRING;
  };

  const checkPostalCodeBySpouse = (postalCodeData: MagicKeyValue) => {
    if (!selectSpouseAddress?.length) return false;

    for (const item of selectSpouseAddress) {
      if (
        item.addressSubdivisionOneText?.trim() === STATE.NY &&
        checkPostalCodeDisallowed(postalCodeData, item.postalCode?.trim())
      ) {
        return true;
      }
    }

    return false;
  };

  const customerInquiry = (authData?.customerInquiry ||
    []) as CallerAuthenticationData[];

  const trimmedData = trimData(customerInquiry);

  const formattedCallerAuthenticationData = trimmedData.map(
    (item: CallerAuthenticationData) => {
      const socialSecurityIdentifier = getMaskedValue(
        item?.socialSecurityIdentifier
      ).split('-')[2];
      const customerRoleTypeValue = mapRoleType(item?.customerRoleTypeCode);
      
      const memberSequenceId = item?.memberSequenceIdentifier;

      return {
        ...item,
        presentationInstrumentIdentifier: getMaskedValue(
          item?.presentationInstrumentIdentifier
        ),
        birthDate: getMaskedValue(item?.birthDate),
        socialSecurityIdentifier: socialSecurityIdentifier,
        customerRoleTypeValue,
        fullCustomerName: `${
          item?.customerName || ''
        } • ${customerRoleTypeValue}`,
        address: getAddressBaseOnRoleType(
          customerRoleTypeValue,
          memberSequenceId
        ),
        state: getStatePrimary(customerRoleTypeValue, memberSequenceId),
        postalCode: getPostalCodeByRoleType(
          customerRoleTypeValue,
          memberSequenceId
        )
      };
    }
  );

  const primaryData = formattedCallerAuthenticationData.find(
    (item: CallerAuthenticationData) =>
      item.customerRoleTypeCode === CUSTOMER_ROLE_CODE.primary
  );

  const otherCallerAuthenticationData = DEFAULT_CALLER.map(item => {
    if (item.value === SPOUSE) {
      return {
        ...primaryData,
        fullCustomerName: item.description,
        isSpouseConfirm: checkPostalCodeBySpouse(postalCodeBySpouse),
        customerRoleTypeCode: item.value
      };
    }
    return {
      ...primaryData,
      fullCustomerName: item.description,
      customerRoleTypeCode: item.value
    };
  });

  const fullCallerAuthenticationData = formattedCallerAuthenticationData.concat(
    otherCallerAuthenticationData
  );

  return {
    data: fullCallerAuthenticationData,
    rawAccountDetailData: authData
  };
});

export const getCallerAuthenticationBuilder = (
  builder: ActionReducerMapBuilder<CallerAuthenticationState>
) => {
  builder
    .addCase(getCallerAuthentication.pending, (draftState, action) => {
      // clear old data cached
      draftState.authenticationInfo = {
        data: [],
        isLoading: true
      };
    })
    .addCase(getCallerAuthentication.fulfilled, (draftState, action) => {
      const { data, rawAccountDetailData } = action.payload;
      const [defaultCallResult] = data;
      return {
        ...draftState,
        selectedCustomerName: defaultCallResult?.customerName,
        authenticationInfo: {
          ...draftState.authenticationInfo,
          data,
          isLoading: false,
          error: false,
          rawAccountDetailData
        },
        selectedCaller: defaultCallResult
      };
    })
    .addCase(getCallerAuthentication.rejected, (draftState, action) => {
      draftState.authenticationInfo = {
        ...draftState.authenticationInfo,
        isLoading: false,
        error: true
      };
    });
};
