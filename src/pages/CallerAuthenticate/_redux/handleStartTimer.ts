import { timerWorkingActions } from 'pages/TimerWorking/_redux/reducers';
import { batch } from 'react-redux';

export const handleStartTimer: AppThunk =
  ({ isByPass } = { isByPass: false }) =>
  (dispatch, getState) => {
    const { callerAuthentication } = getState();

    const eValue = callerAuthentication?.selectedAccountDetailData?.eValue;

    const contactPerson =
      callerAuthentication.selectedCaller!.customerRoleTypeValue!;

    batch(() => {
      dispatch(timerWorkingActions.startAuthenticate(eValue!));
      dispatch(
        timerWorkingActions.setContactPersonName({
          eValue: eValue!,
          contactPerson: isByPass ? '' : contactPerson
        })
      );
    });
  };
