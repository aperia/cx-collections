import { createStore, Store } from '@reduxjs/toolkit';
import { mockActionCreator, responseDefault } from 'app/test-utils';

import { rootReducer } from 'storeConfig';

import { triggerActionEntryCreateWorkFlow } from './triggerActionEntryCreateWorkFlow';
import { reducer, callerAuthenticationActions } from './reducers';
import { collectionFormService } from 'pages/CollectionForm/collectionFormServices';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

const spyApiErrorNotificationAction = mockActionCreator(
  apiErrorNotificationAction
);
const spyCallerAuthentication = mockActionCreator(callerAuthenticationActions);

describe('CallerAuthenticate > redux triggerActionEntryCreateWorkFlow', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {
      callerAuthentication: {}
    });
  });

  it('pending', () => {
    const nextState = reducer(
      {},
      triggerActionEntryCreateWorkFlow.pending(
        'triggerActionEntryCreateWorkFlow',
        {} as any
      )
    );
    expect(nextState?.authenticationInfo?.isLoading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      {},
      triggerActionEntryCreateWorkFlow.fulfilled(
        undefined,
        'triggerActionEntryCreateWorkFlow',
        {} as any
      )
    );
    expect(nextState?.authenticationInfo?.isLoading).toEqual(false);
    expect(nextState?.authenticationInfo?.error).toEqual(false);
  });
  it('rejected', () => {
    const nextState = reducer(
      {},
      triggerActionEntryCreateWorkFlow.rejected(
        null,
        'triggerActionEntryCreateWorkFlow',
        {} as any
      )
    );
    expect(nextState?.authenticationInfo?.isLoading).toEqual(false);
    expect(nextState?.authenticationInfo?.error).toEqual(true);
  });

  it('should trigger updateActionEntryWorkFlow with status error', async () => {
    const mockClearApiErrorData =
      spyApiErrorNotificationAction('clearApiErrorData');
    const mockToggleModalCallerAuthentication = spyCallerAuthentication(
      'toggleModalCallerAuthentication'
    );

    jest
      .spyOn(collectionFormService, 'updateActionEntryWorkFlow')
      .mockResolvedValue({
        ...responseDefault,
        data: { status: 'error' }
      });

    const response = await triggerActionEntryCreateWorkFlow({
      postData: {} as any
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual({
      config: {},
      data: { status: 'error' },
      headers: {},
      status: 200,
      statusText: ''
    });
    expect(mockClearApiErrorData).not.toBeCalled();
    expect(mockToggleModalCallerAuthentication).not.toBeCalled();
  });

  it('should trigger updateActionEntryWorkFlow with status success', async () => {
    const mockClearApiErrorData =
      spyApiErrorNotificationAction('clearApiErrorData');
    const mockToggleModalCallerAuthentication = spyCallerAuthentication(
      'toggleModalCallerAuthentication'
    );

    jest
      .spyOn(collectionFormService, 'updateActionEntryWorkFlow')
      .mockResolvedValue({
        ...responseDefault,
        data: { status: 'success' }
      });

    const response = await triggerActionEntryCreateWorkFlow({
      postData: {} as any
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual(undefined);
    expect(mockClearApiErrorData).toBeCalledWith({
      forSection: 'inModalBody',
      storeId: API_ERROR_STORE_ID.AUTHENTICATION_MODAL
    });
    expect(mockToggleModalCallerAuthentication).toBeCalled();
  });

  it('should throw error on call updateActionEntryWorkFlow fail', async () => {
    const error = { error: 'error' };
    jest
      .spyOn(collectionFormService, 'updateActionEntryWorkFlow')
      .mockRejectedValue(error);

    const response = await triggerActionEntryCreateWorkFlow({
      postData: {} as any
    })(store.dispatch, store.getState, {});

    expect(response.payload).toEqual(error);
  });
});
