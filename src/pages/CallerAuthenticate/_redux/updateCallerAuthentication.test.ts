import { callerAuthenticationActions } from './reducers';
import { createStore, Store } from '@reduxjs/toolkit';
import { triggerUpdateCallerAuthentication } from './updateCallerAuthentication';

import { callerAuthenticationService } from '../callerAuthenticationService';
import { mockActionCreator, responseDefault, storeId } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { TOAST_MESSAGE } from '../constants';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { CHRONICLE_CALL_AUTHENTICATION_KEY } from 'pages/ClientConfiguration/Memos/constants';
import { CALLER_AUTHENTICATION_ACTIONS } from 'pages/Memos/constants';
import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';

const initialState: Partial<RootState> = {
  callerAuthentication: {
    selectedCustomerName: 'selectedCustomerName',
    updateAuthentication: {
      loading: false,
      error: false
    },
    selectedAccountDetailData: {
      eValue: 'eValue',
      memberSequenceIdentifier: 'memberSequenceIdentifier'
    },
    numberPhoneAuthenticated: 'numberPhoneAuthenticated',
    selectedCaller: {
      clientIdentifier: 'clientIdentifier',
      systemIdentifier: 'system',
      principalIdentifier: 'prin',
      agentIdentifier: 'agent',
      state: 'state',
      postalCode: 'postalCode'
    },
    authenticationInfo: {
      rawAccountDetailData: {
        customerInquiry: [{ memberSequenceIdentifier: '01' }]
      }
    }
  }
};

const authenticationType = 'authentication';

const toastSpy = mockActionCreator(actionsToast);
const callerAuthenticationSpy = mockActionCreator(callerAuthenticationActions);
const memoActionSpy = mockActionCreator(memoActions);
const accountDetailActionSpy = mockActionCreator(accountDetailActions);

describe('Test triggerUpdateCallerAuthentication', () => {
  let store: Store<RootState>;
  beforeEach(() => {
    store = createStore(rootReducer, initialState);
  });
  it('Should have success with authentication', async () => {
    jest
      .spyOn(callerAuthenticationService, 'updateCallerAuthentication')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const mockAddToast = toastSpy('addToast');
    const mockToggleModalCallerAuthentication = callerAuthenticationSpy(
      'toggleModalCallerAuthentication'
    );
    const resetReducer = callerAuthenticationSpy('resetReducer');
    const handleOpenNewAccountDetailTab = callerAuthenticationSpy(
      'handleOpenNewAccountDetailTab'
    );

    const postMemo = memoActionSpy('postMemo');

    await triggerUpdateCallerAuthentication({
      postData: { authenticationType },
      storeId: storeId
    })(store.dispatch, store.getState, {});

    expect(mockAddToast).toBeCalledWith({
      message: TOAST_MESSAGE[authenticationType].SUCCESS,
      type: 'success'
    });

    expect(postMemo).toBeCalledWith(
      CALLER_AUTHENTICATION_ACTIONS.VERIFY_SUCCESS,
      {
        cspa: {
          agent: 'agent',
          clientNumber: 'clientIdentifier',
          prin: 'prin',
          system: 'system'
        },
        storeId: 'eValue',
        accEValue: 'eValue',
        chronicleKey: CHRONICLE_CALL_AUTHENTICATION_KEY,
        cardholderContactedName: 'selectedCustomerName'
      }
    );

    expect(handleOpenNewAccountDetailTab).toBeCalled();

    expect(mockToggleModalCallerAuthentication).toBeCalled();

    expect(resetReducer).toBeCalled();
  });

  it('Should have can not Authentication', async () => {
    jest
      .spyOn(callerAuthenticationService, 'updateCallerAuthentication')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const mockAddToast = toastSpy('addToast');
    const mockToggleConfirmModal =
      callerAuthenticationSpy('toggleConfirmModal');

    await triggerUpdateCallerAuthentication({
      postData: {
        authenticationType: 'cannotAuthentication'
      },
      storeId: storeId
    })(store.dispatch, store.getState, {});

    expect(mockAddToast).toBeCalledWith({
      message: TOAST_MESSAGE['cannotAuthentication'].SUCCESS,
      type: 'success'
    });

    expect(mockToggleConfirmModal).toBeCalled();
  });

  it('pending', () => {
    const pendingAction = triggerUpdateCallerAuthentication.pending(
      triggerUpdateCallerAuthentication.pending.type,
      { postData: { authenticationType }, storeId: storeId }
    );
    const actual = rootReducer(
      store.getState(),
      pendingAction
    ).callerAuthentication;

    expect(actual.updateAuthentication?.loading).toEqual(true);
  });

  it('fulfilled', () => {
    const pendingAction = triggerUpdateCallerAuthentication.fulfilled(
      undefined,
      triggerUpdateCallerAuthentication.fulfilled.type,
      { postData: { authenticationType }, storeId: storeId }
    );
    const actual = rootReducer(
      store.getState(),
      pendingAction
    ).callerAuthentication;

    expect(actual.updateAuthentication?.loading).toEqual(false);
    expect(actual.updateAuthentication?.error).toEqual(false);
  });
});

describe('Test triggerUpdateCallerAuthentication with empty data', () => {
  let store: Store<RootState>;
  beforeEach(() => {
    store = createStore(rootReducer, {
      ...initialState,
      callerAuthentication: {
        selectedCaller: {
          clientIdentifier: 'clientNumber',
          systemIdentifier: 'system',
          principalIdentifier: 'prin',
          agentIdentifier: 'agent'
        },
        authenticationInfo: {
          rawAccountDetailData: {
            customerInquiry: [{ memberSequenceIdentifier: '01' }]
          }
        }
      }
    });
  });
  it('Should have success with authentication', async () => {
    jest.useFakeTimers();
    jest
      .spyOn(callerAuthenticationService, 'updateCallerAuthentication')
      .mockResolvedValue({ ...responseDefault, data: {} });

    const mockAddToast = toastSpy('addToast');

    // const transferDataFromAuthentication = accountDetailActionSpy(
    //   'transferDataFromAuthentication'
    // );

    const postMemo = memoActionSpy('postMemo');

    await triggerUpdateCallerAuthentication({
      postData: { authenticationType: 'cannotAuthentication' },
      storeId: storeId
    })(store.dispatch, store.getState, {});

    jest.runAllTimers();

    expect(mockAddToast).toBeCalled();

    expect(postMemo).toBeCalled();

    // expect(transferDataFromAuthentication).toBeCalled();
  });
});
