import './reducers';
import { createStore } from '@reduxjs/toolkit';
import { mockActionCreator } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import { callerAuthenticationActions } from './reducers';
import { timerWorkingActions } from 'pages/TimerWorking/_redux/reducers';

const initState: Partial<RootState> = {
  callerAuthentication: {
    selectedCaller: { customerRoleTypeValue: 'customerRoleTypeValue' },
    selectedAccountDetailData: {
      eValue: 'eValue',
      primaryName: 'cardholderContactedName'
    } as MagicKeyValue
  }
};

const timerWorkingActionSpy = mockActionCreator(timerWorkingActions);

describe('Test handleOnClickConfirm', () => {
  const callAction = (data = initState) => {
    const store = createStore(rootReducer, data);
    return callerAuthenticationActions.handleStartTimer()(
      store.dispatch,
      store.getState,
      {}
    );
  };

  const callAction2 = (data = initState) => {
    const store = createStore(rootReducer, data);
    return callerAuthenticationActions.handleStartTimer({isByPass: true})(
      store.dispatch,
      store.getState,
      {}
    );
  };

  it('Should have execute handleOnClickConfirm', async () => {
    const startAuthenticated = timerWorkingActionSpy('startAuthenticate');

    const setContactPersonName = timerWorkingActionSpy('setContactPersonName');

    callAction();

    expect(startAuthenticated).toHaveBeenCalledWith('eValue');
    expect(setContactPersonName).toHaveBeenCalledWith({
      eValue: 'eValue',
      contactPerson: 'customerRoleTypeValue'
    });
  });

  it('Should have execute handleOnClickConfirm > case 2', async () => {
    const startAuthenticated = timerWorkingActionSpy('startAuthenticate');

    const setContactPersonName = timerWorkingActionSpy('setContactPersonName');

    callAction2();

    expect(startAuthenticated).toHaveBeenCalledWith('eValue');
    expect(setContactPersonName).toHaveBeenCalledWith({
      eValue: 'eValue',
      contactPerson: ''
    });
  });
});
