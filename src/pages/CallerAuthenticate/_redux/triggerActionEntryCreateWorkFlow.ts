import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { DEFAULT_HOME_PHONE } from 'app/constants';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { collectionFormService } from 'pages/CollectionForm/collectionFormServices';
import { batch } from 'react-redux';

// types
import {
  CallerAuthenticationState,
  TriggerActionEntryCreateWorkFlowArgs
} from './../types';
import { callerAuthenticationActions } from './reducers';

export const triggerActionEntryCreateWorkFlow = createAsyncThunk<
  unknown,
  TriggerActionEntryCreateWorkFlowArgs,
  ThunkAPIConfig
>('collectionForm/updateActionEntryWorkFlow', async (args, thunkAPI) => {
  const { rejectWithValue, dispatch, getState } = thunkAPI;
  const { postData } = args;
  const { commonConfig } = window.appConfig || {};
  const { callerAuthentication } = getState();
  const requestData = {
    common: {
      app: commonConfig?.app,
      org: commonConfig?.org,
      accountId: postData.accountId
    },
    callingApplication: commonConfig?.callingApplicationApr01 || '',
    operatorCode: commonConfig?.operatorID,
    cardholderContactedName: postData?.cardholderContactedName,
    // cardholderContactedPhoneNumber is numberPhoneAuthenticated or home phone of primary or default which is 0000000000
    cardholderContactedPhoneNumber:
      callerAuthentication?.numberPhoneAuthenticated ??
      (callerAuthentication.selectedCaller
        ?.primaryCustomerHomePhoneIdentifier ||
        DEFAULT_HOME_PHONE),
    actionCode: postData?.actionCode
  };
  try {
    const response = await collectionFormService.updateActionEntryWorkFlow(
      requestData
    );
    if (response?.data?.status !== 'success') throw response;

    batch(() => {
      dispatch(
        apiErrorNotificationAction.clearApiErrorData({
          forSection: 'inModalBody',
          storeId: API_ERROR_STORE_ID.AUTHENTICATION_MODAL
        })
      );
      dispatch(callerAuthenticationActions.toggleModalCallerAuthentication());
    });
  } catch (error) {
    dispatch(
      apiErrorNotificationAction.updateApiError({
        forSection: 'inModalBody',
        storeId: API_ERROR_STORE_ID.AUTHENTICATION_MODAL,
        apiResponse: error
      })
    );
    return rejectWithValue(error);
  }
});

export const triggerActionEntryCreateWorkFlowBuilder = (
  builder: ActionReducerMapBuilder<CallerAuthenticationState>
) => {
  builder
    .addCase(triggerActionEntryCreateWorkFlow.pending, draftState => {
      draftState.authenticationInfo = {
        ...draftState.authenticationInfo,
        isLoading: true
      };
    })
    .addCase(triggerActionEntryCreateWorkFlow.fulfilled, draftState => {
      draftState.authenticationInfo = {
        ...draftState.authenticationInfo,
        isLoading: false,
        error: false
      };
    })
    .addCase(triggerActionEntryCreateWorkFlow.rejected, draftState => {
      draftState.authenticationInfo = {
        ...draftState.authenticationInfo,
        isLoading: false,
        error: true
      };
    });
};
