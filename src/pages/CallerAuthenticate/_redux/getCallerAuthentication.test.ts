import { getCallerAuthentication } from './getCallerAuthentication';
import { callerAuthenticationService } from '../callerAuthenticationService';
import { responseDefault, storeId } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import {
  CallerAuthenticationData,
  GetCallerAuthenticationArgs
} from '../types';
import { createStore } from 'redux';
import { ADDRESS_MAP, SPOUSE_FULL, STATE } from '../constants';
import { SPOUSE } from 'app/constants';

const mockArgs: GetCallerAuthenticationArgs = {
  accountId: storeId
};

let spy1: jest.SpyInstance;
let spy2: jest.SpyInstance;
let spy3: jest.SpyInstance;
let spy4: jest.SpyInstance;
let spy5: jest.SpyInstance;

describe('Test getCallerAuthentication', () => {
  afterEach(() => {
    spy1?.mockReset();
    spy1?.mockRestore();

    spy2?.mockReset();
    spy2?.mockRestore();

    spy3?.mockReset();
    spy3?.mockRestore();

    spy4?.mockReset();
    spy4?.mockRestore();

    spy5?.mockReset();
    spy5?.mockRestore();
  });

  it('should be fulfilled', async () => {
    const store = createStore(rootReducer, {});
    const today = new Date();
    const nextDate = new Date();
    nextDate.setDate(today.getDate() + 1);
    const yesterday = new Date();
    yesterday.setDate(today.getDate() - 1);

    spy1 = jest
      .spyOn(callerAuthenticationService, 'triggerRenewToken')
      .mockResolvedValue({ ...responseDefault, data: { jwt: 'test' } });

    spy2 = jest
      .spyOn(callerAuthenticationService, 'getCallerAuthentication')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          customerInquiry: [
            { customerRoleTypeCode: '01', delinquentAmount: '1' },
            { customerRoleTypeCode: '02', delinquentAmount: '1' },
            {
              customerRoleTypeCode: '03',
              delinquentAmount: '1',
              memberSequenceIdentifier: '01'
            },
            { customerRoleTypeCode: '04', delinquentAmount: '1' }
          ]
        }
      });

    spy3 = jest
      .spyOn(callerAuthenticationService, 'getAddressInfo')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          primary: [
            {
              addressContinuationLine1Text: 'address 1',
              addressTypeCode: ADDRESS_MAP.BILLING
            },
            {
              addressContinuationLine1Text: 'address 3'
            }
          ],
          secondary: [{ addressContinuationLine1Text: 'Secondary Address' }],
          additionalUsers: [
            {
              memberSequenceIdentifier: '01'
            }
          ]
        }
      });

    spy4 = jest
      .spyOn(callerAuthenticationService, 'checkTypeofAddress')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    spy5 = jest
      .spyOn(callerAuthenticationService, 'getPostalCodeList')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    const mockDispatch = jest.fn().mockReturnValue('Token');
    const response = await getCallerAuthentication(mockArgs)(
      mockDispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'callerAuthentication/getCallerAuthentication/fulfilled'
    );
  });

  it('should be fulfilled > EXPAND_ADDRESS', async () => {
    const store = createStore(rootReducer, {});
    const today = new Date();
    const nextDate = new Date();
    nextDate.setDate(today.getDate() + 1);
    const yesterday = new Date();
    yesterday.setDate(today.getDate() - 1);

    spy1 = jest
      .spyOn(callerAuthenticationService, 'triggerRenewToken')
      .mockResolvedValue({ ...responseDefault, data: { jwt: 'test' } });

    spy2 = jest
      .spyOn(callerAuthenticationService, 'getCallerAuthentication')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          customerInquiry: [
            { customerRoleTypeCode: '01', delinquentAmount: '1' },
            { customerRoleTypeCode: '02', delinquentAmount: '1' },
            {
              customerRoleTypeCode: '03',
              delinquentAmount: '1',
              memberSequenceIdentifier: '01'
            },
            { customerRoleTypeCode: '04', delinquentAmount: '1' }
          ]
        }
      });

    spy3 = jest
      .spyOn(callerAuthenticationService, 'getAddressInfo')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          primary: [
            {
              addressContinuationLine1Text: 'address 1',
              addressTypeCode: ADDRESS_MAP.BILLING
            },
            {
              addressSubdivisionOneText: STATE.MA,
              addressContinuationLine1Text: 'address 2',
              addressTypeCode: ADDRESS_MAP.BILLING,
              addressCategoryCode: ADDRESS_MAP.PERMANENT,
              effectiveFromDate: yesterday,
              effectiveToDate: nextDate
            },
            {
              addressContinuationLine1Text: 'address 3'
            }
          ],
          secondary: [{ addressContinuationLine1Text: 'Secondary Address' }],
          additionalUsers: [
            {
              memberSequenceIdentifier: '01'
            }
          ]
        }
      });

    spy4 = jest
      .spyOn(callerAuthenticationService, 'checkTypeofAddress')
      .mockResolvedValue({
        ...responseDefault,
        data: { expansionAddressCode: ADDRESS_MAP.EXPAND_ADDRESS }
      });

    spy5 = jest
      .spyOn(callerAuthenticationService, 'getPostalCodeList')
      .mockResolvedValue({
        ...responseDefault,
        data: { [SPOUSE.toLowerCase()]: { code: ['S'] } }
      });

    const mockDispatch = jest.fn().mockReturnValue('Token');
    const response = await getCallerAuthentication(mockArgs)(
      mockDispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'callerAuthentication/getCallerAuthentication/fulfilled'
    );
  });

  it('should be fulfilled > PERMANENT', async () => {
    const store = createStore(rootReducer, {});
    const today = new Date();
    const nextDate = new Date();
    nextDate.setDate(today.getDate() + 1);
    const yesterday = new Date();
    yesterday.setDate(today.getDate() - 1);

    spy1 = jest
      .spyOn(callerAuthenticationService, 'triggerRenewToken')
      .mockResolvedValue({ ...responseDefault, data: { jwt: 'test' } });

    spy2 = jest
      .spyOn(callerAuthenticationService, 'getCallerAuthentication')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          customerInquiry: [
            { customerRoleTypeCode: '01', delinquentAmount: '1' },
            { customerRoleTypeCode: '02', delinquentAmount: '1' },
            {
              customerRoleTypeCode: '03',
              delinquentAmount: '1',
              memberSequenceIdentifier: '01'
            },
            { customerRoleTypeCode: '04', delinquentAmount: '1' }
          ]
        }
      });

    spy3 = jest
      .spyOn(callerAuthenticationService, 'getAddressInfo')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          primary: [
            {
              addressContinuationLine1Text: 'address 3'
            },
            {
              addressContinuationLine1Text: 'address 1',
              addressTypeCode: ADDRESS_MAP.BILLING,
              postalCode: 'S'
            },
            {
              addressSubdivisionOneText: STATE.MA,
              addressContinuationLine1Text: 'address 2',
              addressTypeCode: ADDRESS_MAP.BILLING,
              addressCategoryCode: ADDRESS_MAP.PERMANENT,
              effectiveFromDate: yesterday,
              effectiveToDate: nextDate,
              postalCode: 'S'
            },
            {
              addressSubdivisionOneText: STATE.NY,
              addressContinuationLine1Text: 'address 4',
              addressTypeCode: ADDRESS_MAP.BILLING,
              addressCategoryCode: ADDRESS_MAP.PERMANENT,
              effectiveFromDate: yesterday,
              effectiveToDate: nextDate,
              postalCode: 'S'
            }
          ],
          secondary: [{ addressContinuationLine1Text: 'Secondary Address' }],
          additionalUsers: [
            {
              memberSequenceIdentifier: '01'
            }
          ],
          selectSpouseAddress: [
            {
              addressSubdivisionOneText: STATE.NY,
              addressContinuationLine1Text: 'address 4',
              addressTypeCode: ADDRESS_MAP.BILLING,
              addressCategoryCode: ADDRESS_MAP.PERMANENT,
              effectiveFromDate: yesterday,
              effectiveToDate: nextDate,
              postalCode: 'S'
            }
          ],
          [SPOUSE.toLowerCase()]: { code: ['S'] }
        }
      });

    spy4 = jest
      .spyOn(callerAuthenticationService, 'checkTypeofAddress')
      .mockResolvedValue({
        ...responseDefault,
        data: { expansionAddressCode: ADDRESS_MAP.EXPAND_ADDRESS }
      });

    spy5 = jest
      .spyOn(callerAuthenticationService, 'getPostalCodeList')
      .mockResolvedValue({
        ...responseDefault,
        data: { [SPOUSE_FULL.toLowerCase()]: { code: ['S'] } }
      });

    const mockDispatch = jest.fn().mockReturnValue('Token');
    const response = await getCallerAuthentication(mockArgs)(
      mockDispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'callerAuthentication/getCallerAuthentication/fulfilled'
    );
  });

  it('should be fulfilled > getCallerAuthentication, getAddressInfo, checkTypeofAddress are rejected', async () => {
    const store = createStore(rootReducer, {});

    spy1 = jest
      .spyOn(callerAuthenticationService, 'triggerRenewToken')
      .mockResolvedValue({ ...responseDefault });

    spy2 = jest
      .spyOn(callerAuthenticationService, 'getCallerAuthentication')
      .mockRejectedValue({ ...responseDefault });

    spy3 = jest
      .spyOn(callerAuthenticationService, 'getAddressInfo')
      .mockRejectedValue({ ...responseDefault });

    spy4 = jest
      .spyOn(callerAuthenticationService, 'checkTypeofAddress')
      .mockRejectedValue({ ...responseDefault });

    const mockDispatch = jest.fn().mockReturnValue('Token');
    const response = await getCallerAuthentication(mockArgs)(
      mockDispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'callerAuthentication/getCallerAuthentication/fulfilled'
    );
  });

  it('should be rejected', async () => {
    const store = createStore(rootReducer, {});

    spy1 = jest
      .spyOn(callerAuthenticationService, 'triggerRenewToken')
      .mockRejectedValue({ ...responseDefault });

    spy2 = jest
      .spyOn(callerAuthenticationService, 'getCallerAuthentication')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          customerInquiry: [
            { customerRoleTypeCode: '01', delinquentAmount: '1' }
          ]
        }
      });

    spy3 = jest
      .spyOn(callerAuthenticationService, 'getAddressInfo')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          primary: [
            { addressContinuationLine1Text: 'address 1' },
            {
              addressContinuationLine1Text: 'address 2',
              addressTypeCode: 'BLL1'
            }
          ]
        }
      });

    spy4 = jest
      .spyOn(callerAuthenticationService, 'checkTypeofAddress')
      .mockResolvedValue({ ...responseDefault });

    const mockDispatch = jest.fn().mockReturnValue('Token');
    const response = await getCallerAuthentication(mockArgs)(
      mockDispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'callerAuthentication/getCallerAuthentication/rejected'
    );
  });

  it('checks builder > pending', () => {
    const pendingAction = getCallerAuthentication.pending(
      getCallerAuthentication.pending.type,
      mockArgs
    );
    const store = createStore(rootReducer, {});
    const state = store.getState();

    const actual = rootReducer(state, pendingAction);

    expect(actual?.callerAuthentication?.authenticationInfo?.isLoading).toBe(
      true
    );
  });

  it('checks builder > fulfilled', () => {
    const data: CallerAuthenticationData[] = [];
    const fulfilled = getCallerAuthentication.fulfilled(
      { data, rawAccountDetailData: {} },
      getCallerAuthentication.fulfilled.type,
      mockArgs
    );
    const store = createStore(rootReducer, {});
    const state = store.getState();

    const actual = rootReducer(state, fulfilled);

    expect(actual?.callerAuthentication?.authenticationInfo?.isLoading).toBe(
      false
    );
    expect(actual?.callerAuthentication?.authenticationInfo?.data).toBe(data);
  });

  it('checks builder > rejected', () => {
    const pendingAction = getCallerAuthentication.rejected(
      null,
      getCallerAuthentication.rejected.type,
      mockArgs
    );
    const store = createStore(rootReducer, {});
    const state = store.getState();

    const actual = rootReducer(state, pendingAction);

    expect(actual?.callerAuthentication?.authenticationInfo?.isLoading).toBe(
      false
    );
  });
});
