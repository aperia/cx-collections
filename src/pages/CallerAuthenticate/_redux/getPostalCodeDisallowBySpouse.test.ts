import { createStore, Store } from '@reduxjs/toolkit';
import { responseDefault } from 'app/test-utils';

import { rootReducer } from 'storeConfig';

import { getPostalCodeDisallowBySpouse } from './getPostalCodeDisallowBySpouse';
import { reducer } from './reducers';
import { SPOUSE_FULL } from '../constants';
import { callerAuthenticationService } from '../callerAuthenticationService';

describe('CallerAuthenticate > redux getPostalCodeDisallowBySpouse', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {
      callerAuthentication: {}
    });
  });

  it('pending', () => {
    const nextState = reducer(
      {},
      getPostalCodeDisallowBySpouse.pending(
        'getPostalCodeDisallowBySpouse',
        {}
      )
    );
    expect(nextState?.postalCodeDisallowBySpouse?.loading).toEqual(true);
  });
  it('fulfilled', () => {
    const nextState = reducer(
      {},
      getPostalCodeDisallowBySpouse.fulfilled(
        { data: {} },
        'getPostalCodeDisallowBySpouse',
        {}
      )
    );
    expect(nextState?.postalCodeDisallowBySpouse?.loading).toEqual(false);
    expect(nextState?.postalCodeDisallowBySpouse?.error).toEqual(false);
    expect(nextState?.postalCodeDisallowBySpouse?.data).toEqual({});
  });
  it('rejected', () => {
    const nextState = reducer(
      {},
      getPostalCodeDisallowBySpouse.rejected(null, 'getPostalCodeDisallowBySpouse', {})
    );
    expect(nextState?.postalCodeDisallowBySpouse?.loading).toEqual(false);
    expect(nextState?.postalCodeDisallowBySpouse?.error).toEqual(true);
  });

  it('should return data', async () => {
    jest.spyOn(callerAuthenticationService, 'getPostalCodeList').mockResolvedValue({
      ...responseDefault,
      data: { [SPOUSE_FULL.toLowerCase()]: {} }
    });

    const response = await getPostalCodeDisallowBySpouse({})(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({ data: {} });
  });
});
