import { batch } from 'react-redux';

// Type
import { ItemTab } from 'pages/__commons/TabBar/types';

// Redux
import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';
import { tabActions } from 'pages/__commons/TabBar/_redux';

export interface TriggerAddNewTabArgs {
  storeId: string;
  tabItem: ItemTab;
  customerName: string;
  customerNameForCreateWorkflow: string;
  selectedCaller: MagicKeyValue;
}

export const triggerAddNewTab: AppThunk<Promise<unknown>> =
  ({
    storeId,
    tabItem,
    customerName,
    customerNameForCreateWorkflow,
    selectedCaller
  }: TriggerAddNewTabArgs) =>
  async (dispatch, getState) => {
    const { tab } = getState();

    // ReAuthenticate
    if (tab.tabs.some(item => item.storeId === storeId)) {
      batch(() => {
        dispatch(
          accountDetailActions.saveSelectedCaller({
            storeId: storeId,
            data: selectedCaller,
            authData: {
              customerName,
              customerNameForCreateWorkflow
            }
          })
        );

        dispatch(tabActions.addTab(tabItem));
        dispatch(accountDetailActions.setRefreshAcc({ storeId }));
      });

      return;
    }

    batch(() => {
      dispatch(
        accountDetailActions.saveSelectedCaller({
          storeId: storeId,
          data: selectedCaller,
          authData: {
            customerName,
            customerNameForCreateWorkflow
          }
        })
      );

      dispatch(tabActions.addTab(tabItem));
    });
  };
