import { callerAuthenticationActions as actions, reducer } from './reducers';

// utils
import { storeId } from 'app/test-utils';

// types
import { AccountDetailData, CallerAuthenticationState } from '../types';

const initialState: CallerAuthenticationState = {};

describe('Test Caller Authenticate reducers', () => {
  it('toggleModalCallerAuthentication', () => {
    const state = reducer(
      initialState,
      actions.toggleModalCallerAuthentication()
    );

    expect(state).toEqual({ isOpenModal: true });
  });

  describe('setNumberPhoneAuthenticated', () => {
    it('when selectedCaller and authenticationInfo provided', () => {
      const state = reducer(
        {
          selectedCaller: {
            customerRoleTypeCode: 'customerRoleTypeCode'
          },
          authenticationInfo: {
            data: [{}]
          }
        },
        actions.setNumberPhoneAuthenticated({
          numberPhone: 'phoneSelected'
        })
      );

      expect(state).toEqual({
        authenticationInfo: {
          data: [{}]
        },
        selectedCaller: {
          customerRoleTypeCode: 'customerRoleTypeCode'
        },
        numberPhoneAuthenticated: '0000000000'
      });
    });

    it('when selectedCaller not provided', () => {
      const state = reducer(
        {
          selectedCaller: {},
          authenticationInfo: {
            data: [{}]
          }
        },
        actions.setNumberPhoneAuthenticated({
          numberPhone: 'phoneSelected'
        })
      );

      expect(state).toEqual({
        authenticationInfo: {
          data: [{}]
        },
        selectedCaller: {},
        numberPhoneAuthenticated: '0000000000'
      });
    });

    it('when authenticationInfo not provided', () => {
      const state = reducer(
        {
          selectedCaller: {
            customerRoleTypeCode: 'customerRoleTypeCode'
          },
          authenticationInfo: {
            data: []
          }
        },
        actions.setNumberPhoneAuthenticated({
          numberPhone: 'phoneSelected'
        })
      );

      expect(state).toEqual({
        authenticationInfo: {
          data: []
        },
        selectedCaller: {
          customerRoleTypeCode: 'customerRoleTypeCode'
        }
      });
    });

    it('when phoneType === otherPhone', () => {
      const state = reducer(
        {
          phoneType: 'otherPhone',
          selectedCaller: {
            customerRoleTypeCode: 'customerRoleTypeCode'
          },
          authenticationInfo: {
            data: []
          }
        },
        actions.setNumberPhoneAuthenticated({
          numberPhone: 'phoneSelected'
        })
      );

      expect(state.numberPhoneAuthenticated ).toEqual('phoneSelected');
    });
  });

  it('setAuthenticateData', () => {
    const accountDetailData: AccountDetailData = { eValue: storeId };

    const state = reducer(
      initialState,
      actions.setAuthenticateData({ accountDetailData })
    );

    expect(state.selectedAccountDetailData).toEqual(accountDetailData);
  });

  it('toggleConfirmModal', () => {
    const state = reducer(initialState, actions.toggleConfirmModal());

    expect(state.modalConfirmOpen).toEqual(true);
  });

  it('toggleConfirmModal', () => {
    const state = reducer(initialState, actions.toggleConfirmModal());

    expect(state.modalConfirmOpen).toEqual(true);
  });

  it('setQuestionConfig', () => {
    const state = reducer(initialState, actions.setQuestionConfig([]));

    expect(state.questionConfig).toEqual([]);
  });

  it('setLoadingAuthenticateModal', () => {
    const state = reducer(initialState, actions.setLoadingAuthenticateModal());

    expect(state.updateAuthentication?.loading).toEqual(true);
  });

  it('setSelectedCaller', () => {
    const payload = {
      memberSequenceIdentifier: 'memberSequenceIdentifier',
      customerName: 'customerName',
      customerRoleTypeCode: '01'
    };
    const state = reducer(initialState, actions.setSelectedCaller(payload));

    expect(state?.selectedCaller).toEqual(payload);
  });

  it('setSelectedCaller save phoneType as phoneOther', () => {
    const payload = {
      memberSequenceIdentifier: 'memberSequenceIdentifier',
      customerName: 'customerName',
      customerRoleTypeCode: '04'
    };
    const state = reducer(initialState, actions.setSelectedCaller(payload));

    expect(state?.selectedCaller).toEqual(payload);
  });

  it('setGetAuthenConfigLoading', () => {
    const state = reducer(
      initialState,
      actions.setGetAuthenConfigLoading({ loading: true })
    );

    expect(state.getConfigLoading).toEqual(true);
  });

  it('resetReducer', () => {
    const state = reducer(
      initialState,
      actions.resetReducer()
    );

    expect(state).toEqual({});
  });

  it('setPhoneType', () => {
    const payload = 'homePhone'
    const state = reducer(initialState, actions.setPhoneType(payload));

    expect(state?.phoneType).toEqual(payload);
  });
});
