import './reducers';
import { createStore } from '@reduxjs/toolkit';
import { mockActionCreator } from 'app/test-utils';
import { rootReducer } from 'storeConfig';
import { callerAuthenticationActions } from './reducers';
import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { CALLER_AUTHENTICATION_ACTIONS } from 'pages/Memos/constants';
import { CHRONICLE_CALL_AUTHENTICATION_KEY } from 'pages/ClientConfiguration/Memos/constants';
import { DASH } from 'app/constants';

const initState: Partial<RootState> = {
  callerAuthentication: {
    phoneType: 'phoneType',
    selectedCaller: {
      clientIdentifier: 'clientIdentifier',
      systemIdentifier: 'system',
      principalIdentifier: 'prin',
      agentIdentifier: 'agent',
      state: 'state',
      postalCode: 'postalCode'
    },
    selectedAccountDetailData: {
      eValue: 'eValue',
      memberSequenceIdentifier: 'memberSequenceIdentifier'
    }
  }
};

const accountDetailActionSpy = mockActionCreator(accountDetailActions);
const memoActionSpy = mockActionCreator(memoActions);
const callerAuthenticationActionSpy = mockActionCreator(
  callerAuthenticationActions
);

describe('Test handleClickByPass', () => {
  const callAction = (data = initState) => {
    const store = createStore(rootReducer, data);
    return callerAuthenticationActions.handleClickByPass()(
      store.dispatch,
      store.getState,
      {}
    );
  };

  it('Should have execute handleClickByPass', async () => {
    const timer = callerAuthenticationActionSpy('handleStartTimer');
    const toggle = callerAuthenticationActionSpy(
      'toggleModalCallerAuthentication'
    );
    const newTab = callerAuthenticationActionSpy(
      'handleOpenNewAccountDetailTab'
    );
    const postMemo = memoActionSpy('postMemo');
    const transfer = accountDetailActionSpy('transferDataFromAuthentication');
    const reset = callerAuthenticationActionSpy('resetReducer');

    callAction();

    expect(timer).toHaveBeenCalled();
    expect(toggle).toHaveBeenCalled();
    expect(newTab).toHaveBeenCalledWith(true);
    expect(postMemo).toHaveBeenCalledWith(
      CALLER_AUTHENTICATION_ACTIONS.BYPASS_AUTHENTICATION,
      {
        cspa: {
          clientNumber: 'clientIdentifier',
          system: 'system',
          prin: 'prin',
          agent: 'agent'
        },
        storeId: 'eValue',
        chronicleKey: CHRONICLE_CALL_AUTHENTICATION_KEY,
        accEValue: 'eValue'
      }
    );
    expect(transfer).toHaveBeenCalledWith({
      storeId: `eValue${DASH}memberSequenceIdentifier`,
      numberPhoneAuthenticated: '0000000000',
      sharedInfoTimer: {
        stateCode: 'state',
        zipCode: 'postalCode',
        phoneType: 'phoneType'
      }
    });
    expect(reset).toHaveBeenCalled();
  });

  it('Should have execute handleClickByPass without state, postalCode', async () => {
    const timer = callerAuthenticationActionSpy('handleStartTimer');
    const toggle = callerAuthenticationActionSpy(
      'toggleModalCallerAuthentication'
    );
    const newTab = callerAuthenticationActionSpy(
      'handleOpenNewAccountDetailTab'
    );
    const postMemo = memoActionSpy('postMemo');
    const transfer = accountDetailActionSpy('transferDataFromAuthentication');
    const reset = callerAuthenticationActionSpy('resetReducer');

    callAction({
      ...initState,
      callerAuthentication: {
        ...initState.callerAuthentication,
        selectedCaller: {
          ...initState.callerAuthentication?.selectedCaller,
          state: undefined,
          postalCode: undefined
        }
      }
    });

    expect(timer).toHaveBeenCalled();
    expect(toggle).toHaveBeenCalled();
    expect(newTab).toHaveBeenCalledWith(true);
    expect(postMemo).toHaveBeenCalledWith(
      CALLER_AUTHENTICATION_ACTIONS.BYPASS_AUTHENTICATION,
      {
        cspa: {
          clientNumber: 'clientIdentifier',
          system: 'system',
          prin: 'prin',
          agent: 'agent'
        },
        storeId: 'eValue',
        chronicleKey: CHRONICLE_CALL_AUTHENTICATION_KEY,
        accEValue: 'eValue'
      }
    );
    expect(transfer).toHaveBeenCalledWith({
      storeId: `eValue${DASH}memberSequenceIdentifier`,
      numberPhoneAuthenticated: '0000000000',
      sharedInfoTimer: {
        stateCode: '',
        zipCode: '',
        phoneType: 'phoneType'
      }
    });
    expect(reset).toHaveBeenCalled();
  });
});
