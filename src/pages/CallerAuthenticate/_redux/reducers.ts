import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Types
import {
  CallerAuthenticationState,
  AccountDetailData,
  CallerAuthenticationData
} from '../types';

// Actions
import { handleOnClickConfirm } from './handleOnClickConfirm';
import { handleStartTimer } from './handleStartTimer';
import { handleOpenNewAccountDetailTab } from './handleOpenNewAccountDetailTab';
import { handleClickByPass } from './handleClickByPass';

import {
  getCallerAuthentication,
  getCallerAuthenticationBuilder
} from './getCallerAuthentication';

import {
  triggerUpdateCallerAuthentication,
  updateCallerAuthenticationBuilder
} from './updateCallerAuthentication';

import {
  getPostalCodeDisallowBySpouse,
  getPostalCodeDisallowBySpouseBuilder
} from './getPostalCodeDisallowBySpouse';

import {
  triggerActionEntryCreateWorkFlow,
  triggerActionEntryCreateWorkFlowBuilder
} from './triggerActionEntryCreateWorkFlow';

import { handleNumberPhoneAuthenticated } from '../helpers';
import { PhoneType } from '../constants';

const customerRole = ['01', '02', '03'];

const { actions, reducer } = createSlice({
  name: 'callerAuthentication',
  initialState: {} as CallerAuthenticationState,
  reducers: {
    toggleModalCallerAuthentication: draftState => {
      const currentState = draftState?.isOpenModal;
      draftState.isOpenModal = !currentState;
    },
    setPhoneType: (draftState, action: PayloadAction<string>) => {
      draftState.phoneType = action.payload;
    },
    setNumberPhoneAuthenticated: (
      draftState,
      action: PayloadAction<{ numberPhone?: string }>
    ) => {
      const { numberPhone } = action.payload;
      const { selectedCaller, authenticationInfo, phoneType } = draftState;

      if (phoneType === PhoneType.OTHER_PHONE) {
        draftState.numberPhoneAuthenticated = numberPhone;
        return;
      }

      if (!authenticationInfo?.data || !authenticationInfo?.data.length) return;

      draftState.numberPhoneAuthenticated = handleNumberPhoneAuthenticated(
        selectedCaller!,
        phoneType || ''
      );
    },
    setAuthenticateData: (
      draftState,
      action: PayloadAction<{
        accountDetailData: AccountDetailData;
      }>
    ) => {
      const { accountDetailData } = action.payload;
      draftState.selectedAccountDetailData = accountDetailData;
    },
    toggleConfirmModal: draftState => {
      const currentState = draftState?.modalConfirmOpen;
      draftState.modalConfirmOpen = !currentState;
    },
    setQuestionConfig: (draftState, action: PayloadAction<string[]>) => {
      draftState.questionConfig = action.payload;
    },
    setLoadingAuthenticateModal: draftState => {
      draftState.updateAuthentication = {
        ...draftState.updateAuthentication,
        loading: !draftState.updateAuthentication?.loading
      };
    },
    setSelectedCaller: (
      draftState,
      action: PayloadAction<CallerAuthenticationData>
    ) => {
      const { phoneType } = draftState;

      const isRoleType = customerRole.includes(
        action.payload.customerRoleTypeCode!
      );

      draftState.phoneType = isRoleType ? phoneType : PhoneType.OTHER_PHONE;
      draftState.selectedCaller = action.payload;
      draftState.selectedCustomerName = action.payload.customerName;
    },
    setGetAuthenConfigLoading: (
      draftState,
      action: PayloadAction<{ loading: boolean }>
    ) => {
      const { loading } = action.payload;
      draftState.getConfigLoading = loading;
    },
    resetReducer: () => {
      return {};
    }
  },
  extraReducers: builder => {
    getCallerAuthenticationBuilder(builder);
    updateCallerAuthenticationBuilder(builder);
    getPostalCodeDisallowBySpouseBuilder(builder);
    triggerActionEntryCreateWorkFlowBuilder(builder);
  }
});

const allActions = {
  ...actions,
  handleOnClickConfirm,
  handleStartTimer,
  handleOpenNewAccountDetailTab,
  handleClickByPass,
  getCallerAuthentication,
  triggerUpdateCallerAuthentication,
  getPostalCodeDisallowBySpouse,
  triggerActionEntryCreateWorkFlow
};

export { allActions as callerAuthenticationActions, reducer };
