import React from 'react';
import { act, fireEvent, screen } from '@testing-library/react';
import CustomerDropdown from './CustomerDropdown';
import {
  mockActionCreator,
  queryByClass,
  renderMockStoreId
} from 'app/test-utils';
import userEvent from '@testing-library/user-event';
import { callerAuthenticationActions } from './_redux/reducers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';

const initialState: Partial<RootState> = {
  callerAuthentication: {
    authenticationInfo: {
      data: [
        { customerRoleTypeCode: '01', fullCustomerName: 'name' },

        { customerRoleTypeCode: 'SP', fullCustomerName: 'txt_spouse' }
      ]
    }
  }
};

const callerAuthenticationActionSpy = mockActionCreator(
  callerAuthenticationActions
);
const apiErrorNotificationActionSpy = mockActionCreator(
  apiErrorNotificationAction
);

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('Test CustomerDropdown component', () => {
  it('render component', () => {
    const setSelectedCaller =
      callerAuthenticationActionSpy('setSelectedCaller');

    const clearApiErrorData =
      apiErrorNotificationActionSpy('clearApiErrorData');

    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(<CustomerDropdown />, {
      initialState
    });
    jest.runAllTimers();

    const element = queryByClass(wrapper.container, 'text-field-container');
    act(() => {
      userEvent.click(element!);
    });
    jest.runAllTimers();

    const item = screen.getByText('txt_spouse');
    expect(item).toBeInTheDocument();

    fireEvent.click(item);

    expect(setSelectedCaller).toHaveBeenCalledWith({
      customerRoleTypeCode: 'SP',
      fullCustomerName: 'txt_spouse'
    });

    expect(clearApiErrorData).toHaveBeenCalledWith({
      forSection: 'inModalBody',
      storeId: API_ERROR_STORE_ID.AUTHENTICATION_MODAL
    });
  });

  it('render dropdown item > empty data', () => {
    jest.useFakeTimers();
    const { wrapper } = renderMockStoreId(<CustomerDropdown />, {
      initialState: {
        callerAuthentication: {
          authenticationInfo: {
            data: []
          }
        }
      }
    });
    jest.runAllTimers();

    const element = queryByClass(wrapper.container, 'text-field-container');
    act(() => {
      userEvent.click(element!);
    });
    jest.runAllTimers();

    expect(screen.getByText('txt_no_results_found')).toBeInTheDocument();
  });
});
