import React from 'react';
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import SelectPhoneType from './SelectPhoneType';
import {
  fireEvent,
  queryByPlaceholderText,
  queryByText,
  screen
} from '@testing-library/react';
import { callerAuthenticationActions } from './_redux/reducers';
import { I18N_AUTHENTICATION } from './constants';
import { placeHolderPhone } from 'app/constants';
import { act } from 'react-dom/test-utils';

HTMLCanvasElement.prototype.getContext = jest.fn();

const spy = mockActionCreator(callerAuthenticationActions);

describe('Test SelectPhoneType Component', () => {
  const setConfirmAuthentication = jest.fn();

  afterEach(() => {
    jest.restoreAllMocks();
    jest.resetAllMocks();
    jest.clearAllMocks();
  });

  const callerAuthentication = {
    phoneType: 'workPhone',
    selectedCaller: { customerRoleTypeCode: '01', fullCustomerName: 'name' }
  };

  const initialState: Partial<RootState> = {
    callerAuthentication
  };

  it('Should call action in useEffect', () => {
    const action = spy('setNumberPhoneAuthenticated');
    renderMockStoreId(
      <SelectPhoneType
        setConfirmAuthentication={setConfirmAuthentication}
        confirmAuthentication={true}
      />,
      { initialState }
    );
    expect(action).toHaveBeenCalled();
  });

  it('Should call action in useEffect component InputPhone', () => {
    const action = spy('setNumberPhoneAuthenticated');
    renderMockStoreId(
      <SelectPhoneType
        setConfirmAuthentication={setConfirmAuthentication}
        confirmAuthentication={true}
      />,
      {
        initialState: {
          callerAuthentication: {
            ...callerAuthentication,
            phoneType: 'otherPhone'
          }
        }
      }
    );
    expect(action).toHaveBeenCalledTimes(1);
  });

  it('Should not call action in useEffect', () => {
    const action = spy('setNumberPhoneAuthenticated');
    renderMockStoreId(
      <SelectPhoneType
        setConfirmAuthentication={setConfirmAuthentication}
        confirmAuthentication={true}
      />,
      {
        initialState: {
          callerAuthentication: {
            ...callerAuthentication,
            phoneType: undefined
          }
        }
      }
    );

    expect(action).not.toHaveBeenCalled();
  });

  it('Should not call action in useEffect', () => {
    const action = spy('setNumberPhoneAuthenticated');
    renderMockStoreId(
      <SelectPhoneType
        setConfirmAuthentication={setConfirmAuthentication}
        confirmAuthentication={true}
      />,
      {
        initialState: {
          callerAuthentication: {
            ...callerAuthentication,
            selectedCaller: undefined
          }
        }
      }
    );

    expect(action).not.toBeCalled();
  });

  it('Should have render InlineMessage', () => {
    const { wrapper } = renderMockStoreId(
      <SelectPhoneType
        setConfirmAuthentication={setConfirmAuthentication}
        confirmAuthentication={true}
      />,
      {
        initialState: {
          callerAuthentication: {
            ...callerAuthentication,
            selectedCaller: {
              fullCustomerName: 'Spouse',
              isSpouseConfirm: true
            }
          }
        }
      }
    );
    const queryInlineMessage = queryByText(
      wrapper.container,
      I18N_AUTHENTICATION.NEW_YORK_STATE_LAW_MSG
    );
    expect(queryInlineMessage).toBeInTheDocument();
  });

  it('Should have click setConfirmAuthentication', () => {
    const confirmAuthentication = true;
    const { wrapper } = renderMockStoreId(
      <SelectPhoneType
        setConfirmAuthentication={setConfirmAuthentication}
        confirmAuthentication={confirmAuthentication}
      />,
      {
        initialState
      }
    );

    const queryInlineMessage = queryByText(
      wrapper.container,
      I18N_AUTHENTICATION.SECURITY_VERIFICATION_REQUIRED
    );

    queryInlineMessage?.click();

    expect(setConfirmAuthentication).toHaveBeenCalledWith(
      !confirmAuthentication
    );
  });

  it('Should have onChangePhone', () => {
    const confirmAuthentication = true;
    const setPhoneType = spy('setPhoneType');

    renderMockStoreId(
      <SelectPhoneType
        setConfirmAuthentication={setConfirmAuthentication}
        confirmAuthentication={confirmAuthentication}
      />,
      {
        initialState
      }
    );

    const allRadios = screen.getByTestId(
      'authenticateModal_home-phone_dls-radio_input'
    );

    fireEvent.click(allRadios);

    expect(setPhoneType).toHaveBeenCalled();
  });
});

describe('Test InputPhone Component', () => {
  const setConfirmAuthentication = jest.fn();

  afterEach(() => {
    jest.restoreAllMocks();
    jest.resetAllMocks();
    jest.clearAllMocks();
  });

  const callerAuthentication = {
    phoneType: 'otherPhone',
    selectedCaller: { customerRoleTypeCode: '01', fullCustomerName: 'name' }
  };

  const initialState: Partial<RootState> = {
    callerAuthentication
  };

  it('Should have message txt_error_msg_phone_number', () => {
    const setNumberPhoneAuthenticated = spy('setNumberPhoneAuthenticated');
    const value = '1234';
    const { wrapper } = renderMockStoreId(
      <SelectPhoneType
        setConfirmAuthentication={setConfirmAuthentication}
        confirmAuthentication={true}
      />,
      { initialState }
    );

    const queryInputPhone = queryByPlaceholderText(
      wrapper.container,
      placeHolderPhone
    );
    act(() => {
      fireEvent.change(queryInputPhone!, { target: { value } });
    });

    fireEvent.blur(queryInputPhone!, { target: { value } });
    expect(setNumberPhoneAuthenticated).toHaveBeenCalled();

    fireEvent.focus(queryInputPhone!);
    const queryText = queryByText(
      wrapper.baseElement as HTMLElement,
      'txt_error_msg_phone_number'
    );
    expect(queryText).toBeInTheDocument();
  });

  it('Should have message txt_error_msg_phone_number', () => {
    const { wrapper } = renderMockStoreId(
      <SelectPhoneType
        setConfirmAuthentication={setConfirmAuthentication}
        confirmAuthentication={true}
      />,
      { initialState }
    );

    const queryInputPhone = queryByPlaceholderText(
      wrapper.container,
      placeHolderPhone
    );

    fireEvent.focus(queryInputPhone!);

    act(() => {
      fireEvent.blur(queryInputPhone!, { target: { value: '' } });
    });

    fireEvent.focus(queryInputPhone!);

    const queryText = queryByText(
      wrapper.baseElement as HTMLElement,
      'txt_phone_number_is_required'
    );

    expect(queryText).toBeInTheDocument();
  });
});
