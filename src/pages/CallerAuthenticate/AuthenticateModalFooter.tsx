import React from 'react';

// component
import { Button, ModalFooter } from 'app/_libraries/_dls/components';

// redux
import { callerAuthenticationActions } from './_redux/reducers';
import {
  selectorErrorPhoneType,
  selectorIsRenderLawMsg
} from './_redux/selectors';
import { batch, useDispatch, useSelector } from 'react-redux';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail } from 'app/hooks';

import { I18N_AUTHENTICATION } from './constants';

interface AuthenticateModalFooterProps {
  confirmAuthentication: boolean;
}

const AuthenticateModalFooter: React.FC<AuthenticateModalFooterProps> = ({
  confirmAuthentication
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const isError = useSelector(selectorErrorPhoneType);
  const isRenderNYStateLawMsg = useSelector(selectorIsRenderLawMsg);

  const handleCannotAuthenticate = () => {
    batch(() => {
      dispatch(callerAuthenticationActions.toggleModalCallerAuthentication());
      dispatch(callerAuthenticationActions.toggleConfirmModal());
    });
  };

  const handleAuthenticate = () => {
    batch(() => {
      dispatch(callerAuthenticationActions.handleStartTimer());
      dispatch(
        callerAuthenticationActions.triggerUpdateCallerAuthentication({
          postData: {
            authenticationType: 'authentication'
          },
          storeId
        })
      );
    });
  };

  const handleOnClickConfirm = () => {
    dispatch(callerAuthenticationActions.handleOnClickConfirm());
  };

  const handleClickByPass = () => {
    dispatch(callerAuthenticationActions.handleClickByPass());
  };

  const renderUI = () => {
    if (!isRenderNYStateLawMsg) {
      return (
        <ModalFooter
          okButtonText={t(I18N_AUTHENTICATION.AUTHENTICATE)}
          cancelButtonText={t(I18N_AUTHENTICATION.CANNOT_AUTHENTICATE)}
          onCancel={handleCannotAuthenticate}
          disabledOk={!confirmAuthentication || isError}
          onOk={handleAuthenticate}
          dataTestId="authenticateModal_footer"
        >
          <div className="flex-1 d-flex">
            <Button
              id="caller-authenticate__authenticate-modal__by-pass-auth-btn"
              dataTestId="authenticateModal_footer_by-pass-btn"
              variant="outline-primary"
              className="ml-n8"
              onClick={handleClickByPass}
            >
              {t(I18N_AUTHENTICATION.BYPASS_AUTHENTICATION)}
            </Button>
          </div>
        </ModalFooter>
      );
    } else {
      return (
        <ModalFooter
          dataTestId="authenticateModal_footer"
          okButtonText={t(I18N_AUTHENTICATION.CONFIRM)}
          onOk={handleOnClickConfirm}
        />
      );
    }
  };

  return renderUI();
};

export default React.memo(AuthenticateModalFooter);
