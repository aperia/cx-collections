import React from 'react';
import { screen } from '@testing-library/react';

import AuthenticateModalFooter from './AuthenticateModalFooter';
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import { callerAuthenticationActions } from './_redux/reducers';
import userEvent from '@testing-library/user-event';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => () => ({
  t: (text: string) => text
}));

const mockCallerAuthenticationActions = mockActionCreator(
  callerAuthenticationActions
);

describe('test Authentication InfoView component with isRenderNYStateLawMsg true', () => {
  it('should be render and click txt_bypass_authentication', () => {
    renderMockStoreId(<AuthenticateModalFooter confirmAuthentication={true} />);

    const mockHandleClickByPass =
      mockCallerAuthenticationActions('handleClickByPass');

    const byPassAuthenButton = screen.getByRole('button', {
      name: 'txt_bypass_authentication'
    });

    userEvent.click(byPassAuthenButton);

    expect(mockHandleClickByPass).toBeCalled();
  });

  it('should be render and click txt_cannot_authenticate', () => {
    renderMockStoreId(<AuthenticateModalFooter confirmAuthentication={true} />);

    const mockTriggerToggleModalCallerAuthentication =
      mockCallerAuthenticationActions('toggleModalCallerAuthentication');
    const mockToggleConfirmModal =
      mockCallerAuthenticationActions('toggleConfirmModal');

    const cannotAuthenticationButton = screen.getByRole('button', {
      name: 'txt_cannot_authenticate'
    });

    userEvent.click(cannotAuthenticationButton);

    expect(mockTriggerToggleModalCallerAuthentication).toBeCalled();

    expect(mockToggleConfirmModal).toBeCalled();
  });

  it('should be render and click txt_authenticate', () => {
    const initialState = {
      callerAuthentication: {
        phoneType: 'test'
      }
    };
    renderMockStoreId(
      <AuthenticateModalFooter confirmAuthentication={true} />,
      { initialState }
    );

    const mockTriggerUpdateCallerAuthenication =
      mockCallerAuthenticationActions('triggerUpdateCallerAuthentication');
    const mockHandleStartTimer =
      mockCallerAuthenticationActions('handleStartTimer');

    const authenticateButton = screen.getByRole('button', {
      name: 'txt_authenticate'
    });

    userEvent.click(authenticateButton);

    expect(mockTriggerUpdateCallerAuthenication).toBeCalledWith({
      postData: {
        authenticationType: 'authentication'
      },
      storeId
    });

    expect(mockHandleStartTimer).toBeCalled();
  });

  it('should be render and click special case (spouse)', () => {
    const initialState = {
      callerAuthentication: {
        selectedCaller: {
          fullCustomerName: 'Spouse',
          isSpouseConfirm: true
        }
      }
    };
    renderMockStoreId(
      <AuthenticateModalFooter confirmAuthentication={true} />,
      { initialState }
    );

    const mockHandleOnClickConfirm = mockCallerAuthenticationActions(
      'handleOnClickConfirm'
    );

    const confirmButton = screen.getByRole('button', {
      name: 'txt_confirm'
    });

    userEvent.click(confirmButton);

    expect(mockHandleOnClickConfirm).toBeCalled();
  });
});
