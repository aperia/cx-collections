import { DEFAULT_HOME_PHONE } from 'app/constants';
import { PhoneType } from './constants';
import { CallerAuthenticationData } from './types';

export const mapRoleType = (code?: string) => {
  switch (code) {
    case '01':
      return 'txt_primary';
    case '02':
      return 'txt_secondary';
    case '03':
      return 'txt_authorized';
    default:
      return '';
  }
};

export const checkPostalCodeDisallowed = (
  postalCodeData: MagicKeyValue,
  postalCode?: string
) => {
  for (const item in postalCodeData) {
    const index = postalCodeData[item].findIndex(
      (e: string) => e === postalCode
    );
    if (index !== -1) {
      return true;
    }
  }

  return false;
};

export const handleNumberPhoneAuthenticated = (
  data: CallerAuthenticationData,
  phoneSelected: string | undefined
): string => {
  let phoneType = '';

  if (phoneSelected === PhoneType.HOME_PHONE) {
    phoneType = 'primaryCustomerHomePhoneIdentifier';
  }

  if (phoneSelected === PhoneType.MOBILE_PHONE) {
    phoneType = 'mobilePhoneIdentifier';
  }

  if (phoneSelected === PhoneType.WORK_PHONE) {
    phoneType = 'primaryCustomerSecondPhoneIdentifier';
  }

  return data[phoneType] ? data[phoneType] : DEFAULT_HOME_PHONE;
};
