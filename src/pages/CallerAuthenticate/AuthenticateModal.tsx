import React, { useEffect, useState } from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Component
import CustomerDropdown from './CustomerDropdown';
import { Modal, ModalHeader, ModalTitle } from 'app/_libraries/_dls/components';
import { View } from 'app/_libraries/_dof/core';
import ModalBodyWithApiError from 'pages/ApiErrorNotification/ModalBodyWithApiError';
import SelectPhoneType from './SelectPhoneType';
import AuthenticateModalFooter from './AuthenticateModalFooter';

// redux
import { callerAuthenticationActions } from './_redux/reducers';
import {
  selectLoading,
  selectOpenModal,
  selectorSelectedCaller,
  selectUpdateAuthenticationLoading
} from './_redux/selectors';

// Const
import { I18N_AUTHENTICATION } from './constants';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';

export interface CallerAuthenticationProps {}

const AuthenticateModal: React.FC<CallerAuthenticationProps> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [confirmAuthentication, setConfirmAuthentication] = useState(false);

  const selectedCaller = useSelector(selectorSelectedCaller);
  const isOpenModal = useSelector(selectOpenModal);
  const isLoading = useSelector(selectLoading);
  const isUpdateLoading = useSelector(selectUpdateAuthenticationLoading);

  // get caller authentication data
  useEffect(() => {
    if (!isOpenModal) return;
    dispatch(callerAuthenticationActions.getCallerAuthentication());
    return () => {
      setConfirmAuthentication(false);
    };
  }, [dispatch, isOpenModal]);

  const handleCloseModalCallerAuthentication = () => {
    batch(() => {
      dispatch(callerAuthenticationActions.toggleModalCallerAuthentication());
      dispatch(callerAuthenticationActions.resetReducer());
    });
  };

  if (!isOpenModal) return null;

  return (
    <Modal
      md
      id="caller-authenticate__authenticate-modal"
      dataTestId="authenticateModal"
      loading={isLoading || isUpdateLoading}
      show={isOpenModal}
      animationDuration={500}
    >
      <ModalHeader
        border
        closeButton
        onHide={handleCloseModalCallerAuthentication}
        dataTestId="authenticateModal_header"
      >
        <ModalTitle dataTestId="authenticateModal_header-title">
          {t(I18N_AUTHENTICATION.CONTACT_AUTHENTICATION)}
        </ModalTitle>
      </ModalHeader>
      <ModalBodyWithApiError
        noPadding
        storeId={API_ERROR_STORE_ID.AUTHENTICATION_MODAL}
        apiErrorClassName="pt-24 pl-24 bg-light-l20"
        dataTestId="authenticateModal_body"
      >
        <div className="bg-light-l20 border-bottom p-24">
          <CustomerDropdown />
          <View
            id={`callerAuthentication`}
            descriptor="callerAuthenticationView"
            formKey={`callerAuthentication`}
            value={{
              ...selectedCaller,
              customerRoleTypeValue: t(selectedCaller?.customerRoleTypeValue)
            }}
          />
        </div>
        <SelectPhoneType
          setConfirmAuthentication={setConfirmAuthentication}
          confirmAuthentication={confirmAuthentication}
        />
      </ModalBodyWithApiError>
      <AuthenticateModalFooter confirmAuthentication={confirmAuthentication} />
    </Modal>
  );
};

export default AuthenticateModal;
