export interface IDataRequest {
  common: {
    accountId: string;
  };
}
export interface APIGetCallerAuthenticationPayload {
  customerInquiry: MagicKeyValue[];
}

export interface AccountDetailData {
  queueId?: string;
  primaryName?: string;
  memberSequenceIdentifier?: string;
  eValue?: string;
  maskedValue?: string;
  socialSecurityIdentifier?: string;
}

export interface CallerAuthenticationState {
  questionConfig?: string[];
  authenticationInfo?: {
    isLoading?: boolean;
    data?: CallerAuthenticationData[];
    error?: boolean;
    rawAccountDetailData?: {
      customerInquiry?: MagicKeyValue[];
      cardholderInfo?: MagicKeyValue[];
    };
  };
  selectedAccountDetailData?: AccountDetailData;
  isOpenModal?: boolean;
  updateAuthentication?: {
    loading?: boolean;
    error?: boolean;
  };
  modalConfirmOpen?: boolean;
  selectedCustomerName?: string;
  phoneType?: string;
  selectedCaller?: CallerAuthenticationData;
  postalCodeDisallowBySpouse?: {
    loading?: boolean;
    error?: boolean;
    data?: MagicKeyValue;
  };
  numberPhoneAuthenticated?: string;
  getConfigLoading?: boolean;
}

export interface CallerAuthenticationData extends MagicKeyValue {
  memberSequenceIdentifier?: string;
  birthDate?: string;
  clientControlledPhoneIdentifier?: string;
  creditLimitAmount?: string;
  customerExternalIdentifier?: string;
  customerName?: string;
  customerRoleTypeCode?: string;
  externalCardTypeCode?: string;
  facsimilePhoneIdentifier?: string;
  lastPaymentAmount?: string;
  mobilePhoneIdentifier?: string;
  mothersMaidenName?: string;
  plasticReplacementIndicator?: string;
  presentationInstrumentIdentifier?: string;
  presentationInstrumentReplacementSequenceNumber?: string;
  presentationInstrumentStatusCode?: string;
  presentationInstrumentTypeCode?: string;
  primaryCustomerHomePhoneIdentifier?: string;
  primaryCustomerSecondPhoneIdentifier?: string;
  relationshipCode?: string;
  socialSecurityIdentifier?: string;
  fullCustomerName?: string;
  customerRoleTypeValue?: string;
  state?: string;
  postalCode?: string;
  isSpouseConfirm?: boolean;
  clientIdentifier?: string;
  systemIdentifier?: string;
  agentIdentifier?: string;
  principalIdentifier?: string;
  isByPass?: boolean;
}

export interface CallerAuthenticationPostData {
  common?: {
    accountId?: string;
  };
  selectFields?: string[];
}

export interface CallerAuthenticationPayLoad {
  data: CallerAuthenticationData[];
  rawAccountDetailData: MagicKeyValue;
}

// ------------UPDATE AUTHENTICATION ---------------

export type AuthenticationType = 'authentication' | 'cannotAuthentication';
export interface UpdateAuthenticationCaller {
  postData: {
    authenticationType: AuthenticationType;
  };
  storeId: string;
}
export interface UpdateAuthenticationCallerRequest
  extends CommonAccountIdRequestBody {
  authenticationType: string;
  phoneType: string;
  isSendLetter: boolean;
}

// --------- GET ADDRESS INFO -----------
export interface GetAddressRequest extends CommonAccountIdRequestBody {}

export interface AddressInfo {
  addressContinuationLine1Text: string;
  addressContinuationLine2Text?: string;
  addressContinuationLine3Text?: string;
  addressContinuationLine4Text?: string;
  addressCategoryCode?: string;
  addressTypeCode?: string;
  effectiveFromDate?: Date | string;
  effectiveToDate?: Date | string;
  cityName?: string;
  addressSubdivisionOneText?: string;
  postalCode?: string;
  countryCode?: string;
  memberSequenceIdentifier?: string;
}

export interface GetTypeAddress extends CommonAccountIdRequestBody {}

export interface TypeAddressInfo {
  expansionAddressCode: string;
}

export interface GetPostalCodeListPayload {
  data?: MagicKeyValue;
}

export interface GetPostalCodeListArgs {}

export interface TriggerActionEntryCreateWorkFlowArgs {
  postData: {
    accountId: string;
    cardholderContactedName: string;
    actionCode: string;
  };
}
