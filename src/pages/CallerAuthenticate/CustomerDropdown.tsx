import React, { useCallback, useMemo } from 'react';
import isEmpty from 'lodash.isempty';

// components
import {
  DropdownBaseChangeEvent,
  DropdownList
} from 'app/_libraries/_dls/components';

// redux
import { selectAuthData, selectorSelectedCaller } from './_redux/selectors';
import { callerAuthenticationActions } from './_redux/reducers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useDispatch, useSelector } from 'react-redux';

// helper
import { I18N_AUTHENTICATION } from './constants';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { API_ERROR_STORE_ID } from 'pages/AccountDetails/constants';
import { CallerAuthenticationData } from './types';
import { DEFAULT_CALLER } from 'app/constants';
import { isUndefined } from 'lodash';

export interface CustomerDropdownProps {}

const CustomerDropdown: React.FC<CustomerDropdownProps> = ({}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const callers = useSelector(selectAuthData);
  const selectedCaller = useSelector(selectorSelectedCaller);

  const translateCustomerFullName = useCallback(
    (item?: CallerAuthenticationData) =>
      item
        ? {
            ...item,
            fullCustomerName: isUndefined(
              DEFAULT_CALLER.find(e => e.value === item.customerRoleTypeCode)
            )
              ? `${t(item.customerName)} • ${t(item.customerRoleTypeValue)}`
              : t(item.fullCustomerName)
          }
        : {},
    [t]
  );

  const value: CallerAuthenticationData =
    translateCustomerFullName(selectedCaller);

  const renderItem = useMemo(() => {
    if (isEmpty(callers)) {
      return [];
    }

    return callers.map((item, index) => {
      const caller: CallerAuthenticationData = translateCustomerFullName(item);
      return (
        <DropdownList.Item
          key={`${caller.memberSequenceIdentifier}-${index}`}
          label={caller?.fullCustomerName}
          value={caller}
        />
      );
    });
  }, [callers, translateCustomerFullName]);

  const handleChange = (event: DropdownBaseChangeEvent) => {
    dispatch(callerAuthenticationActions.setSelectedCaller(event.value));
    dispatch(
      apiErrorNotificationAction.clearApiErrorData({
        forSection: 'inModalBody',
        storeId: API_ERROR_STORE_ID.AUTHENTICATION_MODAL
      })
    );
  };

  return (
    <div className="row">
      <div className="col-12">
        <p
          data-testid={genAmtId(
            'customerDropdown',
            'label',
            'CustomerDropdown'
          )}
        >
          {t(I18N_AUTHENTICATION.SELECT_A_CONTACT)}
        </p>
      </div>
      <div className="col-6 mt-16">
        <DropdownList
          id="caller-authenticate__contact-dropdown"
          dataTestId="customerDropdown"
          name="callerAuthentication"
          label={t('txt_contact')}
          value={value}
          onChange={handleChange}
          textField="fullCustomerName"
          required
          noResult={t('txt_no_results_found')}
        >
          {renderItem}
        </DropdownList>
      </div>
    </div>
  );
};

export default React.memo(CustomerDropdown);
