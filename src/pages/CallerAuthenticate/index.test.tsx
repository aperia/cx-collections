import { renderMockStoreId } from 'app/test-utils';
import React from 'react';
import CallerAuthenticationWrapper from './index';
import { screen } from '@testing-library/react';

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('Test CallerAuthenticationWrapper component', () => {
  it('opens authentication modal', () => {
    jest.useFakeTimers();
    renderMockStoreId(<CallerAuthenticationWrapper />, {
      initialState: {
        callerAuthentication: {
          isOpenModal: true
        }
      }
    });
    jest.runAllTimers();

    expect(screen.getByText('txt_contact_authentication')).toBeInTheDocument();
  });

  it('opens cannot authentication modal', () => {
    jest.useFakeTimers();
    renderMockStoreId(<CallerAuthenticationWrapper />, {
      initialState: {
        callerAuthentication: {
          modalConfirmOpen: true
        }
      }
    });
    jest.runAllTimers();

    expect(screen.getByText('txt_cannot_authenticate')).toBeInTheDocument();
  });
});
