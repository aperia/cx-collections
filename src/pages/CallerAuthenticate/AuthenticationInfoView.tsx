import GroupTextControl from 'app/components/_dof_controls/controls/GroupTextControl';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import React from 'react';
import { AUTHENTICATION_INFO_VIEW_CONFIG } from './constants';
import { CallerAuthenticationData } from './types';

interface AuthenticationInfoViewProps {
  data: CallerAuthenticationData;
  questionConfig: string[];
}

export const DataField = ({
  label,
  value,
  className,
  format,
  dataTestId
}: {
  label: string;
  value: string | boolean | undefined;
  className: string;
  format?: string;
  dataTestId?: string;
}) => {
  return (
    <div className={className}>
      <GroupTextControl
        input={{ value } as never}
        label={label}
        options={format ? ({ format } as never) : {}}
        id={label}
        meta={{} as never}
        dataTestId={dataTestId}
      />
    </div>
  );
};

export const AuthenticationInfoView: React.FC<AuthenticationInfoViewProps> = ({
  data = {},
  questionConfig
}) => {
  const { t } = useTranslation();

  return (
    <div className="row">
      <h5 className="col-12" data-testid={genAmtId('authenticationInfoView', 'title', '')}>
        {t('txt_authentication_information')}
      </h5>
      {AUTHENTICATION_INFO_VIEW_CONFIG.map(fields =>
        questionConfig.includes(fields.name) ? (
          <DataField
            key={fields.label}
            className={fields.className}
            label={fields.label}
            value={data[fields.dataField as keyof CallerAuthenticationData]}
            format={fields.format}
            dataTestId={`${fields.name}_authenticationInfoView_field`}
          />
        ) : null
      )}
    </div>
  );
};
