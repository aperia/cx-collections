import { callerAuthenticationService } from './callerAuthenticationService';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import {
  CallerAuthenticationPostData,
  UpdateAuthenticationCallerRequest
} from './types';

describe('callerAuthenticationService', () => {
  const token = 'token';

  describe('getCallerAuthentication', () => {
    const params: CallerAuthenticationPostData = {
      selectFields: ['field_1', 'field_2']
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      callerAuthenticationService.getCallerAuthentication(params, token);

      expect(mockService).toBeCalledWith('', params, {
        headers: { Authorization: token }
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      callerAuthenticationService.getCallerAuthentication(params, token);

      expect(mockService).toBeCalledWith(
        apiUrl.callerAuthentication.getCallerAuthentication,
        params,
        { headers: { Authorization: token } }
      );
    });
  });

  it('updateCallerAuthentication', async () => {
    const params: UpdateAuthenticationCallerRequest = {
      authenticationType: 'Bean'
    };

    const { data } =
      await callerAuthenticationService.updateCallerAuthentication(params);

    expect(data).toEqual({ message: 'Update successfully' });
  });

  describe('getAddressInfo', () => {
    const params: CallerAuthenticationPostData = {
      selectFields: ['field_1', 'field_2']
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      callerAuthenticationService.getAddressInfo(params, token);

      expect(mockService).toBeCalledWith('', params, {
        headers: { Authorization: token }
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      callerAuthenticationService.getAddressInfo(params, token);

      expect(mockService).toBeCalledWith(
        apiUrl.callerAuthentication.getAddressInfo,
        params,
        { headers: { Authorization: token } }
      );
    });
  });

  describe('triggerRenewToken', () => {
    const params: RenewTokenPayload = {
      jwt: 'eyJhbGciOiJIUzI1NiJ9.eyJlc3NVc2VyTmFtZSI6IlRlc3Qg'
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      callerAuthenticationService.triggerRenewToken(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      callerAuthenticationService.triggerRenewToken(params);

      expect(mockService).toBeCalledWith(apiUrl.common.renewToken, params);
    });
  });

  describe('checkTypeofAddress', () => {
    const params: MagicKeyValue = {
      histories: [],
      openModal: 'ADD',
      isLoading: false,
      isEndLoadMore: false
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      callerAuthenticationService.checkTypeofAddress(params, token);

      expect(mockService).toBeCalledWith('', params, {
        headers: { Authorization: token }
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      callerAuthenticationService.checkTypeofAddress(params, token);

      expect(mockService).toBeCalledWith(
        apiUrl.callerAuthentication.checkTypeAddress,
        params,
        { headers: { Authorization: token } }
      );
    });
  });

  describe('getPostalCodeList', () => {
    it('getPostalCodeList', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      callerAuthenticationService.getPostalCodeList();

      expect(mockService).toBeCalledWith(
        apiUrl.callerAuthentication.getPostalCodeList
      );
    });
  });
});
