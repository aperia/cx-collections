import React, { useEffect } from 'react';

// component
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

// redux
import {
  selectConfirmModalOpen,
  selectUpdateAuthenticationLoading
} from './_redux/selectors';
import { callerAuthenticationActions } from './_redux/reducers';
import { batch, useDispatch, useSelector } from 'react-redux';

// hooks
import { useAccountDetail, useStoreIdSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { I18N_AUTHENTICATION } from './constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

interface ConfirmModalProp {}

const ConfirmCannotAuthentication: React.FC<ConfirmModalProp> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();

  const testId = 'confirmCannotAuthenticationModal';

  const showModal = useSelector<RootState, boolean>(selectConfirmModalOpen);

  const isUpdateLoading = useStoreIdSelector<boolean>(
    selectUpdateAuthenticationLoading
  );

  useEffect(() => {
    return () => {
      showModal && dispatch(callerAuthenticationActions.resetReducer());
    };
  }, [dispatch, showModal]);

  const handleCancel = () => {
    batch(() => {
      dispatch(callerAuthenticationActions.toggleConfirmModal());
    });
  };

  const handleContinue = () => {
    dispatch(
      callerAuthenticationActions.triggerUpdateCallerAuthentication({
        storeId,
        postData: {
          authenticationType: 'cannotAuthentication'
        }
      })
    );
  };

  if (!showModal) return null;

  return (
    <Modal show={showModal} loading={isUpdateLoading} dataTestId={testId}>
      <ModalHeader
        border
        closeButton
        onHide={handleCancel}
        dataTestId={`${testId}_header`}
      >
        <ModalTitle dataTestId={`${testId}_title`}>
          {t(I18N_AUTHENTICATION.CANNOT_AUTHENTICATE)}
        </ModalTitle>
      </ModalHeader>
      <ModalBody dataTestId={`${testId}_body`}>
        {t(I18N_AUTHENTICATION.CANNOT_AUTHENTICATION_DESCRIPTION)}
      </ModalBody>
      <ModalFooter dataTestId={`${testId}_footer`}>
        <Button
          variant="secondary"
          onClick={handleCancel}
          dataTestId={`${testId}_cancel-btn`}
        >
          {t(I18N_COMMON_TEXT.CANCEL)}
        </Button>

        <Button
          variant="danger"
          onClick={handleContinue}
          dataTestId={`${testId}_continue-btn`}
        >
          {t(I18N_COMMON_TEXT.CONTINUE)}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default ConfirmCannotAuthentication;
