import { QUESTION_CONFIG_ID } from 'pages/ClientConfiguration/Authentication/constants';
import { AuthenticationType } from './types';
export const I18N_AUTHENTICATION = {
  AUTHENTICATE: 'txt_authenticate',
  TOAST_AUTHENTICATION_FAILED: 'txt_authentication_failed_to_submit_toast',
  TOAST_AUTHENTICATION_PASSED: 'txt_authentication_passed_toast',
  TOAST_CANNOT_AUTHENTICATION_PASSED: 'txt_cannot_authentication_passed_toast',
  TOAST_CANNOT_AUTHENTICATION_FAILED: 'txt_cannot_authentication_failed_toast',
  CANNOT_AUTHENTICATE: 'txt_cannot_authenticate',
  BYPASS_AUTHENTICATION: 'txt_bypass_authentication',
  CONTACT_AUTHENTICATION: 'txt_contact_authentication',
  SELECT_A_CONTACT: 'txt_select_a_contact_to_authenticate',
  AUTHENTICATION_INFORMATION: 'txt_authentication_information',
  CANNOT_AUTHENTICATION_DESCRIPTION:
    'txt_cannot_authenticate_description_confirm',
  SECURITY_VERIFICATION_REQUIRED:
    'txt_security_verification_required_before_proceeding',
  TOAST_AUTHENTICATION_BYPASSED: 'txt_authentication_bypassed_toast',
  WORK_PHONE: 'txt_phone_work',
  HOME_PHONE: 'txt_phone_home',
  MOBILE_PHONE: 'txt_phone_mobile',
  OTHER_PHONE: 'txt_other_phone',
  SELECT_PHONE_TYPE: 'txt_select_phone_type',
  NEW_YORK_STATE_LAW_MSG: 'txt_new_york_state_law_msg',
  CONFIRM: 'txt_confirm'
};

export interface TOAST_MESSAGE_GROUP {
  SUCCESS: string;
  FAILED: string;
}

export const TOAST_MESSAGE: Record<AuthenticationType, TOAST_MESSAGE_GROUP> = {
  authentication: {
    SUCCESS: I18N_AUTHENTICATION.TOAST_AUTHENTICATION_PASSED,
    FAILED: I18N_AUTHENTICATION.TOAST_AUTHENTICATION_FAILED
  },
  cannotAuthentication: {
    SUCCESS: I18N_AUTHENTICATION.TOAST_CANNOT_AUTHENTICATION_PASSED,
    FAILED: I18N_AUTHENTICATION.TOAST_CANNOT_AUTHENTICATION_FAILED
  }
};

export const AUTHENTICATION_SELECTED_FIELDS = [
  'customerInquiry.customerName',
  'customerInquiry.customerRoleTypeCode',
  'customerInquiry.relationshipCode',
  'customerInquiry.presentationInstrumentIdentifier',
  'customerInquiry.externalCardTypeCode',
  'customerInquiry.presentationInstrumentStatusCode',
  'customerInquiry.mothersMaidenName',
  'customerInquiry.birthDate',
  'customerInquiry.socialSecurityIdentifier',
  'customerInquiry.lastPaymentAmount',
  'customerInquiry.creditLimitAmount',
  'customerInquiry.primaryCustomerHomePhoneIdentifier',
  'customerInquiry.primaryCustomerSecondPhoneIdentifier',
  'customerInquiry.mobilePhoneIdentifier',
  'customerInquiry.facsimilePhoneIdentifier',
  'customerInquiry.clientControlledPhoneIdentifier'
];

export const ADDRESS_REQUEST_SELECT_FIELDS = [
  'cityName',
  'postalCode',
  'addressContinuationLine1Text',
  'addressContinuationLine2Text',
  'addressContinuationLine3Text',
  'addressContinuationLine4Text',
  'addressSubdivisionOneText',
  'addressSubdivisionTwoText',
  'memberSequenceIdentifier',
  'addressCategoryCode',
  'addressTypeCode',
  'effectiveFromDate',
  'effectiveToDate',
  'countryCode'
];

export const FULL_CUSTOMER_NAME = [
  'Branch',
  'Administrator',
  'Merchant/Other',
  'Back office',
  'Third Party',
  'Attorney',
  'Power of Attorney',
  'Debt Management Company'
];

export const ADDRESS_MAP = {
  PERMANENT: 'P',
  BILLING: 'BLL1',
  EXPAND_ADDRESS: '1',
  LETTER_ADDRESS: 'LTTR'
};

export enum PhoneType {
  WORK_PHONE = 'workPhone',
  HOME_PHONE = 'homePhone',
  MOBILE_PHONE = 'cellPhone',
  OTHER_PHONE = 'otherPhone'
}

export const STATE = {
  MA: 'MA',
  NY: 'NY'
};

export const SPOUSE_FULL = 'Spouse';

export const AUTHENTICATION_INFO_VIEW_CONFIG = [
  {
    name: QUESTION_CONFIG_ID.MOTHERS_MAIDEN_NAME,
    className: 'col-4',
    label: 'txt_maiden_name_of_mother',
    dataField: 'mothersMaidenName'
  },
  {
    name: QUESTION_CONFIG_ID.DATE_OF_BIRTH,
    className: 'col-4',
    label: 'txt_date_of_birth',
    dataField: 'birthDate',
    format: 'maskText'
  },
  {
    name: QUESTION_CONFIG_ID.LAST_4_SSN,
    className: 'col-4',
    label: 'txt_last_4_ssn',
    dataField: 'socialSecurityIdentifier'
  },
  {
    name: QUESTION_CONFIG_ID.LAST_PAYMENT_AMOUNT,
    className: 'col-4',
    label: 'txt_last_payment_amount',
    dataField: 'lastPaymentAmount',
    format: 'amount'
  },
  {
    name: QUESTION_CONFIG_ID.CREDIT_LIMIT,
    className: 'col-4',
    label: 'txt_credit_limit',
    dataField: 'creditLimitAmount',
    format: 'amount'
  },
  {
    name: QUESTION_CONFIG_ID.HOME_PHONE,
    className: 'col-4',
    label: 'txt_phone_home',
    dataField: 'primaryCustomerHomePhoneIdentifier',
    format: 'phoneNumber'
  },
  {
    name: QUESTION_CONFIG_ID.WORK_PHONE,
    className: 'col-4',
    label: 'txt_phone_work',
    dataField: 'primaryCustomerSecondPhoneIdentifier',
    format: 'phoneNumber'
  },
  {
    name: QUESTION_CONFIG_ID.MOBILE_PHONE,
    className: 'col-4',
    label: 'txt_phone_mobile',
    dataField: 'mobilePhoneIdentifier',
    format: 'phoneNumber'
  },
  {
    name: QUESTION_CONFIG_ID.FAX_BILLING_ADDRESS,
    className: 'col-4',
    label: 'txt_fax',
    dataField: 'facsimilePhoneIdentifier',
    format: 'phoneNumber'
  },
  {
    name: QUESTION_CONFIG_ID.MISCELLANEOUS,
    className: 'col-4',
    label: 'txt_miscellaneous',
    dataField: 'clientControlledPhoneIdentifier',
    format: 'phoneNumber'
  },
  {
    name: QUESTION_CONFIG_ID.PERMANENT_BILLING_ADDRESS,
    className: 'col-8',
    label: 'txt_address',
    format: 'fiveLinesAddress',
    dataField: 'address'
  }
];

export const SPOUSE_TEXTS = ['spouse', 'cónyuge', 'conjoint€'];
