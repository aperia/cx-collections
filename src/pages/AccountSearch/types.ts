import { MainSubConditionValue } from 'app/_libraries/_dls/components';

export type FieldType<T> = {
  value?: T;
};

// --------------------ATTRIBUTE SEARCH --------------------------//

export type AttrMainSubConditionModel = FieldType<MainSubConditionValue>;
export type AttrCommonModel = FieldType<string>;
export type AttrAmountModel = {
  value: string[] | undefined[];
};
export interface AttributeSearchFieldModel {
  firstName?: AttrCommonModel;
  lastName?: AttrMainSubConditionModel;
  middleInitial?: AttrCommonModel;
  accountNumber?: AttrCommonModel;
  affinityNumber?: AttrCommonModel;
  phoneNumber?: AttrCommonModel;
  ssn?: AttrCommonModel;
  state?: AttrCommonModel;
  zipCode?: AttrCommonModel;
}

export enum ATTRIBUTE_SEARCH_FIELD {
  ACCOUNT_NUMBER = 'accountNumber',
  AFFINITY_NUMBER = 'affinityNumber',
  FIRST_NAME = 'firstName',
  LAST_NAME = 'lastName',
  MIDDLE_INITIAL = 'middleInitial',
  PHONE_NUMBER = 'phoneNumber',
  SSN = 'ssn',
  STATE = 'state',
  ZIP_CODE = 'zipCode'
}
