import React from 'react';

// components
import {
  AttributeSearchData,
  MainSubConditionValue
} from 'app/_libraries/_dls/components';
import ComboBoxControl from 'app/components/AttributeSearchField';
import InputControl from 'app/components/InputControl';
import MainSubConditionControl from 'app/components/MainSubConditionControl';
import MaskedTextBoxControl from 'app/components/MaskedTextBoxControl';

// constant
import {
  PHONE_MASK_INPUT_REGEX,
  SSN_MASK_INPUT_REGEX,
  MESSAGE,
  defaultExactness,
  mainSubConditionData
} from './constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { ATTRIBUTE_SEARCH_FIELD } from './types';

export const getInitialAttrData = (t: any) => {
  const accountNumberField: AttributeSearchData = {
    key: ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER,
    name: I18N_COMMON_TEXT.ACCOUNT_NUMBER,
    description: t('txt_account_number_description'),
    component: props => (
      <InputControl
        placeholder={I18N_COMMON_TEXT.ENTER_KEYWORD}
        maxLength={16}
        mode="number"
        {...props}
      />
    ),
    groupName: t(I18N_COMMON_TEXT.SEARCH_PARAMETERS)
  };

  const affinityNumberField: AttributeSearchData = {
    key: ATTRIBUTE_SEARCH_FIELD.AFFINITY_NUMBER,
    name: I18N_COMMON_TEXT.AFFINITY_NUMBER,
    description: t('txt_affinity_number_description'),
    component: props => (
      <InputControl
        placeholder={I18N_COMMON_TEXT.ENTER_KEYWORD}
        maxLength={16}
        mode="number"
        {...props}
      />
    ),
    groupName: t(I18N_COMMON_TEXT.SEARCH_PARAMETERS)
  };

  const firstNameField: AttributeSearchData = {
    key: ATTRIBUTE_SEARCH_FIELD.FIRST_NAME,
    name: I18N_COMMON_TEXT.FIRST_NAME,
    description: t('txt_first_name_description'),
    component: props => {
      return (
        <InputControl
          placeholder={I18N_COMMON_TEXT.ENTER_KEYWORD}
          mode="text"
          {...props}
        />
      );
    },
    disabledFields: {
      [ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER]: t(
        MESSAGE.NO_COMBINE_WITH_FIRST_NAME
      ),
      [ATTRIBUTE_SEARCH_FIELD.AFFINITY_NUMBER]: t(
        MESSAGE.NO_COMBINE_WITH_FIRST_NAME
      ),
      [ATTRIBUTE_SEARCH_FIELD.PHONE_NUMBER]: t(
        MESSAGE.NO_COMBINE_WITH_FIRST_NAME
      ),
      [ATTRIBUTE_SEARCH_FIELD.SSN]: t(MESSAGE.NO_COMBINE_WITH_FIRST_NAME)
    },
    groupName: t(I18N_COMMON_TEXT.SEARCH_PARAMETERS)
  };

  const lastNameField: AttributeSearchData = {
    key: ATTRIBUTE_SEARCH_FIELD.LAST_NAME,
    name: I18N_COMMON_TEXT.LAST_NAME,
    description: t('txt_last_name_description'),
    component: ({ value, ...props }) => {
      const defaultValue: MainSubConditionValue = {
        level: {
          ...defaultExactness!,
          text: t(defaultExactness?.text)
        },
        keyword: undefined
      };
      return (
        <MainSubConditionControl
          data={mainSubConditionData}
          placeholder={I18N_COMMON_TEXT.ENTER_KEYWORD}
          value={value ? value : defaultValue}
          autoFocusDropdownList={false}
          autoFocusInput
          {...props}
        />
      );
    },
    disabledFields: {
      [ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER]: t(
        MESSAGE.NO_COMBINE_WITH_LAST_NAME
      ),
      [ATTRIBUTE_SEARCH_FIELD.AFFINITY_NUMBER]: t(
        MESSAGE.NO_COMBINE_WITH_LAST_NAME
      ),
      [ATTRIBUTE_SEARCH_FIELD.PHONE_NUMBER]: t(
        MESSAGE.NO_COMBINE_WITH_LAST_NAME
      ),
      [ATTRIBUTE_SEARCH_FIELD.SSN]: t(MESSAGE.NO_COMBINE_WITH_LAST_NAME)
    },
    groupName: t(I18N_COMMON_TEXT.SEARCH_PARAMETERS)
  };

  const middleInitialField: AttributeSearchData = {
    key: ATTRIBUTE_SEARCH_FIELD.MIDDLE_INITIAL,
    name: I18N_COMMON_TEXT.MIDDLE_INITIAL,
    description: t('txt_middle_initial_description'),
    component: props => {
      return (
        <InputControl
          placeholder={I18N_COMMON_TEXT.ENTER_KEYWORD}
          mode="text"
          maxLength={1}
          {...props}
        />
      );
    },
    disabledFields: {
      [ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER]: t(
        MESSAGE.NO_COMBINE_WITH_MIDDLE_INITIAL
      ),
      [ATTRIBUTE_SEARCH_FIELD.AFFINITY_NUMBER]: t(
        MESSAGE.NO_COMBINE_WITH_MIDDLE_INITIAL
      ),
      [ATTRIBUTE_SEARCH_FIELD.PHONE_NUMBER]: t(
        MESSAGE.NO_COMBINE_WITH_MIDDLE_INITIAL
      ),
      [ATTRIBUTE_SEARCH_FIELD.SSN]: t(MESSAGE.NO_COMBINE_WITH_MIDDLE_INITIAL)
    },
    groupName: t(I18N_COMMON_TEXT.SEARCH_PARAMETERS)
  };

  const phoneNumberField: AttributeSearchData = {
    key: ATTRIBUTE_SEARCH_FIELD.PHONE_NUMBER,
    name: I18N_COMMON_TEXT.PHONE_NUMBER,
    description: t(I18N_COMMON_TEXT.TEN_DIGITS),
    component: props => (
      <MaskedTextBoxControl
        placeholder={t(MESSAGE.PHONE_NUMBER_PLACE_HOLDER)}
        mask={PHONE_MASK_INPUT_REGEX}
        {...props}
      />
    ),
    groupName: t(I18N_COMMON_TEXT.SEARCH_PARAMETERS)
  };

  const ssnField: AttributeSearchData = {
    key: ATTRIBUTE_SEARCH_FIELD.SSN,
    name: I18N_COMMON_TEXT.SSN_NAME,
    description: t(I18N_COMMON_TEXT.NINE_DIGITS),
    component: props => (
      <MaskedTextBoxControl
        placeholder={t(MESSAGE.SSN_PLACE_HOLDER)}
        mask={SSN_MASK_INPUT_REGEX}
        {...props}
      />
    ),
    groupName: t(I18N_COMMON_TEXT.SEARCH_PARAMETERS)
  };

  const stateField: AttributeSearchData = {
    key: ATTRIBUTE_SEARCH_FIELD.STATE,
    name: I18N_COMMON_TEXT.STATE,
    description: t(I18N_COMMON_TEXT.STATE_DESCRIPTION),
    component: props => (
      <ComboBoxControl
        placeholder={I18N_COMMON_TEXT.SELECT_AN_ITEM}
        {...props}
      />
    ),
    groupName: t(I18N_COMMON_TEXT.SEARCH_PARAMETERS)
  };

  const zipCodeField: AttributeSearchData = {
    key: ATTRIBUTE_SEARCH_FIELD.ZIP_CODE,
    name: I18N_COMMON_TEXT.ZIP_CODE,
    description: t(I18N_COMMON_TEXT.FIVE_DIGITS),
    component: props => (
      <InputControl
        placeholder={I18N_COMMON_TEXT.ENTER_KEYWORD}
        maxLength={5}
        mode="number"
        {...props}
      />
    ),
    groupName: t(I18N_COMMON_TEXT.SEARCH_PARAMETERS)
  };

  const constraintFields = [
    {
      field: ATTRIBUTE_SEARCH_FIELD.FIRST_NAME,
      requiredField: lastNameField,
      errorMessage: ''
    },
    {
      field: ATTRIBUTE_SEARCH_FIELD.MIDDLE_INITIAL,
      requiredField: lastNameField,
      errorMessage: ''
    },
    {
      field: ATTRIBUTE_SEARCH_FIELD.MIDDLE_INITIAL,
      requiredField: firstNameField,
      errorMessage: ''
    },
    {
      field: ATTRIBUTE_SEARCH_FIELD.STATE,
      requiredField: lastNameField,
      errorMessage: ''
    },
    {
      field: ATTRIBUTE_SEARCH_FIELD.ZIP_CODE,
      requiredField: lastNameField,
      errorMessage: ''
    }
  ];

  return {
    constraintFields,
    data: [
      accountNumberField,
      affinityNumberField,
      firstNameField,
      lastNameField,
      middleInitialField,
      phoneNumberField,
      ssnField,
      stateField,
      zipCodeField
    ]
  };
};
