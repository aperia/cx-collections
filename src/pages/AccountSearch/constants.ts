// components
import { NoCombineField } from 'app/_libraries/_dls/components';

// --------------------ATTRIBUTE SEARCH --------------------------//
import { MainSubConditionLevel } from 'app/_libraries/_dls/components';
import { ATTRIBUTE_SEARCH_FIELD } from './types';

export const MESSAGE = {
  INVALID_FORMAT: 'txt_invalid_format',
  SSN_NO_COMBINE: 'txt_ssn_cannot_be_combined_with_other_parameters',
  SSN_NAME: 'txt_ssn',
  PHONE_NAME: 'txt_phone_number',
  SSN_DESCRIPTION: 'txt_nine_digits',
  PHONE_DESCRIPTION: 'txt_ten_digits',
  ACCOUNT_NUMBER_SEARCH:
    'txt_account_number_cannot_be_combined_with_other_parameters',
  AFFINITY_NUMBER_SEARCH:
    'txt_affinity_number_cannot_be_combined_with_other_parameters',
  GROUP_NAME: 'txt_search_parameters',
  PHONE_NUMBER_PLACE_HOLDER: '(___) ___-____',
  SSN_PLACE_HOLDER: '___-__-____',
  PHONE_NUMBER_NO_COMBINE:
    'txt_phone_number_cannot_be_combined_with_other_parameters',
  NO_COMBINE_WITH_FIRST_NAME: 'txt_cannot_be_combined_with_first_name',
  NO_COMBINE_WITH_LAST_NAME: 'txt_cannot_be_combined_with_last_name',
  NO_COMBINE_WITH_MIDDLE_INITIAL: 'txt_cannot_be_combined_with_middle_initial',
  FIRST_NAME_REQUIRED: 'txt_first_name_is_required',
  LAST_NAME_REQUIRED: 'txt_last_name_is_required',
  MIDDLE_INITIAL_REQUIRED: 'txt_middle_initial_is_required',
  STATE_REQUIRED: 'txt_state_is_required',
  STATE: 'txt_state',
  STATE_DESCRIPTION: 'txt_tbd',
  ZIP_CODE: 'txt_zip_code',
  ZIP_CODE_DESCRIPTION: 'txt_five_digits',
  ZIP_CODE_REQUIRED: 'txt_zip_code_is_required'
};

export const mainSubConditionData: MainSubConditionLevel[] = [
  { key: 'E', text: 'txt_exact' },
  { key: 'M', text: 'txt_moderate' },
  { key: 'L', text: 'txt_low' }
];

export const defaultExactness = mainSubConditionData.find(
  item => item.key === 'M'
);

export const PHONE_MASK_INPUT_REGEX = [
  '(',
  /\d/,
  /\d/,
  /\d/,
  ')',
  ' ',
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  /\d/,
  /\d/
];

export const SSN_MASK_INPUT_REGEX = [
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  /\d/,
  /\d/
];

export const NO_COMBINE_FIELDS: NoCombineField[] = [
  {
    field: ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER,
    filterMessage: MESSAGE.ACCOUNT_NUMBER_SEARCH
  },
  {
    field: ATTRIBUTE_SEARCH_FIELD.AFFINITY_NUMBER,
    filterMessage: MESSAGE.AFFINITY_NUMBER_SEARCH
  },
  {
    field: ATTRIBUTE_SEARCH_FIELD.SSN,
    filterMessage: MESSAGE.SSN_NO_COMBINE
  },
  {
    field: ATTRIBUTE_SEARCH_FIELD.PHONE_NUMBER,
    filterMessage: MESSAGE.PHONE_NUMBER_NO_COMBINE
  }
];

export const ACC_SEARCH_OTHERS_PARAMS_SELECT_FIELDS = [
  'customerExternalIdentifier',
  'customerName',
  'presentationInstrumentIdentifier',
  'customerRoleTypeCode',
  'addressLineOne',
  'cityName',
  'memberSequenceIdentifier',
  'socialSecurityNumber',
  'clientIdentifier',
  'systemIdentifier',
  'principalIdentifier',
  'agentIdentifier'
];

export const ACC_SEARCH_ACC_PARAMS_SELECT_FIELDS = [
  'customerInquiry.accountIdentifier',
  'customerInquiry.customerExternalIdentifier',
  'customerInquiry.customerName',
  'customerInquiry.customerRoleTypeCode',
  'customerInquiry.primaryCustomerName',
  'customerInquiry.presentationInstrumentIdentifier',
  'customerInquiry.addressContinuationLine1Text',
  'customerInquiry.cityName',
  'customerInquiry.memberSequenceIdentifier',
  'customerInquiry.socialSecurityIdentifier',
  'customerInquiry.clientIdentifier',
  'customerInquiry.systemIdentifier',
  'customerInquiry.principalIdentifier',
  'customerInquiry.agentIdentifier'
];

// --------------------ATTRIBUTE SEARCH --------------------------//
