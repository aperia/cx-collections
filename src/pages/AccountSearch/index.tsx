import React from 'react';

// components
import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import AccountSearchList from 'pages/AccountSearch/AccountSearchList';

interface SearchResultListProps {
  searchParams: AttributeSearchValue[];
  storeId: string;
}

const SearchResultList: React.FC<SearchResultListProps> = ({
  searchParams,
  storeId
}) => {
  return <AccountSearchList searchParams={searchParams} storeId={storeId} />;
};

export default React.memo(SearchResultList);
