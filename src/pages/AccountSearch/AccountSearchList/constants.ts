export const SEARCH_LIST_VIEW_NAME = {
  BASIC_INFO: 'search_result_views',
  MORE_INFO: 'search_result_more_info_views'
};

export const GET_MORE_ACC_INFO_SELECT_FIELDS = [
  'customerInquiry.lifetimeDelinquentCycleCount',
  'customerInquiry.delinquentAmount',
  'customerInquiry.internalStatusCode',
  'customerInquiry.externalStatusCode',
  'customerInquiry.daysDelinquentCount'
];

export const NUMBER_RECORDS_PER_LOAD = 25;
export const DEFAULT_SEQUENCE = { startSequence: 1, endSequence: 25 };

export const NO_RESULT_CODE = '455';
export const ACCOUNT_IDENTIFY = 'accountIdentifier';
export const OTHER_IDENTIFY = 'presentationInstrumentIdentifier';
export const IDENTIFIER_INVALID_CODE = '0308';
