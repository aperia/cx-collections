import { AttributeSearchValue } from 'app/_libraries/_dls/components';

export interface AccountSearchListState {
  [storeId: string]: AccountSearchData;
}

export interface AccountSearchData {
  loading?: boolean;
  isLoadingRenewToken?: boolean;
  error?: boolean;
  data: AccountData[];
  isEnd?: boolean;
  showMoreRowIds?: AccountIdObject<boolean>;
  moreInfoData?: AccountIdObject<AccountMoreInfoData>;
}

export interface AccountMoreInfoData {
  loading?: boolean;
  data?: MoreInfo;
}

export interface MoreInfo {
  cycleCounts?: number;
  daysDelinquent?: number;
  delinquentAmount?: number;
  externalStatusCode?: string;
  internalStatusCode?: string;
}

export interface AccountData {
  accountId?: string;
  memberSequenceIdentifier?: string;
  socialSecurityIdentifier?: string;
  customerRoleTypeCode?: string;
  accNbr?: string | null;
  address?: {
    addressLineOne?: string;
    addressLineTwo?: string;
  };
  cityName?: string;
  id?: string;
  lastSSN?: string | null;
  primaryName?: string;
  secondaryName?: string;
  clientId?: string;
  clientIdentifier?: string;
  systemIdentifier?: string;
  principalIdentifier?: string;
  agentIdentifier?: string;
}

export interface AccountIdObject<T> {
  [accountId: string]: T;
}

export interface ShowMoreAccountPayload {
  storeId: string;
  accountId: string;
}

export interface IGetMoreAccountInfoPayload {
  data: {
    cycleCounts: number;
    delinquentAmount: number;
    internalStatusCode: string;
    externalStatusCode: string;
    daysDelinquent: number;
  };
}

export interface IGetMoreAccountInfoArgs {
  storeId: string;
  accountId: string;
}

export interface SearchAccountListRequestArgs {
  storeId: string;
  searchParams?: AttributeSearchValue[];
  startSequence?: number;
  endSequence?: number;
}

export interface GetMoreAccountInfoRequestArgs {
  storeId: string;
  accountId: string;
  accEValue: string;
}

export interface GetMoreAccountInfoRequestPayload {
  data: MagicKeyValue;
}

export interface SearchAccountListResponse {
  data: AccountData[];
}
