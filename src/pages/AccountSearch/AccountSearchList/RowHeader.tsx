import React, { useState, useEffect, useMemo, useCallback } from 'react';

// components
import {
  AttributeSearch,
  AttributeSearchValue,
  AttributeSearchEvent
} from 'app/_libraries/_dls/components';

// helper
import every from 'lodash.every';
import pick from 'lodash.pick';
import { getTabTitle } from 'app/helpers';
import { getAttributeSearchPlaceholder } from '../helpers';

// constant
import { NO_COMBINE_FIELDS } from '../constants';
import { attrValidations } from 'pages/AccountSearch/searchValidator';
import { getInitialAttrData } from 'pages/AccountSearch/attributeSearch';
import { ATTRIBUTE_SEARCH_FIELD } from 'pages/AccountSearch/types';
import { I18N_COMMON_TEXT as COMMON_TEXT } from 'app/constants/i18n';

// redux
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';
import { useDispatch, useSelector } from 'react-redux';

// redux-store
import { selectSearchResultsData } from 'pages/AccountSearch/AccountSearchList/_redux/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface SearchHeaderProps {
  storeId: string;
  searchValues: AttributeSearchValue[];
  onSearchValue: (searchValues: AttributeSearchValue[]) => void;
}

const RowHeader: React.FC<SearchHeaderProps> = ({
  storeId,
  onSearchValue,
  searchValues
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  // save attribute search value before press search button
  const [filterValue, setFilterValue] =
    useState<AttributeSearchValue[]>(searchValues);

  const searchResultsData = useSelector(selectSearchResultsData(storeId));
  const handleChange = (event: AttributeSearchEvent) => {
    const { value } = event;
    setFilterValue(value);
  };

  const handleSearch = useCallback(
    (event: AttributeSearchEvent, error?: Record<string, string>) => {
      if (error || !event.value.length) return;
      const newSearchValue = event.value.map(item =>
        pick(item, ['key', 'value'])
      );

      // update tab title
      const newTitle = t('txt_search_results_title', '', {
        title: getTabTitle(newSearchValue)
      });
      dispatch(
        actionsTab.updateTabPops({
          storeId: storeId,
          title: newTitle,
          props: {
            searchParams: newSearchValue,
            storeId
          }
        })
      );
      onSearchValue(newSearchValue);
    },
    [dispatch, onSearchValue, storeId, t]
  );

  const handleOnClear = () => {
    setFilterValue([]);
  };

  const totalTitle = () => {
    if (!searchResultsData || searchResultsData?.length === 0) return;
    return t(COMMON_TEXT.RESULT_FOUND, { count: searchResultsData.length });
  };

  const listNoCombineFields = useMemo(() => {
    const showFullListField = [
      ATTRIBUTE_SEARCH_FIELD.FIRST_NAME,
      ATTRIBUTE_SEARCH_FIELD.LAST_NAME,
      ATTRIBUTE_SEARCH_FIELD.MIDDLE_INITIAL
    ];

    const isEnableNoCombineFeature = every(
      filterValue,
      ({ key }) => !showFullListField.includes(key as ATTRIBUTE_SEARCH_FIELD)
    );

    return isEnableNoCombineFeature
      ? NO_COMBINE_FIELDS.map(item => ({
          ...item,
          filterMessage: t(item.filterMessage)
        }))
      : [];
  }, [filterValue, t]);

  useEffect(() => {
    setFilterValue(searchValues);
  }, [searchValues]);

  const { constraintFields, data } = useMemo(() => getInitialAttrData(t), [t]);

  return (
    <div id={`${storeId}-search-results`}>
      <div className="d-flex align-items-center justify-content-between pt-24">
        <h3
          data-testid={genAmtId(
            'accountSearchListHeader',
            'title',
            'RowHeader'
          )}
        >
          {t('txt_results_list')}
        </h3>
        <AttributeSearch
          id={`${storeId}__attributeSearch`}
          dataTestId="resultsListSearch"
          placeholder={t(
            getAttributeSearchPlaceholder(
              constraintFields,
              data,
              !!listNoCombineFields.length
            )(filterValue)
          )}
          data={data}
          noCombineFields={listNoCombineFields}
          value={filterValue}
          validator={(value: Record<string, any>) => attrValidations(value, t)}
          orderBy="asc"
          constraintFields={constraintFields}
          small
          onChange={handleChange}
          onSearch={handleSearch}
          onClear={handleOnClear}
          clearTooltipProps={{
            placement: 'top',
            containerClassName: 'tooltip-clear',
            element: t('txt_clear_all_criteria')
          }}
          noFilterResultText={t('txt_no_other_parameters_available')}
        />
      </div>
      {totalTitle && (
        <div className="mt-24">
          <span
            id={`${storeId}__totalTitle`}
            data-testid={genAmtId(
              'accountSearchListHeader',
              'totalTitle',
              'RowHeader'
            )}
          >
            {totalTitle()}
          </span>
        </div>
      )}
    </div>
  );
};

export default RowHeader;
