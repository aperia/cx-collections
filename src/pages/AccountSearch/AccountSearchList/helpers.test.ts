import { CustomerInquiry } from 'pages/AccountDetails/types';
import { ACC_SEARCH_ACC_PARAMS_SELECT_FIELDS } from '../constants';
import {
  cleanNumberString,
  mapSearchAttribute,
  handleMapDataSearchList
} from './helpers';
import isArray from 'lodash.isarray';

Object.defineProperty(String, 'replace', {
  value: null
});

describe('AccountSearchList helpers', () => {
  describe('cleanNumberString', () => {
    it('cleanNumberString', () => {
      const result = cleanNumberString('1231()');
      expect(result).toEqual('1231');
    });
  });

  describe('mapSearchAttribute', () => {
    const searchMock = [
      {
        key: 'accountNumber',
        value: { keyword: 'keyword', level: { key: 'key' } }
      },

      { key: 'lastName', value: { keyword: 'keyword', level: { key: 'key' } } },
      {
        key: 'firstName',
        value: { keyword: 'keyword', level: { key: 'key' } }
      },
      {
        key: 'middleInitial',
        value: { keyword: 'keyword', level: { key: 'key' } }
      },
      {
        key: 'phoneNumber',
        value: { keyword: 'keyword', level: { key: 'key' } }
      },
      { key: 'ssn', value: { keyword: 'keyword', level: { key: 'key' } } },
      { key: 'state', value: { keyword: 'keyword', level: { key: 'key' } } },
      { key: 'zipCode', value: { keyword: 'keyword', level: { key: 'key' } } },
      { key: '', value: { keyword: 'keyword', level: { key: 'key' } } }
    ];

    it('case switch', () => {
      const result = mapSearchAttribute(searchMock, 1, 1, '8997');
      expect(result).toEqual({
        selectFields: ACC_SEARCH_ACC_PARAMS_SELECT_FIELDS,
        odsNaming: false,
        searchFields: {
          enteredAccountId: {
            keyword: 'keyword',
            level: {
              key: 'key'
            }
          }
        },
        inquiryFields: {
          endSequence: 1,
          startSequence: 1
        }
      });
    });

    it('affinity number', () => {
      const result = mapSearchAttribute(
        [
          {
            key: 'affinityNumber',
            value: { keyword: 'keyword', level: { key: 'key' } }
          }
        ],
        1,
        1,
        '8997'
      );
      expect(result).toEqual({
        selectFields: ACC_SEARCH_ACC_PARAMS_SELECT_FIELDS,
        odsNaming: false,
        searchFields: {
          enteredAffinity: {
            keyword: 'keyword',
            level: {
              key: 'key'
            }
          }
        },
        inquiryFields: {
          endSequence: 1,
          startSequence: 1
        }
      });
    });

    it('case # case switch', () => {
      const result = mapSearchAttribute(
        [
          { key: '12355', value: { keyword: 'keyword', level: { key: 'key' } } }
        ],
        1,
        1,
        '8997'
      );
      expect(result).toEqual({
        odsNaming: false,
        searchFields: {
          clientIdentifier: '8997',
          endSequence: 1,
          startSequence: 1
        },
        selectFields: [
          'customerExternalIdentifier',
          'customerName',
          'presentationInstrumentIdentifier',
          'customerRoleTypeCode',
          'addressLineOne',
          'cityName',
          'memberSequenceIdentifier',
          'socialSecurityNumber',
          'clientIdentifier',
          'systemIdentifier',
          'principalIdentifier',
          'agentIdentifier'
        ]
      });
    });

    it('case empty search', () => {
      const result = mapSearchAttribute([], 1, 1, '8997');
      expect(result).toEqual({});
    });
  });

  describe('handleMapDataSearchList', () => {
    const data = [
      {
        addressLineOne: 'ADDRESS 123               ',
        cityName: 'DALLAS                   ',
        customerExternalIdentifier: 'C03098090011800710071012',
        customerName: 'SMITH,ADELE               ',
        customerRoleTypeCode: '01',
        memberSequenceIdentifier: '001',
        presentationInstrumentIdentifier:
          '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
        sequenceStartId:
          'C03098090011800710071012022009001002624    SMITH             ADELE              ',
        socialSecurityNumber:
          '{"maskedValue":"***-**-1111","eValue":"VOL(bGl2RGN1bmskIzlSLiMo)"}'
      },
      {
        addressLineOne: '                          ',
        cityName: '                         ',
        customerExternalIdentifier: 'C20273234913858500085026',
        customerName: 'SMITH,AMY  M              ',
        customerRoleTypeCode: '03',
        memberSequenceIdentifier: '004',
        presentationInstrumentIdentifier:
          '{"maskedValue":"022002******5078","eValue":"VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)"}',
        sequenceStartId:
          'C20273234913858500085026022002000915078    SMITH             AMY                ',
        socialSecurityNumber:
          '{"maskedValue":"***-**-2333","eValue":"VOL(KGs4Yj9FNE15Q3wtYUJ+)"}'
      }
    ];

    const data2 = [
      {
        addressLineOne: 'ADDRESS 123               ',
        cityName: 'DALLAS                   ',
        customerExternalIdentifier: 'C03098090011800710071012',
        customerName: 'SMITH,ADELE               ',
        customerRoleTypeCode: '01',
        memberSequenceIdentifier: '001',
        presentationInstrumentIdentifier:
          '{"maskedValue":"","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
        sequenceStartId:
          'C03098090011800710071012022009001002624    SMITH             ADELE              ',
        socialSecurityNumber: '{"eValue":"VOL(bGl2RGN1bmskIzlSLiMo)"}'
      },
      {
        addressLineOne: '                          ',
        cityName: '                         ',
        customerExternalIdentifier: 'C20273234913858500085026',
        customerName: 'SMITH,AMY  M              ',
        customerRoleTypeCode: '03',
        memberSequenceIdentifier: '004',
        presentationInstrumentIdentifier:
          '{"maskedValue":"","eValue":"VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)"}',
        sequenceStartId:
          'C20273234913858500085026022002000915078    SMITH             AMY                ',
        socialSecurityNumber: '{"eValue":"VOL(KGs4Yj9FNE15Q3wtYUJ+)"}'
      }
    ];

    const mapData = {
      accountId: 'presentationInstrumentIdentifier',
      // address: {
      //   addressLineOne: "addressLineOne",
      //   addressLineTwo: "cityName"
      // },
      cardNbr: 'presentationInstrumentIdentifier',
      cityName: 'cityName',
      customerExternalIdentifier: 'customerExternalIdentifier',
      customerRoleTypeCode: 'customerRoleTypeCode',
      id: 'customerExternalIdentifier',
      lastSSN: 'socialSecurityNumber',
      memberSequenceIdentifier: 'memberSequenceIdentifier',
      primaryName: 'customerName',
      secondaryName: 'secondaryName',
      socialSecurityIdentifier: 'socialSecurityNumber'
    };

    it('is Not Array', () => {
      const result = handleMapDataSearchList(
        {} as CustomerInquiry[],
        mapData,
        true
      );
      expect(result).toEqual([]);
    });

    it('is Error', () => {
      const result = handleMapDataSearchList(
        {} as CustomerInquiry[],
        {},
        false
      );
      expect(result).toEqual([]);
    });

    it('is Array with Mapping', () => {
      const result = handleMapDataSearchList(data, mapData, true);
      expect(result).toEqual([
        {
          accNbr: '022009******2624',
          accountId:
            '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
          cardNbr:
            '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
          cityName: 'DALLAS',
          customerExternalIdentifier: 'C03098090011800710071012',
          customerRoleTypeCode: '01',
          id: 'C03098090011800710071012',
          lastSSN: '1111',
          memberSequenceIdentifier: '001',
          primaryName: 'SMITH,ADELE',
          secondaryName: undefined,
          socialSecurityIdentifier:
            '{"maskedValue":"***-**-1111","eValue":"VOL(bGl2RGN1bmskIzlSLiMo)"}'
        }
      ]);
    });

    it('is Array with no Mapping', () => {
      const result = handleMapDataSearchList(
        data2,
        {
          accountId: '',
          lastSSN: 'socialSecurityNumber'
        },
        true
      );
      expect(isArray(result)).toEqual(true);
    });

    it('is Array with no Mapping 2', () => {
      const result = handleMapDataSearchList(
        data2,
        {
          accountId: '',
          lastSSN: ''
        },
        true
      );
      expect(isArray(result)).toEqual(true);
    });

    it('is Array', () => {
      const result = handleMapDataSearchList(data, mapData, false);
      expect(result).toEqual([
        {
          accNbr: '022009******2624',
          accountId:
            '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
          cardNbr:
            '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
          cityName: 'DALLAS',
          customerExternalIdentifier: 'C03098090011800710071012',
          customerRoleTypeCode: '01',
          id: 'C03098090011800710071012',
          lastSSN: '1111',
          memberSequenceIdentifier: '001',
          primaryName: 'SMITH,ADELE',
          secondaryName: undefined,
          socialSecurityIdentifier:
            '{"maskedValue":"***-**-1111","eValue":"VOL(bGl2RGN1bmskIzlSLiMo)"}'
        },
        {
          accNbr: '022002******5078',
          accountId:
            '{"maskedValue":"022002******5078","eValue":"VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)"}',
          cardNbr:
            '{"maskedValue":"022002******5078","eValue":"VOL(I0ohfE18YlUkL2hCb1RQPUE1LQ==)"}',
          cityName: '',
          customerExternalIdentifier: 'C20273234913858500085026',
          customerRoleTypeCode: '03',
          id: 'C20273234913858500085026',
          lastSSN: '2333',
          memberSequenceIdentifier: '004',
          primaryName: 'SMITH,AMY  M',
          socialSecurityIdentifier:
            '{"maskedValue":"***-**-2333","eValue":"VOL(KGs4Yj9FNE15Q3wtYUJ+)"}'
        }
      ]);
    });

    it('is Array with Mapping => throw error', () => {
      const result = handleMapDataSearchList(
        [
          {
            addressLineOne: 'ADDRESS 123               ',
            cityName: 'DALLAS                   ',
            customerExternalIdentifier: 'C03098090011800710071012',
            customerName: 'SMITH,ADELE               ',
            customerRoleTypeCode: '01',
            memberSequenceIdentifier: '001',
            presentationInstrumentIdentifier: '[1,2,3,4,]',
            sequenceStartId:
              'C03098090011800710071012022009001002624    SMITH             ADELE              ',
            socialSecurityNumber: '[1,2,3,4,]'
          },
          {
            addressLineOne: '                          ',
            cityName: '                         ',
            customerExternalIdentifier: 'C20273234913858500085026',
            customerName: 'SMITH,AMY  M              ',
            customerRoleTypeCode: '03',
            memberSequenceIdentifier: '004',
            presentationInstrumentIdentifier: '[1,2,3,4,]',
            sequenceStartId:
              'C20273234913858500085026022002000915078    SMITH             AMY                ',
            socialSecurityNumber: '[1,2,3,4,]'
          }
        ],
        mapData,
        false
      );
      expect(result).toEqual([]);
    });
  });
});
