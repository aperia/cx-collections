import isArray from 'lodash.isarray';
import isEmpty from 'lodash.isempty';

// Types
import { AccountData } from './types';
import { CustomerInquiry } from 'pages/AccountDetails/types';

// Helpers
import { mappingDataFromArray } from 'app/helpers';

// Constants
import {
  ACC_SEARCH_ACC_PARAMS_SELECT_FIELDS,
  ACC_SEARCH_OTHERS_PARAMS_SELECT_FIELDS
} from '../constants';
import {
  CUSTOMER_ROLE_CODE,
  // EMPTY_STRING_OBJECT,
  EMPTY_OBJECT,
  DASH,
  EMPTY_STRING,
  EMPTY_STRING_OBJECT
} from 'app/constants';
import { ACCOUNT_IDENTIFY, OTHER_IDENTIFY } from './constants';
import { ATTRIBUTE_SEARCH_FIELD } from '../types';

export const cleanNumberString = (str: string) =>
  String(str).replace(/[()\-\s]/g, '');

export const mapSearchAttribute = (
  searchParams: MagicKeyValue,
  startSequence: number,
  endSequence: number,
  clientIdentifier: string
) => {
  if (isEmpty(searchParams)) return {};
  let result: MagicKeyValue = {};
  const searchParamsMapped: Record<string, string | number> = {};
  searchParams.forEach((element: MagicKeyValue) => {
    switch (element.key) {
      case ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER:
        result = {
          selectFields: ACC_SEARCH_ACC_PARAMS_SELECT_FIELDS,
          odsNaming: false,
          searchFields: {
            enteredAccountId: element.value
          },
          inquiryFields: {
            startSequence,
            endSequence
          }
        };
        return;

      case ATTRIBUTE_SEARCH_FIELD.AFFINITY_NUMBER:
        result = {
          selectFields: ACC_SEARCH_ACC_PARAMS_SELECT_FIELDS,
          odsNaming: false,
          searchFields: {
            enteredAffinity: element.value
          },
          inquiryFields: {
            startSequence,
            endSequence
          }
        };
        return;
      case ATTRIBUTE_SEARCH_FIELD.LAST_NAME:
        searchParamsMapped['lastName'] = element.value.keyword;
        searchParamsMapped['levelCode'] = element.value.level.key;
        break;
      case ATTRIBUTE_SEARCH_FIELD.FIRST_NAME:
        searchParamsMapped['firstName'] = element.value;
        break;
      case ATTRIBUTE_SEARCH_FIELD.MIDDLE_INITIAL:
        searchParamsMapped['middleInitial'] = element.value;
        break;
      case ATTRIBUTE_SEARCH_FIELD.PHONE_NUMBER:
        searchParamsMapped['phoneNumber'] = cleanNumberString(element.value);
        break;
      case ATTRIBUTE_SEARCH_FIELD.SSN:
        searchParamsMapped['socialSecurityNumber'] = cleanNumberString(
          element.value
        );
        break;
      case ATTRIBUTE_SEARCH_FIELD.STATE:
        searchParamsMapped['stateCode'] = element.value.text;
        break;
      case ATTRIBUTE_SEARCH_FIELD.ZIP_CODE:
        searchParamsMapped['postalCode'] = element.value;
        break;
      default:
        break;
    }
  });

  if (!isEmpty(result)) return result;
  return {
    selectFields: ACC_SEARCH_OTHERS_PARAMS_SELECT_FIELDS,
    odsNaming: false,
    searchFields: {
      ...searchParamsMapped,
      clientIdentifier,
      startSequence,
      endSequence
    }
  };
};

export const handleMapDataSearchList = (
  customers: CustomerInquiry[],
  mappingData: Record<string, string>,
  isSearchByAccount?: boolean
) => {
  try {
    if (!isArray(customers)) return [];
    const dataArr = isSearchByAccount
      ? customers.filter(
          (item: CustomerInquiry) =>
            item.customerRoleTypeCode === CUSTOMER_ROLE_CODE.primary
        )
      : customers;

    const listCustomerMapped = mappingDataFromArray<AccountData>(
      dataArr,
      mappingData
    );

    listCustomerMapped.forEach(customerMapped => {
      const { maskedValue } = JSON.parse(
        customerMapped?.accountId || EMPTY_STRING_OBJECT
      ) as CommonIdentifier;

      const kyIdentify = isSearchByAccount ? ACCOUNT_IDENTIFY : OTHER_IDENTIFY;

      const { customerName } =
        customers.find(
          item =>
            item[kyIdentify] === customerMapped.accountId &&
            item.customerRoleTypeCode === CUSTOMER_ROLE_CODE.secondary
        ) || (EMPTY_OBJECT as CustomerInquiry);

      customerMapped.accNbr = maskedValue;
      customerMapped.secondaryName = customerName;

      if (customerMapped?.lastSSN?.length) {
        const { maskedValue: ssn = EMPTY_STRING } = JSON.parse(
          customerMapped?.lastSSN
        ) as CommonIdentifier;
        const [, , lastSSN = customerMapped.lastSSN] = ssn.split(DASH);
        customerMapped.lastSSN = lastSSN;
      }
    });

    return listCustomerMapped;
  } catch (e) {
    return [];
  }
};
