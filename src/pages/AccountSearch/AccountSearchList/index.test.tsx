import React from 'react';
import cloneDeep from 'lodash.clonedeep';
import { mockActionCreator, renderMockStoreId, storeId } from 'app/test-utils';
import AccountSearchList from './index';
import { fireEvent, screen } from '@testing-library/react';
import { searchResultsAction } from './_redux/reducers';
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';
import {
  AttributeSearchEvent,
  AttributeSearchProps
} from 'app/_libraries/_dls/components';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import intersectionObserver from 'app/test-utils/mocks/intersectionObserver';
import {
  mockClientHeight,
  mockScrollHeight
} from 'app/test-utils/mocks/mockProperty';
import { AppCaches } from 'app/_libraries/_dof/core/redux/states';
import { DOFAppState } from 'storeConfig';

const searchParams = [{ key: 'lastName', value: 'smith' }];

const initialState: Partial<DOFAppState> = {
  accountSearchList: {
    [storeId]: {
      data: [
        {
          accountId:
            '{"maskedValue":"433305******6612","eValue":"VOL(PSlFY3cvQ3prd2lbcVVaKCFjfQ==)"}',
          memberSequenceIdentifier: '001',
          socialSecurityIdentifier: '001',
          customerRoleTypeCode: '01',
          accNbr: '11',
          address: {
            addressLineOne: 'addressLineOne',
            addressLineTwo: 'addressLineTwo'
          },
          cityName: 'cityName',
          id: 'id',
          lastSSN: '0001',
          primaryName: 'primaryName',
          secondaryName: 'secondaryName',
          clientId: 'clientId'
        },
        {
          accountId:
            '{"maskedValue":"433305******6613","eValue":"VOL(PSlFY3cvQ3prd2lbcVVaKCFjfP==)"}',
          memberSequenceIdentifier: '001',
          socialSecurityIdentifier: '001',
          customerRoleTypeCode: '01',
          accNbr: '11',
          address: {
            addressLineOne: 'addressLineOne',
            addressLineTwo: 'addressLineTwo'
          },
          cityName: 'cityName',
          id: 'id',
          lastSSN: '0001',
          primaryName: 'primaryName',
          secondaryName: 'secondaryName',
          clientId: 'clientId'
        }
      ],
      loading: true,
      isEnd: true,
      error: false
    }
  },
  caches: {
    views: {
      search_result_views: {
        name: 'search_result_views'
      }
    },
    controls: {
      GroupTextControl: (<>text</>) as unknown as React.ComponentType
    }
  } as AppCaches
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(
    <AccountSearchList storeId={storeId} searchParams={searchParams} />,
    { initialState }
  );
};

const searchResultsActionSpy = mockActionCreator(searchResultsAction);
const actionSpy = mockActionCreator(actionsTab);

const attributeSearchEvent = {
  value: [
    {
      key: 'lastName',
      value: 'smith'
    }
  ],
  valueMap: {
    lastName: {
      key: 'lastName',
      name: 'txt_last_name',
      description: 'txt_last_name_description',
      component: () => null,
      disabledFields: {},
      groupName: 'txt_search_parameters',
      value: 'smith'
    }
  }
} as AttributeSearchEvent;

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');

  return {
    ...dlsComponents,
    AttributeSearch: (props: AttributeSearchProps) => (
      <>
        <div data-testid="AttributeSearch_value">
          {JSON.stringify(props.value)}
        </div>
        <button
          data-testid="AttributeSearch_onClear"
          onClick={() => props.onClear!()}
        />
        <button
          data-testid="AttributeSearch_onSearch"
          onClick={(e: any) =>
            props.onSearch!(attributeSearchEvent, e.target.error)
          }
        />
        <button
          data-testid="AttributeSearch_onChange"
          onClick={(e: any) => props.onChange!(attributeSearchEvent)}
        />
      </>
    )
  };
});

describe('Test AccountSearchList component', () => {
  beforeEach(() => {
    HTMLCanvasElement.prototype.getContext = () => null;
    searchResultsActionSpy('searchAccountsRequest');
  });

  it('should render component with Error', () => {
    renderWrapper({
      accountSearchList: {
        [storeId]: {
          data: [],
          error: true
        }
      }
    });
    expect(screen.getByText('txt_results_list')).toBeInTheDocument();
  });

  it('should render component', () => {
    const { wrapper } = renderWrapper(initialState);
    expect(wrapper.getByText('txt_results_list')).toBeInTheDocument();
  });

  it('should handleReloadButton', () => {
    const spy = searchResultsActionSpy('showLoading');
    const { wrapper } = renderWrapper({
      accountSearchList: {
        [storeId]: {
          data: [
            {
              accountId:
                '{"maskedValue":"433305******6612","eValue":"VOL(PSlFY3cvQ3prd2lbcVVaKCFjfQ==)"}',
              memberSequenceIdentifier: '001',
              socialSecurityIdentifier: '001',
              customerRoleTypeCode: '01',
              accNbr: '11',
              address: {
                addressLineOne: 'addressLineOne',
                addressLineTwo: 'addressLineTwo'
              },
              cityName: 'cityName',
              id: 'id',
              lastSSN: '0001',
              primaryName: 'primaryName',
              secondaryName: 'secondaryName',
              clientId: 'clientId'
            }
          ],
          loading: false,
          isEnd: true,
          error: true
        }
      }
    });
    const buttonReload = wrapper.getAllByText(I18N_COMMON_TEXT.RELOAD)[0];
    fireEvent.click(buttonReload);
    expect(spy).toHaveBeenCalledWith({
      storeId
    });
  });

  it('should handleSearchValues', () => {
    actionSpy('updateTabPops');
    window.HTMLElement.prototype.scroll = jest.fn();
    const { wrapper } = renderWrapper(initialState);

    const searchButton = wrapper.getByTestId('AttributeSearch_onSearch');

    fireEvent.click(searchButton);
  });

  it('should handleRenderNoResults', () => {
    renderWrapper({
      accountSearchList: {
        [storeId]: {
          data: [],
          error: false
        }
      }
    });
    expect(screen.getByText('txt_results_list')).toBeInTheDocument();
  });

  it('should handleLoadMore', () => {
    Object.defineProperty(window, 'IntersectionObserver', {
      writable: true,
      configurable: true,
      value: intersectionObserver({})
    });

    mockScrollHeight(100);
    mockClientHeight(90);

    const _initialState = cloneDeep(initialState);
    _initialState.accountSearchList![storeId].isEnd = false;

    const { wrapper } = renderWrapper(_initialState);

    expect(wrapper).toBeTruthy();
  });
});
