import React from 'react';
import { screen, act } from '@testing-library/react';
import CardRow from './CardRow';
import {
  mockActionCreator,
  queryByClass,
  renderMockStore,
  renderMockStoreId,
  StoreConfigProps,
  storeId
} from 'app/test-utils';
import { searchResultsAction } from './_redux/reducers';
import * as accountDetailActions from './_redux/openAccountDetail';
import cloneDeep from 'lodash.clonedeep';
import { AccountData } from './types';

const accountData = {
  accountId:
    '{"maskedValue":"433305******6612","eValue":"VOL(PSlFY3cvQ3prd2lbcVVaKCFjfQ==)"}',
  accNbr: '093725•••••••3448',
  address: {
    addressLineOne: 'Address Line One',
    addressLineTwo: 'Address Line Two'
  },
  memberSequenceIdentifier: '001',
  cityName: 'City Name',
  id: '093725•••••••3448',
  lastSSN: '5954',
  primaryName: 'Primary Name',
  secondaryName: 'Secondary Name',
  socialSecurityIdentifier: '1'
};

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

const searchResultsActionSpy = mockActionCreator(searchResultsAction);

const mockAccountDetailActions = mockActionCreator(accountDetailActions);

describe('CardRow component', () => {
  it('should not render component when empty data', () => {
    const { wrapper } = renderMockStoreId(<CardRow id="test" data={{}} />);
    const bodyEle = wrapper.container.querySelector('body');
    expect(bodyEle?.innerHTML).toEqual(undefined);
  });

  it('should render component when having data', () => {
    renderMockStoreId(<CardRow id="test" data={accountData} />);

    expect(screen.getByText('Primary Name')).toBeInTheDocument();
    expect(screen.getByText('txt_show_more')).toBeInTheDocument();
  });

  it('should click show more', () => {
    const updateShowMoreRowIdsSpy = searchResultsActionSpy(
      'updateShowMoreRowIds'
    );
    const getMoreAccountInfoRequest = searchResultsActionSpy(
      'getMoreAccountInfoRequest'
    );

    const _accountData = cloneDeep(accountData);
    _accountData.accountId = '';
    _accountData.accNbr = '';
    _accountData.id = '';
    const initialState = {
      accountSearchList: {
        [storeId]: {
          data: [] as AccountData[]
        }
      },
      draftState: {
        [storeId]: {}
      }
    } as StoreConfigProps;

    const { wrapper } = renderMockStoreId(
      <CardRow id="test" data={_accountData} />,
      initialState
    );

    const btn = wrapper.getByText('txt_show_more');
    btn.click();

    expect(updateShowMoreRowIdsSpy).toBeCalled();
    expect(getMoreAccountInfoRequest).toHaveBeenCalled();
  });

  it('handleOpenModalCallerAuthentication', () => {
    const mOpenAccountDetail = mockAccountDetailActions('openAccountDetail');
    const _accountData = cloneDeep(accountData);
    _accountData.accountId = '';
    const { wrapper } = renderMockStoreId(
      <CardRow id="test" data={_accountData} />
    );

    const button = queryByClass(wrapper.container, 'card-primary');
    button?.parentElement?.click();

    expect(mOpenAccountDetail).toBeCalled();
  });

  it('renderShowMoreInfoSection', () => {
    searchResultsActionSpy('updateShowMoreRowIds');
    searchResultsActionSpy('getMoreAccountInfoRequest');

    const initialState: Partial<RootState> = {
      accountSearchList: {
        [storeId]: {
          moreInfoData: {
            [accountData.accNbr]: {
              loading: true,
              data: {
                cycleCounts: 500
              }
            }
          },
          data: [] as AccountData[]
        }
      }
    };

    const { wrapper } = renderMockStore(
      <CardRow id="test" data={accountData} storeId={storeId} />,
      { initialState }
    );

    const btn = wrapper.getByText('txt_show_more');
    act(() => {
      btn.click();
    });

    expect(wrapper.getByText('txt_load_more_information')).toBeInTheDocument();
  });
});
