import React, { useEffect, useState, useCallback } from 'react';
import isEmpty from 'lodash.isempty';

// components
import { View } from 'app/_libraries/_dof/core';
import { Button } from 'app/_libraries/_dls/components';

// redux store
import { useDispatch, useSelector } from 'react-redux';
import { searchResultsAction } from 'pages/AccountSearch/AccountSearchList/_redux/reducers';
import {
  selectShowMoreRowIds,
  selectMoreLoading,
  selectMoreInfoData
} from 'pages/AccountSearch/AccountSearchList/_redux/selectors';
import { openAccountDetail } from './_redux/openAccountDetail';

// Types
import { AccountData } from 'pages/AccountSearch/AccountSearchList/types';

// constants and helpers
import { SEARCH_LIST_VIEW_NAME } from './constants';
import { EMPTY_STRING_OBJECT } from 'app/constants';
import { WorkType } from 'pages/TimerWorking/types';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface CardRowProps {
  data: AccountData;
  id?: string;
  storeId?: string;
  dataTestId?: string;
}

const CardRow: React.FC<CardRowProps> = ({
  id,
  dataTestId,
  storeId = '',
  data
}) => {
  const { t } = useTranslation();
  const [showMore, setShowMore] = useState<boolean>(false);

  const accountId = data?.accNbr || '';

  const customerExternalIdentifier = data?.id || '';
  const dispatch = useDispatch();

  const loading = useSelector(selectMoreLoading(storeId, accountId));

  const moreInfoData = useSelector(selectMoreInfoData(storeId, accountId));
  const showMoreRowIds = useSelector(selectShowMoreRowIds(storeId));

  const handleToggle = useCallback(
    (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
      event.stopPropagation();
      setShowMore(!showMore);
      dispatch(
        searchResultsAction.updateShowMoreRowIds({
          storeId,
          accountId: customerExternalIdentifier
        })
      );
      if (!showMore && isEmpty(moreInfoData)) {
        dispatch(
          searchResultsAction.getMoreAccountInfoRequest({
            storeId,
            accountId,
            accEValue: JSON.parse(data?.accountId || '{}')?.eValue
          })
        );
      }
    },
    [
      showMore,
      dispatch,
      storeId,
      accountId,
      moreInfoData,
      data,
      customerExternalIdentifier
    ]
  );

  const renderHeadingInfo = () => {
    const { primaryName } = data;
    return (
      <div className="d-flex align-items-center justify-content-between">
        <h5
          id={`${id}__primary-name`}
          data-testid={genAmtId(dataTestId!, 'primary-name', 'CardRow')}
        >
          {primaryName}
        </h5>
        <div className="ml-auto mr-n8">
          <Button
            id={`${id}__show-more-btn`}
            dataTestId={genAmtId(dataTestId!, 'show-more-btn', 'CardRow')}
            className="btn-sm"
            variant="outline-primary"
            onClick={handleToggle}
          >
            {t(showMore ? 'txt_show_less' : 'txt_show_more')}
          </Button>
        </div>
      </div>
    );
  };

  const renderShowMoreInfoSection = () => {
    return (
      <>
        {loading && (
          <div className="col-12 d-flex justify-content-center pt-16">
            <span className="loading loading-sm mr-16" />
            <span
              data-testid={genAmtId(dataTestId!, 'loading-more', 'CardRow')}
            >
              {t('txt_load_more_information')}
            </span>
          </div>
        )}
        {showMore && !isEmpty(moreInfoData) && (
          <View
            id={`${id}__${SEARCH_LIST_VIEW_NAME.MORE_INFO}`}
            dataTestId={genAmtId(
              dataTestId!,
              SEARCH_LIST_VIEW_NAME.MORE_INFO,
              'CardRow'
            )}
            formKey={`${id}__${SEARCH_LIST_VIEW_NAME.MORE_INFO}_view`}
            descriptor={SEARCH_LIST_VIEW_NAME.MORE_INFO}
            value={moreInfoData}
          />
        )}
      </>
    );
  };

  const handleOpenModalCallerAuthentication = () => {
    const {
      primaryName,
      memberSequenceIdentifier,
      socialSecurityIdentifier,
      accountId: currentAccountId,
      clientIdentifier,
      systemIdentifier,
      principalIdentifier,
      agentIdentifier
    } = data;
    const { eValue } = JSON.parse(
      currentAccountId || EMPTY_STRING_OBJECT
    ) as CommonIdentifier;
    const { maskedValue } = JSON.parse(
      currentAccountId || EMPTY_STRING_OBJECT
    ) as CommonIdentifier;

    dispatch(
      openAccountDetail({
        eValue,
        primaryName,
        memberSequenceIdentifier,
        maskedValue,
        socialSecurityIdentifier,
        clientIdentifier,
        systemIdentifier,
        principalIdentifier,
        agentIdentifier,
        type: WorkType.ACCOUNT_SEARCH
      })
    );
  };

  useEffect(() => {
    if (showMoreRowIds[customerExternalIdentifier] !== showMore) {
      setShowMore(showMoreRowIds[customerExternalIdentifier]);
    }
  }, [showMoreRowIds, accountId, showMore, customerExternalIdentifier]);

  return !isEmpty(data) ? (
    <div
      data-testid={genAmtId(dataTestId!, 'container', 'CardRow')}
      onClick={handleOpenModalCallerAuthentication}
    >
      <div className="card-primary">
        <div className="card-item p-16">
          {renderHeadingInfo()}
          <View
            id={id}
            dataTestId={genAmtId(
              dataTestId!,
              SEARCH_LIST_VIEW_NAME.BASIC_INFO,
              'CardRow'
            )}
            formKey={`${id}__${SEARCH_LIST_VIEW_NAME.BASIC_INFO}_view`}
            descriptor={SEARCH_LIST_VIEW_NAME.BASIC_INFO}
            value={data}
          />
          {renderShowMoreInfoSection()}
        </div>
      </div>
    </div>
  ) : null;
};

export default CardRow;
