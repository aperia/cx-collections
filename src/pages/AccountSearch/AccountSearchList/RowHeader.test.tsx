import React from 'react';
import RowHeader from './RowHeader';
import { screen } from '@testing-library/react';
import {
  AttributeSearchEvent,
  AttributeSearchProps
} from 'app/_libraries/_dls/components';
import { renderMockStoreId, storeId } from 'app/test-utils';

jest.mock('react-redux', () => {
  return {
    ...(jest.requireActual('react-redux') as object),
    useDispatch: () => (x: Function) => jest.fn().mockImplementation(() => x())
  };
});

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => () => ({
  t: jest.fn((x, options) => {
    if (options?.count > 1) return `${x}_plural`;
    return x;
  })
}));

jest.mock('app/_libraries/_dls/components', () => {
  return {
    ...jest.requireActual('app/_libraries/_dls/components'),
    AttributeSearch: (props: AttributeSearchProps) => {
      return (
        <>
          <div onClick={() => props.onClear!()}>onClear</div>
          <div
            onClick={() =>
              props.onChange!({ value: {} } as AttributeSearchEvent)
            }
          >
            onChange
          </div>
          <div
            onClick={() =>
              props.onChange!({
                value: [{ key: 'firstName', value: '1' }]
              } as AttributeSearchEvent)
            }
          >
            onChangeValue
          </div>
          <div
            onClick={() =>
              props.onSearch!({
                value: [{ key: 'lastName', value: 'a' }]
              } as AttributeSearchEvent)
            }
          >
            onSearch
          </div>
          <div
            onClick={() =>
              props.onSearch!({
                value: {}
              } as AttributeSearchEvent)
            }
          >
            onSearchError
          </div>
          <div
            onClick={() =>
              props.validator!({
                value: {}
              } as AttributeSearchEvent)
            }
          >
            onValidator
          </div>
        </>
      );
    }
  };
});

describe('RowHeader component', () => {
  it('should render component with result text', () => {
    renderMockStoreId(
      <RowHeader
        storeId={storeId}
        onSearchValue={() => {}}
        searchValues={[]}
      />,
      {
        initialState: {
          accountSearchList: {
            [storeId]: {
              data: [
                {
                  accountId: '',
                  memberSequenceIdentifier: '',
                  socialSecurityIdentifier: '',
                  customerRoleTypeCode: '',
                  accNbr: ''
                }
              ]
            }
          }
        }
      }
    );

    const onSearchEle = screen.getByText('onSearchError');
    onSearchEle.click();

    const onValidator = screen.getByText('onValidator');
    onValidator.click();

    expect(screen.getByText('txt_result_found')).toBeInTheDocument();
  });

  it('should not render result', () => {
    const { wrapper } = renderMockStoreId(
      <RowHeader storeId={storeId} onSearchValue={() => {}} searchValues={[]} />
    );

    const onClearEle = screen.getByText('onClear');
    onClearEle.click();

    const onChangeEle = screen.getByText('onChange');
    onChangeEle.click();

    const onSearchEle = screen.getByText('onSearch');
    onSearchEle.click();

    const spanEle = wrapper.container.querySelector(
      'span[class="123456789__totalTitle"]'
    );
    expect(spanEle?.innerHTML).toBeUndefined();
  });

  it('run for coverage', () => {
    const { wrapper } = renderMockStoreId(
      <RowHeader storeId={storeId} onSearchValue={() => {}} searchValues={[]} />
    );
    const onChangeEle = screen.getByText('onChangeValue');
    onChangeEle.click();

    const spanEle = wrapper.container.querySelector(
      'span[class="123456789__totalTitle"]'
    );
    expect(spanEle?.innerHTML).toBeUndefined();
  });
});
