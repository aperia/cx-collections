import searchListService from './searchListService';

// utils
import { storeId, mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// types
import { GET_MORE_ACC_INFO_SELECT_FIELDS } from './constants';

describe('searchListService', () => {
  describe('getMoreAccountInfo', () => {
    const params = storeId;

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      searchListService.getMoreAccountInfo(params);

      expect(mockService).toBeCalledWith('', {
        odsNaming: false,
        searchFields: {
          enteredAccountId: params
        },
        selectFields: GET_MORE_ACC_INFO_SELECT_FIELDS
      });
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      searchListService.getMoreAccountInfo(params);

      expect(mockService).toBeCalledWith(apiUrl.account.searchAccount, {
        odsNaming: false,
        searchFields: {
          enteredAccountId: params
        },
        selectFields: GET_MORE_ACC_INFO_SELECT_FIELDS
      });
    });
  });

  describe('getSearchAccounts', () => {
    const params: MagicKeyValue = {
      customerInquiry: 'id',
      customers: [{ customer: 'customerId' }],
      cardholderInfo: {}
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      searchListService.getSearchAccounts(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      searchListService.getSearchAccounts(params);

      expect(mockService).toBeCalledWith(apiUrl.account.searchAccount, params);
    });
  });
});
