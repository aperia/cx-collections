import React, { useState, useEffect, useCallback } from 'react';
import {
  AttributeSearchValue,
  SimpleBar
} from 'app/_libraries/_dls/components';

// components
import {
  NoResult,
  FailedApiReload as FailedApi,
  LoadMore
} from 'app/components';
import CardRow from './CardRow';
import RowHeader from './RowHeader';

// helpers
import isEmpty from 'lodash.isempty';
import classnames from 'classnames';
import { DEFAULT_SEQUENCE, NUMBER_RECORDS_PER_LOAD } from './constants';

// redux store
import { useDispatch, useSelector } from 'react-redux';
import { searchResultsAction } from './_redux/reducers';
import {
  selectLoading,
  selectIsError,
  selectSearchResultsData,
  selectIsEndList
} from './_redux/selectors';

// types
import { DOFAppState } from 'storeConfig';
import { selectAuthenConfigLoading } from 'pages/CallerAuthenticate/_redux/selectors';

export interface SearchResultListProps {
  searchParams: AttributeSearchValue[];
  storeId: string;
}
export interface SearchSequenceParams {
  startSequence: number;
  endSequence: number;
}

const AccountSearchList: React.FC<SearchResultListProps> = ({
  searchParams: initialSearchValues,
  storeId
}) => {
  const dispatch = useDispatch();

  // attribute search values after press Search button
  const [searchFilter, setSearchFilter] = useState({
    searchValues: initialSearchValues,
    searchSequence: DEFAULT_SEQUENCE
  });

  const loading: boolean = useSelector(selectLoading(storeId));
  const isEndList: boolean = useSelector(selectIsEndList(storeId));
  const isError: boolean = useSelector(selectIsError(storeId));
  const searchResultsData = useSelector(selectSearchResultsData(storeId));
  const isDOFRegistered = useSelector<DOFAppState, boolean>(state => {
    return (
      (isEmpty(state.caches.views['search_result_views']) ||
        !state.caches.controls['GroupTextControl']) &&
      !isEmpty(searchResultsData)
    );
  });
  const openAccLoading = useSelector(selectAuthenConfigLoading);

  const isFirstLoad =
    searchFilter.searchSequence.startSequence ===
    DEFAULT_SEQUENCE.startSequence;

  const isEmptyResult = isEmpty(searchResultsData) && !loading;

  const getSearchAccounts = useCallback(() => {
    const { startSequence, endSequence } = searchFilter.searchSequence;
    dispatch(
      searchResultsAction.searchAccountsRequest({
        storeId,
        startSequence,
        endSequence,
        searchParams: searchFilter.searchValues
      })
    );
  }, [dispatch, searchFilter, storeId]);

  const handleLoadMore = () => {
    const endSequence = searchFilter.searchSequence.endSequence;
    setSearchFilter(filter => ({
      ...filter,
      searchSequence: {
        startSequence: endSequence + 1,
        endSequence: endSequence + NUMBER_RECORDS_PER_LOAD
      }
    }));
  };

  const handleSearchValues = (values: AttributeSearchValue[]) => {
    setSearchFilter({ searchValues: values, searchSequence: DEFAULT_SEQUENCE });
  };

  const handleReloadButton = () => {
    dispatch(
      searchResultsAction.showLoading({
        storeId
      })
    );

    getSearchAccounts();
  };

  const handleOnLoadMore = () => {
    !isError && handleLoadMore();
  };

  const handleRenderRows = () => {
    return searchResultsData.map((item, index) => {
      const { id } = item;
      const isLastRow = index === searchResultsData.length - 1;
      const domId = `${storeId}_${id}`;

      return (
        <div key={`${domId}-${index}`} id={`${domId}_container`}>
          <div className={classnames({ 'pb-16': !isLastRow })}>
            <CardRow
              id={`${domId}_item`}
              storeId={storeId}
              data={item}
              dataTestId={`${index}-item_accountSearchList`}
            />
          </div>
          {isLastRow && !isError && (
            <div className="pb-16">
              <LoadMore
                dataTestId="accountSearchList-load-more"
                loading={
                  loading &&
                  searchFilter.searchSequence.startSequence >
                    DEFAULT_SEQUENCE.startSequence
                }
                onLoadMore={handleOnLoadMore}
                isEnd={isEndList}
              />
            </div>
          )}
          {isLastRow && isError && (
            <FailedApi
              id={`${storeId}-account-search-list-failed`}
              dataTestId="accountSearchList-failed"
              className="my-24"
              onReload={handleReloadButton}
            />
          )}
        </div>
      );
    });
  };

  const handleRenderFailedAPITreatmentAtTheFirstLoad = () => {
    return (
      isError &&
      isFirstLoad && (
        <FailedApi
          id="account-search-list-failed-bottom"
          onReload={getSearchAccounts}
          className="mt-160"
          dataTestId="accountSearchList-failed-firstLoad"
        />
      )
    );
  };

  const handleRenderNoResults = () => {
    return (
      !isError && isEmptyResult && <NoResult id={`${storeId}_No_Result`} />
    );
  };

  // handle fetching data when changing sequence and search Params
  useEffect(() => {
    getSearchAccounts();
  }, [dispatch, getSearchAccounts, storeId]);

  return (
    <div
      className={classnames('h-100', {
        loading: (isFirstLoad && loading) || isDOFRegistered || openAccLoading
      })}
    >
      <SimpleBar>
        <div className="px-24">
          <RowHeader
            storeId={storeId}
            searchValues={searchFilter.searchValues}
            onSearchValue={handleSearchValues}
          />
          <div className="mt-16">{!isEmptyResult && handleRenderRows()}</div>
        </div>

        {handleRenderFailedAPITreatmentAtTheFirstLoad()}

        {handleRenderNoResults()}
      </SimpleBar>
    </div>
  );
};

export default AccountSearchList;
