import { storeId, accEValue } from 'app/test-utils';
import { AccountSearchListState } from '../types';
import { searchResultsAction, reducer } from './reducers';

const initialState: AccountSearchListState = {
  [storeId]: {
    data: []
  }
};

const sampleState: AccountSearchListState = {
  [storeId]: {
    data: [],
    showMoreRowIds: {
      [accEValue]: true
    }
  }
};

const emptyState: AccountSearchListState = {};

const { removeStore, updateShowMoreRowIds, showLoading } = searchResultsAction;

describe('AccountSearchList reducer', () => {
  describe('removeStore action', () => {
    it('remove storeId object in store', () => {
      const state = reducer(initialState, removeStore({ storeId }));
      expect(state[storeId]).toBeUndefined();
    });
  });

  describe('updateShowMoreRowIds action', () => {
    it('should add new accountId = true into showMoreRowIds object', () => {
      const state = reducer(
        initialState,
        updateShowMoreRowIds({ storeId, accountId: accEValue })
      );
      expect(state[storeId].showMoreRowIds![accEValue]).toEqual(true);
    });

    it('should add new accountId = false into showMoreRowIds object', () => {
      const state = reducer(
        sampleState,
        updateShowMoreRowIds({ storeId, accountId: accEValue })
      );
      expect(state[storeId].showMoreRowIds![accEValue]).toEqual(false);
    });
  });

  describe('showLoading action', () => {
    it('not return loading when not having storeId', () => {
      const state = reducer(emptyState, showLoading({ storeId }));
      expect(state[storeId]).toBeUndefined();
    });

    it('return loading = true when having storeId', () => {
      const state = reducer(initialState, showLoading({ storeId }));
      expect(state[storeId].loading).toEqual(true);
    });
  });
});
