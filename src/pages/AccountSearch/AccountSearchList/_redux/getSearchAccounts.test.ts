import { mockActionCreator, responseDefault, storeId } from 'app/test-utils';
import searchListService from '../searchListService';
import { searchAccountsRequest } from './getSearchAccounts';
import { rootReducer } from 'storeConfig';
import { createStore, Store } from 'redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { SearchAccountListResponse } from '../types';

let spy: jest.SpyInstance;
let spy2: jest.SpyInstance;

const messageError = JSON.stringify({ message: 'test', resultCode: '0308' });

describe('searchAccountsRequest', () => {
  let store: Store<RootState>;

  beforeEach(() => {
    store = createStore(rootReducer, {
      mapping: {},
      accessConfig: {
        cspaList: [{ cspa: '9999-1000-2000-3000' }]
      }
    });
  });

  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();

    spy2?.mockReset();
    spy2?.mockRestore();
  });

  const mockToastAction = mockActionCreator(actionsToast);

  const mockApiService = (response?: MagicKeyValue) => {
    spy = jest.spyOn(searchListService, 'getSearchAccounts').mockResolvedValue({
      ...responseDefault,
      ...response
    });
  };

  const mockApiServiceError = (status?: string, data?: MagicKeyValue) => {
    spy = jest.spyOn(searchListService, 'getSearchAccounts').mockRejectedValue({
      ...data
    });
  };

  it('should return empty data', async () => {
    mockApiService();

    const response = await searchAccountsRequest({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'accountSearchList/searchAccountList/fulfilled'
    );
    expect(response?.payload).toEqual({ data: [] });
  });

  it('should return empty data > api return empty customers', async () => {
    mockApiService({ data: { customers: [] } });

    const response = await searchAccountsRequest({ storeId, startSequence: 1 })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'accountSearchList/searchAccountList/fulfilled'
    );
    expect(response?.payload).toEqual({ data: [] });
  });

  it('should return empty data > api return empty customerInquiry', async () => {
    mockApiService({ data: { customerInquiry: [] } });

    const response = await searchAccountsRequest({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'accountSearchList/searchAccountList/fulfilled'
    );
    expect(response?.payload).toEqual({ data: [] });
  });

  it('should return data', async () => {
    mockApiService({
      data: {
        customers: [
          {
            lastName: 'Smith',
            accountIdentifier: JSON.stringify({ maskedValue: '1' }),
            presentationInstrumentIdentifier: JSON.stringify({
              maskedValue: '1'
            }),
            customerRoleTypeCode: '02',
            customerName: 'secondary'
          }
        ]
      }
    });

    const store = createStore(rootReducer, {
      mapping: {
        data: {
          accountSearchListOtherFields: {
            lastName: 'lastName',
            accountId: 'accountIdentifier',
            customerRoleTypeCode: 'customerRoleTypeCode'
          }
        }
      }
    });
    const response = await searchAccountsRequest({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'accountSearchList/searchAccountList/fulfilled'
    );
    expect(response.payload).toEqual({
      data: [
        {
          lastName: 'Smith',
          accNbr: '1',
          accountId: JSON.stringify({ maskedValue: '1' }),
          customerRoleTypeCode: '02',
          secondaryName: 'secondary'
        }
      ]
    });
  });

  it('return empty data > api return status 455', async () => {
    mockApiServiceError('455', { status: '455' });

    const response = await searchAccountsRequest({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'accountSearchList/searchAccountList/fulfilled'
    );
    expect(response.payload).toEqual({ data: [] });
  });

  it('return error > api return error message', async () => {
    const toastSpy = mockToastAction('addToast');

    mockApiServiceError('500', {
      data: { message: `;${messageError}` }
    });

    const response = await searchAccountsRequest({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'accountSearchList/searchAccountList/rejected'
    );
    expect(response.payload).toEqual({
      errorMessage: `;${messageError}`
    });
    expect(toastSpy).toBeCalledWith({
      show: true,
      type: 'error',
      message: 'test'
    });
  });

  it('return error > api return error message', async () => {
    mockApiServiceError('500', {
      data: { message: `;` }
    });

    const response = await searchAccountsRequest({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'accountSearchList/searchAccountList/rejected'
    );
    expect(response.payload).toEqual({
      errorMessage: `;`
    });
  });

  it('return error > coverage catch', async () => {
    mockApiServiceError('500', {
      data: { message: `;a` }
    });

    const response = await searchAccountsRequest({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'accountSearchList/searchAccountList/rejected'
    );
    expect(response.payload).toEqual({
      errorMessage: `;a`
    });
  });

  it('run for coverage rejected', async () => {
    mockApiServiceError('500', {
      data: { message: `;${messageError}` }
    });

    const response = await searchAccountsRequest({ storeId, startSequence: 1 })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'accountSearchList/searchAccountList/rejected'
    );
    expect(response.payload).toEqual({
      errorMessage: `;${messageError}`
    });
  });

  it('run for coverage > should return data', async () => {
    const customers = [];
    for (let i = 0; i < 26; i++) {
      customers.push({
        lastName: 'Smith',
        accountIdentifier: JSON.stringify({ maskedValue: '1' }),
        presentationInstrumentIdentifier: JSON.stringify({
          maskedValue: '1'
        }),
        customerRoleTypeCode: '02',
        customerName: 'secondary'
      });
    }
    mockApiService({ data: { customers } });

    const store = createStore(rootReducer, {
      mapping: {
        data: {
          accountSearchListOtherFields: {
            lastName: 'lastName',
            accountId: 'accountIdentifier',
            customerRoleTypeCode: 'customerRoleTypeCode'
          }
        }
      }
    });
    const response = await searchAccountsRequest({ storeId })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual(
      'accountSearchList/searchAccountList/fulfilled'
    );
    const { data } = response.payload as SearchAccountListResponse;
    expect(data.length).toEqual(26);
  });
});
