import { responseDefault, accEValue, storeId, accNbr } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import searchListService from '../searchListService';
import { getMoreAccountInfoRequest } from './getMoreAccountInfo';

let spy: jest.SpyInstance;

describe('getMoreAccountInfoRequest', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  const mockApiService = (response?: MagicKeyValue) => {
    spy = jest
      .spyOn(searchListService, 'getMoreAccountInfo')
      .mockResolvedValue({
        ...responseDefault,
        ...response
      });
  };

  const mockApiServiceError = (status?: string, data?: MagicKeyValue) => {
    spy = jest
      .spyOn(searchListService, 'getMoreAccountInfo')
      .mockRejectedValue({
        status,
        ...data
      });
  };

  it('return empty data', async () => {
    mockApiService();

    const store = createStore(rootReducer, {});
    const response = await getMoreAccountInfoRequest({
      storeId,
      accountId: accNbr,
      accEValue
    })(store.dispatch, store.getState, {});

    expect(response.type).toEqual(
      'accountSearchList/getMoreAccountInfo/fulfilled'
    );
    expect(response.payload).toEqual({ data: {} });
  });

  it('should empty data > api response customerInquiry invalid type', async () => {
    mockApiService({ data: { customerInquiry: {} } });

    const store = createStore(rootReducer, {});
    const response = await getMoreAccountInfoRequest({
      storeId,
      accountId: accNbr,
      accEValue
    })(store.dispatch, store.getState, {});

    expect(response.type).toEqual(
      'accountSearchList/getMoreAccountInfo/fulfilled'
    );
    expect(response.payload).toEqual({ data: {} });
  });

  it('return empty data > api response empty customerInquiry', async () => {
    mockApiService({ data: { customerInquiry: [] } });

    const store = createStore(rootReducer, {});
    const response = await getMoreAccountInfoRequest({
      storeId,
      accountId: accNbr,
      accEValue
    })(store.dispatch, store.getState, {});

    expect(response.type).toEqual(
      'accountSearchList/getMoreAccountInfo/fulfilled'
    );
    expect(response.payload).toEqual({ data: {} });
  });

  it('return data', async () => {
    mockApiService({
      data: {
        customerInquiry: [
          {
            lifetimeDelinquentCycleCount: 'lifetimeDelinquentCycleCount',
            delinquentAmount: 'delinquentAmount',
            internalStatusCode: 'internalStatusCode',
            externalStatusCode: 'externalStatusCode',
            daysDelinquentCount: 'daysDelinquentCount',
            customerRoleTypeCode: '01'
          }
        ]
      }
    });

    const store = createStore(rootReducer, {
      mapping: {
        data: {
          accountMoreInfo: {
            cycleCounts: 'lifetimeDelinquentCycleCount',
            delinquentAmount: 'delinquentAmount',
            internalStatusCode: 'internalStatusCode',
            externalStatusCode: 'externalStatusCode',
            daysDelinquent: 'daysDelinquentCount'
          }
        }
      }
    });
    const response = await getMoreAccountInfoRequest({
      storeId,
      accountId: accNbr,
      accEValue
    })(store.dispatch, store.getState, {});

    expect(response.type).toEqual(
      'accountSearchList/getMoreAccountInfo/fulfilled'
    );
    expect(response.payload).toEqual({
      data: {
        cycleCounts: 'lifetimeDelinquentCycleCount',
        delinquentAmount: 'delinquentAmount',
        internalStatusCode: 'internalStatusCode',
        externalStatusCode: 'externalStatusCode',
        daysDelinquent: 'daysDelinquentCount'
      }
    });
  });

  it('run reject', async () => {
    mockApiServiceError();

    const store = createStore(rootReducer, {});
    const response = await getMoreAccountInfoRequest({
      storeId,
      accountId: accNbr,
      accEValue
    })(store.dispatch, store.getState, {});

    const states = store.getState();
    expect(response.type).toEqual(
      'accountSearchList/getMoreAccountInfo/rejected'
    );
    expect(
      states.accountSearchList[storeId].moreInfoData![accNbr].loading
    ).toEqual(false);
  });

  it('run for coverage', async () => {
    mockApiService();

    const store = createStore(rootReducer, {
      accountSearchList: {
        [storeId]: {
          moreInfoData: {}
        }
      }
    });
    const response = await getMoreAccountInfoRequest({
      storeId,
      accountId: accNbr,
      accEValue
    })(store.dispatch, store.getState, {});

    expect(response.type).toEqual(
      'accountSearchList/getMoreAccountInfo/fulfilled'
    );
    expect(response.payload).toEqual({ data: {} });
  });
});
