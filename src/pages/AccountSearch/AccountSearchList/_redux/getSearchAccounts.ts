import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import isEmpty from 'lodash.isempty';

// Types
import {
  SearchAccountListRequestArgs,
  AccountSearchListState,
  SearchAccountListResponse,
  AccountData
} from '../types';

// ConstantS
import {
  NUMBER_RECORDS_PER_LOAD,
  DEFAULT_SEQUENCE,
  NO_RESULT_CODE,
  IDENTIFIER_INVALID_CODE
} from '../constants';
import {
  EMPTY_ARRAY,
  EMPTY_STRING,
  EMPTY_STRING_OBJECT,
  SEMICOLON
} from 'app/constants';

// Helpers
import { handleMapDataSearchList, mapSearchAttribute } from '../helpers';

// Service
import searchListService from '../searchListService';

// Actions
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

export const searchAccountsRequest = createAsyncThunk<
  SearchAccountListResponse,
  SearchAccountListRequestArgs,
  ThunkAPIConfig
>('accountSearchList/searchAccountList', async (args, thunkAPI) => {
  const { getState, dispatch } = thunkAPI;

  try {
    const { searchParams = {}, startSequence = 0, endSequence = 0 } = args;
    const { mapping, accessConfig } = getState();
    const [clientNumber = ''] =
      accessConfig.cspaList?.[0]?.cspa?.split('-') || [];

    const requestBody = mapSearchAttribute(
      searchParams,
      startSequence,
      endSequence,
      clientNumber
    );

    const { data } = await searchListService.getSearchAccounts(requestBody);

    let result: AccountData[] = [];

    if (isEmpty(data)) {
      return {
        data: result
      };
    }

    const { customerInquiry = EMPTY_ARRAY, customers = EMPTY_ARRAY } = data;
    const { accSearchListByAccNumber = {}, accountSearchListOtherFields = {} } =
      mapping?.data || {};

    const isEmptyCustomer = isEmpty(data.customers);
    const customerData = isEmptyCustomer ? customerInquiry : customers;
    const mappingData = isEmptyCustomer
      ? accSearchListByAccNumber
      : accountSearchListOtherFields;

    result = handleMapDataSearchList(
      customerData,
      mappingData,
      isEmptyCustomer
    );

    return {
      data: result
    };
  } catch (err) {
    const errorStatus =
      err?.status?.toString() || err?.response?.status?.toString();
    if (errorStatus === NO_RESULT_CODE) {
      return {
        data: []
      };
    }

    try {
      const [, errorMessage]: Array<string> =
        err?.data?.message?.split(SEMICOLON);

      const { message = EMPTY_STRING, resultCode } = JSON.parse(
        errorMessage || EMPTY_STRING_OBJECT
      ) as CommonErrorMsg;

      if (IDENTIFIER_INVALID_CODE === resultCode) {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'error',
            message
          })
        );
      }
    } catch (e) {
      console.log(e);
    }

    return thunkAPI.rejectWithValue({
      errorMessage: err?.data?.message
    });
  }
});

/**
 * EXTRA REDUCERS BUILDER: GET search account number
 */
export const searchAccountsBuilder = (
  builder: ActionReducerMapBuilder<AccountSearchListState>
) => {
  builder
    .addCase(searchAccountsRequest.pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        loading: true,
        isEnd: false,
        error: false
      };
    })
    .addCase(searchAccountsRequest.fulfilled, (draftState, action) => {
      const { startSequence, storeId } = action.meta.arg;
      const { data } = action.payload;
      if (data.length < NUMBER_RECORDS_PER_LOAD) {
        draftState[storeId] = {
          ...draftState[storeId],
          isEnd: true
        };
      }

      draftState[storeId] = {
        ...draftState[storeId],
        loading: false
      };

      if (startSequence === DEFAULT_SEQUENCE.startSequence) {
        draftState[storeId] = {
          ...draftState[storeId],
          data
        };
      } else {
        const existedAccountList = draftState[storeId].data || [];
        draftState[storeId] = {
          ...draftState[storeId],
          data: [...existedAccountList, ...data]
        };
      }
    })
    .addCase(searchAccountsRequest.rejected, (draftState, action) => {
      const { storeId, startSequence } = action.meta.arg;

      if (startSequence === DEFAULT_SEQUENCE.startSequence) {
        draftState[storeId] = {
          ...draftState[storeId],
          data: []
        };
      }
      draftState[storeId] = {
        ...draftState[storeId],
        loading: false,
        error: true
      };
    });
};
