import { createSelector } from '@reduxjs/toolkit';

// Types
import { MoreInfo, AccountIdObject, AccountData } from '../types';

export const selectLoading = (storeId: string) => {
  return createSelector(
    (states: RootState) => states.accountSearchList[storeId]?.loading || false,
    (loading: boolean) => loading
  );
};

export const selectIsEndList = (storeId: string) => {
  return createSelector(
    (states: RootState) => states.accountSearchList[storeId]?.isEnd || false,
    (isEnd: boolean) => isEnd
  );
};

export const selectIsError = (storeId: string) => {
  return createSelector(
    (states: RootState) => states.accountSearchList[storeId]?.error || false,
    (isError: boolean) => isError
  );
};

export const selectSearchResultsData = (storeId: string) => {
  return createSelector(
    (states: RootState) => states.accountSearchList[storeId]?.data || [],
    (data: AccountData[]) => data
  );
};

export const selectShowMoreRowIds = (storeId: string) => {
  return createSelector(
    (states: RootState) =>
      states.accountSearchList[storeId]?.showMoreRowIds || {},
    (showMoreRowIds: AccountIdObject<boolean>) => showMoreRowIds
  );
};

export const selectMoreLoading = (storeId: string, accountId: string) => {
  return createSelector(
    (state: RootState) => {
      const moreInfo = state.accountSearchList[storeId]?.moreInfoData || {};
      return moreInfo[accountId]?.loading || false;
    },
    (loading: boolean) => loading
  );
};

export const selectMoreInfoData = (storeId: string, accountId: string) => {
  return createSelector(
    (state: RootState) => {
      const moreInfo = state.accountSearchList[storeId]?.moreInfoData || {};
      return moreInfo[accountId]?.data || {};
    },
    (data: MoreInfo) => data
  );
};
