import { batch } from 'react-redux';
import { createAsyncThunk } from '@reduxjs/toolkit';
import { formatMaskNumber, removeStoreRedux } from 'app/helpers';
import { timerWorkingActions } from 'pages/TimerWorking/_redux/reducers';
import { tabActions } from 'pages/__commons/TabBar/_redux';
import { callerAuthenticationActions } from 'pages/CallerAuthenticate/_redux/reducers';

import accountDetailService from 'pages/AccountDetails/accountDetailService';

import {
  DEFAULT_QUESTION_CONFIG,
  DEFAULT_SOURCE_CONFIG,
  SOURCE_CONFIG_ID
} from 'pages/ClientConfiguration/Authentication/constants';
import { AUTHENTICATION_COMPONENT_ID, DASH } from 'app/constants';
import { WorkType } from 'pages/TimerWorking/types';
import { nextActions } from 'pages/AccountDetails/NextActions/_redux/reducers';
import { ItemTab } from 'pages/__commons/TabBar/types';
import { QueueTypeEnum } from 'pages/AccountSearch/Home/Dashboard/QueueDetail/types';
import { queueAccountsActions } from 'pages/AccountSearch/Home/Dashboard/QueueDetail/_redux';

export const getAuthenticationConfig = async (args: {
  common: Record<string, string>;
}) => {
  try {
    const { data } = await accountDetailService.getAuthenticationConfig(args);
    const filterConfig = (
      data.clientConfig.components as CollectionsClientConfigItem['components']
    ).find(item => item.component === AUTHENTICATION_COMPONENT_ID);

    if (!filterConfig)
      return {
        sourceConfig: DEFAULT_SOURCE_CONFIG,
        questionConfig: DEFAULT_QUESTION_CONFIG
      };

    return {
      sourceConfig: filterConfig.jsonValue.sourceConfig ?? [],
      questionConfig: filterConfig.jsonValue.questionConfig ?? []
    };
  } catch (_) {
    return {
      sourceConfig: DEFAULT_SOURCE_CONFIG,
      questionConfig: DEFAULT_QUESTION_CONFIG
    };
  }
};

export const openAccountDetail = createAsyncThunk<
  unknown,
  {
    queueId?: string;
    eValue?: string;
    primaryName?: string;
    memberSequenceIdentifier?: string;
    maskedValue?: string;
    socialSecurityIdentifier?: string;
    clientIdentifier?: string;
    systemIdentifier?: string;
    principalIdentifier?: string;
    agentIdentifier?: string;
    type: WorkType;
    queueType?: QueueTypeEnum;
  },
  ThunkAPIConfig
>('accountSearchList/openAccountDetail', async (args, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;
  const { accountDetail, queueAccounts } = getState();

  const {
    queueId,
    eValue = '',
    primaryName = '',
    memberSequenceIdentifier = '',
    maskedValue = '',
    socialSecurityIdentifier = '',
    clientIdentifier = '',
    systemIdentifier = '',
    principalIdentifier = '',
    agentIdentifier = '',
    type,
    queueType
  } = args;
  const { user = '', app = '', org } = window.appConfig?.commonConfig || {};

  const mask = formatMaskNumber(maskedValue);
  const maskedValueFormat =
    mask && mask?.firstText + mask?.maskText + mask?.lastText;
  const title = `${primaryName} ${DASH} ${maskedValueFormat}`;
  const newStoreId = `${eValue}${DASH}${memberSequenceIdentifier}`;

  const newTab: ItemTab = {
    title,
    storeId: newStoreId,
    accEValue: eValue,
    tabType: 'accountDetail',
    iconName: 'account-details',
    props: {
      primaryName,
      accEValue: eValue,
      storeId: newStoreId,
      accNbr: maskedValue,
      memberSequenceIdentifier,
      socialSecurityIdentifier,
      queueId
    }
  };

  // check if account detail tab exists or not
  // set active tab if exists and open the same accountNumber
  if (accountDetail[newStoreId]) {
    dispatch(
      tabActions.selectTab({
        storeId: newStoreId,
        accEValue: eValue
      })
    );
  }

  if (queueId) {
    dispatch(nextActions.setQueueId({ eValue, queueId, queueType }));

    if (queueType === QueueTypeEnum.PULL_QUEUE) {
      const { currentQueueAccount } = queueAccounts[queueId] || {};
      const { tabStoreId, eValue: queueAccountEvalue } =
        currentQueueAccount || {};

      // close previous pull-queue account detail tab if exists
      // Trigger action remove tab
      if (tabStoreId && queueAccountEvalue) {
        batch(() => {
          dispatch(tabActions.removeTab({ storeId: tabStoreId }));
          dispatch(
            removeStoreRedux(
              'accountDetail',
              tabStoreId,
              queueAccountEvalue,
              queueId
            )
          );
        });
      }

      batch(() => {
        dispatch(tabActions.addTab(newTab));

        dispatch(
          queueAccountsActions.setCurrentQueueAccount({
            storeId: queueId,
            currentQueueAccount: {
              eValue,
              tabStoreId: newStoreId
            }
          })
        );
      });
    }
  }

  dispatch(
    callerAuthenticationActions.setGetAuthenConfigLoading({
      loading: true
    })
  );
  const { sourceConfig, questionConfig } = await getAuthenticationConfig({
    common: {
      agent: agentIdentifier,
      clientNumber: clientIdentifier,
      system: systemIdentifier,
      prin: principalIdentifier,
      user,
      app,
      orgId: org
    }
  });
  dispatch(
    callerAuthenticationActions.setGetAuthenConfigLoading({
      loading: false
    })
  );

  dispatch(
    callerAuthenticationActions.setAuthenticateData({
      accountDetailData: {
        queueId,
        eValue,
        primaryName,
        memberSequenceIdentifier,
        maskedValue,
        socialSecurityIdentifier
      }
    })
  );
  if (
    (type === WorkType.ACCOUNT_SEARCH &&
      sourceConfig.includes(SOURCE_CONFIG_ID.SEARCH_RESULT)) ||
    (type === WorkType.QUEUE &&
      sourceConfig.includes(SOURCE_CONFIG_ID.PUSH_QUEUE))
  ) {
    batch(() => {
      dispatch(callerAuthenticationActions.setQuestionConfig(questionConfig));
      dispatch(callerAuthenticationActions.toggleModalCallerAuthentication());
      dispatch(
        timerWorkingActions.setWorkType({
          eValue,
          workType: type
        })
      );
    });
  } else {
    dispatch(
      timerWorkingActions.setWorkType({
        eValue,
        workType: type
      })
    );
    const contactPerson = primaryName;
    batch(() => {
      dispatch(timerWorkingActions.startAuthenticate(eValue));
      dispatch(
        timerWorkingActions.setContactPersonName({
          eValue,
          contactPerson
        })
      );
    });
    dispatch(tabActions.addTab(newTab));
  }
});
