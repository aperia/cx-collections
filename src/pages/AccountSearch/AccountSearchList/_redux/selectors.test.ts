import { selectorWrapper, storeId } from 'app/test-utils';
import { DOFAppState } from 'storeConfig';
import * as selectors from './selectors';

const states: Partial<DOFAppState> = {
  accountSearchList: {
    [storeId]: {
      data: [
        {
          accountId: 'accountId',
          memberSequenceIdentifier: '001',
          socialSecurityIdentifier: '001',
          customerRoleTypeCode: '01',
          accNbr: '11',
          address: {
            addressLineOne: 'addressLineOne',
            addressLineTwo: 'addressLineTwo'
          },
          cityName: 'cityName',
          id: 'id',
          lastSSN: '0001',
          primaryName: 'primaryName',
          secondaryName: 'secondaryName',
          clientId: 'clientId'
        }
      ],
      loading: true,
      isEnd: true,
      error: true,
      showMoreRowIds: {
        [storeId]: true
      },
      moreInfoData: {
        [storeId]: {
          loading: true,
          data: {
            cycleCounts: 1,
            daysDelinquent: 1,
            delinquentAmount: 1,
            externalStatusCode: 'externalStatusCode',
            internalStatusCode: 'internalStatusCode'
          }
        }
      }
    }
  }
};

const triggerSelector = selectorWrapper(states);

describe('AccountSearchList selectors', () => {
  it('selectLoading', () => {
    const { data, emptyData } = triggerSelector(
      selectors.selectLoading(storeId)
    );
    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectIsEndList', () => {
    const { data, emptyData } = triggerSelector(
      selectors.selectIsEndList(storeId)
    );
    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectIsError', () => {
    const { data, emptyData } = triggerSelector(
      selectors.selectIsError(storeId)
    );
    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectSearchResultsData', () => {
    const { data, emptyData } = triggerSelector(
      selectors.selectSearchResultsData(storeId)
    );
    expect(data).toEqual(states.accountSearchList![storeId].data);
    expect(emptyData).toEqual([]);
  });

  it('selectShowMoreRowIds', () => {
    const { data, emptyData } = triggerSelector(
      selectors.selectShowMoreRowIds(storeId)
    );
    expect(data).toEqual(states.accountSearchList![storeId].showMoreRowIds);
    expect(emptyData).toEqual({});
  });

  it('selectMoreLoading', () => {
    const { data, emptyData } = triggerSelector(
      selectors.selectMoreLoading(storeId, storeId)
    );
    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('selectMoreInfoData', () => {
    const { data, emptyData } = triggerSelector(
      selectors.selectMoreInfoData(storeId, storeId)
    );
    expect(data).toEqual(
      states.accountSearchList![storeId]?.moreInfoData![storeId]?.data
    );
    expect(emptyData).toEqual({});
  });
});
