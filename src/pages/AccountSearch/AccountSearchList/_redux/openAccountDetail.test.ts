import {
  openAccountDetail,
  getAuthenticationConfig
} from './openAccountDetail';

import { callerAuthenticationActions } from 'pages/CallerAuthenticate/_redux/reducers';
import { tabActions } from 'pages/__commons/TabBar/_redux';
import accountDetailService from 'pages/AccountDetails/accountDetailService';
import { timerWorkingActions } from 'pages/TimerWorking/_redux/reducers';
import { mockActionCreator, responseDefault } from 'app/test-utils';
import { AUTHENTICATION_COMPONENT_ID } from 'app/constants';
import {
  DEFAULT_QUESTION_CONFIG,
  DEFAULT_SOURCE_CONFIG
} from 'pages/ClientConfiguration/Authentication/constants';
import { createStore } from '@reduxjs/toolkit';
import { rootReducer } from 'storeConfig';
import { WorkType } from 'pages/TimerWorking/types';
import { QueueTypeEnum } from 'pages/AccountSearch/Home/Dashboard/QueueDetail/types';
import * as removeStore from 'app/helpers/removeStore';
import { queueAccountsActions } from 'pages/AccountSearch/Home/Dashboard/QueueDetail/_redux';

const tabActionSpy = mockActionCreator(tabActions);
const callerAuthenticationActionsSpy = mockActionCreator(
  callerAuthenticationActions
);
const timerWorkingActionsSpy = mockActionCreator(timerWorkingActions);

describe('test getAuthenticationConfig', () => {
  it('has no filterConfig', async () => {
    const mSpy = jest
      .spyOn(accountDetailService, 'getAuthenticationConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          clientConfig: {
            components: [
              {
                component: 'something else',
                jsonValue: {
                  sourceConfig: DEFAULT_SOURCE_CONFIG,
                  questionConfig: DEFAULT_QUESTION_CONFIG
                }
              }
            ]
          }
        }
      });
    const result = await getAuthenticationConfig({ common: {} });
    expect(result).toEqual({
      sourceConfig: DEFAULT_SOURCE_CONFIG,
      questionConfig: DEFAULT_QUESTION_CONFIG
    });
    mSpy.mockReset();
    mSpy.mockRestore();
  });

  it('has error ', async () => {
    const mSpy = jest
      .spyOn(accountDetailService, 'getAuthenticationConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });
    const result = await getAuthenticationConfig({ common: {} });
    expect(result).toEqual({
      sourceConfig: DEFAULT_SOURCE_CONFIG,
      questionConfig: DEFAULT_QUESTION_CONFIG
    });
    mSpy.mockReset();
    mSpy.mockRestore();
  });

  it('should be return empty ', async () => {
    const mSpy = jest
      .spyOn(accountDetailService, 'getAuthenticationConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          clientConfig: {
            components: [
              {
                component: AUTHENTICATION_COMPONENT_ID,
                jsonValue: {}
              }
            ]
          }
        }
      });
    const result = await getAuthenticationConfig({ common: {} });
    expect(result).toEqual({
      sourceConfig: [],
      questionConfig: []
    });
    mSpy.mockReset();
    mSpy.mockRestore();
  });
});

describe('test open account detail action', () => {
  const mockParam = {
    type: WorkType.ACCOUNT_SEARCH
  };
  it('open caller Authentication modal with Account search', async () => {
    const mockToggleModalCallerAuthentication = callerAuthenticationActionsSpy(
      'toggleModalCallerAuthentication'
    );
    const mockSetQuestionConfig =
      callerAuthenticationActionsSpy('setQuestionConfig');
    const mockSetWorkType = timerWorkingActionsSpy('setWorkType');
    const mSpy = jest
      .spyOn(accountDetailService, 'getAuthenticationConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          clientConfig: {
            components: [
              {
                component: AUTHENTICATION_COMPONENT_ID,
                jsonValue: {
                  sourceConfig: DEFAULT_SOURCE_CONFIG,
                  questionConfig: DEFAULT_QUESTION_CONFIG
                }
              }
            ]
          }
        }
      });
    const store = createStore(rootReducer, {});
    await openAccountDetail(mockParam)(store.dispatch, store.getState, {});

    expect(mockToggleModalCallerAuthentication).toBeCalled();
    expect(mockSetWorkType).toBeCalled();
    expect(mockSetQuestionConfig).toBeCalled();
    mSpy.mockReset();
    mSpy.mockRestore();
  });

  it('Account detail tab exists ', async () => {
    const selectTabSpy = tabActionSpy('selectTab');
    const initialState: Partial<RootState> = {
      accountDetail: {
        'eValue-memberSequenceIdentifier': { data: {}, isLoading: false }
      }
    };
    const store = createStore(rootReducer, { ...initialState });
    await openAccountDetail({
      ...mockParam,
      queueId: 'ATY001',
      eValue: 'eValue',
      memberSequenceIdentifier: 'memberSequenceIdentifier'
    })(store.dispatch, store.getState, {});
    expect(selectTabSpy).toBeCalledWith(
      expect.objectContaining({
        accEValue: 'eValue',
        storeId: 'eValue-memberSequenceIdentifier'
      })
    );
  });

  it('Close previous pull-queue account detail tab - Trigger action remove tab', async () => {
    const removeTabSpy = tabActionSpy('removeTab');
    const removeStoreReduxSpy = jest
      .spyOn(removeStore, 'removeStoreRedux')
      .mockImplementation((args: MagicKeyValue[]) => () => ({ type: 'type' }));

    const initialState: Partial<RootState> = {
      queueAccounts: {
        ATY001: {
          isLoading: false,
          isError: false,
          data: [],
          currentQueueAccount: {
            eValue: 'eValue',
            tabStoreId: 'tabStoreId'
          }
        }
      }
    };
    const store = createStore(rootReducer, { ...initialState });
    await openAccountDetail({
      ...mockParam,
      queueId: 'ATY001',
      queueType: QueueTypeEnum.PULL_QUEUE
    })(store.dispatch, store.getState, {});

    expect(removeTabSpy).toBeCalled();
    expect(removeStoreReduxSpy).toBeCalled();
    removeStoreReduxSpy.mockReset();
    removeStoreReduxSpy.mockRestore();
  });

  it('Open pull-queue account detail tab', async () => {
    const mockAddTab = tabActionSpy('addTab');
    const setCurrentQueueAccountActionsSpy =
      mockActionCreator(queueAccountsActions);
    const mockSetCurrentQueueAccount = setCurrentQueueAccountActionsSpy(
      'setCurrentQueueAccount'
    );
    const store = createStore(rootReducer, {});

    await openAccountDetail({
      ...mockParam,
      queueId: 'ATY001',
      queueType: QueueTypeEnum.PULL_QUEUE
    })(store.dispatch, store.getState, {});

    expect(mockAddTab).toBeCalled();
    expect(mockSetCurrentQueueAccount).toBeCalled();
  });

  it('open caller Authentication modal with Queue', async () => {
    const mockToggleModalCallerAuthentication = callerAuthenticationActionsSpy(
      'toggleModalCallerAuthentication'
    );
    const mockSetQuestionConfig =
      callerAuthenticationActionsSpy('setQuestionConfig');
    const mockSetWorkType = timerWorkingActionsSpy('setWorkType');
    const mSpy = jest
      .spyOn(accountDetailService, 'getAuthenticationConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          clientConfig: {
            components: [
              {
                component: AUTHENTICATION_COMPONENT_ID,
                jsonValue: {
                  sourceConfig: DEFAULT_SOURCE_CONFIG,
                  questionConfig: DEFAULT_QUESTION_CONFIG
                }
              }
            ]
          }
        }
      });
    const store = createStore(rootReducer, {});
    await openAccountDetail({ ...mockParam, type: WorkType.QUEUE })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(mockToggleModalCallerAuthentication).toBeCalled();
    expect(mockSetWorkType).toBeCalled();
    expect(mockSetQuestionConfig).toBeCalled();
    mSpy.mockReset();
    mSpy.mockRestore();
  });

  it('not open caller Authentication modal', async () => {
    const mockToggleModalCallerAuthentication = callerAuthenticationActionsSpy(
      'toggleModalCallerAuthentication'
    );
    const mockAddTab = tabActionSpy('addTab');
    const mockStartAuthenticate = timerWorkingActionsSpy('startAuthenticate');
    const mSpy = jest
      .spyOn(accountDetailService, 'getAuthenticationConfig')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          clientConfig: {
            components: [
              {
                component: AUTHENTICATION_COMPONENT_ID,
                jsonValue: {
                  sourceConfig: [''],
                  questionConfig: DEFAULT_QUESTION_CONFIG
                }
              }
            ]
          }
        }
      });
    const store = createStore(rootReducer, {});
    await openAccountDetail({ ...mockParam, maskedValue: '1234******1234' })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(mockToggleModalCallerAuthentication).not.toBeCalled();
    expect(mockStartAuthenticate).toBeCalled();
    expect(mockAddTab).toBeCalled();
    mSpy.mockReset();
    mSpy.mockRestore();
  });
});
