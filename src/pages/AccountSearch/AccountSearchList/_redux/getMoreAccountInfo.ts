import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import isArray from 'lodash.isarray';
import isEmpty from 'lodash.isempty';
import { mappingDataFromObj } from 'app/helpers';
import {
  AccountSearchListState,
  GetMoreAccountInfoRequestArgs,
  GetMoreAccountInfoRequestPayload
} from '../types';
import { CUSTOMER_ROLE_CODE } from 'app/constants/index';

// service
import searchListService from '../searchListService';

export const getMoreAccountInfoRequest = createAsyncThunk<
  GetMoreAccountInfoRequestPayload,
  GetMoreAccountInfoRequestArgs,
  ThunkAPIConfig
>('accountSearchList/getMoreAccountInfo', async (args, thunkAPI) => {
  const { accEValue } = args;
  const state = thunkAPI.getState();
  // Take mapping model
  const accountSearchListMapping = state.mapping?.data?.accountMoreInfo || {};
  // Request search API
  const { data } = await searchListService.getMoreAccountInfo(accEValue);

  if (isEmpty(data))
    return {
      data: {}
    };
  /* Because data is list of customer
   * we need to find exact info by rule customerRoleTypeCode = "01" - primary
   */
  if (!isArray(data.customerInquiry)) return { data: {} };
  const moreInfoDataObj =
    data.customerInquiry.find(
      (item: MagicKeyValue) =>
        item.customerRoleTypeCode === CUSTOMER_ROLE_CODE.primary
    ) ?? {};

  const mappedData: MagicKeyValue = mappingDataFromObj(
    moreInfoDataObj,
    accountSearchListMapping
  );
  return {
    data: mappedData
  };
});

export const getMoreAccountInfoBuilder = (
  builder: ActionReducerMapBuilder<AccountSearchListState>
) => {
  builder
    .addCase(getMoreAccountInfoRequest.pending, (draftState, action) => {
      const { storeId, accountId } = action.meta.arg;
      if (!draftState[storeId]?.moreInfoData) {
        draftState[storeId] = {
          ...draftState[storeId],
          moreInfoData: {
            [accountId]: {
              loading: true
            }
          }
        };
      }
      draftState[storeId]!.moreInfoData![accountId] = {
        ...draftState[storeId].moreInfoData![accountId],
        loading: true
      };
    })
    .addCase(getMoreAccountInfoRequest.fulfilled, (draftState, action) => {
      const { storeId, accountId } = action.meta.arg;
      const { data } = action.payload;
      draftState[storeId].moreInfoData![accountId] = {
        ...draftState[storeId].moreInfoData![accountId],
        loading: false,
        data
      };
    })
    .addCase(getMoreAccountInfoRequest.rejected, (draftState, action) => {
      const { storeId, accountId } = action.meta.arg;
      draftState[storeId].moreInfoData![accountId] = {
        ...draftState[storeId].moreInfoData![accountId],
        loading: false
      };
    });
};
