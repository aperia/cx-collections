import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Types
import { AccountSearchListState, ShowMoreAccountPayload } from '../types';

// Actions
import {
  searchAccountsRequest,
  searchAccountsBuilder
} from './getSearchAccounts';
import {
  getMoreAccountInfoRequest,
  getMoreAccountInfoBuilder
} from './getMoreAccountInfo';

const { actions, reducer } = createSlice({
  name: 'accountSearchList',
  initialState: {} as AccountSearchListState,
  reducers: {
    removeStore: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    updateShowMoreRowIds: (
      draftState,
      action: PayloadAction<ShowMoreAccountPayload>
    ) => {
      const { storeId, accountId } = action.payload;
      if (!draftState[storeId].showMoreRowIds) {
        draftState[storeId].showMoreRowIds = {};
        draftState[storeId].showMoreRowIds![accountId] = true;
        return;
      }

      draftState[storeId].showMoreRowIds![accountId] =
        !draftState[storeId].showMoreRowIds![accountId];
    },
    showLoading: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;

      if (!draftState[storeId]) {
        return;
      }
      draftState[storeId].loading = true;
      draftState[storeId].error = false;
    }
  },
  extraReducers: builder => {
    searchAccountsBuilder(builder);
    getMoreAccountInfoBuilder(builder);
  }
});

const searchResultsAction = {
  ...actions,
  searchAccountsRequest,
  getMoreAccountInfoRequest
};

export { searchResultsAction, reducer };
