import { apiService } from 'app/utils/api.service';
// constants
import { GET_MORE_ACC_INFO_SELECT_FIELDS } from './constants';

const searchListService = {
  getMoreAccountInfo(accId: string) {
    const url = window.appConfig?.api?.account?.searchAccount || '';
    const bodyRequest = {
      odsNaming: false,
      searchFields: {
        enteredAccountId: accId
      },
      selectFields: GET_MORE_ACC_INFO_SELECT_FIELDS
    };
    return apiService.post(url, bodyRequest);
  },
  getSearchAccounts: (requestBody: MagicKeyValue) => {
    const url = window.appConfig?.api?.account?.searchAccount || '';
    return apiService.post<{
      customerInquiry?: MagicKeyValue[];
      customers?: MagicKeyValue[];
      cardholderInfo?: MagicKeyValue;
    }>(url, requestBody);
  }
};
export default searchListService;
