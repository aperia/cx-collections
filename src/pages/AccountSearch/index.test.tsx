import { renderMockStoreId, storeId } from 'app/test-utils';
import React from 'react';
import SearchResultList from './index';
import { screen } from '@testing-library/react';

describe('SearchResultList component', () => {
  it('should render component', () => {
    renderMockStoreId(<SearchResultList searchParams={[]} storeId={storeId} />);
    expect(screen.getByText('txt_results_list')).toBeInTheDocument();
  });
});
