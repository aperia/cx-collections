import isEmpty from 'lodash.isempty';
import {
  AttributeSearchData,
  AttributeSearchDependencyField,
  AttributeSearchValue
} from 'app/_libraries/_dls/components';

// Const
import { EMPTY_OBJECT, EMPTY_STRING } from 'app/constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

export const getAttributeSearchPlaceholder =
  (
    constraintFields?: AttributeSearchDependencyField[],
    data?: AttributeSearchData[],
    isNoCombineField?: boolean
  ) =>
  (searchValues: AttributeSearchValue[]) => {
    if (isEmpty(searchValues)) {
      return I18N_COMMON_TEXT.TYPE_TO_ADD_PARAMETER;
    }

    if (isNoCombineField && searchValues.length) {
      return EMPTY_STRING;
    }

    const listRemaining = data?.filter(
      item =>
        searchValues.findIndex(searchItem => searchItem.key === item.key) === -1
    );

    let listDisabled: string[] = [];

    listRemaining?.forEach(remainingItem => {
      let fieldDisabled;
      constraintFields?.forEach(constraintItem => {
        const { disabledFields = EMPTY_OBJECT } = constraintItem?.requiredField;
        fieldDisabled =
          Object.keys(disabledFields).find(
            disabledKy => disabledKy === remainingItem.key
          ) || EMPTY_STRING;
        if (
          fieldDisabled.length &&
          listDisabled.indexOf(fieldDisabled) === -1
        ) {
          listDisabled = [...listDisabled, fieldDisabled];
        }
      });
    });

    if (listDisabled.length === listRemaining?.length) {
      return EMPTY_STRING;
    }

    return I18N_COMMON_TEXT.TYPE_TO_ADD_PARAMETER;
  };
