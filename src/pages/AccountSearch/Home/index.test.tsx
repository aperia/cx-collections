import React from 'react';
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import Home from '.';
import { fireEvent } from '@testing-library/react';
import {
  AttributeSearchEvent,
  AttributeSearchProps
} from 'app/_libraries/_dls/components';
import { actions } from 'pages/__commons/TabBar/_redux';
import { actionsLiveVox } from 'app/livevox-dialer/_redux/reducers';

jest.mock('chart.js', () => {
  class Chart {
    constructor(ctx: any, options: any) {}

    public static register = (list: any) => undefined;

    destroy = () => undefined;
    update = () => undefined;
  }

  return {
    Chart,
    registerables: []
  };
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const initialState: Partial<RootState> = {};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(<Home />, { initialState });
};

jest.mock('./Dashboard', () => () => <div>Dashboard</div>);

jest.mock('app/_libraries/_dls/components', () => {
  return {
    ...jest.requireActual('app/_libraries/_dls/components'),
    AttributeSearch: (props: AttributeSearchProps) => {
      return (
        <>
          <button
            onClick={() =>
              props.onChange!({
                value: [{ key: 'firstName', value: '1' }]
              } as AttributeSearchEvent)
            }
          >
            onChange
          </button>
          <button
            onClick={() =>
              props.onSearch!({
                value: [{ key: 'lastName', value: 'a' }]
              } as AttributeSearchEvent)
            }
          >
            onSearch
          </button>
          <button
            onClick={() =>
              props.onSearch!(
                {
                  value: {}
                } as AttributeSearchEvent,
                { lastName: 'error' }
              )
            }
          >
            onSearchError
          </button>
          <button
            onClick={() =>
              props.onSearch!({
                value: {}
              } as AttributeSearchEvent)
            }
          >
            onSearchEmpty
          </button>
          <button onClick={props.onClear}>onClear</button>
          <button onClick={props.validator}>onValidator</button>
        </>
      );
    }
  };
});

describe('Home component', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isAdminRole: true,
      isCollector: false
    }
  };
  it('handleChange', () => {
    const { wrapper } = renderWrapper(initialState);
    const changeButton = wrapper.getByText('onChange');
    wrapper.getByText('onValidator').click();
    fireEvent.click(changeButton);
  });

  it('onClear', () => {
    const { wrapper } = renderWrapper(initialState);
    const clearButton = wrapper.getByText('onClear');
    fireEvent.click(clearButton);
  });

  describe('handleSearch', () => {
    const spyActions = mockActionCreator(actions);

    it('with error', () => {
      const mockAddTabAction = spyActions('addTab');
      const { wrapper } = renderWrapper(initialState);
      const searchButton = wrapper.getByText('onSearchError');
      fireEvent.click(searchButton);
      expect(mockAddTabAction).not.toHaveBeenCalled();
    });

    it('with empty value', () => {
      const mockAddTabAction = spyActions('addTab');
      const { wrapper } = renderWrapper(initialState);
      const searchButton = wrapper.getByText('onSearchEmpty');
      fireEvent.click(searchButton);
      expect(mockAddTabAction).not.toHaveBeenCalled();
    });

    it('should dispatch action', () => {
      const mockAddTabAction = spyActions('addTab');
      const { wrapper } = renderWrapper(initialState);
      const searchButton = wrapper.getByText('onSearch');
      fireEvent.click(searchButton);
      expect(mockAddTabAction).toHaveBeenCalled();
    });

    it('should dispatch dialer actions', () => {
      jest.useFakeTimers();
      jest.spyOn(global, 'setInterval');
      const mockLiveVoxDialerActions = mockActionCreator(actionsLiveVox);
      const mockLiveVoxDialerAgentStatusThunk = mockLiveVoxDialerActions(
        'getLiveVoxDialerAgentStatusThunk'
      );
      const mockLiveVoxDialerScreenPopThunk = mockLiveVoxDialerActions(
        'getLiveVoxDialerScreenPopThunk'
      );

      const liveVoxInitialState = {
        liveVoxReducer: {
          sessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
          isAgentAuthenticated: true,
          lineNumber: 'ACD',
          accountNumber: '2345',
          lastInCallAccountNumber: '2345',
          inCallAccountNumber: '12345',
          call: {
            accountNumber: '5166480500018901',
            accountNumberRequired: true,
            callCenterId: 75,
            callRecordingStarted: true,
            callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
            callTransactionId: '126650403531',
            phoneNumber: '1234567890',
            serviceId: 1094568
          }
        }
      };

      renderWrapper(liveVoxInitialState);
      jest.advanceTimersByTime(3100);

      expect(mockLiveVoxDialerAgentStatusThunk).toBeCalledWith({
        sessionId: '27c84ce7-c479-4cc7-9710-2daf07334784'
      });
      expect(mockLiveVoxDialerAgentStatusThunk).toBeCalledTimes(1);

      expect(mockLiveVoxDialerScreenPopThunk).toBeCalledWith({
        sessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
        lineNumber: 'ACD'
      });
    });

    it('should not dispatch action addTab', () => {
      jest.useFakeTimers();
      jest.spyOn(global, 'setInterval');
      const mockAddTabAction = spyActions('addTab');

      const mockLiveVoxDialerActions = mockActionCreator(actionsLiveVox);
      mockLiveVoxDialerActions('getLiveVoxDialerAgentStatusThunk');
      mockLiveVoxDialerActions('getLiveVoxDialerScreenPopThunk');

      const liveVoxInitialState = {
        liveVoxReducer: {
          sessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
          isAgentAuthenticated: true,
          lineNumber: 'ACD',
          accountNumber: '',
          lastInCallAccountNumber: '2345',
          inCallAccountNumber: '',
          call: {
            accountNumber: '5166480500018901',
            accountNumberRequired: true,
            callCenterId: 75,
            callRecordingStarted: true,
            callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
            callTransactionId: '126650403531',
            phoneNumber: '1234567890',
            serviceId: 1094568
          }
        }
      };

      renderWrapper(liveVoxInitialState);
      jest.advanceTimersByTime(3100);

      expect(mockAddTabAction).not.toHaveBeenCalled();
    });
  });
});
