import axios from 'axios';

const homeService = {
  getState() {
    const url = window.appConfig?.api?.refData?.getState || '';
    return axios.get(url);
  }
};

export default homeService;
