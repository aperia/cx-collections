// mocks
import { mockAxiosResolve } from 'app/test-utils/mocks/mockAxiosResolve';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';

// services
import homeService from './homeService';

describe('homeService', () => {
  describe('getState', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      homeService.getState();

      expect(mockAxiosResolve).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      homeService.getState();

      expect(mockAxiosResolve).toBeCalledWith(apiUrl.refData.getState);
    });
  });
});
