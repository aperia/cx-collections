import { isEmpty } from 'lodash';

/**
 * convert number | NaN  => number | 0
 * @param number
 */
export const getNumber = (number: number) => {
  return !number ? 0 : number;
};

/**
 * 3600 => 1h00m:00s | 3800 => 1h03m20s
 * @param secs = number
 * @param mapping
 */
export const convertSecondsNumberToDDHHMMSS = (
  secs: number,
  mapping = ['d', 'h', 'm', 's']
) => {
  const _secs = getNumber(secs);
  const days = Math.floor(_secs / (3600 * 24));
  const hours = Math.floor((_secs % (3600 * 24)) / 3600);
  const minutes = Math.floor((_secs % 3600) / 60);
  const seconds = Math.floor(_secs % 60);

  const result = [days, hours, minutes, seconds].reduce(
    (prev, value, index) => {
      if (value <= 0 && !prev) return prev;

      const valueView = !prev ? value : value < 10 ? '0' + value : value;
      return `${prev}${valueView}${mapping[index]}`;
    },
    ''
  );
  return isEmpty(result) ? '0s' : result;
};
