import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef
} from 'react';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { dashboardActions as actions } from '../_redux/reducers';
import { getData, getError, getLoading, getPeriod } from './selectors';

// component
import { FailedApiReload } from 'app/components';
import HeaderCallActivity from './Header';
import DoughnutChart, {
  DoughnutChartData
} from 'app/_libraries/_dls/components/DoughnutChart';

// helpers
import isEmpty from 'lodash.isempty';
import classNames from 'classnames';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { DashBoardProps } from '../types';
import { DASHBOARD_PERIOD } from '../constants';
import { convertSecondsNumberToDDHHMMSS } from './helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';

const DashboardCallActivity: React.FC<DashBoardProps> = ({ title }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const ref = useRef<HTMLDivElement>(null);

  const data = useSelector(getData);
  const period = useSelector(getPeriod);
  const isError = useSelector(getError);
  const loading = useSelector(getLoading);
  const testId = 'dashboardCallActivity';

  const handleReload = useCallback(() => {
    if (!period) return;
    dispatch(actions.getDashboardCallActivity({ period }));
  }, [dispatch, period]);

  const content = useMemo(() => {
    if (isError) {
      return (
        <FailedApiReload
          id="dashboard-goal-failed"
          className="mt-80"
          onReload={handleReload}
          dataTestId={`${testId}_failed-api`}
        />
      );
    }

    if (loading || !data || isEmpty(data)) return null;

    const {
      averageTimePerCall,
      inboundCall,
      outboundCall,
      totalBreakTime,
      totalBreaks
    } = data;

    const total = Number(inboundCall) + Number(outboundCall);
    const amountData = {
      labels: [t('txt_inbound_calls'), t('txt_outbound_calls')],
      datasets: [
        {
          data: [Number(outboundCall), Number(inboundCall)],
          backgroundColor: ['#D977C4', '#7777D9']
        }
      ],
      center: {
        label: t('txt_total_capitalize'),
        value: `${total}`
      }
    } as DoughnutChartData;

    return (
      <div className="d-flex">
        <div className="w-fixed-left">
          <h5 data-testid={genAmtId(testId, 'break-information', '')}>
            {t('txt_break_information')}
          </h5>
          <div className="mt-26">
            <h5
              className="color-grey"
              data-testid={genAmtId(testId, 'totalBreaks', '')}
            >
              {totalBreaks}
            </h5>
            <h6
              className="color-grey-l24 mt-4"
              data-testid={genAmtId(testId, 'totalBreaks-label', '')}
            >
              {t('txt_total_breaks')}
            </h6>
          </div>
          <div className="divider-dashed mt-16" />
          <div className="mt-16">
            <h5
              className="color-grey"
              data-testid={genAmtId(testId, 'totalBreakTime', '')}
            >{`${totalBreakTime}h`}</h5>
            <h6
              className="color-grey-l24 mt-4"
              data-testid={genAmtId(testId, 'totalBreakTime-label', '')}
            >
              {t('txt_total_break_time')}
            </h6>
          </div>
        </div>

        <div className="flex-1 d-flex flex-column justify-content-between">
          <h5 data-testid={genAmtId(testId, 'call-information', '')}>
            {t('txt_call_information')}
          </h5>

          <div className="d-flex">
            <div className="w-35 d-flex justify-content-between">
              <div className="d-flex flex-column justify-content-end">
                <h5
                  className="color-grey"
                  data-testid={genAmtId(
                    testId,
                    'call-information-averageTimePerCall',
                    ''
                  )}
                >
                  {convertSecondsNumberToDDHHMMSS(averageTimePerCall)}
                </h5>
                <h6
                  className="color-grey-l24 mt-4"
                  data-testid={genAmtId(
                    testId,
                    'call-information-averageTimePerCall-label',
                    ''
                  )}
                >
                  {t('txt_average_time_per_call')}
                </h6>
              </div>

              <div className="divider-dashed-y"></div>
            </div>

            <div className="w-65 d-flex flex-column justify-content-end">
              <DoughnutChart
                chartData={amountData}
                dataTestId="call-information-chart"
              />
            </div>
          </div>
        </div>
      </div>
    );
  }, [data, handleReload, isError, loading, t]);

  // init data
  useEffect(() => {
    dispatch(
      actions.setPeriodCallActivity({ value: DASHBOARD_PERIOD[0].value })
    );
  }, [dispatch]);

  // fetch data
  useEffect(() => {
    handleReload();
  }, [handleReload]);

  // handle height loading
  useLayoutEffect(() => {
    ref.current!.style.height = loading ? '221px' : 'auto';
  }, [loading]);

  return (
    <>
      <HeaderCallActivity title={title} />
      <div ref={ref} className={classNames('card card-dashboard', loading)}>
        {content}
      </div>
    </>
  );
};

export default DashboardCallActivity;
