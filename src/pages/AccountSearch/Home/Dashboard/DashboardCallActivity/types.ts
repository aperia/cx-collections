export interface CallActivityState {
  data?: MagicKeyValue;
  loading?: boolean;
  error?: boolean;
  period?: string;
}
