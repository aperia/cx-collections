import React from 'react';
import { screen } from '@testing-library/react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import DashboardCallActivity from './';
import { dashboardActions } from '../_redux/reducers';

jest.mock('app/_libraries/_dls/components/DoughnutChart', () => () => {
  return <div data-testid="chart">DoughnutChart</div>;
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <DashboardCallActivity title="Call Activity" userType="CXCOL5" />,
    { initialState }
  );
};

describe('Render', () => {
  it('Render UI', async () => {
    renderWrapper({
      dashboard: {
        callActivity: {
          data: {
            totalBreaks: '20',
            totalBreakTime: '40',
            averageTimePerCall: '323',
            inboundCall: '22',
            outboundCall: '85'
          }
        }
      }
    });

    expect(screen.getByText('Call Activity')).toBeInTheDocument();
  });

  it('Render UI with error', async () => {
    const spyAction = mockActionCreator(dashboardActions);
    const spyGetDashboardCallActivity = spyAction('getDashboardCallActivity');

    jest.useFakeTimers();
    renderWrapper({ dashboard: { callActivity: { error: true } } });
    jest.runAllTimers();

    expect(screen.getByText('txt_failure_ocurred_on')).toBeInTheDocument();

    const btnReload = screen.getByRole('button');
    btnReload.click();
    expect(spyGetDashboardCallActivity).toBeCalledTimes(2);
  });

  it('Render UI in loading', async () => {
    const { baseElement } = renderWrapper({
      dashboard: { callActivity: { loading: true } }
    });

    expect(
      queryByClass(baseElement as HTMLElement, /loading/)
    ).not.toBeInTheDocument();
  });

  it('Render UI in non loading', async () => {
    renderWrapper({
      dashboard: { callActivity: { loading: false } }
    });
  });
});
