import { createSelector } from '@reduxjs/toolkit';

const getDashboardCallActivity = (states: RootState) =>
  states.dashboard.callActivity;

export const getData = createSelector(
  getDashboardCallActivity,
  data => data?.data
);

export const getLoading = createSelector(
  getDashboardCallActivity,
  data => data?.loading
);

export const getError = createSelector(
  getDashboardCallActivity,
  data => data?.error
);

export const getPeriod = createSelector(
  getDashboardCallActivity,
  data => data?.period
);
