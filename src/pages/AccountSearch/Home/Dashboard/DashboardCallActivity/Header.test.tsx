import React from 'react';
import { act, fireEvent, screen } from '@testing-library/react';
import { mockActionCreator, renderMockStore } from 'app/test-utils';
import HeaderCallActivity from './Header';
import { DASHBOARD_DROPDOWN_LIST } from '../constants';
import { queryByClass } from 'app/_libraries/_dls/test-utils';
import userEvent from '@testing-library/user-event';
import { dashboardActions } from '../_redux/reducers';

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

HTMLCanvasElement.prototype.getContext = jest.fn();

const title = 'Call Activity';

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(<HeaderCallActivity title={title} />, {
    initialState
  });
};

describe('Render', () => {
  it('Render UI', async () => {
    const mockActions = mockActionCreator(dashboardActions);

    const setPeriodCallActivity = mockActions('setPeriodCallActivity');

    jest.useFakeTimers();
    const { container } = renderWrapper({
      dashboard: { callActivity: { period: DASHBOARD_DROPDOWN_LIST[0] } }
    });
    jest.runAllTimers();

    const element = queryByClass(container, 'text-field-container');
    act(() => {
      userEvent.click(element!);
    });

    jest.runAllTimers();
    const item = screen.getByText('txt_month');
    expect(item).toBeInTheDocument();

    fireEvent.click(item);
    jest.runAllTimers();

    fireEvent.blur(item);

    expect(setPeriodCallActivity).toHaveBeenCalledWith({
      value: { description: 'txt_month', value: 'month' }
    });
    expect(screen.getByText(title)).toBeInTheDocument();
  });
});
