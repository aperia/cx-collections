import { getNumber, convertSecondsNumberToDDHHMMSS } from './helpers';

describe('test function getNumber', () => {
  it('should return number (happy case)', () => {
    const result = getNumber(10);
    expect(result).toEqual(10);
  });

  it('should return number with 0 is param', () => {
    const result = getNumber(0);
    expect(result).toEqual(0);
  });
});

describe('test function convertSecondsNumberToDDHHMMSS', () => {
  it('should return array (happy case), secs < 10', () => {
    const result = convertSecondsNumberToDDHHMMSS(5);
    expect(result).toEqual('5s');
  });

  it('should return array (happy case), secs > 10', () => {
    const result = convertSecondsNumberToDDHHMMSS(90061);
    expect(result).toEqual('1d01h01m01s');
  });

  it('should return array (happy case), secs = 1', () => {
    const result = convertSecondsNumberToDDHHMMSS(1);
    expect(result).toEqual('1s');
  });

  it('should return array with non second argument', () => {
    const result = convertSecondsNumberToDDHHMMSS(5);
    expect(result).toEqual('5s');
  });

  it('should return 0s', () => {
    const result = convertSecondsNumberToDDHHMMSS(0);
    expect(result).toEqual('0s');
  });
});
