import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import * as selector from './selectors';
import { DASHBOARD_DROPDOWN_LIST } from '../constants';

const store = createStore(rootReducer);
const state = store.getState();

state.dashboard = {
  callActivity: {
    error: false,
    loading: false,
    period: DASHBOARD_DROPDOWN_LIST[0],
    data: {
      totalBreaks: '20',
      totalBreakTime: '40',
      averageTimePerCall: '323',
      inboundCall: '22',
      outboundCall: '85'
    }
  }
};

describe('DashboardCallActivity selector', () => {
  it('selectData', () => {
    const result = selector.getData(state);
    expect(result).toEqual(state.dashboard.callActivity.data);
  });

  it('getError', () => {
    const result = selector.getError(state);
    expect(result).toEqual(state.dashboard.callActivity.error);
  });

  it('getLoading', () => {
    const result = selector.getLoading(state);
    expect(result).toEqual(state.dashboard.callActivity.loading);
  });

  it('getPeriod', () => {
    const result = selector.getPeriod(state);
    expect(result).toEqual(DASHBOARD_DROPDOWN_LIST[0]);
  });
});
