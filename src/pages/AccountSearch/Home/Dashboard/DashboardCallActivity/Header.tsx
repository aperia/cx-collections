import React, { useCallback } from 'react';

// redux
import { useDispatch } from 'react-redux';
import { dashboardActions as actions } from '../_redux/reducers';

// helpers
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { I18N_DASH_BOARD } from '../constants';
import DashboardDropdownList from '../common/DashboardDropdownList';
import { genAmtId } from 'app/_libraries/_dls/utils';

interface Props {
  title?: string;
}

const HeaderCallActivity: React.FC<Props> = ({ title }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const handleBlur = useCallback(
    event => {
      dispatch(actions.setPeriodCallActivity({ value: event.value }));
    },
    [dispatch]
  );

  return (
    <div className="mb-16 d-flex justify-content-between">
      <h4
        className="title"
        data-testid={genAmtId('callActivity_header', 'title', '')}
      >
        {title}
      </h4>
      <div className="mr-n8">
        <span
          className="color-grey fw-500 mr-4"
          data-testid={genAmtId('callActivity_header', 'period', '')}
        >
          {t(I18N_DASH_BOARD.PERIOD)}:
        </span>
        <DashboardDropdownList
          onBlur={handleBlur}
          dataTestId="callActivity_header_period-dropdown"
        />
      </div>
    </div>
  );
};

export default HeaderCallActivity;
