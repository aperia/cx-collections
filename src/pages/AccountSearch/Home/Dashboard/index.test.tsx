import React from 'react';
import { screen } from '@testing-library/dom';
//components
import Dashboard from './';

import { renderMockStore } from 'app/test-utils';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

jest.mock('app/_libraries/_dls/components/DoughnutChart', () => () => {
  return <div data-testid="chart">DoughnutChart</div>;
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(<Dashboard />, { initialState });
};

describe('Dashboard Goals', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app'
    }
  };
  const initialState = {};
  it('should render UI', () => {
    renderWrapper(initialState);
    expect(screen.getByTestId('Dashboard_title')).toBeInTheDocument();
  });

  it('should render UI with role is admin', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app',
        isAdminRole: true,
        isCollector: false
      }
    };

    renderWrapper({});
    expect(screen.getByText('txt_collector_statistics')).toBeInTheDocument();
    expect(screen.getByText('txt_team_goal')).toBeInTheDocument();
    expect(screen.getByText('txt_team_call_activity')).toBeInTheDocument();
  });

  it('should render UI with role is admin with PROD env', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app',
        isAdminRole: true,
        isCollector: false,
        selectedEnv: 'PROD'
      }
    };

    renderWrapper({});
    expect(
      screen.queryByText('txt_collector_statistics')
    ).not.toBeInTheDocument();
    expect(
      screen.queryByText('txt_team_work_statistics')
    ).not.toBeInTheDocument();
  });

  it('should render UI with role is user', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app',
        isAdminRole: false,
        isCollector: true
      }
    };

    renderWrapper({});
    expect(screen.getByTestId('Dashboard_title')).toBeInTheDocument();
  });

  it('should render UI with role is user > case 2', () => {
    window.appConfig = {} as AppConfiguration;

    renderWrapper({});
    expect(screen.getByTestId('Dashboard_title')).toBeInTheDocument();
  });
});
