import { dashboardServices } from './dashboardServices';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';
import { DashboardRequest } from './types';

const mockRequest = {
  collectorId: '384'
} as DashboardRequest;

describe('dashboardServices', () => {
  describe('getDashboardCallActivity', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      dashboardServices.getDashboardCallActivity();

      expect(mockService).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      dashboardServices.getDashboardCallActivity();

      expect(mockService).toBeCalledWith(
        apiUrl.dashboard.getDashboardCallActivity
      );
    });
  });

  describe('getDashboardGoal', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      dashboardServices.getDashboardGoal();

      expect(mockService).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      dashboardServices.getDashboardGoal();

      expect(mockService).toBeCalledWith(apiUrl.dashboard.getDashboardGoal);
    });
  });

  describe('getDashboardCollectorWorkStatistics', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      dashboardServices.getDashboardCollectorWorkStatistics(mockRequest);

      expect(mockService).toBeCalledWith('', mockRequest);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      dashboardServices.getDashboardCollectorWorkStatistics(mockRequest);

      expect(mockService).toBeCalledWith(
        apiUrl.dashboard.getDashboardCollectorWorkStatistics,
        mockRequest
      );
    });
  });

  describe('getDashboardSupervisorWorkStatistics', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      dashboardServices.getDashboardSupervisorWorkStatistics();

      expect(mockService).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      dashboardServices.getDashboardSupervisorWorkStatistics();

      expect(mockService).toBeCalledWith(
        'refData/dashboard/dashboardWorkStatistics.json'
      );
    });
  });

  describe('getDashboardWorkQueue', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      dashboardServices.getDashboardWorkQueue(mockRequest);

      expect(mockService).toBeCalledWith('', mockRequest);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      dashboardServices.getDashboardWorkQueue(mockRequest);

      expect(mockService).toBeCalledWith(
        apiUrl.dashboard.getDashboardWorkQueue,
        mockRequest
      );
    });
  });

  describe('getDashboardCollector', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      dashboardServices.getDashboardCollector();

      expect(mockService).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      dashboardServices.getDashboardCollector();

      expect(mockService).toBeCalledWith(
        apiUrl.dashboard.getDashboardWorkCollector
      );
    });
  });

  describe('getDashboardCollector', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      dashboardServices.getDashboardCollector();

      expect(mockService).toBeCalledWith('');
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      dashboardServices.getDashboardCollector();

      expect(mockService).toBeCalledWith(
        apiUrl.dashboard.getDashboardWorkCollector
      );
    });
  });

  describe('getRecentlyWorkQueue', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();
      const params = {
        collectorId: '384',
        collectorName: 'collectorName'
      } as DashboardRequest;
      dashboardServices.getRecentlyWorkQueue(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();
      const params = {
        collectorId: '384',
        collectorName: 'collectorName'
      } as DashboardRequest;
      dashboardServices.getRecentlyWorkQueue(params);

      expect(mockService).toBeCalledWith(
        apiUrl.dashboard.getRecentlyWorkQueue,
        params
      );
    });
  });

  describe('getNextAccountWorkOnQueue', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();
      const params = {
        common: {
          org: 'org',
          app: 'app'
        },
        queueId: 'queueId'
      };
      dashboardServices.getNextAccountWorkOnQueue(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();
      const params = {
        common: {
          org: 'org',
          app: 'app'
        },
        queueId: 'queueId'
      };
      dashboardServices.getNextAccountWorkOnQueue(params);

      expect(mockService).toBeCalledWith(
        apiUrl.dashboard.getNextAccountWorkOnQueue,
        params
      );
    });
  });

  describe('updateCollectionAccount', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();
      dashboardServices.updateCollectionAccount({});

      expect(mockService).toBeCalledWith('', {});
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();
      const params = {
        accountToken: 'abc',
        homePhone: 'abc',
        workPhone: 'abc-mock',
        nextWorkDate: 'abc-mock',
        permCollCd: 'abc-mock',
        extStatus: 'abc-mock',
        statusRcd: 'abc-mock',
        agentId: 'abc-mock'
      };
      dashboardServices.updateCollectionAccount(params);

      expect(mockService).toBeCalledWith(
        apiUrl.dashboard.updateCollectionAccount,
        params
      );
    });
  });

  describe('getAccountExtendLockTime', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();
      const params = {
        common: {
          clientNumber: '0000',
          system: '0000',
          prin: '0000',
          agent: '0000',
          user: '0000',
          app: '0000',
          org: '0000',
          accountId: '0000'
        }
      };

      dashboardServices.getAccountExtendLockTime(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      const params = {
        common: {
          clientNumber: '0000',
          system: '0000',
          prin: '0000',
          agent: '0000',
          user: '0000',
          app: '0000',
          org: '0000',
          accountId: '0000'
        }
      };

      dashboardServices.getAccountExtendLockTime(params);

      expect(mockService).toBeCalledWith(
        apiUrl.dashboard.getAccountExtendLockTime,
        params
      );
    });
  });

  describe('getAccountLockStatusInquiry', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();
      const params = {
        common: {
          clientNumber: '0000',
          system: '0000',
          prin: '0000',
          agent: '0000',
          user: '0000',
          app: '0000',
          org: '0000',
          accountId: '0000'
        }
      };

      dashboardServices.getAccountLockStatusInquiry(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      const params = {
        common: {
          clientNumber: '0000',
          system: '0000',
          prin: '0000',
          agent: '0000',
          user: '0000',
          app: '0000',
          org: '0000',
          accountId: '0000'
        }
      };

      dashboardServices.getAccountLockStatusInquiry(params);

      expect(mockService).toBeCalledWith(
        apiUrl.dashboard.getAccountLockStatusInquiry,
        params
      );
    });
  });

  describe('getAccountLockTimeUpdate', () => {
    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();
      const params = {
        common: {
          app: '0000',
          org: '0000',
          accountId: '0000'
        },
        acctWorkStatus: '0'
      };

      dashboardServices.getAccountLockTimeUpdate(params);

      expect(mockService).toBeCalledWith('', params);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      const params = {
        common: {
          app: '0000',
          org: '0000',
          accountId: '0000'
        },
        acctWorkStatus: '0'
      };

      dashboardServices.getAccountLockTimeUpdate(params);

      expect(mockService).toBeCalledWith(
        apiUrl.dashboard.getAccountLockTimeUpdate,
        params
      );
    });
  });
});
