import React, { useLayoutEffect, useRef } from 'react';

//translate
import { useTranslation } from 'app/_libraries/_dls/hooks';

//constants
import { I18N_DASH_BOARD, TITLE_CODE } from './constants';

//components
import DashboardGoals from './DashboardGoals';
import DashBoardCollector from './Collector';
import DashboardWorkStatistics from './DashboardWorkStatistics';
import DashboardCallActivity from './DashboardCallActivity';
import DashBoardWorkQueue from './DashboardWorkedQueue';
import DashboardRecentWorkQueue from './DashboardRecentWorkqueue';

//helpers
import { dynamicTitle } from './helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';

const DashBoard: React.FC = () => {
  const { t } = useTranslation();
  const { isAdminRole, isCollector, selectedEnv, isSupervisor } =
    window.appConfig.commonConfig || {};
  const ref = useRef<HTMLDivElement>(null);

  useLayoutEffect(() => {
    ref.current && (ref.current.style.minHeight = 'calc(100vh - 230px)');
  }, [ref]);

  return (
    <div ref={ref} className="dash-board">
      {!isAdminRole && (
        <h3
          className="text-center mb-24"
          data-testid={genAmtId('Dashboard', 'title', '')}
        >
          {t(I18N_DASH_BOARD.DASH_BOARD)}
        </h3>
      )}
      <div className="dash-board-content">
        <div className="d-xl-flex">
          {selectedEnv !== 'PROD' && (
            <div className="flex-1 mr-xl-24">
              <div className="db-goal">
                <DashboardGoals title={`${t(dynamicTitle(TITLE_CODE.GOAL))}`} />
              </div>
              <div className="db-call-activity mt-24">
                <DashboardCallActivity
                  title={`${t(dynamicTitle(TITLE_CODE.CALL_ACTIVITY))}`}
                />
              </div>
            </div>
          )}
          {isAdminRole && selectedEnv === 'PROD' ? null : (
            <div className="db-work-statistics">
              <DashboardWorkStatistics
                title={`${t(dynamicTitle(TITLE_CODE.WORK_STATISTIC))}`}
              />
            </div>
          )}
        </div>

        {(isCollector || isSupervisor) && (
          <div className="db-work-queue mt-24">
            <DashBoardWorkQueue
              title={`${t(dynamicTitle(TITLE_CODE.WORK_QUEUE))}`}
            />

            <div className="db-recent-work-queue mt-24">
              <DashboardRecentWorkQueue />
            </div>
          </div>
        )}

        {isAdminRole && selectedEnv !== 'PROD' && (
          <div className="db-collector-statistics mt-24">
            <DashBoardCollector
              title={`${t(dynamicTitle(TITLE_CODE.COLLECTOR_STATISTIC))}`}
            />
          </div>
        )}
      </div>
    </div>
  );
};

export default DashBoard;
