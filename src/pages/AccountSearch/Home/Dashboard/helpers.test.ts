import {
  averagePercent,
  dynamicTitle,
  formatPeriod,
  numberToPercentString,
  padLeadingZeros,
  randomQueueType
} from './helpers';

describe('dashboard helpers', () => {
  it('should dynamicTitle with admin role', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app',
        isAdminRole: true
      }
    };
    expect(dynamicTitle('G')).toEqual('txt_team_goal');

    expect(dynamicTitle('WS')).toEqual('txt_team_work_statistics');

    expect(dynamicTitle('CS')).toEqual('txt_collector_statistics');

    expect(dynamicTitle('CA')).toEqual('txt_team_call_activity');
  });

  it('should dynamicTitle with user role', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app'
      }
    };
    expect(dynamicTitle('G')).toEqual('txt_team_goal');

    expect(dynamicTitle('WS')).toEqual('txt_team_work_statistics');

    expect(dynamicTitle('WQ')).toEqual('txt_work_queue');

    expect(dynamicTitle('CA')).toEqual('txt_team_call_activity');
  });

  it('should dynamicTitle with user role > case 2', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: undefined
    };
    expect(dynamicTitle('G')).toEqual('txt_my_goal');

    expect(dynamicTitle('WS')).toEqual('txt_work_statistics');

    expect(dynamicTitle('WQ')).toEqual('txt_work_queue');

    expect(dynamicTitle('CA')).toEqual('txt_call_activity');
  });

  it('should numberToPercentString ', () => {
    expect(numberToPercentString(10)).toEqual('10%');
  });

  it('should averagePercent', () => {
    expect(averagePercent(100, 20)).toEqual('20%');
  });

  it('should formatPeriod', () => {
    expect(formatPeriod('today')).toBeTruthy();

    expect(formatPeriod('month')).toBeTruthy();

    expect(formatPeriod('year')).toBeTruthy();

    expect(formatPeriod('default')).toBeTruthy();
  });

  it('should padLeadingZeros', () => {
    expect(padLeadingZeros(1, 1)).toEqual('1');
    expect(padLeadingZeros(1, 2)).toEqual('01');
  });

  it('should randomQueueType', () => {
    expect(randomQueueType(1)).toEqual('Push');
    expect(randomQueueType(0)).toEqual('Pull');
  });
});
