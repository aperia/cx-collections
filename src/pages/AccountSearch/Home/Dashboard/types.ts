import { IQueueAccount } from './QueueDetail/types';
import { QueueTypeEnum } from 'pages/AccountSearch/Home/Dashboard/QueueDetail/types';
import { CallActivityState } from './DashboardCallActivity/types';

export interface DashBoardProps {
  title?: string;
}

export interface DashBoardGoalProps {
  accountTitle: string | undefined;
  items: any;
  amountTitle: string;
  goalType: string;
}

export interface DashBoardAccountStatisticsProps {
  leftItem: string;
  leftItemTextColor?: 'blue' | 'grey' | 'green' | 'red';
  leftTitle: string;
  rightItem: string;
  rightItemTextColor?: 'green' | 'red' | 'blue' | 'grey';
  rightTitle: string;
  border?: 'dashed' | 'line' | 'none';
  isCoreView?: boolean;
  isQueueStatistics?: boolean;
  dataTestId?: string;
}

export interface DashboardState {
  goal?: {
    data?: MagicKeyValue;
    loading?: boolean;
    error?: string;
  };
  workStatistic?: {
    data?: any;
    loading?: boolean;
    selectedPeriod?: RefDataValue;
    error?: string;
  };
  workQueue?: {
    data?: any;
    loading?: boolean;
    error?: string;
  };
  recentWorkQueue?: {
    data?: any;
    loading?: boolean;
    error?: string;
  };
  collector?: {
    data?: any;
    loading?: boolean;
    selectedPeriod?: RefDataValue;
    error?: string;
  };
  callActivity?: CallActivityState;
  accountLockTime?: number;
  accountCountTime?: Record<string, AccCountTimeType>;
  queueAccountToken?: string;
  extendLockTime?: number;
  resetTime?: boolean;
  queueAccount?: {
    previousAccount?: IQueueAccount;
    currentAccount: IQueueAccount;
  }
}

export interface AccCountTimeType {
  toggleExtendModal: boolean;
}

export interface GetDashboardGoalArgs {}

export interface GetDashboardGoalPayload {
  data: MagicKeyValue;
}

export interface GetDashboardStatisticsArgs {
  period: string;
}
export interface GetDashboardWorkQueueArg {
  period: string;
}

export interface DashboardWorkQueuePayload {
  totalQueues: string;
  collectorQueueStats: string;
  collectorPromiseStats: string;
}

export interface RecentlyWorkQueuePayload {
  queueName: string;
  queueId: string;
  collectorQueueStats: MagicKeyValue;
  collectorPromiseStats: MagicKeyValue;
  queueType: string;
  lastWorkDate: string;
  assignedTo: string;
  totalNumbersOfAccount: number | string;
  totalOutstandingBalanceAmount: string;
}
export interface GetDashboardWorkQueuePayload {
  data: DashboardWorkQueuePayload;
}
export interface GetRecentlyWorkQueuePayload {
  data: RecentlyWorkQueuePayload;
}
export interface GetDashboardCollectorArgs extends GetDashboardGoalArgs {
  period: string;
}

export interface GetDashboardStatisticsPayload
  extends GetDashboardGoalPayload {}

export interface GetDashboardCollectorPayload extends GetDashboardGoalPayload {}

export interface DBGoal {
  accountValue: string;
  amountValue: string;
  accountWorked: string;
  amountCollected: string;
}

export interface RecentlyWorkedQueueProps {
  queueName: string;
  queueId: string;
  collectorQueueStats: MagicKeyValue;
  collectorPromiseStats: MagicKeyValue;
  queueType: string;
  listWorkQueueAccount?: any;
  lastWorkDate: string;
  assignedTo: string;
  totalNumbersOfAccount: number | string;
  totalOutstandingBalanceAmount: string;
}

export interface TriggerNextAccountWorkOnQueueArgs {
  queueId: string;
  inAccountDetail?: boolean;
  inQueueDetail?: boolean;
  storeId?: string;
  queueType?: QueueTypeEnum;
  accountNumber?: string;
  eValue?: string;
}

export interface CollectorPromises {
  name?: string;
  value?: number;
  amount?: number;
  averageValue?: number;
  averageAmount?: number;
}

export interface OwnerModel {
  userId?: string;
  userName?: string;
  fullName?: string;
}

export interface CollectorDetail {
  collectorId?: string;
  owner?: OwnerModel;
  workQueue?: number;
  accountWorked?: number;
  totalAccountWorked?: number;
  amountCollected?: number;
  totalAmountCollected?: number;
  accountAverage?: number;
  averageCollected?: number;
  promises?: CollectorPromises[];
  breakTotal?: number;
  breakTime?: number;
  inbound?: number;
  outbound?: number;
  averageTime?: number;
  averageBreakTotal?: number;
  averageBreakTime?: number;
  averageInbound?: number;
  averageOutbound?: number;
  teamAverageTime?: number;
}

export interface DashboardRequest {
  startDateTime?: string;
  endDateTime?: string;
  resultSetSize?: string;
}

export interface GetNextAccountArgs {
  requestBody: GetNextAccountRequest;
  queueType?: QueueTypeEnum;
  accountNumber?: string;
}

export interface GetNextAccountRequest {
  common: {
    org: string;
    app: string;
  };
  queueId: string;
  nextWorkDate: string;
}

export interface GetNextAccountWorkOnQueuePayload {
  data: any;
}
export interface GetAccountLockRequest {
  common: {
    clientNumber?: string;
    system?: string;
    prin?: string;
    agent?: string;
    user?: string;
    app?: string;
    org?: string;
    accountId?: string;
  };
  accountToken?: string;
  acctWorkStatus?: string;
}

export interface GetAccountLockArg {
  clientNumber?: string;
  system?: string;
  prin?: string;
  agent?: string;
  accountId?: string;
  storeId?: string;
  acctWorkStatus?: string;
  relatedStoreId?: string;
  accEValue?: string;
}

interface AccountLockTimer {
  status?: string;
  accountToken?: string;
  lockAcquiredTime?: string;
  configuredLockTimeInterval?: string;
  lockExpiryTimeInterval?: string;
}

export interface GetAccountLockPayload {
  data: AccountLockTimer;
}

export interface UpdateCollectionAccountRequest {
  accountToken?: string;
  homePhone?: string;
  workPhone?: string;
  nextWorkDate?: string;
  permCollCd?: string;
  extStatus?: string;
  statusRcd?: string;
  agentId?: string;
}

export interface UpdateCollectionAccountArgs
  extends UpdateCollectionAccountRequest {}

interface UpdateCollectionAccount {
  success?: boolean;
  statusMessage?: string;
  status?: string;
}

export interface UpdateCollectionAccountPayload {
  data: UpdateCollectionAccount;
}

export interface AccountExtendLockTimePayload {
  storeId: string;
  message?: string;
}
