import queueAccounts from './queueAccounts.json';

// types
import { IQueueAccount } from '../types';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import {
  IGetQueueAccountsArgs,
  IGetQueueAccountsPayload
} from '../_redux/getQueueAccounts';

let data = [] as IQueueAccount[];

data = JSON.parse(JSON.stringify(queueAccounts)).data;

const TIME_DELAY = 600;
const isHaveError = window.location.href.includes('error');
const isHaveEmpty = window.location.href.includes('empty');

export const queueAccountsController = {
  getAccounts(args: IGetQueueAccountsArgs) {
    const { searchValues } = args;

    let filteredData = [...data];

    (searchValues as AttributeSearchValue[]).forEach(searchValue => {
      const { key, value } = searchValue;

      const keyword = value?.keyword || value || '';
      filteredData = filteredData.filter((item: IQueueAccount) => {
        const itemValue = item[key as keyof IQueueAccount] || '';
        if (Array.isArray(keyword)) {
          const [min, max] = keyword;

          return +itemValue >= +min && +itemValue <= +max;
        }

        return itemValue.toLowerCase() === keyword;
      });
    });

    return new Promise<IGetQueueAccountsPayload>(function (resolve, reject) {
      setTimeout(() => {
        if (isHaveError) {
          return reject({
            data: {
              success: false,
              data: [],
              message: 'Failed to get accounts'
            }
          });
        }

        resolve({
          success: true,
          data: isHaveEmpty ? [] : filteredData
        });
      }, TIME_DELAY);
    });
  },
  getNextAccount({ accountNumber }: { accountNumber?: string }) {
    let previousAccount: IQueueAccount | undefined;
    let currentAccount = data[0];

    if (accountNumber) {
      const accountIndex = data.findIndex(
        (acc: IQueueAccount) => acc.accountNumber === accountNumber
      );

      if (accountIndex !== -1) {
        previousAccount = data[accountIndex];
        currentAccount = data[accountIndex + 1];
      }
    }

    return new Promise<{
      success: boolean;
      data: {
        previousAccount?: IQueueAccount;
        currentAccount: IQueueAccount;
      };
    }>(function (resolve, reject) {
      setTimeout(() => {
        resolve({
          success: true,
          data: {
            previousAccount,
            currentAccount
          }
        });
      }, TIME_DELAY);
    });
  }
};
