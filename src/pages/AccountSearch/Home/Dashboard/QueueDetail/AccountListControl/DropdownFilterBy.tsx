import React, { useCallback, useMemo } from 'react';

// components
import {
  DropdownBaseChangeEvent,
  DropdownList,
  DropdownListProps
} from 'app/_libraries/_dls/components';

// helpers
import classnames from 'classnames';
import { isUndefined } from 'lodash';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface DropdownFilterByProps {
  data: any[];
  value: any;
  label?: string;
  labelClassName?: string;
  onChange: Function;
  dropdownProps?: Omit<DropdownListProps, 'children'>;
  dataTestId?: string;
}

const DropdownFilterBy: React.FC<DropdownFilterByProps> = ({
  value: valueProps,
  data,
  onChange,
  label,
  labelClassName = '',
  dropdownProps = {},
  dataTestId
}) => {
  const { t } = useTranslation();
  const handleChange = useCallback(
    (e: DropdownBaseChangeEvent) => {
      const { value: newValue } = e;

      onChange(newValue);
    },
    [onChange]
  );

  const value = useMemo(
    () => data.find(item => item.value === valueProps),
    [data, valueProps]
  );

  return (
    <div className="d-flex align-items-center">
      {!isUndefined(label) && (
        <span className={classnames(labelClassName)}>{label}:</span>
      )}

      <DropdownList
        // variant="no-border"
        dataTestId={dataTestId}
        value={value}
        onChange={handleChange}
        textField="description"
        {...dropdownProps}
        noResult={t('txt_no_results_found')}
      >
        {data.map((item, index) => (
          <DropdownList.Item
            key={index}
            label={item.description}
            value={item}
          />
        ))}
      </DropdownList>
    </div>
  );
};

export default React.memo(DropdownFilterBy);
