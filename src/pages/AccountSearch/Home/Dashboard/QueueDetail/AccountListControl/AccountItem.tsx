import React, { useCallback } from 'react';

// components
import GroupTextControl from 'app/components/_dof_controls/controls/GroupTextControl';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { GroupTextFormat } from 'app/constants/enums';

// helpers
import { maskAccount } from 'app/helpers';
import { IQueueAccount } from '../types';

export interface AccountItemProps {
  data: IQueueAccount;
  onItemClick?: (accountNumber: string) => void;
}
const AccountItem: React.FC<AccountItemProps> = ({ data, onItemClick }) => {
  const { t } = useTranslation();
  const {
    id,
    lastName,
    firstName,
    accountNumber,
    lastWorkDate,
    daysDelinquent,
    currentBalance,
    amountDelinquent,
    lastPaymentDate,
    timeZone
  } = data;

  const maskedValueFormat = maskAccount(accountNumber);

  const handleItemClick = useCallback(() => {
    if (!onItemClick) return;

    onItemClick(accountNumber);
  }, [accountNumber, onItemClick]);

  return (
    <div className="card-primary" onClick={handleItemClick}>
      <div className="card-item br-radius-8 p-16 mt-16">
        <p className="fw-500">{`${lastName} ${firstName}`}</p>
        <div className="row align-items-center justify-content-start">
          <GroupTextControl
            id={t(id)}
            input={
              {
                value: maskedValueFormat
              } as any
            }
            meta={{} as any}
            label={t(I18N_COMMON_TEXT.ACCOUNT_NUMBER)}
            options={{
              classNameProps: { container: 'col-xl-3 col-md-3 mt-12' },
              format: GroupTextFormat.MaskText
            }}
          />

          <GroupTextControl
            id={t(id)}
            input={{ value: lastWorkDate } as any}
            meta={{} as any}
            label={t(I18N_COMMON_TEXT.LAST_WORK_DATE)}
            options={{
              classNameProps: { container: 'col-xl-2 col-md-3 mt-12' },
              format: GroupTextFormat.Api_Date
            }}
          />

          <GroupTextControl
            id={t(id)}
            input={{ value: daysDelinquent } as any}
            meta={{} as any}
            label={t(I18N_COMMON_TEXT.DAYS_DELINQUENT)}
            options={{
              classNameProps: { container: 'col-xl-2 col-md-3 mt-12' }
            }}
          />

          <GroupTextControl
            id={t(id)}
            input={{ value: currentBalance } as any}
            meta={{} as any}
            label={t(I18N_COMMON_TEXT.CURRENT_BALANCE)}
            options={{
              classNameProps: { container: 'col-xl-2 col-md-3' },
              format: GroupTextFormat.Api_Amount
            }}
          />

          <GroupTextControl
            id={t(id)}
            input={{ value: amountDelinquent } as any}
            meta={{} as any}
            label={t(I18N_COMMON_TEXT.AMOUNT_DELINQUENT)}
            options={{
              classNameProps: { container: 'col-xl-3 col-md-3' },
              format: GroupTextFormat.Api_Amount
            }}
          />

          <GroupTextControl
            id={t(id)}
            input={{ value: lastPaymentDate } as any}
            meta={{} as any}
            label={t(I18N_COMMON_TEXT.LAST_PAYMENT_DATE)}
            options={{
              classNameProps: { container: 'col-xl-3 col-md-3' },
              format: GroupTextFormat.Api_Date
            }}
          />

          <GroupTextControl
            id={t(id)}
            input={{ value: timeZone } as any}
            meta={{} as any}
            label={t(I18N_COMMON_TEXT.TIME_ZONE)}
            options={{ classNameProps: { container: 'col-xl-2 col-md-3' } }}
          />
        </div>
      </div>
    </div>
  );
};

export default AccountItem;
