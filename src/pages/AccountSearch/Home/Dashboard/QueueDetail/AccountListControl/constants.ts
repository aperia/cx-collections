import { I18N_COMMON_TEXT } from 'app/constants/i18n';

export const ATTRIBUTE_SEARCH_FIELD = {
  ACCOUNT_NUMBER: {
    name: 'accountNumber',
    label: I18N_COMMON_TEXT.ACCOUNT_NUMBER,
    description: I18N_COMMON_TEXT.ACCOUNT_NUMBER_DESCRIPTION,
    groupName: I18N_COMMON_TEXT.SEARCH_PARAMETERS
  },
  AMOUNT_DELINQUENT: {
    name: 'amountDelinquent',
    label: I18N_COMMON_TEXT.AMOUNT_DELINQUENT,
    description: I18N_COMMON_TEXT.AMOUNT_DESCRIPTION,
    groupName: I18N_COMMON_TEXT.SEARCH_PARAMETERS
  },
  CURRENT_BALANCE: {
    name: 'currentBalance',
    label: I18N_COMMON_TEXT.CURRENT_BALANCE,
    description: I18N_COMMON_TEXT.AMOUNT_DESCRIPTION,
    groupName: I18N_COMMON_TEXT.SEARCH_PARAMETERS
  },
  DAYS_DELINQUENT: {
    name: 'daysDelinquent',
    label: I18N_COMMON_TEXT.DAYS_DELINQUENT,
    description: I18N_COMMON_TEXT.DAYS_DELINQUENT_DESCRIPTION,
    groupName: I18N_COMMON_TEXT.SEARCH_PARAMETERS
  },
  FIRST_NAME: {
    name: 'firstName',
    label: I18N_COMMON_TEXT.FIRST_NAME,
    description: I18N_COMMON_TEXT.FIRST_NAME_ONLY_DESCRIPTION,
    groupName: I18N_COMMON_TEXT.SEARCH_PARAMETERS
  },
  LAST_NAME: {
    name: 'lastName',
    label: I18N_COMMON_TEXT.LAST_NAME,
    description: I18N_COMMON_TEXT.LAST_NAME_DESCRIPTION,
    groupName: I18N_COMMON_TEXT.SEARCH_PARAMETERS
  },
  LAST_PAYMENT_DATE: {
    name: 'lastPaymentDate',
    label: I18N_COMMON_TEXT.LAST_PAYMENT_DATE,
    description: 'mm/dd/yyyy',
    groupName: I18N_COMMON_TEXT.SEARCH_PARAMETERS
  },
  LAST_WORK_DATE: {
    name: 'lastWorkDate',
    label: I18N_COMMON_TEXT.LAST_WORK_DATE,
    description: 'mm/dd/yyyy',
    groupName: I18N_COMMON_TEXT.SEARCH_PARAMETERS
  }
};
