import React from 'react';
//components
import AccountList from './AccountList';

// helpers
import { renderMockStore } from 'app/test-utils';

// mock data
import queueAccounts from '../__mocks__/queueAccounts.json';
import { IQueueAccount } from '../types';

const mockData = JSON.parse(JSON.stringify(queueAccounts)).data;

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const renderWrapper = (data: IQueueAccount[], isSearching?: boolean) => {
  return renderMockStore(
    <AccountList data={data} isSearching={isSearching} />,
    {
      initialState: {}
    }
  );
};

describe('AccountList Goals', () => {
  it('should render UI', () => {
    renderWrapper(mockData);
  });

  it('should render UI with error without search', () => {
    renderWrapper([], false);
  });

  it('should render UI with error', () => {
    renderWrapper([], true);
  });
});
