import { handleSort } from './helpers';

const dataDefault = [
  {
    id: '1',
    lastName: 'Smith',
    firstName: 'Jackie',
    currentBalance: '1000',
    accountNumber: '2812901234566986',
    lastWorkDate: '01-05-2022',
    daysDelinquent: '358',
    amountDelinquent: '1964.20',
    lastPaymentDate: '12-23-2021',
    timeZone: 'CST'
  },
  {
    id: '2',
    lastName: 'Harley',
    firstName: 'Murphy',
    currentBalance: '2000',
    accountNumber: '2812901234561111',
    lastWorkDate: '01-10-2022',
    daysDelinquent: '123',
    amountDelinquent: '1964.20',
    lastPaymentDate: '12-23-2021',
    timeZone: 'CST'
  }
];

const dataDefaultSortAsc = [
  {
    id: '1',
    lastName: 'Smith',
    firstName: 'Jackie',
    currentBalance: '1000',
    accountNumber: '2812901234566986',
    lastWorkDate: '01-05-2022',
    daysDelinquent: '358',
    amountDelinquent: '1964.20',
    lastPaymentDate: '12-23-2021',
    timeZone: 'CST'
  },
  {
    id: '2',
    lastName: 'Harley',
    firstName: 'Murphy',
    currentBalance: '2000',
    accountNumber: '2812901234561111',
    lastWorkDate: '01-10-2022',
    daysDelinquent: '123',
    amountDelinquent: '1964.20',
    lastPaymentDate: '12-23-2021',
    timeZone: 'CST'
  }
];

const dataDefaultSortDesc = [
  {
    id: '2',
    lastName: 'Harley',
    firstName: 'Murphy',
    currentBalance: '2000',
    accountNumber: '2812901234561111',
    lastWorkDate: '01-10-2022',
    daysDelinquent: '123',
    amountDelinquent: '1964.20',
    lastPaymentDate: '12-23-2021',
    timeZone: 'CST'
  },
  {
    id: '1',
    lastName: 'Smith',
    firstName: 'Jackie',
    currentBalance: '1000',
    accountNumber: '2812901234566986',
    lastWorkDate: '01-05-2022',
    daysDelinquent: '358',
    amountDelinquent: '1964.20',
    lastPaymentDate: '12-23-2021',
    timeZone: 'CST'
  }
];

describe('handleSort helpers', () => {
  it('handleSort with order by asc', () => {
    const result = handleSort(dataDefault, 'lastWorkDate', 'asc');
    expect(result).toEqual(dataDefaultSortAsc);
  });

  it('handleSort with order by desc', () => {
    const result = handleSort(dataDefault, 'currentBalance', 'desc');
    expect(result).toEqual(dataDefaultSortDesc);
  });
});
