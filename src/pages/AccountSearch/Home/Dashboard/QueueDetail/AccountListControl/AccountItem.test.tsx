import React from 'react';
//components
import AccountItem from './AccountItem';

// helpers
import { queryAllByClass, renderMockStore } from 'app/test-utils';

// mock data
import queueAccounts from '../__mocks__/queueAccounts.json';
import { IQueueAccount } from '../types';
import userEvent from '@testing-library/user-event';

const mockData = JSON.parse(JSON.stringify(queueAccounts)).data;

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const renderWrapper = (data: IQueueAccount) => {
  return renderMockStore(<AccountItem data={data} />, {
    initialState: {}
  });
};
describe('AccountItem Goals', () => {
  it('should render UI', () => {
    renderWrapper(mockData[0]);
  });

  it('should render UI', () => {
    const { wrapper } = renderWrapper(mockData[0]);

    userEvent.click(
      queryAllByClass(wrapper.baseElement as HTMLElement, /card-primary/)[0]
    );
  });
});
