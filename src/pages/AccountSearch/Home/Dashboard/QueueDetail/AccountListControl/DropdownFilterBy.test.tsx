import React from 'react';
import { fireEvent, screen } from '@testing-library/react';
import { queryByClass, renderMockStoreId } from 'app/test-utils';
import userEvent from '@testing-library/user-event';

import DropdownFilterBy from './DropdownFilterBy';
import { DropdownListProps } from 'app/_libraries/_dls/components';

HTMLCanvasElement.prototype.getContext = jest.fn();

const sortByData = [
  {
    value: 'currentBalance',
    description: 'Current Balance'
  },
  {
    value: 'daysDelinquent',
    description: 'Days Delinquent'
  },
  { value: 'lastWorkDate', description: 'Last Work Date' }
];

const onChange = jest.fn();

interface IRenderWrapperProps {
  value: string;
  labelClassName?: string;
  dropdownProps?: Omit<DropdownListProps, 'children'>;
}

const label = 'Dropdown Filter By';

const renderWrapper = (props: IRenderWrapperProps) => {
  return renderMockStoreId(
    <div>
      <DropdownFilterBy
        dataTestId={`DropdownFilterBy`}
        value={props.value}
        data={sortByData}
        onChange={onChange}
        label={label}
        labelClassName={props.labelClassName}
        dropdownProps={props.dropdownProps}
      />
    </div>,
    { initialState: {} }
  );
};

describe('Actions', () => {
  it('handleChangeDropDown render default', () => {
    const { wrapper } = renderWrapper({
      value: sortByData[2].value
    });
    expect(wrapper.getByText(sortByData[2].description)).toBeInTheDocument();
  });

  it('handleChangeDropDown on change', () => {
    const { wrapper } = renderWrapper({
      value: '',
      labelClassName: 'color-grey fs-14 fw-500',
      dropdownProps: { variant: 'no-border' }
    });
    const button = queryByClass(wrapper.container, /text-field-container/);

    userEvent.click(button!);
    const el = screen.queryAllByText('Last Work Date');
    fireEvent.click(el[0]!);
  });
});
