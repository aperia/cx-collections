import React from 'react';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { renderMockStore } from 'app/test-utils';
import useGetSortByData from './useGetSortByData';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

describe('useGetOrderByData hooks', () => {
  const initialState: Partial<RootState> = {
    clientConfiguration: {
      settings: {
        selectedConfig: {}
      },
      status: true
    }
  };

  const Comp = () => {
    const sortByData = useGetSortByData();

    return (
      <>
        {sortByData.map((item, key: number) => (
          <p key={key.toString()}>{item.description}</p>
        ))}
      </>
    );
  };

  it('should useGetOrderByData being render', () => {
    const { wrapper } = renderMockStore(<Comp />, { initialState });

    expect(wrapper.getByText(I18N_COMMON_TEXT.CURRENT_BALANCE)).toBeInTheDocument();
    expect(wrapper.getByText(I18N_COMMON_TEXT.DAYS_DELINQUENT)).toBeInTheDocument();
    expect(wrapper.getByText(I18N_COMMON_TEXT.LAST_WORK_DATE)).toBeInTheDocument();
  });
});
