import React from 'react';
import { render, screen } from '@testing-library/react';
import useGetSearchFieldData from './useGetSearchFieldData';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

jest.mock('app/_libraries/_dls/components/AttributeSearch/controls/InputControl', () => () => <div>InputControl</div>);
jest.mock('app/_libraries/_dls/components/AttributeSearch/controls/AmountRangeControl', () => () => (
  <div>AmountRangeControl</div>
));
jest.mock('app/_libraries/_dls/components/AttributeSearch/controls/MainSubConditionControl', () => () => (
  <div>MainSubConditionControl</div>
));
jest.mock('app/_libraries/_dls/components/AttributeSearch/controls/DatePickerControl', () => () => (
  <div>DatePickerControl</div>
));

interface LastNameProps {
  value?: any;
}

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('AccountNumberInputControl hooks', () => {
  const AccountNumberInputControl = () => {
    const searchFieldData = useGetSearchFieldData();
    const Component = searchFieldData[0].component;

    return <Component />;
  };

  it('should AccountNumberInputControl being render', () => {
    render(<AccountNumberInputControl />);
    expect(screen.getByText('InputControl')).toBeInTheDocument();
  });
});

describe('AmountDelinquentAmountRangeControl hooks', () => {
  const AmountDelinquentAmountRangeControl = () => {
    const searchFieldData = useGetSearchFieldData();
    const Component = searchFieldData[1].component;

    return <Component />;
  };

  it('should AmountDelinquentAmountRangeControl being render', () => {
    render(<AmountDelinquentAmountRangeControl />);
    expect(screen.getByText('AmountRangeControl')).toBeInTheDocument();
  });
});

describe('CurrentBalanceRangeControl hooks', () => {
  const CurrentBalanceRangeControl = () => {
    const searchFieldData = useGetSearchFieldData();
    const Component = searchFieldData[2].component;

    return <Component />;
  };

  it('should CurrentBalanceRangeControl being render', () => {
    render(<CurrentBalanceRangeControl />);
    expect(screen.getByText('AmountRangeControl')).toBeInTheDocument();
  });
});

describe('DayDelinquentInputControl hooks', () => {
  const DayDelinquentInputControl = () => {
    const searchFieldData = useGetSearchFieldData();
    const Component = searchFieldData[3].component;

    return <Component />;
  };

  it('should DayDelinquentInputControl being render', () => {
    render(<DayDelinquentInputControl />);
    expect(screen.getByText('InputControl')).toBeInTheDocument();
  });
});

describe('FirstNameInputControl hooks', () => {
  const FirstNameInputControl = () => {
    const searchFieldData = useGetSearchFieldData();
    const Component = searchFieldData[4].component;

    return <Component />;
  };

  it('should FirstNameInputControl being render', () => {
    render(<FirstNameInputControl />);
    expect(screen.getByText('InputControl')).toBeInTheDocument();
  });
});

describe('LastNameMainSubConditionControl hooks', () => {
  const LastNameMainSubConditionControl: React.FC<LastNameProps> = ({
    value
  }) => {
    const searchFieldData = useGetSearchFieldData();
    const Component = searchFieldData[5].component;

    return <Component value={value} />;
  };

  it('should LastNameMainSubConditionControl being render', () => {
    render(<LastNameMainSubConditionControl value="demo" />);
    expect(screen.getByText('MainSubConditionControl')).toBeInTheDocument();
  });

  it('should LastNameMainSubConditionControl being render with value empty', () => {
    render(<LastNameMainSubConditionControl value="" />);
    expect(screen.getByText('MainSubConditionControl')).toBeInTheDocument();
  });
});

describe('LastPaymentDateControl hooks', () => {
  const LastPaymentDateControl = () => {
    const searchFieldData = useGetSearchFieldData();
    const Component = searchFieldData[6].component;

    return <Component />;
  };

  it('should LastPaymentDateControl being render', () => {
    render(<LastPaymentDateControl />);
    expect(screen.getByText('DatePickerControl')).toBeInTheDocument();
  });
});

describe('LastWorkDateControl hooks', () => {
  const LastWorkDateControl = () => {
    const searchFieldData = useGetSearchFieldData();
    const Component = searchFieldData[7].component;

    return <Component />;
  };

  it('should LastWorkDateControl being render', () => {
    render(<LastWorkDateControl />);
    expect(screen.getByText('DatePickerControl')).toBeInTheDocument();
  });
});