import React, { useMemo } from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// components
import {
  AmountRangeControl,
  AttributeSearchData,
  DatePickerControl,
  InputControl,
  MainSubConditionControl,
  MainSubConditionValue
} from 'app/_libraries/_dls/components';

// constants
import { ATTRIBUTE_SEARCH_FIELD } from '../constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import {
  defaultExactness,
  mainSubConditionData,
  MESSAGE
} from 'pages/AccountSearch/constants';

const useGetSearchFieldData = (): AttributeSearchData[] => {
  const { t } = useTranslation();

  const data: AttributeSearchData[] = useMemo(
    () => [
      {
        key: ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER.name,
        name: t(ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER.label),
        description: t(ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER.description),
        component: props => (
          <InputControl
            placeholder={t(I18N_COMMON_TEXT.ENTER_KEYWORD)}
            maxLength={16}
            mode="text"
            {...props}
          />
        ),
        groupName: t(ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER.groupName)
      },
      {
        key: ATTRIBUTE_SEARCH_FIELD.AMOUNT_DELINQUENT.name,
        name: t(ATTRIBUTE_SEARCH_FIELD.AMOUNT_DELINQUENT.label),
        description: t(ATTRIBUTE_SEARCH_FIELD.AMOUNT_DELINQUENT.description),
        component: props => (
          <AmountRangeControl
            {...props}
            minProps={{ placeholder: t(I18N_COMMON_TEXT.MIN_AMOUNT) }}
            maxProps={{ placeholder: t(I18N_COMMON_TEXT.MAX_AMOUNT) }}
          />
        ),
        groupName: t(ATTRIBUTE_SEARCH_FIELD.AMOUNT_DELINQUENT.groupName)
      },
      {
        key: ATTRIBUTE_SEARCH_FIELD.CURRENT_BALANCE.name,
        name: t(ATTRIBUTE_SEARCH_FIELD.CURRENT_BALANCE.label),
        description: t(ATTRIBUTE_SEARCH_FIELD.CURRENT_BALANCE.description),
        component: props => (
          <AmountRangeControl
            {...props}
            minProps={{ placeholder: t(I18N_COMMON_TEXT.MIN_AMOUNT) }}
            maxProps={{ placeholder: t(I18N_COMMON_TEXT.MAX_AMOUNT) }}
          />
        ),
        groupName: t(ATTRIBUTE_SEARCH_FIELD.CURRENT_BALANCE.groupName)
      },
      {
        key: ATTRIBUTE_SEARCH_FIELD.DAYS_DELINQUENT.name,
        name: t(ATTRIBUTE_SEARCH_FIELD.DAYS_DELINQUENT.label),
        description: t(ATTRIBUTE_SEARCH_FIELD.DAYS_DELINQUENT.description),
        component: props => (
          <InputControl
            placeholder={I18N_COMMON_TEXT.ENTER_KEYWORD}
            mode="number"
            {...props}
          />
        ),
        groupName: t(ATTRIBUTE_SEARCH_FIELD.DAYS_DELINQUENT.groupName)
      },
      {
        key: ATTRIBUTE_SEARCH_FIELD.FIRST_NAME.name,
        name: t(ATTRIBUTE_SEARCH_FIELD.FIRST_NAME.label),
        description: t(ATTRIBUTE_SEARCH_FIELD.FIRST_NAME.description),
        component: props => (
          <InputControl
            placeholder={I18N_COMMON_TEXT.ENTER_KEYWORD}
            mode="text"
            {...props}
          />
        ),
        disabledFields: {
          [ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER.name]: t(
            MESSAGE.NO_COMBINE_WITH_FIRST_NAME
          ),
          [ATTRIBUTE_SEARCH_FIELD.AMOUNT_DELINQUENT.name]: t(
            MESSAGE.NO_COMBINE_WITH_FIRST_NAME
          ),
          [ATTRIBUTE_SEARCH_FIELD.CURRENT_BALANCE.name]: t(
            MESSAGE.NO_COMBINE_WITH_FIRST_NAME
          ),
          [ATTRIBUTE_SEARCH_FIELD.DAYS_DELINQUENT.name]: t(
            MESSAGE.NO_COMBINE_WITH_FIRST_NAME
          ),
          [ATTRIBUTE_SEARCH_FIELD.LAST_NAME.name]: t(
            MESSAGE.NO_COMBINE_WITH_FIRST_NAME
          ),
          [ATTRIBUTE_SEARCH_FIELD.LAST_PAYMENT_DATE.name]: t(
            MESSAGE.NO_COMBINE_WITH_FIRST_NAME
          ),
          [ATTRIBUTE_SEARCH_FIELD.LAST_WORK_DATE.name]: t(
            MESSAGE.NO_COMBINE_WITH_FIRST_NAME
          )
        },
        groupName: t(ATTRIBUTE_SEARCH_FIELD.FIRST_NAME.groupName)
      },
      {
        key: ATTRIBUTE_SEARCH_FIELD.LAST_NAME.name,
        name: t(ATTRIBUTE_SEARCH_FIELD.LAST_NAME.label),
        description: t(ATTRIBUTE_SEARCH_FIELD.LAST_NAME.description),
        component: ({ value, ...props }) => {
          const defaultValue: MainSubConditionValue = {
            level: {
              ...defaultExactness!,
              text: t(defaultExactness?.text)
            },
            keyword: undefined
          };
          return (
            <MainSubConditionControl
              data={mainSubConditionData}
              placeholder={I18N_COMMON_TEXT.ENTER_KEYWORD}
              value={value ? value : defaultValue}
              autoFocusDropdownList={false}
              autoFocusInput
              {...props}
            />
          );
        },
        disabledFields: {
          [ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER.name]: t(
            MESSAGE.NO_COMBINE_WITH_LAST_NAME
          ),
          [ATTRIBUTE_SEARCH_FIELD.AMOUNT_DELINQUENT.name]: t(
            MESSAGE.NO_COMBINE_WITH_LAST_NAME
          ),
          [ATTRIBUTE_SEARCH_FIELD.CURRENT_BALANCE.name]: t(
            MESSAGE.NO_COMBINE_WITH_LAST_NAME
          ),
          [ATTRIBUTE_SEARCH_FIELD.DAYS_DELINQUENT.name]: t(
            MESSAGE.NO_COMBINE_WITH_LAST_NAME
          ),
          [ATTRIBUTE_SEARCH_FIELD.FIRST_NAME.name]: t(
            MESSAGE.NO_COMBINE_WITH_LAST_NAME
          ),
          [ATTRIBUTE_SEARCH_FIELD.LAST_PAYMENT_DATE.name]: t(
            MESSAGE.NO_COMBINE_WITH_LAST_NAME
          ),
          [ATTRIBUTE_SEARCH_FIELD.LAST_WORK_DATE.name]: t(
            MESSAGE.NO_COMBINE_WITH_LAST_NAME
          )
        },
        groupName: t(ATTRIBUTE_SEARCH_FIELD.LAST_NAME.groupName)
      },
      {
        key: ATTRIBUTE_SEARCH_FIELD.LAST_PAYMENT_DATE.name,
        name: t(ATTRIBUTE_SEARCH_FIELD.LAST_PAYMENT_DATE.label),
        description: t(ATTRIBUTE_SEARCH_FIELD.LAST_PAYMENT_DATE.description),
        component: props => (
          <DatePickerControl
            popupBaseProps={{ placement: 'bottom-end' }}
            {...props}
          />
        ),
        groupName: t(ATTRIBUTE_SEARCH_FIELD.LAST_PAYMENT_DATE.groupName)
      },
      {
        key: ATTRIBUTE_SEARCH_FIELD.LAST_WORK_DATE.name,
        name: t(ATTRIBUTE_SEARCH_FIELD.LAST_WORK_DATE.label),
        description: t(ATTRIBUTE_SEARCH_FIELD.LAST_WORK_DATE.description),
        component: props => (
          <DatePickerControl
            popupBaseProps={{ placement: 'bottom-end' }}
            {...props}
          />
        ),
        groupName: t(ATTRIBUTE_SEARCH_FIELD.LAST_WORK_DATE.groupName)
      }
    ],
    [t]
  );

  return data;
};

export default useGetSearchFieldData;
