import React from 'react';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { renderMockStore } from 'app/test-utils';
import useGetOrderByData from './useGetOrderByData';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

describe('useGetOrderByData hooks', () => {
  const initialState: Partial<RootState> = {
    clientConfiguration: {
      settings: {
        selectedConfig: {}
      },
      status: true
    }
  };

  const Comp = () => {
    const orderByData = useGetOrderByData();

    return (
      <>
        {orderByData.map((item, key: number) => (
          <p key={key.toString()}>{item.description}</p>
        ))}
      </>
    );
  };

  it('should useGetOrderByData being render', () => {
    const { wrapper } = renderMockStore(<Comp />, { initialState });

    expect(wrapper.getByText(I18N_COMMON_TEXT.DESCENDING)).toBeInTheDocument();
    expect(wrapper.getByText(I18N_COMMON_TEXT.ASCENDING)).toBeInTheDocument();
  });
});
