import { useMemo } from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

const useGetSortByData = () => {
  const { t } = useTranslation();

  const data = useMemo(
    () => [
      {
        value: 'currentBalance',
        description: t(I18N_COMMON_TEXT.CURRENT_BALANCE)
      },
      {
        value: 'daysDelinquent',
        description: t(I18N_COMMON_TEXT.DAYS_DELINQUENT)
      },
      { value: 'lastWorkDate', description: t(I18N_COMMON_TEXT.LAST_WORK_DATE) }
    ],
    [t]
  );

  return data;
};

export default useGetSortByData;
