import { useMemo } from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

const useGetOrderByData = () => {
  const { t } = useTranslation();

  const data = useMemo(
    () => [
      { value: 'desc', description: t(I18N_COMMON_TEXT.DESCENDING) },
      { value: 'asc', description: t(I18N_COMMON_TEXT.ASCENDING) }
    ],
    [t]
  );

  return data;
};

export default useGetOrderByData;
