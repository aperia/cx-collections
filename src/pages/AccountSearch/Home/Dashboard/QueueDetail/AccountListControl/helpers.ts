import { orderBy } from 'app/_libraries/_dls/lodash';

export const handleSort = <T>(
  list: Array<T>,
  sortKey: string,
  orderKey: string
) => {
  const data = list.slice(0);

  let sortByValue: string | ((item: any) => Date)[] = sortKey;

  if (sortKey.toLowerCase().includes('date')) {
    sortByValue = [item => new Date(item[sortKey])];
  }

  const orderByValue = orderKey === 'asc' ? 'asc' : 'desc';

  const sortedData = orderBy(data, sortByValue, orderByValue);
  return sortedData;
};
