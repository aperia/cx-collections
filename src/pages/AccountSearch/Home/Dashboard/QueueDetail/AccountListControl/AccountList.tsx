import React from 'react';

// components
import { NoDataGrid, SearchNodata } from 'app/components';
import AccountItem from './AccountItem';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

// types
import { IQueueAccount } from '../types';

export interface AccountListProps {
  data: IQueueAccount[];
  isSearching?: boolean;
  onItemClick?: (accountNumber: string) => void;
  onClearSearch: () => void;
}
const AccountList: React.FC<AccountListProps> = ({
  data,
  onItemClick,
  onClearSearch,
  isSearching = false,
}) => {
  const testId = 'accountList';
  const { t } = useTranslation();

  if (!data.length) {
    if (isSearching) {
      return  <SearchNodata
      text={t('txt_no_search_results_found')}
      onClearSearch={onClearSearch}
    />
    }

    return (
      <>
        <div className="p-24">
          <NoDataGrid
            text={t(I18N_COMMON_TEXT.NO_ACCOUNTS_TO_DISPLAY)}
            dataTestId={`${testId}_NoDataGrid`}
          />
        </div>
      </>
    );
  }

  return (
    <>
      {data.map(item => (
        <AccountItem key={item.id} data={item} onItemClick={onItemClick} />
      ))}
    </>
  );
};

export default AccountList;
