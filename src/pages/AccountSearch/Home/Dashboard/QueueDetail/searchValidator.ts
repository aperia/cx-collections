// types
import { AttrAmountModel, AttrCommonModel } from 'pages/AccountSearch/types';

// constants
import { ATTRIBUTE_SEARCH_FIELD } from './AccountListControl/constants';

// helpers
import { stringValidate } from 'app/helpers/stringValidate';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { DEFAULT_SET_MAX_VALUE } from 'app/_libraries/_dls/components/AttributeSearch/controls/AmountRangeControl/helper';

export interface AttributeSearchFieldModel {
  accountNumber?: AttrCommonModel;
  amountDelinquent?: AttrAmountModel;
  currentBalance?: AttrAmountModel;
  daysDelinquent?: AttrCommonModel;
  firstName?: AttrCommonModel;
  lastName?: AttrCommonModel;
  lastPaymentDate?: AttrCommonModel;
  lastWorkDate?: AttrCommonModel;
}

export const validateAccountNumber = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  const { accountNumber } = data;
  if (accountNumber) {
    const methodValid = stringValidate(accountNumber?.value);

    if (!methodValid.isRequire() || !methodValid.minLength(10)) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER.name.toString()]: translateFn(
          I18N_COMMON_TEXT.INVALID_FORMAT
        )
      };
    }
  }
};

export const validateAmountDelinquent = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  const { amountDelinquent } = data;

  if (amountDelinquent) {
    const [min = '', max = ''] = amountDelinquent.value;
    const minValue = Number.parseFloat(min);
    const maxValue = Number.parseFloat(max);

    if (
      !minValue ||
      !maxValue ||
      minValue > DEFAULT_SET_MAX_VALUE ||
      maxValue > DEFAULT_SET_MAX_VALUE
    ) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.AMOUNT_DELINQUENT.name.toString()]: translateFn(
          I18N_COMMON_TEXT.INVALID_FORMAT
        )
      };
    }

    if (minValue > maxValue) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.AMOUNT_DELINQUENT.name.toString()]: translateFn(
          I18N_COMMON_TEXT.INVALID_AMOUNT_RANGE
        )
      };
    }
  }
};

export const validateCurrentBalance = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  const { currentBalance } = data;

  if (currentBalance) {
    const [min = '', max = ''] = currentBalance.value;
    const minValue = Number.parseFloat(min);
    const maxValue = Number.parseFloat(max);

    if (
      !minValue ||
      !maxValue ||
      minValue > DEFAULT_SET_MAX_VALUE ||
      maxValue > DEFAULT_SET_MAX_VALUE
    ) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.CURRENT_BALANCE.name.toString()]: translateFn(
          I18N_COMMON_TEXT.INVALID_FORMAT
        )
      };
    }

    if (minValue > maxValue) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.CURRENT_BALANCE.name.toString()]: translateFn(
          I18N_COMMON_TEXT.INVALID_AMOUNT_RANGE
        )
      };
    }
  }
};

export const validateDaysDelinquent = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  const { daysDelinquent } = data;
  if (daysDelinquent) {
    const methodValid = stringValidate(daysDelinquent?.value);

    if (!methodValid.isRequire()) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.DAYS_DELINQUENT.name.toString()]: translateFn(
          I18N_COMMON_TEXT.INVALID_FORMAT
        )
      };
    }
  }
};

export const validateFirstName = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  const { firstName } = data;
  if (firstName) {
    const methodValid = stringValidate(firstName?.value);

    if (!methodValid.isRequire()) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.FIRST_NAME.name.toString()]: translateFn(
          I18N_COMMON_TEXT.INVALID_FORMAT
        )
      };
    }
  }
};

export const validateLastName = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  const { lastName } = data;
  if (lastName) {
    const methodValid = stringValidate(lastName?.value);

    if (!methodValid.isRequire()) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.LAST_NAME.name.toString()]: translateFn(
          I18N_COMMON_TEXT.INVALID_FORMAT
        )
      };
    }
  }
};

export const validateLastPaymentDate = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  const { lastPaymentDate } = data;
  if (lastPaymentDate) {
    const methodValid = stringValidate(lastPaymentDate?.value);

    if (!methodValid.isRequire()) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.LAST_PAYMENT_DATE.name.toString()]: translateFn(
          I18N_COMMON_TEXT.INVALID_FORMAT
        )
      };
    }
  }
};

export const validateLastWorkDate = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  const { lastWorkDate } = data;
  if (lastWorkDate) {
    const methodValid = stringValidate(lastWorkDate?.value);

    if (!methodValid.isRequire()) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.LAST_WORK_DATE.name.toString()]: translateFn(
          I18N_COMMON_TEXT.INVALID_FORMAT
        )
      };
    }
  }
};

export const attrValidations = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  const validateList = [
    validateAccountNumber,
    validateAmountDelinquent,
    validateCurrentBalance,
    validateDaysDelinquent,
    validateFirstName,
    validateLastName,
    validateLastPaymentDate,
    validateLastWorkDate
  ];

  const runAllValidates = validateList.map(fn => {
    return fn(data, translateFn);
  }, []);

  const errors: any = runAllValidates.reduce((accumulator, current) => {
    if (!current) return accumulator;
    return { ...accumulator, ...current };
  }, {});
  return Object.keys(errors).length ? errors : undefined;
};
