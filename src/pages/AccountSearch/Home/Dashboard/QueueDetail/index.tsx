import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { usePagination } from 'app/hooks';
import useGetSortByData from './AccountListControl/hooks/useGetSortByData';
import useGetOrderByData from './AccountListControl/hooks/useGetOrderByData';
import useGetSearchFieldData from './AccountListControl/hooks/useGetSearchFieldData';

// constants
import { DASH } from 'app/constants';
import { I18N_DASH_BOARD } from '../constants';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { ATTRIBUTE_SEARCH_FIELD } from './AccountListControl/constants';
import { MESSAGE } from 'pages/AccountSearch/constants';

// helpers
import classnames from 'classnames';
import cloneDeep from 'lodash.clonedeep';
import { attrValidations } from './searchValidator';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { formatTimeDefault } from 'app/helpers';

// components
import {
  AttributeSearch,
  AttributeSearchEvent,
  AttributeSearchValue,
  Button,
  Pagination,
  SimpleBar
} from 'app/_libraries/_dls/components';
import QueueDetailView from './QueueDetailView';
import { FailedApiReload } from 'app/components';
import AccountList from './AccountListControl/AccountList';
import DropdownFilterBy from './AccountListControl/DropdownFilterBy';

// types
import { QueueDetailSnapshotData, QueueTypeEnum } from './types';
import { FormatTime } from 'app/constants/enums';

// selectors
import {
  selectQueueAccountsData,
  selectQueueAccountsIsError,
  selectQueueAccountsIsLoading
} from './_redux/selectors';

// actions
import { queueAccountsActions } from './_redux';
import { handleSort } from './AccountListControl/helpers';
import { dashboardActions } from '../_redux/reducers';

export interface QueueDetailSnapshotProps {
  data: QueueDetailSnapshotData;
  dataTestId?: string;
}
const QueueDetailSnapshot: React.FC<QueueDetailSnapshotProps> = ({
  data,
  dataTestId
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const testId = 'account-list-control';

  const { queueId, queueName } = data;

  const initialFilters = {
    sortBy: 'lastWorkDate',
    orderBy: 'desc',
    searchValues: [] as AttributeSearchValue[]
  };

  const sortByData = useGetSortByData();
  const orderByData = useGetOrderByData();
  const searchFieldData = useGetSearchFieldData();

  const [filters, setFilters] = useState(initialFilters);
  const [searchValues, setSearchValues] = useState<AttributeSearchValue[]>([]);

  const accounts = useSelector(selectQueueAccountsData(queueId));
  const isLoading = useSelector(selectQueueAccountsIsLoading(queueId));
  const isError = useSelector(selectQueueAccountsIsError(queueId));

  const handleOpenWorkOnQueue = useCallback(() => {
    batch(() => {
      dispatch(
        dashboardActions.triggerNextAccountWorkOnQueue({
          queueId,
          inAccountDetail: false,
          inQueueDetail: true,
          queueType: QueueTypeEnum.PULL_QUEUE
        })
      );
    });
  }, [dispatch, queueId]);

  const handleSortByChange = useCallback(
    (newValue: RefDataValue) => {
      const { value } = newValue;
      const newFilters = { ...filters, sortBy: value };

      setFilters(newFilters);
    },
    [filters]
  );

  const handleSearchChange = useCallback((event: AttributeSearchEvent) => {
    const { value } = event;
    setSearchValues(value);
  }, []);

  const handleClearSearch = useCallback(() => {
    setSearchValues([]);
    const newFilters = { ...filters, searchValues: [] };
    setFilters(newFilters);
  }, [filters]);

  const handleSearch = useCallback(
    (event: AttributeSearchEvent, error?: Record<string, string>) => {
      const { value: attrSearchValues } = event;

      if (isEmpty(attrSearchValues) || error) return;
      const preparedSearchValues = attrSearchValues.map(searchValue => {
        const { value } = searchValue;
        return {
          ...searchValue,
          value:
            value instanceof Date
              ? formatTimeDefault(value, FormatTime.DateWithHyphen)
              : value
        };
      });

      const newFilters = {
        ...filters,
        searchValues: preparedSearchValues
      };
      setFilters(newFilters);
    },
    [filters]
  );

  const handleOrderByChange = useCallback(
    (newValue: RefDataValue) => {
      const { value } = newValue;
      const newFilters = { ...filters, orderBy: value };

      setFilters(newFilters);
    },
    [filters]
  );

  const handleOpenAccountDetail = useCallback(
    (accountNumber: string) => {
      dispatch(
        queueAccountsActions.getQueueAccountDetail({
          queueId,
          accountNumber,
          queueType: QueueTypeEnum.PULL_QUEUE
        })
      );
    },
    [dispatch, queueId]
  );

  const handleGetQueueAccounts = useCallback(() => {
    dispatch(
      queueAccountsActions.getQueueAccounts({
        storeId: queueId,
        searchValues: filters.searchValues
      })
    );
  }, [dispatch, queueId, filters]);

  const handleOnClear = () => {
    handleClearSearch();
  };

  // get queue accounts for the first render or filter changes
  useEffect(() => {
    handleGetQueueAccounts();
  }, [handleGetQueueAccounts]);

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(accounts);
  const showPagination = total > pageSize[0];

  const sortedList = useMemo(() => {
    const { sortBy, orderBy } = filters;

    return handleSort(gridData, sortBy, orderBy);
  }, [gridData, filters]);

  if (isError) {
    return (
      <FailedApiReload
        id="dashboard-queue-accounts-failed"
        className="mt-80"
        onReload={handleGetQueueAccounts}
        dataTestId="dashboardQueueAccounts_failedApi"
      />
    );
  }

  return (
    <SimpleBar>
      <div
        data-testid={dataTestId}
        className={classnames({ loading: isLoading })}
      >
        <div className="bg-light-l20 border-bottom p-24">
          <div className="d-flex justify-content-between align-items-center">
            <h3>{`${queueId} ${DASH} ${queueName}`}</h3>
            <Button
              size="sm"
              className="mr-n8"
              variant="outline-primary"
              onClick={handleOpenWorkOnQueue}
            >
              {t(I18N_DASH_BOARD.WORK_ON_QUEUE)}
            </Button>
          </div>
          <QueueDetailView data={data} />
        </div>

        <div className="p-24">
          <div className="d-flex align-items-center justify-content-between">
            <h3>{t(I18N_COMMON_TEXT.ACCOUNT_LIST)}</h3>
            <AttributeSearch
              small
              orderBy="asc"
              id={`queueAccounts__attributeSearch`}
              dataTestId={`${testId}_attributeSearch`}
              value={searchValues}
              data={cloneDeep(searchFieldData)}
              validator={(value: Record<string, any>) =>
                attrValidations(value, t)
              }
              onChange={handleSearchChange}
              onSearch={handleSearch}
              onClear={handleClearSearch}
              noCombineFields={[
                {
                  field: ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER.name,
                  filterMessage: t(MESSAGE.ACCOUNT_NUMBER_SEARCH)
                }
              ]}
              clearTooltipProps={{
                placement: 'top',
                containerClassName: 'tooltip-clear',
                element: t(I18N_COMMON_TEXT.CLEAR_ALL_CRITERIA)
              }}
              placeholder={t('txt_type_to_add_parameter')}
              noFilterResultText={t('txt_no_other_parameters_available')}
            />
          </div>
          <div className="d-flex align-items-center justify-content-end mr-n8 mt-16">
            <DropdownFilterBy
              dataTestId={`queueAccounts__sortBy`}
              value={filters.sortBy}
              data={sortByData}
              onChange={handleSortByChange}
              label={t(I18N_COMMON_TEXT.SORT_BY)}
              labelClassName="color-grey mr-4 fw-500"
              dropdownProps={{ variant: 'no-border' }}
            />
            <div className="ml-24">
              <DropdownFilterBy
                dataTestId={`queueAccounts__orderBy`}
                value={filters.orderBy}
                data={orderByData}
                onChange={handleOrderByChange}
                label={t(I18N_COMMON_TEXT.ORDER_BY)}
                labelClassName="color-grey mr-4 fw-500"
                dropdownProps={{ variant: 'no-border' }}
              />
            </div>
          </div>

          {!isEmpty(searchValues) && !isEmpty(gridData) && (
            <div className="text-right mt-16 mr-n8 mr-4">
              <Button
                size="sm"
                variant="outline-primary"
                onClick={handleOnClear}
                dataTestId={`${testId}__clearAndResetSearch`}
              >
                {t('txt_clear_and_reset')}
              </Button>
            </div>
          )}
          <AccountList
            data={sortedList}
            isSearching={!isEmpty(searchValues)}
            onItemClick={handleOpenAccountDetail}
            onClearSearch={handleOnClear}
          />

          {showPagination && (
            <div className="pt-16">
              <Pagination
                totalItem={total}
                pageSize={pageSize}
                pageNumber={currentPage}
                pageSizeValue={currentPageSize}
                compact
                onChangePage={onPageChange}
                onChangePageSize={onPageSizeChange}
                dataTestId={testId}
              />
            </div>
          )}
        </div>
      </div>
    </SimpleBar>
  );
};

export default React.memo(QueueDetailSnapshot);
