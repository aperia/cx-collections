import React from 'react';
import { screen, fireEvent, act } from '@testing-library/react';

// constants
import { DASH } from 'app/constants';

//components
import QueueDetail from './';

// helpers
import {
  mockActionCreator,
  queryAllByClass,
  renderMockStoreId
} from 'app/test-utils';
import { queryByClass } from 'app/_libraries/_dls/test-utils';

// redux
import { queueAccountsActions } from './_redux';

// mock data
import queueAccounts from './__mocks__/queueAccounts.json';
import userEvent from '@testing-library/user-event';
import { I18N_DASH_BOARD } from '../constants';
import {
  AttributeSearchEvent,
  AttributeSearchProps,
  DropdownBaseChangeEvent,
  DropdownListProps
} from 'app/_libraries/_dls/components';

const mockData = JSON.parse(JSON.stringify(queueAccounts)).data;

const queueAccountActionsMock = mockActionCreator(queueAccountsActions);

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

jest.mock('./AccountListControl/DropdownFilterBy', () => {
  jest.requireActual('./AccountListControl/DropdownFilterBy');

  const DropdownList = ({ onChange, value, dataTestId }: DropdownListProps) => {
    return (
      <>
        {value}
        <input
          data-testid={dataTestId}
          onChange={e =>
            onChange!({
              value: e.target.value
            } as unknown as DropdownBaseChangeEvent)
          }
        />
      </>
    );
  };

  return {
    __esModule: true,
    default: DropdownList
  };
});

jest.mock('app/_libraries/_dls/components', () => {
  const dlsComponents = jest.requireActual('app/_libraries/_dls/components');

  return {
    ...dlsComponents,
    AttributeSearch: (props: AttributeSearchProps) => (
      <>
        <div data-testid="AttributeSearch_value">
          {JSON.stringify(props.value)}
        </div>
        <button
          data-testid="AttributeSearch_onClear"
          onClick={() => props.onClear!()}
        />
        <button
          data-testid="AttributeSearch_onValidator"
          onClick={() =>
            props.validator!({
              value: {}
            } as AttributeSearchEvent)
          }
        />
        <input
          data-testid="AttributeSearch_onSearch"
          onChange={(e: any) => props.onSearch!(e.target.event, e.target.error)}
        />
        <input
          data-testid="AttributeSearch_onChange"
          onChange={(e: any) => props.onChange!(e.target.event)}
        />
      </>
    )
  };
});

HTMLCanvasElement.prototype.getContext = jest.fn();

beforeEach(() => {
  queueAccountActionsMock('getQueueAccounts');
});

const dataTest = {
  queueId: 'ATY001',
  queueName: 'Becket and Lee',
  assignedTo: 'ATY001 - Attorney 1',
  createdDate: '01/22/2022',
  workedAccounts: '4',
  remainingAccounts: '5',
  totalAccounts: '7'
};

const dataTestId = 'QueueDetail';

const initialStateDefault: Partial<RootState> = {
  queueAccounts: {
    [dataTest.queueId]: {
      isLoading: false,
      data: [mockData[0]],
      isError: false
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>, data?: any) => {
  return renderMockStoreId(
    <QueueDetail data={{ ...dataTest, ...data }} dataTestId={dataTestId} />,
    {
      initialState: {
        ...initialStateDefault,
        ...initialState
      }
    }
  );
};

describe('QueueDetail Goals', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isCollector: true
    }
  };

  it('should render UI', () => {
    renderWrapper({});
    expect(
      screen.getByText(`${dataTest.queueId} ${DASH} ${dataTest.queueName}`)
    ).toBeInTheDocument();
  });

  it('should render UI with error', () => {
    renderWrapper({
      queueAccounts: {
        [dataTest.queueId]: {
          isLoading: false,
          data: [],
          isError: true
        }
      }
    });
    expect(screen.getByText('txt_reload')).toBeInTheDocument();
  });

  it('should render UI when have pagination', () => {
    const { wrapper } = renderWrapper({
      queueAccounts: {
        [dataTest.queueId]: {
          isLoading: false,
          data: mockData,
          isError: false
        }
      }
    });

    expect(
      queryByClass(wrapper.container, /pagination-wrapper/)
    ).toBeInTheDocument();
  });

  it('should render UI when click WORK ON QUEUE', () => {
    const mockAction = queueAccountActionsMock('toggleLoading');
    renderWrapper({
      queueAccounts: {
        [dataTest.queueId]: {
          isLoading: false,
          data: mockData,
          isError: false
        }
      }
    });

    userEvent.click(screen.getByText(I18N_DASH_BOARD.WORK_ON_QUEUE));

    expect(mockAction).toBeCalled();
  });

  it('should render UI when change sort by', () => {
    const { wrapper } = renderWrapper({
      queueAccounts: {
        [dataTest.queueId]: {
          isLoading: false,
          data: mockData,
          isError: false
        }
      }
    });

    act(() => {
      fireEvent.change(screen.getByTestId('queueAccounts__sortBy'), {
        target: { value: 'lastWorkDate' }
      });
    });

    expect(wrapper.getByText('lastWorkDate')).toBeInTheDocument();
  });

  it('should render UI when change order by', () => {
    const { wrapper } = renderWrapper({
      queueAccounts: {
        [dataTest.queueId]: {
          isLoading: false,
          data: mockData,
          isError: false
        }
      }
    });

    act(() => {
      fireEvent.change(screen.getByTestId('queueAccounts__orderBy'), {
        target: { value: 'asc' }
      });
    });

    expect(wrapper.getByText('asc')).toBeInTheDocument();
  });

  it('handleOnClear', () => {
    // render
    renderWrapper({
      queueAccounts: {
        [dataTest.queueId]: {
          isLoading: false,
          data: mockData,
          isError: false
        }
      }
    });

    screen.getByTestId('AttributeSearch_onValidator').click();
    screen.getByTestId('AttributeSearch_onClear').click();

    const expected = JSON.parse(
      screen.getByTestId('AttributeSearch_value').textContent!
    );
    expect(expected).toEqual([]);
  });

  describe('handleSearch', () => {
    it('without error', () => {
      // mock
      renderWrapper({
        queueAccounts: {
          [dataTest.queueId]: {
            isLoading: false,
            data: mockData,
            isError: false
          }
        }
      });

      fireEvent.change(screen.getByTestId('AttributeSearch_onSearch'), {
        target: {
          value: 'undefined',
          event: { value: [{ key: 'firstName', value: 'Smith' }] }
        }
      });

      expect(
        screen.getByText(`${dataTest.queueId} ${DASH} ${dataTest.queueName}`)
      ).toBeInTheDocument();
    });

    it('have data search with type date', () => {
      // mock
      renderWrapper({
        queueAccounts: {
          [dataTest.queueId]: {
            isLoading: false,
            data: mockData,
            isError: false
          }
        }
      });

      fireEvent.change(screen.getByTestId('AttributeSearch_onSearch'), {
        target: {
          value: 'undefined',
          event: { value: [{ key: 'lastWorkDate', value: new Date() }] }
        }
      });

      expect(
        screen.getByText(`${dataTest.queueId} ${DASH} ${dataTest.queueName}`)
      ).toBeInTheDocument();
    });

    it('with error', () => {
      // render
      renderWrapper({
        queueAccounts: {
          [dataTest.queueId]: {
            isLoading: false,
            data: mockData,
            isError: false
          }
        }
      });

      fireEvent.change(screen.getByTestId('AttributeSearch_onSearch'), {
        target: {
          value: 'undefined',
          event: { value: [{ key: 'accountNumber', value: 'Smith' }] },
          error: { message: 'error' }
        }
      });

      const expected = JSON.parse(
        screen.getByTestId('AttributeSearch_value').textContent!
      );
      expect(expected).toEqual([]);
    });
  });

  it('handleChange', () => {
    // render
    renderWrapper({
      queueAccounts: {
        [dataTest.queueId]: {
          isLoading: false,
          data: mockData,
          isError: false
        }
      }
    });

    fireEvent.change(screen.getByTestId('AttributeSearch_onChange'), {
      target: {
        value: 'undefined',
        event: { value: [{ key: 'firstName', value: 'Smith' }] }
      }
    });
    const expected = JSON.parse(
      screen.getByTestId('AttributeSearch_value').textContent!
    );
    expect(expected).toEqual([{ key: 'firstName', value: 'Smith' }]);
  });

  it('handleOpenAccountDetail', () => {
    // render
    const { wrapper } = renderWrapper({
      queueAccounts: {
        [dataTest.queueId]: {
          isLoading: false,
          data: mockData,
          isError: false
        }
      }
    });

    userEvent.click(
      queryAllByClass(wrapper.baseElement as HTMLElement, /card-primary/)[0]
    );
  });

  it('handleOnClearSearch', () => {
    // mock
    renderWrapper({
      queueAccounts: {
        [dataTest.queueId]: {
          isLoading: false,
          data: mockData,
          isError: false
        }
      }
    });

    fireEvent.change(screen.getByTestId('AttributeSearch_onChange'), {
      target: {
        value: 'undefined',
        event: { value: [{ key: 'lastWorkDate', value: '01-13-2022' }] }
      }
    });

    const btnReset = screen.getByTestId(
      'account-list-control__clearAndResetSearch_dls-button'
    );
    userEvent.click(btnReset);
  });
});
