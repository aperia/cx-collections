import { IGetQueueAccountsArgs } from './_redux/getQueueAccounts';
import { queueAccountsController } from './__mocks__/queueAccountsController';

const queueAccountsServices = {
  getQueueAccounts(args: IGetQueueAccountsArgs) {
    return queueAccountsController.getAccounts(args);
  },
  getNextPullQueueAccount(args: any) {
    return queueAccountsController.getNextAccount(args);
  }
};

export default queueAccountsServices;
