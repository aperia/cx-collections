import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import * as searchValidator from './searchValidator';

jest.mock('i18next', () => {
  return { t: (text: string) => text };
});

const translateFn = (text: string) => text;

describe('validateFirstName', () => {
  it('should run validateFirstName without value', () => {
    const result = searchValidator.validateFirstName(
      {
        firstName: { value: '' }
      },
      translateFn
    );
    expect(result).toEqual({ firstName: I18N_COMMON_TEXT.INVALID_FORMAT });
  });

  it('should run validateFirstName with value', () => {
    const result = searchValidator.validateFirstName(
      {
        firstName: { value: 'Jackie' }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });
});

describe('validateLastName', () => {
  it('should run validateLastName without lastName', () => {
    const result = searchValidator.validateLastName({}, jest.fn());

    expect(result).toEqual(undefined);
  });

  it('should run validateLastName without value', () => {
    const result = searchValidator.validateLastName(
      {
        lastName: { value: '' }
      },
      translateFn
    );

    expect(result).toEqual({ lastName: I18N_COMMON_TEXT.INVALID_FORMAT });
  });

  it('should run validateLastName with value', () => {
    const result = searchValidator.validateLastName(
      {
        lastName: { value: 'smith' }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });
});

describe('validateAmountDelinquent', () => {
  it('should run validateAmountDelinquent without value', () => {
    const result = searchValidator.validateAmountDelinquent(
      {
        amountDelinquent: { value: [undefined, undefined] }
      },
      translateFn
    );
    expect(result).toEqual({
      amountDelinquent: I18N_COMMON_TEXT.INVALID_FORMAT
    });
  });

  it('should run validateAmountDelinquent with error INVALID_AMOUNT_RANGE', () => {
    const result = searchValidator.validateAmountDelinquent(
      {
        amountDelinquent: { value: ['7', '5'] }
      },
      translateFn
    );
    expect(result).toEqual({
      amountDelinquent: I18N_COMMON_TEXT.INVALID_AMOUNT_RANGE
    });
  });

  it('should run validateAmountDelinquent with value', () => {
    const result = searchValidator.validateAmountDelinquent(
      {
        amountDelinquent: { value: ['5', '7'] }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });
});

describe('validateAccountNumber', () => {
  it('should run validateAccountNumber and return Invalid format', () => {
    const result = searchValidator.validateAccountNumber(
      {
        accountNumber: { value: '' }
      },
      translateFn
    );
    expect(result).toEqual({ accountNumber: I18N_COMMON_TEXT.INVALID_FORMAT });
  });

  it('should run validateAccountNumber with valid format', () => {
    const result = searchValidator.validateAccountNumber(
      {
        accountNumber: { value: '123456789123123' }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });

  it('should run validateAccountNumber without value', () => {
    const result = searchValidator.validateAccountNumber(
      {
        accountNumber: undefined
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });
});

describe('validateCurrentBalance', () => {
  it('should run validateCurrentBalance and return Invalid format', () => {
    const result = searchValidator.validateCurrentBalance(
      {
        currentBalance: { value: [undefined, undefined] }
      },
      translateFn
    );
    expect(result).toEqual({ currentBalance: I18N_COMMON_TEXT.INVALID_FORMAT });
  });

  it('should run validateCurrentBalance with error INVALID_AMOUNT_RANGE', () => {
    const result = searchValidator.validateCurrentBalance(
      {
        currentBalance: { value: ['7', '5'] }
      },
      translateFn
    );
    expect(result).toEqual({
      currentBalance: I18N_COMMON_TEXT.INVALID_AMOUNT_RANGE
    });
  });

  it('should run validateMiddleInitial with value', () => {
    const result = searchValidator.validateCurrentBalance(
      {
        currentBalance: { value: ['5', '7'] }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });
});

describe('validateDaysDelinquent', () => {
  it('should run validateDaysDelinquent without value', () => {
    const result = searchValidator.validateDaysDelinquent(
      {
        daysDelinquent: { value: '' }
      },
      translateFn
    );
    expect(result).toEqual({ daysDelinquent: I18N_COMMON_TEXT.INVALID_FORMAT });
  });

  it('should run validateDaysDelinquent with value', () => {
    const result = searchValidator.validateDaysDelinquent(
      {
        daysDelinquent: { value: '4' }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });
});

describe('validateLastPaymentDate', () => {
  it('should run validateLastPaymentDate without value', () => {
    const result = searchValidator.validateLastPaymentDate(
      {
        lastPaymentDate: { value: '' }
      },
      translateFn
    );
    expect(result).toEqual({
      lastPaymentDate: I18N_COMMON_TEXT.INVALID_FORMAT
    });
  });

  it('should run validateLastPaymentDate with value', () => {
    const result = searchValidator.validateLastPaymentDate(
      {
        lastPaymentDate: { value: '02/20/2022' }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });
});

describe('validateLastWorkDate', () => {
  it('should run validateLastWorkDate without value', () => {
    const result = searchValidator.validateLastWorkDate(
      {
        lastWorkDate: { value: '' }
      },
      translateFn
    );
    expect(result).toEqual({ lastWorkDate: I18N_COMMON_TEXT.INVALID_FORMAT });
  });

  it('should run validateLastWorkDate with value', () => {
    const result = searchValidator.validateLastWorkDate(
      {
        lastWorkDate: { value: '02/22/2022' }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });
});

describe('attrValidations', () => {
  it('should run attrValidations with value', () => {
    const result = searchValidator.attrValidations(
      {
        lastName: { value: 'smith' }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });

  it('should run attrValidations with invalid value', () => {
    const result = searchValidator.attrValidations(
      {
        lastName: { value: '' }
      },
      translateFn
    );
    expect(result).toEqual({ lastName: I18N_COMMON_TEXT.INVALID_FORMAT });
  });
});
