import queueAccountsServices from './queueAccountsServices';

describe('queueAccountsServices', () => {
  describe('getQueueAccounts', () => {
    it('call service get queue account', () => {
      queueAccountsServices.getQueueAccounts({
        storeId: '1',
        searchValues: [
          {
            key: 'firstName',
            value: 'Smith'
          }
        ]
      });
    });
  });

  describe('getNextPullQueueAccount', () => {
    it('call service get queue account', () => {
      queueAccountsServices.getNextPullQueueAccount({
        storeId: '1',
        searchValues: [
          {
            key: 'firstName',
            value: 'Smith'
          }
        ]
      });
    });
  });
});
