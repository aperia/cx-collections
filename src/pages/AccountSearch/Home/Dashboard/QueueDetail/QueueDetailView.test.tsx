import React from 'react';
import { screen } from '@testing-library/react';

//components
import QueueDetailView from './QueueDetailView';

// helpers
import { renderMockStoreId } from 'app/test-utils';

// mock data
import queueAccounts from './__mocks__/queueAccounts.json';

const mockData = JSON.parse(JSON.stringify(queueAccounts)).data;

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const dataTest = {
  queueId: 'ATY001',
  queueName: 'Becket and Lee',
  assignedTo: 'ATY001 - Attorney 1',
  createdDate: '01/22/2022',
  workedAccounts: '4',
  remainingAccounts: '5',
  totalAccounts: '7'
};

const initialStateDefault: Partial<RootState> = {
  queueAccounts: {
    [dataTest.queueId]: {
      isLoading: false,
      data: [mockData[0]],
      isError: false
    }
  }
};

const renderWrapper = (data?: any) => {
  return renderMockStoreId(<QueueDetailView data={data} />, {
    initialState: initialStateDefault
  });
};

describe('QueueDetailView Goals', () => {
  it('should render UI', () => {
    renderWrapper(dataTest);
    expect(screen.getByText(dataTest.createdDate)).toBeInTheDocument();
    expect(screen.getByText(dataTest.workedAccounts)).toBeInTheDocument();
    expect(screen.getByText(dataTest.remainingAccounts)).toBeInTheDocument();
    expect(screen.getByText(dataTest.totalAccounts)).toBeInTheDocument();
    expect(screen.getByText(dataTest.assignedTo)).toBeInTheDocument();
  });

  it('should render UI with error', () => {
    renderWrapper();
    expect(screen.getByText('txt_created_date:')).toBeInTheDocument();
  });
});
