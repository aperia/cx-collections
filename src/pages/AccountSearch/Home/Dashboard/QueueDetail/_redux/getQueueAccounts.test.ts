import { getQueueAccounts } from './getQueueAccounts';
import {
  createStoreWithDefaultMiddleWare,
} from 'app/test-utils';
import queueAccountsServices from '../queueAccountsServices';
import queueAccounts from '../__mocks__/queueAccounts.json';

import { IQueueAccount } from '../types';

const mockData: IQueueAccount[] = JSON.parse(
  JSON.stringify(queueAccounts)
).data;

let spy: jest.SpyInstance;

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    org: 'org',
    app: 'app'
  }
};

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('should have test getQueueAccounts', () => {
  it('success response', async () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: { commonConfig: { org: 'org', app: 'app' } }
    });

    jest.spyOn(queueAccountsServices, 'getQueueAccounts').mockResolvedValue({
      success: true,
      data: mockData,
      message: 'success'
    });

    const store = createStoreWithDefaultMiddleWare({
      mapping: { data: {}, loading: false }
    });

    const params = {
      storeId: '1',
      searchValues: []
    };

    await getQueueAccounts(params)(store.dispatch, store.getState, mockData);

    expect(store.getState().queueAccounts[params.storeId]?.data).toEqual(mockData);
  });

  it('success response with data null', async () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: { commonConfig: { org: 'org', app: 'app' } }
    });

    jest.spyOn(queueAccountsServices, 'getQueueAccounts').mockResolvedValue({
      success: true,
      data: undefined,
      message: 'success'
    });

    const store = createStoreWithDefaultMiddleWare({
      mapping: { data: {}, loading: false }
    });

    const params = {
      storeId: '1',
      searchValues: []
    };

    await getQueueAccounts(params)(store.dispatch, store.getState, undefined);

    expect(store.getState().queueAccounts[params.storeId]?.data).toEqual([]);
  });

  it('error response', async () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: { commonConfig: { org: 'org', app: 'app' } }
    });

    Object.defineProperty(window, 'location', {
      writable: true,
      value: { href: '?error=not-found' }
    });

    jest.spyOn(queueAccountsServices, 'getQueueAccounts').mockRejectedValue({
      success: false,
      data: [],
      message: 'Failed to get accounts'
    });

    const store = createStoreWithDefaultMiddleWare({
      mapping: { data: {}, loading: false }
    });

    const params = {
      storeId: '1',
      searchValues: []
    };

    await getQueueAccounts(params)(store.dispatch, store.getState, undefined);

    expect(store.getState().queueAccounts[params.storeId]?.data).toEqual([]);
  });
});
