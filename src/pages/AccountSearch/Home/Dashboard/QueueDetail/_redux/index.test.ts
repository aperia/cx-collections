import { queueAccountsActions, reducer } from './index';

// utils
import { storeId } from 'app/test-utils';

const { toggleLoading, setCurrentQueueAccount } = queueAccountsActions;

describe('Test reducer', () => {
  it('toggleLoading action', () => {
    const state = reducer(
      {
        [storeId]: {
          isLoading: false,
          data: [],
          isError: false
        }
      },
      toggleLoading({ storeId })
    );

    expect(state).toEqual({
      [storeId]: {
        isError: false,
        data: [],
        isLoading: true
      }
    });
  });

  it('toggleLoading action empty', () => {
    const state = reducer({}, toggleLoading({ storeId }));

    expect(state).toEqual({
      [storeId]: {
        isLoading: true
      }
    });
  });

  it('toggleLoading action empty', () => {
    const state = reducer(
      {},
      setCurrentQueueAccount({
        storeId,
        currentQueueAccount: {
          eValue: 'eValue_test',
          tabStoreId: 'tabStoreId_test'
        }
      })
    );

    expect(state).toEqual({
      [storeId]: {
        currentQueueAccount: {
          eValue: 'eValue_test',
          tabStoreId: 'tabStoreId_test'
        }
      }
    });
  });
});
