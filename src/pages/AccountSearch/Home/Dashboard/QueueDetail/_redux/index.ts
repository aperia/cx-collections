import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ICurrentQueueAccount, IInitialStateQueueAccounts } from '../types';

import { getQueueAccounts, getQueueAccountsBuilder } from './getQueueAccounts';
import {
  getQueueAccountDetail,
  getQueueAccountDetailBuilder
} from './getQueueAccountDetail';

const initialState: IInitialStateQueueAccounts = {};

export const { actions, reducer } = createSlice({
  name: 'queueAccounts',
  initialState: initialState,
  reducers: {
    toggleLoading: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      const { isLoading = false } = draftState[storeId] || {};

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: !isLoading
      };
    },
    setCurrentQueueAccount: (
      draftState,
      action: PayloadAction<
        StoreIdPayload & { currentQueueAccount?: ICurrentQueueAccount }
      >
    ) => {
      const { storeId, currentQueueAccount } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        currentQueueAccount
      };
    }
  },
  extraReducers: builder => {
    getQueueAccountsBuilder(builder);
    getQueueAccountDetailBuilder(builder);
  }
});

const combineActions = {
  ...actions,
  getQueueAccounts,
  getQueueAccountDetail
};

export { combineActions as queueAccountsActions };
