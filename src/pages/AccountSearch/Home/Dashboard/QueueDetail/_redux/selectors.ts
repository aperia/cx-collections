import { createSelector } from '@reduxjs/toolkit';

// types
import { IQueueAccount } from '../types';

export const selectQueueAccountsData = (storeId: string) => {
  return createSelector(
    (state: RootState) =>
      state.queueAccounts[storeId]?.data || ([] as IQueueAccount[]),
    (data: IQueueAccount[]) => data
  );
};

export const selectQueueAccountsIsLoading = (storeId: string) => {
  return createSelector(
    (state: RootState) => !!state.queueAccounts[storeId]?.isLoading,
    (data: boolean) => data
  );
};

export const selectQueueAccountsIsError = (storeId: string) => {
  return createSelector(
    (state: RootState) => !!state.queueAccounts[storeId]?.isError,
    (data: boolean) => data
  );
};
