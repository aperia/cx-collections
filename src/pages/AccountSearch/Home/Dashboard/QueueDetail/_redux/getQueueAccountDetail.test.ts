import { IGetQueueAccountDetailArgs } from './getQueueAccountDetail';
import { mockActionCreator, responseDefault } from 'app/test-utils';
import accountDetailService from 'pages/AccountDetails/accountDetailService';
import { QueueTypeEnum } from '../types';
import * as accountDetailActions from 'pages/AccountSearch/AccountSearchList/_redux/openAccountDetail';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import { queueAccountsActions } from '.';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { DASHBOARD_GET_NEXT_ACCOUNT } from '../../constants';

const mockAccountDetailActions = mockActionCreator(accountDetailActions);
const mockToastActions = mockActionCreator(actionsToast);

const mockData = {
  customerInquiry: [
    {
      customerName: 'SMITH,ADELE ',
      memberSequenceIdentifier: '00001',
      socialSecurityIdentifier:
        '{"maskedValue":"***-**-1111","eValue":"VOL(UTVqRzYhcWlVc0laZTpe)"}',
      clientIdentifier: '8897',
      systemIdentifier: '1000',
      principalIdentifier: 'prin',
      agentIdentifier: '3000',
      presentationInstrumentIdentifier:
        '{"maskedValue":"***-**-1111","eValue":"VOL(UTVqRzYhcWlVc0laZTpe)"}'
    }
  ]
};

let spy: jest.SpyInstance;

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    org: 'org',
    app: 'app'
  }
};

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

describe('should have test getQueueAccounts', () => {
  const store = createStore(rootReducer, {});

  const callApi = async (isReject = false, data = mockData) => {
    if (!isReject) {
      jest.spyOn(accountDetailService, 'getAccountDetails').mockResolvedValue({
        ...responseDefault,
        data
      });
    } else {
      jest
        .spyOn(accountDetailService, 'getAccountDetails')
        .mockRejectedValue(new Error('Error'));
    }

    const params: IGetQueueAccountDetailArgs = {
      queueId: 'queueId',
      accountNumber: 'accountNumber',
      queueType: QueueTypeEnum.PUSH_QUEUE
    };

    return queueAccountsActions.getQueueAccountDetail(params)(
      store.dispatch,
      store.getState,
      {}
    );
  };

  it('Should have success', async () => {
    const mOpenAccountDetail = mockAccountDetailActions('openAccountDetail');

    await callApi();

    expect(mOpenAccountDetail).toHaveBeenCalled();
  });

  it('Should have data empty', async () => {
    const response = await callApi(false, { customerInquiry: [] });

    expect(response.payload).toEqual({
      config: {},
      data: { customerInquiry: [] },
      headers: {},
      status: 200,
      statusText: ''
    });
  });

  it('Should have error', async () => {
    const addToast = mockToastActions('addToast');

    await callApi(true);

    expect(addToast).toHaveBeenCalledWith({
      show: true,
      type: 'error',
      message: DASHBOARD_GET_NEXT_ACCOUNT.ACCOUNT_FAILED_TO_RETRIEVE
    });
  });
});
