import { storeId } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';

import * as selector from './selectors';
import queueAccounts from '../__mocks__/queueAccounts.json';

import { IInitialStateQueueAccounts, IQueueAccount } from '../types';

const mockData: IQueueAccount[] = JSON.parse(
  JSON.stringify(queueAccounts)
).data;

const store = createStore(rootReducer);
const state = store.getState();

state.queueAccounts = {
  [storeId]: {
    data: mockData,
    isError: false,
    isLoading: false
  }
};

describe('redux-store > queue-list > selector', () => {
  it('selectQueueAccountsData', () => {
    const result = selector.selectQueueAccountsData(storeId)(state);

    expect(result).toEqual(mockData);
  });

  it('selectQueueAccountsData empty', () => {
    const result = selector.selectQueueAccountsData(storeId)({
      queueAccounts: {} as IInitialStateQueueAccounts
    } as RootState);

    expect(result).toEqual([]);
  });

  it('selectQueueAccountsIsError', () => {
    const result = selector.selectQueueAccountsIsError(storeId)(state);

    expect(result).toEqual(false);
  });

  it('selectQueueAccountsIsLoading', () => {
    const result = selector.selectQueueAccountsIsLoading(storeId)(state);

    expect(result).toEqual(false);
  });
});
