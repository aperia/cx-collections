import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// services
import queueAccountsServices from '../queueAccountsServices';

// types
import { IInitialStateQueueAccounts, IQueueAccount } from '../types';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';

export interface IGetQueueAccountsPayload {
  success: boolean;
  data?: IQueueAccount[];
  message?: string;
}
export interface IGetQueueAccountsArgs {
  storeId: string;
  searchValues: AttributeSearchValue[];
}

export const getQueueAccounts = createAsyncThunk<
  IGetQueueAccountsPayload,
  IGetQueueAccountsArgs,
  ThunkAPIConfig
>('queueAccounts/getQueueAccounts', async (args, thunkAPI) => {
  try {
    return await queueAccountsServices.getQueueAccounts(args);
  } catch (error) {
    return thunkAPI.rejectWithValue({});
  }
});

export const getQueueAccountsBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateQueueAccounts>
) => {
  const { pending, fulfilled, rejected } = getQueueAccounts;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true,
        isError: false
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data = [] as IQueueAccount[] } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: false,
        data
      };
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        isLoading: false,
        isError: true,
        data: []
      };
    });
};
