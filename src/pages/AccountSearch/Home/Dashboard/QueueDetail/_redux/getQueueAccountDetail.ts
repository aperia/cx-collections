import { IInitialStateQueueAccounts, QueueTypeEnum } from './../types';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// actions
import { openAccountDetail } from 'pages/AccountSearch/AccountSearchList/_redux/openAccountDetail';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// services
import accountDetailService from 'pages/AccountDetails/accountDetailService';

// types
import { WorkType } from 'pages/TimerWorking/types';

// constants
import { DASHBOARD_GET_NEXT_ACCOUNT } from '../../constants';
import { isArrayHasValue } from 'app/helpers';

export interface IGetQueueAccountDetailPayload {
  data?: unknown;
}

export interface IGetQueueAccountDetailArgs {
  queueId: string;
  accountNumber: string;
  queueType: QueueTypeEnum;
}

export const getQueueAccountDetail = createAsyncThunk<
  IGetQueueAccountDetailPayload,
  IGetQueueAccountDetailArgs,
  ThunkAPIConfig
>('queueAccounts/getQueueAccountDetail', async (args, thunkAPI) => {
  const { dispatch } = thunkAPI;
  try {
    const { queueId, accountNumber, queueType } = args;

    const response = await accountDetailService.getAccountDetails({
      common: { accountId: accountNumber },
      selectFields: [
        'customerInquiry.customerName',
        'customerInquiry.socialSecurityIdentifier',
        'customerInquiry.memberSequenceIdentifier',
        'customerInquiry.clientIdentifier',
        'customerInquiry.systemIdentifier',
        'customerInquiry.principalIdentifier',
        'customerInquiry.agentIdentifier'
      ]
    });

    const customerInquiry = response?.data?.customerInquiry;

    if (customerInquiry && isArrayHasValue(customerInquiry)) {
      const presentationInstrumentIdentifier =
        customerInquiry[0]?.presentationInstrumentIdentifier;

      const parseData = JSON.parse(presentationInstrumentIdentifier!);

      await dispatch(
        openAccountDetail({
          queueId,
          eValue: parseData.eValue,
          primaryName: customerInquiry[0]?.customerName,
          memberSequenceIdentifier:
            customerInquiry[0]?.memberSequenceIdentifier,
          maskedValue: parseData.maskedValue,
          socialSecurityIdentifier:
            customerInquiry[0]?.socialSecurityIdentifier,
          clientIdentifier: customerInquiry[0]?.clientIdentifier,
          systemIdentifier: customerInquiry[0]?.systemIdentifier,
          principalIdentifier: customerInquiry[0]?.principalIdentifier,
          agentIdentifier: customerInquiry[0]?.agentIdentifier,
          type: WorkType.QUEUE,
          queueType
        })
      );
    }

    return response;
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: DASHBOARD_GET_NEXT_ACCOUNT.ACCOUNT_FAILED_TO_RETRIEVE
      })
    );
    return thunkAPI.rejectWithValue({});
  }
});

export const getQueueAccountDetailBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateQueueAccounts>
) => {
  const { pending, fulfilled, rejected } = getQueueAccountDetail;
  builder
    .addCase(pending, (draftState, action) => {
      const { queueId: storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true,
        isError: false
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      const { queueId: storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: false,
      };
    })
    .addCase(rejected, (draftState, action) => {
      const { queueId: storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false
      };
    });
};
