import React from 'react';

// components
import { Bubble } from 'app/_libraries/_dls/components';
import TextViewGroup from 'app/components/TextViewGroup';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { genAmtId } from 'app/_libraries/_dls/utils';

// types
import { QueueDetailSnapshotData } from './types';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';

export interface QueueDetailViewProps {
  data: QueueDetailSnapshotData;
}
const QueueDetailView: React.FC<QueueDetailViewProps> = ({ data = {} }) => {
  const testId = 'queue-detail-view';

  const {
    createdDate,
    workedAccounts,
    remainingAccounts,
    totalAccounts,
    assignedTo
  } = data;

  const { t } = useTranslation();

  return (
    <div data-testid={genAmtId(testId, `${testId}_container`, '')}>
      <div className="d-flex mt-16">
        <TextViewGroup
          testId="createdDate"
          label={`${t(I18N_COMMON_TEXT.CREATED_DATE)}: `}
          content={createdDate}
          labelClassName="color-grey mr-8"
        />

        <TextViewGroup label="•" labelClassName="color-grey mx-8" />

        <TextViewGroup
          testId="assignedTo"
          label={`${t(I18N_COMMON_TEXT.ASSIGNED_TO)}: `}
          content={() => (
            <>
              <Bubble small name={assignedTo} />
              <span className="bubble-name ml-4">{assignedTo}</span>
            </>
          )}
          labelClassName="color-grey mr-4"
          contentClassName="color-grey"
        />
      </div>

      <div className="row mt-24">
        <div className="col-lg-4 col-md-6 col-xl-3">
          <TextViewGroup
            testId="workedAccounts"
            label={`${t(I18N_COMMON_TEXT.WORKED_ACCOUNTS)}: `}
            content={workedAccounts}
            labelClassName="fw-500 mr-8"
          />
        </div>

        <div className="col-lg-4 col-md-6 col-xl-3">
          <TextViewGroup
            testId="remainingAccounts"
            label={`${t(I18N_COMMON_TEXT.REMAINING_ACCOUNTS)}: `}
            content={remainingAccounts}
            labelClassName="fw-500 mr-8"
          />
        </div>

        <div className="col-lg-4 col-md-6 col-xl-3 mt-md-16 mt-lg-0">
          <TextViewGroup
            testId="totalAccounts"
            label={`${t(I18N_COMMON_TEXT.TOTAL_ACCOUNTS)}: `}
            content={totalAccounts}
            labelClassName="fw-500 mr-8"
          />
        </div>
      </div>
    </div>
  );
};

export default React.memo(QueueDetailView);
