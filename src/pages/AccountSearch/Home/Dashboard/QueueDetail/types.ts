export interface IQueueAccount {
  id?: string;
  accountNumber: string;
  lastName: string;
  firstName: string;
  lastWorkDate: string;
  daysDelinquent: string;
  currentBalance: string;
  amountDelinquent: string;
  lastPaymentDate: string;
  timeZone: string;
}

export interface ICurrentQueueAccount {
  eValue: string;
  tabStoreId: string;
}
export interface IQueueAccountsState {
  isLoading: boolean;
  isError: boolean;
  data: IQueueAccount[];
  currentQueueAccount?: ICurrentQueueAccount;
}

export interface IInitialStateQueueAccounts {
  [storeId: string]: IQueueAccountsState;
}

export interface QueueDetailSnapshotData {
  queueId: string;
  queueName: string;
  assignedTo: string;
  createdDate: string;
  workedAccounts: string;
  remainingAccounts: string;
  totalAccounts: string;
}

export enum QueueTypeEnum {
  PULL_QUEUE = 'Pull_Queue',
  PUSH_QUEUE = 'Push_Queue'
}
