import { formatCommon, formatTime } from 'app/helpers';
import { Bubble } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import React from 'react';

export interface QueueTypeInformationProps {
  assignedTo: string;
  lastWorkDate: string;
  queueType: string;
  totalNumbersOfAccount: number | string;
  totalOutstandingBalanceAmount: string;
  className?: string;
}
export const QueueTypeInformation: React.FC<QueueTypeInformationProps> = ({
  assignedTo,
  lastWorkDate,
  queueType,
  totalNumbersOfAccount,
  totalOutstandingBalanceAmount,
  className
}) => {
  const { t } = useTranslation();
  return (
    <div className={className}>
      <div className="w-xl-35 w-lg-15 w-md-35">
        <h6 className="mb-4 color-grey">{t('txt_queue_type')}</h6>
        <p className="text-capitalize">{queueType.toLowerCase()}</p>
      </div>

      <div className="w-xl-35 w-lg-15 w-md-35">
        <h6 className="mb-4 color-grey">{t('txt_last_work_date')}</h6>
        {formatTime(lastWorkDate).date}
      </div>

      <div className="w-xl-30 w-lg-15 w-md-30">
        <h6 className="mb-4 color-grey">{t('txt_assigned_to')}</h6>
        <div>
          <Bubble small name={assignedTo} />
          <span className="bubble-name">{assignedTo}</span>
        </div>
      </div>

      <div className="w-xl-35 w-lg-20 w-md-35 mt-xl-16 mt-lg-0 mt-md-16">
        <h6 className="mb-4 color-grey">{t('txt_total_number_of_accounts')}</h6>
        {totalNumbersOfAccount}
      </div>
      <div className="w-xl-65 w-lg-35 w-md-65 mt-xl-16 mt-lg-0 mt-md-16">
        <h6 className="mb-4 color-grey">
          {t('txt_total_outstanding_balance_amount')}
        </h6>
        {formatCommon(totalOutstandingBalanceAmount).currency()}
      </div>
    </div>
  );
};

export default QueueTypeInformation;
