import React, { useMemo } from 'react';

// components
import { Button } from 'app/_libraries/_dls/components';
import ProgressBar from 'app/_libraries/_dls/components/ProgressBar';

// helpers

import { formatCommon } from 'app/helpers';
import upperFirst from 'lodash/upperFirst';

// translate
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { DASHBOARD_GOAL_ITEM, I18N_DASH_BOARD } from '../constants';

// types
import PromiseStatistic from '../common/PromiseStatistic';
import { batch, useDispatch } from 'react-redux';
import { dashboardActions } from '../_redux/reducers';
import { isUndefined } from 'app/_libraries/_dls/lodash';
import { genAmtId } from 'app/_libraries/_dls/utils';
import QueueTypeInformation from './QueueTypeInformation';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { CoreViewQueueDetail } from './CoreViewQueueDetail';
import { tabActions } from 'pages/__commons/TabBar/_redux';
import { DASH } from 'app/constants';

interface CardQueueProps extends DLSId {
  id: string;
  dataTestId?: string;
  collectorQueueStats: MagicKeyValue;
  collectorPromiseStats: MagicKeyValue;
  queueType: string;
  queueName: string;
  queueId: string;
  userId?: string;
  agentId?: string;
  lastWorkDate: string;
  assignedTo: string;
  totalNumbersOfAccount: number | string;
  totalOutstandingBalanceAmount: string;
  fromDashboard?: boolean;
}

const CardQueue: React.FC<CardQueueProps> = ({
  id,
  collectorQueueStats,
  queueName,
  collectorPromiseStats,
  queueType = '',
  lastWorkDate = '',
  assignedTo = '',
  totalNumbersOfAccount = '',
  totalOutstandingBalanceAmount = '',
  queueId = '162672832060',
  fromDashboard = true,
  dataTestId
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { isCollector, isSupervisor, privileges } =
    window.appConfig.commonConfig || {};

  const isCoreView = isCollector;

  const userRole = privileges?.[0] || '';

  const isCXCOL5 = userRole === 'CXCOL5';

  const dataSnapshot = useMemo(() => {
    const workedAccountsCount = '1';
    const workedAccountsAmount = '40000';
    const remainingAccountsCount = `${
      +totalNumbersOfAccount - +workedAccountsCount
    }`;
    const remainingAccountsAmount = `${
      Number.parseFloat(totalOutstandingBalanceAmount) -
      Number.parseFloat(workedAccountsAmount)
    }`;

    return {
      queueId,
      queueName,
      assignedTo,
      createdDate: '11/23/2020',
      workedAccounts: `${
        +totalNumbersOfAccount - +remainingAccountsCount
      } | ${formatCommon(workedAccountsAmount).currency(2)}`,

      remainingAccounts: `${
        +totalNumbersOfAccount - +workedAccountsCount
      } | ${formatCommon(remainingAccountsAmount).currency(2)}`,

      totalAccounts: `${totalNumbersOfAccount} | ${formatCommon(
        totalOutstandingBalanceAmount
      ).currency(2)}`
    };
  }, [
    assignedTo,
    queueId,
    queueName,
    totalNumbersOfAccount,
    totalOutstandingBalanceAmount
  ]);

  const handleOpenWorkOnQueue = React.useCallback(() => {
    batch(() => {
      dispatch(
        dashboardActions.triggerNextAccountWorkOnQueue({
          queueId,
          inAccountDetail: false
        })
      );
    });
  }, [dispatch, queueId]);

  const renderPercent = (testId: string) => (values: number[]) =>
    (
      <div
        className="dls-progress-bar-percent"
        data-testid={genAmtId(dataTestId, `${testId}_percent`, '')}
      >
        {(((values[0] / values[1]) % 100) * 100).toFixed(0)}%
      </div>
    );

  const renderAccountsWorkedTopView = (testId: string) => (values: number[]) =>
    (
      <div
        className="dls-progress-bar-top"
        data-testid={genAmtId(dataTestId, `${testId}_topViewWorked`, '')}
      >
        {formatCommon(values[0]).quantity} / {formatCommon(values[1]).quantity}
      </div>
    );

  const renderAmountCollectedTopView = (testId: string) => (values: number[]) =>
    (
      <div
        className="dls-progress-bar-top"
        data-testid={genAmtId(dataTestId, `${testId}_topViewCollected`, '')}
      >
        {formatCommon(values[0]).currency(2)} /{' '}
        {formatCommon(values[1]).currency(2)}
      </div>
    );

  const renderMoreDetail = () => {
    const {
      accountsWorked,
      totalAccounts,
      amountCollected,
      totalAmount: totalAmountQueue
    } = collectorQueueStats;

    const {
      pendingPromises,
      pendingAmount,
      keptPromises,
      keptAmount,
      brokenPromises,
      brokenAmount,
      totalPromises,
      totalAmount
    } = collectorPromiseStats;

    return (
      <div className="d-flex flex-wrap mt-12">
        <div className="w-xl-20 w-md-50 border-right pr-16">
          <div className="progress-item mb-16">
            <h6
              className="color-grey-l24 mb-4"
              data-testid={genAmtId(dataTestId, 'accountWorked-title', '')}
            >
              {t(DASHBOARD_GOAL_ITEM.ACCOUNTS_WORKED)}
            </h6>
            <ProgressBar
              id={`${id}__progress-bar__account-worked`}
              data={{
                values: [Number(accountsWorked), Number(totalAccounts)],
                colors: ['#5cbae6', '#D7D7DE']
              }}
              dataTestId={genAmtId(dataTestId, 'accountWorked-progress', '')}
              renderPercent={values => renderPercent('accountWorked')(values)}
              renderTopView={values =>
                renderAccountsWorkedTopView('accountWorked')(values)
              }
            />
          </div>
          <div className="progress-item">
            <h6
              className="color-grey-l24 mb-4"
              data-testid={genAmtId(dataTestId, 'amountCollected-title', '')}
            >
              {t(DASHBOARD_GOAL_ITEM.AMOUNT_COLLECTED)}
            </h6>
            <ProgressBar
              id={`${id}__progress-bar__amount-collected`}
              data={{
                values: [Number(amountCollected), Number(totalAmountQueue)],
                colors: ['#77D97C', '#D7D7DE']
              }}
              dataTestId={genAmtId(dataTestId, 'amountCollected-progress', '')}
              renderPercent={values => renderPercent('amountCollected')(values)}
              renderTopView={values =>
                renderAmountCollectedTopView('amountCollected')(values)
              }
            />
          </div>
        </div>
        <div className="w-xl-30 w-lg-40 w-md-50 pl-16 border-xl-right pr-xl-24">
          <PromiseStatistic
            recentPromise
            pendingPromises={pendingPromises}
            pendingAmount={pendingAmount}
            keptPromises={keptPromises}
            keptAmount={keptAmount}
            brokenPromises={brokenPromises}
            brokenAmount={brokenAmount}
            totalPromises={totalPromises}
            totalAmount={totalAmount}
            dataTestId={`${dataTestId}_promisesStatistic`}
          />
        </div>
        <div className="w-xl-50 w-md-100 pl-xl-24 mt-md-20 mt-xl-0 pt-md-24 pt-xl-0  border-md-top border-xl-none">
          <QueueTypeInformation
            className="row"
            queueType={queueType}
            lastWorkDate={lastWorkDate}
            assignedTo={assignedTo}
            totalNumbersOfAccount={totalNumbersOfAccount}
            totalOutstandingBalanceAmount={totalOutstandingBalanceAmount}
          />
        </div>
      </div>
    );
  };

  const handleOpenQueueDetail = React.useCallback(() => {
    const title = `${queueId} ${DASH} ${queueName}`;
    const newStoreId = `${queueId}${DASH}${queueName}`;

    dispatch(
      tabActions.addTab({
        title,
        storeId: newStoreId,
        iconName: 'file',
        tabType: 'queueDetail',
        props: {
          data: dataSnapshot
        }
      })
    );
  }, [dataSnapshot, dispatch, queueId, queueName]);

  return (
    <div className="bg-white rounded-lg border br-light-l04 p-xl-16 px-md-24 py-md-16 mt-16">
      <div className="d-flex align-items-center">
        <h5
          id={`${id}__txt-${queueName}`}
          className="flex-1 pr-16 fs-14"
          data-testid={genAmtId(dataTestId, 'queueName', '')}
        >
          {`${queueId} - ${upperFirst(queueName)}`}
        </h5>
        <div className="ml-auto mr-n8">
          {queueType.toLowerCase() === 'pull' && (
            <Button
              id={`${id}__${queueName}__btn-view-work-on-queue`}
              size="sm"
              variant="outline-primary"
              dataTestId={`${dataTestId}_view-btn`}
              onClick={handleOpenQueueDetail}
            >
              {t(I18N_COMMON_TEXT.VIEW)}
            </Button>
          )}

          <Button
            id={`${id}__${queueName}__btn-work-on-queue`}
            onClick={handleOpenWorkOnQueue}
            size="sm"
            variant="outline-primary"
            dataTestId={`${dataTestId}_workOnQueue-btn`}
          >
            {t(I18N_DASH_BOARD.WORK_ON_QUEUE)}
          </Button>
        </div>
      </div>

      {!isUndefined(collectorQueueStats) &&
        !isUndefined(collectorPromiseStats) &&
        (isSupervisor || isCXCOL5) &&
        renderMoreDetail()}

      {!isUndefined(collectorQueueStats) &&
        !isUndefined(collectorPromiseStats) &&
        isCoreView &&
        !isCXCOL5 && (
          <CoreViewQueueDetail
            className="row "
            queueType={queueType}
            assignedTo={assignedTo}
            lastWorkDate={lastWorkDate}
            totalNumbersOfAccount={totalNumbersOfAccount}
            totalOutstandingBalanceAmount={totalOutstandingBalanceAmount}
            fromDashboard={fromDashboard}
          />
        )}
    </div>
  );
};

export default CardQueue;
