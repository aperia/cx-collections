import { formatCommon, formatTime } from 'app/helpers';
import { Bubble } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import React from 'react';

export interface CoreViewQueueDetailProps {
  lastWorkDate: string;
  queueType: string;
  totalNumbersOfAccount: number | string;
  totalOutstandingBalanceAmount: string;
  assignedTo: string;
  fromDashboard?: boolean;
  className?: string;
}
export const CoreViewQueueDetail: React.FC<CoreViewQueueDetailProps> = ({
  lastWorkDate,
  queueType,
  totalNumbersOfAccount,
  totalOutstandingBalanceAmount,
  assignedTo,
  fromDashboard = true,
  className
}) => {
  const { t } = useTranslation();
  return (
    <div className={className}>
      <div className="col-2">
        <p className="text-uppercase mb-4 fs-11">{t('txt_queue_type')}</p>
        <p className="text-capitalize">{queueType.toLowerCase()}</p>
      </div>

      <div className="col-2">
        <p className="text-uppercase mb-4 fs-11">{t('txt_last_work_date')}</p>
        {formatTime(lastWorkDate).date}
      </div>

      <div className="col-2">
        <p className="text-uppercase mb-4 fs-11">
          {t('txt_total_number_of_accounts')}
        </p>
        {totalNumbersOfAccount}
      </div>
      <div className="col-3">
        <p className="text-uppercase mb-4 fs-11">
          {t('txt_total_outstanding_balance_amount')}
        </p>
        {formatCommon(totalOutstandingBalanceAmount).currency()}
      </div>

      {!fromDashboard && (
        <div className="col-3">
          <p className="text-uppercase mb-4 fs-11">{t('txt_assigned_to')}</p>
          <div>
            <Bubble small name={assignedTo} />
            <span className="bubble-name">{assignedTo}</span>
          </div>
        </div>
      )}
    </div>
  );
};

export default CoreViewQueueDetail;
