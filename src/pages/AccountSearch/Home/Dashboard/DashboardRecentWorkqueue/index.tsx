import React from 'react';

//utils
import classnames from 'classnames';

//constants
import { I18N_DASH_BOARD } from '../constants';

//hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

//redux
import { useDispatch, useSelector } from 'react-redux';

//selectors
import {
  selectedRecentDashboardWorkQueue,
  selectedRecentDashboardWorkQueueLoading,
  selectedRecentDashboardWorkQueueError
} from '../_redux/selectors';

//components
import CardQueue from './CardQueue';
import { FailedApiReload } from 'app/components';

//actions
import { dashboardActions as actions } from '../_redux/reducers';

//types
import { DashBoardProps, RecentlyWorkedQueueProps } from '../types';
import { genAmtId } from 'app/_libraries/_dls/utils';

const DashboardRecentWorkQueue: React.FC<DashBoardProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { collectorRecentWorkQueueList } = useSelector(
    selectedRecentDashboardWorkQueue
  );
  const testId = 'dashboard_recentWorkQueue';

  const isLoading = useSelector(selectedRecentDashboardWorkQueueLoading);

  const isError = useSelector(selectedRecentDashboardWorkQueueError);

  const handleReload = () => dispatch(actions.getRecentlyWorkQueue());

  React.useEffect(() => {
    dispatch(actions.getRecentlyWorkQueue());
  }, [dispatch]);

  if (isError)
    return (
      <div>
        <FailedApiReload
          id="dashboard-recent-work-queue-failed"
          className="mt-80"
          onReload={handleReload}
          dataTestId={`${testId}_failed-api`}
        />
      </div>
    );

  return (
    <div className={classnames({ loading: isLoading })}>
      <p data-testid={genAmtId(testId, 'title', '')}>
        {collectorRecentWorkQueueList?.length || 0}{' '}
        {t(I18N_DASH_BOARD.RECENTLY_WORKED_QUEUES)}
      </p>
      {(collectorRecentWorkQueueList || [])?.map(
        (item: RecentlyWorkedQueueProps, index: number) => (
          <CardQueue
            id={`dashboard__work-queue__card-queue__${item.queueName}`}
            key={index}
            queueName={item.queueName}
            queueId={item.queueId}
            collectorQueueStats={item.collectorQueueStats}
            collectorPromiseStats={item.collectorPromiseStats}
            queueType={item.queueType}
            lastWorkDate={item.lastWorkDate}
            assignedTo={item.assignedTo}
            totalNumbersOfAccount={item.totalNumbersOfAccount}
            totalOutstandingBalanceAmount={item.totalOutstandingBalanceAmount}
            dataTestId={`${item.queueName}_${testId}_cardQueue`}
          />
        )
      )}
    </div>
  );
};

export default DashboardRecentWorkQueue;
