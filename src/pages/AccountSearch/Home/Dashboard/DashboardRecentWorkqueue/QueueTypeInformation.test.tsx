//react-testing-lib
import { screen } from '@testing-library/react';
//mock test utils
import { renderMockStore } from 'app/test-utils';
import React from 'react';
//components
import QueueTypeInformation from './QueueTypeInformation';

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockData = {
  assignedTo: 'Mock',
  lastWorkDate: '01/01/2022',
  queueType: 'Pull',
  totalNumbersOfAccount: '4',
  totalOutstandingBalanceAmount: '10000000'
};
const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(<QueueTypeInformation {...mockData} />, {
    initialState
  });
};

describe('Should render UI QueueTypeInformation', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isCollector: true
    }
  };

  it('should render UI', () => {
    const data: Partial<RootState> = {
      dashboard: {
        callActivity: {
          data: [],
          period: '',
          error: false,
          loading: false
        },
        goal: {
          data: [],
          error: '',
          loading: false
        },
        workQueue: {
          data: {},
          error: '',
          loading: false
        },
        workStatistic: {
          data: [],
          error: '',
          selectedPeriod: '',
          loading: false
        },
        recentWorkQueue: {
          data: {
            collectorRecentWorkQueueList: [
              {
                queueName: 'Queue 004 - Bankruptcy',
                collectorQueueStats: {
                  accountsWorked: '180',
                  amountCollected: '24032.00',
                  totalAccounts: '680',
                  totalAmount: '59092.00'
                },
                collectorPromiseStats: {
                  brokenAmount: '2352.00',
                  brokenPromises: '156',
                  keptAmount: '20840.00',
                  keptPromises: '220',
                  pendingAmount: '35900.00',
                  pendingPromises: '304',
                  totalAmount: '59092.00',
                  totalPromises: '680'
                }
              }
            ],
            listWorkQueueAccount: {
              memberSequenceIdentifier: '001',
              presentationInstrumentIdentifier:
                'presentationInstrumentIdentifier',
              primaryName: 'primaryName',
              socialSecurityIdentifier: 'socialSecurityIdentifier'
            }
          },
          error: '',
          loading: false
        }
      }
    };

    renderWrapper(data);

    const queryTextTitle = screen.getByText('$10,000,000.00');

    expect(queryTextTitle).toBeInTheDocument();
  });
});
