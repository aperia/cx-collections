//react-testing-lib
import { screen } from '@testing-library/react';
//mock test utils
import { renderMockStore } from 'app/test-utils';
import React from 'react';
//components
import CoreViewQueueDetail from './CoreViewQueueDetail';

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockData = {
  lastWorkDate: '01/01/2022',
  queueType: 'Pull',
  totalNumbersOfAccount: '4',
  totalOutstandingBalanceAmount: '10000000',
  assignedTo: 'Mock Name'
};
const renderWrapper = () => {
  return renderMockStore(<CoreViewQueueDetail {...mockData} />, {});
};

describe('Should render UI CoreViewQueueDetail', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isCollector: true
    }
  };

  it('should render UI', () => {
    renderWrapper();

    const queryTextTitle = screen.getByText('$10,000,000.00');

    expect(queryTextTitle).toBeInTheDocument();
  });

  it('should render UI > case 2', () => {
    const mockData2 = {
      lastWorkDate: '01/01/2022',
      queueType: 'Pull',
      totalNumbersOfAccount: '4',
      totalOutstandingBalanceAmount: '10000000',
      assignedTo: 'Mock Name',
      fromDashboard: false
    };

    renderMockStore(<CoreViewQueueDetail {...mockData2} />, {});

    const queryTextTitle = screen.getByText('$10,000,000.00');

    expect(queryTextTitle).toBeInTheDocument();
  });
});
