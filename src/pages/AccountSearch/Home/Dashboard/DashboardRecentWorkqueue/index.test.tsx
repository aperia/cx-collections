import React from 'react';

//mock test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';

//components
import DashboardRecentWorkQueue from '.';

//actions
import { dashboardActions } from '../_redux/reducers';
import userEvent from '@testing-library/user-event';
import { USER_ROLE } from '../constants';

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockActions = mockActionCreator(dashboardActions);

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <DashboardRecentWorkQueue userType={USER_ROLE.CXCOL5} />,
    { initialState }
  );
};

describe('Dashboard RecentWorkQueue', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isCollector: true
    }
  };
  it('should render UI', () => {
    const data: Partial<RootState> = {
      dashboard: {
        callActivity: {
          data: [],
          period: '',
          error: false,
          loading: false
        },
        goal: {
          data: [],
          error: '',
          loading: false
        },
        workQueue: {
          data: {},
          error: '',
          loading: false
        },
        workStatistic: {
          data: [],
          error: '',
          selectedPeriod: '',
          loading: false
        },
        recentWorkQueue: {
          data: {
            collectorRecentWorkQueueList: [
              {
                queueName: 'Queue 004 - Bankruptcy',
                collectorQueueStats: {
                  accountsWorked: '180',
                  amountCollected: '24032.00',
                  totalAccounts: '680',
                  totalAmount: '59092.00'
                },
                collectorPromiseStats: {
                  brokenAmount: '2352.00',
                  brokenPromises: '156',
                  keptAmount: '20840.00',
                  keptPromises: '220',
                  pendingAmount: '35900.00',
                  pendingPromises: '304',
                  totalAmount: '59092.00',
                  totalPromises: '680'
                }
              }
            ],
            listWorkQueueAccount: {
              memberSequenceIdentifier: '001',
              presentationInstrumentIdentifier:
                'presentationInstrumentIdentifier',
              primaryName: 'primaryName',
              socialSecurityIdentifier: 'socialSecurityIdentifier'
            }
          },
          error: '',
          loading: false
        }
      }
    };

    renderWrapper(data);

    const queryTextTitle = screen.getByText('txt_total_number_of_accounts');

    expect(queryTextTitle).toBeInTheDocument();
  });

  it('should trigger workQueue with collector', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app',
        isCollector: false
      }
    };
    const data: Partial<RootState> = {
      dashboard: {
        callActivity: {
          data: [],
          period: '',
          error: false,
          loading: false
        },
        goal: {
          data: [],
          error: '',
          loading: false
        },
        workQueue: {
          data: [],
          error: '',
          loading: false
        },
        workStatistic: {
          data: [],
          error: '',
          selectedPeriod: '',
          loading: false
        },
        recentWorkQueue: {
          data: {
            collectorRecentWorkQueueList: [
              {
                queueName: 'Queue 004 - Bankruptcy',
                collectorQueueStats: {
                  accountsWorked: '180',
                  amountCollected: '24032.00',
                  totalAccounts: '680',
                  totalAmount: '59092.00'
                },
                collectorPromiseStats: {
                  brokenAmount: '2352.00',
                  brokenPromises: '156',
                  keptAmount: '20840.00',
                  keptPromises: '220',
                  pendingAmount: '35900.00',
                  pendingPromises: '304',
                  totalAmount: '59092.00',
                  totalPromises: '680'
                },
                queueType: 'Pull'
              }
            ],
            listWorkQueueAccount: {
              memberSequenceIdentifier: '001',
              presentationInstrumentIdentifier:
                'presentationInstrumentIdentifier',
              primaryName: 'primaryName',
              socialSecurityIdentifier: 'socialSecurityIdentifier'
            }
          },
          error: '',
          loading: false
        }
      }
    };
    renderWrapper(data);
  });
});

describe('should render with error msg', () => {
  const data: Partial<RootState> = {
    dashboard: {
      callActivity: {
        data: [],
        period: '',
        error: false,
        loading: false
      },
      goal: {
        data: [],
        error: '',
        loading: false
      },
      workQueue: {
        data: [],
        error: '',
        loading: false
      },
      workStatistic: {
        data: [],
        error: '',
        selectedPeriod: '',
        loading: false
      },
      recentWorkQueue: {
        data: [],
        error: 'error_msg',
        loading: false
      }
    }
  };

  it('should render UI with error message', () => {
    renderWrapper(data);

    const queryTextReload = screen.getByText(
      'txt_data_load_unsuccessful_click_reload_to_try_again'
    );

    expect(queryTextReload).toBeInTheDocument();
  });

  it('should render UI with error message and click reload button', () => {
    const mockReload = mockActions('getRecentlyWorkQueue');

    renderWrapper(data);

    const reloadButton = screen.getByRole('button', {
      name: 'txt_reload'
    });

    userEvent.click(reloadButton);

    expect(mockReload).toBeCalledWith();
  });
});
