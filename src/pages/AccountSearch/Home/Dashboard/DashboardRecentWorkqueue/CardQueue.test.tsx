import React from 'react';
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import { screen } from '@testing-library/react';
import CardQueue from './CardQueue';
import { timerWorkingActions } from 'pages/TimerWorking/_redux/reducers';
import { tabActions } from 'pages/__commons/TabBar/_redux';
import { RecentlyWorkedQueueProps } from '../types';
import { dashboardActions } from '../_redux/reducers';

const data: RecentlyWorkedQueueProps = {
  queueName: 'test',
  queueId: '1123',
  collectorPromiseStats: {
    pendingPromises: '1000',
    pendingAmount: '652000.00',
    keptPromises: '200',
    keptAmount: '128000.00',
    brokenPromises: '800',
    brokenAmount: '500000.00',
    totalPromises: '2000',
    totalAmount: '1400500.00'
  },
  collectorQueueStats: {
    accountsWorked: '180',
    amountCollected: '24032.00',
    totalAccounts: '680',
    totalAmount: '59092.00'
  },
  listWorkQueueAccount: {
    primaryName: 'SMITH,ADELE               ',
    memberSequenceIdentifier: '001',
    socialSecurityIdentifier:
      '{"maskedValue":"***-**-1111","eValue":"VOL(bGl2RGN1bmskIzlSLiMo)"}',
    presentationInstrumentIdentifier:
      '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}'
  },
  assignedTo: 'Mock name',
  lastWorkDate: '2020/10/10',
  totalNumbersOfAccount: 4,
  totalOutstandingBalanceAmount: '10000000'
} as any;

const renderWrapper = (data: RecentlyWorkedQueueProps) => {
  return renderMockStoreId(
    <CardQueue
      id="id"
      queueName="test"
      collectorQueueStats={data.collectorQueueStats}
      collectorPromiseStats={data.collectorPromiseStats}
      queueType={data.queueType}
      assignedTo={data.assignedTo}
      queueId={data.queueId}
      lastWorkDate={data.lastWorkDate}
      totalNumbersOfAccount={data.totalNumbersOfAccount}
      totalOutstandingBalanceAmount={data.totalOutstandingBalanceAmount}
    />
  );
};

const timerWorkingActionsSpy = mockActionCreator(timerWorkingActions);
const tabActionsSpy = mockActionCreator(tabActions);
const dashboardActionsMock = mockActionCreator(dashboardActions);

describe('CardQueue', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isCollector: true
    }
  };
  it('Render component', () => {
    renderWrapper(data);
    expect(screen.getByText('txt_work_on_queue')).toBeInTheDocument();
  });

  it('Render component > case 2', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app',
        isCollector: true,
        privileges: ['CXCOL5']
      }
    };
    renderWrapper(data);
    expect(screen.getByText('txt_work_on_queue')).toBeInTheDocument();
  });

  it('Render component > case 3', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app',
        isCollector: true,
        privileges: ['CXCOL5']
      }
    };
    renderWrapper({
      ...data,
      lastWorkDate: undefined as never,
      assignedTo: undefined as never,
      totalNumbersOfAccount: undefined as never,
      totalOutstandingBalanceAmount: undefined as never,
      queueId: undefined as never
    });
    expect(screen.getByText('txt_work_on_queue')).toBeInTheDocument();
  });

  it('handleOpenWorkOnQueue', () => {
    timerWorkingActionsSpy('startAuthenticate');
    tabActionsSpy('addTab');
    const triggerNextAccountWorkOnQueue = dashboardActionsMock(
      'triggerNextAccountWorkOnQueue'
    );
    renderWrapper(data);
    const button = screen.getByText('txt_work_on_queue');
    button?.click();
    expect(triggerNextAccountWorkOnQueue).toHaveBeenCalled();
  });

  it('handleOpenQueueDetail', () => {
    const tabActionsSpy = mockActionCreator(tabActions);
    const addTab = tabActionsSpy('addTab');
    renderWrapper({ ...data, queueType: 'Pull' });
    const button = screen.getByText('txt_view');
    button?.click();
    expect(addTab).toBeCalled();
  });

  it('handleOpenWorkOnQueue with empty data', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: undefined
    };
    const spy = timerWorkingActionsSpy('startAuthenticate');
    const spyAction = tabActionsSpy('addTab');
    renderWrapper({ ...data, queueName: 'test', listWorkQueueAccount: {} });
    const button = screen.getByText('txt_work_on_queue');
    button?.click();
    expect(spy).not.toHaveBeenCalledWith('VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)');
    expect(spyAction).not.toHaveBeenCalledWith({
      title: 'SMITH,ADELE                - 022009······2624',
      storeId: 'VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)-001',
      accEValue: 'VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)',
      tabType: 'accountDetail',
      iconName: 'account-details',
      props: {
        accEValue: 'VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)',
        accNbr: '022009******2624',
        memberSequenceIdentifier: '001',
        primaryName: 'SMITH,ADELE               ',
        socialSecurityIdentifier:
          '{"maskedValue":"***-**-1111","eValue":"VOL(bGl2RGN1bmskIzlSLiMo)"}',
        storeId: 'VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)-001'
      }
    });
  });
});
