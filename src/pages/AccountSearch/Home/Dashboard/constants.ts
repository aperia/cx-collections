export const I18N_DASH_BOARD = {
  DASH_BOARD: 'txt_dash_board',
  TEAM_GOAL: 'txt_team_goal',
  MY_GOAL: 'txt_my_goal',
  WORK_STATISTICS: 'txt_work_statistics',
  TEAM_WORK_STATISTICS: 'txt_team_work_statistics',
  CALL_ACTIVITY: 'txt_call_activity',
  TEAM_CALL_ACTIVITY: 'txt_team_call_activity',
  WORK_QUEUE: 'txt_work_queue',
  COLLECTOR_STATISTICS: 'txt_collector_statistics',
  ACCOUNT_STATISTICS: 'txt_account_statistics',
  PROMISE_STATISTICS: 'txt_promise_statistics',
  DAILY: 'txt_daily',
  MONTHLY: 'txt_monthly',
  PERIOD: 'txt_period',
  PROMISES: 'txt_promises',
  AMOUNT: 'txt_amount',
  PENDING: 'txt_pending_plural',
  KEPT: 'txt_kept',
  BROKEN: 'txt_broken',
  TOTAL: 'txt_total',
  TOTAL_CAPITALIZE: 'txt_total_capitalize',
  TOTAL_CODE: 'total promises',
  ACCOUNT_WORKED_PER_HOUR: 'txt_account_worked_per_hour',
  PROMISES_TO_PAY_PER_HOUR: 'txt_promises_to_pay_per_hour',
  CONTACT_RATE: 'txt_contact_rate',
  KEPT_RATE: 'txt_kept_rate',
  VIEW_ALL_QUEUES: 'txt_view_all_queues',
  VIEW_ALL_COLLECTORS: 'txt_view_all_collectors',
  TOTAL_QUEUES: 'txt_total_queues',
  QUEUE_STATISTICS: 'txt_queue_statistics',
  ACCOUNT: 'txt_account',
  WORK_ON_QUEUE: 'txt_work_on_queue',
  RECENTLY_WORKED_QUEUES: 'txt_recently_worked_queues',
  QUEUE: 'txt_queue'
};

export const TITLE_CODE = {
  GOAL: 'G',
  WORK_STATISTIC: 'WS',
  CALL_ACTIVITY: 'CA',
  WORK_QUEUE: 'WQ',
  COLLECTOR_STATISTIC: 'CS'
};

export const USER_ROLE = {
  ADMIN_ROLE: 'CXADMN',
  USER_ROLE: 'CXCOL',
  ADMIN1: 'CXADMN1',
  ADMIN2: 'CXADMN2',
  CXCOL1: 'CXCOL1',
  CXCOL5: 'CXCOL5',
  COLLECTOR1: 'COLLECTOR1',
  SUPERVISOR: 'SUPERVISOR',
  ADMIN: 'ADMIN'
};

export const DASHBOARD_DROPDOWN_LIST = ['Today', 'Month', 'Year'];
export const DASHBOARD_PERIOD: RefDataValue[] = [
  {
    value: 'today',
    description: 'txt_today'
  },
  {
    value: 'month',
    description: 'txt_month'
  },
  {
    value: 'year',
    description: 'txt_year'
  }
];

export const DASHBOARD_GOAL_ITEM = {
  ACCOUNTS_PER_MONTH: 'txt_accounts_per_month',
  ACCOUNTS_PER_DAY: 'txt_accounts_per_day',
  ACCOUNTS_WORKED: 'txt_accounts_worked',
  AMOUNT_COLLECTED: 'txt_amount_collected',
  AMOUNT_PER_DAY: 'txt_amount_per_day',
  AMOUNT_PER_MONTH: 'txt_amount_per_month'
};

export const DASHBOARD_WORK_STATISTICS = {
  TOTAL_ACCOUNTS: 'txt_total_accounts',
  TOTAL_AMOUNT: 'txt_total_amount',
  ACCOUNTS_WORKED: 'txt_accounts_worked_plural',
  AMOUNT_COLLECTED: 'txt_amount_collected_plural',
  NEXT_WORK_ACCOUNT: 'txt_next_work_account',
  DUE_ACCOUNT: 'txt_due_account'
};

export const DASHBOARD_COLLECTOR = {
  HIGH_PERFORMING: 'txt_collector_top_high_performing',
  PENDING_PROMISES: 'txt_pending_promises',
  KEPT_PROMISES: 'txt_kept_promises',
  BROKEN_PROMISES: 'txt_broken_promises',
  TOTAL_PROMISES: 'txt_total_promises',
  WORK_QUEUES: 'txt_work_queues',
  BREAK: 'txt_break',
  BREAK_CAPITALIZE: 'txt_break_capitalize',
  TOTAL_BREAK: 'txt_total_break',
  TOTAL_BREAKS: 'txt_total_breaks',
  TOTAL_BREAK_TIME: 'txt_total_break_time',
  CALL_INFORMATION: 'txt_call_information',
  INBOUND: 'txt_inbound',
  OUTBOUND: 'txt_outbound',
  AVERAGE_TIME: 'txt_average_time_db',
  TEAM_AVERAGE: 'txt_team_average',
  EFFECTIVE: 'txt_effective',
  HIGH: 'txt_high',
  MEDIUM: 'txt_medium',
  POOR: 'txt_poor',
  LOW: 'txt_low',
  REST: 'txt_rest',
  EFFECTIVE_BREAK: 'txt_effective_break',
  EFFECTIVE_REST: 'txt_effective_rest',
  MEDIUM_BREAK: 'txt_medium_break',
  MEDIUM_REST: 'txt_medium_rest',
  COLLECTOR_LIST: 'txt_collector_list',
  COLLECTOR: 'txt_collector'
};

export const DASHBOARD_GET_NEXT_ACCOUNT = {
  NEXT_ACCOUNT_FAILED_TO_RETRIEVE: 'txt_next_account_failed_to_retrieve',
  ACCOUNT_FAILED_TO_RETRIEVE: 'txt_account_failed_to_retrieve'
};

export const QUEUE_TYPE_INFO = {
  lastWorkDate: '01/22/2022',
  assignedTo: 'Nate Nash',
  totalNumbersOfAccount: '4',
  totalOutstandingBalanceAmount: '9000000'
};
