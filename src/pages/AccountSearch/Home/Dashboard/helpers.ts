import { FormatTime } from 'app/constants/enums';
import { getEndOfDay, getStartOfDay } from 'app/helpers';
import { I18N_DASH_BOARD, TITLE_CODE } from './constants';

export const dynamicTitle = (name: string) => {
  const { isAdminRole } = window.appConfig.commonConfig || {};

  if (isAdminRole) {
    switch (name) {
      case TITLE_CODE.GOAL:
        return I18N_DASH_BOARD.TEAM_GOAL;
      case TITLE_CODE.WORK_STATISTIC:
        return I18N_DASH_BOARD.TEAM_WORK_STATISTICS;
      case TITLE_CODE.CALL_ACTIVITY:
        return I18N_DASH_BOARD.TEAM_CALL_ACTIVITY;
      case TITLE_CODE.COLLECTOR_STATISTIC:
        return I18N_DASH_BOARD.COLLECTOR_STATISTICS;
    }
  }

  switch (name) {
    case TITLE_CODE.GOAL:
      return I18N_DASH_BOARD.MY_GOAL;
    case TITLE_CODE.WORK_STATISTIC:
      return I18N_DASH_BOARD.WORK_STATISTICS;
    case TITLE_CODE.CALL_ACTIVITY:
      return I18N_DASH_BOARD.CALL_ACTIVITY;
    case TITLE_CODE.WORK_QUEUE:
      return I18N_DASH_BOARD.WORK_QUEUE;
  }
};

export const numberToPercentString = (number: number) => {
  return `${number}%`;
};

export const averagePercent = (total: number, per: number) => {
  return `${Math.round((per * 100) / total)}%`;
};

export const formatPeriod = (inPeriod: string) => {
  const currentDate = new Date();

  const startMonthDate = new Date(
    currentDate.getFullYear(),
    currentDate.getMonth(),
    1
  );
  const endMonthDate = new Date(
    currentDate.getFullYear(),
    currentDate.getMonth() + 1,
    0
  );
  const startYearDate = new Date(currentDate.getFullYear(), 0, 1);
  const endYearDate = new Date(currentDate.getFullYear(), 11, 31);
  const startDateTime = getStartOfDay(currentDate, FormatTime.UTCAllDatetime);

  switch (inPeriod) {
    case 'today':
      return {
        startDateTime,
        endDateTime: getEndOfDay(currentDate, FormatTime.UTCAllDatetime)
      };
    case 'month':
      return {
        startDateTime: getStartOfDay(startMonthDate, FormatTime.UTCAllDatetime),
        endDateTime: getEndOfDay(endMonthDate, FormatTime.UTCAllDatetime)
      };
    case 'year':
      return {
        startDateTime: getStartOfDay(startYearDate, FormatTime.UTCAllDatetime),
        endDateTime: getEndOfDay(endYearDate, FormatTime.UTCAllDatetime)
      };
    default:
      return {
        startDateTime,
        endDateTime: ''
      };
  }
};

export const padLeadingZeros = (num: number, size: number) => {
  let m = `${num}`;
  if (m.length < size) m = '0' + m;
  return m;
};

export const randomQueueType = (randomNumber: number) => {
  // randomNumber is 0 or 1
  const QUEUE_TYPE = ['Pull', 'Push'];
  return QUEUE_TYPE[randomNumber];
}