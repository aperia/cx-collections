import { act } from '@testing-library/react';
import { renderMockStore, renderMockStoreId, storeId } from 'app/test-utils';
import React from 'react';
import useCountDownTimer from './useCountDownTimer';

describe('should useCountDownTimer with full data', () => {
  const Comp = () => {
    const { toStringMin, toStringSec } = useCountDownTimer(
      1,
      0,
      '1',
      storeId,
      false
    );

    return (
      <>
        <div>{toStringMin}</div>
        <div>{toStringSec}</div>
      </>
    );
  };

  it('should useCountDownTimer', () => {
    jest.useFakeTimers();
    act(() => {
      renderMockStore(<Comp />, {});
    });
    jest.advanceTimersByTime(1000);
    jest.runOnlyPendingTimers();
  });
});

describe('should useCountDownTimer with full data with toggle true', () => {
  const Comp = () => {
    const { toStringMin, toStringSec } = useCountDownTimer(
      0,
      0,
      '1234',
      storeId,
      false
    );

    return (
      <>
        <div>{toStringMin}</div>
        <div>{toStringSec}</div>
      </>
    );
  };

  it('should useCountDownTimer', () => {
    const initialState = {
      dashboard: {
        accountCountTime: {
          [storeId]: {
            toggleExtendModal: true
          }
        }
      }
    };
    jest.useFakeTimers();
    act(() => {
      renderMockStoreId(<Comp />, { initialState });
    });
    jest.advanceTimersByTime(1000);
    jest.runOnlyPendingTimers();
  });
});

describe('should useCountDownTimer with full data', () => {
  const Comp = () => {
    const { toStringMin, toStringSec } = useCountDownTimer(
      5,
      0,
      '1',
      storeId,
      false
    );

    return (
      <>
        <div>{toStringMin}</div>
        <div>{toStringSec}</div>
      </>
    );
  };

  it('should ', () => {
    jest.useFakeTimers();
    act(() => {
      renderMockStore(<Comp />, {});
    });
    jest.advanceTimersByTime(1000);
    jest.runOnlyPendingTimers();
  });
});

describe('should useCountDownTimer with full data', () => {
  const Comp = () => {
    const { toStringMin, toStringSec } = useCountDownTimer(
      0,
      0,
      '1',
      storeId,
      false
    );

    return (
      <>
        <div>{toStringMin}</div>
        <div>{toStringSec}</div>
      </>
    );
  };

  it('should ', () => {
    jest.useFakeTimers();
    act(() => {
      renderMockStore(<Comp />, {});
    });
    jest.advanceTimersByTime(1000);
    jest.runOnlyPendingTimers();
  });
});

describe('should useCountDownTimer with empty data', () => {
  const Comp = () => {
    const { toStringMin, toStringSec } = useCountDownTimer(
      0,
      0,
      '',
      storeId,
      false
    );

    return (
      <>
        <div>{toStringMin}</div>
        <div>{toStringSec}</div>
      </>
    );
  };
  it('useCountDownTimer', () => {
    jest.useFakeTimers();
    renderMockStore(<Comp />, {});
    jest.runAllTimers();
  });
});

describe('should useCountDownTimer with extend time', () => {
  const Comp = () => {
    const { toStringMin, toStringSec } = useCountDownTimer(
      5,
      7,
      '',
      storeId,
      false
    );

    return (
      <>
        <div>{toStringMin}</div>
        <div>{toStringSec}</div>
      </>
    );
  };
  it('useCountDownTimer', () => {
    jest.useFakeTimers();
    renderMockStore(<Comp />, {});
    jest.runAllTimers();
  });
});

describe('should useCountDownTimer with extend time and isReset true', () => {
  const Comp = () => {
    const { toStringMin, toStringSec } = useCountDownTimer(
      5,
      7,
      '',
      storeId,
      true
    );

    return (
      <>
        <div>{toStringMin}</div>
        <div>{toStringSec}</div>
      </>
    );
  };
  it('useCountDownTimer', () => {
    jest.useFakeTimers();
    renderMockStore(<Comp />, {});
    jest.runAllTimers();
  });
});
