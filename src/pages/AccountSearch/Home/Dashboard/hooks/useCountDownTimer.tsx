import { useStoreIdSelector } from 'app/hooks';

import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { useCallback, useEffect, useState } from 'react';
import { batch, useDispatch } from 'react-redux';
import { padLeadingZeros } from '../helpers';
import { dashboardActions } from '../_redux/reducers';
import { takeToggleAccountExtend } from '../_redux/selectors';

const useCountDownTimer = (
  minutes: number | undefined,
  extendMinutes: number,
  queueId: string,
  storeId: string,
  isReset: boolean | undefined
) => {
  const dispatch = useDispatch();
  const isOpenExtendModal: boolean = useStoreIdSelector(
    takeToggleAccountExtend,
    storeId
  );
  const [[toStringMin, toStringSec, min, sec], setTime] = useState([
    '00',
    '00',
    0,
    0
  ]);

  const [over, setOver] = useState(false);

  const [reset, setReset] = useState(false);

  const tick = useCallback(() => {
    if (min === 0 && sec === 0) {
      batch(() => {
        setOver(true);
        isOpenExtendModal &&
          dispatch(dashboardActions.toggleExtendModal({ storeId }));
      });
    }

    if (sec === 0) {
      setTime([
        `${padLeadingZeros(min - 1, 2)}`,
        `${padLeadingZeros(59, 2)}`,
        min - 1,
        59
      ]);
    } else {
      setTime([
        `${padLeadingZeros(min, 2)}`,
        `${padLeadingZeros(sec - 1, 2)}`,
        min,
        sec - 1
      ]);
    }

    if (min === 5 && sec === 0) {
      dispatch(dashboardActions.toggleExtendModal({ storeId }));
    }
  }, [min, sec, dispatch, storeId, isOpenExtendModal]);

  useEffect(() => {
    if (over || reset || !queueId) return;
    const timerID = setInterval(() => tick(), 1000);
    return () => clearInterval(timerID);
  }, [min, sec, over, reset, queueId, tick]);

  useEffect(() => {
    if (minutes) setTime(['00', '00', minutes, 0]);
    if (extendMinutes) {
      setTime(['00', '00', extendMinutes, 0]);
      dispatch(dashboardActions.resetExtendLockTime());
    }
  }, [minutes, setTime, extendMinutes, dispatch]);

  useEffect(() => {
    over &&
      !reset &&
      queueId &&
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'warning',
          message: 'txt_account_lock_timer_msg'
        })
      );
  }, [dispatch, storeId, over, reset, queueId]);

  useEffect(() => {
    isReset && setReset(true);
  }, [isReset]);

  return {
    toStringMin,
    toStringSec,
    openCountDownModal: isOpenExtendModal
  };
};

export default useCountDownTimer;
