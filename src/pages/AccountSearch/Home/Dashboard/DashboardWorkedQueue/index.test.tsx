import React from 'react';

//mock test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';

//components
import DashboardWorkQueue from '.';

//actions
import { dashboardActions } from '../_redux/reducers';
import userEvent from '@testing-library/user-event';
import { USER_ROLE } from '../constants';
import { QUEUE_LIST } from 'pages/AccountDetails/NextActions/constants';
import { tabActions } from 'pages/__commons/TabBar/_redux';

jest.mock('app/_libraries/_dls/components/ProgressBar', () => () => {
  return <div data-testid="progress-bar">ProgressBar</div>;
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockActions = mockActionCreator(dashboardActions);

const mockTabActions = mockActionCreator(tabActions);

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <DashboardWorkQueue userType={USER_ROLE.CXCOL5} title="test title" />,
    { initialState }
  );
};

describe('Dashboard WorkQueue', () => {
  it('should render UI', () => {
    const data: Partial<RootState> = {
      dashboard: {
        callActivity: {
          data: [],
          period: '',
          error: false,
          loading: false
        },
        goal: {
          data: [],
          error: '',
          loading: false
        },
        workQueue: {
          data: {
            promiseStatistics: [
              {
                name: 'pending promise',
                promises: 7777,
                amount: 2332
              },
              {
                name: 'kept promise',
                promises: 1234,
                amount: 10000
              },
              {
                name: 'broken promise',
                promises: 5678,
                amount: 25000
              },
              {
                name: 'total promise',
                promises: 9001,
                amount: 184200
              }
            ],
            presentWorkedQueues: [
              {
                queueId: '004',
                description: 'bankruptcy',
                primaryName: 'SMITH,ADELE',
                memberSequenceIdentifier: '001',
                socialSecurityIdentifier:
                  '{"maskedValue":"***-**-1111","eValue":"VOL(bGl2RGN1bmskIzlSLiMo)"}',
                presentationInstrumentIdentifier:
                  '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}',
                accountWorked: '180',
                totalAccountWorked: '680',
                amountCollected: '24032',
                totalAmountCollected: '59092',
                promiseStatistics: [
                  {
                    name: 'pending promises',
                    promises: 304,
                    amount: 35900
                  },
                  {
                    name: 'kept promises',
                    promises: 220,
                    amount: 20840
                  },
                  {
                    name: 'broken promises',
                    promises: 159,
                    amount: 2352
                  },
                  {
                    name: 'total promises',
                    promises: 680,
                    amount: 59092
                  }
                ]
              }
            ]
          },
          error: '',
          loading: false
        },
        workStatistic: {
          data: [],
          error: '',
          selectedPeriod: '',
          loading: false
        }
      }
    };

    renderWrapper(data);

    const queryTextTitle = screen.getByText('test title');

    expect(queryTextTitle).toBeInTheDocument();
  });

  it('should trigger view all queues with collector', () => {
    const mockAddTab = mockTabActions('addTab');
    const data: Partial<RootState> = {
      dashboard: {
        callActivity: {
          data: [],
          period: '',
          error: false,
          loading: false
        },
        goal: {
          data: [],
          error: '',
          loading: false
        },
        workQueue: {
          data: [],
          error: '',
          loading: false
        },
        workStatistic: {
          data: [],
          error: '',
          selectedPeriod: '',
          loading: false
        }
      }
    };

    renderWrapper(data);

    const viewAllQueueButton = screen.getByRole('button', {
      name: 'txt_view_all_queues'
    });

    userEvent.click(viewAllQueueButton);

    expect(mockAddTab).toBeCalledWith({
      title: QUEUE_LIST,
      storeId: 'queueList',
      accEValue: 'accEValue',
      iconName: 'file',
      tabType: 'queueList'
    });
  });
});

describe('should render with error msg', () => {
  const data: Partial<RootState> = {
    dashboard: {
      callActivity: {
        data: [],
        period: '',
        error: false,
        loading: false
      },
      goal: {
        data: [],
        error: '',
        loading: false
      },
      workQueue: {
        data: [],
        error: 'error_msg',
        loading: false
      },
      workStatistic: {
        data: [],
        error: '',
        selectedPeriod: '',
        loading: false
      }
    }
  };

  it('should render UI with error message', () => {
    renderWrapper(data);

    const queryTextReload = screen.getByText(
      'txt_data_load_unsuccessful_click_reload_to_try_again'
    );

    expect(queryTextReload).toBeInTheDocument();
  });

  it('should render UI with error message and click reload button', () => {
    const mockReload = mockActions('getDashboardWorkQueue');

    renderWrapper(data);

    const reloadButton = screen.getByRole('button', {
      name: 'txt_reload'
    });

    userEvent.click(reloadButton);

    expect(mockReload).toBeCalledTimes(2);
  });
});
