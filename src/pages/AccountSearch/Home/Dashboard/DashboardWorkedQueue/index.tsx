import React from 'react';

//libs
import classnames from 'classnames';

//redux
import { useDispatch, useSelector } from 'react-redux';
import { tabActions } from 'pages/__commons/TabBar/_redux';

//types
// RecentlyWorkedQueueProps
import { DashBoardProps } from '../types';

//actions
import { dashboardActions as actions } from '../_redux/reducers';

//constants
import { DASHBOARD_WORK_STATISTICS, I18N_DASH_BOARD } from '../constants';
import { QUEUE_LIST } from 'pages/AccountDetails/NextActions/constants';

//selectors
import {
  selectedDashboardWorkQueue,
  selectedDashboardWorkQueueLoading,
  selectedDashboardWorkQueueError,
  selectedDashboardWorkStatisticPeriod
} from '../_redux/selectors';

//translate
import { useTranslation } from 'app/_libraries/_dls/hooks';

//components
import { Button } from 'app/_libraries/_dls/components';
import { FailedApiReload } from 'app/components';
import AccountStatisticItem from '../common/AccountStatisticItem';
import PromiseStatistic from '../common/PromiseStatistic';

//helpers
import { formatCommon } from 'app/helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';

const DashBoardWorkQueue: React.FC<DashBoardProps> = ({ title }) => {
  const dispatch = useDispatch();

  const { totalQueues, collectorQueueStats, collectorPromiseStats } =
    useSelector(selectedDashboardWorkQueue);

  const period: RefDataValue = useSelector(
    selectedDashboardWorkStatisticPeriod
  );

  const { t } = useTranslation();

  const isLoading = useSelector(selectedDashboardWorkQueueLoading);

  const isError = useSelector(selectedDashboardWorkQueueError);

  const handleReload = () =>
    dispatch(actions.getDashboardWorkQueue({ period: period.value }));

  const handleViewAllQueues = () => {
    dispatch(
      tabActions.addTab({
        title: QUEUE_LIST,
        storeId: 'queueList',
        accEValue: 'accEValue',
        iconName: 'file',
        tabType: 'queueList'
      })
    );
  };

  React.useEffect(() => {
    dispatch(actions.getDashboardWorkQueue({ period: period.value }));
  }, [dispatch, period]);

  if (isError)
    return (
      <div>
        <FailedApiReload
          id="dashboard-work-queue-failed"
          className="mt-80"
          onReload={handleReload}
          dataTestId="dashboardWorkedQueue_failedApi"
        />
      </div>
    );

  const renderWorkQueueCollector = (
    <div className={classnames({ loading: isLoading })}>
      <div className="d-flex justify-content-between mb-16">
        <h4 data-testid={genAmtId('dashboardWorkedQueue', 'title', '')}>
          {title}
        </h4>
        <Button
          id="dashboard__work-queue__btn-view-all-queues"
          className="mr-n8"
          onClick={handleViewAllQueues}
          size="sm"
          variant="outline-primary"
          dataTestId="dashboardWorkedQueue_view-all-btn"
        >
          {t(I18N_DASH_BOARD.VIEW_ALL_QUEUES)}
        </Button>
      </div>
      <div className="card card-dashboard">
        <div className="d-xl-flex">
          <div className="d-flex w-xl-55">
            <div className="w-fixed-left">
              <div className="d-flex flex-column justify-content-md-end h-100">
                <h3
                  className="color-grey fw-500"
                  data-testid={genAmtId(
                    'dashboardWorkedQueue',
                    'totalQueue',
                    ''
                  )}
                >
                  {totalQueues}
                </h3>
                <p
                  className="fs-11 color-grey-l24"
                  data-testid={genAmtId(
                    'dashboardWorkedQueue',
                    'totalQueue-label',
                    ''
                  )}
                >
                  {t(I18N_DASH_BOARD.TOTAL_QUEUES)}
                </p>
              </div>
            </div>

            <div className="flex-1 border-xl-right pr-xl-24">
              <div className="d-flex flex-column justify-content-between h-100">
                <h5
                  data-testid={genAmtId(
                    'dashboardWorkedQueue',
                    'queue-statistics',
                    ''
                  )}
                >
                  {t(I18N_DASH_BOARD.QUEUE_STATISTICS)}
                </h5>
                <div>
                  <AccountStatisticItem
                    leftItem={
                      formatCommon(collectorQueueStats?.totalAccounts).quantity
                    }
                    leftTitle={t(DASHBOARD_WORK_STATISTICS.TOTAL_ACCOUNTS)}
                    rightItem={formatCommon(
                      collectorQueueStats?.totalAmount
                    ).currency(2)}
                    rightTitle={t(DASHBOARD_WORK_STATISTICS.TOTAL_AMOUNT)}
                    isQueueStatistics
                    dataTestId="dashboardWorkedQueue_total"
                  />
                  <AccountStatisticItem
                    leftItem={
                      formatCommon(collectorQueueStats?.accountsWorked).quantity
                    }
                    leftTitle={t(DASHBOARD_WORK_STATISTICS.ACCOUNTS_WORKED)}
                    rightItem={formatCommon(
                      collectorQueueStats?.amountCollected
                    ).currency(2)}
                    rightTitle={t(DASHBOARD_WORK_STATISTICS.AMOUNT_COLLECTED)}
                    border="none"
                    isQueueStatistics
                    dataTestId="dashboardWorkedQueue_accounts"
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="w-xl-45 border-md-top border-xl-none pt-md-24 pl-xl-24 pt-xl-0 mt-xl-0 mt-md-24">
            <h5
              className="mb-16"
              data-testid={genAmtId(
                'dashboardWorkedQueue',
                'promise-statistics',
                ''
              )}
            >
              {t(I18N_DASH_BOARD.PROMISE_STATISTICS)}
            </h5>
            <PromiseStatistic
              pendingPromises={collectorPromiseStats?.pendingPromises}
              pendingAmount={collectorPromiseStats?.pendingAmount}
              keptPromises={collectorPromiseStats?.keptPromises}
              keptAmount={collectorPromiseStats?.keptAmount}
              brokenPromises={collectorPromiseStats?.brokenPromises}
              brokenAmount={collectorPromiseStats?.brokenAmount}
              totalPromises={collectorPromiseStats?.totalPromises}
              totalAmount={collectorPromiseStats?.totalAmount}
              dataTestId="dashboardWorkedQueue_promisesStatistic"
            />
          </div>
        </div>
      </div>
    </div>
  );

  return <>{renderWorkQueueCollector}</>;
};

export default DashBoardWorkQueue;
