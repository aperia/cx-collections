import React from 'react';

//mock test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen, fireEvent } from '@testing-library/react';

//components
import DashboardDropdownList from './DashboardDropdownList';

//actions
import { dashboardActions } from '../_redux/reducers';

// constants
import { DASHBOARD_PERIOD } from '../constants';

jest.mock('app/_libraries/_dls/components/DropdownList', () =>
  jest.requireActual('app/test-utils/mocks/MockDropdownList')
);

HTMLCanvasElement.prototype.getContext = jest.fn();
const mockOnBlur = jest.fn();
const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(<DashboardDropdownList onBlur={mockOnBlur} />, {
    initialState
  });
};

const mockActions = mockActionCreator(dashboardActions);

describe('DashboardDropdownList component', () => {
  const initialState: Partial<RootState> = {
    dashboard: {
      callActivity: {
        data: [],
        period: '',
        error: false,
        loading: false
      },
      goal: {
        data: [],
        error: '',
        loading: false
      },
      workQueue: {
        data: [],
        error: '',
        loading: false
      },
      workStatistic: {
        data: [],
        error: '',
        selectedPeriod: undefined,
        loading: false
      }
    }
  };
  it('should render component onChange', () => {
    const spy = mockActions('selectedPeriod');

    renderWrapper(initialState);

    fireEvent.change(screen.getByTestId('DropdownList.onChange'), {
      value: DASHBOARD_PERIOD[1]
    });

    expect(spy).not.toBeCalled();
  });

  it('should render component onBlur', () => {
    renderWrapper(initialState);

    fireEvent.blur(screen.getByTestId('DropdownList.onBlur'), {
      target: { value: 'undefined', valueObject: { id: 1, value: 'Test' } }
    });

    expect(mockOnBlur).toBeCalled();
  });
});
