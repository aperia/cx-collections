import React from 'react';

//react-testing-lib
import { render, screen } from '@testing-library/react';

//components
import PromiseStatistic from './PromiseStatistic';

describe('Dashboard PromiseStatistics component', () => {
  it('should render UI', () => {
    render(
      <PromiseStatistic
        pendingPromises="pendingPromises"
        pendingAmount="10"
        keptPromises="keptPromises"
        keptAmount="15"
        brokenPromises="brokenPromises"
        brokenAmount="20"
        totalPromises="totalPromises"
        totalAmount="25"
        isCoreView={true}
        recentPromise={true}
      />
    );

    const pendingPromisesText = screen.getByText('pendingPromises');
    const pendingAmountText = screen.getByText('$10.00');
    const keptPromisesText = screen.getByText('keptPromises');
    const keptAmountText = screen.getByText('$15.00');
    const brokenPromisesText = screen.getByText('brokenPromises');
    const brokenAmountText = screen.getByText('$20.00');
    const totalPromisesText = screen.getByText('totalPromises');
    const totalAmountText = screen.getByText('$25.00');

    expect(pendingPromisesText).toBeInTheDocument();
    expect(pendingAmountText).toBeInTheDocument();
    expect(keptAmountText).toBeInTheDocument();
    expect(keptPromisesText).toBeInTheDocument();
    expect(brokenPromisesText).toBeInTheDocument();
    expect(pendingAmountText).toBeInTheDocument();
    expect(brokenAmountText).toBeInTheDocument();
    expect(totalPromisesText).toBeInTheDocument();
    expect(totalAmountText).toBeInTheDocument();
  });
});
