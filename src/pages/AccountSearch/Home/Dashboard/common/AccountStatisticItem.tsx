import React from 'react';

import classnames from 'classnames';
//types
import { DashBoardAccountStatisticsProps } from '../types';
import { genAmtId } from 'app/_libraries/_dls/utils';

const AccountStatisticItem: React.FC<DashBoardAccountStatisticsProps> = ({
  leftItem,
  leftItemTextColor = 'blue',
  leftTitle,
  rightItem,
  rightItemTextColor = 'green',
  rightTitle,
  border = 'dashed',
  isCoreView,
  isQueueStatistics,
  dataTestId
}) => {
  const testId = genAmtId(dataTestId!, 'accountStatisticItem', '');

  return (
    <div>
      <div className="row gutters-4">
        <div
          className={classnames(
            isQueueStatistics ? 'col-xl-6 col-md-5' : 'col-md-7'
          )}
        >
          <h5
            className={classnames(
              leftItemTextColor === 'blue' && 'color-cyan-d04',
              leftItemTextColor === 'green' && 'color-green-d04',
              leftItemTextColor === 'grey' && 'color-grey',
              leftItemTextColor === 'red' && 'color-red-l08',
              isCoreView ? 'mb-4' : ''
            )}
            data-testid={genAmtId(testId!, `${leftTitle}-value`, '')}
          >
            {leftItem}
          </h5>
          <h6
            className="color-grey-l24"
            data-testid={genAmtId(testId!, `${leftTitle}-title`, '')}
          >
            {leftTitle}
          </h6>
        </div>
        <div
          className={classnames(
            isQueueStatistics ? 'col-xl-6 col-md-7' : 'col-md-5'
          )}
        >
          <h5
            className={classnames(
              rightItemTextColor === 'blue' && 'color-cyan-d04',
              rightItemTextColor === 'green' && 'color-green-d04',
              rightItemTextColor === 'grey' && 'color-grey',
              rightItemTextColor === 'red' && 'color-red-l08',
              isCoreView ? 'mb-4' : ''
            )}
            data-testid={genAmtId(testId!, `${rightTitle}-value`, '')}
          >
            {rightItem}
          </h5>
          <h6
            className="color-grey-l24"
            data-testid={genAmtId(testId!, `${rightTitle}-title`, '')}
          >
            {rightTitle}
          </h6>
        </div>
      </div>
      {border !== 'none' && (
        <div
          className={classnames(
            border === 'dashed' && 'divider-dashed',
            border === 'line' && 'border-bottom',
            isCoreView ? 'my-16' : 'my-8'
          )}
        />
      )}
    </div>
  );
};

export default AccountStatisticItem;
