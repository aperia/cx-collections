import React from 'react';

//translate
import { useTranslation } from 'app/_libraries/_dls/hooks';

//helpers
import { formatCommon } from 'app/helpers';
import classnames from 'classnames';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface PromiseStatisticProps extends DLSId {
  pendingPromises: string;
  pendingAmount: string;
  keptPromises: string;
  keptAmount: string;
  brokenPromises: string;
  brokenAmount: string;
  totalPromises: string;
  totalAmount: string;
  isCoreView?: boolean;
  recentPromise?: boolean;
}
const PromiseStatistic: React.FC<PromiseStatisticProps> = ({
  recentPromise = false,
  pendingPromises,
  pendingAmount,
  keptPromises,
  keptAmount,
  brokenPromises,
  brokenAmount,
  totalPromises,
  totalAmount,
  isCoreView,
  dataTestId
}) => {
  const { t } = useTranslation();

  const renderRecentPromises = (
    <div>
      <div className="d-flex  align-items-center mb-8">
        <div className="w-40 pr-8">
          <h6 data-testid={genAmtId(dataTestId, 'pendingPromisesTitle', '')}>
            {t('txt_pending_promises')}
          </h6>
        </div>
        <div className="w-15 pr-8">
          <p data-testid={genAmtId(dataTestId, 'pendingPromisesQuantity', '')}>
            {formatCommon(pendingPromises).quantity}
          </p>
        </div>
        <div className="w-45 border-left pl-8">
          <p data-testid={genAmtId(dataTestId, 'pendingPromisesAmount', '')}>
            {formatCommon(pendingAmount).currency(2)}
          </p>
        </div>
      </div>
      <div className="mb-8 divider-dashed" />
      <div className="d-flex  align-items-center mb-8">
        <div className="w-40 pr-8">
          <h6 data-testid={genAmtId(dataTestId, 'keptPromisesTitle', '')}>
            {t('txt_kept_promises')}
          </h6>
        </div>
        <div className="w-15 pr-8">
          <p data-testid={genAmtId(dataTestId, 'keptPromisesQuantity', '')}>
            {formatCommon(keptPromises).quantity}
          </p>
        </div>
        <div className="w-45 border-left pl-8">
          <p data-testid={genAmtId(dataTestId, 'keptPromisesAmount', '')}>
            {formatCommon(keptAmount).currency(2)}
          </p>
        </div>
      </div>
      <div className="mb-8 divider-dashed" />
      <div className="d-flex  align-items-center mb-8">
        <div className="w-40 pr-8">
          <h6 data-testid={genAmtId(dataTestId, 'brokenPromisesTitle', '')}>
            {t('txt_broken_promises')}
          </h6>
        </div>
        <div className="w-15 pr-8">
          <p data-testid={genAmtId(dataTestId, 'brokenPromisesQuantity', '')}>
            {formatCommon(brokenPromises).quantity}
          </p>
        </div>
        <div className="w-45 border-left pl-8">
          <p data-testid={genAmtId(dataTestId, 'brokenPromisesAmount', '')}>
            {formatCommon(brokenAmount).currency(2)}
          </p>
        </div>
      </div>
      <div className="mb-8 divider-dashed" />
      <div className="d-flex  align-items-center">
        <div className="w-40 pr-8">
          <h6 data-testid={genAmtId(dataTestId, 'totalPromisesTitle', '')}>
            {t('txt_total_promises')}
          </h6>
        </div>
        <div className="w-15 pr-8">
          <p data-testid={genAmtId(dataTestId, 'totalPromisesQuantity', '')}>
            {formatCommon(totalPromises).quantity}
          </p>
        </div>
        <div className="w-45 border-left pl-8">
          <p data-testid={genAmtId(dataTestId, 'totalPromisesAmount', '')}>
            {formatCommon(totalAmount).currency(2)}
          </p>
        </div>
      </div>
    </div>
  );

  const renderDefaultPromises = (
    <div>
      <div className="row gutters-4 align-items-center">
        <div className="col-xl-4 col-md-3">
          <h6 data-testid={genAmtId(dataTestId, 'pendingPromisesTitle', '')}>
            {t('txt_pending_promises')}
          </h6>
        </div>
        <div className="col-xl-3 col-md-4 pr-12">
          <p className="color-grey border-right">
            <span className="fs-16 fw-500" data-testid={genAmtId(dataTestId, 'pendingPromisesQuantity', '')}>
              {formatCommon(pendingPromises).quantity}
            </span>
          </p>
        </div>
        <div className="col-xl-5 col-md-5">
          <p className="color-grey">
            <span className="fs-16 fw-500" data-testid={genAmtId(dataTestId, 'pendingPromisesAmount', '')}>
              {formatCommon(pendingAmount).currency(2)}
            </span>
          </p>
        </div>
      </div>
      <div
        className={classnames('divider-dashed', isCoreView ? 'my-8' : 'my-4')}
      />
      <div className="row gutters-4 align-items-center">
        <div className="col-xl-4 col-md-3">
          <h6 data-testid={genAmtId(dataTestId, 'keptPromisesTitle', '')}>{t('txt_kept_promises')}</h6>
        </div>
        <div className="col-xl-3 col-md-4 pr-12">
          <p className="color-grey border-right">
            <span className="fs-16 fw-500" data-testid={genAmtId(dataTestId, 'keptPromisesQuantity', '')}>
              {formatCommon(keptPromises).quantity}
            </span>
          </p>
        </div>
        <div className="col-xl-5 col-md-5">
          <p className="color-grey">
            <span className="fs-16 fw-500" data-testid={genAmtId(dataTestId, 'keptPromisesAmount', '')}>
              {formatCommon(keptAmount).currency(2)}
            </span>
          </p>
        </div>
      </div>
      <div
        className={classnames('divider-dashed', isCoreView ? 'my-8' : 'my-4')}
      />
      <div className="row gutters-4 align-items-center">
        <div className="col-xl-4 col-md-3">
          <h6 data-testid={genAmtId(dataTestId, 'brokenPromisesTitle', '')}>{t('txt_broken_promises')}</h6>
        </div>
        <div className="col-xl-3 col-md-4 pr-12">
          <p className="color-grey border-right">
            <span className="fs-16 fw-500" data-testid={genAmtId(dataTestId, 'brokenPromisesQuantity', '')}>
              {formatCommon(brokenPromises).quantity}
            </span>
          </p>
        </div>
        <div className="col-xl-5 col-md-5">
          <p className="color-grey">
            <span className="fs-16 fw-500" data-testid={genAmtId(dataTestId, 'brokenPromisesAmount', '')}>
              {formatCommon(brokenAmount).currency(2)}
            </span>
          </p>
        </div>
      </div>
      <div
        className={classnames('divider-dashed', isCoreView ? 'my-8' : 'my-4')}
      />
      <div className="row gutters-4 align-items-center">
        <div className="col-xl-4 col-md-3">
          <h6 data-testid={genAmtId(dataTestId, 'totalPromisesTitle', '')}>{t('txt_total_promises')}</h6>
        </div>
        <div className="col-xl-3 col-md-4 pr-12">
          <p className="color-grey border-right">
            <span className="fs-16 fw-500" data-testid={genAmtId(dataTestId, 'totalPromisesQuantity', '')}>
              {formatCommon(totalPromises).quantity}
            </span>
          </p>
        </div>
        <div className="col-xl-5 col-md-5">
          <p className="color-grey">
            <span className="fs-16 fw-500" data-testid={genAmtId(dataTestId, 'totalPromisesAmount', '')}>
              {formatCommon(totalAmount).currency(2)}
            </span>
          </p>
        </div>
      </div>
    </div>
  );

  return <>{recentPromise ? renderRecentPromises : renderDefaultPromises}</>;
};

export default PromiseStatistic;
