import React from 'react';
import { render, screen } from '@testing-library/react';
import AccountStatisticItem from './AccountStatisticItem'

const defaultMockData: any = {
    leftItem: "aaa",
    leftTitle: "bbb",
    rightItem: "ccc",
    rightTitle: "ddd",
    isCoreView: true,
    isQueueStatistics: true,
    dataTestId: "test-common"
}
describe('Test common AccountStatisticItem Component', () => {
    it('should render AccountStatisticItem with isCoreView & isQueueStatistics', () => {
        render(<AccountStatisticItem
            {...defaultMockData}
        />)
        expect(screen.getByTestId('test-common_accountStatisticItem_bbb-value')).toBeInTheDocument()
        expect(screen.getByTestId('test-common_accountStatisticItem_bbb-title')).toBeInTheDocument()
    })

    it('should render AccountStatisticItem with !isCoreView & !isQueueStatistics', () => {
        const dataCase2 = { ...defaultMockData, isCoreView: false, isQueueStatistics: false }
        render(<AccountStatisticItem
            {...dataCase2}
        />)
        expect(screen.getByTestId('test-common_accountStatisticItem_bbb-value')).toBeInTheDocument()
        expect(screen.getByTestId('test-common_accountStatisticItem_bbb-title')).toBeInTheDocument()
    })

    it('should render AccountStatisticItem with text color case 1', () => {
        const dataCase3 = {
            ...defaultMockData,
            isCoreView: false,
            isQueueStatistics: false,
            leftItemTextColor: 'green',
            rightItemTextColor: 'blue',
            border: 'line'
        }

        render(<AccountStatisticItem
            {...dataCase3}
        />)
        expect(screen.getByTestId('test-common_accountStatisticItem_bbb-value')).toBeInTheDocument()
        expect(screen.getByTestId('test-common_accountStatisticItem_bbb-title')).toBeInTheDocument()
    })

    it('should render AccountStatisticItem with text color case 2', () => {
        const dataCase5 = {
            ...defaultMockData,
            isCoreView: false,
            isQueueStatistics: false,
            leftItemTextColor: 'grey',
            rightItemTextColor: 'grey',
            border: 'line'
        }

        render(<AccountStatisticItem
            {...dataCase5}
        />)
        expect(screen.getByTestId('test-common_accountStatisticItem_bbb-value')).toBeInTheDocument()
        expect(screen.getByTestId('test-common_accountStatisticItem_bbb-title')).toBeInTheDocument()
    })

    it('should render AccountStatisticItem with text color case 3', () => {
        const dataCase6 = {
            ...defaultMockData,
            isCoreView: false,
            isQueueStatistics: false,
            leftItemTextColor: 'red',
            rightItemTextColor: 'red',
            border: 'line'
        }

        render(<AccountStatisticItem
            {...dataCase6}
        />)
        expect(screen.getByTestId('test-common_accountStatisticItem_bbb-value')).toBeInTheDocument()
        expect(screen.getByTestId('test-common_accountStatisticItem_bbb-title')).toBeInTheDocument()
    })
})
