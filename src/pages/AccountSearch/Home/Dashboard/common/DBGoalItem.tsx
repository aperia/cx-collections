import React from 'react';

//types
import { DashBoardGoalProps, DBGoal } from '../types';

//constants
import { DASHBOARD_GOAL_ITEM, I18N_DASH_BOARD } from '../constants';

//helpers
import { formatCommon } from 'app/helpers';

//components
import DoughnutChart, {
  DoughnutChartData
} from 'app/_libraries/_dls/components/DoughnutChart';

//translate
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

const DBGoalItem: React.FC<DashBoardGoalProps> = ({
  accountTitle,
  amountTitle,
  items,
  goalType
}) => {
  const { t } = useTranslation();
  const ACCOUNTS_WORKED_TEXT = t(DASHBOARD_GOAL_ITEM.ACCOUNTS_WORKED);
  const AMOUNT_COLLECTED_TEXT = t(DASHBOARD_GOAL_ITEM.AMOUNT_COLLECTED);

  const dynamicProps: Record<string, DBGoal> = {
    ['daily']: {
      accountValue: items?.accountPerDay,
      amountValue: items?.amountPerDay,
      accountWorked: items?.accountsWorkedByDay,
      amountCollected: items?.amountCollectedByDay
    },
    ['monthly']: {
      accountValue: items?.accountPerMonth,
      amountValue: items?.amountPerMonth,
      accountWorked: items?.accountsWorkedByMonth,
      amountCollected: items?.amountCollectedByMonth
    }
  };

  const accountData = {
    labels: [accountTitle, ACCOUNTS_WORKED_TEXT],
    datasets: [
      {
        label: `${t(I18N_DASH_BOARD.ACCOUNT)}`,
        data: [
          Number(dynamicProps[goalType]?.accountValue),
          Number(dynamicProps[goalType]?.accountWorked)
        ],
        backgroundColor: ['#D7D7DE', '#5CBAE6']
      }
    ],
    center: {
      label: `${t(I18N_DASH_BOARD.TOTAL)}`,
      value: '0.00'
    }
  } as DoughnutChartData;

  const amountData = {
    labels: [amountTitle, AMOUNT_COLLECTED_TEXT],
    datasets: [
      {
        label: `${t(I18N_DASH_BOARD.AMOUNT)}`,
        data: [
          Number(dynamicProps[goalType]?.amountValue),
          Number(dynamicProps[goalType]?.amountCollected)
        ],
        backgroundColor: ['#D7D7DE', '#77D97C']
      }
    ],
    center: {
      label: `${t(I18N_DASH_BOARD.TOTAL)}`,
      value: '0.00'
    }
  } as DoughnutChartData;

  return (
    <>
      <DoughnutChart
        chartData={accountData}
        size="small"
        legendValueRender={value => {
          return isNaN(value) ? '0' : formatCommon(value).quantity;
        }}
        legendLabelRender={label => {
          return `${label}`;
        }}
        dataTestId={genAmtId(accountTitle!, 'quantity-chart', '')}
      />

      <div className="divider-dashed my-16" />

      <DoughnutChart
        chartData={amountData}
        size="small"
        legendValueRender={value => {
          return isNaN(value) ? '0' : formatCommon(value).currency(2);
        }}
        legendLabelRender={label => {
          return `${label}`;
        }}
        dataTestId={genAmtId(amountTitle!, 'amount-chart', '')}
      />
    </>
  );
};

export default DBGoalItem;
