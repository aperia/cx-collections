import React from 'react';

//react-testing-lib
import { render, screen } from '@testing-library/react';

//components
import DBGoalItem from './DBGoalItem';

jest.mock('app/_libraries/_dls/components/DoughnutChart', () =>
  jest.requireActual('app/test-utils/mocks/MockDoughnutChart')
);

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('DBGoalItems', () => {
  it('should render UI', () => {
    render(
      <DBGoalItem
        accountTitle="accountTitle"
        amountTitle="amountTitle"
        goalType="daily"
        items={{
          accountPerDay: 'accountPerDay',
          amountPerDay: 'amountPerDay',
          accountsWorkedByDay: 'accountsWorkedByDay',
          amountCollectedByDay: 'amountCollectedByDay'
        }}
      />
    );

    const chartText = screen.getAllByTestId('doughnut-chart');

    expect(chartText).toHaveLength(2);
  });
});
