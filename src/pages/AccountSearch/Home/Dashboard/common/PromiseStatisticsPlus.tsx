import React from 'react';
//types
import { PromiseStatisticsPlusProps } from '../../types';

//helpers
import { numberToPercentString } from '../helpers';

//constants
import { I18N_DASH_BOARD } from '../constants';

//translate
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

const PromiseStatisticsPlus: React.FC<PromiseStatisticsPlusProps> = ({
  promisesPerHour,
  keptRate
}) => {
  const { t } = useTranslation();
  const testId = 'promiseStatisticsPlus';

  return (
    <div>
      <div className="divider-dashed mb-8 mt-6" />
      <div className="row gutters-4 d-flex justify-content-between align-items-center">
        <div className="col-md-7">
          <p
            className="color-grey"
            data-testid={genAmtId(testId!, 'promisesPerHour', '')}
          >
            {t(I18N_DASH_BOARD.PROMISES_TO_PAY_PER_HOUR)}:{' '}
            <span className="fs-16 fw-500">{'0'}</span>
          </p>
        </div>

        <div className="col-md-5">
          <p
            className="color-grey"
            data-testid={genAmtId(testId!, 'keptRate', '')}
          >
            {t(I18N_DASH_BOARD.KEPT_RATE)}:{' '}
            <span
              className="fs-16 fw-500"
              data-testid={genAmtId(testId!, 'keptRate-value', '')}
            >
              {numberToPercentString(0)}
            </span>
          </p>
        </div>
      </div>
    </div>
  );
};

export default PromiseStatisticsPlus;
