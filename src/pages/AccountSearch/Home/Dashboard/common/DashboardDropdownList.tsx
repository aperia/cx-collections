import React from 'react';

//components
import {
  DropdownList,
  DropdownBaseChangeEvent
} from 'app/_libraries/_dls/components';

//constants
import { DASHBOARD_PERIOD } from '../constants';

import { isFunction } from 'app/_libraries/_dls/lodash';
import { useTranslation } from 'app/_libraries/_dls/hooks';

interface DashboardDropdownProps {
  onBlur: (event: any) => void;
  dataTestId?: string;
}
const DashboardDropdownList: React.FC<DashboardDropdownProps> = ({
  onBlur,
  dataTestId
}) => {
  const { t } = useTranslation();
  const [initialValue, setInitialValue] = React.useState(DASHBOARD_PERIOD[0]);
  const handleBlur = (event: any) => {
    onBlur && isFunction(onBlur(event));
  };

  const handleChange = (event: DropdownBaseChangeEvent) => {
    setInitialValue(event.value);
  };

  return (
    <DropdownList
      dataTestId={dataTestId}
      name="periodDashBoard"
      variant="no-border"
      onBlur={handleBlur}
      value={initialValue}
      onChange={handleChange}
      className="text-capitalize"
      textFieldRender={value => t(value?.description)}
      noResult={t('txt_no_results_found')}
    >
      {DASHBOARD_PERIOD.map(item => (
        <DropdownList.Item
          key={item.value}
          label={t(item.description)}
          value={item}
          className="text-capitalize"
        />
      ))}
    </DropdownList>
  );
};

export default DashboardDropdownList;
