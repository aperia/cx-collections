import React from 'react';

//mock test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { fireEvent, screen } from '@testing-library/react';

//components
import DashboardWorkStatistics from '.';

//actions
import { dashboardActions } from '../_redux/reducers';
import userEvent from '@testing-library/user-event';
import { USER_ROLE } from '../constants';

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockActions = mockActionCreator(dashboardActions);

jest.mock('app/_libraries/_dls/components/DropdownList', () =>
  jest.requireActual('app/test-utils/mocks/MockDropdownList')
);

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <DashboardWorkStatistics userType={USER_ROLE.CXCOL5} title="test title" />,
    { initialState }
  );
};

const renderAdminWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <DashboardWorkStatistics userType={USER_ROLE.CXCOL1} title="test title" />,
    { initialState }
  );
};

describe('Dashboard WorkStatistics', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isAdminRole: true,
      isCollector: false
    }
  };
  const data: Partial<RootState> = {
    dashboard: {
      callActivity: {
        data: [],
        period: '',
        error: false,
        loading: false
      },
      goal: {
        data: [],
        error: '',
        loading: false
      },
      workQueue: {
        data: {},
        error: '',
        loading: false
      },
      workStatistic: {
        data: {
          accountStatistics: [],
          promiseStatistics: {
            pendingPromises: '5',
            pendingAmount: '20000.00',
            keptPromises: '10',
            keptAmount: '40000.00',
            brokenPromises: '15',
            brokenAmount: '60000.00',
            totalPromises: '30',
            totalAmount: '120000.00'
          }
        },
        error: '',
        selectedPeriod: '',
        loading: false
      }
    }
  };
  it('should render UI with core plus view', () => {
    renderWrapper(data);

    const queryTextTitle = screen.getByText('test title');

    expect(queryTextTitle).toBeInTheDocument();
  });

  it('should render UI with core plus view > case2', () => {
    renderWrapper({
      dashboard: {
        callActivity: {
          data: [],
          period: '',
          error: false,
          loading: false
        },
        goal: {
          data: [],
          error: '',
          loading: false
        },
        workQueue: {
          data: {},
          error: '',
          loading: false
        },
        workStatistic: {
          data: undefined,
          error: '',
          selectedPeriod: '',
          loading: false
        }
      } as any
    });

    const queryTextTitle = screen.getByText('test title');

    expect(queryTextTitle).toBeInTheDocument();
  });

  it('should render UI with core view', () => {
    renderAdminWrapper(data);

    const queryTextTitle = screen.getByText('test title');

    expect(queryTextTitle).toBeInTheDocument();
  });

  it('should render component onBlur', () => {
    const mockBlur = mockActions('selectedPeriod');

    renderWrapper(data);

    fireEvent.blur(screen.getByTestId('DropdownList.onBlur'), {
      target: { value: undefined, valueObject: { id: 1, value: 'Test' } }
    });

    expect(mockBlur).toBeCalledWith(undefined);
  });
});

describe('should render with error msg', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isAdminRole: false,
      isCollector: true
    }
  };
  const data: Partial<RootState> = {
    dashboard: {
      callActivity: {
        data: [],
        period: '',
        error: false,
        loading: false
      },
      goal: {
        data: [],
        error: '',
        loading: false
      },
      workQueue: {
        data: [],
        error: 'error_msg',
        loading: false
      },
      workStatistic: {
        data: {
          accountStatistics: [],
          promiseStatistics: {
            pendingPromises: '5',
            pendingAmount: '20000.00',
            keptPromises: '10',
            keptAmount: '40000.00',
            brokenPromises: '15',
            brokenAmount: '60000.00',
            totalPromises: '30',
            totalAmount: '120000.00'
          }
        },
        error: 'error_msg',
        selectedPeriod: '',
        loading: false
      }
    }
  };

  it('should render UI with error message', () => {
    renderWrapper(data);

    const queryTextReload = screen.getByText(
      'txt_data_load_unsuccessful_click_reload_to_try_again'
    );

    expect(queryTextReload).toBeInTheDocument();
  });

  it('should render UI with error message > case 2', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: undefined
    };
    renderWrapper(data);

    const queryTextReload = screen.getByText(
      'txt_data_load_unsuccessful_click_reload_to_try_again'
    );

    expect(queryTextReload).toBeInTheDocument();
  });

  it('should render UI with error message and click reload button', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app',
        isAdminRole: false,
        isCollector: false
      }
    };
    const mockReload = mockActions('getDashboardStatistics');

    renderWrapper(data);

    const reloadButton = screen.getByRole('button', {
      name: 'txt_reload'
    });

    userEvent.click(reloadButton);

    expect(mockReload).toBeCalledTimes(2);
  });
});
