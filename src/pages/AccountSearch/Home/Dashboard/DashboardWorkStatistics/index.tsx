import React from 'react';

//components
import DashboardDropdownList from '../common/DashboardDropdownList';
import AccountStatisticItem from '../common/AccountStatisticItem';
import { FailedApiReload } from 'app/components';

//constants
import { I18N_DASH_BOARD, DASHBOARD_WORK_STATISTICS } from '../constants';

//helpers
import { formatCommon } from 'app/helpers';

//types
import { DashBoardProps } from '../types';

//translate
import { useTranslation } from 'app/_libraries/_dls/hooks';

//redux
import { useDispatch, useSelector } from 'react-redux';

//selectors
import {
  selectedDashboardWorkStatisticPeriod,
  selectedDashboardWorkStatistic,
  selectedDashboardWorkStatisticError,
  selectedDashboardWorkStatisticLoading
} from '../_redux/selectors';

//actions
import { dashboardActions as actions } from '../_redux/reducers';

//libs
import classnames from 'classnames';

//components
import PromiseStatisticsPlus from '../common/PromiseStatisticsPlus';

//helpers
import { numberToPercentString } from '../helpers';
import PromiseStatistic from '../common/PromiseStatistic';
import { genAmtId } from 'app/_libraries/_dls/utils';

const DashboardWorkStatistics: React.FC<DashBoardProps> = ({ title }) => {
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const { isAdminRole, isCollector } = window.appConfig.commonConfig || {};
  const isCoreView = isAdminRole || isCollector;

  const period: RefDataValue = useSelector(
    selectedDashboardWorkStatisticPeriod
  );

  const { accountStats, promiseStats } =
    useSelector(selectedDashboardWorkStatistic) || {};

  const isLoading = useSelector(selectedDashboardWorkStatisticLoading);

  const isError = useSelector(selectedDashboardWorkStatisticError);

  const handleReload = () =>
    dispatch(actions.getDashboardStatistics({ period: period.value }));

  const handleBlur = React.useCallback(
    event => {
      dispatch(actions.selectedPeriod(event.value));
    },
    [dispatch]
  );

  React.useEffect(() => {
    dispatch(actions.getDashboardStatistics({ period: period.value }));
  }, [dispatch, period]);

  const renderAccountStatistics = (
    <>
      <h5
        className={classnames(isCoreView ? 'mb-16' : 'mb-10')}
        data-testid={genAmtId(
          'dashboardWorkStatistics',
          'account-statistics',
          ''
        )}
      >
        {t(I18N_DASH_BOARD.ACCOUNT_STATISTICS)}
      </h5>
      <AccountStatisticItem
        leftItem={formatCommon(accountStats?.totalAccounts).quantity}
        leftTitle={t(DASHBOARD_WORK_STATISTICS.TOTAL_ACCOUNTS)}
        rightItem={formatCommon(accountStats?.totalAmount).currency(2)}
        rightTitle={t(DASHBOARD_WORK_STATISTICS.TOTAL_AMOUNT)}
        isCoreView={isCoreView}
        dataTestId="dashboardWorkStatistics_total"
      />
      <AccountStatisticItem
        leftItem={formatCommon(accountStats?.accountsWorked).quantity}
        leftTitle={t(DASHBOARD_WORK_STATISTICS.ACCOUNTS_WORKED)}
        rightItem={formatCommon(accountStats?.amountCollected).currency(2)}
        rightTitle={t(DASHBOARD_WORK_STATISTICS.AMOUNT_COLLECTED)}
        isCoreView={isCoreView}
        dataTestId="dashboardWorkStatistics_account"
      />
      {(isAdminRole || isCollector) && (
        //TODO: hardcore data & waiting for API to get Core plus view in Phase 2.
        <AccountStatisticItem
          leftItem={
            formatCommon(accountStats?.accountWorkedPerHour).quantity || '0'
          }
          leftTitle={t(I18N_DASH_BOARD.ACCOUNT_WORKED_PER_HOUR)}
          rightItem={numberToPercentString(accountStats?.contactRate || 0)}
          rightTitle={t(I18N_DASH_BOARD.CONTACT_RATE)}
          rightItemTextColor="grey"
          isCoreView={isCoreView}
          dataTestId="dashboardWorkStatistics_rate"
        />
      )}
      <AccountStatisticItem
        leftItem={formatCommon(accountStats?.nextWorkAccount).quantity}
        leftTitle={t(DASHBOARD_WORK_STATISTICS.NEXT_WORK_ACCOUNT)}
        leftItemTextColor="grey"
        rightItem={formatCommon(accountStats?.dueAccount).quantity}
        rightTitle={t(DASHBOARD_WORK_STATISTICS.DUE_ACCOUNT)}
        rightItemTextColor="red"
        border="line"
        isCoreView={isCoreView}
        dataTestId="dashboardWorkStatistics_nextWork"
      />
    </>
  );

  const renderPromiseStatistics = (
    <>
      <h5
        className={classnames('mt-8', isCoreView ? 'mb-16' : 'mb-10')}
        data-testid={genAmtId(
          'dashboardWorkStatistics',
          'promise-statistics',
          ''
        )}
      >
        {t(I18N_DASH_BOARD.PROMISE_STATISTICS)}
      </h5>

      <PromiseStatistic
        pendingPromises={promiseStats?.pendingPromises}
        pendingAmount={promiseStats?.pendingAmount}
        keptPromises={promiseStats?.keptPromises}
        keptAmount={promiseStats?.keptAmount}
        brokenPromises={promiseStats?.brokenPromises}
        brokenAmount={promiseStats?.brokenAmount}
        totalPromises={promiseStats?.totalPromises}
        totalAmount={promiseStats?.totalAmount}
        isCoreView={isCoreView}
        dataTestId="dashboardWorkStatistics_promisesStatistic"
      />

      {!isCoreView && (
        <PromiseStatisticsPlus
          //TODO: hardcore data & waiting for API to get Core plus view in Phase 2.
          promisesPerHour={accountStats?.promisesToPayPerHour || '20'}
          keptRate={accountStats?.keptRate || '50'}
        />
      )}
    </>
  );

  if (isError)
    return (
      <div>
        <FailedApiReload
          id="dashboard-work-statistics-failed"
          dataTestId="dashboard-work-statistics-failed"
          className="mt-80"
          onReload={handleReload}
        />
      </div>
    );

  return (
    <div className={classnames({ loading: isLoading })}>
      <div className="mb-16 d-flex justify-content-between">
        <h4
          className="title"
          data-testid={genAmtId('dashboardWorkStatistics', 'title', '')}
        >
          {title}
        </h4>
        <div className="mr-n8">
          <span
            className="color-grey fw-500 mr-4"
            data-testid={genAmtId('dashboardWorkStatistics', 'period', '')}
          >
            {t(I18N_DASH_BOARD.PERIOD)}:
          </span>
          <DashboardDropdownList
            onBlur={handleBlur}
            dataTestId="dashboardWorkStatistics-dropdown"
          />
        </div>
      </div>
      <div className="card card-dashboard">
        {renderAccountStatistics}
        {renderPromiseStatistics}
      </div>
    </div>
  );
};

export default DashboardWorkStatistics;
