import { apiService } from 'app/utils/api.service';
import {
  DashboardRequest,
  GetAccountLockRequest,
  GetNextAccountRequest,
  UpdateCollectionAccountRequest
} from './types';

export const dashboardServices = {
  getDashboardCallActivity() {
    const url =
      window.appConfig?.api?.dashboard?.getDashboardCallActivity || '';
    return apiService.get(url);
  },
  getDashboardGoal() {
    const url = window.appConfig?.api?.dashboard?.getDashboardGoal || '';
    return apiService.get(url);
  },
  getDashboardCollectorWorkStatistics(request: DashboardRequest) {
    const url =
      window.appConfig?.api?.dashboard?.getDashboardCollectorWorkStatistics ||
      '';
    return apiService.post(url, request);
  },
  getDashboardSupervisorWorkStatistics() {
    const url =
      window.appConfig?.api?.dashboard?.getDashboardSupervisorWorkStatistics ||
      '';
    return apiService.get(url);
  },
  getDashboardWorkQueue(requestBody: DashboardRequest) {
    const url = window.appConfig?.api?.dashboard?.getDashboardWorkQueue || '';
    return apiService.post(url, requestBody);
  },
  getDashboardCollector() {
    const url =
      window.appConfig?.api?.dashboard?.getDashboardWorkCollector || '';
    return apiService.get(url);
  },
  getRecentlyWorkQueue(requestBody: DashboardRequest) {
    const url = window.appConfig?.api?.dashboard?.getRecentlyWorkQueue || '';
    return apiService.post(url, requestBody);
  },
  getNextAccountWorkOnQueue(bodyRequest: GetNextAccountRequest) {
    const url = window.appConfig.api?.dashboard.getNextAccountWorkOnQueue || '';
    return apiService.post(url, bodyRequest);
  },
  getAccountExtendLockTime(bodyRequest: GetAccountLockRequest) {
    const url = window.appConfig.api?.dashboard.getAccountExtendLockTime || '';
    return apiService.post(url, bodyRequest);
  },
  getAccountLockStatusInquiry(bodyRequest: GetAccountLockRequest) {
    const url =
      window.appConfig.api?.dashboard.getAccountLockStatusInquiry || '';
    return apiService.post(url, bodyRequest);
  },
  getAccountLockTimeUpdate(bodyRequest: GetAccountLockRequest) {
    const url = window.appConfig.api?.dashboard.getAccountLockTimeUpdate || '';
    return apiService.post(url, bodyRequest);
  },
  updateCollectionAccount(bodyRequest: UpdateCollectionAccountRequest) {
    const url = window.appConfig.api?.dashboard.updateCollectionAccount || '';
    return apiService.post(url, bodyRequest);
  }
};
