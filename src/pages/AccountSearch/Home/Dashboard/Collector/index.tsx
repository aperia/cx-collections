import React from 'react';

//libs
import classnames from 'classnames';

//redux
import { useDispatch, useSelector } from 'react-redux';

//types
import { CollectorDetail, DashBoardProps } from '../types';

//actions
import { dashboardActions as actions } from '../_redux/reducers';
import { tabActions } from 'pages/__commons/TabBar/_redux';

//constants
import { I18N_DASH_BOARD, DASHBOARD_COLLECTOR } from '../constants';
import { QUEUE_LIST } from 'pages/AccountDetails/NextActions/constants';
import { EMPTY_STRING } from 'app/constants';

//selectors
import {
  selectedDashboardCollector,
  selectedDashboardCollectorPeriod,
  selectedDashboardCollectorLoading,
  selectedDashboardCollectorError
} from '../_redux/selectors';

//translate
import { useTranslation } from 'app/_libraries/_dls/hooks';

//components
import { Button } from 'app/_libraries/_dls/components';
import { FailedApiReload } from 'app/components';
import DashboardDropdownList from '../common/DashboardDropdownList';
import CoreView from './CoreView';
import CorePlusView from './CorePlusView';
import { genAmtId } from 'app/_libraries/_dls/utils';

const DashBoardCollector: React.FC<DashBoardProps> = ({ title }) => {
  const dispatch = useDispatch();
  const testId = 'dashboard_collector';

  const dataCollector = useSelector(selectedDashboardCollector);

  const { isAdminRole, isCollector } = window.appConfig.commonConfig || {};
  const isCoreView = isAdminRole || isCollector;

  const period: RefDataValue = useSelector(selectedDashboardCollectorPeriod);

  const { t } = useTranslation();

  const isLoading = useSelector(selectedDashboardCollectorLoading);

  const isError = useSelector(selectedDashboardCollectorError);

  const handleReload = () =>
    dispatch(actions.getDashboardCollector({ period: period.value }));

  const handleBlur = React.useCallback(
    event => {
      dispatch(actions.selectedCollectorPeriod(event.value));
    },
    [dispatch]
  );

  const handleOpenViewAllCollector = () => {
    const tabTitle = DASHBOARD_COLLECTOR.COLLECTOR_LIST;
    dispatch(
      tabActions.addTab({
        title: tabTitle,
        storeId: 'collectorList',
        accEValue: 'collectorValue',
        tabType: 'collectorList',
        iconName: 'file'
      })
    );
  };

  const handleOpenWorkQueue = (dataItem?: CollectorDetail) => {
    const collectorId = dataItem?.collectorId || EMPTY_STRING;

    if (!collectorId) return;

    const tabTitle = QUEUE_LIST;
    const eValue = `${collectorId}`;

    dispatch(
      tabActions.addTab({
        title: tabTitle,
        storeId: 'workQueueCollector',
        accEValue: eValue,
        tabType: 'workQueueCore',
        iconName: 'file',
        props: {
          collectorId,
          selectedOwner: dataItem?.owner
        }
      })
    );
  };

  React.useEffect(() => {
    dispatch(actions.getDashboardCollector({ period: period.value }));
  }, [dispatch, period]);

  if (isError) {
    return (
      <div>
        <FailedApiReload
          id="dashboard-work-queue-failed"
          className="mt-80"
          onReload={handleReload}
          dataTestId={`${testId}_failed-api`}
        />
      </div>
    );
  }

  return (
    <div className={classnames({ loading: isLoading })}>
      <div className="d-flex align-items-center justify-content-between mb-16">
        <h4 data-testid={genAmtId(testId, 'title', '')}>{title}</h4>
        <div className="mr-n8">
          <Button
            onClick={handleOpenViewAllCollector}
            size="sm"
            variant="outline-primary"
            dataTestId={`${testId}_viewAllCollectors-btn`}
          >
            {t(I18N_DASH_BOARD.VIEW_ALL_COLLECTORS)}
          </Button>
        </div>
      </div>
      <div className="d-flex align-items-center justify-content-between">
        <p data-testid={genAmtId(testId, 'highPerformingTitle', '')}>
          {t(DASHBOARD_COLLECTOR.HIGH_PERFORMING)}
        </p>
        <div className="mr-n8">
          <span
            className="color-grey fw-500 mr-4"
            data-testid={genAmtId(testId, 'periodTitle', '')}
          >
            {t(I18N_DASH_BOARD.PERIOD)}
          </span>
          <DashboardDropdownList
            onBlur={handleBlur}
            dataTestId={`${testId}_dropdown`}
          />
        </div>
      </div>
      {isCoreView ? (
        <CoreView
          data={dataCollector}
          title={title}
          handleOpenWorkQueue={handleOpenWorkQueue}
        />
      ) : (
        <CorePlusView
          data={dataCollector}
          title={title}
          handleOpenWorkQueue={handleOpenWorkQueue}
        />
      )}
    </div>
  );
};

export default DashBoardCollector;
