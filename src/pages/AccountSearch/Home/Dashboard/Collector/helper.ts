import { DASHBOARD_COLLECTOR } from "../constants";
import { COLOR_VALUE } from "./contants";

export const getColorValue = (average: number, value: number): string => {
  if (value >= average) return 'green';

  const value20 = average - (average * COLOR_VALUE.MEDIUM) / 100;

  if (value >= value20) return 'orange';

  return 'red';
};

export const formatTime = (value?: number, unit?: string, childUnit?: string): string => {  
  if (value !== 0 && !value) return '';
  if (value === 0) return `0${unit}0${childUnit}`;
  if (value < 60) return `0${unit}${value}${childUnit}`;

  const parentValue = Math.floor(value / 60);
  const childValue = value % 60;

  return childValue > 0
    ? `${parentValue}${unit}${childValue}${childUnit}`
    : `${parentValue}${unit}`;
};

export const getPromisesTitle = (t: any, value?: string): string => {
  switch (value) {
    case 'pending':
      return t(DASHBOARD_COLLECTOR.PENDING_PROMISES);
    case 'kept':
      return t(DASHBOARD_COLLECTOR.KEPT_PROMISES);
    case 'broken':
      return t(DASHBOARD_COLLECTOR.BROKEN_PROMISES);
    case 'total':
      return t(DASHBOARD_COLLECTOR.TOTAL_PROMISES);
    default:
      return '';
  }
};