import { formatTime, getColorValue } from './helper';
import { mockUseTranslate } from 'app/test-utils/mockUtils';

mockUseTranslate()

describe('formatTime', () => {
  it('case < 60', () => {
    const result = formatTime(1, 'unit', 'childUnit')
    expect(result).toEqual('0unit1childUnit')
  })
  
  it('case = 60', () => {
    const result = formatTime(60, 'unit', 'childUnit')
    expect(result).toEqual('1unit')
  })

  it('case > 60', () => {
    const result = formatTime(63, 'unit', 'childUnit')
    expect(result).toEqual('1unit3childUnit')
  })

  it('case = 0', () => {
    const result = formatTime(0, 'unit', 'childUnit')
    expect(result).toEqual('0unit0childUnit')
  })

  it('case empty', () => {
    const result = formatTime()
    expect(result).toEqual('')
  })
})

describe('getColorValue', () => {
  it('case value > average', () => {
    const result = getColorValue(1, 2)
    expect(result).toEqual('green')
  })

  it('case value > value20', () => {
    const result = getColorValue(6, 5)
    expect(result).toEqual('orange')
  })

  it('case default', () => {
    const result = getColorValue(5, 2)
    expect(result).toEqual('red')
  })
})