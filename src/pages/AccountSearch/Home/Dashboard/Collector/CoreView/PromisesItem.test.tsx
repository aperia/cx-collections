import React from 'react';
import { renderMockStoreId } from 'app/test-utils';
import { screen } from '@testing-library/react';
import PromisesItem from './PromisesItem';

const renderWrapper = () => {
  return renderMockStoreId(
    <PromisesItem
      title="title"
      value={20}
      amount={20}
      isRenderDivider
    />
  );
};

describe('PromisesItem',() => {
  it('Render component', () => {
    renderWrapper()
    expect(screen.getByText(/title/)).toBeInTheDocument()
  })
})