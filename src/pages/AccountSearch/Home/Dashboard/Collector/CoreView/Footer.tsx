import React from 'react';

//translate
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { DASHBOARD_COLLECTOR } from '../../constants';

// helper
import { formatTime } from '../helper';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface FooterProps {
  breakTotal?: number;
  breakTime?: number;
  inbound?: number;
  outbound?: number;
  averageTime?: number;
  dataTestId?: string;
}

const Footer: React.FC<FooterProps> = ({
  breakTotal,
  breakTime,
  inbound,
  outbound,
  averageTime,
  dataTestId
}) => {
  const { t } = useTranslation();

  return (
    <>
      <div className="d-flex align-items-center">
        <h6 className="mr-16" data-testid={genAmtId(dataTestId, 'breakTitle', '')}>
          {t(DASHBOARD_COLLECTOR.BREAK)}
        </h6>
        <div className="d-flex align-items-center mr-16">
          <p className="mr-4 color-grey" data-testid={genAmtId(dataTestId, 'totalBreakTitle', '')}>
            {t(DASHBOARD_COLLECTOR.TOTAL_BREAKS)}:
          </p>
          <p data-testid={genAmtId(dataTestId, 'breakTotal', '')}>{breakTotal || 0}</p>
        </div>
        <div className="d-flex align-items-center">
          <p className="mr-4 color-grey" data-testid={genAmtId(dataTestId, 'totalBreakTimeTitle', '')}>
            {t(DASHBOARD_COLLECTOR.TOTAL_BREAK_TIME)}:
          </p>
          <p data-testid={genAmtId(dataTestId, 'totalBreakTime', '')}>{formatTime(breakTime || 0, 'h', 'm')}</p>
        </div>
      </div>
      <div className="divider-dashed my-8"></div>
      <div className="d-flex align-items-center">
        <h6 className="mr-16" data-testid={genAmtId(dataTestId, 'callInfoTitle', '')}>
          {t(DASHBOARD_COLLECTOR.CALL_INFORMATION)}
        </h6>
        <div className="d-flex align-items-center mr-24">
          <p className="mr-4 color-grey" data-testid={genAmtId(dataTestId, 'inboundTitle', '')}>
            {t(DASHBOARD_COLLECTOR.INBOUND)}:
          </p>
          <p data-testid={genAmtId(dataTestId, 'inbound', '')}>{inbound || 0}</p>
        </div>
        <div className="d-flex align-items-center mr-24">
          <p className="mr-4 color-grey" data-testid={genAmtId(dataTestId, 'outboundTitle', '')}>
            {t(DASHBOARD_COLLECTOR.OUTBOUND)}:
            </p>
          <p data-testid={genAmtId(dataTestId, 'outbound', '')}>{outbound || 0}</p>
        </div>
        <div className="d-flex align-items-center">
          <p className="mr-4 color-grey" data-testid={genAmtId(dataTestId, 'averageTimeTitle', '')}>
            {t(DASHBOARD_COLLECTOR.AVERAGE_TIME)}:
          </p>
          <p data-testid={genAmtId(dataTestId, 'averageTime', '')}>
            {formatTime(averageTime || 0, 'm', 's')}
          </p>
        </div>
      </div>
    </>
  );
};

export default Footer;
