import React from 'react';

// types
import { DashBoardProps, CollectorDetail } from '../../types';

// components
import { Bubble, Button } from 'app/_libraries/_dls/components';
import Footer from './Footer';
import Detail from './Detail';

// translate
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_DASH_BOARD, DASHBOARD_COLLECTOR } from '../../constants';
import { ZERO } from 'app/constants';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { NoDataGrid } from 'app/components';

// helpers
import isEmpty from 'lodash.isempty';

export interface CoreViewProps extends DashBoardProps {
  data?: CollectorDetail[];
  handleOpenWorkQueue?: (dataItem?: CollectorDetail) => void;
}

const CoreView: React.FC<CoreViewProps> = ({ data, handleOpenWorkQueue }) => {
  const { t } = useTranslation();
  const testId = 'dashboard_collector_coreView';

  const handleClick = (dataItem?: CollectorDetail) => {
    typeof handleOpenWorkQueue === 'function' && handleOpenWorkQueue(dataItem);
  };

  const renderCollectorList = (dataItem: CollectorDetail, idx: number) => {
    const workQueue = dataItem?.workQueue || ZERO;
    return (
      <div className="col-xl-6 col-md-12 mt-16">
        <div className="px-24 pb-24 pt-20 bg-white rounded-lg border br-light-l04">
          <div className="d-flex align-items-center">
            <Bubble
              name={dataItem?.owner?.fullName}
              small
              dataTestId={`${idx}_${testId}_fullName-bubble`}
            />
            <h5
              className="ml-8"
              data-testid={genAmtId(`${idx}_${testId}`, 'fullName', '')}
            >
              {dataItem?.owner?.fullName}
            </h5>
            <Button
              className="ml-auto mr-n8"
              onClick={() => handleClick(dataItem)}
              size="sm"
              variant="outline-primary"
              dataTestId={`${idx}_${testId}_workQueues-btn`}
            >
              {`${workQueue} ${
                workQueue > 1
                  ? t(DASHBOARD_COLLECTOR.WORK_QUEUES)
                  : t(I18N_DASH_BOARD.WORK_QUEUE)
              }`}
            </Button>
          </div>
          <Detail dataItem={dataItem} dataTestId={`${idx}_${testId}_detail`} />
          <div className="my-16 border-bottom"></div>
          <Footer
            breakTotal={dataItem?.breakTotal}
            breakTime={dataItem?.breakTime}
            inbound={dataItem?.inbound}
            outbound={dataItem?.outbound}
            averageTime={dataItem?.averageTime}
            dataTestId={`${idx}_${testId}_footer`}
          />
        </div>
      </div>
    );
  };

  return (
    <div className="row">
      {data && !isEmpty(data) ? (
        data.map((item, index) => {
          return (
            <React.Fragment key={index}>
              {renderCollectorList(item, index)}
            </React.Fragment>
          );
        })
      ) : (
        <div className="col-12">
          <NoDataGrid text={t('txt_no_collector_data')} />
        </div>
      )}
    </div>
  );
};

export default CoreView;
