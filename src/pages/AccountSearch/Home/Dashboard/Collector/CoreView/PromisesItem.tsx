import React from 'react';

//libs
import classnames from 'classnames';

// helpers
import { formatCommon } from 'app/helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface PromisesItemProps {
  title?: string;
  value?: number;
  amount?: number;
  isRenderDivider?: boolean;
  dataTestId?: string;
}

const PromisesItem: React.FC<PromisesItemProps> = ({
  title,
  value,
  amount,
  isRenderDivider,
  dataTestId,
}) => {
  return (
    <>
      <div
        className={classnames(
          'd-flex  align-items-center',
          isRenderDivider && 'mb-8'
        )}
      >
        <div className="w-40 pr-8">
          <h6 data-testid={genAmtId(dataTestId, 'title', '')}>{title}</h6>
        </div>
        <div className="w-15 pr-8">
          <p data-testid={genAmtId(dataTestId, 'value', '')}>{value}</p>
        </div>
        <div className="w-45 border-left pl-8">
          <p data-testid={genAmtId(dataTestId, 'amount', '')}>{amount && formatCommon(amount).currency(2)}</p>
        </div>
      </div>
      {isRenderDivider && <div className="mb-8 divider-dashed"></div>}
    </>
  );
};

export default PromisesItem;
