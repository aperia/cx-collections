import React from 'react';
import { renderMockStoreId } from 'app/test-utils';
import Detail from './Detail';

const renderWrapper = (data: Record<string, any>) => {
  return renderMockStoreId(<Detail dataItem={data} />);
};

describe('Detail', () => {
  it('Render component with data', () => {
    const data = {
      collectorId: 'collectorId',
      owner: 'OwnerModel',
      workQueue: 'workQueue',
      accountWorked: 'totalAmountCollected',
      totalAccountWorked: 'totalAmountCollected',
      amountCollected: 'totalAmountCollected',
      totalAmountCollected: 'totalAmountCollected',
      accountAverage: 'accountAverage',
      averageCollected: 'averageCollected',
      promises: [
        {
          name: 'pending',
          value: 'value',
          amount: 1,
          averageValue: 1,
          averageAmount: 2
        },
        {
          name: 'kept',
          value: 'value',
          amount: 1,
          averageValue: 1,
          averageAmount: 2
        },
        {
          name: 'broken',
          value: 'value',
          amount: 1,
          averageValue: 1,
          averageAmount: 2
        },
        {
          name: 'total',
          value: 'value',
          amount: 1,
          averageValue: 1,
          averageAmount: 2
        },
        {
          name: '',
          value: 'value',
          amount: 1,
          averageValue: 1,
          averageAmount: 2
        }
      ],
      breakTotal: 1,
      breakTime: 1,
      inbound: 2,
      outbound: 3,
      averageTime: 4,
      averageBreakTotal: 5,
      averageBreakTime: 6,
      averageInbound: 7,
      averageOutbound: 8,
      teamAverageTime: 9
    };
    const { wrapper } = renderWrapper(data);
    expect(wrapper.getByText('txt_accounts_worked_plural')).toBeInTheDocument();
  });

  it('Render component with empty data', () => {
    const { wrapper } = renderWrapper({});
    expect(wrapper.getByText('txt_accounts_worked_plural')).toBeInTheDocument();
  });
});
