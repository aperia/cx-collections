import React from 'react';
import { renderMockStoreId } from 'app/test-utils';
import { screen } from '@testing-library/react';
//components
import PromisesItem from './PromisesItem';

const renderWrapper = (data: Record<string, any>) => {
  return renderMockStoreId(
    <PromisesItem
      title={data.title}
      value={data.value}
      amount={data.amount}
      averageAmount={data.averageAmount}
      averageValue={data.averageValue}
      isRenderDivider
    />
  );
};

describe('PromisesItem with data', () => {
  const data = {
    title: '',
    value: 20,
    amount: 20,
    averageAmount: 12,
    averageValue: 10
  };
  it('Render component', () => {
    renderWrapper(data);
    expect(screen.getByText(/txt_team_average/)).toBeInTheDocument();
  });
});

describe('PromisesItem empty data', () => {
  it('Render component', () => {
    renderWrapper({});

    expect(screen.getByText(/txt_team_average/)).toBeInTheDocument();
  });
});
