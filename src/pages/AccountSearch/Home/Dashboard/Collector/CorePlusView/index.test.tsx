import React from 'react';
import { renderMockStoreId } from 'app/test-utils';
import { screen } from '@testing-library/react';
import CorePlusView from './index';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

const mockFn = jest.fn();
const renderWrapper = (data: any) => {
  return renderMockStoreId(
    <CorePlusView userType="" data={data} handleOpenWorkQueue={mockFn} />
  );
};

describe('CorePlusView', () => {
  it('Render component', () => {
    const data = [
      {
        collectorId: 'collectorId',
        owner: 'OwnerModel',
        workQueue: 'workQueue',
        accountWorked: 'totalAmountCollected',
        totalAccountWorked: 'totalAmountCollected',
        amountCollected: 'totalAmountCollected',
        totalAmountCollected: 'totalAmountCollected',
        accountAverage: 'accountAverage'
      }
    ];
    renderWrapper(data);
    expect(screen.getByText('txt_accounts_worked_plural')).toBeInTheDocument();
  });

  it('Render component with empty data', () => {
    renderMockStoreId(
      <CorePlusView userType="" handleOpenWorkQueue={mockFn} />
    );

    expect(screen.getByText('txt_low')).toBeInTheDocument();
  });

  it('Render component with empty workQueue', () => {
    const data = [
      {
        collectorId: 'collectorId',
        owner: 'OwnerModel',
        workQueue: '',
        accountWorked: 'totalAmountCollected',
        totalAccountWorked: 'totalAmountCollected',
        amountCollected: 'totalAmountCollected',
        totalAmountCollected: 'totalAmountCollected',
        accountAverage: 'accountAverage'
      }
    ];
    renderWrapper(data);
    expect(screen.getByText('txt_low')).toBeInTheDocument();
  });

  it('Render component with workQueue more than 1', () => {
    const data = [
      {
        collectorId: 'collectorId',
        owner: 'OwnerModel',
        workQueue: 2,
        accountWorked: 'totalAmountCollected',
        totalAccountWorked: 'totalAmountCollected',
        amountCollected: 'totalAmountCollected',
        totalAmountCollected: 'totalAmountCollected',
        accountAverage: 'accountAverage'
      }
    ];
    renderWrapper(data);
    const button = screen.getByText(/txt_work_queues/);
    button.click();

    expect(mockFn).toHaveBeenCalled();
  });

  it('Handle click', () => {
    const data = [
      {
        collectorId: 'collectorId',
        owner: 'OwnerModel',
        workQueue: 'workQueue',
        accountWorked: 'totalAmountCollected',
        totalAccountWorked: 'totalAmountCollected',
        amountCollected: 'totalAmountCollected',
        totalAmountCollected: 'totalAmountCollected',
        accountAverage: 'accountAverage'
      }
    ];
    renderWrapper(data);
    const button = screen.getByText(/txt_work_queue/);
    button.click();

    expect(mockFn).toHaveBeenCalled();
  });
});
