import React from 'react';

//translate
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { DASHBOARD_COLLECTOR } from '../../constants';
import { ZERO } from 'app/constants';

// helper
import { formatTime, getColorValue } from '../helper';

// type
import { CollectorDetail } from '../../types';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface FooterProps {
  dataItem?: CollectorDetail;
  dataTestId?: string;
}

const Footer: React.FC<FooterProps> = ({ dataItem, dataTestId }) => {
  const { t } = useTranslation();

  return (
    <React.Fragment>
      <div className="d-flex">
        <div className="d-flex flex-column">
          <div className="d-flex align-items-center">
            <h6 className="mr-16" data-testid={genAmtId(dataTestId, 'breakTitle', '')}>
              {t(DASHBOARD_COLLECTOR.BREAK)}
            </h6>
            <p className="mr-4" data-testid={genAmtId(dataTestId, 'totalBreaksTitle', '')}>
              {t(DASHBOARD_COLLECTOR.TOTAL_BREAKS)}:
              </p>
          </div>
          <p className="color-grey-l16" data-testid={genAmtId(dataTestId, 'break_teamAverageTitle', '')}>
            {t(DASHBOARD_COLLECTOR.TEAM_AVERAGE)}:
          </p>
        </div>
        <div className="mr-16">
          <p
            data-testid={genAmtId(dataTestId, 'breakTotal', '')}
            className={`color-${getColorValue(
              dataItem?.averageBreakTotal || ZERO,
              dataItem?.breakTotal || ZERO
            )}-d08`}
          >
            {dataItem?.breakTotal}
          </p>
          <p className="color-grey-l16" data-testid={genAmtId(dataTestId, 'averageBreakTotal', '')}>
            {dataItem?.averageBreakTotal}
          </p>
        </div>
        <p
          className="mr-4"
          data-testid={genAmtId(dataTestId, 'totalBreakTimeTitle', '')}
        > {t(DASHBOARD_COLLECTOR.TOTAL_BREAK_TIME)}:</p>

        <div>
          <p
            className={`color-${getColorValue(
              dataItem?.averageBreakTime || ZERO,
              dataItem?.breakTime || ZERO
            )}-d08`}
            data-testid={genAmtId(dataTestId, 'breakTime', '')}
          >
            {formatTime(dataItem?.breakTime, 'h', 'm')}
          </p>
          <p
            className="color-grey-l16"
            data-testid={genAmtId(dataTestId, 'averageBreakTime', '')}
          >
            {formatTime(dataItem?.averageBreakTime, 'h', 'm')}
          </p>
        </div>
      </div>
      <div className="divider-dashed my-8"></div>
      <div className="d-flex">
        <div className="d-flex flex-column">
          <div className="d-flex align-items-center">
            <h6 className="mr-16" data-testid={genAmtId(dataTestId, 'callInfoTitle', '')}>
              {t(DASHBOARD_COLLECTOR.CALL_INFORMATION)}
            </h6>
            <p className="color-grey mr-4" data-testid={genAmtId(dataTestId, 'inboundTitle', '')}>
              {t(DASHBOARD_COLLECTOR.INBOUND)}:
            </p>
          </div>
          <p className="color-grey-l16" data-testid={genAmtId(dataTestId, 'callInfo_teamAverageTitle', '')}>
            {t(DASHBOARD_COLLECTOR.TEAM_AVERAGE)}:
          </p>
        </div>
        <div className="mr-24">
          <p
            className={`color-${getColorValue(
              dataItem?.averageInbound || ZERO,
              dataItem?.inbound || ZERO
            )}-d08`}
            data-testid={genAmtId(dataTestId, 'inbound', '')}
          >
            {dataItem?.inbound}
          </p>
          <p className="color-grey-l16" data-testid={genAmtId(dataTestId, 'averageInbound', '')}>
            {dataItem?.averageInbound}
          </p>
        </div>
        <p
          className="color-grey mr-4"
          data-testid={genAmtId(dataTestId, 'outnboundTitle', '')}
        >{t(DASHBOARD_COLLECTOR.OUTBOUND)}:</p>
        <div className="mr-24">
          <p
            className={`color-${getColorValue(
              dataItem?.averageOutbound || ZERO,
              dataItem?.outbound || ZERO
            )}-d08`}
            data-testid={genAmtId(dataTestId, 'outbound', '')}
          >
            {dataItem?.outbound}
          </p>
          <p
            className="color-grey-l16"
            data-testid={genAmtId(dataTestId, 'averageOutbound', '')}
          >{dataItem?.averageOutbound}</p>
        </div>
        <p className="color-grey mr-4" data-testid={genAmtId(dataTestId, 'callInfo_averageTimeTitle', '')}>
          {t(DASHBOARD_COLLECTOR.AVERAGE_TIME)}:
        </p>

        <div className="d-flex flex-column">
          <p
            className={`color-${getColorValue(
              dataItem?.teamAverageTime || ZERO,
              dataItem?.averageTime || ZERO
            )}-d08`}
            data-testid={genAmtId(dataTestId, 'callInfo_averageTime', '')}
          >
            {formatTime(dataItem?.averageTime, 'm', 's')}
          </p>
          <p className="color-grey-l16" data-testid={genAmtId(dataTestId, 'callInfo_teamAverageTime', '')}>
            {formatTime(dataItem?.teamAverageTime, 'm', 's')}
          </p>
        </div>
      </div>
    </React.Fragment>
  );
};

export default Footer;
