import React from 'react';

// component
import { Button, Icon, Popover } from 'app/_libraries/_dls/components';

//translate
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { DASHBOARD_COLLECTOR } from '../../constants';
import { COLOR_VALUE } from '../contants';
import { genAmtId } from 'app/_libraries/_dls/utils';

export const Information = () => {
  const { t } = useTranslation();
  const testId = 'dashboard_collector_corePlusView_info';

  return (
    <div className="d-flex align-items-center mt-16">
      <div className="d-flex align-items-center mr-24">
        <span className="dot bg-green-d08"></span>
        <span
          className="ml-4 color-grey-l16 fs-12"
          data-testid={genAmtId(testId, 'high', '')}
        >
          {t(DASHBOARD_COLLECTOR.HIGH)}
        </span>
      </div>
      <div className="d-flex align-items-center mr-24">
        <span className="dot bg-orange-d08"></span>
        <span
          className="ml-4 color-grey-l16 fs-12"
          data-testid={genAmtId(testId, 'medium', '')}
        >
          {t(DASHBOARD_COLLECTOR.MEDIUM)}
        </span>
      </div>
      <div className="d-flex align-items-center mr-24">
        <span className="dot bg-red-d08"></span>
        <span
          className="ml-4 color-grey-l16 fs-12"
          data-testid={genAmtId(testId, 'low', '')}
        >
          {t(DASHBOARD_COLLECTOR.LOW)}
        </span>
      </div>
      <Popover
        size="md"
        placement="top"
        popoverInnerClassName="overflow-hidden p-0"
        element={
          <>
            <div className="popover-heading bg-light-l20 px-8">
              <div className="row gutters-8 pt-16">
                <div className="col-4 pb-16 pl-16">
                  <div className="d-flex align-items-center">
                    <span className="dot bg-green-d08"></span>
                    <span className="ml-4" data-testid={genAmtId(testId, 'high-popover', '')}>
                      {t(DASHBOARD_COLLECTOR.HIGH)}
                    </span>
                  </div>
                </div>
                <div className="col-4 pb-16 pl-16 border-left">
                  <div className="d-flex align-items-center">
                    <span className="dot bg-orange-d08"></span>
                    <span className="ml-4" data-testid={genAmtId(testId, 'medium-popover', '')}>
                      {t(DASHBOARD_COLLECTOR.MEDIUM)}
                    </span>
                  </div>
                </div>
                <div className="col-4 pb-16 pl-16 border-left">
                  <div className="d-flex align-items-center">
                    <span className="dot bg-red-d08"></span>
                    <span className="ml-4" data-testid={genAmtId(testId, 'low-popover', '')}>
                      {t(DASHBOARD_COLLECTOR.LOW)}
                    </span>
                  </div>
                </div>
              </div>
            </div>
            <div className="popover-content px-8 pb-16">
              <div className="row gutters-8">
                <div className="col-4 pt-8 pl-16">
                  <p className="fw-500" data-testid={genAmtId(testId, 'effectBreak_break', '')}>
                    {t(DASHBOARD_COLLECTOR.BREAK_CAPITALIZE)}
                  </p>
                  <p data-testid={genAmtId(testId, 'effectBreak', '')}>
                    {t(DASHBOARD_COLLECTOR.EFFECTIVE_BREAK)}
                  </p>
                </div>
                <div className="col-4 pt-8 pl-16 border-left">
                  <p className="fw-500" data-testid={genAmtId(testId, 'mediumBreak_break', '')}>
                    {t(DASHBOARD_COLLECTOR.BREAK_CAPITALIZE)}
                  </p>
                  <p data-testid={genAmtId(testId, 'mediumBreak', '')}>
                    {t(DASHBOARD_COLLECTOR.MEDIUM_BREAK).replace(
                      '{0}',
                      COLOR_VALUE.MEDIUM
                    )}
                  </p>
                </div>
                <div className="col-4 pt-8 pl-16 border-left">
                  <p className="fw-500" data-testid={genAmtId(testId, 'poorBreak_break', '')}>
                    {t(DASHBOARD_COLLECTOR.BREAK_CAPITALIZE)}
                  </p>
                  <p data-testid={genAmtId(testId, 'poorBreak', '')}>
                    {t(DASHBOARD_COLLECTOR.MEDIUM_BREAK).replace(
                      '{0}',
                      COLOR_VALUE.POOR
                    )}
                  </p>
                </div>
              </div>
              <div className="row gutters-8">
                <div className="col-4 pt-16 pl-16">
                  <p className="fw-500" data-testid={genAmtId(testId, 'effectRest_rest', '')}>
                    {t(DASHBOARD_COLLECTOR.REST)}
                  </p>
                  <p data-testid={genAmtId(testId, 'effectRest', '')}>
                    {t(DASHBOARD_COLLECTOR.EFFECTIVE_REST)}
                  </p>
                </div>
                <div className="col-4 pt-16 pl-16 border-left">
                  <p className="fw-500" data-testid={genAmtId(testId, 'mediumRest_rest', '')}>
                    {t(DASHBOARD_COLLECTOR.REST)}
                  </p>
                  <p data-testid={genAmtId(testId, 'mediumRest', '')}>
                    {t(DASHBOARD_COLLECTOR.MEDIUM_REST).replace(
                      '{0}',
                      COLOR_VALUE.MEDIUM
                    )}
                  </p>
                </div>
                <div className="col-4 pt-16 pl-16 border-left">
                  <p className="fw-500" data-testid={genAmtId(testId, 'poorRest_rest', '')}>
                    {t(DASHBOARD_COLLECTOR.REST)}
                  </p>
                  <p data-testid={genAmtId(testId, 'poorRest', '')}>
                    {t(DASHBOARD_COLLECTOR.MEDIUM_REST).replace(
                      '{0}',
                      COLOR_VALUE.POOR
                    )}
                  </p>
                </div>
              </div>
            </div>
          </>
        }
      >
        <Button
          size="sm"
          variant="icon-secondary"
          dataTestId={`${testId}_info-btn`}
        >
          <Icon name="information" />
        </Button>
      </Popover>
    </div>
  );
};

export default Information;
