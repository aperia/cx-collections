import React from 'react';

// component
import ProgressBar from 'app/_libraries/_dls/components/ProgressBar';
import PromisesItem from './PromisesItem';

// language
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constant
import {
  DASHBOARD_COLLECTOR,
  DASHBOARD_WORK_STATISTICS
} from '../../constants';
import { ZERO } from 'app/constants';

// helper
import { formatCommon } from 'app/helpers';
import { getColorValue, getPromisesTitle } from '../helper';

// type
import { CollectorDetail } from '../../types';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface DetailProps {
  dataItem?: CollectorDetail;
  dataTestId?: string;
}

const Detail: React.FC<DetailProps> = ({ dataItem, dataTestId }) => {
  const { t } = useTranslation();

  const renderPercent = (id: string) => (values: number[]) => (
    <div
      className="dls-progress-bar-percent"
      data-testid={genAmtId(dataTestId, `${id}-percent`, '')}
    >
      {(((values[0] / values[1]) % 100) * 100).toFixed(0)}%
    </div>
  );

  const renderTopView = (id: string) => (values: number[], isCurrency: boolean) => (
    <div
      className="dls-progress-bar-top"
      data-testid={genAmtId(dataTestId, `${id}-topView`, '')}
    >
      <span
        className={`color-${getColorValue(
          dataItem?.accountAverage || ZERO,
          dataItem?.accountWorked || ZERO
        )}-d08`}
      >
        {isCurrency
          ? formatCommon(values[0] || 0).currency(2)
          : formatCommon(values[0] || 0).quantity}
      </span>{' '}
      /{' '}
      {isCurrency
        ? formatCommon(values[1] || 0).currency(2)
        : formatCommon(values[1] || 0).quantity}
    </div>
  );

  const renderAverage = (id: string) => (average: number | undefined) => (
    <div
      className="dls-progress-bar-bottom"
      data-testid={genAmtId(dataTestId, `${id}-avenrage`, '')}
    >
      {t(DASHBOARD_COLLECTOR.TEAM_AVERAGE)}:{' '}
      {formatCommon(average || 0).currency(2)}
    </div>
  );

  return (
    <div className="d-flex mt-12">
      <div className="w-40 border-right pr-16">
        <div className="progress-item mb-16">
          <h6
            className="color-grey-l24 mb-8 pt-6"
            data-testid={genAmtId(dataTestId, 'accountWorker-title', '')}
          >
            {t(DASHBOARD_WORK_STATISTICS.ACCOUNTS_WORKED)}
          </h6>

          <ProgressBar
            data={{
              values: [
                dataItem?.accountWorked || ZERO,
                dataItem?.totalAccountWorked || ZERO
              ],
              colors: ['#5cbae6', '#D7D7DE'],
              average: dataItem?.accountAverage || ZERO
            }}
            dataTestId={`${dataTestId}_accountWorker-progress`}
            renderPercent={values => renderPercent('accountWorker')(values)}
            renderTopView={values => renderTopView('accountWorker')(values, false)}
          />
        </div>
        <div className="mb-16 divider-dashed"></div>
        <div className="progress-item">
          <h6
            className="color-grey-l24 mb-8"
            data-testid={genAmtId(dataTestId, 'amountCollected-title', '')}
          >
            {t(DASHBOARD_WORK_STATISTICS.AMOUNT_COLLECTED)}
          </h6>

          <ProgressBar
            data={{
              values: [
                dataItem?.amountCollected || ZERO,
                dataItem?.totalAmountCollected || ZERO
              ],
              colors: ['#77D97C', '#D7D7DE'],
              average: dataItem?.averageCollected || ZERO
            }}
            dataTestId={`${dataTestId}_amountCollected-progress`}
            renderPercent={values => renderPercent('amountCollected')(values)}
            renderTopView={values => renderTopView('amountCollected')(values, true)}
            renderAverage={average => renderAverage('amountCollected')(average)}
          />
        </div>
      </div>
      <div className="w-60 pl-16">
        {dataItem?.promises &&
          dataItem?.promises.map((item, index) => {
            const name = getPromisesTitle(t, item?.name);
            const isLastItem =
              dataItem?.promises && dataItem?.promises?.length - 1 === index;
            return (
              <PromisesItem
                key={item?.name}
                title={name}
                value={item?.value}
                amount={item?.amount}
                averageValue={item?.averageValue}
                averageAmount={item?.averageAmount}
                isRenderDivider={!isLastItem}
                dataTestId={`${index}_${dataTestId}`}
              />
            );
          })}
      </div>
    </div>
  );
};

export default Detail;
