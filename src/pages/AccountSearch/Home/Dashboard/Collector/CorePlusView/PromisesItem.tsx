import React from 'react';

//libs
import classnames from 'classnames';

// helpers
import { getColorValue } from '../helper';

// language
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { DASHBOARD_COLLECTOR } from '../../constants';
import { ZERO } from 'app/constants';
import { formatCommon } from 'app/helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface PromisesItemProps {
  title?: string;
  value?: number;
  amount?: number;
  averageValue?: number;
  averageAmount?: number;
  isRenderDivider?: boolean;
  dataTestId?: string;
}

const PromisesItem: React.FC<PromisesItemProps> = ({
  title,
  value,
  amount,
  averageValue,
  averageAmount,
  isRenderDivider,
  dataTestId
}) => {
  const { t } = useTranslation();

  return (
    <>
      <div className={classnames('d-flex align-items-center')}>
        <div className="w-40 pr-8">
          <div className="d-flex flex-column">
            <h6 data-testid={genAmtId(dataTestId, 'title', '')}>{title}</h6>
            <p className="color-grey-l16" data-testid={genAmtId(dataTestId, 'teamAverageTitle', '')}>
              {t(DASHBOARD_COLLECTOR.TEAM_AVERAGE)}:
            </p>
          </div>
        </div>
        <div className="w-15 pr-8">
          <div className="d-flex flex-column">
            <p
              className={`color-${getColorValue(
                averageValue || ZERO,
                value || ZERO
              )}-d08`}
              data-testid={genAmtId(dataTestId, 'value', '')}
            >
              {value}
            </p>
            <p
              className="color-grey-l16"
              data-testid={genAmtId(dataTestId, 'averageValue', '')}
            >{averageValue}</p>
          </div>
        </div>
        <div className="w-45 border-left pl-8">
          <div className="d-flex flex-column">
            <p
              className={`color-${getColorValue(
                averageAmount || ZERO,
                amount || ZERO
              )}-d08`}
              data-testid={genAmtId(dataTestId, 'amount', '')}
            >
              {formatCommon(amount || ZERO).currency(2)}
            </p>
            <p
              className="color-grey-l16"
              data-testid={genAmtId(dataTestId, 'averageAmount', '')}
            >
              {formatCommon(averageAmount || ZERO).currency(2)}
            </p>
          </div>
        </div>
      </div>
      {isRenderDivider && <div className="divider-dashed my-8"></div>}
    </>
  );
};

export default PromisesItem;
