import React from 'react';
import {
  renderMockStoreId,
  mockActionCreator,
  queryByClass
} from 'app/test-utils';
import { screen, fireEvent } from '@testing-library/react';
import DashBoardCollector from './index';
import { dashboardActions } from '../_redux/reducers';
import { tabActions } from 'pages/__commons/TabBar/_redux';
import { I18N_DASH_BOARD } from '../constants';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

const initialErrorState: Partial<RootState> = {
  dashboard: {
    collector: {
      data: [],
      selectedPeriod: 'Today',
      loading: true,
      error: 'error'
    }
  }
};

const initialState: Partial<RootState> = {
  dashboard: {
    collector: {
      data: [
        {
          collectorId: 'collectorId',
          owner: 'OwnerModel',
          workQueue: 'workQueue',
          accountWorked: 'totalAmountCollected',
          totalAccountWorked: 'totalAmountCollected',
          amountCollected: 'totalAmountCollected',
          totalAmountCollected: 'totalAmountCollected',
          accountAverage: 'accountAverage'
        }
      ],
      selectedPeriod: 'Today',
      loading: true,
      error: ''
    }
  }
};

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStoreId(
    <DashBoardCollector userType="CXADMN1" title="Shyn" />,
    { initialState }
  );
};

const dashBoardActionSpy = mockActionCreator(dashboardActions);
const tabActionsSpy = mockActionCreator(tabActions);

describe('Home component', () => {
  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app',
      isAdminRole: true,
      isCollector: false
    }
  };

  beforeEach(() => {
    HTMLCanvasElement.prototype.getContext = () => null;
    dashBoardActionSpy('getDashboardCollector');
  });

  it('render component with Error -> handleReload', () => {
    const spy = dashBoardActionSpy('getDashboardCollector');
    renderWrapper(initialErrorState);

    // handleReload
    const button = screen.getByText('txt_reload');
    button?.click();

    expect(spy).toBeCalledTimes(2);
  });

  it('render component -> onBlur', () => {
    const spy = dashBoardActionSpy('selectedCollectorPeriod');
    const { wrapper } = renderWrapper(initialState);

    // onBlur
    const dropdown = queryByClass(wrapper.container, /text-field-container/);
    fireEvent.blur(dropdown!);
    expect(spy).toHaveBeenCalled();
  });

  it('handleOpenViewAllCollector', () => {
    const spy = tabActionsSpy('addTab');
    const { wrapper } = renderWrapper(initialState);

    const button = wrapper.getByText('txt_view_all_collectors');
    button?.click();
    expect(spy).toHaveBeenCalledWith({
      title: 'txt_collector_list',
      storeId: 'collectorList',
      accEValue: 'collectorValue',
      tabType: 'collectorList',
      iconName: 'file'
    });
  });

  it('handleOpenViewAllCollector', () => {
    const spy = tabActionsSpy('addTab');
    const { wrapper } = renderWrapper(initialState);

    const button = wrapper.getByText(/txt_work_queue/);
    button?.click();
    expect(spy).toHaveBeenCalledWith({
      title: 'txt_queue_list',
      storeId: 'workQueueCollector',
      accEValue: 'collectorId',
      tabType: 'workQueueCore',
      iconName: 'file',
      props: {
        collectorId: 'collectorId',
        selectedOwner: 'OwnerModel'
      }
    });
  });

  it('handleOpenViewAllCollector with error case', () => {
    const spy = tabActionsSpy('addTab');
    const { wrapper } = renderWrapper({
      dashboard: {
        collector: {
          data: [
            {
              collectorId: '',
              owner: 'OwnerModel',
              workQueue: 'workQueue',
              accountWorked: 'totalAmountCollected',
              totalAccountWorked: 'totalAmountCollected',
              amountCollected: 'totalAmountCollected',
              totalAmountCollected: 'totalAmountCollected',
              accountAverage: 'accountAverage'
            }
          ],
          selectedPeriod: 'Today',
          loading: true,
          error: ''
        }
      }
    });

    const button = wrapper.getByText(/txt_work_queue/);
    button?.click();
    expect(spy).toBeCalledTimes(0);
  });

  it('render component userType !== ADMIN1, CXCOL1', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app',
        isAdminRole: false,
        isCollector: true
      }
    };
    const renderWrapper = (initialState: Partial<RootState>) => {
      return renderMockStoreId(
        <DashBoardCollector userType="CXCOL2" title="Shyn" />,
        { initialState }
      );
    };
    renderWrapper({});
    expect(screen.getByText(I18N_DASH_BOARD.PERIOD)).toBeInTheDocument();
  });

  it('render component userType !== ADMIN1, CXCOL1 > case 2', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app',
        isAdminRole: false,
        isCollector: false
      }
    };
    const renderWrapper = (initialState: Partial<RootState>) => {
      return renderMockStoreId(
        <DashBoardCollector userType="CXCOL2" title="Shyn" />,
        { initialState }
      );
    };
    renderWrapper({});
    expect(screen.getByText(I18N_DASH_BOARD.PERIOD)).toBeInTheDocument();
  });

  it('render component userType !== ADMIN1, CXCOL1 > case 3', () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: undefined
    };
    const renderWrapper = (initialState: Partial<RootState>) => {
      return renderMockStoreId(
        <DashBoardCollector userType="CXCOL2" title="Shyn" />,
        { initialState }
      );
    };
    renderWrapper({});
    expect(screen.getByText(I18N_DASH_BOARD.PERIOD)).toBeInTheDocument();
  });
});
