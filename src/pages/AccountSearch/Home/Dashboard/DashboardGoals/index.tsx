import React, { useEffect } from 'react';

//constants
import { I18N_DASH_BOARD, DASHBOARD_GOAL_ITEM } from '../constants';

//types
import { DashBoardProps } from '../types';

//translate
import { useTranslation } from 'app/_libraries/_dls/hooks';

//redux
import { useDispatch, useSelector } from 'react-redux';

//libs
import classnames from 'classnames';

//selector
import {
  selectedDashboardGoal,
  selectedDashboardGoalLoading,
  selectedDashboardGoalError
} from '../_redux/selectors';

//actions
import { dashboardActions as actions } from '../_redux/reducers';

//components
import DBGoalItem from '../common/DBGoalItem';
import { FailedApiReload } from 'app/components';

//helpers
import { genAmtId } from 'app/_libraries/_dls/utils';

const DashboardGoals: React.FC<DashBoardProps> = ({ title }) => {
  const { t } = useTranslation();

  const items = useSelector(selectedDashboardGoal);

  const isLoading = useSelector(selectedDashboardGoalLoading);

  const isError = useSelector(selectedDashboardGoalError);

  const dispatch = useDispatch();

  const handleReload = () => dispatch(actions.getDashboardGoal({}));

  useEffect(() => {
    dispatch(actions.getDashboardGoal({}));
  }, [dispatch]);

  const renderDaily = (
    <div className="border-lg-right w-lg-50 w-md-100 pr-lg-24">
      <h5
        className="mb-2"
        data-testid={genAmtId('dashboard_goals_daily', 'title', '')}
      >
        {t(I18N_DASH_BOARD.DAILY)}
      </h5>
      <DBGoalItem
        goalType="daily"
        items={items}
        accountTitle={t(DASHBOARD_GOAL_ITEM.ACCOUNTS_PER_DAY)}
        amountTitle={t(DASHBOARD_GOAL_ITEM.AMOUNT_PER_DAY)}
      />
    </div>
  );

  const renderMonthly = (
    <div className="border-md-top border-lg-none w-lg-50 w-md-100 ml-lg-24 mt-md-24 mt-lg-0 pt-md-24 pt-lg-0">
      <h5
        className="mb-2"
        data-testid={genAmtId('dashboard_goals_monthly', 'title', '')}
      >
        {t(I18N_DASH_BOARD.MONTHLY)}
      </h5>
      <DBGoalItem
        goalType="monthly"
        items={items}
        accountTitle={t(DASHBOARD_GOAL_ITEM.ACCOUNTS_PER_MONTH)}
        amountTitle={t(DASHBOARD_GOAL_ITEM.AMOUNT_PER_MONTH)}
      />
    </div>
  );

  if (isError)
    return (
      <div>
        <FailedApiReload
          id="dashboard-goal-failed"
          dataTestId="dashboard-goal-failed"
          className="mt-80"
          onReload={handleReload}
        />
      </div>
    );

  return (
    <div className={classnames({ loading: isLoading })}>
      <h4
        className="mb-16"
        data-testid={genAmtId('dashboard_goals', 'title', '')}
      >
        {title}
      </h4>
      <div className="card card-dashboard">
        <div className="d-lg-flex">
          {renderDaily}
          {renderMonthly}
        </div>
      </div>
    </div>
  );
};

export default DashboardGoals;
