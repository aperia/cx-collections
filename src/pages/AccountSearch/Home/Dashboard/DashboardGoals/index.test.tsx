import React from 'react';

//mock test utils
import { mockActionCreator, renderMockStore } from 'app/test-utils';

//react-testing-lib
import { screen } from '@testing-library/react';

//components
import DashboardGoals from '.';

//actions
import { dashboardActions } from '../_redux/reducers';
import userEvent from '@testing-library/user-event';
import { USER_ROLE } from '../constants';

jest.mock('app/_libraries/_dls/components/DoughnutChart', () => () => {
  return <div data-testid="chart">DoughnutChart</div>;
});

HTMLCanvasElement.prototype.getContext = jest.fn();

const mockActions = mockActionCreator(dashboardActions);

const renderWrapper = (initialState: Partial<RootState>) => {
  return renderMockStore(
    <DashboardGoals userType={USER_ROLE.CXCOL5} title="test title" />,
    { initialState }
  );
};

describe('Dashboard Goals', () => {
  it('should render UI', () => {
    const data: Partial<RootState> = {
      dashboard: {
        callActivity: {
          data: [],
          period: '',
          error: false,
          loading: false
        },
        goal: {
          data: [],
          error: '',
          loading: false
        },
        workQueue: {
          data: [],
          error: '',
          loading: false
        },
        workStatistic: {
          data: [],
          error: '',
          selectedPeriod: '',
          loading: false
        }
      }
    };

    renderWrapper(data);

    const queryTextTitle = screen.getByText('test title');

    const queryAllChart = screen.getAllByTestId('chart');

    expect(queryTextTitle).toBeInTheDocument();

    expect(queryAllChart).toHaveLength(4);
  });
});

describe('should render with error msg', () => {
  const data: Partial<RootState> = {
    dashboard: {
      callActivity: {
        data: [],
        period: '',
        error: false,
        loading: false
      },
      goal: {
        data: [],
        error: 'error_msg',
        loading: false
      },
      workQueue: {
        data: [],
        error: '',
        loading: false
      },
      workStatistic: {
        data: [],
        error: '',
        selectedPeriod: '',
        loading: false
      }
    }
  };

  it('should render UI with error message', () => {
    renderWrapper(data);

    const queryTextReload = screen.getByText(
      'txt_data_load_unsuccessful_click_reload_to_try_again'
    );

    expect(queryTextReload).toBeInTheDocument();
  });

  it('should render UI with error message and click reload button', () => {
    const mockReload = mockActions('getDashboardGoal');

    renderWrapper(data);

    const reloadButton = screen.getByRole('button', {
      name: 'txt_reload'
    });

    userEvent.click(reloadButton);

    expect(mockReload).toBeCalledTimes(2);
  });
});
