import { createStore, Store } from '@reduxjs/toolkit';
import { getDashboardCollector } from './getDashboardCollector';
import { dashboardServices } from '../dashboardServices';
import { responseDefault } from 'app/test-utils';
import { rootReducer } from 'storeConfig';

let store: Store<RootState>;
let state: RootState;

const mockUserArgs = {
  userType: 'CXCOL5',
  period: 'today'
};

beforeEach(() => {
  store = createStore(rootReducer, {});
  state = store.getState();
});

describe('should have test getDashboardCollector', () => {
  it('Should return data', async () => {
    jest
      .spyOn(dashboardServices, 'getDashboardCollector')
      .mockResolvedValue({
        ...responseDefault,
        data: []
      });

    const response = await getDashboardCollector(mockUserArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toBeUndefined()
  });

  it('should response getDashboardCollector builder', () => {
    const pendingAction = getDashboardCollector.pending(
      getDashboardCollector.pending.type,
      mockUserArgs
    );
    const actual = rootReducer(state, pendingAction);

    expect(actual?.dashboard?.collector?.loading).toEqual(true);
  });

  it('should response getDashboardCollector fulfilled', () => {
    const fulfilled = getDashboardCollector.fulfilled(
      { data: [] },
      getDashboardCollector.fulfilled.type,
      mockUserArgs
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual?.dashboard?.collector?.loading).toEqual(false);
    expect(actual?.dashboard?.collector?.data).toEqual([]);
  });

  it('should response getDashboardCollector rejected', () => {
    const rejected = getDashboardCollector.rejected(
      null,
      getDashboardCollector.rejected.type,
      mockUserArgs
    );
    const actual = rootReducer(state, rejected);

    expect(actual?.dashboard?.collector?.loading).toEqual(false);
  });
})