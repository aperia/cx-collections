import './reducers';
import { createStore, Store } from '@reduxjs/toolkit';

import { getRecentlyWorkQueue } from './getRecentlyWorkQueue';

import { dashboardServices } from '../dashboardServices';

import { responseDefault } from 'app/test-utils';

import { rootReducer } from 'storeConfig';

let store: Store<RootState>;
let state: RootState;

let spy: jest.SpyInstance;
let spy1: jest.SpyInstance;

describe('should have test getRecentlyWorkQueue', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();

    spy1?.mockReset();
    spy1?.mockRestore();
  });
  // beforeEach(() => {
  //   store = createStore(rootReducer, {
  //     mapping: {
  //       data: {
  //         dashboardRecentWorkQueue: 'collectorRecentWorkQueueList'
  //       }
  //     }
  //   } as any);
  //   state = store.getState();
  // });
  it('Should return data', async () => {
    store = createStore(rootReducer, {
      mapping: {
        data: {
          dashboardRecentWorkQueue: {
            collectorRecentWorkQueueList: 'collectorRecentWorkQueueList'
          }
        }
      }
    } as any);
    spy = jest
      .spyOn(dashboardServices, 'getRecentlyWorkQueue')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          collectorRecentWorkQueueList: [
            {
              queueId: '01',
              collectorPromiseStats: {
                brokenAmount: '2352.00',
                brokenPromises: '156',
                keptAmount: '20840.00',
                keptPromises: '220',
                pendingAmount: '35900.00',
                pendingPromises: '304',
                totalAmount: '59092.00',
                totalPromises: '680'
              },
              collectorQueueStats: {
                accountsWorked: '180',
                amountCollected: '24032.00',
                totalAccounts: '680',
                totalAmount: '59092.00'
              },
              queueName: 'test'
            }
          ],
          status: 'success'
        }
      });

    const response = await getRecentlyWorkQueue()(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig['extra']
    );

    expect(response.type).toEqual('dashboard/getRecentlyWorkQueue/fulfilled');
  });

  it('Should return data > case 2', async () => {
    spy = jest
      .spyOn(dashboardServices, 'getRecentlyWorkQueue')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          collectorRecentWorkQueueList: [
            {
              queueId: '01',
              collectorPromiseStats: {
                brokenAmount: '2352.00',
                brokenPromises: '156',
                keptAmount: '20840.00',
                keptPromises: '220',
                pendingAmount: '35900.00',
                pendingPromises: '304',
                totalAmount: '59092.00',
                totalPromises: '680'
              },
              collectorQueueStats: {
                accountsWorked: '180',
                amountCollected: '24032.00',
                totalAccounts: '680',
                totalAmount: '59092.00'
              },
              queueName: 'test'
            }
          ],
          status: 'success'
        }
      });

    store = createStore(rootReducer, {
      mapping: {
        data: {
          dashboardRecentWorkQueue: undefined
        }
      }
    } as any);

    const response = await getRecentlyWorkQueue()(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig['extra']
    );

    expect(response.type).toEqual('dashboard/getRecentlyWorkQueue/rejected');
  });

  it('Should return data default value mapping', async () => {
    store = createStore(rootReducer, {
      mapping: {
        data: {
          dashboardRecentWorkQueue: {
            collectorRecentWorkQueueList: 'collectorRecentWorkQueueList'
          }
        }
      }
    } as any);

    spy = jest
      .spyOn(dashboardServices, 'getRecentlyWorkQueue')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          collectorRecentWorkQueueList: [
            {
              queueId: '01',
              collectorPromiseStats: {
                brokenAmount: '2352.00',
                brokenPromises: '156',
                keptAmount: '20840.00',
                keptPromises: '220',
                pendingAmount: '35900.00',
                pendingPromises: '304',
                totalAmount: '59092.00',
                totalPromises: '680'
              },
              collectorQueueStats: {
                accountsWorked: '180',
                amountCollected: '24032.00',
                totalAccounts: '680',
                totalAmount: '59092.00'
              },
              queueName: 'test'
            }
          ],
          totalQueues: 'test'
        }
      });

    await getRecentlyWorkQueue()(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig['extra']
    );

    expect(
      store.getState().mapping.data.dashboardRecentWorkQueue
        ?.collectorRecentWorkQueueList
    ).toEqual('collectorRecentWorkQueueList');
  });

  it('should response getRecentlyWorkQueue builder', () => {
    const pendingAction = getRecentlyWorkQueue.pending(
      getRecentlyWorkQueue.pending.type,
      undefined
    );
    const actual = rootReducer(state, pendingAction);

    expect(actual?.dashboard?.recentWorkQueue?.loading).toEqual(true);
  });

  it('should response getRecentlyWorkQueue fulfilled', () => {
    const fulfilled = getRecentlyWorkQueue.fulfilled(
      {
        data: {
          totalQueues: 'totalQueues',
          collectorQueueStats: 'collectorQueueStats',
          collectorPromiseStats: 'collectorPromiseStats'
        }
      },
      getRecentlyWorkQueue.fulfilled.type,
      undefined
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual?.dashboard?.recentWorkQueue?.loading).toEqual(false);
    expect(actual?.dashboard?.recentWorkQueue?.data).toEqual({
      totalQueues: 'totalQueues',
      collectorQueueStats: 'collectorQueueStats',
      collectorPromiseStats: 'collectorPromiseStats'
    });
  });

  it('should response getRecentlyWorkQueue rejected', () => {
    const rejected = getRecentlyWorkQueue.rejected(
      null,
      getRecentlyWorkQueue.rejected.type,
      undefined
    );
    const actual = rootReducer(state, rejected);

    expect(actual?.dashboard?.recentWorkQueue?.loading).toEqual(false);
  });
});
