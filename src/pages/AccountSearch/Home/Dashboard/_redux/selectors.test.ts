import { selectorWrapper, storeId } from 'app/test-utils';
import { DASHBOARD_PERIOD } from '../constants';
import { IQueueAccount } from '../QueueDetail/types';
import {
  selectedDashboardGoal,
  selectedDashboardGoalLoading,
  selectedDashboardGoalError,
  selectedDashboardWorkQueue,
  selectedDashboardWorkQueueError,
  selectedDashboardWorkQueueLoading,
  selectedDashboardWorkStatistic,
  selectedDashboardWorkStatisticError,
  selectedDashboardWorkStatisticPeriod,
  selectedDashboardWorkStatisticLoading,
  selectedRecentDashboardWorkQueue,
  selectedRecentDashboardWorkQueueError,
  selectedRecentDashboardWorkQueueLoading,
  selectedDashboardCollector,
  selectedDashboardCollectorError,
  selectedDashboardCollectorLoading,
  selectedDashboardCollectorPeriod,
  takeAccountLockTime,
  takeAccountExtendLockTime,
  takeAccountResetTime,
  takeToggleAccountExtend,
  takeQueueAccount
} from './selectors';

describe('Test Dashboard Selectors', () => {
  const store: Partial<RootState> = {
    dashboard: {
      callActivity: {
        data: ['callActivity'],
        period: 'today',
        error: false,
        loading: false
      },
      goal: {
        data: ['goal'],
        error: 'error_msg',
        loading: false
      },
      workQueue: {
        data: ['workQueue'],
        error: 'workQueue_error_msg',
        loading: false
      },
      workStatistic: {
        data: ['workStatistic'],
        error: 'workStatistic_error_msg',
        selectedPeriod: 'month',
        loading: false
      },
      recentWorkQueue: {
        data: ['recentWorkQueue'],
        error: 'recentWorkQueue_error_msg',
        loading: false
      },
      collector: {
        data: ['collector'],
        error: 'collector_error_msg',
        loading: false,
        selectedPeriod: 'year'
      },
      accountLockTime: 0,
      queueAccountToken: '',
      extendLockTime: 0,
      accountCountTime: {
        [storeId]: {
          toggleExtendModal: true
        }
      },
      queueAccount: {
        previousAccount: undefined,
        currentAccount: {
          accountNumber: '123'
        } as IQueueAccount
      }
    }
  };

  it('selectedDashboardGoal', () => {
    const { data, emptyData } = selectorWrapper(store)(selectedDashboardGoal);

    expect(data).toEqual(['goal']);
    expect(emptyData).toEqual([]);
  });

  it('selectedDashboardGoalLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedDashboardGoalLoading
    );

    expect(data).toEqual(false);
    expect(emptyData).toEqual(false);
  });

  it('selectedDashboardGoalError', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedDashboardGoalError
    );

    expect(data).toEqual('error_msg');
    expect(emptyData).toEqual('');
  });

  it('selectedDashboardWorkQueue', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedDashboardWorkQueue
    );

    expect(data).toEqual(['workQueue']);
    expect(emptyData).toEqual([]);
  });

  it('selectedDashboardWorkQueueError', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedDashboardWorkQueueError
    );

    expect(data).toEqual('workQueue_error_msg');
    expect(emptyData).toEqual('');
  });

  it('selectedDashboardWorkQueueLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedDashboardWorkQueueLoading
    );

    expect(data).toEqual(false);
    expect(emptyData).toEqual(false);
  });

  it('selectedDashboardWorkStatistic', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedDashboardWorkStatistic
    );

    expect(data).toEqual(['workStatistic']);
    expect(emptyData).toEqual([]);
  });

  it('selectedDashboardWorkStatisticError', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedDashboardWorkStatisticError
    );

    expect(data).toEqual('workStatistic_error_msg');
    expect(emptyData).toEqual('');
  });

  it('selectedDashboardWorkStatisticPeriod', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedDashboardWorkStatisticPeriod
    );

    expect(data).toEqual('month');
    expect(emptyData).toEqual(DASHBOARD_PERIOD[0]);
  });

  it('selectedDashboardWorkStatisticLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedDashboardWorkStatisticLoading
    );

    expect(data).toEqual(false);
    expect(emptyData).toEqual(false);
  });

  it('selectedRecentDashboardWorkQueue', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedRecentDashboardWorkQueue
    );

    expect(data).toEqual(['recentWorkQueue']);
    expect(emptyData).toEqual(undefined);
  });
  it('selectedRecentDashboardWorkQueueLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedRecentDashboardWorkQueueLoading
    );

    expect(data).toEqual(false);
    expect(emptyData).toEqual(undefined);
  });

  it('selectedRecentDashboardWorkQueueError', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedRecentDashboardWorkQueueError
    );

    expect(data).toEqual('recentWorkQueue_error_msg');
    expect(emptyData).toEqual(undefined);
  });

  it('selectedDashboardCollector', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedDashboardCollector
    );

    expect(data).toEqual(['collector']);
    expect(emptyData).toEqual(undefined);
  });
  it('selectedDashboardCollectorError', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedDashboardCollectorError
    );

    expect(data).toEqual('collector_error_msg');
    expect(emptyData).toEqual(undefined);
  });

  it('selectedDashboardCollectorLoading', () => {
    const { data, emptyData } = selectorWrapper(store)(
      selectedDashboardCollectorLoading
    );

    expect(data).toEqual(false);
    expect(emptyData).toEqual(undefined);
  });

  it('selectedDashboardCollectorPeriod', () => {
    const { data } = selectorWrapper(store)(selectedDashboardCollectorPeriod);

    expect(data).toEqual('year');
  });

  it('takeAccountLockTime', () => {
    const { data, emptyData } = selectorWrapper(store)(takeAccountLockTime);

    expect(data).toEqual(0);
    expect(emptyData).toEqual(undefined);
  });

  it('takeAccountExtendLockTime', () => {
    const { data, emptyData } = selectorWrapper(store)(
      takeAccountExtendLockTime
    );

    expect(data).toEqual(0);
    expect(emptyData).toEqual(0);
  });

  it('takeAccountResetTime', () => {
    const { data, emptyData } = selectorWrapper(store)(takeAccountResetTime);

    expect(data).toEqual(false);
    expect(emptyData).toEqual(false);
  });

  it('takeToggleAccountExtend', () => {
    const { data, emptyData } = selectorWrapper(store)(takeToggleAccountExtend);

    expect(data).toEqual(true);
    expect(emptyData).toEqual(false);
  });

  it('takeQueueAccount', () => {
    const { data, emptyData } = selectorWrapper(store)(takeQueueAccount);

    expect(data).toEqual(
      expect.objectContaining(store.dashboard?.queueAccount)
    );
    expect(emptyData).toEqual(undefined);
  });
});
