import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

import { dashboardServices } from '../dashboardServices';
import { DashboardState } from '../types';

interface Args {
  period: string;
}

interface Payload {
  data: MagicKeyValue;
}

export const getDashboardCallActivity = createAsyncThunk<
  Payload,
  Args,
  ThunkAPIConfig
>('dashboard/getDashboardCallActivity', async (args, thunkAPI) => {
  const { period } = args;
  const response = await dashboardServices.getDashboardCallActivity();

  return { data: response.data[period] };
});

export const getDashboardCallActivityBuilder = (
  builder: ActionReducerMapBuilder<DashboardState>
) => {
  builder.addCase(getDashboardCallActivity.pending, (draftState, action) => {
    draftState.callActivity = {
      ...draftState?.callActivity,
      loading: true
    };
  });
  builder.addCase(getDashboardCallActivity.fulfilled, (draftState, action) => {
    const payload = action.payload;

    draftState.callActivity = {
      ...draftState?.callActivity,
      data: payload?.data,
      loading: false,
      error: false
    };
  });
  builder.addCase(getDashboardCallActivity.rejected, (draftState, action) => {
    draftState.callActivity = {
      ...draftState?.callActivity,
      loading: false,
      error: true
    };
  });
};
