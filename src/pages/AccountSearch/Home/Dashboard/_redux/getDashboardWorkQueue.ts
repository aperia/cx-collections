import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { dashboardServices } from '../dashboardServices';
import {
  DashboardRequest,
  DashboardState,
  GetDashboardWorkQueueArg,
  GetDashboardWorkQueuePayload
} from '../types';

import { mappingDataFromObj } from 'app/helpers';
import { formatPeriod } from '../helpers';

export const getDashboardWorkQueue = createAsyncThunk<
  GetDashboardWorkQueuePayload,
  GetDashboardWorkQueueArg,
  ThunkAPIConfig
>('dashboard/getDashboardWorkQueue', async (args, thunkAPI) => {
  const { period } = args;
  const { mapping } = thunkAPI.getState();

  const { startDateTime, endDateTime } = formatPeriod(period);

  const requestBody: DashboardRequest = {
    startDateTime: startDateTime,
    endDateTime: endDateTime
  };

  const mappingData = mapping.data.dashboardWorkQueue;

  const { data } = await dashboardServices.getDashboardWorkQueue(requestBody);

  const formatData = mappingDataFromObj(data, mappingData || {});
  return { data: formatData };
});

export const getDashboardWorkQueueBuilder = (
  builder: ActionReducerMapBuilder<DashboardState>
) => {
  builder.addCase(getDashboardWorkQueue.pending, (draftState, action) => {
    draftState.workQueue = {
      ...draftState.workQueue,
      loading: true
    };
  });
  builder.addCase(getDashboardWorkQueue.fulfilled, (draftState, action) => {
    const payload = action.payload;

    draftState.workQueue = {
      ...draftState.workQueue,
      data: payload?.data,
      loading: false
    };
  });
  builder.addCase(getDashboardWorkQueue.rejected, (draftState, action) => {
    draftState.workQueue = {
      ...draftState.workQueue,
      loading: false,
      error: action.payload?.errorMessage
    };
  });
};
