import { createSelector } from '@reduxjs/toolkit';
//constants
import { DASHBOARD_PERIOD } from '../constants';
import { AccCountTimeType } from '../types';

const getDashboardGoal = (states: RootState) => states.dashboard.goal;

const getDashboardWorkStatistic = (states: RootState) =>
  states.dashboard.workStatistic;

const getDashboardWorkQueue = (states: RootState) => states.dashboard.workQueue;

const getDashboardCollector = (states: RootState) => states.dashboard.collector;

const getDashboardRecentWorkQue = (states: RootState) =>
  states.dashboard.recentWorkQueue;

const getDashboardAccountLockTime = (states: RootState) =>
  states.dashboard.accountLockTime;

const getExtendLockTime = (states: RootState) =>
  states.dashboard.extendLockTime;

const getResetTime = (states: RootState) => states.dashboard.resetTime;
const getQueueAccount = (states: RootState) => states.dashboard.queueAccount;

export const selectedDashboardCollectorPeriod = (states: RootState) =>
  states.dashboard.collector?.selectedPeriod || DASHBOARD_PERIOD[0];

//queue lock unlock
export const takeAccountLockTime = createSelector(
  getDashboardAccountLockTime,
  data => data
);

export const takeAccountExtendLockTime = createSelector(
  getExtendLockTime,
  data => data || 0
);

export const takeAccountResetTime = createSelector(
  getResetTime,
  data => data || false
);

export const takeQueueAccount = createSelector(getQueueAccount, data => data);

export const takeToggleAccountExtend = createSelector(
  (states: RootState, storeId: string) =>
    states.dashboard?.accountCountTime?.[storeId],
  (data: AccCountTimeType | undefined) => data?.toggleExtendModal || false
);

//dashboard goal
export const selectedDashboardGoal = createSelector(
  getDashboardGoal,
  data => data?.data
);

export const selectedDashboardGoalLoading = createSelector(
  getDashboardGoal,
  data => data?.loading
);

export const selectedDashboardGoalError = createSelector(
  getDashboardGoal,
  data => data?.error
);

//dashboard work statistics
export const selectedDashboardWorkStatistic = createSelector(
  getDashboardWorkStatistic,
  data => data?.data
);

export const selectedDashboardWorkStatisticLoading = createSelector(
  getDashboardWorkStatistic,
  data => data?.loading
);

export const selectedDashboardWorkStatisticError = createSelector(
  getDashboardWorkStatistic,
  data => data?.error
);

export const selectedDashboardWorkStatisticPeriod = createSelector(
  getDashboardWorkStatistic,
  data => data?.selectedPeriod || DASHBOARD_PERIOD[0]
);

//dashboard work queue
export const selectedDashboardWorkQueue = createSelector(
  getDashboardWorkQueue,
  data => data?.data
);

export const selectedDashboardWorkQueueLoading = createSelector(
  getDashboardWorkQueue,
  data => data?.loading
);

export const selectedDashboardWorkQueueError = createSelector(
  getDashboardWorkQueue,
  data => data?.error
);

//dashboard recent work queue
export const selectedRecentDashboardWorkQueue = createSelector(
  getDashboardRecentWorkQue,
  data => data?.data
);

export const selectedRecentDashboardWorkQueueLoading = createSelector(
  getDashboardRecentWorkQue,
  data => data?.loading
);

export const selectedRecentDashboardWorkQueueError = createSelector(
  getDashboardRecentWorkQue,
  data => data?.error
);

//dashboard selector list
export const selectedDashboardCollector = createSelector(
  getDashboardCollector,
  data => data?.data
);

export const selectedDashboardCollectorLoading = createSelector(
  getDashboardCollector,
  data => data?.loading
);

export const selectedDashboardCollectorError = createSelector(
  getDashboardCollector,
  data => data?.error
);
