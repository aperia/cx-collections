import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  PayloadAction
} from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { dashboardServices } from '../dashboardServices';
import {
  DashboardState,
  GetAccountLockArg,
  GetAccountLockPayload,
  GetAccountLockRequest
} from '../types';

export const getAccountExtendLockTime = createAsyncThunk<
  GetAccountLockPayload,
  GetAccountLockArg,
  ThunkAPIConfig
>('dashboard/getAccountExtendLockTime', async (args, thunkAPI) => {
  const { storeId = '' } = args;
  const { app, user } = window?.appConfig?.commonConfig;
  const { rejectWithValue, getState, dispatch } = thunkAPI;

  const { accountDetail, dashboard, accessToken } = getState();
  const selectedCSPA = accountDetail[storeId]?.data || {};

  const { clientID, agentId, sysId, principalId } = selectedCSPA;

  const accountToken =
    dashboard.queueAccountToken ||
    accessToken[storeId]?.accountInfo?.token ||
    '';

  const requestBody: GetAccountLockRequest = {
    common: {
      clientNumber: clientID,
      system: sysId,
      prin: principalId,
      agent: agentId,
      app,
      user
    },
    accountToken: accountToken
  };
  try {
    const response = await dashboardServices.getAccountExtendLockTime(
      requestBody
    );

    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'success',
        message: 'txt_account_lock_extend_success_msg'
      })
    );

    return { data: response.data };
  } catch (error) {
    return rejectWithValue({ errorMessage: error });
  }
});

export const getAccountExtendLockTimeBuilder = (
  builder: ActionReducerMapBuilder<DashboardState>
) => {
  builder.addCase(getAccountExtendLockTime.pending, draftState => {
    draftState.extendLockTime = 0;
  });
  builder.addCase(
    getAccountExtendLockTime.fulfilled,
    (draftState, action: PayloadAction<MagicKeyValue>) => {
      const payload = action.payload;
      draftState.extendLockTime = payload?.data?.configuredLockTimeInterval;
    }
  );
  builder.addCase(getAccountExtendLockTime.rejected, draftState => {
    draftState.extendLockTime = 0;
  });
};
