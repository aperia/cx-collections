import './reducers';
import { createStore, Store } from '@reduxjs/toolkit';

import { getDashboardWorkQueue } from './getDashboardWorkQueue';

import { dashboardServices } from '../dashboardServices';

import { responseDefault } from 'app/test-utils';

import { rootReducer } from 'storeConfig';

let store: Store<RootState>;
let state: RootState;

let spy: jest.SpyInstance;

describe('should have test getDashboardWorkQueue', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });
  beforeEach(() => {
    store = createStore(rootReducer, {});
    state = store.getState();
  });
  it('Should return data', async () => {
    spy = jest
      .spyOn(dashboardServices, 'getDashboardWorkQueue')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          collectorPromisesStats: {},
          collectorQueueStats: {},
          totalQueues: ''
        }
      });

    store = createStore(rootReducer, {
      mapping: {
        data: {
          dashboardWorkQueue: {}
        }
      }
    });

    const response = await getDashboardWorkQueue({ period: 'period' })(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig['extra']
    );

    expect(response.type).toEqual('dashboard/getDashboardWorkQueue/fulfilled');
  });

  it('Should return data default value mapping', async () => {
    spy = jest
      .spyOn(dashboardServices, 'getDashboardWorkQueue')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          collectorPromisesStats: {},
          collectorQueueStats: {},
          totalQueues: ''
        }
      });

    store = createStore(rootReducer, {
      mapping: {
        data: {
          dashboardWorkQueue: undefined
        }
      }
    });

    const response = await getDashboardWorkQueue({ period: 'period' })(
      store.dispatch,
      store.getState,
      undefined as unknown as ThunkAPIConfig['extra']
    );

    expect(response.type).toEqual('dashboard/getDashboardWorkQueue/fulfilled');
  });

  it('Should return error', async () => {
    spy = jest
      .spyOn(dashboardServices, 'getDashboardWorkQueue')
      .mockRejectedValue({
        ...responseDefault,
        data: {
          collectorPromisesStats: {},
          collectorQueueStats: {},
          totalQueues: ''
        }
      });

    const response = await getDashboardWorkQueue()(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('dashboard/getDashboardWorkQueue/rejected');
  });

  it('should response getDashboardWorkQueue builder', () => {
    const pendingAction = getDashboardWorkQueue.pending(
      getDashboardWorkQueue.pending.type,
      undefined
    );
    const actual = rootReducer(state, pendingAction);

    expect(actual?.dashboard?.workQueue?.loading).toEqual(true);
  });

  it('should response getDashboardWorkQueue fulfilled', () => {
    const fulfilled = getDashboardWorkQueue.fulfilled(
      {
        data: {
          totalQueues: 'totalQueues',
          collectorQueueStats: 'collectorQueueStats',
          collectorPromiseStats: 'collectorPromiseStats'
        }
      },
      getDashboardWorkQueue.fulfilled.type,
      undefined
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual?.dashboard?.workQueue?.loading).toEqual(false);
    expect(actual?.dashboard?.workQueue?.data).toEqual({
      totalQueues: 'totalQueues',
      collectorQueueStats: 'collectorQueueStats',
      collectorPromiseStats: 'collectorPromiseStats'
    });
  });

  it('should response getDashboardWorkQueue rejected', () => {
    const rejected = getDashboardWorkQueue.rejected(
      null,
      getDashboardWorkQueue.rejected.type,
      undefined
    );
    const actual = rootReducer(state, rejected);

    expect(actual?.dashboard?.workQueue?.loading).toEqual(false);
  });
});
