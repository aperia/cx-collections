import { rootReducer } from 'storeConfig';
import { createStore } from '@reduxjs/toolkit';

import { getAccountLockStatusInquiry } from './getAccountLockStatusInquiry';

import { dashboardServices } from '../dashboardServices';

import { responseDefault } from 'app/test-utils';

let state: RootState;

const mockParams = {
  accountId: 'accountId',
  clientNumber: '0000',
  system: '0000',
  agent: '0000',
  prin: '0000'
};

let spy: jest.SpyInstance;

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    org: 'org',
    app: 'app',
    user: 'user'
  }
};

describe('Test getAccountLockStatusInquiry', () => {
  afterEach(() => {
    spy.mockReset();
    spy.mockRestore();
  });

  const mockApiServiceError = (response?: MagicKeyValue) => {
    spy = jest
      .spyOn(dashboardServices, 'getAccountLockStatusInquiry')
      .mockRejectedValue({
        ...responseDefault,
        ...response
      });
  };

  const mockApiService = (response?: MagicKeyValue) => {
    spy = jest
      .spyOn(dashboardServices, 'getAccountLockStatusInquiry')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          responseStatus: 'success',
          configuredLockTimeInterval: '30'
        }
      });
  };

  it('should fulfilled', async () => {
    mockApiService();

    const store = createStore(rootReducer, {} as any);

    const response = await getAccountLockStatusInquiry(mockParams)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'dashboard/getAccountLockStatusInquiry/fulfilled'
    );
    expect(response?.payload).toEqual({
      data: { configuredLockTimeInterval: '30', responseStatus: 'success' }
    });
  });

  it('should rejected', async () => {
    mockApiServiceError();

    const store = createStore(rootReducer, {} as any);
    const response = await getAccountLockStatusInquiry(mockParams)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'dashboard/getAccountLockStatusInquiry/rejected'
    );
    expect(response?.payload).toEqual({
      errorMessage: {
        config: {},
        data: null,
        headers: {},
        status: 200,
        statusText: ''
      }
    });
  });

  it('should response getAccountLockStatusInquiry builder', () => {
    const pendingAction = getAccountLockStatusInquiry.pending(
      getAccountLockStatusInquiry.pending.type,
      mockParams
    );
    const actual = rootReducer(state, pendingAction);

    expect(actual?.dashboard?.accountLockTime).toEqual(0);
  });

  it('should response getAccountLockStatusInquiry fulfilled', () => {
    const fulfilled = getAccountLockStatusInquiry.fulfilled(
      { data: { configuredLockTimeInterval: 'test' } },
      getAccountLockStatusInquiry.fulfilled.type,
      mockParams
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual?.dashboard?.accountLockTime).toBeTruthy();
  });

  it('should response getAccountLockStatusInquiry rejected', () => {
    const rejected = getAccountLockStatusInquiry.rejected(
      null,
      getAccountLockStatusInquiry.rejected.type,
      mockParams
    );
    const actual = rootReducer(state, rejected);

    expect(actual?.dashboard?.accountLockTime).toEqual(0);
  });
});
