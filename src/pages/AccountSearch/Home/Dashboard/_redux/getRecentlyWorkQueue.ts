import { RecentlyWorkQueuePayload } from './../types';
import { QUEUE_TYPE_INFO } from './../constants';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { dashboardServices } from '../dashboardServices';
import {
  DashboardRequest,
  DashboardState,
  GetRecentlyWorkQueuePayload
} from '../types';

import { mappingDataFromObj } from 'app/helpers';
import { randomQueueType } from '../helpers';

export const getRecentlyWorkQueue = createAsyncThunk<
  GetRecentlyWorkQueuePayload,
  undefined,
  ThunkAPIConfig
>('dashboard/getRecentlyWorkQueue', async (args, thunkAPI) => {
  const { mapping } = thunkAPI.getState();
  const requestBody: DashboardRequest = {
    resultSetSize: '4'
  };

  const mappingData = mapping.data.dashboardRecentWorkQueue;

  const { data } = await dashboardServices.getRecentlyWorkQueue(requestBody);

  const formatData = mappingDataFromObj(data, mappingData || {});

  // Mock add queue type information
  const combineData = formatData.collectorRecentWorkQueueList.map(
    (item: RecentlyWorkQueuePayload) => ({
      ...item,
      ...QUEUE_TYPE_INFO,
      queueType: randomQueueType(Math.round(Math.random()))
    })
  );
  formatData.collectorRecentWorkQueueList = [...combineData];

  return { data: formatData };
});

export const getRecentlyWorkQueueBuilder = (
  builder: ActionReducerMapBuilder<DashboardState>
) => {
  builder.addCase(getRecentlyWorkQueue.pending, (draftState, action) => {
    draftState.recentWorkQueue = {
      ...draftState.recentWorkQueue,
      loading: true
    };
  });
  builder.addCase(getRecentlyWorkQueue.fulfilled, (draftState, action) => {
    const payload = action.payload;

    draftState.recentWorkQueue = {
      ...draftState.recentWorkQueue,
      data: payload?.data,
      loading: false
    };
  });
  builder.addCase(getRecentlyWorkQueue.rejected, (draftState, action) => {
    draftState.recentWorkQueue = {
      ...draftState.recentWorkQueue,
      loading: false,
      error: action.payload?.errorMessage
    };
  });
};
