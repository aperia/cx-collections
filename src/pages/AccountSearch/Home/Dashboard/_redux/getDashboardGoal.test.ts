import { createStore, Store } from '@reduxjs/toolkit';

import { getDashboardGoal } from './getDashboardGoal';

import { dashboardServices } from '../dashboardServices';

import { responseDefault } from 'app/test-utils';

import { rootReducer } from 'storeConfig';

const mockArgs = {
  userType: 'CXCOL5'
};

let store: Store<RootState>;
let state: RootState;

beforeEach(() => {
  store = createStore(rootReducer, {});
  state = store.getState();
});

let spy1: jest.SpyInstance;

afterEach(() => {
  spy1?.mockReset();
  spy1?.mockRestore();
});

describe('should have test getDashboardGoal', () => {
  it('Should return data with userType === user', async () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app'
      }
    };
    spy1 = jest.spyOn(dashboardServices, 'getDashboardGoal').mockResolvedValue({
      ...responseDefault,
      data: {
        user: 'user',
        admin: 'admin'
      }
    });

    const response = await getDashboardGoal(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      data: 'user'
    });
  });

  it('Should return data with userType === user > case 2', async () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: undefined
    };
    spy1 = jest.spyOn(dashboardServices, 'getDashboardGoal').mockResolvedValue({
      ...responseDefault,
      data: {
        user: 'user',
        admin: 'admin'
      }
    });

    const response = await getDashboardGoal(mockArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      data: 'user'
    });
  });

  it('Should return data with userType === admin', async () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app',
        isAdminRole: true
      }
    };
    
    spy1 = jest.spyOn(dashboardServices, 'getDashboardGoal').mockResolvedValue({
      ...responseDefault,
      data: {
        user: 'user',
        admin: 'admin'
      }
    });

    const response = await getDashboardGoal({ userType: 'CXADMN1' })(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      data: 'admin'
    });
  });

  it('should response getDashboardGoal builder', () => {
    const pendingAction = getDashboardGoal.pending(
      getDashboardGoal.pending.type,
      mockArgs
    );
    const actual = rootReducer(state, pendingAction);

    expect(actual?.dashboard?.goal?.loading).toEqual(true);
  });

  it('should response getDashboardGoal fulfilled', () => {
    const fulfilled = getDashboardGoal.fulfilled(
      { data: [] },
      getDashboardGoal.fulfilled.type,
      mockArgs
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual?.dashboard?.goal?.loading).toEqual(false);
    expect(actual?.dashboard?.goal?.data).toEqual([]);
  });

  it('should response getDashboardGoal rejected', () => {
    const rejected = getDashboardGoal.rejected(
      null,
      getDashboardGoal.rejected.type,
      mockArgs
    );
    const actual = rootReducer(state, rejected);

    expect(actual?.dashboard?.goal?.loading).toEqual(false);
  });
});
