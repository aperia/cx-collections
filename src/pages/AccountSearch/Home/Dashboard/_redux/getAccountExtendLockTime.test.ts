import { rootReducer } from 'storeConfig';
import { createStore } from '@reduxjs/toolkit';

import { getAccountExtendLockTime } from './getAccountExtendLockTime';

import { dashboardServices } from '../dashboardServices';

import { responseDefault } from 'app/test-utils';

let state: RootState;

const mockParams = {
  accountId: 'accountId'
};

let spy: jest.SpyInstance;

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    org: 'org',
    app: 'app',
    user: 'user'
  }
};

describe('Test getAccountExtendLockTime', () => {
  afterEach(() => {
    spy.mockReset();
    spy.mockRestore();
  });

  const mockApiServiceError = (response?: MagicKeyValue) => {
    spy = jest
      .spyOn(dashboardServices, 'getAccountExtendLockTime')
      .mockRejectedValue({
        ...responseDefault,
        ...response
      });
  };

  const mockApiService = (response?: MagicKeyValue) => {
    spy = jest
      .spyOn(dashboardServices, 'getAccountExtendLockTime')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          responseStatus: 'success',
          configuredLockTimeInterval: '30'
        }
      });
  };

  it('should fulfilled', async () => {
    mockApiService();

    const store = createStore(rootReducer, {} as any);

    const response = await getAccountExtendLockTime(mockParams)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'dashboard/getAccountExtendLockTime/fulfilled'
    );
    expect(response?.payload).toEqual({
      data: { configuredLockTimeInterval: '30', responseStatus: 'success' }
    });
  });

  it('should rejected', async () => {
    mockApiServiceError();

    const store = createStore(rootReducer, {} as any);
    const response = await getAccountExtendLockTime({})(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'dashboard/getAccountExtendLockTime/rejected'
    );
    expect(response?.payload).toEqual({
      errorMessage: {
        config: {},
        data: null,
        headers: {},
        status: 200,
        statusText: ''
      }
    });
  });

  it('should response getAccountExtendLockTime builder', () => {
    const pendingAction = getAccountExtendLockTime.pending(
      getAccountExtendLockTime.pending.type,
      mockParams
    );
    const actual = rootReducer(state, pendingAction);

    expect(actual?.dashboard?.extendLockTime).toEqual(0);
  });

  it('should response getAccountExtendLockTime fulfilled', () => {
    const fulfilled = getAccountExtendLockTime.fulfilled(
      { data: { configuredLockTimeInterval: 'test' } },
      getAccountExtendLockTime.fulfilled.type,
      mockParams
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual?.dashboard?.extendLockTime).toBeTruthy();
  });

  it('should response getAccountExtendLockTime rejected', () => {
    const rejected = getAccountExtendLockTime.rejected(
      null,
      getAccountExtendLockTime.rejected.type,
      mockParams
    );
    const actual = rootReducer(state, rejected);

    expect(actual?.dashboard?.extendLockTime).toEqual(0);
  });
});
