import './reducers';
import { createStore, Store } from '@reduxjs/toolkit';

import {
  getNextAccountWorkOnQueue,
  triggerNextAccountWorkOnQueue
} from './getNextAccountWorkOnQueue';

import accountDetailService from 'pages/AccountDetails/accountDetailService';
import { dashboardServices } from '../dashboardServices';

import {
  byPassFulfilled,
  byPassRejected,
  mockActionCreator,
  responseDefault
} from 'app/test-utils';

import { rootReducer } from 'storeConfig';
import { dashboardActions } from './reducers';
import { formatTime } from 'app/helpers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { DASHBOARD_GET_NEXT_ACCOUNT } from '../constants';
import { IQueueAccount, QueueTypeEnum } from '../QueueDetail/types';

let store: Store<RootState>;

let spy: jest.SpyInstance;
let spy1: jest.SpyInstance;
let spy2: jest.SpyInstance;

const dashboardActionsSpy = mockActionCreator(dashboardActions);
const toastActionsSpy = mockActionCreator(actionsToast);
describe('should have test getNextAccountWorkOnQueue', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();

    spy1?.mockReset();
    spy1?.mockRestore();

    spy2?.mockReset();
    spy2?.mockRestore();
  });

  beforeEach(() => {
    store = createStore(rootReducer, {
      mapping: {
        data: {
          dashboardRecentWorkQueue: {}
        }
      }
    } as any);
  });

  window.appConfig = {
    ...window.appConfig,
    commonConfig: {
      ...window.appConfig?.commonConfig,
      callingApplication: '',
      org: 'org',
      app: 'app'
    }
  };

  it(`Should return data with queueType === ${QueueTypeEnum.PULL_QUEUE}`, async () => {
    spy = jest
      .spyOn(dashboardServices, 'getNextAccountWorkOnQueue')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          accountId: JSON.stringify({
            eValue: 'eValue',
            maskedValue: 'maskedValue'
          }),
          previousAccount: undefined,
          currentAccount: {
            accountNumber: '123'
          } as IQueueAccount
        }
      });

    const response = await getNextAccountWorkOnQueue({
      requestBody: {
        common: {
          org: 'FDES',
          app: 'cx'
        },
        queueId: '162672832060',
        nextWorkDate: formatTime(new Date().toString()).utcDate!
      },
      queueType: QueueTypeEnum.PULL_QUEUE,
      accountNumber: '123'
    })(store.dispatch, store.getState, undefined as unknown as ThunkAPIConfig);

    expect(response.type).toEqual(
      'dashboard/getNextAccountWorkOnQueue/fulfilled'
    );
  });

  it(`Should return data with queueType === ${QueueTypeEnum.PUSH_QUEUE}`, async () => {
    spy = jest
      .spyOn(dashboardServices, 'getNextAccountWorkOnQueue')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          accountId: JSON.stringify({
            eValue: 'eValue',
            maskedValue: 'maskedValue'
          }),
          previousAccount: undefined,
          currentAccount: {
            accountNumber: '123'
          } as IQueueAccount
        }
      });

    const response = await getNextAccountWorkOnQueue({
      requestBody: {
        common: {
          org: 'FDES',
          app: 'cx'
        },
        queueId: '162672832060',
        nextWorkDate: formatTime(new Date().toString()).utcDate!
      },
      queueType: QueueTypeEnum.PUSH_QUEUE,
      accountNumber: '123'
    })(store.dispatch, store.getState, undefined as unknown as ThunkAPIConfig);

    expect(response.type).toEqual(
      'dashboard/getNextAccountWorkOnQueue/fulfilled'
    );
  });

  it('Should return non data', async () => {
    const getNextAccountWorkOnQueue = dashboardActionsSpy(
      'getNextAccountWorkOnQueue'
    );

    spy = jest
      .spyOn(dashboardServices, 'getNextAccountWorkOnQueue')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          accountId: JSON.stringify({ eValue: '123', maskedValue: '123' })
        }
      });
    spy2 = jest
      .spyOn(accountDetailService, 'getAccountDetails')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    await triggerNextAccountWorkOnQueue({
      queueId: 'queueId',
      queueType: QueueTypeEnum.PULL_QUEUE,
      accountNumber: '123'
    })(
      byPassFulfilled(triggerNextAccountWorkOnQueue.fulfilled.type, {
        data: JSON.stringify({ eValue: 'eValue', maskedValue: 'maskedValue' })
      }),
      store.getState,
      {}
    );
    expect(getNextAccountWorkOnQueue).toBeCalled();
  });

  it('Should return data with failure status', async () => {
    const getNextAccountWorkOnQueue = dashboardActionsSpy(
      'getNextAccountWorkOnQueue'
    );

    spy = jest
      .spyOn(dashboardServices, 'getNextAccountWorkOnQueue')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          accountId: JSON.stringify({ eValue: '123', maskedValue: '123' })
        }
      });
    spy2 = jest
      .spyOn(accountDetailService, 'getAccountDetails')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    await triggerNextAccountWorkOnQueue({
      queueId: 'queueId',
      inAccountDetail: true
    })(
      byPassFulfilled(triggerNextAccountWorkOnQueue.fulfilled.type, {
        data: {
          eValue: 'eValue',
          maskedValue: 'maskedValue',
          status: 'failure'
        }
      }),
      store.getState,
      {}
    );
    expect(getNextAccountWorkOnQueue).toBeCalled();
  });

  it('Should reject inAccountDetail', async () => {
    const addToast = toastActionsSpy('addToast');
    spy = jest
      .spyOn(dashboardServices, 'getNextAccountWorkOnQueue')
      .mockRejectedValue(new Error('Async'));
    spy2 = jest
      .spyOn(accountDetailService, 'getAccountDetails')
      .mockRejectedValue(new Error('Async'));

    await triggerNextAccountWorkOnQueue({
      queueId: 'queueId',
      inAccountDetail: true
    })(
      byPassRejected(triggerNextAccountWorkOnQueue.rejected.type),
      store.getState,
      {}
    );
    expect(addToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: DASHBOARD_GET_NEXT_ACCOUNT.NEXT_ACCOUNT_FAILED_TO_RETRIEVE
    });
  });

  it('Should reject inAccountDetail', async () => {
    const addToast = toastActionsSpy('addToast');
    spy = jest
      .spyOn(dashboardServices, 'getNextAccountWorkOnQueue')
      .mockRejectedValue(new Error('Async'));
    spy2 = jest
      .spyOn(accountDetailService, 'getAccountDetails')
      .mockRejectedValue(new Error('Async'));

    await triggerNextAccountWorkOnQueue({
      queueId: 'queueId',
      inAccountDetail: true,
      queueType: QueueTypeEnum.PULL_QUEUE,
      accountNumber: '123'
    })(
      byPassRejected(triggerNextAccountWorkOnQueue.rejected.type),
      store.getState,
      {}
    );
    expect(addToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: DASHBOARD_GET_NEXT_ACCOUNT.NEXT_ACCOUNT_FAILED_TO_RETRIEVE
    });
  });

  it('Should reject inQueueDetail', async () => {
    const addToast = toastActionsSpy('addToast');
    spy = jest
      .spyOn(dashboardServices, 'getNextAccountWorkOnQueue')
      .mockRejectedValue(new Error('Async'));
    spy2 = jest
      .spyOn(accountDetailService, 'getAccountDetails')
      .mockRejectedValue(new Error('Async'));

    await triggerNextAccountWorkOnQueue({
      queueId: 'queueId',
      inAccountDetail: false,
      queueType: QueueTypeEnum.PULL_QUEUE,
      accountNumber: '123'
    })(
      byPassRejected(triggerNextAccountWorkOnQueue.rejected.type),
      store.getState,
      {}
    );
    expect(addToast).toBeCalledWith({
      show: true,
      type: 'error',
      message: DASHBOARD_GET_NEXT_ACCOUNT.ACCOUNT_FAILED_TO_RETRIEVE
    });
  });

  it(`Should return inQueueDetail with queueType === ${QueueTypeEnum.PULL_QUEUE}`, async () => {
    spy = jest
      .spyOn(dashboardServices, 'getNextAccountWorkOnQueue')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          accountId: JSON.stringify({
            eValue: 'eValue',
            maskedValue: 'maskedValue'
          })
        }
      });

    spy2 = jest
      .spyOn(accountDetailService, 'getAccountDetails')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    await triggerNextAccountWorkOnQueue({
      queueId: 'queueId',
      inQueueDetail: true,
      queueType: QueueTypeEnum.PULL_QUEUE,
      accountNumber: '123'
    })(
      byPassFulfilled(triggerNextAccountWorkOnQueue.fulfilled.type, {
        data: {
          accountId: JSON.stringify({
            eValue: 'eValue',
            maskedValue: 'maskedValue'
          }),
          previousAccount: undefined,
          currentAccount: {
            accountNumber: '123'
          } as IQueueAccount
        }
      }),
      store.getState,
      {}
    );
  });

  it(`Should return inQueueDetail with queueType === ${QueueTypeEnum.PUSH_QUEUE}`, async () => {
    spy = jest
      .spyOn(dashboardServices, 'getNextAccountWorkOnQueue')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          accountId: JSON.stringify({
            eValue: 'eValue',
            maskedValue: 'maskedValue'
          })
        }
      });

    spy2 = jest
      .spyOn(accountDetailService, 'getAccountDetails')
      .mockResolvedValue({
        ...responseDefault,
        data: {}
      });

    await triggerNextAccountWorkOnQueue({
      queueId: 'queueId',
      inQueueDetail: true,
      accountNumber: '123'
    })(
      byPassFulfilled(triggerNextAccountWorkOnQueue.fulfilled.type, {
        data: {
          accountId: JSON.stringify({
            eValue: 'eValue',
            maskedValue: 'maskedValue'
          })
        }
      }),
      store.getState,
      {}
    );
  });
});
