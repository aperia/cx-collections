import { createStore, Store } from '@reduxjs/toolkit';

import { getDashboardStatistics } from './getDashboardStatistics';

import { dashboardServices } from '../dashboardServices';

import { responseDefault } from 'app/test-utils';

import { rootReducer } from 'storeConfig';

const mockUserArgs = {
  userType: 'CXCOL5',
  period: 'today'
};

const mockAdminArgs = {
  userType: 'CXADMN1',
  period: 'today'
};

let store: Store<RootState>;
let state: RootState;

beforeEach(() => {
  store = createStore(rootReducer, {
    mapping: {
      data: {
        dashboardWorkStatistic: {}
      }
    }
  } as any);
  state = store.getState();
});

let spy1: jest.SpyInstance;

afterEach(() => {
  spy1?.mockReset();
  spy1?.mockRestore();
});

describe('should have test getDashboardStatistics', () => {
  it('Should return data with admin role', async () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app',
        isAdminRole: true
      }
    };
    spy1 = jest
      .spyOn(dashboardServices, 'getDashboardSupervisorWorkStatistics')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          admin: {
            today: {
              item: []
            }
          }
        }
      });

    const response = await getDashboardStatistics(mockAdminArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('dashboard/getDashboardStatistics/fulfilled');
  });

  it('Should return data with mapping undefined with admin role', async () => {
    spy1 = jest
      .spyOn(dashboardServices, 'getDashboardSupervisorWorkStatistics')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          admin: {
            today: {
              item: []
            }
          }
        }
      });

    store = createStore(rootReducer, {
      mapping: {
        data: {
          dashboardWorkStatistic: undefined
        }
      }
    } as any);

    const response = await getDashboardStatistics(mockAdminArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('dashboard/getDashboardStatistics/fulfilled');
  });

  it('Should return data with user role', async () => {
    window.appConfig = {
      ...window.appConfig,
      commonConfig: {
        ...window.appConfig?.commonConfig,
        callingApplication: '',
        org: 'org',
        app: 'app',
        isAdminRole: false,
        isCollector: true
      }
    };
    spy1 = jest
      .spyOn(dashboardServices, 'getDashboardCollectorWorkStatistics')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          accountStats: 'accountStats',
          promiseStats: 'promiseStats'
        }
      });

    const response = await getDashboardStatistics(mockUserArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('dashboard/getDashboardStatistics/fulfilled');
  });

  it('Should return data with mapping undefined with user role', async () => {
    window.appConfig = {} as AppConfiguration;

    spy1 = jest
      .spyOn(dashboardServices, 'getDashboardCollectorWorkStatistics')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          accountStats: 'accountStats',
          promiseStats: 'promiseStats'
        }
      });

    store = createStore(rootReducer, {
      mapping: {
        data: {
          dashboardWorkStatistic: undefined
        }
      }
    } as any);

    const response = await getDashboardStatistics(mockUserArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('dashboard/getDashboardStatistics/fulfilled');
  });

  it('should response getDashboardStatistics builder', () => {
    const pendingAction = getDashboardStatistics.pending(
      getDashboardStatistics.pending.type,
      mockUserArgs
    );
    const actual = rootReducer(state, pendingAction);

    expect(actual?.dashboard?.workStatistic?.loading).toEqual(true);
  });

  it('should response getDashboardStatistics fulfilled', () => {
    const fulfilled = getDashboardStatistics.fulfilled(
      { data: [] },
      getDashboardStatistics.fulfilled.type,
      mockUserArgs
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual?.dashboard?.workStatistic?.loading).toEqual(false);
    expect(actual?.dashboard?.workStatistic?.data).toEqual([]);
  });

  it('should response getDashboardStatistics rejected', () => {
    const rejected = getDashboardStatistics.rejected(
      null,
      getDashboardStatistics.rejected.type,
      mockUserArgs
    );
    const actual = rootReducer(state, rejected);

    expect(actual?.dashboard?.workStatistic?.loading).toEqual(false);
  });
});
