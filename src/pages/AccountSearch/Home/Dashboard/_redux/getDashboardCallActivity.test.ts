import { createStore, Store } from '@reduxjs/toolkit';

import { getDashboardCallActivity } from './getDashboardCallActivity';

import { dashboardServices } from '../dashboardServices';

import { responseDefault } from 'app/test-utils';

import { rootReducer } from 'storeConfig';

const mockUserArgs = {
  userType: 'CXCOL5',
  period: 'today'
};

const mockAdminArgs = {
  userType: 'CXADMN1',
  period: 'today'
};

let store: Store<RootState>;
let state: RootState;

beforeEach(() => {
  store = createStore(rootReducer, {});
  state = store.getState();
});

let spy1: jest.SpyInstance;

afterEach(() => {
  spy1?.mockReset();
  spy1?.mockRestore();
});

describe('should have test getDashboardCallActivity', () => {
  it('Should return data with userType === user', async () => {
    spy1 = jest
      .spyOn(dashboardServices, 'getDashboardCallActivity')
      .mockResolvedValue({
        ...responseDefault,
        data: []
      });

    const response = await getDashboardCallActivity(mockUserArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      undefined
    });
  });

  it('Should return data with userType === admin', async () => {
    spy1 = jest
      .spyOn(dashboardServices, 'getDashboardCallActivity')
      .mockResolvedValue({
        ...responseDefault,
        data: []
      });

    const response = await getDashboardCallActivity(mockAdminArgs)(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.payload).toEqual({
      undefined
    });
  });

  it('should response getDashboardCallActivity builder', () => {
    const pendingAction = getDashboardCallActivity.pending(
      getDashboardCallActivity.pending.type,
      mockUserArgs
    );
    const actual = rootReducer(state, pendingAction);

    expect(actual?.dashboard?.callActivity?.loading).toEqual(true);
  });

  it('should response getDashboardCallActivity fulfilled', () => {
    const fulfilled = getDashboardCallActivity.fulfilled(
      { data: [] },
      getDashboardCallActivity.fulfilled.type,
      mockUserArgs
    );
    const actual = rootReducer(state, fulfilled);

    expect(actual?.dashboard?.callActivity?.loading).toEqual(false);
    expect(actual?.dashboard?.callActivity?.data).toEqual([]);
  });

  it('should response getDashboardCallActivity rejected', () => {
    const rejected = getDashboardCallActivity.rejected(
      null,
      getDashboardCallActivity.rejected.type,
      mockUserArgs
    );
    const actual = rootReducer(state, rejected);

    expect(actual?.dashboard?.callActivity?.loading).toEqual(false);
  });
});
