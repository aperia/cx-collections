import { QueueTypeEnum } from 'pages/AccountSearch/Home/Dashboard/QueueDetail/types';
import { createAsyncThunk, isFulfilled } from '@reduxjs/toolkit';
import accountDetailService from 'pages/AccountDetails/accountDetailService';
import { WorkType } from 'pages/TimerWorking/types';
import { dashboardServices } from '../dashboardServices';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import {
  GetNextAccountArgs,
  GetNextAccountRequest,
  GetNextAccountWorkOnQueuePayload,
  TriggerNextAccountWorkOnQueueArgs
} from '../types';
import { dashboardActions } from './reducers';
import { DASHBOARD_GET_NEXT_ACCOUNT } from '../constants';
import { openAccountDetail } from 'pages/AccountSearch/AccountSearchList/_redux/openAccountDetail';
import { queueListActions } from 'pages/QueueList/_redux/reducers';
import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';
import { formatTime } from 'app/helpers';
import { queueAccountsActions } from '../QueueDetail/_redux';
import queueAccountsServices from '../QueueDetail/queueAccountsServices';

export const getNextAccountWorkOnQueue = createAsyncThunk<
  GetNextAccountWorkOnQueuePayload,
  GetNextAccountArgs,
  ThunkAPIConfig
>('dashboard/getNextAccountWorkOnQueue', async args => {
  const { requestBody, queueType, accountNumber } = args;
  let result = {};
  const response = await dashboardServices.getNextAccountWorkOnQueue(
    requestBody
  );

  result = {
    ...response.data
  };

  if (queueType === QueueTypeEnum.PULL_QUEUE) {
    const nextPullQueueAccountResponse =
      await queueAccountsServices.getNextPullQueueAccount({
        accountNumber
      });

    const { previousAccount, currentAccount } =
      nextPullQueueAccountResponse.data;

    result = {
      ...result,
      previousAccount,
      currentAccount
    };
  }

  return {
    data: result
  };
});

export const triggerNextAccountWorkOnQueue = createAsyncThunk<
  void,
  TriggerNextAccountWorkOnQueueArgs,
  ThunkAPIConfig
>('dashboard/triggerGetNextAccountWorkOnQueue', async (args, { dispatch }) => {
  const {
    queueId,
    inAccountDetail,
    inQueueDetail,
    storeId = '',
    queueType,
    accountNumber
  } = args;

  const toggleLoadingAction = () => {
    if (inAccountDetail) {
      dispatch(accountDetailActions.toggleLoading({ storeId }));
    }

    if (inQueueDetail) {
      dispatch(
        queueAccountsActions.toggleLoading({
          storeId: queueId
        })
      );
    }

    dispatch(queueListActions.toggleLoading());
  };

  const requestBody: GetNextAccountRequest = {
    common: {
      org: window.appConfig.commonConfig.org,
      app: window.appConfig.commonConfig.app
    },
    queueId,
    nextWorkDate: formatTime(new Date().toString()).utcDate!
  };

  toggleLoadingAction();

  try {
    const response = await dispatch(
      dashboardActions.getNextAccountWorkOnQueue({
        requestBody,
        accountNumber,
        queueType
      })
    );

    const data = isFulfilled(response) && response?.payload?.data;

    dispatch(dashboardActions.setQueueAccountToken(data.accountToken || ''));

    const parsePushQueueData = JSON.parse(
      '{"maskedValue":"022009******2624","eValue":"VOL(KlkpelxuMEh5Yih5aU8qMmk3JQ==)"}'
    );
    let accountId = parsePushQueueData.eValue;

    if (queueType === QueueTypeEnum.PULL_QUEUE) {
      accountId = data.currentAccount.accountNumber;
      const { currentAccount, previousAccount } = data;
      dispatch(
        dashboardActions.setQueueAccount({ previousAccount, currentAccount })
      );
    }

    const responseAccountDetail = await accountDetailService.getAccountDetails({
      common: { accountId },
      selectFields: [
        'customerInquiry.customerName',
        'customerInquiry.socialSecurityIdentifier',
        'customerInquiry.memberSequenceIdentifier',
        'customerInquiry.clientIdentifier',
        'customerInquiry.systemIdentifier',
        'customerInquiry.principalIdentifier',
        'customerInquiry.agentIdentifier'
      ]
    });

    const {
      data: { customerInquiry = [] }
    } = responseAccountDetail;

    const { presentationInstrumentIdentifier = '{}' } =
      customerInquiry[0] || {};

    const parsePullQueueData = JSON.parse(presentationInstrumentIdentifier);

    const parseData =
      queueType === QueueTypeEnum.PULL_QUEUE
        ? parsePullQueueData
        : parsePushQueueData;

    await dispatch(
      openAccountDetail({
        queueId,
        eValue: parseData.eValue,
        primaryName: customerInquiry[0]?.customerName,
        memberSequenceIdentifier: customerInquiry[0]?.memberSequenceIdentifier,
        maskedValue: parseData.maskedValue,
        socialSecurityIdentifier: customerInquiry[0]?.socialSecurityIdentifier,
        clientIdentifier: customerInquiry[0]?.clientIdentifier,
        systemIdentifier: customerInquiry[0]?.systemIdentifier,
        principalIdentifier: customerInquiry[0]?.principalIdentifier,
        agentIdentifier: customerInquiry[0]?.agentIdentifier,
        type: WorkType.QUEUE,
        queueType
      })
    );
    toggleLoadingAction();
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: inAccountDetail
          ? DASHBOARD_GET_NEXT_ACCOUNT.NEXT_ACCOUNT_FAILED_TO_RETRIEVE
          : DASHBOARD_GET_NEXT_ACCOUNT.ACCOUNT_FAILED_TO_RETRIEVE
      })
    );
    toggleLoadingAction();
  }
});
