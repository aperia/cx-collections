import { createAsyncThunk } from '@reduxjs/toolkit';
import { dashboardServices } from '../dashboardServices';
import {
  GetAccountLockArg,
  GetAccountLockPayload,
  GetAccountLockRequest
} from '../types';

export const getAccountLockTimeUpdate = createAsyncThunk<
  GetAccountLockPayload,
  GetAccountLockArg,
  ThunkAPIConfig
>('dashboard/getAccountLockTimeUpdate', async (args, thunkAPI) => {
  const { acctWorkStatus, storeId = '' } = args;
  const { app, org } = window?.appConfig?.commonConfig;
  const { rejectWithValue, getState } = thunkAPI;
  const { accessToken } = getState();
  const accountToken = accessToken[storeId]?.accountInfo?.token || '';
  const requestBody: GetAccountLockRequest = {
    common: {
      app,
      org,
      accountId: accountToken
    },
    acctWorkStatus
  };
  try {
    const response = await dashboardServices.getAccountLockTimeUpdate(
      requestBody
    );

    return { data: response.data };
  } catch (error) {
    return rejectWithValue({ errorMessage: error });
  }
});
