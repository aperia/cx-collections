import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { dashboardServices } from '../dashboardServices';
import {
  DashboardRequest,
  DashboardState,
  GetDashboardStatisticsArgs,
  GetDashboardStatisticsPayload
} from '../types';

//helpers
import { mappingDataFromObj } from 'app/helpers';
import { formatPeriod } from '../helpers';

export const getDashboardStatistics = createAsyncThunk<
  GetDashboardStatisticsPayload,
  GetDashboardStatisticsArgs,
  ThunkAPIConfig
>('dashboard/getDashboardStatistics', async (args, thunkAPI) => {
  const { period } = args;
  const { isAdminRole } = window.appConfig.commonConfig || {};

  const { mapping } = thunkAPI.getState();

  const mappingData = mapping.data.dashboardWorkStatistic;

  const formatData = (inData: MagicKeyValue) => {
    return mappingDataFromObj(inData, mappingData || {});
  };

  const { startDateTime, endDateTime } = formatPeriod(period);

  const requestBody: DashboardRequest = {
    startDateTime: startDateTime,
    endDateTime: endDateTime
  };

  if (isAdminRole) {
    const {
      data: { admin }
    } = await dashboardServices.getDashboardSupervisorWorkStatistics();

    return { data: admin[period] };
  }

  const { data } = await dashboardServices.getDashboardCollectorWorkStatistics(
    requestBody
  );

  const dataMapping = formatData(data);

  return { data: dataMapping };
});

export const getDashboardStatisticsBuilder = (
  builder: ActionReducerMapBuilder<DashboardState>
) => {
  builder.addCase(getDashboardStatistics.pending, (draftState, action) => {
    draftState.workStatistic = {
      ...draftState.workStatistic,
      loading: true
    };
  });
  builder.addCase(getDashboardStatistics.fulfilled, (draftState, action) => {
    const payload = action.payload;

    draftState.workStatistic = {
      ...draftState.workStatistic,
      data: payload?.data,
      loading: false
    };
  });
  builder.addCase(getDashboardStatistics.rejected, (draftState, action) => {
    draftState.workStatistic = {
      ...draftState.workStatistic,
      loading: false,
      error: action.payload?.errorMessage
    };
  });
};
