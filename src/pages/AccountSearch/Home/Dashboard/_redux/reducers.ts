import { IQueueAccount } from './../QueueDetail/types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// types
import { AccountExtendLockTimePayload, DashboardState } from '../types';

//thunks
import { getDashboardGoal, getDashboardGoalBuilder } from './getDashboardGoal';
import {
  getDashboardStatistics,
  getDashboardStatisticsBuilder
} from './getDashboardStatistics';
import {
  getDashboardCallActivity,
  getDashboardCallActivityBuilder
} from './getDashboardCallActivity';

import {
  getDashboardWorkQueue,
  getDashboardWorkQueueBuilder
} from './getDashboardWorkQueue';

import {
  getDashboardCollector,
  getDashboardCollectorBuilder
} from './getDashboardCollector';

import {
  getRecentlyWorkQueue,
  getRecentlyWorkQueueBuilder
} from './getRecentlyWorkQueue';

import {
  getNextAccountWorkOnQueue,
  triggerNextAccountWorkOnQueue
} from './getNextAccountWorkOnQueue';

import {
  getAccountExtendLockTime,
  getAccountExtendLockTimeBuilder
} from './getAccountExtendLockTime';

import {
  getAccountLockStatusInquiry,
  getAccountLockStatusInquiryBuilder
} from './getAccountLockStatusInquiry';

import { getAccountLockTimeUpdate } from './getAccountLockTimeUpdate';

const { actions, reducer } = createSlice({
  name: 'dashboard',
  initialState: {
    workStatistic: {
      data: {
        accountStatistics: {},
        promiseStatistics: {}
      },
      loading: false,
      error: ''
    },
    workQueue: {
      data: {
        totalQueues: '',
        collectorQueueStats: {},
        collectorPromiseStats: {}
      },
      loading: false,
      error: ''
    },
    recentWorkQueue: {
      data: {
        collectorRecentWorkQueueList: []
      },
      loading: false,
      error: ''
    },
    accountLockTime: 0,
    accountCountTime: {},
    queueAccountToken: '',
    extendLockTime: 0,
    resetTime: false,
    queueAccount: undefined
  } as DashboardState,
  reducers: {
    selectedPeriod: (draftState, action: PayloadAction<RefDataValue>) => {
      draftState.workStatistic = {
        ...draftState.workStatistic,
        selectedPeriod: action.payload
      };
    },
    setPeriodCallActivity: (
      draftState,
      action: PayloadAction<{ value: string }>
    ) => {
      const { value } = action.payload;

      draftState.callActivity = {
        ...draftState.callActivity,
        period: value
      };
    },
    selectedCollectorPeriod: (
      draftState,
      action: PayloadAction<RefDataValue>
    ) => {
      draftState.collector = {
        ...draftState.collector,
        selectedPeriod: action.payload
      };
    },
    setQueueAccountToken: (draftState, action: PayloadAction<string>) => {
      draftState.queueAccountToken = action.payload;
    },
    setQueueAccount: (
      draftState,
      action: PayloadAction<{
        previousAccount?: IQueueAccount;
        currentAccount: IQueueAccount;
      }>
    ) => {
      draftState.queueAccount = action.payload;
    },
    resetExtendLockTime: draftState => {
      draftState.extendLockTime = 0;
    },
    resetTime: (draftState, action: PayloadAction<boolean>) => {
      draftState.resetTime = action.payload;
    },
    toggleExtendModal: (
      draftState,
      action: PayloadAction<AccountExtendLockTimePayload>
    ) => {
      const { storeId } = action.payload;
      const toggleExtendModal =
        !!draftState?.accountCountTime?.[storeId]?.toggleExtendModal;

      draftState.accountCountTime = {
        [storeId]: {
          ...draftState.accountCountTime?.[storeId],
          toggleExtendModal: !toggleExtendModal
        }
      };
    }
  },
  extraReducers: builder => {
    getDashboardGoalBuilder(builder);
    getDashboardStatisticsBuilder(builder);
    getDashboardWorkQueueBuilder(builder);
    getDashboardCallActivityBuilder(builder);
    getDashboardCollectorBuilder(builder);
    getRecentlyWorkQueueBuilder(builder);
    getAccountExtendLockTimeBuilder(builder);
    getAccountLockStatusInquiryBuilder(builder);
  }
});

const allActions = {
  ...actions,
  getDashboardGoal,
  getDashboardStatistics,
  getDashboardCallActivity,
  getDashboardWorkQueue,
  getRecentlyWorkQueue,
  getDashboardCollector,
  getNextAccountWorkOnQueue,
  triggerNextAccountWorkOnQueue,
  getAccountExtendLockTime,
  getAccountLockStatusInquiry,
  getAccountLockTimeUpdate
};

export { allActions as dashboardActions, reducer as dashboard };
