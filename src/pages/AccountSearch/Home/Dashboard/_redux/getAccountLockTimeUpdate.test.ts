import { rootReducer } from 'storeConfig';
import { createStore } from '@reduxjs/toolkit';

import { getAccountLockTimeUpdate } from './getAccountLockTimeUpdate';

import { dashboardServices } from '../dashboardServices';

import { responseDefault } from 'app/test-utils';

const mockParams = {
  accountId: 'accountId'
};

let spy: jest.SpyInstance;

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    org: 'org',
    app: 'app',
    user: 'user'
  }
};

describe('Test getAccountLockTimeUpdate', () => {
  afterEach(() => {
    spy.mockReset();
    spy.mockRestore();
  });

  const mockApiServiceError = (response?: MagicKeyValue) => {
    spy = jest
      .spyOn(dashboardServices, 'getAccountLockTimeUpdate')
      .mockRejectedValue({
        ...responseDefault,
        ...response
      });
  };

  const mockApiService = (response?: MagicKeyValue) => {
    spy = jest
      .spyOn(dashboardServices, 'getAccountLockTimeUpdate')
      .mockResolvedValue({
        ...responseDefault,
        data: {
          responseStatus: 'success',
          configuredLockTimeInterval: '30'
        }
      });
  };

  it('should fulfilled', async () => {
    mockApiService();

    const store = createStore(rootReducer, {});

    const response = await getAccountLockTimeUpdate(mockParams)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'dashboard/getAccountLockTimeUpdate/fulfilled'
    );
    expect(response?.payload).toEqual({
      data: { configuredLockTimeInterval: '30', responseStatus: 'success' }
    });
  });

  it('should rejected', async () => {
    mockApiServiceError();

    const store = createStore(rootReducer, {});
    const response = await getAccountLockTimeUpdate(mockParams)(
      store.dispatch,
      store.getState,
      {}
    );
    expect(response.type).toEqual(
      'dashboard/getAccountLockTimeUpdate/rejected'
    );
    expect(response?.payload).toEqual({
      errorMessage: {
        config: {},
        data: null,
        headers: {},
        status: 200,
        statusText: ''
      }
    });
  });
});
