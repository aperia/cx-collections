// types
import { DashboardState } from '../types';

//reducers
import { dashboard, dashboardActions } from './reducers';
import { storeId } from 'app/test-utils';
import { IQueueAccount } from '../QueueDetail/types';

describe('Test Client Configuration Code To Text Reducers', () => {
  const initialState: DashboardState = {
    callActivity: {
      data: [],
      period: '',
      error: false,
      loading: false
    },
    goal: {
      data: [],
      error: '',
      loading: false
    },
    workQueue: {
      data: [],
      error: '',
      loading: false
    },
    workStatistic: {
      data: [],
      error: '',
      selectedPeriod: '',
      loading: false
    },
    accountCountTime: {
      [storeId]: { toggleExtendModal: false }
    }
  };

  it('should setPeriodCallActivity', () => {
    const state = dashboard(
      initialState,
      dashboardActions.setPeriodCallActivity({
        value: 'month'
      })
    );
    expect(state?.callActivity?.period).toEqual('month');
  });

  it('should selectedPeriod', () => {
    const state = dashboard(
      initialState,
      dashboardActions.selectedPeriod('month')
    );
    expect(state?.workStatistic?.selectedPeriod).toEqual('month');
  });

  it('should selectedCollectorPeriod', () => {
    const state = dashboard(
      initialState,
      dashboardActions.selectedCollectorPeriod('month')
    );
    expect(state?.collector?.selectedPeriod).toEqual('month');
  });

  it('toggleExtendModal', () => {
    const params = {
      storeId
    };
    const state = dashboard(
      initialState,
      dashboardActions.toggleExtendModal(params)
    );
    expect(state).toBeTruthy();
  });

  it('setQueueAccountToken', () => {
    const params = '123';
    const state = dashboard(
      initialState,
      dashboardActions.setQueueAccountToken(params)
    );
    expect(state.queueAccountToken).toEqual('123');
  });

  it('setQueueAccount', () => {
    const params = {
      previousAccount: undefined,
      currentAccount: {
        accountNumber: '123'
      } as IQueueAccount
    };
    const state = dashboard(
      initialState,
      dashboardActions.setQueueAccount(params)
    );
    expect(state.queueAccount).toEqual(expect.objectContaining(params));
  });

  it('resetExtendLockTime', () => {
    const state = dashboard(
      initialState,
      dashboardActions.resetExtendLockTime()
    );
    expect(state.extendLockTime).toEqual(0);
  });

  it('resetTime', () => {
    const state = dashboard(initialState, dashboardActions.resetTime(true));
    expect(state.resetTime).toEqual(true);
  });
});
