import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// services
import { dashboardServices } from '../dashboardServices';

// type
import {
  DashboardState,
  GetDashboardCollectorArgs,
  GetDashboardCollectorPayload
} from '../types';

export const getDashboardCollector = createAsyncThunk<
  GetDashboardCollectorPayload,
  GetDashboardCollectorArgs,
  ThunkAPIConfig
>('dashboard/getDashboardCollector', async (args, thunkAPI) => {
  const { period } = args;
  const { data } = await dashboardServices.getDashboardCollector();

  return { data: data['CXCOL1'][period] };
});

export const getDashboardCollectorBuilder = (
  builder: ActionReducerMapBuilder<DashboardState>
) => {
  builder.addCase(getDashboardCollector.pending, (draftState, action) => {
    draftState.collector = {
      ...draftState.collector,
      loading: true
    };
  });
  builder.addCase(getDashboardCollector.fulfilled, (draftState, action) => {
    const payload = action.payload;

    draftState.collector = {
      ...draftState.collector,
      data: payload?.data,
      loading: false
    };
  });
  builder.addCase(getDashboardCollector.rejected, (draftState, action) => {
    draftState.collector = {
      ...draftState.collector,
      loading: false,
      error: action.payload?.errorMessage
    };
  });
};
