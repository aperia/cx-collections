import {
  ActionReducerMapBuilder,
  createAsyncThunk,
  PayloadAction
} from '@reduxjs/toolkit';
import { dashboardServices } from '../dashboardServices';
import {
  DashboardState,
  GetAccountLockArg,
  GetAccountLockPayload,
  GetAccountLockRequest
} from '../types';

export const getAccountLockStatusInquiry = createAsyncThunk<
  GetAccountLockPayload,
  GetAccountLockArg,
  ThunkAPIConfig
>('dashboard/getAccountLockStatusInquiry', async (args, thunkAPI) => {
  const { storeId = '', relatedStoreId = '', accEValue = '' } = args;
  const { app, user } = window?.appConfig?.commonConfig;
  const { rejectWithValue, getState } = thunkAPI;
  const { accountDetail } = getState();
  const selectedCaller =
    accountDetail[storeId]?.selectedCaller ||
    accountDetail[relatedStoreId]?.selectedCaller ||
    {};
  const {
    clientIdentifier = '',
    systemIdentifier = '',
    principalIdentifier = '',
    agentIdentifier = ''
  } = selectedCaller;

  const requestBody: GetAccountLockRequest = {
    common: {
      clientNumber: clientIdentifier,
      system: systemIdentifier,
      prin: principalIdentifier,
      agent: agentIdentifier,
      app,
      user,
      accountId: accEValue
    }
  };

  try {
    const response = await dashboardServices.getAccountLockStatusInquiry(
      requestBody
    );

    return {
      data: response.data
    };
  } catch (error) {
    return rejectWithValue({ errorMessage: error });
  }
});

export const getAccountLockStatusInquiryBuilder = (
  builder: ActionReducerMapBuilder<DashboardState>
) => {
  builder.addCase(getAccountLockStatusInquiry.pending, draftState => {
    draftState.accountLockTime = 0;
  });
  builder.addCase(
    getAccountLockStatusInquiry.fulfilled,
    (draftState, action: PayloadAction<MagicKeyValue>) => {
      const payload = action.payload;

      draftState.accountLockTime = payload?.data?.configuredLockTimeInterval;
    }
  );
  builder.addCase(getAccountLockStatusInquiry.rejected, draftState => {
    draftState.accountLockTime = 0;
  });
};
