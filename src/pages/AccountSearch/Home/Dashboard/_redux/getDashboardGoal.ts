import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// Service
import { dashboardServices } from '../dashboardServices';

// Type
import {
  DashboardState,
  GetDashboardGoalArgs,
  GetDashboardGoalPayload
} from '../types';

export const getDashboardGoal = createAsyncThunk<
  GetDashboardGoalPayload,
  GetDashboardGoalArgs,
  ThunkAPIConfig
>('dashboard/getDashboardGoal', async (args, thunkAPI) => {
  const { isAdminRole } = window.appConfig.commonConfig || {};
  const {
    data: { admin, user }
  } = await dashboardServices.getDashboardGoal();

  if (isAdminRole) {
    return { data: admin };
  }

  return { data: user };
});

export const getDashboardGoalBuilder = (
  builder: ActionReducerMapBuilder<DashboardState>
) => {
  builder.addCase(getDashboardGoal.pending, (draftState, action) => {
    draftState.goal = {
      ...draftState?.goal,
      loading: true
    };
  });
  builder.addCase(getDashboardGoal.fulfilled, (draftState, action) => {
    const payload = action.payload;

    draftState.goal = {
      ...draftState?.goal,
      data: payload?.data,
      loading: false
    };
  });
  builder.addCase(getDashboardGoal.rejected, (draftState, action) => {
    draftState.goal = {
      ...draftState?.goal,
      loading: false,
      error: action.payload?.errorMessage
    };
  });
};
