import React, {
  useState,
  useMemo,
  useRef,
  useEffect,
  useCallback
} from 'react';

// components
import {
  AttributeSearchValue,
  AttributeSearchEvent,
  AttributeSearch,
  AttributeSearchRef,
  SimpleBar
} from 'app/_libraries/_dls/components';
import Dashboard from './Dashboard';

// hooks
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { getTabTitle } from 'app/helpers';
import pick from 'lodash.pick';
import every from 'lodash.every';
import { getAttributeSearchPlaceholder } from '../helpers';

// Configs
import { attrValidations } from 'pages/AccountSearch/searchValidator';
import { getInitialAttrData } from 'pages/AccountSearch/attributeSearch';
import { ATTRIBUTE_SEARCH_FIELD } from 'pages/AccountSearch/types';
import { NO_COMBINE_FIELDS } from '../constants';
// Redux
import { actions as actionsTab } from 'pages/__commons/TabBar/_redux';
import { getPageTabs } from './_redux/selector';
import { actionsLiveVox } from 'app/livevox-dialer/_redux/reducers';
import { isEmpty } from 'lodash';
// import { AGENT_STATUS } from 'app/constants/livevoxAgentStatus';
import * as liveVoxSelectors from 'app/livevox-dialer/_redux/selectors';
import { ScreenPopArgs } from 'app/livevox-dialer/types';

const Home: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const attributeSearchRef = useRef<AttributeSearchRef | null>(null);

  const [searchValues, setSearchValues] = useState<AttributeSearchValue[]>([]);
  const pageTabs = useSelector(getPageTabs);

  const handleChange = (event: AttributeSearchEvent) => {
    const { value } = event;
    setSearchValues(value);
  };

  const handleSearch = useCallback(
    (event: AttributeSearchEvent, error?: Record<string, string>) => {
      if (error || !event.value.length) return;
      const searchParams = event.value.map(item =>
        pick(item, ['key', 'value'])
      );
      const existedSearchTab = pageTabs.filter(
        tab => tab.tabType === 'accountSearchList'
      );
      const storeId = `searchAccountList_${existedSearchTab.length + 1}`;

      const title = t('txt_search_results_title', '', {
        title: getTabTitle(searchParams)
      });

      dispatch(
        actionsTab.addTab({
          title: title,
          iconName: 'search-result',
          storeId,
          tabType: 'accountSearchList',
          props: {
            searchParams,
            storeId
          }
        })
      );
    },
    [dispatch, pageTabs, t]
  );

  const noCombineFields = useMemo(() => {
    const showFullListField = [
      ATTRIBUTE_SEARCH_FIELD.FIRST_NAME,
      ATTRIBUTE_SEARCH_FIELD.LAST_NAME,
      ATTRIBUTE_SEARCH_FIELD.MIDDLE_INITIAL
    ];
    const isEnableNoCombineFeature = every(searchValues, ({ key }) => {
      return !showFullListField.includes(key as ATTRIBUTE_SEARCH_FIELD);
    });

    return isEnableNoCombineFeature
      ? NO_COMBINE_FIELDS.map(item => ({
          ...item,
          filterMessage: t(item.filterMessage)
        }))
      : [];
  }, [searchValues, t]);
  const callHandleSearch = useCallback(
    (event: AttributeSearchEvent) => handleSearch(event),
    [handleSearch]
  );
  const { constraintFields, data } = useMemo(() => getInitialAttrData(t), [t]);

  // LiveVox
  const liveVoxDialerSessionId = useSelector(
    liveVoxSelectors.getLiveVoxDialerSessionId
  );
  const liveVoxDialerLineNumber = useSelector(
    liveVoxSelectors.getLiveVoxDialerLineNumber
  );
  const liveVoxDialerInCallAccountNumber = useSelector(
    liveVoxSelectors.getLiveVoxDialerInCallAccountNumber
  );
  const liveVoxDialerLastInCallAccountNumber = useSelector(
    liveVoxSelectors.getLiveVoxDialerLastInCallAccountNumber
  );

  // LiveVox request to get Agent Status data
  useEffect(() => {
    if (!isEmpty(liveVoxDialerSessionId)) {
      const intervalId = setInterval(() => {
        dispatch(
          actionsLiveVox.getLiveVoxDialerAgentStatusThunk({
            sessionId: liveVoxDialerSessionId
          })
        );
      }, 3000);

      return () => clearInterval(intervalId);
    }
  }, [liveVoxDialerSessionId, dispatch]);

  // LiveVox request to get ScreenPop data and to perform a search
  useEffect(() => {
    if (!isEmpty(liveVoxDialerLineNumber)) {
      const screenPopArgs: ScreenPopArgs = {
        sessionId: liveVoxDialerSessionId,
        lineNumber: liveVoxDialerLineNumber
      };
      dispatch(actionsLiveVox.getLiveVoxDialerScreenPopThunk(screenPopArgs));

      if (
        liveVoxDialerInCallAccountNumber !==
        liveVoxDialerLastInCallAccountNumber
      ) {
        dispatch(
          actionsLiveVox.setLiveVoxDialerLastInCallAccountNumber(
            liveVoxDialerInCallAccountNumber
          )
        );

        if (!isEmpty(liveVoxDialerInCallAccountNumber)) {
          const event: AttributeSearchEvent = {
            value: [
              {
                key: 'accountNumber',
                value: liveVoxDialerInCallAccountNumber
              }
            ],
            valueMap: {}
          };
          callHandleSearch(event);
        }
      }
    }
  }, [
    dispatch,
    callHandleSearch,
    liveVoxDialerLineNumber,
    liveVoxDialerSessionId,
    liveVoxDialerInCallAccountNumber,
    liveVoxDialerLastInCallAccountNumber
  ]);

  return (
    <div className="h-100">
      <SimpleBar>
        <div className="home-page">
          <div className="home-search">
            <AttributeSearch
              id="homepage__attributeSearch"
              dataTestId="homepageSearch"
              placeholder={t(
                getAttributeSearchPlaceholder(
                  constraintFields,
                  data,
                  !!noCombineFields.length
                )(searchValues)
              )}
              ref={attributeSearchRef}
              data={data}
              noCombineFields={noCombineFields}
              value={searchValues}
              validator={(value: Record<string, any>) =>
                attrValidations(value, t)
              }
              onClear={() => {
                setSearchValues([]);
              }}
              orderBy="asc"
              onSearch={handleSearch}
              onChange={handleChange}
              clearTooltipProps={{ element: t('txt_clear_all_criteria') }}
              constraintFields={constraintFields}
              noFilterResultText={t('txt_no_other_parameters_available')}
            />
          </div>
          <Dashboard />
        </div>
      </SimpleBar>
    </div>
  );
};

export default Home;
