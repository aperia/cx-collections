export interface RefDataState {
  [refDataType: string]: RefData;
}

export interface RefData {
  error?: boolean;
  loading?: boolean;
  data?: RefDataValue[];
}

export interface RefStateData {
  code: number;
  text: string;
  value?: string;
  description?: string;
}

export interface CommonRefData {
  value?: string;
  description?: string;
}

export const REF_DATA_TYPE = {
  STATE: 'stateRefData',
  USA_STATE: 'states'
};

export interface GetStateRefDataRequestArgs {}

export interface PromiseStatisticsPlusProps {
  promisesPerHour: string;
  keptRate: number;
}

export interface GetStateRefDataRequestPayload {
  data: RefDataValue[];
  state: RefDataValue[];
}
