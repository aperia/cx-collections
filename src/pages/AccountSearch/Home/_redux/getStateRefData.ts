import {
  REF_DATA_TYPE,
  RefDataState,
  GetStateRefDataRequestPayload,
  GetStateRefDataRequestArgs
} from '../types';
import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import homeService from '../homeService';

export const getStateRefDataRequest = createAsyncThunk<
  GetStateRefDataRequestPayload,
  GetStateRefDataRequestArgs,
  ThunkAPIConfig
>('referenceData', async (args, thunkAPI) => {
  const { data } = await homeService.getState();
  return {
    data,
    state: data
  };
});

export const getStateRefDataBuilder = (
  builder: ActionReducerMapBuilder<RefDataState>
) => {
  builder
    .addCase(getStateRefDataRequest.pending, (draftState, action) => {
      draftState[REF_DATA_TYPE.STATE] = {
        loading: false
      };
      draftState[REF_DATA_TYPE.USA_STATE] = {};
    })
    .addCase(getStateRefDataRequest.fulfilled, (draftState, action) => {
      const { data, state } = action.payload;
      draftState[REF_DATA_TYPE.STATE] = {
        ...draftState[REF_DATA_TYPE.STATE],
        loading: true,
        data
      };
      draftState[REF_DATA_TYPE.USA_STATE] = {
        ...draftState[REF_DATA_TYPE.USA_STATE],
        data: state
      };
    })
    .addCase(getStateRefDataRequest.rejected, (draftState, action) => {
      draftState[REF_DATA_TYPE.STATE] = {
        ...draftState[REF_DATA_TYPE.STATE],
        loading: false
      };
    });
};
