import { createSelector } from '@reduxjs/toolkit';
import { AppState } from 'storeConfig';

export const getTabs = (state: AppState) => {
  return state.tab.tabs;
};

export const getPageTabs = createSelector(getTabs, data => data);
