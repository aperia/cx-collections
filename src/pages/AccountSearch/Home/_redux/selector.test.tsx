import { getTabs, getPageTabs } from './selector';
import { ItemTab } from 'pages/__commons/TabBar/types';
import { selectorWrapper } from 'app/test-utils';
import { AppState } from 'storeConfig';

describe('Selector test',() => {
  const data = [
    {
      storeId: 'tab01',
      title: 'Tab 01',
      tabType: 'accountDetail',
      className: 'page-home',
      iconName: 'home'
    }
  ]

  const store: Partial<AppState> = {
    tab: {
      tabs: data as ItemTab[]
    }
  };


  it('getTabs', () => {
    const result = getTabs({
      tab: {
        tabs: data
      }
    });

    expect(result).toEqual(data)
  })

  it('selectedDashboardGoal', () => {
    const { data, emptyData } = selectorWrapper(store)(getPageTabs);

    expect(data).toEqual([{
      className: "page-home",
      iconName: "home",
      storeId: "tab01",
      tabType: "accountDetail",
      title: "Tab 01",
    }]);
    expect(emptyData).toEqual([]);
  });
})