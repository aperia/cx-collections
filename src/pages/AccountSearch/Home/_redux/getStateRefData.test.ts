import {} from '@testing-library/react';
import { responseDefault } from 'app/test-utils';
import { createStore } from 'redux';
import { rootReducer } from 'storeConfig';
import homeService from '../homeService';
import { getStateRefDataRequest } from './getStateRefData';

let spy: jest.SpyInstance;

describe('Test getStateRefDataRequest async thunk', () => {
  afterEach(() => {
    spy?.mockReset();
    spy?.mockRestore();
  });

  it('fulfilled', async () => {
    spy = jest
      .spyOn(homeService, 'getState')
      .mockResolvedValue({ ...responseDefault });

    const store = createStore(rootReducer);
    const response = await getStateRefDataRequest({})(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('referenceData/fulfilled');
    expect(response.payload).toEqual({ data: null, state: null });
  });

  it('rejected', async () => {
    spy = jest
      .spyOn(homeService, 'getState')
      .mockRejectedValue({ status: 500 });

    const store = createStore(rootReducer);
    const response = await getStateRefDataRequest({})(
      store.dispatch,
      store.getState,
      {}
    );

    expect(response.type).toEqual('referenceData/rejected');
  });
});
