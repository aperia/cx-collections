import { createSlice } from '@reduxjs/toolkit';
import { RefDataState } from '../types';

import {
  getStateRefDataRequest,
  getStateRefDataBuilder
} from './getStateRefData';

export interface IRemoveAccountSearchTabPayload {
  storeId: string;
}

const { actions, reducer } = createSlice({
  name: 'refData',
  initialState: {} as RefDataState,
  reducers: {},
  extraReducers: builder => {
    getStateRefDataBuilder(builder);
  }
});

const homeActions = {
  ...actions,
  getStateRefDataRequest
};

export { homeActions, reducer };
