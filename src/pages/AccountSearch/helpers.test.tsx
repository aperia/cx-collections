import React from 'react';
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { getAttributeSearchPlaceholder } from './helpers';
import { EMPTY_STRING } from 'app/constants';

describe('AccountSearch helpers', () => {
  const data = [
    {
      key: 'key',
      name: 'name',
      description: 'description',
      groupName: 'groupName',
      component: () => <div />
    }
  ];
  const dataWithDisabled = [
    {
      key: 'lastName',
      name: 'lastName',
      description: 'description',
      groupName: 'groupName',
      component: () => <div />,
      disabledFields: {
        ['phone']: 'No combine message',
        ['firstName']: 'No combine message'
      }
    }
  ];
  const constraintFields = [
    {
      field: 'firstName',
      requiredField: data[0],
      errorMessage: ''
    }
  ];
  const constraintFieldsWithDisabled = [
    {
      field: 'firstName',
      requiredField: dataWithDisabled[0],
      errorMessage: ''
    }
  ];

  it('listDisabled = listRemaining', () => {
    const result = getAttributeSearchPlaceholder([], [], false)([{key: 'key', value: 'value'}]);
    expect(result).toEqual(EMPTY_STRING)
  })

  it('empty searchValues > return empty string ', () => {
    const result = getAttributeSearchPlaceholder([], [], true)([]);
    expect(result).toEqual(I18N_COMMON_TEXT.TYPE_TO_ADD_PARAMETER);
  });

  it('isNoCombineField is true > return empty string ', () => {
    const searchValues = [{ key: 'lastName', value: 'a' }];
    const result = getAttributeSearchPlaceholder([], [], true)(searchValues);
    expect(result).toEqual('');
  });

  it('has attribute search data > return placeholder content', () => {
    const searchValues = [{ key: 'lastName', value: 'a' }];
    const result = getAttributeSearchPlaceholder([], data, false)(searchValues);
    expect(result).toEqual(I18N_COMMON_TEXT.TYPE_TO_ADD_PARAMETER);
  });

  it('having full data > return placeholder content', () => {
    const searchValues = [{ key: 'lastName', value: 'a' }];
    const result = getAttributeSearchPlaceholder(
      constraintFields,
      data,
      false
    )(searchValues);
    expect(result).toEqual(I18N_COMMON_TEXT.TYPE_TO_ADD_PARAMETER);
  });

  it('having full data > return placeholder content', () => {
    const searchValues = [{ key: 'lastName', value: 'a' }];
    const result = getAttributeSearchPlaceholder(
      constraintFieldsWithDisabled,
      data,
      false
    )(searchValues);
    expect(result).toEqual(I18N_COMMON_TEXT.TYPE_TO_ADD_PARAMETER);
  });
});
