import { stringValidate } from 'app/helpers/stringValidate';
import get from 'lodash.get';
import { ATTRIBUTE_SEARCH_FIELD, AttributeSearchFieldModel } from './types';
import { MESSAGE } from './constants';

export const validateFirstName = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  if (data.firstName && !data.firstName?.value) {
    return {
      [ATTRIBUTE_SEARCH_FIELD.FIRST_NAME]: translateFn(
        MESSAGE.FIRST_NAME_REQUIRED
      )
    };
  }
};

export const validateLastName = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  const lastNameValue = data.lastName?.value;
  const lastNameKeyword = get(lastNameValue, ['keyword']);
  if (data.lastName && !lastNameKeyword) {
    return {
      [ATTRIBUTE_SEARCH_FIELD.LAST_NAME]: translateFn(
        MESSAGE.LAST_NAME_REQUIRED
      )
    };
  }
};

export const validateMiddleInitial = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  if (data.middleInitial && !data.middleInitial?.value) {
    return {
      [ATTRIBUTE_SEARCH_FIELD.MIDDLE_INITIAL]: translateFn(
        MESSAGE.MIDDLE_INITIAL_REQUIRED
      )
    };
  }
};

export const validateAccountNumber = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  const accountNumberValue = data.accountNumber?.value;
  if (data.accountNumber) {
    const methodValid = stringValidate(accountNumberValue);
    const isRequire = methodValid.isRequire();
    const isValidMinLength = accountNumberValue && methodValid.minLength(10);

    if (!isRequire || !isValidMinLength) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER]: translateFn(
          MESSAGE.INVALID_FORMAT
        )
      };
    }
  }
};

export const validateAffinityNumber = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  const affinityNumberValue = data.affinityNumber?.value;
  if (data.affinityNumber) {
    const methodValid = stringValidate(affinityNumberValue);
    const isRequire = methodValid.isRequire();
    const isValidMinLength = affinityNumberValue && methodValid.minLength(10);

    if (!isRequire || !isValidMinLength) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.AFFINITY_NUMBER]: translateFn(
          MESSAGE.INVALID_FORMAT
        )
      };
    }
  }
};

export const validatePhoneNumber = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  if (data.phoneNumber) {
    const phoneNumberValue = data.phoneNumber?.value || '';
    const methodValid = stringValidate(phoneNumberValue?.trim());
    const isRequire = methodValid.isRequire();
    if (!isRequire || phoneNumberValue.includes('_')) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.PHONE_NUMBER]: translateFn(
          MESSAGE.INVALID_FORMAT
        )
      };
    }
  }
};
export const validateSSN = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  if (data.ssn) {
    const ssnValue = data.ssn?.value || '';
    const methodValid = stringValidate(ssnValue?.trim());
    const isRequire = methodValid.isRequire();
    if (!isRequire || ssnValue.includes('_')) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.SSN]: translateFn(MESSAGE.INVALID_FORMAT)
      };
    }
  }
};

export const validateState = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  const stateValue = data.state?.value;
  if (data.state && !stateValue) {
    return {
      [ATTRIBUTE_SEARCH_FIELD.STATE]: translateFn(MESSAGE.STATE_REQUIRED)
    };
  }
};

export const validateZipCode = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  if (data.zipCode) {
    const zipCodeValue = data.zipCode?.value;
    const methodValid = stringValidate(zipCodeValue);
    if (!methodValid.isRequire()) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.ZIP_CODE]: translateFn(
          MESSAGE.ZIP_CODE_REQUIRED
        )
      };
    }
    if (!methodValid.minLength(5)) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.ZIP_CODE]: translateFn(MESSAGE.INVALID_FORMAT)
      };
    }
  }
};

export const attrValidations = (
  data: AttributeSearchFieldModel,
  translateFn: Function
) => {
  const validateList = [
    validateFirstName,
    validateLastName,
    validateMiddleInitial,
    validateAccountNumber,
    validateAffinityNumber,
    validatePhoneNumber,
    validateSSN,
    validateState,
    validateZipCode
  ];

  const runAllValidates = validateList.map(fn => {
    return fn(data, translateFn);
  }, []);

  const errors: Record<string, string> = runAllValidates.reduce(
    (accumulator, current) => {
      if (!current) return accumulator;
      return {
        ...accumulator,
        ...current
      };
    },
    {}
  );
  return Object.keys(errors).length ? errors : undefined;
};
