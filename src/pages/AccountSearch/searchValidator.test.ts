import * as searchValidator from './searchValidator';

const translateFn = (text: string) => text;

describe('validateFirstName', () => {
  it('should run validateFirstName without value', () => {
    const result = searchValidator.validateFirstName(
      {
        firstName: { value: '' }
      },
      translateFn
    );
    expect(result).toEqual({ firstName: 'txt_first_name_is_required' });
  });

  it('should run validateFirstName with value', () => {
    const result = searchValidator.validateFirstName(
      {
        firstName: { value: 'smith' }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });
});

describe('validateLastName', () => {
  it('should run validateLastName without value', () => {
    const result = searchValidator.validateLastName(
      {
        lastName: { value: { keyword: '' } }
      },
      translateFn
    );
    expect(result).toEqual({ lastName: 'txt_last_name_is_required' });
  });

  it('should run validateLastName with value', () => {
    const result = searchValidator.validateLastName(
      {
        lastName: { value: { keyword: 'a' } }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });
});

describe('validateMiddleInitial', () => {
  it('should run validateMiddleInitial without value', () => {
    const result = searchValidator.validateMiddleInitial(
      {
        middleInitial: { value: '' }
      },
      translateFn
    );
    expect(result).toEqual({ middleInitial: 'txt_middle_initial_is_required' });
  });

  it('should run validateMiddleInitial with value', () => {
    const result = searchValidator.validateMiddleInitial(
      {
        middleInitial: { value: 'a' }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });
});

describe('validateAccountNumber', () => {
  it('should run validateAccountNumber and return Invalid format', () => {
    const result = searchValidator.validateAccountNumber(
      {
        accountNumber: { value: '' }
      },
      translateFn
    );
    expect(result).toEqual({ accountNumber: 'txt_invalid_format' });
  });

  it('should run validateAccountNumber with valid format', () => {
    const result = searchValidator.validateAccountNumber(
      {
        accountNumber: { value: '123456789123123' }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });

  it('should run validateAccountNumber without value', () => {
    const result = searchValidator.validateAccountNumber(
      {
        accountNumber: undefined
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });
});

describe('validateAffinityNumber', () => {
  it('should run validateAffinityNumber and return Invalid format', () => {
    const result = searchValidator.validateAffinityNumber(
      {
        affinityNumber: { value: '' }
      },
      translateFn
    );
    expect(result).toEqual({ affinityNumber: 'txt_invalid_format' });
  });

  it('should run validateAffinityNumber with valid format', () => {
    const result = searchValidator.validateAffinityNumber(
      {
        affinityNumber: { value: '123456789123123' }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });

  it('should run validateAffinityNumber without value', () => {
    const result = searchValidator.validateAffinityNumber(
      {
        affinityNumber: undefined
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });
});

describe('validatePhoneNumber', () => {
  it('should run validatePhoneNumber with value', () => {
    const result = searchValidator.validatePhoneNumber(
      {
        phoneNumber: { value: '(123) 123-1234' }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });

  it('should run validatePhoneNumber without value', () => {
    const result = searchValidator.validatePhoneNumber(
      {
        phoneNumber: undefined
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });

  it('should run validatePhoneNumber without value', () => {
    const result = searchValidator.validatePhoneNumber(
      {
        phoneNumber: { value: '' }
      },
      translateFn
    );
    expect(result).toEqual({ phoneNumber: 'txt_invalid_format' });
  });
});

describe('validateSSN', () => {
  it('should run validateSSN without value', () => {
    const result = searchValidator.validateSSN(
      {
        ssn: { value: '' }
      },
      translateFn
    );
    expect(result).toEqual({ ssn: 'txt_invalid_format' });
  });

  it('should run validateSSN without value', () => {
    const result = searchValidator.validateSSN(
      {
        ssn: undefined
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });

  it('should run validateSSN with value', () => {
    const result = searchValidator.validateSSN(
      {
        ssn: { value: '123-12-3123' }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });
});

describe('validateState', () => {
  it('should run validateState without value', () => {
    const result = searchValidator.validateState(
      {
        state: { value: '' }
      },
      translateFn
    );
    expect(result).toEqual({ state: 'txt_state_is_required' });
  });

  it('should run validateState with value', () => {
    const result = searchValidator.validateState(
      {
        state: { value: 'LA' }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });
});

describe('validateZipCode', () => {
  it('should run validateZipCode without value', () => {
    const result = searchValidator.validateZipCode(
      {
        zipCode: { value: '' }
      },
      translateFn
    );
    expect(result).toEqual({ zipCode: 'txt_zip_code_is_required' });
  });

  it('should run validateZipCode without value', () => {
    const result = searchValidator.validateZipCode(
      {
        zipCode: undefined
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });

  it('should run validateState with invalid value', () => {
    const result = searchValidator.validateZipCode(
      {
        zipCode: { value: '123' }
      },
      translateFn
    );
    expect(result).toEqual({ zipCode: 'txt_invalid_format' });
  });

  it('should run validateState with value', () => {
    const result = searchValidator.validateZipCode(
      {
        zipCode: { value: '12345' }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });
});

describe('attrValidations', () => {
  it('should run attrValidations with value', () => {
    const result = searchValidator.attrValidations(
      {
        zipCode: { value: '12345' }
      },
      translateFn
    );
    expect(result).toEqual(undefined);
  });

  it('should run attrValidations with invalid value', () => {
    const result = searchValidator.attrValidations(
      {
        zipCode: { value: '123' }
      },
      translateFn
    );
    expect(result).toEqual({ zipCode: 'txt_invalid_format' });
  });
});
