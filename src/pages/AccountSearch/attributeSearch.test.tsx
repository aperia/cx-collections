import React from 'react';
import * as attributeSearch from './attributeSearch';
import { render, screen } from '@testing-library/react';

jest.mock('app/components/InputControl', () => () => <div>InputControl</div>);
jest.mock('app/components/MainSubConditionControl', () => () => (
  <div>MainSubConditionControl</div>
));
jest.mock('app/components/MaskedTextBoxControl', () => () => (
  <div>MaskedTextBoxControl</div>
));
jest.mock('app/components/AttributeSearchField', () => () => (
  <div>ComboBoxControl</div>
));

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('attributeSearch', () => {
  it('should run ACCOUNT_NUMBER success', () => {
    const result = attributeSearch.getInitialAttrData(jest.fn());
    const Component = result.data[0].component;
    render(<Component />);
    expect(screen.getByText('InputControl')).toBeInTheDocument();
  });

  it('should run AFFINITY_NUMBER success', () => {
    const result = attributeSearch.getInitialAttrData(jest.fn());
    const Component = result.data[1].component;
    render(<Component />);
    expect(screen.getByText('InputControl')).toBeInTheDocument();
  });

  it('should run FIRST_NAME success', () => {
    const result = attributeSearch.getInitialAttrData(jest.fn());
    const Component = result.data[2].component;
    render(<Component />);
    expect(screen.getByText('InputControl')).toBeInTheDocument();
  });

  it('should run LAST_NAME success', () => {
    const result = attributeSearch.getInitialAttrData(jest.fn());
    const Component = result.data[3].component;
    render(<Component value="" />);
    expect(screen.getByText('MainSubConditionControl')).toBeInTheDocument();
  });

  it('should run MIDDLE_INITIAL success', () => {
    const result = attributeSearch.getInitialAttrData(jest.fn());
    const Component = result.data[3].component;
    render(<Component value="a" />);
    expect(screen.getByText('MainSubConditionControl')).toBeInTheDocument();
  });

  it('should run ACCOUNT_NUMBER success', () => {
    const result = attributeSearch.getInitialAttrData(jest.fn());
    const Component = result.data[4].component;
    render(<Component />);
    expect(screen.getByText('InputControl')).toBeInTheDocument();
  });

  it('should run PHONE_NUMBER success', () => {
    const result = attributeSearch.getInitialAttrData(jest.fn());
    const Component = result.data[5].component;
    render(<Component />);
    expect(screen.getByText('MaskedTextBoxControl')).toBeInTheDocument();
  });

  it('should run SSN success', () => {
    const result = attributeSearch.getInitialAttrData(jest.fn());
    const Component = result.data[6].component;
    render(<Component />);
    expect(screen.getByText('MaskedTextBoxControl')).toBeInTheDocument();
  });

  it('should run STATE success', () => {
    const result = attributeSearch.getInitialAttrData(jest.fn());
    const Component = result.data[7].component;
    render(<Component />);
    expect(screen.getByText('ComboBoxControl')).toBeInTheDocument();
  });

  it('should run ZIP_CODE success', () => {
    const result = attributeSearch.getInitialAttrData(jest.fn());
    const Component = result.data[8].component;
    render(<Component />);
    expect(screen.getByText('InputControl')).toBeInTheDocument();
  });
});
