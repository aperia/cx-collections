import { ThunkAction, ThunkDispatch } from 'redux-thunk';
import { ActionCreator, AnyAction } from 'redux';

import { AppState } from 'storeConfig';
import {
  AsyncThunkAction,
  ParametricSelector,
  PayloadAction
} from '@reduxjs/toolkit';
import { AxiosError } from 'axios';

// Const
import {
  ENTITLEMENT_COMPONENTS,
  PERMISSIONS
} from 'app/entitlements/constants';

declare global {
  interface RootState extends AppState {}
  interface CommonConfig {
    org: string;
    app: string;
    appCxCollections: string;
    callingApplication: string;
    callingApplicationApr01: string;
    callingApplicationAPP1?: string;
    callingApplicationTestA?: string;
    operatorID?: string;
    operatorCode?: string;
    x500id: string;
    essUserName: string;
    privileges: Array<string>;
    isAdminRole: boolean;
    isManager: boolean;
    isSupervisor: boolean;
    isCollector: boolean;
    user: string;
    collectionsUrl: string;
    solutionBuilderUrl: string;
    loginURL: string;
    realAPI: string;
    mockAPI?: string;
    defaultMockAPI: string;
    roles: Record<string, string>;
    selectedEnv: string;
  }
  interface AppConfiguration {
    i18n?: {
      supplemental?: string;
      en?: {
        allComponents?: string;
        main?: string;
      };
      ja?: {
        allComponents?: string;
        main?: string;
      };
      vi?: {
        allComponents?: string;
        main?: string;
      };
    };
    api?: {
      accessConfig?: {
        getAccessConfig?: string;
      };
      clientConfigMemos?: {
        getMemoConfig?: string;
        getMemosConfig: string;
        saveMemosConfig: string;
      };
      common?: {
        accessToken?: string;
        renewToken?: string;
        getESSToken?: string;
        entitlement?: string;
        refEntitlement?: string;
        saveUserEntitlement?: string;
        getUserEntitlement?: string;
      };
      letterConfig?: {
        getLetterConfiguration: string;
        addLetterConfiguration: string;
        deleteLetterConfiguration: string;
        editLetterConfiguration: string;
        saveLetterConfiguration: string;
        getLetterCaptionFields: string;
        getGenerateLetterFieldMapping: string;
      };
      sendLetterConfig?: {
        getSendLetterConfiguration: string;
        addSendLetterConfiguration: string;
        deleteSendLetterConfiguration: string;
        editSendLetterConfiguration: string;
        getFrequencyConfiguration: string;
        addFrequencyConfiguration: string;
        deleteFrequencyConfiguration: string;
        editFrequencyConfiguration: string;
      };
      account?: {
        searchAccount?: string;
        getAccountDetails?: string;
        getExternalConfig?: string;
        getRelatedAccounts?: string;
        getOverviewAccount?: string;
        getOverviewCollection?: string;
        getExternalStatusRefData?: string;
        getExternalStatusReasonRefData?: string;
        updateAccountExternalStatusCode?: string;
        getAuthenticationConfig?: string;
      };
      refData?: {
        getState?: string;
        getAssets?: string;
        getProSE?: string;
      };
      collectionForm: {
        bankruptcyInformation: string;
        bankruptcyUpdateWorkflow: string;
        bankruptcyInformationDelete: string;
        getCreditBureauCodeRefData: string;
        updateActionEntryWorkFlow: string;
        getCallResultsRefData: string;
        getDetailCallResultType: string;
        removeStatusCallResult: string;
        callResultReferData: string;
        addSinglePromiseToPay: string;
        addRecurringPromiseToPay: string;
        addCustomPromiseToPay: string;
        getPromiseToPay: string;
        getPromiseToPayConfig: string;
        getPromiseToPayMethod: string;
        removeBankruptcyStatus: string;
        getTalkingPoint: string;
        updateNextWorkDate: string;
        updateMoveToQueCode: string;
        actionEntry: string;
        getLast25Actions: string;
        getDescriptionCodeForLast25Actions: string;
        uploadCaseDocument: string;
        downloadCaseDocument: string;
        uploadMilitaryFiles: string;
        getCaseDocumentList: string;
        removeDeathCertificates: string;
        getLongTermMedical: string;
        updateLongTermMedical: string;
        removeLongTermMedical: string;
        getIncarceration: string;
        removeIncarceration: string;
        updateIncarceration: string;
        getDetailsHardship: string;
        setupHardship: string;
        getHardshipDebtManagementDetails: string;
        removeHardship: string;
        getMilitaryFile: string;
        getCCCSDetail: string;
        updatePendingCCCS: string;
        updateAcceptProposalCCCS: string;
        updateDeclineProposalCCCS: string;
        updateDiscontinuePlanCCCS: string;
        getSettlement: string;
        updateSettlement: string;
        updateSettlementSingle: string;
        updateSettlementRecurring: string;
        updateSettlementNonRecurring: string;
        getSettlementInformation: string;
      };
      pendingAuthorizations: {
        getPendingAuthTransactionDetail: string;
        getPendingAuthTransactions: string;
      };
      statement: {
        getStatementMonthList: string;
        getAccountStatementOverview: string;
        getAccountStatementHistory: string;
      };
      transaction?: {
        getTransactionList: string;
        getTransactionDetail: string;
      };
      transactionAdjustmentRequest: {
        adjustTransaction: string;
        rejectAdjustment: string;
        approveAdjustment: string;
        pendingQueue: string;
        approvedQueue: string;
        rejectedQueue: string;
      };
      adjustmentTransactionConfig: {
        getAdjustmentConfiguration: string;
        addAdjustmentConfiguration: string;
        deleteAdjustmentConfiguration: string;
        editAdjustmentConfiguration: string;
      };
      clientConfigClusterToQueueMapping: {
        getClusterQueueAssignmentMethod: string;
      };
      memo: {
        getChronicleMemosList: string;
        addChronicleMemo: string;
        updateChronicleMemo: string;
        deleteChronicleMemo: string;
        getMemoConfig: string;
        getCISMemos: string;
        addCISMemo: string;
        deleteCISMemo: string;
        updateCISMemo: string;
        getMemoChronicleBehaviors: string;
        getMemoChronicleFilters: string;
        CISSource: string;
        CISType: string;
      };
      payment: {
        getConvenienceFee: string;
        submitACHMakePayment: string;
        submitScheduleMakePayment: string;
        getPayments: string;
        checkPaymentListPendingStatus: string;
        getPaymentViewMore: string;
        getBankAccounts: string;
        getDetailPaymentInfo: string;
        getDetailAccountPaymentInfo: string;
        deleteRegisteredAccount: string;
        validateBankAccountInfo: string;
        registerBankAccount: string;
        cancelPendingPayments: string;
        getACHPayment: string;
        verifyACHBankAccount: string;
        addACHBankAccount: string;
        getACHBankAccount: string;
        updateACHBankAccount: string;
        getACHPaymentInformation: string;
        deleteACHBankAccount: string;
        getPaymentHistory: string;
        getCardInfoACH: string;
        getBankInfoACH: string;
      };
      sendLetter: {
        getLetters: string;
        getPendingLetters: string;
        deletePendingLetter: string;
      };
      clientContactInfo: {
        getClientContactInfo: string;
      };
      i18next: {
        getMultipleLanguage: string;
      };
      cardholdersMaintenance: {
        checkTypeAddress: string;
        getCardholderMaintenance: string;
        addAndUpdateExpandedAddress: string;
        update: string;
        addStandardAddress: string;
        editStandardAddress: string;
        getStatesByCountry: string;
        deleteAddressExpandedAddress: string;
        getExpandedAddressHistory: string;
        getStandardAddressHistory: string;
        getAddressInfo: string;
      };
      letter: {
        sendLetter: string;
        letterInquiry: string;
      };
      company: {
        getCompanyList: string;
        addCompany: string;
        updateCompany: string;
      };
      deceased: {
        deceasedInformation: string;
        deceasedCreateWorkflow: string;
      };
      nextActions: {
        getNextAccount: string;
      };
      callerAuthentication: {
        getCallerAuthentication: string;
        updateCallerAuthentication: string;
        getAddressInfo: string;
        checkTypeAddress: string;
        getPostalCodeList: string;
      };
      timerWorking: {
        logWorkingTime: string;
        getTimerConfig: string;
      };
      clientConfig: {
        clientInfoGetList: string;
        getSectionList: string;
        add: string;
        update: string;
        addUpdateClientInfo: string;
        getClientInfoById: string;
        changeStatusClientConfig: string;
        getCollectionsClientConfig: string;
        saveCollectionsClientConfig: string;
        getAccessConfig: string;
      };
      configViewEntitlement: {
        getInfo: string;
        updateInfo: string;
      };
      contactAndAccountEntitlement: {
        getContactAccListCSPA?: string;
        getContactAccEntitlement?: string;
        updateContactAccEntitlement?: string;
      };
      getClientConfigurationTalkingPoint: {
        getPanelContent: string;
        getClientConfigTalkingPoint: string;
        updateClientConfigTalkingPoint: string;
      };
      clientConfigAuthentication: {
        submitAuthentication: string;
        getAuthenticationConfig: string;
      };
      clientConfigTimer: {
        getTimerConfig: string;
        updateTimerConfig: string;
      };
      clientConfigPromiseToPay: {
        submitPromiseToPay: string;
        getPromiseToPayConfig: string;
      };
      clientConfigAccountExternalStatus: {
        saveExternalStatus: string;
        getAccountExternalStatus: string;
        getReasonCode: string;
        addReasonCode: string;
        editReasonCode: string;
        deleteReasonCode: string;
      };
      clientConfigCodeToText: {
        getListDropDownCodeToText: string;
        postListData: string;
        addCodeToText: string;
        editCodeToText: string;
        deleteCodeToText: string;
      };
      clientConfigReference: {
        getListReference: string;
        updateReference: string;
      };
      clientConfigCallResult: {
        getList: string;
        add: string;
        update: string;
        delete: string;
        reactive: string;
      };
      clientConfigClientContactInformation: {
        getPanelContent: string;
        updatePanelContent: string;
      };
      clientConfigQueueAndRoleMapping: {
        getQueueAndRoleMapping: string;
        addQueueAndRoleMapping: string;
        deleteQueueAndRoleMapping: string;
        getQueueNameList: string;
        getUserRoleList: string;
      };
      dashboard: {
        getDashboardGoal: string;
        getDashboardCallActivity: string;
        getDashboardCollectorWorkStatistics: string;
        getDashboardSupervisorWorkStatistics: string;
        getDashboardWorkQueue: string;
        getRecentlyWorkQueue: string;
        getDashboardWorkCollector: string;
        getNextAccountWorkOnQueue: string;
        getAccountExtendLockTime: string;
        getAccountLockStatusInquiry: string;
        getAccountLockTimeUpdate: string;
        updateCollectionAccount: string;
      };
      collectorProductivity: {
        getTimePeriods: string;
        getAdminTimePeriods: string;
        getReportTypes: string;
        getStatistics: string;
      };
      promisesPaymentsStatistics: {
        getReports: string;
        getCollectors: string;
        getTeams: string;
        getTimePeriods: string;
        getReportTypes: string;
        getPromiseTypes: string;
        getPromiseStatuses: string;
      };
      debtManagementCompanies: {
        getCompany: string;
        deleteCompany: string;
        updateCompany: string;
      };
      userRoleEntitlement: {
        getEntitlement: string;
        updateUserRoleEntitlement: string;
      };
      queueList: {
        getQueueList: string;
        getCollectorQueueList: string;
      };
      collector: {
        getCollectorWorkQueue: string;
        getUserList: string;
        getCollectorList: string;
      };
      clientConfigPayByPhone: {
        submitPayByPhone: string;
        getPayByPhoneConfig: string;
      };
      clientConfigSettlementPayment: {
        submitSettlementPayment: string;
        getSettlementPaymentConfig: string;
      };
      changeHistory: {
        getChangedCategories: string;
        getChangedHistories: string;
        addChangeHistories: string;
        getHistoryLabel: string;
      };
      functionRuleMapping: {
        getFunctionRuleMapping: string;
        addFunctionRuleMapping: string;
        editFunctionRuleMapping: string;
        getReferenceDataForFunction: string;
      };
      exceptionHandling: {
        cspaGetList: string;
        caseDetailsSearch: string;
        getCaseStatuses: string;
        secureArchiveRetrievalByCaseId: string;
        reprocessCase: string;
      };
      liveVoxDialer: {
        getSessionId: string;
        getAgentStatus: string;
        getScreenPopDetails: string;
        getTermCode: string;
        saveTermCode: string;
      };
    };
    mappingUrl?: string;
    jwtConfig?: {
      inactiveTime?: number;
      expTime?: number;
    };
    orgList: string[];
    commonConfig: CommonConfig;
  }

  type ENVIRONMENT_TYPE = 'CAT' | 'QA' | 'PROD' | 'VNQA';
  interface ENVConfig {
    defaultMockAPI: string;
    defaultENV: ENVIRONMENT_TYPE;
    orgList: string[];
    commonConfig: {
      [env in ENVIRONMENT_TYPE]: CommonConfig;
    };
  }

  interface RejectMetaData<A = any> {
    arg: A;
    requestId: string;
    rejectedWithValue: boolean;
    requestStatus: 'rejected';
    aborted: boolean;
    condition: boolean;
  }

  type RejectedAPIPayload<A = any> = Partial<
    PayloadAction<
      Partial<AxiosError>,
      string,
      RejectMetaData<A>,
      Partial<AxiosError>
    >
  >;
  interface Window {
    debug?: boolean;
    appConfig: AppConfiguration;
    jwtTimes?: {
      [ky: string]: NodeJS.Timeout | undefined;
    };

    currentENV: ENVIRONMENT_TYPE;
    clientConfiguration?: {
      isHavingTabListener?: boolean;
    };

    delayProgress?: {
      [id: string]: NodeJS.Timeout | undefined;
    };
  }

  interface FeatureCommonConfig {
    selectFields: string[];
  }

  type FeatureConfigKy = keyof FeatureConfig;

  type AppThunkDispatch = ThunkDispatch<RootState, {}, AnyAction>;

  type AppThunkAction<T extends any = void> = ThunkAction<
    T,
    AppState,
    unknown,
    Action<string>
  >;

  type AppThunk<T extends any = void> = ActionCreator<AppThunkAction<T>>;

  type AsyncThunkActionPayloadAction = AsyncThunkAction<
    any,
    any,
    ThunkAPIConfig
  >;

  export type DeepPartial<T> = {
    [P in keyof T]?: Partial<T[P]>;
  };

  type ThunkAPIConfig = {
    rejectValue: {
      errorMessage?: string;
      response?: Partial<AxiosError>;
      others?: any;
    };
    state: RootState;
  };

  interface StoreIdPayload {
    storeId: string;
  }
  interface AccountId {
    accountId: string;
  }

  interface ForceRefreshStoreIdPayload extends StoreIdPayload {
    isForceRefresh?: boolean;
  }

  // MagicKeyValue used when we can't define type of that object or properties
  interface MagicKeyValue {
    [ky: string]: any;
  }

  type CustomStoreIdSelector<R> = ParametricSelector<RootState, string, R>;
  export interface RefData {
    code?: number;
    text?: string;
  }

  interface IInlineMessage extends StoreIdPayload {
    message?: string;
  }
  interface RefDataValue {
    description: string;
    value: string;
  }

  interface ArgsCommon {
    storeId: string;
    postData: { accountId: string };
  }

  interface IRefData {
    description?: string;
    fieldID: string;
    fieldValue: string;
  }

  interface DOFEntitlement {
    entitlementConfig?: {
      entitlementCode?: string;
    };
  }

  export interface EntitlementItem {
    entitlementCode?: PERMISSIONS;
    active?: boolean;
  }

  interface EntitlementConfig {
    component?: ENTITLEMENT_COMPONENTS;
    entitlements?: {
      items?: Array<EntitlementItem>;
    };
  }

  export interface AdditionalFieldsMapType {
    name: string;
    active?: boolean;
    value?: string;
    description?: string;
  }
  interface CSPA {
    id?: string;
    cspaId?: string;
    clientId?: string;
    systemId?: string;
    principleId?: string;
    agentId?: string;
    description?: string;
    active?: boolean;
    userId?: string;
    isDefaultClientInfoKey?: boolean;
    additionalFields?: AdditionalFieldsMapType[];
  }

  interface FormCSPAValue {
    clientRadio: RadioOptions;
    clientId?: string;
    systemId?: string;
    systemRadio: RadioOptions;
    principleId?: string;
    principleRadio: RadioOptions;
    agentId?: string;
    agentRadio: RadioOptions;
    description?: string;
  }

  type FormCSPAKy = keyof FormValue;

  export type RadioCSPAFieldKy = Extract<
    FormKy,
    'clientRadio' | 'systemRadio' | 'principleRadio' | 'agentRadio'
  >;

  interface PaymentConfig {
    common?: {
      app?: string;
      org?: string;
      x500id?: string;
    };
  }

  interface CommonRequest {
    common?: {
      privileges?: Array<string>;
      userPrivileges?: { cspa?: Array<string>; groups?: Array<string> };
      clientName?: string;
      accountId?: string;
      org?: string;
      x500id?: string;
      app?: string;
      clientNumber?: string;
      agent?: string;
      prin?: string;
      system?: string;
      user?: string;
    };
  }

  interface CommonAccountIdRequestBody extends CommonRequest {
    inquiryFields?: {
      memberSequenceIdentifier?: string;
      customerRoleTypeCode?: string;
    };
    selectFields?: string[];
    isCallRealApi?: boolean;
  }

  interface RenewTokenPayload {
    jwt: string;
    common?: {
      applicationId?: string;
      accountId?: string;
    };
  }

  interface AccessTokenPayload {
    accountId: string;
  }

  export interface MaskPattern {
    isRegex?: boolean;
    pattern?: string;
  }

  export interface GroupDate {
    month?: string;
    day?: string;
    year?: string;
  }
  interface CommonIdentifier {
    maskedValue?: string;
    eValue?: string;
  }

  interface CommonErrorMsg {
    errorCode?: string;
    message?: string;
    responseCode?: string;
    resultCode?: string;
    status?: string;
  }

  type SessionStorageKey =
    | 'FS_TOKEN'
    | 'tab'
    | 'timerWorking'
    | 'accountDetail'
    | 'collectionInProgressStatus'
    | 'CURRENT_LANGUAGE';

  interface CollectionsClientConfigItem {
    active: boolean;
    components: Array<{ component: string; jsonValue: Record<string, any> }>;
    cspa: string;
  }

  interface JWT {
    privileges?: Array<string>;
    iss?: string;
    collectorId?: string;
    userName?: string;
    userId?: string;
    essUserName?: string;
    OrgID?: string;
    OrgId?: string;
    nbf?: number;
    appId?: number;
    ocsShell?: string;
    exp?: number;
    operatorId?: string;
    x500Id?: string;
    session?: string;
    jti?: string;
  }
  class ClipboardItem {
    constructor(data: { [mimeType: string]: Blob });
  }

  interface Clipboard {
    write(items: Array<ClipboardItem>): Promise<void>;
  }
}
