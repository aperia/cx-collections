import { WorkingConfig } from 'pages/TimerWorking/types';

export interface TimerWorkerConfig {
  config: WorkingConfig;
}
