export interface BankruptcyConfiguration {
  defaultFields?: MagicKeyValue;
  selectFields?: Array<string>;
  mappingFields?: MagicKeyValue;
}
