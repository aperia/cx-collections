export interface TalkingPointRequestBody {
  common: {
    app: string;
    accountId: string;
  };
}
export interface CollectionFormConfig {
  talkingPointRequestBody: TalkingPointRequestBody;
}
