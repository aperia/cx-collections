export interface Field {
  populateFieldName?: string;
  fieldName?: string;
  additionalOptions?: MagicKeyValue;
  additionalValidations?: Array<MagicKeyValue>;
}
export interface FieldConfig {
  optionsName?: string;
  additionalOptions?: MagicKeyValue;
  additionalValidations?: Array<MagicKeyValue>;
}

export interface PopulateField {
  ky?: string;
  fieldName?: string;
  includesLabel?: Array<string>;
}
export interface SendLetterConfiguration {
  fieldType?: {
    currency?: Array<string>;
    date?: Array<string>;
  };
  populateFields?: Array<PopulateField>;
  additionalOptionsConfig?: Array<FieldConfig>;
  combineTwoFields?: Array<string>;
  customerSelectFields?: Array<string>;
  addressSelectFields?: Array<string>;
  mappingDynamicKys?: MagicKeyValue;
}
