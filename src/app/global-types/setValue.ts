export interface SetBooleanValue extends StoreIdPayload {
  value?: boolean;
}

export interface SetMagicValue extends StoreIdPayload {
  value: MagicKeyValue;
}

export interface SetStringValue extends StoreIdPayload {
  value: string;
}
