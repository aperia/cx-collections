export interface RelatedAccountConfig {
  selectFields: Array<string>;
  defaultFields: MagicKeyValue;
  selectFieldDetail: Array<string>;
}
