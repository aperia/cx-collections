import { format2DigitNumber } from 'app/_libraries/_dls/components/TimePicker/helpers';

export const getTime = () => {
  return `${format2DigitNumber(new Date().getHours())}:${format2DigitNumber(
    new Date().getMinutes()
  )}:${format2DigitNumber(new Date().getSeconds())}`;
};
