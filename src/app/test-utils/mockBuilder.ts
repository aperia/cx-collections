import { FULFILLED, PENDING, REJECTED } from 'app/constants';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';

export const mockBuilder = (createAsyncThunk = {}, args = {}) => {
  return (actionState = FULFILLED, state = {}, response = {}) => {
    // Given
    const store = createStore(rootReducer, state, applyMiddleware(thunk));

    // When
    const actionCreator = createAsyncThunk[
      actionState as keyof typeof createAsyncThunk
    ] as any;

    let actionCreatorArgs = [{ ...response }, actionCreator.type, args];

    switch (actionState) {
      case PENDING:
        actionCreatorArgs = [actionCreator.type, args];
        break;
      case REJECTED:
        actionCreatorArgs = [new Error('error'), actionCreator.type, args];
        break;
      case FULFILLED:
      default:
        break;
    }
    const action = actionCreator(...actionCreatorArgs);

    const actual = rootReducer(store.getState(), action);

    return {
      actual
    };
  };
};
