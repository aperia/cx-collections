export const mockStoreId = '0412';
export const mockAccEValue = 'accEValue';
export const mockData = {
  storeId: mockStoreId,
  initReducer: {
    [mockStoreId]: {}
  },
  request: {
    postData: {
      accountId: mockStoreId,
      callResultType: 2
    },
    storeId: mockStoreId
  }
};
export const mockService = {
  updateCallResultStatus: (requestData: any) =>
    jest.fn().mockResolvedValue({ data: requestData }),
  checkCallResultStatus: () =>
    jest.fn().mockResolvedValue({ data: { bankruptcy: null } })
};
