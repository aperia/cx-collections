import { apiService } from 'app/utils/api.service';

let spies: jest.SpyInstance[] = [];

const mockService = (action: string) => (mock?: any) => {
  const mockAction = mock ? mock : jest.fn();

  const spy = jest
    .spyOn(apiService as any, `${action}`)
    .mockImplementation(mockAction);

  spies.push(spy);

  return mockAction;
};

// support mock action, dev don't need to mock dispatch()
const mockApiServices = {
  get: mockService('get'),
  post: mockService('post'),
  put: mockService('put')
};

afterEach(() => {
  spies.forEach(spy => {
    spy?.mockReset();
    spy?.mockRestore();
  });
  spies = [];
});

export { mockApiServices };
