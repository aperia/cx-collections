export const mockApiMapping = {
  cisMemoService: {
    getCISMemos: jest.fn(),
    addCISMemo: jest.fn()
  },
  i18nextService: {
    getMultipleLanguage: jest.fn()
  },
  collectionFormService: {
    updateCCCS: jest.fn(),
    getDetailCallResultType: jest.fn()
  },
  cardholdersMaintenance: { getCardholderMaintenance: jest.fn() },
  refDataService: { getReferenceDataOptions: jest.fn() }
};
