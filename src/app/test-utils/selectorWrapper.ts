import { useSelectorMock } from './useSelectorMock';
import { storeId } from './renderComponentWithMockStore';
import { TransactionAdjustmentRequest } from 'pages/TransactionAdjustment/RequestList/types';
import { DEFAULT_FORM_ID_LIST } from 'pages/ClientConfiguration/AdjustmentTransaction/constants';
import { initialState as promiseToPayConfig } from 'pages/ClientConfiguration/PromiseToPay/_redux/reducers';
import { defaultTalkingPoint } from 'pages/ClientConfiguration/TalkingPoint/_redux/reducers';
import { SEND_LETTER_GRID_COLUMN_NAME } from 'pages/ClientConfiguration/SendLetter/constants';
import { ReferenceState } from 'pages/ClientConfiguration/References/types';
import { listCriteria } from 'app/entitlements/constants';

const emptyStateDefault: Partial<RootState> = {
  hardship: {},
  sharedInfo: { data: {} },
  accountCollection: {},
  accountDetailSnapshot: {},
  accountDetailTabs: {},
  accountInformation: {},
  accountSearchList: {},
  bankruptcy: {},
  cardholderMaintenance: {},
  cccs: {},
  cisMemo: {},
  clientContactInfo: {},
  collectionForm: {},
  company: {},
  deceased: {},
  demandACHPayment: {},
  form: {},
  letter: {},
  letterList: {},
  memo: {},
  memoChronicle: {},
  payment: {},
  paymentHistory: {},
  paymentsList: {},
  pendingAuthorization: {},
  pendingLetter: {},
  promiseToPay: {},
  refData: {},
  renewToken: {},
  schedulePayment: {},
  sendLetter: {},
  statementTransactionList: {},
  clientConfigurationDebtCompanies: {} as any,
  transactionAdjustment: {} as unknown as TransactionAdjustmentRequest,
  tab: {
    tabs: [],
    tabStoreIdSelected: ''
  },
  adjustmentTransactionConfig: {
    adjustmentConfigurationList: { loading: false, data: [], error: '' },
    addAdjustmentConfiguration: {
      loading: false,
      openModal: false,
      formIdList: DEFAULT_FORM_ID_LIST
    },
    deleteAdjustmentConfiguration: {
      openModal: false,
      loading: false
    },
    editAdjustmentConfiguration: {
      openModal: false,
      loading: false,
      validateError: ''
    }
  },
  timerWorking: {
    config: {},
    warningMessage: {},
    workingFromRecord: {},
    authenticateFromRecord: {},
    contactPerson: {},
    workLogged: {},
    workTypeRecord: {},
    lastCollectionCodeRecord: {}
  },
  callerAuthentication: {},
  generalInformation: {},
  longTermMedical: {},
  incarceration: {},
  authenticationConfig: {
    previousSourceConfig: [],
    previousQuestionConfig: [],
    currentSourceConfig: [],
    currentQuestionConfig: [],
    loading: false
  },
  promiseToPayConfig,
  promiseToPayHistory: {},
  relatedAccount: {},
  dashboard: {
    callActivity: {
      data: [],
      period: '',
      error: false,
      loading: false
    },
    goal: {
      data: [],
      error: '',
      loading: false
    },
    workQueue: {
      data: [],
      error: '',
      loading: false
    },
    workStatistic: {
      data: [],
      error: '',
      loading: false
    }
  },
  userSessionWarning: { endSessionTime: 0, disableBeforeUnload: false },
  clientConfigurationTalkingPoint: {
    criterion: 'criterion',
    originalTalkingPoint: { id: '1', panelContentText: defaultTalkingPoint },
    isPrevious: false,
    talkingPoint: { id: '1', panelContentText: defaultTalkingPoint },
    isLoading: false
  },
  workQueueList: {
    workQueue: {
      data: undefined,
      loading: false,
      error: '',
      page: 1,
      pageSize: 10,
      collectorValue: ''
    },
    userList: {
      data: [],
      loading: false,
      error: '',
      page: 1,
      pageSize: 10
    }
  },
  collectorList: {
    data: undefined,
    loading: false,
    error: ''
  },
  letterConfig: {
    letterConfigurationList: {
      loading: false,
      data: []
    },
    saveLetterModal: {
      open: false,
      letter: {
        letterId: '',
        letterCode: '',
        letterDescription: '',
        variableLetter: false
      }
    }
  },
  configurationUserRoleEntitlement: {
    textSearch: '',
    selectedCSPA: {} as CSPA,
    selectedUserRole: {} as RefDataValue,
    entitlements: [],
    entitlementsTextSearch: '',
    defaultEntitlements: [],
    sortBy: { id: 'clientId', order: undefined },
    isLoadingModal: false,
    isOpenModal: false,
    isLoading: true,
    userRoles: [],
    listRoleCSPA: [],
    listRoleEntitlements: [],
    isGetListCSPAError: false,
    isGetListEntitlementError: false,
    isNoResult: false,
    isOpenAddCSPA: false,
    addCSPAErrorMsg: '',
    isOpenStatusModal: false
  },
  functionRuleMapping: {
    functionRuleMappingForm: {
      loading: false,
      openAddModal: false,
      openEditModal: false
    },
    functionRuleMappingList: {
      loading: false,
      data: [],
      sortBy: { id: 'functionCode', order: undefined }
    }
  },
  clientConfigClusterToQueueMapping: {
    loading: false,
    loadingForm: false,
    cluster: {},
    clusterList: [],
    error: '',
    errorForm: '',
    isOpenModalForm: false,
    isOpenModalDelete: false,
    sortBy: { id: '' },
    queueList: []
  },
  sendLetterConfig: {
    sortBy: { id: SEND_LETTER_GRID_COLUMN_NAME.LETTER_STATE, order: undefined },
    sendLetterConfigurationList: { loading: false, data: [], error: '' },
    updateSendLetterConfig: {
      openModal: false,
      loading: false,
      actionType: ''
    },
    callFrequencyConfigurationList: { loading: false, data: [], error: '' },
    addCallFrequencyConfiguration: {
      loading: false,
      openModal: false
    },
    deleteCallFrequencyConfiguration: {
      openModal: false,
      loading: false
    },
    editCallFrequencyConfiguration: {
      openModal: false,
      loading: false
    }
  },
  clientConfigCallResult: { sortBy: { id: 'id' } },
  memosConfiguration: {
    memosConfiguration: {
      loading: false,
      data: {},
      error: false
    }
  },
  reference: {
    data: {}
  } as unknown as ReferenceState,
  configurationContactAccountEntitlement: {
    entitlements: [],
    openStatusModal: false,
    isLoadingModal: false,
    isLoadingListCSPA: false,
    listContactAccountCSPA: [],
    selectedCSPA: {} as CSPA,
    textSearch: '',
    isNoResultsFound: true,
    isErrorSettingEntitlement: false,
    isErrorLoadingListCSPA: false,
    isOpenViewElement: false,
    sortBy: { id: 'clientId', order: undefined },
    activeTabEntitlement: {},
    activeEntitlement: {},
    listActiveEntitlements: [],
    listCriteria,
    listCSPA: [],
    isToggle: false,
    isSelectedAll: false,
    isOpenAddCSPA: false,
    addCSPAErrorMsg: ''
  },
  entitlement: {},
  accessToken: {},
  changeHistory: {
    sortType: {
      id: ''
    }
  },
  liveVoxReducer: {
    sessionId: '',
    isAgentAuthenticated: false,
    lineNumber: '',
    accountNumber: '',
    call: {
      accountNumber: '',
      accountNumberRequired: false,
      callCenterId: -1,
      callRecordingStarted: false,
      callSessionId: '',
      callTransactionId: '',
      phoneNumber: '',
      serviceId: -1
    },
    inCallAccountNumber: '',
    lastInCallAccountNumber: ''
  }
};

const selectorWrapper =
  (states: Partial<RootState>, emptyState = emptyStateDefault) =>
  (fn: Function) => {
    const data = useSelectorMock(fn, states, storeId);

    const emptyData = useSelectorMock(fn, emptyState, storeId);

    return { data, emptyData };
  };

export { selectorWrapper };
