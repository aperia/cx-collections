import React from 'react';
import { App as DOFApp } from 'app/_libraries/_dof/core';
import { ReducersMapObject } from 'redux';
import { reducerMappingList } from 'storeConfig';
import { urlConfigs } from 'App';

export interface DOFAppWrapperProps {
  customReducers?: ReducersMapObject<any, any>;
}

export const DOFAppWrapper: React.FC<DOFAppWrapperProps> = ({
  customReducers,
  children
}) => (
  <DOFApp
    customReducers={customReducers || reducerMappingList}
    urlConfigs={urlConfigs}
  >
    {children}
  </DOFApp>
);
