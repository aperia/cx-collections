let spy: jest.SpyInstance;

// support mock action, dev don't need to mock dispatch()
const mockActionCreator = (actionObj: any) => (action: string, mock?: any) => {
  const mockAction = mock
    ? mock
    : jest.fn().mockImplementation(() => ({ type: 'type' }));

  spy = jest.spyOn(actionObj, `${action}`).mockImplementation(mockAction);

  return mockAction;
};

afterEach(() => {
  spy?.mockReset();
  spy?.mockRestore();
});

export { mockActionCreator };
