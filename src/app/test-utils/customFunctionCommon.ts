import { IValueChangeFunc } from 'app/_libraries/_dof/core';

export const mockOnFindGenerator = ({
  setVisible = jest.fn(),
  setValue = jest.fn(),
  dependentField = [],
  setReadOnly = jest.fn(),
  setOthers = jest.fn(),
  setData = jest.fn(),
  setEnable = jest.fn(),
  setRequired = jest.fn(),
  value,
  data,
  dependentType
}: {
  setVisible?: jest.Mock;
  setValue?: jest.Mock;
  dependentField?: any[];
  setReadOnly?: jest.Mock;
  setOthers?: jest.Mock | Function;
  setData?: jest.Mock;
  setEnable?: jest.Mock;
  setRequired?: jest.Mock;
  value?: any;
  data?: any;
  dependentType?: string;
}) =>
  jest.fn(() => ({
    value,
    props: {
      props: {
        dependentField,
        setVisible,
        setValue,
        setReadOnly,
        setOthers,
        setData,
        setEnable,
        data,
        setRequired,
        dependentType
      }
    },
    ref: { current: { props: { value } } }
  }));

export const customFunctionCommon = (
  fn: IValueChangeFunc,
  params:
    | {
        newValue?: any;
        onFind?: jest.SpyInstance;
        formKy?: string;
        setFormValues?: jest.Mock;
        formValues?: any;
        previousValue?: any;
        name?: string;
        event?: any;
      }
    | undefined = {
    newValue: undefined,
    onFind: jest.fn(x => null),
    setFormValues: jest.fn(),
    event: {},
    formValues: {},
    formKy: '',
    previousValue: '',
    name: 'mockName'
  }
): Promise<any> => {
  const result = fn(
    params.name!,
    params.newValue,
    params.previousValue,
    params.onFind as never,
    params.formKy,
    params.setFormValues,
    params.event,
    params.formValues
  );
  return result as unknown as Promise<any>;
};
