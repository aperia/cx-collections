import mainJson from '../../../../public/config/main.json';

export const mockInnerWidth = (value: number) => {
  return Object.defineProperty(window, 'innerWidth', {
    configurable: true,
    writable: true,
    value
  });
};

export const mockInnerHeight = (value: number) => {
  return Object.defineProperty(window, 'innerHeight', {
    writable: true,
    value
  });
};

export const mockClientHeight = (value: number) => {
  return Object.defineProperty(HTMLElement.prototype, 'clientHeight', {
    writable: true,
    value
  });
};

export const mockScrollHeight = (value: number) => {
  return Object.defineProperty(HTMLElement.prototype, 'scrollHeight', {
    writable: true,
    value
  });
};

export const mockScrollTop = (value: number) => {
  return Object.defineProperty(HTMLElement.prototype, 'scrollTop', {
    writable: true,
    value
  });
};

export const mockScrollToFn = (fn: Function) => {
  return Object.defineProperty(HTMLElement.prototype, 'scrollTo', {
    writable: true,
    value: fn
  });
};

export const apiUrl = mainJson.api;
export const mappingUrl = mainJson.mappingUrl;

export const mockAppConfigApi = {
  setup: () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: { api: apiUrl, mappingUrl }
    });
  },
  clear: () => {
    Object.defineProperty(window, 'appConfig', {
      writable: true,
      value: {}
    });
  }
};
