import React, {
  forwardRef,
  RefForwardingComponent,
  useImperativeHandle,
  useState,
  useEffect,
  useRef
} from 'react';

// types
import { ViewProps } from 'app/_libraries/_dof/core';
import { FormInstance } from 'redux-form';
import isFunction from 'lodash.isfunction';

export const storeIdViewNull = '123456789-view-ref-null';
export const storeIdViewNoValue = '123456789-view-no-value';
export const storeIdViewNotFound = '123456789-view-not-found';

const MockViewChangeValues: RefForwardingComponent<
  FormInstance<any, React.PropsWithChildren<any>, string>,
  ViewProps
> = ({ value: valueProp, formKey, descriptor, onChange }, ref) => {
  const [value, setValue] = useState(valueProp);
  const refResult = useRef<any>({});

  useEffect(() => { setValue(valueProp) }, [valueProp]);

  const onFind = (fieldId: string) => {
    if (formKey.includes(storeIdViewNotFound)) return null;

    const onFindProps = {
      props: {
        setData: () => undefined,
        setValue: () => undefined,
        setVisible: () => undefined,
        setEnable: () => undefined,
        setReadOnly: () => undefined,
        setRequired: () => undefined,
        setOthers: (param: Function | MagicKeyValue) => {
          const prev = {
            radios: [
              {
                description: 'txt_pending_proposal',
                value: 'pendingProposal',
                readOnly: true
              }
            ]
          };
          if (isFunction(param)) return param(prev);
          return;
        }
      }
    };

    if (formKey.includes(storeIdViewNoValue))
      return {
        props: onFindProps
      };

    if (value && value[fieldId])
      return {
        props: onFindProps
      };

    return {
      value,
      props: onFindProps
    };
  };

  const current = {
    props: { onFind },
    lastFieldValidatorKeys: ['lastFieldValidatorKeys'],
    values: value
  };

  refResult.current.props = { onFind };
  refResult.current.lastFieldValidatorKeys = ['lastFieldValidatorKeys'];
  refResult.current.values = value;
  refResult.current.ref = { current };

  useImperativeHandle(ref, () => {
    return formKey.includes(storeIdViewNull)
      ? (null as unknown as FormInstance<
          any,
          React.PropsWithChildren<any>,
          string
        >)
      : (refResult.current as unknown as FormInstance<
          any,
          React.PropsWithChildren<any>,
          string
        >);
  });

  return (
    <div data-testid={descriptor}>
      {JSON.stringify(value)}{' '}
      <input
        data-testid={`${descriptor}_onChange`}
        onChange={(e: any) => {
          if (onChange) {
            onChange(
              e.target.values,
              jest.fn(),
              e.target.viewProps,
              e.target.previousValues
            )
          } else {
            setValue(e.target.values);
          }
          
        }
        }
      />
    </div>
  );
};

export default forwardRef<
  FormInstance<any, React.PropsWithChildren<any>, string>,
  ViewProps
>(MockViewChangeValues);
