import React, { forwardRef, useImperativeHandle } from 'react';
import { useSelector } from 'react-redux';
// types
import { TextEditorProps } from 'app/_libraries/_dls/components/TextEditor/types';
import { PayByPhoneItem } from 'pages/ClientConfiguration/PayByPhone/types';

// selectors
import { selectCurrentConfig } from 'pages/ClientConfiguration/PayByPhone/_redux/selectors';

// constants
import {
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE,
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE
} from 'pages/ClientConfiguration/PayByPhone/constants';

const MockTextEditor = (props: TextEditorProps, ref: any) => {
  const currentValue =
    useSelector<RootState, PayByPhoneItem>(selectCurrentConfig);

  const {
    paymentAgreementMessageWithConvenienceFee,
    paymentAgreementMessageWithoutConvenienceFee
  } = currentValue;

  const generateMockData = () => {
    if (
      !paymentAgreementMessageWithConvenienceFee ||
      !paymentAgreementMessageWithoutConvenienceFee
    ) {
      return '';
    }

    if (
      paymentAgreementMessageWithConvenienceFee ===
      DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE
    ) {
      return DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE;
    }

    if (
      paymentAgreementMessageWithoutConvenienceFee ===
      DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE
    ) {
      return DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE;
    }

    return 'Mock value @[Data Value](DataValue)';
  };

  useImperativeHandle(ref, () => ({
    getMarkdown: generateMockData,
    setMarkdown: () => undefined
  }));

  return (
    <input
      data-testid={props.dataTestId}
      ref={ref}
      onBlur={props.onBlur}
      onClick={() => props.onPlainTextChange!('Message content')}
    />
  );
};
export default forwardRef(MockTextEditor);
