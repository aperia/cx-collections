import React, {
  forwardRef,
  RefForwardingComponent,
  useImperativeHandle,
  useMemo,
  useRef
} from 'react';

import { GridRef, GridProps } from 'app/_libraries/_dls/components/Grid/types';
import Grid from 'app/_libraries/_dls/components/Grid';

const MockGrid: RefForwardingComponent<GridRef, GridProps> = (props, ref) => {
  const { onSortChange, onLoadMore, loadElement } = props;

  const bodyRef = useRef<HTMLDivElement | null>(null);

  const gridRef = useMemo(
    () => ({
      gridBodyElement: {
        scroll: () => undefined,
        scrollTo: () => undefined
      }
    }),
    []
  );

  useImperativeHandle(ref, () => gridRef as GridRef);

  return (
    <>
      {loadElement}
      <div ref={bodyRef} />
      <input
        data-testid="Grid_onSortChange"
        onChange={({ target: { id, order } }: any) =>
          onSortChange && onSortChange({ id, order })
        }
      />
      <button
        data-testid="Grid_onLoadMore"
        onClick={() => onLoadMore && onLoadMore()}
      />

      {/* render Components in columns */}
      <Grid {...props} loadElement={<></>} />
    </>
  );
};

const MockGridBase = forwardRef<GridRef, GridProps>(MockGrid);

// export default not work with jest.requireActual
export { MockGridBase as MockGrid };
