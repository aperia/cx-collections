const entry = {
  contentRect: { width: 100 }
};

class ResizeObserver {
  constructor(callback: Function) {
    callback([entry]);
  }

  static observe: Function;
  static unobserver: Function;
  static disconnect: Function;

  observe = () => undefined;
  unobserve = () => undefined;
  disconnect = () => undefined;
}

export default ResizeObserver;
