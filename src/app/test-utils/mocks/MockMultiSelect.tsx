import React from 'react';
import { MultiSelectProps } from 'app/_libraries/_dls/components/MultiSelect';
import { DropdownBaseItemProps } from 'app/_libraries/_dls/components';

const MockMultiSelect = (props: MultiSelectProps) => {
  const { onChange, children, label, onBlur, error, groupFormatInputText } =
    props;
  return (
    <>
      <input
        data-testid="MultiSelect.Input_onChange"
        onChange={onChange as never}
        onBlur={onBlur}
      />
      <button
        data-testid="MultiSelect_groupFormatInputText"
        onClick={() => groupFormatInputText?.(3, 3)}
      >
        groupFormatInputText
      </button>
      {!!error && <p data-testid="MultiSelect.Error">{error.message}</p>}
      <span>{label}</span>
      {children}
    </>
  );
};

MockMultiSelect.Item = ({ value, label }: DropdownBaseItemProps) => (
  <div data-testid={`MultiSelect.Item-${label}`}>{label}</div>
);

export default MockMultiSelect;
