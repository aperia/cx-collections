import React from 'react';
import {
  DropdownBaseChangeEvent,
  DropdownBaseItemProps,
  DropdownListProps
} from 'app/_libraries/_dls/components';
import isFunction from 'lodash.isfunction';

const MockDropdownList = (props: DropdownListProps) => {
  const {
    onChange,
    label,
    onBlur,
    error,
    textFieldRender,
    value,
    children
  } = props;

  return (
    <>
      DropdownList
      <div>{children}</div>
      <span>{label}</span>
      <input
        data-testid="DropdownList.onChange"
        onChange={(e: any) =>
          onChange!({
            target: { value: e.target.valueObject }
          } as DropdownBaseChangeEvent)
        }
      />
      <input data-testid="DropdownList.onBlur" onBlur={onBlur} />
      {!!error && <p data-testid="DropdownList.Error">{error.message}</p>}
      {textFieldRender && isFunction(textFieldRender) && textFieldRender(value)}
    </>
  );
};

MockDropdownList.Item = ({ value, label }: DropdownBaseItemProps) => (
  <div key={`${value}-${label}`} data-testid={`DropdownList.Item-${label}`}>
    {label}
  </div>
);

export default MockDropdownList;
