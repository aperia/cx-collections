const observeFn = jest.fn();
const unobserveFn = jest.fn();
const intersectionObserver = ({ entries = { isIntersecting: true } }) =>
  class IntersectionObserver {
    constructor(callback: Function, options: IntersectionObserverInit) {
      callback([entries], options);
    }

    observe: (target: Element) => void = observeFn;
    unobserve: (target: Element) => void = unobserveFn;
  };

export default intersectionObserver;
