import { generateEntries } from './generateEntries';

class ResizeObserver {
  constructor(callback: Function) {
    const entries = generateEntries();
    callback(entries);
  }

  static observe: Function;
  static disconnect: Function;

  observe = () => undefined;
  unobserve = () => undefined;
  disconnect = () => undefined;
}

export default ResizeObserver;
