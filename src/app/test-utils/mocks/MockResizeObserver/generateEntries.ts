// control entities which will be use as a param of callback by MockResizeObserver
export const generateEntries = () => {
    return [{ contentRect: { width: 100 } }];
  };
  