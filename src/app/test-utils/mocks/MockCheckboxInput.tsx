import React from 'react';
import { InputProps } from 'app/_libraries/_dls/components/CheckBox/Input';

jest.mock(
  'app/_libraries/_dls/components/CheckBox/Input',
  () => ({ onChange }: InputProps) => {
    return (
      <input
        data-testid="CheckBox.Input_onChange"
        onChange={e => onChange!(e)}
      />
    );
  }
);
