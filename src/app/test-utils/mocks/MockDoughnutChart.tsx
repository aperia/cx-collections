import React from 'react';
import isFunction from 'lodash.isfunction';
import { DoughnutChartProps } from 'app/_libraries/_dls/components/DoughnutChart';

const MockDoughnutChart = (props: DoughnutChartProps) => {
  const { size, legendValueRender, legendLabelRender, allowToggleLegend } =
    props as any;

  return (
    <>
      <div data-testid="doughnut-chart">MockDoughnutChart</div>
      <div>{size}</div>
      <span>{allowToggleLegend}</span>
      {legendValueRender &&
        isFunction(legendValueRender) &&
        legendValueRender(10)}
      {legendValueRender &&
        isFunction(legendValueRender) &&
        legendValueRender(undefined)}
      {legendLabelRender &&
        isFunction(legendLabelRender) &&
        legendLabelRender('value')}
    </>
  );
};

export default MockDoughnutChart;
