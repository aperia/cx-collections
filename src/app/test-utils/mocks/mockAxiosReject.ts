import { AxiosRequestConfig } from 'axios';

const mockAxiosReject = jest.fn();

jest.mock('axios', () => {
  const axios = jest.requireActual('axios');

  const mockAction = (params?: AxiosRequestConfig) => {
    mockAxiosReject(params);
    return Promise.reject({ data: { isMock: true } });
  };

  const axiosMock = (params?: AxiosRequestConfig) => mockAction(params);

  axiosMock.get = (params?: AxiosRequestConfig) => mockAction(params);
  axiosMock.create = axios.create;

  return {
    ...axios,
    __esModule: true,
    default: axiosMock,
    defaults: axiosMock
  };
});

export { mockAxiosReject };
