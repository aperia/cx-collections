import React from 'react';
import { TooltipProps } from 'app/_libraries/_dls/components';

jest.mock(
  'app/_libraries/_dls/components/Tooltip',
  () => ({ children, element, opened, onVisibilityChange }: TooltipProps) => (
    <div>
      <input
        data-testid="Tooltip_onVisibilityChange"
        onChange={(e: any) => onVisibilityChange!(e.target.isOpened)}
      />
      {opened && element}
      {children}
    </div>
  )
);
