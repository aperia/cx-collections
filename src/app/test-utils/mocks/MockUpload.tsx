import React, { useImperativeHandle, useRef } from 'react';
import { UploadProps, UploadRef } from 'app/_libraries/_dls/components/Upload';

const Component: React.RefForwardingComponent<UploadRef, UploadProps> = (
  props,
  ref
) => {
  const cancelTokenFiles = useRef(null);
  const uploadRef = useRef(null);
  const containerRef = useRef(null);

  useImperativeHandle(
    ref,
    () => ({
      cancelTokenFiles: cancelTokenFiles.current!,
      inputElement: uploadRef.current!,
      containerElement: containerRef.current!,
      uploadFiles: jest.fn(),
      files: []
    }),
    []
  );

  return (
    <>
      <div>Upload</div>
      <div onClick={() => props.onChangeFile!({ files: [] })}>onChangeFile</div>
      <div
        onClick={() =>
          props.onChangeFile!({
            files: [{ idx: '1', status: 4, length: 19, item: () => null }]
          })
        }
      >
        onChangeFileFail
      </div>
      <div onClick={() => props.onFinally!([{ status: 'fulfilled' }])}>
        onFinally
      </div>
      <div
        onClick={() =>
          props.onFinally!([{ status: 'fulfilled' }, { status: 'rejected' }])
        }
      >
        onFinallyFail
      </div>
      <div onClick={() => props.onRemove!(0)}>onRemove</div>
      <div onClick={() => props.saveUrl!([{ fileId: '0' }], jest.fn())}>
        saveUrl
      </div>
      <div>
        {props.renderElm!({
          files: [{ idx: '1', length: 19, item: () => null }]
        })}
      </div>
    </>
  );
};

const MockUpload = React.forwardRef<UploadRef, UploadProps>(Component);

export { MockUpload };
