import React from 'react';
import { ComboBoxProps } from 'app/_libraries/_dls/components/ComboBox';
import { DropdownBaseItemProps } from 'app/_libraries/_dls/components';

const MockComboBox = (props: ComboBoxProps) => {
  const { onChange, children, label, onBlur, error } = props;
  return (
    <>
      <input
        data-testid="ComboBox.Input_onChange"
        onChange={onChange as never}
        onBlur={onBlur}
      />
      {!!error && <p data-testid="ComboBox.Error">{error.message}</p>}
      <span>{label}</span>
      {children}
    </>
  );
};

MockComboBox.Item = ({ value, label }: DropdownBaseItemProps) => (
  <div data-testid={`ComboBox.Item-${label}`}>{label}</div>
);

export default MockComboBox;
