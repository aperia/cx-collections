import React, { forwardRef, useImperativeHandle } from 'react';
import { useSelector } from 'react-redux';
// types
import { TextEditorProps } from 'app/_libraries/_dls/components/TextEditor/types';
import { SettlementPaymentItem } from 'pages/ClientConfiguration/SettlementPayment/types';

// selectors
import { selectCurrentConfig } from 'pages/ClientConfiguration/SettlementPayment/_redux/selectors';

// constants

import {
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE,
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE,
  DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE
} from 'pages/ClientConfiguration/SettlementPayment/constants';

const MockTextEditor = (props: TextEditorProps, ref: any) => {
  const currentValue =
    useSelector<RootState, SettlementPaymentItem>(selectCurrentConfig);

  const {
    agreementMessage,
    agreementMessageWithConvenienceFee,
    agreementMessageWithoutConvenienceFee
  } = currentValue;

  const generateMockData = () => {
    if (
      !agreementMessage ||
      !agreementMessageWithConvenienceFee ||
      !agreementMessageWithoutConvenienceFee
    ) {
      return '';
    }

    if (agreementMessage === DEFAULT_PAYMENT_AGREEMENT_MESSAGE) {
      return DEFAULT_PAYMENT_AGREEMENT_MESSAGE;
    }

    if (
      agreementMessageWithConvenienceFee ===
      DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE
    ) {
      return DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITH_FEE;
    }

    if (
      agreementMessageWithoutConvenienceFee ===
      DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE
    ) {
      return DEFAULT_PAYMENT_AGREEMENT_MESSAGE_WITHOUT_FEE;
    }

    return 'Mock value @[Data Value](DataValue)';
  };

  useImperativeHandle(ref, () => ({
    getMarkdown: generateMockData,
    setMarkdown: () => undefined
  }));

  return (
    <input
      data-testid={props.dataTestId}
      ref={ref}
      onBlur={props.onBlur}
      onClick={() => props.onPlainTextChange!('Message content')}
    />
  );
};
export default forwardRef(MockTextEditor);
