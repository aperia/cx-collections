import { Dispatch } from '@reduxjs/toolkit';

interface dataProps {
  value?: any;
  id?: string;
  name?: string;
  label?: string;
  meta?: {
    active?: boolean;
    asyncValidating?: boolean;
    autofilled?: boolean;
    initial?: string;
    form?: string;
    dirty?: boolean;
    dispatch?: any;
    invalid?: boolean;
    pristine?: boolean;
    submitFailed?: boolean;
    submitting?: boolean;
    touched?: boolean;
    valid?: boolean;
    visited?: boolean;
  };
  onChange?: (e: any) => void;
  onBlur?: () => void;
  onFocus?: () => void;
  options?: any;
  readOnly?: boolean;
  required?: boolean;
  mask?: Array<{
    isRegex: boolean;
    pattern: string;
  }>;
}

export const getDefaultDOFMockProps = (data?: dataProps) => {
  const defaultFunc = () => {};

  return {
    input: {
      value: data?.value,
      onChange: data?.onChange || defaultFunc,
      onBlur: data?.onBlur || defaultFunc,
      onFocus: data?.onFocus || defaultFunc,
      onDrop: () => {},
      onDragStart: () => {},
      name: data?.name || ''
    },
    meta: {
      active: false,
      asyncValidating: false,
      autofilled: false,
      initial: undefined,
      form: data?.meta?.form ?? '',
      dispatch: null as unknown as Dispatch<any>,
      dirty: false,
      invalid: data?.meta?.invalid || false,
      pristine: true,
      submitFailed: false,
      submitting: false,
      touched: data?.meta?.touched || false,
      valid: true,
      visited: false
    },
    id: data?.id || '123456789',
    options: data?.options || undefined,
    required: data?.required || false,
    readOnly: data?.readOnly || false,
    enable: true,
    mask: data?.mask || undefined,
    label: data?.label || ''
  };
};
