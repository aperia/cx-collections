export const useSelectorMock = (
  fn: Function,
  state: Partial<RootState> | null,
  accountId: string | undefined
) => {
  return fn(state, accountId);
};
