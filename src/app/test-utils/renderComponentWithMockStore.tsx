import React from 'react';
import { Provider } from 'react-redux';
import { Formik, Form, FormikProps } from 'formik';

import {
  createStore,
  applyMiddleware,
  combineReducers,
  AnyAction
} from 'redux';

import { caches } from 'app/_libraries/_dof/core/redux/reducers';
// react-testing-library
import { Matcher, queryHelpers, render } from '@testing-library/react';
import { renderHook } from '@testing-library/react-hooks';

// DOF
import { DOFAppWrapper } from './DOFAppWrapper';

// Redux
import { reducerMappingList, rootReducer } from 'storeConfig';
import thunk, { ThunkMiddleware } from 'redux-thunk';

import data from '../../../public/mapping/all.json';
import { MappingStateData } from 'pages/__commons/MappingProvider/types';
import { mockApiMapping } from './mockApiMapping';
import I18next from 'pages/__commons/I18nextProvider';
import { AccountDetailProvider } from 'app/hooks';
import { AxiosResponse } from 'axios';

type typeMapping = Partial<MappingStateData>;

export interface StoreConfigProps {
  initialState?: Partial<RootState>;
  mapping?: typeMapping;
  middleware?: ThunkMiddleware<{}, AnyAction, {}>[];
}

export const storeId = '123456789';
export const accEValue = 'accEValue';
export const accNbr = '323*****322';
export const memberSequenceIdentifierPrimary = '00001';
export const mockMemberSequenceIdentifier = 'memberSequenceIdentifier';
export const mockSocialSecurityIdentifier = 'socialSecurityIdentifier';

export const mockMiddleware = [thunk.withExtraArgument({ ...mockApiMapping })];

export const queryByClass = (container: HTMLElement, id: Matcher) => {
  return queryHelpers.queryByAttribute('class', container, id);
};

export const queryAllByClass = (container: HTMLElement, id: Matcher) => {
  return queryHelpers.queryAllByAttribute('class', container, id);
};

export const queryByName = (container: HTMLElement, id: Matcher) => {
  return queryHelpers.queryByAttribute('name', container, id);
};

export const queryAllByName = (container: HTMLElement, id: Matcher) => {
  return queryHelpers.queryAllByAttribute('name', container, id);
};

export const queryById = (container: HTMLElement, id: Matcher) => {
  return queryHelpers.queryByAttribute('id', container, id);
};

export const queryAllById = (container: HTMLElement, id: Matcher) => {
  return queryHelpers.queryAllByAttribute('id', container, id);
};

export const queryAllBy = (
  container: HTMLElement,
  type: string,
  id: Matcher
) => {
  return queryHelpers.queryAllByAttribute(type, container, id);
};

export const queryBy = (container: HTMLElement, type: string, id: Matcher) => {
  return queryHelpers.queryByAttribute(type, container, id);
};

export const responseDefault: AxiosResponse<any> = {
  data: null,
  status: 200,
  statusText: '',
  headers: {},
  config: {}
};

export const mockCreateStore = (
  initialState: MagicKeyValue,
  middleware = mockMiddleware,
  mapping?: MagicKeyValue
) =>
  createStore(
    combineReducers({ ...reducerMappingList, caches }),
    { ...initialState, mapping: { data: mapping || data } },
    applyMiddleware(...middleware)
  );

export const renderHookWithStore = <TProps, TResult>(
  callback: (props: TProps) => TResult,
  storeConfig?: StoreConfigProps
) => {
  const { initialState = {}, mapping, middleware } = storeConfig || {};

  const store = mockCreateStore(initialState, middleware, mapping);
  return renderHook(callback, {
    wrapper: ({ children }) => (
      <Provider store={store}>
        <I18next>{children}</I18next>
      </Provider>
    )
  });
};

export const setupWrapper = (
  element?: React.ReactElement,
  storeConfig?: StoreConfigProps,
  isWrappedByDOF?: boolean
) => {
  const { initialState = {}, mapping, middleware } = storeConfig || {};

  const store = mockCreateStore(initialState, middleware, mapping);

  const wrapper = isWrappedByDOF ? (
    <DOFAppWrapper>
      <Provider store={store}>
        <I18next>{element}</I18next>
      </Provider>
    </DOFAppWrapper>
  ) : (
    <Provider store={store}>
      <I18next>{element}</I18next>
    </Provider>
  );

  return { wrapper, store };
};

export const renderMockStore = (
  element?: React.ReactElement,
  storeConfig?: StoreConfigProps,
  isWrappedByDOF?: boolean
) => {
  const { wrapper: preWrapper, store } = setupWrapper(
    element,
    storeConfig,
    isWrappedByDOF
  );

  const wrapper = render(preWrapper);

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement,
    container: wrapper.container as HTMLElement,
    store,
    rerender: (element: React.ReactElement) =>
      wrapper.rerender(
        setupWrapper(element, storeConfig, isWrappedByDOF).wrapper
      )
  };
};

export const renderMockStoreId = (
  element?: React.ReactElement,
  storeConfig?: StoreConfigProps,
  isProvider = true,
  isI18next = true
) => {
  const { initialState = {}, mapping, middleware } = storeConfig || {};

  const store = mockCreateStore(initialState, middleware, mapping);

  const preWrapper = (
    <DOFAppWrapper>
      <Provider store={store}>
        <I18next>
          <AccountDetailProvider
            value={{
              storeId,
              accEValue,
              memberSequenceIdentifier: mockMemberSequenceIdentifier,
              socialSecurityIdentifier: mockSocialSecurityIdentifier
            }}
          >
            {element}
          </AccountDetailProvider>
        </I18next>
      </Provider>
    </DOFAppWrapper>
  );

  return {
    wrapper: render(preWrapper),
    store,
    rerender: (element: React.ReactElement) =>
      render(preWrapper).rerender(
        setupWrapper(element, storeConfig, isProvider).wrapper
      )
  };
};

export const createStoreWithDefaultMiddleWare = (
  initialState: Partial<RootState>
) => createStore(rootReducer, initialState, applyMiddleware(thunk));

export const FormikContainer = ({
  initialValues,
  validationSchema,
  children,
  validate
}: any) => {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={validationSchema}
      validate={validate}
      onSubmit={() => {}}
      validateOnBlur
      validateOnMount
    >
      {(props: FormikProps<any>) => <Form>{children}</Form>}
    </Formik>
  );
};
