import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

// store
import { createReducer } from 'app/_libraries/_dof/core/redux/createAppStore';
import { reducerMappingList } from 'storeConfig';

export function wait(amount = 0) {
  return new Promise(resolve => setTimeout(resolve, amount));
}

export function wrapperComponent(
  component: React.ReactNode,
  initialState: Record<string, unknown> = {}
) {
  const rootReducer = createReducer(reducerMappingList);
  const store = createStore(rootReducer, initialState, applyMiddleware(thunk));

  return <Provider store={store}>{component}</Provider>;
}

const byPassFulfilled = (type = '', payload: MagicKeyValue | null = null) => {
  return jest.fn().mockResolvedValue({
    meta: {
      arg: {},
      requestStatus: 'fulfilled',
      requestId: 'mockRequestID'
    },
    type,
    payload
  });
};

const byPassRejected = (type = '', payload: MagicKeyValue | null = null) => {
  return jest.fn().mockResolvedValue({
    meta: {
      arg: {},
      requestStatus: 'rejected',
      requestId: 'mockRequestID'
    },
    type,
    payload,
    error: {}
  });
};

const byPassPending = (type = '', payload: MagicKeyValue | null = null) => {
  return jest.fn().mockResolvedValue({
    meta: {
      arg: {},
      requestStatus: 'pending',
      requestId: 'mockRequestID'
    },
    type,
    payload,
    error: {}
  });
};

export const specificName = 'specific action name';

const byPassRejectedSpecificAction = (
  type = '',
  payloadProp: MagicKeyValue | null = null
) => {
  return jest.fn().mockImplementation((payload: string) => {
    if (payload === specificName)
      return jest.fn().mockResolvedValue({
        meta: {
          arg: {},
          requestStatus: 'rejected',
          requestId: 'mockRequestID'
        },
        type,
        payload,
        error: {}
      })();

    return jest.fn().mockRejectedValue({ payload: payloadProp || payload });
  });
};

export const convertToExpectedKeys = ({
  payload,
  mappingRule,
  removeKeys = []
}: {
  payload: any;
  mappingRule: any;
  removeKeys?: string[];
}) => {
  const removedKeysObject = { ...payload };

  removeKeys?.forEach(key => {
    delete removedKeysObject[key];
  });

  return Object.keys(removedKeysObject).reduce(
    (mappedObj: any, key: string) => {
      const newKey = mappingRule[key] || key;
      mappedObj[newKey] = removedKeysObject[key];
      return mappedObj;
    },
    {}
  );
};

export {
  byPassFulfilled,
  byPassRejected,
  byPassPending,
  byPassRejectedSpecificAction
};
