export const waitFakeTime = (callback: Function) => {
  jest.useFakeTimers();

  callback();

  jest.runAllTimers();
  jest.useRealTimers();
};
