import { FULFILLED, REJECTED } from 'app/constants';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import { byPassFulfilled, byPassRejected, responseDefault } from '.';

export const mockAsyncThunk = ({
  service,
  fn = {},
  triggerFn,
  customFn
}: any) => {
  let methodName = '';

  if (fn['typePrefix']) {
    const typePrefixes = fn['typePrefix'].split('/') as any[];

    methodName = typePrefixes[typePrefixes.length - 1];
  }

  methodName = customFn || methodName;
  return ({ args }: any) => {
    return async ({
      actionState = FULFILLED,
      payload = {},
      initialState = {}
    }) => {
      jest.spyOn(service, methodName).mockResolvedValue({
        ...responseDefault,
        data: payload
      });

      const triggerAction = triggerFn || fn;

      let byPassDispatchAction = byPassFulfilled(fn.fulfilled.type, payload);

      if (actionState === REJECTED) {
        jest
          .spyOn(service, methodName)
          .mockRejectedValue(new Error('rejected error'));
        byPassDispatchAction = byPassRejected(fn.rejected.type, payload);
      }

      const store = createStore(
        rootReducer,
        initialState,
        applyMiddleware(thunk)
      );

      const result = await triggerAction(args)(
        byPassDispatchAction,
        store.getState,
        {}
      );

      return {
        result
      };
    };
  };
};
