import React, { useContext } from 'react';

interface AccountDetailContextProps {
  storeId: string;
  accEValue: string;
  accNbr?: string;
  memberSequenceIdentifier?: string;
  customerRoleTypeCode?: string;
  socialSecurityIdentifier?: string;
}

const AccountDetailContext = React.createContext<AccountDetailContextProps>({
  memberSequenceIdentifier: '',
  accEValue: '',
  storeId: '',
  accNbr: ''
});

export const useAccountDetail = () => {
  return useContext<AccountDetailContextProps>(AccountDetailContext);
};

export const AccountDetailProvider = ({
  children,
  value
}: {
  children: React.ReactNode;
  value: AccountDetailContextProps;
}) => {
  return (
    <AccountDetailContext.Provider value={value}>
      {children}
    </AccountDetailContext.Provider>
  );
};
