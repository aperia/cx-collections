import { useState, useEffect } from 'react';

// observer
import ResizeObserver from 'resize-observer-polyfill';

// constants
import { DESKTOP_WIDTH } from 'app/constants';

// utils
import get from 'lodash.get';

export interface DesktopScreenSizeHook {
  (): boolean;
}

const useDesktopScreenSize: DesktopScreenSizeHook = () => {
  const initialWidth = get(document, ['documentElement', 'clientWidth'], 0);

  const [isDesktopScreen, setIsDesktopScreen] = useState(
    initialWidth > DESKTOP_WIDTH
  );

  useEffect(() => {
    const handleCheckScreen = (entries: ResizeObserverEntry[]) => {
      if (!get(entries, '0')) return;

      const screenWidth = get(document, ['documentElement', 'clientWidth'], 0);
      setIsDesktopScreen(screenWidth > DESKTOP_WIDTH);
    };

    const resizeObserver = new ResizeObserver(handleCheckScreen);
    resizeObserver.observe(document.documentElement);

    return () => resizeObserver.unobserve(document.documentElement);
  }, []);

  return isDesktopScreen;
};

export { useDesktopScreenSize };
