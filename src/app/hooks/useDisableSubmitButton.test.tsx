import { useDisableSubmitButton } from './useDisableSubmitButton';
import { renderHookWithStore } from 'app/test-utils';
 
describe('test useDisableSubmitButton', () => {
  const mNextValue = {
    key: 123,
    key2: undefined
  }
  it('should be return false', () => {
    const { result } = renderHookWithStore(
      () => useDisableSubmitButton(mNextValue),
      { initialState: {} }
    );
    const { disableSubmit, setPrimitiveValues } = result.current;
    setPrimitiveValues({
      key: 123,
      key2: undefined
    });
    expect(disableSubmit).toEqual(false);
  });

  it('should be return false > case 2', () => {
    const { result } = renderHookWithStore(
      () => useDisableSubmitButton(null),
      { initialState: {} }
    );
    const { disableSubmit, setPrimitiveValues } = result.current;
    setPrimitiveValues(null);
    expect(disableSubmit).toEqual(true);
  });
});