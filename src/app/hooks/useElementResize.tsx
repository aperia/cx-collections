import { useEffect, useState } from 'react';
import ResizeObserver from 'resize-observer-polyfill';
import debounce from 'lodash.debounce';

export const useElementSize = (
  ref: React.RefObject<HTMLDivElement | HTMLElement | undefined>
) => {
  const [contentRect, setContentRect] = useState<DOMRectReadOnly>();

  useEffect(() => {
    if (!ref.current) return;
    const calculateSize = new ResizeObserver(
      debounce(
        entries => {
          setContentRect(entries[0].contentRect as DOMRectReadOnly);
        },
        200,
        { leading: true }
      )
    );
    const element = ref.current;

    calculateSize.observe(element);

    return () => calculateSize.unobserve(element);
  }, [ref]);

  return contentRect;
};
