import { isInvalid } from 'redux-form';
import { useSelector } from 'react-redux';
import { useEffect, useState } from 'react';

export const invalidSelector = (viewList: string[]) => (state: RootState) => {
  const invalidList = viewList.map(formId => {
    return isInvalid(formId)(state);
  });
  return invalidList.some(item => item === true);
};

export const useInvalidForm = (viewList: string[]) => {
  const [, setViewListLen] = useState(viewList.length);

  useEffect(() => {
    setViewListLen(viewList.length);
  }, [viewList.length]);

  const isFormInValid = useSelector<RootState, boolean>(
    invalidSelector(viewList)
  );

  return isFormInValid;
};
