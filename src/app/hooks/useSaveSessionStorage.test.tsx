import { render, screen } from '@testing-library/react';
import store from 'app/_libraries/_dof/core/redux/createAppStore';
import React from 'react';
import { useSaveSessionStorage } from './useSaveSessionStorage';

jest.mock('app/_libraries/_dof/core/redux/createAppStore', () =>
  jest.requireActual('app/test-utils/createAppStore')
);

const Component: React.FC<any> = () => {
  useSaveSessionStorage();
  return <div data-testid="useSaveSessionStorage_render" />;
};

const tabMock = {};
const useRoleMock = {};
const timersWorkingMock = {};
const accountDetailMock = {};

window.appConfig = {
  ...window.appConfig,
  commonConfig: {
    ...window.appConfig?.commonConfig,
    callingApplication: '',
    org: 'org',
    app: 'app'
  }
};

describe('Test useSaveSessionStorage hook', () => {
  let spyStore: jest.SpyInstance;
  beforeEach(() => {
    jest.resetModules();
    spyStore = jest.spyOn(store, 'getState').mockReturnValue({
      tab: tabMock,
      userRole: useRoleMock,
      timerWorking: timersWorkingMock,
      accountDetail: accountDetailMock
    });
  });
  afterEach(() => {
    jest.clearAllMocks();

    spyStore?.mockReset();
    spyStore?.mockRestore();
  });

  it('should pass store.subscribe and previousData.current is default', () => {
    render(<Component />);
    expect(
      screen.getByTestId('useSaveSessionStorage_render')
    ).toBeInTheDocument();
  });

  it('should pass store.subscribe and previousData.current is assigned', () => {
    jest.spyOn(React, 'useRef').mockReturnValue({
      current: {
        prevTab: tabMock,
        prevTimeWorking: timersWorkingMock,
        prevUserRole: useRoleMock,
        prevAccDetail: accountDetailMock
      }
    });
    render(<Component />);
    expect(
      screen.getByTestId('useSaveSessionStorage_render')
    ).toBeInTheDocument();
  });
});
