import React from 'react';
import { render, screen } from '@testing-library/react';
import * as generateEntries from 'app/test-utils/mocks/MockResizeObserver/generateEntries';
import { useDesktopScreenSize } from './useDesktopScreenSize';

jest.mock('resize-observer-polyfill', () =>
  jest.requireActual('app/test-utils/mocks/MockResizeObserver')
);

const Component = () => {
  useDesktopScreenSize();

  return <div data-testid="useDesktopScreenSize_render" />;
};

describe('Test useDesktopScreenSize hook', () => {
  it('entity is null', () => {
    const spy = jest
      .spyOn(generateEntries, 'generateEntries')
      .mockImplementation(() => null);
    render(<Component />);
    expect(
      screen.getByTestId('useDesktopScreenSize_render')
    ).toBeInTheDocument();

    spy.mockRestore();
    spy.mockReset();
  });

  it('entity is not null', () => {
    render(<Component />);
    expect(
      screen.getByTestId('useDesktopScreenSize_render')
    ).toBeInTheDocument();
  });
});
