import { useSelector } from 'react-redux';
import { useAccountDetail, useCustomStoreId } from './';

export const useStoreIdSelector = <T extends unknown>(
  selector: Function,
  storeId?: string
): T => {
  const { storeId: accountDetailStoreId } = useAccountDetail();

  const dataSelector = useSelector<RootState, T>(states =>
    selector(states, storeId || accountDetailStoreId)
  );
  return dataSelector;
};

export const useCustomStoreIdSelector = <T extends unknown>(
  selector: Function
): T => {
  const { customStoreId } = useCustomStoreId();
  const dataSelector = useSelector<RootState, T>(states =>
    selector(states, customStoreId)
  );
  return dataSelector;
};
