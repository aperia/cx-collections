import { useRef } from 'react';
import { Store } from 'redux';
import appStore from 'app/_libraries/_dof/core/redux/createAppStore';

// Helpers
import {
  setSessionStorage,
  setSessionStorageWithUF8
} from 'app/helpers/commons';
import { pickNestObjectKey } from 'app/helpers';

interface PreviousData {
  prevTab: MagicKeyValue;
  prevUserRole: MagicKeyValue;
  prevTimeWorking: MagicKeyValue;
  prevAccDetail: MagicKeyValue;
}

export const useSaveSessionStorage = () => {
  const store = appStore as Store<RootState>;

  const previousData = useRef<PreviousData>({
    prevTab: {},
    prevUserRole: {},
    prevTimeWorking: {},
    prevAccDetail: {}
  });

  store.subscribe(() => {
    const { tab, timerWorking, accountDetail } = store.getState();
    const { prevTab, prevTimeWorking, prevAccDetail } = previousData.current;

    if (prevTab !== tab) {
      setSessionStorage('tab', tab);
      previousData.current.prevTab = tab;
    }

    if (prevTimeWorking !== timerWorking) {
      setSessionStorage('timerWorking', timerWorking);
      previousData.current.prevTimeWorking = timerWorking;
    }

    if (prevAccDetail !== accountDetail) {
      const authenticationData = pickNestObjectKey(accountDetail, [
        'numberPhoneAuthenticated',
        'selectedAuthenticationData',
        'selectedCaller'
      ]);

      setSessionStorageWithUF8('accountDetail', authenticationData);

      previousData.current.prevAccDetail = accountDetail;
    }
  });
};
