import React from 'react';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import isNumber from 'lodash.isnumber';
import { usePagination } from './usePagination';

let result: any;

const fakeData = [
  { id: '001', cliendId: 'xxx001' },
  { id: '002', cliendId: 'xxx002' },
  { id: '003', cliendId: 'xxx003' }
];

describe('Test usePagination hook', () => {
  it('should pass currentPage as an argument', () => {
    const Component = () => {
      result = usePagination(fakeData, [2], 10);
      return null;
    };
    render(<Component />);
    expect(isNumber(result.total)).toEqual(true);
  });

  it('should not pass currentPage as an argument', () => {
    const Component = () => {
      result = usePagination(fakeData);
      return null;
    };
    render(<Component />);
    expect(isNumber(result.total)).toEqual(true);
  });

  it('should trigger action onPageChange, onPageSizeChange', () => {
    const Component = () => {
      const { onPageChange, onPageSizeChange } = usePagination(fakeData);
      return (
        <div>
          <div
            data-testid="Pagination_onPageChange"
            onClick={() => onPageChange!(2)}
          />
          <div
            data-testid="Pagination_onPageSizeChange"
            onClick={() => onPageSizeChange!({ size: 20, page: 2 })}
          />
        </div>
      );
    };
    render(<Component />);
    userEvent.click(screen.getByTestId('Pagination_onPageChange'));
    userEvent.click(screen.getByTestId('Pagination_onPageSizeChange'));

    expect(screen.getByTestId('Pagination_onPageChange')).toBeInTheDocument();
    expect(
      screen.getByTestId('Pagination_onPageSizeChange')
    ).toBeInTheDocument();
  });
});
