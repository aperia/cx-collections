import React, { useContext } from 'react';

interface CustomStoreIdContextProps {
  customStoreId: string;
}

const CustomStoreIdContext = React.createContext<CustomStoreIdContextProps>({
  customStoreId: ''
});

export const useCustomStoreId = () => {
  const context = useContext<CustomStoreIdContextProps>(CustomStoreIdContext);
  return context;
};

export const CustomStoreIdProvider: React.FC<{
  value: CustomStoreIdContextProps;
}> = ({ children, value }) => {
  return (
    <CustomStoreIdContext.Provider value={value}>
      {children}
    </CustomStoreIdContext.Provider>
  );
};
