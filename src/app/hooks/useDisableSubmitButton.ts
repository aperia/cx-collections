import { useCallback, useMemo, useState } from 'react';
import isEqual from 'lodash.isequal';
export const useDisableSubmitButton = <T extends unknown>(nextValues: T) => {
  const [primitive, setPrimitive] = useState<T>();
  const setPrimitiveValues = useCallback((values: T) => {
    setPrimitive(values);
  }, []);
  const replacer = (key: string, value: any) => {
    return value || '';
  };
  const disableSubmit = useMemo(() => {
    const _primitive = primitive
      ? JSON.parse(JSON.stringify(primitive, replacer))
      : '';
    const _values = nextValues
      ? JSON.parse(JSON.stringify(nextValues, replacer))
      : '';
    return isEqual(_primitive, _values);
  }, [primitive, nextValues]);
  return {
    disableSubmit,
    setPrimitiveValues
  };
};
