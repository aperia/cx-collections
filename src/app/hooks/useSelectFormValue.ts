import { createSelector } from '@reduxjs/toolkit';
import { useSelector } from 'react-redux';

const getFormValue = (formKy: string) =>
  createSelector(
    [
      (state: RootState) => {
        return state.form[formKy]?.values;
      }
    ],
    val => val
  );

export const useSelectFormValue = (formKy: string) => {
  const data = useSelector(getFormValue(formKy));
  return data;
};
