import React, { useContext } from 'react';

export interface MemoContextProps {
  isHiddenAddSection?: boolean;
}

export const MemoContext = React.createContext<MemoContextProps>({});

export const useMemoProvider = () => {
  return useContext(MemoContext);
};
