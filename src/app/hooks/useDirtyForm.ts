import { isDirty } from 'redux-form';
import { useSelector } from 'react-redux';
import { useEffect, useState } from 'react';

export const dirtySelector = (viewList: string[]) => (state: RootState) => {
  const dirtyList = viewList.map(formId => {
    return isDirty(formId)(state);
  });
  return dirtyList.some(item => item === true);
};

export const useIsDirtyForm = (viewList: string[]) => {
  const [, setViewListLen] = useState(viewList.length);

  useEffect(() => {
    setViewListLen(viewList.length);
  }, [viewList.length]);

  const isFormDirty = useSelector<RootState, boolean>(dirtySelector(viewList));

  return isFormDirty;
};
