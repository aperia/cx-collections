import React from 'react';
import { screen } from '@testing-library/dom';
import { renderMockStoreId } from 'app/test-utils';
import { useRef } from 'react';
import { useElementSize } from './useElementResize';

jest.mock('resize-observer-polyfill', () =>
  jest.requireActual('app/test-utils/mocks/MockResizeObserver')
);

const initialState: Partial<RootState> = {};

describe('Test useElementSize hook', () => {
  it('should render when ref is null', () => {
    const Component = () => {
      const containerRef = useRef<HTMLDivElement>(null);
      useElementSize(containerRef);
      return <div data-testid="useElementSize_render01" />;
    };
    renderMockStoreId(<Component />, { initialState });
    expect(screen.getByTestId('useElementSize_render01')).toBeInTheDocument();
  });

  it('should render when ref is not null', () => {
    const Component = () => {
      const containerRef = useRef<HTMLDivElement>(null);
      useElementSize(containerRef);
      return <div data-testid="useElementSize_render02" ref={containerRef} />;
    };
    renderMockStoreId(<Component />, { initialState });
    expect(screen.getByTestId('useElementSize_render02')).toBeInTheDocument();
  });
});
