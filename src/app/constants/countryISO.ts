export const countryISO = [
  {
    fieldValue: 'United States of America',
    fieldID: 'USA'
  },
  {
    fieldValue: 'Afghanistan',
    fieldID: 'AFG'
  },
  {
    fieldValue: 'Albania',
    fieldID: 'ALB'
  },
  {
    fieldValue: 'Algeria',
    fieldID: 'DZA'
  },
  {
    fieldValue: 'American Samoa',
    fieldID: 'ASM'
  },
  {
    fieldValue: 'Andorra',
    fieldID: 'AND'
  },
  {
    fieldValue: 'Angola',
    fieldID: 'AGO'
  },
  {
    fieldValue: 'Anguilla',
    fieldID: 'AIA'
  },
  {
    fieldValue: 'Antarctica',
    fieldID: 'ATA'
  },
  {
    fieldValue: 'Antigua-Barbuda',
    fieldID: 'ATG'
  },
  {
    fieldValue: 'Argentina',
    fieldID: 'ARG'
  },
  {
    fieldValue: 'Armenia',
    fieldID: 'ARM'
  },
  {
    fieldValue: 'Aruba',
    fieldID: 'ABW'
  },
  {
    fieldValue: 'Australia',
    fieldID: 'AUS'
  },
  {
    fieldValue: 'Austria',
    fieldID: 'AUT'
  },
  {
    fieldValue: 'Azerbaijan',
    fieldID: 'AZE'
  },
  {
    fieldValue: 'Bahamas',
    fieldID: 'BHS'
  },
  {
    fieldValue: 'Bahrain',
    fieldID: 'BHR'
  },
  {
    fieldValue: 'Bangladesh',
    fieldID: 'BGD'
  },
  {
    fieldValue: 'Barbados',
    fieldID: 'BRB'
  },
  {
    fieldValue: 'Belarus',
    fieldID: 'BLR'
  },
  {
    fieldValue: 'Belgium',
    fieldID: 'BEL'
  },
  {
    fieldValue: 'Belize',
    fieldID: 'BLZ'
  },
  {
    fieldValue: 'Benin',
    fieldID: 'BEN'
  },
  {
    fieldValue: 'Bermuda',
    fieldID: 'BMU'
  },
  {
    fieldValue: 'Bhutan',
    fieldID: 'BTN'
  },
  {
    fieldValue: 'Bolivia',
    fieldID: 'BOL'
  },
  {
    fieldValue: 'Bonaire, St Eustatius, Saba',
    fieldID: 'BES'
  },
  {
    fieldValue: 'Bosnia and Herzegovina',
    fieldID: 'BIH'
  },
  {
    fieldValue: 'Botswana',
    fieldID: 'BWA'
  },
  {
    fieldValue: 'Bouvet Island',
    fieldID: 'BVT'
  },
  {
    fieldValue: 'Brazil',
    fieldID: 'BRA'
  },
  {
    fieldValue: 'British Indian Ocean Territories',
    fieldID: 'IOT'
  },
  {
    fieldValue: 'British Virgin Islands',
    fieldID: 'VGB'
  },
  {
    fieldValue: 'Brunei Darussalam',
    fieldID: 'BRN'
  },
  {
    fieldValue: 'Bulgaria',
    fieldID: 'BGR'
  },
  {
    fieldValue: 'Burkina Faso',
    fieldID: 'BFA'
  },
  {
    fieldValue: 'Burundi',
    fieldID: 'BDI'
  },
  {
    fieldValue: 'Cambodia',
    fieldID: 'KHM'
  },
  {
    fieldValue: 'Cameroon, United Republic of',
    fieldID: 'CMR'
  },
  {
    fieldValue: 'Canada',
    fieldID: 'CAN'
  },
  {
    fieldValue: 'Cape Verde Island',
    fieldID: 'CPV'
  },
  {
    fieldValue: 'Cayman Islands',
    fieldID: 'CYM'
  },
  {
    fieldValue: 'Central African Republic',
    fieldID: 'CAF'
  },
  {
    fieldValue: 'Chad',
    fieldID: 'TCD'
  },
  {
    fieldValue: 'Chile',
    fieldID: 'CHL'
  },
  {
    fieldValue: 'China',
    fieldID: 'CHN'
  },
  {
    fieldValue: 'Christmas Island',
    fieldID: 'CXR'
  },
  {
    fieldValue: 'Cocos (Keeling) Island',
    fieldID: 'CCK'
  },
  {
    fieldValue: 'Colombia',
    fieldID: 'COL'
  },
  {
    fieldValue: 'Comoro Islands',
    fieldID: 'COM'
  },
  {
    fieldValue: 'Congo',
    fieldID: 'COG'
  },
  {
    fieldValue: 'Congo, Democratic Republic of the (was Zaire)',
    fieldID: 'COD'
  },
  {
    fieldValue: 'Cook Islands',
    fieldID: 'COK'
  },
  {
    fieldValue: 'Costa Rica',
    fieldID: 'CRI'
  },
  {
    fieldValue: 'Côte d’Ivoire (Ivory Coast)',
    fieldID: 'CIV'
  },
  {
    fieldValue: 'Croatia',
    fieldID: 'HRV'
  },
  {
    fieldValue: 'Cuba',
    fieldID: 'CUB'
  },
  {
    fieldValue: 'Curacao',
    fieldID: 'CUW'
  },
  {
    fieldValue: 'Cyprus',
    fieldID: 'CYP'
  },
  {
    fieldValue: 'Czech Republic',
    fieldID: 'CZE'
  },
  {
    fieldValue: 'Denmark',
    fieldID: 'DNK'
  },
  {
    fieldValue: 'Djibouti',
    fieldID: 'DJI'
  },
  {
    fieldValue: 'Dominica',
    fieldID: 'DMA'
  },
  {
    fieldValue: 'Dominican Republic',
    fieldID: 'DOM'
  },
  {
    fieldValue: 'East Timor (Timor-Leste)',
    fieldID: 'TLS'
  },
  {
    fieldValue: 'Ecuador',
    fieldID: 'ECU'
  },
  {
    fieldValue: 'Egypt',
    fieldID: 'EGY'
  },
  {
    fieldValue: 'El Salvador',
    fieldID: 'SLV'
  },
  {
    fieldValue: 'Equatorial Guinea',
    fieldID: 'GNQ'
  },
  {
    fieldValue: 'Estonia',
    fieldID: 'EST'
  },
  {
    fieldValue: 'Ethiopia',
    fieldID: 'ETH'
  },
  {
    fieldValue: 'Faeroe Islands',
    fieldID: 'FRO'
  },
  {
    fieldValue: 'Falkland Islands',
    fieldID: 'FLK'
  },
  {
    fieldValue: 'Fiji',
    fieldID: 'FJI'
  },
  {
    fieldValue: 'Finland',
    fieldID: 'FIN'
  },
  {
    fieldValue: 'France',
    fieldID: 'FRA'
  },
  {
    fieldValue: 'France, Metropolitan',
    fieldID: 'FXX'
  },
  {
    fieldValue: 'French Guiana',
    fieldID: 'GUF'
  },
  {
    fieldValue: 'French Polynesia',
    fieldID: 'PYF'
  },
  {
    fieldValue: 'French Southern Territories',
    fieldID: 'ATF'
  },
  {
    fieldValue: 'Gabon',
    fieldID: 'GAB'
  },
  {
    fieldValue: 'Gambia',
    fieldID: 'GMB'
  },
  {
    fieldValue: 'Georgia',
    fieldID: 'GEO'
  },
  {
    fieldValue: 'Germany (Mastercard)',
    fieldID: 'DEU'
  },
  {
    fieldValue: 'Germany (Visa)',
    fieldID: 'DEU'
  },
  {
    fieldValue: 'Ghana',
    fieldID: 'GHA'
  },
  {
    fieldValue: 'Gibraltar',
    fieldID: 'GIB'
  },
  {
    fieldValue: 'Greece',
    fieldID: 'GRC'
  },
  {
    fieldValue: 'Greenland',
    fieldID: 'GRL'
  },
  {
    fieldValue: 'Grenada',
    fieldID: 'GRD'
  },
  {
    fieldValue: 'Guadaloupe',
    fieldID: 'GLP'
  },
  {
    fieldValue: 'Guam',
    fieldID: 'GUM'
  },
  {
    fieldValue: 'Guatemala',
    fieldID: 'GTM'
  },
  {
    fieldValue: 'Guernsey',
    fieldID: 'GGY'
  },
  {
    fieldValue: 'Guinea',
    fieldID: 'GIN'
  },
  {
    fieldValue: 'Guinea – Bissau',
    fieldID: 'GNB'
  },
  {
    fieldValue: 'Guyana',
    fieldID: 'GUY'
  },
  {
    fieldValue: 'Haiti',
    fieldID: 'HTI'
  },
  {
    fieldValue: 'Heard and McDonald Islands',
    fieldID: 'HMD'
  },
  {
    fieldValue: 'Honduras',
    fieldID: 'HND'
  },
  {
    fieldValue: 'Hong Kong',
    fieldID: 'HKG'
  },
  {
    fieldValue: 'Hungary',
    fieldID: 'HUN'
  },
  {
    fieldValue: 'Iceland',
    fieldID: 'ISL'
  },
  {
    fieldValue: 'India',
    fieldID: 'IND'
  },
  {
    fieldValue: 'Indonesia',
    fieldID: 'IDN'
  },
  {
    fieldValue: 'Iran',
    fieldID: 'IRN'
  },
  {
    fieldValue: 'Iraq',
    fieldID: 'IRQ'
  },
  {
    fieldValue: 'Ireland',
    fieldID: 'IRL'
  },
  {
    fieldValue: 'Isle of Man',
    fieldID: 'IMN'
  },
  {
    fieldValue: 'Israel',
    fieldID: 'ISR'
  },
  {
    fieldValue: 'Italy',
    fieldID: 'ITA'
  },
  {
    fieldValue: 'Ivory Coast (Cote d’Ivoire)',
    fieldID: 'CIV'
  },
  {
    fieldValue: 'Jamaica',
    fieldID: 'JAM'
  },
  {
    fieldValue: 'Japan',
    fieldID: 'JPN'
  },
  {
    fieldValue: 'Jersey',
    fieldID: 'JEY'
  },
  {
    fieldValue: 'Jordan',
    fieldID: 'JOR'
  },
  {
    fieldValue: 'Kazakhstan',
    fieldID: 'KAZ'
  },
  {
    fieldValue: 'Kenya',
    fieldID: 'KEN'
  },
  {
    fieldValue: 'Kiribati',
    fieldID: 'KIR'
  },
  {
    fieldValue: 'Korea, Democratic People’s Republic of',
    fieldID: 'PRK'
  },
  {
    fieldValue: 'Korea, Republic of',
    fieldID: 'KOR'
  },
  {
    fieldValue: 'Kosovo',
    fieldID: 'QZZ'
  },
  {
    fieldValue: 'Kuwait',
    fieldID: 'KWT'
  },
  {
    fieldValue: 'Kyrgyzstan',
    fieldID: 'KGZ'
  },
  {
    fieldValue: 'Lao People’s Democratic Republic',
    fieldID: 'LAO'
  },
  {
    fieldValue: 'Latvia',
    fieldID: 'LVA'
  },
  {
    fieldValue: 'Lebanon',
    fieldID: 'LBN'
  },
  {
    fieldValue: 'Lesotho',
    fieldID: 'LSO'
  },
  {
    fieldValue: 'Liberia',
    fieldID: 'LBR'
  },
  {
    fieldValue: 'Libyan Arab Jamahiriya',
    fieldID: 'LBY'
  },
  {
    fieldValue: 'Liechtenstein',
    fieldID: 'LIE'
  },
  {
    fieldValue: 'Lithuania',
    fieldID: 'LTU'
  },
  {
    fieldValue: 'Luxembourg',
    fieldID: 'LUX'
  },
  {
    fieldValue: 'Macao',
    fieldID: 'MAC'
  },
  {
    fieldValue: 'Macau',
    fieldID: 'MAC'
  },
  {
    fieldValue: 'Macedonia, Former Yugoslav Republic of',
    fieldID: 'MKD'
  },
  {
    fieldValue: 'Madagascar',
    fieldID: 'MGA'
  },
  {
    fieldValue: 'Malawi',
    fieldID: 'MWI'
  },
  {
    fieldValue: 'Malaysia',
    fieldID: 'MYS'
  },
  {
    fieldValue: 'Maldives',
    fieldID: 'MDV'
  },
  {
    fieldValue: 'Mali',
    fieldID: 'MLI'
  },
  {
    fieldValue: 'Malta',
    fieldID: 'MLT'
  },
  {
    fieldValue: 'Marshall Islands',
    fieldID: 'MHL'
  },
  {
    fieldValue: 'Martinique',
    fieldID: 'MTQ'
  },
  {
    fieldValue: 'Mauritania',
    fieldID: 'MRT'
  },
  {
    fieldValue: 'Mauritius',
    fieldID: 'MUS'
  },
  {
    fieldValue: 'Mayotte',
    fieldID: 'MYT'
  },
  {
    fieldValue: 'Mexico',
    fieldID: 'MEX'
  },
  {
    fieldValue: 'Micronesia',
    fieldID: 'FSM'
  },
  {
    fieldValue: 'Micronesia, Federated States of',
    fieldID: 'FSM'
  },
  {
    fieldValue: 'Moldova',
    fieldID: 'MDA'
  },
  {
    fieldValue: 'Monaco',
    fieldID: 'MCO'
  },
  {
    fieldValue: 'Mongolia',
    fieldID: 'MNG'
  },
  {
    fieldValue: 'Montenegro',
    fieldID: 'MNE'
  },
  {
    fieldValue: 'Montserrat',
    fieldID: 'MSR'
  },
  {
    fieldValue: 'Morocco',
    fieldID: 'MAR'
  },
  {
    fieldValue: 'Mozambique',
    fieldID: 'MOZ'
  },
  {
    fieldValue: 'Myanmar',
    fieldID: 'MMR'
  },
  {
    fieldValue: 'Namibia',
    fieldID: 'NAM'
  },
  {
    fieldValue: 'Nauru',
    fieldID: 'NRU'
  },
  {
    fieldValue: 'Nepal',
    fieldID: 'NPL'
  },
  {
    fieldValue: 'Netherlands',
    fieldID: 'NLD'
  },
  {
    fieldValue: 'Netherlands, Antilles (not valid for Visa)',
    fieldID: 'ANT'
  },
  {
    fieldValue: 'New Caledonia',
    fieldID: 'NCL'
  },
  {
    fieldValue: 'New Zealand',
    fieldID: 'NZL'
  },
  {
    fieldValue: 'Nicaragua',
    fieldID: 'NIC'
  },
  {
    fieldValue: 'Niger',
    fieldID: 'NER'
  },
  {
    fieldValue: 'Nigeria',
    fieldID: 'NGA'
  },
  {
    fieldValue: 'Niue Island',
    fieldID: 'NIU'
  },
  {
    fieldValue: 'Norfolk Island',
    fieldID: 'NFK'
  },
  {
    fieldValue: 'Northern Marianas Islands',
    fieldID: 'MNP'
  },
  {
    fieldValue: 'Norway',
    fieldID: 'NOR'
  },
  {
    fieldValue: 'Occupied Palestinian Territory',
    fieldID: 'PSE'
  },
  {
    fieldValue: 'Oman',
    fieldID: 'OMN'
  },
  {
    fieldValue: 'Pakistan',
    fieldID: 'PAK'
  },
  {
    fieldValue: 'Palau',
    fieldID: 'PLW'
  },
  {
    fieldValue: 'Palestinian Territory Occupied',
    fieldID: 'PSE'
  },
  {
    fieldValue: 'Panama',
    fieldID: 'PAN'
  },
  {
    fieldValue: 'Papua New Guinea',
    fieldID: 'PNG'
  },
  {
    fieldValue: 'Paraguay',
    fieldID: 'PRY'
  },
  {
    fieldValue: 'Peru',
    fieldID: 'PER'
  },
  {
    fieldValue: 'Philippines',
    fieldID: 'PHL'
  },
  {
    fieldValue: 'Pitcairn Islands',
    fieldID: 'PCN'
  },
  {
    fieldValue: 'Poland',
    fieldID: 'POL'
  },
  {
    fieldValue: 'Portugal',
    fieldID: 'PRT'
  },
  {
    fieldValue: 'Puerto Rico',
    fieldID: 'PRI'
  },
  {
    fieldValue: 'Qatar',
    fieldID: 'QAT'
  },
  {
    fieldValue: 'Reunion',
    fieldID: 'REU'
  },
  {
    fieldValue: 'Romania',
    fieldID: 'ROU'
  },
  {
    fieldValue: 'Russian Federation',
    fieldID: 'RUS'
  },
  {
    fieldValue: 'Rwanda',
    fieldID: 'RWA'
  },
  {
    fieldValue: 'Saint Barthelemy (American Express)',
    fieldID: 'BLM'
  },
  {
    fieldValue: 'Saint Helena',
    fieldID: 'SHN'
  },
  {
    fieldValue: 'Saint Kitts-Nevis',
    fieldID: 'KNA'
  },
  {
    fieldValue: 'Saint Lucia',
    fieldID: 'LCA'
  },
  {
    fieldValue: 'Saint Martin (American Express)',
    fieldID: 'MAF'
  },
  {
    fieldValue: 'Saint Pierre and Miquelon',
    fieldID: 'SPM'
  },
  {
    fieldValue: 'Saint Vincent and the Grenadines',
    fieldID: 'VCT'
  },
  {
    fieldValue: 'Samoa',
    fieldID: 'WSM'
  },
  {
    fieldValue: 'San Marino',
    fieldID: 'SMR'
  },
  {
    fieldValue: 'Sao Tome and Principe',
    fieldID: 'STP'
  },
  {
    fieldValue: 'Saudi Arabia',
    fieldID: 'SAU'
  },
  {
    fieldValue: 'Senegal',
    fieldID: 'SEN'
  },
  {
    fieldValue: 'Serbia',
    fieldID: 'SRB'
  },
  {
    fieldValue: 'Seychelles',
    fieldID: 'SYC'
  },
  {
    fieldValue: 'Sierra Leone',
    fieldID: 'SLE'
  },
  {
    fieldValue: 'Singapore',
    fieldID: 'SGP'
  },
  {
    fieldValue: 'Saint Maarten (Dutch Part)',
    fieldID: 'SXM'
  },
  {
    fieldValue: 'Slovakia',
    fieldID: 'SVK'
  },
  {
    fieldValue: 'Slovenia',
    fieldID: 'SVN'
  },
  {
    fieldValue: 'Solomon Islands',
    fieldID: 'SLB'
  },
  {
    fieldValue: 'Somalia',
    fieldID: 'SOM'
  },
  {
    fieldValue: 'South Africa',
    fieldID: 'ZAF'
  },
  {
    fieldValue: 'South Sudan',
    fieldID: 'SSP'
  },
  {
    fieldValue: 'South Georgia and South Sandwich Islands',
    fieldID: 'SGS'
  },
  {
    fieldValue: 'Spain',
    fieldID: 'ESP'
  },
  {
    fieldValue: 'Sri Lanka',
    fieldID: 'LKA'
  },
  {
    fieldValue: 'St Helena, Ascension, Tristan Da Cunha',
    fieldID: 'SHN'
  },
  {
    fieldValue: 'The State of Eritrea',
    fieldID: 'ERI'
  },
  {
    fieldValue: 'Sudan',
    fieldID: 'SDN'
  },
  {
    fieldValue: 'Suriname',
    fieldID: 'SUR'
  },
  {
    fieldValue: 'Svalbard and Jan Mayen Islands',
    fieldID: 'SJM'
  },
  {
    fieldValue: 'Swaziland',
    fieldID: 'SWZ'
  },
  {
    fieldValue: 'Sweden',
    fieldID: 'SWE'
  },
  {
    fieldValue: 'Switzerland',
    fieldID: 'CHE'
  },
  {
    fieldValue: 'Syrian Arab Republic',
    fieldID: 'SYR'
  },
  {
    fieldValue: 'Taiwan (Province of China)',
    fieldID: 'TWN'
  },
  {
    fieldValue: 'Tajikistan',
    fieldID: 'TJK'
  },
  {
    fieldValue: 'Tanzania, United Republic of',
    fieldID: 'TZA'
  },
  {
    fieldValue: 'Thailand',
    fieldID: 'THA'
  },
  {
    fieldValue: 'Togo',
    fieldID: 'TGO'
  },
  {
    fieldValue: 'Tokelau Island',
    fieldID: 'TKL'
  },
  {
    fieldValue: 'Tonga',
    fieldID: 'TON'
  },
  {
    fieldValue: 'Trinidad and Tobago',
    fieldID: 'TTO'
  },
  {
    fieldValue: 'Tunisia',
    fieldID: 'TUN'
  },
  {
    fieldValue: 'Turkey',
    fieldID: 'TUR'
  },
  {
    fieldValue: 'Turkmenistan',
    fieldID: 'TKM'
  },
  {
    fieldValue: 'Turks and Caicos Islands',
    fieldID: 'TCA'
  },
  {
    fieldValue: 'Tuvalu',
    fieldID: 'TUV'
  },
  {
    fieldValue: 'Uganda',
    fieldID: 'UGA'
  },
  {
    fieldValue: 'Ukraine',
    fieldID: 'UKR'
  },
  {
    fieldValue: 'United Arab Emirates',
    fieldID: 'ARE'
  },
  {
    fieldValue: 'United Kingdom',
    fieldID: 'GBR'
  },
  {
    fieldValue: 'United Nations Mission in Kosovo (UNMIK)',
    fieldID: 'QZZ'
  },
  {
    fieldValue: 'United States Minor Outlying Islands',
    fieldID: 'UMI'
  },
  {
    fieldValue: 'United States Virgin Islands',
    fieldID: 'VIR'
  },
  {
    fieldValue: 'Uruguay',
    fieldID: 'URY'
  },
  {
    fieldValue: 'Uzbekistan',
    fieldID: 'UZB'
  },
  {
    fieldValue: 'Vanuatu',
    fieldID: 'VUT'
  },
  {
    fieldValue: 'Vatican City State',
    fieldID: 'VAT'
  },
  {
    fieldValue: 'Venezuela',
    fieldID: 'VEN'
  },
  {
    fieldValue: 'Vietnam',
    fieldID: 'VNM'
  },
  {
    fieldValue: 'Wallis and Futuna Islands',
    fieldID: 'WLF'
  },
  {
    fieldValue: 'Western Sahara',
    fieldID: 'ESH'
  },
  {
    fieldValue: 'Yemen',
    fieldID: 'YEM'
  },
  {
    fieldValue: 'Zambia',
    fieldID: 'ZMW'
  },
  {
    fieldValue: 'Zimbabwe',
    fieldID: 'ZWE'
  }
];
