export const I18N_COMMON_TEXT = {
  ADD: 'txt_add',
  ACCOUNT_NUMBER: 'txt_account_number',
  ACCOUNT_LIST: 'txt_account_list',
  ACCOUNT_NUMBER_DESCRIPTION: 'txt_account_number_description',
  AFFINITY_NUMBER: 'txt_affinity_number',
  LAST_NAME: 'txt_last_name',
  FIRST_NAME: 'txt_first_name',
  FIRST_NAME_DESCRIPTION: 'txt_first_name_description',
  FIRST_NAME_ONLY_DESCRIPTION: 'txt_first_name_only_description',
  LAST_NAME_DESCRIPTION: 'txt_last_name_description',
  MIDDLE_INITIAL: 'txt_middle_initial',
  SSN_NAME: 'txt_ssn',
  PHONE_NUMBER: 'txt_phone_number',
  STATE: 'txt_state',
  STATE_DESCRIPTION: 'txt_state_description',
  ZIP_CODE: 'txt_zip_code',
  ZIP: 'txt_zip_uppercase',
  SEARCH_PARAMETERS: 'txt_search_parameters',
  CLEAR_AND_RESET: 'txt_clear_and_reset',
  FIVE_DIGITS: 'txt_five_digits',
  TEN_DIGITS: 'txt_ten_digits',
  NINE_DIGITS: 'txt_nine_digits',
  ENTER_KEYWORD: 'txt_enter_keyword',
  SELECT_AN_ITEM: 'txt_select_an_item',
  SHOW_MORE: 'txt_show_more',
  SHOW_LESS: 'txt_show_less',
  SUBMIT: 'txt_submit',
  CANCEL: 'txt_cancel',
  CLOSE: 'txt_close',
  SAVE: 'txt_save',
  SEARCH: 'txt_search',
  SEARCH_BY: 'txt_search_by',
  APPLY: 'txt_apply',
  RESET_TO_DEFAULT: 'txt_reset_to_default',
  YESTERDAY: 'txt_yesterday',
  TODAY: 'txt_today',
  SEND: 'txt_send',
  DELETE: 'txt_delete',
  EDIT: 'txt_edit',
  MORE: 'txt_more',
  LESS: 'txt_less',
  SORT_BY: 'txt_sort_by',
  ORDER_BY: 'txt_order_by',
  ASCENDING: 'txt_ascending',
  DESCENDING: 'txt_descending',
  TYPE: 'txt_type',
  DATE: 'txt_date',
  PAYMENT_METHOD: 'txt_payment_method',
  ADD_PAYMENT_METHOD: 'txt_add_method_payment',
  SPECIFIC_DATE: 'txt_specific_date',
  DATE_RANGE: 'txt_date_range',
  COLLAPSE: 'txt_collapse',
  EXPAND: 'txt_expand',
  COLLAPSE_ALL: 'txt_collapse_all',
  EXPAND_ALL: 'txt_expand_all',
  REMOVE: 'txt_remove',
  MAXIMIZE: 'txt_maximize',
  MINIMIZE: 'txt_minimize',
  FOLLOW_UP: 'txt_follow_up',
  COLLECTION_FORM_HELP_TEXT: 'txt_collection_form_help_text',
  PROMISE_TO_PAY: 'txt_promise_to_pay',
  HARDSHIP: 'txt_hardship',
  RECURRING: 'txt_recurring',
  ADDRESS: 'txt_address',
  CONTINUE: 'txt_continue',
  TELEPHONE: 'txt_telephone',
  EMAIL: 'txt_email',
  FAX: 'txt_fax',
  MISCELLANEOUS: 'txt_miscellaneous',
  NO_DATA: 'txt_no_data',
  NO_COMPANIES_TO_DISPLAY: 'txt_no_companies_to_display',
  PHONE: 'txt_phone',
  CONTACT: 'txt_contact',
  ACTION: 'txt_action',
  TYPE_TO_SEARCH: 'txt_type_to_search',
  RESULT_FOUND: 'txt_result_found',
  RESULT_FOUND_PLURAL: 'txt_result_found_plural',
  ALERT: 'txt_alert',
  ALL_ALERTS: 'txt_all_alerts',
  VIEW: 'txt_view',
  UPLOAD: 'txt_upload',
  UPLOAD_FAILED_MESSAGE: 'txt_upload_failed',
  CLIENT_CONFIGURATION: 'txt_client_configuration',
  TYPE_TO_ADD_PARAMETER: 'txt_type_to_add_parameter',
  VIEW_SETTINGS: 'txt_view_settings',
  DESCRIPTION: 'txt_description',
  ACTIONS: 'txt_actions',
  CLIENT_ID: 'txt_client_id',
  UPDATE: 'txt_update',
  INVALID_FORMAT: 'txt_invalid_format',
  INVALID_AMOUNT_RANGE: 'txt_invalid_amount_range',
  OTHER: 'txt_other',
  RELOAD: 'txt_reload',
  ACTIVATE: 'txt_activate',
  DEACTIVATE: 'txt_deactivate',
  MEMO_SINGULAR: 'txt_memo_singular',
  EXTERNAL_STATUS: 'txt_external_status',
  EXTERNAL_STATUS_REASON: 'txt_external_status_reason',
  FILES_FAILED_TO_REMOVE: 'txt_files_failed_to_remove',
  FILE_FAILED_TO_REMOVE: 'txt_file_failed_to_remove',
  FILES: 'txt_files',
  ADDRESS_LINE_ONE: 'txt_address_line_one',
  NAME: 'txt_name',
  ADDRESS_LINE_TWO: 'txt_address_line_two',
  CITY: 'txt_city',
  COMPANY_LIST: 'txt_company_list',
  ADD_COMPANY: 'txt_add_company',
  TXT_ARE_YOU_WANT_DELETE_COMPANY: 'txt_are_you_want_delete_company',
  CONFIRM_DELETE_COMPANY: 'txt_confirm_delete_company',
  ENTER_PHONE_FAX_OR_BOTH: 'txt_enter_phone_fax_or_both',
  ENTER_AT_LEAST_ONE_FIELD: 'txt_enter_at_least_one_field',
  COMPANY_EXIST: 'txt_company_exists',
  EDIT_COMPANY: 'txt_edit_company',
  VIEW_DETAILS: 'txt_view_detail',
  COPY: 'txt_copy',
  COPIED: 'txt_copied',
  BANK_INFORMATION: 'txt_bank_information',
  BACK_TO_ENTRY_FORM: 'txt_back_to_entry_form',
  BANK_ACCOUNT_FAILED_TO_UPDATE: 'txt_bank_account_failed_to_update',
  BANK_ACCOUNT_UPDATED: 'txt_bank_account_updated',
  BANK_ACCOUNT_FAILED_TO_ADD: 'txt_bank_account_failed_to_add',
  DELETE_LETTER_SUCCESS: 'txt_delete_letter_success',
  DELETE_LETTER_FAIL: 'txt_delete_letter_fail',
  NO_LETTERS_TO_DISPLAY: 'txt_no_letters_to_display',
  API_ERROR_DETAIL: 'txt_api_error_detail',
  API_ERROR: 'txt_api_error',
  API_ERROR_DETAIL_RESPONSE_REQUEST_PAYLOAD:
    'txt_api_error_detail_response_request_payload',
  QUEUE_NAME: 'txt_queue_name',
  RANGE_DATE_MUST_HAVE_FROM_TO_DATE: 'txt_range_date_must_have_from_to_date',
  STATUS: 'txt_status',
  NO_ENTITLEMENT_ARE_SETUP: 'txt_no_entitlements_are_setup',
  CONTACT_ADMIN_FOR_MORE_DETAILS: 'txt_contact_admin_for_more_details',
  QUEUE_ID: 'txt_queue_id',
  AMOUNT_DELINQUENT: 'txt_amount_delinquent',
  AMOUNT_DESCRIPTION: 'txt_amount_description',
  MIN_AMOUNT: 'txt_min_amount',
  MAX_AMOUNT: 'txt_max_amount',
  CURRENT_BALANCE: 'txt_current_balance',
  DAYS_DELINQUENT: 'txt_days_delinquent',
  DAYS_DELINQUENT_DESCRIPTION: 'txt_days_delinquent_description',
  LAST_PAYMENT_DATE: 'txt_last_payment_date',
  LAST_WORK_DATE: 'txt_last_work_date',
  CLEAR_ALL_CRITERIA: 'txt_clear_all_criteria',
  NO_ACCOUNTS_TO_DISPLAY: 'txt_no_accounts_to_display',
  TIME_ZONE: 'txt_time_zone',
  ASSIGNED_TO: 'txt_assigned_to',
  CREATED_DATE: 'txt_created_date',
  WORKED_ACCOUNTS: 'txt_worked_accounts',
  REMAINING_ACCOUNTS: 'txt_remaining_accounts',
  TOTAL_ACCOUNTS: 'txt_total_accounts'
};

// ----------------- COLLECTION FORM -----------------------------

export const I18N_COLLECTION_FORM = {
  COLLECTION_FORM: 'txt_collection_form',
  CREATE_COLLECTION_FORM: 'txt_create_collection_form',
  EDIT_COLLECTION_FORM: 'txt_edit_collection_form',
  CALL_RESULT: 'txt_call_result',
  EDIT_GENERAL_INFORMATION: 'txt_edit_general_information',
  GENERAL_INFORMATION: 'txt_general_information',
  MAIN_INFORMATION: 'txt_main_information',
  LAST_25_ACTIONS: 'txt_last_25_actions',
  LAST_25_ACTIONS_NO_DATA: 'txt_no_actions_to_display',
  IN_PROGRESS_STATUS: 'txt_call_result_in_progress',

  EDIT_CALL_RESULT: 'txt_edit_call_result',
  TYPE: 'txt_type',
  SINGLE: 'txt_single',
  MULTIPLE: 'txt_multiple',
  RECURRING: 'txt_recurring',
  PROMISE: 'txt_promise',
  REASON: 'txt_reason',
  CUSTOM: 'txt_custom',
  ACCOUNT_STATUS_PROMISE_TO_PAY: 'txt_account_status_is_promise_to_pay',

  // TOAST
  TOAST_COLLECTION_FORM_SUBMITTED: 'txt_collection_form_submitted',
  TOAST_COLLECTION_FORM_UPDATED: 'txt_collection_form_updated',
  TOAST_COLLECTION_FORM_SUBMIT_FAILED: 'txt_collection_form_submit_failed',
  TOAST_COLLECTION_FORM_UPDATE_FAILED: 'txt_collection_form_update_failed',

  // DECEASED
  PRIMARY_DECEASED_PARTY_MAIN: 'txt_primary_deceased_party',
  SECOND_DECEASED_PARTY_MAIN: 'txt_secondary_deceased_party',
  PRIMARY_DECEASED_PARTY: 'txt_primary_deceased_party',
  SECOND_DECEASED_PARTY: 'txt_second_deceased_party',
  REMOVE_DECEASED_STATUS: 'txt_remove_deceased_status',
  CONFIRM_REMOVE_DECEASED_STATUS: 'txt_confirm_remove_deceased_status',
  ARE_YOU_SURE_REMOVE_DECEASED_STATUS: 'txt_are_you_sure_remove_deceased',
  SETTLEMENT_DECEASED_HEADER: 'txt_settlement_header',

  TOAST_DECEASED_UPDATE_FAILURE: 'txt_deceased_update_failure',
  TOAST_DECEASED_SUBMIT_FAILURE: 'txt_deceased_submit_failure',
  DECEASED_UPLOAD_CERTIFICATE_DESCRIPTION:
    'txt_deceased_upload_certificate_description',
  UPLOAD_DEATH_CERTIFICATE: 'txt_upload_death_certificate',
  DEATH_CERTIFICATE: 'txt_death_certificate',
  DEATH_CERTIFICATES_UPLOADED: 'txt_death_certificates_uploaded',

  // HARDSHIP
  CONFIRM_REMOVE_HARDSHIP: 'txt_confirm_remove_hardship_status',
  ARE_YOU_SURE_REMOVE_HARDSHIP: 'txt_are_you_sure_remove_hardship',
  REMOVE_HARDSHIP_STATUS: 'txt_remove_hardship_status',
  TITLE_REMOVE_HARDSHIP_STATUS: 'txt_title_remove_hardship_status',
  REASON1_REMOVE_HARDSHIP_STATUS: 'txt_reason1',
  REASON2_REMOVE_HARDSHIP_STATUS: 'txt_reason2',
  REMOVE_HARDSHIP_SUCCESS: 'txt_remove_hardship_success',
  REMOVE_HARDSHIP_FAILURE: 'txt_remove_hardship_failure',
  REMOVE_DECEASED_SUCCESS: 'txt_remove_deceased_success',
  REMOVE_DECEASED_FAILURE: 'txt_remove_deceased_failure',
  EDIT_INFORMATION: 'txt_edit_information',
  HARDSHIP: 'txt_hardship',
  ADD_PROMISE_TO_PAY: 'txt_add_promise_to_pay',
  DEBT_MANAGEMENT_COMPANY_INFORMATION:
    'txt_debt_management_company_information',
  ADD_COMPANY: 'txt_add_company',
  HARDSHIP_TITLE: 'txt_hardship_title',
  NATURAL_DISASTER_TITLE: 'txt_natural_disaster_title',
  CONFIRM_SUBMIT_HARDSHIP_BODY: 'txt_confirm_submit_hardship_body',
  UPLOAD_HARDSHIP_TITLE: 'txt_upload_order_of_deployment',
  UPLOAD_HARDSHIP_DESCRIPTION: 'txt_upload_order_of_deployment_description',
  UPLOAD_HARDSHIP_UPLOADED: 'txt_orders_of_deployment_uploaded',

  // CCCS
  COMPANY_NAME: 'txt_company_name',
  DEBT_MANAGEMENT_COMPANY_LIST: 'txt_debt_management_company_list',
  NO_COMPANIES_TO_DISPLAY: 'txt_no_companies_to_display',
  COMPANY_FOUND: 'txt_company_found',
  COMPANY_UPDATED_SUCCESS: 'txt_company_updated_success',
  COMPANY_UPDATED_FAILED: 'txt_company_updated_failed',
  COMPANY_ADDED_SUCCESS: 'txt_company_added_success',
  COMPANY_ADDED_FAILURE: 'txt_company_added_failure',
  CCCS_SUBMIT_SUCCESS: 'txt_cccs_collection_form_submitted',
  CCCS_UPDATE_SUCCESS: 'txt_cccs_collection_form_updated',
  CCCS_FAILED: 'txt_cccs_collection_form_failed',
  CONFIRM_SUBMIT_CALL_RESULT: 'txt_confirm_submit_call_result',
  CONFIRM_CHANGE_CALL_RESULT: 'txt_confirm_change_call_result',
  CONFIRM_SUBMIT_CCCS_BODY: 'txt_confirm_submit_cccs_body',
  DECLINE_PROPOSAL_REASON_REQUIRED: 'txt_decline_proposal_reason_required',
  DISCONTINUE_PLAN_REASON_REQUIRED: 'txt_discontinue_plan_reason_required',
  ACTION_REASON_TEXT_REQUIRED: 'txt_action_reason_text_required',

  // I18N_BANKRUPTCY
  REMOVE_BANKRUPTCY_STATUS: 'txt_remove_bankruptcy_status',
  BANKRUPTCY_CONFIRM_REMOVE_STATUS: 'txt_bankruptcy_confirm_remove_status',
  BANKRUPTCY_CONFIRM_REMOVE_STATUS_TITLE:
    'txt_bankruptcy_confirm_remove_status_title',
  BANKRUPTCY_ADDITIONAL_ATTORNEY_INFO:
    'txt_bankruptcy_additional_attorney_information',
  BANKRUPTCY_TRUSTEE_INFO: 'txt_bankruptcy_trustee_information',
  BANKRUPTCY_CLERK_INFO: 'txt_bankruptcy_clerk_court_information',

  TOAST_REMOVE_BANKRUPTCY_SUCCESS: 'txt_bankruptcy_remove_success',
  TOAST_REMOVE_BANKRUPTCY_FAILURE: 'txt_bankruptcy_remove_failure',
  BANKRUPTCY_DOCUMENT: 'txt_bankruptcy_document',
  UPLOAD_BANKRUPTCY_TITLE: 'txt_upload_bankruptcy_title',
  UPLOAD_BANKRUPTCY_DESCRIPTION: 'txt_upload_bankruptcy_description',
  UPLOAD_BANKRUPTCY_UPLOADED: 'txt_upload_bankruptcy_uploaded',

  // I18N_LONG_TEAM_MEDICAL
  LONG_TEAM_MEDICAL: 'txt_long_term_medical',
  LONG_TEAM_MEDICAL_INFORMATION: 'txt_long_term_medical_information',
  LONG_TEAM_MEDICAL_ENTER_IMPACT: 'txt_long_term_medical_enter_impact',
  LONG_TEAM_MEDICAL_REQUIRED: 'txt_long_term_medical_required',
  LONG_TEAM_MEDICAL_IMPACT: 'txt_long_term_medical_impact',

  // I18N_INCARCERATION
  INCARCERATION: 'txt_incarceration',
  INCARCERATION_INFORMATION: 'txt_incarceration_information',
  INCARCERATION_CONTACT_NAME: 'txt_incarceration_contact_name',
  INCARCERATION_START_DATE: 'txt_incarceration_start_date',
  INCARCERATION_END_DATE: 'txt_incarceration_end_date',
  INCARCERATION_LOCATION: 'txt_incarceration_location',

  // Alert message
  ACCOUNT_STATUS_DECEASED: 'txt_account_status_is_deceased',
  ACCOUNT_STATUS_BANKRUPTCY: 'txt_account_status_is_bankruptcy',
  ACCOUNT_STATUS_BANKRUPTCY_STATUS_PENDING:
    'txt_account_status_is_bankruptcy_status_pending',
  ACCOUNT_STATUS_DECEASED_STATUS_PENDING:
    'txt_account_status_is_deceased_status_pending',
  ACCOUNT_STATUS_HARDSHIP: 'txt_account_status_is_hardship',
  ACCOUNT_STATUS_NOT_HARDSHIP: 'txt_account_status_is_not_hardship',
  ACCOUNT_STATUS_INCARCERATION: 'txt_account_status_is_incarceration',
  CCCS_PROPOSAL_ACCEPTED: 'txt_cccs_proposal_accepted',
  CCCS_PROPOSAL_PENDING: 'txt_cccs_proposal_pending',
  CCCS_IN_PROGRESS: 'txt_cccs_in_progress',

  // I18N_SETTLEMENTS
  SETTLEMENT: 'txt_settlements',
  SETTLEMENT_INFORMATION: 'txt_settlement_information',
  SETTLEMENT_SUGGEST_PAYMENT_AMOUNT: 'txt_suggest_payment_amount',
  SETTLEMENT_CARDHOLDER_ACCEPTANCE_TO_PAY: 'txt_cardholder_acceptance_to_pay',
  SETTLEMENT_PAYMENT_VIA: 'txt_payment_via',
  SETTLEMENT_COMMENT: 'txt_comment',
  SETTLEMENT_COMMENT_DESCRIPTION: 'txt_comment_description',
  SETTLEMENT_COMMENT_DESCRIPTION_REQUIRED: 'txt_comment_description_required',
  SETTLEMENT_COMMENT_PLACEHOLDER: 'txt_comment_placeholder',
  SETTLEMENT_COMMENT_OPTIONAL_PLACEHOLDER: 'txt_comment_optional',
  SETTLEMENT_PROMISE_TO_PAY: 'txt_promise_to_pay',
  SETTLEMENT_ACCEPTED_BALANCE_AMOUNT_DESCRIPTION:
    'txt_accepted_balance_amount_description',
  SETTLEMENT_PAYMENT_NOTE: 'txt_payment_note',
  SETTLEMENT_ACCEPTED_BALANCE_AMOUNT_REQUIRED:
    'txt_accepted_balance_amount_required',
  SETTLEMENT_ACCEPTED_BALANCE_AMOUNT_ERROR: 'txt_accepted_balance_amount_error',
  SETTLEMENT_TYPE_MEMO_REQUIRED: 'txt_type_is_required',
  SETTLEMENT_COMMENT_REQUIRED: 'txt_comment_is_required'
};

export const I18N_CARD_MAINTENANCE = {
  ADD_ADDRESS: 'txt_add_address',
  EDIT_ADDRESS: 'txt_edit_address',
  DELETE_ADDRESS: 'txt_delete_address',
  VIEW_ADD_ADDRESS_HISTORY: 'txt_view_address_history',
  ADDRESS_DETAIL: 'txt_address_detail',
  ADDRESS_STANDARD_NOTE: 'txt_standard_note',
  CARDHOLDER_MAINTENANCE: 'txt_cardholder_maintenance'
};

export const I18N_PENDING_LETTER = {
  CONFIRM_HEADER: 'txt_delete_letters_confirm_header',
  CONFIRM_BODY: 'txt_delete_letters_confirm_body',
  RETURN_MAIL_FLAG: 'txt_return_mail_flag'
};

export enum RELATED_ACCOUNT_LANG {
  TITLE = 'txt_related_account_title'
}

export enum SNAPSHOT_LANG {
  EDIT_EXTERNAL_STATUS = 'txt_edit_external_status'
}

export const I18N_CLIENT_CONFIG = {
  CLIENT_CONFIG_LIST: 'txt_client_config_list',
  ADD_CLIENT_CONFIG: 'txt_add_client_config',
  EDIT_CLIENT_CONFIG: 'txt_edit_client_config',
  DELETE_CLIENT_CONFIG: 'txt_delete_client_config',
  CLIENT_CONFIG_SETTINGS: 'txt_client_config_settings',
  SYSTEM_ID: 'txt_system_id',
  PRINCIPLE_ID: 'txt_principle_id',
  AGENT_ID: 'txt_agent_id',
  PROVIDE_HIERARCHY: 'txt_provide_hierarchy',
  PROVIDE_HIERARCHY_TOOLTIP: 'txt_provide_hierarchy_tooltip',
  ALL: 'txt_all',
  CUSTOM: 'txt_custom',
  CLIENT_ID: 'txt_client_Id',
  CLIENT_CONFIGURATION_ADDED: 'txt_client_configuration_added',
  CLIENT_CONFIGURATION_UPDATED: 'txt_client_configuration_updated',
  CLIENT_CONFIGURATION_CHANGE_STATUS: 'txt_client_configuration_change_status',
  CLIENT_CONFIGURATION_ALREADY_EXISTS:
    'txt_client_configuration_already_exists',
  CLIENT_CONFIGURATION_FAILED_TO_ADD: 'txt_client_configuration_failed_to_add',
  CLIENT_CONFIGURATION_FAILED_TO_UPDATE:
    'txt_client_configuration_failed_to_update',
  CLIENT_CONFIGURATION_FAILED_TO_CHANGE_STATUS:
    'txt_client_configuration_failed_to_change_status',
  DESCRIPTION_IS_REQUIRED: 'txt_description_is_required',
  CLIENT_ID_IS_REQUIRED: 'txt_client_id_is_required',
  SYSTEM_ID_IS_REQUIRED: 'txt_system_id_is_required',
  PRINCIPLE_ID_IS_REQUIRED: 'txt_principle_id_is_required',
  AGENT_ID_IS_REQUIRED: 'txt_agent_id_is_required',
  CLIENT: 'txt_client',
  SYSTEM: 'txt_system',
  PRINCIPLE: 'txt_principle',
  AGENT: 'txt_agent',
  CONFIRM_CHANGE_STATUS_CLIENT_CONFIGURATION:
    'txt_confirm_change_status_client_configuration',
  CONFIRM_CHANGE_STATUS_CLIENT_CONFIGURATION_TEXT:
    'txt_confirm_change_status_client_configuration_text',
  CLIENT_CONFIGURATION: 'txt_client_configuration',
  CLIENT_CONFIGURATION_LOWER_CASE: 'txt_client_configuration_lower_case',
  ACTION_CODE: 'txt_action_code',
  CLIENT_ACTION_CODE_CONFIGURATION: 'txt_client_action_code_configuration',
  ADD_ACTION_CODE: 'txt_add_action_code',
  EDIT_ACTION_CODE: 'txt_edit_action_code',
  ACTION_CODE_ALREADY_EXISTS: 'txt_action_code_already_exists',
  CODE_ALREADY_EXISTS: 'txt_code_already_exists',
  ACTIVATE_CALL_RESULT: 'txt_activate_call_result',
  ACTIVATE_CALL_RESULT_CONFIRM: 'txt_activate_call_result_confirm',
  DEACTIVATE_CALL_RESULT: 'txt_deactivate_call_result',
  DEACTIVATE_CALL_RESULT_CONFIRM: 'txt_deactivate_call_result_confirm',
  ACTION_CODE_CONFIRM: 'txt_action_code_confirm',
  ACTION_CODE_ADDED: 'txt_action_code_added',
  ACTION_CODE_FAILED_TO_ADD: 'txt_action_code_failed_to_add',
  ACTION_CODE_UPDATED: 'txt_action_code_updated',
  ACTION_CODE_FAILED_TO_UPDATE: 'txt_action_code_failed_to_update',
  ACTION_CODE_ACTIVATED: 'txt_action_code_activated',
  ACTION_CODE_FAILED_TO_ACTIVATE: 'txt_action_code_failed_to_activate',
  ACTION_CODE_DEACTIVATED: 'txt_action_code_deactivated',
  ACTION_CODE_FAILED_TO_DEACTIVATE: 'txt_action_code_failed_to_deactivate',
  CALL_RESULT_SINGULAR: 'txt_call_result_singular',
  TYPE_SLASH_TO_ADD_VARIABLES: 'txt_type_slash_to_add_variables',
  ENTER_MEMO_HERE: 'txt_enter_memo_here',
  NEXT_WORK_DATE_VARIABLE: 'txt_next_work_date_variable',
  BEHAVIOR_TYPE_IS_REQUIRED: 'txt_behavior_type_is_required',
  MEMO_IS_REQUIRED: 'txt_memo_is_required',
  ACTION_TYPE_NAME: 'txt_action_type_name',
  CALL_RESULT_ACTION_TYPE_COUNT: 'txt_call_result_action_type_count',
  LAST_MODIFIED_ON: 'txt_last_modified_on',
  BY: 'txt_by',
  CHANGE_HISTORY: 'txt_change_history',
  ACTION_CODE_MEMO_NON_SPECIAL: 'txt_action_code_memo_non_special',
  MEMO_CHARACTERS_LEFT: 'txt_memo_characters_left',
  COUNT_AS_COUNT: 'txt_count_as_contact',
  YES: 'txt_yes',
  NO: 'txt_no',
  SAVE_CLIENT_CONFIG_MESSAGE: 'txt_save_client_config_message',
  EDIT_CLIENT_CONFIG_MESSAGE: 'txt_edit_client_config_message'
};

export const I18N_CODE_TO_TEXT = {
  CODE_TO_TEXT: 'txt_code_to_txt',
  CODE_TO_TEXT_DESCRIPTION: 'txt_code_to_txt_description',
  ADD_CODE: 'txt_add_code',
  CODE: 'txt_code',
  EDIT_CODE: 'txt_edit_code',
  DELETE_CODE: 'txt_delete_code',
  SAVE: 'txt_save',
  CANCEL: 'txt_cancel',
  SUBMIT: 'txt_submit',
  NO_DATA: 'txt_no_data',
  DELETE: 'txt_delete',
  DESCRIPTION: 'txt_description',
  DELETE_MODAL_CONTENT: 'txt_code_to_txt_delete_modal_content',
  CODE_TO_TEXT_ADD_MESSAGE: 'txt_code_to_txt_add_msg',
  CODE_TO_TEXT_DELETE_MESSAGE: 'txt_code_to_txt_delete_msg',
  CODE_TO_TEXT_EDIT_MESSAGE: 'txt_code_to_txt_edit_msg',
  CODE_TO_TEXT_ADD_SERVER_ERROR: 'txt_code_to_txt_add_server_err',
  CODE_TO_TEXT_DELETE_SERVER_ERROR: 'txt_code_to_txt_delete_server_err',
  CODE_TO_TEXT_EDIT_SERVER_ERROR: 'txt_code_to_txt_edit_server_err',
  CODE_TO_TEXT_CODE_DUPLICATE_MESSAGE: 'txt_code_to_txt_code_duplicate_msg'
};

export const I18N_DEBT_COMPANY_TO_TEXT = {
  DEBT_MANAGEMENT_COMPANIES: 'txt_debt_management_companies',
  DELETE_DEBT_COMPANY_SUCCESS: 'txt_delete_debt_company_success',
  DELETE_DEBT_COMPANY_FAIL: 'txt_delete_debt_company_fail',
  ADD_DEBT_COMPANY_SUCCESS: 'txt_add_debt_company_success',
  ADD_DEBT_COMPANY_FAIL: 'txt_add_debt_company_fail',
  UPDATE_DEBT_COMPANY_SUCCESS: 'txt_update_debt_company_success',
  UPDATE_DEBT_COMPANY_FAIL: 'txt_update_debt_company_fail'
};

export const I18N_COLLECTOR_PRODUCTIVITY = {
  COLLECTOR_PRODUCTIVITY: 'txt_collector_productivity',
  VIEW_PROMISES_PAYMENTS_STATISTICS: 'txt_view_promises_payments_statistics',
  SETTINGS: 'txt_settings',
  REPORT: 'txt_report',
  TIME_PERIOD: 'txt_time_period',
  REPORT_TYPE: 'txt_report_type',
  DATE_RANGE: 'txt_date_range',
  GOAL_ID: 'txt_goal_id',
  DESCRIPTION: 'txt_description',
  GOAL: 'txt_goal',
  GOALS: 'txt_goals',
  BREAKS: 'txt_breaks',
  ACTUAL: 'txt_actual',
  CODE: 'txt_code',
  ACTIVITY: 'txt_activity',
  TOTAL_TIME: 'txt_total_time',
  TOTAL_NUMBER: 'txt_total_number',
  AVERAGE_TIME: 'txt_average_time',
  NO_DATA: 'txt_no_data',
  DATE_RANGE_IS_REQUIRED: 'txt_date_range_is_required'
};

export const I18N_PROMISES_PAYMENTS_STATISTICS = {
  SETTINGS: 'txt_settings',
  NO_DATA: 'txt_no_data',
  RESET_TO_DEFAULT: 'txt_reset_to_default',
  RUN: 'txt_run',
  ACCOUNT_NUMBER: 'txt_account_number',
  TYPE: 'txt_type',
  METHOD: 'txt_method',
  PROMISE_AMOUNT: 'txt_promise_amount',
  PAID_AMOUNT: 'txt_paid_amount',
  STATUS: 'txt_status',
  REPORT: 'txt_report',
  PROMISE_DATE: 'txt_promise_date',
  ENTERED_DATE: 'txt_entered_date',
  UPDATED_DATE: 'txt_updated_date',
  TIME_PERIOD: 'txt_time_period',
  MOST_300_RECENT_RECORDS: 'txt_most_300_recent_records',
  ENTERED_PROMISE_DATE: 'txt_entered_promise_date',
  DUE_PROMISE_DATE: 'txt_due_promise_date',
  REPORT_TYPE: 'txt_report_type',
  INDIVIDUAL: 'txt_individual',
  ROLL_UP: 'txt_roll_up',
  PROMISE_TYPE: 'txt_promise_type',
  NON_RECURRING_MULTIPLE_PROMISE: 'txt_pm_non_recurring_multiple_promise',
  PROMISE_TO_PAY: 'txt_pp_promise_to_pay',
  RECURRING_MULTIPLE_PROMISE: 'txt_pr_recurring_multiple_promise',
  PROMISE_STATUS: 'txt_promise_status',
  KEPT_PROMISE: 'txt_kept_promise',
  BROKEN_PROMISE: 'txt_broken_promise',
  PENDING_PROMISE: 'txt_pending_promise',
  PROMISE_AMOUNT_FROM: 'txt_from',
  PROMISE_AMOUNT_TO: 'txt_to',
  COLLECTOR_NAME: 'txt_collector_name',
  TEAM_NAME: 'txt_team_name',
  COLLECTOR_IS_REQUIRED: 'txt_collector_name_is_required',
  TEAM_IS_REQUIRED: 'txt_team_name_is_required',
  ENTERED_PROMISE_DATE_IS_REQUIRED: 'txt_entered_promise_date_is_required',
  DUE_PROMISE_DATE_IS_REQUIRED: 'txt_due_promise_date_is_required',
  TYPE_AT_LEAST_3_CHARACTERS: 'txt_type_at_least_3_characters'
};

export const I18N_REFERENCE = {
  REFERENCES: 'txt_references',
  REFERENCES_DESCRIPTION: 'txt_reference_description',
  REFERENCES_NOTE: 'txt_reference_note',
  ADD_CODE: 'txt_add_code',
  FIELDS: 'txt_fields',
  CODE: 'txt_code',
  EDIT_CODE: 'txt_edit_code',
  DELETE_CODE: 'txt_delete_code',
  SAVE: 'txt_save',
  CANCEL: 'txt_cancel',
  SUBMIT: 'txt_submit',
  NO_DATA: 'txt_no_data',
  DELETE: 'txt_delete',
  DESCRIPTION: 'txt_description',
  DELETE_MODAL_CONTENT: 'txt_reference_delete_modal_content',
  DEACTIVATE: 'txt_reference_deactivate',
  ACTIVATE: 'txt_reference_activate',
  ACTIVATE_MODAL_CONTENT: 'txt_reference_activate_modal_content',
  DEACTIVATE_MODAL_CONTENT: 'txt_reference_deactivate_modal_content',
  REFERENCE_ADD_MESSAGE: 'txt_reference_add_msg',
  REFERENCE_DELETE_MESSAGE: 'txt_reference_delete_msg',
  REFERENCE_EDIT_MESSAGE: 'txt_reference_edit_msg',
  REFERENCE_ADD_SERVER_ERROR: 'txt_reference_add_server_err',
  REFERENCE_ACTIVATE_MESSAGE: 'txt_reference_activate_msg',
  REFERENCE_DEACTIVATE_MESSAGE: 'txt_reference_deactivate_msg',
  REFERENCE_DELETE_SERVER_ERROR: 'txt_reference_delete_server_err',
  REFERENCE_EDIT_SERVER_ERROR: 'txt_reference_edit_server_err',
  REFERENCE_DEACTIVATE_SERVER_ERROR: 'txt_reference_deactivate_server_err',
  REFERENCE_ACTIVATE_SERVER_ERROR: 'txt_reference_activate_server_err',
  REFERENCE_CODE_DUPLICATE_MESSAGE: 'txt_reference_code_duplicate_msg',
  REFERENCE_SECTION_TITLE: 'txt_reference_section_title',
  REFERENCE_ADJUSTMENT_GROUP_CODE_FAILED_TO_UPDATE:
    'txt_adjustment_group_code_failed_to_update',
  REFERENCE_DECEASED_CONTACT_CODE_FAILED_TO_UPDATE:
    'txt_deceased_contact_code_failed_to_update',
  REFERENCE_DELINQUENCY_REASON_CODE_FAILED_TO_UPDATE:
    'txt_delinquency_reason_code_failed_to_update',
  REFERENCE_MOVE_TO_QUEUE_CODE_FAILED_TO_UPDATE:
    'txt_move_to_queue_code_failed_to_update'
};

export const I18N_CHANGE_HISTORY = {
  DATE_AND_TIME: 'txt_date_&_time',
  CHANGED_BY: 'txt_changed_by',
  CSPA: 'txt_cspa',
  ACTION: 'txt_action',
  CHANGED_CATEGORY: 'txt_changed_category',
  CHANGED_SECTION: 'txt_changed_section',
  CHANGED_ITEM: 'txt_changed_item',
  BEFORE: 'txt_before',
  AFTER: 'txt_after',
  FILTER: 'txt_filter',
  DATE_RANGE: 'txt_date_range',
  CHANGED_CATEGORY_LIST: 'txt_changed_category_list',
  CSPA_LIST: 'txt_cspa_list',
  RESET_TO_DEFAULT: 'txt_reset_to_default',
  APPLY: 'txt_apply',
  NO_DATA: 'txt_no_data',
  NO_RESULTS_FOUND_SELECT_ANOTHER_FILTER:
    'txt_no_results_found_select_another_filter',
  CLEAR_AND_RESET: 'txt_clear_and_reset'
};

export const I18N_LETTER_CONFIG = {
  LETTER_NUMBER: 'txt_letter_number',
  LETTER_DESCRIPTION: 'txt_letter_description',
  LETTER_CONFIG_ID: 'txt_letter_config_id',
  LETTER_CONFIG_NUMBER: 'txt_letter_config_number',
  LETTER_TYPE_CODE: 'txt_letter_type_code',
  LETTER_CONFIG_DESCRIPTION: 'txt_letter_config_description',
  ACTIONS: 'txt_actions',
  ADD_LETTER: 'txt_letter_config_add_title',
  ADD_LETTER_CONFIG_SUCCESS: 'txt_letter_config_add_success',
  ADD_LETTER_CONFIG_FAIL: 'txt_letter_config_add_fail',
  DELETE_LETTER_CONFIRM_TITLE: 'txt_letter_config_delete_confirm',
  DELETE_LETTER_CONFIFM_BODY: 'txt_letter_config_delete_body',
  LETTER_CONFIG: 'txt_letter_config',
  LETTER_CONFIG_TITLE: 'txt_letter_config_title',
  DELETE_LETTER_CONFIG_SUCCESS: 'txt_letter_config_delete_success',
  DELETE_LETTER_CONFIG_FAIL: 'txt_letter_config_delete_fail',
  MODAL_ADD_TITLE: 'txt_letter_config_add_title',
  MODAL_EDIT_TITLE: 'txt_letter_config_edit_title',
  LETTER_VARIABLES: 'txt_letter_variables',
  LETTER_CAPTION_FIELDS: 'txt_letter_caption_fields',
  GENERATE_LETTER_FIELD_MAPPING: 'txt_generate_letter_field_mapping',
  ADD_VARIABLE: 'txt_add_variable',
  EDIT_LETTER_CONFIG_SUCCESS: 'txt_letter_config_edit_success',
  EDIT_LETTER_CONFIG_FAIL: 'txt_letter_config_edit_fail',
  LETTER_ID_IS_REQUIRED: 'txt_letter_id_is_required',
  LETTER_NUMBER_IS_REQUIRED: 'txt_letter_number_is_required',
  LETTER_NUMBER_ALREADY_EXISTS: 'txt_letter_number_already_exists',
  LETTER_TYPE_CODE_IS_REQUIRED: 'txt_letter_type_code_is_required',
  LETTER_DESCRIPTION_IS_REQUIRED: 'txt_letter_description_is_required',
  LETTER_CATION_FIELDS_IS_REQUIRED: 'txt_letter_caption_fields_is_required',
  GENERATE_LETTER_FIELD_MAPPING_IS_REQUIRED:
    'txt_generate_letter_field_mapping_is_required'
};

export const I18N_GENERAL_INFORMATION = {
  NEXT_WORK_DATE: 'txt_next_work_date',
  MOVE_TO_QUEUE: 'txt_move_to_queue',
  DELINQUENCY_REASON: 'txt_delinquency_reason',
  NEXT_WORK_DATE_IS_REQUIRED: 'txt_next_work_date_is_required',
  MOVE_TO_QUEUE_IS_REQUIRED: 'txt_move_to_queue_is_required'
};

export const I18N_LIVEVOX_DIALER = {
  TOAST_LIVEVOX_SAVE_TERM_CODE_FAIL: 'txt_livevox_save_term_code_failed',
  TOAST_LIVEVOX_SAVE_TERM_CODE_SUCCESS: 'txt_livevox_save_term_code_success'
};

export const I18N_DIALER_AGENT_MODAL = {
  TXT_AGENT_LOGIN_BUTTON: 'txt_dialer_agent_login_button',
  TXT_AGENT_LOGIN_SUBMIT_BUTTON: 'txt_dialer_agent_login_submit_button'
};

export const I18N_REF_DATA = {
  SSN_CANNOT_BE_COMBINED_WITH_OTHER_PARAMETERS:
    'txt_ssn_cannot_be_combined_with_other_parameters.',
  ACCOUNT_NUMBER_CANNOT_BE_COMBINED_WITH_OTHER_PARAMETERS:
    'txt_account_number_cannot_be_combined_with_other_parameters.',
  AFFINITY_NUMBER_CANNOT_BE_COMBINED_WITH_OTHER_PARAMETERS:
    'txt_affinity_number_cannot_be_combined_with_other_parameters.',
  PHONE_NUMBER_CANNOT_BE_COMBINED_WITH_OTHER_PARAMETERS:
    'txt_phone_number_cannot_be_combined_with_other_parameters.',
  CANNOT_BE_COMBINED_WITH_FIRST_NAME: 'txt_cannot_be_combined_with_first_name.',
  CANNOT_BE_COMBINED_WITH_LAST_NAME: 'txt_cannot_be_combined_with_last_name.',
  CANNOT_BE_COMBINED_WITH_MIDDLE_INITIAL:
    'txt_cannot_be_combined_with_middle_initial.',
  FIRST_NAME_IS_REQUIRED: 'txt_first_name_is_required.',
  LAST_NAME_IS_REQUIRED: 'txt_last_name_is_required.',
  MIDDLE_INITIAL_IS_REQUIRED: 'txt_middle_initial_is_required.',
  ZIP_CODE_IS_REQUIRED: 'txt_zip_code_is_required.',
  MONTH: 'txt_month',
  YEAR: 'txt_year',
  JANUARY: 'txt_january',
  FEBRUARY: 'txt_february',
  MARCH: 'txt_march',
  APRIL: 'txt_april',
  MAY: 'txt_may',
  JUNE: 'txt_june',
  JULY: 'txt_july',
  AUGUST: 'txt_august',
  SEPTEMBER: 'txt_september',
  OCTOBER: 'txt_october',
  NOVEMBER: 'txt_november',
  DECEMBER: 'txt_december',
  TYPE_TO_SEARCH: 'txt_type_to_search',
  NO_RESULTS_FOUND: 'txt_no_results_found',
  RESULTS_PER_PAGE: 'txt_results_per_page',
  USERNAME: 'txt_username',
  PASSWORD: 'txt_password',
  ALL_ITEMS_SELECTED: 'txt_all_items_selected',
  EXACT: 'txt_exact',
  MODERATE: 'txt_moderate',
  NAVIGATE: 'txt_navigate',
  TAB: 'txt_tab',
  SELECT_OR_FINISH_ADDING: 'txt_select_or_finish_adding',
  ENTER: 'txt_enter',
  SEARCH_RESULTS: 'txt_search_results',
  QUICK_ACTIONS: 'txt_quick_actions',
  CYCLE_TO_DATE: 'txt_cycle_to_date',
  NEW_BALANCE: 'txt_new_balance',
  CREDIT_COUNT: 'txt_credit_count',
  MIN_PAY_DUE: 'txt_min_pay_due',
  CASH_BALANCE: 'txt_cash_balance',
  PURCHASE_COUNT: 'txt_purchase_count',
  PURCHASE_AMOUNT: 'txt_purchase_amount',
  CREDIT_AMOUNT: 'txt_credit_amount',
  CASH_COUNT: 'txt_cash_count',
  CASH_AMOUNT: 'txt_cash_amount',
  PAYMENT_COUNT: 'txt_payment_count',
  ADJUSTMENT_COUNT: 'txt_adjustment_count',
  ADJUSTMENT_AMOUNT: 'txt_adjustment_amount',
  APR_CASH: 'txt_apr_cash',
  APR_MERCHANT: 'txt_apr_merchant',
  LATE_FEES: 'txt_late_fees',
  OVER_LIMIT_FEES: 'txt_over_limit_fees',
  'FINANCE_CHARGE_(P)': 'txt_finance_charge_(p)',
  'FINANCE_CHARGE_(C)': 'txt_finance_charge_(c)',
  CASH_BALANCE_ADB: 'txt_cash_balance_adb',
  PURCHASE_BALANCE_ADB: 'txt_purchase_balance_adb',
  BEHAVIOR: 'txt_behavior',
  BUREAU_SCORE: 'txt_bureau_score',
  'THE_SYSTEM_COULD_NOT_VERIFY_CVV/CVC_DUE_TO_INSUFFICIENT_TRACK_1_DATA':
    'txt_the_system_could_not_verify_cvv/cvc_due_to_insufficient_track_1_data',
  'THE_SYSTEM_COULD_NOT_VERIFY_CVV/CVC_DUE_TO_INSUFFICIENT_TRACK_2_DATA':
    'txt_the_system_could_not_verify_cvv/cvc_due_to_insufficient_track_2_data',
  'THE_CVV2/CVC2_VALUE_DID_NOT_MATCH_THE_VALUE_ON_THE_ACCOUNT_RECORD':
    'txt_the_cvv2/cvc2_value_did_not_match_the_value_on_the_account_record',
  'THE_CVV2/CVC2_VALUE_WAS_NOT_PROCESSED':
    'txt_the_cvv2/cvc2_value_was_not_processed',
  THE_CVV2_VALUE_WAS_ON_THE_PLASTIC: 'txt_the_cvv2_value_was_on_the_plastic',
  'THE_ISSUER_IS_EITHER_NOT_CERTIFIED_BY_VISA_OR_DID_NOT_PROVIDE_THE_CVV2_KEYS_TO_VISA.THIS_CODE_IS_USED_ONLY_FOR_VISA_TRANSACTIONS':
    'txt_the_issuer_is_either_not_certified_by_visa_or_did_not_provide_the_cvv2_keys_to_visa.this_code_is_used_only_for_visa_transactions',
  'THE_CVV/CVC_IS_INVALID_THE_VALUE_ON_TRACK_1_IS_000':
    'txt_the_cvv/cvc_is_invalid_the_value_on_track_1_is_000',
  'THE_CVV/CVC_IS_VALID': 'txt_the_cvv/cvc_is_valid',
  'THE_CVV/CVC_IS_INVALID_THE_VALUE_ON_TRACK_2_IS_000':
    'txt_the_cvv/cvc_is_invalid_the_value_on_track_2_is_000',
  'THE_SYSTEM_COULD_NOT_VERIFY_CVV/CVC_DUE_TO_NO_MAGNETIC_STRIPE':
    'txt_the_system_could_not_verify_cvv/cvc_due_to_no_magnetic_stripe',
  'THE_CAVV/CVV/CVC_IS_INVALID.FOR_CVV/CVC':
    'txt_the_cavv/cvv/cvc_is_invalid.for_cvv/cvc',
  CAVV_VALIDATION_COULD_NOT_BE_PERFORMED_BECAUSE_AUTHENTICATION_WAS_ATTEMPTED:
    'txt_cavv_validation_could_not_be_performed_because_authentication_was_attempted',
  CAVV_VALIDATION_COULD_NOT_BE_PERFORMED_DUE_TO_AUTHENTICATION_SYSTEM_ERROR_OR_FAILURE:
    'txt_cavv_validation_could_not_be_performed_due_to_authentication_system_error_or_failure',
  'THE_CVV/CVC_IS_INVALID_THE_SYSTEM_USED_THE_VALUE_FROM_TRACK_1_DATA':
    'txt_the_cvv/cvc_is_invalid_the_system_used_the_value_from_track_1_data.',
  'THE_CVV/CVC_IS_INVALID_THE_SYSTEM_USED_THE_VALUE_FROM_TRACK_2_DATA':
    'txt_the_cvv/cvc_is_invalid_the_system_used_the_value_from_track_2_data.',
  'THE_SYSTEM_COULD_NOT_VERIFY_THE_CVV/CVC_DUE_TO_INSUFFICIENT_TRACK_1_DATA':
    'txt_the_system_could_not_verify_the_cvv/cvc_due_to_insufficient_track_1_data.',
  'THE_SYSTEM_COULD_NOT_VERIFY_THE_CVV/CVC_DUE_TO_INSUFFICIENT_TRACK_2_DATA':
    'txt_the_system_could_not_verify_the_cvv/cvc_due_to_insufficient_track_2_data.',
  THE_TRANSACTION_DID_NOT_INCLUDE_TRACK_DATA:
    'txt_the_transaction_did_not_include_track_data',
  ADDRESS_MATCHES_ZIP_CODE_DOES_NOT_MATCH:
    'txt_address_matches_zip_code_does_not_match',
  STREET_ADDRESSES_MATCH_FOR_INTERNATIONAL_TRANSACTION:
    'txt_street_addresses_match_for_international_transaction',
  STREET_ADDRESS_AND_POSTAL_CODE_NOT_VERIFIED_FOR_INTERNATIONAL_TRANSACTION_DUE_TO_INCOMPATIBLE_FORMATS:
    'txt_street_address_and_postal_code_not_verified_for_international_transaction_due_to_incompatible_formats',
  STREET_ADDRESSES_AND_POSTAL_CODES_MATCH_FOR_INTERNATIONAL_TRANSACTION:
    'txt_street_addresses_and_postal_codes_match_for_international_transaction',
  TRANSACTION_INELIGIBLE_FOR_AVS: 'txt_transaction_ineligible_for_avs',
  'ADDRESS_INFORMATION_NOT_VERIFIED_FOR_INTERNATIONAL_TRANSACTION_GLOBAL_NON-AVS_PARTICIPANT':
    'txt_address_information_not_verified_for_international_transaction_global_non-avs_participant',
  RESERVED_FOR_FUTURE_USE: 'txt_reserved_for_future_use',
  NEITHER_ADDRESS_NOR_ZIP_CODE_MATCHES:
    'txt_neither_address_nor_zip_code_matches',
  'N/NOT_APPLICABLE_VERIFICATION_REQUEST_NOT_GENERATED':
    'txt_n/not_applicable_verification_request_not_generated',
  POSTAL_CODES_MATCH_FOR_INTERNATIONAL_TRANSACTION:
    'txt_postal_codes_match_for_international_transaction',
  REENTER_AVS_UNAVAILABLE: 'txt_reenter_avs_unavailable',
  AVS_IS_NOT_SUPPORTED: 'txt_avs_is_not_supported',
  ADDRESS_INFORMATION_NOT_VERIFIED_FOR_DOMESTIC_TRANSACTION:
    'txt_address_information_not_verified_for_domestic_transaction',
  'NINE-DIGIT_ZIP_CODE_MATCHES_ADDRESS_DOES_NOT_MATCH':
    'txt_nine-digit_zip_code_matches_address_does_not_match',
  'ADDRESS_AND_9-DIGIT_ZIP_CODE_MATCH':
    'txt_address_and_9-digit_zip_code_match',
  'ADDRESS_AND_5-DIGIT_ZIP_CODE_MATCH':
    'txt_address_and_5-digit_zip_code_match',
  'THREE_OR_FIVE-DIGIT_ZIP_CODE_MATCHES_ADDRESS_DOES_NOT_MATCH':
    'txt_three_or_five-digit_zip_code_matches_address_does_not_match',
  '004_VISA-ASSIGNED_TRANSACTION_IDENTIFIER_TRANSACTION_DID_NOT_QUALIFY_FOR_CPS':
    'txt_004_visa-assigned_transaction_identifier_transaction_did_not_qualify_for_cps',
  '009_FDR-ASSIGNED_TRANSACTION_IDENTIFIER_TRANSACTION_DID_NOT_QUALIFY_FOR_CPS':
    'txt_009_fdr-assigned_transaction_identifier_transaction_did_not_qualify_for_cps',
  '014_VISA-ASSIGNED_TRANSACTION_IDENTIFIER_TRANSACTION_QUALIFIED_FOR_CPS':
    'txt_014_visa-assigned_transaction_identifier_transaction_qualified_for_cps',
  '019_FDR-ASSIGNED_TRANSACTION_IDENTIFIER_TRANSACTION_QUALIFIED_FOR_CPS':
    'txt_019_fdr-assigned_transaction_identifier_transaction_qualified_for_cps',
  SORT_HARD_COPY_STATEMENTS_ACCORDING_TO_CUSTOMER_ACCOUNT_IDENTIFIER_AND_MAIL_TO_YOU:
    'txt_sort_hard_copy_statements_according_to_customer_account_identifier_and_mail_to_you.',
  SYSTEM_GENERATED_CODE_THAT_REPRESENTS_DEFAULT_CARDHOLDER_PRICING_STRATEGY_ZBFD_HAS_BEEN_ASSIGNED_TO_THE_CUSTOMER:
    'txt_system_generated_code_that_represents_default_cardholder_pricing_strategy_zbfd_has_been_assigned_to_the_customer.',
  SEND_HARD_COPY_STATEMENT_TO_CUSTOMER:
    'txt_send_hard_copy_statement_to_customer.',
  RETURNED_MAIL_DO_NOT_PRINT_A_HARD_COPY_STATEMENT:
    'txt_returned_mail_do_not_print_a_hard_copy_statement.',
  SEND_HARD_COPY_STATEMENT_TO_FDR_FRAUD_MANAGEMENT_SERVICES:
    'txt_send_hard_copy_statement_to_fdr_fraud_management_services.',
  ENLARGE_STATEMENT_CONTENTS_TO_FIT_ON_11X14_PAPER_AND_SEND_TO_CUSTOMER:
    'txt_enlarge_statement_contents_to_fit_on_11x14_paper_and_send_to_customer.',
  'DO_NOT_PRINT_A_HARD_COPY_STATEMENT._INSTEAD':
    'txt_do_not_print_a_hard_copy_statement._instead',
  SEND_HARD_COPY_STATEMENT_TO_YOU: 'txt_send_hard_copy_statement_to_you.',
  SEND_HARD_COPY_STATEMENT_TO_YOU_FOR_THIS_NUMBER_OF_BILLING_CYCLES:
    'txt_send_hard_copy_statement_to_you_for_this_number_of_billing_cycles',
  THE_TRACK_1_NAME_WAS_NOT_VERIFIED: 'txt_the_track_1_name_was_not_verified.',
  THE_TRACK_1_NAME_WAS_MONITORED_ONLY:
    'txt_the_track_1_name_was_monitored_only',
  THE_TRACK_1_NAME_WAS_VERIFIED: 'txt_the_track_1_name_was_verified.',
  THE_TRACK_1_NAME_WAS_NOT_CHECKED: 'txt_the_track_1_name_was_not_checked.',
  '0_STATEMENT': 'txt_0_statement',
  '1_FOR_FIRST_DATA_USE_ONLY': 'txt_1_for_first_data_use_only',
  '2_AUTHORIZATION': 'txt_2_authorization',
  '253_MERCHANDISE_SALE': 'txt_253_merchandise_sale',
  '254_CASH_ADVANCE': 'txt_254_cash_advance',
  '255_MERCHANDISE_RETURN': 'txt_255_merchandise_return',
  '271_PAYMENT': 'txt_271_payment',
  '280_SPECIFIC_CREDIT_AMOUNT_ADJUSTMENT':
    'txt_280_specific_credit_amount_adjustment',
  '281_CASH_ADVANCE_ITEM_CHARGE_REFUND':
    'txt_281_cash_advance_item_charge_refund',
  '282_LATE_CHARGE_REFUND': 'txt_282_late_charge_refund',
  '283_MERCHANDISE_FINANCE_CHARGE_REFUND':
    'txt_283_merchandise_finance_charge_refund',
  '284_CASH_ADVANCE_FINANCE_CHARGE_REFUND':
    'txt_284_cash_advance_finance_charge_refund',
  '286_MERCHANDISE_ITEM_CHARGE_REFUND':
    'txt_286_merchandise_item_charge_refund',
  '287_OVERLIMIT_CHARGE_REFUND': 'txt_287_overlimit_charge_refund',
  '400_ACCOUNT_TRANSFER': 'txt_400_account_transfer',
  '401_RESERVED_FOR_FIRST_DATA_USE_ONLY_THIS_TRANSACTION_CODE_INDICATES_THAT_AN_ADJUSTMENT_TRANSACTION_WAS_POSTED_DURING_AN_ACCOUNT_TRANSFER':
    'txt_401_reserved_for_first_data_use_only_this_transaction_code_indicates_that_an_adjustment_transaction_was_posted_during_an_account_transfer.',
  '900_FINANCE_CHARGE_(ITEM_CHARGE)': 'txt_900_finance_charge_(item_charge)',
  '910_MONETARY_TRANSACTION_THAT_HAS_BEEN_MEMO_POSTED_TO_THE_ACCOUNT_(COMMERCIAL_CARD_ACCOUNTS_ONLY)THIS_TRANSACTION_CODE_IS_ALSO_USED_FOR_MEMO_DETAILS_IDENTIFYING_THE_DIVERTED_FROM_ACCOUNT_NUMBER':
    'txt_910_monetary_transaction_that_has_been_memo_posted_to_the_account_(commercial_card_accounts_only)this_transaction_code_is_also_used_for_memo_details_identifying_the_diverted_from_account_number.',
  '911_MEMO_POSTED_RETURN': 'txt_911_memo_posted_return',
  '912_MEMO_POSTED_PAYMENT': 'txt_912_memo_posted_payment',
  '947_IVA_TAX_(LATIN_AMERICA)': 'txt_947_iva_tax_(latin_america)',
  '948_FOREIGN_TRANSACTION_FEE': 'txt_948_foreign_transaction_fee',
  '959_RESERVED_FOR_RESTRICTED_USE': 'txt_959_reserved_for_restricted_use',
  '960_RESERVED_FOR_RESTRICTED_USE': 'txt_960_reserved_for_restricted_use',
  '961_LATE_CHARGE': 'txt_961_late_charge',
  '962_CREDIT_INSURANCE_CHARGE': 'txt_962_credit_insurance_charge',
  '963_FINANCE_CHARGE_(CASH_OR_MERCHANDISE)':
    'txt_963_finance_charge_(cash_or_merchandise)',
  '964_MERCHANDISE_FINANCE_CHARGE': 'txt_964_merchandise_finance_charge',
  '965_OVERLIMIT_ACCOUNT_MESSAGE': 'txt_965_overlimit_account_message',
  '966_RESERVED_FOR_RESTRICTED_USE': 'txt_966_reserved_for_restricted_use',
  '967_ACCOUNT_LEVEL_PROCESSING_SM_STRATEGY_CHANGE_MESSAGE':
    'txt_967_account_level_processing_sm_strategy_change_message',
  '976_RESERVED_FOR_RESTRICTED_USE': 'txt_976_reserved_for_restricted_use',
  '977_REBATE_REDEEMED_(COBRAND_ACCOUNTS_ONLY)':
    'txt_977_rebate_redeemed_(cobrand_accounts_only)',
  '978_RESERVED_FOR_RESTRICTED_USE': 'txt_978_reserved_for_restricted_use',
  '979_RESERVED_FOR_RESTRICTED_USE': 'txt_979_reserved_for_restricted_use',
  '980_ANCILLARY_FEE_DESCRIPTION': 'txt_980_ancillary_fee_description',
  '981_ACCOUNT_BALANCE_DETAILS_FOR_MULTRAN_ACCOUNTS':
    'txt_981_account_balance_details_for_multran_accounts',
  '984_BACKDATED_CASH_ADVANCE_FINANCE_CHARGE':
    'txt_984_backdated_cash_advance_finance_charge',
  '985_BACKDATED_PAYMENT_INTEREST_ADJUSTMENTS':
    'txt_985_backdated_payment_interest_adjustments',
  '986_CASH_ADVANCE_ITEM_CHARGE': 'txt_986_cash_advance_item_charge',
  '987_MERCHANDISE_ITEM_CHARGE': 'txt_987_merchandise_item_charge',
  '988_OVERLIMIT_CHARGE': 'txt_988_overlimit_charge',
  '989_INTEREST_EARNED_OR_CREDIT_INTEREST':
    'txt_989_interest_earned_or_credit_interest',
  '990_SERVICE_CHARGE_OR_STATEMENT_CHARGE':
    'txt_990_service_charge_or_statement_charge',
  '991_CASH_ADVANCE_ITEM_CHARGE': 'txt_991_cash_advance_item_charge',
  '992_FEE_OR_FINANCE_CHARGE_REBATES': 'txt_992_fee_or_finance_charge_rebates',
  '993_BACKDATED_PAYMENT_OR_CASH_ADVANCE_ADJUSTMENTS':
    'txt_993_backdated_payment_or_cash_advance_adjustments',
  '994_SKIP_PAYMENT_CERTIFICATE_MESSAGE':
    'txt_994_skip_payment_certificate_message',
  '997_RETAIL_SPECIAL_DETAILS': 'txt_997_retail_special_details',
  '998_AIRLINE_ITINERARY_SPECIAL_DETAILS':
    'txt_998_airline_itinerary_special_details',
  "999_FOR_FIRST_DATA_USE_ONLY'": "txt_999_for_first_data_use_only'",
  '01_PLASTIC_CARD': 'txt_01_plastic_card',
  '02_CONTACTLESS_CHIP_CARD': 'txt_02_contactless_chip_card',
  '03_EXT_TOKEN_MOBILE_DEVICE': 'txt_03_ext_token_mobile_device',
  '04_EXT_TOKEN_CARD_ON_FILE': 'txt_04_ext_token_card_on_file',
  '06_EMV_DUAL_INTERFACE_CHIP_CARD': 'txt_06_emv_dual_interface_chip_card',
  '07_MOBILE_DEVICE': 'txt_07_mobile_device',
  '08_EMV_CONTACT_ONLY_CHIP_CARD': 'txt_08_emv_contact_only_chip_card',
  '99_UNASSIGNED': 'txt_99_unassigned',
  A_FLOOR_LIMIT_OF_ZERO_IS_REQUIRED: 'txt_a_floor_limit_of_zero_is_required.',
  THE_MERCHANT_TERMINAL_IS_CAPABLE_OF_DISPLAYING_THE_ACCOUNT_IDENTIFIER_ENCODED_ON_THE_MAGNETIC_STRIPE_OF_A_PLASTIC:
    'txt_the_merchant_terminal_is_capable_of_displaying_the_account_identifier_encoded_on_the_magnetic_stripe_of_a_plastic.',
  A_FLOOR_LIMIT_OF_ZERO_IS_REQUIRED_AND_THE_MERCHANT_TERMINAL_IS_CAPABLE_OF_DISPLAYING_THE_ACCOUNT_IDENTIFIER_ENCODED_ON_THE_MAGNETIC_STRIPE_OF_A_PLASTIC:
    'txt_a_floor_limit_of_zero_is_required_and_the_merchant_terminal_is_capable_of_displaying_the_account_identifier_encoded_on_the_magnetic_stripe_of_a_plastic.',
  'AUTHORIZATION_WAS_APPROVED_OFFLINE_BY_MEMBER_CONTROLLED_AUTHORIZATION_SERVICE_(MCAS)':
    'txt_authorization_was_approved_offline_by_member_controlled_authorization_service_(mcas).',
  AUTHORIZATION_WAS_APPROVED_OFFLINE_BY_MCAS_FOR_RANDOMLY_SELECTED_ONLINE_TRANSACTION_WITH_DATA_COMMUNICATION_FAILURE:
    'txt_authorization_was_approved_offline_by_mcas_for_randomly_selected_online_transaction_with_data_communication_failure.',
  AUTHORIZATION_WAS_APPROVED_OFFLINE_BY_MCAS_FOR_RANDOMLY_SELECTED_ONLINE_TRANSACTION_WITH_NO_RESPONSE:
    'txt_authorization_was_approved_offline_by_mcas_for_randomly_selected_online_transaction_with_no_response.',
  AUTHORIZATION_WAS_BY_REFERRAL: 'txt_authorization_was_by_referral.',
  'POST-AUTHORIZATION_OR_CREDIT_WAS_ENTERED_OFFLINE':
    'txt_post-authorization_or_credit_was_entered_offline.',
  '0_ADVICE_OF_EXCEPTION_FILE_UPDATE': 'txt_0_advice_of_exception_file_update.',
  '1_RESPONSE_WAS_STIP': 'txt_1_response_was_stip',
  '2_RESPONSE_WAS_LCS': 'txt_2_response_was_lcs',
  '3_RESPONSE_WAS_STIP': 'txt_3_response_was_stip',
  '4_RESPONSE_WAS_STIP': 'txt_4_response_was_stip',
  '5_AUTHORIZATION_WAS_APPROVED_BY_ISSUER':
    'txt_5_authorization_was_approved_by_issuer.',
  '6_RESPONSE_WAS_STIP': 'txt_6_response_was_stip',
  AUTHORIZATION_WAS_APPROVED_BY_THE_ACQUIRER_BECAUSE_BASE_I_WAS_UNAVAILABLE:
    'txt_authorization_was_approved_by_the_acquirer_because_base_i_was_unavailable.',
  AUTHORIZATION_WAS_APPROVED_BY_THE_ACQUIRER_BY_REFERRAL:
    'txt_authorization_was_approved_by_the_acquirer_by_referral.',
  AUTHORIZATION_WAS_APPROVED_BY_AUTOMATED_REFERRAL_SERVICE:
    'txt_authorization_was_approved_by_automated_referral_service.',
  '009_FIRST_DATA-ASSIGNED_TRANSACTION_IDENTIFIER_TRANSACTION_DID_NOT_QUALIFY_FOR_CPS':
    'txt_009_first_data-assigned_transaction_identifier_transaction_did_not_qualify_for_cps',
  '019_FIRST_DATA-ASSIGNED_TRANSACTION_IDENTIFIER_TRANSACTION_QUALIFIED_FOR_CPS':
    'txt_019_first_data-assigned_transaction_identifier_transaction_qualified_for_cps',
  'SINGLE_TRANSACTION_OF_A_MAIL/TELEPHONE_ORDER':
    'txt_single_transaction_of_a_mail/telephone_order',
  'RECURRING_TRANSACTION_OF_A_MAIL/TELEPHONE_ORDER':
    'txt_recurring_transaction_of_a_mail/telephone_order',
  INSTALLMENT_BILLING: 'txt_installment_billing',
  UNKNOWN_CLASSIFICATION: 'txt_unknown_classification',
  'SECURE_ELECTRONIC_TRANSACTION_(SET)_WITH_CARDHOLDER_CERTIFICATE':
    'txt_secure_electronic_transaction_(set)_with_cardholder_certificate',
  NON_AUTHENTICATED_SECURITY_TRANSACTION_WITH_SET_MERCHANT_CERTIFICATE:
    'txt_non_authenticated_security_transaction_with_set_merchant_certificate',
  NON_AUTHENTICATED_SECURITY_TRANSACTION_WITHOUT_SET_MERCHANT_CERTIFICATE:
    'txt_non_authenticated_security_transaction_without_set_merchant_certificate',
  NON_SECURE_TRANSACTION: 'txt_non_secure_transaction',
  NON_AUTHENTICATED_SECURITY_TRANSACTION_THAT_DOES_NOT_MEET_SET_REQUIREMENTS_FROM_A_MERCHANT_WITH_SET_CAPABILITIES:
    'txt_non_authenticated_security_transaction_that_does_not_meet_set_requirements_from_a_merchant_with_set_capabilities',
  NO_ACCOUNT_SPECIFIED: 'txt_no_account_specified',
  SAVINGS_ACCOUNT_SPECIFIED: 'txt_savings_account_specified',
  CHECKING_ACCOUNT_SPECIFIED: 'txt_checking_account_specified',
  CREDIT_CARD_ACCOUNT_SPECIFIED: 'txt_credit_card_account_specified',
  '00_PAN_ENTRY_MODE_UNKNOWN': 'txt_00_pan_entry_mode_unknown',
  '01_PAN_MANUAL_ENTRY': 'txt_01_pan_manual_entry',
  '02_PAN_AUTO-ENTRY_VIA_MAGNETIC_STRIPE_TRACK_DATA_IS_NOT_REQUIRED_OR_ACQUIRER_IS_NOT_QUALIFIED_TO_SUBMIT_MAGNETIC_STRIPE_TRANSACTIONS':
    'txt_02_pan_auto-entry_via_magnetic_stripe_track_data_is_not_required_or_acquirer_is_not_qualified_to_submit_magnetic_stripe_transactions',
  '03_PAN_AUTO-ENTRY_VIA_BAR_CODE_READER':
    'txt_03_pan_auto-entry_via_bar_code_reader',
  '04_PAN_AUTO-ENTRY_VIA_OPTICAL_CHARACTER_READER_(OCR)':
    'txt_04_pan_auto-entry_via_optical_character_reader_(ocr)',
  '05_PAN_AUTO-ENTRY_VIA_CHIP': 'txt_05_pan_auto-entry_via_chip',
  '06_KEY-ENTERED_ONLY_CAPABILITY': 'txt_06_key-entered_only_capability',
  '07_PAN_AUTO-ENTRY_VIA_CONTACTLESS_M/CHIP':
    'txt_07_pan_auto-entry_via_contactless_m/chip',
  '09_PAN/TOKEN_ENTRY_VIA_ELECTRONIC_COMMERCE_CONTAINING_DSRP_CRYPTOGRAM_IN_DE_55_(INTEGRATED_CIRCUIT_CARD_[ICC]_SYSTEM-RELATED_DATA)':
    'txt_09_pan/token_entry_via_electronic_commerce_containing_dsrp_cryptogram_in_de_55_(integrated_circuit_card_[icc]_system-related_data)',
  '10_CREDENTIAL_ON_FILE': 'txt_10_credential_on_file',
  '79_POINT_OF_SALE_ENTRY_MODE': 'txt_79_point_of_sale_entry_mode',
  '80_POINT_OF_SALE_ENTRY_MODE': 'txt_80_point_of_sale_entry_mode',
  '81_POINT_OF_SALE_ENTRY_MODE': 'txt_81_point_of_sale_entry_mode',
  '82_POINT_OF_SALE_ENTRY_MODE': 'txt_82_point_of_sale_entry_mode',
  '90_PAN_AUTO-ENTRY_VIA_MAGNETIC_STRIPE':
    'txt_90_pan_auto-entry_via_magnetic_stripe',
  '91_PAN_AUTO-ENTRY_VIA_CONTACTLESS_MAGNETIC_STRIPE':
    'txt_91_pan_auto-entry_via_contactless_magnetic_stripe',
  AUTHORIZATIONS_ARE_PROHIBITED: 'txt_authorizations_are_prohibited',
  BANKRUPT: 'txt_bankrupt',
  CLOSED: 'txt_closed',
  REVOKED: 'txt_revoked',
  FROZEN: 'txt_frozen',
  INTEREST_ACCRUAL_PROHIBITED: 'txt_interest_accrual_prohibited',
  LOST: 'txt_lost',
  STOLEN: 'txt_stolen',
  CHARGED_OFF: 'txt_charged_off',
  VALID: 'txt_valid',
  INVALID: 'txt_invalid',
  SOLICIT_ALLOWED: 'txt_solicit_allowed',
  DO_NOT_SOLICIT: 'txt_do_not_solicit',
  INVALID_NUMBER: 'txt_invalid_number',
  LAND_LINE: 'txt_land_line',
  UNKNOWN_TYPE: 'txt_unknown_type',
  CELL_PHONE: 'txt_cell_phone',
  DO_NOT_CONTACT_CEASE_AND_DESIST: 'txt_do_not_contact_cease_and_desist',
  NUMBER_NOT_VALID: 'txt_number_not_valid',
  DISCONNECTED: 'txt_disconnected',
  NO_LONGER_AT_THIS_NUMBER: 'txt_no_longer_at_this_number',
  CONSENT_FOR_VOICE_CONTACT: 'txt_consent_for_voice_contact',
  'H-DO_NOT_CONTACT': 'txt_h-do_not_contact',
  CONSENT_FOR_SMS_TEXT_MESSAGES_AND_VOICE:
    'txt_consent_for_sms_text_messages_and_voice',
  CONSENT_FOR_SMS_TEXT_MESSAGES_ONLY: 'txt_consent_for_sms_text_messages_only',
  NUMBER_IS_UNLISTED: 'txt_number_is_unlisted',
  DO_NOT_CONTACT: 'txt_do_not_contact',
  NUMBER_IS_NOT_VERIFIED: 'txt_number_is_not_verified',
  DO_NOT_CONTACT_THE_CUSTOMER: 'txt_do_not_contact_the_customer',
  NUMBER_IS_VALID_MANUAL_DIAL_ONLY: 'txt_number_is_valid_manual_dial_only',
  'CO-MAKER': 'txt_co-maker',
  DECEASED: 'txt_deceased',
  DELETE_ACCT: 'txt_delete_acct',
  DO_NOT_REPORT: 'txt_do_not_report',
  IND_WITH_AUTH_USER: 'txt_ind_with_auth_user',
  JOINT: 'txt_joint',
  MAKER: 'txt_maker',
  RESTORE: 'txt_restore',
  TERMINATED: 'txt_terminated',
  UNKNOWN_GENDER: 'txt_unknown_gender',
  MALE: 'txt_male',
  FEMALE: 'txt_female',
  NO_SOLICITING: 'txt_no_soliciting',
  NO_TELEMARKETING: 'txt_no_telemarketing',
  PERMANENT: 'txt_permanent',
  REPEATING: 'txt_repeating',
  TEMPORARY: 'txt_temporary',
  BLL1_BILLING: 'txt_bll1_billing',
  LTTR_LETTER: 'txt_lttr_letter',
  PLST_PLASTICS: 'txt_plst_plastics',
  RFRN_REFERENCE: 'txt_rfrn_reference',
  FAILURE: 'txt_failure',
  SPOUSE: 'txt_spouse',
  ADMINISTRATOR: 'txt_administrator',
  BACK_OFFICE: 'txt_back_office',
  BRANCH: 'txt_branch',
  MERCHANT_OR_OTHER: 'txt_merchant_or_other',
  POWER_OF_ATTORNEY: 'txt_power_of_attorney',
  THIRD_PARTY: 'txt_third_party',
  ATTORNEY: 'txt_attorney',
  BANK_ROUTING_NUMBER_MUST_BE_9_DIGITS:
    'txt_bank_routing_number_must_be_9_digits.',
  CHECKING_ACCOUNT_NUMBER_MUST_BE_AT_LEAST_5_DIGITS:
    'txt_checking_account_number_must_be_at_least_5_digits.',
  SAVINGS_ACCOUNT_NUMBER_MUST_BE_AT_LEAST_5_DIGITS:
    'txt_savings_account_number_must_be_at_least_5_digits.',
  CREDIT_LINE_CHANGE: 'txt_credit_line_change',
  CROSS_REFERENCE: 'txt_cross_reference',
  CREDIT_LIFE: 'txt_credit_life',
  FLAGS: 'txt_flags',
  DISPUTED: 'txt_disputed',
  ACCOUNTS: 'txt_accounts',
  HISTORY: 'txt_history',
  NORMAL: 'txt_normal',
  ADJUST: 'txt_adjust',
  CYCLE: 'txt_cycle',
  ACTIVE: 'txt_active',
  INACTIVE: 'txt_inactive'
};
