export const addressRelationship = [
  {
    fieldID: 'B',
    fieldValue: 'Business Street Address',
    description: 'txt_b_business_street_address'
  },
  {
    fieldID: 'H',
    fieldValue: 'Home or Residence Street Address',
    description: 'txt_h_home_or_residence_street_address'
  },
  {
    fieldID: 'O',
    fieldValue: 'Other',
    description: 'txt_o_other'
  },
  {
    fieldID: 'U',
    fieldValue: 'Unknown',
    description: 'txt_u_unknown'
  }
];
