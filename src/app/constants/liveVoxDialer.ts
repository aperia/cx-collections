export const LV_ACCESS = {
  TOKEN: 'fb433ced-d75a-4daf-ba9c-18ed30f56de7'
};

export const AGENT_STATUS = {
  INCALL: 'INCALL',
  READY: 'READY',
  NOTREADY: 'NOTREADY',
  WRAPUP: 'WRAPUP'
};

export const LIVEVOX_CLIENT_NAME = {
  CLIENT_NAME: 'fiserv_dev'
};
