export const addressCategory = [
  {
    fieldValue: 'Permanent',
    fieldID: 'P',
    description: 'txt_p_permanent'
  },
  {
    fieldValue: 'Repeating',
    fieldID: 'R',
    description: 'txt_r_repeating'
  },
  {
    fieldValue: 'Temporary',
    fieldID: 'T',
    description: 'txt_t_temporary'
  }
];
