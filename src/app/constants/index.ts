import { BadgeProps, SortType } from 'app/_libraries/_dls/components';

export const placeHolderPhone = '(__) ___-____';

export const COMMON_CODE_ERROR = {
  noData: 455
};

export const DEFAULT_HOME_PHONE = '0000000000';

export const COMMON_STATUS = {
  Submitted: 'green',
  Inactive: 'grey',
  Pending: 'orange',
  Canceled: 'grey',
  Kept: 'green',
  Broken: 'red',
  Open: 'cyan',
  FinalFailed: 'red',
  Normal: 'green',
  // Internal/External Statuses
  Blank: 'green',
  D: 'orange',
  N: 'orange',
  O: 'orange',
  X: 'red',
  A: 'orange',
  B: 'red',
  C: 'grey',
  E: 'red',
  F: 'red',
  I: 'orange',
  L: 'orange',
  U: 'orange',
  Z: 'red'
} as Record<string, BadgeProps['color']>;

export const AUTHORIZATION_METHOD_CODE = {
  '0': 'txt_authorization_method_code_0',
  '1': 'txt_authorization_method_code_1',
  '2': 'txt_authorization_method_code_2',
  '3': 'txt_authorization_method_code_3',
  '4': 'txt_authorization_method_code_4',
  '5': 'txt_authorization_method_code_5',
  '6': 'txt_authorization_method_code_6',
  '7': 'txt_authorization_method_code_7',
  '8': 'txt_authorization_method_code_8',
  C: 'txt_authorization_method_code_9'
} as Record<string, string>;

export const AUTHORIZATION_LINE_ID_METHOD_CODE = {
  BAS: 'txt_visa',
  B: 'txt_visa',
  FDR: 'txt_first_data_resources',
  'FIRST DATA': 'txt_intra_fdc_transaction',
  INA: 'txt_mastercard_acquiring',
  IZ: 'txt_mastercard_interchange',
  OTH: 'txt_other',
  ZAM: 'txt_amex',
  Zxxx: 'txt_bank_line_identification'
} as Record<string, string>;

export const AUTHORIZATION_FILTER_METHOD_CODE = {
  A: 'txt_aged',
  D: 'txt_not_affect_account',
  // space
  '': '  - Affects account'
} as Record<string, string>;

export const AUTHORIZATION_TYPE_CODE_METHOD_CODE = {
  AA: 'txt_authorization_adjustment',
  AP: 'txt_preauthorization_request_approved',
  CA: 'txt_cash_advance_authorization_approved',
  CD: 'txt_cash_advance_authorization_declined',
  CE: 'txt_cash_advance_authorization_approved_by_expansion',
  CO: 'txt_override_declined_cash_advance',
  CP: 'txt_cash_advance_authorization_approved_by_positive_identification',
  CS: 'txt_cash_referral',
  CT: 'txt_cash_advance_authorization_approved_by_name',
  CX: 'txt_cash_advance_authorization_manually_declined',
  DA: 'txt_duplicate_authorization',
  DC: 'txt_cash_authorization_approved_without_issuer_contact',
  DM: 'txt_merchandise_authorization_approved_without_issuer_contact',
  DP: 'txt_preauthorization_request_denied',
  IA: 'txt_issuer’s_adjustment',
  MA: 'txt_merchandise_authorization_approved_the_partial_approval_message_appears_if_the_authorization_amount_was_approved_for_a_partial_amount',
  MC: 'txt_in-store_return',
  MD: 'txt_merchandise_authorization_declined',
  ME: 'txt_merchandise_authorization_approved_by_expansion',
  MI: 'txt_right-time_payment',
  MO: 'txt_override_declined_merchandise_authorization',
  MP: 'txt_merchandise_authorization_approved_by_positive_identification',
  MR: 'txt_right-time_pay/,reversal',
  MS: 'txt_merchandise_referral',
  MT: 'txt_merchandise_authorization_approved_by_name',
  MX: 'txt_merchandise_authorization_manually_declined',
  PA: 'txt_permanent_authorization_request',
  PP: 'txt_visa_and_mastercard_person-to-person_payment_transaction_approval',
  PR: 'txt_visa_and_mastercard_person-to-person_payment_transaction_reversal',
  PX: 'txt_visa_and_mastercard_person-to-person_payment_transaction_decline',
  PZ: 'txt_permanent_authorization_adjustment',
  QA: 'txt_merchandise_return_adjustment',
  QD: 'txt_merchandise_return_declined',
  QQ: 'txt_959_reserved_for_restricted_use',
  QR: 'txt_merchandise_return_approved',
  QZ: 'txt_merchandise_return_adjustment_declined',
  VA: 'txt_private_label_etc_return_posted_at_the_time_of_return',
  XC: 'txt_manual_override_cash_advance_authorization',
  XM: 'txt_manual_override_merchandise_authorization'
} as Record<string, string>;

export const NA_CVV_VERIFICATION_METHOD_CODE = {
  A: 'txt_the_system_could_not_verify_cvv/cvc_due_to_insufficient_track_1_data',
  B: 'txt_the_system_could_not_verify_cvv/cvc_due_to_insufficient_track_2_data',
  N: 'txt_the_cvv2/cvc2_value_did_not_match_the_value_on_the_account_record',
  P: 'txt_the_cvv2/cvc2_value_was_not_processed',
  S: 'txt_the_cvv2_value_was_on_the_plastic',
  U: 'txt_the_issuer_is_either_not_certified_by_visa_or_did_not_provide_the_cvv2_keys_to_visa.this_code_is_used_only_for_visa_transactions',
  X: 'txt_the_cvv/cvc_is_invalid_the_value_on_track_1_is_000',
  Y: 'txt_the_cvv/cvc_is_valid',
  Z: 'txt_the_cvv/cvc_is_invalid_the_value_on_track_2_is_000',
  0: 'txt_the_system_could_not_verify_cvv/cvc_due_to_no_magnetic_stripe',
  1: 'txt_the_cavv/cvv/cvc_is_invalid.for_cvv/cvc',
  2: 'txt_the_cvv/cvc_is_invalid_the_system_used_the_value_from_track_2_data',
  3: 'txt_cavv_validation_could_not_be_performed_because_authentication_was_attempted',
  4: 'txt_cavv_validation_could_not_be_performed_due_to_authentication_system_error_or_failure',
  // blank
  '': 'txt_transaction_did_not_require_a_cvv/cvc_verification'
} as Record<string, string>;

export const NA_PIN_VERIFICATION_METHOD_CODE = {
  1: 'txt_the_cvv/cvc_is_invalid_the_system_used_the_value_from_track_1_data',
  2: 'txt_the_cvv/cvc_is_invalid_the_system_used_the_value_from_track_2_data',
  A: 'txt_the_system_could_not_verify_the_cvv/cvc_due_to_insufficient_track_1_data',
  B: 'txt_the_system_could_not_verify_the_cvv/cvc_due_to_insufficient_track_2_data',
  X: 'txt_the_cvv/cvc_is_invalid_the_value_on_track_1_is_000',
  Y: 'txt_the_cvv/cvc_is_valid',
  Z: 'txt_the_cvv/cvc_is_invalid_the_value_on_track_2_is_000',
  // blank
  '': 'txt_the_transaction_did_not_include_track_data'
} as Record<string, string>;

export const NA_AVS = {
  A: 'txt_address_matches_zip_code_does_not_match',
  B: 'txt_street_addresses_match_for_international_transaction',
  C: 'txt_street_address_and_postal_code_not_verified_for_international_transaction_due_to_incompatible_formats',
  D: 'txt_street_addresses_and_postal_codes_match_for_international_transaction',
  E: 'txt_transaction_ineligible_for_avs',
  G: 'txt_address_information_not_verified_for_international_transaction_global_non-avs_participant',
  I: 'txt_reserved_for_future_use',
  N: 'txt_neither_address_nor_zip_code_matches',
  'N/A': 'txt_not_applicable_verification_request_not_generated',
  P: 'txt_postal_codes_match_for_international_transaction',
  R: 'txt_reenter_avs_unavailable',
  S: 'txt_avs_is_not_supported',
  U: 'txt_address_information_not_verified_for_domestic_transaction',
  W: 'txt_nine-digit_zip_code_matches_address_does_not_match',
  X: 'txt_address_and_9-digit_zip_code_match',
  Y: 'txt_address_and_5-digit_zip_code_match',
  Z: 'txt_three_or_five-digit_zip_code_matches_address_does_not_match'
} as Record<string, string>;

export const TRANSACTION_SOURCE_IDENTIFIER_METHOD_CODE = {
  '004':
    'txt_004_visa-assigned_transaction_identifier_transaction_did_not_qualify_for_cps',
  '009':
    'txt_009_fdr-assigned_transaction_identifier_transaction_did_not_qualify_for_cps',
  '014':
    'txt_014_visa-assigned_transaction_identifier_transaction_qualified_for_cps',
  '019':
    'txt_019_fdr-assigned_transaction_identifier_transaction_qualified_for_cps'
} as Record<string, string>;

export const HELD_STATEMENT_DESTINATION_CODE = {
  C: 'txt_sort_hard_copy_statements_according_to_customer_account_identifier_and_mail_to_you',
  D: 'txt_system_generated_code_that_represents_default_cardholder_pricing_strategy_zbfd_has_been_assigned_to_the_customer',
  N: 'txt_send_hard_copy_statement_to_customer',
  R: 'txt_returned_mail_do_not_print_a_hard_copy_statement',
  S: 'txt_send_hard_copy_statement_to_fdr_fraud_management_services',
  V: 'txt_enlarge_statement_contents_to_fit_on_11x14_paper_and_send_to_customer',
  W: 'txt_do_not_print_a_hard_copy_statement._instead',
  Y: 'txt_send_hard_copy_statement_to_you',
  '1-9':
    'txt_send_hard_copy_statement_to_you_for_this_number_of_billing_cycles',
  // blank
  '': 'txt_send_hard_copy_statement_to_customer'
} as Record<string, string>;

export const NAME_MATCH_CODE = {
  N: 'txt_the_track_1_name_was_not_verified',
  X: 'txt_the_track_1_name_was_monitored_only',
  Y: 'txt_the_track_1_name_was_verified',
  // blank
  '': 'txt_the_track_1_name_was_not_checked'
} as Record<string, string>;

export const TRANSACTION_CODE = {
  0: 'txt_0_statement',
  1: 'txt_1_for_first_data_use_only',
  2: 'txt_2_authorization',
  253: 'txt_253_merchandise_sale',
  254: 'txt_254_cash_advance',
  255: 'txt_255_merchandise_return',
  271: 'txt_271_payment',
  280: 'txt_280_specific_credit_amount_adjustment',
  281: 'txt_281_cash_advance_item_charge_refund',
  282: 'txt_281_cash_advance_item_charge_refund',
  283: 'txt_281_cash_advance_item_charge_refund',
  284: 'txt_284_cash_advance_finance_charge_refund',
  286: 'txt_286_merchandise_item_charge_refund',
  287: 'txt_287_overlimit_charge_refund',
  400: 'txt_400_account_transfer',
  401: 'txt_401_reserved_for_first_data_use_only_this_transaction_code_indicates_that_an_adjustment_transaction_was_posted_during_an_account_transfer',
  900: 'txt_900_finance_charge_(item_charge)',
  910: 'txt_910_monetary_transaction_that_has_been_memo_posted_to_the_account_(commercial_card_accounts_only)this_transaction_code_is_also_used_for_memo_details_identifying_the_diverted_from_account_number',
  911: 'txt_911_memo_posted_return',
  912: 'txt_912_memo_posted_payment',
  947: 'txt_947_iva_tax_(latin_america)',
  948: 'txt_947_iva_tax_(latin_america)',
  959: 'txt_959_reserved_for_restricted_use',
  960: 'txt_960_reserved_for_restricted_use',
  961: 'txt_961_late_charge',
  962: 'txt_962_credit_insurance_charge',
  963: 'txt_963_finance_charge_(cash_or_merchandise)',
  964: 'txt_964_merchandise_finance_charge',
  965: 'txt_965_overlimit_account_message',
  966: 'txt_966_reserved_for_restricted_use',
  967: 'txt_967_account_level_processing_sm_strategy_change_message',
  976: 'txt_976_reserved_for_restricted_use',
  977: 'txt_977_rebate_redeemed_(cobrand_accounts_only)',
  978: 'txt_978_reserved_for_restricted_use',
  979: 'txt_979_reserved_for_restricted_use',
  980: 'txt_980_ancillary_fee_description',
  981: 'txt_981_account_balance_details_for_multran_accounts',
  984: 'txt_984_backdated_cash_advance_finance_charge',
  985: 'txt_985_backdated_payment_interest_adjustments',
  986: 'txt_986_cash_advance_item_charge',
  987: 'txt_987_merchandise_item_charge',
  988: 'txt_988_overlimit_charge',
  989: 'txt_989_interest_earned_or_credit_interest',
  990: 'txt_990_service_charge_or_statement_charge',
  991: 'txt_991_cash_advance_item_charge',
  992: 'txt_992_fee_or_finance_charge_rebates',
  993: 'txt_993_backdated_payment_or_cash_advance_adjustments',
  994: 'txt_994_skip_payment_certificate_message',
  997: 'txt_997_retail_special_details',
  998: 'txt_998_airline_itinerary_special_details',
  999: 'txt_999_for_first_data_use_only'
} as Record<string, string>;

export const PRESENTATION_INSTRUMENT_TYPE_CODE = {
  '01': 'txt_01_plastic_card',
  '02': 'txt_02_contactless_chip_card',
  '03': 'txt_03_ext_token_mobile_device',
  '04': 'txt_04_ext_token_card_on_file',
  '06': 'txt_06_emv_dual_interface_chip_card',
  '07': 'txt_07_mobile_device',
  '08': 'txt_08_emv_contact_only_chip_card',
  '99': 'txt_99_unassigned'
} as Record<string, string>;

export const RISK_IDENTIFICATION_SERVICE_INDICATOR = {
  '1': 'txt_a_floor_limit_of_zero_is_required',
  '2': 'txt_the_merchant_terminal_is_capable_of_displaying_the_account_identifier_encoded_on_the_magnetic_stripe_of_a_plastic',
  '3': 'txt_a_floor_limit_of_zero_is_required_and_the_merchant_terminal_is_capable_of_displaying_the_account_identifier_encoded_on_the_magnetic_stripe_of_a_plastic'
} as Record<string, string>;

export const AUTHORIZATION_SOURCE_CODE = {
  A: 'txt_authorization_was_approved_offline_by_member_controlled_authorization_service_(mcas)',
  B: 'txt_authorization_was_approved_offline_by_mcas_for_randomly_selected_online_transaction_with_data_communication_failure',
  C: 'txt_authorization_was_approved_offline_by_mcas_for_randomly_selected_online_transaction_with_no_response',
  D: 'txt_authorization_was_by_referral',
  E: 'txt_post-authorization_or_credit_was_entered_offline',
  0: 'txt_0_advice_of_exception_file_update',
  1: 'txt_1_response_was_stip',
  2: 'txt_2_response_was_lcs',
  3: 'txt_3_response_was_stip',
  4: 'txt_4_response_was_stip',
  5: 'txt_5_authorization_was_approved_by_issuer',
  6: 'txt_6_response_was_stip',
  7: 'txt_authorization_was_approved_by_the_acquirer_because_base_i_was_unavailable',
  8: 'txt_authorization_was_approved_by_the_acquirer_by_referral',
  9: 'txt_authorization_was_approved_by_automated_referral_service'
} as Record<string, string>;

export const SOURCE_TRANSACTION_IDENTIFIER = {
  '004': 'assigned_transaction_identifier_transaction_did_not_qualify_for_cps',
  '009':
    'txt_009_first_data-assigned_transaction_identifier_transaction_did_not_qualify_for_cps',
  '014': 'assigned_transaction_identifier_transaction_qualified_for_cps',
  '019':
    'txt_019_first_data-assigned_transaction_identifier_transaction_qualified_for_cps'
} as Record<string, string>;

export const MAIL_PHONE_INDICATOR = {
  '1': 'txt_single_transaction_of_a_mail/telephone_order',
  '2': 'txt_recurring_transaction_of_a_mail/telephone_order',
  '3': 'txt_installment_billing',
  '4': 'txt_unknown_classification',
  '5': 'txt_secure_electronic_transaction_(set)_with_cardholder_certificate',
  '6': 'txt_non_authenticated_security_transaction_with_set_merchant_certificate',
  '7': 'txt_non_authenticated_security_transaction_without_set_merchant_certificate',
  '8': 'txt_non_secure_transaction',
  '9': 'txt_non_authenticated_security_transaction_that_does_not_meet_set_requirements_from_a_merchant_with_set_capabilities'
} as Record<string, string>;

export const DUALITY_FLAG = {
  M: 'txt_primary_account_is_with_mastercard',
  V: 'txt_primary_account_is_with_visa'
} as Record<string, string>;

export const AUTOMATED_TELLER_MACHINE_ATM_FLAG = {
  '0': 'txt_no_account_specified',
  '1': 'txt_savings_account_specified',
  '2': 'txt_checking_account_specified',
  '3': 'txt_credit_card_account_specified'
} as Record<string, string>;

export const POINT_OF_SALE_ENTRY_MODE = {
  '00': 'txt_00_pan_entry_mode_unknown',
  '01': 'txt_01_pan_manual_entry',
  '02': 'txt_02_pan_auto',
  '03': 'txt_03_pan_auto-entry_via_bar_code_reader',
  '04': 'txt_04_pan_auto-entry_via_optical_character_reader_(ocr)',
  '05': 'txt_05_pan_auto-entry_via_chip',
  '06': 'txt_06_key-entered_only_capability',
  '07': 'txt_07_pan_auto-entry_via_contactless_m/chip',
  '09': 'txt_09_pan/token_entry_via_electronic_commerce_containing_dsrp_cryptogram_in_de_55_(integrated_circuit_card_[icc]_system-related_data)',
  '10': 'txt_10_credential_on_file',
  '79': 'txt_79_point_of_sale_entry_mode',
  '80': 'txt_80_point_of_sale_entry_mode',
  '81': 'txt_81_point_of_sale_entry_mode',
  '82': 'txt_82_point_of_sale_entry_mode',
  '90': 'txt_90_pan_auto-entry_via_magnetic_stripe',
  '91': 'txt_91_pan_auto-entry_via_contactless_magnetic_stripe'
} as Record<string, string>;

export const DESKTOP_WIDTH = 1024;

export const MEMO_TEXT_REGEX = '^([^\\\\"()+;%]*)$';
export const MEMO_CIS_TEXT_REGEX = '^([^\\\\"()+;%]*)$';
export const DATE_FORMAT = {
  FULL_DATE_TIME_DISPLAY: 'MM/dd/yyyy hh:mm:ss a'
};

export const NUMBER_PAGINATION = {
  START_SEQUENCE: 1,
  END_SEQUENCE_FIFTY: 50,
  STEP: 50
};
export enum SORT_TYPE {
  ASC = 'asc',
  DESC = 'desc'
}

export const CUSTOMER_ROLE_CODE = {
  primary: '01',
  secondary: '02',
  auth: '03'
};

export const COUNTRY_ID = {
  USA: 'USA'
};

export const EXTERNAL_STATUS = {
  A: 'txt_authorizations_are_prohibited',
  B: 'txt_bankrupt',
  C: 'txt_closed',
  R: 'txt_revoked',
  F: 'txt_frozen',
  I: 'txt_interest_accrual_prohibited',
  L: 'txt_lost',
  U: 'txt_stolen',
  Z: 'txt_charged_off'
} as MagicKeyValue;

export const CARD_ROLE = {
  '01': 'txt_primary',
  '02': 'txt_secondary',
  '03': 'txt_authorized'
} as MagicKeyValue;

export const EMAIL_STATUS = {
  Y: 'txt_y_valid',
  N: 'txt_n_invalid'
} as MagicKeyValue;

export const SOLICIT_FLAG = {
  Y: 'txt_y_solicit_allowed',
  N: 'txt_n_not_solicit'
} as MagicKeyValue;

export const CARD_DEVICE_TYPE = {
  I: 'txt_i_invalid_number',
  L: 'txt_l_land_line',
  U: 'txt_u_unknown_type',
  W: 'txt_w_cell_phone'
} as MagicKeyValue;

export const CARD_FLAG_TYPE = {
  C: 'txt_do_not_contact_cease_and_desist',
  N: 'txt_number_not_valid',
  D: 'txt_disconnected',
  E: 'txt_no_longer_at_this_number',
  G: 'txt_consent_for_voice_contact',
  H: 'txt_h-do_not_contact',
  B: 'txt_consent_for_sms_text_messages_and_voice',
  S: 'txt_consent_for_sms_text_messages_only',
  U: 'txt_number_is_unlisted',
  V: 'txt_number_is_not_verified',
  X: 'txt_do_not_contact_the_customer',
  Y: 'txt_number_is_valid_manual_dial_only'
} as MagicKeyValue;

export const AUTH_CBR_FLAG = {
  '1': 'txt_individual',
  '5': 'txt_co-maker',
  X: 'txt_deceased',
  D: 'txt_delete_acct',
  Z: 'txt_do_not_report',
  '3': 'txt_ind_with_auth_user',
  '2': 'txt_joint',
  '7': 'txt_maker',
  '?': 'txt_restore',
  T: 'txt_terminated'
} as MagicKeyValue;

export const CARD_SALUTATION = {
  U: 'txt_unknown_gender',
  M: 'txt_male',
  F: 'txt_female',
  C: 'txt_company'
} as MagicKeyValue;

export const CARD_SOLICITATION = {
  S: 'txt_no_soliciting',
  T: 'txt_no_telemarketing'
} as MagicKeyValue;

export const ADDRESS_CATEGORY_MAP = {
  P: 'txt_p_permanent',
  R: 'txt_r_repeating',
  T: 'txt_t_temporary'
};

export const ADDRESS_TYPE_MAP = {
  BLL1: 'txt_bll1_billing',
  LTTR: 'txt_lttr_letter',
  PLST: 'txt_plst_plastics',
  RFRN: 'txt_rfrn_reference'
};

export const RECORD_NOT_FOUND_MESSAGE = 'RECORD NOT FOUND';
export const FAILURE_MSG = 'failure';

export const ZERO = 0;
export const EMPTY_STRING = '';
export const EMPTY_STRING_OBJECT = '{}';
export const EMPTY_OBJECT: MagicKeyValue = {};
export const EMPTY_ARRAY: Array<any> = [];
export const DASH = '-';
export const SLASH = '/';
export const SPACE = ' ';
export const UNDERSCORE = '_';
export const SEMICOLON = ';';
export const MAPPING_STORE_KY = '__store_ky__';
export const FULFILLED = 'fulfilled';
export const REJECTED = 'rejected';
export const PENDING = 'pending';
export const INVALID_DATE = ['00000000'];

export const NO_RECORD_CODE = '455';

export const SUPPORTED_FILE_TYPES = ['pdf', 'jpg', 'png', 'tiff'];

export const NUMERIC_REGEX = '^[0-9]*$';

export const CARD_HOLDER_CONTACT_NUMBER = '698-589-1103';

export const PAGE_SIZE_COMMON: number[] = [10, 25, 50];
export const PAGE_SIZE_SMALL: number[] = [5, 25, 50];

export const TIMER_COMPONENT_ID = 'component_timer_config';
export const AUTHENTICATION_COMPONENT_ID = 'component_authentication';
export const EXTERNAL_STATUS_COMPONENT_ID = 'account-external-status-component';

export enum ROLE {
  ADMIN = 'CXADMN',
  USER = 'CXCOL',
  ADMIN1 = 'CXADMN1',
  ADMIN2 = 'CXADMN2',
  CXCOL1 = 'CXCOL1',
  CXCOL5 = 'CXCOL5',
  COLLECTOR1 = 'COLLECTOR1',
  SUPERVISOR = 'SUPERVISOR'
}

export const DEFAULT_CALLER = [
  {
    value: 'SP',
    description: 'txt_spouse'
  },
  {
    value: 'CD2',
    description: 'txt_administrator'
  },
  {
    value: 'NC',
    description: 'txt_back_office'
  },
  {
    value: 'CD1',
    description: 'txt_branch'
  },
  {
    value: 'DMC',
    description: 'txt_debt_management_company'
  },
  {
    value: 'CD3',
    description: 'txt_merchant_or_other'
  },
  {
    value: 'CD4',
    description: 'txt_power_of_attorney'
  },
  {
    value: 'CD5',
    description: 'txt_third_party'
  },
  {
    value: 'AT',
    description: 'txt_attorney'
  }
];

export const SPOUSE = 'SP';

export const DEFAULT_SORT_VALUE: SortType = {
  id: 'status',
  order: undefined
};

export const USER_TOKEN_ID = 'user-token';
export const ADMIN_ROLE = 'CXADMN';
export const CXMGR_ROLE = 'CXMGR';
export const CXSUP_ROLE = 'CXSUP';

export const DROPDOWN_SORT_QUEUE_LIST = [
  { description: 'txt_queue_name', value: 'queueName' },
  { description: 'txt_last_work_date', value: 'lastWorkDate' }
];

export const ORDER_DROPDOWN = [
  {
    description: 'txt_ascending',
    value: 'asc'
  },
  {
    description: 'txt_descending',
    value: 'desc'
  }
];
export const DROPDOWN_COLLECTOR = [{ description: 'txt_all', value: 'all' }];
