export const PAYMENT_STATUS: MagicKeyValue = {
  BROKEN: 'Broken',
  PENDING: 'Pending',
  KEPT: 'Kept'
};

export const PAYMENT_METHOD: MagicKeyValue = {
  CHECK: 'Check'
};

export const PROMISE_TYPE: MagicKeyValue = {
  PM: 'Non-recurring multiple promise',
  PP: 'Promise to Pay',
  PR: 'Recurring multiple promise'
};
