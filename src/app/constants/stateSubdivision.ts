export const stateSubdivision = [
  {
    fieldValue: 'APO/FPO Florida',
    fieldID: 'AA',
    countryCode: 'USA'
  },
  {
    fieldValue: 'APO/FPO New York',
    fieldID: 'AE',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Alabama',
    fieldID: 'AL',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Alaska',
    fieldID: 'AK',
    countryCode: 'USA'
  },
  {
    fieldValue: 'APO/FPO California',
    fieldID: 'AP',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Arkansas',
    fieldID: 'AR',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Arizona',
    fieldID: 'AZ',
    countryCode: 'USA'
  },
  {
    fieldValue: 'California',
    fieldID: 'CA',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Colorado',
    fieldID: 'CO',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Connecticut',
    fieldID: 'CT',
    countryCode: 'USA'
  },
  {
    fieldValue: 'District of Columbia',
    fieldID: 'DC',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Delaware',
    fieldID: 'DE',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Florida',
    fieldID: 'FL',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Georgia',
    fieldID: 'GA',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Hawaii',
    fieldID: 'HI',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Iowa',
    fieldID: 'IA',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Idaho',
    fieldID: 'ID',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Illinois',
    fieldID: 'IL',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Indiana',
    fieldID: 'IN',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Kansas',
    fieldID: 'KS',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Kentucky',
    fieldID: 'KY',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Louisiana',
    fieldID: 'LA',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Massachusetts',
    fieldID: 'MA',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Maryland',
    fieldID: 'MD',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Maine',
    fieldID: 'ME',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Michigan',
    fieldID: 'MI',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Minnesota',
    fieldID: 'MN',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Missouri',
    fieldID: 'MO',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Mississippi',
    fieldID: 'MS',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Montana',
    fieldID: 'MT',
    countryCode: 'USA'
  },
  {
    fieldValue: 'North Carolina',
    fieldID: 'NC',
    countryCode: 'USA'
  },
  {
    fieldValue: 'North Dakota',
    fieldID: 'ND',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Nebraska',
    fieldID: 'NE',
    countryCode: 'USA'
  },
  {
    fieldValue: 'New Hampshire',
    fieldID: 'NH',
    countryCode: 'USA'
  },
  {
    fieldValue: 'New Jersey',
    fieldID: 'NJ',
    countryCode: 'USA'
  },
  {
    fieldValue: 'New Mexico',
    fieldID: 'NM',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Nevada',
    fieldID: 'NV',
    countryCode: 'USA'
  },
  {
    fieldValue: 'New York',
    fieldID: 'NY',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Ohio',
    fieldID: 'OH',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Oklahoma',
    fieldID: 'OK',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Oregon',
    fieldID: 'OR',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Pennsylvania',
    fieldID: 'PA',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Puerto Rico',
    fieldID: 'PR',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Rhode Island',
    fieldID: 'RI',
    countryCode: 'USA'
  },
  {
    fieldValue: 'South Carolina',
    fieldID: 'SC',
    countryCode: 'USA'
  },
  {
    fieldValue: 'South Dakota',
    fieldID: 'SD',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Tennessee',
    fieldID: 'TN',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Texas',
    fieldID: 'TX',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Utah',
    fieldID: 'UT',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Virginia',
    fieldID: 'VA',
    countryCode: 'USA'
  },
  {
    fieldValue: 'U.S. Virgin Islands',
    fieldID: 'VI',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Vermont',
    fieldID: 'VT',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Washington',
    fieldID: 'WA',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Wisconsin',
    fieldID: 'WI',
    countryCode: 'USA'
  },
  {
    fieldValue: 'West Virginia',
    fieldID: 'WV',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Wyoming',
    fieldID: 'WY',
    countryCode: 'USA'
  },
  {
    fieldValue: 'Alberta',
    fieldID: 'AB',
    countryCode: 'CAN'
  },
  {
    fieldValue: 'British Columbia',
    fieldID: 'BC',
    countryCode: 'CAN'
  },
  {
    fieldValue: 'Manitoba',
    fieldID: 'MB',
    countryCode: 'CAN'
  },
  {
    fieldValue: 'New Brunswick',
    fieldID: 'NB',
    countryCode: 'CAN'
  },
  {
    fieldValue: 'Newfoundland and Labrador',
    fieldID: 'NL',
    countryCode: 'CAN'
  },
  {
    fieldValue: 'Nova Scotia',
    fieldID: 'NS',
    countryCode: 'CAN'
  },
  {
    fieldValue: 'Northwest Territories',
    fieldID: 'NT',
    countryCode: 'CAN'
  },
  {
    fieldValue: 'Nunavut',
    fieldID: 'NU',
    countryCode: 'CAN'
  },
  {
    fieldValue: 'Ontario',
    fieldID: 'ON',
    countryCode: 'CAN'
  },
  {
    fieldValue: 'Prince Edward Island',
    fieldID: 'PE',
    countryCode: 'CAN'
  },
  {
    fieldValue: 'Quebec',
    fieldID: 'QC',
    countryCode: 'CAN'
  },
  {
    fieldValue: 'Saskatchewan',
    fieldID: 'SK',
    countryCode: 'CAN'
  },
  {
    fieldValue: 'Yukon',
    fieldID: 'YT',
    countryCode: 'CAN'
  },
  {
    fieldValue: 'Australian-Capital Territory',
    fieldID: 'ACT',
    countryCode: 'AUS'
  },
  {
    fieldValue: 'Jervis Bay Territory',
    fieldID: 'JBT',
    countryCode: 'AUS'
  },
  {
    fieldValue: 'New South Wales',
    fieldID: 'NSW',
    countryCode: 'AUS'
  },
  {
    fieldValue: 'Northern Territory',
    fieldID: 'NT',
    countryCode: 'AUS'
  },
  {
    fieldValue: 'Queensland',
    fieldID: 'QLD',
    countryCode: 'AUS'
  },
  {
    fieldValue: 'South Australia',
    fieldID: 'SA',
    countryCode: 'AUS'
  },
  {
    fieldValue: 'Tasmania',
    fieldID: 'TAS',
    countryCode: 'AUS'
  },
  {
    fieldValue: 'Victoria',
    fieldID: 'VIC',
    countryCode: 'AUS'
  },
  {
    fieldValue: 'Western Australia',
    fieldID: 'WA',
    countryCode: 'AUS'
  }
];
