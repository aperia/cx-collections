export enum FormatTime {
  Year = 'YYYY',
  Date = 'MM/DD/YYYY',
  DateWithHyphen = 'MM-DD-YYYY',
  Datetime = 'MM/DD/YYYY hh:mm A',
  MonthDate = 'MM/DD',
  ShortYearMonth = 'YY/MM',
  ShortMonthShortYear = 'MM/YY',
  ShortYearShortMonthShortDate = 'YYMMDD',
  AllDatetime = 'MM/DD/YYYY hh:mm:ss A',
  FullTime = 'hh:mm:ss',
  FullTimeMeridiem = 'hh:mm:ss A',
  MonthYear = 'MMMM YYYY',
  ShortMonthYear = 'MM/YYYY',
  YearMonthDay = 'YYYY/MM/DD',
  YearMonthDayWithHyphen = 'YYYY-MM-DD',
  UTCDate = 'YYYY-MM-DD',
  UTCAllDatetime = 'YYYY-MM-DD HH:mm:ss',
  DefaultPostDate = 'MMDDYYYY',
  DefaultShortPostDate = 'MMDDYY',
  FullYearMonthDay = 'YYYYMMDD',
  ShortTimeMeridiem = 'hh:mm A'
}

export enum GroupTextAction {
  Copy = 'copy',
  ShowHide = 'showHide'
}

export enum GroupTextFormat {
  Api_Amount = 'api_amount',
  Api_Date = 'api_date',
  Number = 'number',
  Date = 'date',
  Text = 'text',
  Label = 'label',
  Amount = 'amount',
  Percent = 'percent',
  CardNumber = 'cardNumber',
  Status = 'status',
  TwoLinesAddress = 'twoLinesAddress',
  FiveLinesAddress = 'fiveLinesAddress',
  FormatType = 'formatType',
  Mask = 'mask',
  ShortMonthYear = 'shortMonthYear',
  Datetime = 'dateTime',
  MonthDate = 'monthDate',
  ShortYearMonth = 'shortYearMonth',
  ShortMonthShortYear = 'shortMonthShortYear',
  AddTab = 'addTab',
  MaskText = 'maskText',
  PhoneNumber = 'phoneNumber',
  Truncate = 'truncate',
  TruncateLessMore = 'truncateLessMore',
  Hour = 'hour',
  CustomDate = 'customDate'
}

export enum GroupTextFormatType {
  NameMatchCode = 'nameMatchCode',
  HeldStatementDestinationCode = 'heldStatementDestinationCode',
  PresentationInstrumentTypeCode = 'presentationInstrumentTypeCode',
  AuthorizationFilterCode = 'authorizationFilterCode',
  AuthorizationMethodCode = 'authorizationMethod',
  PointOfSaleEntryMode = 'pointOfSaleEntryMode',
  AutomatedTellerMachineAtmFlag = 'automatedTellerMachineAtmFlag',
  DualityFlag = 'dualityFlag',
  MailPhoneIndicator = 'mailPhoneIndicator',
  SourceTransactionIdentifier = 'sourceTransactionIdentifier',
  AuthorizationSourceCode = 'authorizationSourceCode',
  RiskIdentificationServiceIndicator = 'riskIdentificationServiceIndicator',
  TransactionCode = 'transactionCode',
  LastMonetary = 'lastMonetary'
}

export enum CodeErrorType {
  ValidationException = 453,
  ODSErrorException = 455,
  DuplicateRecord = 409
}
