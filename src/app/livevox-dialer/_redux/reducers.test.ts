import { LiveVoxState } from '../types';
import { actionsLiveVox, reducer } from './reducers';

const initialState: LiveVoxState = {
  sessionId: '',
  isAgentAuthenticated: false,
  lineNumber: '',
  accountNumber: '',
  call: {
      accountNumber: '',
      accountNumberRequired: false,
      callCenterId: -1,
      callRecordingStarted: false,
      callSessionId: '',
      callTransactionId: '',
      phoneNumber: '',
      serviceId: -1
  },
  inCallAccountNumber: '',
  lastInCallAccountNumber: ''
};

describe('LiveVoxDialer > reducer', () => {
  it('setLiveVoxDialerSessionId', () => {
    const sessionId = '123';
    const state = reducer(initialState, actionsLiveVox.setLiveVoxDialerSessionId(sessionId));
    expect(state.sessionId).toEqual('123');
  });

  it('setLiveVoxDialerAgentAuthenticated', () => {
    const isAgentAuthenticated = true;
    const state = reducer(initialState, actionsLiveVox.setLiveVoxDialerAgentAuthenticated(isAgentAuthenticated));
    expect(state.isAgentAuthenticated).toEqual(true);
  });

  it('setLiveVoxDialerLineNumber', () => {
    const lineNumber = 'ACD';
    const state = reducer(initialState, actionsLiveVox.setLiveVoxDialerLineNumber(lineNumber));
    expect(state.lineNumber).toEqual('ACD');
  });

  it('setLiveVoxDialerAccountNumber', () => {
    const accountNumber = '1234567890123456';
    const state = reducer(initialState, actionsLiveVox.setLiveVoxDialerAccountNumber(accountNumber));
    expect(state.accountNumber).toEqual('1234567890123456');
  });

  it('setLiveVoxDialerLineActiveAndReadyData', () => {
    const call = {
      accountNumber: '1234567890123456',
      accountNumberRequired: true,
      callCenterId: 123,
      callRecordingStarted: true,
      callSessionId: '999',
      callTransactionId: '888',
      phoneNumber: '1234567890',
      serviceId: 444
    };
    const state = reducer(initialState, actionsLiveVox.setLiveVoxDialerLineActiveAndReadyData(call));
    expect(state.call).toEqual({
      accountNumber: '1234567890123456',
      accountNumberRequired: true,
      callCenterId: 123,
      callRecordingStarted: true,
      callSessionId: '999',
      callTransactionId: '888',
      phoneNumber: '1234567890',
      serviceId: 444
    });
  });

  it('setLiveVoxDialerInCallAccountNumber', () => {
    const inCallAccountNumber = '1234567890123456';
    const state = reducer(initialState, actionsLiveVox.setLiveVoxDialerInCallAccountNumber(inCallAccountNumber));
    expect(state.inCallAccountNumber).toEqual('1234567890123456');
  });

  it('setLiveVoxDialerLastInCallAccountNumber', () => {
    const lastInCallAccountNumber = '1234567890123456';
    const state = reducer(initialState, actionsLiveVox.setLiveVoxDialerLastInCallAccountNumber(lastInCallAccountNumber));
    expect(state.lastInCallAccountNumber).toEqual('1234567890123456');
  });
});
