import { apiService } from 'app/utils/api.service';
import { AxiosRequestConfig } from 'axios';
import { LiveVoxCallResultArgs, SessionLoginData } from '../types';

const liveVoxService = {
    getLiveVoxDialerSessionId: (data: SessionLoginData, sessionLoginHeaders: AxiosRequestConfig)=>{
        const url = window.appConfig?.api?.liveVoxDialer.getSessionId || '';
        return apiService.post(url, data, sessionLoginHeaders);
    },

    getLiveVoxDialerAgentStatus: (data: {}, agentStatusHeaders: AxiosRequestConfig)=>{
        const url = window.appConfig?.api?.liveVoxDialer.getAgentStatus || '';
        return apiService.post(url, data, agentStatusHeaders);
    },

    getLiveVoxDialerScreenPop: (screenPopHeaders: AxiosRequestConfig)=>{
        const url = window.appConfig?.api?.liveVoxDialer.getScreenPopDetails || '';
        return apiService.get(url, screenPopHeaders);
    },

    getLiveVoxDialerTermCodes: (resultsCodeHeaders: AxiosRequestConfig)=>{
        const url = window.appConfig?.api?.liveVoxDialer.getTermCode || '';
        return apiService.get(url, resultsCodeHeaders);
    },

    saveLiveVoxDialerTermCode: (data: LiveVoxCallResultArgs, resultsCodeHeaders: AxiosRequestConfig)=>{
        const url = window.appConfig?.api?.liveVoxDialer.saveTermCode || '';
        return apiService.put(url, data, resultsCodeHeaders);
    }
}

export default liveVoxService;