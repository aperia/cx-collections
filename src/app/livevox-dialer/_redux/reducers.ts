import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
    liveVoxDialerTermCodesThunk,
    liveVoxDialerTermCodesThunkBuilder
} from './thunks/liveVoxDialerTermCodesThunk';

import {
    getLiveVoxDialerSessionIdThunk,
    getLiveVoxDialerSessionIdThunkBuilder 
} from './thunks/getLiveVoxDialerSessionIdThunk';

import {
    getLiveVoxDialerAgentStatusThunk,
    getLiveVoxDialerAgentStatusThunkBuilder
} from './thunks/getLiveVoxDialerAgentStatusThunk';

import {
    getLiveVoxDialerScreenPopThunk,
    getLiveVoxDialerScreenPopThunkBuilder
} from './thunks/getLiveVoxDialerScreenPopThunk';

import {
    Call,
    LiveVoxState
} from '../types';

export const initialState: LiveVoxState = {
    sessionId: '',
    isAgentAuthenticated: false,
    lineNumber: '',
    accountNumber: '',
    call: {
        accountNumber: '',
        accountNumberRequired: false,
        callCenterId: -1,
        callRecordingStarted: false,
        callSessionId: '',
        callTransactionId: '',
        phoneNumber: '',
        serviceId: -1
    },
    inCallAccountNumber: '',
    lastInCallAccountNumber: ''
};

const { actions, reducer } = createSlice({
    name: 'session',
    initialState,
    reducers:{
        setLiveVoxDialerSessionId:(draftState, action: PayloadAction<string>)=>{
            draftState.sessionId = action.payload;
        },
        setLiveVoxDialerAgentAuthenticated:(draftState, action: PayloadAction<boolean>)=>{
            draftState.isAgentAuthenticated = action.payload;
        },
        setLiveVoxDialerLineNumber:(draftState, action: PayloadAction<string>)=>{
            draftState.lineNumber = action.payload;
        },
        setLiveVoxDialerAccountNumber:(draftState, action: PayloadAction<string>)=>{
            draftState.accountNumber = action.payload;
        },
        setLiveVoxDialerLineActiveAndReadyData:(draftState, action: PayloadAction<Call>)=>{
            draftState.call = action.payload;
        },
        setLiveVoxDialerInCallAccountNumber:(draftState, action: PayloadAction<string>)=>{
            draftState.inCallAccountNumber = action.payload;
        },
        setLiveVoxDialerLastInCallAccountNumber:(draftState, action: PayloadAction<string>)=>{
            draftState.lastInCallAccountNumber = action.payload;
        }
    },
    extraReducers: builder => {
        getLiveVoxDialerSessionIdThunkBuilder(builder);
        getLiveVoxDialerAgentStatusThunkBuilder(builder);
        getLiveVoxDialerScreenPopThunkBuilder(builder);
        liveVoxDialerTermCodesThunkBuilder(builder);
    }
});
const allActions = {
    ...actions, liveVoxDialerTermCodesThunk, getLiveVoxDialerScreenPopThunk,
    getLiveVoxDialerAgentStatusThunk, getLiveVoxDialerSessionIdThunk};
export { allActions as actionsLiveVox, reducer};
