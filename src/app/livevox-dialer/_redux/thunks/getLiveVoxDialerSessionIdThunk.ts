import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { SessionLoginData, SessionLoginResponse } from 'app/livevox-dialer/types';
import { AxiosResponse } from 'axios';
import liveVoxService from '../api-services';
import { LV_ACCESS } from 'app/constants/liveVoxDialer';

export const getLiveVoxDialerSessionIdThunk = createAsyncThunk<any, SessionLoginData, ThunkAPIConfig>(
    'livevox/getLiveVoxDialerSessionId',
    async (args: SessionLoginData, thunkAPI) => {
      try {
        const response: AxiosResponse<SessionLoginResponse> = await liveVoxService.getLiveVoxDialerSessionId(
          {
            clientName: args.clientName,
            userName: args.userName,
            password: args.password,
            agent: args.agent
          },
          {
          headers: {
              'LV-Access': LV_ACCESS.TOKEN,
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            }
          }
        );
        return response.data;
      } catch (error: any) {
        return thunkAPI.rejectWithValue({errorMessage: error});
      }
    }
);

export const getLiveVoxDialerSessionIdThunkBuilder = (builder: ActionReducerMapBuilder<any>)=>{
    builder.addCase(getLiveVoxDialerSessionIdThunk.pending, (draftState, action)=>{});

    builder.addCase(getLiveVoxDialerSessionIdThunk.fulfilled, (draftState, action)=>{
      draftState.sessionId = action.payload.sessionId;
      draftState.isAgentAuthenticated = true;
    });

    builder.addCase(getLiveVoxDialerSessionIdThunk.rejected, (draftState, action)=>{
      draftState.sessionId = '';
      draftState.isAgentAuthenticated = false;
    });
}