import { SessionLoginResponse } from 'app/livevox-dialer/types';
import { responseDefault } from 'app/test-utils';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import liveVoxService from '../api-services';
import { getLiveVoxDialerSessionIdThunk } from './getLiveVoxDialerSessionIdThunk';

describe('getLiveVoxDialerSessionIdThunk.test', () => {
  const store = createStore(rootReducer, {}, applyMiddleware(thunk));

  it('should return get sessionId from LiveVox Dialer agent session', async () => {
    const getLiveVoxDialerSessionLoginResponse: SessionLoginResponse = {
      sessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
      userId: 1756363,
      clientId: 1094568,
      daysUntilPasswordExpires: 166
    };

    jest.spyOn(liveVoxService, 'getLiveVoxDialerSessionId').mockResolvedValue({
      ...responseDefault,
      data: getLiveVoxDialerSessionLoginResponse
    });

    const response = await getLiveVoxDialerSessionIdThunk({
      agent: true,
      clientName: 'fiserv_dev',
      password: process.env.REACT_APP_PWD!,
      userName: 'lvagent1'
    })(store.dispatch, store.getState, {});

    expect(response.payload.sessionId).toEqual(
      getLiveVoxDialerSessionLoginResponse.sessionId
    );
  });

  it('should not retrieve sessionId if incorrect agent creds', async () => {
    const getLiveVoxDialerSessionLoginResponse = {
      data: {
        errorMessage: 'error'
      }
    };

    jest.spyOn(liveVoxService, 'getLiveVoxDialerSessionId').mockRejectedValue({
      ...responseDefault,
      data: getLiveVoxDialerSessionLoginResponse
    });

    const response = await getLiveVoxDialerSessionIdThunk({
      agent: true,
      clientName: 'fiserv_dev',
      password: process.env.REACT_APP_BAD_PWD!,
      userName: 'bad-user'
    })(store.dispatch, store.getState, {});

    expect(response.payload.errorMessage.data.data.errorMessage).toEqual(
      getLiveVoxDialerSessionLoginResponse.data.errorMessage
    );
  });
});
