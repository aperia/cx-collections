import { ScreenPopDetailsResponse } from 'app/livevox-dialer/types';
import { responseDefault } from 'app/test-utils';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import liveVoxService from '../api-services';
import { getLiveVoxDialerScreenPopThunk } from './getLiveVoxDialerScreenPopThunk';

describe('getLiveVoxDialerScreenPopThunk.test', () => {
  const store = createStore(rootReducer, {}, applyMiddleware(thunk));

  it('should get screenpop details from LiveVox Dialer', async () => {
    const getLiveVoxDialerScreenPopDetailsResponse: ScreenPopDetailsResponse = {
      screenPopRow: [
        {
          key: 'CALL TYPE',
          value: 'Outbound'
        },
        {
          key: 'FIRST NAME',
          value: ''
        },
        {
          key: 'LAST NAME',
          value: ''
        },
        {
          key: 'ACCOUNT NUMBER',
          value: '5166480500018901'
        },
        {
          key: 'PHONE NUMBER',
          value: '1234567890'
        }
      ]
    
    }

    jest.spyOn(liveVoxService, 'getLiveVoxDialerScreenPop').mockResolvedValue({
      ...responseDefault,
      data: getLiveVoxDialerScreenPopDetailsResponse
    })

    const response = await getLiveVoxDialerScreenPopThunk({
      sessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
      lineNumber: 'ACD'
    })(store.dispatch, store.getState, {});

    expect(response.payload.screenPopRow).toEqual(getLiveVoxDialerScreenPopDetailsResponse.screenPopRow);
  });

  it('should not get screenpop details from LiveVox Dialer if agent is not connected to a client', async () => {
    const getLiveVoxDialerScreenPopDetailsResponse = {
      data: {
        errorMessage: 'error'
      }
    }

    jest.spyOn(liveVoxService, 'getLiveVoxDialerScreenPop').mockRejectedValue({
      ...responseDefault,
      data: getLiveVoxDialerScreenPopDetailsResponse
    })

    const response = await getLiveVoxDialerScreenPopThunk({
      sessionId: '27c84ce7-c479-4cc7-9710-2daf07334784',
      lineNumber: ''
    })(store.dispatch, store.getState, {});

    expect(response.payload.errorMessage.data.data.errorMessage).toEqual(getLiveVoxDialerScreenPopDetailsResponse.data.errorMessage);
  });
});
