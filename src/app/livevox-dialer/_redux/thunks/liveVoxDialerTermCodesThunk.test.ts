import { LiveVoxTermCodesResponse } from 'app/livevox-dialer/types';
import { responseDefault } from 'app/test-utils';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import liveVoxService from '../api-services';
import { liveVoxDialerTermCodesThunk } from './liveVoxDialerTermCodesThunk';

describe('liveVoxDialerTermCodesThunk.test', () => {
  const store = createStore(rootReducer, {}, applyMiddleware(thunk));

  it('should get term codes from LiveVox Dialer in order to save the matching code selected on Call Results', async () => {
    const getLiveVoxDialerTermCodesResponse: LiveVoxTermCodesResponse = {
      termCodeCategories: [
        {
          name: '3PC_OTHER',
          termCodes: [
            {
              actionType: 'WPC',
              id: '102478488',
              lvResult: 'AGENT - Wrong Number',
              lvResultId: "735",
              name: "Wrong Number",
              paymentAmtRequired: false,
              previewDialEnabled: false,
              reportDisplayOrder: 0
            }
          ]
        }
      ]
    }

    jest.spyOn(liveVoxService, 'getLiveVoxDialerTermCodes').mockResolvedValue({
      ...responseDefault,
      data: getLiveVoxDialerTermCodesResponse
    })

    jest.spyOn(liveVoxService, 'saveLiveVoxDialerTermCode').mockResolvedValue({
      ...responseDefault,
      data: {}
    })

    await liveVoxDialerTermCodesThunk({
      liveVoxDialerSessionId: 'fc8848c1-e27a-433a-9adc-0475f3ab0a54',
      liveVoxDialerLineNumber: 'ACD',
      selectedCallResult: {
        description: 'AGENT - Wrong Number',
        code: '735'
      },
      termCodeId: '102478488',
      serviceId: 1094568,
      callTransactionId: '126516822091',
      callSessionId: 'UC219DT617C1B69@10.101.19.154',
      account: '1234567890123456',
      phoneDialed: '1234567890',
      moveAgentToNotReady: false
    })(store.dispatch, store.getState, {});

    expect(liveVoxService.saveLiveVoxDialerTermCode).toBeCalled();
  });

  it('should not save term codes in LiveVox Dialer if no matching code found on Call Results', async () => {
    const getLiveVoxDialerTermCodesResponse: LiveVoxTermCodesResponse = {
      termCodeCategories: [
        {
          name: '3PC_OTHER',
          termCodes: [
            {
              actionType: 'WWW',
              id: '102478488',
              lvResult: 'AGENT - No Matching Code',
              lvResultId: "735",
              name: "No Matching Code",
              paymentAmtRequired: false,
              previewDialEnabled: false,
              reportDisplayOrder: 0
            }
          ]
        }
      ]
    }

    jest.spyOn(liveVoxService, 'getLiveVoxDialerTermCodes').mockResolvedValue({
      ...responseDefault,
      data: getLiveVoxDialerTermCodesResponse
    })

    jest.spyOn(liveVoxService, 'saveLiveVoxDialerTermCode').mockResolvedValue({
      ...responseDefault,
      data: {}
    })

    await liveVoxDialerTermCodesThunk({
      liveVoxDialerSessionId: 'fc8848c1-e27a-433a-9adc-0475f3ab0a54',
      liveVoxDialerLineNumber: 'ACD',
      selectedCallResult: {
        description: 'AGENT - Wrong Number',
        code: '735'
      },
      termCodeId: '1',
      serviceId: 1094568,
      callTransactionId: '126516822091',
      callSessionId: 'UC219DT617C1B69@10.101.19.154',
      account: '1234567890123456',
      phoneDialed: '1234567890',
      moveAgentToNotReady: false
    })(store.dispatch, store.getState, {});

    expect(liveVoxService.saveLiveVoxDialerTermCode).not.toBeCalled();

  });

  it('should fail no matching code found on Call Results', async () => {
    const getLiveVoxDialerTermCodesResponse = {
      data: {
        errorMessage: 'error message'
      }
    }

    jest.spyOn(liveVoxService, 'getLiveVoxDialerTermCodes').mockResolvedValue({
      ...responseDefault,
      data: getLiveVoxDialerTermCodesResponse
    })

    jest.spyOn(liveVoxService, 'saveLiveVoxDialerTermCode').mockResolvedValue({
      ...responseDefault,
      data: {}
    })

    await liveVoxDialerTermCodesThunk({
      liveVoxDialerSessionId: '',
      liveVoxDialerLineNumber: 'ACD',
      selectedCallResult: {
        description: 'AGENT - Wrong Number',
        code: '735'
      },
      termCodeId: '1',
      serviceId: 1094568,
      callTransactionId: '126516822091',
      callSessionId: 'UC219DT617C1B69@10.101.19.154',
      account: '1234567890123456',
      phoneDialed: '1234567890',
      moveAgentToNotReady: false
    })(store.dispatch, store.getState, {});

    expect(liveVoxService.saveLiveVoxDialerTermCode).not.toBeCalled();

  });
});
