import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { AGENT_STATUS } from 'app/constants/liveVoxDialer';
import { AgentStatusArgs, AgentStatusResponse } from 'app/livevox-dialer/types';
import { isEmpty } from 'app/_libraries/_dls/lodash';
import { AxiosResponse } from 'axios';
import liveVoxService from '../api-services';

export const getLiveVoxDialerAgentStatusThunk = createAsyncThunk<any, AgentStatusArgs, ThunkAPIConfig>(
    'livevox/getLiveVoxDialerAgentStatus',
    async (args: AgentStatusArgs, thunkAPI) => {
      try {
        const response: AxiosResponse<AgentStatusResponse> = await liveVoxService.getLiveVoxDialerAgentStatus(
          {}, {
            headers: {
              'LV-Session': args.sessionId,
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            }
          }
        );
        return response.data;

      } catch (error: any) {
        return thunkAPI.rejectWithValue({errorMessage: error});
      }
    }
);

export const getLiveVoxDialerAgentStatusThunkBuilder = (builder: ActionReducerMapBuilder<any>)=>{
    builder.addCase(getLiveVoxDialerAgentStatusThunk.pending, (draftState, action)=>{});
    builder.addCase(getLiveVoxDialerAgentStatusThunk.fulfilled, (draftState, action)=>{
      const agentStatusResponse: AgentStatusResponse = action.payload;
      const lineActiveAndReady = agentStatusResponse.agentStatus[0].lines
        .filter(item => item.active && item.state === AGENT_STATUS.INCALL)[0];
      const lineNumberFromResp = lineActiveAndReady ? lineActiveAndReady.lineNumber : '';

      draftState.lineNumber = lineNumberFromResp;
      draftState.call = lineActiveAndReady?.call;

      if (isEmpty(lineNumberFromResp)) {
        draftState.lastInCallAccountNumber = '';
        draftState.accountNumber = '';
      }
    });
    builder.addCase(getLiveVoxDialerAgentStatusThunk.rejected, (draftState, action)=>{});
}