import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { LiveVoxCallResultArgs, LiveVoxTermCodesResponse } from 'app/livevox-dialer/types';
import { AxiosResponse } from 'axios';
import liveVoxService from '../api-services';

export const liveVoxDialerTermCodesThunk = createAsyncThunk<any, LiveVoxCallResultArgs, ThunkAPIConfig>(
    'livevox/getLiveVoxDialerTermCodes',
    async (args: LiveVoxCallResultArgs, thunkAPI) => {
      try {
        const response: AxiosResponse<LiveVoxTermCodesResponse> = await liveVoxService.getLiveVoxDialerTermCodes({
          headers: {
            'LV-Session': args.liveVoxDialerSessionId,
            'Content-Type': 'application/json',
            'Accept': 'application/json'
          },
          params: {
            lineNumber: args.serviceId
          }
        });
        
        const matchingTermCode = response.data.termCodeCategories[0].termCodes
          .find(item => item.id === args.termCodeId);

        if (matchingTermCode) {
          const saveResponse: AxiosResponse<any> = await liveVoxService.saveLiveVoxDialerTermCode(
            {
              serviceId: args.serviceId,
              callTransactionId: args.callTransactionId,
              callSessionId: args.callSessionId,
              account: args.account,
              termCodeId: matchingTermCode.id,
              phoneDialed: args.phoneDialed,
              moveAgentToNotReady: args.moveAgentToNotReady,
            },
            {
            headers: {
              'LV-Session': args.liveVoxDialerSessionId,
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
            params: {
              lineNumber: args.liveVoxDialerLineNumber
            }
          });

          const data = saveResponse.data;
          return data;
        } else {
          return thunkAPI.rejectWithValue({errorMessage: 'No matching term code found'});
        }

      } catch (error: any) {
        return thunkAPI.rejectWithValue({errorMessage: error});
      }
    }
);

export const liveVoxDialerTermCodesThunkBuilder = (builder: ActionReducerMapBuilder<any>)=>{
    builder.addCase(liveVoxDialerTermCodesThunk.pending, draftState=>{});
    builder.addCase(liveVoxDialerTermCodesThunk.fulfilled, draftState=>{});
    builder.addCase(liveVoxDialerTermCodesThunk.rejected, draftState=>{});
}