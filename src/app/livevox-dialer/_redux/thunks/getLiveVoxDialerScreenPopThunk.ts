import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { ScreenPopArgs, ScreenPopDetailsResponse, ScreenPopRow } from 'app/livevox-dialer/types';
import { AxiosResponse } from 'axios';
import liveVoxService from '../api-services';

export const getLiveVoxDialerScreenPopThunk = createAsyncThunk<any, ScreenPopArgs, ThunkAPIConfig>(
    'livevox/getLiveVoxScreenPopStatus',
    async (args: ScreenPopArgs, thunkAPI) => {
      try {
        const response: AxiosResponse<ScreenPopDetailsResponse> = await liveVoxService.getLiveVoxDialerScreenPop(
          {
            headers: {
              'LV-Session': args.sessionId,
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
            params: {
              'lineNumber': args.lineNumber
            }
          });
        return response.data;

      } catch (error: any) {
        return thunkAPI.rejectWithValue({errorMessage: error});
      }
    }
);

export const getLiveVoxDialerScreenPopThunkBuilder = (builder: ActionReducerMapBuilder<any>)=>{
    builder.addCase(getLiveVoxDialerScreenPopThunk.pending, (draftState, action)=>{});

    builder.addCase(getLiveVoxDialerScreenPopThunk.fulfilled, (draftState, action)=>{
      const screenPopDetailsResponse: ScreenPopDetailsResponse = action.payload
      const { screenPopRow }: {screenPopRow: ScreenPopRow[]} = screenPopDetailsResponse;
      draftState.inCallAccountNumber = screenPopRow.filter(item => item.key === 'ACCOUNT NUMBER')[0].value ;
      
    });

    builder.addCase(getLiveVoxDialerScreenPopThunk.rejected, (draftState, action)=>{
      draftState.inCallAccountNumber = ''
    });
}