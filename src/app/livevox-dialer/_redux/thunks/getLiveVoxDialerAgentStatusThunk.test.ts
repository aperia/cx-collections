import { AgentStatusResponse } from 'app/livevox-dialer/types';
import { responseDefault } from 'app/test-utils';
import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import { rootReducer } from 'storeConfig';
import liveVoxService from '../api-services';
import { getLiveVoxDialerAgentStatusThunk } from './getLiveVoxDialerAgentStatusThunk';

describe('getLiveVoxDialerAgentStatusThunk.test', () => {
  const store = createStore(rootReducer, {}, applyMiddleware(thunk));

  it('should get agent status from LiveVox Dialer', async () => {
    const getLiveVoxDialerAgentStatusResponse: AgentStatusResponse = {
      agentStatus: [
        {
          service: {
            callsInProgress: 1,
            callsRemaining: 0,
            callsOnHold: 0
          },
          voicemail: {},
          chat: {},
          lines: [
            {
              call: {
                callTransactionId: '126650403531',
                callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
                callRecordingStarted: true,
                serviceId: 1094568,
                callCenterId: 1028617,
                accountNumber: '5166480500018901',
                phoneNumber: '3059289674',
                accountNumberRequired: false
              },
              lineNumber: 'ACD',
              displayName: 'Voice',
              active: true,
              callControl: true,
              state: 'INCALL',
              stateChangedTime: 1635950570358
            },
            {
              lineNumber: 'DIRECT',
              displayName: 'Direct',
              active: false,
              callControl: false,
              state: 'READY',
              stateChangedTime: 1635950473368
            }
          ],
          agentCallConnected: true
        }
      ]
    
    
    }

    jest.spyOn(liveVoxService, 'getLiveVoxDialerAgentStatus').mockResolvedValue({
      ...responseDefault,
      data: getLiveVoxDialerAgentStatusResponse
    })

    const response = await getLiveVoxDialerAgentStatusThunk({
        sessionId: 'fc8848c1-e27a-433a-9adc-0475f3ab0a54'
      }
    )(store.dispatch, store.getState, {});

    expect(response.payload.agentStatus).toEqual(getLiveVoxDialerAgentStatusResponse.agentStatus);
  });

  it('should not set lineNumber if agent status is not INCALL', async () => {
    const getLiveVoxDialerAgentStatusResponse: AgentStatusResponse = {
      agentStatus: [
        {
          service: {
            callsInProgress: 1,
            callsRemaining: 0,
            callsOnHold: 0
          },
          voicemail: {},
          chat: {},
          lines: [
            {
              call: {
                callTransactionId: '126650403531',
                callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
                callRecordingStarted: true,
                serviceId: 1094568,
                callCenterId: 1028617,
                accountNumber: '5166480500018901',
                phoneNumber: '3059289674',
                accountNumberRequired: false
              },
              lineNumber: '',
              displayName: 'Voice',
              active: true,
              callControl: true,
              state: 'READY',
              stateChangedTime: 1635950570358
            }
          ],
          agentCallConnected: true
        }
      ]
    
    
    }

    jest.spyOn(liveVoxService, 'getLiveVoxDialerAgentStatus').mockResolvedValue({
      ...responseDefault,
      data: getLiveVoxDialerAgentStatusResponse
    })

    const response = await getLiveVoxDialerAgentStatusThunk({
        sessionId: 'fc8848c1-e27a-433a-9adc-0475f3ab0a54'
      }
    )(store.dispatch, store.getState, {});

    expect(response.payload.agentStatus[0].lines[0].lineNumber).toEqual(getLiveVoxDialerAgentStatusResponse.agentStatus[0].lines[0].lineNumber);
  });

  it('should not get agent status from LiveVox Dialer if no sessionId', async () => {
    const getLiveVoxDialerAgentStatusResponse = {
      data: {
        errorMessage: 'error'
      }
    }

    jest.spyOn(liveVoxService, 'getLiveVoxDialerAgentStatus').mockRejectedValue({
      ...responseDefault,
      data: getLiveVoxDialerAgentStatusResponse
    })

    const response = await getLiveVoxDialerAgentStatusThunk({
      sessionId: ''
    })(store.dispatch, store.getState, {});

    expect(response.payload.errorMessage.data.data.errorMessage).toEqual(getLiveVoxDialerAgentStatusResponse.data.errorMessage);
  });
});
