
import { createSelector } from 'reselect';
import { LiveVoxState } from '../types';

export const getLiveVoxDialerData = (state: RootState) => state.liveVoxReducer;

export const getLiveVoxDialerSessionId = createSelector(
    getLiveVoxDialerData,
    (data: LiveVoxState) => data.sessionId
  );
  
export const getLiveVoxDialerLineNumber = createSelector(
    getLiveVoxDialerData,
    (data: LiveVoxState) => data.lineNumber
);
  
export const isLiveVoxDialerAgentAuthenticated = createSelector(
    getLiveVoxDialerData,
    (data: LiveVoxState) => data.isAgentAuthenticated
);
  
export const getLiveVoxDialerAccountNumber = createSelector(
    getLiveVoxDialerData,
    (data: LiveVoxState) => data.accountNumber
);

export const getLiveVoxDialerLineActiveAndReadyData = createSelector(
    getLiveVoxDialerData,
    (data: LiveVoxState) => data.call
);
  
export const getLiveVoxDialerInCallAccountNumber = createSelector(
    getLiveVoxDialerData,
    (data: LiveVoxState) => data.inCallAccountNumber
);
  
export const getLiveVoxDialerLastInCallAccountNumber = createSelector(
    getLiveVoxDialerData,
    (data: LiveVoxState) => data.lastInCallAccountNumber
);
  