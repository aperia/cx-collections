import liveVoxService from './api-services';

// utils
import { mockApiServices } from 'app/test-utils';
import { mockAppConfigApi, apiUrl } from 'app/test-utils/mocks/mockProperty';
import { AxiosRequestConfig } from 'axios';
import { SessionLoginData } from '../types';
import { LV_ACCESS } from 'app/constants/liveVoxDialer';

describe('liveVoxService', () => {
  describe('getLiveVoxDialerSessionId', () => {
    const sessionLoginData: SessionLoginData = {
      agent: true,
      clientName: 'fiserv_dev',
      password: process.env.REACT_APP_PWD!,
      userName: 'lvagent1'
    };

    const sessionLoginHeaders: AxiosRequestConfig = {
      headers: {
        'LV-Access': LV_ACCESS.TOKEN,
        'Content-Type': 'application/json',
        Accept: 'application/json'
      }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      liveVoxService.getLiveVoxDialerSessionId(
        sessionLoginData,
        sessionLoginHeaders
      );

      expect(mockService).toBeCalledWith(
        '',
        sessionLoginData,
        sessionLoginHeaders
      );
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      liveVoxService.getLiveVoxDialerSessionId(
        sessionLoginData,
        sessionLoginHeaders
      );

      expect(mockService).toBeCalledWith(
        apiUrl.liveVoxDialer.getSessionId,
        sessionLoginData,
        sessionLoginHeaders
      );
    });
  });

  describe('getLiveVoxDialerAgentStatus', () => {
    const agentStatusHeaders: AxiosRequestConfig = {
      headers: {
        'LV-Session': '123',
        'Content-Type': 'application/json',
        Accept: 'application/json'
      }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.post();

      liveVoxService.getLiveVoxDialerAgentStatus({}, agentStatusHeaders);

      expect(mockService).toBeCalledWith('', {}, agentStatusHeaders);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.post();

      liveVoxService.getLiveVoxDialerAgentStatus({}, agentStatusHeaders);

      expect(mockService).toBeCalledWith(
        apiUrl.liveVoxDialer.getAgentStatus,
        {},
        agentStatusHeaders
      );
    });
  });

  describe('getLiveVoxDialerScreenPop', () => {
    const screenPopHeaders: AxiosRequestConfig = {
      headers: {
        'LV-Session': '123',
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      params: {
        lineNumber: 'ACD'
      }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      liveVoxService.getLiveVoxDialerScreenPop(screenPopHeaders);

      expect(mockService).toBeCalledWith('', screenPopHeaders);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      liveVoxService.getLiveVoxDialerScreenPop(screenPopHeaders);

      expect(mockService).toBeCalledWith(
        apiUrl.liveVoxDialer.getScreenPopDetails,
        screenPopHeaders
      );
    });
  });

  describe('getLiveVoxDialerTermCodes', () => {
    const termCodesHeaders: AxiosRequestConfig = {
      headers: {
        'LV-Session': '123',
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      params: {
        lineNumber: 'ACD'
      }
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.get();

      liveVoxService.getLiveVoxDialerTermCodes(termCodesHeaders);

      expect(mockService).toBeCalledWith('', termCodesHeaders);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.get();

      liveVoxService.getLiveVoxDialerTermCodes(termCodesHeaders);

      expect(mockService).toBeCalledWith(
        apiUrl.liveVoxDialer.getTermCode,
        termCodesHeaders
      );
    });
  });

  describe('getLiveVoxDialerTermCodes', () => {
    const saveTermCodeHeaders: AxiosRequestConfig = {
      headers: {
        'LV-Session': '123',
        'Content-Type': 'application/json',
        Accept: 'application/json'
      },
      params: {
        lineNumber: 'ACD'
      }
    };

    const data = {
      termCodeId: '735',
      serviceId: 1094568,
      callTransactionId: '126516822091',
      callSessionId: 'UC219DT617C1B69@10.101.19.154',
      account: '1234567890123456',
      phoneDialed: '1234567890',
      moveAgentToNotReady: false
    };

    it('when url was not defined', () => {
      mockAppConfigApi.clear();

      const mockService = mockApiServices.put();

      liveVoxService.saveLiveVoxDialerTermCode(data, saveTermCodeHeaders);

      liveVoxService.saveLiveVoxDialerTermCode(data, saveTermCodeHeaders);
      expect(mockService).toBeCalledWith('', data, saveTermCodeHeaders);
    });

    it('when url was defined', () => {
      mockAppConfigApi.setup();

      const mockService = mockApiServices.put();

      liveVoxService.saveLiveVoxDialerTermCode(data, saveTermCodeHeaders);

      expect(mockService).toBeCalledWith(
        apiUrl.liveVoxDialer.saveTermCode,
        data,
        saveTermCodeHeaders
      );
    });
  });
});
