import { selectorWrapper } from 'app/test-utils';
import * as selector from './selectors';

const states: Partial<RootState> = {
  liveVoxReducer: {
    sessionId: 'fc8848c1-e27a-433a-9adc-0475f3ab0a54',
    isAgentAuthenticated: true,
    lineNumber: 'ACD',
    accountNumber: '5166480500018901',
    call: {
      accountNumber: '5166480500018901',
      accountNumberRequired: true,
      callCenterId: 75,
      callRecordingStarted: true,
      callSessionId: 'U6C4A0T61829FE7@10.101.21.167',
      callTransactionId: '126650403531',
      phoneNumber: '1234567890',
      serviceId: 1094568
    },
    inCallAccountNumber: '5166480500018901',
    lastInCallAccountNumber: '5166480500018901'
  }
};

const selectorTrigger = selectorWrapper(states);

describe('LiveVoxDialer selector', () => {
  it('getLiveVoxDialerSessionId', () => {
    const { data, emptyData } = selectorTrigger(selector.getLiveVoxDialerSessionId);
    expect(data).toEqual(states.liveVoxReducer?.sessionId);
    expect(emptyData).toEqual('');
  });

  it('getLiveVoxDialerLineNumber', () => {
    const { data, emptyData } = selectorTrigger(selector.getLiveVoxDialerLineNumber);
    expect(data).toEqual(states.liveVoxReducer?.lineNumber);
    expect(emptyData).toEqual('');
  });

  it('isLiveVoxDialerAgentAuthenticated', () => {
    const { data, emptyData } = selectorTrigger(selector.isLiveVoxDialerAgentAuthenticated);
    expect(data).toEqual(states.liveVoxReducer?.isAgentAuthenticated);
    expect(emptyData).toEqual(false);
  });

  it('getLiveVoxDialerAccountNumber', () => {
    const { data, emptyData } = selectorTrigger(selector.getLiveVoxDialerAccountNumber);
    expect(data).toEqual(states.liveVoxReducer?.accountNumber);
    expect(emptyData).toEqual('');
  });

  it('getLiveVoxDialerLineActiveAndReadyData', () => {
    const { data, emptyData } = selectorTrigger(selector.getLiveVoxDialerLineActiveAndReadyData);
    expect(data).toEqual(states.liveVoxReducer?.call);
    expect(emptyData).toEqual({
        accountNumber: '',
        accountNumberRequired: false,
        callCenterId: -1,
        callRecordingStarted: false,
        callSessionId: '',
        callTransactionId: '',
        phoneNumber: '',
        serviceId: -1
    });
  });

  it('getLiveVoxDialerInCallAccountNumber', () => {
    const { data, emptyData } = selectorTrigger(selector.getLiveVoxDialerInCallAccountNumber);
    expect(data).toEqual(states.liveVoxReducer?.inCallAccountNumber);
    expect(emptyData).toEqual('');
  });

  it('getLiveVoxDialerLastInCallAccountNumber', () => {
    const { data, emptyData } = selectorTrigger(selector.getLiveVoxDialerLastInCallAccountNumber);
    expect(data).toEqual(states.liveVoxReducer?.lastInCallAccountNumber);
    expect(emptyData).toEqual('');
  });
});
