import { CallResult } from "pages/CollectionForm/types";

export interface SessionLoginResponse {
  sessionId: string,
  userId: number,
  clientId: number,
  daysUntilPasswordExpires: number
}

export interface SessionLoginData {
  clientName: string,
  userName: string,
  password: string,
  agent: boolean
}
  
export interface Lines {
  lineNumber: string,
  displayName: string,
  active: Boolean,
  callControl: Boolean,
  state: string,
  stateChangedTime: number,
  call?: Call
}

export interface Call {
  accountNumber: string,
  accountNumberRequired: boolean,
  callCenterId: number,
  callRecordingStarted: boolean,
  callSessionId: string,
  callTransactionId: string,
  phoneNumber: string,
  serviceId: number
}

export interface Service {
  callsInProgress: number,
  callsRemaining: number,
  callsOnHold: number
}

export interface AgentStatusArgs {
  sessionId: string
}

export interface ScreenPopArgs {
  sessionId: string,
  lineNumber: string
}

export interface AgentStatusResponseElement {
  service: Service,
  voicemail: {},
  chat: {},
  lines: Lines[],
  agentCallConnected: Boolean
}

export interface AgentStatusResponse {
  agentStatus: AgentStatusResponseElement[]
}

export interface LiveVoxState {
  sessionId: string,
  isAgentAuthenticated: boolean,
  lineNumber: string,
  accountNumber: string,
  call?: Call,
  inCallAccountNumber: string,
  lastInCallAccountNumber: string
}

export interface ScreenPopDetailsResponse {
  screenPopRow: ScreenPopRow[]
}

export interface ScreenPopRow {
  key: string,
  value: string
}

export interface LiveVoxCallResultArgs {
  liveVoxDialerSessionId?: string,
  liveVoxDialerLineNumber?: string,
  selectedCallResult?: CallResult,
  termCodeId?: string,
  serviceId: number,
  callTransactionId: string,
  callSessionId: string,
  account: string,
  phoneDialed: string,
  moveAgentToNotReady: boolean
}

export interface LiveVoxTermCodesResponse {
  termCodeCategories: TermCodeCategory[]
}

export interface TermCodeCategory {
  name: string,
  termCodes: TermCodes[]
}

export interface TermCodes {
  id: string,
  name: string,
  lvResultId: string,
  lvResult: string,
  reportDisplayOrder: number,
  previewDialEnabled: boolean,
  actionType: string,
  paymentAmtRequired: boolean,
}
