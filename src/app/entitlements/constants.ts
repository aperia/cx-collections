/**
 * Use mapping for API
 */

export enum COMPONENTS_MAP {
  ACCOUNT_SEARCH = 'account-search',
  ACCOUNT_INFORMATION = 'account-information',
  ACCOUNT_SNAPSHOT = 'account-snapshot',
  WEBSITE_LAUNCHER = 'website-launcher',
  CARDHOLDER_MAINTENANCE = 'cardholder-maintenance',
  CHANGE_HISTORY = 'change-history',
  CLIENT_CONTACT_INFO = 'client-contact-information',
  CLIENT_CONFIGURATION = 'client-configuration',
  COLLECTIONS_ACTIVITIES = 'collection-activities',
  COLLECTION_INFORMATION = 'collection-information',
  CONTACT_ACCOUNT_ENTITLEMENT = 'contact-account-entitlement',
  CUSTOMER_AUTHENTICATION = 'customer-authentication',
  DASHBOARD = 'dashboard',
  LAST_25_ACTIONS = 'last-actions',
  LETTER = 'letter',
  MAKE_PAYMENT = 'make-payment',
  MEMO = 'memo',
  MONETARY_ADJUSTMENT = 'monetary-adjustment',
  OVERVIEW_HISTORICAL_INFORMATION = 'overview-historical-information',
  OVERVIEW_MONETARY_INFORMATION = 'overview-monetary-information',
  PAYMENT_HISTORY = 'payment-history',
  PAYMENT_LIST = 'payment-list',
  PENDING_AUTHORIZATION = 'pending-authorization',
  PUSH_QUEUE_LIST = 'push-queue-list',
  RELATED_ACCOUNT = 'related-accounts',
  REPORTING = 'reporting',
  STATEMENT = 'statement',
  STATEMENT_HISTORY = 'statement-history',
  TALKING_POINTS = 'talking-points',
  USER_ROLE_ENTITLEMENT = 'user-role-entitlement'
}

/**
 * Use mapping for application
 */

export enum ENTITLEMENT_COMPONENTS {
  ACCOUNT_SEARCH = 'AccountSearch',
  ACCOUNT_INFORMATION = 'AccountInformation',
  ACCOUNT_SNAPSHOT = 'AccountSnapshot',
  WEBSITE_LAUNCHER = 'WebsiteLauncher',
  CARDHOLDER_MAINTENANCE = 'CardholderMaintenance',
  CHANGE_HISTORY = 'ChangeHistory',
  CLIENT_CONTACT_INFO = 'ClientContactInformation',
  CLIENT_CONFIGURATION = 'ClientConfiguration',
  COLLECTIONS_ACTIVITIES = 'CollectionActivities',
  COLLECTION_INFORMATION = 'CollectionInformation',
  CONTACT_ACCOUNT_ENTITLEMENT = 'ContactAccountEntitlement',
  CUSTOMER_AUTHENTICATION = 'CustomerAuthentication',
  DASHBOARD = 'Dashboard',
  LAST_25_ACTIONS = 'LastActions',
  LETTER = 'Letter',
  MAKE_PAYMENT = 'MakePayment',
  MEMO = 'Memo',
  MONETARY_ADJUSTMENT = 'MonetaryAdjustment',
  OVERVIEW_HISTORICAL_INFORMATION = 'OverviewHistoricalInformation',
  OVERVIEW_MONETARY_INFORMATION = 'OverviewMonetaryInformation',
  PAYMENT_HISTORY = 'PaymentHistory',
  PAYMENT_LIST = 'PaymentList',
  PENDING_AUTHORIZATION = 'PendingAuthorization',
  PUSH_QUEUE_LIST = 'PushQueueList',
  RELATED_ACCOUNT = 'RelatedAccounts',
  REPORTING = 'Reporting',
  STATEMENT = 'Statement',
  STATEMENT_HISTORY = 'StatementHistory',
  TALKING_POINTS = 'TalkingPoints',
  USER_ROLE_ENTITLEMENT = 'UserRoleEntitlement'
}

export enum PERMISSIONS {
  ACCOUNT_SEARCH_VIEW = 'AccountSearch.View',

  ACCOUNT_INFORMATION_VIEW = 'AccountInformation.View',

  ACCOUNT_SNAPSHOT_VIEW = 'AccountSnapshot.View',
  WEBSITE_LAUNCHER_VIEW = 'WebsiteLauncher.View',
  ACCOUNT_SNAPSHOT_EDIT_EXTERNAL_CODE = 'AccountSnapshot.EditExternalStatusCode',
  ACCOUNT_SNAPSHOT_EDIT_REASON_CODE = 'AccountSnapshot.EditExternalStatusReasonCode',

  CARDHOLDER_MAINTENANCE_VIEW = 'CardholderMaintenance.View',
  CARDHOLDER_MAINTENANCE_VIEW_ADDRESS = 'CardholderMaintenance.ViewAddress',
  CARDHOLDER_MAINTENANCE_ADD_ADDRESS = 'CardholderMaintenance.AddAddress',
  CARDHOLDER_MAINTENANCE_EDIT_ADDRESS = 'CardholderMaintenance.EditAddress',
  CARDHOLDER_MAINTENANCE_VIEW_ADDRESS_HISTORY = 'CardholderMaintenance.ViewAddressHistory',
  CARDHOLDER_MAINTENANCE_DELETE_ADDRESS = 'CardholderMaintenance.DeleteAddress',
  CARDHOLDER_MAINTENANCE_VIEW_PHONE = 'CardholderMaintenance.ViewPhone',
  CARDHOLDER_MAINTENANCE_EDIT_PHONE = 'CardholderMaintenance.EditPhone',
  CARDHOLDER_MAINTENANCE_VIEW_EMAIL = 'CardholderMaintenance.ViewEmail',
  CARDHOLDER_MAINTENANCE_EDIT_EMAIL = 'CardholderMaintenance.EditEmail',

  CHANGE_HISTORY_VIEW = 'ChangeHistory.View',

  CLIENT_CONFIGURATION_VIEW = 'ClientConfiguration.View',
  CLIENT_CONFIGURATION_ADD_CSPA = 'ClientConfiguration.AddCSPA',
  CLIENT_CONFIGURATION_EDIT_CSPA = 'ClientConfiguration.EditCSPA',
  CLIENT_CONFIGURATION_DELETE_CSPA = 'ClientConfiguration.DeleteCSPA',
  CLIENT_CONFIGURATION_ACTIVE_DEACTIVATE = 'ClientConfiguration.ActivateDeactivate',
  CLIENT_CONFIGURATION_VIEW_SETTINGS = 'ClientConfiguration.ViewSettings',
  CLIENT_CONFIGURATION_CHANGE_SETTINGS = 'ClientConfiguration.ChangeSettings',

  COLLECTION_ACTIVITIES_VIEW = 'CollectionActivities.View',
  COLLECTION_ACTIVITIES_UPDATE_NEXT_WORK_DATE = 'CollectionActivities.UpdateNextWorkDate',
  COLLECTION_ACTIVITIES_UPDATE_MOVE_TO_QUEUE = 'CollectionActivities.UpdateMoveToQueue',
  COLLECTION_ACTIVITIES_UPDATE_DELINQUENCY_REASON = 'CollectionActivities.UpdateDelinquencyReason',

  COLLECTION_CONTACT_INFO_VIEW = 'ClientContactInformation.View',

  COLLECTION_INFORMATION_VIEW = 'CollectionInformation.View',

  CONTACT_ACCOUNT_ENTITLEMENT_VIEW = 'ContactAccountEntitlement.View',
  CONTACT_ACCOUNT_ENTITLEMENT_EDIT = 'ContactAccountEntitlement.Edit',

  CUSTOMER_AUTHENTICATION_VIEW = 'CustomerAuthentication.View',
  CUSTOMER_AUTHENTICATION_BYPASS = 'CustomerAuthentication.BypassAuthentication',

  DASHBOARD_VIEW = 'Dashboard.View',

  LAST_25_ACTIONS_VIEW = 'LastActions.View',

  LETTER_VIEW_LETTER_LIST = 'Letter.ViewLetterList',
  LETTER_SEND_LETTER = 'Letter.SendLetter',
  LETTER_VIEW_PENDING_LIST = 'Letter.ViewPendingList',
  LETTER_DELETE_PENDING_LETTER = 'Letter.DeletePendingLetter',

  MAKE_PAYMENT_ADD_BANK_REGISTRATION = 'MakePayment.AddBankRegistration',
  MAKE_PAYMENT_DELETE_BANK_REGISTRATION = 'MakePayment.DeleteBankRegistration',
  MAKE_PAYMENT_SCHEDULE_PAYMENT = 'MakePayment.SchedulePayment',
  MAKE_PAYMENT_UPDATE_CHECKING_ACCOUNT = 'MakePayment.UpdateCheckingAccount',
  MAKE_PAYMENT_UPDATE_SAVINGS_ACCOUNT = 'MakePayment.UpdateSavingsAccount',
  MAKE_PAYMENT_MAKE_ON_DEMAND_ACH = 'MakePayment.MakeOnDemandACH',

  VIEW_MEMO = 'Memo.ViewMemo',
  ADD_MEMO = 'Memo.AddMemo',
  EDIT_MEMO = 'Memo.EditMemo',
  DELETE_MEMO = 'Memo.DeleteMemo',

  MONETARY_ADJUSTMENT_ADJUST_TRANSACTION = 'MonetaryAdjustment.AdjustTransaction',
  MONETARY_ADJUSTMENT_VIEW_ADJUSTMENT_LIST = 'MonetaryAdjustment.ViewAdjustmentList',
  MONETARY_ADJUSTMENT_APPROVE_ADJUSTMENT = 'MonetaryAdjustment.ApproveAdjustment',
  MONETARY_ADJUSTMENT_REJECT_ADJUSTMENT = 'MonetaryAdjustment.RejectAdjustment',

  OVERVIEW_HISTORICAL_INFORMATION_VIEW = 'OverviewHistoricalInformation.View',

  OVERVIEW_MONETARY_INFORMATION_VIEW = 'OverviewMonetaryInformation.View',

  PAYMENT_HISTORY_VIEW = 'PaymentHistory.View',

  PAYMENT_LIST_VIEW = 'PaymentList.View',
  PAYMENT_LIST_CANCEL_PENDING_PAYMENT = 'PaymentList.CancelPendingPayment',

  PENDING_AUTHORIZATION_VIEW = 'PendingAuthorization.View',

  PUSH_QUEUE_LIST_VIEW = 'PushQueueList.View',

  RELATED_ACCOUNTS_VIEW = 'RelatedAccounts.View',

  REPORTING_VIEW = 'Reporting.View',

  STATEMENT_VIEW = 'Statement.View',

  STATEMENT_HISTORY_VIEW = 'StatementHistory.View',

  TALKING_POINTS_VIEW = 'TalkingPoints.View',

  USER_ROLE_ENTITLEMENT_VIEW = 'UserRoleEntitlement.View',
  USER_ROLE_ENTITLEMENT_EDIT = 'UserRoleEntitlement.Edit'
}

export const API_ENTITLEMENT_CODE = [
  'view',
  'edit',
  'editExternalStatusCode',
  'editExternalStatusReasonCode',
  'viewAddress',
  'viewAddressHistory',
  'addAddress',
  'editAddress',
  'deleteAddress',
  'viewPhone',
  'editPhone',
  'viewEmail',
  'editEmail',
  'addCSPA',
  'editCSPA',
  'deleteCSPA',
  'viewSettings',
  'changeSettings',
  'updateNextWorkDate',
  'updateMoveToQueue',
  'updateDelinquencyReason',
  'bypassAuthentication',
  'viewLetterList',
  'viewPendingList',
  'sendLetter',
  'deletePendingLetter',
  'cancelPendingPayment',
  'addBankRegistration',
  'deleteBankRegistration',
  'schedulePayment',
  'updateCheckingAccount',
  'updateSavingsAccount',
  'makeOnDemandACH',
  'viewMemo',
  'addMemo',
  'editMemo',
  'deleteMemo',
  'adjustTransaction',
  'viewAdjustmentList',
  'approveAdjustment',
  'rejectAdjustment'
];

export enum CRITERIA_CODE {
  INTERNAL_STATUS_CODE = 'internalStatusCode',
  EXTERNAL_STATUS_CODE = 'externalStatusCode',
  COMMERCIAL_CARD_CODE = 'commercialCardCode',
  CUSTOMER_ROLE_TYPE_CODE = 'customerRoleTypeCode',
  PRESENTATION_INS_STATUS_CODE = 'presentationInstrumentStatusCode'
}

export enum CRITERIA_NAME {
  INTERNAL_STATUS_CODE = 'AccountInternalStatus',
  EXTERNAL_STATUS_CODE = 'AccountExternalStatus',
  COMMERCIAL_CARD_CODE = 'CommercialCardRelationship',
  CUSTOMER_ROLE_TYPE_CODE = 'AccountCustomerRoleTypeCode',
  PRESENTATION_INS_STATUS_CODE = 'PlasticExternalStatus'
}

export const listCriteria = [
  CRITERIA_NAME.INTERNAL_STATUS_CODE,
  CRITERIA_NAME.EXTERNAL_STATUS_CODE,
  CRITERIA_NAME.COMMERCIAL_CARD_CODE,
  CRITERIA_NAME.CUSTOMER_ROLE_TYPE_CODE,
  CRITERIA_NAME.PRESENTATION_INS_STATUS_CODE
];
