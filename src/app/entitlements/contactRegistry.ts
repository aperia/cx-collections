import isNil from 'lodash.isnil';

// Types
import { ContactEntitlementRegister } from './types';

export const contactRegistry = () => {
  const contactEntitlementRegister: ContactEntitlementRegister = {};

  const register = (storeId: string, entitlements: Array<EntitlementItem>) => {
    if (isNil(contactEntitlementRegister[storeId]))
      contactEntitlementRegister[storeId] = [];
    contactEntitlementRegister[storeId] = [
      ...contactEntitlementRegister[storeId],
      ...entitlements
    ];
  };

  const removeRegister = (storeId: string) => {
    delete contactEntitlementRegister[storeId];
  };

  const registerAll = (
    descriptors: Array<EntitlementConfig>,
    storeId: string
  ) => {
    descriptors.forEach(item => {
      const { entitlements: { items = [] } = {} } = item;
      return register(storeId, items);
    });
  };

  const takeEntitlement = (entitlementCode: string, storeId: string) => {
    if (isNil(contactEntitlementRegister[storeId])) return undefined;
    return contactEntitlementRegister[storeId].find(
      item => item.entitlementCode === entitlementCode
    );
  };

  return {
    removeRegister,
    registerAll,
    takeEntitlement
  };
};

export const contactEntitlement = contactRegistry();
