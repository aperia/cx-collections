import { CRITERIA_NAME } from '../constants';

export const masterAccountStatusStructure = [
  {
    criteriaCode: CRITERIA_NAME.INTERNAL_STATUS_CODE,

    items: [
      { value: 'D', description: 'txt_delinquent', active: false },
      { value: 'O', description: 'txt_overlimit', active: false },
      {
        value: 'X',
        description: 'txt_overlimit_and_delinquent',
        active: false
      },
      { value: ' ', description: 'txt_normal', active: false }
    ]
  },
  {
    criteriaCode: CRITERIA_NAME.EXTERNAL_STATUS_CODE,
    items: [
      {
        value: 'A',
        description: 'txt_authorization_prohibited',
        active: false
      },
      {
        value: 'B',
        description: 'txt_bankrupt',
        active: false
      },
      {
        value: 'C',
        description: 'txt_closed',
        active: false
      },
      {
        value: 'E',
        description: 'txt_revoked',
        active: false
      },
      {
        value: 'F',
        description: 'txt_frozen',
        active: false
      },
      {
        value: 'I',
        description: 'txt_interest_prohibited',
        active: false
      },
      {
        value: 'L',
        description: 'txt_lost',
        active: false
      },
      {
        value: 'U',
        description: 'txt_stolen',
        active: false
      },
      {
        value: 'Z',
        description: 'txt_charged_off',
        active: false
      },
      {
        value: ' ',
        description: 'txt_normal',
        active: false
      }
    ]
  },
  {
    criteriaCode: CRITERIA_NAME.COMMERCIAL_CARD_CODE,
    items: [
      {
        value: 'C',
        description: 'txt_control',
        active: false
      },
      {
        value: 'I',
        description: 'txt_individual',
        active: false
      },
      {
        value: 'S',
        description: 'txt_sub_account',
        active: false
      }
    ]
  },
  {
    criteriaCode: CRITERIA_NAME.CUSTOMER_ROLE_TYPE_CODE,
    items: [
      {
        value: '01',
        description: 'txt_primary',
        active: false
      },
      {
        value: '02',
        description: 'txt_secondary',
        active: false
      },
      {
        value: '03',
        description: 'txt_authorized',
        active: false
      },
      {
        value: 'CD2',
        description: 'txt_administrator',
        active: false
      },
      {
        value: 'NC',
        description: 'txt_back_office',
        active: false
      },
      {
        value: 'CD1',
        description: 'txt_branch',
        active: false
      },
      {
        value: 'DMC',
        description: 'txt_debt_management_company',
        active: false
      },
      {
        value: 'CD3',
        description: 'txt_merchant_or_other',
        active: false
      },
      {
        value: 'CD4',
        description: 'txt_power_of_attorney',
        active: false
      },
      {
        value: 'CD5',
        description: 'txt_third_party',
        active: false
      },
      {
        value: 'SP',
        description: 'txt_spouse',
        active: false
      },
      {
        value: 'AT',
        description: 'txt_attorney',
        active: false
      }
    ]
  },
  {
    criteriaCode: CRITERIA_NAME.PRESENTATION_INS_STATUS_CODE,
    items: [
      {
        value: 'A',
        description: 'txt_authorization_prohibited',
        active: false
      },
      {
        value: 'C',
        description: 'txt_closed',
        active: false
      },
      {
        value: 'L',
        description: 'txt_lost',
        active: false
      },
      {
        value: 'U',
        description: 'txt_stolen',
        active: false
      },
      {
        value: ' ',
        description: 'txt_normal',
        active: false
      }
    ]
  }
];

export default masterAccountStatusStructure;
