import { ENTITLEMENT_COMPONENTS, PERMISSIONS } from '../constants';

// fix sonar
const masterComponentStruture = [
  {
    component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.ACCOUNT_INFORMATION_VIEW,
          active: false
        }
      ]
    }
  },
  {
    component: ENTITLEMENT_COMPONENTS.ACCOUNT_SNAPSHOT,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.ACCOUNT_SNAPSHOT_VIEW,
          active: false
        },
        {
          entitlementCode: PERMISSIONS.ACCOUNT_SNAPSHOT_EDIT_EXTERNAL_CODE,
          active: false
        }
      ]
    }
  },
  {
    component: ENTITLEMENT_COMPONENTS.CARDHOLDER_MAINTENANCE,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.CARDHOLDER_MAINTENANCE_VIEW,
          active: false
        },
        {
          entitlementCode: PERMISSIONS.CARDHOLDER_MAINTENANCE_VIEW_ADDRESS,
          active: false
        },
        {
          entitlementCode: PERMISSIONS.CARDHOLDER_MAINTENANCE_ADD_ADDRESS,
          active: false
        },
        {
          entitlementCode: PERMISSIONS.CARDHOLDER_MAINTENANCE_EDIT_ADDRESS,
          active: false
        },
        {
          entitlementCode: PERMISSIONS.CARDHOLDER_MAINTENANCE_DELETE_ADDRESS,
          active: false
        },
        {
          entitlementCode:
            PERMISSIONS.CARDHOLDER_MAINTENANCE_VIEW_ADDRESS_HISTORY,
          active: false
        },
        {
          entitlementCode: PERMISSIONS.CARDHOLDER_MAINTENANCE_VIEW_PHONE,
          active: false
        },
        {
          entitlementCode: PERMISSIONS.CARDHOLDER_MAINTENANCE_EDIT_PHONE,
          active: false
        },
        {
          entitlementCode: PERMISSIONS.CARDHOLDER_MAINTENANCE_VIEW_EMAIL,
          active: false
        },
        {
          entitlementCode: PERMISSIONS.CARDHOLDER_MAINTENANCE_EDIT_EMAIL,
          active: false
        }
      ]
    }
  },
  {
    component: ENTITLEMENT_COMPONENTS.CLIENT_CONTACT_INFO,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.COLLECTION_CONTACT_INFO_VIEW,
          active: false
        }
      ]
    }
  },
  {
    component: ENTITLEMENT_COMPONENTS.COLLECTIONS_ACTIVITIES,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.COLLECTION_ACTIVITIES_VIEW,
          active: false
        },
        {
          entitlementCode:
            PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_NEXT_WORK_DATE,
          active: false
        },
        {
          entitlementCode:
            PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_MOVE_TO_QUEUE,
          active: false
        },
        {
          entitlementCode:
            PERMISSIONS.COLLECTION_ACTIVITIES_UPDATE_DELINQUENCY_REASON,
          active: false
        }
      ]
    }
  },
  {
    component: ENTITLEMENT_COMPONENTS.COLLECTION_INFORMATION,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.COLLECTION_INFORMATION_VIEW,
          active: false
        }
      ]
    }
  },
  {
    component: ENTITLEMENT_COMPONENTS.LAST_25_ACTIONS,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.LAST_25_ACTIONS_VIEW,
          active: false
        }
      ]
    }
  },
  {
    component: ENTITLEMENT_COMPONENTS.LETTER,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.LETTER_VIEW_LETTER_LIST,
          active: false
        },
        {
          entitlementCode: PERMISSIONS.LETTER_SEND_LETTER,
          active: false
        },
        {
          entitlementCode: PERMISSIONS.LETTER_VIEW_PENDING_LIST,
          active: false
        },
        {
          entitlementCode: PERMISSIONS.LETTER_DELETE_PENDING_LETTER,
          active: false
        }
      ]
    }
  },
  {
    component: ENTITLEMENT_COMPONENTS.MAKE_PAYMENT,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.MAKE_PAYMENT_ADD_BANK_REGISTRATION,

          active: false
        },
        {
          entitlementCode: PERMISSIONS.MAKE_PAYMENT_DELETE_BANK_REGISTRATION,

          active: false
        },
        {
          entitlementCode: PERMISSIONS.MAKE_PAYMENT_SCHEDULE_PAYMENT,

          active: false
        },
        {
          entitlementCode: PERMISSIONS.MAKE_PAYMENT_UPDATE_CHECKING_ACCOUNT,

          active: false
        },
        {
          entitlementCode: PERMISSIONS.MAKE_PAYMENT_UPDATE_SAVINGS_ACCOUNT,

          active: false
        },
        {
          entitlementCode: PERMISSIONS.MAKE_PAYMENT_MAKE_ON_DEMAND_ACH,

          active: false
        }
      ]
    }
  },
  {
    component: ENTITLEMENT_COMPONENTS.MEMO,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.VIEW_MEMO,

          active: false
        },
        {
          entitlementCode: PERMISSIONS.ADD_MEMO,

          active: false
        },
        {
          entitlementCode: PERMISSIONS.EDIT_MEMO,

          active: false
        },
        {
          entitlementCode: PERMISSIONS.DELETE_MEMO,

          active: false
        }
      ]
    }
  },
  {
    component: ENTITLEMENT_COMPONENTS.OVERVIEW_HISTORICAL_INFORMATION,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.OVERVIEW_HISTORICAL_INFORMATION_VIEW,

          active: false
        }
      ]
    }
  },
  {
    component: ENTITLEMENT_COMPONENTS.OVERVIEW_MONETARY_INFORMATION,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.OVERVIEW_MONETARY_INFORMATION_VIEW,

          active: false
        }
      ]
    }
  },
  {
    component: ENTITLEMENT_COMPONENTS.PAYMENT_HISTORY,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.PAYMENT_HISTORY_VIEW,

          active: false
        }
      ]
    }
  },
  {
    component: ENTITLEMENT_COMPONENTS.PAYMENT_LIST,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.PAYMENT_LIST_VIEW,
          active: false
        },
        {
          entitlementCode: PERMISSIONS.PAYMENT_LIST_CANCEL_PENDING_PAYMENT,
          active: false
        }
      ]
    }
  },
  {
    component: ENTITLEMENT_COMPONENTS.PENDING_AUTHORIZATION,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.PENDING_AUTHORIZATION_VIEW,

          active: false
        }
      ]
    }
  },
  {
    component: ENTITLEMENT_COMPONENTS.RELATED_ACCOUNT,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.RELATED_ACCOUNTS_VIEW,

          active: false
        }
      ]
    }
  },

  {
    component: ENTITLEMENT_COMPONENTS.STATEMENT,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.STATEMENT_VIEW,

          active: false
        }
      ]
    }
  },
  {
    component: ENTITLEMENT_COMPONENTS.STATEMENT_HISTORY,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.STATEMENT_HISTORY_VIEW,

          active: false
        }
      ]
    }
  },
  {
    component: ENTITLEMENT_COMPONENTS.TALKING_POINTS,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.TALKING_POINTS_VIEW,
          active: false
        }
      ]
    }
  },
  {
    component: ENTITLEMENT_COMPONENTS.WEBSITE_LAUNCHER,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.WEBSITE_LAUNCHER_VIEW,
          active: false
        }
      ]
    }
  }
];

export default masterComponentStruture;
