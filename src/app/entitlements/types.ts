import { PERMISSIONS } from './constants';

export type UserEntitlementRegister = Array<EntitlementItem>;

/**
 * Contact & Account status entitlement
 */

export type ContactEntitlementRegister = Record<string, Array<EntitlementItem>>;

export interface CallerEntitlementConfig {
  components?: [
    {
      component?: string;
      jsonValue?: {
        [entitlement: string]: boolean | undefined;
      };
    }
  ];
}
export interface RoleEntitlementConfig {
  components?: [
    {
      component?: string;
      entitlements?: {
        [entitlement: string]: boolean | undefined;
      };
    }
  ];
}

export type PermissionKy = keyof typeof PERMISSIONS;
