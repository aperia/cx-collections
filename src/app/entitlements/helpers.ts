import { isEmpty, isUndefined } from 'app/_libraries/_dls/lodash';

// Helper
import { contactEntitlement } from './contactRegistry';

// Type
import { CallerEntitlementConfig, RoleEntitlementConfig } from './types';

// Dictionary
import { AppComponentDictionary } from './dictionary';

// Const
import {
  API_ENTITLEMENT_CODE,
  COMPONENTS_MAP,
  ENTITLEMENT_COMPONENTS,
  PERMISSIONS
} from './constants';

// Master data
import masterComponentStructure from './MasterStructure/masterComponentStruture';

export const buildEntitlementCode = (
  component: ENTITLEMENT_COMPONENTS,
  entitlement: string
) => {
  if (!API_ENTITLEMENT_CODE.includes(entitlement)) return;

  return `${component}.${
    entitlement.charAt(0).toLocaleUpperCase() + entitlement.slice(1)
  }`;
};

export const convertUserRoleEntitlement = (data: RoleEntitlementConfig) => {
  if (isUndefined(data.components)) return [];

  return data.components?.reduce((newArr: Array<EntitlementConfig>, item) => {
    const { component = '', entitlements = {} } = item;

    const buildComponentKy = AppComponentDictionary.get(
      component as COMPONENTS_MAP
    );

    if (isUndefined(buildComponentKy)) return newArr;

    const newEntitlements = Object.keys(entitlements).reduce(
      (newItems: Array<EntitlementItem>, ky) => {
        const entitlementCode = buildEntitlementCode(
          buildComponentKy,
          ky
        ) as PERMISSIONS;

        if (isUndefined(entitlementCode)) return newItems;

        newItems = [
          ...newItems,
          {
            active: entitlements[ky],
            entitlementCode
          }
        ];
        return newItems;
      },
      []
    );

    newArr = [
      ...newArr,
      {
        component: buildComponentKy,
        entitlements: {
          items: newEntitlements
        }
      }
    ];

    return newArr;
  }, []);
};

export const convertCallerRoleEntitlement = (data: CallerEntitlementConfig) => {
  if (isUndefined(data.components)) return [];

  return data.components?.reduce((newArr: Array<EntitlementConfig>, item) => {
    const { component = '', jsonValue = {} } = item;

    const buildComponentKy = AppComponentDictionary.get(
      component as COMPONENTS_MAP
    );

    if (isUndefined(buildComponentKy)) return newArr;

    const newEntitlements = Object.keys(jsonValue).reduce(
      (newItems: Array<EntitlementItem>, ky) => {
        const entitlementCode = buildEntitlementCode(
          buildComponentKy,
          ky
        ) as PERMISSIONS;

        if (isUndefined(entitlementCode)) return newItems;

        newItems = [
          ...newItems,
          {
            active: jsonValue[ky],
            entitlementCode
          }
        ];
        return newItems;
      },
      []
    );

    newArr = [
      ...newArr,
      {
        component: buildComponentKy,
        entitlements: {
          items: newEntitlements
        }
      }
    ];
    return newArr;
  }, []);
};

export const mappingPermission = (
  roleData: Array<EntitlementConfig>,
  callerData: Array<EntitlementConfig>
) => {
  const buildEntitlements = (
    templateEntitlements: Array<EntitlementItem>,
    roleItems: Array<EntitlementItem>,
    callerItems: Array<EntitlementItem>
  ) => {
    return templateEntitlements.reduce(
      (newItems: Array<EntitlementItem>, item) => {
        const { entitlementCode } = item;
        const roleEntitlement = roleItems.find(
          i => i.entitlementCode === entitlementCode
        );
        const callerEntitlement = callerItems.find(
          i => i.entitlementCode === entitlementCode
        );

        if (roleEntitlement && callerEntitlement) {
          newItems = [
            ...newItems,
            {
              active: roleEntitlement.active && callerEntitlement.active,
              entitlementCode
            }
          ];
        }

        if (roleEntitlement && isUndefined(callerEntitlement)) {
          newItems = [
            ...newItems,
            {
              active: false,
              entitlementCode
            }
          ];
        }

        if (callerEntitlement && isUndefined(roleEntitlement)) {
          newItems = [
            ...newItems,
            {
              active: false,
              entitlementCode
            }
          ];
        }

        return newItems;
      },
      []
    );
  };

  return masterComponentStructure.reduce(
    (newArrData: Array<EntitlementConfig>, item) => {
      const { component, entitlements } = item;
      const roleComponent = roleData.find(i => i.component === component);
      const callerComponent = callerData.find(i => i.component === component);

      const { items: roleItems = [] } = roleComponent?.entitlements || {};
      const { items: callerItems = [] } = callerComponent?.entitlements || {};

      newArrData = [
        ...newArrData,
        {
          component,
          entitlements: {
            items: buildEntitlements(
              entitlements?.items,
              roleItems,
              callerItems
            )
          }
        }
      ];

      return newArrData;
    },
    []
  );
};

export const checkPermission = (
  entitlementCode: string,
  storeId: string
): boolean => {
  if (isEmpty(entitlementCode)) return false;

  const contactPermission = contactEntitlement.takeEntitlement(
    entitlementCode,
    storeId || ''
  );

  if (contactPermission) {
    return !!contactPermission?.active;
  }

  return false;
};
