import { PERMISSIONS } from './constants';
import { userEntitlement } from './userRegistry';

describe('Test userRegister', () => {
  const mCode = PERMISSIONS.ACCOUNT_SEARCH_VIEW;
  it('register empty descriptor', () => {
    userEntitlement.registerAll([{}]);
    expect(userEntitlement.takeEntitlement(mCode)).toBe(undefined);
  });

  it('should be working', () => {
    const mEntitlement = {
      entitlements: {
        items: [
          {
            entitlementCode: mCode,
            active: true
          }
        ]
      }
    };

    userEntitlement.registerAll([mEntitlement]);

    expect(userEntitlement.takeEntitlement(mCode)).toEqual(
      mEntitlement.entitlements.items[0]
    );
  });
});
