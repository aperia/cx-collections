import { storeId } from 'app/test-utils';
import { PERMISSIONS } from './constants';
import { contactEntitlement } from './contactRegistry';

describe('Test contactRegister', () => {
  const mCode = PERMISSIONS.ACCOUNT_SEARCH_VIEW;
  it('register empty descriptor', () => {
    contactEntitlement.registerAll([{}], storeId);
    expect(contactEntitlement.takeEntitlement(mCode, storeId)).toBe(undefined);
  });

  it('should be working', () => {
    const mEntitlement = {
      entitlements: {
        items: [
          {
            entitlementCode: mCode,
            active: true
          }
        ]
      }
    };

    contactEntitlement.registerAll([mEntitlement], storeId);

    expect(contactEntitlement.takeEntitlement(mCode, storeId)).toEqual(
      mEntitlement.entitlements.items[0]
    );
  });

  it('removeRegister', () => {
    const mEntitlement = {
      entitlements: {
        items: [
          {
            entitlementCode: mCode,
            active: true
          }
        ]
      }
    };

    contactEntitlement.registerAll([mEntitlement], storeId);
    contactEntitlement.removeRegister(storeId);

    expect(contactEntitlement.takeEntitlement(mCode, storeId)).toEqual(
      undefined
    );
  });

  it('take wrong storeId', () => {
    const mEntitlement = {
      entitlements: {
        items: [
          {
            entitlementCode: mCode,
            active: true
          }
        ]
      }
    };

    contactEntitlement.registerAll([mEntitlement], storeId);

    expect(contactEntitlement.takeEntitlement(mCode, 'any')).toEqual(undefined);
  });
});
