// Types
import { UserEntitlementRegister } from './types';

const userEntitlementRegistry = () => {
  let userEntitlements: UserEntitlementRegister = [];

  const register = (entitlements: Array<EntitlementItem>) => {
    userEntitlements = [...userEntitlements, ...entitlements];
  };

  const registerAll = (descriptors: Array<EntitlementConfig>) => {
    descriptors.forEach(item => {
      const { entitlements: { items = [] } = {} } = item;
      register(items);
    });
  };

  const takeEntitlement = (entitlementCode: string) => {
    return userEntitlements.find(
      item => item.entitlementCode === entitlementCode
    );
  };

  return {
    registerAll,
    takeEntitlement
  };
};

export const userEntitlement = userEntitlementRegistry();
