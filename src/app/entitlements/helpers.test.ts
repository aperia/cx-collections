import { storeId } from 'app/test-utils';
import {
  API_ENTITLEMENT_CODE,
  COMPONENTS_MAP,
  ENTITLEMENT_COMPONENTS,
  PERMISSIONS
} from './constants';
import { contactEntitlement } from './contactRegistry';
import {
  buildEntitlementCode,
  checkPermission,
  convertCallerRoleEntitlement,
  convertUserRoleEntitlement,
  mappingPermission
} from './helpers';
import { CallerEntitlementConfig, RoleEntitlementConfig } from './types';

describe('test buildEntitlementCode', () => {
  it('entitlement code is not in dictionary', () => {
    const result = buildEntitlementCode(
      ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH,
      'any-action'
    );
    expect(result).toBe(undefined);
  });
  it('should be working', () => {
    const result = buildEntitlementCode(
      ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH,
      API_ENTITLEMENT_CODE[0]
    );
    expect(result).toBe(
      `${ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH}.${
        API_ENTITLEMENT_CODE[0].charAt(0).toLocaleUpperCase() +
        API_ENTITLEMENT_CODE[0].slice(1)
      }`
    );
  });
});

describe('test convertUserRoleEntitlement ', () => {
  const mData = {
    components: [
      {
        component: COMPONENTS_MAP.ACCOUNT_SEARCH,
        entitlements: {
          [API_ENTITLEMENT_CODE[0]]: true
        }
      }
    ]
  } as RoleEntitlementConfig;

  it('empty component', () => {
    const result = convertUserRoleEntitlement({ components: [{}] });
    expect(result).toEqual([]);
  });

  it('component is undefined', () => {
    const result = convertUserRoleEntitlement({ components: undefined });
    expect(result).toEqual([]);
  });

  it('component is not in dictionary', () => {
    const result = convertUserRoleEntitlement({
      components: [
        {
          component: 'any-component',
          entitlements: {
            [PERMISSIONS.ACCOUNT_SEARCH_VIEW]: true
          }
        }
      ]
    });
    expect(result).toEqual([]);
  });

  it('entitlementCode is not in dictionary', () => {
    const result = convertUserRoleEntitlement({
      components: [
        {
          component: COMPONENTS_MAP.ACCOUNT_SEARCH,
          entitlements: {
            'any-entitlement': true
          }
        }
      ]
    });
    expect(result).toEqual([
      {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH,
        entitlements: { items: [] }
      }
    ]);
  });

  it('should be working', () => {
    const result = convertUserRoleEntitlement(mData);
    expect(result).toEqual([
      {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH,
        entitlements: {
          items: [
            {
              active: true,
              entitlementCode: `${ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH}.${
                API_ENTITLEMENT_CODE[0].charAt(0).toLocaleUpperCase() +
                API_ENTITLEMENT_CODE[0].slice(1)
              }`
            }
          ]
        }
      }
    ]);
  });
});

describe('test convertCallerRoleEntitlement', () => {
  const mData = {
    components: [
      {
        component: COMPONENTS_MAP.ACCOUNT_SEARCH,
        jsonValue: {
          [API_ENTITLEMENT_CODE[0]]: true
        }
      }
    ]
  } as CallerEntitlementConfig;

  it('empty component', () => {
    const result = convertCallerRoleEntitlement({ components: [{}] });
    expect(result).toEqual([]);
  });

  it('component is undefined', () => {
    const result = convertCallerRoleEntitlement({ components: undefined });
    expect(result).toEqual([]);
  });

  it('component is not in dictionary', () => {
    const result = convertCallerRoleEntitlement({
      components: [
        {
          component: 'any-component',
          jsonValue: {
            [PERMISSIONS.ACCOUNT_SEARCH_VIEW]: true
          }
        }
      ]
    });
    expect(result).toEqual([]);
  });

  it('entitlementCode is not in dictionary', () => {
    const result = convertCallerRoleEntitlement({
      components: [
        {
          component: COMPONENTS_MAP.ACCOUNT_SEARCH,
          jsonValue: {
            'any-entitlement': true
          }
        }
      ]
    });
    expect(result).toEqual([
      {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH,
        entitlements: { items: [] }
      }
    ]);
  });

  it('should be working', () => {
    const result = convertCallerRoleEntitlement(mData);
    expect(result).toEqual([
      {
        component: ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH,
        entitlements: {
          items: [
            {
              active: true,
              entitlementCode: `${ENTITLEMENT_COMPONENTS.ACCOUNT_SEARCH}.${
                API_ENTITLEMENT_CODE[0].charAt(0).toLocaleUpperCase() +
                API_ENTITLEMENT_CODE[0].slice(1)
              }`
            }
          ]
        }
      }
    ]);
  });
});

describe('test mappingPermission ', () => {
  const mConfigUnit = {
    component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.ACCOUNT_INFORMATION_VIEW,
          active: false
        }
      ]
    }
  };

  const mConfigUnitActive = {
    component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION,
    entitlements: {
      items: [
        {
          entitlementCode: PERMISSIONS.ACCOUNT_INFORMATION_VIEW,
          active: true
        }
      ]
    }
  };

  const MASTER = [
    {
      component: 'AccountSnapshot',
      entitlements: {
        items: []
      }
    },
    {
      component: 'CardholderMaintenance',
      entitlements: {
        items: []
      }
    },
    {
      component: 'ClientContactInformation',
      entitlements: {
        items: []
      }
    },
    {
      component: 'CollectionActivities',
      entitlements: {
        items: []
      }
    },
    {
      component: 'CollectionInformation',
      entitlements: {
        items: []
      }
    },
    {
      component: 'LastActions',
      entitlements: {
        items: []
      }
    },
    {
      component: 'Letter',
      entitlements: {
        items: []
      }
    },
    {
      component: 'MakePayment',
      entitlements: {
        items: []
      }
    },
    {
      component: 'Memo',
      entitlements: {
        items: []
      }
    },
    {
      component: 'OverviewHistoricalInformation',
      entitlements: {
        items: []
      }
    },
    {
      component: 'OverviewMonetaryInformation',
      entitlements: {
        items: []
      }
    },
    {
      component: 'PaymentHistory',
      entitlements: {
        items: []
      }
    },
    {
      component: 'PaymentList',
      entitlements: {
        items: []
      }
    },
    {
      component: 'PendingAuthorization',
      entitlements: {
        items: []
      }
    },
    {
      component: 'RelatedAccounts',
      entitlements: {
        items: []
      }
    },
    {
      component: 'Statement',
      entitlements: {
        items: []
      }
    },
    {
      component: 'StatementHistory',
      entitlements: {
        items: []
      }
    },
    {
      component: 'TalkingPoints',
      entitlements: {
        items: []
      }
    },
    {
      component: 'WebsiteLauncher',
      entitlements: {
        items: []
      }
    }
  ];

  const mWrongComponentConfig = {
    component: undefined,
    entitlements: undefined
  } as EntitlementConfig;

  const mWrongEntitlementsConfig = {
    component: ENTITLEMENT_COMPONENTS.ACCOUNT_INFORMATION,
    entitlements: {
      items: [
        {
          entitlementCode: 'any-code' as never,
          active: false
        }
      ]
    }
  };

  it('should be working', () => {
    const result = mappingPermission([mConfigUnitActive], [mConfigUnit]);
    expect(result).toEqual([mConfigUnit, ...MASTER]);
  });
  it('callerComponent is undefined', () => {
    const result = mappingPermission([mConfigUnit], [mWrongComponentConfig]);
    expect(result).toEqual([mConfigUnit, ...MASTER]);
  });
  it('role Component is undefined', () => {
    const result = mappingPermission([mWrongComponentConfig], [mConfigUnit]);
    expect(result).toEqual([mConfigUnit, ...MASTER]);
  });

  it('wrong caller Component', () => {
    const result = mappingPermission([mConfigUnit], [mWrongEntitlementsConfig]);
    expect(result).toEqual([mConfigUnit, ...MASTER]);
  });

  it('wrong role Component', () => {
    const result = mappingPermission([mWrongEntitlementsConfig], [mConfigUnit]);
    expect(result).toEqual([mConfigUnit, ...MASTER]);
  });
});

describe('test checkPermission', () => {
  it('should be has permission when empty entitlementCode', () => {
    const result = checkPermission('', storeId);
    expect(result).toEqual(false);
  });

  it('should be has permission', () => {
    contactEntitlement.registerAll(
      [
        {
          entitlements: {
            items: [
              {
                entitlementCode: PERMISSIONS.ACCOUNT_SEARCH_VIEW,
                active: true
              }
            ]
          }
        }
      ],
      storeId
    );

    const result = checkPermission(PERMISSIONS.ACCOUNT_SEARCH_VIEW, storeId);
    expect(result).toEqual(true);
  });

  it('should be has not permission when empty storeId', () => {
    contactEntitlement.registerAll(
      [
        {
          entitlements: {
            items: [
              {
                entitlementCode: PERMISSIONS.ACCOUNT_SEARCH_VIEW,
                active: true
              }
            ]
          }
        }
      ],
      storeId
    );

    const result = checkPermission(
      PERMISSIONS.ACCOUNT_SEARCH_VIEW,
      undefined as never
    );
    expect(result).toEqual(false);
  });
});
