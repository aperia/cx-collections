import React from 'react';
import { ViewLayoutProps } from '../../core';

export interface BootstrapGridLayoutProps
  extends Partial<React.HTMLProps<HTMLDivElement>> {
  columnWidth?: number;
}

const BootstrapGridLayout: React.FunctionComponent<
  ViewLayoutProps<BootstrapGridLayoutProps> & { className: string }
> = ({ viewElements, className = '', viewName }) => {
  return (
    <div className={`row ${className}`} role="dof-view" role-name={viewName}>
      {viewElements.map((viewElement, index) => {
        if (!viewElement.layoutProps) {
          return (
            <div key={index} role="dof-field">
              {viewElement.renderedNode}
            </div>
          );
        }

        const { className, columnWidth, ...others } = viewElement.layoutProps;
        const classNames: string[] = [];
        if (columnWidth) {
          classNames.push(`col-${columnWidth}`);
        }
        if (className) {
          classNames.push(className);
        }
        return (
          <div
            key={index}
            className={classNames.join(' ')}
            {...others}
            role="dof-field"
          >
            {viewElement.renderedNode}
          </div>
        );
      })}
    </div>
  );
};

export default BootstrapGridLayout;
