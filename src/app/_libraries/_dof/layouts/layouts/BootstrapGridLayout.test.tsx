import '@testing-library/jest-dom';
import { render, RenderResult } from '@testing-library/react';
import React from 'react';
import BootstrapGridLayout from './BootstrapGridLayout';

describe('Text Layouts > BootstrapGridLayout', () => {
  const bootstrapGridLayoutProps = {
    viewName: 'mock-view-name',
    viewElements: []
  };

  const renderBootstrapGridLayout = (props?: any): RenderResult => {
    return render(
      <BootstrapGridLayout {...bootstrapGridLayoutProps} {...props} />
    );
  };

  it('Render BootstrapGridLayout UI, empty viewElements', () => {
    const wrapper = renderBootstrapGridLayout();
    expect(wrapper.getByRole('dof-view')).toBeInTheDocument();
    expect(wrapper.getByRole('dof-view').childElementCount).toEqual(0);
  });

  it('Render BootstrapGridLayout UI, with viewElements have empty layout props', () => {
    const viewElements = [
      {
        renderedNode: <div>Mock view</div>
      }
    ];
    const wrapper = renderBootstrapGridLayout({ viewElements });
    expect(wrapper.getByRole('dof-field').className).toEqual('');
    expect(wrapper.getByRole('dof-view').firstChild!.textContent).toEqual(
      'Mock view'
    );
  });

  it('Render BootstrapGridLayout UI, with viewElements have layout props', () => {
    let viewElements = [
      {
        layoutProps: {
          className: 'mock-name',
          columnWidth: '3'
        },
        renderedNode: <div>Mock view with layout props</div>
      }
    ];
    let wrapper = renderBootstrapGridLayout({ viewElements });
    expect(wrapper.getByRole('dof-field').className).toEqual('col-3 mock-name');
    expect(wrapper.getByRole('dof-view').firstChild!.textContent).toEqual(
      'Mock view with layout props'
    );

    wrapper.unmount();

    viewElements = [
      {
        layoutProps: {
          tabIndex: 2
        } as any,
        renderedNode: <div>Mock view with layout props</div>
      }
    ];
    wrapper = renderBootstrapGridLayout({ viewElements });

    expect(wrapper.getByRole('dof-field').className).toEqual('');
    expect(wrapper.getByRole('dof-field').tabIndex).toEqual(2);
    expect(wrapper.getByRole('dof-view').firstChild!.textContent).toEqual(
      'Mock view with layout props'
    );
  });
});
