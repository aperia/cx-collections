import '@testing-library/jest-dom';
import { render, RenderResult } from '@testing-library/react';
import React from 'react';
import SimpleLayout from './SimpleLayout';

describe('Text Layouts > SimpleLayout', () => {
  const simpleLayoutProps = {
    viewName: 'mock-view-name',
    viewElements: []
  };

  const renderSimpleLayout = (props?: any): RenderResult => {
    return render(
      <SimpleLayout {...simpleLayoutProps} {...props} />
    );
  };

  it('Render SimpleLayout UI, empty viewElements', () => {
    const wrapper = renderSimpleLayout();
    expect(wrapper.baseElement.firstChild).toBeEmptyDOMElement();
  });

  it('Render SimpleLayout UI, with viewElements data', () => {
    const viewElements = [
      {
        renderedNode: <div>Mock view</div>
      }
    ];
    const wrapper = renderSimpleLayout({ viewElements });
    expect(wrapper.baseElement.firstChild!.textContent).toEqual(
      'Mock view'
    );
  });
});
