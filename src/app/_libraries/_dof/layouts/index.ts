import { layoutRegistry } from '../core';
import BootstrapGridLayout from './layouts/BootstrapGridLayout';
import HtmlTemplateLayout from './layouts/HtmlTemplateLayout';
import SimpleLayout from './layouts/SimpleLayout';

layoutRegistry.registerLayout('BootstrapGridLayout', BootstrapGridLayout);
layoutRegistry.registerLayout('HtmlTemplateLayout', HtmlTemplateLayout);
layoutRegistry.registerLayout('SimpleLayout', SimpleLayout);

export { BootstrapGridLayout, HtmlTemplateLayout, SimpleLayout };
