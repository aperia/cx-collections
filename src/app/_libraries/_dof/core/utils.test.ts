import { compareTo, isPromise, pad } from './utils';

describe('Test Utils file', () => {
  it('test isPromise function', () => {
    let result = isPromise(true);
    expect(result).toEqual(false);

    result = isPromise(Promise.resolve(true));
    expect(result).toEqual(true);
  });

  it('test pad function', () => {
    let result = pad('12.33', '');
    expect(result).toEqual(['12.33', '000.00']);

    result = pad('', '12.33');
    expect(result).toEqual(['000.00', '12.33']);

    result = pad('123.11', '12.33');
    expect(result).toEqual(['123.11', '012.33']);

    result = pad('11.123', '12.33');
    expect(result).toEqual(['11.123', '12.330']);

    result = pad('11.12', '12.333');
    expect(result).toEqual(['11.120', '12.333']);
  });

  it('test compareTo function', () => {
    let result = compareTo('-2', '3');
    expect(result).toEqual(-1);

    result = compareTo('2', '-3');
    expect(result).toEqual(1);

    result = compareTo('-3', '-3');
    expect(result).toEqual(0);

    result = compareTo('115', '113');
    expect(result).toEqual(1);

    result = compareTo('-113', '-115');
    expect(result).toEqual(1);

    result = compareTo('-234', '-230');
    expect(result).toEqual(-1);

    result = compareTo('230', '234');
    expect(result).toEqual(-1);

    result = compareTo('1', '1');
    expect(result).toEqual(0);

    result = compareTo('0', '');
    expect(result).toEqual(0);
  });
});
