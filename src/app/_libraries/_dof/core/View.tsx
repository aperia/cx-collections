/* eslint-disable react-hooks/exhaustive-deps */
import React, { useContext, useEffect, useMemo, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  initialize,
  reduxForm,
  FormInstance,
  FormSubmitHandler,
  Field,
  getFormValues as getReduxFormValues
} from 'redux-form';

import RegisteredComponentsContext from './contexts/RegisteredComponentsContext';

import {
  FieldDescriptorOverrides,
  ViewLayoutProps,
  ViewDescriptor,
  ViewElementDescriptor,
  ViewLayoutElement,
  ViewLayoutDescriptor,
  ExtraFieldProps
} from './interfaces/dynamic';

import ViewElement from './ViewElement';
import { useView } from './hooks';
import DataApiContext, { DataApiRequest } from './contexts/DataApiContext';
import valueChangeRegistry from './registries/valueChangeRegistry';
import { Dispatch } from 'redux';
import { viewRegistry } from './registries';

export interface ViewProps<
  TValue = any,
  LayoutOptions extends ViewLayoutProps<LayoutProps> = ViewLayoutProps<any>,
  LayoutProps = unknown
> {
  id?: string;
  dataTestId?: string;
  formKey: string;
  descriptor: string | ViewDescriptor<LayoutOptions, LayoutProps>;
  value?: TValue;
  onSubmit?: FormSubmitHandler<TValue, ViewProps<TValue, LayoutOptions>>;
  onChange?(
    values: TValue,
    dispatch: Dispatch<any>,
    props: ViewProps<TValue, LayoutOptions>,
    previousValues: TValue
  ): void;
  onViewRefChange?: (viewRef: any) => void;
}

const View: React.RefForwardingComponent<
  FormInstance<any, React.PropsWithChildren<any>, string>,
  ViewProps
> = (props, ref) => {
  const {
    id,
    dataTestId,
    formKey,
    descriptor,
    value: initialValues,
    onSubmit,
    onChange,
    onViewRefChange
  } = props;
  const viewDescriptor = useView(descriptor);
  const layout =
    viewDescriptor && normalizeViewLayoutDescriptor(viewDescriptor.layout);
  const registeredComponents = useContext(RegisteredComponentsContext);
  const fields = useMemo(
    () => (viewDescriptor && normalizeFields(viewDescriptor.fields)) || [],
    [viewDescriptor]
  );

  const formValues = useSelector(getReduxFormValues(formKey));

  const ReduxForm = useMemo(
    () =>
      reduxForm<any, React.PropsWithChildren<any>, string>({
        form: formKey,
        onSubmit,
        onChange,
        shouldValidate: () => true
      })((propsWithChildren: React.PropsWithChildren<any>) => (
        <>{propsWithChildren.children}</>
      )),
    [formKey, onSubmit, onChange]
  );

  const dispatch = useDispatch();
  useEffect(() => {
    if (initialValues) {
      dispatch(initialize(formKey, initialValues));
    }
  }, [dispatch, formKey, initialValues]);

  const viewDataApiContext = useContext(DataApiContext);
  const { dataApi, dataName } = viewDescriptor || {};
  useEffect(() => {
    if (dataApi && initialValues == undefined) {
      const { url, customRequest } = dataApi;
      let request: DataApiRequest;
      if (customRequest && viewDataApiContext) {
        const customRequestMethod = viewDataApiContext[customRequest];
        request = customRequestMethod(url);
      } else {
        request = {
          method: 'GET',
          headers: [],
          requestData: {}
        };
      }
      (async function () {
        const { method, headers, requestData } = request;
        //console.log("view " + descriptor + " request data: ", request);

        let response: Response;
        if (method === 'GET' || method === 'HEAD') {
          response = await fetch(url, {
            method,
            headers: headers.reduce<Record<string, string>>(
              (curr, next) => ({ ...curr, ...next }),
              {}
            )
          });
        } else {
          response = await fetch(url, {
            method,
            body: JSON.stringify(requestData),
            headers: headers.reduce<Record<string, string>>(
              (curr, next) => ({ ...curr, ...next }),
              {}
            )
          });
        }
        const values = await response.json();
        dispatch(initialize(formKey, values));
      })();
    }
  }, [viewDataApiContext, dataApi, formKey]);

  const { registeredDataSource } = registeredComponents;
  useEffect(() => {
    if (
      dataName &&
      !dataApi &&
      initialValues == undefined &&
      Object.entries(registeredDataSource).length > 0
    ) {
      const dataSource = registeredDataSource[dataName];
      const { url, customRequest } = dataSource;
      let request: DataApiRequest;
      if (customRequest && viewDataApiContext) {
        const customRequestMethod = viewDataApiContext[customRequest];
        request = customRequestMethod(url);
      } else {
        request = {
          method: 'GET',
          headers: [],
          requestData: {}
        };
      }
      (async function () {
        const { method, headers, requestData } = request;
        //console.log("view " + descriptor + " request data: ", request);

        let response: Response;
        if (method === 'GET' || method === 'HEAD') {
          response = await fetch(url, {
            method,
            headers: headers.reduce<Record<string, string>>(
              (curr, next) => ({ ...curr, ...next }),
              {}
            )
          });
        } else {
          response = await fetch(url, {
            method,
            body: JSON.stringify(requestData),
            headers: headers.reduce<Record<string, string>>(
              (curr, next) => ({ ...curr, ...next }),
              {}
            )
          });
        }
        const values = await response.json();
        dispatch(initialize(formKey, values));
      })();
    }
  }, [viewDataApiContext, dataName, formKey]);

  // Update: check childs.current[i] null
  const childs = useRef<any[]>([]);
  // console.log("childs: ", childs.current);

  // Notify when viewRef updates completed!
  useEffect(() => {
    typeof onViewRefChange === 'function' &&
      childs.current.length > 0 &&
      onViewRefChange(ref);
  }, [childs.current.length]);

  const findField = (
    name: string
  ): Field<ExtraFieldProps & { disabled: boolean }> => {
    for (let i = 0; i < childs.current.length; i++) {
      if (childs.current[i]?.name === name) return childs.current[i];
    }
    return null!; //can not find the child with this name
  };

  const { registeredLayouts = {} } = registeredComponents;
  const Layout: React.ComponentType<ViewLayoutProps<any>> | undefined =
    layout &&
    (typeof layout.name === 'string'
      ? registeredLayouts[layout.name]
      : layout.name);

  const setFormValues = (form: string, values: any) => {
    dispatch(initialize(form, values));
  };

  const receiveChildValue = (
    funcName: string,
    newValue: any,
    previousValue: any,
    name: string,
    event: any
  ) => {
    const changeOrBlurHandler = valueChangeRegistry.resolve(funcName);
    changeOrBlurHandler(
      name,
      newValue,
      previousValue,
      findField,
      formKey,
      setFormValues,
      event,
      formValues
    );
  };

  const setChildRef = (child: any, index: number) => {
    childs.current[index] = child;
  };
  const viewElements = fields.map<ViewLayoutElement<any>>((field, index) => ({
    name: field.name,
    renderedNode: (
      <ViewElement
        id={`${id}_${field.name}`}
        dataTestId={`${dataTestId ? dataTestId : descriptor}_${field.name}`}
        name={field.name}
        overrides={field}
        formKey={formKey}
        ref={child => setChildRef(child, index)}
        childToParentCallback={receiveChildValue}
      />
    ),
    layoutProps: field.layoutProps
  }));

  const { registeredViews, allViews } = registeredComponents;
  const viewName =
    typeof descriptor === 'string' ? descriptor : descriptor.name;

  // return View not found : if not available
  // return null: if not has permission
  // else View
  return allViews !== undefined && !allViews.includes(viewName) ? (
    <h5>View {descriptor} is not found!</h5>
  ) : registeredViews[viewName] === undefined &&
    viewRegistry.viewsMap[viewName] === undefined ? null : (
    <ReduxForm ref={ref} onFind={findField}>
      {Layout && layout && (
        <Layout
          viewElements={viewElements}
          viewName={viewName}
          {...(layout.layoutOptions || {})}
        />
      )}
    </ReduxForm>
  );
};

function normalizeFields<LayoutProps = unknown>(
  fields: undefined | ViewElementDescriptor<LayoutProps>[]
): (FieldDescriptorOverrides & { layoutProps?: LayoutProps })[] {
  return (fields || []).map<
    FieldDescriptorOverrides & { layoutProps?: LayoutProps }
  >(field => (typeof field === 'string' ? { name: field } : field));
}

function normalizeViewLayoutDescriptor<LayoutOptions = never>(
  layout: string | ViewLayoutDescriptor<LayoutOptions> | undefined
): ViewLayoutDescriptor<LayoutOptions> {
  return layout === undefined
    ? { name: 'SimpleLayout' }
    : typeof layout === 'string'
    ? { name: layout }
    : layout;
}

export default React.forwardRef(View);
