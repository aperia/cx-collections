import { viewRegistry } from "./";
import { ViewDescriptor } from "..";

describe("Text Registries > viewRegistry", () => {
  const mockViewDescriptor = {
    name: "Mock view name",
  } as ViewDescriptor;

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("test registerView function", () => {
    viewRegistry.registerView("mock-view-1", mockViewDescriptor);

    expect(viewRegistry.viewsMap["mock-view-1"]).toEqual(mockViewDescriptor);
  });

  it("test registerViews function", () => {
    const mockViewDescriptor2 = {
      name: "Mock view name 2",
    } as ViewDescriptor;

    return Promise.resolve(
      viewRegistry.registerViews([mockViewDescriptor, mockViewDescriptor2])
    ).then(() => {
      expect(viewRegistry.viewsMap["Mock view name"]).not.toBeUndefined();
      expect(viewRegistry.viewsMap["Mock view name 2"]).not.toBeUndefined();
    });
  });

  it("test registerAvailableViews function", () => {
    viewRegistry.registerAvailableViews([
      "Mock view name 1",
      "Mock view name 2",
    ]);

    expect(viewRegistry.allViews).toEqual([
      "Mock view name 1",
      "Mock view name 2",
    ]);
  });

  it("test unregisterView function", () => {
    viewRegistry.registerView("mock-view-1", mockViewDescriptor);
    viewRegistry.unregisterView("mock-view-1");

    expect(viewRegistry.viewsMap["mock-view-1"]).toBeUndefined();
  });
});
