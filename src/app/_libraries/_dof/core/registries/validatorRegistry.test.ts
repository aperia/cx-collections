import { validatorRegistry } from "./";
import { Validator, ValidationContext } from "..";

describe("Text Registries > validatorRegistry", () => {
  class MockValidator implements Validator<string> {
    validate(value: string, context: ValidationContext): string | undefined {
      if (value === "mock-error-value") {
        return "Mock error";
      }

      return undefined;
    }
  }

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("test registerValidator function", () => {
    validatorRegistry.registerValidator("mock-validator-1", MockValidator);

    expect(validatorRegistry.validatorsMap["mock-validator-1"]).toEqual(
      Promise.resolve(MockValidator)
    );
  });

  it("test unregisterValidator function", () => {
    validatorRegistry.registerValidator("mock-validator-1", MockValidator);

    expect(validatorRegistry.validatorsMap["mock-validator-1"]).toEqual(
      Promise.resolve(MockValidator)
    );
    validatorRegistry.unregisterValidator("mock-validator-1");
    expect(validatorRegistry.validatorsMap["mock-validator-1"]).toBeUndefined();
  });
});
