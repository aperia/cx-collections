import {
  IChangeFuncRegistry,
  ValueChangeMap,
  IValueChangeFunc
} from "../interfaces/dynamic";

class DefaultValueChangeRegistry implements IChangeFuncRegistry {
  valueChangeFuncMap: ValueChangeMap = {};

  register(
    valueChangeFuncName: string,
    handleValueChange: IValueChangeFunc
  ): void {
    this.valueChangeFuncMap[valueChangeFuncName] = handleValueChange;
  }

  resolve(valueChangeFuncName: string): IValueChangeFunc {
    return this.valueChangeFuncMap[valueChangeFuncName];
  }
}

const valueChangeRegistry: IChangeFuncRegistry = new DefaultValueChangeRegistry();

export default valueChangeRegistry;
