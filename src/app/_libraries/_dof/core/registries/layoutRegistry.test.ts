import { layoutRegistry } from "./";

describe("Text Registries > layoutRegistry", () => {
  const mockLayoutComponent = {
    displayName: "Layout component",
  } as React.FunctionComponent;

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("test registerLayout function", () => {
    layoutRegistry.registerLayout("mock-layout-1", mockLayoutComponent);

    expect(layoutRegistry.layoutsMap["mock-layout-1"]).toEqual(
      Promise.resolve(mockLayoutComponent)
    );
  });

  it("test unregisterLayout function", () => {
    layoutRegistry.registerLayout("mock-layout-1", mockLayoutComponent);
    expect(layoutRegistry.layoutsMap["mock-layout-1"]).toEqual(
      Promise.resolve(mockLayoutComponent)
    );

    layoutRegistry.unregisterLayout("mock-layout-1");
    expect(layoutRegistry.layoutsMap["mock-layout-1"]).toBeUndefined();
  });
});
