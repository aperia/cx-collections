import React, { useContext, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import RegisteredComponentsContext from '../contexts/RegisteredComponentsContext';

import { AppState } from '../redux/states';
import { resolveControl } from '../redux/actions';

export interface UseControlHook {
    (controlKey: string | React.ComponentType<any> | undefined): React.ComponentType<any> | undefined;
}

const useControl: UseControlHook = (controlKey: string | React.ComponentType<any> | undefined) => {
    const cachedControl = useSelector<AppState, React.ComponentType<any> | undefined>(
        state => (state.caches.controls && typeof controlKey === 'string' && state.caches.controls[controlKey]) || undefined,
        (prev, next) => (prev === undefined && next === undefined) || (prev === next)
    );

    const registeredComponents = useContext(RegisteredComponentsContext);
    const dispatch = useDispatch();
    useEffect(() => {
        if (cachedControl === undefined && typeof controlKey === 'string' && registeredComponents !== undefined) {
            const { 
                registeredControls: { 
                    [controlKey]: registeredControl 
                } = {} 
            } = registeredComponents;
            dispatch(resolveControl(controlKey, registeredControl));
        }
    }, [controlKey, cachedControl, registeredComponents, dispatch]);

    return typeof controlKey === 'string' ? cachedControl : controlKey;
}

export default useControl;