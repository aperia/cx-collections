import { renderHook } from "@testing-library/react-hooks";
import { useControl } from "./";
import * as reactRedux from "react-redux";
import React from "react";

jest.mock("../redux/actions", () => ({
  resolveControl: (
    controlKey: string | React.ComponentType<any> | undefined
  ) => {
    return controlKey;
  },
}));

const mockUseDispatch = jest.spyOn(reactRedux, "useDispatch");
const mockUseSelector = jest.spyOn(reactRedux, "useSelector");
const mockUseContext = jest.spyOn(React, "useContext");

describe("Test hooks > useControl.ts", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it("should dispatch action resolveControl when controlKey not exist", () => {
    const dispatchFn = jest.fn();
    mockUseDispatch.mockReturnValue(dispatchFn);
    mockUseSelector.mockImplementation((selector, equalityFn) => {
      selector({ caches: { controls: undefined } });
      equalityFn!(undefined, undefined);
    });
    mockUseContext.mockReturnValue({
      registeredControls: undefined,
    });
    const { result } = renderHook(() => useControl("control key mock"));

    expect(result.current).toBeUndefined();
    expect(dispatchFn).toHaveBeenCalledWith("control key mock");
  });
  it("should return cachedControl controlKey exist", () => {
    const dispatchFn = jest.fn();
    const reactComponent = jest.fn();
    mockUseDispatch.mockReturnValue(dispatchFn);
    mockUseSelector.mockImplementation((selector) =>
      selector({
        caches: {
          controls: {
            ["control key mock"]: reactComponent,
          },
        },
      })
    );
    const { result } = renderHook(() => useControl("control key mock"));

    expect(result.current).toEqual(reactComponent);
    expect(dispatchFn).not.toHaveBeenCalledWith("control key mock");
  });
  it("should return controlKey when controlKey's type is not string", () => {
    const dispatchFn = jest.fn();
    const reactComponent = jest.fn();
    const controlComponent = jest.fn();
    mockUseDispatch.mockReturnValue(dispatchFn);
    mockUseSelector.mockImplementation((selector, equalityFn) => {
      selector({
        caches: {
          controls: {
            ["control key mock"]: reactComponent,
          },
        },
      });
      equalityFn!("key 1", "key 1");
    });
    const { result } = renderHook(() => useControl(controlComponent));

    expect(result.current).toEqual(controlComponent);
  });
});
