import { renderHook } from '@testing-library/react-hooks';
import React from 'react';
import * as reactRedux from 'react-redux';
import { useView } from './';

jest.mock('../redux/actions', () => ({
  resolveView: (viewIdOrDescriptor: string) => {
    return viewIdOrDescriptor;
  },
  resolveViewSuccess: (viewIdOrDescriptor: string) => {
    return viewIdOrDescriptor;
  }
}));

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const mockUseSelector = jest.spyOn(reactRedux, 'useSelector');
const mockUseContext = jest.spyOn(React, 'useContext');

describe('Test hooks > useView.ts', () => {
  afterEach(() => {
    jest.clearAllMocks();
  });
  it('should return undefined if view id is promise', () => {
    const dispatchFn = jest.fn();
    mockUseContext.mockReturnValue({
      registeredViews: {
        ['view id mock']: {
          isCached: false
        }
      }
    });
    mockUseDispatch.mockReturnValue(dispatchFn);
    mockUseSelector.mockImplementation((selector, equalityFn) => {
      selector({});
      equalityFn!(undefined, undefined);
    });
    const { result } = renderHook(() =>
      useView(Promise.resolve({ name: 'mock view name' }))
    );
    expect(result.current).toBeUndefined();
    // expect(dispatchFn).toHaveBeenCalledWith('mock view name');
    const { result: result1 } = renderHook(() =>
      useView(Promise.reject('view id promise error'))
    );
    expect(result1.current).toBeUndefined();
  });

  it('should return cachedViewDescriptor if view id is object with name', () => {
    const dispatchFn = jest.fn();
    mockUseContext.mockReturnValue({
      registeredViews: {
        ['view id mock']: {
          isCached: false
        }
      }
    });
    mockUseDispatch.mockReturnValue(dispatchFn);
    mockUseSelector.mockImplementation(selector => selector({}));
    const { result } = renderHook(() => useView({ name: 'view id mock' }));
    expect(result.current).not.toBeUndefined();
  });

  it('should return cachedViewDescriptor if view id is string', () => {
    const dispatchFn = jest.fn();
    mockUseContext.mockReturnValue({
      registeredViews: {
        ['view id mock']: {
          isCached: true
        }
      }
    });
    mockUseDispatch.mockReturnValue(dispatchFn);
    mockUseSelector.mockImplementation(selector =>
      selector({
        caches: {
          views: {
            ['view id mock']: {
              name: 'view name mock'
            }
          }
        }
      })
    );
    const { result } = renderHook(() => useView('view id mock'));
    expect(result.current).not.toBeUndefined();
    mockUseContext.mockReturnValue({
      registeredViews: {}
    });
    const { result: result1 } = renderHook(() =>
      useView('view id mock')
    ) as any;
    expect(result1.current['name']).toEqual('view name mock');
    const { result: result2 } = renderHook(() =>
      useView('view id mock not exist')
    ) as any;
    expect(result2.current).toBeUndefined();
  });

  it('should dispatch resolveView if cachedViewDescriptor is undefined & registeredView exists', () => {
    const dispatchFn = jest.fn();
    mockUseContext.mockReturnValue({
      registeredViews: {
        ['view id mock']: {
          isCached: true
        }
      }
    });
    mockUseDispatch.mockReturnValue(dispatchFn);
    mockUseSelector.mockImplementation((selector, equalityFn) => {
      selector({
        caches: {
          views: {}
        }
      });
      equalityFn!('key 1', 'key 1');
    });
    const { result } = renderHook(() => useView('view id mock'));
    expect(result.current).toBeUndefined();
  });
});
