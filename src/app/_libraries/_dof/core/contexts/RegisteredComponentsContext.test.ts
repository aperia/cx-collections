import RegisteredComponentsContext from "./RegisteredComponentsContext";
import { renderHook } from "@testing-library/react-hooks";
import { useContext } from "react";

describe("Test contexts > RegisteredComponentsContext.ts", () => {
  it("should return default context data", () => {
    const {
      result: { current: mockContext },
    } = renderHook(() => useContext(RegisteredComponentsContext));

    expect(mockContext.registeredControls).not.toBeNull();
    expect(mockContext.registeredFields).not.toBeNull();
    expect(mockContext.registeredHighOrderValidationFactories).not.toBeNull();
    expect(mockContext.registeredLayouts).not.toBeNull();
    expect(mockContext.registeredViews).not.toBeNull();
    expect(mockContext.registeredDataSource).not.toBeNull();
  });
});
