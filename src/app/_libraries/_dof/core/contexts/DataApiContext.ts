import { createContext } from 'react';

export interface DataApiRequest {
    method: 'DELETE' | 'GET' | 'HEAD' | 'PATCH' | 'POST' | 'PUT';
    headers: Record<string, string>[];
    requestData: any;
}

export interface DataApiMethod {
    (url: string): DataApiRequest;
}

export interface DataApiContextData {
    [methodName: string]: DataApiMethod;
}

const DataApiContext = createContext<DataApiContextData | undefined>(undefined);

export default DataApiContext;
