import { combineReducers, AnyAction, Reducer } from 'redux';

import { 
    ResolveControlFailureAction, 
    ResolveControlSuccessAction,
    ResolveViewFailureAction,
    ResolveViewSuccessAction
} from './actions';
import { 
    RESOLVE_CONTROL_FAILURE, 
    RESOLVE_CONTROL_SUCCESS,
    RESOLVE_VIEW_FAILURE,
    RESOLVE_VIEW_SUCCESS
} from './actionTypes';
import { AppCaches, RegisteredControlsCache, RegisteredViewsCache } from './states';

type ControlsCacheActions = ResolveControlFailureAction | ResolveControlSuccessAction;

const controls: Reducer<RegisteredControlsCache, ControlsCacheActions> = (state = {}, action) => {
    const { controlKey } = action;
    switch (action.type) {
        case RESOLVE_CONTROL_FAILURE:
            const { error } = action;
            console.log(`Error occurred while resolving control ${controlKey}: `, error);
            return state;

        case RESOLVE_CONTROL_SUCCESS:
            const { controlType } = action;
            return {
                ...state,
                [controlKey]: controlType
            };

        default:
            return state;
    }
}

type ViewsCacheActions = ResolveViewFailureAction | ResolveViewSuccessAction;

const views: Reducer<RegisteredViewsCache, ViewsCacheActions> = (state = {}, action) => {
    const { viewId } = action;
    switch (action.type) {
        case RESOLVE_VIEW_FAILURE:
            const { error } = action;
            console.log(`Error occurred while resolving control ${viewId}: `, error);
            return state;

        case RESOLVE_VIEW_SUCCESS:
            const { viewDescriptor } = action;
            return {
                ...state,
                [viewId]: viewDescriptor
            };

        default: 
            return state;
    }
}

export const caches: Reducer<AppCaches, AnyAction> = combineReducers({
    controls,
    views
});
