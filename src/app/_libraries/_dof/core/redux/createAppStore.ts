import {
  applyMiddleware,
  combineReducers,
  createStore,
  AnyAction,
  ReducersMapObject,
  Store,
  Reducer,
} from "redux";
import { reducer as form } from "redux-form";

import { AppState } from "./states";
import { caches } from "./reducers";

import dynamicMiddleware from "./dynamicMiddleware";

export function createAppStore(): Store {
  return createStore<any, AnyAction, unknown, unknown>(
    createReducer(),
    applyMiddleware(dynamicMiddleware.enhancer)
  );
}

export function createReducer(
  customReducers?: ReducersMapObject<any, AnyAction>
): Reducer {
  return combineReducers<AppState, AnyAction>({
    ...(customReducers || {}),
    form,
    caches,
  });
}
export function addReducersToStore(
  appStore: Store,
  reducers?: ReducersMapObject<any, AnyAction>
) {
  reducers && appStore.replaceReducer(createReducer(reducers));
}

const store = createAppStore();
export default store;
