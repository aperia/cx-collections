import store from "./createAppStore";
import dynamicMiddlewareInstance from "./dynamicMiddleware";
import * as Redux from "redux";
import thunk from "redux-thunk";

const mockCompose = jest.spyOn(Redux, "compose");

describe("Text Redux > dynamicMiddleware", () => {
  const OLD_ENV = process.env;

  beforeEach(() => {
    jest.resetModules();
    process.env = { ...OLD_ENV };
  });

  afterEach(() => {
    process.env = OLD_ENV;
  });

  it("test function dynamicMiddlewareInstance > enhancer", () => {
    const mockAction: Redux.AnyAction = {
      data: "mock-data",
      type: "mock-type",
    };

    mockCompose.mockReturnValue(() => (action: Redux.AnyAction) => action);
    const mockStore = Object.assign({}, store);
    const { enhancer } = dynamicMiddlewareInstance;
    const enhancerHOC = enhancer(mockStore)({})(mockAction);

    expect(enhancerHOC).toEqual(mockAction);
  });

  it("test function dynamicMiddlewareInstance > addMiddleware", () => {
    const { addMiddleware } = dynamicMiddlewareInstance;
    const customMiddleWareRs = addMiddleware([thunk]);

    expect(customMiddleWareRs).toBeUndefined();
  });

  it("test function dynamicMiddlewareInstance > addMiddleware > with redux-logger", () => {
    process.env.NODE_ENV = "development";
    process.env.REACT_APP_REDUX_LOGGER = "true";

    const { addMiddleware } = dynamicMiddlewareInstance;
    const customMiddleWareRs = addMiddleware();

    expect(customMiddleWareRs).toBeUndefined();
  });
});
