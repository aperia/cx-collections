import '@testing-library/jest-dom';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import React from 'react';
import * as reactRedux from 'react-redux';
import { Provider } from 'react-redux';
import * as reduxForm from 'redux-form';
import { DataApiContext, valueChangeRegistry } from '.';
import RegisteredComponentsContext, {
  RegisteredComponentsContextData,
  RegisteredLayoutsMap,
  RegisteredViewsMap
} from './contexts/RegisteredComponentsContext';
import store from './redux/createAppStore';
import View, { ViewProps } from './View';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const mockUseSelector = jest.spyOn(reactRedux, 'useSelector');

jest.mock('./ViewElement', () => {
  const ActualReact = jest.requireActual('react');
  return {
    __esModule: true,
    default: ActualReact.forwardRef(
      (props: Record<string, any>, ref: React.Ref<any>) => {
        return (
          <div>
            {props.name}
            <input type="hidden" ref={ref} name={props.name} />
            <button
              onClick={() => {
                props?.childToParentCallback(
                  'mock-value-change-fn',
                  'new-val',
                  'pre-val',
                  props.name,
                  {}
                );
              }}
            >
              {`Mock childToParentCallback${' '}${props.name}`}
            </button>
          </div>
        );
      }
    )
  };
});

describe('Text CORE > View', () => {
  const mockLayout: React.ComponentType = ({ viewElements }: any) => {
    return (
      <div>
        <div>Layout container</div>
        {viewElements &&
          viewElements.map((v: any, index: number) => (
            <div key={index}>{v.renderedNode}</div>
          ))}
      </div>
    );
  };

  const mockField: React.ComponentType = (props: any) => {
    return <div>Field {props.name}</div>;
  };

  const dispatchFn = jest.fn();

  let originFetch: any;

  beforeEach(() => {
    jest.resetModules();
    mockUseDispatch.mockReturnValue(dispatchFn);
    mockUseSelector.mockImplementation(selector =>
      selector({
        caches: {}
      })
    );

    originFetch = window.fetch;
  });

  afterEach(() => {
    dispatchFn.mockClear();
    mockUseDispatch.mockClear();
    mockUseSelector.mockClear();
    window.fetch = originFetch;
  });

  const viewProps = {
    id: 'mock-id',
    formKey: 'mock-formkey',
    descriptor: 'view-1'
  } as ViewProps;

  const initContextValue = {
    registeredViews: {
      'view-1': {
        isCached: false,
        layout: 'mock-layout',
        fields: [
          {
            name: 'field-1'
          },
          'field-2'
        ]
      }
    } as RegisteredViewsMap,
    registeredLayouts: {
      'mock-layout': mockLayout
    } as RegisteredLayoutsMap,
    registeredFields: {
      'field-1': mockField,
      'field-2': mockField
    } as Record<string, any>
  } as RegisteredComponentsContextData;

  const renderView = (
    props?: ViewProps,
    contextValue?: Record<string, any>,
    dataApiContextValue?: Record<string, any>
  ): RenderResult => {
    const mergeProps = {
      ...initContextValue,
      ...contextValue
    };
    return render(
      <Provider store={store}>
        <RegisteredComponentsContext.Provider value={mergeProps}>
          <DataApiContext.Provider value={dataApiContextValue}>
            <View ref={() => {}} {...viewProps} {...props} />
          </DataApiContext.Provider>
        </RegisteredComponentsContext.Provider>
      </Provider>
    );
  };

  const mockContextValue = (...props: Record<string, any>[]) => {
    const mergeProps = props.reduceRight(
      (pre, curr) => ({ ...pre, ...curr }),
      {}
    );
    return {
      registeredViews: {
        'view-1': {
          isCached: false,
          layout: 'mock-layout',
          fields: [
            {
              name: 'field-1'
            },
            'field-2'
          ]
        }
      },
      registeredLayouts: {
        'mock-layout': mockLayout
      },
      registeredFields: {
        'field-1': mockField,
        'field-2': mockField
      },
      ...mergeProps
    };
  };

  it('Render View with view name not in all views', () => {
    const wrapper = renderView(viewProps, {
      registeredViews: {
        ['view-1']: {
          isCached: false
        }
      },
      allViews: ['view-2', 'view-3']
    });
    expect(wrapper.getByText(/View view-1 is not found!/i));
  });

  it('Render empty View with view name not in view registry and views context', () => {
    const wrapper = renderView(
      {
        ...viewProps,
        descriptor: {
          name: 'view-1'
        }
      },
      {
        registeredViews: {}
      }
    );
    expect(wrapper.baseElement.firstChild).toBeEmptyDOMElement();
  });

  it('Render View > view name & layout exists', () => {
    const wrapper = renderView({
      ...viewProps,
      dataTestId: 'test-id-mock',
      value: {
        field1: 'mock-value-1'
      }
    });
    expect(wrapper.getByText('Layout container')).toBeInTheDocument();
    expect(wrapper.getByText('field-1')).toBeInTheDocument();

    wrapper.unmount();

    const wrapper1 = renderView(viewProps, {
      registeredViews: {
        'view-1': {
          isCached: false,
          layout: {
            name: mockLayout
          }
        }
      }
    });
    expect(wrapper1.getByText('Layout container')).toBeInTheDocument();
  });

  it('Render View > view with dataApi url & GET request ', async () => {
    const fetchFn = jest.fn();

    window.fetch = fetchFn.mockResolvedValue({
      json: jest.fn().mockResolvedValue({
        field1: 'mock-value-1'
      })
    } as any);
    renderView(
      {
        ...viewProps,
        value: undefined
      },
      {
        registeredViews: {
          'view-1': {
            ...initContextValue.registeredViews['view-1'],
            dataApi: {
              url: 'mock-url'
            }
          }
        }
      }
    );
    expect(fetchFn).toHaveBeenCalledWith('mock-url', {
      headers: {},
      method: 'GET'
    });
  });

  it('Render View > view with dataApi url & custom HEAD request ', async () => {
    const fetchFn = jest.fn();
    const contextValue = mockContextValue();

    window.fetch = fetchFn.mockResolvedValue({
      json: jest.fn().mockResolvedValue({
        field1: 'mock-value-1'
      })
    } as any);
    renderView(
      {
        ...viewProps,
        value: undefined
      },
      {
        registeredViews: {
          'view-1': {
            ...contextValue.registeredViews['view-1'],
            dataApi: {
              url: 'mock-url',
              customRequest: 'getMock'
            }
          }
        }
      },
      {
        getMock: () => ({
          method: 'HEAD',
          headers: [
            {
              Accept: 'application/json',
              'Content-Type': 'application/json'
            }
          ]
        })
      }
    );
    expect(fetchFn).toHaveBeenCalledWith('mock-url', {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      },
      method: 'HEAD'
    });
  });

  it('Render View > view with dataApi url & custom POST request ', async () => {
    const fetchFn = jest.fn();

    window.fetch = fetchFn.mockResolvedValue({
      json: jest.fn().mockResolvedValue({
        field1: 'mock-value-1'
      })
    } as any);
    renderView(
      {
        ...viewProps,
        value: undefined
      },
      {
        registeredViews: {
          'view-1': {
            ...initContextValue.registeredViews['view-1'],
            dataApi: {
              url: 'mock-url',
              customRequest: 'getMock'
            }
          }
        }
      },
      {
        getMock: () => ({
          method: 'POST',
          headers: [
            {
              'Content-Type': 'application/json'
            }
          ],
          requestData: {
            id: 'mock-id'
          }
        })
      }
    );
    expect(fetchFn).toHaveBeenCalledWith('mock-url', {
      body: `{"id":"mock-id"}`,
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST'
    });
  });

  it('Render View > view with dataName url & GET request ', async () => {
    const mockDataName = 'data-source-name-1';
    const fetchFn = jest.fn();

    window.fetch = fetchFn.mockResolvedValue({
      json: jest.fn().mockResolvedValue({
        field1: 'mock-value-1'
      })
    } as any);
    renderView(
      {
        ...viewProps,
        value: undefined
      },
      {
        registeredViews: {
          'view-1': {
            ...initContextValue.registeredViews['view-1'],
            dataName: mockDataName
          }
        },
        registeredDataSource: {
          [`${mockDataName}`]: {
            url: 'mock-url'
          }
        }
      },
      {}
    );
    expect(fetchFn).toHaveBeenCalledWith('mock-url', {
      headers: {},
      method: 'GET'
    });
  });

  it('Render View > view with dataName url & custom HEAD request ', async () => {
    const mockDataName = 'data-source-name-1';
    const fetchFn = jest.fn();

    window.fetch = fetchFn.mockResolvedValue({
      json: jest.fn().mockResolvedValue({
        field1: 'mock-value-1'
      })
    } as any);
    renderView(
      {
        ...viewProps,
        value: undefined
      },
      {
        registeredViews: {
          'view-1': {
            ...initContextValue.registeredViews['view-1'],
            dataName: mockDataName
          }
        },
        registeredDataSource: {
          [`${mockDataName}`]: {
            url: 'mock-url',
            customRequest: 'getMock'
          }
        }
      },
      {
        getMock: () => ({
          method: 'HEAD',
          headers: [
            {
              'Content-Type': 'application/json'
            }
          ]
        })
      }
    );
    expect(fetchFn).toHaveBeenCalledWith('mock-url', {
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'HEAD'
    });
  });

  it('Render View > view with dataName url & custom POST request ', async () => {
    const mockDataName = 'data-source-name-1';
    const fetchFn = jest.fn();

    window.fetch = fetchFn.mockResolvedValue({
      json: jest.fn().mockResolvedValue({
        field1: 'mock-value-1'
      })
    } as any);
    renderView(
      {
        ...viewProps,
        value: undefined
      },
      {
        registeredViews: {
          'view-1': {
            ...initContextValue.registeredViews['view-1'],
            dataName: mockDataName
          }
        },
        registeredDataSource: {
          [`${mockDataName}`]: {
            url: 'mock-url',
            customRequest: 'getMock'
          }
        },
        registeredLayouts: undefined
      },
      {
        getMock: () => ({
          method: 'POST',
          headers: [
            {
              'Content-Type': 'application/json'
            }
          ],
          requestData: {
            id: 'mock-id'
          }
        })
      }
    );
    expect(fetchFn).toHaveBeenCalledWith('mock-url', {
      body: `{"id":"mock-id"}`,
      headers: {
        'Content-Type': 'application/json'
      },
      method: 'POST'
    });
  });

  it('Render View > test valueChangeRegistry is called on receiveChildValue', async () => {
    const mockValueChangeFunc = jest.fn();
    valueChangeRegistry.register('mock-value-change-fn', mockValueChangeFunc);

    mockValueChangeFunc.mockImplementation((...params) => {
      const setFormValues = params[5];
      setFormValues('field-1', 'some value');
    });

    const wrapper = renderView();

    fireEvent.click(wrapper.getByText('Mock childToParentCallback field-1'));

    expect(mockValueChangeFunc).toHaveBeenCalled();
  });

  describe('Mock redux form', () => {
    it('Render View > onViewRefChange called after loading fields', async () => {
      const viewRefChangeFn = jest.fn();
      jest.spyOn(reduxForm, 'reduxForm').mockImplementation(
        ({ form: formKey, shouldValidate }: Record<string, any>) =>
          (): any =>
            React.forwardRef(
              ({ children, onFind }: any, ref: React.Ref<HTMLDivElement>) => {
                shouldValidate();
                return (
                  <div id={formKey} ref={ref}>
                    {children}
                    <button onClick={() => onFind('field-1')}>
                      Mock onFind field-1
                    </button>
                    <button onClick={() => onFind('')}>
                      Mock onFind empty
                    </button>
                  </div>
                );
              }
            )
      );
      const wrapper = renderView({
        ...viewProps,
        onViewRefChange: viewRefChangeFn
      });

      fireEvent.click(wrapper.getByText('Mock onFind field-1'));
      fireEvent.click(wrapper.getByText('Mock onFind empty'));

      expect(viewRefChangeFn).toHaveBeenCalled();
      jest.clearAllMocks();
    });
  });
});
