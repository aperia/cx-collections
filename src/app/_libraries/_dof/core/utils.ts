export function isPromise<T>(value: any): value is Promise<T> {
  return isPromiseLike(value) && typeof (value as any).catch === 'function';
}

export function isPromiseLike<T>(value: any): value is PromiseLike<T> {
  return !!value && typeof value.then === 'function';
}

export const pad = (number1: string, number2: string) => {
  const parts1 = number1.split('.'),
    parts2 = number2.split('.');

  // pad integral part
  let length1 = parts1[0].length,
    length2 = parts2[0].length;

  if (length1 > length2) {
    parts2[0] =
      new Array(Math.abs(length1 - length2) + 1).join('0') +
      (parts2[0] ? parts2[0] : '0');
  } else {
    parts1[0] =
      new Array(Math.abs(length1 - length2) + 1).join('0') +
      (parts1[0] ? parts1[0] : '0');
  }

  // pad fractional
  length1 = parts1[1] ? parts1[1].length : 0;
  length2 = parts2[1] ? parts2[1].length : 0;
  if (length1 || length2) {
    if (length1 > length2) {
      parts2[1] =
        (parts2[1] ? parts2[1] : '') +
        new Array(Math.abs(length1 - length2) + 1).join('0');
    } else {
      parts1[1] =
        (parts1[1] ? parts1[1] : '') +
        new Array(Math.abs(length1 - length2) + 1).join('0');
    }
  }

  number1 = parts1[0] + (parts1[1] ? '.' + parts1[1] : '');
  number2 = parts2[0] + (parts2[1] ? '.' + parts2[1] : '');

  return [number1, number2];
};

export const compareTo = (number1: string, number2: string) => {
  let negative = false;
  if (number1[0] == '-' && number2[0] != '-') {
    return -1;
  } else if (number1[0] != '-' && number2[0] == '-') {
    return 1;
  } else if (number1[0] == '-' && number2[0] == '-') {
    number1 = number1.substr(1);
    number2 = number2.substr(1);
    negative = true;
  }

  [number1, number2] = pad(number1, number2);
  if (number1.localeCompare(number2) == 0) {
    return 0;
  }
  for (let i = 0; i < number1.length; i++) {
    if (number1[i] == number2[i]) {
      continue;
    } else if (number1[i] > number2[i]) {
      if (negative) {
        return -1;
      } else {
        return 1;
      }
    } else {
      if (negative) {
        return 1;
      } else {
        return -1;
      }
    }
  }

  return 0;
};
