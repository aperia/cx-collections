import { HighOrderFunction } from "../interfaces/highOrder";

/**
 * Represents a validation error which occurs when an invalid
 * value is being set to a property or an object.
 */
export class ValidationError extends Error {
  constructor(message?: string) {
    super(message);
    this.name = "ValidationError";

    // To prevent unit tests fail due to wrong error class
    Object.setPrototypeOf(this, ValidationError.prototype);
  }
}

/**
 * Represents the context of a validation process for a dynamic view. The validation context
 * holds some contextual information such as values of the other fields on the same dynamic view.
 */
export interface ValidationContext {
  /**
   * Gets label of a field on the current dynamic view.
   * @param fieldIdOrDataField The ID of the dynamic field or the property path to
   * the data field of the model of the view.
   * @returns {string} The label of the field.
   */
  labelOf(fieldIdOrDataField: string): string | undefined;

  /**
   * Gets value of a field on the current dynamic view.
   * @template TValue Type of the value.
   * @param fieldIdOrDataField The ID of the dynamic field or the property path to
   * the data field of the model of the view.
   * @returns {TValue} The current value of the field.
   */
  valueOf<TValue = any>(fieldIdOrDataField: string): TValue | undefined;

  /**
   * Gets required state of a field on the current dynamic view.
   * @template TValue Type of the value.
   * @returns {TValue} The current value of the field.
   */
  requiredOf?(): boolean;
}

/**
 * Represents a generic validation function.
 * @template TValue Type of the value to be validated.
 * @template TError Type of the validation errors.
 */
export interface ValidationFn<TValue, TError> {
  /**
   * @param value The value to validate.
   * @param context The validation context.
   */
  (value: TValue, context: ValidationContext): TError | undefined;
}

/**
 * Represents a validator which is responsible for validating a given value
 * against a specific validation rule.
 * @template TValue Type of the value to validate. Default to `any`.
 * @template TError Type of the validation errors. Default to `string`.
 */
export interface Validator<TValue = any, TError = string> {
  /**
   * Validates the given value against a specific validation rule.
   * @param value The value to validate.
   * @param context The validation context.
   * @returns {TError | undefined} The validation error if any. Otherwise `undefined`.
   */
  validate(
    value: TValue | null | undefined,
    context: ValidationContext
  ): TError | undefined;
}

/**
 * Represents a base class for all validation options.
 */
export interface ValidationOptions<TError = string> {
  /**
   * The customized error message.
   */
  errorMsg?: TError;
}

/**
 * The base option class for any cross-fields validation.
 */
export interface CrossFieldsValidationOptions<TError = string>
  extends ValidationOptions<TError> {
  fieldToCompare?: string;
}

/**
 * Represents a class constructor of a validator.
 * @template TOptions Type of the options which are used to initialize the validator.
 * @template TValue Type of the value to validate.
 * @template TError Type of the validation error.
 */
export interface ValidatorConstructor<
  TOptions extends ValidationOptions<TError> = ValidationOptions<any>,
  TValue = any,
  TError = string
> {
  new (options?: TOptions): Validator<TValue, TError>;
}

/**
 * Represents a collection of validation messages when validating an object.
 * @template TObject Type of the object. Default to `unknown` if omitted.
 * @template TMessage Type of the validation message. Default to `string` if omitted.
 */
export type ValidationErrors<TObject = unknown, TMessage = string> = {
  /**
   * Gets or sets the validation message for each property of the object.
   */
  [P in keyof TObject]: TMessage;
};

/**
 * Represents a high-order validation function.
 * @template TValue Type of the value to validate. Default to `any`.
 * @template TError Type of the error message as a result of the validation function. Default to `string`.
 */
export type HighOrderValidation<
  TValue = any,
  TError = string
> = HighOrderFunction<ValidationFn<TValue | null | undefined, TError>>;

/**
 * Represents a factory method which produces a high-order validation function.
 * @template TValidationOptions Type of the options to customize the validation function. Default to `never`, which means no option.
 * @template TValue Type of the value to validate. Default to `any`.
 * @template TError Type of the error message as a result of the validation function. Default to `string`.
 */
export interface HighOrderValidationFactory<
  TOptions extends ValidationOptions<TError> = never,
  TValue = any,
  TError = string
> {
  /**
   * @param options Provides properties and values to customize the validation function.
   * @returns {HighOrderValidation<TValue, TError>} The customized high-order validation function.
   */
  (options: TOptions): HighOrderValidation<TValue, TError>;
}
