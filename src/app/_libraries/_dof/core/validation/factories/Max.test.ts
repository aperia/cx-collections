import Max from "./Max";
import NoValidation from "../NoValidation";
import { ValidationContext } from "../interfaces";

describe("Max(number)", function () {
  const context: ValidationContext = {
    labelOf(): string | undefined {
      return undefined;
    },
    valueOf() {
      return undefined;
    },
  };

  it("should return a validation message when a number value is greater than the given maximum value.", function () {
    const validationFn = Max({ maxValue: 10 })(NoValidation);
    const message = validationFn(20, context);
    expect(message).not.toBeUndefined();
  });

  it("should NOT return a validation message when a number value is equal to the given maximum value.", function () {
    const validationFn = Max({ maxValue: 10 })(NoValidation);
    const message = validationFn(10, context);
    expect(message).toBeUndefined();
  });

  it("should NOT return a validation message when a number value is less than the given maximum value.", function () {
    const validationFn = Max({ maxValue: 10 })(NoValidation);
    const message = validationFn("0", context);
    expect(message).toBeUndefined();
  });

  it("should return a validation message when a STRING number value is greater than the given maximum value.", function () {
    const validationFn = Max({ maxValue: 10 })(NoValidation);
    const message = validationFn("20", context);
    expect(message).not.toBeUndefined();
  });

  it("should NOT return a validation message when a number value is undefined.", function () {
    const validationFn = Max(undefined as any)(NoValidation);
    const message = validationFn(20, context);
    expect(message).toBeUndefined();
  });
});
