import GreaterThan from "./GreaterThan";
import NoValidation from "../NoValidation";
import { ValidationContext } from "../interfaces";

describe("GreaterThan", function () {
  const context: ValidationContext = {
    labelOf(): string | undefined {
      return undefined;
    },
    valueOf(_anyField: string): any {
      return 100;
    },
  };

  it("should return a validation message when the given value is less than the value of the given field.", function () {
    const validationFn = GreaterThan({ fieldToCompare: "otherField" })(
      NoValidation
    );
    const message = validationFn(20, context);
    expect(message).not.toBeUndefined();
  });

  it("should return a validation message when the given value is equal to the value of the given field.", function () {
    const validationFn = GreaterThan({ fieldToCompare: "otherField" })(
      NoValidation
    );
    const message = validationFn(100, context);
    expect(message).not.toBeUndefined();
  });

  it("should NOT return a validation message when the given value is greater than the value of the given field.", function () {
    const validationFn = GreaterThan({ fieldToCompare: "otherField" })(
      NoValidation
    );
    const message = validationFn(200, context);
    expect(message).toBeUndefined();
  });

  it("should NOT return a validation message when the given value is undefined.", function () {
    const validationFn = GreaterThan(undefined as any)(NoValidation);
    const message = validationFn(200, context);
    expect(message).toBeUndefined();
  });
});
