import { HighOrderValidationFactory, ValidationOptions } from "../interfaces";
import OfType from "./OfType";

export interface LengthValidationOptions<TError = string>
  extends ValidationOptions<TError> {
  length?: number;
}

const Phone: HighOrderValidationFactory<LengthValidationOptions, string> = ({
  length,
  errorMsg = `Value must be a text that has ${length} letter(s).`,
} = {}) => (next) =>
  OfType<string>(
    errorMsg,
    "string"
  )((value, context) => {
    const rawValue = value?.replace(/[()_ -]/g, "") || "";
    return value !== null &&
      value !== undefined &&
      rawValue !== "" &&
      length !== undefined &&
      rawValue.length !== length
      ? errorMsg
      : next(value, context);
  });

export default Phone;
