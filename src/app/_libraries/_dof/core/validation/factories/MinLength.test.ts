import MinLength from "./MinLength";
import NoValidation from "../NoValidation";
import { ValidationContext } from "../interfaces";

describe("MinLength(number)", function () {
  const context: ValidationContext = {
    labelOf(): string | undefined {
      return undefined;
    },
    valueOf() {
      return undefined;
    },
  };

  it("should return a validation message when a text value has less letters than the given minimum length.", function () {
    const validationFn = MinLength({ minLength: 5 })(NoValidation);
    const message = validationFn("1234", context);
    expect(message).not.toBeUndefined();
  });

  it("should NOT return a validation message when a text value has number of letters equal to the given minimum length.", function () {
    const validationFn = MinLength({ minLength: 5 })(NoValidation);
    const message = validationFn("12345", context);
    expect(message).toBeUndefined();
  });

  it("should NOT return a validation message when a text value has more letters than the given minimum length.", function () {
    const validationFn = MinLength({ minLength: 5 })(NoValidation);
    const message = validationFn("123456789", context);
    expect(message).toBeUndefined();
  });

  it("should NOT return a validation message when options is undefined.", function () {
    const validationFn = MinLength(undefined as any)(NoValidation);
    const message = validationFn("123456789", context);
    expect(message).toBeUndefined();
  });
});
