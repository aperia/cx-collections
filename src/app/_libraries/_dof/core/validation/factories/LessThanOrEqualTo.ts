import { CrossFieldsValidationOptions, HighOrderValidationFactory } from '../interfaces';

const LessThanOrEqualTo: HighOrderValidationFactory<CrossFieldsValidationOptions, any> = ({ 
    fieldToCompare, 
    errorMsg
} = {}) => (next) => (value, context) => 
    !!value && !!fieldToCompare && value > context.valueOf(fieldToCompare) 
        ? (errorMsg || `Value must be less than or equal to field ${context.labelOf(fieldToCompare)}.`) 
        : next(value, context);

export default LessThanOrEqualTo;
