import MaxLength from "./MaxLength";
import NoValidation from "../NoValidation";
import { ValidationContext } from "../interfaces";

describe("MaxLength(number)", function () {
  const context: ValidationContext = {
    labelOf(): string | undefined {
      return undefined;
    },
    valueOf() {
      return undefined;
    },
  };

  it("should return a validation message when a text value has more letters than the given maximum length.", function () {
    const validationFn = MaxLength({ maxLength: 10 })(NoValidation);
    const message = validationFn("1234567890123456789", context);
    expect(message).not.toBeUndefined();
  });

  it("should NOT return a validation message when a text value has number of letters equal to the given maximum length.", function () {
    const validationFn = MaxLength({ maxLength: 10 })(NoValidation);
    const message = validationFn("1234567890", context);
    expect(message).toBeUndefined();
  });

  it("should NOT return a validation message when a text value has less letters than the given maximum length.", function () {
    const validationFn = MaxLength({ maxLength: 10 })(NoValidation);
    const message = validationFn("123456789", context);
    expect(message).toBeUndefined();
  });

  it("should NOT return a validation message when options is undefined.", function () {
    const validationFn = MaxLength(undefined as any)(NoValidation);
    const message = validationFn("1234567890123456789", context);
    expect(message).toBeUndefined();
  });
});
