import OfType from "./OfType";
import NoValidation from "../NoValidation";
import { ValidationContext } from "../interfaces";

describe("OfType", function () {
  const context: ValidationContext = {
    labelOf(): string | undefined {
      return undefined;
    },
    valueOf() {
      return undefined;
    },
  };

  it("should return a validation message when value type is different with input type", function () {
    const validationFn = OfType<boolean>(
      "Mock error msg",
      "string"
    )(NoValidation);
    const message = validationFn(true, context);
    expect(message).toBe("Mock error msg");
  });

  it("should return a validation message when value type is function", function () {
    class MockClass {
      constructor() {}
    }

    const validationFn = OfType<string>(
      "Mock error msg",
      MockClass
    )(NoValidation);
    const message = validationFn("some mock string", context);
    expect(message).toBe("Mock error msg");
  });

  it("should return a default message when value type is different with input type", function () {
    const validationFn = OfType<string>(undefined, "boolean")(NoValidation);
    const message = validationFn("mock string", context);
    expect(message).toBe(
      "Expected value to be one of types [boolean] but got string."
    );
  });
});
