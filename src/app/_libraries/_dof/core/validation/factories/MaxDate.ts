import { HighOrderValidationFactory, ValidationOptions } from '../interfaces';
import OfType from './OfType';

export interface MaxDateValidationOptions<TError = string> extends ValidationOptions<TError> {
    maxValue?: Date;
}

const MaxDate: HighOrderValidationFactory<MaxDateValidationOptions, Date> = ({ 
    maxValue, 
    errorMsg = `Value must be a date that is equal to or less than ${maxValue}.`
} = {}) => (next) => 
    OfType<Date>(errorMsg, Date)((value, context) => 
        value !== null && value !== undefined && maxValue !== undefined && value > maxValue ? errorMsg : next(value, context));

export default MaxDate;
