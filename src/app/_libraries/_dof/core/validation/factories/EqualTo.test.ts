import EqualTo from "./EqualTo";
import NoValidation from "../NoValidation";
import { ValidationContext } from "../interfaces";

describe("EqualTo", function () {
  const context: ValidationContext = {
    labelOf(): string | undefined {
      return undefined;
    },
    valueOf(_anyField: string): any {
      return 100;
    },
  };

  it("should return a validation message when the given value is NOT equal to the value of the given field.", function () {
    const validationFn = EqualTo({ fieldToCompare: "otherField" })(
      NoValidation
    );
    const message = validationFn(20, context);
    expect(message).not.toBeUndefined();
  });

  it("should NOT return a validation message when the given value is equal to the value of the given field.", function () {
    const validationFn = EqualTo({ fieldToCompare: "otherField" })(
      NoValidation
    );
    const message = validationFn(100, context);
    expect(message).toBeUndefined();
  });

  it("should NOT return a validation message when the given value is undefined", function () {
    const validationFn = EqualTo(undefined as any)(NoValidation);
    const message = validationFn(20, context);
    expect(message).toBeUndefined();
  });
});
