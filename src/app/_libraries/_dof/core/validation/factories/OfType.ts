import { PropertyType } from "../../metadata/Type";
import { HighOrderValidation } from "../interfaces";

export default function OfType<TValue = any>(
  errorMsg?: string,
  ...types: PropertyType<any>[]
): HighOrderValidation<TValue, string> {
  return (next) => (value, context) => {
    const valueType = typeof value;
    if (
      types.every(
        (type) =>
          (typeof type === "function" && !(value instanceof type)) ||
          (typeof type !== "function" && valueType !== type)
      ) &&
      (value !== undefined && value !== null)
    ) {
      const typeNames = types
        .map((type) => (typeof type === "function" ? type.name : type))
        .join(", ");
      return (
        errorMsg ||
        `Expected value to be one of types [${typeNames}] but got ${valueType}.`
      );
    }
    return next(value, context);
  };
}
