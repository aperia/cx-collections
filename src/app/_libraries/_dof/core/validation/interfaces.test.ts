import { ValidationError } from "./interfaces";

describe("validation > interface", () => {
  it("test ValidationError constructor", () => {
    const validationErr = new ValidationError("mock error");
    expect(validationErr.message).toEqual("mock error");
  });
});
