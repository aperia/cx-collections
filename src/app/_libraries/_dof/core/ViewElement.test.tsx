import '@testing-library/jest-dom';
import {
  fireEvent,
  render,
  RenderResult,
  waitFor
} from '@testing-library/react';
import React from 'react';
import * as reactRedux from 'react-redux';
import { Provider } from 'react-redux';
import { reduxForm } from 'redux-form';
import { DataApiContext, valueChangeRegistry } from '.';
import RegisteredComponentsContext, {
  RegisteredComponentsContextData,
  RegisteredFieldsMap,
  RegisteredHighOrderValidationFactoriesMap
} from './contexts/RegisteredComponentsContext';
import * as DOFHook from './hooks';
import { DataApiConfiguration, ViewLayoutElement } from './interfaces';
import store from './redux/createAppStore';
import MockLayout from './test-utils/mocks/MockLayout';
import * as builtinHighOrderValidationFactories from './validation/factories';
import ViewElement, { ViewElementProps } from './ViewElement';

const mockUseDispatch = jest.spyOn(reactRedux, 'useDispatch');
const mockUseSelector = jest.spyOn(reactRedux, 'useSelector');

jest.mock('./hooks', () => {
  const actualModule = jest.requireActual('./hooks');
  return {
    __esModule: true,
    ...actualModule
  };
});

const mockHook: any = DOFHook;

describe('Text CORE > ViewElement', () => {
  const dispatchFn = jest.fn();
  let originFetch: any;

  const mockDataSourceApi = (customRequest?: boolean) => {
    const result = {
      url: 'mock-url'
    } as DataApiConfiguration;
    if (customRequest) result.customRequest = 'getMock';
    return result;
  };

  const mockCustomRequestHEAD = {
    method: 'HEAD',
    headers: [
      {
        Accept: 'application/json',
        'Content-Type': 'application/json'
      }
    ]
  };
  const mockCustomRequestPOST = {
    method: 'POST',
    headers: [
      {
        'Content-Type': 'application/json'
      }
    ],
    requestData: {
      id: 'mock-id'
    }
  };

  const mockFieldType: React.ComponentType = (props: Record<string, any>) => {
    const { id, data = {}, label, input, setValue } = props;

    return (
      <div id={id}>
        <div>Field {props.name}</div>
        <label>{label}</label>
        <div>{data['field']}</div>
        <input
          id={`field-input-${id}`}
          className="field-input"
          type="text"
          {...input}
        />
        <button onClick={setValue}>Set value</button>
      </div>
    );
  };

  beforeEach(() => {
    jest.resetModules();
    mockUseDispatch.mockReturnValue(dispatchFn);
    mockUseSelector.mockImplementation(selector =>
      selector({
        caches: {}
      })
    );

    originFetch = window.fetch;
  });

  afterEach(() => {
    dispatchFn.mockClear();
    mockUseDispatch.mockClear();
    mockUseSelector.mockClear();
    window.fetch = originFetch;
  });

  const viewElementProps = {
    id: 'mock-id',
    name: 'field-1',
    formKey: 'mock-formkey'
  } as ViewElementProps;

  const initContextValue = {
    registeredFields: {
      'field-1': {
        dataField: 'mockData',
        label: 'Mock label',
        type: mockFieldType,
        props: {
          enable: true,
          visible: true,
          readOnly: true,
          required: true
        }
      }
    } as RegisteredFieldsMap
  } as RegisteredComponentsContextData;

  const renderElementView = (
    props?: ViewElementProps,
    contextValue?: Record<string, any>,
    dataApiContextValue?: Record<string, any>
  ): RenderResult => {
    const ReduxForm = reduxForm<any, React.PropsWithChildren<any>, string>({
      form: 'mock-form-key'
    })(() => <ViewElement ref={() => {}} {...viewElementProps} {...props} />);

    const mergeProps = {
      ...initContextValue,
      ...contextValue
    };
    return render(
      <Provider store={store}>
        <RegisteredComponentsContext.Provider value={mergeProps}>
          <DataApiContext.Provider value={dataApiContextValue}>
            <ReduxForm ref={() => {}} />
          </DataApiContext.Provider>
        </RegisteredComponentsContext.Provider>
      </Provider>
    );
  };

  const renderLayoutWithElementView = (
    formKey: string,
    fields: any[],
    contextValue?: Record<string, any>
  ): RenderResult => {
    const viewElements = fields.map<ViewLayoutElement<any>>((field, index) => ({
      name: field.name,
      renderedNode: (
        <ViewElement
          id={`mock-id_${field.name}`}
          name={field.name}
          overrides={field}
          formKey={formKey}
        />
      ),
      layoutProps: field.layoutProps
    }));

    const ReduxForm = reduxForm<any, React.PropsWithChildren<any>, string>({
      form: 'mock-form-key'
    })(() => (
      <MockLayout viewElements={viewElements} viewName="mock-view-name" />
    ));
    const mergeProps = {
      ...initContextValue,
      ...contextValue
    };
    return render(
      <Provider store={store}>
        <RegisteredComponentsContext.Provider value={mergeProps}>
          <ReduxForm ref={() => {}} />
        </RegisteredComponentsContext.Provider>
      </Provider>
    );
  };

  const builtinHOCValidationFactories: RegisteredHighOrderValidationFactoriesMap =
    {
      ...builtinHighOrderValidationFactories
    };

  it('Render ViewElement > field not found', () => {
    const wrapper = renderElementView(viewElementProps, {
      registeredFields: {}
    } as RegisteredComponentsContextData);
    expect(
      wrapper.getByText(`Field field-1 is not found!`)
    ).toBeInTheDocument();
  });

  it("Render ViewElement > field isn't visible", () => {
    const wrapper = renderElementView(viewElementProps, {
      registeredFields: {
        'field-1': {
          ...initContextValue.registeredFields['field-1'],
          props: {
            enable: true,
            visible: false
          }
        }
      }
    });

    expect(wrapper.baseElement.firstChild).toBeEmptyDOMElement();
  });

  it('Render ViewElement > with dataSourceApi url', async () => {
    const fetchFn = jest.fn();

    window.fetch = fetchFn.mockResolvedValue({
      json: jest.fn().mockResolvedValue({
        field: 'mock-value-1'
      })
    } as any);

    const wrapper = renderElementView(viewElementProps, {
      registeredFields: {
        'field-1': {
          ...initContextValue.registeredFields['field-1'],
          dataSourceApi: mockDataSourceApi()
        }
      }
    });
    fireEvent.change(wrapper.baseElement.querySelector('input.field-input')!, {
      target: { value: 'mock-value' }
    });
    fireEvent.click(wrapper.getByText('Set value'));
    expect(dispatchFn).toHaveBeenCalled();

    expect(wrapper.getByText('Mock label')).toBeInTheDocument();
    await waitFor(() => {
      expect(wrapper.getByText('mock-value-1')).toBeInTheDocument();
    });
  });

  it('Render ViewElement > with dataSourceApi & without customRequest', () => {
    const fetchFn = jest.fn();

    window.fetch = fetchFn.mockResolvedValue({
      json: jest.fn().mockResolvedValue({
        field: 'mock-value-2'
      })
    } as any);

    const wrapper = renderElementView(
      viewElementProps,
      {
        ...initContextValue,
        registeredFields: {
          'field-1': {
            ...initContextValue.registeredFields['field-1'],
            dataSourceApi: mockDataSourceApi(true)
          }
        }
      },
      {}
    );

    expect(fetchFn).not.toHaveBeenCalled();
    expect(
      wrapper.getByText('field-1: customRequest getMock is not found!')
    ).toBeInTheDocument();
  });

  it('Render ViewElement > with dataSourceApi & HEAD customRequest', async () => {
    const fetchFn = jest.fn();

    window.fetch = fetchFn.mockResolvedValue({
      json: jest.fn().mockResolvedValue({
        field: 'mock-value-2'
      })
    } as any);

    const wrapper = renderElementView(
      viewElementProps,
      {
        registeredFields: {
          'field-1': {
            ...initContextValue.registeredFields['field-1'],
            dataField: undefined,
            name: 'mockData',
            dataSourceApi: mockDataSourceApi(true)
          }
        }
      },
      {
        getMock: () => mockCustomRequestHEAD
      }
    );
    fireEvent.blur(wrapper.baseElement.querySelector('input.field-input')!);
    fireEvent.click(wrapper.getByText('Set value'));

    expect(wrapper.getByText('Mock label')).toBeInTheDocument();
    await waitFor(() => {
      expect(wrapper.getByText('mock-value-2')).toBeInTheDocument();
    });
  });

  it('Render ViewElement > with dataSourceApi & POST customRequest', async () => {
    const fetchFn = jest.fn();

    window.fetch = fetchFn.mockResolvedValue({
      json: jest.fn().mockResolvedValue({
        field: 'mock-value-3'
      })
    } as any);

    const wrapper = renderElementView(
      viewElementProps,
      {
        registeredFields: {
          'field-1': {
            ...initContextValue.registeredFields['field-1'],
            dataSourceApi: mockDataSourceApi(true)
          }
        }
      },
      { getMock: () => mockCustomRequestPOST }
    );

    expect(wrapper.getByText('Mock label')).toBeInTheDocument();
    await waitFor(() => {
      expect(wrapper.getByText('mock-value-3')).toBeInTheDocument();
    });
  });

  it('Render ViewElement > with dataSourceName exists', async () => {
    const mockDataSourceName = 'data-source-name-1';
    const fetchFn = jest.fn();

    window.fetch = fetchFn.mockResolvedValue({
      json: jest.fn().mockResolvedValue({
        field: 'mock-value-1'
      })
    } as any);

    const wrapper = renderElementView(
      viewElementProps,
      {
        registeredFields: {
          'field-1': {
            ...initContextValue.registeredFields['field-1'],
            dataSourceName: mockDataSourceName
          }
        },
        registeredDataSource: {
          [`${mockDataSourceName}`]: mockDataSourceApi()
        }
      },
      {}
    );

    expect(wrapper.getByText('Mock label')).toBeInTheDocument();
    await waitFor(() => {
      expect(wrapper.getByText('mock-value-1')).toBeInTheDocument();
    });
  });

  it('Render ViewElement > with dataSourceName does not exist', () => {
    const mockDataSourceName = 'data-source-name-1';
    const fetchFn = jest.fn();

    window.fetch = fetchFn.mockResolvedValue({
      json: jest.fn().mockResolvedValue({
        field: 'mock-value-1'
      })
    } as any);

    const wrapper = renderElementView(
      viewElementProps,
      {
        registeredFields: {
          'field-1': {
            ...initContextValue.registeredFields['field-1'],
            dataSourceName: mockDataSourceName
          }
        },
        registeredDataSource: {
          [`${mockDataSourceName}`]: undefined
        }
      },
      {}
    );

    expect(fetchFn).not.toHaveBeenCalled();
    expect(
      wrapper.getByText(
        `field-1: DataSourceName ${mockDataSourceName} is not found!`
      )
    ).toBeInTheDocument();
  });

  it('Render ViewElement > with dataSourceName & HEAD customRequest', async () => {
    const mockDataSourceName = 'data-source-name-1';
    const fetchFn = jest.fn();

    window.fetch = fetchFn.mockResolvedValue({
      json: jest.fn().mockResolvedValue({
        field: 'mock-value-2'
      })
    } as any);

    const wrapper = renderElementView(
      viewElementProps,
      {
        registeredFields: {
          'field-1': {
            ...initContextValue.registeredFields['field-1'],
            dataSourceName: mockDataSourceName
          }
        },
        registeredDataSource: {
          [`${mockDataSourceName}`]: mockDataSourceApi(true)
        }
      },
      { getMock: () => mockCustomRequestHEAD }
    );

    await waitFor(() => {
      expect(wrapper.getByText('mock-value-2')).toBeInTheDocument();
    });
  });

  it('Render ViewElement > with dataSourceName & POST customRequest', async () => {
    const mockDataSourceName = 'data-source-name-1';
    const fetchFn = jest.fn();

    window.fetch = fetchFn.mockResolvedValue({
      json: jest.fn().mockResolvedValue({
        field: 'mock-value-2'
      })
    } as any);

    const wrapper = renderElementView(
      viewElementProps,
      {
        registeredFields: {
          'field-1': {
            ...initContextValue.registeredFields['field-1'],
            dataSourceName: mockDataSourceName
          }
        },
        registeredDataSource: {
          [`${mockDataSourceName}`]: mockDataSourceApi(true)
        }
      },
      {
        getMock: () => mockCustomRequestPOST
      }
    );

    await waitFor(() => {
      expect(wrapper.getByText('mock-value-2')).toBeInTheDocument();
    });
  });

  it('Render ViewElement > handle onChange > onValueChangedFunc does not exist', () => {
    const wrapper = renderElementView(viewElementProps, {
      registeredFields: {
        'field-1': {
          ...initContextValue.registeredFields['field-1'],
          onValueChangedFunc: 'mockValueChangeFn'
        }
      }
    });
    fireEvent.change(wrapper.baseElement.querySelector('input.field-input')!, {
      target: { value: 'mock-value' }
    });
    expect(
      wrapper.getByText(
        `field-1: ValueChangeFunc mockValueChangeFn is not found!`
      )
    ).toBeInTheDocument();
  });

  it('Render ViewElement > handle onChange > onValueChangedFunc exists', () => {
    const childToParentCallbackFn = jest.fn();
    valueChangeRegistry.register('mockValueChangeFn', jest.fn());

    const wrapper = renderElementView(
      {
        ...viewElementProps,
        childToParentCallback: childToParentCallbackFn
      },
      {
        registeredFields: {
          'field-1': {
            ...initContextValue.registeredFields['field-1'],
            onValueChangedFunc: 'mockValueChangeFn'
          }
        }
      }
    );
    fireEvent.change(wrapper.baseElement.querySelector('input.field-input')!, {
      target: { value: 'mock-value' }
    });
    expect(childToParentCallbackFn).toHaveBeenCalled();
  });

  it('Render ViewElement > handle onBlur > onBlurFunc does not exist', () => {
    const wrapper = renderElementView(viewElementProps, {
      registeredFields: {
        'field-1': {
          ...initContextValue.registeredFields['field-1'],
          onBlurFunc: 'mockBlurFn'
        }
      }
    });
    fireEvent.blur(wrapper.baseElement.querySelector('input.field-input')!);
    expect(
      wrapper.getByText(`field-1: ValueChangeFunc is not found!`)
    ).toBeInTheDocument();
  });

  it('Render ViewElement > handle onBlur > onBlurFunc does not exist', () => {
    const childToParentCallbackFn = jest.fn();
    valueChangeRegistry.register('mockBlurFn', jest.fn());

    const wrapper = renderElementView(
      {
        ...viewElementProps,
        childToParentCallback: childToParentCallbackFn
      },
      {
        registeredFields: {
          'field-1': {
            ...initContextValue.registeredFields['field-1'],
            onBlurFunc: 'mockBlurFn'
          }
        }
      }
    );
    fireEvent.blur(wrapper.baseElement.querySelector('input.field-input')!);

    expect(childToParentCallbackFn).toHaveBeenCalled();
  });

  const layoutFields = [
    {
      dataField: 'mockData',
      label: 'Mock label',
      type: mockFieldType,
      name: 'field-1',
      validationRules: [
        {
          ruleName: 'Required'
        },
        {
          ruleName: 'EqualTo',
          ruleOptions: { fieldToCompare: 'field-2' }
        }
      ],
      props: {
        enable: true,
        readOnly: false
      }
    },
    {
      dataField: 'mockData1',
      label: undefined,
      type: mockFieldType,
      name: 'field-2',
      validationRules: [
        {
          ruleName: 'EqualTo',
          ruleOptions: { fieldToCompare: 'field-1' }
        }
      ],
      props: {
        enable: true,
        readOnly: false
      }
    }
  ];

  it('Render ViewElement > with validation rules', () => {
    const wrapper = renderLayoutWithElementView(
      viewElementProps.formKey,
      layoutFields,
      {
        registeredFields: {
          'field-1': layoutFields[0],
          'field-2': layoutFields[1]
        },
        registeredHighOrderValidationFactories: builtinHOCValidationFactories
      }
    );

    const inputField1 = wrapper.baseElement.querySelector(
      'input#field-input-mock-id_field-1'
    ) as HTMLInputElement;
    const inputField2 = wrapper.baseElement.querySelector(
      'input#field-input-mock-id_field-2'
    ) as HTMLInputElement;

    fireEvent.change(inputField1, {
      target: { value: 'mock-value-1' }
    });
    fireEvent.change(inputField2, {
      target: { value: 'mock-value-2' }
    });

    expect(inputField1.value).toEqual('mock-value-1');
    expect(inputField2.value).toEqual('mock-value-2');

    wrapper.unmount();

    const wrapper1 = renderLayoutWithElementView(
      viewElementProps.formKey,
      layoutFields,
      {
        registeredFields: undefined,
        registeredHighOrderValidationFactories: builtinHOCValidationFactories
      }
    );

    const inputField3 = wrapper1.baseElement.querySelector(
      'input#field-input-mock-id_field-1'
    ) as HTMLInputElement;

    fireEvent.change(inputField3, {
      target: { value: 'mock-value-1' }
    });

    expect(inputField3.value).toEqual('mock-value-1');
  });

  it('Render ViewElement > field Control is undefined', () => {
    mockHook.useControl = () => undefined;

    const wrapper = renderElementView(viewElementProps, {
      registeredFields: {
        'field-1': {
          ...initContextValue.registeredFields['field-1'],
          dataField: undefined,
          name: 'mockData'
        }
      }
    });

    expect(wrapper.baseElement.firstChild).toBeEmptyDOMElement();
  });
});
