import { useRef } from 'react';

// using lodash.isEqual will give warning "iterable.length has been deprecated..."
// react-fast-compare = optimize performance + remove warning above
import isEqual from 'react-fast-compare';

export interface DeepValueHook {
  <T extends unknown>(value: T): typeof value;
}

const useDeepValue: DeepValueHook = (value) => {
  const valueRef = useRef(value);

  if (!isEqual(valueRef.current, value)) {
    valueRef.current = value;
  }

  return valueRef.current;
};

export default useDeepValue;
