import React, { useEffect, useRef, useState } from 'react';
import { render } from '@testing-library/react';
import * as canvasTextWidth from '../../utils/canvasTextWidth';

import useResizeElementFitText from '.';
import { ResizeElementByTextConfig } from './types';

const Comp = ({
  isUndefinedParam = false,
  text: textProp,
  adjust
}: {
  isUndefinedParam?: boolean;
  text?: string;
  adjust?: () => void;
}) => {
  const [text, setText] = useState<string | undefined>('');
  const textRef = useRef(null);

  useEffect(() => {
    setText(textProp);
  }, [textProp]);

  useResizeElementFitText(
    isUndefinedParam
      ? (undefined as unknown as ResizeElementByTextConfig)
      : {
          element: textRef.current || undefined,
          text,
          adjust
        }
  );

  return <p ref={textRef}>{text}</p>;
};

describe('useResizeElementFitText', () => {
  it('without param', () => {
    jest.spyOn(canvasTextWidth, 'default').mockImplementation(() => null);
    render(<Comp isUndefinedParam />);
  });

  it('with text is undefined > has no text metrics', () => {
    jest.spyOn(canvasTextWidth, 'default').mockImplementation(() => null);
    render(<Comp text={undefined} />);
  });

  it('with text is undefined > has text metrics', () => {
    jest
      .spyOn(canvasTextWidth, 'default')
      .mockImplementation(() => ({} as TextMetrics));
    render(<Comp text={undefined} />);
  });

  it('withtext is undefined > has text metrics and adjust function', () => {
    jest
      .spyOn(canvasTextWidth, 'default')
      .mockImplementation(() => ({} as TextMetrics));
    render(<Comp text={undefined} adjust={jest.fn()} />);
  });
});
