export interface ResizeElementByTextConfig {
  element?: HTMLElement;
  text?: string;
  adjust?: (element: HTMLElement, textWidth: number) => void;
}

export interface UseResizeElementByTextHook {
  (config: ResizeElementByTextConfig): void;
}
