import { useEffect, useRef } from 'react';

// utils
import isFunction from 'lodash.isfunction';
import { canvasTextWidth } from '../../utils';

// types
import { UseResizeElementByTextHook } from './types';

const useResizeElementFitText: UseResizeElementByTextHook = ({
  element,
  text,
  adjust
} = {}) => {
  // refs
  const keepRef = useRef<Record<string, any>>({});

  // keep reference
  keepRef.current.adjust = adjust;

  useEffect(() => {
    if (!element) return;

    // innerText will be supported for DropdownList, unused for now
    const innerText = element.innerText;
    const placeholder = element.getAttribute('placeholder');
    const finalText = text || innerText || placeholder || '';

    const computedStyles = window.getComputedStyle(element);
    const textMetrics = canvasTextWidth(finalText, computedStyles.font);

    if (!textMetrics) return;

    // adjust width if adjust is exists
    const { adjust } = keepRef.current;
    if (isFunction(adjust)) {
      return adjust(element, textMetrics.width);
    }

    // resize element to text width
    element.style.width = `${textMetrics.width}px`;
  }, [element, text]);

  return null;
};

export default useResizeElementFitText;
