import React, { useEffect, useRef, useState } from 'react';
import { render, screen } from '@testing-library/react';
import { OptionsGeneric, Modifier } from '@popperjs/core';
import usePopper from '.';
import { className } from '../../utils';
type DefaultOptionsType = Partial<OptionsGeneric<Partial<Modifier<any, any>>>>;
const Comp = ({
  defaultOptions: defaultOptionsProp,
  isHaveContainer = false
}: {
  defaultOptions?: DefaultOptionsType;
  isHaveContainer?: boolean;
}) => {
  const referenceRef = useRef(null);
  const poperRef = useRef(null);
  const [defaultOptions, setDefaultOptions] = useState<
    DefaultOptionsType | undefined
  >({});
  useEffect(() => {
    setDefaultOptions(defaultOptionsProp);
  }, [defaultOptionsProp]);
  const [instance] = usePopper({
    reference: referenceRef.current!,
    popper: poperRef.current!,
    defaultOptions
  });
  return (
    <div ref={referenceRef}>
      <div data-testid="isInstanceNull">{instance ? 'true' : 'false'}</div>
      <div ref={poperRef}>
        {isHaveContainer && (
          <div className={className.dropdownBase.SCROLL_CONTAINER}></div>
        )}
      </div>
    </div>
  );
};
describe('usePopper', () => {
  it('without defaultOptions', () => {
    render(<Comp />);
    expect(screen.getByTestId('isInstanceNull').innerText).toEqual(undefined);
  });
  it('with defaultOptions', () => {
    Object.defineProperties(HTMLElement.prototype, {
      offsetHeight: { get: () => 0 }
    });
    render(<Comp defaultOptions={{ placement: 'auto' }} />);
    expect(screen.getByTestId('isInstanceNull').innerText).toEqual(undefined);
  });
  it('when not enought space bottom', () => {
    Object.defineProperties(HTMLElement.prototype, {
      offsetHeight: { get: () => 1000 }
    });
    render(<Comp defaultOptions={{ placement: 'auto' }} />);
    expect(screen.getByTestId('isInstanceNull').innerText).toEqual(undefined);
  });
  describe('with defaultOptions and has container', () => {
    const bounding = {
      bottom: 0,
      height: 0,
      left: 0,
      right: 0,
      top: 0,
      width: 0,
      x: 0,
      y: 0,
      toJSON: () => undefined
    };
    it('when reference element has top is 0', () => {
      Object.defineProperties(HTMLElement.prototype, {
        offsetHeight: { get: () => 1000 }
      });
      Element.prototype.getBoundingClientRect = () => ({ ...bounding, top: 0 });
      render(<Comp isHaveContainer defaultOptions={{ placement: 'auto' }} />);
      expect(screen.getByTestId('isInstanceNull').innerText).toEqual(undefined);
    });
    it('when reference element has top is 1000', () => {
      Object.defineProperties(HTMLElement.prototype, {
        offsetHeight: { get: () => 1000 }
      });
      Element.prototype.getBoundingClientRect = () => ({
        ...bounding,
        top: 1000
      });
      render(<Comp isHaveContainer defaultOptions={{ placement: 'auto' }} />);
      expect(screen.getByTestId('isInstanceNull').innerText).toEqual(undefined);
    });
  });
});
