import { CSSProperties, useEffect, useMemo, useRef, useState } from 'react';

import {
  createPopper,
  Instance,
  OptionsGeneric,
  Modifier,
  Placement
} from '@popperjs/core';
import { className } from '../../utils';

export interface PopperArgs {
  reference: HTMLElement;
  popper: HTMLElement;
  defaultOptions?: Partial<OptionsGeneric<Partial<Modifier<any, any>>>>;
}

export interface UsePopperHook {
  (args: PopperArgs): [Instance | null, CSSProperties, Placement | undefined];
}

/**
 * - usePopper is lower-level construct hook that is leveraged by the @popperjs/core
 * - usePopper will re-running when "reference element" change or "popper element" change
 * - This hook shared for those components in the project:
 * -    PopupBase
 */
const usePopper: UsePopperHook = ({ reference, popper, defaultOptions }) => {
  // refs
  const keepRef = useRef<Partial<PopperArgs>>({});
  const popperInitialStylesRef = useRef<CSSProperties>({
    display: 'none'
  });

  // states
  const [placement, setPlacement] = useState(defaultOptions?.placement);

  // keep reference
  keepRef.current.defaultOptions = defaultOptions;

  const popperInstance = useMemo(() => {
    if (!reference || !popper) return null;

    // create popper instance
    const instance = createPopper(reference, popper, {
      modifiers: [
        {
          name: 'flip',
          options: {
            altBoundary: false
          }
        }
      ]
    });

    // binding default options
    const { defaultOptions } = keepRef.current;
    if (defaultOptions) instance.setOptions(defaultOptions);

    // we need to update position to get the initial position for
    // the popper element (for now, the popper element still display:none)
    instance.forceUpdate();

    // reset display style to default
    // because display:none can't calculate height of the element
    // so we need to display it to trigger reflow process of the browser
    // (we can't use visibility for this case, because this occurs scroll flicking)
    popper.style.display = '';

    // dynamic height when show
    // handle for case SpaceTop and SpaceBottom is not enough to flip
    const windowHeight = window.innerHeight;
    const { top: referenceTop, bottom: referenceBottom } =
      reference.getBoundingClientRect();
    const distanceBottom = windowHeight - referenceBottom;
    const distanceTop = referenceTop;
    const isEnoughSpaceTop = referenceTop > popper.offsetHeight;
    const isEnoughSpaceBottom = distanceBottom > popper.offsetHeight;
    if (!isEnoughSpaceBottom && !isEnoughSpaceTop) {
      const dropdownBaseScrollContainer = popper.querySelector(
        `.${className.dropdownBase.SCROLL_CONTAINER}`
      ) as HTMLElement;

      // DropdownBase scroll container: padding top + padding bottom = 16px;
      // Popup container: border top + border bottom = 2px;
      // Popup container: padding top container = 4px;
      // total => 22px;
      // we need popup after resize height auto margin top or margin bottom 4px
      // so we need more 4px
      // => -22 -4
      if (dropdownBaseScrollContainer) {
        if (distanceTop > distanceBottom) {
          dropdownBaseScrollContainer.style.maxHeight = `${
            distanceTop - 22 - 4
          }px`;
        } else {
          dropdownBaseScrollContainer.style.maxHeight = `${
            distanceBottom - 22 - 4
          }px`;
        }
      }
    }

    // after reflow process completed, we have the height of the popper element
    // we need to calculate position/placement again
    instance.forceUpdate();

    return instance;
  }, [reference, popper]);

  // handle destroy popper instance
  useEffect(() => () => popperInstance?.destroy(), [popperInstance]);

  // handle watching popper placement
  useEffect(() => {
    if (!popper) return;

    const mutationObserver = new MutationObserver(() => {
      setPlacement(popper.getAttribute('data-popper-placement') as Placement);
    });

    mutationObserver.observe(popper, {
      attributeFilter: ['data-popper-placement']
    });

    return () => mutationObserver.disconnect();
  }, [popper]);

  return [popperInstance, popperInitialStylesRef.current, placement];
};

export default usePopper;
