/**
 * - Nếu item được chọn không nằm trong tầm nhìn của scrollContainer,
 * hook này sẽ giúp scrollContainer tự scroll để nhìn thấy element
 * - Hiện tại hook này chỉ support navigation up/down
 */
export interface ScrollOnNavigationHook {
  (
    /**
     * - React ref chứa scroll container element
     * - required
     */
    scrollContainerRef: React.MutableRefObject<HTMLElement | null>,

    /**
     * - React ref chứa chiều đang navigation
     * - required
     */
    directionRef: React.MutableRefObject<'top' | 'bottom'>,

    /**
     * - Function dùng để custom check
     * - return true nếu đây là element cần thao tác, ngược lại return false
     * - Lưu ý: cho tối ưu hiệu năng, isContains sẽ giữ lại tham chiếu lần đầu
     * trao qua hook này, điều này đồng nghĩa bạn sẽ không thể truy cập được
     * giá trị mới nhất bên ngoài function này. Nếu bạn muốn truy cập giá trị mới
     * nhất từ function này, vui lòng sử dụng React.SetStateAction
     * - default: undefined
     */
    isContains?: (target: HTMLElement) => boolean,

    /**
     * - Khoảng cách từ element được chọn đến 2 cạnh top/bottom
     * - Nếu khoảng cách này chạm ngưỡng, scroll container element
     * sẽ kích hoạt scroll lên/xuống
     * - Nếu margin lớn hơn scrollContainerHeight/2 - targetHeight, margin sẽ tự động
     * bị vô hiệu hoá
     * - default: 0
     */
    margin?: number,

    /**
     * - Cơ chế tương tự như "margin" nhưng tính theo số total height của các items
     * - default: 0
     */
    marginByItems?: number
  ): void;
}
