import { useEffect, useRef, useCallback } from 'react';

// utils
import get from 'lodash.get';
import isFunction from 'lodash.isfunction';
import { throwError, className } from '../../utils';

// types
import { ScrollOnNavigationHook } from './types';

/**
 * - If item selected is not visible in ScrollContainer,
 * this hook will be auto scroll to see item selected
 * - Support: up/down direction
 */
const useScrollOnNavigation: ScrollOnNavigationHook = (
  scrollContainerRef,
  directionRef,
  isContainsArg,
  marginArg = 0,
  marginByItems = 0
) => {
  // keep reference for isContainsArg function
  const isContainsRef = useRef(isContainsArg);

  // handle get total height
  // <<total height of previous items>> target selected <<total height of next items>>
  // support marginByItems prop
  const getPrevNextTotalHeight = useCallback(
    (target: HTMLElement) => {
      const prev: HTMLElement[] = [],
        next: HTMLElement[] = [];
      let marginByItemsTemp = marginByItems;

      while (marginByItemsTemp-- > 0) {
        const prevElement = get(
          prev,
          [prev.length - 1, 'previousSibling'],
          target.previousSibling
        ) as HTMLElement;

        const nextElement = get(
          next,
          [next.length - 1, 'nextSibling'],
          target.nextSibling
        ) as HTMLElement;

        prev.push(prevElement);
        next.push(nextElement);
      }

      const prevTotalHeight = prev.reduce((prev, curr) => {
        const currHeight = get(curr, 'offsetHeight', 0);
        return prev + currHeight;
      }, 0);
      const nextTotalHeight = next.reduce((prev, curr) => {
        const currHeight = get(curr, 'offsetHeight', 0);
        return prev + currHeight;
      }, 0);

      return {
        prevTotalHeight,
        nextTotalHeight
      };
    },
    [marginByItems]
  );

  // get selected target current
  const getTargetSelected = useCallback((records: MutationRecord[]) => {
    const isContains = isContainsRef.current;

    // get selected record current
    const recordSelected = records.find((record) => {
      const target = get(record, 'target') as HTMLElement;
      if (isFunction(isContains)) {
        return isContains(target);
      }

      return target.classList.contains(className.state.SELECTED);
    });

    const target = get(recordSelected, 'target') as HTMLElement;

    return target;
  }, []);

  useEffect(() => {
    if (!scrollContainerRef.current) return;

    if (marginArg < 0) throwError.cantLessThan('margin', 0);

    const container = scrollContainerRef.current;
    const handleMutation = (
      records: MutationRecord[],
      mutationObserver: MutationObserver,
      defaultTarget?: HTMLElement
    ) => {
      // get selected target
      const target = defaultTarget || getTargetSelected(records);
      if (!target) return;

      let { prevTotalHeight, nextTotalHeight } = getPrevNextTotalHeight(target);

      const popupScrollTop = get(container, 'scrollTop', 0);
      const popupHeight = get(container, 'clientHeight', 0);
      const popupBottom = popupHeight + popupScrollTop;

      const targetTop = get(target, 'offsetTop', 0);
      const targetHeight = get(target, 'offsetHeight', 0);
      const targetBottom = targetHeight + targetTop;
      let overlap: 'top' | 'bottom' | undefined;

      // Adjust margin, support scroll correctly
      let margin = marginArg;
      if (margin > popupHeight / 2 - targetHeight) {
        margin = 0;
      }

      // Adjust prevTotalHeight, support scroll correctly
      if (prevTotalHeight > popupHeight / 2 - targetHeight) {
        prevTotalHeight = 0;
      }

      // Adjust nextTotalHeight, support scroll correctly
      if (nextTotalHeight > popupHeight / 2 - targetHeight) {
        nextTotalHeight = 0;
      }

      // check status target is overlap top or not
      if (popupScrollTop > targetTop - margin - prevTotalHeight) {
        overlap = 'top';
      }

      // check status target is overlap bottom or not
      if (popupBottom < targetBottom + margin + nextTotalHeight) {
        overlap = 'bottom';
      }

      // if target is not overlap, don't trigger scroll
      if (!overlap) return;

      // if target is overlap
      // if direction is ArrowUp, target will be display on the top
      // if direction is ArrowDown, target will be display on the bottom
      let position = 0;
      switch (directionRef.current) {
        case 'top':
          // item selected will be display on the top
          position = targetTop;

          // handle add extra margin, example: if margin is 100
          // selected item will be margin top 100px
          position -= margin;

          // handle add extra height, example: if extra height is 100
          // selected item will be margin top 100px
          position -= prevTotalHeight;
          break;
        case 'bottom':
          // item selected will be display on the bottom
          position = targetBottom - popupHeight;

          // handle add extra margin, example: if margin is 100
          // selected item will be margin bottom 100px
          position += margin;

          // handle add extra height, example: if extra height is 100
          // selected item will be margin bottom 100px
          position += nextTotalHeight;
          break;
      }

      container.scrollTo({
        left: 0,
        top: position,
        behavior: 'auto'
      });
    };

    const mutationObserver = new MutationObserver(handleMutation as any);
    mutationObserver.observe(container, {
      childList: true,
      subtree: true,
      attributeFilter: ['class']
    });

    // run first time
    const defaultTarget = scrollContainerRef.current?.querySelector(
      `.${className.state.SELECTED}`
    ) as HTMLElement;
    if (defaultTarget) {
      // need check later
      process.nextTick(() =>
        handleMutation([], mutationObserver, defaultTarget)
      );
    }

    return () => mutationObserver.disconnect();
  }, [
    scrollContainerRef,
    directionRef,
    marginArg,
    marginByItems,
    getPrevNextTotalHeight,
    getTargetSelected
  ]);
};

export * from './types';
export default useScrollOnNavigation;
