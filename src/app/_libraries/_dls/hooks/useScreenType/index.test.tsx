import React from 'react';

// testing library
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';

// hooks
import useScreenType from '.';

const Wrapper: React.FC<any> = () => {
  useScreenType();

  return <div>Wrapper</div>;
};

const event = window.document.createEvent('HTMLEvents');
event.initEvent('orientationchange', true, true);

describe('useScreenType', () => {
  it('should run for desktop', () => {
    jest.useFakeTimers();

    window.innerWidth = 1280;
    window.innerHeight = 1024;

    const wrapper = render(<Wrapper />);
    jest.runAllTimers();

    expect(wrapper.queryByText('Wrapper')).toBeInTheDocument();
  });

  it('should run for tablet landscape', () => {
    jest.useFakeTimers();

    window.innerWidth = 1024;
    window.innerHeight = 768;

    const wrapper = render(<Wrapper />);
    jest.runAllTimers();

    expect(wrapper.queryByText('Wrapper')).toBeInTheDocument();
  });

  it('should run for tablet landscape', () => {
    jest.useFakeTimers();

    window.innerWidth = 768;
    window.innerHeight = 1024;

    const wrapper = render(<Wrapper />);
    jest.runAllTimers();

    expect(wrapper.queryByText('Wrapper')).toBeInTheDocument();
  });

  it('should run for custom', () => {
    jest.useFakeTimers();

    window.innerWidth = 999;
    window.innerHeight = 999;

    const wrapper = render(<Wrapper />);
    jest.runAllTimers();

    expect(wrapper.queryByText('Wrapper')).toBeInTheDocument();
  });
});
