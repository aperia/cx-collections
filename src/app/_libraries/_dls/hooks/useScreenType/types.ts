const ScreenSize = Object.freeze({
  DESKTOP: {
    width: 1280,
    height: 1024
  },
  TABLET_LANDSCAPE: {
    width: 1024,
    height: 768
  },
  TABLET_PORTRAIT: {
    width: 768,
    height: 1024
  }
});

enum ScreenType {
  DESKTOP,
  TABLET_LANDSCAPE,
  TABLET_PORTRAIT,
  CUSTOM,
  UNAVAILABLE
}

export { ScreenSize, ScreenType };
