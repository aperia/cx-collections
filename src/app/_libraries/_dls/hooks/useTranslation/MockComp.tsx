import React, { Suspense } from 'react';
import { I18nextProvider } from '../../components';

export const CompWrapper = (props: any) => {
  return (
    <Suspense fallback={<div></div>}>
      <I18nextProvider
        initReactI18nextProps={true}
        resource={{
          lang: 'en',
          data: {
            welcome_trans: 'Change Language',
            txt_acc_nbr: 'Account number'
          }
        }}
      >
        {props.children}
      </I18nextProvider>
    </Suspense>
  );
};

export const Comp = React.lazy(() => import('./Comp'));
