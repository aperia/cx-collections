import { render } from '@testing-library/react';
import { wait } from 'app/test-utils';
import React from 'react';
import { CompWrapper, Comp } from './MockComp';

describe('useTranslation should be tested', () => {
  it('test useTranslation', async () => {
    render(
      <CompWrapper>
        <Comp />
      </CompWrapper>
    );
    await wait();
    expect(true).toEqual(true);
  });
});
