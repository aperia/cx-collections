import React from 'react';
import { useTranslation } from '..';

const Comp = () => {
  const { t } = useTranslation();

  return <div>{t('welcome_trans')}</div>;
};

export default Comp;
