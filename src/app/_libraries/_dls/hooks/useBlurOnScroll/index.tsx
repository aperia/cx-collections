import { useEffect } from 'react';

const useBlurOnScroll = (triggerElement: HTMLElement, shouldRun: boolean) => {
  useEffect(() => {
    if (!shouldRun) return;

    const handleScrollWindow = () => {
      triggerElement?.blur?.();
    };

    // handleScrollWindow not need to be debounced
    // if shouldRun change, cleanup function should be called
    // noticed: shouldRun must to changed when we call blur on triggerElement
    // see details in: TextBox, NumericTextBox, MaskedTextBox, TextArea

    window.addEventListener('scroll', handleScrollWindow, true);
    return () => window.removeEventListener('scroll', handleScrollWindow, true);
  }, [triggerElement, shouldRun]);
};

export default useBlurOnScroll;
