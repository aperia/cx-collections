import { useEffect } from 'react';

const useRenderElement = (
  ref: React.RefObject<HTMLDivElement | null>,
  element: HTMLElement | null
) => {
  useEffect(() => {
    if (element === null) {
      return;
    }
    const prevRef = ref.current;
    prevRef?.appendChild(element);
    return () => {
      prevRef?.removeChild(element);
    };
  }, [element, ref]);
};

export default useRenderElement;
