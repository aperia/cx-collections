import React, { useRef } from 'react';

// testing library
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';

// hooks
import useRenderElement from '.';

const Wrapper: React.FC<any> = ({ element }) => {
  const divRef = useRef<HTMLDivElement | null>(null);

  useRenderElement(divRef, element);

  return <div ref={divRef}>Wrapper</div>;
};

describe('useRenderElement', () => {
  it('should cover 100%', () => {
    jest.useFakeTimers();
    const { rerender, queryByText } = render(<Wrapper element={null} />);

    rerender(<Wrapper element={document.createElement('div')} />);
    expect(queryByText('Wrapper')).toBeInTheDocument();
  });
});
