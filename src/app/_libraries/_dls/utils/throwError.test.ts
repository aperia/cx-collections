import throwError from './throwError';

describe('test throwError', () => {
  it('should run notFound', () => {
    expect(throwError.notFound).toThrow(
      "[DLS] - Can't found value with undefined=undefined, please define your undefined"
    );
  });

  it('should run cantLessThan', () => {
    expect(throwError.cantLessThan).toThrow(
      "[DLS] - undefined can't be less than undefined"
    );
  });

  it('should run cantMoreThan', () => {
    expect(throwError.cantMoreThan).toThrow(
      "[DLS] - undefined can't be more than undefined"
    );
  });
});
