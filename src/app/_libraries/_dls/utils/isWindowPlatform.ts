const windowsPlatforms = ['Win32', 'Win64', 'Windows', 'WinCE'];

const isWindowPlatform = () => {
  const { platform } = window.navigator;
  return windowsPlatforms.includes(platform);
};

export default isWindowPlatform;
