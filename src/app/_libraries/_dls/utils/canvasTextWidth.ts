let keepCanvas: HTMLCanvasElement;

const canvasTextWidth = (
  text: string,
  font = '14px "Open Sans", sans-serif'
) => {
  let canvas: HTMLCanvasElement;

  if (keepCanvas) canvas = keepCanvas;
  else {
    canvas = document.createElement('canvas');
    keepCanvas = canvas;
  }

  const context = canvas.getContext('2d');
  if (!context) return null;

  context.font = font;
  return context.measureText(text);
};

export default canvasTextWidth;
