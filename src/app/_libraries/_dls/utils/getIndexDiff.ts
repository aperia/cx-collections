const getIndexDiff = (previous: string, next: string) => {
  let indexDiff = -1;
  if (!previous) return indexDiff;

  const previousList = previous.split('');
  const nextList = next.split('');

  for (let index = 0; index < previousList.length; index++) {
    if (previousList[index] === nextList[index]) continue;

    indexDiff = index;
    break;
  }

  return indexDiff;
};

export default getIndexDiff;
