export default {
  textBox: {
    get CONTAINER() {
      return 'dls-text-box';
    },
    get LABEL_ICON() {
      return 'text-box-label-icon';
    },
    get PREFIX() {
      return 'text-box-prefix';
    },
    get SUFFIX() {
      return 'text-box-suffix';
    }
  },
  input: {
    get PREFIX() {
      return 'prefix';
    },
    get SUFFIX() {
      return 'suffix';
    },
    get CONTAINER() {
      return 'dls-input-container';
    },
    get LABEL() {
      return 'label';
    }
  },
  floatingLabel: {
    get CONTAINER() {
      return 'dls-floating-label';
    }
  },
  state: {
    get REQUIRED() {
      return 'dls-required';
    },
    get ERROR() {
      return 'dls-error';
    },
    get SELECTED() {
      return 'dls-selected';
    },
    get HOVER() {
      return 'dls-hover';
    },
    get FOCUSED() {
      return 'dls-focused';
    },
    get DISABLED() {
      return 'dls-disabled';
    },
    get READ_ONLY() {
      return 'dls-readonly';
    },
    get FLOATING() {
      return 'dls-floating';
    }
  },
  popper: {
    get CONTAINER() {
      return 'dls-popper-container';
    },
    get ARROW() {
      return 'dls-popper-arrow';
    },
    get TRIGGER() {
      return 'dls-popper-trigger';
    }
  },
  tooltip: {
    get CONTAINER() {
      return 'dls-tooltip-container';
    },
    get ARROW() {
      return 'tooltip-arrow';
    },
    get INNER() {
      return 'tooltip-inner';
    }
  },
  popupBase: {
    get CONTAINER() {
      return 'dls-popup';
    },
    get ANIMATION_CONTAINER() {
      return 'animation-container';
    },
    get FLUID() {
      return 'fluid';
    },
    get NO_BORDER() {
      return 'no-border';
    }
  },
  dropdownBase: {
    get CONTAINER() {
      return 'dls-dropdown-base-container';
    },
    get SCROLL_CONTAINER() {
      return 'dls-dropdown-base-scroll-container';
    },
    get ITEM() {
      return 'item';
    },
    get HEADER() {
      return 'item-heading';
    }
  },
  switch: {
    get CONTAINER() {
      return 'dls-custom-control dls-custom-switch';
    },
    get INPUT() {
      return 'dls-custom-control-input';
    },
    get LABEL() {
      return 'dls-custom-control-label';
    },
    get LABEL_TEXT() {
      return 'dls-custom-switch-label';
    }
  },
  combobox: {},
  dropdownList: {
    get CONTAINER() {
      return;
    }
  },
  grid: {
    get GRID() {
      return 'dls-grid';
    },
    get GRID_HEAD() {
      return 'dls-grid-head';
    },
    get GRID_BODY() {
      return 'dls-grid-body';
    },
    get GRID_FOOTER() {
      return 'dls-grid-footer';
    },
    get SORT() {
      return 'sort';
    },
    get SORT_ICON() {
      return 'sort-icon';
    },
    get FILTER_BUTTON() {
      return 'dls-filter-button';
    },
    get FILTER_ICON() {
      return 'dls-filter-button';
    },
    get FILTERING() {
      return 'filtering';
    },
    get FIXED() {
      return 'fixed';
    },
    get GRID_FOR_WINDOW() {
      return 'grid-for-window';
    },
    get HAS_FIXED_BORDER() {
      return 'has-fixed-border';
    },
    get FIXED_LAST_LEFT() {
      return 'fixed-last-left';
    },
    get FIXED_FIRST_RIGHT() {
      return 'fixed-first-right';
    },
    get SCROLLABLE() {
      return 'scrollable';
    },
    get WITH_FOOTER() {
      return 'with-footer';
    },
    get SELECTED() {
      return 'selected';
    },
    get GRID_EXPAND_ROW() {
      return 'dls-grid-expand-row';
    },
    get GRID_CHECKED_STYLE() {
      return 'checked-style';
    },
    get GRID_DND() {
      return 'dnd';
    }
  },
  infoBar: {
    get CONTAINER() {
      return 'infobar-container';
    },
    get NAV_CONTAINER() {
      return 'infobar-nav-container';
    },
    get NAV_ITEM() {
      return 'infobar-nav-item';
    },
    get NAV_ITEM_SEPARATOR() {
      return 'infobar-nav-item-separator';
    },
    get NAV_ITEM_TOGGLE() {
      return 'infobar-nav-item-toggle';
    },
    get BODY() {
      return 'infobar-body';
    },
    get DROPDOWN() {
      return 'infobar-dropdown';
    },
    get DROPDOWN_HEADER() {
      return 'infobar-dropdown-header';
    },
    get DROPDOWN_ITEM() {
      return 'infobar-dropdown-item';
    },
    get DROPDOWN_ITEM_ICON() {
      return 'infobar-dropdown-item-icon';
    },
    get DROPDOWN_ITEM_LABEL() {
      return 'infobar-dropdown-item-label';
    },
    get ACTIVE_DROPDOWN() {
      return 'active-dropdown';
    },
    get ITEM_DISABLED() {
      return 'item-disabled';
    },
    get CONTENT() {
      return 'infobar-content';
    },
    get CONTENT_PINNED() {
      return 'inforbar-content-pinned';
    },
    get ACTIVE() {
      return 'active';
    },
    get COLLAPSED() {
      return 'collapsed';
    },
    get MTA() {
      return 'mt-auto';
    },
    get CSS_TOOLTIP_WRAPPER() {
      return 'css-tooltip-wrapper';
    },
    get CONTENT_HIDE() {
      return 'infobar-content hide';
    },
    get EMPTY() {
      return 'infobar-empty';
    }
  },
  checkbox: {
    get CONTAINER() {
      return 'custom-control custom-checkbox dls-checkbox';
    },
    get INPUT() {
      return 'custom-control-input dls-checkbox-input';
    },
    get FAKE_INPUT() {
      return 'custom-control-input dls-checkbox-fake-input';
    },
    get LABEL() {
      return 'custom-control-label dls-checkbox-label';
    }
  },
  radio: {
    get CONTAINER() {
      return 'custom-control custom-radio dls-radio';
    },
    get INPUT() {
      return 'custom-control-input dls-radio-input';
    },
    get LABEL() {
      return 'custom-control-label dls-radio-label';
    }
  },
  modal: {
    get BACKDROP() {
      return 'modal-backdrop';
    },
    get ROOT() {
      return 'dls-modal-root';
    },
    get DIALOG() {
      return 'dls-modal-dialog';
    },
    get CONTENT() {
      return 'modal-content';
    },
    get HEADER() {
      return 'dls-modal-header';
    },
    get BORDER_HEADER() {
      return 'dls-modal-br-header';
    },
    get BODY() {
      return 'dls-modal-body';
    },
    get FOOTER() {
      return 'dls-modal-footer';
    },
    get BORDER_FOOTER() {
      return 'dls-modal-br-footer';
    },
    get TITLE() {
      return 'dls-modal-title';
    },
    get NO_PADDING() {
      return 'no-padding';
    },
    get FULL() {
      return 'modal-full';
    },
    get LEFT() {
      return 'modal-left';
    },
    get RIGHT() {
      return 'modal-right';
    }
  },
  animation: {
    get CONTAINER() {
      return 'dls-animation';
    },
    get FADE() {
      return 'dls-animation__fade';
    },
    get SLIDE() {
      return 'dls-animation__slide';
    }
  },
  tabBar: {
    get TAB_BAR() {
      return 'dls-tabbar';
    },
    get HOME() {
      return 'home-tab';
    },
    get TAB() {
      return 'dls-tab';
    },
    get CONTENT() {
      return 'dls-content';
    },
    get DLS_STATE_ACTIVE() {
      return 'dls-state-active';
    },
    get NAV() {
      return 'dls-tabbar-nav';
    },
    get NAV_BODY() {
      return 'dls-nav-body';
    },
    get TAB_STRIP_ITEMS() {
      return 'dls-tabstrip-items';
    },
    get ITEM() {
      return 'dls-item';
    },
    get ITEM_WRAP() {
      return 'dls-item-wrap';
    },
    get ARROW_LEFT() {
      return 'dls-arrow arrow-left';
    },
    get ARROW_RIGHT() {
      return 'dls-arrow arrow-right';
    },
    get MARK_ACTIVE() {
      return 'mark-active-tab';
    },
    get PRE_ACTIVE() {
      return 'pre-active';
    },
    get STATE_DISABLED() {
      return 'state-disabled';
    },
    get STATE_ACTIVE() {
      return 'state-active';
    }
  },
  verticalTabs: {
    get CONTAINER() {
      return 'vertical-tab';
    }
  },
  horizontalTabs: {
    get CONTAINER() {
      return 'horizontal-container';
    },
    get NAV() {
      return 'horizontal-nav';
    }
  },
  textSearch: {
    get CONTAINER() {
      return 'dls-text-search';
    },
    get SEARCH_ICON() {
      return 'dls-text-search-icon';
    },
    get INPUT() {
      return 'dls-text-search-input';
    },
    get CLEAR_BUTTON() {
      return 'dls-text-search-clear';
    },
    get SEARCH_BUTTON() {
      return 'dls-text-search-search';
    }
  },
  breadcrumb: {
    get CONTAINER() {
      return 'dls-breadcrumb';
    },
    get LIST() {
      return 'dls-breadcrumb-list';
    },
    get ITEM() {
      return 'dls-breadcrumb-item';
    },
    get ITEM_TEXT() {
      return 'dls-breadcrumb-item-text';
    },
    get ITEM_ACTIVE() {
      return 'active';
    }
  }
};
