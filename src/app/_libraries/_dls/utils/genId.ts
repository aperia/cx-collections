import { nanoid } from 'nanoid';

/**
 * - Util generate unique id
 */
const genId = () => `dls-${nanoid()}`;

export default genId;
