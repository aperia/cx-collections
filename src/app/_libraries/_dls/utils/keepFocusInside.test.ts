import keepFocusInside from './keepFocusInside';

describe('test keepFocusInside', () => {
  const element = { focus: () => true };
  Object.defineProperty(document, 'activeElement', {
    value: element
  });

  it('event.keyCode is 9', () => {
    keepFocusInside({ keyCode: 10 }, {});
    expect(true).toBeTruthy();
  });

  it('focusableElements.length === 0', () => {
    keepFocusInside({ keyCode: 9 }, { querySelectorAll: () => [] });
    expect(true).toBeTruthy();
  });

  it('focusableElements.length !== 0', () => {
    keepFocusInside({ keyCode: 9 }, { querySelectorAll: () => [1, 2] });
    expect(true).toBeTruthy();
  });

  it('event.shiftKey is false', () => {
    keepFocusInside(
      { keyCode: 9, shiftKey: false },
      { querySelectorAll: () => [1, 2] }
    );
    expect(true).toBeTruthy();
  });

  it('event.shiftKey is false and document.activeElement === lastElement', () => {
    keepFocusInside(
      { keyCode: 9, shiftKey: false, preventDefault: () => undefined },
      { querySelectorAll: () => [{ focus: () => undefined }, element] }
    );
    expect(true).toBeTruthy();
  });

  it('event.shiftKey is false and document.activeElement !== lastElement', () => {
    keepFocusInside(
      { keyCode: 9, shiftKey: false, preventDefault: () => undefined },
      { querySelectorAll: () => [element, 2] }
    );
    expect(true).toBeTruthy();
  });

  it('event.shiftKey is true and document.activeElement === firstElement', () => {
    keepFocusInside(
      { keyCode: 9, shiftKey: true, preventDefault: () => undefined },
      { querySelectorAll: () => [element, element] }
    );
    expect(true).toBeTruthy();
  });

  it('event.shiftKey is true and document.activeElement !== firstElement', () => {
    keepFocusInside(
      { keyCode: 9, shiftKey: true, preventDefault: () => undefined },
      { querySelectorAll: () => [1, element] }
    );
    expect(true).toBeTruthy();
  });
});
