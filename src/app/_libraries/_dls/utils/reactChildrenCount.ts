const reactChildrenCount = (children: any): number => {
  let counter = 0;

  if (!Array.isArray(children)) return ++counter;

  children.forEach((item) => {
    if (Array.isArray(item.props.children)) {
      counter += reactChildrenCount(item.props.children);
      return;
    }
    counter += 1;
  });

  return counter;
};

export default reactChildrenCount;
