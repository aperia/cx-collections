import replaceSpecialCharacter from './replaceSpecialCharacter';

describe('test replaceSpecialCharacter', () => {
  it('should run if param is undefined', () => {
    const value = undefined;
    const nextValue = replaceSpecialCharacter(value);
    expect(nextValue).toEqual('');
  });

  it('should replace spacial character', () => {
    const value = '-=a';
    const nextValue = replaceSpecialCharacter(value);
    expect(nextValue).toEqual('a');
  });
});
