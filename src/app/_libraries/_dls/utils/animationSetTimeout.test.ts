import animationSetTimeout from './animationSetTimeout';

describe('test animationSetTimeout', () => {
  it('should call callback fn', () => {
    const callback = jest.fn();

    jest.useFakeTimers();
    animationSetTimeout(callback, 500);
    jest.runAllTimers();

    expect(callback).toBeCalled();
  });
});
