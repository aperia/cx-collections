const keycode = {
  CTRL: 17,
  A: 65,
  C: 67,
  V: 86,
  UP: 38,
  DOWN: 40,
  LEFT: 37,
  RIGHT: 39,
  TAB: 9,
  ENTER: 13,
  BACKSPACE: 8,
  SPACE: 32,
  DELETE: 46,
  isSelectAll: function (ctrlKey: boolean, keyCode: number) {
    return ctrlKey && keyCode === this.A;
  },
  isCopy: function (ctrlKey: boolean, keyCode: number) {
    return ctrlKey && keyCode === this.C;
  },
  isPaste: function (ctrlKey: boolean, keyCode: number) {
    return ctrlKey && keyCode === this.V;
  },
  isTab: function (keyCode: number) {
    return keyCode === this.TAB;
  },
  isShiftTab: function (shiftKey: boolean, keyCode: number) {
    return shiftKey && keyCode === this.TAB;
  }
};

export default Object.freeze(keycode);
