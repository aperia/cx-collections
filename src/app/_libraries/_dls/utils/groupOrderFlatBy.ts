import groupBy from 'lodash.groupby';
import orderByLodash from 'lodash.orderby';

function groupOrderFlatBy<T extends Record<string, any>>(
  data: T[],
  groupKey: string,
  orderKey: string,
  orderBy?: 'asc' | 'desc'
) {
  if (!Array.isArray(data) || !groupKey || !orderKey) return [];

  const dataObjectGrouped = groupBy(data, (item) => item[groupKey]) as Record<
    string,
    T[]
  >;

  const dataGrouped = Object.entries(dataObjectGrouped).reduce<T[]>(
    (prev, [, dataItems]) => {
      if (orderBy) {
        dataItems = orderByLodash(
          dataItems,
          [
            (dataItem) => {
              if (typeof dataItem[orderKey] !== 'string') return;

              return dataItem[orderKey].toLowerCase();
            }
          ],
          [orderBy]
        );
      }

      return [...prev, ...dataItems];
    },
    []
  );

  return dataGrouped;
}

export default groupOrderFlatBy;
