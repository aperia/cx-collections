import {
  getPadding,
  getMargin,
  getBorder,
  getScrollBarWidth,
  getAvailableWidth
} from './getElementAttribute';

Object.defineProperty(window, 'getComputedStyle', {
  value: () => {
    return {
      paddingTop: undefined,
      paddingRight: undefined,
      paddingLeft: undefined,
      paddingBottom: undefined,
      marginTop: undefined,
      marginRight: undefined,
      marginLeft: undefined,
      marginBottom: undefined,
      borderTop: undefined,
      borderRight: undefined,
      borderLeft: undefined,
      borderBottom: undefined
    };
  }
});

describe('test getElementAttribute', () => {
  it('getPadding without element', () => {
    const result = getPadding(null);
    expect(result).toEqual(null);
  });

  it('getPadding with element', () => {
    const result = getPadding(document.body);
    expect(result).not.toEqual(null);
  });

  it('getMargin without element', () => {
    const result = getMargin(null);
    expect(result).toEqual(null);
  });

  it('getMargin with element', () => {
    const result = getMargin(document.body);
    expect(result).not.toEqual(null);
  });

  it('getBorder without element', () => {
    const result = getBorder(null);
    expect(result).toEqual(null);
  });

  it('getBorder with element', () => {
    const result = getBorder(document.body);
    expect(result).not.toEqual(null);
  });

  it('getScrollBarWidth without element', () => {
    const result = getScrollBarWidth(null);
    expect(result).toEqual({
      vertical: 0,
      horizontal: 0
    });
  });

  it('getScrollBarWidth with element', () => {
    const result = getScrollBarWidth(document.body);
    expect(result).not.toEqual(null);
  });

  it('getAvailableWidth without element', () => {
    const result = getAvailableWidth(null);
    expect(result).toEqual(0);
  });

  it('getAvailableWidth with element', () => {
    const result = getAvailableWidth(document.body);
    expect(result).not.toEqual(null);
  });
});
