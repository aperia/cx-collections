import React, { FC, createContext, useContext, useRef } from 'react';

export type StorageContextArgs = {
  values?: any[];
  add?: (value: any) => void;
  remove?: (value: any) => void;
};

// context
const Context = createContext<StorageContextArgs>({});

// hook
const useStorage = () => useContext(Context);

const StorageProvider: FC = (props) => {
  const storage = useRef<StorageContextArgs>({ values: [] });

  storage.current.add = (value) => storage.current.values?.push(value);
  storage.current.remove = (value) => {
    storage.current.values = storage.current.values?.filter(
      (currentValue) => currentValue !== value
    );
  };

  return <Context.Provider value={storage.current} {...props} />;
};

export { useStorage };
export default StorageProvider;
