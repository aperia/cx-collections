import React, {
  useRef,
  forwardRef,
  useImperativeHandle,
  RefForwardingComponent,
  useState,
  useLayoutEffect,
  useContext,
  useCallback,
  useEffect,
  useMemo
} from 'react';
import ReactDOM from 'react-dom';

import { DndProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import { useTable, useSortBy, ActionType } from 'react-table';
import isEmpty from 'lodash.isempty';
import findLast from 'lodash.findlast';
import get from 'lodash.get';
import find from 'lodash.find';
import sumBy from 'lodash.sumby';
import isFunction from 'lodash.isfunction';
import classnames from 'classnames';

// Types
import { GridBaseProps, TableState, ColumnType, GridRef } from './types';

// Utils
import {
  className as classConst,
  genId,
  getScrollBarWidth,
  isProduction,
  isWindowPlatform
} from '../../utils';

// Contexts
import { GridContext } from './GridContext';

// Components
import GridHeader from './GridHeader';
import GridBody from './GridBody';
import GridFooter from './GridFooter';
import ResizeObserver from 'resize-observer-polyfill';
import DragLayer from './DragLayer';
import { useDeepValue } from '../../hooks';

// react-table actions
const actions = {
  resetSortBy: 'resetSortBy',
  toggleSortBy: 'toggleSortBy'
};

const resizeTableBody = (bodyElement: HTMLTableSectionElement | null) => {
  const { clientHeight, scrollHeight } = bodyElement!;

  const grid = bodyElement!.closest('.dls-grid');
  const header = grid?.getElementsByClassName(
    'dls-grid-head'
  )[0] as HTMLTableSectionElement;

  // reset margin right for table header
  header.style.marginRight = `0`;

  const scrollWidth = getScrollBarWidth(bodyElement);

  // table body doesn't have vertical scroll
  if (scrollHeight <= clientHeight + scrollWidth.horizontal) return;

  // set margin right for table header
  header.style.marginRight = `${scrollWidth.vertical}px`;
};

// catch react-table actions
const stateReducer = (
  newState: TableState,
  action: ActionType,
  previousState: TableState
) => {
  const { type } = action;

  // prevent auto reset sortBy after data is updated
  if (type === actions.resetSortBy) return previousState;

  return newState;
};

const GridBase: RefForwardingComponent<GridRef, GridBaseProps> = (
  { className, classes, id, dataTestId },
  ref
) => {
  const {
    head: headerClassName,
    body: bodyClassName,
    foot: footClassName
  } = classes || {};

  const {
    state: {
      data,
      dataFooter,
      sortBy,
      columns: columnsPropUnstable,
      variant,
      expand,
      togglable,
      hasHorizontalScroll: hasHorizontalScrollState,
      alwaysBorderFixedColumn,
      maxHeight,
      onLoadMore,
      dndId,
      dnd,
      dataItemKey
    },
    dispatch
  } = useContext(GridContext);

  const columnsProp = useDeepValue(columnsPropUnstable);

  // config props for fixed columns
  const [totalWidth, setTotalWidth] = useState<number>();
  const [columns, setColumns] = useState<ColumnType[]>([]);
  const [isTriggerLoadMore, setTriggerLoadMore] = useState(false);
  const [isRendered, setRendered] = useState(false);
  const [dragLayer, setDragLayer] = useState<React.ReactNode>(null);

  const gridRef = useRef<HTMLTableElement | null>(null);
  const headerRef = useRef<HTMLTableSectionElement | null>(null);
  const bodyRef = useRef<HTMLTableSectionElement | null>(null);
  const footerRef = useRef<HTMLTableSectionElement | null>(null);

  // control react-table's state
  const useControlledState = (state: TableState) => {
    const newState = { ...state };

    // control sort, if not, react-table will control
    if (sortBy) newState.sortBy = [];
    return newState;
  };

  const idToggleColumn = useMemo(() => {
    if (!togglable) return;

    return genId();
  }, [togglable]);

  useImperativeHandle(
    ref,
    () => ({
      gridElement: gridRef.current!,
      gridHeaderElement: headerRef.current!,
      gridBodyElement: bodyRef.current!,
      gridFooterElement: footerRef.current!
    }),
    []
  );

  // config columns before useTable generate config.
  useLayoutEffect(() => {
    const copyColumnsProp = [...columnsProp];

    // add variant column to columnList
    if (variant) {
      const width = variant.type === 'radio-check' ? 65 : 57;

      const newColumn: ColumnType = {
        Header: '', // prevent react-table render ' ' when [Header] isn't existed
        width,
        ...variant
      };

      copyColumnsProp.splice(variant.order || 0, 0, newColumn);
    }

    // add expand column to columnList

    if (expand || togglable) {
      !isProduction() &&
        expand &&
        console.warn(
          "[DLS] - Grid's expand prop has been deprecated, please use togglable instead"
        );

      const newColumn: ColumnType = {
        Header: '', // prevent react-table render ' ' when [Header] isn't existed
        width: 60,
        type: 'expand',
        id: idToggleColumn,
        ...(expand as any)
      };

      copyColumnsProp.splice(expand?.order || 0, 0, newColumn);
    }

    // The last column which has { isFixedLeft = true }
    const lastLeft = findLast(
      copyColumnsProp,
      ({ isFixedLeft }) => isFixedLeft
    );

    // The first column which has { isFixedRight = true }
    const firstRight = find(
      copyColumnsProp,
      ({ isFixedRight }) => isFixedRight
    );

    // use for calculating positions of fixed columns
    let currentWidthLeft = 0;
    let sumWidthRight = sumBy(
      copyColumnsProp,
      ({ width, isFixedRight }) => (isFixedRight && width) || 0
    );

    const newCols = copyColumnsProp.map((column) => {
      const {
        id,
        width,
        isFixedLeft,
        isFixedRight,
        className: columnClassName,
        required,
        info
      } = column;

      // Clone all column properties before over write
      const newColumn: ColumnType = { style: {}, ...column };

      // Add field width to prevent useTable add default value for width (150)
      newColumn.width = width;

      // Generate className
      newColumn.className = classnames(
        columnClassName,
        (isFixedLeft || isFixedRight) && classConst.grid.FIXED,
        id === get(lastLeft, 'id') && classConst.grid.FIXED_LAST_LEFT,
        id === get(firstRight, 'id') && classConst.grid.FIXED_FIRST_RIGHT
      );

      // Update positions (style.left, style.right) of fixed columns
      // left position equals to total width of previous fixed columns
      if (isFixedLeft) {
        newColumn.style!.left = `${currentWidthLeft}px`;
        currentWidthLeft += width || 0;
      }

      // right position equals to total width of next fixed columns
      if (isFixedRight) {
        sumWidthRight -= width || 0;
        newColumn.style!.right = `${sumWidthRight}px`;
      }

      // column is required or not
      newColumn.required = required;

      // info column
      newColumn.info = info;

      return newColumn;
    });

    let newTotalWidth;

    // calculate all column's width
    // [totalWidth] will be used for setting subTable's width
    if (!newCols.find((column) => !column.width && !column.columns)) {
      newTotalWidth = newCols.reduce((sum, column) => {
        let columnWidth = 0;

        if (column.width) columnWidth = column.width;

        // table has sub Header
        if (column.columns)
          columnWidth = (column.columns as ColumnType[]).reduce(
            (subSum, column) => subSum + (column.width || 0),
            0
          );

        return sum + columnWidth;
      }, 0);
    }

    setTotalWidth(newTotalWidth);
    setColumns(newCols);
  }, [columnsProp, variant, expand, togglable, idToggleColumn]);

  const getRowId = useCallback(
    (row: Record<string, any>) => {
      return get(row, dataItemKey!);
    },
    [dataItemKey]
  );

  const gridInstance = useTable(
    {
      columns,
      data,
      useControlledState,
      stateReducer,
      getRowId: dnd ? getRowId : undefined
    },
    useSortBy
  );

  useLayoutEffect(() => {
    dispatch({ type: 'SET_PROPS', totalWidth });
  }, [totalWidth, dispatch]);

  useLayoutEffect(() => {
    dispatch({ type: 'SET_GRID_INSTANCE', gridInstance });
  }, [gridInstance, dispatch]);

  useLayoutEffect(() => {
    setRendered(true);
  }, []);

  useLayoutEffect(() => {
    if (!isFunction(onLoadMore) || !isTriggerLoadMore || !isRendered) return;
    setTriggerLoadMore(() => {
      onLoadMore();
      return false;
    });
  }, [onLoadMore, isTriggerLoadMore, isRendered]);

  useLayoutEffect(() => {
    const resizeObserver = new ResizeObserver(() => {
      dispatch({
        type: 'SET_PROPS',
        headerClientWidth: headerRef.current?.clientWidth
      });

      // check scrollable when window resize
      if (bodyRef.current && headerRef.current && maxHeight) {
        const bodyScrollHeight = bodyRef.current.scrollHeight;
        const headerOffsetHeight = headerRef.current.offsetHeight;

        // scroll when grid's height greater than maxHeight
        const isScrollable = bodyScrollHeight + headerOffsetHeight > maxHeight;

        // after render data, [onLoadMore] will be triggered
        //   if the height of grid is less than maxHeight
        if (!isScrollable) setTriggerLoadMore(true);

        // [scrollable] is used for render loadElement
        dispatch({ type: 'SET_PROPS', scrollable: isScrollable });
        dispatch({
          type: 'SET_PROPS',
          maxBodyHeight: isScrollable
            ? maxHeight - headerOffsetHeight - 2 // width of grid border
            : undefined
        });
      }
    });

    resizeObserver.observe(gridRef.current!);

    return () => resizeObserver.disconnect();
  }, [dispatch, maxHeight]);

  useLayoutEffect(() => {
    const resizeObserver = new ResizeObserver(
      (entry: ResizeObserverEntry[]) => {
        const { width } = entry[0].contentRect;

        // table will has scroll bar when table's is smaller than total column widths
        /* istanbul ignore next */
        const hasHorizontalScroll = totalWidth && width < totalWidth;

        // prevent update hasHorizontalScroll with the same value
        if (!hasHorizontalScrollState !== !hasHorizontalScroll) {
          dispatch({ type: 'SET_PROPS', hasHorizontalScroll });
        }

        // trigger resizeTableBody when grid size was changed
        resizeTableBody(bodyRef.current);
      }
    );

    resizeObserver.observe(gridRef.current!);

    return () => resizeObserver.disconnect();
  }, [totalWidth, data, hasHorizontalScrollState, dispatch]);

  const isWindow = isWindowPlatform();
  const tableElement = (
    <table
      ref={gridRef}
      {...gridInstance.getTableProps([
        {
          className: classnames(
            classConst.grid.GRID,
            dataFooter && classConst.grid.WITH_FOOTER,
            (alwaysBorderFixedColumn || hasHorizontalScrollState) &&
              classConst.grid.HAS_FIXED_BORDER,
            isWindow && classConst.grid.GRID_FOR_WINDOW,
            className
          )
        }
      ])}
      id={id}
      data-testid={dataTestId}
    >
      <colgroup>
        {columns.map((col) => (
          <col key={col.id} width={col.width}></col>
        ))}
      </colgroup>

      <GridHeader
        ref={headerRef}
        className={headerClassName}
        dataTestId={dataTestId}
      />

      <GridBody
        ref={bodyRef}
        className={bodyClassName}
        dataTestId={dataTestId}
      />

      {!isEmpty(dataFooter) && (
        <GridFooter
          ref={footerRef}
          className={footClassName}
          dataTestId={dataTestId}
        />
      )}
    </table>
  );

  useEffect(() => {
    if (!dnd) return setDragLayer(null);

    const dragLayerElement = ReactDOM.createPortal(
      <DragLayer dndId={dndId} />,
      document.body
    );

    setDragLayer(dragLayerElement);
  }, [dnd, dndId]);

  return dnd ? (
    <DndProvider backend={HTML5Backend}>
      {tableElement}
      {dragLayer}
    </DndProvider>
  ) : (
    tableElement
  );
};

export default forwardRef<GridRef, GridBaseProps>(GridBase);
