import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { GridProvider } from './GridContext';
import GridBase from './GridBase';

describe('test GridBase', () => {
  it('with horizontal scroll', () => {
    render(
      <GridProvider
        value={{
          state: {
            scrollable: true,
            staticHeaderId: 'staticHeaderId',
            hasHorizontalScroll: true,
            columns: [],
            dataFooter: {},
            gridInstance: {
              headerGroups: [
                {
                  getHeaderGroupProps: () => ({}),
                  headers: [
                    {
                      id: 'staticHeaderId',
                      placeholderOf: 'asd',
                      render: () => undefined,
                      getHeaderProps: () => ({}),
                      originalId: 'staticHeaderId',
                      totalFlexWidth: 1
                    },
                    {
                      id: 'staticHeaderId',
                      placeholderOf: 'asd',
                      render: () => undefined,
                      getHeaderProps: () => ({}),
                      originalId: 'staticHeaderId',
                      totalFlexWidth: 1
                    }
                  ]
                }
              ],
              rows: [],
              getTableBodyProps: () => ({})
            }
          },
          dispatch: () => undefined
        }}
      >
        <GridBase />
      </GridProvider>
    );
    expect(true).toBeTruthy();
  });
});
