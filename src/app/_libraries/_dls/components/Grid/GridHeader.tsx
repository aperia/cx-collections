import React, {
  FC,
  useState,
  ReactNode,
  useRef,
  useEffect,
  useImperativeHandle,
  RefForwardingComponent,
  forwardRef,
  useContext,
  useMemo
} from 'react';
import classnames from 'classnames';
import isFunction from 'lodash.isfunction';
import isUndefined from 'lodash.isundefined';
import isEmpty from 'lodash.isempty';
import isEqual from 'lodash.isequal';
import { HeaderGroup, TableHeaderProps } from 'react-table';

// Types
import { GridHeaderProps, SortType, CellType, ColumnType } from './types';

// Utils
import { className as classConst, genAmtId } from '../../utils';
import isString from 'lodash.isstring';

// Providers
import { StorageProvider } from '../../providers';

// Contexts
import { GridContext } from './GridContext';

// Components
import Icon from '../Icon';
import CheckBox from '../CheckBox';
import PopupBase, { PopupBaseRef } from '../../components/PopupBase';
import Tooltip from '../Tooltip';
import { isBoolean } from '../../lodash';

const mapOrder = (order?: string | boolean) => {
  // descending: undefined -> 'asc' -> 'desc' -> undefined -> ...
  if (isUndefined(order)) return { current: undefined, next: 'asc' };

  if (order === 'asc' || !order) return { current: 'asc', next: 'desc' };

  return { current: 'desc', next: undefined };
};

const customHeaderGroup = (
  headerGroups: HeaderGroup<Record<string, unknown>>[],
  headerClientWidth = 0,
  staticHeaderId?: string,
  hasHorizontalScroll?: boolean
) => {
  // table don't have sub header
  if (headerGroups.length < 2) return headerGroups;

  // table has sub header
  //  ______________________________________________________________
  // |       XX       |     Available Status to Change To           |
  // | Current Status | Normal | A - Auth Prohibited | B - Bankrupt |
  // |________________|________|_____________________|______________|
  // react-table will add header cell (XX cell) for all Header columns which don't have subHeader
  // We need to replace XX cell's configs by header cell's configs (Current Status)
  headerGroups[0].headers = headerGroups[0].headers.map((header) => {
    if (!header.placeholderOf) return header;

    return {
      ...header, // XX cell's configs
      ...header.placeholderOf, // header cell's configs (Current Status)
      // don't display Header in this cell,
      //    but don't remove text Header to let xx cell has height
      cellHeaderProps: { style: { color: 'transparent' } },
      placeholderOf: undefined
    };
  });

  if (staticHeaderId) {
    headerGroups[0].headers = headerGroups[0].headers.map((header: any) => {
      if (
        header.id === staticHeaderId ||
        header.originalId === staticHeaderId
      ) {
        header.width = hasHorizontalScroll
          ? headerClientWidth - header.totalLeft
          : header.totalFlexWidth;
        header.className = 'static-header-cell';
        header.cellHeaderProps = {
          style: { left: hasHorizontalScroll ? `${header.totalLeft}px` : '0' }
        };
      }

      return header;
    });
  }

  return headerGroups;
};

const GridHeader: RefForwardingComponent<
  HTMLTableSectionElement,
  GridHeaderProps
> = ({ className, dataTestId }, ref) => {
  const {
    state: {
      staticHeaderId,
      headerClientWidth,
      hasHorizontalScroll,
      gridInstance: { headerGroups }
    }
  } = useContext(GridContext);

  const headerRef = useRef<HTMLTableSectionElement | null>(null);

  useImperativeHandle(ref, () => headerRef.current!);

  return (
    <thead
      ref={headerRef}
      className={classnames(classConst.grid.GRID_HEAD, className)}
    >
      {customHeaderGroup(
        headerGroups,
        headerClientWidth,
        staticHeaderId,
        hasHorizontalScroll
      ).map((group, index) => {
        const { key: rowKey, ...rowProps } = group.getHeaderGroupProps();
        const isNeedFakeColumn =
          hasHorizontalScroll && headerGroups.length > 1 && index === 0;

        return (
          <tr key={rowKey} {...rowProps}>
            {(group.headers as any[]).map((cell, index) => {
              const {
                id,
                originalId,
                isSortedDesc,
                width,
                totalFlexWidth,
                required,
                info
              } = cell;

              let fakeColumnWidth = 0;

              if (
                isNeedFakeColumn &&
                // get fake column width in last cell
                index === group.headers.length - 1 &&
                originalId === staticHeaderId
              ) {
                fakeColumnWidth = totalFlexWidth;
              }

              return (
                <React.Fragment key={id}>
                  <TableHeaderCell
                    key={id}
                    cell={cell}
                    width={width}
                    isSortedDesc={isSortedDesc}
                    required={required}
                    info={info}
                    dataTestId={dataTestId}
                  />
                  {!!fakeColumnWidth && (
                    <td
                      key="fake-column"
                      className="fake-column"
                      width={fakeColumnWidth}
                    />
                  )}
                </React.Fragment>
              );
            })}
          </tr>
        );
      })}
    </thead>
  );
};

declare interface TableHeaderCellProps extends DLSId {
  cell: CellType;
  isSortedDesc?: boolean;
  width?: number;
  required?: ColumnType['required'];
  info?: ColumnType['info'];
}

const TableHeaderCell: FC<TableHeaderCellProps> = ({
  cell,
  // trigger useEffect when sort change for case uncontrolled
  isSortedDesc,
  width: widthBase,
  required,
  info,
  dataTestId
}) => {
  const {
    state: {
      variant,
      dataHeader,
      isSortMulti,
      sortBy,
      indeterminate,
      readOnly,
      checkAll,
      filteringId,
      onSortChange,
      onCheckRowHeader,
      hasHorizontalScroll
    }
  } = useContext(GridContext);

  // states
  const [cellProps, setCellProps] = useState<TableHeaderProps | null>(null);
  const [order, setOrder] = useState<string | undefined>(undefined);

  // refs
  const cellRef = useRef<HTMLTableHeaderCellElement | null>(null);

  // variables
  const suffixTestId = isString(cell?.Header)
    ? `header-${cell?.Header}`
    : 'header';
  const headerCellTestId = genAmtId(dataTestId, suffixTestId, 'GridHeader');

  const tooltip = useMemo(() => {
    if (!variant?.tooltipProps) return;

    const {
      displayFor = 'default',
      displayForCallback,
      ...tooltipProps
    } = variant.tooltipProps;
    const isDisplayForCallback = displayForCallback?.();

    return {
      isDisplay: isBoolean(isDisplayForCallback)
        ? isDisplayForCallback
        : displayFor === 'default'
        ? readOnly
        : displayFor === 'read-only',
      tooltipProps
    };
  }, [variant, readOnly]);

  useEffect(() => {
    const {
      id,
      cellProps,
      cellHeaderProps,
      className,
      style,
      isSort,
      autoWidth
    } = cell;

    let width: number | undefined = widthBase;

    // if table isn't scrolling, this cell's width will be removed
    if (autoWidth && !hasHorizontalScroll) {
      width = undefined;
    }

    const extraProps = [
      { ...cellProps },
      { ...cellHeaderProps },
      { className, style },
      { width },
      { title: undefined } // remove title
    ];

    // [isSortedDesc] is updated by react-table, so it need to map for match with [SortType]
    // isSortedDesc: true | false | undefined
    // order: 'desc' | 'asc' | undefined
    let { current: currentOrder } = mapOrder(isSortedDesc);

    // uncontrolled
    let sortValue: SortType = { id, order: currentOrder as any };

    // controlled
    if (sortBy && !isEmpty(sortBy)) {
      sortValue = sortBy.find((item) => item.id === id) || { id };
      currentOrder = sortValue.order;
    }

    if (isSort) {
      const { next: nextOrder } = mapOrder(sortValue.order);

      extraProps.push(
        { className: classConst.grid.SORT },
        {
          onClick: (event: React.MouseEvent<HTMLElement>) => {
            // if (!cellRef || cellRef.current !== event.target) return;

            isFunction(onSortChange) &&
              onSortChange({ id, order: nextOrder as any });

            // controlled
            if (!isEmpty(sortBy)) return;

            // uncontrolled
            // react-table will check shiftKey for making sort action is multi or not
            // set shiftKey is true to let react-table know we are sorting by multi columns
            if (isSortMulti) event.shiftKey = true;
            cell.getSortByToggleProps().onClick(event);
          }
        }
      );
    }

    const mergedCellProps = cell.getHeaderProps(
      isSort ? cell.getSortByToggleProps(extraProps) : extraProps
    );

    setOrder(currentOrder);
    setCellProps(mergedCellProps);
  }, [
    cell,
    widthBase,
    isSortMulti,
    onSortChange,
    sortBy,
    isSortedDesc,
    hasHorizontalScroll
  ]);

  const requiredElement = required ? (
    <span className="asterisk color-danger">*</span>
  ) : null;

  const infoIconElement = info ? (
    <Icon size="3x" name="information" {...info?.iconProps} />
  ) : null;
  const infoTooltipElement =
    info?.tooltipProps?.element && infoIconElement ? (
      <Tooltip triggerClassName="info-icon" {...info.tooltipProps}>
        {infoIconElement}
      </Tooltip>
    ) : null;

  if (cell.type === 'checkbox') {
    const checkboxElement = (
      <CheckBox dataTestId={headerCellTestId}>
        <CheckBox.Input
          indeterminate={indeterminate}
          checked={checkAll}
          readOnly={readOnly}
          onChange={() => onCheckRowHeader?.(!checkAll)}
        />
      </CheckBox>
    );

    return (
      <th ref={cellRef} {...cellProps}>
        {tooltip?.tooltipProps?.element && tooltip?.isDisplay ? (
          <Tooltip {...tooltip?.tooltipProps}>{checkboxElement}</Tooltip>
        ) : (
          checkboxElement
        )}
        {requiredElement}
      </th>
    );
  }

  return (
    <th ref={cellRef} data-testid={headerCellTestId} {...cellProps}>
      {!isUndefined(order) && (
        <span className={classConst.grid.SORT_ICON}>
          <Icon name={order === 'desc' ? 'arrow-down' : 'arrow-up'} />
        </span>
      )}
      {!dataHeader ? cell.render('Header') : cell.accessor(dataHeader)}
      {cell.filter && (
        <FilterPopup id={cell.id} opened={isEqual(cell.id, filteringId)}>
          {cell.filter}
        </FilterPopup>
      )}
      {infoTooltipElement}
      {requiredElement}
    </th>
  );
};

export interface FilterPopupProps {
  id: string;
  opened?: boolean;
  children?: ReactNode;
}

const FilterPopup: FC<FilterPopupProps> = ({ id, opened, children }) => {
  const { dispatch } = useContext(GridContext);

  const iconRef = useRef<HTMLSpanElement | null>(null);
  const popupRef = useRef<PopupBaseRef | null>(null);

  const handleOpenMode = (nextOpened: boolean) => {
    dispatch({ type: 'SET_PROPS', filteringId: nextOpened ? '' : id });
  };

  return (
    <>
      <span
        ref={iconRef}
        className={classnames(
          classConst.grid.FILTER_BUTTON,
          opened && classConst.grid.FILTERING
        )}
        onClick={() => handleOpenMode(!!opened)}
      >
        <Icon className={classConst.grid.FILTER_ICON} name="filter" />
      </span>
      <StorageProvider>
        <PopupBase
          ref={popupRef}
          reference={iconRef.current!}
          opened={opened}
          placement="bottom-end"
          onVisibilityChange={handleOpenMode}
        >
          {children}
        </PopupBase>
      </StorageProvider>
    </>
  );
};

export default forwardRef<HTMLTableSectionElement, GridHeaderProps>(GridHeader);
