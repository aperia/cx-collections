import {
  HTMLAttributes,
  MouseEvent,
  CSSProperties,
  ReactNode,
  Dispatch
} from 'react';
import { ButtonProps } from 'react-bootstrap';

import {
  Column,
  HeaderGroup,
  UseSortByColumnProps,
  TableState as TableStateBase,
  SortingRule,
  Row,
  TableBodyPropGetter
} from 'react-table';
import { IconProps } from '../Icon';
import { TooltipProps } from '../Tooltip';

type VariantDefault = {
  id: string;
  order?: number;
  cellProps?: HTMLAttributes<HTMLElement>;
  cellHeaderProps?: HTMLAttributes<HTMLElement>;
  cellBodyProps?: HTMLAttributes<HTMLElement>;
  cellFooterProps?: HTMLAttributes<HTMLElement>;
  Footer?: string;
  width?: number;
  isFixedLeft?: boolean;
  isFixedRight?: boolean;

  /**
   * - ONLY WORKS FOR VARIANT CHECKBOX (thead cell). If you need configure tooltipProps for tbody cell, please check readOnlyCheckboxTooltipProps on Grid's props
   * - Config and display tooltip option, default: display on readOnly or disabled checkbox
   * - Warning: disabled is unsupported
   */
  tooltipProps?: Omit<CheckBoxTooltipProps, 'id'>;
};

export type Variant = {
  type: 'checkbox' | 'radio' | 'radio-check' | 'dnd';
} & VariantDefault;

export type Expand = VariantDefault;

// Types - Context
export type GridStateProps = {
  data: any;
  rowKey?: string;
  totalWidth?: number;
  dataHeader: any;
  dataFooter: any;
  gridInstance: {
    rows: Row[];
    headerGroups: HeaderGroup[];
    footerGroups: HeaderGroup[];
    prepareRow: (row: Row) => void;
    getTableBodyProps: (propGetter?: TableBodyPropGetter<any>) => any;
  };
  isSortMulti?: boolean;
  scrollable?: boolean;
  sortBy?: SortType[];
  loadElement?: ReactNode;
  columns: ColumnType[];
  onSortChange?: (sort: SortType) => void;
  onLoadMore?: () => void;

  filteringId?: string;

  variant?: Variant;
  readOnly?: boolean;
  indeterminate?: boolean;
  checkAll?: boolean;
  dataItemKey?: string;
  checkedList?: string[];
  checkboxTooltipPropsList?: CheckBoxTooltipProps[];
  readOnlyCheckbox?: string[];
  onCheckRowBody?: (isChecked: boolean, item: Record<string, any>) => void;
  onCheckRowHeader?: (isChecked: boolean) => void;
  onCheckRadioRowBody?: (item: Record<string, any>) => void;
  subRow?: any;
  expandedItemKey?: string;
  expandedList?: string[];
  expand?: Expand;
  togglable?: boolean;
  onExpand?: (isExpanded: boolean, item: Record<string, any>) => void;
  toggleButtonConfigList?: GridProps['toggleButtonConfigList'];

  expandedText?: string;
  collapsedText?: string;

  dndId?: string;
  dnd?: boolean;
  dndReadOnly?: boolean;
  onRowMove?: (dragIndex: number, hoverIndex: number) => void;

  hasHorizontalScroll?: boolean;
  alwaysBorderFixedColumn?: boolean;

  maxHeight?: number;
  maxBodyHeight?: number;
  headerClientWidth?: number;
  staticHeaderId?: string;
};

export type DispatchProps = {
  type: string;
  [order: string]: any;
};

export type GridReducer = (
  prevState: GridStateProps,
  action: DispatchProps
) => GridStateProps;

export type GridContextArgs = {
  state: GridStateProps;
  dispatch: Dispatch<DispatchProps>;
};

// Types - Component
export type SortType = {
  id: string;
  order?: 'desc' | 'asc' | undefined;
};

export type ColumnType<T extends Record<string, any> = Record<string, any>> = {
  id: string;
  type?: 'checkbox' | 'radio' | 'radio-check' | 'expand' | 'dnd';
  cellProps?: HTMLAttributes<HTMLElement>;
  cellHeaderProps?: HTMLAttributes<HTMLElement>;
  cellBodyProps?: HTMLAttributes<HTMLElement>;
  cellFooterProps?: HTMLAttributes<HTMLElement>;
  Footer?: string;
  width?: number;
  isSort?: boolean;
  filter?: ReactNode;
  isFixedLeft?: boolean;
  isFixedRight?: boolean;
  action?: string;
  className?: string;
  style?: CSSProperties;
  autoWidth?: boolean;
  columns?: ColumnType<T>[];
  required?: boolean;
  info?: {
    iconProps?: IconProps;
    tooltipProps?: TooltipProps;
  };
} & Column<T>;

export type TableSortByToggleProps = {
  onClick: (event: MouseEvent) => void;
};

export type CellType = {
  accessor: (row: Row) => any;
  getSortByToggleProps: (
    props?: Partial<TableSortByToggleProps>
  ) => TableSortByToggleProps;
} & HeaderGroup &
  ColumnType &
  UseSortByColumnProps<any>;

export interface CheckBoxTooltip {
  isDisplay: boolean;
  tooltipProps: TooltipProps;
}

export interface CheckBoxTooltipProps extends TooltipProps {
  /**
   * - [id] defined in data
   * - Example:
   *   data = [
   *      {
   *          uniqueId: 12345
   *      }
   *   ]
   *
   *   -> id='uniqueId'
   */
  id: string;
  /**
   * - Display tooltip for cases
   * - Warning: disabled is unsupported
   */
  displayFor?: 'default' | 'read-only' | 'disabled';
  /**
   * - If you need to control the display of the tooltip by using a callback
   */
  displayForCallback?: () => boolean;
}

export interface ToggleButtonConfig {
  /**
   * - [id]: value defined in data
   */
  id: string;
  expandedTooltipProps?: TooltipProps;
  collapsedTooltipProps?: TooltipProps;
  buttonProps?: ButtonProps;
}

export type TableState = {
  sortBy?: SortType[];
  sortByItem?: SortingRule<any>;
} & TableStateBase;

// Interfaces
export interface GridRef {
  gridElement?: HTMLTableElement | null;
  gridHeaderElement?: HTMLTableSectionElement | null;
  gridBodyElement?: HTMLTableSectionElement | null;
  gridFooterElement?: HTMLTableSectionElement | null;
}

export interface GridBaseProps extends DLSId {
  className?: string;
  classes?: {
    head?: string;
    body?: string;
    foot?: string;
  };
}

export interface GridProps extends GridBaseProps {
  data: any[];
  dataHeader?: any;
  dataFooter?: any;
  columns: ColumnType[] | ColumnType<any>[];

  fixed?: boolean;

  scrollable?: boolean;

  isSortMulti?: boolean;
  sortBy?: SortType[];
  onSortChange?: (sort: SortType) => void;

  loadElement?: ReactNode;
  onLoadMore?: () => void;

  variant?: Variant;
  dataItemKey?: string;
  checkedList?: string[];
  /**
   * - ONLY WORKS FOR VARIANT CHECKBOX (tbody cell). If you need configure tooltipProps for thead cell, please check variant.tooltipProps on Grid's props
   * - Config and display tooltip option, default: display on readOnly or disabled checkbox
   * - Warning: disabled is unsupported
   */
  checkboxTooltipPropsList?: CheckBoxTooltipProps[];
  readOnlyCheckbox?: string[];
  onCheck?: (dataKeyList: string[]) => void;
  /**
   * - If you need control checkAll for thead
   */
  checkAllControlled?: (
    dataKeyList: string[]
  ) => 'checked' | 'indeterminate' | 'uncheck';

  subRow?: any;
  expand?: Expand;
  togglable?: boolean;
  expandedItemKey?: string;
  expandedList?: string[];
  isExpand?: boolean;
  onExpand?: (dataKeyList: string[]) => void;
  expandedText?: string;
  collapsedText?: string;
  toggleButtonConfigList?: ToggleButtonConfig[];

  rowKey?: string;

  dnd?: boolean;
  dndReadOnly?: boolean;
  onDataChange?: (nextData: GridProps['data']) => void;

  alwaysBorderFixedColumn?: boolean;
  maxHeight?: number;
  staticHeaderId?: string;
}

export interface GridHeaderProps extends DLSId {
  className?: string;
}

export interface GridBodyProps extends DLSId {
  className?: string;
}

export interface GridFooterProps extends DLSId {
  className?: string;
}
