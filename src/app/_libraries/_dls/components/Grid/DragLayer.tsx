import React, { CSSProperties } from 'react';

import { useDragLayer, XYCoord } from 'react-dnd';
import { GridStateProps } from './types';

export interface DragLayerProps {
  dndId: GridStateProps['dndId'];
}

const DragLayer: React.FC<DragLayerProps> = ({ dndId }) => {
  const { item, initialOffset, currentOffset } = useDragLayer((monitor) => ({
    item: monitor.getItem(),
    initialOffset: monitor.getInitialSourceClientOffset(),
    currentOffset: monitor.getClientOffset(),
    isDragging: monitor.isDragging()
  }));

  const getDragLayerStyles = (
    initialOffset: XYCoord | null,
    currentOffset: XYCoord | null,
    rest: any
  ): CSSProperties => {
    if (!initialOffset || !currentOffset) {
      return {
        display: 'none'
      };
    }

    const { x: left, y: top } = currentOffset;
    return { left: left - 24, top: top - 24, ...rest };
  };

  return item?.type === dndId && item?.template ? (
    <table
      className="dls-grid dls-drag-layer"
      style={getDragLayerStyles(initialOffset, currentOffset, {
        width: item?.width,
        backgroundColor: item?.backgroundColor
      })}
    >
      <tbody>{item.template}</tbody>
    </table>
  ) : null;
};

export default DragLayer;
