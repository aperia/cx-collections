import React, {
  RefForwardingComponent,
  forwardRef,
  useRef,
  useImperativeHandle,
  useContext
} from 'react';
import classnames from 'classnames';

// Utils
import { className as classConst } from '../../utils';

// Contexts
import { GridContext } from './GridContext';

// Types
import { GridFooterProps, CellType } from './types';

const generateCellProps = (cell: CellType) => {
  const { cellProps, cellFooterProps, width, style, className } = cell;

  const extraProps = [
    { ...cellProps },
    { ...cellFooterProps },
    { className, width, style }
  ];

  const { key: cellKey, ...mergedCellProps } = cell.getFooterProps(extraProps);

  return { cellKey, mergedCellProps };
};

const GridFooter: RefForwardingComponent<
  HTMLTableSectionElement,
  GridFooterProps
> = ({ className }, ref) => {
  const footerRef = useRef<HTMLTableSectionElement | null>(null);

  const {
    state: {
      dataFooter,
      gridInstance: { footerGroups }
    }
  } = useContext(GridContext);

  useImperativeHandle(ref, () => footerRef.current!);

  return (
    <tfoot
      ref={footerRef}
      className={classnames(classConst.grid.GRID_FOOTER, className)}
    >
      {footerGroups.map((group) => {
        const { key: rowKey, ...rowProps } = group.getFooterGroupProps();

        return (
          <tr key={rowKey} {...rowProps}>
            {(group.headers as CellType[]).map((cell) => {
              const { cellKey, mergedCellProps } = generateCellProps(cell);

              return (
                <td key={cellKey} {...mergedCellProps}>
                  {cell.accessor(dataFooter)}
                </td>
              );
            })}
          </tr>
        );
      })}
    </tfoot>
  );
};

export default forwardRef<HTMLTableSectionElement, GridFooterProps>(GridFooter);
