export const statementHistory = [
  {
    NA_statementDate: '27/07/2020',
    NA_creditCount: 1,
    NA_creditAmount: '$8674.00',
    NA_cashCount: '000',
    NA_cashAmount: '$1,031.85',
    NA_paymentCount: '009',
    NA_paymentAmount: '$13.00',
    NA_adjustmentCount: '0010',
    NA_adjustmentAmount: '$42.00',
    NA_aprCash: '45.81%',
    NA_aPRMerchant: '82.29%',
    NA_lateFees: '$0.00',
    NA_overLimitFees: '$0.00',
    NA_financeChargeP: '$255.30',
    NA_financeChargeC: '$1.90',
    NA_cashBalanceADB: '$44.81',
    NA_purchaseBalanceADB: '$229.59',
    NA_behavior: '55',
    NA_bureauScore: '603'
  },
  {
    NA_statementDate: '25/07/2020',
    NA_creditCount: 1,
    NA_creditAmount: '$8674.00',
    NA_cashCount: '000',
    NA_cashAmount: '$1,031.85',
    NA_paymentCount: '009',
    NA_paymentAmount: '$13.00',
    NA_adjustmentCount: '0010',
    NA_adjustmentAmount: '$42.00',
    NA_aprCash: '45.81%',
    NA_aPRMerchant: '82.29%',
    NA_lateFees: '$0.00',
    NA_overLimitFees: '$0.00',
    NA_financeChargeP: '$255.30',
    NA_financeChargeC: '$1.90',
    NA_cashBalanceADB: '$44.81',
    NA_purchaseBalanceADB: '$229.59',
    NA_behavior: '55',
    NA_bureauScore: '603'
  },
  {
    NA_statementDate: '25/07/2020',
    NA_creditCount: 1,
    NA_creditAmount: '$8674.00',
    NA_cashCount: '000',
    NA_cashAmount: '$1,031.85',
    NA_paymentCount: '009',
    NA_paymentAmount: '$13.00',
    NA_adjustmentCount: '0010',
    NA_adjustmentAmount: '$42.00',
    NA_aprCash: '45.81%',
    NA_aPRMerchant: '82.29%',
    NA_lateFees: '$0.00',
    NA_overLimitFees: '$0.00',
    NA_financeChargeP: '$255.30',
    NA_financeChargeC: '$1.90',
    NA_cashBalanceADB: '$44.81',
    NA_purchaseBalanceADB: '$229.59',
    NA_behavior: '55',
    NA_bureauScore: '603'
  },
  {
    NA_statementDate: '25/07/2020',
    NA_creditCount: 1,
    NA_creditAmount: '$8674.00',
    NA_cashCount: '000',
    NA_cashAmount: '$1,031.85',
    NA_paymentCount: '009',
    NA_paymentAmount: '$13.00',
    NA_adjustmentCount: '0010',
    NA_adjustmentAmount: '$42.00',
    NA_aprCash: '45.81%',
    NA_aPRMerchant: '82.29%',
    NA_lateFees: '$0.00',
    NA_overLimitFees: '$0.00',
    NA_financeChargeP: '$255.30',
    NA_financeChargeC: '$1.90',
    NA_cashBalanceADB: '$44.81',
    NA_purchaseBalanceADB: '$229.59',
    NA_behavior: '55',
    NA_bureauScore: '603'
  },
  {
    NA_statementDate: '25/07/2020',
    NA_creditCount: 1,
    NA_creditAmount: '$8674.00',
    NA_cashCount: '000',
    NA_cashAmount: '$1,031.85',
    NA_paymentCount: '009',
    NA_paymentAmount: '$13.00',
    NA_adjustmentCount: '0010',
    NA_adjustmentAmount: '$42.00',
    NA_aprCash: '45.81%',
    NA_aPRMerchant: '82.29%',
    NA_lateFees: '$0.00',
    NA_overLimitFees: '$0.00',
    NA_financeChargeP: '$255.30',
    NA_financeChargeC: '$1.90',
    NA_cashBalanceADB: '$44.81',
    NA_purchaseBalanceADB: '$229.59',
    NA_behavior: '55',
    NA_bureauScore: '603'
  },
  {
    NA_statementDate: '25/07/2020',
    NA_creditCount: 1,
    NA_creditAmount: '$8674.00',
    NA_cashCount: '000',
    NA_cashAmount: '$1,031.85',
    NA_paymentCount: '009',
    NA_paymentAmount: '$13.00',
    NA_adjustmentCount: '0010',
    NA_adjustmentAmount: '$42.00',
    NA_aprCash: '45.81%',
    NA_aPRMerchant: '82.29%',
    NA_lateFees: '$0.00',
    NA_overLimitFees: '$0.00',
    NA_financeChargeP: '$255.30',
    NA_financeChargeC: '$1.90',
    NA_cashBalanceADB: '$44.81',
    NA_purchaseBalanceADB: '$229.59',
    NA_behavior: '55',
    NA_bureauScore: '603'
  },
  {
    NA_statementDate: '25/07/2020',
    NA_creditCount: 1,
    NA_creditAmount: '$8674.00',
    NA_cashCount: '000',
    NA_cashAmount: '$1,031.85',
    NA_paymentCount: '009',
    NA_paymentAmount: '$13.00',
    NA_adjustmentCount: '0010',
    NA_adjustmentAmount: '$42.00',
    NA_aprCash: '45.81%',
    NA_aPRMerchant: '82.29%',
    NA_lateFees: '$0.00',
    NA_overLimitFees: '$0.00',
    NA_financeChargeP: '$255.30',
    NA_financeChargeC: '$1.90',
    NA_cashBalanceADB: '$44.81',
    NA_purchaseBalanceADB: '$229.59',
    NA_behavior: '55',
    NA_bureauScore: '603'
  }
];

export const statementHistoryHeader = [
  '27/07/2020',
  '07/07/2020',
  '06/25/2020',
  '05/27/2020',
  '04/26/2020',
  '03/27/2020'
];

export const statementHistoryQA = [
  {
    label: 'New Balance',
    items: [
      '$3,550.82',
      '$3,550.82',
      '$3,550.82',
      '$3,550.82',
      '$3,550.82',
      '$3,550.82'
    ]
  },
  {
    label: 'Min Pay Due',
    items: ['$1,031.85', '$558.29', '$971.28', '$883.92', '$171.49', '$457.79 ']
  },
  {
    label: 'Due Date',
    items: [
      '02/25/2020',
      '08/29/2020',
      '09/27/2020',
      '12/17/2020',
      '06/27/2020',
      '06/02/2020'
    ]
  },
  {
    label: 'Past Due',
    items: ['$175.99', '$211.46', '$114.86', '$86.07', '$294.93', '$187.81 ']
  },
  {
    label: 'Cash balance',
    items: ['$96.36', '$26.36', '$8.91', '$41.12', '$14.81', '$11.63 ']
  },
  {
    label: 'Available Credit',
    items: ['16.30%', '16.30%', '16.30%', '16.30%', '16.30%', '16.30%']
  },
  {
    label: 'Credit Limit',
    items: [
      '$11,220.00',
      '$11,220.00',
      '$11,220.00',
      '$11,220.00',
      '$11,220.00',
      '$11,220.00'
    ]
  },
  {
    label: 'Purchase Count',
    items: ['510', '154', '769', '797', '469', '774']
  },
  {
    label: 'Purchase Amount',
    items: ['$0.00', '$0.00', '$0.00', '$0.00', '$0.00', '$0.00']
  },
  {
    label: 'Credit Count',
    items: ['8', '7', '6', '5', '4', '3']
  },
  {
    label: 'Credit Amount',
    items: ['$229.59', '$31.76', '$1.78', '$240.66', '$21.94', '$88.55 ']
  },
  // layout 2
  {
    label: 'Cash Count',
    items: ['000', '000', '000', '000', '000', '000']
  },
  {
    label: 'Cash amount',
    items: ['$1,031.85', '$558.29', '$971.28', '$883.92', '$171.49', '$457.79 ']
  },
  {
    label: 'Payment count',
    items: ['001', '001', '001', '001', '001', '001']
  },
  {
    label: 'Payment Amount',
    items: ['$175.99', '$211.46', '$114.86', '$86.07', '$294.93', '$187.81 ']
  },
  {
    label: 'Adjustment count',
    items: ['001', '001', '001', '001', '001', '001']
  },
  {
    label: 'Adjustment AMount',
    items: ['16.30%', '16.30%', '16.30%', '16.30%', '16.30%', '16.30%']
  },
  {
    label: 'apr cash',
    items: ['45.81%', '64.54%', '72.65%', '50.13%', '97.58%', '53.23%']
  },
  {
    label: 'APR merchant',
    items: ['82.29%', '93.79%', '82.75%', '94.27%', '46.11%', '65.97%']
  },
  {
    label: 'Late Fees',
    items: ['$0.00', '$0.00', '$0.00', '$0.00', '$0.00', '$0.00']
  },
  {
    label: 'OverLimit fees',
    items: ['$0.00', '$0.00', '$0.00', '$0.00', '$0.00', '$0.00']
  },
  {
    label: 'Finance Charge (P)',
    items: ['$255.30', '$260.14', '$31.25', '$131.05', '$206.09', '$10.89 ']
  },
  // layout 3
  {
    label: 'Adjustment AMount',
    items: ['16.30%', '16.30%', '16.30%', '16.30%', '16.30%', '16.30%']
  },
  {
    label: 'apr cash',
    items: ['45.81%', '64.54%', '72.65%', '50.13%', '97.58%', '53.23%']
  },
  {
    label: 'APR merchant',
    items: ['82.29%', '93.79%', '82.75%', '94.27%', '46.11%', '65.97%']
  },
  {
    label: 'Late Fees',
    items: ['$0.00', '$0.00', '$0.00', '$0.00', '$0.00', '$0.00']
  },
  {
    label: 'OverLimit fees',
    items: ['$0.00', '$0.00', '$0.00', '$0.00', '$0.00', '$0.00']
  },
  {
    label: 'Finance Charge (P)',
    items: ['$255.30', '$260.14', '$31.25', '$131.05', '$206.09', '$10.89 ']
  },
  {
    label: 'Finance charge (C)',
    items: ['$1.90', '$1.65', '$42.03', '$16.95', '$43.94', '$16.04 ']
  },
  {
    label: 'Cash balance ADB',
    items: [
      '$44.81',
      '$107.32',
      '$764.23',
      '$1,162.54',
      '$1,109.97',
      '$615.75 '
    ]
  },
  {
    label: 'Purchase Balance ADB',
    items: ['$229.59', '$31.76', '$1.78', '$240.66', '$21.94', '$88.55 ']
  },
  {
    label: 'Behavior',
    items: ['713', '713', '713', '713', '713', '713']
  },
  {
    label: 'Bureau Score',
    items: ['804', '804', '804', '804', '804', '804']
  }
];

export const pendingAuthorizations = [
  {
    dateTime: {
      date: '12/12/2020',
      time: '11:00:15 AM'
    },
    approval: 'DAS006',
    amount: '$582.78',
    availCredit: '$99,417.22',
    description: 'LIONS EYE HOSPITAL',
    actionText: 'View'
  },
  {
    dateTime: {
      date: '12/12/2020',
      time: '02:20:25 PM'
    },
    approval: 'DAS006',
    amount: '$1,031.62',
    availCredit: '$98,385.60',
    description: 'WHOLESALEBOX INTERNET',
    actionText: 'View'
  },
  {
    dateTime: {
      date: '12/12/2020',
      time: '08:45:10 AM'
    },
    approval: 'DAS006',
    amount: '$607.91',
    availCredit: '$97,777.69',
    description: 'EXPLORE BANARAS TECHNO',
    actionText: 'View'
  },
  {
    dateTime: {
      date: '12/12/2020',
      time: '08:05:45 PM'
    },
    approval: 'DAS006',
    amount: '$1,103.76',
    availCredit: '$96,673.33',
    description: 'LIONS EYE HOSPITAL',
    actionText: 'View'
  },
  {
    dateTime: {
      date: '11/12/2020',
      time: '08:20:35 AM'
    },
    approval: 'DAS006',
    amount: '$1,195.07',
    availCredit: '$95,381.26',
    description: 'GOLDIN WASTE SERVICES',
    actionText: 'View'
  },
  {
    dateTime: {
      date: '11/12/2020',
      time: '08:20:35 AM'
    },
    approval: 'DAS006',
    amount: '$45.01',
    availCredit: '$95,334.25',
    description: 'LIONS EYE HOSPITAL',
    actionText: 'View'
  },
  {
    dateTime: {
      date: '11/12/2020',
      time: '11:10:55 PM'
    },
    approval: 'DAS006',
    amount: '$890.99',
    availCredit: '$94,124.11',
    description: 'WANDERTRAILS SERVICES',
    actionText: 'View'
  },
  {
    dateTime: {
      date: '11/12/2020',
      time: '10:40:15 PM'
    },
    approval: 'DAS006',
    amount: '$163.51',
    availCredit: '$93,014.00',
    description: 'WHOLESALEBOX INTERNET',
    actionText: 'View'
  },
  {
    dateTime: {
      date: '09/12/2020',
      time: '01:50:50 PM'
    },
    approval: 'DAS006',
    amount: '$418.44',
    availCredit: '$92,592.00',
    description: 'WHOLESALEBOX INTERNET',
    actionText: 'View'
  },
  {
    dateTime: {
      date: '09/12/2020',
      time: '05:05:15 AM'
    },
    approval: 'DAS006',
    amount: '$1,148.38',
    availCredit: '$91,392.50',
    description: 'LIONS EYE HOSPITAL',
    actionText: 'View'
  }
];

export const transactionList = [
  {
    transDate: '12/16/2020',
    postingDate: '12/16/2020',
    amount: '$984.71 ',
    statementDetails: 'DHFL PRAMERICA LIFE INSURNACE',
    actions: 'dropdownAction'
  },
  {
    transDate: '12/14/2020',
    postingDate: '12/14/2020',
    amount: '$723.15 ',
    statementDetails: 'SCHOOLKART TECHNOLOGIES',
    actionType: 'dropdownAction'
  },
  {
    transDate: '12/14/2020',
    postingDate: '12/14/2020',
    amount: '$612.76 ',
    statementDetails: 'A.M. MARKETPLACES',
    actions: 'dropdownAction'
  },
  {
    transDate: '12/09/2020',
    postingDate: '12/09/2020',
    amount: '$569.30 ',
    statementDetails: 'INNERCHEF PRIVATE',
    actions: 'dropdownAction'
  },
  {
    transDate: '12/09/2020',
    postingDate: '12/09/2020',
    amount: '$875.36 ',
    statementDetails: 'MEP RGSL TOLL BRIDGE',
    actions: 'dropdownAction'
  },
  {
    transDate: '12/04/2020',
    postingDate: '12/04/2020',
    amount: '$1,031.62 ',
    statementDetails: 'CUREHUB HEALTHCARE',
    actions: 'dropdownAction'
  },
  {
    transDate: '12/04/2020',
    postingDate: '12/04/2020',
    amount: '$415.20 ',
    statementDetails: 'MN & C SUPPLY LINKS RETAIL',
    actions: 'dropdownAction'
  },
  {
    transDate: '12/04/2020',
    postingDate: '12/04/2020',
    amount: '$491.57 ',
    statementDetails: 'RELIANCE LIFE INSURANCE',
    actions: 'dropdownAction'
  },
  {
    transDate: '12/04/2020',
    postingDate: '12/04/2020',
    amount: '$635.07 ',
    statementDetails: 'THE RIVER AND RAIL CAFÉ',
    actions: 'dropdownAction'
  },
  {
    transDate: '12/04/2020',
    postingDate: '12/04/2020',
    amount: '$462.83 ',
    statementDetails: 'GIANT TECH LABS',
    actions: 'dropdownAction'
  }
];

export const payments = [
  {
    id: { name: 'data-1' },
    scheduledDate: '09/20/2020',
    status: 0,
    amount: '$100.00',
    accountNumber: '****1234',
    confirmationNumber: 'HDKSL-348KDS'
  },
  {
    id: { name: 'data-2' },
    scheduledDate: '09/19/2020',
    status: 2,
    amount: '$60.00',
    accountNumber: '****1534',
    confirmationNumber: 'HDKSL-348KDS'
  },
  {
    id: { name: 'data-3' },
    scheduledDate: '09/17/2020',
    status: 0,
    amount: '$60.00',
    accountNumber: '****1231',
    confirmationNumber: 'HDKSL-348KDS'
  },
  {
    id: { name: 'data-4' },
    scheduledDate: '09/16/2020',
    status: 0,
    amount: '$1240.00',
    accountNumber: '****1256',
    confirmationNumber: 'HDKSL-348KDS'
  },
  {
    id: { name: 'data-5' },
    scheduledDate: '09/10/2020',
    status: 1,
    amount: '$1240.00',
    accountNumber: '****9834',
    confirmationNumber: 'HDKSL-348KDS'
  }
];

export const bankInformationList = [
  {
    id: 'id_1',
    accountNumber: '• • • • • 9102',
    type: 'Checking',
    routingNumber: '• • • • • 3981',
    bankName: 'Texas Capital Bank'
  },
  {
    id: 'id_2',
    accountNumber: '• • • • • 9103',
    type: 'Checking',
    routingNumber: '• • • • • 3983',
    bankName: 'NewYork Bank'
  }
];

export const subTableData = [
  {
    postingDate: '01/03/2021',
    effectiveDate: '01/03/2021',
    amountPaid: '$48.88',
    description: '967 - Account Level Processing SM strategy change message.'
  },
  {
    postingDate: '01/02/2021',
    effectiveDate: '01/29/2021',
    amountPaid: '$50.00',
    description:
      '912 - Memo posted payment, payment reversal, payment amount adjustment, or other monetary adjustment to the account.'
  }
];

export const paymentHistory = [
  {
    cycleDate: '01/02/2021',
    dueDate: '01/30/2021',
    minimumPaymentDue: '$205.94',
    DELQAmount: '$5.13',
    daysDELQ: '0'
  },
  {
    cycleDate: '12/02/2020',
    dueDate: '12/30/2020',
    minimumPaymentDue: '$58.36',
    DELQAmount: '$241.05',
    daysDELQ: '30'
  },
  {
    cycleDate: '11/02/2020',
    dueDate: '11/30/2020',
    minimumPaymentDue: '$98.88',
    DELQAmount: '$75.53',
    daysDELQ: '60'
  },
  {
    cycleDate: '10/02/2020',
    dueDate: '10/30/2020',
    minimumPaymentDue: '$150.78',
    DELQAmount: '$233.81',
    daysDELQ: '90'
  },
  {
    cycleDate: '09/02/2020',
    dueDate: '09/30/2020',
    minimumPaymentDue: '$170.94',
    DELQAmount: '$28.48',
    daysDELQ: '0'
  },
  {
    cycleDate: '08/02/2020',
    dueDate: '08/30/2020',
    minimumPaymentDue: '$59.24',
    DELQAmount: '$256.52',
    daysDELQ: '30'
  },
  {
    cycleDate: '07/02/2020',
    dueDate: '07/30/2020',
    minimumPaymentDue: '$77.69',
    DELQAmount: '$110.85',
    daysDELQ: '60'
  },
  {
    cycleDate: '06/02/2020',
    dueDate: '06/30/2020',
    minimumPaymentDue: '$70.26',
    DELQAmount: '$1.49',
    daysDELQ: '90'
  },
  {
    cycleDate: '05/02/2020',
    dueDate: '05/30/2020',
    minimumPaymentDue: '$251.11',
    DELQAmount: '$11.28',
    daysDELQ: '0'
  },
  {
    cycleDate: '04/02/2020',
    dueDate: '04/30/2020',
    minimumPaymentDue: '$260.14',
    DELQAmount: '$171.32',
    daysDELQ: '30'
  },
  {
    cycleDate: '03/02/2020',
    dueDate: '03/30/2020',
    minimumPaymentDue: '$251.11',
    DELQAmount: '$11.28',
    daysDELQ: '0'
  },
  {
    cycleDate: '02/02/2020',
    dueDate: '02/30/2020',
    minimumPaymentDue: '$260.14',
    DELQAmount: '$171.32',
    daysDELQ: '30'
  }
];
