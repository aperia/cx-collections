import React, {
  useRef,
  HTMLAttributes,
  useImperativeHandle,
  useMemo,
  useEffect,
  useState
} from 'react';
import classNames from 'classnames';
import { isEmpty, isFunction, isNumber } from '../../lodash';
import { useDeepValue, useTranslation } from '../../hooks';
import { genAmtId } from '../../utils';

export interface DataProgress {}
export interface ProgressBarProps
  extends Omit<HTMLAttributes<HTMLDivElement>, ''>,
    DLSId {
  data: {
    values: number[];
    colors: string[];
    labels?: string[];
    average?: number;
  };
  renderPercent?: (
    values: number[],
    viewsValues?: ValuesView[]
  ) => React.ReactElement;
  renderTopView?: (
    values: number[],
    viewsValues?: ValuesView[]
  ) => React.ReactElement;
  renderAverage?: (average?: number) => React.ReactElement;
}

interface ValuesView {
  color: string;
  label?: string;
  value: number;
}

const getPercentage = (now: number, max: number) => {
  const percentage = (now * 100) / max;
  return Math.round(percentage);
};

const ProgressBar: React.RefForwardingComponent<
  HTMLDivElement,
  ProgressBarProps
> = (props, ref) => {
  const { t } = useTranslation();
  const {
    data: { average, colors, values, labels },
    className,
    renderAverage,
    renderTopView,
    renderPercent,

    dataTestId,
    ...rest
  } = props;

  const containerRef = useRef<HTMLDivElement>(null);
  const deepValues = useDeepValue(values);
  const deepColors = useDeepValue(colors);
  const deepLabels = useDeepValue(labels);
  const [valuesView, setValuesView] = useState<ValuesView[]>();

  // render view in top progress bar
  const topView = useMemo(() => {
    if (!valuesView || isEmpty(valuesView)) {
      return null;
    }

    const content = [...valuesView]
      .reverse()
      .map(({ value }) => value)
      .join(' / ');

    return renderTopView ? (
      renderTopView(deepValues, valuesView)
    ) : (
      <div className="dls-progress-bar-top">{content}</div>
    );
  }, [deepValues, renderTopView, valuesView]);

  // render progress and line average
  const contentView = useMemo(() => {
    if (!valuesView || isEmpty(valuesView)) {
      return null;
    }
    const _values = [...valuesView];
    const { value: max } = _values[0];
    const isShowLineAverage = isNumber(average);

    return (
      <>
        {/* render progress bar */}
        <div className="dls-group-progress">
          {_values?.map(({ value, color, label }, index) => {
            const percentage = getPercentage(value, max);

            return (
              <div
                key={`${value}-${index}`}
                className="dls-item-progress"
                style={{
                  width: `${percentage}%`,
                  backgroundColor: color
                }}
              >
                {label}
              </div>
            );
          })}
        </div>
        {/* render line */}
        {isShowLineAverage && (
          <div
            className="dls-progress-line"
            style={{
              marginLeft: `${getPercentage(average!, max)}%`,
              zIndex: _values.length + 1
            }}
          />
        )}
      </>
    );
  }, [average, valuesView]);

  // render view after right progress bar
  const percentView = useMemo(() => {
    if (renderPercent && isFunction(renderPercent)) {
      return renderPercent(deepValues, valuesView);
    }

    if (!valuesView || isEmpty(valuesView) || valuesView.length > 2) {
      return null;
    }

    const percent = ((valuesView[1].value / valuesView[0].value) % 100) * 100;
    const content = Math.round(percent) + '%';

    return <div className="dls-progress-bar-percent">{content}</div>;
  }, [deepValues, renderPercent, valuesView]);

  // render view in bottom progress bar
  const bottomView = useMemo(() => {
    if (!isNumber(average)) return null;

    if (renderAverage && isFunction(renderAverage)) {
      return renderAverage(average);
    }
    return (
      <div className="dls-progress-bar-bottom">
        {t('Team Average')}: {average}
      </div>
    );
  }, [average, renderAverage, t]);

  // throw error if length color || label !== value
  useEffect(() => {
    if (!deepValues || isEmpty(deepValues)) return;

    if (deepColors && deepValues.length !== deepColors.length) {
      throw new Error('Length Color will equal with length value!');
    }

    if (deepLabels && deepValues.length !== deepLabels.length) {
      throw new Error('Length labels will equal with length value!');
    }

    const newValueViews = deepValues.map((value, index: number) => ({
      value,
      label: deepLabels ? deepLabels[index] : '',
      color: deepColors[index]
    }));

    newValueViews.sort((a, b) => b.value - a.value);
    setValuesView(newValueViews);
  }, [deepColors, deepValues, deepLabels]);

  useImperativeHandle(ref, () => containerRef.current!);

  return (
    <div
      ref={containerRef}
      className={classNames('dls-progress-bar', className)}
      data-testid={genAmtId(dataTestId, 'dls-progress-bar', 'ProgressBar')}
      {...rest}
    >
      {topView}
      <div className="dls-progress-bar-content">
        {contentView}
        {percentView}
      </div>
      {bottomView}
    </div>
  );
};

export default React.forwardRef<HTMLDivElement, ProgressBarProps>(ProgressBar);
