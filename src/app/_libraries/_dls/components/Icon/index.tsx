import React, { HTMLAttributes, useRef, useImperativeHandle } from 'react';

// utils
import classnames from 'classnames';
import { IconNames, IconSizes, IconColors } from './constants';
import { genAmtId } from '../../utils';

export interface IconProps
  extends Omit<HTMLAttributes<HTMLSpanElement>, 'size'>,
    DLSId {
  name?: IconNames;
  size?: IconSizes;
  color?: IconColors;
  className?: string;
}

const Icon: React.RefForwardingComponent<HTMLSpanElement, IconProps> = (
  { name = 'cross', size, color, className, id, dataTestId, ...props },
  ref
) => {
  const iconRef = useRef<HTMLSpanElement | null>(null);
  useImperativeHandle(ref, () => iconRef.current!);

  const rootClassName = classnames(
    `icon icon-${name}`,
    size && `size-${size}`,
    color && `color-${color}`,
    className
  );

  return (
    <i
      ref={iconRef}
      className={rootClassName}
      id={id}
      data-testid={genAmtId(dataTestId, 'dls-icon-${name}', 'Icon')}
      {...props}
    />
  );
};

export * from './constants';
export default React.forwardRef<HTMLSpanElement, IconProps>(Icon);
