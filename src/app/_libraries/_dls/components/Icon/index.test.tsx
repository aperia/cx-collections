import React from 'react';
import { render } from '@testing-library/react';
// component
import Icon from './index';

describe('Icon component', () => {
  it('should render with name undefined', () => {
    const ref = React.createRef<HTMLSpanElement>();
    const wrapper = render(<Icon ref={ref} />);
    expect(
      wrapper.container.querySelector('i[class="icon icon-cross"]')
    ).not.toBeNull();
  });

  it('should render class "icon icon-account-contact"', () => {
    const ref = React.createRef<HTMLSpanElement>();
    const wrapper = render(<Icon ref={ref} name="account-contact" />);
    expect(
      wrapper.container.querySelector('i[class="icon icon-account-contact"]')
    ).not.toBeNull();
  });

  it('should render class "icon icon-account-contact size-2x" when having size', () => {
    const ref = React.createRef<HTMLSpanElement>();
    const wrapper = render(<Icon ref={ref} name="account-contact" size="2x" />);
    expect(
      wrapper.container.querySelector(
        'i[class="icon icon-account-contact size-2x"]'
      )
    ).not.toBeNull();
  });

  it('should render class "icon icon-account-contact color-blue" when having color', () => {
    const ref = React.createRef<HTMLSpanElement>();
    const wrapper = render(
      <Icon ref={ref} name="account-contact" color="blue" />
    );
    expect(
      wrapper.container.querySelector(
        'i[class="icon icon-account-contact color-blue"]'
      )
    ).not.toBeNull();
  });

  it('should render class "icon icon-account-contact test-class" when having className', () => {
    const ref = React.createRef<HTMLSpanElement>();
    const wrapper = render(
      <Icon ref={ref} name="account-contact" className="test-class" />
    );
    expect(
      wrapper.container.querySelector(
        'i[class="icon icon-account-contact test-class"]'
      )
    );
  });

  describe('check type icon', () => {
    it('should render class "icon icon-account-contact"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="account-contact" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-account-contact"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-account-details"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="account-details" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-account-details"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-add-file"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="add-file" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-add-file"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-arrow-down"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="arrow-down" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-arrow-down"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-arrow-left"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="arrow-left" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-arrow-left"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-arrow-right"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="arrow-right" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-arrow-right"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-arrow-up"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="arrow-up" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-arrow-up"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-article-id"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="article-id" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-article-id"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-billing"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="billing" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-billing"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-calendar"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="calendar" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-calendar"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-chat"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="chat" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-chat"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-chevron-right"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="chevron-right" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-chevron-right"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-chevron-down"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="chevron-down" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-chevron-down"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-chevron-left"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="chevron-left" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-chevron-left"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-chevron-up"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="chevron-up" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-chevron-up"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-client-communications"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="client-communications" />);
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-communications"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-close"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="close" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-close"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-collapse-vertical"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="collapse-vertical" />);
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-collapse-vertical"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-exit-fullscreen"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="exit-fullscreen" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-exit-fullscreen"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-comingsoon"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="comingsoon" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-comingsoon"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-comment"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="comment" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-comment"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-communty"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="communty" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-communty"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-cross"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="cross" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-cross"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-download"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="download" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-download"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-edit"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="edit" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-edit"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-email"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="email" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-email"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-error"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="error" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-error"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-expand-vertical"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="expand-vertical" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-expand-vertical"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-fullscreen"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="fullscreen" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-fullscreen"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-eye-hide"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="eye-hide" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-eye-hide"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-eye"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="eye" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-eye"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-file"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="file" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-file"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-information"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="information" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-information"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-knowledge-base"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="knowledge-base" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-knowledge-base"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-megaphone"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="megaphone" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-megaphone"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-minus"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="minus" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-minus"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-more"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="more" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-more"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-most-view-articles"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="most-view-articles" />);
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-most-view-articles"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-phone"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="phone" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-phone"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-plus"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="plus" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-plus"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-popout"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="popout" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-popout"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-product-info"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="product-info" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-product-info"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-home"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="home" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-home"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-push-left"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="push-left" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-push-left"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-push-right"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="push-right" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-push-right"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-rating-line"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="rating-line" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-rating-line"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-reload"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="reload" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-reload"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-reports"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="reports" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-reports"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-request"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="request" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-request"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-search-result"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="search-result" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-search-result"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-search"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="search" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-search"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-services"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="services" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-services"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-filter"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="filter" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-filter"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-sprint"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="sprint" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-sprint"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-star-fill"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="star-fill" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-star-fill"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-success"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="success" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-success"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-system-status"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="system-status" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-system-status"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-thumb-up"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="thumb-up" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-thumb-up"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-time"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="time" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-time"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-view-grid"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="view-grid" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-view-grid"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-view-list"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="view-list" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-view-list"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-warning"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="warning" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-warning"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-check"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="check" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-check"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-collapse-all"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="collapse-all" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-collapse-all"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-expand-all"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="expand-all" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-expand-all"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-more-vertical"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="more-vertical" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-more-vertical"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-sort-by"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="sort-by" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-sort-by"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-account-information"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="account-information" />);
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-account-information"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-card-collection"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="card-collection" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-card-collection"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-card-master"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="card-master" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-card-master"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-card-visa"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="card-visa" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-card-visa"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-change"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="change" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-change"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-collection-information"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="collection-information" />);
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-collection-information"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-delete"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="delete" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-delete"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-pin"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="pin" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-pin"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-unpin"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="unpin" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-unpin"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-quick-action"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="quick-action" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-quick-action"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-attachment"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="attachment" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-attachment"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-action-list"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="action-list" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-action-list"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-cardholder"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="cardholder" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-cardholder"]')
      ).not.toBeNull();
    });

    it('should render class "icon icon-client-contact"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(<Icon ref={ref} name="client-contact" />);
      expect(
        wrapper.container.querySelector('i[class="icon icon-client-contact"]')
      ).not.toBeNull();
    });
  });

  describe('check icon size', () => {
    it('should render class "icon icon-client-contact size-2x"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(
        <Icon ref={ref} name="client-contact" size="2x" />
      );
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-contact size-2x"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-client-contact size-3x"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(
        <Icon ref={ref} name="client-contact" size="3x" />
      );
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-contact size-3x"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-client-contact size-4x"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(
        <Icon ref={ref} name="client-contact" size="4x" />
      );
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-contact size-4x"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-client-contact size-5x"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(
        <Icon ref={ref} name="client-contact" size="5x" />
      );
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-contact size-5x"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-client-contact size-6x"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(
        <Icon ref={ref} name="client-contact" size="6x" />
      );
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-contact size-6x"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-client-contact size-7x"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(
        <Icon ref={ref} name="client-contact" size="7x" />
      );
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-contact size-7x"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-client-contact size-8x"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(
        <Icon ref={ref} name="client-contact" size="8x" />
      );
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-contact size-8x"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-client-contact size-9x"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(
        <Icon ref={ref} name="client-contact" size="9x" />
      );
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-contact size-9x"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-client-contact size-11x"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(
        <Icon ref={ref} name="client-contact" size="11x" />
      );
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-contact size-11x"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-client-contact size-12x"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(
        <Icon ref={ref} name="client-contact" size="12x" />
      );
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-contact size-12x"]'
        )
      ).not.toBeNull();
    });
  });

  describe('check color', () => {
    it('should render class "icon icon-client-contact color-blue"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(
        <Icon ref={ref} name="client-contact" color="blue" />
      );
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-contact color-blue"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-client-contact color-green"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(
        <Icon ref={ref} name="client-contact" color="green" />
      );
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-contact color-green"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-client-contact color-grey"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(
        <Icon ref={ref} name="client-contact" color="grey" />
      );
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-contact color-grey"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-client-contact color-orange"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(
        <Icon ref={ref} name="client-contact" color="orange" />
      );
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-contact color-orange"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-client-contact color-purple"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(
        <Icon ref={ref} name="client-contact" color="purple" />
      );
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-contact color-purple"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-client-contact color-red"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(
        <Icon ref={ref} name="client-contact" color="red" />
      );
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-contact color-red"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-client-contact color-cyan"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(
        <Icon ref={ref} name="client-contact" color="cyan" />
      );
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-contact color-cyan"]'
        )
      ).not.toBeNull();
    });

    it('should render class "icon icon-client-contact color-white"', () => {
      const ref = React.createRef<HTMLSpanElement>();
      const wrapper = render(
        <Icon ref={ref} name="client-contact" color="white" />
      );
      expect(
        wrapper.container.querySelector(
          'i[class="icon icon-client-contact color-white"]'
        )
      ).not.toBeNull();
    });
  });
});
