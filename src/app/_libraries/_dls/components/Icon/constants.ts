const iconNames = <const>[
  'account-contact',
  'account-details',
  'add-file',
  'arrow-down',
  'arrow-left',
  'arrow-right',
  'arrow-up',
  'article-id',
  'billing',
  'calendar',
  'chat',
  'chevron-right',
  'chevron-down',
  'chevron-left',
  'chevron-up',
  'client-communications',
  'close',
  'collapse-vertical',
  'exit-fullscreen',
  'comingsoon',
  'comment',
  'communty',
  'cross',
  'download',
  'edit',
  'email',
  'error',
  'expand-vertical',
  'fullscreen',
  'eye-hide',
  'eye',
  'file',
  'information',
  'knowledge-base',
  'megaphone',
  'minus',
  'more',
  'most-view-articles',
  'phone',
  'plus',
  'popout',
  'product-info',
  'home',
  'push-left',
  'push-right',
  'rating-line',
  'reload',
  'reports',
  'request',
  'search-result',
  'search',
  'services',
  'filter',
  'sprint',
  'star-fill',
  'success',
  'system-status',
  'thumb-up',
  'time',
  'view-grid',
  'view-list',
  'warning',
  'check',
  'collapse-all',
  'expand-all',
  'more-vertical',
  'sort-by',
  'account-information',
  'card-collection',
  'card-master',
  'card-visa',
  'change',
  'collection-information',
  'delete',
  'pin',
  'unpin',
  'quick-action',
  'attachment',
  'action-list',
  'cardholder',
  'client-contact',
  'lock',
  'notification',
  'workflow',
  'approve-chain',
  'account-level',
  'fee-penalty',
  'run-transaction',
  'bold',
  'italic',
  'list-bullet',
  'list-number',
  'link',
  'text-color',
  'underline',
  'align-right',
  'align-center',
  'align-left',
  'indent',
  'outdent',
  'drag',
  'globe',
  'email-send',
  'notification-manage',
  'method-create',
  'method-add',
  'method-clone',
  'price-strategy',
  'price-manage',
  'card-new',
  'account-change',
  'credit-flow',
  'strike-through',
  'unlink',
  'quote',
  'view-html',
  'clear-formatting',
  'undo',
  'redo',
  'preformatted-text-box',
  'align-justified',
  'cart',
  'file-clock-setting',
  'account-remove',
  'scissor',
  'gift',
  'account-transfer',
  'flow-money',
  'flow-card',
  'flow-test',
  'flow-loan',
  'spell-check',
  'image',
  'account-setting',
  'account-stop',
  'agency',
  'arrow-change',
  'arrow-navigator',
  'document-create',
  'highlight',
  'no-color',
  'run-non-monetary',
  'card-flag',
  'account-group',
  'fax',
  'IVR',
  'mail',
  'voice',
  'walkin',
  'application-options',
  'design',
  'data-management',
  'email-management',
  'web',
  'grid',
  'background-color',
  'image-url',
  'unlock',
  'manage-ocs',
  'manage-account-level',
  'utilities',
  'manage-payoff'
];

const iconSizes = <const>[
  '2x',
  '3x',
  '4x',
  '5x',
  '6x',
  '7x',
  '8x',
  '9x',
  '10x',
  '11x',
  '12x'
];

const iconColors = <const>[
  'blue',
  'green',
  'grey',
  'orange',
  'purple',
  'red',
  'cyan',
  'white'
];

export { iconNames, iconSizes, iconColors };
export type IconNames = typeof iconNames[number];
export type IconSizes = typeof iconSizes[number];
export type IconColors = typeof iconColors[number];
