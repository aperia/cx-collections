import React, { PropsWithChildren } from 'react';

import { Tab } from 'react-bootstrap';
import ToggleButton, { ToggleButtonProps } from './ToggleButton';

export interface ContentProps
  extends ToggleButtonProps,
    PropsWithChildren<any>,
    DLSId {
  hideCollapsedButton?: boolean;
}

const Content: React.FC<ContentProps> = ({
  hideCollapsedButton,
  expandMessage,
  collapseMessage,
  children,
  collapsed,
  onToggleCollapsed,

  dataTestId,
  ...props
}) => (
  <Tab.Content {...props}>
    {hideCollapsedButton ? null : (
      <ToggleButton
        collapsed={collapsed}
        onToggleCollapsed={onToggleCollapsed}
        expandMessage={expandMessage}
        collapseMessage={collapseMessage}
        dataTestId={dataTestId}
      />
    )}
    {children}
  </Tab.Content>
);

export default Content;
