import React, {
  forwardRef,
  ForwardRefExoticComponent,
  ForwardRefRenderFunction,
  PropsWithChildren,
  RefAttributes,
  useImperativeHandle,
  useRef
} from 'react';

// utils
import classNames from 'classnames';

// components/types
import {
  Nav as ReactBSNav,
  NavProps as ReactBSNavProps
} from 'react-bootstrap';

export interface NavProps extends ReactBSNavProps, PropsWithChildren<any> {
  collapsed?: boolean;
  isCollapsing?: boolean;
}

export interface NavRef {}

export interface ExtraStaticProp
  extends ForwardRefExoticComponent<NavProps & RefAttributes<NavRef>> {
  Item: typeof ReactBSNav.Item;
  Link: typeof ReactBSNav.Link;
}

const Nav: ForwardRefRenderFunction<NavRef, NavProps> = (
  { collapsed, isCollapsing, children, ...props },
  ref
) => {
  const navRef = useRef(null);
  useImperativeHandle(ref, () => navRef.current!);

  return (
    <ReactBSNav
      className={classNames('flex-column', {
        'is-collapsed': collapsed,
        'is-collapsing': isCollapsing
      })}
      ref={navRef}
      {...props}
    >
      {children}
    </ReactBSNav>
  );
};

const ExtraStaticNav = forwardRef<NavRef, NavProps>(Nav) as ExtraStaticProp;

ExtraStaticNav.Item = ReactBSNav.Item;
ExtraStaticNav.Link = ReactBSNav.Link;

export default ExtraStaticNav;
