import React, {
  Children,
  cloneElement,
  forwardRef,
  ForwardRefExoticComponent,
  ForwardRefRenderFunction,
  isValidElement,
  RefAttributes,
  useCallback,
  useEffect,
  useImperativeHandle,
  useMemo,
  useRef,
  useState
} from 'react';

// components/types
import { Tab, TabContainerProps } from 'react-bootstrap';
import Nav from './Nav';
import Content, { ContentProps } from './Content';

// utils
import isBoolean from 'lodash.isboolean';
import isFunction from 'lodash.isfunction';
import classNames from 'classnames';
import { className, genAmtId } from '../../utils';

export interface TabsProps extends TabContainerProps, ContentProps, DLSId {
  children?: React.ReactNode;
}

export interface ExtraStaticProp
  extends ForwardRefExoticComponent<TabsProps & RefAttributes<HTMLDivElement>> {
  Nav: typeof Nav;
  Content: typeof Content;
  Pane: typeof Tab.Pane;
}

const Tabs: ForwardRefRenderFunction<HTMLDivElement, TabsProps> = (
  {
    hideCollapsedButton,
    expandMessage,
    collapseMessage,
    collapsed: collapsedProp,
    onToggleCollapsed,
    children,
    className: classNameProp,

    id,
    dataTestId,
    ...props
  },
  ref
) => {
  const containerRef = useRef<HTMLDivElement | null>(null);
  useImperativeHandle(ref, () => containerRef.current!);

  // toggle button state
  const [collapsed, setCollapsed] = useState(
    isBoolean(collapsedProp) && collapsedProp
  );
  const [isCollapsing, setCollapsing] = useState(false);

  const handleToggleCollapsed = useCallback(() => {
    isFunction(onToggleCollapsed) && onToggleCollapsed(!collapsed);

    !isBoolean(collapsedProp) && setCollapsed(!collapsed);

    setCollapsing(true);
    setTimeout(() => {
      setCollapsing(false);
    }, 500);
  }, [collapsedProp, onToggleCollapsed, collapsed]);

  useEffect(() => {
    if (!isBoolean(collapsedProp)) return;

    setCollapsed(collapsedProp);
  }, [collapsedProp]);

  // map children
  const mappedChildren = useMemo(() => {
    return Children.map(children, (children) => {
      if (!isValidElement(children)) return children;

      if (children.type === Content)
        return cloneElement(children, {
          ...children.props,
          hideCollapsedButton,
          expandMessage,
          collapseMessage,
          collapsed,
          onToggleCollapsed: handleToggleCollapsed
        });

      if (children.type === Nav) {
        return cloneElement(children, {
          ...children.props,
          collapsed,
          isCollapsing
        });
      }

      return children;
    });
  }, [
    hideCollapsedButton,
    children,
    expandMessage,
    collapseMessage,
    collapsed,
    isCollapsing,
    handleToggleCollapsed
  ]);

  return (
    <Tab.Container {...props}>
      <div
        ref={containerRef}
        className={classNames(className.verticalTabs.CONTAINER, classNameProp)}
        id={id}
        data-testid={genAmtId(dataTestId, 'dls-vertical-tabs', 'VerticalTabs')}
      >
        {mappedChildren}
      </div>
    </Tab.Container>
  );
};

const ExtraStaticTabs = forwardRef<HTMLDivElement, TabsProps>(
  Tabs
) as ExtraStaticProp;
ExtraStaticTabs.Nav = Nav;
ExtraStaticTabs.Content = Content;
ExtraStaticTabs.Pane = Tab.Pane;

export default ExtraStaticTabs;
