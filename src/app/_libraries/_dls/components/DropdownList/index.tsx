import React, {
  useRef,
  useState,
  useEffect,
  useImperativeHandle,
  useMemo
} from 'react';

// components/types
import DropdownBase, {
  DropdownBaseChangeEvent,
  DropdownBaseRef,
  DropdownBaseGroup,
  DropdownBaseItem,
  HighLightWordsContextType,
  HighLightWordsContext
} from '../DropdownBase';
import PopupBase, { PopupBaseRef } from '../PopupBase';
import Tooltip from '../Tooltip';
import Label from '../Label';
import Bubble from '../Bubble';
import Icon from '../Icon';
import { DropdownListRef, DropdownListProps } from './types';
import { PopperRef } from '../Popper';

// utils
import classnames from 'classnames';
import {
  className,
  truncateTooltip,
  extendsEvent,
  classes,
  genAmtId
} from '../../utils';
import {
  get,
  isBoolean,
  isFunction,
  isObject,
  isString,
  pick
} from '../../lodash';

const DropdownList: React.ForwardRefRenderFunction<
  DropdownListRef,
  DropdownListProps
> = (
  {
    value: valueProp,
    onChange,

    label,
    textField,
    textFieldRender,

    onFocus,
    onBlur,
    opened: openedProp,
    onVisibilityChange,

    onFilterChange,

    // should show searchBar or not
    searchBar,
    // edit placeholder of the searchbar
    searchBarPlaceholder = 'Type to search',

    bubble,

    readOnly,
    disabled,
    placeholder,

    id,
    name,
    required,
    error,
    errorTooltipProps,

    optional = false,
    variant,
    size,
    maxWidthNoBorder = 300,

    popupBaseProps,
    autoFocus = false,
    children: childrenProp,

    anchor,
    className: classNameProp,

    tabIndex: tabIndexProp = 0,

    dataTestId,

    ...props
  },
  ref
) => {
  // refs
  const popupBaseRef = useRef<PopupBaseRef | null>(null);
  const toolTipRef = useRef<PopperRef | null>(null);
  const inputRef = useRef<HTMLInputElement | null>(null);
  const autoFocusRef = useRef(autoFocus);
  const textfieldRef = useRef<HTMLDivElement | null>(null);
  const floatingLabelRef = useRef<HTMLSpanElement | null>(null);
  const fakeFocusRef = useRef<HTMLDivElement | null>(null);
  const keepRef = useRef<Record<string, any>>({});

  // states
  const [container, setContainer] = useState<HTMLDivElement | null>(null);
  const [dropdownBase, setDropdownBase] = useState<DropdownBaseRef>();
  const [opened, setOpened] = useState(!!openedProp);
  const [value, setValue] = useState(valueProp);
  const [filter, setFilter] = useState('');
  const [children, setChildren] = useState(childrenProp);
  const [highLightWordsContextValue, setHighLightWordsContextValue] =
    useState<HighLightWordsContextType>({
      highlightTextFilter: '',
      setHighlightTextFilter: (nextHighLightTextFilter) =>
        setHighLightWordsContextValue((state) => ({
          ...state,
          highlightTextFilter: nextHighLightTextFilter
        }))
    });

  const nextChildren = useMemo(() => {
    if (!optional || !Array.isArray(children)) return children;

    const optionalElement = (
      <DropdownBaseItem key="PP-D-None" value={null} label="None" />
    );

    const result = [optionalElement, ...children];
    return result as typeof children;
  }, [children, optional]);

  // handle mousedown on DropdownList element
  const handleOnMouseDown = (
    event?: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    if (disabled) {
      event?.preventDefault();
      return;
    }

    const replacer = dropdownBase?.searchBarElement;
    const triggerElement = replacer || inputRef.current;

    if (document?.activeElement && document.activeElement === triggerElement) {
      // triggerElement.blur() will trigger blur, but focus will be trigger also, and blur will be trigger again
      // this is not behavior we want, so just preventDefault focus/blur event.
      event?.preventDefault();

      triggerElement.blur();
    }
  };

  // handle value uncontrolled/controlled
  const handleValueMode = (event: DropdownBaseChangeEvent) => {
    isFunction(onChange) && onChange(event);

    // we need to call onBlur on nextTick
    // because we need to make sure onChange has done (new value updated) before we call onBlur
    if (event.changeBy !== 'up/down') {
      const replacer = dropdownBase?.searchBarElement;
      const triggerElement = replacer || inputRef.current;
      process.nextTick(() => triggerElement?.blur());
    }

    // controlled
    if (valueProp) return;

    // uncontrolled
    setValue(event.target.value);
  };

  // handle opened uncontrolled/controlled
  const handleOpenedMode = (nextOpened: boolean) => {
    isFunction(onVisibilityChange) && onVisibilityChange(nextOpened);

    // controlled
    if (isBoolean(openedProp)) return;

    // uncontrolled
    setOpened(nextOpened);
  };

  // handle filter uncontrolled/controlled
  // for now, we removed controlled mode, just support uncontrolled mode
  const handleFilterMode = (nextFilter: string) => {
    let nextEvent = extendsEvent({} as any, 'target', {
      id,
      name,
      value: nextFilter
    });
    nextEvent = extendsEvent(nextEvent, null, { value: nextFilter });
    onFilterChange?.(nextEvent);

    // uncontrolled (remaining code below)
    setFilter(nextFilter);

    // update current text filter to Context
    const { setHighlightTextFilter } = highLightWordsContextValue;
    setHighlightTextFilter!(nextFilter);

    const nextChildren = filterChildren(nextFilter);
    setChildren(nextChildren);
  };

  // handle filter children
  // input: current filter
  // output: list child matching with current filter
  const filterChildren = (nextFilter: string) => {
    const collector: any = [];

    React.Children.forEach(childrenProp, (child) => {
      const labelFilter = get(child, ['props', 'label']) as string;

      // basic filter algorithm
      if (labelFilter.toLowerCase().indexOf(nextFilter.toLowerCase()) === -1) {
        return;
      }

      collector.push(child);
    });

    return collector as typeof children;
  };

  // handle focus on DropdownList element
  const handleOnFocus = () => {
    let nextEvent = extendsEvent({} as any, 'target', { name, id, value });
    nextEvent = extendsEvent(nextEvent, null, { value });

    onFocus?.(nextEvent);

    handleOpenedMode(true);
  };

  // handle blur on DropdownList element
  const handleOnBlur = () => {
    // if replacer is exists, we know that
    // DropdownList has search bar in dropdown, and search bar is focusing
    // search bar will handle blur
    const replacer = dropdownBase?.searchBarElement;
    if (replacer) return;

    // what if the user clicks on something that is not in the browser?
    // example: desktop, taskbar, developer tools...
    // by default, if the user clicks on those thing, the browser will keeps
    // the previous state
    // So the next time, the user clicks (mousedown) on the screen (valid container DOM browser),
    // onFocus will be called and onMouseDown will be not called
    // we don't want that behavior
    // so this code block to prevent that behavior
    if (
      document?.activeElement &&
      document.activeElement === inputRef.current
    ) {
      inputRef.current?.blur();
    }

    let nextEvent = extendsEvent({} as any, 'target', { name, id, value });
    nextEvent = extendsEvent(nextEvent, null, { value });

    onBlur?.(nextEvent);
    handleOpenedMode(false);
  };

  // handle onChange
  const handleOnChange = handleValueMode;

  // handle mousedown on icon
  const handleOnMouseDownIcon = (
    event: React.MouseEvent<HTMLSpanElement, MouseEvent>
  ) => {
    if (disabled) return;

    const replacer = dropdownBase?.searchBarElement;
    const triggerElement = replacer || inputRef.current;

    // if we call triggerElement.focus() inside mousedown icon,
    // triggerElement will be focus, and call blur also...
    // this is not behavior we want, so we need to stop default behavior browser (blur/focus)
    event.preventDefault();

    if (document?.activeElement && document.activeElement === triggerElement) {
      triggerElement?.blur();
      return;
    }

    triggerElement?.focus({ preventScroll: true });
  };

  // if the popup shown, we want the button element to stay in focus even if we
  // click on the popup, read more details in side this function
  const handleOnPopupShown = () => {
    const popupElement = popupBaseRef.current?.element;

    // handle mousedown on Popup's DropdownList
    // mousedown on Popup's DropdownList will trigger onBlur default of the DropdownList
    // that is not the behavior we want, so we need to preventDefault it
    const handleMouseDown = (event: MouseEvent) => {
      // stop browser events
      event.preventDefault();
    };

    // don't need to remove event later, because popup element will be removed from DOM
    popupElement?.addEventListener('mousedown', handleMouseDown);
  };

  // consider refactor this code
  const handleOnPopupClosed = () => {
    if (document?.activeElement === document?.body) {
      fakeFocusRef.current?.focus({ preventScroll: true });
    }

    if (!filter) return;
    handleFilterMode('');
  };

  keepRef.current.value = value;
  keepRef.current.handleValueMode = handleValueMode;
  keepRef.current.handleOpenedMode = handleOpenedMode;

  // provide ref
  const publicRef = useRef<DropdownListRef | null>();
  useImperativeHandle(ref, () => {
    if (!publicRef.current) publicRef.current = {};

    publicRef.current.popupBaseRef = popupBaseRef;
    publicRef.current.dropdownBase = dropdownBase;
    publicRef.current.inputElement = inputRef.current;

    return publicRef.current;
  });

  // set props
  useEffect(() => setOpened(!!openedProp), [openedProp]);
  useEffect(() => setValue(valueProp), [valueProp]);
  useEffect(() => setChildren(childrenProp), [childrenProp]);

  // handle truncate tooltip input text
  useEffect(() => truncateTooltip(textfieldRef.current!), [value]);

  // handle truncate tooltip label
  useEffect(() => truncateTooltip(floatingLabelRef.current!), [label]);

  // handle autofocus first time render
  useEffect(() => {
    if (!autoFocusRef.current) return;

    autoFocusRef.current = false;
    inputRef.current?.focus();
  }, []);

  // handle set max width for variant no border
  useEffect(() => {
    if (variant !== 'no-border') return;

    toolTipRef.current!.triggerElement.style.maxWidth = `${maxWidthNoBorder}px`;
  }, [maxWidthNoBorder, variant]);

  const { popupBaseClassName, ...popupProps } = popupBaseProps || {};

  // const check required
  const isRequired = required && !readOnly && !disabled;

  // check if variant is noBorder or not
  const noBorder = variant === 'no-border';

  // check small size
  const isSmallSize = size === 'small' && !noBorder;

  // check should show error or not
  // if variant is noBorder, we will ignore error
  const { message, status } = pick(error, 'message', 'status');
  const hasError = Boolean(!noBorder && status && message);
  const isOpenedTooltip = hasError && opened;

  // check no label
  const noLabel = !label || noBorder || isSmallSize;

  // check should floating label or not
  const floating =
    (!noLabel && opened && !readOnly && !disabled) ||
    value ||
    (value === null && optional);

  // text inside DropdownList
  const inputText =
    isString(textField) && isObject(value) ? get(value, textField, '') : value;

  // text 'None' inside DropdownList, for case optional
  const optionalText = value === null && optional ? 'None' : '';

  // check should show placeholder or not
  const showPlaceholder =
    !inputText && !optionalText && placeholder && !noBorder;

  // or if textFieldRender used
  const textFromTextFieldRender =
    value && isFunction(textFieldRender) && textFieldRender(value);

  // text combine with Bubble
  const inputTextWithBubble = bubble &&
    inputText &&
    !optionalText &&
    !showPlaceholder &&
    !textFromTextFieldRender && (
      <div className="with-bubble">
        <Bubble small name={inputText} />
        <span className={classes.utils.ellipsis}>{inputText}</span>
      </div>
    );

  return (
    <>
      <Tooltip
        ref={toolTipRef}
        opened={isOpenedTooltip}
        element={message}
        variant="error"
        placement="top-start"
        triggerClassName={!noBorder ? 'd-block' : ''}
        dataTestId={`${dataTestId}-dropdown-list-error`}
        {...errorTooltipProps}
      >
        <div
          ref={setContainer}
          className={classnames(
            {
              [classes.dropdownList.container]: true,
              [classes.dropdownList.noBorder]: noBorder,
              [classes.dropdownList.bubble]: inputTextWithBubble,
              [classes.state.focused]: opened,
              [classes.state.readOnly]: readOnly,
              [classes.state.disabled]: disabled,
              [classes.state.error]: hasError,
              [classes.state.floating]: floating,
              [classes.state.noLabel]: noLabel,
              [classes.state.small]: isSmallSize
            },
            classNameProp
          )}
          data-testid={genAmtId(
            dataTestId,
            'dls-dropdown-list',
            'DropdownList'
          )}
        >
          <div
            ref={inputRef}
            onMouseDown={handleOnMouseDown}
            onFocus={handleOnFocus}
            onBlur={handleOnBlur}
            tabIndex={disabled ? -1 : tabIndexProp}
            className={classes.inputContainer.container}
          >
            <div
              ref={textfieldRef}
              className={classnames({
                [classes.inputContainer.input]: true,
                [classes.inputContainer.placeholder]: showPlaceholder
              })}
            >
              {showPlaceholder
                ? placeholder
                : textFromTextFieldRender ||
                  inputTextWithBubble || (
                    <span className={classes.utils.ellipsis}>
                      {inputText || optionalText}
                    </span>
                  )}
            </div>
            {!noLabel && (
              <Label
                ref={floatingLabelRef}
                asterisk={isRequired}
                error={status}
              >
                <span>{label}</span>
              </Label>
            )}
          </div>
          <Icon
            onMouseDown={handleOnMouseDownIcon}
            size={isSmallSize ? '2x' : '3x'}
            name="chevron-down"
          />
          <div ref={fakeFocusRef} tabIndex={-1} />
        </div>
      </Tooltip>
      <PopupBase
        ref={popupBaseRef}
        reference={anchor || container!}
        opened={opened && !readOnly && !disabled}
        onVisibilityChange={handleOpenedMode}
        onPopupShown={handleOnPopupShown}
        onPopupClosed={handleOnPopupClosed}
        popupBaseClassName={classnames(
          className.popupBase.CONTAINER,
          noBorder && className.popupBase.FLUID,
          popupBaseClassName
        )}
        fluid={!noBorder}
        dataTestId={`${dataTestId}-popup-dropdown-list`}
        {...popupProps}
      >
        <HighLightWordsContext.Provider value={highLightWordsContextValue}>
          <DropdownBase
            ref={setDropdownBase as any}
            allowKeydown={opened}
            valueType={searchBar ? 'single' : 'new-behavior'}
            allowCallOnChangeByUpDown={searchBar}
            allowCallOnChangeByClick
            allowCallOnChangeByEnter
            value={value}
            onChange={handleOnChange}
            //
            highlightTextItem={searchBar}
            truncateTooltip={!searchBar}
            filter={filter}
            onFilter={handleFilterMode}
            searchBar={searchBar}
            searchBarPlaceholder={searchBarPlaceholder}
            onFocus={onFocus}
            onBlur={onBlur}
            handleOpenedMode={handleOpenedMode}
            id={id}
            name={name}
            dataTestId={dataTestId}
            {...props}
          >
            {nextChildren}
          </DropdownBase>
        </HighLightWordsContext.Provider>
      </PopupBase>
    </>
  );
};

const DropdownListExtraStaticProp = React.forwardRef<
  DropdownListRef,
  DropdownListProps
>(DropdownList) as React.ForwardRefExoticComponent<
  DropdownListProps & React.RefAttributes<DropdownListRef>
> & {
  Group: typeof DropdownBaseGroup;
  Item: typeof DropdownBaseItem;
};

DropdownListExtraStaticProp.Group = DropdownBaseGroup;
DropdownListExtraStaticProp.Item = DropdownBaseItem;

export * from './types';
export {
  DropdownBaseGroup as DropdownListGroup,
  DropdownBaseItem as DropdownListItem
};
export default DropdownListExtraStaticProp;
