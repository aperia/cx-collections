import {
  DropdownBaseProps,
  DropdownBaseRef,
  DropdownBaseItemProps,
  DropdownBaseGroupProps,
  DropdownBaseItemRef
} from '../DropdownBase';
import { PopupBaseRef, PopupBaseProps } from '../PopupBase';
import { TooltipProps } from '../Tooltip';

export interface DropdownListRef {
  popupBaseRef?: React.MutableRefObject<PopupBaseRef | null>;
  dropdownBase?: DropdownBaseRef;
  inputElement?: HTMLDivElement | null;
}

export interface DropdownListProps
  extends Omit<
      DropdownBaseProps,
      | 'selected'
      | 'onSelect'
      | 'allowKeydown'
      | 'reference'
      | 'scrollContainerProps'
    >,
    DLSId {
  label?: React.ReactNode;
  textField?: string;
  textFieldRender?: (value: any) => React.ReactElement<any>;

  onBlur?: (event: React.FocusEvent) => void;
  onFocus?: (event: React.FocusEvent) => void;

  opened?: boolean;
  onVisibilityChange?: (nextOpened: boolean) => void;

  // For easy maintenance, controlled filter is not supported
  // Just provides a mechanism to watch the filter
  onFilterChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;

  // should show Bubble inside DropdownList or not
  bubble?: boolean;

  readOnly?: boolean;
  disabled?: boolean;
  placeholder?: string;

  required?: boolean;
  error?: {
    message?: string;
    status?: boolean;
  };
  errorTooltipProps?: TooltipProps;

  popupBaseProps?: Omit<
    PopupBaseProps,
    'opened' | 'onVisibilityChange' | 'reference'
  >;
  autoFocus?: boolean;

  optional?: boolean;
  variant?: 'no-border';
  size?: 'small';
  maxWidthNoBorder?: number;

  anchor?: HTMLElement;
  tabIndex?: number;
}

export interface DropdownListItemRef extends DropdownBaseItemRef {}

export interface DropdownListItemProps extends DropdownBaseItemProps {}

export interface DropdownListGroupProps extends DropdownBaseGroupProps {}
