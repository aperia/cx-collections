enum ToolEnum {
  Bold = 'Bold',
  Italic = 'Italic',
  UnderLine = 'UnderLine',
  SubScript = 'SubScript',
  SuperScript = 'SuperScript',
  TextLeft = 'TextLeft',
  TextCenter = 'TextCenter',
  TextRight = 'TextRight',
  UnOrderedList = 'UnOrderedList',
  OrderedList = 'OrderedList',
  InOutdent = 'InOutdent',
  InsertLink = 'InsertLink',
  InsertLinkNoTarget = 'InsertLinkNoTarget',
  InsertImage = 'InsertImage',
  BlockTypes = 'BlockTypes',
  FontName = 'FontName',
  FontSize = 'FontSize',
  TextColor = 'TextColor',
  TextBackgroundColor = 'TextBackgroundColor',
  CleanFormatting = 'CleanFormatting',
  ViewHtml = 'ViewHtml',
  Print = 'Print',
  Mention = 'Mention'
}

export type ToolKeys = keyof typeof ToolEnum | ToolKeys[];

export { ToolEnum };
