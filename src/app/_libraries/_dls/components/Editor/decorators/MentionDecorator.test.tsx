import React from 'react';

// mock libs
import { render, queryByText } from '@testing-library/react';
import '@testing-library/jest-dom';
import MentionDecorator from './MentionDecorator';

describe('Decorators', () => {
  it('MentioDeconrator', () => {
    const { container } = render(
      <MentionDecorator>something</MentionDecorator>
    );

    expect(queryByText(container, 'something')).toBeInTheDocument();
  });
});
