import React from 'react';

export interface MentionDecoratorProps {}

const MentionDecorator: React.FC<MentionDecoratorProps> = (props) => {
  return <span className="dls-editor-mention-decorator">{props.children}</span>;
};

export default MentionDecorator;
