import React from 'react';

// mock libs
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import Link from './Link';

const LinkWrapper = (props: any) => {
  return (
    <Link
      contentState={
        {
          getEntity: () => ({
            getData: () => ({
              webAddress: 'webAddress',
              title: 'title',
              isOpenNewWindow: false,
              target: props.target
            })
          })
        } as any
      }
      entityKey="entityKey"
      decoratedText="decoratedText"
      {...props}
    >
      something
    </Link>
  );
};

describe('Decorators', () => {
  it('render Link with target', () => {
    const { container, rerender } = render(<LinkWrapper />);

    rerender(
      <LinkWrapper
        contentState={
          {
            getEntity: () => ({
              getData: () => ({
                webAddress: 'webAddress',
                title: 'title',
                isOpenNewWindow: true,
                target: true
              })
            })
          } as any
        }
      />
    );

    expect(container.querySelector('[target="_blank"]')).toBeInTheDocument();

    rerender(
      <LinkWrapper
        contentState={
          {
            getEntity: () => ({
              getData: () => ({
                webAddress: 'webAddress',
                title: 'title',
                isOpenNewWindow: false,
                target: true
              })
            })
          } as any
        }
      />
    );

    expect(container.querySelector('[target=""]')).toBeInTheDocument();
  });

  it('render Link without target', () => {
    const { container, rerender } = render(<LinkWrapper />);

    rerender(
      <LinkWrapper
        contentState={
          {
            getEntity: () => ({
              getData: () => ({
                webAddress: 'webAddress',
                title: 'title',
                isOpenNewWindow: true,
                target: false
              })
            })
          } as any
        }
      />
    );

    expect(
      container.querySelector('[target="_blank"]')
    ).not.toBeInTheDocument();
  });
});
