import React, { useEffect, useMemo, useState } from 'react';

import { EditorState, Modifier } from 'draft-js';
import classnames from 'classnames';

// components/types
import { DropdownBaseChangeEvent, DropdownList } from '../../..';

// contexts
import { useEditorContext } from '../../contexts';
import { getCurrentSelectedBlock } from '../../fns';
import { compact, isEmpty } from '../../../../lodash';
import { Map } from 'immutable';

export interface BlockTypesProps {}

export interface BlockType {
  description: string;
  className: string;
}

const availableBlockTypes: BlockType[] = [
  {
    description: 'Heading',
    className: 'h5'
  },
  {
    description: 'Subheading',
    className: 'dls-sub-heading'
  },
  {
    description: 'Paragraph',
    className: 'dls-paragraph'
  }
];

const BLOCK_TYPE_BLOCK_DATA_KY = 'BLOCK_TYPE_BLOCK_DATA_KY';

const BlockTypes: React.FC<BlockTypesProps> = () => {
  // contexts
  const [editorState, setEditorState] = useEditorContext();

  // states
  const [value, setValue] = useState<BlockType>();

  const defaultValue = availableBlockTypes[2];

  const blockTypeElements = useMemo(() => {
    return availableBlockTypes.map((blockType, index) => {
      return (
        <DropdownList.Item key={index} value={blockType} variant="custom">
          {blockType.className === 'h5' ? (
            <div className="h5">{blockType.description}</div>
          ) : blockType.className === 'dls-sub-heading' ? (
            <div className="dls-sub-heading">{blockType.description}</div>
          ) : (
            blockType.description
          )}
        </DropdownList.Item>
      );
    });
  }, []);

  const handleOnChange = (event: DropdownBaseChangeEvent) => {
    const nextValue = event.target.value;
    setValue(event.target.value);

    // detect and get block current
    const selectedBlock = getCurrentSelectedBlock(editorState);

    const {
      contentState,
      currentBlock,
      // comment out for Unit test, can't cover this case
      // hasAtomicBlock,
      selectionState
    } = selectedBlock;

    // comment out for Unit test, can't cover this case
    // if (hasAtomicBlock || !currentBlock) return;

    const blockData = currentBlock.getData();
    let className = blockData.get('className');
    className = isEmpty(className) ? '' : className;
    const classNameList = compact<string>(className.split(' '));

    const nextBlockType = nextValue.className;
    const nextClassName = classNameList.filter((cn) => {
      return ['h5', 'dls-sub-heading'].indexOf(cn) === -1;
    });

    const nextEditorState = EditorState.push(
      editorState,
      Modifier.mergeBlockData(
        contentState,
        selectionState,
        Map({
          [BLOCK_TYPE_BLOCK_DATA_KY]: nextBlockType,
          className: nextClassName
        })
      ),
      'change-block-data'
    );

    setEditorState(nextEditorState);

    // const selectionState = editorState.getSelection();

    // let nextEditorState = RichUtils.toggleBlockType(
    //   editorState,
    //   event.target.value.type
    // );

    // nextEditorState = EditorState.forceSelection(
    //   nextEditorState,
    //   selectionState
    // );

    // setEditorState(nextEditorState);
  };

  // watching editorState change, update value according
  useEffect(() => {
    const selectionState = editorState.getSelection();
    const startKey = selectionState.getStartKey();
    const contentBlock = editorState
      .getCurrentContent()
      .getBlockForKey(startKey);
    const blockData = contentBlock.getData();

    const currentClassName = (blockData.get(BLOCK_TYPE_BLOCK_DATA_KY) ||
      blockData.get('className') ||
      '') as string;

    const nextValue =
      availableBlockTypes.find(
        (block) => currentClassName.indexOf(block.className) !== -1
      ) || defaultValue;

    setValue(nextValue);
  }, [defaultValue, editorState]);

  return (
    <DropdownList
      value={value}
      onChange={handleOnChange}
      label={value?.description}
      textField="description"
      variant="no-border"
      className={classnames('dls-editor-tool dropdown')}
      popupBaseProps={{
        placement: 'bottom-end'
      }}
    >
      {blockTypeElements}
    </DropdownList>
  );
};

export { BLOCK_TYPE_BLOCK_DATA_KY };
export default BlockTypes;
