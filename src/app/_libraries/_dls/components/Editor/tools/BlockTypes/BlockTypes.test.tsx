import React, { useEffect, useRef } from 'react';

// testing library
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';

// mocks
import '../../../../test-utils/mocks/mockCanvas';

// components
import Editor, { EditorRef } from '../..';

const MockEditorWrapper: React.FC<any> = ({ shouldSetHTML = false }) => {
  const editorRef = useRef<EditorRef>();

  useEffect(() => {
    if (!shouldSetHTML) return;

    editorRef.current?.setHTML('<div class="h5">phat</div');
  }, [shouldSetHTML]);

  return <Editor tools={['BlockTypes', 'Bold']} ref={editorRef as any} />;
};

describe('BlockTypes', () => {
  it('should cover handleOnChange (setHTML is not called)', () => {
    jest.useFakeTimers();
    const { container, baseElement } = render(<MockEditorWrapper />);

    const dropdownListElement = container.querySelector(
      '.text-field-container'
    )!;
    userEvent.click(dropdownListElement);

    const popupElement = baseElement.querySelector('.dls-popup')!;
    const headingElement = popupElement.querySelector('.h5')!;
    userEvent.click(headingElement);
    jest.runAllTimers();

    expect(true).toBeTruthy();
  });

  it('should cover handleOnChange (setHTML is called)', () => {
    jest.useFakeTimers();
    const { container, baseElement, rerender } = render(
      <MockEditorWrapper shouldSetHTML />
    );

    rerender(<MockEditorWrapper />);

    const dropdownListElement = container.querySelector(
      '.text-field-container'
    )!;
    userEvent.click(dropdownListElement);

    const popupElement = baseElement.querySelector('.dls-popup')!;
    const headingElement = popupElement.querySelector('.h5')!;
    userEvent.click(headingElement);
    jest.runAllTimers();

    expect(true).toBeTruthy();
  });
});
