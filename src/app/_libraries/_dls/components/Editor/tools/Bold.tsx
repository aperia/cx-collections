import React from 'react';

import { RichUtils } from 'draft-js';
import Button from '../../Button';
import classnames from 'classnames';

import { useEditorContext } from '../contexts';
import Icon from '../../Icon';

export interface BoldProps {}

const Bold: React.FC<BoldProps> = () => {
  const [editorState, setEditorState] = useEditorContext();

  const handleUnOrderedList: React.MouseEventHandler<HTMLButtonElement> = (
    event
  ) => {
    event.preventDefault();

    const nextEditorState = RichUtils.toggleInlineStyle(editorState, 'BOLD');
    setEditorState(nextEditorState);
  };

  return (
    <Button
      onMouseDown={handleUnOrderedList}
      className={classnames('dls-editor-tool', {
        active: editorState.getCurrentInlineStyle().has('BOLD')
      })}
      variant="icon-secondary"
      title="Bold"
    >
      <Icon name="bold" />
    </Button>
  );
};

export default Bold;
