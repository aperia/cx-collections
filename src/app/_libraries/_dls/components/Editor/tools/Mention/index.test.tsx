import React, { useEffect, useRef } from 'react';

// testing library
import { createEvent, fireEvent, render } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';

// mocks
import '../../../../test-utils/mocks/mockCanvas';

// components
import Editor, { EditorRef } from '../..';

const MockEditorWrapper: React.FC<any> = ({
  shouldSetHTML = false,
  shouldHasMentionTextField = true,
  isText = false,
  maxPlainText
}) => {
  const editorRef = useRef<EditorRef>();

  useEffect(() => {
    if (!shouldSetHTML) return;

    editorRef.current?.setHTML('<div class="h5">phat/</div');
  }, [shouldSetHTML]);

  return (
    <Editor
      tools={['BlockTypes', 'Bold', 'Mention']}
      ref={editorRef as any}
      mentionData={
        !isText
          ? [
              { id: 1, description: 'Contact Name' },
              { id: 2, description: 'Contact Phone' },
              { id: 3, description: 'Operator ID' }
            ]
          : ['Contact Name', 'Contact Phone', 'Operator ID']
      }
      mentionTextField={shouldHasMentionTextField ? 'description' : undefined}
      maxPlainText={maxPlainText}
    />
  );
};

describe('Mention', () => {
  it('should cover if mentionData is array of aobject and mentionTextField is undefined', () => {
    const { unmount } = render(
      <MockEditorWrapper shouldHasMentionTextField={false} />
    );

    unmount();
    expect(true).toBeTruthy();
  });

  it('should cover if mentionData is array of text', () => {
    Range.prototype.getBoundingClientRect = () => ({
      width: 100,
      height: 50,
      bottom: 0,
      left: 0,
      right: 0,
      top: 0,
      x: 0,
      y: 0,
      toJSON: () => undefined
    });

    jest.useFakeTimers();
    const { container, queryByText, unmount } = render(
      <MockEditorWrapper isText />
    );
    const editorContainer = container.querySelector(
      '.public-DraftEditor-content'
    )!;

    const event = createEvent.paste(editorContainer, {
      clipboardData: {
        types: ['text/plain'],
        getData: () => 'abc/'
      }
    });
    fireEvent(editorContainer, event);

    const otherItemDropdown = queryByText('Contact Name')!;
    userEvent.click(otherItemDropdown);

    jest.runAllTimers();

    unmount();
    expect(true).toBeTruthy();
  });

  it('should cover if mentionData is array of object', () => {
    Range.prototype.getBoundingClientRect = () => ({
      width: 100,
      height: 50,
      bottom: 0,
      left: 0,
      right: 0,
      top: 0,
      x: 0,
      y: 0,
      toJSON: () => undefined
    });

    jest.useFakeTimers();
    const { queryByText, container, unmount } = render(<MockEditorWrapper />);
    const editorContainer = container.querySelector(
      '.public-DraftEditor-content'
    )!;

    const event = createEvent.paste(editorContainer, {
      clipboardData: {
        types: ['text/plain'],
        getData: () => 'abc/'
      }
    });
    fireEvent(editorContainer, event);

    const otherItemDropdown = queryByText('Contact Name')!;
    userEvent.click(otherItemDropdown);

    jest.runAllTimers();

    unmount();
    expect(true).toBeTruthy();
  });

  it('maxPlainText', () => {
    jest.useFakeTimers();
    const { queryByText, container, unmount } = render(
      <MockEditorWrapper maxPlainText={20} />
    );
    const editorContainer = container.querySelector(
      '.public-DraftEditor-content'
    )!;

    const event = createEvent.paste(editorContainer, {
      clipboardData: {
        types: ['text/plain'],
        getData: () => 'abc/'
      }
    });
    fireEvent(editorContainer, event);

    const otherItemDropdown = queryByText('Contact Name')!;
    userEvent.click(otherItemDropdown);

    jest.runAllTimers();

    unmount();
    expect(true).toBeTruthy();
  });

  it('draftEditorContainerElement should be null', () => {
    Selection.prototype.getRangeAt = () => ({
      startContainer: {
        textContent: 'text conten/t'
      },
      cloneRange: () => ({
        startContainer: {
          parentElement: undefined
        }
      })
    });

    jest.useFakeTimers();
    const { container, unmount } = render(<MockEditorWrapper />);
    const editorContainer = container.querySelector(
      '.public-DraftEditor-content'
    )!;

    const event = createEvent.paste(editorContainer, {
      clipboardData: {
        types: ['text/plain'],
        getData: () => 'abc/'
      }
    });
    fireEvent(editorContainer, event);
    jest.runAllTimers();

    unmount();
    expect(true).toBeTruthy();
  });
});
