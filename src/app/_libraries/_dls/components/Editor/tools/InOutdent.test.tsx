import React, { useEffect, useRef } from 'react';

// testing library
import { createEvent, fireEvent, render } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';

// mocks
import '../../../test-utils/mocks/mockCanvas';

// components
import Editor, { EditorRef } from '..';

const MockEditorWrapper: React.FC<any> = ({ shouldSetHTML = false }) => {
  const editorRef = useRef<EditorRef>();

  useEffect(() => {
    if (!shouldSetHTML) return;

    editorRef.current?.setHTML('<div class="h5">phat/</div');
  }, [shouldSetHTML]);

  return (
    <Editor
      tools={['BlockTypes', 'Bold', 'InOutdent']}
      ref={editorRef as any}
    />
  );
};

describe('InOutdent', () => {
  it('html without dls-indent', () => {
    const { container } = render(<MockEditorWrapper />);

    const editorContainer = container.querySelector(
      '.public-DraftEditor-content'
    )!;
    const iconIndentElement = container.querySelector('.icon-indent')!;
    const iconOutdenElement = container.querySelector('.icon-outdent')!;

    const event = createEvent.paste(editorContainer, {
      clipboardData: {
        types: ['text/plain'],
        getData: () => 'abc'
      }
    });
    fireEvent(editorContainer, event);

    userEvent.click(iconIndentElement);
    userEvent.click(iconOutdenElement);

    expect(true).toBeTruthy();
  });

  it('html with dls-indent', () => {
    const { container } = render(<MockEditorWrapper />);

    const editorContainer = container.querySelector(
      '.public-DraftEditor-content'
    )!;
    const iconIndentElement = container.querySelector('.icon-indent')!;
    const iconOutdenElement = container.querySelector('.icon-outdent')!;

    const event = createEvent.paste(editorContainer, {
      clipboardData: {
        types: ['text/plain'],
        getData: () => '<div class="dls-indent">abc</div>'
      }
    });
    fireEvent(editorContainer, event);

    userEvent.click(iconIndentElement);
    userEvent.click(iconOutdenElement);

    expect(true).toBeTruthy();
  });
});
