import React from 'react';

import { RichUtils } from 'draft-js';
import Button from '../../Button';

import { useEditorContext } from '../contexts';
import classnames from 'classnames';
import Icon from '../../Icon';

export interface ItalicProps {}

const Italic: React.FC<ItalicProps> = () => {
  const [editorState, setEditorState] = useEditorContext();

  const handleUnOrderedList: React.MouseEventHandler<HTMLButtonElement> = (
    event
  ) => {
    event.preventDefault();

    const nextEditorState = RichUtils.toggleInlineStyle(editorState, 'ITALIC');
    setEditorState(nextEditorState);
  };

  return (
    <Button
      onMouseDown={handleUnOrderedList}
      className={classnames('dls-editor-tool', {
        active: editorState.getCurrentInlineStyle().has('ITALIC')
      })}
      variant="icon-secondary"
      title="Italic"
    >
      <Icon name="italic" />
    </Button>
  );
};

export default Italic;
