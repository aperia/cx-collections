import React, { useEffect, useRef } from 'react';

// testing library
import { createEvent, fireEvent, render } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';

// mocks
import '../../../test-utils/mocks/mockCanvas';

// components
import Editor, { EditorRef } from '..';

const MockEditorWrapper: React.FC<any> = ({ shouldSetHTML = false }) => {
  const editorRef = useRef<EditorRef>();

  useEffect(() => {
    if (!shouldSetHTML) return;

    editorRef.current?.setHTML('<div class="text-left">phat/</div');
  }, [shouldSetHTML]);

  return (
    <Editor
      tools={['BlockTypes', 'Bold', 'TextLeft', 'TextCenter', 'TextRight']}
      ref={editorRef as any}
    />
  );
};

describe('TextAlign', () => {
  it('should cover 3 tools', () => {
    const { container } = render(<MockEditorWrapper shouldSetHTML />);

    const iconLeftElement = container.querySelector('.icon-align-left')!;
    const iconCenterElement = container.querySelector('.icon-align-center')!;
    const iconRightElement = container.querySelector('.icon-align-right')!;

    userEvent.click(iconLeftElement);
    userEvent.click(iconCenterElement);
    userEvent.click(iconRightElement);

    expect(true).toBeTruthy();
  });

  it('should cover block data', () => {
    const { container } = render(<MockEditorWrapper />);

    const editorContainer = container.querySelector(
      '.public-DraftEditor-content'
    )!;
    const iconLeftElement = container.querySelector('.icon-align-left')!;

    const event = createEvent.paste(editorContainer, {
      clipboardData: {
        types: ['text/plain'],
        getData: () => 'abc'
      }
    });

    fireEvent(editorContainer, event);
    userEvent.click(iconLeftElement);
    userEvent.click(iconLeftElement);

    expect(true).toBeTruthy();
  });

  it('should cover 100%', () => {
    const { container } = render(<MockEditorWrapper />);

    const editorContainer = container.querySelector(
      '.public-DraftEditor-content'
    )!;
    const iconLeftElement = container.querySelector('.icon-align-left')!;

    const event = createEvent.paste(editorContainer, {
      clipboardData: {
        types: ['text/plain'],
        getData: () => '<div class="h5">abc</div>'
      }
    });

    fireEvent(editorContainer, event);
    userEvent.click(iconLeftElement);
    userEvent.click(iconLeftElement);

    expect(true).toBeTruthy();
  });
});
