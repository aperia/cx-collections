import React, { useEffect, useRef } from 'react';

// testing library
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';

// mocks
import '../../../test-utils/mocks/mockCanvas';

// components
import Editor, { EditorRef } from '..';

const MockEditorWrapper: React.FC<any> = ({ shouldSetHTML = false }) => {
  const editorRef = useRef<EditorRef>();

  useEffect(() => {
    if (!shouldSetHTML) return;

    editorRef.current?.setHTML('<div class="h5">phat/</div');
  }, [shouldSetHTML]);

  return (
    <Editor
      tools={['BlockTypes', 'Bold', 'InsertLinkNoTarget']}
      ref={editorRef as any}
    />
  );
};

describe('InsertLinkNoTarget', () => {
  it('should render without target', () => {
    const { container } = render(<MockEditorWrapper />);

    expect(container.querySelector('.icon-link')).toBeInTheDocument();
  });
});
