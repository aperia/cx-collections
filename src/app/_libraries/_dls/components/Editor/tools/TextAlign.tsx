import React, { useEffect, useState } from 'react';

// draft
import { EditorState, Modifier } from 'draft-js';

// components
import Button from '../../Button';

// contexts
import { useEditorContext } from '../contexts';

// utils
import classnames from 'classnames/dedupe';
import { getCurrentSelectedBlock } from '../fns';
import Icon, { IconProps } from '../../Icon';

// others
import { Map } from 'immutable';
import { compact, isEmpty } from 'lodash';

export interface TextAlignProps {
  textAlignClassName?: keyof typeof textAlignTitle;
  iconName?: IconProps['name'];
}

// constants
const TEXT_ALIGN_BLOCK_DATA_KY = 'TEXT_ALIGN_BLOCK_DATA_KY';
const textAlignTitle = {
  'text-left': 'Align Left',
  'text-center': 'Align Center',
  'text-right': 'Align Right'
};

const TextAlign: React.FC<TextAlignProps> = ({
  textAlignClassName,
  iconName
}) => {
  const [editorState, setEditorState] = useEditorContext();

  const [isActive, setIsActive] = useState(false);

  // handle toggle text align
  const handleTextAlign: React.MouseEventHandler<HTMLButtonElement> = (
    event
  ) => {
    event.preventDefault();

    // detect and get block current
    const selectedBlock = getCurrentSelectedBlock(editorState);

    const {
      contentState,
      currentBlock,
      // comment out for unit test, can't cover this case
      // hasAtomicBlock,
      selectionState
    } = selectedBlock;

    // comment out for unit test, can't cover this case
    // if (hasAtomicBlock || !currentBlock) return;

    const blockData = currentBlock.getData();

    // blockData.get(TEXT_ALIGN_BLOCK_DATA_KY) will has value when we run TextAlign tool itself
    const textAlign = blockData.get(TEXT_ALIGN_BLOCK_DATA_KY);
    let className = blockData.get('className');
    className = isEmpty(className) ? '' : className;
    const classNameList = Array.isArray(className)
      ? className
      : compact<string>(className.split(' '));
    let nextTextAlign, nextClassName;

    // from block data
    if (textAlign) {
      nextTextAlign =
        textAlign === textAlignClassName ? '' : textAlignClassName;
      nextClassName = classNameList.filter((cn) => {
        return ['text-left', 'text-center', 'text-right'].indexOf(cn) === -1;
      });
    }
    // from HTML (paste)
    else {
      const isExist = classNameList.indexOf(textAlignClassName!) !== -1;
      nextTextAlign = isExist ? '' : textAlignClassName;
      nextClassName = classNameList.filter((cn) => {
        return ['text-left', 'text-center', 'text-right'].indexOf(cn) === -1;
      });
    }

    const nextEditorState = EditorState.push(
      editorState,
      Modifier.mergeBlockData(
        contentState,
        selectionState,
        Map({
          [TEXT_ALIGN_BLOCK_DATA_KY]: nextTextAlign,
          className: nextClassName
        })
      ),
      'change-block-data'
    );

    setEditorState(nextEditorState);
  };

  // if the caret changed, we need toggle alignment state
  useEffect(() => {
    const selectionState = editorState.getSelection();
    const startKey = selectionState.getStartKey();
    const contentBlock = editorState
      .getCurrentContent()
      .getBlockForKey(startKey);
    const blockData = contentBlock.getData();

    // blockData.get(TEXT_ALIGN_BLOCK_DATA_KY) will work if we run TextAlign tool itself
    //
    // blockData.get(className) will work if convertFromHTMLConfig is run
    // convertFromHTMLConfig run: after pasted/setHTML
    const currentClassName = (blockData.get(TEXT_ALIGN_BLOCK_DATA_KY) ||
      blockData.get('className') ||
      '') as string;

    const nextIsActive = currentClassName.includes(textAlignClassName!);

    setIsActive(nextIsActive);
  }, [editorState, textAlignClassName]);

  return (
    <Button
      onMouseDown={handleTextAlign}
      className={classnames('dls-editor-tool', {
        active: isActive
      })}
      variant="icon-secondary"
      title={textAlignTitle[textAlignClassName!]}
    >
      <Icon name={iconName!} />
    </Button>
  );
};

const TextLeft: React.FC<TextAlignProps> = ({
  textAlignClassName = 'text-left',
  iconName = 'align-left',
  ...props
}) => {
  return (
    <TextAlign
      textAlignClassName={textAlignClassName}
      iconName={iconName}
      {...props}
    />
  );
};

const TextCenter: React.FC<TextAlignProps> = ({
  textAlignClassName = 'text-center',
  iconName = 'align-center',
  ...props
}) => {
  return (
    <TextAlign
      textAlignClassName={textAlignClassName}
      iconName={iconName}
      {...props}
    />
  );
};

const TextRight: React.FC<TextAlignProps> = ({
  textAlignClassName = 'text-right',
  iconName = 'align-right',
  ...props
}) => {
  return (
    <TextAlign
      textAlignClassName={textAlignClassName}
      iconName={iconName}
      {...props}
    />
  );
};

export { TEXT_ALIGN_BLOCK_DATA_KY, TextLeft, TextCenter, TextRight };
export default TextAlign;
