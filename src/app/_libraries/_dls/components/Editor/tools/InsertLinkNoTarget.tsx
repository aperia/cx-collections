import React from 'react';

import InsertLink, { InsertLinkProps } from './InsertLink';

export interface InsertLinkNoTargetProps extends InsertLinkProps {}

const InsertLinkNoTarget: React.FC<InsertLinkNoTargetProps> = () => {
  return <InsertLink target={false} />;
};

export default InsertLinkNoTarget;
