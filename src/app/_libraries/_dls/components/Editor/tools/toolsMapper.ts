import { DraftDecorator } from 'draft-js';
import {
  Bold,
  InsertLink,
  Italic,
  OrderedList,
  TextLeft,
  TextCenter,
  TextRight,
  UnderLine,
  UnOrderedList,
  BlockTypes,
  InOutdent,
  Mention
} from '.';
import { Link, MentionDecorator } from '../decorators';
import { ToolEnum } from '../types';
import InsertLinkNoTarget from './InsertLinkNoTarget';

export interface Tool {
  Component?: React.FunctionComponent;
  decorator?: DraftDecorator;
  isInvisible?: boolean;
}

const genStrategy: (type: string) => DraftDecorator['strategy'] =
  (type) => (contentBlock, callback, contentState) => {
    contentBlock.findEntityRanges((characterMetaData) => {
      const entityKey = characterMetaData.getEntity();
      let entityType;
      if (entityKey) {
        const entityInstance = contentState.getEntity(entityKey);
        entityType = entityInstance.getType();
      }
      return Boolean(entityType) && entityType === type;
    }, callback);
  };

const toolsMapper: Record<keyof typeof ToolEnum, Tool | undefined> = {
  Bold: {
    Component: Bold
  },
  Italic: {
    Component: Italic
  },
  UnderLine: {
    Component: UnderLine
  },
  SubScript: undefined,
  SuperScript: undefined,
  TextLeft: {
    Component: TextLeft
  },
  TextCenter: {
    Component: TextCenter
  },
  TextRight: {
    Component: TextRight
  },
  UnOrderedList: {
    Component: UnOrderedList
  },
  OrderedList: {
    Component: OrderedList
  },
  InOutdent: {
    Component: InOutdent
  },
  InsertLink: {
    Component: InsertLink,
    decorator: {
      component: Link,
      strategy: genStrategy('LINK')
    }
  },
  InsertLinkNoTarget: {
    Component: InsertLinkNoTarget,
    decorator: {
      component: Link,
      strategy: genStrategy('LINK')
    }
  },
  InsertImage: undefined,
  BlockTypes: {
    Component: BlockTypes
  },
  Mention: {
    Component: Mention as any,
    isInvisible: true,
    decorator: {
      component: MentionDecorator,
      strategy: genStrategy('MENTION')
    }
  },
  FontName: undefined,
  FontSize: undefined,
  TextColor: undefined,
  TextBackgroundColor: undefined,
  CleanFormatting: undefined,
  ViewHtml: undefined,
  Print: undefined
};

export default toolsMapper;
