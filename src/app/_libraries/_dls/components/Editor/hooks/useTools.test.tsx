import React from 'react';

// mock libs
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import useTools from './useTools';
import { isUndefined } from 'lodash';

const tools = [
  ['Bold', 'Italic', 'UnderLine'],
  ['TextLeft', 'TextCenter', 'TextRight'],
  ['UnOrderedList', 'OrderedList', 'InOutdent'],
  ['InsertLink'],
  ['BlockTypes'],
  'Mention'
];

const WrapperHookUseTools: React.FC<any> = (props) => {
  useTools(
    !isUndefined(props.tools) ? props.tools : ['Bold', 'Italic', 'UnderLine']
  );

  return 'WrapperHookUseTools' as any;
};

describe('useTools', () => {
  it('should return if tools is null', () => {
    render(<WrapperHookUseTools tools={null} />);
    expect(true).toBeTruthy();
  });

  it('have multiple-dimension tool arrays', () => {
    render(<WrapperHookUseTools tools={tools} />);
    expect(true).toBeTruthy();
  });

  it('should cover 100%', () => {
    render(<WrapperHookUseTools />);
    expect(true).toBeTruthy();
  });
});
