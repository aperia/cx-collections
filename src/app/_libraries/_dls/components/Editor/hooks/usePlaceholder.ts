import { EditorState } from 'draft-js';
import { IN_OUT_DENT_BLOCK_DATA_KY } from '../tools';

export interface PlaceholderHook {
  (editorState: EditorState): [boolean];
}

const usePlaceholder: PlaceholderHook = (editorState) => {
  const contentState = editorState.getCurrentContent();
  const hasText = contentState.hasText();
  let isShow = false;

  if (!hasText) {
    const contentBlock = contentState.getBlockMap().first();
    const contentBlockData = contentBlock.getData();

    const isIndent =
      contentBlockData.get(IN_OUT_DENT_BLOCK_DATA_KY) === 'dls-indent' ||
      contentBlockData.get('className')?.includes('dls-indent');
    const isStyled = contentBlock.getType() === 'unstyled';

    isShow = isStyled && !isIndent;
  }

  return [isShow];
};

export default usePlaceholder;
