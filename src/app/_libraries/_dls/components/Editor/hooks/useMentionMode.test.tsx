import React from 'react';

// mock libs
import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import useMentionMode from './useMentionMode';

const Wrapper = (props: any) => {
  const { tools } = props;
  const result = useMentionMode(tools);

  return <div>{result ? 'true' : 'false'}</div>;
};

describe('useMentionMode', () => {
  it('should render false', () => {
    const { getByText } = render(<Wrapper />);
    expect(getByText('false')).toBeInTheDocument();
  });

  it('should render true', () => {
    const { getByText } = render(<Wrapper tools={['Mention']} />);
    expect(getByText('true')).toBeInTheDocument();
  });
});
