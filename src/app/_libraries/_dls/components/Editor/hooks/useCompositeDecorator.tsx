import { useMemo } from 'react';

import { DraftDecorator } from 'draft-js';
import { pick } from '../../../lodash';
import { ToolEnum, ToolKeys } from '../types';
import { toolsMapper } from '../tools';

export interface CompositeDecoratorHook {
  (tools?: ToolKeys): DraftDecorator[];
}

const useCompositeDecorator: CompositeDecoratorHook = (tools) => {
  const decorators = useMemo(() => {
    if (!Array.isArray(tools)) return [];

    const results = (tools as (keyof typeof ToolEnum)[]).reduce(
      (prev, curr) => {
        const { decorator } = pick(toolsMapper[curr], 'Component', 'decorator');

        if (decorator?.strategy && decorator?.component) {
          prev.push(decorator);
        }

        return prev;
      },
      [] as DraftDecorator[]
    );

    return results;
  }, [tools]);

  return decorators;
};

export default useCompositeDecorator;
