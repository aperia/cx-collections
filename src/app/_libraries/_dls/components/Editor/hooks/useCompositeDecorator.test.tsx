import React from 'react';

// mock libs
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import useCompositeDecorator from './useCompositeDecorator';
import { isUndefined } from 'lodash';

const WrapperHookDepositDecorator: React.FC<any> = (props) => {
  useCompositeDecorator(
    !isUndefined(props.originTools)
      ? props.originTools
      : ['BlockTypes', 'Bold', 'CleanFormatting', 'InsertLink']
  );

  return 'WrapperHookDepositDecorator' as any;
};

describe('useCompositeDecorator', () => {
  it('should return if originTools is null', () => {
    render(<WrapperHookDepositDecorator originTools={null} />);
    expect(true).toBeTruthy();
  });

  it('should cover 100%', () => {
    render(<WrapperHookDepositDecorator />);
    expect(true).toBeTruthy();
  });
});
