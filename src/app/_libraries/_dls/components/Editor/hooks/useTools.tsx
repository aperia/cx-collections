import React, { useMemo } from 'react';

import { toolsMapper } from '../tools';
import { useDeepValue } from '../../../hooks';
import { ToolEnum, ToolKeys } from '../types';
import { isEmpty } from 'lodash';

export type ToolsHookReturnType = [
  React.ReactElement | React.ReactElement[],
  keyof typeof ToolEnum
];

export interface ToolsHook {
  (nestedTools?: ToolKeys): ToolsHookReturnType;
}

// handle generate tool elements
const useTools: ToolsHook = (nestedTools) => {
  const originTools = useDeepValue(nestedTools);

  const allTools = useMemo(() => {
    if (!Array.isArray(originTools)) return [];

    const invisibleTools: (keyof typeof ToolEnum)[] = [];

    const gen = (prev: any[], curr: keyof typeof ToolEnum, index: number) => {
      const toolKy = curr + index;
      const Component = toolsMapper[curr]?.Component;
      const isInvisible = toolsMapper[curr]?.isInvisible;

      if (isInvisible) invisibleTools.push(curr);

      if (Component && !isInvisible) {
        prev.push(<Component key={toolKy} />);
      }

      return prev;
    };

    const results = originTools.reduce(
      (prev: any[], tools: any, index: any) => {
        let toolsInGroup, toolAlone;

        if (Array.isArray(tools)) {
          toolsInGroup = tools.reduce(gen, []);
        } else {
          toolAlone = [tools].reduce(gen, []);
        }

        if (toolsInGroup) {
          prev.push(
            <div key={index} className="dls-editor-tools-group">
              {toolsInGroup}
            </div>
          );
        }

        if (!isEmpty(toolAlone)) {
          prev.push(toolAlone);
        }

        return prev;
      },
      []
    );

    return [results, invisibleTools];
  }, [originTools]);

  return allTools as ToolsHookReturnType;
};

export default useTools;
