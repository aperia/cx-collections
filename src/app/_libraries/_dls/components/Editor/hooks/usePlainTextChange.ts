import { useEffect, useRef } from 'react';
import { EditorState } from 'draft-js';

export interface PlainTextChangeHook {
  (
    editorState: EditorState,
    isMention: boolean,
    onPlainTextChange?: (plainText: string) => void
  ): void;
}

const usePlainTextChange: PlainTextChangeHook = (
  editorState,
  isMention,
  onPlainTextChange
) => {
  const coreRef = useRef<Record<string, any>>({});
  coreRef.current.onPlainTextChange = onPlainTextChange;

  useEffect(() => {
    if (!isMention) return;

    const { onPlainTextChange } = coreRef.current;
    const plainText = editorState.getCurrentContent().getPlainText();
    onPlainTextChange?.(plainText);
  }, [editorState, isMention]);
};

export default usePlainTextChange;
