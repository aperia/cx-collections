import React, { useEffect, useRef } from 'react';

// mock libs
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { isUndefined } from 'lodash';
import useEditorState from './useEditorState';
import { useDeepValue } from '../../../hooks';

const component = () => 'component';
const strategy = (block: any, callback: any, contentState: any) => null;

const WrapperHookEditorState: React.FC<any> = (props) => {
  const nextDecorators = useDeepValue(
    !isUndefined(props.decorators)
      ? props.decorators
      : [
          {
            component,
            props: {},
            strategy
          }
        ]
  );

  const { renewEditorState } = useEditorState(nextDecorators as any);

  const coreRef = useRef<Record<string, any>>({});
  coreRef.current.renewEditorState = renewEditorState;

  useEffect(() => coreRef.current.renewEditorState(), []);

  useEffect(() => coreRef.current.renewEditorState('has html'), []);

  return 'WrapperHookEditorState' as any;
};

describe('useEditorState', () => {
  it('should cover 76%', () => {
    const { rerender } = render(<WrapperHookEditorState />);

    rerender(
      <WrapperHookEditorState
        decorators={[
          {
            component,
            props: { a: 'a' },
            strategy
          }
        ]}
      />
    );

    expect(true).toBeTruthy();
  });
});
