import React from 'react';

import { IConvertToHTMLConfig } from 'draft-convert';
import { LinkData } from '../decorators';

// utils
import { get } from '../../../lodash';
import classnames from 'classnames/dedupe';

// ky
import {
  BLOCK_TYPE_BLOCK_DATA_KY,
  IN_OUT_DENT_BLOCK_DATA_KY,
  TEXT_ALIGN_BLOCK_DATA_KY
} from '../tools';

const convertToHTMLConfig: IConvertToHTMLConfig = {
  entityToHTML: (entity, originalText) => {
    // Using:
    //  - InsertLink
    if (entity.type === 'LINK') {
      const entityData = entity.data as LinkData;
      return (
        <a
          href={entityData?.webAddress}
          title={entityData?.title}
          target={entityData.isOpenNewWindow ? '_blank' : undefined}
          rel="noreferrer"
          className="link"
        >
          {originalText}
        </a>
      ) as React.ReactElement;
    }

    if (entity.type === 'MENTION') {
      return (
        <span className="dls-editor-mention-decorator">{originalText}</span>
      );
    }

    return originalText;
  },
  blockToHTML: (blockContent) => {
    // Using:
    //  - TextAlign
    //  - Indent
    //  - BlockTypes

    let Component: any;

    // collect block type class name
    const blockTypeClassName = get(blockContent.data, BLOCK_TYPE_BLOCK_DATA_KY);

    // collect text align class name
    const textAlignClassName = get(blockContent.data, TEXT_ALIGN_BLOCK_DATA_KY);

    // collect in out dent class name
    const inOutDentClassName = get(
      blockContent.data,
      IN_OUT_DENT_BLOCK_DATA_KY
    );

    // get by className will work when we copy/paste
    const className = get(blockContent.data, 'className');

    const nextClassName = classnames(
      blockTypeClassName,
      textAlignClassName,
      inOutDentClassName,
      className
    );

    // handle BlockTypes
    const blockType = blockContent.type;

    switch (blockType) {
      case 'ordered-list-item':
        return {
          start: `<li class="${nextClassName}">`,
          end: '</li>',
          nest: <ol />
        };
      case 'unordered-list-item':
        return {
          start: `<li class="${nextClassName}">`,
          end: '</li>',
          nest: <ul />
        };
      case 'unstyled':
      default:
        Component = 'div';
        break;
    }

    return (<Component className={nextClassName} />) as React.ReactElement;
  }
};

export default convertToHTMLConfig;
