import { IConvertFromHTMLConfig } from 'draft-convert';
import { LinkData } from '../decorators';

const getValidClassName = (className: string) => {
  return className
    .split(' ')
    .filter((segment) => !segment.startsWith('public-'))
    .join(' ');
};

const convertFromHTMLConfig: IConvertFromHTMLConfig = {
  htmlToEntity: (nodeName, node, createEntity) => {
    // Using:
    //   - InsertLink
    if (nodeName === 'a') {
      const linkEntity = createEntity('LINK', 'MUTABLE', {
        webAddress: node.href,
        title: node.title,
        isOpenNewWindow: node.target === '_blank'
      } as LinkData);

      return linkEntity;
    }

    if (
      nodeName === 'span' &&
      node.classList.contains('dls-editor-mention-decorator')
    ) {
      const mentionEntity = createEntity('MENTION', 'SEGMENTED', {});

      return mentionEntity;
    }
  },
  htmlToBlock: (nodeName, node) => {
    // Using:
    //  - BlockTypes
    //  - TextAlignment

    switch (nodeName) {
      case 'li': {
        const blockType =
          node.parentNode?.nodeName === 'OL'
            ? 'ordered-list-item'
            : 'unordered-list-item';

        return {
          type: blockType,
          data: {
            className: getValidClassName(node.className)
          }
        };
      }
      case 'div':
        return {
          type: 'unstyled',
          data: {
            className: getValidClassName(node.className)
          }
        };
    }
  }
};

export default convertFromHTMLConfig;
