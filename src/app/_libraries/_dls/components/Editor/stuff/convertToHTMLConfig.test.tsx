import React from 'react';

// mock libs
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import convertToHTMLConfig from './convertToHTMLConfig';
import {
  BLOCK_TYPE_BLOCK_DATA_KY,
  IN_OUT_DENT_BLOCK_DATA_KY,
  TEXT_ALIGN_BLOCK_DATA_KY
} from '../tools';
import classnames from 'classnames';

describe('convertToHTMLConfig', () => {
  describe('entityToHTML', () => {
    it('should return link element if entity is LINK and isOpenNewWindow is true', () => {
      const element = (convertToHTMLConfig as any).entityToHTML(
        {
          type: 'LINK',
          data: {
            webAddress: 'webAddress',
            title: 'title',
            isOpenNewWindow: true
          }
        },
        'LINK originalText'
      );

      const { getByText } = render(element);
      const originalTextElement = getByText('LINK originalText');

      expect(originalTextElement).toBeInTheDocument();
    });

    it('should return link element if entity is LINK and isOpenNewWindow is false', () => {
      const element = (convertToHTMLConfig as any).entityToHTML(
        {
          type: 'LINK',
          data: {
            webAddress: 'webAddress',
            title: 'title',
            isOpenNewWindow: false
          }
        },
        'LINK originalText'
      );

      const { getByText } = render(element);
      const originalTextElement = getByText('LINK originalText');

      expect(originalTextElement).toBeInTheDocument();
    });

    it('should return mention element if entity is MENTION', () => {
      const element = (convertToHTMLConfig as any).entityToHTML(
        {
          type: 'MENTION'
        },
        'MENTION originalText'
      );

      const { getByText } = render(element);
      const originalTextElement = getByText('MENTION originalText');

      expect(originalTextElement).toBeInTheDocument();
    });

    it('should return originalText if entity not found', () => {
      const element = (convertToHTMLConfig as any).entityToHTML(
        {},
        'Not found originalText'
      );

      expect(element).toEqual('Not found originalText');
    });
  });

  describe('blockToHTML', () => {
    it('should return ordered-list-item', () => {
      const block = (convertToHTMLConfig as any).blockToHTML({
        type: 'ordered-list-item',
        data: {
          [BLOCK_TYPE_BLOCK_DATA_KY]: 'BLOCK_TYPE_BLOCK_DATA_KY',
          [TEXT_ALIGN_BLOCK_DATA_KY]: 'TEXT_ALIGN_BLOCK_DATA_KY',
          [IN_OUT_DENT_BLOCK_DATA_KY]: 'IN_OUT_DENT_BLOCK_DATA_KY',
          className: 'className'
        }
      });

      const nextClassName = classnames(
        'BLOCK_TYPE_BLOCK_DATA_KY',
        'TEXT_ALIGN_BLOCK_DATA_KY',
        'IN_OUT_DENT_BLOCK_DATA_KY',
        'className'
      );

      expect(block).toEqual({
        start: `<li class="${nextClassName}">`,
        end: '</li>',
        nest: <ol />
      });
    });

    it('should return unordered-list-item', () => {
      const block = (convertToHTMLConfig as any).blockToHTML({
        type: 'unordered-list-item',
        data: {
          [BLOCK_TYPE_BLOCK_DATA_KY]: 'BLOCK_TYPE_BLOCK_DATA_KY',
          [TEXT_ALIGN_BLOCK_DATA_KY]: 'TEXT_ALIGN_BLOCK_DATA_KY',
          [IN_OUT_DENT_BLOCK_DATA_KY]: 'IN_OUT_DENT_BLOCK_DATA_KY',
          className: 'className'
        }
      });

      const nextClassName = classnames(
        'BLOCK_TYPE_BLOCK_DATA_KY',
        'TEXT_ALIGN_BLOCK_DATA_KY',
        'IN_OUT_DENT_BLOCK_DATA_KY',
        'className'
      );

      expect(block).toEqual({
        start: `<li class="${nextClassName}">`,
        end: '</li>',
        nest: <ul />
      });
    });

    it('should return unstyled', () => {
      const element = (convertToHTMLConfig as any).blockToHTML({
        type: 'unstyled',
        data: {
          className: 'unstyled-className'
        }
      });

      const { container } = render(element);
      const unstyleElement = container.querySelector('.unstyled-className');

      expect(unstyleElement).toBeInTheDocument();
    });
  });
});
