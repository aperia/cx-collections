// mock libs
import '@testing-library/jest-dom';
import convertFromHTMLConfig from './convertFromHTMLConfig';

describe('convertFromHTMLConfig', () => {
  describe('htmlToEntity', () => {
    it('should return linkEntity if nodeName is a', () => {
      const linkEntity = (convertFromHTMLConfig as any).htmlToEntity(
        'a',
        {
          href: 'something',
          target: undefined
        },
        () => 'linkEntity node a'
      );

      expect(linkEntity).toEqual('linkEntity node a');
    });

    it('should return mentionEntity if nodeName is span', () => {
      const linkEntity = (convertFromHTMLConfig as any).htmlToEntity(
        'span',
        {
          classList: {
            contains: (className: string) =>
              className === 'dls-editor-mention-decorator'
          }
        },
        () => 'mention node span'
      );

      expect(linkEntity).toEqual('mention node span');
    });
  });

  describe('htmlToBlock', () => {
    it('should return blockLi if nodeName is li and parentNode is ol', () => {
      const blockOl = (convertFromHTMLConfig as any).htmlToBlock('li', {
        parentNode: {
          nodeName: 'OL'
        },
        className: 'test ol'
      });

      expect(blockOl).toEqual({
        type: 'ordered-list-item',
        data: {
          className: 'test ol'
        }
      });
    });

    it('should return blockLi if nodeName is li and parentNode is ul', () => {
      const blockUl = (convertFromHTMLConfig as any).htmlToBlock('li', {
        parentNode: {
          nodeName: 'UL'
        },
        className: 'test ul'
      });

      expect(blockUl).toEqual({
        type: 'unordered-list-item',
        data: {
          className: 'test ul'
        }
      });
    });

    it('should return blockDiv if nodeName is div', () => {
      const blockDiv = (convertFromHTMLConfig as any).htmlToBlock('div', {
        className: 'test div'
      });

      expect(blockDiv).toEqual({
        type: 'unstyled',
        data: {
          className: 'test div'
        }
      });
    });
  });
});
