import { EditorState } from 'draft-js';
import React, { useContext } from 'react';

export type EditorContext = [
  editorState: EditorState,
  setEditorState: React.Dispatch<React.SetStateAction<Draft.EditorState>>,
  maxPlainText?: number
];

const EditorContext = React.createContext<EditorContext | []>([]);
const useEditorContext = () => {
  const contextValue = useContext(EditorContext);
  return contextValue as EditorContext;
};

export { useEditorContext };
export default EditorContext;
