import React from 'react';

// mock libs
import { render, queryByText } from '@testing-library/react';
import '@testing-library/jest-dom';

// types
import { useEditorContext } from './EditorContext';

const MockComponent: React.FC = () => {
  useEditorContext();

  return 'Mock EditorContext' as any;
};

describe('EditorContext', () => {
  it('should return context value', () => {
    const { baseElement } = render(<MockComponent />);

    expect(
      queryByText(baseElement as HTMLElement, 'Mock EditorContext')
    ).toBeTruthy();
  });
});
