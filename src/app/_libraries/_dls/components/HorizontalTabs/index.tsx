import React, {
  forwardRef,
  ForwardRefExoticComponent,
  ForwardRefRenderFunction,
  RefAttributes,
  SyntheticEvent,
  useCallback,
  useEffect,
  useImperativeHandle,
  useMemo,
  useState
} from 'react';

// components/types
import Icon from '../Icon';
import { Tab, Tabs, TabsProps as ReactBSTabsProps } from 'react-bootstrap';

// utils
import classNames from 'classnames';
import { className, genAmtId } from '../../utils';
import debounce from 'lodash.debounce';
import isEmpty from 'lodash.isempty';
import isFunction from 'lodash.isfunction';
import isString from 'lodash.isstring';
import toInteger from 'lodash.tointeger';

// hooks
import useHammerPan from './useHammerPan';

export interface HorizontalTabsProps extends ReactBSTabsProps, DLSId {
  className?: string;
  level2?: boolean;
  children?: React.ReactNode;
}

export interface ExtraStaticProp
  extends ForwardRefExoticComponent<
    HorizontalTabsProps & RefAttributes<HTMLDivElement>
  > {
  Tab: typeof Tab;
}

const HorizontalTabs: ForwardRefRenderFunction<
  HTMLDivElement,
  HorizontalTabsProps
> = (
  {
    children: childrenProp,
    className: classNameProp,
    defaultActiveKey,
    activeKey: activeKeyProp,
    onSelect,
    level2,

    id,
    dataTestId,
    ...props
  },
  ref
) => {
  const containerRef = useHammerPan();
  useImperativeHandle(ref, () => containerRef.current!);

  const [arrowWidth, setArrowWidth] = useState(0);
  const [paddingSize, setPaddingSize] = useState(0);

  const [showScrollRight, setShowScrollRight] = useState(false);
  const [showScrollLeft, setShowScrollLeft] = useState(false);
  const [activeKey, setActiveKey] = useState(defaultActiveKey);
  const [isHavingIcon, setIsHavingIcon] = useState(false);

  const containerId = genAmtId(
    dataTestId!,
    'dls-horizontal-tabs',
    'HorizontalTabs'
  );
  const navTestId = genAmtId(containerId!, 'nav', 'HorizontalTabs');

  const calculateShowScroll = useCallback(() => {
    const tabContainer = containerRef?.current?.children[0] as Element;

    const { scrollWidth, clientWidth, scrollLeft } = tabContainer;

    const isHavingIcon = !isEmpty(tabContainer.getElementsByClassName('icon'));
    setIsHavingIcon(isHavingIcon);

    setShowScrollRight(scrollLeft + clientWidth + arrowWidth < scrollWidth);

    setShowScrollLeft(scrollLeft > paddingSize);
  }, [containerRef, paddingSize, arrowWidth]);

  /**
   * Calculate the padding size of the nav
   */
  useEffect(() => {
    const beforeElement = window.getComputedStyle(
      containerRef?.current?.firstChild as Element,
      ':before'
    );
    const width = beforeElement.width;
    const widthNumber = toInteger(width.replace('px', ''));

    setPaddingSize(widthNumber);
  }, [containerRef]);

  // Handle show scroll button
  useEffect(() => {
    const tabContainer = containerRef?.current?.children[0] as Element;

    const calculateShowScrollDebounce = debounce(calculateShowScroll, 200, {
      trailing: true
    });

    calculateShowScroll();
    window.addEventListener('resize', calculateShowScrollDebounce);
    tabContainer.addEventListener('scroll', calculateShowScrollDebounce);

    return () => {
      tabContainer.removeEventListener('scroll', calculateShowScrollDebounce);
      window.removeEventListener('resize', calculateShowScrollDebounce);
    };
  }, [calculateShowScroll, containerRef]);

  const handleScrollRight = () => {
    const tabContainer = containerRef?.current?.children[0] as Element;
    const tabContainerRect = tabContainer.getBoundingClientRect();
    const { scrollWidth, clientWidth, scrollLeft } = tabContainer;

    // calculate the amount to scroll
    let amountToScroll = 0;
    for (let index = 0; index < tabContainer.childElementCount; index++) {
      const child = tabContainer.children[index];
      const childRect = child.getBoundingClientRect();

      // the cutoff width of a tab on the right edge
      const cutOffWidth =
        childRect.left +
        childRect.width -
        (tabContainerRect.left + tabContainerRect.width);

      // cutoff width > 0 which means that tab is hidden
      if (cutOffWidth >= 0) {
        const nextChild = tabContainer.children[index + 1];
        const nextChildRect = nextChild?.getBoundingClientRect();

        amountToScroll = cutOffWidth + (nextChildRect?.width || 0);

        // scroll an additional space if the left arrow is going to be shown
        !showScrollLeft && (amountToScroll += arrowWidth);

        break;
      }
    }

    const shouldShowScrollRight =
      scrollLeft + amountToScroll + clientWidth + arrowWidth < scrollWidth;
    setShowScrollRight(shouldShowScrollRight);
    setShowScrollLeft(true);

    // recalculate the amount to scroll if it scrolled to the end
    !shouldShowScrollRight &&
      (amountToScroll = amountToScroll - arrowWidth + paddingSize);

    tabContainer.scroll({ left: scrollLeft + amountToScroll });
  };

  const handleScrollLeft = () => {
    const tabContainer = containerRef?.current?.children[0] as Element;
    const tabContainerRect = tabContainer.getBoundingClientRect();
    const { scrollLeft } = tabContainer;

    let amountToScroll = 0;
    for (let index = tabContainer.childElementCount - 1; index >= 0; index--) {
      const child = tabContainer.children[index];
      const childRect = child.getBoundingClientRect();

      // the cutoff width of a tab on the right edge
      const cutoffWidth = tabContainerRect.left - childRect.left;

      // if cutoff width approximately > 0 which means that tab is hidden
      if (cutoffWidth >= -1) {
        const nextChild = tabContainer.children[index - 1];
        const nextChildRect = nextChild?.getBoundingClientRect();

        amountToScroll = cutoffWidth + (nextChildRect?.width || 0);

        // scroll an additional padding space if scrolled to the head
        index <= 1 && (amountToScroll += paddingSize);

        break;
      }
    }

    setShowScrollRight(true);
    setShowScrollLeft(scrollLeft - amountToScroll - arrowWidth > 0);

    tabContainer.scroll({ left: scrollLeft - amountToScroll });
  };

  const handleScrollToActiveElement = (activeElement: HTMLAnchorElement) => {
    const tabContainer = containerRef?.current?.children[0] as Element;
    const tabContainerRect = tabContainer?.getBoundingClientRect();
    const { scrollLeft, clientWidth, scrollWidth } = tabContainer;

    const activeElementRect = activeElement.getBoundingClientRect();

    // the cutoff width of a active element on the right edge
    const cutoffWidth =
      activeElementRect.left +
      activeElementRect.width -
      (tabContainerRect.left + tabContainerRect.width);

    // if cutoff width > 0 means the active element is hidden on the right side then
    // scroll to the right
    if (cutoffWidth > 0) {
      tabContainer?.scroll({
        left: scrollLeft + cutoffWidth + (!showScrollLeft ? arrowWidth : 0)
      });
      setShowScrollRight(
        scrollLeft + cutoffWidth + clientWidth + arrowWidth + paddingSize <
          scrollWidth
      );
      setShowScrollLeft(true);
    }

    // if the active element is hidden on the left side -> scroll to the left
    if (activeElementRect.left < tabContainerRect.left) {
      tabContainer.scroll({
        left:
          scrollLeft -
          (tabContainerRect.left - activeElementRect.left) -
          paddingSize
      });

      setShowScrollLeft(
        scrollLeft -
          (tabContainerRect.left - activeElementRect.left) -
          paddingSize >
          0
      );
      setShowScrollRight(true);
    }
  };

  const handleSelect = (
    activeKey: string | null,
    e: SyntheticEvent<unknown>
  ) => {
    isFunction(onSelect) && onSelect(activeKey, e);

    // uncontrolled mode
    !isString(activeKeyProp) &&
      setActiveKey(activeKey as HorizontalTabsProps['activeKey']);

    handleScrollToActiveElement(e.target as HTMLAnchorElement);
  };

  useEffect(() => {
    const tabsNavItems = containerRef?.current?.querySelectorAll(
      `[data-testid="${navTestId}"] > a.nav-item`
    );

    tabsNavItems?.forEach((navItem) => {
      const id = navItem.getAttribute('id');
      id && navItem.setAttribute('data-testid', id);
    });
  }, [navTestId, containerRef]);

  const children = useMemo(() => {
    return React.Children.map(childrenProp as React.ReactElement[], (child) => {
      const childProps = child.props as any;

      return React.cloneElement(child, {
        ...childProps,
        id: genAmtId(navTestId!, `item_${childProps.eventKey}`, '')
      });
    });
  }, [navTestId, childrenProp]);

  return (
    <div
      ref={containerRef}
      className={classNames(className.horizontalTabs.CONTAINER, {
        'level-2': level2
      })}
      id={id}
      data-testid={containerId}
    >
      <Tabs
        className={classNames(
          className.horizontalTabs.NAV,
          { 'show-scroll-right': showScrollRight },
          { 'show-scroll-left': showScrollLeft },
          classNameProp
        )}
        activeKey={isString(activeKeyProp) ? activeKeyProp : activeKey}
        onSelect={handleSelect}
        mountOnEnter
        unmountOnExit
        data-testid={navTestId}
        {...props}
      >
        {children}
      </Tabs>
      {showScrollLeft && (
        <span
          className={classNames('scroll', {
            'scroll__with-icon': isHavingIcon,
            'scroll__level-2': level2
          })}
          onClick={handleScrollLeft}
          ref={(ref) => setArrowWidth(ref?.clientWidth || 0)}
          data-testid={genAmtId(containerId, 'scroll-left', 'HorizontalTabs')}
        >
          <Icon name="chevron-left" size="5x" />
        </span>
      )}
      {showScrollRight && (
        <span
          className={classNames('scroll scroll__right', {
            'scroll__with-icon': isHavingIcon,
            'scroll__level-2': level2
          })}
          onClick={handleScrollRight}
          ref={(ref) => setArrowWidth(ref?.clientWidth || 0)}
          data-testid={genAmtId(containerId, 'scroll-right', 'HorizontalTabs')}
        >
          <Icon name="chevron-right" size="5x" />
        </span>
      )}
    </div>
  );
};

const ExtraStaticTabs = forwardRef<HTMLDivElement, HorizontalTabsProps>(
  HorizontalTabs
) as ExtraStaticProp;
ExtraStaticTabs.Tab = Tab;

export default ExtraStaticTabs;
