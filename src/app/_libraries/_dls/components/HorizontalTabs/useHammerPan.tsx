import { useEffect, useRef } from 'react';

import Hammer from '@egjs/hammerjs';

const useHammerPan = () => {
  const hammerRef = useRef<HTMLDivElement | null>(null);

  useEffect(() => {
    if (!hammerRef.current) return;

    const hammerElement = hammerRef.current.children[0];

    const hammer = new (Hammer as any)(
      hammerRef.current.children[0] as HTMLDivElement
    );

    const onPan = (ev: HammerInput) => {
      hammerElement.classList.add('panning');

      // const direction = ev.direction === Hammer.DIRECTION_LEFT ? 1 : -1;
      let direction;
      switch (ev.direction) {
        case Hammer.DIRECTION_LEFT:
          direction = 1;
          break;
        case Hammer.DIRECTION_RIGHT:
          direction = -1;
          break;
        default:
          direction = 0;
      }
      hammerElement.scroll({ left: hammerElement.scrollLeft + 12 * direction });
    };

    const onPanEnd = () => {
      hammerElement.classList.remove('panning');
    };

    hammer.on('pan', onPan);
    hammer.on('panend pancancel', onPanEnd);

    return () => {
      hammer.off('pan', onPan);
      hammer.off('panend pancancel', onPanEnd);
    };
  }, []);

  return hammerRef;
};

export default useHammerPan;
