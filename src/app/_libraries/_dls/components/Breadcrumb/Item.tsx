import React, {
  forwardRef,
  ForwardRefRenderFunction,
  useRef,
  useImperativeHandle
} from 'react';

// utils
import classNames from 'classnames';

import { className } from '../../utils';

export interface ItemProps extends DLSId {
  label?: string;
  href?: string;
  value?: any;
  active?: boolean;
  onClick?: (data: any) => void;
}

export interface BreadcrumbRef {}

const Item: ForwardRefRenderFunction<BreadcrumbRef, ItemProps> = (
  { href, active, onClick, label, value, id, dataTestId },
  ref
) => {
  const breadcrumbItemRef = useRef(null);
  useImperativeHandle(ref, () => breadcrumbItemRef.current!);

  const handleClick = (
    event: React.MouseEvent<HTMLAnchorElement, MouseEvent>
  ) => {
    event.preventDefault();

    onClick?.(value);
  };

  return (
    <li
      id={id}
      data-testid={dataTestId}
      ref={breadcrumbItemRef}
      className={classNames(className.breadcrumb.ITEM, {
        [className.breadcrumb.ITEM_ACTIVE]: active
      })}
      {...(active && {
        'arial-current': 'page'
      })}
    >
      {active ? (
        <span className={className.breadcrumb.ITEM_TEXT}>{label}</span>
      ) : (
        <a
          href={href}
          rel="noreferrer"
          onClick={handleClick}
          className={className.breadcrumb.ITEM_TEXT}
        >
          {label}
        </a>
      )}
    </li>
  );
};

const ItemForwardedRef = forwardRef<BreadcrumbRef, ItemProps>(Item);
export default React.memo(ItemForwardedRef);
