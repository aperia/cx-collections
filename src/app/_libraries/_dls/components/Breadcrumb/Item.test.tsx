import React from 'react';

// testing library
import '@testing-library/jest-dom';
import { render, Matcher, screen } from '@testing-library/react';

// utils
import { queryByClass } from '../../test-utils/queryHelpers';

// components
import Item, { ItemProps } from './Item';

let baseElement: HTMLElement;
const idTesting = 'dls-breadcrumb-item-testing';

const renderComponent = (props: ItemProps) => {
  const wrapper = render(<Item ref={{ current: null }} {...props} />);
  baseElement = wrapper.baseElement as HTMLElement;

  return wrapper;
};

const expectByClass = (className: Matcher) => {
  expect(queryByClass(baseElement, className)).toBeInTheDocument();
};

describe('render', () => {
  it('should render class active when prop active is true', () => {
    renderComponent({ active: true });

    expectByClass(/active/);
  });

  it('should render as SPAN tag with active item', () => {
    renderComponent({ dataTestId: idTesting, active: true });
    const item = screen.getByTestId(idTesting);
    expect(item.children.item(0)?.tagName).toEqual('SPAN');
  });

  it('should render as A tag with inactive item', () => {
    renderComponent({ dataTestId: idTesting });
    const item = screen.getByTestId(idTesting);
    expect(item.children.item(0)?.tagName).toEqual('A');
  });
});
