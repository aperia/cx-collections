import React, { useMemo, useRef } from 'react';

// testing library
import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

// components
import Breadcrumb, { BreadcrumbProps } from '.';

// utils
import * as getElementAttribute from '../../utils/getElementAttribute';
import { mockClientWidth } from '../../test-utils/mocks/defineProperty';
import { BreadcrumbRef } from './Item';

// Setup jest mock
jest.mock('resize-observer-polyfill', () =>
  jest.requireActual('../../test-utils/mocks/MockResizeObserver')
);

jest.mock('../../utils/canvasTextWidth.ts');

let spyGetElementAttribute: jest.SpyInstance;

beforeEach(() => {
  spyGetElementAttribute = jest
    .spyOn(getElementAttribute, 'getAvailableWidth')
    .mockImplementation(() => 1205);
});

afterEach(() => {
  spyGetElementAttribute?.mockReset();
  spyGetElementAttribute?.mockRestore();
});

const mockCrumbs = [
  {
    label: 'Level 1',
    name: 'lv1',
    path: '/home'
  },
  {
    label: 'Level 2',
    name: 'lv2',
    path: '/product'
  },
  {
    label: 'Level 3',
    name: 'lv3',
    path: '/laptop'
  },
  {
    label: 'Level 4',
    name: 'lv4',
    path: '/hp'
  },
  {
    label: 'Level 5',
    name: 'lv5',
    path: '/14-inch'
  }
];

interface BreadcrumbUnitTestProps extends Omit<BreadcrumbProps, 'children'> {
  noChildren?: boolean;
  crumbList?: typeof mockCrumbs;
}

const RenderComponent = (props: BreadcrumbUnitTestProps) => {
  const breadcrummbRef = useRef<BreadcrumbRef | null>(null);
  const { noChildren, activeCrumb, crumbList = mockCrumbs, ...rest } = props;

  const dropdownListItem = useMemo(() => {
    return crumbList.map(item => (
      <Breadcrumb.Item
        key={item.name}
        label={item.label}
        value={item}
        {...(activeCrumb && {
          active: activeCrumb === item.name
        })}
      />
    ));
  }, [activeCrumb, crumbList]);

  return (
    <Breadcrumb ref={breadcrummbRef} activeCrumb={activeCrumb} {...rest}>
      {noChildren ? (
        <Breadcrumb.Item
          key="keyy"
          label="label"
          value="item"
          {...(activeCrumb && {
            active: activeCrumb === 'keyy'
          })}
        />
      ) : (
        dropdownListItem
      )}
    </Breadcrumb>
  );
};

const idTesting = 'dls-breadcrumb-testing';

describe('render', () => {
  // Default render
  it('should render all level in first time', () => {
    render(<RenderComponent dataTestId={idTesting} />);

    const nav = screen.getByTestId(idTesting);

    expect(nav.firstChild?.childNodes.length).toEqual(mockCrumbs.length);
  });

  it('render with no children', () => {
    render(<RenderComponent dataTestId={idTesting} noChildren />);

    const nav = screen.getByTestId(idTesting);

    expect(nav.firstChild?.childNodes.length).toEqual(1);
  });

  // State active
  it('should render class active at last level (uncontrolled)', () => {
    render(<RenderComponent dataTestId={idTesting} />);

    const nav = screen.getByTestId(idTesting);

    expect(nav.firstChild?.lastChild).toHaveClass('active');
  });

  it('should render correct class active level base on prop activeCrumb value (controlled)', () => {
    const nameCrumbTesting = 'laptop';

    render(
      <RenderComponent activeCrumb={nameCrumbTesting} dataTestId={idTesting} />
    );

    const nav = screen.getByTestId(idTesting);

    // Get current object crumb
    const objCrumb = mockCrumbs.find(crumb => crumb.name === nameCrumbTesting);

    // Get active crumb
    const activeCrumb = nav.getElementsByClassName('active');

    expect(activeCrumb.item(0)?.textContent).toEqual(objCrumb?.label);
  });

  // Tablet view
  it('should show three dot button in tablet view when total level > 3', () => {
    spyGetElementAttribute = jest
      .spyOn(getElementAttribute, 'getAvailableWidth')
      .mockImplementation(() => 790);
    mockClientWidth(790);

    const { container } = render(<RenderComponent dataTestId={idTesting} />);

    const button = container.querySelector('button');

    expect(button).toBeInTheDocument();
  });

  it('should show 2 level outside three dot if total level > 3', () => {
    spyGetElementAttribute = jest
      .spyOn(getElementAttribute, 'getAvailableWidth')
      .mockImplementation(() => 790);
    mockClientWidth(790);

    const { container } = render(<RenderComponent dataTestId={idTesting} />);

    const listCrumbs = container.querySelector('ol');

    const isFirstChildIsButton =
      listCrumbs?.children.item(0)?.children.item(0)?.tagName === 'BUTTON';
    expect(isFirstChildIsButton).toBe(true);

    expect(listCrumbs?.children.length).toEqual(3); // first children is dropdown button for level 1 | 2 | 3 and two children rest is level need show
  });

  it('should show all level in tablet view when total level <= 3', () => {
    const crumbList = mockCrumbs.slice(0, 3);

    spyGetElementAttribute = jest
      .spyOn(getElementAttribute, 'getAvailableWidth')
      .mockImplementation(() => 790);

    mockClientWidth(790);

    const { container } = render(
      <RenderComponent crumbList={crumbList} dataTestId={idTesting} />
    );

    const button = container.querySelector('button');

    expect(button).toBeNull();
  });

  // Mobile view
  it('should show three dot button in mobile view when total level > 2', () => {
    spyGetElementAttribute = jest
      .spyOn(getElementAttribute, 'getAvailableWidth')
      .mockImplementation(() => 390);

    mockClientWidth(390);

    const { container } = render(<RenderComponent dataTestId={idTesting} />);

    const button = container.querySelector('button');

    expect(button).toBeInTheDocument();
  });

  it('should show 1 level outside three dot if total level > 2', () => {
    spyGetElementAttribute = jest
      .spyOn(getElementAttribute, 'getAvailableWidth')
      .mockImplementation(() => 390);
    mockClientWidth(390);

    const { container } = render(<RenderComponent dataTestId={idTesting} />);

    const listCrumbs = container.querySelector('ol');

    const isFirstChildIsButton =
      listCrumbs?.children.item(0)?.children.item(0)?.tagName === 'BUTTON';
    expect(isFirstChildIsButton).toBe(true);

    expect(listCrumbs?.children.length).toEqual(2); // first children is dropdown button for level 1 | 2 |3 and rest children is level need show
  });

  it('should show all level in mobile view when total level <= 2', () => {
    const crumbList = mockCrumbs.slice(0, 2);

    spyGetElementAttribute = jest
      .spyOn(getElementAttribute, 'getAvailableWidth')
      .mockImplementation(() => 390);

    mockClientWidth(390);

    const { container } = render(
      <RenderComponent crumbList={crumbList} dataTestId={idTesting} />
    );

    const button = container.querySelector('button');

    expect(button).toBeNull();
  });
});

describe('actions', () => {
  it('should active level be clicked', () => {
    const { container, queryByText } = render(
      <RenderComponent dataTestId={idTesting} />
    );

    // Query and trigger event click for level 3
    const level3Element = queryByText('Level 3');

    userEvent.click(level3Element!);

    const currentActiveLevel = container.querySelector('.active');

    expect(currentActiveLevel?.textContent).toEqual(level3Element?.textContent);
  });

  it('should hide children level of level be clicked', () => {
    const { container, queryByText } = render(
      <RenderComponent dataTestId={idTesting} />
    );

    // Convert list crumbs to array label
    const arrayLabel = mockCrumbs.map(crumb => crumb.label);

    const indexLabelTest = 1; // Level 3

    // Query and trigger event click for specified level
    const levelElement = queryByText(arrayLabel[indexLabelTest]);
    userEvent.click(levelElement!);

    // New list crumb after click on level element
    const newCrumbs = container.querySelectorAll('.dls-breadcrumb-item');

    // Convert new list crumbs to array label
    const newArrayLabel: any = [];
    newCrumbs.forEach(level => newArrayLabel.push(level.textContent));

    // Expect new crumb
    const result = arrayLabel.slice(0, indexLabelTest + 1);

    expect(newArrayLabel).toEqual(result);
  });

  it('should not change crumb item when click with active crumb (disabled click)', () => {
    const { container, queryByText } = render(
      <RenderComponent activeCrumb={'Level 3'} dataTestId={idTesting} />
    );

    // Convert list crumbs to array label
    const arrayLabel = mockCrumbs.map(crumb => crumb.label);

    // Query and trigger event click for active level
    const activeCrumb = queryByText('Level 5');
    userEvent.click(activeCrumb!);

    // New list crumb after click on level element
    const newCrumbs = container.querySelectorAll('.dls-breadcrumb-item');

    // Convert new list crumbs to array label
    const newArrayLabel: any = [];
    newCrumbs.forEach(level => newArrayLabel.push(level.textContent));

    expect(newArrayLabel).toEqual(arrayLabel);
  });

  it('should show popup list level when click on three dot button', () => {
    spyGetElementAttribute = jest
      .spyOn(getElementAttribute, 'getAvailableWidth')
      .mockImplementation(() => 790);
    mockClientWidth(790);

    const { container, baseElement, queryByText } = render(
      <RenderComponent dataTestId={idTesting} />
    );

    const threeDotBtn = container.querySelector('.dls-dropdown-button');
    userEvent.click(threeDotBtn!);
    const popupElement = baseElement.querySelector('.dls-popup');
    expect(popupElement).toBeInTheDocument();

    // Query and trigger event click for level 3
    const level3Element = queryByText('Level 3');
    userEvent.click(level3Element!);

    expect(popupElement).not.toBeInTheDocument();
  });
});
