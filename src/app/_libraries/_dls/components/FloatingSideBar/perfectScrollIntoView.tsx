const perfectScrollIntoView = (
  element: HTMLElement,
  prevBtnElement: HTMLDivElement,
  nextBtnElement: HTMLDivElement,
  containerElement: HTMLDivElement
) => {
  if (!element || !containerElement) {
    return;
  }

  element.scrollIntoView({
    behavior: 'smooth',
    block: 'nearest'
  });

  const {
    y: yElementSelected,
    height: heightElementSelected
  } = element.getBoundingClientRect();
  const { y: yPrevBtn, height: heightPrevBtn } =
    (prevBtnElement && prevBtnElement.getBoundingClientRect()) || {};
  const { y: yNextBtn } =
    (nextBtnElement && nextBtnElement.getBoundingClientRect()) || {};

  // perfect view
  if (
    prevBtnElement &&
    nextBtnElement &&
    yElementSelected >= yPrevBtn + heightPrevBtn &&
    yElementSelected + heightElementSelected <= yNextBtn &&
    element.nextSibling
  ) {
    return;
  }

  // view replaced by prev button
  if (prevBtnElement && yElementSelected < yPrevBtn + heightPrevBtn) {
    let scrollByY = yElementSelected - (yPrevBtn + heightPrevBtn);

    scrollByY = !!element.previousSibling ? scrollByY : scrollByY - 4;
    containerElement.scrollBy(0, scrollByY);
  }

  // view replaced by next button
  if (
    nextBtnElement &&
    yElementSelected + heightElementSelected + 1 > yNextBtn
  ) {
    let scrollByY = yElementSelected + heightElementSelected - yNextBtn;

    scrollByY = !!element.nextSibling ? scrollByY : scrollByY + 4;
    containerElement.scrollBy(0, scrollByY);
  }
};

export default perfectScrollIntoView;
