import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';

import FloatingSideBar from '.';
import { queryAllByClass, queryByClass } from '../../test-utils/queryHelpers';
import { FloatingSideBarItem } from './FloatingSideBarTabItem';
import { FloatingSideBarProps } from './FloatingSideBar';

import * as perfectScrollIntoView from './perfectScrollIntoView';

const items: FloatingSideBarItem[] = [
  {
    key: 100000520,
    text: '100000520',
    actived: false
  },
  {
    key: 'a1',
    text: '1',
    parent: 100000520,
    actived: false
  },
  {
    key: 'a2',
    text: '1402',
    parent: 100000520,
    actived: false
  },
  {
    key: 'a3',
    text: '103'
  },
  {
    key: 'a4',
    text: '101404',
    parent: 'a3'
  },
  {
    key: 'a41',
    text: '100014041',
    parent: 'a3'
  },
  {
    key: 'a5',
    text: '10001405',
    parent: 'a3'
  },
  {
    key: 'a6',
    text: '100001406',
    parent: 'a3'
  },
  {
    key: 'a7',
    text: '11407',
    parent: 'a3'
  },
  {
    key: 'a8',
    text: '10408'
  },
  {
    key: 'a9',
    text: '109',
    parent: 'a8'
  },
  {
    key: 'a10',
    text: '1000014010',
    parent: 'a8',
    actived: true
  },
  {
    key: 'a11',
    text: '1000014011',
    parent: 'a8'
  }
];

let wrapper: RenderResult;

interface RenderComponentProps extends Omit<FloatingSideBarProps, 'children'> {}

const renderComponent = (props: RenderComponentProps) => {
  wrapper = render(
    <FloatingSideBar
      content={<h3>FloatingSideBar Content</h3>}
      items={items}
      {...props}
    />
  );

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLDivElement,
    rerender: (props: any) => {
      wrapper.rerender(
        (
          <FloatingSideBar
            content={<h3>FloatingSideBar Content</h3>}
            {...props}
          />
        ) as any
      );
    }
  };
};

const getNode = {
  get container() {
    return queryAllByClass(
      wrapper.baseElement as HTMLDivElement,
      /floating-sidebar/
    )[0];
  },
  get leftSideBar() {
    return queryByClass(
      wrapper.baseElement as HTMLDivElement,
      /floating-sidebar-lef/
    )!;
  },
  get nextButton() {
    return queryByClass(wrapper.baseElement as HTMLDivElement, /next-button/)!;
  },
  get previousButton() {
    return queryByClass(
      wrapper.baseElement as HTMLDivElement,
      /previous-button/
    )!;
  },
  get hammer() {
    return queryByClass(
      wrapper.baseElement as HTMLDivElement,
      /ds-floating-sidebar-hammer/
    )!;
  },
  get tabItems() {
    return queryAllByClass(
      wrapper.baseElement as HTMLDivElement,
      /^tab-item$|^tab-item actived$/
    );
  }
};

const mockGetBoundingClientRect = ({
  lastElementTop = 0,
  lastElementHeight = 0,
  hammerElementTop = 0,
  hammerElementHeight = 0,
  topItemStep = 30,
  heightItem = 100
}) => {
  let count = -1;

  Element.prototype.getBoundingClientRect = () => {
    count++;

    const defaultReturn = {
      bottom: 0,
      left: 0,
      right: 0,
      width: 0,
      x: 0,
      y: 0,
      toJSON: () => undefined
    };

    // hammerElement
    if (count === 0) {
      return {
        ...defaultReturn,
        height: hammerElementHeight,
        top: hammerElementTop
      };
    }

    // lastElement
    if (count === 1) {
      return {
        ...defaultReturn,
        height: lastElementHeight,
        top: lastElementTop
      };
    }

    return {
      ...defaultReturn,
      height: count === 2 ? 100 : heightItem,
      top: count === 2 ? 100 : topItemStep * (count - 2)
    };
  };
};

describe('Render', () => {
  it('test render', () => {
    renderComponent({});

    mockGetBoundingClientRect({
      hammerElementTop: 100,
      hammerElementHeight: 100
    });

    expect(getNode.container).toBeInTheDocument();
  });

  it('should cover isRefresh.current is false', () => {
    const { rerender } = renderComponent({});

    rerender({
      items: items.slice(0, 2)
    });

    expect(getNode.container).toBeInTheDocument();
  });

  it('test render when zoom', () => {
    renderComponent({ zoom: true });

    expect(getNode.container.classList.contains('zoom')).toBeTruthy();
  });

  it('test render when collapse', () => {
    renderComponent({ collapse: true });

    expect(getNode.container.classList.contains('collapse')).toBeTruthy();
  });
});

describe('Actions', () => {
  describe('handlePreviousOnClick', () => {
    describe('when appear a half item', () => {
      it('has no previousSibling', () => {
        const mockPerfectScrollIntoView = jest.fn();
        jest
          .spyOn(perfectScrollIntoView, 'default')
          .mockImplementation(mockPerfectScrollIntoView);

        mockGetBoundingClientRect({ topItemStep: 120, heightItem: 100 });

        renderComponent({});

        userEvent.click(getNode.previousButton);

        expect(mockPerfectScrollIntoView).toBeCalledWith(
          getNode.tabItems[0],
          getNode.previousButton,
          getNode.nextButton,
          getNode.hammer
        );
      });

      it('has previousSibling', () => {
        const mockPerfectScrollIntoView = jest.fn();
        jest
          .spyOn(perfectScrollIntoView, 'default')
          .mockImplementation(mockPerfectScrollIntoView);

        mockGetBoundingClientRect({ topItemStep: 40, heightItem: 100 });

        renderComponent({});

        userEvent.click(getNode.previousButton);

        expect(mockPerfectScrollIntoView).toBeCalledWith(
          getNode.tabItems[1],
          getNode.previousButton,
          getNode.nextButton,
          getNode.hammer
        );
      });
    });

    it('when appear full item', () => {
      const mockPerfectScrollIntoView = jest.fn();
      jest
        .spyOn(perfectScrollIntoView, 'default')
        .mockImplementation(mockPerfectScrollIntoView);

      mockGetBoundingClientRect({ topItemStep: 50, heightItem: 50 });

      renderComponent({});

      userEvent.click(getNode.previousButton);

      expect(mockPerfectScrollIntoView).toBeCalledWith(
        getNode.tabItems[2],
        getNode.previousButton,
        getNode.nextButton,
        getNode.hammer
      );
    });
  });

  // describe('handleNextOnClick', () => {
  describe('when appear a half item', () => {
    it('has no nextSibling', () => {
      const mockPerfectScrollIntoView = jest.fn();
      jest
        .spyOn(perfectScrollIntoView, 'default')
        .mockImplementation(mockPerfectScrollIntoView);

      mockGetBoundingClientRect({ topItemStep: 60, heightItem: 50 });

      renderComponent({});

      userEvent.click(getNode.nextButton);

      const tabItems = getNode.tabItems;
      expect(mockPerfectScrollIntoView).toBeCalledWith(
        tabItems[tabItems.length - 1],
        getNode.previousButton,
        getNode.nextButton,
        getNode.hammer
      );
    });

    it('has nextSibling', () => {
      const mockPerfectScrollIntoView = jest.fn();
      jest
        .spyOn(perfectScrollIntoView, 'default')
        .mockImplementation(mockPerfectScrollIntoView);

      mockGetBoundingClientRect({ topItemStep: 20, heightItem: 50 });

      renderComponent({});

      userEvent.click(getNode.nextButton);

      const tabItems = getNode.tabItems;

      expect(mockPerfectScrollIntoView).toBeCalledWith(
        tabItems[tabItems.length - 2],
        getNode.previousButton,
        getNode.nextButton,
        getNode.hammer
      );
    });

    it('when appear full item', () => {
      const mockPerfectScrollIntoView = jest.fn();
      jest
        .spyOn(perfectScrollIntoView, 'default')
        .mockImplementation(mockPerfectScrollIntoView);

      mockGetBoundingClientRect({ topItemStep: 50, heightItem: 50 });

      renderComponent({});

      userEvent.click(getNode.nextButton);

      const tabItems = getNode.tabItems;
      expect(mockPerfectScrollIntoView).toBeCalledWith(
        tabItems[tabItems.length - 2],
        getNode.previousButton,
        getNode.nextButton,
        getNode.hammer
      );
    });
  });
});
