import FloatingSideBar from './FloatingSideBar';

export * from './FloatingSideBar';
export * from './FloatingSideBarTabItem';
export default FloatingSideBar;
