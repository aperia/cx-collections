import React from 'react';

import Group from './Group';
import Item from './Item';
import { DropdownBaseItemProps } from './types';

export interface ItemAllProps extends DropdownBaseItemProps {}

const ItemAll: React.FC<ItemAllProps> = ({ dataTestId, ...props }) => {
  return (
    <Group>
      <Item dataTestId={dataTestId} {...props} />
    </Group>
  );
};

export default ItemAll;
