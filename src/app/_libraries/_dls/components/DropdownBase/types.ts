// components/types
import DropdownBase from '.';
import Group from './Group';
import Item from './Item';
import { BadgeProps } from '../Badge';
import { BubbleProps } from '../Bubble';
import { TooltipProps } from '../Tooltip';

// hooks
import { ScrollOnNavigationHook } from '../../hooks';
import { MutableRefObject } from 'react';
import { MultiSelectProps } from '../MultiSelect';
import { GroupProps } from '../MultiSelect/Group';

export type DropdownBaseDirection = 'top' | 'bottom';

export type DropdownBaseValueType = 'single' | 'multiple' | 'new-behavior';

export type DropdownBaseChangeBy = 'up/down' | 'enter' | 'click';

export type DropdownBaseValueSingle = any;

export type DropdownBaseValueMultiple = any[];

export type DropdownBaseHoverValue = any;

export type DropdownBaseItemVariant =
  | 'badge'
  | 'badge-only'
  | 'bubble'
  | 'text'
  | 'checkbox'
  | 'custom';

export interface DropdownBaseCore {
  handleOnChangeModeSingle: (
    nextValueSingle: DropdownBaseValueSingle,
    changeBy?: DropdownBaseChangeBy
  ) => void;
  handleOnChangeModeMultiple: (
    nextValueMultiple: DropdownBaseValueMultiple
  ) => void;
  valueSingle: DropdownBaseValueSingle;
  valueMultiple: DropdownBaseValueMultiple;
  valueType: DropdownBaseValueType;
  items: React.FunctionComponentElement<DropdownBaseItemProps>[];
  hoverValue: any;
  setHoverValue: React.Dispatch<DropdownBaseHoverValue>;
  checkAll?: boolean;
  checkAllLabel?: string;
  filter?: string;
  onFilter?: (nextFilter?: string) => void;
  baseChildren?: any;
  shouldShowCheckAll: () => boolean;

  // ComboBox
  onInfinityScroll?: () => void;
}

export interface DropdownBaseChangeEvent
  extends React.ChangeEvent<typeof DropdownBase> {
  target: EventTarget &
    typeof DropdownBase & { value?: any; id?: string; name?: string };
  value?: any;
  changeBy?: DropdownBaseChangeBy;
}

export interface DropdownBaseRef {
  containerElement?: HTMLElement | null;
  scrollContainerElement?: HTMLElement | null;
  searchBarElement?: HTMLElement | null;
}

export interface DropdownBaseItemRef {
  element?: HTMLElement;
}

export interface DropdownBaseProps extends DLSId {
  id?: string;
  name?: string;

  value?: any;
  onChange?: (event: DropdownBaseChangeEvent) => void;
  allowCallOnChangeByUpDown?: boolean;
  allowCallOnChangeByEnter?: boolean;
  allowCallOnChangeByClick?: boolean;

  // default: single
  // single: DropdownList/ComboBox/DropdownButton
  // multiple: MultiSelect
  valueType?: DropdownBaseValueType;

  // MultiSelect group mode:
  // should show checkAll option or not
  checkAll?: boolean;
  checkAllLabel?: string;
  // should show searchBar or not
  searchBar?: boolean;
  // placeholder of the search bar
  searchBarPlaceholder?: string;
  // max length filter of the search bar
  maxLengthFilter?: number;
  // filter, supports searchBar
  filter?: string;
  // onFilter, supports searchBar
  onFilter?: (nextFilter: string) => void;
  // binding baseChildren to count length and other purposes
  baseChildren?: MultiSelectProps['children'];
  // we need to control focus behavior of the MultiSelect
  onFocus?: GroupProps['onFocus'];
  // we need to control blur behavior of the MultiSelect
  onBlur?: GroupProps['onBlur'];
  // we need to control behavior close/open popup of the MultiSelect
  handleOpenedMode?: GroupProps['handleOpenedMode'];

  className?: string;
  scrollContainerProps?: any;
  margin?: Parameters<ScrollOnNavigationHook>[3];
  marginByItems?: Parameters<ScrollOnNavigationHook>[4];
  defaultDirection?: DropdownBaseDirection;
  // just allow keydown when allowKeydown is true
  allowKeydown?: boolean;
  // allow run highlight text for DropdownBase.Item (label like text filter)
  highlightTextItem?: boolean;
  // allow run truncate tooltip for DropdownBase.Item
  truncateTooltip?: boolean;
  noResult?: React.ReactNode;

  /**
   * - Turnon virtual scroll on dropdown. Should only be used when item height is fixed
   */
  virtualScroll?: boolean;

  // ComboBox
  onInfinityScroll?: () => void;
  loadingInfinityScroll?: boolean;

  children:
    | React.ReactElement<DropdownBaseGroupProps, typeof Group>
    | React.ReactElement<DropdownBaseGroupProps, typeof Group>[]
    | React.ReactElement<DropdownBaseItemProps, typeof Item>
    | React.ReactElement<DropdownBaseItemProps, typeof Item>[];
}

export interface DropdownBaseItemProps extends DLSId {
  variant?: DropdownBaseItemVariant;
  checked?: boolean;
  indeterminate?: boolean;
  disabled?: boolean;
  badgeProps?: BadgeProps;
  bubbleProps?: BubbleProps;
  tooltipProps?: TooltipProps;
  textProps?: {
    children?: React.ReactNode;
    className?: string;
  };
  label?: string;
  className?: string;
  value?: any;
  children?: any;
}

export interface DropdownBaseItemDetailProps extends DropdownBaseItemProps {
  // this prop will received from DropdownBase
  coreRef?: MutableRefObject<DropdownBaseCore>;
  // should run highlight text, this prop will received from DropdownBase
  highlightTextItem?: boolean;
  // should run truncate tooltip, this prop will received from DropdownBase
  truncateTooltip?: boolean;
}

export interface DropdownBaseGroupProps {
  label?: string;
  className?: string;
  children?:
    | React.ReactElement<DropdownBaseItemProps, typeof Item>
    | React.ReactElement<DropdownBaseItemProps, typeof Item>[];
}
