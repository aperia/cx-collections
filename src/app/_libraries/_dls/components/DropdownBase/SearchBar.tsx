import React, {
  InputHTMLAttributes,
  useEffect,
  useImperativeHandle,
  useRef,
  useState
} from 'react';

// components
import Icon from '../Icon';

// utils
import { isFunction, isNumber } from '../../lodash';
import classnames from 'classnames';
import { nanoid } from 'nanoid';
import { DropdownBaseProps } from './types';
import { classes, extendsEvent } from '../../utils';

export interface SearchBarRef {}

export interface SearchBarProps extends InputHTMLAttributes<HTMLInputElement> {
  valueDropdownBase: DropdownBaseProps['value'];
  handleOpenedMode: DropdownBaseProps['handleOpenedMode'];
  maxLengthFilter: DropdownBaseProps['maxLengthFilter'];
}

const SearchBar: React.RefForwardingComponent<SearchBarRef, SearchBarProps> = (
  {
    onChange,
    onFocus,
    onBlur,
    onPaste,
    valueDropdownBase,
    handleOpenedMode,

    maxLengthFilter,
    placeholder,
    id,
    name
  },
  ref
) => {
  const [value, setValue] = useState('');
  const [focused, setFocused] = useState(false);

  const inputRef = useRef<HTMLInputElement | null>(null);

  const handleOnFocus: React.FocusEventHandler<HTMLInputElement> = (event) => {
    setFocused(true);
  };

  const handleOnBlur: React.FocusEventHandler<HTMLInputElement> = (event) => {
    // what if the user clicks on something that is not in the browser?
    // example: desktop, taskbar, developer tools...
    // by default, if the user clicks on those thing, the browser will keeps
    // the previous state
    // So the next time, the user clicks (mousedown) on the screen (valid container DOM browser),
    // onFocus will be called and onMouseDown will be not called
    // we don't want that behavior
    // so this code block to prevent that behavior
    if (inputRef.current && document.activeElement === inputRef.current) {
      inputRef.current.blur();
    }

    setFocused(false);
    isFunction(handleOpenedMode) && handleOpenedMode(false);

    let nextEvent = extendsEvent({} as any, 'target', {
      id,
      name,
      value: valueDropdownBase
    });
    nextEvent = extendsEvent(nextEvent, null, { value: valueDropdownBase });
    isFunction(onBlur) && onBlur(nextEvent);
  };

  const handleOnChange: React.InputHTMLAttributes<HTMLInputElement>['onChange'] =
    (event) => {
      const nextValue = event.target.value;

      if (isNumber(maxLengthFilter) && nextValue.length > maxLengthFilter) {
        return;
      }

      setValue(nextValue);
      onChange?.(event);
    };

  const handleOnPaste: React.DOMAttributes<HTMLInputElement>['onPaste'] = (
    event
  ) => {
    const nextValue = event.clipboardData.getData('text');
    if (isNumber(maxLengthFilter) && nextValue.length > maxLengthFilter) {
      return event.preventDefault();
    }

    onPaste?.(event);
  };

  useImperativeHandle(ref, () => inputRef.current!);

  useEffect(() => {
    process.nextTick(() => {
      inputRef.current?.focus({ preventScroll: true });
      const popup = inputRef.current?.closest(
        `.${classes.popupBase.container}`
      ) as HTMLElement;
      popup?.addEventListener('mousedown', (event) => event.preventDefault());
    });
  }, []);

  return (
    <div
      className={classnames('dls-dropdown-base-search-bar', {
        'dls-focused': focused
      })}
    >
      <input
        ref={inputRef}
        value={value}
        onChange={handleOnChange}
        onFocus={handleOnFocus}
        onBlur={handleOnBlur}
        onPaste={handleOnPaste}
        placeholder={placeholder}
        autoComplete={nanoid()}
      />
      <Icon name="search" size="5x" />
    </div>
  );
};

export default React.forwardRef<SearchBarRef, SearchBarProps>(SearchBar);
