import React, { FC } from 'react';

// types
import { DropdownBaseGroupProps } from './types';

// utils
import { className } from '../../utils';

const Group: FC<DropdownBaseGroupProps> = ({ label, children }) => {
  return (
    <>
      <span className="start-group" />
      <span className={className.dropdownBase.HEADER}>{label}</span>
      {children}
      <span className="end-group" />
    </>
  );
};

Group.displayName = 'DropdownBase.Group';
export default Group;
