import React, { FC, useState } from 'react';

import MaskedTextBox, { MaskedTextBoxProps } from '.';

export interface MaskedTextBoxWrapperProps
  extends Omit<MaskedTextBoxProps, 'children' | 'onChange' | 'mask'> {
  mask: (string | RegExp)[];
  onChange?: (value: string) => void;
}
const MockMaskedTextBoxWrapper: FC<MaskedTextBoxWrapperProps> = ({
  value: valueProp,
  onChange,
  ...props
}) => {
  const [value, setValue] = useState(valueProp);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    onChange && onChange(event.target.value);

    setValue(event.target.value);
  };

  return (
    <MaskedTextBox
      ref={{ current: null } as any}
      value={value}
      onChange={handleChange}
      {...props}
    />
  );
};

export default MockMaskedTextBoxWrapper;
