import { PipeFunction } from 'vanilla-text-mask';
import { HTMLProps } from 'react';
import { TooltipProps } from '../Tooltip';

export interface MaskedTextBoxRef {
  inputElement?: HTMLElement;
}

export interface MaskedTextBoxReferenceRef {
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  valueProp?: string;
}

export interface MaskedTextBoxProps extends HTMLProps<HTMLInputElement>, DLSId {
  mask: (string | RegExp)[];
  guide?: boolean;
  placeholderChar?: string;
  keepCharPositions?: boolean;
  pipe?: PipeFunction;

  label?: string;
  readOnly?: boolean;
  disabled?: boolean;
  required?: boolean;
  error?: {
    status?: boolean;
    message?: string;
  };

  errorTooltipProps?: TooltipProps;
  small?: boolean;
}

export interface MaskFunction {
  (rawValue: string): string;
}

export interface ConformToMaskFunction {
  (text: string, mask: (string | RegExp)[] | MaskFunction, config?: any): {
    conformedValue: string;
  };
}
