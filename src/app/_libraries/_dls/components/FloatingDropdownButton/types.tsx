import {
  DropdownBaseGroupProps,
  DropdownBaseItemProps,
  DropdownBaseItemRef,
  DropdownBaseProps,
  DropdownBaseRef
} from '../DropdownBase';
import { PopupBaseProps, PopupBaseRef } from '../PopupBase';
import { TooltipProps } from '../Tooltip';

export interface FloatingDropdownButtonRef {
  popupBaseRef?: React.MutableRefObject<PopupBaseRef | null>;
  dropdownBaseRef?: React.MutableRefObject<DropdownBaseRef | null>;
  inputElement?: HTMLDivElement | null;
}

export interface FloatingDropdownButtonProps
  extends Omit<
      DropdownBaseProps,
      | 'selected'
      | 'onSelect'
      | 'allowKeydown'
      | 'reference'
      | 'scrollContainerProps'
    >,
    DLSId {
  textField?: string;
  textFieldRender?: (value: any) => React.ReactElement<any>;

  onBlur?: (event: React.FocusEvent) => void;
  onFocus?: (event: React.FocusEvent) => void;

  opened?: boolean;
  onVisibilityChange?: (nextOpened: boolean) => void;

  readOnly?: boolean;
  disabled?: boolean;

  required?: boolean;
  error?: {
    message?: string;
    status?: boolean;
  };
  errorTooltipProps?: TooltipProps;

  popupBaseProps?: Omit<
    PopupBaseProps,
    'opened' | 'onVisibilityChange' | 'reference'
  >;
  autoFocus?: boolean;

  onClickNavigation?: (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => void;
}

export interface DropdownListItemRef extends DropdownBaseItemRef {}

export interface DropdownListItemProps extends DropdownBaseItemProps {}

export interface DropdownListGroupProps extends DropdownBaseGroupProps {}
