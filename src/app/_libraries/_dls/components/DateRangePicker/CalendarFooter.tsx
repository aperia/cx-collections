import React from 'react';

// components
import Button, { ButtonProps } from '../Button';

export interface CalendarFooterProps extends ButtonProps {
  label?: string;
}

const CalendarFooter: React.FC<CalendarFooterProps> = ({
  label = 'Today',
  ...props
}) => {
  return (
    <div className="react-calendar__footer">
      <Button variant="outline-primary" size="sm" {...props}>
        {label}
      </Button>
    </div>
  );
};

export default CalendarFooter;
