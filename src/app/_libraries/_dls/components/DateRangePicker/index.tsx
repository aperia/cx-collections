import React, { useEffect, useMemo, useRef, useState } from 'react';

// components/types
import Icon from '../Icon';
import Label from '../Label';
import MaskedTextBox, { MaskedTextBoxRef } from '../MaskedTextBox';
import PopupBase, { PopupBaseRef } from '../PopupBase';
import Tooltip from '../Tooltip';
import Calendar, {
  CalendarProps,
  Detail,
  FormatterCallback,
  OnChangeDateCallback
} from 'react-calendar';
import {
  DateRangeConfig,
  DateRangePickerKeepReference,
  DateRangePickerMaxDetail,
  DateRangePickerMinDetail,
  DateRangePickerProps,
  DateRangePickerValue,
} from './types';

// utils
import classnames from 'classnames';
import { classes, extendsEvent, genAmtId, truncateTooltip } from '../../utils';
import datetime from 'date-and-time';
import {
  get,
  isArray,
  isBoolean,
  isDate,
  isFunction,
  isNil,
  pick
} from '../../lodash';

// hooks
import { useResizeElementFitText } from '../../hooks';
import { DatePickerProps } from '../DatePicker';
import { useDateTooltipPortals, useTodayPortal } from './hooks';
import { correctDateRange, isValidDate } from './helpers';

const MIN_DATE = new Date('01/01/1900');
const MAX_DATE = new Date('01/01/3000');
const formatShortWeekday: FormatterCallback = (locale, date) => {
  return datetime.format(date, 'dd');
};
const formatMonth: FormatterCallback = (locale, date) => {
  return datetime.format(date, 'MMM');
};
const MONTH_VIEW = {
  placeholder: 'mm/dd/yyyy',
  format: 'MM/DD/YYYY',
  minDetail: 'century' as DateRangePickerMinDetail,
  maxDetail: 'month' as DateRangePickerMaxDetail,
  label: 'Today',
  mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/],
  validLength: 10
};
const YEAR_VIEW = {
  placeholder: 'mm/yyyy',
  format: 'MM/YYYY',
  minDetail: 'century' as DateRangePickerMinDetail,
  maxDetail: 'year' as DateRangePickerMaxDetail,
  label: 'Current Month',
  mask: [/\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/],
  validLength: 7
};

const DateRangePicker: React.FC<DateRangePickerProps> = ({
  value: valueProp,
  onChange,

  label,
  size,

  onBlur,
  onFocus,

  opened: openedProp,
  onVisibilityChange,

  readOnly,
  disabled,

  required,
  error,
  errorTooltipProps,

  popupBaseProps,

  id,
  name,
  autoFocus,
  className: classNameProp,

  minDate = MIN_DATE,
  maxDate = MAX_DATE,
  dateTooltip,
  disabledRange,

  titleDisabled,

  view = 'month',

  dataTestId
}) => {
  // refs
  const containerRef = useRef<HTMLDivElement | null>(null);
  const popupBaseRef = useRef<PopupBaseRef | null>(null);
  const textFieldContainerRef = useRef<HTMLDivElement | null>(null);
  const isMouseDownRef = useRef(false);
  const isTypingsRef = useRef(false);
  const keepRef = useRef<
    DateRangePickerKeepReference<
      typeof handleValueMode,
      typeof value,
      typeof isValidDate,
      typeof correctDateRange
    >
  >({} as any);
  const autoFocusRef = useRef(autoFocus);
  const fakeFocusRef = useRef<HTMLDivElement | null>(null);
  const floatingLabelRef = useRef<HTMLSpanElement | null>(null);

  // states
  const [maskedTextBoxStart, setMaskedTextBoxStart] =
    useState<MaskedTextBoxRef | null>(null);
  const [maskedTextBoxEnd, setMaskedTextBoxEnd] =
    useState<MaskedTextBoxRef | null>(null);
  const [opened, setOpened] = useState(!!openedProp);
  const [value, setValue] = useState(valueProp);
  const [valueMaskedTextBox, setValueMaskedTextBox] = useState({
    start: '',
    end: ''
  });
  const [selectRange, setSelectRange] = useState(false);

  // variables
  const { message, status } = pick(error, 'message', 'status');
  const isOpenedTooltip = Boolean(status && message && opened);
  const { popupBaseClassName, ...popupProps } = popupBaseProps || {};
  const isRequired = required && !readOnly && !disabled;
  const isSmallSize = size === 'small';
  const noLabel = !label || isSmallSize;
  const floating = Boolean(
    (!noLabel && opened && !readOnly && !disabled) || value?.start || value?.end
  );

  // refresh disabledRange to start of day
  const formatDisabledRange = (
    disabledRange: DatePickerProps['disabledRange']
  ) => {
    const nextDisabledRange = disabledRange?.map((range) => {
      const { fromDate, toDate } = range;
      let nextFromDate, nextToDate;

      // clone date
      if (fromDate) {
        nextFromDate = new Date(fromDate.getTime());
      }
      if (toDate) {
        nextToDate = new Date(toDate.getTime());
      }

      // refresh clone date
      nextFromDate?.setHours(0, 0, 0, 0);
      nextToDate?.setHours(0, 0, 0, 0);

      // return clone date
      return { fromDate: nextFromDate, toDate: nextToDate };
    });

    return nextDisabledRange;
  };

  // configs
  const viewConfig = useMemo(
    () => (view === 'month' ? MONTH_VIEW : YEAR_VIEW),
    [view]
  );

  // date config
  const dateConfig: DateRangeConfig = useMemo(() => {
    let iMinDate = minDate;
    let iMaxDate = maxDate;

    // if maxDate < minDate, swap it
    if (iMaxDate < iMinDate) {
      const tempDate = iMaxDate;
      iMaxDate = iMinDate;
      iMinDate = tempDate;
    }

    const minYear = iMinDate.getFullYear();
    const minMonth = iMinDate.getMonth() + 1;
    const minBeginDate = new Date(iMinDate.getTime());
    minBeginDate.setHours(0, 0, 0, 0);

    const maxYear = iMaxDate.getFullYear();
    const maxMonth = iMaxDate.getMonth() + 1;
    const maxBeginDate = new Date(iMaxDate.getTime());
    maxBeginDate.setHours(0, 0, 0, 0);

    return {
      min: {
        year: minYear,
        month: minMonth,
        beginDate: minBeginDate
      },
      max: {
        year: maxYear,
        month: maxMonth,
        beginDate: maxBeginDate
      }
    };
  }, [minDate, maxDate]);

  const checkDisabledRange = (date: Date) => {
    const nextDisabledRange = formatDisabledRange(disabledRange);

    if (Array.isArray(nextDisabledRange)) {
      for (let index = 0; index < nextDisabledRange.length; index++) {
        const { fromDate, toDate } = nextDisabledRange[index];
        const isValidFromDate = isValidDate(fromDate);
        const isValidToDate = isValidDate(toDate);

        if (
          isValidFromDate &&
          date >= fromDate! &&
          isValidToDate &&
          date <= toDate!
        ) {
          return true;
        }
      }
    }

    return false;
  };

  const checkDisabledToday = () => {
    const today = new Date();
    today.setHours(0, 0, 0, 0);
    return (
      today < dateConfig.min.beginDate ||
      today > dateConfig.max.beginDate ||
      checkDisabledRange(today)
    );
  };

  const checkDisabled = (date: Date, view: Detail) => {
    const year = date.getFullYear();
    const month = date.getMonth() + 1;

    switch (view) {
      case 'century':
        const centuryRangeMaxYear = year + 9;
        const centuryCondition =
          centuryRangeMaxYear < dateConfig.min.year ||
          centuryRangeMaxYear > dateConfig.max.year;
        if (centuryCondition) return centuryCondition;
        break;
      case 'decade':
        const decadeCondition =
          year < dateConfig.min.year || year > dateConfig.max.year;
        if (decadeCondition) return decadeCondition;
        break;
      case 'year':
        const yearCondition =
          year < dateConfig.min.year ||
          year > dateConfig.max.year ||
          (year <= dateConfig.min.year && month < dateConfig.min.month) ||
          (year >= dateConfig.max.year && month > dateConfig.max.month);
        if (yearCondition) return yearCondition;
        break;
      case 'month':
        const monthCondition =
          date < dateConfig.min.beginDate || date > dateConfig.max.beginDate;
        if (monthCondition) return monthCondition;
        break;
    }

    // handle disabled range
    const isDisabledRange = checkDisabledRange(date);
    if (isDisabledRange) return isDisabledRange;

    return false;
  };

  // handle titleDisabled props Calendar component
  const handleTitleDisabled: typeof titleDisabled = ({
    date,
    view,
    ...restProps
  }) => {
    if (isFunction(titleDisabled)) {
      return titleDisabled({ date, view, ...restProps });
    }

    return checkDisabled(date, view);
  };

  const handleTileContent: CalendarProps['tileContent'] = (
    calendarTileProperties
  ) => {
    return (
      <div className="dls-tooltip-cache-date">
        {calendarTileProperties.date?.toString()}
      </div>
    );
  };

  const resetRef = () => {
    isMouseDownRef.current = false;
    isTypingsRef.current = false;
    autoFocusRef.current = false;
  };

  // handle focus on textFieldContainerRef.current (the container element)
  const handleFocus = (event: React.FocusEvent<HTMLInputElement>) => {
    if (disabled) return;

    const target = event.target as HTMLElement;
    const relatedTarget = event.relatedTarget as HTMLElement;

    if (!isMouseDownRef.current && !autoFocusRef.current) {
      return;
    }

    // if DateRangePicker is not floating
    // we always focus to MaskedTextBoxStart input element
    const { inputStartElement, inputEndElement } = keepRef.current;
    if (!floating) inputStartElement?.focus();
    if (floating && target && target === textFieldContainerRef.current) {
      const isInputEndFocus =
        (value?.start && value?.end) || (value?.start && !value?.end);

      if (isInputEndFocus) inputEndElement?.focus();
      else inputStartElement?.focus();
    }

    // if MaskedTextBoxStart or MaskedTextBoxEnd is focusing,
    // and focused element is MaskedTextBoxStart or MaskedTextBoxEnd
    // we should be ignore this focus
    if (
      textFieldContainerRef.current?.contains(target) &&
      textFieldContainerRef.current?.contains(relatedTarget)
    ) {
      return;
    }

    // otherwise, we should call onFocus prop
    let nextEvent = extendsEvent({} as any, 'target', { name, id, value });
    nextEvent = extendsEvent(nextEvent, null, { value });
    isFunction(onFocus) && onFocus(nextEvent);

    // and reset anything
    resetRef();

    // and open the popup
    handleOpenedMode(true);
  };

  // handle blur on textFieldContainerRef.current (the container element)
  const handleBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    const target = event.target as HTMLElement;
    const relatedTarget = event.relatedTarget as HTMLElement;

    // we are still focusing inside, maybe start input, maybe end input
    if (
      textFieldContainerRef.current?.contains(target) &&
      textFieldContainerRef.current?.contains(relatedTarget)
    ) {
      return;
    }

    // what if the user clicks on something that is not in the browser?
    // example: desktop, taskbar, developer tools...
    // by default, if the user clicks on those thing, the browser will keeps
    // the previous state
    // So the next time, the user clicks (mousedown) on the screen (valid container DOM browser),
    // onFocus will be called and onMouseDown will be not called
    // we don't want that behavior
    // so this code block below to prevent that behavior
    const { inputStartElement, inputEndElement } = keepRef.current;

    // if textFieldContainer element is focusing, trigger blur it
    if (
      document.activeElement &&
      document.activeElement === textFieldContainerRef.current
    ) {
      textFieldContainerRef.current?.blur();
    }

    // if MaskedTextBoxStart input element is focusing, trigger blur it
    if (
      document.activeElement &&
      document.activeElement === inputStartElement
    ) {
      inputStartElement?.blur();
    }

    // if MaskedTextBoxEnd input element is focusing, trigger blur it
    if (document.activeElement && document.activeElement === inputEndElement) {
      inputEndElement?.blur();
    }

    // otherwise, we should call onBlur prop if exists
    let nextEvent = extendsEvent({} as any, 'target', { name, id, value });
    nextEvent = extendsEvent(nextEvent, null, { value });
    isFunction(onBlur) && onBlur(nextEvent);

    // and reset anything
    resetRef();

    // and close the popup
    handleOpenedMode(false);
  };

  const handleOnBlurMaskedTextBox = (
    event: React.FocusEvent<HTMLInputElement>
  ) => {
    const { name } = pick(event.target, ['name']);

    // we are in the onBlur stage
    // the valueMaskedTextBox maybe valid or maybe invalid
    // if the current value is valid and not equal valueMaskedTextBox
    // we will reset the valueMaskedTextBox back to current value
    const date = get(value, [name]) as Date;
    const isValidDate = isDate(date) && !isNaN(date.valueOf());

    if (isValidDate) {
      const dateFormatted = datetime.format(date, viewConfig.format);
      const dateMaskedTextBox = get(valueMaskedTextBox, [name]) as string;

      if (dateFormatted !== dateMaskedTextBox) {
        setValueMaskedTextBox((state) => ({
          ...state,
          [name]: dateFormatted
        }));
      }
    }
    // otherwise, we will reset the valueMaskedTextBox to empty string
    else setValueMaskedTextBox((state) => ({ ...state, [name]: '' }));
  };

  // handle value uncontrolled/controlled
  const handleValueMode = (nextValue?: DateRangePickerValue) => {
    isFunction(onChange) &&
      onChange({
        target: {
          value: nextValue,
          id,
          name
        },
        value: nextValue
      } as any);

    // if isMouseDownRef.current is true and we have start date and en date,
    // we know that we need to call onBlur on nextTick
    // because we need to make sure onChange has done (new value updated) before we call onBlur
    const { start, end } = pick(nextValue, ['start', 'end']);
    const isValidStartDate = isDate(start) && !isNaN(start.valueOf());
    const isValidEndDate = isDate(end) && !isNaN(end.valueOf());
    const { inputStartElement, inputEndElement } = keepRef.current;

    if (isMouseDownRef.current && isValidStartDate && isValidEndDate) {
      process.nextTick(() => {
        // if we have activeElement is textFieldContainerRef.current, trigger blur it
        if (document?.activeElement === textFieldContainerRef.current) {
          textFieldContainerRef.current?.blur();
        }

        // if we have activeElement is MaskedTextBoxStart input element, trigger blur it
        // blur will bubble to textFieldContainerRef.current
        if (document?.activeElement === inputStartElement) {
          inputStartElement?.blur();
        }

        if (document?.activeElement === inputEndElement) {
          inputEndElement?.blur();
        }
      });
    }

    // controlled
    if (!isNil(valueProp)) return;

    // uncontrolled
    setValue(nextValue);
  };

  // handle mousedown on MaskedTextBox input element
  const handleOnMouseDown = () => {
    isMouseDownRef.current = true;
  };

  // handle opened uncontrolled/controlled
  const handleOpenedMode = (nextOpened: boolean) => {
    isFunction(onVisibilityChange) && onVisibilityChange(nextOpened);

    // controlled
    if (isBoolean(openedProp)) return;

    // uncontrolled
    setOpened(nextOpened);
  };

  useEffect(() => {
    const { start, end } = pick(value, ['start', 'end']);

    if ((start && !end) || (!start && end)) {
      return setSelectRange(true);
    }

    return setSelectRange(false);
  }, [value]);

  const handleOnChangeCalendar: OnChangeDateCallback = (valueFromCalendar) => {
    const { start, end } = pick(value, ['start', 'end']);

    const isArrayValue = isArray(valueFromCalendar);

    // if we have start and end in two inputs, just update start to valueFromCalendar, end to undefined
    if (start && end) {
      let nextValue: typeof value;

      if (isArrayValue) {
        valueFromCalendar = valueFromCalendar as Date[];
        nextValue = { start: valueFromCalendar[0], end: valueFromCalendar[1] };
      } else {
        valueFromCalendar = valueFromCalendar as Date;
        nextValue = { start: valueFromCalendar, end: undefined };
      }

      handleValueMode(nextValue);
      return;
    }

    // if we have start, don't have end, just keep start, and update end to valueFromCalendar

    if (start && !end) {
      let nextValue;

      /* istanbul ignore else */
      if (isArrayValue) {
        valueFromCalendar = valueFromCalendar as Date[];
        nextValue = { start: valueFromCalendar[0], end: valueFromCalendar[1] };
      } else {
        valueFromCalendar = valueFromCalendar as Date;
        nextValue = { start, end: valueFromCalendar };
      }

      handleValueMode(nextValue);
      return;
    }

    // if we have end, don't have start, just keep end, and update start to valueFromCalendar
    if (!start && end) {
      let nextValue;

      /* istanbul ignore else */
      if (isArrayValue) {
        valueFromCalendar = valueFromCalendar as Date[];
        nextValue = { start: valueFromCalendar[0], end: valueFromCalendar[1] };
      } else {
        valueFromCalendar = valueFromCalendar as Date;
        nextValue = { start: valueFromCalendar, end };
      }

      handleValueMode(nextValue);
      return;
    }

    let nextValue;

    if (isArrayValue) {
      valueFromCalendar = valueFromCalendar as Date[];
      nextValue = { start: valueFromCalendar[0], end: valueFromCalendar[1] };
    } else {
      valueFromCalendar = valueFromCalendar as Date;
      nextValue = { start: valueFromCalendar, end: undefined };
    }

    handleValueMode(nextValue);
    return;
  };

  // handle onChange MaskedTextBoxStart/MaskedTextBoxEnd
  const handleOnChangeMaskedTextBox = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    if (readOnly || disabled) return;

    // if user is typings, we need to reset isMouseDownRef.current to false
    // just to make sure logic is running correct
    // setting isMouseDownRef.current to true maybe redundant
    isMouseDownRef.current = false;

    // signal that the user is typings
    isTypingsRef.current = true;

    const { name, value } = pick(event.target, ['name', 'value']);

    setValueMaskedTextBox((state) => ({
      ...state,
      [name]: value
    }));
  };

  // handle mousedown on icon
  const handleOnMouseDownIcon = (
    event: React.MouseEvent<HTMLSpanElement, MouseEvent>
  ) => {
    if (disabled) return;

    const { inputStartElement, inputEndElement } = keepRef.current;

    // stop default behavior browser (blur/focus, abc, xyz...)
    event.preventDefault();

    // if we have activeElement is textFieldContainerRef.current, trigger blur it
    if (document?.activeElement === textFieldContainerRef.current) {
      return textFieldContainerRef.current?.blur();
    }

    // if we have activeElement is MaskedTextBoxStart input element, trigger blur it
    // blur will bubble to textFieldContainerRef.current
    if (document?.activeElement === inputStartElement) {
      return inputStartElement?.blur();
    }

    // if we have activeElement is MaskedTextBoxEnd input element, trigger blur it
    // blur will bubble to textFieldContainerRef.current
    if (document?.activeElement === inputEndElement) {
      return inputEndElement?.blur();
    }

    handleOnMouseDown();

    const preventScroll = { preventScroll: true };

    if (!floating) return inputStartElement?.focus(preventScroll);

    if (value?.start && value?.end) {
      inputEndElement?.focus(preventScroll);
    } else if (value?.start && !value?.end) {
      inputEndElement?.focus(preventScroll);
    } else if (!value?.start && value?.end) {
      inputStartElement?.focus(preventScroll);
    } else inputStartElement?.focus(preventScroll);
  };

  // handle click button on footer
  const handleClickButtonFooter = () => {
    const dateNow = new Date();
    let valueFromCalendar;

    const { value } = keepRef.current;
    let nextValue: DateRangePickerValue;

    if ((value?.start && value?.end) || (!value?.start && !value?.end)) {
      nextValue = correctDateRange(dateConfig, {
        start: dateNow,
        end: undefined
      });
      valueFromCalendar = nextValue.start;
    }

    if (value?.start && !value?.end) {
      nextValue = correctDateRange(dateConfig, {
        start: value?.start,
        end: dateNow
      });
      valueFromCalendar = [nextValue.start, nextValue.end];
    }

    if (!value?.start && value?.end) {
      nextValue = correctDateRange(dateConfig, {
        start: dateNow,
        end: value?.end
      });
      valueFromCalendar = [nextValue.start, nextValue.end];
    }

    handleOnChangeCalendar(valueFromCalendar as Date | Date[]);
  };

  const handleOnPopupShown = () => {
    const popupElement = popupBaseRef.current?.element;

    const handleMouseDown = (event: MouseEvent) => {
      // stop browser events
      event.preventDefault();

      // need set isMouseDownRef.current to true
      // because handleValueMode will be called if user choose start date or end date or both
      // handleValueMode need to know if date is chosen by using mousedown
      // to consider trigger blur on next tick (after value has been handled)

      isMouseDownRef.current = true;
    };

    // no need to remove event later, because popup element will be remove from DOM
    popupElement?.addEventListener('mousedown', handleMouseDown);
  };

  const handleOnPopupClosed = () => {
    if (document?.activeElement === document?.body) {
      fakeFocusRef.current?.focus({ preventScroll: true });
    }
  };

  // keep reference
  keepRef.current.handleValueMode = handleValueMode;
  keepRef.current.value = value;
  keepRef.current.isValidDate = isValidDate;
  keepRef.current.correctDateRange = correctDateRange;
  keepRef.current.inputStartElement = maskedTextBoxStart?.inputElement;
  keepRef.current.inputEndElement = maskedTextBoxEnd?.inputElement;
  keepRef.current.handleClickButtonFooter = handleClickButtonFooter;
  keepRef.current.dateConfig = dateConfig;

  // set props
  useEffect(() => setValue(valueProp), [valueProp]);
  useEffect(() => setOpened(!!openedProp), [openedProp]);

  // base on value, if value has a valid date, format it and set to MaskedTextBox value
  useEffect(() => {
    const { start, end } = pick(value, ['start', 'end']);
    let startValueMaskedTextBox = '';
    let endValueMaskedTextBox = '';

    if (isDate(start) && !isNaN(start.valueOf())) {
      startValueMaskedTextBox = datetime.format(start, viewConfig.format);
    }

    if (isDate(end) && !isNaN(end.valueOf())) {
      endValueMaskedTextBox = datetime.format(end, viewConfig.format);
    }

    setValueMaskedTextBox({
      start: startValueMaskedTextBox,
      end: endValueMaskedTextBox
    });
  }, [value, viewConfig]);

  // handle value MaskedTextBoxStart
  const { start } = valueMaskedTextBox;
  useEffect(() => {
    if (!isTypingsRef.current) return;

    // we based on isTypingsRef.current to know if user typing or not
    // so we need to reset isTyingRefs.current to false for the next check
    isTypingsRef.current = false;

    const {
      value,
      handleValueMode,
      isValidDate,
      correctDateRange,
      dateConfig
    } = keepRef.current;

    // if value MaskedTextBoxStart is empty string
    if (start === '') {
      handleValueMode({ start: undefined, end: value?.end });
      return;
    }

    // parse MaskedTextBoxStart value to date object
    const dateParsed = datetime.parse(start, viewConfig.format);

    // if date is invalid, and length is full length (typings is completed)
    // just reset value MaskedTextBoxEnd to empty string
    if (!isValidDate(dateParsed)) {
      if (start.split('_').join('').length === viewConfig.validLength) {
        setValueMaskedTextBox((state) => ({ ...state, start: '' }));
      }
      return;
    }

    // otherwise, dateParsed is a Date valid, we need to call handleValueMode to update start date
    // we need to call correctDateRange to correct nextValue
    const nextValue = correctDateRange(dateConfig, {
      start: dateParsed,
      end: value?.end
    });
    handleValueMode(nextValue);
  }, [start, viewConfig]);

  // handle value MaskedTextBoxEnd
  const { end } = valueMaskedTextBox;
  useEffect(() => {
    if (!isTypingsRef.current) return;

    // we based on isTypingsRef.current to know if user typing or not
    // so we need to reset isTyingRefs.current to false for the next check
    isTypingsRef.current = false;

    const {
      value,
      handleValueMode,
      isValidDate,
      correctDateRange,
      dateConfig
    } = keepRef.current;

    // if value MaskedTextBoxEnd is empty string
    if (end === '') {
      handleValueMode({ start: value?.start, end: undefined });
      return;
    }

    // parse MaskedTextBoxEnd value to date object
    const dateParsed = datetime.parse(end, viewConfig.format);

    // if date is invalid, and length is full length (typings is completed)
    // just reset valueMaskedTextBox to empty string
    if (!isValidDate(dateParsed)) {
      if (end.split('_').join('').length === viewConfig.validLength) {
        setValueMaskedTextBox((state) => ({ ...state, end: '' }));
      }
      return;
    }

    // otherwise, dateParsed is a Date valid, we need to call handleValueMode to update end date
    // we need to call correctDateRange to correct nextValue
    const nextValue = correctDateRange(dateConfig, {
      start: value?.start,
      end: dateParsed
    });
    handleValueMode(nextValue);
  }, [end, viewConfig]);

  // handle truncate tooltip label
  useEffect(() => truncateTooltip(floatingLabelRef.current!), [label]);

  // handle autofocus first time render
  useEffect(() => {
    if (!autoFocusRef.current) return;

    // check details: setOpened
    process.nextTick(() => keepRef.current.inputStartElement?.focus());
  }, [keepRef.current.inputStartElement]);

  // handle resize MaskedTextBoxStart/MaskedTextBoxEnd
  useResizeElementFitText({
    element: keepRef.current.inputStartElement,
    text: valueMaskedTextBox.start
  });
  useResizeElementFitText({
    element: keepRef.current.inputEndElement,
    text: valueMaskedTextBox.end
  });

  // scroll auto close will trigger opened to false but onBlur is not triggered
  // we need trigger handleBlur manually
  useEffect(() => {
    if (opened) return;

    const maskedTextBoxStartElement = maskedTextBoxStart?.inputElement;
    const maskedTextBoxEndElement = maskedTextBoxEnd?.inputElement;

    // we are still focusing inside, maybe start input, maybe end input
    if (maskedTextBoxStartElement === document.activeElement) {
      maskedTextBoxStartElement?.blur();
    }
    if (maskedTextBoxEndElement === document.activeElement) {
      maskedTextBoxEndElement?.blur();
    }
  }, [opened, maskedTextBoxEnd, maskedTextBoxStart]);

  // this block code need to refactor
  // because react-calendar input will be difference all the time
  // we need to make sure, input is consistent all the time
  let valueNeedShow;
  if (value?.start && !value.end) {
    valueNeedShow = value.start;
  } else if (!value?.start && value?.end) {
    valueNeedShow = value.end;
  } else if (value?.start && value.end) {
    if (value.start > value.end) {
      valueNeedShow = [value?.end, value?.start];
    } else {
      valueNeedShow = [value?.start, value?.end];
    }
  }

  // handle add today button to Popup's DatePicker
  // for the first time render, viewRef.current is undefined
  const todayPortal = useTodayPortal(popupBaseRef, handleClickButtonFooter, {
    opened,
    viewConfig,
    view,
    isDisabled: checkDisabledToday()
  });

  // handle watching view change to trigger logic render tooltip
  const dateTooltipPortals = useDateTooltipPortals(popupBaseRef, {
    opened: opened && !readOnly,
    dateTooltip
  });

  const containerId = genAmtId(
    dataTestId,
    'dls-date-range-picker',
    'DateRangePicker'
  );

  return (
    <>
      <Tooltip
        opened={isOpenedTooltip}
        element={message}
        variant="error"
        placement="top-start"
        triggerClassName="d-block"
        dataTestId={genAmtId(containerId, 'error', 'DateRangePicker')}
        {...errorTooltipProps}
      >
        <div
          ref={containerRef}
          className={classnames(
            {
              [classes.dateRangePicker.container]: true,
              [classes.state.focused]: opened,
              [classes.state.readOnly]: readOnly,
              [classes.state.disabled]: disabled,
              [classes.state.error]: status,
              [classes.state.floating]: floating,
              [classes.state.noLabel]: noLabel,
              [classes.state.small]: isSmallSize
            },
            classNameProp
          )}
          data-testid={containerId}
        >
          <div
            ref={textFieldContainerRef}
            className="text-field-container"
            onMouseDown={handleOnMouseDown}
            onFocus={handleFocus}
            onBlur={handleBlur}
            tabIndex={-1}
          >
            <MaskedTextBox
              ref={setMaskedTextBoxStart}
              name="start"
              value={valueMaskedTextBox.start}
              onChange={handleOnChangeMaskedTextBox as any}
              onBlur={handleOnBlurMaskedTextBox}
              placeholder={viewConfig.placeholder}
              mask={viewConfig.mask}
              dataTestId={genAmtId(
                containerId,
                'input-start',
                'DateRangePicker'
              )}
            />
            <span
              className={classnames({
                separator: true,
                ['has-value']: value?.start || value?.end
              })}
            >
              -
            </span>
            <MaskedTextBox
              ref={setMaskedTextBoxEnd}
              name="end"
              value={valueMaskedTextBox.end}
              onChange={handleOnChangeMaskedTextBox as any}
              onBlur={handleOnBlurMaskedTextBox}
              placeholder={viewConfig.placeholder}
              mask={viewConfig.mask}
              dataTestId={genAmtId(containerId, 'input-end', 'DateRangePicker')}
            />
            {!noLabel && (
              <Label
                ref={floatingLabelRef}
                asterisk={isRequired}
                error={status}
                dataTestId={genAmtId(containerId, 'label', 'DateRangePicker')}
              >
                <span>{label}</span>
              </Label>
            )}
          </div>
          <Icon
            onMouseDown={handleOnMouseDownIcon}
            name="calendar"
            size={isSmallSize ? '4x' : '5x'}
            dataTestId={genAmtId(
              containerId,
              'calendar-icon',
              'DateRangePicker'
            )}
          />
          <div ref={fakeFocusRef} tabIndex={-1} />
        </div>
      </Tooltip>
      <PopupBase
        ref={popupBaseRef}
        reference={textFieldContainerRef.current!}
        opened={opened && !readOnly && !disabled}
        onVisibilityChange={handleOpenedMode}
        onPopupShown={handleOnPopupShown}
        onPopupClosed={handleOnPopupClosed}
        popupBaseClassName={classnames(
          'dls-popup',
          'dls-date-picker-popup',
          popupBaseClassName
        )}
        fluid
        dataTestId={genAmtId(containerId, 'popup', 'DateRangePicker')}
        {...popupProps}
      >
        <Calendar
          value={valueNeedShow as any}
          onChange={handleOnChangeCalendar}
          formatShortWeekday={formatShortWeekday}
          formatMonth={formatMonth}
          calendarType="US"
          tileDisabled={handleTitleDisabled}
          tileContent={handleTileContent}
          minDetail={viewConfig.minDetail}
          maxDetail={viewConfig.maxDetail}
          showDoubleView
          allowPartialRange
          selectRange={selectRange}
        />
        {todayPortal}
        {dateTooltipPortals}
      </PopupBase>
    </>
  );
};

export * from './types';
export default DateRangePicker;
