// testing library
import '@testing-library/jest-dom';

// hooks
import correctDateRange from './correctDateRange';
import { DateRangeConfig } from '../types';

const dateConfig: DateRangeConfig = {
  min: {
    year: 2021,
    month: 5,
    beginDate: new Date('05/10/2021')
  },
  max: {
    year: 2022,
    month: 5,
    beginDate: new Date('05/10/2022')
  }
};

describe('test correctDateRange', () => {
  it('when start more than end, start is end, end is start', () => {
    const inputTest = {
      start: new Date('07/10/2021'),
      end: new Date('06/10/2021')
    };
    const nextValue = correctDateRange(dateConfig, inputTest);

    expect(nextValue).toEqual({ start: inputTest.end, end: inputTest.start });
  });

  it('when start less than min.beginDate, start is min.beginDate', () => {
    const nextValue = correctDateRange(dateConfig, {
      start: new Date('04/10/2021')
    });

    expect(nextValue.start).toEqual(dateConfig.min.beginDate);
  });

  it('when start more than max.beginDate, start is max.beginDate', () => {
    const nextValue = correctDateRange(dateConfig, {
      start: new Date('06/10/2022')
    });

    expect(nextValue.start).toEqual(dateConfig.max.beginDate);
  });

  it('when end less than min.beginDate, end is min.beginDate', () => {
    const nextValue = correctDateRange(dateConfig, {
      end: new Date('04/10/2021')
    });

    expect(nextValue.end).toEqual(dateConfig.min.beginDate);
  });

  it('when end more than max.beginDate, end is max.beginDate', () => {
    const nextValue = correctDateRange(dateConfig, {
      end: new Date('06/10/2022')
    });

    expect(nextValue.end).toEqual(dateConfig.max.beginDate);
  });
});
