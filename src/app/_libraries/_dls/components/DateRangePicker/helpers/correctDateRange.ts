// helpers
import isValidDate from './isValidDate';

// types
import {
  CorrectDateRange,
  DateRangeConfig,
  DateRangePickerValue
} from '../types';

// Help to correct date range
// this helper will help:
//    - if start > end, we will swap it
//    - if start < min, we will update start to min
//    - if start > max, we will update start to max
//    - if end < min, we will update end to min
//    - if end > max, we will update end to max
const correctDateRange: CorrectDateRange = (
  dateConfig: DateRangeConfig,
  value?: DateRangePickerValue
) => {
  let nextValue = { ...value };
  const { min, max } = dateConfig;

  if (
    isValidDate(nextValue?.start) &&
    isValidDate(nextValue?.end) &&
    nextValue.start > nextValue.end
  ) {
    nextValue = { start: nextValue.end, end: nextValue.start };
  }

  if (isValidDate(nextValue.start) && nextValue.start < min.beginDate) {
    nextValue.start = min.beginDate;
  }

  if (isValidDate(nextValue.start) && nextValue.start > max.beginDate) {
    nextValue.start = max.beginDate;
  }

  if (isValidDate(nextValue.end) && nextValue.end < min.beginDate) {
    nextValue.end = min.beginDate;
  }

  if (isValidDate(nextValue.end) && nextValue.end > max.beginDate) {
    nextValue.end = max.beginDate;
  }

  return nextValue;
};

export default correctDateRange;
