import React from 'react';
import {
  fireEvent,
  queryHelpers,
  render,
  RenderResult,
  screen
} from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';

// components
import { DateRangePickerProps } from '.';
import DateRangePickerWrapper from './DateRangePickerMock';

// mocks
import '../../test-utils/mocks/mockCanvas';

// utils
import datetime from 'date-and-time';
import mockDate from '../../test-utils/mockDate';
import Button from '../Button';
import Tooltip from '../Tooltip';

let wrapper: RenderResult;

interface RenderComponentProps extends Omit<DateRangePickerProps, 'children'> {}

const startString = '05/10/2021';
const endString = '05/11/2021';
const todayString = '05/12/2021';
const fromDateString = '01/01/2021';
const toDateString = '05/01/2021';

const start = new Date(startString);
const end = new Date(endString);
const today = new Date(todayString);
const fromDate = new Date(fromDateString);
const toDate = new Date(toDateString);

const renderComponent = (props: RenderComponentProps) => {
  jest.useFakeTimers();

  wrapper = render(
    <div>
      <DateRangePickerWrapper
        name="dateRange"
        value={{ start, end }}
        label="DateRangePicker"
        {...props}
      />
    </div>
  );

  jest.runAllTimers();

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement,
    rerender: (props: DateRangePickerProps) => {
      return wrapper.rerender(
        <div>
          <DateRangePickerWrapper
            name="dateRange"
            value={{ start, end }}
            label="DateRangePicker"
            {...props}
          />
        </div>
      );
    },
    queryByText: wrapper.queryByText
  };
};

const getNode = {
  get inputStart() {
    return wrapper.container.querySelectorAll(
      '.dls-masked-textbox input'
    )[0] as HTMLInputElement;
  },
  get inputEnd() {
    return wrapper.container.querySelectorAll(
      '.dls-masked-textbox input'
    )[1] as HTMLInputElement;
  },
  get textField() {
    return wrapper.container.querySelectorAll(
      '.text-field-container'
    )[0] as HTMLDivElement;
  },
  get iconCalendar() {
    return wrapper.container.querySelector('.icon-calendar') as HTMLElement;
  },
  get container() {
    return wrapper.container.querySelector(
      '.dls-date-range-picker'
    ) as HTMLDivElement;
  },
  containerWidth: (className: string) => {
    let query = '.dls-date-range-picker';
    if (className) query += `.${className}`;

    return wrapper.container.querySelector(query) as HTMLDivElement;
  }
};

let dateSpy: jest.SpyInstance;

beforeEach(() => {
  jest.useFakeTimers();

  mockDate.set(today.getTime());
});

afterEach(() => {
  wrapper?.unmount();
  dateSpy?.mockRestore();

  mockDate.reset();
});

const queryByDate = (date: Date | string) => {
  return queryHelpers.queryByAttribute(
    'aria-label',
    wrapper.baseElement as HTMLElement,
    datetime.format(new Date(date), 'MMMM D, YYYY')
  );
};

const queryByMonth = (date: Date | string) => {
  return queryHelpers.queryByAttribute(
    'aria-label',
    wrapper.baseElement as HTMLElement,
    datetime.format(new Date(date), 'MMMM YYYY')
  );
};

const userEventClick = (element: HTMLElement) => {
  userEvent.click(element);
  jest.runAllTimers();
};

describe('hook useDateTooltipPortal', () => {
  it('should return if opened is false', () => {
    renderComponent({
      opened: false,
      dateTooltip: (date) => <Tooltip element="Date tooltip" />
    });

    expect(true).toBeTruthy();
  });

  it('should run dateTooltip feature', () => {
    renderComponent({
      opened: true,
      dateTooltip: (date) => <Tooltip element="Date tooltip" />
    });

    expect(true).toBeTruthy();
  });

  it('should return if dateTooltip return undefined', () => {
    renderComponent({
      opened: true,
      dateTooltip: (date) => undefined
    });

    expect(true).toBeTruthy();
  });

  it('should call onClickTrigger if date is clicked', () => {
    const { baseElement } = renderComponent({
      opened: true,
      dateTooltip: (date) => <Tooltip element="Date tooltip" />
    });

    const calendarElement = baseElement.querySelector('.react-calendar');

    const tooltipTrigger = calendarElement?.querySelector(
      '.dls-tooltip-date-picker'
    );
    userEvent.click(tooltipTrigger!);

    expect(true).toBeTruthy();
  });

  it('should cover century/decade switch case', () => {
    const { baseElement } = renderComponent({
      opened: true,
      dateTooltip: (date) => <Tooltip element="Date tooltip" />
    });

    const calendarElement = baseElement.querySelector('.react-calendar');
    const navigationElement = calendarElement?.querySelector(
      '.react-calendar__navigation__label'
    );

    userEvent.click(navigationElement!);
    userEvent.click(navigationElement!);

    expect(true).toBeTruthy();
  });
});

describe('Render', () => {
  it('test render with small size', () => {
    renderComponent({ size: 'small' });

    expect(document.querySelector('.dls-small')).toBeInTheDocument();
  });

  it('test render with month view', () => {
    renderComponent({ opened: true, view: 'month' });

    expect(getNode.inputStart.value).toEqual(startString);
    expect(getNode.inputEnd.value).toEqual(endString);
    expect(getNode.container).toBeInTheDocument();
  });

  it('test render with year view', () => {
    renderComponent({ opened: true, view: 'year' });

    expect(getNode.inputStart.value).toEqual('05/2021');
    expect(getNode.inputEnd.value).toEqual('05/2021');
    expect(getNode.container).toBeInTheDocument();
  });

  it('test render when empty value', () => {
    renderComponent({
      opened: true,
      value: {}
    });

    expect(getNode.inputStart.value).toEqual('');
    expect(getNode.inputEnd.value).toEqual('');
    expect(getNode.containerWidth('dls-focused')).toBeInTheDocument();

    HTMLElement.prototype.innerText = undefined as any;
  });

  it('test render when start date greater than end date', () => {
    renderComponent({
      opened: true,
      value: { start: end, end: start }
    });

    expect(getNode.inputStart.value).toEqual(endString);
    expect(getNode.inputEnd.value).toEqual(startString);
    expect(getNode.containerWidth('dls-focused')).toBeInTheDocument();
  });

  it('test render without start date range', () => {
    renderComponent({ value: { end } });

    expect(getNode.inputStart.value).toEqual('');
    expect(getNode.inputEnd.value).toEqual(endString);
    expect(getNode.container).toBeInTheDocument();
  });

  it('test render without end date range', () => {
    renderComponent({ value: { start } });

    expect(getNode.inputStart.value).toEqual(startString);
    expect(getNode.inputEnd.value).toEqual('');
    expect(getNode.container).toBeInTheDocument();
  });

  it('test render when readOnly', () => {
    renderComponent({ readOnly: true });

    expect(getNode.inputStart.value).toEqual(startString);
    expect(getNode.inputEnd.value).toEqual(endString);
    expect(getNode.containerWidth('dls-readonly')).toBeInTheDocument();
  });

  it('test render when required', () => {
    renderComponent({
      required: true,
      readOnly: false,
      disabled: false
    });

    expect(getNode.inputStart.value).toEqual(startString);
    expect(getNode.inputEnd.value).toEqual(endString);
    expect(document.querySelector('.asterisk')).toBeInTheDocument();
  });

  it('test render when disabled', () => {
    renderComponent({ disabled: true });

    expect(getNode.inputStart.value).toEqual(startString);
    expect(getNode.inputEnd.value).toEqual(endString);
    expect(getNode.containerWidth('dls-disabled')).toBeInTheDocument();
  });

  describe('test render when error', () => {
    it('when hide dropdown', () => {
      const message = 'error message';
      renderComponent({ error: { status: true, message } });

      expect(getNode.inputStart.value).toEqual(startString);
      expect(getNode.inputEnd.value).toEqual(endString);
      expect(getNode.containerWidth('dls-error')).toBeInTheDocument();
      expect(screen.queryByText(message)).toBeNull();
    });

    it('when show dropdown', () => {
      const message = 'error message';
      renderComponent({ opened: true, error: { status: true, message } });

      expect(getNode.inputStart.value).toEqual(startString);
      expect(getNode.inputEnd.value).toEqual(endString);
      expect(getNode.containerWidth('dls-error')).toBeInTheDocument();
      expect(screen.getByText('error message')).toBeInTheDocument();
    });
  });

  it('test render when autofocus', () => {
    const onFocus = jest.fn();

    renderComponent({ opened: true, autoFocus: true, onFocus });

    expect(true).toEqual(true);
  });

  it('test render when minDate greater than maxDate', () => {
    renderComponent({
      opened: true,
      autoFocus: true,
      minDate: end,
      maxDate: start
    });

    expect(true).toEqual(true);
  });

  it('test render when startdate less than min.beginDate', () => {
    const startDate = new Date(startString);
    const minBeginDate = new Date(startString);
    minBeginDate.setDate(minBeginDate.getDate() - 1);

    renderComponent({ value: { start: startDate }, minDate: minBeginDate });
  });
});

describe('Actions', () => {
  describe('titleDisabled', () => {
    it('controlled', () => {
      const titleDisabled = jest.fn();
      renderComponent({
        opened: true,
        value: undefined,
        titleDisabled
      });

      expect(titleDisabled).toBeCalled();
    });

    it('without titleDisabled', () => {
      const { wrapper } = renderComponent({
        opened: true,
        minDate: new Date('05/10/2021'),
        maxDate: new Date('09/10/2021')
      });

      const navigateButton = wrapper.baseElement.querySelector(
        '.react-calendar__navigation__label'
      )!;
      userEvent.click(navigateButton);
      userEvent.click(navigateButton);
      userEvent.click(navigateButton);

      expect(navigateButton).toBeInTheDocument();
    });

    it('without titleDisabled, cover century', () => {
      const { wrapper } = renderComponent({
        opened: true,
        minDate: new Date('05/10/2200'),
        maxDate: new Date('09/10/2200')
      });

      const navigateButton = wrapper.baseElement.querySelector(
        '.react-calendar__navigation__label'
      )!;
      userEvent.click(navigateButton);
      userEvent.click(navigateButton);
      userEvent.click(navigateButton);

      expect(navigateButton).toBeInTheDocument();
    });

    it('in view year', () => {
      renderComponent({
        opened: true,
        value: undefined,
        view: 'year',
        // new Date() -> 05/12/2021
        minDate: new Date('05/10/2021'),
        maxDate: new Date('09/10/2021')
      });

      expect(queryByMonth('04/10/2021')!.closest('button')).toBeDisabled();
      expect(queryByMonth('10/10/2021')!.closest('button')).toBeDisabled();
    });

    it('in view month', () => {
      renderComponent({
        opened: true,
        value: undefined,
        // new Date() -> 05/12/2021
        minDate: new Date('05/10/2021'),
        maxDate: new Date('05/10/2021')
      });

      expect(queryByDate('05/09/2021')!.closest('button')).toBeDisabled();
      expect(queryByDate('05/11/2021')!.closest('button')).toBeDisabled();
    });

    it('disabled range with full range', () => {
      renderComponent({
        opened: true,
        value: undefined,
        disabledRange: [{ fromDate, toDate }]
      });
      renderComponent({
        opened: true,
        value: undefined,
        disabledRange: [{}]
      });
      expect(true).toBeTruthy();
    });
  });

  describe('handleOnChangeCalendar', () => {
    describe('without value', () => {
      it('input start date was focused', () => {
        const newStartString = '05/13/2021';
        const newEndString = '05/15/2021';

        renderComponent({ opened: true, value: undefined });

        userEventClick(getNode.inputStart);
        expect(document.activeElement === getNode.inputStart).toBeTruthy();

        userEventClick(queryByDate(newStartString)!);
        userEventClick(queryByDate(newEndString)!);

        expect(document.activeElement === getNode.inputStart).toBeFalsy();
        expect(getNode.inputStart.value).toEqual(newStartString);
        expect(getNode.inputEnd.value).toEqual(newEndString);
      });

      it('input end date was focused', () => {
        const newStartString = '05/13/2021';
        const newEndString = '05/15/2021';

        renderComponent({ opened: true, value: undefined });

        userEventClick(getNode.inputEnd);
        expect(document.activeElement === getNode.inputEnd).toBeTruthy();

        userEventClick(queryByDate(newStartString)!);
        userEventClick(queryByDate(newEndString)!);

        expect(document.activeElement === getNode.inputEnd).toBeFalsy();
        expect(getNode.inputStart.value).toEqual(newStartString);
        expect(getNode.inputEnd.value).toEqual(newEndString);
      });
    });

    it('with start date and end date', () => {
      const newStartString = '05/13/2021';
      const newEndString = '05/15/2021';

      renderComponent({ opened: true, value: { start, end } });

      userEventClick(queryByDate(newStartString)!);
      userEventClick(queryByDate(newEndString)!);

      expect(getNode.inputStart.value).toEqual(newStartString);
      expect(getNode.inputEnd.value).toEqual(newEndString);
    });

    it('with start date', () => {
      const newEndString = '05/15/2021';

      renderComponent({ opened: true, value: { start } });

      userEventClick(queryByDate(newEndString)!);

      expect(getNode.inputStart.value).toEqual(startString);
      expect(getNode.inputEnd.value).toEqual(newEndString);
    });

    it('with start date 2', () => {
      const newEndString = '05/15/2021';

      renderComponent({ opened: true, value: { start } });

      userEventClick(queryByDate(newEndString)!);

      expect(getNode.inputStart.value).toEqual(startString);
      expect(getNode.inputEnd.value).toEqual(newEndString);
    });

    it('with end date', () => {
      const newStartString = '05/13/2021';

      renderComponent({ opened: true, value: { end } });

      userEventClick(queryByDate(newStartString)!);

      expect(getNode.inputStart.value).toEqual(endString);
      expect(getNode.inputEnd.value).toEqual(newStartString);
    });

    it('handleValueMode should be call', () => {
      renderComponent({
        opened: true
      });

      userEvent.click(queryByDate('05/13/2021')!);
      userEvent.click(queryByDate('05/15/2021')!);
      fireEvent.focus(getNode.textField);
      getNode.textField.focus();

      jest.runAllTimers();
      expect(true).toBeTruthy();
    });

    it('handleOnChangeCalendar should be call', () => {
      const { wrapper } = renderComponent({
        opened: true
      });

      userEvent.click(queryByDate('05/13/2021')!);
      userEvent.click(queryByDate('05/15/2021')!);
      fireEvent.focus(getNode.textField);
      getNode.textField.focus();

      jest.runAllTimers();

      const todayBtn = wrapper.getByText('Today')!;
      userEvent.click(todayBtn);
      userEvent.click(todayBtn);

      expect(true).toBeTruthy();
    });

    it('handleOnChangeCalendar should be call final case', () => {
      const { wrapper } = renderComponent({
        opened: true,
        value: undefined
      });

      fireEvent.focus(getNode.textField);
      getNode.textField.focus();

      jest.runAllTimers();

      const todayBtn = wrapper.getByText('Today')!;
      userEvent.click(todayBtn);
      userEvent.click(todayBtn);

      expect(true).toBeTruthy();
    });
  });

  describe('handleFocus', () => {
    it('when disabled', () => {
      const onFocus = jest.fn();

      renderComponent({ onFocus, value: { end, start }, disabled: true });

      fireEvent.focus(getNode.textField);

      expect(onFocus).not.toBeCalled();
    });

    it('when enabled', () => {
      const onFocus = jest.fn();

      renderComponent({ onFocus, value: { end, start }, opened: true });

      fireEvent.focus(getNode.textField);

      expect(onFocus).not.toBeCalled();
    });

    describe('when enabled and autofocus', () => {
      it('with start date and end date', () => {
        const onFocus = jest.fn();

        renderComponent({
          label: 'label',
          onFocus,
          value: { end, start },
          opened: true,
          autoFocus: true
        });

        fireEvent.focus(getNode.textField);

        expect(onFocus).toBeCalledWith(
          expect.objectContaining({ value: { start, end } })
        );
      });

      it('with only start date', () => {
        renderComponent({
          opened: true
        });

        userEvent.click(queryByDate('05/13/2021')!);
        userEvent.click(document.body);
        fireEvent.focus(getNode.textField);
        getNode.textField.focus();

        jest.runAllTimers();
        expect(true).toBeTruthy();
      });

      it('with only end date', () => {
        renderComponent({
          value: {
            end
          }
        });

        userEvent.click(getNode.textField);
        jest.runAllTimers();

        expect(getNode.inputStart).toHaveFocus();
      });

      it('without dates', () => {
        const onFocus = jest.fn();

        renderComponent({
          label: 'label',
          onFocus,
          value: undefined,
          opened: true,
          autoFocus: true
        });

        fireEvent.focus(getNode.textField);

        expect(getNode.inputStart).toHaveFocus();
        expect(onFocus).toBeCalledWith({ target: { name: 'dateRange' } });
      });

      it('without dates and opened', () => {
        const onFocus = jest.fn();

        renderComponent({
          label: 'label',
          onFocus,
          value: undefined,
          opened: true,
          autoFocus: true
        });

        fireEvent.focus(getNode.textField);

        expect(getNode.inputStart).toHaveFocus();
        expect(onFocus).toBeCalledWith({ target: { name: 'dateRange' } });
      });
    });
  });

  describe('handleBlur', () => {
    it('when clicked input start date before blur', () => {
      const onBlur = jest.fn();

      renderComponent({ onBlur, value: { end, start } });

      userEventClick(getNode.inputStart);
      expect(getNode.inputStart === document.activeElement).toBeTruthy();

      fireEvent.blur(getNode.textField);
      expect(getNode.inputStart === document.activeElement).toBeFalsy();

      expect(onBlur).toBeCalledWith(
        expect.objectContaining({ value: { end, start } })
      );
    });

    it('when clicked input end date before blur', () => {
      const onBlur = jest.fn();

      renderComponent({ onBlur, value: { end, start } });

      userEventClick(getNode.inputEnd);
      expect(getNode.inputEnd === document.activeElement).toBeTruthy();

      fireEvent.blur(getNode.textField);
      expect(getNode.inputEnd === document.activeElement).toBeFalsy();

      expect(onBlur).toBeCalledWith(
        expect.objectContaining({ value: { end, start } })
      );
    });

    it('when clicked input start date and button end date before blur', () => {
      const onBlur = jest.fn();

      renderComponent({ onBlur, value: { end, start } });

      userEventClick(getNode.inputStart);
      expect(getNode.inputStart === document.activeElement).toBeTruthy();

      userEventClick(getNode.inputEnd);
      expect(getNode.inputEnd === document.activeElement).toBeTruthy();

      fireEvent.blur(getNode.textField);
      expect(getNode.inputEnd === document.activeElement).toBeFalsy();

      expect(onBlur).toBeCalledWith(
        expect.objectContaining({ value: { end, start } })
      );
    });

    it('should call blur when closeOnScroll', () => {
      const { rerender } = renderComponent({ opened: true });

      getNode.inputStart.focus();
      rerender({ opened: false });
      rerender({ opened: true });

      getNode.inputEnd.focus();
      rerender({ opened: false });
      rerender({ opened: true });

      expect(true).toBeTruthy();
    });

    it('cover 100%', () => {
      const onBlur = jest.fn();

      renderComponent({ onBlur, value: { end, start } });

      userEventClick(getNode.inputStart);

      fireEvent.focus(getNode.textField);
      getNode.textField.focus();

      fireEvent.blur(getNode.textField);

      expect(true).toEqual(true);
    });
  });

  describe('handleOnChangeMaskedTextBox', () => {
    describe('in input start', () => {
      it('when disabled', () => {
        renderComponent({ value: { end }, disabled: true });

        userEvent.type(getNode.inputStart, '05132021');

        expect(getNode.inputStart.value).toEqual('');
      });

      describe('when enabled', () => {
        it('typing correct format', () => {
          renderComponent({ value: { end } });

          userEvent.type(getNode.inputStart, '05102021');
          expect(getNode.inputStart.value).toEqual('05/10/2021');

          // typing backspace 1 times
          userEvent.type(getNode.inputStart, '{backspace}');
          // unfocus inputStart
          userEventClick(document.body);
          expect(getNode.inputStart.value).toEqual('05/10/2021');

          // typing backspace 10 times
          userEvent.type(
            getNode.inputStart,
            new Array(10).fill('{backspace}').join('')
          );
          expect(getNode.inputStart.value).toEqual('');
        });

        it('typing wrong format', () => {
          renderComponent({ value: {} });

          userEvent.type(getNode.inputStart, '99999999');
          expect(getNode.inputStart.value).toEqual('');
        });
      });
    });

    describe('in input end', () => {
      it('when disabled', () => {
        renderComponent({ value: { start }, disabled: true });

        userEvent.type(getNode.inputEnd, '05132021');

        expect(getNode.inputEnd.value).toEqual('');
      });

      describe('when enabled', () => {
        it('typing correct format', () => {
          renderComponent({ value: { start } });

          userEvent.type(getNode.inputEnd, '05112021');
          expect(getNode.inputEnd.value).toEqual('05/11/2021');

          // typing backspace 1 times
          userEvent.type(getNode.inputEnd, '{backspace}');
          // unfocus inputStart
          userEventClick(document.body);
          expect(getNode.inputEnd.value).toEqual('05/11/2021');

          userEvent.type(
            getNode.inputEnd,
            new Array(10).fill('{backspace}').join('')
          );
          expect(getNode.inputEnd.value).toEqual('');
        });

        it('typing wrong format', () => {
          renderComponent({ value: { start } });

          userEvent.type(getNode.inputEnd, '99999999');
          expect(getNode.inputEnd.value).toEqual('');
        });
      });
    });
  });

  describe('handleOnMouseDownIcon', () => {
    it('when disabled', () => {
      const onVisibilityChange = jest.fn();

      renderComponent({ value: { start }, onVisibilityChange, disabled: true });

      userEventClick(getNode.iconCalendar);

      expect(onVisibilityChange).not.toBeCalled();
    });

    it('handleOnMouseDownIcon cover 100%', () => {
      jest.useFakeTimers();
      renderComponent({ opened: true });

      fireEvent.focus(getNode.textField);
      getNode.textField.focus();

      userEventClick(getNode.iconCalendar);

      jest.runAllTimers();

      expect(true).toBeTruthy();
    });

    describe('when enabled', () => {
      describe('body was focused', () => {
        it('with start date and end date', () => {
          const onVisibilityChange = jest.fn();

          renderComponent({ value: { start, end }, onVisibilityChange });

          userEventClick(getNode.iconCalendar);
          expect(document.activeElement === getNode.inputEnd).toBeTruthy();

          expect(onVisibilityChange).toBeCalledWith(true);
        });

        it('with start date', () => {
          const onVisibilityChange = jest.fn();

          renderComponent({ value: { start }, onVisibilityChange });

          userEventClick(getNode.iconCalendar);
          expect(document.activeElement === getNode.inputEnd).toBeTruthy();

          expect(onVisibilityChange).toBeCalledWith(true);
        });

        it('with end date', () => {
          const onVisibilityChange = jest.fn();

          renderComponent({ value: { end }, onVisibilityChange });

          userEventClick(getNode.iconCalendar);
          expect(document.activeElement === getNode.inputStart).toBeTruthy();

          expect(onVisibilityChange).toBeCalledWith(true);
        });

        it('without value', () => {
          const onVisibilityChange = jest.fn();

          renderComponent({ value: {}, onVisibilityChange });

          userEventClick(getNode.iconCalendar);
          expect(document.activeElement === getNode.inputStart).toBeTruthy();

          expect(onVisibilityChange).toBeCalledWith(true);
        });

        it('without value but opened', () => {
          const onVisibilityChange = jest.fn();

          renderComponent({ opened: true, value: {}, onVisibilityChange });

          userEventClick(getNode.iconCalendar);
          expect(document.activeElement === getNode.inputStart).toBeTruthy();

          expect(onVisibilityChange).toBeCalledWith(true);
        });
      });

      it('input start date was focused', () => {
        const onVisibilityChange = jest.fn();

        renderComponent({ opened: true, value: {}, onVisibilityChange });

        userEventClick(getNode.inputStart);
        expect(document.activeElement === getNode.inputStart).toBeTruthy();

        userEventClick(getNode.iconCalendar);
        expect(document.activeElement === getNode.inputStart).toBeFalsy();

        expect(onVisibilityChange).toBeCalledWith(true);
      });

      it('input end date was focused', () => {
        const onVisibilityChange = jest.fn();

        renderComponent({ opened: true, value: {}, onVisibilityChange });

        userEventClick(getNode.inputEnd);
        expect(document.activeElement === getNode.inputEnd).toBeTruthy();

        userEventClick(getNode.iconCalendar);
        expect(document.activeElement === getNode.inputEnd).toBeFalsy();

        expect(onVisibilityChange).toBeCalledWith(true);
      });
    });
  });

  describe('handleClickButtonFooter', () => {
    describe('when has no values', () => {
      it('when today greater than maxDate', () => {
        const yesterday = '05/11/2021';
        renderComponent({
          opened: true,
          value: { end },
          maxDate: new Date(yesterday)
        });

        fireEvent.click(screen.getByText('Today'));
        fireEvent.click(screen.getByText('Today'));

        // today is 05/12/2021
        expect(getNode.inputStart.value).toEqual('');
        expect(getNode.inputEnd.value).toEqual(endString);
      });

      it('when today less than minDate', () => {
        const tomorrow = '05/13/2021';

        renderComponent({
          opened: true,
          value: { start },
          minDate: new Date(tomorrow) // 05/13/2021
        });

        fireEvent.click(screen.getByText('Today'));
        fireEvent.click(screen.getByText('Today'));

        // today is 05/12/2021
        expect(getNode.inputStart.value).toEqual(startString); // maxDate
        expect(getNode.inputEnd.value).toEqual(''); // maxDate
      });
    });

    it('when has start date', () => {
      renderComponent({ opened: true, value: { start } });

      fireEvent.click(screen.getByText('Today'));

      expect(getNode.inputStart.value).toEqual(startString);
      expect(getNode.inputEnd.value).toEqual(todayString);
    });

    it('when has end date', () => {
      renderComponent({ opened: true, value: { end } });

      fireEvent.click(screen.getByText('Today'));

      expect(getNode.inputStart.value).toEqual(endString);
      expect(getNode.inputEnd.value).toEqual(todayString);
    });

    it('when has start date and end date', () => {
      renderComponent({ opened: true, value: { start, end } });

      fireEvent.click(screen.getByText('Today'));

      expect(getNode.inputStart.value).toEqual(todayString);
      expect(getNode.inputEnd.value).toEqual('');
    });
  });

  it('should cover handlePopupClosed and apply next tabIndex is fakeFocusRef.current', () => {
    jest.useFakeTimers();
    const { queryByText, container } = render(
      <div>
        <DateRangePickerWrapper name="dateRange" label="DateRangePicker" />
        <Button autoBlur={false}>Test handleOnPopupClosed</Button>
      </div>
    );

    const textFieldContainer = container.querySelector(
      '.text-field-container'
    )!;

    const buttonElement = queryByText('Test handleOnPopupClosed')!;
    userEvent.click(textFieldContainer);
    jest.runAllTimers();

    userEvent.click(document.body);
    jest.runAllTimers();

    userEvent.click(textFieldContainer);
    jest.runAllTimers();

    userEvent.click(buttonElement);
    jest.runAllTimers();

    expect(true).toBeTruthy();
  });
});
