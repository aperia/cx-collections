import React, { useLayoutEffect, useRef, useState } from 'react';
import ReactDOM from 'react-dom';

// components/types
import { PopupBaseRef } from '../../PopupBase';
import CalendarFooter from '../CalendarFooter';
import { Detail } from 'react-calendar';

// utils
import { get } from '../../../lodash';

export interface TodayPortalHookDependencies {
  opened?: boolean;
  viewConfig?: any;
  view?: Detail;
  isDisabled?: boolean;
}

export interface TodayPortalHook {
  (
    popupBaseRef: React.MutableRefObject<PopupBaseRef | null>,
    onClick: () => void,
    dependencies: TodayPortalHookDependencies
  ): void;
}

const useTodayPortal: TodayPortalHook = (
  popupBaseRef,
  onClick,
  { opened, viewConfig, view, isDisabled }
) => {
  // refs
  const keepRef = useRef<Record<string, any>>({});

  // states
  const [todayPortal, setTodayPortal] =
    useState<React.ReactPortal | null>(null);

  keepRef.current.popupBaseRef = popupBaseRef;
  keepRef.current.onClick = onClick;

  useLayoutEffect(() => {
    if (!opened || !viewConfig || !view) return;

    process.nextTick(() => {
      const { onClick, popupBaseRef } = keepRef.current;
      const popupElement = get(popupBaseRef.current, 'element') as HTMLElement;
      if (!popupElement) return;

      const reactCalendarElement =
        popupElement.querySelector('.react-calendar');

      const portal =
        reactCalendarElement &&
        ReactDOM.createPortal(
          <CalendarFooter
            disabled={isDisabled}
            label={viewConfig.label}
            onClick={onClick}
          />,
          reactCalendarElement
        );

      setTodayPortal(portal);
    });
  }, [opened, viewConfig, view, isDisabled]);

  return todayPortal;
};

export default useTodayPortal;
