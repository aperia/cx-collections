import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';

// components
import TabBarNavigationItem from './TabBarNavigationItem';

// utils
import { queryByClass } from '../../test-utils/queryHelpers';

describe('Render', () => {
  it('should render', () => {
    const { baseElement } = render(<TabBarNavigationItem index={0} />);

    expect(
      queryByClass(baseElement as HTMLElement, /dls-item-wrap/)
    ).toBeInTheDocument();
  });

  it('should render with active class', () => {
    const { baseElement } = render(<TabBarNavigationItem index={0} active />);

    expect(
      queryByClass(baseElement as HTMLElement, /state-active/)
    ).toBeInTheDocument();
  });

  it('should render with disabled class', () => {
    const { baseElement } = render(<TabBarNavigationItem index={0} disabled />);

    expect(
      queryByClass(baseElement as HTMLElement, /state-disabled/)
    ).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('onSelect', () => {
    const onSelect = jest.fn();
    const title = 'tab item title';

    render(
      <TabBarNavigationItem index={0} title={title} onSelect={onSelect} />
    );

    screen.getByText(title).click();

    expect(onSelect).toBeCalledWith(0);
  });
});
