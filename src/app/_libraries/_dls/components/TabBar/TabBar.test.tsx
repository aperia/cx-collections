import React, { MutableRefObject } from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';

// components
import { TabBar, TabBarTab } from '.';
import Icon from '../../components/Icon';

// utils
import { queryByClass } from '../../test-utils/queryHelpers';

const mockRef: MutableRefObject<HTMLDivElement | null> = { current: null };

const tabs = [
  {
    id: 'home',
    title: 'Home',
    icon: <Icon name="home" size="4x" />,
    children: 'Content Home Tab'
  },
  {
    id: 'tab1',
    title: 'Tab Name 1',
    icon: <Icon name="account-details" size="4x" />,
    children: 'Content Tab 01'
  }
];

const renderComponent = (onSelect?: jest.Mock, selected?: number) => {
  const wrapper = render(
    <TabBar ref={mockRef} onSelect={onSelect} selected={selected}>
      {tabs.map(({ id, title, icon, ...tabProps }) => {
        return (
          <TabBarTab
            key={id}
            title={
              <React.Fragment>
                {icon}
                <span className="tab-title" id={id}>
                  {title}
                </span>
                {<Icon name="close" onClick={jest.fn()} />}
              </React.Fragment>
            }
            hoverTitle={title}
            {...tabProps}
          />
        );
      })}
    </TabBar>
  );

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement
  };
};

describe('Render', () => {
  it('render', () => {
    const { baseElement } = renderComponent();

    expect(queryByClass(baseElement, 'dls-tabbar')).toBeInTheDocument();
  });

  it('render with empty children', () => {
    render(<TabBar ref={mockRef}></TabBar>);
  });
});

describe('Actions', () => {
  describe('onSelect', () => {
    it('uncontrolled', () => {
      const { baseElement } = renderComponent();

      screen.getByText('Tab Name 1').click();

      const tabNavBody = queryByClass(
        baseElement,
        /dls-nav-body/
      ) as HTMLElement;

      // don/t support uncontrolled
      expect(queryByClass(tabNavBody, /state-active/)).toBeNull();
    });

    it('controlled', () => {
      const onSelect = jest.fn();

      renderComponent(onSelect, 0);

      screen.getByText('Home').click();

      expect(onSelect).not.toBeCalled();

      screen.getByText('Tab Name 1').click();

      expect(onSelect).toBeCalledWith({ selected: 1 });
    });
  });
});
