import { render } from '@testing-library/react';
import React from 'react';

// components
import TabBarTab from './TabBarTab';

describe('Render', () => {
  it('render should return null', () => {
    const { container } = render(<TabBarTab />);

    expect(container.children.length).toEqual(0);
  });
});
