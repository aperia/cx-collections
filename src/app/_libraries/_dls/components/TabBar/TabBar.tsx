import React, {
  RefForwardingComponent,
  Children,
  forwardRef,
  useRef,
  useImperativeHandle
} from 'react';

// components/types
import TabStripNavigation from './TabBarNavigation';
import TabStripContent from './TabBarContent';
import { TabBarProps } from './types';

// utils
import { className, genAmtId } from '../../utils';
import classNames from 'classnames';

const TabBar: RefForwardingComponent<HTMLDivElement, TabBarProps> = (
  props,
  ref
) => {
  const { id, selected, onSelect: onSelectProps, children, dataTestId } = props;

  const containerRef = useRef<HTMLDivElement | null>(null);
  useImperativeHandle(ref, () => containerRef.current!);

  const onSelect = (index: number) => {
    if (selected !== index) {
      if (onSelectProps) {
        onSelectProps({
          selected: index
        });
      }
    }
  };

  const tabProps: any = {
    ...props,
    children: Children.toArray(children).filter((c) => c),
    onSelect: onSelect
  };

  const componentClasses = classNames(
    className.tabBar.TAB_BAR,
    props.className
  );

  if (!Children.toArray(children).length) return null;

  return (
    <div
      id={id}
      ref={containerRef}
      dir={props.dir}
      className={componentClasses}
      style={props.style}
      data-testid={genAmtId(dataTestId, 'dls-tab-bar', 'TabBar')}
    >
      <TabStripNavigation {...tabProps} />
      <TabStripContent
        index={selected}
        {...tabProps}
        style={tabProps.tabContentStyle}
      />
    </div>
  );
};

export default forwardRef<HTMLDivElement, TabBarProps>(TabBar);
