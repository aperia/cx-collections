export interface TabBarSelectEventArguments {
  selected: number;
}

export interface TabBarProps extends DLSId {
  id?: string;
  animation?: boolean;
  selected?: number;
  style?: any;
  tabContentStyle?: any;
  tabPosition?: string;
  tabIndex?: number;
  dir?: string;
  className?: string;
  onSelect?: (e: TabBarSelectEventArguments) => void;
  children?: React.ReactNode;
}

export interface TabBarTabProps {
  disabled?: boolean;

  children?: React.ReactNode | React.ReactElement;

  title?: React.ReactNode | string | React.ReactElement;
  hoverTitle?: string;

  contentClassName?: string;
}
