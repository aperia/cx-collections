import React, { MutableRefObject } from 'react';
import { fireEvent, render } from '@testing-library/react';
import '@testing-library/jest-dom';

// utils
import '../../test-utils/mocks/mockCanvas';

// components
import userEvent from '@testing-library/user-event';
import TextEditor from '.';
import { TextEditorRef } from './types';

describe('test TextEditor', () => {
  describe('test render', () => {
    it('should render TextEditor', () => {
      const wrapper = render(<TextEditor />);

      wrapper.rerender(<TextEditor mention mentionData={['1', '2', '3']} />);

      expect(
        wrapper.container.querySelector('.ProseMirror')
      ).toBeInTheDocument();
    });
  });
  describe('test action', () => {
    beforeAll(() => {
      const div = document.createElement('div');
      document.elementFromPoint = () => div;
    });
    it('should has ref', () => {
      const ref: MutableRefObject<TextEditorRef> = { current: null };
      const wrapper = render(<TextEditor ref={ref} mention />);

      let getMarkdownResult = ref.current.getMarkdown();
      let setMarkdownResult = ref.current.setMarkdown('');
      expect(getMarkdownResult).toEqual('');
      expect(setMarkdownResult).toEqual(undefined);

      wrapper.rerender(<TextEditor ref={ref} />);
      getMarkdownResult = ref.current.getMarkdown();
      setMarkdownResult = ref.current.setMarkdown('');
      expect(getMarkdownResult).toEqual('');
      expect(setMarkdownResult).toEqual(undefined);
    });
    it('should prevent toggleMention if empty data', () => {
      const handlePlainTextChange = jest.fn();

      const wrapper = render(
        <TextEditor onPlainTextChange={handlePlainTextChange} mention />
      );
      const contenteEditable = wrapper.container.querySelector('.ProseMirror')!;
      const triggerElement = document.createElement('div');

      window.getSelection = () => ({
        anchorNode: {
          querySelector: () => triggerElement
        }
      });
      userEvent.type(contenteEditable, '/');

      expect(handlePlainTextChange).not.toBeCalled();
    });
    it('should run mention with selection, anchorNode', () => {
      const handlePlainTextChange = jest.fn();

      const wrapper = render(
        <TextEditor
          onPlainTextChange={handlePlainTextChange}
          mention
          mentionData={['1', '2', '3']}
        />
      );
      const contenteEditable = wrapper.container.querySelector('.ProseMirror')!;
      const triggerElement = document.createElement('div');

      window.getSelection = () => ({
        anchorNode: {
          querySelector: () => triggerElement
        }
      });
      userEvent.type(contenteEditable, '/');

      expect(handlePlainTextChange).not.toBeCalled();
    });
    it('should run mention with selection, without anchorNode.querySelector', () => {
      const handlePlainTextChange = jest.fn();

      const wrapper = render(
        <TextEditor
          onPlainTextChange={handlePlainTextChange}
          mention
          mentionData={['1', '2', '3']}
        />
      );
      const contenteEditable = wrapper.container.querySelector('.ProseMirror')!;
      const triggerElement = document.createElement('div');

      window.getSelection = () => ({
        anchorNode: {
          querySelector: undefined
        },
        getRangeAt: () => triggerElement
      });
      userEvent.type(contenteEditable, '/');

      expect(handlePlainTextChange).not.toBeCalled();
    });
    it('should run mention with selection, without anchorNode.querySelector and close', () => {
      const handlePlainTextChange = jest.fn();

      const wrapper = render(
        <TextEditor
          onPlainTextChange={handlePlainTextChange}
          mention
          mentionData={['1', '2', '3']}
        />
      );
      const contenteEditable = wrapper.container.querySelector('.ProseMirror')!;
      const triggerElement = document.createElement('div');

      window.getSelection = () => ({
        anchorNode: {
          querySelector: undefined
        },
        getRangeAt: () => triggerElement
      });
      userEvent.type(contenteEditable, '/{enter}');

      expect(handlePlainTextChange).toBeCalled();
    });
    it('should call onFocus', () => {
      const handleFocus = jest.fn();

      const wrapper = render(
        <TextEditor
          onFocus={handleFocus}
          error={{ message: '', status: true }}
        />
      );
      const contenteEditable = wrapper.container.querySelector('.ProseMirror')!;
      fireEvent.focus(contenteEditable);

      expect(handleFocus).toBeCalled();
    });
    it('should call onBlur', () => {
      const handleBlur = jest.fn();

      const wrapper = render(
        <TextEditor onBlur={handleBlur} mention mentionData={['1', '2', '3']} />
      );
      const contenteEditable = wrapper.container.querySelector('.ProseMirror')!;
      fireEvent.blur(contenteEditable);

      expect(handleBlur).toBeCalled();
    });
  });
});
