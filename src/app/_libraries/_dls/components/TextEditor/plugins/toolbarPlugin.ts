import { MarkType } from 'prosemirror-model';
import { Plugin, PluginKey } from 'prosemirror-state';
import { TextEditorContextType } from '../contexts';

const toolbarPlugin = (
  setContextValue: (value: React.SetStateAction<TextEditorContextType>) => void
) => {
  return new Plugin({
    key: new PluginKey('dls-text-editor-tool-bar'),
    view: () => {
      return {
        update: (view, prevState) => {
          const markActive = (type: MarkType) => {
            const { from, $from, to, empty } = view.state.selection;
            if (empty) {
              return type.isInSet(view.state.storedMarks || $from.marks());
            } else return view.state.doc.rangeHasMark(from, to, type);
          };

          const checkBlock = (className: string) => {
            const { $from, to, node } = view.state.selection as any;
            if (node) {
              return node.attrs?.classes?.indexOf(className) !== -1;
            }
            return (
              to <= $from.end() &&
              $from.parent.attrs?.classes?.indexOf(className) !== -1
            );
          };

          const checkBlockType = (level?: number) => {
            const { $from, to, node } = view.state.selection as any;
            if (node) {
              return node.attrs?.level === level;
            }
            return to <= $from.end() && $from.parent.attrs?.level === level;
          };

          const checkCollapse = () => {
            const { from, to } = view.state.selection as any;
            return from === to;
          };

          const boldActived = markActive(view.state.schema.marks.strong);
          const italicActived = markActive(view.state.schema.marks.em);
          const underlineActived = markActive(
            view.state.schema.marks.underline
          );
          const textLeftActived = checkBlock('dls-text-left');
          const textCenterActived = checkBlock('dls-text-center');
          const textRightActived = checkBlock('dls-text-right');
          const headingActived = checkBlockType(4);
          const subHeadingActived = checkBlockType(5);
          const paragraphActived = !headingActived && !subHeadingActived;

          const toggleLinkActived = !checkCollapse();

          setContextValue((state) => ({
            ...state,
            boldActived: Boolean(boldActived),
            italicActived: Boolean(italicActived),
            underlineActived: Boolean(underlineActived),
            textLeftActived: Boolean(textLeftActived),
            textCenterActived: Boolean(textCenterActived),
            textRightActived: Boolean(textRightActived),
            headingActived: Boolean(headingActived),
            subHeadingActived: Boolean(subHeadingActived),
            paragraphActived: Boolean(paragraphActived),
            toggleLinkActived: Boolean(toggleLinkActived)
          }));
        }
      };
    }
  });
};

export default toolbarPlugin;
