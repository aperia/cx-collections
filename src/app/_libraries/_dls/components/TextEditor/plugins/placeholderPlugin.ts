import { Plugin, PluginKey } from 'prosemirror-state';
import { Decoration, DecorationSet } from 'prosemirror-view';

const placeholderPlugin = (placeholder: string) => {
  return new Plugin({
    key: new PluginKey('placeholder-plugin-key'),
    props: {
      decorations(state) {
        const doc = state.doc;
        const placeholderElement = document.createElement('span');
        placeholderElement.classList.add('dls-placeholder');
        placeholderElement.innerText = placeholder;

        if (
          doc.childCount === 1 &&
          doc.firstChild?.isTextblock &&
          doc.firstChild.content.size === 0
        ) {
          return DecorationSet.create(doc, [
            Decoration.widget(1, placeholderElement)
          ]);
        }
      }
    }
  });
};

export default placeholderPlugin;
