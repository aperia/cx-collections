import '@testing-library/jest-dom';

// utils
import '../../../test-utils/mocks/mockCanvas';
import { keycode } from '../../../utils';
import { schema } from '../prosemirror-markdown';

// components
import mentionPlugin from './mentionPlugin';

describe('test mentionPlugin', () => {
  it('should cover', () => {
    let plugin = mentionPlugin({
      mentionTrigger: '/',
      allowSpace: false,
      suggestionTextClass: 'dls-text-editor-mention',
      toggleMention: (opened, handleChange) => {
        handleChange?.({
          target: {
            value: 'Item 1'
          }
        });
      },
      maxPlainText: 5,
      mention: false
    });
    plugin = mentionPlugin({
      mentionTrigger: '/',
      allowSpace: false,
      suggestionTextClass: 'dls-text-editor-mention',
      toggleMention: (opened, handleChange) => {
        handleChange?.({
          target: {
            value: 'Item 1'
          }
        });
      },
      maxPlainText: 5,
      mention: true
    });

    // cover init
    plugin.spec.state?.init();

    // cover apply
    plugin.spec.state?.apply({
      selection: { from: null, to: {} }
    });
    plugin.spec.state?.apply({
      selection: {
        from: null,
        to: null,
        $from: {
          before: () => 0,
          start: () => 1,
          doc: {
            textBetween: () => ' /'
          }
        }
      }
    });
    plugin.spec.state?.apply({
      selection: {
        from: null,
        to: null,
        $from: {
          before: () => 0,
          start: () => 1,
          doc: {
            textBetween: () => 's'
          }
        }
      }
    });
    plugin.spec.state?.apply({
      selection: {
        from: null,
        to: null,
        $from: {
          before: () => 0,
          start: () => 1,
          doc: {
            textBetween: () => '/'
          }
        }
      }
    });

    // cover props.handleKeyDown
    let newHandleKeyDown = plugin.spec.props?.handleKeyDown?.bind({
      getState: () => ({
        active: false
      })
    });
    newHandleKeyDown({ state: {} });
    newHandleKeyDown = plugin.spec.props?.handleKeyDown?.bind({
      getState: () => ({
        active: true
      })
    });
    newHandleKeyDown(
      { state: {} },
      {
        keyCode: keycode.ENTER
      }
    );
    newHandleKeyDown(
      { state: {} },
      {
        keyCode: keycode.DOWN
      }
    );
    newHandleKeyDown(
      { state: {} },
      {
        keyCode: keycode.UP
      }
    );
    newHandleKeyDown(
      { state: {} },
      {
        keyCode: keycode.TAB
      }
    );

    // cover decorations
    let newDecorations = plugin.spec.props?.decorations?.bind({
      getState: () => ({
        active: false
      })
    });
    newDecorations({});
    newDecorations = plugin.spec.props?.decorations?.bind({
      getState: () => ({
        active: true,
        range: {
          from: 0,
          to: 0
        }
      })
    });
    newDecorations({
      doc: []
    });

    // cover view
    let newView = plugin.spec.view.bind({
      key: {
        getState: () => ({
          active: false
        })
      }
    });
    let viewResult = newView();
    viewResult.update({ state: {} });
    //
    newView = plugin.spec.view.bind({
      key: {
        getState: () => ({
          active: true,
          range: {
            from: 0,
            to: 0
          }
        })
      }
    });
    viewResult = newView();
    viewResult.update({
      state: {
        schema,
        tr: {
          replaceWith: () => ({
            insertText: () => ({})
          })
        },
        apply: () => ({
          doc: {
            content: {
              size: 5
            }
          }
        })
      },
      updateState: () => ({})
    });
    viewResult.update({
      state: {
        schema,
        tr: {
          replaceWith: () => ({
            insertText: () => ({})
          })
        },
        apply: () => ({
          doc: {
            content: {
              size: 6
            }
          }
        })
      },
      updateState: () => ({})
    });

    expect(true).toBeTruthy();
  });
});
