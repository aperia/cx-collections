import { Plugin, PluginKey } from 'prosemirror-state';

const maxLengthPlugin = (maxLength?: number) => {
  return new Plugin({
    key: new PluginKey('dls-text-editor-max-length-plugin'),
    appendTransaction: (transactions, oldState, newState) => {
      if (!maxLength) return;

      const max = maxLength;
      const oldLength = oldState.doc.content.size;
      const newLength = newState.doc.content.size;

      if (newLength > max && newLength > oldLength) {
        const newTr = newState.tr;
        newTr.insertText('', max + 1, newLength);

        return newTr;
      }
    }
  });
};

export default maxLengthPlugin;
