import '@testing-library/jest-dom';
import { EditorState } from 'prosemirror-state';
import { EditorView } from 'prosemirror-view';

// utils
import '../../../test-utils/mocks/mockCanvas';
import { defaultMarkdownParser, schema } from '../prosemirror-markdown';

// components
import toolbarPlugin from './toolbarPlugin';

describe('test toolbarPlugin', () => {
  beforeAll(() => {
    const div = document.createElement('div');
    document.elementFromPoint = () => div;
  });

  it('should cover', () => {
    const plugin = toolbarPlugin((callback) => {
      callback?.();
    });

    const doc = defaultMarkdownParser.parse('**bbbb**');
    const editorState = EditorState.create({
      schema,
      doc
    });

    const view = new EditorView(document.createElement('div'), {
      state: editorState
    });

    plugin.spec.view().update({
      ...view,
      state: {
        ...view.state,
        schema,
        selection: {
          ...view.state.selection,
          empty: true,
          to: 0,
          $from: {
            marks: () => ({}),
            end: () => 1,
            parent: {}
          }
        }
      }
    });
    plugin.spec.view().update({
      ...view,
      state: {
        ...view.state,
        schema,
        selection: {
          ...view.state.selection,
          empty: false,
          to: 0,
          $from: {
            marks: () => ({}),
            end: () => ({})
          },
          node: {}
        }
      }
    });
    expect(true).toBeTruthy();
  });
});
