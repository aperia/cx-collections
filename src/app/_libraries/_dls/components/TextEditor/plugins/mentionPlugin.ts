import { Plugin, PluginKey } from 'prosemirror-state';
import { Decoration, DecorationSet } from 'prosemirror-view';
import { DropdownListProps } from '../../DropdownList';
import { keycode } from '../../../utils';

function getMatch($position: any, opts: any) {
  // take current para text content upto cursor start.
  // this makes the regex simpler and parsing the matches easier.
  const parastart = $position.before();
  const text = $position.doc.textBetween(parastart, $position.pos, '\n', '\0');

  const regex = new RegExp('(^|\\s)' + opts.mentionTrigger + '$');

  // only one of the below matches will be true.
  const match = text.match(regex);

  // if match found, return match with useful information.
  if (match) {
    // adjust match.index to remove the matched extra space
    match.index = match[0].startsWith(' ') ? match.index + 1 : match.index;
    match[0] = match[0].startsWith(' ')
      ? match[0].substring(1, match[0].length)
      : match[0];

    // The absolute position of the match in the document
    const from = $position.start() + match.index;
    const to = from + match[0].length;
    const queryText = match[2];

    return {
      range: { from: from, to: to },
      queryText: queryText,
      type: 'mention'
    };
  }
}

const getNewState = function () {
  return {
    active: false,
    range: {
      from: 0,
      to: 0
    },
    type: '',
    text: '',
    suggestions: [],
    index: 0
  };
};

const mentionsPlugin = (opts: any) => {
  if (!opts.mention) {
    return new Plugin({});
  }

  return new Plugin({
    key: new PluginKey('dls-text-editor-mention-plugin'),

    // we will need state to track if suggestion dropdown is currently active or not
    state: {
      init() {
        return getNewState();
      },

      apply(tr, state) {
        // compute state.active for current transaction and return
        const newState = getNewState();
        const selection = tr.selection;
        if (selection.from !== selection.to) {
          return newState;
        }

        const $position = selection.$from;
        const match = getMatch($position, opts);

        // if match found update state
        if (match) {
          newState.active = true;
          newState.range = match.range;
          newState.type = match.type!;
          newState.text = match.queryText;
        }

        return newState;
      }
    },

    // Custom keydown/keyup & enter events when suggestion dropdown is actived
    props: {
      handleKeyDown(view, e) {
        const state = this.getState(view.state);

        // don't handle if no suggestions or not in active mode
        if (!state.active) {
          return false;
        }

        // if any of the below keys, override with custom handlers.
        const enter = e.keyCode === keycode.ENTER;
        const down = e.keyCode === keycode.DOWN;
        const up = e.keyCode === keycode.UP;

        if (enter || down || up) {
          return true;
        }

        // didn't handle. handover to prosemirror for handling.
        return false;
      },

      // to decorate the currently active @mention text in ui
      decorations(editorState) {
        const { active, range } = this.getState(editorState);
        if (!active) return null;

        return DecorationSet.create(editorState.doc, [
          Decoration.inline(range.from, range.to, {
            nodeName: 'span',
            class: opts.suggestionTextClass
          })
        ]);
      }
    },

    // To track down state mutations and add dropdown reactions
    view() {
      return {
        update: (view) => {
          const state = (this as any).key.getState(view.state);
          if (!state.active) {
            opts.toggleMention(null, null, false);
            return;
          }

          const handleChange: DropdownListProps['onChange'] = (event) => {
            const attrs = {
              name: event.target.value
            };

            const node = view.state.schema.nodes['mention'].create(attrs);
            let tr = view.state.tr.replaceWith(
              state.range.from,
              state.range.to,
              node
            );
            tr = tr.insertText(' ', state.range.to);

            const newState = view.state.apply(tr);
            if (
              opts.maxPlainText &&
              newState.doc.content.size > opts.maxPlainText
            ) {
              return opts.toggleMention(false);
            }

            view.updateState(newState);
          };

          opts.toggleMention(true, handleChange);
        }
      };
    }
  });
};

export default mentionsPlugin;
