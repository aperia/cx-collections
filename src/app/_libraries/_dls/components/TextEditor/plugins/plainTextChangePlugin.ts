import { Plugin, PluginKey } from 'prosemirror-state';
import { TextEditorProps } from '../types';

const plainTextChangePlugin = (
  onPlainTextChange: TextEditorProps['onPlainTextChange'],
  proseMirrorContainer: HTMLDivElement
) => {
  return new Plugin({
    key: new PluginKey('dls-text-editor-plain-text=change'),
    appendTransaction: (transactions, oldState, newState) => {
      const proseMirror = proseMirrorContainer.querySelector('.ProseMirror')!;
      onPlainTextChange?.(proseMirror.textContent || '');
    }
  });
};

export default plainTextChangePlugin;
