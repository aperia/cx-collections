import '@testing-library/jest-dom';

// utils
import '../../../test-utils/mocks/mockCanvas';

// components
import maxLengthPlugin from './maxLengthPlugin';

describe('test maxLengthPlugin', () => {
  it('should return if maxLength is zero', () => {
    const plugin = maxLengthPlugin(0);
    plugin.spec.appendTransaction();
    expect(true).toBeTruthy();
  });

  it('should run with maxLength', () => {
    const plugin = maxLengthPlugin(5);
    plugin.spec.appendTransaction(
      null,
      {
        doc: {
          content: {
            size: 5
          }
        }
      },
      {
        doc: {
          content: {
            size: 6
          }
        },
        tr: {
          insertText: () => ({})
        }
      }
    );
    plugin.spec.appendTransaction(
      null,
      {
        doc: {
          content: {
            size: 5
          }
        }
      },
      {
        doc: {
          content: {
            size: 4
          }
        },
        tr: {
          insertText: () => ({})
        }
      }
    );
    expect(true).toBeTruthy();
  });
});
