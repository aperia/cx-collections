import '@testing-library/jest-dom';

// utils
import '../../../test-utils/mocks/mockCanvas';

// components
import plainTextChangePlugin from './plainTextChangePlugin';

describe('test plainTextChangePlugin', () => {
  it('should cover', () => {
    const handlePlainTextChange = jest.fn();
    const textEditor = document.createElement('div');
    const contentEditable = document.createElement('div');
    contentEditable.classList.add('ProseMirror');
    contentEditable.textContent = 'PP';
    textEditor.appendChild(contentEditable);

    const plugin = plainTextChangePlugin(handlePlainTextChange, textEditor);
    plugin.spec.appendTransaction();
    expect(handlePlainTextChange).toBeCalledWith('PP');

    contentEditable.textContent = '';
    plugin.spec.appendTransaction();
    expect(handlePlainTextChange).toBeCalledWith('');
  });
});
