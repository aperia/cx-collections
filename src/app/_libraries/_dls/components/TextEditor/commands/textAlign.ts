import { EditorView } from 'prosemirror-view';
import classnames from 'classnames';

const textAlign = (
  editorView: EditorView,
  alignment: 'left' | 'center' | 'right'
) => {
  const { state, dispatch } = editorView;

  const { schema, selection } = state;
  let tr = state.tr.setSelection(selection);

  const { from, to } = selection;
  const {nodes} = schema;

  const tasks: any[] = [];
  const allowedNodeTypes = new Set([nodes['paragraph'], nodes['heading']])
  
  tr.doc.nodesBetween(from, to, (node, pos) => {
    const nodeType = node.type;

    if (!allowedNodeTypes.has(nodeType)) return;

    // always paragraph or heading
    const classes = node.attrs.classes || '';
    let nextClasses = '';

    const alignmentClassName = `dls-text-${alignment}`;
    const hasAlignment = classes.indexOf(alignmentClassName) !== -1;

    if (hasAlignment) {
      nextClasses = classes.split(alignmentClassName).join('');
    } else {
      nextClasses = classnames(
        classes
          .split(`dls-text-left`)
          .join('')
          .split('dls-text-center')
          .join('')
          .split('dls-text-right')
          .join('')
          .trim(),
        alignmentClassName
      );
    }

    tasks.push({
      node,
      pos,
      nodeType,
      nextClasses
    });

    return true;
  });

  tasks?.forEach((task) => {
    const { node, pos, nodeType, nextClasses } = task;
    let { attrs } = node;
    attrs = {
      ...attrs,
      classes: nextClasses
    };
    tr = tr.setNodeMarkup(pos, nodeType, attrs, node.marks);
  });

  tr.docChanged && dispatch(tr);
};

export default textAlign;
