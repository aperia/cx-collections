import '@testing-library/jest-dom';

// utils
import '../../../test-utils/mocks/mockCanvas';

// components
import { inoutdent } from '.';

import { EditorState } from 'prosemirror-state';
import { EditorView } from 'prosemirror-view';
import { defaultMarkdownParser, schema } from '../prosemirror-markdown';

describe('test inoutdent command', () => {
  it('test inoutdent', () => {
    const doc = defaultMarkdownParser.parse('##### PP');
    const doc2 = defaultMarkdownParser.parse('* Bullet List');

    const state = EditorState.create({
      schema,
      doc
    });
    const state2 = EditorState.create({
      schema,
      doc: doc2
    });

    const editorAnchorHtml = document.createElement('div');

    const view = new EditorView(editorAnchorHtml, { state });
    inoutdent(view, 'in');
    inoutdent(view, 'in');
    inoutdent(view, 'in');
    inoutdent(view, 'in');
    inoutdent(view, 'out');
    inoutdent(view, 'out');
    inoutdent(view, 'out');

    const view2 = new EditorView(editorAnchorHtml, { state: state2 });
    inoutdent(view2, 'in');

    expect(true).toBeTruthy();
  });
  it('100', () => {
    const editorAnchorHtml = document.createElement('div');
    const emptyView = new EditorView(editorAnchorHtml, {
      state: EditorState.create({
        schema
      })
    });
    emptyView.state = {
      ...emptyView.state,
      tr: {
        setSelection: () => ({ selection: null })
      }
    };
    inoutdent(emptyView, 'in');

    expect(true).toBeTruthy();
  });
});
