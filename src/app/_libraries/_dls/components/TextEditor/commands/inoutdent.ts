import { EditorView } from 'prosemirror-view';
import classnames from 'classnames';

const inoutdent = (editorView: EditorView, direction: 'in' | 'out') => {
  const { state, dispatch } = editorView;
  const { schema, selection } = state;
  let tr = state.tr.setSelection(selection);

  if (!tr.selection || !tr.doc) return tr;

  const { from, to } = selection;
  const { nodes } = schema;

  const tasks: any[] = [];
  const allowedNodeTypes = new Set([nodes['paragraph'], nodes['heading']]);

  tr.doc.nodesBetween(from, to, (node, pos) => {
    const nodeType = node.type;

    if (allowedNodeTypes.has(nodeType)) {
      const classes = node.attrs.classes || '';
      let nextClasses = '';

      const classNames = ['dls-indent-1', 'dls-indent-2', 'dls-indent-3'];
      const hasInoutdent = classNames.find(className => {
        return classes.indexOf(className) !== -1;
      });

      const index = classNames.indexOf(hasInoutdent!);
      const nextIndex = index + (direction === 'in' ? 1 : -1);
      const nextInoutdentClassName = classNames[nextIndex];

      if (hasInoutdent === 'dls-indent-3' && !nextInoutdentClassName) {
        return;
      }

      nextClasses = classes.replace(hasInoutdent!, '').trim();
      nextClasses = classnames(nextClasses, nextInoutdentClassName);

      tasks.push({
        node,
        pos,
        nodeType,
        nextClasses
      });
    }

    return true;
  });

  if (!tasks.length) return tr;

  tasks.forEach(task => {
    const { node, pos, nodeType, nextClasses } = task;
    let { attrs } = node;
    attrs = {
      ...attrs,
      classes: nextClasses
    };
    tr = tr.setNodeMarkup(pos, nodeType, attrs, node.marks);
  });

  tr.docChanged && dispatch(tr);
};

export default inoutdent;
