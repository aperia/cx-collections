import '@testing-library/jest-dom';

// utils
import '../../../test-utils/mocks/mockCanvas';

// components
import { textAlign } from '.';

import { EditorState } from 'prosemirror-state';
import { EditorView } from 'prosemirror-view';
import { defaultMarkdownParser, schema } from '../prosemirror-markdown';

describe('test textAlign command', () => {
  it('should run', () => {
    const doc = defaultMarkdownParser.parse('PP');
    const editorAnchorHtml = document.createElement('div');
    const state = EditorState.create({
      schema,
      doc
    });
    const view = new EditorView(editorAnchorHtml, { state });

    textAlign(view, 'left');
    textAlign(view, 'left');
    expect(true).toBeTruthy();
  });
  it('should prevent if Node is invalid', () => {
    textAlign(
      {
        state: {
          schema,

          selection: {
            from: 1,

            to: 5
          },

          tr: {
            setSelection: () => ({
              doc: {
                nodesBetween: (a, b, c) => {
                  c({
                    type: schema.nodes.text
                  });
                }
              }
            })
          }
        },

        dispatch: () => ({})
      },

      'left'
    );

    expect(true).toBeTruthy();
  });
  it('should run if Node is valid', () => {
    textAlign(
      {
        state: {
          schema,
          selection: {
            from: 1,
            to: 5
          },
          tr: {
            setSelection: () => ({
              doc: {
                nodesBetween: (a, b, c) => {
                  c({
                    type: schema.nodes['paragraph'],
                    attrs: {
                      classes: undefined
                    }
                  });
                }
              },
              setNodeMarkup: () => ({
                docChanged: true
              })
            })
          }
        },
        dispatch: () => ({})
      },
      'left'
    );
    textAlign(
      {
        state: {
          schema,
          selection: {
            from: 1,
            to: 5
          },
          tr: {
            setSelection: () => ({
              doc: {
                nodesBetween: (a, b, c) => {
                  c({
                    type: schema.nodes['paragraph'],

                    attrs: {
                      classes: 'dls-text-left'
                    }
                  });
                }
              },
              setNodeMarkup: () => ({
                docChanged: true
              })
            })
          }
        },
        dispatch: () => ({})
      },
      'left'
    );
    expect(true).toBeTruthy();
  });
});
