import React, { useEffect, useState } from 'react';
import DropdownList, {
  DropdownListProps,
  DropdownListRef
} from '../../DropdownList';

export interface MentionDropdownListProps extends DropdownListProps {}

const MentionDropdownList: React.RefForwardingComponent<
  DropdownListRef,
  DropdownListProps
> = ({ opened, onChange, children, ...props }, ref) => {
  const [value, setValue] = useState(null);

  const handleChange: DropdownListProps['onChange'] = (event) => {
    setValue(event.target.value);
    onChange?.(event);
  };

  useEffect(() => {
    opened && setValue(null);
  }, [opened]);

  return (
    <DropdownList
      ref={ref}
      value={value}
      onChange={handleChange}
      opened={opened}
      variant="no-border"
      allowCallOnChangeByUpDown={false}
      tabIndex={-1}
    >
      {children}
    </DropdownList>
  );
};

export default React.forwardRef<DropdownListRef, DropdownListProps>(
  MentionDropdownList
);
