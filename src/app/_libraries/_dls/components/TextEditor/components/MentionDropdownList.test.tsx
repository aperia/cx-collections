import React from 'react';
import '@testing-library/jest-dom';

// utils
import '../../../test-utils/mocks/mockCanvas';
import {
  createMockMutationObserver,
  clearMockMutationObserver
} from '../../../test-utils/mocks/mockMutationObserver';
import { keycode } from '../../../utils';

// components
import MentionDropdownList from './MentionDropdownList';
import DropdownList from '../../DropdownList';
import { fireEvent, render } from '@testing-library/react';

describe('test MentionDropdownList', () => {
  createMockMutationObserver([]);
  afterAll(clearMockMutationObserver);

  const MockComponent: React.FC<any> = (props) => {
    return (
      <MentionDropdownList {...props}>
        <DropdownList.Item value="Item 1" label="Item 1" />
        <DropdownList.Item value="Item 2" label="Item 2" />
      </MentionDropdownList>
    );
  };

  it('should run with opened true', () => {
    jest.useFakeTimers();
    const wrapper = render(<MockComponent opened />);
    jest.runAllTimers();

    expect(wrapper.baseElement.querySelector('.dls-popup')).toBeInTheDocument();
  });

  it('should run with onChange', () => {
    const handleOnChange = jest.fn();

    jest.useFakeTimers();
    const wrapper = render(<MockComponent opened onChange={handleOnChange} />);
    jest.runAllTimers();

    fireEvent.keyDown(wrapper.container, {
      keyCode: keycode.DOWN
    });
    fireEvent.keyDown(wrapper.container, {
      keyCode: keycode.ENTER
    });

    expect(handleOnChange).toBeCalledWith({
      bubbles: false,
      cancelable: false,
      changeBy: 'enter',
      isTrusted: true,
      target: { id: undefined, name: undefined, value: 'Item 1' },
      value: 'Item 1'
    });
  });
});
