import classnames from 'classnames';
import { wrapInList } from 'prosemirror-schema-list';
import React from 'react';
import Button, { ButtonProps } from '../../Button';
import Icon from '../../Icon';
import { useTextEditorContext } from '../contexts';

export interface OrderedListProps {}

const OrderedList: React.FC<OrderedListProps> = (props) => {
  const {
    editorView,
    headingActived,
    subHeadingActived,
    orderedListActived,
    bulletListActived
  } = useTextEditorContext();

  // toggle ordered list
  const handleOrderedList: ButtonProps['onMouseDown'] = (event) => {
    event.preventDefault();

    const command = wrapInList(editorView.state.schema.nodes.ordered_list, {});
    command(editorView.state, editorView.dispatch);
  };

  return (
    <Button
      onMouseDown={handleOrderedList}
      className={classnames({
        ['dls-text-editor-tool']: true,
        active: orderedListActived
      })}
      disabled={bulletListActived || headingActived || subHeadingActived}
      variant="icon-secondary"
      title="OrderedList"
    >
      <Icon name="list-number" />
    </Button>
  );
};

export default OrderedList;
