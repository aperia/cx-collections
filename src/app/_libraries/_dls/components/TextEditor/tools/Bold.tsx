import classnames from 'classnames';
import { toggleMark } from 'prosemirror-commands';
import React from 'react';
import Button, { ButtonProps } from '../../Button';
import Icon from '../../Icon';
import { useTextEditorContext } from '../contexts';

export interface BoldProps {}

const Bold: React.FC<BoldProps> = (props) => {
  const { editorView, boldActived } = useTextEditorContext();

  // toggle bold
  const handleToggleBold: ButtonProps['onMouseDown'] = (event) => {
    event.preventDefault();

    const { schema } = editorView.state;
    const command = toggleMark(schema.marks.strong);
    command(editorView.state, editorView.dispatch);
  };

  return (
    <Button
      onMouseDown={handleToggleBold}
      className={classnames({
        ['dls-text-editor-tool']: true,
        active: boldActived
      })}
      variant="icon-secondary"
      title="Bold"
    >
      <Icon name="bold" />
    </Button>
  );
};

export default Bold;
