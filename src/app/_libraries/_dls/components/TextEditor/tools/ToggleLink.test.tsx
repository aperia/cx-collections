import React from 'react';
import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

// utils
import '../../../test-utils/mocks/mockCanvas';
import { TextEditorContext } from '../contexts';
import ToggleLink from './ToggleLink';

describe('test ToggleLink', () => {
  it('should run with opened false', () => {
    const wrapper = render(
      <TextEditorContext.Provider
        value={{
          editorView: {
            state: {
              doc: {
                rangeHasMark: () => ({})
              },
              selection: {
                from: 0,
                $from: {
                  marks: () => ({})
                },
                ranges: []
              },
              schema: {
                marks: {
                  link: {}
                }
              }
            }
          },
          toggleLinkActived: true
        }}
      >
        <ToggleLink />
      </TextEditorContext.Provider>
    );

    const button = wrapper.container.querySelector('button')!;
    userEvent.click(button);
    expect(true).toBeTruthy();
  });
  it('should run with opened true', () => {
    const wrapper = render(
      <TextEditorContext.Provider
        value={{
          editorView: {
            focus: () => ({}),
            state: {
              doc: {
                rangeHasMark: () => null
              },
              selection: {
                empty: true,
                from: 0,
                $from: {
                  marks: () => ({})
                },
                ranges: []
              },
              schema: {
                marks: {
                  link: {
                    isInSet: () => false
                  }
                }
              }
            }
          },
          toggleLinkActived: true
        }}
      >
        <ToggleLink />
      </TextEditorContext.Provider>
    );

    const button = wrapper.container.querySelector('button')!;
    userEvent.click(button);

    const webAddress = wrapper
      .queryByText('Web Address')
      ?.closest('.dls-input-container')
      ?.querySelector('input');
    const title = wrapper
      .queryByText('Title')
      ?.closest('.dls-input-container')
      ?.querySelector('input');
    const insert = wrapper.queryByText('Insert')?.closest('button');
    const cancel = wrapper.queryByText('Cancel')?.closest('button');

    userEvent.type(webAddress!, 'PP');
    userEvent.type(title!, 'PP');
    userEvent.click(insert!);

    userEvent.click(button);
    userEvent.click(cancel!);

    expect(true).toBeTruthy();
  });
});
