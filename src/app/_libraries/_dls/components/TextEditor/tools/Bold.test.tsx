import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import TextEditor from '..';

// utils
import '../../../test-utils/mocks/mockCanvas';

describe('test Bold', () => {
  it('should call onChange', () => {
    const wrapper = render(<TextEditor />);

    const bold = wrapper.container.querySelector('[title="Bold"]')!;
    userEvent.click(bold);
    expect(true).toBeTruthy();
  });
});
