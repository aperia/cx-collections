import classnames from 'classnames';
import React from 'react';
import Button, { ButtonProps } from '../../Button';
import Icon from '../../Icon';
import { textAlign } from '../commands';
import { useTextEditorContext } from '../contexts';

export interface TextLeftProps {}

const TextLeft: React.FC<TextLeftProps> = (props) => {
  const { editorView, textLeftActived } = useTextEditorContext();

  // toggle bold
  const handleToggleBold: ButtonProps['onMouseDown'] = (event) => {
    event.preventDefault();

    textAlign(editorView, 'left');
  };

  return (
    <Button
      onMouseDown={handleToggleBold}
      className={classnames({
        ['dls-text-editor-tool']: true,
        active: textLeftActived
      })}
      variant="icon-secondary"
      title="TextLeft"
    >
      <Icon name="align-left" />
    </Button>
  );
};

export default TextLeft;
