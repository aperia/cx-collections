import '@testing-library/jest-dom';
import { fireEvent, render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import TextEditor from '..';

// utils
import '../../../test-utils/mocks/mockCanvas';
import { keycode } from '../../../utils';

describe('test BlockTypes', () => {
  it('should call onChange', () => {
    const wrapper = render(<TextEditor />);

    const blockTypes = wrapper.container.querySelector(
      '.dls-dropdown-list .text-field-container'
    )!;
    const contentEditable = wrapper.container.querySelector('.ProseMirror')!;

    jest.useFakeTimers();
    userEvent.click(blockTypes);
    jest.runAllTimers();
    jest.useFakeTimers();
    fireEvent.keyDown(contentEditable, {
      keyCode: keycode.UP
    });
    fireEvent.keyDown(contentEditable, {
      keyCode: keycode.ENTER
    });
    jest.runAllTimers();

    jest.useFakeTimers();
    userEvent.click(blockTypes);
    jest.runAllTimers();
    jest.useFakeTimers();
    fireEvent.keyDown(contentEditable, {
      keyCode: keycode.UP
    });
    fireEvent.keyDown(contentEditable, {
      keyCode: keycode.UP
    });
    fireEvent.keyDown(contentEditable, {
      keyCode: keycode.ENTER
    });
    jest.runAllTimers();

    expect(true).toBeTruthy();
  });
});
