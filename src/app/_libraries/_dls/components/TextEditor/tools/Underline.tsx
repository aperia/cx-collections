import classnames from 'classnames';
import { toggleMark } from 'prosemirror-commands';
import React from 'react';
import Button, { ButtonProps } from '../../Button';
import Icon from '../../Icon';
import { useTextEditorContext } from '../contexts';

export interface UnderlineProps {}

const Underline: React.FC<UnderlineProps> = (props) => {
  const { editorView, underlineActived } = useTextEditorContext();

  // toggle underline
  const handleToggleUnderline: ButtonProps['onMouseDown'] = (event) => {
    event.preventDefault();

    const { schema } = editorView.state;
    const command = toggleMark(schema.marks.underline);
    command(editorView.state, editorView.dispatch);
  };

  return (
    <Button
      onMouseDown={handleToggleUnderline}
      className={classnames({
        ['dls-text-editor-tool']: true,
        active: underlineActived
      })}
      variant="icon-secondary"
      title="Underline"
    >
      <Icon name="underline" />
    </Button>
  );
};

export default Underline;
