import classnames from 'classnames';
import { wrapInList } from 'prosemirror-schema-list';
import React from 'react';
import Button, { ButtonProps } from '../../Button';
import Icon from '../../Icon';
import { useTextEditorContext } from '../contexts';

export interface BulletListProps {}

const BulletList: React.FC<BulletListProps> = (props) => {
  const {
    editorView,
    headingActived,
    subHeadingActived,
    orderedListActived,
    bulletListActived
  } = useTextEditorContext();

  // toggle bullet list
  const handleBulletList: ButtonProps['onMouseDown'] = (event) => {
    event.preventDefault();

    const command = wrapInList(editorView.state.schema.nodes.bullet_list, {});
    command(editorView.state, editorView.dispatch);
  };

  return (
    <Button
      onMouseDown={handleBulletList}
      className={classnames({
        ['dls-text-editor-tool']: true,
        active: bulletListActived
      })}
      disabled={orderedListActived || headingActived || subHeadingActived}
      variant="icon-secondary"
      title="BulletList"
    >
      <Icon name="list-bullet" />
    </Button>
  );
};

export default BulletList;
