import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import TextEditor from '..';

// utils
import '../../../test-utils/mocks/mockCanvas';

describe('test TextRight', () => {
  it('should call mousedown', () => {
    const wrapper = render(<TextEditor />);

    const indent = wrapper.container.querySelector('[title="TextRight"]')!;
    userEvent.click(indent);
    expect(true).toBeTruthy();
  });
});
