import classnames from 'classnames';
import React from 'react';
import Button, { ButtonProps } from '../../Button';
import Icon from '../../Icon';
import { inoutdent } from '../commands';
import { useTextEditorContext } from '../contexts';

export interface OutdentProps {}

const Outdent: React.FC<OutdentProps> = (props) => {
  const { editorView } = useTextEditorContext();

  // handle outdent
  const handleOutdent: ButtonProps['onMouseDown'] = (event) => {
    event.preventDefault();
    inoutdent(editorView, 'out');
  };

  return (
    <Button
      onMouseDown={handleOutdent}
      className={classnames({
        ['dls-text-editor-tool']: true
      })}
      variant="icon-secondary"
      title="Outdent"
    >
      <Icon name="outdent" />
    </Button>
  );
};

export default Outdent;
