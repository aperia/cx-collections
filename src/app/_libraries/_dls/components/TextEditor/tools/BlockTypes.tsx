import { setBlockType } from 'prosemirror-commands';
import React from 'react';

import DropdownList, { DropdownListProps } from '../../DropdownList';
import { useTextEditorContext } from '../contexts';

export interface BlockTypesProps {}

const data = [
  {
    value: 'h4',
    description: 'Heading',
    nodeTypeKy: 'heading',
    attrs: {
      level: 4
    }
  },
  {
    value: 'h5',
    description: 'Subheading',
    nodeTypeKy: 'heading',
    attrs: {
      level: 5
    }
  },
  {
    value: 'p',
    description: 'Paragraph',
    nodeTypeKy: 'paragraph',
    attrs: {}
  }
];

const BlockTypes: React.FC<BlockTypesProps> = (props) => {
  const { editorView, headingActived, subHeadingActived } =
    useTextEditorContext();

  const handleOnChange: DropdownListProps['onChange'] = (event) => {
    const value = event.target.value;

    const { nodes } = editorView.state.schema;
    const { nodeTypeKy, attrs } = value;
    const command = setBlockType(nodes[nodeTypeKy], attrs);
    command(editorView.state, editorView.dispatch);
    process.nextTick(() => editorView?.focus?.());
  };

  const findValueActived = () => {
    if (headingActived) return data[0];
    if (subHeadingActived) return data[1];
    return data[2];
  };

  return (
    <DropdownList
      value={findValueActived()}
      onChange={handleOnChange}
      textField="description"
      size="small"
      popupBaseProps={{
        fluid: false,
        popupBaseClassName: 'dls-text-editor-blocktypes'
      }}
    >
      {data.map((item) => {
        return (
          <DropdownList.Item
            key={item.value}
            value={item}
            label={item.description}
            variant="custom"
          >
            <div className={item.value}>{item.description}</div>
          </DropdownList.Item>
        );
      })}
    </DropdownList>
  );
};

export default BlockTypes;
