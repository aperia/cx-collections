import classnames from 'classnames';
import React from 'react';
import Button, { ButtonProps } from '../../Button';
import Icon from '../../Icon';
import { inoutdent } from '../commands';
import { useTextEditorContext } from '../contexts';

export interface IndentProps {}

const Indent: React.FC<IndentProps> = (props) => {
  const { editorView } = useTextEditorContext();

  // handle indent
  const handleIndent: ButtonProps['onMouseDown'] = (event) => {
    event.preventDefault();
    inoutdent(editorView, 'in');
  };

  return (
    <Button
      onMouseDown={handleIndent}
      className={classnames({
        ['dls-text-editor-tool']: true
      })}
      variant="icon-secondary"
      title="Indent"
    >
      <Icon name="indent" />
    </Button>
  );
};

export default Indent;
