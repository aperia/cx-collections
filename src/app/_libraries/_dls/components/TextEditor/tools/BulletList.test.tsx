import '@testing-library/jest-dom';
import { render } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import React from 'react';
import TextEditor from '..';

// utils
import '../../../test-utils/mocks/mockCanvas';

describe('test BulletList', () => {
  it('should call onChange', () => {
    const wrapper = render(<TextEditor />);

    const bullet = wrapper.container.querySelector('[title="BulletList"]')!;
    userEvent.click(bullet);
    expect(true).toBeTruthy();
  });
});
