import classnames from 'classnames';
import React from 'react';
import Button, { ButtonProps } from '../../Button';
import Icon from '../../Icon';
import { textAlign } from '../commands';
import { useTextEditorContext } from '../contexts';

export interface TextRightProps {}

const TextRight: React.FC<TextRightProps> = (props) => {
  const { editorView, textRightActived } = useTextEditorContext();

  // toggle bold
  const handleToggleBold: ButtonProps['onMouseDown'] = (event) => {
    event.preventDefault();

    textAlign(editorView, 'right');
  };

  return (
    <Button
      onMouseDown={handleToggleBold}
      className={classnames({
        ['dls-text-editor-tool']: true,
        active: textRightActived
      })}
      variant="icon-secondary"
      title="TextRight"
    >
      <Icon name="align-right" />
    </Button>
  );
};

export default TextRight;
