import classnames from 'classnames';
import React from 'react';
import Button, { ButtonProps } from '../../Button';
import Icon from '../../Icon';
import { textAlign } from '../commands';
import { useTextEditorContext } from '../contexts';

export interface TextCenterProps {}

const TextCenter: React.FC<TextCenterProps> = (props) => {
  const { editorView, textCenterActived } = useTextEditorContext();

  // toggle bold
  const handleToggleBold: ButtonProps['onMouseDown'] = (event) => {
    event.preventDefault();

    textAlign(editorView, 'center');
  };

  return (
    <Button
      onMouseDown={handleToggleBold}
      className={classnames({
        ['dls-text-editor-tool']: true,
        active: textCenterActived
      })}
      variant="icon-secondary"
      title="TextCenter"
    >
      <Icon name="align-center" />
    </Button>
  );
};

export default TextCenter;
