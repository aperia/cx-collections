import classnames from 'classnames';
import { toggleMark } from 'prosemirror-commands';
import React from 'react';
import Button, { ButtonProps } from '../../Button';
import Icon from '../../Icon';
import { useTextEditorContext } from '../contexts';

export interface ItalicProps {}

const Italic: React.FC<ItalicProps> = (props) => {
  const { editorView, italicActived } = useTextEditorContext();

  // toggle italic
  const handleToggleItalic: ButtonProps['onMouseDown'] = (event) => {
    event.preventDefault();

    const { schema } = editorView.state;
    const command = toggleMark(schema.marks.em);
    command(editorView.state, editorView.dispatch);
  };

  return (
    <Button
      onMouseDown={handleToggleItalic}
      className={classnames({
        ['dls-text-editor-tool']: true,
        active: italicActived
      })}
      variant="icon-secondary"
      title="Italic"
    >
      <Icon name="italic" />
    </Button>
  );
};

export default Italic;
