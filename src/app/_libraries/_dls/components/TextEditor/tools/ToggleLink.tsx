import { toggleMark } from 'prosemirror-commands';
import { MarkType } from 'prosemirror-model';
import { EditorState } from 'prosemirror-state';
import React, { useEffect, useState } from 'react';
import Button from '../../Button';
import Icon from '../../Icon';
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from '../../Modal';
import TextBox from '../../TextBox';
import classnames from 'classnames';
import { useTextEditorContext } from '../contexts';

export interface ToggleLinkProps {}

const ToggleLink: React.FC<ToggleLinkProps> = (props) => {
  const { editorView, toggleLinkActived } = useTextEditorContext();

  // states
  const [webAddress, setWebAddress] = useState('');
  const [title, setTitle] = useState('');
  const [opened, setOpened] = useState(false);

  const handleOpenModal = () => setOpened(true);
  const handleCloseModal = () => setOpened(false);
  const handleCancel = handleCloseModal;

  // handle change webAddress
  const handleOnChangeWebAddress: React.ChangeEventHandler<HTMLInputElement> = (
    event
  ) => {
    setWebAddress(event.target.value);
  };

  // handle change title
  const handleOnChangeTitle: React.ChangeEventHandler<HTMLInputElement> = (
    event
  ) => {
    setTitle(event.target.value);
  };

  // handle insert link
  const handleOk = () => {
    const command = toggleMark(editorView.state.schema.marks.link, {
      href: webAddress,
      title: title
    });

    command(editorView.state, editorView.dispatch);
    setOpened(false);
    editorView.focus();
  };

  useEffect(() => {
    if (opened) return;

    setWebAddress('');
    setTitle('');
  }, [opened]);

  return (
    <>
      <Button
        onMouseDown={(event) => {
          event.preventDefault();

          function markActive(state: EditorState, type: MarkType) {
            const { from, $from, to, empty } = state.selection;
            if (empty) {
              return type.isInSet(state.storedMarks || $from.marks());
            } else return state.doc.rangeHasMark(from, to, type);
          }

          const isActive = markActive(
            editorView.state,
            editorView.state.schema.marks.link
          );
          if (isActive) {
            const command = toggleMark(editorView.state.schema.marks.link, {
              href: '',
              title: ''
            });
            command(editorView.state, editorView.dispatch);
            return;
          }

          handleOpenModal();
        }}
        disabled={!toggleLinkActived}
        className={classnames('dls-text-editor-tool', {})}
        variant="icon-secondary"
      >
        <Icon name="link" />
      </Button>
      <Modal xs show={opened} animationDuration={500} placement="center">
        <ModalHeader border closeButton onHide={handleCloseModal}>
          <ModalTitle>Insert hyperlink</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <TextBox
            label="Web Address"
            value={webAddress}
            onChange={handleOnChangeWebAddress}
            className="mb-16"
          />
          <TextBox label="Title" value={title} onChange={handleOnChangeTitle} />
        </ModalBody>
        <ModalFooter
          cancelButtonText="Cancel"
          okButtonText="Insert"
          onCancel={handleCancel}
          onOk={handleOk}
        />
      </Modal>
    </>
  );
};

export default ToggleLink;
