const replaceMentionWith = (markdownOrigin: string) => {
  const regex = new RegExp(
    `@\\[[a-zA-Z0-9]+(([',. -][a-zA-Z0-9 ])?[a-zA-Z0-9]*)*\\]\\(\\w+\\)`,
    'gm'
  );
  let output = markdownOrigin;
  const matches = markdownOrigin.match(regex);

  if (matches?.length) {
    matches.forEach((match) => {
      const removeFrom = match.lastIndexOf('(');
      const withRemoveLink = match.slice(0, removeFrom);
      const replacer = withRemoveLink.replace('@[', '{').replace(']', '}');

      output = output.replace(match, replacer);
    });
  }

  return output;
};

export default replaceMentionWith;
