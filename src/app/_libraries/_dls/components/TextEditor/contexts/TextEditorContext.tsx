import { EditorView } from 'prosemirror-view';
import React, { useContext } from 'react';

interface TextEditorContextType {
  editorView: EditorView<any>;
  readOnly?: boolean;
  boldActived?: boolean;
  italicActived?: boolean;
  underlineActived?: boolean;
  textLeftActived?: boolean;
  textCenterActived?: boolean;
  textRightActived?: boolean;
  orderedListActived?: boolean;
  bulletListActived?: boolean;
  headingActived?: boolean;
  subHeadingActived?: boolean;
  paragraphActived?: boolean;
  toggleLinkActived?: boolean;
}

const TextEditorContext = React.createContext<any>(null);

const useTextEditorContext = () => {
  const contextValue = useContext(TextEditorContext);
  return contextValue as TextEditorContextType;
};

export { useTextEditorContext };
export type { TextEditorContextType };
export default TextEditorContext;
