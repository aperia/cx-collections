import { DOMSerializer } from 'prosemirror-model';
import { EditorState } from 'prosemirror-state';
import { EditorView } from 'prosemirror-view';
import { defaultMarkdownParser, schema } from './prosemirror-markdown';

const markdownToHTML = (markdown?: string) => {
  if (!markdown) {
    return {
      element: null,
      html: ''
    };
  }

  const doc = defaultMarkdownParser.parse(markdown);
  const state = EditorState.create({
    schema,
    doc
  });

  const editorAnchorHtml = document.createElement('div');
  const view = new EditorView(editorAnchorHtml, { state });

  const fragment = DOMSerializer.fromSchema(schema).serializeFragment(
    view.state.doc.content
  );

  const outputElement = document.createElement('div');
  outputElement.appendChild(fragment);

  return {
    element: outputElement,
    html: outputElement.innerHTML
  };
};

export default markdownToHTML;
