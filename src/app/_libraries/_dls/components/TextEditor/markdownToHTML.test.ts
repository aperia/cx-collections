import '@testing-library/jest-dom';

// utils
import '../../test-utils/mocks/mockCanvas';

// components

import markdownToHTML from './markdownToHTML';

describe('test markdownToHTML', () => {
  it('should return empty result if markdown not found', () => {
    const result = markdownToHTML('');
    expect(result).toEqual({ element: null, html: '' });
  });
  it('otherwise, should return result', () => {
    const result = markdownToHTML('##### PP');
    expect(result.html).toEqual('<h5>PP</h5>');
  });
});
