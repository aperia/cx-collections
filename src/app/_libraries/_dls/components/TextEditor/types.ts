import { TooltipProps } from '../Tooltip';

export interface TextEditorRef {
  getMarkdown: () => string;
  setMarkdown: (markdown: string) => void;
}

export interface TextEditorProps extends DLSId {
  placeholder?: string;
  onFocus?: React.FocusEventHandler<HTMLDivElement>;
  onBlur?: React.FocusEventHandler<HTMLDivElement>;
  error?: {
    message?: string;
    status?: boolean;
  };
  errorTooltipProps?: TooltipProps;

  maxPlainText?: number;
  onPlainTextChange?: (nextPlainText: string) => void;
  mention?: boolean;
  mentionData?: string[];

  name?: string;
  className?: string;
  readOnly?: boolean;
}
