import { Schema } from 'prosemirror-model';
import ParagraphNode from './nodes/ParagraphNode';
import HeadingNodeSpec from './nodes/HeadingNode';
import MentionNode from './nodes/MentionNode';

export const schema = new Schema({
  nodes: {
    // Doc
    doc: {
      content: 'block+'
    },

    // Paragraph
    // paragraph: {
    //   content: 'inline*',
    //   group: 'block',
    //   parseDOM: [{ tag: 'p' }],
    //   toDOM() {
    //     return ['p', 0];
    //   }
    // },
    paragraph: ParagraphNode,

    // BlockQuote
    blockquote: {
      content: 'block+',
      group: 'block',
      parseDOM: [{ tag: 'blockquote' }],
      toDOM() {
        return ['blockquote', 0];
      }
    },

    // Horizontal Rule
    horizontal_rule: {
      group: 'block',
      parseDOM: [{ tag: 'hr' }],
      toDOM() {
        return ['div', ['hr']];
      }
    },

    // Heading
    // heading: {
    //   attrs: { level: { default: 1 } },
    //   content: '(text | image)*',
    //   group: 'block',
    //   defining: true,
    //   parseDOM: [
    //     { tag: 'h1', attrs: { level: 1 } },
    //     { tag: 'h2', attrs: { level: 2 } },
    //     { tag: 'h3', attrs: { level: 3 } },
    //     { tag: 'h4', attrs: { level: 4 } },
    //     { tag: 'h5', attrs: { level: 5 } },
    //     { tag: 'h6', attrs: { level: 6 } }
    //   ],
    //   toDOM(node) {
    //     return ['h' + node.attrs.level, 0];
    //   }
    // },
    heading: HeadingNodeSpec,

    // Code Block
    code_block: {
      content: 'text*',
      group: 'block',
      code: true,
      defining: true,
      marks: '',
      attrs: { params: { default: '' } },
      parseDOM: [
        {
          tag: 'pre',
          preserveWhitespace: 'full',
          getAttrs: (dom) => {
            const domElement = dom as HTMLElement;
            return {
              params: domElement.getAttribute('data-params') || ''
            };
          }
        }
      ],
      toDOM: (node) => {
        return [
          'pre',
          node.attrs.params ? { 'data-params': node.attrs.params } : {},
          ['code', 0]
        ];
      }
    },

    // Ordered List
    ordered_list: {
      content: 'list_item+',
      group: 'block',
      attrs: {
        order: { default: 1 },
        tight: { default: false }
      },
      parseDOM: [
        {
          tag: 'ol',
          getAttrs: (dom) => {
            const domElement = dom as HTMLElement;
            return {
              order: domElement.hasAttribute('start')
                ? +domElement.getAttribute('start')!
                : 1,
              tight: domElement.hasAttribute('data-tight')
            };
          }
        }
      ],
      toDOM: (node) => {
        return [
          'ol',
          {
            start: node.attrs.order == 1 ? null : node.attrs.order,
            'data-tight': node.attrs.tight ? 'true' : null
          },
          0
        ];
      }
    },

    // Bullet List
    bullet_list: {
      content: 'list_item+',
      group: 'block',
      attrs: { tight: { default: false } },
      parseDOM: [
        {
          tag: 'ul',
          getAttrs: (dom) => {
            const domElement = dom as HTMLElement;
            return { tight: domElement.hasAttribute('data-tight') };
          }
        }
      ],
      toDOM: (node) => {
        return ['ul', { 'data-tight': node.attrs.tight ? 'true' : null }, 0];
      }
    },

    // List Item
    list_item: {
      content: 'paragraph block*',
      defining: true,
      parseDOM: [{ tag: 'li' }],
      toDOM: () => {
        return ['li', 0];
      }
    },

    mention: MentionNode,

    // Text
    text: {
      group: 'inline'
    },

    // Image
    image: {
      inline: true,
      attrs: {
        src: {},
        alt: { default: null },
        title: { default: null }
      },
      group: 'inline',
      draggable: true,
      parseDOM: [
        {
          tag: 'img[src]',
          getAttrs: (dom) => {
            const domElement = dom as HTMLElement;
            return {
              src: domElement.getAttribute('src'),
              title: domElement.getAttribute('title'),
              alt: domElement.getAttribute('alt')
            };
          }
        }
      ],
      toDOM: (node) => {
        return ['img', node.attrs];
      }
    },

    // Hard Break
    hard_break: {
      inline: true,
      group: 'inline',
      selectable: false,
      parseDOM: [{ tag: 'br' }],
      toDOM: () => {
        return ['br'];
      }
    }
  },

  marks: {
    // Emphasis (Italic)
    em: {
      parseDOM: [
        { tag: 'i' },
        { tag: 'em' },
        { style: 'font-style', getAttrs: (value) => value == 'italic' && null }
      ],
      toDOM: () => {
        return ['em', 0];
      }
    },

    // Strong (Bold)
    strong: {
      parseDOM: [
        { tag: 'b' },
        { tag: 'strong' },
        {
          style: 'font-weight',
          getAttrs: (dom) => {
            return /^(bold(er)?|[5-9]\d{2,})$/.test(dom as string) && null;
          }
        }
      ],
      toDOM: () => {
        return ['strong', 0];
      }
    },

    // Underline (new)
    underline: {
      parseDOM: [
        { tag: 'u' },
        {
          style: 'text-decoration-line',
          getAttrs: (dom) => {
            return dom === 'underline' && null;
          }
        },
        {
          style: 'text-decoration',
          getAttrs: (dom) => {
            return dom === 'underline' && null;
          }
        }
      ],
      toDOM: () => {
        return ['u', 0];
      }
    },

    // Link
    link: {
      attrs: {
        href: {},
        title: { default: null }
      },
      inclusive: false,
      parseDOM: [
        {
          tag: 'a[href]',
          getAttrs(dom) {
            const domElement = dom as HTMLElement;
            return {
              href: domElement.getAttribute('href'),
              title: domElement.getAttribute('title')
            };
          }
        }
      ],
      toDOM: (node) => {
        return ['a', node.attrs];
      }
    },

    // Code
    code: {
      parseDOM: [{ tag: 'code' }],
      toDOM: () => {
        return ['code'];
      }
    }
  }
});
