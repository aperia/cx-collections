import { Schema } from 'prosemirror-model';
import ParagraphNode from './nodes/ParagraphNode';
import MentionNode from './nodes/MentionNode';

export const schemaMention = new Schema({
  nodes: {
    // Doc
    doc: {
      content: 'block+'
    },

    // Paragraph
    // paragraph: {
    //   content: 'inline*',
    //   group: 'block',
    //   parseDOM: [{ tag: 'p' }],
    //   toDOM() {
    //     return ['p', 0];
    //   }
    // },
    paragraph: ParagraphNode,

    mention: MentionNode,

    // Text
    text: {
      group: 'inline'
    }
  },

  marks: {}
});
