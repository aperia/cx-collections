import '@testing-library/jest-dom';

// utils
import '../../../test-utils/mocks/mockCanvas';
import { schema } from '../prosemirror-markdown';

describe('test schema', () => {
  beforeAll(() => {
    const div = document.createElement('div');
    document.elementFromPoint = () => div;
  });

  it('should cover', () => {
    schema.nodes.blockquote.spec.toDOM();
    schema.nodes.horizontal_rule.spec.toDOM();

    // CodeBlock
    schema.nodes.code_block.spec.toDOM({
      attrs: {
        params: ''
      }
    });
    schema.nodes.code_block.spec.toDOM({
      attrs: {
        params: 'params'
      }
    });
    const codeBlockDOM = document.createElement('div');
    codeBlockDOM.setAttribute('data-params', '');
    schema.nodes.code_block.spec.parseDOM[0].getAttrs(codeBlockDOM);

    // List Item
    schema.nodes.list_item.spec.toDOM({});

    // Ordered List
    schema.nodes.ordered_list.spec.toDOM({
      attrs: {
        order: 0,
        tight: null
      }
    });
    schema.nodes.ordered_list.spec.toDOM({
      attrs: {
        order: 1,
        tight: true
      }
    });
    const orderedListDOM = document.createElement('div');
    orderedListDOM.setAttribute('start', true);
    schema.nodes.ordered_list.spec.parseDOM[0].getAttrs(orderedListDOM);
    orderedListDOM.removeAttribute('start');
    schema.nodes.ordered_list.spec.parseDOM[0].getAttrs(orderedListDOM);

    // Bullet List
    schema.nodes.bullet_list.spec.toDOM({
      attrs: {
        tight: null
      }
    });
    schema.nodes.bullet_list.spec.toDOM({
      attrs: {
        tight: true
      }
    });
    const bulletListDOM = document.createElement('div');
    bulletListDOM.setAttribute('data-tight', true);
    schema.nodes.bullet_list.spec.parseDOM[0].getAttrs(bulletListDOM);

    // Image
    schema.nodes.image.spec.toDOM({
      attrs: {}
    });
    const imageDOM = document.createElement('div');
    schema.nodes.image.spec.parseDOM[0].getAttrs(imageDOM);

    // Hard Break
    schema.nodes.hard_break.spec.toDOM();

    // Em
    schema.marks.em.spec.toDOM();
    schema.marks.em.spec.parseDOM[2].getAttrs('');
    schema.marks.em.spec.parseDOM[2].getAttrs('italic');

    // Strong
    schema.marks.strong.spec.toDOM();
    schema.marks.strong.spec.parseDOM[2].getAttrs('bold');

    // Underline
    schema.marks.underline.spec.toDOM();
    schema.marks.underline.spec.parseDOM[1].getAttrs('underline');
    schema.marks.underline.spec.parseDOM[2].getAttrs('underline');

    // Link
    schema.marks.link.spec.toDOM({
      attrs: ''
    });
    const linkDOM = document.createElement('div');
    schema.marks.link.spec.parseDOM[0].getAttrs(linkDOM);

    // Code
    schema.marks.code.spec.toDOM();

    expect(true).toBeTruthy();
  });
});
