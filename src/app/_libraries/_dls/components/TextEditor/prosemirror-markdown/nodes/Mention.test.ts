import '@testing-library/jest-dom';

// utils
import '../../../../test-utils/mocks/mockCanvas';
import MentionNode from './MentionNode';

describe('test MentionNode', () => {
  it('should cover', () => {
    const divDOM = document.createElement('div');
    divDOM.setAttribute('data-mention-name', 'PP');
    MentionNode.parseDOM[0].getAttrs(divDOM);

    MentionNode.toDOM({
      attrs: {
        name: 'PP'
      }
    });

    expect(true).toBeTruthy();
  });
});
