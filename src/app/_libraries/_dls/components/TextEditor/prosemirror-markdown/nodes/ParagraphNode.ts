import { NodeSpec, ParseRule } from 'prosemirror-model';

// UNUSED CODE
// const ALIGN_PATTERN = /(dls-text-left|dls-text-center|dls-text-right)/;
const getAttrs: ParseRule['getAttrs'] = (dom) => {
  const domElement = dom as HTMLElement;
  const { className } = domElement;
  // let align: string | null =
  // domElement.getAttribute('align') || textAlign || '';
  // align = ALIGN_PATTERN.test(align) ? align : null;
  return { classes: className };
};

// fired when EditorView change
const toDOM: NodeSpec['toDOM'] = (node) => {
  const { classes } = node.attrs;

  const attrs: Record<string, any> = {};
  classes && (attrs.class = classes);
  return ['p', attrs, 0];
};

const ParagraphNode: NodeSpec = {
  attrs: {
    classes: { default: '' }
  },
  content: 'inline*',
  group: 'block',
  parseDOM: [{ tag: 'p', getAttrs }],
  toDOM
};

const paragraphAttrs = getAttrs;
const paragraphToDOM = toDOM;
export { paragraphAttrs, paragraphToDOM };
export default ParagraphNode;
