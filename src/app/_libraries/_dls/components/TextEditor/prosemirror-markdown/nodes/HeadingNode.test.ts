import '@testing-library/jest-dom';

// utils
import '../../../../test-utils/mocks/mockCanvas';
import HeadingNode from './HeadingNode';

describe('test HeadingNode', () => {
  it('should cover', () => {
    const divDOM = document.createElement('div');
    HeadingNode.parseDOM[0].getAttrs(divDOM);

    const headingDOM = document.createElement('h1');
    HeadingNode.parseDOM[0].getAttrs(headingDOM);

    HeadingNode.toDOM({
      attrs: 0
    });
    HeadingNode.toDOM({
      attrs: 1
    });

    expect(true).toBeTruthy();
  });
});
