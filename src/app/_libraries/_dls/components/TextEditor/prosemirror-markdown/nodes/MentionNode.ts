import { NodeSpec } from 'prosemirror-model';

// fired when EditorView change
const toDOM: NodeSpec['toDOM'] = (node) => {
  return [
    'span',
    {
      'data-mention-name': node.attrs.name,
      class: 'dls-text-editor-mention-node'
    },
    node.attrs.name
  ];
};

const parseDOM: NodeSpec['parseDOM'] = [
  {
    tag: 'span[data-mention-name]',
    getAttrs: (dom) => {
      const domElement = dom as HTMLElement;
      const name = domElement.getAttribute('data-mention-name');
      return {
        name
      };
    }
  }
];

const MentionNode: NodeSpec = {
  group: 'inline',
  inline: true,
  atom: true,
  attrs: {
    name: { default: '' },
    classes: { default: '' }
  },
  selectable: false,
  draggable: false,
  toDOM,
  parseDOM
};

export default MentionNode;
