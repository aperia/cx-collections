import { NodeSpec, ParseRule } from 'prosemirror-model';
import { paragraphAttrs, paragraphToDOM } from './ParagraphNode';

const TAG_NAME_TO_LEVEL: Record<string, any> = {
  h1: 1,
  h2: 2,
  h3: 3,
  h4: 4,
  h5: 5,
  h6: 6
};

const getAttrs: ParseRule['getAttrs'] = (dom) => {
  const domElement = dom as HTMLElement;

  const attrs = paragraphAttrs(domElement) as Record<string, any>;
  const level = TAG_NAME_TO_LEVEL[domElement.nodeName.toLowerCase()] || 1;

  attrs.level = level;

  return attrs;
};

const toDOM: NodeSpec['toDOM'] = (node) => {
  const dom = paragraphToDOM(node);
  const level = node.attrs.level || 1;

  (dom as any)[0] = `h${level}`;
  return dom;
};

const HeadingNode: NodeSpec = {
  attrs: {
    level: { default: 1 },
    classes: { default: '' }
  },
  content: '(text | image)*',
  group: 'block',
  defining: true,
  parseDOM: [
    { tag: 'h1', getAttrs },
    { tag: 'h2', getAttrs },
    { tag: 'h3', getAttrs },
    { tag: 'h4', getAttrs },
    { tag: 'h5', getAttrs },
    { tag: 'h6', getAttrs }
  ],
  toDOM
};

export default HeadingNode;
