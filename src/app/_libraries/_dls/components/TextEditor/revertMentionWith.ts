const revertMentionWith = (markdownModified: string) => {
  const regex = new RegExp(
    `\\{[a-zA-Z0-9]+(([',. -][a-zA-Z0-9 ])?[a-zA-Z0-9]*)*\\}`,
    'gm'
  );
  let output = markdownModified;

  const matches = markdownModified.match(regex);
  if (matches?.length) {
    matches.forEach((match) => {
      const withLink = `(${match.replace('{', '').replace('}', '')})`
        .split(' ')
        .join('');
      const prefix = match.replace('{', '@[').replace('}', ']');
      const replacer = prefix + withLink;

      output = output.replace(match, replacer);
    });
  }

  return output;
};

export default revertMentionWith;
