import '@testing-library/jest-dom';

// utils
import '../../test-utils/mocks/mockCanvas';

// components

import replaceMentionWith from './replaceMentionWith';

describe('test replaceMentionWith', () => {
  it('should return empty string if markdown is empty', () => {
    const result = replaceMentionWith('');
    expect(result).toEqual('');
  });
  it('should return new markdown', () => {
    const result = replaceMentionWith('@[Item 1](Item1)');
    expect(result).toEqual('{Item 1}');
  });
});
