import '@testing-library/jest-dom';

// utils
import '../../test-utils/mocks/mockCanvas';

// components

import revertMentionWith from './revertMentionWith';

describe('test revertMentionWith', () => {
  it('should return empty string if markdown is empty', () => {
    const result = revertMentionWith('');
    expect(result).toEqual('');
  });
  it('should return new markdown', () => {
    const result = revertMentionWith('{Item 1}');
    expect(result).toEqual('@[Item 1](Item1)');
  });
});
