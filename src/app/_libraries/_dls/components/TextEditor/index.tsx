import React, {
  DOMAttributes,
  useEffect,
  useImperativeHandle,
  useLayoutEffect,
  useRef,
  useState
} from 'react';
import ReactDOM from 'react-dom';

import { EditorState } from 'prosemirror-state';
import { EditorView } from 'prosemirror-view';
import { keymap } from 'prosemirror-keymap';
import { baseKeymap } from 'prosemirror-commands';
import { dropCursor } from 'prosemirror-dropcursor';
import { gapCursor } from 'prosemirror-gapcursor';
import { history } from 'prosemirror-history';
import { TextEditorProps, TextEditorRef } from './types';
import classnames from 'classnames';
import {
  defaultMarkdownParser,
  defaultMarkdownSerializer,
  schema,
  schemaMention,
  buildKeymap
} from './prosemirror-markdown';
import {
  maxLengthPlugin,
  mentionPlugin,
  toolbarPlugin,
  plainTextChangePlugin,
  placeholderPlugin
} from './plugins';
import {
  BlockTypes,
  Bold,
  ToggleLink,
  Italic,
  Underline,
  TextLeft,
  TextCenter,
  TextRight,
  OrderedList,
  BulletList,
  Indent,
  Outdent
} from './tools';
import DropdownList, {
  DropdownListProps,
  DropdownListRef
} from '../DropdownList';
import Tooltip from '../Tooltip';
import { useBlurOnScroll, useDeepValue } from '../../hooks';
import { TextEditorContext, TextEditorContextType } from './contexts';
import { pick } from '../../lodash';
import { classes } from '../../utils';
import { MentionDropdownList } from './components';
import markdownToHTML from './markdownToHTML';
import replaceMentionWith from './replaceMentionWith';
import revertMentionWith from './revertMentionWith';

const TextEditor: React.RefForwardingComponent<TextEditorRef, TextEditorProps> =
  (
    {
      placeholder = 'Type and format your text.',
      onFocus,
      onBlur,
      error,
      errorTooltipProps,
      readOnly,

      maxPlainText,
      onPlainTextChange,
      mention,
      mentionData,

      dataTestId
    },
    ref
  ) => {
    // refs
    const containerRef = useRef<HTMLDivElement | null>(null);
    const textEditorHoleRef = useRef<HTMLDivElement | null>(null);
    const coreRef = useRef<Record<string, any>>({});

    // states
    const [contextValue, setContextValue] = useState<TextEditorContextType>(
      {} as any
    );
    const [focused, setFocused] = useState(false);
    const [mentionDropdownList, setMentionDropdownList] =
      useState<React.ReactNode>();
    const [dropdownListRef, setDropdownListRef] = useState<DropdownListRef>();

    // persistent
    const contextValuePersisted = useDeepValue({
      ...contextValue,
      readOnly
    })!;

    // For Mention
    const toggleMention = (
      opened: boolean,
      handleChange: DropdownListProps['onChange']
    ) => {
      if (!mentionData) return;

      const portal = ReactDOM.createPortal(
        <MentionDropdownList
          ref={el => {
            setDropdownListRef((opened ? el : null) as any);
          }}
          opened={opened}
          variant="no-border"
          onChange={handleChange}
          tabIndex={-1}
        >
          {mentionData.map((mentionItem, index) => {
            return (
              <DropdownList.Item
                key={mentionItem + index}
                value={mentionItem}
                label={mentionItem}
              />
            );
          })}
        </MentionDropdownList>,
        textEditorHoleRef.current!
      );

      setMentionDropdownList(portal);
    };

    coreRef.current.toggleMention = toggleMention;
    coreRef.current.onPlainTextChange = onPlainTextChange;

    const handleFocus: DOMAttributes<HTMLDivElement>['onFocus'] = event => {
      onFocus?.(event);

      setFocused(true);
    };

    const handleBlur: DOMAttributes<HTMLDivElement>['onBlur'] = event => {
      onBlur?.(event);
      setFocused(false);
    };

    useImperativeHandle(
      ref,
      () => {
        return {
          getMarkdown: () => {
            const { editorView } = contextValuePersisted;
            const markdown = defaultMarkdownSerializer.serialize(
              editorView?.state?.doc
            );

            return markdown;
          },
          setMarkdown: (markdown: string) => {
            const { editorView } = contextValuePersisted;
            const doc = defaultMarkdownParser.parse(markdown);
            const nextSchema = mention ? schemaMention : schema;
            const plugins = [
              mentionPlugin({
                mentionTrigger: '/',
                suggestionTextClass: 'dls-text-editor-mention',
                toggleMention: coreRef.current.toggleMention,
                maxPlainText,
                mention
              }),
              placeholderPlugin(placeholder),
              keymap(buildKeymap(nextSchema, {}) as any),
              keymap(baseKeymap),
              dropCursor(),
              gapCursor(),
              history(),
              maxLengthPlugin(maxPlainText),
              plainTextChangePlugin(
                coreRef.current.onPlainTextChange,
                textEditorHoleRef.current!
              )
            ];

            if (!mention) {
              plugins.push(toolbarPlugin(setContextValue));
            }

            const state = EditorState.create({
              schema: nextSchema,
              plugins,
              doc
            });

            editorView?.updateState(state);
            coreRef.current.onPlainTextChange?.(
              editorView?.state?.doc?.textContent
            );
          }
        };
      },
      [contextValuePersisted, maxPlainText, mention, placeholder]
    );

    // For Mention
    useLayoutEffect(() => {
      if (!dropdownListRef) return;

      // if dropdownListRef exists, window always has selection
      const selection = window.getSelection()!;
      const anchorNode = selection.anchorNode as HTMLElement;
      let trigger;

      // if anchorNode is TextNode, querySelector will be undefined
      if (!anchorNode.querySelector) {
        trigger = selection.getRangeAt(0);
      }
      // otherwise
      else {
        trigger = anchorNode.querySelector(
          '.dls-text-editor-mention'
        ) as HTMLElement;
      }

      const containerPos = textEditorHoleRef.current!.getBoundingClientRect();
      const rangePosition = trigger.getBoundingClientRect();

      const relativeLeft = rangePosition.left - containerPos.left;
      const relativeTop =
        rangePosition.height + rangePosition.top - containerPos.top;

      const containerElement = dropdownListRef.inputElement?.closest(
        '.dls-popper-trigger'
      ) as HTMLElement;
      containerElement.style.position = 'absolute';
      containerElement.style.left = `${relativeLeft}px`;
      containerElement.style.top = `${relativeTop}px`;
    }, [dropdownListRef]);

    // init
    useEffect(() => {
      const nextSchema = mention ? schemaMention : schema;
      const plugins = [
        mentionPlugin({
          mentionTrigger: '/',
          allowSpace: false,
          suggestionTextClass: 'dls-text-editor-mention',
          toggleMention: coreRef.current.toggleMention,
          maxPlainText,
          mention
        }),
        placeholderPlugin(placeholder),
        keymap(buildKeymap(nextSchema, {}) as any),
        keymap(baseKeymap),
        dropCursor(),
        gapCursor(),
        history(),
        maxLengthPlugin(maxPlainText),
        plainTextChangePlugin(
          coreRef.current.onPlainTextChange,
          textEditorHoleRef.current!
        )
      ];

      if (!mention) {
        plugins.push(toolbarPlugin(setContextValue));
      }

      const state = EditorState.create({
        schema: nextSchema,
        plugins
      });

      // create new editor view
      const view = new EditorView(textEditorHoleRef.current!, {
        state: state
      });

      setContextValue(state => ({
        ...state,
        editorView: view
      }));

      return () => view.destroy();
    }, [maxPlainText, mention, placeholder]);

    // check should show error or not
    const { message, status } = pick(error, 'message', 'status');

    // add blur on scroll if Tooltip is opened && error && scrolled
    useBlurOnScroll(
      textEditorHoleRef.current?.querySelector('.ProseMirror') as HTMLElement,
      Boolean(status && focused)
    );

    return (
      <div
        ref={containerRef}
        onFocus={handleFocus}
        onBlur={handleBlur}
        className={classnames('dls-text-editor', {
          [classes.state.error]: Boolean(status)
        })}
      >
        {mention ? null : (
          <TextEditorContext.Provider value={contextValuePersisted}>
            <div className="dls-text-editor-tools-container">
              <div className="dls-text-editor-tools-group">
                <Bold />
                <Italic />
                <Underline />
              </div>
              <div className="dls-text-editor-tools-group">
                <TextLeft />
                <TextCenter />
                <TextRight />
              </div>
              <div className="dls-text-editor-tools-group">
                <OrderedList />
                <BulletList />
                <Indent />
                <Outdent />
              </div>
              <div className="dls-text-editor-tools-group">
                <ToggleLink />
              </div>
              <BlockTypes />
            </div>
          </TextEditorContext.Provider>
        )}

        <Tooltip
          opened={Boolean(status && focused)}
          element={message}
          variant="error"
          placement="top-start"
          triggerClassName="d-block"
          dataTestId={`${dataTestId}-editor-error`}
          {...errorTooltipProps}
        >
          <div ref={textEditorHoleRef} className="dls-text-editor-hole" />
        </Tooltip>
        {mentionDropdownList}
      </div>
    );
  };

export { markdownToHTML, replaceMentionWith, revertMentionWith };
export default React.forwardRef<TextEditorRef, TextEditorProps>(TextEditor);
