import React, {
  useRef,
  useState,
  useEffect,
  forwardRef,
  useImperativeHandle,
  useMemo,
  DOMAttributes
} from 'react';

// utils
import classnames from 'classnames';
import datetime from 'date-and-time';
import {
  keycode,
  extendsEvent,
  genAmtId,
  classes,
  truncateTooltip
} from '../../utils';
import { isBoolean, isDate, isEmpty, isFunction, pick } from '../../lodash';

// components/types
import MaskedTextBox, { MaskedTextBoxRef } from '../MaskedTextBox';
import Calendar, {
  Detail,
  FormatterCallback,
  OnChangeDateCallback,
  ViewCallback
} from 'react-calendar';
import PopupBase, { PopupBaseRef } from '../PopupBase';
import Label from '../Label';
import Tooltip from '../Tooltip';
import {
  DatePickerProps,
  DatePickerRef,
  DatePickerViewConfig,
  MaxDetail,
  MinDetail
} from './types';
import Icon from '../Icon';

// hooks
import { useTodayPortal, useDateTooltipPortals } from './hooks';

const formatShortWeekday: FormatterCallback = (locale, date) => {
  return datetime.format(date, 'dd');
};
const formatMonth: FormatterCallback = (locale, date) => {
  return datetime.format(date, 'MMM');
};
const MIN_DATE = new Date('01/01/1900');
const MAX_DATE = new Date('01/01/3000');
const MONTH_VIEW: DatePickerViewConfig = {
  placeholder: 'mm/dd/yyyy',
  format: 'MM/DD/YYYY',
  minDetail: 'century' as MinDetail,
  maxDetail: 'month' as MaxDetail,
  label: 'Today',
  mask: [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/],
  validLength: 10
};
const YEAR_VIEW: DatePickerViewConfig = {
  placeholder: 'mm/yyyy',
  format: 'MM/YYYY',
  minDetail: 'century' as MinDetail,
  maxDetail: 'year' as MaxDetail,
  label: 'Current Month',
  mask: [/\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/],
  validLength: 7
};

const DatePicker: React.RefForwardingComponent<
  DatePickerRef,
  DatePickerProps
> = (
  {
    value: valueProp,
    onChange,

    label,
    size,

    onBlur,
    onFocus,

    opened: openedProp,
    onVisibilityChange,
    onPopupShown,
    onPopupClosed,

    readOnly,
    disabled,
    placeholder,

    required,
    error,
    errorTooltipProps,

    popupBaseProps,

    id,
    name,
    autoFocus,
    className: classNameProp,

    minDate = MIN_DATE,
    maxDate = MAX_DATE,
    dateTooltip,
    disabledRange,

    titleDisabled,

    defaultView = 'month',
    hideIcon = false,
    locale = 'en-EN',
    todayLabel = 'Today',
    currentMonthLabel = 'Current Month',

    dataTestId
  },
  ref
) => {
  // refs
  const containerRef = useRef<HTMLDivElement | null>(null);
  const popupBaseRef = useRef<PopupBaseRef | null>(null);
  const textFieldContainerRef = useRef<HTMLDivElement | null>(null);
  const isMouseDownRef = useRef(false);
  const isKeyboardRef = useRef(false);
  const isTypingsRef = useRef(false);
  const keepRef = useRef<Record<string, any>>({});
  const autoFocusRef = useRef(autoFocus);
  const fakeFocusRef = useRef<HTMLDivElement | null>(null);
  const maskedTextBoxRef = useRef<MaskedTextBoxRef | null>(null);
  const floatingLabelRef = useRef<HTMLSpanElement | null>(null);

  // states
  const [opened, setOpened] = useState(!!openedProp);
  const [value, setValue] = useState(valueProp);
  const [valueMaskedTextBox, setValueMaskedTextBox] = useState('');
  const [view, setView] = useState<Detail>('month');

  // refresh disabledRange to start of day
  const formatDisabledRange = (
    disabledRange: DatePickerProps['disabledRange']
  ) => {
    const nextDisabledRange = disabledRange?.map((range) => {
      const { fromDate, toDate } = range;
      let nextFromDate, nextToDate;

      // clone date
      if (fromDate) {
        nextFromDate = new Date(fromDate.getTime());
      }
      if (toDate) {
        nextToDate = new Date(toDate.getTime());
      }

      // refresh clone date
      nextFromDate?.setHours(0, 0, 0, 0);
      nextToDate?.setHours(0, 0, 0, 0);

      // return clone date
      return { fromDate: nextFromDate, toDate: nextToDate };
    });

    return nextDisabledRange;
  };

  // config
  const viewConfig = useMemo(
    () =>
      defaultView === 'month'
        ? {
            ...MONTH_VIEW,
            label: todayLabel
          }
        : {
            ...YEAR_VIEW,
            label: currentMonthLabel
          },
    [defaultView, todayLabel, currentMonthLabel]
  );

  // date config
  const dateConfig = useMemo(() => {
    let iMinDate = minDate;
    let iMaxDate = maxDate;

    // if maxDate < minDate, swap it
    if (iMaxDate < iMinDate) {
      const tempDate = iMaxDate;
      iMaxDate = iMinDate;
      iMinDate = tempDate;
    }

    const minYear = iMinDate.getFullYear();
    const minMonth = iMinDate.getMonth() + 1;
    const minBeginDate = new Date(iMinDate.getTime());
    minBeginDate.setHours(0, 0, 0, 0);

    const maxYear = iMaxDate.getFullYear();
    const maxMonth = iMaxDate.getMonth() + 1;
    const maxBeginDate = new Date(iMaxDate.getTime());
    maxBeginDate.setHours(0, 0, 0, 0);

    return {
      min: {
        year: minYear,
        month: minMonth,
        beginDate: minBeginDate
      },
      max: {
        year: maxYear,
        month: maxMonth,
        beginDate: maxBeginDate
      }
    };
  }, [minDate, maxDate]);

  const checkDisabledRange = (date: Date) => {
    const nextDisabledRange = formatDisabledRange(disabledRange);

    if (Array.isArray(nextDisabledRange) && !isEmpty(nextDisabledRange)) {
      for (let index = 0; index < nextDisabledRange.length; index++) {
        const { fromDate, toDate } = nextDisabledRange[index];

        const isValidFromDate = isDate(fromDate) && !isNaN(fromDate.valueOf());
        const isValidToDate = isDate(toDate) && !isNaN(toDate.valueOf());

        if (
          isValidFromDate &&
          date >= fromDate! &&
          isValidToDate &&
          date <= toDate!
        ) {
          return true;
        }
      }
    }

    return false;
  };

  const checkDisabledToday = () => {
    const today = new Date();
    today.setHours(0, 0, 0, 0);
    return (
      today < dateConfig.min.beginDate ||
      today > dateConfig.max.beginDate ||
      checkDisabledRange(today)
    );
  };

  const checkDisabled = (date: Date, view: Detail) => {
    const year = date.getFullYear();
    const month = date.getMonth() + 1;

    switch (view) {
      case 'century':
        const centuryRangeMaxYear = year + 9;
        const centuryCondition =
          centuryRangeMaxYear < dateConfig.min.year ||
          centuryRangeMaxYear > dateConfig.max.year;
        if (centuryCondition) return centuryCondition;
        break;
      case 'decade':
        const decadeCondition =
          year < dateConfig.min.year || year > dateConfig.max.year;
        if (decadeCondition) return decadeCondition;
        break;
      case 'year':
        const yearCondition =
          year < dateConfig.min.year ||
          year > dateConfig.max.year ||
          (year <= dateConfig.min.year && month < dateConfig.min.month) ||
          (year >= dateConfig.max.year && month > dateConfig.max.month);
        if (yearCondition) return yearCondition;
        break;
      case 'month':
        const monthCondition =
          date < dateConfig.min.beginDate || date > dateConfig.max.beginDate;
        if (monthCondition) return monthCondition;
        break;
    }

    // handle disabled range
    const isDisabledRange = checkDisabledRange(date);
    if (isDisabledRange) return isDisabledRange;

    return false;
  };

  // handle titleDisabled props Calendar component
  const handleTitleDisabled: typeof titleDisabled = ({
    date,
    view,
    ...restProps
  }) => {
    if (isFunction(titleDisabled)) {
      return titleDisabled({ date, view, ...restProps });
    }

    return checkDisabled(date, view);
  };

  // provide ref
  useImperativeHandle(
    ref,
    () => ({
      containerElement: containerRef.current,
      inputContainerElement: textFieldContainerRef.current,
      maskedTextBox: maskedTextBoxRef.current,
      popupBaseRef
    }),
    []
  );

  const resetRef = () => {
    isMouseDownRef.current = false;
    isKeyboardRef.current = false;
    isTypingsRef.current = false;
    autoFocusRef.current = false;
  };

  // handle opened uncontrolled/controlled
  const handleOpenedMode = (nextOpened: boolean) => {
    isFunction(onVisibilityChange) && onVisibilityChange(nextOpened);

    // controlled
    if (isBoolean(openedProp)) return;

    // uncontrolled
    setOpened(nextOpened);
  };

  // handle value uncontrolled/controlled
  const handleValueMode = (nextValue?: Date) => {
    isFunction(onChange) &&
      onChange({
        target: {
          value: nextValue,
          id,
          name
        },
        value: nextValue
      } as any);

    // if isMouseDownRef.current is true, we know that we need to call onBlur on nextTick
    // because we need to make sure onChange has done (new value updated) before we call onBlur
    if (isMouseDownRef.current) {
      process.nextTick(() => maskedTextBoxRef.current?.inputElement?.blur());
    }

    // controlled
    if (isDate(valueProp)) return;

    // uncontrolled
    setValue(nextValue);
  };

  const handlePopupShown = () => {
    // temp code, just exists for DatePicker AttributeSearch control
    if (isFunction(onPopupShown)) onPopupShown();

    const popupElement = popupBaseRef.current?.element;
    if (!popupElement) return;

    // handle mousedown on Popup's DatePicker
    // mousedown on Popup's DatePicker will trigger onBlur default of the DatePicker
    // that is not the behavior we want, so we need to preventDefault it
    const handleMouseDown = (event: MouseEvent) => {
      // stop browser events
      event.preventDefault();

      // need set isMouseDownRef.current to true
      // because handleValueMode will be called if user choose date
      // handleValueMode need to know if date is chosen by using mousedown
      // to trigger blur on next tick (after value has been handled)
      isMouseDownRef.current = true;
    };

    // not need to remove event later, because popup element will be remove from DOM
    popupElement.addEventListener('mousedown', handleMouseDown);
  };

  const handlePopupClosed = () => {
    if (isFunction(onPopupClosed)) onPopupClosed();

    if (document?.activeElement === document?.body) {
      fakeFocusRef.current?.focus({ preventScroll: true });
    }
  };

  // handle focus on MaskedTextBox input element
  const handleFocus = (event: React.FocusEvent<HTMLInputElement>) => {
    if (
      !isMouseDownRef.current &&
      !isKeyboardRef.current &&
      !autoFocusRef.current
    ) {
      return;
    }

    resetRef();
    let nextEvent = extendsEvent(event, 'target', {
      id,
      name,
      value
    });
    nextEvent = extendsEvent(nextEvent, null, { value });

    isFunction(onFocus) && onFocus(nextEvent);
    handleOpenedMode(true);
  };

  // handle blur on MaskedTextBox input element
  const handleBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    // we are in the onBlur stage
    // the valueMaskedTextBox maybe valid or maybe invalid
    // if the current value is valid and not equal valueMaskedTextBox
    // we will reset the valueMaskedTextBox back to current value
    const isValidValue = isDate(value) && !isNaN(value.valueOf());
    if (isValidValue) {
      const formattedValue = datetime.format(value!, viewConfig.format);
      if (formattedValue !== valueMaskedTextBox) {
        setValueMaskedTextBox(formattedValue);
      }
    } else setValueMaskedTextBox('');

    // what if the user clicks on something that is not in the browser?
    // example: desktop, taskbar, developer tools...
    // by default, if the user clicks on those thing, the browser will keeps
    // the previous state
    // So the next time, the user clicks (mousedown) on the screen (valid container DOM browser),
    // onFocus will be called and onMouseDown will be not called
    // we don't want that behavior
    // so this code block to prevent that behavior
    if (
      document.activeElement &&
      document.activeElement === maskedTextBoxRef.current?.inputElement
    ) {
      maskedTextBoxRef.current?.inputElement?.blur();
    }

    resetRef();
    let nextEvent = extendsEvent(event, 'target', {
      id,
      name,
      value
    });
    nextEvent = extendsEvent(nextEvent, null, { value });

    isFunction(onBlur) && onBlur(nextEvent);
    handleOpenedMode(false);
  };

  // handle onChange Calendar
  const handleOnChangeCalendar: OnChangeDateCallback = (date) => {
    handleValueMode(date as Date);
  };

  // handle onChange MaskedTextBox
  const handleOnChangeMaskedTextBox = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    if (readOnly || disabled) return;

    // if user is typings, we need to reset isMouseDownRef.current to false
    // just to make sure logic is running correct
    // setting isMouseDownRef.current to true maybe redundant
    isMouseDownRef.current = false;

    // signal that the user is typings
    isTypingsRef.current = true;
    setValueMaskedTextBox(event.target.value);
  };

  // handle onViewChange Calendar
  const handleOnViewChange: ViewCallback = (viewCallbackProps) => {
    setView(viewCallbackProps.view);
  };

  // handle mousedown on icon
  const handleOnMouseDownIcon = (
    event: React.MouseEvent<HTMLSpanElement, MouseEvent>
  ) => {
    const inputElement = maskedTextBoxRef.current?.inputElement;

    // stop default behavior browser (blur/focus, abc, xyz...)
    event.preventDefault();

    if (document.activeElement && document.activeElement === inputElement) {
      inputElement?.blur();
      return;
    }

    isMouseDownRef.current = true;
    inputElement?.focus({ preventScroll: true });
  };

  // handle click button on footer
  const handleClickButtonFooter = () => handleOnChangeCalendar(new Date());

  // temp
  const handleOnMouseDown: DOMAttributes<HTMLDivElement>['onMouseDown'] = (
    event
  ) => {
    event.preventDefault();

    const inputElement = maskedTextBoxRef.current?.inputElement;
    isMouseDownRef.current = true;
    inputElement?.focus({ preventScroll: true });
  };

  // keep reference
  keepRef.current.handleValueMode = handleValueMode;
  keepRef.current.handleClickButtonFooter = handleClickButtonFooter;

  // set props
  useEffect(() => setValue(valueProp), [valueProp]);
  useEffect(() => setOpened(!!openedProp), [openedProp]);

  // provides tab/shift-tab keyboard to DatePicker
  // just checking if previous keyboard action is tab or shift+tab
  useEffect(() => {
    const handleKeydown = (event: KeyboardEvent) => {
      const { keyCode, shiftKey } = event;

      if (keyCode === keycode.TAB || (shiftKey && keyCode === keycode.TAB)) {
        isKeyboardRef.current = true;
      }
    };

    window.addEventListener('keydown', handleKeydown);
    return () => window.removeEventListener('keydown', handleKeydown);
  }, [disabled]);

  // base on value, if value is a valid date, format it and set to MaskedTextBox value
  useEffect(() => {
    let nextMaskedTextBoxValue = '';

    if (isDate(value) && !isNaN(value.valueOf())) {
      nextMaskedTextBoxValue = datetime.format(value, viewConfig.format);
    }

    setValueMaskedTextBox(nextMaskedTextBoxValue);
  }, [value, viewConfig]);

  // handle valueMaskedTextBox
  useEffect(() => {
    if (!isTypingsRef.current) return;

    // we based on isTypingsRef.current to know if user typing or not
    // so we need to reset isTyingRefs.current to false for the next check
    isTypingsRef.current = false;

    const { handleValueMode } = keepRef.current;
    const dateParsed = datetime.parse(valueMaskedTextBox, viewConfig.format);

    // if valueMaskedTextBox is empty
    if (valueMaskedTextBox === '') {
      handleValueMode(undefined);
      return;
    }

    const isValidDate = isDate(dateParsed) && !isNaN(dateParsed.valueOf());

    // if date is invalid, and length is not full length (typings is not completed)
    // just reset valueMaskedTextBox to empty
    if (!isValidDate) {
      if (
        valueMaskedTextBox.split('_').join('').length === viewConfig.validLength
      ) {
        setValueMaskedTextBox('');
      }

      return;
    }

    // if dateParsed is valid date object
    if (dateParsed < minDate) return handleValueMode(minDate);
    if (dateParsed > maxDate) return handleValueMode(maxDate);
    handleValueMode(dateParsed);
  }, [maxDate, minDate, valueMaskedTextBox, viewConfig]);

  // handle truncate tooltip label
  useEffect(() => truncateTooltip(floatingLabelRef.current!), [label]);

  // handle autofocus first time render
  useEffect(() => {
    if (!autoFocusRef.current) return;

    maskedTextBoxRef.current?.inputElement?.focus();
  }, []);

  // handle add today button to Popup's DatePicker
  // for the first time render, viewRef.current is undefined
  const todayPortal = useTodayPortal(popupBaseRef, handleClickButtonFooter, {
    opened,
    viewConfig,
    view,
    isDisabled: checkDisabledToday()
  });

  // handle watching view change to trigger logic render tooltip again
  const dateTooltipPortals = useDateTooltipPortals(popupBaseRef, {
    opened: opened && !readOnly,
    dateTooltip
  });

  const containerId = genAmtId(dataTestId, 'dls-date-picker', 'DatePicker');
  const { popupBaseClassName, ...popupProps } = popupBaseProps || {};

  // check required
  const isRequired = required && !readOnly && !disabled;

  // check small size
  const isSmallSize = size === 'small';

  // check should show error or not
  const { message, status } = pick(error, 'message', 'status');
  const isOpenedTooltip = Boolean(status && message && opened);

  // check no label
  const noLabel = !label || isSmallSize;

  // check should floating label or not
  const floating = (!noLabel && opened && !readOnly && !disabled) || value;

  return (
    <>
      <Tooltip
        opened={isOpenedTooltip}
        element={message}
        variant="error"
        placement="top-start"
        triggerClassName="d-block"
        dataTestId={genAmtId(containerId, 'error', 'DatePicker')}
        {...errorTooltipProps}
      >
        <div
          ref={containerRef}
          onMouseDown={handleOnMouseDown}
          className={classnames(
            {
              [classes.datePicker.container]: true,
              [classes.state.focused]: opened,
              [classes.state.readOnly]: readOnly,
              [classes.state.disabled]: disabled,
              [classes.state.error]: status,
              [classes.state.floating]: floating,
              [classes.state.noLabel]: noLabel,
              [classes.state.small]: isSmallSize
            },
            classNameProp
          )}
          data-testid={containerId}
        >
          <div ref={textFieldContainerRef} className="text-field-container">
            <MaskedTextBox
              ref={maskedTextBoxRef}
              value={valueMaskedTextBox}
              onChange={handleOnChangeMaskedTextBox}
              onFocus={handleFocus}
              onBlur={handleBlur}
              mask={viewConfig.mask}
              id={id}
              name={name}
              placeholder={placeholder || viewConfig.placeholder}
              disabled={disabled}
              dataTestId={genAmtId(containerId, 'input', 'DatePicker')}
            />
            {!noLabel && (
              <Label
                ref={floatingLabelRef}
                asterisk={isRequired}
                error={status}
                dataTestId={genAmtId(containerId, 'label', 'DatePicker')}
              >
                <span>{label}</span>
              </Label>
            )}
          </div>
          {!hideIcon && (
            <Icon
              onMouseDown={handleOnMouseDownIcon}
              name="calendar"
              size={isSmallSize ? '4x' : '5x'}
              dataTestId={genAmtId(containerId, 'calendar-icon', 'DatePicker')}
            />
          )}
          <div ref={fakeFocusRef} tabIndex={-1} />
        </div>
      </Tooltip>
      <PopupBase
        ref={popupBaseRef}
        reference={maskedTextBoxRef.current?.inputElement as HTMLElement}
        opened={opened && !readOnly && !disabled}
        onVisibilityChange={handleOpenedMode}
        onPopupShown={handlePopupShown}
        onPopupClosed={handlePopupClosed}
        popupBaseClassName={classnames(
          'dls-popup',
          'dls-date-picker-popup',
          popupBaseClassName
        )}
        fluid
        dataTestId={genAmtId(containerId, 'popup', 'DatePicker')}
        {...popupProps}
      >
        <Calendar
          value={value}
          onChange={handleOnChangeCalendar}
          formatShortWeekday={formatShortWeekday}
          formatMonth={formatMonth}
          calendarType="US"
          tileDisabled={handleTitleDisabled}
          tileContent={(props) => {
            return (
              <div className="dls-tooltip-cache-date">
                {props.date?.toString()}
              </div>
            );
          }}
          minDetail={viewConfig.minDetail}
          maxDetail={viewConfig.maxDetail}
          view={view}
          onViewChange={handleOnViewChange}
          locale={locale}
        />
        {todayPortal}
        {dateTooltipPortals}
      </PopupBase>
    </>
  );
};

export * from './types';
export default forwardRef<DatePickerRef, DatePickerProps>(DatePicker);
