import React, {
  useRef,
  useImperativeHandle,
  useEffect,
  useState,
  useContext
} from 'react';

// utils
import classnames from 'classnames';
import { classes, genAmtId } from '../../utils';
import { isBoolean, isFunction } from '../../lodash';

// components/types
import Popper, { PopperProps, PopperRef } from '../Popper';
import { StorageProvider, useStorage } from '../../providers';
import useCloseOnScroll from './useCloseOnScroll';
import useMouseDownOutside from './useMouseDownOutside';

export interface PopoverProps extends PopperProps {
  popoverInnerClassName?: string;
  onVisibilityChange?: (isOpened: boolean) => void;
  size?: 'sm' | 'md' | 'lg' | 'xl' | 'auto';
}

export interface PopoverContextType {
  value?: any;
  opened?: boolean;
  keepMount?: boolean;
  setValue?: (nextValue: any) => void;
  setOpened?: (nextValue?: boolean) => void;
  setKeepMount?: (nextValue?: boolean) => void;
}

const PopoverContext = React.createContext<PopoverContextType>({});
const usePopoverContext = () => useContext(PopoverContext);

const Popover: React.RefForwardingComponent<PopperRef, PopoverProps> = (
  {
    element,
    containerClassName,
    arrowClassName,
    popoverInnerClassName,
    opened: openedProp,
    onVisibilityChange,
    keepMount: keepMountProp,
    size = 'sm',

    dataTestId,
    ...props
  },
  ref
) => {
  const storage = useStorage();

  // refs
  const popperRef = useRef<PopperRef | null>(null);
  const popoverInnerRef = useRef<HTMLDivElement | null>(null);
  const keepRef = useRef<Record<string, any>>({});

  // states
  const [opened, setOpened] = useState(openedProp);
  const [keepMount, setKeepMount] = useState(keepMountProp);
  const [popoverContextValue, setPopoverContextValue] =
    useState<PopoverContextType>({
      value: '',
      opened,
      keepMount,
      setValue: (nextValue) => {
        setPopoverContextValue((state) => ({ ...state, value: nextValue }));
      },
      setOpened: (nextValue) => {
        setPopoverContextValue((state) => ({ ...state, opened: nextValue }));
      },
      setKeepMount: (nextKeepMount) => {
        setPopoverContextValue((state) => ({
          ...state,
          keepMount: nextKeepMount
        }));
      }
    });

  // handle controlled opened
  const handleOpenedMode = (isOpened: boolean) => {
    isFunction(onVisibilityChange) && onVisibilityChange(isOpened);

    // controlled
    if (isBoolean(openedProp)) return;

    // uncontrolled
    setOpened(isOpened);
  };

  // keep references
  keepRef.current.handleOpenedMode = handleOpenedMode;

  // provides ref
  useImperativeHandle(ref, () => popperRef.current!);

  const popoverContextValueSetOpened = popoverContextValue.setOpened;
  useEffect(() => {
    popoverContextValueSetOpened!(opened);
  }, [opened, popoverContextValueSetOpened]);

  const popoverContextValueSetKeepMount = popoverContextValue.setKeepMount;
  useEffect(() => {
    popoverContextValueSetKeepMount!(keepMount);
  }, [keepMount, popoverContextValueSetKeepMount]);

  // set props
  useEffect(() => setOpened(openedProp), [openedProp]);
  useEffect(() => setKeepMount(keepMountProp), [keepMountProp]);

  // handle toggle open popover in un-controlled mode
  useEffect(() => {
    // because this useEffect handle for un-controlled mode only
    // if controlled mode, we just ignore it
    if (isBoolean(openedProp)) return;

    const triggerElement = popperRef.current?.triggerElement;
    const handleClick = () => keepRef.current?.handleOpenedMode(!opened);

    triggerElement?.addEventListener('click', handleClick);
    return () => triggerElement?.removeEventListener('click', handleClick);
  }, [opened, openedProp]);

  // handle mousedown outside
  useMouseDownOutside(opened, storage, popperRef, popoverInnerRef, keepRef);

  // handle scroll to close popup
  useCloseOnScroll(opened, storage, popoverInnerRef, keepRef);

  return (
    <PopoverContext.Provider value={popoverContextValue}>
      <Popper
        ref={popperRef}
        opened={opened}
        containerClassName={classnames(
          classes.popover.container,
          `size-${size}`,
          containerClassName,
          popoverContextValue.value
        )}
        arrowClassName={classnames(classes.popover.arrow, arrowClassName)}
        element={
          <div
            ref={popoverInnerRef}
            className={classnames(classes.popover.inner, popoverInnerClassName)}
            data-testid={genAmtId(dataTestId, 'dls-popover', 'Popover')}
          >
            {element}
          </div>
        }
        keepMount={keepMount}
        {...props}
      />
    </PopoverContext.Provider>
  );
};

const PopoverForwardedRef = React.forwardRef<PopperRef, PopoverProps>(Popover);

const PopoverHOC: React.RefForwardingComponent<PopperRef, PopoverProps> = (
  props,
  ref
) => {
  return (
    <StorageProvider>
      <PopoverForwardedRef ref={ref} {...props}></PopoverForwardedRef>
    </StorageProvider>
  );
};

export { usePopoverContext };
export default React.forwardRef<PopperRef, PopoverProps>(PopoverHOC);
