import React from 'react';
import '@testing-library/jest-dom';
import { fireEvent, render } from '@testing-library/react';
import useMouseDownOutside, {
  MouseDownOutsideHook
} from './useMouseDownOutside';

const MockComponent: React.FC<{ args: Parameters<MouseDownOutsideHook> }> = ({
  args
}) => {
  useMouseDownOutside(...args);
  return null;
};

describe('test mousedown outside', () => {
  it('should use default value', () => {
    const handleOpenedMode = jest.fn();
    render(
      <MockComponent
        args={[
          true,
          {
            values: {
              find: () => null
            }
          },
          { current: { state: {} } },
          {
            current: {
              contains: () => false
            }
          },
          {
            current: {
              handleOpenedMode
            }
          }
        ]}
      />
    );
    fireEvent.mouseDown(window);

    expect(handleOpenedMode).toBeCalled();
  });
});
