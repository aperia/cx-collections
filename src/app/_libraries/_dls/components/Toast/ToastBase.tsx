import React, { useLayoutEffect, useRef, useState } from 'react';
import { CSSTransition } from 'react-transition-group';

// components
import { Toast, ToastProps } from 'react-bootstrap';
import ToastBody from './ToastBody';
import TruncateText from '../TruncateText';

// helpers
import classnames from 'classnames';
import isFunction from 'lodash.isfunction';

export interface ToastBaseProps extends ToastProps {
  dataId: string;
  from?: number;
  to?: number;
  message?: string;
  delay?: number;
  duration?: number;
  autohide?: boolean;
  resizable?: boolean;
  closeButton?: boolean;
  isTopDirection?: boolean;
  type?: 'attention' | 'success' | 'warning' | 'error';
  height?: number;
  anchor?: HTMLDivElement | null;
  onClose?: () => void;
}

const ToastBase: React.FC<ToastBaseProps> = ({
  dataId,
  from = 0,
  to = 24,
  delay = 3000,
  duration = 250,
  autohide: autoHideProp = true,
  closeButton = true,
  isTopDirection,
  resizable = false,
  type = 'success',
  message = '',
  anchor,
  onClose,
  ...props
}) => {
  const toastRef = useRef<HTMLDivElement | null>(null);
  const parentRef = useRef<HTMLElement | null>(null);

  const [show, setShow] = useState(false);
  const [outTo, setOutTo] = useState(0);
  const [autoHide, setAutoHide] = useState(autoHideProp);

  const handleOnEnter = (element: HTMLElement) => {
    if (!parentRef.current) {
      parentRef.current = element.parentElement;
    }

    if (anchor) return anchor.append(element);
    document.body.append(element);
  };

  const handleOnExited = (element: HTMLElement) => {
    parentRef.current?.append(element);

    // call close after done running animation
    if (isFunction(onClose)) onClose();
  };

  const handleClose = () => {
    const toastElement = toastRef.current! as HTMLDivElement;
    const { top, bottom } = toastElement.getBoundingClientRect();

    if (isTopDirection) {
      setOutTo(to - top);
    } else {
      setOutTo(to + bottom - window.innerHeight);
    }

    setShow(false);
  };

  const handleMouseEnter = () => setAutoHide(false);

  const handleMouseLeave = () => {
    if (!autoHideProp) return;
    setAutoHide(true);
  };

  // wait for CSSTransition render before set [show=true]
  useLayoutEffect(() => setShow(true), []);

  return (
    <CSSTransition
      in={show}
      timeout={duration}
      mountOnEnter
      unmountOnExit
      onEnter={handleOnEnter}
      onExited={handleOnExited}
    >
      {(state) => {
        return (
          <Toast
            ref={toastRef}
            show={true}
            id={dataId}
            delay={delay}
            autohide={autoHide}
            bsPrefix={classnames('toast toast__animation', type, state)}
            style={{
              animationDuration: `${duration}ms`,
              animationFillMode: 'forwards',
              '--toast-from': `${from}px`,
              '--toast-to': `${to}px`,
              '--toast-out-from': `${to}px`,
              '--toast-out-to': `${outTo}px`
            }}
            onClose={handleClose}
            onMouseEnter={handleMouseEnter}
            onMouseLeave={handleMouseLeave}
            {...props}
          >
            <ToastBody
              type={type}
              onClose={handleClose}
              showCloseIcon={closeButton}
            >
              <TruncateText lines={2} title={message} resizable={resizable}>
                {message}
              </TruncateText>
            </ToastBody>
          </Toast>
        );
      }}
    </CSSTransition>
  );
};

export default ToastBase;
