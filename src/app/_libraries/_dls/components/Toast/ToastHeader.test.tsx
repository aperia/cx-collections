import React from 'react';
import '@testing-library/jest-dom';
import { render } from '@testing-library/react';

// components
import ToastHeader from './ToastHeader';
import { queryByClass } from '../../test-utils';

describe('Render', () => {
  it('should render', () => {
    const { baseElement } = render(<ToastHeader />);

    expect(
      queryByClass(baseElement as HTMLElement, /toast-header/)
    ).toBeInTheDocument();
  });
});
