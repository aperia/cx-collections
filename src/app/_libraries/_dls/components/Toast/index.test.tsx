import React, { MutableRefObject } from 'react';
import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';

// components
import Toast, { ToastDataType } from '.';
import { ToastBaseProps } from './ToastBase';

jest.mock('./ToastBase', () => ({ onClose }: ToastBaseProps) => {
  return (
    <div>
      ToastBase<button onClick={onClose}>ToastBase_onClose</button>
    </div>
  );
});

const mockRef: MutableRefObject<HTMLDivElement | null> = { current: null };

const toastSuccess: ToastDataType = {
  id: 'id_success',
  message: 'message success',
  type: 'success'
};

describe('Render', () => {
  it('should not render when data empty', () => {
    render(
      <Toast ref={mockRef} data={[]} onClose={jest.fn()} direction="top-end" />
    );

    expect(screen.queryByText('ToastBase')).toBeNull();
  });

  it('should render', () => {
    const onClose = jest.fn();
    render(<Toast ref={mockRef} data={[toastSuccess]} onClose={onClose} />);

    screen.getByText('ToastBase_onClose').click();

    expect(onClose).toBeCalled();
  });
});
