import React, { ReactElement } from 'react';
import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';

// components
import ToastBase from './ToastBase';

// utils
import { queryByClass } from '../../test-utils';

// mocks
import { elementId } from '../../test-utils/mocks/MockCSSTransition';

// mocks
import '../../test-utils/mocks/mockCanvas';
import userEvent from '@testing-library/user-event';

jest.mock('react-transition-group', () =>
  jest.requireActual('../../test-utils/mocks/MockCSSTransition')
);

const renderComponent = (element: ReactElement) => {
  const wrapper = render(<div className="dls-toast-group">{element}</div>);

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement
  };
};

const to = 0;
const from = 0;
const duration = 200;

describe('Render', () => {
  it('should render', () => {
    renderComponent(<ToastBase dataId={'id'} />);

    expect(screen.getByRole('alert')).toBeInTheDocument();
  });

  it('should render with anchor', () => {
    const mockAppend = jest.fn();
    const anchorElement = { append: mockAppend } as unknown as HTMLDivElement;

    renderComponent(<ToastBase dataId={'id'} anchor={anchorElement} />);

    expect(screen.getByRole('alert')).toBeInTheDocument();
    expect(mockAppend).toBeCalledWith(
      expect.objectContaining({ id: elementId })
    );
  });

  it('should render with message', () => {
    const message = 'message';
    renderComponent(<ToastBase dataId={'id'} message={message} />);

    expect(screen.getByRole('alert')).toBeInTheDocument();
    expect(screen.getByText(message)).toBeInTheDocument();
  });

  describe('render with type', () => {
    it('type success', () => {
      const { wrapper } = renderComponent(
        <ToastBase dataId={'id'} type="success" />
      );

      expect(wrapper.baseElement.querySelector('.success')).toBeInTheDocument();
    });

    it('type warning', () => {
      const { wrapper } = renderComponent(
        <ToastBase dataId={'id'} type="warning" />
      );

      expect(wrapper.baseElement.querySelector('.warning')).toBeInTheDocument();
    });

    it('type error', () => {
      const { wrapper } = renderComponent(
        <ToastBase dataId={'id'} type="error" />
      );

      expect(wrapper.baseElement.querySelector('.error')).toBeInTheDocument();
    });
  });
});

describe('Actions', () => {
  describe('onClose', () => {
    it('in Top direction', () => {
      const onClose = jest.fn();

      const { baseElement } = renderComponent(
        <ToastBase
          dataId={'id'}
          onClose={onClose}
          duration={duration}
          to={to}
          from={from}
          isTopDirection
        />
      );

      queryByClass(baseElement, /icon-close/)!.click();
      expect(onClose).toBeCalled();
    });

    it('not in Top direction', () => {
      const onClose = jest.fn();

      const { baseElement } = renderComponent(
        <ToastBase
          dataId={'id'}
          onClose={onClose}
          duration={duration}
          to={to}
          from={from}
        />
      );

      queryByClass(baseElement, /icon-close/)!.click();
      expect(onClose).toBeCalled();
    });
  });
  describe('hover', () => {
    it('hover without autoHideProp', () => {
      const onClose = jest.fn();

      const { baseElement } = renderComponent(
        <ToastBase
          dataId={'id'}
          onClose={onClose}
          duration={duration}
          to={to}
          from={from}
          isTopDirection
        />
      );

      const toastElement = baseElement.querySelector('.toast__animation')!;
      userEvent.hover(toastElement);
      userEvent.unhover(toastElement);

      queryByClass(baseElement, /icon-close/)!.click();
      expect(onClose).toBeCalled();
    });

    it('hover with autoHideProp', () => {
      const onClose = jest.fn();

      const { baseElement } = renderComponent(
        <ToastBase
          dataId={'id'}
          onClose={onClose}
          duration={duration}
          to={to}
          from={from}
          isTopDirection
          autohide={false}
        />
      );

      const toastElement = baseElement.querySelector('.toast__animation')!;
      userEvent.hover(toastElement);
      userEvent.unhover(toastElement);

      queryByClass(baseElement, /icon-close/)!.click();
      expect(onClose).toBeCalled();
    });
  });
});
