import React from 'react';

// testing library
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import {
  clearMockMutationObserver,
  createMockMutationObserver
} from '../../test-utils/mocks/mockMutationObserver';

// hook
import useOverlap from './useOverlap';

const Wrapper: React.FC<any> = (hookArgs) => {
  useOverlap(hookArgs as any);

  return <div>Wrapper</div>;
};

afterAll(clearMockMutationObserver);
createMockMutationObserver([]);

describe('useOverlap', () => {
  it('render with placement top-start', () => {
    const { rerender, queryByText } = render(<Wrapper opened={false} />);

    rerender(
      <Wrapper
        opened
        popperContainer={{
          className: ''
        }}
      />
    );

    rerender(
      <Wrapper
        opened
        popperContainer={{
          className: 'dls-tooltip-container',
          previousSibling: undefined
        }}
      />
    );

    rerender(
      <Wrapper
        opened
        popperContainer={{
          className: 'dls-tooltip-container',
          previousSibling: {
            getAttribute: () => 'top-start'
          }
        }}
        popperInstance={{
          setOptions: () => undefined
        }}
      />
    );

    rerender(
      <Wrapper
        opened
        popperContainer={{
          className: 'dls-tooltip-container',
          previousSibling: {
            getAttribute: () => 'bottom-start'
          }
        }}
        popperInstance={{
          setOptions: () => undefined
        }}
      />
    );

    expect(queryByText('Wrapper')).toBeInTheDocument();
  });
});
