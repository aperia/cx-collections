import { useEffect } from 'react';

export interface Options {
  popperContainer: HTMLElement;
  opened: boolean;
}

export interface OverlapHook {
  (Options: Options): void;
}

const usePreventClickOnTooltip: OverlapHook = ({ popperContainer, opened }) => {
  useEffect(() => {
    if (!opened) return;

    const isTooltip =
      popperContainer?.className.indexOf('dls-tooltip-container') !== -1;
    if (!isTooltip) return;

    const handlePreventClick = (event: MouseEvent) => {
      event.stopPropagation();
    };

    popperContainer?.addEventListener('mousedown', handlePreventClick, true);
    return () => {
      popperContainer?.removeEventListener(
        'mousedown',
        handlePreventClick,
        true
      );
    };
  }, [opened, popperContainer]);
};

export default usePreventClickOnTooltip;
