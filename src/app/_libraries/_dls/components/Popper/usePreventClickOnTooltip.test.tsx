import React from 'react';

// testing library
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import {
  clearMockMutationObserver,
  createMockMutationObserver
} from '../../test-utils/mocks/mockMutationObserver';

// hook
import usePreventClickOnTooltip from './usePreventClickOnTooltip';

const Wrapper: React.FC<any> = (hookArgs) => {
  usePreventClickOnTooltip(hookArgs as any);

  return <div>Wrapper</div>;
};

afterAll(clearMockMutationObserver);
createMockMutationObserver([]);

describe('usePreventClickOnTooltip', () => {
  it('', () => {
    const { rerender, queryByText } = render(<Wrapper opened={false} />);

    rerender(
      <Wrapper
        opened
        popperContainer={{
          addEventListener: (type: string, callback: (event: any) => void) => {
            callback({ stopPropagation: () => undefined });
          },
          removeEventListener: () => undefined,
          className: ''
        }}
      />
    );

    rerender(
      <Wrapper
        opened
        popperContainer={{
          addEventListener: (type: string, callback: (event: any) => void) => {
            callback({ stopPropagation: () => undefined });
          },
          removeEventListener: () => undefined,
          className: 'dls-tooltip-container'
        }}
      />
    );

    expect(queryByText('Wrapper')).toBeInTheDocument();
  });
});
