import get from 'lodash.get';

// Popper arrow is always centered against the trigger element
// For IconButton, we will custom this arrow by using css
// for the remaining cases, we will custom this arrow by using js
const adjustArrowModifier = {
  name: 'adjust-arrow',
  enabled: true,
  phase: 'write',
  fn: ({ state }: any) => {
    const { placement } = state;
    const popper = get(state.elements, 'popper') as HTMLElement;
    const arrow = get(state.elements, 'arrow') as HTMLElement;

    if (popper && popper.classList.contains('icon-button')) return;

    switch (placement) {
      case 'top-start':
      case 'bottom-start':
        arrow.style.transform = 'unset';
        arrow.style.left = '20px';
        break;
      case 'left-start':
      case 'right-start':
      case 'left-end':
      case 'right-end':
        arrow.style.transform = 'unset';
        arrow.style.top = '20px';
        break;
      case 'top-end':
      case 'bottom-end':
        arrow.style.transform = 'unset';
        arrow.style.left = 'calc(100% - 20px)';
        break;
    }

    return state;
  }
};

export default adjustArrowModifier;
