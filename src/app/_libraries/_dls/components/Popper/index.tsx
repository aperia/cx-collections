import ReactDOM from 'react-dom';
import React, { useRef, useEffect, useState, useImperativeHandle } from 'react';

// components/types/hooks
import { createPopper, Instance } from '@popperjs/core';
import { PopperRef, PopperProps } from './types';
import useOverlap from './useOverlap';

// adjustArrowModifier
import adjustArrowModifier from './adjustArrowModifier';

// utils
import classnames from 'classnames';
import { className, genAmtId } from '../../utils';
import usePreventClickOnTooltip from './usePreventClickOnTooltip';

/**
 * - Popper is a lower-level construct that is leveraged by the @popperjs/core
 * - This component shared for those components in the project:
 * -      Tooltip
 * -      Popover
 */
const Popper: React.RefForwardingComponent<PopperRef, PopperProps> = (
  {
    opened: openedProp = false,
    element,
    keepMount = false,
    placement = 'top',
    modifiers,
    onFirstUpdate,
    strategy = 'absolute',
    triggerClassName,
    containerClassName,
    arrowClassName,
    children,
    dataTestId
  },
  ref
) => {
  const triggerRef = useRef<HTMLSpanElement | null>(null);
  const popperContainerRef = useRef<HTMLSpanElement | null>(null);
  const popperArrowRef = useRef<HTMLSpanElement | null>(null);
  const instanceRef = useRef<Instance | null>(null);
  const previousElementRef = useRef<React.ReactNode>(null);
  const previousContainerClassNameRef = useRef<string[]>([]);
  const previousArrowClassNameRef = useRef<string[]>([]);
  const sorterRef = useRef<(left: string, right: string) => number>(
    (left, right) => {
      if (left > right) return 1;
      if (left < right) return -1;
      return 0;
    }
  );

  const [portal, setPortal] = useState<React.ReactNode | null>(null);
  const [opened, setOpened] = useState(openedProp);

  useImperativeHandle(
    ref,
    () =>
      ({
        ...instanceRef.current,
        triggerElement: triggerRef.current
      } as PopperRef)
  );

  // set props
  useEffect(() => setOpened(openedProp), [openedProp]);

  // handle create popper instance
  useEffect(() => {
    if (!opened) return;

    // create popper container DOM element
    if (!popperContainerRef.current) {
      const popperContainerElement = document.createElement('span');
      const baseClasses = [className.popper.CONTAINER];
      const additionalClasses = containerClassName?.split(' ') || [];

      baseClasses.push(...additionalClasses);
      popperContainerElement.classList.add(...baseClasses);

      // we need to keep the current className to compare with the next one
      previousContainerClassNameRef.current = additionalClasses.sort(
        sorterRef.current
      );
      popperContainerRef.current = popperContainerElement;
    }

    // create arrow DOM element
    if (!popperArrowRef.current) {
      const arrowElement = document.createElement('span');
      const baseClasses = [className.popper.ARROW];
      const additionalClasses = arrowClassName?.split(' ') || [];

      baseClasses.push(...additionalClasses);
      arrowElement.classList.add(...baseClasses);
      popperContainerRef.current.appendChild(arrowElement);

      // we need to keep the current className to compare with the next one
      previousArrowClassNameRef.current = additionalClasses.sort(
        sorterRef.current
      );
      popperArrowRef.current = arrowElement;
    }

    // create popper instance
    if (!instanceRef.current) {
      instanceRef.current = createPopper(
        triggerRef.current!,
        popperContainerRef.current
      );
    }
  }, [opened, containerClassName, arrowClassName]);

  // handle change class for popperContainerRef.current and popperArrowRef.current
  useEffect(() => {
    if (!opened || !popperContainerRef.current || !popperArrowRef.current) {
      return;
    }

    const sortString = (input = '') => {
      return input.split(' ').sort(sorterRef.current).join(' ');
    };

    const containerClassNameSorted = sortString(containerClassName);
    const arrowClassNameSorted = sortString(arrowClassName);

    // remove previous className and add new className if detect different
    if (
      containerClassNameSorted !==
      previousContainerClassNameRef.current.join(' ')
    ) {
      popperContainerRef.current.classList.remove(
        ...previousContainerClassNameRef.current
      );
      const containerClassNameSortedSplit = containerClassNameSorted.split(' ');
      popperContainerRef.current.classList.add(
        ...containerClassNameSortedSplit
      );
      previousContainerClassNameRef.current = containerClassNameSortedSplit;
    }

    // remove previous className and add new className if detect different
    if (arrowClassNameSorted !== previousArrowClassNameRef.current.join(' ')) {
      popperArrowRef.current.classList.remove(
        ...previousArrowClassNameRef.current
      );
      const arrowClassNameSortedSplit = arrowClassNameSorted.split(' ');
      popperArrowRef.current.classList.add(...arrowClassNameSortedSplit);
      previousArrowClassNameRef.current = arrowClassNameSortedSplit;
    }
  }, [opened, containerClassName, arrowClassName]);

  // handle set options into popper instance
  useEffect(() => {
    if (!opened || !instanceRef.current) return;

    instanceRef.current.setOptions({
      modifiers: [
        {
          name: 'arrow',
          options: {
            element: popperArrowRef.current
          }
        },
        adjustArrowModifier,
        ...(modifiers || [])
      ],
      placement,
      onFirstUpdate,
      strategy
    });
  }, [opened, modifiers, placement, onFirstUpdate, strategy]);

  // handle append/remove popper container DOM element at the end of body
  useEffect(() => {
    // if set opened is true on first time render, popper will be wrong position
    if (!opened) return popperContainerRef.current?.remove();

    document.body.append(popperContainerRef.current!);

    if (previousElementRef.current !== element) {
      const portalElement = ReactDOM.createPortal(
        element,
        popperContainerRef.current!
      );

      previousElementRef.current = element;
      setPortal(portalElement);
    }
  }, [opened, element]);

  // handle non keepMount component
  useEffect(() => {
    if (
      keepMount ||
      opened ||
      !popperContainerRef.current ||
      !instanceRef.current
    ) {
      return;
    }

    setPortal(null);

    popperContainerRef.current.remove();
    popperContainerRef.current = null;
    popperArrowRef.current = null;

    instanceRef.current.destroy();
    instanceRef.current = null;
  }, [keepMount, opened]);

  // destroy popper instance when unMount
  useEffect(
    () => () => {
      instanceRef.current?.destroy();
    },
    []
  );

  // use overlap
  useOverlap({
    opened,
    popperContainer: popperContainerRef.current!,
    popperInstance: instanceRef.current!
  });

  // use prevent click on tooltip
  usePreventClickOnTooltip({
    opened,
    popperContainer: popperContainerRef.current!
  });

  return (
    <>
      <span
        ref={triggerRef}
        className={classnames(className.popper.TRIGGER, triggerClassName)}
        data-testid={genAmtId(dataTestId!, 'dls-popper', 'Popper')}
      >
        {children}
      </span>
      {portal}
    </>
  );
};

export * from './types';
export default React.forwardRef<PopperRef, PopperProps>(Popper);
