import adjustArrowModifier from './adjustArrowModifier';

describe('test function', () => {
  it('popper has class icon-button', () => {
    const state = {
      placement: 'top-start',
      elements: {
        popper: {
          classList: {
            contains: (classname: string) => classname === 'icon-button'
          }
        }
      }
    };
    const stateResult = adjustArrowModifier.fn({ state });

    expect(stateResult).toBeUndefined();
  });

  it('top-start', () => {
    const state = {
      placement: 'top-start',
      elements: { arrow: { style: {} } }
    };
    const stateResult = adjustArrowModifier.fn({ state });

    expect(stateResult).toEqual({
      placement: 'top-start',
      elements: { arrow: { style: { transform: 'unset', left: '20px' } } }
    });
  });

  it('left-start', () => {
    const state = {
      placement: 'left-start',
      elements: { arrow: { style: {} } }
    };
    const stateResult = adjustArrowModifier.fn({ state });

    expect(stateResult).toEqual({
      placement: 'left-start',
      elements: { arrow: { style: { transform: 'unset', top: '20px' } } }
    });
  });

  it('top-end', () => {
    const state = {
      placement: 'top-end',
      elements: { arrow: { style: {} } }
    };
    const stateResult = adjustArrowModifier.fn({ state });

    expect(stateResult).toEqual({
      placement: 'top-end',
      elements: {
        arrow: { style: { transform: 'unset', left: 'calc(100% - 20px)' } }
      }
    });
  });
});
