import React, {
  useState,
  useImperativeHandle,
  useEffect,
  useRef,
  MouseEvent
} from 'react';

// component
import Label from '../Label';
import Tooltip from '../Tooltip';

// helper
import { classes, extendsEvent, genAmtId, truncateTooltip } from '../../utils';
import classnames from 'classnames';
import isFunction from 'lodash.isfunction';
import isString from 'lodash.isstring';
import pick from 'lodash.pick';
import unset from 'lodash.unset';
import { nanoid } from 'nanoid';

// types
import { TextAreaProps } from './types';
import { useBlurOnScroll } from '../../hooks';

const TextArea: React.RefForwardingComponent<
  HTMLTextAreaElement,
  TextAreaProps
> = (
  {
    value: valueProp,
    onChange,

    onFocus,
    onBlur,

    label,

    id,
    name,
    required,
    error,
    errorTooltipProps,

    readOnly,
    disabled,
    placeholder,
    autoFocus,

    dataTestId,
    ...props
  },
  ref
) => {
  // handle eslint no-unused-var, can't destructor params above
  unset(props, 'defaultValue');

  // states
  const [value, setValue] = useState(valueProp);
  const [focus, setFocus] = useState<boolean>();

  // check should show error or not
  const { message, status } = pick(error, 'message', 'status');
  const isOpenedTooltip = Boolean(status && message && focus);

  // check should floating label or not
  const floating = (focus && !readOnly) || value;

  // refs
  const textareaRef = useRef<HTMLTextAreaElement | null>(null);
  const floatingLabelRef = useRef<HTMLSpanElement | null>(null);
  const selectionStartRef = useRef(-1);

  const handleOnChange = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    const { value: nextValue, selectionStart } = event.target;
    selectionStartRef.current = selectionStart;

    let nextEvent = extendsEvent({} as any, 'target', {
      name,
      id,
      value: nextValue
    });
    nextEvent = extendsEvent(nextEvent, null, { value: nextValue });

    isFunction(onChange) && onChange(nextEvent);

    // controlled
    if (isString(valueProp)) return;

    // uncontrolled
    setValue(nextValue);
  };

  const handleOnFocus = () => {
    let nextEvent = extendsEvent({} as any, 'target', {
      name,
      id,
      value
    });
    nextEvent = extendsEvent(nextEvent, null, { value });

    isFunction(onFocus) && onFocus(nextEvent);
    setFocus(true);
  };

  const handleOnBlur = () => {
    let nextEvent = extendsEvent({} as any, 'target', {
      name,
      id,
      value
    });
    nextEvent = extendsEvent(nextEvent, null, { value });

    isFunction(onBlur) && onBlur(nextEvent);
    setFocus(false);
  };

  const handleOnMouseDownLabel = (event: MouseEvent) => {
    event.preventDefault();
    textareaRef.current?.focus();
  };

  // provides ref
  useImperativeHandle(ref, () => textareaRef.current!, []);

  // add blur on scroll if Tooltip is opened && error && scrolled
  useBlurOnScroll(textareaRef.current!, Boolean(status && focus));

  // set props
  useEffect(() => setValue(valueProp), [valueProp]);

  // update selection start
  useEffect(() => {
    if (!textareaRef.current || selectionStartRef.current === -1) return;

    textareaRef.current.selectionStart = selectionStartRef.current;
    textareaRef.current.selectionEnd = selectionStartRef.current;
    selectionStartRef.current = -1;
  }, [value]);

  // handle truncate tooltip label
  useEffect(() => truncateTooltip(floatingLabelRef.current!), [label]);

  const isRequired = required && !readOnly && !disabled;

  const containerId = genAmtId(dataTestId, 'dls-text-area', 'TextArea');

  return (
    <Tooltip
      opened={isOpenedTooltip}
      element={message}
      variant="error"
      placement="top-start"
      triggerClassName="d-block"
      dataTestId={genAmtId(containerId, 'tooltip', 'TextArea')}
      {...errorTooltipProps}
    >
      <div
        className={classnames({
          [classes.textarea.container]: true,
          [classes.state.focused]: focus,
          [classes.state.readOnly]: readOnly,
          [classes.state.disabled]: disabled,
          [classes.state.error]: status,
          [classes.state.floating]: floating
        })}
        data-testid={containerId}
      >
        <div className={classes.inputContainer.container}>
          {label && (
            <Label
              ref={floatingLabelRef}
              asterisk={isRequired}
              error={status}
              dataTestId={genAmtId(containerId, 'label', 'TextArea')}
              onMouseDown={handleOnMouseDownLabel}
            >
              <span className="text-truncate">{label}</span>
            </Label>
          )}
          <textarea
            ref={textareaRef}
            value={value}
            onChange={handleOnChange}
            onFocus={handleOnFocus}
            onBlur={handleOnBlur}
            placeholder={placeholder}
            disabled={disabled}
            readOnly={readOnly}
            autoFocus={autoFocus}
            autoComplete={nanoid()}
            data-testid={genAmtId(containerId, 'input', 'TextArea')}
            {...props}
          />
        </div>
      </div>
    </Tooltip>
  );
};

export * from './types';
export default React.forwardRef<HTMLTextAreaElement, TextAreaProps>(TextArea);
