import React from 'react';

// test
import '@testing-library/jest-dom';
import { render } from '@testing-library/react';

// components
import GroupButton from '../GroupButton';
import Button from '.';
import { Icon, Tooltip } from '..';
import Popover from '../Popover';
import userEvent from '@testing-library/user-event';

describe('test render', () => {
  it('should render', () => {
    const ref = React.createRef<HTMLButtonElement>();
    const { queryByText } = render(<Button ref={ref}>Button</Button>);

    expect(queryByText('Button')).toBeInTheDocument();
  });

  it('should render with other sizes', () => {
    const { container } = render(<Button size="sm">Button</Button>);
    expect(container.querySelector('.btn-sm')).toBeInTheDocument();
  });

  it('should render with loading', () => {
    const { container } = render(<Button loading>Button</Button>);
    expect(container.querySelector('.loading')).toBeInTheDocument();
  });

  it('should render with selected', () => {
    const { container } = render(<Button selected>Button</Button>);
    expect(container.querySelector('.active')).toBeInTheDocument();
  });
});

describe('test action', () => {
  it('should handle click', () => {
    const handleOnClick = jest.fn();
    const { container } = render(
      <Button onClick={handleOnClick} size="sm">
        Button
      </Button>
    );
    const button = container.querySelector('.btn')!;
    userEvent.click(button);

    expect(handleOnClick).toBeCalled();
  });

  it('should call handle click with GroupButton', () => {
    const handleOnClick = jest.fn();
    const { container } = render(
      <GroupButton>
        <Button onClick={handleOnClick} size="sm">
          Button
        </Button>
      </GroupButton>
    );
    const button = container.querySelector('.btn')!;
    userEvent.click(button);

    expect(handleOnClick).toBeCalled();
  });

  it('should call handle click with GroupButton controlled mode', () => {
    const handleOnClick = jest.fn();

    const { container } = render(
      <GroupButton>
        <Button onClick={handleOnClick} selected size="sm">
          Button
        </Button>
      </GroupButton>
    );
    const button = container.querySelector('.btn')!;
    userEvent.click(button);

    expect(handleOnClick).toBeCalled();
  });
});

describe('test effect', () => {
  it('should add class if Button is IconButton and inside Tooltip', () => {
    const { baseElement, container } = render(
      <Tooltip element={<div>Tooltip element</div>}>
        <Button variant="icon-primary">
          <Icon name="edit" />
        </Button>
      </Tooltip>
    );

    const trigger = container.querySelector('.icon')!;
    userEvent.hover(trigger);

    expect(
      baseElement.querySelector('.dls-tooltip-container.icon-button')
    ).toBeInTheDocument();

    userEvent.unhover(trigger);
  });

  it('should not add class if Button is not IconButton and inside Tooltip', () => {
    const { baseElement, queryByText } = render(
      <Tooltip element={<div>Tooltip element</div>}>
        <Button variant="icon-primary">Trigger</Button>
      </Tooltip>
    );

    const trigger = queryByText('Trigger')!;
    userEvent.hover(trigger);

    expect(
      baseElement.querySelector('.dls-tooltip-container.icon-button')
    ).not.toBeInTheDocument();

    userEvent.unhover(trigger);
  });

  it('should add class if Button is IconButton and inside Popover', () => {
    const { baseElement, container } = render(
      <Popover element={<div>Popover element</div>}>
        <Button variant="icon-primary">
          <Icon name="edit" />
        </Button>
      </Popover>
    );

    const trigger = container.querySelector('.icon')!;
    userEvent.click(trigger);

    expect(
      baseElement.querySelector('.dls-popover-container.icon-button')
    ).toBeInTheDocument();

    userEvent.click(trigger);
  });

  it('should not add class if Button is not IconButton and inside Tooltip', () => {
    const { baseElement, queryByText } = render(
      <Popover element={<div>Popover element</div>}>
        <Button variant="icon-primary">Trigger</Button>
      </Popover>
    );

    const trigger = queryByText('Trigger')!;
    userEvent.click(trigger);

    expect(
      baseElement.querySelector('.dls-popover-container.icon-button')
    ).not.toBeInTheDocument();

    userEvent.click(trigger);
  });

  it('should ignore if Button is inside Popover has className is .icon-button', () => {
    const { baseElement, container } = render(
      <Tooltip
        element={
          <div>
            Tooltip element<Button>Button</Button>
          </div>
        }
      >
        <Button variant="icon-primary">
          <Icon name="edit" />
        </Button>
      </Tooltip>
    );

    const trigger = container.querySelector('.icon')!;
    userEvent.hover(trigger);

    expect(
      baseElement.querySelector('.dls-tooltip-container button')
    ).toBeInTheDocument();

    userEvent.unhover(trigger);
  });
});
