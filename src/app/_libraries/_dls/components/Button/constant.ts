const btnVariants = <const>[
  'primary',
  'secondary',
  'danger',
  'outline-primary',
  'outline-secondary',
  'outline-danger',
  'icon-primary',
  'icon-secondary',
  'icon-danger'
];
const btnSizes = <const>['lg', 'sm'];

export type BtnVariants = typeof btnVariants[number];
export type BtnSizes = typeof btnSizes[number];
