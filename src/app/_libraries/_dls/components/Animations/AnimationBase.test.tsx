import React, { MutableRefObject, ReactElement } from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';

// utils
import { queryByClass } from '../../test-utils';

// components
import AnimationBase from './AnimationBase';

jest.mock('react-transition-group', () =>
  jest.requireActual('../../test-utils/mocks/MockCSSTransition')
);

const mockRef: MutableRefObject<HTMLDivElement | null> = { current: null };

const renderComponent = (element: ReactElement) => {
  const wrapper = render(element);

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement
  };
};

describe('Render', () => {
  it('should not render', () => {
    const text = 'AnimationBase animation';
    renderComponent(
      <AnimationBase ref={mockRef}>
        <p>{text}</p>
      </AnimationBase>
    );

    expect(screen.queryByText(text)).toBeNull();
  });

  it('should render', () => {
    const text = 'AnimationBase animation';
    renderComponent(
      <AnimationBase ref={mockRef} in={true}>
        <p>{text}</p>
      </AnimationBase>
    );

    expect(screen.getByText(text)).toBeInTheDocument();
  });

  it('should render with custom className', () => {
    const text = 'AnimationBase animation';
    const { baseElement } = renderComponent(
      <AnimationBase
        in={true}
        animationCustomClassName="class-name"
        duration={200}
      >
        <p>{text}</p>
      </AnimationBase>
    );

    expect(queryByClass(baseElement, /class-name/)).toBeInTheDocument();
  });

  it('should update animation', () => {
    const duration = 2000;
    const { baseElement } = renderComponent(
      <AnimationBase in={true} duration={duration}>
        <p>AnimationBase animation</p>
      </AnimationBase>
    );

    expect(queryByClass(baseElement, /dls-animation/)).toBeInTheDocument();
    expect(queryByClass(baseElement, /dls-animation/)).toHaveStyle({
      'animation-duration': `${duration}ms`
    });
  });

  it('should render in anchor', () => {
    const mockAppend = jest.fn();
    const anchorElement = ({ append: mockAppend } as unknown) as HTMLDivElement;

    const { baseElement } = renderComponent(
      <AnimationBase in={true} anchor={anchorElement}>
        <p>AnimationBase animation</p>
      </AnimationBase>
    );

    expect(queryByClass(baseElement, /dls-animation/)).toBeInTheDocument();
  });
});
