import React, {
  forwardRef,
  RefForwardingComponent,
  useRef,
  useImperativeHandle
} from 'react';

import { className as classConst } from '../../utils';
import { AnimationProps } from './types';
import AnimationBase from './AnimationBase';

const Fade: RefForwardingComponent<
  HTMLDivElement,
  Omit<AnimationProps, 'direction'>
> = ({ animationCustomClassName, ...props }, ref) => {
  const containerRef = useRef<HTMLDivElement | null>(null);

  useImperativeHandle(ref, () => containerRef.current!);

  return (
    <AnimationBase
      ref={containerRef}
      animationCustomClassName={
        animationCustomClassName || classConst.animation.FADE
      }
      {...props}
    />
  );
};

export default forwardRef<HTMLDivElement, AnimationProps>(Fade);
