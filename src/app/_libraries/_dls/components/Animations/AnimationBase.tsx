import React, {
  useEffect,
  useState,
  useRef,
  forwardRef,
  cloneElement,
  useImperativeHandle,
  RefForwardingComponent
} from 'react';
import { CSSTransition } from 'react-transition-group';
import classnames from 'classnames';

import { className as classConst } from '../../utils';
import { AnimationProps } from './types';

const AnimationBase: RefForwardingComponent<HTMLDivElement, AnimationProps> = (
  {
    in: inProp = false,
    duration = 350,
    animationCustomClassName,
    children,

    ...props
  },
  ref
) => {
  const containerRef = useRef<HTMLDivElement | null>(null);
  const parentElementRef = useRef<HTMLElement | null>(null);

  const [rendered, setRendered] = useState(false);
  const [show, setShow] = useState(false);

  const handleOnEnter = (element: HTMLElement) => {
    if (!parentElementRef.current) {
      parentElementRef.current = element.parentElement;
    }
  };

  const handleOnExited = (element: HTMLElement) => {
    if (!parentElementRef.current) return;

    parentElementRef.current.append(element);
  };

  useImperativeHandle(ref, () => containerRef.current!);

  // When use [process.nextTick], unit test will throw error
  // "cannot read property 'body' of null". (don't know why)
  // Need to replace by using [rendered] state to prevent this error.
  //
  // [before]: will make error "cannot read property 'body' of null"
  // useEffect(() => process.nextTick(() => setShow(inProp)), [inProp]);
  //
  // [after]: fix error "cannot read property 'body' of null" when run unit test
  useEffect(() => {
    if (rendered) return setShow(inProp);
  }, [rendered, inProp]);

  useEffect(() => setRendered(true), []);

  return (
    <CSSTransition
      in={show}
      timeout={duration}
      mountOnEnter
      unmountOnExit
      onEnter={handleOnEnter}
      onExited={handleOnExited}
      {...props}
    >
      {(state) => {
        return cloneElement(children, {
          ...children.props,
          style: {
            animationDuration: `${duration}ms`,
            ...children.props.style
          },
          className: classnames(
            classConst.animation.CONTAINER,
            animationCustomClassName,
            state,
            children.props.className
          )
        });
      }}
    </CSSTransition>
  );
};

export default forwardRef<HTMLDivElement, AnimationProps>(AnimationBase);
