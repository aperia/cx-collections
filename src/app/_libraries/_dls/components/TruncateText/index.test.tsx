import React, { MutableRefObject } from 'react';
import '@testing-library/jest-dom';

// components
import TruncateText from '../TruncateText';

// rtl
import { render, screen } from '@testing-library/react';

jest.mock('../../utils', () => {
  const utilFunctions = jest.requireActual('../../utils');

  return { ...utilFunctions, getAvailableWidth: () => 1000 };
});

jest.mock('resize-observer-polyfill', () =>
  jest.requireActual('../../test-utils/mocks/MockResizeObserver')
);

const text = 'this is long text';

const mockRef: MutableRefObject<HTMLSpanElement | null> = { current: null };

const mockGetBoundingClientRect = (...args: number[]) => {
  let count = -1;
  const lastHeight = args[args.length - 1];

  Element.prototype.getBoundingClientRect = () => {
    count++;
    if (args.length === count) count = 0;
    return {
      bottom: 0,
      height: args[count] || lastHeight,
      left: 0,
      right: 0,
      top: 0,
      width: 0,
      x: 0,
      y: 0,
      toJSON: () => undefined
    };
  };
};

const mockGetComputedStyle = (
  lineHeight: number,
  textTransform = 'inherrit'
) => {
  global.window = Object.create(window);
  Object.defineProperty(window, 'getComputedStyle', {
    value: () => ({ lineHeight, textTransform })
  });
};

beforeEach(() => {
  // maxHeight: is used for [checkAndGetNodeTruncate] -> [div.getBoundingClientRect]
  const maxHeight = 300;

  // wordHeights: is used for [getTruncateText] -> [elementForRender.getBoundingClientRect]
  //   when calculate by words, Ex:
  //   [  100       |  100       |  200           |  300           ]
  //   [  100 < 200 |  100 < 200 |  200 = 200     | 300 > 200      ]
  //   [ 'this'     | 'this is'  | 'this is text' | 'this is text' ]
  const wordHeights = [100, 100, 200, 300];

  // letterHeights: is used for all [checkAndGetNodeTruncate] -> [div.getBoundingClientRect]
  //   when calculate by letters, Ex:
  //   [                  |  300           |  200           ]
  //   [  first slice     |  300 > 200     |  200 = 200     ]
  //   [ 'this is tex...' | 'this is te...'| 'this is t...' ]
  const letterHeights = [300, 200];

  mockGetBoundingClientRect(maxHeight, ...wordHeights, ...letterHeights);
  mockGetComputedStyle(200);
});

describe('Render', () => {
  it('should not truncate', () => {
    const onTruncate = jest.fn();

    // when lineHeight is greater than [maxHeight], text will be not truncated
    mockGetComputedStyle(400);

    const { rerender } = render(
      <TruncateText
        prefix={{ element: 'prefix' }}
        lines={0}
        className="class-name"
        onTruncate={onTruncate}
      >
        {text}
      </TruncateText>
    );

    rerender(
      <TruncateText
        suffix={{ element: 'prefix' }}
        lines={0}
        className="class-name"
        onTruncate={onTruncate}
      >
        {text}
      </TruncateText>
    );

    expect(onTruncate).toBeCalledWith(false);
    expect(screen.getByText(text)).toBeInTheDocument();
  });

  it('should not truncate when children is element', () => {
    const onTruncate = jest.fn();

    // when lineHeight is greater than [maxHeight], text will be not truncated
    mockGetComputedStyle(400);

    render(
      <TruncateText className="class-name" onTruncate={onTruncate}>
        <p>{text}</p>
      </TruncateText>
    );

    expect(onTruncate).not.toBeCalled();
    expect(screen.queryByText(text)).toBeNull();
  });

  it('will trigger onTruncate', () => {
    const onTruncate = jest.fn();

    render(
      <TruncateText
        ref={mockRef}
        className="class-name"
        onTruncate={onTruncate}
        autoShowTooltip={false}
        followRef={mockRef}
        resizable
        suffix={{ element: 'Suffix' }}
      >
        {text}
      </TruncateText>
    );

    expect(onTruncate).toBeCalledWith(false);
    expect(screen.queryByText(text)).not.toBeNull();
    expect(screen.queryByText('this is lo...')).not.toBeInTheDocument();
  });

  it('will trigger onTruncate 2', () => {
    mockGetComputedStyle(99);
    const onTruncate = jest.fn();

    render(
      <TruncateText
        ref={mockRef}
        className="class-name"
        onTruncate={onTruncate}
        autoShowTooltip={false}
        followRef={mockRef}
        resizable
        suffix={{ element: 'Suffix' }}
      >
        {text}
      </TruncateText>
    );

    expect(onTruncate).toBeCalledWith(true);
    expect(screen.queryByText(text)).toBeNull();
    expect(screen.queryByText('this is lo...')).not.toBeInTheDocument();
  });

  describe('will truncate with textTransform', () => {
    it('textTransform capitalize', () => {
      mockGetComputedStyle(200, 'capitalize');

      const onTruncate = jest.fn();

      render(
        <TruncateText
          className="class-name"
          onTruncate={onTruncate}
          autoShowTooltip={false}
          followRef={mockRef}
          prefix={{ element: 'Prefix' }}
        >
          {text}
        </TruncateText>
      );

      expect(onTruncate).toBeCalledWith(false);
      expect(screen.queryByText(text)).toBeNull();
      expect(screen.queryByText('This Is Lo...')).not.toBeInTheDocument();
    });

    it('textTransform capitalize 2', () => {
      mockGetComputedStyle(99);

      const onTruncate = jest.fn();

      render(
        <TruncateText
          className="class-name"
          onTruncate={onTruncate}
          autoShowTooltip={false}
          followRef={mockRef}
          prefix={{ element: 'Prefix' }}
        >
          {text}
        </TruncateText>
      );

      expect(onTruncate).toBeCalledWith(true);
      expect(screen.queryByText(text)).toBeNull();
      expect(screen.queryByText('This Is Lo...')).not.toBeInTheDocument();
    });

    it('textTransform uppercase', () => {
      mockGetComputedStyle(200, 'uppercase');

      const onTruncate = jest.fn();

      render(
        <TruncateText
          className="class-name"
          onTruncate={onTruncate}
          autoShowTooltip={false}
          followRef={mockRef}
        >
          {text}
        </TruncateText>
      );

      expect(onTruncate).toBeCalledWith(true);
      expect(screen.queryByText(text)).toBeNull();
      expect(screen.queryByText('THIS IS LO...')).toBeInTheDocument();
    });

    it('textTransform lowercase', () => {
      mockGetComputedStyle(200, 'lowercase');

      const onTruncate = jest.fn();

      render(
        <TruncateText
          className="class-name"
          onTruncate={onTruncate}
          autoShowTooltip={false}
          followRef={mockRef}
        >
          {text}
        </TruncateText>
      );

      expect(onTruncate).toBeCalledWith(true);
      expect(screen.queryByText(text)).toBeNull();
      expect(screen.queryByText('this is lo...')).toBeInTheDocument();
    });
  });

  it('will render with title', () => {
    render(
      <TruncateText className="class-name" title={text}>
        {text}
      </TruncateText>
    );

    expect(screen.getByTitle(text)).toBeInTheDocument();
  });

  it('will toggle title when render with more less', () => {
    render(
      <TruncateText
        className="class-name"
        title={text}
        ellipsisMoreText="more"
        ellipsisLessText="less"
      >
        {text}
      </TruncateText>
    );

    expect(screen.getByTitle(text)).toBeInTheDocument();

    screen.getByText(/more/).click();

    expect(screen.queryByTitle(text)).toBeNull();

    screen.getByText(/less/).click();

    expect(screen.queryByTitle(text)).toBeInTheDocument();
  });
});
