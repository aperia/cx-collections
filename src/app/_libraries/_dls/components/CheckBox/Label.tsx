import React, { DetailedHTMLProps, LabelHTMLAttributes } from 'react';

// utils
import classNames from 'classnames';
import { className } from '../../utils';

export interface LabelProps
  extends React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLLabelElement>,
    HTMLLabelElement
  > {}

const Label: React.ForwardRefRenderFunction<
  HTMLLabelElement,
  DetailedHTMLProps<LabelHTMLAttributes<HTMLLabelElement>, HTMLLabelElement>
> = ({ className: labelClassName, children, ...props }, ref) => {
  return (
    <label
      ref={ref}
      className={classNames(className.checkbox.LABEL, labelClassName)}
      {...props}
    >
      {children}
    </label>
  );
};

export default React.forwardRef<
  HTMLLabelElement,
  DetailedHTMLProps<LabelHTMLAttributes<HTMLLabelElement>, HTMLLabelElement>
>(Label);
