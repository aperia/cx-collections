import React, { useEffect, useImperativeHandle, useRef, useState } from 'react';

// utils
import isBoolean from 'lodash.isboolean';
import isFunction from 'lodash.isfunction';
import classNames from 'classnames';
import { className, genAmtId } from '../../utils';

// components
import { Form } from 'react-bootstrap';

export interface InputProps
  extends React.DetailedHTMLProps<
      React.InputHTMLAttributes<HTMLInputElement>,
      HTMLInputElement
    >,
    DLSId {
  indeterminate?: boolean;
  readOnly?: boolean;
  inputClassName?: string;
  disabled?: boolean;
  checked?: boolean;
  id?: string;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

const Input: React.ForwardRefRenderFunction<HTMLInputElement, InputProps> = (
  {
    onChange: onChangeProp,
    id,
    indeterminate,
    readOnly,
    disabled,
    checked: checkedProp,
    className: inputClassName,

    dataTestId,
    ...props
  },
  ref
) => {
  const inputRef = useRef<HTMLInputElement | null>(null);
  useImperativeHandle(ref, () => inputRef.current!);

  const [checked, setChecked] = useState(isBoolean(checkedProp) && checkedProp);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (readOnly) return;

    isFunction(onChangeProp) && onChangeProp(event);

    // uncontrolled
    !isBoolean(checkedProp) && setChecked((state) => !state);
  };

  useEffect(() => {
    if (!isBoolean(checkedProp)) return;

    setChecked(checkedProp);
  }, [checkedProp]);

  return (
    <input
      type="checkbox"
      id={id}
      disabled={disabled}
      readOnly={readOnly}
      className={classNames(
        className.checkbox.INPUT,
        { indeterminate },
        inputClassName
      )}
      checked={indeterminate ? false : checked}
      onChange={handleChange}
      ref={inputRef}
      data-testid={genAmtId(dataTestId, 'dls-checkbox-input', 'CheckBox.Input')}
      {...props}
    />
  );
};

Form.Check.Input = Input;

export default React.forwardRef<HTMLInputElement, InputProps>(Input);
