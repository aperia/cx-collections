import React from 'react';

import { queryByText, render, getByText } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';

// components
import TreeView, { TreeViewValue } from '.';
import Item from './Item';
import { set } from '../../lodash';

// data
const treeViewValue: TreeViewValue[] = [
  {
    id: 'item 1',
    label: '--- Item-1 ---',
    items: [
      {
        id: 'item 1.1',
        label: '--- Item-1.1 ---',
        items: [
          { id: 'item 1.1.1', label: '--- Item-1.1.1 ---' },
          { id: 'item 1.1.2', label: '--- Item-1.1.2 ---' }
        ]
      },
      {
        id: 'item 1.3',
        label: '--- Item-1.3 ---',
        items: [{ id: 'item 1.3.1', label: '--- Item-1.3.1 ---' }]
      }
    ]
  },
  {
    id: 'item 2',
    label: ''
  }
];

const getTreeElement = (container: HTMLElement) => {
  return container.querySelector('[class="dls-treeview"]') as HTMLDivElement;
};

describe('TreeView render', () => {
  it('should render empty tree if value is not an array', () => {
    const { container } = render(
      <TreeView ref={{ current: null }} variant="checkbox" />
    );

    const treeViewElement = getTreeElement(container);

    expect(treeViewElement.innerHTML).toBe('');
  });

  it('should render with defaultValue', () => {
    const { container, rerender } = render(
      <TreeView
        ref={{ current: null }}
        defaultValue={treeViewValue}
        variant="checkbox"
      />
    );

    rerender(<TreeView ref={{ current: null }} defaultValue={treeViewValue} />);

    const treeViewElement = getTreeElement(container);
    expect(treeViewElement).toBeInTheDocument();
  });

  it('should render with value', () => {
    const { container } = render(
      <TreeView value={treeViewValue} variant="checkbox" />
    );

    const treeViewElement = getTreeElement(container);
    expect(treeViewElement).toBeInTheDocument();
  });

  it('should render with highlighText', () => {
    const { container } = render(
      <TreeView value={treeViewValue} variant="checkbox" highlightText="item" />
    );

    const treeViewElement = getTreeElement(container);
    expect(treeViewElement).toBeInTheDocument();
  });

  it('should rerender with new value prop', () => {
    const { container, rerender } = render(
      <TreeView value={treeViewValue} variant="checkbox" />
    );

    const treeViewElement = getTreeElement(container);
    expect(treeViewElement).toBeInTheDocument();

    const newLabel = 'new label item 1';
    let nextTreeView = JSON.parse(JSON.stringify(treeViewValue));
    nextTreeView = set(nextTreeView, ['0', 'label'], newLabel);

    rerender(<TreeView value={nextTreeView} variant="checkbox" />);
    const element = queryByText(container, newLabel);
    expect(element).toBeInTheDocument();
  });

  it("don't should rerender if current value isEqual new value (deep compare)", () => {
    const { rerender, container } = render(
      <TreeView value={treeViewValue} variant="checkbox" />
    );

    rerender(<TreeView value={[...treeViewValue]} variant="checkbox" />);

    // we don't have any expect here, just to test deep compare in hook // set props
    getByText(container, treeViewValue[0].label!);
  });
});

describe('TreeView actions', () => {
  it('should has item --- Item-1.3 --- checked', () => {
    const onChange = jest.fn();

    const { container } = render(
      <TreeView value={treeViewValue} onChange={onChange} variant="checkbox" />
    );

    const elementHasText = queryByText(container, '--- Item-1.3 ---');
    const checkboxInputElement = elementHasText
      ?.closest('.dls-checkbox')
      ?.querySelector('.dls-checkbox-input');
    userEvent.click(checkboxInputElement!);

    const expectCallWithArgs = {
      activeItemId: 'item 1.3',
      activeItemPath: ['0', 'items', '1'],
      value: [
        {
          checked: false,
          id: 'item 1',
          indeterminate: true,
          items: [
            {
              id: 'item 1.1',
              label: '--- Item-1.1 ---',
              items: [
                {
                  id: 'item 1.1.1',
                  label: '--- Item-1.1.1 ---'
                },
                {
                  id: 'item 1.1.2',
                  label: '--- Item-1.1.2 ---'
                }
              ]
            },
            {
              checked: true,
              expanded: false,
              indeterminate: undefined,
              id: 'item 1.3',
              items: [
                {
                  checked: true,
                  id: 'item 1.3.1',
                  indeterminate: undefined,
                  label: '--- Item-1.3.1 ---'
                }
              ],
              label: '--- Item-1.3 ---',
              path: ['0', 'items', '1']
            }
          ],
          label: '--- Item-1 ---'
        },
        {
          id: 'item 2',
          label: ''
        }
      ]
    };

    expect(onChange).toBeCalledWith(expectCallWithArgs);
  });

  it('if item 1.3 is checked then item 1 will has indeterminate is true', () => {
    const onChange = jest.fn();

    const { container } = render(
      <TreeView value={treeViewValue} onChange={onChange} variant="checkbox" />
    );

    const element1dot3 = queryByText(container, '--- Item-1.3 ---');
    const checkboxInputElement1dot3 = element1dot3
      ?.closest('.dls-checkbox')
      ?.querySelector('.dls-checkbox-input');
    userEvent.click(checkboxInputElement1dot3!);

    const expectCallWithArgs1 = {
      activeItemId: 'item 1.3',
      activeItemPath: ['0', 'items', '1'],
      value: [
        {
          id: 'item 1',
          label: '--- Item-1 ---',
          checked: false,
          indeterminate: true,
          items: [
            {
              id: 'item 1.1',
              label: '--- Item-1.1 ---',
              items: [
                {
                  id: 'item 1.1.1',
                  label: '--- Item-1.1.1 ---'
                },
                {
                  id: 'item 1.1.2',
                  label: '--- Item-1.1.2 ---'
                }
              ]
            },
            {
              id: 'item 1.3',
              label: '--- Item-1.3 ---',
              path: ['0', 'items', '1'],
              checked: true,
              expanded: false,
              indeterminate: undefined,
              items: [
                {
                  id: 'item 1.3.1',
                  label: '--- Item-1.3.1 ---',
                  checked: true,
                  indeterminate: undefined
                }
              ]
            }
          ]
        },
        {
          id: 'item 2',
          label: ''
        }
      ]
    };

    expect(onChange).toBeCalledWith(expectCallWithArgs1);
  });

  it('if item 1.1.1 is checked then item 1.1 will has indeterminate is true and item 1 will also has indeterminate is true', () => {
    const onChange = jest.fn();

    const { container } = render(
      <TreeView value={treeViewValue} onChange={onChange} variant="checkbox" />
    );

    const element1dot1dot1 = queryByText(container, '--- Item-1.1.1 ---');
    const checkboxInputElement1dot1dot1 = element1dot1dot1
      ?.closest('.dls-checkbox')
      ?.querySelector('.dls-checkbox-input');
    userEvent.click(checkboxInputElement1dot1dot1!);

    const expectCallWithArgs1 = {
      activeItemId: 'item 1.1.1',
      activeItemPath: ['0', 'items', '0', 'items', '0'],
      value: [
        {
          id: 'item 1',
          label: '--- Item-1 ---',
          checked: false,
          indeterminate: true,
          items: [
            {
              id: 'item 1.1',
              label: '--- Item-1.1 ---',
              checked: false,
              indeterminate: true,
              items: [
                {
                  id: 'item 1.1.1',
                  label: '--- Item-1.1.1 ---',
                  path: ['0', 'items', '0', 'items', '0'],
                  checked: true,
                  expanded: false,
                  indeterminate: undefined
                },
                {
                  id: 'item 1.1.2',
                  label: '--- Item-1.1.2 ---'
                }
              ]
            },
            {
              id: 'item 1.3',
              label: '--- Item-1.3 ---',
              items: [
                {
                  id: 'item 1.3.1',
                  label: '--- Item-1.3.1 ---'
                }
              ]
            }
          ]
        },
        {
          id: 'item 2',
          label: ''
        }
      ]
    };

    expect(onChange).toBeCalledWith(expectCallWithArgs1);
  });

  it('if item 1.1.1 is checked then is unchecked, then item 1.1 will has indeterminate is undefined and item 1 will also has indeterminate is undefined', () => {
    const onChange = jest.fn();

    const { container } = render(
      <TreeView
        defaultValue={treeViewValue}
        onChange={onChange}
        variant="checkbox"
      />
    );

    const element1dot1dot1 = queryByText(container, '--- Item-1.1.1 ---');
    const checkboxInputElement1dot1dot1 = element1dot1dot1
      ?.closest('.dls-checkbox')
      ?.querySelector('.dls-checkbox-input');

    userEvent.click(checkboxInputElement1dot1dot1!);

    onChange.mockClear();
    userEvent.click(checkboxInputElement1dot1dot1!);

    const expectCallWithArgs = {
      activeItemId: 'item 1.1.1',
      activeItemPath: ['0', 'items', '0', 'items', '0'],
      value: [
        {
          checked: false,
          id: 'item 1',
          indeterminate: undefined,
          items: [
            {
              checked: false,
              id: 'item 1.1',
              indeterminate: undefined,
              items: [
                {
                  checked: false,
                  expanded: false,
                  id: 'item 1.1.1',
                  indeterminate: undefined,
                  label: '--- Item-1.1.1 ---',
                  path: ['0', 'items', '0', 'items', '0']
                },
                { id: 'item 1.1.2', label: '--- Item-1.1.2 ---' }
              ],
              label: '--- Item-1.1 ---'
            },
            {
              id: 'item 1.3',
              items: [{ id: 'item 1.3.1', label: '--- Item-1.3.1 ---' }],
              label: '--- Item-1.3 ---'
            }
          ],
          label: '--- Item-1 ---'
        },
        {
          id: 'item 2',
          label: ''
        }
      ]
    };

    expect(onChange).toBeCalledWith(expectCallWithArgs);
  });

  it('if item 1.1.1 checked and item 1.1.2 checked, then item 1.1 will has indeterminate undefined', () => {
    const onChange = jest.fn();

    const { container } = render(
      <TreeView
        defaultValue={treeViewValue}
        onChange={onChange}
        variant="checkbox"
      />
    );

    const element1dot1dot1 = queryByText(container, '--- Item-1.1.1 ---');
    const checkboxInputElement1dot1dot1 = element1dot1dot1
      ?.closest('.dls-checkbox')
      ?.querySelector('.dls-checkbox-input');
    userEvent.click(checkboxInputElement1dot1dot1!);

    onChange.mockClear();

    const element1dot1dot2 = queryByText(container, '--- Item-1.1.2 ---');
    const checkboxInputElement1dot1dot2 = element1dot1dot2
      ?.closest('.dls-checkbox')
      ?.querySelector('.dls-checkbox-input');
    userEvent.click(checkboxInputElement1dot1dot2!);

    const expectCallWithArgs = {
      activeItemId: 'item 1.1.2',
      activeItemPath: ['0', 'items', '0', 'items', '1'],
      value: [
        {
          checked: false,
          id: 'item 1',
          indeterminate: true,
          items: [
            {
              checked: true,
              id: 'item 1.1',
              indeterminate: false,
              items: [
                {
                  checked: true,
                  expanded: false,
                  id: 'item 1.1.1',
                  label: '--- Item-1.1.1 ---',
                  path: ['0', 'items', '0', 'items', '0']
                },
                {
                  checked: true,
                  expanded: false,
                  indeterminate: undefined,
                  id: 'item 1.1.2',
                  label: '--- Item-1.1.2 ---',
                  path: ['0', 'items', '0', 'items', '1']
                }
              ],
              label: '--- Item-1.1 ---'
            },
            {
              id: 'item 1.3',
              items: [{ id: 'item 1.3.1', label: '--- Item-1.3.1 ---' }],
              label: '--- Item-1.3 ---'
            }
          ],
          label: '--- Item-1 ---'
        },
        {
          id: 'item 2',
          label: ''
        }
      ]
    };

    expect(onChange).toBeCalledWith(expectCallWithArgs);
  });

  it('should expand when click on expand icon (uncontrolled)', () => {
    const onChange = jest.fn();

    const { container } = render(
      <TreeView
        defaultValue={treeViewValue}
        onChange={onChange}
        variant="checkbox"
      />
    );

    const element1 = queryByText(container, '--- Item-1 ---');
    const expandButtonElement = element1
      ?.closest('.dls-treeview-line')
      ?.querySelector('.btn');
    userEvent.click(expandButtonElement!);

    const groupElement = element1?.closest('.has-group');

    expect(groupElement?.className).toContain('expanded');
  });

  it('should expand when click on expand icon (controlled)', () => {
    const onChange = jest.fn();

    const { container } = render(
      <TreeView value={treeViewValue} onChange={onChange} variant="checkbox" />
    );

    const element1 = queryByText(container, '--- Item-1 ---');
    const expandButtonElement = element1
      ?.closest('.dls-treeview-line')
      ?.querySelector('.btn');
    userEvent.click(expandButtonElement!);

    expect(onChange).toBeCalled();
  });
});

describe('TreeView Item render', () => {
  it('should render with highlightwords', () => {
    render(<Item variant="text" highlightText="ABC" />);
    expect(true).toBeTruthy();
  });
});
