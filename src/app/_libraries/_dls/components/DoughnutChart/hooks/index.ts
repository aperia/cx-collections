export { default as useRegister } from './useRegister';
export { default as useDoughnutLegend } from './useDoughnutLegend';
export { default as useDoughnutCenterCanvas } from './useDoughnutCenterCanvas';
