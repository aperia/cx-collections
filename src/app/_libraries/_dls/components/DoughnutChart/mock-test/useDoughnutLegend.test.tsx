import React from 'react';
import { queryByText, render } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';
import { useDoughnutLegend } from '../hooks';
import Button from '../../Button';
import { Chart } from 'chart.js';

jest.mock('chart.js', () => ({
  Chart: {
    register: (list: any) => undefined
  },
  registerables: []
}));

const DoughnutLegendMock: React.FC<any> = ({
  hasData = true,
  shouldUseGenerateLabels = true,
  allowToggleLegend = false,
  legendValueRender,
  legendLabelRender
}) => {
  const [plugin, elements] = useDoughnutLegend(
    '1',
    allowToggleLegend,
    legendValueRender,
    legendLabelRender
  );

  const handleTriggerBeforeDraw = () => {
    (plugin as any).afterUpdate({
      options: {
        plugins: {
          legend: {
            labels: {
              generateLabels: shouldUseGenerateLabels
                ? (chart: Chart) => [{}, {}]
                : undefined
            }
          }
        }
      },
      data: hasData
        ? { datasets: [{ data: [1, 2] }], labels: ['1', '2'] }
        : undefined,

      toggleDataVisibility: (toggleIndex: number) => undefined,
      update: () => undefined
    });
  };

  return (
    <>
      <Button onClick={handleTriggerBeforeDraw}>trigger beforeDraw</Button>
      {elements}
    </>
  );
};

describe('useDoughnutLegend', () => {
  it('should return if chartData is undefined', () => {
    const { container } = render(
      <DoughnutLegendMock shouldUseChartData={false} />
    );

    const buttonElement = queryByText(container, 'trigger beforeDraw');
    userEvent.click(buttonElement!);

    // temp
    expect(true).toBeTruthy();
  });

  it('should return if labels is undefined', () => {
    const { container } = render(
      <DoughnutLegendMock shouldUseLabels={false} />
    );

    const buttonElement = queryByText(container, 'trigger beforeDraw');
    userEvent.click(buttonElement!);

    // temp
    expect(true).toBeTruthy();
  });

  it('should null if generateLabels is undefined', () => {
    const { container } = render(
      <DoughnutLegendMock shouldUseGenerateLabels={false} />
    );

    const buttonElement = queryByText(container, 'trigger beforeDraw');
    userEvent.click(buttonElement!);

    // temp
    expect(true).toBeTruthy();
  });

  it('should run handleOnClickLegend and deny toggle legend', () => {
    const { container } = render(<DoughnutLegendMock />);

    const buttonElement = queryByText(container, 'trigger beforeDraw');
    userEvent.click(buttonElement!);

    const legendElement = container.querySelector(
      '.doughnut-chart-legend-item'
    );
    userEvent.click(legendElement!);

    // temp
    expect(true).toBeTruthy();
  });

  it('should run handleOnClickLegend and allow toggle legend', () => {
    const { container } = render(<DoughnutLegendMock allowToggleLegend />);

    const buttonElement = queryByText(container, 'trigger beforeDraw');
    userEvent.click(buttonElement!);

    const legendElement = container.querySelector(
      '.doughnut-chart-legend-item'
    );
    userEvent.click(legendElement!);

    // temp
    expect(true).toBeTruthy();
  });

  it('should run legendValueRender and legendLabelRender', () => {
    const { container } = render(
      <DoughnutLegendMock
        legendValueRender={() => ''}
        legendLabelRender={() => ''}
      />
    );

    const buttonElement = queryByText(container, 'trigger beforeDraw');
    userEvent.click(buttonElement!);

    // temp
    expect(true).toBeTruthy();
  });

  it('should cover 100%', () => {
    const { container } = render(<DoughnutLegendMock hasData={false} />);

    const buttonElement = queryByText(container, 'trigger beforeDraw');
    userEvent.click(buttonElement!);

    // temp
    expect(true).toBeTruthy();
  });
});
