import React from 'react';
import { queryByText, render } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';
import { useDoughnutCenterCanvas } from '../hooks';
import Button from '../../Button';

import '../../../test-utils/mocks/mockCanvas';

jest.mock('chart.js', () => ({
  Chart: {
    register: (list: any) => undefined
  },
  registerables: []
}));

const DoughnutLegendMock: React.FC<any> = ({ size }) => {
  const plugin = useDoughnutCenterCanvas(
    '1',
    {
      value: '9999',
      label: 'Total'
    },
    size
  );

  const handleTriggerBeforeDraw = () => {
    (plugin as any).beforeDraw({
      ctx: {
        font: '',
        fillStyle: '',
        fillText: (value: string, position: any) => undefined
      },
      width: 100,
      height: 100
    });
  };

  return <Button onClick={handleTriggerBeforeDraw}>trigger beforeDraw</Button>;
};

describe('useDoughnutCenterCanvas', () => {
  it('should use size small', () => {
    const { container } = render(<DoughnutLegendMock size="small" />);

    const buttonElement = queryByText(container, 'trigger beforeDraw');
    userEvent.click(buttonElement!);

    expect(true).toBeTruthy();
  });

  it('should cover 100%', () => {
    const { container } = render(<DoughnutLegendMock />);

    const buttonElement = queryByText(container, 'trigger beforeDraw');
    userEvent.click(buttonElement!);

    expect(true).toBeTruthy();
  });
});
