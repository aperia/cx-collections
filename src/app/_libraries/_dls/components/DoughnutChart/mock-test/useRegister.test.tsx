import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { useRegister } from '../hooks';

jest.mock('chart.js', () => ({
  Chart: {
    register: (list: any) => undefined
  },
  registerables: []
}));

const RegisterHookMock: React.FC<any> = (props) => {
  useRegister();

  return null;
};

describe('useRegister', () => {
  it('should cover 100%', () => {
    const { rerender } = render(<RegisterHookMock />);

    rerender(<RegisterHookMock />);

    // temp
    expect(true).toBeTruthy();
  });
});
