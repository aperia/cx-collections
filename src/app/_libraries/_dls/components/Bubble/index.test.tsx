import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';

// component
import Bubble from './index';

describe('test render', () => {
  it('should render', () => {
    const ref = React.createRef<HTMLDivElement>();
    const { baseElement } = render(<Bubble ref={ref} small />);
    expect(baseElement.querySelector('.bubble')).toBeInTheDocument();
  });

  it('should render small size', () => {
    const { baseElement } = render(<Bubble small />);
    expect(baseElement.querySelector('.bb-sz-sm')).toBeInTheDocument();
  });

  it('should render default img', () => {
    const wrapper = render(<Bubble isUnassigned={true} />);
    expect(wrapper.baseElement.querySelector('img')?.src).toEqual(
      `${window.location.origin}/icon-unassigned.svg`
    );
  });

  it('should not render img', () => {
    const wrapper = render(<Bubble isUnassigned={false} />);
    expect(wrapper.baseElement.querySelector('img')).toBeNull();
  });

  it('should render with disabled prop', () => {
    const wrapper = render(<Bubble disabled />);
    expect(wrapper.baseElement.querySelector('.disabled')).toBeInTheDocument();
  });
});

describe('test display name', () => {
  it('should render content A when isSystem props is false', () => {
    const wrapper = render(<Bubble isSystem={false} />);
    expect(wrapper.baseElement.querySelector('span')?.innerHTML).toEqual('A');
  });

  it('should render content S when isSystem props is true', () => {
    const wrapper = render(<Bubble isSystem={true} />);
    expect(wrapper.baseElement.querySelector('span')?.innerHTML).toEqual('S');
  });

  it('should render short name > having name props', () => {
    const wrapper = render(<Bubble name="ABC DEF GHI" />);
    expect(wrapper.baseElement.querySelector('span')?.innerHTML).toEqual('AG');
  });

  it('should render content A when name props is empty', () => {
    const wrapper = render(<Bubble isSystem={false} name="" />);
    expect(wrapper.baseElement.querySelector('span')?.innerHTML).toEqual('A');
  });
});
