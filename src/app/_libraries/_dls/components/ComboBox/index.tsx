import React, {
  useRef,
  useState,
  useEffect,
  useImperativeHandle,
  DOMAttributes
} from 'react';

// components/types/hooks
import DropdownBase, {
  DropdownBaseChangeEvent,
  DropdownBaseGroup,
  DropdownBaseItem,
  DropdownBaseRef,
  HighLightWordsContextType
} from '../DropdownBase';
import PopupBase, { PopupBaseRef } from '../PopupBase';
import Tooltip from '../Tooltip';
import Label from '../Label';
import { ComboBoxRef, ComboBoxProps } from './types';
import Icon from '../Icon';
import { HighLightWordsContext } from '../DropdownBase';
import Bubble from '../Bubble';

// utils
import classnames from 'classnames';
import {
  className,
  truncateTooltip,
  classes,
  extendsEvent,
  genAmtId
} from '../../utils';
import {
  get,
  isBoolean,
  isFunction,
  isObject,
  isString,
  pick
} from '../../lodash';
import { nanoid } from 'nanoid';

const getTextValue = (value: any, textField?: string) => {
  if (!value) return '';

  if (isObject(value) && !textField) {
    throw new Error(
      'value is object so textField should be defined, please define your textField prop.'
    );
  }

  let textValue;

  if (isString(value)) {
    textValue = value;
  }

  if (isObject(value) && textField) {
    textValue = get(value, textField, '');
  }

  return textValue;
};

const ComboBox: React.RefForwardingComponent<ComboBoxRef, ComboBoxProps> = (
  {
    value: valueProp,
    onChange,

    label,
    textField,

    bubble = false,
    size,

    onFocus,
    onBlur,
    opened: openedProp,
    onVisibilityChange,

    onFilterChange,

    loading,

    onInfinityScroll,
    loadingInfinityScroll,

    readOnly,
    disabled,
    placeholder,

    id,
    name,
    required,
    error,
    errorTooltipProps,

    popupBaseProps,
    autoFocus = false,

    children: childrenProp,

    dataTestId,
    ...props
  },
  ref
) => {
  // refs
  const containerRef = useRef<HTMLDivElement | null>(null);
  const popupBaseRef = useRef<PopupBaseRef | null>(null);
  const dropdownBaseRef = useRef<DropdownBaseRef | null>(null);
  const inputRef = useRef<HTMLInputElement | null>(null);
  const floatingLabelRef = useRef<HTMLSpanElement | null>(null);
  const autoFocusRef = useRef(autoFocus);
  const fakeFocusRef = useRef<HTMLDivElement | null>(null);
  const keepRef = useRef<Record<string, any>>({});

  // states
  const [opened, setOpened] = useState(!!openedProp);
  const [value, setValue] = useState(valueProp);
  const [filter, setFilter] = useState('');
  const [children, setChildren] = useState(childrenProp);
  const [focus, setFocus] = useState(false);
  const [highLightWordsContextValue, setHighLightWordsContextValue] =
    useState<HighLightWordsContextType>({
      highlightTextFilter: filter,
      setHighlightTextFilter: (nextHighLightTextFilter) =>
        setHighLightWordsContextValue((state) => ({
          ...state,
          highlightTextFilter: nextHighLightTextFilter
        }))
    });

  // basic filter algorithm
  const isMatched = (text: string, pattern: string) => {
    return text.toLowerCase().indexOf(pattern.toLowerCase()) !== -1;
  };

  // handle filter children
  // input: current filter
  // output: list child match with current filter
  const filterChildren = (nextFilter: string) => {
    const collector: any = [];

    React.Children.forEach(childrenProp, (child) => {
      const isGroup = get(child, 'type') === DropdownBaseGroup;

      if (isGroup) {
        const childrenInGroup = get(child, ['props', 'children']) as unknown[];
        const childrenMatchedInGroup = childrenInGroup.filter((element) => {
          const labelFilter = get(element, ['props', 'label']) as string;
          return isMatched(labelFilter, nextFilter);
        });

        if (childrenMatchedInGroup.length) {
          const nextChildProps = {
            ...get(child, 'props'),
            children: childrenMatchedInGroup
          };
          const childCloned = React.cloneElement(child as any, nextChildProps);
          collector.push(childCloned);
        }

        return;
      }

      const labelFilter = get(child, ['props', 'label']) as string;
      if (isMatched(labelFilter, nextFilter)) collector.push(child);
    });

    return collector as typeof children;
  };

  const resetChildren = () => setChildren(childrenProp);

  // handle value uncontrolled/controlled
  const handleValueMode = (
    event: DropdownBaseChangeEvent,
    keepPopupOpen?: boolean
  ) => {
    isFunction(onChange) && onChange(event);

    // if isMouseDownRef.current is true, we know that we need to call onBlur on nextTick
    // because we need to make sure onChange has done (new value updated) before we call onBlur
    if (event.changeBy !== 'up/down' && !keepPopupOpen) {
      process.nextTick(() => inputRef.current?.blur());
    }

    // controlled
    if (valueProp) return;

    // uncontrolled
    setValue(event.target.value);
  };

  // handle opened uncontrolled/controlled
  const handleOpenedMode = (nextOpened: boolean) => {
    isFunction(onVisibilityChange) && onVisibilityChange(nextOpened);

    // controlled
    if (isBoolean(openedProp)) return;

    // uncontrolled
    setOpened(nextOpened);
  };

  // handle filter uncontrolled/controlled
  // for now, we removed controlled mode, just support uncontrolled mode
  const handleFilterMode = (nextFilter: string) => {
    onFilterChange?.({
      target: {
        id,
        name,
        value: nextFilter
      },
      value: nextFilter
    } as any);

    // uncontrolled (remaining code below)
    setFilter(nextFilter);

    // filter children based on the filter
    const textValue = getTextValue(value, textField);

    // update current text filter to Context
    const { setHighlightTextFilter } = highLightWordsContextValue;
    setHighlightTextFilter?.(nextFilter === textValue ? '' : nextFilter);

    // following the code line 173
    if (nextFilter === textValue) return resetChildren();
    const nextChildren = filterChildren(nextFilter);
    setChildren(nextChildren);

    // handle reset the value to undefined if the filter is empty string
    // and the current value is exist
    if (!nextFilter && textValue) {
      handleValueMode(
        {
          target: { id, name, value: undefined },
          value: undefined
        } as any,
        true
      );
    }
  };

  // handle focus on ComboBox element
  const handleOnFocus = () => {
    let nextEvent = extendsEvent({} as any, 'target', { name, id, value });
    nextEvent = extendsEvent(nextEvent, null, { value });

    isFunction(onFocus) && onFocus(nextEvent);

    // just support lazy load case, will remove on future
    setFocus(true);

    handleOpenedMode(true);
  };

  // handle blur on ComboBox element
  const handleOnBlur = () => {
    // just support lazy load case, will remove on future
    setFocus(false);

    // we are in the onBlur stage
    // the filter maybe valid or maybe invalid
    // if the current value is valid and text field of the current value
    // is not equal filter
    // we will reset the filter back to text field of the current value
    // else we will reset the filter to empty string
    if (value) {
      const textValue = getTextValue(value, textField);
      if (filter && filter !== textValue) {
        handleFilterMode(textValue);
      }
    } else {
      if (filter) handleFilterMode('');
    }

    // what if the user clicks on something that is not in the browser?
    // example: desktop, taskbar, developer tools...
    // by default, if the user clicks on those thing, the browser will keeps
    // the previous state
    // So the next time, the user clicks (mousedown) on the screen (valid container DOM browser),
    // onFocus will be called and onMouseDown will be not called
    // we don't want that behavior
    // so this code block to prevent that behavior
    if (
      document?.activeElement &&
      document.activeElement === inputRef.current
    ) {
      inputRef.current?.blur();
    }

    let nextEvent = extendsEvent({} as any, 'target', { name, id, value });
    nextEvent = extendsEvent(nextEvent, null, { value });

    isFunction(onBlur) && onBlur(nextEvent);
    handleOpenedMode(false);
  };

  // handle onChange ComboBox
  const handleOnChange = handleValueMode;

  // handle onChange filter ComboBox
  const handleOnChangeFilter = (event: React.ChangeEvent<HTMLInputElement>) => {
    handleFilterMode(event.target.value);
  };

  // handle mousedown on icon
  const handleOnMouseDownIcon = (
    event: React.MouseEvent<HTMLSpanElement, MouseEvent>
  ) => {
    if (disabled) return;

    const inputElement = inputRef.current;

    // stop default behavior browser (blur/focus, abc, xyz...)
    event.preventDefault();

    if (document?.activeElement && document.activeElement === inputElement) {
      inputElement?.blur();
      return;
    }

    // prevent parent element auto scroll to inputElement
    inputElement?.focus({ preventScroll: true });
  };

  const handleOnPopupShown = () => {
    const popupElement = popupBaseRef.current?.element;

    const handleMouseDown = (event: MouseEvent) => {
      // stop browser events
      event.preventDefault();
    };

    // no need to remove event later, because popup element will be remove from DOM
    popupElement?.addEventListener('mousedown', handleMouseDown);
  };

  const handleOnPopupClosed = () => {
    if (document?.activeElement === document?.body) {
      fakeFocusRef.current?.focus({ preventScroll: true });
    }
  };

  // temp
  const handleMouseDown: DOMAttributes<HTMLDivElement>['onMouseDown'] = (
    event
  ) => {
    event.preventDefault();
    inputRef.current?.focus();
  };

  keepRef.current.value = value;
  keepRef.current.handleValueMode = handleValueMode;
  keepRef.current.handleOpenedMode = handleOpenedMode;
  keepRef.current.handleFilterMode = handleFilterMode;
  keepRef.current.filterChildren = filterChildren;
  keepRef.current.resetChildren = resetChildren;

  // provide ref
  const publicRef = useRef<ComboBoxRef | null>();
  useImperativeHandle(ref, () => {
    if (!publicRef.current) publicRef.current = {};

    publicRef.current.popupBaseRef = popupBaseRef;
    publicRef.current.dropdownBaseRef = dropdownBaseRef;
    publicRef.current.inputElement = inputRef.current;

    return publicRef.current;
  });

  // set props
  useEffect(() => setOpened(!!openedProp), [openedProp]);
  useEffect(() => setValue(valueProp), [valueProp]);
  useEffect(() => setChildren(childrenProp), [childrenProp]);

  // handle update the filter based on the value
  useEffect(() => {
    const textValue = getTextValue(value, textField);

    keepRef.current.handleFilterMode(textValue);
  }, [value, textField]);

  // text should be selected when opened is true
  useEffect(() => {
    if (!focus) return;

    inputRef.current?.select();
  }, [focus]);

  // handle truncate tooltip label
  useEffect(() => truncateTooltip(floatingLabelRef.current!), [label]);

  // if opened is controlled, input filter not focused -> focus input filter
  useEffect(() => {
    if (!opened || document.activeElement === inputRef.current) return;

    inputRef.current?.focus();
  }, [opened]);

  // handle autofocus first time render
  useEffect(() => {
    if (!autoFocusRef.current) return;

    autoFocusRef.current = false;
    inputRef.current?.focus();
  }, []);

  // check should show error or not
  const { message, status } = pick(error, 'message', 'status');
  const isOpenedTooltip = Boolean(status && message && (opened || focus));

  // check required
  const isRequired = required && !readOnly && !disabled;

  // check should floating label or not
  const floating =
    (label && opened && !readOnly && !disabled) ||
    value ||
    filter ||
    (focus && !readOnly);

  // text
  const textValue = getTextValue(value, textField);

  // with bubble
  const withBubble = bubble && textValue;

  // check small size
  const isSmallSize = size === 'small';

  // check no label
  const noLabel = !label || isSmallSize;

  const { popupBaseClassName, ...popupProps } = popupBaseProps || {};

  return (
    <>
      <Tooltip
        opened={isOpenedTooltip}
        element={message}
        variant="error"
        placement="top-start"
        triggerClassName="d-block"
        dataTestId={`${dataTestId}-combobox-error`}
        {...errorTooltipProps}
      >
        <div
          ref={containerRef}
          className={classnames({
            [classes.combobox.container]: true,
            [classes.combobox.bubble]: withBubble,
            [classes.state.focused]: opened || focus,
            [classes.state.readOnly]: readOnly,
            [classes.state.disabled]: disabled,
            [classes.state.error]: status,
            [classes.state.floating]: floating,
            [classes.state.noLabel]: noLabel,
            [classes.state.small]: isSmallSize
          })}
          data-testid={genAmtId(dataTestId, 'dls-combobox', 'ComboBox')}
        >
          <div
            className={classes.inputContainer.container}
            onMouseDown={handleMouseDown}
          >
            <form onSubmit={(e) => e.preventDefault()} autoComplete={nanoid()}>
              {withBubble && <Bubble small name={textValue} />}
              <input
                ref={inputRef}
                value={filter}
                onChange={handleOnChangeFilter}
                onFocus={handleOnFocus}
                onBlur={handleOnBlur}
                placeholder={placeholder}
                disabled={disabled}
                readOnly={readOnly}
                type="text"
                autoComplete={nanoid()}
              />
            </form>
            {!noLabel && (
              <Label
                ref={floatingLabelRef}
                asterisk={isRequired}
                error={status}
              >
                <span>{label}</span>
              </Label>
            )}
          </div>
          <Icon
            onMouseDown={handleOnMouseDownIcon}
            size={isSmallSize ? '2x' : '3x'}
            name="chevron-down"
          />
          <div ref={fakeFocusRef} tabIndex={-1} />
        </div>
      </Tooltip>
      <PopupBase
        ref={popupBaseRef}
        reference={containerRef.current!}
        opened={opened && !readOnly && !disabled}
        onVisibilityChange={handleOpenedMode}
        onPopupShown={handleOnPopupShown}
        onPopupClosed={handleOnPopupClosed}
        popupBaseClassName={classnames(
          className.popupBase.CONTAINER,
          popupBaseClassName
        )}
        fluid
        dataTestId={`${dataTestId}-popup-combobox`}
        {...popupProps}
      >
        {loading ? (
          <div className="loading loading-md lazy" />
        ) : (
          <HighLightWordsContext.Provider value={highLightWordsContextValue}>
            <DropdownBase
              ref={dropdownBaseRef}
              allowKeydown={opened}
              allowCallOnChangeByUpDown
              allowCallOnChangeByClick
              allowCallOnChangeByEnter
              value={value}
              onChange={handleOnChange}
              highlightTextItem
              onInfinityScroll={onInfinityScroll}
              loadingInfinityScroll={loadingInfinityScroll}
              id={id}
              name={name}
              dataTestId={dataTestId}
              {...props}
            >
              {children}
            </DropdownBase>
          </HighLightWordsContext.Provider>
        )}
      </PopupBase>
    </>
  );
};

const ComboBoxExtraStaticProp = React.forwardRef<ComboBoxRef, ComboBoxProps>(
  ComboBox
) as React.ForwardRefExoticComponent<
  ComboBoxProps & React.RefAttributes<ComboBoxRef>
> & {
  Group: typeof DropdownBaseGroup;
  Item: typeof DropdownBaseItem;
};

ComboBoxExtraStaticProp.Group = DropdownBaseGroup;
ComboBoxExtraStaticProp.Item = DropdownBaseItem;

export * from './types';
export default ComboBoxExtraStaticProp;
