import {
  DropdownBaseProps,
  DropdownBaseRef,
  DropdownBaseItemProps,
  DropdownBaseGroupProps,
  DropdownBaseItemRef
} from '../DropdownBase';
import { PopupBaseRef, PopupBaseProps } from '../PopupBase';
import { TooltipProps } from '../Tooltip';

export interface ComboBoxRef {
  popupBaseRef?: React.MutableRefObject<PopupBaseRef | null>;
  dropdownBaseRef?: React.MutableRefObject<DropdownBaseRef | null>;
  inputElement?: HTMLInputElement | null;
}

export interface ComboBoxProps
  extends Omit<
      DropdownBaseProps,
      | 'selected'
      | 'onSelect'
      | 'allowKeydown'
      | 'reference'
      | 'scrollContainerProps'
    >,
    DLSId {
  label?: React.ReactNode;
  textField?: string;

  // support show bubble next to input filter
  bubble?: boolean;

  // support small size
  size?: 'small';

  onBlur?: (event: React.FocusEvent) => void;
  onFocus?: (event: React.FocusEvent) => void;

  opened?: boolean;
  onVisibilityChange?: (nextOpened: boolean) => void;

  // For easy maintenance, controlled filter is not supported
  // Just provides a mechanism to watch the filter
  onFilterChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;

  loading?: boolean;

  readOnly?: boolean;
  disabled?: boolean;
  placeholder?: string;

  required?: boolean;
  error?: {
    message?: string;
    status?: boolean;
  };
  errorTooltipProps?: TooltipProps;

  popupBaseProps?: Omit<
    PopupBaseProps,
    'opened' | 'onVisibilityChange' | 'reference'
  >;
  autoFocus?: boolean;

  isSearchControl?: boolean;
}

export interface ComboBoxItemRef extends DropdownBaseItemRef {}

export interface ComboBoxItemProps extends DropdownBaseItemProps {}

export interface ComboBoxGroupProps extends DropdownBaseGroupProps {}
