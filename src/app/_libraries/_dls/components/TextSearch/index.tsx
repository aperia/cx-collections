import React, {
  ChangeEvent,
  FormEvent,
  ForwardRefRenderFunction,
  useEffect,
  useImperativeHandle,
  useRef,
  useState
} from 'react';

// components
import Icon from '../Icon';
import TextBox, { TextBoxProps, TextBoxRef } from '../TextBox';
import Tooltip from '../Tooltip';
import PopupBase, { PopupBaseRef } from '../PopupBase';

// utils
import classnames from 'classnames';
import { classes, genAmtId } from '../../utils';
import { isBoolean, isEmpty, isUndefined } from '../../lodash';

export interface TextSearchProps extends TextBoxProps {
  className?: string;
  clearTooltip?: string;
  onSearch?: (value: string) => void;
  value?: string;
  onChange?: (event: ChangeEvent<HTMLInputElement>) => void;
  onClear?: () => void;

  popupElement?: React.ReactNode;
  /**
   * Just works if popupElement is provided
   */
  opened?: boolean;
  /**
   * Just works if popupElement is provided
   */
  onVisibilityChange?: (nextOpened: boolean) => void;
  popupBaseClassName?: string;
}

const TextSearch: ForwardRefRenderFunction<TextBoxRef, TextSearchProps> = (
  {
    placeholder = '',
    className: classNameProp,
    clearTooltip = 'Clear Search Criteria',
    onSearch,
    value: valueProp,
    onChange: onChangeProp,
    onClear,
    disabled,

    popupElement: popupElementProp,
    opened: openedProp,
    onVisibilityChange,
    popupBaseClassName,

    dataTestId,
    ...props
  },
  ref
) => {
  const containerRef = useRef(null);
  const popupBaseRef = useRef<PopupBaseRef | null>(null);

  const [textBoxElement, setTextBoxElement] = useState<TextBoxRef | null>(null);
  const [opened, setOpened] = useState(openedProp);
  const [searchValue, setSearchValue] = useState(
    !isUndefined(valueProp) ? valueProp : ''
  );

  const handleOpenedMode = (nextOpened: boolean) => {
    // not handled
    if (!popupElementProp) return;

    // controlled
    if (isBoolean(openedProp)) {
      return onVisibilityChange?.(nextOpened);
    }

    // uncontrolled
    setOpened(nextOpened);
  };

  const handleSearch = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (isEmpty(searchValue.trim())) return;

    onSearch?.(searchValue);
  };

  const handleClear = () => {
    onClear?.();
    textBoxElement?.inputElement?.focus();

    // uncontrolled mode
    if (isUndefined(valueProp)) setSearchValue('');
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    onChangeProp?.(event);

    // uncontrolled mode
    if (isUndefined(valueProp)) setSearchValue(event.target.value);
  };

  const handleFocus = () => {
    handleOpenedMode(true);
  };

  const handleBlur = () => {
    handleOpenedMode(false);
  };

  // if the popup shown, we want the button element to stay in focus even if we
  // click on the popup, read more details in side this function
  const handleOnPopupShown = () => {
    const popupElement = popupBaseRef.current?.element;

    // handle mousedown on Popup's DropdownList
    // mousedown on Popup's DropdownList will trigger onBlur default of the DropdownList
    // that is not the behavior we want, so we need to preventDefault it
    const handleMouseDown = (event: MouseEvent) => {
      // stop browser events
      event.preventDefault();
    };

    // don't need to remove event later, because popup element will be removed from DOM
    popupElement?.addEventListener('mousedown', handleMouseDown);
  };

  // set props
  useEffect(() => {
    if (isUndefined(valueProp)) return;

    setSearchValue(valueProp);
  }, [valueProp]);

  useEffect(() => {
    setOpened(openedProp);
  }, [openedProp]);

  useImperativeHandle(ref, () => textBoxElement!);

  const containerId = genAmtId(dataTestId, 'dls-text-search', 'TextSearch');

  return (
    <>
      <form
        onSubmit={handleSearch}
        ref={containerRef}
        data-testid={containerId}
      >
        <TextBox
          ref={setTextBoxElement}
          value={searchValue}
          onChange={handleChange}
          onFocus={handleFocus}
          onBlur={handleBlur}
          placeholder={placeholder}
          size="sm"
          className={classnames({
            [classes.textSearch.container]: true,
            [classes.state.disabled]: disabled,
            classNameProp
          })}
          suffix={
            <div className={classnames(classes.textSearch.searchIcon)}>
              <Tooltip
                element={clearTooltip}
                placement="top"
                variant="primary"
                triggerClassName={classes.textSearch.clearButton}
                dataTestId={genAmtId(containerId, 'error', 'TextSearch')}
              >
                <div
                  onClick={handleClear}
                  className={classnames({
                    'd-none': isEmpty(searchValue)
                  })}
                  role="button"
                  data-testid={genAmtId(containerId, 'clear-btn', 'TextSearch')}
                >
                  <Icon name="close" size="4x" />
                </div>
              </Tooltip>
              <button
                type="submit"
                className={classnames(classes.textSearch.searchButton)}
                data-testid={genAmtId(containerId, 'search-btn', 'TextSearch')}
              >
                <Icon name="search" />
              </button>
            </div>
          }
          dataTestId={genAmtId(containerId, 'input', 'TextSearch')}
          disabled={disabled}
          {...props}
        />
        {popupElementProp ? (
          <PopupBase
            ref={popupBaseRef}
            reference={textBoxElement?.containerElement as HTMLElement}
            opened={opened}
            onVisibilityChange={handleOpenedMode}
            onPopupShown={handleOnPopupShown}
            popupBaseClassName={classnames(
              classes.popupBase.container,
              popupBaseClassName
            )}
            fluid
            dataTestId={genAmtId(containerId, 'PopupBase', 'TextSearch')}
          >
            {popupElementProp}
          </PopupBase>
        ) : null}
      </form>
    </>
  );
};

export default React.forwardRef<TextBoxRef, TextSearchProps>(TextSearch);
