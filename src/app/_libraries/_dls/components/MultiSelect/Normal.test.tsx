import React from 'react';
import {
  fireEvent,
  render,
  RenderResult,
  screen
} from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';

// components
import MultiSelect from '.';
import Normal, { NormalProps } from './Normal';

// mocks
import '../../test-utils/mocks/mockCanvas';

// utils
import { queryByClass, queryAllByClass } from '../../test-utils/queryHelpers';

jest.mock('../../utils', () => {
  const utils = jest.requireActual('../../utils');

  return {
    ...utils,
    canvasTextWidth: (text: string) => (text === '' ? null : text.length * 50)
  };
});

const data = [
  { FieldID: 'NER', FieldValue: 'NIGER' },
  { FieldID: 'BMU', FieldValue: 'BERMUDA' }
];
let wrapper: RenderResult;

interface RenderComponentProps
  extends Omit<
    NormalProps,
    | 'children'
    | 'handleOpenedMode'
    | 'handleValueMode'
    | 'handleFilterMode'
    | 'filter'
  > {
  filter?: string;
  handleOpenedMode?: jest.Mock;
  handleValueMode?: jest.Mock;
  handleFilterMode?: jest.Mock;
}

const renderComponent = (props: RenderComponentProps) => {
  wrapper = render(
    <div>
      <Normal
        label="Label"
        placeholder="My MultiSelect"
        textField="FieldValue"
        filter=""
        handleOpenedMode={jest.fn()}
        handleValueMode={jest.fn()}
        handleFilterMode={jest.fn()}
        {...props}
      >
        {data.map((item, index) => (
          <MultiSelect.Item
            key={item.FieldID + index}
            label={item.FieldValue}
            value={item}
            variant="checkbox"
          />
        ))}
      </Normal>
    </div>
  );

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement
  };
};

const rerenderComponent = (props: RenderComponentProps) => {
  wrapper.rerender(
    <div>
      <Normal
        label="Label"
        placeholder="My MultiSelect"
        textField="FieldValue"
        filter=""
        handleOpenedMode={jest.fn()}
        handleValueMode={jest.fn()}
        handleFilterMode={jest.fn()}
        {...props}
      >
        {data.map((item, index) => (
          <MultiSelect.Item
            key={item.FieldID + index}
            label={item.FieldValue}
            value={item}
            variant="checkbox"
          />
        ))}
      </Normal>
    </div>
  );

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement
  };
};

afterEach(() => {
  wrapper?.unmount();
});

describe('Render', () => {
  it('should render', () => {
    const { baseElement } = renderComponent({
      textField: undefined,
      filter: ' ',
      value: data,
      opened: true
    });

    expect(
      queryByClass(baseElement, /text-field-container/)
    ).toBeInTheDocument();

    screen.getByRole('textbox').focus();

    rerenderComponent({
      textField: undefined,
      filter: ' ',
      value: data,
      opened: false
    });
  });
});

describe('Actions', () => {
  describe('handleOnClickIcon', () => {
    it('when enabled', () => {
      const handleValueMode = jest.fn();

      const { baseElement } = renderComponent({
        handleValueMode,
        value: [data[0]]
      });

      const itemElementList = queryAllByClass(baseElement, /icon-close/);

      // remove item selected
      userEvent.click(itemElementList[0]);

      expect(handleValueMode).toBeCalledWith(
        expect.objectContaining({ value: [] })
      );
    });

    it('when disabled', () => {
      const handleValueMode = jest.fn();

      const { baseElement } = renderComponent({
        handleValueMode,
        value: data,
        disabled: true
      });

      const textFieldContainerList = queryAllByClass(
        baseElement,
        /text-field-container/
      );
      const itemElementList = queryAllByClass(baseElement, /icon-close/);

      userEvent.click(textFieldContainerList[0]);

      // remove item selected
      userEvent.click(itemElementList[0]);

      expect(handleValueMode).toBeCalled();
    });
  });

  it('OnFocus and OnBlur', () => {
    const onBlur = jest.fn();
    const onFocus = jest.fn();

    renderComponent({ onBlur, onFocus, value: data });

    userEvent.click(screen.getByRole('textbox'));

    fireEvent.blur(screen.getByRole('textbox'));

    expect(onBlur).toBeCalled();
  });

  it('handleFilterMode', () => {
    const handleFilterMode = jest.fn();

    renderComponent({ handleFilterMode, value: [] });

    fireEvent.change(screen.getByRole('textbox'), {
      target: { value: data[0].FieldValue }
    });

    expect(handleFilterMode).toBeCalledWith(data[0].FieldValue);
  });

  it('handleFilterMode with readOnly/disabled true', () => {
    const handleFilterMode = jest.fn();

    renderComponent({ handleFilterMode, value: data, disabled: true });

    fireEvent.change(screen.getByRole('textbox'), {
      target: { value: data[0].FieldValue }
    });

    expect(handleFilterMode).not.toBeCalledWith(data[0].FieldValue);
  });

  it('handle backspace', () => {
    const handleValueMode = jest.fn();
    const output = [{ FieldID: 'NER', FieldValue: 'NIGER' }];

    jest.useFakeTimers();
    const { wrapper } = renderComponent({ value: data, handleValueMode });
    jest.runAllTimers();

    const inputElement = wrapper.container.querySelector('input')!;

    fireEvent.keyDown(inputElement, { keyCode: 9 });
    expect(handleValueMode).not.toBeCalledWith(output);

    fireEvent.keyDown(inputElement, { keyCode: 8 });
    expect(handleValueMode).not.toBeCalledWith(output);
  });
});
