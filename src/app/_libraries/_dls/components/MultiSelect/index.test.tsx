import React from 'react';
import {
  fireEvent,
  getByRole,
  render,
  RenderResult
} from '@testing-library/react';
import '@testing-library/jest-dom';
import { act } from 'react-dom/test-utils';
import userEvent from '@testing-library/user-event';

// components
import MultiSelect from '.';

// mocks
import '../../test-utils/mocks/mockCanvas';

// utils
import { queryAllByClass, queryByClass } from '../../test-utils/queryHelpers';
import { MultiSelectProps } from './types';

const data = [
  { FieldID: 'ALL', FieldValue: 'All' },
  { FieldID: 'NER', FieldValue: 'NIGER' }
];

const filterName = 'filter_name';
let wrapper: RenderResult;

const renderComponent = (
  props: Omit<MultiSelectProps & { withGroup?: boolean }, 'children'>
) => {
  const items = data.map((item, index) => (
    <MultiSelect.Item
      key={item.FieldID + index}
      label={item.FieldValue}
      value={item}
      variant="checkbox"
    />
  ));

  wrapper = render(
    <div>
      <MultiSelect
        error={{ status: true, message: 'Error' }}
        ref={{ current: null }}
        name={filterName}
        textField="FieldID"
        label="Label"
        placeholder="My MultiSelect"
        {...props}
      >
        {props.withGroup ? (
          <MultiSelect.Group label="GroupHeading">{items}</MultiSelect.Group>
        ) : (
          items
        )}
      </MultiSelect>
    </div>
  );

  act(() => {
    jest.runAllTimers();
  });

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement
  };
};

beforeEach(() => {
  jest.useFakeTimers();
});

afterEach(() => {
  wrapper?.unmount();
});

const userClick = (element: HTMLElement) => {
  act(() => {
    userEvent.click(element);
    jest.runAllTimers();
  });
};

describe('Render', () => {
  it('should render', () => {
    const { baseElement } = renderComponent({});

    expect(queryByClass(baseElement, /dls-popper-trigger/)).toBeInTheDocument();
    expect(queryByClass(baseElement, /dls-dropdown-base-container/)).toBeNull();
  });

  it('should render oppened', () => {
    const { baseElement } = renderComponent({ opened: true });

    expect(queryByClass(baseElement, /dls-popper-trigger/)).toBeInTheDocument();
    expect(
      queryByClass(baseElement, /dls-dropdown-base-container/)
    ).toBeInTheDocument();
  });

  it('should render input autofocus', () => {
    const { baseElement } = renderComponent({ opened: true, autoFocus: true });

    expect(queryByClass(baseElement, /dls-popper-trigger/)).toBeInTheDocument();
    expect(
      queryByClass(baseElement, /dls-dropdown-base-container/)
    ).toBeInTheDocument();

    // wait for update autofocus
    // current: autofocus not effect to any element
    expect(true).toEqual(true);
  });

  it('should render with require', () => {
    renderComponent({ required: true, disabled: false, readOnly: false });

    expect(document.querySelector('.asterisk')).toBeInTheDocument();
  });

  it('should render with noborder variant', () => {
    const { baseElement } = renderComponent({ variant: 'no-border' });

    expect(queryByClass(baseElement, /variant-no-border/)).toBeInTheDocument();
  });
});

describe('Actions', () => {
  describe('handleOnChange', () => {
    it('should return if readonly/disabled is true', () => {
      const onChange = jest.fn();

      const { baseElement } = renderComponent({
        onChange,
        disabled: true,
        value: data.slice(0, 5)
      });

      const iconClose = queryAllByClass(baseElement, /icon-close/);
      userClick(iconClose[0]);

      expect(onChange).not.toBeCalled();
    });

    it('uncontrolled', () => {
      const onChange = jest.fn();

      const { baseElement } = renderComponent({
        opened: true,
        onChange,
        searchBar: true
      });

      const itemElementList = queryAllByClass(baseElement, /item-checkbox/);

      userClick(itemElementList[0]);

      expect(onChange).toBeCalledWith(
        expect.objectContaining({
          value: [data[0]]
        })
      );
    });

    it('controlled', () => {
      // mock
      const onChange = jest.fn();

      // render
      const { baseElement } = renderComponent({
        opened: true,
        onChange,
        value: [data[0]]
      });

      // query
      const itemElementList = queryAllByClass(baseElement, /item-checkbox/);

      // simulate
      userClick(itemElementList[1]);

      // expect
      expect(onChange).toBeCalledWith(
        expect.objectContaining({
          value: [data[0], data[1]]
        })
      );
    });
  });

  describe('handleOpenedMode', () => {
    it('action with Group', () => {
      // mock
      const onVisibilityChange = jest.fn();

      // render
      const { baseElement } = renderComponent({
        onVisibilityChange,
        variant: 'group'
      });

      // query
      const inputElement = queryByClass(baseElement, /text-field-container/)!;

      // simulate
      fireEvent.focus(inputElement);
      jest.runAllTimers();

      // expect
      expect(onVisibilityChange).toBeCalledWith(true);
    });

    it('action with Normal', () => {
      // mock
      const onVisibilityChange = jest.fn();

      // render
      const { baseElement } = renderComponent({ onVisibilityChange });

      // query
      const filter = queryByClass(baseElement, /^filter$/)!;
      const inputElement = getByRole(filter, 'textbox')!;

      // simulate
      fireEvent.focus(inputElement);
      jest.runAllTimers();

      // expect
      expect(onVisibilityChange).toBeCalledWith(true);
    });
  });

  describe('onBlur', () => {
    it('action with Group', () => {
      // mock
      const onBlur = jest.fn();
      const onVisibilityChange = jest.fn();

      // render
      const { baseElement } = renderComponent({
        opened: true,
        variant: 'group',
        onBlur,
        onVisibilityChange
      });

      // query
      const inputElement = queryByClass(baseElement, /text-field-container/)!;

      // simulate
      fireEvent.blur(inputElement);

      // expect
      expect(onBlur).toBeCalled();
      expect(onVisibilityChange).toBeCalledWith(false);
    });

    it('action with Normal', () => {
      // mock
      const onBlur = jest.fn();
      const onVisibilityChange = jest.fn();

      // render
      const { baseElement } = renderComponent({
        opened: true,
        onBlur,
        onVisibilityChange
      });

      // query
      const filter = queryByClass(baseElement, /^filter$/)!;
      const inputElement = getByRole(filter, 'textbox')!;

      // // simulate
      fireEvent.blur(inputElement);

      // expect
      expect(onBlur).toBeCalled();
      expect(onVisibilityChange).toBeCalledWith(false);
    });
  });

  describe('handleFilterMode', () => {
    it('with group', () => {
      // mock
      const onFilterChange = jest.fn();

      // render
      const { baseElement } = renderComponent({
        opened: true,
        withGroup: true
      });

      // query
      const filter = queryByClass(baseElement, /^filter$/)!;
      const inputElement = getByRole(filter, 'textbox')!;

      // simulate
      const filterValue = data[1].FieldValue;
      fireEvent.change(inputElement, { target: { value: filterValue } });

      // expect
      expect(onFilterChange).not.toBeCalled();

      // simulate
      fireEvent.change(inputElement, { target: { value: 'xxx' } });

      // expect
      expect(onFilterChange).not.toBeCalled();
    });

    it('uncontrolled', () => {
      // mock
      const onFilterChange = jest.fn();

      // render
      const { baseElement } = renderComponent({ opened: true });

      // query
      const filter = queryByClass(baseElement, /^filter$/)!;
      const inputElement = getByRole(filter, 'textbox')!;

      // simulate
      const filterValue = data[1].FieldValue;
      fireEvent.change(inputElement, { target: { value: filterValue } });

      // expect
      expect(onFilterChange).not.toBeCalled();
    });

    it('controlled', () => {
      // mock
      const onFilterChange = jest.fn();

      // render
      const { baseElement } = renderComponent({
        variant: 'group',
        searchBar: true,
        onFilterChange
      });

      // query
      const inputElement = queryByClass(baseElement, /text-field-container/)!;

      userClick(inputElement);

      const searchBar = queryByClass(
        baseElement,
        /dls-dropdown-base-search-bar/
      )!;
      const inputSearch = getByRole(searchBar, 'textbox');

      // // simulate
      const filterValue = data[1].FieldValue;
      fireEvent.change(inputSearch, { target: { value: filterValue } });

      // expect
      // expect
      expect(onFilterChange).toBeCalledWith(
        expect.objectContaining({ value: filterValue })
      );
      onFilterChange.mockClear();

      // reopen dropdown
      userClick(inputElement);
      // close dropdown
      userClick(inputElement);

      // expect
      expect(onFilterChange).toBeCalledWith(
        expect.objectContaining({ value: '' })
      );
    });
  });
});
