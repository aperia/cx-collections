export { default as useAfterCloseOnScroll } from './useAfterCloseOnScroll';
export type {
  AfterCloseOnScrollHook,
  AfterCloseOnScrollHookArgs
} from './useAfterCloseOnScroll';
