import React from 'react';

// testing library
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';

// hooks
import useAfterCloseOnScroll, {
  AfterCloseOnScrollHookArgs
} from './useAfterCloseOnScroll';

const Wrapper: React.FC<AfterCloseOnScrollHookArgs> = ({
  opened,
  blurElement
}) => {
  useAfterCloseOnScroll({
    opened,
    blurElement
  } as any);

  return <div>Wrapper</div>;
};

describe('useAfterCloseOnScroll', () => {
  it('should cover 100%', () => {
    const activeElement = { blur: () => undefined };
    Object.defineProperty(document, 'activeElement', {
      value: activeElement
    });
    const { rerender, queryByText } = render(<Wrapper />);

    rerender(<Wrapper opened={false} blurElement={activeElement as any} />);

    rerender(<Wrapper opened />);
    expect(queryByText('Wrapper')).toBeInTheDocument();
  });
});
