export interface ResponsiveFilterElementArgs {
  containerElement: HTMLElement;
  filterElement: HTMLElement;
  callback: () => void;
}

// handle remove redundant space in MultiSelect Normal
const responsiveFilterElement = ({
  containerElement,
  filterElement,
  callback
}: ResponsiveFilterElementArgs) => {
  const lastItemElement = filterElement?.previousElementSibling;

  const widthListElement = containerElement.getBoundingClientRect().width - 28;
  const widthFilterElement = filterElement.getBoundingClientRect().width;
  const widthLastItemElement =
    lastItemElement?.getBoundingClientRect?.()?.width || 0;

  if (widthListElement < widthFilterElement + widthLastItemElement) {
    filterElement!.style.width = '0';
  }

  callback();
};

export default responsiveFilterElement;
