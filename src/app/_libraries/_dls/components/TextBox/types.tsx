import { InputHTMLAttributes } from 'react';
import { TooltipProps } from '../../components/Tooltip';

export interface TextBoxRef {
  containerElement?: HTMLDivElement;
  inputElement?: HTMLInputElement;
}

export interface TextBoxProps
  extends Omit<
      InputHTMLAttributes<HTMLInputElement>,
      'size' | 'prefix' | 'suffix' | 'ref'
    >,
    DLSId {
  label?: string;

  containerClassName?: string;
  allowAutoFocus?: boolean;
  size?: 'sm' | '' | string;
  error?: {
    status: boolean;
    message: string;
  };
  errorTooltipProps?: TooltipProps;
  prefix?: React.ReactElement | string;
  suffix?: React.ReactElement | string;
}
