import React, {
  useLayoutEffect,
  useEffect,
  useState,
  forwardRef,
  useImperativeHandle,
  RefForwardingComponent,
  useMemo
} from 'react';
import classnames from 'classnames';
import { Modal as ModalBase } from 'react-bootstrap';
import isFunction from 'lodash.isfunction';

import { genId, className as classConst, genAmtId } from '../../utils';
import { ModalProps } from './types';

const Modal: RefForwardingComponent<HTMLElement, ModalProps> = (
  {
    id: idProp,
    xs,
    sm,
    md,
    lg,
    full,
    rt,
    lt,
    show: showProp,
    loading,
    animationDuration,
    className: animationClass,
    classes,
    placement,
    children,

    onHide,

    dataTestId,
    ...props
  },
  ref
) => {
  const {
    modal: modalClass,
    content: contentClass,
    dialog: dialogClass,
    backdrop: backdropClass
  } = classes || {};

  const id = useMemo(() => idProp || `id_${genId()}`, [idProp]);

  const [show, setShow] = useState(showProp);
  const [isLoading, setIsLoading] = useState(false);
  const [dialogClasses, setDialogClasses] = useState<string[]>([]);

  useImperativeHandle(ref, () => document.getElementById(id)!);

  useLayoutEffect(() => {
    const dialogClassNames: string[] = [];

    const add = (className: string) => {
      dialogClassNames.push(className);
      return true;
    };

    add(animationClass || '');
    add(dialogClass || '');

    placement && add(`placement-${placement}`);

    (sm && add('window-sm')) ||
      (md && add('window-md')) ||
      (lg && add('window-lg')) ||
      (full && add('window-full')) ||
      (rt && add('window-right')) ||
      (lt && add('window-left')) ||
      add('window-xs');

    setDialogClasses(dialogClassNames);
  }, [
    show,
    placement,
    xs,
    sm,
    md,
    lg,
    rt,
    lt,
    full,
    animationClass,
    dialogClass
  ]);

  useEffect(() => {
    const dialog = document.getElementById(id);
    const backdrop = document.getElementsByClassName(
      classConst.modal.BACKDROP
    )[0] as HTMLDivElement;

    if (backdrop) backdrop.style.animationDuration = `${animationDuration}ms`;
    if (dialog) dialog.style.animationDuration = `${animationDuration}ms`;
  }, [id, show, animationDuration]);

  useEffect(() => setIsLoading(loading || false), [loading]);

  // make sure
  useEffect(() => setShow(showProp), [showProp]);

  return (
    <ModalBase
      id={id}
      show={show}
      animation={false}
      centered
      scrollable
      backdropClassName={backdropClass}
      className={classnames(
        classConst.modal.ROOT,
        modalClass,
        full && classConst.modal.FULL,
        lt && classConst.modal.LEFT,
        rt && classConst.modal.RIGHT
      )}
      contentClassName={classnames(isLoading && 'loading', contentClass)}
      dialogClassName={classnames(classConst.modal.DIALOG, dialogClasses)}
      onHide={() => isFunction(onHide) && onHide()}
      data-testid={genAmtId(dataTestId, 'dls-modal', 'Modal')}
      {...props}
    >
      {children}
    </ModalBase>
  );
};

export default forwardRef<HTMLElement, ModalProps>(Modal);
