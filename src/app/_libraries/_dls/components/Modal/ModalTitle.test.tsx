import React, { MutableRefObject } from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';

// components
import ModalTitle from './ModalTitle';

// utils
import { queryByClass } from '../../test-utils/queryHelpers';

const mockRef: MutableRefObject<HTMLDivElement | null> = { current: null };

describe('Render', () => {
  it('should render', () => {
    const { baseElement } = render(<ModalTitle ref={mockRef} />);

    expect(
      queryByClass(baseElement as HTMLElement, /dls-modal-title/)
    ).toBeInTheDocument();
  });

  it('should has className', () => {
    const { baseElement } = render(
      <ModalTitle ref={mockRef} className={'class-name'} />
    );

    expect(
      queryByClass(baseElement as HTMLElement, /class-name/)
    ).toBeInTheDocument();
  });
});
