export { default as Modal } from './ModalWrapper';
export { default as ModalHeader } from './ModalHeader';
export { default as ModalTitle } from './ModalTitle';
export { default as ModalBody } from './ModalBody';
export { default as ModalFooter } from './ModalFooter';

export * from './types';
