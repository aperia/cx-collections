import React, { MutableRefObject } from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import {
  clearMockMutationObserver,
  createMockMutationObserver
} from '../../test-utils/mocks/mockMutationObserver';

// components
import Modal from './Modal';
import ModalHeader from './ModalHeader';

// utils
import { queryByClass } from '../../test-utils';

const mockRef: MutableRefObject<HTMLDivElement | null> = { current: null };

beforeEach(() => {
  createMockMutationObserver();
});
afterEach(clearMockMutationObserver);

describe('Render', () => {
  it('should render', () => {
    const { baseElement } = render(<ModalHeader ref={mockRef} />);

    expect(
      queryByClass(baseElement as HTMLElement, /dls-modal-header/)
    ).toBeInTheDocument();
  });

  it('should has className', () => {
    const { baseElement } = render(
      <ModalHeader ref={mockRef} className={'class-name'} />
    );

    expect(
      queryByClass(baseElement as HTMLElement, /class-name/)
    ).toBeInTheDocument();
  });

  describe('border', () => {
    it('should not has border', () => {
      createMockMutationObserver([
        {
          target: {}
        }
      ]);
      const { baseElement } = render(
        <Modal show>
          <ModalHeader ref={mockRef}>modal header</ModalHeader>
        </Modal>
      );
      expect(
        baseElement.querySelector('.dls-modal-br-header')
      ).not.toBeInTheDocument();
    });

    it('should has border with empty classList', () => {
      createMockMutationObserver([
        {
          target: {
            classList: {
              contains: () => false
            }
          }
        }
      ]);
      const { baseElement } = render(
        <Modal show>
          <ModalHeader ref={mockRef} border>
            modal header
          </ModalHeader>
        </Modal>
      );
      expect(
        baseElement.querySelector('.dls-modal-br-header')
      ).toBeInTheDocument();
    });

    it('should has border with classList', () => {
      createMockMutationObserver([
        {
          target: {
            classList: {
              contains: () => true
            }
          }
        }
      ]);
      const { baseElement } = render(
        <Modal show>
          <ModalHeader ref={mockRef} border>
            modal header
          </ModalHeader>
        </Modal>
      );
      expect(
        baseElement.querySelector('.dls-modal-br-header')
      ).toBeInTheDocument();
    });
  });

  it('should has close class name', () => {
    render(
      <Modal show>
        <ModalHeader
          ref={mockRef}
          closeButton
          closeClassName="close-class-name"
        >
          modal header
        </ModalHeader>
      </Modal>
    );

    const closeButton = screen.getByRole('button');

    expect(queryByClass(closeButton, /close-class-name/)).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('onHide', () => {
    const onHide = jest.fn();
    render(
      <Modal show>
        <ModalHeader ref={mockRef} closeButton onHide={onHide}>
          modal header
        </ModalHeader>
      </Modal>
    );

    screen.getByRole('button').click();

    expect(onHide).toBeCalled();
  });
});
