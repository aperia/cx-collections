import React, { MutableRefObject } from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';

// components
import ModalBody from './ModalBody';

// utils
import { queryByClass } from '../../test-utils';

const mockRef: MutableRefObject<HTMLDivElement | null> = { current: null };

describe('Render', () => {
  it('should render', () => {
    const { baseElement } = render(<ModalBody ref={mockRef} />);

    expect(
      queryByClass(baseElement as HTMLElement, /dls-modal-body/)
    ).toBeInTheDocument();
  });

  describe('padding', () => {
    it('should has padding', () => {
      const { baseElement } = render(<ModalBody ref={mockRef} />);

      expect(
        queryByClass(baseElement as HTMLElement, /no-padding/)
      ).not.toBeInTheDocument();
    });
    it('should not have padding', () => {
      const { baseElement } = render(<ModalBody ref={mockRef} noPadding />);

      expect(
        queryByClass(baseElement as HTMLElement, /no-padding/)
      ).toBeInTheDocument();
    });
  });
});
