import React, {
  useRef,
  useImperativeHandle,
  forwardRef,
  RefForwardingComponent
} from 'react';
import classnames from 'classnames';
import ModalBodyBase from 'react-bootstrap/ModalBody';

import { className as classConst, genAmtId } from '../../utils';
import { ModalBodyProps } from './types';

const ModalBody: RefForwardingComponent<HTMLDivElement, ModalBodyProps> = (
  { noPadding, className, children, dataTestId, ...props },
  ref
) => {
  const containerRef = useRef<HTMLDivElement | null>(null);

  useImperativeHandle(ref, () => containerRef.current!);

  return (
    <ModalBodyBase
      ref={containerRef}
      className={classnames(
        classConst.modal.BODY,
        noPadding && classConst.modal.NO_PADDING,
        className
      )}
      data-testid={genAmtId(dataTestId, 'dls-modal-body', 'Modal.Body')}
      {...props}
    >
      {children}
    </ModalBodyBase>
  );
};
export default forwardRef<HTMLDivElement, ModalBodyProps>(ModalBody);
