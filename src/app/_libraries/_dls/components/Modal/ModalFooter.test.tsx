import React, { MutableRefObject } from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import {
  createMockMutationObserver,
  clearMockMutationObserver
} from '../../test-utils/mocks/mockMutationObserver';

// components
import Modal from './Modal';
import ModalFooter from './ModalFooter';

// utils
import { queryByClass } from '../../test-utils';

const mockRef: MutableRefObject<HTMLDivElement | null> = { current: null };

beforeEach(() => {
  createMockMutationObserver();
});
afterEach(clearMockMutationObserver);

describe('Render', () => {
  it('should render', () => {
    const { baseElement } = render(<ModalFooter ref={mockRef} />);

    expect(
      queryByClass(baseElement as HTMLElement, /dls-modal-footer/)
    ).toBeInTheDocument();
  });

  it('should has className', () => {
    const { baseElement } = render(
      <ModalFooter ref={mockRef} className={'class-name'} />
    );

    expect(
      queryByClass(baseElement as HTMLElement, /class-name/)
    ).toBeInTheDocument();
  });

  describe('border', () => {
    it('should not has border', () => {
      const { baseElement } = render(
        <Modal show>
          <ModalFooter ref={mockRef} />
        </Modal>
      );

      expect(
        baseElement.querySelector('.dls-modal-br-footer')
      ).not.toBeInTheDocument();
    });

    it('should has border', () => {
      const { baseElement } = render(
        <Modal show>
          <ModalFooter ref={mockRef} border>
            modal header
          </ModalFooter>
        </Modal>
      );
      expect(
        baseElement.querySelector('.dls-modal-br-footer')
      ).toBeInTheDocument();
    });

    it('should not trigger MutationObserver', () => {
      createMockMutationObserver([
        {
          target: {}
        }
      ]);

      const { baseElement } = render(
        <Modal show>
          <ModalFooter ref={mockRef} border>
            modal header
          </ModalFooter>
        </Modal>
      );
      expect(
        baseElement.querySelector('.dls-modal-br-footer')
      ).toBeInTheDocument();
    });

    it('should trigger MutationObserver with empty classList', () => {
      createMockMutationObserver([
        {
          target: {
            classList: {
              contains: () => false
            }
          }
        }
      ]);

      const { baseElement } = render(
        <Modal show>
          <ModalFooter ref={mockRef} border>
            modal header
          </ModalFooter>
        </Modal>
      );
      expect(
        baseElement.querySelector('.dls-modal-br-footer')
      ).toBeInTheDocument();
    });

    it('should trigger MutationObserver with classList', () => {
      createMockMutationObserver([
        {
          target: {
            classList: {
              contains: () => true
            }
          }
        }
      ]);

      const { baseElement } = render(
        <Modal show>
          <ModalFooter ref={mockRef} border>
            modal header
          </ModalFooter>
        </Modal>
      );
      expect(
        baseElement.querySelector('.dls-modal-br-footer')
      ).toBeInTheDocument();
    });
  });
});

describe('Actions', () => {
  it('onCancel', () => {
    const onCancel = jest.fn();

    render(
      <ModalFooter
        ref={mockRef}
        cancelButtonText="Cancel"
        onCancel={onCancel}
      />
    );

    screen.getByText('Cancel').click();

    expect(onCancel).toBeCalled();
  });

  it('onOk', () => {
    const onOk = jest.fn();

    render(<ModalFooter ref={mockRef} okButtonText="Ok" onOk={onOk} />);

    screen.getByText('Ok').click();

    expect(onOk).toBeCalled();
  });
});
