import { createContext } from 'react';
import { ModalStateProps } from './types';

const ModalContext = createContext<ModalStateProps>({ isInsideModal: false });

const { Provider: ModalProvider } = ModalContext;

export { ModalProvider };

export default ModalContext;
