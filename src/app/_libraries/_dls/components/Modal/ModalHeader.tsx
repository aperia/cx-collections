import React, {
  forwardRef,
  RefForwardingComponent,
  useRef,
  useImperativeHandle,
  useEffect
} from 'react';
import ModalHeaderBase from 'react-bootstrap/ModalHeader';
import classnames from 'classnames';
import get from 'lodash.get';
import isEmpty from 'lodash.isempty';

import { ModalHeaderProps } from './types';
import { className as classConst, genAmtId } from '../../utils';
import Button from '../../components/Button';
import Icon from '../../components/Icon';

const ModalHeader: RefForwardingComponent<HTMLDivElement, ModalHeaderProps> = (
  {
    closeButton = false,
    border = false,
    className,
    closeClassName,
    children,

    onHide,

    dataTestId,
    ...props
  },
  ref
) => {
  const containerRef = useRef<HTMLDivElement | null>(null);

  useImperativeHandle(ref, () => containerRef.current!);

  useEffect(() => {
    const { BORDER_HEADER, CONTENT } = classConst.modal;
    const parentElement = containerRef.current?.closest(`.${CONTENT}`);

    if (!parentElement) return;

    if (border) {
      parentElement.classList.add(BORDER_HEADER);
    } else {
      parentElement.classList.remove(BORDER_HEADER);
    }

    // watch the changing of modal-content className for adding BORDER_HEADER
    const mutation = new MutationObserver((records) => {
      const target = get(records, ['0', 'target']) as typeof parentElement;

      if (!border || isEmpty(target)) return;
      // BORDER_HEADER was existed
      if (border && target.classList.contains(BORDER_HEADER)) return;

      parentElement.classList.add(BORDER_HEADER);
    });

    // Modal will replace all class which was added before, Ex: add class 'loading'
    // We need to watch attribute class to add if BORDER_HEADER was replaced
    mutation.observe(parentElement!, { attributeFilter: ['class'] });

    return () => mutation.disconnect();
  }, [border]);

  const containerId = genAmtId(dataTestId, 'dls-modal-header', 'Modal.Header');

  return (
    <ModalHeaderBase
      ref={containerRef}
      className={classnames(classConst.modal.HEADER, className)}
      data-testid={containerId}
      {...props}
    >
      {children}
      {closeButton && (
        <Button
          tabIndex={-1}
          variant="icon-secondary" 
          onClick={onHide}
          dataTestId={genAmtId(containerId, 'close-btn', '')}
        >
          <Icon name="close" className={classnames(closeClassName)} />
        </Button>
      )}
    </ModalHeaderBase>
  );
};

export default forwardRef<HTMLDivElement, ModalHeaderProps>(ModalHeader);
