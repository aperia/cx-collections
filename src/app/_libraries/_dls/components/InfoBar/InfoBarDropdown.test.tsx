import React from 'react';
import '@testing-library/jest-dom';
import InfoBarDropdown from './InfoBarDropdown';
import { fireEvent, render, screen } from '@testing-library/react';
import { IconNames } from '../Icon/constants';
import { queryByClass } from '../../test-utils';

describe('InfoBarDropdown component', () => {
  it('should render default component', () => {
    const ref = React.createRef<HTMLDivElement>();
    const wrapper = render(<InfoBarDropdown ref={ref} />);
    expect(
      wrapper.container.querySelector('h6[class="infobar-dropdown-header"]')
    ).toBeInTheDocument();
  });

  it('should render items', () => {
    const ref = React.createRef<HTMLDivElement>();
    const items = [
      {
        text: 'Item 1',
        icon: {
          name: 'add-file' as IconNames
        }
      },
      {
        text: 'Item 2',
        disabled: true
      }
    ];

    render(<InfoBarDropdown ref={ref} items={items} />);

    expect(screen.getByText('Item 1')).toBeInTheDocument();
    expect(screen.getByText('Item 2')).toBeInTheDocument();
  });

  it('should call onActive', () => {
    const ref = React.createRef<HTMLDivElement>();
    const onActiveSpy = jest.fn();

    const wrapper = render(
      <InfoBarDropdown ref={ref} active={true} onActive={onActiveSpy} />
    );

    fireEvent.click(wrapper.baseElement);

    expect(onActiveSpy).toBeCalled();
  });

  it('should call onActive > with id', () => {
    const ref = React.createRef<HTMLDivElement>();
    const onActiveSpy = jest.fn();

    const wrapper = render(
      <InfoBarDropdown id="id" ref={ref} active={true} onActive={onActiveSpy} />
    );

    fireEvent.click(wrapper.baseElement);

    expect(onActiveSpy).toBeCalled();
  });

  it('should not call onActive > click header', () => {
    const ref = React.createRef<HTMLDivElement>();
    const items = [
      {
        text: 'Item 1',
        icon: {
          name: 'add-file' as IconNames
        },
        disabled: true
      },
      {
        text: 'Item 2',
        disabled: true
      }
    ];
    const onActiveSpy = jest.fn();

    const wrapper = render(
      <InfoBarDropdown
        id="id"
        ref={ref}
        items={items}
        active={true}
        onActive={onActiveSpy}
        classes={{
          container: 'disabled'
        }}
      />
    );

    const ele = queryByClass(wrapper.container, 'infobar-dropdown-header');
    fireEvent.click(ele as HTMLElement);

    expect(onActiveSpy).not.toBeCalled();
  });

  it('should not call onActive >', () => {
    const ref = React.createRef<HTMLDivElement>();
    const items = [
      {
        text: 'Item 1',
        icon: {
          name: 'add-file' as IconNames
        },
        disabled: true
      },
      {
        text: 'Item 2',
        disabled: true
      }
    ];
    const onActiveSpy = jest.fn();

    const wrapper = render(
      <InfoBarDropdown
        id="id"
        ref={ref}
        items={items}
        active={true}
        onActive={onActiveSpy}
      />
    );

    // getByText(wrapper.container, 'aaa');
    const element = queryByClass(wrapper.container, 'infobar-dropdown active');
    fireEvent.click(element as HTMLElement);

    expect(onActiveSpy).not.toBeCalled();
  });

  it('should not call onActive > autoClose is false', () => {
    const ref = React.createRef<HTMLDivElement>();
    const onActiveSpy = jest.fn();

    const wrapper = render(
      <InfoBarDropdown
        id="id"
        autoClose={false}
        ref={ref}
        active={true}
        onActive={onActiveSpy}
      />
    );

    fireEvent.click(wrapper.baseElement);

    expect(onActiveSpy).not.toBeCalled();
  });
});
