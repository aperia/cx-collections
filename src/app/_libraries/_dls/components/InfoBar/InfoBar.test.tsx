import React, { ReactType } from 'react';
import '@testing-library/jest-dom';
import { fireEvent, render, screen } from '@testing-library/react';
import InfoBar from './InfoBar';
import { act } from 'react-dom/test-utils';
import { queryByClass } from '../../test-utils';
import { ComponentProps, InfoBarItem } from '.';
import Button from '../Button';

const MockComponent: React.FC = () => <div>Mock</div>;

describe('InfoBar', () => {
  it('should render component', () => {
    jest.useFakeTimers();
    act(() => {
      const wrapper = render(<InfoBar />);
      jest.runAllTimers();
      expect(
        wrapper.container.querySelector(
          'aside[class="infobar-container collapsed"]'
        )
      ).toBeTruthy();
      expect(
        wrapper.container.querySelector('nav[class="infobar-nav-container"]')
      ).toBeTruthy();
      expect(
        wrapper.container.querySelector('div[class="infobar-body"]')
      ).toBeTruthy();
      expect(
        wrapper.container.querySelector('div[class="infobar-empty active"]')
      ).toBeTruthy();
    });
  });

  it('should render collapsed component > expand = false', () => {
    jest.useFakeTimers();
    const items = [
      {
        id: 'pin',
        name: 'First item'
      },
      {
        id: 'secondary',
        name: 'Secondary item'
      }
    ];

    act(() => {
      const ref = React.createRef<HTMLElement>();

      const wrapper = render(
        <InfoBar ref={ref} items={items} expand={false} />
      );

      jest.runAllTimers();

      const element = queryByClass(
        wrapper.container,
        'infobar-container collapsed'
      );

      expect(element).toBeInTheDocument();
    });
  });

  it('should render pin component > expand = true, pinned = pin', () => {
    jest.useFakeTimers();
    const items = [
      {
        id: 'pin',
        name: 'First item',
        component: MockComponent
      },
      {
        id: 'secondary',
        name: 'Secondary item'
      }
    ];
    act(() => {
      const wrapper = render(
        <InfoBar items={items} expand={true} pinned="pin" />
      );

      jest.runAllTimers();

      const element = queryByClass(
        wrapper.container,
        'inforbar-content-pinned'
      );

      expect(element).toBeInTheDocument();
      expect(element?.innerHTML).toEqual('<div>Mock</div>');
    });
  });

  it('should be expand > pinned having value', () => {
    jest.useFakeTimers();
    const items = [
      {
        id: 'pin',
        name: 'First item',
        component: MockComponent
      },
      {
        id: 'secondary',
        name: 'Secondary item'
      }
    ];

    act(() => {
      const wrapper = render(<InfoBar items={items} pinned="pin" />);

      jest.runAllTimers();

      const element = queryByClass(wrapper.container, 'infobar-container');
      const pinElement = queryByClass(
        wrapper.container,
        'inforbar-content-pinned'
      );

      expect(element).toBeInTheDocument();
      expect(pinElement?.innerHTML).toEqual('<div>Mock</div>');
    });
  });

  it('should be collapsed > empty pinned', () => {
    jest.useFakeTimers();
    const items = [
      {
        id: 'pin',
        name: 'First item'
      },
      {
        id: 'secondary',
        name: 'Secondary item'
      }
    ];

    act(() => {
      const wrapper = render(<InfoBar items={items} pinned="" />);

      jest.runAllTimers();

      const element = queryByClass(
        wrapper.container,
        'infobar-container collapsed'
      );

      expect(element).toBeInTheDocument();
    });
  });

  it('should be controlled > onExpand with expand = false', () => {
    jest.useFakeTimers();
    const items = [
      {
        id: 'pin',
        name: 'First item'
      },
      {
        id: 'secondary',
        name: 'Secondary item'
      }
    ];

    const onExpandSpy = jest.fn();

    act(() => {
      const wrapper = render(
        <InfoBar items={items} expand={false} onExpand={onExpandSpy} />
      );

      jest.runAllTimers();

      const ele = queryByClass(
        wrapper.container,
        'infobar-nav-item infobar-nav-item mt-auto css-tooltip-wrapper'
      );
      ele?.click();

      const element = queryByClass(
        wrapper.container,
        'infobar-container collapsed'
      );
      expect(element).toBeInTheDocument();
      expect(onExpandSpy).toBeCalled();
    });
  });

  it('should be controlled > onExpand without expand', () => {
    jest.useFakeTimers();
    const items = [
      {
        id: 'pin',
        name: 'First item'
      },
      {
        id: 'secondary',
        name: 'Secondary item'
      }
    ];

    const onExpandSpy = jest.fn();

    act(() => {
      const wrapper = render(<InfoBar items={items} onExpand={onExpandSpy} />);

      jest.runAllTimers();

      const ele = queryByClass(
        wrapper.container,
        'infobar-nav-item infobar-nav-item mt-auto css-tooltip-wrapper'
      );
      ele?.click();

      const element = queryByClass(
        wrapper.container,
        'infobar-container collapsed'
      );
      expect(element).toBeInTheDocument();
      expect(onExpandSpy).toBeCalled();
    });
  });

  it('should be controlled > onCollapse with expand = true', () => {
    jest.useFakeTimers();
    const items = [
      {
        id: 'pin',
        name: 'First item'
      },
      {
        id: 'secondary',
        name: 'Secondary item'
      }
    ];

    const onCollapsedSpy = jest.fn();

    act(() => {
      const wrapper = render(
        <InfoBar items={items} expand={true} onCollapse={onCollapsedSpy} />
      );

      jest.runAllTimers();

      const ele = queryByClass(
        wrapper.container,
        'infobar-nav-item infobar-nav-item mt-auto css-tooltip-wrapper'
      );
      ele?.click();

      expect(onCollapsedSpy).toBeCalled();
    });
  });

  it('should be keep active item', () => {
    jest.useFakeTimers();
    const items = [
      {
        id: 'pin',
        name: 'First item'
      },
      {
        id: 'secondary',
        name: 'Secondary item',
        icon: { name: 'cross' }
      },
      {
        id: '',
        name: 'Third item',
        icon: { key: '', name: 'time' }
      }
    ] as InfoBarItem[];

    act(() => {
      const wrapper = render(<InfoBar items={items} pinned="pin" />);

      jest.runAllTimers();

      const ele = wrapper.container.querySelector(
        'i[class="icon icon-time size-6x"] '
      );
      fireEvent.click(ele as HTMLElement);

      const div = wrapper.container.querySelector(
        'div[class="infobar-nav-item active"]'
      );
      expect(div?.innerHTML).toEqual('<i class="icon icon-time size-6x"></i>');
    });
  });

  it('should be changed active item', () => {
    jest.useFakeTimers();
    const items = [
      {
        id: 'pin',
        name: 'First item'
      },
      {
        id: 'secondary',
        name: 'Secondary item',
        icon: { name: 'cross' }
      },
      {
        id: '',
        name: 'Third item',
        icon: { key: '', name: 'time' }
      }
    ] as InfoBarItem[];

    act(() => {
      const wrapper = render(<InfoBar items={items} pinned="pin" />);

      jest.runAllTimers();

      const ele = wrapper.container.querySelector(
        'i[class="icon icon-cross size-6x"] '
      );
      fireEvent.click(ele as HTMLElement);

      const div = wrapper.container.querySelector(
        'div[class="infobar-nav-item active"]'
      );
      expect(div?.innerHTML).toEqual('<i class="icon icon-time size-6x"></i>');
    });
  });

  it('should be call onClickIcon', () => {
    jest.useFakeTimers();
    const spy = jest.fn();
    const items = [
      {
        id: 'pin',
        name: 'First item'
      },
      {
        id: 'secondary',
        name: 'Secondary item',
        icon: { name: 'cross' },
        mode: 'static',
        onClickIcon: spy
      },
      {
        id: '',
        name: 'Third item',
        icon: { key: '', name: 'time' }
      }
    ] as InfoBarItem[];

    act(() => {
      const wrapper = render(<InfoBar items={items} pinned="pin" />);

      jest.runAllTimers();

      const ele = wrapper.container.querySelector(
        'i[class="icon icon-cross size-6x"] '
      );
      fireEvent.click(ele as HTMLElement);

      expect(spy).toBeCalled();
    });
  });

  const PinComponent: React.FC<ComponentProps> = ({
    handlePin,
    handleUnpin
  }) => {
    return (
      <div>
        <Button className="d-inline" variant="primary" onClick={handlePin}>
          Pin
        </Button>
        <Button className="d-inline" variant="primary" onClick={handleUnpin}>
          UnPin
        </Button>
      </div>
    );
  };

  it('should call onHandlePin', () => {
    jest.useFakeTimers();

    const items = [
      {
        id: 'pin',
        name: 'First item',
        component: PinComponent,
        icon: { name: 'cross' }
      },
      {
        id: 'secondary',
        name: 'Secondary item'
      },
      {
        id: 'third',
        name: 'Third item',
        icon: { name: 'time' }
      }
    ] as InfoBarItem[];

    const onHandlePinSpy = jest.fn();

    act(() => {
      const wrapper = render(
        <InfoBar
          onHandlePin={onHandlePinSpy}
          items={items}
          expand={true}
          pinned="pin"
        />
      );

      jest.runAllTimers();

      const ele = wrapper.container.querySelector(
        'i[class="icon icon-cross size-6x"] '
      );
      fireEvent.click(ele as HTMLElement);

      const btn = screen.getByText('Pin');
      btn.click();

      expect(onHandlePinSpy).toBeCalled();
    });
  });

  it('should call onHandlePin', () => {
    jest.useFakeTimers();

    const items = [
      {
        id: 'pin',
        name: 'First item',
        component: PinComponent,
        icon: { name: 'cross' }
      },
      {
        id: 'secondary',
        name: 'Secondary item'
      },
      {
        id: '',
        name: 'Third item',
        icon: { name: 'time' }
      }
    ] as InfoBarItem[];

    const onHandlePinSpy = jest.fn();

    act(() => {
      const wrapper = render(
        <InfoBar onHandlePin={onHandlePinSpy} items={items} expand={true} />
      );

      jest.runAllTimers();

      const ele = wrapper.container.querySelector(
        'i[class="icon icon-cross size-6x"] '
      );
      fireEvent.click(ele as HTMLElement);
      jest.runAllTimers();

      const btn = screen.getByText('Pin');
      btn.click();

      expect(onHandlePinSpy).toBeCalled();
    });
  });

  it('should call onHandleUnPin', () => {
    jest.useFakeTimers();

    const items = [
      {
        id: '',
        name: 'First item',
        component: PinComponent,
        icon: { name: 'cross' }
      },
      {
        id: 'secondary',
        name: 'Secondary item'
      },
      {
        id: 'third',
        name: 'Third item',
        icon: { name: 'time' }
      }
    ] as InfoBarItem[];

    const onHandleUnPinSpy = jest.fn();

    act(() => {
      const wrapper = render(
        <InfoBar
          onHandleUnPin={onHandleUnPinSpy}
          items={items}
          expand={true}
          pinned=""
        />
      );

      jest.runAllTimers();

      const ele = wrapper.container.querySelector(
        'i[class="icon icon-cross size-6x"] '
      );
      fireEvent.click(ele as HTMLElement);

      const btn = screen.getByText('UnPin');
      btn.click();

      expect(onHandleUnPinSpy).toBeCalled();
    });
  });

  it('should call onHandleUnPin', () => {
    jest.useFakeTimers();

    const items = [
      {
        id: 'pin',
        name: 'First item',
        component: PinComponent,
        icon: { name: 'cross' },
        separator: true
      },
      {
        id: 'secondary',
        name: 'Secondary item'
      },
      {
        id: 'third',
        name: 'Third item',
        icon: { name: 'time' }
      }
    ] as InfoBarItem[];

    const onHandleUnPinSpy = jest.fn();

    act(() => {
      const wrapper = render(
        <InfoBar
          onHandleUnPin={onHandleUnPinSpy}
          items={items}
          expand={true}
          pinned="pin"
        />
      );

      jest.runAllTimers();

      const ele = wrapper.container.querySelector(
        'i[class="icon icon-cross size-6x"] '
      );
      fireEvent.click(ele as HTMLElement);

      const btn = screen.getByText('UnPin');
      btn.click();

      expect(onHandleUnPinSpy).toBeCalled();
    });
  });

  it('should run onClickItem of dropdown item', () => {
    jest.useFakeTimers();

    const items = [
      {
        id: 'pin',
        name: 'First item',
        component: PinComponent,
        icon: { name: 'cross' }
      },
      {
        id: 'secondary',
        name: 'Secondary item'
      },
      {
        id: 'third',
        name: 'Third item',
        icon: { name: 'time' },
        mode: 'dropdown'
      }
    ] as InfoBarItem[];

    const onHandleUnPinSpy = jest.fn();

    act(() => {
      const wrapper = render(
        <InfoBar
          onHandleUnPin={onHandleUnPinSpy}
          items={items}
          expand={true}
          pinned="pin"
        />
      );

      jest.runAllTimers();

      const ele = wrapper.container.querySelector(
        'i[class="icon icon-time size-6x"]'
      );
      fireEvent.click(ele as HTMLElement);

      jest.runAllTimers();

      expect(
        wrapper.container.querySelector(
          'div[class="infobar-nav-item active-dropdown"]'
        )
      ).toBeInTheDocument();
    });
  });

  it('should call onHandlePinSpy', () => {
    jest.useFakeTimers();

    const items = [
      {
        id: 'pin',
        name: 'First item',
        icon: { name: 'cross' },
        mode: 'floating',
        keepComponent: true,
        component: ((props) => <PinComponent {...props} />) as ReactType
      },
      {
        id: 'secondary',
        name: 'Secondary item',
        keepComponent: false
      },
      {
        id: 'third',
        name: 'Third item',
        icon: { name: 'time' },
        mode: 'dropdown',
        component: (({}) => <MockComponent />) as ReactType
      }
    ] as InfoBarItem[];

    const onHandlePinSpy = jest.fn();

    act(() => {
      render(
        <InfoBar
          onHandlePin={onHandlePinSpy}
          items={items}
          expand={true}
          pinned="pin"
        />
      );

      jest.runAllTimers();

      const btn = screen.getByText('Pin');
      btn.click();

      expect(onHandlePinSpy).toBeCalled();
    });
  });

  it('should render item > keepComponent is true', () => {
    jest.useFakeTimers();

    const items = [
      {
        id: 'pin',
        name: 'First item',
        icon: { name: 'cross' },
        mode: 'floating'
      },
      {
        id: 'secondary',
        name: 'Secondary item',
        keepComponent: true,
        component: MockComponent
      },
      {
        id: 'third',
        name: 'Third item',
        icon: { name: 'time' },
        mode: 'dropdown',
        component: (({}) => <MockComponent />) as ReactType
      }
    ] as InfoBarItem[];

    const onHandleUnPinSpy = jest.fn();

    act(() => {
      const wrapper = render(
        <InfoBar
          onHandleUnPin={onHandleUnPinSpy}
          items={items}
          expand={true}
          pinned="pin"
        />
      );

      jest.runAllTimers();

      const div = queryByClass(wrapper.container, 'infobar-content hide');
      expect(div?.innerHTML).toEqual('<div>Mock</div>');
    });
  });
});
