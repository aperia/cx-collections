import React, {
  useState,
  useCallback,
  useEffect,
  forwardRef,
  useRef,
  useImperativeHandle,
  RefForwardingComponent,
  ReactNode
} from 'react';

import useInfoBarItems, { InfoBarItem } from './useInfoBarItems';
import { TooltipProps } from '../../components/Tooltip';
import Toggle from './Toggle';
import { InfoBarProvider } from './InfoBarContext';

import classnames from 'classnames';
import isFunction from 'lodash.isfunction';
import isBoolean from 'lodash.isboolean';
import isNil from 'lodash.isnil';
import { className, genAmtId } from '../../utils';

export interface InfoBarProps extends DLSId {
  items?: InfoBarItem[];

  emptyElement?: ReactNode;
  collapseText?: string;
  expandText?: string;
  classes?: {
    container?: string;
    navContainer?: string;
    navItem?: string;
    body?: string;
  };
  toolTipProps?: TooltipProps;

  pinned?: string;
  onHandlePin?: (id: string) => void;
  onHandleUnPin?: () => void;

  expand?: boolean;
  onExpand?: () => void;
  onCollapse?: () => void;

  toggle?: boolean;
}

const InfoBar: RefForwardingComponent<HTMLElement, InfoBarProps> = (
  {
    items = [],

    emptyElement,
    collapseText = 'Collapse Sidebar',
    expandText = 'Expand Sidebar',
    classes,
    toolTipProps,

    pinned,
    onHandlePin,
    onHandleUnPin,

    expand: expandProp,
    onExpand,
    onCollapse,

    toggle = true,

    id,
    dataTestId
  },
  ref
) => {
  const {
    container: classContainer,
    navContainer: classNavContainer,
    navItem: classNavItem,
    body: classBody
  } = classes || {};

  const containerRef = useRef<HTMLElement | null>(null);

  const [expand, setExpand] = useState(
    isBoolean(expandProp) ? expandProp : !!pinned
  );
  const [itemSelected, setItemSelected] = useState(pinned);
  const [itemIdPinned, setItemIdPinned] = useState(pinned);
  const [itemDropdown, setItemDropdown] = useState<string>();

  // handle controlled/uncontrolled expand
  const handleExpandMode = useCallback(
    (nextExpand: boolean, callback?: () => void) => {
      // controlled mode
      if (isBoolean(expandProp)) {
        return isFunction(callback) && callback();
      }

      // uncontrolled mode
      setExpand(nextExpand);
      isFunction(callback) && callback();
    },
    [expandProp]
  );

  // handle controlled/uncontrolled pinned
  const handlePinnedMode = useCallback(
    (id: string, callback: ((id: string) => void) | undefined) => {
      // controlled
      if (!isNil(pinned)) {
        return isFunction(callback) && callback(id);
      }

      // uncontrolled
      setItemIdPinned(id);
      isFunction(callback) && callback(id);
    },
    [pinned]
  );

  const handleSelectedItem = useCallback((item: string) => {
    setItemSelected((state) => (state === item ? '' : item));
  }, []);

  const handleClickItem = useCallback((item: string) => {
    setItemDropdown(item);
  }, []);

  // handle expand
  const handleExpand = useCallback(
    () => handleExpandMode(true, onExpand),
    [handleExpandMode, onExpand]
  );

  // handle collapse
  const handleCollapse = useCallback(
    () => handleExpandMode(false, onCollapse),
    [handleExpandMode, onCollapse]
  );

  // handle pin
  const handlePin = useCallback(
    (id: string) => {
      handleExpand();
      handlePinnedMode(id, onHandlePin);

      setItemSelected(id);
    },
    [handleExpand, onHandlePin, handlePinnedMode]
  );

  // handle unpin
  const handleUnpin = useCallback(() => {
    handleCollapse();
    handlePinnedMode('', onHandleUnPin);

    if (itemSelected === itemIdPinned) setItemSelected('');
  }, [
    handleCollapse,
    itemSelected,
    itemIdPinned,
    onHandleUnPin,
    handlePinnedMode
  ]);

  const { navItems, bodyItems, dropdownItems } = useInfoBarItems({
    items,
    itemSelected,
    itemIdPinned,
    itemDropdown,
    expand: !expand,
    onSelectedItem: handleSelectedItem,
    onClickItem: handleClickItem,
    handleExpand,
    handleCollapse,
    handlePin,
    handleUnpin,
    emptyElement,
    dataTestId
  });

  useImperativeHandle(ref, () => containerRef.current!);

  // watching props
  useEffect(() => {
    isBoolean(expandProp) && setExpand(expandProp);
  }, [expandProp]);
  useEffect(() => setItemIdPinned(pinned), [pinned]);

  // handle watching pinned change -> trigger default behaviors
  useEffect(() => {
    // reset item selected to empty string if item selected is pinned
    if (itemIdPinned && itemSelected && itemIdPinned === itemSelected) {
      setItemSelected('');
    }
  }, [itemIdPinned, itemSelected]);

  // if itemIdPinned change -> trigger default behaviors
  useEffect(() => {
    if (isBoolean(expandProp)) return;

    let nextState, nextCallback;
    if (itemIdPinned) {
      nextState = true;
      nextCallback = onExpand;
    }

    if (itemIdPinned === '') {
      nextState = false;
      nextCallback = onCollapse;
    }

    if (isBoolean(nextState)) {
      handleExpandMode(nextState, nextCallback);
    }
  }, [itemIdPinned, handleExpandMode, onExpand, onCollapse, expandProp]);

  return (
    <InfoBarProvider value={{ isInsideInfoBar: true }}>
      <aside
        ref={containerRef}
        className={classnames(
          className.infoBar.CONTAINER,
          !expand && className.infoBar.COLLAPSED,
          classContainer
        )}
        id={id}
        data-testid={genAmtId(dataTestId, 'dls-info-bar', 'InfoBar')}
      >
        <nav
          className={classnames(
            className.infoBar.NAV_CONTAINER,
            classNavContainer
          )}
        >
          {navItems}
          {toggle && (
            <Toggle
              expand={expand}
              onCollapse={handleCollapse}
              onExpand={handleExpand}
              className={classnames(
                className.infoBar.NAV_ITEM,
                className.infoBar.MTA,
                className.infoBar.CSS_TOOLTIP_WRAPPER,
                classNavItem
              )}
              variant="primary"
              placement="right"
              element={expand ? collapseText : expandText}
              dataTestId={dataTestId}
            />
          )}
        </nav>
        {dropdownItems}
        <div className={classnames(className.infoBar.BODY, classBody)}>
          {bodyItems}
        </div>
      </aside>
    </InfoBarProvider>
  );
};

export default forwardRef<HTMLElement, InfoBarProps>(InfoBar);
