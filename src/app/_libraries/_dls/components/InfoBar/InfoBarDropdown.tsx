import React, {
  useEffect,
  useRef,
  forwardRef,
  useImperativeHandle,
  DetailedHTMLProps,
  HTMLAttributes,
  RefForwardingComponent,
  FC,
  ReactNode
} from 'react';
import classnames from 'classnames';
import isFunction from 'lodash.isfunction';

import { className, genAmtId } from '../../utils';
import Icon, { IconProps } from '../../components/Icon';

export interface InfoBarDropDownItem
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement> {
  text?: string;
  icon?: IconProps;
  disabled?: boolean;
  tooltip?: string;
}

export interface InfoBarDropdownProps extends DLSId {
  id?: string;
  active?: boolean;
  header?: ReactNode;
  items?: InfoBarDropDownItem[];
  onActive?: any;
  autoClose?: boolean;
  classes?: {
    container?: string;
    header?: string;
    items?: string;
  };
}

export interface DropdownItemProps extends DLSId {
  disabled?: boolean;
  onClick?: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
  className?: string;
  icon?: IconProps;
  text?: string;
}

const DropdownItem: FC<DropdownItemProps> = ({
  disabled,
  onClick,
  className: classNameProp,
  icon,
  text,

  dataTestId,
  ...itemProps
}) => (
  <div
    onClick={!disabled ? onClick : undefined}
    className={classnames(
      className.infoBar.DROPDOWN_ITEM,
      { disabled },
      classNameProp
    )}
    data-testid={genAmtId(
      dataTestId,
      'dls-info-bar-dropdown-item',
      'InfoBar.DropdownItem'
    )}
    {...itemProps}
  >
    {icon && (
      <Icon
        className={className.infoBar.DROPDOWN_ITEM_ICON}
        name={icon.name}
        size={icon.size || '5x'}
      />
    )}
    <span
      className={classnames(
        className.infoBar.DROPDOWN_ITEM_LABEL,
        disabled && className.infoBar.ITEM_DISABLED
      )}
    >
      {text}
    </span>
  </div>
);

const InfoBarDropdown: RefForwardingComponent<
  HTMLDivElement,
  InfoBarDropdownProps
> = (
  {
    id,
    active,
    header,
    items = [],
    onActive,
    autoClose = true,
    classes,
    dataTestId
  },
  ref
) => {
  const {
    container: classContainer,
    header: classHeader,
    items: classItems
  } = classes || {};

  const containerRef = useRef<HTMLDivElement | null>(null);

  useImperativeHandle(ref, () => containerRef.current!);

  useEffect(() => {
    if (!active) return;

    const handleClickOutSide = (event: MouseEvent) => {
      const targetElement = event.target as HTMLElement;
      if (targetElement.closest('div')?.classList.contains('disabled')) return;

      if (!autoClose) {
        return;
      }

      if (
        !containerRef.current ||
        (containerRef.current.contains(targetElement) &&
          !targetElement.closest(`.${className.infoBar.DROPDOWN_ITEM}`))
      ) {
        return;
      }

      const nextId = id ? '' : id;
      isFunction(onActive) && onActive(nextId);
    };

    document.addEventListener('click', handleClickOutSide);
    return () => document.removeEventListener('click', handleClickOutSide);
  }, [active, autoClose, id, onActive]);

  return (
    <div
      id={id}
      ref={containerRef}
      className={classnames(
        className.infoBar.DROPDOWN,
        { active },
        classContainer
      )}
      data-testid={genAmtId(
        dataTestId,
        'info-bar-dropdown',
        'InfoBar.Dropdown'
      )}
    >
      <h6
        className={classnames(className.infoBar.DROPDOWN_HEADER, classHeader)}
        data-testid={genAmtId(
          dataTestId,
          'info-bar-dropdown_header',
          'InfoBar.Dropdown'
        )}
      >
        {header}
      </h6>
      {Array.isArray(items) &&
        items.map(({ ...itemProps }, index) => (
          <DropdownItem key={index} className={classItems} {...itemProps} />
        ))}
    </div>
  );
};

export default forwardRef<HTMLDivElement, InfoBarDropdownProps>(
  InfoBarDropdown
);
