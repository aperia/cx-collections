import { useEffect } from 'react';
import { StorageContextArgs } from '../../providers';

export interface MouseDownOutsideDependencies {
  opened?: boolean;
  popupElement?: HTMLElement | null;
  reference?: HTMLElement | null;
  storage: StorageContextArgs;
}

export interface MouseDownOutsideHook {
  (
    coreRef: React.MutableRefObject<Record<string, any>>,
    dependencies: MouseDownOutsideDependencies
  ): void;
}

const useMouseDownOutside: MouseDownOutsideHook = (
  coreRef,
  { opened, popupElement, reference, storage }
) => {
  useEffect(() => {
    if (!opened) return;

    const handleMouseDownOutside = (event: MouseEvent) => {
      if (!reference || !popupElement) return;

      const target = event.target as HTMLElement;
      const { handleOpenedMode } = coreRef.current;

      // element should be ignored
      if (reference.contains(target) || popupElement.contains(target)) {
        return;
      }

      // elements should be ignored
      if (Array.isArray(storage.values)) {
        // this case below maybe never happens, comment out for unit test cover
        // const shouldIgnoreElement = storage.values.find(
        //   (element: HTMLElement) => {
        //     console.log('element...', element.className);
        //     console.log('popupElement...', popupElement.className);
        //     return element !== popupElement && element.contains(target);
        //   }
        // );
        // if (shouldIgnoreElement) return;
      }

      handleOpenedMode?.(false);
    };

    window.addEventListener('mousedown', handleMouseDownOutside);
    return () => {
      window.removeEventListener('mousedown', handleMouseDownOutside);
    };
  }, [coreRef, opened, popupElement, reference, storage]);
};

export default useMouseDownOutside;
