import React from 'react';

// testing library
import { fireEvent, render } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';

// mocks
import '../../test-utils/mocks/mockCanvas';

// components
import MultiSelect from '../MultiSelect';

let count = 100;
const data: any[] = [];
while (count > 0) {
  data.push({
    id: count,
    description: `Item ${count}`
  });
  count--;
}

const dataItems = data.map((item, index) => (
  <MultiSelect.Item key={item.id} label={item.description} value={item} />
));

const MockPopupBase: React.FC = props => {
  return (
    <div>
      <MultiSelect
        ref={{ current: null }}
        label="MultiSelect"
        placeholder="MultiSelect placeholder"
        variant="group"
        checkAll
        searchBar
        {...props}
      >
        {dataItems}
      </MultiSelect>
    </div>
  );
};

describe('PopupBase', () => {
  describe('should cover handle scroll to popup', () => {
    it('should cover if popupElement does not contains event.target', () => {
      jest.useFakeTimers();
      const { container } = render(<MockPopupBase />);
      const textFieldContainerElement = container.querySelector(
        '.text-field-container'
      )!;
      userEvent.click(textFieldContainerElement);
      jest.runAllTimers();

      fireEvent.scroll(document.body, { target: { scrollY: 100 } });

      expect(true).toBeTruthy();
    });

    it('should cover if popupElement does not contains event.target', () => {
      jest.useFakeTimers();
      const { container } = render(<MockPopupBase />);
      const textFieldContainerElement = container.querySelector(
        '.text-field-container'
      )!;
      userEvent.click(textFieldContainerElement);
      jest.runAllTimers();

      fireEvent.scroll(document.body, {
        target: { scrollY: 100, closest: () => true }
      });

      expect(true).toBeTruthy();
    });
  });

  it('should call handleOpenedMode with uncontrolled flow', () => {
    jest.useFakeTimers();
    const { container, baseElement } = render(<MockPopupBase />);
    const textFieldContainerElement = container.querySelector(
      '.text-field-container'
    )!;
    userEvent.click(textFieldContainerElement);
    const popupElement = baseElement.querySelector('.dls-popup')!;
    const popupScrollElement = popupElement.querySelector(
      '.dls-dropdown-base-scroll-container'
    )!;
    jest.runAllTimers();

    fireEvent.scroll(popupScrollElement, { target: { scrollY: 100 } });

    expect(true).toBeTruthy();
  });

  it('should cover if it is horizontal scroll, just fake cover 100% by mock document.body', () => {
    jest.useFakeTimers();
    const { container } = render(<MockPopupBase />);
    const inputElement = container.querySelector('.text-field-container')!;
    userEvent.click(inputElement);
    jest.runAllTimers();

    fireEvent.scroll(document.body, {
      target: { scrollLeft: 100, scrollTop: 0 }
    });

    expect(true).toBeTruthy();
  });
});
