import { useEffect } from 'react';

export interface AutoFillArgs {
  autoFill?: boolean;
  focused?: boolean;
  disabled?: boolean;
  readOnly?: boolean;
  value: string;
  valueProp?: React.ReactText;
  inputElement: HTMLInputElement;
}

export interface AutoFillHook {
  (args: AutoFillArgs): void;
}

const useAutoFill: AutoFillHook = ({
  autoFill,
  focused,
  disabled,
  readOnly,
  value,
  valueProp,
  inputElement
}) => {
  useEffect(() => {
    if (!inputElement) return;

    const [number, float] = value.split('.');
    if (
      autoFill &&
      (!focused || disabled || readOnly) &&
      parseFloat(float) === 0
    ) {
      inputElement.value = number;
    }
  }, [autoFill, focused, disabled, readOnly, value, valueProp, inputElement]);
};

export default useAutoFill;
