import '@testing-library/jest-dom';

import {
  isNumber,
  isNumberFloat,
  isValueView,
  isValidPasteInput,
  getFormat,
  convertValueToDataView,
  convertDataViewFloat,
  calcPositionCaret,
  getNewPosition,
  getDeference,
  getLengthDataView,
  isValidStep
} from './service';
import { CURRENCY, NUMBER, PERCENT, INTEGER, DEFAULT_FORMAT } from './constant';

describe('Service - isNumberFloat', () => {
  it('check input interger', () => {
    expect(isNumberFloat('123.')).toEqual(true);
  });

  it('check input decimal', () => {
    expect(isNumberFloat('.1')).toEqual(true);
  });

  it('check input string', () => {
    expect(isNumberFloat('abc')).toEqual(false);
  });

  it('check input undefined', () => {
    expect(isNumberFloat()).toEqual(false);
  });
});

describe('Service - isValueView', () => {
  it('check input interger', () => {
    expect(isValueView('777,000.')).toEqual(true);
  });

  it('check input decimal', () => {
    expect(isValueView('.1')).toEqual(true);
  });

  it('check input undefined', () => {
    expect(isValueView()).toEqual(false);
  });
});

describe('Service - isValidPasteInput', () => {
  it('check input data view', () => {
    expect(isValidPasteInput('7,777.00')).toEqual(true);
  });

  it('check input decimal', () => {
    expect(isValidPasteInput('7,777.')).toEqual(true);
  });

  it('check input', () => {
    expect(isValidPasteInput('1234')).toEqual(true);
  });

  it('check input string', () => {
    expect(isValidPasteInput('7,7')).toEqual(false);
  });

  it('check input undefined', () => {
    expect(isValidPasteInput()).toEqual(false);
  });
});

describe('Service - isNumber', () => {
  it('check input interger', () => {
    expect(isNumber('123')).toEqual(true);
  });

  it('check input decimal', () => {
    expect(isNumber('1.1')).toEqual(false);
  });

  it('check input string', () => {
    expect(isNumber('abc')).toEqual(false);
  });
});

describe('Service - getFormat', () => {
  const objectContaining = (type: string, numberDecimal: number) =>
    expect.objectContaining({
      type,
      numberDecimal
    });

  it('default format', () => {
    expect(getFormat()).toEqual(objectContaining(NUMBER, 2));
    expect(getFormat('')).toEqual(objectContaining(NUMBER, 0));
    expect(getFormat('m2')).toEqual(objectContaining(NUMBER, 2));
  });

  it('format currency', () => {
    expect(getFormat('c')).toEqual(objectContaining(CURRENCY, 0));
    expect(getFormat('c2')).toEqual(objectContaining(CURRENCY, 2));
  });

  it('format percent', () => {
    expect(getFormat('p')).toEqual(objectContaining(PERCENT, 0));
    expect(getFormat('p2')).toEqual(objectContaining(PERCENT, 2));
  });

  it('format integer', () => {
    expect(getFormat('i')).toEqual(objectContaining(INTEGER, 0));
    expect(getFormat('i2')).toEqual(objectContaining(INTEGER, 2));
  });

  it('format number', () => {
    expect(getFormat('n')).toEqual(objectContaining(NUMBER, 0));
    expect(getFormat('n2')).toEqual(objectContaining(NUMBER, 2));
  });
});

describe('Service - convertValueToDataView', () => {
  it('invalid format', () => {
    expect(convertValueToDataView(undefined, DEFAULT_FORMAT)).toEqual('');
    expect(convertValueToDataView('abc', DEFAULT_FORMAT)).toEqual('');
    expect(convertValueToDataView('1.23', 'i2')).toEqual('');
  });

  it('format currency', () => {
    expect(convertValueToDataView(1100, 'c')).toEqual('1,100');
    expect(convertValueToDataView(1100, 'c2')).toEqual('1,100.00');
    expect(convertValueToDataView(1100.5, 'c2')).toEqual('1,100.50');
    expect(convertValueToDataView(-1100, 'c2')).toEqual('-1,100.00');
  });

  it('format percent', () => {
    expect(convertValueToDataView(1100, 'p')).toEqual('1,100');
    expect(convertValueToDataView(1100, 'p2')).toEqual('1,100.00');
    expect(convertValueToDataView(1100.5, 'p2')).toEqual('1,100.50');
    expect(convertValueToDataView(-1100, 'p2')).toEqual('-1,100.00');
  });

  it('format integer', () => {
    expect(convertValueToDataView(1100, 'i')).toEqual('1,100');
    expect(convertValueToDataView(1100, 'i2')).toEqual('1,100.00');
    expect(convertValueToDataView(-1100, 'i2')).toEqual('-1,100.00');
  });

  it('format integer with autofill', () => {
    expect(convertValueToDataView(1100.0, 'i2', true)).toEqual('1,100');
  });
});

describe('Service - convertDataViewFloat', () => {
  it('invalid format', () => {
    expect(convertDataViewFloat('abc', DEFAULT_FORMAT)).toEqual('');
    expect(convertDataViewFloat('1.23', 'i2')).toEqual('');
  });

  it('format currency', () => {
    expect(convertDataViewFloat(1100, 'c')).toEqual('1100');
    expect(convertDataViewFloat(1100, 'c2')).toEqual('1100.00');
    expect(convertDataViewFloat(1100.5, 'c2')).toEqual('1100.50');
    expect(convertDataViewFloat(-1100, 'c2')).toEqual('-1100.00');
  });

  it('format percent', () => {
    expect(convertDataViewFloat(1100, 'p2')).toEqual('1100.00');
    expect(convertDataViewFloat(1100.5, 'p2')).toEqual('1100.50');
    expect(convertDataViewFloat(-1100, 'p2')).toEqual('-1100.00');
    expect(convertDataViewFloat(1100, 'p')).toEqual('1100');
  });

  it('format integer', () => {
    expect(convertDataViewFloat(1100, 'i')).toEqual('1100');
    expect(convertDataViewFloat(1100, 'i2')).toEqual('1100.00');
    expect(convertDataViewFloat(-1100, 'i2')).toEqual('-1100.00');
  });
});

describe('Service - calcPositionCaret', () => {
  it('integer', () => {
    expect(calcPositionCaret('1', '2', 'i')).toEqual(0);
    expect(calcPositionCaret('1', '', 'i')).toEqual(0);
  });

  it('length of current value greater than previus value', () => {
    expect(calcPositionCaret('1000', '1,000', 'n2')).toEqual(1);
    expect(calcPositionCaret('1,000', '10,000', 'n2')).toEqual(0);
  });

  it('length of current value less than previus value', () => {
    expect(calcPositionCaret('1000', '100', 'n2')).toEqual(0);
    expect(calcPositionCaret('1,000', '100', 'n2')).toEqual(-1);
  });

  it('length of current value equal previus value', () => {
    expect(calcPositionCaret('10,00', '1,000', 'n2')).toEqual(0);
  });
});

describe('Service - getNewPosition', () => {
  it('check new position', () => {
    expect(getNewPosition('10', '0,9', 'n2')).toEqual(0);

    expect(getNewPosition('1000', '1,000', 'n2')).toEqual(1);
    expect(getNewPosition('1,000', '1,2', 'n2', -2)).toEqual(0);
    expect(getNewPosition('1,000', '-9.2', 'n2')).toEqual(0);
  });
});

describe('Service - getDeference', () => {
  const fakeResult = (n: number) => new Array(n + 1).join(',').split('');

  it('check deference', () => {
    expect(getDeference('1000', '1,000')).toEqual(fakeResult(1));
    expect(getDeference('1,0000', '1,000')).toEqual(fakeResult(0));
    expect(getDeference('10,000,000', '10000000')).toEqual(fakeResult(2));
  });
});

describe('Service - getLengthDataView', () => {
  it('check length data view', () => {
    expect(getLengthDataView('1000')).toEqual(4);
    expect(getLengthDataView('123,456.00')).toEqual(8);
    expect(getLengthDataView('-123,456.00')).toEqual(9);
  });
});

describe('Service - isValidStep', () => {
  it('check step is valid or not', () => {
    expect(isValidStep(null as any)).toEqual(false);
    expect(isValidStep(undefined, 'c2')).toEqual(true);
    expect(isValidStep('1.555', 'c2')).toEqual(false);
  });
});
