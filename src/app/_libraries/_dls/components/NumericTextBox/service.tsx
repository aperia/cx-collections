import toString from 'lodash.tostring';
import { ReactText } from 'react';
import { isProduction } from '../../utils';
import { CURRENCY, DEFAULT_FORMAT, NUMBER, PERCENT, INTEGER } from './constant';
import { NumericProps } from './types';

const numericRegex = new RegExp(/^\d*$/);
const regexIntegerView = /\d(?=(\d{3})+(?!\d))/g;

export const isNumber = (value: string) => {
  return numericRegex.test(value);
};

export const isNumberFloat = (value = '') => {
  // 123. || 123.00
  const isFloat1 = /^\d+(\.)?(\d+)?$/.test(value);
  // .123 || 0.123
  const isFloat2 = /^(\d+)?(\.)?(\d+)$/.test(value);
  return isFloat1 || isFloat2;
};

export const isValueView = (value = '') => {
  if (!value) return false;
  return value.replace(/,/g, '').replace(regexIntegerView, '$&,') === value;
};
/**
 *
 * @param value
 * @returns
 * 7,7,7 =>  false
 * 7,777 =>  true
 * 7777 => true
 */
export const isValidPasteInput = (value = '') => {
  if (!value) return false;
  return value.includes(',') ? isValueView(value) : isNumber(value);
};

/**
 * Get information of input format
 *
 * @param format accept format c0 | c1 | c2 | c3 ... cn, n0 | n1 ..., p1 | p2
 *
 * @returns an object {type: string, numberDecimal: number} with type: currency | decimal | number
 */
export const getFormat = (format = DEFAULT_FORMAT) => {
  const [type, numberDecimal] = format.split('');

  const mappingType: Record<string, string> = {
    c: CURRENCY,
    p: PERCENT,
    n: NUMBER,
    i: INTEGER
  };

  return {
    type: mappingType[type] || NUMBER,
    numberDecimal: Number(numberDecimal || 0)
  };
};

export const isValidStep = (
  step: NumericProps['step'] = '1',
  format: NumericProps['format'] = DEFAULT_FORMAT
) => {
  if (!step || !format) return false;

  const { numberDecimal } = getFormat(format);
  const [, stepDecimal = ''] = step.split('.');

  if (stepDecimal.length > numberDecimal) {
    !isProduction() &&
      console.warn(
        `Format: \`${format}\` and Step: \`${step}\` can't be combined, please update and try again.`
      );
    return false;
  }

  return true;
};

export const isValidNumber = (value: NumericProps['value'], format: string) => {
  const valueString = value?.toString();

  if (!valueString) return false;

  if (!isFinite(Number(valueString.replace(/,/g, '')))) return false;

  const { type } = getFormat(format);

  if (type === INTEGER && valueString.includes('.')) return false;

  return true;
};

// value = 123456.789 format = n2 => 123,456.78
// value = 1,234,56.789 format = n2 => 123,456.78
export const convertValueToDataView = (
  value: NumericProps['value'],
  format: string,
  autoFill = false
) => {
  if (!isValidNumber(value, format)) {
    return '';
  }
  const strValue = toString(value).replace(/,/g, '');
  const { numberDecimal } = getFormat(format);
  const isFloat = numberDecimal !== 0;

  const [integer, decimal = ''] = strValue.split('.');
  const hasSign = integer[0] === '-';
  let absInteger = hasSign ? integer.substring(1) : integer;
  absInteger = toString(Number(absInteger));

  const formatIntegerData = absInteger
    .replace(/,/g, '')
    .replace(regexIntegerView, '$&,');

  let formatDecimalData = '';

  if (isFloat) {
    formatDecimalData += '.';
    for (let i = 0; i < numberDecimal; i++) {
      formatDecimalData += decimal[i] || '0';
    }
    if (autoFill && parseFloat(formatDecimalData) === 0) {
      formatDecimalData = '';
    }
  }

  return `${hasSign ? '-' : ''}${formatIntegerData}${formatDecimalData}`;
};

/**
 *
 * @param value
 * @param format
 * value = 1,234,56.789 format = n2 => 123456.78
 */
export const convertDataViewFloat = (value: ReactText, format: string) => {
  return convertValueToDataView(value, format).replace(/,/g, '');
};

/**
 * Calculate position of caret when user input.
 *
 * @param previousValue
 * @param currentValue
 * @param format accept format c1,c2,c3...n1,n2,n3
 */
export const calcPositionCaret = (
  previousValue: string,
  currentValue: string,
  format: string
) => {
  if (!currentValue || previousValue === '-') return 0;

  const { type } = getFormat(format);
  if (type === INTEGER) return 0;

  // Calc length comma
  const lengthCommaBefore = previousValue.split(',').length - 1;
  const lengthCommaAfter = currentValue.split(',').length - 1;

  const compareCommaLength = lengthCommaAfter - lengthCommaBefore;
  const compareFormatLength = currentValue.length - previousValue.length;

  if (compareFormatLength > 0) {
    return compareCommaLength > 0 ? 1 : 0;
  }

  if (compareFormatLength < 0) {
    return compareCommaLength < 0 ? -1 : 0;
  }

  return 0;
};

const removeSemicolon = (str: string) => str?.replace(/,/g, '');
/**
 * Calculate position of caret when user input.
 *
 * @param prevValue
 * @param currentValue
 * @param format accept format c1,c2,c3...n1,n2,n3
 */
export const getNewPosition = (
  previousValue: React.ReactText,
  currentValue: React.ReactText,
  format: string,
  position = 0
) => {
  let result = position;
  const prev = toString(previousValue);
  const current = toString(currentValue);
  let buffer = calcPositionCaret(
    prev,
    convertValueToDataView(currentValue, format),
    format
  );

  const currentFloat = removeSemicolon(current);
  const [currentInteger] = currentFloat.split('.');

  if (
    currentInteger.length === 2 &&
    Number(currentInteger) < 10 &&
    Number(currentInteger) >= 0 &&
    Number(currentInteger) !== -0
  ) {
    buffer = -1;
  }
  result = position + buffer;

  return result < 0 ? 0 : result;
};

const stringToArray = (str: string) => str?.split('');

export const getDeference = (str1: string, str2: string) => {
  const arr1 = str1.length > str2.length ? str1 : str2;
  const arr2 = str1.length > str2.length ? str2 : str1;

  return stringToArray(arr1).filter(x => !stringToArray(arr2).includes(x));
};

// 123,456.00 => length = 8 (12345600)
export const getLengthDataView = (str: string) => {
  return str.replace(/[,.]/g, '').length;
};
