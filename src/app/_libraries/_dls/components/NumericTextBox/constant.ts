export const DEFAULT_CURRENCY = '$';
export const DEFAULT_PERCENT = '%';
export const DEFAULT_MAX_LENGTH = 16;
export const DEFAULT_FORMAT = 'n2';

export const CURRENCY = 'currency';
export const PERCENT = 'decimal';
export const NUMBER = 'number';
export const INTEGER = 'integer';

export const MAX_LENGTH_ERROR = "Ops! Value is greater than max length."
export const WRONG_VALUE_ERROR = 'Ops! Value is a wrong number!'