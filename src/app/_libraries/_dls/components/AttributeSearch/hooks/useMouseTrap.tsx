import { useMemo } from 'react';

import MouseTrap, { MousetrapInstance } from 'mousetrap';

export interface MouseTrapHook {
  (element?: HTMLElement): MousetrapInstance;
}

const useMouseTrap: MouseTrapHook = (element) => {
  const mousetrap = useMemo(() => {
    const instance = new MouseTrap(element);
    return instance;
  }, [element]);

  return mousetrap;
};

export default useMouseTrap;
