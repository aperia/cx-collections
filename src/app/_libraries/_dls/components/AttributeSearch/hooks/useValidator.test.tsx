import React from 'react';
import { render, screen } from '@testing-library/react';

import useValidator from './useValidator';

import { AttributeSearchField } from '..';

const Comp = ({
  fields,
  validatorProp
}: {
  fields: AttributeSearchField[];
  validatorProp:
    | ((value: Record<string, any>) => Record<string, string> | undefined)
    | undefined;
}) => {
  const result = useValidator(fields, validatorProp);

  return <div data-testid="useValidator">{JSON.stringify(result())}</div>;
};

describe('useValidator', () => {
  describe('not error', () => {
    it('with validatorProp', () => {
      const validatorProp = jest.fn();
      const fields: AttributeSearchField[] = [
        {
          key: 'key_0',
          name: 'name_0',
          value: 'value_0',
          component: () => <div />,
          validator: { type: 'number', errorMessage: 'is not number' }
        }
      ];

      render(<Comp fields={fields} validatorProp={validatorProp} />);

      const content = screen.getByTestId('useValidator').textContent!;

      expect(content).toEqual('');
      expect(validatorProp).toBeCalledWith({ [fields[0].key]: fields[0] });
    });

    it('without validatorProp', () => {
      const fields: AttributeSearchField[] = [
        {
          key: 'key_0',
          name: 'name_0',
          value: 'value_0',
          component: () => <div />,
          validator: { type: 'number', errorMessage: 'is not number' }
        }
      ];

      render(<Comp fields={fields} validatorProp={undefined} />);

      const content = screen.getByTestId('useValidator').textContent!;

      expect(content).toEqual('');
    });
  });

  it('error', () => {
    const fields: AttributeSearchField[] = [
      {
        key: 'key_0',
        name: 'name_0',
        value: 'value_0',
        component: () => <div />,
        validator: { type: 'string', errorMessage: 'is not string' }
      },
      {
        key: 'key_1',
        name: 'name_1',
        value: 'value_1',
        component: () => <div />,
        validator: [
          {},
          { type: 'number', errorMessage: 'is not number' },
          { type: 'string', errorMessage: 'is not string' }
        ]
      },
      {
        key: 'key_2',
        name: 'name_2',
        value: 'value_2',
        component: () => <div />,
        validator: {}
      },
      {
        key: 'key_2',
        name: 'name_2',
        value: 'value_2',
        component: () => <div />
      }
    ];

    render(<Comp fields={fields} validatorProp={undefined} />);

    const content = screen.getByTestId('useValidator').textContent!;

    expect(JSON.parse(content)).toEqual({
      [fields[0].key]: fields[0].validator?.errorMessage,
      [fields[1].key]: fields[1].validator[2].errorMessage
    });
  });
});
