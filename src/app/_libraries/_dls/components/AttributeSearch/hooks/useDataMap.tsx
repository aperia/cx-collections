import { useMemo } from 'react';

import { AttributeSearchData } from '..';

const useDataMap = (data: AttributeSearchData[]) => {
  const dataMap = useMemo(() => {
    if (!Array.isArray(data)) return;

    const dataMap = data.reduce<Record<string, AttributeSearchData>>(
      (prev, curr) => {
        return {
          ...prev,
          [curr.key]: curr
        };
      },
      {}
    );

    return dataMap;
  }, [data]);

  return dataMap;
};

export default useDataMap;
