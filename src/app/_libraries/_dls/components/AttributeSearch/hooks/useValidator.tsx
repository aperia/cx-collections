import { useCallback } from 'react';

import { AttributeSearchField, AttributeSearchValidator } from '..';
import { defaultValidators } from '../utils';

export interface UseValidator {
  (
    fields: AttributeSearchField[],
    validatorProp:
      | ((value: Record<string, any>) => Record<string, string> | undefined)
      | undefined
  ): () => Record<string, any> | undefined;
}

const useValidator: UseValidator = (fields, validatorProp) => {
  const collectErrorValidator = (
    value: any,
    defaultValidator: AttributeSearchValidator
  ) => {
    if (!defaultValidator || !defaultValidator.type) return;

    const defaultValidate = defaultValidators[defaultValidator.type];
    const isError = defaultValidate(value);

    if (isError) return defaultValidator.errorMessage;
  };

  const validator = useCallback(() => {
    let error: Record<string, any> | undefined;

    // convert fields array to map
    const fieldMap = fields.reduce((prev, curr) => {
      let errorMessage;

      if (Array.isArray(curr.validator)) {
        const validators = curr.validator;
        for (let index = 0; index < validators.length; index++) {
          const validator = validators[index];
          const { value } = curr;
          errorMessage = collectErrorValidator(value, validator);
          if (errorMessage) break;
        }
      } else if (curr.validator && curr.validator.type) {
        const { value, validator } = curr;
        errorMessage = collectErrorValidator(value, validator);
      }

      if (errorMessage) error = { ...error, [curr.key]: errorMessage };

      prev = {
        ...prev,
        [curr.key]: curr
      };

      return prev;
    }, {});

    // run global validator if exists
    if (typeof validatorProp === 'function') {
      error = {
        ...error,
        ...validatorProp(fieldMap)
      };
    }

    error = Object.keys(error || {}).length ? error : undefined;
    return error;
  }, [fields, validatorProp]);

  return validator;
};

export default useValidator;
