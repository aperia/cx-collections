import {
  AttributeSearchData,
  AttributeSearchField,
  AttributeSearchEvent,
  AttributeSearchDetailData
} from '..';

export interface AttributeSearchConstraintField {
  field: string;
  requiredField: AttributeSearchData;
  errorMessage: string;
}

const autoAddConstraintFieldsOnSearch = (
  cosntraintFields: AttributeSearchConstraintField[],
  fields: AttributeSearchField[],
  error: Record<string, string>,
  fieldsProp: Pick<AttributeSearchField, 'value' | 'key'>[],
  onChange: (event: AttributeSearchEvent) => void,
  setFields: React.Dispatch<React.SetStateAction<AttributeSearchField[]>>,
  setError: React.Dispatch<
    React.SetStateAction<Record<string, string> | undefined>
  >
) => {
  if (!Array.isArray(cosntraintFields)) return error;

  cosntraintFields.forEach((dependency) => {
    const fieldsNotContainsRequiredField =
      fields.find((field) => field.key === dependency.field) &&
      !fields.find((field) => field.key === dependency.requiredField.key);

    if (!fieldsNotContainsRequiredField || !dependency.requiredField) return;

    const requiredField = dependency.requiredField;

    if (fieldsProp && typeof onChange === 'function') {
      onChange({
        value: [...fields, requiredField],
        valueMap: { [dependency.field]: requiredField }
      });
      error = {
        ...error,
        [dependency.requiredField.key]: dependency.errorMessage
      };
    }

    setFields([...fields, requiredField]);
  });

  setError(error);

  return error;
};

const autoAddConstraintFieldsOnFocusInputFilter = (
  cosntraintFields: AttributeSearchConstraintField[],
  fields: AttributeSearchField[],
  fieldsProp: Pick<AttributeSearchField, 'value' | 'key'>[],
  data: AttributeSearchDetailData[],
  onChange: (event: AttributeSearchEvent) => void,
  setFields: React.Dispatch<React.SetStateAction<AttributeSearchField[]>>
) => {
  if (!Array.isArray(cosntraintFields)) return;

  cosntraintFields.forEach((dependency) => {
    const fieldsNotContainsRequiredField =
      fields.find((field) => field.key === dependency.field) &&
      !fields.find((field) => field.key === dependency.requiredField.key);
    if (!fieldsNotContainsRequiredField) return;

    const requiredField: AttributeSearchData | undefined = dependency
      .requiredField.component
      ? dependency.requiredField
      : data.find((field) => field.key === dependency.requiredField.key);
    if (!requiredField) return;

    if (fieldsProp && typeof onChange === 'function') {
      onChange({
        value: [...fields, requiredField],
        valueMap: { [dependency.field]: requiredField }
      });
    }

    setFields([...fields, requiredField]);
  });
};

export {
  autoAddConstraintFieldsOnSearch,
  autoAddConstraintFieldsOnFocusInputFilter
};
