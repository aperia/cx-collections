import React from 'react';
import { standAloneField } from './standAloneField';

import { NoCombineField, AttributeSearchField } from '..';

describe('standAloneField', () => {
  it('with empty params', () => {
    const fields: AttributeSearchField[] = [];
    const noCombineFields: NoCombineField[] = [];
    const currentDataFiltered: any = [];
    const setNoFilterResultText: (value: React.SetStateAction<string>) => void =
      () => {};
    const noFilterResultTextProp = '';

    const result = standAloneField(
      fields,
      noCombineFields,
      currentDataFiltered,
      setNoFilterResultText,
      noFilterResultTextProp
    );

    expect(result).toEqual([]);
  });

  it('without noCombineFields', () => {
    const fields: AttributeSearchField[] = [];
    const noCombineFields = null as unknown as NoCombineField[];
    const currentDataFiltered: any = [];
    const setNoFilterResultText: (value: React.SetStateAction<string>) => void =
      () => {};
    const noFilterResultTextProp = '';

    const result = standAloneField(
      fields,
      noCombineFields,
      currentDataFiltered,
      setNoFilterResultText,
      noFilterResultTextProp
    );

    expect(result).toEqual(currentDataFiltered);
  });

  it('when has no combined field', () => {
    const fields: AttributeSearchField[] = [
      { key: 'key', name: 'name', value: 'value', component: () => <div /> },
      { key: 'key_1', name: 'name', value: 'value', component: () => <div /> }
    ];
    const noCombineFields: NoCombineField[] = [
      { field: 'key_1', filterMessage: '' },
      { field: 'key_2', filterMessage: '' }
    ];
    const currentDataFiltered: any = [
      { key: 'key', name: 'name', value: 'value', component: () => <div /> },
      { key: 'key_1', name: 'name', value: 'value', component: () => <div /> }
    ];
    const setNoFilterResultText: (value: React.SetStateAction<string>) => void =
      () => {};
    const noFilterResultTextProp = '';

    const result = standAloneField(
      fields,
      noCombineFields,
      currentDataFiltered,
      setNoFilterResultText,
      noFilterResultTextProp
    );

    expect(result).toEqual([]);
  });

  it('when has no combined field', () => {
    const fields: AttributeSearchField[] = [
      { key: 'key', name: 'name', value: 'value', component: () => <div /> },
      { key: 'key_1', name: 'name', value: 'value', component: () => <div /> }
    ];
    const noCombineFields: NoCombineField[] = [
      { field: 'no combile field', filterMessage: '' }
    ];
    const currentDataFiltered: any = [
      { key: 'key', name: 'name', value: 'value', component: () => <div /> },
      { key: 'key_1', name: 'name', value: 'value', component: () => <div /> }
    ];
    const setNoFilterResultText: (value: React.SetStateAction<string>) => void =
      () => {};
    const noFilterResultTextProp = '';

    const result = standAloneField(
      fields,
      noCombineFields,
      currentDataFiltered,
      setNoFilterResultText,
      noFilterResultTextProp
    );

    expect(result).not.toEqual([]);
  });
});
