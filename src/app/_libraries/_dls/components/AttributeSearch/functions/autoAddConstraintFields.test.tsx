import React from 'react';

import {
  autoAddConstraintFieldsOnSearch,
  autoAddConstraintFieldsOnFocusInputFilter,
  AttributeSearchConstraintField
} from './autoAddConstraintFields';

import {
  AttributeSearchField,
  AttributeSearchEvent,
  AttributeSearchDetailData
} from '..';

describe('autoAddConstraintFieldsOnSearch', () => {
  it('when cosntraintFields is null', () => {
    const cosntraintFields =
      null as unknown as AttributeSearchConstraintField[];
    const fields: AttributeSearchField[] = [];
    const error: Record<string, string> = {};
    const fieldsProp: Pick<AttributeSearchField, 'value' | 'key'>[] = [];
    const onChange: (event: AttributeSearchEvent) => void = () => {};
    const setFields: React.Dispatch<
      React.SetStateAction<AttributeSearchField[]>
    > = () => {};
    const setError: React.Dispatch<
      React.SetStateAction<Record<string, string> | undefined>
    > = () => {};

    const result = autoAddConstraintFieldsOnSearch(
      cosntraintFields,
      fields,
      error,
      fieldsProp,
      onChange,
      setFields,
      setError
    );

    expect(result).toEqual(error);
  });

  it('when error', () => {
    const cosntraintFields: AttributeSearchConstraintField[] = [
      {
        field: 'field_1',
        requiredField: {
          key: 'key_1',
          name: 'name_1',
          component: () => <div />
        },
        errorMessage: 'required'
      }
    ];
    const fields: AttributeSearchField[] = [];
    const error: Record<string, string> = {};
    const fieldsProp: Pick<AttributeSearchField, 'value' | 'key'>[] = [];
    const onChange: (event: AttributeSearchEvent) => void = () => {};
    const setFields: React.Dispatch<
      React.SetStateAction<AttributeSearchField[]>
    > = () => {};
    const setError: React.Dispatch<
      React.SetStateAction<Record<string, string> | undefined>
    > = () => {};

    const result = autoAddConstraintFieldsOnSearch(
      cosntraintFields,
      fields,
      error,
      fieldsProp,
      onChange,
      setFields,
      setError
    );

    expect(result).toEqual({});
  });

  describe('when error', () => {
    it('with onChange', () => {
      const cosntraintFields: AttributeSearchConstraintField[] = [
        {
          field: 'field_1',
          requiredField: {
            key: 'key_1',
            name: 'name_1',
            component: () => <div />
          },
          errorMessage: 'required'
        }
      ];
      const fields: AttributeSearchField[] = [
        { key: 'field_1', name: 'name_1', component: () => <div /> }
      ];
      const error: Record<string, string> = {};
      const fieldsProp: Pick<AttributeSearchField, 'value' | 'key'>[] = [];
      const onChange: (event: AttributeSearchEvent) => void = () => {};
      const setFields: React.Dispatch<
        React.SetStateAction<AttributeSearchField[]>
      > = () => {};
      const setError: React.Dispatch<
        React.SetStateAction<Record<string, string> | undefined>
      > = () => {};

      const result = autoAddConstraintFieldsOnSearch(
        cosntraintFields,
        fields,
        error,
        fieldsProp,
        onChange,
        setFields,
        setError
      );

      expect(result).toEqual({ key_1: 'required' });
    });

    it('with onChange', () => {
      const cosntraintFields: AttributeSearchConstraintField[] = [
        {
          field: 'field_1',
          requiredField: {
            key: 'key_1',
            name: 'name_1',
            component: () => <div />
          },
          errorMessage: 'required'
        }
      ];
      const fields: AttributeSearchField[] = [
        { key: 'field_1', name: 'name_1', component: () => <div /> }
      ];
      const error: Record<string, string> = {};
      const fieldsProp: Pick<AttributeSearchField, 'value' | 'key'>[] = [];
      const onChange = undefined as unknown as (
        event: AttributeSearchEvent
      ) => void;
      const setFields: React.Dispatch<
        React.SetStateAction<AttributeSearchField[]>
      > = () => {};
      const setError: React.Dispatch<
        React.SetStateAction<Record<string, string> | undefined>
      > = () => {};

      const result = autoAddConstraintFieldsOnSearch(
        cosntraintFields,
        fields,
        error,
        fieldsProp,
        onChange,
        setFields,
        setError
      );

      expect(result).toEqual({});
    });
  });
});

describe('autoAddConstraintFieldsOnFocusInputFilter', () => {
  it('when cosntraintFields is null', () => {
    const cosntraintFields =
      null as unknown as AttributeSearchConstraintField[];
    const fields: AttributeSearchField[] = [];
    const fieldsProp: Pick<AttributeSearchField, 'value' | 'key'>[] = [];
    const data: AttributeSearchDetailData[] = [];
    const onChange: (event: AttributeSearchEvent) => void = () => {};
    const setFields: React.Dispatch<
      React.SetStateAction<AttributeSearchField[]>
    > = () => {};

    const result = autoAddConstraintFieldsOnFocusInputFilter(
      cosntraintFields,
      fields,
      fieldsProp,
      data,
      onChange,
      setFields
    );

    expect(result).toEqual(undefined);
  });

  it('when not cosntraint required Fields', () => {
    const cosntraintFields: AttributeSearchConstraintField[] = [
      {
        field: 'field_1',
        requiredField: {
          key: 'key_1',
          name: 'name_1',
          component: () => <div />
        },
        errorMessage: 'required'
      }
    ];
    const fields: AttributeSearchField[] = [
      { key: 'field_2', name: 'name_1', component: () => <div /> }
    ];
    const fieldsProp: Pick<AttributeSearchField, 'value' | 'key'>[] = [];
    const data: AttributeSearchDetailData[] = [];
    const onChange: (event: AttributeSearchEvent) => void = () => {};
    const setFields: React.Dispatch<
      React.SetStateAction<AttributeSearchField[]>
    > = () => {};

    const result = autoAddConstraintFieldsOnFocusInputFilter(
      cosntraintFields,
      fields,
      fieldsProp,
      data,
      onChange,
      setFields
    );

    expect(result).toEqual(undefined);
  });

  it('when cosntraint required Fields', () => {
    const cosntraintFields: AttributeSearchConstraintField[] = [
      {
        field: 'field_1',
        requiredField: {
          key: 'key_1',
          name: 'name_1',
          component: () => <div />
        },
        errorMessage: 'required'
      }
    ];
    const fields: AttributeSearchField[] = [
      { key: 'field_1', name: 'name_1', component: () => <div /> }
    ];
    const fieldsProp = undefined as unknown as Pick<
      AttributeSearchField,
      'value' | 'key'
    >[];
    const data: AttributeSearchDetailData[] = [];
    const onChange: (event: AttributeSearchEvent) => void = () => {};
    const setFields: React.Dispatch<
      React.SetStateAction<AttributeSearchField[]>
    > = () => {};

    const result = autoAddConstraintFieldsOnFocusInputFilter(
      cosntraintFields,
      fields,
      fieldsProp,
      data,
      onChange,
      setFields
    );

    expect(result).toEqual(undefined);
  });

  it('when not has component but has data', () => {
    const cosntraintFields: AttributeSearchConstraintField[] = [
      {
        field: 'field_1',
        requiredField: {
          key: 'key_1',
          name: 'name_1',
          component: undefined as unknown as React.ReactType<any>
        },
        errorMessage: 'required'
      }
    ];
    const fields: AttributeSearchField[] = [
      { key: 'field_1', name: 'name_1', component: () => <div /> }
    ];
    const fieldsProp: Pick<AttributeSearchField, 'value' | 'key'>[] = [];
    const data: AttributeSearchDetailData[] = [
      { key: 'key_1', name: 'name_1', component: () => <div /> }
    ];
    const onChange: (event: AttributeSearchEvent) => void = () => {};
    const setFields: React.Dispatch<
      React.SetStateAction<AttributeSearchField[]>
    > = () => {};

    const result = autoAddConstraintFieldsOnFocusInputFilter(
      cosntraintFields,
      fields,
      fieldsProp,
      data,
      onChange,
      setFields
    );

    expect(result).toEqual(undefined);
  });

  it('when not has component and data', () => {
    const cosntraintFields: AttributeSearchConstraintField[] = [
      {
        field: 'field_1',
        requiredField: {
          key: 'key_1',
          name: 'name_1',
          component: undefined as unknown as React.ReactType<any>
        },
        errorMessage: 'required'
      }
    ];
    const fields: AttributeSearchField[] = [
      { key: 'field_1', name: 'name_1', component: () => <div /> }
    ];
    const fieldsProp: Pick<AttributeSearchField, 'value' | 'key'>[] = [];
    const data: AttributeSearchDetailData[] = [];
    const onChange: (event: AttributeSearchEvent) => void = () => {};
    const setFields: React.Dispatch<
      React.SetStateAction<AttributeSearchField[]>
    > = () => {};

    const result = autoAddConstraintFieldsOnFocusInputFilter(
      cosntraintFields,
      fields,
      fieldsProp,
      data,
      onChange,
      setFields
    );

    expect(result).toEqual(undefined);
  });
});
