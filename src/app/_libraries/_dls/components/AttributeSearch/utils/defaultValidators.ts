const isNumber = (value: any) => /^\d+$/.test(value + '');

const isString = (value: any) => typeof value === 'string';

const isDateTime = (value: any) => {
  if (typeof value !== 'object' || typeof value.getTime !== 'function') {
    return false;
  }

  return !isNaN(value.getTime());
};

const isBoolean = (value: any) => typeof value === 'boolean';

const isRequired = (value: any) => !value;

const defaultValidators = {
  number: isNumber,
  string: isString,
  datetime: isDateTime,
  boolean: isBoolean,
  required: isRequired
};

export default defaultValidators;
