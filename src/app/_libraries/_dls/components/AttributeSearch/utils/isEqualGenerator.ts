export interface IsEqualGeneratorFunction {
  (by: string): (
    left: Record<string, any>,
    right: Record<string, any>
  ) => boolean;
}

const isEqualGenerator: IsEqualGeneratorFunction = (by) => (left, right) => {
  if (typeof left !== 'object' || typeof right !== 'object') return false;

  return left[by] === right[by];
};

export default isEqualGenerator;
