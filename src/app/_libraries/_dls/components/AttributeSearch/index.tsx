import React, {
  useMemo,
  useState,
  useRef,
  useEffect,
  useCallback,
  useImperativeHandle
} from 'react';

// components/types
import PopupBase from '../PopupBase';
import Tooltip, { TooltipProps } from '../Tooltip';
import { Header, Item, Footer } from './components';

// utils/hooks/types
import diff from 'lodash.differencewith';
import find from 'lodash.find';
import isEmpty from 'lodash.isempty';
import get from 'lodash.get';
import isNumber from 'lodash.isnumber';
import isUndefined from 'lodash.isundefined';
import classnames from 'classnames';
import {
  useHammerPan,
  useFuse,
  useMouseTrap,
  useDataMap,
  useValidator,
  useObserverPlaceholder
} from './hooks';
import { isEqualGenerator, fieldsMap, defaultModifiers } from './utils';
import { genAmtId, groupOrderFlatBy } from '../../utils';
// import { Placement } from '@popperjs/core';
import { ExtendedKeyboardEvent } from 'mousetrap';

// functions
import { standAloneField } from './functions/standAloneField';
import {
  autoAddConstraintFieldsOnSearch,
  autoAddConstraintFieldsOnFocusInputFilter
} from './functions/autoAddConstraintFields';
import { Placement } from '@popperjs/core';
import { useTranslation } from '../../hooks';

export interface AttributeSearchInputFilterRef extends HTMLInputElement {
  validate: () => void;
  closePopup: () => void;
  openPopup: () => void;
}

export interface AttributeSearchValidator {
  type?: 'number' | 'string' | 'boolean' | 'datetime' | 'required';
  errorMessage?: string;
}

export interface AttributeSearchData {
  key: string;
  name: string;
  component: React.ReactType;
  groupName?: string;
  description?: string;
  validator?: AttributeSearchValidator | AttributeSearchValidator[];
  disabledFields?: Record<string, string>;
}

export interface AttributeSearchDetailData extends AttributeSearchData {
  itemElement?: HTMLDivElement | null;
  disabled?: boolean;
}

export interface AttributeSearchField extends AttributeSearchDetailData {
  value?: any;
  inputElement?: HTMLInputElement | null;
}

export type AttributeSearchValue = Pick<AttributeSearchField, 'key' | 'value'>;

export interface AttributeSearchEvent {
  value: AttributeSearchValue[];
  valueMap: Record<string, AttributeSearchValue>;
}

export interface ChangeComponentEvent {
  value?: any;
  target?: {
    value?: any;
  };
}

export interface AttributeSearchDependencyField {
  field: string;
  requiredField: AttributeSearchData;
  errorMessage: string;
}

export interface NoCombineField {
  field: string;
  filterMessage: string;
}

export interface AttributeSearchRef {
  /** Set error, example: call setError() to reset error attribute search to initial state */
  setError: (optionalError?: Record<string, any>) => void;
  /** Set data, example: call setData() to reset data attribute search to initial state */
  setData: (optionalData?: AttributeSearchDetailData[]) => void;
  /** Set value, just working with uncontrolled mode, example: call setValue([]) to reset value attribute search to initial state */
  setValue: (optionalFields: AttributeSearchField[]) => void;
  /** Set show, example: call setShow(false) to hide attribute search popup  */
  setShow?: (show: boolean) => void;

  error?: Record<string, any>;
  show?: boolean;
  fieldFocused?: AttributeSearchField;
  dataFiltered?: AttributeSearchDetailData[];
  itemSelected?: AttributeSearchDetailData | null;
  fields?: AttributeSearchField[];
}

export interface DisabledFields {
  key: string;
  description: string;
}

export interface AttributeSearchProps extends DLSId {
  data?: AttributeSearchData[];
  value?: AttributeSearchValue[];
  onChange?: (event: AttributeSearchEvent) => void;
  onSearch?: (
    event: AttributeSearchEvent,
    error?: Record<string, string>
  ) => void;
  onClear?: () => void;
  onFocusInputFilter?: (event: React.FocusEvent<HTMLInputElement>) => void;
  validator?: (
    value: Record<string, any>
  ) => Record<string, string> | undefined;
  small?: boolean;
  tooltipProps?: TooltipProps;
  clearTooltipProps?: TooltipProps;
  noFilterResultText?: string;
  placeholder?: string;
  footer?: React.ReactNode;
  orderBy?: 'asc' | 'desc';
  constraintFields?: AttributeSearchDependencyField[];
  noCombineFields?: NoCombineField[];
  popupClassName?: string;
  disabledFields?: DisabledFields[];
  anchor?: HTMLElement;
}

const isEqualDataFiltered = isEqualGenerator('name');
const dataOutside: AttributeSearchData[] = [];

const AttributeSearch: React.RefForwardingComponent<
  AttributeSearchRef,
  AttributeSearchProps
> = (
  {
    data,
    value: fieldsProp,
    onSearch,
    onChange,
    onClear,
    onFocusInputFilter: onFocusInputFilterProp,
    validator: validatorProp,
    small = false,
    tooltipProps,
    clearTooltipProps,
    noFilterResultText:
      noFilterResultTextProp = 'No other parameters available.',
    placeholder = 'Type to add parameter',
    footer,
    orderBy,
    constraintFields,
    noCombineFields,
    popupClassName,
    disabledFields = [],
    anchor,

    dataTestId
  },
  ref
) => {
  if (isUndefined(data)) data = dataOutside;
  const attrSearchRef = useRef<HTMLDivElement | null>(null);
  const inputFilterRef = useRef<HTMLInputElement | null>(null);
  const searchBtnRef = useRef<HTMLButtonElement | null>(null);
  const clearBtnRef = useRef<HTMLButtonElement | null>(null);
  const itemPositionRef = useRef(-1);
  const keepFields = useRef<Record<string, AttributeSearchField | null>>({});

  const [placement, setPlacement] = useState<Placement>();
  const [dropdownElement, setDropdownElement] =
    useState<HTMLDivElement | null>(null);
  const [error, setError] = useState<Record<string, string>>();
  const [show, setShow] = useState<boolean | undefined>();
  const [needValidateFirstTime, setNeedValidateFirstTime] = useState(
    Array.isArray(fieldsProp) &&
      fieldsProp.length &&
      typeof onChange === 'function'
  );
  const [needFocusInputFilter, setNeedFocusInputFilter] =
    useState<boolean | undefined>();
  const [fieldFocused, setFieldFocused] =
    useState<AttributeSearchField | undefined>();
  const [dataFiltered, setDataFiltered] = useState<AttributeSearchDetailData[]>(
    () => groupOrderFlatBy(data!, 'groupName', 'name', orderBy)
  );
  const [itemSelected, setItemSelected] =
    useState<AttributeSearchDetailData | null>();
  const [fields, setFields] = useState<AttributeSearchField[]>([]);
  const [valueInputFilter, setValueInputFilter] =
    useState<string | undefined>('');
  const [triggerBlurAfterThat, setTriggerBlurAfterThat] =
    useState<boolean | undefined>();
  const [noFilterResultText, setNoFilterResultText] = useState(
    noFilterResultTextProp
  );
  // const [placement, setPlacement] = useState<Placement>();

  // hammerRef
  const hammerRef =
    useHammerPan() as React.MutableRefObject<HTMLDivElement | null>;
  // (re)initial fuse based on data
  const fuse = useFuse<AttributeSearchData>(data);
  // initial mousetrap
  const mousetrap = useMouseTrap(attrSearchRef.current as HTMLElement);

  // Map array data to object
  const dataMap = useDataMap(data);

  // validator fn
  const validator = useValidator(fields, validatorProp);

  const { t } = useTranslation();

  // public methods, state
  useImperativeHandle(
    ref,
    () => ({
      setError: (optionalError?: Record<string, any>) =>
        setError(optionalError),
      setData: (optionalData: AttributeSearchDetailData[] = []) =>
        setDataFiltered(optionalData),
      setValue: (optionalFields: AttributeSearchField[] = []) =>
        setFields(optionalFields),
      setShow: (show: boolean) => typeof show === 'boolean' && setShow(show),
      error,
      show,
      fieldFocused,
      dataFiltered,
      itemSelected,
      fields
    }),
    [error, show, fieldFocused, dataFiltered, itemSelected, fields]
  );

  // re(update) fields when fieldsProp change
  useEffect(() => {
    if (!Array.isArray(fieldsProp)) return;

    const newFields = fieldsProp.map(fieldProp => {
      const dataField = dataMap![fieldProp.key];

      return {
        ...dataField,
        ...fieldProp
      };
    });

    setFields(newFields);

    // handle refresh fields, not testing yet
    if (!fieldsProp.length) {
      keepFields.current = {};
      setError(undefined);
      setValueInputFilter('');
    }
  }, [fieldsProp, dataMap, onChange]);

  // update dataFiltered when valueInputFilter change
  //						 or	data change
  //						 or	fields change
  useEffect(() => {
    let currentDataFiltered;

    // just display first item in dataFiltered according field focusing...
    if (fieldFocused && dataMap) {
      const itemCurrentFocusing = dataMap[fieldFocused.key];
      itemCurrentFocusing && setDataFiltered([itemCurrentFocusing]);
      setItemSelected(null);
      itemPositionRef.current = -1;
      return;
    }

    if (!valueInputFilter) {
      currentDataFiltered = diff(data, fields, isEqualDataFiltered);
    } else {
      currentDataFiltered = fuse.search(valueInputFilter);
      currentDataFiltered = diff(
        currentDataFiltered,
        fields,
        isEqualDataFiltered
      );
    }

    // Filter out fields that cannot be combined with other parameter
    currentDataFiltered = standAloneField(
      fields,
      noCombineFields!,
      currentDataFiltered,
      setNoFilterResultText,
      noFilterResultTextProp
    );

    currentDataFiltered = groupOrderFlatBy(
      currentDataFiltered as AttributeSearchDetailData[],
      'groupName',
      'name',
      orderBy
    );

    setDataFiltered(currentDataFiltered);
    if (!Array.isArray(currentDataFiltered) || !currentDataFiltered[0]) {
      return setItemSelected(null);
    }

    setItemSelected(currentDataFiltered[0]);
    itemPositionRef.current = 0;
  }, [
    valueInputFilter,
    data,
    dataMap,
    fields,
    fieldFocused,
    fuse,
    noCombineFields,
    noFilterResultTextProp,
    orderBy
  ]);

  // update noFilterResultText when noFilterResultTextProps change
  useEffect(() => {
    setNoFilterResultText(noFilterResultTextProp);
  }, [noFilterResultTextProp]);

  // run validator once in controlled mode
  useEffect(() => {
    // just run validator once in controlled mode
    // in controlled mode, fields initial is empty array, fields will update based on fieldsProp
    // second render: fields have value
    // make sure just run when fields have value
    if (!needValidateFirstTime || !Array.isArray(fields) || !fields.length)
      return;

    const error = validator();
    setError(error);
    setNeedValidateFirstTime(false);
  }, [needValidateFirstTime, fields, validator]);

  // handle add field (put item to fields, remove item from dataFiltered)
  const handleAddField = useCallback(
    (item: AttributeSearchDetailData) => {
      if (fieldsProp && typeof onChange !== 'function') return;

      // clear valueInputFilter when field added
      setValueInputFilter('');

      // always add new field to the end of the fields array
      const newFields = [...fields, item];

      // controlled component mode
      if (fieldsProp && typeof onChange === 'function') {
        return onChange({
          value: newFields,
          valueMap: fieldsMap(newFields)
        });
      }

      // uncontrolled component mode
      setFields(newFields);

      // always call onChange prop after that
      typeof onChange === 'function' &&
        onChange({ value: newFields, valueMap: fieldsMap(newFields) });
    },
    [fields, fieldsProp, onChange]
  );

  // handle remove field (remove item from fields, update dataFiltered)
  const handleRemoveField = useCallback(
    (config: { byPosition?: number; byFieldKey?: string } = {}) => {
      if (fieldsProp && typeof onChange !== 'function') return;

      const { byPosition, byFieldKey } = config;

      // default delete latest field
      let newFields = fields.slice(0, fields.length - 1);
      let fieldDeleted: AttributeSearchField | undefined;

      // delete by position
      if (typeof byPosition === 'number' && byPosition !== -1) {
        newFields = [
          ...fields.slice(0, byPosition),
          ...fields.slice(byPosition + 1, fields.length)
        ];
        fieldDeleted = fields[byPosition];
      }

      // delete by field key
      if (byFieldKey && !fieldDeleted) {
        const index = fields.findIndex(field => field.key === byFieldKey);
        if (index !== -1) {
          newFields = [
            ...fields.slice(0, index),
            ...fields.slice(index + 1, fields.length)
          ];
          fieldDeleted = fields[index];
        }
      }

      // remove from keepFields if field will be removed
      if (fieldDeleted) {
        process.nextTick(() => setFieldFocused(undefined));
        keepFields.current[fieldDeleted.key] = null;
      }

      // remove error property of field need delete if exists
      // set focus input filter again if necessary
      // need call after setFields, please don't edit this code
      process.nextTick(() => {
        setError(error => {
          if (!fieldDeleted) return error;
          const isExists = error && error[fieldDeleted.key];
          if (!isExists) return error;

          const newError: Record<string, string> = { ...error };
          delete newError[fieldDeleted.key];

          return Object.keys(newError).length ? newError : undefined;
        });
      });

      // controlled component mode
      if (fieldsProp && typeof onChange === 'function') {
        return onChange({
          value: newFields,
          valueMap: fieldsMap(newFields)
        });
      }

      // uncontrolled component mode
      setFields(newFields);

      // WARNING: this code hasn't been tested yet
      typeof onChange === 'function' &&
        onChange({ value: newFields, valueMap: fieldsMap(newFields) });
    },
    [fields, fieldsProp, onChange]
  );

  // handle close popup when mousedown outside
  useEffect(() => {
    const handleClickOutSide = (event: MouseEvent) => {
      const attrSearchElement = attrSearchRef?.current as HTMLDivElement;
      const target = event.target as HTMLElement;
      const wildCard = target.closest('.modal-wildcard');

      const container =
        typeof target.closest === 'function' &&
        (target.closest('.dls-attribute-search-popup') ||
          target.closest('.dls-popup'));

      if (attrSearchElement?.contains(target) || container || wildCard) {
        return;
      }

      // reset fieldFocused
      setFieldFocused(undefined);

      // close popup
      setShow(false);
    };

    window.addEventListener('mousedown', handleClickOutSide);
    return () => window.removeEventListener('mousedown', handleClickOutSide);
  }, []);

  // handle click magnifying glass (search icon)
  const handleSearch = useCallback(() => {
    if (typeof onSearch !== 'function') return;

    let error = validator();

    // always reset error
    setError(error);

    // always reset fieldFocused
    setFieldFocused(undefined);

    // auto add missing dependency field
    error = autoAddConstraintFieldsOnSearch(
      constraintFields!,
      fields,
      error!,
      fieldsProp!,
      onChange!,
      setFields,
      setError
    );

    // always call onSearch
    onSearch(
      {
        value: fields,
        valueMap: fieldsMap(fields)
      },
      error
    );

    // if error is not exists => blur current, close popup
    if (!error || !Object.keys(error).length) {
      const inputElement = document.activeElement as HTMLInputElement;
      typeof inputElement.blur === 'function' && inputElement.blur();

      return setShow(false);
    }

    // else, focus input filter
    setNeedFocusInputFilter(true);
  }, [onSearch, validator, constraintFields, fields, fieldsProp, onChange]);

  // handle click icon clear search parameters
  const handleClear = useCallback(() => {
    // clear valueInputFilter
    setValueInputFilter('');

    // new fields empty array
    const newFields: AttributeSearchDetailData[] = [];

    // uncontrolled component mode
    if (!Array.isArray(fieldsProp)) {
      setFields(newFields);
    }

    // always focus input filter
    show && inputFilterRef.current!.focus();

    // call onClear prop
    typeof onClear === 'function' && onClear();

    // reset fieldFocused
    setFieldFocused(undefined);

    process.nextTick(() => {
      // reset error
      setError(undefined);
    });
  }, [fieldsProp, onClear, show]);

  // binding keyboard actions on popup
  useEffect(() => {
    if (!dropdownElement || !mousetrap) return;

    const isAllowMove = () => {
      let allowMove = document.activeElement === inputFilterRef.current;
      if (!allowMove) {
        allowMove = !!fields.find(
          field => field.inputElement === document.activeElement
        );
      }

      return allowMove;
    };

    const handleMouseTrapKeyDown = (event: ExtendedKeyboardEvent) => {
      // prevent caret of input filter move left/right
      event.preventDefault();

      // prevent if fieldFocused is exists
      if (fieldFocused) return;

      if (!isAllowMove()) return;

      let next = itemPositionRef.current + 1;
      if (next >= dataFiltered.length) return;

      // if item is disabled, ignore it
      let item = dataFiltered[next];
      while (item && item.disabled) {
        item = dataFiltered[++next];
      }
      if (!item) return;

      setItemSelected(item);
      itemPositionRef.current = next;
    };

    const handleMouseTrapKeyUp = (event: ExtendedKeyboardEvent) => {
      // prevent caret of input filter move left/right
      event.preventDefault();

      // prevent if fieldFocused is exists
      if (fieldFocused) return;

      if (!isAllowMove()) return;

      let next = itemPositionRef.current - 1;
      if (next < 0) return;

      // if item is disabled, ignore it
      let item = dataFiltered[next];
      while (item && item.disabled) {
        item = dataFiltered[--next];
      }
      if (!item) return;

      setItemSelected(item);
      itemPositionRef.current = next;
    };

    const handleMouseTrapKeyTab = (event: ExtendedKeyboardEvent) => {
      // keep logic add field and keep default behavior on tab keypress when item is not selected
      if (!itemSelected) return;

      event.preventDefault();
      handleAddField(itemSelected);
    };

    mousetrap.bind('up', handleMouseTrapKeyUp);
    mousetrap.bind('down', handleMouseTrapKeyDown);
    mousetrap.bind('tab', handleMouseTrapKeyTab);
    return () => {
      mousetrap.unbind('tab');
      mousetrap.unbind('up');
      mousetrap.unbind('down');
    };
  }, [
    dropdownElement,
    fields,
    dataFiltered,
    itemSelected,
    handleAddField,
    mousetrap,
    fieldFocused
  ]);

  // handle dropdown scrolling by observing item selected
  useEffect(() => {
    if (!dropdownElement) return;

    const mutationCallback: MutationCallback = records => {
      const recordSelected = records.find(record => {
        const element = record.target as HTMLElement;
        return element.classList.contains('selected');
      });
      if (!recordSelected) return;

      const elementSelected = recordSelected.target as HTMLElement;
      dropdownElement.scrollTop = elementSelected.offsetTop - 36 * 5;
    };
    const mutationObserver = new MutationObserver(mutationCallback);
    mutationObserver.observe(dropdownElement, {
      attributes: true,
      subtree: true
    });

    return () => mutationObserver.disconnect();
  }, [dropdownElement]);

  // binding keyboard other actions
  useEffect(() => {
    // allow clear field when inputfilter focused and empty valueInputFilter
    // or clear field when input field focused and focusPotion > 0
    const handleMouseTrapKeyBackSpace = () => {
      // const fieldFocusedIndex = fields.findIndex(
      //   (field) => field.inputElement === document.activeElement
      // );

      // if (fieldFocusedIndex > 0) {
      //   const nextFieldFocusedIndex = fieldFocusedIndex - 1;
      //   return handleRemoveField({ byPosition: nextFieldFocusedIndex });
      // }

      if (
        !valueInputFilter &&
        document.activeElement === inputFilterRef.current
      ) {
        handleRemoveField({ byPosition: fields.length - 1 });

        // issue: field error -> delete -> choose field error again -> enter -> not focus input filter
        // setNeedFocusInputFilter(true);
      }
    };

    // legacy code
    // const handleMouseTrapKeyDelete = () => {
    //   const fieldFocusedIndex = fields.findIndex(
    //     (field) => field.inputElement === document.activeElement
    //   );

    //   if (fieldFocusedIndex !== -1) {
    //     handleRemoveField({ byPosition: fieldFocusedIndex });

    //     const fieldNeedFocus = fields[fieldFocusedIndex + 1];
    //     if (!fieldNeedFocus) {
    //       return inputFilterRef.current!.focus();
    //     }

    //     fieldNeedFocus.inputElement!.focus();
    //   }
    // };

    mousetrap.bind('backspace', handleMouseTrapKeyBackSpace);
    mousetrap.bind('enter', handleSearch);
    // mousetrap.bind('del', handleMouseTrapKeyDelete);
    return () => {
      mousetrap.unbind('backspace');
      mousetrap.unbind('enter');
      // mousetrap.unbind('del');
    };
  }, [
    fields,
    valueInputFilter,
    handleRemoveField,
    onSearch,
    validator,
    mousetrap,
    handleSearch
  ]);

  // handle focus input filter again when delete a field by keyboard 'backspace'
  // or if we need focus input filter
  useEffect(() => {
    if (!needFocusInputFilter) return;

    // if (document.activeElement !== inputFilterRef.current) {
    inputFilterRef.current!.focus();
    setNeedFocusInputFilter(false);
    // }
  }, [needFocusInputFilter]);

  // handle call control onBlur again if needed
  useEffect(() => {
    if (!triggerBlurAfterThat) return;

    setTriggerBlurAfterThat(false);

    // always validate when onBlur
    process.nextTick(() => {
      const error = validator();
      setError(error);
    });
  }, [triggerBlurAfterThat, validator]);

  // handle horizontal scroll to view full parameter in search bar
  useEffect(() => {
    if (!fieldFocused || !keepFields.current[fieldFocused.key]) return;

    const inputElement = keepFields.current[fieldFocused.key]!.inputElement;

    const { left: leftInput, right: rightInput } =
      inputElement!.getBoundingClientRect();

    const { left: leftClearBtn } =
      clearBtnRef.current?.getBoundingClientRect() || {};
    const { left: leftSearchBtn } =
      searchBtnRef.current!.getBoundingClientRect();
    const { left: leftAttributeSearch } =
      attrSearchRef.current!.getBoundingClientRect();
    let scrollByX = 0;

    const margin = small ? 4 : 8;

    // auto-scroll left to right
    if (isNumber(leftAttributeSearch) && leftAttributeSearch > leftInput) {
      scrollByX = leftInput - leftAttributeSearch - margin;
    }

    // auto-scroll left to right
    if (!scrollByX && isNumber(leftClearBtn) && rightInput > leftClearBtn) {
      scrollByX = rightInput - leftClearBtn + margin;
    }

    // auto-scroll left to right
    if (!scrollByX && isNumber(leftSearchBtn) && rightInput > leftSearchBtn) {
      scrollByX = rightInput - leftSearchBtn + margin;
    }

    // if (isNumber(scrollByX)) {
    // temp code
    const timeoutId = setTimeout(() => {
      hammerRef.current?.scrollBy({
        behavior: 'smooth',
        left: scrollByX,
        top: 0
      });
    }, 250);

    return () => clearTimeout(timeoutId);
    // }
  }, [fieldFocused, fields, small, hammerRef]);

  // observe dropmenu to update placeholder
  useObserverPlaceholder(
    dropdownElement,
    '.ds-attr-search-item:not(.disabled)',
    inputFilterRef.current,
    placeholder
  );

  // handle onFocus input filter
  const handleOnFocusInputFilter = (
    event: React.FocusEvent<HTMLInputElement>
  ) => {
    typeof onFocusInputFilterProp === 'function' &&
      onFocusInputFilterProp(event);

    // reset field focused
    setFieldFocused(undefined);

    // show popup
    setShow(true);

    // auto add missing dependency field
    // why need to use process.nextTick?
    // need to review again
    process.nextTick(() => {
      autoAddConstraintFieldsOnFocusInputFilter(
        constraintFields!,
        fields,
        fieldsProp!,
        data!,
        onChange!,
        setFields
      );
    });

    hammerRef.current!.scroll({ left: inputFilterRef.current!.offsetLeft });
  };

  // handle search item (valueInputFilter change)
  const handleOnChangeInputFilter = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setValueInputFilter(event.target.value);
  };

  // handle onChange's Component (update value for field)
  const handleChangeComponent = useCallback(
    (event: ChangeComponentEvent, item: AttributeSearchField) => {
      if (fieldsProp && typeof onChange !== 'function') return;

      let value;
      if (event.target && event.target.value) {
        value = event.target.value;
      }
      if (event.value) value = event.value;

      const indexNeedUpdate = fields.findIndex(field => field.key === item.key);
      // if (indexNeedUpdate === -1) return;

      const newFields = [
        ...fields.slice(0, indexNeedUpdate),
        { ...item, value },
        ...fields.slice(indexNeedUpdate + 1)
      ];

      // controlled component mode
      if (fieldsProp && typeof onChange === 'function') {
        return onChange({
          value: newFields,
          valueMap: fieldsMap(newFields)
        });
      }

      // uncontrolled component mode
      setFields(newFields);

      // WARNING: this code hasn't been tested yet
      typeof onChange === 'function' &&
        onChange({ value: newFields, valueMap: fieldsMap(newFields) });
    },
    [fields, fieldsProp, onChange]
  );

  // handle placement change
  const handlePlacementChange = (nextPlacement: Placement) => {
    setPlacement(nextPlacement);
  };

  // get all disabled fields
  const allDisabledFields = useMemo(() => {
    const allDisabledFields = fields.reduce<Record<string, any>>(
      (prev, curr) => {
        const dataItem = dataMap![curr.key];
        if (!dataItem) return prev;

        return { ...prev, ...dataItem.disabledFields };
      },
      {}
    );

    return allDisabledFields;
  }, [dataMap, fields]);

  // render items
  const itemElements = useMemo(() => {
    const result: React.ReactNode[] = [];
    const headers: Record<string, any> = {};

    if (!Array.isArray(dataFiltered) || !dataFiltered.length) return result;

    dataFiltered.forEach(data => {
      const item = { ...data };
      if (item.groupName && !headers[item.groupName]) {
        headers[item.groupName] = true;

        const headerElement = (
          <Header
            key={item.groupName}
            name={item.groupName}
            dataTestId={genAmtId(
              dataTestId,
              `${name}-params-header`,
              'AttributeSearch.Header'
            )}
          />
        );
        result.push(headerElement);
      }

      const disabledAttribute: DisabledFields | undefined = find(
        disabledFields,
        {
          key: item.key
        }
      );

      const isDisabled =
        (allDisabledFields && allDisabledFields.hasOwnProperty(item.key)) ||
        !isEmpty(disabledAttribute);

      // update info for item disabled
      if (isDisabled) {
        item.disabled = isDisabled;
        item.description =
          (allDisabledFields as Record<string, string>)[item.key] ||
          get(disabledAttribute, ['description'], item.description);
      }

      // find next item not disabled to update position for keyboard up/down, state item selected
      if (isDisabled && itemSelected && itemSelected.key === item.key) {
        let itemSelectedIndex = dataFiltered.indexOf(itemSelected);
        let nextItem = dataFiltered[++itemSelectedIndex];

        while (nextItem) {
          const isDisabled =
            allDisabledFields && allDisabledFields.hasOwnProperty(nextItem.key);
          if (isDisabled) {
            nextItem = dataFiltered[++itemSelectedIndex];
            continue;
          }

          itemPositionRef.current = itemSelectedIndex;
          setItemSelected(nextItem);
          break;
        }

        if (!nextItem) {
          itemPositionRef.current = -1;
          setItemSelected(null);
        }
      }

      const itemElement = (
        <Item
          key={item.key}
          ref={ref => {
            // support for calc perfect scrollbar move up/down
            item.itemElement = ref;
          }}
          onClick={event => {
            // prevent onClick bunbble on dropdown parent
            event.stopPropagation();
            if (!itemSelected || isDisabled) return;

            handleAddField(item);
          }}
          name={t(item.name)}
          valueInputFilter={valueInputFilter}
          description={item.description}
          className={classnames({
            selected:
              !isDisabled && itemSelected && itemSelected.name === item.name,
            disabled: isDisabled
          })}
          dataTestId={genAmtId(
            dataTestId!,
            'params-item',
            'AttributeSearch.Header'
          )}
        />
      );

      result.push(itemElement);
    });

    return result;
  }, [
    dataFiltered,
    disabledFields,
    allDisabledFields,
    itemSelected,
    t,
    valueInputFilter,
    dataTestId,
    handleAddField
  ]);

  // render fields
  const fieldElements = useMemo(() => {
    let realFields = fields;

    // in controlled mode, need useEffect to update fields
    // => mismatch fields and fieldsProp in 1 round
    // fix this
    if (fieldsProp && typeof onChange === 'function' && dataMap) {
      realFields = fieldsProp.map(fieldProp => ({
        ...dataMap[fieldProp.key],
        ...fieldProp
      }));
    }

    const result = realFields.map((item, index) => {
      const Component = item.component as React.ReactType;
      const valueComponent = item.value;

      const fieldError = error && error[item.key];

      const componentProps = {
        itemKey: item.key,
        onRemoveField: handleRemoveField,
        onNeedFocusInputFilter: () => setNeedFocusInputFilter(true),
        onSearch: handleSearch,
        /** ********************/
        value: valueComponent,
        onChange: (event: ChangeComponentEvent) => {
          handleChangeComponent(event, item);
        },
        onFocus: () => {
          setFieldFocused(item);
          if (show) return;
          process.nextTick(() => setShow(true));
        },
        onBlur: () => setTriggerBlurAfterThat(true),
        anchor: attrSearchRef.current,
        allowFieldAutoFocus:
          needValidateFirstTime === true || !show
            ? false
            : !keepFields.current[item.key],
        className: 'mousetrap',
        dataTestId: genAmtId(
          `${item.key}_${dataTestId || ''}`,
          'field',
          'AttributeSearch'
        )
      };

      if (!keepFields.current[item.key]) {
        keepFields.current[item.key] = item;
      }

      const fieldDetailElement = (
        <div
          className={classnames(
            'field-detail',
            fieldFocused && fieldFocused.key === item.key && 'focused'
          )}
        >
          <span
            data-testid={genAmtId(
              `${item.key}_${dataTestId || ''}`,
              'field-name',
              'AttributeSearch'
            )}
          >
            {`${t(item.name)}:`}
          </span>
          <Component {...componentProps} />
          <button
            tabIndex={-1}
            onFocus={() => handleRemoveField({ byFieldKey: item.key })}
            className="icon icon-close close-button"
            data-testid={genAmtId(
              `${item.key}_${dataTestId || ''}`,
              'field-close-btn',
              'AttributeSearch'
            )}
          />
        </div>
      );

      return (
        <div
          key={item.key}
          className={classnames('ds-attr-search-field', fieldError && 'error')}
        >
          <input
            disabled
            ref={ref => {
              if (!keepFields.current[item.key]) return;
              keepFields.current[item.key]!.inputElement = ref;
            }}
            value=""
            onChange={() => undefined}
            type="text"
            className={classnames('input-position', 'mousetrap')}
            data-testid={genAmtId(
              `${item.key}_${dataTestId || ''}`,
              'field-input',
              'AttributeSearch'
            )}
          />
          {fieldError ? (
            <Tooltip
              modifiers={defaultModifiers(
                attrSearchRef.current as HTMLDivElement,
                typeof onClear === 'function',
                small
              )}
              variant="error"
              placement="top-start"
              element={fieldError}
              dataTestId={genAmtId(
                `${item.key}_${dataTestId || ''}`,
                'field-error',
                'AttributeSearch'
              )}
              {...tooltipProps}
            >
              {fieldDetailElement}
            </Tooltip>
          ) : (
            fieldDetailElement
          )}
        </div>
      );
    });

    return result;
  }, [
    fields,
    fieldsProp,
    onChange,
    dataMap,
    error,
    handleRemoveField,
    handleSearch,
    needValidateFirstTime,
    show,
    dataTestId,
    fieldFocused,
    t,
    onClear,
    small,
    tooltipProps,
    handleChangeComponent
  ]);

  // clear button element
  const clearButtonElement = useMemo(() => {
    if (
      !attrSearchRef.current ||
      (!valueInputFilter && !fields.length) ||
      typeof onClear !== 'function'
    ) {
      return;
    }

    return (
      <button
        ref={clearBtnRef}
        tabIndex={-1}
        onMouseDown={handleClear}
        className="ds-attr-clear-button"
        data-testid={genAmtId(dataTestId, 'clear-btn', 'AttributeSearch')}
      >
        <Tooltip
          variant="primary"
          element="Clear and Reset Search"
          placement="top"
          dataTestId={genAmtId(dataTestId, 'reset-button', 'AttributeSearch')}
          {...clearTooltipProps}
        >
          <span className="icon icon-close" />
        </Tooltip>
      </button>
    );
  }, [
    valueInputFilter,
    fields.length,
    onClear,
    handleClear,
    clearTooltipProps,
    dataTestId
  ]);

  const extraClassNames = classnames({
    opened: show,
    error,
    small,
    focused: fieldFocused
  });

  return (
    <>
      <div
        data-testid={genAmtId(
          dataTestId,
          'dls-attribute-search',
          'AttributeSearch'
        )}
        ref={attrSearchRef}
        className={classnames('dls-attribute-search', extraClassNames)}
        data-popper-placement={placement}
      >
        <div
          ref={hammerRef}
          className="dls-attribute-search-hammer"
          data-popper-placement={placement}
        >
          <div className="dls-attribute-search-pan-zone">
            {fieldElements}
            <input
              ref={inputFilterRef}
              value={valueInputFilter}
              onChange={handleOnChangeInputFilter}
              onFocus={handleOnFocusInputFilter}
              placeholder={fieldFocused ? '' : placeholder}
              type="text"
              className={classnames(
                'dls-attribute-search-input-filter',
                'mousetrap'
              )}
              data-testid={genAmtId(
                dataTestId,
                'dls-attribute-search_input',
                'AttributeSearch'
              )}
            />
          </div>
          {clearButtonElement}
          <button
            ref={searchBtnRef}
            tabIndex={-1}
            onFocus={handleSearch}
            className="dls-attribute-search-button"
            data-testid={genAmtId(
              dataTestId,
              'dls-attribute-search_search-btn',
              'AttributeSearch'
            )}
          >
            <span className="icon icon-search" />
          </button>
        </div>
      </div>
      <PopupBase
        opened={show}
        reference={attrSearchRef.current as HTMLElement}
        style={{
          width: attrSearchRef.current
            ? attrSearchRef.current.offsetWidth
            : 'auto'
        }}
        popupBaseClassName={classnames(
          'dls-attribute-search-popup',
          popupClassName
        )}
        popupAnimationClassName="dls-attribute-search-popup-animation"
        anchor={anchor}
        fluid
        onPlacementChange={handlePlacementChange}
        dataTestId={`${dataTestId}-popup-attribute-search`}
      >
        <div>
          <div
            ref={setDropdownElement}
            className="dls-attribute-search-dropdown"
          >
            {itemElements.length ? (
              itemElements
            ) : (
              <span
                className="no-data"
                data-testid={genAmtId(
                  dataTestId,
                  'no-results',
                  'AttributeSearch.NoResults'
                )}
              >
                {noFilterResultText}
              </span>
            )}
          </div>
          {footer || <Footer dataTestId={dataTestId} />}
        </div>
      </PopupBase>
    </>
  );
};

export default React.forwardRef<AttributeSearchRef, AttributeSearchProps>(
  AttributeSearch
);
