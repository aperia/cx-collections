import React from 'react';
import '@testing-library/jest-dom';

// components
import { AmountControlProps } from '.';
import MockAmountControlWrapper from './MockAmountControlWrapper';

import { render, RenderResult } from '@testing-library/react';
import { queryByClass } from '../../../../test-utils/queryHelpers';

// mocks
import '../../../../test-utils/mocks/mockCanvas';
import userEvent from '@testing-library/user-event';
import { act } from 'react-dom/test-utils';

let wrapper: RenderResult;
let container: HTMLElement;

const renderComponent = (props: Partial<AmountControlProps>) => {
  jest.useFakeTimers();

  wrapper = render(
    <MockAmountControlWrapper ref={{ current: null }} {...props} />
  );

  container = wrapper.container as HTMLElement;

  jest.runAllTimers();
};

const getNode = {
  get input() {
    return queryByClass(
      wrapper.container,
      /input-control/
    )! as HTMLInputElement;
  }
};

describe('Render', () => {
  it('should render with string value 0', () => {
    renderComponent({ value: '0', regex: new RegExp(`^[0-9]`) });

    expect(getNode.input).toBeInTheDocument();
    expect(getNode.input.value).toEqual('0.00');
  });

  describe('should render with number value 0', () => {
    it('allow show number', () => {
      renderComponent({
        value: 0,
        regex: new RegExp(`^[0-9]`),
        isAllShowNumber: true,
        placeholder: 'placeholder'
      });

      expect(getNode.input).toBeInTheDocument();
      expect(getNode.input.value).toEqual('0.00');
    });

    it('not allow show number', () => {
      renderComponent({
        value: 0,
        regex: new RegExp(`^[0-9]`),
        isAllShowNumber: false,
        placeholder: 'placeholder'
      });

      expect(getNode.input).toBeInTheDocument();
      expect(getNode.input.value).toEqual('placeholder');
    });
  });
});

describe('Actions', () => {
  describe('handleFocus and handleBlur', () => {
    it('when user click clearButton after focus input', () => {
      const onChangeIsEdit = jest.fn();
      const onBlur = jest.fn();
      const onFocus = jest.fn();
      const onChange = jest.fn();

      renderComponent({
        onChangeIsEdit,
        onBlur,
        onFocus,
        onChange
      });

      // focus input
      userEvent.click(getNode.input);

      expect(onChangeIsEdit).toBeCalledWith(true);
      expect(onFocus).toBeCalled();

      userEvent.click(queryByClass(container, /ds-attr-clear-button/)!);

      expect(onChangeIsEdit).toBeCalledWith(false);
      expect(onBlur).toBeCalled();
      expect(onChange).not.toBeCalled();
    });

    it('when user click body after focus input', () => {
      const onChangeIsEdit = jest.fn();
      const onBlur = jest.fn();
      const onFocus = jest.fn();
      const onChange = jest.fn();

      renderComponent({ onChangeIsEdit, onBlur, onFocus, onChange });

      userEvent.click(getNode.input);

      expect(onChangeIsEdit).toBeCalledWith(true);
      expect(onFocus).toBeCalled();

      userEvent.click(document.body);

      expect(onChangeIsEdit).toBeCalledWith(false);
      expect(onBlur).toBeCalled();
      expect(onChange).not.toBeCalled();
    });
  });

  describe('handlePaste', () => {
    it('when paste number', () => {
      const value = '095684';
      const onChange = jest.fn();

      renderComponent({ onChange });

      userEvent.paste(getNode.input, value, {
        clipboardData: { getData: () => value } as unknown as DataTransfer
      });

      expect(onChange).toBeCalled();
    });

    it('when paste string', () => {
      const value = 'text';
      const onChange = jest.fn();

      renderComponent({ onChange });

      userEvent.paste(getNode.input, value, {
        clipboardData: { getData: () => value } as unknown as DataTransfer
      });

      expect(onChange).not.toBeCalled();
    });
  });

  describe('handleChange', () => {
    const onChange = jest.fn();
    const onKeyDown = jest.fn();

    renderComponent({ onChange, onKeyDown });

    act(() => {
      userEvent.type(getNode.input, '0..00');
    });

    expect(onKeyDown).toBeCalledTimes(4);
  });
});
