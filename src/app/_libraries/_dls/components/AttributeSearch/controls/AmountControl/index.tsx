import React, {
  useRef,
  useEffect,
  useImperativeHandle,
  useState,
  useCallback
} from 'react';

// components/types
import Input, { InputControlProps } from '../InputControl';

// utils & helper
import isString from 'lodash.isstring';
import isFunction from 'lodash.isfunction';
import {
  // DEFAULT_SET_MIN_VALUE,
  // getNewValue,
  haveDot,
  isKeyDot,
  isKeyValid
} from '../AmountRangeControl/helper';

export interface AmountControlProps
  extends Partial<InputControlProps | any>,
    DLSId {
  onNeedFocusInputFilter: () => void;
  onSearch: () => void;
  onBlur: (event: React.FocusEvent<HTMLInputElement>) => void;
  onFocus: (event: React.FocusEvent<HTMLInputElement>) => void;
  onChangeIsEdit?: (newValue: boolean) => void;

  defaultValueBlur: React.ReactText;
  value: string | number;
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onKeyDown?: (event: React.KeyboardEvent<HTMLInputElement>) => void;
  isEdit?: boolean;
  maxLength?: number;
  maxDecimalLength?: number;
  regex?: RegExp | any;
  isAllShowNumber?: boolean;
}

const AmountControl = React.forwardRef<HTMLInputElement, AmountControlProps>(
  (props, ref) => {
    const {
      onNeedFocusInputFilter,
      value: valueProp = '',
      onChange: onChangeProp,
      onKeyDown: onKeyDownProp,
      onBlur: onBlurProp,
      onFocus: onFocusProp,
      onChangeIsEdit,
      maxLength: maxLengthProp = 11,
      maxDecimalLength: maxDecimalLengthProp = 2,
      regex: regexProp,
      placeholder,
      isAllShowNumber = false,
      allowFieldAutoFocus,
      // defaultValueBlur = DEFAULT_SET_MIN_VALUE,
      isEdit: isEditProp,
      anchor,

      dataTestId,
      ...rest
    } = props;

    // ref
    const inputRef = useRef<HTMLInputElement | null>(null);
    const latestKey = useRef<string>('');

    // state
    const [isEdit, setIsEdit] = useState(isEditProp || false);
    const [validationRegex, setValidationRegex] = useState(regexProp);

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      const newValue = event.target.value;

      if (!validationRegex.test(newValue)) return;

      const newEvent = {
        ...event,
        target: { ...event.target, value: newValue }
      };
      onChangeProp && onChangeProp(newEvent);
    };

    const handleKeyDown = useCallback(
      (event: React.KeyboardEvent<HTMLInputElement>) => {
        setIsEdit(true);
        if (
          !isKeyValid(event) ||
          (isKeyDot(event.key) &&
            (haveDot(`${valueProp}`) || isKeyDot(latestKey.current)))
        ) {
          event.preventDefault();
          return;
        }

        latestKey.current = event.key;
        isFunction(onKeyDownProp) && onKeyDownProp(event);
      },
      [onKeyDownProp, valueProp]
    );

    const handleBlur = (event: React.FocusEvent<HTMLDivElement>) => {
      setIsEdit(false);
      isFunction(onChangeIsEdit) && onChangeIsEdit(false);
      isFunction(onBlurProp) && onBlurProp(event);

      const containerElement = inputRef.current as HTMLDivElement;

      const clearButtonElement = anchor.querySelector('.ds-attr-clear-button')!;
      const closeButtonElement = containerElement
        ?.closest('.field-detail')
        ?.querySelector('.close-button') as HTMLElement;
      const relatedTarget = event.relatedTarget as HTMLElement;

      // click on Clear || remove this control
      if (
        (closeButtonElement && closeButtonElement.contains(relatedTarget)) ||
        (clearButtonElement && clearButtonElement.contains(relatedTarget))
      ) {
        return;
      }
      // const newValue = getNewValue(valueProp, defaultValueBlur);
      // const newEvent = { target: { value: newValue }, value: newValue };

      // isFunction(onChangeProp) && onChangeProp(newEvent);
    };

    const handleFocus = (event: React.FocusEvent<HTMLInputElement>) => {
      setIsEdit(true);
      isFunction(onChangeIsEdit) && onChangeIsEdit(true);
      isFunction(onFocusProp) && onFocusProp(event);
    };

    const handlePaste = useCallback(
      (event: React.ClipboardEvent<HTMLInputElement>) => {
        const data = event.clipboardData.getData('text/plain');
        if (validationRegex.test(data)) return;

        event.preventDefault();
      },
      [validationRegex]
    );

    const formatCurrency = useCallback((value: string | number) => {
      const formatterCurrency = new Intl.NumberFormat('en-US', {
        minimumFractionDigits: 2
      });
      const floatValue = `${value}` === '0' ? 0 : parseFloat(`${value}`);
      return !floatValue && floatValue !== 0
        ? ''
        : formatterCurrency.format(floatValue);
    }, []);

    const value = isEdit
      ? valueProp
      : isString(valueProp) || (isAllShowNumber && !isEdit)
      ? formatCurrency(valueProp)
      : placeholder;

    useEffect(() => {
      setValidationRegex(
        regexProp
          ? regexProp
          : new RegExp(
              `^[0-9]{0,${maxLengthProp}}(\\.([0-9]{1,${maxDecimalLengthProp}})?)?$`
            )
      );
    }, [regexProp, maxDecimalLengthProp, maxLengthProp]);

    useEffect(() => setIsEdit(isEdit), [isEdit]);

    useImperativeHandle(ref, () => inputRef.current!);

    return (
      <Input
        type="text"
        ref={inputRef}
        onNeedFocusInputFilter={onNeedFocusInputFilter}
        onKeyDown={handleKeyDown}
        value={value}
        onChange={handleChange}
        autoFocus={allowFieldAutoFocus}
        onBlur={handleBlur}
        onFocus={handleFocus}
        onRemoveField={() => null}
        onPaste={handlePaste}
        placeholder={placeholder}
        dataTestId={dataTestId}
        {...rest}
      />
    );
  }
);

export default AmountControl;
