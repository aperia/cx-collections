import React, {
  useRef,
  useImperativeHandle,
  useState,
  useEffect,
  useLayoutEffect
} from 'react';

// components
import ComboBox, { ComboBoxProps, ComboBoxRef } from '../../../ComboBox';

// utils
import isFunction from 'lodash.isfunction';
import isString from 'lodash.isstring';
import classnames from 'classnames';
import { canvasTextWidth, keycode, className } from '../../../../utils';
import { DropdownBaseChangeEvent } from '../../../DropdownBase';

const { BACKSPACE, DELETE, ENTER } = keycode;

export interface ComboBoxControlProps extends ComboBoxProps, DLSId {
  itemKey?: string;
  onRemoveField: (config: {
    byPosition?: number;
    byFieldKey?: string;
    byAction?: 'click' | 'keyboard';
  }) => void;
  onNeedFocusInputFilter: () => void;
  onSearch: () => void;
  /** 4 required properties (above) must be handled separately for each component type  */
  allowFieldAutoFocus?: boolean;
}
const Combobox: React.RefForwardingComponent<
  ComboBoxRef,
  ComboBoxControlProps
> = (
  {
    value,
    onChange,
    itemKey,
    onRemoveField,
    onBlur,

    allowFieldAutoFocus,
    placeholder,
    children,
    className: classNameProp,

    dataTestId,
    ...props
  },
  ref
) => {
  // refs
  const comboBoxRef = useRef<ComboBoxRef | null>(null);
  const isMouseDownRef = useRef(false);

  // states
  const [opened, setOpened] = useState(false);
  const [filter, setFilter] = useState('');

  const handleVisibilityChange = (nextOpened: boolean) => setOpened(nextOpened);

  useImperativeHandle(ref, () => comboBoxRef.current!);

  // handle dynamic input width
  useLayoutEffect(() => {
    const inputElement = comboBoxRef.current?.inputElement as HTMLInputElement;

    const extraWidth =
      isString(filter) && filter[filter.length - 1] === ' ' ? 16 : 10;
    const textMetrics = canvasTextWidth(filter || placeholder || '');
    if (!textMetrics) return;

    const width = textMetrics.width + extraWidth;

    inputElement.style.width = `${width}px`;
  }, [filter, placeholder]);

  // add event onKeydown to input comboBox
  useEffect(() => {
    const inputElement = comboBoxRef.current?.inputElement as HTMLInputElement;

    // handle keydown Input
    // handle AttributeSearch keyboards: backspace/delete to remove field
    const handleKeydownInput = (event: KeyboardEvent) => {
      const { keyCode } = event;

      // enter to close dropdown & search
      if (keyCode === ENTER) setOpened(false);

      // backspace/delete to remove field
      if (
        !inputElement.value &&
        (keyCode === BACKSPACE || keyCode === DELETE)
      ) {
        isFunction(onRemoveField) && onRemoveField({ byFieldKey: itemKey });
      }
    };

    inputElement.addEventListener('keydown', handleKeydownInput);
    return () => {
      inputElement.removeEventListener('keydown', handleKeydownInput);
    };
  }, [comboBoxRef, itemKey, onRemoveField]);

  // prevent trigger internal behavior of the ComboBox's popup element
  useEffect(() => {
    if (!opened) return;

    process.nextTick(() => {
      const popupElement = comboBoxRef.current?.popupBaseRef?.current?.element;

      const handleMouseDown = (event: MouseEvent) => {
        const target = event.target as HTMLElement;

        // TODO: never happen
        if (target.closest(`.${className.dropdownBase.ITEM}`)) {
          isMouseDownRef.current = true;
        }
      };

      popupElement?.addEventListener('mousedown', handleMouseDown, true);
    });
  }, [opened]);

  // provides mousedown toggle opened to input element
  useEffect(() => {
    const inputElement = comboBoxRef.current?.inputElement;

    const handleMouseDown = () => setOpened(!opened);

    inputElement?.addEventListener('mousedown', handleMouseDown);
    return () => {
      inputElement?.removeEventListener('mousedown', handleMouseDown);
    };
  }, [opened]);

  const handleOnChange = (event: DropdownBaseChangeEvent) => {
    isFunction(onChange) && onChange(event);
  };

  const handleOnBlur = (event: React.FocusEvent<Element>) => {
    // if isMouseDownRef.current is true and value changes
    // we know that value is changed by user click
    if (isMouseDownRef.current) {
      isMouseDownRef.current = false;
      comboBoxRef.current?.inputElement?.focus();
      return;
    }
    isFunction(onBlur) && onBlur(event);
  };

  return (
    <div className={classnames(classNameProp, 'combobox-control')}>
      <ComboBox
        ref={comboBoxRef}
        value={value}
        onChange={handleOnChange}
        opened={opened}
        onVisibilityChange={handleVisibilityChange}
        onFilterChange={(event) => setFilter(event.target.value)}
        onBlur={handleOnBlur}
        popupBaseProps={{
          popupBaseClassName: classnames('w-auto', 'dls-control-popup')
        }}
        autoFocus={allowFieldAutoFocus}
        placeholder={placeholder}
        isSearchControl
        dataTestId={dataTestId}
        {...props}
      >
        {children}
      </ComboBox>
    </div>
  );
};

export default React.forwardRef<ComboBoxRef, ComboBoxControlProps>(Combobox);
