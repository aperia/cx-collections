import React, { useRef, ReactText, useImperativeHandle } from 'react';

// utils
import isFunction from 'lodash.isfunction';
import classnames from 'classnames';

// components/types
import AmountControl, { AmountControlProps } from '../AmountControl';
export type AmountRangeControlType = 'min' | 'max';

// helper
import {
  DEFAULT_SET_MAX_VALUE,
  getNewValue,
  DEFAULT_VALUE,
  DEFAULT_SET_MIN_VALUE
} from './helper';

export interface AmountRangeControlProps extends DLSId {
  itemKey?: string;
  onRemoveField: (config: {
    byPosition?: number | undefined;
    byFieldKey?: string | undefined;
    byAction?: 'click' | 'keyboard' | undefined;
  }) => void;
  onNeedFocusInputFilter: () => void;
  onSearch: () => void;
  anchor: HTMLElement;
  /** 4 required properties (above) must be handled separately for each component type  */
  defaultValue: string[] | number[];
  value: any[] | string[] | number[];
  onChange: any;
  onBlur: () => void;
  onFocus: () => void;
  className: string;
  minProps?: AmountControlProps;
  maxProps?: AmountControlProps;
  allowFieldAutoFocus?: boolean;
}

interface AmountRangeControlRef {
  minRef: HTMLInputElement;
  maxRef: HTMLInputElement;
}

const AmountRangeControl = React.forwardRef<
  AmountRangeControlRef,
  AmountRangeControlProps
>((props, ref) => {
  const {
    itemKey,
    onRemoveField,
    onNeedFocusInputFilter,
    onSearch,
    anchor,
    /** 4 required properties (above) must be handled separately for each component type  */
    value: valueProp = DEFAULT_VALUE,
    onChange: onChangeProp,
    onBlur: onBlurProp,
    onFocus: onFocusProp,
    allowFieldAutoFocus,
    maxProps,
    minProps,
    defaultValue,

    dataTestId
  } = props;

  // never happen
  // default value is required field
  const [defaultSetMin, defaultSetMax] = defaultValue || [
    DEFAULT_SET_MIN_VALUE,
    DEFAULT_SET_MAX_VALUE
  ];
  const [minValue, maxValue] = valueProp;
  // ref
  const containerRef = useRef<HTMLDivElement | null>(null);
  const minRef = useRef<HTMLInputElement | null>(null);
  const maxRef = useRef<HTMLInputElement | null>(null);

  const getNewValueByType = (
    type: AmountRangeControlType,
    newValue: ReactText
  ) => {
    return type === 'min' ? [newValue, maxValue] : [minValue, newValue];
  };

  const handleChange = (
    event: React.ChangeEvent<HTMLInputElement>,
    type: AmountRangeControlType
  ) => {
    const eventValue = event.target.value;
    const newValue = getNewValueByType(type, eventValue);

    const newEvent = {
      ...event,
      value: newValue,
      target: { ...event.target, value: newValue }
    };
    onChangeProp && onChangeProp(newEvent);
  };

  const handleKeyDown = (
    event: React.KeyboardEvent<HTMLInputElement>,
    type: AmountRangeControlType
  ) => {
    const minInputElement = minRef.current;
    const maxInputElement = maxRef.current;
    const { keyCode } = event;

    const currentInput = {
      type,
      input: type === 'min' ? minInputElement : maxInputElement,
      value: type === 'min' ? minValue : maxValue,
      action: () => {
        if (type === 'max') {
          minInputElement?.focus();
          event.preventDefault();
          return;
        }
        isFunction(onRemoveField) && onRemoveField({ byFieldKey: itemKey });
      }
    };

    // never happen
    // currentInput.input has never null
    const { selectionStart, selectionEnd } = currentInput.input!;
    const isAllowDelete = keyCode === 46 && !currentInput.value;
    const isAllowBackSpace =
      keyCode === 8 && selectionStart === selectionEnd && selectionStart === 0;

    if (!isAllowDelete && !isAllowBackSpace) return;

    currentInput.action();
  };

  const handleBlur = (event: React.FocusEvent<HTMLDivElement>) => {
    const containerElement = containerRef.current as HTMLDivElement;

    const minInputElement = minRef.current!;
    const maxInputElement = maxRef.current!;

    const searchButtonElement = anchor.querySelector(
      '.dls-attribute-search-button'
    )!;
    const clearButtonElement = anchor.querySelector('.ds-attr-clear-button')!;
    const closeButtonElement = containerElement
      ?.closest('.field-detail')
      ?.querySelector('.close-button') as HTMLElement;
    const relatedTarget = event.relatedTarget as HTMLElement;
    const target = event.target as HTMLElement;

    if (
      (closeButtonElement && closeButtonElement.contains(relatedTarget)) ||
      (clearButtonElement && clearButtonElement.contains(relatedTarget))
    ) {
      return;
    }
    const nextValue = [
      getNewValue(minValue, defaultSetMin),
      getNewValue(maxValue, defaultSetMax)
    ];
    const nextEvent = { target: { value: nextValue }, value: nextValue };

    if (searchButtonElement && searchButtonElement.contains(relatedTarget)) {
      // check current active is element in Amount range
      const isActiveElement =
        (minInputElement && minInputElement.contains(target)) ||
        (maxInputElement && maxInputElement.contains(target));

      // never happen
      // onChangeProp is required field
      if (isActiveElement && isFunction(onChangeProp)) {
        onChangeProp(nextEvent);
        return;
      }
    }

    process.nextTick(() => {
      // check element active is not focus max/min Input => call validation check
      if (
        document &&
        document.activeElement &&
        !containerElement.contains(document.activeElement)
      ) {
        isFunction(onChangeProp) && onChangeProp(nextEvent);
        isFunction(onBlurProp) && onBlurProp(event);
        return;
      }
    });
  };

  const handleBlurAmount = (type: AmountRangeControlType) => (event: any) => {
    const isMin = type === 'min';
    const elm = isMin ? minRef : maxRef;
    const defaultValue = isMin
      ? minProps?.defaultValueBlur || DEFAULT_SET_MIN_VALUE
      : maxProps?.defaultValueBlur || DEFAULT_SET_MAX_VALUE;
    const valueProp = isMin ? minValue : maxValue;

    const containerElement = elm.current as HTMLDivElement;

    const clearButtonElement = anchor.querySelector('.ds-attr-clear-button')!;
    const closeButtonElement = containerElement
      ?.closest('.field-detail')
      ?.querySelector('.close-button') as HTMLElement;
    const relatedTarget = event.relatedTarget as HTMLElement;

    // click on Clear || remove this control
    if (
      (closeButtonElement && closeButtonElement.contains(relatedTarget)) ||
      (clearButtonElement && clearButtonElement.contains(relatedTarget))
    ) {
      return;
    }

    const newValue = getNewValue(valueProp, defaultValue);
    const newEvent: any = { target: { value: newValue }, value: newValue };

    handleChange(newEvent, type);
  };
  const rootClassName = classnames('rangeNumber', {
    active:
      document.activeElement !== minRef.current ||
      document.activeElement !== maxRef.current
  });

  useImperativeHandle(ref, () => ({
    minRef: minRef.current!,
    maxRef: maxRef.current!
  }));

  return (
    <div
      ref={containerRef}
      className={rootClassName}
      onBlur={handleBlur}
      onFocus={onFocusProp}
    >
      <AmountControl
        key="min"
        ref={minRef}
        onNeedFocusInputFilter={onNeedFocusInputFilter}
        onKeyDown={(event: React.KeyboardEvent<HTMLInputElement>) => {
          handleKeyDown(event, 'min');
        }}
        onSearch={onSearch}
        value={minValue}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
          handleChange(event, 'min')
        }
        onBlur={handleBlurAmount('min')}
        allowFieldAutoFocus={allowFieldAutoFocus}
        anchor={anchor}
        dataTestId={`${dataTestId}-min`}
        {...minProps}
      />
      <span className="text-dash">{' - '}</span>
      <AmountControl
        key="max"
        ref={maxRef}
        autoFocus={false}
        onNeedFocusInputFilter={onNeedFocusInputFilter}
        onKeyDown={(event: React.KeyboardEvent<HTMLInputElement>) => {
          handleKeyDown(event, 'max');
        }}
        onSearch={onSearch}
        value={maxValue}
        onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
          handleChange(event, 'max')
        }
        onBlur={handleBlurAmount('max')}
        defaultValueBlur={DEFAULT_SET_MAX_VALUE}
        anchor={anchor}
        dataTestId={`${dataTestId}-max`}
        {...maxProps}
      />
    </div>
  );
});

export default AmountRangeControl;
