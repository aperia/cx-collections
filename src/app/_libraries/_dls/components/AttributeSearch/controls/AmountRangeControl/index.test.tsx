import React from 'react';
import '@testing-library/jest-dom';

// components
import MockAmountRangeControlWrapper, {
  MockAmountControlWrapperProps
} from './MockAmountRangeControlWrapper';

import { render, RenderResult } from '@testing-library/react';
import {
  queryAllByClass,
  queryByClass
} from '../../../../test-utils/queryHelpers';

// mocks
import '../../../../test-utils/mocks/mockCanvas';
import userEvent from '@testing-library/user-event';

// utils
import { act } from 'react-dom/test-utils';

let wrapper: RenderResult;
let container: HTMLElement;

interface RenderComponentProps
  extends Omit<MockAmountControlWrapperProps, 'onRemoveField'> {
  onRemoveField?: jest.Mock;
  onNeedFocusInputFilter?: jest.Mock;
  onSearch?: jest.Mock;
  onFocus?: jest.Mock;
  onChange?: jest.Mock;
  onBlur?: jest.Mock;
  defaultValue?: string[] | number[];
  className?: string;
}

const renderComponent = ({
  onRemoveField = jest.fn(),
  onNeedFocusInputFilter = jest.fn(),
  onSearch = jest.fn(),
  onFocus = jest.fn(),
  onChange,
  onBlur = jest.fn(),
  defaultValue,
  className = '',
  ...props
}: RenderComponentProps) => {
  jest.useFakeTimers();

  wrapper = render(
    <MockAmountRangeControlWrapper
      className={className}
      onSearch={onSearch}
      onFocus={onFocus}
      onChange={onChange}
      onBlur={onBlur}
      defaultValue={defaultValue as unknown as string[]}
      onNeedFocusInputFilter={onNeedFocusInputFilter}
      onRemoveField={onRemoveField}
      {...props}
    />
  );
  container = wrapper.container as HTMLElement;
};

const getNode = {
  get container() {
    return queryByClass(wrapper.container, /rangeNumber/)! as HTMLInputElement;
  },
  get inputMin() {
    return queryAllByClass(
      wrapper.container,
      /input-control/
    )[0]! as HTMLInputElement;
  },
  get inputMax() {
    return queryAllByClass(
      wrapper.container,
      /input-control/
    )[1]! as HTMLInputElement;
  }
};

const userBlur = () => {
  act(() => {
    userEvent.click(document.body);
    jest.runAllTimers();
  });
};

const userPress = (element: HTMLElement, text: string) => {
  act(() => {
    userEvent.click(element);
    jest.runAllTimers();
    userEvent.type(element, text);
    jest.runAllTimers();
    userBlur();
  });
};

describe('Render', () => {
  it('should render', () => {
    renderComponent({ value: [123, 321] });

    expect(getNode.container).toBeInTheDocument();
  });
});

describe('Actions', () => {
  describe('handleChange, handleFocus and handleBlur', () => {
    it('when press in min input', () => {
      const onBlur = jest.fn();
      const onChange = jest.fn();

      renderComponent({ onChange, onBlur });

      userPress(getNode.inputMin, '123');

      expect(onBlur).toBeCalled();
      expect(onChange).toBeCalled();
    });

    it('when press in max input', () => {
      const onBlur = jest.fn();
      const onChange = jest.fn();

      renderComponent({ onChange, onBlur });

      userPress(getNode.inputMax, '456');

      expect(onBlur).toBeCalled();
      expect(onChange).toBeCalled();
    });

    it('when user click clearButton after focus input', () => {
      const onBlur = jest.fn();
      const onFocus = jest.fn();
      const onChange = jest.fn();

      renderComponent({ onBlur, onFocus, onChange });

      // focus input
      userEvent.click(getNode.inputMin);

      expect(onFocus).toBeCalled();

      userEvent.click(queryByClass(container, /ds-attr-clear-button/)!);
      jest.runAllTimers();

      expect(onBlur).not.toBeCalled();
      expect(onChange).not.toBeCalled();
    });

    it('when user click searchButton after focus input min without prop onChange', () => {
      const onBlur = jest.fn();
      const onFocus = jest.fn();
      const onChange = jest.fn();

      renderComponent({ onBlur, onFocus, onChange });

      // focus input
      userEvent.click(getNode.inputMax);

      expect(onFocus).toBeCalled();

      userEvent.click(queryByClass(container, /dls-attribute-search-button/)!);
      jest.runAllTimers();

      userBlur();

      expect(onChange).toBeCalled();
      expect(onBlur).not.toBeCalled();
    });

    it('when user click searchButton after focus input min', () => {
      const onBlur = jest.fn();
      const onFocus = jest.fn();
      const onChange = jest.fn();

      renderComponent({ onBlur, onFocus, onChange });

      // focus input
      userEvent.click(getNode.inputMax);

      expect(onFocus).toBeCalled();

      userEvent.click(queryByClass(container, /dls-attribute-search-button/)!);
      jest.runAllTimers();

      userBlur();

      expect(onBlur).not.toBeCalled();
      expect(onChange).toBeCalled();
    });

    describe('when user click searchButton after focus input max', () => {
      it('without prop onChange', () => {
        const onBlur = jest.fn();
        const onFocus = jest.fn();

        renderComponent({ onBlur, onFocus });

        // focus input
        userEvent.click(getNode.inputMin);

        expect(onFocus).toBeCalled();

        userEvent.click(
          queryByClass(container, /dls-attribute-search-button/)!
        );
        jest.runAllTimers();

        userBlur();

        expect(onBlur).toBeCalled();
      });

      it('with prop onChange', () => {
        const onBlur = jest.fn();
        const onFocus = jest.fn();
        const onChange = jest.fn();

        renderComponent({ onBlur, onFocus, onChange });

        // focus input
        userEvent.click(getNode.inputMin);

        expect(onFocus).toBeCalled();

        userEvent.click(
          queryByClass(container, /dls-attribute-search-button/)!
        );
        jest.runAllTimers();

        userBlur();

        expect(onBlur).not.toBeCalled();
        expect(onChange).toBeCalled();
      });
    });

    it('when user click body after focus input', () => {
      const onBlur = jest.fn();
      const onFocus = jest.fn();
      const onChange = jest.fn();

      renderComponent({ onBlur, onFocus, onChange });

      userEvent.click(getNode.inputMin);

      expect(onFocus).toBeCalled();

      userBlur();

      expect(onBlur).toBeCalled();
      expect(onChange).toBeCalled();
    });

    it('press delete after typing value in input min', () => {
      const itemKey = 'itemKey';
      const onRemoveField = jest.fn();
      const onChange = jest.fn();

      renderComponent({ itemKey, onChange, onRemoveField });

      userPress(getNode.inputMin, '456');
      userPress(getNode.inputMin, '{delete}');

      expect(onRemoveField).toBeCalledWith({ byFieldKey: itemKey });
    });

    it('press backspace after typing value in input min', () => {
      const itemKey = 'itemKey';
      const onRemoveField = jest.fn();
      const onChange = jest.fn();

      renderComponent({ itemKey, onChange, onRemoveField });

      act(() => {
        userPress(getNode.inputMax, '456');
      });
      act(() => {
        userPress(getNode.inputMax, '{backspace}');
      });

      expect(onRemoveField).not.toBeCalled();
    });
  });
});
