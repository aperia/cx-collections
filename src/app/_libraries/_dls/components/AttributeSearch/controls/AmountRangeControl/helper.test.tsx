import * as helper from './helper';
import { keycode } from '../../../../utils';
import { KeyboardEvent } from 'react';

describe('AmountRangeControl helper', () => {
  it('formatValue', () => {
    expect(helper.formatValue('.')).toEqual('0');
    expect(helper.formatValue('1.23')).toEqual('1.23');
  });

  it('haveValue', () => {
    expect(helper.haveValue('.', undefined as unknown as number)).toEqual(
      false
    );
  });

  it('getNewValue', () => {
    expect(helper.getNewValue('1.2', undefined as unknown as string)).toEqual(
      '1.2'
    );
    expect(helper.getNewValue('', undefined as unknown as string)).toEqual(
      helper.DEFAULT_SET_MIN_VALUE
    );
  });

  it('isKeyNumber', () => {
    expect(helper.isKeyNumber('5')).toEqual(true);
    expect(helper.isKeyNumber('z')).toEqual(false);
  });

  it('isKeyAction', () => {
    expect(helper.isKeyAction(keycode.UP)).toEqual(false);
    expect(helper.isKeyAction(keycode.C)).toEqual(true);
  });

  it('isKeyDot', () => {
    expect(helper.isKeyDot('A')).toEqual(false);
    expect(helper.isKeyDot('.')).toEqual(true);
  });

  it('isKeyValid', () => {
    expect(
      helper.isKeyValid({
        key: '',
        keyCode: keycode.C
      } as unknown as KeyboardEvent<HTMLInputElement>)
    ).toEqual(true);
  });

  it('haveDot', () => {
    expect(helper.haveDot('1.23')).toEqual(true);
    expect(helper.haveDot('1,23')).toEqual(false);
  });
});
