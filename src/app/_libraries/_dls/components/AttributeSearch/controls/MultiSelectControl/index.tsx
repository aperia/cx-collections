import React from 'react';

// components
import MultiSelect, {
  MultiSelectProps,
  MultiSelectRef
} from '../../../MultiSelect';

// utils
import classnames from 'classnames';

export interface MultiSelectControlProps extends MultiSelectProps, DLSId {
  itemKey?: string;
  onRemoveField: (config: {
    byPosition?: number;
    byFieldKey?: string;
    byAction?: 'click' | 'keyboard';
  }) => void;
  onNeedFocusInputFilter: () => void;
  onSearch: () => void;
  /** 4 required properties (above) must be handled separately for each component type  */
  allowFieldAutoFocus?: boolean;
}

const MultiSelectControl: React.RefForwardingComponent<
  MultiSelectRef,
  MultiSelectControlProps
> = (
  {
    itemKey,
    onRemoveField,

    allowFieldAutoFocus,
    placeholder,
    children,
    className: classNameProp,

    dataTestId,
    ...props
  },
  ref
) => {
  return (
    <div className={classnames(classNameProp, 'multi-select-control')}>
      <MultiSelect
        popupBaseProps={{
          popupBaseClassName: classnames('w-auto', 'dls-control-popup')
        }}
        variant="group"
        searchBar
        checkAll
        autoFocus={allowFieldAutoFocus}
        placeholder={placeholder}
        dataTestId={dataTestId}
        {...props}
      >
        {children}
      </MultiSelect>
    </div>
  );
};

export default React.forwardRef<MultiSelectRef, MultiSelectControlProps>(
  MultiSelectControl
);
