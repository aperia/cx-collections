import { render, RenderResult } from '@testing-library/react';
import React from 'react';
import '../../../../test-utils/mocks/mockCanvas';
import DateRangePickerControl, { DateRangePickerControlProps } from './';

describe('Test AttributeSearch controls > DateRangePickerControl', () => {
  const mockDatePickerProp = {
    placeholder: 'mm/dd/yyyy',
    id: 'mock-dp-id'
  } as DateRangePickerControlProps;

  const renderDateRangePickerControl = (
    props?: DateRangePickerControlProps
  ): RenderResult => {
    return render(
      <div>
        <DateRangePickerControl {...mockDatePickerProp} {...props} />
      </div>
    );
  };

  it('should render DateRangePickerControl UI with placeholder', () => {
    const wrapper = renderDateRangePickerControl();
    const input: HTMLInputElement | null = wrapper.baseElement.querySelector(
      '.text-field-container > input'
    );

    expect(input?.getAttribute('placeholder')).toEqual('mm/dd/yyyy');
  });
});
