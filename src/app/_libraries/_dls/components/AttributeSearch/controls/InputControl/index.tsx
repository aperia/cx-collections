import React, {
  useRef,
  useLayoutEffect,
  useImperativeHandle,
  HTMLProps
} from 'react';

// utils
import classnames from 'classnames';
import { canvasTextWidth, genAmtId, keycode } from '../../../../utils';
import isString from 'lodash.isstring';
import isNil from 'lodash.isnil';
import isFunction from 'lodash.isfunction';
import unset from 'lodash.unset';

// hooks
import { useTranslation } from '../../../../hooks';

export interface InputControlProps extends HTMLProps<HTMLInputElement>, DLSId {
  itemKey?: string;
  onRemoveField: (config: {
    byPosition?: number;
    byFieldKey?: string;
    byAction?: 'click' | 'keyboard';
  }) => void;
  onNeedFocusInputFilter: () => void;
  onSearch: () => void;
  /** 4 required properties (above) must be handled separately for each component type  */
  anchor?: Element;
  allowFieldAutoFocus?: boolean;
  placeholder?: string;
  mode?: 'text' | 'number' | 'alpha';
}

const Input: React.RefForwardingComponent<
  HTMLInputElement,
  InputControlProps
> = (
  {
    itemKey,
    onRemoveField,
    /** 4 required properties (above) must be handled separately for each component type  */
    value,
    onChange,
    onKeyDown,
    onPaste,
    allowFieldAutoFocus,
    mode = 'text',
    maxLength,
    placeholder,
    className,

    dataTestId,
    ...props
  },
  ref
) => {
  // handle eslint no-unused-var, can't destructor params above
  unset(props, 'onNeedFocusInputFilter');
  unset(props, 'onSearch');
  unset(props, 'anchor');
  const { t } = useTranslation();
  const inputRef = useRef<HTMLInputElement | null>(null);
  value = isNil(value) ? '' : value;

  // handle attributesearch Input keyboard logic: Enter for search, BackSpace/Delete for remove field
  const handleOnKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    // mousetrap handled enter keyboard, not need to handle here

    isFunction(onKeyDown) && onKeyDown(event);

    // handle backspace/delete keyboard
    if (
      event.keyCode === keycode.BACKSPACE ||
      event.keyCode === keycode.DELETE
    ) {
      !value &&
        isFunction(onRemoveField) &&
        onRemoveField({ byFieldKey: itemKey });
      return;
    }
  };

  const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    let allowCallOnChange = false;

    switch (mode) {
      case 'text':
        allowCallOnChange = true;
        break;
      case 'number':
        allowCallOnChange = /^\d+$/.test(value);
        break;
      case 'alpha':
        allowCallOnChange = /^[a-zA-Z]+$/.test(value);
        break;
    }

    if (!allowCallOnChange && value) return;
    isFunction(onChange) && onChange(event);
  };

  const handleOnPaste = (event: React.ClipboardEvent<HTMLInputElement>) => {
    const text = event.clipboardData.getData('text');

    isFunction(onPaste) && onPaste(event);

    if (!isNil(maxLength) && text.length > maxLength) {
      event.preventDefault();
    }
  };

  useImperativeHandle(ref, () => inputRef.current!);

  // handle dynamic input width
  useLayoutEffect(() => {
    const extraWidth =
      isString(value) && value[value.length - 1] === ' ' ? 16 : 10;
    const textMetrics = canvasTextWidth((value as string) || placeholder || '');
    if (!textMetrics) return;

    const inputElement = inputRef.current as HTMLInputElement;
    inputElement.style.width = textMetrics.width + extraWidth + 'px';
  }, [value, placeholder]);

    return (
      <input
        ref={inputRef}
        value={value}
        onChange={handleOnChange}
        onKeyDown={handleOnKeyDown}
        onPaste={handleOnPaste}
        autoFocus={allowFieldAutoFocus}
        maxLength={maxLength}
        type="text"
        placeholder={t(placeholder)}
        className={classnames(className, 'input-control')}
        data-testid={genAmtId(dataTestId, 'dls-input-control', 'InputControl')}
        {...props}
      />
    );
  };

export default React.forwardRef<HTMLInputElement, InputControlProps>(Input);
