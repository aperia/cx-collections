import React from 'react';
import '@testing-library/jest-dom';

// components
import InputControl, { InputControlProps } from '.';

import { render, RenderResult } from '@testing-library/react';
import { queryByClass } from '../../../../test-utils/queryHelpers';

// mocks
import '../../../../test-utils/mocks/mockCanvas';
import userEvent from '@testing-library/user-event';

// utils
import * as canvasTextWidth from '../../../../utils/canvasTextWidth';

let wrapper: RenderResult;

const renderComponent = (props: Partial<InputControlProps>) => {
  jest.useFakeTimers();

  wrapper = render(<InputControl ref={{ current: null }} {...props} />);

  jest.runAllTimers();
};

const getNode = {
  get input() {
    return queryByClass(
      wrapper.container,
      /input-control/
    )! as HTMLInputElement;
  }
};

describe('Render', () => {
  it('should render', () => {
    const value = 'value ';
    renderComponent({ value });

    expect(getNode.input).toBeInTheDocument();

    expect(getNode.input.value).toEqual(value);
  });

  it('should render when text is null', () => {
    const spy = jest
      .spyOn(canvasTextWidth, 'default')
      .mockImplementation(() => null);

    renderComponent({});

    expect(getNode.input).toBeInTheDocument();
    expect(getNode.input.value).toEqual('');

    spy.mockReset();
    spy.mockRestore();
  });
});

describe('Actions', () => {
  describe('handleOnPaste', () => {
    const value = '58684';

    it('when value is less than maxLength', () => {
      const onPaste = jest.fn();
      const onChange = jest.fn();

      renderComponent({ onPaste, onChange, maxLength: value.length + 1 });

      userEvent.paste(getNode.input, value, {
        clipboardData: { getData: () => value } as unknown as DataTransfer
      });

      expect(onPaste).toHaveBeenCalled();
      expect(onChange).toHaveBeenCalled();
    });

    it('when value is greater than maxLength', () => {
      const onPaste = jest.fn();
      const onChange = jest.fn();

      renderComponent({ onPaste, onChange, maxLength: value.length - 1 });

      userEvent.paste(getNode.input, value, {
        clipboardData: { getData: () => value } as unknown as DataTransfer
      });

      expect(onPaste).toHaveBeenCalled();
      expect(onChange).toHaveBeenCalled();
    });
  });

  describe('handleOnChange', () => {
    it('value is text', () => {
      const value = 'text';
      const onChange = jest.fn();

      renderComponent({ mode: 'text', onChange });

      userEvent.type(getNode.input, value);

      expect(onChange).toHaveBeenCalled();
    });

    it('value is number', () => {
      const value = '58684';
      const onChange = jest.fn();

      renderComponent({ mode: 'number', onChange });

      userEvent.type(getNode.input, value);

      expect(onChange).toHaveBeenCalled();
    });

    describe('value is number', () => {
      it('when value is number', () => {
        const value = '58684';
        const onChange = jest.fn();

        renderComponent({ mode: 'number', onChange });

        userEvent.type(getNode.input, value);

        expect(onChange).toHaveBeenCalled();
      });

      it('when value is not number', () => {
        const value = 'text';
        const onChange = jest.fn();

        renderComponent({ mode: 'number', onChange });

        userEvent.type(getNode.input, value);

        expect(onChange).not.toHaveBeenCalled();
      });
    });

    describe('value is alpha', () => {
      it('when value is alpha', () => {
        const value = 'tExT';
        const onChange = jest.fn();

        renderComponent({ mode: 'alpha', onChange });

        userEvent.type(getNode.input, value);

        expect(onChange).toHaveBeenCalled();
      });

      it('when value is not alpha', () => {
        const value = 'text';
        const onChange = jest.fn();

        renderComponent({ mode: 'alpha', onChange });

        userEvent.type(getNode.input, value);

        expect(onChange).toHaveBeenCalled();
      });
    });
  });

  describe('handleOnKeyDown', () => {
    it('when press backspace key', () => {
      const value = 'text';
      const itemKey = 'itemKey';
      const onKeyDown = jest.fn();
      const onRemoveField = jest.fn();

      renderComponent({ onKeyDown, onRemoveField, itemKey });

      userEvent.type(getNode.input, value);
      userEvent.type(getNode.input, '{backspace}');

      expect(onKeyDown).toHaveBeenCalled();
      expect(onRemoveField).toBeCalledWith({ byFieldKey: itemKey });
    });

    it('when press delete key', () => {
      const value = 'text';
      const itemKey = 'itemKey';
      const onKeyDown = jest.fn();
      const onRemoveField = jest.fn();

      renderComponent({ onKeyDown, onRemoveField, itemKey });

      userEvent.type(getNode.input, value);
      userEvent.type(getNode.input, '{delete}');

      expect(onKeyDown).toHaveBeenCalled();
      expect(onRemoveField).toBeCalledWith({ byFieldKey: itemKey });
    });
  });
});
