import { act, fireEvent, render, RenderResult } from '@testing-library/react';
import React from 'react';
import '../../../../test-utils/mocks/mockCanvas';
import DatePickerControl, { DatePickerControlProps } from './';

describe('Test AttributeSearch controls > DatePickerControl', () => {
  const mockDatePickerProp = {
    placeholder: 'mm/yyyy',
    id: 'mock-dp-id'
  } as DatePickerControlProps;

  const renderDatePickerControl = (
    props?: DatePickerControlProps
  ): RenderResult => {
    return render(
      <div>
        <DatePickerControl {...mockDatePickerProp} {...props} />
      </div>
    );
  };

  it('should render DatePickerControl UI with placeholder', () => {
    const wrapper = renderDatePickerControl();
    const input: HTMLInputElement | null = wrapper.baseElement.querySelector(
      '.text-field-container > input'
    );

    expect(input?.getAttribute('placeholder')).toEqual('mm/yyyy');
  });

  it('should render DatePickerControl UI with empty placeholder', () => {
    const wrapper = renderDatePickerControl({
      ...mockDatePickerProp,
      placeholder: ''
    });

    const input: HTMLInputElement | null = wrapper.baseElement.querySelector(
      '.text-field-container > input'
    );
    expect(input?.getAttribute('placeholder')).toEqual('mm/dd/yyyy');
  });

  it('should render DatePickerControl UI without placeholder', () => {
    const wrapper = renderDatePickerControl({
      ...mockDatePickerProp,
      placeholder: undefined
    });

    const input: HTMLInputElement | null = wrapper.baseElement.querySelector(
      '.text-field-container > input'
    );
    expect(input?.getAttribute('placeholder')).toEqual('mm/dd/yyyy');

    act(() => {
      fireEvent.change(input!, { target: { value: '1' } });
    });

    expect(input?.value).toEqual('1_/__/____');
  });

  it('render DatePickerControl UI > user event > handle blur', () => {
    const blurFn = jest.fn();

    const wrapper = renderDatePickerControl({
      ...mockDatePickerProp,
      onBlur: blurFn,
      placeholder: undefined
    });

    const input: HTMLInputElement | null = wrapper.baseElement.querySelector(
      '.text-field-container > input'
    );

    // blur on input > call onblur
    fireEvent.blur(input!);
    expect(blurFn).toHaveBeenCalled();
  });

  it('render DatePickerControl UI > user event > handle popup shown > not focus input', () => {
    jest.useFakeTimers();
    const blurFn = jest.fn();

    const wrapper = renderDatePickerControl({
      ...mockDatePickerProp,
      onBlur: blurFn,
      placeholder: undefined
    });

    const input: HTMLInputElement | null = wrapper.baseElement.querySelector(
      '.text-field-container > input'
    );
    // mouse down & focus on input to open calendar popup
    fireEvent.mouseDown(input!);
    fireEvent.focus(input!);

    act(() => {
      jest.runAllTimers();
    });

    // Mock click outside popup
    const dpControl = wrapper.baseElement.querySelector(
      '.date-picker-control'
    ) as HTMLDivElement;
    fireEvent.mouseDown(dpControl);

    // blur on input > call onblur
    fireEvent.blur(input!);

    expect(blurFn).toHaveBeenCalled();
  });

  it('render DatePickerControl UI > user event > handle popup shown > focus on input', () => {
    jest.useFakeTimers();
    const blurFn = jest.fn();

    const wrapper = renderDatePickerControl({
      ...mockDatePickerProp,
      onBlur: blurFn,
      placeholder: undefined
    });

    const input: HTMLInputElement | null = wrapper.baseElement.querySelector(
      '.text-field-container > input'
    );
    // mouse down & focus on input to open calendar popup
    fireEvent.mouseDown(input!);
    fireEvent.focus(input!);

    act(() => {
      jest.runAllTimers();
    });

    const popupElement = wrapper.baseElement.querySelector(
      '.dls-popup'
    ) as HTMLDivElement;
    // Mock click on popup calendar
    fireEvent.mouseDown(popupElement);

    // keep focus after pick date on calendar popup
    fireEvent.blur(input!);

    expect(blurFn).not.toHaveBeenCalled();
  });
});
