import React from 'react';
import '@testing-library/jest-dom';

// components
import Item from './Item';
import { render } from '@testing-library/react';
import { queryByClass } from '../../../test-utils/queryHelpers';

jest.mock('../../../hooks/useTranslation', () => () => ({
  t: (text: string) => text
}));

describe('Render', () => {
  it('should render', () => {
    const { baseElement } = render(<Item name="name" />);

    expect(
      queryByClass(baseElement as HTMLDivElement, 'ds-attr-search-item')
    ).toBeInTheDocument();
  });
});
