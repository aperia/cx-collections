import React from 'react';
import '@testing-library/jest-dom';

// components
import Footer from './Footer';
import { render, screen } from '@testing-library/react';

jest.mock('../../../hooks/useTranslation', () => () => ({
  t: (text: string) => text
}));

describe('Render', () => {
  it('should render', () => {
    const onClickWildCard = jest.fn();

    render(<Footer onClickWildCard={onClickWildCard} />);

    expect(screen.getByText('txt_navigate')).toBeInTheDocument();
    expect(screen.getByText('txt_tab')).toBeInTheDocument();
    expect(screen.getByText('txt_select_or_finish_adding')).toBeInTheDocument();
    expect(screen.getByText('txt_enter')).toBeInTheDocument();
    expect(screen.getByText('txt_search')).toBeInTheDocument();
    expect(screen.getByText('txt_wildcard')).toBeInTheDocument();
    expect(screen.getByText('txt_searching_is_supported')).toBeInTheDocument();
  });
});
