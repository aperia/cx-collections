import React from 'react';

// hooks
import { useTranslation } from '../../../hooks';
import { genAmtId } from '../../../utils';

export interface FooterProps extends DLSId {
  onClickWildCard?: (
    event: React.MouseEvent<HTMLSpanElement, MouseEvent>
  ) => void;
}

const Footer: React.FC<FooterProps> = ({ onClickWildCard, dataTestId }) => {
  const { t } = useTranslation();

  return (
    <div
      className="ds-attr-search-suggestion"
      data-testid={genAmtId(dataTestId, 'footer', 'AttributeSearch.Footer')}
    >
      <div className="suggestion-left">
        <span className="suggestion-icon icon icon-chevron-up" />
        <span className="suggestion-icon icon icon-chevron-down" />
        <span className="suggestion-text">{t('txt_navigate', 'Navigate')}</span>
        <div className="suggestion-button tab">{t('txt_tab', 'Tab')}</div>
        <span className="suggestion-text">
          {t('txt_select_or_finish_adding', 'Select or finish adding')}
        </span>
        <div className="suggestion-button enter">{t('txt_enter', 'Enter')}</div>
        <span className="suggestion-text">{t('txt_search', 'Search')}</span>
      </div>
      {onClickWildCard && (
        <div
          className="suggestion-right"
          data-testid={genAmtId(
            dataTestId,
            'wildcard',
            'AttributeSearch.Footer'
          )}
        >
          <span className="suggestion-text">
            <span className="dotted">{t('txt_wildcard', 'Wildcard')}</span>{' '}
            {t('txt_searching_is_supported', 'searching is supported.')}
          </span>
        </div>
      )}
    </div>
  );
};

export default Footer;
