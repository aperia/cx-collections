import React from 'react';
import { Trans, useTranslation } from 'react-i18next';

export interface ITransDLS {
  keyTranslation: string;
  count?: number;
}

const TransDLS: React.FC<ITransDLS> = ({ keyTranslation, count, ...props }) => {
  const { t } = useTranslation();
  return (
    <Trans t={t} i18nKey={keyTranslation} count={count}>
      {props.children}
    </Trans>
  );
};

export default TransDLS;
