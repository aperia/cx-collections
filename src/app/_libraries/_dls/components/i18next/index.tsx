import React, { useEffect, useRef } from 'react';
import i18next from 'i18next';
import {
  I18nextProvider as ReactI18nextProvider,
  initReactI18next
} from 'react-i18next';
import isEmpty from 'lodash.isempty';
import LanguageDetector from 'i18next-browser-languagedetector';
import HttpApi from 'i18next-http-backend';

export declare type Resource = {
  lang: string;
  data: {
    [key: string]:
      | string
      | {
          [key: string]: any;
        };
  };
};
export interface II18nextProvider {
  resource?: Resource;
  initReactI18nextProps?: boolean;
}

const I18nextProvider: React.FC<II18nextProvider> = ({
  initReactI18nextProps = false,
  resource,
  ...props
}) => {
  //create instance to use i18next if you don't wrapper children by I18nextProviderDLS
  initReactI18nextProps && i18next.use(initReactI18next);
  const ref = useRef<any>(null);

  //init
  if (!ref.current) {
    // i18next.init({
    //   interpolation: {
    //     escapeValue: false
    //   },
    //   react: {
    //     transSupportBasicHtmlNodes: true,
    //     transKeepBasicHtmlNodesFor: ['br', 'strong', 'i', 'b']
    //   },
    //   resources: {},
    //   ns: 'translation',
    //   lng: 'en',
    //   defaultNS: 'translation',
    //   fallbackLng: 'en'
    // });
    i18next
      .use(initReactI18next)
      .use(LanguageDetector)
      .use(HttpApi)
      .init({
        react: {
          useSuspense: false
        },
        supportedLngs: ['en', 'fr', 'es'],
        fallbackLng: 'en',
        detection: {
          order: ['cookie', 'localStorage', 'path', 'htmlTag', 'subdomain'],
          caches: ['cookie']
        },
        backend: {
          loadPath: '/i18n/{{lng}}/translation.json'
        }
      });
    ref.current = 'rendered';
  }

  useEffect(() => {
    if (resource && !isEmpty(resource.data)) {
      const { lang, data } = resource;
      i18next.changeLanguage(lang);
      //check if i18next.init exist lang
      if (i18next.hasResourceBundle(lang, 'translation')) {
        return;
      }

      //add and change language
      i18next
        .addResourceBundle(lang, 'translation', data, true, true)
        .reloadResources();
    }
  }, [resource]);

  return (
    <ReactI18nextProvider i18n={i18next}>{props.children}</ReactI18nextProvider>
  );
};

export default I18nextProvider;
