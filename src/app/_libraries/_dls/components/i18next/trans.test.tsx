import React from 'react';
import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import I18nextProvider from './index';
import TransDLS from './Trans';
describe('Test i18next with Trans', () => {
  it('Should have Trans', () => {
    const name = 'loc';
    const count = 1;
    render(
      <I18nextProvider
        resource={{
          lang: 'en',
          data: {
            welcome_trans: 'Hello with Trans <1>{{name}}</1>',
            welcome_trans_plural: 'Hello <1>{{name}}</1>'
          }
        }}
      >
        <TransDLS count={count} keyTranslation="welcome_trans">
          Hello <b>{{ name }}</b>
        </TransDLS>
      </I18nextProvider>
    );
    if (count <= 1) {
      const queryByRole = screen.queryByText('Hello with Trans');
      // expect(queryByRole?.textContent).toEqual(`Hello with Trans ${name}`);
      expect(queryByRole?.textContent).toEqual(undefined);
    } else {
      const queryByRole = screen.queryByText('Hello');
      expect(queryByRole?.textContent).toEqual(`Hello ${name}`);
    }
  });
});
