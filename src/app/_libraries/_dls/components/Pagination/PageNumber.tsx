import React, { useMemo } from 'react';

// components/types
import DropdownList from '../DropdownList';
import { PageNumberProps } from './type';
import { DropdownBaseChangeEvent } from '../DropdownBase';

// utils
import { genAmtId } from '../../utils';
import { useTranslation } from '../../hooks';

const PageNumber: React.FC<PageNumberProps> = ({
  pageSizes,
  pageSizeValue,
  modeDisplay,
  onChangePageSize,

  dataTestId
}) => {
  const { t } = useTranslation();
  const dropdownListItem = useMemo(() => {
    if (!pageSizes) return [];
    return pageSizes.map(item => (
      <DropdownList.Item key={item} label={item.toString()} value={item} />
    ));
  }, [pageSizes]);

  const handleOnChangePageSize = (event: DropdownBaseChangeEvent) => {
    onChangePageSize(event.target.value);
  };

  const containerId = genAmtId(dataTestId, 'dls-page-number', 'PageNumber');

  return (
    <div className="pagination-number pagination-row-center">
      <div
        className="pagination-text"
        data-testid={genAmtId(containerId, 'text', 'PageNumber')}
      >
        {t(modeDisplay.textPerPage)}
      </div>
      <div className="pagination-page-size">
        <DropdownList
          textField=""
          size="small"
          value={pageSizeValue}
          onChange={handleOnChangePageSize}
          dataTestId={genAmtId(containerId, 'page-size', 'PageNumber')}
        >
          {dropdownListItem}
        </DropdownList>
      </div>
    </div>
  );
};

export default PageNumber;
