import React from 'react';
import {
  fireEvent,
  getByText,
  queryByText,
  render,
  screen
} from '@testing-library/react';
import '@testing-library/jest-dom';
import Pagination from '.';
import { queryAllByClass, queryByClass } from '../../test-utils/queryHelpers';
import { act } from 'react-dom/test-utils';
import {
  mockClientWidth,
  mockComputedPadding,
  mockPropertyValue
} from '../../test-utils/mocks/defineProperty';
import * as getElementAttribute from '../../utils/getElementAttribute';

jest.mock('resize-observer-polyfill', () =>
  jest.requireActual('../../test-utils/mocks/MockResizeObserver')
);
jest.mock('../../utils/canvasTextWidth.ts');

let spyGetElementAttribute: jest.SpyInstance;

beforeEach(() => {
  spyGetElementAttribute = jest
    .spyOn(getElementAttribute, 'getAvailableWidth')
    .mockImplementation(() => 1000);
});

afterEach(() => {
  spyGetElementAttribute?.mockReset();
  spyGetElementAttribute?.mockRestore();
});

describe('Test Pagination', () => {
  it('Should have handleOnChangePageSize uncontrol mode and over pageNumber', () => {
    jest.useFakeTimers();
    const onChange = jest.fn().mockReturnValue(1);
    const { container, baseElement } = render(
      <Pagination
        onChangePageSize={onChange}
        ref={{ current: null }}
        totalItem={1}
        pageNumber={7}
        pageSize={[5, 10, 20]}
      />
    );
    const dropdownPageSize = queryByClass(container, /text-field-container/);
    act(() => {
      dropdownPageSize?.focus();
      dropdownPageSize?.click();
      jest.runAllTimers();
    });
    const queryContainerPageSize = queryByClass(
      baseElement as HTMLElement,
      /dls-dropdown-base-scroll-container/
    );
    const itemPageSize = queryAllByClass(
      queryContainerPageSize as HTMLElement,
      /item/
    );
    fireEvent.click(itemPageSize[0]);
    expect(onChange).toBeCalledWith({ page: 1, size: 5 });
  });

  it('Should have handleOnChangePageSize control mode', () => {
    jest.useFakeTimers();
    const { container, baseElement } = render(
      <Pagination
        ref={{ current: null }}
        totalItem={20}
        pageSizeValue={10}
        pageNumber={1}
        pageSize={[5, 10, 20]}
      />
    );
    const dropdownPageSize = queryByClass(container, /text-field-container/);
    act(() => {
      dropdownPageSize?.focus();
      dropdownPageSize?.click();
      jest.runAllTimers();
    });
    const queryContainerPageSize = queryByClass(
      baseElement as HTMLElement,
      /dls-dropdown-base-scroll-container/
    );
    const itemPageSize = getByText(queryContainerPageSize as HTMLElement, '5');
    itemPageSize?.click();
    const queryNumberPage = queryAllByClass(container, /page-link/);
    expect(queryNumberPage.length).toEqual(4);
  });

  it('Should have handleOnChangePageSize uncontrol mode and without pageNumber', () => {
    jest.useFakeTimers();
    const onChange = jest.fn().mockReturnValue(2);
    const { container, baseElement } = render(
      <Pagination
        onChangePageSize={onChange}
        ref={{ current: null }}
        totalItem={20}
        pageSize={[5, 10, 20]}
      />
    );
    const dropdownPageSize = queryByClass(container, /text-field-container/);
    act(() => {
      dropdownPageSize?.focus();
      dropdownPageSize?.click();
      jest.runAllTimers();
    });
    const queryContainerPageSize = queryByClass(
      baseElement as HTMLElement,
      /dls-dropdown-base-scroll-container/
    );
    const itemPageSize = getByText(queryContainerPageSize as HTMLElement, '5');
    itemPageSize?.click();
    const queryNumberPage = queryAllByClass(container, /page-link/);
    const dataPageNumber = queryByClass(container, /page-item active/);
    expect(dataPageNumber?.textContent).toContain('1');
    expect(onChange).toHaveBeenCalled();
    expect(queryNumberPage.length).toEqual(6);
  });

  it('Should have handleOnChangePageSize uncontrol mode with pageNumber', () => {
    jest.useFakeTimers();
    const onChange = jest.fn().mockReturnValue(2);
    const { container, baseElement } = render(
      <Pagination
        onChangePageSize={onChange}
        ref={{ current: null }}
        totalItem={20}
        pageNumber={1}
        pageSize={[5, 10, 20]}
      />
    );
    const dropdownPageSize = queryByClass(container, /text-field-container/);
    act(() => {
      dropdownPageSize?.focus();
      dropdownPageSize?.click();
      jest.runAllTimers();
    });
    const queryContainerPageSize = queryByClass(
      baseElement as HTMLElement,
      /dls-dropdown-base-scroll-container/
    );
    const itemPageSize = getByText(queryContainerPageSize as HTMLElement, '5');
    itemPageSize?.click();
    const queryNumberPage = queryAllByClass(container, /page-link/);
    expect(onChange).toHaveBeenCalled();
    expect(queryNumberPage.length).toEqual(6);
  });

  it('Should have onChangePage control mode', () => {
    const onChangePage = jest.fn();
    const { container } = render(
      <Pagination
        ref={{ current: null }}
        onChangePage={onChangePage}
        totalItem={20}
        pageNumber={1}
        pageSize={[5, 10, 20]}
      />
    );
    const queryPageItem = screen.queryByText('2');
    const dataPageNumber = queryByClass(container, /page-item active/);
    queryPageItem?.click();
    expect(onChangePage).toHaveBeenCalled();
    expect(dataPageNumber?.textContent).toContain('1');
  });
  it('Should have onChangePage uncontrol mode', () => {
    const { container } = render(
      <Pagination
        ref={{ current: null }}
        totalItem={20}
        pageSize={[5, 10, 20]}
      />
    );
    const queryPageItem = screen.queryByText('2');
    queryPageItem?.click();
    const dataPageNumber = queryByClass(container, /page-item active/);
    expect(dataPageNumber?.textContent).toContain('2');
  });

  it('Should have pageSizeValue,pageNumberProp,Text-Info props', () => {
    const totalItem = 10;
    const pageSizeValue = 5;
    const dataToContain =
      pageSizeValue > totalItem
        ? '1'
        : Math.floor(totalItem / pageSizeValue).toString();
    const { container } = render(
      <Pagination
        textInfo="Text-Info"
        ref={{ current: null }}
        pageSizeValue={5}
        totalItem={10}
        pageNumber={2}
        pageSize={[5, 10, 20]}
      />
    );
    const dataPageSize = queryByClass(container, /input/);
    const dataPageNumber = queryByClass(container, /page-item active/);
    const textInfo = queryByClass(container, /pagination-info pagination-text/);
    expect(dataPageSize?.textContent).toEqual(pageSizeValue.toString());
    expect(dataPageNumber?.textContent).toContain(dataToContain);
    expect(textInfo?.textContent).toEqual('Text-Info');
  });

  it('Should have pageSizeValue,pageNumberProp,textInfo Default', () => {
    const { container } = render(
      <Pagination
        ref={{ current: null }}
        totalItem={10}
        pageSize={[5, 10, 20]}
      />
    );
    const dataPageSize = queryByClass(container, /input/);
    const dataPageNumber = queryByClass(container, /page-item active/);
    const textInfo = queryByClass(container, /pagination-info pagination-text/);
    expect(dataPageSize?.textContent).toEqual('10');
    expect(dataPageNumber?.textContent).toContain('1');
    expect(textInfo).toBeInTheDocument();
  });

  it("When parent's width is 0", () => {
    spyGetElementAttribute = jest
      .spyOn(getElementAttribute, 'getAvailableWidth')
      .mockImplementation(() => 0);

    mockComputedPadding(100, 150, 150, 100);
    mockPropertyValue(100);
    mockClientWidth(0);

    const { container } = render(
      <Pagination
        ref={{ current: null }}
        totalItem={20}
        pageNumber={1}
        pageSize={[5, 10, 20]}
        numberDisplay={1}
      />
    );
    const pageLink = queryAllByClass(container, /page-link/);
    const textCompact = queryByText(container, /results/);
    const textShowing = queryByText(container, /Showing/);
    expect(textCompact).not.toBeInTheDocument();
    expect(textShowing).not.toBeInTheDocument();
    expect(pageLink.length).toEqual(4);
  });

  it('Should have mobile', () => {
    spyGetElementAttribute = jest
      .spyOn(getElementAttribute, 'getAvailableWidth')
      .mockImplementation(() => 100);

    mockComputedPadding(100, 150, 150, 100);
    mockPropertyValue(100);
    mockClientWidth(300);

    const { container } = render(
      <Pagination
        ref={{ current: null }}
        totalItem={20}
        pageNumber={1}
        pageSize={[5, 10, 20]}
        numberDisplay={1}
      />
    );
    const pageLink = queryAllByClass(container, /page-link/);
    const textCompact = queryByText(container, /results/);
    const textShowing = queryByText(container, /Showing/);
    expect(textCompact).not.toBeInTheDocument();
    expect(textShowing).not.toBeInTheDocument();
    expect(pageLink.length).toEqual(4);
  });

  it('Should have compact tablet', () => {
    spyGetElementAttribute = jest
      .spyOn(getElementAttribute, 'getAvailableWidth')
      .mockImplementation(() => 790);

    mockComputedPadding(100, 150, 150, 100);
    mockPropertyValue(100);
    mockClientWidth(790);
    const { container } = render(
      <Pagination
        ref={{ current: null }}
        totalItem={20}
        pageNumber={7}
        pageSize={[5, 10, 20]}
      />
    );
    const textCompact = queryByText(container, /txt_total_result/);
    expect(textCompact).toBeInTheDocument();
  });
});
