import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import PaginationItem from '../Pagination/PaginationItem';
import { queryByClass } from '../../test-utils';

describe('Test PaginationItem', () => {
  it('Should have children', () => {
    const onClick = jest.fn();
    render(<PaginationItem onClick={onClick}>PaginationItem</PaginationItem>);
    const queryByText = screen.queryByText('PaginationItem');
    queryByText?.click();
    expect(onClick).toHaveBeenCalled();
    expect(queryByText).toBeInTheDocument();
  });

  it('Should have disable props', () => {
    const { container } = render(
      <PaginationItem disabled={true}>PaginationItem</PaginationItem>
    );

    const queryClass = queryByClass(container, /disabled/);
    expect(queryClass).toBeInTheDocument();
  });
});
