import React, { memo } from 'react';

// component/type
import { PaginationItemProps } from './type';

// utils
import classnames from 'classnames';
import { genAmtId } from '../../utils';

const PaginationItem: React.FC<PaginationItemProps> = ({
  onClick,
  disabled = false,
  dataTestId,
  children
}) => {
  return (
    <li className={classnames('page-item', { disabled: disabled })}>
      <a
        onClick={onClick}
	className="page-link"
	data-testid={genAmtId(dataTestId, 'custom', 'PaginationItemCustom')}
      >
        {children}
      </a>
    </li>
  );
};
export default memo(PaginationItem);
