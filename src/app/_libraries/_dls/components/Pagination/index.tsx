import React, {
  useState,
  useEffect,
  useRef,
  useImperativeHandle,
  forwardRef,
  useLayoutEffect
} from 'react';

// components/types
import Paper from './Paper';
import PageNumber from './PageNumber';
import InfoPagination from './InfoPagination';
import { PaginationWrapperProps } from './type';
import {
  DEFAULT_PAGE_SIZE,
  DEFAULT_PAGE_NUMBER,
  CONTAINER_WITH_LT_768,
  CONTAINER_WITH_GTE_992,
  CONTAINER_WITH_GTE_768
} from './constants';

// utils
import classnames from 'classnames';
import ResizeObserver from 'resize-observer-polyfill';
import { genAmtId, getAvailableWidth } from '../../utils';
import { useTranslation } from '../../hooks';

const PaginationWrapper: React.RefForwardingComponent<
  HTMLDivElement,
  PaginationWrapperProps
> = (
  {
    className,
    pageNumber: pageNumberProp,
    totalItem,
    pageAble = true,
    pageSize,
    pageSizeText,
    pageSizeValue: pageSizeValueProp,
    textInfo: textInfoProp,
    onChangePage,
    onChangePageSize,

    id,
    dataTestId
  },
  ref
) => {
  const { t } = useTranslation();
  const [width, setWidth] = useState<number>(0);

  const [pageNumber, setPageNumber] = useState(
    pageNumberProp || DEFAULT_PAGE_NUMBER
  );
  const [pageSizeValue, setPageSizeValue] = useState(
    pageSizeValueProp || DEFAULT_PAGE_SIZE
  );

  const [modeDisplay, setModeDisplay] =
    useState<typeof CONTAINER_WITH_GTE_992>();

  const containerRef = useRef<HTMLDivElement | null>(null);
  const totalPage = Math.ceil(totalItem / pageSizeValue);

  useImperativeHandle(ref, () => containerRef.current!);

  // set props
  useEffect(() => {
    if (pageNumberProp) setPageNumber(pageNumberProp);
  }, [pageNumberProp]);

  useEffect(() => {
    if (pageSizeValueProp) setPageSizeValue(pageSizeValueProp);
  }, [pageSizeValueProp]);

  const handleOnChangePage = (page: number) => {
    if (onChangePage) {
      onChangePage(page);
    }

    // controlled mode
    if (pageNumberProp) return;

    // Un-controlled mode
    setPageNumber(page);
  };

  const handleOnChangePageSize = (size: number) => {
    if (onChangePageSize) {
      const newTotalPage = Math.ceil(totalItem / pageNumber);
      onChangePageSize({
        size,
        page: newTotalPage >= pageNumber ? pageNumber : 1
      });
    }

    // controlled mode
    if (pageSizeValueProp) return;

    // Un-controlled mode
    setPageSizeValue(size);

    if (pageNumberProp) return;
    setPageNumber(1);
  };

  useLayoutEffect(() => {
    const { parentElement } = containerRef.current as HTMLDivElement;

    // calculate max width after parent's size was changed
    const resizeObserver = new ResizeObserver(() => {
      const parentWidth = getAvailableWidth(parentElement);
      if (!parentWidth) return;

      setWidth(parentWidth);
    });

    // init
    resizeObserver.observe(parentElement!);

    return () => resizeObserver.disconnect();
  }, []);

  useEffect(() => {
    if (width >= CONTAINER_WITH_GTE_992.breakpoint) {
      return setModeDisplay(CONTAINER_WITH_GTE_992);
    }
    if (width >= CONTAINER_WITH_GTE_768.breakpoint) {
      return setModeDisplay(CONTAINER_WITH_GTE_768);
    }
    return setModeDisplay(CONTAINER_WITH_LT_768);
  }, [width]);

  const textInfo = () => {
    const showFrom = pageSizeValue * (pageNumber - 1) + 1;
    const showTo =
      pageSizeValue * pageNumber > totalItem
        ? totalItem
        : pageSizeValue * pageNumber;

    if (textInfoProp) return textInfoProp;

    const nextTextInfo = t(modeDisplay?.textInfo)
      .replace('{showFrom}', showFrom + '')
      .replace('{showTo}', showTo + '')
      .replace('{totalItem}', totalItem + '')
      .replace('{plural}', totalItem > 1 ? 's' : '');

    return nextTextInfo;
  };

  const containerId = genAmtId(dataTestId, 'dls-pagination', 'Pagination');

  return (
    <div
      ref={containerRef}
      className={`pagination-wrapper pagination-row-center ${classnames(
        className,
        {
          'justify-content-between': !modeDisplay?.onlyNumberDisplay,
          'justify-content-center': modeDisplay?.onlyNumberDisplay
        }
      )}`}
      id={id}
      data-testid={containerId}
    >
      {modeDisplay && (
        <>
          {!modeDisplay.onlyNumberDisplay ? (
            <InfoPagination textInfo={textInfo()!} dataTestId={containerId} />
          ) : null}
          <Paper
            modeDisplay={modeDisplay}
            pageNumber={pageNumber}
            totalPage={totalPage}
            onClick={handleOnChangePage}
            dataTestId={containerId}
          />
          {pageAble && !modeDisplay.onlyNumberDisplay ? (
            <PageNumber
              modeDisplay={modeDisplay}
              pageSizes={pageSize}
              pageSizeValue={pageSizeValue}
              dataTestId={containerId}
              onChangePageSize={handleOnChangePageSize}
            />
          ) : null}
        </>
      )}
    </div>
  );
};

export default forwardRef<HTMLDivElement, PaginationWrapperProps>(
  PaginationWrapper
);
export * from './type';
