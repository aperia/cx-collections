import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import InfoPagination from '../Pagination/InfoPagination';

describe('Test InfoPagination', () => {
  it('Should have children textInfo', () => {
    render(<InfoPagination textInfo={'InfoPagination'} />);
    const queryByText = screen.queryByText('InfoPagination');
    expect(queryByText).toBeInTheDocument();
  });
});
