import React, { useImperativeHandle, useRef } from 'react';

// utils
import classNames from 'classnames';
import { genAmtId } from '../../utils';

// components
import Icon, { IconNames } from '../../components/Icon';
import { Alert, AlertProps } from 'react-bootstrap';

export interface InlineMessageProps extends AlertProps, DLSId {
  withIcon?: boolean;
  children?: React.ReactNode;
}

const iconNamesMap: Record<string, IconNames> = {
  info: 'megaphone',
  warning: 'warning',
  danger: 'error'
};

const InlineMessage: React.RefForwardingComponent<
  HTMLDivElement,
  InlineMessageProps
> = (
  { variant, children, withIcon, className, id, dataTestId, ...props },
  ref
) => {
  const alertRef = useRef<HTMLDivElement | null>(null);

  useImperativeHandle(ref, () => alertRef.current!);

  return (
    <Alert
      ref={alertRef}
      variant={variant}
      className={classNames('d-inline-block', className)}
      id={id}
      data-testid={genAmtId(dataTestId, 'dls-inline-message', 'InlineMessage')}
      {...props}
    >
      <div className="d-flex flex-row">
        {withIcon && variant && <Icon name={iconNamesMap[variant]} />}
        {children}
      </div>
    </Alert>
  );
};

export default React.forwardRef<HTMLDivElement, InlineMessageProps>(
  InlineMessage
);
