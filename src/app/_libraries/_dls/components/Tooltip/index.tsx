import React, {
  useRef,
  useImperativeHandle,
  useEffect,
  useState,
  useCallback,
  useContext
} from 'react';

// utils
import classnames from 'classnames';
import get from 'lodash.get';
import isBoolean from 'lodash.isboolean';
import isFunction from 'lodash.isfunction';
import { className, genAmtId } from '../../utils';

// components/types
import Popper, { PopperProps, PopperRef } from '../Popper';

export interface TooltipProps extends PopperProps {
  tooltipInnerClassName?: string;
  variant?: 'primary' | 'error' | 'default';
  onVisibilityChange?: (isOpened: boolean) => void;

  displayOnClick?: boolean;
  onClickTrigger?: () => void;
}

export interface TooltipContextType {
  value?: any;
  setValue?: (nextValue: any) => void;
}

const TooltipContext = React.createContext<TooltipContextType>({});
const useTooltipContext = () => useContext(TooltipContext);

const Tooltip: React.RefForwardingComponent<PopperRef, TooltipProps> = (
  {
    element,
    containerClassName,
    arrowClassName,
    tooltipInnerClassName,
    variant = 'default',
    opened: openedProp,
    onVisibilityChange,
    displayOnClick = false,
    onClickTrigger,

    dataTestId,
    ...props
  },
  ref
) => {
  // refs
  const popperRef = useRef<PopperRef | null>(null);
  const keepRef = useRef<Record<string, any>>({});
  useImperativeHandle(ref, () => popperRef.current!);

  // states
  const [opened, setOpened] = useState(openedProp);
  const [tooltipContextValue, setTooltipContextValue] =
    useState<TooltipContextType>({
      value: '',
      setValue: (nextValue) => {
        setTooltipContextValue((state) => ({ ...state, value: nextValue }));
      }
    });

  // handle controlled opened
  const handleControlledOpened = useCallback(
    (isOpened: boolean) => {
      // controlled
      if (isBoolean(openedProp)) {
        return isFunction(onVisibilityChange) && onVisibilityChange(isOpened);
      }

      // uncontrolled
      setOpened(isOpened);
      isFunction(onVisibilityChange) && onVisibilityChange(isOpened);
    },
    [openedProp, onVisibilityChange]
  );

  // keep references
  keepRef.current.onClickTrigger = onClickTrigger;

  // set props
  useEffect(() => setOpened(openedProp), [openedProp]);

  // handle hover to toggle open tooltip
  useEffect(() => {
    const triggerElement = get(
      popperRef.current,
      'triggerElement'
    ) as HTMLSpanElement;

    // if tooltip is opened, on next mousedown we need hidden tooltip
    const handleMouseDown = () => {
      const nextOpened = displayOnClick;
      handleControlledOpened(nextOpened);
    };

    // current, we suporting onClickTrigger for dateTooltip of DatePicker
    const handleTriggerClick = () => {
      isFunction(keepRef.current.onClickTrigger) &&
        keepRef.current.onClickTrigger();
    };

    // handle update popper placement after click
    const handleUpdateTooltipInstance = () => {
      if (!displayOnClick) return;
      process.nextTick(() => (popperRef.current as any)?.update?.());
    };

    // show tooltip on hover
    const handleMouseEnter = () => {
      handleControlledOpened(true);
      triggerElement.addEventListener('mousedown', handleMouseDown);
      triggerElement.addEventListener('click', handleTriggerClick);
      triggerElement.addEventListener('click', handleUpdateTooltipInstance);
    };

    // hidden tooltip on un-hover
    const handleMouseLeave = () => {
      handleControlledOpened(false);
      triggerElement.removeEventListener('mousedown', handleMouseDown);
      triggerElement.removeEventListener('click', handleUpdateTooltipInstance);
    };

    triggerElement.addEventListener('mouseenter', handleMouseEnter);
    triggerElement.addEventListener('mouseleave', handleMouseLeave);
    return () => {
      triggerElement.removeEventListener('mouseenter', handleMouseEnter);
      triggerElement.removeEventListener('mouseleave', handleMouseLeave);
      triggerElement.removeEventListener('mousedown', handleMouseDown);
      triggerElement.removeEventListener('click', handleTriggerClick);
      triggerElement.removeEventListener('click', handleUpdateTooltipInstance);
    };
  }, [displayOnClick, handleControlledOpened]);

  return (
    <TooltipContext.Provider value={tooltipContextValue}>
      <Popper
        ref={popperRef}
        opened={opened}
        containerClassName={classnames(
          className.tooltip.CONTAINER,
          containerClassName,
          variant === 'default' ? '' : variant,
          tooltipContextValue.value
        )}
        arrowClassName={classnames(className.tooltip.ARROW, arrowClassName)}
        element={
          <div
            className={classnames(
              className.tooltip.INNER,
              tooltipInnerClassName
            )}
            data-testid={genAmtId(dataTestId, 'dls-tooltip', 'Tooltip')}
          >
            {element}
          </div>
        }
        {...props}
      />
    </TooltipContext.Provider>
  );
};

export { useTooltipContext };
export default React.forwardRef<PopperRef, TooltipProps>(Tooltip);
