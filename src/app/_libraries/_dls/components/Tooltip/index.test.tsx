import React from 'react';

import { render } from '@testing-library/react';
import '@testing-library/jest-dom';

// utils
import { queryByClass } from '../../test-utils/queryHelpers';

// components
import Tooltip from '.';
import { Button, Icon, PopperRef } from '..';
import userEvent from '@testing-library/user-event';

const getTooltipContainerElement = (baseElement: Element) => {
  return queryByClass(
    baseElement as HTMLSpanElement,
    /dls-tooltip-container/
  ) as HTMLDivElement;
};
const getTriggerElement = (baseElement: Element) => {
  return queryByClass(
    baseElement as HTMLElement,
    /dls-popper-trigger/
  ) as HTMLSpanElement;
};

describe('Test render', () => {
  it('useImperativeHandle should run', () => {
    const tooltipRef = React.createRef<PopperRef>();

    render(
      <Tooltip ref={tooltipRef}>
        <Button>
          <Icon name="close" />
        </Button>
      </Tooltip>
    );
  });

  it('should render and existed in document when user hover on trigger', () => {
    const { baseElement } = render(
      <Tooltip>
        <Button>
          <Icon name="close" />
        </Button>
      </Tooltip>
    );

    const triggerElement = getTriggerElement(baseElement);

    userEvent.hover(triggerElement);
    const containerElement = getTooltipContainerElement(baseElement);
    expect(containerElement).toBeInTheDocument();

    userEvent.unhover(triggerElement);
  });

  it('should unmount from document when user leave on trigger', () => {
    const { baseElement } = render(
      <Tooltip>
        <Button>
          <Icon name="close" />
        </Button>
      </Tooltip>
    );

    const triggerElement = getTriggerElement(baseElement);

    userEvent.hover(triggerElement);
    const tooltipContainerElement = getTooltipContainerElement(baseElement);
    expect(tooltipContainerElement).toBeInTheDocument();

    userEvent.unhover(triggerElement);
    expect(tooltipContainerElement).not.toBeInTheDocument();
  });

  it('should render with variants', () => {
    const { baseElement, rerender } = render(
      <Tooltip variant="primary">
        <Button>
          <Icon name="close" />
        </Button>
      </Tooltip>
    );

    const triggerElement = getTriggerElement(baseElement);

    userEvent.hover(triggerElement);
    const tooltipContainerElement = getTooltipContainerElement(baseElement);
    expect(tooltipContainerElement).toBeInTheDocument();

    // should be primary
    expect(tooltipContainerElement?.classList.contains('primary')).toBeTruthy();

    // should be error
    rerender(
      <Tooltip variant="error">
        <Button>
          <Icon name="close" />
        </Button>
      </Tooltip>
    );
    expect(tooltipContainerElement?.classList.contains('error')).toBeTruthy();

    // should be default
    rerender(
      <Tooltip>
        <Button>
          <Icon name="close" />
        </Button>
      </Tooltip>
    );
    expect(tooltipContainerElement?.classList.contains('primary')).toBeFalsy();
    expect(tooltipContainerElement?.classList.contains('error')).toBeFalsy();

    userEvent.unhover(triggerElement);
  });
});

describe('Test flow controlled/uncontrolled', () => {
  describe('controlled', () => {
    it('opened is false -> user hover trigger -> onVisibilityChange get true', () => {
      const onVisibilityChange = jest.fn();

      const { baseElement } = render(
        <Tooltip opened={false} onVisibilityChange={onVisibilityChange}>
          <Button>
            <Icon name="close" />
          </Button>
        </Tooltip>
      );

      const triggerElement = getTriggerElement(baseElement);

      userEvent.hover(triggerElement);
      expect(onVisibilityChange).toBeCalledWith(true);

      userEvent.unhover(triggerElement);
    });

    it('opened is true -> user click trigger -> onVisibilityChange get false', () => {
      const onVisibilityChange = jest.fn();

      const { baseElement, rerender } = render(
        <Tooltip opened onVisibilityChange={onVisibilityChange}>
          <Button>
            <Icon name="close" />
          </Button>
        </Tooltip>
      );

      const triggerElement = getTriggerElement(baseElement);

      userEvent.click(triggerElement);
      expect(onVisibilityChange).toBeCalledWith(false);

      rerender(
        <Tooltip onVisibilityChange={onVisibilityChange}>
          <Button>
            <Icon name="close" />
          </Button>
        </Tooltip>
      );
    });

    it('opened is false -> user hover trigger -> onVisibilityChange get true -> user click trigger -> onVisibilityChange get false', () => {
      const onVisibilityChange = jest.fn();

      const { baseElement } = render(
        <Tooltip opened={false} onVisibilityChange={onVisibilityChange}>
          <Button>
            <Icon name="close" />
          </Button>
        </Tooltip>
      );

      const triggerElement = getTriggerElement(baseElement);

      userEvent.hover(triggerElement);
      expect(onVisibilityChange).toBeCalledWith(true);

      userEvent.click(triggerElement);
      expect(onVisibilityChange).toBeCalledWith(false);

      userEvent.unhover(triggerElement);
    });
  });

  describe('uncontrolled', () => {
    it('user hover trigger -> onVisibilityChange get true', () => {
      const onVisibilityChange = jest.fn();

      const { baseElement } = render(
        <Tooltip onVisibilityChange={onVisibilityChange}>
          <Button>
            <Icon name="close" />
          </Button>
        </Tooltip>
      );

      const triggerElement = getTriggerElement(baseElement);

      userEvent.hover(triggerElement);
      const tooltipContainerElement = getTooltipContainerElement(baseElement);
      expect(tooltipContainerElement).toBeInTheDocument();
      expect(onVisibilityChange).toBeCalledWith(true);

      userEvent.unhover(triggerElement);
    });

    it('user hover trigger -> onVisibilityChange get true -> user unhover -> onVisibility get false', () => {
      const onVisibilityChange = jest.fn();

      const { baseElement } = render(
        <Tooltip onVisibilityChange={onVisibilityChange}>
          <Button>
            <Icon name="close" />
          </Button>
        </Tooltip>
      );

      const triggerElement = getTriggerElement(baseElement);

      userEvent.hover(triggerElement);
      const tooltipContainerElement = getTooltipContainerElement(baseElement);
      expect(tooltipContainerElement).toBeInTheDocument();
      expect(onVisibilityChange).toBeCalledWith(true);

      userEvent.unhover(triggerElement);
      expect(onVisibilityChange).toBeCalledWith(false);
    });
  });

  it('should call handleUpdateTooltipInstance if user clicks on trigger', () => {
    jest.useFakeTimers();
    const { baseElement } = render(
      <Tooltip displayOnClick>
        <Button>
          <Icon name="close" />
        </Button>
      </Tooltip>
    );

    const triggerElement = getTriggerElement(baseElement);
    userEvent.click(triggerElement);
    jest.runAllTimers();

    const tooltip = getTooltipContainerElement(baseElement);
    expect(tooltip).toBeInTheDocument();
  });

  it('onClickTrigger support for DatePicker', () => {
    const onClickTrigger = jest.fn();

    const { baseElement } = render(
      <Tooltip onClickTrigger={onClickTrigger}>
        <Button>
          <Icon name="close" />
        </Button>
      </Tooltip>
    );

    const triggerElement = getTriggerElement(baseElement);

    userEvent.click(triggerElement);
    expect(onClickTrigger).toBeCalled();
  });
});
