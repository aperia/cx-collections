import React from 'react';
import '@testing-library/jest-dom';
import TimePicker from './index';
import {
  act,
  fireEvent,
  queryHelpers,
  render,
  screen
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Button from '../Button';

// author: SangLy

Element.prototype.scrollTo = jest.fn();

describe('Test TimePicker component', () => {
  it('render component', () => {
    jest.useFakeTimers();
    const wrapper = render(<TimePicker />);
    jest.runAllTimers();

    const element = wrapper.container.querySelector(
      'div[class="dls-time-picker"]'
    );
    expect(element).not.toBeNull();
  });

  it('render component with time column', () => {
    const onFocusSpy = jest.fn();

    jest.useFakeTimers();
    const wrapper = render(
      <div>
        <TimePicker
          label="Label"
          readOnly={false}
          disabled={false}
          onFocus={onFocusSpy}
        />
      </div>
    );

    const element = wrapper.container.querySelector('input');

    act(() => {
      fireEvent.focus(element!);
    });
    jest.runAllTimers();

    expect(screen.getByText('Hour')).toBeInTheDocument();
    expect(screen.getByText('Minute')).toBeInTheDocument();
    expect(screen.getByText('AM/PM')).toBeInTheDocument();
  });

  it('trigger handleKeyDown of input', () => {
    const onFocusSpy = jest.fn();

    jest.useFakeTimers();
    const wrapper = render(
      <div>
        <TimePicker
          label="Label"
          readOnly={false}
          disabled={false}
          onFocus={onFocusSpy}
        />
      </div>
    );

    const element = wrapper.container.querySelector('input');

    act(() => {
      fireEvent.keyDown(element!, { keyCode: 40 });
    });
    jest.runAllTimers();

    expect(element).toBeInTheDocument();
  });

  it('render component with error', () => {
    const onFocusSpy = jest.fn();

    jest.useFakeTimers();
    const wrapper = render(
      <div>
        <TimePicker
          label="Label"
          error={{
            message: 'Error Message',
            status: true
          }}
          required
          readOnly={false}
          disabled={false}
          onFocus={onFocusSpy}
        />
      </div>
    );

    const element = wrapper.container.querySelector('input');

    act(() => {
      fireEvent.focus(element!);
    });
    jest.runAllTimers();

    expect(screen.getByText('Error Message')).toBeInTheDocument();
  });

  it('calls onFocus props', () => {
    const onFocusSpy = jest.fn();

    jest.useFakeTimers();

    const wrapper = render(
      <div>
        <TimePicker
          label="Label"
          readOnly={false}
          disabled={false}
          onFocus={onFocusSpy}
        />
      </div>
    );

    const element = wrapper.container.querySelector('input');

    act(() => {
      fireEvent.focus(element!);
    });
    jest.runAllTimers();

    expect(onFocusSpy).toBeCalled();
  });

  it('on blur > value having hour', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    const mockValue = {
      hour: '08'
    };

    jest.useFakeTimers();

    const wrapper = render(
      <div>
        <TimePicker
          label="Label"
          readOnly={false}
          disabled={false}
          value={mockValue}
          onChange={onChangeSpy}
          onBlur={onBlurSpy}
        />
      </div>
    );

    const element = wrapper.container.querySelector('input');

    act(() => {
      fireEvent.focus(element!);
    });
    jest.runAllTimers();

    act(() => {
      fireEvent.blur(element!);
      jest.runAllTimers();
    });

    expect(onBlurSpy).toBeCalled();
    expect(onChangeSpy).toBeCalled();
  });

  it('on blur > value having meridiem', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    const mockValue = {
      meridiem: 'AM'
    };

    jest.useFakeTimers();

    const wrapper = render(
      <div>
        <TimePicker
          label="Label"
          readOnly={false}
          disabled={false}
          value={mockValue}
          onChange={onChangeSpy}
          onBlur={onBlurSpy}
        />
      </div>
    );

    const element = wrapper.container.querySelector('input');

    act(() => {
      fireEvent.focus(element!);
    });
    jest.runAllTimers();

    act(() => {
      fireEvent.blur(element!);
      jest.runAllTimers();
    });

    expect(onBlurSpy).toBeCalled();
    expect(onChangeSpy).toBeCalled();
  });

  it('on blur > readOnly = true', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();

    jest.useFakeTimers();

    const wrapper = render(
      <div>
        <TimePicker
          label="Label"
          readOnly={true}
          disabled={false}
          onChange={onChangeSpy}
          onBlur={onBlurSpy}
        />
      </div>
    );

    const element = wrapper.container.querySelector('input');

    act(() => {
      fireEvent.focus(element!);
    });
    jest.runAllTimers();

    act(() => {
      fireEvent.blur(element!);
      jest.runAllTimers();
    });

    expect(onBlurSpy).not.toBeCalled();
  });

  it('check handleOnChangeMaskedTextBox function', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();

    jest.useFakeTimers();

    const wrapper = render(
      <div>
        <TimePicker
          label="Label"
          readOnly={false}
          disabled={false}
          onChange={onChangeSpy}
          onBlur={onBlurSpy}
        />
      </div>
    );

    const element = wrapper.container.querySelector('input');

    act(() => {
      userEvent.type(element!, '01:00');
    });
    jest.runAllTimers();

    expect(onChangeSpy).toBeCalled();
  });

  it('check handleChangeTimeByScrollOrClick function', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    const mockValue = {
      hour: '08',
      minute: '00',
      meridiem: 'AM'
    };
    jest.useFakeTimers();

    const wrapper = render(
      <div>
        <TimePicker
          label="Label"
          readOnly={false}
          disabled={false}
          value={mockValue}
          onChange={onChangeSpy}
          onBlur={onBlurSpy}
        />
      </div>
    );

    const element = wrapper.container.querySelector('input');

    act(() => {
      fireEvent.focus(element!);
    });
    jest.runAllTimers();

    const hourElement = screen.getAllByText('01');
    act(() => {
      fireEvent.click(hourElement[0]);
      jest.runAllTimers();
    });

    expect(onChangeSpy).toBeCalled();
  });

  it('check handleChangeTimeByScrollOrClick function > not having value', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();

    jest.useFakeTimers();

    const wrapper = render(
      <div>
        <TimePicker
          label="Label"
          readOnly={false}
          disabled={false}
          onChange={onChangeSpy}
          onBlur={onBlurSpy}
        />
      </div>
    );

    const element = wrapper.container.querySelector('input');

    act(() => {
      fireEvent.focus(element!);
    });
    jest.runAllTimers();

    const hourElement = screen.getAllByText('01');
    act(() => {
      fireEvent.click(hourElement[0]);
      jest.runAllTimers();
    });

    expect(onChangeSpy).not.toBeCalled();
  });

  it('check handleChangeTimeByScrollOrClick function', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    const mockValue = {
      hour: '08',
      minute: '00',
      meridiem: 'AM'
    };
    jest.useFakeTimers();

    const wrapper = render(
      <div>
        <TimePicker
          label="Label"
          readOnly={false}
          disabled={false}
          value={mockValue}
          onChange={onChangeSpy}
          onBlur={onBlurSpy}
        />
      </div>
    );

    const element = wrapper.container.querySelector('input');

    act(() => {
      fireEvent.focus(element!);
    });
    jest.runAllTimers();

    const iconElement = wrapper.container.querySelector(
      'i[class="icon icon-time size-5x"]'
    ) as HTMLElement;
    iconElement?.focus();

    const hourElement = screen.getAllByText('01');
    act(() => {
      fireEvent.click(hourElement[0]);
      jest.runAllTimers();
    });

    expect(onChangeSpy).toBeCalled();
  });

  it('check handleChangeTimeByScrollOrClick function > coverage process.nextTick', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    const mockValue = {
      hour: '08',
      minute: '00',
      meridiem: 'AM'
    };
    jest.useFakeTimers();

    const wrapper = render(
      <div>
        <TimePicker
          label="Label"
          readOnly={false}
          disabled={false}
          value={mockValue}
          onChange={onChangeSpy}
          onBlur={onBlurSpy}
        />
      </div>
    );

    const element = wrapper.container.querySelector('input');

    act(() => {
      fireEvent.focus(element!);
    });
    jest.runAllTimers();

    element?.focus();

    const meridiemElement = screen.getByText('PM');
    act(() => {
      fireEvent.click(meridiemElement);
      jest.runAllTimers();
    });

    expect(onChangeSpy).toBeCalled();
  });

  it('check handleOnMouseDownIcon function', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();

    jest.useFakeTimers();

    const wrapper = render(
      <div>
        <TimePicker
          label="Label"
          readOnly={false}
          disabled={false}
          onChange={onChangeSpy}
          onBlur={onBlurSpy}
        />
      </div>
    );

    const iconElement = wrapper.container.querySelector(
      'i[class="icon icon-time size-5x"]'
    );
    act(() => {
      userEvent.click(iconElement!);
      jest.runAllTimers();
    });

    expect(onChangeSpy).not.toBeCalled();
  });

  it('check handleOnMouseDownIcon function > trigger onBlur', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();

    jest.useFakeTimers();

    const wrapper = render(
      <div>
        <TimePicker
          label="Label"
          readOnly={false}
          disabled={false}
          onChange={onChangeSpy}
          onBlur={onBlurSpy}
        />
      </div>
    );

    const iconElement = wrapper.container.querySelector(
      'i[class="icon icon-time size-5x"]'
    );

    const element = wrapper.container.querySelector('input');
    element?.focus();

    act(() => {
      userEvent.click(iconElement!);
      jest.runAllTimers();
    });

    expect(onBlurSpy).toBeCalled();
  });

  it('check handleOnMouseDownIcon function > disabled', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();

    jest.useFakeTimers();

    const wrapper = render(
      <div>
        <TimePicker
          label="Label"
          readOnly={false}
          disabled={true}
          onChange={onChangeSpy}
          onBlur={onBlurSpy}
        />
      </div>
    );

    const iconElement = wrapper.container.querySelector(
      'i[class="icon icon-time size-5x"]'
    );
    act(() => {
      userEvent.click(iconElement!);
      jest.runAllTimers();
    });

    expect(onChangeSpy).not.toBeCalled();
  });

  it('check handleOnMouseDownIcon function > readOnly', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();

    jest.useFakeTimers();

    const wrapper = render(
      <div>
        <TimePicker
          label="Label"
          readOnly={true}
          disabled={false}
          onChange={onChangeSpy}
          onBlur={onBlurSpy}
        />
      </div>
    );

    const iconElement = wrapper.container.querySelector(
      'i[class="icon icon-time size-5x"]'
    );
    act(() => {
      userEvent.click(iconElement!);
      jest.runAllTimers();
    });

    expect(onChangeSpy).not.toBeCalled();
  });

  it('on blur > document.activeElement === maskedTextBoxRef.current?.inputElement', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    const mockValue = {
      hour: '08'
    };

    jest.useFakeTimers();

    const wrapper = render(
      <div>
        <TimePicker
          label="Label"
          readOnly={false}
          disabled={false}
          value={mockValue}
          onChange={onChangeSpy}
          onBlur={onBlurSpy}
        />
      </div>
    );

    const element = wrapper.container.querySelector('input');

    act(() => {
      fireEvent.focus(element!);
    });
    jest.runAllTimers();

    act(() => {
      fireEvent.blur(element!);
      jest.runAllTimers();
    });

    element?.focus();

    act(() => {
      fireEvent.blur(element!);
      jest.runAllTimers();
    });

    expect(onBlurSpy).toBeCalled();
  });

  it('mouseDown popup', () => {
    const onChangeSpy = jest.fn();
    const onBlurSpy = jest.fn();
    const mockValue = {
      hour: '08'
    };

    jest.useFakeTimers();

    const wrapper = render(
      <div>
        <TimePicker
          label="Label"
          readOnly={false}
          disabled={false}
          value={mockValue}
          onChange={onChangeSpy}
          onBlur={onBlurSpy}
        />
      </div>
    );

    const element = wrapper.container.querySelector('input');

    act(() => {
      fireEvent.focus(element!);
    });
    jest.runAllTimers();

    const popup = queryHelpers.queryByAttribute(
      'class',
      wrapper.baseElement as HTMLElement,
      /dls-popup/
    );

    act(() => {
      fireEvent.mouseDown(popup!);
      jest.runAllTimers();
    });

    expect(popup).toBeInTheDocument();
  });

  it('should cover handlePopupClosed and apply next tabIndex is fakeFocusRef.current', () => {
    jest.useFakeTimers();
    const { container, queryByText } = render(
      <div>
        <TimePicker />
        <Button autoBlur={false}>Test handleOnPopupClosed</Button>
      </div>
    );

    const inputElement = container.querySelector(
      '.text-field-container input'
    )!;

    const buttonElement = queryByText('Test handleOnPopupClosed')!;
    userEvent.click(inputElement);
    jest.runAllTimers();

    userEvent.click(document.body);
    jest.runAllTimers();

    userEvent.click(inputElement);
    jest.runAllTimers();

    userEvent.click(buttonElement);
    jest.runAllTimers();

    expect(true).toBeTruthy();
  });
  describe('for using second ', () => {
    it('render with showSecond prop', () => {
      const onChangeSpy = jest.fn();
      const onBlurSpy = jest.fn();
      const mockValue = {
        hour: '08'
      };
      jest.useFakeTimers();

      const wrapper = render(
        <div>
          <TimePicker
            label="Label"
            value={mockValue}
            onChange={onChangeSpy}
            onBlur={onBlurSpy}
            small
            showSecond
          />
        </div>
      );

      const timePicker = queryHelpers.queryByAttribute(
        'class',
        wrapper.baseElement as HTMLElement,
        /dls-time-picker small/
      );
      expect(timePicker).toBeInTheDocument();
    });
    it('render with showSecond prop', () => {
      const onChangeSpy = jest.fn();
      const onBlurSpy = jest.fn();
      const mockValue = {
        hour: '08',
        minute: '08',
        second: '99'
      };
      jest.useFakeTimers();

      const wrapper = render(
        <div>
          <TimePicker
            label="Label"
            value={mockValue}
            onChange={onChangeSpy}
            onBlur={onBlurSpy}
            small
            showSecond
          />
        </div>
      );

      const inputElement = wrapper.container.querySelector('input');

      act(() => {
        userEvent.type(inputElement!, '01:00:99');
      });
      jest.runAllTimers();

      expect(inputElement?.value).toEqual('08:08:59');
    });
  });
});
