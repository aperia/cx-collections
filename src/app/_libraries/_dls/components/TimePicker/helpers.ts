import isEmpty from 'lodash.isempty';
import { TimePickerValue } from './types';
export const validNumber = (value: string | undefined) => {
  if (!value || isNaN(value as any)) return undefined;
  return value;
};

const checkValidNumber = (value: string) => {
  const validReg = /(__|\d\d)/i;
  return validReg.test(value);
};

export const checkValidAmPm = (value?: string) => {
  if (!value) return false;
  const validReg = /(__|am|pm)/i;
  return validReg.test(value);
};

export const isFullValidTimePicker = (value: TimePickerValue): boolean => {
  const { hour, meridiem, minute, second } = value;

  if (
    hour === undefined &&
    meridiem === undefined &&
    minute === undefined &&
    second === undefined
  )
    return true;

  return Boolean(hour && minute && (meridiem || second));
};

type TimeParser = (
  value: string,
  showSecond?: boolean
) => {
  hour?: string;
  minute?: string;
  meridiem?: string;
  second?: string;
  isValid: boolean;
  maskedValue: string;
};

export const combineTime = (
  value: TimePickerValue,
  showSecond?: boolean
): string => {
  if (isEmpty(value)) return '';
  if (Object.values(value).every(item => item === '__')) {
    return '';
  }

  if (Object.values(value).every(item => item === undefined)) {
    return '';
  }

  const { hour, meridiem, minute, second } = value;
  const connector = showSecond ? ':' : ' ';
  const thirdValue = showSecond ? second : meridiem;
  return `${hour || '__'}:${minute || '__'}${connector}${thirdValue || '__'}`;
};

/**
 * input 12:20 AM,  1_:__ __
 * output {hours:12,minutes:20,ampm:"AM"}  {hours:undefined,minutes:undefined, ampm:undefined }
 */
export const timeParser: TimeParser = (value, showSecond) => {
  let hour = value.substr(0, 2);
  let minute = value.substr(3, 2);
  let second = value.substr(6, 2);
  let isValid = false;
  const meridiem = !showSecond ? value.substr(6, 2).toUpperCase() : undefined;

  const hourInt = parseInt(hour);
  const minuteInt = parseInt(minute);
  const secondInt = parseInt(second);

  if (!isNaN(hourInt) && hourInt > 12) {
    const newHours = Math.min(hourInt - 12, 12);
    hour = newHours > 10 ? `${newHours}` : `0${newHours}`;
  }

  if (!isNaN(minuteInt) && minuteInt > 59) {
    minute = Math.min(minuteInt, 59).toString();
  }

  if (!isNaN(secondInt) && secondInt > 59) {
    second = Math.min(secondInt, 59).toString();
  }

  const isValidSecondOrMeridiem = showSecond
    ? checkValidNumber(second)
    : checkValidAmPm(meridiem);

  if (checkValidNumber(hour) && checkValidNumber(minute)) {
    isValid = true;
  }

  return {
    hour: validNumber(hour),
    minute: validNumber(minute),
    second: validNumber(second),
    meridiem: meridiem && meridiem.includes('_') ? undefined : meridiem,
    isValid: isValid && isValidSecondOrMeridiem,
    maskedValue: combineTime({ hour, meridiem, minute, second }, showSecond)
  };
};

export const format2DigitNumber = (value: number): string => {
  return value < 10 ? `0${value}` : `${value}`;
};

export const generateTimeData = (from: number, to: number) => {
  const data: string[] = [];
  for (let index = from; index <= to; index++) {
    data.push(format2DigitNumber(index));
  }
  return data;
};

export const formatReturnValue = (value: TimePickerValue): TimePickerValue => {
  const newValue: TimePickerValue = {};
  const timeKeys = Object.keys(value) as Array<keyof TimePickerValue>;
  timeKeys.forEach(timeKey => {
    if (value[timeKey]) newValue[timeKey] = value[timeKey];
  });
  return newValue;
};
