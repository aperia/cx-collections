import React, { useCallback, useEffect, useRef, useState } from 'react';
import { TimePickerProps } from './types';

import MaskedTextBox, { MaskedTextBoxRef } from '../MaskedTextBox';
import PopupBase, { PopupBaseRef } from '../PopupBase';

import TimeColumn from './TimeColumn';

import { className, genAmtId } from '../../utils';

import {
  TimeColumnChangeEvent,
  TimePickerValue,
  TimePickerChangeEvent,
  TimeName
} from './types';

import {
  timeParser,
  combineTime,
  validNumber,
  isFullValidTimePicker,
  generateTimeData,
  formatReturnValue
} from './helpers';
import { Icon, Tooltip } from '..';
import classnames from 'classnames';
import Label from '../Label';
import pick from 'lodash.pick';
import isFunction from 'lodash.isfunction';
import isEmpty from 'lodash.isempty';

// Design reference
// https://aperiainc.invisionapp.com/console/Collections---DLS-ckhgzuo8q9lja01y3tzs2wiry/ckmd2ildb01530110hybtd0jk/inspect

const hoursData = generateTimeData(0, 12);
const minuteData = generateTimeData(0, 59);
const secondsData = generateTimeData(0, 59);
const amPmData = ['AM', 'PM'];

const maskFormat = [/\d/, /\d/, ':', /\d/, /\d/, ' ', /[A|P]/i, /[M]/i];
const maskSecondFormat = [/\d/, /\d/, ':', /\d/, /\d/, ':', /\d/, /\d/];
const defaultPlaceholder = 'hh:mm AM/PM';
const defaultPlaceholderWithSecond = 'hh:mm:ss';

const TimePicker: React.FC<TimePickerProps> = ({
  disabled,
  error,
  label,
  placeholder: placeholderProp,
  readOnly,
  required,
  value: valueProp,
  className: classNameProp,
  data,
  mask: maskProp,
  errorTooltipProps,
  name,
  onChange: onChangeProp,
  onBlur,
  onFocus,
  small,
  showSecond,
  timeLabels,
  id,

  dataTestId
}) => {
  const maskedTextBoxRef = useRef<MaskedTextBoxRef | null>(null);
  const popupBaseRef = useRef<PopupBaseRef | null>(null);
  const lastValidValue = useRef<TimePickerValue | undefined>();
  const fakeFocusRef = useRef<HTMLDivElement | null>(null);

  const mask = maskProp || showSecond ? maskSecondFormat : maskFormat;
  const placeholder =
    placeholderProp || showSecond
      ? defaultPlaceholderWithSecond
      : defaultPlaceholder;

  const [opened, setOpened] = useState(false);

  const [timePickerValue, setTimePickerValue] = useState<TimePickerValue>(
    valueProp || {}
  );

  const {
    hours = hoursData,
    meridiem = amPmData,
    minutes = minuteData,
    seconds = secondsData
  } = data || {};

  const [maskedTextBoxValue, setMaskTextBoxValue] = useState<string>('');

  const handleChange = useCallback(
    (timeValue: TimePickerValue) => {
      const value = formatReturnValue(timeValue);
      const newTarget = {
        target: {
          value,
          id,
          name
        },
        value
      } as TimePickerChangeEvent;
      typeof onChangeProp === 'function' && onChangeProp(newTarget);
      lastValidValue.current = value;
    },
    [id, name, onChangeProp]
  );

  // handle focus on MaskedTextBox input element
  const handleFocus = (event: React.FocusEvent<HTMLInputElement>) => {
    isFunction(onFocus) &&
      onFocus({
        ...event,
        target: {
          value: timePickerValue,
          id,
          name
        },
        value: timePickerValue
      } as any);
    handleOpenedMode(true);
  };

  // handle blur on MaskedTextBox input element
  const handleBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    if (
      document.activeElement &&
      document.activeElement === maskedTextBoxRef.current?.inputElement
    ) {
      maskedTextBoxRef.current?.inputElement?.blur();
    }

    handleOpenedMode(false);

    if (readOnly) return;

    let value = lastValidValue.current || {};
    // when user select only hours -> set minute :00 , and AM
    if (
      !showSecond &&
      validNumber(timePickerValue?.hour) &&
      !timePickerValue?.minute &&
      !timePickerValue?.meridiem
    ) {
      value = { ...timePickerValue, minute: '00', meridiem: 'AM' };
      handleChange(value);
    }

    if (
      !showSecond &&
      !timePickerValue?.hour &&
      !timePickerValue?.minute &&
      timePickerValue?.meridiem
    ) {
      value = { hour: '00', minute: '00', meridiem: timePickerValue.meridiem };
      handleChange(value);
    }

    // update masked textBox with previous valid value
    setMaskTextBoxValue(combineTime(value, showSecond));
    setTimePickerValue(value);
    isFunction(onBlur) &&
      onBlur({
        ...event,
        target: {
          value,
          id,
          name
        },
        value
      } as any);
  };

  // handle opened uncontrolled/controlled
  const handleOpenedMode = (nextOpened: boolean) => {
    setOpened(nextOpened);
  };

  const handleOnChangeMaskedTextBox = (event: any) => {
    const { isValid, maskedValue, ...timeValue } = timeParser(
      event.value,
      showSecond
    );

    setMaskTextBoxValue(maskedValue);
    if (!isValid) return;

    setTimePickerValue(timeValue);

    isFullValidTimePicker(timeValue) && handleChange(timeValue);
  };

  const handleChangeTimeByScrollOrClick = useCallback(
    (event: TimeColumnChangeEvent) => {
      const { name: timeName, value, shouldBlur } = event;

      setTimePickerValue((currentValue) => {
        const newTimePickerValue = { ...currentValue, [timeName]: value };
        if (isFullValidTimePicker(newTimePickerValue)) {
          handleChange(newTimePickerValue);
          lastValidValue.current = newTimePickerValue;
        }
        return newTimePickerValue;
      });

      const inputElement = maskedTextBoxRef.current?.inputElement;
      if (!inputElement || !shouldBlur) return;
      process.nextTick(() => {
        inputElement?.blur();
      });
    },
    [handleChange]
  );

  const handlePopupShown = () => {
    const popupElement = popupBaseRef.current?.element;

    // handle mousedown on Popup's TimePicker
    // mousedown on Popup's TimePicker will trigger onBlur default of the MaskedTextbox
    // that is not the behavior we want, so we need to preventDefault it
    const handleMouseDown = (event: MouseEvent) => {
      event.preventDefault();
    };

    // not need to remove event later, because popup element will be remove from DOM
    popupElement?.addEventListener('mousedown', handleMouseDown);
  };

  const handlePopupClosed = () => {
    if (document?.activeElement === document?.body) {
      fakeFocusRef.current?.focus({ preventScroll: true });
    }
  };

  // handle mousedown on icon
  const handleOnMouseDownIcon = (
    event: React.MouseEvent<HTMLSpanElement, MouseEvent>
  ) => {
    if (disabled || readOnly) return;

    const inputElement = maskedTextBoxRef.current?.inputElement;

    // stop default behavior browser (blur/focus, abc, xyz...)
    event.preventDefault();

    if (document.activeElement && document.activeElement === inputElement) {
      inputElement?.blur();
      return;
    }
    inputElement?.focus({ preventScroll: true });
  };

  // update timePickerValue in controlled mode
  useEffect(() => {
    if (valueProp === undefined) return;
    setTimePickerValue(valueProp);

    if (isFullValidTimePicker(valueProp)) {
      lastValidValue.current = valueProp;
    }
  }, [valueProp]);

  // update maskedTextBoxValue
  useEffect(() => {
    setMaskTextBoxValue(combineTime(timePickerValue, showSecond));
  }, [showSecond, timePickerValue]);

  // check required
  const isRequired = required && !readOnly && !disabled;

  // check should show label or not
  const { message, status } = pick(error, 'message', 'status');
  const isOpenedTooltip = Boolean(status && message && opened);
  const floating = (opened && !readOnly) || !isEmpty(timePickerValue);

  return (
    <>
      <Tooltip
        opened={isOpenedTooltip}
        element={message}
        variant="error"
        placement="top-start"
        triggerClassName="d-block"
        dataTestId={`${dataTestId}-time-picker-error`}
        {...errorTooltipProps}
      >
        <div
          className={classnames(
            'dls-time-picker',
            small && 'small',
            opened && className.state.FOCUSED,
            readOnly && className.state.READ_ONLY,
            disabled && className.state.DISABLED,
            status && className.state.ERROR,
            !small && floating && className.state.FLOATING,
            classNameProp
          )}
          data-testid={genAmtId(dataTestId, 'dls-time-picker', 'TimePicker')}
        >
          <div className="text-field-container">
            <MaskedTextBox
              ref={maskedTextBoxRef}
              mask={mask}
              onFocus={handleFocus}
              onBlur={handleBlur}
              value={maskedTextBoxValue}
              placeholder={placeholder}
              onChange={handleOnChangeMaskedTextBox}
              disabled={disabled}
              readOnly={readOnly}
              name={name}
              autoComplete="off"
              small
            />
            {!small && label && (
              <Label asterisk={isRequired} error={status}>
                {label}
              </Label>
            )}
          </div>
          <Icon
            onMouseDown={handleOnMouseDownIcon}
            name="time"
            size={small ? '4x' : '5x'}
          />
          <div ref={fakeFocusRef} tabIndex={-1} />
        </div>
      </Tooltip>
      <PopupBase
        reference={maskedTextBoxRef.current?.inputElement as HTMLElement}
        opened={opened && !readOnly && !disabled}
        onVisibilityChange={handleOpenedMode}
        onPopupShown={handlePopupShown}
        onPopupClosed={handlePopupClosed}
        ref={popupBaseRef}
        dataTestId={`${dataTestId}-popup-time-picker`}
      >
        <div className="dls-time-picker__body">
          <div
            className={classnames(
              'dls-time-picker__selection',
              showSecond && 'show-second'
            )}
          ></div>
          <TimeColumn
            label={timeLabels?.hour || 'Hour'}
            data={hours}
            value={timePickerValue?.hour}
            name={TimeName.hour}
            onChange={handleChangeTimeByScrollOrClick}
          />
          <TimeColumn
            label={timeLabels?.minute || 'Minute'}
            data={minutes}
            value={timePickerValue?.minute}
            name={TimeName.minute}
            onChange={handleChangeTimeByScrollOrClick}
          />
          {showSecond ? (
            <TimeColumn
              label={timeLabels?.second || 'Second'}
              data={seconds}
              value={timePickerValue?.second}
              name={TimeName.second}
              onChange={handleChangeTimeByScrollOrClick}
            />
          ) : (
            <TimeColumn
              label={timeLabels?.meridiem || 'AM/PM'}
              data={meridiem}
              value={timePickerValue?.meridiem}
              name={TimeName.meridiem}
              onChange={handleChangeTimeByScrollOrClick}
            />
          )}
        </div>
      </PopupBase>
    </>
  );
};

export type {
  ErrorType,
  TimeColumnChangeEvent,
  TimePickerValue
} from './types';
export default TimePicker;
