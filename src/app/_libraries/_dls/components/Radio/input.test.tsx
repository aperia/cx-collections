import React from 'react';
import Input from './Input';
import '@testing-library/jest-dom';
import { fireEvent, render } from '@testing-library/react';
import { queryBy } from '../../test-utils/queryHelpers';
import { mockUseRef } from '../../test-utils/hooks';
import { Radio } from '..';

describe('Test Input Radio Button', () => {
  const spyRef: any = mockUseRef();
  it('Should have default attribute', () => {
    const { container } = render(<Input ref={spyRef} />);
    const queryClass = queryBy(
      container,
      'class',
      'custom-control-input dls-radio-input'
    );
    const queryType = queryBy(container, 'type', 'radio');
    const queryChecked = queryBy(container, 'checked', '');
    expect(queryChecked).not.toBeInTheDocument;
    expect(queryType).toBeInTheDocument();
    expect(queryClass).toBeInTheDocument();
  });

  it('Should have checkProps', () => {
    const { container } = render(<Input checked={true} />);
    const queryType = queryBy(container, 'type', '');
    expect(queryType).toBeInTheDocument;
  });

  it('Should have className', () => {
    const className = 'className';
    const { container } = render(<Input className={className} />);
    const queryClass = queryBy(
      container,
      'class',
      `custom-control-input dls-radio-input ${className}`
    );
    expect(queryClass).toBeInTheDocument;
  });

  it('Should have disabled', () => {
    const { container } = render(<Input disabled={true} />);
    const queryDisabled = queryBy(container, 'class', `disabled`);
    expect(queryDisabled).toBeInTheDocument;
  });

  it('Should have id', () => {
    const id = 'id-test';
    const { container } = render(<Input id={id} />);
    const queryChecked = queryBy(container, 'id', id);
    expect(queryChecked).toBeInTheDocument;
  });

  it('Should have readOnly', () => {
    const onChange = jest.fn();
    const { container } = render(<Input readOnly={true} onChange={onChange} />);
    const queryReadOnly = queryBy(container, 'readonly', '');
    expect(queryReadOnly).toBeInTheDocument();
  });

  it('Should have uncontrolled', () => {
    const onChange = jest.fn();
    const { container } = render(<Radio.Input onChange={onChange} />);
    const queryClass = queryBy(
      container,
      'class',
      'custom-control-input dls-radio-input'
    );
    fireEvent.click(queryClass!);
    expect(onChange).toHaveBeenCalled();
  });

  it('Should have onChange not call', () => {
    const onChange = jest.fn();
    const { container } = render(
      <Radio.Input readOnly={true} onChange={onChange} />
    );
    const queryClass = queryBy(
      container,
      'class',
      'custom-control-input dls-radio-input'
    );
    fireEvent.click(queryClass!);
    expect(onChange).not.toHaveBeenCalled();
  });

  it('Should have indeterminate', () => {
    const onChange = jest.fn();
    const { container } = render(
      <Radio.Input indeterminate={true} onChange={onChange} />
    );
    const queryClass = queryBy(
      container,
      'class',
      `custom-control-input dls-radio-input indeterminate`
    );
    const queryChecked = queryBy(container, 'checked', '');
    expect(queryClass).toBeInTheDocument();
    expect(queryChecked).not.toBeInTheDocument();
  });
});
