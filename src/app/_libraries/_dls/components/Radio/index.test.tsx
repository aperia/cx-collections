import React from 'react';
import Radio from './index';
import { queryByText, render } from '@testing-library/react';
import { mockUseRef } from '../../test-utils/hooks';
import { queryBy } from '../../test-utils/queryHelpers';
import '@testing-library/jest-dom';
describe('Test Radio Button', () => {
  const spyRef: any = mockUseRef();
  it('Should have checked-style', () => {
    const { container } = render(
      <Radio ref={spyRef}>
        <Radio.Input className="checked-style" id="r0" name="e" />
      </Radio>
    );

    const queryClassStyle = queryBy(container, 'class', /checked-style/);

    const queryCustomRadio = queryBy(
      container,
      'class',
      /custom-control custom-radio dls-radio/
    );

    expect(queryCustomRadio).toBeInTheDocument();
    expect(queryClassStyle).toBeInTheDocument();
  });

  it('Should haven"t checked-style', () => {
    const { container } = render(
      <Radio ref={spyRef}>
        <Radio.Input id="r0" name="e" />
      </Radio>
    );

    const queryClassStyle = queryBy(container, 'class', /checked-style/);

    const queryCustomRadio = queryBy(
      container,
      'class',
      /custom-control custom-radio dls-radio/
    );

    expect(queryCustomRadio).toBeInTheDocument();
    expect(queryClassStyle).not.toBeInTheDocument();
  });

  it('Should have className', () => {
    const className = 'className';
    const { container } = render(
      <Radio className={className} ref={spyRef}>
        <Radio.Input id="r0" name="e" />
      </Radio>
    );
    const queryClassProps = queryBy(
      container,
      'class',
      `custom-control custom-radio dls-radio ${className}`
    );
    expect(queryClassProps).toBeInTheDocument();
  });

  it('Should have label', () => {
    const text = 'Normal';
    const { container } = render(
      <Radio ref={spyRef}>
        <Radio.Input id="r2" />
        <Radio.Label>{text}</Radio.Label>
      </Radio>
    );
    const queryClassProps = queryByText(container, /Normal/);
    expect(queryClassProps?.textContent).toEqual(text);
  });

  it('Should have id for input', () => {
    const id = 'r2';
    const { container } = render(
      <Radio ref={spyRef}>
        <Radio.Input id={id} />
        <Radio.Label>Normal</Radio.Label>
      </Radio>
    );
    const queryId = queryBy(container, 'id', id);
    const queryFor = queryBy(container, 'for', id);
    expect(queryFor).toBeInTheDocument();
    expect(queryId).toBeInTheDocument();
  });

  it('Should haven"t id for input', () => {
    const { container } = render(
      <Radio ref={spyRef}>
        <Radio.Input />
        <Radio.Label>Normal</Radio.Label>
      </Radio>
    );
    const queryId = queryBy(container, 'id', /dls/);
    const queryFor = queryBy(container, 'for', /dls/);
    // console.log(queryId, queryFor);
    expect(queryId!.getAttribute('id')).toEqual(queryFor!.getAttribute('for'));
  });

  it('Should haven"t id for Label', () => {
    const id = 'r2';
    const { container } = render(
      <Radio ref={spyRef}>
        <Radio.Input />
        <Radio.Label id={id}>Normal</Radio.Label>
      </Radio>
    );
    const queryId = queryBy(container, 'id', id);
    expect(queryId).toBeInTheDocument();
  });
});
