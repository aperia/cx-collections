import classNames from 'classnames';
import { className } from '../../utils';
import React, { DetailedHTMLProps, FC, LabelHTMLAttributes } from 'react';

export interface LabelProps
  extends React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLLabelElement>,
    HTMLLabelElement
  > {}

const Label: FC<
  DetailedHTMLProps<LabelHTMLAttributes<HTMLLabelElement>, HTMLLabelElement>
> = ({ className: labelClassName, children, ...props }) => {
  return (
    <label
      className={classNames(className.radio.LABEL, labelClassName)}
      {...props}
    >
      {children}
    </label>
  );
};

export default Label;
