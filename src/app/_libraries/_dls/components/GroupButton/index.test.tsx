import React from 'react';
import '@testing-library/jest-dom';
import GroupButton, { GroupButtonContext, useIsGroupButton } from './index';
import { render, screen } from '@testing-library/react';
import { queryByClass } from '../../test-utils/queryHelpers';

const MockButton = () => {
  const isDisplay = useIsGroupButton();
  return (
    <React.Fragment>{isDisplay ? 'display' : 'nonDisplay'}</React.Fragment>
  );
};

describe('Test Group Button', () => {
  it('Render Group Button', () => {
    const { container } = render(
      <GroupButtonContext.Provider value={true}>
        <MockButton />
        <GroupButton></GroupButton>
      </GroupButtonContext.Provider>
    );
    const getByClass = queryByClass(container, /dls-group-button/)!;
    const queryByText = screen.queryByText('display')!;
    expect(queryByText.textContent).toEqual('display');
    expect(getByClass).toBeInTheDocument();
  });

  it('Render Group Button', () => {
    const { container } = render(
      <GroupButtonContext.Provider value={false}>
        <MockButton />
        <GroupButton></GroupButton>
      </GroupButtonContext.Provider>
    );
    const getByClass = queryByClass(container, /dls-group-button/)!;
    const queryByText = screen.queryByText('nonDisplay')!;
    expect(queryByText.textContent).toEqual('nonDisplay');
    expect(getByClass).toBeInTheDocument();
  });
});
