import React from 'react';
import '@testing-library/jest-dom';
import { cleanup, render } from '@testing-library/react';

import Messages from './Messages';
import { formatBytes } from './helper';

afterEach(cleanup);

describe('Messages', () => {
  it('is done', () => {
    const valid = { isTypeSupport: true };
    const status = { isDone: true };

    const { container } = render(<Messages valid={valid} status={status} />);
    expect(container.textContent).toEqual('Uploaded');
  });

  it('is uploading', () => {
    const valid = { isTypeSupport: true };
    const status = { isUpload: true };

    const { container } = render(<Messages valid={valid} status={status} />);
    expect(container.textContent).toEqual('Uploading ...');
  });

  it('is upload failed', () => {
    const valid = { isTypeSupport: true };
    const status = { isUploadFailed: true };

    const { container } = render(<Messages valid={valid} status={status} />);
    expect(container.textContent).toEqual('Upload Failed');
  });

  it('is invalid type file & over size', () => {
    const valid = { isTypeSupport: false, isOverSize: true };
    const status = { maxFile: 1024, minFile: 0 };
    const txtMaxSize = formatBytes(status.maxFile);

    const { container } = render(<Messages valid={valid} status={status} />);
    expect(container.textContent).toEqual(
      `Invalid file format and size exceeds ${txtMaxSize}`
    );
  });

  it('is invalid type file', () => {
    const valid = { isTypeSupport: false };
    const status = {};

    const { container } = render(<Messages valid={valid} status={status} />);
    expect(container.textContent).toEqual('Invalid file format');
  });

  it('is over size', () => {
    const valid = { isTypeSupport: true, isOverSize: true };
    const status = { maxFile: 1024 };
    const txtMaxSize = formatBytes(status.maxFile);

    const { container } = render(<Messages valid={valid} status={status} />);
    expect(container.textContent).toEqual(`File size exceeds ${txtMaxSize}`);
  });

  it('is under size', () => {
    const valid = { isTypeSupport: true, isUnderSize: true };
    const status = { minFile: 1024 };
    const txtMinSize = formatBytes(status.minFile);

    const { container } = render(<Messages valid={valid} status={status} />);
    expect(container.textContent).toEqual(`File size exceeds ${txtMinSize}`);
  });

  it('is valid file', () => {
    const valid = { isTypeSupport: true };
    const status = {};

    const { container } = render(<Messages valid={valid} status={status} />);
    expect(container.textContent).toEqual('Valid');
  });
});
