import React from 'react';
import '@testing-library/jest-dom';
import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import Upload from './';
import { queryByClass } from '../../test-utils/queryHelpers';
import { UploadRef } from './type';
import { STATUS } from './helper';
import * as axios from 'axios';
import axios1, { AxiosRequestConfig } from 'axios';
import { act } from 'react-dom/test-utils';

jest.mock('../../hooks', () => {
  const hooks = jest.requireActual('../../hooks');

  return {
    ...hooks,
    useTranslation: () => ({ t: (text: string) => text })
  };
});

afterEach(cleanup);

const generaFiles = (options: any[]) => {
  return options.map((option, index) => ({
    idx: `id${index}`,
    name: `file${index}.txt`,
    size: 1024,
    length: 1024,
    item: (index: number) => null,
    ...option
  }));
};

const getMockAxios = (result: any) => {
  return jest.spyOn(axios1, 'create').mockImplementation(() => {
    const { create: axiosCreate } = jest.requireActual('axios');
    return {
      ...axiosCreate,
      post: async (_saveUrl, formData, options) => {
        const { onUploadProgress } = options as AxiosRequestConfig;

        onUploadProgress!({
          loaded: 50,
          total: 100
        } as ProgressEvent<EventTarget>);
        return result;
      }
    };
  });
};

const getMockAxiosForTestRemoveInUploading = (result: any) => {
  return jest.spyOn(axios, 'create').mockImplementation(() => {
    const { create: axiosCreate } = jest.requireActual('axios');
    return {
      ...axiosCreate,
      post: async (_saveUrl, formData, options) => {
        const { onUploadProgress } = options as AxiosRequestConfig;

        onUploadProgress!({
          loaded: 50,
          total: 100
        } as ProgressEvent<EventTarget>);
        return result;
      },
      CancelToken: {
        source: () => {
          return {
            token: {},
            cancel: jest.fn()
          };
        }
      }
    };
  });
};

describe('Upload', () => {
  it('Render mode uncontrolled', () => {
    const ref = React.createRef<UploadRef>();
    const handleDragEnter = jest.fn();
    const handleDragLeave = jest.fn();
    const handleDrop = jest.fn();

    const { container } = render(
      <Upload
        ref={ref}
        saveUrl="localhost:3000"
        isEditFileName
        renderElm={() => <div className="elm" />}
        onDragEnter={handleDragEnter}
        onDragLeave={handleDragLeave}
        onDrop={handleDrop}
        inputConfig={{ className: 'file-test' }}
      />
    );

    expect(queryByClass(container, 'dls-upload')).toBeInTheDocument();
    expect(queryByClass(container, 'elm')).toBeInTheDocument();
  });

  describe('onDragEnter', () => {
    const eventDragEnter = {
      preventDefault: jest.fn(),
      dataTransfer: { items: [{ id: 1 }] }
    };
    const ref = React.createRef<UploadRef>();
    const handleDragEnter = jest.fn();
    const props = {
      ref,
      saveUrl: 'localhost:3000',
      inputConfig: { className: 'file-test' },
      onDragEnter: handleDragEnter
    };

    it('multiple', () => {
      const { container } = render(<Upload {...props} />);
      const cpmInput = queryByClass(container, 'file-test') as HTMLInputElement;

      fireEvent.dragEnter(cpmInput, eventDragEnter);
      expect(handleDragEnter).toHaveBeenCalled();
      expect(
        queryByClass(container, /action-upload/)?.getElementsByTagName(
          'span'
        )[0].textContent
      ).toEqual('txt_dls_dropping_file');
    });
    it('single', () => {
      const { container } = render(<Upload {...props} multiple={false} />);
      const cpmInput = queryByClass(container, 'file-test') as HTMLInputElement;

      fireEvent.dragEnter(cpmInput, eventDragEnter);
      expect(handleDragEnter).not.toHaveBeenCalled();
      expect(
        queryByClass(container, /action-upload/)?.getElementsByTagName(
          'span'
        )[0]?.textContent
      ).toEqual('txt_dls_choose_file');
    });
  });

  it('onDragLeave', () => {
    const ref = React.createRef<UploadRef>();
    const handleDragLeave = jest.fn();

    const { container } = render(
      <Upload
        ref={ref}
        saveUrl="localhost:3000"
        onDragLeave={handleDragLeave}
        inputConfig={{ className: 'file-test' }}
      />
    );
    const cpmInput = queryByClass(container, 'file-test') as HTMLInputElement;

    const eventDragEnter = {
      preventDefault: jest.fn(),
      dataTransfer: { items: [{ id: 1 }] }
    };
    fireEvent.dragEnter(cpmInput, eventDragEnter);

    const eventDragLeave = { preventDefault: jest.fn() };
    fireEvent.dragLeave(cpmInput, eventDragLeave);
    expect(handleDragLeave).toHaveBeenCalled();
    expect(
      queryByClass(container, /action-upload/)?.getElementsByTagName('span')[0]
        ?.textContent
    ).toEqual('txt_dls_drag_and_drop_file');
  });

  it('onDragLeave with single', () => {
    const ref = React.createRef<UploadRef>();
    const handleDragLeave = jest.fn();

    const { container } = render(
      <Upload
        ref={ref}
        saveUrl="localhost:3000"
        onDragLeave={handleDragLeave}
        inputConfig={{ className: 'file-test' }}
        multiple={false}
      />
    );
    const cpmInput = queryByClass(container, 'file-test') as HTMLInputElement;

    const eventDragEnter = {
      preventDefault: jest.fn(),
      dataTransfer: { items: [{ id: 1 }] }
    };
    fireEvent.dragEnter(cpmInput, eventDragEnter);

    const eventDragLeave = { preventDefault: jest.fn() };
    fireEvent.dragLeave(cpmInput, eventDragLeave);
    expect(handleDragLeave).not.toHaveBeenCalled();
    expect(
      queryByClass(container, /action-upload/)?.getElementsByTagName('span')[0]
        ?.textContent
    ).toEqual('txt_dls_choose_file');
  });

  describe('onDragDrop', () => {
    it('uncontrolled', () => {
      const ref = React.createRef<UploadRef>();
      const handleDrop = jest.fn();

      const { container } = render(
        <Upload
          ref={ref}
          saveUrl="localhost:3000"
          onDrop={handleDrop}
          inputConfig={{ className: 'file-test' }}
        />
      );
      const cpmInput = queryByClass(container, 'file-test') as HTMLInputElement;

      const eventDrop = {
        preventDefault: jest.fn(),
        stopPropagation: jest.fn(),
        dataTransfer: {
          files: [new File(['test'], 'file1.txt', { type: 'text/plain' })]
        }
      };
      fireEvent.drop(cpmInput, eventDrop);
      expect(handleDrop).toHaveBeenCalled();
    });
    it('controlled', () => {
      const ref = React.createRef<UploadRef>();
      const handleDrop = jest.fn();
      const handleChangeFile = jest.fn();

      const { container } = render(
        <Upload
          ref={ref}
          saveUrl="localhost:3000"
          files={[]}
          onDrop={handleDrop}
          onChangeFile={handleChangeFile}
          inputConfig={{ className: 'file-test' }}
        />
      );
      const cpmInput = queryByClass(container, 'file-test') as HTMLInputElement;

      const eventDrop = {
        preventDefault: jest.fn(),
        stopPropagation: jest.fn(),
        dataTransfer: {
          files: [new File(['test'], 'file1.txt', { type: 'text/plain' })]
        }
      };
      fireEvent.drop(cpmInput, eventDrop);
      expect(handleDrop).toHaveBeenCalled();
      expect(handleChangeFile).toHaveBeenCalled();
    });
    it('with single', () => {
      const ref = React.createRef<UploadRef>();
      const handleDrop = jest.fn();

      const { container } = render(
        <Upload
          ref={ref}
          saveUrl="localhost:3000"
          onDrop={handleDrop}
          inputConfig={{ className: 'file-test' }}
          multiple={false}
        />
      );
      const cpmInput = queryByClass(container, 'file-test') as HTMLInputElement;

      const eventDrop = {
        preventDefault: jest.fn(),
        stopPropagation: jest.fn(),
        dataTransfer: {
          files: [new File(['test'], 'file1.txt', { type: 'text/plain' })]
        }
      };
      fireEvent.drop(cpmInput, eventDrop);
      expect(handleDrop).not.toHaveBeenCalled();
    });
  });

  describe('handleRemoveFile', () => {
    it('normal remove file', () => {
      const ref = React.createRef<UploadRef>();
      const handleRemove = jest.fn();

      render(
        <Upload
          ref={ref}
          saveUrl="localhost:3000"
          onRemove={handleRemove}
          files={[
            {
              idx: 'id1',
              name: 'file1.txt',
              size: 1024,
              length: 1024,
              item: (index: number) => null
            }
          ]}
          inputConfig={{ className: 'file-test' }}
        />
      );
      screen.getByRole('button').click();
      expect(ref.current?.uploadFiles.length).toBe(0);
      expect(handleRemove).toHaveBeenCalled();
    });

    it('add file ->remove file', () => {
      const ref = React.createRef<UploadRef>();
      const handleRemove = jest.fn();

      const { container } = render(
        <Upload
          ref={ref}
          saveUrl="localhost:3000"
          onRemove={handleRemove}
          inputConfig={{ className: 'file-test' }}
        />
      );

      const cpmInput = queryByClass(container, 'file-test') as HTMLInputElement;
      const eventChange = {
        preventDefault: jest.fn(),
        target: { files: [{ name: 'image.png', size: 1024 }] }
      };
      fireEvent.change(cpmInput, eventChange);

      screen.getByRole('button').click();
      expect(ref.current?.uploadFiles.length).toBe(0);
    });

    it('add file -> upload -> remove file', () => {
      const ref = React.createRef<UploadRef>();
      const handleRemove = jest.fn();
      const handleChangeFile = jest.fn();

      const { container } = render(
        <Upload
          ref={ref}
          saveUrl="localhost:3000"
          onRemove={handleRemove}
          onChangeFile={handleChangeFile}
          inputConfig={{ className: 'file-test' }}
        />
      );

      const cpmInput = queryByClass(container, 'file-test') as HTMLInputElement;
      const eventChange = {
        preventDefault: jest.fn(),
        target: { files: [{ name: 'image.png', size: 1024 }] }
      };

      fireEvent.change(cpmInput, eventChange);
      jest.runAllTimers();

      ref.current?.uploadFiles();
      jest.runAllTimers();

      screen.getByRole('button').click();
      expect(ref.current?.uploadFiles.length).toBe(0);
      expect(handleChangeFile).toHaveBeenCalled();
    });

    it('remove in upload', async () => {
      const spy = getMockAxiosForTestRemoveInUploading(Promise.resolve());

      const ref = React.createRef<UploadRef>();
      const handleChangeFile = jest.fn();

      const files = generaFiles([{ status: STATUS.valid }]);
      const handleFinally = jest.fn();
      render(
        <Upload
          ref={ref}
          files={files}
          saveUrl="localhost:3000"
          onFinally={handleFinally}
          onChangeFile={handleChangeFile}
          inputConfig={{ className: 'file-test' }}
        />
      );

      ref.current?.uploadFiles();
      screen.getByRole('button').click();

      spy.mockReset();
      spy.mockRestore();
    });
  });

  describe('uploadFiles', () => {
    it('add file > upload - uncontrolled', () => {
      const spy = getMockAxios(Promise.resolve());
      const ref = React.createRef<UploadRef>();

      const { container } = render(
        <Upload
          ref={ref}
          saveUrl="localhost:3000"
          inputConfig={{ className: 'file-test' }}
        />
      );

      const cpmInput = queryByClass(container, 'file-test') as HTMLInputElement;
      const eventChange = {
        preventDefault: jest.fn(),
        target: { files: [{ name: 'image.png', size: 1024 }] }
      };

      fireEvent.change(cpmInput, eventChange);
      jest.runAllTimers();

      ref.current?.uploadFiles();

      expect(
        queryByClass(container, /file-progress uploading/)
      ).toBeInTheDocument();

      spy.mockReset();
      spy.mockRestore();
    });

    it('upload with no files', () => {
      const ref = React.createRef<UploadRef>();
      const handleChangeFile = jest.fn();
      render(
        <Upload
          ref={ref}
          saveUrl="localhost:3000"
          onChangeFile={handleChangeFile}
          files={[]}
          inputConfig={{ className: 'file-test' }}
        />
      );

      ref.current?.uploadFiles();
    });

    describe('saveURL is string', () => {
      it('resolve', async () => {
        const spy = getMockAxios(Promise.resolve());

        const ref = React.createRef<UploadRef>();
        const handleChangeFile = jest.fn();

        const files = generaFiles([
          { status: STATUS.valid },
          { status: STATUS.error }
        ]);
        const handleFinally = jest.fn();
        const { container } = render(
          <Upload
            ref={ref}
            files={files}
            saveUrl="localhost:3000"
            onFinally={handleFinally}
            onChangeFile={handleChangeFile}
            inputConfig={{ className: 'file-test' }}
          />
        );

        act(() => {
          ref.current?.uploadFiles();
          jest.runAllTimers();
        });
        expect(
          queryByClass(container, /action-upload disabled/)
        ).toBeInTheDocument();

        spy.mockReset();
        spy.mockRestore();
      });

      it('reject', async () => {
        const spy = getMockAxios(Promise.reject());

        const files = generaFiles([{ status: STATUS.valid }]);

        const ref = React.createRef<UploadRef>();
        const handleChangeFile = jest.fn();
        const handleFinally = jest.fn();
        render(
          <Upload
            ref={ref}
            files={files}
            saveUrl="localhost:3000"
            formatFormData={formData => formData}
            onFinally={handleFinally}
            onChangeFile={handleChangeFile}
            inputConfig={{ className: 'file-test' }}
          />
        );
        act(() => {
          ref.current?.uploadFiles();
        });

        spy.mockReset();
        spy.mockRestore();
      });
    });

    it('saveUrl is function', async () => {
      const spy = getMockAxios(Promise.reject());

      const ref = React.createRef<UploadRef>();
      const handleChangeFile = jest.fn();

      const files = generaFiles([{ status: STATUS.valid }]);
      const handleFinally = jest.fn();
      const saveUrl = jest.fn();
      render(
        <Upload
          ref={ref}
          files={files}
          saveUrl={saveUrl}
          formatFormData={formData => formData}
          onFinally={handleFinally}
          onChangeFile={handleChangeFile}
          inputConfig={{ className: 'file-test' }}
        />
      );
      act(() => {
        ref.current?.uploadFiles();
      });

      expect(saveUrl).toHaveBeenCalled();
      spy.mockReset();
      spy.mockRestore();
    });
  });

  describe('onChange', () => {
    it('uncontrolled', () => {
      const ref = React.createRef<UploadRef>();
      const handleChange = jest.fn();
      const handleChangeFile = jest.fn();

      const { container } = render(
        <Upload
          ref={ref}
          saveUrl="localhost:3000"
          onChange={handleChange}
          onChangeFile={handleChangeFile}
          inputConfig={{ className: 'file-test' }}
        />
      );
      const cpmInput = queryByClass(container, 'file-test') as HTMLInputElement;

      const eventChange = {
        preventDefault: jest.fn(),
        target: { files: [{ name: 'image.png', size: 1024 }] }
      };
      fireEvent.change(cpmInput, eventChange);
      expect(handleChange).toHaveBeenCalled();
    });

    it('controlled', () => {
      const handleChange = jest.fn();

      const { container } = render(
        <Upload
          saveUrl="localhost:3000"
          files={[]}
          onChange={handleChange}
          inputConfig={{ className: 'file-test' }}
        />
      );
      const cpmInput = queryByClass(container, 'file-test') as HTMLInputElement;

      const eventChange = {
        preventDefault: jest.fn(),
        target: { files: [{ name: 'image.png', size: 1024 }] }
      };
      fireEvent.change(cpmInput, eventChange);
      expect(handleChange).toHaveBeenCalled();
    });

    it('empty file - pass coverage', () => {
      const handleChange = jest.fn();

      const { container } = render(
        <Upload
          files={[]}
          saveUrl="localhost:3000"
          onChange={handleChange}
          inputConfig={{ className: 'file-test' }}
        />
      );
      const cpmInput = queryByClass(container, 'file-test') as HTMLInputElement;

      const eventChange = { preventDefault: jest.fn(), target: {} };
      fireEvent.change(cpmInput, eventChange);
    });
  });

  it('renderFiles', () => {
    const ref = React.createRef<UploadRef>();

    render(
      <Upload
        ref={ref}
        saveUrl="localhost:3000"
        inputConfig={{ className: 'file-test' }}
        renderFiles={() => <div data-testid="render-files" />}
      />
    );

    expect(screen.getByTestId('render-files')).toBeInTheDocument();
  });
});
