import React, { useMemo } from 'react';
import Icon from '../Icon';
import { useTranslation } from '../../hooks';
import { formatBytes } from './helper';

export interface MsgProps {
  valid: {
    isInvalid?: boolean;
    isTypeSupport?: boolean;
    isOverSize?: boolean;
    isUnderSize?: boolean;
  };
  status: {
    isUpload?: boolean;
    isDone?: boolean;
    isUploadFailed?: boolean;
    maxFile?: number;
    minFile?: number;
  };
  isError?: boolean;
}

const Message: React.FC<MsgProps> = ({ valid, status, isError }) => {
  const { t } = useTranslation();

  const content = useMemo(() => {
    const { isOverSize, isTypeSupport, isUnderSize } = valid;
    const { isDone, isUpload, isUploadFailed, maxFile, minFile } = status;
    const txtMaxSize = formatBytes(maxFile);
    const txtMinSize = formatBytes(minFile);

    if (isUploadFailed) {
      return t('txt_dls_upload_file_failed', 'Upload Failed');
    }
    if (!isTypeSupport && isOverSize) {
      return t(
        'txt_dls_invalid_and_over_size_file',
        `Invalid file format and size exceeds ${txtMaxSize}`,
        { size: txtMaxSize }
      );
    }

    if (!isTypeSupport) {
      return t('txt_dls_invalid_format_file', 'Invalid file format');
    }
    if (isOverSize) {
      return t('txt_dls_over_size_file', `File size exceeds ${txtMaxSize}`, {
        size: txtMaxSize
      });
    }
    if (isUnderSize) {
      return t('txt_dls_under_size_file', `File size exceeds ${txtMinSize}`, {
        size: txtMinSize
      });
    }

    if (isDone) {
      return t('txt_dls_uploaded_file', 'Uploaded');
    }
    if (isUpload) {
      return t('txt_dls_uploading_file', 'Uploading ...');
    }

    return t('txt_dls_valid_file', 'Valid');
  }, [status, t, valid]);

  return (
    <>
      {!status.isUpload && <Icon name={isError ? 'error' : 'success'} />}
      <div title={content}>{content}</div>
    </>
  );
};

export default Message;
