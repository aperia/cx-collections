import React, {
  useRef,
  useImperativeHandle,
  useState,
  useCallback,
  ReactText,
  useEffect
} from 'react';

// helper
import axios, { CancelTokenSource } from 'axios';
import isEmpty from 'lodash.isempty';
import isFunction from 'lodash.isfunction';
import isString from 'lodash.isstring';
import classNames from 'classnames';
import { STATUS, formatFiles, MAX_FILE, MIN_FILE } from './helper';
import { useTranslation } from '../../hooks';

// type
import { UploadRef, UploadProps, PromiseFile } from './type';

// component
import ListFile from './ListFile';
import FileItem, { StateFile } from './File';
import { genAmtId } from '../../utils';

export const STATUS_SUCCESS = 'fulfilled';

const Upload: React.RefForwardingComponent<UploadRef, UploadProps> = (
  {
    className,
    classNameFile,
    classNameListFile,
    inputConfig,
    files: filesProps,
    saveUrl,

    onDragLeave,
    onDragEnter,
    onDrop,
    onChange,

    disabled,
    onRemove,
    onFinally,
    onChangeFile,

    multiple = true,
    isEditFileName,
    isEditFileDescription,
    maxFile = MAX_FILE,
    minFile = MIN_FILE,
    renderElm,
    renderFiles,
    renderFile,
    typeFiles,
    axiosOptions,
    isCancelUploadingFile,
    formatFormData,
    onDownloadFile,
    renderDownloadFile,

    id,
    dataTestId
  },
  ref
) => {
  const { t } = useTranslation();
  const uploadRef = useRef(null);
  const containerRef = useRef(null);
  const refDragCounter = useRef(0);
  const refFiles = useRef<typeof files>();
  const cancelTokenFiles = useRef<Record<string, CancelTokenSource>>({});

  const [files, setFiles] = useState<StateFile[]>(filesProps || []);
  const [isDrag, setIsDrag] = useState(false);
  const [isUploading, setIsUploading] = useState(false);

  refFiles.current = files;

  const isDisabled = disabled || inputConfig?.disabled || isUploading;

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { files: newFiles } = event.target;

    if (!newFiles || newFiles.length === 0) return;
    const _newFiles = formatFiles(newFiles, { maxFile, minFile, typeFiles });

    // uncontrolled
    if (!filesProps) {
      setFiles(prevArray => [...prevArray, ..._newFiles]);
    }

    const _files = [...files, ..._newFiles];
    isFunction(onChangeFile) && onChangeFile && onChangeFile({ files: _files });

    onChange && onChange(event);
  };

  const dragEnter = (event: React.DragEvent<HTMLInputElement>) => {
    if (!multiple) return;
    event.preventDefault();

    refDragCounter.current++;
    const { items } = event?.dataTransfer;
    const hasFiles = items && items.length > 0;

    /* istanbul ignore else */
    if (multiple) {
      hasFiles && setIsDrag(true);
    } else {
      hasFiles && items.length === 1 && setIsDrag(true);
    }
    onDragEnter && onDragEnter(event);
  };

  const dragLeave = (event: React.DragEvent<HTMLInputElement>) => {
    if (!multiple) return;
    event.preventDefault();

    refDragCounter.current--;
    setIsDrag(false);
    onDragLeave && onDragLeave(event);
  };

  const handleDrop = (event: React.DragEvent<HTMLInputElement>) => {
    if (!multiple) return;
    event.preventDefault();
    event.stopPropagation();

    const { files: newFiles } = event.dataTransfer;

    const _newFiles = formatFiles(newFiles, { maxFile, minFile, typeFiles });
    // mode uncontrolled
    if (!filesProps) {
      setFiles(prevArray => [...prevArray, ..._newFiles]);
    }

    // controlled
    const _files = [...files, ..._newFiles];
    isFunction(onChangeFile) && onChangeFile && onChangeFile({ files: _files });

    refDragCounter.current = 0;
    setIsDrag(false);
    isFunction(onDrop) && onDrop && onDrop(event);
  };

  const handleUploadProgress = useCallback(
    (idx: ReactText, event: ProgressEvent<EventTarget>) => {
      const files = refFiles.current!;
      const percentage = Math.round((100 * event.loaded) / event.total);

      const file = files.find(_file => _file.idx === idx)!;

      file.percentage = percentage;
      file.status = STATUS.uploading;
      const _files = [...files];
      if (filesProps) {
        isFunction(onChangeFile) &&
          onChangeFile &&
          onChangeFile({ files: _files });
        return;
      }
      setFiles(_files);
    },
    [filesProps, onChangeFile]
  );

  const upload = useCallback(
    (_files: StateFile[], _saveUrl: typeof saveUrl) => {
      if (isString(_saveUrl)) {
        return _files.map(file => {
          const { idx } = file;
          cancelTokenFiles.current[idx] = axios.CancelToken.source();
          const axiosInstance = axios.create({
            ...axiosOptions,
            cancelToken: cancelTokenFiles.current[idx].token,
            headers: {
              'Content-Type': 'application/json',
              ...axiosOptions?.headers
            }
          });

          let formData = new FormData();
          formData.append('file', file as unknown as Blob);
          if (isFunction(formatFormData)) {
            formData = formatFormData(formData);
          }
          return axiosInstance
            .post(_saveUrl, formData, {
              headers: { 'Content-Type': 'multipart/form-data' },
              onUploadProgress: (event: ProgressEvent<EventTarget>) => {
                handleUploadProgress(idx, event);
              }
            })
            .then(() => Promise.resolve({ idx }))
            .catch(() => Promise.reject({ idx }));
        });
      }

      return _saveUrl(_files, handleUploadProgress);
    },
    [handleUploadProgress, axiosOptions, formatFormData]
  );

  const handleFileSuccess = useCallback(
    ({ idx }: { idx: string }) => {
      const _files = refFiles.current!;

      const file = _files.find(_file => _file.idx === idx)!;

      file.percentage = 0;
      file.status = STATUS.success;
      if (filesProps) {
        isFunction(onChangeFile) &&
          onChangeFile &&
          onChangeFile({ files: [..._files] });
        return;
      }
      setFiles([..._files]);
    },
    [filesProps, onChangeFile]
  );

  const handleFileFail = useCallback(
    ({ idx }: { idx: string }) => {
      const _files = refFiles.current!;

      const file = _files.find(_file => _file.idx === idx)!;

      file.percentage = 0;
      file.status = STATUS.error;
      if (filesProps) {
        isFunction(onChangeFile) &&
          onChangeFile &&
          onChangeFile({ files: [..._files] });
        return;
      }
      setFiles([..._files]);
    },
    [filesProps, onChangeFile]
  );

  const uploadFiles = useCallback(() => {
    const files = refFiles.current!;

    const validFiles = files.filter(
      file => file.status === STATUS.valid || file.status === STATUS.error
    );
    const uploadPromises = upload(validFiles, saveUrl);

    if (isEmpty(uploadPromises)) return;

    setIsUploading(true);
    uploadPromises.forEach((promise: any) => {
      promise.then(handleFileSuccess).catch(handleFileFail);
    });

    (Promise as any)
      .allSettled(uploadPromises)
      .then((result: PromiseFile[]) => {
        setIsUploading(false);
        onFinally && onFinally(result);
      });
  }, [handleFileFail, handleFileSuccess, onFinally, saveUrl, upload]);

  const handleRemoveFile = (i: number) => {
    const idx = files[i]?.idx;
    const hasCancelToken = idx && cancelTokenFiles.current[idx];

    // controlled
    if (filesProps && !isEmpty(filesProps)) {
      onRemove && onRemove(i);
      return;
    }

    // uncontrolled
    // need to rule for delete in uploading
    if (isUploading && hasCancelToken) {
      // cancel request post data
      cancelTokenFiles.current[idx].cancel();
      // reset status in file
      files[i].status = STATUS.valid;
    } else {
      files.splice(i, 1);
    }

    const _files = [...files];
    onChangeFile && isFunction(onChangeFile) && onChangeFile({ files: _files });

    setFiles(_files);
  };

  useEffect(
    () => setFiles(formatFiles(filesProps, { maxFile, minFile, typeFiles })),
    [filesProps, maxFile, minFile, typeFiles]
  );

  // custom ref public
  useImperativeHandle(
    ref,
    () => ({
      cancelTokenFiles: cancelTokenFiles.current,
      inputElement: uploadRef.current!,
      containerElement: containerRef.current!,
      uploadFiles,
      files
    }),
    [uploadFiles, files]
  );

  const viewProps = {
    files,
    onDownloadFile,
    renderDownloadFile,
    onRemoveFile: handleRemoveFile,
    classNameFile,
    className: classNameListFile,
    isEditFileName,
    isEditFileDescription,
    typeFiles,
    maxFile,
    minFile,
    isCancelUploadingFile
  };

  return (
    <div
      className={classNames('dls-upload', { single: !multiple }, className)}
      ref={containerRef}
      id={id}
      data-testid={genAmtId(dataTestId, 'dls-upload', 'Upload')}
    >
      <div
        className={classNames('action-upload', {
          active: isDrag,
          disabled: isDisabled,
          uploaded: !isEmpty(files)
        })}
      >
        <input
          ref={uploadRef}
          type="file"
          onDragEnter={dragEnter}
          onDragLeave={dragLeave}
          onDrop={handleDrop}
          onChange={handleChange}
          multiple={!!multiple}
          {...inputConfig}
          value=""
          disabled={isDisabled}
          data-testid={genAmtId(dataTestId, 'dls-upload-input', 'Upload.Input')}
        />
        <span>
          {isDrag && !isDisabled
            ? t('txt_dls_dropping_file', 'Drop your files here ...')
            : multiple
            ? t(
                'txt_dls_drag_and_drop_file',
                'Drag and drop or click to upload files'
              )
            : t('txt_dls_choose_file', 'Choose File')}
        </span>
      </div>
      {renderElm && renderElm(viewProps)}
      {renderFiles ? (
        renderFiles(viewProps)
      ) : (
        <ListFile {...viewProps} renderFile={renderFile} />
      )}
    </div>
  );
};

export * from './type';
export default React.forwardRef<UploadRef, UploadProps>(Upload);
export { ListFile as FileItems, FileItem };
