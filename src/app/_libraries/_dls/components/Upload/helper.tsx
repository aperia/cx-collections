import isArray from 'lodash.isarray';
import { nanoid } from 'nanoid';
import { getMargin, getPadding } from '../../utils';
// type
import { StateFile } from './File';

export const MAX_FILE = 999999999999;
export const MIN_FILE = 0;

export const formatBytes = (bytes = 0) => {
  if (bytes === 0) return '0 Bytes';
  const k = 1024;
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  const i = Math.floor(Math.log(bytes) / Math.log(k));
  const dm = i <= 1 ? 0 : 1;
  return `${parseFloat((bytes / Math.pow(k, i)).toFixed(dm))} ${sizes[i]}`;
};

export const fileExtensions: Record<string, string> = {
  png: 'bg-file-type-png',
  ppt: 'bg-file-type-ppt',
  docx: 'bg-file-type-docx',
  rtf: 'bg-file-type-rtf',
  xlsx: 'bg-file-type-xlsx',
  pdf: 'bg-file-type-pdf',
  bmp: 'bg-file-type-bmp',
  csv: 'bg-file-type-csv',
  doc: 'bg-file-type-doc',
  na: 'bg-file-type-na',
  msg: 'bg-file-type-msg',
  gif: 'bg-file-type-gif',
  jpg: 'bg-file-type-jpg',
  jpeg: 'bg-file-type-jpeg',
  txt: 'bg-file-type-txt',
  tiff: 'bg-file-type-tiff',
  tif: 'bg-file-type-tif',
  mp4: 'bg-file-type-mp4',
  xls: 'bg-file-type-xls'
};

export const getFileExtension = (name = '') => {
  const typeFile = name.split('.').pop()?.toLocaleLowerCase() || '';
  return fileExtensions[typeFile] || 'bg-file-type-na';
};

export const getValid = (file: StateFile, options?: Options) => {
  const { name = '', size = 0 } = file;
  const { maxFile, typeFiles, minFile } = options || {};
  const extension = (name.split('.').pop() || '').toLocaleLowerCase();
  const isTypeSupport = Boolean(
    typeFiles && isArray(typeFiles)
      ? typeFiles.map(type => type.toLocaleLowerCase()).includes(extension)
      : true
  );

  const isOverSize = Boolean(maxFile ? maxFile < size : false);
  const isUnderSize = Boolean(minFile ? minFile > size : false);

  return {
    isInvalid: Boolean(!isTypeSupport || isOverSize || isUnderSize),
    isTypeSupport,
    isOverSize,
    isUnderSize
  };
};

export interface Options {
  maxFile?: number;
  minFile?: number;
  typeFiles?: string[];
}

export const formatFiles = (files: any, options: Options) => {
  if (!files) return [];
  for (let i = 0; i < files.length; i++) {
    const file = files[i];
    if (!file.idx) {
      file.idx = nanoid();
    }
    if (file.status === undefined) {
      const { isInvalid } = getValid(file, options);
      file.status = isInvalid ? STATUS.invalid : STATUS.valid;
    }
  }
  return files;
};

export enum STATUS {
  valid,
  invalid,
  uploading,
  success,
  error
}

// helper for File View

export const getTotalMarginPadding = (
  element: HTMLElement | Element | null
) => {
  const { right: rightP = 0, left: leftP = 0 } = getPadding(element) || {};
  const { right: rightM = 0, left: leftM = 0 } = getMargin(element) || {};
  return rightM + rightP + leftM + leftP;
};

export const getAllWidthChildren = (element: HTMLElement | Element | null) => {
  if (!element) return 0;
  let total = 0;
  const elm = element as any;
  for (let i = 0; i < elm.children.length; i++) {
    const child = elm.children[i];

    total += child.offsetWidth + getTotalMarginPadding(child);
  }
  return total ? total + 1 : 0;
};
