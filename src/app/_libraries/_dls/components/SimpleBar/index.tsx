import React from 'react';

// components
import SimpleBarReact from 'simplebar-react';

export interface SimpleBarProps extends React.HTMLAttributes<HTMLElement> {
  scrollableNodeProps?: Record<string, any>;
  options?: Record<string, any>;
  autoHide?: boolean;
  forceVisible?: string;
  direction?: string;
  timeout?: number;
  clickOnTrack?: boolean;
  scrollbarMinSize?: number;
  scrollbarMaxSize?: number;
}

const SimpleBar: React.RefForwardingComponent<
  SimpleBarReact,
  SimpleBarProps
> = (props, ref) => {
  return <SimpleBarReact ref={ref} {...props} />;
};

export default React.forwardRef<SimpleBarReact, SimpleBarProps>(SimpleBar);
