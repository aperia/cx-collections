declare global {
  interface FunctionRestVoid {
    (...args: any[]): void;
  }

  interface DLSId {
    id?: string;
    dataTestId?: string;
  }
}

export {};
