class Pan {
  constructor() {}
}

class MockHammer {
  element: HTMLElement;

  constructor(element: HTMLElement) {
    this.element = element;
  }

  add() {}
  on(eventName: string, handle: Function) {
    const event = {} as any;

    const { x, y, height } = this.element?.getBoundingClientRect();

    if (x > 0) event.additionalEvent = 'panleft';
    if (y > 0) event.additionalEvent = 'panright';
    if (height > 0) event.direction = height;

    if (eventName === 'pan') handle(event);
    if (eventName === 'panend pancancel') handle();
  }
  off() {}

  static Pan = Pan;
  static DIRECTION_LEFT = 2;
  static DIRECTION_RIGHT = 4;
  static DIRECTION_UP = 8;
}

export default MockHammer;
