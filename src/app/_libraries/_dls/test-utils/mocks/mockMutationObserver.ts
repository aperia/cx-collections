const createMockMutationObserver = (records: any) => {
  (global as any).MutationObserver = class {
    constructor(callback: any) {
      (this as any).callback = callback;
    }
    disconnect() {
      return;
    }
    observe() {
      (this as any).callback(records);
    }
  };
};

const clearMockMutationObserver = () => {
  (global as any).MutationObserver = undefined;
};

export { createMockMutationObserver, clearMockMutationObserver };
