import React, { FC, useEffect, useState } from 'react';
import { CSSTransitionProps } from 'react-transition-group/CSSTransition';
import isFunction from 'lodash.isfunction';

export const elementId = 'element-id';
export const parentElementId = 'element-id';

export const CSSTransition: FC<CSSTransitionProps> = ({
  children,
  in: inProp = false,
  duration,
  onEnter,
  onExited
}) => {
  const [show, setShow] = useState(false);

  useEffect(() => {
    const parentElement = {
      id: parentElementId,
      append: () => undefined
    } as HTMLElement;

    const element = { id: elementId, parentElement } as HTMLElement;

    if (inProp) {
      setShow(true);
      onEnter && onEnter(element, false);
      onExited && onExited(element);
    } else {
      onExited && onExited(element);
      onEnter && onEnter(element, false);
    }

    const timeOut = setTimeout(() => {
      setShow(false);
    }, duration);

    return () => clearTimeout(timeOut);
  }, [duration, inProp, onEnter, onExited]);

  if (!isFunction(children) || !show) return <></>;

  return <div>{children('entered')}</div>;
};
