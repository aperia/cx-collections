import { 
  apiService,
  ServiceSingleton,
  InterceptorsInstance,
} from './api.service';

describe('test apiService', () => {
  it('should return apiService instance', () => {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    //@ts-ignore
    expect(new ServiceSingleton()).toBeTruthy();
    expect(ServiceSingleton.getInstance()).toBeTruthy();
  });

  it('should setup with custom interceptor', () => {
    const interceptors: InterceptorsInstance = {
      request: {},
      response: {},
    };

    expect(apiService.setupAxiosInterceptors?.(interceptors)).toBeUndefined();
  });
});
