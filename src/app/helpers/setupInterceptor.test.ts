import { applyMiddleware, createStore } from 'redux';
import thunk from 'redux-thunk';
import * as setupInterceptor from './setupInterceptor';
import { rootReducer } from 'storeConfig';
import { waitFor } from '@testing-library/dom';

window.appConfig = {
  commonConfig: {}
} as AppConfiguration;

describe('Test RenewToken helpers', () => {
  const mockToken =
    'eyJhbGciOiJIUzI1NiJ9.eyJlc3NVc2VyTmFtZSI6IlRlc3QgTmFtZSIsImV4cCI6MTYxNjEyNzY4MSwiYXBwSWQiOiJjeENvbGxlY3Rpb25zIiwib3BlcmF0b3JJZCI6IkZERVNNQVNUIiwiT3JnSUQiOiJGREVTIiwidXNlcklkIjoiRkRFU01BU1QiLCJuYmYiOjE2MTYxMjY3ODEsImlzcyI6Ik5TQSIsIm9jc1NoZWxsIjoiRkRFU01BU1QiLCJwcml2aWxlZ2VzIjpbIkNYQ09MMSIsIkNYQ09MNSJdLCJ1c2VyTmFtZSI6InRlc3RuYW1lIiwieDUwMElkIjoiQUFBQTExMTQiLCJlbnRpdGxlbWVudCI6W10sImdyb3VwcyI6WyJITE5TQSJdLCJzZXNzaW9uIjoiOGIwZjA4NGItMzcxNC00MTFlLWFjNmYtZTQ5ZWFjNDg0NzBkIiwiY2xpZW50SWRlbnRpZmllciI6IjUzMDIiLCJzeXN0ZW1JZGVudGlmaWVyIjoiNjM5NSIsInByaW5jaXBhbElkZW50aWZpZXIiOiIwMDAwIiwiYWdlbnRJZGVudGlmaWVyIjoiMDkwMCIsImFjY291bnRJZCI6IkZTRU5DKGV5SmpiMjVtYVdjaU9pSm1jeTV3WW1VdWEyVjVMakV1VUVKRlYwbFVTRk5JUVRJMU5rRk9SREkxTmtKSlZFRkZVeTFEUWtNdFFrTWlMQ0prWVhSaElqb2lSVTVES0Zkb1FXZHJORzlLV0dVclREQTJXVzR5TVdGRU5IVkhSelJVTmxoWWJsSkxiMUJyUzJneFUxUXZjbU42WW05VU9UbENTMDh6ZFdWNlVuVTNlbE50TTBJcEluMD0pIiwicGlpZCI6IlZPTChZVlpHV2tGdktXOUpNaTlTVTFWV2VDcFhMUT09KSIsImN1c3RvbWVyRXh0ZXJuYWxJZGVudGlmaWVyIjoiQzIwMjMyMDczMjU2NzUwMDAwOTM4NTQ4IiwicHJlc2VudGF0aW9uSWQiOiJGU0VOQyhleUpqYjI1bWFXY2lPaUptY3k1d1ltVXVhMlY1TGpFdVVFSkZWMGxVU0ZOSVFUSTFOa0ZPUkRJMU5rSkpWRUZGVXkxRFFrTXRRa01pTENKa1lYUmhJam9pUlU1REtETkxSbEowY1hGbGMwOUNXa3hqT0ZWeWMyUjFVbTVUYTNKSVJXSXdObWhxU1RWUGNHUXZSVzhyVXpGeVdqQkxRbWR4TWpScFlqbHlOM2xDZFdSdWMyd3BJbjA9KSIsImNhcmRUeXBlIjoiY29tbWVyY2lhbCIsIm1lbW9UeXBlIjoiQ2hyb25pY2xlIiwianRpIjoiYjc1ZWEzMjItMTI5Mi00YTg1LTllYTMtMjIxMjkyOGE4NTQxIn0.BlJcYCeaYEqvjqL31PdL8-1G1wTf49tmCEdsdtYiBtM';

  const mockReturnValue = {
    time: 2216134669052,
    token: mockToken
  };

  const realUrl = 'fs/collectionsAccounts/v1/accountSearch';
  const mockUrl = 'mockApi/account/externalStatusRefData';

  describe('Test getCurrentTabTokenAppThunk', () => {
    it('Should return account token based store id ', () => {
      const { dispatch } = createStore(
        rootReducer,
        {
          tab: {
            tabStoreIdSelected: '2216134669052',
            tabs: [
              {
                storeId: '2216134669052',
                tabType: 'accountDetail'
              }
            ]
          },
          renewToken: {
            ['2216134669052']: {
              token: mockReturnValue.token
            }
          }
        },
        applyMiddleware(thunk)
      );
      const token = dispatch(setupInterceptor.getCurrentTabTokenAppThunk());

      expect(token).toEqual(mockReturnValue.token);
    });

    it('Should return account token based store id with renew token i', () => {
      const { dispatch } = createStore(
        rootReducer,
        {
          tab: {
            tabStoreIdSelected: '2216134669052',
            tabs: [
              {
                storeId: '2216134669052',
                tabType: 'accountDetail'
              }
            ]
          },
          renewToken: {
            ['2216134669052']: {
              token: ''
            }
          }
        },
        applyMiddleware(thunk)
      );
      const token = dispatch(setupInterceptor.getCurrentTabTokenAppThunk());

      expect(token).toEqual('');
    });

    it('Should return account token based store id with tabType is empty string ', () => {
      const { dispatch } = createStore(
        rootReducer,
        {
          tab: {
            tabStoreIdSelected: '2216134669052',
            tabs: [
              {
                storeId: '2216134669052',
                tabType: undefined
              }
            ]
          },
          renewToken: {
            ['2216134669052']: {
              token: mockReturnValue.token
            }
          }
        },
        applyMiddleware(thunk)
      );
      const token = dispatch(setupInterceptor.getCurrentTabTokenAppThunk());

      expect(token).toEqual('');
    });
  });
  describe('Test isMockAPI', () => {
    it('Should return false when url is not mock api', () => {
      const status = setupInterceptor.isMockAPI(realUrl);

      expect(status).toBe(false);
    });

    it('Should return true when url is mock api', () => {
      const status = setupInterceptor.isMockAPI(mockUrl);

      expect(status).toBe(true);
    });
  });

  describe('Test handleResponseSuccess ', () => {
    it('Should return resolve promise', () => {
      const mockData: any = {};

      const data = setupInterceptor.handleResponseSuccess(mockData);
      expect(data).toEqual(Promise.resolve(mockData));
    });
  });

  describe('Test handleResponseFailure  ', () => {
    it('Should return reject promise > error is object', () => {
      const error: any = { response: 'error' };

      waitFor(() => {
        let errResponse;

        setupInterceptor.handleResponseFailure(error).catch(err => {
          errResponse = err;
        });

        expect(errResponse).toEqual(error.response);
      });
    });

    it('Should return reject promise > error is string', () => {
      const error: any = 'error';

      waitFor(() => {
        let errResponse;

        setupInterceptor.handleResponseFailure(error).catch(err => {
          errResponse = err;
        });

        expect(errResponse).toEqual(error.response);
      });
    });
  });

  describe('Test handleRequestFailure', () => {
    it('Should return reject promise', () => {
      const error: any = { request: 'error' };

      waitFor(() => {
        let errResponse;

        setupInterceptor.handleRequestFailure(error).catch(err => {
          errResponse = err;
        });

        expect(errResponse).toEqual('error.request');
      });
    });
  });

  describe('Test handleRequestSuccess  ', () => {
    it('Should be work with isGettingRefData', () => {
      const mockData: any = {
        headers: {
          Authorization: ''
        },
        url: 'refData 123'
      };

      const { dispatch } = createStore(rootReducer, {}, applyMiddleware(thunk));

      const data = setupInterceptor.handleRequestSuccess.bind(
        { dispatch } as any,
        mockData
      )();
      expect(data).toEqual(Promise.resolve(mockData));
    });

    it('Should return resolve promise with mock url', () => {
      const mockData: any = {
        headers: {
          Authorization: ''
        },
        url: mockUrl
      };

      const { dispatch } = createStore(rootReducer, {}, applyMiddleware(thunk));

      const data = setupInterceptor.handleRequestSuccess.bind(
        { dispatch } as any,
        mockData
      )();
      expect(data).toEqual(Promise.resolve(mockData));
    });

    it('Should return resolve promise with real api', () => {
      const mockData: any = {
        headers: {
          Authorization: ''
        },
        url: realUrl
      };

      const { dispatch } = createStore(rootReducer, {}, applyMiddleware(thunk));

      const data = setupInterceptor.handleRequestSuccess.bind(
        { dispatch } as any,
        mockData
      )();
      expect(data).toEqual(Promise.resolve(mockData));
    });

    it('Should return resolve promise with undefined url', () => {
      const mockData: any = {
        headers: {
          Authorization: ''
        }
      };

      const { dispatch } = createStore(rootReducer, {}, applyMiddleware(thunk));

      const data = setupInterceptor.handleRequestSuccess.bind(
        { dispatch } as any,
        mockData
      )();
      expect(data).toEqual(Promise.resolve(mockData));
    });

    it('getMockApiUrl > NODE_ENV is development', () => {
      process.env = Object.assign(process.env, {
        NODE_ENV: 'development'
      });
      const res = setupInterceptor.getMockApiUrl();
      expect(res).toEqual('');
    });
  });
});
