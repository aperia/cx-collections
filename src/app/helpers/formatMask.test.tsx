import {
  formatMaskNumber,
  renderMaskedValue,
  convertMaskAccount,
  maskAccount
} from './formatMask';

describe('Test formatMask.test.ts', () => {
  it('formatMaskNumber', () => {
    const firstNumber = 100;
    const lastNumber = 3;
    const mask = '.testStringCharacter';
    const value = `${firstNumber}${mask}${lastNumber}`;

    let result = formatMaskNumber(value);
    expect(result!.firstText).toEqual(`${firstNumber}`);
    expect(result!.lastText).toEqual(`${lastNumber}`);
    expect(result!.maskText!.length).toEqual(mask.length);
    expect(formatMaskNumber('')).toEqual(undefined);

    result = formatMaskNumber(`${firstNumber}`);
    expect(result!.firstText).toEqual(`${firstNumber}`);
    expect(result!.lastText).toEqual(undefined);
    expect(result!.maskText).toEqual(undefined);
    expect(formatMaskNumber('')).toEqual(undefined);
  });

  it('formatMaskNumber with max dot', () => {
    const firstNumber = 100;
    const lastNumber = 3;
    const mask = '.testStringCharacter';
    const value = `${firstNumber}${mask}${lastNumber}`;

    let result = formatMaskNumber(value, 2);
    expect(result!.firstText).toEqual(`${firstNumber}`);
    expect(result!.lastText).toEqual(`${lastNumber}`);
    expect(result!.maskText!.length).toEqual(2);

    result = formatMaskNumber(value, 22);
    expect(result!.firstText).toEqual(`${firstNumber}`);
    expect(result!.lastText).toEqual(`${lastNumber}`);
    expect(result!.maskText!.length).toEqual(20);
  });

  it('renderMaskedValue', () => {
    const result = renderMaskedValue('value');
    expect(result.props.className).toEqual('data-value');
  });

  it('convertMaskAccount', () => {
    const result = convertMaskAccount('123***');
    expect(result).toEqual('123•••');
  });

  it('convertMaskAccount', () => {
    const result = convertMaskAccount();
    expect(result).toEqual('');
  });

  it('maskAccount', () => {
    const result = maskAccount('022009001033000');
    expect(result).toEqual('022009•••••3000');
  });

  it('maskAccount', () => {
    const result = maskAccount();
    expect(result).toEqual('');
  });

});
