export const getGroupFormatInputText = (
  itemsSelected: number,
  itemsLength: number,
  value?: any,
  textField?: string
) => {
  if (itemsLength === itemsSelected) return 'txt_all_items_selected';
  if (itemsSelected === 1) {
    if (textField) return (value[0] as MagicKeyValue)[textField];
    return value;
  }
  if (itemsSelected === 0) return '';
  return 'txt_of_selected';
};
