import React from 'react';
import { getScrollParent } from './getScrollParent';
import { render } from '@testing-library/react';

describe('Test getScrollParent', () => {
  it('Without node', () => {
    const result = getScrollParent(null);

    expect(result).toBeNull();
  });

  it('With div tag', () => {
    const wrapper = render(<div style={{ overflow: 'auto' }}>text</div>);
    const result = getScrollParent(wrapper.container);

    expect(result?.nodeName).toEqual('BODY');
  });

  it('With div tag and isScroll parent', () => {
    Object.defineProperty(window, 'getComputedStyle', {
      value: () => ({
        overflowY: 'auto',
        overflow: 'auto',
        overflowX: 'scroll'
      })
    });
    const wrapper = render(
      <div style={{ overflow: 'auto', overflowX: 'scroll', overflowY: 'auto' }}>
        text
      </div>
    );
    const result = getScrollParent(wrapper.container);
    expect(result?.nodeName).toEqual('DIV');
  });

  it('With html tag', () => {
    const htmlNode = document.createElement('html');
    const result = getScrollParent(htmlNode);

    expect(result?.nodeName).toEqual('BODY');
  });
});
