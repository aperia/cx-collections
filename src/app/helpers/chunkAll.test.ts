import {chunksAll} from './chunkAll'

describe('test chunkAll', () => {
    it('return data correctly', () => {
        const searchWord = /abc/;
        const parentString = 'test abc def';
        const data = chunksAll({searchWord,parentString, regexOption: undefined} );
        expect(data).toEqual([{start: 5, end: 8}])
    })

    it('return data correctly with no data', () => {
        const searchWord = 'abc';
        const parentString = 'test ttt def';
        const data = chunksAll({searchWord,parentString, regexOption: undefined} );
        expect(data).toEqual([])
    })
})