const isScrollParent = (element: HTMLElement): boolean => {
  const { overflow, overflowX, overflowY } = window.getComputedStyle(element);
  return /auto|scroll|overlay/.test(overflow + overflowY + overflowX);
};

const isHTMLElement = (node: Node): boolean => {
  return node instanceof Element;
};

export const getScrollParent = (
  node: HTMLElement | null
): HTMLElement | null => {
  if (!node) return null;
  if (['html', 'body', '#document'].indexOf(node.nodeName.toLowerCase()) >= 0) {
    return node.ownerDocument.body;
  }
  if (isHTMLElement(node) && isScrollParent(node as HTMLElement)) {
    return node as HTMLElement;
  }
  return getScrollParent(node.parentElement);
};
