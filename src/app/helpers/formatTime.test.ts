import { FormatTime } from 'app/constants/enums';
import {
  convertAPIDateToView,
  formatTime,
  isToday,
  isYesterday,
  convertToGMTTimes,
  formatDateToSubmit,
  getFormattedDate,
  timeSplit,
  combineDateAndTime,
  // common function
  formatTimeDefault,
  formatTimeConvert,
  convertUTCDateToLocalDate,
  parseDateWithoutTimezone,
  getStartOfDay,
  getEndOfDay,
  prepareDateStringFormat,
  convertToDD_MM_YYYY
} from './formatTime';

describe('Test formatTime.test.ts', () => {
  describe('isToday function ', () => {
    it('string newDate', () => {
      const value = new Date().toString();
      const result = isToday(value);

      expect(result).toBeTruthy();
    });
    it('string order date not is today', () => {
      const value = '11/11/2000';
      const result = isToday(value);

      expect(result).toBeFalsy();
    });
  });

  describe('convertAPIDateToView function ', () => {
    it('string newDate', () => {
      const value = '2020-12-10';
      const result = convertAPIDateToView(value);

      expect(result).toEqual('12/10/2020');
    });

    it('format value not match', () => {
      const value = '2020/12/10';
      const result = convertAPIDateToView(value);

      expect(result).toEqual('2020/12/10');
    });

    it('param is invalid format', () => {
      const inputFormat = '';
      const outputFormat = '';
      let result = convertAPIDateToView('000000', inputFormat);
      expect(result).toEqual('');

      result = convertAPIDateToView('2020-12-13', undefined, outputFormat);
      expect(result).toEqual('2020/12/13');

      result = convertAPIDateToView('00-00-00', undefined, outputFormat);
      expect(result).toEqual('00/00/00');

      result = convertAPIDateToView('', inputFormat, outputFormat);
      expect(result).toEqual('');
    });
  });

  describe('isYesterday function ', () => {
    it('string yesterday', () => {
      const value = new Date();
      value.setDate(value.getDate() - 1);
      const result = isYesterday(value.toString());

      expect(result).toBeTruthy();
    });
    it('string order date not is yesterday', () => {
      const value = '11/11/2000';
      const result = isYesterday(value);

      expect(result).toBeFalsy();
    });
  });

  describe('convertUTCDateToLocalDate function', () => {
    it('string order date not is yesterday', () => {
      const date = new Date();
      const expected = new Date(
        date.getTime() + date.getTimezoneOffset() * 60000
      );

      const result = convertUTCDateToLocalDate(date);
      expect(result).toEqual(expected);
    });
  });

  describe('parseDateWithoutTimezone function', () => {
    it('string order date not is yesterday', () => {
      const date = new Date();
      const expected = new Date(convertUTCDateToLocalDate(date)).toISOString();

      const result = parseDateWithoutTimezone(date);
      expect(result).toEqual(expected);
    });
  });

  describe('convertToGMTTimes function ', () => {
    it('date time 11/11/2000', () => {
      const value = new Date('11/11/2000 09:09:09');
      const result = convertToGMTTimes(value);

      expect(result.getHours()).toEqual(0);
      expect(result.getMinutes()).toEqual(0);
      expect(result.getSeconds()).toEqual(0);
      expect(result.getMilliseconds()).toEqual(0);
    });
  });

  describe('formatTime function ', () => {
    it('yearMonthDayWithHyphen', () => {
      const value = new Date('10/10/2020').toString();
      const result = formatTime(value).yearMonthDayWithHyphen;
      expect(result).toEqual('2020-10-10');
    });

    it('shortTimeMeridiem', () => {
      const value = new Date('10/10/2020').toString();
      const result = formatTime(value).shortTimeMeridiem;
      expect(result).toEqual('12:00 AM');
    });

    it('utcDate', () => {
      const value = new Date('10/10/2020').toString();
      const result = formatTime(value).utcDate;
      expect(result).toEqual('2020-10-10');
    });

    it('dateWithHyphen', () => {
      const value = new Date('10/10/2020').toString();
      const result = formatTime(value).dateWithHyphen();
      expect(result).toEqual('10-10-2020');
    });

    it('shortYearShortMonthShortDate', () => {
      const value = new Date('10/10/2020').toString();
      const result = formatTime(value).shortYearShortMonthShortDate;
      expect(result).toEqual('201010');
    });

    it('format year', () => {
      const value = new Date().toString();
      const result = formatTime(value).year;

      expect(result).toEqual(formatTimeDefault(value, FormatTime.Year));
    });

    it('format date', () => {
      const value = new Date().toString();
      const result = formatTime(value).date;

      expect(result).toEqual(formatTimeDefault(value, FormatTime.Date));
    });

    it('format datetime', () => {
      const value = new Date().toString();
      const result = formatTime(value).datetime;

      expect(result).toEqual(formatTimeDefault(value, FormatTime.Datetime));
    });

    it('format monthDate', () => {
      const value = new Date().toString();
      const result = formatTime(value).monthDate;

      expect(result).toEqual(formatTimeDefault(value, FormatTime.MonthDate));
    });

    it('format shortYearMonth', () => {
      const value = new Date().toString();
      const result = formatTime(value).shortYearMonth;

      expect(result).toEqual(
        formatTimeDefault(value, FormatTime.ShortYearMonth)
      );
    });

    it('format shortMonthShortYear', () => {
      const value = new Date().toString();
      const result = formatTime(value).shortMonthShortYear;

      expect(result).toEqual(
        formatTimeDefault(value, FormatTime.ShortMonthShortYear)
      );
    });

    it('format allDatetime', () => {
      const value = new Date().toString();
      const result = formatTime(value).allDatetime;

      expect(result).toEqual(formatTimeDefault(value, FormatTime.AllDatetime));
    });

    it('format fullTimeMeridiem', () => {
      const value = new Date().toString();
      const result = formatTime(value).fullTimeMeridiem;

      expect(result).toEqual(
        formatTimeDefault(value, FormatTime.FullTimeMeridiem)
      );
    });

    it('format monthYear', () => {
      const value = new Date().toString();
      const result = formatTime(value).monthYear;

      expect(result).toEqual(formatTimeDefault(value, FormatTime.MonthYear));
    });

    it('format shortMonthYear', () => {
      const value = new Date().toString();
      const result = formatTime(value).shortMonthYear;

      expect(result).toEqual(
        formatTimeDefault(value, FormatTime.ShortMonthYear)
      );
    });

    it('format fullYearMonthDay', () => {
      const value = new Date().toString();
      const result = formatTime(value).fullYearMonthDay;

      expect(result).toEqual(
        formatTimeDefault(value, FormatTime.FullYearMonthDay)
      );
    });

    it('format fullTime', () => {
      const value = new Date().toString();
      const result = formatTime(value).fullTime;

      expect(result).toEqual(result);
    });

    it('format convertLocalDateToUTCDate', () => {
      const value = '2020/10/01';
      const result = formatTime(value).shortMonthYear;

      expect(result).toEqual(
        formatTimeDefault(value, FormatTime.ShortMonthYear)
      );
    });

    it('format allDateTimeToUTC', () => {
      const value = new Date().toString();
      const result = formatTime(value).allDateTimeToUTC();
      const resultExpect = formatTimeConvert(value, FormatTime.AllDatetime)();

      expect(result).toEqual(resultExpect);
    });

    it('format fullTimeMeridiemToUTC', () => {
      const value = '01/26/2021 03:20:46 PM';
      const result = formatTime(value).fullTimeMeridiemToUTC();
      const resultExpect = formatTimeConvert(
        value,
        FormatTime.FullTimeMeridiem
      )();

      expect(result).toEqual(resultExpect);
    });

    it('format allDateTimeToUTC convert local', () => {
      const value = '01/26/2021';
      const result = formatTime(value).fullTimeMeridiemToUTC();
      const expected = formatTimeConvert(
        value,
        FormatTime.FullTimeMeridiem,
        false,
        true
      )();

      expect(result).toEqual(expected);
    });
  });

  describe('formatTimeConvert function ', () => {
    it('formatTimeConvert with time', () => {
      const date = '01/26/2021';
      const expected = formatTimeDefault(`${date}Z`, FormatTime.AllDatetime);
      let result = formatTimeConvert(date, FormatTime.AllDatetime, true)();
      expect(result).toEqual(expected);

      result = formatTimeConvert('', FormatTime.AllDatetime, true)();
      expect(result).toBeUndefined();
    });
  });

  describe('formatTimeDefault function ', () => {
    it('string order format not string date time', () => {
      const value = 'test order format';
      const format = FormatTime.Date;
      const result = formatTimeDefault(value.toString(), format);
      expect(result).toBeUndefined();
    });

    it('Empty date', () => {
      const format = FormatTime.Date;
      const result = formatTimeDefault('', format);
      expect(result).toBeUndefined();
    });

    it('Invalid date', () => {
      const format = FormatTime.Date;
      const result = formatTimeDefault('20/20//2200', format);
      expect(result).toBeUndefined();
    });

    it('Date invalid', () => {
      const format = FormatTime.Date;
      const result = formatTimeDefault(new Date('12062020'), format);

      expect(result).toBeUndefined();
    });

    it('Date string mm-yyyy', () => {
      const result = formatTimeDefault('06-2020', FormatTime.ShortMonthYear);
      expect(result).toEqual('06/2020');
    });

    it('Date string mmddyyyy', () => {
      const result = formatTimeDefault(
        '12222020',
        FormatTime.Date,
        FormatTime.UTCDate
      );
      expect(result).toEqual('12/22/2020');
    });

    it('format date have four number', () => {
      const value = '1229';

      expect('12/29').toEqual(formatTimeDefault(value, FormatTime.MonthDate));
    });

    it('format date have four number with special character', () => {
      const value = '12-29';

      expect('12/29').toEqual(formatTimeDefault(value, FormatTime.MonthDate));
    });
  });

  describe('formatDateToSubmit', () => {
    it('should be return date is empty string', () => {
      expect(formatDateToSubmit('')).toEqual('00000000');
    });

    it('should be return valid date', () => {
      expect(formatDateToSubmit(new Date('10/10/2020'))).toEqual('20201010');
      expect(formatDateToSubmit(new Date('10/01/2020'))).toEqual('20201001');
      expect(formatDateToSubmit(new Date('01/01/2020'))).toEqual('20200101');
    });

    it('should be return valid date (dd/mm/yyyyy)', () => {
      expect(formatDateToSubmit('10/10/2020')).toEqual('20201010');
    });

    it('should be return valid date include GMT', () => {
      expect(formatDateToSubmit(new Date('10/10/2010').toString())).toEqual(
        '20101010'
      );
    });

    it('should be return valid date (dd-mm-yyyy) => (ddmmyyyyy)', () => {
      expect(formatDateToSubmit('20-10-2020')).toEqual('20102020');
    });

    it('should be return valid date with date.now', () => {
      expect(formatDateToSubmit('Month April 2012')).toEqual('20120401');
    });

    it('should be return undefined', () => {
      expect(formatDateToSubmit('2020/32/32')).toEqual(undefined);
    });
  });

  describe('getFormattedDate', () => {
    it('it should return data correctly with arg is falsy', () => {
      expect(getFormattedDate(null)).toEqual('00000000');
    });

    it('it should return data correctly with arg is string', () => {
      expect(getFormattedDate('10/10/2010')).toEqual('10/10/2010');
    });

    it('it should return data correctly with arg is date', () => {
      const dateObject = new Date('10/10/2010');
      expect(getFormattedDate(dateObject)).toEqual(dateObject.toISOString());
    });
  });

  describe('timeSplit', () => {
    it('it should be return empty string', () => {
      expect(timeSplit('')).toEqual('');
      expect(timeSplit('2222')).toEqual('');
    });
    it('it should be return data correctly', () => {
      expect(timeSplit('03:07:33')).toEqual('03:07:33');
      expect(timeSplit('030733')).toEqual('03:07:33');
    });
  });

  describe('combineDateAndTime', () => {
    it('it should be return empty string', () => {
      expect(combineDateAndTime('')).toEqual(undefined);
    });
    it('it should be return data correctly', () => {
      const newDateString = '10/10/2020';
      const newTimeString = '202020';

      const newDateResult = new Date(newDateString);
      newDateResult.setHours(20, 20, 20);
      expect(combineDateAndTime(newDateString, newTimeString)).toEqual(
        newDateResult
      );
    });
  });

  describe('get start/end of day', () => {
    it('getStartOfDay > should return empty when date is undefined', () => {
      expect(getStartOfDay(undefined)).toEqual('');
    });
    it('getStartOfDay > should return start date when date has value', () => {
      const newDateResult = new Date('01-01-2022');
      expect(getStartOfDay(newDateResult, FormatTime.UTCAllDatetime)).toContain(
        '00:00:00'
      );
    });
    it('getEndOfDay > should return empty when date is undefined', () => {
      expect(getEndOfDay(undefined)).toEqual('');
    });
    it('getEndOfDay > should return end dáte when date has value', () => {
      const newDateResult = new Date('01-01-2022');
      expect(getEndOfDay(newDateResult, FormatTime.UTCAllDatetime)).toContain(
        '23:59:59'
      );
    });
  });

  describe('prepareDateStringFormat', () => {
    it('invalid date string', () => {
      const result = prepareDateStringFormat('12-22');
      expect(result).toEqual('');
    });

    it('Date string mm-dd-yyyy', () => {
      const result = prepareDateStringFormat('12-22-2020');
      expect(result).toEqual('12-22-2020');
    });

    it('Date string dd-mm-yyyy', () => {
      const result = prepareDateStringFormat('22-12-2020');
      expect(result).toEqual('12-22-2020');
    });

    it('Date string yyyy-mm-dd', () => {
      const result = prepareDateStringFormat('0001-09-26');
      expect(result).toEqual('09-26-0001');
    });

    it('Date string mm-dd-yyyy should return yyyy-mm-dd format if outputFormat is YYYY_MM_DD', () => {
      const result = prepareDateStringFormat('05-31-2020', 'YYYY_MM_DD');
      expect(result).toEqual('2020-05-31');
    });

    it('Date string dd-mm-yyyy should return yyyy-mm-dd format if outputFormat is YYYY_MM_DD', () => {
      const result = prepareDateStringFormat('31-05-2020', 'YYYY_MM_DD');
      expect(result).toEqual('2020-05-31');
    });

    it('Date string yyyy-mm-dd should return same format if outputFormat is YYYY_MM_DD', () => {
      const result = prepareDateStringFormat('2020-05-31', 'YYYY_MM_DD');
      expect(result).toEqual('2020-05-31');
    });
  });

  describe('convertToDD_MM_YYYY', () => {
    it('Date string dd-mm-yyyy', () => {
      const result = convertToDD_MM_YYYY('26-09-2020');
      expect(result).toEqual('26-09-2020');
    });

    it('Date string dd-yyyy-mm', () => {
      const result = convertToDD_MM_YYYY('26-2020-09');
      expect(result).toEqual('26-09-2020');
    });

    it('Date string dd/yyyy/mm', () => {
      const result = convertToDD_MM_YYYY('26/2020/09');
      expect(result).toEqual('26/09/2020');
    });
  });
});
