import { convertStringToNumberOnly } from './convertCommon';

describe('Test convertCommon.test.ts', () => {
  describe('convertStringToNumberOnly function ', () => {
    it('convert string to string only number ', () => {
      const value = 'test number 100%';

      const result = convertStringToNumberOnly(value);
      expect(result).toEqual(`100`);
    });
  });
});
