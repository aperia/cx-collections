import {
  formatAPICurrency,
  formatCommon,
  formatDecimalView,
  formatDisplayDate,
  formatPercent,
  mapStringAmountToPost
} from './formatCommon';

jest.mock('app/helpers/formatContactPhone', () => ({
  formatContactPhone: jest.fn()
}));

jest.mock('app/helpers/formatTime', () => ({ formatTime: jest.fn() }));

describe('formatDisplayDate', () => {
  it('should be return date string with format MM/DD/YYYY', () => {
    expect(formatDisplayDate('09-18-2008')).toEqual('09/18/2008');
  });
});

describe('mapStringAmountToPost', () => {
  it('should be return correct value', () => {
    expect(mapStringAmountToPost('100')).toEqual(10000);
  });
  it('should be return correct value', () => {
    expect(mapStringAmountToPost('100', 3)).toEqual(100000);
  });
});

describe('Test formatCommon.test.ts', () => {
  describe('formatCommon function ', () => {
    it('input invalid', () => {
      const value: any = { toString: '' };

      const format = formatCommon(value);

      expect(format.number).toEqual(0);
      expect(format.percent).toEqual(null);
      expect(format.phone).toBeUndefined();
      expect(format.quantity).toEqual('');
      expect(format.currency()).toEqual('$0.00');
      expect(format.time).toBeUndefined();
      expect(format.cardNumber).toEqual('');
    });

    it('format number', () => {
      const format = formatCommon(123456);
      expect(format.number).toEqual(123456);
    });

    it('format number invalid', () => {
      const format = formatCommon('aa');
      expect(format.number).toEqual('aa');
    });

    it('format quantity', () => {
      const format = formatCommon('123456');
      expect(format.quantity).toEqual('123,456');
    });

    it('format quantity without number', () => {
      const format = formatCommon('aa');
      expect(format.quantity).toEqual('aa');
    });

    it('format currency value is string number > 0', () => {
      const format = formatCommon('123456');
      const result = format.currency();
      expect(result).toEqual('$123,456.00');
      expect(result.indexOf('$') >= 0).toBeTruthy();
      expect(result.indexOf('.') >= 0).toBeTruthy();
    });

    it('format currency value is string number < 0', () => {
      const format = formatCommon('-123456');
      const result = format.currency();
      expect(result).toEqual('($123,456.00)');
      expect(result.indexOf('$') >= 0).toBeTruthy();
      expect(result.indexOf('.') >= 0).toBeTruthy();
      expect(result.indexOf('(') >= 0).toBeTruthy();
      expect(result.indexOf(')') >= 0).toBeTruthy();
    });

    it('format currency invalid', () => {
      const format = formatCommon('aa');
      expect(format.currency()).toEqual('aa');
    });

    it('formatDecimalView', () => {
      const result = formatDecimalView('1', 2);
      const result1 = formatDecimalView('10', 2);
      const result2 = formatDecimalView('1000', 2);

      expect(result).toEqual('10');
      expect(result1).toEqual('10');
      expect(result2).toEqual('1000');
    });

    it('format API Currency without decimal', () => {
      const result = formatAPICurrency('-123456');

      expect(result).toEqual('($123,456.00)');
    });

    it('format API Currency with decimal', () => {
      const result = formatAPICurrency('-123456.1234');

      expect(result).toEqual('($123,456.12)');
    });

    it('format API Currency with data is invalid', () => {
      const result = formatAPICurrency('');

      expect(result).toEqual('');
      expect(formatAPICurrency()).toEqual('');
    });

    it('format API Currency with decimalIs0 === 0', () => {
      const result = formatAPICurrency('1000.0');

      expect(result).toEqual('$1,000.00');
    });

    it('format API Currency with default result', () => {
      const result = formatAPICurrency('1000.200', 2);

      expect(result).toEqual('$1,000.20');
    });

    it('format percent', () => {
      const format = formatCommon('99');
      const result = format.percent;
      expect(result).toEqual('99.00%');
    });

    it('format cardNumber', () => {
      const format = formatCommon('12345678901234');
      expect(format.cardNumber).toEqual('12-3456-7890-1234');
      expect(format.cardNumber).toEqual('12-3456-7890-1234');
    });

    it('format cardNumber without number', () => {
      const format = formatCommon('aa');
      expect(format.cardNumber).toEqual('aa');
    });
  });

  describe('formatPercent', () => {
    it('should be return data without format', () => {
      const data = formatPercent('aa');
      expect(data).toEqual('aa');
    });

    it('should be return data with fractionDigits = null', () => {
      const data = formatPercent('100', null);
      expect(data).toEqual('100%');
    });

    it('should be return data correctly', () => {
      expect(formatPercent('100')).toEqual('100.00%');
      expect(formatPercent('50.22', 3)).toEqual('50.220%');
    });
  });
});
