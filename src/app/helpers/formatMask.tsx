import React, { Fragment } from 'react';
import findIndex from 'lodash.findindex';
import findLastIndex from 'lodash.findlastindex';
import isEmpty from 'lodash.isempty';

export const maskAccount = (accNumber?: string): string => {
  if (!accNumber) return '';
  return accNumber.replace(/(?<=\d{6})\d+(?=\d{4})/, (match: string) => {
    const dot = '•';
    return Array.from(dot.repeat(match.length)).join('');
  });
};

export const convertMaskAccount = (str?: string) =>
  str?.replace(/\*/gi, '•') || '';

export const formatMaskNumber = (val: string, maxDot?: number) => {
  if (isEmpty(val)) {
    return;
  }

  const arrNumber = val.split('');
  const firstIndex = findIndex(arrNumber, number =>
    isNaN(parseInt(number, 10))
  );
  const lastIndex = findLastIndex(arrNumber, number =>
    isNaN(parseInt(number, 10))
  );
  let maskText = '·';
  let lengthDot = lastIndex - firstIndex;

  if (maxDot) {
    lengthDot = lengthDot < maxDot ? lengthDot : maxDot - 1;
  }

  for (let i = 0; i < lengthDot; i++) {
    maskText = maskText + '·';
  }
  if (lastIndex === firstIndex) {
    return {
      firstText: val,
      lastText: undefined,
      maskText: undefined
    };
  }
  return {
    firstText: val.slice(0, firstIndex),
    lastText: val.slice(lastIndex + 1, val.length),
    maskText
  };
};

export const renderMaskedValue = (val: string) => {
  const arrVal = val.split('');
  const dotText = val.match(/•/g);
  const dots = new Array(dotText?.length || 5).fill(0);
  const firstDotIndex = arrVal.findIndex(item => isNaN(parseInt(item)));
  const lastDotIndex = findLastIndex(arrVal, item => isNaN(parseInt(item)));

  return (
    <div className="data-value">
      <span className="custom-account-number">
        {val.slice(0, firstDotIndex)}
        <span className="hide-account-icon">
          {dots.map((_, index) => {
            return <Fragment key={index}>&middot;</Fragment>;
          })}
        </span>
        {val.slice(lastDotIndex + 1, val.length)}
      </span>
    </div>
  );
};
