import { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';

// Config & Type
import { RenewTokenConfig } from 'pages/__commons/RenewToken/types';
import { ServiceSingletonType } from 'app/utils/api.service';
import { TabTypeComponent } from 'pages/__commons/TabBar/types';

// Const
import { userSessionWarningActions } from 'pages/UserSessionWarning/_redux/reducer';
import { SLASH, USER_TOKEN_ID } from 'app/constants';

export const mockAPIRegex = /mockApi/i;

export const TAB_TYPE_ACCEPTATION: Array<TabTypeComponent> = [
  'accountDetail',
  'accountRelated'
];

export const isMockAPI = (url: string) => {
  return mockAPIRegex.test(url);
};

const isGettingRefData = (url: string) => {
  return /refData/i.test(url);
};

export const handleResponseSuccess = (
  response: AxiosResponse
): Promise<AxiosResponse> => {
  return Promise.resolve(response);
};

export const handleResponseFailure = (
  error: AxiosError
): Promise<AxiosError> => {
  return Promise.reject({
    errorMessage: error.message,
    response: error.response || error
  });
};

export const getDefaultTokenBasedOnHostName: AppThunk<string> =
  () => (_, getState) => {
    const { renewToken } = getState();
    const { token = '' } = renewToken[USER_TOKEN_ID] || {};

    return token;
  };

export const getCurrentTabTokenAppThunk: AppThunk<string> =
  () => (dispatch, getState) => {
    const { tab, renewToken } = getState();
    const userToken = dispatch<string>(getDefaultTokenBasedOnHostName());
    const tabData = tab.tabs.find(
      item => item?.storeId === tab?.tabStoreIdSelected
    );
    const storeId = tab?.tabStoreIdSelected;

    if (tabData && !TAB_TYPE_ACCEPTATION.includes(tabData?.tabType)) {
      return userToken;
    }

    return renewToken[storeId]?.token || userToken;
  };

export const getMockApiUrl = (): string => {
  const { commonConfig } = window.appConfig;
  if (process.env.NODE_ENV === 'development') {
    return process.env.REACT_APP_MOCK_API || '';
  }
  return commonConfig?.mockAPI || commonConfig.defaultMockAPI;
};

export function handleRequestSuccess(
  this: ServiceSingletonType,
  config: RenewTokenConfig
): Promise<AxiosRequestConfig> {
  const {
    url = '',
    headers: { Authorization: existingAuthorization }
  } = config;

  const { commonConfig } = window.appConfig;

  if (isMockAPI(url)) {
    config.baseURL = getMockApiUrl();
  } else if (isGettingRefData(url)) {
    config.baseURL = process.env.PUBLIC_URL || SLASH;
  } else {
    config.baseURL = commonConfig?.realAPI;
  }

  config.headers['Authorization'] =
    existingAuthorization ||
    this.dispatch<ReturnType<any>>(getCurrentTabTokenAppThunk());

  // check url is not equal renewToken -> reset idle time
  if (config.url !== window.appConfig.api?.common?.renewToken) {
    this.dispatch(userSessionWarningActions.setEndSessionTime());
  }

  return Promise.resolve(config);
}

export const handleRequestFailure = (error: AxiosError): Promise<Error> => {
  return Promise.reject(error.request);
};
