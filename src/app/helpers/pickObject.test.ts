import { pickNestObjectKey } from './pickObject';

describe('test pickNestObjectKey', () => {
  it('should be return data correctly', () => {
    const sampleInput = { address: { isCity: false }, fullName: {} };
    const result = pickNestObjectKey(sampleInput, ['isCity']);
    expect(result).toEqual({ address: { isCity: false } });
  });

  it('should be return empty object', () => {
    const sampleInput = null as unknown as Record<string, MagicKeyValue>;
    const result = pickNestObjectKey(sampleInput, ['isCity']);
    expect(result).toEqual({});
  });
});
