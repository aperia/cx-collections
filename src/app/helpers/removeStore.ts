// Type
import { TabTypeComponent } from 'pages/__commons/TabBar/types';

// Redux
import { searchResultsAction } from 'pages/AccountSearch/AccountSearchList/_redux/reducers';
import { accountDetailSnapshotActions } from 'pages/AccountDetails/AccountDetailSnapshot/_redux';
import { accountCollection } from 'pages/AccountDetails/AccountCollection/_redux';
import { accountInformation } from 'pages/AccountDetails/AccountInformation/_redux';
import { actionMemoChronicle } from 'pages/Memos/Chronicle/_redux/reducers';
import { memoActions } from 'pages/Memos/_redux/reducers';
import { statementTransactionAction } from 'pages/StatementsAndTransactions/StatementTransaction/__redux/reducers';
import { actions as paymentsListAction } from 'pages/Payment/PaymentList/_redux/reducers';
import { actions as accountDetailTabsAction } from 'pages/__commons/AccountDetailTabs/_redux';
import { demandACHPaymentAction } from 'pages/MakePayment/ACHPayment/_redux/reducers';
import { actions as paymentConfirmActions } from 'pages/MakePayment/ModalConfirm/_redux/reducers';
import { collectionFormActions } from 'pages/CollectionForm/_redux/reducers';
import { promiseToPayActions } from 'pages/PromiseToPay/_redux/reducers';
import { hardshipActions } from 'pages/HardShip/_redux/reducers';
import { cccsActions } from 'pages/CCCS/_redux/reducers';
import { bankruptcyActions } from 'pages/Bankruptcy/_redux/reducers';
import { deceasedActions } from 'pages/Deceased/_redux/reducers';
import { companyActions } from 'pages/CCCS/CompanyList/_redux/reducers';
import { letterAction } from 'pages/Letters/_redux/reducers';
import { letterListAction } from 'pages/Letters/LetterList/_redux/reducers';
import { pendingAction as pendingLetterActions } from 'pages/Letters/PendingLetterList/_redux/reducers';
import { transactionAdjustmentAction } from 'pages/TransactionAdjustment/RequestList/_redux/reducer';
import { timerWorkingActions } from 'pages/TimerWorking/_redux/reducers';
import { renewTokenActions } from 'pages/__commons/RenewToken/_redux/reducer';
import { relatedAccountActions } from 'pages/AccountDetails/RelatedAccount/_redux/reducer';
import { accountDetailActions } from 'pages/AccountDetails/_redux/reducer';
import { actionsInfoBar } from 'pages/__commons/InfoBar/_redux';
import { clientConfigurationActions } from 'pages/ClientConfiguration/_redux/reducers';
import { collectorProductivityActions } from 'pages/Reporting/CollectorProductivity/_redux/reducers';
import { promisesPaymentsStatisticsActions } from 'pages/Reporting/PromisesPaymentsStatistics/_redux/reducers';
import { collectorsTeamsComboBoxActions } from 'pages/Reporting/CollectorsTeamsComboBox/_redux/reducers';
import { reportingTabsActions } from 'pages/Reporting/ReportingTabs/_redux/reducers';
import { apiErrorNotificationAction } from 'pages/ApiErrorNotification/_redux/reducers';
import { changeHistoryActions } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';
import { queueAndRoleMappingActions } from 'pages/ClientConfiguration/QueueAndRoleMapping/_redux/reducers';

// Helper
import { removeDelayProgress } from 'pages/CollectionForm/helpers';
import { contactEntitlement } from 'app/entitlements';

export const removeStoreRedux: AppThunk =
  (tabType: TabTypeComponent, storeId: string, accEValue: string) =>
  dispatch => {
    switch (tabType) {
      case 'accountSearchList':
        dispatch(searchResultsAction.removeStore({ storeId }));
        break;
      case 'accountDetail':
        dispatch(removeDelayProgress(storeId));
        dispatch(
          timerWorkingActions.triggerStopWorking({
            storeId,
            accEValue
          })
        );
        dispatch(actionsInfoBar.removeInfoBar({ storeId }));
        dispatch(accountDetailSnapshotActions.removeStore({ storeId }));
        dispatch(accountInformation.removeStore({ storeId }));
        dispatch(accountCollection.removeStore({ storeId }));
        dispatch(actionMemoChronicle.removeChronicleMemo({ storeId }));
        dispatch(memoActions.removeMemo({ storeId }));
        dispatch(statementTransactionAction.removeStore({ storeId }));
        dispatch(paymentsListAction.removeStore({ storeId }));
        dispatch(accountDetailTabsAction.removeStore({ storeId }));
        dispatch(demandACHPaymentAction.removeStore({ storeId }));
        dispatch(collectionFormActions.removeStore({ storeId }));
        dispatch(promiseToPayActions.removeStore({ storeId }));
        dispatch(hardshipActions.removeStore({ storeId }));
        dispatch(cccsActions.removeStore({ storeId }));
        dispatch(bankruptcyActions.removeStore({ storeId }));
        dispatch(deceasedActions.removeStore({ storeId }));
        dispatch(companyActions.removeStore({ storeId }));
        dispatch(letterAction.removeStore({ storeId }));
        dispatch(letterListAction.removeStore({ storeId }));
        dispatch(pendingLetterActions.removeStore({ storeId }));
        dispatch(renewTokenActions.removeRenewTokenBasedOnAccId({ storeId }));
        dispatch(paymentConfirmActions.clearData({ storeId }));
        dispatch(accountDetailActions.removeStore({ storeId }));
        dispatch(
          apiErrorNotificationAction.clearApiErrorData({
            storeId,
            forSection: 'inAddressCardholderFlyOut'
          })
        );
        contactEntitlement.removeRegister(storeId);

        break;
      case 'transactionAdjustment':
        dispatch(transactionAdjustmentAction.removeStore());
        break;
      case 'accountRelated':
        dispatch(accountDetailActions.removeStore({ storeId }));
        dispatch(
          relatedAccountActions.removeRelatedAccountBasedOnStoreId({ storeId })
        );
        contactEntitlement.removeRegister(storeId);
        break;
      case 'clientConfiguration':
        return dispatch(clientConfigurationActions.removeStore());
      case 'reporting':
        dispatch(collectorProductivityActions.cleanup());
        dispatch(promisesPaymentsStatisticsActions.cleanup());
        dispatch(collectorsTeamsComboBoxActions.removeStore());
        dispatch(reportingTabsActions.reset());
        break;
      case 'changeHistory':
        dispatch(changeHistoryActions.reset());
        break;
      case 'queueRoleMapping':
        dispatch(queueAndRoleMappingActions.reset());
        break;
      default:
        return;
    }
  };
