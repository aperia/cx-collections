import isEmpty from 'lodash.isempty';
import pick from 'lodash.pick';

export function pickNestObjectKey(
  objectData: Record<string, MagicKeyValue | undefined>,
  pickKeys: string[]
) {
  if (objectData === null || objectData === undefined) return {};
  const kys = Object.keys(objectData);
  const results = {} as Record<string, MagicKeyValue>;

  kys.forEach((ky: string) => {
    let nestKeyValue = {};

    pickKeys.forEach(pKy => {
      nestKeyValue = pick(objectData[ky], [pKy]);

      if (isEmpty(nestKeyValue)) return;

      results[ky] = {
        ...results[ky],
        ...nestKeyValue
      };
    });
  });

  return results;
}
