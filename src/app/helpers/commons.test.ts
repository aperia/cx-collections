import {
  cleanObj,
  getPropertyDifference,
  getTabTitle,
  isArrayHasValue,
  maxDate,
  buildCommonPayload,
  addDays,
  addMonths,
  capitalizeWord,
  handleWithRefOnFind,
  handleOnFind,
  parseJSONString,
  trimData,
  setErrorField,
  getMaskedValue,
  getDOFViewValue,
  removeUnMatchingData,
  parseJwt,
  joinAddress,
  formatPostalCodeUSA,
  joinStateAndPostalCode,
  generateDateObject,
  isInvalidDateValue,
  getSystemCommon,
  getSessionStorage,
  setSessionStorage,
  getFeatureConfig,
  compareTime,
  timeToSecond,
  setSessionStorageWithUF8,
  getSessionStorageWithUF8
} from './commons';
import { storeId } from 'app/test-utils';
import dateTime from 'date-and-time';
import { Field } from 'redux-form';
import { ExtraFieldProps } from 'app/_libraries/_dof/core';
import { MutableRefObject } from 'react';
import { AxiosRequestConfig } from 'axios';
import { useTranslation } from 'app/_libraries/_dls/hooks';

jest.mock('axios', () => {
  const axios = jest.requireActual('axios');
  const mockAction = (params?: AxiosRequestConfig) => {
    if (params === '/testData') {
      return Promise.resolve({ data: { config: true } });
    }
    return Promise.resolve({});
  };
  const axiosMock = (params?: AxiosRequestConfig) => mockAction(params);
  axiosMock.get = (params?: AxiosRequestConfig) => mockAction(params);
  axiosMock.create = axios.create;
  return {
    ...axios,
    __esModule: true,
    default: axiosMock,
    defaults: axiosMock
  };
});

jest.mock('app/_libraries/_dls/hooks', () => ({
  useTranslation: () => ({
    t: (key: string) => key,
    i18n: { changeLanguage: jest.fn() }
  })
}));

describe('Test common.test.ts', () => {
  describe('getTabTitle function', () => {
    const { t } = useTranslation();

    it('params is list value string ', () => {
      const searchParams: any = [1, 2, 3, 4].map(e => ({ value: `${e}` }));

      const tabTitle = getTabTitle(searchParams);
      expect(tabTitle).toEqual(`1, 2, 3, 4`);
    });

    it('params is list value string > case 2 ', () => {
      const searchParams: any = [1, 2, 3, 4].map(e => ({ value: `${e}` }));

      const tabTitle = getTabTitle(searchParams);
      expect(tabTitle).toEqual(`1, 2, 3, 4`);
    });

    it('params is list value string and 1 item is empty ', () => {
      const searchParams: any = [1, 2, 3, 4].map(e => {
        if (e === 4) return {};
        return { value: `${e}` };
      });

      const tabTitle = getTabTitle(searchParams);
      expect(tabTitle).toEqual(`1, 2, 3`);
    });

    it('params is list value string & object ', () => {
      const searchParams: any = [1, 2, 3, 4].map(e => {
        if (e === 1) return { value: { keyword: '1a' } };
        return { value: `${e}` };
      });

      const result = getTabTitle(searchParams);
      expect(result).toEqual(`1a, 2, 3, 4`);
    });
    it('params is list value string & object empty ', () => {
      const searchParams: any = ['', '', ''];

      const result = getTabTitle(searchParams);
      expect(result).toEqual(``);
    });
  });

  describe('isArrayHasValue function', () => {
    it('input is array 2 value', () => {
      const array = [{ ky: '1' }, { ky: '2' }];

      const result = isArrayHasValue(array);
      expect(result).toBeTruthy();
    });
    it('input is empty array', () => {
      const array: MagicKeyValue[] = [];

      const isArray = isArrayHasValue(array);
      expect(isArray).toBeFalsy();
    });
  });

  describe('buildCommonPayload function', () => {
    it('params is null', () => {
      const common = buildCommonPayload('');
      expect(common).toEqual({ common: { accountId: '' } });
    });

    it('param is string', () => {
      const accountId = storeId;

      const common = buildCommonPayload(accountId);
      expect(common).toEqual({ common: { accountId: storeId } });
    });
  });

  describe('maxDate function', () => {
    it('params is 2 days', () => {
      const dateA = new Date('2020/01/01');
      const dateB = new Date('2019/01/01');

      let max = maxDate(dateA, dateB);
      expect(max).toEqual(dateA);

      max = maxDate(dateB, dateA);
      expect(max).toEqual(dateA);
    });

    it('params are 1 day and 1 null', () => {
      const dateA = new Date('2020/01/01');
      const dateB = new Date('2019/01/01');

      let max = maxDate(dateA, undefined);
      expect(max).toEqual(dateA);

      max = maxDate(undefined, dateB);
      expect(max).toEqual(dateB);
    });
  });

  describe('addDays', () => {
    it('days > 1', () => {
      const currentDate = new Date();

      const tomorrow = addDays(currentDate, 1);

      expect(tomorrow).toEqual(
        dateTime.format(dateTime.addDays(currentDate, 1), 'MM/DD/YYYY')
      );
    });

    it('days < 1', () => {
      const currentDate = new Date();

      const tomorrow = addDays(currentDate, 0);

      expect(tomorrow).toEqual(dateTime.format(currentDate, 'MM/DD/YYYY'));
    });
  });

  describe('addMonths', () => {
    it('months > 1', () => {
      const currentDate = '10/20/2020';

      const nextMonth = addMonths(currentDate, 1);

      expect(nextMonth).toEqual('11/20/2020');
    });

    it('months < 1', () => {
      const currentDate = new Date();

      const tomorrow = addMonths(currentDate, 0);

      expect(tomorrow).toEqual(dateTime.format(currentDate, 'MM/DD/YYYY'));
    });
  });

  it('capitalizeWord', () => {
    const result = capitalizeWord('ho chi minh');
    expect(result).toEqual('Ho Chi Minh');
  });

  it('handleWithRefOnFind', () => {
    const refCurrent: MutableRefObject<object> = {
      current: { props: { onFind: (name: string) => ({ props: { name } }) } }
    };
    const result = handleWithRefOnFind(refCurrent, 'field');
    expect(result).toEqual({ name: 'field' });
  });

  it('handleOnFind', () => {
    const onFind = (name: string) => ({ props: { name } });
    const result = handleOnFind(
      'field',
      onFind as unknown as (name: string) => Field<ExtraFieldProps>
    );
    expect(result).toEqual(onFind('field').props);
  });

  describe('parseJSONString', () => {
    it('object string', () => {
      const objStr = '{"name":"value"}';
      const result = parseJSONString(objStr);
      expect(result).toEqual(JSON.parse(objStr));
    });

    it('empty string', () => {
      const result = parseJSONString('');
      expect(result).toBeUndefined();
    });
  });

  describe('trimData', () => {
    it('object date', () => {
      const date = new Date('10/10/2010');
      expect(trimData(date)).toEqual(date);
    });

    it('object string', () => {
      const objStr = { name: 'value ' };
      const result = trimData(objStr);
      expect(result).toEqual({ name: 'value' });
    });

    it('object number', () => {
      const objStr = { name: 123 };
      const result = trimData(objStr);
      expect(result).toEqual({ name: 123 });
    });

    it('is array', () => {
      const objStr = ['1234  '];
      const result = trimData(objStr);
      expect(result).toEqual(objStr);
    });

    it('is not object or array', () => {
      const objStr = 1234;
      const result = trimData(objStr);
      expect(result).toEqual(objStr);
    });

    it('is nested object', () => {
      const objStr = { name: { value: '12345' } };
      const result = trimData(objStr);
      expect(result).toEqual(objStr);
    });

    it('is nested object and include listIgnore', () => {
      const objStr = { value: '12345  ', ignore: '123  123   ' };
      const result = trimData(objStr, ['ignore']);
      expect(result).toEqual({ value: '12345', ignore: '123  123   ' });
    });
  });

  describe('setErrorField', () => {
    it('setErrorField work correctly', () => {
      const status = true;

      const field = {
        props: {
          props: { setOthers: (cb: Function) => cb({}) },
          name: 'test'
        }
      } as Field<ExtraFieldProps>;

      setErrorField(field, status);
      // for coverage
      expect(true).toEqual(true);
    });
  });

  describe('getMaskedValue', () => {
    it('it should be return data', () => {
      const sample = {
        maskedValue: '123'
      };
      const data = getMaskedValue(JSON.stringify(sample));
      expect(data).toEqual('123');
    });
    it('it should be return empty data', () => {
      const sample = {};
      const data = getMaskedValue(JSON.stringify(sample));
      expect(data).toEqual('');
    });
  });

  describe('getDOFViewValue', () => {
    it('getDOFViewValue work correctly', () => {
      const mappingFields = { name: 'test', age: 10 };
      const formValues = { name: 'test', address: null, Date: '10/10/2010' };
      const data = getDOFViewValue(formValues, mappingFields);
      expect(data).toEqual({ test: 'test', Date: '10102010' });
    });

    it('getDOFViewValue work correctly with date is invalid', () => {
      const mappingFields = { name: 'test', age: 10 };
      const formValues = { name: 'test', Date: '20/20/2010' };
      const isKeepDot = false;
      const data = getDOFViewValue(formValues, mappingFields, isKeepDot);
      expect(data).toEqual({ test: 'test', Date: '20/20/2010' });
    });

    it('getDOFViewValue work correctly with isKeepDot', () => {
      const mappingFields = { Amount: 'data' };
      const formValues = { Amount: '10000.00' };
      const isKeepDot = true;
      const data = getDOFViewValue(formValues, mappingFields, isKeepDot);
      expect(data).toEqual({ data: '10000.00' });
    });

    it('getDOFViewValue work correctly without isKeepDot', () => {
      const mappingFields = { Amount: 'data' };
      const formValues = { Amount: '10000.00' };
      const isKeepDot = false;
      const data = getDOFViewValue(formValues, mappingFields, isKeepDot);
      expect(data).toEqual({ data: '1000000' });
    });

    it('getDOFViewValue work correctly with throw error', () => {
      const mappingFields = { Amount: 'data' };
      const formValues = { Amount: { value: '10000.00' } };
      const isKeepDot = false;
      const data = getDOFViewValue(formValues, mappingFields, isKeepDot);
      expect(data).toEqual({ data: { value: '10000.00' } });
    });

    it('getDOFViewValue work correctly with key is a objecy', () => {
      const mappingFields = { test: 'data' };
      const formValues = { test: { value: '10000.00' } };
      const isKeepDot = false;
      const data = getDOFViewValue(formValues, mappingFields, isKeepDot);
      expect(data).toEqual({ data: '10000.00' });
    });
  });

  describe('test removeUnMatchingData', () => {
    it('it should return {}', () => {
      expect(removeUnMatchingData({ name: 'test' })).toEqual({});
      expect(removeUnMatchingData(undefined, { name: 'test' })).toEqual({});
    });

    it('it should return data correctly', () => {
      const matchData = { name: '123', age: 20 };
      const sampleData = { age: 20, address: '233' };

      const data = removeUnMatchingData(matchData, sampleData);
      expect(data).toEqual({ age: 20 });
    });
  });

  describe('joinAddress', () => {
    it('should be work correctly', () => {
      const sample = ['234', undefined, 'test'];
      const data = joinAddress(...sample);
      expect(data).toEqual('234, test');
    });
  });

  describe('formatPostalCodeUSA', () => {
    it('should be return data follow by code USA', () => {
      const data = formatPostalCodeUSA('USA', '12345567');
      expect(data).toEqual('12345');
    });
    it('should be return data follow by code USA', () => {
      const data = formatPostalCodeUSA('', '12345567');
      expect(data).toEqual('12345');
    });
    it('should be return data', () => {
      const data = formatPostalCodeUSA('AAA', '12345567');
      expect(data).toEqual('12345567');
    });
  });

  describe('joinStateAndPostalCode', () => {
    it('it should be return undefined', () => {
      expect(joinStateAndPostalCode()).toEqual(undefined);
    });
    it('it should be return state', () => {
      const state = 'test';
      expect(joinStateAndPostalCode('test')).toEqual(state);
    });
    it('it should be return postalCode', () => {
      const postalCode = 'test';
      expect(joinStateAndPostalCode(undefined, postalCode)).toEqual(postalCode);
    });
    it('it should be return correctly', () => {
      const state = 'test';
      const postalCode = 'test';
      expect(joinStateAndPostalCode(state, postalCode)).toEqual(
        `${state} ${postalCode}`
      );
    });
  });

  describe('generateDateObject', () => {
    it('should return data date correctly', () => {
      const data = generateDateObject('10/10/2010');
      const result = new Date('10/10/2010');
      result.setHours(0, 0, 0, 0);
      expect(data).toEqual(result);
    });
  });

  describe('isInvalidDateValue', () => {
    it('should be return true with empty value', () => {
      const data = isInvalidDateValue('');
      expect(data).toBeTruthy();
    });

    it('should be return true with invalid value', () => {
      const data = isInvalidDateValue('null');
      expect(data).toBeTruthy();
    });

    it('should be return false with valid value', () => {
      const data = isInvalidDateValue(new Date());
      expect(data).toBeFalsy();
    });
  });

  describe('getSystemCommon', () => {
    it('should be return data undefined', () => {
      const data = getSystemCommon({});
      expect(data).toEqual({
        clientID: undefined,
        system: undefined,
        prin: undefined,
        agent: undefined
      });
    });

    it('should be return data undefined', () => {
      const data = getSystemCommon({ rawData: { clientIdentifier: '123' } });
      expect(data).toEqual({
        clientID: '123',
        system: undefined,
        prin: undefined,
        agent: undefined
      });
    });
  });

  describe('SessionStorage', () => {
    const sessionStorageMock = (() => {
      let store: MagicKeyValue = {};
      return {
        getItem: function (key: string) {
          return store[key] || undefined;
        },
        setItem: function (key: string, value: any) {
          store[key] = value ? value.toString() : value + '';
        },
        clear: function () {
          store = {};
        }
      };
    })();

    Object.defineProperty(window, 'sessionStorage', {
      value: sessionStorageMock
    });

    beforeEach(() => {
      sessionStorage.clear();
    });
    it('it get session storage correctly', () => {
      sessionStorage.setItem(
        'tab',
        btoa(JSON.stringify({ value: '123-1234' }))
      );
      const data = getSessionStorage('tab');
      expect(data).toEqual({ value: '123-1234' });
      expect(getSessionStorage('accountDetail')).toBeUndefined();

      sessionStorage.setItem(
        'accountDetail',
        JSON.stringify({ value: '123-1234' })
      );
      expect(getSessionStorage('accountDetail')).toBeUndefined();
    });

    it('it get session storage correctly', () => {
      sessionStorage.setItem(
        'tab',
        btoa(JSON.stringify({ value: '123-1234' }))
      );
      const data = getSessionStorage('tab');
      expect(data).toEqual({ value: '123-1234' });
      expect(getSessionStorage('accountDetail')).toBeUndefined();

      sessionStorage.setItem(
        'accountDetail',
        JSON.stringify({ value: '123-1234' })
      );
      expect(getSessionStorage('accountDetail')).toBeUndefined();
    });

    it('it get/set session storage with UF8 correctly', () => {
      setSessionStorageWithUF8('tab', { value: '123-1234' });
      const data = getSessionStorageWithUF8('tab');
      expect(data).toEqual({ value: '123-1234' });
      expect(getSessionStorageWithUF8('accountDetail')).toBeUndefined();

      setSessionStorageWithUF8('accountDetail', { value: '123-1234' });
      expect(getSessionStorageWithUF8('accountDetail')).toEqual({
        value: '123-1234'
      });
    });

    it('it get/set session storage with UF8 error', () => {
      setSessionStorageWithUF8('tab', '');
      sessionStorage.setItem('accountDetail', '&^@'); // try error
      const data = getSessionStorageWithUF8('accountDetail');
      expect(data).toEqual(undefined);
    });

    it('it set session storage correctly', () => {
      expect(setSessionStorage('tab', {})).toBeUndefined();

      setSessionStorage('tab', { value: '1234' });

      expect(getSessionStorage('tab')).toEqual({ value: '1234' });
    });
  });

  describe('test getFeatureConfig', () => {
    it('it should be work correctly', async () => {
      const data = await getFeatureConfig('/testData');
      expect(data).toEqual({ config: true });

      const dataInStoreConfig = await getFeatureConfig('/testData');
      expect(dataInStoreConfig).toEqual({ config: true });

      const responseWithoutData = await getFeatureConfig('/testNoData');
      expect(responseWithoutData).toEqual(undefined);
    });
  });

  describe('compareTime', () => {
    it('it compare with greaterThan with different hour', () => {
      const date1 = { hour: '9', minute: '10', second: '12' };
      const date2 = { hour: '10', minute: '10', second: '12' };
      expect(compareTime(date1, 'greaterThan', date2)).toEqual(false);
      expect(compareTime(date2, 'greaterThan', date1)).toEqual(true);
      // For Coverage
      expect(compareTime(date2, '' as never, date1)).toEqual(false);
    });

    it('it compare with greaterThan with different minute', () => {
      const date1 = { hour: '9', minute: '10', second: '12' };
      const date2 = { hour: '9', minute: '11', second: '12' };
      expect(compareTime(date1, 'greaterThan', date2)).toEqual(false);
      expect(compareTime(date2, 'greaterThan', date1)).toEqual(true);
    });

    it('it compare with greaterThan with different second', () => {
      const date1 = { hour: '9', minute: '10', second: '12' };
      const date2 = { hour: '9', minute: '10', second: '13' };
      expect(compareTime(date1, 'greaterThan', date2)).toEqual(false);
      expect(compareTime(date2, 'greaterThan', date1)).toEqual(true);
    });

    it('it compare with lessThan with different hour', () => {
      const date1 = { hour: '9', minute: '10', second: '12' };
      const date2 = { hour: '10', minute: '10', second: '12' };
      expect(compareTime(date1, 'lessThan', date2)).toEqual(true);
      expect(compareTime(date2, 'lessThan', date1)).toEqual(false);
    });

    it('it compare with lessThan with different minute', () => {
      const date1 = { hour: '9', minute: '10', second: '12' };
      const date2 = { hour: '9', minute: '11', second: '12' };
      expect(compareTime(date1, 'lessThan', date2)).toEqual(true);
      expect(compareTime(date2, 'lessThan', date1)).toEqual(false);
    });

    it('it compare with lessThan with different second', () => {
      const date1 = { hour: '9', minute: '10', second: '12' };
      const date2 = { hour: '9', minute: '10', second: '13' };
      expect(compareTime(date1, 'lessThan', date2)).toEqual(true);
      expect(compareTime(date2, 'lessThan', date1)).toEqual(false);
    });
  });

  describe('test timeToSecond', () => {
    it('return default value', () => {
      expect(timeToSecond()).toEqual(35999);
    });
    it('return correct value', () => {
      const date = { hour: '9', minute: '10', second: '12' };
      const minPerSecond = 60;
      const hourPerSecond = 6 * minPerSecond; // 1h = 360 second => Because of business
      const result =
        Number(date.second) +
        Number(date.minute) * minPerSecond +
        Number(date.hour) * hourPerSecond;
      expect(timeToSecond(date)).toEqual(result);
    });
  });

  it('cleanObj', () => {
    const data = { a: { b: undefined, c: undefined }, undefined };

    const result = cleanObj(data);
    expect(result).toEqual({ a: {} });
  });

  it('parseJwt error', () => {
    const result = parseJwt('1234');
    expect(result).toEqual(undefined);
  });

  it('parseJwt error', () => {
    const result = parseJwt(
      'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlc3NVc2VyTmFtZSI6IlRlc3QgTmFtZSIsImV4cCI6MTY0MTAyNDAwMCwiYXBwSWQiOiJjeENvbGxlY3Rpb25zIiwib3BlcmF0b3JJZCI6IkZERVNNQVNUIiwiT3JnSUQiOiJGRFJUIiwidXNlcklkIjoiRkRFU01BU1QiLCJuYmYiOjE2MDU4OTA2OTQsImlzcyI6Ik5TQSIsIm9jc1NoZWxsIjoiRkRFU01BU1QiLCJwcml2aWxlZ2VzIjpbIkNYQ09MNSJdLCJ1c2VyTmFtZSI6InRlc3RuYW1lIiwieDUwMElkIjoiQUFBQTExMTQiLCJlbnRpdGxlbWVudCI6W10sImdyb3VwcyI6WyJITE5TQSJdfQ.xYuzqxCAE_HSIjne_BjwdnsxrOEbLCjUatYzABlnNR0'
    );
    expect(result).toBeTruthy();
  });

  it('getPropertyDifference', () => {
    const oldValue = { field: '1', field1: '1', fieldNumber: 0, field2: '2' };
    const newValue = { field: '2', fieldNumber: 0, field2: undefined };

    const result = getPropertyDifference(oldValue, newValue);
    expect(result.field).toEqual({ new: newValue.field, old: oldValue.field });
    expect(result.field1).toEqual({ new: '', old: oldValue.field1 });

    const oldValue1 = { field: undefined };
    const newValue1 = { field: undefined };
    const result1 = getPropertyDifference(oldValue1, newValue1);

    expect(result1).toEqual({});

    const oldValue2 = { field: undefined };
    const newValue2 = { field: 'new' };
    const result2 = getPropertyDifference(oldValue2, newValue2);

    expect(result2.field).toEqual({ new: newValue2.field, old: '' });
  });
});
