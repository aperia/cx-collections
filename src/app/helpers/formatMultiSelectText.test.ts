import { getGroupFormatInputText } from './formatMultiSelectText';

describe('format multi select text', () => {
  it('should be selected all', () => {
    expect(getGroupFormatInputText(3, 3)).toEqual('txt_all_items_selected');
  });
  it('should be selected only one with string value', () => {
    expect(getGroupFormatInputText(1, 3, 'value')).toEqual('value');
  });
  it('should be selected only one with object value', () => {
    expect(
      getGroupFormatInputText(1, 3, [{ value: 'value' }], 'value')
    ).toEqual('value');
  });
  it('should be not selected', () => {
    expect(getGroupFormatInputText(0, 3)).toEqual('');
  });
  it('should be selected over one but not all', () => {
    expect(getGroupFormatInputText(2, 3)).toEqual('txt_of_selected');
  });
});
