export const convertFileToBase64 = (file: Blob): Promise<any> => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onloadend = function () {
      const [, ...urlFile] = (reader.result as string).split(',');
      resolve(urlFile.join(''));
    };
    reader.onerror = function (error) {
      reject(error);
    };
    reader.readAsDataURL(file);
  });
};
