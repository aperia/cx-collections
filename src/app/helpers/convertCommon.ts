export const convertStringToNumberOnly = (value: string) => {
  return value.toString().replace(/\D/g, '');
};
