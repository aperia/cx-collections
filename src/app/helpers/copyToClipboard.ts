export const copyToClipboard = (content: string, type = 'text/html') => {
  const blob = new Blob([content], { type });
  const data = [new ClipboardItem({ [type]: blob })];
  navigator.clipboard.write(data);
};
