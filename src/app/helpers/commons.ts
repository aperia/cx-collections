import { MutableRefObject } from 'react';
import { Field } from 'redux-form';
import get from 'lodash.get';
import axios from 'axios';
import dateTime from 'date-and-time';
import isObject from 'lodash.isobject';
import isUndefined from 'lodash.isundefined';
import isEmpty from 'lodash.isempty';
import isArray from 'lodash.isarray';
import isDate from 'lodash.isdate';
import isNil from 'lodash.isnil';
import isNull from 'lodash.isnull';

// Types
import { ExtraFieldProps } from 'app/_libraries/_dof/core';

// Component
import { AttributeSearchValue } from 'app/_libraries/_dls/components';

// Const
import { FormatTime } from 'app/constants/enums';

// Helper
import { INVALID_DATE } from 'app/constants';
import { isValidDate } from './formatTime';
import { Time } from 'pages/ClientConfiguration/Timer/types';
import isNumber from 'lodash.isnumber';
import difference from 'lodash.difference';
import isEqual from 'lodash.isequal';

export const cleanObj = <T extends Record<string, unknown>>(obj: T) => {
  const newObj = { ...obj };
  Object.keys(newObj).forEach(
    key => newObj[key] === undefined && delete newObj[key]
  );
  return newObj;
};

export const DATA_SOURCE = {
  getIncomeFrequency: {
    url: `refData/incomeFrequency.json`,
    customRequest: 'getIncomeFrequency'
  },
  getCountryOfDeployment: {
    url: `refData/countryOfDeployment.json`,
    customRequest: 'getCountryOfDeployment'
  },
  getPaymentMethod: {
    url: `refData/paymentMethod.json`,
    customRequest: 'getPaymentMethod'
  },
  getLongTermReageMonths: {
    url: `refData/longTermReageMonths.json`,
    customRequest: 'getLongTermReageMonths'
  },
  bankruptcyChapter: {
    url: `refData/chapter.json`,
    customRequest: 'bankruptcyChapter'
  },
  fillingInformationType: {
    url: `refData/fillingType.json`,
    customRequest: 'fillingInformationType'
  },
  fillingInformationAssets: {
    url: `refData/assets.json`,
    customRequest: 'fillingInformationAssets'
  },
  fillingInformationProSe: {
    url: `refData/proSE.json`,
    customRequest: 'fillingInformationProSe'
  },
  fillingInformationSurrenderIndicator: {
    url: `refData/surrenderIndicator.json`,
    customRequest: 'fillingInformationSurrenderIndicator'
  },
  fillingInformationNonfilerIndicator: {
    url: `refData/nonfilerInd.json`,
    customRequest: 'fillingInformationNonfilerIndicator'
  },
  fillingInformationNonfilerCollectibleIndicator: {
    url: `refData/nonfierCollectible.json`,
    customRequest: 'fillingInformationNonfilerCollectibleIndicator'
  },
  fillingInformationPrimaryCustomerInformationIndicatorCode: {
    url: `refData/customerInfoCode.json`,
    customRequest: 'fillingInformationPrimaryCustomerInformationIndicatorCode'
  },
  fillingInformationPrimaryCreditBureauReportCode: {
    url: `refData/creditBureauCode.json`,
    customRequest: 'fillingInformationPrimaryCreditBureauReportCode'
  },
  fillingInformationSecondaryCustomerInformationIndicatorCode: {
    url: `refData/customerInfoCode.json`,
    customRequest: 'fillingInformationSecondaryCustomerInformationIndicatorCode'
  },
  fillingInformationSecondaryCreditBureauReportCode: {
    url: `refData/creditBureauCode.json`,
    customRequest: 'fillingInformationSecondaryCreditBureauReportCode'
  },
  additionalAttorneyInformationRetained: {
    url: `refData/retained.json`,
    customRequest: 'additionalAttorneyInformationRetained'
  },
  getStateRefData: {
    url: `refData/state.json`,
    customRequest: 'getStateRefData'
  },
  getUSAState: {
    url: `refData/USAState.json`,
    customRequest: 'getUSAState'
  },
  settlementMethod: {
    url: `refData/settlementMethod.json`,
    customRequest: 'settlementMethod'
  },
  getDeceasedContact: {
    url: `refData/deceasedContact.json`,
    customRequest: 'getDeceasedContact'
  },
  promiseToPayMethod: {
    url: `refData/promiseToPayMethod.json`,
    customRequest: 'getPromiseToPayMethod'
  },
  promiseToPayInterval: {
    url: `refData/interval.json`,
    customRequest: 'getPromiseToPayInterval'
  },
  promiseToPayCount: {
    url: `refData/count.json`,
    customRequest: 'getPromiseToPayCount'
  },
  getFullMonth: {
    url: `refData/fullMonth.json`,
    customRequest: 'getFullMonth'
  },
  getMethodToSend: {
    url: `refData/sendMethod.json`,
    customRequest: 'getMethodToSend'
  },
  editPhoneDeviceType: {
    url: `refData/phoneDeviceType.json`,
    customRequest: 'editPhoneDeviceType'
  },
  editPhoneFlag: {
    url: `refData/phoneFlag.json`,
    customRequest: 'editPhoneFlag'
  },
  getAddressCategory: {
    url: `refData/addressCategory.json`,
    customRequest: 'getAddressCategory'
  },
  getAddressType: {
    url: `refData/addressType.json`,
    customRequest: 'getAddressType'
  },
  getAddressRelationShip: {
    url: `refData/addressRelationship.json`,
    customRequest: 'getAddressRelationShip'
  },
  getCountryIso: {
    url: `refData/countryISO.json`,
    customRequest: 'getCountryIso'
  },
  adjustTransactionReason: {
    url: `refData/reasonAdjustment.json`,
    customRequest: 'reasonForAdjustment'
  },
  adjustTransactionPromoId: {
    url: `refData/promoId.json`,
    customRequest: 'promoId'
  },
  moveToQueue: {
    url: `refData/moveToQueue.json`,
    customRequest: 'getMoveToQueue'
  },
  delinquencyReason: {
    url: `refData/delinquencyReason.json`,
    customRequest: 'getDelinquencyReason'
  },
  reagePaymentPeriod: {
    url: `refData/reageMonths.json`,
    customRequest: 'reagePaymentPeriod'
  },
  getExternalStatus: {
    url: 'refData/externalStatus.json',
    customRequest: 'getExternalStatus'
  },
  getExternalStatusReason: {
    url: 'refData/externalStatusReason.json',
    customRequest: 'getExternalStatusReason'
  },
  filerName: {
    url: 'refData/externalStatusReason.json',
    customRequest: 'getExternalStatusReason'
  }
};

export const getTabTitle = (searchParams: AttributeSearchValue[]): string => {
  return searchParams
    .map(item => {
      if (!item) return '';
      if (typeof item.value === 'string') {
        return item.value;
      }
      if (typeof item.value === 'object') {
        return get(item, ['value', 'keyword']);
      }
      return '';
    })
    .filter(e => e)
    .join(', ');
  // title = `${title} - ${t?.('txt_search_results')}`;
  // return title;
};

export const isArrayHasValue = (data: Array<MagicKeyValue>): boolean => {
  return data && data.length ? true : false;
};

export const buildCommonPayload = (accountId: string) => {
  return {
    common: {
      accountId
    }
  };
};

export const maxDate = (a?: Date, b?: Date) => {
  if (!a) return b;
  if (!b) return a;

  return a > b ? a : b;
};

export const addDays = (date: string | Date, days: number) => {
  if (!days || days < 1) return dateTime.format(new Date(date), 'MM/DD/YYYY');
  const newDate = new Date(date);
  newDate?.setHours(0, 0, 0, 0);
  newDate.setDate(newDate.getDate() + days);
  return dateTime.format(newDate, 'MM/DD/YYYY');
};

export const addMonths = (date: string | Date, months: number) => {
  if (!months || months < 1)
    return dateTime.format(new Date(date), 'MM/DD/YYYY');
  const getDatesInMonth = (year: number, month: number) => {
    return new Date(year, month, 0).getDate();
  };
  const newDate = new Date(date);
  const thisDate = newDate.getDate();
  newDate.setDate(1);
  newDate.setMonth(newDate.getMonth() + months);
  newDate.setDate(
    Math.min(
      thisDate,
      getDatesInMonth(newDate.getFullYear(), newDate.getMonth() + 1)
    )
  );
  return dateTime.format(newDate, 'MM/DD/YYYY');
};

export const capitalizeRegex = new RegExp(
  atob('KF5cd3sxfSl8KFxzK1x3ezF9KQ=='),
  'g'
);
export const capitalizeWord = (sentence: string) => {
  const lowerCaseSentence = sentence.toLocaleLowerCase();
  return lowerCaseSentence.replace(capitalizeRegex, letter =>
    letter.toLocaleUpperCase()
  );
};

export const handleWithRefOnFind = <T extends MagicKeyValue>(
  refCurrent: MutableRefObject<T>,
  name: string
): ExtraFieldProps => {
  return refCurrent?.current?.props?.onFind(name)?.props;
};

export const handleOnFind = (
  name: string,
  onFind: (name: string) => Field<ExtraFieldProps>
): ExtraFieldProps => {
  const findField = onFind(name);
  const fieldProps = findField.props;
  return fieldProps;
};

export const setErrorField = (
  field: Field<ExtraFieldProps>,
  status: boolean
) => {
  field.props.props.setOthers((prevOthers: MagicKeyValue) => ({
    ...prevOthers,
    options: { ...prevOthers.options, isError: status }
  }));
};

export const parseJSONString = (str?: string) => {
  try {
    return JSON.parse(str || '');
  } catch {
    return undefined;
  }
};

export const getMaskedValue = (str?: string): string => {
  return parseJSONString(str)?.maskedValue || '';
};

/**
 * trim all property that of type string
 * @param data: any type
 */
export const trimData = (
  data: any,
  listIgnore?: Array<string>
): typeof data => {
  if (isArray(data)) {
    return data.map(obj => trimData(obj));
  }

  if (isDate(data)) {
    return data;
  }

  if (!isObject(data)) {
    return data;
  }

  const result: MagicKeyValue = {};

  Object.entries(data).forEach(([ky, value]) => {
    if (isObject(value)) {
      result[ky] = trimData(value);

      return;
    }

    if (listIgnore?.includes(ky)) {
      return (result[ky] = value);
    }

    result[ky] = isUndefined(value?.trim?.()) ? value : value.trim();
  });

  return result;
};

const generateFeatureConfig = () => {
  const storeFeatureConfig: MagicKeyValue = {};

  const getConfig = async <T>(endpoint: string): Promise<T> => {
    // Return the config if there is already have it, else try to get the current

    if (storeFeatureConfig[endpoint]) {
      return Promise.resolve(storeFeatureConfig[endpoint] as T);
    }
    const response = await axios.get(endpoint);

    storeFeatureConfig[endpoint] = response.data || {};

    return Promise.resolve(response.data as T);
  };

  return getConfig;
};

export const getFeatureConfig = generateFeatureConfig();

export const getDOFViewValue = (
  formValues: MagicKeyValue,
  mappingFields: MagicKeyValue,
  isKeepDot?: boolean
) => {
  return Object.keys(formValues)?.reduce(
    (reduceInfo: MagicKeyValue, kyInfo: string) => {
      const mappingKey = mappingFields[kyInfo] || kyInfo;
      try {
        if (isNull(formValues[kyInfo])) {
          return reduceInfo;
        }

        if (
          isValidDate(formValues[kyInfo]) &&
          kyInfo.includes('Date') &&
          !INVALID_DATE.includes(formValues[kyInfo])
        ) {
          return {
            ...reduceInfo,
            [mappingKey]: dateTime.format(
              new Date(formValues[kyInfo]),
              FormatTime.DefaultPostDate
            )
          };
        }

        if (kyInfo.includes('Amount')) {
          return {
            ...reduceInfo,
            [mappingKey]: isKeepDot
              ? formValues[kyInfo]
              : (formValues[kyInfo] as string)?.replace('.', '')
          };
        }

        if (isObject(formValues[kyInfo])) {
          return {
            ...reduceInfo,
            [mappingKey]: formValues[kyInfo]?.value
          };
        }

        reduceInfo = { ...reduceInfo, [mappingKey]: formValues[kyInfo] };

        return reduceInfo;
      } catch (error) {
        return { ...reduceInfo, [mappingKey]: formValues[kyInfo] };
      }
    },
    {}
  );
};

export const removeUnMatchingData = (
  matchData?: MagicKeyValue,
  data?: MagicKeyValue
) => {
  if (isNil(matchData) || isNil(data)) return {};

  const matchKys = Object.keys(matchData);
  const currentKys = Object.keys(data);

  return currentKys.reduce((nextData, ky) => {
    if (matchKys.includes(ky)) {
      nextData = { ...nextData, [ky]: data[ky] };
    }

    return nextData;
  }, {});
};

export const joinAddress = (...args: Array<string | undefined>) => {
  return args.filter(item => !isEmpty(item)).join(', ');
};

export const formatPostalCodeUSA = (
  countryCode?: string,
  postalCode?: string
) => {
  if (
    countryCode === 'USA' ||
    (!countryCode?.match(/^[\S]+$/) && postalCode && postalCode?.length > 5)
  )
    return postalCode?.slice(0, 5);
  return postalCode;
};

export const joinStateAndPostalCode = (state?: string, postalCode?: string) => {
  if (!state) return postalCode;
  if (!postalCode) return state;

  return `${state} ${postalCode}`;
};

export const generateDateObject = (date: string) => {
  const result = new Date(date);
  result.setHours(0, 0, 0, 0);
  return result;
};

export const isInvalidDateValue = (value: any) => {
  return !isDate(value);
};

export const getSystemCommon = (value: MagicKeyValue) => {
  const {
    systemIdentifier,
    clientIdentifier,
    principalIdentifier,
    agentIdentifier
  } = value.rawData ?? {};

  return {
    clientID: clientIdentifier,
    system: systemIdentifier,
    prin: principalIdentifier,
    agent: agentIdentifier
  };
};

export const getSessionStorage = <T extends MagicKeyValue>(
  saveKey: SessionStorageKey
): T | undefined => {
  const valueByKey = sessionStorage.getItem(saveKey);

  if (!valueByKey) return undefined;

  try {
    return JSON.parse(atob(valueByKey)) as T;
  } catch (error) {
    sessionStorage.clear();
    return undefined;
  }
};

export const setSessionStorage = <T = MagicKeyValue>(
  saveKey: SessionStorageKey,
  data: T
) => {
  if (isEmpty(data)) return;
  const encryptData = btoa(JSON.stringify(data));
  sessionStorage.setItem(saveKey, encryptData);
};

export const getSessionStorageWithUF8 = <T extends MagicKeyValue>(
  saveKey: SessionStorageKey
): T | undefined => {
  const valueByKey = sessionStorage.getItem(saveKey);

  if (!valueByKey) return undefined;

  try {
    return JSON.parse(decodeURIComponent(escape(atob(valueByKey)))) as T;
  } catch (error) {
    sessionStorage.clear();
    return undefined;
  }
};

export const setSessionStorageWithUF8 = <T = MagicKeyValue>(
  saveKey: SessionStorageKey,
  data: T
) => {
  if (isEmpty(data)) return;
  const encryptData = btoa(unescape(encodeURIComponent(JSON.stringify(data))));
  sessionStorage.setItem(saveKey, encryptData);
};

type ConditionTime = 'lessThan' | 'greaterThan';
export function compareTime(
  time1: Time,
  condition: ConditionTime,
  time2: Time
): boolean {
  switch (condition) {
    case 'greaterThan':
      if (
        parseInt(time1.hour, 10) > parseInt(time2.hour, 10) ||
        (time1.hour === time2.hour &&
          parseInt(time1.minute, 10) > parseInt(time2.minute, 10)) ||
        (time1.hour === time2.hour &&
          time1.minute === time2.minute &&
          parseInt(time1.second, 10) > parseInt(time2.second, 10))
      )
        return true;
      return false;
    case 'lessThan':
      if (
        parseInt(time1.hour, 10) < parseInt(time2.hour, 10) ||
        (time1.hour === time2.hour &&
          parseInt(time1.minute, 10) < parseInt(time2.minute, 10)) ||
        (time1.hour === time2.hour &&
          time1.minute === time2.minute &&
          parseInt(time1.second, 10) < parseInt(time2.second, 10))
      )
        return true;
      return false;
    default:
      return false;
  }
}

export function timeToSecond(time?: Time, defaultValue?: number) {
  if (!time) return defaultValue ?? 35999; // infinite
  return +time.hour * 360 + +time.minute * 60 + +time.second;
}

export const getPropertyDifference = (
  oldValues: MagicKeyValue,
  newValues: MagicKeyValue
) => {
  const oldValueKeys = Object.keys(oldValues);
  const newValueKeys = Object.keys(newValues);

  const differenceKey = difference(oldValueKeys, newValueKeys);
  const result: MagicKeyValue = {};

  differenceKey.forEach(key => {
    result[key] = {
      old: oldValues[key],
      new: newValues[key]
    };
  });

  oldValueKeys.forEach(key => {
    const oldValue = oldValues[key];
    const newValue = newValues[key];
    const isOldValueEmpty = !oldValue && !isNumber(oldValue);
    const isNewValueEmpty = !newValue && !isNumber(newValue);
    if (isOldValueEmpty && isNewValueEmpty) {
      return;
    }

    if (isEqual(oldValue, newValue)) {
      return;
    }
    result[key] = {
      old: isOldValueEmpty ? '' : oldValue,
      new: isNewValueEmpty ? '' : newValue
    };
  });

  return result;
};

export const parseJwt = (token: string) => {
  try {
    const [, base64Url] = token.split('.');
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(
      atob(base64)
        .split('')
        .map(item => {
          return '%' + ('00' + item.charCodeAt(0).toString(16)).slice(-2);
        })
        .join('')
    );
    return JSON.parse(jsonPayload) as JWT;
  } catch (e) {
    console.log(e);
  }
};
