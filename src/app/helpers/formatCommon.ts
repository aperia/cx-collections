import isNaN from 'lodash.isnan';
import isString from 'lodash.isstring';
import isFunction from 'lodash.isfunction';

import { formatContactPhone } from './formatContactPhone';
import { formatTime } from './formatTime';

export const mapStringAmountToPost = (amount: string, fixed = 2) => {
  const fixedNumber = Math.pow(10, fixed);
  return Math.round(parseFloat(amount) * fixedNumber);
};


/**
 * - input: 09-18-2008
 * - output: 09/18/2008
 * @param {string} dateStr
 * @returns {string}
 */
export const formatDisplayDate = (dateStr: string): string => {
  return dateStr.replace(/-/g, '/');
}

/**
 * - input: 80.2135
 * - output: 80.21%
 * @param {string} text
 * @returns {string}
 */
export const formatPercent = (
  text: string,
  fractionDigits: null | number | undefined = 2
) => {
  if (text === '') return null;

  if (isNaN(Number(text))) return text;

  if (fractionDigits === null) {
    return `${Number(text)}%`;
  }

  return `${Number(text).toFixed(fractionDigits)}%`;
};

/**
 * - input: 1000
 * - output: 1,000
 * @param {string} text
 * @returns {string}
 */
const regexFormatQuantity = new RegExp(
  atob('XGQoPz0oPzpcZHszfSkrKD8hXGQpKQ==')
);
const formatQuantity = (text: string) => {
  if (isNaN(Number(text))) return text;
  return text.toString().replace(regexFormatQuantity, (a: string) => a + ',');
};

const regexFormatCardNumber = new RegExp(
  atob('XGQoPz0oPzpcZHs0fSkrKD8hXGQpKQ=='),
  'g'
);
const formatCardNumber = (text: string) => {
  if (isNaN(Number(text))) return text;
  return text.toString().replace(regexFormatCardNumber, (a: string) => a + '-');
};

/**
 *
 * @param decimal 5
 * @param fractionDigits 2
 * @returns 50
 *
 * @param decimal 5000
 * @param fractionDigits 2
 * @returns 5000
 */
export const formatDecimalView = (decimal: string, fractionDigits: number) => {
  if (decimal.length > fractionDigits) return decimal;

  let result = decimal;
  for (let index = result.length; index < fractionDigits; index++) {
    result += '0';
  }

  return result;
};

/**
 * - input: text: 1000.123456, fractionDigits: null
 * - output: 1,000.123456
 * @param {string} text
 * @param {number} fractionDigits
 * @returns {string}
 */
export const formatAPICurrency = (text = '', fractionDigits = 2) => {
  if ((text + '').trim() === '') return text;

  let result = formatCurrency(text)(fractionDigits);

  if ((text + '').includes('.')) {
    const [number, decimal] = result.split('.');
    const formatDecimal = decimal.replace(')', '');
    const decimalIs0 = Number(formatDecimal) || 0;

    if (decimalIs0 === 0) {
      return result;
    }

    const isSignal = number[0] === '(';

    result =
      number +
      '.' +
      formatDecimalView(formatDecimal, fractionDigits) +
      (isSignal ? ')' : '');
  }

  return result;
};

/**
 * - input: text: 1000, fractionDigits: 2
 * - output: 1,000.00
 * @param {string} text
 * @param {number} fractionDigits
 * @returns {string}
 */
const formatCurrency =
  (text: string) =>
  (fractionDigits = 2) => {
    if (isNaN(Number(text))) return text;

    let bracket = false;
    let amount = Number(text);

    if (amount < 0) {
      bracket = true;
      amount = Number(text) * -1;
    }

    const val = `$${amount
      .toFixed(fractionDigits)
      .replace(/(\d)(?=(\d\d\d)+(?=\.))/g, '$1,')}`;

    return bracket ? `(${val})` : val;
  };

/**
 * - input: text: 0000
 * - output: 0
 * @param {string} text
 * @returns {number}
 */
const formatNumber = (text: string) => {
  if (isNaN(Number(text))) return text;

  return Number(text);
};

/**
 * - Common format
 * @param {string | Date | number} input
 * @returns {object}
 */
export const formatCommon = (input: string | Date | number) => {
  const text = isString(input)
    ? input
    : isFunction(input?.toString)
    ? input.toString()
    : '';

  return {
    get number() {
      return formatNumber(text);
    },
    get phone() {
      return formatContactPhone(text);
    },
    get quantity() {
      return formatQuantity(text);
    },
    get currency() {
      return formatCurrency(text);
    },
    get percent() {
      return formatPercent(text);
    },
    get time() {
      return formatTime(text);
    },
    get cardNumber() {
      return formatCardNumber(text);
    }
  };
};
