import get from 'lodash.get';
import set from 'lodash.set';
import isEmpty from 'lodash.isempty';
import isArray from 'lodash.isarray';
import isString from 'lodash.isstring';
import toString from 'lodash.tostring';
import { chunksAll } from 'app/helpers/chunkAll';
// import { parseJSON } from 'pages/Payment/PaymentList/helpers';

const customFieldRegex = new RegExp(atob('e1tcXC58XHddK30='));

const getValue = (
  inputKy: string,
  chunkAlls: any[],
  regexReplace = /{|}/gm
) => {
  return chunkAlls.map(({ start, end }) => {
    return inputKy.substring(start, end).replace(regexReplace, '').trim();
  });
};

/**
 * - ...
 * @param {object} Object Value need to be transformed/formatted
 * @param {object} Mapping Object
 * @returns {object}
 */
export const mappingDataFromObj = <R = any>(
  itemValue: MagicKeyValue,
  mappingModel: object,
  isRevert = false
): R => {
  // That result used generic type, so that we can't define the specifically type
  const result: any = {};
  if (isEmpty(itemValue) || isEmpty(mappingModel)) return result;

  if (isRevert) {
    Object.entries(mappingModel).forEach(([outputKy, inputKy]) => {
      set(result, inputKy, get(itemValue, outputKy));
    });
  } else {
    Object.entries(mappingModel).forEach(([outputKy, inputKy]) => {
      let inputValue = get(itemValue, inputKy);

      // check mapping Modal has custom mapping format code
      if (customFieldRegex.test(inputKy.trim())) {
        // get all position of match pattern
        const chunkAlls = chunksAll({
          parentString: inputKy,
          searchWord: /{[\w|\\.]+}/,
          regexOption: 'gi'
        });

        let convertedValue = inputKy;

        const kyNames = getValue(inputKy, chunkAlls);

        const [firstItem, ...restKyNames] = kyNames;

        if (firstItem === 'SUM') {
          // combineField: "{SUM}{statementSummaryLine1Field2}{statementSummaryLine1Field3}{statementSummaryLine1Field4}"
          const hasValidField = restKyNames.some(kyName => {
            const valueByKey = get(itemValue, kyName, '') as string;
            return !isNaN(parseFloat(valueByKey));
          });

          if (!hasValidField) {
            convertedValue = undefined;
          } else {
            let sum = 0;

            restKyNames.forEach(kyName => {
              const valueByKey = get(itemValue, kyName, '0') as string;

              const value = isNaN(parseFloat(valueByKey))
                ? 0
                : parseFloat(valueByKey);
              sum += value;
            });
            convertedValue = toString(sum);
          }
        } else {
          //combineField: "{accountNumberField} - {cityField}"
          kyNames.forEach(kyName => {
            const valueByKey = toString(get(itemValue, kyName, ''));
            const kyPattern = new RegExp(`{${kyName}}`, 'gi');
            convertedValue = convertedValue.replace(
              kyPattern,
              valueByKey.trim()
            );
          });
        }
        inputValue = convertedValue;
      }

      inputValue = isString(inputValue) ? inputValue.trim() : inputValue;

      inputKy && set(result, outputKy, inputValue);
    });
  }
  return result;
};

/**
 * - ...
 * @param {array} input Data
 * @param {object} mapping Model
 * @returns {boolean} is reverse the mapping
 */
export const mappingDataFromArray = <R>(
  data: MagicKeyValue[],
  mappingModel: object,
  isRevert = false
): R[] => {
  if (!isArray(data) || isEmpty(data) || isEmpty(mappingModel)) return [];
  return data.map(itemValue =>
    mappingDataFromObj(itemValue, mappingModel, isRevert)
  );
};

/**
 * Convert object mapping to Group array key value
 *
 * Input:
 * mappingData = {
 *     'group1.key1.key'
 *     'group1.key2.key'
 *     'group2.key1.key'
 *     'group3.key'
 * }
 * Output:
 * {
 *     group1: ['key1.key', 'key2.key'],
 *     group2: ['key1.key']
 *     group3: [key]
 * }
 *
 */

export const getGroupMappingData = (mappingData: MagicKeyValue) => {
  return Object.keys(mappingData).reduce((prev: MagicKeyValue, key) => {
    const [groupKey, ...rest] = key.split('.');
    const valueKey = rest.join('.');

    if (!prev[groupKey]) {
      prev[groupKey] = [];
    }

    return { ...prev, [groupKey]: [...prev[groupKey], valueKey] };
  }, {});
};

export const parseRawData = (data: MagicKeyValue = {}, deepParse = false) => {
  return Object.keys(data).reduce((prev, key) => {
    const item = data[key];
    let result = item;
    if (isString(item)) {
      result = item.trim();
      const regex = new RegExp(atob('Xi0/XGQqXC4/XGQqJA=='));
      const isNumberString = regex.test(result);
      if (isNumberString) {
        return { ...prev, [key]: Number(result) };
      }

      const isObjectString = result.includes('{');
      if (isObjectString) {
        return { ...prev, [key]: JSON.parse(result) };
      }
    }

    return { ...prev, [key]: result };
  }, {});
};
