import { copyToClipboard } from './copyToClipboard';

describe('test copyToClipboard function', () => {
  it('should be working correctly', () => {
    const mWrite = jest.fn();

    Object.assign(window, { ClipboardItem: jest.fn() });

    Object.assign(navigator, { clipboard: { write: mWrite } });

    copyToClipboard('abc');

    expect(mWrite).toBeCalled();
  });
});
