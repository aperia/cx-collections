import {
  mappingDataFromArray,
  mappingDataFromObj,
  getGroupMappingData,
  parseRawData
} from './mappingData';

describe('Test mappingData.test.ts', () => {
  describe('mappingDataFromObj function ', () => {
    it('Test sum field', () => {
      const itemValue = { a: 0, b: 0, c: 100, d: 'd' };
      const mappingModel = { nameKey: '{SUM}{a}{b}', valueKey: '{SUM}{c}{d}' };
      const result = mappingDataFromObj(itemValue, mappingModel);
      expect(result).toEqual({ nameKey: '0', valueKey: '100' });
    });

    it('Test sum field invalid field', () => {
      const itemValue = { a: 0, b: 0, c: 'c100', d: '' };
      const mappingModel = { nameKey: '{SUM}{a}{b}', valueKey: '{SUM}{c}{d}' };
      const result = mappingDataFromObj(itemValue, mappingModel);
      expect(result).toEqual({ nameKey: '0', valueKey: undefined });
    });

    it('Empty case', () => {
      const result = mappingDataFromObj({}, {});
      expect(result).toEqual({});
    });

    it('Revert case', () => {
      const itemValue = { name: 'name', id: 'id' };
      const mappingModel = { nameKey: 'name', idKey: 'id' };
      const result = mappingDataFromObj(itemValue, mappingModel, true);
      expect(result).toEqual({});
    });

    it('Revert case with data result', () => {
      const itemValue = { nameKey: 'nameKeyValue', idKey: 'idKeyValue' };
      const mappingModel = { nameKey: 'name', idKey: 'id' };
      const result = mappingDataFromObj(itemValue, mappingModel, true);
      expect(result).toEqual({ name: 'nameKeyValue', id: 'idKeyValue' });
    });

    it('Normal case with data', () => {
      const itemValue = { key01: 'key01Value', key02: 'key02Value' };
      const mappingModel = { ourKey01: 'notExistingKey', ourKey02: 'key02' };

      const result = mappingDataFromObj(itemValue, mappingModel);
      expect(result).toEqual({
        ourKey01: undefined,
        ourKey02: itemValue.key02
      });
    });

    it('custom field when mapping', () => {
      const itemValue = {
        name: { first: 'john', last: 'smith' },
        key02: { nestKey02: 'nestKey02Value' }
      };

      const mappingModel = {
        fullName: '{name.first} {name.last}',
        newField02: 'key02.nestKey02'
      };

      const { fullName, newField02 } =
        mappingDataFromObj(itemValue, mappingModel) || {};
      expect(fullName).toEqual('john smith');
      expect(newField02).toEqual('nestKey02Value');
    });
  });

  describe('mappingDataFromArray function ', () => {
    it('Empty case', () => {
      const valueObj: MagicKeyValue[] = [];
      const jsonModel = { name: 'name' };
      const jsonEmpty = {};

      const resultValueEmpty = mappingDataFromArray([], jsonModel);
      expect(resultValueEmpty).toEqual([]);

      const resultValueObj = mappingDataFromArray(valueObj, jsonModel);
      expect(resultValueObj).toEqual([]);

      const resultJsonEmpty = mappingDataFromArray(valueObj, jsonEmpty);
      expect(resultJsonEmpty).toEqual([]);
    });

    it('Normal case with data', () => {
      const mockValues = [
        {
          key01: 'name-01',
          uniqueId: 'uniqueId-01'
        },
        {
          key01: 'name-02',
          uniqueId: 'uniqueId-02'
        }
      ];

      const mappingModel = { newField: 'key01', id: 'uniqueId' };

      const result = mappingDataFromArray(
        mockValues,
        mappingModel
      ) as MagicKeyValue[];

      expect(result.length).toEqual(mockValues.length);
      expect(result[0]?.newField).toEqual('name-01');
    });
  });

  describe('getGroupMappingData', () => {
    it('should be return data correctly', () => {
      const sampleInit = {
        'a.name.key': '123',
        'b.name': '1234',
        c: '1234',
        'a.name.title': 'test'
      };
      const result = getGroupMappingData(sampleInit);
      expect(result).toEqual({
        a: ['name.key', 'name.title'],
        b: ['name'],
        c: ['']
      });
    });
  });

  describe('parseRawData', () => {
    it('should set default parameter', () => {
      const result = parseRawData();
      expect(result).toEqual({});
    });
    it('should be return data correctly', () => {
      const sampleInit = {
        name: 'John',
        age: 1234,
        portalCode: '70000',
        address: JSON.stringify({ isCity: false }),
        testEmpty: JSON.stringify({})
      };
      const result = parseRawData(sampleInit);
      expect(result).toEqual({
        age: 1234,
        name: 'John',
        portalCode: 70000,
        address: { isCity: false },
        testEmpty: {}
      });
    });

    it('should be return data empty object', () => {
      const result = parseRawData();
      expect(result).toEqual({});
    });
  });
});
