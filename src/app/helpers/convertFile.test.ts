import * as helpers from './convertFile';

class FileReaderThrowError extends FileReader {
  RealFileReader;

  constructor() {
    super();
    this.RealFileReader = FileReader;
  }

  readAsDataURL() {
    this.onerror && this.onerror({} as ProgressEvent<FileReader>);
  }
}

const RealFileReader = FileReader;

describe('test convertFileToBase64', () => {
  it('should return base 64 data with success', async () => {
    window.FileReader = RealFileReader;

    const file = new File(['1'], '.xml.pdf', { type: '' });
    const dataBase64 = await helpers.convertFileToBase64(file);
    expect(dataBase64).toEqual('MQ==');
  });

  it('should return base 64 data with error', async () => {
    window.FileReader = FileReaderThrowError;

    const file = new File(['1'], '.xml.pdf', { type: '' });
    try {
      await helpers.convertFileToBase64(file);
    } catch (e) {
      expect(e).toEqual({});
    }
  });
});
