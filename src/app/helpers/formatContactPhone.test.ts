import { formatContactPhone } from './formatContactPhone';

describe('Test formatContactPhone.test.ts', () => {

  it('value is empty string', () => {
    const value = '';

    const result = formatContactPhone(value);
    expect(result).toEqual(value);
  });
  describe('formatContactPhone function ', () => {
    it('value is not string number', () => {
      const value = 'test string';

      const result = formatContactPhone(value);
      expect(result).toEqual(value);
    });
    it('convert string number to string format phone ', () => {
      const value = '0885864896';

      const result = formatContactPhone(value);
      expect(result).toEqual('(088) 586-4896');
    });
    it('convert string number to string format phone ', () => {
      const value = '0885864896';

      const result = formatContactPhone(value);
      expect(result.indexOf('(')).toEqual(0);
      expect(result.indexOf(')')).toEqual(4);
      expect(result.indexOf(' ')).toEqual(5);
    });
  });
});
