import { createStore, applyMiddleware } from 'redux';
import { rootReducer } from 'storeConfig';
import thunk from 'redux-thunk';

// Helper
import { removeStoreRedux } from 'app/helpers';
import { storeId } from 'app/test-utils';
import { initialState as initialStatePromisesPaymentsStatistics } from 'pages/Reporting/PromisesPaymentsStatistics/_redux/reducers';
import { initialState as initialStateCollectorProductivity } from 'pages/Reporting/CollectorProductivity/_redux/reducers';
import { initialState as initialStateReportingTab } from 'pages/Reporting/ReportingTabs/_redux/reducers';
import { initialState as initialStateHistory } from 'pages/ClientConfiguration/ChangeHistory/_redux/reducers';

describe('Test removeStore.test.ts', () => {
  it('test for case tabType: accountSearchList', () => {
    const storeAsync = createStore(
      rootReducer,
      {
        accountSearchList: {
          storeId: { data: [] }
        }
      },
      applyMiddleware(thunk)
    );
    storeAsync.dispatch<any>(removeStoreRedux('accountSearchList', storeId));
    expect(storeAsync.getState().accountSearchList[storeId]).toEqual(undefined);
  });

  it('test for case tabType: accountDetail', () => {
    const storeAsync = createStore(
      rootReducer,
      {
        accountCollection: {
          [storeId]: { data: [] }
        },
        accountInformation: {
          [storeId]: { data: [] }
        }
      },
      applyMiddleware(thunk)
    );
    storeAsync.dispatch<any>(removeStoreRedux('accountDetail', storeId));
    expect(storeAsync.getState().accountCollection[storeId]).toEqual(undefined);
    expect(storeAsync.getState().accountInformation[storeId]).toEqual(
      undefined
    );
  });
  it('test for case tabType: default case', () => {
    const storeAsync = createStore(
      rootReducer,
      {
        accountCollection: {
          [storeId]: { data: [] }
        },
        accountInformation: {
          [storeId]: { data: [] }
        }
      },
      applyMiddleware(thunk)
    );
    storeAsync.dispatch<any>(removeStoreRedux('default_case', storeId));
    expect(storeAsync.getState().accountCollection[storeId]).toEqual({
      data: []
    });
    expect(storeAsync.getState().accountInformation[storeId]).toEqual({
      data: []
    });
  });

  it('test for case tabType: transactionAdjustment', () => {
    const storeAsync = createStore(
      rootReducer,
      {
        transactionAdjustment: {
          currentRequest: '123'
        }
      },
      applyMiddleware(thunk)
    );
    storeAsync.dispatch<any>(
      removeStoreRedux('transactionAdjustment', storeId)
    );
    expect(storeAsync.getState().transactionAdjustment).toEqual({
      adjustmentDetail: {},
      currentRequest: 'pending',
      isError: false,
      isLoading: false,
      isLoadingView: false,
      openModal: false,
      sortBy: {
        id: 'operatorDateTime'
      },
      transactionAdjustmentRequestList: []
    });
  });

  it('test for case tabType: accountRelated', () => {
    const storeAsync = createStore(
      rootReducer,
      {
        accountDetail: {
          [storeId]: {}
        },
        relatedAccount: {
          [storeId]: {}
        }
      },
      applyMiddleware(thunk)
    );
    storeAsync.dispatch<any>(removeStoreRedux('accountRelated', storeId));
    expect(storeAsync.getState().accountDetail[storeId]).toEqual(undefined);
    expect(storeAsync.getState().relatedAccount[storeId]).toEqual(undefined);
  });

  it('test for case tabType: clientConfiguration', () => {
    const storeAsync = createStore(
      rootReducer,
      {
        clientConfiguration: {
          list: {
            loading: true
          }
        }
      },
      applyMiddleware(thunk)
    );
    storeAsync.dispatch<any>(removeStoreRedux('clientConfiguration'));
    expect(storeAsync.getState().clientConfiguration).toEqual({
      list: undefined
    });
  });

  it('test for case tabType: reporting', () => {
    const storeAsync = createStore(
      rootReducer,
      {
        collectorProductivity: {
          isCollapsed: true,
          isMounted: true
        },
        promisesPaymentsStatistics: {
          isCollapsed: true,
          isMounted: true
        },
        collectorsTeamsComboBox: {
          lazyLoadComboBox: {
            opened: true
          }
        },
        reportingTabs: {
          activeTab: undefined
        }
      },
      applyMiddleware(thunk)
    );
    storeAsync.dispatch<any>(removeStoreRedux('reporting'));
    expect(storeAsync.getState().collectorsTeamsComboBox).toEqual({
      lazyLoadComboBox: {
        filter: '',
        opened: false
      }
    });
    expect(storeAsync.getState().collectorProductivity).toEqual(
      initialStateCollectorProductivity
    );
    expect(storeAsync.getState().promisesPaymentsStatistics).toEqual(
      initialStatePromisesPaymentsStatistics
    );
    expect(storeAsync.getState().reportingTabs).toEqual(
      initialStateReportingTab
    );
  });

  it('test for case tabType: changeHistory', () => {
    const storeAsync = createStore(
      rootReducer,
      {
        collectorProductivity: {
          isCollapsed: true,
          isMounted: true
        },
        promisesPaymentsStatistics: {
          isCollapsed: true,
          isMounted: true
        },
        collectorsTeamsComboBox: {
          lazyLoadComboBox: {
            opened: true
          }
        },
        reportingTabs: {
          activeTab: undefined
        }
      },
      applyMiddleware(thunk)
    );
    storeAsync.dispatch<any>(removeStoreRedux('changeHistory'));
    expect(storeAsync.getState().collectorsTeamsComboBox).toEqual({
      lazyLoadComboBox: {
        opened: true
      }
    });
    expect(storeAsync.getState().changeHistory).toEqual(initialStateHistory);
  });

  it('test for case tabType: queueRoleMapping', () => {
    const storeAsync = createStore(
      rootReducer,
      {
        clientConfigQueueAndRoleMapping: {
          loading: false,
          loadingModal: false,
          loadingQueueName: false,
          queueAndRoleList: [
            {
              queueId: '1',
              queueName: 'queueName'
            }
          ],
          queueAndRole: {},
          error: '',
          errorModal: '',
          errorQueueName: '',
          isOpenModal: false,
          isOpenModalDelete: false,
          sortBy: { id: 'queueName', order: 'desc' },
          queueNameList: [],
          userRoleList: [],
          loadingUserRoleList: false,
          textSearch: 'test',
          selectedUserRole: {
            value: 'CXCOL1',
            description: 'CXCOL1'
          }
        }
      },
      applyMiddleware(thunk)
    );
    storeAsync.dispatch<any>(removeStoreRedux('queueRoleMapping'));
    expect(storeAsync.getState().clientConfigQueueAndRoleMapping).toEqual({
      error: '',
      errorModal: '',
      errorQueueName: '',
      isOpenModal: false,
      isOpenModalDelete: false,
      loading: false,
      loadingModal: false,
      loadingQueueName: false,
      loadingUserRoleList: false,
      queueAndRole: undefined,
      queueAndRoleList: [],
      queueNameList: [],
      selectedUserRole: undefined,
      sortBy: {
        id: 'queueName',
        order: undefined
      },
      textSearch: '',
      userRoleList: []
    });
  });
});
