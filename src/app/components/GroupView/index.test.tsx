import React from 'react';
import GroupView from './index';
import { render, screen } from '@testing-library/react';
import { queryByClass } from 'app/test-utils';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

jest.mock('app/_libraries/_dof/core/View', () =>
  jest.requireActual('app/test-utils/mocks/MockView')
);

describe('Test GroupView component', () => {
  it('render default', () => {
    const wrapper = render(<GroupView descriptor="descriptor" />);

    expect(
      queryByClass(wrapper.container, /color-grey mt-16/)
    ).toBeInTheDocument();
  });

  it('render component > header is object', () => {
    render(
      <GroupView
        descriptor="descriptor"
        header={{ input: { value: 'Header Text' } }}
      />
    );

    expect(screen.getByText('Header Text')).toBeInTheDocument();
  });

  it('render component > empty data', () => {
    const Footer = <div>Footer</div>;

    render(
      <GroupView descriptor="descriptor" header="Header" footer={Footer} />
    );

    expect(screen.getByText('Header')).toBeInTheDocument();
    expect(screen.getByText('txt_no_data')).toBeInTheDocument();
    expect(screen.getByText('Footer')).toBeInTheDocument();
  });

  it('render component > with data', () => {
    const Footer = <div>Footer</div>;
    const testData = {
      testField: true
    };

    render(
      <GroupView
        descriptor="descriptor"
        header="Header"
        footer={Footer}
        data={testData}
        fields={['testField']}
      />
    );

    expect(screen.getByText('Header')).toBeInTheDocument();
    expect(screen.getByTestId('descriptor')).toBeInTheDocument();
  });

  it('render component > with data and check only one field', () => {
    const Footer = <div>Footer</div>;
    const testData = {
      testField: true
    };

    render(
      <GroupView
        descriptor="descriptor"
        header="Header"
        footer={Footer}
        data={testData}
        fields={['testField']}
        onlyFields={['testField']}
      />
    );

    expect(screen.getByText('Header')).toBeInTheDocument();
    expect(screen.getByTestId('descriptor')).toBeInTheDocument();
  });
});
