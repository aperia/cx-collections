import React, { ReactElement, useMemo } from 'react';

// components
import { CommonControlProps, View } from 'app/_libraries/_dof/core';
import HeadingControl from 'app/components/_dof_controls/controls/HeadingControl';
import DividerControl from 'app/components/_dof_controls/controls/DividerControl';

// hooks
import { useAccountDetail } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import get from 'lodash.get';
import isObject from 'lodash.isobject';
import isString from 'lodash.isstring';

export interface GroupViewProps {
  descriptor: string;
  fields?: string[];
  onlyFields?: string[];
  data?: Object;
  header?: string | Object;
  contentEmpty?: ReactElement | string;
  footer?: ReactElement | null;
  dataTestId?: string;
}

export const isEmptyFields = (
  data: Object = {},
  fields: string[] = [],
  onlyFields: string[] = []
) => {
  return (
    fields.every((field: string) => !get(data, field)) ||
    (onlyFields.length > 0 &&
      onlyFields.every((onlyField: string) => !get(data, onlyField)))
  );
};

const GroupView: React.FC<GroupViewProps> = ({
  fields,
  onlyFields,
  data,
  descriptor,
  header,
  contentEmpty,
  dataTestId,
  footer
}) => {
  const { t } = useTranslation();
  const { accEValue } = useAccountDetail();

  const formKey = `${accEValue}-${descriptor}`;
  const isEmptyValue = isEmptyFields(data, fields, onlyFields);

  const headerView = useMemo(() => {
    const props = {
      input: {},
      meta: {}
    } as CommonControlProps;
    if (isString(header)) {
      return (
        <HeadingControl
          {...props}
          label={header}
          id={`${formKey}_header`}
          dataTestId={dataTestId}
        />
      );
    }
    if (isObject(header)) {
      return (
        <HeadingControl
          {...props}
          {...header}
          id={`${formKey}_header`}
          dataTestId={dataTestId}
        />
      );
    }

    return header;
  }, [formKey, header, dataTestId]);

  const contentView = useMemo(() => {
    if (isEmptyValue) {
      return (
        contentEmpty || <p className="color-grey mt-16">{t('txt_no_data')}</p>
      );
    }

    return (
      <View
        id={`${accEValue}_${descriptor}_view`}
        formKey={formKey}
        descriptor={descriptor}
        value={data}
      />
    );
  }, [isEmptyValue, accEValue, descriptor, formKey, data, contentEmpty, t]);

  const footerView = useMemo(() => {
    if (isEmptyValue) {
      const props = {} as CommonControlProps;
      return (
        footer || (
          <DividerControl
            {...props}
            id={`${accEValue}_${descriptor}_divider_view`}
          />
        )
      );
    }
    return null;
  }, [accEValue, descriptor, footer, isEmptyValue]);

  return (
    <>
      {headerView}
      {contentView}
      {footerView}
    </>
  );
};

export default GroupView;
