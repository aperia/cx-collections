import React from 'react';
import classnames from 'classnames';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_COMMON_TEXT } from 'app/constants/i18n';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface ApiErrorProp {
  onClickViewErrorDetail?: () => void;
  className?: string;
  dataTestId?: string;
}

const ApiError = React.forwardRef<HTMLDivElement, ApiErrorProp>(
  ({ onClickViewErrorDetail, className, dataTestId }, ref) => {
    const { t } = useTranslation();

    const testId = genAmtId(dataTestId!, 'api-error', 'ApiError');

    const handleClickViewDetail = () => {
      if (typeof onClickViewErrorDetail === 'function')
        onClickViewErrorDetail();
    };

    return (
      <div className={classnames('api-error-detail', className)} ref={ref}>
        <div className="d-inline-block alert alert-danger mb-0">
          <span>{t(I18N_COMMON_TEXT.API_ERROR)}</span>
          <a
            onClick={handleClickViewDetail}
            className="link text-decoration-none ml-4"
            data-testid={testId}
          >
            {t('txt_view_detail')}
          </a>
        </div>
      </div>
    );
  }
);

export default ApiError;
