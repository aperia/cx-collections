import React from 'react';

// RTL
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

// components
import ApiError from './index';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

describe('Test ApiError component', () => {
  it('render component', () => {
    render(<ApiError />);

    userEvent.click(screen.getByText('txt_view_detail'));

    expect(screen.getByText('txt_api_error')).toBeInTheDocument();
    expect(screen.getByText('txt_view_detail')).toBeInTheDocument();
  });

  it('click on ', () => {
    const onClickViewErrorDetailSpy = jest.fn();

    render(<ApiError onClickViewErrorDetail={onClickViewErrorDetailSpy} />);

    userEvent.click(screen.getByText('txt_view_detail'));

    expect(onClickViewErrorDetailSpy).toBeCalled();
  });
});
