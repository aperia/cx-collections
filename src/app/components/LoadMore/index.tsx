import React, { useEffect, useCallback, useState, useRef } from 'react';
import classnames from 'classnames';
import get from 'lodash.get';
import isFunction from 'lodash.isfunction';
import { getScrollParent } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface LoadMoreProps {
  dataTestId?: string;
  loading: boolean;
  onLoadMore: () => void;
  isEnd: boolean;
  onBackToTop?: () => void;
  className?: string;
  loadingText?: string;
  endLoadMoreText?: string;
  hideBackToTop?: boolean;
}

const LoadMore: React.FC<LoadMoreProps> = ({
  dataTestId,
  loading,
  onLoadMore,
  onBackToTop: onBackToTopProp,
  isEnd,
  className,
  loadingText = 'txt_loading_more_results',
  endLoadMoreText = 'txt_end_of_result_list',
  hideBackToTop = false,
  ...props
}) => {
  const { t } = useTranslation();
  const [loadMoreElement, setLoadMoreElement] =
    useState<HTMLElement | null>(null);
  const [isShowLoadMore, setIsShowLoadMore] = useState<boolean>(false);
  const scrollParentRef = useRef<HTMLElement | null>(null);

  const handleIntersection = useCallback(
    entries => {
      const { isIntersecting } = get(entries, [0]);

      if (!isIntersecting) return;

      // Do Not show LoadMore if they don't have scroll
      scrollParentRef.current = getScrollParent(loadMoreElement);
      const scrollHeight = scrollParentRef.current!.scrollHeight;
      const clientHeight = scrollParentRef.current!.clientHeight;
      const isShowLoadMore = scrollHeight > clientHeight;
      setIsShowLoadMore(isShowLoadMore);

      if (isFunction(onLoadMore) && !loading && isShowLoadMore && !isEnd) {
        onLoadMore();
      }
    },
    [onLoadMore, loading, isEnd, loadMoreElement]
  );

  const handleBackToTop = () => {
    if (isFunction(onBackToTopProp)) return onBackToTopProp();

    scrollParentRef &&
      scrollParentRef.current &&
      scrollParentRef.current.scrollTo({ top: 0, behavior: 'smooth' });
  };

  useEffect(() => {
    const options: IntersectionObserverInit = {
      root: null,
      rootMargin: '24px',
      threshold: 0.9
    };

    if (loadMoreElement) {
      const observer = new IntersectionObserver(handleIntersection, options);
      observer.observe(loadMoreElement);
      return () => observer.unobserve(loadMoreElement);
    }
  }, [loadMoreElement, handleIntersection]);

  const testId = genAmtId(dataTestId!, 'load-more', 'LoadMore');

  return (
    <>
      {/* element to watch show on Window */}
      <span className="element-to-watch" ref={setLoadMoreElement} />
      {isShowLoadMore && (
        <div
          className={classnames(
            className && className,
            (loading || isEnd) &&
              'd-flex align-items-center justify-content-center fs-14 mt-16'
          )}
          data-testid={testId}
        >
          {loading && (
            <>
              <span className="loading loading-sm mr-16" />
              <span>{t(loadingText)}</span>
            </>
          )}
          {!loading && isEnd && !hideBackToTop && (
            <>
              <span className="mr-2">{t(endLoadMoreText)}</span>
              <span
                className="link text-decoration-none"
                onClick={handleBackToTop}
                data-testid={genAmtId(
                  testId!,
                  'load-more-back-to-top',
                  'LoadMore'
                )}
              >
                {t('txt_back_to_top')}
              </span>
            </>
          )}
        </div>
      )}
    </>
  );
};

export default LoadMore;
