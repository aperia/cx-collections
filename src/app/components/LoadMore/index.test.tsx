import React from 'react';
import LoadMore from './index';
import { fireEvent, screen } from '@testing-library/react';
import { queryByClass, renderMockStoreId } from 'app/test-utils';
import intersectionObserver from 'app/test-utils/mocks/intersectionObserver';
import {
  mockClientHeight,
  mockScrollHeight
} from 'app/test-utils/mocks/mockProperty';

describe('Test LoadMore component', () => {
  it('Should have null UI with loading is true', () => {
    Object.defineProperty(window, 'IntersectionObserver', {
      writable: true,
      configurable: true,
      value: intersectionObserver({})
    });

    const loadingText = 'loadingText';

    renderMockStoreId(
      <LoadMore
        loading={true}
        onLoadMore={() => {}}
        isEnd={false}
        loadingText={loadingText}
      />
    );

    const queryText = screen.queryByText(loadingText);
    expect(queryText).not.toBeInTheDocument();
  });

  it('Should have null UI scrollHeight and clientHeight', () => {
    Object.defineProperty(window, 'IntersectionObserver', {
      writable: true,
      configurable: true,
      value: intersectionObserver({})
    });

    mockScrollHeight(90);
    mockClientHeight(100);

    const loadingText = 'loadingText';

    renderMockStoreId(
      <LoadMore
        loading={false}
        onLoadMore={() => {}}
        isEnd={false}
        loadingText={loadingText}
      />
    );
    const queryText = screen.queryByText(loadingText);
    expect(queryText).not.toBeInTheDocument();
  });

  it('Should have onLoadMore', () => {
    Object.defineProperty(window, 'IntersectionObserver', {
      writable: true,
      configurable: true,
      value: intersectionObserver({})
    });

    mockScrollHeight(100);
    mockClientHeight(90);
    const onLoadMore = jest.fn();

    const { wrapper } = renderMockStoreId(
      <LoadMore
        loading={false}
        onLoadMore={onLoadMore}
        isEnd={false}
        className="text"
      />,
      {},
      false
    );

    const queryText = queryByClass(wrapper.container, /text/);
    const queryElementToWatch = queryByClass(
      wrapper.container,
      /element-to-watch/
    );
    fireEvent.scroll(queryElementToWatch!, {
      target: {},
      currentTarget: { scrollBottom: 0 }
    });
    expect(queryText).toBeInTheDocument();
    expect(onLoadMore).toHaveBeenCalled();
  });

  it('Should have loading', () => {
    Object.defineProperty(window, 'IntersectionObserver', {
      writable: true,
      configurable: true,
      value: intersectionObserver({})
    });

    mockScrollHeight(100);
    mockClientHeight(90);
    const onLoadMore = jest.fn();
    const loadingText = 'loading-text';
    renderMockStoreId(
      <LoadMore
        loading={true}
        onLoadMore={onLoadMore}
        isEnd={false}
        loadingText={loadingText}
      />,
      {},
      false
    );

    const queryText = screen.queryByText(loadingText);

    expect(queryText).toBeInTheDocument();
  });

  it('Should have loading with text custom', () => {
    Object.defineProperty(window, 'IntersectionObserver', {
      writable: true,
      configurable: true,
      value: intersectionObserver({})
    });

    mockScrollHeight(100);
    mockClientHeight(90);
    const onLoadMore = jest.fn();

    const { wrapper } = renderMockStoreId(
      <LoadMore loading={true} onLoadMore={onLoadMore} isEnd={false} />,
      {},
      false
    );

    const queryText = screen.queryByText(/txt_loading_more_results/);

    const queryLoading = queryByClass(wrapper.container, /loading/);
    expect(queryLoading).toBeInTheDocument();
    expect(queryText).toBeInTheDocument();
  });

  it('Should haven intersection is false', () => {
    Object.defineProperty(window, 'IntersectionObserver', {
      writable: true,
      configurable: true,
      value: intersectionObserver({ entries: { isIntersecting: false } })
    });

    mockScrollHeight(100);
    mockClientHeight(90);
    const onLoadMore = jest.fn();

    const { wrapper } = renderMockStoreId(
      <LoadMore loading={false} onLoadMore={onLoadMore} isEnd={false} />,
      {},
      false
    );

    const queryText = queryByClass(wrapper.container, /element-to-watch/);
    fireEvent.scroll(queryText!, {
      target: {},
      currentTarget: { scrollBottom: 0 }
    });
    expect(onLoadMore).not.toHaveBeenCalled();
  });

  it('Should have prop is onBackToTop function', () => {
    Object.defineProperty(window, 'IntersectionObserver', {
      writable: true,
      configurable: true,
      value: intersectionObserver({})
    });

    const mockScrollTo = jest.fn();
    Element.prototype.scrollTo = mockScrollTo;

    mockScrollHeight(100);
    mockClientHeight(90);
    renderMockStoreId(
      <LoadMore loading={false} onLoadMore={() => {}} isEnd={true} />,
      {},
      false
    );

    const queryTextBackToTop = screen.queryByText(/txt_back_to_top/);
    const queryTextEndResult = screen.queryByText(/txt_end_of_result_list/);
    queryTextBackToTop!.click();
    expect(queryTextEndResult).toBeInTheDocument();
    expect(mockScrollTo).toHaveBeenCalledWith({ top: 0, behavior: 'smooth' });
  });

  it('Should have prop is onBackToTop function', () => {
    Object.defineProperty(window, 'IntersectionObserver', {
      writable: true,
      configurable: true,
      value: intersectionObserver({})
    });

    const mockScrollTo = jest.fn();
    Element.prototype.scrollTo = mockScrollTo;
    const onBackToTopSpy = jest.fn();

    mockScrollHeight(100);
    mockClientHeight(90);
    renderMockStoreId(
      <LoadMore
        loading={false}
        onLoadMore={() => {}}
        isEnd={true}
        onBackToTop={onBackToTopSpy}
      />,
      {},
      false
    );

    const queryTextBackToTop = screen.queryByText(/txt_back_to_top/);
    const queryTextEndResult = screen.queryByText(/txt_end_of_result_list/);
    queryTextBackToTop!.click();
    expect(queryTextEndResult).toBeInTheDocument();
    expect(onBackToTopSpy).toBeCalled();
  });

  it('Should have prop is endLoadMoreText', () => {
    Object.defineProperty(window, 'IntersectionObserver', {
      writable: true,
      configurable: true,
      value: intersectionObserver({})
    });

    const mockScrollTo = jest.fn();
    Element.prototype.scrollTo = mockScrollTo;

    mockScrollHeight(100);
    mockClientHeight(90);
    const endLoadMoreText = 'test-end-load-more';
    renderMockStoreId(
      <LoadMore
        loading={false}
        endLoadMoreText={endLoadMoreText}
        onLoadMore={() => {}}
        isEnd={true}
      />,
      {},
      false
    );
    const queryTextEndResult = screen.queryByText(endLoadMoreText);
    expect(queryTextEndResult).toBeInTheDocument();
  });
});
