import React from 'react';
import { formatTime } from 'app/helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface DateTimeGridProps {
  date: string;
  time: string;
  dataTestId?: string;
}

export const DateTimeGrid: React.FC<DateTimeGridProps> = ({
  date,
  time,
  dataTestId
}) => {
  const formattedDate = formatTime(date).date;
  const formattedTime = formatTime(`${date} ${time}`).fullTimeMeridiem;
  const testId = genAmtId(
    dataTestId!,
    `date-time-grid-${date}-${time}`,
    'DateTimeGrid'
  );
  return (
    <div className="d-flex flex-column" data-testid={testId}>
      <span className="color-grey-d20 fs-14">{formattedDate}</span>
      <span className="color-grey fs-14">{formattedTime}</span>
    </div>
  );
};
