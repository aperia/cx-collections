import React from 'react';
import { DateTimeGrid } from './index';
import { render } from '@testing-library/react';

describe('Test DateTimeGrid component', () => {
  it('render default component', () => {
    const dateValue = '06/01/2021';
    const timeValue = '19:00';

    const wrapper = render(<DateTimeGrid date={dateValue} time={timeValue} />);

    expect(
      wrapper.container.querySelector('span[class="color-grey-d20 fs-14"]')
        ?.innerHTML
    ).toEqual(dateValue);
    expect(
      wrapper.container.querySelector('span[class="color-grey fs-14"]')
        ?.innerHTML
    ).toEqual('07:00:00 PM');
  });
});
