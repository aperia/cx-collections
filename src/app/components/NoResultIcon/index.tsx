import React, { ReactElement } from 'react';

// components
import { IconProps, Button } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface NoResultIconProps {
  dataTestId?: string;
  description: string;
  icon: ReactElement<IconProps>;
  onReset?: () => void;
  resetText?: string;
}

const NoResultIcon: React.FC<NoResultIconProps> = ({
  dataTestId,
  icon,
  description,
  onReset,
  resetText
}) => {
  const { t } = useTranslation();
  return (
    <div className="d-flex flex-column align-items-center">
      {icon}
      <p id="noResultDescription" className="fs-14 color-grey mt-16">
        {t(description)}
      </p>
      {!!resetText?.length && (
        <Button
          id="resetText"
          className="mt-24"
          size="sm"
          variant="outline-primary"
          onClick={onReset}
          dataTestId={genAmtId(dataTestId!, 'no-result-icon-reset-btn', 'NoResultIcon')}
        >
          {t(resetText)}
        </Button>
      )}
    </div>
  );
};

export default NoResultIcon;
