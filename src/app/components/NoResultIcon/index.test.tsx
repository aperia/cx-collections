import React from 'react';
import NoResultIcon from './index';
import { screen } from '@testing-library/react';
import { queryByClass, renderMockStoreId } from 'app/test-utils';
import { Icon } from 'app/_libraries/_dls/components';

describe('Test NoResultIcon component', () => {
  it('Should have render UI default', () => {
    const description = 'test-description-no-result-icon';
    const { wrapper } = renderMockStoreId(
      <NoResultIcon
        icon={<Icon className="color-light-l12 fs-80" name="comment" />}
        description={description}
      />
    );
    const queryText = screen.queryByText(description);
    const queryClassComment = queryByClass(
      wrapper.container,
      /icon icon-comment/
    );
    expect(queryClassComment).toBeInTheDocument();
    expect(queryText).toBeInTheDocument();
  });

  it('Should have render with resetText', () => {
    const resetText = 'test-reset-text';
    const onReset = jest.fn();
    renderMockStoreId(
      <NoResultIcon
        icon={<Icon className="color-light-l12 fs-80" name="comment" />}
        description={'description'}
        onReset={onReset}
        resetText={resetText}
      />
    );
    const queryText = screen.queryByText(resetText);
    queryText!.click();
    expect(onReset).toHaveBeenCalled;
    expect(queryText).toBeInTheDocument();
  });
});
