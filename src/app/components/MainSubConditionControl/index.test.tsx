import React from 'react';
import MainSubConditionControl from './index';
import { render, screen } from '@testing-library/react';
import { mainSubConditionData } from 'pages/AccountSearch/constants';

jest.mock('app/_libraries/_dls/hooks/useTranslation', () => {
  return () => ({ t: (text: string) => text });
});

HTMLCanvasElement.prototype.getContext = jest.fn();

describe('Test MainSubConditionControl component', () => {
  it('render', () => {
    const { rerender } = render(
      <MainSubConditionControl data={mainSubConditionData} />
    );
    rerender(<MainSubConditionControl data={mainSubConditionData} />);

    expect(screen.getByText('txt_exact')).toBeInTheDocument();
    expect(screen.getByText('txt_moderate')).toBeInTheDocument();
    expect(screen.getByText('txt_low')).toBeInTheDocument();
  });
});
