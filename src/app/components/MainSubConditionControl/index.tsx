import React from 'react';

import { useTranslation } from 'app/_libraries/_dls/hooks';
import {
  MainSubConditionControl as MainSubConditionControlDLS,
  MainSubConditionLevel
} from 'app/_libraries/_dls/components';

const MainSubConditionControl: React.FC<any> = ({
  placeholder,
  value,
  data,
  autoFocusDropdownList,
  autoFocusInput,
  dataTestId,
  ...props
}) => {
  const { t } = useTranslation();
  const translateData = data.map((item: MainSubConditionLevel) => ({
    ...item,
    text: t(item.text)
  }));
  return (
    <MainSubConditionControlDLS
      dataTestId={dataTestId}
      data={translateData}
      value={value}
      inputProps={{ placeholder: t(placeholder) }}
      autoFocusDropdownList={autoFocusDropdownList}
      autoFocusInput={autoFocusInput}
      {...props}
    />
  );
};

export default MainSubConditionControl;
