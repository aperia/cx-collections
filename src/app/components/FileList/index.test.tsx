import React from 'react';
import { renderMockStoreId } from 'app/test-utils';
import FileList from './index';
import { screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

const files = [
  {
    documentName: 'searchAccount.png',
    documentObjectId: '9b794575-ad90-45c8-a25b-8ecc6365243a'
  }
];

describe('FileList', () => {
  it('render with data files ', () => {
    const mockOnDownLoad = jest.fn();
    renderMockStoreId(<FileList files={files} onDownload={mockOnDownLoad} />);
    expect(screen.queryByText('searchAccount.png')).toBeInTheDocument();
  });

  it('render with data files ', () => {
    const mockOnDownLoad = jest.fn();
    renderMockStoreId(<FileList files={[]} onDownload={mockOnDownLoad} />);
    expect(screen.queryByText('txt_no_data')).toBeInTheDocument();
  });

  it('render with data files and triger onDownLoad ', () => {
    const mockOnDownLoad = jest.fn();
    renderMockStoreId(<FileList files={files} onDownload={mockOnDownLoad} />);

    userEvent.click(screen.queryByRole('button')!);

    expect(mockOnDownLoad).toHaveBeenCalled();
  });
});
