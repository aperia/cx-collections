import React from 'react';
import { Button, Icon, Tooltip } from 'app/_libraries/_dls/components';
import { getFileExtension } from 'app/_libraries/_dls/components/Upload/helper';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import classnames from 'classnames';
import { isEmpty } from 'lodash';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface FileDetail {
  documentName: string;
  documentObjectId: string;
}
export interface DocumentList {
  onDownload: (file: FileDetail) => void;
  files: FileDetail[];
  noDataText?: string;
  className?: string;
  dataTestId?: string;
}
const DocumentList: React.FC<DocumentList> = ({
  onDownload,
  files,
  noDataText = 'txt_no_data',
  className,
  dataTestId
}) => {
  const { t } = useTranslation();
  if (isEmpty(files))
    return (
      <div
        className="color-grey"
        data-testid={genAmtId(dataTestId, 'no-data', '')}
      >
        {t(noDataText)}
      </div>
    );

  return (
    <div className={classnames('list-file', className)}>
      {files.map((file: FileDetail, index: number) => (
        <div key={file.documentObjectId} className="file-item">
          <div className="file-item-content justify-content-between">
            <i
              className={`${getFileExtension(file.documentName)} bg-file-type `}
            />
            <div className="file-name" title={file.documentName}>
              <div
                className="text-truncate"
                data-testid={genAmtId(dataTestId, `${index}-document`, '')}
              >
                {file.documentName}
              </div>
            </div>
            <div>
              <Tooltip
                placement="top"
                variant="primary"
                element={t('txt_download_file', 'Download file')}
                triggerClassName="d-flex"
                dataTestId={dataTestId}
              >
                <Button
                  variant="icon-secondary"
                  onClick={() => onDownload(file)}
                  dataTestId={genAmtId(dataTestId, `${index}-download-btn`, '')}
                >
                  <Icon
                    className="m-auto color-grey-l16"
                    name="download"
                    size="5x"
                    dataTestId={dataTestId}
                  />
                </Button>
              </Tooltip>
            </div>
          </div>
        </div>
      ))}
    </div>
  );
};

export default DocumentList;
