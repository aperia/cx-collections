export interface DialerAgentLoginData {
  username: string;
  agentKeyWord: string;
}
