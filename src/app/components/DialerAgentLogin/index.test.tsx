import React from 'react';
import { mockActionCreator, renderMockStoreId } from 'app/test-utils';
import DialerAgentLogin from '.';
import { fireEvent, screen, waitFor } from '@testing-library/dom';
import { I18N_DIALER_AGENT_MODAL } from 'app/constants/i18n';
import { actionsLiveVox } from 'app/livevox-dialer/_redux/reducers';
import { act } from '@testing-library/react';

const renderWrapper = () => {
  return renderMockStoreId(<DialerAgentLogin />, {});
};

describe('Render DialerAgentLogin modal', () => {
  it('render button', () => {
    const { wrapper } = renderWrapper();
    expect(
      wrapper.getByTestId('dialer-agent-login-modal-btn_dls-button')
    ).toBeInTheDocument();
  });

  it('handleOpenDialerAgentLoginModal when clicking button', () => {
    const { wrapper } = renderWrapper();
    const openDialerAgentLoginModalButton = screen.getByText(
      I18N_DIALER_AGENT_MODAL.TXT_AGENT_LOGIN_BUTTON
    );

    fireEvent.click(openDialerAgentLoginModalButton);
    expect(
      wrapper.getByTestId('dialer-agent-login-modal_dls-modal')
    ).toBeInTheDocument();

    const closeDialerAgentLoginModalButton = screen.getByTestId(
      'dialer-agent-login-modal-header_dls-modal-header_close-btn_dls-button'
    );
    fireEvent.click(closeDialerAgentLoginModalButton);
  });

  it('handleOpenDialerAgentLoginModal when clicking button with bad password', async () => {
    const mockLiveVoxDialerActions = mockActionCreator(actionsLiveVox);
    const mockLiveVoxDialerSessionIdThunk = mockLiveVoxDialerActions(
      'getLiveVoxDialerSessionIdThunk'
    );

    renderWrapper();
    const openDialerAgentLoginModalButton = screen.getByText(
      I18N_DIALER_AGENT_MODAL.TXT_AGENT_LOGIN_BUTTON
    );
    act(() => {
      fireEvent.click(openDialerAgentLoginModalButton);
    });

    const username = screen.getByTestId('dialer-agent-login-modal-username');
    const agentKeyWord = screen.getByTestId(
      'dialer-agent-login-modal-agent-key-word'
    );

    act(() => {
      fireEvent.change(username, { target: { value: 'test' } });
      fireEvent.change(agentKeyWord, {
        target: { value: process.env.REACT_APP_PWD! }
      });
    });

    const submitDialerAgentLoginButton = screen.getByText(
      I18N_DIALER_AGENT_MODAL.TXT_AGENT_LOGIN_SUBMIT_BUTTON
    );
    act(() => {
      fireEvent.click(submitDialerAgentLoginButton);
    });

    await waitFor(() =>
      expect(mockLiveVoxDialerSessionIdThunk).toHaveBeenCalledWith({
        clientName: 'fiserv_dev',
        userName: 'test',
        password: process.env.REACT_APP_PWD!,
        agent: true
      })
    );
  });
});
