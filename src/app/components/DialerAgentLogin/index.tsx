import React, { useState } from 'react';

// components
import {
  Modal,
  ModalBody,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { Button } from 'app/_libraries/_dls/components';
import { I18N_DIALER_AGENT_MODAL } from 'app/constants/i18n';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { ErrorMessage, Field, Form, Formik, FormikProps } from 'formik';
import * as Yup from 'yup';
import { DialerAgentLoginData } from './types';
import { useDispatch } from 'react-redux';
import { actionsLiveVox } from 'app/livevox-dialer/_redux/reducers';
import { SessionLoginData } from 'app/livevox-dialer/types';
import { LIVEVOX_CLIENT_NAME } from 'app/constants/liveVoxDialer';

const DialerAgentLogin: React.FC<{}> = ({}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const testId = 'dialer-agent-login-modal';
  const formId = 'dialer-agent-login-form';
  const [openDialerAgentLoginModal, setOpenDialerAgentLoginModal] =
    useState(false);

  const handleOpenDialerAgentLoginModal = () => {
    setOpenDialerAgentLoginModal(true);
  };

  const handleCloseDialerAgentLoginModal = () => {
    setOpenDialerAgentLoginModal(false);
  };

  const handleAgentLoginSubmit = (values: DialerAgentLoginData) => {
    // Request LiveVox Dialer session data
    const sessionLoginData: SessionLoginData = {
      clientName: LIVEVOX_CLIENT_NAME.CLIENT_NAME,
      userName: values.username,
      password: values.agentKeyWord,
      agent: true
    };

    dispatch(actionsLiveVox.getLiveVoxDialerSessionIdThunk(sessionLoginData));
    setOpenDialerAgentLoginModal(false);
  };

  const LoginValidation = Yup.object().shape({
    username: Yup.string().required('Required field'),
    agentKeyWord: Yup.string()
      .min(8)
      .max(16)
      .matches(/^[a-z0-9]+$/, 'Must Contain a minimum of 8 characters')
      .required('Required field')
  });

  const renderBodyByType = () => {
    return (
      <div className="dialer-agent-login-modal">
        <Formik
          initialValues={{
            username: '',
            agentKeyWord: ''
          }}
          validationSchema={LoginValidation}
          onSubmit={handleAgentLoginSubmit}
          validateOnBlur
          validateOnMount
        >
          {(props: FormikProps<DialerAgentLoginData>) => (
            <Form id={formId} className="row mb-64">
              <div className="m-auto w-75">
                <Field
                  className="col-12"
                  type="text"
                  name="username"
                  placeholder="Username"
                  autoComplete="off"
                  data-testid={`${testId}-username`}
                />
                <ErrorMessage className="mt-16 col-12" name="username" />
                <Field
                  className="mt-16 col-12"
                  type="password"
                  name="agentKeyWord"
                  placeholder="Password"
                  autoComplete="off"
                  data-testid={`${testId}-agent-key-word`}
                />
                <ErrorMessage className="mt-16 col-12" name="agentKeyWord" />
                <Button
                  id="dialer-agent-login-submit-btn"
                  className="mt-16 col-12"
                  type="submit"
                  size="lg"
                  variant="primary"
                  dataTestId={`${testId}-submit`}
                >
                  {t(I18N_DIALER_AGENT_MODAL.TXT_AGENT_LOGIN_SUBMIT_BUTTON)}
                </Button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    );
  };

  return (
    <>
      <Button
        id="dialer-agent-login-btn"
        onClick={handleOpenDialerAgentLoginModal}
        size="sm"
        variant="outline-primary"
        dataTestId={`${testId}-btn`}
      >
        {t(I18N_DIALER_AGENT_MODAL.TXT_AGENT_LOGIN_BUTTON)}
      </Button>
      {openDialerAgentLoginModal && (
        <Modal
          xs
          dataTestId={testId}
          show={openDialerAgentLoginModal}
          className="agent-login-modal"
        >
          <ModalHeader
            border
            closeButton
            onHide={handleCloseDialerAgentLoginModal}
            dataTestId={`${testId}-header`}
          >
            <ModalTitle dataTestId={`${testId}-title`}>
              {t('txt_livevox_agent_login_modal_title')}
            </ModalTitle>
          </ModalHeader>
          <ModalBody>{renderBodyByType()}</ModalBody>
        </Modal>
      )}
    </>
  );
};

export default DialerAgentLogin;
